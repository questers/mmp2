
@ECHO off                                                                    
REM ***************************************************************************
REM                                                                            
REM Script Name: provision.bat                                                  
REM Author: Raymond Wang                                                 
REM Date:                                              
REM                                                                            
REM Description: Xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx               
REM                                                                            
REM ***************************************************************************
REM

REM Environment setup
TITLE = Questers Provision Utility
SET TITLE=Questers Provision Utility
SET VERSION=0.0.1
SET PATH=%path%
SET ERROR_STRING=
SET CURR_DIR=%CD%
SET WTP=%CURR_DIR%/wtpdownload.exe
SET WTP_SCRIPT=%CURR_DIR%/wtp_script.ini
SET DKB_SCRIPT=%CURR_DIR%/dkb.ini
REM Change to dir contains right firmware images

REM Script Initialization Section      
cls

echo %DATE% %TIME%
echo ========================================================================
ECHO               %TITLE%(%VERSION%)
echo ========================================================================
echo Initializing runtime environment ......   

IF NOT EXIST %WTP% 	SET ERROR_STRING="%WTP%  not found" &	goto :FileNotFound

IF NOT EXIST %WTP_SCRIPT% SET ERROR_STRING="%WTP_SCRIPT% not found" &	goto :FileNotFound


for /f "tokens=1-7 delims=/-:. " %%a in ('echo;%date% %time%') do call date2sec %%a %%b %%c %%e %%f %%g TIME_START

%WTP% -x %DKB_SCRIPT%

echo waiting for DKB running ...
CALL DELAY 5

%WTP% -x %WTP_SCRIPT%

SET ERROR_STRING=Successfully

for /f "tokens=1-7 delims=/-:. " %%a in ('echo;%date% %time%') do call date2sec %%a %%b %%c %%e %%f %%g TIME_END

goto Finished


:FileNotFound
cls
echo ==================OOPS==========================
echo.
echo Current Directory:
echo %CURR_DIR%
echo File %ERROR_STRING%        
echo.
echo ================================================ 
goto :EOF

:Finished
SET /A TIME_COST=TIME_END-TIME_START
echo =================Operate Completed==============
echo.
echo %ERROR_STRING%  
echo.Time cost=%TIME_COST%s
echo ================================================
goto :EOF


:EOF
                                                                  