REM 
REM	DELAY subroutine
REM PARAMETER: %1
REM 

SETLOCAL 
IF %0% == "" (
SET /A DELAY_TIME=5
) ELSE (
SET /A DELAY_TIME=%1
)

for /f "tokens=1-7 delims=/-:. " %%a in ('echo;%date% %time%') do call date2sec %%a %%b %%c %%e %%f %%g TIME_START


:LOOP_START

for /f "tokens=1-7 delims=/-:. " %%a in ('echo;%date% %time%') do call date2sec %%a %%b %%c %%e %%f %%g TIME_END  
SET /A TIME_PAST=%TIME_END%-%TIME_START%
IF %TIME_PAST% LSS %DELAY_TIME% GOTO :LOOP_START

:LOOPEND
	

ENDLOCAL
