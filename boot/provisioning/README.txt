
This directory is for main board provisioning.
update.txt is fixed file name for sd card provisioning.
Put it into sdcard directory /root/recovery/ .So it full path is
/root/recovery/update.txt

In update.txt,the command syntax is full compatible with os loader.

Instructions for provisioning on a os loader only raw board 
1.copy all necessary files to sdcard /root/recovery/

2.copy update.txt (in this directory) to /root/recovery/

3.edit update.txt in /root/recovery to match instruction with your update files.
  In default,the update.txt describle all files:
  primary_gpt    -->filename primary_gpt
  secondary_gpt  -->filename secondary_gpt
  kernel				 -->uImage
  recovery       -->uImage_recovery
  system         -->system.img
  splash				 -->splash.img
4.There is two methods to run sd card provisioning/upgrade.
4.1 No partition table founded,this is often for product developing,
    Os loader will run provisioning automatically.
4.2 Force to run sdcard upgrade
    Within os loader booting,press combined keys BACK+MENU at least 3s will trigger sd upgrade to run.  
  

