
The wtp and auto_upg tools are for product provistioning.
All tools require win32 host to run

Step 1.wtp is a tool to write system booting sector 
  For MMP2,this including 
  TIM,WTM,OBM,UBOOT
  
  For example:
  >cd wtp
  >provision.bat
  2011-07-05 ���ڶ� 20:17:24.70
========================================================================
              Questers Provision Utility(0.0.1)
========================================================================
Initializing runtime environment ......
Non-trusted only version for NTIM Downloading and Jtag Re-Enablement
Version:      3.3.1.80
Release Date: Jul  5 2011
======================================
VERBOSE:FALSE
PORT:USB
Message:DOWNLOAD+MSG
FASTMODE:TRUE
TIM FILE:dkb\ntim_mmp2_mmc_dkb.bin
IMAGE FILE[1]:dkb\ntim_mmp2_mmc_dkb.bin
IMAGE FILE[2]:dkb\Wtm_rel_mmp2_h.bin
IMAGE FILE[3]:dkb\mmp2_mmc_dkb_h.bin
======================================
Number of Images listed in TIM: 3

TIM image ID list:
Image ID: TIMH
Image ID: WTMI
Image ID: DKBI
Number of files to download: 3
Platform Type (-T switch): 1
Downloading file dkb\ntim_mmp2_mmc_dkb.bin    100%
  Success: Download file complete for image #1!
Downloading file dkb\Wtm_rel_mmp2_h.bin    100%
  Success: Download file complete for image #2!
Downloading file dkb\mmp2_mmc_dkb_h.bin    100%
  Success: Download file complete for image #3!

Success: WtpDownload Exiting with Success Code!
waiting for DKB running ...
Non-trusted only version for NTIM Downloading and Jtag Re-Enablement
Version:      3.3.1.80
Release Date: Jul  5 2011
======================================
VERBOSE:FALSE
PORT:USB
Message:DOWNLOAD+MSG
FASTMODE:FALSE
TIM FILE:binary\ntim_mmp2_nand_bbu_ddr.bin
IMAGE FILE[1]:binary\ntim_mmp2_nand_bbu_ddr.bin
IMAGE FILE[2]:binary\MMP2_NTLOADER_3_2_19_h.bin
IMAGE FILE[3]:binary\Wtm_rel_mmp2_h.bin
IMAGE FILE[4]:binary\u-boot_h.bin
======================================
Number of Images listed in TIM: 4

TIM image ID list:
Image ID: TIMH
Image ID: OBMI
Image ID: WTMI
Image ID: OSLO
Number of files to download: 4
Platform Type (-T switch): 1

Version: 3215
Date: 0x12052009
Processor: MMP2
Downloading file binary\ntim_mmp2_nand_bbu_ddr.bin    100%
  Success: Download file complete for image #1!

Version: 3215
Date: 0x12052009
Processor: MMP2
  Error: Don't have a PartitionTable image asked for by target: 0x50415254
  Error: Continuing with next image
  Error: Image asked for by target does not exist!
  Error: Download failed for file number: 2

 Retrying this image download!

Version: 3215
Date: 0x12052009
Processor: MMP2
Downloading file binary\u-boot_h.bin    100%
  Success: Download file complete for image #2!

Version: 3215
Date: 0x12052009
Processor: MMP2
Downloading file binary\Wtm_rel_mmp2_h.bin    100%
  Success: Download file complete for image #3!

Version: 3215
Date: 0x12052009
Processor: MMP2
Downloading file binary\MMP2_NTLOADER_3_2_19_h.bin    100%
  Success: Download file complete for image #4!

Success: WtpDownload Exiting with Success Code!
=================Operate Completed==============

Successfully
Time cost=19s
================================================

Step 2:
  Now target is running with OSLOADER (u-boot for us)
  Prepare the target images and copy to directory firmware.
  >cd auto_upg
  >start.bat
  After all system images written,target will restart and boot to normal system.