
@ECHO off                                                                    
REM ***************************************************************************
REM                                                                            
REM Script Name: autoupgrade.bat                                                  
REM Author: Raymond Wang                                                 
REM Date:                                              
REM                                                                            
REM Description: Xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx               
REM                                                                            
REM ***************************************************************************
REM
REM TODO LIST
REM 1.Add multi product support
REM 2.
REM Environment setup
TITLE = Questers Auto Upgrade Utility
SET PATH=%path%
SET ERROR_STRING=
SET CURR_DIR=%CD%

REM Change to dir contains right firmware images
SET FIRMWARE_DIR=firmware
REM firmware



SET FASTBOOT=
SET KERNEL_IMAGE=""
SET RECOVERY_IMAGE=""
SET SYSTEM_IMAGE=""
SET USERDATA_IMAGE=""
SET CACHE_IMAGE=""
SET SPLASH_IMAGE=""
SET CUSTOM_IMAGE=""
SET PGPT_IMAGE=""
SET AGPT_IMAGE=""

REM VERSION                                                                                
REM Script Initialization Section      
cls
echo ========================================================================
ECHO               Questers Auto Upgrade Utility(Version:0.1)
echo ========================================================================
echo %DATE% %TIME%
echo Initializing runtime environment ......   

IF "%1"=="" (
  echo No firmware directory provided,using default %FIRMWARE_DIR%
) ELSE (
  SET FIRMWARE_DIR=%1
) 

IF NOT EXIST %CURR_DIR%/fastboot.exe (	
	SET ERROR_STRING="%CURR_DIR%/fastboot.exe not found" &	goto :FileNotFound
) ELSE (
		SET FASTBOOT=%CURR_DIR%/fastboot.exe 
)

IF NOT EXIST %CURR_DIR%/%FIRMWARE_DIR%/zImage (
	SET ERROR_STRING=%CURR_DIR%/%FIRMWARE_DIR%/zImage	not found &	goto :FileNotFound
) ELSE (
	SET KERNEL_IMAGE=%CURR_DIR%/%FIRMWARE_DIR%/zImage	
)

IF NOT EXIST %CURR_DIR%/%FIRMWARE_DIR%/zImage_recovery (
	SET ERROR_STRING=%CURR_DIR%/%FIRMWARE_DIR%/zImage_recovery	not found &	goto :FileNotFound
) ELSE (
	SET RECOVERY_IMAGE=%CURR_DIR%/%FIRMWARE_DIR%/zImage_recovery	
)

IF NOT EXIST %CURR_DIR%/%FIRMWARE_DIR%/system.img (
	SET ERROR_STRING=%CURR_DIR%/%FIRMWARE_DIR%/system.img	not found &	goto :FileNotFound
) ELSE (
	SET SYSTEM_IMAGE=%CURR_DIR%/%FIRMWARE_DIR%/system.img	
)

REM IF NOT EXIST %CURR_DIR%/%FIRMWARE_DIR%/userdata.img (
REM 	SET ERROR_STRING=%CURR_DIR%/%FIRMWARE_DIR%/userdata.img	not found &	goto :FileNotFound
REM ) ELSE (
REM 	SET USERDATA_IMAGE=%CURR_DIR%/%FIRMWARE_DIR%/userdata.img	
REM )
REM 
REM IF NOT EXIST %CURR_DIR%/%FIRMWARE_DIR%/cache.img (
REM 	SET ERROR_STRING=%CURR_DIR%/%FIRMWARE_DIR%/cache.img	not found &	goto :FileNotFound
REM ) ELSE (
REM 	SET CACHE_IMAGE=%CURR_DIR%/%FIRMWARE_DIR%/cache.img	
REM )

IF NOT EXIST %CURR_DIR%/%FIRMWARE_DIR%/custom.img (
	SET ERROR_STRING=%CURR_DIR%/%FIRMWARE_DIR%/custom.img	not found &	goto :FileNotFound
) ELSE (
	SET CUSTOM_IMAGE=%CURR_DIR%/%FIRMWARE_DIR%/custom.img	
)

IF NOT EXIST %CURR_DIR%/%FIRMWARE_DIR%/splash.img (
	SET ERROR_STRING=%CURR_DIR%/%FIRMWARE_DIR%/splash.img	not found &	goto :FileNotFound
) ELSE (
	SET SPLASH_IMAGE=%CURR_DIR%/%FIRMWARE_DIR%/splash.img	
)

IF NOT EXIST %CURR_DIR%/%FIRMWARE_DIR%/primary_gpt_2g (
	SET ERROR_STRING=%CURR_DIR%/%FIRMWARE_DIR%/primary_gpt_2g	not found &	goto :FileNotFound
) ELSE (
	SET PGPT_IMAGE=%CURR_DIR%/%FIRMWARE_DIR%/primary_gpt_2g
)

IF NOT EXIST %CURR_DIR%/%FIRMWARE_DIR%/secondary_gpt_2g (
	SET ERROR_STRING=%CURR_DIR%/%FIRMWARE_DIR%/secondary_gpt_2g	not found &	goto :FileNotFound
) ELSE (
	SET AGPT_IMAGE=%CURR_DIR%/%FIRMWARE_DIR%/secondary_gpt_2g
)

echo image list to flash
IF %KERNEL_IMAGE% NEQ "" echo %KERNEL_IMAGE% 
IF %RECOVERY_IMAGE% NEQ "" echo %RECOVERY_IMAGE% 
IF %SYSTEM_IMAGE% NEQ "" echo %SYSTEM_IMAGE%
IF %USERDATA_IMAGE% NEQ "" echo %USERDATA_IMAGE%
IF %CACHE_IMAGE% NEQ "" echo %CACHE_IMAGE%
IF %CUSTOM_IMAGE% NEQ "" echo %CUSTOM_IMAGE%
IF %SPLASH_IMAGE% NEQ "" echo %SPLASH_IMAGE%
IF %PGPT_IMAGE% NEQ "" echo %PGPT_IMAGE%
IF %AGPT_IMAGE% NEQ "" echo %AGPT_IMAGE%

for /f "tokens=1-7 delims=/-:. " %%a in ('echo;%date% %time%') do call date2sec %%a %%b %%c %%e %%f %%g TIME_START

REM Main Processing Section              
IF %KERNEL_IMAGE% NEQ "" %FASTBOOT% flash kernel %KERNEL_IMAGE%
IF %RECOVERY_IMAGE% NEQ "" %FASTBOOT% flash recovery %RECOVERY_IMAGE%
IF %CUSTOM_IMAGE% NEQ "" %FASTBOOT% flash custom %CUSTOM_IMAGE%
IF %SPLASH_IMAGE% NEQ "" %FASTBOOT% flash splash %SPLASH_IMAGE%
IF %PGPT_IMAGE% NEQ "" %FASTBOOT% flash pgpt %PGPT_IMAGE%
IF %AGPT_IMAGE% NEQ "" %FASTBOOT% flash agpt %AGPT_IMAGE%
IF %SYSTEM_IMAGE% NEQ "" %FASTBOOT% flash system %SYSTEM_IMAGE%
IF %USERDATA_IMAGE% NEQ "" %FASTBOOT% flash kernel %USERDATA_IMAGE%
IF %CACHE_IMAGE% NEQ "" %FASTBOOT% flash cache %CACHE_IMAGE%


%FASTBOOT% erase config
REM echo Target device will reboot now.
REM %FASTBOOT% reboot-bootloader

REM echo delay 5s ...
REM CALL DELAY 5

REM echo Save environment now
REM %FASTBOOT% oem saveenv
echo Target device will reboot now.
%FASTBOOT% reboot
SET ERROR_STRING=Successfully

for /f "tokens=1-7 delims=/-:. " %%a in ('echo;%date% %time%') do call date2sec %%a %%b %%c %%e %%f %%g TIME_END

goto Finished
                                                                               
REM Subroutine and Procedure Section                                           

:FileNotFound
REM cls
echo ==================OOPS==========================
echo.
echo Current Directory:
echo %CURR_DIR%
echo File %ERROR_STRING%        
echo.
echo ================================================ 
goto :EOF

:Finished
SET /A TIME_COST=TIME_END-TIME_START
echo =================Operate Completed==============
echo.
echo %ERROR_STRING%  
echo.Time cost=%TIME_COST%s
echo ================================================
goto :EOF

:EOF
                                                                  