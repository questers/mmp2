/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// MarvellBootUtilityDoc.cpp : implementation of the CMarvellBootUtilityDoc class
//

#include "stdafx.h"
#include "MarvellBootUtility.h"

#include "MarvellBootUtilityDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMarvellBootUtilityDoc

IMPLEMENT_DYNCREATE(CMarvellBootUtilityDoc, CDocument)

BEGIN_MESSAGE_MAP(CMarvellBootUtilityDoc, CDocument)
END_MESSAGE_MAP()


// CMarvellBootUtilityDoc construction/destruction

CMarvellBootUtilityDoc::CMarvellBootUtilityDoc()
{
	// TODO: add one-time construction code here

}

CMarvellBootUtilityDoc::~CMarvellBootUtilityDoc()
{

}

BOOL CMarvellBootUtilityDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CMarvellBootUtilityDoc serialization

void CMarvellBootUtilityDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		POSITION pos = GetFirstViewPosition();
		CEditView* pView = dynamic_cast<CEditView*>((CEditView*)GetNextView(pos));
		if ( pView )
		{
			// make sure control can handle all excessive output
			pView->GetEditCtrl().Serialize(ar);
		}
		// TODO: add storing code here
	}
	else
	{
		POSITION pos = GetFirstViewPosition();
		CEditView* pView = dynamic_cast<CEditView*>((CEditView*)GetNextView(pos));
		if ( pView )
		{
			pView->GetEditCtrl().Serialize(ar);
		}
		// TODO: add loading code here
	}
}


// CMarvellBootUtilityDoc diagnostics

#ifdef _DEBUG
void CMarvellBootUtilityDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMarvellBootUtilityDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMarvellBootUtilityDoc commands

void CMarvellBootUtilityDoc::OnCloseDocument()
{
	// TODO: Add your specialized code here and/or call the base class
	if ( theApp.DocumentClosing( this ) )
		CDocument::OnCloseDocument();
}

BOOL CMarvellBootUtilityDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	// TODO: Add your specialized code here and/or call the base class

	return CDocument::OnSaveDocument(lpszPathName);
}

BOOL CMarvellBootUtilityDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;


	// TODO:  Add your specialized creation code here

	return TRUE;
}
