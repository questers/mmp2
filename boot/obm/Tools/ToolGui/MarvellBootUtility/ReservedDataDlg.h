/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "TimDescriptor.h"

// CReservedDataDlg dialog

class CReservedDataDlg : public CDialog
{
	DECLARE_DYNAMIC(CReservedDataDlg)

public:
	CReservedDataDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CReservedDataDlg();

// Dialog Data
	enum { IDD = IDD_RESERVED_DATA_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

public:
	CListCtrl PackagesListCtrl;
	CListCtrl ExtPackagesListCtrl;

	CButton InsertPackageBtn;
	CButton EditPackageBtn;
	CButton MoveUpBtn;
	CButton MoveDownBtn;
	CButton DeletePackageBtn;
	CButton AddERDPackageBtn;
	CButton EditERDPackageBtn;
	CButton DeleteERDPackageBtn;

	afx_msg void OnBnClickedInsertPackageBtn();
	afx_msg void OnBnClickedEditPackageBtn();
	afx_msg void OnBnClickedDeletePackageBtn();
	afx_msg void OnBnClickedReservedDataMoveUpBtn();
	afx_msg void OnBnClickedReservedDataMoveDownBtn();

	afx_msg void OnBnClickedAddExtReservedBtn();
	afx_msg void OnBnClickedEditExtReservedBtn();
	afx_msg void OnBnClickedDeleteExtPackageBtn();
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);

public:
	void LocalTimDescriptor( CTimDescriptor& TimDescriptor ) { m_LocalTimDescriptor = TimDescriptor; }
	CTimDescriptor& LocalTimDescriptor() { return m_LocalTimDescriptor; }

	void UpdateChangeMarkerInTitle();

private:
	CTimDescriptor m_LocalTimDescriptor;

	void RefreshState();
	void UpdateControls();

	void RefreshExtPackage( int& nItem, const string& sPackage, t_PairList& Fields, t_stringVector& FieldNames );
	void RefreshExtPackage( int& nItem, t_ErdBaseVectorIter& ErdIter );
	void RefreshExtPackage( int& nItem, t_ConsumerIDVecIter& ErdIter );
	int iTotalSize;
	int iExtTotalSize;
};
