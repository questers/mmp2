/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

//#if TOOLS_GUI == 1
//#include "StdAfx.h"
//#include "MarvellBootUtility.h"
//#endif

#pragma warning ( disable : 4996 )

#include "XdbDdrInitScriptParser.h"

//#if LINUX
//#include <string.h>
//#include <stdlib.h>
//#else
//#if TOOLS_GUI == 1
//static char buf[1024]={0};
//#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
//#else
//#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
//#endif
//#endif

CXdbDdrInitScriptParser::CXdbDdrInitScriptParser(void)
{
}

CXdbDdrInitScriptParser::~CXdbDdrInitScriptParser(void)
{
	t_stringVectorIter Iter = DdrInitPkgLines.begin();
	while ( Iter != DdrInitPkgLines.end() )
		delete *Iter++;
	DdrInitPkgLines.clear();
}


bool CXdbDdrInitScriptParser::ParseFile( string sFilePath )
{
//	sFilePath = "C:\\siedev_3_3_1_0_WORK_AREA\\Tools\\ToolGui\\MarvellBootUtility";
//	sFilePath += "\\20100629_Brownstone_MMP2-ZR1_Elpida_EDJ1108DBSE-DJ-F_533MHz.xdb";

	if ( sFilePath.empty() )
		return false;

	ifstream ifsXdbScriptFile;
	ifsXdbScriptFile.open( sFilePath.c_str(), ios_base::in );
	if ( ifsXdbScriptFile.fail() || ifsXdbScriptFile.bad() )
	{
		printf ("\n  Error: Cannot open file name <%s> !", sFilePath.c_str());
		return false;
	}	

	DdrInitPkgLines.clear();
	DdrInitPkgLines.push_back( new string( "" ) );
	DdrInitPkgLines.push_back( new string( "DDR Initialization:" ));
	DdrInitPkgLines.push_back( new string( "DDR_PID: DDR#    ; replace # with a unique number" ));
	DdrInitPkgLines.push_back( new string( "" ) );
	DdrInitPkgLines.push_back( new string(  "Operations:" ));
	DdrInitPkgLines.push_back( new string( "" ) );
	DdrInitPkgLines.push_back( new string( "DDR_INIT_ENABLE: 1" ));
	DdrInitPkgLines.push_back( new string( "DDR_MEMTEST_ENABLE: 1" ));
	DdrInitPkgLines.push_back( new string( "End Operations:" ));
	DdrInitPkgLines.push_back( new string( "" ) );
	DdrInitPkgLines.push_back( new string( "Instructions:" ));
	DdrInitPkgLines.push_back( new string( "" ) );

	string sLine;
	string sNewLine;
	CTimLib TimLib;
	t_stringVector IfFILO;
	bool bIf = false;

	while ( ifsXdbScriptFile.good() && !ifsXdbScriptFile.fail() && !ifsXdbScriptFile.eof() )
	{
		::getline( ifsXdbScriptFile, sLine );
		
		string sTemp = TimLib.TrimLeadingWS( sLine );
		// keep blank line to maintain some formatting
		if ( sLine.empty() )
		{
			// make line a comment and save it
			DdrInitPkgLines.push_back( new string( sLine ) );
			continue;
		}

		// is line a comment?
		if ( sTemp[0] == '!' )
		{
			// make line a comment and save it
			DdrInitPkgLines.push_back( new string( ';' + sLine ) );
			continue;
		}

		size_t nPos = 0;

		// process "if/then/else/end" and nested "if"
		// only the body of the top level "if" is parsed
		// the rest is simply turned into comments
		nPos = TimLib.ToUpper( sTemp ).find( "IF" );
		if ( nPos == 0 )
		{
			if ( !bIf && IfFILO.empty()  )
				bIf = true;

			// push all "if" statements, including nested ones
			IfFILO.push_back( new string( "if" ) );
			// make line a comment and save it because there is no value assignment
			DdrInitPkgLines.push_back( new string( ';' + sLine ) );
			continue;
		}

		nPos = TimLib.ToUpper( sTemp ).find( "ELSE IF" );
		if ( nPos == 0 )
		{
			bIf = false;

			// push all "else if" statements, including nested ones
			IfFILO.push_back( new string( "else if" ) );

			// make line a comment and save it because there is no value assignment
			DdrInitPkgLines.push_back( new string( ';' + sLine ) );
			continue;
		}

		nPos = TimLib.ToUpper( sTemp ).find( "ELSE" );
		if ( nPos == 0 )
		{
			bIf = false;

			// make line a comment and save it because there is no value assignment
			DdrInitPkgLines.push_back( new string( ';' + sLine ) );
			continue;
		}

		nPos = TimLib.ToUpper( sTemp ).find( "END" );
		if ( nPos == 0 )
		{
			// pop all "end"s, including nested ones
			IfFILO.pop_back();

			if ( IfFILO.size() == 1 )
				bIf = true;
			else
				bIf = false;

			// make line a comment and save it because there is no value assignment
			DdrInitPkgLines.push_back( new string( ';' + sLine ) );
			continue;
		}

		// catch all lines, valid or not, and make as comments parsing an "if" statement
		// but not in the first "if" clause 
		if ( IfFILO.size() > 0 && bIf == false )
		{
			// make line a comment and save it because there is no value assignment
			DdrInitPkgLines.push_back( new string( ';' + sLine ) );
			continue;
		}

		nPos = TimLib.ToUpper( sTemp ).find( "SET OPTION" );
		if ( nPos == 0 )
		{
			// make line a comment and save it because there is no value assignment
			DdrInitPkgLines.push_back( new string( ';' + sLine ) );
			continue;
		}

		nPos = TimLib.ToUpper( sTemp ).find( "CUSTOM \"WAIT(" );
		if ( nPos == 0 )
		{
			// make line a comment and save it
			DdrInitPkgLines.push_back( new string( ';' + sLine ) );
			nPos = TimLib.ToUpper( sTemp ).find_first_of( "0123456789", nPos );
			DdrInitPkgLines.push_back( new string( "DELAY: " + TimLib.HexFormattedAscii( TimLib.Translate( sTemp.substr( nPos ) ) ) ) );
			DdrInitPkgLines.push_back( new string( "" ) );
			continue;
		}

		nPos = TimLib.ToUpper( sTemp ).find( "SET VALUE" );
		if ( nPos == string::npos )
		{
			// make line a comment and save it because there is no value assignment
			DdrInitPkgLines.push_back( new string( ';' + sLine ) );
			continue;
		}

		// skip trying to parse values that have operators
		nPos = TimLib.ToUpper( sTemp ).find_first_of( "|&" );
		if ( nPos != string::npos )
		{
			// make line a comment and save it because there is no value assignment
			DdrInitPkgLines.push_back( new string( ';' + sLine ) );
			continue;
		}

		// skip parsing if line contains a "@"
		nPos = sTemp.find( "@" );
		if ( nPos != string::npos )
		{
			// make line a comment and save it because there is no value assignment
			DdrInitPkgLines.push_back( new string( ';' + sLine ) );
			continue;
		}

		// try to parse line
		size_t nEqPos = sTemp.find( "=" );
		if ( nEqPos == string::npos )
		{
			// make line a comment and save it because there is no value assignment
			DdrInitPkgLines.push_back( new string( ';' + sLine ) );
			continue;
		}
		
		// remove all the "=" as we won't need them
		// replace it with a space to ensure a white-space terminator between values
		while ( string::npos != ( nEqPos = sTemp.find( "=" ) ) )
		{
			sTemp.replace( nEqPos, 1, " " );
		}

		// remove all occurrences of "/SIZE"
		
		while ( string::npos != ( nPos = TimLib.ToUpper( sTemp ).find( "/SIZE"  ) ) )
		{
			sTemp.replace( nPos, sizeof( "/SIZE" )-1, "" );
		}

		// remove all occurrences of "*(UNSIGNED LONG*)"
		
		while ( string::npos != ( nPos = TimLib.ToUpper( sTemp ).find( "*(UNSIGNED LONG*)" ) ) )
		{
			sTemp.replace( nPos, sizeof( "*(UNSIGNED LONG*)" )-1, "" );
		}

		// remove all occurrences of "*(UNSIGNED LONG *)"
		
		while ( string::npos != ( nPos = TimLib.ToUpper( sTemp ).find( "*(UNSIGNED LONG *)" ) ) )
		{
			sTemp.replace( nPos, sizeof( "*(UNSIGNED LONG *)" )-1, "" );
		}

		// remove all occurrences of "LONG"
		
		while ( string::npos != ( nPos = TimLib.ToUpper( sTemp ).find( "LONG"  ) ) )
		{
			sTemp.replace( nPos, sizeof( "LONG" )-1, "" );
		}

		// remove all occurrences of "PHYS"
	
		while ( string::npos != ( nPos = TimLib.ToUpper( sTemp ).find( "PHYS" ) ) )
		{
			sTemp.replace( nPos, sizeof( "PHYS" )-1, "" );
		}

		// remove all occurrences of "("
		
		while ( string::npos != (nPos = TimLib.ToUpper( sTemp ).find_first_of( "()" ) ) )
		{
			sTemp.replace( nPos, 1, "" );
		}

		nPos = TimLib.ToUpper( sTemp ).find( "SET VALUE" );
		if ( nPos == 0 )
		{
			sTemp.replace( nPos, sizeof( "SET VALUE" )-1, "" );
		}

		sTemp = TimLib.TrimLeadingWS( sTemp );
		// at this point, temp should be of the form "0x00000000 = 0x00000000 !comment"
		nPos = TimLib.ToUpper( sTemp ).find( "!" );
		if ( nPos != string::npos )
		{
			// make trailing comment of correct form
			sTemp.replace( nPos, 2, ";!" );
		}

		// make line a comment and save it
		DdrInitPkgLines.push_back( new string( ';' + sLine ) );
		sNewLine = "WRITE: " + sTemp;
		DdrInitPkgLines.push_back( new string( sNewLine ) );
		DdrInitPkgLines.push_back( new string( "" ) );
	}

	DdrInitPkgLines.push_back( new string( "" ) );
	DdrInitPkgLines.push_back( new string( "End Instructions:" ));
	DdrInitPkgLines.push_back( new string( "End DDR Initialization:" ));
	DdrInitPkgLines.push_back( new string( "" ) );

	// reduce multiple blank lines to a single blank line
	t_stringVectorIter Iter = DdrInitPkgLines.begin();
	while ( Iter != DdrInitPkgLines.end() )
	{
		t_stringVectorIter NextIter = Iter;
		if ( (*Iter)->empty() )
		{
			NextIter++;
			if ( NextIter != DdrInitPkgLines.end() )
			{
				if ( (*NextIter)->empty() )
				{
					delete *NextIter;
					DdrInitPkgLines.erase( NextIter );
					Iter--;
				}
			}
		}
		Iter++;
	}

	// deallocate any left over strings, it should be empty but just in case
	Iter = IfFILO.begin();
	while ( Iter != IfFILO.end() )
		delete *Iter++;
	IfFILO.clear();

	return true;
}

