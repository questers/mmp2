/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "editlistctrl.h"
#include "afxwin.h"

#include "PartitionData.h"

// CPartitionDlg dialog

class CPartitionDlg : public CDialog
{
	DECLARE_DYNAMIC(CPartitionDlg)

public:
	CPartitionDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPartitionDlg();

// Dialog Data
	enum { IDD = IDD_PARTITION_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

public:
	CHexItemEdit PartitionIdEdt;
	CHexItemEdit PartitionAttributesEdt;
	CComboBox PartitionUsageCb;
	CComboBox PartitionTypeCb;
	CHexItemEdit16 PartitionStartAddressEdt;
	CHexItemEdit16 PartitionEndAddressEdt;
	CComboBox RuntimeBBTTypeCb;
	CHexItemEdit RuntimeBBTLocationEdt;
	CHexItemEdit BackupRuntimeBBTLocationEdt;
	CHexItemEdit16 ReservedPoolStartAddressEdt;
	CHexItemEdit ReservedPoolSizeEdt;
	CComboBox ReservedPoolAlgorithmCb;

	CString PartitionId;
	CString PartitionAttributes;
	CString PartitionStartAddress;
	CString PartitionEndAddress;
	CString RuntimeBBTLocation;
	CString BackupRuntimeBBTLocation;
	CString ReservedPoolStartAddress;
	CString ReservedPoolSize;

public:
	afx_msg void OnEnChangePartitionIdEdt();
	afx_msg void OnCbnSelchangePartitionUsageCb();
	afx_msg void OnEnChangePartitionStartAddressEdt();
	afx_msg void OnCbnSelchangeRuntimeBbtTypeCb();
	afx_msg void OnEnChangeRuntimeBbtLocationEdt();
	afx_msg void OnEnChangeBackupRuntimeBbtLocationEdt();
	afx_msg void OnEnChangePartitionAttributesEdt();
	afx_msg void OnCbnSelchangePartitionTypeCb();
	afx_msg void OnEnChangePartitionEndAddressEdt();
	afx_msg void OnEnChangeReservedPoolStartAddressEdt();
	afx_msg void OnEnChangeReservedPoolSizeEdt();
	afx_msg void OnCbnSelchangeReservedPoolAlgorithmCb();
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);

	CPartitionData& PartitionData() { return m_PartitionData; }

public:
	void UpdateChangeMarkerInTitle();

private:
	CPartitionData m_PartitionData;
};
