/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <strstream>
using namespace std;

#include "CommandLineParser.h"
#include "PartitionTable.h"

class CTimDescriptorParser;

class CImageBuild
{
public:
	CImageBuild(void);
	~CImageBuild(void);

public:
	void TbbExePath( string& sFilePath ) { sTbbExePath = sFilePath; }
	string& TbbExePath() { return sTbbExePath; }

	void TimDescriptorFilePath( string& sFilePath );
	string& TimDescriptorFilePath() { return sTimDescriptorFilePath; }

	void HashKeyFilePath( string& sFilePath );
	string& HashKeyFilePath() { return sHashKeyFilePath; }

	void PrivateKeyFilePath( string& sFilePath );
	string& PrivateKeyFilePath() { return sPrivateKeyFilePath; }

	void DigitalSignatureFilePath( string& sFilePath );
	string& DigitalSignatureFilePath() { return sDigitalSignatureFilePath; }

	void TimInputFilePath( string& sFilePath );
	string& TimInputFilePath() { return sTimInputFilePath; }

	void TimOutputFilePath( string& sFilePath );
	string& TimOutputFilePath() { return sTimOutputFilePath; }

	void ReservedFilePath( string& sFilePath );
	string& ReservedFilePath() { return sReservedFilePath; }

	void PartitionFilePath( string& sFilePath );
	string& PartitionFilePath() { return sPartitionFilePath; }

	void Trusted( bool bSet ) { CommandLineParser.bIsNonTrusted = !bSet; m_bChanged = true; }
	bool Trusted() { return !CommandLineParser.bIsNonTrusted; }

	void Verify( bool bSet ) { CommandLineParser.bIsTimVerify = bSet; m_bChanged = true; }
	bool Verify() { return CommandLineParser.bIsTimVerify; }

	void VerboseMode( bool bSet ) { CommandLineParser.bVerbose = bSet; m_bChanged = true; }
	bool VerboseMode() { return CommandLineParser.bVerbose; }

	void Concatenate( bool bSet ) { CommandLineParser.bConcatenate = bSet; m_bChanged = true; }
	bool Concatenate() { return CommandLineParser.bConcatenate; }

	void OneNANDPadding( bool bSet ) { CommandLineParser.bOneNANDPadding = bSet; m_bChanged = true; }
	bool OneNANDPadding() { return CommandLineParser.bOneNANDPadding; }

	void PaddedSize( unsigned int uiSize ) { CommandLineParser.uiPaddedSize = uiSize; m_bChanged = true; }
	unsigned int PaddedSize() { return CommandLineParser.uiPaddedSize; }

	void HashKeyFile( bool bSet ) { CommandLineParser.bIsHashFile = bSet; m_bChanged = true; }
	bool HashKeyFile() { return CommandLineParser.bIsHashFile; }

	void PrivateKeyFile( bool bSet ) { CommandLineParser.bIsKeyFile = bSet; m_bChanged = true; }
	bool PrivateKeyFile() { return CommandLineParser.bIsKeyFile; }

	void DigitalSignatureFile( bool bSet ) { CommandLineParser.bIsDsFile = bSet; m_bChanged = true; }
	bool DigitalSignatureFile() { return CommandLineParser.bIsDsFile; }

	void TimInputFile( bool bSet ) { CommandLineParser.bIsTimInFile = bSet; m_bChanged = true; }
	bool TimInputFile() { return CommandLineParser.bIsTimInFile; }

	void TimOutputFile( bool bSet ) { CommandLineParser.bIsTimOutFile = bSet; m_bChanged = true; }
	bool TimOutputFile() { return CommandLineParser.bIsTimOutFile; }

	void ReservedFile( bool bSet ) { CommandLineParser.bIsReservedDataInFile = bSet; m_bChanged = true; }
	bool ReservedFile() { return CommandLineParser.bIsReservedDataInFile; }

	void PartitionFile( bool bSet ) { CommandLineParser.bIsPartitionDataFile = bSet; m_bChanged = true; }
	bool PartitionFile() { return CommandLineParser.bIsPartitionDataFile; }

	void AdditionalOptions( string& sOption ) { sAdditionalOptions = sOption; m_bChanged = true; }
	string& AdditionalOptions() { return sAdditionalOptions; }

	void AdditionalOptionsEnable( bool bSet ) { bAdditionalOptions = bSet; m_bChanged = true; }
	bool AdditionalOptionsEnable() { return bAdditionalOptions; }

	void ProcessorTypeEnable( bool bSet ) { bProcessorType = bSet; m_bChanged = true; }
	bool ProcessorTypeEnable() { return bProcessorType; }

	unsigned int Mode() { return CommandLineParser.iOption; }
	void Mode( unsigned int uiOption ) { CommandLineParser.iOption = uiOption; m_bChanged = true; }

#if TOOLS_GUI == 1
	bool SaveState( ofstream& ofs );
	bool LoadState( ifstream& ifs );
#endif

	bool IsChanged(){ return m_bChanged; }
	void Changed( bool bSet ){ m_bChanged = bSet; }

	void Reset();
	string& CommandLineArgs(CTimDescriptorParser& TimDescriptorParser);

	bool Launch();
	bool Validate();

	CPartitionTable& PartitionTable() { return m_PartitionTable; }

#if TOOLS_GUI == 1
	CCommandLineParser& CommandLineParser;
#else
	CCommandLineParser CommandLineParser;
#endif

private:
	string  sTbbExePath;
	string  sTimDescriptorFilePath;
	string	sHashKeyFilePath;
	string  sPrivateKeyFilePath;
	string	sDigitalSignatureFilePath;
	string  sTimInputFilePath;
	string  sTimOutputFilePath;
	string	sReservedFilePath;
	string  sPartitionFilePath;

	string	sCommandLineArgs;
	string  sAdditionalOptions;

	bool bAdditionalOptions;
	bool bProcessorType;

	CPartitionTable     m_PartitionTable;

	bool m_bChanged;
};
