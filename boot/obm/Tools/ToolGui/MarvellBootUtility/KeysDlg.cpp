/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// KeysDlg.cpp : implementation file
//

#include "stdafx.h"
#if TRUSTED

#include "MarvellBootUtility.h"
#include "KeysDlg.h"
#include "KeyDlg.h"
#include "CommentDlg.h"

// CKeysDlg dialog

IMPLEMENT_DYNAMIC(CKeysDlg, CDialog)

CKeysDlg::CKeysDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CKeysDlg::IDD, pParent)
{

}

CKeysDlg::~CKeysDlg()
{
}

void CKeysDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_REMOVE_KEY_BTN, RemoveKeyBtn);
    DDX_Control(pDX, IDC_EDIT_KEY_BTN, EditKeyBtn);
    DDX_Control(pDX, IDC_KEYS_LIST_CTRL, KeysListCtrl);

    // updates marker in title
    UpdateChangeMarkerInTitle();
    theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CKeysDlg, CDialog)
    ON_BN_CLICKED(IDC_REMOVE_KEY_BTN, &CKeysDlg::OnBnClickedRemoveKeyBtn)
    ON_BN_CLICKED(IDC_EDIT_KEY_BTN, &CKeysDlg::OnBnClickedEditKeyBtn)
    ON_BN_CLICKED(IDC_INSERT_KEY_BTN, &CKeysDlg::OnBnClickedInsertKeyBtn)
    ON_BN_CLICKED(IDC_KEY_MOVE_UP_BTN, &CKeysDlg::OnBnClickedKeyMoveUpBtn)
    ON_BN_CLICKED(IDC_KEY_MOVE_DOWN_BTN, &CKeysDlg::OnBnClickedKeyMoveDownBtn)
    ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()


// CKeysDlg message handlers
BOOL CKeysDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    KeysListCtrl.SetExtendedStyle(LVS_EX_TRACKSELECT
                                  //  LVS_EX_BORDERSELECT
                                  | LVS_EX_FULLROWSELECT 
                                  | LVS_EX_HEADERDRAGDROP 
                                  | LVS_EX_ONECLICKACTIVATE 
//								  | LVS_EX_LABELTIP 
                                  //| LVS_EX_GRIDLINES
                                  );

    KeysListCtrl.InsertColumn(0,"Key ID", LVCFMT_LEFT, 120, -1 );
    KeysListCtrl.InsertColumn(1,"Key Tag", LVCFMT_LEFT, 120, -1 );
    KeysListCtrl.InsertColumn(2,"Hash Algorithm", LVCFMT_LEFT, 120, -1 );
    KeysListCtrl.InsertColumn(3,"Component Size", LVCFMT_LEFT, 120, -1 );

    RefreshState();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}


void CKeysDlg::RefreshState()
{
    KeysListCtrl.DeleteAllItems();
    t_KeyList KeyList = m_LocalTimDescriptor.KeyList();
    t_KeyListIter iter = KeyList.begin();
    int nItem = 0;
    while( iter != KeyList.end() )
    {
        CKey* pKey = (*iter);
        KeysListCtrl.InsertItem(nItem, pKey->KeyId().c_str());
        KeysListCtrl.SetItemData(nItem,(DWORD_PTR)pKey);  // item retains image*
        KeysListCtrl.SetItem(nItem,1,LVIF_TEXT,pKey->KeyTag().c_str(),0,0,0,0);
        KeysListCtrl.SetItem(nItem,2,LVIF_TEXT,pKey->HashAlgorithmId().c_str(),0,0,0,0);
        stringstream ss;
        ss << pKey->KeySize();
        KeysListCtrl.SetItem(nItem,3,LVIF_TEXT,ss.str().c_str(),0,0,0,0);
        
        nItem++;
        iter++;
    }

    UpdateData( FALSE );
}

void CKeysDlg::OnBnClickedRemoveKeyBtn()
{
    POSITION Pos = KeysListCtrl.GetFirstSelectedItemPosition();
    if ( Pos )
    {
        int nItem = KeysListCtrl.GetNextSelectedItem(Pos);
        CKey* pKey = (CKey*)KeysListCtrl.GetItemData(nItem);  // item retains image*
        if ( pKey )
        {
            m_LocalTimDescriptor.KeyList().remove(pKey);
            delete pKey;
            m_LocalTimDescriptor.KeysChanged(true);
            m_LocalTimDescriptor.NotWritten(true);
        }
    }
    RefreshState();
}

void CKeysDlg::OnBnClickedEditKeyBtn()
{
    POSITION Pos = KeysListCtrl.GetFirstSelectedItemPosition();
    if ( Pos )
    {
        int nItem = KeysListCtrl.GetNextSelectedItem(Pos);
        CKey* pKey = (CKey*)KeysListCtrl.GetItemData(nItem);  // item retains key*
        CKeyDlg Dlg(m_LocalTimDescriptor); 
        Dlg.Key( *pKey );	// causes trusted bool to be set correctly in dialog
        if ( Dlg.DoModal() == IDOK )
        {
            if ( Dlg.Key().IsChanged() )
            {
                // update key with changes
                *pKey = Dlg.Key();
                m_LocalTimDescriptor.KeysChanged( true );
                m_LocalTimDescriptor.NotWritten(true);
                RefreshState();
            }
        }
    }
}

void CKeysDlg::OnBnClickedInsertKeyBtn()
{
    UpdateData( TRUE );
    if ( m_LocalTimDescriptor.KeyList().size() >= 3 )
    {
        AfxMessageBox("Maximum of 3 Keys already defined.  Delete a key if you wish to add another.");
        return;
    }

    int nItem = KeysListCtrl.GetSelectionMark();
    if ( nItem == -1 )
        // nothing selected 
        nItem = KeysListCtrl.GetItemCount();
    else
        nItem++;	// insert after selected item

    CKey* pKey = new CKey;
    CKeyDlg Dlg(m_LocalTimDescriptor);
    Dlg.Key( *pKey );	// causes trusted bool to be set correctly in dialog
    if ( Dlg.DoModal() == IDOK )
    {
        if ( Dlg.Key().IsChanged() )
        {
            *pKey = Dlg.Key();
            //put key in correct position in list
            t_KeyListIter Iter = m_LocalTimDescriptor.KeyList().begin();
            int ItemPos = nItem-1;
            while ( Iter != m_LocalTimDescriptor.KeyList().end() && ItemPos >= 0 )
            {
                ItemPos--;
                Iter++;
            }
            m_LocalTimDescriptor.KeyList().insert( Iter, pKey );
            m_LocalTimDescriptor.KeysChanged(true);		
            m_LocalTimDescriptor.NotWritten(true);
            RefreshState();
        }
    }
    else
        delete pKey;
}

void CKeysDlg::OnBnClickedKeyMoveUpBtn()
{
    int nItem = KeysListCtrl.GetSelectionMark();
    if ( nItem > 0 )
    {
        CKey* pKey = (CKey*)KeysListCtrl.GetItemData( nItem );
        t_KeyList& KeyList = m_LocalTimDescriptor.KeyList();

        //put key in correct position in list
        t_KeyListIter Iter = KeyList.begin();
        while ( Iter != KeyList.end() )
        {
            if ( *Iter == pKey )
            {
                // move it up unless already at top of list
                if ( Iter != KeyList.begin() )
                {
                    t_KeyListIter insertiter = --Iter;

                    KeyList.remove( pKey );
                    KeyList.insert( insertiter, pKey );
                    KeysListCtrl.SetSelectionMark( --nItem );

                    m_LocalTimDescriptor.KeysChanged(true);
                    m_LocalTimDescriptor.NotWritten(true);
                }
                break;
            }
            Iter++;
        }
    }

    RefreshState();
}

void CKeysDlg::OnBnClickedKeyMoveDownBtn()
{
    int nItem = KeysListCtrl.GetSelectionMark();
    if ( nItem != -1 )
    {
        CKey* pKey = (CKey*)KeysListCtrl.GetItemData( nItem );

        //put key in correct position in list
        t_KeyList& KeyList = m_LocalTimDescriptor.KeyList();
        t_KeyListIter Iter = KeyList.begin();
        while ( Iter != KeyList.end() )
        {
            if ( *Iter == pKey )
            {
                // move it down unless already at end of list
                if ( ++Iter != KeyList.end() )
                {
                    KeyList.remove( pKey );
                    KeyList.insert( ++Iter, pKey );
                    KeysListCtrl.SetSelectionMark( ++nItem );
                    m_LocalTimDescriptor.KeysChanged(true);
                    m_LocalTimDescriptor.NotWritten(true);
                }
                break;
            }
            Iter++;
        }
    }

    RefreshState();
}

void CKeysDlg::UpdateChangeMarkerInTitle()
{
    if ( m_hWnd == 0 )
        return;

    CString sTitle;
    GetWindowTextA( sTitle );
    // add or remove * as appropriate
    if ( m_LocalTimDescriptor.KeysIsChanged() )
    {
        if ( -1 == sTitle.Find('*') )
            sTitle += "*";
    }
    else
    {
        if ( -1 != sTitle.Find('*') )
            sTitle.Remove('*');
    }
    SetWindowTextA(sTitle);
    UpdateWindow();
}

void CKeysDlg::OnContextMenu(CWnd* pWnd, CPoint /*point*/)
{
    CCommentDlg Dlg(this);
    int nItem = -1;
    int iCtlId = pWnd->GetDlgCtrlID();
    switch ( iCtlId )
    {
        case IDC_KEYS_LIST_CTRL:
            nItem = KeysListCtrl.GetSelectionMark();
            if ( nItem != -1 )
            {
                CKey* pData = (CKey*)KeysListCtrl.GetItemData( nItem );
                if ( pData )
                    Dlg.Display( pData );
            }
            break;

        default:
            break;
    }
}

#endif


