/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// ImportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "ImportDlg.h"
#include "XdbDdrInitScriptParser.h"
#include "TimBinaryParser.h"
#include "ImportTimBinaryDlg.h"

// CImportDlg dialog

IMPLEMENT_DYNAMIC(CImportDlg, CDialog)

CImportDlg::CImportDlg(CTimDescriptor& rTimDescriptor, CWnd* pParent /*=NULL*/)
    : CDialog(CImportDlg::IDD, pParent), TimDescriptor( rTimDescriptor )
{
    sDdrXdbScriptFilepath = _T("");
    sTimBinaryFilepath = _T("");
    bImportedXdbScript = false;
    bImportedTimBinary = false;
    sTimText = TimDescriptor.TimDescriptorText();
}

CImportDlg::~CImportDlg()
{
    t_stringListIter iter = Lines.begin();
    while( iter != Lines.end() )
        delete (*iter++);
    Lines.clear();
}

void CImportDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_DDR_XDB_SCRIPT_FILE_PATH_EDT, sDdrXdbScriptFilepath);
    DDX_Text(pDX, IDC_TIM_BINARY_FILE_PATH_EDT, sTimBinaryFilepath);
    DDX_Control(pDX, IDC_IMPORTED_XDB_SCRIPT_TXT, ImportedXdbScriptTxt);
    DDX_Control(pDX, IDC_IMPORTED_TIM_BINARY_TXT, ImportedTimBinaryTxt);
    DDX_Control(pDX, IDC_TIM_PROC_CB, ProcessorTypeCb);
}


BEGIN_MESSAGE_MAP(CImportDlg, CDialog)
    ON_BN_CLICKED(IDC_DDR_XDB_SCRIPT_BROWSE_BTN, &CImportDlg::OnBnClickedDdrXdbScriptBrowseBtn)
    ON_EN_CHANGE(IDC_DDR_XDB_SCRIPT_FILE_PATH_EDT, &CImportDlg::OnEnChangeDdrXdbScriptFilePathEdt)
    ON_BN_CLICKED(IDC_IMPORT_DDR_XDB_FILE_BTN, &CImportDlg::OnBnClickedImportDdrXdbFileBtn)
    ON_BN_CLICKED(IDC_TIM_BINARY_FILE_BROWSE_BTN, &CImportDlg::OnBnClickedTimBinaryFileBrowseBtn)
    ON_BN_CLICKED(IDC_IMPORT_TIM_BINARY_FILE_BTN, &CImportDlg::OnBnClickedImportTimBinaryFileBtn)
    ON_EN_CHANGE(IDC_TIM_BINARY_FILE_PATH_EDT, &CImportDlg::OnEnChangeTimBinaryFilePathEdt)
    ON_CBN_SELCHANGE(IDC_TIM_PROC_CB, &CImportDlg::OnCbnSelchangeTimProcCb)
END_MESSAGE_MAP()


// CImportDlg message handlers

BOOL CImportDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    int idx = -1;
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA168].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA30x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA31x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA32x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA688].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA91x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA92x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA93x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA94x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA95x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[ARMADA168].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[ARMADA610].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[MG1].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[TAVOR_PV_MG2].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[ESHEL].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA978].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[ARMADA620].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[ESHEL_LTE].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[ARMADA622].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[WUKONG].c_str());
//	ProcessorTypeCb.InsertString(++idx, gsProcessorType[P_88AP001].c_str());

    UpdateControls();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void CImportDlg::OnCbnSelchangeTimProcCb()
{
    UpdateData(TRUE);

    int idx = CB_ERR;
    if ( (idx = ProcessorTypeCb.GetCurSel()) != CB_ERR )
    {
        CString sText;
        ProcessorTypeCb.GetWindowTextA(sText);

        // keep all the Processor Types consistent
        TimDescriptor.ProcessorType( (LPCSTR)sText );
    }
}

void CImportDlg::UpdateControls()
{
    ProcessorTypeCb.SetCurSel( ProcessorTypeCb.FindStringExact( 0, TimDescriptor.ProcessorType().c_str() ));

    GetDlgItem( IDC_IMPORT_DDR_XDB_FILE_BTN )->EnableWindow( TimDescriptor.TimDescriptorFilePath().length() > 0 && !bImportedXdbScript && sDdrXdbScriptFilepath.GetLength() > 0 );
    GetDlgItem( IDC_DDR_XDB_SCRIPT_FILE_PATH_EDT )->EnableWindow( TimDescriptor.TimDescriptorFilePath().length()>0 );
    GetDlgItem( IDC_DDR_XDB_SCRIPT_BROWSE_BTN )->EnableWindow( TimDescriptor.TimDescriptorFilePath().length()>0 );
    
    GetDlgItem( IDC_IMPORT_TIM_BINARY_FILE_BTN )->EnableWindow( !bImportedTimBinary && sTimBinaryFilepath.GetLength() > 0 );
    ImportedXdbScriptTxt.ShowWindow( bImportedXdbScript ? SW_SHOWNORMAL : SW_HIDE );
    ImportedTimBinaryTxt.ShowWindow( bImportedTimBinary ? SW_SHOWNORMAL : SW_HIDE );
}

void CImportDlg::OnBnClickedDdrXdbScriptBrowseBtn()
{
    UpdateData( TRUE );
    CFileDialog Dlg( TRUE, _T("*.xdb"), NULL, 
                     OFN_PATHMUSTEXIST,
                     _T("XDB (*.xdb)|*.xdb|All Files (*.*)|*.*||") );
    if ( Dlg.DoModal() == IDOK )
    {
        sDdrXdbScriptFilepath = Dlg.GetPathName();
        bImportedXdbScript = false;
        UpdateData( FALSE );
    }
    UpdateControls();
}

void CImportDlg::OnEnChangeDdrXdbScriptFilePathEdt()
{
    // TODO:  If this is a RICHEDIT control, the control will not
    // send this notification unless you override the CDialog::OnInitDialog()
    // function and call CRichEditCtrl().SetEventMask()
    // with the ENM_CHANGE flag ORed into the mask.

    // TODO:  Add your control notification handler code here
    bImportedXdbScript = false;
    UpdateControls();
}

void CImportDlg::OnBnClickedImportDdrXdbFileBtn()
{
    if ( sDdrXdbScriptFilepath.IsEmpty() )
        return;

    CXdbDdrInitScriptParser XdbParser;
    if ( XdbParser.ParseFile( string((CStringA)sDdrXdbScriptFilepath )) )
    {
        // find the insertion point for the ddri package
        size_t nPos = sTimText.rfind( "End DDR Initialization:" );
        if ( nPos != string::npos )
            // after the end of the existing ddri
            nPos += sizeof("End DDR Initialization:");
        else
            // before the end of extended reserved data
            nPos = sTimText.rfind( "End Extended Reserved Data:" );

//		// skip intervening white space
//		if ( nPos != string::npos )
//			nPos = sTimText.find_first_not_of( " \t\n\r", nPos );

        if ( nPos == string::npos )
            nPos = sTimText.length();

        // if no Extended Reserved Data defined, then add one
        if ( sTimText.find( "\nExtended Reserved Data:" ) == string::npos )
        {
            sTimText.insert( nPos, "\nExtended Reserved Data:\n" );
            nPos += sizeof( "Extended Reserved Data:" );
        }

        // if no End Extended Reserved Data defined, then add one
        if ( sTimText.rfind( "\nEnd Extended Reserved Data:" ) == string::npos )
            sTimText.insert( nPos, "\nEnd Extended Reserved Data:\n" );	

        sTimText.insert( nPos++, "\n" );

        // insert the package
        t_stringVectorIter Iter = XdbParser.DdrInitPkgLines.end();
        while ( Iter != XdbParser.DdrInitPkgLines.begin() )
        {
            Iter--;
            sTimText.insert( nPos, "\n" );
            sTimText.insert( nPos, *(*Iter) );
        }

        for ( unsigned int i = 0; i < sTimText.length(); i++ )
        {
            if ( sTimText[i] == '\n' )
            {
                sTimText = sTimText.substr(0,i) + '\r' + sTimText.substr(i);
                i++;
            }
        }

        CString sLine;
        string sModifiedTimText = sTimText;
        while ( sModifiedTimText.length() > 0 )
        {
            size_t pos = sModifiedTimText.find("\r\n");
            sLine = sModifiedTimText.substr(0, pos).c_str();
            Lines.push_back( new string( sLine ) );
            sModifiedTimText = sModifiedTimText.substr(pos+2);
        }
        bImportedXdbScript = true;
    }
    else
        AfxMessageBox( "Unable to import XBD script file." );

    UpdateControls();
}

void CImportDlg::OnBnClickedTimBinaryFileBrowseBtn()
{
    UpdateData( TRUE );
    CFileDialog Dlg( TRUE, _T("*.bin"), NULL, 
                     OFN_PATHMUSTEXIST,
                     _T("Binary (*.bin)|*.bin|All Files (*.*)|*.*||") );
Retry:
    if ( Dlg.DoModal() == IDOK )
    {
        if ( Dlg.GetPathName().Find( "_h.bin" ) >= 0 )
        {
            CString sMsg( "File name appears to be a TBB generated *_h.bin file instead of an image.bin file." );
            sMsg += "Do you want to select another *.bin file?";
            if ( IDYES == AfxMessageBox( sMsg, MB_YESNO ) )
                goto Retry;
        }

        sTimBinaryFilepath = Dlg.GetPathName();
        bImportedTimBinary = false;
        UpdateData( FALSE );
    }

    UpdateControls();
}

void CImportDlg::OnEnChangeTimBinaryFilePathEdt()
{
    // TODO:  If this is a RICHEDIT control, the control will not
    // send this notification unless you override the CDialog::OnInitDialog()
    // function and call CRichEditCtrl().SetEventMask()
    // with the ENM_CHANGE flag ORed into the mask.

    // TODO:  Add your control notification handler code here

    // TBD implementation
    bImportedTimBinary = false;
    UpdateControls();
}

void CImportDlg::OnBnClickedImportTimBinaryFileBtn()
{
    if ( sTimBinaryFilepath.IsEmpty() )
        return;

    if ( sTimBinaryFilepath.Find( "_h.bin" ) >= 0 )
    {
        CString sMsg( "File name appears to be a TBB generated *_h.bin file instead of an image.bin file." );
        sMsg += "Do you want to try to import this file? Results may be unpredictable.";
        if ( IDNO == AfxMessageBox( sMsg, MB_YESNO ) )
            return;
    }

    if ( ProcessorTypeCb.GetCurSel() == CB_ERR )
    {
        AfxMessageBox("A Processor Type must be selected to parse the imported binary file.");
        ProcessorTypeCb.SetFocus();
        return;
    }

    CTimBinaryParser TimBinParser;
    string sProcessorType = TimDescriptor.ProcessorType();

    if ( TimBinParser.ParseFile( string((CStringA)sTimBinaryFilepath )) )
    {
        CImportTimBinaryDlg Dlg( TimBinParser.TimDescriptor );
        if ( Dlg.DoModal() == IDOK )
        {
            TimDescriptor = Dlg.TimDescriptor;
            sTimText = TimDescriptor.TimDescriptorText();
            
            // populate Lines from text
            CString sText( sTimText.c_str() );
            CString sLine;
            while ( sText.GetLength() > 0 )
            {
                int pos = sText.Find("\n");
                sLine = sText.Left(pos);
                Lines.push_back( new string( sLine ) );
                sText = sText.Right(sText.GetLength()-pos-1);
            }
        }
        bImportedTimBinary = true;
    }
    else
        AfxMessageBox( "Unable to parse binary image file. File must be a TIM or NTIM binary image to import.");
        
    TimDescriptor.ProcessorType( sProcessorType );

    UpdateControls();
}

void CImportDlg::SetTimText( string& sText )
{
    sTimText = sText;
}

