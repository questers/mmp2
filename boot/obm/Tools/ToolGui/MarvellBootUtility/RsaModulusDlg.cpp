/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// RsaModulusDlg.cpp : implementation file
//

#include "stdafx.h"

#if TRUSTED

#include "MarvellBootUtility.h"
#include "RsaModulusDlg.h"


// CRsaModulusDlg dialog

IMPLEMENT_DYNAMIC(CRsaModulusDlg, CDialog)

CRsaModulusDlg::CRsaModulusDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRsaModulusDlg::IDD, pParent)
{
	uiSizeInBits = 0;
}

CRsaModulusDlg::~CRsaModulusDlg()
{
}

void CRsaModulusDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RSA_SYSTEM_MODULUS_LIST_CTRL, RsaSystemModulusListCtrl);
	DDX_Text(pDX, IDC_SIZE_IN_BITS, uiSizeInBits);

	// updates marker in title
	UpdateChangeMarkerInTitle();
	theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CRsaModulusDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CRsaModulusDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_RESET_MODULUS_BTN, &CRsaModulusDlg::OnBnClickedResetModulusBtn)
	ON_NOTIFY(LVN_ITEMACTIVATE, IDC_RSA_SYSTEM_MODULUS_LIST_CTRL, &CRsaModulusDlg::OnLvnItemActivateRsaSystemModulusListCtrl)
	ON_NOTIFY(NM_KILLFOCUS, IDC_RSA_SYSTEM_MODULUS_LIST_CTRL, &CRsaModulusDlg::OnNMKillfocusRsaSystemModulusListCtrl)
END_MESSAGE_MAP()


// CRsaModulusDlg message handlers
BOOL CRsaModulusDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	RsaSystemModulusListCtrl.SetExtendedStyle( //  LVS_EX_TRACKSELECT
											    //  LVS_EX_BORDERSELECT
												LVS_EX_FULLROWSELECT 
												| LVS_EX_HEADERDRAGDROP 
												| LVS_EX_ONECLICKACTIVATE 
												| LVS_EX_LABELTIP 
												| LVS_EX_GRIDLINES
											  );

	RsaSystemModulusListCtrl.InsertColumn(0,"#", LVCFMT_LEFT, 36, -1 );
	RsaSystemModulusListCtrl.InsertColumn(1,"Modulus Word #", LVCFMT_LEFT, 140, -1 );
	RsaSystemModulusListCtrl.InsertColumn(2,"Modulus Word #+1", LVCFMT_LEFT, 140, -1 );
	RsaSystemModulusListCtrl.InsertColumn(3,"Modulus Word #+2", LVCFMT_LEFT, 140, -1 );
	RsaSystemModulusListCtrl.InsertColumn(4,"Modulus Word #+3", LVCFMT_LEFT, 140, -1 );

	RsaSystemModulusListCtrl.EditControlType[0] = ReadOnly;
	RsaSystemModulusListCtrl.EditControlType[1] = HexEdit;
	RsaSystemModulusListCtrl.EditControlType[2] = HexEdit;
	RsaSystemModulusListCtrl.EditControlType[3] = HexEdit;
	RsaSystemModulusListCtrl.EditControlType[4] = HexEdit;

	uiSizeInBits = m_DigitalSignature.KeySize();
	int nRows = (((uiSizeInBits+31)/32)+3)/4;
	for ( int i = 0; i < nRows; i++ )
	{
		RsaSystemModulusListCtrl.InsertItem(i,""); 
	}

	UpdateData(FALSE);
	RefreshState();
	RsaSystemModulusListCtrl.Changed( m_DigitalSignature.IsChanged() );
	SetWindowTextA( m_sTitle.c_str() );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CRsaModulusDlg::RefreshState()
{
	UpdateData( TRUE );

	t_stringList* pCompBList = &m_DigitalSignature.RsaSystemModulusList();
	if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_256" || m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_521" )
		pCompBList = &m_DigitalSignature.ECDSAPublicKeyCompYList();

	int idxItem = 0; 
	int idxSubItem = 0;

	t_stringListIter iterModulus( pCompBList->begin() );
	while( iterModulus != pCompBList->end() )
	{
		if ( idxSubItem == 0 )
		{
			CString sRow;
			sRow.Format( "%d", idxItem*4 ); // assumes 5 columns per row
			RsaSystemModulusListCtrl.SetItemText( idxItem, idxSubItem++, sRow );
		}
		else
			RsaSystemModulusListCtrl.SetItemText( idxItem, idxSubItem++, (*iterModulus++)->c_str() );

		// set idx for next row
		if ( idxSubItem == 5 )	// assumes 5 columns per row
		{
			idxItem++;
			idxSubItem = 0;
		}
	}

	UpdateData( FALSE );
}

void CRsaModulusDlg::OnBnClickedResetModulusBtn()
{
	m_DigitalSignature.ResetModulus();
	RefreshState();
}


void CRsaModulusDlg::OnBnClickedOk()
{
	UpdateRsaModulusList();
	OnOK();
}

void CRsaModulusDlg::UpdateRsaModulusList()
{
	t_stringList* pCompBList = &m_DigitalSignature.RsaSystemModulusList();
	if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_256" || m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_521" )
		pCompBList = &m_DigitalSignature.ECDSAPublicKeyCompYList();

	int idxItem = 0; 
	int idxSubItem = 1;

	t_stringListIter iterModulus( pCompBList->begin() );
	while( iterModulus != pCompBList->end() )
	{
		*(*iterModulus++) = RsaSystemModulusListCtrl.GetItemText( idxItem, idxSubItem++ );
		
		// set idx for next row
		if ( idxSubItem == 5 )	// assumes 5 columns per row
		{
			idxItem++;
			idxSubItem = 1;
		}
	}
	UpdateData(FALSE);
}

void CRsaModulusDlg::UpdateChangeMarkerInTitle()
{
	if ( m_hWnd == 0 )
		return;

	CString sTitle;
	GetWindowTextA( sTitle );
	// add or remove * as appropriate
	if ( RsaSystemModulusListCtrl.IsChanged() )
	{
		m_DigitalSignature.Changed( true );
		if ( -1 == sTitle.Find('*') )
			sTitle += "*";
	}
	else
	{
		if ( -1 != sTitle.Find('*') )
			sTitle.Remove('*');
	}
	SetWindowTextA(sTitle);
	UpdateWindow();
}

void CRsaModulusDlg::OnLvnItemActivateRsaSystemModulusListCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	UpdateData(FALSE);
}

void CRsaModulusDlg::OnNMKillfocusRsaSystemModulusListCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	UpdateRsaModulusList();
	m_DigitalSignature.Changed(true);
}
#endif