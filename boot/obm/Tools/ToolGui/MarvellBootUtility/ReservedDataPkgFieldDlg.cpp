/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// ReservedDataPkgFieldDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "ReservedDataPkgFieldDlg.h"
#include "TimLib.h"


// CReservedDataPkgFieldDlg dialog

IMPLEMENT_DYNAMIC(CReservedDataPkgFieldDlg, CDialog)

CReservedDataPkgFieldDlg::CReservedDataPkgFieldDlg( bool bInsert,
													CString &sPkgId,
						   						    unsigned int& nSelectedItem,
							                        t_stringVector* pFieldNames, 
												    t_PairList& PkgFields, 
												    CWnd* pParent /*=NULL*/)
: CDialog(CReservedDataPkgFieldDlg::IDD, pParent), 
	m_bInsert( bInsert ), m_sPackageId(sPkgId), 
	m_nSelectedItem( nSelectedItem ),
	m_pFieldNames(pFieldNames), m_PkgFields(PkgFields)
{
	sFieldValue = _T("0x00000000");
	m_bChanged = false;
}

CReservedDataPkgFieldDlg::~CReservedDataPkgFieldDlg()
{
}

void CReservedDataPkgFieldDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_RESERVED_DATA_PACKAGE_PACKAGE_ID_EDT, m_sPackageId);
	DDX_Control(pDX, IDC_RESERVED_DATA_PACKAGE_FIELD_NAME_CB, FieldNameCb);
	DDX_Control(pDX, IDC_RESERVED_DATA_PACKAGE_FIELD_VALUE_EDT, FieldValueEdt);
	DDX_Text(pDX, IDC_RESERVED_DATA_PACKAGE_FIELD_VALUE_EDT, sFieldValue);

	// updates marker in title
	UpdateChangeMarkerInTitle();
	theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CReservedDataPkgFieldDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_RESERVED_DATA_PACKAGE_FIELD_NAME_CB, &CReservedDataPkgFieldDlg::OnCbnSelchangeReservedDataPackageFieldNameCb)
	ON_EN_CHANGE(IDC_RESERVED_DATA_PACKAGE_FIELD_VALUE_EDT, &CReservedDataPkgFieldDlg::OnEnChangeReservedDataPackageFieldValueEdt)
END_MESSAGE_MAP()


// CReservedDataPkgFieldDlg message handlers

BOOL CReservedDataPkgFieldDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	int nItem = 0;
	int idx = 0;

	if ( m_pFieldNames )
	{
		t_stringVector& FieldNames =  *m_pFieldNames;
		if ( m_nSelectedItem == LB_ERR )
			m_nSelectedItem = 0;
		else if ( m_nSelectedItem > m_PkgFields.size() )
			m_nSelectedItem = m_PkgFields.size() > 0 ? (int)m_PkgFields.size()-1 : 0;

		if ( m_bInsert )	// inserting
		{
			// inserting a field so populate list with all fields for user selection
			int iField = 0;
			if ( m_sPackageId != "DATA" )
			{
				for ( iField = 0; iField < (int)FieldNames.size(); iField++  )
				{
					// for now, appears CONSUMER_ID is not really supported
					// by bootrom or bootloader so eliminate the field
					// even though it is in processor_enums.
					if ( m_sPackageId == "OpMode" || m_sPackageId == "OpDiv" )
					{
						if ( iField == 0 && (FieldNames[iField]->find("CONSUMER_ID") != string::npos) )
							continue;
					}

					FieldNameCb.InsertString( nItem, FieldNames[iField]->c_str() );
					FieldNameCb.SetItemData( nItem, iField );
					nItem++;
				}
			}
			else if ( m_sPackageId == "DATA" )
			{
				stringstream ss;
				ss << "DATA[" << FieldNames.size() << "]";
				FieldNameCb.InsertString( nItem, ss.str().c_str() );
				FieldNameCb.SetItemData( nItem, (int)FieldNames.size() );
			}
		}

		t_PairListIter iter = m_PkgFields.begin();
		while ( iter != m_PkgFields.end() )
		{
			if ( idx == m_nSelectedItem )
			{
				if ( !m_bInsert )	// editing
				{
					// for now, appears CONSUMER_ID is not really supported
					// by bootrom or bootloader so eliminate the field
					// even though it is in processor_enums.
					if ( m_sPackageId == "OpMode" || m_sPackageId == "OpDiv" )
					{
						if ( (*iter)->first == 0 && (FieldNames[(*iter)->first]->find("CONSUMER_ID") != string::npos) )
							continue;
					}

					// editing a field so only populate list with the correct field
					FieldNameCb.InsertString( nItem, FieldNames[(*iter)->first]->c_str() );
					FieldNameCb.SetItemData( nItem, idx );
					break;
				}
			}
			iter++;
			idx++;
		}
	}

	// select the first field
	int Sel = FieldNameCb.SetCurSel(0);

	FieldValueEdt.m_bHexOnly = true;
	
	RefreshState();
	m_bChanged = false;
	UpdateData( FALSE );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CReservedDataPkgFieldDlg::RefreshState()
{
	CTimLib TimLib;
	int iSel;

	FieldNameCb.EnableWindow( FieldNameCb.GetCount() );
	FieldValueEdt.EnableWindow(	CB_ERR != (iSel = FieldNameCb.GetCurSel()) );
	if ( iSel != CB_ERR )
	{
		if ( !m_bInsert )
		{
			unsigned int iFieldIdx = (unsigned int)FieldNameCb.GetItemData( iSel );

			t_PairListIter iter = m_PkgFields.begin();
			bool bFound = false;
			unsigned int idx = 0;
			while ( !bFound && (iter != m_PkgFields.end()) )
			{
				if ( idx  == iFieldIdx )
				{
					bFound = true;
					break;
				}
				iter++;
				idx++;
			}

			if ( bFound )
				sFieldValue = TimLib.HexFormattedAscii((*iter)->second).c_str();
		}
	}

	UpdateData( FALSE );
}

void CReservedDataPkgFieldDlg::OnCbnSelchangeReservedDataPackageFieldNameCb()
{
	if ( FieldNameCb.GetCurSel() != CB_ERR )
		sFieldValue = "0x00000000";

	m_bChanged = true;
	RefreshState();
}

void CReservedDataPkgFieldDlg::OnEnChangeReservedDataPackageFieldValueEdt()
{
	m_bChanged = true;
	UpdateData(TRUE);
}

void CReservedDataPkgFieldDlg::OnOK()
{
	UpdateData( TRUE );
	CTimLib TimLib;
	int nIdx = CB_ERR;
	CString sFieldName;
	if ( ( nIdx = FieldNameCb.GetCurSel()) != CB_ERR )
	{
		// update modified field
		unsigned int iField = (unsigned int)FieldNameCb.GetItemData( nIdx );
		unsigned int iFieldValue = TimLib.Translate(sFieldValue);
		FieldNameCb.GetLBText( nIdx, sFieldName );

		bool bFound = false;
		if ( !m_bInsert ) 
		{
			t_PairListIter iter = m_PkgFields.begin();
			unsigned int idx = 0;
			while( !bFound && iter != m_PkgFields.end() )
			{
				if ( idx == iField )
				{
					(*iter)->second = iFieldValue;
					bFound = true;
					break;
				}
				iter++;
				idx++;
			}
		}
		else if ( m_bInsert ) 		// or add new field at insert point or at end
		{
			if ( m_pFieldNames )
			{
				t_stringVector& FieldNames =  *m_pFieldNames;
				int iField = 0;
				CStringA sFieldName;
				FieldNameCb.GetLBText( nIdx, sFieldName );
				for ( iField = 0; iField < (int)FieldNames.size(); iField++  )
				{
					if ( *FieldNames[ iField ] == string( LPCTSTR(sFieldName) ) )
					{
						bFound = true;
						break;
					}
				}

				if ( bFound || m_sPackageId == "DATA" )
				{
					// insert after selected field
					t_PairListIter iter = m_PkgFields.begin();
					unsigned int idx = 0;
					while( iter != m_PkgFields.end() )
					{
						if ( idx == m_nSelectedItem+1 )
							break;
						iter++;
						idx++;
					}

					// permit only one field in OpMode and OpDiv but allow
					// user to replace the existing field with a new one
					if ( m_PkgFields.size() > 0 && (m_sPackageId == "OpDiv" || m_sPackageId == "OpMode") )
					{
						delete *m_PkgFields.begin();
						m_PkgFields.clear();
					}

					m_PkgFields.insert( iter, new pair<unsigned int, unsigned int>(iField, iFieldValue) );
				}
			}
		}
	}

	CDialog::OnOK();
}

void CReservedDataPkgFieldDlg::UpdateChangeMarkerInTitle()
{
	if ( m_hWnd == 0 )
		return;

	CString sTitle;
	GetWindowTextA( sTitle );
	// add or remove * as appropriate
	if ( m_bChanged )
	{
		if ( -1 == sTitle.Find('*') )
			sTitle += "*";
	}
	else
	{
		if ( -1 != sTitle.Find('*') )
			sTitle.Remove('*');
	}
	SetWindowTextA(sTitle);
	UpdateWindow();
}
