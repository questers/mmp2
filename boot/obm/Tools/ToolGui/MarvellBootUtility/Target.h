/******************************************************************************
 *
 *  (C)Copyright 2005 - 2011 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include <windows.h>
#include <string>
#include <list>
#include <iterator>
using namespace std;

// bootrom vid-pid
const string MH_L_BOOTROM_VID_PID("#Vid_8086&Pid_c001");
const string TAVOR_BOOTROM_VID_PID("#Vid_8086&Pid_d001");
const string MH_P_BOOTROM_VID_PID("#Vid_8086&Pid_e001");
// loader vid-pid
const string MH_P_DKB_VID_PID("#Vid_1286&Pid_8100"); // PXA320
const string MH_L_DKB_VID_PID("#Vid_1286&Pid_8101"); // PXA300
const string MH_LV_DKB_VID_PID("#Vid_1286&Pid_8102"); // PXA310
 const string TAVOR_P_DKB_VID_PID("#Vid_1286&Pid_8103"); // Tavor-P/
const string TAVOR_PV_DKB_VID_PID("#Vid_1286&Pid_8104"); // Tavor-PV/PV2 or PXA940 and PXA950
const string TAVOR_L_DKB_VID_PID("#Vid_1286&Pid_8105"); // Tavor-L
// single device vid-pid
const string PXA168_VID_PID("#Vid_1286&Pid_8106"); // Aspen
const string PXA920_VID_PID("#Vid_1286&Pid_8111"); // Tavor TD
const string ARMADA_610_VID_PID("#Vid_1286&Pid_8112");  // PXA688/MMP2
const string P_88AP001_VID_PID("#Vid_1286&Pid_8113"); // MORONA
const string MG1_VID_PID("#Vid_1286&Pid_8114"); // 88CP955
const string TAVOR_PV2_MG2_VID_PID("#Vid_1286&Pid_8115"); // 88CP958
const string ESHEL_VID_PID("#Vid_1286&Pid_8116"); 
const string PXA978_VID_PID("#Vid_1286&Pid_8117"); // Nevo
const string ARMADA_620_VID_PID("#Vid_1286&Pid_8118"); // MMP3
const string ESHEL_LTE_VID_PID("#Vid_1286&Pid_8128"); 
const string ARMADA_622_VID_PID("#Vid_1286&Pid_8129"); 
const string WUKONG_VID_PID("#Vid_1286&Pid_812a"); 

class CDownloaderInterface;

class CTarget
{
public:
	enum eTargetState { TARGET_IDLE, TARGET_DOWNLOADING, TARGET_TERMINATED, TARGET_ERROR };

public:
	CTarget();
	~CTarget(void);

	bool StartDownload();
	bool TerminateDownload();

	string& InterfaceName(){ return m_sInterfaceName; }
	void InterfaceName( string sName ){ m_sInterfaceName = sName; }

	eTargetState TargetState(){ return m_TargetState; }
	void TargetState( eTargetState iTargetState ){ m_TargetState = iTargetState; }

	bool ReadFromPipe();

	CDocument* Document() { return m_pDoc; }
	void CloseDocument() { if ( m_pDoc ) m_pDoc = 0; }

	PROCESS_INFORMATION DownloaderProcessInfo() { return m_ProcessInformation; }
	void DisplayMsg(string& sMsg);

	void Connected( bool bCon ) { m_bConnected = bCon; }
	bool Connected() { return m_bConnected; }

	void DownloaderInterface( CDownloaderInterface* pDownloaderInterface )
		{ m_pDownloaderInterface = pDownloaderInterface; }

	CDownloaderInterface* DownloaderInterface()
		{ return m_pDownloaderInterface; }

private:
	string					m_sInterfaceName;
	bool					m_bConnected;
	eTargetState			m_TargetState;
	STARTUPINFO				m_StartupInfo;
	PROCESS_INFORMATION		m_ProcessInformation;  // wtptp process
	CDocument*				m_pDoc;
	CDownloaderInterface*	m_pDownloaderInterface;

	HANDLE m_hChildStdinRd;
	HANDLE m_hChildStdinWr;
	HANDLE m_hChildStdoutRd;
	HANDLE m_hChildStdoutWr;
	HANDLE m_DownloadThreadHandle;

	bool Validate();
	bool CreateDownloaderProcess( string& sFullPath, string& sCommandLine );
	int CreateMessagePipes();
	VOID WriteToPipe( string& sMsg );
};

DWORD DownloadThread( LPVOID lpParameter );
typedef list<CTarget*>			 t_TargetsList;
typedef list<CTarget*>::iterator t_TargetsIter;


