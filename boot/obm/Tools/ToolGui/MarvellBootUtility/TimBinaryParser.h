/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "TimLib.h"
#include "TimDescriptor.h"

#pragma once

class CTimBinaryParser : public CTimLib
{
public:
    CTimBinaryParser(void);
    virtual ~CTimBinaryParser(void);

    bool ParseFile( string sFilePath );

    CTimDescriptor TimDescriptor;

private:
    bool ParseTimHeader( unsigned int& iValuePos );
    void ParseImages( unsigned int& iValuePos );
    bool ParseReservedData( unsigned int& iValuePos );
    void SkipAllOnesPadding( unsigned int& iValuePos );

#if TRUSTED
    void ParseKeys( unsigned int& iValuePos );
    bool ParseDigitalSignature( unsigned int& iValuePos );
#endif

    void ParseERDFields( unsigned int uiLoop, unsigned int& iValuePos, CExtendedReservedData& Ext,
                         void (CExtendedReservedData::*AddField)( std::pair< unsigned int, unsigned int >*& ) );
    
    void ParseERDBaseFields( CErdBase* pERD, unsigned int uiLoop, unsigned int& iValuePos );

private:
    char* FileData;;

};
