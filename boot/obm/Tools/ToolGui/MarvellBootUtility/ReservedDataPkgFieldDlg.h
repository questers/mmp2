/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include <string>
#include <list>
//#include "afxwin.h"
using namespace std;

#include "EditListCtrl.h"

// CReservedDataPkgFieldDlg dialog

class CReservedDataPkgFieldDlg : public CDialog
{
	DECLARE_DYNAMIC(CReservedDataPkgFieldDlg)

public:
	CReservedDataPkgFieldDlg( bool bInsert, 
							  CString& sPkgId, 
							  unsigned int& nSelectedItem,
							  t_stringVector* pFieldNames, 
							  t_PairList& PkgFields, 
							  CWnd* pParent = NULL);   // standard constructor

	virtual ~CReservedDataPkgFieldDlg();

// Dialog Data
	enum { IDD = IDD_RESERVED_DATA_PKG_FIELD_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	CComboBox FieldNameCb;
	CHexItemEdit FieldValueEdt;
	CString sFieldValue;

private:
	
	void RefreshState();

	CString				m_sPackageId;
	unsigned int&		m_nSelectedItem;
	t_stringVector*		m_pFieldNames;
	t_PairList&			m_PkgFields;
	bool				m_bInsert;
	bool				m_bChanged;

public:
	afx_msg void OnCbnSelchangeReservedDataPackageFieldNameCb();
	afx_msg void OnEnChangeReservedDataPackageFieldValueEdt();

public:
	void UpdateChangeMarkerInTitle();

protected:
	virtual void OnOK();

};
