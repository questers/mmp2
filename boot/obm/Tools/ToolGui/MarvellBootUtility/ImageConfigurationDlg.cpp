/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// ImageConfigurationDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "ImageConfigurationDlg.h"
#include "CommentDlg.h"

// CImageConfigurationDlg dialog

IMPLEMENT_DYNAMIC(CImageConfigurationDlg, CDialog)

CImageConfigurationDlg::CImageConfigurationDlg( CDownloaderInterface& rDownloaderInterface, CImageDescription& OrigImage, CWnd* pParent /*=NULL*/)
	: CDialog(CImageConfigurationDlg::IDD, pParent)
	, sImageId(_T("")), m_Image( OrigImage ), DownloaderInterface(rDownloaderInterface)
{
	// the caller is responsible to provide the image
	sImageFilePath = _T("0x0");
	sImageIdTag = _T("0x0");
	sFlashEntryAddress = _T("0x0");
	sLoadAddress = _T("0x0");
	sImageSizeToHash = _T("0x0");
//	uiImageSizeToHash = 0;
	uiPartitionNumber = 0;
}

CImageConfigurationDlg::~CImageConfigurationDlg()
{
}

void CImageConfigurationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_IMAGE_FILE_PATH_EDT, sImageFilePath);
	DDX_Text(pDX, IDC_IMAGE_ID_TAG, sImageIdTag);
	DDX_Text(pDX, IDC_IMAGE_FLASH_ENTRY_EDT, sFlashEntryAddress);
	DDX_Text(pDX, IDC_IMAGE_LOAD_ADDRESS_EDT, sLoadAddress);
	DDX_Control(pDX, IDC_IMAGE_ID_CB, ImageIdCb);
	DDX_Control(pDX, IDC_IMAGE_ID_TAG, ImageIdTagEdt);
	DDX_Control(pDX, IDC_IMAGE_HASH_ALGORITHM_ID_CB, HashAlgorithmIdCb);
	DDX_Control(pDX, IDC_IMAGE_SIZE_TO_HASH_EDT, ImageSizeToHashEdt);
	DDX_Text(pDX, IDC_IMAGE_SIZE_TO_HASH_EDT, sImageSizeToHash);
	DDX_Control(pDX, IDC_IMAGE_FLASH_ENTRY_EDT, ImageFlashEntryEdt);
	DDX_Control(pDX, IDC_IMAGE_LOAD_ADDRESS_EDT, ImageLoadAddressEdt);
	DDX_Control(pDX, IDC_PARTITION_NUMBER_EDT, PartitionNumberEdt);
	DDX_Text(pDX, IDC_PARTITION_NUMBER_EDT, uiPartitionNumber);
	DDX_CBString(pDX, IDC_IMAGE_ID_CB, sImageId);
	DDV_MaxChars(pDX, sImageId, 4);
	DDX_Control(pDX, IDC_IMAGE_FILE_PATH_EDT, ImageFilePathEdt);

	// updates marker in title
	UpdateChangeMarkerInTitle();
	theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CImageConfigurationDlg, CDialog)
	ON_WM_CONTEXTMENU()
	ON_EN_CHANGE(IDC_IMAGE_FILE_PATH_EDT, &CImageConfigurationDlg::OnEnChangeImageFilePathEdt)
	ON_BN_CLICKED(IDC_IMAGE_FILE_PATH_BROWSE_BTN, &CImageConfigurationDlg::OnBnClickedImageFilePathBrowseBtn)
	ON_CBN_SELCHANGE(IDC_IMAGE_ID_CB, &CImageConfigurationDlg::OnCbnSelchangeImageIdCb)
	ON_EN_CHANGE(IDC_IMAGE_ID_TAG, &CImageConfigurationDlg::OnEnChangeImageIdTag)
	ON_EN_CHANGE(IDC_IMAGE_FLASH_ENTRY_EDT, &CImageConfigurationDlg::OnEnChangeImageFlashEntryEdt)
	ON_EN_CHANGE(IDC_IMAGE_LOAD_ADDRESS_EDT, &CImageConfigurationDlg::OnEnChangeImageLoadAddressEdt)
	ON_BN_CLICKED(IDOK, &CImageConfigurationDlg::OnBnClickedOk)
	ON_CBN_SELCHANGE(IDC_IMAGE_HASH_ALGORITHM_ID_CB, &CImageConfigurationDlg::OnCbnSelchangeImageHashAlgorithmIdCb)
	ON_EN_CHANGE(IDC_IMAGE_SIZE_TO_HASH_EDT, &CImageConfigurationDlg::OnEnChangeImageSizeToHashEdt)
	ON_CBN_EDITCHANGE(IDC_IMAGE_ID_CB, &CImageConfigurationDlg::OnCbnEditchangeImageIdCb)
	ON_EN_CHANGE(IDC_PARTITION_NUMBER_EDT, &CImageConfigurationDlg::OnEnChangePartitionNumberEdt)
END_MESSAGE_MAP()


// CImageConfigurationDlg message handlers

void CImageConfigurationDlg::OnEnChangeImageFilePathEdt()
{
	UpdateData( TRUE );
	m_Image.ImageFilePath( string((CStringA)sImageFilePath) );
}

void CImageConfigurationDlg::OnBnClickedImageFilePathBrowseBtn()
{
	UpdateData( TRUE );
	CFileDialog Dlg( TRUE, _T("*.bin"), NULL, 
					 //OFN_FILEMUSTEXIST | 
					 OFN_PATHMUSTEXIST,
					 _T("Binary (*.bin)|*.bin|All Files (*.*)|*.*||") );
	if ( Dlg.DoModal() == IDOK )
	{
		sImageFilePath = Dlg.GetPathName();
		m_Image.ImageFilePath( string((CStringA)sImageFilePath) );
	}
	RefreshState();
	SetFocus();
}

void CImageConfigurationDlg::OnCbnSelchangeImageIdCb()
{
	UpdateData(TRUE);
	int idx = ImageIdCb.GetCurSel();
	ImageIdCb.GetLBText( idx, sImageId );
	m_Image.ImageId( string((CStringA)sImageId) );
	sImageIdTag = m_Image.ImageIdTag().c_str();
	UpdateData(FALSE);
}

void CImageConfigurationDlg::OnCbnEditchangeImageIdCb()
{
	UpdateData( TRUE );
	ImageIdCb.GetWindowTextA( sImageId );
	if ( sImageId.GetLength() > 4 )
	{
		sImageId = sImageId.Left(4);
		ImageIdCb.SetWindowTextA( sImageId );
	}
	m_Image.ImageId( string((CStringA)sImageId) );
	sImageIdTag = m_Image.ImageIdTag().c_str();
	ImageIdTagEdt.SetWindowTextA( sImageIdTag );
}

void CImageConfigurationDlg::OnEnChangeImageIdTag()
{
	UpdateData( TRUE );
	m_Image.ImageIdTag( string((CStringA)sImageIdTag) );
}

void CImageConfigurationDlg::OnEnChangeImageFlashEntryEdt()
{
	UpdateData( TRUE );
	m_Image.FlashEntryAddress( string((CStringA)sFlashEntryAddress ) );
}

void CImageConfigurationDlg::OnEnChangeImageLoadAddressEdt()
{
	UpdateData( TRUE );
	m_Image.LoadAddress( string((CStringA)sLoadAddress ) );
}

void CImageConfigurationDlg::OnEnChangeImageSizeToHashEdt()
{
	UpdateData( TRUE );
	m_Image.ImageSizeToHash( m_Image.Translate((CStringA)sImageSizeToHash) );
}


void CImageConfigurationDlg::OnEnChangePartitionNumberEdt()
{
	UpdateData( TRUE );
	m_Image.PartitionNumber( uiPartitionNumber );
}

BOOL CImageConfigurationDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	bool bTimHFound = false;
	bool bTimDFound = false;
	t_ImagesList& ImagesList = DownloaderInterface.TimDescriptorParser.TimDescriptor().ImagesList();
	t_ImagesIter iter = ImagesList.begin();
	while ( iter != ImagesList.end() )
	{
		if ( (*iter)->ImageId().compare(0,4,"TIMH") == 0 )
			bTimHFound = true;
		if ( (*iter)->ImageId().compare(0,4,"TIMD") == 0 )
			bTimDFound = true;
		iter++;
	}

	int idx = 0;
	ImageIdCb.InsertString(idx++,"");
	if ( !bTimHFound )
		ImageIdCb.InsertString(idx++,"TIMH");
	if ( !bTimDFound )
		ImageIdCb.InsertString(idx++,"TIMD");
	ImageIdCb.InsertString(idx++,"DKBI"); 
	ImageIdCb.InsertString(idx++,"MONI");
	ImageIdCb.InsertString(idx++,"OBMI");
	ImageIdCb.InsertString(idx++,"OSLO");
	ImageIdCb.InsertString(idx++,"TBRI");
	ImageIdCb.InsertString(idx++,"TSZI");
	ImageIdCb.InsertString(idx++,"WTMI");

	ImageIdCb.SetCurSel(0);

	HashAlgorithmIdCb.InsertString(0, "SHA-160");
	HashAlgorithmIdCb.InsertString(1, "SHA-256");
	if ( DownloaderInterface.TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version >= TIM_3_4_00 )
		HashAlgorithmIdCb.InsertString(2, "SHA-512");
	HashAlgorithmIdCb.SetCurSel(0);

	ImageIdTagEdt.m_bHexOnly = true;
	ImageFlashEntryEdt.m_bHexOnly = true;
	ImageLoadAddressEdt.m_bHexOnly = true;
	ImageSizeToHashEdt.m_bHexOnly = true;

	RefreshState();
	UpdateControls();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CImageConfigurationDlg::RefreshState()
{
	sImageFilePath = m_Image.ImageFilePath().c_str();
	sImageIdTag = m_Image.ImageIdTag().c_str();
	sFlashEntryAddress = m_Image.FlashEntryAddress().c_str();
	sLoadAddress = m_Image.LoadAddress().c_str();
	CTimLib TimLib;
	sImageSizeToHash = TimLib.HexFormattedAscii(m_Image.ImageSizeToHash()).c_str();	
//	uiImageSizeToHash = m_Image.ImageSizeToHash();	
	uiPartitionNumber = m_Image.PartitionNumber();
	
	sImageId = m_Image.ImageId().c_str();
	int idx = ImageIdCb.FindStringExact(0, sImageId);
	if ( idx != LB_ERR )
		ImageIdCb.SetCurSel( idx );
	else
		ImageIdCb.SetWindowTextA( sImageId );

	HashAlgorithmIdCb.SetCurSel(HashAlgorithmIdCb.FindStringExact(0, m_Image.HashAlgorithmId().c_str()));
	UpdateData( FALSE );
}

void CImageConfigurationDlg::UpdateControls()
{
	BOOL bTrusted = DownloaderInterface.TimDescriptorParser.TimDescriptor().Trusted() ? TRUE : FALSE;
	HashAlgorithmIdCb.EnableWindow( bTrusted );
	ImageSizeToHashEdt.EnableWindow( bTrusted );

	PartitionNumberEdt.EnableWindow( DownloaderInterface.TimDescriptorParser.TimDescriptor().Version() >= "0x00030200" );
	if ( DownloaderInterface.TimDescriptorParser.TimDescriptor().Version() < "0x00030200" )
		PartitionNumberEdt.SetWindowTextA("0");
}


void CImageConfigurationDlg::OnBnClickedOk()
{
	if ( sImageFilePath.GetLength() == 0 )
	{
		AfxMessageBox( "Entry a valid Image File path" );
		return;
	}
	if ( m_Image.ImageId().length() == 0 )
	{
		AfxMessageBox( "Select an Image ID or enter a custom Image ID" );
		return;
	}

	OnOK();
}

void CImageConfigurationDlg::OnCbnSelchangeImageHashAlgorithmIdCb()
{
	UpdateData( TRUE );
	CString sText;
	HashAlgorithmIdCb.GetLBText( HashAlgorithmIdCb.GetCurSel(), sText );
	m_Image.HashAlgorithmId( string((CStringA)sText) );
	RefreshState();
}

void CImageConfigurationDlg::UpdateChangeMarkerInTitle()
{
	if ( m_hWnd == 0 )
		return;

	CString sTitle;
	GetWindowTextA( sTitle );
	// add or remove * as appropriate
	if ( m_Image.IsChanged() )
	{
		if ( -1 == sTitle.Find('*') )
			sTitle += "*";
	}
	else
	{
		if ( -1 != sTitle.Find('*') )
			sTitle.Remove('*');
	}
	SetWindowTextA(sTitle);
	UpdateWindow();
}

void CImageConfigurationDlg::OnContextMenu(CWnd* pWnd, CPoint /*point*/)
{
	int iCtlId = pWnd->GetDlgCtrlID();
	CCommentDlg Dlg(this);
	switch ( iCtlId )
	{
		case IDC_IMAGE_FILE_PATH_EDT:
			Dlg.Display( &m_Image.ImageFilePath() );
			break;

		case IDC_IMAGE_ID_CB:
			Dlg.Display( &m_Image.ImageIdTag() );
			break;
	
		case IDC_IMAGE_ID_TAG:
			Dlg.Display( &m_Image.ImageIdTag() );
			break;
	
		case IDC_IMAGE_FLASH_ENTRY_EDT:
			Dlg.Display( &m_Image.FlashEntryAddress() );
			break;
	
		case IDC_IMAGE_LOAD_ADDRESS_EDT:
			Dlg.Display( &m_Image.LoadAddress() );
			break;

		case IDC_IMAGE_HASH_ALGORITHM_ID_CB:
			Dlg.Display( &m_Image.HashAlgorithmId() );
			break;
	
		case IDC_IMAGE_SIZE_TO_HASH_EDT:
			Dlg.Display( &m_Image.ImageSizeToHash() );
			break;

		case IDC_PARTITION_NUMBER_EDT:
			Dlg.Display( &m_Image.PartitionNumber() );
			break;

		default:
			break;
	}
}
