/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// KeyDlg.cpp : implementation file
//

#include "stdafx.h"

#if TRUSTED

#include "MarvellBootUtility.h"
#include "KeyDlg.h"
#include "CommentDlg.h"

#include "PublicKeyExponentDlg.h"
#include "RsaModulusDlg.h"

// CKeyDlg dialog

IMPLEMENT_DYNAMIC(CKeyDlg, CDialog)

CKeyDlg::CKeyDlg(CTimDescriptor& TimDescriptor, CWnd* pParent /*=NULL*/)
    : CDialog(CKeyDlg::IDD, pParent),
    m_TimDescriptor( TimDescriptor )
{

}

CKeyDlg::~CKeyDlg()
{
}

void CKeyDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_KEYID_CB, KeyIdCb);
    DDX_Control(pDX, IDC_HASH_ALGORITHM_ID_CB, HashAlgorithmIdCb);
    DDX_Control(pDX, IDC_KEY_SIZE_CB, KeySizeCb);
    DDX_Text(pDX, IDC_KEY_TAG_EDT, sKeyTag);
    DDX_Control(pDX, IDC_KEY_TAG_EDT, KeyTagEdt);
    DDX_Control(pDX, IDC_ENCRYPT_ALGORITHM_ID_CB, EncryptAlgorithmIdCb);
    DDX_Control(pDX, IDC_PUBLIC_KEY_EXPONENT_BTN, PubKeyCompXBtn);
    DDX_Control(pDX, IDC_RSA_MODULUS_BTN, RsaModulusCompYBtn);
    
    // updates marker in title
    UpdateChangeMarkerInTitle();
    theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}

BEGIN_MESSAGE_MAP(CKeyDlg, CDialog)
    ON_CBN_SELCHANGE(IDC_KEY_SIZE_CB, &CKeyDlg::OnCbnSelchangeKeySizeCb)
    ON_CBN_SELCHANGE(IDC_HASH_ALGORITHM_ID_CB, &CKeyDlg::OnCbnSelchangeHashAlgorithmIdCb)
    ON_CBN_SELCHANGE(IDC_KEYID_CB, &CKeyDlg::OnCbnSelchangeKeyidCb)
    ON_EN_CHANGE(IDC_KEY_TAG_EDT, &CKeyDlg::OnEnChangeKeyTagEdt)
    ON_CBN_EDITCHANGE(IDC_KEYID_CB, &CKeyDlg::OnCbnEditchangeKeyidCb)
    ON_BN_CLICKED(IDC_RSA_MODULUS_BTN, &CKeyDlg::OnBnClickedRsaModulusBtn)
    ON_BN_CLICKED(IDC_PUBLIC_KEY_EXPONENT_BTN, &CKeyDlg::OnBnClickedPublicKeyExponentBtn)
    ON_WM_CONTEXTMENU()
    ON_CBN_SELCHANGE(IDC_ENCRYPT_ALGORITHM_ID_CB, &CKeyDlg::OnCbnSelchangeEncryptAlgorithmIdCb)
    ON_BN_CLICKED(IDC_KEY_INSERT_SAMPLE_KEY_BTN, &CKeyDlg::OnBnClickedKeyInsertSampleKeyBtn)
END_MESSAGE_MAP()

// CKeyDlg message handlers

BOOL CKeyDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    HashAlgorithmIdCb.InsertString(0, "SHA-160" );
    HashAlgorithmIdCb.InsertString(1, "SHA-256" );
    if ( m_TimDescriptor.TimHeader().VersionBind.Version >= TIM_3_4_00 )
        HashAlgorithmIdCb.InsertString(2, "SHA-512" );
    HashAlgorithmIdCb.SetCurSel(0);

    EncryptAlgorithmIdCb.InsertString(0, "Marvell" );
    EncryptAlgorithmIdCb.InsertString(1, "PKCS1_v1_5" );
    EncryptAlgorithmIdCb.InsertString(2, "PKCS1_v2_1" );
    EncryptAlgorithmIdCb.InsertString(3, "PKCS1_v1_5 (Ippcp)" );
    EncryptAlgorithmIdCb.InsertString(4, "PKCS1_v2_1 (Ippcp)" );
    if ( m_TimDescriptor.TimHeader().VersionBind.Version >= TIM_3_4_00 )
    {
        EncryptAlgorithmIdCb.InsertString(5, "ECDSA_256" );
        EncryptAlgorithmIdCb.InsertString(6, "ECDSA_521" );
    }
    EncryptAlgorithmIdCb.SetCurSel(0);

    KeyIdCb.SetCurSel(0);
    KeySizeCb.SetCurSel(0);

    KeyTagEdt.m_bHexOnly = true;
    RefreshState();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}


void CKeyDlg::RefreshState()
{
    UpdateData( TRUE );

    if ( LB_ERR != KeyIdCb.FindStringExact(0, m_Key.KeyId().c_str() ) )
    {
        // predefined key id selected
        KeyIdCb.SetCurSel(KeyIdCb.FindStringExact(0, m_Key.KeyId().c_str()));
        KeyTagEdt.EnableWindow( FALSE );
    }
    else
    {
        // custom user defined key id
        KeyIdCb.SetWindowTextA( m_Key.KeyId().c_str() );
        KeyTagEdt.EnableWindow( TRUE );
    }

    HashAlgorithmIdCb.SetCurSel(HashAlgorithmIdCb.FindStringExact(0, m_Key.HashAlgorithmId().c_str()));

    sKeyTag = m_Key.KeyTag().c_str();

    CString sEncryptAlgorithm = m_Key.EncryptAlgorithmId().c_str();
    if ( LB_ERR != EncryptAlgorithmIdCb.FindStringExact(0, sEncryptAlgorithm ) )
    {
        // predefined key id selected
        EncryptAlgorithmIdCb.SetCurSel(EncryptAlgorithmIdCb.FindStringExact(0, sEncryptAlgorithm));
    }
    else
    {
        // custom user defined key id
        EncryptAlgorithmIdCb.SetWindowTextA( sEncryptAlgorithm );
    }

    KeySizeCb.ResetContent();
    if ( sEncryptAlgorithm == "ECDSA_256" )
    {
        KeySizeCb.AddString("256");
        m_Key.KeySize(256);
        m_Key.PublicKeySize(256);
        PubKeyCompXBtn.SetWindowText("ECC Public Key Comp X...");
        RsaModulusCompYBtn.SetWindowText("ECC Public Key Comp Y...");
    }
    else if ( sEncryptAlgorithm == "ECDSA_521" )
    {
        KeySizeCb.AddString("521");
        m_Key.KeySize(521);
        m_Key.PublicKeySize(521);
        PubKeyCompXBtn.SetWindowText("ECC Public Key Comp X...");
        RsaModulusCompYBtn.SetWindowText("ECC Public Key Comp Y...");
    }
    else
    {
        KeySizeCb.AddString("1024");
        KeySizeCb.AddString("2048");
        m_Key.KeySize(1024);
        m_Key.PublicKeySize(1024);
        PubKeyCompXBtn.SetWindowText("Public Key Exponent...");
        RsaModulusCompYBtn.SetWindowText("RSA Modulus...");
    }

    stringstream ss;
    ss << m_Key.KeySize();
    KeySizeCb.SetCurSel(KeySizeCb.FindStringExact(0, ss.str().c_str()));

    UpdateData( FALSE );
}

void CKeyDlg::OnCbnSelchangeKeySizeCb()
{
    UpdateData( TRUE );
    CString sText;
    KeySizeCb.GetLBText( KeySizeCb.GetCurSel(), sText );
    m_Key.PublicKeySize( m_Key.Translate(sText) );
    m_Key.KeySize( m_Key.Translate(sText) );
    RefreshState();
}

void CKeyDlg::OnCbnSelchangeHashAlgorithmIdCb()
{
    UpdateData( TRUE );
    CString sText;
    HashAlgorithmIdCb.GetLBText( HashAlgorithmIdCb.GetCurSel(), sText );
    m_Key.HashAlgorithmId( string((CStringA)sText) );
}

void CKeyDlg::OnCbnSelchangeKeyidCb()
{
    UpdateData( TRUE );
    CString sText;
    int idx = KeyIdCb.GetCurSel();
    KeyIdCb.GetLBText(idx, sText );
    m_Key.KeyId( string((CStringA)sText) );
    
    if ( LB_ERR != idx )
    {
        if ( idx == 0 )
            sKeyTag = "0x4A544147";	// JTAG
        else if ( idx == 1 )
            sKeyTag = "0x50415442"; // PATC
        else if ( idx == 2 )
            sKeyTag = "0x5443414B"; // TCAK
        else
        {
            KeyTagEdt.EnableWindow( TRUE );
            sKeyTag = "0x00000000";
        }
        m_Key.KeyTag( string((CStringA)sKeyTag) );
        KeyTagEdt.SetWindowTextA( sKeyTag );
        KeyTagEdt.EnableWindow( FALSE );
    }
}

void CKeyDlg::OnEnChangeKeyTagEdt()
{
    UpdateData( TRUE );
    m_Key.KeyTag( string((CStringA)sKeyTag) );
}

void CKeyDlg::OnCbnEditchangeKeyidCb()
{
    UpdateData( TRUE );
    CString sText;
    KeyIdCb.GetWindowText( sText );
    m_Key.KeyId( string((CStringA)sText) );
    
    sKeyTag = "0x00000000";
    m_Key.KeyTag( string((CStringA)sKeyTag) );
    KeyTagEdt.SetWindowTextA( sKeyTag );
    KeyTagEdt.EnableWindow( TRUE );
}


void CKeyDlg::OnBnClickedRsaModulusBtn()
{
    CRsaModulusDlg Dlg;
    Dlg.Key( m_Key );

    if ( m_Key.EncryptAlgorithmId() == "ECDSA_256" || m_Key.EncryptAlgorithmId() == "ECDSA_521" )
        Dlg.Title("ECC Public Key Comp Y");
    else
        Dlg.Title("RSA Modulus");
    if ( Dlg.DoModal() == IDOK )
    {
        if ( Dlg.Key().IsChanged() )
        {
            m_Key = Dlg.Key();
        }
    }
    UpdateData(FALSE);
    SetFocus();
}

void CKeyDlg::OnBnClickedPublicKeyExponentBtn()
{
    CPublicKeyExponentDlg Dlg;
    Dlg.Key( m_Key );
    if ( m_Key.EncryptAlgorithmId() == "ECDSA_256" || m_Key.EncryptAlgorithmId() == "ECDSA_521" )
        Dlg.Title("ECC Public Key Comp X");
    else
        Dlg.Title("Public Key Exponent");

    if ( Dlg.DoModal() == IDOK )
    {
        if ( Dlg.Key().IsChanged() )
        {
            m_Key = Dlg.Key();
        }
    }
    UpdateData(FALSE);
    SetFocus();
}

void CKeyDlg::UpdateChangeMarkerInTitle()
{
    if ( m_hWnd == 0 )
        return;

    CString sTitle;
    GetWindowTextA( sTitle );
    // add or remove * as appropriate
    if ( m_Key.IsChanged() )
    {
        if ( -1 == sTitle.Find('*') )
            sTitle += "*";
    }
    else
    {
        if ( -1 != sTitle.Find('*') )
            sTitle.Remove('*');
    }
    SetWindowTextA(sTitle);
    UpdateWindow();
}

void CKeyDlg::OnContextMenu(CWnd* pWnd, CPoint /*point*/)
{
    CCommentDlg Dlg(this);
    int nItem = -1;
    int iCtlId = pWnd->GetDlgCtrlID();
    switch ( iCtlId )
    {
        case IDC_KEYID_CB:
            Dlg.Display( &m_Key.KeyTag() );
            break;

        case IDC_KEY_TAG_EDT:
//			Dlg.Display( &m_Key.KeyTag() );
            break;

        case IDC_HASH_ALGORITHM_ID_CB:
            Dlg.Display( &m_Key.HashAlgorithmId() );
            break;

        case IDC_KEY_SIZE_CB:
            Dlg.Display( &m_Key.KeySize() );
            break;

        default:
            break;
    }
}

void CKeyDlg::OnCbnSelchangeEncryptAlgorithmIdCb()
{
    UpdateData( TRUE );
    CString sText;
    int idx = EncryptAlgorithmIdCb.GetCurSel();
    EncryptAlgorithmIdCb.GetLBText(idx, sText );
    if ( m_Key.EncryptAlgorithmId() != string((CStringA)sText) )
    {
        m_Key.EncryptAlgorithmId( string((CStringA)sText) );
        m_Key.ResetCompX();
        m_Key.ResetCompY();
        m_Key.ResetExponent();
        m_Key.ResetModulus();
    }
    RefreshState();
}

void CKeyDlg::OnBnClickedKeyInsertSampleKeyBtn()
{
    UpdateData( TRUE );
    if ( m_TimDescriptor.KeyList().size() >= 3 )
    {
        AfxMessageBox("Maximum of 3 Keys already defined.  Delete a key if you wish to add another.");
        return;
    }

    if ( IDYES == AfxMessageBox("Repopulate Key components with Sample Values", MB_YESNO ) )
    {
        m_Key.KeyTag( string("0x4A544147") );
        if ( m_Key.EncryptAlgorithmId() == "" )
             m_Key.EncryptAlgorithmId() = "Marvell";

        if ( m_Key.EncryptAlgorithmId() == "ECDSA_256" )
        {
            // clear old keys
            m_Key.KeySize(0);
            // create new sample ds
            m_Key.KeySize(256);
            m_Key.HashAlgorithmId(string("SHA-160"));

            t_stringListIter iterX = m_Key.ECDSAPublicKeyCompXList().begin();
            *(*(iterX++)) = string("0x5e2be958");
            *(*(iterX++)) = string("0x244b9784");
            *(*(iterX++)) = string("0x648b1912");
            *(*(iterX++)) = string("0xdb1bb81f");
            *(*(iterX++)) = string("0xe2a96d24");
            *(*(iterX++)) = string("0xf6203dc6");
            *(*(iterX++)) = string("0x8ae234ed");
            *(*(iterX++)) = string("0x0d0c4b6f");

            t_stringListIter iterY = m_Key.ECDSAPublicKeyCompYList().begin();
            *(*(iterY++)) = string("0xe71d7abe");
            *(*(iterY++)) = string("0x3aab23b8");
            *(*(iterY++)) = string("0xf64a49d1");
            *(*(iterY++)) = string("0xcab01c4d");
            *(*(iterY++)) = string("0xe095bdf6");
            *(*(iterY++)) = string("0xad85804a");
            *(*(iterY++)) = string("0xdc5c5b10");
            *(*(iterY++)) = string("0x0b0cdfd2");
        }
        else if ( m_Key.EncryptAlgorithmId() == "ECDSA_521" )
        {
            // clear old keys
            m_Key.KeySize(0);
            // create new sample ds
            m_Key.KeySize(521);
            m_Key.HashAlgorithmId(string("SHA-160"));

            t_stringListIter iterX = m_Key.ECDSAPublicKeyCompXList().begin();
            *(*(iterX++)) = string("0x62C8367F");
            *(*(iterX++)) = string("0x3244C9CB");
            *(*(iterX++)) = string("0x7B0B6AC4");
            *(*(iterX++)) = string("0x4ACAC507");
            *(*(iterX++)) = string("0x98E3C011");
            *(*(iterX++)) = string("0x6A61FC54");
            *(*(iterX++)) = string("0x29CA98B6");
            *(*(iterX++)) = string("0x85737715");
            *(*(iterX++)) = string("0x41523555");
            *(*(iterX++)) = string("0xF3B4D4FC");
            *(*(iterX++)) = string("0x1CF20110");
            *(*(iterX++)) = string("0xA7152D7C");
            *(*(iterX++)) = string("0xD2C5A731");
            *(*(iterX++)) = string("0x67CE711E");
            *(*(iterX++)) = string("0x5AF9C5E3");
            *(*(iterX++)) = string("0x2630DBC8");
            *(*(iterX++)) = string("0x0000006A");

            t_stringListIter iterY = m_Key.ECDSAPublicKeyCompYList().begin();
            *(*(iterY++)) = string("0x47A318A3");
            *(*(iterY++)) = string("0x7F9642FB");
            *(*(iterY++)) = string("0x4E45C1D3");
            *(*(iterY++)) = string("0x72B3642A");
            *(*(iterY++)) = string("0x35BC87F3");
            *(*(iterY++)) = string("0xEA7CB21D");
            *(*(iterY++)) = string("0x1DEC9BB9");
            *(*(iterY++)) = string("0x77A5EC46");
            *(*(iterY++)) = string("0x0933645F");
            *(*(iterY++)) = string("0x49006F9A");
            *(*(iterY++)) = string("0x6EBF775A");
            *(*(iterY++)) = string("0x4C6967D7");
            *(*(iterY++)) = string("0x190B6A06");
            *(*(iterY++)) = string("0x511A2A61");
            *(*(iterY++)) = string("0xF46A98D3");
            *(*(iterY++)) = string("0x5B6EF39B");
            *(*(iterY++)) = string("0x00000063");
        }
        else
        {
            // clear old keys
            m_Key.KeySize(0);
            // create new sample ds
            m_Key.PublicKeySize(1024);
            m_Key.KeySize(1024);
            m_Key.HashAlgorithmId(string("SHA-160"));
            t_stringListIter iter = m_Key.PublicKeyExponentList().begin();
            *(*(iter)) = string("0x00000003");

            iter = m_Key.RsaSystemModulusList().begin();
            *(*(iter++)) = string("0xE34F43BD");
            *(*(iter++)) = string("0x75049E3F");
            *(*(iter++)) = string("0xA3527B1A");
            *(*(iter++)) = string("0x9846D1AC");

            *(*(iter++)) = string("0xD15DC003");
            *(*(iter++)) = string("0x0B3DF2F4");
            *(*(iter++)) = string("0xB9AD7A35");
            *(*(iter++)) = string("0x5DF0EC2D");

            *(*(iter++)) = string("0xE48AF431");
            *(*(iter++)) = string("0xAEE9F9E5");
            *(*(iter++)) = string("0x4925FD4F");
            *(*(iter++)) = string("0x5AAFEF7C");

            *(*(iter++)) = string("0x4E676F39");
            *(*(iter++)) = string("0x3A3A69BE");
            *(*(iter++)) = string("0x9EC21E4D");
            *(*(iter++)) = string("0xD5C1E639");

            *(*(iter++)) = string("0x984AB34B");
            *(*(iter++)) = string("0xE9802CB8");
            *(*(iter++)) = string("0x0FA840F5");
            *(*(iter++)) = string("0x28A83FE7");

            *(*(iter++)) = string("0x64D6569A");
            *(*(iter++)) = string("0x80172DE0");
            *(*(iter++)) = string("0x450D7FA9");
            *(*(iter++)) = string("0x52E6A947");

            *(*(iter++)) = string("0xEEE8B271");
            *(*(iter++)) = string("0x7954D939");
            *(*(iter++)) = string("0x003BC642");
            *(*(iter++)) = string("0x0D0AD7E5");

            *(*(iter++)) = string("0x664CA269");
            *(*(iter++)) = string("0xF9786672");
            *(*(iter++)) = string("0xDFE54FE8");
            *(*(iter++)) = string("0xACD1CC46");
        }
    }

    RefreshState();
}

#endif
