/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// PartitionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "PartitionDlg.h"

#include "TimLib.h"
CTimLib TimLib;

// CPartitionDlg dialog

IMPLEMENT_DYNAMIC(CPartitionDlg, CDialog)

CPartitionDlg::CPartitionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPartitionDlg::IDD, pParent)
{
	PartitionId = "0x00000000";
	PartitionAttributes = "0x00000000";
	PartitionStartAddress = "0x0000000000000000";
	PartitionEndAddress = "0x0000000000000000";
	RuntimeBBTLocation = "0x00000000";
	BackupRuntimeBBTLocation = "0x00000000";
	ReservedPoolStartAddress = "0x0000000000000000";
	ReservedPoolSize = "0x00000000";

}

CPartitionDlg::~CPartitionDlg()
{
}

void CPartitionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PARTITION_ID_EDT, PartitionIdEdt);
	DDX_Control(pDX, IDC_PARTITION_ATTRIBUTES_EDT, PartitionAttributesEdt);
	DDX_Control(pDX, IDC_PARTITION_USAGE_CB, PartitionUsageCb);
	DDX_Control(pDX, IDC_PARTITION_TYPE_CB, PartitionTypeCb);
	DDX_Control(pDX, IDC_PARTITION_START_ADDRESS_EDT, PartitionStartAddressEdt);
	DDX_Control(pDX, IDC_PARTITION_END_ADDRESS_EDT, PartitionEndAddressEdt);
	DDX_Control(pDX, IDC_RUNTIME_BBT_TYPE_CB, RuntimeBBTTypeCb);
	DDX_Control(pDX, IDC_RUNTIME_BBT_LOCATION_EDT, RuntimeBBTLocationEdt);
	DDX_Control(pDX, IDC_BACKUP_RUNTIME_BBT_LOCATION_EDT, BackupRuntimeBBTLocationEdt);
	DDX_Control(pDX, IDC_RESERVED_POOL_START_ADDRESS_EDT, ReservedPoolStartAddressEdt);
	DDX_Control(pDX, IDC_RESERVED_POOL_SIZE_EDT, ReservedPoolSizeEdt);
	DDX_Control(pDX, IDC_RESERVED_POOL_ALGORITHM_CB, ReservedPoolAlgorithmCb);
	DDX_Text(pDX, IDC_PARTITION_ID_EDT, PartitionId);
	DDX_Text(pDX, IDC_PARTITION_START_ADDRESS_EDT, PartitionStartAddress);
	DDX_Text(pDX, IDC_PARTITION_ATTRIBUTES_EDT, PartitionAttributes);
	DDX_Text(pDX, IDC_PARTITION_END_ADDRESS_EDT, PartitionEndAddress);
	DDX_Text(pDX, IDC_RUNTIME_BBT_LOCATION_EDT, RuntimeBBTLocation);
	DDX_Text(pDX, IDC_BACKUP_RUNTIME_BBT_LOCATION_EDT, BackupRuntimeBBTLocation);
	DDX_Text(pDX, IDC_RESERVED_POOL_START_ADDRESS_EDT, ReservedPoolStartAddress);
	DDX_Text(pDX, IDC_RESERVED_POOL_SIZE_EDT, ReservedPoolSize);

	// updates marker in title
	UpdateChangeMarkerInTitle();
	theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CPartitionDlg, CDialog)
	ON_EN_CHANGE(IDC_PARTITION_ID_EDT, &CPartitionDlg::OnEnChangePartitionIdEdt)
	ON_CBN_SELCHANGE(IDC_PARTITION_USAGE_CB, &CPartitionDlg::OnCbnSelchangePartitionUsageCb)
	ON_EN_CHANGE(IDC_PARTITION_START_ADDRESS_EDT, &CPartitionDlg::OnEnChangePartitionStartAddressEdt)
	ON_CBN_SELCHANGE(IDC_RUNTIME_BBT_TYPE_CB, &CPartitionDlg::OnCbnSelchangeRuntimeBbtTypeCb)
	ON_EN_CHANGE(IDC_RUNTIME_BBT_LOCATION_EDT, &CPartitionDlg::OnEnChangeRuntimeBbtLocationEdt)
	ON_EN_CHANGE(IDC_BACKUP_RUNTIME_BBT_LOCATION_EDT, &CPartitionDlg::OnEnChangeBackupRuntimeBbtLocationEdt)
	ON_EN_CHANGE(IDC_PARTITION_ATTRIBUTES_EDT, &CPartitionDlg::OnEnChangePartitionAttributesEdt)
	ON_CBN_SELCHANGE(IDC_PARTITION_TYPE_CB, &CPartitionDlg::OnCbnSelchangePartitionTypeCb)
	ON_EN_CHANGE(IDC_PARTITION_END_ADDRESS_EDT, &CPartitionDlg::OnEnChangePartitionEndAddressEdt)
	ON_EN_CHANGE(IDC_RESERVED_POOL_START_ADDRESS_EDT, &CPartitionDlg::OnEnChangeReservedPoolStartAddressEdt)
	ON_EN_CHANGE(IDC_RESERVED_POOL_SIZE_EDT, &CPartitionDlg::OnEnChangeReservedPoolSizeEdt)
	ON_CBN_SELCHANGE(IDC_RESERVED_POOL_ALGORITHM_CB, &CPartitionDlg::OnCbnSelchangeReservedPoolAlgorithmCb)
	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()


// CPartitionDlg message handlers

void CPartitionDlg::OnEnChangePartitionIdEdt()
{
	UpdateData( TRUE );
	m_PartitionData.PartitionId( TimLib.Translate((CStringA)PartitionId));
	UpdateData(FALSE);
}

void CPartitionDlg::OnCbnSelchangePartitionUsageCb()
{
	UpdateData( TRUE );
	CString sText;
	PartitionUsageCb.GetLBText( PartitionUsageCb.GetCurSel(), sText );
	m_PartitionData.PartitionUsage( string((CStringA)sText) );
}

void CPartitionDlg::OnEnChangePartitionStartAddressEdt()
{
	UpdateData( TRUE );
	m_PartitionData.PartitionStartAddress( TimLib.TranslateQWord((CStringA)PartitionStartAddress ));
	UpdateData(FALSE);
}

void CPartitionDlg::OnCbnSelchangeRuntimeBbtTypeCb()
{
	UpdateData( TRUE );
	CString sText;
	RuntimeBBTTypeCb.GetLBText( RuntimeBBTTypeCb.GetCurSel(), sText );
	m_PartitionData.RuntimeBBTType( string((CStringA)sText) );
}

void CPartitionDlg::OnEnChangeRuntimeBbtLocationEdt()
{
	UpdateData( TRUE );
	m_PartitionData.RuntimeBBTLocation( TimLib.Translate((CStringA)RuntimeBBTLocation) );
	UpdateData(FALSE);
}

void CPartitionDlg::OnEnChangeBackupRuntimeBbtLocationEdt()
{
	UpdateData( TRUE );
	m_PartitionData.BackupRuntimeBBTLocation( TimLib.Translate((CStringA)BackupRuntimeBBTLocation) );
	UpdateData(FALSE);
}

void CPartitionDlg::OnEnChangePartitionAttributesEdt()
{
	UpdateData( TRUE );
	m_PartitionData.PartitionAttributes( TimLib.Translate((CStringA)PartitionAttributes) );
	UpdateData(FALSE);
}

void CPartitionDlg::OnCbnSelchangePartitionTypeCb()
{
	UpdateData( TRUE );
	CString sText;
	PartitionTypeCb.GetLBText( PartitionTypeCb.GetCurSel(), sText );
	m_PartitionData.PartitionType( string((CStringA)sText) );
}

void CPartitionDlg::OnEnChangePartitionEndAddressEdt()
{
	UpdateData( TRUE );
	m_PartitionData.PartitionEndAddress( TimLib.TranslateQWord((CStringA)PartitionEndAddress) );
	UpdateData(FALSE);
}

void CPartitionDlg::OnEnChangeReservedPoolStartAddressEdt()
{
	UpdateData( TRUE );
	m_PartitionData.ReservedPoolStartAddress( TimLib.TranslateQWord((CStringA)ReservedPoolStartAddress) );
	UpdateData(FALSE);
}

void CPartitionDlg::OnEnChangeReservedPoolSizeEdt()
{
	UpdateData( TRUE );
	m_PartitionData.ReservedPoolSize( TimLib.Translate((CStringA)ReservedPoolSize) );
	UpdateData(FALSE);
}

void CPartitionDlg::OnCbnSelchangeReservedPoolAlgorithmCb()
{
	UpdateData( TRUE );
	CString sText;
	ReservedPoolAlgorithmCb.GetLBText( ReservedPoolAlgorithmCb.GetCurSel(), sText );
	m_PartitionData.ReservedPoolAlgorithm( string((CStringA)sText) );
}

BOOL CPartitionDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	PartitionIdEdt.m_bHexOnly = true;
	PartitionAttributesEdt.m_bHexOnly = true;
	PartitionStartAddressEdt.m_bHexOnly = true;
	PartitionEndAddressEdt.m_bHexOnly = true;
	RuntimeBBTLocationEdt.m_bHexOnly = true;
	BackupRuntimeBBTLocationEdt.m_bHexOnly = true;
	ReservedPoolStartAddressEdt.m_bHexOnly = true;
	ReservedPoolSizeEdt.m_bHexOnly = true;

	PartitionId = TimLib.HexFormattedAscii(m_PartitionData.PartitionId()).c_str();
	PartitionAttributes = TimLib.HexFormattedAscii(m_PartitionData.PartitionAttributes()).c_str();
	PartitionStartAddress = TimLib.HexFormattedAscii64(m_PartitionData.PartitionStartAddress()).c_str();
	PartitionEndAddress = TimLib.HexFormattedAscii64(m_PartitionData.PartitionEndAddress()).c_str();
	RuntimeBBTLocation = TimLib.HexFormattedAscii(m_PartitionData.RuntimeBBTLocation()).c_str();
	BackupRuntimeBBTLocation = TimLib.HexFormattedAscii(m_PartitionData.BackupRuntimeBBTLocation()).c_str();
	ReservedPoolStartAddress = TimLib.HexFormattedAscii64(m_PartitionData.ReservedPoolStartAddress()).c_str();
	ReservedPoolSize = TimLib.HexFormattedAscii(m_PartitionData.ReservedPoolSize()).c_str();

	if ( LB_ERR != PartitionUsageCb.FindStringExact(0, m_PartitionData.PartitionUsage().c_str() ) )
	{
		// predefined key id selected
		PartitionUsageCb.SetCurSel(PartitionUsageCb.FindStringExact(0, m_PartitionData.PartitionUsage().c_str()));
	}

	if ( LB_ERR != PartitionTypeCb.FindStringExact(0, m_PartitionData.PartitionType().c_str() ) )
	{
		// predefined key id selected
		PartitionTypeCb.SetCurSel(PartitionTypeCb.FindStringExact(0, m_PartitionData.PartitionType().c_str()));
	}

	if ( LB_ERR != RuntimeBBTTypeCb.FindStringExact(0, m_PartitionData.RuntimeBBTType().c_str() ) )
	{
		// predefined key id selected
		RuntimeBBTTypeCb.SetCurSel(RuntimeBBTTypeCb.FindStringExact(0, m_PartitionData.RuntimeBBTType().c_str()));
	}

	if ( LB_ERR != ReservedPoolAlgorithmCb.FindStringExact(0, m_PartitionData.ReservedPoolAlgorithm().c_str() ) )
	{
		// predefined key id selected
		ReservedPoolAlgorithmCb.SetCurSel(ReservedPoolAlgorithmCb.FindStringExact(0, m_PartitionData.ReservedPoolAlgorithm().c_str()));
	}

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CPartitionDlg::UpdateChangeMarkerInTitle()
{
	if ( m_hWnd == 0 )
		return;

	CString sTitle;
	GetWindowTextA( sTitle );
	// add or remove * as appropriate
	if ( m_PartitionData.IsChanged() )
	{
		if ( -1 == sTitle.Find('*') )
			sTitle += "*";
	}
	else
	{
		if ( -1 != sTitle.Find('*') )
			sTitle.Remove('*');
	}
	SetWindowTextA(sTitle);
	UpdateWindow();
}

void CPartitionDlg::OnContextMenu(CWnd* pWnd, CPoint point)
{
#if 0
	// this is just a place holder implementation
	// since the partition data is not in the TIM text file, there is
	// not yet a need to support comments on specific fields.
	// may revisit this later.
	CCommentDlg Dlg(this);
	int nItem = -1;
	int iCtlId = pWnd->GetDlgCtrlID();

	switch ( iCtlId )
	{
		case IDC_PARTITION_ID_EDT:
		case IDC_PARTITION_USAGE_CB:
		case IDC_PARTITION_START_ADDRESS_EDT:
		case IDC_PARTITION_ATTRIBUTES_EDT:
		case IDC_PARTITION_TYPE_CB:
		case IDC_PARTITION_END_ADDRESS_EDT:
		case IDC_RUNTIME_BBT_TYPE_CB:
		case IDC_RUNTIME_BBT_LOCATION_EDT:
		case IDC_BACKUP_RUNTIME_BBT_LOCATION_EDT:
		case IDC_RESERVED_POOL_START_ADDRESS_EDT:
		case IDC_RESERVED_POOL_SIZE_EDT:
		case IDC_RESERVED_POOL_ALGORITHM_CB:
			break;

		default:
			break;
	}
#else
	CDialog::OnContextMenu( pWnd, point );
#endif
}