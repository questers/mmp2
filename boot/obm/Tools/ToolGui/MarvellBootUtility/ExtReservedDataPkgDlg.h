/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

// forward declarations
class CAutoBind;
class CEscapeSeq;
class CResumeDdr;
class CUart;
class CUsb;
class CGpio;
class CGpioSet;
class CTBRXferSet;
class CXfer;
class CUsbVendorReq;
class CDDRInitialization;
class CTimDescriptor;
class CConsumerID;
class CCoreReset;
class CTzInitialization;

// CExtReservedDataPkgDlg dialog

class CExtReservedDataPkgDlg : public CDialog
{
    DECLARE_DYNAMIC(CExtReservedDataPkgDlg)

public:
    CExtReservedDataPkgDlg( CTimDescriptor& TimDescriptor, 
                            CString& sPkgId, CExtendedReservedData& Ext, 
                            bool bEdit, string& sProcessorType,
                            CErdBase*& pErdBase, CErdBase* pParentErdBase = 0, 
                            CWnd* pParent = NULL);   // standard constructor
    virtual ~CExtReservedDataPkgDlg();

// Dialog Data
    enum { IDD = IDD_EXT_RESERVED_DATA_PKG_DLG };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    DECLARE_MESSAGE_MAP()
public:
    CComboBox PackageIdCb;
    CListCtrl PackageFieldsListCtrl;
    CButton InsertFieldBtn;
    CButton EditFieldBtn;
    CButton MoveUpBtn;
    CButton MoveDownBtn;
    CButton DeleteFieldBtn;

    CListCtrl NestedPackagesList;
    CButton AddNestedPkgBtn;
    CButton EditNestedPkgBtn;
    CButton DeleteNestedPkgBtn;

    CString m_sPackageId;

public:
    afx_msg void OnBnClickedExtReservedDataPkgInsertFieldBtn();
    afx_msg void OnBnClickedExtReservedDataPkgEditFieldBtn();
    afx_msg void OnBnClickedExtReservedDataPkgMoveUpBtn();
    afx_msg void OnBnClickedExtReservedDataPkgMoveDownBtn();
    afx_msg void OnBnClickedExtReservedDataPkgDeleteFieldBtn();
    afx_msg void OnCbnSelchangeExtReservedDataPkgIdCb();
    virtual BOOL OnInitDialog();

    afx_msg void OnBnClickedAddExtReservedBtn();
    afx_msg void OnBnClickedEditExtReservedBtn();
    afx_msg void OnBnClickedDeleteExtPackageBtn();
    afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);

public:
    void UpdateChangeMarkerInTitle();

private:
    void RefreshState();
    void UpdateFields( int& nItem, t_PairList& List, t_stringVector& FieldNames );
    void UpdateControls();
    void GetDefinedPkgFields( t_PairList& PkgFieldsList );
    void UpdatePkgFields( t_PairList& PkgFields );
    void MoveFieldUp( t_PairList& PkgFields, unsigned int iSelFieldId );
    void MoveFieldDown( t_PairList& PkgFields, unsigned int iSelFieldId );
    void DeleteField( t_PairList& PkgFields, unsigned int iSelFieldId );
    void ReplaceFields( t_PairList& DestPkgFields, t_PairList& SourcePkgFields );

    // integrate old style reserved data packages
    void UpdateFields( int& nItem, CErdBase* pErdBase );
    
    CExtendedReservedData& ExtReservedData;

    bool			m_bEdit;
    CAutoBind*		m_pAutoBind;
    CEscapeSeq*		m_pEscapeSeq;
    CGpio*			m_pGpio;
    CGpioSet*		m_pGpioSet;
    CResumeDdr*		m_pResumeDdr;
    CTBRXferSet*	m_pTbrXferSet;
    CXfer*			m_pXfer;
    CUart*			m_pUart;
    CUsb*			m_pUsb;
    CUsbVendorReq*	m_pUsbVendorReq;
    CDDRInitialization*	m_pDdrInitialization;
    CConsumerID*	m_pConsumerID;
    CCoreReset*		m_pCoreReset;
    CTzInitialization* m_pTzInitialization;

    CErdBase*&		m_pErdBase;
    CErdBase*		m_pParentErdBase; 
    string			m_sProcessorType;
    CTimDescriptor	m_LocalTimDescriptor;
};
