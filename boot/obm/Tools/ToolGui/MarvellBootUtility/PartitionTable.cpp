/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#include "StdAfx.h"
#include "PartitionTable.h"
#include "MarvellBootUtility.h"
#include "DownloaderInterface.h"

#include "Partition.h"
#include "TimLib.h"

CPartitionTable::CPartitionTable(void)
: CTimLib()
{
	m_PartitionTableVersion = 0x31303031;
	m_bChanged = false;
	m_bNotWritten = false;
}

CPartitionTable::~CPartitionTable(void)
{
	DiscardAll();
}
	
void CPartitionTable::DiscardAll()
{
	t_PartitionListIter iterPartition = m_Partitions.begin();
	while ( iterPartition != m_Partitions.end() )
	{
		delete *iterPartition;
		iterPartition++;
	}
	m_Partitions.clear();
}

CPartitionTable::CPartitionTable( const CPartitionTable& rhs )
: CTimLib()
{
	// copy constructor
	if ( &rhs != this )
	{
		m_PartitionFilePath = rhs.m_PartitionFilePath;
		m_PartitionTableVersion = rhs.m_PartitionTableVersion;
		m_bChanged = rhs.m_bChanged;
		m_bNotWritten = rhs.m_bNotWritten;

		// need to do a deep copy of lists to avoid dangling references
		CPartitionTable& nc_rhs = const_cast<CPartitionTable&>(rhs);
		t_PartitionListIter& iterPartition = nc_rhs.m_Partitions.begin();
		while( iterPartition != nc_rhs.m_Partitions.end() )
		{
			m_Partitions.push_back( new CPartitionData( *(*iterPartition )) );
			iterPartition++;
		}
	}
}

CPartitionTable& CPartitionTable::operator=( const CPartitionTable& rhs ) 
{
	// assignment operator
	if ( &rhs != this )
	{
		// delete the existing list and recreate a new one
		DiscardAll();

		m_PartitionFilePath = rhs.m_PartitionFilePath;
		m_PartitionTableVersion = rhs.m_PartitionTableVersion;
		m_bChanged = rhs.m_bChanged;
		m_bNotWritten = rhs.m_bNotWritten;

		// need to do a deep copy of lists to avoid dangling references
		CPartitionTable& nc_rhs = const_cast<CPartitionTable&>(rhs);
		t_PartitionListIter& iterPartition = nc_rhs.m_Partitions.begin();
		while( iterPartition != nc_rhs.m_Partitions.end() )
		{
			m_Partitions.push_back( new CPartitionData( *(*iterPartition )) );
			iterPartition++;
		}
	}
	return *this;
}

bool CPartitionTable::SaveState( ofstream& ofs )
{
	stringstream ss;
	ss << m_PartitionFilePath << endl;
	ss << m_PartitionTableVersion << endl;

	ss << m_Partitions.size() << endl;
	// save the string stream
	ofs << ss.str();
	
	t_PartitionListIter& iterPartition = m_Partitions.begin();
	while( iterPartition != m_Partitions.end() )
	{
		(*iterPartition)->SaveState( ofs );
		iterPartition++;
	}

	ofs.flush();

	return ofs.good(); // SUCCESS
}

bool CPartitionTable::LoadState( ifstream& ifs )
{
	string sbuf;
	DiscardAll();

	::getline( ifs, m_PartitionFilePath );
	
	::getline( ifs, sbuf );
	m_PartitionTableVersion = Translate(sbuf);

	::getline( ifs, sbuf );
	unsigned int PartitionsSize = Translate(sbuf);

	while( PartitionsSize > 0 )
	{
		CPartitionData* pPartition = new CPartitionData;
		if ( pPartition->LoadState( ifs ) )
		{
			m_Partitions.push_back( pPartition );
		}
		else
		{
			delete pPartition;
			return false;
		}

		PartitionsSize--;
	}

	return ifs.good() && !ifs.fail(); // success
}

string CPartitionTable::GetText()
{
	CTimLib TimLib;
	stringstream ss;
	ss << "Version: " << TimLib.HexFormattedAscii(m_PartitionTableVersion) << endl;
	ss << "Number of Partitions: " << m_Partitions.size() << endl << endl;

	t_PartitionListIter& iterPartition = m_Partitions.begin();
	while( iterPartition != m_Partitions.end() )
	{
		ss << "ID: " << TimLib.HexFormattedAscii((*iterPartition)->PartitionId()) << endl;
		ss << "Usage: " << (*iterPartition)->PartitionUsage() << endl;
		ss << "Type: " << (*iterPartition)->PartitionType() << endl;
		ss << "Attributes: " <<  TimLib.HexFormattedAscii((*iterPartition)->PartitionAttributes()) << endl;
		ss << "Start Address: " << TimLib.HexFormattedAscii64((*iterPartition)->PartitionStartAddress()) << endl;
		ss << "End Address: " << TimLib.HexFormattedAscii64((*iterPartition)->PartitionEndAddress()) << endl;
		ss << "RP Start Address: " << TimLib.HexFormattedAscii64((*iterPartition)->ReservedPoolStartAddress()) << endl;
		ss << "RP Size: " << TimLib.HexFormattedAscii((*iterPartition)->ReservedPoolSize()) << endl;
		ss << "RP Algorithm: " << (*iterPartition)->ReservedPoolAlgorithm() << endl;
		ss << "Runtime BBT Type: " << (*iterPartition)->RuntimeBBTType() << endl;
		ss << "Runtime BBT Location: " << TimLib.HexFormattedAscii((*iterPartition)->RuntimeBBTLocation()) << endl;
		ss << "Backup BBT Location: " << TimLib.HexFormattedAscii((*iterPartition)->BackupRuntimeBBTLocation()) << endl;
		ss << endl;
		iterPartition++;
	}
	
	return ss.str();
}


#if TOOLS_GUI == 1
bool CPartitionTable::ParsePartitionTextFile(CDownloaderInterface& DownloaderInterface)
{
	ifstream ifsPartitionFile;
	PartitionTable_T  PartitionHeader;
	P_PartitionInfo_T pPartitionInfo = 0;

	ifsPartitionFile.open( m_PartitionFilePath.c_str(), ios_base::in );
	if ( ifsPartitionFile.fail() || ifsPartitionFile.bad() )
		return false;

	CPartitionTable FilePartitionTable;
	FilePartitionTable.PartitionFilePath( m_PartitionFilePath );

	CTimLib TimLib;

	if ( DownloaderInterface.TimDescriptorParser.ParsePartitionTextFile(ifsPartitionFile, PartitionHeader, pPartitionInfo) )
	{
		FilePartitionTable.m_PartitionTableVersion = PartitionHeader.Version;

		for( unsigned int i = 0; i < PartitionHeader.NumPartitions; i++ )
		{
			CPartitionData* pPartition = new CPartitionData;
			pPartition->PartitionId( pPartitionInfo[i].Indentifier );
			pPartition->PartitionUsage( TimLib.HexAsciiToText(TimLib.HexFormattedAscii(pPartitionInfo[i].Usage)) );
			pPartition->PartitionType( TimLib.HexAsciiToText(TimLib.HexFormattedAscii(pPartitionInfo[i].Type)) );

			// need some way to convert PartAttributes_T to unsigned int
			// when standard casts do not compile
			union
			{
				PartAttributes_T BitFields;
				unsigned int	 uAttributes;
			} Attribs;
			Attribs.BitFields = pPartitionInfo[i].Attributes;
			pPartition->PartitionAttributes( Attribs.uAttributes );

			pPartition->PartitionStartAddress( pPartitionInfo[i].StartAddr );
			pPartition->PartitionEndAddress( pPartitionInfo[i].EndAddr );
			pPartition->ReservedPoolStartAddress( pPartitionInfo[i].ReserveStartAddr);
			pPartition->ReservedPoolSize( pPartitionInfo[i].ReserveSize );
			pPartition->ReservedPoolAlgorithm( TimLib.HexAsciiToText(TimLib.HexFormattedAscii(pPartitionInfo[i].ReserveAlgorithm)) );
			pPartition->RuntimeBBTType( TimLib.HexAsciiToText(TimLib.HexFormattedAscii(pPartitionInfo[i].BBT_Type)) );
			pPartition->RuntimeBBTLocation( pPartitionInfo[i].RBBT_Location );
			pPartition->BackupRuntimeBBTLocation( pPartitionInfo[i].BackupRBBT_Location );

			FilePartitionTable.Partitions().push_back( pPartition );
		}
		*this = FilePartitionTable;
	}
	ifsPartitionFile.close();
	
	if ( pPartitionInfo )
		free( pPartitionInfo );
	
	return true;
}
#endif

bool CPartitionTable::IsChanged()
{ 
	if ( m_bChanged == true )
		return m_bChanged;

	t_PartitionListIter iterPartition = m_Partitions.begin();
	while ( iterPartition != m_Partitions.end() )
	{
		if ((*iterPartition)->IsChanged() )
			return true;

		iterPartition++;
	}
	
	return m_bChanged; 
}
