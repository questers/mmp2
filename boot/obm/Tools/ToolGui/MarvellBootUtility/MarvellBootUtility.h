/******************************************************************************
 *
 *  (C)Copyright 2005 - 2011 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// MarvellBootUtility.h : main header file for the MarvellBootUtility application
//
#pragma once

#ifndef __AFXWIN_H__
    #error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols

#include "DownloaderInterface.h"
#include "DownloadController.h"
#include "ImageBuild.h"
#include "Target.h"
#include "TimDescriptor.h"
#include "CommandLineParser.h"
#include "ToolsInterfacePropertySheet.h"

#if TRUSTED
#include "TrustedTimDescriptorParser.h"
#else
#include "TimDescriptorParser.h"
#endif 

#include <string>
using namespace std;

// forward declarations
class CLaunchDlg;
class CMarvellBootUtilityDoc;

// CMarvellBootUtilityApp:
// See MarvellBootUtility.cpp for the implementation of this class
//

const string sAppTag("MWTP");
const int AppVersion	= 0x03030125;  // 3.3.1.25
const int AppDate		= 0x20110302;

class CMarvellBootUtilityApp : public CWinApp
{
public:
    CMarvellBootUtilityApp();
    virtual ~CMarvellBootUtilityApp();

// Overrides
public:
    virtual BOOL InitInstance();
    virtual int ExitInstance();

// Implementation
    afx_msg void OnAppAbout();
    afx_msg void OnFileLoadproject();
    afx_msg void OnFileSaveproject();
    afx_msg void OnConfigTargetlist();
    afx_msg void OnAppExit();
    afx_msg void OnConfigImagebuilder();
    afx_msg void OnConfigDKBDownloader();
    afx_msg void OnConfigOBMDownloader();
    afx_msg void OnConfigFbfDownloader();

protected:
    virtual BOOL InitApplication();
    virtual BOOL SaveAllModified();
    DECLARE_MESSAGE_MAP()

public:
    bool SaveState();
    bool LoadState();

    bool IsChanged();
    void Changed( bool bSet );

    void DisplayMsg(CString& sMsg);
    
    bool AddTarget( CString& sInterfaceName );
    bool RemoveTarget( CString& sInterfaceName );

    bool StartDownloadTarget( CString& sInterfaceName );
    bool StartDownloadAllTargets();

    bool StopDownloadTarget( CString& sInterfaceName );
    bool StopDownloadAllTargets();

    bool IsValidDoc( CDocument* pRefDoc );
    bool DocumentClosing( CMarvellBootUtilityDoc* pDoc );
    bool LaunchTBB();

    t_TargetsList& TargetList(){ return m_Targets; }

    CCommandLineParser		CommandLineParser;
    
    CToolsInterfacePropertySheet& ToolsInterfacePropSheet(){ return *m_pToolsInterfacePropSheet; }
    CDownloaderInterface&	DKBDownloaderInterface(){ return *m_pDKBDownloaderInterface; }
    CDownloaderInterface&	OBMDownloaderInterface(){ return *m_pOBMDownloaderInterface; }
    CDownloaderInterface&	FBFDownloaderInterface(){ return *m_pFBFDownloaderInterface; }
    CDownloadController&	DownloadController(){ return *m_pDownloadController; }

    void RefreshModelessDialogsState();
    void RefreshTargetsList();
    
    CDocument* NewTargetLog( string& sTargetName );

    int ProjectVersion;		// for versioning project file
    int ProjectDate;		// for versioning project file
    
    void UARTTargetDownloaderInterface( CDownloaderInterface* pDownloaderInterface )
        { m_UARTTarget.DownloaderInterface( pDownloaderInterface ); }

    void RefreshDownloadFlag();

public:
    CTimDescriptorPropertyPage*		m_pDKBTimDescriptorPropPage;
    CTimDescriptorPropertyPage*		m_pOBMTimDescriptorPropPage;
    CFBFDownloadPropertyPage*		m_pFBFDownloadPropertyPage;

private:
    CTarget							m_UARTTarget; // for manual launch of UART target
    t_TargetsList					m_Targets;
    CLaunchDlg*						m_pLaunchDlg;
    CToolsInterfacePropertySheet*	m_pToolsInterfacePropSheet;

    CDownloaderInterface*			m_pDKBDownloaderInterface;
    CDownloaderInterface*			m_pOBMDownloaderInterface;
    CDownloaderInterface*			m_pFBFDownloaderInterface;
    CDownloadController*			m_pDownloadController;

    string m_sCurrentProject;

    bool m_bChanged;
};

extern CMarvellBootUtilityApp theApp;