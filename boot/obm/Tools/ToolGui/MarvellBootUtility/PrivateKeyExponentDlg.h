/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "afxcmn.h"
#include "EditListCtrl.h"
#include "DigitalSignature.h"
#include "afxwin.h"

// CPrivateKeyExponentDlg dialog

class CPrivateKeyExponentDlg : public CDialog
{
	DECLARE_DYNAMIC(CPrivateKeyExponentDlg)

public:
	CPrivateKeyExponentDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPrivateKeyExponentDlg();

// Dialog Data
	enum { IDD = IDD_PRIVATE_KEY_EXPONENT_DLG };

public:
	CEditListCtrl PrivateKeyExponentListCtrl;
	afx_msg void OnBnClickedResetExponentBtn();
	afx_msg void OnBnClickedOk();
	afx_msg void OnLvnItemActivatePrivateKeyExponentListCtrl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMKillfocusPrivateKeyExponentListCtrl(NMHDR *pNMHDR, LRESULT *pResult);

	UINT uiSizeInBits;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

public:
	void UpdateChangeMarkerInTitle();

	void DigitalSignature( CDigitalSignature& DigitalSignature ) { m_DigitalSignature = DigitalSignature; }
	CDigitalSignature& DigitalSignature() { return m_DigitalSignature; }

	void Title( string sTitle ){ m_sTitle = sTitle; }

private:
	void RefreshState();
	void UpdatePrivateKeyExponentList();

	CDigitalSignature	m_DigitalSignature;
	string m_sTitle;
};
