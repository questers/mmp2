/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once

#include "TimLib.h"

// forward reference
class CTarget;

const int MaxHub = 7;
const int MaxPort = 7;

class CDownloadController : public CTimLib
{
public:
	CDownloadController();
	virtual ~CDownloadController();

	bool Hub_Port( string& sInterfaceName, int& iHub, int& iPort /*, string& sDeviceName */  );
	bool ConditionalLaunchDownload( CTarget* pTarget );
	void UpdateDownloadState( CTarget* pTarget );

	bool m_bDownloading;

	bool m_bDkbDownload;
	bool m_bObmDownload;
	bool m_bFbfDownload;

	bool m_bLaunchOnDiscovery;

	enum DeviceState { IDLE, DOWNLOADING };
	enum DownloadInProgress { NONE, DKB, OBM, FBF };

	struct t_TargetState
	{
		DeviceState			iDeviceState;
		DownloadInProgress	iDownloadInProgress;
	};

	t_TargetState DownloadState[MaxHub][MaxPort];

};
