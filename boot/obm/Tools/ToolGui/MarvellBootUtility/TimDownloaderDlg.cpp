/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// DownloaderPropertyPage.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "TimDownloaderDlg.h"
#include "LaunchDlg.h"
#include "ToolsInterfacePropertySheet.h"

#include <string>
#include <fstream>
using namespace std;


// CTimDownloaderDlg dialog

IMPLEMENT_DYNAMIC(CTimDownloaderDlg, CDialog)

CTimDownloaderDlg::CTimDownloaderDlg(CDownloaderInterface& rDownloaderInterface)
    : CDialog(CTimDownloaderDlg::IDD)
    , DownloaderInterface(rDownloaderInterface)
{
    bVerboseMode = FALSE;
    bRunDebugCmd = FALSE;
    bAdditionalOptions = FALSE;
    bBinaryImageFiles = TRUE;
    bPartitionBinary = FALSE;
    bProcessorType = FALSE;
    bUploadSpecFile = FALSE;

    sUartDelayMs = _T("");
    sJtagKeyFilePath = _T("");
    sWtptpExePath = _T("");
    sTimBinFilePath = _T("");
    sPartitionBinaryFilePath = _T("");
    sUsbPacketSize = _T("");
    sUploadSpecFilePath = _T("");

    sCommandLineTextStr = _T("");
    sAdditionalOptions = _T("");
}

CTimDownloaderDlg::~CTimDownloaderDlg()
{
}

void CTimDownloaderDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Check(pDX, IDC_VERBOSE_MODE_CHK, bVerboseMode);
    DDX_Text(pDX, IDC_COMMAND_LINE_TEXT, sCommandLineTextStr);
    DDX_Control(pDX, IDC_FLASH_TYPE_COMBO, FlashTypeCB);
    DDX_Check(pDX, IDC_RUN_DEBUG_BOOT_CMD_CHK, bRunDebugCmd);
    DDX_Text(pDX, IDC_UART_DELAY, sUartDelayMs);
    DDV_MaxChars(pDX, sUartDelayMs, 5);
    DDX_Text(pDX, IDC_USB_PACKET_SIZE, sUsbPacketSize);
    DDV_MaxChars(pDX, sUsbPacketSize, 6);
    DDX_Text(pDX, IDC_JTAG_KEY_FILE_PATH, sJtagKeyFilePath);
    DDX_Control(pDX, IDC_MSG_MODE_CB, MsgModeCB);
    DDX_Control(pDX, IDC_PORT_TYPE_CB, PortTypeCB);
    DDX_Control(pDX, IDC_COM_PORT_CB, ComPortCB);
    DDX_Control(pDX, IDC_BAUD_RATE_CB, BaudRateCB);
    DDX_Control(pDX, IDC_UART_DELAY, UartDelayEdit);
    DDX_Control(pDX, IDC_USB_PACKET_SIZE, UsbPacketSizeEdt);
    DDX_Text(pDX, IDC_WTPTP_EXE_PATH, sWtptpExePath);
    DDX_Text(pDX, IDC_TIM_BIN_FILE_PATH, sTimBinFilePath);
    DDX_Text(pDX, IDC_WTPTP_ADDITIONAL_OPTIONS_EDT, sAdditionalOptions);
    DDX_Control(pDX, IDC_WTPTP_IMAGES_LIST_CTRL, ImagesListCtrl);
    DDX_Check(pDX, IDC_PARTITION_BINARY_CHK, bPartitionBinary);
    DDX_Control(pDX, IDC_PARTITION_BINARY_BROWSE_BTN, PartitionBinaryBrowseBtn);
    DDX_Text(pDX, IDC_PARTITION_FILE_PATH_EDT, sPartitionBinaryFilePath);
    DDX_Control(pDX, IDC_PARTITION_FILE_PATH_EDT, PartitionBinaryFilePathEdt);
    DDX_Check(pDX, IDC_DOWNLOADER_ADDITIONAL_OPTIONS_CHK, bAdditionalOptions);
    DDX_Control(pDX, IDC_WTPTP_ADDITIONAL_OPTIONS_EDT, AdditionalOptionsEdt);
    DDX_Control(pDX, IDC_USB_PACKET_SIZE, UsbPacketSizeEdt);
    DDX_Text(pDX, IDC_USB_PACKET_SIZE, sUsbPacketSize);
    DDX_Control(pDX, IDC_WTPTP_DOWNLOAD_BTN, UartDownloadBtn);
    DDX_Control(pDX, IDC_WTPTP_DOWNLOAD_STOP_BTN, UartStopDownloadBtn);
    DDX_Control(pDX, IDC_USB_DOWNLOAD_BTN, UsbDownloadBtn);
    DDX_Check(pDX, IDC_DOWNLOAD_PROCESSOR_TYPE_CHK, bProcessorType);
    DDX_Check(pDX, IDC_UPLOAD_SPEC_FILE_CHK, bUploadSpecFile);
    DDX_Text(pDX, IDC_UPLOAD_SPEC_FILE_PATH_EDT, sUploadSpecFilePath);
    DDX_Control(pDX, IDC_UPLOAD_SPEC_FILE_PATH_EDT, UploadSpecFilePathEdt);
    DDX_Control(pDX, IDC_UPLOAD_SPEC_FILE_BROWSE_BTN, UploadSpecFilePathBrowseBtn);

    // updates marker in title
    UpdateChangeMarkerInTitle();
    theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CTimDownloaderDlg, CDialog)
    ON_BN_CLICKED(IDC_VERBOSE_MODE_CHK, &CTimDownloaderDlg::OnBnClickedVerboseModeChk)
    ON_EN_CHANGE(IDC_UART_DELAY, &CTimDownloaderDlg::OnEnChangeUartDelay)
    ON_EN_CHANGE(IDC_USB_PACKET_SIZE, &CTimDownloaderDlg::OnEnChangeUsbPacketSize)
    ON_CBN_SELCHANGE(IDC_FLASH_TYPE_COMBO, &CTimDownloaderDlg::OnCbnSelchangeFlashTypeCombo)
    ON_BN_CLICKED(IDC_RUN_DEBUG_BOOT_CMD_CHK, &CTimDownloaderDlg::OnBnClickedRunDebugBootCmdChk)
    ON_BN_CLICKED(IDC_JTAG_KEY_FILE_BROWSE_BTN, &CTimDownloaderDlg::OnBnClickedJtagKeyFileBrowseBtn)
    ON_EN_CHANGE(IDC_JTAG_KEY_FILE_PATH, &CTimDownloaderDlg::OnEnChangeJtagKeyFilePath)
    ON_CBN_SELCHANGE(IDC_PORT_TYPE_CB, &CTimDownloaderDlg::OnCbnSelchangePortTypeCb)
    ON_BN_CLICKED(IDC_WTPTP_EXE_BROWSE_BTN, &CTimDownloaderDlg::OnBnClickedWtptpExeBrowseBtn)
    ON_EN_CHANGE(IDC_WTPTP_EXE_PATH, &CTimDownloaderDlg::OnEnChangeWtptpExePath)
    ON_CBN_SELCHANGE(IDC_MSG_MODE_CB, &CTimDownloaderDlg::OnCbnSelchangeMsgModeCb)
    ON_CBN_SELCHANGE(IDC_COM_PORT_CB, &CTimDownloaderDlg::OnCbnSelchangeComPortCb)
    ON_CBN_SELCHANGE(IDC_BAUD_RATE_CB, &CTimDownloaderDlg::OnCbnSelchangeBaudRateCb)
    ON_BN_CLICKED(IDC_WTPTP_RESET_BTN, &CTimDownloaderDlg::OnBnClickedWtptpResetBtn)
    ON_EN_CHANGE(IDC_WTPTP_ADDITIONAL_OPTIONS_EDT, &CTimDownloaderDlg::OnEnChangeWtptpAdditionalOptionsEdt)
    ON_BN_CLICKED(IDC_WTPTP_DOWNLOAD_BTN, &CTimDownloaderDlg::OnBnClickedWtptpDownloadBtn)
    ON_BN_CLICKED(IDC_WTPTP_DOWNLOAD_STOP_BTN, &CTimDownloaderDlg::OnBnClickedWtptpDownloadStopBtn)
    ON_BN_CLICKED(IDC_PARTITION_BINARY_BROWSE_BTN, &CTimDownloaderDlg::OnBnClickedPartitionBinaryBrowseBtn)
    ON_EN_CHANGE(IDC_PARTITION_FILE_PATH_EDT, &CTimDownloaderDlg::OnEnChangePartitionFilePathEdt)
    ON_BN_CLICKED(IDC_PARTITION_BINARY_CHK, &CTimDownloaderDlg::OnBnClickedPartitionBinaryChk)
    ON_BN_CLICKED(IDC_DOWNLOADER_ADDITIONAL_OPTIONS_CHK, &CTimDownloaderDlg::OnBnClickedDownloaderAdditionalOptionsChk)
    ON_BN_CLICKED(IDC_USB_DOWNLOAD_BTN, &CTimDownloaderDlg::OnBnClickedUsbDownloadBtn)
    ON_BN_CLICKED(IDC_DOWNLOAD_PROCESSOR_TYPE_CHK, &CTimDownloaderDlg::OnBnClickedDownloadProcessorTypeChk)
    ON_BN_CLICKED(IDOK, &CTimDownloaderDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDC_UPLOAD_SPEC_FILE_CHK, &CTimDownloaderDlg::OnBnClickedUploadSpecFileChk)
    ON_EN_CHANGE(IDC_UPLOAD_SPEC_FILE_PATH_EDT, &CTimDownloaderDlg::OnEnChangeUploadSpecFilePathEdt)
    ON_BN_CLICKED(IDC_UPLOAD_SPEC_FILE_BROWSE_BTN, &CTimDownloaderDlg::OnBnClickedUploadSpecFileBrowseBtn)
END_MESSAGE_MAP()


// CTimDownloaderDlg message handlers
BOOL CTimDownloaderDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    int idx = -1;
    FlashTypeCB.InsertString(++idx,"Default - Probe Flash"); FlashTypeCB.SetItemData(idx,0x00000000);
    FlashTypeCB.InsertString(++idx,"HSI1");FlashTypeCB.SetItemData(idx,0x48534901);
    FlashTypeCB.InsertString(++idx,"x16 OneNAND");FlashTypeCB.SetItemData(idx,0x4E414E02);
    FlashTypeCB.InsertString(++idx,"XIP Flash x16");FlashTypeCB.SetItemData(idx,0x58495003);
    FlashTypeCB.InsertString(++idx,"x16 NAND Hamming");FlashTypeCB.SetItemData(idx,0x4E414E04);
    FlashTypeCB.InsertString(++idx,"x16 XIP SIBLEY");FlashTypeCB.SetItemData(idx,0x58495005);
    FlashTypeCB.InsertString(++idx,"NAND x8 Hamming");FlashTypeCB.SetItemData(idx,0x4E414E06);
    FlashTypeCB.InsertString(++idx,"SDMMC Flash Opt0");FlashTypeCB.SetItemData(idx,0x4D4D4307);
    FlashTypeCB.InsertString(++idx,"SDMMC Flash Opt1");FlashTypeCB.SetItemData(idx,0x4D4D4308);
    FlashTypeCB.InsertString(++idx,"SDMMC Flash Opt2");FlashTypeCB.SetItemData(idx,0x4D4D4309);
    FlashTypeCB.InsertString(++idx,"SPI Flash");FlashTypeCB.SetItemData(idx,0x5350490A);
    FlashTypeCB.InsertString(++idx,"SDMMC Flash Opt3");FlashTypeCB.SetItemData(idx,0x4D4D430B);
    FlashTypeCB.InsertString(++idx,"SDMMC Flash Opt4");FlashTypeCB.SetItemData(idx,0x4D4D430C);
    FlashTypeCB.InsertString(++idx,"x16 NAND BCH");FlashTypeCB.SetItemData(idx,0x4E414E0D);
    FlashTypeCB.InsertString(++idx,"x8 NAND BCH");FlashTypeCB.SetItemData(idx,0x4E414E0E);

    MsgModeCB.InsertString(0,"None");
    MsgModeCB.InsertString(1,"Message Only");
    MsgModeCB.InsertString(2,"Message and Download");

    PortTypeCB.InsertString(0,"UART");
    PortTypeCB.InsertString(1,"USB");

    ComPortCB.InsertString(0,"None");
    ComPortCB.InsertString(1,"1");
    ComPortCB.InsertString(2,"2");
    ComPortCB.InsertString(3,"3");
    ComPortCB.InsertString(4,"4");
    ComPortCB.InsertString(5,"5");
    ComPortCB.InsertString(6,"6");
    ComPortCB.InsertString(7,"7");
    ComPortCB.InsertString(8,"8");
    ComPortCB.InsertString(9,"9");
    ComPortCB.InsertString(10,"10");
    ComPortCB.InsertString(11,"11");
    ComPortCB.InsertString(12,"12");
    ComPortCB.InsertString(13,"13");
    ComPortCB.InsertString(14,"14");
    ComPortCB.InsertString(15,"15");
    ComPortCB.InsertString(16,"16");

    BaudRateCB.InsertString(0,"38400");
    BaudRateCB.InsertString(1,"115200");

    ImagesListCtrl.InsertColumn(0,"Image File Path", LVCFMT_LEFT, 656, -1 );
    ImagesListCtrl.EditControlType[0] = ReadOnly;

    RefreshState();

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTimDownloaderDlg::OnEnChangeUartDelay()
{
    UpdateData( TRUE );
    while ( sUartDelayMs.Left(1) == _T("0") )
        sUartDelayMs = sUartDelayMs.Right( sUartDelayMs.GetLength()-1 );

    DownloaderInterface.UartDelayMs( string((CStringA)sUartDelayMs) );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnEnChangeUsbPacketSize()
{
    UpdateData( TRUE );
    while ( sUsbPacketSize.Left(1) == _T("0") )
        sUsbPacketSize = sUsbPacketSize.Right( sUsbPacketSize.GetLength()-1 );

    DownloaderInterface.UsbPacketSize( string((CStringA)sUsbPacketSize) );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnBnClickedVerboseModeChk()
{
    UpdateData( TRUE );
    DownloaderInterface.VerboseMode( bVerboseMode == TRUE ? true : false );
    UpdateCommandLineText();
}


void CTimDownloaderDlg::UpdateCommandLineText()
{
    UpdateControls();
    sCommandLineTextStr = DownloaderInterface.CommandLineArgs().c_str();
    UpdateData( FALSE );
}

void CTimDownloaderDlg::OnCbnSelchangeFlashTypeCombo()
{
    UpdateData( TRUE );
    DownloaderInterface.FlashType( (CDownloaderInterface::eFlashType)FlashTypeCB.GetCurSel() );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnBnClickedRunDebugBootCmdChk()
{
    UpdateData( TRUE );
    DownloaderInterface.RunDebugBootCommand( bRunDebugCmd == TRUE ? true : false );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnBnClickedJtagKeyFileBrowseBtn()
{
    UpdateData( TRUE );
    CFileDialog Dlg( TRUE, _T("*.txt"), NULL, 
                     OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST,
                     _T("Text (*.txt)|*.txt|All Files (*.*)|*.*||") );
    if ( Dlg.DoModal() == IDOK )
    {
        sJtagKeyFilePath = Dlg.GetPathName();
        DownloaderInterface.JtagKeyFilePath( string((CStringA)sJtagKeyFilePath) );
    }
    UpdateCommandLineText();
    SetFocus();
}

void CTimDownloaderDlg::OnEnChangeJtagKeyFilePath()
{
    UpdateData( TRUE );
    DownloaderInterface.JtagKeyFilePath( string((CStringA)sJtagKeyFilePath) );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnCbnSelchangePortTypeCb()
{
    UpdateData( TRUE );
    CString sText;
    PortTypeCB.GetLBText( PortTypeCB.GetCurSel(), sText );
    DownloaderInterface.PortType( string((CStringA)sText) );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnBnClickedWtptpExeBrowseBtn()
{
    UpdateData( TRUE );
    CFileDialog Dlg( TRUE, _T("*.exe"), NULL, 
                     OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST,
                     _T("Executable (*.exe)|*.exe|All Files (*.*)|*.*||") );
    if ( Dlg.DoModal() == IDOK )
    {
        sWtptpExePath = Dlg.GetPathName();
        DownloaderInterface.WtptpExePath( string((CStringA)sWtptpExePath) );
    }
    UpdateCommandLineText();
    SetFocus();
}

void CTimDownloaderDlg::OnEnChangeWtptpExePath()
{
    UpdateData( TRUE );
    DownloaderInterface.WtptpExePath( string((CStringA)sWtptpExePath) );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::RefreshState()
{
    if ( m_hWnd != NULL )
    {
        UpdateData( TRUE );

        bVerboseMode = DownloaderInterface.VerboseMode() ? TRUE : FALSE;
        bRunDebugCmd = DownloaderInterface.RunDebugBootCommand() ? TRUE : FALSE;
        sUartDelayMs = DownloaderInterface.UartDelayMs().c_str();
        sUsbPacketSize = DownloaderInterface.UsbPacketSize().c_str();
        sJtagKeyFilePath = DownloaderInterface.JtagKeyFilePath().c_str();
        sWtptpExePath = DownloaderInterface.WtptpExePath().c_str();
        sTimBinFilePath = DownloaderInterface.TimFilePath().c_str();
        bProcessorType = DownloaderInterface.ProcessorTypeEnable() ? TRUE : FALSE;
        bUploadSpecFile = DownloaderInterface.UploadSpecFileEnable() ? TRUE : FALSE;
        sUploadSpecFilePath = DownloaderInterface.UploadSpecFilePath().c_str();

        MsgModeCB.SetCurSel( MsgModeCB.FindStringExact(0,CString(DownloaderInterface.MsgMode().c_str()) ) );
        PortTypeCB.SetCurSel( PortTypeCB.FindStringExact(0,CString(DownloaderInterface.PortType().c_str()) ) );
        ComPortCB.SetCurSel( ComPortCB.FindStringExact(0,CString(DownloaderInterface.ComPort().c_str()) ) );
        BaudRateCB.SetCurSel( BaudRateCB.FindStringExact(0,CString(DownloaderInterface.BaudRate().c_str()) ) );
        FlashTypeCB.SetCurSel( (int)DownloaderInterface.FlashType() );

        ImagesListCtrl.DeleteAllItems();
        t_stringList& ImageBinList = DownloaderInterface.ImageBinFiles();
        // get all ImageBinList strings
        t_stringListIter iterImage = ImageBinList.begin();
        while( iterImage != ImageBinList.end() )
        {
            ImagesListCtrl.InsertItem( ImagesListCtrl.GetItemCount(), (*iterImage)->c_str() );
            iterImage++;
        }

        bPartitionBinary = DownloaderInterface.PartitionBinary();
        sPartitionBinaryFilePath = DownloaderInterface.PartitionBinaryFilePath().c_str();

        if ( bAdditionalOptions )
            sAdditionalOptions = DownloaderInterface.AdditionalOptions().c_str();
        
        UpdateCommandLineText();
//        UpdateControls();
    }
}

void CTimDownloaderDlg::UpdateControls()
{
    BOOL bUsb = (DownloaderInterface.PortType() == "USB" ) ? TRUE : FALSE;

    ComPortCB.EnableWindow( !bUsb );
    BaudRateCB.EnableWindow( !bUsb );
    UartDelayEdit.EnableWindow( !bUsb );
    UsbPacketSizeEdt.EnableWindow( bUsb );

    UartDownloadBtn.EnableWindow( !bUsb );
    UartStopDownloadBtn.EnableWindow( !bUsb );
    UsbDownloadBtn.EnableWindow( bUsb );

    ImagesListCtrl.EnableWindow( FALSE );
    
    PartitionBinaryBrowseBtn.EnableWindow( bPartitionBinary );
    PartitionBinaryFilePathEdt.EnableWindow( bPartitionBinary );
    
    AdditionalOptionsEdt.EnableWindow( bAdditionalOptions );

    UploadSpecFilePathBrowseBtn.EnableWindow( bUploadSpecFile );
    UploadSpecFilePathEdt.EnableWindow( bUploadSpecFile );
}


void CTimDownloaderDlg::OnCbnSelchangeMsgModeCb()
{
    UpdateData( TRUE );
    CString sText;
    MsgModeCB.GetLBText( MsgModeCB.GetCurSel(), sText );
    DownloaderInterface.MsgMode( string((CStringA)sText) );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnCbnSelchangeComPortCb()
{
    UpdateData( TRUE );
    CString sText;
    ComPortCB.GetLBText( ComPortCB.GetCurSel(), sText );
    DownloaderInterface.ComPort( string((CStringA)sText) );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnCbnSelchangeBaudRateCb()
{
    UpdateData( TRUE );
    CString sText;
    BaudRateCB.GetLBText( BaudRateCB.GetCurSel(), sText );
    DownloaderInterface.BaudRate( string((CStringA)sText) );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnBnClickedWtptpResetBtn()
{
    DownloaderInterface.Reset();
    RefreshState();
}

void CTimDownloaderDlg::OnEnChangeWtptpAdditionalOptionsEdt()
{
    UpdateData( TRUE );
    DownloaderInterface.AdditionalOptions( string((CStringA)sAdditionalOptions) );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnBnClickedWtptpDownloadBtn()
{
    if ( DownloaderInterface.WtptpExePath().length() == 0 )
    {
        AfxMessageBox( "WTPTP path is not assigned in Downloader Configuration" );
        return;
    }

    ifstream ifs;
    ifs.open( DownloaderInterface.TimFilePath().c_str(), ios_base::in | ios_base::binary );
    if ( ifs.is_open() )
        ifs.close();
    else
    {
        AfxMessageBox( "Error: Unable to open TIM binary file. Check for valid file name and path." );
        return;
    }
    
    CString sPortType;
    PortTypeCB.GetWindowTextA( sPortType );
    if ( sPortType == "UART" )
        theApp.UARTTargetDownloaderInterface( &DownloaderInterface );

    theApp.StartDownloadTarget( sPortType ); 
}

void CTimDownloaderDlg::OnBnClickedWtptpDownloadStopBtn()
{
    CString sPortType;
    PortTypeCB.GetWindowTextA( sPortType );
    theApp.StopDownloadTarget( sPortType ); 
}

void CTimDownloaderDlg::OnBnClickedPartitionBinaryBrowseBtn()
{
    UpdateData( TRUE );
    CFileDialog Dlg( TRUE, _T("*.bin"), NULL, 
                     OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST,
                     _T("Binary (*.bin)|*.bin|All Files (*.*)|*.*||") );
    if ( Dlg.DoModal() == IDOK )
    {
        sPartitionBinaryFilePath = Dlg.GetPathName();
        DownloaderInterface.PartitionBinaryFilePath( string((CStringA)sPartitionBinaryFilePath) );
    }
    UpdateCommandLineText();
    SetFocus();
}

void CTimDownloaderDlg::OnEnChangePartitionFilePathEdt()
{
    UpdateData( TRUE );
    DownloaderInterface.PartitionBinaryFilePath( string((CStringA)sPartitionBinaryFilePath) );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnBnClickedPartitionBinaryChk()
{
    UpdateData( TRUE );
    DownloaderInterface.PartitionBinary( bPartitionBinary == TRUE ? true : false );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnBnClickedDownloaderAdditionalOptionsChk()
{
    UpdateData( TRUE );
    DownloaderInterface.AdditionalOptionsEnable( bAdditionalOptions == TRUE ? true : false );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::UpdateChangeMarkerInTitle()
{
    if ( m_hWnd == 0 )
        return;

    CString sTitle;
    GetWindowTextA( sTitle );
    // add or remove * as appropriate
    if ( DownloaderInterface.IsChanged() )
    {
        if ( -1 == sTitle.Find('*') )
            sTitle += "*";
    }
    else
    {
        if ( -1 != sTitle.Find('*') )
            sTitle.Remove('*');
    }
    SetWindowTextA(sTitle);
    UpdateWindow();
}

void CTimDownloaderDlg::OnBnClickedUsbDownloadBtn()
{
    theApp.OnConfigTargetlist();
    // close this dialog so the user can use the USB Target dialog
    OnOK();
}



void CTimDownloaderDlg::OnBnClickedDownloadProcessorTypeChk()
{
    UpdateData( TRUE );
    DownloaderInterface.ProcessorTypeEnable( bProcessorType == TRUE ? true : false );
    theApp.CommandLineParser.ProcessorType( DownloaderInterface.TimDescriptorParser.TimDescriptor().ProcessorType() );
    UpdateCommandLineText();
}

void CTimDownloaderDlg::OnBnClickedOk()
{
    OnOK();
}

void CTimDownloaderDlg::OnBnClickedUploadSpecFileChk()
{
    UpdateData( TRUE );
    DownloaderInterface.UploadSpecFileEnable( bUploadSpecFile == TRUE ? true : false );
    UpdateCommandLineText();
}


void CTimDownloaderDlg::OnEnChangeUploadSpecFilePathEdt()
{
    UpdateData( TRUE );
    DownloaderInterface.UploadSpecFilePath( string((CStringA)sUploadSpecFilePath) );
    UpdateCommandLineText();
}


void CTimDownloaderDlg::OnBnClickedUploadSpecFileBrowseBtn()
{
    UpdateData( TRUE );
    CFileDialog Dlg( TRUE, _T("*.txt"), NULL, 
                     OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST,
                     _T("Text (*.txt)|*.txt|All Files (*.*)|*.*||") );
    if ( Dlg.DoModal() == IDOK )
    {
        sUploadSpecFilePath = Dlg.GetPathName();
        DownloaderInterface.UploadSpecFilePath( string((CStringA)sUploadSpecFilePath) );
    }
    UpdateCommandLineText();
    SetFocus();
}
