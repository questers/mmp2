/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// PrivateKeyExponentDlg.cpp : implementation file
//

#include "stdafx.h"

#if TRUSTED

#include "MarvellBootUtility.h"
#include "PrivateKeyExponentDlg.h"

// CPrivateKeyExponentDlg dialog

IMPLEMENT_DYNAMIC(CPrivateKeyExponentDlg, CDialog)

CPrivateKeyExponentDlg::CPrivateKeyExponentDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrivateKeyExponentDlg::IDD, pParent)
{
	uiSizeInBits = 0;
}

CPrivateKeyExponentDlg::~CPrivateKeyExponentDlg()
{
}

void CPrivateKeyExponentDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PRIVATE_KEY_EXPONENT_LIST_CTRL, PrivateKeyExponentListCtrl);
	DDX_Text(pDX, IDC_SIZE_IN_BITS, uiSizeInBits);

	// updates marker in title
	UpdateChangeMarkerInTitle();
	theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CPrivateKeyExponentDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CPrivateKeyExponentDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_RESET_EXPONENT_BTN, &CPrivateKeyExponentDlg::OnBnClickedResetExponentBtn)
	ON_NOTIFY(LVN_ITEMACTIVATE, IDC_PRIVATE_KEY_EXPONENT_LIST_CTRL, &CPrivateKeyExponentDlg::OnLvnItemActivatePrivateKeyExponentListCtrl)
	ON_NOTIFY(NM_KILLFOCUS, IDC_PRIVATE_KEY_EXPONENT_LIST_CTRL, &CPrivateKeyExponentDlg::OnNMKillfocusPrivateKeyExponentListCtrl)
END_MESSAGE_MAP()


// CPrivateKeyExponentDlg message handlers

BOOL CPrivateKeyExponentDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	PrivateKeyExponentListCtrl.SetExtendedStyle( //  LVS_EX_TRACKSELECT
											    //  LVS_EX_BORDERSELECT
												LVS_EX_FULLROWSELECT 
												| LVS_EX_HEADERDRAGDROP 
												| LVS_EX_ONECLICKACTIVATE 
												| LVS_EX_LABELTIP 
												| LVS_EX_GRIDLINES
											  );

	PrivateKeyExponentListCtrl.InsertColumn(0,"#", LVCFMT_LEFT, 36, -1 );
	PrivateKeyExponentListCtrl.InsertColumn(1,"Exponent Word #", LVCFMT_LEFT, 140, -1 );
	PrivateKeyExponentListCtrl.InsertColumn(2,"Exponent Word #+1", LVCFMT_LEFT, 140, -1 );
	PrivateKeyExponentListCtrl.InsertColumn(3,"Exponent Word #+2", LVCFMT_LEFT, 140, -1 );
	PrivateKeyExponentListCtrl.InsertColumn(4,"Exponent Word #+3", LVCFMT_LEFT, 140, -1 );

	PrivateKeyExponentListCtrl.EditControlType[0] = ReadOnly;
	PrivateKeyExponentListCtrl.EditControlType[1] = HexEdit;
	PrivateKeyExponentListCtrl.EditControlType[2] = HexEdit;
	PrivateKeyExponentListCtrl.EditControlType[3] = HexEdit;
	PrivateKeyExponentListCtrl.EditControlType[4] = HexEdit;

	uiSizeInBits = m_DigitalSignature.KeySize();
	int nRows = (((uiSizeInBits+31)/32)+3)/4;
	for ( int i = 0; i < nRows; i++ )
	{
		PrivateKeyExponentListCtrl.InsertItem(i,""); 
	}

	UpdateData(FALSE);
	RefreshState();
	PrivateKeyExponentListCtrl.Changed( m_DigitalSignature.IsChanged() );
	SetWindowTextA( m_sTitle.c_str() );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CPrivateKeyExponentDlg::RefreshState()
{
	UpdateData( TRUE );

	t_stringList* pPrivateKeyList = &m_DigitalSignature.RsaPrivateKeyList();
	if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_256" || m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_521" )
		pPrivateKeyList = &m_DigitalSignature.ECDSAPrivateKeyList();

	int idxItem = 0; 
	int idxSubItem = 0;

	t_stringListIter iterExponent( pPrivateKeyList->begin() );
	while( iterExponent != pPrivateKeyList->end() )
	{
		if ( idxSubItem == 0 )
		{
			CString sRow;
			sRow.Format( "%d", idxItem*4 ); // assumes 5 columns per row
			PrivateKeyExponentListCtrl.SetItemText( idxItem, idxSubItem++, sRow );
		}
		else
			PrivateKeyExponentListCtrl.SetItemText( idxItem, idxSubItem++, (*iterExponent++)->c_str() );
		
		// set idx for next row
		if ( idxSubItem == 5 )	// assumes 5 columns per row
		{
			idxItem++;
			idxSubItem = 0;
		}
	}
	
	UpdateData( FALSE );
}


void CPrivateKeyExponentDlg::OnBnClickedResetExponentBtn()
{
	m_DigitalSignature.ResetPrivateKey();
	RefreshState();
}


void CPrivateKeyExponentDlg::OnBnClickedOk()
{
	UpdatePrivateKeyExponentList();
	OnOK();
}


void CPrivateKeyExponentDlg::UpdatePrivateKeyExponentList()
{
	t_stringList* pPrivateKeyList = &m_DigitalSignature.RsaPrivateKeyList();
	if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_256" || m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_521" )
		pPrivateKeyList = &m_DigitalSignature.ECDSAPrivateKeyList();

	int idxItem = 0;
	int idxSubItem = 1;

	t_stringListIter iterExponent( pPrivateKeyList->begin() );
	while( iterExponent != pPrivateKeyList->end() )
	{
		*(*iterExponent++) = PrivateKeyExponentListCtrl.GetItemText( idxItem, idxSubItem++ );

		// set idx for next row
		if ( idxSubItem == 5 )	// assumes 5 columns per row
		{
			idxItem++;
			idxSubItem = 1;  // ignore row number in subitem 0
		}
	}
	UpdateData(FALSE);
}

void CPrivateKeyExponentDlg::UpdateChangeMarkerInTitle()
{
	if ( m_hWnd == 0 )
		return;

	CString sTitle;
	GetWindowTextA( sTitle );
	// add or remove * as appropriate
	if ( PrivateKeyExponentListCtrl.IsChanged() == true )
	{
		m_DigitalSignature.Changed( true );
		if ( -1 == sTitle.Find('*') )
			sTitle += "*";
	}
	else
	{
		if ( -1 != sTitle.Find('*') )
			sTitle.Remove('*');
	}
	SetWindowTextA(sTitle);
	UpdateWindow();
}


void CPrivateKeyExponentDlg::OnLvnItemActivatePrivateKeyExponentListCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	UpdateData(FALSE);
}

void CPrivateKeyExponentDlg::OnNMKillfocusPrivateKeyExponentListCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	UpdatePrivateKeyExponentList();
	m_DigitalSignature.Changed(true);
}
#endif