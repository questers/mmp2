/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "EditListCtrl.h"
#include "ImageDescription.h"
#include "MyEdit.h"
#include "MyComboBox.h"
#include "DownloaderInterface.h"

// CImageConfigurationDlg dialog


class CImageConfigurationDlg : public CDialog
{
	DECLARE_DYNAMIC(CImageConfigurationDlg)

public:
	CImageConfigurationDlg(CDownloaderInterface& DownloaderInterface, CImageDescription& OrigImage, CWnd* pParent = NULL);   // standard constructor
	virtual ~CImageConfigurationDlg();

// Dialog Data
	enum { IDD = IDD_TIM_IMAGE_CONFIG_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

	CImageDescription m_Image;

public:
	afx_msg void OnEnChangeImageFilePathEdt();
	afx_msg void OnBnClickedImageFilePathBrowseBtn();
	afx_msg void OnCbnSelchangeImageIdCb();
	afx_msg void OnCbnEditchangeImageIdCb();
	afx_msg void OnEnChangeImageIdTag();
	afx_msg void OnEnChangeImageFlashEntryEdt();
	afx_msg void OnEnChangeImageLoadAddressEdt();
	afx_msg void OnCbnSelchangeImageHashAlgorithmIdCb();
	afx_msg void OnEnChangeImageSizeToHashEdt();
	afx_msg void OnBnClickedOk();
	afx_msg void OnEnChangePartitionNumberEdt();
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);

	CString sImageFilePath;
	CString sImageIdTag;
	CString sFlashEntryAddress;
	CString sLoadAddress;
	CString sImageSizeToHash;
	UINT uiPartitionNumber;
	CString sImageId;

	CMyEdit		 ImageFilePathEdt;
	CMyComboBox	 ImageIdCb;
	CMyComboBox	 HashAlgorithmIdCb;
	CMyEdit		 PartitionNumberEdt;

	CHexItemEdit ImageSizeToHashEdt;
	CHexItemEdit ImageIdTagEdt;
	CHexItemEdit ImageFlashEntryEdt;
	CHexItemEdit ImageLoadAddressEdt;

public:
	CImageDescription& Image() { return m_Image; }

	void UpdateChangeMarkerInTitle();

private:
	void RefreshState();
	void UpdateControls();

	CDownloaderInterface& DownloaderInterface;
};
