/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "TimLib.h"

#include <vector>
using namespace std;

typedef vector<string*>           t_stringVector;
typedef vector<string*>::iterator t_stringVectorIter;

class CXdbDdrInitScriptParser
{
public:
	CXdbDdrInitScriptParser(void);
	~CXdbDdrInitScriptParser(void);

	bool ParseFile( string sFilePath );

	t_stringVector DdrInitPkgLines;
};
