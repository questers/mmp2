/******************************************************************************
 *
 *  (C)Copyright 2005 - 2011 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once

#include "EditListCtrl.h"
#include "DDRInitialization.h"
#include "afxwin.h"

// CInstructionsDlg dialog
class CTimDescriptor;

class CInstructionsDlg : public CDialog
{
    DECLARE_DYNAMIC(CInstructionsDlg)

public:
    CInstructionsDlg(CTimDescriptor& TimDescriptor, t_InstructionList& Instructions, CWnd* pParent = NULL);   // standard constructor
    virtual ~CInstructionsDlg();

// Dialog Data
    enum { IDD = IDD_INSTRUCTIONS_DLG };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    DECLARE_MESSAGE_MAP()
public:
    CListCtrl InstructionsListCtrl;
    CButton InsertBtn;
    CButton RemoveBtn;
    CButton MoveUpBtn;
    CButton MoveDownBtn;

    CComboBox InstructionOpCodeCb;
    CEdit InstructionOpCodeEdt;
    CEdit NumParametersEdt;
    CEdit Param1DescEdt;
    CEdit Param2DescEdt;
    CEdit Param3DescEdt;
    CEdit Param4DescEdt;
    CEdit Param5DescEdt;

    CHexItemEdit Param1Edt;
    CHexItemEdit Param2Edt;
    CHexItemEdit Param3Edt;
    CHexItemEdit Param4Edt;
    CHexItemEdit Param5Edt;

    afx_msg void OnBnClickedInsertBtn();
    afx_msg void OnBnClickedRemoveBtn();
    afx_msg void OnBnClickedMoveUp();
    afx_msg void OnBnClickedMoveDown();
    afx_msg void OnCbnSelchangeInstructionOpCodeCb();
    afx_msg void OnEnChangeParam1ValEdt();
    afx_msg void OnEnChangeParam2ValEdt();
    afx_msg void OnEnChangeParam3ValEdt();
    afx_msg void OnEnChangeParam4ValEdt();
    afx_msg void OnEnChangeParam5ValEdt();
    afx_msg void OnBnClickedOk();
    afx_msg void OnLvnItemActivateInstructionsList(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnLvnItemChangedInstructionsList(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnNMClickInstructionsList(NMHDR *pNMHDR, LRESULT *pResult);

    virtual BOOL OnInitDialog();

    void RefreshState();
    void RefreshInstState();
    void UpdateControls();
    void UpdateChangeMarkerInTitle();

    t_InstructionList& m_Instructions;
    CTimDescriptor	m_LocalTimDescriptor;
    
    int m_OpCodeCbSel;
    bool m_bRefreshState;
    CString sComment;
    CEdit CommentTxt;
    afx_msg void OnEnChangeInstructionComment();
};
