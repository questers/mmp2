/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#include "stdafx.h"
#include "MarvellBootUtility.h"

#include "FBFMakeConfig.h"
#include "ProcessLauncher.h"

CFBFMakeConfig::CFBFMakeConfig()
: CTimLib()
{
    Reset();
}


CFBFMakeConfig::~CFBFMakeConfig(void)
{
}

CFBFMakeConfig::CFBFMakeConfig( const CFBFMakeConfig& rhs)
: CTimLib(rhs)
{
    if ( &rhs != this )
    {
        //COPY CONSTRUCTOR
        fbfMakeExe = rhs.fbfMakeExe;
        IniFile    = rhs.IniFile;
        fbfFile    = rhs.fbfFile;
        fbfMakeCmd = rhs.fbfMakeCmd;
        bMode      = rhs.bMode;
        bReset     = rhs.bReset;
        bChng      = rhs.bChng;
        FBFmodeOpt = rhs.FBFmodeOpt;
        FBFResetOpt = rhs.FBFResetOpt;
    }
}

CFBFMakeConfig& CFBFMakeConfig::operator=( const CFBFMakeConfig& rhs)
{
    //ASSIGNMENT OPERATOR
    if (&rhs != this )
    {
        CTimLib::operator=(rhs);

        Reset();

        fbfMakeExe = rhs.fbfMakeExe;
        IniFile    = rhs.IniFile;
        fbfFile    = rhs.fbfFile;
        fbfMakeCmd = rhs.fbfMakeCmd;
        bMode      = rhs.bMode;
        bReset     = rhs.bReset;
        bChng      = rhs.bChng;
        FBFmodeOpt = rhs.FBFmodeOpt;
        FBFResetOpt = rhs.FBFResetOpt;
    }
    return *this;
}

void CFBFMakeConfig::Reset()
{
    bChng = false;
    bMode = false;
    bReset =false;
    fbfMakeCmd = (_T(""));
    fbfMakeExe = (_T(""));
    IniFile =    (_T(""));
    fbfFile =    (_T(""));
    FBFmodeOpt = 0;
    FBFResetOpt = 0;
}

void CFBFMakeConfig::updateCmd(CString str)
{
    if (fbfMakeCmd != str)
    {
        fbfMakeCmd = str;
        bChng = true;
    }
}


void CFBFMakeConfig::setFbfMakeExe(CString str)
{
    if (fbfMakeExe != str)
    {
        fbfMakeExe = str;
        bChng = true;
    }
}


void CFBFMakeConfig::setInifile   (CString str)
{
    if (IniFile != str)
    {
        IniFile = str;
        bChng = true;
    }
}


void CFBFMakeConfig::setFbfFile   (CString str)
{
    if (fbfFile != str)
    {
        fbfFile = str;
        bChng = true;
    }
}


void CFBFMakeConfig::setFMode     (bool opt)
{
    if (bMode != opt)
    {
        bMode = opt;
        bChng = true;
    }
}


void CFBFMakeConfig::setFReset    (bool opt)
{
    if (bReset != opt)
    {
        bReset = opt;
        bChng = true;
    }
}


void  CFBFMakeConfig::setFBFmodeOpt (int val)
{
    if (FBFmodeOpt != val)
    {
        FBFmodeOpt = val;
        bChng = true;
    }
}


void  CFBFMakeConfig::setFBFResetOpt(int val)
{
    if (FBFResetOpt != val)
    {
        FBFResetOpt = val;
        bChng = true;
    }
}

int CFBFMakeConfig::getFBFmodeOpt()
{
    return FBFmodeOpt;
}


int CFBFMakeConfig::getFBFResetOpt()
{
    return FBFResetOpt;
}

CString CFBFMakeConfig::getFbfMakeExe()
{
    return fbfMakeExe;
}

CString CFBFMakeConfig::getFbfMakeCmd()
{
    return fbfMakeCmd;
}

CString CFBFMakeConfig::getInifile()
{
    return IniFile;
}

CString CFBFMakeConfig::getFbfFile()
{
    return fbfFile;
}
    
bool CFBFMakeConfig::getFMode()
{
    return bMode;
}

bool CFBFMakeConfig::getFReset()
{
    return bReset;
}

bool CFBFMakeConfig::getFchng()
{
    return bChng;
}


#if TOOLS_GUI == 1
bool CFBFMakeConfig::SaveState( ofstream& ofs )
{
    stringstream ss;
    
    ss << string((CStringA)(fbfMakeExe)) << endl;
    ss << string((CStringA)(IniFile)) << endl;
    ss << string((CStringA)(fbfFile)) << endl;
    ss << string((CStringA)(fbfMakeCmd)) << endl;
    ss << boolalpha << bMode << endl;
    ss << boolalpha << bReset << endl;
    ss << boolalpha << bChng << endl;
    ss << FBFmodeOpt << endl;
    ss << FBFResetOpt << endl;

    ofs << ss.str();
    ofs.flush();

    return ofs.good(); // SUCCESS
}

bool CFBFMakeConfig::LoadState( ifstream& ifs )
{
    if ( theApp.ProjectVersion >= 0x03030110 )
    {
        string sbuf;

        ::getline( ifs, sbuf );
        fbfMakeExe = sbuf.c_str();

        ::getline( ifs, sbuf );
        IniFile = sbuf.c_str();

        ::getline( ifs, sbuf );
        fbfFile = sbuf.c_str();

        ::getline( ifs, sbuf );
        fbfMakeCmd = sbuf.c_str();

        ::getline( ifs, sbuf ); 
        bMode = ( sbuf == "true" ? true : false );

        ::getline( ifs, sbuf ); 
        bReset = ( sbuf == "true" ? true : false );

        ::getline( ifs, sbuf ); 
        bChng = ( sbuf == "true" ? true : false );

        ::getline( ifs, sbuf ); 
        FBFmodeOpt = atoi(sbuf.c_str());

        ::getline( ifs, sbuf ); 
        FBFResetOpt = atoi(sbuf.c_str());
    }

    return ifs.good() && !ifs.fail(); // success
}
#endif

bool CFBFMakeConfig::Launch()
{
   CProcessLauncher  ProcessLauncher;

    if ( fbfMakeExe.GetLength() == 0 )
    {
        AfxMessageBox( "Enter a path for the FBF_Make tool in the FBF Downloader Interface dialog." );
        return false;
    }

    return ProcessLauncher.Launch( string((CStringA)fbfMakeExe), string((CStringA)fbfMakeCmd) );
}