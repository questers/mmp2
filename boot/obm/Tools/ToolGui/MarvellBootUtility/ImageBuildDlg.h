/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "DownloaderInterface.h"

// CImageBuildDlg dialog

class CImageBuildDlg : public CDialog
{
	DECLARE_DYNAMIC(CImageBuildDlg)

public:
	CImageBuildDlg(CDownloaderInterface& rDownloaderInterface, CWnd* pParent = NULL);   // standard constructor
	virtual ~CImageBuildDlg();

// Dialog Data
	enum { IDD = IDD_IMAGE_BUILD_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	BOOL bTrusted;
	BOOL bVerify;
	BOOL bVerboseMode;
	BOOL bConcatenate;
	BOOL bOneNANDPadding;
	BOOL bHashKey;
	BOOL bPrivateKey;
	BOOL bDigitalSignature;
	BOOL bTimInputFile;
	BOOL bTimOutputFile;
	BOOL bReservedFile;
	BOOL bPartitionFile;
	BOOL bAdditionalOptions;
	BOOL bProcessorType;
	CString sTbbExePath;
	CString sTimDescriptorFilePath;
	CString sHashKeyFilePath;
	CString sPrivateKeyFilePath;
	CString sDigitalSignatureFilePath;
	CString sTimInputFilePath;
	CString sTimOutputFilePath;
	CString sReservedFilePath;
	CString sPartitionFilePath;

	CButton HashKeyFileChk;
	CEdit HashKeyFilePathEdt;
	CButton HashKeyFilePathBrowseBtn;
	CButton PrivateKeyChk;
	CEdit PrivateKeyFilePathEdt;
	CButton PrivateKeyFilePathBrowseBtn;
	CButton DigitalSignatureChk;
	CEdit DigitalSignatureFilePathEdt;
	CButton DigitalSignatureFilePathBrowseBtn;
	CButton TimInputFileChk;
	CEdit TimInputFilePathEdt;
	CButton TimInputFilePathBrowseBtn;
	CButton TimOutputFileChk;
	CEdit TimOutputFilePathEdt;
	CButton TimOutputFilePathBrowseBtn;
	CButton ReservedFileChk;
	CEdit ReservedFilePathEdt;
	CButton ReservedFilePathBrowseBtn;
	CButton PartitionFileChk;
	CEdit PartitionFilePathEdt;
	CButton PartitionFilePathBtn;
	CButton BuildImageBtn;
	CEdit AdditionalOptionsEdt;
	CButton PartitionTableBtn;
	INT iOneNANDPaddingSize;
	CEdit OneNANDPaddingSizeEdt;
	CSpinButtonCtrl OneNANDPaddingSizeSpin;

	afx_msg void OnEnChangeTbbExePath();
	afx_msg void OnBnClickedTbbExeBrowseBtn();
	afx_msg void OnBnClickedTimDescriptorFilePathBrowseBtn();
	afx_msg void OnEnChangeTimDescriptorFilePath();
	afx_msg void OnEnChangeHashKeyFilePath();
	afx_msg void OnBnClickedHashKeyFilePathBrowseBtn();
	afx_msg void OnEnChangePrivateKeyFilePath();
	afx_msg void OnBnClickedPrivateKeyFilePathBrowseBtn();
	afx_msg void OnEnChangeDigitalSignatureFilePath();
	afx_msg void OnBnClickedDigitalSignatureFilePathBrowseBtn();
	afx_msg void OnEnChangeTimInputFilePath();
	afx_msg void OnBnClickedTimInputFilePathBrowseBtn();
	afx_msg void OnEnChangeTimOutputFilePath();
	afx_msg void OnBnClickedTimOutputFilePathBrowseBtn();	
	afx_msg void OnEnChangeReservedFilePath();
	afx_msg void OnBnClickedReservedFilePathBrowseBtn();
	afx_msg void OnBnClickedPartitionFileChk();
	afx_msg void OnEnChangePartitionFilePath();
	afx_msg void OnBnClickedPartitionFilePathBrowseBtn();
	afx_msg void OnBnClickedTrustedChk();
	afx_msg void OnBnClickedVerifyChk();
	afx_msg void OnBnClickedVerboseChk();
	afx_msg void OnBnClickedConcatenate();
	afx_msg void OnBnClickedOnenandPaddingChk();
	afx_msg void OnBnClickedHashKeyFileChk();
	afx_msg void OnBnClickedPrivateKeyChk();
	afx_msg void OnBnClickedDigitalSignatureChk();
	afx_msg void OnBnClickedTimInputFileChk();
	afx_msg void OnBnClickedTimOutputFileChk();
	afx_msg void OnBnClickedReservedFileChk();
	afx_msg void OnBnClickedTbbReset();
	afx_msg void OnBnClickedImageBuildBtn();
	afx_msg void OnEnChangeBuildAdditionalOptionsEdt();
	afx_msg void OnBnClickedBuildAdditionalOptionsChk();
	afx_msg void OnBnClickedBuildProcessorTypeChk();
	afx_msg void OnBnClickedPartitionTableBtn();
	afx_msg void OnBnClickedOk();
	afx_msg void OnDeltaposOnenandPaddingTimSizeSpin(NMHDR *pNMHDR, LRESULT *pResult);
	
public:
	void RefreshState();
	void UpdateChangeMarkerInTitle();

private:
	void UpdateCommandLineText();
	void UpdateControls();

private:
	CImageBuild&			m_ImageBuild;
	CTimDescriptor&			m_TimDescriptor;
	CDownloaderInterface&	m_DownloaderInterface;

	CString				sCommandLineTextStr;  // displays command line text for use in launching WPTPT.exe
	CString				sAdditionalOptions;
};
