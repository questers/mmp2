/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once

#include "DDRInitialization.h"

// CDDRInitializationPkgDlg dialog
class CTimDescriptor;

class CDDRInitializationPkgDlg : public CDialog
{
	DECLARE_DYNAMIC(CDDRInitializationPkgDlg)

public:
	CDDRInitializationPkgDlg(CTimDescriptor& TimDescriptor, CDDRInitialization& DDRInit, CWnd* pParent = NULL);   // standard constructor
	virtual ~CDDRInitializationPkgDlg();

// Dialog Data
	enum { IDD = IDD_DDR_INITIALIZATION_PKG_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedInstructionsBtn();
	afx_msg void OnBnClickedOperationsBtn();
	afx_msg void OnEnChangePkgIdEdt();
	virtual BOOL OnInitDialog();

	CButton InstructionsBtn;
	CButton OperationsBtn;
	CEdit PackageIdEdt;
	CEdit PackageTagEdt;

	void RefreshState();
	void UpdateControls();
	void UpdateChangeMarkerInTitle();

	CDDRInitialization m_DDRInit;
	CTimDescriptor	m_LocalTimDescriptor;

	bool m_bRefreshState;
};
