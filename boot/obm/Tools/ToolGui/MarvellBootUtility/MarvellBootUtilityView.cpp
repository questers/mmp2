/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// MarvellBootUtilityView.cpp : implementation of the CMarvellBootUtilityView class
//

#include "stdafx.h"
#include "MarvellBootUtility.h"

#include "MarvellBootUtilityDoc.h"
#include "MarvellBootUtilityView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMarvellBootUtilityView

IMPLEMENT_DYNCREATE(CMarvellBootUtilityView, CEditView)

BEGIN_MESSAGE_MAP(CMarvellBootUtilityView, CEditView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CMarvellBootUtilityView construction/destruction

CMarvellBootUtilityView::CMarvellBootUtilityView()
{
	// TODO: add construction code here

}

CMarvellBootUtilityView::~CMarvellBootUtilityView()
{
}

BOOL CMarvellBootUtilityView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	return CEditView::PreCreateWindow(cs);
}

// CMarvellBootUtilityView drawing

void CMarvellBootUtilityView::OnDraw(CDC* /*pDC*/)
{
	CMarvellBootUtilityDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CMarvellBootUtilityView printing

BOOL CMarvellBootUtilityView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMarvellBootUtilityView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMarvellBootUtilityView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CMarvellBootUtilityView diagnostics

#ifdef _DEBUG
void CMarvellBootUtilityView::AssertValid() const
{
	CEditView::AssertValid();
}

void CMarvellBootUtilityView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMarvellBootUtilityDoc* CMarvellBootUtilityView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMarvellBootUtilityDoc)));
	return (CMarvellBootUtilityDoc*)m_pDocument;
}
#endif //_DEBUG


