/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "PartitionData.h"
#include "TimLib.h"

#include <string>
#include <list>
#include <iterator>
#include <fstream>
#include <iostream>
#include <strstream>
#include <sstream>
using namespace std;

class CDownloaderInterface;

class CPartitionTable : public CTimLib
{
public:
	CPartitionTable(void);
	virtual ~CPartitionTable(void);

	// copy constructor
	CPartitionTable( const CPartitionTable& rhs );
	// assignment operator
	CPartitionTable& operator=( const CPartitionTable& rhs );

	void PartitionFilePath( string& sFilePath ){ m_PartitionFilePath = sFilePath; m_bNotWritten = m_bChanged = true; }
	string& PartitionFilePath() { return m_PartitionFilePath; }

	void PartitionTableVersion( unsigned int uiVer ) { m_PartitionTableVersion = uiVer; m_bNotWritten = m_bChanged = true; }
	unsigned int PartitionTableVersion() { return m_PartitionTableVersion; }

	t_PartitionList& Partitions() { return m_Partitions; }
	
	void DiscardAll();

	bool SaveState( ofstream& ofs );
	bool LoadState( ifstream& ifs );

	bool IsChanged();
	void Changed( bool bSet ){ m_bChanged = bSet; }
	bool IsNotWritten() { return m_bNotWritten; };
	void NotWritten( bool bSet ) { m_bNotWritten = bSet; }

	string GetText();
#if TOOLS_GUI == 1
	bool ParsePartitionTextFile(CDownloaderInterface& DownloaderInterface);
#endif

private:
	string			m_PartitionFilePath;
	unsigned int	m_PartitionTableVersion;
	t_PartitionList m_Partitions;

	bool m_bChanged;
	bool m_bNotWritten;
};
