/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "EditListCtrl.h"
#include "Key.h"
#include "TimDescriptor.h"

// CKeyDlg dialog

class CKeyDlg : public CDialog
{
	DECLARE_DYNAMIC(CKeyDlg)

public:
	CKeyDlg(CTimDescriptor& TimDescriptor, CWnd* pParent = NULL);   // standard constructor
	virtual ~CKeyDlg();

// Dialog Data
	enum { IDD = IDD_KEY_DLG };

	void Key( CKey& Key ) { m_Key = Key; }
	CKey& Key() { return m_Key; }

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

public:
	CComboBox KeyIdCb;
	CComboBox HashAlgorithmIdCb;
    CComboBox EncryptAlgorithmIdCb;
	CComboBox KeySizeCb;
	CString sKeyTag;
	CHexItemEdit KeyTagEdt;
	CButton PubKeyCompXBtn;
	CButton RsaModulusCompYBtn;

	afx_msg void OnCbnSelchangeKeySizeCb();
	afx_msg void OnCbnSelchangeHashAlgorithmIdCb();
	afx_msg void OnCbnSelchangeKeyidCb();
	afx_msg void OnEnChangeKeyTagEdt();
	afx_msg void OnCbnEditchangeKeyidCb();
	afx_msg void OnBnClickedRsaModulusBtn();
	afx_msg void OnBnClickedPublicKeyExponentBtn();
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
    afx_msg void OnCbnSelchangeEncryptAlgorithmIdCb();
   	afx_msg void OnBnClickedKeyInsertSampleKeyBtn();

public:
	void UpdateChangeMarkerInTitle();

private:
	void RefreshState();
	void UpdateKey();

private:
	CKey	m_Key;
	CTimDescriptor& m_TimDescriptor;
};
