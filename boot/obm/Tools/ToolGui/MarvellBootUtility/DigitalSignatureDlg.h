/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once

#include "DigitalSignature.h"

// CDigitalSignatureDlg dialog

class CDigitalSignatureDlg : public CDialog
{
	DECLARE_DYNAMIC(CDigitalSignatureDlg)

public:
	CDigitalSignatureDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDigitalSignatureDlg();

// Dialog Data
	enum { IDD = IDD_DIGITAL_SIGNATURE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

public:
	CComboBox EncryptAlgorithmCb;
	CComboBox HashAlgorithmIdCb;
	CComboBox ModulusSizeCb;
	CButton PubKeyCompXBtn;
	CButton RsaModulusCompYBtn;
	CButton RsaEcdsaPrivateKeyBtn;
	BOOL bIncludeInTim;

	afx_msg void OnCbnSelchangeModulusSizeCb();
	afx_msg void OnCbnSelchangeHashAlgorithmIdCb();
	afx_msg void OnCbnSelchangeEncryptAlgorithmCb();
	afx_msg void OnBnClickedPublicKeyExponentBtn();
	afx_msg void OnBnClickedRsaModulusBtn();
	afx_msg void OnBnClickedRsaPrivateKeyBtn();
	afx_msg void OnBnClickedIncludeInTim();
	afx_msg void OnBnClickedSampleDsBtn();
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);

public:
	void DigitalSignature( CDigitalSignature& DigitalSignature );
	CDigitalSignature& DigitalSignature() { return m_DigitalSignature; }
	
	void UpdateChangeMarkerInTitle();

private:
	void RefreshState();

private:
	CDigitalSignature	m_DigitalSignature;
};
