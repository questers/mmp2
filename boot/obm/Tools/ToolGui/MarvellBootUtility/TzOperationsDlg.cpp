/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
// TzOperationsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "TimDescriptor.h"
#include "TzOperationsDlg.h"


// CTzOperationsDlg dialog

IMPLEMENT_DYNAMIC(CTzOperationsDlg, CDialog)

CTzOperationsDlg::CTzOperationsDlg(CTimDescriptor& TimDescriptor, t_TzOperationList& TzOperations, CWnd* pParent /*=NULL*/)
	: CDialog(CTzOperationsDlg::IDD, pParent),
	m_LocalTimDescriptor( TimDescriptor ),
	m_TzOperations( TzOperations )
{
	m_OpCbSel = CB_ERR;
	m_bRefreshState = false;
}

CTzOperationsDlg::~CTzOperationsDlg()
{
}

void CTzOperationsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OPERATIONS_LIST, OperationsListCtrl);
	DDX_Control(pDX, IDC_INSERT_BTN, InsertBtn);
	DDX_Control(pDX, IDC_REMOVE_BTN, RemoveBtn);
	DDX_Control(pDX, IDC_MOVE_UP, MoveUpBtn);
	DDX_Control(pDX, IDC_MOVE_DOWN, MoveDownBtn);
	DDX_Control(pDX, IDC_OPERATION_ID_CB, OperationIdCb);
	DDX_Control(pDX, IDC_OPERATION_VALUE_EDT, OperationValueEdt);
	DDX_Control(pDX, IDC_OPERATION_ID_TAG_EDT, OperationIDTagEdt);

	// updates marker in title
	UpdateChangeMarkerInTitle();
	theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CTzOperationsDlg, CDialog)
	ON_BN_CLICKED(IDC_INSERT_BTN, &CTzOperationsDlg::OnBnClickedInsertBtn)
	ON_BN_CLICKED(IDC_REMOVE_BTN, &CTzOperationsDlg::OnBnClickedRemoveBtn)
	ON_BN_CLICKED(IDC_MOVE_UP, &CTzOperationsDlg::OnBnClickedMoveUp)
	ON_BN_CLICKED(IDC_MOVE_DOWN, &CTzOperationsDlg::OnBnClickedMoveDown)
	ON_CBN_SELCHANGE(IDC_OPERATION_ID_CB, &CTzOperationsDlg::OnCbnSelchangeOperationIdCb)
	ON_EN_CHANGE(IDC_OPERATION_VALUE_EDT, &CTzOperationsDlg::OnEnChangeOperationValueEdt)
	ON_BN_CLICKED(IDOK, &CTzOperationsDlg::OnBnClickedOk)
	ON_NOTIFY(LVN_ITEMACTIVATE, IDC_OPERATIONS_LIST, &CTzOperationsDlg::OnLvnItemActivateOperationsList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_OPERATIONS_LIST, &CTzOperationsDlg::OnLvnItemChangedOperationsList)
	ON_NOTIFY(NM_CLICK, IDC_OPERATIONS_LIST, &CTzOperationsDlg::OnNMClickOperationsList)
END_MESSAGE_MAP()


// CTzOperationsDlg message handlers

BOOL CTzOperationsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	OperationsListCtrl.SetExtendedStyle( //LVS_EX_TRACKSELECT |
									  //  LVS_EX_BORDERSELECT |
									    LVS_EX_FULLROWSELECT 
									  | LVS_EX_HEADERDRAGDROP 
									  | LVS_EX_ONECLICKACTIVATE 
									  | LVS_EX_LABELTIP 
									  //| LVS_EX_GRIDLINES
									 );

	OperationsListCtrl.InsertColumn(0,"OpId", LVCFMT_LEFT, 175, -1 );
	OperationsListCtrl.InsertColumn(1,"Value", LVCFMT_LEFT, 80, -1 );

	OperationIdCb.InsertString(0, "NOP" );
	OperationIdCb.InsertString(1, "TZ_CONFIG_ENABLE" );

	OperationValueEdt.m_bHexOnly = true;
	
	RefreshState();
	UpdateControls();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTzOperationsDlg::RefreshState()
{
	m_bRefreshState = true;

	int iSel = OperationsListCtrl.GetSelectionMark();
	OperationsListCtrl.DeleteAllItems();

	int nItem = 0;
	t_TzOperationListIter Iter = m_TzOperations.begin();
	while ( Iter != m_TzOperations.end() )
	{
		OperationsListCtrl.InsertItem( nItem, (*Iter)->m_sOpIdText.c_str() );
		OperationsListCtrl.SetItemData( nItem, (DWORD_PTR)(*Iter) );
		OperationsListCtrl.SetItemText( nItem, 1, ((*Iter)->HexFormattedAscii((*Iter)->m_Value)).c_str() );

		nItem++;
		Iter++;
	}

	if ( iSel != LB_ERR )
		OperationsListCtrl.SetSelectionMark( iSel );

	m_bRefreshState = false;
}

void CTzOperationsDlg::RefreshOperationState()
{
	if ( !m_bRefreshState )
	{
		int iSel = OperationsListCtrl.GetSelectionMark();
		if ( iSel != LB_ERR )
		{
			CTzOperation* pTzOp = (CTzOperation*)OperationsListCtrl.GetItemData( iSel );
			if ( pTzOp )
			{
				if ( CB_ERR != OperationIdCb.SetCurSel(OperationIdCb.FindStringExact( -1, pTzOp->m_sOpIdText.c_str() )))
				{
					m_OpCbSel = OperationIdCb.GetCurSel();
					OperationIdCb.SetItemDataPtr( m_OpCbSel, pTzOp );

					OperationIDTagEdt.SetWindowTextA( pTzOp->HexFormattedAscii( pTzOp->m_OperationId ).c_str() );
					OperationValueEdt.SetWindowTextA( pTzOp->HexFormattedAscii( pTzOp->m_Value).c_str() );
				}
			}

			OperationsListCtrl.SetSelectionMark( iSel );	
		}

		UpdateControls();
	}
}

void CTzOperationsDlg::UpdateControls()
{
	CTzOperation* pTzOp = 0;
	int iSel = OperationsListCtrl.GetSelectionMark();
	if ( iSel != LB_ERR )
		pTzOp = (CTzOperation*)OperationsListCtrl.GetItemData( iSel );
	
	InsertBtn.EnableWindow(TRUE);
	RemoveBtn.EnableWindow( iSel != LB_ERR && OperationsListCtrl.GetItemCount() > 0 );
	MoveUpBtn.EnableWindow( iSel != LB_ERR && OperationsListCtrl.GetItemCount() > 1 );
	MoveDownBtn.EnableWindow( iSel != LB_ERR && OperationsListCtrl.GetItemCount() > 1 );

	OperationIdCb.EnableWindow( iSel != LB_ERR && pTzOp != 0 );
	OperationValueEdt.EnableWindow( iSel != LB_ERR && pTzOp != 0 );

	if ( iSel == LB_ERR && pTzOp == 0 )
	{
		OperationIdCb.SetCurSel( CB_ERR );
		m_OpCbSel = CB_ERR;
		OperationIDTagEdt.SetWindowTextA(_T(""));
		OperationValueEdt.SetWindowTextA(_T(""));
	}
}

void CTzOperationsDlg::OnBnClickedInsertBtn()
{
	UpdateData( TRUE );

	int nItem = OperationsListCtrl.GetSelectionMark();
	if ( nItem == LB_ERR )
		// nothing selected 
		nItem = OperationsListCtrl.GetItemCount();
	else
		nItem++;	// insert after selected item

	t_TzOperationListIter iter = m_TzOperations.begin();
	int ItemPos = nItem-1;
	while ( iter != m_TzOperations.end() && ItemPos >= 0 )
	{
		ItemPos--;
		iter++;
	}

	CTzOperation* pTzOp = new CTzOperation( TZ_NOP, string("NOP") );
	m_TzOperations.insert( iter, pTzOp );

//	OperationsListCtrl.InsertItem( nItem, pTzOp->m_sOpIdText.c_str() );
//	OperationsListCtrl.SetItemData( nItem, (DWORD_PTR)pTzOp );
		
	RefreshState();
	RefreshOperationState();
}

void CTzOperationsDlg::OnBnClickedRemoveBtn()
{
	int nItem = OperationsListCtrl.GetSelectionMark();
	if ( nItem != LB_ERR )
	{
		CTzOperation* pTzOp = (CTzOperation*)OperationsListCtrl.GetItemData( nItem );
		if ( pTzOp )
		{
			m_TzOperations.remove(pTzOp);
			delete pTzOp;
		}
	}

	RefreshState();
	RefreshOperationState();
}

void CTzOperationsDlg::OnBnClickedMoveUp()
{
	int nItem = OperationsListCtrl.GetSelectionMark();
	if ( nItem > LB_ERR )
	{
		CTzOperation* pTzOp = (CTzOperation*)OperationsListCtrl.GetItemData( nItem );
		if ( pTzOp )
		{
			t_TzOperationListIter iter = m_TzOperations.begin();
			while ( iter != m_TzOperations.end() )
			{
				if ( *iter == pTzOp )
				{
					// move it up unless already at top of list
					if ( iter != m_TzOperations.begin() )
					{
						t_TzOperationListIter insertiter = --iter;

						m_TzOperations.remove( pTzOp );
						m_TzOperations.insert( insertiter, pTzOp );
						OperationsListCtrl.SetSelectionMark( --nItem );
					}
					break;
				}
				iter++;
			}
		}
	}

	RefreshState();
	RefreshOperationState();
}

void CTzOperationsDlg::OnBnClickedMoveDown()
{
	int nItem = OperationsListCtrl.GetSelectionMark();
	if ( nItem != LB_ERR )
	{
		CTzOperation* pTzOp = (CTzOperation*)OperationsListCtrl.GetItemData( nItem );
		if ( pTzOp )
		{
			t_TzOperationListIter iter = m_TzOperations.begin();
			while ( iter != m_TzOperations.end() )
			{
				if ( *iter == pTzOp )
				{
					if ( ++iter != m_TzOperations.end() )
					{
						m_TzOperations.remove( pTzOp );
						m_TzOperations.insert( ++iter, pTzOp );
						OperationsListCtrl.SetSelectionMark( ++nItem );
					}
					break;
				}
				iter++;
			}
		}
	}

	RefreshState();
	RefreshOperationState();
}

void CTzOperationsDlg::OnCbnSelchangeOperationIdCb()
{
	UpdateData(TRUE);
	CString sText;
	OperationIdCb.GetWindowTextA( sText );
	CTzOperation* pTzOp = (CTzOperation*)OperationIdCb.GetItemDataPtr( m_OpCbSel );	
	if ( pTzOp )
	{
		string sID = (CStringA)sText;
		pTzOp->SetOperationID( sID );
	}
	m_OpCbSel = OperationIdCb.GetCurSel(); 

	RefreshState();
	RefreshOperationState();
}

void CTzOperationsDlg::OnEnChangeOperationValueEdt()
{
	UpdateData( TRUE );

	int iSel = OperationsListCtrl.GetSelectionMark();
	if ( iSel != LB_ERR )
	{
		CTzOperation* pTzOp = (CTzOperation*)OperationsListCtrl.GetItemData( iSel );
		if ( pTzOp )
		{ 
			CString sVal;
			OperationValueEdt.GetWindowTextA( sVal );
			pTzOp->m_Value = pTzOp->Translate( (CStringA)sVal );
			OperationsListCtrl.SetItemText( iSel, 2, sVal );
		}
	}
}

void CTzOperationsDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	OnOK();
}

void CTzOperationsDlg::UpdateChangeMarkerInTitle()
{
	if ( m_hWnd == 0 )
		return;

	CString sTitle;
	GetWindowTextA( sTitle );
	// add or remove * as appropriate
	if ( m_LocalTimDescriptor.ReservedIsChanged() )
	{
		if ( -1 == sTitle.Find('*') )
			sTitle += "*";
	}
	else
	{
		if ( -1 != sTitle.Find('*') )
			sTitle.Remove('*');
	}
	SetWindowTextA(sTitle);
	UpdateWindow();
}

void CTzOperationsDlg::OnLvnItemActivateOperationsList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	RefreshOperationState();
}

void CTzOperationsDlg::OnLvnItemChangedOperationsList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	RefreshOperationState();
}

void CTzOperationsDlg::OnNMClickOperationsList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;
	UpdateControls();
}
