/******************************************************************************
 *
 *  (C)Copyright 2010-2011 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// InstructionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "InstructionsDlg.h"
#include "TimDescriptor.h"

#pragma warning ( disable : 4996 ) // Disable warning messages

// CInstructionsDlg dialog

IMPLEMENT_DYNAMIC(CInstructionsDlg, CDialog)

CInstructionsDlg::CInstructionsDlg(CTimDescriptor& TimDescriptor, 
                                   t_InstructionList& Instructions, CWnd* pParent /*=NULL*/)
    : CDialog(CInstructionsDlg::IDD, pParent),
    m_LocalTimDescriptor( TimDescriptor ),
    m_Instructions( Instructions )
    , sComment(_T(""))
{
    m_OpCodeCbSel = CB_ERR;
    m_bRefreshState = false;
}

CInstructionsDlg::~CInstructionsDlg()
{
}

void CInstructionsDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_INSTRUCTIONS_LIST, InstructionsListCtrl);
    DDX_Control(pDX, IDC_INSERT_BTN, InsertBtn);
    DDX_Control(pDX, IDC_REMOVE_BTN, RemoveBtn);
    DDX_Control(pDX, IDC_MOVE_UP, MoveUpBtn);
    DDX_Control(pDX, IDC_MOVE_DOWN, MoveDownBtn);
    DDX_Control(pDX, IDC_INSTRUCTION_OP_CODE_CB, InstructionOpCodeCb);
    DDX_Control(pDX, IDC_OP_CODE_VALUE_EDT, InstructionOpCodeEdt);
    DDX_Control(pDX, IDC_NUM_PARAMS_EDT, NumParametersEdt);
    DDX_Control(pDX, IDC_PARAM1_DESC_EDT, Param1DescEdt);
    DDX_Control(pDX, IDC_PARAM2_DESC_EDT, Param2DescEdt);
    DDX_Control(pDX, IDC_PARAM3_DESC_EDT, Param3DescEdt);
    DDX_Control(pDX, IDC_PARAM4_DESC_EDT, Param4DescEdt);
    DDX_Control(pDX, IDC_PARAM5_DESC_EDT, Param5DescEdt);
    DDX_Control(pDX, IDC_PARAM1_VAL_EDT, Param1Edt);
    DDX_Control(pDX, IDC_PARAM2_VAL_EDT, Param2Edt);
    DDX_Control(pDX, IDC_PARAM3_VAL_EDT, Param3Edt);
    DDX_Control(pDX, IDC_PARAM4_VAL_EDT, Param4Edt);
    DDX_Control(pDX, IDC_PARAM5_VAL_EDT, Param5Edt);
    DDX_Text(pDX, IDC_INSTRUCTION_COMMENT, sComment);
    DDX_Control(pDX, IDC_INSTRUCTION_COMMENT, CommentTxt);

    // updates marker in title
    UpdateChangeMarkerInTitle();
    theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CInstructionsDlg, CDialog)
    ON_BN_CLICKED(IDC_INSERT_BTN, &CInstructionsDlg::OnBnClickedInsertBtn)
    ON_BN_CLICKED(IDC_REMOVE_BTN, &CInstructionsDlg::OnBnClickedRemoveBtn)
    ON_BN_CLICKED(IDC_MOVE_UP, &CInstructionsDlg::OnBnClickedMoveUp)
    ON_BN_CLICKED(IDC_MOVE_DOWN, &CInstructionsDlg::OnBnClickedMoveDown)
    ON_CBN_SELCHANGE(IDC_INSTRUCTION_OP_CODE_CB, &CInstructionsDlg::OnCbnSelchangeInstructionOpCodeCb)
    ON_EN_CHANGE(IDC_PARAM1_VAL_EDT, &CInstructionsDlg::OnEnChangeParam1ValEdt)
    ON_EN_CHANGE(IDC_PARAM2_VAL_EDT, &CInstructionsDlg::OnEnChangeParam2ValEdt)
    ON_EN_CHANGE(IDC_PARAM3_VAL_EDT, &CInstructionsDlg::OnEnChangeParam3ValEdt)
    ON_EN_CHANGE(IDC_PARAM4_VAL_EDT, &CInstructionsDlg::OnEnChangeParam4ValEdt)
    ON_EN_CHANGE(IDC_PARAM5_VAL_EDT, &CInstructionsDlg::OnEnChangeParam5ValEdt)
    ON_BN_CLICKED(IDOK, &CInstructionsDlg::OnBnClickedOk)
    ON_NOTIFY(LVN_ITEMACTIVATE, IDC_INSTRUCTIONS_LIST, &CInstructionsDlg::OnLvnItemActivateInstructionsList)
    ON_NOTIFY(LVN_ITEMCHANGED, IDC_INSTRUCTIONS_LIST, &CInstructionsDlg::OnLvnItemChangedInstructionsList)
    ON_NOTIFY(NM_CLICK, IDC_INSTRUCTIONS_LIST, &CInstructionsDlg::OnNMClickInstructionsList)
    ON_EN_CHANGE(IDC_INSTRUCTION_COMMENT, &CInstructionsDlg::OnEnChangeInstructionComment)
END_MESSAGE_MAP()


// CInstructionsDlg message handlers

BOOL CInstructionsDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    InstructionsListCtrl.SetExtendedStyle( //LVS_EX_TRACKSELECT |
                                      //  LVS_EX_BORDERSELECT |
                                        LVS_EX_FULLROWSELECT 
                                      | LVS_EX_HEADERDRAGDROP 
                                      | LVS_EX_ONECLICKACTIVATE 
                                      | LVS_EX_LABELTIP 
                                      | LVS_EX_GRIDLINES
                                     );

    InstructionsListCtrl.InsertColumn(0,"OpCode", LVCFMT_LEFT, 180, -1 );
    InstructionsListCtrl.InsertColumn(1,"Param1", LVCFMT_LEFT, 80, -1 );
    InstructionsListCtrl.InsertColumn(2,"Param2", LVCFMT_LEFT, 80, -1 );
    InstructionsListCtrl.InsertColumn(3,"Param3", LVCFMT_LEFT, 80, -1 );
    InstructionsListCtrl.InsertColumn(4,"Param4", LVCFMT_LEFT, 80, -1 );
    InstructionsListCtrl.InsertColumn(5,"Param5", LVCFMT_LEFT, 80, -1 );
    InstructionsListCtrl.InsertColumn(6,"Comment", LVCFMT_LEFT, 180, -1 );

    InstructionOpCodeCb.InsertString(0, "NOP" ); InstructionOpCodeCb.SetItemData( 0, 0 );
    InstructionOpCodeCb.InsertString(1, "WRITE" ); InstructionOpCodeCb.SetItemData( 1, 2 );
    InstructionOpCodeCb.InsertString(2, "READ" ); InstructionOpCodeCb.SetItemData( 2, 2 );
    InstructionOpCodeCb.InsertString(3, "DELAY" ); InstructionOpCodeCb.SetItemData( 3, 1 );
    InstructionOpCodeCb.InsertString(4, "WAIT_FOR_BIT_SET" ); InstructionOpCodeCb.SetItemData( 4, 3 );
    InstructionOpCodeCb.InsertString(5, "WAIT_FOR_BIT_CLEAR" ); InstructionOpCodeCb.SetItemData( 5, 3 );
    InstructionOpCodeCb.InsertString(6, "AND_VAL" ); InstructionOpCodeCb.SetItemData( 6, 2 );
    InstructionOpCodeCb.InsertString(7, "OR_VAL" ); InstructionOpCodeCb.SetItemData( 7, 2 );
    // new DDR script instructions
    InstructionOpCodeCb.InsertString(8, "SET_BITFIELD" ); InstructionOpCodeCb.SetItemData( 8, 3 );
    InstructionOpCodeCb.InsertString(9, "WAIT_FOR_BIT_PATTERN" ); InstructionOpCodeCb.SetItemData( 9, 4 );
    InstructionOpCodeCb.InsertString(10, "TEST_IF_ZERO_AND_SET" ); InstructionOpCodeCb.SetItemData( 10, 5 );
    InstructionOpCodeCb.InsertString(11, "TEST_IF_NOT_ZERO_AND_SET" ); InstructionOpCodeCb.SetItemData( 11, 5 );
    InstructionOpCodeCb.InsertString(12, "LOAD_SM_ADDR" ); InstructionOpCodeCb.SetItemData( 12, 2 );
    InstructionOpCodeCb.InsertString(13, "LOAD_SM_VAL" ); InstructionOpCodeCb.SetItemData( 13, 2 );
    InstructionOpCodeCb.InsertString(14, "STORE_SM_ADDR" ); InstructionOpCodeCb.SetItemData( 14, 2 );
    InstructionOpCodeCb.InsertString(15, "MOV_SM_SM" ); InstructionOpCodeCb.SetItemData( 15, 2 );
    InstructionOpCodeCb.InsertString(16, "RSHIFT_SM_VAL" ); InstructionOpCodeCb.SetItemData( 16, 2 );
    InstructionOpCodeCb.InsertString(17, "LSHIFT_SM_VAL" ); InstructionOpCodeCb.SetItemData( 17, 2 );
    InstructionOpCodeCb.InsertString(18, "AND_SM_VAL" ); InstructionOpCodeCb.SetItemData( 18, 2 );
    InstructionOpCodeCb.InsertString(19, "OR_SM_VAL" ); InstructionOpCodeCb.SetItemData( 19, 2 );
    InstructionOpCodeCb.InsertString(20, "OR_SM_SM" ); InstructionOpCodeCb.SetItemData( 20, 2 );
    InstructionOpCodeCb.InsertString(21, "AND_SM_SM" ); InstructionOpCodeCb.SetItemData( 21, 2 );
    InstructionOpCodeCb.InsertString(22, "TEST_SM_IF_ZERO_AND_SET" ); InstructionOpCodeCb.SetItemData( 22, 5 );
    InstructionOpCodeCb.InsertString(23, "TEST_SM_IF_NOT_ZERO_AND_SET" ); InstructionOpCodeCb.SetItemData( 23, 5 );

    Param1Edt.m_bHexOnly = true;
    Param2Edt.m_bHexOnly = true;
    Param3Edt.m_bHexOnly = true;
    Param4Edt.m_bHexOnly = true;
    Param5Edt.m_bHexOnly = true;

    RefreshState();
    UpdateControls();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void CInstructionsDlg::RefreshState()
{
    m_bRefreshState = true;

    int iSel = InstructionsListCtrl.GetSelectionMark();
    InstructionsListCtrl.DeleteAllItems();

    int nItem = 0;
    t_InstructionListIter Iter = m_Instructions.begin();
    while ( Iter != m_Instructions.end() )
    {
        InstructionsListCtrl.InsertItem( nItem, (*Iter)->m_InstructionText.c_str() );
        InstructionsListCtrl.SetItemData( nItem, (DWORD_PTR)(*Iter) );
        
        if ( (*Iter)->m_NumParamsUsed > 0 )
            InstructionsListCtrl.SetItemText( nItem, 1, (*Iter)->HexFormattedAscii((*Iter)->m_ParamValues[0]).c_str() );

        if ( (*Iter)->m_NumParamsUsed > 1 )
            InstructionsListCtrl.SetItemText( nItem, 2, (*Iter)->HexFormattedAscii((*Iter)->m_ParamValues[1]).c_str() );

        if ( (*Iter)->m_NumParamsUsed > 2 )
            InstructionsListCtrl.SetItemText( nItem, 3, (*Iter)->HexFormattedAscii((*Iter)->m_ParamValues[2]).c_str() );

        if ( (*Iter)->m_NumParamsUsed > 3 )
            InstructionsListCtrl.SetItemText( nItem, 4, (*Iter)->HexFormattedAscii((*Iter)->m_ParamValues[3]).c_str() );

        if ( (*Iter)->m_NumParamsUsed > 4 )
            InstructionsListCtrl.SetItemText( nItem, 5, (*Iter)->HexFormattedAscii((*Iter)->m_ParamValues[4]).c_str() );

        InstructionsListCtrl.SetItemText( nItem, 6, (*Iter)->m_sComment.c_str() );

        nItem++;
        Iter++;
    }

    if ( iSel != LB_ERR )
        InstructionsListCtrl.SetSelectionMark( iSel );

    m_bRefreshState = false;
}

void CInstructionsDlg::RefreshInstState()
{
    if ( !m_bRefreshState )
    {
        int iSel = InstructionsListCtrl.GetSelectionMark();
        if ( iSel != LB_ERR )
        {
            CInstruction* pInst = (CInstruction*)InstructionsListCtrl.GetItemData( iSel );
            if ( pInst != 0 )
            {
                if ( CB_ERR != InstructionOpCodeCb.SetCurSel(InstructionOpCodeCb.FindStringExact( -1, pInst->m_InstructionText.c_str() )))
                {
                    m_OpCodeCbSel = InstructionOpCodeCb.GetCurSel();
                    InstructionOpCodeCb.SetItemDataPtr( m_OpCodeCbSel, pInst );

                    InstructionOpCodeEdt.SetWindowTextA( pInst->HexFormattedAscii( pInst->m_InstructionOpCode).c_str() );
                    char Numparams[20]={0};
                    NumParametersEdt.SetWindowTextA( _itoa(pInst->m_NumParamsUsed,Numparams,10) );
                    CommentTxt.SetWindowTextA( pInst->m_sComment.c_str() );

                    if ( pInst->m_NumParamsUsed > 0 )
                    {
                        Param1DescEdt.SetWindowTextA( pInst->m_ParamNames[0].c_str() );
                        Param1Edt.SetWindowTextA( pInst->HexFormattedAscii( pInst->m_ParamValues[0]).c_str() );
                    }
                    else
                    {
                        Param1DescEdt.SetWindowTextA(_T(""));
                        Param1Edt.SetWindowTextA(_T(""));
                    }

                    if ( pInst->m_NumParamsUsed > 1 )
                    {
                        Param2DescEdt.SetWindowTextA( pInst->m_ParamNames[1].c_str() );
                        Param2Edt.SetWindowTextA( pInst->HexFormattedAscii( pInst->m_ParamValues[1]).c_str() );
                    }
                    else
                    {
                        Param2DescEdt.SetWindowTextA(_T(""));
                        Param2Edt.SetWindowTextA(_T(""));
                    }

                    if ( pInst->m_NumParamsUsed > 2 )
                    {
                        Param3DescEdt.SetWindowTextA( pInst->m_ParamNames[2].c_str() );
                        Param3Edt.SetWindowTextA( pInst->HexFormattedAscii( pInst->m_ParamValues[2]).c_str() );
                    }
                    else
                    {
                        Param3DescEdt.SetWindowTextA(_T(""));
                        Param3Edt.SetWindowTextA(_T(""));
                    }
                    if ( pInst->m_NumParamsUsed > 3 )
                    {
                        Param4DescEdt.SetWindowTextA( pInst->m_ParamNames[3].c_str() );
                        Param4Edt.SetWindowTextA( pInst->HexFormattedAscii( pInst->m_ParamValues[3]).c_str() );
                    }
                    else
                    {
                        Param4DescEdt.SetWindowTextA(_T(""));
                        Param4Edt.SetWindowTextA(_T(""));
                    }
                    if ( pInst->m_NumParamsUsed > 4 )
                    {
                        Param5DescEdt.SetWindowTextA( pInst->m_ParamNames[4].c_str() );
                        Param5Edt.SetWindowTextA( pInst->HexFormattedAscii( pInst->m_ParamValues[4]).c_str() );
                    }
                    else
                    {
                        Param5DescEdt.SetWindowTextA(_T(""));
                        Param5Edt.SetWindowTextA(_T(""));
                    }
                }
            }

            InstructionsListCtrl.SetSelectionMark( iSel );
        }

        UpdateControls();
    }
}


void CInstructionsDlg::UpdateControls()
{
    CInstruction* pInst = 0;
    int iSel = InstructionsListCtrl.GetSelectionMark();
    if ( iSel != LB_ERR )
        pInst = (CInstruction*)InstructionsListCtrl.GetItemData( iSel );
    
    InstructionOpCodeCb.EnableWindow( iSel != LB_ERR && pInst != 0);
    InstructionOpCodeEdt.EnableWindow( iSel != LB_ERR && pInst != 0);
    Param1Edt.EnableWindow( iSel != LB_ERR && pInst != 0 && pInst->m_NumParamsUsed > 0 );
    Param2Edt.EnableWindow( iSel != LB_ERR && pInst != 0 && pInst->m_NumParamsUsed > 1 );
    Param3Edt.EnableWindow( iSel != LB_ERR && pInst != 0 && pInst->m_NumParamsUsed > 2 );
    Param4Edt.EnableWindow( iSel != LB_ERR && pInst != 0 && pInst->m_NumParamsUsed > 3 );
    Param5Edt.EnableWindow( iSel != LB_ERR && pInst != 0 && pInst->m_NumParamsUsed > 4 );

    InsertBtn.EnableWindow(TRUE);
    RemoveBtn.EnableWindow( iSel != LB_ERR && InstructionsListCtrl.GetItemCount() > 0 );
    MoveUpBtn.EnableWindow( iSel != LB_ERR && InstructionsListCtrl.GetItemCount() > 1 );
    MoveDownBtn.EnableWindow( iSel != LB_ERR && InstructionsListCtrl.GetItemCount() > 1 );

    CommentTxt.EnableWindow( iSel != LB_ERR );

    if ( iSel == LB_ERR && pInst == 0 )
    {
        InstructionOpCodeCb.SetCurSel( CB_ERR );
        m_OpCodeCbSel = CB_ERR;
        InstructionOpCodeEdt.SetWindowTextA(_T(""));
        NumParametersEdt.SetWindowTextA(_T(""));
        Param1DescEdt.SetWindowTextA(_T(""));
        Param1Edt.SetWindowTextA(_T(""));
        Param2DescEdt.SetWindowTextA(_T(""));
        Param2Edt.SetWindowTextA(_T(""));
        Param3DescEdt.SetWindowTextA(_T(""));
        Param3Edt.SetWindowTextA(_T(""));
        Param4DescEdt.SetWindowTextA(_T(""));
        Param4Edt.SetWindowTextA(_T(""));
        Param5DescEdt.SetWindowTextA(_T(""));
        Param5Edt.SetWindowTextA(_T(""));
    }
}

void CInstructionsDlg::OnBnClickedInsertBtn()
{
    UpdateData( TRUE );

    int nItem = InstructionsListCtrl.GetSelectionMark();
    if ( nItem == LB_ERR )
        // nothing selected 
        nItem = InstructionsListCtrl.GetItemCount();
    else
        nItem++;	// insert after selected item

    t_InstructionList& Instructions = m_Instructions;
    t_InstructionListIter iter = Instructions.begin();
    int ItemPos = nItem-1;
    while ( iter != Instructions.end() && ItemPos >= 0 )
    {
        ItemPos--;
        iter++;
    }

    string sNULL("");
    CInstruction* pInst = new CInstruction( (INSTRUCTION_OP_CODE_SPEC_T)INSTR_NOP, string("NOP"), 0, sNULL, sNULL, sNULL, sNULL, sNULL );
    m_Instructions.insert( iter, pInst );

//	InstructionsListCtrl.InsertItem( nItem, pInst->m_InstructionText.c_str() );
//	InstructionsListCtrl.SetItemData( nItem, (DWORD_PTR)pInst );
        
    RefreshState();
    if ( nItem != LB_ERR )
        InstructionsListCtrl.SetSelectionMark( nItem );
    RefreshInstState();
}

void CInstructionsDlg::OnBnClickedRemoveBtn()
{
    int nItem = InstructionsListCtrl.GetSelectionMark();
    if ( nItem != LB_ERR )
    {
        CInstruction* pInst = (CInstruction*)InstructionsListCtrl.GetItemData( nItem );
        if ( pInst )
        {
            m_Instructions.remove(pInst);
            delete pInst;
        }
    }

    RefreshState();
    RefreshInstState();
}

void CInstructionsDlg::OnBnClickedMoveUp()
{
    int nItem = InstructionsListCtrl.GetSelectionMark();
    if ( nItem > LB_ERR )
    {
        CInstruction* pInst = (CInstruction*)InstructionsListCtrl.GetItemData( nItem );
        if ( pInst )
        {
            t_InstructionList& Instructions = m_Instructions;
            t_InstructionListIter iter = Instructions.begin();
            while( iter != Instructions.end() )
            {
                if ( *iter == pInst )
                {
                    // move it up unless already at top of list
                    if ( iter != Instructions.begin() )
                    {
                        t_InstructionListIter insertiter = --iter;
                    
                        Instructions.remove( pInst );
                        Instructions.insert( insertiter, pInst );
                        InstructionsListCtrl.SetSelectionMark( --nItem );
                    }
                    break;
                }
                iter++;
            }
        }
    }

    RefreshState();
    RefreshInstState();
}

void CInstructionsDlg::OnBnClickedMoveDown()
{
    int nItem = InstructionsListCtrl.GetSelectionMark();
    if ( nItem != LB_ERR )
    {
        CInstruction* pInst = (CInstruction*)InstructionsListCtrl.GetItemData( nItem );
        if ( pInst )
        {
            t_InstructionList& Instructions = m_Instructions;
            t_InstructionListIter iter = Instructions.begin();
            while( iter != Instructions.end() )
            {
                if ( *iter == pInst )
                {
                    // move it down unless already at end of list
                    if ( ++iter != Instructions.end() )
                    {
                        Instructions.remove( pInst );
                        Instructions.insert( ++iter, pInst );
                        InstructionsListCtrl.SetSelectionMark( ++nItem );
                    }
                    break;
                }
                iter++;
            }
        }
    }

    RefreshState();
    RefreshInstState();
}

void CInstructionsDlg::OnCbnSelchangeInstructionOpCodeCb()
{
    UpdateData(TRUE);
    CString sText;
    InstructionOpCodeCb.GetWindowTextA( sText );
    CInstruction* pInst = (CInstruction*)InstructionOpCodeCb.GetItemDataPtr( m_OpCodeCbSel );	
    if ( pInst )
    {
        string sType = (CStringA)sText;
        pInst->SetInstructionType( sType );
    }
    m_OpCodeCbSel = InstructionOpCodeCb.GetCurSel(); 
    
    RefreshState();
    RefreshInstState();
}

void CInstructionsDlg::OnEnChangeParam1ValEdt()
{
    UpdateData( TRUE );

    int iSel = InstructionsListCtrl.GetSelectionMark();
    if ( iSel != LB_ERR )
    {
        CInstruction* pInst = (CInstruction*)InstructionsListCtrl.GetItemData( iSel );
        if ( pInst && pInst->m_NumParamsUsed > 0 )
        { 
            CString sVal;
            Param1Edt.GetWindowTextA( sVal );
            pInst->m_ParamValues[0] = pInst->Translate( (CStringA)sVal );
            InstructionsListCtrl.SetItemText( iSel, 1, sVal );
        }
    }
}

void CInstructionsDlg::OnEnChangeParam2ValEdt()
{
    UpdateData( TRUE );

    int iSel = InstructionsListCtrl.GetSelectionMark();
    if ( iSel != LB_ERR )
    {
        CInstruction* pInst = (CInstruction*)InstructionsListCtrl.GetItemData( iSel );
        if ( pInst && pInst->m_NumParamsUsed > 1 )
        { 
            CString sVal;
            Param2Edt.GetWindowTextA( sVal );
            pInst->m_ParamValues[1] = pInst->Translate( (CStringA)sVal );
            InstructionsListCtrl.SetItemText( iSel, 2, sVal );
        }
    }
}

void CInstructionsDlg::OnEnChangeParam3ValEdt()
{
    UpdateData( TRUE );

    int iSel = InstructionsListCtrl.GetSelectionMark();
    if ( iSel != LB_ERR )
    {
        CInstruction* pInst = (CInstruction*)InstructionsListCtrl.GetItemData( iSel );
        if ( pInst && pInst->m_NumParamsUsed > 2 )
        { 
            CString sVal;
            Param3Edt.GetWindowTextA( sVal );
            pInst->m_ParamValues[2] = pInst->Translate( (CStringA)sVal );
            InstructionsListCtrl.SetItemText( iSel, 3, sVal );
        }
    }
}

void CInstructionsDlg::OnEnChangeParam4ValEdt()
{
    UpdateData( TRUE );

    int iSel = InstructionsListCtrl.GetSelectionMark();
    if ( iSel != LB_ERR )
    {
        CInstruction* pInst = (CInstruction*)InstructionsListCtrl.GetItemData( iSel );
        if ( pInst && pInst->m_NumParamsUsed > 3 )
        { 
            CString sVal;
            Param4Edt.GetWindowTextA( sVal );
            pInst->m_ParamValues[3] = pInst->Translate( (CStringA)sVal );
            InstructionsListCtrl.SetItemText( iSel, 4, sVal );
        }
    }
}

void CInstructionsDlg::OnEnChangeParam5ValEdt()
{
    UpdateData( TRUE );

    int iSel = InstructionsListCtrl.GetSelectionMark();
    if ( iSel != LB_ERR )
    {
        CInstruction* pInst = (CInstruction*)InstructionsListCtrl.GetItemData( iSel );
        if ( pInst && pInst->m_NumParamsUsed > 4 )
        { 
            CString sVal;
            Param5Edt.GetWindowTextA( sVal );
            pInst->m_ParamValues[4] = pInst->Translate( (CStringA)sVal );
            InstructionsListCtrl.SetItemText( iSel, 5, sVal );
        }
    }
}

void CInstructionsDlg::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    OnOK();
}

void CInstructionsDlg::UpdateChangeMarkerInTitle()
{
    if ( m_hWnd == 0 )
        return;

    CString sTitle;
    GetWindowTextA( sTitle );
    // add or remove * as appropriate
    if ( m_LocalTimDescriptor.ReservedIsChanged() )
    {
        if ( -1 == sTitle.Find('*') )
            sTitle += "*";
    }
    else
    {
        if ( -1 != sTitle.Find('*') )
            sTitle.Remove('*');
    }
    SetWindowTextA(sTitle);
    UpdateWindow();
}

void CInstructionsDlg::OnLvnItemActivateInstructionsList(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    // TODO: Add your control notification handler code here
    *pResult = 0;

    RefreshInstState();
}

void CInstructionsDlg::OnLvnItemChangedInstructionsList(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    // TODO: Add your control notification handler code here
    *pResult = 0;

    RefreshInstState();
}



void CInstructionsDlg::OnNMClickInstructionsList(NMHDR *pNMHDR, LRESULT *pResult)
{
    // TODO: Add your control notification handler code here
    *pResult = 0;

    UpdateControls();
}


void CInstructionsDlg::OnEnChangeInstructionComment()
{
    // TODO:  If this is a RICHEDIT control, the control will not
    // send this notification unless you override the CDialog::OnInitDialog()
    // function and call CRichEditCtrl().SetEventMask()
    // with the ENM_CHANGE flag ORed into the mask.

    // TODO:  Add your control notification handler code here
    UpdateData( TRUE );

    int iSel = InstructionsListCtrl.GetSelectionMark();
    if ( iSel != LB_ERR )
    {
        CInstruction* pInst = (CInstruction*)InstructionsListCtrl.GetItemData( iSel );
        if ( pInst )
        { 
            CString sVal;
            CommentTxt.GetWindowTextA( sVal );
            pInst->m_sComment = (CStringA)sVal;
            InstructionsListCtrl.SetItemText( iSel, 6, sVal );
        }
    }
}
