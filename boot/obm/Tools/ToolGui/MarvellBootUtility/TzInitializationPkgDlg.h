/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once

#include "TzInitialization.h"

// CTzInitializationPkgDlg dialog
class CTimDescriptor;

class CTzInitializationPkgDlg : public CDialog
{
	DECLARE_DYNAMIC(CTzInitializationPkgDlg)

public:
	CTzInitializationPkgDlg(CTimDescriptor& TimDescriptor, CTzInitialization& TzInit, CWnd* pParent = NULL);   // standard constructor
	virtual ~CTzInitializationPkgDlg();

// Dialog Data
	enum { IDD = IDD_TZ_INITIALIZATION_PKG_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedInstructionsBtn();
	afx_msg void OnBnClickedOperationsBtn();
	afx_msg void OnEnChangePkgIdEdt();
	virtual BOOL OnInitDialog();

	CButton InstructionsBtn;
	CButton OperationsBtn;
	CEdit PackageIdEdt;
	CEdit PackageTagEdt;

	void RefreshState();
	void UpdateControls();
	void UpdateChangeMarkerInTitle();

	CTzInitialization m_TzInit;
	CTimDescriptor	m_LocalTimDescriptor;

	bool m_bRefreshState;
};
