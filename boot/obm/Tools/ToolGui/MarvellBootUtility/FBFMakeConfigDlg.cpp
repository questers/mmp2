/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
// FBF_Make_DLGDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"

#include "FBFMakeConfigDlg.h"

#include <string>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
IMPLEMENT_DYNAMIC(CFBFMakeConfigDlg, CDialog)

// CFBFMakeConfigDlg dialog
CFBFMakeConfigDlg::CFBFMakeConfigDlg(CFBFMakeConfig& FBFmakecfg, CWnd* pParent /*=NULL*/)
    : cpFBFmakecfg( FBFmakecfg ), CDialog(CFBFMakeConfigDlg::IDD, pParent)
{
    sFBFMakePath = _T("");
    sFBFmake_ini_file = _T("");
    sFBFfile = _T("");
    bFBFmakeMode = FALSE;
    bFBFmakeReset = FALSE;
    sFBFmode = _T("");
    sFBFreset = _T("");
    ModeOpt = 0;
    ResetOpt = 0;
}

CFBFMakeConfigDlg::~CFBFMakeConfigDlg()
{
}

/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_FBF_MAKE_EXE_PATH, sFBFMakePath);
    DDX_Text(pDX, IDC_INI_PATH, sFBFmake_ini_file);
    DDX_Control(pDX, IDC_FBF_MAKE_EXE_PATH, mFBFmake_path);
    DDX_Control(pDX, IDC_INI_PATH, mFBFmake_ini_file);
    DDX_Text(pDX, IDC_FBF_FILE_PATH, sFBFfile);
    DDX_Control(pDX, IDC_FBF_FILE_PATH, mFBFfile);
    DDX_Control(pDX, IDC_FBF_MAKE_COMMAND_LINE_TXT, mFBFcmd);
    DDX_Control(pDX, IDC_FBF_MAKE_MODE, mFBFmakeMode);
    DDX_Check(pDX, IDC_FBF_MAKE_MODE, bFBFmakeMode);
    DDX_Control(pDX, IDC_FBF_MAKE_RESET, mFBFmakeReset);
    DDX_Check(pDX, IDC_FBF_MAKE_RESET, bFBFmakeReset);
    DDX_Control(pDX, IDC_FBF_MODE_CHOICES, cbFBFmode);
    DDX_CBString(pDX, IDC_FBF_MODE_CHOICES, sFBFmode);
    DDX_Control(pDX, IDC_FBF_RESET_CHOICES, cbFBFreset);
    DDX_CBString(pDX, IDC_FBF_RESET_CHOICES, sFBFreset);
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
BEGIN_MESSAGE_MAP(CFBFMakeConfigDlg, CDialog)
    //}}AFX_MSG_MAP
    ON_EN_CHANGE(IDC_FBF_MAKE_EXE_PATH, &CFBFMakeConfigDlg::OnEnChangeFbfMakeExePath)
    ON_EN_CHANGE(IDC_INI_PATH, &CFBFMakeConfigDlg::OnEnChangeIniPath)
    ON_BN_CLICKED(IDC_FBF_MAKE_EXE_BROWSE_BTN, &CFBFMakeConfigDlg::OnBnClickedFbfMakeExeBrowseBtn)
    ON_BN_CLICKED(IDC_INI_BROWSE_BTN, &CFBFMakeConfigDlg::OnBnClickedIniBrowseBtn)
    ON_BN_CLICKED(IDOK, &CFBFMakeConfigDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CFBFMakeConfigDlg::OnBnClickedCancel)
    ON_EN_CHANGE(IDC_INI_PATH, &CFBFMakeConfigDlg::OnEnChangeIniPath)
    ON_EN_CHANGE(IDC_FBF_FILE_PATH, &CFBFMakeConfigDlg::OnEnChangeFbfPath)
    ON_BN_CLICKED(IDC_FBF_FILE_BROWSE_BTN, &CFBFMakeConfigDlg::OnBnClickedFbfBrowseBtn)
    ON_BN_CLICKED(IDC_FBF_MAKE_MODE, &CFBFMakeConfigDlg::OnBnClickedFbfMakeMode)
    ON_BN_CLICKED(IDC_FBF_MAKE_RESET, &CFBFMakeConfigDlg::OnBnClickedFbfMakeReset)
    ON_BN_CLICKED(IDC_FBF_MAKE_LAUNCH, &CFBFMakeConfigDlg::OnBnClickedFbfMakeLaunch)
    ON_CBN_SELCHANGE(IDC_FBF_MODE_CHOICES, &CFBFMakeConfigDlg::OnCbnSelchangeFbfModeChoices)
    ON_CBN_SELCHANGE(IDC_FBF_RESET_CHOICES, &CFBFMakeConfigDlg::OnCbnSelchangeFbfResetChoices)
END_MESSAGE_MAP()

/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
// CFBFMakeConfigDlg message handlers

BOOL CFBFMakeConfigDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    //Custom initialization
    initDlgVals();

    return TRUE;  // return TRUE  unless you set the focus to a control
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::initDlgVals()
{
    cmdline = cpFBFmakecfg.getFbfMakeCmd();
    sFBFMakePath = cpFBFmakecfg.getFbfMakeExe();
    sFBFmake_ini_file    = cpFBFmakecfg.getInifile();
    sFBFfile    = cpFBFmakecfg.getFbfFile();
    
    initComboBoxes();

    if (sFBFMakePath == "")
        sFBFMakePath = _T("<FBF_MAKE>");

    if (sFBFmake_ini_file == "")
        sFBFmake_ini_file = _T("<INI file>");
    iniArg = _T("-i ") + sFBFmake_ini_file;

    if (sFBFfile == "")
        sFBFfile = _T("<FBF_FILE>");
    fbfArg = _T("-f ") + sFBFfile;

    cmdline.Format(_T ("%s %s %s %s"), iniArg, fbfArg, sFBFmakeMode, sFBFmakeReset);

    mFBFcmd.SetWindowTextA(cmdline);
    UpdateData(false);
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::initComboBoxes()
{
    int idx = -1;
    cbFBFmode.InsertString(++idx, "Normal Mode");cbFBFmode.SetItemData(idx, 0);
    cbFBFmode.InsertString(++idx, "Low Level Format Mode");cbFBFmode.SetItemData(idx, 1);
    cbFBFmode.InsertString(++idx, "Production Mode");cbFBFmode.SetItemData(idx, 2);
    cbFBFmode.InsertString(++idx, "Erase All Flash");cbFBFmode.SetItemData(idx, 8);

    idx = -1;
    cbFBFreset.InsertString(++idx, "No Target Reset");cbFBFreset.SetItemData(idx, 0);
    cbFBFreset.InsertString(++idx, "Target Reset");cbFBFreset.SetItemData(idx, 1);

    for ( int i = 0; i < cbFBFmode.GetCount(); i++ )
    {
        if ( cbFBFmode.GetItemData( i ) == cpFBFmakecfg.getFBFmodeOpt() )
            cbFBFmode.SetCurSel( i );
    }

    for ( int i = 0; i < cbFBFreset.GetCount(); i++ )
    {
        if ( cbFBFreset.GetItemData( i ) == cpFBFmakecfg.getFBFResetOpt() )
            cbFBFreset.SetCurSel( i );
    }

    if (cpFBFmakecfg.getFMode())
        sFBFmakeMode.Format(_T (" -m %d"), cpFBFmakecfg.getFBFmodeOpt()); 
    else
        sFBFmakeMode.Format(_T (" [-m ModeOpt] "));

    if (cpFBFmakecfg.getFReset())
        sFBFmakeReset.Format(_T (" -r %d"), cpFBFmakecfg.getFBFResetOpt());
    else
        sFBFmakeReset.Format(_T (" [-r ResetOpt] "));

    bFBFmakeMode = cpFBFmakecfg.getFMode();
    bFBFmakeReset = cpFBFmakecfg.getFReset();
    setMode();
    setReset();
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::update_fbf_cmd()
{
    CString FBFmakeCMD;

    UpdateData( TRUE );

    if (sFBFMakePath == "")
        FBFmakeCMD = _T("<FBF_MAKE>");
    else
        FBFmakeCMD = sFBFMakePath;

    if (sFBFmake_ini_file != "")
        iniArg = _T("-i ") + sFBFmake_ini_file;
    else
        iniArg = _T("-i <INI file>");

    if (sFBFfile != "")
        fbfArg = _T("-f ") + sFBFfile;
    else
        fbfArg = _T("-f <FBF_FILE>");

    cmdline.Format(_T ("%s %s %s %s"), iniArg, fbfArg, sFBFmakeMode, sFBFmakeReset);
    mFBFcmd.SetWindowTextA(cmdline);
    
    //update data
    cpFBFmakecfg.updateCmd(cmdline);
    UpdateData( FALSE);
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnEnChangeFbfMakeExePath()
{
    //update data
    setMode();
    setReset();
    update_fbf_cmd();
    if (sFBFMakePath == "")
        cpFBFmakecfg.setFbfMakeExe(_T("<FBF_MAKE>"));
    else
        cpFBFmakecfg.setFbfMakeExe(sFBFMakePath);
}

/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnEnChangeIniPath()
{
    //update data
    setMode();
    setReset();
    update_fbf_cmd();
    if (sFBFmake_ini_file == "")
        cpFBFmakecfg.setInifile(_T("-i <INI file>"));
    else
        cpFBFmakecfg.setInifile(sFBFmake_ini_file);
}

/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnEnChangeFbfPath()
{
    //update data
//	UpdateData( TRUE );

    setMode();
    setReset();
    update_fbf_cmd();
    if (sFBFfile == "")
        cpFBFmakecfg.setFbfFile(_T("<FBF_MAKE>"));
    else
        cpFBFmakecfg.setFbfFile(sFBFfile);
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnBnClickedFbfMakeExeBrowseBtn()
{
    UpdateData( TRUE );
    CString sExt;
    CString sType;
    sExt = _T("*.exe");
    sType = _T("FBF_make.4.57 (*.exe)|*.exe|All Files (*.*)|*.*||");

    CFileDialog Dlg( TRUE, sExt, NULL, 
                     OFN_FILEMUSTEXIST | 
                     OFN_PATHMUSTEXIST,
                     sType );
    if ( Dlg.DoModal() == IDOK )
    {
        sFBFMakePath = Dlg.GetPathName();
    }

    mFBFmake_path.SetWindowTextA(sFBFMakePath);
    cpFBFmakecfg.setFbfMakeExe(sFBFMakePath);	//update the data

    setMode();
    setReset();

    cmdline.Format(_T ("%s %s %s %s"), iniArg, fbfArg, sFBFmakeMode, sFBFmakeReset);
    mFBFcmd.SetWindowTextA(cmdline);
    cpFBFmakecfg.updateCmd(cmdline);	//update data

    SetFocus();
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnBnClickedIniBrowseBtn()
{
    UpdateData( TRUE );
    CString sExt;
    CString sType;
    CString iniSwtch(_T("-i "));
    sExt = _T("*.ini");
    sType = _T("INIFile (*.ini)|*.ini|All Files (*.*)|*.*||");

    CFileDialog Dlg( TRUE, sExt, NULL, 
                     OFN_FILEMUSTEXIST | 
                     OFN_PATHMUSTEXIST,
                     sType );
    if ( Dlg.DoModal() == IDOK )
    {
        sFBFmake_ini_file = Dlg.GetPathName();
    }

    mFBFmake_ini_file.SetWindowTextA(sFBFmake_ini_file);
    cpFBFmakecfg.setInifile(sFBFmake_ini_file);	//update data

    iniArg = iniSwtch + sFBFmake_ini_file;
    setMode();
    setReset();

    cmdline.Format(_T ("%s %s %s %s"), iniArg, fbfArg, sFBFmakeMode, sFBFmakeReset);
    mFBFcmd.SetWindowTextA(cmdline);
    cpFBFmakecfg.updateCmd(cmdline);	//update data

    SetFocus();
}

/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnBnClickedFbfBrowseBtn()
{
    UpdateData( TRUE );
    CString sExt;
    CString sType;
    CString fbfSwtch(_T("-f "));
    sExt = _T("*.fbf");
    sType = _T("FBFfile (*.fbf)|*.fbf|All Files (*.*)|*.*||");

    CFileDialog Dlg( TRUE, sExt, NULL, 
//					 OFN_FILEMUSTEXIST | 
                     OFN_PATHMUSTEXIST,
                     sType );
    if ( Dlg.DoModal() == IDOK )
    {
        sFBFfile = Dlg.GetPathName();
    }

    mFBFfile.SetWindowTextA(sFBFfile); 
    cpFBFmakecfg.setFbfFile(sFBFfile);	//update data

    fbfArg = fbfSwtch + sFBFfile;
    setMode();
    setReset();
    cmdline.Format(_T ("%s %s %s %s"), iniArg, fbfArg, sFBFmakeMode, sFBFmakeReset);
    mFBFcmd.SetWindowTextA(cmdline);
    cpFBFmakecfg.updateCmd(cmdline);	//update data

    SetFocus();
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnBnClickedOk()
{
    if (! validateAll())
        return;
    OnOK();	
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnBnClickedFbfMakeLaunch()
{
    if (! validateAll())
        return;

    cpFBFmakecfg.Launch();
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnBnClickedCancel()
{
    OnCancel();
}

/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnBnClickedFbfMakeMode()
{
    CString FBFcmd;

    UpdateData( TRUE );
    setMode();
    if (sFBFMakePath == "")
        FBFcmd = _T("<FBF_MAKE>");
    else
        FBFcmd = sFBFMakePath;
    cmdline.Format(_T ("%s %s %s %s"), iniArg, fbfArg, sFBFmakeMode, sFBFmakeReset);
    mFBFcmd.SetWindowTextA(cmdline);
    SetFocus();
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnBnClickedFbfMakeReset()
{
    CString FBFcmd;

    UpdateData( TRUE );
    setReset();

    if (sFBFMakePath == "")
        FBFcmd = _T("<FBF_MAKE>");
    else
        FBFcmd = sFBFMakePath;

    cmdline.Format(_T ("%s %s %s %s"), iniArg, fbfArg, sFBFmakeMode, sFBFmakeReset);
    mFBFcmd.SetWindowTextA(cmdline);
    SetFocus();
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::setMode()
{
    if (bFBFmakeMode){
        cbFBFmode.EnableWindow(true);
        sFBFmakeMode.Format(_T (" -m %d"), cpFBFmakecfg.getFBFmodeOpt()); 
    }
    else{
        sFBFmakeMode.Format(_T (""));
        cbFBFmode.EnableWindow(false);
    }
    cpFBFmakecfg.setFMode(bFBFmakeMode ? true : false);
}

/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::setReset()
{
    if (bFBFmakeReset){
        cbFBFreset.EnableWindow(true);
        sFBFmakeReset.Format(_T (" -r %d"), cpFBFmakecfg.getFBFResetOpt());
    }
    else{
        sFBFmakeReset.Format(_T (""));
        cbFBFreset.EnableWindow(false);
    }

    cpFBFmakecfg.setFReset(bFBFmakeReset ? true : false);
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
bool CFBFMakeConfigDlg::validateAll()
{
    if (! validateFBFmakePath()) return false;
    if (! validateFBFiniPath())  return false;
    if (! validateFBFpath())     return false;

    return true;
}
/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
bool CFBFMakeConfigDlg::validateFBFmakePath()
{
    CString var(_T(""));
    int ret = 0;

    var = cpFBFmakecfg.getFbfMakeExe();
    ret = var.Find('<', 0);
    if (-1 != ret )
    {
        CString msg(_T("The path for FBF_make is not defined!"));
        AfxMessageBox(msg);
        return false;
    }

    return true;
}

/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
bool CFBFMakeConfigDlg::validateFBFiniPath()
{
    CString var(_T(""));
    int ret = 0;

    var = cpFBFmakecfg.getInifile();
    ret = var.Find('<', 0);
    if (-1 != ret)
    {
        CString msg(_T("The path for the INI file is not defined!"));
        AfxMessageBox(msg);
        return false;
    }

    return true;
}

/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
bool CFBFMakeConfigDlg::validateFBFpath()
{
    CString var(_T(""));
    int ret = 0;
    var = cpFBFmakecfg.getFbfFile();
    ret = var.Find('<', 0 );
    if (-1 != ret)
    {
        CString msg(_T("You did not define the path for the FBF file!"));
        AfxMessageBox(msg);
        return false;
    }

    return true;
}

/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnCbnSelchangeFbfModeChoices()
{
    int indx = cbFBFmode.GetCurSel();
    CString FBFcmd;

    UpdateData( TRUE );	
    
    if (sFBFMakePath == "")
        FBFcmd = _T("<FBF_MAKE>");
    else
        FBFcmd = sFBFMakePath;

    if (indx == 3)
        ModeOpt = 8;
    else
        ModeOpt = indx;
    
    cpFBFmakecfg.setFBFmodeOpt(ModeOpt);
    sFBFmakeMode.Format(_T (" -m %d"), ModeOpt); 
    cmdline.Format(_T ("%s %s %s %s"), iniArg, fbfArg, sFBFmakeMode, sFBFmakeReset);
    mFBFcmd.SetWindowTextA(cmdline);
    SetFocus();

}

/*----------------------------------------------------------------------------
 *
 *----------------------------------------------------------------------------*/
void CFBFMakeConfigDlg::OnCbnSelchangeFbfResetChoices()
{
    CString FBFcmd;

    UpdateData( TRUE );	
    ResetOpt = cbFBFreset.GetCurSel();
    
    if (sFBFMakePath == "")
        FBFcmd = _T("<FBF_MAKE>");
    else
        FBFcmd = sFBFMakePath;
    
    cpFBFmakecfg.setFBFResetOpt (ResetOpt);
    sFBFmakeReset.Format(_T (" -r %d"), ResetOpt); 
    cmdline.Format(_T ("%s %s %s %s"), iniArg, fbfArg, sFBFmakeMode, sFBFmakeReset);
    mFBFcmd.SetWindowTextA(cmdline);
    SetFocus();
}

