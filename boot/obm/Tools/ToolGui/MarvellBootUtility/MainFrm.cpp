/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "MarvellBootUtility.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include <dbt.h>  // device broadcast header
#include <initguid.h>

#include <string>
using namespace std;

// GUIDs that are of interest
DEFINE_GUID(GUID_CLASS_BOOTROM,
0x6F40032F,0x99BE,0x47C0,0xB1,0x9C,0xBD,0xB4,0xBF,0x32,0x2F,0x49);

const string USBDEV( "\\\\?\\USB" );

// Notification Infrastructure Stuff
HDEVNOTIFY      hNotifyDev;

// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
    ON_WM_CREATE()
    ON_WM_SHOWWINDOW()
    ON_WM_DEVICECHANGE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
    ID_SEPARATOR,           // status line indicator
    ID_INDICATOR_CAPS,
    ID_INDICATOR_NUM,
    ID_INDICATOR_SCRL,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
    // TODO: add member initialization code here
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
        | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
        !m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
    {
        TRACE0("Failed to create toolbar\n");
        return -1;      // fail to create
    }

    if (!m_wndStatusBar.Create(this) ||
        !m_wndStatusBar.SetIndicators(indicators,
          sizeof(indicators)/sizeof(UINT)))
    {
        TRACE0("Failed to create status bar\n");
        return -1;      // fail to create
    }

    // TODO: Delete these three lines if you don't want the toolbar to be dockable
    m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
    EnableDocking(CBRS_ALIGN_ANY);
    DockControlBar(&m_wndToolBar);

    return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
    if( !CMDIFrameWnd::PreCreateWindow(cs) )
        return FALSE;
    // TODO: Modify the Window class or styles here by modifying
    //  the CREATESTRUCT cs

    return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
    CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
    CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers




void CMainFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
    CMDIFrameWnd::OnShowWindow(bShow, nStatus);

    // set up the infrastructure for monitoring usb events
    DEV_BROADCAST_DEVICEINTERFACE   broadcastInterface;

    // Register to receive notification when the BootROM device is plugged in.
    broadcastInterface.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
    broadcastInterface.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;

    memcpy( &(broadcastInterface.dbcc_classguid),
            &(GUID_CLASS_BOOTROM),
            sizeof(struct _GUID));

    hNotifyDev = RegisterDeviceNotification( m_hWnd,
                                             &broadcastInterface,
                                             DEVICE_NOTIFY_WINDOW_HANDLE);
}


BOOL CMainFrame::OnDeviceChange( UINT nEventType, DWORD_PTR dwData )
{
    DEV_BROADCAST_HDR             *devbcasthdr;
    DEV_BROADCAST_DEVICEINTERFACE *iface;

    BOOL                          bDeviceKnown = FALSE;

    // get a timestamp into szTime
    __time64_t ltime;    // for timestamps on messages.
    _time64( &ltime );   // for timestamps on messages
    char szTime[255];
    _ctime64_s( szTime, 255, &ltime );
//	szTime[24]=' ';

    CString sBuf;

    POSITION pos = theApp.GetFirstDocTemplatePosition();
    CDocTemplate* pDocTemplate = theApp.GetNextDocTemplate(pos);
    pos = pDocTemplate->GetFirstDocPosition();
    CDocument* pDoc = pDocTemplate->GetNextDoc(pos);
    ASSERT(pDoc);
    pos = pDoc->GetFirstViewPosition();
    CEditView* pView = (CEditView*)pDoc->GetNextView(pos);
    ASSERT(pView);

    switch( nEventType )
    {
    case DBT_DEVICEARRIVAL:
        bDeviceKnown = FALSE;
        devbcasthdr = (DEV_BROADCAST_HDR*)dwData;

        if( DBT_DEVTYP_DEVICEINTERFACE == devbcasthdr->dbch_devicetype )
        {
            iface = (DEV_BROADCAST_DEVICEINTERFACE*)dwData;

            sBuf.Format(_T("Device Arrival: %s.\r\n"), iface->dbcc_name );
            pView->GetEditCtrl().ReplaceSel(sBuf);

            // is this a known interface?
            // search by GUID first...
            if( memcmp( &iface->dbcc_classguid, &GUID_CLASS_BOOTROM, sizeof(GUID_CLASS_BOOTROM) ) == 0 )
            {
                string sInterfaceName( iface->dbcc_name );

                // check the vid & pid in the name field
                // MH-L / MH-LV and Tavor bootROM
                if( sInterfaceName.find( MH_L_BOOTROM_VID_PID ) != string::npos
                    || sInterfaceName.find( TAVOR_BOOTROM_VID_PID ) != string::npos
                    || sInterfaceName.find( MH_P_BOOTROM_VID_PID ) != string::npos )
                {
                    sBuf.Format(_T("%s BootROM attached.\r\n"), szTime);
                    pView->GetEditCtrl().ReplaceSel(sBuf);
                    bDeviceKnown = TRUE;
                    theApp.AddTarget( CString(iface->dbcc_name) );
                }
                else
                // MH-L / MH-LV and Tavor DKB
                if( sInterfaceName.find( MH_P_DKB_VID_PID ) != string::npos
                    || sInterfaceName.find( MH_L_DKB_VID_PID ) != string::npos
                    || sInterfaceName.find( MH_LV_DKB_VID_PID ) != string::npos
                    || sInterfaceName.find( TAVOR_P_DKB_VID_PID ) != string::npos
                    || sInterfaceName.find( TAVOR_PV_DKB_VID_PID ) != string::npos
                    || sInterfaceName.find( TAVOR_L_DKB_VID_PID ) != string::npos )
                {
                    sBuf.Format(_T("%s DKB attached.\r\n"), szTime);
                    pView->GetEditCtrl().ReplaceSel(sBuf);
                    bDeviceKnown = TRUE;
                    theApp.AddTarget( CString(iface->dbcc_name) );
                }
                else
                // for devices with single vid-pid
                if ( sInterfaceName.find( PXA168_VID_PID ) != string::npos
                    || sInterfaceName.find( PXA920_VID_PID ) != string::npos
                    || sInterfaceName.find( ARMADA_610_VID_PID ) != string::npos
                    || sInterfaceName.find( P_88AP001_VID_PID ) != string::npos 
                    || sInterfaceName.find( MG1_VID_PID ) != string::npos
                    || sInterfaceName.find( TAVOR_PV2_MG2_VID_PID ) != string::npos 
                    || sInterfaceName.find( ESHEL_VID_PID ) != string::npos 
                    || sInterfaceName.find( PXA978_VID_PID ) != string::npos 
                    || sInterfaceName.find( ARMADA_620_VID_PID ) != string::npos 
                    || sInterfaceName.find( ESHEL_LTE_VID_PID ) != string::npos 
                    || sInterfaceName.find( ARMADA_622_VID_PID ) != string::npos 
                    || sInterfaceName.find( WUKONG_VID_PID ) != string::npos 
                   )
                {
                    sBuf.Format(_T("%s attached.\r\n"), szTime);
                    pView->GetEditCtrl().ReplaceSel(sBuf);
                    bDeviceKnown = TRUE;
                    theApp.AddTarget( CString(iface->dbcc_name) );
                }
            }
        }
        break;  // end of switch nEventType - case DBT_ARRIVAL

    case  DBT_DEVICEREMOVECOMPLETE:
        bDeviceKnown = FALSE;
        devbcasthdr = (DEV_BROADCAST_HDR*)dwData;

        if( DBT_DEVTYP_DEVICEINTERFACE == devbcasthdr->dbch_devicetype )
        {
            iface = (DEV_BROADCAST_DEVICEINTERFACE*)dwData;

            sBuf.Format(_T("Device removal: %s.\r\n"), iface->dbcc_name);
            pView->GetEditCtrl().ReplaceSel(sBuf);

            // is this a known interface?
            // search by GUID first...
            if( memcmp( &iface->dbcc_classguid, &GUID_CLASS_BOOTROM, sizeof(GUID_CLASS_BOOTROM) ) == 0 )
            {
                string sInterfaceName( iface->dbcc_name );

                // check the vid & pid in the name field
                // MH-L / MH-LV and Tavor bootROM
                if( sInterfaceName.find( MH_L_BOOTROM_VID_PID ) != string::npos
                    || sInterfaceName.find( TAVOR_BOOTROM_VID_PID ) != string::npos
                    || sInterfaceName.find( MH_P_BOOTROM_VID_PID ) != string::npos )
                {
                    sBuf.Format(_T("%s BootROM removed.\r\n"), szTime);
                    pView->GetEditCtrl().ReplaceSel(sBuf);
                    bDeviceKnown = TRUE;
                    theApp.RemoveTarget( CString(iface->dbcc_name) );
                }
                else
                // MH-L / MH-LV and Tavor DKB
                if( sInterfaceName.find( MH_P_DKB_VID_PID ) != string::npos
                    || sInterfaceName.find( MH_L_DKB_VID_PID ) != string::npos
                    || sInterfaceName.find( MH_LV_DKB_VID_PID ) != string::npos
                    || sInterfaceName.find( TAVOR_P_DKB_VID_PID ) != string::npos
                    || sInterfaceName.find( TAVOR_PV_DKB_VID_PID ) != string::npos
                    || sInterfaceName.find( TAVOR_L_DKB_VID_PID ) != string::npos )
                {
                    sBuf.Format(_T("%s DKB removed.\r\n"), szTime);
                    pView->GetEditCtrl().ReplaceSel(sBuf);
                    bDeviceKnown = TRUE;
                    theApp.RemoveTarget( CString(iface->dbcc_name) );
                }
                else
                // for devices with single vid-pid
                if ( sInterfaceName.find( PXA168_VID_PID ) != string::npos
                    || sInterfaceName.find( PXA920_VID_PID ) != string::npos
                    || sInterfaceName.find( ARMADA_610_VID_PID ) != string::npos
                    || sInterfaceName.find( P_88AP001_VID_PID ) != string::npos 
                    || sInterfaceName.find( MG1_VID_PID ) != string::npos
                    || sInterfaceName.find( TAVOR_PV2_MG2_VID_PID ) != string::npos 
                    || sInterfaceName.find( ESHEL_VID_PID ) != string::npos 
                    || sInterfaceName.find( PXA978_VID_PID ) != string::npos 
                    || sInterfaceName.find( ARMADA_620_VID_PID ) != string::npos 
                    || sInterfaceName.find( ESHEL_LTE_VID_PID ) != string::npos 
                    || sInterfaceName.find( ARMADA_622_VID_PID ) != string::npos 
                    || sInterfaceName.find( WUKONG_VID_PID ) != string::npos 
                   )
                {
                    sBuf.Format(_T("%s removed.\r\n"), szTime);
                    pView->GetEditCtrl().ReplaceSel(sBuf);
                    bDeviceKnown = TRUE;
                    theApp.RemoveTarget( CString(iface->dbcc_name) );
                }

            } // end of if devicetype == DEVICEINTERFACE handling
        }
        break;

    } // end of switch nEventType

    if ( bDeviceKnown == TRUE )
        theApp.RefreshModelessDialogsState();

    return TRUE;  // indicate message handled.
}

string CMainFrame::CreateTitle( string& sTargetName )
{
    string sTargetStr = sTargetName;
    size_t iPos = 0;
    
    if ( string::npos != (iPos = sTargetStr.find(USBDEV)) )
    {
        sTargetStr.replace( iPos, USBDEV.length(), "" );
    
        if ( string::npos != (iPos = sTargetStr.find(MH_L_BOOTROM_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, MH_L_BOOTROM_VID_PID.length()+1, "MH-L Bootrom -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(TAVOR_BOOTROM_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, TAVOR_BOOTROM_VID_PID.length()+1, "TAVOR Bootrom -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(MH_P_BOOTROM_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, MH_P_BOOTROM_VID_PID.length()+1, "MH-P Bootrom -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(MH_P_DKB_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, MH_P_DKB_VID_PID.length()+1, "MH-P DKB-OBM -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(MH_L_DKB_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, MH_L_DKB_VID_PID.length()+1, "MH-L DKB-OBM -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(MH_LV_DKB_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, MH_LV_DKB_VID_PID.length()+1, "MH-LV DKB-OBM -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(TAVOR_P_DKB_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, TAVOR_P_DKB_VID_PID.length()+1, "TAVOR-P DKB-OBM -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(TAVOR_PV_DKB_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, TAVOR_PV_DKB_VID_PID.length()+1, "TAVOR-PV DKB-OBM -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(TAVOR_L_DKB_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, TAVOR_L_DKB_VID_PID.length()+1, "TAVOR-L DKB-OBM -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(PXA168_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, PXA168_VID_PID.length()+1, "PXA168/ARMADA168 -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(ARMADA_610_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, ARMADA_610_VID_PID.length()+1, "PXA688/ARMADA610 -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(PXA920_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, PXA920_VID_PID.length()+1, "PXA920 -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(MG1_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, MG1_VID_PID.length()+1, "MG1/88CP955 -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(TAVOR_PV2_MG2_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, TAVOR_PV2_MG2_VID_PID.length()+1, "TAVOR-PV MG2/88CP958 -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(P_88AP001_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, P_88AP001_VID_PID.length()+1, "88AP001 -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(ESHEL_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, ESHEL_VID_PID.length()+1, "Eshel -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(PXA978_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, PXA978_VID_PID.length()+1, "Nevo/PXA978 -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(ARMADA_620_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, ARMADA_620_VID_PID.length()+1, "ARMADA620 -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(ESHEL_LTE_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, ESHEL_LTE_VID_PID.length()+1, "Eshel LTE -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(ARMADA_622_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, ARMADA_622_VID_PID.length()+1, "ARMADA622 -- HubId{" );
        else if ( string::npos != (iPos = sTargetStr.find(WUKONG_VID_PID)) )
            sTargetStr = sTargetStr.replace( iPos, WUKONG_VID_PID.length()+1, "Wukong -- HubId{" );

        iPos = sTargetStr.find_last_of( "&" );
        sTargetStr.replace(iPos, 1, "");
        sTargetStr.insert( iPos, "} Port{" );
        iPos = sTargetStr.find_last_of( "#" );
        sTargetStr.replace(iPos, 1, "");
        sTargetStr.insert( iPos, "} DeviceId" );
    }

    return sTargetStr;
}

void CMainFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
    CString sTitle = GetTitle();
    if ( theApp.IsChanged() && bAddToTitle )
    {
        if ( -1 == sTitle.Find('*') )
        {
            sTitle += "*";
            SetTitle(sTitle);

        }
    }
    else
    {
        if ( -1 != sTitle.Find('*') )
        {
            sTitle.Remove('*');
            SetTitle(sTitle);
        }
    }

    CMDIFrameWnd::OnUpdateFrameTitle(bAddToTitle);
}
