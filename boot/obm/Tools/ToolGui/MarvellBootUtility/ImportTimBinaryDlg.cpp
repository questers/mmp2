/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// ImportTimBinaryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "ImportTimBinaryDlg.h"
#include "TimDescriptor.h"

// CImportTimBinaryDlg dialog

// images list ctrl column numbers
const int IMAGE_ID = 0;
const int IMAGE_ID_TAG = 1;
const int FLASH_ADDRESS = 2;
const int LOAD_ADDRESS = 3;
const int IMAGE_FILE_PATH = 4;


IMPLEMENT_DYNAMIC(CImportTimBinaryDlg, CDialog)

CImportTimBinaryDlg::CImportTimBinaryDlg(CTimDescriptor& rTimDescriptor, CWnd* pParent /*=NULL*/)
    : CDialog(CImportTimBinaryDlg::IDD, pParent)
    ,TimDescriptor( rTimDescriptor )
{
    m_pImage = 0;
}

CImportTimBinaryDlg::~CImportTimBinaryDlg()
{
}

void CImportTimBinaryDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_BIN_IMAGES_LIST_CTRL, ImagesListCtrl);
    DDX_Text(pDX, IDC_IMAGE_FILE_PATH_EDT, sBinaryFilePath);
}


BEGIN_MESSAGE_MAP(CImportTimBinaryDlg, CDialog)
    ON_EN_CHANGE(IDC_IMAGE_FILE_PATH_EDT, &CImportTimBinaryDlg::OnEnChangeBinaryFilePathEdt)
    ON_BN_CLICKED(IDC_IMAGE_FILE_BROWSE_BTN, &CImportTimBinaryDlg::OnBnClickedBinaryFilePathBrowseBtn)
    ON_NOTIFY(LVN_ITEMACTIVATE, IDC_BIN_IMAGES_LIST_CTRL, &CImportTimBinaryDlg::OnLvnItemActivateBinImagesListCtrl)
    ON_BN_CLICKED(IDOK, &CImportTimBinaryDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CImportTimBinaryDlg message handlers

BOOL CImportTimBinaryDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    ImagesListCtrl.SetExtendedStyle( LVS_EX_TRACKSELECT
                                    | LVS_EX_FULLROWSELECT 
                                    | LVS_EX_HEADERDRAGDROP 
                                    | LVS_EX_ONECLICKACTIVATE);

    ImagesListCtrl.InsertColumn(IMAGE_ID,"Image ID", LVCFMT_LEFT, 60, -1 );
    ImagesListCtrl.InsertColumn(IMAGE_ID_TAG,"Image ID Tag", LVCFMT_LEFT, 85, -1 );
    ImagesListCtrl.InsertColumn(FLASH_ADDRESS,"Flash Address", LVCFMT_LEFT, 85, -1 );
    ImagesListCtrl.InsertColumn(LOAD_ADDRESS,"Load Address", LVCFMT_LEFT, 85, -1 );
    ImagesListCtrl.InsertColumn(IMAGE_FILE_PATH,"Image File Path", LVCFMT_LEFT, 600, -1 );

    RefreshState();
    UpdateControls();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void CImportTimBinaryDlg::UpdateControls()
{
    GetDlgItem(IDC_IMAGE_FILE_PATH_EDT)->EnableWindow(m_pImage != 0);
}

void CImportTimBinaryDlg::RefreshState()
{
    ImagesListCtrl.DeleteAllItems();
    unsigned int i = 0; 
    while ( i < TimDescriptor.ImagesCount() )
    {
        CImageDescription* pImage(TimDescriptor.Image( i ));
        ImagesListCtrl.InsertItem(i, pImage->ImageId().c_str());
        ImagesListCtrl.SetItemData(i,(DWORD_PTR)pImage);  // item retains image*
        ImagesListCtrl.SetItem(i,IMAGE_ID_TAG,LVIF_TEXT,pImage->ImageIdTag().c_str(),0,0,0,0);
        ImagesListCtrl.SetItem(i,FLASH_ADDRESS,LVIF_TEXT,pImage->FlashEntryAddress().c_str(),0,0,0,0);
        ImagesListCtrl.SetItem(i,LOAD_ADDRESS,LVIF_TEXT,pImage->LoadAddress().c_str(),0,0,0,0);
        ImagesListCtrl.SetItem(i,IMAGE_FILE_PATH,LVIF_TEXT,pImage->ImageFilePath().c_str(),0,0,0,0);
        i++;
    }
}

void CImportTimBinaryDlg::OnEnChangeBinaryFilePathEdt()
{
    UpdateData( TRUE );

    if ( m_pImage )
    {
        m_pImage->ImageFilePath( string((LPCTSTR)sBinaryFilePath) );
    }
    RefreshState();
    UpdateControls();
}

void CImportTimBinaryDlg::OnBnClickedBinaryFilePathBrowseBtn()
{
    UpdateData( TRUE );
    if ( m_pImage )
    {
        CFileDialog Dlg( TRUE, _T("*.bin"), NULL, 
                         OFN_PATHMUSTEXIST,
                         _T("Binary (*.bin)|*.bin|All Files (*.*)|*.*||") );
        if ( Dlg.DoModal() == IDOK )
        {
            sBinaryFilePath = Dlg.GetPathName();
            UpdateData( FALSE );
        }

        m_pImage->ImageFilePath( string((LPCTSTR)sBinaryFilePath) );
    }

    RefreshState();
    UpdateControls();
}


void CImportTimBinaryDlg::OnLvnItemActivateBinImagesListCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    // TODO: Add your control notification handler code here
    *pResult = 0;
    m_pImage = 0;

    int iSel = ImagesListCtrl.GetSelectionMark();
    if ( iSel != LB_ERR )
        m_pImage = (CImageDescription*)ImagesListCtrl.GetItemData( iSel );

    if ( m_pImage )
        sBinaryFilePath = m_pImage->ImageFilePath().c_str();
    
    UpdateData(FALSE);
    UpdateControls();
}

void CImportTimBinaryDlg::OnBnClickedOk()
{
    OnOK();
}
