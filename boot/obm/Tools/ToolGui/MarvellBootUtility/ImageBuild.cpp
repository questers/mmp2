/******************************************************************************
 *
 *  (C)Copyright 2008 Marvell International Ltd. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#include "StdAfx.h"
#include "ImageBuild.h"
#include "MarvellBootUtility.h"

#include <ProcessLauncher.h>
#include <stdio.h> 
 
CImageBuild::CImageBuild(void)
#if TOOLS_GUI == 1
: CommandLineParser( theApp.CommandLineParser )
#endif
{
    sTbbExePath = "";
    Reset();
    m_bChanged = false;

    // try to find (NT)TBBV4.exe by default if it is in
    // the same directory as MarvellBootUtility.exe
    ifstream ifs;
    char szDir[MAX_PATH]={0};
    GetCurrentDirectory(MAX_PATH, szDir);
    ifs.open( "TBBV4.exe", ios_base::in | ios_base::binary );
    if ( ifs.is_open() )
    {
        sTbbExePath = szDir;
        sTbbExePath += "\\TBBV4.exe";
        ifs.close();
    }
    else
    {
        ifs.open( "NTTBBV4.exe", ios_base::in | ios_base::binary );
        if ( ifs.is_open() )
        {
            sTbbExePath = szDir;
            sTbbExePath += "\\NTTBBV4.exe";
            ifs.close();
        }
    }
}

CImageBuild::~CImageBuild(void)
{
}

void CImageBuild::Reset()
{
//	sTbbExePath = "";
    sTimDescriptorFilePath = "";
    sHashKeyFilePath = "";
    sPrivateKeyFilePath = "";
    sDigitalSignatureFilePath = "";
    sTimInputFilePath = "";
    sTimOutputFilePath = "";
    sReservedFilePath = "";
    sPartitionFilePath = "";

    CommandLineParser.Reset();
    bAdditionalOptions = false;
    bProcessorType = false;
    m_bChanged = false;
}

void CImageBuild::TimDescriptorFilePath( string& sFilePath ) 
{ 
    sTimDescriptorFilePath = sFilePath; 
    CommandLineParser.KeyFileName = sFilePath;
    m_bChanged = true;
}

void CImageBuild::HashKeyFilePath( string& sFilePath ) 
{ 
    sHashKeyFilePath = sFilePath; 
    CommandLineParser.HashFileName = sFilePath;
    m_bChanged = true;
}

void CImageBuild::PrivateKeyFilePath( string& sFilePath ) 
{ 
    sPrivateKeyFilePath = sFilePath; 
    CommandLineParser.KeyFileName = sFilePath;
    m_bChanged = true;
}

void CImageBuild::DigitalSignatureFilePath( string& sFilePath ) 
{ 
    sDigitalSignatureFilePath = sFilePath; 
    CommandLineParser.DsFileName = sFilePath;
    m_bChanged = true;
}

void CImageBuild::TimInputFilePath( string& sFilePath ) 
{ 
    sTimInputFilePath = sFilePath; 
    CommandLineParser.TimInFileName = sFilePath;
    m_bChanged = true;
}

void CImageBuild::TimOutputFilePath( string& sFilePath ) 
{ 
    sTimOutputFilePath = sFilePath; 
    CommandLineParser.TimOutFileName = sFilePath;
    m_bChanged = true;
}

void CImageBuild::ReservedFilePath( string& sFilePath ) 
{ 
    sReservedFilePath = sFilePath; 
    CommandLineParser.ReservedFileName = sFilePath;
    m_bChanged = true;
}

void CImageBuild::PartitionFilePath( string& sFilePath ) 
{ 
    sPartitionFilePath = sFilePath; 
    CommandLineParser.PartitionFileName = sFilePath;
    m_bChanged = true;
}

#if TOOLS_GUI == 1
bool CImageBuild::SaveState( ofstream& ofs )
{
    stringstream ss;

    ss << sTbbExePath << endl;
    ss << sTimDescriptorFilePath << endl;
    ss << sHashKeyFilePath << endl;
    ss << sPrivateKeyFilePath << endl;
    ss << sDigitalSignatureFilePath << endl;
    ss << sTimInputFilePath << endl;
    ss << sTimOutputFilePath << endl;
    ss << sReservedFilePath << endl;
    ss << sPartitionFilePath << endl;
    ss << boolalpha << CommandLineParser.bIsNonTrusted << endl;
    ss << boolalpha << CommandLineParser.bIsTimVerify << endl;
    ss << boolalpha << CommandLineParser.bVerbose << endl;
    ss << boolalpha << CommandLineParser.bConcatenate << endl;
    ss << boolalpha << CommandLineParser.bOneNANDPadding << endl;
    ss << CommandLineParser.uiPaddedSize << endl;
    ss << boolalpha << CommandLineParser.bIsKeyFile << endl;
    ss << boolalpha << CommandLineParser.bIsHashFile << endl;
    ss << boolalpha << CommandLineParser.bIsDsFile << endl;
    ss << boolalpha << CommandLineParser.bIsTimInFile << endl;
    ss << boolalpha << CommandLineParser.bIsTimOutFile << endl;
    ss << boolalpha << CommandLineParser.bIsReservedDataInFile << endl;
    ss << boolalpha << CommandLineParser.bIsPartitionDataFile << endl;
    ss << CommandLineParser.iOption << endl;
    ss << boolalpha << bProcessorType << endl;
    ss << boolalpha << bAdditionalOptions << endl;
    ss << sAdditionalOptions << endl;
    
    ofs << ss.str();
    ofs.flush();

    m_PartitionTable.SaveState( ofs );

    return ofs.good(); // SUCCESS
}


bool CImageBuild::LoadState( ifstream& ifs )
{
    string sbuf;

    ::getline( ifs, sbuf ); TbbExePath( sbuf );
    ::getline( ifs, sbuf ); TimDescriptorFilePath( sbuf );
    ::getline( ifs, sbuf ); HashKeyFilePath( sbuf );
    ::getline( ifs, sbuf ); PrivateKeyFilePath( sbuf );
    ::getline( ifs, sbuf ); DigitalSignatureFilePath( sbuf );
    ::getline( ifs, sbuf ); TimInputFilePath( sbuf );
    ::getline( ifs, sbuf ); TimOutputFilePath( sbuf );
    ::getline( ifs, sbuf ); ReservedFilePath( sbuf );
    ::getline( ifs, sbuf ); PartitionFilePath( sbuf );
    ::getline( ifs, sbuf ); CommandLineParser.bIsNonTrusted = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); CommandLineParser.bIsTimVerify = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); CommandLineParser.bVerbose = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); CommandLineParser.bConcatenate = ( sbuf == "true" ? true : false );
    
    if ( theApp.ProjectVersion >= 0x03030102 )
    {
        ::getline( ifs, sbuf ); 
        CommandLineParser.bOneNANDPadding = ( sbuf == "true" ? true : false );
        ::getline( ifs, sbuf ); 
        CommandLineParser.uiPaddedSize = atoi(sbuf.c_str());
    }

    ::getline( ifs, sbuf ); CommandLineParser.bIsKeyFile = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); CommandLineParser.bIsHashFile = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); CommandLineParser.bIsDsFile = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); CommandLineParser.bIsTimInFile = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); CommandLineParser.bIsTimOutFile = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); CommandLineParser.bIsReservedDataInFile = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); CommandLineParser.bIsPartitionDataFile = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); CommandLineParser.iOption = atoi(sbuf.c_str());
    ::getline( ifs, sbuf ); bProcessorType = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); bAdditionalOptions = ( sbuf == "true" ? true : false );
    ::getline( ifs, sAdditionalOptions );

    m_PartitionTable.LoadState( ifs );

    return ifs.good() && !ifs.fail(); // SUCCESS
}
#endif

string& CImageBuild::CommandLineArgs(CTimDescriptorParser& TimDescriptorParser)
{
    sCommandLineArgs = "";

    //	sCommandLineArgs = TbbExePath().c_str();
//	sCommandLineArgs += " ";

    // add verbose switch
    if ( VerboseMode() ) sCommandLineArgs += "-v ";

    // add verify switch
    if ( Verify() ) sCommandLineArgs += "-V ";

    // add concatenate
    if ( Concatenate() ) sCommandLineArgs += "-C ";

    // add concatenate
    if ( OneNANDPadding() && PaddedSize() > 0 ) 
    {
        sCommandLineArgs += "-O ";
        stringstream ss;
        ss << PaddedSize();
        sCommandLineArgs += ss.str();
        sCommandLineArgs += " ";
    }

    if ( bProcessorType )
    {
        if ( CommandLineParser.iProcessorType > PXA_NONE && CommandLineParser.iProcessorType < PXAMAX_PT )
        {
            // add processor type
            sCommandLineArgs += "-T ";
            stringstream ss;
            ss << CommandLineParser.iProcessorType;
            sCommandLineArgs += ss.str();
            sCommandLineArgs += " ";
        }
    }

    // add mode option switch and file paths
    sCommandLineArgs += "-m ";
    if ( !HashKeyFile() )
    {
        // mode 1 - not trusted, everything in Timdescriptor text file
        CommandLineParser.iOption = 1;
        sCommandLineArgs += "1 -r ";

        if ( TimDescriptorFilePath().length() > 0 )
        {
            sCommandLineArgs += TimDescriptorParser.QuotedFilePath( TimDescriptorFilePath() );
            sCommandLineArgs += " ";
        }
        else
            sCommandLineArgs += "<input TimDescriptorFilePath> ";
    }
    else if ( HashKeyFile() && !PrivateKeyFile() )
    {
        // mode 2 - trusted, input TimDescriptor, input hash key file
        CommandLineParser.iOption = 2;
        sCommandLineArgs += "2 -r ";
        if ( TimDescriptorFilePath().length() > 0 )
        {
            sCommandLineArgs += TimDescriptorFilePath();
            sCommandLineArgs += " ";
        }
        else
            sCommandLineArgs += "<input TimDescriptorFilePath> ";

        sCommandLineArgs += "-h ";
        if ( HashKeyFile() && HashKeyFilePath().length() > 0 )
        {
            sCommandLineArgs += TimDescriptorParser.QuotedFilePath( HashKeyFilePath() );
            sCommandLineArgs += " ";
        }
        else
            sCommandLineArgs += "<input HashKeyFilePath> ";
        
    }
    else if ( HashKeyFile() && PrivateKeyFile() && !TimInputFile() && !TimOutputFile() )
    {
        // mode 3 - trusted, input HashKeyFile, 
        //			output DigitalSignature file
        CommandLineParser.iOption = 3;
        sCommandLineArgs += "3 -r ";
        if ( PrivateKeyFilePath().length() > 0 )
        {
            sCommandLineArgs += TimDescriptorParser.QuotedFilePath( PrivateKeyFilePath() );
            sCommandLineArgs += " ";
        }
        else
            sCommandLineArgs += "<input PrivateKeyFilePath> ";

        sCommandLineArgs += "-h ";
        if ( HashKeyFilePath().length() > 0 )
        {
            sCommandLineArgs += TimDescriptorParser.QuotedFilePath( HashKeyFilePath() );
            sCommandLineArgs += " ";
        }
        else
            sCommandLineArgs += "<input HashKeyFilePath> ";

        sCommandLineArgs += "-s ";
        if ( DigitalSignatureFile() && DigitalSignatureFilePath().length() > 0 )
        {
            sCommandLineArgs += TimDescriptorParser.QuotedFilePath( DigitalSignatureFilePath() );

            sCommandLineArgs += " ";
        }
        else
            sCommandLineArgs += "<output DigitalSignatureFilePath> ";
    }
    else if ( DigitalSignatureFile() && TimInputFile() )
    {
        // mode 4 - trusted, input HashKeyFile, 
        //			output DigitalSignature file
        CommandLineParser.iOption = 4;
        sCommandLineArgs += "4 -i ";
        if ( TimInputFile() && TimInputFilePath().length() > 0 )
        {
            sCommandLineArgs += TimDescriptorParser.QuotedFilePath( TimInputFilePath() );
            sCommandLineArgs += " ";
        }
        else
            sCommandLineArgs += "<input TimInputFilePath>";

        sCommandLineArgs += " -o ";
        if ( TimOutputFile() && TimOutputFilePath().length() > 0 )
        {
            sCommandLineArgs += TimDescriptorParser.QuotedFilePath( TimOutputFilePath() );
            sCommandLineArgs += " ";
        }
        else
            sCommandLineArgs += "<output TimOutputFilePath> ";

        sCommandLineArgs += "-s ";
        if ( DigitalSignatureFilePath().length() > 0 )
        {
            sCommandLineArgs += TimDescriptorParser.QuotedFilePath( DigitalSignatureFilePath() );
            sCommandLineArgs += " ";
        }
        else
            sCommandLineArgs += "<input DigitalSignatureFilePath> ";
    }

    if ( ReservedFile() )
    {
        sCommandLineArgs += "-R ";
        if ( ReservedFilePath().length() > 0 )
        {
            sCommandLineArgs += TimDescriptorParser.QuotedFilePath( ReservedFilePath() );
            sCommandLineArgs += " ";
        }
        else
            sCommandLineArgs += "<input ReservedFilePath> ";
    }

    if ( PartitionFile() && TimDescriptorParser.TimDescriptor().Version() >= "0x00030200" )
    {
        sCommandLineArgs += "-P ";
        if ( PartitionFilePath().length() > 0 )
        {
            sCommandLineArgs += TimDescriptorParser.QuotedFilePath( PartitionFilePath() );
            sCommandLineArgs += " ";
        }
        else
            sCommandLineArgs += "<input PartitionFilePath> ";
    }

    if ( bAdditionalOptions && AdditionalOptions().length() > 0 )
    {
        sCommandLineArgs += " ";
        sCommandLineArgs += AdditionalOptions();
    }

    return sCommandLineArgs;
}

bool CImageBuild::Validate()
{
    // simple test for missing paths in command line
    if ( sCommandLineArgs.find("<") == string::npos && sCommandLineArgs.find(">") == string::npos )
        return true;
    else
    {
        AfxMessageBox( "Error: TBB command line options are not valid." );
        return false;
    }
}

bool CImageBuild::Launch()
{
   CProcessLauncher  ProcessLauncher;

    if ( sTbbExePath.length() == 0 )
    {
        AfxMessageBox( "Enter a path for the TBB tool in the Image Build dialog." );
        return false;
    }

    return ProcessLauncher.Launch( sTbbExePath, sCommandLineArgs );
}