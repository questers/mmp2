/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#include "TimLib.h"

class CFBFMakeConfig : public CTimLib
{
public:
    CFBFMakeConfig();
    virtual ~CFBFMakeConfig();
    CFBFMakeConfig( const CFBFMakeConfig& rhs);  //copy constructor
    CFBFMakeConfig& operator=( const CFBFMakeConfig& rhs);  //operator assignment

    void updateCmd     (CString str);
    void setFbfMakeExe (CString str);
    void setInifile    (CString str);
    void setFbfFile    (CString str);
    void setFMode      (bool opt);
    void setFReset     (bool opt);
    void setFBFmodeOpt (int val);
    void setFBFResetOpt(int val);

    CString getFbfMakeCmd ();
    CString getFbfMakeExe ();
    CString getInifile    ();
    CString getFbfFile    ();
    bool    getFMode	  ();
    bool    getFReset     ();
    bool    getFchng	  ();
    int     getFBFmodeOpt ();
    int     getFBFResetOpt();

#if TOOLS_GUI == 1
    bool SaveState( ofstream& ofs );
    bool LoadState( ifstream& ifs );
    bool Launch();
#endif

    void Reset();

private:
    CString fbfMakeExe;
    CString IniFile;
    CString fbfFile;
    CString fbfMakeCmd;

    bool bMode;
    bool bReset;
    bool bChng;

    unsigned int FBFmodeOpt;
    unsigned int FBFResetOpt;
};












