/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "EditListCtrl.h"
#include "Key.h"

// CPublicKeyExponentDlg dialog

class CPublicKeyExponentDlg : public CDialog
{
	DECLARE_DYNAMIC(CPublicKeyExponentDlg)

public:
	CPublicKeyExponentDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPublicKeyExponentDlg();

// Dialog Data
	enum { IDD = IDD_PUBLIC_KEY_EXPONENT_DLG };

public:
	CEditListCtrl PublicKeyExponentListCtrl;
	afx_msg void OnBnClickedResetExponentBtn();
	afx_msg void OnBnClickedOk();
	afx_msg void OnLvnItemActivatePublicKeyExponentListCtrl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMKillfocusPublicKeyExponentListCtrl(NMHDR *pNMHDR, LRESULT *pResult);

	UINT uiSizeInBits;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

public:
	void UpdateChangeMarkerInTitle();

	CKey& Key() { return m_Key; }
	void Key( CKey& Key ){ m_Key = Key; }

	void Title( string sTitle ){ m_sTitle = sTitle; }

private:
	void RefreshState();
	void UpdatePublicKeyExponentList();

	CKey m_Key;
	string m_sTitle;
};
