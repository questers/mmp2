/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// ReservedDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "ReservedDataDlg.h"
#include "ReservedDataPackageDlg.h"
#include "ExtReservedDataPkgDlg.h"
#include "UsbVendorReqERD.h"
#include "CommentDlg.h"
#include "ConsumerPackageDlg.h"

#include "DDRInitializationPkgDlg.h"
#include "TzInitializationPkgDlg.h"


// CReservedDataDlg dialog

IMPLEMENT_DYNAMIC(CReservedDataDlg, CDialog)

CReservedDataDlg::CReservedDataDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CReservedDataDlg::IDD, pParent)
{
    iTotalSize = 0;
    iExtTotalSize = 0;
}

CReservedDataDlg::~CReservedDataDlg()
{
}

void CReservedDataDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_PACKAGES_LIST_CTRL, PackagesListCtrl);
    DDX_Control(pDX, IDC_EXT_RESERVED_DATA_PACKAGES_LIST, ExtPackagesListCtrl);
    DDX_Text(pDX, IDC_TOTAL_SIZE, iTotalSize);
    DDX_Text(pDX, IDC_TOTAL_SIZE_EXT_RESERVED_DATA_EDT, iExtTotalSize);
    DDX_Control(pDX, IDC_INSERT_PACKAGE_BTN, InsertPackageBtn);
    DDX_Control(pDX, IDC_EDIT_PACKAGE_BTN, EditPackageBtn);
    DDX_Control(pDX, IDC_RESERVED_DATA_MOVE_UP_BTN, MoveUpBtn);
    DDX_Control(pDX, IDC_RESERVED_DATA_MOVE_DOWN_BTN, MoveDownBtn);
    DDX_Control(pDX, IDC_DELETE_PACKAGE_BTN, DeletePackageBtn);
    DDX_Control(pDX, IDC_ADD_EXT_RESERVED_BTN, AddERDPackageBtn);
    DDX_Control(pDX, IDC_EDIT_EXT_RESERVED_BTN, EditERDPackageBtn);
    DDX_Control(pDX, IDC_DELETE_EXT_PACKAGE_BTN, DeleteERDPackageBtn);

    // updates marker in title
    UpdateChangeMarkerInTitle();
    theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CReservedDataDlg, CDialog)
    ON_BN_CLICKED(IDC_EDIT_PACKAGE_BTN, &CReservedDataDlg::OnBnClickedEditPackageBtn)
    ON_BN_CLICKED(IDC_DELETE_PACKAGE_BTN, &CReservedDataDlg::OnBnClickedDeletePackageBtn)
    ON_BN_CLICKED(IDC_RESERVED_DATA_MOVE_UP_BTN, &CReservedDataDlg::OnBnClickedReservedDataMoveUpBtn)
    ON_BN_CLICKED(IDC_RESERVED_DATA_MOVE_DOWN_BTN, &CReservedDataDlg::OnBnClickedReservedDataMoveDownBtn)
    ON_BN_CLICKED(IDC_INSERT_PACKAGE_BTN, &CReservedDataDlg::OnBnClickedInsertPackageBtn)
    ON_BN_CLICKED(IDC_EDIT_EXT_RESERVED_BTN, &CReservedDataDlg::OnBnClickedEditExtReservedBtn)
    ON_BN_CLICKED(IDC_DELETE_EXT_PACKAGE_BTN, &CReservedDataDlg::OnBnClickedDeleteExtPackageBtn)
    ON_BN_CLICKED(IDC_ADD_EXT_RESERVED_BTN, &CReservedDataDlg::OnBnClickedAddExtReservedBtn)
    ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()


// CReservedDataDlg message handlers

BOOL CReservedDataDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    PackagesListCtrl.SetExtendedStyle(    LVS_EX_TRACKSELECT
                                          //  LVS_EX_BORDERSELECT
                                          | LVS_EX_FULLROWSELECT 
                                          | LVS_EX_HEADERDRAGDROP 
                                          | LVS_EX_ONECLICKACTIVATE 
                                          | LVS_EX_LABELTIP 
                                          //| LVS_EX_GRIDLINES
                                         );

    PackagesListCtrl.InsertColumn(0,"Package ID", LVCFMT_LEFT, 160, -1 );
    PackagesListCtrl.InsertColumn(1,"Package Tag", LVCFMT_LEFT, 90, -1 );
    PackagesListCtrl.InsertColumn(2,"Size", LVCFMT_LEFT, 35, -1 );
    PackagesListCtrl.InsertColumn(3,"Data Block", LVCFMT_LEFT, 350, -1 );

    ExtPackagesListCtrl.SetExtendedStyle(    LVS_EX_TRACKSELECT
                                          //  LVS_EX_BORDERSELECT
                                          | LVS_EX_FULLROWSELECT 
                                          | LVS_EX_HEADERDRAGDROP 
                                          | LVS_EX_ONECLICKACTIVATE 
                                          | LVS_EX_LABELTIP 
                                          //| LVS_EX_GRIDLINES
                                         );

    ExtPackagesListCtrl.InsertColumn(0,"Package ID", LVCFMT_LEFT, 150, -1 );
    ExtPackagesListCtrl.InsertColumn(1,"Pkg Size", LVCFMT_LEFT, 60, -1 );
    ExtPackagesListCtrl.InsertColumn(2,"Field(s) : Value(s)", LVCFMT_LEFT, 485, -1 );

    RefreshState();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void CReservedDataDlg::UpdateControls()
{
    InsertPackageBtn.EnableWindow(true);
    EditPackageBtn.EnableWindow(PackagesListCtrl.GetItemCount()>0);
    MoveUpBtn.EnableWindow(PackagesListCtrl.GetItemCount()>1);
    MoveDownBtn.EnableWindow(PackagesListCtrl.GetItemCount()>1);
    DeletePackageBtn.EnableWindow(PackagesListCtrl.GetItemCount()>0);
    AddERDPackageBtn.EnableWindow(true);
    EditERDPackageBtn.EnableWindow(ExtPackagesListCtrl.GetItemCount()>0);
    DeleteERDPackageBtn.EnableWindow(ExtPackagesListCtrl.GetItemCount()>0);
}

void CReservedDataDlg::RefreshState()
{
    PackagesListCtrl.DeleteAllItems();
    t_ReservedDataList& ReservedDataList = m_LocalTimDescriptor.ReservedDataList();
    t_ReservedDataListIter Iter = ReservedDataList.begin();
    int nItem = 0;
    while( Iter != ReservedDataList.end() )
    {
        CReservedPackageData* pData = (*Iter);
        PackagesListCtrl.InsertItem( nItem, pData->PackageId().c_str() );
        PackagesListCtrl.SetItemData( nItem, (DWORD_PTR)pData );
        string sDataBlock;
        int ReservedDataBlockSize = pData->DataBlock( sDataBlock );
        stringstream ss;
        ss << ReservedDataBlockSize;
        PackagesListCtrl.SetItemText( nItem, 1, pData->PackageIdTag().c_str() );
        PackagesListCtrl.SetItemText( nItem, 2, ss.str().c_str() );
        PackagesListCtrl.SetItemText( nItem, 3, sDataBlock.c_str() );
        nItem++;
        Iter++;
    }
    iTotalSize = m_LocalTimDescriptor.ReservedDataTotalSize();		

    ExtPackagesListCtrl.DeleteAllItems();
    nItem = 0;
    CExtendedReservedData& Ext = m_LocalTimDescriptor.ExtendedReservedData();

    if ( Ext.ClockEnableFields.size() > 0 )
        RefreshExtPackage( nItem, sClockEnable, Ext.ClockEnableFields, Ext.g_ClockEnableFields );

    if ( Ext.DDRGeometryFields.size() > 0 )
        RefreshExtPackage( nItem, sDDRGeometry, Ext.DDRGeometryFields, Ext.g_SdramSpecFields );

    if ( Ext.DDRTimingFields.size() > 0 )
        RefreshExtPackage( nItem, sDDRTiming, Ext.DDRTimingFields, Ext.g_SdramSpecFields );

    if ( Ext.DDRCustomFields.size() > 0 )
        RefreshExtPackage( nItem, sDDRCustom, Ext.DDRCustomFields, Ext.g_DDRCustomFields );

    if ( Ext.FrequencyFields.size() > 0 )
        RefreshExtPackage( nItem, sFrequency, Ext.FrequencyFields, Ext.g_FrequencyFields );

    if ( Ext.VoltagesFields.size() > 0 )
        RefreshExtPackage( nItem, sVoltages, Ext.VoltagesFields, Ext.g_VoltagesFields );

    if ( Ext.ConfigMemoryControlFields.size() > 0 )
        RefreshExtPackage( nItem, sConfigMemoryControl, Ext.ConfigMemoryControlFields, Ext.g_ConfigMemoryControlFields );

    if ( Ext.TrustZoneRegidFields.size() > 0 )
        RefreshExtPackage( nItem, sTrustZoneRegid, Ext.TrustZoneRegidFields, Ext.g_TrustZoneRegidFields );

    if ( Ext.TrustZoneFields.size() > 0 )
        RefreshExtPackage( nItem, sTrustZone, Ext.TrustZoneFields, Ext.g_TrustZoneFields );

    if ( Ext.OpDivFields.size() > 0 )
        RefreshExtPackage( nItem, sOpDiv, Ext.OpDivFields, Ext.g_OpDivFields );

    if ( Ext.OpModeFields.size() > 0 )
        RefreshExtPackage( nItem, sOpMode, Ext.OpModeFields, Ext.g_OpModeFields );

    if ( Ext.ErdVec.size() > 0 )
    {
        t_ErdBaseVectorIter ErdIter = Ext.ErdVec.begin();
        while( ErdIter != Ext.ErdVec.end() )
        {
            RefreshExtPackage( nItem, ErdIter );
            ErdIter++;
        }
    }

    if ( Ext.m_Consumers.size() > 0 )
    {
        t_ConsumerIDVecIter ConsumerIter = Ext.m_Consumers.begin();
        int iFirstConsumer = nItem+1;
        while( ConsumerIter != Ext.m_Consumers.end() )
        {
            RefreshExtPackage( nItem, ConsumerIter );
            stringstream ss;
            if ( iFirstConsumer == nItem )
            {
                // reset size the first consumers because the WRAH and num consumers
                // is only needed for the first CIDP.  Multiple CIDPs are collapsed to
                // only on CIDP package with a list of consumers and consumed.
                ss << (*ConsumerIter)->PackageSize()+12;
                ExtPackagesListCtrl.SetItemText( nItem-1, 1, ss.str().c_str() );
            }

            ConsumerIter++;
        }
    }

    // add OPTH header/terminator if only Ext and no reserved data
    iExtTotalSize = ((iTotalSize == 0) && (Ext.Size() > 0)) ? (Ext.Size()+16) : Ext.Size(); 

    UpdateControls();
    UpdateData( FALSE );
}

void CReservedDataDlg::OnBnClickedEditPackageBtn()
{
    UpdateData( TRUE );
    POSITION pos = PackagesListCtrl.GetFirstSelectedItemPosition();
    if ( pos )
    {
        int idx = PackagesListCtrl.GetNextSelectedItem(pos);
        if ( idx != -1 )
        {
            CReservedDataPackageDlg Dlg;
            CReservedPackageData* pData = (CReservedPackageData*)PackagesListCtrl.GetItemData( idx );
            if ( pData )
            {
                Dlg.PackageData() = *pData;
                if ( Dlg.DoModal() == IDOK )
                {
                    if ( Dlg.PackageData().IsChanged() )
                    {
                        *pData = Dlg.PackageData();
                        m_LocalTimDescriptor.ReservedChanged(true);
                        m_LocalTimDescriptor.NotWritten(true);
                        RefreshState();
                    }
                }
            }
        }
    }
    SetFocus();
}

void CReservedDataDlg::OnBnClickedDeletePackageBtn()
{
    UpdateData( TRUE );
    POSITION pos = PackagesListCtrl.GetFirstSelectedItemPosition();
    if ( pos )
    {
        int idx = PackagesListCtrl.GetNextSelectedItem(pos);
        if ( idx != -1 )
        {
            CReservedPackageData* pData = (CReservedPackageData*)PackagesListCtrl.GetItemData( idx );
            m_LocalTimDescriptor.ReservedDataList().remove( pData );
            delete pData;
            m_LocalTimDescriptor.ReservedChanged(true);
            m_LocalTimDescriptor.NotWritten(true);
            RefreshState();
        }
    }
}

void CReservedDataDlg::OnBnClickedReservedDataMoveUpBtn()
{
    int nItem = PackagesListCtrl.GetSelectionMark();
    if ( nItem > 0 )
    {
        CReservedPackageData* pData = (CReservedPackageData*)PackagesListCtrl.GetItemData( nItem );
        t_ReservedDataList& ReservedDataList = m_LocalTimDescriptor.ReservedDataList();
        t_ReservedDataListIter Iter = ReservedDataList.begin();
        while( Iter != ReservedDataList.end() )
        {
            if ( *Iter == pData )
            {
                // move it up unless already at top of list
                if ( Iter != ReservedDataList.begin() )
                {
                    t_ReservedDataListIter insertiter = --Iter;

                    ReservedDataList.remove( pData );
                    ReservedDataList.insert( insertiter, pData );
                    PackagesListCtrl.SetSelectionMark( --nItem );

                    m_LocalTimDescriptor.ReservedChanged(true);
                    m_LocalTimDescriptor.NotWritten(true);
                }
                break;
            }
            Iter++;
        }
    }

    RefreshState();
}

void CReservedDataDlg::OnBnClickedReservedDataMoveDownBtn()
{
    int nItem = PackagesListCtrl.GetSelectionMark();
    if ( nItem != -1 )
    {
        CReservedPackageData* pData = (CReservedPackageData*)PackagesListCtrl.GetItemData( nItem );
        t_ReservedDataList& ReservedDataList = m_LocalTimDescriptor.ReservedDataList();
        t_ReservedDataListIter Iter = ReservedDataList.begin();
        while( Iter != ReservedDataList.end() )
        {
            if ( *Iter == pData )
            {
                // move it down unless already at end of list
                if ( ++Iter != ReservedDataList.end() )
                {
                    ReservedDataList.remove( pData );
                    ReservedDataList.insert( ++Iter, pData );
                    PackagesListCtrl.SetSelectionMark( ++nItem );
                    m_LocalTimDescriptor.ReservedChanged(true);
                    m_LocalTimDescriptor.NotWritten(true);
                }
                break;
            }
            Iter++;
        }
    }

    RefreshState();
}

void CReservedDataDlg::OnBnClickedInsertPackageBtn()
{
    UpdateData( TRUE );

    t_ReservedDataList& ReservedDataList = m_LocalTimDescriptor.ReservedDataList();

    int nItem = PackagesListCtrl.GetSelectionMark();
    if ( nItem == -1 )
        // nothing selected 
        nItem = PackagesListCtrl.GetItemCount();
    else
        nItem++;	// insert after selected item

    CReservedDataPackageDlg Dlg;
    if ( Dlg.DoModal() == IDOK )
    {
        CReservedPackageData* pData = new CReservedPackageData(Dlg.PackageData());
        //put key in correct position in list
        t_ReservedDataListIter Iter = ReservedDataList.begin();
        int ItemPos = nItem-1;
        while ( Iter != ReservedDataList.end() && ItemPos >= 0 )
        {
            ItemPos--;
            Iter++;
        }
        ReservedDataList.insert( Iter, pData );
        m_LocalTimDescriptor.ReservedChanged(true);
        m_LocalTimDescriptor.NotWritten(true);
        RefreshState();
    }
    SetFocus();
}

void CReservedDataDlg::UpdateChangeMarkerInTitle()
{
    if ( m_hWnd == 0 )
        return;

    CString sTitle;
    GetWindowTextA( sTitle );
    // add or remove * as appropriate
    if ( m_LocalTimDescriptor.ReservedIsChanged() )
    {
        if ( -1 == sTitle.Find('*') )
            sTitle += "*";
    }
    else
    {
        if ( -1 != sTitle.Find('*') )
            sTitle.Remove('*');
    }
    SetWindowTextA(sTitle);
    UpdateWindow();
}

void CReservedDataDlg::OnBnClickedAddExtReservedBtn()
{
    UpdateData( TRUE );

    CExtendedReservedData TempExt = m_LocalTimDescriptor.ExtendedReservedData();
    CErdBase* pErdBase = 0;
    CExtReservedDataPkgDlg Dlg( m_LocalTimDescriptor, CString(""), TempExt, false, 
                                m_LocalTimDescriptor.ProcessorType(), pErdBase);
    if ( Dlg.DoModal() == IDOK )
    {
        CExtendedReservedData& ERD = m_LocalTimDescriptor.ExtendedReservedData();
        // copy DDR ERD
        if ( pErdBase )
        {
            if ( pErdBase->ErdPkgType() == CErdBase::CONSUMER_ID_ERD )
            {
                CConsumerID* pConsumerID = dynamic_cast<CConsumerID*>(pErdBase);
                if ( pConsumerID )
                    ERD.m_Consumers.push_back( pConsumerID );
            }
            else
                ERD.ErdVec.push_back( pErdBase );
        }
        else
        {
            ERD = TempExt;
        }

        m_LocalTimDescriptor.ReservedChanged(true);
        m_LocalTimDescriptor.NotWritten(true);
        RefreshState();
    }
    SetFocus();
}

void CReservedDataDlg::OnBnClickedEditExtReservedBtn()
{
    UpdateData( TRUE );

    int nItem = ExtPackagesListCtrl.GetSelectionMark();
    if ( nItem != -1 )
    {
        CString sPackageId = ExtPackagesListCtrl.GetItemText( nItem, 0 );

        CExtendedReservedData& ERD = m_LocalTimDescriptor.ExtendedReservedData();
        CExtendedReservedData TempExt = ERD; // local copy to edit

        // select the package so all of it's fields can be removed
        t_PairList* pFields = 0;
        if ( sPackageId == sClockEnable.c_str() )
            pFields = &ERD.ClockEnableFields;
        else if ( sPackageId == sDDRGeometry.c_str() )
            pFields = &ERD.DDRGeometryFields;
        else if ( sPackageId == sDDRTiming.c_str() )
            pFields = &ERD.DDRTimingFields;
        else if ( sPackageId == sDDRCustom.c_str() )
            pFields = &ERD.DDRCustomFields;
        else if ( sPackageId == sFrequency.c_str() )
            pFields = &ERD.FrequencyFields;
        else if ( sPackageId == sVoltages.c_str() )
            pFields = &ERD.VoltagesFields;
        else if ( sPackageId == sConfigMemoryControl.c_str() )
            pFields = &ERD.ConfigMemoryControlFields;
        else if ( sPackageId == sTrustZoneRegid.c_str() )
            pFields = &ERD.TrustZoneRegidFields;
        else if ( sPackageId == sTrustZone.c_str() )
            pFields = &ERD.TrustZoneFields;
        else if ( sPackageId == sOpDiv.c_str() )
            pFields = &ERD.OpDivFields;
        else if ( sPackageId == sOpMode.c_str() )
            pFields = &ERD.OpModeFields;

        CErdBase* pData = 0;
        if ( pFields == 0 )
            pData = (CErdBase*)((ExtPackagesListCtrl.GetItemData( nItem )));

        if ( pData )
        {
            if ( pData->ErdPkgType() == CErdBase::CONSUMER_ID_ERD )
            {
                CConsumerID* pConsumerID = dynamic_cast<CConsumerID*>( pData );
                if ( pConsumerID )
                {
                    CConsumerPackageDlg Dlg( m_LocalTimDescriptor, pConsumerID );
                    if ( IDOK == Dlg.DoModal() )
                    {
                        *pData = *pConsumerID;

                        m_LocalTimDescriptor.ReservedChanged(true);
                        m_LocalTimDescriptor.NotWritten(true);
                        RefreshState();
                        SetFocus();
                    }
                    return;
                }
            }
            else if ( pData->ErdPkgType() == CErdBase::DDR_INITIALIZATION_ERD )
            {
                CDDRInitialization* pDDROrig = dynamic_cast<CDDRInitialization*>( pData );
                if ( pDDROrig )
                {
                    CDDRInitializationPkgDlg Dlg( m_LocalTimDescriptor, *pDDROrig );
                    if ( Dlg.DoModal() == IDOK )
                    {
                        // if the PID is changed
                        if ( pDDROrig->m_sDdrPID != Dlg.m_DDRInit.m_sDdrPID )
                        {
                            // update all consumers from the old PID to the new PID
                            t_ConsumerIDVecIter Iter = TempExt.m_Consumers.begin();
                            while ( Iter != TempExt.m_Consumers.end() )
                            {
                                t_stringVectorIter PIDIter = (*Iter)->m_PIDs.begin();
                                while ( PIDIter != (*Iter)->m_PIDs.end() )
                                {
                                    if ( *(*PIDIter) == pDDROrig->m_sDdrPID )
                                        *(*PIDIter) = Dlg.m_DDRInit.m_sDdrPID;

                                    PIDIter++;
                                }
                                Iter++;
                            }
                        }

                        *pDDROrig = Dlg.m_DDRInit;
                        
                        m_LocalTimDescriptor.ReservedChanged(true);
                        m_LocalTimDescriptor.NotWritten(true);
                        RefreshState();
                        SetFocus();
                    }
                    return;
                }
            }
            if ( pData->ErdPkgType() == CErdBase::TZ_INITIALIZATION_ERD )
            {
                CTzInitialization* pTzOrig = dynamic_cast<CTzInitialization*>( pData );
                if ( pTzOrig )
                {
                    CTzInitializationPkgDlg Dlg( m_LocalTimDescriptor, *pTzOrig );
                    if ( Dlg.DoModal() == IDOK )
                    {
                        // if the PID is changed
                        if ( pTzOrig->m_sTzPID != Dlg.m_TzInit.m_sTzPID )
                        {
                            // update all consumers from the old PID to the new PID
                            t_ConsumerIDVecIter Iter = TempExt.m_Consumers.begin();
                            while ( Iter != TempExt.m_Consumers.end() )
                            {
                                t_stringVectorIter PIDIter = (*Iter)->m_PIDs.begin();
                                while ( PIDIter != (*Iter)->m_PIDs.end() )
                                {
                                    if ( *(*PIDIter) == pTzOrig->m_sTzPID )
                                        *(*PIDIter) = Dlg.m_TzInit.m_sTzPID;

                                    PIDIter++;
                                }
                                Iter++;
                            }
                        }

                        *pTzOrig = Dlg.m_TzInit;
                        
                        m_LocalTimDescriptor.ReservedChanged(true);
                        m_LocalTimDescriptor.NotWritten(true);
                        RefreshState();
                        SetFocus();
                    }
                    return;
                }
            }
        }

        CExtReservedDataPkgDlg Dlg( m_LocalTimDescriptor, sPackageId, TempExt, 
                                    true, m_LocalTimDescriptor.ProcessorType(), pData );
        if ( Dlg.DoModal() == IDOK )
        {
            if ( pData )
            {
                // update other ERD object, if any
                t_ErdBaseVectorIter& iterVec = ERD.ErdVec.begin();
                while ( iterVec != ERD.ErdVec.end() )
                {
                    if ( pData == *iterVec )
                        *(*iterVec) = *pData;
                    iterVec++;
                }
            }
            else
            {
                ERD = TempExt;
            }
        
            m_LocalTimDescriptor.ReservedChanged(true);
            m_LocalTimDescriptor.NotWritten(true);
            RefreshState();
            SetFocus();
            return;
        }
    }
    else
        AfxMessageBox( "Select Package to Edit" );

    SetFocus();
}

void CReservedDataDlg::OnBnClickedDeleteExtPackageBtn()
{
    UpdateData( TRUE );

    int nItem = ExtPackagesListCtrl.GetSelectionMark();
    if ( nItem != CB_ERR )
    {
        CString sPackageId = ExtPackagesListCtrl.GetItemText( nItem, 0 );
        CExtendedReservedData& ERD = m_LocalTimDescriptor.ExtendedReservedData();
        
        // select the package so all of it's fields can be removed
        t_PairList* pFields = 0;
        if ( sPackageId == sClockEnable.c_str() )
            pFields = &ERD.ClockEnableFields;
        else if ( sPackageId == sDDRGeometry.c_str() )
            pFields = &ERD.DDRGeometryFields;
        else if ( sPackageId == sDDRTiming.c_str() )
            pFields = &ERD.DDRTimingFields;
        else if ( sPackageId == sDDRCustom.c_str() )
            pFields = &ERD.DDRCustomFields;
        else if ( sPackageId == sFrequency.c_str() )
            pFields = &ERD.FrequencyFields;
        else if ( sPackageId == sVoltages.c_str() )
            pFields = &ERD.VoltagesFields;
        else if ( sPackageId == sConfigMemoryControl.c_str() )
            pFields = &ERD.ConfigMemoryControlFields;
        else if ( sPackageId == sTrustZoneRegid.c_str() )
            pFields = &ERD.TrustZoneRegidFields;
        else if ( sPackageId == sTrustZone.c_str() )
            pFields = &ERD.TrustZoneFields;
        else if ( sPackageId == sOpDiv.c_str() )
            pFields = &ERD.OpDivFields;
        else if ( sPackageId == sOpMode.c_str() )
            pFields = &ERD.OpModeFields;

        if ( pFields )
        {
            // delete the fields and clear the list
            t_PairListIter iter = pFields->begin();
            while ( iter != pFields->end() )
            {
                delete *iter;
                iter++;
            }
            pFields->clear();
        }
    
        CErdBase* pErdBase = (CErdBase*)ExtPackagesListCtrl.GetItemData( nItem );
        if ( pErdBase )
        {
            if ( pErdBase->ErdPkgType() == CErdBase::DDR_INITIALIZATION_ERD )
            {
                CDDRInitialization* pDDROrig = dynamic_cast<CDDRInitialization*>( pErdBase );
                if ( pDDROrig )
                {
                    // update all consumers from the old PID to the new PID
                    t_ConsumerIDVecIter Iter = ERD.m_Consumers.begin();
                    while ( Iter != ERD.m_Consumers.end() )
                    {
                        t_stringVectorIter PIDIter = (*Iter)->m_PIDs.begin();
                        while ( PIDIter != (*Iter)->m_PIDs.end() )
                        {
                            if ( *(*PIDIter) == pDDROrig->m_sDdrPID )
                            {
                                delete (*PIDIter);
                                (*Iter)->m_PIDs.erase(PIDIter);
                                break;
                            }

                            PIDIter++;
                        }
                        Iter++;
                    }
                }
            }
            
            if ( pErdBase->ErdPkgType() == CErdBase::CONSUMER_ID_ERD )
            {
                t_ConsumerIDVecIter Iter = ERD.m_Consumers.begin();
                while( Iter != ERD.m_Consumers.end() )
                {
                    if ( pErdBase == *Iter )
                    {
                        // found it
                        ERD.m_Consumers.erase( Iter );
                        break;
                    }
                    else
                        Iter++;
                }
                delete pErdBase;
            }
            else if ( ERD.ErdVec.size() > 0 )
            {
                t_ErdBaseVectorIter ErdIter = ERD.ErdVec.begin();
                while( ErdIter != ERD.ErdVec.end() )
                {
                    if ( pErdBase == *ErdIter )
                    {
                        // found it
                        ERD.ErdVec.erase( ErdIter );
                        break;
                    }
                    else
                        ErdIter++;
                }
                delete pErdBase;
            }
        }

        m_LocalTimDescriptor.ReservedChanged(true);
        m_LocalTimDescriptor.NotWritten(true);
        RefreshState();
    }
}


void CReservedDataDlg::RefreshExtPackage( int& nItem, const string& sPackage, t_PairList& Fields, t_stringVector& FieldNames )
{
    CTimLib TimLib;
    ExtPackagesListCtrl.InsertItem( nItem, sPackage.c_str() );
//	CErdBase* pErd = (CErdBase*)ExtPackagesListCtrl.GetItemData( nItem );
    stringstream ss;
    ss << ((Fields.size()*4*2) +8); //4 bytes for value + 4 bytes for field id +8 for WRAH
//	ss << pErd->PackageSize();
    ExtPackagesListCtrl.SetItemText( nItem, 1, ss.str().c_str() );
    ExtPackagesListCtrl.SetItemData( nItem, (DWORD_PTR)(&Fields) );

    string sDataBlock;
    t_PairListIter iter = Fields.begin();
    while( iter != Fields.end() )
    {
        if ( (*iter)->first >= FieldNames.size() )
        {
            t_PairListIter NextIter = iter;
            NextIter++;
            // remove invalid fields
            delete (*iter);
            Fields.remove( *iter );
            iter = NextIter;
            continue;
        }
        else
        {
            sDataBlock += *(FieldNames[(*iter)->first]);
            sDataBlock += ":";
            sDataBlock += TimLib.HexFormattedAscii((*iter)->second);
            sDataBlock += ";";
        }
        iter++;
    }
    ExtPackagesListCtrl.SetItemText( nItem, 2, sDataBlock.c_str() );
    nItem++;
}

void CReservedDataDlg::RefreshExtPackage( int& nItem, t_ErdBaseVectorIter& ErdIter )
{
    ExtPackagesListCtrl.InsertItem( nItem, (*ErdIter)->PackageName().c_str() );
    stringstream ss;
    ss << (*ErdIter)->PackageSize();
    ExtPackagesListCtrl.SetItemText( nItem, 1, ss.str().c_str() );
    t_stringVector& FieldNames = (*ErdIter)->FieldNames();
    t_stringVector& FieldValues = (*ErdIter)->FieldValues();

    ss.str("");
    
    if ( (*ErdIter)->ErdPkgType() == CErdBase::DDR_INITIALIZATION_ERD )
    {
        ss << *FieldNames[CDDRInitialization::DDR_PID] << ":";
        ss << ((CDDRInitialization*)(*ErdIter))->m_sDdrPID << ";";
    }
    else if ( (*ErdIter)->ErdPkgType() == CErdBase::TZ_INITIALIZATION_ERD )
    {
        ss << *FieldNames[CTzInitialization::TZ_PID] << ":";
        ss << ((CTzInitialization*)(*ErdIter))->m_sTzPID << ";";
    }
    else if ( (*ErdIter)->ErdPkgType() == CErdBase::CONSUMER_ID_ERD )
    {
        ss << *FieldNames[CConsumerID::CID] << ":";
        ss << *(((CConsumerID*)(*ErdIter))->psCID()) << ";";

        t_stringVectorIter Iter = ((CConsumerID*)(*ErdIter))->m_PIDs.begin();
        while ( Iter != ((CConsumerID*)(*ErdIter))->m_PIDs.end() )
        {
            ss << *FieldNames[CConsumerID::PID] << ":";
            ss << (*Iter)->c_str() << ";";
            Iter++;
        }
    }
    else if ( (*ErdIter)->ErdPkgType() == CErdBase::USBVENDORREQ_ERD )
    {
        for ( unsigned int i = 0; i < (*ErdIter)->MaxFieldNum()-1; i++ )
            ss << *FieldNames[i] << ":" << *FieldValues[i] << ";";

        ss << "DATA:";
        t_stringVectorIter Iter = ((CUsbVendorReq*)(*ErdIter))->DataList().begin();
        while ( Iter != ((CUsbVendorReq*)(*ErdIter))->DataList().end() )
        {
            if ( Iter != ((CUsbVendorReq*)(*ErdIter))->DataList().begin() )
                ss << "," << *(*Iter++);
            else
                ss << *(*Iter++);
        }
    }
    else
    {
        for ( unsigned int i = 0; i < (*ErdIter)->MaxFieldNum(); i++ )
            ss << *FieldNames[i] << ":" << *FieldValues[i] << ";";
    }

    ExtPackagesListCtrl.SetItemText( nItem, 2, ss.str().c_str() );
    ExtPackagesListCtrl.SetItemData( nItem, (DWORD_PTR)(*ErdIter) );
    nItem++;
}

void CReservedDataDlg::RefreshExtPackage( int& nItem, t_ConsumerIDVecIter& ErdIter )
{
    ExtPackagesListCtrl.InsertItem( nItem, (*ErdIter)->PackageName().c_str() );
    stringstream ss;
    ss << (*ErdIter)->PackageSize();
    ExtPackagesListCtrl.SetItemText( nItem, 1, ss.str().c_str() );
    t_stringVector& FieldNames = (*ErdIter)->FieldNames();
    t_stringVector& FieldValues = (*ErdIter)->FieldValues();

    ss.str("");
    if ( (*ErdIter)->ErdPkgType() == CErdBase::CONSUMER_ID_ERD )
    {
        ss << *FieldNames[CConsumerID::CID] << ":";
        ss << *(((CConsumerID*)(*ErdIter))->psCID()) << ";";

        t_stringVectorIter Iter = ((CConsumerID*)(*ErdIter))->m_PIDs.begin();
        while ( Iter != ((CConsumerID*)(*ErdIter))->m_PIDs.end() )
        {
            ss << *FieldNames[CConsumerID::PID] << ":";
            ss << (*Iter)->c_str() << ";";
            Iter++;
        }
    }
    else
    {
        for ( unsigned int i = 0; i < (*ErdIter)->MaxFieldNum(); i++ )
            ss << *FieldNames[i] << ":" << *FieldValues[i] << ";";
    }

    ExtPackagesListCtrl.SetItemText( nItem, 2, ss.str().c_str() );
    ExtPackagesListCtrl.SetItemData( nItem, (DWORD_PTR)(*ErdIter) );
    nItem++;
}

void CReservedDataDlg::OnContextMenu(CWnd* pWnd, CPoint /*point*/)
{
    CCommentDlg Dlg(this);
    int nItem = -1;
    CErdBase* pErd = 0;
    t_PairList* pFields = 0;
    CString sPackageId;
    CExtendedReservedData& ERD = m_LocalTimDescriptor.ExtendedReservedData();

    int iCtlId = pWnd->GetDlgCtrlID();
    switch ( iCtlId )
    {
        case IDC_PACKAGES_LIST_CTRL:
            nItem = PackagesListCtrl.GetSelectionMark();
            if ( nItem != -1 )
            {
                CReservedPackageData* pData = (CReservedPackageData*)PackagesListCtrl.GetItemData( nItem );
                if ( pData )
                    Dlg.Display( pData );
            }
            break;

        case IDC_EXT_RESERVED_DATA_PACKAGES_LIST:
            nItem = ExtPackagesListCtrl.GetSelectionMark();
            if ( nItem != -1 )
            {
                pErd = (CErdBase*)(ExtPackagesListCtrl.GetItemData( nItem ));
                if ( pErd )
                    Dlg.Display( pErd );
                else
                {
                    sPackageId = ExtPackagesListCtrl.GetItemText( nItem, 0 );
                    int pos = sPackageId.Find( " (" );
                    if ( pos > -1 )
                        sPackageId = sPackageId.Left( pos-1 );

                    // select the package so all of it's fields can be removed
                    if ( sPackageId == sClockEnable.c_str() )
                        pFields = &ERD.ClockEnableFields;
                    else if ( sPackageId == sDDRGeometry.c_str() )
                        pFields = &ERD.DDRGeometryFields;
                    else if ( sPackageId == sDDRTiming.c_str() )
                        pFields = &ERD.DDRTimingFields;
                    else if ( sPackageId == sDDRCustom.c_str() )
                        pFields = &ERD.DDRCustomFields;
                    else if ( sPackageId == sFrequency.c_str() )
                        pFields = &ERD.FrequencyFields;
                    else if ( sPackageId == sVoltages.c_str() )
                        pFields = &ERD.VoltagesFields;
                    else if ( sPackageId == sConfigMemoryControl.c_str() )
                        pFields = &ERD.ConfigMemoryControlFields;
                    else if ( sPackageId == sTrustZoneRegid.c_str() )
                        pFields = &ERD.TrustZoneRegidFields;
                    else if ( sPackageId == sTrustZone.c_str() )
                        pFields = &ERD.TrustZoneFields;
                    else if ( sPackageId == sOpDiv.c_str() )
                        pFields = &ERD.OpDivFields;
                    else if ( sPackageId == sOpMode.c_str() )
                        pFields = &ERD.OpModeFields;

                    if ( pFields )
                        Dlg.Display( pFields );
                }
            }
            break;

        default:
            break;
    }
}

