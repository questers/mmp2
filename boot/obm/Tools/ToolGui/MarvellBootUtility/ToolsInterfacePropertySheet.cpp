/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "ToolsInterfacePropertySheet.h"
#include "TimDescriptorPropPage.h"
#include "FBFDownloadPropertyPage.h"

#pragma warning ( disable : 4996 ) // Disable warning messages

CToolsInterfacePropertySheet::CToolsInterfacePropertySheet(void)
    : CPropertySheet("Tool Interface Configuration")
{
}

CToolsInterfacePropertySheet::~CToolsInterfacePropertySheet(void)
{
}

BEGIN_MESSAGE_MAP(CToolsInterfacePropertySheet, CPropertySheet)
END_MESSAGE_MAP()


BOOL CToolsInterfacePropertySheet::OnInitDialog()
{
    BOOL bResult = CPropertySheet::OnInitDialog();

    CTabCtrl* pTab = GetTabControl();

    TC_ITEM ti;
    ti.mask = TCIF_TEXT;
    char szCaption[40]={0};
#if TRUSTED
    strcpy( szCaption, "DKB TIM Descriptor" );
#else
    strcpy( szCaption, "DKB NTIM Descriptor" );
#endif
    ti.pszText = szCaption;
    VERIFY (pTab->SetItem (0, &ti));

#if TRUSTED
    strcpy( szCaption, "OBM/Other TIM Descriptor" );
#else
    strcpy( szCaption, "OBM/Other NTIM Descriptor" );
#endif
    ti.pszText = szCaption;
    VERIFY (pTab->SetItem (1, &ti));

//	RefreshState();

    return bResult;
}

void CToolsInterfacePropertySheet::RefreshState()
{
    if ( m_hWnd )
    {
        if ( theApp.m_pDKBTimDescriptorPropPage && theApp.m_pDKBTimDescriptorPropPage->m_hWnd )
        {
            CDownloaderInterface& DownloaderInterface = theApp.m_pDKBTimDescriptorPropPage->DownloaderInterface;
            if ( DownloaderInterface.TimFilePath().length() > 0 )
            {
                DownloaderInterface.TimDescriptorParser.GetTimDescriptorLines( DownloaderInterface.ImageBuild.CommandLineParser );
                DownloaderInterface.TimDescriptorParser.ParseDescriptor( DownloaderInterface );
            }
            theApp.m_pDKBTimDescriptorPropPage->RefreshState();
        }

        if ( theApp.m_pOBMTimDescriptorPropPage && theApp.m_pOBMTimDescriptorPropPage->m_hWnd )
        {
            CDownloaderInterface& DownloaderInterface = theApp.m_pOBMTimDescriptorPropPage->DownloaderInterface;
            if ( DownloaderInterface.TimFilePath().length() > 0 )
            {
                DownloaderInterface.TimDescriptorParser.GetTimDescriptorLines( DownloaderInterface.ImageBuild.CommandLineParser );
                DownloaderInterface.TimDescriptorParser.ParseDescriptor( DownloaderInterface );
            }
            theApp.m_pOBMTimDescriptorPropPage->RefreshState();
        }

        if ( theApp.m_pFBFDownloadPropertyPage && theApp.m_pFBFDownloadPropertyPage->m_hWnd )
            theApp.m_pFBFDownloadPropertyPage->RefreshState();
    }
}

