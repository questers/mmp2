/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once


// CImportTimBinaryDlg dialog

class CImportTimBinaryDlg : public CDialog
{
	DECLARE_DYNAMIC(CImportTimBinaryDlg)

public:
	CImportTimBinaryDlg(CTimDescriptor& rTimDescriptor, CWnd* pParent = NULL);   // standard constructor
	virtual ~CImportTimBinaryDlg();

// Dialog Data
	enum { IDD = IDD_IMPORT_TIM_BINARY_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	CListCtrl ImagesListCtrl;
	CString sBinaryFilePath;
	
	virtual BOOL OnInitDialog();
	afx_msg void OnLvnItemActivateBinImagesListCtrl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBinaryFilePathBrowseBtn();
	afx_msg void OnEnChangeBinaryFilePathEdt();		

	CTimDescriptor TimDescriptor;
	CImageDescription* m_pImage;

	void RefreshState();
	void UpdateControls();
	afx_msg void OnBnClickedOk();
};
