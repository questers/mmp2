/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

// CKeysDlg dialog

class CKeysDlg : public CDialog
{
	DECLARE_DYNAMIC(CKeysDlg)

public:
	CKeysDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CKeysDlg();

// Dialog Data
	enum { IDD = IDD_KEYS_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	CButton EditKeyBtn;
	CButton RemoveKeyBtn;
	CListCtrl KeysListCtrl;

	afx_msg void OnBnClickedRemoveKeyBtn();
	afx_msg void OnBnClickedEditKeyBtn();
	afx_msg void OnBnClickedInsertKeyBtn();
	afx_msg void OnBnClickedKeyMoveUpBtn();
	afx_msg void OnBnClickedKeyMoveDownBtn();
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);

public:
	// temp descriptor for use by dialog
	void LocalTimDescriptor( CTimDescriptor& TimDescriptor ) { m_LocalTimDescriptor = TimDescriptor; }
	CTimDescriptor& LocalTimDescriptor() { return m_LocalTimDescriptor; }
	void UpdateChangeMarkerInTitle();

private:
	void RefreshState();

	CTimDescriptor m_LocalTimDescriptor;
};
