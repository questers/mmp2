/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// LaunchDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "LaunchDlg.h"
#include "Target.h"

#pragma warning ( disable : 4996 ) // Disable warning messages

// CLaunchDlg dialog

IMPLEMENT_DYNAMIC(CLaunchDlg, CDialog)

CLaunchDlg::CLaunchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLaunchDlg::IDD, pParent)
{

}

CLaunchDlg::~CLaunchDlg()
{
}

void CLaunchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TARGET_LIST, TargetListCtrl);
	DDX_Control(pDX, IDC_DKB_DOWNLOAD_CHK, DkbDownloadChk);
	DDX_Control(pDX, IDC_OBM_DOWNLOAD_CHK, ObmDownloadChk);
	DDX_Control(pDX, IDC_FBF_DOWNLOAD_CHK, FbfDownloadChk);
	DDX_Control(pDX, IDC_LAUNCH_DISC_CHK, LaunchOnDiscoveryChk);
}


BEGIN_MESSAGE_MAP(CLaunchDlg, CDialog)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_LAUNCH_TARGET_BTN, &CLaunchDlg::OnBnClickedLaunchTargetBtn)
	ON_BN_CLICKED(IDC_REFRESH_TARGETS_BTN, &CLaunchDlg::OnBnClickedRefreshTargetsBtn)
	ON_BN_CLICKED(IDC_LAUNCH_ALL_BTN, &CLaunchDlg::OnBnClickedLaunchAllBtn)
	ON_BN_CLICKED(IDC_TERMINATE_TARGET_BTN, &CLaunchDlg::OnBnClickedTerminateTargetBtn)
	ON_BN_CLICKED(IDC_TERMINATE_ALL_BTN, &CLaunchDlg::OnBnClickedTerminateAllBtn)
	ON_BN_CLICKED(IDC_DKB_DOWNLOAD_CHK, &CLaunchDlg::OnBnClickedDkbDownloadChk)
	ON_BN_CLICKED(IDC_OBM_DOWNLOAD_CHK, &CLaunchDlg::OnBnClickedObmDownloadChk)
	ON_BN_CLICKED(IDC_FBF_DOWNLOAD_CHK, &CLaunchDlg::OnBnClickedFbfDownloadChk)
	ON_BN_CLICKED(IDC_LAUNCH_DISC_CHK, &CLaunchDlg::OnBnClickedLaunchDiscChk)
END_MESSAGE_MAP()


// CLaunchDlg message handlers

BOOL CLaunchDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	TargetListCtrl.SetExtendedStyle( LVS_EX_TRACKSELECT
									//| LVS_EX_FULLROWSELECT
									| LVS_EX_LABELTIP 
									| LVS_EX_HEADERDRAGDROP
									| LVS_EX_ONECLICKACTIVATE);

	TargetListCtrl.InsertColumn(0,"Target ID",0,475,0); 
	TargetListCtrl.InsertColumn(1,"Run State",0,100,0);

	RefreshState();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CLaunchDlg::RefreshState()
{
	if ( m_hWnd )
	{
		TargetListCtrl.DeleteAllItems();

		t_TargetsList& Targets = theApp.TargetList();
		CTarget* pTarget = 0;
		t_TargetsIter iter = Targets.begin();

		while( iter != Targets.end() )
		{
			pTarget = *iter;
			int idx = TargetListCtrl.GetItemCount();
			TargetListCtrl.InsertItem( idx, pTarget->InterfaceName().c_str() );
			TargetListCtrl.SetItemData( idx, (DWORD_PTR)pTarget );
			switch ( pTarget->TargetState() )
			{
				case CTarget::TARGET_IDLE:
					TargetListCtrl.SetItemText( idx, 1, "Target Idle" );
					break;
				case CTarget::TARGET_DOWNLOADING:
					TargetListCtrl.SetItemText( idx, 1, "Target Downloading" );
					break;
				case CTarget::TARGET_TERMINATED:
					TargetListCtrl.SetItemText( idx, 1, "Target Terminated" );
					break;
				case CTarget::TARGET_ERROR:		
					TargetListCtrl.SetItemText( idx, 1, "Target Error" );
					break;
			}
			iter++;
		}
		TargetListCtrl.UpdateWindow();

		char buf[50] = {0};

		DkbDownloadChk.SetCheck( theApp.DKBDownloaderInterface().IncludeInDownload() ? BST_CHECKED : BST_UNCHECKED );
		ObmDownloadChk.SetCheck( theApp.OBMDownloaderInterface().IncludeInDownload() ? BST_CHECKED : BST_UNCHECKED );
		FbfDownloadChk.SetCheck( theApp.FBFDownloaderInterface().IncludeInDownload() ? BST_CHECKED : BST_UNCHECKED );
		LaunchOnDiscoveryChk.SetCheck( theApp.DownloadController().m_bLaunchOnDiscovery ? BST_CHECKED : BST_UNCHECKED );
	}
}

void CLaunchDlg::OnBnClickedRefreshTargetsBtn()
{
	theApp.RefreshTargetsList();
	RefreshState();
}

void CLaunchDlg::OnBnClickedLaunchTargetBtn()
{
	bool bDkbDownloadChk = (DkbDownloadChk.GetCheck() == BST_CHECKED);
	bool bObmDownloadChk = (ObmDownloadChk.GetCheck() == BST_CHECKED);
	bool bFbfDownloadChk = (FbfDownloadChk.GetCheck() == BST_CHECKED);

	if ( bDkbDownloadChk || bObmDownloadChk || bFbfDownloadChk )
	{
		theApp.DownloadController().m_bDkbDownload = bDkbDownloadChk;
		theApp.DownloadController().m_bObmDownload = bObmDownloadChk;
		theApp.DownloadController().m_bFbfDownload = bFbfDownloadChk;

		POSITION pos = TargetListCtrl.GetFirstSelectedItemPosition();
		if ( pos != NULL )
		{
			int idx = TargetListCtrl.GetNextSelectedItem( pos );
			if ( idx != LB_ERR )
			{
				CTarget* pTarget = (CTarget*)TargetListCtrl.GetItemData(idx);
				if ( pTarget )
				{
					if ( pTarget->TargetState() == CTarget::TARGET_IDLE )
					{
						theApp.DownloadController().ConditionalLaunchDownload( pTarget );
						RefreshState();
					}
				}
			}
		}
	}
	else
		AfxMessageBox( "Nothing selected to download." );
}

void CLaunchDlg::OnBnClickedTerminateTargetBtn()
{
	POSITION pos = TargetListCtrl.GetFirstSelectedItemPosition();
	if ( pos != NULL )
	{
		int idx = TargetListCtrl.GetNextSelectedItem( pos );
		if ( idx != LB_ERR )
		{
			theApp.StopDownloadTarget( TargetListCtrl.GetItemText(idx, 0 ) );
		}
	}
}

void CLaunchDlg::OnBnClickedLaunchAllBtn()
{
	theApp.StopDownloadAllTargets();

	bool bDkbDownloadChk = (DkbDownloadChk.GetCheck() == BST_CHECKED);
	bool bObmDownloadChk = (ObmDownloadChk.GetCheck() == BST_CHECKED);
	bool bFbfDownloadChk = (FbfDownloadChk.GetCheck() == BST_CHECKED);

	if ( bDkbDownloadChk || bObmDownloadChk || bFbfDownloadChk )
	{
		theApp.DownloadController().m_bDkbDownload = bDkbDownloadChk;
		theApp.DownloadController().m_bObmDownload = bObmDownloadChk;
		theApp.DownloadController().m_bFbfDownload = bFbfDownloadChk;

		theApp.StartDownloadAllTargets();
	}
	else
		AfxMessageBox( "Nothing selected to download." );
}

void CLaunchDlg::OnBnClickedTerminateAllBtn()
{
	theApp.StopDownloadAllTargets();
}

void CLaunchDlg::OnBnClickedDkbDownloadChk()
{
	string& sCommandLineArgs = theApp.DKBDownloaderInterface().CommandLineArgs();
	// simple test for missing paths in command line
	if ( (sCommandLineArgs.find("<") == string::npos && sCommandLineArgs.find(">") == string::npos )
		&& ( theApp.DKBDownloaderInterface().TimFilePath().length() > 0 ) )
	{
		theApp.DKBDownloaderInterface().IncludeInDownload( DkbDownloadChk.GetCheck() == BST_CHECKED ? true : false );
		theApp.DownloadController().m_bDkbDownload = ( DkbDownloadChk.GetCheck() == BST_CHECKED ? true : false );
	}
	else
	{
		AfxMessageBox( "TIM Based Download DKB command line is not valid." );
		DkbDownloadChk.SetCheck( BST_UNCHECKED );
	}
}

void CLaunchDlg::OnBnClickedObmDownloadChk()
{
	string& sCommandLineArgs = theApp.OBMDownloaderInterface().CommandLineArgs();
	// simple test for missing paths in command line
	if ( ( sCommandLineArgs.find("<") == string::npos && sCommandLineArgs.find(">") == string::npos )
		 && ( theApp.OBMDownloaderInterface().TimFilePath().length() > 0 ) )
	{
		theApp.OBMDownloaderInterface().IncludeInDownload( ObmDownloadChk.GetCheck() == BST_CHECKED ? true : false );
		theApp.DownloadController().m_bObmDownload = ( ObmDownloadChk.GetCheck() == BST_CHECKED ? true : false );
	}
	else
	{
		AfxMessageBox( "TIM Based Download OBM command line is not valid." );
		ObmDownloadChk.SetCheck( BST_UNCHECKED );
	}
}

void CLaunchDlg::OnBnClickedFbfDownloadChk()
{
	string& sCommandLineArgs = theApp.FBFDownloaderInterface().CommandLineArgs();
	// simple test for missing paths in command line
	if ( ( sCommandLineArgs.find("<") == string::npos && sCommandLineArgs.find(">") == string::npos )
		&& ( theApp.FBFDownloaderInterface().FbfFilePath().length() > 0 ) )
	{
		theApp.FBFDownloaderInterface().IncludeInDownload( FbfDownloadChk.GetCheck() == BST_CHECKED ? true : false );
		theApp.DownloadController().m_bFbfDownload = ( FbfDownloadChk.GetCheck() == BST_CHECKED ? true : false );
	}
	else
	{
		AfxMessageBox( "FBF File Images Download command line is not valid." );
		FbfDownloadChk.SetCheck( BST_UNCHECKED );
	}
}


void CLaunchDlg::OnBnClickedLaunchDiscChk()
{
	theApp.DownloadController().m_bLaunchOnDiscovery = ( LaunchOnDiscoveryChk.GetCheck() == BST_CHECKED ? true : false );
}
