/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "DownloaderInterface.h"
#include "afxwin.h"
#include "EditListCtrl.h"
#include "afxcmn.h"


// CTimDownloaderDlg dialog

class CTimDownloaderDlg : public CDialog
{
	DECLARE_DYNAMIC(CTimDownloaderDlg)

public:
	CTimDownloaderDlg(CDownloaderInterface& rDownloaderInterface);
	virtual ~CTimDownloaderDlg();

// Dialog Data
	enum { IDD = IDD_TIM_DOWNLOADER_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnEnChangeUartDelay();
	afx_msg void OnEnChangeUsbPacketSize();
	afx_msg void OnBnClickedVerboseModeChk();
	afx_msg void OnCbnSelchangeFlashTypeCombo();
	afx_msg void OnBnClickedRunDebugBootCmdChk();
	afx_msg void OnBnClickedJtagKeyFileBrowseBtn();
	afx_msg void OnEnChangeJtagKeyFilePath();
	afx_msg void OnEnChangeWtptpExePath();
	afx_msg void OnCbnSelchangeMsgModeCb();
	afx_msg void OnCbnSelchangeComPortCb();
	afx_msg void OnCbnSelchangeBaudRateCb();
	afx_msg void OnCbnSelchangePortTypeCb();
	afx_msg void OnBnClickedWtptpExeBrowseBtn();
	afx_msg void OnBnClickedWtptpResetBtn();
	afx_msg void OnEnChangeWtptpAdditionalOptionsEdt();
	afx_msg void OnBnClickedWtptpDownloadBtn();
	afx_msg void OnBnClickedWtptpDownloadStopBtn();
	afx_msg void OnBnClickedPartitionBinaryBrowseBtn();
	afx_msg void OnEnChangePartitionFilePathEdt();
	afx_msg void OnBnClickedPartitionBinaryChk();
	afx_msg void OnBnClickedDownloaderAdditionalOptionsChk();
	afx_msg void OnBnClickedUsbDownloadBtn();
	afx_msg void OnBnClickedDownloadProcessorTypeChk();
    afx_msg void OnBnClickedUploadSpecFileChk();
    afx_msg void OnEnChangeUploadSpecFilePathEdt();
    afx_msg void OnBnClickedUploadSpecFileBrowseBtn();
	afx_msg void OnBnClickedOk();
	
	BOOL bVerboseMode;  // adds printing of additional debug messages
	BOOL bRunDebugCmd;
	BOOL bPartitionBinary;
	BOOL bAdditionalOptions;
	BOOL bBinaryImageFiles;
	BOOL bProcessorType;
    BOOL bUploadSpecFile;

	CString sUartDelayMs;
	CString sJtagKeyFilePath;
	CString sWtptpExePath;
	CString sTimBinFilePath;
	CString sPartitionBinaryFilePath;
	CString sUsbPacketSize;
    CString sUploadSpecFilePath;
	
	CComboBox FlashTypeCB;	// FlashType code for supported flashes
	CComboBox MsgModeCB;
	CComboBox PortTypeCB;
	CComboBox ComPortCB;
	CComboBox BaudRateCB;

	CEdit UartDelayEdit;
	CEditListCtrl ImagesListCtrl;
	CEdit PartitionBinaryFilePathEdt;
	CEdit AdditionalOptionsEdt;
	CEdit UsbPacketSizeEdt;
    CEdit UploadSpecFilePathEdt;

	CButton PartitionBinaryBrowseBtn;
	CButton UartDownloadBtn;
	CButton UartStopDownloadBtn;
	CButton UsbDownloadBtn;
    CButton UploadSpecFilePathBrowseBtn;

public:
	void RefreshState();
	void UpdateChangeMarkerInTitle();

private:
	void UpdateCommandLineText();
	void UpdateControls();
	CString& InputImageFilePath( int idx );

private:
	CDownloaderInterface&	DownloaderInterface;
	CString					sCommandLineTextStr;  // displays command line text for use in launching WPTPT.exe
	CString					sAdditionalOptions;
};
