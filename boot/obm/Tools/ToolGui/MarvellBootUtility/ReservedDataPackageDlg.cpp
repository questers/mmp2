/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// ReservedDataPackageDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "ReservedDataPackageDlg.h"
#include "TimLib.h"
#include "CommentDlg.h"

#define COMMENT     1

#define BLANK									0
#define AUTOBIND_ID								1
#define DDRID_ID								2
#define ESCAPESEQ_ID							3
#define GPIOID_ID								4
#define OEMCUSTOM_ID							5
#define RESUMEBLID_ID							6
#define TBR_XFER_ID								7
#define	UARTID_ID								8
#define	USBID_ID								9
#define CORE_ID									10
#define	USB_CONFIG_DESCRIPTOR_ID				11
#define	USB_DEFAULT_STRING_DESCRIPTOR_ID		12
#define	USB_DEVICE_DESCRIPTOR_ID				13
#define	USB_ENDPOINT_DESCRIPTOR_ID				14
#define	USB_INTERFACE_DESCRIPTOR_ID				15
#define	USB_INTERFACE_STRING_DESCRIPTOR_ID		16
#define	USB_LANGUAGE_STRING_DESCRIPTOR_ID		17
#define	USB_MANUFACTURER_STRING_DESCRIPTOR_ID	18
#define	USB_PRODUCT_STRING_DESCRIPTOR_ID		19
#define	USB_SERIAL_STRING_DESCRIPTOR_ID			20
#define USB_VENDORREQ_ID						21
#define NOMONITOR_ID							22
#define CLKE_ID									23
#define CMCC_ID									24
#define DDRG_ID									25
#define DDRT_ID									26
#define DDRC_ID									27
#define FREQ_ID									28
#define OPDIV_ID								29
#define OPMODE_ID								30
#define TZID_ID									31
#define TZON_ID									32
#define VOLT_ID									33

// CReservedDataPackageDlg dialog

IMPLEMENT_DYNAMIC(CReservedDataPackageDlg, CDialog)

CReservedDataPackageDlg::CReservedDataPackageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CReservedDataPackageDlg::IDD, pParent)
{
	sPackageHeaderIdTag = _T("");
	iCurSel = -1;
}

CReservedDataPackageDlg::~CReservedDataPackageDlg()
{
}

void CReservedDataPackageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PACKAGE_HEADER_ID, PackageHeaderIdCb);
	DDX_Control(pDX, IDC_PACKAGE_DATA_LIST_CTRL, PackageDataListCtrl);
	DDX_Text(pDX, IDC_PACKAGE_HEADER_ID_TAG_EDT, sPackageHeaderIdTag);
	DDX_Control(pDX, IDC_PACKAGE_HEADER_ID_TAG_EDT, PackageHeaderIdTagEdt);
	DDX_Control(pDX, IDC_PACKAGE_INSERT_ITEM_BTN, PackageInsertItemBtn);
	DDX_Control(pDX, IDC_PACKAGE_DELETE_ITEM_BTN, PackageDeleteItemBtn);
	DDX_Control(pDX, IDC_PACKAGE_DATE_MOVE_UP_BTN, MoveUpBtn);
	DDX_Control(pDX, IDC_PACKAGE_DATA_MOVE_DOWN_BTN, MoveDownBtn);

	// updates marker in title
	UpdateChangeMarkerInTitle();
	theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CReservedDataPackageDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CReservedDataPackageDlg::OnBnClickedOk)
	ON_EN_CHANGE(IDC_PACKAGE_HEADER_ID_TAG_EDT, &CReservedDataPackageDlg::OnEnChangePackageHeaderIdTagEdt)
	ON_CBN_SELCHANGE(IDC_PACKAGE_HEADER_ID, &CReservedDataPackageDlg::OnCbnSelchangePackageHeaderId)
	ON_CBN_EDITCHANGE(IDC_PACKAGE_HEADER_ID, &CReservedDataPackageDlg::OnCbnEditchangePackageHeaderId)
	ON_BN_CLICKED(IDC_PACKAGE_DELETE_ITEM_BTN, &CReservedDataPackageDlg::OnBnClickedPackageDeleteItem)
	ON_BN_CLICKED(IDC_PACKAGE_INSERT_ITEM_BTN, &CReservedDataPackageDlg::OnBnClickedPackageInsertItemBtn)
	ON_BN_CLICKED(IDC_PACKAGE_DATE_MOVE_UP_BTN, &CReservedDataPackageDlg::OnBnClickedPackageDataMoveUpBtn)
	ON_BN_CLICKED(IDC_PACKAGE_DATA_MOVE_DOWN_BTN, &CReservedDataPackageDlg::OnBnClickedPackageDataMoveDownBtn)
	ON_NOTIFY(LVN_ITEMACTIVATE, IDC_PACKAGE_DATA_LIST_CTRL, &CReservedDataPackageDlg::OnLvnItemActivatePackageDataListCtrl)
	ON_NOTIFY(NM_KILLFOCUS, IDC_PACKAGE_DATA_LIST_CTRL, &CReservedDataPackageDlg::OnNMKillfocusPackageDataListCtrl)
	ON_WM_CONTEXTMENU()
	ON_BN_CLICKED(IDCANCEL, &CReservedDataPackageDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CReservedDataPackageDlg message handlers

BOOL CReservedDataPackageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	PackageDataListCtrl.SetExtendedStyle( //  LVS_EX_TRACKSELECT
										  //  LVS_EX_BORDERSELECT
										  LVS_EX_FULLROWSELECT 
										  | LVS_EX_HEADERDRAGDROP 
										  | LVS_EX_ONECLICKACTIVATE 
										  | LVS_EX_LABELTIP 
										  //| LVS_EX_GRIDLINES
										 );

	PackageDataListCtrl.InsertColumn(0,"Longword Hex Data", LVCFMT_LEFT, 110, -1 );
	PackageDataListCtrl.InsertColumn(1,"Comment", LVCFMT_LEFT, 596, -1 );
	
	PackageDataListCtrl.EditControlType[0] = HexEdit;
	PackageDataListCtrl.EditControlType[1] = Edit;

	CTimLib TimLib;

	// new format for packages, may need to add support for old format as well based on version #
	PackageHeaderIdCb.InsertString(BLANK,""); PackageHeaderIdCb.SetItemDataPtr(BLANK, new string(""));
	PackageHeaderIdCb.InsertString(AUTOBIND_ID,"AUTOBIND"); PackageHeaderIdCb.SetItemDataPtr(AUTOBIND_ID, new string( TimLib.HexFormattedAscii(AUTOBIND)) );
	PackageHeaderIdCb.InsertString(DDRID_ID,"DDRID"); PackageHeaderIdCb.SetItemDataPtr(DDRID_ID, new string( TimLib.HexFormattedAscii(DDRID)) );
	PackageHeaderIdCb.InsertString(ESCAPESEQ_ID,"ESCAPE_SEQUENCE"); PackageHeaderIdCb.SetItemDataPtr(ESCAPESEQ_ID, new string(TimLib.HexFormattedAscii(ESCAPESEQID)) );
	PackageHeaderIdCb.InsertString(GPIOID_ID,"GPIOID"); PackageHeaderIdCb.SetItemDataPtr(GPIOID_ID, new string(TimLib.HexFormattedAscii(GPIOID)) );
	PackageHeaderIdCb.InsertString(OEMCUSTOM_ID,"CUST"); PackageHeaderIdCb.SetItemDataPtr(OEMCUSTOM_ID, new string(TimLib.HexFormattedAscii(OEMCUSTOMID)) );
	PackageHeaderIdCb.InsertString(RESUMEBLID_ID,"RESUMEBLID"); PackageHeaderIdCb.SetItemDataPtr(RESUMEBLID_ID, new string(TimLib.HexFormattedAscii(RESUMEBLID)) );
	PackageHeaderIdCb.InsertString(TBR_XFER_ID,"TBR_XFER"); PackageHeaderIdCb.SetItemDataPtr(TBR_XFER_ID, new string(TimLib.HexFormattedAscii(TBR_XFER)) );
	PackageHeaderIdCb.InsertString(UARTID_ID,"UARTID"); PackageHeaderIdCb.SetItemDataPtr(UARTID_ID, new string(TimLib.HexFormattedAscii(UARTID)) );
	PackageHeaderIdCb.InsertString(USBID_ID,"USBID"); PackageHeaderIdCb.SetItemDataPtr(USBID_ID, new string(TimLib.HexFormattedAscii(USBID)) );
	PackageHeaderIdCb.InsertString(CORE_ID,"COREID"); PackageHeaderIdCb.SetItemDataPtr(CORE_ID, new string( TimLib.HexFormattedAscii(COREID)) );
	PackageHeaderIdCb.InsertString(USB_CONFIG_DESCRIPTOR_ID,"USB_CONFIG_DESCRIPTOR"); PackageHeaderIdCb.SetItemDataPtr(USB_CONFIG_DESCRIPTOR_ID, new string(TimLib.HexFormattedAscii(USB_CONFIG_DESCRIPTOR)) );
	PackageHeaderIdCb.InsertString(USB_DEFAULT_STRING_DESCRIPTOR_ID,"USB_DEFAULT_STRING_DESCRIPTOR"); PackageHeaderIdCb.SetItemDataPtr(USB_DEFAULT_STRING_DESCRIPTOR_ID, new string(TimLib.HexFormattedAscii(USB_DEFAULT_STRING_DESCRIPTOR)) );
	PackageHeaderIdCb.InsertString(USB_DEVICE_DESCRIPTOR_ID,"USB_DEVICE_DESCRIPTOR"); PackageHeaderIdCb.SetItemDataPtr(USB_DEVICE_DESCRIPTOR_ID, new string(TimLib.HexFormattedAscii(USB_DEVICE_DESCRIPTOR)) );
	PackageHeaderIdCb.InsertString(USB_ENDPOINT_DESCRIPTOR_ID,"USB_ENDPOINT_DESCRIPTOR"); PackageHeaderIdCb.SetItemDataPtr(USB_ENDPOINT_DESCRIPTOR_ID, new string(TimLib.HexFormattedAscii(USB_ENDPOINT_DESCRIPTOR)) );
	PackageHeaderIdCb.InsertString(USB_INTERFACE_DESCRIPTOR_ID,"USB_INTERFACE_DESCRIPTOR"); PackageHeaderIdCb.SetItemDataPtr(USB_INTERFACE_DESCRIPTOR_ID, new string(TimLib.HexFormattedAscii(USB_INTERFACE_DESCRIPTOR)) );
	PackageHeaderIdCb.InsertString(USB_INTERFACE_STRING_DESCRIPTOR_ID,"USB_INTERFACE_STRING_DESCRIPTOR"); PackageHeaderIdCb.SetItemDataPtr(USB_INTERFACE_STRING_DESCRIPTOR_ID, new string(TimLib.HexFormattedAscii(USB_INTERFACE_STRING_DESCRIPTOR)) );
	PackageHeaderIdCb.InsertString(USB_LANGUAGE_STRING_DESCRIPTOR_ID,"USB_LANGUAGE_STRING_DESCRIPTOR"); PackageHeaderIdCb.SetItemDataPtr(USB_LANGUAGE_STRING_DESCRIPTOR_ID, new string(TimLib.HexFormattedAscii(USB_LANGUAGE_STRING_DESCRIPTOR)) );
	PackageHeaderIdCb.InsertString(USB_MANUFACTURER_STRING_DESCRIPTOR_ID,"USB_MANUFACTURER_STRING_DESCRIPTOR"); PackageHeaderIdCb.SetItemDataPtr(USB_MANUFACTURER_STRING_DESCRIPTOR_ID, new string(TimLib.HexFormattedAscii(USB_MANUFACTURER_STRING_DESCRIPTOR)) );
	PackageHeaderIdCb.InsertString(USB_PRODUCT_STRING_DESCRIPTOR_ID,"USB_PRODUCT_STRING_DESCRIPTOR"); PackageHeaderIdCb.SetItemDataPtr(USB_PRODUCT_STRING_DESCRIPTOR_ID, new string(TimLib.HexFormattedAscii(USB_PRODUCT_STRING_DESCRIPTOR)) );
	PackageHeaderIdCb.InsertString(USB_SERIAL_STRING_DESCRIPTOR_ID,"USB_SERIAL_STRING_DESCRIPTOR"); PackageHeaderIdCb.SetItemDataPtr(USB_SERIAL_STRING_DESCRIPTOR_ID, new string(TimLib.HexFormattedAscii(USB_SERIAL_STRING_DESCRIPTOR)) );
	PackageHeaderIdCb.InsertString(USB_VENDORREQ_ID,"USB_VENDORREQ"); PackageHeaderIdCb.SetItemDataPtr(USB_VENDORREQ_ID, new string(TimLib.HexFormattedAscii(USBVENDORREQ)) );
	PackageHeaderIdCb.InsertString(NOMONITOR_ID,"NOMONITORID"); PackageHeaderIdCb.SetItemDataPtr(NOMONITOR_ID, new string(TimLib.HexFormattedAscii(NOMONITORID)) );
	PackageHeaderIdCb.InsertString(CLKE_ID,"Clock Enable"); PackageHeaderIdCb.SetItemDataPtr(CLKE_ID, new string( TimLib.HexFormattedAscii(CLKEID)) );
	PackageHeaderIdCb.InsertString(CMCC_ID,"Configure Memory Control"); PackageHeaderIdCb.SetItemDataPtr(CMCC_ID, new string( TimLib.HexFormattedAscii(CMCCID)) );
	PackageHeaderIdCb.InsertString(DDRG_ID,"DDR Geometry"); PackageHeaderIdCb.SetItemDataPtr(DDRG_ID, new string( TimLib.HexFormattedAscii(DDRGID)) );
	PackageHeaderIdCb.InsertString(DDRT_ID,"DDR Timing"); PackageHeaderIdCb.SetItemDataPtr(DDRT_ID, new string( TimLib.HexFormattedAscii(DDRTID)) );
	PackageHeaderIdCb.InsertString(DDRC_ID,"DDR Custom"); PackageHeaderIdCb.SetItemDataPtr(DDRC_ID, new string( TimLib.HexFormattedAscii(DDRCID)) );
	PackageHeaderIdCb.InsertString(FREQ_ID,"Frequency"); PackageHeaderIdCb.SetItemDataPtr(FREQ_ID, new string( TimLib.HexFormattedAscii(FREQID)) );
	PackageHeaderIdCb.InsertString(OPDIV_ID,"OpDiv"); PackageHeaderIdCb.SetItemDataPtr(OPDIV_ID, new string( TimLib.HexFormattedAscii(OPDIVID)) );
	PackageHeaderIdCb.InsertString(OPMODE_ID,"OpMode"); PackageHeaderIdCb.SetItemDataPtr(OPMODE_ID, new string( TimLib.HexFormattedAscii(OPMODEID)) );
	PackageHeaderIdCb.InsertString(TZID_ID,"Trust Zone"); PackageHeaderIdCb.SetItemDataPtr(TZID_ID, new string( TimLib.HexFormattedAscii(TZID)) );
	PackageHeaderIdCb.InsertString(TZON_ID,"Trust Zone Regid"); PackageHeaderIdCb.SetItemDataPtr(TZON_ID, new string( TimLib.HexFormattedAscii(TZON)) );
	PackageHeaderIdCb.InsertString(VOLT_ID,"Voltages"); PackageHeaderIdCb.SetItemDataPtr(VOLT_ID, new string( TimLib.HexFormattedAscii(VOLTID)) );

	PackageHeaderIdCb.SetCurSel(0);

	int idx = 0;
	if ( m_PackageData.PackageId().length() == 0 )
	{
		PackageHeaderIdCb.SetCurSel(0);
		sPackageHeaderIdTag = (char*)PackageHeaderIdCb.GetItemDataPtr(0);
		iCurSel = 0;
	}
	else
	{
		idx = PackageHeaderIdCb.FindStringExact(0, m_PackageData.PackageId().c_str());
		if ( LB_ERR != idx )
		{
			PackageHeaderIdCb.SetCurSel( idx );
			sPackageHeaderIdTag = ((string*)PackageHeaderIdCb.GetItemDataPtr(idx))->c_str();
			iCurSel = idx;
//			PackageHeaderIdCb.EnableWindow(FALSE);
		}
		else
		{
			int iSel = PackageHeaderIdCb.InsertString(PackageHeaderIdCb.GetCount(), m_PackageData.PackageId().c_str()); 
			PackageHeaderIdCb.SetItemDataPtr(iSel, new string(m_PackageData.PackageIdTag()));
			PackageHeaderIdCb.SetCurSel(iSel);
			sPackageHeaderIdTag = m_PackageData.PackageIdTag().c_str();
			iCurSel = iSel;

//			NEED TO DO INSERT OF UNKNOWN PACKAGEID INSTEAD OF USING CUST
//			PackageHeaderIdCb.SetCurSel(OEMCUSTOM_ID);
//			sPackageHeaderIdTag = m_PackageData.PackageIdTag().c_str();
//			PackageHeaderIdCb.SetWindowTextA( m_PackageData.PackageId().c_str() );
//			iCurSel = OEMCUSTOM_ID;
		}
	}
	PackageHeaderIdTagEdt.EnableWindow( LB_ERR != idx ? FALSE : TRUE );		

	t_stringList& datalist = m_PackageData.PackageDataList();
	t_stringListIter iter = datalist.begin();
	idx = 0;
	string* psitem;
	while ( iter != datalist.end() )
	{
		psitem = *iter;
		PackageDataListCtrl.InsertItem( idx, psitem->c_str() );
		PackageDataListCtrl.SetItemData( idx, (DWORD_PTR)psitem );
		iter++;
		idx++;
	}

	t_stringList& commentlist = m_PackageData.PackageCommentList();
	t_stringListIter iterComment = commentlist.begin();
	idx = 0;
	while ( iterComment != commentlist.end() )
	{
		psitem = *iterComment;
		PackageDataListCtrl.SetItemText( idx, 1, psitem->c_str() );
		iterComment++;
		idx++;
	}
	
	PackageDataListCtrl.Changed(true);
	AddItemFunctionToList();
	UpdatePackageData();
	PackageDataListCtrl.Changed(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CReservedDataPackageDlg::OnBnClickedOk()
{
	UpdatePackageData();
	if ( m_PackageData.PackageId() == "" )
	{
		AfxMessageBox( "Select a Package ID or enter a custom Package ID" );
		return;
	}

	// delete the strings used in itemdataptrs
	// need to do this before the control is destroyed
	int idx = BLANK;
	while ( idx != PackageHeaderIdCb.GetCount() )
		delete ((string*)PackageHeaderIdCb.GetItemDataPtr(idx++));

	OnOK();
}

void CReservedDataPackageDlg::OnBnClickedCancel()
{
	// delete the strings used in itemdataptrs
	// need to do this before the control is destroyed
	int idx = BLANK;
	while ( idx != PackageHeaderIdCb.GetCount() )
		delete ((string*)PackageHeaderIdCb.GetItemDataPtr(idx++));

	OnCancel();
}

void CReservedDataPackageDlg::UpdateControls()
{
	if ( PackageDataListCtrl.GetItemText( PackageDataListCtrl.GetItemCount()-1, 0 ) != "<add item>" )
	{
		PackageInsertItemBtn.EnableWindow(FALSE);
		PackageDeleteItemBtn.EnableWindow(FALSE);
		MoveUpBtn.EnableWindow(FALSE);
		MoveDownBtn.EnableWindow(FALSE);
	}
	else
	{
		PackageInsertItemBtn.EnableWindow(m_PackageData.PackageId()!="");
		PackageDeleteItemBtn.EnableWindow(m_PackageData.PackageDataList().size()>0);
		MoveUpBtn.EnableWindow(m_PackageData.PackageDataList().size()>1);
		MoveDownBtn.EnableWindow(m_PackageData.PackageDataList().size()>1);
	}

	PackageHeaderIdTagEdt.EnableWindow( PackageHeaderIdCb.GetCurSel() == OEMCUSTOM_ID );
}

void CReservedDataPackageDlg::UpdatePackageData()
{
	if ( PackageDataListCtrl.IsChanged() )
	{
		int idxEnd = PackageDataListCtrl.GetItemCount();
		{
			for ( int idx = 0; idx < idxEnd; idx++ )
			{
				if ( PackageDataListCtrl.GetItemText( idx, 0 ) != "<add item>" )
				{
					bool bFound = false;
					// update the specific field in the package data with a new value
					t_stringListIter iterData = m_PackageData.PackageDataList().begin();
					while( iterData != m_PackageData.PackageDataList().end() )
					{
						if ( (string*)PackageDataListCtrl.GetItemData( idx ) == (*iterData ) )
						{
							(*(*iterData)) = (CStringA)PackageDataListCtrl.GetItemText( idx, 0 );
							bFound = true;
							break;
						}
						iterData++;
					}

					if ( !bFound )
					{
						string* psField = new string( (CStringA)PackageDataListCtrl.GetItemText( idx, 0 ));
						string* psComment = new string( (CStringA)PackageDataListCtrl.GetItemText( idx, 1 ));
						m_PackageData.AddData( psField, psComment );
						PackageDataListCtrl.SetItemData( idx, (DWORD_PTR)psField );
					}
				}
			}
		}

#if 0
		if ( PackageHeaderIdCb.GetCurSel() == OEMCUSTOM_ID )
		{
			// ensure pointer is using the actual tag for the custom package
			string* ptag = (string*)PackageHeaderIdCb.GetItemDataPtr( OEMCUSTOM_ID );
			if ( ptag )
			{
				*ptag = m_PackageData.PackageIdTag();
				PackageHeaderIdTagEdt.SetWindowTextA( (*ptag).c_str() );
				PackageHeaderIdCb.SetWindowTextA( m_PackageData.PackageId().c_str() );
			}
		}
		else
		{
			// ensure pointer is returned to default string
			CTimLib TimLib;
			string* ptag = (string*)PackageHeaderIdCb.GetItemDataPtr( OEMCUSTOM_ID );
			if ( ptag )
			{
				*ptag = TimLib.HexFormattedAscii(OEMCUSTOMID);
				PackageHeaderIdTagEdt.SetWindowTextA( (*ptag).c_str() );
				PackageHeaderIdCb.SetWindowTextA( "CUST" );
			}
		}
#endif

		// TBD need to deal with the comment field appropriately in the package data
//		ASSERT(0);
	}
	UpdateData( FALSE );
	UpdateControls();
}

void CReservedDataPackageDlg::OnEnChangePackageHeaderIdTagEdt()
{
	UpdateData( TRUE );
	m_PackageData.PackageIdTag( string(CStringA(sPackageHeaderIdTag)) );
	UpdateData( FALSE );
}

void CReservedDataPackageDlg::OnCbnSelchangePackageHeaderId()
{
	UpdateData( TRUE );
	CString sText;
	int idx = PackageHeaderIdCb.GetCurSel();
	// handle the new selection
	if ( LB_ERR != idx )
	{
		// handle the prior selection before updating to the new selection
		if ( iCurSel > 0 && m_PackageData.PackageId().c_str() != ""  )
		{
			if ( IDNO == AfxMessageBox( "Changing Package ID will eliminate data from previous package.\nDo you wish to continue?", MB_YESNO ) )
			{
				PackageHeaderIdCb.SetCurSel( iCurSel );
				UpdatePackageData();
				return;
			}
			m_PackageData.DiscardDataAndComments();
		}
		iCurSel = idx;

		PackageHeaderIdCb.GetLBText(idx, sText );
		m_PackageData.PackageId( string((CStringA)sText ));

		sPackageHeaderIdTag = ((string*)PackageHeaderIdCb.GetItemDataPtr(idx))->c_str();
		m_PackageData.PackageIdTag( string((CStringA)sPackageHeaderIdTag ));
		PackageHeaderIdTagEdt.EnableWindow( FALSE );
		PredefinedPackageInit();
	}

	UpdatePackageData();
}

void CReservedDataPackageDlg::OnCbnEditchangePackageHeaderId()
{
	UpdateData( TRUE );
	CString sText;
	PackageHeaderIdCb.GetWindowText( sText );
	m_PackageData.PackageId( string(sText));
	sPackageHeaderIdTag = "0x00000000";
	m_PackageData.PackageIdTag( string(CStringA(sPackageHeaderIdTag)) );
	PackageHeaderIdTagEdt.SetWindowTextA( sPackageHeaderIdTag );
	PackageHeaderIdTagEdt.EnableWindow( TRUE );
	UpdatePackageData();
}

void CReservedDataPackageDlg::AddItemFunctionToList()
{
	 // adds <add item> at bottom of col 0 if needed
	int idx = PackageHeaderIdCb.GetCurSel();
	switch ( idx )
	{
		case AUTOBIND_ID:
		case DDRID_ID:
		case UARTID_ID:
		case USBID_ID:
		case CORE_ID:
		case ESCAPESEQ_ID:
		case RESUMEBLID_ID:
			// cannot add items to these
			PackageInsertItemBtn.EnableWindow(FALSE);
			PackageDeleteItemBtn.EnableWindow(FALSE);
			break;

		case OEMCUSTOM_ID:
		case GPIOID_ID:
		case CLKE_ID:
		case CMCC_ID:
		case DDRG_ID:
		case DDRT_ID:
		case DDRC_ID:
		case FREQ_ID:
		case OPDIV_ID:
		case OPMODE_ID:
		case TZID_ID:
		case TZON_ID:
		case VOLT_ID:
		case TBR_XFER_ID:
		case USB_CONFIG_DESCRIPTOR_ID:
		case USB_DEFAULT_STRING_DESCRIPTOR_ID:
		case USB_DEVICE_DESCRIPTOR_ID:
		case USB_ENDPOINT_DESCRIPTOR_ID:
		case USB_INTERFACE_DESCRIPTOR_ID:
		case USB_INTERFACE_STRING_DESCRIPTOR_ID:
		case USB_LANGUAGE_STRING_DESCRIPTOR_ID:
		case USB_MANUFACTURER_STRING_DESCRIPTOR_ID:
		case USB_PRODUCT_STRING_DESCRIPTOR_ID:
		case USB_SERIAL_STRING_DESCRIPTOR_ID:
		case USB_VENDORREQ_ID:
			// fall-through, no break;
		default:
			if ( idx != 0 )
			{
				// can add items to these
				PackageDataListCtrl.InsertItem( PackageDataListCtrl.GetItemCount(), "<add item>" );
				PackageInsertItemBtn.EnableWindow(TRUE);
				PackageDeleteItemBtn.EnableWindow(TRUE);
			}
			break;
	}
}

void CReservedDataPackageDlg::PredefinedPackageInit()
{
	int idx = PackageHeaderIdCb.GetCurSel();
	if ( idx != LB_ERR )
	{
		PackageDataListCtrl.DeleteAllItems();
		switch ( idx )
		{
			case OEMCUSTOM_ID:
				PackageDataListCtrl.DeleteAllItems();
				PackageDataListCtrl.InsertItem(0,"0x00000000");
				PackageDataListCtrl.SetItemText(0,COMMENT,"1st Custom Value");
				break;
			
			case DDRID_ID:
				PackageDataListCtrl.DeleteAllItems();
				PackageDataListCtrl.InsertItem(0,"0x00000000");
				PackageDataListCtrl.SetItemText(0,COMMENT,"ACCR_VALUE");
				PackageDataListCtrl.InsertItem(1,"0x00000000");
				PackageDataListCtrl.SetItemText(1,COMMENT,"MDCNFG_VALUE");
				PackageDataListCtrl.InsertItem(2,"0x00000000");
				PackageDataListCtrl.SetItemText(2,COMMENT,"DDR_HCAL_VALUE");
				PackageDataListCtrl.InsertItem(3,"0x00000000");
				PackageDataListCtrl.SetItemText(3,COMMENT,"MDREFR_VALUE");
				break;
			
			case GPIOID_ID:
				PackageDataListCtrl.DeleteAllItems();
				PackageDataListCtrl.InsertItem(0,"0x00000001");
				PackageDataListCtrl.SetItemText(0,COMMENT,"Number of GPIO Address & Value Pairs below");
				PackageDataListCtrl.InsertItem(1,"0x00000000");
				PackageDataListCtrl.SetItemText(1,COMMENT,"Address");
				PackageDataListCtrl.InsertItem(2,"0x00000000");
				PackageDataListCtrl.SetItemText(2,COMMENT,"Value");
				break;
			
			case RESUMEBLID_ID:
				PackageDataListCtrl.DeleteAllItems();
				PackageDataListCtrl.InsertItem(0,"0x00000000");
				PackageDataListCtrl.SetItemText(0,COMMENT,"DDRResumeRecordAddr");
				PackageDataListCtrl.InsertItem(1,"0x00000000");
				PackageDataListCtrl.SetItemText(1,COMMENT,"DDRScratchAreaAddr");
				PackageDataListCtrl.InsertItem(2,"0x00000000");
				PackageDataListCtrl.SetItemText(2,COMMENT,"DDRScratchAreaLength");
				break;
			
			case TBR_XFER_ID:
				PackageDataListCtrl.DeleteAllItems();
				PackageDataListCtrl.InsertItem(0,"0x00000000");
				PackageDataListCtrl.SetItemText(0,COMMENT,"XFER Location");
				PackageDataListCtrl.InsertItem(1,"0x00000001");
				PackageDataListCtrl.SetItemText(1,COMMENT,"num_data_pairs");
				PackageDataListCtrl.InsertItem(2,"0x00000001");
				PackageDataListCtrl.SetItemText(2,COMMENT,"data id");
				PackageDataListCtrl.InsertItem(3,"0x00000000");
				PackageDataListCtrl.SetItemText(3,COMMENT,"location");
				break;
			
			case UARTID_ID:
				PackageDataListCtrl.DeleteAllItems();
				PackageDataListCtrl.InsertItem(0,"0x00004646");
				PackageDataListCtrl.SetItemText(0,COMMENT,"Port: 0x00004646 = FFIDENTIFIER -- see docs for other IDs");
				PackageDataListCtrl.InsertItem(1,"0x00000000");
				PackageDataListCtrl.SetItemText(1,COMMENT,"Enabled");
				break;
			
			case USBID_ID:
				PackageDataListCtrl.DeleteAllItems();
				PackageDataListCtrl.InsertItem(0,"0x00434932");
				PackageDataListCtrl.SetItemText(0,COMMENT,"Port: 0x00434932 = CI2IDENTIFIER -- see docs for other IDs");
				PackageDataListCtrl.InsertItem(1,"0x00000000");
				PackageDataListCtrl.SetItemText(1,COMMENT,"Enabled");
				break;

			case CORE_ID:
				PackageDataListCtrl.DeleteAllItems();
				PackageDataListCtrl.InsertItem(0,"0x00000000");
				PackageDataListCtrl.SetItemText(0,COMMENT,"MM = 0, MP1 = 1, MP2 = 2");
				break;

			case USB_VENDORREQ_ID:
				PackageDataListCtrl.DeleteAllItems();
				PackageDataListCtrl.InsertItem(0,"0x00000000");
				PackageDataListCtrl.SetItemText(0,COMMENT,"bm Request Type");
				PackageDataListCtrl.InsertItem(1,"0x00000000");
				PackageDataListCtrl.SetItemText(1,COMMENT,"b Request");
				PackageDataListCtrl.InsertItem(2,"0x00000000");
				PackageDataListCtrl.SetItemText(2,COMMENT,"wValue");
				PackageDataListCtrl.InsertItem(3,"0x00000000");
				PackageDataListCtrl.SetItemText(3,COMMENT,"wIndex");
				PackageDataListCtrl.InsertItem(4,"0x00000000");
				PackageDataListCtrl.SetItemText(4,COMMENT,"wLength");
				PackageDataListCtrl.InsertItem(5,"0x00000000");
				PackageDataListCtrl.SetItemText(5,COMMENT,"wData");
				break;
			
			case ESCAPESEQ_ID:
				PackageDataListCtrl.DeleteAllItems();
				PackageDataListCtrl.InsertItem(0,"0x00000000");
				PackageDataListCtrl.SetItemText(0,COMMENT,"EscSeqTimeOutMS");
				break;

			case AUTOBIND_ID:
				PackageDataListCtrl.DeleteAllItems();
				PackageDataListCtrl.InsertItem(0,"0x00000000");
				PackageDataListCtrl.SetItemText(0,COMMENT,"Autobind");
				break;

			case CLKE_ID:
			case CMCC_ID:
			case DDRG_ID:
			case DDRT_ID:
			case DDRC_ID:
			case FREQ_ID:
			case OPDIV_ID:
			case OPMODE_ID:
			case TZID_ID:
			case TZON_ID:
			case VOLT_ID:
				PackageDataListCtrl.DeleteAllItems();
				PackageDataListCtrl.InsertItem(0,"0x00000000");
				PackageDataListCtrl.SetItemText(0,COMMENT,"Address");
				PackageDataListCtrl.InsertItem(1,"0x00000000");
				PackageDataListCtrl.SetItemText(1,COMMENT,"Value");
				break;

			case USB_CONFIG_DESCRIPTOR_ID:
			case USB_DEFAULT_STRING_DESCRIPTOR_ID:
			case USB_DEVICE_DESCRIPTOR_ID:
			case USB_ENDPOINT_DESCRIPTOR_ID:
			case USB_INTERFACE_DESCRIPTOR_ID:
			case USB_INTERFACE_STRING_DESCRIPTOR_ID:
			case USB_LANGUAGE_STRING_DESCRIPTOR_ID:
			case USB_MANUFACTURER_STRING_DESCRIPTOR_ID:
			case USB_PRODUCT_STRING_DESCRIPTOR_ID:
			case USB_SERIAL_STRING_DESCRIPTOR_ID:
				break;

			default:
				break;
		}
		AddItemFunctionToList();
		PackageDataListCtrl.Changed(true);
	}
}

void CReservedDataPackageDlg::OnBnClickedPackageDeleteItem()
{
	int iItem = PackageDataListCtrl.GetSelectionMark();
	if ( iItem != -1 )
	{
		CString sLastItem = PackageDataListCtrl.GetItemText( PackageDataListCtrl.GetItemCount() -1, 0 );
		if ( sLastItem == "<add item>" )
		{
			string* ps = (string*)PackageDataListCtrl.GetItemData( iItem );
			m_PackageData.DeleteData( ps );
			PackageDataListCtrl.DeleteSelectedItem();
		}
	}
	UpdatePackageData();
}

void CReservedDataPackageDlg::OnBnClickedPackageInsertItemBtn()
{
	CString sLastItem = PackageDataListCtrl.GetItemText( PackageDataListCtrl.GetItemCount() -1, 0 );
	if ( sLastItem == "<add item>" )
		PackageDataListCtrl.InsertAfterSelectedItem();
	UpdatePackageData();
}

void CReservedDataPackageDlg::OnBnClickedPackageDataMoveUpBtn()
{
	int item = PackageDataListCtrl.GetSelectionMark();
	if ( item > 0 )
	{
		if ( PackageDataListCtrl.GetItemText( item, 0 ) != "<add item>" )
		{
			CString sValue = PackageDataListCtrl.GetItemText( item, 0 );
			CString sComment = PackageDataListCtrl.GetItemText( item, 1 );
			PackageDataListCtrl.DeleteItem( item-- );
			PackageDataListCtrl.InsertItem( item, sValue );
			PackageDataListCtrl.SetItemText( item, 1, sComment );
			PackageDataListCtrl.SetSelectionMark( item );
			UpdatePackageData();
		}
	}
}

void CReservedDataPackageDlg::OnBnClickedPackageDataMoveDownBtn()
{
	int item = PackageDataListCtrl.GetSelectionMark();
	if ( item != -1 )
	{
		if ( item + 1 < PackageDataListCtrl.GetItemCount() && PackageDataListCtrl.GetItemText( item + 1, 0 ) != "<add item>" )
		{
			CString sValue = PackageDataListCtrl.GetItemText( item, 0 );
			CString sComment = PackageDataListCtrl.GetItemText( item, 1 );
			PackageDataListCtrl.DeleteItem( item++ );
			PackageDataListCtrl.InsertItem( item, sValue );
			PackageDataListCtrl.SetItemText( item, 1, sComment );
			PackageDataListCtrl.SetSelectionMark( item );
			UpdatePackageData();
		}
	}
}

void CReservedDataPackageDlg::UpdateChangeMarkerInTitle()
{
	if ( m_hWnd == 0 )
		return;

	CString sTitle;
	GetWindowTextA( sTitle );
	// add or remove * as appropriate
	if ( PackageDataListCtrl.IsChanged() )
	{
		if ( -1 == sTitle.Find('*') )
			sTitle += "*";
	}
	else
	{
		if ( -1 != sTitle.Find('*') )
			sTitle.Remove('*');
	}
	SetWindowTextA(sTitle);
	UpdateWindow();
}


void CReservedDataPackageDlg::OnLvnItemActivatePackageDataListCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	UpdateData(FALSE);
}

void CReservedDataPackageDlg::OnNMKillfocusPackageDataListCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	UpdatePackageData();
	m_PackageData.Changed(true);
}

void CReservedDataPackageDlg::OnContextMenu(CWnd* pWnd, CPoint /*point*/)
{
	CCommentDlg Dlg(this);
	int nItem = -1;

	int iCtlId = pWnd->GetDlgCtrlID();
	switch ( iCtlId )
	{
		case IDC_PACKAGE_HEADER_ID:
//			if ( pData )
//				Dlg.Display( pData );
			break;

		case IDC_PACKAGE_HEADER_ID_TAG_EDT:
			break;

		case IDC_PACKAGE_DATA_LIST_CTRL:
			nItem = PackageDataListCtrl.GetSelectionMark();
			if ( nItem != -1 )
			{
				string* pData = (string*)PackageDataListCtrl.GetItemData( nItem );
				if ( pData )
					Dlg.Display( pData );
			}
			break;

		default:
			break;
	}
}

