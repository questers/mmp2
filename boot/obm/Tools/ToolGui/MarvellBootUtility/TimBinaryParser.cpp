/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

//#if TOOLS_GUI == 1
//#include "StdAfx.h"
//#include "MarvellBootUtility.h"
//#endif

#pragma warning ( disable : 4996 )

#include "TimBinaryParser.h"
#include "ImageDescription.h"

#include "Key.h"
#include "DigitalSignature.h"

#include "ReservedPackageData.h"
#include "ExtendedReservedData.h"
#include "ErdBase.h"
#include "AutoBindERD.h"
#include "CoreResetERD.h"
#include "EscapeSeqERD.h"
#include "GpioSetERD.h"
#include "ResumeDdrERD.h"
#include "TbrXferERD.h"
#include "UartERD.h"
#include "UsbERD.h"
#include "UsbVendorReqERD.h"
#include "TzInitialization.h"


CTimBinaryParser::CTimBinaryParser(void)
{
    FileData = 0;
}

CTimBinaryParser::~CTimBinaryParser(void)
{
    delete [] FileData;
}

bool CTimBinaryParser::ParseFile( string sFilePath )
{
    if ( sFilePath.empty() )
        return false;

    ifstream ifsTimBinaryFile;
    ifsTimBinaryFile.open( sFilePath.c_str(), ios_base::in | ios::binary );
    if ( ifsTimBinaryFile.fail() || ifsTimBinaryFile.bad() )
    {
        printf ("\n  Error: Cannot open file name <%s> !", sFilePath.c_str());
        return false;
    }

    // get the file length for allocating a buffer to hold the file
    ifsTimBinaryFile.seekg( 0, ios_base::end );
    ifsTimBinaryFile.clear();
    unsigned int ifsPos = (unsigned int)ifsTimBinaryFile.tellg();
    ifsTimBinaryFile.seekg( 0, ios_base::beg );
    if ( ifsPos <= 0 )
    {
        printf ("\n  Error: File is empty!" );
        return false;
    }

    // alloc a data array to load the file
    FileData = new char [ ifsPos ];
    memset( FileData, 0, ifsPos );
    ifsTimBinaryFile.read( FileData, ifsPos );
    if ( ifsTimBinaryFile.fail() )
    {
        printf ("\n  Error: Failed to read all of file!" );
        return false;
    }
    
    unsigned int iValuePos = 0;

    if ( !ParseTimHeader( iValuePos ) )
        return false;

    ParseImages( iValuePos );
#if TRUSTED
    ParseKeys( iValuePos );
#endif
    ParseReservedData( iValuePos );
    SkipAllOnesPadding( iValuePos );

#if TRUSTED
    if ( !ParseDigitalSignature( iValuePos ) )
        return false;
#endif

    return true;
}


bool CTimBinaryParser::ParseTimHeader( unsigned int& iValuePos )
{
    // parse tim header
    // if the version translates as ascii characters, then this is likely an _h.bin rather than a bin
    string sVersion = HexAsciiToText(HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ]));
    if ( sVersion.length() > 0 && IsAlphaNumeric( sVersion ) )
        return false;	
        
    TimDescriptor.TimHeader().VersionBind.Version = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    TimDescriptor.TimHeader().VersionBind.Identifier = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;

    TimDescriptor.TimHeader().VersionBind.Trusted = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    // trusted value must be 0 or 1 or indicates a bad bin file format
    if ( !(TimDescriptor.TimHeader().VersionBind.Trusted == 0 || TimDescriptor.TimHeader().VersionBind.Trusted == 1) )
        return false;

    TimDescriptor.TimHeader().VersionBind.IssueDate = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    TimDescriptor.TimHeader().VersionBind.OEMUniqueID = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    TimDescriptor.TimHeader().FlashInfo.WTMFlashSign = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    TimDescriptor.TimHeader().FlashInfo.WTMEntryAddr = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    TimDescriptor.TimHeader().FlashInfo.WTMEntryAddrBack = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    TimDescriptor.TimHeader().FlashInfo.WTMPatchSign = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    TimDescriptor.TimHeader().FlashInfo.WTMPatchAddr = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    TimDescriptor.TimHeader().FlashInfo.BootFlashSign = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    
    TimDescriptor.TimHeader().NumImages = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    // number of images is suspiciously large and indicates this is not a proper bin file
    if ( TimDescriptor.TimHeader().NumImages > 20 )
        return false;

    TimDescriptor.TimHeader().NumKeys = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    // number of keys is suspiciously large and indicates this is not a proper bin file
    if ( TimDescriptor.TimHeader().NumKeys > 6 )
        return false;

    TimDescriptor.TimHeader().SizeOfReserved = ( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
    return true;
}

void CTimBinaryParser::ParseImages( unsigned int& iValuePos )
{
    if ( TimDescriptor.TimHeader().VersionBind.Version <= TIM_3_3_00 )
    {
        for ( unsigned int i = 0; i < TimDescriptor.TimHeader().NumImages; i++ )
        {
            CImageDescription* pImage = new CImageDescription;
            
            pImage->ImageIdTag( HexFormattedAscii( *(unsigned int*)&FileData[ iValuePos ] ) ); iValuePos += 4;
            pImage->NextImageIdTag( HexFormattedAscii( *(unsigned int*)&FileData[ iValuePos ] ) ); iValuePos += 4;
            pImage->FlashEntryAddress( HexFormattedAscii( *(unsigned int*)&FileData[ iValuePos ] ) ); iValuePos += 4;
            pImage->LoadAddress( HexFormattedAscii( *(unsigned int*)&FileData[ iValuePos ] ) ); iValuePos += 4;
            pImage->ImageSize( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
            pImage->ImageSizeToHash( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;

            unsigned int HashAlgorithmID  = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
            if ( HashAlgorithmID == SHA256 )
                pImage->HashAlgorithmId( string("SHA-256") );
            else if ( HashAlgorithmID == SHA160 )
                pImage->HashAlgorithmId( string("SHA-160") );

            for ( int j = 0; j < 8; j++ )
            {
                pImage->Hash[j] = *(unsigned int*)&FileData[ iValuePos ]; 
                iValuePos += 4;
            }
            
            if ( TimDescriptor.TimHeader().VersionBind.Version == TIM_3_3_00 )
                pImage->PartitionNumber( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;

            TimDescriptor.ImagesList().push_back( pImage );
        }
    }
    else if ( TimDescriptor.TimHeader().VersionBind.Version >= TIM_3_4_00 )
    {
        for ( unsigned int i = 0; i < TimDescriptor.TimHeader().NumImages; i++ )
        {
            CImageDescription* pImage = new CImageDescription;
            
            pImage->ImageIdTag( HexFormattedAscii( *(unsigned int*)&FileData[ iValuePos ] ) ); iValuePos += 4;
            pImage->NextImageIdTag( HexFormattedAscii( *(unsigned int*)&FileData[ iValuePos ] ) ); iValuePos += 4;
            pImage->FlashEntryAddress( HexFormattedAscii( *(unsigned int*)&FileData[ iValuePos ] ) ); iValuePos += 4;
            pImage->LoadAddress( HexFormattedAscii( *(unsigned int*)&FileData[ iValuePos ] ) ); iValuePos += 4;
            pImage->ImageSize( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
            pImage->ImageSizeToHash( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;

            unsigned int HashAlgorithmID  = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
            if ( HashAlgorithmID == SHA256 )
                pImage->HashAlgorithmId( string("SHA-256") );
            else if ( HashAlgorithmID == SHA160 )
                pImage->HashAlgorithmId( string("SHA-160") );
            else if ( HashAlgorithmID == SHA512 )
                pImage->HashAlgorithmId( string("SHA-512") );

            for ( unsigned int j = 0; j < 16; j++ )
            {
                pImage->Hash[j] = *(unsigned int*)&FileData[ iValuePos ];
                iValuePos += 4;
            }

            pImage->PartitionNumber( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
            
            TimDescriptor.ImagesList().push_back( pImage );
        }
    }
}

#if TRUSTED
void CTimBinaryParser::ParseKeys( unsigned int& iValuePos )
{
    if ( TimDescriptor.TimHeader().VersionBind.Trusted )
    {
        for ( unsigned int i = 0; i < TimDescriptor.TimHeader().NumKeys; i++ )
        {
            if ( TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
            {
                CKey* pKey = new CKey;
                pKey->KeyTag( HexFormattedAscii( *(unsigned int*)&FileData[ iValuePos ] ) ); iValuePos += 4;

                unsigned int HashAlgorithmID = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
                if (HashAlgorithmID == 64 || HashAlgorithmID == 512 )
                    pKey->HashAlgorithmId( string("SHA-512") );
                else if (HashAlgorithmID == 2 || HashAlgorithmID == 32 || HashAlgorithmID == 256 )
                    pKey->HashAlgorithmId( string("SHA-256") );
                else
                    pKey->HashAlgorithmId( string("SHA-160") );

                // convert bytes to bits when getting sizes
                pKey->KeySize( 8* (*(unsigned int*)&FileData[ iValuePos ] )); iValuePos += 4;
                pKey->PublicKeySize( 8* (*(unsigned int*)&FileData[ iValuePos ] )); iValuePos += 4;

                t_stringListIter iterPubExp = pKey->PublicKeyExponentList().begin();
                while ( iterPubExp != pKey->PublicKeyExponentList().end() )
                {
                    *(*iterPubExp) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ] ); 
                    iValuePos += 4;
                    iterPubExp++;
                }

                // discard unused bytes from file because MAXRSAKEYSIZEWORDS were written
                // though not all bytes were actually used and should be 0's
                size_t discard = MAXRSAKEYSIZEWORDS - pKey->PublicKeyExponentList().size();
                while ( discard-- > 0 )
                {
                    unsigned int discardedValue = *(unsigned int*)&FileData[ iValuePos ];
                    iValuePos += 4;
                }

                t_stringListIter iterSysMod = pKey->RsaSystemModulusList().begin();
                while ( iterSysMod != pKey->RsaSystemModulusList().end() )
                {
                    *(*iterSysMod) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ] );
                    iValuePos += 4;
                    iterSysMod++;
                }
                
                // discard unused bytes from file because MAXRSAKEYSIZEWORDS were written
                // though not all bytes were actually used and should be 0's
                discard = MAXRSAKEYSIZEWORDS - pKey->RsaSystemModulusList().size();
                while ( discard-- > 0 )
                {
                    unsigned int discardedValue = *(unsigned int*)&FileData[ iValuePos ];
                    iValuePos += 4;
                }
                
                UINT_T KeyHash[8]={0}; // hash in binary is skipped over and discarded
                for( unsigned int i = 0; i < 8; i++ )
                {
                    KeyHash[i] = *(unsigned int*)&FileData[ iValuePos ]; 
                    iValuePos += 4;
                }

                TimDescriptor.KeyList().push_back( pKey );
            }
            else if ( TimDescriptor.TimHeader().VersionBind.Version >= TIM_3_3_00 )
            {
                CKey* pKey = new CKey;
                pKey->KeyTag( HexFormattedAscii( *(unsigned int*)&FileData[ iValuePos ] ) ); iValuePos += 4;

                unsigned int HashAlgorithmID = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
                if (HashAlgorithmID == 64 || HashAlgorithmID == 512 )
                    pKey->HashAlgorithmId( string("SHA-512") );
                else if (HashAlgorithmID == 2 || HashAlgorithmID == 32 || HashAlgorithmID == 256 )
                    pKey->HashAlgorithmId( string("SHA-256") );
                else
                    pKey->HashAlgorithmId( string("SHA-160") );

                pKey->KeySize( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;
                pKey->PublicKeySize( *(unsigned int*)&FileData[ iValuePos ] ); iValuePos += 4;

                unsigned int EncryptAlgorithmID = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
                pKey->EncryptAlgorithmId( (ENCRYPTALGORITHMID_T)EncryptAlgorithmID );

                if ( pKey->EncryptAlgorithmId() == "ECDSA_256" || pKey->EncryptAlgorithmId() == "ECDSA_521" )
                {
                    t_stringListIter iterPubExp = pKey->ECDSAPublicKeyCompXList().begin();
                    while ( iterPubExp != pKey->ECDSAPublicKeyCompXList().end() )
                    {
                        *(*iterPubExp) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ] ); 
                        iValuePos += 4;
                        iterPubExp++;
                    }

                    // discard unused bytes from file because MAXRSAKEYSIZEWORDS were written
                    // though not all bytes were actually used and should be 0's
                    size_t discard = MAXECCKEYSIZEWORDS - pKey->ECDSAPublicKeyCompXList().size();
                    while ( discard-- > 0 )
                    {
                        unsigned int discardedValue = *(unsigned int*)&FileData[ iValuePos ];
                        iValuePos += 4;
                    }

                    iterPubExp = pKey->ECDSAPublicKeyCompYList().begin();
                    while ( iterPubExp != pKey->ECDSAPublicKeyCompYList().end() )
                    {
                        *(*iterPubExp) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ] ); 
                        iValuePos += 4;
                        iterPubExp++;
                    }
        
                    // discard unused bytes from file because MAXRSAKEYSIZEWORDS were written
                    // though not all bytes were actually used and should be 0's
                    discard = MAXECCKEYSIZEWORDS - pKey->ECDSAPublicKeyCompYList().size();
                    while ( discard-- > 0 )
                    {
                        unsigned int discardedValue = *(unsigned int*)&FileData[ iValuePos ];
                        iValuePos += 4;
                    }

                    TimDescriptor.KeyList().push_back( pKey );
                }
                else
                {
                    t_stringListIter iterPubExp = pKey->PublicKeyExponentList().begin();
                    while ( iterPubExp != pKey->PublicKeyExponentList().end() )
                    {
                        *(*iterPubExp) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ] ); 
                        iValuePos += 4;
                        iterPubExp++;
                    }

                    // discard unused bytes from file because MAXRSAKEYSIZEWORDS were written
                    // though not all bytes were actually used and should be 0's
                    size_t discard = MAXRSAKEYSIZEWORDS - pKey->PublicKeyExponentList().size();
                    while ( discard-- > 0 )
                    {
                        unsigned int discardedValue = *(unsigned int*)&FileData[ iValuePos ];
                        iValuePos += 4;
                    }

                    t_stringListIter iterSysMod = pKey->RsaSystemModulusList().begin();
                    while ( iterSysMod != pKey->RsaSystemModulusList().end() )
                    {
                        *(*iterSysMod) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ] );
                        iValuePos += 4;
                        iterSysMod++;
                    }
                
                    // discard unused bytes from file because MAXRSAKEYSIZEWORDS were written
                    // though not all bytes were actually used and should be 0's
                    discard = MAXRSAKEYSIZEWORDS - pKey->RsaSystemModulusList().size();
                    while ( discard-- > 0 )
                    {
                        unsigned int discardedValue = *(unsigned int*)&FileData[ iValuePos ];
                        iValuePos += 4;
                    }
                
                    UINT_T KeyHash[8]={0}; // hash in binary is skipped over and discarded
                    for( unsigned int i = 0; i < 8; i++ )
                    {
                        KeyHash[i] = *(unsigned int*)&FileData[ iValuePos ]; 
                        iValuePos += 4;
                    }
                    TimDescriptor.KeyList().push_back( pKey );
                }
            }
        }
    }
}
#endif

bool CTimBinaryParser::ParseReservedData( unsigned int& iValuePos )
{
    unsigned int ResAreaTag = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
    if ( ResAreaTag == WTPRESERVEDAREAID )
    {
        CExtendedReservedData& Ext = TimDescriptor.ExtendedReservedData();

        unsigned int uiNumPackages = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
        unsigned int j = 0;
        unsigned fieldValue = 0;
        
        for ( unsigned int i = 0; i < uiNumPackages; i++ )
        {
            unsigned int uiPackageId = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
            unsigned int uiPackageSize = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
            
            // check if package size is extreme, indicating a parsing error or incorrect binary format
            if ( uiPackageSize > 4096*4 )
                return false;

            if ( TimDescriptor.ProcessorType() == "PXA30x"
                 || TimDescriptor.ProcessorType() == "PXA31x"
                 || TimDescriptor.ProcessorType() == "PXA32x" )
            {
                // no extended reserved data supported for these processor types

                if ( uiPackageId == TERMINATORID )
                    // done processing reserved data
                    break;

                // make it an oem custom package instead
                CReservedPackageData* pCustom = new CReservedPackageData;
                pCustom->PackageIdTag( HexFormattedAscii( uiPackageId ) );
                pCustom->PackageId( HexAsciiToText( HexFormattedAscii( uiPackageId ) ) );

                for ( j = 0; j < (uiPackageSize-8)/4; j++ )
                {
                    string* pData = new string( HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ]) );
                    iValuePos += 4;
                    string* pComment = new string("");
                    pCustom->AddData( pData, pComment ); 
                }
                TimDescriptor.ReservedDataList().push_back( pCustom );
                continue;
            }

            // check for DDRn
            if ( (uiPackageId & 0xffffff00) == 0x44445200 )
            {
                if ( ((uiPackageId & 0x000000ff) >= 0x30) && ((uiPackageId & 0x000000ff) <= 0x39) )
                {
                    // process DDRn packages apart from all others that are DDR[alpha] packages
                    CDDRInitialization* pDDRn = new CDDRInitialization;
                    *(pDDRn->FieldValues()[CDDRInitialization::DDR_PID]) = HexFormattedAscii( uiPackageId ); 
                    pDDRn->m_sDdrPID = HexAsciiToText( *pDDRn->FieldValues()[CDDRInitialization::DDR_PID] );

                    unsigned int iOperations = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4; 
                    unsigned int iInstructions = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4; 
                    
                    for ( j = 0; j < iOperations; j++ )
                    {
                        CDDROperation* pOperation = new CDDROperation;
                        pOperation->SetOperationID( (DDR_OPERATION_SPEC_T)(*(unsigned int*)&FileData[ iValuePos ]) ); 
                        iValuePos += 4; 
                        pOperation->m_Value = *(unsigned int*)&FileData[ iValuePos ];
                        iValuePos += 4; 
                        pDDRn->m_DdrOperations.m_DdrOperations.push_back( pOperation );
                    }

                    for ( j = 0; j < iInstructions; j++ )
                    {
                        CInstruction* pInstruction = new CInstruction;
                        pInstruction->SetInstructionType( (INSTRUCTION_OP_CODE_SPEC_T)(*(unsigned int*)&FileData[ iValuePos ]) ); 
                        iValuePos += 4; 
                        for ( unsigned int k = 0; k < pInstruction->m_NumParamsUsed; k++ )
                        {
                            pInstruction->m_ParamValues[k] = *(unsigned int*)&FileData[ iValuePos ];
                            iValuePos += 4; 
                        }
                        pDDRn->m_DdrInstructions.m_Instructions.push_back( pInstruction );
                    }

                    Ext.ErdVec.push_back( pDDRn );
                    continue;
                }
            }

            switch ( uiPackageId )
            {
            case TERMINATORID:
                // done processing reserved data
                goto Term;
                break;

            // reserved data packages
            case AUTOBIND:
                {
                    CAutoBind* pAutoBind = new CAutoBind;
                    ParseERDBaseFields( pAutoBind, (uiPackageSize-8)/4, iValuePos );
                    Ext.ErdVec.push_back( pAutoBind );
                }
                break;
            
            case ESCAPESEQID:
                {
                    CEscapeSeq* pEscapeSeq = new CEscapeSeq;
                    ParseERDBaseFields( pEscapeSeq, (uiPackageSize-8)/4, iValuePos );
                    Ext.ErdVec.push_back( pEscapeSeq );
                }
                break;

            case GPIOID:
                {
                    CGpioSet* pGpioSet = new CGpioSet;
                    *(pGpioSet->FieldValues()[CGpioSet::NUM_GPIOS]) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ]); 
                    iValuePos += 4; 
                    if ( (uiPackageSize-8-4)/(4*2) == Translate( *(pGpioSet->FieldValues()[CGpioSet::NUM_GPIOS] ) ) )
                    {
                        for ( j = 0; j < (uiPackageSize-8-4)/(4*2); j++ )
                        {
                            CGpio* pGpio = new CGpio;
                            *(pGpio->FieldValues()[CGpio::ADDRESS]) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ]); 
                            iValuePos += 4; 
                            *(pGpio->FieldValues()[CGpio::VALUE]) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ]); 
                            iValuePos += 4; 
                            
                            pGpioSet->GpiosList().push_back( pGpio );
                        }
                    }
                    Ext.ErdVec.push_back( pGpioSet );
                }
                break;

            case RESUMEBLID:
                {
                    CResumeDdr* pResumeDdr = new CResumeDdr;
                    ParseERDBaseFields( pResumeDdr, (uiPackageSize-8)/4, iValuePos );
                    Ext.ErdVec.push_back( pResumeDdr );
                }
                break;


            case TBR_XFER:
                {
                    CTBRXferSet* pTBRXferSet = new CTBRXferSet;
                    *(pTBRXferSet->FieldValues()[CTBRXferSet::XFER_LOC]) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ]); iValuePos += 4; 
                    *(pTBRXferSet->FieldValues()[CTBRXferSet::NUM_DATA_PAIRS]) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ]); iValuePos += 4; 
                    if ( (uiPackageSize-8-8)/(4*2) == Translate( *(pTBRXferSet->FieldValues()[CTBRXferSet::NUM_DATA_PAIRS] ) ) )
                    {
                        for ( j = 0; j < (uiPackageSize-8-8)/(4*2); j++ )
                        {
                            CXfer* pXfer = new CXfer;
                            *(pXfer->FieldValues()[CXfer::DATA_ID]) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ]); 
                            iValuePos += 4; 
                            *(pXfer->FieldValues()[CXfer::LOCATION]) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ]); 
                            iValuePos += 4; 
                            
                            pTBRXferSet->XfersList().push_back( pXfer );
                        }
                    }
                    Ext.ErdVec.push_back( pTBRXferSet );
                }
                break;

            case UARTID:
                {
                    CUart* pUart = new CUart;
                    ParseERDBaseFields( pUart, (uiPackageSize-8)/4, iValuePos );
                    Ext.ErdVec.push_back( pUart );
                }
                break;

            case USBID:
                {
                    CUsb* pUsb = new CUsb;
                    ParseERDBaseFields( pUsb, (uiPackageSize-8)/4, iValuePos );
                    Ext.ErdVec.push_back( pUsb );
                }
                break;

            case COREID:
                {
                    CCoreReset* pCoreReset = new CCoreReset;
                    ParseERDBaseFields( pCoreReset, (uiPackageSize-8)/4, iValuePos );
                    Ext.ErdVec.push_back( pCoreReset );
                }
                break;

            case USBVENDORREQ:
                {
                    CUsbVendorReq* pUsbVendorReq = new CUsbVendorReq;
                    ParseERDBaseFields( pUsbVendorReq, (uiPackageSize-8)/4, iValuePos );
                    Ext.ErdVec.push_back( pUsbVendorReq );
                }
                break;

            case CMCCID:
                ParseERDFields( (uiPackageSize-8)/(4*2), iValuePos, Ext,
                                 &CExtendedReservedData::AddConfigMemoryControlField );
                break;

            case DDRGID:
                ParseERDFields( (uiPackageSize-8)/(4*2), iValuePos, Ext,
                                 &CExtendedReservedData::AddDDRGeometryField );
                break;

            case TZID:
                ParseERDFields( (uiPackageSize-8)/(4*2), iValuePos, Ext,
                                 &CExtendedReservedData::AddTrustZoneField );
                break;

            case TZON:
                ParseERDFields( (uiPackageSize-8)/(4*2), iValuePos, Ext,
                                 &CExtendedReservedData::AddTrustZoneRegidField );
                break;

            case DDRTID:
                ParseERDFields( (uiPackageSize-8)/(4*2), iValuePos, Ext,
                                 &CExtendedReservedData::AddDDRTimingField );
                break;

            case DDRCID:
                ParseERDFields( (uiPackageSize-8)/(4*2), iValuePos, Ext,
                                 &CExtendedReservedData::AddDDRCustomField );
                break;

            case OPDIVID:
                ParseERDFields( (uiPackageSize-8)/(4*2), iValuePos, Ext,
                                 &CExtendedReservedData::AddOpDivField );
                break;

            case OPMODEID:
                ParseERDFields( (uiPackageSize-8)/(4*2), iValuePos, Ext,
                                 &CExtendedReservedData::AddOpModeField );
                break;

            case CLKEID:
                ParseERDFields( (uiPackageSize-8)/(4*2), iValuePos, Ext,
                                 &CExtendedReservedData::AddClockEnableField );
                break;

            case FREQID: 
                ParseERDFields( (uiPackageSize-8)/(4*2), iValuePos, Ext,
                                 &CExtendedReservedData::AddFrequencyField );
                break;

            case VOLTID:
                ParseERDFields( (uiPackageSize-8)/(4*2), iValuePos, Ext,
                                 &CExtendedReservedData::AddVoltagesField );
                break;

            case TZRI:
                {
                    CTzInitialization* pTzInit = new CTzInitialization;
                    *(pTzInit->FieldValues()[CTzInitialization::TZ_PID]) = HexFormattedAscii( uiPackageId ); 
                    pTzInit->m_sTzPID = HexAsciiToText( *pTzInit->FieldValues()[CTzInitialization::TZ_PID] );

                    unsigned int iOperations = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4; 
                    unsigned int iInstructions = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4; 
                    
                    for ( j = 0; j < iOperations; j++ )
                    {
                        CTzOperation* pOperation = new CTzOperation;
                        pOperation->SetOperationID( (TZ_OPERATION_SPEC_T)(*(unsigned int*)&FileData[ iValuePos ]) ); 
                        iValuePos += 4; 
                        pOperation->m_Value = *(unsigned int*)&FileData[ iValuePos ];
                        iValuePos += 4; 
                        pTzInit->m_TzOperations.m_TzOperations.push_back( pOperation );
                    }

                    for ( j = 0; j < iInstructions; j++ )
                    {
                        CInstruction* pInstruction = new CInstruction;
                        pInstruction->SetInstructionType( (INSTRUCTION_OP_CODE_SPEC_T)(*(unsigned int*)&FileData[ iValuePos ]) ); 
                        iValuePos += 4; 
                        for ( unsigned int k = 0; k < pInstruction->m_NumParamsUsed; k++ )
                        {
                            pInstruction->m_ParamValues[k] = *(unsigned int*)&FileData[ iValuePos ];
                            iValuePos += 4; 
                        }
                        pTzInit->m_TzInstructions.m_Instructions.push_back( pInstruction );
                    }

                    Ext.ErdVec.push_back( pTzInit );
                    break;
                }

                // use default reserved data for these and treat as a custom package
            case USB_DEVICE_DESCRIPTOR:
            case USB_CONFIG_DESCRIPTOR:
            case USB_INTERFACE_DESCRIPTOR:
            case USB_LANGUAGE_STRING_DESCRIPTOR:
            case USB_MANUFACTURER_STRING_DESCRIPTOR:
            case USB_PRODUCT_STRING_DESCRIPTOR:
            case USB_SERIAL_STRING_DESCRIPTOR:
            case USB_INTERFACE_STRING_DESCRIPTOR:
            case USB_DEFAULT_STRING_DESCRIPTOR:
            case USB_ENDPOINT_DESCRIPTOR:
            case OEMCUSTOMID:
            case NOMONITORID:
            case DDRID:	
            default:
                {
                    // make it an oem custom package instead
                    CReservedPackageData* pCustom = new CReservedPackageData;
                    pCustom->PackageId( HexAsciiToText( HexFormattedAscii( uiPackageId ) ) );
                    pCustom->PackageIdTag( HexFormattedAscii( uiPackageId ) );

                    for ( j = 0; j < (uiPackageSize-8)/4; j++ )
                    {
                        string* pData = new string( HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ]) );
                        iValuePos += 4;
                        string* pComment = new string("");
                        pCustom->AddData( pData, pComment ); 
                    }
                    TimDescriptor.ReservedDataList().push_back( pCustom );
                }
                break;

            }
        }
    }

Term:

    // update tim header SizeOfReservedData based on actual size
    // some reserved data may have been parsed as ExtendedReservedData
    // and if there was any all ones padding, the padding is not included in TIM.txt
    // but will be included in the TIM.bin only if the TBB -O switch is used
    TimDescriptor.TimHeader().SizeOfReserved = TimDescriptor.ReservedDataTotalSize();

    return true;
}

#if TRUSTED
bool CTimBinaryParser::ParseDigitalSignature( unsigned int& iValuePos )
{
    if ( TimDescriptor.TimHeader().VersionBind.Trusted )
    {
        CDigitalSignature& DS = TimDescriptor.DigitalSignature();
        DS.IncludeInTim( true );

        unsigned int iDSAlgorithmID = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
        DS.EncryptAlgorithmId( (ENCRYPTALGORITHMID_T)iDSAlgorithmID );

        unsigned int HashAlgorithmID = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
        if (HashAlgorithmID == 2 || HashAlgorithmID == 32 || HashAlgorithmID == 256 )
            DS.HashAlgorithmId( string("SHA-256") );
        else
            DS.HashAlgorithmId( string("SHA-160") );

        if ( TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
        {
            // size is in bytes prior to version 3.3
            DS.KeySize( (*(unsigned int*)&FileData[ iValuePos ]) ); 
            iValuePos += 4;
        }
        else
        {
            // size is in bits after version 3.3
            DS.KeySize( (*(unsigned int*)&FileData[ iValuePos ]) ); 
            iValuePos += 4;
        }

        UINT_T Hash[8]={0}; // hash in binary is skipped over and discarded
        for( unsigned int i = 0; i < 8; i++ )
        {
            Hash[i] = *(unsigned int*)&FileData[ iValuePos ]; 
            iValuePos += 4;
        }

        if ( TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
        {
            if ( DS.EncryptAlgorithmId() == "ECDSA_256" || DS.EncryptAlgorithmId() == "ECDSA_521" )
                return false;

            t_stringListIter iterPubExp = DS.PublicKeyExponentList().begin();
            while ( iterPubExp != DS.PublicKeyExponentList().end() )
            {
                *(*iterPubExp) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ] ); 
                iValuePos += 4;
                iterPubExp++;
            }

            // discard unused bytes from file because MAXRSAKEYSIZEWORDS were written
            // though not all bytes were actually used and should be 0's
            size_t discard = MAXRSAKEYSIZEWORDS - DS.PublicKeyExponentList().size();
            while ( discard-- > 0 )
            {
                unsigned int discardedValue = *(unsigned int*)&FileData[ iValuePos ];
                iValuePos += 4;
            }

            t_stringListIter iterSysMod = DS.RsaSystemModulusList().begin();
            while ( iterSysMod != DS.RsaSystemModulusList().end() )
            {
                *(*iterSysMod) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ] );
                iValuePos += 4;
                iterSysMod++;
            }
            
            // discard unused bytes from file because MAXRSAKEYSIZEWORDS were written
            // though not all bytes were actually used and should be 0's
            discard = MAXRSAKEYSIZEWORDS - DS.RsaSystemModulusList().size();
            while ( discard-- > 0 )
            {
                unsigned int discardedValue = *(unsigned int*)&FileData[ iValuePos ];
                iValuePos += 4;
            }
        }
        else if ( TimDescriptor.TimHeader().VersionBind.Version >= TIM_3_3_00 )
        {
            if ( DS.EncryptAlgorithmId() == "ECDSA_256" || DS.EncryptAlgorithmId() == "ECDSA_521" )
            {
                t_stringListIter iterPubExp = DS.ECDSAPublicKeyCompXList().begin();
                while ( iterPubExp != DS.ECDSAPublicKeyCompXList().end() )
                {
                    *(*iterPubExp) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ] ); 
                    iValuePos += 4;
                    iterPubExp++;
                }

                // discard unused bytes from file because MAXRSAKEYSIZEWORDS were written
                // though not all bytes were actually used and should be 0's
                size_t discard = MAXECCKEYSIZEWORDS - DS.ECDSAPublicKeyCompXList().size();
                while ( discard-- > 0 )
                {
                    unsigned int discardedValue = *(unsigned int*)&FileData[ iValuePos ];
                    iValuePos += 4;
                }

                iterPubExp = DS.ECDSAPublicKeyCompYList().begin();
                while ( iterPubExp != DS.ECDSAPublicKeyCompYList().end() )
                {
                    *(*iterPubExp) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ] ); 
                    iValuePos += 4;
                    iterPubExp++;
                }
    
                // discard unused bytes from file because MAXRSAKEYSIZEWORDS were written
                // though not all bytes were actually used and should be 0's
                discard = MAXECCKEYSIZEWORDS - DS.ECDSAPublicKeyCompYList().size();
                while ( discard-- > 0 )
                {
                    unsigned int discardedValue = *(unsigned int*)&FileData[ iValuePos ];
                    iValuePos += 4;
                }
            }
            else
            {
                t_stringListIter iterPubExp = DS.PublicKeyExponentList().begin();
                while ( iterPubExp != DS.PublicKeyExponentList().end() )
                {
                    *(*iterPubExp) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ] ); 
                    iValuePos += 4;
                    iterPubExp++;
                }

                // discard unused bytes from file because MAXRSAKEYSIZEWORDS were written
                // though not all bytes were actually used and should be 0's
                size_t discard = MAXRSAKEYSIZEWORDS - DS.PublicKeyExponentList().size();
                while ( discard-- > 0 )
                {
                    unsigned int discardedValue = *(unsigned int*)&FileData[ iValuePos ];
                    iValuePos += 4;
                }

                t_stringListIter iterSysMod = DS.RsaSystemModulusList().begin();
                while ( iterSysMod != DS.RsaSystemModulusList().end() )
                {
                    *(*iterSysMod) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ] );
                    iValuePos += 4;
                    iterSysMod++;
                }

                // discard unused bytes from file because MAXRSAKEYSIZEWORDS were written
                // though not all bytes were actually used and should be 0's
                discard = MAXRSAKEYSIZEWORDS - DS.RsaSystemModulusList().size();
                while ( discard-- > 0 )
                {
                    unsigned int discardedValue = *(unsigned int*)&FileData[ iValuePos ];
                    iValuePos += 4;
                }			
            }
        }
    }
    return true;
}
#endif

void CTimBinaryParser::SkipAllOnesPadding( unsigned int& iValuePos )
{
    while ( *(unsigned int*)&FileData[ iValuePos ] == 0xffffffff )
    {
        iValuePos += 4;
    }
}


void CTimBinaryParser::ParseERDFields( unsigned int uiLoop, unsigned int& iValuePos, CExtendedReservedData& Ext,
                                       void (CExtendedReservedData::*AddField)( std::pair< unsigned int, unsigned int >*& ) )
{
    for ( unsigned int j = 0; j < uiLoop; j++ )
    {
        unsigned int iField = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
        unsigned int iFieldValue = *(unsigned int*)&FileData[ iValuePos ]; iValuePos += 4;
        std::pair<unsigned int, unsigned int>* pPair = new std::pair<unsigned int, unsigned int>( iField, iFieldValue ); 
        (Ext.*AddField)( pPair ); 
    }
}

void CTimBinaryParser::ParseERDBaseFields( CErdBase* pERD, unsigned int uiLoop, unsigned int& iValuePos )
{
    for ( unsigned int j = 0; j < uiLoop; j++ )
    {
        *(pERD->FieldValues()[j]) = HexFormattedAscii(*(unsigned int*)&FileData[ iValuePos ]); 
        iValuePos += 4; 
    }
}
