/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "TimDescriptor.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "EditListCtrl.h"
#include "DownloaderInterface.h"
#include "ImageBuildDlg.h"
#include "TimDownloaderDlg.h"

// CTimDescriptorPropertyPage dialog

class CTimDescriptorPropertyPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CTimDescriptorPropertyPage)

public:
	CTimDescriptorPropertyPage(CDownloaderInterface& rDownloaderInterface);   // standard constructor
	virtual ~CTimDescriptorPropertyPage();

// Dialog Data
	enum { IDD = IDD_TIM_DESCRIPTOR_PROPPAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL OnSetActive();

	DECLARE_MESSAGE_MAP()

public:
	CString sTimDescriptorFilePath;
	CString sIssueDate;
	CString sOemUniqueId;
	CString sBootRomFlashSignature;
	BOOL bTrusted;
	CString sWtmFlashSignature;
	CString sWtmFlashEntryAddress;
	CString sWtmBackupEntryAddress;
	CHexItemEdit IssueDateEdt;
	CHexItemEdit OemUniqueIdEdt;
	CHexItemEdit BootromFlashSignatureEdt;
	CHexItemEdit WtmFlashSignatureEdt;
	CHexItemEdit WtmFlashEntryAddressEdt;
	CHexItemEdit WtmBackupEntryEdt;
	CButton DigitalSignatureBtn;
	CButton KeysBtn;
	CButton AddImageBtn;
	CButton EditImageBtn;
	CButton MoveUpBtn;
	CButton MoveDownBtn;
	CButton RemoveImageBtn;
	CButton WriteTIMBtn;
	CButton ReadTIMBtn;
	CButton ViewTIMBtn;
	CButton ImportBtn;
	CButton TrustedChk;
	CComboBox BootRomFlashSignatureCb;
	CComboBox WTMFlashSignatureCb;
	CComboBox ProcessorTypeCb;
	CListCtrl ImagesListCtrl;
	CButton ImageBuilderBtn;
	CButton DownloadConfigBtn;

	afx_msg void OnEnChangeTimDescriptorFilePathEdt();
	afx_msg void OnEnChangeTimVersionEdt();
	afx_msg void OnEnChangeIssueDateEdt();
	afx_msg void OnEnChangeOemUniqueIdEdt();
	afx_msg void OnCbnSelchangeBootromFlashSignatureCb();
	afx_msg void OnEnChangeBootromFlashSignatureEdt();
	afx_msg void OnEnChangeWtmFlashSignatureEdt();
	afx_msg void OnEnChangeWtmFlashEntryEdt();
	afx_msg void OnCbnSelchangeWtmFlashSignatureCb();
	afx_msg void OnEnChangeWtmBackupEntryEdt();
	afx_msg void OnNMSetfocusImagesListCtrl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMKillfocusImagesListCtrl(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnBnClickedTimDescriptorFilePathBrowseBtn();
	afx_msg void OnBnClickedTimDescriptorWriteBtn();
	afx_msg void OnBnClickedTimDescriptorReadBtn();
	afx_msg void OnBnClickedTrustedChk();
	afx_msg void OnBnClickedTimResetBtn();
	afx_msg void OnBnClickedKeysBtn();

	afx_msg void OnBnClickedReservedDataBtn();
	afx_msg void OnBnClickedAddImageBtn();
	afx_msg void OnBnClickedEditImageBtn();
	afx_msg void OnBnClickedRemoveImageBtn();
	afx_msg void OnBnClickedImagesMoveUpBtn();
	afx_msg void OnBnClickedImageMoveDownBtn();
	afx_msg void OnBnClickedDigitalSignatureBtn();
	afx_msg void OnCbnSelchangeTimProcCb();
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);

	afx_msg void OnBnClickedImageBuilderBtn();
	afx_msg void OnBnClickedTimDescriptorViewBtn();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedTimDownloadConfigBtn();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnBnClickedImportDlgBtn();

public:
	void RefreshState();
	void UpdateChangeMarkerInTitle();

private:
	void UpdateControls();

public:
	CTimDescriptor&			TimDescriptor;
	CDownloaderInterface&	DownloaderInterface;

private:
	CImageBuildDlg			ImageBuildDlg;
	CTimDownloaderDlg		TimDownloaderDlg;
public:
	CComboBox TimVersionCb;
	afx_msg void OnCbnSelchangeTimVersionCb();
	afx_msg void OnCbnEditchangeTimVersionCb();
    virtual BOOL OnKillActive();
};
