/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// ExtReservedDataPkgDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "ExtReservedDataPkgDlg.h"
#include "ReservedDataPkgFieldDlg.h"
#include "TimLib.h"
#include "CommentDlg.h"

#include "AutoBindERD.h"
#include "CoreResetERD.h"
#include "EscapeSeqERD.h"
#include "GpioSetERD.h"
#include "ResumeDdrERD.h"
#include "TbrXferERD.h"
#include "UartERD.h"
#include "UsbERD.h"
#include "UsbVendorReqERD.h"
#include "DDRInitialization.h"
#include "ConsumerID.h"
#include "DDRInitializationPkgDlg.h"
#include "ConsumerPackageDlg.h"
#include "TzInitializationPkgDlg.h"

// CExtReservedDataPkgDlg dialog

IMPLEMENT_DYNAMIC(CExtReservedDataPkgDlg, CDialog)

CExtReservedDataPkgDlg::CExtReservedDataPkgDlg(CTimDescriptor& TimDescriptor, 
    CString& sPkgId, CExtendedReservedData& Ext, bool bEdit, string& sProcessorType, 
    CErdBase*& pErdBase, CErdBase* pParentErdBase, CWnd* pParent /*=NULL*/)
    : CDialog(CExtReservedDataPkgDlg::IDD, pParent), 
      m_LocalTimDescriptor( TimDescriptor ),
      m_sPackageId( sPkgId ), ExtReservedData( Ext ),
      m_bEdit( bEdit ), m_sProcessorType( sProcessorType ), m_pErdBase( pErdBase ), m_pParentErdBase( pParentErdBase )
{
    if ( m_pErdBase == (CErdBase*)CB_ERR )
        m_pErdBase = 0;

    // create temp objects for access to field names
    m_pAutoBind = new CAutoBind;
    m_pEscapeSeq = new CEscapeSeq;
    m_pGpioSet = new CGpioSet;
    m_pGpio = new CGpio;
    m_pResumeDdr = new CResumeDdr;
    m_pTbrXferSet = new CTBRXferSet;
    m_pXfer = new CXfer;
    m_pUart = new CUart;
    m_pUsb = new CUsb;
    m_pUsbVendorReq = new CUsbVendorReq;
    m_pDdrInitialization = new CDDRInitialization;
    m_pConsumerID = new CConsumerID;
    m_pCoreReset = new CCoreReset;
    m_pTzInitialization = new CTzInitialization;
}

CExtReservedDataPkgDlg::~CExtReservedDataPkgDlg()
{
    // destroy temp objects for access to field names
    delete m_pAutoBind;
    delete m_pEscapeSeq;
    delete m_pGpioSet;
    delete m_pGpio;
    delete m_pResumeDdr;
    delete m_pTbrXferSet;
    delete m_pXfer;
    delete m_pUart;
    delete m_pUsb;
    delete m_pUsbVendorReq;
    delete m_pDdrInitialization;
    delete m_pConsumerID;
    delete m_pCoreReset;
    delete m_pTzInitialization;
}

void CExtReservedDataPkgDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EXT_RESERVED_DATA_PKG_ID_CB, PackageIdCb);
    DDX_Control(pDX, IDC_EXT_RESERVED_DATA_PKG_FIELDS_LIST, PackageFieldsListCtrl);
    DDX_Control(pDX, IDC_EXT_RESERVED_DATA_NESTED_PKG_LIST, NestedPackagesList);
    DDX_Control(pDX, IDC_ADD_EXT_RESERVED_BTN, AddNestedPkgBtn);
    DDX_Control(pDX, IDC_EDIT_EXT_RESERVED_BTN, EditNestedPkgBtn);
    DDX_Control(pDX, IDC_DELETE_EXT_PACKAGE_BTN, DeleteNestedPkgBtn);
    DDX_Control(pDX, IDC_EXT_RESERVED_DATA_PKG_INSERT_FIELD_BTN, InsertFieldBtn);
    DDX_Control(pDX, IDC_EXT_RESERVED_DATA_PKG_EDIT_FIELD_BTN, EditFieldBtn);
    DDX_Control(pDX, IDC_EXT_RESERVED_DATA_PKG_MOVE_UP_BTN, MoveUpBtn);
    DDX_Control(pDX, IDC_EXT_RESERVED_DATA_PKG_MOVE_DOWN_BTN, MoveDownBtn);
    DDX_Control(pDX, IDC_EXT_RESERVED_DATA_PKG_DELETE_FIELD_BTN, DeleteFieldBtn);

    // updates marker in title
    UpdateChangeMarkerInTitle();
    theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CExtReservedDataPkgDlg, CDialog)
    ON_BN_CLICKED(IDC_EXT_RESERVED_DATA_PKG_INSERT_FIELD_BTN, &CExtReservedDataPkgDlg::OnBnClickedExtReservedDataPkgInsertFieldBtn)
    ON_BN_CLICKED(IDC_EXT_RESERVED_DATA_PKG_EDIT_FIELD_BTN, &CExtReservedDataPkgDlg::OnBnClickedExtReservedDataPkgEditFieldBtn)
    ON_BN_CLICKED(IDC_EXT_RESERVED_DATA_PKG_MOVE_UP_BTN, &CExtReservedDataPkgDlg::OnBnClickedExtReservedDataPkgMoveUpBtn)
    ON_BN_CLICKED(IDC_EXT_RESERVED_DATA_PKG_MOVE_DOWN_BTN, &CExtReservedDataPkgDlg::OnBnClickedExtReservedDataPkgMoveDownBtn)
    ON_BN_CLICKED(IDC_EXT_RESERVED_DATA_PKG_DELETE_FIELD_BTN, &CExtReservedDataPkgDlg::OnBnClickedExtReservedDataPkgDeleteFieldBtn)
    ON_CBN_SELCHANGE(IDC_EXT_RESERVED_DATA_PKG_ID_CB, &CExtReservedDataPkgDlg::OnCbnSelchangeExtReservedDataPkgIdCb)
    ON_BN_CLICKED(IDC_EDIT_EXT_RESERVED_BTN, &CExtReservedDataPkgDlg::OnBnClickedEditExtReservedBtn)
    ON_BN_CLICKED(IDC_DELETE_EXT_PACKAGE_BTN, &CExtReservedDataPkgDlg::OnBnClickedDeleteExtPackageBtn)
    ON_BN_CLICKED(IDC_ADD_EXT_RESERVED_BTN, &CExtReservedDataPkgDlg::OnBnClickedAddExtReservedBtn)
    ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()


// CExtReservedDataPkgDlg message handlers

BOOL CExtReservedDataPkgDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    PackageFieldsListCtrl.SetExtendedStyle(    LVS_EX_TRACKSELECT
                                          //  LVS_EX_BORDERSELECT
                                          | LVS_EX_FULLROWSELECT 
                                          | LVS_EX_HEADERDRAGDROP 
                                          | LVS_EX_ONECLICKACTIVATE 
                                          | LVS_EX_LABELTIP 
                                          //| LVS_EX_GRIDLINES
                                         );

    PackageFieldsListCtrl.InsertColumn(0,"Field Name", LVCFMT_LEFT, 150, -1 );
    PackageFieldsListCtrl.InsertColumn(1,"Value", LVCFMT_LEFT, 77, -1 );

    int nItem = -1;
    if ( !m_pParentErdBase )
    {

        PackageIdCb.InsertString(++nItem,(sClockEnable+" (CLKE)").c_str()); PackageIdCb.SetItemData(nItem,(DWORD_PTR)&ExtReservedData.g_ClockEnableFields);
        PackageIdCb.InsertString(++nItem,(sDDRGeometry+" (DDRG)").c_str()); PackageIdCb.SetItemData(nItem,(DWORD_PTR)&ExtReservedData.g_SdramSpecFields);
        PackageIdCb.InsertString(++nItem,(sDDRTiming+" (DDRT)").c_str()); PackageIdCb.SetItemData(nItem,(DWORD_PTR)&ExtReservedData.g_SdramSpecFields);
        PackageIdCb.InsertString(++nItem,(sDDRCustom+" (DDRC)").c_str()); PackageIdCb.SetItemData(nItem,(DWORD_PTR)&ExtReservedData.g_DDRCustomFields);
        PackageIdCb.InsertString(++nItem,(sFrequency+" (FREQ)").c_str()); PackageIdCb.SetItemData(nItem,(DWORD_PTR)&ExtReservedData.g_FrequencyFields);
        PackageIdCb.InsertString(++nItem,(sVoltages+" (VOLT)").c_str()); PackageIdCb.SetItemData(nItem,(DWORD_PTR)&ExtReservedData.g_VoltagesFields);
        PackageIdCb.InsertString(++nItem,(sConfigMemoryControl+" (CCMC)").c_str()); PackageIdCb.SetItemData(nItem,(DWORD_PTR)&ExtReservedData.g_ConfigMemoryControlFields);
        
        if ( m_sProcessorType == gsProcessorType[PXA688] 
             || m_sProcessorType == gsProcessorType[ARMADA610]
             || m_sProcessorType == gsProcessorType[ARMADA620] )
//			 || m_sProcessorType == gsProcessorType[P_88AP001] )
        {
            PackageIdCb.InsertString(++nItem,(sTrustZone+" (TZID)").c_str()); PackageIdCb.SetItemData(nItem,(DWORD_PTR)&ExtReservedData.g_TrustZoneFields);
            PackageIdCb.InsertString(++nItem,(sTrustZoneRegid+" (TZON)").c_str()); PackageIdCb.SetItemData(nItem,(DWORD_PTR)&ExtReservedData.g_TrustZoneRegidFields);
        }

        if ( m_sProcessorType == gsProcessorType[ARMADA620] )
        {
            PackageIdCb.InsertString(++nItem,(m_pTzInitialization->PackageName()+" (TZRI)").c_str());PackageIdCb.SetItemData(nItem,(DWORD_PTR)&m_pTzInitialization->FieldNames());
        }

        if ( m_sProcessorType == gsProcessorType[PXA168] || m_sProcessorType == gsProcessorType[ARMADA168] )
        {
            PackageIdCb.InsertString(++nItem,(sOpDiv+" (OPDV)").c_str()); PackageIdCb.SetItemData(nItem,(DWORD_PTR)&ExtReservedData.g_OpDivFields);
            PackageIdCb.InsertString(++nItem,(sOpMode+" (MODE)").c_str()); PackageIdCb.SetItemData(nItem,(DWORD_PTR)&ExtReservedData.g_OpModeFields);
        }

        PackageIdCb.InsertString(++nItem,(m_pAutoBind->PackageName()+" (BIND)").c_str());PackageIdCb.SetItemData(nItem,(DWORD_PTR)&m_pAutoBind->FieldNames());
        PackageIdCb.InsertString(++nItem,(m_pCoreReset->PackageName()+" (CORE)").c_str());PackageIdCb.SetItemData(nItem,(DWORD_PTR)&m_pCoreReset->FieldNames());
        PackageIdCb.InsertString(++nItem,(m_pEscapeSeq->PackageName()+" (ESCS)").c_str());PackageIdCb.SetItemData(nItem,(DWORD_PTR)&m_pEscapeSeq->FieldNames());
        PackageIdCb.InsertString(++nItem,m_pGpioSet->PackageName().c_str());PackageIdCb.SetItemData(nItem,(DWORD_PTR)&m_pGpioSet->FieldNames());
        PackageIdCb.InsertString(++nItem,(m_pResumeDdr->PackageName()+" (Rsm2)").c_str());PackageIdCb.SetItemData(nItem,(DWORD_PTR)&m_pResumeDdr->FieldNames());
        PackageIdCb.InsertString(++nItem,m_pTbrXferSet->PackageName().c_str());PackageIdCb.SetItemData(nItem,(DWORD_PTR)&m_pTbrXferSet->FieldNames());
        PackageIdCb.InsertString(++nItem,(m_pUart->PackageName()+" (UART)").c_str());PackageIdCb.SetItemData(nItem,(DWORD_PTR)&m_pUart->FieldNames());
        PackageIdCb.InsertString(++nItem,(m_pUsb->PackageName()+" (USB)").c_str());PackageIdCb.SetItemData(nItem,(DWORD_PTR)&m_pUsb->FieldNames());
        PackageIdCb.InsertString(++nItem,(m_pUsbVendorReq->PackageName()+" (VREQ)").c_str());PackageIdCb.SetItemData(nItem,(DWORD_PTR)&m_pUsbVendorReq->FieldNames());
        PackageIdCb.InsertString(++nItem,(m_pConsumerID->PackageName()+" (CIDP)").c_str());PackageIdCb.SetItemData(nItem,(DWORD_PTR)&m_pConsumerID->FieldNames());
        PackageIdCb.InsertString(++nItem,(m_pDdrInitialization->PackageName()+" (DDRn)").c_str());PackageIdCb.SetItemData(nItem,(DWORD_PTR)&m_pDdrInitialization->FieldNames());
    }
    else
    {
        if ( m_pParentErdBase->ErdPkgType() == CErdBase::GPIOSET_ERD )
        {
            PackageIdCb.InsertString(0,(m_pGpio->PackageName()+" (GPIO)").c_str());
            PackageIdCb.SetItemData(0,(DWORD_PTR)&m_pGpio->FieldNames());
        }
        else if ( m_pParentErdBase->ErdPkgType() == CErdBase::TBRXFER_ERD )
        {
            PackageIdCb.InsertString(0,(m_pXfer->PackageName()+" (TBRX)").c_str());
            PackageIdCb.SetItemData(0,(DWORD_PTR)&m_pXfer->FieldNames());
        }
        else if ( m_pParentErdBase->ErdPkgType() == CErdBase::USBVENDORREQ_ERD )
        {
            CUsbVendorReq& UsbVendorReq = dynamic_cast<CUsbVendorReq&>(*m_pParentErdBase);
            PackageIdCb.InsertString(0,"DATA");
            // in this case, erdbase actually point to the specific data[n] string
            PackageIdCb.SetItemData(0,(DWORD_PTR)&UsbVendorReq.DataFieldNames());
            // need to clear the pointer so it is not misinterpreted as an ERD
            m_pErdBase = 0;
        }
    }

    int idx = -1;
    NestedPackagesList.SetExtendedStyle(    LVS_EX_TRACKSELECT
                                          //  LVS_EX_BORDERSELECT
                                          | LVS_EX_FULLROWSELECT 
                                          | LVS_EX_HEADERDRAGDROP 
                                          | LVS_EX_ONECLICKACTIVATE 
                                          | LVS_EX_LABELTIP 
                                          //| LVS_EX_GRIDLINES
                                         );

    NestedPackagesList.InsertColumn(0,"Package ID", LVCFMT_LEFT, 150, -1 );
    NestedPackagesList.InsertColumn(1,"#Fields", LVCFMT_LEFT, 60, -1 );
    NestedPackagesList.InsertColumn(2,"Field(s) : Value(s)", LVCFMT_LEFT, 485, -1 );

    RefreshState();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void CExtReservedDataPkgDlg::UpdateControls()
{
    InsertFieldBtn.EnableWindow(false);
    EditFieldBtn.EnableWindow(false);
    MoveUpBtn.EnableWindow(false);
    MoveDownBtn.EnableWindow(false);
    DeleteFieldBtn.EnableWindow(false);

    AddNestedPkgBtn.EnableWindow(false);
    EditNestedPkgBtn.EnableWindow(false);
    DeleteNestedPkgBtn.EnableWindow(false);
    NestedPackagesList.EnableWindow(false);

    if ( m_pErdBase && !m_pParentErdBase )
    {
        int Id = PackageIdCb.SetCurSel(PackageIdCb.FindString(0, m_pErdBase->PackageName().c_str()));
        if ( Id != CB_ERR )
            PackageIdCb.EnableWindow(false);

        EditFieldBtn.EnableWindow(true);

        switch ( m_pErdBase->ErdPkgType() )
        {
            case CErdBase::GPIOSET_ERD:
            case CErdBase::TBRXFER_ERD:
                AddNestedPkgBtn.EnableWindow(true);
                EditNestedPkgBtn.EnableWindow(true);
                DeleteNestedPkgBtn.EnableWindow(true);
                NestedPackagesList.EnableWindow(true);
                break;

            case CErdBase::USBVENDORREQ_ERD:
                EditNestedPkgBtn.EnableWindow(true);
                NestedPackagesList.EnableWindow(true);
                break;
        }
    }
    else if ( m_pParentErdBase )
    {
        int Id = PackageIdCb.SetCurSel(0);
        if ( Id != CB_ERR )
        {
            PackageIdCb.EnableWindow(false);
        }

        EditFieldBtn.EnableWindow(true);

        switch ( m_pParentErdBase->ErdPkgType() )
        {
            case CErdBase::USBVENDORREQ_ERD:
                if ( m_sPackageId == "DATA" )
                {
                    InsertFieldBtn.EnableWindow(PackageIdCb.GetCurSel()!=CB_ERR);
                    MoveUpBtn.EnableWindow(PackageFieldsListCtrl.GetItemCount()>1);
                    MoveDownBtn.EnableWindow(PackageFieldsListCtrl.GetItemCount()>1);
                    DeleteFieldBtn.EnableWindow(PackageFieldsListCtrl.GetItemCount()>0);
                    break;
                }
                // else fall through
            case CErdBase::GPIO_ERD:
            case CErdBase::XFER_ERD:
                break;
        }
    }
    else
    {
        PackageIdCb.SetCurSel(PackageIdCb.FindString(0, m_sPackageId));
        InsertFieldBtn.EnableWindow(PackageIdCb.GetCurSel()!=CB_ERR);
        EditFieldBtn.EnableWindow(PackageFieldsListCtrl.GetItemCount()>0);
        MoveUpBtn.EnableWindow(PackageFieldsListCtrl.GetItemCount()>1);
        MoveDownBtn.EnableWindow(PackageFieldsListCtrl.GetItemCount()>1);
        DeleteFieldBtn.EnableWindow(PackageFieldsListCtrl.GetItemCount()>0);
    }

    PackageIdCb.EnableWindow(!m_bEdit);
}

void CExtReservedDataPkgDlg::RefreshState()
{
    UpdateData( TRUE );

    NestedPackagesList.DeleteAllItems();

    int nItem = 0;
    if ( m_sPackageId == (LPCTSTR)sClockEnable.c_str() )
    {
        UpdateFields( nItem, ExtReservedData.ClockEnableFields, ExtReservedData.g_ClockEnableFields );
    }
    else if ( m_sPackageId == sDDRGeometry.c_str() )
    {
        UpdateFields( nItem, ExtReservedData.DDRGeometryFields, ExtReservedData.g_SdramSpecFields );
    }
    else if ( m_sPackageId == sDDRTiming.c_str() )
    {
        UpdateFields( nItem, ExtReservedData.DDRTimingFields, ExtReservedData.g_SdramSpecFields );
    }
    else if ( m_sPackageId == sDDRCustom.c_str() )
    {
        UpdateFields( nItem, ExtReservedData.DDRCustomFields, ExtReservedData.g_DDRCustomFields );
    }
    else if ( m_sPackageId == sFrequency.c_str() )
    {
        UpdateFields( nItem, ExtReservedData.FrequencyFields, ExtReservedData.g_FrequencyFields );
    }
    else if ( m_sPackageId == sVoltages.c_str() )
    {
        UpdateFields( nItem, ExtReservedData.VoltagesFields, ExtReservedData.g_VoltagesFields );
    }	
    else if ( m_sPackageId == sConfigMemoryControl.c_str() )
    {
        UpdateFields( nItem, ExtReservedData.ConfigMemoryControlFields, ExtReservedData.g_ConfigMemoryControlFields );
    }	
    else if ( m_sPackageId == sTrustZoneRegid.c_str() )
    {
        UpdateFields( nItem, ExtReservedData.TrustZoneRegidFields, ExtReservedData.g_TrustZoneRegidFields );
    }	
    else if ( m_sPackageId == sTrustZone.c_str() )
    {
        UpdateFields( nItem, ExtReservedData.TrustZoneFields, ExtReservedData.g_TrustZoneFields );
    }	
    else if ( m_sPackageId == sOpDiv.c_str() )
    {
        UpdateFields( nItem, ExtReservedData.OpDivFields, ExtReservedData.g_OpDivFields );
    }	
    else if ( m_sPackageId == sOpMode.c_str() )
    {
        UpdateFields( nItem, ExtReservedData.OpModeFields, ExtReservedData.g_OpModeFields );
    }	
    else if ( m_pErdBase && !m_pParentErdBase && (m_sPackageId == m_pErdBase->PackageName().c_str()) )
    {
        // edit an existing ERD
        int nNestedItem = 0;
        CTimLib TimLib;

        if ( m_pErdBase->ErdPkgType() == CErdBase::GPIOSET_ERD )
        {
            CGpioSet& GpioSet = dynamic_cast<CGpioSet&>(*m_pErdBase);
            t_stringVector& FieldValues = GpioSet.FieldValues();
            *FieldValues[ CGpioSet::NUM_GPIOS ] = TimLib.HexFormattedAscii((unsigned int)GpioSet.GpiosList().size());

            UpdateFields( nItem, m_pErdBase );

            t_GpioListIter& ErdIter = GpioSet.GpiosList().begin();
            while ( ErdIter != GpioSet.GpiosList().end() )
            {
                NestedPackagesList.InsertItem( nNestedItem, (*ErdIter)->PackageName().c_str() );
                stringstream ss;
                ss << (*ErdIter)->PackageSize()/4;
                NestedPackagesList.SetItemText( nNestedItem, 1, ss.str().c_str() );
                string sDataBlock;
                t_stringVector& FieldNames = (*ErdIter)->FieldNames();
                t_stringVector& FieldValues = (*ErdIter)->FieldValues();
                for ( unsigned int i = 0; i < (*ErdIter)->MaxFieldNum(); i++ )
                {
                    sDataBlock += *FieldNames[i];
                    sDataBlock += ":";
                    sDataBlock += *FieldValues[i];
                    sDataBlock += ";";
                }
                NestedPackagesList.SetItemText( nNestedItem, 2, sDataBlock.c_str() );
                NestedPackagesList.SetItemData( nNestedItem, (DWORD_PTR)(*ErdIter) );
                nNestedItem++;
                ErdIter++;
            }
        }
        else if ( m_pErdBase->ErdPkgType() == CErdBase::TBRXFER_ERD )
        {
            CTBRXferSet& TbrXferSet = dynamic_cast<CTBRXferSet&>(*m_pErdBase);
            t_stringVector& FieldValues = TbrXferSet.FieldValues();
            *FieldValues[ CTBRXferSet::NUM_DATA_PAIRS ] = TimLib.HexFormattedAscii((unsigned int)TbrXferSet.XfersList().size());

            UpdateFields( nItem, m_pErdBase );

            t_XferListIter& ErdIter = TbrXferSet.XfersList().begin();
            while ( ErdIter != TbrXferSet.XfersList().end() )
            {
                NestedPackagesList.InsertItem( nNestedItem, (*ErdIter)->PackageName().c_str() );
                stringstream ss;
                ss << (*ErdIter)->PackageSize()/4;
                NestedPackagesList.SetItemText( nNestedItem, 1, ss.str().c_str() );
                string sDataBlock;
                t_stringVector& FieldNames = (*ErdIter)->FieldNames();
                t_stringVector& FieldValues = (*ErdIter)->FieldValues();
                for ( unsigned int i = 0; i < (*ErdIter)->MaxFieldNum(); i++ )
                {
                    sDataBlock += *FieldNames[i];
                    sDataBlock += ":";
                    sDataBlock += *FieldValues[i];
                    sDataBlock += ";";
                }
                NestedPackagesList.SetItemText( nNestedItem, 2, sDataBlock.c_str() );
                NestedPackagesList.SetItemData( nNestedItem, (DWORD_PTR)(*ErdIter) );
                nNestedItem++;
                ErdIter++;
            }
        }
        else if ( m_pErdBase->ErdPkgType() == CErdBase::USBVENDORREQ_ERD )
        {
            UpdateFields( nItem, m_pErdBase );

            CUsbVendorReq& UsbVendorReq = dynamic_cast<CUsbVendorReq&>(*m_pErdBase);
            NestedPackagesList.InsertItem( nNestedItem, "DATA" );
            stringstream ss;
            ss << UsbVendorReq.DataList().size()/4;
            NestedPackagesList.SetItemText( nNestedItem, 1, ss.str().c_str() );
            string sDataBlock;
            t_stringVectorIter& strIter = UsbVendorReq.DataList().begin();
            while ( strIter != UsbVendorReq.DataList().end() )
            {
                sDataBlock += *(*strIter);
                sDataBlock += ";";
                strIter++;
            }
            NestedPackagesList.SetItemText( nNestedItem, 2, sDataBlock.c_str() );
            NestedPackagesList.SetItemData( nNestedItem, (DWORD_PTR)(&UsbVendorReq.DataList()) );
        }
        else
            UpdateFields( nItem, m_pErdBase );
    }
    else if ( !m_pErdBase && !m_pParentErdBase && !m_sPackageId.IsEmpty())
    {
        // add a new ERD based on selected package id
        m_pErdBase = CErdBase::Create( string(CStringA(m_sPackageId) ) );
    
        int nNestedItem = 0;
        CTimLib TimLib;

        if ( m_pErdBase->ErdPkgType() == CErdBase::GPIOSET_ERD )
        {
            CGpioSet& GpioSet = dynamic_cast<CGpioSet&>(*m_pErdBase);
            t_stringVector& FieldValues = GpioSet.FieldValues();
            *FieldValues[ CGpioSet::NUM_GPIOS ] = TimLib.HexFormattedAscii((unsigned int)GpioSet.GpiosList().size());

            UpdateFields( nItem, m_pErdBase );

            t_GpioListIter& ErdIter = GpioSet.GpiosList().begin();
            while ( ErdIter != GpioSet.GpiosList().end() )
            {
                NestedPackagesList.InsertItem( nNestedItem, (*ErdIter)->PackageName().c_str() );
                stringstream ss;
                ss << (*ErdIter)->PackageSize()/4;
                NestedPackagesList.SetItemText( nNestedItem, 1, ss.str().c_str() );
                string sDataBlock;
                t_stringVector& FieldNames = (*ErdIter)->FieldNames();
                t_stringVector& FieldValues = (*ErdIter)->FieldValues();
                for ( unsigned int i = 0; i < (*ErdIter)->MaxFieldNum(); i++ )
                {
                    sDataBlock += *FieldNames[i];
                    sDataBlock += ":";
                    sDataBlock += *FieldValues[i];
                    sDataBlock += ";";
                }
                NestedPackagesList.SetItemText( nNestedItem, 2, sDataBlock.c_str() );
                NestedPackagesList.SetItemData( nNestedItem, (DWORD_PTR)(*ErdIter) );
                nNestedItem++;
                ErdIter++;
            }
        }
        else if ( m_pErdBase->ErdPkgType() == CErdBase::TBRXFER_ERD )
        {
            UpdateFields( nItem, m_pErdBase );

            CTBRXferSet& TbrXferSet = dynamic_cast<CTBRXferSet&>(*m_pErdBase);
            t_XferListIter& ErdIter = TbrXferSet.XfersList().begin();
            while ( ErdIter != TbrXferSet.XfersList().end() )
            {
                NestedPackagesList.InsertItem( nNestedItem, (*ErdIter)->PackageName().c_str() );
                stringstream ss;
                ss << (*ErdIter)->PackageSize()/4;
                NestedPackagesList.SetItemText( nNestedItem, 1, ss.str().c_str() );
                string sDataBlock;
                t_stringVector& FieldNames = (*ErdIter)->FieldNames();
                t_stringVector& FieldValues = (*ErdIter)->FieldValues();
                for ( unsigned int i = 0; i < (*ErdIter)->MaxFieldNum(); i++ )
                {
                    sDataBlock += *FieldNames[i];
                    sDataBlock += ":";
                    sDataBlock += *FieldValues[i];
                    sDataBlock += ";";
                }
                NestedPackagesList.SetItemText( nNestedItem, 2, sDataBlock.c_str() );
                NestedPackagesList.SetItemData( nNestedItem, (DWORD_PTR)(*ErdIter) );
                nNestedItem++;
                ErdIter++;
            }
        }
        else if ( m_pErdBase->ErdPkgType() == CErdBase::USBVENDORREQ_ERD )
        {
            UpdateFields( nItem, m_pErdBase );

            CUsbVendorReq& UsbVendorReq = dynamic_cast<CUsbVendorReq&>(*m_pErdBase);
            NestedPackagesList.InsertItem( nNestedItem, "DATA" );
            stringstream ss;
            ss << UsbVendorReq.DataList().size()/4;
            NestedPackagesList.SetItemText( nNestedItem, 1, ss.str().c_str() );
            string sDataBlock;
            t_stringVectorIter& strIter = UsbVendorReq.DataList().begin();
            while ( strIter != UsbVendorReq.DataList().end() )
            {
                sDataBlock += *(*strIter);
                sDataBlock += ";";
                strIter++;
            }
            NestedPackagesList.SetItemText( nNestedItem, 2, sDataBlock.c_str() );
            NestedPackagesList.SetItemData( nNestedItem, (DWORD_PTR)(&UsbVendorReq.DataList()) );
        }
        else if ( m_pErdBase->ErdPkgType() == CErdBase::CONSUMER_ID_ERD )
        {
            CConsumerID* pConsumerId = dynamic_cast<CConsumerID*>( m_pErdBase );
            if ( pConsumerId )
            {
                CConsumerPackageDlg Dlg( m_LocalTimDescriptor, pConsumerId );
                if ( Dlg.DoModal() == IDOK )
                {
                    *pConsumerId = *Dlg.m_pConsumerID;
                    OnOK();
                    return;
                }
                OnCancel();
                return;
            }
        }
        else if ( m_pErdBase->ErdPkgType() == CErdBase::DDR_INITIALIZATION_ERD )
        {
            CDDRInitialization* pDDRInit = dynamic_cast<CDDRInitialization*>( m_pErdBase );
            if ( pDDRInit )
            {
                CDDRInitializationPkgDlg Dlg( m_LocalTimDescriptor, *pDDRInit );
                if ( Dlg.DoModal() == IDOK )
                {
                    *pDDRInit = Dlg.m_DDRInit;
                    OnOK();
                    return;
                }
                OnCancel();
                return;
            }
        }
        else if ( m_pErdBase->ErdPkgType() == CErdBase::TZ_INITIALIZATION_ERD )
        {
            CTzInitialization* pTzInit = dynamic_cast<CTzInitialization*>( m_pErdBase );
            if ( pTzInit )
            {
                CTzInitializationPkgDlg Dlg( m_LocalTimDescriptor, *pTzInit );
                if ( Dlg.DoModal() == IDOK )
                {
                    *pTzInit = Dlg.m_TzInit;
                    OnOK();
                    return;
                }
                OnCancel();
                return;
            }
        }
        else
            UpdateFields( nItem, m_pErdBase );
    }
    else if ( !m_pErdBase && m_pParentErdBase )
    {
        // adding a new nested ERD
        CTimLib TimLib;

        if ( m_pParentErdBase->ErdPkgType() == CErdBase::GPIOSET_ERD )
        {
            CGpioSet& GpioSet = dynamic_cast<CGpioSet&>(*m_pParentErdBase);
            t_stringVector& FieldValues = GpioSet.FieldValues();
            *FieldValues[ CGpioSet::NUM_GPIOS ] = TimLib.HexFormattedAscii((unsigned int)GpioSet.GpiosList().size());

            m_pErdBase = CErdBase::Create( CErdBase::GPIO_ERD );
            UpdateFields( nItem, m_pErdBase );
        }
        else if ( m_pParentErdBase->ErdPkgType() == CErdBase::TBRXFER_ERD )
        {
            CTBRXferSet& TbrXferSet = dynamic_cast<CTBRXferSet&>(*m_pParentErdBase);
            t_stringVector& FieldValues = TbrXferSet.FieldValues();
            *FieldValues[ CTBRXferSet::NUM_DATA_PAIRS ] = TimLib.HexFormattedAscii((unsigned int)TbrXferSet.XfersList().size());

            m_pErdBase = CErdBase::Create( CErdBase::XFER_ERD );
            UpdateFields( nItem, m_pErdBase );
        }
        else if ( m_pParentErdBase->ErdPkgType() == CErdBase::USBVENDORREQ_ERD )
        {
            PackageFieldsListCtrl.DeleteAllItems();
            CUsbVendorReq& UsbVendorReq = dynamic_cast<CUsbVendorReq&>(*m_pParentErdBase);
            string sDataBlock;
            int idx = 0;
            t_stringVectorIter& NamesIter = UsbVendorReq.DataFieldNames().begin();
            t_stringVectorIter& DataIter = UsbVendorReq.DataList().begin();
            while ( DataIter != UsbVendorReq.DataList().end() )
            {
                PackageFieldsListCtrl.InsertItem( nItem, (*NamesIter)->c_str() );
                PackageFieldsListCtrl.SetItemText( nItem, 1, (*DataIter)->c_str() );
                PackageFieldsListCtrl.SetItemData( nItem, (DWORD_PTR)(nItem));
                NamesIter++;
                DataIter++;
                nItem++;
            }
        }
        else
            UpdateFields( nItem, m_pErdBase );
    }
    else if ( m_pErdBase && m_pParentErdBase )
    {
        // edit an existing nested ERD
        CTimLib TimLib;

        if ( m_pParentErdBase->ErdPkgType() == CErdBase::GPIOSET_ERD )
        {
            CGpioSet& GpioSet = dynamic_cast<CGpioSet&>(*m_pParentErdBase);
            t_stringVector& FieldValues = GpioSet.FieldValues();
            *FieldValues[ CGpioSet::NUM_GPIOS ] = TimLib.HexFormattedAscii((unsigned int)GpioSet.GpiosList().size());

            UpdateFields( nItem, m_pErdBase );
        }
        else if ( m_pParentErdBase->ErdPkgType() == CErdBase::TBRXFER_ERD )
        {
            CTBRXferSet& TbrXferSet = dynamic_cast<CTBRXferSet&>(*m_pParentErdBase);
            t_stringVector& FieldValues = TbrXferSet.FieldValues();
            *FieldValues[ CTBRXferSet::NUM_DATA_PAIRS ] = TimLib.HexFormattedAscii((unsigned int)TbrXferSet.XfersList().size());

            UpdateFields( nItem, m_pErdBase );
        }
        else if ( m_pParentErdBase->ErdPkgType() == CErdBase::USBVENDORREQ_ERD )
        {
            CUsbVendorReq& UsbVendorReq = dynamic_cast<CUsbVendorReq&>(*m_pParentErdBase);
            string sDataBlock;
            int idx = 0;
            t_stringVectorIter& NamesIter = UsbVendorReq.DataFieldNames().begin();
            t_stringVectorIter& DataIter = UsbVendorReq.DataList().begin();
            while ( DataIter != UsbVendorReq.DataList().end() )
            {
                PackageFieldsListCtrl.InsertItem( nItem, (*NamesIter)->c_str() );
                PackageFieldsListCtrl.SetItemText( nItem, 1, (*DataIter)->c_str() );
                PackageFieldsListCtrl.SetItemData( nItem, (DWORD_PTR)(nItem));
                NamesIter++;
                DataIter++;
                nItem++;
            }
        }
        else
            UpdateFields( nItem, m_pErdBase );
    }
    
    UpdateControls();
    UpdateData( FALSE );
}

void CExtReservedDataPkgDlg::OnBnClickedExtReservedDataPkgInsertFieldBtn()
{
    unsigned int iSelFieldId = LB_ERR;

    // get selected field, if any
//	CString sSelFieldName = "";
    int nItem = PackageFieldsListCtrl.GetSelectionMark();
    if ( nItem != LB_ERR )
    {
//		sSelFieldName = PackageFieldsListCtrl.GetItemText(nItem, 0 );
        iSelFieldId = nItem;
    }

    CString sPackageId = "";
    int nId = PackageIdCb.GetCurSel();
    if ( nId != CB_ERR )
        PackageIdCb.GetLBText( nId, sPackageId );
    int pos = sPackageId.Find( " (" );
    if ( pos > -1 )
        sPackageId = sPackageId.Left( pos );

    t_stringVector* pFieldNames = 0;
    if ( m_pErdBase )
        pFieldNames = &m_pErdBase->FieldNames();
    else if ( m_pErdBase == 0 && nId != CB_ERR && m_pParentErdBase && m_pParentErdBase->ErdPkgType() == CErdBase::USBVENDORREQ_ERD )
        pFieldNames = &((CUsbVendorReq*)m_pParentErdBase)->DataFieldNames();
    else if ( m_pParentErdBase )
        pFieldNames = &m_pParentErdBase->FieldNames();
    else if ( m_pErdBase == 0 && nId != CB_ERR )
        pFieldNames = ((t_stringVector*)PackageIdCb.GetItemData(nId));

    // get the list of currently defined fields in the package
    t_PairList PkgFieldsList;
    GetDefinedPkgFields( PkgFieldsList );

    CReservedDataPkgFieldDlg Dlg(true, 
                                 m_sPackageId, 
//								 sSelFieldName, 
                                 iSelFieldId, 
                                 pFieldNames, 
                                 PkgFieldsList);

    if ( IDOK == Dlg.DoModal() )
    {
        UpdatePkgFields( PkgFieldsList );
        ExtReservedData.Changed(true);
    }

    t_PairListIter iter = PkgFieldsList.begin();
    while( iter != PkgFieldsList.end() )
    {
        delete *iter;
        iter++;
    }
    PkgFieldsList.clear();

    UpdateData(FALSE);
    SetFocus();
}

void CExtReservedDataPkgDlg::OnBnClickedExtReservedDataPkgEditFieldBtn()
{
    int nId = PackageIdCb.GetCurSel();
    if ( nId != CB_ERR )
    {
        PackageIdCb.GetLBText( nId, m_sPackageId );
        int pos = m_sPackageId.Find( " (" );
        if ( pos > -1 )
            m_sPackageId = m_sPackageId.Left( pos );

        unsigned int iSelFieldId = 0;

        // get selected field, if any
        int nItem = PackageFieldsListCtrl.GetSelectionMark();
        CString sSelFieldName;
        if ( nItem != LB_ERR )
        {
//			sSelFieldName = PackageFieldsListCtrl.GetItemText(nItem, 0 );
            iSelFieldId = nItem;
        }
        else
        {
            AfxMessageBox("Select a field to edit.");
            return;
        }

        t_stringVector* pFieldNames = (t_stringVector*)PackageIdCb.GetItemData(nId);

        // get the list of currently defined fields in the package
        t_PairList PkgFieldsList;
        GetDefinedPkgFields( PkgFieldsList );

        CReservedDataPkgFieldDlg Dlg(false, 
                                     m_sPackageId, 
//									 sSelFieldName, 
                                     iSelFieldId, 
                                     pFieldNames, 
                                     PkgFieldsList);

        if ( IDOK == Dlg.DoModal() )
        {
            UpdatePkgFields( PkgFieldsList );
            ExtReservedData.Changed(true);
        }

        if ( m_pErdBase )
        {
            t_PairListIter iter = PkgFieldsList.begin();
            while( iter != PkgFieldsList.end() )
            {
                delete *iter;
                iter++;
            }
            PkgFieldsList.clear();
        }
    }
    UpdateData(FALSE);
    SetFocus();
}

void CExtReservedDataPkgDlg::OnBnClickedExtReservedDataPkgMoveUpBtn()
{
    int nItem = PackageFieldsListCtrl.GetSelectionMark();
    int nId = PackageIdCb.GetCurSel();
    if ( nItem > 0 && nId != CB_ERR )
    {
        unsigned int iSelFieldId = nItem;

        if ( m_sPackageId == sClockEnable.c_str() )
        {
            MoveFieldUp( ExtReservedData.ClockEnableFields, iSelFieldId );
        }
        else if ( m_sPackageId == sDDRGeometry.c_str() )
        {
            MoveFieldUp( ExtReservedData.DDRGeometryFields, iSelFieldId );
        }
        else if ( m_sPackageId == sDDRTiming.c_str() )
        {
            MoveFieldUp( ExtReservedData.DDRTimingFields, iSelFieldId );
        }
        else if ( m_sPackageId == sDDRCustom.c_str() )
        {
            MoveFieldUp( ExtReservedData.DDRCustomFields, iSelFieldId );
        }
        else if ( m_sPackageId == sFrequency.c_str() )
        {
            MoveFieldUp( ExtReservedData.FrequencyFields, iSelFieldId );
        }
        else if ( m_sPackageId == sVoltages.c_str() )
        {
            MoveFieldUp( ExtReservedData.VoltagesFields, iSelFieldId );
        }
        else if ( m_sPackageId == sConfigMemoryControl.c_str() )
        {
            MoveFieldUp( ExtReservedData.ConfigMemoryControlFields, iSelFieldId );
        }
        else if ( m_sPackageId == sTrustZoneRegid.c_str() )
        {
            MoveFieldUp( ExtReservedData.TrustZoneRegidFields, iSelFieldId );
        }
        else if ( m_sPackageId == sTrustZone.c_str() )
        {
            MoveFieldUp( ExtReservedData.TrustZoneFields, iSelFieldId );
        }
        else if ( m_sPackageId == sOpDiv.c_str() )
        {
            MoveFieldUp( ExtReservedData.OpDivFields, iSelFieldId );
        }
        else if ( m_sPackageId == sOpMode.c_str() )
        {
            MoveFieldUp( ExtReservedData.OpModeFields, iSelFieldId );
        }
        else if ( m_pErdBase )
        {
            //  cannot move fields in CErdBase objects
        }

        PackageFieldsListCtrl.SetSelectionMark( --nItem );
        ExtReservedData.Changed(true);
        UpdateData(FALSE);
    }
}

void CExtReservedDataPkgDlg::OnBnClickedExtReservedDataPkgMoveDownBtn()
{
    int nItem = PackageFieldsListCtrl.GetSelectionMark();
    int nId = PackageIdCb.GetCurSel();
    if ( nItem >= 0 && nId != CB_ERR )
    {
        unsigned int iSelFieldId = nItem;

        if ( m_sPackageId == sClockEnable.c_str() )
        {
            MoveFieldDown( ExtReservedData.ClockEnableFields, iSelFieldId );
        }
        else if ( m_sPackageId == sDDRGeometry.c_str() )
        {
            MoveFieldDown( ExtReservedData.DDRGeometryFields, iSelFieldId );
        }
        else if ( m_sPackageId == sDDRTiming.c_str() )
        {
            MoveFieldDown( ExtReservedData.DDRTimingFields, iSelFieldId );
        }
        else if ( m_sPackageId == sDDRCustom.c_str() )
        {
            MoveFieldDown( ExtReservedData.DDRCustomFields, iSelFieldId );
        }
        else if ( m_sPackageId == sFrequency.c_str() )
        {
            MoveFieldDown( ExtReservedData.FrequencyFields, iSelFieldId );
        }
        else if ( m_sPackageId == sVoltages.c_str() )
        {
            MoveFieldDown( ExtReservedData.VoltagesFields, iSelFieldId );
        }
        else if ( m_sPackageId == sConfigMemoryControl.c_str() )
        {
            MoveFieldDown( ExtReservedData.ConfigMemoryControlFields, iSelFieldId );
        }
        else if ( m_sPackageId == sTrustZoneRegid.c_str() )
        {
            MoveFieldDown( ExtReservedData.TrustZoneRegidFields, iSelFieldId );
        }
        else if ( m_sPackageId == sTrustZone.c_str() )
        {
            MoveFieldDown( ExtReservedData.TrustZoneFields, iSelFieldId );
        }
        else if ( m_sPackageId == sOpDiv.c_str() )
        {
            MoveFieldDown( ExtReservedData.OpDivFields, iSelFieldId );
        }
        else if ( m_sPackageId == sOpMode.c_str() )
        {
            MoveFieldDown( ExtReservedData.OpModeFields, iSelFieldId );
        }
        else if ( m_pErdBase )
        {
            //  cannot move fields in CErdBase objects
        }

        PackageFieldsListCtrl.SetSelectionMark( nItem );
        ExtReservedData.Changed(true);
        UpdateData(FALSE);
    }
}

void CExtReservedDataPkgDlg::OnBnClickedExtReservedDataPkgDeleteFieldBtn()
{
    int nItem = PackageFieldsListCtrl.GetSelectionMark();
    int nId = PackageIdCb.GetCurSel();
    if ( nItem >= 0 && nId != CB_ERR )
    {
        unsigned int iSelFieldId = nItem;

        if ( m_sPackageId == sClockEnable.c_str() )
        {
            DeleteField( ExtReservedData.ClockEnableFields, iSelFieldId );
        }
        else if ( m_sPackageId == sDDRGeometry.c_str() )
        {
            DeleteField( ExtReservedData.DDRGeometryFields, iSelFieldId );
        }
        else if ( m_sPackageId == sDDRTiming.c_str() )
        {
            DeleteField( ExtReservedData.DDRTimingFields, iSelFieldId );
        }
        else if ( m_sPackageId == sDDRCustom.c_str() )
        {
            DeleteField( ExtReservedData.DDRCustomFields, iSelFieldId );
        }
        else if ( m_sPackageId == sFrequency.c_str() )
        {
            DeleteField( ExtReservedData.FrequencyFields, iSelFieldId );
        }
        else if ( m_sPackageId == sVoltages.c_str() )
        {
            DeleteField( ExtReservedData.VoltagesFields, iSelFieldId );
        }
        else if ( m_sPackageId == sConfigMemoryControl.c_str() )
        {
            DeleteField( ExtReservedData.ConfigMemoryControlFields, iSelFieldId );
        }
        else if ( m_sPackageId == sTrustZoneRegid.c_str() )
        {
            DeleteField( ExtReservedData.TrustZoneRegidFields, iSelFieldId );
        }
        else if ( m_sPackageId == sTrustZone.c_str() )
        {
            DeleteField( ExtReservedData.TrustZoneFields, iSelFieldId );
        }
        else if ( m_sPackageId == sOpDiv.c_str() )
        {
            DeleteField( ExtReservedData.OpDivFields, iSelFieldId );
        }
        else if ( m_sPackageId == sOpMode.c_str() )
        {
            DeleteField( ExtReservedData.OpModeFields, iSelFieldId );
        }
        else if ( m_pErdBase && m_pParentErdBase )
        {
            if ( m_pParentErdBase->ErdPkgType() == CErdBase::GPIOSET_ERD )
            {
                CGpio* pGpio = dynamic_cast<CGpio*>(m_pErdBase);
                if ( pGpio )
                {
                    CGpioSet& GpioSet = dynamic_cast<CGpioSet&>(*m_pParentErdBase);
                    t_GpioListIter& iter = GpioSet.GpiosList().begin();
                    while ( iter != GpioSet.GpiosList().end() )
                    {
                        if ( pGpio == *iter )
                        {
                            GpioSet.GpiosList().erase(iter);
                            delete pGpio;
                            break;
                        }
                        iter++;
                    }
                }
            }
            else if ( m_pParentErdBase->ErdPkgType() == CErdBase::TBRXFER_ERD )
            {
                CXfer* pXfer = dynamic_cast<CXfer*>(m_pErdBase);
                if ( pXfer )
                {
                    CTBRXferSet& TbrXferSet = dynamic_cast<CTBRXferSet&>(*m_pParentErdBase);
                    t_XferListIter& iter = TbrXferSet.XfersList().begin();
                    while ( iter != TbrXferSet.XfersList().end() )
                    {
                        if ( pXfer == *iter )
                        {
                            TbrXferSet.XfersList().erase(iter);
                            delete pXfer;
                            break;
                        }
                        iter++;
                    }
                }
            }
            else if ( m_pParentErdBase->ErdPkgType() == CErdBase::USBVENDORREQ_ERD )
            {
                CUsbVendorReq& UsbVendorReq = dynamic_cast<CUsbVendorReq&>(*m_pParentErdBase);
                string* pDataStr = dynamic_cast<string*>(m_pErdBase);
                t_stringVectorIter& strIter = UsbVendorReq.DataList().begin();
                while ( strIter != UsbVendorReq.DataList().end() )
                {
                    if ( pDataStr == (*strIter) )
                    {
                        UsbVendorReq.DataList().erase(strIter);
                        delete pDataStr;
                        break;
                    }						
                    strIter++;
                }
            }
        }
        else if ( !m_pErdBase && m_pParentErdBase )
        {
            CUsbVendorReq& UsbVendorReq = dynamic_cast<CUsbVendorReq&>(*m_pParentErdBase);
            string* pDataStr = 0;
            t_stringVectorIter& strIter = UsbVendorReq.DataList().begin();
            int idx = 0;
            while ( strIter != UsbVendorReq.DataList().end() )
            {
                if ( idx == iSelFieldId )
                {
                    pDataStr = dynamic_cast<string*>(*strIter);
                    UsbVendorReq.DataList().erase(strIter);
                    break;
                }	
                idx++;
                strIter++;
            }
            delete pDataStr;
        }

        PackageFieldsListCtrl.SetSelectionMark( nItem );
        ExtReservedData.Changed(true);
        UpdateData(FALSE);
    }
    RefreshState();
}

void CExtReservedDataPkgDlg::OnCbnSelchangeExtReservedDataPkgIdCb()
{
    int nId = PackageIdCb.GetCurSel();
    if ( nId != CB_ERR )
    {
        PackageIdCb.GetLBText( nId, m_sPackageId );
        int pos = m_sPackageId.Find( " (" );
        if ( pos > -1 )
            m_sPackageId = m_sPackageId.Left( pos );
        m_bEdit = true;
        RefreshState();
    }
}

void CExtReservedDataPkgDlg::UpdateFields( int& nItem, t_PairList& List, t_stringVector& FieldNames )
{
    PackageFieldsListCtrl.DeleteAllItems();

    CTimLib TimLib;
    t_PairListIter iter = List.begin();
    while ( iter != List.end() )
    {
        if ( (*iter)->first >= FieldNames.size() )
        {
            t_PairListIter NextIter = iter;
            NextIter++;
            // remove invalid fields
            delete (*iter);
            List.remove( *iter );
            iter = NextIter;
            continue;
        }
        else
        {
            string& sFN = *FieldNames[(*iter)->first];
            PackageFieldsListCtrl.InsertItem( nItem, CStringA(sFN.c_str()) );
            string sValue = TimLib.HexFormattedAscii((*iter)->second);
            PackageFieldsListCtrl.SetItem( nItem, 1, LVIF_TEXT, CStringA(sValue.c_str()), 0, 0, 0, 0 );
            PackageFieldsListCtrl.SetItemData( nItem, (DWORD_PTR)(*iter) );
            nItem++;
        }
        iter++;
    }
}

void CExtReservedDataPkgDlg::UpdateFields( int& nItem, CErdBase* pErdBase )
{
    PackageFieldsListCtrl.DeleteAllItems();

    CTimLib TimLib;
    t_stringVector& FieldNames = pErdBase->FieldNames();
    t_stringVector& FieldValues = pErdBase->FieldValues();
//	t_stringVector& FieldComments = pErdBase->FieldComments();

    size_t iMax = FieldNames.size();
    // skip DATA field in CUsbVendorReq because it is handled as nested data object
    if ( dynamic_cast<CUsbVendorReq*>(pErdBase) )
        iMax--;

    for ( size_t i = 0; i < iMax; i++ )
    {
        string& sFN = *FieldNames[i];
        PackageFieldsListCtrl.InsertItem( nItem, CStringA(sFN.c_str()) );
        string sValue = TimLib.HexFormattedAscii(TimLib.Translate(*FieldValues[i]));
        PackageFieldsListCtrl.SetItem( nItem, 1, LVIF_TEXT, CStringA(sValue.c_str()), 0, 0, 0, 0 );
        PackageFieldsListCtrl.SetItemData( nItem, i );
        nItem++;
    }
}

void CExtReservedDataPkgDlg::ReplaceFields( t_PairList& DestPkgFields, t_PairList& SourcePkgFields )
{
    t_PairListIter DestIter = DestPkgFields.begin();
    while ( DestIter != DestPkgFields.end() )
    {
        delete *DestIter;
        DestIter++;
    }
    DestPkgFields.clear();

    t_PairListIter SourceIter = SourcePkgFields.begin();
    while ( SourceIter != SourcePkgFields.end() )
    {
        DestPkgFields.push_back( new pair<unsigned int, unsigned int>((*SourceIter)->first, (*SourceIter)->second ) );
        SourceIter++;
    }
}


void CExtReservedDataPkgDlg::GetDefinedPkgFields( t_PairList& PkgFields )
{
    if ( m_sPackageId == sClockEnable.c_str() )
        ReplaceFields(PkgFields, ExtReservedData.ClockEnableFields);
    else if ( m_sPackageId == sDDRGeometry.c_str() )
        ReplaceFields(PkgFields, ExtReservedData.DDRGeometryFields);
    else if ( m_sPackageId == sDDRTiming.c_str() )
        ReplaceFields(PkgFields, ExtReservedData.DDRTimingFields);
    else if ( m_sPackageId == sDDRCustom.c_str() )
        ReplaceFields(PkgFields, ExtReservedData.DDRCustomFields);
    else if ( m_sPackageId == sFrequency.c_str() )
        ReplaceFields(PkgFields, ExtReservedData.FrequencyFields);
    else if ( m_sPackageId == sVoltages.c_str() )
        ReplaceFields(PkgFields, ExtReservedData.VoltagesFields);
    else if ( m_sPackageId == sConfigMemoryControl.c_str() )
        ReplaceFields(PkgFields, ExtReservedData.ConfigMemoryControlFields);
    else if ( m_sPackageId == sTrustZoneRegid.c_str() )
        ReplaceFields(PkgFields, ExtReservedData.TrustZoneRegidFields);
    else if ( m_sPackageId == sTrustZone.c_str() )
        ReplaceFields(PkgFields, ExtReservedData.TrustZoneFields);
    else if ( m_sPackageId == sOpDiv.c_str() )
        ReplaceFields(PkgFields, ExtReservedData.OpDivFields);
    else if ( m_sPackageId == sOpMode.c_str() )
        ReplaceFields(PkgFields, ExtReservedData.OpModeFields);
    else if ( m_pErdBase )
    {
        CTimLib TimLib;
        t_stringVector& FieldValues = m_pErdBase->FieldValues();
        for ( unsigned int i = 0; i < m_pErdBase->MaxFieldNum(); i++ )
        {
            PkgFields.push_back(new pair<unsigned int, unsigned int>( i, TimLib.Translate((*FieldValues[i]).c_str()) ) );
        }
    }
    else if ( m_pParentErdBase )
    {
        CTimLib TimLib;
        if ( m_pParentErdBase->ErdPkgType() == CErdBase::USBVENDORREQ_ERD )
        {
            CUsbVendorReq& UsbVendorReq = dynamic_cast<CUsbVendorReq&>(*m_pParentErdBase);
            int idx = 0;
            t_stringVectorIter& strIter = UsbVendorReq.DataList().begin();
            while ( strIter != UsbVendorReq.DataList().end() )
            {
                PkgFields.push_back(new pair<unsigned int, unsigned int>( idx, TimLib.Translate((*strIter)->c_str()) ) );
                strIter++;
                idx++;
            }
        }
    }
}

void CExtReservedDataPkgDlg::UpdatePkgFields( t_PairList& PkgFields )
{
    if ( m_sPackageId == sClockEnable.c_str() )
        ReplaceFields(ExtReservedData.ClockEnableFields, PkgFields);
    else if ( m_sPackageId == sDDRGeometry.c_str() )
        ReplaceFields(ExtReservedData.DDRGeometryFields, PkgFields);
    else if ( m_sPackageId == sDDRTiming.c_str() )
        ReplaceFields(ExtReservedData.DDRTimingFields, PkgFields);
    else if ( m_sPackageId == sDDRCustom.c_str() )
        ReplaceFields(ExtReservedData.DDRCustomFields, PkgFields);
    else if ( m_sPackageId == sFrequency.c_str() )
        ReplaceFields(ExtReservedData.FrequencyFields, PkgFields);
    else if ( m_sPackageId == sVoltages.c_str() )
        ReplaceFields(ExtReservedData.VoltagesFields, PkgFields);
    else if ( m_sPackageId == sConfigMemoryControl.c_str() )
        ReplaceFields(ExtReservedData.ConfigMemoryControlFields, PkgFields);
    else if ( m_sPackageId == sTrustZoneRegid.c_str() )
        ReplaceFields(ExtReservedData.TrustZoneRegidFields, PkgFields);
    else if ( m_sPackageId == sTrustZone.c_str() )
        ReplaceFields(ExtReservedData.TrustZoneFields, PkgFields);
    else if ( m_sPackageId == sOpDiv.c_str() )
        ReplaceFields(ExtReservedData.OpDivFields, PkgFields);
    else if ( m_sPackageId == sOpMode.c_str() )
        ReplaceFields(ExtReservedData.OpModeFields, PkgFields);
    else if ( m_pErdBase )
    {
        CTimLib TimLib;
        ASSERT( m_pErdBase->MaxFieldNum() == PkgFields.size() );
        t_stringVector& FieldValues = m_pErdBase->FieldValues();
        t_PairListIter iter = PkgFields.begin();
        int idx = 0;
        while ( iter != PkgFields.end() )
        {
            if ( (*iter)->first == idx )
                *FieldValues[idx] = TimLib.HexFormattedAscii((*iter++)->second);
            idx++;
        }
    }
    else if ( m_pParentErdBase )
    {
        CTimLib TimLib;
        if ( m_pParentErdBase->ErdPkgType() == CErdBase::USBVENDORREQ_ERD )
        {
            CUsbVendorReq& UsbVendorReq = dynamic_cast<CUsbVendorReq&>(*m_pParentErdBase);
            t_stringVector& DataList = UsbVendorReq.DataList();
        
            // if a field was added to PkgFields but is not yet in the datalist
            // then add it before assigning a value string
            if ( DataList.size() < PkgFields.size() )
                DataList.push_back(new string("0x00000000"));

            t_PairListIter iter = PkgFields.begin();
            int idx = 0;
            while ( iter != PkgFields.end() )
            {
                *DataList[idx] = TimLib.HexFormattedAscii((*iter++)->second);
                idx++;
            }
        }
    }

    RefreshState();
}

void CExtReservedDataPkgDlg::MoveFieldUp( t_PairList& PkgFields, unsigned int iSelFieldId )
{
    t_PairListIter iter = PkgFields.begin();
    t_PairListIter PrevIter = PkgFields.begin();
    unsigned int idx = 0;
    while ( iter != PkgFields.end() )
    {
        if ( idx == iSelFieldId )
        {
            pair<unsigned int, unsigned int>* pPair = *iter;
            PkgFields.remove( *iter );
            PkgFields.insert( PrevIter, pPair );
            break;
        }
        PrevIter = iter;
        iter++;
        idx++;
    }

    RefreshState();
}

void CExtReservedDataPkgDlg::MoveFieldDown( t_PairList& PkgFields, unsigned int iSelFieldId )
{
    t_PairListIter iter = PkgFields.begin();
    t_PairListIter NextIter = PkgFields.begin();
    unsigned int idx = 0;
    while ( iter != PkgFields.end() )
    {
        NextIter++;
        if ( idx == iSelFieldId )
        {
            pair<unsigned int, unsigned int>* pPair = *iter;
            PkgFields.remove( *iter );
            if ( NextIter != PkgFields.end() )
                NextIter++;
            PkgFields.insert( NextIter, pPair );
            break;
        }
        iter++;
        idx++;
    }

    RefreshState();
}

void CExtReservedDataPkgDlg::DeleteField( t_PairList& PkgFields, unsigned int iSelFieldId  )
{
    t_PairListIter iter = PkgFields.begin();
    unsigned int idx = 0;
    while ( iter != PkgFields.end() )
    {
        if ( idx == iSelFieldId )
        {
            delete (*iter);
            PkgFields.remove( *iter );
            break;
        }
        iter++;
        idx++;
    }

    RefreshState();
}

void CExtReservedDataPkgDlg::UpdateChangeMarkerInTitle()
{
    if ( m_hWnd == 0 )
        return;

    CString sTitle;
    GetWindowTextA( sTitle );
    // add or remove * as appropriate
    if ( ExtReservedData.IsChanged() )
    {
        if ( -1 == sTitle.Find('*') )
            sTitle += "*";
    }
    else
    {
        if ( -1 != sTitle.Find('*') )
            sTitle.Remove('*');
    }
    SetWindowTextA(sTitle);
    UpdateWindow();
}

void CExtReservedDataPkgDlg::OnBnClickedAddExtReservedBtn()
{
    UpdateData( TRUE );

    CErdBase* pErdBase = 0;
    CExtendedReservedData TempExt = ExtReservedData;
    CExtReservedDataPkgDlg Dlg( m_LocalTimDescriptor, CString(""), TempExt, 
                                false, m_sProcessorType, pErdBase, /*parent*/ m_pErdBase);
    {
        if ( Dlg.DoModal() == IDOK )
        {
            if ( pErdBase && m_pErdBase )
            {
                if ( m_pErdBase->ErdPkgType() == CErdBase::GPIOSET_ERD )
                {
                    CGpioSet& GpioSet = dynamic_cast<CGpioSet&>(*m_pErdBase);
                    CGpio& Gpio = dynamic_cast<CGpio&>(*pErdBase);
                    GpioSet.GpiosList().push_back( &Gpio );
                    pErdBase = 0;
                }
                else if ( m_pErdBase->ErdPkgType() == CErdBase::TBRXFER_ERD )
                {
                    CTBRXferSet& TbrXferSet = dynamic_cast<CTBRXferSet&>(*m_pErdBase);
                    CXfer& Xfer = dynamic_cast<CXfer&>(*pErdBase);
                    TbrXferSet.XfersList().push_back( &Xfer );
                    pErdBase = 0;
                }
                // pErdBase should be 0 but if not prevent a leak
                ASSERT( !pErdBase );
                delete pErdBase;
            }
            else if ( m_pErdBase->ErdPkgType() == CErdBase::USBVENDORREQ_ERD )
            {
                CUsbVendorReq& UsbVendorReq = dynamic_cast<CUsbVendorReq&>(*m_pParentErdBase);
                // TBD
                ASSERT(0);
            }
            else
                ExtReservedData = TempExt;
        }

        RefreshState();
    }
    SetFocus();
}

void CExtReservedDataPkgDlg::OnBnClickedEditExtReservedBtn()
{
    UpdateData( TRUE );

    int nNestedItem = NestedPackagesList.GetSelectionMark();
    if ( nNestedItem != LB_ERR )
    {
        CString sNestedPackageId = NestedPackagesList.GetItemText( nNestedItem, 0 );
        CErdBase* pErdBase = (CErdBase*)(NestedPackagesList.GetItemData( nNestedItem ));
        CExtendedReservedData TempExt = ExtReservedData;
        CExtReservedDataPkgDlg Dlg( m_LocalTimDescriptor, sNestedPackageId, TempExt, 
                                    true, m_sProcessorType, pErdBase, /*parent*/ m_pErdBase );
        {
            if ( Dlg.DoModal() == IDOK )
            {
                ExtReservedData = TempExt;
            }
            RefreshState();
        }
    }
    SetFocus();
}

void CExtReservedDataPkgDlg::OnBnClickedDeleteExtPackageBtn()
{
    UpdateData( TRUE );

    int nNestedItem = NestedPackagesList.GetSelectionMark();
    if ( nNestedItem != CB_ERR )
    {
        CErdBase* pErdBase = (CErdBase*)NestedPackagesList.GetItemData( nNestedItem );
        if ( pErdBase )
        {
            if ( ExtReservedData.ErdVec.size() > 0 )
            {
                t_ErdBaseVectorIter ErdIter = ExtReservedData.ErdVec.begin();
                bool bFound = false;
                while( !bFound & ErdIter != ExtReservedData.ErdVec.end() )
                {
                    switch ( (*ErdIter)->ErdPkgType() )
                    {
                        case CErdBase::GPIOSET_ERD:
                        {
                            if ( pErdBase->ErdPkgType() == CErdBase::GPIO_ERD )
                            {
                                CGpioSet& GpioSet = dynamic_cast<CGpioSet&>(*m_pErdBase);
                                t_GpioListIter& NestedErdIter = GpioSet.GpiosList().begin();
                                while ( NestedErdIter != GpioSet.GpiosList().end() )
                                {
                                    if ( pErdBase == *NestedErdIter )
                                    {
                                        GpioSet.GpiosList().erase(NestedErdIter);
                                        bFound = true;
                                        break;
                                    }
                                    NestedErdIter++;
                                }
                            }
                            break;
                        }
                        
                        case CErdBase::TBRXFER_ERD:
                        {
                            if ( pErdBase->ErdPkgType() == CErdBase::XFER_ERD )
                            {
                                CTBRXferSet& TbrXferSet = dynamic_cast<CTBRXferSet&>(*m_pErdBase);
                                t_XferListIter& NestedErdIter = TbrXferSet.XfersList().begin();
                                while ( NestedErdIter != TbrXferSet.XfersList().end() )
                                {
                                    if ( pErdBase == *NestedErdIter )
                                    {
                                        TbrXferSet.XfersList().erase(NestedErdIter);
                                        bFound = true;
                                        break;
                                    }
                                    NestedErdIter++;
                                }
                            }
                            break;
                        }

                        case CErdBase::USBVENDORREQ_ERD:
                        {
                            string* pStr = dynamic_cast<string*>(pErdBase);
                            if ( pStr )
                            {
                                CUsbVendorReq& UsbVendorReq = dynamic_cast<CUsbVendorReq&>(*m_pErdBase);
                                t_stringVectorIter& strIter = UsbVendorReq.DataList().begin();
                                while ( strIter != UsbVendorReq.DataList().end() )
                                {
                                    if ( pStr == *strIter )
                                    {
                                        UsbVendorReq.DataList().erase(strIter);
                                        bFound = true;
                                        break;
                                    }
                                    strIter++;
                                }
                            }
                            break;
                        }
                    }
                    ErdIter++;
                }
                if ( bFound )
                    delete pErdBase;
            }
        }

        RefreshState();
    }
}

void CExtReservedDataPkgDlg::OnContextMenu(CWnd* pWnd, CPoint /*point*/)
{
    CCommentDlg Dlg(this);
    int nItem = -1;

    int iCtlId = pWnd->GetDlgCtrlID();
    switch ( iCtlId )
    {
        case IDC_EXT_RESERVED_DATA_PKG_ID_CB:
//			PackageIdCb
//			if ( pData )
//				Dlg.Display( pData );
            break;

        case IDC_EXT_RESERVED_DATA_PKG_FIELDS_LIST:
            nItem = PackageFieldsListCtrl.GetSelectionMark();
            if ( nItem != -1 )
            {
                void* pData = (void*)PackageFieldsListCtrl.GetItemData( nItem );
                if ( pData )
                    Dlg.Display( pData );
            }
            break;

        case IDC_EXT_RESERVED_DATA_NESTED_PKG_LIST:
            nItem = NestedPackagesList.GetSelectionMark();
            if ( nItem != -1 )
            {
                void* pData = (void*)NestedPackagesList.GetItemData( nItem );
                if ( pData )
                    Dlg.Display( pData );
            }
            break;

        default:
            break;
    }
}
