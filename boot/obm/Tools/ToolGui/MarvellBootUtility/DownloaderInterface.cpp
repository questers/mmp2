/******************************************************************************
 *
 *  (C)Copyright 2005 - 2011 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#include "StdAfx.h"
#include "DownloaderInterface.h"
#include "ImageDescription.h"

#if TOOLS_GUI == 1
#include "MarvellBootUtility.h"
#endif

CDownloaderInterface::CDownloaderInterface(eDownloadType Type)
: nDownloadType(Type), CTimLib()
{	
    sWtptpExePath = "";
    
    Reset();
    m_bChanged = false;

    // try to find (NT)WtpDownload by default if it is in
    // the same directory as MarvellBootUtility.exe
    ifstream ifs;
    char szDir[MAX_PATH]={0};
    GetCurrentDirectory(MAX_PATH, szDir);
    ifs.open( "WtpDownload.exe", ios_base::in | ios_base::binary );
    if ( ifs.is_open() )
    {
        sWtptpExePath = szDir;
        sWtptpExePath += "\\WtpDownload.exe";
        ifs.close();
    }
    else
    {
        ifs.open( "NTWtpDownload.exe", ios_base::in | ios_base::binary );
        if ( ifs.is_open() )
        {
            sWtptpExePath = szDir;
            sWtptpExePath += "\\NTWtpDownload.exe";
            ifs.close();
        }
    }
}

CDownloaderInterface::~CDownloaderInterface(void)
{
    Reset();
}

void CDownloaderInterface::DiscardAll()
{
    // delete all ImageBinList strings
    t_stringListIter iterImage = m_ImageBinFiles.begin();
    while( iterImage != m_ImageBinFiles.end() )
    {
        delete *iterImage;
        iterImage++;
    }
    m_ImageBinFiles.clear();
}

void CDownloaderInterface::Reset()
{
    DiscardAll();

//	sWtptpExePath = "";
    sTimFilePath = "";
    sFbfFilePath = "";
    sMsgMode = "Message and Download";
    sPortType = "USB";
    sComPort = "1";
    sBaudRate = "115200";
    
    sJtagKeyFilePath = "";
    bPartitionBinary = false;
    sPartitionBinaryFilePath = "";

    bVerboseMode = false;
    bRunDebugBootCommand = false;
    nFlashType = DEFAULT;
    sUartDelayMs = "0";
    sUsbPacketSize = "4088";
    sCommandLineArgs = "";
    sAdditionalOptions = "";
    bAdditionalOptions = false;
    bProcessorType = false;
    bUploadSpecFile = false;
//	nDownloadType = NO_TYPE; set in ctor so do not reset
    bIncludeInDownload = false;

    m_bChanged = false;
    ImageBuild.Reset();
    FBFmakecfg.Reset();
//	TimDescriptorParser.Reset();
}

#if TOOLS_GUI == 1
bool CDownloaderInterface::SaveState( ofstream& ofs )
{
    TimDescriptorParser.TimDescriptor().WriteTimDescriptorFile();

    stringstream ss;

    ss << sTimFilePath << endl;
    ss << sFbfFilePath << endl;
    ss << nDownloadType << endl;
    ss << sMsgMode << endl;
    ss << sPortType << endl;
    ss << sComPort << endl;
    ss << sBaudRate << endl;
    ss << m_ImageBinFiles.size() << endl;

    t_stringListIter iterImage = m_ImageBinFiles.begin();
    while( iterImage != m_ImageBinFiles.end() )
    {
        ss << *(*iterImage) << endl;
        iterImage++;
    }

    ss << sJtagKeyFilePath << endl;
    ss << sWtptpExePath << endl;
    ss << boolalpha << bPartitionBinary << endl;
    ss << sPartitionBinaryFilePath << endl;
    ss << boolalpha << bVerboseMode << endl;
    ss << boolalpha << bRunDebugBootCommand << endl;
    ss << nFlashType << endl;
    ss << sUartDelayMs << endl;
    ss << sUsbPacketSize << endl;

    ss << boolalpha << bAdditionalOptions << endl;
    ss << sAdditionalOptions << endl;

    ss << boolalpha << bIncludeInDownload << endl;
    ss << boolalpha << bProcessorType << endl;

    ss << boolalpha << bUploadSpecFile << endl;
    ss << sUploadSpecFilePath << endl;

    ofs << ss.str();
    ofs.flush();

    if ( !(ofs.good() && !ofs.fail() && ( ImageBuild.SaveState( ofs ) )) ) 
    {
        AfxMessageBox("Failed to save (N)TIM Image Build Config.");
        return false;
    }

    if ( !(ofs.good() && !ofs.fail() && ( TimDescriptorParser.TimDescriptor().SaveState( ofs ) )) )
    {
        AfxMessageBox("Failed to save Tim Descriptor Config.");
        return false;
    }

    if ( !(ofs.good() && !ofs.fail() && ( FBFmakecfg.SaveState( ofs ) )) ) 
    {
        AfxMessageBox("Failed to save FBF Make Config.");
        return false;
    }

    return ofs.good(); // SUCCESS
}


bool CDownloaderInterface::LoadState( ifstream& ifs )
{
    Reset();

    string sbuf;

    t_TimDescriptorLines& g_Lines = TimDescriptorParser.TimDescriptor().g_TimDescriptorLines;
    TimDescriptorParser.TimDescriptor().DiscardTimDescriptorLines();

    ::getline( ifs, sTimFilePath );
    ::getline( ifs, sFbfFilePath );
    ::getline( ifs, sbuf ); nDownloadType = (eDownloadType)Translate(sbuf);
    ::getline( ifs, sMsgMode );
    ::getline( ifs, sPortType );
    ::getline( ifs, sComPort );
    ::getline( ifs, sBaudRate );

    ::getline( ifs, sbuf );
    int ImagesListSize = Translate(sbuf);
    while ( ImagesListSize > 0 )
    {
        ::getline( ifs, sbuf );
        m_ImageBinFiles.push_back( new string(sbuf) );
        ImagesListSize--;
    }

    ::getline( ifs, sJtagKeyFilePath );
    ::getline( ifs, sWtptpExePath );
    ::getline( ifs, sbuf ); bPartitionBinary = ( sbuf == "true" ? true : false );
    ::getline( ifs, sPartitionBinaryFilePath );
    ::getline( ifs, sbuf ); bVerboseMode = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); bRunDebugBootCommand = ( sbuf == "true" ? true : false );
    ::getline( ifs, sbuf ); nFlashType = (eFlashType)Translate(sbuf);
    ::getline( ifs, sUartDelayMs );
    
    if ( theApp.ProjectVersion >= 0x03021501 )
        ::getline( ifs, sUsbPacketSize );

    ::getline( ifs, sbuf ); bAdditionalOptions = ( sbuf == "true" ? true : false );
    ::getline( ifs, sAdditionalOptions );

    ::getline( ifs, sbuf ); bIncludeInDownload = ( sbuf == "true" ? true : false );

    if ( theApp.ProjectVersion >= 0x03021600 && theApp.ProjectDate >= 0x20100105 )
    {
        ::getline( ifs, sbuf ); bProcessorType = ( sbuf == "true" ? true : false );
    }

    if ( theApp.ProjectVersion >= 0x03030120 )
    {
        ::getline( ifs, sbuf ); bUploadSpecFile = ( sbuf == "true" ? true : false );
        ::getline( ifs, sUploadSpecFilePath );
    }

    if ( !(ifs.good() && !ifs.fail() && ( ImageBuild.LoadState( ifs ) )) ) 
    {
        AfxMessageBox("Failed to Load (N)TIM Image Build Config.");
        return false;
    }

    if ( !( ifs.good() && !ifs.fail() && ( TimDescriptorParser.TimDescriptor().LoadState( ifs ) )) )
    {
        AfxMessageBox("Failed to load TIM Descriptor config.  Try again.");
        return false;
    }

    if ( nDownloadType != FBF_BASED_FILE )
        theApp.CommandLineParser.ProcessorType(TimDescriptorParser.TimDescriptor().ProcessorType());

    if ( theApp.ProjectVersion >= 0x03030110 )
    {
        if ( !( ifs.good() && !ifs.fail() && ( FBFmakecfg.LoadState( ifs ) )) )
        {
            AfxMessageBox("Failed to load FBF Make Config.  Try again.");
            return false;
        }
    }

    return ifs.good() && !ifs.fail(); // success
}
#endif

bool CDownloaderInterface::IsChanged()
{ 
    return ( TimDescriptorParser.TimDescriptor().IsChanged() || ImageBuild.IsChanged() || m_bChanged); 
}

void CDownloaderInterface::Changed( bool bSet )
{ 
    TimDescriptorParser.TimDescriptor().Changed(bSet);
    ImageBuild.Changed(bSet); 
    m_bChanged = bSet; 
}

string& CDownloaderInterface::CommandLineArgs()
{
#define WTPTP_3_2_19 1
#if WTPTP_3_2_19 // previous version commandline switches now obsolete
#define TIM_BIN_FILE	't'
#define COMM_PORT		'C'
#define DEBUG_BOOT		'D'
#define FBF_FILE		'F'
#define INTERFACE_NAME	'i'
#define HELP			'h'
#define IMAGE_FILE		'f'
#define JTAG_KEY		'k'
#define LOG_FILE		'L'
#define MESSAGE_MODE	'M'
#define DISABLE_FAST_DOWNLOAD	'N'
#define OTP_FLASH_TYPE	'O'
#define PORT_TYPE		'P'
#define PARTITION_TABLE	'p'
#define BAUD_RATE		'B'
#define UART_DELAY		'S'
#define PLATFORM_TYPE	'T'
#define USB_PACKET_SIZE 'U'
#define VERBOSE			'V'
#define UPLOAD_DATA     'W'
#define SCRIPT_FILE		'x'

#else

// new commandline switches (you can use upper or lower case)
#define TIM_BIN_FILE	'B'
#define COMM_PORT		'C'
#define DEBUG_BOOT		'D'
#define FBF_FILE		'F'
#define INTERFACE_NAME	'G'
#define HELP			'H'
#define IMAGE_FILE		'I'
#define JTAG_KEY		'J'
#define LOG_FILE		'L'
#define MESSAGE_MODE	'M'
#define DISABLE_FAST_DOWNLOAD	'N'
#define OTP_FLASH_TYPE	'O'
#define PORT_TYPE		'P'
#define PARTITION_TABLE	'Q'
#define BAUD_RATE		'R'
#define UART_DELAY		'S'
#define PLATFORM_TYPE	'T'
#define USB_PACKET_SIZE 'U'
#define VERBOSE	        'V'
#define UPLOAD_DATA     'W'
#define SCRIPT_FILE		'X'
#endif

    sCommandLineArgs = "";
    stringstream ss;
                
    // add verbose switch
    if ( VerboseMode() ) ss << "-" << char(VERBOSE) << " ";

    // add RunDebugBootCmd switch
    if ( RunDebugBootCommand() ) ss << "-" << char(DEBUG_BOOT) << " ";

    if ( bProcessorType )
    {
        if ( theApp.CommandLineParser.iProcessorType > PXA_NONE && theApp.CommandLineParser.iProcessorType < PXAMAX_PT )
        {
            // add processor type
            ss << "-" << char(PLATFORM_TYPE) << " ";
            ss << theApp.CommandLineParser.iProcessorType << " ";
        }
    }

    if ( MsgMode() == "None" )
        ss << "-" << char(MESSAGE_MODE) << " " << "1 "; 
    else if ( MsgMode() == "Message Only" )
        ss << "-" << char(MESSAGE_MODE) << " " << "2 "; 
    else ss << "-" << char(MESSAGE_MODE) << " " << "3 "; 


    // add port type
    if( PortType() != "None" )
    {
        ss << "-" << char(PORT_TYPE) << " " << PortType() << " "; 
        if ( PortType() == "UART" )
        {
            if ( ComPort() != "None" )
            {
                ss << "-" << char(COMM_PORT) << " " << ComPort() << " ";
            }
            ss << "-" << char(BAUD_RATE) << " " << BaudRate() << " ";
  
            // add UART Delay
            if ( UartDelayMs() != "" )
            {
                ss << "-" << char(UART_DELAY) << " " << UartDelayMs() << " ";
            }
        }
        else if ( PortType() == "USB" )
        {
            // add UART Delay
            if ( UsbPacketSize() != "" )
            {
                ss << "-" << char(USB_PACKET_SIZE) << " " << UsbPacketSize() << " ";
            }
        }
    }

    if ( MsgMode() != "Message Only" )
    {
        // add image file(s)
        if ( nDownloadType == TIM_BASED_DKB || nDownloadType == TIM_BASED_OBM )
        {
            // add TIM bin file
            if ( TimFilePath().length() > 0 )
            {
                ss << "-" << char(TIM_BIN_FILE) << " ";
                if ( TimFilePath().length() > 0 )
                    ss << CTimLib::QuotedFilePath( TimFilePath() ) << " "; 
                else 
                    ss << "<input TIMFilePath> "; 
            }

            t_stringListIter iterImage = m_ImageBinFiles.begin();
            while( iterImage != m_ImageBinFiles.end() )
            {
                ss << "-" << char(IMAGE_FILE) << " ";
                if ( (*(*iterImage)).length() > 0 )
                    ss << CTimLib::QuotedFilePath( *(*iterImage) ) << " "; 
                else
                    ss << "<input ImageFilePath> ";
                iterImage++;
            }
        }
        else if ( nDownloadType == FBF_BASED_FILE )// add FBF image file
        {
            ss << "-" << char(FBF_FILE) << " ";
            if ( sFbfFilePath.length() > 0 )
                ss << CTimLib::QuotedFilePath( sFbfFilePath ) << " "; 
            else 
                ss << "<input FBFFilePath> ";
        }

        // add Jtag key file
        if ( JtagKeyFilePath().length() > 0 )
        {
            ss << "-" << char(JTAG_KEY) << " ";
            if ( JtagKeyFilePath().length() > 0 )
                ss << CTimLib::QuotedFilePath( JtagKeyFilePath() ) << " "; 
            else 
                ss << "<input JTAGKeyFilePath> ";
        }
    }

    // -O is not compatible with -k or -t or -F
    // add flash type switch and selection
    if ( ss.str().find( char(JTAG_KEY) ) == string::npos // not found
         && ss.str().find( char(TIM_BIN_FILE) ) == string::npos
         && ss.str().find( char(FBF_FILE) ) == string::npos )
    {
        //ok to add -O switch
        ss << "-" << char(OTP_FLASH_TYPE) << " " << FlashType() << " ";
    }

    if ( PartitionBinary() && PartitionBinaryFilePath().length() > 0 )
    {
        ss << "-" << char(PARTITION_TABLE) << " ";
        if ( PartitionBinaryFilePath().length() > 0 )
            ss << CTimLib::QuotedFilePath( PartitionBinaryFilePath() ) << " "; 
        else 
            ss << "<input ParitionTableFilePath> ";
    }

    if ( UploadSpecFileEnable() && UploadSpecFilePath().length() > 0 )
    {
        ss << "-" << char(UPLOAD_DATA) << " ";
        if ( UploadSpecFilePath().length() > 0 )
            ss << CTimLib::QuotedFilePath( UploadSpecFilePath() ) << " "; 
        else 
            ss << "<input UploadSpecFilePath> ";
    }

    if ( bAdditionalOptions && AdditionalOptions().length() > 0 )
    {
        ss << AdditionalOptions() << " ";
    }

    sCommandLineArgs = ss.str();

    return sCommandLineArgs;
}

unsigned int CDownloaderInterface::AddBinImage( string& sBinImageFilePath )
{
    m_ImageBinFiles.push_back( new string( sBinImageFilePath ));
    m_bChanged = true;
    return (unsigned int)m_ImageBinFiles.size();
}

unsigned int CDownloaderInterface::DeleteBinImage( string& sBinImageFilePath )
{
    t_stringListIter iterImage = m_ImageBinFiles.begin();
    // only the first image file is used for FBF
    while( iterImage != m_ImageBinFiles.end() )
    {
        string sPath = *(*iterImage);
        if ( sPath == sBinImageFilePath )
        {
            m_ImageBinFiles.remove( *iterImage );
            delete *iterImage;
            break;
        }
        iterImage++;
    }
    m_bChanged = true;
    return (unsigned int)m_ImageBinFiles.size();
}

#if TOOLS_GUI == 1
unsigned int CDownloaderInterface::RefreshImageList( t_ImagesList& NewImageList )
{
    DiscardAll();
    string sImageBinPath;
    t_ImagesIter iterImage = NewImageList.begin();
    // only the first image file is used for FBF
    while( iterImage != NewImageList.end() )
    {
        if ( (*iterImage)->ImageId().compare(0,3,"TIM") == 0 )
        {
            CreateOutputTimBinImageName((*iterImage)->ImageFilePath(), sImageBinPath);
            TimFilePath( sImageBinPath );
        }
        else if ( (*iterImage)->ImageId().compare(0,3,"TIM") != 0 )
        {
            CreateOutputImageName((*iterImage)->ImageFilePath(), sImageBinPath);			
            AddBinImage( sImageBinPath );
        }
        iterImage++;
    }

    m_bChanged = true;
    return (unsigned int)m_ImageBinFiles.size();
}
#endif