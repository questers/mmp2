/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

// CItemEdit
class CHexItemEdit : public CEdit
{
	DECLARE_DYNAMIC(CHexItemEdit)

public:
	CHexItemEdit( int iDigits = 8 );
	virtual ~CHexItemEdit();

protected:
	DECLARE_MESSAGE_MAP()

public:
	int m_x;
	int m_y;
	int m_digits;
	bool m_bHexOnly;

	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnEnUpdate();
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
};

class CHexItemEdit16 : public CHexItemEdit
{
public:
	CHexItemEdit16();
	virtual ~CHexItemEdit16();
};

#define MAX_COLUMNS 20
enum eEditControlType { ReadOnly, Edit, HexEdit, ComboBox };

// CEditListCtrl
class CEditListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC(CEditListCtrl)

public:
	CEditListCtrl();
	virtual ~CEditListCtrl();

protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnLvnBeginlabeledit(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnEndlabeledit(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);

	eEditControlType EditControlType[MAX_COLUMNS];  //  assumes 20 columns max

public:
	void InsertAfterSelectedItem();
	void DeleteSelectedItem();

	bool IsChanged(){ return m_bChanged; }
	void Changed( bool bSet ){ m_bChanged = bSet; }

private:
	int m_item;
	int m_subitem;
	CHexItemEdit  m_ItemEditCtrl;
	bool m_bChanged;
};


