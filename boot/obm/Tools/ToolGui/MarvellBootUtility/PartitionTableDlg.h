/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "EditListCtrl.h"
#include "afxcmn.h"
#include "afxwin.h"


// CPartitionTableDlg dialog
class CDownloaderInterface;

class CPartitionTableDlg : public CDialog
{
	DECLARE_DYNAMIC(CPartitionTableDlg)

public:
	CPartitionTableDlg(CDownloaderInterface& rDownloaderInterface, CWnd* pParent = NULL);   // standard constructor
	virtual ~CPartitionTableDlg();

// Dialog Data
	enum { IDD = IDD_PARTITION_TABLE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

public:
	CHexItemEdit PartitionTableVersion;
	CListCtrl PartitionsListCtrl;
	CButton MoveUpBtn;
	CButton MoveDownBtn;
	CButton DeletePartitionBtn;

	CString sPartitionTextFilePath;
	CString sPartitionVersion;

	afx_msg void OnBnClickedInsertPartitionBtn();
	afx_msg void OnBnClickedPartitionMoveUpBtn();
	afx_msg void OnBnClickedPartitionMoveDownBtn();
	afx_msg void OnBnClickedDeletePartitionBtn();
	afx_msg void OnEnChangePartitionVersionEdt();
	afx_msg void OnEnChangePartitionTextFilePathEdt();
	afx_msg void OnBnClickedPartitionBrowseBtn();
	afx_msg void OnBnClickedPartitionReadBtn();
	afx_msg void OnBnClickedPartitionWriteBtn();
	afx_msg void OnBnClickedEditPartitionBtn();
	afx_msg void OnBnClickedOk();
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);

public:
	CPartitionTable& PartitionTable() { return m_PartitionTable; }
	void RefreshState();
	void UpdateChangeMarkerInTitle();

private:
	CPartitionTable			m_PartitionTable; // used for local edit
	CDownloaderInterface&	m_DownloaderInterface;
};
