/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell International Ltd. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#include "StdAfx.h"
#include "ProcessLauncher.h"
#include "MarvellBootUtility.h"

#include <windows.h>
#include <stdio.h> 

#define BUFSIZE 4096 

CProcessLauncher::CProcessLauncher(void)
{
    hChildStdinRd = 0;
    hChildStdinWr = 0;
    hChildStdoutRd = 0;
    hChildStdoutWr = 0;
//	hInputFile = 0;
//	hStdout = 0;

    memset( &piProcInfo, 0, sizeof(PROCESS_INFORMATION) );
    memset( &siStartInfo, 0, sizeof(STARTUPINFO) );
}

CProcessLauncher::~CProcessLauncher(void)
{
}

bool CProcessLauncher::Launch( string& sExePath, string& sCommandLineArgs )
{
// NOTE: this code is exerpted from MS VS 2005 Documentation
//  from article "Creating a Child Process with Redirected Input and Output"
// ms-help://MS.VSCC.v80/MS.MSDN.vAug06.en/dllproc/base/creating_a_child_process_with_redirected_input_and_output.htm
//  Code has been modified and added as member functions to this class.

    SECURITY_ATTRIBUTES saAttr; 
    bool fSuccess = true; 
 
// Set the bInheritHandle flag so pipe handles are inherited. 
    saAttr.nLength = sizeof(SECURITY_ATTRIBUTES); 
    saAttr.bInheritHandle = TRUE; 
    saAttr.lpSecurityDescriptor = NULL; 

// Get the handle to the current STDOUT. 
//   hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
 
//   if ( hStdout == INVALID_HANDLE_VALUE )
//	   return -2;

    // Create a pipe for the child process's STDOUT. 
    if (! CreatePipe(&hChildStdoutRd, &hChildStdoutWr, &saAttr, 0)) 
        AfxMessageBox("Stdout pipe creation failed\n"); 

    // Ensure the read handle to the pipe for STDOUT is not inherited.
    SetHandleInformation( hChildStdoutRd, HANDLE_FLAG_INHERIT, 0);

    // Create a pipe for the child process's STDIN. 
    if (! CreatePipe(&hChildStdinRd, &hChildStdinWr, &saAttr, 0)) 
        AfxMessageBox("Stdin pipe creation failed\n"); 

    // Ensure the write handle to the pipe for STDIN is not inherited. 
    SetHandleInformation( hChildStdinWr, HANDLE_FLAG_INHERIT, 0);
 
    // Now create the child process. 
    fSuccess = CreateExeProcess( sExePath, sCommandLineArgs );
    if (fSuccess) 
    {
#if 0
    // Get a handle to the parent's input file. 
       if (argc == 1) 
          AfxMessageBox("Please specify an input file"); 

       printf( "Debug: argv[1] = %s\n", argv[1]);
        string sLogFile = "TestLogFile.txt";

       hInputFile = CreateFile(argv[1], GENERIC_READ, 0, NULL, 
          OPEN_EXISTING, FILE_ATTRIBUTE_READONLY, NULL); 

       if (hInputFile == INVALID_HANDLE_VALUE) 
          AfxMessageBox("CreateFile failed"); 

    // Write to pipe that is the standard input for a child process. 
       WriteToPipe(); 
     
#endif

        // Read from pipe that is the standard output for child process. 
        ReadFromPipe(); 
    }

    if ( hChildStdoutRd )
    {
        if (!CloseHandle(hChildStdoutRd)) 
            AfxMessageBox("Closing handle hChildStdoutRd failed"); 
        hChildStdoutRd = 0;
    }

    if ( hChildStdoutWr )
    {
        if (!CloseHandle(hChildStdoutWr)) 
            AfxMessageBox("Closing handle hChildStdoutWr failed"); 

        hChildStdoutWr = 0;
    }

    return fSuccess; 
} 
 
bool CProcessLauncher::CreateExeProcess( string& sExePath, string& sCommandLineArgs ) 
{ 
    if ( sExePath.length() == 0 )
    {
        AfxMessageBox( "Exe Path not provided to Process Launcher! Cannot execute process." );
        return false;
    }
// NOTE: this code is exerpted from MS VS 2005 Documentation
//  from article "Creating a Child Process with Redirected Input and Output"
// ms-help://MS.VSCC.v80/MS.MSDN.vAug06.en/dllproc/base/creating_a_child_process_with_redirected_input_and_output.htm
//  The MS Code has been modified and added as member functions to this class.
    string sCommandLine = sExePath;
    sCommandLine += " ";
    sCommandLine += sCommandLineArgs;
    sCommandLine += "\n";

    theApp.DisplayMsg( CString("\nLaunch Tool with commandline:\n") );
    theApp.DisplayMsg( CString(sCommandLine.c_str()) );
    theApp.DisplayMsg(CString("\n"));

    char path_buffer[_MAX_PATH]={0};
    //	char drive[_MAX_DRIVE]={0};
    char dir[_MAX_DIR]={0};
    //	char fname[_MAX_FNAME]={0};
    //	char ext[_MAX_EXT]={0};
    errno_t err;

    strcpy_s( path_buffer, MAX_PATH, sExePath.c_str() );
    //	err = _splitpath_s( path_buffer, drive, _MAX_DRIVE, dir, _MAX_DIR, fname,
    //                     _MAX_FNAME, ext, _MAX_EXT );
    err = _splitpath_s( path_buffer, 0, 0, dir, _MAX_DIR, 0, 0, 0, 0 );
    if (err != 0)
    {
      printf("Error splitting the path. Error code %d.\n", err);
      return false;
    }
    BOOL bRet = SetCurrentDirectory( dir );

    bool bFuncRetn = false; 

    // Set up members of the PROCESS_INFORMATION structure. 
    ZeroMemory( &piProcInfo, sizeof(PROCESS_INFORMATION) );

    // Set up members of the STARTUPINFO structure. 
    ZeroMemory( &siStartInfo, sizeof(STARTUPINFO) );
    siStartInfo.cb = sizeof(STARTUPINFO); 
    siStartInfo.hStdError = hChildStdoutWr;
    siStartInfo.hStdOutput = hChildStdoutWr;
    siStartInfo.hStdInput = hChildStdinRd;
    siStartInfo.dwFlags |= (STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW);
    siStartInfo.wShowWindow = SW_HIDE;

    // Create the child process.    
    bFuncRetn = CreateProcess( NULL, 
                              (LPSTR)sCommandLine.c_str(),     // command line 
                              NULL,          // process security attributes 
                              NULL,          // primary thread security attributes 
                              TRUE,          // handles are inherited 
                              0,             // creation flags 
                              NULL,          // use parent's environment 
                              NULL,          // use parent's current directory 
                              &siStartInfo,  // STARTUPINFO pointer 
                              &piProcInfo)
                              == TRUE ? true : false;  // receives PROCESS_INFORMATION 

    if (bFuncRetn == false) 
    {
        AfxMessageBox("CreateProcess failed\n");
        CloseHandle(piProcInfo.hProcess);
        piProcInfo.hProcess = 0;
        CloseHandle(piProcInfo.hThread);
        piProcInfo.hThread = 0;
    }

    return bFuncRetn;
}

#if 0
VOID CProcessLauncher::WriteToPipe(VOID) 
{ 
   DWORD dwRead = 0, dwWritten = 0; 
   CHAR chBuf[BUFSIZE]={0}; 
 
// Read from a file and write its contents to a pipe. 
 
   for (;;) 
   { 
      if (! ReadFile(hInputFile, chBuf, BUFSIZE, &dwRead, NULL) || 
         dwRead == 0) break; 
      if (! WriteFile(hChildStdinWr, chBuf, dwRead, 
         &dwWritten, NULL)) break; 
   } 
 
// Close the pipe handle so the child process stops reading. 
 
   if (! CloseHandle(hChildStdinWr)) 
      AfxMessageBox("Close pipe failed\n"); 
} 
#endif
 
bool CProcessLauncher::ReadFromPipe(VOID) 
{ 
    // Close the write end of the pipe before reading from the 
    // read end of the pipe. 
    if ( hChildStdoutWr )
    {
        if (!CloseHandle(hChildStdoutWr)) 
            AfxMessageBox("Closing handle hChildStdoutWr failed"); 
        hChildStdoutWr = 0;
    }

    CHAR chBuf[BUFSIZE] = {0}; 
    DWORD dwRead = 0;
    DWORD ExitCode = 0;
    
    GetExitCodeProcess( piProcInfo.hProcess, &ExitCode );

    // give exe process up to 2 seconds to start up if system is very busy
    int iWait = 2000;
    while ( iWait > 0 && piProcInfo.hProcess != 0 )
    {
        Sleep(10);
        iWait -= 10;
        // determine if downloader process is now active
        if ( GetExitCodeProcess( piProcInfo.hProcess, &ExitCode ) )
        {
            if ( ExitCode == STILL_ACTIVE )
                break;
        }
    }

    if ( ExitCode != STILL_ACTIVE )
    {
        string msg("Tool exe failed to execute.  Check installation of builder.\n\n");
        AfxMessageBox(msg.c_str()); 
        theApp.DisplayMsg( CStringA(msg.c_str()) );
        return false;
    }

    // Read output from the child process, and write to parent's STDOUT. 
    while( piProcInfo.hProcess != 0 && ExitCode == STILL_ACTIVE )
    {
        // determine if builder process is still active or has exited
        if ( GetExitCodeProcess( piProcInfo.hProcess, &ExitCode ) )
        {
            if ( ExitCode == STILL_ACTIVE )
            {
                if ( ReadFile(hChildStdoutRd, chBuf, BUFSIZE, &dwRead, NULL) && dwRead != 0 )
                {
                    if ( dwRead != 0 )
                    {
                        theApp.DisplayMsg( CStringA(chBuf) );
                        memset(chBuf,0,dwRead);
                        dwRead = 0;
                    }
                }
            }
        }

        if ( ExitCode != STILL_ACTIVE && ExitCode != 1 )
        {
            stringstream ssMsg;
            ssMsg << endl << "Builder exited with error. Exit code: ";
            ssMsg << ExitCode << endl;
            AfxMessageBox(ssMsg.str().c_str()); 
            theApp.DisplayMsg( CStringA(ssMsg.str().c_str()) );
            return false;
        }
    }

    return true;	
} 
