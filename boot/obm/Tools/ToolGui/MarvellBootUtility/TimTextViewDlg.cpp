/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// TimTextViewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "TimTextViewDlg.h"

#include "XdbDdrInitScriptParser.h"

// CTimTextViewDlg dialog

IMPLEMENT_DYNAMIC(CTimTextViewDlg, CDialog)

CTimTextViewDlg::CTimTextViewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTimTextViewDlg::IDD, pParent)
{
}

CTimTextViewDlg::~CTimTextViewDlg()
{
	t_stringListIter iter = Lines.begin();
	while( iter != Lines.end() )
		delete (*iter++);
	Lines.clear();
}

void CTimTextViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TIM_CONTENTS_EDT, FileContentsEdt);
}


BEGIN_MESSAGE_MAP(CTimTextViewDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CTimTextViewDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CTimTextViewDlg message handlers

void CTimTextViewDlg::SetViewText( string& sText )
{
	sViewText = sText;

	for ( unsigned int i = 0; i < sViewText.length(); i++ )
	{
		if ( sViewText[i] == '\n' )
		{
			sViewText = sViewText.substr(0,i) + '\r' + sViewText.substr(i);
			i++;
		}
	}
}

BOOL CTimTextViewDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	LOGFONT lf;
	memset(&lf, 0, sizeof(LOGFONT));       // zero out structure
	lf.lfHeight = 16;                      // request a 16-pixel-height font
	strcpy_s(lf.lfFaceName, 32, "Courier New");  // request a face name "Arial"
	VERIFY(font.CreateFontIndirect(&lf));  // create the font
	
	// Do something with the font just created...
	FileContentsEdt.SetFont(&font);
	FileContentsEdt.SetWindowTextA( sViewText.c_str() );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTimTextViewDlg::OnBnClickedOk()
{
	CString sText;
	string sLine;
	FileContentsEdt.GetWindowTextA( sText );

	while ( sText.GetLength() > 0 )
	{
		int pos = sText.Find("\r\n");
		sLine = sText.Left(pos);
		Lines.push_back( new string( sLine ) );
		sText = sText.Right(sText.GetLength()-pos-2);
	}
	OnOK();
}
