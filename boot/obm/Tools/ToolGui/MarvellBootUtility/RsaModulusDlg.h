/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include <string>
#include <list>
#include <iterator>
using namespace std;

typedef list<string*>           t_stringList;
typedef list<string*>::iterator t_stringListIter;

#include "EditListCtrl.h"
#include "DigitalSignature.h"

// CRsaModulusDlg dialog

class CRsaModulusDlg : public CDialog
{
	DECLARE_DYNAMIC(CRsaModulusDlg)

public:
	CRsaModulusDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CRsaModulusDlg();

// Dialog Data
	enum { IDD = IDD_RSA_MODULUS_DLG };

public:
	CEditListCtrl RsaSystemModulusListCtrl;

	afx_msg void OnBnClickedResetModulusBtn();
	afx_msg void OnBnClickedOk();
	afx_msg void OnLvnItemActivateRsaSystemModulusListCtrl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMKillfocusRsaSystemModulusListCtrl(NMHDR *pNMHDR, LRESULT *pResult);

	UINT uiSizeInBits;

//	void BitSize( unsigned int iSize ) { uiSizeInBits = iSize; }
//	void RsaModulusList( t_stringList* pList ) { m_pRsaModulusList = pList; }

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

public:
	void UpdateChangeMarkerInTitle();

	CDigitalSignature& DigitalSignature() { return m_DigitalSignature; }
	void DigitalSignature( CDigitalSignature& DigitalSignature ){ m_DigitalSignature = DigitalSignature; }

	CKey& Key() { return m_DigitalSignature; }
	void Key( CKey& Key ){ dynamic_cast<CKey&>(m_DigitalSignature) = Key; }

	void Title( string sTitle ){ m_sTitle = sTitle; }

private:
	void RefreshState();
	void UpdateRsaModulusList();

	CDigitalSignature m_DigitalSignature;
	string m_sTitle;
};
