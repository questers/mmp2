/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// PublicKeyExponentDlg.cpp : implementation file
//

#include "stdafx.h"

#if TRUSTED

#include "MarvellBootUtility.h"
#include "PublicKeyExponentDlg.h"


// CPublicKeyExponentDlg dialog

IMPLEMENT_DYNAMIC(CPublicKeyExponentDlg, CDialog)

CPublicKeyExponentDlg::CPublicKeyExponentDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CPublicKeyExponentDlg::IDD, pParent)
{
    uiSizeInBits = 0;
}

CPublicKeyExponentDlg::~CPublicKeyExponentDlg()
{
}

void CPublicKeyExponentDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_PUBLIC_KEY_EXPONENT_LIST_CTRL, PublicKeyExponentListCtrl);
    DDX_Text(pDX, IDC_SIZE_IN_BITS, uiSizeInBits);

    // updates marker in title
    UpdateChangeMarkerInTitle();
    theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CPublicKeyExponentDlg, CDialog)
    ON_BN_CLICKED(IDOK, &CPublicKeyExponentDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDC_RESET_EXPONENT_BTN, &CPublicKeyExponentDlg::OnBnClickedResetExponentBtn)
    ON_NOTIFY(LVN_ITEMACTIVATE, IDC_PUBLIC_KEY_EXPONENT_LIST_CTRL, &CPublicKeyExponentDlg::OnLvnItemActivatePublicKeyExponentListCtrl)
    ON_NOTIFY(NM_KILLFOCUS, IDC_PUBLIC_KEY_EXPONENT_LIST_CTRL, &CPublicKeyExponentDlg::OnNMKillfocusPublicKeyExponentListCtrl)
END_MESSAGE_MAP()


// CPublicKeyExponentDlg message handlers

BOOL CPublicKeyExponentDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    PublicKeyExponentListCtrl.SetExtendedStyle( //  LVS_EX_TRACKSELECT
                                                //  LVS_EX_BORDERSELECT
                                                LVS_EX_FULLROWSELECT 
                                                | LVS_EX_HEADERDRAGDROP 
                                                | LVS_EX_ONECLICKACTIVATE 
                                                | LVS_EX_LABELTIP 
                                                | LVS_EX_GRIDLINES
                                              );

    PublicKeyExponentListCtrl.InsertColumn(0,"#", LVCFMT_LEFT, 36, -1 );
    PublicKeyExponentListCtrl.InsertColumn(1,"Exponent Word #", LVCFMT_LEFT, 140, -1 );
    PublicKeyExponentListCtrl.InsertColumn(2,"Exponent Word #+1", LVCFMT_LEFT, 140, -1 );
    PublicKeyExponentListCtrl.InsertColumn(3,"Exponent Word #+2", LVCFMT_LEFT, 140, -1 );
    PublicKeyExponentListCtrl.InsertColumn(4,"Exponent Word #+3", LVCFMT_LEFT, 140, -1 );

    PublicKeyExponentListCtrl.EditControlType[0] = ReadOnly;
    PublicKeyExponentListCtrl.EditControlType[1] = HexEdit;
    PublicKeyExponentListCtrl.EditControlType[2] = HexEdit;
    PublicKeyExponentListCtrl.EditControlType[3] = HexEdit;
    PublicKeyExponentListCtrl.EditControlType[4] = HexEdit;

    uiSizeInBits = m_Key.KeySize();
    int nRows = (((uiSizeInBits+31)/32)+3)/4;
    for ( int i = 0; i < nRows; i++ )
    {
        PublicKeyExponentListCtrl.InsertItem(i,""); 
    }

    UpdateData(FALSE);
    RefreshState();

    PublicKeyExponentListCtrl.Changed( m_Key.IsChanged() );
    SetWindowTextA( m_sTitle.c_str() );
    
    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void CPublicKeyExponentDlg::RefreshState()
{
    UpdateData( TRUE );

    t_stringList* pCompAList = &m_Key.PublicKeyExponentList();
    if ( m_Key.EncryptAlgorithmId() == "ECDSA_256" || m_Key.EncryptAlgorithmId() == "ECDSA_521" )
        pCompAList = &m_Key.ECDSAPublicKeyCompXList();

    int idxItem = 0; 
    int idxSubItem = 0;
    t_stringListIter iterExponent( pCompAList->begin() );
    while( iterExponent != pCompAList->end() )
    {
        if ( idxSubItem == 0 )
        {
            CString sRow;
            sRow.Format( "%d", idxItem*4 ); // assumes 5 columns per row
            PublicKeyExponentListCtrl.SetItemText( idxItem, idxSubItem++, sRow );
        }
        else
            PublicKeyExponentListCtrl.SetItemText( idxItem, idxSubItem++, (*iterExponent++)->c_str() );
        
        // set idx for next row
        if ( idxSubItem == 5 )	// assumes 5 columns per row
        {
            idxItem++;
            idxSubItem = 0;
        }
    }
    
    UpdateData( FALSE );
}


void CPublicKeyExponentDlg::OnBnClickedResetExponentBtn()
{
    m_Key.ResetExponent();
    RefreshState();
}


void CPublicKeyExponentDlg::OnBnClickedOk()
{
    UpdatePublicKeyExponentList();
    OnOK();
}


void CPublicKeyExponentDlg::UpdatePublicKeyExponentList()
{
    t_stringList* pCompAList = &m_Key.PublicKeyExponentList();
    if ( m_Key.EncryptAlgorithmId() == "ECDSA_256" || m_Key.EncryptAlgorithmId() == "ECDSA_521" )
        pCompAList = &m_Key.ECDSAPublicKeyCompXList();

    int idxItem = 0;
    int idxSubItem = 1;

    t_stringListIter iterExponent( pCompAList->begin() );
    while( iterExponent != pCompAList->end() )
    {
        *(*iterExponent++) = PublicKeyExponentListCtrl.GetItemText( idxItem, idxSubItem++ );

        // set idx for next row
        if ( idxSubItem == 5 )	// assumes 5 columns per row
        {
            idxItem++;
            idxSubItem = 1;  // ignore row number in subitem 0
        }
    }
    UpdateData(FALSE);
}

void CPublicKeyExponentDlg::UpdateChangeMarkerInTitle()
{
    if ( m_hWnd == 0 )
        return;

    CString sTitle;
    GetWindowTextA( sTitle );
    // add or remove * as appropriate
    if ( PublicKeyExponentListCtrl.IsChanged() == true )
    {
        m_Key.Changed( true );
        if ( -1 == sTitle.Find('*') )
            sTitle += "*";
    }
    else
    {
        if ( -1 != sTitle.Find('*') )
            sTitle.Remove('*');
    }
    SetWindowTextA(sTitle);
    UpdateWindow();
}


void CPublicKeyExponentDlg::OnLvnItemActivatePublicKeyExponentListCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    // TODO: Add your control notification handler code here
    *pResult = 0;

    UpdateData(FALSE);
}

void CPublicKeyExponentDlg::OnNMKillfocusPublicKeyExponentListCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
    // TODO: Add your control notification handler code here
    *pResult = 0;

    UpdatePublicKeyExponentList();
    m_Key.Changed(true);
}
#endif