/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#include "StdAfx.h"
#include "Target.h"
#include "MainFrm.h"

#include "MarvellBootUtility.h"
#include "MarvellBootUtilityDoc.h"
#include "MarvellBootUtilityView.h"
#include "DownloaderInterface.h"

#include <sstream>
using namespace std;

#include <windows.h> 
 
#define BUFSIZE 4096*32
 
CTarget::CTarget()
{
    m_sInterfaceName = "UART";
    m_TargetState = CTarget::TARGET_IDLE;
    m_bConnected = false;
    m_pDoc = 0;
    m_DownloadThreadHandle = 0;
    m_hChildStdinRd = 0;
    m_hChildStdinWr = 0;
    m_hChildStdoutRd = 0;
    m_hChildStdoutWr = 0;
    m_pDownloaderInterface = 0;
    memset( &m_StartupInfo, 0, sizeof(STARTUPINFO) );
    memset( &m_ProcessInformation, 0, sizeof(PROCESS_INFORMATION) );

    GetStartupInfo(&m_StartupInfo);
}

CTarget::~CTarget(void)
{
//	delete m_pDownloaderInterface;
//	delete m_pDoc;
}

bool CTarget::StartDownload()
{
    __time64_t ltime = 0;    // for timestamps on messages.
    char szTime[255] = {0};

    if ( m_pDownloaderInterface == 0 )
    {
        AfxMessageBox( "No download interface assigned to target.  Download aborted." );
        return false;
    }

    if ( m_pDownloaderInterface->WtptpExePath().length() == 0 )
    {
        AfxMessageBox( "Enter a path for the WTPTP tool in the Downloader Configuration dialog." );
        return false;
    }

    bool bRet = true;
    if ( m_TargetState != CTarget::TARGET_IDLE )
    {
        AfxMessageBox( "Cannot start download because Target is not idle!" );
        return false;
    }

    if ( ( m_pDownloaderInterface->DownloadType() == CDownloaderInterface::FBF_BASED_FILE )
             || ( m_pDownloaderInterface->DownloadType() == CDownloaderInterface::TIM_BASED_OBM ) )
    {
        // do not try to download FBF to BootRom
        if ( InterfaceName().find( MH_L_BOOTROM_VID_PID ) != string::npos
            || InterfaceName().find( TAVOR_BOOTROM_VID_PID ) != string::npos
            || InterfaceName().find( MH_P_BOOTROM_VID_PID ) != string::npos )
        {
            AfxMessageBox("Download aborted. Only TIM based DKB download allowed to BootRom target.");
            return false;
        }
    }

    if ( m_DownloadThreadHandle )
    {
        TerminateThread( m_DownloadThreadHandle, 0 );
        CloseHandle( m_DownloadThreadHandle );
        m_DownloadThreadHandle = 0;
    }

    string sCommandLine(" ");
    sCommandLine += m_pDownloaderInterface->CommandLineArgs();
    if ( m_sInterfaceName.length() > 0 )
    {
        sCommandLine += " -i \"";
        sCommandLine += m_sInterfaceName;
        sCommandLine += "\"";
    }

    stringstream ss;
    ss << "\nLaunch Downloader Command line: \n" << endl;
    ss << m_pDownloaderInterface->WtptpExePath() << " " << sCommandLine << "\n" << endl;
    DisplayMsg( ss.str() );
    
    DWORD ThreadId = 0;

    // validate should make sure we have all the information needed to launch
    // and indicate what is missing to the user
    if ( !Validate() )
        return false;

    if ( m_DownloadThreadHandle == 0 )
        m_DownloadThreadHandle = CreateThread(
                                            NULL,	// LPSECURITY_ATTRIBUTES lpsa, 
                                            0,		// DWORD cbStack, 
                                            (LPTHREAD_START_ROUTINE)DownloadThread,		// LPTHREAD_START_ROUTINE lpStartAddr, 
                                            this,				// LPVOID lpvThreadParam, 
                                            CREATE_SUSPENDED,	// DWORD fdwCreate, 
                                            &ThreadId			// LPDWORD lpIDThread
                                            ); 

    if ( CreateMessagePipes() != 0 )
        bRet = false;

    if( bRet ) 
    {
        bRet = CreateDownloaderProcess( m_pDownloaderInterface->WtptpExePath(), sCommandLine );
        if ( bRet )
        {
            _time64( &ltime );   // for timestamps on messages
            _ctime64_s( szTime, 255, &ltime );

            stringstream ss;
            ss << szTime << "Process " << std::hex << m_ProcessInformation.dwProcessId << " for " << m_sInterfaceName << " started." << endl;
            DisplayMsg( ss.str() );

            m_TargetState = CTarget::TARGET_DOWNLOADING;
            DWORD dwRslt = ResumeThread(m_DownloadThreadHandle);
        }
        else
        {
            m_TargetState = CTarget::TARGET_ERROR;
            TerminateDownload();  // not downloading, but need to terminate threads/pipe
        }
    }
    else
    {
        int	rc = GetLastError();
        stringstream ss;
        ss << "error " << rc << " launching app." << endl;
        DisplayMsg( ss.str() );
    }

    theApp.DownloadController().UpdateDownloadState( this );
    theApp.RefreshTargetsList();

    return bRet;
}

bool CTarget::TerminateDownload()
{
    bool bRet = false;
    __time64_t ltime = 0;    // for timestamps on messages.
    char szTime[255] = {0};
    stringstream ss;

    if ( m_bConnected && m_TargetState == CTarget::TARGET_DOWNLOADING )//|| m_TargetState == CTarget::TARGET_ERROR )
    {
        DWORD ExitCode = 0;
        GetExitCodeProcess( m_ProcessInformation.hProcess, &ExitCode );
        _time64( &ltime );   // for timestamps on messages
        _ctime64_s( szTime, 255, &ltime );

        // ios_base::skipws blocks output of trailing \n in szTime
        ss << endl << szTime;

        if ( ExitCode == STILL_ACTIVE )
        {
            bRet = TerminateProcess( m_ProcessInformation.hProcess, -1 ) ? true : false;		
            // since downloader has exited, update target status and exit this thread
            ss << "Downloader Process Terminated." << endl << endl;
        }
        else
        {
            ss << "Downloader Process Exited." << endl << endl;
        }
    }

    DisplayMsg( ss.str() );

    if ( m_ProcessInformation.hProcess )
    {
        CloseHandle(m_ProcessInformation.hProcess);
        m_ProcessInformation.hProcess = 0;
    }

    if ( m_ProcessInformation.hThread )
    {
        CloseHandle(m_ProcessInformation.hThread);
        m_ProcessInformation.hThread = 0;
    }
        
    // Set up members of the PROCESS_INFORMATION structure. 
    ZeroMemory( &m_ProcessInformation, sizeof(PROCESS_INFORMATION) );
        // Set up members of the STARTUPINFO structure. 
    ZeroMemory( &m_StartupInfo, sizeof(STARTUPINFO) );
    
    if ( m_hChildStdinRd )
    {
        CloseHandle( m_hChildStdinRd );
        m_hChildStdinRd = 0;
    }
    if ( m_hChildStdinWr )
    {
        CloseHandle( m_hChildStdinWr );
        m_hChildStdinWr = 0;
    }
    if ( m_hChildStdoutRd )
    {
        CloseHandle( m_hChildStdoutRd );
        m_hChildStdoutRd = 0;
    }
    if ( m_hChildStdoutWr )
    {
        CloseHandle( m_hChildStdoutWr );
        m_hChildStdoutWr = 0;
    }

    if ( m_DownloadThreadHandle )
    {
//		TerminateThread( m_DownloadThreadHandle, FALSE );
        CloseHandle( m_DownloadThreadHandle );
        m_DownloadThreadHandle = 0;
    }

    theApp.DownloadController().UpdateDownloadState( this );
    theApp.RefreshTargetsList();

    return bRet;
}

int CTarget::CreateMessagePipes()
{
// NOTE: this code is exerpted from MS VS 2005 Documentation
//  from article "Creating a Child Process with Redirected Input and Output"
// ms-help://MS.VSCC.v80/MS.MSDN.vAug06.en/dllproc/base/creating_a_child_process_with_redirected_input_and_output.htm
//  Code has been modified and added as member functions to this class.

   int nRet = 0; 

   SECURITY_ATTRIBUTES saAttr; 
 
    // Set the bInheritHandle flag so pipe handles are inherited. 
    saAttr.nLength = sizeof(SECURITY_ATTRIBUTES); 
    saAttr.bInheritHandle = TRUE; 
    saAttr.lpSecurityDescriptor = NULL; 

//	// Get the handle to the current STDOUT. 
//	hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
 
//	if ( hStdout == INVALID_HANDLE_VALUE )
//		return -2;

    if ( m_hChildStdoutRd == 0 || m_hChildStdoutWr == 0 )
    {
        // Create a pipe for the child process's STDOUT. 
        if (! CreatePipe(&m_hChildStdoutRd, &m_hChildStdoutWr, &saAttr, BUFSIZE))
        {
            AfxMessageBox("Stdout pipe creation failed"); 
            nRet = -3;
        }

        // Ensure the read handle to the pipe for STDOUT is not inherited.
        SetHandleInformation( m_hChildStdoutRd, HANDLE_FLAG_INHERIT, 0);
    }

    if ( m_hChildStdinRd == 0 || m_hChildStdinWr == 0 )
    {
        // Create a pipe for the child process's STDIN. 
        if (! CreatePipe(&m_hChildStdinRd, &m_hChildStdinWr, &saAttr, BUFSIZE)) 
        {
            AfxMessageBox("Stdin pipe creation failed"); 
            nRet = -1;
        }

        // Ensure the write handle to the pipe for STDIN is not inherited. 
        SetHandleInformation( m_hChildStdinWr, HANDLE_FLAG_INHERIT, 0);
    }

    return nRet;
}

 
bool CTarget::CreateDownloaderProcess( string& sFullPath, string& sCommandLine ) 
{ 
// NOTE: this code is exerpted from MS VS 2005 Documentation
//  from article "Creating a Child Process with Redirected Input and Output"
// ms-help://MS.VSCC.v80/MS.MSDN.vAug06.en/dllproc/base/creating_a_child_process_with_redirected_input_and_output.htm
//  Code has been modified and added as member functions to this class.
   
    char path_buffer[_MAX_PATH]={0};
    char drive[_MAX_DRIVE]={0};
    char dir[_MAX_DIR]={0};
    string sFullDir;
    char fname[_MAX_FNAME]={0};
    char ext[_MAX_EXT]={0};
    errno_t err;

    if ( m_pDownloaderInterface == 0 )
        return false;

    strcpy_s(path_buffer,MAX_PATH, m_pDownloaderInterface->TimDescriptorParser.TimDescriptor().TimDescriptorFilePath().c_str());
    err = _splitpath_s( path_buffer, drive, _MAX_DRIVE, dir, _MAX_DIR, fname,
                        _MAX_FNAME, ext, _MAX_EXT );

    if (err != 0)
    {
        printf("Error splitting the path. Error code %d.\n", err);
        return false;
    }

    sFullDir = drive;
    sFullDir += dir;

    BOOL bRet = SetCurrentDirectory( sFullDir.c_str() );

    bool bFuncRetn = false; 
 
    // Set up members of the PROCESS_INFORMATION structure. 
    ZeroMemory( &m_ProcessInformation, sizeof(PROCESS_INFORMATION) );
 
    // Set up members of the STARTUPINFO structure. 
    ZeroMemory( &m_StartupInfo, sizeof(STARTUPINFO) );
    m_StartupInfo.cb = sizeof(STARTUPINFO); 
    m_StartupInfo.hStdError = m_hChildStdoutWr;
    m_StartupInfo.hStdOutput = m_hChildStdoutWr;
    m_StartupInfo.hStdInput = m_hChildStdinRd;
    m_StartupInfo.dwFlags |= (STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW);
    m_StartupInfo.wShowWindow = SW_HIDE;
 
    // Create the child process. 
    char szDir[MAX_PATH]={0};
    GetCurrentDirectory(MAX_PATH, szDir);
    
    bFuncRetn = CreateProcess((LPSTR)sFullPath.c_str(), 
                              (LPSTR)sCommandLine.c_str(),     // command line 
                              NULL,          // process security attributes 
                              NULL,          // primary thread security attributes 
                              TRUE,          // handles are inherited 
                              0,             // creation flags 
                              NULL,          // use parent's environment 
                              szDir,          // use parent's current directory 
                              &m_StartupInfo,  // STARTUPINFO pointer 
                              &m_ProcessInformation)  // receives PROCESS_INFORMATION 
                              == TRUE ? true : false;

    if (bFuncRetn == false) 
        AfxMessageBox("CreateProcess failed\n");
    else 
    {
//		CloseHandle(m_ProcessInformation.hProcess);
//		CloseHandle(m_ProcessInformation.hThread);
    }
    
//	SetPriorityClass( m_ProcessInformation.hProcess, ABOVE_NORMAL_PRIORITY_CLASS );
    return bFuncRetn;
}

VOID CTarget::WriteToPipe(string& sMsg) 
{ 
    DWORD dwSize = (DWORD)sMsg.length();
    DWORD dwWritten;  
 
    // Read from a file and write its contents to a pipe.  
    if (! WriteFile(m_hChildStdinWr, sMsg.c_str(), dwSize, &dwWritten, NULL)) 
        return;
 
    // Close the pipe handle so the child process stops reading. 
    if (!CloseHandle(m_hChildStdinWr)) 
        DisplayMsg( string("Close pipe failed\n")); 
} 
 
bool CTarget::ReadFromPipe() 
{ 
    // Close the write end of the pipe before reading from the 
    // read end of the pipe. 
    if ( m_hChildStdoutWr )
    {
        if (!CloseHandle(m_hChildStdoutWr)) 
            AfxMessageBox("Closing handle hChildStdoutWr failed"); 
        m_hChildStdoutWr = 0;
    }

    CHAR chBuf[BUFSIZE]={0}; 
    DWORD dwRead = 0;
    DWORD ExitCode = -1;

    // give downloader process up to 2 seconds to start up if system is very busy
    int iWait = 2000;
    while ( iWait > 0 && m_ProcessInformation.hProcess != 0 )
    {
        Sleep(10);
        iWait -= 10;
        // determine if downloader process is now active
        if ( GetExitCodeProcess( m_ProcessInformation.hProcess, &ExitCode ) )
        {
            if ( ExitCode == STILL_ACTIVE )
                break;
        }
    }

    if ( ExitCode != STILL_ACTIVE )
    {
        string msg("Downloader failed to execute.  Check installation of downloader.\n\n");
        AfxMessageBox(msg.c_str()); 
        DisplayMsg(msg);
        return false;
    }

    // Read output from the child process, and write to parent's STDOUT. 
    while( m_ProcessInformation.hProcess != 0 && ExitCode == STILL_ACTIVE )
    {
        // determine if builder process is still active or has exited
        if ( GetExitCodeProcess( m_ProcessInformation.hProcess, &ExitCode ) )
        {
            if ( ExitCode == STILL_ACTIVE )
            {
                if ( ReadFile(m_hChildStdoutRd, chBuf, BUFSIZE, &dwRead, NULL) )
                {
                    if ( dwRead != 0 )
                    {
                        DisplayMsg( string(chBuf) );
                        memset(chBuf,0,dwRead);
                        dwRead = 0;
                    }
                }
            }
        }

        if ( ExitCode != STILL_ACTIVE && ExitCode != 1 )
        {
            stringstream ssMsg;
            ssMsg << endl << "Downloader exited with error. Exit code: ";
            ssMsg << ExitCode << endl;
            AfxMessageBox(ssMsg.str().c_str()); 
            DisplayMsg(ssMsg.str());
            return false;
        }
    }
    return true;
} 

bool CTarget::Validate()
{
    if ( m_pDownloaderInterface )
    {
        string& sCommandLineArgs = m_pDownloaderInterface->CommandLineArgs();
        // simple test for missing paths in command line
        if ( sCommandLineArgs.find("<") == string::npos && sCommandLineArgs.find(">") == string::npos )
            return true;
        else
        {
            AfxMessageBox( "Error: WTPTP command line options are not valid." );
            return false;
        }
    }

    return true;
}

void CTarget::DisplayMsg(string& sMsg)
{
    if ( m_pDoc == NULL )
        m_pDoc = theApp.NewTargetLog( m_sInterfaceName );

    if ( m_pDoc != NULL )
    {
        if ( theApp.IsValidDoc( m_pDoc ) )
        {
            POSITION pos = m_pDoc->GetFirstViewPosition();
            CMarvellBootUtilityView* pView = dynamic_cast<CMarvellBootUtilityView*>((CEditView*)m_pDoc->GetNextView(pos));
            if ( pView )
            {
                // make sure control can handle all excessive output
                if ( pView->GetEditCtrl().GetLimitText() != -1 )
                    pView->GetEditCtrl().SetLimitText(-1); 

                CStringA sNLMsg( sMsg.c_str() );
                sNLMsg.Replace( "\n", "\r\n" );
                pView->GetEditCtrl().ReplaceSel( sNLMsg );
            }
        }
        else
            m_pDoc = 0;
    }
}

DWORD DownloadThread( LPVOID lpParameter )
{
    CTarget* pTarget = (CTarget*)lpParameter;

    // Drain remaining messages from pipe 
    bool bRet = pTarget->ReadFromPipe();
    
    if ( !bRet )
        pTarget->TargetState( CTarget::TARGET_ERROR );

    // since downloader has exited, update target status and exit this thread
    pTarget->TerminateDownload();

//	ExitThread(0);
    return DWORD(bRet);
}

