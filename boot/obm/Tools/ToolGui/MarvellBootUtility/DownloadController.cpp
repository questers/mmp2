/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#include "stdafx.h"
#include "DownloadController.h"
#include "MarvellBootUtility.h"
#include "DownloaderInterface.h"
#include "Target.h"

#include <sstream>
using namespace std;

CDownloadController::CDownloadController()
: CTimLib()
{
	m_bDownloading = false; 
	m_bDkbDownload = false;
	m_bObmDownload = false;
	m_bFbfDownload = false;
	m_bLaunchOnDiscovery = false;

	for ( int i = 0; i < MaxHub; i++ )
	{
		for ( int j = 0; j < MaxPort; j++ )
		{
			DownloadState[i][j].iDeviceState = IDLE;
			DownloadState[i][j].iDownloadInProgress = NONE;
		}
	}
}

CDownloadController::~CDownloadController()
{
}


bool CDownloadController::Hub_Port( string& sInterfaceName, int& iHub, int& iPort /*, string& sDeviceName */  )
{
	iHub = -1;
	iPort = -1;

	size_t iPosHub = sInterfaceName.find_last_of( "&" );
	if ( iPosHub != string::npos )
		iHub = CTimLib::Translate( &sInterfaceName[ iPosHub-1 ] );

	size_t iPosPort = sInterfaceName.find_last_of( "#" );
	if ( iPosPort != string::npos )
		iPort = CTimLib::Translate( &sInterfaceName[ iPosPort-1 ] );

	return (( iPosHub != string::npos ) && ( iPosPort != string::npos )); 
}

bool CDownloadController::ConditionalLaunchDownload( CTarget* pTarget )
{
	bool bRet = false;
	int iHub = -1;
	int iPort = -1;
	if ( pTarget )
	{
		bRet = Hub_Port( pTarget->InterfaceName(), iHub, iPort /*, string& sDeviceName */  );
		if ( bRet )
		{
			if ( m_bDkbDownload && DownloadState[iHub][iPort].iDeviceState == IDLE
				 && DownloadState[iHub][iPort].iDownloadInProgress != DKB )
			{
				pTarget->DownloaderInterface( &theApp.DKBDownloaderInterface() );
				bRet = pTarget->StartDownload();
				if ( bRet )
				{
					DownloadState[iHub][iPort].iDeviceState = DOWNLOADING;
					DownloadState[iHub][iPort].iDownloadInProgress = DKB;
					m_bDownloading = true;
				}
			}
			else if ( m_bObmDownload && 
				( DownloadState[iHub][iPort].iDownloadInProgress == DKB || DownloadState[iHub][iPort].iDownloadInProgress == NONE ) )
			{
				pTarget->DownloaderInterface( &theApp.OBMDownloaderInterface() );
				// do not try to download OBM to BootRom
				if ( pTarget->InterfaceName().find( MH_L_BOOTROM_VID_PID ) != string::npos
					|| pTarget->InterfaceName().find( TAVOR_BOOTROM_VID_PID ) != string::npos
					|| pTarget->InterfaceName().find( MH_P_BOOTROM_VID_PID ) != string::npos )
				{
					AfxMessageBox("Skipping download of TIM based OBM (other) images to BootRom target because such a download is not supported by the bootrom");
				}
				else
				{
					bRet = pTarget->StartDownload();
					if ( bRet )
					{
						DownloadState[iHub][iPort].iDeviceState = DOWNLOADING;
						DownloadState[iHub][iPort].iDownloadInProgress = OBM;
						m_bDownloading = true;
					}
				}
			}
			else if ( m_bFbfDownload && 
				( DownloadState[iHub][iPort].iDownloadInProgress == DKB || DownloadState[iHub][iPort].iDownloadInProgress == NONE ) )
			{
				pTarget->DownloaderInterface( &theApp.FBFDownloaderInterface() );
				// do not try to download FBF to BootRom
				if ( pTarget->InterfaceName().find( MH_L_BOOTROM_VID_PID ) != string::npos
					|| pTarget->InterfaceName().find( TAVOR_BOOTROM_VID_PID ) != string::npos
					|| pTarget->InterfaceName().find( MH_P_BOOTROM_VID_PID ) != string::npos )
				{
					AfxMessageBox("Skipping download of FBF images to BootRom target because such a download is not supported by the bootrom");
				}
				else
				{
					bRet = pTarget->StartDownload();
					if ( bRet )
					{
						DownloadState[iHub][iPort].iDeviceState = DOWNLOADING;
						DownloadState[iHub][iPort].iDownloadInProgress = FBF;
						m_bDownloading = true;
					}
				}
			}
		}
		UpdateDownloadState( pTarget );
	}

	return bRet;
}


void CDownloadController::UpdateDownloadState( CTarget* pTarget )
{
	int iHub = -1;
	int iPort = -1;
	if ( Hub_Port( pTarget->InterfaceName(), iHub, iPort /*, string& sDeviceName */  ) )
	{
#if 1
		if ( pTarget->TargetState() == CTarget::TARGET_ERROR )
		{
			stringstream ssMsg;
			ssMsg << "Download ERROR on Target:" << pTarget->InterfaceName() << endl;
			if ( DownloadState[iHub][iPort].iDownloadInProgress == DKB ) ssMsg << " while downloading DKB image." << endl;
			else if ( DownloadState[iHub][iPort].iDownloadInProgress == OBM ) ssMsg << "OBM images." << endl;
			else if ( DownloadState[iHub][iPort].iDownloadInProgress == FBF ) ssMsg << "FBF images." << endl;
			ssMsg << "Downloading aborted on target. Reinitialize or reconnect target and restart download." << endl;
			ssMsg << "You can also try lowering the USB packet size." << endl;
			AfxMessageBox( ssMsg.str().c_str() );

			if ( DownloadState[iHub][iPort].iDeviceState == DOWNLOADING )
			{
				if ( DownloadState[iHub][iPort].iDownloadInProgress = DKB )
				{
					DownloadState[iHub][iPort].iDeviceState = IDLE;
					DownloadState[iHub][iPort].iDownloadInProgress = NONE;
				}
				else
				{
					DownloadState[iHub][iPort].iDownloadInProgress = DKB;
				}
			}
			else
			{
				DownloadState[iHub][iPort].iDeviceState = IDLE;
				DownloadState[iHub][iPort].iDownloadInProgress = NONE;
			}
		}
#endif

		if ( DownloadState[iHub][iPort].iDeviceState == DOWNLOADING
			 && ( DownloadState[iHub][iPort].iDownloadInProgress == OBM
				  || DownloadState[iHub][iPort].iDownloadInProgress == FBF ) )
		{
			// update state if last download was OBM or FBF
			DownloadState[iHub][iPort].iDeviceState = IDLE;
			DownloadState[iHub][iPort].iDownloadInProgress = NONE;		 
			theApp.RefreshDownloadFlag();
		}
		else if ( DownloadState[iHub][iPort].iDeviceState == DOWNLOADING
				  && DownloadState[iHub][iPort].iDownloadInProgress == DKB )
		{
			// update state if only DKB download was requested
			if ( !m_bFbfDownload && !m_bObmDownload )
			{
				// update state if last download was OBM or FBF
				DownloadState[iHub][iPort].iDeviceState = IDLE;
				DownloadState[iHub][iPort].iDownloadInProgress = NONE;		 
				theApp.RefreshDownloadFlag();
			}
		}
	}

	for ( int i = 0; i < MaxHub; i++ )
	{
		for ( int j = 0; j < MaxPort; j++ )
		{
			if ( DownloadState[i][j].iDeviceState != IDLE )
				return;
		}
	}

	m_bDownloading = false;
}