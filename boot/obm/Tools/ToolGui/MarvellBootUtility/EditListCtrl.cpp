/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// EditListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "EditListCtrl.h"


IMPLEMENT_DYNAMIC(CHexItemEdit, CEdit)

CHexItemEdit::CHexItemEdit( int iDigits )
: m_digits( iDigits ), CEdit()
{
	m_x = 0;
	m_y = 0;
	m_bHexOnly = false;
}

CHexItemEdit::~CHexItemEdit()
{
}

BEGIN_MESSAGE_MAP(CHexItemEdit, CEdit)
	ON_WM_WINDOWPOSCHANGING()
	ON_WM_CHAR()
	ON_CONTROL_REFLECT(EN_UPDATE, &CHexItemEdit::OnEnUpdate)
	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()

void CHexItemEdit::OnContextMenu(CWnd* pWnd, CPoint point)
{
	GetParent()->SendMessage(WM_CONTEXTMENU, (WPARAM)pWnd->m_hWnd, (LPARAM)&point );
	// TODO: Add your message handler code here
}

void CHexItemEdit::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	lpwndpos->x=m_x;
//	lpwndpos->y=m_y;
	CEdit::OnWindowPosChanging(lpwndpos);
}

void CHexItemEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if ( m_bHexOnly )
	{
		// protect leading 0x and limit length to 8 hex digits
		int nLength = GetWindowTextLengthA();
		// permit only hex chars and ctrl 
		if ( nLength >= 2 && nLength < m_digits+2 &&
					  (( nChar >= '0' && nChar <= '9') // valid hexidecimal char
						|| ( nChar >= 'A' && nChar <= 'F' ) 
						|| ( nChar >= 'a' && nChar <= 'f' ))
			)
		{
			CEdit::OnChar(nChar, nRepCnt, nFlags);
		}
		else if ( nChar < ' ' && nLength > 2 && nLength <= m_digits+2 )
		{
			// del chars to be input
			CEdit::OnChar(nChar, nRepCnt, nFlags);
		}
	}
	else
		CEdit::OnChar(nChar, nRepCnt, nFlags);

}

void CHexItemEdit::OnEnUpdate()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CEdit::OnInitDialog()
	// function to send the EM_SETEVENTMASK message to the control
	// with the ENM_UPDATE flag ORed into the lParam mask.

	if ( m_bHexOnly )
	{
		// protect leading 0x and limit length to 8 hex digits
		int nLength = GetWindowTextLengthA();
		if ( nLength < 2 ) SetWindowTextA("0x");
	}
}

CHexItemEdit16::CHexItemEdit16()
	: CHexItemEdit( 16 )
{
}

CHexItemEdit16::~CHexItemEdit16()
{
}

// CEditListCtrl

IMPLEMENT_DYNAMIC(CEditListCtrl, CListCtrl)

CEditListCtrl::CEditListCtrl()
: CListCtrl()
{
	m_bChanged = false;

	m_item = -1;
	m_subitem = -1;
	for ( int i = 0; i < MAX_COLUMNS; i++ )
		EditControlType[i] = ReadOnly;
}

CEditListCtrl::~CEditListCtrl()
{
}


BEGIN_MESSAGE_MAP(CEditListCtrl, CListCtrl)
	ON_NOTIFY_REFLECT(LVN_BEGINLABELEDIT, &CEditListCtrl::OnLvnBeginlabeledit)
	ON_NOTIFY_REFLECT(LVN_ENDLABELEDIT, &CEditListCtrl::OnLvnEndlabeledit)
	ON_WM_LBUTTONDOWN()
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()



// CEditListCtrl message handlers
void CEditListCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	LVHITTESTINFO  HitTestInfo;
	HitTestInfo.pt = point;
	int item = SubItemHitTest(&HitTestInfo);

	CListCtrl::OnLButtonDown(nFlags, point);

	// clicked on an item?
	if ( item != -1 && HitTestInfo.iSubItem != -1 && ( HitTestInfo.flags &LVHT_ONITEM ) )
	{
		if ( !( item == m_item && HitTestInfo.iSubItem == m_subitem ) )
		{
			// click is inside an item or subitem that is not currently being edited
			// if another item or subitem was being edited, that edit is ended and a
			// new edit begins on the just hit subitem
			m_subitem = HitTestInfo.iSubItem;
			m_item = item;

			if ( EditControlType[m_subitem] == ReadOnly )
				return;

			if ( GetItemText( m_item, 0 ) == "<add item>" )
			{
				string* psItemData = new string("0x00000000");
				SetItemText( m_item, 0, psItemData->c_str() );
				SetItemData( m_item, (DWORD_PTR)psItemData );

				InsertItem( m_item + 1, "<add item>" );
			}
			EditLabel( m_item );
		}
	}
}

void CEditListCtrl::OnLvnBeginlabeledit(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);

	//if (subitem selected for editing)
	if ( pDispInfo->item.iItem != -1 && pDispInfo->item.iSubItem != -1 )
	{
		ASSERT( m_item == pDispInfo->item.iItem );

		CRect  SubItemRect;
		GetSubItemRect( m_item, m_subitem, LVIR_BOUNDS , SubItemRect );

		//get edit control and subclass
		VERIFY( m_ItemEditCtrl.SubclassWindow(GetEditControl()->GetSafeHwnd() ) );

		if ( EditControlType[m_subitem] == HexEdit )
			m_ItemEditCtrl.m_bHexOnly = true;
		else
			m_ItemEditCtrl.m_bHexOnly = false;

		//move edit control text 1 pixel to the right of org label,
		//as Windows does it...
		m_ItemEditCtrl.m_x = SubItemRect.left;// + 6;
		m_ItemEditCtrl.m_y = SubItemRect.right;
		m_ItemEditCtrl.SetWindowText( GetItemText( m_item, m_subitem ));

		//hide subitem text so it doesn't show if we delete some text in the edit control
		//OnPaint handles other issues also regarding this
//		GetSubItemRect( m_item, m_subitem, LVIR_LABEL, SubItemRect );
//		CDC* hDc = GetDC();
//		hDc->FillRect( SubItemRect, &CBrush(::GetSysColor(COLOR_WINDOW) ) );
//		ReleaseDC(hDc);
	}

	//return: editing permitted
	*pResult = 0;
}


void CEditListCtrl::OnPaint()
{
	if ( m_item > -1 && m_subitem > 0 )
	{
		CRect  SubItemRect;
		GetSubItemRect( m_item, m_subitem, LVIR_LABEL, SubItemRect );

		if ( m_ItemEditCtrl.GetSafeHwnd() != 0 )
		{
			CRect  EditCtrlRect;
			m_ItemEditCtrl.GetWindowRect( EditCtrlRect );
			ScreenToClient( EditCtrlRect );

			//block text redraw of the subitems text (underneath the editcontrol)
			//if we didn't do this and deleted some text in the edit
			//control, the subitems original label would show
			if ( EditCtrlRect.right < SubItemRect.right)
			{
				SubItemRect.left = EditCtrlRect.right;
				ValidateRect( SubItemRect );
			}
		}

		// block filling redraw of leftmost item (caused by FillRect)
		CRect ItemRect;
		GetItemRect( m_item, ItemRect, LVIR_LABEL );
		ValidateRect( ItemRect );
	}

	CListCtrl::OnPaint();
}

void CEditListCtrl::OnSize(UINT nType, int cx, int cy)
{
	//stop editing if resizing
	if( GetFocus() != this ) SetFocus();

	CListCtrl::OnSize(nType, cx, cy);
}


void CEditListCtrl::OnLvnEndlabeledit(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVDISPINFO *plvDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
	LV_ITEM  *plvItem = &plvDispInfo->item;

	//if (end of sub-editing) do cleanup
	if ( m_subitem >= 0 )
	{
		//plvItem->pszText is NULL if editing canceled
//		if ( plvItem->pszText != NULL )
//		  SetItemText( plvItem->iItem, m_subitem, plvItem->pszText);

		CString sText;
		m_ItemEditCtrl.GetWindowTextA( sText );
		SetItemText( plvItem->iItem, m_subitem, sText );
		VERIFY(m_ItemEditCtrl.UnsubclassWindow()!=NULL);

		// done with subitem until another is selected
		m_subitem = -1;
		*pResult = 0;
		m_bChanged = true;
	}
	else
		//return: update label on leftmost item
		*pResult = 1;
}

void CEditListCtrl::InsertAfterSelectedItem()
{
	int item = GetSelectionMark();
	if ( item != -1 )
		item++;
	else
		item = GetItemCount()-1; // put in list before <add item>

	if ( item >= 0 )
	{
		InsertItem( item, "" );
		string* psItemData = new string("0x00000000");
		SetItemText( item, 0, psItemData->c_str() );
		SetItemData( item, (DWORD_PTR)psItemData );
		SetSelectionMark( item );
		m_bChanged = true;
	}
}

void CEditListCtrl::DeleteSelectedItem()
{
	int item = GetSelectionMark();
	if ( item != -1 )
	{
		DeleteItem( item );
		SetSelectionMark( --item );
		m_bChanged = true;
	}
}


void CEditListCtrl::OnContextMenu(CWnd* pWnd, CPoint point)
{
	// TODO: Add your message handler code here
	GetParent()->SendMessage(WM_CONTEXTMENU, (WPARAM)pWnd->m_hWnd, (LPARAM)&point );
}


