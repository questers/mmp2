/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "afxwin.h"


// CTimTextViewDlg dialog

class CTimTextViewDlg : public CDialog
{
	DECLARE_DYNAMIC(CTimTextViewDlg)

public:
	CTimTextViewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTimTextViewDlg();

// Dialog Data
	enum { IDD = IDD_TIM_VIEW_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CEdit FileContentsEdt;
	string sViewText;
	CFont font;

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();

public:
	void SetViewText( string& sText );
	t_stringList Lines;
};
