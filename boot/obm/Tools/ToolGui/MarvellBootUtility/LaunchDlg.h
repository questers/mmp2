/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

// CLaunchDlg dialog

class CLaunchDlg : public CDialog
{
	DECLARE_DYNAMIC(CLaunchDlg)

public:
	CLaunchDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLaunchDlg();

// Dialog Data
	enum { IDD = IDD_WTPTP_LAUNCH_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

public:
	void RefreshState();

public:
	CListCtrl TargetListCtrl;
	afx_msg void OnBnClickedLaunchTargetBtn();
	afx_msg void OnBnClickedRefreshTargetsBtn();
	afx_msg void OnBnClickedLaunchAllBtn();
	afx_msg void OnBnClickedTerminateTargetBtn();
	afx_msg void OnBnClickedTerminateAllBtn();
	afx_msg void OnBnClickedDkbDownloadChk();
	afx_msg void OnBnClickedObmDownloadChk();
	afx_msg void OnBnClickedFbfDownloadChk();
	afx_msg void OnBnClickedLaunchDiscChk();

	CButton DkbDownloadChk;
	CButton ObmDownloadChk;
	CButton FbfDownloadChk;
	CButton LaunchOnDiscoveryChk;
};
