//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MarvellBootUtility.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             101
#define IDR_MAINFRAME                   128
#define IDR_MarvellBootUtilityTYPE      129
#define IDD_WTPTP_LAUNCH_DLG            131
#define IDD_TIM_DESCRIPTOR_PROPPAGE     133
#define IDD_TIM_IMAGE_CONFIG_DLG        134
#define IDD_RESERVED_DATA_DLG           135
#define IDD_RESERVED_DATA_PACKAGE_DLG   136
#define IDD_PUBLIC_KEY_EXPONENT_DLG     137
#define IDD_RSA_MODULUS_DLG             139
#define IDD_KEYS_DLG                    140
#define IDD_KEY_DLG                     141
#define IDD_DIGITAL_SIGNATURE_DLG       143
#define IDD_PARTITION_DLG               147
#define IDD_PARTITION_TABLE_DLG         148
#define IDD_PRIVATE_KEY_EXPONENT_DLG    150
#define IDD_EXT_RESERVED_DATA_PKG_DLG   151
#define IDD_RESERVED_DATA_PKG_FIELD_DLG 152
#define IDD_FIELD_COMMENT_DLG           155
#define IDD_TIM_VIEW_DLG                156
#define IDD_TIM_DOWNLOADER_DLG          157
#define IDD_FBF_DOWNLOADER_PROPPAGE     158
#define IDD_IMAGE_BUILD_DLG             159
#define IDD_INSTRUCTIONS_DLG            160
#define IDD_DDR_OPERATIONS_DLG          161
#define IDD_DDR_INITIALIZATION_PKG_DLG  162
#define IDD_CONSUMER_PKG_DLG            163
#define IDD_IMPORT_DLG                  165
#define IDD_IMPORT_TIM_BINARY_DLG       166
#define IDD_FBF_MAKE_DLG                167
#define IDD_TZ_OPERATIONS_DLG           168
#define IDD_TZ_INITIALIZATION_PKG_DLG   169
#define IDC_COMMAND_LINE_TEXT           1000
#define IDC_VERBOSE_MODE_CHK            1001
#define IDC_JTAG_KEY_FILE_PATH          1014
#define IDC_JTAG_KEY_FILE_BROWSE_BTN    1015
#define IDC_FLASH_TYPE_COMBO            1018
#define IDC_RUN_DEBUG_BOOT_CMD_CHK      1019
#define IDC_UART_DELAY                  1020
#define IDC_PORT_TYPE_CB                1021
#define IDC_MSG_MODE_CB                 1022
#define IDC_BAUD_RATE_CB                1023
#define IDC_COM_PORT_CB                 1024
#define IDC_WTPTP_EXE_PATH              1025
#define IDC_TIM_BIN_FILE_PATH           1026
#define IDC_WTPTP_EXE_BROWSE_BTN        1028
#define IDC_LAUNCH_ALL_BTN              1036
#define IDC_TERMINATE_ALL_BTN           1037
#define IDC_TARGET_LIST                 1040
#define IDC_LAUNCH_DISC_CHK             1043
#define IDC_TERMINATE_TARGET_BTN        1044
#define IDC_LAUNCH_TARGET_BTN           1045
#define IDC_TBB_EXE_PATH                1050
#define IDC_TBB_EXE_BROWSE_BTN          1051
#define IDC_TRUSTED_CHK                 1052
#define IDC_DIGITAL_SIGNATURE_CHK       1057
#define IDC_HASH_KEY_FILE_CHK           1058
#define IDC_HASH_KEY_FILE_PATH          1059
#define IDC_HASH_KEY_FILE_PATH_BROWSE_BTN 1060
#define IDC_DIGITAL_SIGNATURE_KEY_FILE_PATH 1061
#define IDC_DIGITAL_SIGNATURE_KEY_FILE_PATH_BROWSE_BTN 1062
#define IDC_TIM_INPUT_FILE_CHK          1063
#define IDC_TIM_INPUT_FILE_PATH         1064
#define IDC_TIM_INPUT_KEY_FILE_PATH_BROWSE_BTN 1065
#define IDC_TIM_OUTPUT_FILE_CHK         1066
#define IDC_TIM_OUTPUT_FILE_PATH        1067
#define IDC_TIM_OUTPUT_FILE_PATH_BROWSE_BTN 1068
#define IDC_VERIFY_CHK                  1069
#define IDC_RESERVED_FILE_PATH          1070
#define IDC_VERBOSE_CHK                 1071
#define IDC_CONCATENATE                 1072
#define IDC_RESERVED_FILE_CHK           1073
#define IDC_RESERVED_FILE_PATH_BROWSE_BTN 1074
#define IDC_TARGET_TRUSTED_CHK          1075
#define IDC_PRIVATE_KEY_CHK             1076
#define IDC_PRIVATE_KEY_FILE_PATH       1077
#define IDC_TBB_COMMAND_LINE_TXT        1078
#define IDC_TARGET_TIM_FILE_PATH        1079
#define IDC_TARGET_TIM_FILE_PATH_BROWSE_BTN 1080
#define IDC_PRIVATE_KEY_FILE_PATH_BROWSE_BTN 1081
#define IDC_TIM_DESCRIPTOR_FILE_PATH_EDT 1090
#define IDC_TIM_DESCRIPTOR_FILE_PATH_BROWSE_BTN 1091
#define IDC_TBB_RESET_BTN               1100
#define IDC_WTPTP_RESET_BTN             1101
#define IDC_TIM_VERSION_EDT             1105
#define IDC_ISSUE_DATE_EDT              1118
#define IDC_OEM_UNIQUE_ID_EDT           1119
#define IDC_BOOTROM_FLASH_SIGNATURE_EDT 1120
#define IDC_WTM_FLASH_SIGNATURE_EDT     1142
#define IDC_WTM_FLASH_ENTRY_EDT         1143
#define IDC_WTM_BACKUP_ENTRY_EDT        1144
#define IDC_PUBLIC_KEY_EXPONENT_BTN     1150
#define IDC_RSA_PRIVATE_KEY_BTN         1153
#define IDC_TIM_RESET_BTN               1156
#define IDC_RESERVED_DATA_BTN           1157
#define IDC_TIM_DESCRIPTOR_WRITE_BTN    1158
#define IDC_IMAGE_FILE_PATH_EDT         1159
#define IDC_TIM_DESCRIPTOR_READ_BTN     1160
#define IDC_IMAGE_FILE_PATH_BROWSE_BTN  1161
#define IDC_IMAGE_ID_TAG                1162
#define IDC_IMAGE_ID_CB                 1166
#define IDC_IMAGE_FLASH_ENTRY_EDT       1167
#define IDC_IMAGE_LOAD_ADDRESS_EDT      1168
#define IDC_ADD_IMAGE_BTN               1180
#define IDC_EDIT_IMAGE_BTN              1181
#define IDC_REMOVE_IMAGE_BTN            1182
#define IDC_INSERT_PACKAGE_BTN          1188
#define IDC_PACKAGE_HEADER_ID           1189
#define IDC_PACKAGES_LIST_CTRL          1190
#define IDC_PACKAGE_DATA_LIST_CTRL      1191
#define IDC_DELETE_PACKAGE_BTN          1192
#define IDC_DELETE_EXT_PACKAGE_BTN      1193
#define IDC_TOTAL_SIZE                  1194
#define IDC_EDIT_PACKAGE_BTN            1195
#define IDC_EDIT_EXT_RESERVED_BTN       1196
#define IDC_ADD_EXT_RESERVED_BTN        1197
#define IDC_PUBLIC_KEY_EXPONENT_LIST_CTRL 1199
#define IDC_KEYID_CB                    1201
#define IDC_HASH_ALGORITHM_ID_CB        1202
#define IDC_IMAGE_HASH_ALGORITHM_ID_CB  1204
#define IDC_IMAGE_SIZE_TO_HASH_EDT      1205
#define IDC_KEY_SIZE_CB                 1207
#define IDC_RSA_SYSTEM_MODULUS_LIST_CTRL 1208
#define IDC_IMAGES_LIST_CTRL            1209
#define IDC_EDIT_KEY_BTN                1210
#define IDC_REMOVE_KEY_BTN              1211
#define IDC_DIGITAL_SIGNATURE_BTN       1212
#define IDC_KEY_TAG_EDT                 1213
#define IDC_PACKAGE_HEADER_ID_TAG_EDT   1214
#define IDC_RESET_EXPONENT_BTN          1215
#define IDC_RESET_MODULUS_BTN           1216
#define IDC_REFRESH_TARGETS_BTN         1218
#define IDC_RSA_MODULUS_BTN             1219
#define IDC_KEYS_BTN                    1220
#define IDC_SIZE_IN_BITS                1221
#define IDC_KEYS_LIST_CTRL              1222
#define IDC_DSA_ALGORITHM_ID_CB         1223
#define IDC_MODULUS_SIZE_CB             1224
#define ID_BUILD_IMAGE_BTN              1227
#define IDC_INCLUDE_IN_TIM              1229
#define IDC_PARTITION_FILE_CHK          1230
#define IDC_PARTITION_FILE_PATH         1231
#define IDC_PARTITION_FILE_PATH_BROWSE_BTN 1232
#define IDC_PARTITION_NUMBER_EDT        1233
#define IDC_BUILD_ADDITIONAL_OPTIONS_EDT 1234
#define IDC_PACKAGE_INSERT_ITEM_BTN     1235
#define IDC_PACKAGE_DELETE_ITEM_BTN     1236
#define IDC_INSERT_KEY_BTN              1237
#define IDC_KEY_MOVE_UP_BTN             1238
#define IDC_KEY_MOVE_DOWN_BTN           1239
#define IDC_RESERVED_DATA_MOVE_UP_BTN   1240
#define IDC_RESERVED_DATA_MOVE_DOWN_BTN 1241
#define IDC_IMAGES_MOVE_UP_BTN          1242
#define IDC_IMAGE_MOVE_DOWN_BTN         1243
#define IDC_PACKAGE_DATE_MOVE_UP_BTN    1244
#define IDC_PACKAGE_DATA_MOVE_DOWN_BTN  1245
#define IDC_WTPTP_ADDITIONAL_OPTIONS_EDT 1248
#define IDC_WTPTP_IMAGES_LIST_CTRL      1250
#define IDC_BUILD_ADDITIONAL_OPTIONS_CHK 1251
#define IDC_BUILD_PROCESSOR_TYPE_CHK    1252
#define IDC_WTPTP_DOWNLOAD_BTN          1253
#define IDC_WTPTP_DOWNLOAD_STOP_BTN     1254
#define IDC_PARTITION_FILE_PATH_EDT     1255
#define IDC_PARTITION_BINARY_BROWSE_BTN 1256
#define IDC_PARTITION_BINARY_CHK        1257
#define IDC_DOWNLOADER_ADDITIONAL_OPTIONS_CHK 1258
#define IDC_PARTITION_USAGE_CB          1259
#define IDC_PARTITION_ID_EDT            1260
#define IDC_UPLOAD_SPEC_FILE_CHK        1262
#define IDC_UPLOAD_SPEC_FILE_PATH_EDT   1263
#define IDC_UPLOAD_SPEC_FILE_BROWSE_BTN 1264
#define IDC_PARTITION_TYPE_CB           1265
#define IDC_PARTITION_ATTRIBUTES_EDT    1266
#define IDC_PARTITION_START_ADDRESS_EDT 1267
#define IDC_PARTITION_END_ADDRESS_EDT   1268
#define IDC_RESERVED_POOL_START_ADDRESS_EDT 1269
#define IDC_RESERVED_POOL_SIZE_EDT      1270
#define IDC_RESERVED_POOL_ALGORITHM_CB  1271
#define IDC_RUNTIME_BBT_TYPE_CB         1272
#define IDC_RUNTIME_BBT_LOCATION_EDT    1273
#define IDC_BACKUP_RUNTIME_BBT_LOCATION_EDT 1274
#define IDC_PARTITION_VERSION_EDT       1275
#define IDC_PARTITIONS_LIST             1276
#define IDC_INSERT_PARTITION_BTN        1277
#define IDC_PARTITION_MOVE_UP_BTN       1278
#define IDC_PARTITION_MOVE_DOWN_BTN     1279
#define IDC_DELETE_PARTITION_BTN        1280
#define IDC_PARTITION_READ_BTN          1281
#define IDC_PARTITION_WRITE_BTN         1282
#define IDC_PARTITION_TEXT_FILE_PATH_EDT 1283
#define IDC_PARTITION_BROWSE_BTN        1284
#define IDC_PARTITION_TABLE_BTN         1285
#define IDC_EDIT_PARTITION_BTN          1286
#define IDC_PRIVATE_KEY_EXPONENT_LIST_CTRL 1287
#define IDC_EXT_RESERVED_DATA_PKG_ID_CB 1288
#define IDC_EXT_RESERVED_DATA_PKG_INSERT_FIELD_BTN 1289
#define IDC_EXT_RESERVED_DATA_PKG_EDIT_FIELD_BTN 1290
#define IDC_EXT_RESERVED_DATA_PKG_DELETE_FIELD_BTN 1291
#define IDC_EXT_RESERVED_DATA_PKG_MOVE_UP_BTN 1292
#define IDC_EXT_RESERVED_DATA_PKG_MOVE_DOWN_BTN 1293
#define IDC_EXT_RESERVED_DATA_PKG_FIELDS_LIST 1294
#define IDC_RESERVED_DATA_PACKAGE_PACKAGE_ID_EDT 1295
#define IDC_RESERVED_DATA_PACKAGE_FIELD_VALUE_EDT 1296
#define IDC_RESERVED_DATA_PACKAGE_FIELD_NAME_CB 1297
#define IDC_EXT_RESERVED_DATA_NESTED_PKG_LIST 1298
#define IDC_TOTAL_SIZE_EXT_RESERVED_DATA_EDT 1303
#define IDC_EXT_RESERVED_DATA_PACKAGES_LIST 1304
#define IDC_BOOTROM_FLASH_SIGNATURE_CB  1308
#define IDC_KEY_INSERT_SAMPLE_KEY_BTN   1310
#define IDC_SAMPLE_DS_BTN               1311
#define IDC_WTM_FLASH_SIGNATURE_CB      1312
#define IDC_TIM_PROC_CB                 1313
#define IDC_PRE_COMMENT_EDT             1314
#define IDC_POST_COMMENT_EDT            1315
#define IDC_TIM_CONTENTS_EDT            1316
#define IDC_USB_PACKET_SIZE             1317
#define IDC_TIM_DESCRIPTOR_VIEW_BTN     1318
#define IDC_FBF_PATH                    1319
#define IDC_FBF_BROWSE_BTN              1320
#define IDC_USB_DOWNLOAD_BTN            1321
#define IDC_IMAGE_BUILDER_BTN           1323
#define IDC_DKB_DOWNLOAD_CHK            1324
#define IDC_OBM_DOWNLOAD_CHK            1325
#define IDC_FBF_DOWNLOAD_CHK            1326
#define IDC_TIM_DOWNLOAD_CONFIG_BTN     1327
#define IDC_DOWNLOAD_PROCESSOR_TYPE_CHK 1328
#define IDC_INSTRUCTION_OP_CODE_CB      1329
#define IDC_PARAM1_VAL_EDT              1330
#define IDC_PARAM2_VAL_EDT              1331
#define IDC_PARAM3_VAL_EDT              1332
#define IDC_PARAM4_VAL_EDT              1333
#define IDC_PARAM5_VAL_EDT              1334
#define IDC_PARAM1_DESC_EDT             1335
#define IDC_PARAM2_DESC_EDT             1336
#define IDC_PARAM3_DESC_EDT             1337
#define IDC_PARAM4_DESC_EDT             1338
#define IDC_PARAM5_DESC_EDT             1339
#define IDC_NUM_PARAMS_EDT              1340
#define IDC_OP_CODE_VALUE_EDT           1341
#define IDC_INSTRUCTIONS_LIST           1343
#define IDC_OPERATIONS_LIST             1344
#define IDC_OPERATION_ID_CB             1345
#define IDC_OPERATION_ID_TAG_EDT        1346
#define IDC_OPERATION_VALUE_EDT         1347
#define IDC_PKG_ID_EDT                  1348
#define IDC_PKG_TAG_EDT                 1349
#define IDC_INSTRUCTIONS_BTN            1350
#define IDC_OPERATIONS_BTN              1351
#define IDC_CONSUMER_ID_CB              1352
#define IDC_PACKAGE_IDS_LIST            1354
#define IDC_PKG_MOVE_UP_BTN             1355
#define IDC_PKG_MOVE_DOWN_BTN           1356
#define IDC_INSERT_PKG_BTN              1357
#define IDC_REMOVE_PKG_BTN              1358
#define IDC_INSERT_BTN                  1359
#define IDC_REMOVE_BTN                  1360
#define IDC_MOVE_UP                     1361
#define IDC_MOVE_DOWN                   1362
#define IDC_CONSUMMABLE_PKGS_LIST       1377
#define IDC_IMPORT_DLG_BTN              1380
#define IDC_DDR_XDB_SCRIPT_FILE_PATH_EDT 1381
#define IDC_DDR_XDB_SCRIPT_BROWSE_BTN   1382
#define IDC_IMPORT_DDR_XDB_FILE_BTN     1383
#define IDC_TIM_BINARY_FILE_PATH_EDT    1384
#define IDC_TIM_BINARY_FILE_BROWSE_BTN  1385
#define IDC_IMPORT_TIM_BINARY_FILE_BTN  1386
#define IDC_IMPORTED_XDB_SCRIPT_TXT     1387
#define IDC_IMPORTED_TIM_BINARY_TXT     1388
#define IDC_BIN_IMAGES_LIST_CTRL        1389
#define IDC_IMAGE_FILE_BROWSE_BTN       1390
#define IDC_ONENAND_PADDING_CHK         1391
#define IDC_ONENAND_PADDING_TIM_SIZE    1392
#define IDC_ONENAND_PADDING_TIM_SIZE_SPIN 1393
#define IDC_TIM_VERSION_CB              1394
#define IDC_FBF_MAKE                    1500
#define IDC_FBF_MAKE_EXE_PATH           1501
#define IDC_FBF_MAKE_EXE_BROWSE_BTN     1502
#define IDC_FBF_MAKE_COMMAND_LINE_TXT   1504
#define IDC_FBF_MAKE_ADD_OPTS           1505
#define IDC_FBF_MAKE_STATIC             1506
#define IDC_FBF_MAKE_ADDITIONAL_OPTIONS_EDT 1507
#define IDC_FBF_FILE_PATH               1509
#define IDC_FBF_FILE_STATIC             1510
#define IDC_INI_PATH                    1511
#define IDC_INI_BROWSE_BTN              1512
#define IDC_FBF_MAKE_MODE               1513
#define IDC_FBF_FILE_BROWSE_BTN         1514
#define IDC_FBF_NOTE_STATIC             1515
#define IDC_FBF_MAKE_RESET              1516
#define IDC_FBF_MAKE_LAUNCH             1517
#define IDC_FBF_MODE_CHOICES            1518
#define IDC_FBF_RESET_CHOICES           1519
#define IDC_FBF_MAKE_CONFIG_BTN         1520
#define IDC_ENCRYPT_ALGORITHM_ID_CB     1521
#define IDC_EDIT1                       1524
#define IDC_INSTRUCTION_COMMENT         1524
#define ID_FILE_LOADPROJECT             32774
#define ID_FILE_SAVEPROJECT             32775
#define ID_CONFIG_TARGETLIST            32776
#define ID_CONFIG_DKB_DOWNLOADER        32780
#define ID_CONFIG_OBM_DOWNLOADER        32781
#define ID_CONFIG_FBF_DOWNLOADER        32782

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        170
#define _APS_NEXT_COMMAND_VALUE         32783
#define _APS_NEXT_CONTROL_VALUE         1525
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
