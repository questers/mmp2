/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// CImageBuildDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "ImageBuildDlg.h"
#include "PartitionTableDlg.h"
#include "ToolsInterfacePropertySheet.h"
#include "TimDescriptorPropPage.h"

#include "stdlib.h"

#pragma warning ( disable : 4996 ) // Disable warning messages

// CImageBuildDlg dialog

IMPLEMENT_DYNAMIC(CImageBuildDlg, CDialog)

CImageBuildDlg::CImageBuildDlg(CDownloaderInterface& rDownloaderInterface, CWnd* pParent /*=NULL*/)
	: CDialog(CImageBuildDlg::IDD, pParent)
	, m_ImageBuild(rDownloaderInterface.ImageBuild)
	, m_TimDescriptor(rDownloaderInterface.TimDescriptorParser.TimDescriptor())
	, m_DownloaderInterface(rDownloaderInterface)
{
	bTrusted = false;
	bVerify = false;
	bVerboseMode = false;
	bConcatenate = false;
	bOneNANDPadding = false;
	bHashKey = false;
	bPrivateKey = false;
	bDigitalSignature = false;
	bTimInputFile = false;
	bTimOutputFile = false;
	bReservedFile = false;
	bPartitionFile = false;
	bAdditionalOptions = false;
	bProcessorType = false;
	sTbbExePath = _T("");
	sTimDescriptorFilePath = _T("");
	sHashKeyFilePath = _T("");
	sPrivateKeyFilePath = _T("");
	sDigitalSignatureFilePath = _T("");
	sTimInputFilePath = _T("");
	sTimOutputFilePath = _T("");
	sReservedFilePath = _T("");
	sPartitionFilePath = _T("");
	sCommandLineTextStr = _T("");  
	sAdditionalOptions = _T("");
	iOneNANDPaddingSize = 0;
}

CImageBuildDlg::~CImageBuildDlg()
{
}

void CImageBuildDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_TARGET_TRUSTED_CHK, bTrusted);
	DDX_Check(pDX, IDC_VERIFY_CHK, bVerify);
	DDX_Check(pDX, IDC_VERBOSE_CHK, bVerboseMode);
	DDX_Check(pDX, IDC_CONCATENATE, bConcatenate);
	DDX_Check(pDX, IDC_ONENAND_PADDING_CHK, bOneNANDPadding);
	DDX_Check(pDX, IDC_HASH_KEY_FILE_CHK, bHashKey);
	DDX_Check(pDX, IDC_PRIVATE_KEY_CHK, bPrivateKey);
	DDX_Check(pDX, IDC_DIGITAL_SIGNATURE_CHK, bDigitalSignature);
	DDX_Check(pDX, IDC_TIM_INPUT_FILE_CHK, bTimInputFile);
	DDX_Check(pDX, IDC_TIM_OUTPUT_FILE_CHK, bTimOutputFile);
	DDX_Check(pDX, IDC_RESERVED_FILE_CHK, bReservedFile);
	DDX_Text(pDX, IDC_TBB_EXE_PATH, sTbbExePath);
	DDX_Text(pDX, IDC_HASH_KEY_FILE_PATH, sHashKeyFilePath);
	DDX_Text(pDX, IDC_PRIVATE_KEY_FILE_PATH, sPrivateKeyFilePath);
	DDX_Text(pDX, IDC_DIGITAL_SIGNATURE_KEY_FILE_PATH, sDigitalSignatureFilePath);
	DDX_Text(pDX, IDC_TIM_INPUT_FILE_PATH, sTimInputFilePath);
	DDX_Text(pDX, IDC_TIM_OUTPUT_FILE_PATH, sTimOutputFilePath);
	DDX_Text(pDX, IDC_RESERVED_FILE_PATH, sReservedFilePath);
	DDX_Control(pDX, IDC_HASH_KEY_FILE_CHK, HashKeyFileChk);
	DDX_Control(pDX, IDC_HASH_KEY_FILE_PATH, HashKeyFilePathEdt);
	DDX_Control(pDX, IDC_HASH_KEY_FILE_PATH_BROWSE_BTN, HashKeyFilePathBrowseBtn);
	DDX_Control(pDX, IDC_PRIVATE_KEY_CHK, PrivateKeyChk);
	DDX_Control(pDX, IDC_PRIVATE_KEY_FILE_PATH, PrivateKeyFilePathEdt);
	DDX_Control(pDX, IDC_PRIVATE_KEY_FILE_PATH_BROWSE_BTN, PrivateKeyFilePathBrowseBtn);
	DDX_Control(pDX, IDC_DIGITAL_SIGNATURE_CHK, DigitalSignatureChk);
	DDX_Control(pDX, IDC_DIGITAL_SIGNATURE_KEY_FILE_PATH, DigitalSignatureFilePathEdt);
	DDX_Control(pDX, IDC_DIGITAL_SIGNATURE_KEY_FILE_PATH_BROWSE_BTN, DigitalSignatureFilePathBrowseBtn);
	DDX_Control(pDX, IDC_TIM_INPUT_FILE_CHK, TimInputFileChk);
	DDX_Control(pDX, IDC_TIM_INPUT_FILE_PATH, TimInputFilePathEdt);
	DDX_Control(pDX, IDC_TIM_INPUT_KEY_FILE_PATH_BROWSE_BTN, TimInputFilePathBrowseBtn);
	DDX_Control(pDX, IDC_TIM_OUTPUT_FILE_CHK, TimOutputFileChk);
	DDX_Control(pDX, IDC_TIM_OUTPUT_FILE_PATH, TimOutputFilePathEdt);
	DDX_Control(pDX, IDC_TIM_OUTPUT_FILE_PATH_BROWSE_BTN, TimOutputFilePathBrowseBtn);
	DDX_Text(pDX, IDC_TARGET_TIM_FILE_PATH, sTimDescriptorFilePath);
	DDX_Text(pDX, IDC_TBB_COMMAND_LINE_TXT, sCommandLineTextStr);
	DDX_Control(pDX, IDC_RESERVED_FILE_CHK, ReservedFileChk);
	DDX_Control(pDX, IDC_RESERVED_FILE_PATH, ReservedFilePathEdt);
	DDX_Control(pDX, IDC_RESERVED_FILE_PATH_BROWSE_BTN, ReservedFilePathBrowseBtn);
	DDX_Check(pDX, IDC_PARTITION_FILE_CHK, bPartitionFile);
	DDX_Text(pDX, IDC_PARTITION_FILE_PATH, sPartitionFilePath);
	DDX_Control(pDX, IDC_PARTITION_FILE_CHK, PartitionFileChk);
	DDX_Control(pDX, IDC_PARTITION_FILE_PATH, PartitionFilePathEdt);
	DDX_Control(pDX, IDC_PARTITION_FILE_PATH_BROWSE_BTN, PartitionFilePathBtn);
	DDX_Control(pDX, ID_BUILD_IMAGE_BTN, BuildImageBtn);
	DDX_Text(pDX, IDC_BUILD_ADDITIONAL_OPTIONS_EDT, sAdditionalOptions);
	DDX_Check(pDX, IDC_BUILD_ADDITIONAL_OPTIONS_CHK, bAdditionalOptions);
	DDX_Check(pDX, IDC_BUILD_PROCESSOR_TYPE_CHK, bProcessorType);
	DDX_Control(pDX, IDC_BUILD_ADDITIONAL_OPTIONS_EDT, AdditionalOptionsEdt);
	DDX_Control(pDX, IDC_PARTITION_TABLE_BTN, PartitionTableBtn);

	// updates marker in title
	UpdateChangeMarkerInTitle();
	theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
	DDX_Text(pDX, IDC_ONENAND_PADDING_TIM_SIZE, iOneNANDPaddingSize);
	DDX_Control(pDX, IDC_ONENAND_PADDING_TIM_SIZE, OneNANDPaddingSizeEdt);
	DDX_Control(pDX, IDC_ONENAND_PADDING_TIM_SIZE_SPIN, OneNANDPaddingSizeSpin);
}


BEGIN_MESSAGE_MAP(CImageBuildDlg, CDialog)
	ON_EN_CHANGE(IDC_TBB_EXE_PATH, &CImageBuildDlg::OnEnChangeTbbExePath)
	ON_BN_CLICKED(IDC_TBB_EXE_BROWSE_BTN, &CImageBuildDlg::OnBnClickedTbbExeBrowseBtn)
	ON_EN_CHANGE(IDC_HASH_KEY_FILE_PATH, &CImageBuildDlg::OnEnChangeHashKeyFilePath)
	ON_BN_CLICKED(IDC_PRIVATE_KEY_FILE_PATH_BROWSE_BTN, &CImageBuildDlg::OnBnClickedPrivateKeyFilePathBrowseBtn)
	ON_EN_CHANGE(IDC_PRIVATE_KEY_FILE_PATH, &CImageBuildDlg::OnEnChangePrivateKeyFilePath)
	ON_BN_CLICKED(IDC_HASH_KEY_FILE_PATH_BROWSE_BTN, &CImageBuildDlg::OnBnClickedHashKeyFilePathBrowseBtn)
	ON_EN_CHANGE(IDC_DIGITAL_SIGNATURE_KEY_FILE_PATH, &CImageBuildDlg::OnEnChangeDigitalSignatureFilePath)
	ON_BN_CLICKED(IDC_DIGITAL_SIGNATURE_KEY_FILE_PATH_BROWSE_BTN, &CImageBuildDlg::OnBnClickedDigitalSignatureFilePathBrowseBtn)
	ON_EN_CHANGE(IDC_TIM_INPUT_FILE_PATH, &CImageBuildDlg::OnEnChangeTimInputFilePath)
	ON_BN_CLICKED(IDC_TIM_INPUT_KEY_FILE_PATH_BROWSE_BTN, &CImageBuildDlg::OnBnClickedTimInputFilePathBrowseBtn)
	ON_EN_CHANGE(IDC_TIM_OUTPUT_FILE_PATH, &CImageBuildDlg::OnEnChangeTimOutputFilePath)
	ON_EN_CHANGE(IDC_RESERVED_FILE_PATH, &CImageBuildDlg::OnEnChangeReservedFilePath)
	ON_BN_CLICKED(IDC_TARGET_TRUSTED_CHK, &CImageBuildDlg::OnBnClickedTrustedChk)
	ON_BN_CLICKED(IDC_VERIFY_CHK, &CImageBuildDlg::OnBnClickedVerifyChk)
	ON_BN_CLICKED(IDC_VERBOSE_CHK, &CImageBuildDlg::OnBnClickedVerboseChk)
	ON_BN_CLICKED(IDC_CONCATENATE, &CImageBuildDlg::OnBnClickedConcatenate)
	ON_BN_CLICKED(IDC_TIM_OUTPUT_FILE_PATH_BROWSE_BTN, &CImageBuildDlg::OnBnClickedTimOutputFilePathBrowseBtn)
	ON_BN_CLICKED(IDC_RESERVED_FILE_PATH_BROWSE_BTN, &CImageBuildDlg::OnBnClickedReservedFilePathBrowseBtn)
	ON_BN_CLICKED(IDC_TARGET_TIM_FILE_PATH_BROWSE_BTN, &CImageBuildDlg::OnBnClickedTimDescriptorFilePathBrowseBtn)
	ON_EN_CHANGE(IDC_TARGET_TIM_FILE_PATH, &CImageBuildDlg::OnEnChangeTimDescriptorFilePath)
	ON_BN_CLICKED(IDC_HASH_KEY_FILE_CHK, &CImageBuildDlg::OnBnClickedHashKeyFileChk)
	ON_BN_CLICKED(IDC_PRIVATE_KEY_CHK, &CImageBuildDlg::OnBnClickedPrivateKeyChk)
	ON_BN_CLICKED(IDC_DIGITAL_SIGNATURE_CHK, &CImageBuildDlg::OnBnClickedDigitalSignatureChk)
	ON_BN_CLICKED(IDC_TIM_INPUT_FILE_CHK, &CImageBuildDlg::OnBnClickedTimInputFileChk)
	ON_BN_CLICKED(IDC_TIM_OUTPUT_FILE_CHK, &CImageBuildDlg::OnBnClickedTimOutputFileChk)
	ON_BN_CLICKED(IDC_RESERVED_FILE_CHK, &CImageBuildDlg::OnBnClickedReservedFileChk)
	ON_BN_CLICKED(IDC_TBB_RESET_BTN, &CImageBuildDlg::OnBnClickedTbbReset)
	ON_BN_CLICKED(ID_BUILD_IMAGE_BTN, &CImageBuildDlg::OnBnClickedImageBuildBtn)
	ON_BN_CLICKED(IDC_PARTITION_FILE_CHK, &CImageBuildDlg::OnBnClickedPartitionFileChk)
	ON_EN_CHANGE(IDC_PARTITION_FILE_PATH, &CImageBuildDlg::OnEnChangePartitionFilePath)
	ON_BN_CLICKED(IDC_PARTITION_FILE_PATH_BROWSE_BTN, &CImageBuildDlg::OnBnClickedPartitionFilePathBrowseBtn)
	ON_EN_CHANGE(IDC_BUILD_ADDITIONAL_OPTIONS_EDT, &CImageBuildDlg::OnEnChangeBuildAdditionalOptionsEdt)
	ON_BN_CLICKED(IDC_BUILD_ADDITIONAL_OPTIONS_CHK, &CImageBuildDlg::OnBnClickedBuildAdditionalOptionsChk)
	ON_BN_CLICKED(IDC_BUILD_PROCESSOR_TYPE_CHK, &CImageBuildDlg::OnBnClickedBuildProcessorTypeChk)
	ON_BN_CLICKED(IDC_PARTITION_TABLE_BTN, &CImageBuildDlg::OnBnClickedPartitionTableBtn)
	ON_BN_CLICKED(IDOK, &CImageBuildDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_ONENAND_PADDING_CHK, &CImageBuildDlg::OnBnClickedOnenandPaddingChk)
	ON_NOTIFY(UDN_DELTAPOS, IDC_ONENAND_PADDING_TIM_SIZE_SPIN, &CImageBuildDlg::OnDeltaposOnenandPaddingTimSizeSpin)
END_MESSAGE_MAP()


// CImageBuildDlg message handlers

void CImageBuildDlg::OnEnChangeTbbExePath()
{
	UpdateData( TRUE );
	m_ImageBuild.TbbExePath( string((CStringA)sTbbExePath) );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedTbbExeBrowseBtn()
{
	UpdateData( TRUE );
	CFileDialog Dlg( TRUE, _T("*.exe"), NULL, 
					 OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST,
					 _T("Executable (*.exe)|*.exe|All Files (*.*)|*.*||") );
	if ( Dlg.DoModal() == IDOK )
	{
		sTbbExePath = Dlg.GetPathName();
		m_ImageBuild.TbbExePath( string((CStringA)sTbbExePath) );
	}
	UpdateCommandLineText();
	SetFocus();
}


void CImageBuildDlg::OnBnClickedTimDescriptorFilePathBrowseBtn()
{
	UpdateData( TRUE );
	CFileDialog Dlg( TRUE, _T("*.txt"), NULL, 
					 //OFN_FILEMUSTEXIST | 
					 OFN_PATHMUSTEXIST,
					 _T("Text (*.txt)|*.txt|All Files (*.*)|*.*||") );
	if ( Dlg.DoModal() == IDOK )
	{
		sTimDescriptorFilePath = Dlg.GetPathName();
		m_ImageBuild.TimDescriptorFilePath( string((CStringA)sTimDescriptorFilePath) );
	}
	UpdateCommandLineText();
	SetFocus();
}


void CImageBuildDlg::OnEnChangeTimDescriptorFilePath()
{
	UpdateData( TRUE );
	m_ImageBuild.TimDescriptorFilePath( string((CStringA)sTimDescriptorFilePath) );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnEnChangeHashKeyFilePath()
{
	UpdateData( TRUE );
	m_ImageBuild.HashKeyFilePath( string((CStringA)sHashKeyFilePath) );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedHashKeyFilePathBrowseBtn()
{
	UpdateData( TRUE );
	CFileDialog Dlg( TRUE, _T("*.bin"), NULL, 
					 //OFN_FILEMUSTEXIST | 
					 OFN_PATHMUSTEXIST,
					 _T("Binary (*.bin)|*.bin|All Files (*.*)|*.*||") );
	if ( Dlg.DoModal() == IDOK )
	{
		sHashKeyFilePath = Dlg.GetPathName();
		m_ImageBuild.HashKeyFilePath( string((CStringA)sHashKeyFilePath) );
	}
	UpdateCommandLineText();
	SetFocus();
}

void CImageBuildDlg::OnEnChangePrivateKeyFilePath()
{
	UpdateData( TRUE );
	m_ImageBuild.PrivateKeyFilePath( string((CStringA)sPrivateKeyFilePath) );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedPrivateKeyFilePathBrowseBtn()
{
	UpdateData( TRUE );
	CFileDialog Dlg( TRUE, _T("*.txt"), NULL, 
					 //OFN_PATHMUSTEXIST | 
					 OFN_FILEMUSTEXIST,
					 _T("Binary (*.txt)|*.txt|All Files (*.*)|*.*||") );
	if ( Dlg.DoModal() == IDOK )
	{
		sPrivateKeyFilePath = Dlg.GetPathName();
		m_ImageBuild.PrivateKeyFilePath( string((CStringA)sPrivateKeyFilePath) );
	}
	UpdateCommandLineText();
	SetFocus();
}


void CImageBuildDlg::OnEnChangeDigitalSignatureFilePath()
{
	UpdateData( TRUE );
	m_ImageBuild.DigitalSignatureFilePath( string((CStringA)sDigitalSignatureFilePath) );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedDigitalSignatureFilePathBrowseBtn()
{
	UpdateData( TRUE );
	CFileDialog Dlg( TRUE, _T("*.bin"), NULL, 
					 //OFN_FILEMUSTEXIST | 
					 OFN_PATHMUSTEXIST,
					 _T("Binary (*.bin)|*.bin|All Files (*.*)|*.*||") );
	if ( Dlg.DoModal() == IDOK )
	{
		sDigitalSignatureFilePath = Dlg.GetPathName();
		m_ImageBuild.DigitalSignatureFilePath( string((CStringA)sDigitalSignatureFilePath) );
	}
	UpdateCommandLineText();
	SetFocus();
}

void CImageBuildDlg::OnEnChangeTimInputFilePath()
{
	UpdateData( TRUE );
	m_ImageBuild.TimInputFilePath( string((CStringA)sTimInputFilePath) );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedTimInputFilePathBrowseBtn()
{
	UpdateData( TRUE );
	CFileDialog Dlg( TRUE, _T("*.bin"), NULL, 
					 //OFN_FILEMUSTEXIST | 
					 OFN_PATHMUSTEXIST,
					 _T("Binary (*.bin)|*.bin|All Files (*.*)|*.*||") );
	if ( Dlg.DoModal() == IDOK )
	{
		sTimInputFilePath = Dlg.GetPathName();
		m_ImageBuild.TimInputFilePath( string((CStringA)sTimInputFilePath) );
	}
	UpdateCommandLineText();
	SetFocus();
}

void CImageBuildDlg::OnEnChangeTimOutputFilePath()
{
	UpdateData( TRUE );
	m_ImageBuild.TimOutputFilePath( string((CStringA)sTimOutputFilePath) );
	UpdateCommandLineText();
}


void CImageBuildDlg::OnBnClickedTimOutputFilePathBrowseBtn()
{
	UpdateData( TRUE );
	CFileDialog Dlg( TRUE, _T("*.bin"), NULL, 
					 //OFN_FILEMUSTEXIST | 
					 OFN_PATHMUSTEXIST,
					 _T("Binary (*.bin)|*.bin|All Files (*.*)|*.*||") );
	if ( Dlg.DoModal() == IDOK )
	{
		sTimOutputFilePath = Dlg.GetPathName();
		m_ImageBuild.TimOutputFilePath( string((CStringA)sTimOutputFilePath) );
	}
	UpdateCommandLineText();
	SetFocus();
}

void CImageBuildDlg::OnEnChangeReservedFilePath()
{
	UpdateData( TRUE );
	m_ImageBuild.ReservedFilePath( string((CStringA)sReservedFilePath) );
	UpdateCommandLineText();
}


void CImageBuildDlg::OnBnClickedReservedFilePathBrowseBtn()
{
	UpdateData( TRUE );
	CFileDialog Dlg( TRUE, _T("*.txt"), NULL, 
					 //OFN_FILEMUSTEXIST | 
					 OFN_PATHMUSTEXIST,
					 _T("Binary (*.txt)|*.txt|All Files (*.*)|*.*||") );
	if ( Dlg.DoModal() == IDOK )
	{
		sReservedFilePath = Dlg.GetPathName();
		m_ImageBuild.ReservedFilePath( string((CStringA)sReservedFilePath) );
	}
	UpdateCommandLineText();
	SetFocus();
}

void CImageBuildDlg::RefreshState()
{
	if ( m_hWnd )
	{
		UpdateData( TRUE );

		bTrusted = m_ImageBuild.Trusted() ? TRUE : FALSE;
		bVerify = m_ImageBuild.Verify() ? TRUE : FALSE;
		bVerboseMode = m_ImageBuild.VerboseMode() ? TRUE : FALSE;
		bConcatenate = m_ImageBuild.Concatenate() ? TRUE : FALSE;
		bOneNANDPadding = m_ImageBuild.OneNANDPadding() ? TRUE : FALSE;
		iOneNANDPaddingSize = m_ImageBuild.PaddedSize();
		bHashKey = m_ImageBuild.HashKeyFile() ? TRUE : FALSE;
		bPrivateKey = m_ImageBuild.PrivateKeyFile() ? TRUE : FALSE;
		bDigitalSignature = m_ImageBuild.DigitalSignatureFile() ? TRUE : FALSE;
		bTimInputFile = m_ImageBuild.TimInputFile() ? TRUE : FALSE;
		bTimOutputFile = m_ImageBuild.TimOutputFile() ? TRUE : FALSE;
		bReservedFile = m_ImageBuild.ReservedFile() ? TRUE : FALSE;
		bPartitionFile = m_ImageBuild.PartitionFile() ? TRUE : FALSE;
		sTbbExePath = m_ImageBuild.TbbExePath().c_str();
		sHashKeyFilePath = m_ImageBuild.HashKeyFilePath().c_str();
		sPrivateKeyFilePath = m_ImageBuild.PrivateKeyFilePath().c_str();
		sDigitalSignatureFilePath = m_ImageBuild.DigitalSignatureFilePath().c_str();
		sTimInputFilePath = m_ImageBuild.TimInputFilePath().c_str();
		sTimOutputFilePath = m_ImageBuild.TimOutputFilePath().c_str();
		sReservedFilePath = m_ImageBuild.ReservedFilePath().c_str();
		sTimDescriptorFilePath = m_ImageBuild.TimDescriptorFilePath().c_str();
		bProcessorType = m_ImageBuild.ProcessorTypeEnable() ? TRUE : FALSE;
		
		if ( bAdditionalOptions )
			sAdditionalOptions = m_ImageBuild.AdditionalOptions().c_str();
		
		if ( m_TimDescriptor.Version() >= "0x00030200" )
			sPartitionFilePath = m_ImageBuild.PartitionFilePath().c_str();

		UpdateCommandLineText();
		UpdateControls();
	}
}

void CImageBuildDlg::UpdateControls()
{
	BOOL bEnable = bTrusted;

	HashKeyFileChk.EnableWindow( bEnable );
	HashKeyFilePathEdt.EnableWindow( bEnable && bHashKey );
	HashKeyFilePathBrowseBtn.EnableWindow( bEnable && bHashKey );

	bEnable &= bHashKey;
	PrivateKeyChk.EnableWindow( bEnable );
	PrivateKeyFilePathEdt.EnableWindow( bEnable && bPrivateKey );
	PrivateKeyFilePathBrowseBtn.EnableWindow( bEnable && bPrivateKey );

	bEnable &= bPrivateKey;
	DigitalSignatureChk.EnableWindow( bEnable );
	DigitalSignatureFilePathEdt.EnableWindow( bEnable && bDigitalSignature );
	DigitalSignatureFilePathBrowseBtn.EnableWindow( bEnable && bDigitalSignature );

	bEnable &= bDigitalSignature;
	TimInputFileChk.EnableWindow( bEnable );
	TimInputFilePathEdt.EnableWindow( bEnable && bTimInputFile );
	TimInputFilePathBrowseBtn.EnableWindow( bEnable && bTimInputFile );

	bEnable &= bTimInputFile;
	TimOutputFileChk.EnableWindow( bEnable );
	TimOutputFilePathEdt.EnableWindow( bEnable && bTimOutputFile );
	TimOutputFilePathBrowseBtn.EnableWindow( bEnable && bTimOutputFile );

	ReservedFilePathEdt.EnableWindow( bReservedFile );
	ReservedFilePathBrowseBtn.EnableWindow( bReservedFile );

	bEnable = (m_TimDescriptor.Version() >= "0x00030200");
	PartitionFileChk.EnableWindow( bEnable );
	PartitionFilePathEdt.EnableWindow( bEnable && bPartitionFile );
	PartitionFilePathBtn.EnableWindow( bEnable && bPartitionFile );
	PartitionTableBtn.EnableWindow( bEnable && bPartitionFile );

	bool bValidCommandLine = (sCommandLineTextStr.Find('<') == -1 && sCommandLineTextStr.Find('>') == -1);
	bValidCommandLine = bValidCommandLine && !sTbbExePath.IsEmpty();
	BuildImageBtn.EnableWindow( bValidCommandLine );

	AdditionalOptionsEdt.EnableWindow( bAdditionalOptions );
	
	OneNANDPaddingSizeEdt.EnableWindow( bOneNANDPadding );
	OneNANDPaddingSizeSpin.EnableWindow( bOneNANDPadding );
}

void CImageBuildDlg::UpdateCommandLineText()
{
	sCommandLineTextStr = m_ImageBuild.CommandLineArgs(m_DownloaderInterface.TimDescriptorParser).c_str();
	UpdateControls();
	UpdateData( FALSE );
}

void CImageBuildDlg::OnBnClickedTrustedChk()
{
	// trusted can only be changed in the TIM Descriptor Dlg
	return;
}

void CImageBuildDlg::OnBnClickedVerifyChk()
{
	UpdateData( TRUE );
	m_ImageBuild.Verify( bVerify == TRUE ? true : false );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedVerboseChk()
{
	UpdateData( TRUE );
	m_ImageBuild.VerboseMode( bVerboseMode == TRUE ? true : false );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedConcatenate()
{
	UpdateData( TRUE );
	m_ImageBuild.Concatenate( bConcatenate == TRUE ? true : false );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedOnenandPaddingChk()
{
	UpdateData( TRUE );
	m_ImageBuild.OneNANDPadding( bOneNANDPadding == TRUE ? true : false );
	UpdateCommandLineText();
}

BOOL CImageBuildDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	RefreshState();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CImageBuildDlg::OnBnClickedHashKeyFileChk()
{
	UpdateData( TRUE );
	m_ImageBuild.HashKeyFile( bHashKey == TRUE ? true : false );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedPrivateKeyChk()
{
	UpdateData( TRUE );
	m_ImageBuild.PrivateKeyFile( bPrivateKey == TRUE ? true : false );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedDigitalSignatureChk()
{
	UpdateData( TRUE );
	m_ImageBuild.DigitalSignatureFile( bDigitalSignature == TRUE ? true : false );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedTimInputFileChk()
{
	UpdateData( TRUE );
	bTimOutputFile = bTimInputFile;
	m_ImageBuild.TimInputFile( bTimInputFile == TRUE ? true : false );
	m_ImageBuild.TimOutputFile( bTimOutputFile == TRUE ? true : false );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedTimOutputFileChk()
{
	UpdateData( TRUE );
	bTimInputFile = bTimOutputFile;
	m_ImageBuild.TimInputFile( bTimInputFile == TRUE ? true : false );
	m_ImageBuild.TimOutputFile( bTimOutputFile == TRUE ? true : false );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedReservedFileChk()
{
	UpdateData( TRUE );
	m_ImageBuild.ReservedFile( bReservedFile == TRUE ? true : false );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedTbbReset()
{
	m_ImageBuild.Reset();
	RefreshState();
}

void CImageBuildDlg::OnBnClickedImageBuildBtn()
{
	if ( m_TimDescriptor.IsChanged() 
		&& m_TimDescriptor.IsNotWritten() )
	{
		if ( !m_TimDescriptor.WriteTimDescriptorFile() )
		{
			AfxMessageBox( "Failed to Write TIM text file before building project." );
			return;
		}
	}

	if ( m_ImageBuild.Launch() )
		OnOK();
}

void CImageBuildDlg::OnBnClickedPartitionFileChk()
{
	UpdateData( TRUE );
	m_ImageBuild.PartitionFile( bPartitionFile == TRUE ? true : false );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnEnChangePartitionFilePath()
{
	UpdateData( TRUE );
	m_ImageBuild.PartitionFilePath( string((CStringA)sPartitionFilePath) );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedPartitionFilePathBrowseBtn()
{
	UpdateData( TRUE );
	CFileDialog Dlg( TRUE, _T("*.txt"), NULL, 
					 OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST,
					 _T("Text (*.txt)|*.txt|All Files (*.*)|*.*||") );
	if ( Dlg.DoModal() == IDOK )
	{
		sPartitionFilePath = Dlg.GetPathName();
		m_ImageBuild.PartitionFilePath( string((CStringA)sPartitionFilePath) );
	}
	UpdateCommandLineText();
	SetFocus();
}

void CImageBuildDlg::OnEnChangeBuildAdditionalOptionsEdt()
{
	UpdateData( TRUE );
	m_ImageBuild.AdditionalOptions( string((CStringA)sAdditionalOptions) );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedBuildAdditionalOptionsChk()
{
	UpdateData( TRUE );
	m_ImageBuild.AdditionalOptionsEnable( bAdditionalOptions == TRUE ? true : false );
	UpdateCommandLineText();
}

void CImageBuildDlg::OnBnClickedBuildProcessorTypeChk()
{
	UpdateData( TRUE );
	m_ImageBuild.ProcessorTypeEnable( bProcessorType == TRUE ? true : false );
	UpdateCommandLineText();
}


void CImageBuildDlg::OnBnClickedPartitionTableBtn()
{
	CPartitionTableDlg Dlg(m_DownloaderInterface);
	Dlg.PartitionTable() = m_ImageBuild.PartitionTable();
	if ( Dlg.DoModal() == IDOK )
	{
		if ( Dlg.PartitionTable().IsChanged() )
		{
			m_ImageBuild.PartitionTable() = Dlg.PartitionTable();
			sPartitionFilePath = Dlg.PartitionTable().PartitionFilePath().c_str();
			m_ImageBuild.PartitionFilePath( Dlg.PartitionTable().PartitionFilePath() );
		}
	}
	UpdateCommandLineText();
	SetFocus();
}

void CImageBuildDlg::UpdateChangeMarkerInTitle()
{
	if ( m_hWnd == 0 )
		return;

	CString sTitle;
	GetWindowTextA( sTitle );
	// add or remove * as appropriate
	if ( m_ImageBuild.IsChanged() )
	{
		if ( -1 == sTitle.Find('*') )
			sTitle += "*";
	}
	else
	{
		if ( -1 != sTitle.Find('*') )
			sTitle.Remove('*');
	}
	SetWindowTextA(sTitle);
	UpdateWindow();
}

void CImageBuildDlg::OnBnClickedOk()
{
	OnOK();
}


void CImageBuildDlg::OnDeltaposOnenandPaddingTimSizeSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	if ( pNMUpDown->iDelta < 0 )
	{
		if ( iOneNANDPaddingSize >= 8192 )
		{
			iOneNANDPaddingSize = 8192;
			*pResult = 1;
		}
		else
			iOneNANDPaddingSize += 256;
	}
	else
		if ( iOneNANDPaddingSize <= 0 )
		{
			iOneNANDPaddingSize = 0;
			*pResult = 1;
		}
		else
			iOneNANDPaddingSize -= 256;

	m_ImageBuild.PaddedSize( iOneNANDPaddingSize );
	UpdateCommandLineText();
}
