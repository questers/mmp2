/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
// PartitionTableDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "PartitionTableDlg.h"
#include "PartitionDlg.h"
#include "DownloaderInterface.h"

// CPartitionTableDlg dialog

IMPLEMENT_DYNAMIC(CPartitionTableDlg, CDialog)

CPartitionTableDlg::CPartitionTableDlg(CDownloaderInterface& rDownloaderInterface, CWnd* pParent /*=NULL*/)
	: CDialog(CPartitionTableDlg::IDD, pParent)
	, m_DownloaderInterface( rDownloaderInterface )
{
	sPartitionTextFilePath = _T("");
	sPartitionVersion = _T("");
}

CPartitionTableDlg::~CPartitionTableDlg()
{
}

void CPartitionTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PARTITION_VERSION_EDT, PartitionTableVersion);
	DDX_Control(pDX, IDC_PARTITIONS_LIST, PartitionsListCtrl);
	DDX_Control(pDX, IDC_PARTITION_MOVE_UP_BTN, MoveUpBtn);
	DDX_Control(pDX, IDC_PARTITION_MOVE_DOWN_BTN, MoveDownBtn);
	DDX_Control(pDX, IDC_DELETE_PARTITION_BTN, DeletePartitionBtn);
	DDX_Text(pDX, IDC_PARTITION_TEXT_FILE_PATH_EDT, sPartitionTextFilePath);
	DDX_Text(pDX, IDC_PARTITION_VERSION_EDT, sPartitionVersion);

	// updates marker in title
	UpdateChangeMarkerInTitle();
	theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CPartitionTableDlg, CDialog)
	ON_BN_CLICKED(IDC_INSERT_PARTITION_BTN, &CPartitionTableDlg::OnBnClickedInsertPartitionBtn)
	ON_BN_CLICKED(IDC_PARTITION_MOVE_UP_BTN, &CPartitionTableDlg::OnBnClickedPartitionMoveUpBtn)
	ON_BN_CLICKED(IDC_PARTITION_MOVE_DOWN_BTN, &CPartitionTableDlg::OnBnClickedPartitionMoveDownBtn)
	ON_BN_CLICKED(IDC_DELETE_PARTITION_BTN, &CPartitionTableDlg::OnBnClickedDeletePartitionBtn)
	ON_EN_CHANGE(IDC_PARTITION_VERSION_EDT, &CPartitionTableDlg::OnEnChangePartitionVersionEdt)
	ON_EN_CHANGE(IDC_PARTITION_TEXT_FILE_PATH_EDT, &CPartitionTableDlg::OnEnChangePartitionTextFilePathEdt)
	ON_BN_CLICKED(IDC_PARTITION_BROWSE_BTN, &CPartitionTableDlg::OnBnClickedPartitionBrowseBtn)
	ON_BN_CLICKED(IDC_PARTITION_READ_BTN, &CPartitionTableDlg::OnBnClickedPartitionReadBtn)
	ON_BN_CLICKED(IDC_PARTITION_WRITE_BTN, &CPartitionTableDlg::OnBnClickedPartitionWriteBtn)
	ON_BN_CLICKED(IDC_EDIT_PARTITION_BTN, &CPartitionTableDlg::OnBnClickedEditPartitionBtn)
	ON_BN_CLICKED(IDOK, &CPartitionTableDlg::OnBnClickedOk)
	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()


// CPartitionTableDlg message handlers
BOOL CPartitionTableDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	PartitionsListCtrl.SetExtendedStyle(LVS_EX_TRACKSELECT
								  //  LVS_EX_BORDERSELECT
								  | LVS_EX_FULLROWSELECT 
								  | LVS_EX_HEADERDRAGDROP 
								  | LVS_EX_ONECLICKACTIVATE 
//								  | LVS_EX_LABELTIP 
								  //| LVS_EX_GRIDLINES
								  );

	PartitionsListCtrl.InsertColumn(0,"Partition ID", LVCFMT_LEFT, 120, -1 );
	PartitionsListCtrl.InsertColumn(1,"Attributes", LVCFMT_LEFT, 120, -1 );
	PartitionsListCtrl.InsertColumn(2,"Usage", LVCFMT_LEFT, 120, -1 );
	PartitionsListCtrl.InsertColumn(3,"Type", LVCFMT_LEFT, 120, -1 );

	PartitionTableVersion.m_bHexOnly = true;

	RefreshState();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CPartitionTableDlg::RefreshState()
{
	CTimLib TimLib;
	PartitionsListCtrl.DeleteAllItems();

	sPartitionTextFilePath = m_PartitionTable.PartitionFilePath().c_str();
	sPartitionVersion = TimLib.HexFormattedAscii(m_PartitionTable.PartitionTableVersion()).c_str();

	t_PartitionList& Partitions = m_PartitionTable.Partitions();
	t_PartitionListIter iter = Partitions.begin();
	int nItem = 0;
	while( iter != Partitions.end() )
	{
		CPartitionData* pPartitionData = (*iter);
		PartitionsListCtrl.InsertItem(nItem, TimLib.HexFormattedAscii(pPartitionData->PartitionId()).c_str());
		PartitionsListCtrl.SetItemData(nItem,(DWORD_PTR)pPartitionData);  // item retains image*
		PartitionsListCtrl.SetItem(nItem,1,LVIF_TEXT,TimLib.HexFormattedAscii(pPartitionData->PartitionAttributes()).c_str(),0,0,0,0);
		PartitionsListCtrl.SetItem(nItem,2,LVIF_TEXT,pPartitionData->PartitionUsage().c_str(),0,0,0,0);
		PartitionsListCtrl.SetItem(nItem,3,LVIF_TEXT,pPartitionData->PartitionType().c_str(),0,0,0,0);
		
		nItem++;
		iter++;
	}

	UpdateData( FALSE );
}


void CPartitionTableDlg::OnBnClickedInsertPartitionBtn()
{
	UpdateData( TRUE );

	int nItem = PartitionsListCtrl.GetSelectionMark();
	if ( nItem == -1 )
		// nothing selected 
		nItem = PartitionsListCtrl.GetItemCount();
	else
		nItem++;	// insert after selected item

	CPartitionDlg PartitionDlg;
	if ( IDOK == PartitionDlg.DoModal() )
	{	
		t_PartitionList& Partitions = m_PartitionTable.Partitions();
		t_PartitionListIter iter = Partitions.begin();
		int ItemPos = nItem-1;
		while ( iter != Partitions.end() && ItemPos >= 0 )
		{
			ItemPos--;
			iter++;
		}
		CPartitionData* pPartition = new CPartitionData( PartitionDlg.PartitionData() );
		Partitions.insert( iter, pPartition );
		PartitionsListCtrl.SetItemData( nItem, (DWORD_PTR)pPartition );
		
		RefreshState();
	}
	SetFocus();
}

void CPartitionTableDlg::OnBnClickedPartitionMoveUpBtn()
{
	int nItem = PartitionsListCtrl.GetSelectionMark();
	if ( nItem > 0 )
	{
		CPartitionData* pPartitionData = (CPartitionData*)PartitionsListCtrl.GetItemData( nItem );
		t_PartitionList& Partitions = m_PartitionTable.Partitions();

		//put key in correct position in list
		t_PartitionListIter iter = Partitions.begin();
		while( iter != Partitions.end() )
		{
			if ( *iter == pPartitionData )
			{
				// move it up unless already at top of list
				if ( iter != Partitions.begin() )
				{
					t_PartitionListIter insertiter = --iter;

					Partitions.remove( pPartitionData );
					Partitions.insert( insertiter, pPartitionData );
					PartitionsListCtrl.SetSelectionMark( --nItem );
				}
				break;
			}
			iter++;
		}
	}

	RefreshState();
}

void CPartitionTableDlg::OnBnClickedPartitionMoveDownBtn()
{
	int nItem = PartitionsListCtrl.GetSelectionMark();
	if ( nItem != -1 )
	{
		CPartitionData* pPartitionData = (CPartitionData*)PartitionsListCtrl.GetItemData( nItem );
		t_PartitionList& Partitions = m_PartitionTable.Partitions();

		//put key in correct position in list
		t_PartitionListIter iter = Partitions.begin();
		while( iter != Partitions.end() )
		{
			if ( *iter == pPartitionData )
			{
				// move it down unless already at end of list
				if ( ++iter != Partitions.end() )
				{
					Partitions.remove( pPartitionData );
					Partitions.insert( ++iter, pPartitionData );
					PartitionsListCtrl.SetSelectionMark( ++nItem );
				}
				break;
			}
			iter++;
		}
	}

	RefreshState();
}

void CPartitionTableDlg::OnBnClickedDeletePartitionBtn()
{
	POSITION Pos = PartitionsListCtrl.GetFirstSelectedItemPosition();
	if ( Pos )
	{
		int nItem = PartitionsListCtrl.GetNextSelectedItem(Pos);
		CPartitionData* pPartitionData = (CPartitionData*)PartitionsListCtrl.GetItemData( nItem );
		if ( pPartitionData )
		{
			m_PartitionTable.Partitions().remove(pPartitionData);
			delete pPartitionData;
		}
	}
	RefreshState();
}

void CPartitionTableDlg::OnEnChangePartitionVersionEdt()
{
	UpdateData( TRUE );
	m_PartitionTable.PartitionTableVersion(atoi(sPartitionVersion));
	UpdateData(FALSE);
}

void CPartitionTableDlg::OnEnChangePartitionTextFilePathEdt()
{
	UpdateData( TRUE );
	m_PartitionTable.PartitionFilePath( string((CStringA)sPartitionTextFilePath) );
	UpdateData(FALSE);
}

void CPartitionTableDlg::OnBnClickedPartitionBrowseBtn()
{
	UpdateData( TRUE );
	CFileDialog Dlg( TRUE, _T("*.txt"), NULL, 
					 //OFN_FILEMUSTEXIST | 
					 OFN_PATHMUSTEXIST,
					 _T("Text (*.txt)|*.txt|All Files (*.*)|*.*||") );
	if ( Dlg.DoModal() == IDOK )
	{
		sPartitionTextFilePath = Dlg.GetPathName();
		m_PartitionTable.PartitionFilePath( string((CStringA)sPartitionTextFilePath) );
	}

	UpdateData(FALSE);
	SetFocus();
}

void CPartitionTableDlg::OnBnClickedPartitionReadBtn()
{
	if ( m_PartitionTable.ParsePartitionTextFile(m_DownloaderInterface) )
		RefreshState();
}

void CPartitionTableDlg::OnBnClickedPartitionWriteBtn()
{
	UpdateData( TRUE );
	string sFileContents = m_PartitionTable.GetText();

	CFileDialog Dlg( FALSE, _T("*.txt"), NULL, 
					 //OFN_FILEMUSTEXIST | 
					 OFN_PATHMUSTEXIST,
					 _T("Text (*.txt)|*.txt|All Files (*.*)|*.*||") );
	if ( Dlg.DoModal() == IDOK )
	{
		sPartitionTextFilePath = Dlg.GetPathName();
		ofstream ofsPartitionTextFile;
		ofsPartitionTextFile.open( sPartitionTextFilePath, ios_base::out | ios_base::trunc );
		if ( !ofsPartitionTextFile.fail() && !ofsPartitionTextFile.bad() )
			ofsPartitionTextFile << sFileContents << endl;
		ofsPartitionTextFile.close();

		m_PartitionTable.NotWritten(false);
	}
	RefreshState();
	SetFocus();
}


void CPartitionTableDlg::OnBnClickedEditPartitionBtn()
{
	UpdateData( TRUE );

	int nItem = PartitionsListCtrl.GetSelectionMark();
	if ( nItem != -1 )
	{
		CPartitionData* pPartition = (CPartitionData*)PartitionsListCtrl.GetItemData( nItem );
		CPartitionDlg PartitionDlg;
		PartitionDlg.PartitionData() = *pPartition;
		if ( IDOK == PartitionDlg.DoModal() )
		{	
			*pPartition = PartitionDlg.PartitionData();
		}
	}
	RefreshState();
	SetFocus();
}

void CPartitionTableDlg::UpdateChangeMarkerInTitle()
{
	if ( m_hWnd == 0 )
		return;

	CString sTitle;
	GetWindowTextA( sTitle );
	// add or remove * as appropriate
	if ( m_PartitionTable.IsChanged() )
	{
		if ( -1 == sTitle.Find('*') )
			sTitle += "*";
	}
	else
	{
		if ( -1 != sTitle.Find('*') )
			sTitle.Remove('*');
	}
	SetWindowTextA(sTitle);
	UpdateWindow();
}

void CPartitionTableDlg::OnBnClickedOk()
{
	if ( m_PartitionTable.IsChanged() && m_PartitionTable.IsNotWritten() )
		OnBnClickedPartitionWriteBtn();

	OnOK();
}

void CPartitionTableDlg::OnContextMenu(CWnd* pWnd, CPoint point)
{
#if 0
	// this is just a place holder implementation
	// since the partition data is not in the TIM text file, there is
	// not yet a need to support comments on specific fields.
	// may revisit this later.
	CCommentDlg Dlg(this);
	int nItem = -1;
	int iCtlId = pWnd->GetDlgCtrlID();

	switch ( iCtlId )
	{
		case IDC_PARTITION_VERSION_EDT:
		case IDC_PARTITION_TEXT_FILE_PATH_EDT:
		case IDC_PARTITIONS_LIST:
			break;

		default:
			break;
	}
#else
	CDialog::OnContextMenu( pWnd, point );
#endif
}