/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "DownloaderInterface.h"
#include "EditListCtrl.h"


// CFBFDownloadPropertyPage dialog

class CFBFDownloadPropertyPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CFBFDownloadPropertyPage)

public:
	CFBFDownloadPropertyPage(CDownloaderInterface& rDownloaderInterface);
	virtual ~CFBFDownloadPropertyPage();

// Dialog Data
	enum { IDD = IDD_FBF_DOWNLOADER_PROPPAGE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL OnSetActive();

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnEnChangeUartDelay();
	afx_msg void OnEnChangeUsbPacketSize();
	afx_msg void OnBnClickedVerboseModeChk();
	afx_msg void OnCbnSelchangeFlashTypeCombo();
	afx_msg void OnBnClickedRunDebugBootCmdChk();
	afx_msg void OnEnChangeWtptpExePath();
	afx_msg void OnCbnSelchangeMsgModeCb();
	afx_msg void OnCbnSelchangeComPortCb();
	afx_msg void OnCbnSelchangeBaudRateCb();
	afx_msg void OnCbnSelchangePortTypeCb();
	afx_msg void OnBnClickedWtptpExeBrowseBtn();
	afx_msg void OnBnClickedWtptpResetBtn();
	afx_msg void OnBnClickedFbfFileBrowseBtn();
	afx_msg void OnEnChangeFbfFilePath();
	afx_msg void OnEnChangeWtptpAdditionalOptionsEdt();
	afx_msg void OnBnClickedWtptpDownloadBtn();
	afx_msg void OnBnClickedWtptpDownloadStopBtn();
	afx_msg void OnBnClickedDownloaderAdditionalOptionsChk();
	afx_msg void OnBnClickedUsbDownloadBtn();
	afx_msg void OnBnClickedDownloadProcessorTypeChk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedFbfMakeConfigBtn();
    afx_msg void OnBnClickedUploadSpecFileChk();
    afx_msg void OnEnChangeUploadSpecFilePathEdt();
    afx_msg void OnBnClickedUploadSpecFileBrowseBtn();
		
	BOOL bVerboseMode;  // adds printing of additional debug messages
	BOOL bRunDebugCmd;
	BOOL bAdditionalOptions;
	BOOL bProcessorType;
    BOOL bUploadSpecFile;

    CString sUartDelayMs;
	CString sJtagKeyFilePath;
	CString sWtptpExePath;
	CString sFbfFilePath;
	CString sUsbPacketSize;
    CString sUploadSpecFilePath;
	
	CComboBox FlashTypeCB;	// FlashType code for supported flashes
	CComboBox MsgModeCB;
	CComboBox PortTypeCB;
	CComboBox ComPortCB;
	CComboBox BaudRateCB;
	
    CEdit FbfFilePathEdt;
	CEdit UartDelayEdit;
	CEdit AdditionalOptionsEdt;
	CEdit UsbPacketSizeEdt;
    CEdit UploadSpecFilePathEdt;

    CButton FbfFilePathBrowseBtn;
    CButton UartDownloadBtn;
	CButton UartStopDownloadBtn;
	CButton UsbDownloadBtn;
    CButton UploadSpecFilePathBrowseBtn;

public:
	void RefreshState();
	void UpdateChangeMarkerInTitle();

private:
	void UpdateCommandLineText();
	void UpdateControls();

private:
	CDownloaderInterface&	DownloaderInterface;
	CString					sCommandLineTextStr;  // displays command line text for use in launching WPTPT.exe
	CString					sAdditionalOptions;
};
