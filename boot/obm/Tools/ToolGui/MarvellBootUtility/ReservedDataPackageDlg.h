/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "afxwin.h"
#include "EditListCtrl.h"
#include "ReservedPackageData.h"
#include "MyEdit.h"
#include "MyComboBox.h"

// CReservedDataPackageDlg dialog

class CReservedDataPackageDlg : public CDialog
{
	DECLARE_DYNAMIC(CReservedDataPackageDlg)

public:
	CReservedDataPackageDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CReservedDataPackageDlg();

// Dialog Data
	enum { IDD = IDD_RESERVED_DATA_PACKAGE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

public:
	CReservedPackageData& PackageData() { return m_PackageData; }

public:
	CMyComboBox PackageHeaderIdCb;
	CEditListCtrl PackageDataListCtrl;
	CString sPackageHeaderIdTag;
	CMyEdit PackageHeaderIdTagEdt;

	CButton PackageInsertItemBtn;
	CButton PackageDeleteItemBtn;
	CButton MoveUpBtn;
	CButton MoveDownBtn;

	afx_msg void OnBnClickedOk();
	afx_msg void OnEnChangePackageHeaderIdTagEdt();
	afx_msg void OnCbnSelchangePackageHeaderId();
	afx_msg void OnCbnEditchangePackageHeaderId();
	afx_msg void OnBnClickedPackageDeleteItem();
	afx_msg void OnBnClickedPackageInsertItemBtn();
	afx_msg void OnBnClickedPackageDataMoveUpBtn();
	afx_msg void OnBnClickedPackageDataMoveDownBtn();
	afx_msg void OnLvnItemActivatePackageDataListCtrl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMKillfocusPackageDataListCtrl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint /*point*/);

public:
	void UpdateChangeMarkerInTitle();

private:
	CReservedPackageData m_PackageData;
	int iCurSel;

	void UpdatePackageData();
	void UpdateControls();

	void PredefinedPackageInit();
	void AddItemFunctionToList();  // adds <add item> at bottom of col 0 if needed
public:
	afx_msg void OnBnClickedCancel();
};
