/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
// DDROperationsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "DDROperationsDlg.h"
#include "TimDescriptor.h"


// CDDROperationsDlg dialog

IMPLEMENT_DYNAMIC(CDDROperationsDlg, CDialog)

CDDROperationsDlg::CDDROperationsDlg(CTimDescriptor& TimDescriptor, t_DDROperationList& DdrOperations, CWnd* pParent /*=NULL*/)
	: CDialog(CDDROperationsDlg::IDD, pParent),
	m_LocalTimDescriptor( TimDescriptor ),
	m_DdrOperations( DdrOperations )
{
	m_OpCbSel = CB_ERR;
	m_bRefreshState = false;
}

CDDROperationsDlg::~CDDROperationsDlg()
{
}

void CDDROperationsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OPERATIONS_LIST, OperationsListCtrl);
	DDX_Control(pDX, IDC_INSERT_BTN, InsertBtn);
	DDX_Control(pDX, IDC_REMOVE_BTN, RemoveBtn);
	DDX_Control(pDX, IDC_MOVE_UP, MoveUpBtn);
	DDX_Control(pDX, IDC_MOVE_DOWN, MoveDownBtn);
	DDX_Control(pDX, IDC_OPERATION_ID_CB, OperationIdCb);
	DDX_Control(pDX, IDC_OPERATION_VALUE_EDT, OperationValueEdt);
	DDX_Control(pDX, IDC_OPERATION_ID_TAG_EDT, OperationIDTagEdt);

	// updates marker in title
	UpdateChangeMarkerInTitle();
	theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CDDROperationsDlg, CDialog)
	ON_BN_CLICKED(IDC_INSERT_BTN, &CDDROperationsDlg::OnBnClickedInsertBtn)
	ON_BN_CLICKED(IDC_REMOVE_BTN, &CDDROperationsDlg::OnBnClickedRemoveBtn)
	ON_BN_CLICKED(IDC_MOVE_UP, &CDDROperationsDlg::OnBnClickedMoveUp)
	ON_BN_CLICKED(IDC_MOVE_DOWN, &CDDROperationsDlg::OnBnClickedMoveDown)
	ON_CBN_SELCHANGE(IDC_OPERATION_ID_CB, &CDDROperationsDlg::OnCbnSelchangeOperationIdCb)
	ON_EN_CHANGE(IDC_OPERATION_VALUE_EDT, &CDDROperationsDlg::OnEnChangeOperationValueEdt)
	ON_BN_CLICKED(IDOK, &CDDROperationsDlg::OnBnClickedOk)
	ON_NOTIFY(LVN_ITEMACTIVATE, IDC_OPERATIONS_LIST, &CDDROperationsDlg::OnLvnItemActivateOperationsList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_OPERATIONS_LIST, &CDDROperationsDlg::OnLvnItemChangedOperationsList)
	ON_NOTIFY(NM_CLICK, IDC_OPERATIONS_LIST, &CDDROperationsDlg::OnNMClickOperationsList)
END_MESSAGE_MAP()


// CDDROperationsDlg message handlers

BOOL CDDROperationsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	OperationsListCtrl.SetExtendedStyle( //LVS_EX_TRACKSELECT |
									  //  LVS_EX_BORDERSELECT |
									    LVS_EX_FULLROWSELECT 
									  | LVS_EX_HEADERDRAGDROP 
									  | LVS_EX_ONECLICKACTIVATE 
									  | LVS_EX_LABELTIP 
									  //| LVS_EX_GRIDLINES
									 );

	OperationsListCtrl.InsertColumn(0,"OpId", LVCFMT_LEFT, 175, -1 );
	OperationsListCtrl.InsertColumn(1,"Value", LVCFMT_LEFT, 80, -1 );

	OperationIdCb.InsertString(0, "NOP" );
	OperationIdCb.InsertString(1, "DDR_INIT_ENABLE" );
	OperationIdCb.InsertString(2, "DDR_MEMTEST_ENABLE" );
	OperationIdCb.InsertString(3, "DDR_MEMTEST_START_ADDR" );
	OperationIdCb.InsertString(4, "DDR_MEMTEST_SIZE" );
	OperationIdCb.InsertString(5, "DDR_INIT_LOOP_COUNT" );
	OperationIdCb.InsertString(6, "DDR_IGNORE_INST_TO" );

	OperationValueEdt.m_bHexOnly = true;
	
	RefreshState();
	UpdateControls();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDDROperationsDlg::RefreshState()
{
	m_bRefreshState = true;

	int iSel = OperationsListCtrl.GetSelectionMark();
	OperationsListCtrl.DeleteAllItems();

	int nItem = 0;
	t_DDROperationListIter Iter = m_DdrOperations.begin();
	while ( Iter != m_DdrOperations.end() )
	{
		OperationsListCtrl.InsertItem( nItem, (*Iter)->m_sOpIdText.c_str() );
		OperationsListCtrl.SetItemData( nItem, (DWORD_PTR)(*Iter) );
		OperationsListCtrl.SetItemText( nItem, 1, ((*Iter)->HexFormattedAscii((*Iter)->m_Value)).c_str() );

		nItem++;
		Iter++;
	}

	if ( iSel != LB_ERR )
		OperationsListCtrl.SetSelectionMark( iSel );

	m_bRefreshState = false;
}

void CDDROperationsDlg::RefreshOperationState()
{
	if ( !m_bRefreshState )
	{
		int iSel = OperationsListCtrl.GetSelectionMark();
		if ( iSel != LB_ERR )
		{
			CDDROperation* pDDROp = (CDDROperation*)OperationsListCtrl.GetItemData( iSel );
			if ( pDDROp )
			{
				if ( CB_ERR != OperationIdCb.SetCurSel(OperationIdCb.FindStringExact( -1, pDDROp->m_sOpIdText.c_str() )))
				{
					m_OpCbSel = OperationIdCb.GetCurSel();
					OperationIdCb.SetItemDataPtr( m_OpCbSel, pDDROp );

					OperationIDTagEdt.SetWindowTextA( pDDROp->HexFormattedAscii( pDDROp->m_OperationId ).c_str() );
					OperationValueEdt.SetWindowTextA( pDDROp->HexFormattedAscii( pDDROp->m_Value).c_str() );
				}
			}

			OperationsListCtrl.SetSelectionMark( iSel );	
		}

		UpdateControls();
	}
}

void CDDROperationsDlg::UpdateControls()
{
	CDDROperation* pDDROp = 0;
	int iSel = OperationsListCtrl.GetSelectionMark();
	if ( iSel != LB_ERR )
		pDDROp = (CDDROperation*)OperationsListCtrl.GetItemData( iSel );
	
	InsertBtn.EnableWindow(TRUE);
	RemoveBtn.EnableWindow( iSel != LB_ERR && OperationsListCtrl.GetItemCount() > 0 );
	MoveUpBtn.EnableWindow( iSel != LB_ERR && OperationsListCtrl.GetItemCount() > 1 );
	MoveDownBtn.EnableWindow( iSel != LB_ERR && OperationsListCtrl.GetItemCount() > 1 );

	OperationIdCb.EnableWindow( iSel != LB_ERR && pDDROp != 0 );
	OperationValueEdt.EnableWindow( iSel != LB_ERR && pDDROp != 0 );

	if ( iSel == LB_ERR && pDDROp == 0 )
	{
		OperationIdCb.SetCurSel( CB_ERR );
		m_OpCbSel = CB_ERR;
		OperationIDTagEdt.SetWindowTextA(_T(""));
		OperationValueEdt.SetWindowTextA(_T(""));
	}
}

void CDDROperationsDlg::OnBnClickedInsertBtn()
{
	UpdateData( TRUE );

	int nItem = OperationsListCtrl.GetSelectionMark();
	if ( nItem == LB_ERR )
		// nothing selected 
		nItem = OperationsListCtrl.GetItemCount();
	else
		nItem++;	// insert after selected item

	t_DDROperationListIter iter = m_DdrOperations.begin();
	int ItemPos = nItem-1;
	while ( iter != m_DdrOperations.end() && ItemPos >= 0 )
	{
		ItemPos--;
		iter++;
	}

	CDDROperation* pDDROp = new CDDROperation( DDR_NOP, string("NOP") );
	m_DdrOperations.insert( iter, pDDROp );

//	OperationsListCtrl.InsertItem( nItem, pDDROp->m_sOpIdText.c_str() );
//	OperationsListCtrl.SetItemData( nItem, (DWORD_PTR)pDDROp );
		
	RefreshState();
	RefreshOperationState();
}

void CDDROperationsDlg::OnBnClickedRemoveBtn()
{
	int nItem = OperationsListCtrl.GetSelectionMark();
	if ( nItem != LB_ERR )
	{
		CDDROperation* pDDROp = (CDDROperation*)OperationsListCtrl.GetItemData( nItem );
		if ( pDDROp )
		{
			m_DdrOperations.remove(pDDROp);
			delete pDDROp;
		}
	}

	RefreshState();
	RefreshOperationState();
}

void CDDROperationsDlg::OnBnClickedMoveUp()
{
	int nItem = OperationsListCtrl.GetSelectionMark();
	if ( nItem > LB_ERR )
	{
		CDDROperation* pDDROp = (CDDROperation*)OperationsListCtrl.GetItemData( nItem );
		if ( pDDROp )
		{
			t_DDROperationListIter iter = m_DdrOperations.begin();
			while ( iter != m_DdrOperations.end() )
			{
				if ( *iter == pDDROp )
				{
					// move it up unless already at top of list
					if ( iter != m_DdrOperations.begin() )
					{
						t_DDROperationListIter insertiter = --iter;

						m_DdrOperations.remove( pDDROp );
						m_DdrOperations.insert( insertiter, pDDROp );
						OperationsListCtrl.SetSelectionMark( --nItem );
					}
					break;
				}
				iter++;
			}
		}
	}

	RefreshState();
	RefreshOperationState();
}

void CDDROperationsDlg::OnBnClickedMoveDown()
{
	int nItem = OperationsListCtrl.GetSelectionMark();
	if ( nItem != LB_ERR )
	{
		CDDROperation* pDDROp = (CDDROperation*)OperationsListCtrl.GetItemData( nItem );
		if ( pDDROp )
		{
			t_DDROperationListIter iter = m_DdrOperations.begin();
			while ( iter != m_DdrOperations.end() )
			{
				if ( *iter == pDDROp )
				{
					if ( ++iter != m_DdrOperations.end() )
					{
						m_DdrOperations.remove( pDDROp );
						m_DdrOperations.insert( ++iter, pDDROp );
						OperationsListCtrl.SetSelectionMark( ++nItem );
					}
					break;
				}
				iter++;
			}
		}
	}

	RefreshState();
	RefreshOperationState();
}

void CDDROperationsDlg::OnCbnSelchangeOperationIdCb()
{
	UpdateData(TRUE);
	CString sText;
	OperationIdCb.GetWindowTextA( sText );
	CDDROperation* pDDROp = (CDDROperation*)OperationIdCb.GetItemDataPtr( m_OpCbSel );	
	if ( pDDROp )
	{
		string sID = (CStringA)sText;
		pDDROp->SetOperationID( sID );
	}
	m_OpCbSel = OperationIdCb.GetCurSel(); 

	RefreshState();
	RefreshOperationState();
}

void CDDROperationsDlg::OnEnChangeOperationValueEdt()
{
	UpdateData( TRUE );

	int iSel = OperationsListCtrl.GetSelectionMark();
	if ( iSel != LB_ERR )
	{
		CDDROperation* pDDROp = (CDDROperation*)OperationsListCtrl.GetItemData( iSel );
		if ( pDDROp )
		{ 
			CString sVal;
			OperationValueEdt.GetWindowTextA( sVal );
			pDDROp->m_Value = pDDROp->Translate( (CStringA)sVal );
			OperationsListCtrl.SetItemText( iSel, 2, sVal );
		}
	}
}

void CDDROperationsDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	OnOK();
}

void CDDROperationsDlg::UpdateChangeMarkerInTitle()
{
	if ( m_hWnd == 0 )
		return;

	CString sTitle;
	GetWindowTextA( sTitle );
	// add or remove * as appropriate
	if ( m_LocalTimDescriptor.ReservedIsChanged() )
	{
		if ( -1 == sTitle.Find('*') )
			sTitle += "*";
	}
	else
	{
		if ( -1 != sTitle.Find('*') )
			sTitle.Remove('*');
	}
	SetWindowTextA(sTitle);
	UpdateWindow();
}

void CDDROperationsDlg::OnLvnItemActivateOperationsList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	RefreshOperationState();
}

void CDDROperationsDlg::OnLvnItemChangedOperationsList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	RefreshOperationState();
}

void CDDROperationsDlg::OnNMClickOperationsList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;
	UpdateControls();
}
