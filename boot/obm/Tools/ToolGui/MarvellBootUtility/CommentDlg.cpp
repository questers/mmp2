/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// CommentDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "CommentDlg.h"


// CCommentDlg dialog

IMPLEMENT_DYNAMIC(CCommentDlg, CDialog)

CCommentDlg::CCommentDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCommentDlg::IDD, pParent)
	, m_pParent(pParent)
	, m_sPreComment(_T("")), m_sPostComment(_T(""))
{
	m_pLine = 0;
}

CCommentDlg::~CCommentDlg()
{
}

void CCommentDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PRE_COMMENT_EDT, m_sPreComment);
	DDX_Text(pDX, IDC_POST_COMMENT_EDT, m_sPostComment);
}


BEGIN_MESSAGE_MAP(CCommentDlg, CDialog)
END_MESSAGE_MAP()

void CCommentDlg::Display(void* pObj)
{
	m_pLine = 0;
	if ( (m_pLine = CTimDescriptor::GetLineField( "", false, pObj )) != 0 )
	{
		t_stringVectorIter iter  = m_pLine->m_PreCommentLines.begin();
		while( iter != m_pLine->m_PreCommentLines.end() )
		{
			m_sPreComment += (*iter++)->c_str();
			m_sPreComment += "\r\n";
		}

		iter  = m_pLine->m_PostCommentLines.begin();
		while( iter != m_pLine->m_PostCommentLines.end() )
		{
			m_sPostComment += (*iter++)->c_str();
			m_sPostComment += "\r\n";
		}

		DoModal();
	}
}

// CCommentDlg message handlers

void CCommentDlg::OnOK()
{
	UpdateData(true);

	if ( m_pLine )
	{
		// init the pre comment lines list
		t_stringVectorIter Iter = m_pLine->m_PreCommentLines.begin();
		while ( Iter != m_pLine->m_PreCommentLines.end() )
		{
			delete *Iter;
			Iter++;
		}
		m_pLine->m_PreCommentLines.clear();

		// convert a pre-comment string from the edit control into a list of comment lines
		string sLine;
		string sPreComment = m_sPreComment;
		while ( sPreComment.size() > 0 )
		{
			string::size_type pos = sPreComment.find("\r\n");
			if ( pos != string::npos )
				sLine = sPreComment.substr(0, pos);
			else
				sLine = sPreComment;

			//find if 1st non-ws char is a ; and if not insert a ;
			string::size_type pos_non_ws = sLine.find_first_not_of("\t \n\r");
			if ( ( pos_non_ws != string::npos ) && ( sLine.at(pos_non_ws) != ';' ) )
				sLine = sLine.substr(0, pos_non_ws) + ';' + sLine.substr( pos_non_ws );

			if ( pos != string::npos )
				sPreComment = sPreComment.substr(sPreComment.find("\r\n")+2);
			else
				sPreComment = "";
			
			m_pLine->m_PreCommentLines.push_back( new string( sLine ) );
		}

		// init the post comment lines list
		Iter = m_pLine->m_PostCommentLines.begin();
		while ( Iter != m_pLine->m_PostCommentLines.end() )
		{
			delete *Iter;
			Iter++;
		}
		m_pLine->m_PostCommentLines.clear();

		// convert a post-comment string from the edit control into a list of comment lines
		sLine = "";
		string sPostComment = m_sPostComment;
		while ( sPostComment.size() > 0 )
		{
			string::size_type pos = sPostComment.find("\r\n");
			if ( pos != string::npos )
				sLine = sPostComment.substr(0, pos);
			else
				sLine = sPostComment;

			//find if 1st non-ws char is a ; and if not insert a ;
			string::size_type pos_non_ws = sLine.find_first_not_of("\t \n\r");
			if ( ( pos_non_ws != string::npos ) && ( sLine.at(pos_non_ws) != ';' ) )
				sLine = sLine.substr(0, pos_non_ws) + ';' + sLine.substr( pos_non_ws );

			if ( pos != string::npos )
				sPostComment = sPostComment.substr(sPostComment.find("\r\n")+2);
			else
				sPostComment = "";
			
			m_pLine->m_PostCommentLines.push_back( new string( sLine ) );
		}
	}

//	if ( m_pParent )
//		m_pParent->SetFocus();

	CDialog::OnOK();
}
