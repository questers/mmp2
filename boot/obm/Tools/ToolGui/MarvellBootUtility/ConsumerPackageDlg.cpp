/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// ConsumerPackage.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"

#include "ConsumerPackageDlg.h"
#include "TimDescriptor.h"
#include "DDRInitializationPkgDlg.h"

// CConsumerPackage dialog

IMPLEMENT_DYNAMIC(CConsumerPackageDlg, CDialog)

CConsumerPackageDlg::CConsumerPackageDlg(CTimDescriptor& TimDescriptor, CConsumerID* pConsumerID /*=NULL*/, CWnd* pParent /*=NULL*/)
: CDialog(CConsumerPackageDlg::IDD, pParent), 
    m_LocalTimDescriptor( TimDescriptor ),
    m_pConsumerID( pConsumerID )
{

}

CConsumerPackageDlg::~CConsumerPackageDlg()
{
}

void CConsumerPackageDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_INSERT_PKG_BTN, InsertPkgBtn);
    DDX_Control(pDX, IDC_REMOVE_PKG_BTN, RemovePkgBtn);
    DDX_Control(pDX, IDC_PKG_MOVE_UP_BTN, MovePkgUpBtn);
    DDX_Control(pDX, IDC_PKG_MOVE_DOWN_BTN, MovePkgDownBtn);
    DDX_Control(pDX, IDC_PACKAGE_IDS_LIST, PackageIdListCtrl);
    DDX_Control(pDX, IDC_CONSUMER_ID_CB, ConsumerIdCb);
    DDX_Control(pDX, IDC_CONSUMMABLE_PKGS_LIST, ConsummablePkgsListCtrl);

    // updates marker in title
    UpdateChangeMarkerInTitle();
    theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CConsumerPackageDlg, CDialog)
    ON_BN_CLICKED(IDC_INSERT_PKG_BTN, &CConsumerPackageDlg::OnBnClickedInsertPkgBtn)
    ON_BN_CLICKED(IDC_REMOVE_PKG_BTN, &CConsumerPackageDlg::OnBnClickedRemovePkgBtn)
    ON_BN_CLICKED(IDC_PKG_MOVE_UP_BTN, &CConsumerPackageDlg::OnBnClickedPkgMoveUpBtn)
    ON_BN_CLICKED(IDC_PKG_MOVE_DOWN_BTN, &CConsumerPackageDlg::OnBnClickedPkgMoveDownBtn)
    ON_CBN_SELCHANGE(IDC_CONSUMER_ID_CB, &CConsumerPackageDlg::OnCbnSelchangeConsumerIdCb)
    ON_NOTIFY(NM_CLICK, IDC_PACKAGE_IDS_LIST, &CConsumerPackageDlg::OnNMClickPackageIdsList)
END_MESSAGE_MAP()


// CConsumerPackageDlg message handlers

BOOL CConsumerPackageDlg::OnInitDialog()
{
    CDialog::OnInitDialog();
    
    PackageIdListCtrl.SetExtendedStyle(    LVS_EX_TRACKSELECT
                                          //  LVS_EX_BORDERSELECT
                                          | LVS_EX_FULLROWSELECT 
                                          | LVS_EX_HEADERDRAGDROP 
                                          | LVS_EX_ONECLICKACTIVATE 
                                          | LVS_EX_LABELTIP 
                                          //| LVS_EX_GRIDLINES
                                         );

    PackageIdListCtrl.InsertColumn(0,"Packages to Consume", LVCFMT_LEFT, 225, -1 );

    ConsummablePkgsListCtrl.SetExtendedStyle(    LVS_EX_TRACKSELECT
                                          //  LVS_EX_BORDERSELECT
                                          | LVS_EX_FULLROWSELECT 
                                          | LVS_EX_HEADERDRAGDROP 
                                          | LVS_EX_ONECLICKACTIVATE 
                                          | LVS_EX_LABELTIP 
                                          //| LVS_EX_GRIDLINES
                                         );

    ConsummablePkgsListCtrl.InsertColumn(0,"Available Packages", LVCFMT_LEFT, 225, -1 );


    int nIdx = 0;
    ConsumerIdCb.AddString( "OBMI" );
    ConsumerIdCb.AddString( "TBRI" );
    ConsumerIdCb.AddString( "TZSI" );

    RefreshState();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void CConsumerPackageDlg::UpdateControls()
{
    InsertPkgBtn.EnableWindow( ConsummablePkgsListCtrl.GetItemCount() > 0 );

    int iCnt = PackageIdListCtrl.GetItemCount();
    RemovePkgBtn.EnableWindow( iCnt > 0 );

    int iSel = PackageIdListCtrl.GetSelectionMark();
    MovePkgUpBtn.EnableWindow( ( iCnt > 1 ) && ( iSel > 0 ) );
    MovePkgDownBtn.EnableWindow( ( iCnt > 1 ) && ( iSel >= 0 ) && (iSel < ( iCnt - 1 )) );	
}

void CConsumerPackageDlg::RefreshState()
{
    PackageIdListCtrl.DeleteAllItems();
    ConsummablePkgsListCtrl.DeleteAllItems();

    if ( m_pConsumerID )
    {
        if ( CB_ERR != ConsumerIdCb.SetCurSel( ConsumerIdCb.FindString(-1, m_pConsumerID->psCID()->c_str() )) )
        {
            t_ErdBaseVectorIter ERDIter;
            t_stringVectorIter Iter = m_pConsumerID->m_PIDs.begin();
            int nItem = 0;

            // add the DDRInit packages consumed to the consumed list
            while( Iter != m_pConsumerID->m_PIDs.end() )
            {
                ERDIter = m_LocalTimDescriptor.ExtendedReservedData().ErdVec.begin();
                while ( ERDIter != m_LocalTimDescriptor.ExtendedReservedData().ErdVec.end() )
                {
                    if ( (*ERDIter)->ErdPkgType() == CErdBase::DDR_INITIALIZATION_ERD )
                    {
                        if ( *(*Iter) == ((CDDRInitialization*)(*ERDIter))->m_sDdrPID )
                        {
                            PackageIdListCtrl.InsertItem( nItem, (*Iter)->c_str() );
                            PackageIdListCtrl.SetItemData( nItem, (DWORD_PTR)(*ERDIter) );
                            nItem++;
                        }
                    }
                    else if ( (*ERDIter)->ErdPkgType() == CErdBase::TZ_INITIALIZATION_ERD )
                    {
                        if ( *(*Iter) == ((CTzInitialization*)(*ERDIter))->m_sTzPID )
                        {
                            PackageIdListCtrl.InsertItem( nItem, (*Iter)->c_str() );
                            PackageIdListCtrl.SetItemData( nItem, (DWORD_PTR)(*ERDIter) );
                            nItem++;
                        }
                    }
                    ERDIter++;
                }
                Iter++;
            }

            // add the DDRInit packages consumed to the consumed list
            nItem = 0;
            ERDIter = m_LocalTimDescriptor.ExtendedReservedData().ErdVec.begin();
            while ( ERDIter != m_LocalTimDescriptor.ExtendedReservedData().ErdVec.end() )
            {
                bool bToAdd = true;
                if ( m_pConsumerID->m_PIDs.size() > 0 )
                {
                    Iter = m_pConsumerID->m_PIDs.begin();
                    while( Iter != m_pConsumerID->m_PIDs.end() )
                    {
                        if ( (*ERDIter)->ErdPkgType() == CErdBase::DDR_INITIALIZATION_ERD )
                        {
                            if ( *(*Iter) == ((CDDRInitialization*)(*ERDIter))->m_sDdrPID )
                            {
                                bToAdd = false;
                                break;
                            }
                        }
                        else if ( (*ERDIter)->ErdPkgType() == CErdBase::TZ_INITIALIZATION_ERD )
                        {
                            if ( *(*Iter) == ((CTzInitialization*)(*ERDIter))->m_sTzPID )
                            {
                                bToAdd = false;
                                break;
                            }
                        }
                        Iter++;
                    }
                }

                if ( bToAdd )
                {
                    if ( (*ERDIter)->ErdPkgType() == CErdBase::DDR_INITIALIZATION_ERD )
                    {
                        ConsummablePkgsListCtrl.InsertItem( nItem, ((CDDRInitialization*)(*ERDIter))->m_sDdrPID.c_str() );
                        ConsummablePkgsListCtrl.SetItemData( nItem, (DWORD_PTR)(*ERDIter) );
                        nItem++;
                    }
                    else if ( (*ERDIter)->ErdPkgType() == CErdBase::TZ_INITIALIZATION_ERD )
                    {
                        ConsummablePkgsListCtrl.InsertItem( nItem, ((CTzInitialization*)(*ERDIter))->m_sTzPID.c_str() );
                        ConsummablePkgsListCtrl.SetItemData( nItem, (DWORD_PTR)(*ERDIter) );
                        nItem++;
                    }
                }

                ERDIter++;
            }
        }
    }

    UpdateControls();
    UpdateData( FALSE );
}

void CConsumerPackageDlg::OnCbnSelchangeConsumerIdCb()
{
    UpdateData(TRUE);
    CString sText;
    ConsumerIdCb.GetWindowTextA( sText );
    m_pConsumerID->psCID( string((CStringA)sText) );

    RefreshState();
}

void CConsumerPackageDlg::OnBnClickedInsertPkgBtn()
{
    UpdateData( TRUE );

    t_ConsumerIDVec& Consumers = m_LocalTimDescriptor.ExtendedReservedData().m_Consumers;

    int nItem = PackageIdListCtrl.GetSelectionMark();
    if ( nItem == LB_ERR )
        // nothing selected 
        nItem = PackageIdListCtrl.GetItemCount();
    else
        nItem++;	// insert after selected item

    int iSel = ConsummablePkgsListCtrl.GetSelectionMark();
    if ( iSel != LB_ERR && m_pConsumerID )
    {
        // move from consummables list to consumed list
        PackageIdListCtrl.InsertItem( nItem, ConsummablePkgsListCtrl.GetItemText( iSel, 0 ) );
        CErdBase* pErdBase = (CErdBase*)ConsummablePkgsListCtrl.GetItemData( iSel );
        if ( pErdBase->ErdPkgType() == CErdBase::DDR_INITIALIZATION_ERD )
        {
            CDDRInitialization* pDDRInit = dynamic_cast<CDDRInitialization*>(pErdBase);
            if ( pDDRInit )
            {
                PackageIdListCtrl.SetItemData( nItem, ConsummablePkgsListCtrl.GetItemData( iSel ) );
                ConsummablePkgsListCtrl.DeleteItem( iSel );
                t_stringVectorIter Iter = m_pConsumerID->m_PIDs.begin();
                while ( Iter !=  m_pConsumerID->m_PIDs.end() )
                {
                    if ( --nItem <= 0 )
                        break;

                    Iter++;
                }
                m_pConsumerID->m_PIDs.insert( Iter, new string( pDDRInit->m_sDdrPID ) );
            }
        }
        else if ( pErdBase->ErdPkgType() == CErdBase::TZ_INITIALIZATION_ERD )
        {
            CTzInitialization* pTzInit = dynamic_cast<CTzInitialization*>(pErdBase);
            if ( pTzInit )
            {
                PackageIdListCtrl.SetItemData( nItem, ConsummablePkgsListCtrl.GetItemData( iSel ) );
                ConsummablePkgsListCtrl.DeleteItem( iSel );
                t_stringVectorIter Iter = m_pConsumerID->m_PIDs.begin();
                while ( Iter !=  m_pConsumerID->m_PIDs.end() )
                {
                    if ( --nItem <= 0 )
                        break;

                    Iter++;
                }
                m_pConsumerID->m_PIDs.insert( Iter, new string( pTzInit->m_sTzPID ) );
            }
        }
    }

    RefreshState();
    SetFocus();
}

void CConsumerPackageDlg::OnBnClickedRemovePkgBtn()
{
    UpdateData( TRUE );

    t_ConsumerIDVec& Consumers = m_LocalTimDescriptor.ExtendedReservedData().m_Consumers;

    int nItem = ConsummablePkgsListCtrl.GetSelectionMark();
    if ( nItem == LB_ERR )
        // nothing selected 
        nItem = ConsummablePkgsListCtrl.GetItemCount();
    else
        nItem++;	// insert after selected item

    int iSel = PackageIdListCtrl.GetSelectionMark();
    if ( iSel != LB_ERR && m_pConsumerID )
    {
        // move from consummables list to consumed list
        ConsummablePkgsListCtrl.InsertItem( nItem, PackageIdListCtrl.GetItemText( iSel, 0 ) );
        CErdBase* pErdBase = (CErdBase*)ConsummablePkgsListCtrl.GetItemData( iSel );
        if ( pErdBase->ErdPkgType() == CErdBase::DDR_INITIALIZATION_ERD )
        {
            CDDRInitialization* pDDRInit = dynamic_cast<CDDRInitialization*>(pErdBase);
            if ( pDDRInit )
            {
                ConsummablePkgsListCtrl.SetItemData( nItem, PackageIdListCtrl.GetItemData( iSel ) );
                PackageIdListCtrl.DeleteItem( iSel );
                t_stringVectorIter Iter = m_pConsumerID->m_PIDs.begin();
                while ( Iter !=  m_pConsumerID->m_PIDs.end() )
                {
                    if ( *(*Iter) == pDDRInit->m_sDdrPID )
                    {
                        delete (*Iter);
                        m_pConsumerID->m_PIDs.erase(Iter);
                        break;
                    }
                    Iter++;
                }
            }
        }
        else if ( pErdBase->ErdPkgType() == CErdBase::TZ_INITIALIZATION_ERD )
        {
            CTzInitialization* pTzInit = dynamic_cast<CTzInitialization*>(pErdBase);
            if ( pTzInit )
            {
                ConsummablePkgsListCtrl.SetItemData( nItem, PackageIdListCtrl.GetItemData( iSel ) );
                PackageIdListCtrl.DeleteItem( iSel );
                t_stringVectorIter Iter = m_pConsumerID->m_PIDs.begin();
                while ( Iter !=  m_pConsumerID->m_PIDs.end() )
                {
                    if ( *(*Iter) == pTzInit->m_sTzPID )
                    {
                        delete (*Iter);
                        m_pConsumerID->m_PIDs.erase(Iter);
                        break;
                    }
                    Iter++;
                }
            }
        }
    }
    RefreshState();
    SetFocus();
}


void CConsumerPackageDlg::OnBnClickedPkgMoveUpBtn()
{
    int iSel = PackageIdListCtrl.GetSelectionMark();
    if ( iSel > 0 )
    {
        CErdBase* pErdBase = (CErdBase*)PackageIdListCtrl.GetItemData( iSel );
        if ( pErdBase->ErdPkgType() == CErdBase::DDR_INITIALIZATION_ERD 
             || pErdBase->ErdPkgType() == CErdBase::TZ_INITIALIZATION_ERD )
        {
            if ( iSel > 0 )
            {
                string sPID = *m_pConsumerID->m_PIDs[iSel];
                *m_pConsumerID->m_PIDs[iSel] = *m_pConsumerID->m_PIDs[iSel-1];
                *m_pConsumerID->m_PIDs[iSel-1] = sPID;

                PackageIdListCtrl.SetSelectionMark( --iSel );
            }
        }
    }
    RefreshState();
}


void CConsumerPackageDlg::OnBnClickedPkgMoveDownBtn()
{
    int iSel = PackageIdListCtrl.GetSelectionMark();
    if ( iSel != LB_ERR )
    {
        CErdBase* pErdBase = (CErdBase*)PackageIdListCtrl.GetItemData( iSel );
        if ( pErdBase->ErdPkgType() == CErdBase::DDR_INITIALIZATION_ERD 
             || pErdBase->ErdPkgType() == CErdBase::TZ_INITIALIZATION_ERD )
        {
            if ( iSel < (int)m_pConsumerID->m_PIDs.size()-1 )
            {
                string sPID = *m_pConsumerID->m_PIDs[iSel];
                *m_pConsumerID->m_PIDs[iSel] = *m_pConsumerID->m_PIDs[iSel+1];
                *m_pConsumerID->m_PIDs[iSel+1] = sPID;

                PackageIdListCtrl.SetSelectionMark( ++iSel );
            }
        }
    }
    RefreshState();
}

void CConsumerPackageDlg::UpdateChangeMarkerInTitle()
{
    if ( m_hWnd == 0 )
        return;

    CString sTitle;
    GetWindowTextA( sTitle );
    // add or remove * as appropriate
    if ( m_LocalTimDescriptor.ReservedIsChanged() )
    {
        if ( -1 == sTitle.Find('*') )
            sTitle += "*";
    }
    else
    {
        if ( -1 != sTitle.Find('*') )
            sTitle.Remove('*');
    }
    SetWindowTextA(sTitle);
    UpdateWindow();
}

void CConsumerPackageDlg::OnNMClickPackageIdsList(NMHDR *pNMHDR, LRESULT *pResult)
{
    // TODO: Add your control notification handler code here
    *pResult = 0;
    UpdateControls();
}
