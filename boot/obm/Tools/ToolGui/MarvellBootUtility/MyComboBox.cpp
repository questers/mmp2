/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#include "stdafx.h"
#include "MyComboBox.h"

IMPLEMENT_DYNAMIC(CMyComboBox, CComboBox)

CMyComboBox::CMyComboBox(void)
: CComboBox()
{
}

CMyComboBox::~CMyComboBox(void)
{
}

BEGIN_MESSAGE_MAP(CMyComboBox, CComboBox)
	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()

void CMyComboBox::OnContextMenu(CWnd* pWnd, CPoint point)
{
	GetParent()->SendMessage(WM_CONTEXTMENU, (WPARAM)pWnd->m_hWnd, (LPARAM)&point );
}
