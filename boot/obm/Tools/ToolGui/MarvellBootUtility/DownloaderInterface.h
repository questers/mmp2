/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "ImageBuild.h"
#if TRUSTED
#include "TrustedTimDescriptorParser.h"
#else
#include "TimDescriptorParser.h"
#endif

#if TOOLS_GUI
#include "FBFMakeConfig.h"   
#endif

#include <string>
#include <list>
#include <fstream>
#include <iostream>
#include <strstream>
#include <sstream>
using namespace std;

typedef list<string*>           t_stringList;
typedef list<string*>::iterator t_stringListIter;

#define HOST_MAX_IMAGE_FILES 5

#include "TimLib.h"

class CDownloaderInterface : public CTimLib
{
public:
	enum eDownloadType{ NO_TYPE, TIM_BASED_DKB, TIM_BASED_OBM, FBF_BASED_FILE };
	enum eFlashType{ DEFAULT = 0, MSYSTEM_DFI, ONE_NAND_DFI, XIP_DDR_16_EMPI,
					 NAND_1_16_16, SIBLEY_16, NAND_1_8_8, TYAX_16_DFI };

public:
	CDownloaderInterface(eDownloadType);
	virtual ~CDownloaderInterface(void);

	void VerboseMode( bool bSet ) { bVerboseMode = bSet; m_bChanged = true; }
	bool VerboseMode() { return bVerboseMode; }

	void RunDebugBootCommand( bool bSet ) { bRunDebugBootCommand = bSet; m_bChanged = true; }
	bool RunDebugBootCommand() { return bRunDebugBootCommand; }

	void PortType( string& sPort ) { sPortType = sPort; m_bChanged = true; }
	string& PortType() { return sPortType; }

	void MsgMode( string& sMode ) { sMsgMode = sMode; m_bChanged = true; }
	string& MsgMode() { return sMsgMode; }

	void ComPort( string& sPort ) { sComPort = sPort; m_bChanged = true; }
	string& ComPort() { return sComPort; }

	void BaudRate( string& sRate ) { sBaudRate = sRate; m_bChanged = true; }
	string& BaudRate() { return sBaudRate; }

	void FlashType( eFlashType nType ) { nFlashType = nType; m_bChanged = true; }
	eFlashType FlashType() { return nFlashType; }

	void DownloadType( eDownloadType nType ) { nDownloadType = nType; m_bChanged = true; }
	eDownloadType DownloadType() { return nDownloadType; }

	void UartDelayMs( string& sDelay ) { sUartDelayMs = sDelay; m_bChanged = true; }
	string& UartDelayMs() { return sUartDelayMs; }

	void UsbPacketSize( string& sSize ) { sUsbPacketSize = sSize; m_bChanged = true; }
	string& UsbPacketSize() { return sUsbPacketSize; }

	void TimFilePath( string& sFilePath ) { sTimFilePath = sFilePath; m_bChanged = true; }
	string& TimFilePath() { return sTimFilePath; }

	void FbfFilePath( string& sFilePath ) { sFbfFilePath = sFilePath; m_bChanged = true; }
	string& FbfFilePath() { return sFbfFilePath; }

	unsigned int AddBinImage( string& sBinImageFilePath );
	unsigned int DeleteBinImage( string& sBinImageFilePath );
	unsigned int NumImageBinFiles() { return (unsigned int)m_ImageBinFiles.size(); }
	t_stringList& ImageBinFiles() { return m_ImageBinFiles; }

#if TOOLS_GUI == 1
	unsigned int RefreshImageList( t_ImagesList& NewImageList );
#endif

	void JtagKeyFilePath( string& sFilePath ) { sJtagKeyFilePath = sFilePath; m_bChanged = true; }
	string& JtagKeyFilePath() { return sJtagKeyFilePath; }
	
	void PartitionBinary( bool bSet ) { bPartitionBinary = bSet; m_bChanged = true; }
	bool PartitionBinary() { return bPartitionBinary; }

	void PartitionBinaryFilePath( string& sFilePath ) { sPartitionBinaryFilePath = sFilePath; m_bChanged = true; }
	string& PartitionBinaryFilePath() { return sPartitionBinaryFilePath; }

	void WtptpExePath( string& sFilePath ) { sWtptpExePath = sFilePath; m_bChanged = true; }
	string& WtptpExePath() { return sWtptpExePath; }

	void AdditionalOptions( string& sOption ) { sAdditionalOptions = sOption; m_bChanged = true; }
	string& AdditionalOptions() { return sAdditionalOptions; }

	void AdditionalOptionsEnable( bool bSet ) { bAdditionalOptions = bSet; m_bChanged = true; }
	bool AdditionalOptionsEnable() { return bAdditionalOptions; }

	void IncludeInDownload( bool bSet ) { bIncludeInDownload = bSet; m_bChanged = true; }
	bool IncludeInDownload() { return bIncludeInDownload; }

	void ProcessorTypeEnable( bool bSet ) { bProcessorType = bSet; m_bChanged = true; }
	bool ProcessorTypeEnable() { return bProcessorType; }

	void UploadSpecFileEnable( bool bSet ) { bUploadSpecFile = bSet; m_bChanged = true; }
	bool UploadSpecFileEnable() { return bUploadSpecFile; }

	void UploadSpecFilePath( string& sFilePath ) { sUploadSpecFilePath = sFilePath; m_bChanged = true; }
	string& UploadSpecFilePath() { return sUploadSpecFilePath; }

#if TOOLS_GUI == 1
	bool SaveState( ofstream& ofs );
	bool LoadState( ifstream& ifs );
#endif

	bool IsChanged();
	void Changed( bool bSet );

	void Reset();
	void DiscardAll();

	string& CommandLineArgs();

#if TRUSTED
	CTrustedTimDescriptorParser TimDescriptorParser;
#else
	CTimDescriptorParser		TimDescriptorParser;
#endif

#if TOOLS_GUI == 1
	CImageBuild	ImageBuild;
	CFBFMakeConfig FBFmakecfg;   
#endif

private:
	string  sTimFilePath;
	string  sFbfFilePath;
	string  sMsgMode;
	string  sPortType;
	string  sComPort;
	string  sBaudRate;

	string  sJtagKeyFilePath;
	string  sWtptpExePath;

	bool	bPartitionBinary;
	string	sPartitionBinaryFilePath;

	bool	bVerboseMode;
	bool	bRunDebugBootCommand;
	eFlashType    nFlashType;
	string	sUartDelayMs;
	string	sUsbPacketSize;
	string	sCommandLineArgs;
	string	sAdditionalOptions;
	bool	bAdditionalOptions;

	eDownloadType nDownloadType;
	bool	bIncludeInDownload;
	bool	bProcessorType;

    bool    bUploadSpecFile;
    string  sUploadSpecFilePath;

	t_stringList m_ImageBinFiles;
	bool		 m_bChanged;
};

//typedef list<CDownloaderInterface*>				t_DownloaderInterfaceList;
//typedef list<CDownloaderInterface*>::iterator	t_DownloaderInterfaceListIter;
