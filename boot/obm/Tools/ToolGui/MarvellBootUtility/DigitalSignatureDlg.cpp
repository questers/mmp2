/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
// DigitalSignatureDlg.cpp : implementation file
//

#include "stdafx.h"
#if TRUSTED
#include "MarvellBootUtility.h"
#include "DigitalSignatureDlg.h"

#include "PublicKeyExponentDlg.h"
#include "PrivateKeyExponentDlg.h"
#include "RsaModulusDlg.h"
#include "CommentDlg.h"

//#include "KeyGenerator.h"

// CDigitalSignatureDlg dialog

IMPLEMENT_DYNAMIC(CDigitalSignatureDlg, CDialog)

CDigitalSignatureDlg::CDigitalSignatureDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CDigitalSignatureDlg::IDD, pParent)
{
    bIncludeInTim = FALSE;
}

CDigitalSignatureDlg::~CDigitalSignatureDlg()
{
}

void CDigitalSignatureDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_DSA_ALGORITHM_ID_CB, EncryptAlgorithmCb);
    DDX_Control(pDX, IDC_HASH_ALGORITHM_ID_CB, HashAlgorithmIdCb);
    DDX_Control(pDX, IDC_MODULUS_SIZE_CB, ModulusSizeCb);
    DDX_Check(pDX, IDC_INCLUDE_IN_TIM, bIncludeInTim);

    // updates marker in title
    UpdateChangeMarkerInTitle();
    theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
    DDX_Control(pDX, IDC_PUBLIC_KEY_EXPONENT_BTN, PubKeyCompXBtn);
    DDX_Control(pDX, IDC_RSA_MODULUS_BTN, RsaModulusCompYBtn);
    DDX_Control(pDX, IDC_RSA_PRIVATE_KEY_BTN, RsaEcdsaPrivateKeyBtn);
}


BEGIN_MESSAGE_MAP(CDigitalSignatureDlg, CDialog)
    ON_CBN_SELCHANGE(IDC_MODULUS_SIZE_CB, &CDigitalSignatureDlg::OnCbnSelchangeModulusSizeCb)
    ON_CBN_SELCHANGE(IDC_HASH_ALGORITHM_ID_CB, &CDigitalSignatureDlg::OnCbnSelchangeHashAlgorithmIdCb)
    ON_CBN_SELCHANGE(IDC_DSA_ALGORITHM_ID_CB, &CDigitalSignatureDlg::OnCbnSelchangeEncryptAlgorithmCb)
    ON_BN_CLICKED(IDC_PUBLIC_KEY_EXPONENT_BTN, &CDigitalSignatureDlg::OnBnClickedPublicKeyExponentBtn)
    ON_BN_CLICKED(IDC_RSA_MODULUS_BTN, &CDigitalSignatureDlg::OnBnClickedRsaModulusBtn)
    ON_BN_CLICKED(IDC_RSA_PRIVATE_KEY_BTN, &CDigitalSignatureDlg::OnBnClickedRsaPrivateKeyBtn)
    ON_BN_CLICKED(IDC_INCLUDE_IN_TIM, &CDigitalSignatureDlg::OnBnClickedIncludeInTim)
    ON_BN_CLICKED(IDC_SAMPLE_DS_BTN, &CDigitalSignatureDlg::OnBnClickedSampleDsBtn)
    ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()


// CDigitalSignatureDlg message handlers
BOOL CDigitalSignatureDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    RefreshState();

    CString sDsaAlgorithm = m_DigitalSignature.EncryptAlgorithmId().c_str();
    
    ModulusSizeCb.ResetContent();
    if ( sDsaAlgorithm == "ECDSA_256" )
    {
        ModulusSizeCb.AddString("256");
        PubKeyCompXBtn.SetWindowText("ECDSA Public Key Comp X...");
        RsaModulusCompYBtn.SetWindowText("ECDSA Public Key Comp Y...");
        RsaEcdsaPrivateKeyBtn.SetWindowText("ECDSA Private Key...");
    }
    else if ( sDsaAlgorithm == "ECDSA_521" )
    {
        ModulusSizeCb.AddString("521");
        PubKeyCompXBtn.SetWindowText("ECDSA Public Key Comp X...");
        RsaModulusCompYBtn.SetWindowText("ECDSA Public Key Comp Y...");
        RsaEcdsaPrivateKeyBtn.SetWindowText("ECDSA Private Key...");
    }
    else
    {
        ModulusSizeCb.AddString("1024");
        ModulusSizeCb.AddString("2048");
        PubKeyCompXBtn.SetWindowText("Public Key Exponent...");
        RsaModulusCompYBtn.SetWindowText("RSA Modulus...");
        RsaEcdsaPrivateKeyBtn.SetWindowText("RSA Private Key...");
    }

    RefreshState();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void CDigitalSignatureDlg::DigitalSignature( CDigitalSignature& DigitalSignature ) 
{ 
    m_DigitalSignature = DigitalSignature;
}

void CDigitalSignatureDlg::RefreshState()
{
    UpdateData( TRUE );

    CString sEncryptAlgorithm = m_DigitalSignature.EncryptAlgorithmId().c_str();
    if ( LB_ERR != EncryptAlgorithmCb.FindStringExact(0, sEncryptAlgorithm ) )
    {
        // predefined key id selected
        EncryptAlgorithmCb.SetCurSel(EncryptAlgorithmCb.FindStringExact(0, sEncryptAlgorithm));
    }
    else
    {
        // custom user defined key id
        EncryptAlgorithmCb.SetWindowTextA( sEncryptAlgorithm );
    }

    HashAlgorithmIdCb.SetCurSel(HashAlgorithmIdCb.FindStringExact(0, m_DigitalSignature.HashAlgorithmId().c_str()));
    stringstream ss;
    ss << m_DigitalSignature.KeySize();

    ModulusSizeCb.SetCurSel(ModulusSizeCb.FindStringExact(0, ss.str().c_str()));

    bIncludeInTim = m_DigitalSignature.IncludeInTim() ? TRUE : FALSE;

    UpdateData( FALSE );
}

void CDigitalSignatureDlg::OnCbnSelchangeModulusSizeCb()
{
    UpdateData( TRUE );
    CString sText;
    ModulusSizeCb.GetLBText( ModulusSizeCb.GetCurSel(), sText );
    m_DigitalSignature.KeySize( atoi(sText) );
    RefreshState();
}

void CDigitalSignatureDlg::OnCbnSelchangeHashAlgorithmIdCb()
{
    UpdateData( TRUE );
    CString sText;
    HashAlgorithmIdCb.GetLBText( HashAlgorithmIdCb.GetCurSel(), sText );
    m_DigitalSignature.HashAlgorithmId( string((CStringA)sText) );
    RefreshState();
}

void CDigitalSignatureDlg::OnCbnSelchangeEncryptAlgorithmCb()
{
    UpdateData( TRUE );
    CString sText;
    int idx = EncryptAlgorithmCb.GetCurSel();
    EncryptAlgorithmCb.GetLBText(idx, sText );
    m_DigitalSignature.EncryptAlgorithmId( string((CStringA)sText) );

    ModulusSizeCb.ResetContent();
    if ( sText == "ECDSA_256" )
    {
        ModulusSizeCb.AddString("256");
        m_DigitalSignature.KeySize(256);
        PubKeyCompXBtn.SetWindowText("ECDSA Public Key Comp X...");
        RsaModulusCompYBtn.SetWindowText("ECDSA Public Key Comp Y...");
        RsaEcdsaPrivateKeyBtn.SetWindowText("ECDSA Private Key...");
    }
    else if ( sText == "ECDSA_521" )
    {
        ModulusSizeCb.AddString("521");
        m_DigitalSignature.KeySize(521);
        PubKeyCompXBtn.SetWindowText("ECDSA Public Key Comp X...");
        RsaModulusCompYBtn.SetWindowText("ECDSA Public Key Comp Y...");
        RsaEcdsaPrivateKeyBtn.SetWindowText("ECDSA Private Key...");
    }
    else
    {
        ModulusSizeCb.AddString("1024");
        ModulusSizeCb.AddString("2048");
        m_DigitalSignature.KeySize(1024);
        PubKeyCompXBtn.SetWindowText("Public Key Exponent...");
        RsaModulusCompYBtn.SetWindowText("RSA Modulus...");
        RsaEcdsaPrivateKeyBtn.SetWindowText("RSA Private Key...");
    }
    ModulusSizeCb.SetCurSel(0);

    RefreshState();
}


void CDigitalSignatureDlg::OnBnClickedRsaModulusBtn()
{
    CRsaModulusDlg Dlg;
    Dlg.DigitalSignature( m_DigitalSignature );
    if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_256" || m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_521" )
        Dlg.Title("ECDSA Public Key Comp Y");
    else
        Dlg.Title("RSA Modulus");

    if ( Dlg.DoModal() == IDOK )
    {
        if ( Dlg.DigitalSignature().IsChanged() )
        {
            m_DigitalSignature = Dlg.DigitalSignature();
        }
    }
    UpdateData(FALSE);
    SetFocus();
}

void CDigitalSignatureDlg::OnBnClickedPublicKeyExponentBtn()
{
    CPublicKeyExponentDlg Dlg;
    Dlg.Key( dynamic_cast<CKey&>(m_DigitalSignature) );
    if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_256" || m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_521" )
        Dlg.Title("ECDSA Public Key Comp X");
    else
        Dlg.Title("Public Key Exponent");

    if ( Dlg.DoModal() == IDOK )
    {
        if ( Dlg.Key().IsChanged() )
        {
            dynamic_cast<CKey&>(m_DigitalSignature) = Dlg.Key();
        }
    }
    UpdateData(FALSE);
    SetFocus();
}

void CDigitalSignatureDlg::OnBnClickedRsaPrivateKeyBtn()
{
    CPrivateKeyExponentDlg Dlg;
    Dlg.DigitalSignature( m_DigitalSignature );
    if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_256" || m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_521" )
        Dlg.Title("ECDSA Private Key");
    else
        Dlg.Title("RSA Private Key");

    if ( Dlg.DoModal() == IDOK )
    {
        if ( Dlg.DigitalSignature().IsChanged() )
        {
            m_DigitalSignature = Dlg.DigitalSignature();
        }
    }
    UpdateData(FALSE);
    SetFocus();
}


void CDigitalSignatureDlg::OnBnClickedIncludeInTim()
{
    UpdateData( TRUE );
    m_DigitalSignature.IncludeInTim( bIncludeInTim == TRUE ? true : false );
    UpdateData( FALSE );
}

void CDigitalSignatureDlg::UpdateChangeMarkerInTitle()
{
    if ( m_hWnd == 0 )
        return;

    CString sTitle;
    GetWindowTextA( sTitle );
    // add or remove * as appropriate
    if ( m_DigitalSignature.IsChanged() )
    {
        if ( -1 == sTitle.Find('*') )
            sTitle += "*";
    }
    else
    {
        if ( -1 != sTitle.Find('*') )
            sTitle.Remove('*');
    }
    SetWindowTextA(sTitle);
    UpdateWindow();
}

void CDigitalSignatureDlg::OnBnClickedSampleDsBtn()
{
    if ( IDYES == AfxMessageBox("Replace Digital Signature with Sample Values", MB_YESNO ) )
    {
        if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_256" )
        {
            // clear old keys
            m_DigitalSignature.KeySize(0);
            // create new sample ds
            m_DigitalSignature.KeySize(256);
            m_DigitalSignature.HashAlgorithmId(string("SHA-160"));

            t_stringListIter iterX = m_DigitalSignature.ECDSAPublicKeyCompXList().begin();
            *(*(iterX++)) = string("0x5e2be958");
            *(*(iterX++)) = string("0x244b9784");
            *(*(iterX++)) = string("0x648b1912");
            *(*(iterX++)) = string("0xdb1bb81f");
            *(*(iterX++)) = string("0xe2a96d24");
            *(*(iterX++)) = string("0xf6203dc6");
            *(*(iterX++)) = string("0x8ae234ed");
            *(*(iterX++)) = string("0x0d0c4b6f");

            t_stringListIter iterY = m_DigitalSignature.ECDSAPublicKeyCompYList().begin();
            *(*(iterY++)) = string("0xe71d7abe");
            *(*(iterY++)) = string("0x3aab23b8");
            *(*(iterY++)) = string("0xf64a49d1");
            *(*(iterY++)) = string("0xcab01c4d");
            *(*(iterY++)) = string("0xe095bdf6");
            *(*(iterY++)) = string("0xad85804a");
            *(*(iterY++)) = string("0xdc5c5b10");
            *(*(iterY++)) = string("0x0b0cdfd2");

            //default private key is ascii of "MarvellWirelessTrustedModuleVer3"
            t_stringListIter iterPK = m_DigitalSignature.ECDSAPrivateKeyList().begin();
            *(*(iterPK++)) = string("0x56657233");
            *(*(iterPK++)) = string("0x64756c65");
            *(*(iterPK++)) = string("0x65644d6f");
            *(*(iterPK++)) = string("0x72757374");
            *(*(iterPK++)) = string("0x65737354");
            *(*(iterPK++)) = string("0x6972656c");
            *(*(iterPK++)) = string("0x656c6c57");
            *(*(iterPK++)) = string("0x4d617276");
        }
        else if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_521" )
        {
            // clear old keys
            m_DigitalSignature.KeySize(0);
            // create new sample ds
            m_DigitalSignature.KeySize(521);
            m_DigitalSignature.HashAlgorithmId(string("SHA-160"));

            t_stringListIter iterX = m_DigitalSignature.ECDSAPublicKeyCompXList().begin();
            *(*(iterX++)) = string("0x62C8367F");
            *(*(iterX++)) = string("0x3244C9CB");
            *(*(iterX++)) = string("0x7B0B6AC4");
            *(*(iterX++)) = string("0x4ACAC507");
            *(*(iterX++)) = string("0x98E3C011");
            *(*(iterX++)) = string("0x6A61FC54");
            *(*(iterX++)) = string("0x29CA98B6");
            *(*(iterX++)) = string("0x85737715");
            *(*(iterX++)) = string("0x41523555");
            *(*(iterX++)) = string("0xF3B4D4FC");
            *(*(iterX++)) = string("0x1CF20110");
            *(*(iterX++)) = string("0xA7152D7C");
            *(*(iterX++)) = string("0xD2C5A731");
            *(*(iterX++)) = string("0x67CE711E");
            *(*(iterX++)) = string("0x5AF9C5E3");
            *(*(iterX++)) = string("0x2630DBC8");
            *(*(iterX++)) = string("0x0000006A");

            t_stringListIter iterY = m_DigitalSignature.ECDSAPublicKeyCompYList().begin();
            *(*(iterY++)) = string("0x47A318A3");
            *(*(iterY++)) = string("0x7F9642FB");
            *(*(iterY++)) = string("0x4E45C1D3");
            *(*(iterY++)) = string("0x72B3642A");
            *(*(iterY++)) = string("0x35BC87F3");
            *(*(iterY++)) = string("0xEA7CB21D");
            *(*(iterY++)) = string("0x1DEC9BB9");
            *(*(iterY++)) = string("0x77A5EC46");
            *(*(iterY++)) = string("0x0933645F");
            *(*(iterY++)) = string("0x49006F9A");
            *(*(iterY++)) = string("0x6EBF775A");
            *(*(iterY++)) = string("0x4C6967D7");
            *(*(iterY++)) = string("0x190B6A06");
            *(*(iterY++)) = string("0x511A2A61");
            *(*(iterY++)) = string("0xF46A98D3");
            *(*(iterY++)) = string("0x5B6EF39B");
            *(*(iterY++)) = string("0x00000063");

            //default private key random
            t_stringListIter iterPK = m_DigitalSignature.ECDSAPrivateKeyList().begin();
            *(*(iterPK++)) = string("0xCCD41A8A");
            *(*(iterPK++)) = string("0x9C97A119");
            *(*(iterPK++)) = string("0xCA169D1B");
            *(*(iterPK++)) = string("0x65CE7530");
            *(*(iterPK++)) = string("0x572EB73A");
            *(*(iterPK++)) = string("0x1B1C0002");
            *(*(iterPK++)) = string("0x98C3DEDD");
            *(*(iterPK++)) = string("0x1554BD3A");
            *(*(iterPK++)) = string("0x772D46F6");
            *(*(iterPK++)) = string("0x576B449B");
            *(*(iterPK++)) = string("0x60DD3B62");
            *(*(iterPK++)) = string("0x0B293F21");
            *(*(iterPK++)) = string("0xDE70AFF2");
            *(*(iterPK++)) = string("0xC543C238");
            *(*(iterPK++)) = string("0xC1CC3615");
            *(*(iterPK++)) = string("0xC2ED15F2");
            *(*(iterPK++)) = string("0x00000020");
        }
        else
        {
            // clear old keys
            m_DigitalSignature.KeySize(0);
            // create new sample ds
            m_DigitalSignature.KeySize(1024);
//			m_DigitalSignature.EncryptAlgorithmId(string("Marvell"));
            m_DigitalSignature.HashAlgorithmId(string("SHA-160"));
            ModulusSizeCb.ResetContent();
            ModulusSizeCb.AddString("1024");
            ModulusSizeCb.AddString("2048");
            ModulusSizeCb.SetCurSel(0);
            
            t_stringListIter iter = m_DigitalSignature.PublicKeyExponentList().begin();
            *(*(iter)) = string("0x00000003");

            iter = m_DigitalSignature.RsaSystemModulusList().begin();
            *(*(iter++)) = string("0xE34F43BD");
            *(*(iter++)) = string("0x75049E3F");
            *(*(iter++)) = string("0xA3527B1A");
            *(*(iter++)) = string("0x9846D1AC");

            *(*(iter++)) = string("0xD15DC003");
            *(*(iter++)) = string("0x0B3DF2F4");
            *(*(iter++)) = string("0xB9AD7A35");
            *(*(iter++)) = string("0x5DF0EC2D");

            *(*(iter++)) = string("0xE48AF431");
            *(*(iter++)) = string("0xAEE9F9E5");
            *(*(iter++)) = string("0x4925FD4F");
            *(*(iter++)) = string("0x5AAFEF7C");

            *(*(iter++)) = string("0x4E676F39");
            *(*(iter++)) = string("0x3A3A69BE");
            *(*(iter++)) = string("0x9EC21E4D");
            *(*(iter++)) = string("0xD5C1E639");

            *(*(iter++)) = string("0x984AB34B");
            *(*(iter++)) = string("0xE9802CB8");
            *(*(iter++)) = string("0x0FA840F5");
            *(*(iter++)) = string("0x28A83FE7");

            *(*(iter++)) = string("0x64D6569A");
            *(*(iter++)) = string("0x80172DE0");
            *(*(iter++)) = string("0x450D7FA9");
            *(*(iter++)) = string("0x52E6A947");

            *(*(iter++)) = string("0xEEE8B271");
            *(*(iter++)) = string("0x7954D939");
            *(*(iter++)) = string("0x003BC642");
            *(*(iter++)) = string("0x0D0AD7E5");

            *(*(iter++)) = string("0x664CA269");
            *(*(iter++)) = string("0xF9786672");
            *(*(iter++)) = string("0xDFE54FE8");
            *(*(iter++)) = string("0xACD1CC46");

            iter = m_DigitalSignature.RsaPrivateKeyList().begin();
            *(*(iter++)) = string("0x241D3C4B");
            *(*(iter++)) = string("0xC127FD13");
            *(*(iter++)) = string("0xDF07BED5");
            *(*(iter++)) = string("0x677BFE89");

            *(*(iter++)) = string("0x5C462ED5");
            *(*(iter++)) = string("0xA3B76928");
            *(*(iter++)) = string("0x3F80C933");
            *(*(iter++)) = string("0x16B541B3");

            *(*(iter++)) = string("0xBEB93BCA");
            *(*(iter++)) = string("0xC9D0F042");
            *(*(iter++)) = string("0x8E528A1A");
            *(*(iter++)) = string("0x6BC15B35");

            *(*(iter++)) = string("0x6C8A4A0E");
            *(*(iter++)) = string("0x8C9EFBB2");
            *(*(iter++)) = string("0xAC8D36A9");
            *(*(iter++)) = string("0x5D7B4DAA");

            *(*(iter++)) = string("0x1961C88C");
            *(*(iter++)) = string("0xFC400774");
            *(*(iter++)) = string("0xD7F16028");
            *(*(iter++)) = string("0xB1715FFB");

            *(*(iter++)) = string("0x6623B919");
            *(*(iter++)) = string("0xEAAE87A5");
            *(*(iter++)) = string("0xE0D79546");
            *(*(iter++)) = string("0xE326718B");

            *(*(iter++)) = string("0xA7D17312");
            *(*(iter++)) = string("0xBEE37989");
            *(*(iter++)) = string("0xD55F4BB5");
            *(*(iter++)) = string("0x822C7950");

            *(*(iter++)) = string("0x66621B11");
            *(*(iter++)) = string("0x7EE96668");
            *(*(iter++)) = string("0xCFFB8D51");
            *(*(iter++)) = string("0x1CCDA20B");
        }

        m_DigitalSignature.Changed(true);
    }
    RefreshState();
}

void CDigitalSignatureDlg::OnContextMenu(CWnd* pWnd, CPoint /*point*/)
{
    CCommentDlg Dlg(this);
    int nItem = -1;
    int iCtlId = pWnd->GetDlgCtrlID();
    switch ( iCtlId )
    {
        case IDC_DSA_ALGORITHM_ID_CB:
            Dlg.Display( &m_DigitalSignature.EncryptAlgorithmId() );
            break;

        case IDC_MODULUS_SIZE_CB:
            Dlg.Display( &m_DigitalSignature.KeySize() );
            break;

        case IDC_HASH_ALGORITHM_ID_CB:
            Dlg.Display( &m_DigitalSignature.HashAlgorithmId() );
            break;

        default:
            break;
    }
}

#endif
