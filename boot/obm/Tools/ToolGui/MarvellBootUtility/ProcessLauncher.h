/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <strstream>
using namespace std;

class CProcessLauncher
{
public:
    CProcessLauncher(void);
    ~CProcessLauncher(void);

public:
    bool Launch( string& sExePath, string& sCommandLineArgs );

private:
    HANDLE	hChildStdinRd;
    HANDLE	hChildStdinWr;
    HANDLE	hChildStdoutRd;
    HANDLE	hChildStdoutWr;
//	HANDLE	hInputFile;
//	HANDLE	hStdout;
 
    PROCESS_INFORMATION piProcInfo; 
    STARTUPINFO siStartInfo;

    bool CreateExeProcess( string& sExePath, string& sCommandLineArgs ); 
    bool ReadFromPipe(VOID);
//	VOID WriteToPipe(VOID); 
};
