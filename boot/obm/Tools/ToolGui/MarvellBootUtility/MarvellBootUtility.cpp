/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// MarvellBootUtility.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "MainFrm.h"

#include "ChildFrm.h"
#include "MarvellBootUtilityDoc.h"
#include "MarvellBootUtilityView.h"
#include "TimDescriptorPropPage.h"
#include "FBFDownloadPropertyPage.h"

#include "LaunchDlg.h"

#include <string>
#include <fstream>
#include <iostream>
#include <strstream>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMarvellBootUtilityApp

BEGIN_MESSAGE_MAP(CMarvellBootUtilityApp, CWinApp)
    ON_COMMAND(ID_APP_ABOUT, &CMarvellBootUtilityApp::OnAppAbout)
    // Standard file based document commands
    ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
    ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
    // Standard print setup command
    ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinApp::OnFilePrintSetup)
    ON_COMMAND(ID_FILE_LOADPROJECT, &CMarvellBootUtilityApp::OnFileLoadproject)
    ON_COMMAND(ID_FILE_SAVEPROJECT, &CMarvellBootUtilityApp::OnFileSaveproject)
    ON_COMMAND(ID_CONFIG_TARGETLIST, &CMarvellBootUtilityApp::OnConfigTargetlist)
    ON_COMMAND(ID_APP_EXIT, &CMarvellBootUtilityApp::OnAppExit)
    ON_COMMAND(ID_CONFIG_DKB_DOWNLOADER, &CMarvellBootUtilityApp::OnConfigDKBDownloader)
    ON_COMMAND(ID_CONFIG_OBM_DOWNLOADER, &CMarvellBootUtilityApp::OnConfigOBMDownloader)
    ON_COMMAND(ID_CONFIG_FBF_DOWNLOADER, &CMarvellBootUtilityApp::OnConfigFbfDownloader)
END_MESSAGE_MAP()


// CMarvellBootUtilityApp construction

// The one and only CMarvellBootUtilityApp object
CMarvellBootUtilityApp theApp;

CMarvellBootUtilityApp::CMarvellBootUtilityApp()
{
    m_pLaunchDlg = 0;
    m_pToolsInterfacePropSheet = 0;
    m_pDKBTimDescriptorPropPage = 0;
    m_pOBMTimDescriptorPropPage = 0;
    m_pFBFDownloadPropertyPage = 0;
    ProjectVersion = 0;
    ProjectDate = 0;
    m_bChanged = false;

    m_pDKBDownloaderInterface = new CDownloaderInterface( CDownloaderInterface::TIM_BASED_DKB );
    m_pOBMDownloaderInterface = new CDownloaderInterface( CDownloaderInterface::TIM_BASED_OBM );
    m_pFBFDownloaderInterface = new CDownloaderInterface( CDownloaderInterface::FBF_BASED_FILE );
    m_pDownloadController = new CDownloadController;
}

CMarvellBootUtilityApp::~CMarvellBootUtilityApp()
{
    delete m_pToolsInterfacePropSheet;

    delete m_pDKBDownloaderInterface;
    m_pDKBTimDescriptorPropPage->DestroyWindow();
    delete m_pDKBTimDescriptorPropPage;

    delete m_pOBMDownloaderInterface;
    m_pOBMTimDescriptorPropPage->DestroyWindow();
    delete m_pOBMTimDescriptorPropPage;

    delete m_pFBFDownloaderInterface;
    m_pFBFDownloadPropertyPage->DestroyWindow();
    delete m_pFBFDownloadPropertyPage;

    if ( m_pLaunchDlg )
    {
        if ( m_pLaunchDlg->m_hWnd )
            m_pLaunchDlg->DestroyWindow();
        else
            delete m_pLaunchDlg;
    }

    // delete all Targets
    t_TargetsIter iter = m_Targets.begin();
    while( iter != m_Targets.end() )
    {
        delete *iter;
        iter++;
    }
    m_Targets.clear();

    delete m_pDownloadController;
}



// CMarvellBootUtilityApp initialization

BOOL CMarvellBootUtilityApp::InitInstance()
{
    // InitCommonControlsEx() is required on Windows XP if an application
    // manifest specifies use of ComCtl32.dll version 6 or later to enable
    // visual styles.  Otherwise, any window creation will fail.
    INITCOMMONCONTROLSEX InitCtrls;
    InitCtrls.dwSize = sizeof(InitCtrls);
    // Set this to include all the common control classes you want to use
    // in your application.
    InitCtrls.dwICC = ICC_WIN95_CLASSES;
    InitCommonControlsEx(&InitCtrls);

    CWinApp::InitInstance();

    // Initialize OLE libraries
    if (!AfxOleInit())
    {
        AfxMessageBox(IDP_OLE_INIT_FAILED);
        return FALSE;
    }

// RAR - the next line is commented out because if enable it leads to a crash
// in the dtor.  this change is recommended in code guru and seems to
// to work with no side effects that impact this application....at least so far!
//	AfxEnableControlContainer();

    // Standard initialization
    // If you are not using these features and wish to reduce the size
    // of your final executable, you should remove from the following
    // the specific initialization routines you do not need
    // Change the registry key under which our settings are stored
    // TODO: You should modify this string to be something appropriate
    // such as the name of your company or organization
    SetRegistryKey(_T("Local AppWizard-Generated Applications"));
    LoadStdProfileSettings(10);  // Load standard INI file options (including MRU)
    // Register the application's document templates.  Document templates
    //  serve as the connection between documents, frame windows and views
    CMultiDocTemplate* pDocTemplate;
    pDocTemplate = new CMultiDocTemplate(IDR_MarvellBootUtilityTYPE,
        RUNTIME_CLASS(CMarvellBootUtilityDoc),
        RUNTIME_CLASS(CChildFrame), // custom MDI child frame
        RUNTIME_CLASS(CMarvellBootUtilityView));
    if (!pDocTemplate)
        return FALSE;
    AddDocTemplate(pDocTemplate);

    // create main MDI Frame window
    CMainFrame* pMainFrame = new CMainFrame;
    if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
    {
        delete pMainFrame;
        return FALSE;
    }
    m_pMainWnd = pMainFrame;
    // call DragAcceptFiles only if there's a suffix
    //  In an MDI app, this should occur immediately after setting m_pMainWnd
    // Enable drag/drop open
    m_pMainWnd->DragAcceptFiles();

    // Enable DDE Execute open
    EnableShellOpen();
    RegisterShellFileTypes(TRUE);

    // Parse command line for standard shell commands, DDE, file open
    CCommandLineInfo cmdInfo;
    ParseCommandLine(cmdInfo);

    // Dispatch commands specified on the command line.  Will return FALSE if
    // app was launched with /RegServer, /Register, /Unregserver or /Unregister.
    if (!ProcessShellCommand(cmdInfo))
        return FALSE;

    CString sTitle = pMainFrame->GetTitle();
#if TRUSTED
    sTitle += " [TRUSTED VERSION]";
#else
    sTitle += " [NON-TRUSTED VERSION]";
#endif
    pMainFrame->SetTitle( sTitle );

    // The main window has been initialized, so show and update it
    pMainFrame->ShowWindow(m_nCmdShow);
    pMainFrame->UpdateWindow();

    POSITION pos = pDocTemplate->GetFirstDocPosition();
    CDocument* pDoc = pDocTemplate->GetNextDoc(pos);
    if ( pDoc )
        pDoc->SetTitle("Host and Target Messages");

    m_pToolsInterfacePropSheet = new CToolsInterfacePropertySheet;
    m_pDKBTimDescriptorPropPage = new CTimDescriptorPropertyPage(*m_pDKBDownloaderInterface);
    m_pOBMTimDescriptorPropPage = new CTimDescriptorPropertyPage(*m_pOBMDownloaderInterface);
    m_pFBFDownloadPropertyPage = new CFBFDownloadPropertyPage(*m_pFBFDownloaderInterface);

    m_pToolsInterfacePropSheet->AddPage( m_pDKBTimDescriptorPropPage );
    m_pToolsInterfacePropSheet->AddPage( m_pOBMTimDescriptorPropPage );
    m_pToolsInterfacePropSheet->AddPage( m_pFBFDownloadPropertyPage );

    return TRUE;
}



// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
    CAboutDlg();

// Dialog Data
    enum { IDD = IDD_ABOUTBOX };

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// App command to run the dialog
void CMarvellBootUtilityApp::OnAppAbout()
{
    CAboutDlg aboutDlg;
    aboutDlg.DoModal();
}


// CMarvellBootUtilityApp message handlers

void CMarvellBootUtilityApp::OnConfigTargetlist()
{
    // create the launch dlg if it does nto already exist
    if ( m_pLaunchDlg == 0 )
    {
        m_pLaunchDlg = new CLaunchDlg;
        if ( m_pLaunchDlg )
            m_pLaunchDlg->Create((UINT)CLaunchDlg::IDD, m_pMainWnd);
    }

    // update the dialog if it exists
    if ( m_pLaunchDlg )
    {
        m_pLaunchDlg->ShowWindow( SW_SHOWNORMAL );
        m_pLaunchDlg->SetFocus();
        m_pLaunchDlg->RefreshState();
    }
}

BOOL CMarvellBootUtilityApp::InitApplication()
{
    return CWinApp::InitApplication();
}

bool CMarvellBootUtilityApp::SaveState()
{
    do
    {
        CFileDialog Dlg( FALSE, _T("*.wtp"),  m_sCurrentProject.c_str(), 
                         OFN_PATHMUSTEXIST | OFN_HIDEREADONLY,
                         _T("Project File (*.wtp)|*.wtp|All Files (*.*)|*.*||") );

        if ( Dlg.DoModal() == IDOK )
            m_sCurrentProject = Dlg.GetPathName();
        else
            return FALSE; // user cancelled
    } while ( m_sCurrentProject == "" );

    ofstream ofs;
    ofs.open( m_sCurrentProject.c_str() );
    
    // write app versioning info in project file
    ofs << sAppTag << endl;
    ofs << AppVersion << endl;
    ofs << AppDate << endl;

    if ( !(ofs.good() && !ofs.fail() && ( m_pDKBDownloaderInterface->SaveState( ofs ) )) )
    {
        AfxMessageBox("Failed to save file DKB (N)TIM Downloader Interface Config.");
        return false;
    }
    
    if ( !(ofs.good() && !ofs.fail() && ( m_pOBMDownloaderInterface->SaveState( ofs ) )) )
    {
        AfxMessageBox("Failed to save file OBM (N)TIM Downloader Interface Config.");
        return false;
    }

    if ( !(ofs.good() && !ofs.fail() && ( m_pFBFDownloaderInterface->SaveState( ofs ) )) )
    {
        AfxMessageBox("Failed to save file FBF Downloader Interface Config.");
        return false;
    }

    ofs.close();

    Changed( false );

    return true;
}

bool CMarvellBootUtilityApp::LoadState()
{
    do
    {
        CFileDialog Dlg( TRUE, _T("*.wtp"), m_sCurrentProject.c_str(), 
                         OFN_PATHMUSTEXIST | OFN_HIDEREADONLY,
                         _T("Project File (*.wtp)|*.wtp|All Files (*.*)|*.*||") );
        
        if ( Dlg.DoModal() == IDOK )
            m_sCurrentProject = Dlg.GetPathName();
        else
            return false; // user cancelled
    } while ( m_sCurrentProject == "" );

    ifstream ifs;
    ifs.open( m_sCurrentProject.c_str() );

    string sFileTag;
    ::getline( ifs, sFileTag );
    if ( sFileTag != sAppTag )
    {
        AfxMessageBox( "File is not a recognized .wtp file.  File not loaded." );
        return false;
    }

    // read app version info used to create project file
    string sbuf;
    ::getline( ifs, sbuf );
    ProjectVersion = atoi(sbuf.c_str());
    ::getline( ifs, sbuf );
    ProjectDate = atoi(sbuf.c_str());

    // Discards all tim descriptor line 
//	m_pDKBDownloaderInterface->TimDescriptorParser.TimDescriptor().DiscardTimDescriptorLines();

    bool bRet = true;
    if ( !(ifs.good() && !ifs.fail() && ( m_pDKBDownloaderInterface->LoadState( ifs ) )) )
    {
        AfxMessageBox("Failed to load file DKB (N)TIM Downloader Interface Config.");
        bRet = false;
        goto Exit;
    }

    if ( !(ifs.good() && !ifs.fail() && ( m_pOBMDownloaderInterface->LoadState( ifs ) )) )
    {
        AfxMessageBox("Failed to load file OBM (N)TIM Downloader Interface Config.");
        bRet = false;
        goto Exit;
    }

    if ( !(ifs.good() && !ifs.fail() && ( m_pFBFDownloaderInterface->LoadState( ifs ) )) )
    {
        AfxMessageBox("Failed to load file FBF Downloader Interface Config.");
        bRet = false;
        goto Exit;
    }

Exit:
    ifs.close();

    if ( bRet == false )
    { 
        m_pDKBDownloaderInterface->Reset();
        m_pOBMDownloaderInterface->Reset();
        m_pFBFDownloaderInterface->Reset();
    }
    
    RefreshModelessDialogsState();

    return bRet; // SUCCESS
}

void CMarvellBootUtilityApp::RefreshModelessDialogsState()
{
    if ( m_pLaunchDlg )
        m_pLaunchDlg->RefreshState();
//	if ( m_pToolsInterfacePropSheet )
//		m_pToolsInterfacePropSheet->RefreshState();
}

void CMarvellBootUtilityApp::DisplayMsg(CString& sMsg)
{
    POSITION pos = GetFirstDocTemplatePosition();
    CDocTemplate* pDocTemplate = GetNextDocTemplate(pos);
    pos = pDocTemplate->GetFirstDocPosition();
    CDocument* pDoc = pDocTemplate->GetNextDoc(pos);
    if ( pDoc != NULL )
    {
        if ( IsValidDoc( pDoc ) )
        {
            POSITION pos = pDoc->GetFirstViewPosition();
            CMarvellBootUtilityView* pView = dynamic_cast<CMarvellBootUtilityView*>((CEditView*)pDoc->GetNextView(pos));
            if ( pView )
            {
                // make sure control can handle all excessive output
                if ( pView->GetEditCtrl().GetLimitText() != -1 )
                    pView->GetEditCtrl().SetLimitText(-1); 

                sMsg.Replace( "\n", "\r\n" );
                pView->GetEditCtrl().ReplaceSel( sMsg );
            }
        }
    }

}

void CMarvellBootUtilityApp::RefreshTargetsList()
{
    if ( m_pLaunchDlg )
        m_pLaunchDlg->RefreshState();
}

bool CMarvellBootUtilityApp::AddTarget( CString& sInterfaceName )
{
    if ( sInterfaceName != "" )
    {
        CTarget* pTarget = 0;
        t_TargetsIter iter = m_Targets.begin();

        // see if target is already in the target list, update Connected State
        while( iter != m_Targets.end() )
        {
            pTarget = *iter++;
            if ( pTarget && pTarget->InterfaceName().c_str() == sInterfaceName )
            {
                pTarget->Connected(true);
                RefreshTargetsList();

                if ( theApp.DownloadController().m_bDownloading || theApp.DownloadController().m_bLaunchOnDiscovery )
                    m_pDownloadController->ConditionalLaunchDownload( pTarget );

                RefreshModelessDialogsState();

                return false;
            }
        }

        // add a new target
        pTarget = new CTarget();
        if ( pTarget )
        {
            pTarget->InterfaceName( ((LPCTSTR)sInterfaceName) );
            m_Targets.push_back( pTarget );
            pTarget->Connected( true );
            RefreshTargetsList();

            if ( theApp.DownloadController().m_bDownloading || theApp.DownloadController().m_bLaunchOnDiscovery )
                m_pDownloadController->ConditionalLaunchDownload( pTarget );

            RefreshModelessDialogsState();

            return true;
        }
    }

    RefreshTargetsList();
    return false;
}

bool CMarvellBootUtilityApp::RemoveTarget( CString& sInterfaceName )
{
    CTarget* pTarget = 0;
    t_TargetsIter iter = m_Targets.begin();

    while( iter != m_Targets.end() )
    {
        pTarget = *iter;
        if ( pTarget && pTarget->InterfaceName().c_str() == sInterfaceName )
        {
            m_Targets.remove(*iter);
            pTarget->Connected(false);

            if ( pTarget->TargetState() == CTarget::TARGET_DOWNLOADING )
                pTarget->TerminateDownload();
            else
                delete pTarget;
            
            RefreshTargetsList();
            return true;
        }
        iter++;
    }

    RefreshTargetsList();
    return false;
}

bool CMarvellBootUtilityApp::StartDownloadTarget( CString& sInterfaceName )
{
    // launch by Downloader Launch button to selected device type
    if ( sInterfaceName == "UART" )
    {
        if ( m_UARTTarget.DownloaderInterface() == 0 )
        {
            ASSERT(0);
            AfxMessageBox( "No Downloader Interface assigned to Target!" );
            return false;
        }

        if ( m_UARTTarget.DownloaderInterface()->WtptpExePath().length() == 0 )
        {
            AfxMessageBox( "WtpDownload path is not assigned in Downloader Configuration" );
            return false;
        }

        m_UARTTarget.StartDownload();
        return true;
    }

    if ( m_Targets.size() == 0 )
    {
        AfxMessageBox( "No connected USB Target devices detected.  Try reconnecting." );
        return false;
    }	
    
    CTarget* pTarget = 0;
    t_TargetsIter iter = m_Targets.begin();

    // launch by Downloader Launch button to selected device type
    if ( sInterfaceName == "USB" )
    {
        if ( m_Targets.size() == 1 )
            pTarget = *iter;
    }
    else
    {
        while( iter != m_Targets.end() )
        {
            if ( (*iter) && (*iter)->InterfaceName().c_str() == sInterfaceName )
            {
                pTarget = *iter;
                break;
            }
            iter++;
        }
    }

    if ( pTarget )
    {
        if ( pTarget->TargetState() == CTarget::TARGET_IDLE )
            m_pDownloadController->ConditionalLaunchDownload( pTarget );
    }

    OnConfigTargetlist();
    RefreshModelessDialogsState();

    return false;
}

bool CMarvellBootUtilityApp::StartDownloadAllTargets()
{
    if ( m_Targets.size() == 0 )
    {
        AfxMessageBox( "No targets found to download image" );
        return false; 
    }

    CTarget* pTarget = 0;
    t_TargetsIter iter = m_Targets.begin();

    // first, serially download the first found bootrom target
    while( iter != m_Targets.end() )
    {
        pTarget = *iter++;
        if ( pTarget && pTarget->TargetState() == CTarget::TARGET_IDLE )
        {
            m_pDownloadController->ConditionalLaunchDownload( pTarget );
            RefreshModelessDialogsState();
        }
    }

    RefreshTargetsList();
    return true;
}

bool CMarvellBootUtilityApp::StopDownloadTarget( CString& sInterfaceName )
{
    // launch by Downloader Launch button to selected device type
    if ( sInterfaceName == "UART" )
    {
        if ( m_UARTTarget.TargetState() == CTarget::TARGET_DOWNLOADING )
                m_UARTTarget.TerminateDownload();
        return true;
    }

    // launch by Downloader Launch button to selected device type
    if ( sInterfaceName == "USB" )
    {
        if ( m_Targets.size() == 1 )
        { 
            CTarget* pTarget = *m_Targets.begin();
            if ( pTarget && pTarget->TargetState() == CTarget::TARGET_DOWNLOADING )
                pTarget->TerminateDownload();
        }

        RefreshTargetsList();
        return true;
    }

    CTarget* pTarget = 0;
    t_TargetsIter iter = m_Targets.begin();

    while( iter != m_Targets.end() )
    {
        pTarget = *iter++;
        if ( pTarget && pTarget->InterfaceName().c_str() == sInterfaceName )
        {
            if ( pTarget->DownloaderProcessInfo().hProcess != 0 )
                pTarget->TerminateDownload();

            RefreshTargetsList();
            return true;
        }
    }

    RefreshTargetsList();
    return false;
}

bool CMarvellBootUtilityApp::StopDownloadAllTargets()
{
    CTarget* pTarget = 0;
    t_TargetsIter iter = m_Targets.begin();

    while( iter != m_Targets.end() )
    {
        pTarget = *iter++;
        if ( pTarget && pTarget->TargetState() == CTarget::TARGET_DOWNLOADING )
            pTarget->TerminateDownload();
    }
    
    RefreshTargetsList();

    return false;
}

void CMarvellBootUtilityApp::OnFileLoadproject()
{
    if ( IsChanged() )
    {
        if ( IDYES == AfxMessageBox( "Save changes to current project?", MB_YESNO ) )
        {
            if ( SaveState() )
            {
                AfxMessageBox( "Current project saved." );
                Changed(false);
            }
        }
    }
    if (!LoadState())
        return;

    Changed(false);

    CString sTitle( "MarvellBootUtility" );
#if TRUSTED
    sTitle += " [TRUSTED VERSION] {";
#else
    sTitle += " [NON-TRUSTED VERSION] {";
#endif
    sTitle += m_sCurrentProject.c_str();
    sTitle += "}";

    m_pMainWnd->GetTopLevelFrame()->SetTitle( sTitle );
    m_pMainWnd->GetTopLevelFrame()->OnUpdateFrameTitle(true);
    if ( m_pToolsInterfacePropSheet )
        m_pToolsInterfacePropSheet->RefreshState();

    CString sProjectLoadedMsg;
    sProjectLoadedMsg = "Project Loaded: ";
    sProjectLoadedMsg += m_sCurrentProject.c_str();
    sProjectLoadedMsg += "\n";
    DisplayMsg( sProjectLoadedMsg );
}	

void CMarvellBootUtilityApp::OnFileSaveproject()
{
    SaveState();
    Changed(false);

    CString sTitle( "MarvellBootUtility" );
#if TRUSTED
    sTitle += " [TRUSTED VERSION] {";
#else
    sTitle += " [NON-TRUSTED VERSION] {";
#endif
    sTitle += m_sCurrentProject.c_str();
    sTitle += "}";

    m_pMainWnd->GetTopLevelFrame()->SetTitle( sTitle );
    m_pMainWnd->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}

bool CMarvellBootUtilityApp::IsChanged()
{
    bool bChanged = m_pDKBDownloaderInterface->IsChanged()
        || m_pOBMDownloaderInterface->IsChanged()
        || m_pFBFDownloaderInterface->IsChanged();

    return (m_bChanged |= bChanged);
}

void CMarvellBootUtilityApp::Changed( bool bSet )
{ 
    m_bChanged = bSet;

    m_pDKBDownloaderInterface->Changed( bSet );
    m_pOBMDownloaderInterface->Changed( bSet );
    m_pFBFDownloaderInterface->Changed( bSet );
}

CDocument* CMarvellBootUtilityApp::NewTargetLog( string& sTargetName )
{
    string sTargetStr = ((CMainFrame*)m_pMainWnd)->CreateTitle( sTargetName );

    POSITION pos = GetFirstDocTemplatePosition();
    CDocTemplate* pDocTemplate = theApp.GetNextDocTemplate(pos);

    pos = pDocTemplate->GetFirstDocPosition();
    CDocument* pDoc = pDocTemplate->GetNextDoc(pos);
    while ( pDoc )
    {
        // reuse existing doc for this device
        CString sDocTitle = pDoc->GetTitle();
        if ( sDocTitle == sTargetStr.c_str() )
            return pDoc;

        if ( pos == 0 )
            break;

        pDoc = pDocTemplate->GetNextDoc(pos);
    }

    // we have anew device so create a new doc for the device's messages
    pDoc = pDocTemplate->CreateNewDocument();
    if ( pDoc )
    {
        pDoc->SetTitle( sTargetStr.c_str() );
        CFrameWnd* pFrame = pDocTemplate->CreateNewFrame( pDoc, 0 );
        if ( pFrame )
        {
            pFrame->InitialUpdateFrame( pDoc, TRUE );
            pFrame->UpdateFrameTitleForDocument( sTargetStr.c_str() );
        }
    }

    return pDoc;
}


bool CMarvellBootUtilityApp::IsValidDoc( CDocument* pRefDoc )
{
    POSITION pos = GetFirstDocTemplatePosition();
    CDocTemplate* pDocTemplate = GetNextDocTemplate(pos);
    pos = pDocTemplate->GetFirstDocPosition();
    CDocument* pDoc = pDocTemplate->GetNextDoc(pos);
    while ( pDoc )
    {
        if ( pDoc == pRefDoc || pos == 0 )
            return true;

        pDoc = pDocTemplate->GetNextDoc(pos);
    }

    return false;
}


bool CMarvellBootUtilityApp::DocumentClosing( CMarvellBootUtilityDoc* pDoc )
{
    if ( pDoc != NULL )
    {
        if ( IsValidDoc( pDoc ) )
        {
            if ( pDoc->GetTitle().Find("Host and Target Messages") != -1 )
            {
                POSITION pos = pDoc->GetFirstViewPosition();
                CMarvellBootUtilityView* pView = dynamic_cast<CMarvellBootUtilityView*>((CEditView*)pDoc->GetNextView(pos));
                if ( pView )
                {
                    pView->GetEditCtrl().SetSel(0,-1);
                    pView->GetEditCtrl().Cut();
                    pView->GetEditCtrl().UpdateWindow();
                    // make sure control can handle all excessive output
                    if ( pView->GetEditCtrl().GetLimitText() != -1 )
                        pView->GetEditCtrl().SetLimitText(-1); 					
                }
                return false;
            }
        }
        else
            return false;
    }
    else
        return false;


    CTarget* pTarget = 0;
    t_TargetsIter iter = m_Targets.begin();

    while( iter != m_Targets.end() )
    {
        pTarget = *iter++;
        if ( pTarget && pDoc ==  pTarget->Document() )
        {
            pTarget->CloseDocument();
            return true;
        }
    }

    if ( pDoc == m_UARTTarget.Document() )
    {
        m_UARTTarget.CloseDocument();
        return true;
    }

    return true;

#if 0
    POSITION pos = GetFirstDocTemplatePosition();
    CDocTemplate* pDocTemplate = GetNextDocTemplate(pos);
    pos = pDocTemplate->GetFirstDocPosition();

    CDocument* pDoc = 0;
    pos = pDocTemplate->GetFirstDocPosition();
    if ( pos == 0 )
    {
        // we have anew device so create a new doc for the device's messages
        pDoc = pDocTemplate->CreateNewDocument();
        if ( pDoc )
            pDoc->SetTitle( "Host and Target Messages" );
    }
    else
    {
        pDoc = pDocTemplate->GetNextDoc(pos);
    }
#endif

}


void CMarvellBootUtilityApp::OnAppExit()
{
    if ( IsChanged() )
    {
        if ( IDYES == AfxMessageBox("Save changes to current project?", MB_YESNO ) )
        {
            if ( SaveState() )
            {
                AfxMessageBox("Current project saved." );
                Changed(false);
            }
            else
                return;
        }
    }

    m_pDKBDownloaderInterface->TimDescriptorParser.TimDescriptor().DiscardTimDescriptorLines();
    m_pOBMDownloaderInterface->TimDescriptorParser.TimDescriptor().DiscardTimDescriptorLines();

    CWinApp::OnAppExit();
}

BOOL CMarvellBootUtilityApp::SaveAllModified()
{
    if ( IsChanged() )
    {
        if ( IDYES == AfxMessageBox("Save changes to current project?", MB_YESNO ) )
        {
            if ( SaveState() )
            {
                AfxMessageBox("Current project saved." );
                Changed(false);
            }
            else
                return FALSE;
        }
    }

    return CWinApp::SaveAllModified();
}

void CMarvellBootUtilityApp::OnConfigDKBDownloader()
{
    if ( m_pToolsInterfacePropSheet->m_hWnd == 0 )
        m_pToolsInterfacePropSheet->Create();
    
    m_pToolsInterfacePropSheet->SetActivePage(0);
    m_pToolsInterfacePropSheet->ShowWindow(SW_SHOW);
}

void CMarvellBootUtilityApp::OnConfigOBMDownloader()
{
    if ( m_pToolsInterfacePropSheet->m_hWnd == 0 )
        m_pToolsInterfacePropSheet->Create();

    m_pToolsInterfacePropSheet->SetActivePage(1);
    m_pToolsInterfacePropSheet->ShowWindow(SW_SHOW);
}


void CMarvellBootUtilityApp::OnConfigFbfDownloader()
{
    if ( m_pToolsInterfacePropSheet->m_hWnd == 0 )
        m_pToolsInterfacePropSheet->Create();

    m_pToolsInterfacePropSheet->SetActivePage(2);
    m_pToolsInterfacePropSheet->ShowWindow(SW_SHOW);
}

int CMarvellBootUtilityApp::ExitInstance()
{
    return CWinApp::ExitInstance();
}

void CMarvellBootUtilityApp::RefreshDownloadFlag()
{
    t_TargetsIter iter = TargetList().begin();
    // update Downloading State
    while( iter != TargetList().end() )
    {
        if ( (*iter++)->TargetState() == CTarget::TARGET_DOWNLOADING )
            return;
    }

    if ( iter == m_Targets.end() )
        m_pDownloadController->m_bDownloading = false;
}

