/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once

#include "EditListCtrl.h"
#include "DDROperations.h"

// CDDROperationsDlg dialog
class CTimDescriptor;

class CDDROperationsDlg : public CDialog
{
	DECLARE_DYNAMIC(CDDROperationsDlg)

public:
	CDDROperationsDlg(CTimDescriptor& TimDescriptor, t_DDROperationList& DdrOperations, CWnd* pParent = NULL);   // standard constructor
	virtual ~CDDROperationsDlg();

// Dialog Data
	enum { IDD = IDD_DDR_OPERATIONS_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl OperationsListCtrl;
	CButton InsertBtn;
	CButton RemoveBtn;
	CButton MoveUpBtn;
	CButton MoveDownBtn;

	CComboBox OperationIdCb;
	CEdit OperationIDTagEdt;
	CHexItemEdit OperationValueEdt;

	afx_msg void OnBnClickedInsertBtn();
	afx_msg void OnBnClickedRemoveBtn();
	afx_msg void OnBnClickedMoveUp();
	afx_msg void OnBnClickedMoveDown();
	afx_msg void OnCbnSelchangeOperationIdCb();
	afx_msg void OnEnChangeOperationValueEdt();
	afx_msg void OnBnClickedOk();
	afx_msg void OnLvnItemActivateOperationsList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemChangedOperationsList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickOperationsList(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL OnInitDialog();

	void RefreshState();
	void RefreshOperationState();
	void UpdateControls();
	void UpdateChangeMarkerInTitle();

	t_DDROperationList	m_DdrOperations;
	CTimDescriptor	m_LocalTimDescriptor;

	int m_OpCbSel;
	bool m_bRefreshState;
};
