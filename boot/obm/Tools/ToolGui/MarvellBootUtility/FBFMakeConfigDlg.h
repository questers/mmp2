/******************************************************************************
 *
 *  (C)Copyright 2005 - 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
// FBF_Make_DLGDlg.h : header file
//

#pragma once

#include "FBFMakeConfig.h"

// CFBFMakeConfigDlg dialog
class CFBFMakeConfigDlg : public CDialog
{
    DECLARE_DYNAMIC(CFBFMakeConfigDlg)

// Construction
public:
    CFBFMakeConfigDlg(CFBFMakeConfig& FBFmakecfg, CWnd* pParent = NULL);	// standard constructor
    virtual ~CFBFMakeConfigDlg();

// Dialog Data
    enum { IDD = IDD_FBF_MAKE_DLG };

// Implementation
protected:
    // Generated message map functions
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual BOOL OnInitDialog();

    DECLARE_MESSAGE_MAP()
public:
    CEdit mFBFcmd;
    CEdit mFBFmake_ini_file;
    CEdit mFBFmake_path;
    CEdit mFBFfile;

    CString sFBFfile;
    CString cmdline;
    CString sFBFMakePath;
    CString sFBFmake_ini_file;
    CString sFBFmakeMode;
    CString sFBFmakeReset;
    CString iniArg;
    CString fbfArg;

    CButton     mFBFmakeMode;
    CComboBox   cbFBFmode;
    CString     sFBFmode;     
    CComboBox   cbFBFreset;
    CString     sFBFreset;
    CButton     mFBFmakeReset;
    
    afx_msg void OnEnChangeFbfMakeExePath();
    afx_msg void OnEnChangeIniPath();
    afx_msg void OnBnClickedFbfMakeExeBrowseBtn();
    afx_msg void OnBnClickedIniBrowseBtn();
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedCancel();
    afx_msg void OnBnClickedFbfBrowseBtn();
    afx_msg void OnEnChangeFbfPath();
    afx_msg void OnBnClickedFbfMakeMode();
    afx_msg void OnBnClickedFbfMakeReset();
    afx_msg void OnBnClickedFbfMakeLaunch();
    afx_msg void OnCbnSelchangeFbfModeChoices();
    afx_msg void OnCbnSelchangeFbfResetChoices();

public:
    BOOL			bFBFmakeMode;
    int				ModeOpt;
    BOOL			bFBFmakeReset;
    int				ResetOpt;
    CFBFMakeConfig	cpFBFmakecfg;	// working copy of original 

    void update_fbf_cmd();
    void setMode();
    void setReset();
    bool validateAll();
    bool validateFBFmakePath();
    bool validateFBFiniPath();
    bool validateFBFpath();
    void initDlgVals();
    void initComboBoxes();
};
