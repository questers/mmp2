/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

// CConsumerPackage dialog
class CTimDescriptor;
class CConsumerID;

class CConsumerPackageDlg : public CDialog
{
	DECLARE_DYNAMIC(CConsumerPackageDlg)

public:
	CConsumerPackageDlg(CTimDescriptor& TimDescriptor, CConsumerID* pConsumerID = NULL, CWnd* pParent = NULL);   // standard constructor
	virtual ~CConsumerPackageDlg();

// Dialog Data
	enum { IDD = IDD_CONSUMER_PKG_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CComboBox ConsumerIdCb;

	CListCtrl PackageIdListCtrl;
	CButton InsertPkgBtn;	// <<<
	CButton RemovePkgBtn;	// >>>
	CButton MovePkgUpBtn;
	CButton MovePkgDownBtn;
	CListCtrl ConsummablePkgsListCtrl;

	afx_msg void OnCbnSelchangeConsumerIdCb();
	afx_msg void OnBnClickedInsertPkgBtn();
	afx_msg void OnBnClickedRemovePkgBtn();
	afx_msg void OnBnClickedPkgMoveUpBtn();
	afx_msg void OnBnClickedPkgMoveDownBtn();
	afx_msg void OnNMClickPackageIdsList(NMHDR *pNMHDR, LRESULT *pResult);

	virtual BOOL OnInitDialog();

	void UpdateControls();
	void RefreshState();
	void UpdateChangeMarkerInTitle();

	CTimDescriptor	m_LocalTimDescriptor;
	CConsumerID*	m_pConsumerID;
};
