/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

class CImportDlg : public CDialog
{
	DECLARE_DYNAMIC(CImportDlg)

public:
	CImportDlg(CTimDescriptor& rTimDescriptor, CWnd* pParent = NULL);   // standard constructor
	virtual ~CImportDlg();

// Dialog Data
	enum { IDD = IDD_IMPORT_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	CString sDdrXdbScriptFilepath;
	CStatic ImportedXdbScriptTxt;
	CString sTimBinaryFilepath;
	CStatic ImportedTimBinaryTxt;
	CComboBox ProcessorTypeCb;

	afx_msg void OnBnClickedDdrXdbScriptBrowseBtn();
	afx_msg void OnEnChangeDdrXdbScriptFilePathEdt();
	afx_msg void OnBnClickedImportDdrXdbFileBtn();

	afx_msg void OnBnClickedTimBinaryFileBrowseBtn();
	afx_msg void OnBnClickedImportTimBinaryFileBtn();
	afx_msg void OnEnChangeTimBinaryFilePathEdt();
	afx_msg void OnCbnSelchangeTimProcCb();

	virtual BOOL OnInitDialog();

public:
	void SetTimText( string& sText );

	void UpdateControls();

	CTimDescriptor TimDescriptor;

	string		 sTimText;
	t_stringList Lines;
	bool		 bImportedXdbScript;
	bool		 bImportedTimBinary;
};
