/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
// TzInitializationPkgDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "TzInitializationPkgDlg.h"
#include "InstructionsDlg.h"
#include "TzOperationsDlg.h"


// CTzInitializationPkgDlg dialog

IMPLEMENT_DYNAMIC(CTzInitializationPkgDlg, CDialog)

CTzInitializationPkgDlg::CTzInitializationPkgDlg(CTimDescriptor& TimDescriptor, CTzInitialization& TzInit, CWnd* pParent /*=NULL*/)
	: CDialog(CTzInitializationPkgDlg::IDD, pParent),
	m_LocalTimDescriptor( TimDescriptor ), 
	m_TzInit( TzInit )
{
	if ( m_TzInit.m_sTzPID == "" )
		m_TzInit.m_sTzPID = "TZRI";

	m_bRefreshState = false;

}

CTzInitializationPkgDlg::~CTzInitializationPkgDlg()
{
}

void CTzInitializationPkgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_INSTRUCTIONS_BTN, InstructionsBtn);
	DDX_Control(pDX, IDC_OPERATIONS_BTN, OperationsBtn);
	DDX_Control(pDX, IDC_PKG_ID_EDT, PackageIdEdt);
	DDX_Control(pDX, IDC_PKG_TAG_EDT, PackageTagEdt);

	// updates marker in title
	UpdateChangeMarkerInTitle();
	theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CTzInitializationPkgDlg, CDialog)
	ON_BN_CLICKED(IDC_INSTRUCTIONS_BTN, &CTzInitializationPkgDlg::OnBnClickedInstructionsBtn)
	ON_BN_CLICKED(IDC_OPERATIONS_BTN, &CTzInitializationPkgDlg::OnBnClickedOperationsBtn)
	ON_EN_CHANGE(IDC_PKG_ID_EDT, &CTzInitializationPkgDlg::OnEnChangePkgIdEdt)
END_MESSAGE_MAP()


// CTzInitializationPkgDlg message handlers

BOOL CTzInitializationPkgDlg::OnInitDialog()
{
	m_bRefreshState = false;

	CDialog::OnInitDialog();
	RefreshState();
	PackageIdEdt.EnableWindow( GetWindowTextLengthA() !=0 );

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTzInitializationPkgDlg::RefreshState()
{
	m_bRefreshState = true;
	PackageIdEdt.SetWindowTextA( m_TzInit.m_sTzPID.c_str() );

	string sTag;
	m_TzInit.TextToHexFormattedAscii( sTag, m_TzInit.m_sTzPID );
	PackageTagEdt.SetWindowTextA( sTag.c_str() );
	m_bRefreshState = false;
}

void CTzInitializationPkgDlg::UpdateControls()
{

}

void CTzInitializationPkgDlg::OnBnClickedInstructionsBtn()
{
	CInstructionsDlg Dlg( m_LocalTimDescriptor, m_TzInit.m_TzInstructions.m_Instructions );
	if ( IDOK == Dlg.DoModal() )
	{
		m_TzInit.m_TzInstructions.m_Instructions = Dlg.m_Instructions;

		return;
	}
}

void CTzInitializationPkgDlg::OnBnClickedOperationsBtn()
{
	CTzOperationsDlg Dlg( m_LocalTimDescriptor, m_TzInit.m_TzOperations.m_TzOperations );
	if ( IDOK == Dlg.DoModal() )
	{
		m_TzInit.m_TzOperations.m_TzOperations = Dlg.m_TzOperations;

		return;
	}
}


void CTzInitializationPkgDlg::OnEnChangePkgIdEdt()
{
	if ( !m_bRefreshState )
	{
		UpdateData(TRUE);
		CString sText;
		PackageIdEdt.GetWindowTextA( sText );
		m_TzInit.m_sTzPID = CStringA(sText);
		m_TzInit.m_sTzPID = m_TzInit.ToUpper(m_TzInit.m_sTzPID);

		string sTag;
		m_TzInit.TextToHexFormattedAscii( sTag, m_TzInit.m_sTzPID );
		PackageTagEdt.SetWindowTextA( sTag.c_str() );
	}
}

void CTzInitializationPkgDlg::UpdateChangeMarkerInTitle()
{
	if ( m_hWnd == 0 )
		return;

	CString sTitle;
	GetWindowTextA( sTitle );
	// add or remove * as appropriate
	if ( m_LocalTimDescriptor.ReservedIsChanged() )
	{
		if ( -1 == sTitle.Find('*') )
			sTitle += "*";
	}
	else
	{
		if ( -1 != sTitle.Find('*') )
			sTitle.Remove('*');
	}
	SetWindowTextA(sTitle);
	UpdateWindow();
}
