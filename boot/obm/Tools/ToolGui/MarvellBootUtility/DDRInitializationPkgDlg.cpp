/******************************************************************************
 *
 *  (C)Copyright 2011 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
// DDRInitializationPkgDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "DDRInitializationPkgDlg.h"
#include "InstructionsDlg.h"
#include "DDROperationsDlg.h"


// CDDRInitializationPkgDlg dialog

IMPLEMENT_DYNAMIC(CDDRInitializationPkgDlg, CDialog)

CDDRInitializationPkgDlg::CDDRInitializationPkgDlg(CTimDescriptor& TimDescriptor, CDDRInitialization& DDRInit, CWnd* pParent /*=NULL*/)
    : CDialog(CDDRInitializationPkgDlg::IDD, pParent),
    m_LocalTimDescriptor( TimDescriptor ), 
    m_DDRInit( DDRInit )
{
    if ( m_DDRInit.m_sDdrPID == "" )
        m_DDRInit.m_sDdrPID = "DDR";

    m_bRefreshState = false;

}

CDDRInitializationPkgDlg::~CDDRInitializationPkgDlg()
{
}

void CDDRInitializationPkgDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_INSTRUCTIONS_BTN, InstructionsBtn);
    DDX_Control(pDX, IDC_OPERATIONS_BTN, OperationsBtn);
    DDX_Control(pDX, IDC_PKG_ID_EDT, PackageIdEdt);
    DDX_Control(pDX, IDC_PKG_TAG_EDT, PackageTagEdt);

    // updates marker in title
    UpdateChangeMarkerInTitle();
    theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}


BEGIN_MESSAGE_MAP(CDDRInitializationPkgDlg, CDialog)
    ON_BN_CLICKED(IDC_INSTRUCTIONS_BTN, &CDDRInitializationPkgDlg::OnBnClickedInstructionsBtn)
    ON_BN_CLICKED(IDC_OPERATIONS_BTN, &CDDRInitializationPkgDlg::OnBnClickedOperationsBtn)
    ON_EN_CHANGE(IDC_PKG_ID_EDT, &CDDRInitializationPkgDlg::OnEnChangePkgIdEdt)
END_MESSAGE_MAP()


// CDDRInitializationPkgDlg message handlers

BOOL CDDRInitializationPkgDlg::OnInitDialog()
{
    m_bRefreshState = false;

    CDialog::OnInitDialog();
    RefreshState();
    PackageIdEdt.EnableWindow( GetWindowTextLengthA() !=0 );

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}

void CDDRInitializationPkgDlg::RefreshState()
{
    m_bRefreshState = true;
    PackageIdEdt.SetWindowTextA( m_DDRInit.m_sDdrPID.c_str() );

    string sTag;
    m_DDRInit.TextToHexFormattedAscii( sTag, m_DDRInit.m_sDdrPID );
    PackageTagEdt.SetWindowTextA( sTag.c_str() );
    m_bRefreshState = false;
}

void CDDRInitializationPkgDlg::UpdateControls()
{

}

void CDDRInitializationPkgDlg::OnBnClickedInstructionsBtn()
{
    CInstructionsDlg Dlg( m_LocalTimDescriptor, m_DDRInit.m_DdrInstructions.m_Instructions );
    if ( IDOK == Dlg.DoModal() )
    {
        m_DDRInit.m_DdrInstructions.m_Instructions = Dlg.m_Instructions;

        return;
    }
}

void CDDRInitializationPkgDlg::OnBnClickedOperationsBtn()
{
    CDDROperationsDlg Dlg( m_LocalTimDescriptor, m_DDRInit.m_DdrOperations.m_DdrOperations );
    if ( IDOK == Dlg.DoModal() )
    {
        m_DDRInit.m_DdrOperations.m_DdrOperations = Dlg.m_DdrOperations;

        return;
    }
}


void CDDRInitializationPkgDlg::OnEnChangePkgIdEdt()
{
    if ( !m_bRefreshState )
    {
        UpdateData(TRUE);
        CString sText;
        PackageIdEdt.GetWindowTextA( sText );
        m_DDRInit.m_sDdrPID = CStringA(sText);
        m_DDRInit.m_sDdrPID = m_DDRInit.ToUpper(m_DDRInit.m_sDdrPID);

        string sTag;
        m_DDRInit.TextToHexFormattedAscii( sTag, m_DDRInit.m_sDdrPID );
        PackageTagEdt.SetWindowTextA( sTag.c_str() );
    }
}

void CDDRInitializationPkgDlg::UpdateChangeMarkerInTitle()
{
    if ( m_hWnd == 0 )
        return;

    CString sTitle;
    GetWindowTextA( sTitle );
    // add or remove * as appropriate
    if ( m_LocalTimDescriptor.ReservedIsChanged() )
    {
        if ( -1 == sTitle.Find('*') )
            sTitle += "*";
    }
    else
    {
        if ( -1 != sTitle.Find('*') )
            sTitle.Remove('*');
    }
    SetWindowTextA(sTitle);
    UpdateWindow();
}
