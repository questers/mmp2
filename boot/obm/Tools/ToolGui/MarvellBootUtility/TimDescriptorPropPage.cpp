/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

// TimDescriptorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MarvellBootUtility.h"
#include "TimDescriptorPropPage.h"
#include "ToolsInterfacePropertySheet.h"
#include "ImageConfigurationDlg.h"
#include "ReservedDataDlg.h"
#include "CommentDlg.h"
#include "TimTextViewDlg.h"
#include "ImportDlg.h"

#if TRUSTED
#include "KeysDlg.h"
#include "PublicKeyExponentDlg.h"
#include "DigitalSignatureDlg.h"
#endif

// images list ctrl column numbers
const int IMAGE_ID = 0;
const int IMAGE_ID_TAG = 1;
const int FLASH_ADDRESS = 2;
const int LOAD_ADDRESS = 3;
const int IMAGE_FILE_PATH = 4;

// CTimDescriptorPropertyPage dialog

IMPLEMENT_DYNAMIC(CTimDescriptorPropertyPage, CPropertyPage)

CTimDescriptorPropertyPage::CTimDescriptorPropertyPage(CDownloaderInterface& rDownloaderInterface)
    : CPropertyPage(CTimDescriptorPropertyPage::IDD)
    , DownloaderInterface(rDownloaderInterface)
    , TimDescriptor(rDownloaderInterface.TimDescriptorParser.TimDescriptor())
    , ImageBuildDlg(rDownloaderInterface)
    , TimDownloaderDlg(rDownloaderInterface)
{
    bTrusted = TimDescriptor.Trusted();
}

CTimDescriptorPropertyPage::~CTimDescriptorPropertyPage()
{
}

void CTimDescriptorPropertyPage::DoDataExchange(CDataExchange* pDX)
{
    CPropertyPage::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_TIM_DESCRIPTOR_FILE_PATH_EDT, sTimDescriptorFilePath);
    DDX_Control(pDX, IDC_TIM_VERSION_CB, TimVersionCb);
    DDX_Text(pDX, IDC_ISSUE_DATE_EDT, sIssueDate);
    DDX_Text(pDX, IDC_OEM_UNIQUE_ID_EDT, sOemUniqueId);
    DDX_Text(pDX, IDC_BOOTROM_FLASH_SIGNATURE_EDT, sBootRomFlashSignature);
    DDX_Check(pDX, IDC_TRUSTED_CHK, bTrusted);
    DDX_Text(pDX, IDC_WTM_FLASH_SIGNATURE_EDT, sWtmFlashSignature);
    DDX_Text(pDX, IDC_WTM_FLASH_ENTRY_EDT, sWtmFlashEntryAddress);
    DDX_Text(pDX, IDC_WTM_BACKUP_ENTRY_EDT, sWtmBackupEntryAddress);
    DDX_Control(pDX, IDC_WTM_FLASH_SIGNATURE_EDT, WtmFlashSignatureEdt);
    DDX_Control(pDX, IDC_WTM_FLASH_ENTRY_EDT, WtmFlashEntryAddressEdt);
    DDX_Control(pDX, IDC_WTM_BACKUP_ENTRY_EDT, WtmBackupEntryEdt);
    DDX_Control(pDX, IDC_ISSUE_DATE_EDT, IssueDateEdt);
    DDX_Control(pDX, IDC_OEM_UNIQUE_ID_EDT, OemUniqueIdEdt);
    DDX_Control(pDX, IDC_BOOTROM_FLASH_SIGNATURE_EDT, BootromFlashSignatureEdt);
    DDX_Control(pDX, IDC_DIGITAL_SIGNATURE_BTN, DigitalSignatureBtn);
    DDX_Control(pDX, IDC_KEYS_BTN, KeysBtn);
    DDX_Control(pDX, IDC_ADD_IMAGE_BTN, AddImageBtn);
    DDX_Control(pDX, IDC_EDIT_IMAGE_BTN, EditImageBtn);
    DDX_Control(pDX, IDC_IMAGES_MOVE_UP_BTN, MoveUpBtn);
    DDX_Control(pDX, IDC_IMAGE_MOVE_DOWN_BTN, MoveDownBtn);
    DDX_Control(pDX, IDC_REMOVE_IMAGE_BTN, RemoveImageBtn);
    DDX_Control(pDX, IDC_TIM_DESCRIPTOR_WRITE_BTN, WriteTIMBtn);
    DDX_Control(pDX, IDC_TIM_DESCRIPTOR_READ_BTN, ReadTIMBtn);
    DDX_Control(pDX, IDC_TIM_DESCRIPTOR_VIEW_BTN, ViewTIMBtn);
    DDX_Control(pDX, IDC_IMAGES_LIST_CTRL, ImagesListCtrl);
    DDX_Control(pDX, IDC_TRUSTED_CHK, TrustedChk);
    DDX_Control(pDX, IDC_BOOTROM_FLASH_SIGNATURE_CB, BootRomFlashSignatureCb);
    DDX_Control(pDX, IDC_WTM_FLASH_SIGNATURE_CB, WTMFlashSignatureCb);
    DDX_Control(pDX, IDC_IMAGE_BUILDER_BTN, ImageBuilderBtn);
    DDX_Control(pDX, IDC_TIM_PROC_CB, ProcessorTypeCb);
    DDX_Control(pDX, IDC_TIM_DOWNLOAD_CONFIG_BTN, DownloadConfigBtn);
    DDX_Control(pDX, IDC_IMPORT_DLG_BTN, ImportBtn);

    // updates marker in title
    UpdateChangeMarkerInTitle();
    theApp.GetMainWnd()->GetTopLevelFrame()->OnUpdateFrameTitle(true);
}

BEGIN_MESSAGE_MAP(CTimDescriptorPropertyPage, CPropertyPage)
    ON_EN_CHANGE(IDC_TIM_DESCRIPTOR_FILE_PATH_EDT, &CTimDescriptorPropertyPage::OnEnChangeTimDescriptorFilePathEdt)
    ON_BN_CLICKED(IDC_TIM_DESCRIPTOR_FILE_PATH_BROWSE_BTN, &CTimDescriptorPropertyPage::OnBnClickedTimDescriptorFilePathBrowseBtn)
    ON_CBN_SELCHANGE(IDC_TIM_VERSION_CB, &CTimDescriptorPropertyPage::OnCbnSelchangeTimVersionCb)
    ON_BN_CLICKED(IDC_TRUSTED_CHK, &CTimDescriptorPropertyPage::OnBnClickedTrustedChk)
    ON_EN_CHANGE(IDC_ISSUE_DATE_EDT, &CTimDescriptorPropertyPage::OnEnChangeIssueDateEdt)
    ON_EN_CHANGE(IDC_OEM_UNIQUE_ID_EDT, &CTimDescriptorPropertyPage::OnEnChangeOemUniqueIdEdt)
    ON_EN_CHANGE(IDC_BOOTROM_FLASH_SIGNATURE_EDT, &CTimDescriptorPropertyPage::OnEnChangeBootromFlashSignatureEdt)
    ON_EN_CHANGE(IDC_WTM_FLASH_SIGNATURE_EDT, &CTimDescriptorPropertyPage::OnEnChangeWtmFlashSignatureEdt)
    ON_EN_CHANGE(IDC_WTM_FLASH_ENTRY_EDT, &CTimDescriptorPropertyPage::OnEnChangeWtmFlashEntryEdt)
    ON_EN_CHANGE(IDC_WTM_BACKUP_ENTRY_EDT, &CTimDescriptorPropertyPage::OnEnChangeWtmBackupEntryEdt)
    ON_BN_CLICKED(IDC_TIM_RESET_BTN, &CTimDescriptorPropertyPage::OnBnClickedTimResetBtn)
    ON_BN_CLICKED(IDC_RESERVED_DATA_BTN, &CTimDescriptorPropertyPage::OnBnClickedReservedDataBtn)
    ON_BN_CLICKED(IDC_ADD_IMAGE_BTN, &CTimDescriptorPropertyPage::OnBnClickedAddImageBtn)
    ON_BN_CLICKED(IDC_EDIT_IMAGE_BTN, &CTimDescriptorPropertyPage::OnBnClickedEditImageBtn)
    ON_BN_CLICKED(IDC_REMOVE_IMAGE_BTN, &CTimDescriptorPropertyPage::OnBnClickedRemoveImageBtn)
    ON_BN_CLICKED(IDC_DIGITAL_SIGNATURE_BTN, &CTimDescriptorPropertyPage::OnBnClickedDigitalSignatureBtn)
    ON_BN_CLICKED(IDC_TIM_DESCRIPTOR_WRITE_BTN, &CTimDescriptorPropertyPage::OnBnClickedTimDescriptorWriteBtn)
    ON_BN_CLICKED(IDC_TIM_DESCRIPTOR_READ_BTN, &CTimDescriptorPropertyPage::OnBnClickedTimDescriptorReadBtn)
    ON_BN_CLICKED(IDC_KEYS_BTN, &CTimDescriptorPropertyPage::OnBnClickedKeysBtn)
    ON_BN_CLICKED(IDC_IMAGES_MOVE_UP_BTN, &CTimDescriptorPropertyPage::OnBnClickedImagesMoveUpBtn)
    ON_BN_CLICKED(IDC_IMAGE_MOVE_DOWN_BTN, &CTimDescriptorPropertyPage::OnBnClickedImageMoveDownBtn)
    ON_NOTIFY(NM_SETFOCUS, IDC_IMAGES_LIST_CTRL, &CTimDescriptorPropertyPage::OnNMSetfocusImagesListCtrl)
    ON_NOTIFY(NM_KILLFOCUS, IDC_IMAGES_LIST_CTRL, &CTimDescriptorPropertyPage::OnNMKillfocusImagesListCtrl)
    ON_CBN_SELCHANGE(IDC_BOOTROM_FLASH_SIGNATURE_CB, &CTimDescriptorPropertyPage::OnCbnSelchangeBootromFlashSignatureCb)
    ON_CBN_SELCHANGE(IDC_WTM_FLASH_SIGNATURE_CB, &CTimDescriptorPropertyPage::OnCbnSelchangeWtmFlashSignatureCb)
    ON_CBN_SELCHANGE(IDC_TIM_PROC_CB, &CTimDescriptorPropertyPage::OnCbnSelchangeTimProcCb)
    ON_BN_CLICKED(IDC_TIM_DESCRIPTOR_VIEW_BTN, &CTimDescriptorPropertyPage::OnBnClickedTimDescriptorViewBtn)
    ON_BN_CLICKED(IDC_IMAGE_BUILDER_BTN, &CTimDescriptorPropertyPage::OnBnClickedImageBuilderBtn)
    ON_BN_CLICKED(IDCANCEL, &CTimDescriptorPropertyPage::OnBnClickedCancel)
    ON_WM_CONTEXTMENU()
    ON_BN_CLICKED(IDC_TIM_DOWNLOAD_CONFIG_BTN, &CTimDescriptorPropertyPage::OnBnClickedTimDownloadConfigBtn)
    ON_BN_CLICKED(IDC_IMPORT_DLG_BTN, &CTimDescriptorPropertyPage::OnBnClickedImportDlgBtn)
    ON_WM_SETFOCUS()
    ON_CBN_EDITCHANGE(IDC_TIM_VERSION_CB, &CTimDescriptorPropertyPage::OnCbnEditchangeTimVersionCb)
END_MESSAGE_MAP()


void CTimDescriptorPropertyPage::UpdateChangeMarkerInTitle()
{
    if ( m_hWnd == 0 )
        return;

    CString sTitle;
    theApp.ToolsInterfacePropSheet().GetWindowTextA( sTitle );
    {
        // add or remove * as appropriate
        if ( TimDescriptor.IsChanged() && TimDescriptor.IsNotWritten() )
        {
            if ( -1 == sTitle.Find('*') )
                sTitle += "*";
        }
        else
        {
            if ( -1 != sTitle.Find('*') )
                sTitle.Remove('*');
        }
        theApp.ToolsInterfacePropSheet().SetWindowTextA(sTitle);
        theApp.ToolsInterfacePropSheet().UpdateWindow();
    }
}

// CTimDescriptorPropertyPage message handlers

BOOL CTimDescriptorPropertyPage::OnInitDialog()
{
    CPropertyPage::OnInitDialog();

    ImagesListCtrl.SetExtendedStyle( //LVS_EX_TRACKSELECT
                                     LVS_EX_FULLROWSELECT 
                                    | LVS_EX_HEADERDRAGDROP 
                                    | LVS_EX_ONECLICKACTIVATE);
    ImagesListCtrl.InsertColumn(IMAGE_ID,"Image ID", LVCFMT_LEFT, 60, -1 );
    ImagesListCtrl.InsertColumn(IMAGE_ID_TAG,"Image ID Tag", LVCFMT_LEFT, 85, -1 );
    ImagesListCtrl.InsertColumn(FLASH_ADDRESS,"Flash Address", LVCFMT_LEFT, 85, -1 );
    ImagesListCtrl.InsertColumn(LOAD_ADDRESS,"Load Address", LVCFMT_LEFT, 85, -1 );
    ImagesListCtrl.InsertColumn(IMAGE_FILE_PATH,"Image File Path", LVCFMT_LEFT, 600, -1 );

    int idx = -1;
    BootRomFlashSignatureCb.InsertString(++idx,"Default - Probe Flash"); BootRomFlashSignatureCb.SetItemData(idx,0x00000000);
    BootRomFlashSignatureCb.InsertString(++idx,"HSI1");BootRomFlashSignatureCb.SetItemData(idx,0x48534901);
    BootRomFlashSignatureCb.InsertString(++idx,"x16 OneNAND");BootRomFlashSignatureCb.SetItemData(idx,0x4E414E02);
    BootRomFlashSignatureCb.InsertString(++idx,"XIP Flash x16");BootRomFlashSignatureCb.SetItemData(idx,0x58495003);
    BootRomFlashSignatureCb.InsertString(++idx,"x16 NAND Hamming");BootRomFlashSignatureCb.SetItemData(idx,0x4E414E04);
    BootRomFlashSignatureCb.InsertString(++idx,"x16 XIP SIBLEY ");BootRomFlashSignatureCb.SetItemData(idx,0x58495005);
    BootRomFlashSignatureCb.InsertString(++idx,"NAND x8 Hamming");BootRomFlashSignatureCb.SetItemData(idx,0x4E414E06);
    BootRomFlashSignatureCb.InsertString(++idx,"SDMMC Flash Opt0");BootRomFlashSignatureCb.SetItemData(idx,0x4D4D4307);
    BootRomFlashSignatureCb.InsertString(++idx,"SDMMC Flash Opt1");BootRomFlashSignatureCb.SetItemData(idx,0x4D4D4308);
    BootRomFlashSignatureCb.InsertString(++idx,"SDMMC Flash Opt2");BootRomFlashSignatureCb.SetItemData(idx,0x4D4D4309);
    BootRomFlashSignatureCb.InsertString(++idx,"SPI Flash");BootRomFlashSignatureCb.SetItemData(idx,0x5350490A);
    BootRomFlashSignatureCb.InsertString(++idx,"SDMMC Flash Opt3");BootRomFlashSignatureCb.SetItemData(idx,0x4D4D430B);
    BootRomFlashSignatureCb.InsertString(++idx,"SDMMC Flash Opt4");BootRomFlashSignatureCb.SetItemData(idx,0x4D4D430C);
    BootRomFlashSignatureCb.InsertString(++idx,"x16 NAND BCH");BootRomFlashSignatureCb.SetItemData(idx,0x4E414E0D);
    BootRomFlashSignatureCb.InsertString(++idx,"x8 NAND BCH");BootRomFlashSignatureCb.SetItemData(idx,0x4E414E0E);

    idx = -1;
    WTMFlashSignatureCb.InsertString(++idx,"Default - Probe Flash"); WTMFlashSignatureCb.SetItemData(idx,0x00000000);
    WTMFlashSignatureCb.InsertString(++idx,"HSI1");WTMFlashSignatureCb.SetItemData(idx,0x48534901);
    WTMFlashSignatureCb.InsertString(++idx,"x16 OneNAND");WTMFlashSignatureCb.SetItemData(idx,0x4E414E02);
    WTMFlashSignatureCb.InsertString(++idx,"XIP Flash x16");WTMFlashSignatureCb.SetItemData(idx,0x58495003);
    WTMFlashSignatureCb.InsertString(++idx,"x16 NAND Hamming");WTMFlashSignatureCb.SetItemData(idx,0x4E414E04);
    WTMFlashSignatureCb.InsertString(++idx,"x16 XIP SIBLEY ");WTMFlashSignatureCb.SetItemData(idx,0x58495005);
    WTMFlashSignatureCb.InsertString(++idx,"NAND x8 Hamming");WTMFlashSignatureCb.SetItemData(idx,0x4E414E06);
    WTMFlashSignatureCb.InsertString(++idx,"SDMMC Flash Opt0");WTMFlashSignatureCb.SetItemData(idx,0x4D4D4307);
    WTMFlashSignatureCb.InsertString(++idx,"SDMMC Flash Opt1");WTMFlashSignatureCb.SetItemData(idx,0x4D4D4308);
    WTMFlashSignatureCb.InsertString(++idx,"SDMMC Flash Opt2");WTMFlashSignatureCb.SetItemData(idx,0x4D4D4309);
    WTMFlashSignatureCb.InsertString(++idx,"SPI Flash");WTMFlashSignatureCb.SetItemData(idx,0x5350490A);
    WTMFlashSignatureCb.InsertString(++idx,"SDMMC Flash Opt3");WTMFlashSignatureCb.SetItemData(idx,0x4D4D430B);
    WTMFlashSignatureCb.InsertString(++idx,"SDMMC Flash Opt4");WTMFlashSignatureCb.SetItemData(idx,0x4D4D430C);
    WTMFlashSignatureCb.InsertString(++idx,"x16 NAND BCH");WTMFlashSignatureCb.SetItemData(idx,0x4E414E0D);
    WTMFlashSignatureCb.InsertString(++idx,"x8 NAND BCH");WTMFlashSignatureCb.SetItemData(idx,0x4E414E0E);

    IssueDateEdt.m_bHexOnly = true;
    OemUniqueIdEdt.m_bHexOnly = true;
    BootromFlashSignatureEdt.m_bHexOnly = true;
    WtmFlashSignatureEdt.m_bHexOnly = true;
    WtmFlashEntryAddressEdt.m_bHexOnly = true;
    WtmBackupEntryEdt.m_bHexOnly = true;

    idx = -1;
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA168].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA30x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA31x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA32x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA688].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA91x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA92x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA93x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA94x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA95x].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[ARMADA168].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[ARMADA610].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[MG1].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[TAVOR_PV_MG2].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[ESHEL].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[PXA978].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[ARMADA620].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[ESHEL_LTE].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[ARMADA622].c_str());
    ProcessorTypeCb.InsertString(++idx, gsProcessorType[WUKONG].c_str());
//	ProcessorTypeCb.InsertString(++idx, gsProcessorType[P_88AP001].c_str());

    idx = -1;
    TimVersionCb.InsertString(++idx, "0x00030101" );
    TimVersionCb.InsertString(++idx, "0x00030200" ); // Support for Partitioning
    TimVersionCb.InsertString(++idx, "0x00030300" ); // Support for ECDSA-256 and 64 bit addressing
    TimVersionCb.InsertString(++idx, "0x00030400" ); // Support for ECDSA-521

#if TRUSTED
    TrustedChk.EnableWindow( TRUE );
#else
    TrustedChk.EnableWindow( FALSE );
#endif

    if ( TimDescriptor.g_TimDescriptorLines.size() == 0 )
    {
        if ( TimDescriptor.TimDescriptorFilePath().length() > 0 )
        {
            DownloaderInterface.TimDescriptorParser.GetTimDescriptorLines(DownloaderInterface.ImageBuild.CommandLineParser);
            DownloaderInterface.RefreshImageList( TimDescriptor.ImagesList() );
            DownloaderInterface.TimDescriptorParser.ParseDescriptor( DownloaderInterface );
        }
    }

    RefreshState();

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}


void CTimDescriptorPropertyPage::OnEnChangeTimDescriptorFilePathEdt()
{
    UpdateData( TRUE );
    TimDescriptor.TimDescriptorFilePath( string((CStringA)sTimDescriptorFilePath) );
    DownloaderInterface.ImageBuild.TimDescriptorFilePath( string((CStringA)sTimDescriptorFilePath) );
    UpdateControls();
}

void CTimDescriptorPropertyPage::OnBnClickedTimDescriptorFilePathBrowseBtn()
{
    UpdateData( TRUE );
    CFileDialog Dlg( TRUE, _T("*.txt"), NULL, 
                     OFN_PATHMUSTEXIST,
                     _T("Text (*.txt)|*.txt|All Files (*.*)|*.*||") );
    if ( Dlg.DoModal() == IDOK )
    {
        if ( TimDescriptor.IsChanged() && TimDescriptor.IsNotWritten() )
        {
            stringstream ss;
            ss << "Save change to " << TimDescriptor.TimDescriptorFilePath() << " before Read?";
            if ( IDYES == AfxMessageBox( ss.str().c_str(), MB_YESNO ) )
                OnBnClickedTimDescriptorWriteBtn();
        }

        sTimDescriptorFilePath = Dlg.GetPathName();
        TimDescriptor.Reset();
        TimDescriptor.TimDescriptorFilePath( string((CStringA)sTimDescriptorFilePath) );	
        TimDescriptor.Changed(false);
        TimDescriptor.NotWritten(false);

        // if the file exists, then read it
        ifstream ifs;
        ifs.open( TimDescriptor.TimDescriptorFilePath().c_str(), ios_base::in  );
        if ( ifs.is_open() )
        {
            ifs.close();
            // read in the tim file automatically
            OnBnClickedTimDescriptorReadBtn();
        
            DownloaderInterface.ImageBuild.TimDescriptorFilePath( string((CStringA)sTimDescriptorFilePath) );
            RefreshState();

            UpdateData( FALSE );
        }
    }
    UpdateControls();
    SetFocus();
}

void CTimDescriptorPropertyPage::RefreshState()
{
#if TRUSTED
    bTrusted = TimDescriptor.Trusted() ? TRUE : FALSE;
#else
    bTrusted = FALSE;
#endif

    sTimDescriptorFilePath = TimDescriptor.TimDescriptorFilePath().c_str();
//	sTimVersion = TimDescriptor.Version().c_str();
    sIssueDate = TimDescriptor.IssueDate().c_str();
    sOemUniqueId = TimDescriptor.OemUniqueId().c_str();

    CTimLib TimLib;
    int iBootFlashSig = TimLib.Translate( TimDescriptor.BootRomFlashSignature() );
    BootRomFlashSignatureCb.SetCurSel(-1);
    for ( int i=0; i<BootRomFlashSignatureCb.GetCount(); i++ )
    {
        if ( (int)BootRomFlashSignatureCb.GetItemData(i) == iBootFlashSig )
        {
            BootRomFlashSignatureCb.SetCurSel(i);
            break;
        }
    }
    sBootRomFlashSignature = TimDescriptor.BootRomFlashSignature().c_str();

    iBootFlashSig = TimLib.Translate( TimDescriptor.WtmSaveStateFlashSignature() );
    WTMFlashSignatureCb.SetCurSel(-1);
    for ( int i=0; i<WTMFlashSignatureCb.GetCount(); i++ )
    {
        if ( (int)WTMFlashSignatureCb.GetItemData(i) == iBootFlashSig )
        {
            WTMFlashSignatureCb.SetCurSel(i);
            break;
        }
    }
    sWtmFlashSignature = TimDescriptor.WtmSaveStateFlashSignature().c_str();
    sWtmFlashEntryAddress = TimDescriptor.WtmSaveStateFlashEntryAddress().c_str();
    sWtmBackupEntryAddress = TimDescriptor.WtmSaveStateBackupEntryAddress().c_str();

    ImagesListCtrl.DeleteAllItems();
    unsigned int i = 0; 
    while ( i < TimDescriptor.ImagesCount() )
    {
        CImageDescription* pImage(TimDescriptor.Image( i ));
        ImagesListCtrl.InsertItem(i, pImage->ImageId().c_str());
        ImagesListCtrl.SetItemData(i,(DWORD_PTR)pImage);  // item retains image*
        ImagesListCtrl.SetItem(i,IMAGE_ID_TAG,LVIF_TEXT,pImage->ImageIdTag().c_str(),0,0,0,0);
        ImagesListCtrl.SetItem(i,FLASH_ADDRESS,LVIF_TEXT,pImage->FlashEntryAddress().c_str(),0,0,0,0);
        ImagesListCtrl.SetItem(i,LOAD_ADDRESS,LVIF_TEXT,pImage->LoadAddress().c_str(),0,0,0,0);
        ImagesListCtrl.SetItem(i,IMAGE_FILE_PATH,LVIF_TEXT,pImage->ImageFilePath().c_str(),0,0,0,0);
        i++;
    }

    ProcessorTypeCb.SetCurSel( ProcessorTypeCb.FindStringExact( 0, TimDescriptor.ProcessorType().c_str() ));
    
    int iSel = TimVersionCb.FindStringExact( 0, TimDescriptor.Version().c_str() );
    if ( iSel != LB_ERR )
        TimVersionCb.SetCurSel( iSel );
    else
        TimVersionCb.SetWindowTextA( TimDescriptor.Version().c_str() );

    if ( ImageBuildDlg.m_hWnd != NULL )
        ImageBuildDlg.RefreshState();

    if ( TimDownloaderDlg.m_hWnd != NULL )
        TimDownloaderDlg.RefreshState();

    UpdateData( FALSE );
    UpdateControls();
}

void CTimDescriptorPropertyPage::UpdateControls()
{
#if TRUSTED
    bTrusted = TimDescriptor.Trusted() ? TRUE : FALSE;
#else
    bTrusted = FALSE;
#endif

    WTMFlashSignatureCb.EnableWindow( bTrusted );
    WtmFlashSignatureEdt.EnableWindow( bTrusted );
    WtmFlashEntryAddressEdt.EnableWindow( bTrusted );
    WtmBackupEntryEdt.EnableWindow( bTrusted );
    DigitalSignatureBtn.EnableWindow( bTrusted );
    KeysBtn.EnableWindow( bTrusted );

    AddImageBtn.EnableWindow( TimDescriptor.ImagesCount() < 10 );
    EditImageBtn.EnableWindow( TimDescriptor.ImagesCount() > 0 );
    MoveUpBtn.EnableWindow( TimDescriptor.ImagesCount() > 1 );
    MoveDownBtn.EnableWindow( TimDescriptor.ImagesCount() > 1 );
    RemoveImageBtn.EnableWindow( TimDescriptor.ImagesCount() > 0 );
    WriteTIMBtn.EnableWindow( sTimDescriptorFilePath.GetLength() > 0 );
    ReadTIMBtn.EnableWindow( sTimDescriptorFilePath.GetLength() > 0 );
    ViewTIMBtn.EnableWindow( TimDescriptor.g_TimDescriptorLines.size() > 0 );
    ImportBtn.EnableWindow( TRUE );
    ProcessorTypeCb.EnableWindow( TimDescriptor.ExtendedReservedData().Size() == 0 );
}

#if 0
void CTimDescriptorPropertyPage::OnEnChangeTimVersionEdt()
{
    UpdateData( TRUE );
    TimDescriptor.Version( string((CStringA)sTimVersion ) );
    UpdateData( FALSE );
}
#endif


void CTimDescriptorPropertyPage::OnBnClickedTrustedChk()
{
    UpdateData( TRUE );
    TimDescriptor.Trusted( bTrusted == TRUE ? true : false );
    
    // keep image build and image build dlg in sync with TIM descriptor
    DownloaderInterface.ImageBuild.Trusted( bTrusted == TRUE ? true : false );

    UpdateData( FALSE );
    UpdateControls();
}

void CTimDescriptorPropertyPage::OnEnChangeIssueDateEdt()
{
    UpdateData( TRUE );
    TimDescriptor.IssueDate(string((CStringA)sIssueDate ) );
    UpdateData( FALSE );
}

void CTimDescriptorPropertyPage::OnEnChangeOemUniqueIdEdt()
{
    UpdateData( TRUE );
    TimDescriptor.OemUniqueId(string((CStringA)sOemUniqueId ) );
    UpdateData( FALSE );
}

void CTimDescriptorPropertyPage::OnEnChangeBootromFlashSignatureEdt()
{
    UpdateData( TRUE );
    TimDescriptor.BootRomFlashSignature(string((CStringA)sBootRomFlashSignature ) );
    UpdateData( FALSE );
}

void CTimDescriptorPropertyPage::OnEnChangeWtmFlashSignatureEdt()
{
    UpdateData( TRUE );
    TimDescriptor.WtmSaveStateFlashSignature( string((CStringA)sWtmFlashSignature) );
    UpdateData( FALSE );
}

void CTimDescriptorPropertyPage::OnEnChangeWtmFlashEntryEdt()
{
    UpdateData( TRUE );
    TimDescriptor.WtmSaveStateFlashEntryAddress( string((CStringA)sWtmFlashEntryAddress) );
    UpdateData( FALSE );
}

void CTimDescriptorPropertyPage::OnEnChangeWtmBackupEntryEdt()
{
    UpdateData( TRUE );
    TimDescriptor.WtmSaveStateBackupEntryAddress( string((CStringA)sWtmBackupEntryAddress) );
    UpdateData( FALSE );
}

void CTimDescriptorPropertyPage::OnBnClickedTimResetBtn()
{
    TimDescriptor.Reset();
    TimDescriptor.DiscardAll();
    RefreshState();
}

void CTimDescriptorPropertyPage::OnBnClickedReservedDataBtn()
{
    GetDlgItem(IDC_RESERVED_DATA_BTN)->EnableWindow( FALSE );

    UpdateData( TRUE );
    CReservedDataDlg Dlg;
    Dlg.LocalTimDescriptor( TimDescriptor );
    if ( Dlg.DoModal() == IDOK )
    {
        if ( Dlg.LocalTimDescriptor().IsChanged() )
        {
            TimDescriptor = Dlg.LocalTimDescriptor();
        }
    }

    GetDlgItem(IDC_RESERVED_DATA_BTN)->EnableWindow( TRUE );
    UpdateData( FALSE );
    SetFocus();
}

void CTimDescriptorPropertyPage::OnBnClickedAddImageBtn()
{
    UpdateData( TRUE );
    if ( TimDescriptor.ImagesCount() >= 10 )
    {
        AfxMessageBox("Maximum of 10 Images already defined.  Delete an image if you wish to add another.");
        return;
    }

    CImageDescription* pImage = new CImageDescription();
    CImageConfigurationDlg Dlg( DownloaderInterface, *pImage );
    if ( Dlg.DoModal() == IDOK )
    {
        if ( Dlg.Image().IsChanged() )
        {
            *pImage = Dlg.Image();
            if ( !TimDescriptor.AddImage(pImage) )
            {
                if ( pImage->ImageId().compare(0,3,"TIM") == 0 )
                {
                    t_ImagesList& Images = TimDescriptor.ImagesList();
                    t_ImagesIter iterImage = Images.begin();
                    while( iterImage != Images.end() )
                    if ( (*iterImage++)->ImageId().compare(0,3,"TIM") == 0 )
                        AfxMessageBox( "Unable to add TIM* image type. Note that if a TIM* image already exists in the ImageList, you cannot add another.\n " );
                }
                else
                    AfxMessageBox( "Failed to add image type.\n " );

                delete pImage;
                SetFocus();
                return;
            }

            TimDescriptor.UpdateNextImageIds();
            TimDescriptor.NotWritten(true);
        }
        else
            delete pImage; // default pImage was not changed and not added to list
    }
    else
        delete pImage;

    RefreshState();
    DownloaderInterface.RefreshImageList( TimDescriptor.ImagesList() );
    SetFocus();
}


void CTimDescriptorPropertyPage::OnBnClickedEditImageBtn()
{
    POSITION Pos = ImagesListCtrl.GetFirstSelectedItemPosition();
    if ( Pos )
    {
        int nItem = ImagesListCtrl.GetNextSelectedItem(Pos);
        CImageDescription* pImage = (CImageDescription*)ImagesListCtrl.GetItemData(nItem);  // item retains image*
        CImageConfigurationDlg Dlg( DownloaderInterface, *pImage ); 
        if ( Dlg.DoModal() == IDOK )
        {
            if ( Dlg.Image().IsChanged() )
            {
                // update image with changes
                string sImageBinPath;
                if ( Dlg.Image().ImageId().compare(0,3,"TIM") == 0 )
                {
                    t_ImagesList& Images = TimDescriptor.ImagesList();
                    t_ImagesIter iterImage = Images.begin();
                    while( iterImage != Images.end() )
                    {
                        // if ImageID is still the same, continue
                        if ( (*iterImage)->ImageId() == Dlg.Image().ImageId() )
                            break;

                        if ( (*iterImage++)->ImageId().compare(0,3,"TIM") == 0 )
                        {
                            AfxMessageBox( "Unable to modify image type. Note that if a TIM* image already exists in the ImageList, you cannot add another.\n " );
                            SetFocus();
                            return;
                        }
                    }
                }

                *pImage = Dlg.Image();
                DownloaderInterface.TimDescriptorParser.CreateOutputImageName(string(pImage->ImageFilePath()), sImageBinPath);

                TimDescriptor.UpdateNextImageIds();
                TimDescriptor.NotWritten(true);

                RefreshState();
                DownloaderInterface.RefreshImageList( TimDescriptor.ImagesList() );
            }
        }
    }
    SetFocus();
}

void CTimDescriptorPropertyPage::OnBnClickedRemoveImageBtn()
{
    POSITION Pos = ImagesListCtrl.GetFirstSelectedItemPosition();
    if ( Pos )
    {
        int nItem = ImagesListCtrl.GetNextSelectedItem(Pos);
        CImageDescription* pImage = (CImageDescription*)ImagesListCtrl.GetItemData(nItem);  // item retains image*
        if ( TimDescriptor.DeleteImage(pImage) )
        {
            ImagesListCtrl.DeleteItem(nItem);
            TimDescriptor.UpdateNextImageIds();
            TimDescriptor.NotWritten(true);
        }
    }

    RefreshState();
    DownloaderInterface.RefreshImageList( TimDescriptor.ImagesList() );
    SetFocus();
}


void CTimDescriptorPropertyPage::OnBnClickedDigitalSignatureBtn()
{
#if TRUSTED
    CDigitalSignatureDlg Dlg;
    Dlg.DigitalSignature( TimDescriptor.DigitalSignature() );
    if ( Dlg.DoModal() == IDOK )
    {
        // update the original
        if ( Dlg.DigitalSignature().IsChanged() )
            TimDescriptor.DigitalSignature() = Dlg.DigitalSignature();
    }

    RefreshState();
    SetFocus();
#endif
}

void CTimDescriptorPropertyPage::OnBnClickedTimDescriptorWriteBtn()
{
    if ( !TimDescriptor.WriteTimDescriptorFile() )
        AfxMessageBox( "Failed to write TIM descriptor file" );
    else
    {
        theApp.DisplayMsg( CString("\nSuccessful: Write TIM Descriptor text file.\n") );
        TimDescriptor.NotWritten(false);
    }
    UpdateData( FALSE );
    SetFocus();
}


void CTimDescriptorPropertyPage::OnBnClickedTimDescriptorReadBtn()
{
    if ( TimDescriptor.IsChanged() && TimDescriptor.IsNotWritten() )
    {
        stringstream ss;
        ss << "Save change to " << TimDescriptor.TimDescriptorFilePath() << " before Read?";
        if ( IDYES == AfxMessageBox( ss.str().c_str(), MB_YESNO ) )
            OnBnClickedTimDescriptorWriteBtn();
    }

    if ( !DownloaderInterface.TimDescriptorParser.GetTimDescriptorLines(DownloaderInterface.ImageBuild.CommandLineParser) )
    {
        printf( "Failed parsing Tim Descriptor file in GetTimDescriptorLine() \n" );
    }
    else if ( !DownloaderInterface.TimDescriptorParser.ParseDescriptor( DownloaderInterface ) )
    {
        AfxMessageBox( "Failed to parse TIM descriptor file.  See Host and Target Doc for Error messages." );
    }
    else
    {
        theApp.DisplayMsg( CString("\nSuccessful: Read TIM Descriptor text file.\n") );
    }

    theApp.CommandLineParser.ProcessorType(DownloaderInterface.TimDescriptorParser.TimDescriptor().ProcessorType());

    // keep image build and image build dlg in sync with TIM descriptor
    DownloaderInterface.ImageBuild.Trusted( bTrusted == TRUE ? true : false );
    DownloaderInterface.RefreshImageList( TimDescriptor.ImagesList() );

    // ignoring all changes since we just read the tim
    TimDescriptor.Changed( false );

    RefreshState();
    SetFocus();
}


void CTimDescriptorPropertyPage::OnBnClickedKeysBtn()
{
#if TRUSTED
    CKeysDlg Dlg;
    Dlg.LocalTimDescriptor( TimDescriptor );

    if ( Dlg.DoModal() == IDOK )
    {
        if ( Dlg.LocalTimDescriptor().KeysIsChanged() )
        {
            // update the original
            TimDescriptor = Dlg.LocalTimDescriptor();
        }
    }

    RefreshState();
    SetFocus();
#endif
}


void CTimDescriptorPropertyPage::OnBnClickedImagesMoveUpBtn()
{
    int nItem = ImagesListCtrl.GetSelectionMark();
    if ( nItem > 0 )
    {
        CImageDescription* pImage = (CImageDescription*)ImagesListCtrl.GetItemData( nItem );
        t_ImagesList& ImagesList = TimDescriptor.ImagesList();
        //put image in correct position in list
        t_ImagesIter Iter = ImagesList.begin();
        while ( Iter != ImagesList.end() )
        {
            if ( *Iter == pImage )
            {
                // move it up unless already at top of list
                // TIM* cannot move out of first position, other images can move up
                if ( Iter != ImagesList.begin() && (*Iter)->ImageId().compare(0,3,"TIM") != 0 )
                {
                    t_ImagesIter insertiter = --Iter;

                    ImagesList.remove( pImage );
                    ImagesList.insert( insertiter, pImage );
                    ImagesListCtrl.SetSelectionMark( --nItem );
                }
                else
                    AfxMessageBox("TIM* image, if any, must be first image in list.");

                break;
            }
            Iter++;
        }
        TimDescriptor.UpdateNextImageIds();
        TimDescriptor.NotWritten(true);
    }

    RefreshState();
    DownloaderInterface.RefreshImageList( TimDescriptor.ImagesList() );
}

void CTimDescriptorPropertyPage::OnBnClickedImageMoveDownBtn()
{
    int nItem = ImagesListCtrl.GetSelectionMark();
    if ( nItem != -1 )
    {
        CImageDescription* pImage = (CImageDescription*)ImagesListCtrl.GetItemData( nItem );
        // TIM* cannot move out of first position, other images can move down
        if ( pImage->ImageId().compare(0,3,"TIM") != 0 )
        {
            t_ImagesList& ImagesList = TimDescriptor.ImagesList();
            //put image in correct position in list
            t_ImagesIter Iter = ImagesList.begin();
            while ( Iter != ImagesList.end() )
            {
                if ( *Iter == pImage )
                {
                    // move it down unless already at end of list
                    if ( ++Iter != ImagesList.end() )
                    {
                        ImagesList.remove( pImage );
                        ImagesList.insert( ++Iter, pImage );
                        ImagesListCtrl.SetSelectionMark( ++nItem );
                    }
                    break;
                }
                Iter++;
            }
            TimDescriptor.UpdateNextImageIds();
            TimDescriptor.NotWritten(true);
        }
        else
            AfxMessageBox("TIM* image, if any, must be first image in list.");
    }

    RefreshState();
    DownloaderInterface.RefreshImageList( TimDescriptor.ImagesList() );
}

void CTimDescriptorPropertyPage::OnNMSetfocusImagesListCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
    *pResult = 0;
    UpdateData(FALSE);
}


void CTimDescriptorPropertyPage::OnNMKillfocusImagesListCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
    *pResult = 0;
    UpdateData(FALSE);
}


void CTimDescriptorPropertyPage::OnCbnSelchangeBootromFlashSignatureCb()
{
    UpdateData( TRUE );
    int idx = BootRomFlashSignatureCb.GetCurSel();
    if ( CB_ERR != idx )
    {
        CTimLib TimLib;
        int iFlashSig = (int)BootRomFlashSignatureCb.GetItemData(idx);
        sBootRomFlashSignature = (TimLib.HexFormattedAscii( iFlashSig )).c_str();
        TimDescriptor.BootRomFlashSignature(string((CStringA)sBootRomFlashSignature ) );
    }
    UpdateData( FALSE );
}

void CTimDescriptorPropertyPage::OnCbnSelchangeWtmFlashSignatureCb()
{
    UpdateData( TRUE );
    int idx = WTMFlashSignatureCb.GetCurSel();
    if ( CB_ERR != idx )
    {
        CTimLib TimLib;
        int iFlashSig = (int)WTMFlashSignatureCb.GetItemData(idx);
        sWtmFlashSignature = (TimLib.HexFormattedAscii( iFlashSig )).c_str();
        TimDescriptor.WtmSaveStateFlashSignature(string((CStringA)sWtmFlashSignature ) );
    }
    UpdateData( FALSE );
}

void CTimDescriptorPropertyPage::OnCbnSelchangeTimProcCb()
{
    UpdateData(TRUE);

    int idx = CB_ERR;
    if ( (idx = ProcessorTypeCb.GetCurSel()) != CB_ERR )
    {
        CString sText;
        ProcessorTypeCb.GetWindowTextA(sText);

        // keep all the Processor Types consistent
        theApp.CommandLineParser.ProcessorType(string((CStringA)sText));
        theApp.DKBDownloaderInterface().TimDescriptorParser.TimDescriptor().ProcessorType(string((CStringA)sText));
        theApp.OBMDownloaderInterface().TimDescriptorParser.TimDescriptor().ProcessorType(string((CStringA)sText));
        theApp.FBFDownloaderInterface().TimDescriptorParser.TimDescriptor().ProcessorType(string((CStringA)sText));
    }

    RefreshState();
}

void CTimDescriptorPropertyPage::OnContextMenu(CWnd* pWnd, CPoint point)
{
    CCommentDlg Dlg(this);
    int iCtlId = pWnd->GetDlgCtrlID();
    switch ( iCtlId )
    {
        case IDC_TIM_VERSION_CB:
            Dlg.Display( &(TimDescriptor.TimHeader().VersionBind.Version));
            break;
        case IDC_ISSUE_DATE_EDT:
            Dlg.Display( &(TimDescriptor.TimHeader().VersionBind.IssueDate) );
            break;
        case IDC_OEM_UNIQUE_ID_EDT:
            Dlg.Display( &(TimDescriptor.TimHeader().VersionBind.OEMUniqueID) );
            break;
        case IDC_BOOTROM_FLASH_SIGNATURE_CB:
            Dlg.Display( &(TimDescriptor.TimHeader().FlashInfo.BootFlashSign) );
            break;
        case IDC_TIM_PROC_CB:
            Dlg.Display( &(TimDescriptor.ProcessorType()) );
            break;
        case IDC_IMAGES_LIST_CTRL:
            Dlg.Display( &(TimDescriptor.TimHeader().NumImages) );
            break;
        case IDC_TRUSTED_CHK:
            Dlg.Display( &(TimDescriptor.TimHeader().VersionBind.Trusted) );
            break;
        case IDC_WTM_FLASH_SIGNATURE_CB:
            Dlg.Display( &(TimDescriptor.TimHeader().FlashInfo.WTMFlashSign) );
            break;
        case IDC_WTM_FLASH_ENTRY_EDT:
            Dlg.Display( &(TimDescriptor.TimHeader().FlashInfo.WTMEntryAddr) );
            break;			
        case IDC_WTM_BACKUP_ENTRY_EDT:
            Dlg.Display( &(TimDescriptor.TimHeader().FlashInfo.WTMEntryAddrBack) );
            break;			
        default:
            break;
    }
}

void CTimDescriptorPropertyPage::OnBnClickedTimDescriptorViewBtn()
{
    ViewTIMBtn.EnableWindow( FALSE );
    ImportBtn.EnableWindow( FALSE );

    CTimTextViewDlg Dlg;
    Dlg.SetViewText( TimDescriptor.TimDescriptorText() );
    ViewTIMBtn.EnableWindow(FALSE);
    if ( Dlg.DoModal() == IDOK )
    {
        DownloaderInterface.TimDescriptorParser.GetTimDescriptorLines( Dlg.Lines );
        DownloaderInterface.TimDescriptorParser.ParseDescriptor( DownloaderInterface );
    }
    ViewTIMBtn.EnableWindow(TRUE);
    RefreshState();
    if ( theApp.CommandLineParser.ProcessorType() != TimDescriptor.ProcessorType() )
        theApp.CommandLineParser.ProcessorType( TimDescriptor.ProcessorType() );
    
    DownloaderInterface.RefreshImageList( TimDescriptor.ImagesList() );
    ImageBuildDlg.RefreshState();
    TimDownloaderDlg.RefreshState();
    SetFocus();
    UpdateControls();
}

void CTimDescriptorPropertyPage::OnBnClickedImageBuilderBtn()
{
    ImageBuilderBtn.EnableWindow(FALSE);
    if ( ImageBuildDlg.DoModal() == IDOK )
        RefreshState();

    ImageBuilderBtn.EnableWindow(TRUE);
    SetFocus();
}


void CTimDescriptorPropertyPage::OnBnClickedTimDownloadConfigBtn()
{
    DownloadConfigBtn.EnableWindow(FALSE);
    if ( TimDownloaderDlg.DoModal() == IDOK )
        RefreshState();	
    
    DownloadConfigBtn.EnableWindow(TRUE);
    SetFocus();
}


void CTimDescriptorPropertyPage::OnBnClickedCancel()
{
    TimDescriptor.WriteTimDescriptorFile();
//	TimDescriptor.DiscardTimDescriptorLines();
    GetParent()->ShowWindow(FALSE);
}

void CTimDescriptorPropertyPage::OnSetFocus(CWnd* pOldWnd)
{
    CPropertyPage::OnSetFocus(pOldWnd);
//	OnSetActive();
    UpdateControls();
}

BOOL CTimDescriptorPropertyPage::OnSetActive()
{
    OnBnClickedTimDescriptorReadBtn();
    CToolsInterfacePropertySheet* pSheet = (CToolsInterfacePropertySheet*)GetParent();
    if ( DownloaderInterface.TimDescriptorParser.TimDescriptor().ImagesCount() > 1 )
    {
        if ( pSheet && pSheet->GetPageIndex( this ) == 0 )
        {
            CImageDescription* pTimImage = DownloaderInterface.TimDescriptorParser.TimDescriptor().Image(0);
            if ( !pTimImage || pTimImage->ImageId().compare(0,3,"TIM") != 0 )
                AfxMessageBox( "First image must be a TIM image" );
            
            CImageDescription* pDkbImage = 0;
            bool bDkbFound = false;
            for ( unsigned int i = 1; i < DownloaderInterface.TimDescriptorParser.TimDescriptor().ImagesCount(); i++ )
            {
                pDkbImage = DownloaderInterface.TimDescriptorParser.TimDescriptor().Image(i);
                if ( pDkbImage && pDkbImage->ImageId().compare(0,3,"DKB") == 0 )
                    bDkbFound = true;
            }
            if ( !bDkbFound )
                AfxMessageBox( "Did not find DKB image in images list." );
        }
        else if ( pSheet && pSheet->GetPageIndex( this ) == 1 )
        {
            CImageDescription* pTimImage = DownloaderInterface.TimDescriptorParser.TimDescriptor().Image(0);
            if ( !pTimImage || pTimImage->ImageId().compare(0,3,"TIM") != 0 )
                AfxMessageBox( "First image must be a TIM image" );
                
            CImageDescription* pObmImage;
            bool bFound = false;
            for ( unsigned int i = 0; i < DownloaderInterface.TimDescriptorParser.TimDescriptor().ImagesCount(); i++ )
            {
                pObmImage = DownloaderInterface.TimDescriptorParser.TimDescriptor().Image(i);
                if ( !pObmImage || pObmImage->ImageId().compare(0,3,"OBM") == 0 )
                {
                    bFound = true;
                    break;
                }
            }
            if ( !bFound )
                AfxMessageBox( "No OBM image found in list of images." );
        }
    }

    return CPropertyPage::OnSetActive();
}

void CTimDescriptorPropertyPage::OnBnClickedImportDlgBtn()
{
    ViewTIMBtn.EnableWindow( FALSE );
    ImportBtn.EnableWindow( FALSE );

    CImportDlg Dlg( TimDescriptor );
    ImportBtn.EnableWindow(FALSE);
    if ( Dlg.DoModal() == IDOK )
    {
        if ( TimDescriptor.TimDescriptorText() != Dlg.sTimText )
        {
            TimDescriptor = Dlg.TimDescriptor;
            TimDescriptor.Changed( true );

            DownloaderInterface.TimDescriptorParser.GetTimDescriptorLines( Dlg.Lines );
            DownloaderInterface.TimDescriptorParser.ParseDescriptor( DownloaderInterface );
        }
    }
    ImportBtn.EnableWindow(FALSE);
    RefreshState();
    if ( theApp.CommandLineParser.ProcessorType() != TimDescriptor.ProcessorType() )
        theApp.CommandLineParser.ProcessorType( TimDescriptor.ProcessorType() );
    
    DownloaderInterface.RefreshImageList( TimDescriptor.ImagesList() );
    ImageBuildDlg.RefreshState();
    TimDownloaderDlg.RefreshState();
    SetFocus();
    UpdateControls();
}

void CTimDescriptorPropertyPage::OnCbnSelchangeTimVersionCb()
{
    CString sText;
    TimVersionCb.GetLBText( TimVersionCb.GetCurSel(), sText );
    TimDescriptor.Version( string((CStringA)sText) );

    RefreshState();
}

void CTimDescriptorPropertyPage::OnCbnEditchangeTimVersionCb()
{
    CString sText;
    TimVersionCb.GetWindowTextA( sText );
    TimDescriptor.Version( string((CStringA)sText) );
}


BOOL CTimDescriptorPropertyPage::OnKillActive()
{
    // TODO: Add your specialized code here and/or call the base class
    TimDescriptor.WriteTimDescriptorFile();

    return CPropertyPage::OnKillActive();
}
