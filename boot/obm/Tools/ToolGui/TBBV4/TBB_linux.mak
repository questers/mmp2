###############################################################
#	Makefile for the Marvel NonTrusted Boot Builder
#
#
##############################################################

CC = g++
BASE:=$(shell pwd)
IPP_PATH = /opt/intel/ipp/6.1.0.039/ia32
IPP_INCLUDE_DIR = $(IPP_PATH)/include
IPP_STATIC_INCLUDE_DIR = $(IPP_PATH)/tools/staticlib
IPP_STATIC_LIB_DIR = $(IPP_PATH)/lib

TRUSTED=0
IPPV6=0
EXTENDED_RESERVED_DATA=1
MORONA=0

COMMONOBJS =Aspen_Strings.o \
	AutoBindERD.o \
	CommandLineParser.o \
	ConsumerID.o \
	DDRInitialization.o \
	DDROperations.o \
	ErdBase.o \
	EscapeSeqERD.o \
	ExtendedReservedData.o \
	GpioSetERD.o \
	ImageDescription.o \
	Instructions.o \
	Mmp2_Strings.o \
	ReservedPackageData.o \
	ResumeDdrERD.o \
	Sdram_Strings.o \
	TbrXferERD.o \
	TimDescriptor.o \
	TimDescriptorLine.o \
	TimDescriptorParser.o \
	TimLib.o \
	Ttc1_Strings.o \
	UartERD.o \
	UsbERD.o \
	UsbVendorReqERD.o \
	CoreResetERD.o \
	Mmp3_Strings.o \
	TzOperations.o \
	TzInitialization.o \

ifeq "$(MORONA)" "1"
	COMMONOBJS += Morona_Strings.o
endif

TBBOBJS = ImageBuilder.o \
		TBBV4.o

CCFLAGS += -D LINUX=1 -D WINDOWS=0 -D TOOLS_GUI=0 -D DDR_CONFIGURATION=1 -D _DEBUG=0

ifeq "$(EXTENDED_RESERVED_DATA)" "0"
	CCFLAGS += -D EXTENDED_RESERVED_DATA=0 -Wno-deprecated
else
	CCFLAGS += -D EXTENDED_RESERVED_DATA=1 -Wno-deprecated
endif


ifeq "$(TRUSTED)" "1"
	CCFLAGS += -D IPPV6=1
	TBBOBJS += TrustedImageBuilder.o
	COMMONOBJS += TrustedTimDescriptorParser.o \
			DigitalSignature.o \
			Key.o

	LIBS = $(IPP_STATIC_LIB_DIR)/libippcpmerged.a $(IPP_STATIC_LIB_DIR)/libippcore.a

	CCFLAGS += -D TRUSTED=1 -I $(IPP_STATIC_INCLUDE_DIR) -I $(IPP_INCLUDE_DIR)
	OUTPUT = tbb_linux.exe
else
	LIBS =
	CCFLAGS += -D TRUSTED=0
	OUTPUT = ntbb_linux.exe
endif

OBJS = $(COMMONOBJS) $(TBBOBJS)

all: $(OBJS)
	$(CC) -o $(OUTPUT) $(OBJS) $(LIBS)
	-rm *.o

$(COMMONOBJS): %.o: ../Common/%.cpp
	$(CC) $(CCFLAGS) -I $(BASE)/TBBV4 -I ../Common -c "../Common/$*.cpp" -o "$(BASE)/$*.o"

$(TBBOBJS):	%.o: $(BASE)/TBBV4/%.cpp
	$(CC) $(CCFLAGS) -I $(BASE)/TBBV4 -I "../Common" -c $(BASE)/TBBV4/$*.cpp -o $(BASE)/$*.o

.PHONY: clean
clean:
	-rm *.o $(OUTPUT)




