/******************************************************************************
 *
 *  (C)Copyright 2005 - 20011 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
// TBBV4.cpp : Defines the entry point for the console application.
//

#include "CommandLineParser.h"
#include "TimDescriptorParser.h"
#include "ImageBuilder.h"
#if TRUSTED
#include "TrustedImageBuilder.h"
#include "TrustedTimDescriptorParser.h"
#endif

#if _DEBUG
char TBBVersion[] = { "3.3.1.113 (Debug Build)" };
#else
char TBBVersion[] = { "3.3.1.113" };
#endif
char Date[]		  = { "20110302" };

#if LINUX
#include <unistd.h>
#else
#include <direct.h>
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

int main (int argc,char *argv[])
{
    CCommandLineParser		CommandLineParser;

#if TRUSTED
    CTrustedTimDescriptorParser TimDescriptorParser;
    CTrustedImageBuilder		ImageBuilder(CommandLineParser, TimDescriptorParser);
#else
    CTimDescriptorParser	TimDescriptorParser;
    CImageBuilder			ImageBuilder(CommandLineParser, TimDescriptorParser);
#endif

    printf ("\n\nTBB Version: %s\n", TBBVersion);
    printf ("TBB Date   : %s\n\n", Date);

    if (argc < MIN_ARGS)
    {
        CommandLineParser.PrintUsage();
        fflush(stdout);
        return FALSE;
    }
    fflush(stdout);

    if (CommandLineParser.ParseCommandLine (argc,argv) == FALSE) return FALSE;
    fflush(stdout);
    
    // set the current directory to the directory of the TIM.txt file
    string sWorkingDir;
    size_t iPos = CommandLineParser.KeyFileName.find_last_of("\\/");
    if ( iPos != string::npos )
    {
        sWorkingDir = CommandLineParser.KeyFileName.substr(0,iPos);
        if ( sWorkingDir.length() > 0 )
#if LINUX
            chdir( sWorkingDir.c_str() );
#else
            _chdir( sWorkingDir.c_str() );
#endif
    }

    int iOption = CommandLineParser.iOption;
    int iRet = TRUE;
    if ((iOption == 1) || (iOption == 2)) 
    {
        if ( !TimDescriptorParser.GetTimDescriptorLines(CommandLineParser) )
        {
            printf( "\n\nTBB Failed parsing Tim Descriptor file in GetTimDescriptorLines() \n" );
            iRet = FALSE;
        }

        if ( iRet == TRUE )
            iRet = TimDescriptorParser.ParseDescriptor( CommandLineParser ) ? TRUE:FALSE;

        if ( iRet == FALSE )
        {
            // failed either to parse or the Tim text Image
            printf ("\n\nTBB Failed to parse descriptor file!\n");
        }

        if ( iRet == TRUE )
            iRet = ImageBuilder.BuildDescriptorFile() ? TRUE:FALSE;

        if ( iRet == FALSE )
        {
            // failed either to build the Tim Bin Image
            printf ("\n\nTBB Failed to build binary TIM file!\n");
        }
    }
#if TRUSTED
    else if(iOption == 3)
    { 
        if ( TimDescriptorParser.ParsePrivateKeyFile( CommandLineParser ) )
        {
            iRet = ImageBuilder.BuildDigitalSignature();
            if ( iRet == FALSE )
            {
                printf ("\n\nTBB Failed to build Digital Signature!\n");
            }
        }
    }
    else if(iOption == 4)
    {
        iRet = ImageBuilder.AddDigitalSignatureToTim();
        if ( iRet == FALSE )
        {
            printf ("\n\nTBB Failed to Add Digital Signature to TIM binary file!\n");
        }
    }
#endif
    else
    {
        printf ("  Error: Invalid Option Mode Selected!");
    }

    fflush(stdout);
    return iRet;
}

