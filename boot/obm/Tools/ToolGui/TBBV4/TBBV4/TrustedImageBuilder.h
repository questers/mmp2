/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once

#if TRUSTED
#include "ImageBuilder.h"

#if IPPV6
#include <ipp_px.h>
#include <ippdefs.h>
#endif
#include <ippcp.h>

//crypto_scheme: used in TIM_3_4_00 and later 
const int PKCSv1_SHA1_1024RSA		= 0x0000A100;
const int PKCSv1_SHA256_1024RSA		= 0x0000A110;
const int PKCSv1_SHA1_2048RSA		= 0x0000A200;
const int PKCSv1_SHA256_2048RSA		= 0x0000A210;
const int ECCP256_FIPS_DSA_SHA1		= 0x0000B101;
const int ECCP256_FIPS_DSA_SHA256	= 0x0000B111;
//const int ECCP256_FIPS_DSA_SHA512	= 0x0000B141; 
const int ECCP521_FIPS_DSA_SHA1		= 0x0000B301; 
const int ECCP521_FIPS_DSA_SHA256	= 0x0000B311;
//const int ECCP521_FIPS_DSA_SHA512	= 0x0000B341;  


class CCommandLineParser;
class CTrustedTimDescriptorParser;
class CImageDescription;

class CTrustedImageBuilder : public CImageBuilder
{
public:
	CTrustedImageBuilder( CCommandLineParser& rCommandLineParser, 
						  CTrustedTimDescriptorParser& rTimDescriptorParser );
	virtual ~CTrustedImageBuilder(void);

	// trusted overrides base class untrusted TimDescriptorParser
	CTrustedTimDescriptorParser& TimDescriptorParser;

	// build the non-trusted images
	virtual bool BuildDescriptorFile();

	bool BuildDigitalSignature ();
	bool AddDigitalSignatureToTim();
	bool ProcessKeyInfo (fstream& fsTimBinFile);
	bool ProcessDsaInfo (fstream& fsTimBinFile);
	virtual bool GenerateHash (ifstream& ifsImage,CImageDescription& ImageDesc);
	bool DumpHashToFile (CImageDescription& ImageDesc);
	int GetTimHashSize (CTIM *pTimHeader, PLAT_DS& Ds);
	bool GenerateTimHash (fstream& fsTimBinFile, CTIM *pTimHeader, Ipp8u *pDsHash, unsigned int TimSizeToHash);
	bool VerifyTimHash ( fstream& fsTimBinFile, PLAT_DS *pDs,Ipp8u *pDsHash, unsigned int TimSizeToHash);
	bool DumpTimHashToBinFile (PLAT_DS *pDs,Ipp8u *pDsHash, char *HashFileName);
	bool GenerateKeyHash (KEY_MOD_3_4_0* key);
	void ConvertMSWToLSWAndFlip(PLAT_DS *pDs);
	bool InstallDigitalSignature (fstream& fsTimBinFile);
	bool OutputOtpHash (CTIM *pTimHeader,PLAT_DS *pDs);

	bool EncryptSignatureRSA (bool encrypt,PLAT_DS *pDs,Ipp8u *DsHash);
	bool EncryptSignatureECDSA (bool encrypt,PLAT_DS *pDs,Ipp8u *pDsHash);

	Ipp32u RsaDigitalSignature[MAXRSAKEYSIZEWORDS];
	Ipp32u ECDSADigitalSignature[2*MAXECCKEYSIZEWORDS];

	bool EnableEndianConvert;

private:
	void eccp_dsa_sign(unsigned int HashAlgorithmId, Ipp8u *pDsHash, int fesize, Ipp32u *pPriveKey32, IppsBigNumState* pSignature_r, IppsBigNumState* pSignature_s, IppECCType eccp_curve_type );
	IppsBigNumState* New_BN(int size, const Ipp32u* pData=0);
	void eccp_TimHash(int fesize, Ipp32u *pPriveKey32, IppsBigNumState* pSignature_r, IppsBigNumState* pSignature_s, IppECCType eccp_curve_type );
//	void SHA1_HashComputation (Ipp8u const *pSrcMsg, int const msgLength, Ipp8u  *pSHA1_digest);
	void Type_BN(const char* pMsg, const IppsBigNumState*  pBN);
	void eccp_dsa_sign_s_computation ( IppsBigNumState* pRandomK, IppsBigNumState* pOrder,
									   IppsBigNumState* pPriveKey_DSA, IppsBigNumState* r,
									   IppsBigNumState* s, IppsBigNumState * e, int BNsize32);

};

#endif // TRUSTED
