/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#include "Tim.h"
#include "TimLib.h"
#include "Partition.h"
#if TRUSTED
#include "TrustedTimDescriptorParser.h"
#else
#include "TimDescriptorParser.h"
#endif
#include "CommandLineParser.h"

using namespace std;

class CImageDescription;

class CImageBuilder : public CTimLib
{
public:
	CImageBuilder( CCommandLineParser& rCommandLineParser, 
				   CTimDescriptorParser& rTimDescriptorParser );

	virtual ~CImageBuilder(void);

	// build the non-trusted images
	virtual bool BuildDescriptorFile();
	bool GeneratePartitionBinary (PartitionTable_T& PartitionHeader, P_PartitionInfo_T& pPartitionInfo);

protected:
	fstream& OpenTimBinFile( ios_base::openmode mode );
	void CloseTimBinFile();

	bool ProcessImages(fstream& fsTimBinFile, unsigned int* pTimHashSize, int* pTimSizeWithNoDS );
	bool ProcessReservedData (fstream& fsTimBinFile);
	bool ProcessReservedDataInFile (fstream& fsTimBinFile);
	bool ProcessConcatenatedImages (fstream& fsTimBinFile);
//	bool GenerateTimCrc (fstream& fsTimBinFile);
//	bool GenerateImageCrc (FILE *hBinaryImage,int iIndex,UINT32_T *pImageCrc);
	bool PrependID (ifstream& ifsImage, CImageDescription& ImageDesc);
	virtual bool GenerateHash (ifstream& ifsImage,CImageDescription& ImageDesc) { return true; }

	CCommandLineParser&		CommandLineParser;
	CTimDescriptorParser&	TimDescriptorParser;
	fstream					m_fsTimBinFile;  // in/out stream
};
