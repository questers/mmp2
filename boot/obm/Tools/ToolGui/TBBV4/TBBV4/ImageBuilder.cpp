/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#if TOOLS_GUI == 1
#include "StdAfx.h"
#endif

#pragma warning ( disable : 4096 4081 4996 ) // Disable warning messages

#include "ImageBuilder.h"
#include "ImageDescription.h"
#include "Typedef.h"

#if LINUX
#include <string.h>
#include <stdlib.h>
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

CImageBuilder::CImageBuilder( CCommandLineParser& rCommandLineParser, 
                              CTimDescriptorParser& rTimDescriptorParser )
: CommandLineParser(rCommandLineParser),TimDescriptorParser(rTimDescriptorParser)
{
}


CImageBuilder::~CImageBuilder(void)
{

}

fstream& CImageBuilder::OpenTimBinFile ( ios_base::openmode mode )
{
    CloseTimBinFile();

    m_fsTimBinFile.open( TimDescriptorParser.TimDescriptorBinFilePath().c_str(), mode );
    if ( m_fsTimBinFile.bad() || m_fsTimBinFile.fail() )
    {
        printf ("\n  Error: Cannot open TIM Bin file name <%s> !\n", TimDescriptorParser.TimDescriptorBinFilePath().c_str() );
    }
    return m_fsTimBinFile;
}

void CImageBuilder::CloseTimBinFile ()
{
    if ( m_fsTimBinFile.is_open() )
        m_fsTimBinFile.close();
    m_fsTimBinFile.clear();
}

bool CImageBuilder::BuildDescriptorFile ()
{
    bool bRet = true;
    unsigned int TimHashSize = 0;
    char *pData = NULL;
    int numberOfLinesRead = 0;
    int sizeOfReservedDataFile=0;

    CTIM& TimHeader = TimDescriptorParser.TimDescriptor().TimHeader();

    printf ("\nProcessing non trusted TIM...\n\n");

    if ( CommandLineParser.bIsKeyFile )
    {
        // open new or replace existing tim bin file
        ios_base::openmode mode = ios_base::in | ios_base::out | ios_base::binary | ios_base::trunc;
        fstream& fsTimBinFile = OpenTimBinFile( mode ); // create new file
        if ( fsTimBinFile.bad() || fsTimBinFile.fail() )
            return false;

        if ( bRet && !ProcessImages( fsTimBinFile, &TimHashSize, 0 ) )
        {
            printf("  Error: Failed to process TIM images.\n");
            bRet = false;
        }

        if ( bRet && !ProcessReservedData( fsTimBinFile ) )
        {
            printf("  Error: Failed to process Reserved Data Area.\n");
            bRet = false;
        }

        if ( bRet && CommandLineParser.bIsReservedDataInFile)
        {
            if ( !ProcessReservedDataInFile( fsTimBinFile ) )
            {
                printf("  Error: Failed to process Reserved Data file.\n");
                bRet = false;
            }
        }

        if( bRet && TimHashSize != 0 )
        {
    //		if(ImageInfo.ImageSizeToHash != 0){
    //			GenerateTimCrc (hTimBinFile);  for now disable CRC
        }

        if ( bRet && CommandLineParser.bOneNANDPadding )
        {
            // pad out end of tim image to uiPaddedSize with all 0xFF
            const char PadByte = (char)0xff;
            int iPadNeeded = CommandLineParser.uiPaddedSize - TimDescriptorParser.TimDescriptor().GetTimImageSize();
            while ( iPadNeeded-- > 0 )
                fsTimBinFile.write( &PadByte, sizeof (char) );				
        }

        if ( bRet && CommandLineParser.bConcatenate)
        {
            if ( bRet && !ProcessConcatenatedImages (fsTimBinFile) )
            {
                printf("Failed to process concatenated images.\n");
                bRet = false;
            }
        }

        CloseTimBinFile();
    }

    if ( bRet && CommandLineParser.bIsPartitionDataFile 
        && (!CommandLineParser.bIsKeyFile || TimHeader.VersionBind.Version >= TBR_VERSIONS_WITH_PARTITION ) )
    {
        bRet = GeneratePartitionBinary(TimDescriptorParser.PartitionTable(), TimDescriptorParser.PartitionInfo());
    }

    if ( bRet )
    {
        printf ("  Success: Processing has completed successfully!\n\n");
    }
    else
    {
        printf ("  Failed: Processing has failed!\n\n");
    }

    return bRet;
}


bool CImageBuilder::ProcessImages(fstream& fsTimBinFile, unsigned int* pTimHashSize, int* pTimSizeWithNoDS )
{
    bool bRet = true;
    int i = 0;
    IMAGE_INFO_3_4_0 ImageInfo;

    CTIM& TimHeader = TimDescriptorParser.TimDescriptor().TimHeader();
    // return error if number of images > 10
    if (TimHeader.NumImages > MAX_IMAGE_FILES )
    {
        printf("  Error: Maximum allowable number of images in the descriptor file is 10!\n");
        return false;
    }
    
    t_ImagesList& Images = TimDescriptorParser.TimDescriptor().ImagesList();
    if (TimHeader.NumImages != Images.size() )
    {
        printf("  Error: Inconsistent number of images in TimDescriptorParser m_Images and TimHeader!\n");
        return false;
    }

    CImageDescription* pImageDesc = 0;
    t_ImagesIter ImagesIter = Images.begin();
    for (int i = 0; i < (int)TimHeader.NumImages; i++)
    {
        if ( ImagesIter == Images.end() )
            break;

        pImageDesc = (CImageDescription*)*ImagesIter;
        memset( &ImageInfo, 0, sizeof(IMAGE_INFO_3_4_0) );
        ImageInfo.ImageID = Translate(pImageDesc->ImageIdTag());
        ImageInfo.NextImageID = Translate(pImageDesc->NextImageIdTag());
        ImageInfo.FlashEntryAddr = Translate(pImageDesc->FlashEntryAddress());
        ImageInfo.LoadAddr = Translate(pImageDesc->LoadAddress());
        ImageInfo.ImageSizeToHash = pImageDesc->ImageSizeToHash();
        
        if ( pImageDesc->HashAlgorithmId() == "" )
            ImageInfo.HashAlgorithmID = 0;
        else if ( ToUpper(pImageDesc->HashAlgorithmId()) == "SHA-160" )
            ImageInfo.HashAlgorithmID = SHA160;
        else if ( ToUpper(pImageDesc->HashAlgorithmId()) == "SHA-256" )
            ImageInfo.HashAlgorithmID = SHA256;
        else if ( ToUpper(pImageDesc->HashAlgorithmId()) == "SHA-512" )
            ImageInfo.HashAlgorithmID = SHA512;

        ImageInfo.ImageSize = pImageDesc->ImageSize();
        ImageInfo.PartitionNumber = pImageDesc->PartitionNumber();

        if (i == 0)
        {
            if ( (ImageInfo.ImageID & TYPEMASK) != (TIMIDENTIFIER & TYPEMASK) ) // check for TIM*
            {
                printf ("  Error: The ImageID value is incorrect for the TIM image!\n");
                bRet = false;
                break;
            }
            
            if ( pTimSizeWithNoDS != 0 )
            {
                if (pTimSizeWithNoDS && 
                    (((int)ImageInfo.ImageSizeToHash > *pTimSizeWithNoDS) ||
                    (ImageInfo.ImageSizeToHash == 0xFFFFFFFF) ) )
                {
                    // Full (N)TIM hash
                    ImageInfo.ImageSizeToHash = *pTimSizeWithNoDS;
                }
            }

            unsigned ResSize = TimHeader.SizeOfReserved;
            if ( CommandLineParser.bOneNANDPadding )
            {
                // hack SizeOfReserved to include 0xff padded area
                unsigned int unpadded = TimDescriptorParser.TimDescriptor().GetTimImageSize( false, 0 );
                unsigned int padded = TimDescriptorParser.TimDescriptor().GetTimImageSize( true, CommandLineParser.uiPaddedSize );
                TimHeader.SizeOfReserved += (padded - unpadded);
            }
            *pTimHashSize = ImageInfo.ImageSizeToHash;
            fsTimBinFile.write((const char*)&TimHeader,sizeof (CTIM));

            if ( CommandLineParser.bOneNANDPadding )
                // restore correct SizeOfReserved
                TimHeader.SizeOfReserved = ResSize;
        }
        else
        {
            ifstream ifsImage;
            ifsImage.open( pImageDesc->ImageFilePath().c_str(), ios_base::in | ios_base::binary );
            if ( ifsImage.bad() || ifsImage.fail() )
            {
                printf ("  Error: Cannot open file name %s !",pImageDesc->ImageFilePath().c_str());
                bRet = false;
                break;
            }

            if (PrependID (ifsImage,*pImageDesc) == FALSE)
            {
                bRet = false;
                break;
            }

            if (GenerateHash (ifsImage,*pImageDesc) == FALSE)
            {
                bRet = false;
                break;
            }

            ifsImage.close();
        }

        memcpy( ImageInfo.Hash, pImageDesc->Hash, sizeof(ImageInfo.Hash) );

        if ( TimHeader.VersionBind.Version < TIM_3_2_00 )
        {
            // < TIM_3_2_00 uses IMAGE_INFO_3_1_0
            IMAGE_INFO_3_1_0 ImageInfo_3_1;
            ImageInfo_3_1.ImageID = ImageInfo.ImageID;
            ImageInfo_3_1.NextImageID = ImageInfo.NextImageID;
            ImageInfo_3_1.FlashEntryAddr = ImageInfo.FlashEntryAddr;
            ImageInfo_3_1.LoadAddr = ImageInfo.LoadAddr;
            ImageInfo_3_1.ImageSize = ImageInfo.ImageSize;
            ImageInfo_3_1.ImageSizeToHash = ImageInfo.ImageSizeToHash;
            ImageInfo_3_1.HashAlgorithmID = ImageInfo.HashAlgorithmID;
            memcpy( &ImageInfo_3_1.Hash, &ImageInfo.Hash, sizeof( ImageInfo_3_1.Hash ));
            fsTimBinFile.write((const char*)&ImageInfo_3_1, sizeof(ImageInfo_3_1) );
        }
        else if ( TimHeader.VersionBind.Version < TIM_3_4_00 )
        {
            // >= TIM_3_2_00 && < TIM_3_4_00 uses IMAGE_INFO_3_2_0
            IMAGE_INFO_3_2_0 ImageInfo_3_2;
            ImageInfo_3_2.ImageID = ImageInfo.ImageID;
            ImageInfo_3_2.NextImageID = ImageInfo.NextImageID;
            ImageInfo_3_2.FlashEntryAddr = ImageInfo.FlashEntryAddr;
            ImageInfo_3_2.LoadAddr = ImageInfo.LoadAddr;
            ImageInfo_3_2.ImageSize = ImageInfo.ImageSize;
            ImageInfo_3_2.ImageSizeToHash = ImageInfo.ImageSizeToHash;
            ImageInfo_3_2.HashAlgorithmID = ImageInfo.HashAlgorithmID;
            memcpy( &ImageInfo_3_2.Hash, &ImageInfo.Hash, sizeof( ImageInfo_3_2.Hash ));
            ImageInfo_3_2.PartitionNumber = ImageInfo.PartitionNumber;
            fsTimBinFile.write((const char*)&ImageInfo_3_2, sizeof(ImageInfo_3_2) );
        }
        else if ( TimHeader.VersionBind.Version >= TIM_3_4_00 )
        {
            // >= TIM_3_4_00 uses IMAGE_INFO_3_4_0 until a new image info format is needed
            fsTimBinFile.write((const char*)&ImageInfo, sizeof(IMAGE_INFO_3_4_0) );
        }
    
        ImagesIter++;
    }

    return bRet;
}

bool CImageBuilder::PrependID (ifstream& ifsImage,CImageDescription& ImageDesc)
{
const int CHUNK_SIZE = 8192;
    BYTE Data[CHUNK_SIZE] = {0};
    string sImageOutFilename;
    ofstream ofsImageOutFile;

    // This routine simply prepends a 32 bit Image ID to a binary image and
    // saves the resultant off as a new file.

    ifsImage.clear();
    ifsImage.seekg( 0, ios_base::beg );

    if ( !CreateOutputImageName( ImageDesc.ImageFilePath(), sImageOutFilename) )
        return false;

    ofsImageOutFile.open( sImageOutFilename.c_str(), ios_base::out | ios_base::trunc | ios_base::binary );
    if ( ofsImageOutFile.bad() || ofsImageOutFile.fail() )
    {
        printf ("\n  Error: fopen failed to open file: <%s>.\n",sImageOutFilename.c_str());
        return false;
    }

    // Prepend Image ID to output image
    int ImageId = Translate((char*)ImageDesc.ImageIdTag().c_str());
    ofsImageOutFile.write( (char*)&ImageId, sizeof (UINT_T) );

    // Copy input image to output image
    std::streamsize iBlockSize = CHUNK_SIZE;
    while (iBlockSize == CHUNK_SIZE)
    {
        ifsImage.read( (char*)Data, iBlockSize );
        iBlockSize = ifsImage.gcount();
        ofsImageOutFile.write( (char*)Data, iBlockSize );
    }
    ofsImageOutFile.close();

    return true;
}

#if 0
bool CImageBuilder::GenerateImageCrc (FILE *hBinaryImage,int iIndex,UINT32_T *pImageCrc) 
{
    long lFileSize = 0;
    unsigned char *Buffer = NULL;

    // A check sum is generated for the entirity of each image, listed 
    // by the TIM, and stored in its appropriate IMAGE_INFO section.

    fseek (hBinaryImage,0L,SEEK_END); // Set position to EOF
    lFileSize = ftell (hBinaryImage); // Get position of file, thus the file size.
    fseek (hBinaryImage,0L,SEEK_SET); // Set position to SOF

    if (NULL == (Buffer = (unsigned char *)malloc (lFileSize)))
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: Memory allocation for Buffer failed in GenerateImageCrc()!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }

    if (fread (Buffer,1,lFileSize,hBinaryImage) != (unsigned int)lFileSize)
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: UntrustedChecksum couldn't read all of the %ld bytes.\n",lFileSize);
        printf ("Sorry couldn't calculate checksum in GenerateImageCrc()!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        if (Buffer != NULL) free (Buffer);
        return false;
    }

    *pImageCrc = CheckSum (Buffer,lFileSize);
    printf ("CRC for non trusted Image: 0x%08X\n",*pImageCrc);

    if (Buffer != NULL) free (Buffer);
    return true;
}

bool CImageBuilder::GenerateTimCrc (fstream& fsTimBinFile)
{
    long lFileSize = 0;
    unsigned char *TimBuffer = NULL;
    CTIM TimHeader;
    IMAGE_INFO ImageInfo;
    UINT32_T ImageOffset = 0;
    UINT32_T NextImageOffset = 0;
    UINT32_T ImageCrc = 0;

    // A check sum of the entire TIM is generated by this function. The value
    // is stored in the first image module of the TIM. Refer to header file
    // Tim.h for the layout of the IMAGE_INFO struct. The check sum will reside
    // in the base position of the Hash array within that struct.

    fsTimBinFile.seekg( 0, ios_base::end );
    fsTimBinFile.clear();

    lFileSize = fsTimBinFile.tellg();
    fsTimBinFile.seekg( 0, ios_base::beg );

    if (NULL == (TimBuffer = (unsigned char *)malloc (lFileSize)))
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: Memory allocation for TimBuffer failed in GenerateTimCrc()!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return FALSE;
    }

    fsTimBinFile.read( (char*)TimBuffer, lFileSize );
    if ( fsTimBinFile.bad() || fsTimBinFile.fail() )
    {

        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: GenerateTimCrc couldn't read all of the %ld bytes.\n",lFileSize);
        printf ("Sorry couldn't calculate checksum in GenerateTimCrc()!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        if (TimBuffer != NULL) free (TimBuffer);
        return false;
    }

    ImageCrc = CheckSum (TimBuffer,lFileSize);
    printf ("CRC for non trusted TIM: 0x%08X\n\n",ImageCrc);
    if (TimBuffer != NULL) free (TimBuffer);

    fsTimBinFile.seekg( 0, ios_base::beg );  // Set position to SOF
    fsTimBinFile.read( (char*)&TimHeader, sizeof(CTIM) );

    ImageOffset = fsTimBinFile.tellg();

    fsTimBinFile.read( (char*)&ImageInfo, sizeof(IMAGE_INFO) );

    NextImageOffset = fsTimBinFile.tellg();

    ImageInfo.Hash[0] = ImageCrc;
    fsTimBinFile.seekg( ImageOffset, ios_base::beg ); // Set position to Image offset

    fsTimBinFile.write( (char*)&ImageInfo, sizeof(IMAGE_INFO) );

    return true;
}
#endif


bool CImageBuilder::ProcessReservedData (fstream& fsTimBinFile) 
{
    int iTotal = 0;

    // create a local list that combines the ReservedData and the ExtendedReservedData
    CTimDescriptor& TimDescriptor = TimDescriptorParser.TimDescriptor();
    t_ReservedDataList ReservedDataList = TimDescriptor.ReservedDataList();
#if EXTENDED_RESERVED_DATA
    int nBytesAdded = TimDescriptor.ExtendedReservedData().Combine( ReservedDataList );
#else
    int nBytesAdded = 0;
#endif

    if ( ReservedDataList.size() == 0 )
        return true;  // no reserved data to process

    fsTimBinFile.seekg( 0, ios_base::end ); // Set position to EOF
    fsTimBinFile.clear();

    // reserved data area id
    DWORD dwValue = WTPRESERVEDAREAID; 
    fsTimBinFile.write((const char*)&dwValue, sizeof (DWORD));
//	fsTimBinFile << dwValue;
    iTotal++;

    // number of packages
    dwValue = (DWORD)(ReservedDataList.size() + 1);  // +1 for reserved data terminator package
    fsTimBinFile.write((const char*)&dwValue, sizeof (DWORD));
//	fsTimBinFile << dwValue;
    iTotal++;

    CReservedPackageData * pPackageData = 0;
    t_ReservedDataListIter iterPackage( ReservedDataList.begin());
    while( iterPackage != ReservedDataList.end() )
    {
        pPackageData = *iterPackage;
        // package tag id
        dwValue = Translate(pPackageData->PackageIdTag()); 
        fsTimBinFile.write((const char*)&dwValue, sizeof (DWORD));
//		fsTimBinFile << dwValue;
        iTotal++;

        // package size in bytes
        dwValue = pPackageData->Size(); 
        fsTimBinFile.write((const char*)&dwValue, sizeof (DWORD));
//		fsTimBinFile << dwValue;
        iTotal++;

        t_stringList& PackageDataList = pPackageData->PackageDataList();
        t_stringListIter iterData( PackageDataList.begin() );
        while( iterData != PackageDataList.end() )
        {
            // package data values
            dwValue = Translate(*(*iterData)); 
            fsTimBinFile.write((const char*)&dwValue, sizeof (DWORD));
//			fsTimBinFile << dwValue;
            iTotal++;
            iterData++;
        }
        iterPackage++;
    }

    dwValue = TERMINATORID; 
    fsTimBinFile.write((const char*)&dwValue, sizeof (DWORD));
//	fsTimBinFile << dwValue;
    iTotal++;

    dwValue = 8; // size of terminator
    fsTimBinFile.write((const char*)&dwValue, sizeof (DWORD));
//	fsTimBinFile << dwValue;
    iTotal++;

    if ((iTotal * sizeof(DWORD)) != TimDescriptorParser.TimDescriptor().TimHeader().SizeOfReserved)
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: The number of reserved data bytes written, %d, does not equal\n",iTotal * 4);
        printf ("the number indicated by 'Size of Reserved in bytes' directive\n");
        printf ("in the descriptor file. SizeOfReserved = %d\n", TimDescriptorParser.TimDescriptor().TimHeader().SizeOfReserved);
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }

    return true;
}

bool CImageBuilder::ProcessReservedDataInFile (fstream& fsTimBinFile) 
{
    char* pReservedBuffer = 0;
    unsigned int sizeOfReservedDataFile = 0;

    fsTimBinFile.seekg( 0, ios_base::end ); // Set position to EOF
    fsTimBinFile.clear();

    ifstream ifsReservedFile;
    ifsReservedFile.open( CommandLineParser.ReservedFileName.c_str(), ios_base::in | ios_base::binary );
    if ( ifsReservedFile.bad() || ifsReservedFile.fail() )
    {
        printf ("  Error: Cannot open file name %s !", CommandLineParser.ReservedFileName.c_str());
        return false;
    }
    else
    {
        ifsReservedFile.seekg( 0, ios_base::end ); // Set position to EOF
        ifsReservedFile.clear();
        // Get position of file, thus the file size.
        // note that the (unsigned int) cast is dangerous for very large files but should not 
        // be an issue here
        sizeOfReservedDataFile = (unsigned int)ifsReservedFile.tellg(); 
        ifsReservedFile.seekg( 0, ios_base::beg );

        // allocate space for reading reserved data
        pReservedBuffer = (char*)calloc(sizeOfReservedDataFile, sizeof(char));
        // init the buffer
        memset (pReservedBuffer,0,sizeOfReservedDataFile);
        // read file to buffer
        ifsReservedFile.read( pReservedBuffer, sizeOfReservedDataFile );
        // write out buffer to tim bin file
        fsTimBinFile.write(pReservedBuffer, sizeof (char)*sizeOfReservedDataFile);
    }

    ifsReservedFile.close();
    return true;
}


bool CImageBuilder::ProcessConcatenatedImages (fstream& fsTimBinFile) 
{
    unsigned int location = 0;
    BYTE padding = 0x0;
    BYTE Data[8192] = {0};

    CImageDescription* pImageDesc = 0;
    t_ImagesList& Images = TimDescriptorParser.TimDescriptor().ImagesList();
    t_ImagesIter ImagesIter = Images.begin();
    while ( ImagesIter != Images.end() )
    {
        pImageDesc = (CImageDescription*)*ImagesIter;

        fsTimBinFile.seekg( 0, ios_base::end ); // Set position to EOF
        fsTimBinFile.clear();
        // note that the (unsigned int) cast is dangerous for very large files but should not 
        // be an issue here
        location = (unsigned int)fsTimBinFile.tellg();

        //pad out the current binary until we get to the location of where the next image should start
        unsigned int uiFlashEntryAddr = Translate((char*)pImageDesc->FlashEntryAddress().c_str());
        if( uiFlashEntryAddr > location)
            fsTimBinFile.write( (char*)&padding,(uiFlashEntryAddr - location) );

        //open the next image file
        ifstream ifsImage;
        ifsImage.open(pImageDesc->ImageFilePath().c_str(), ios_base::in | ios_base::binary );
        if ( ifsImage.bad() || ifsImage.fail() )
        {
            printf ("  Error: Cannot open file name %s !\n",pImageDesc->ImageFilePath().c_str());
            return false;
        }

        //now write out image in 8K chunks
        int iBlockSize = 8192;
        while (iBlockSize == 8192)
        {
            ifsImage.read( (char*)Data, iBlockSize );
            // note that the (unsigned int) cast is dangerous for very large files but should not 
            // be an issue here
            iBlockSize = (unsigned int)ifsImage.gcount();
            fsTimBinFile.write( (char*)Data, iBlockSize );
        }

        ifsImage.close();
        ImagesIter++;
    }

    return true;
}

bool CImageBuilder::GeneratePartitionBinary (PartitionTable_T& PartitionHeader, P_PartitionInfo_T& pPartitionInfo)
{
    if ( pPartitionInfo == 0 )
    {
        printf ("\n  Error: No PartitionInfo to process!\n" );
        return false;
    }

    char *PartitionBinFileName="partition.bin";
    ofstream ofsPartitionBinFile;
    ofsPartitionBinFile.open( PartitionBinFileName, ios_base::out | ios_base::binary | ios_base::trunc );
    if ( ofsPartitionBinFile.bad() || ofsPartitionBinFile.fail() )
    {
        printf ("\n  Error: Cannot open file name %s !\n", PartitionBinFileName);
        return false;
    }

    ofsPartitionBinFile.write( (char*)&PartitionHeader, sizeof(PartitionTable_T) );
    for (int j = 0; j < (int)PartitionHeader.NumPartitions; j++)
    {
        ofsPartitionBinFile.write( (char*)&pPartitionInfo[j], sizeof(PartitionInfo_T) );
    }

    ofsPartitionBinFile.close();

//	printf("\n  Success: Partitioning binary has been generated successfully!\n");
    return true;
}

