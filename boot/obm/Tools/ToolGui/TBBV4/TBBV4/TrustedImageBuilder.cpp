/******************************************************************************
 *
 *  (C)Copyright 2005 - 2011 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif
#pragma warning ( disable : 4996 4096 4081 )// Disable warning messages

#include "CommandLineParser.h"
#include "TrustedImageBuilder.h"
#include "TrustedTimDescriptorParser.h"
#include "ImageDescription.h"
#include <sstream>

#ifdef _DEBUG
#define DBGVERIFY 1
#else
#define DBGVERIFY 0
#endif

#if LINUX
#include <cstring>
#include <cstdlib>
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

#if TRUSTED

CTrustedImageBuilder::CTrustedImageBuilder( CCommandLineParser& rCommandLineParser, 
                                            CTrustedTimDescriptorParser& rTimDescriptorParser )
: CImageBuilder( rCommandLineParser, rTimDescriptorParser ), TimDescriptorParser( rTimDescriptorParser )
{
    memset( RsaDigitalSignature, 0, sizeof(RsaDigitalSignature) );
    memset( ECDSADigitalSignature, 0, sizeof(ECDSADigitalSignature) );
    EnableEndianConvert = false;
}

CTrustedImageBuilder::~CTrustedImageBuilder(void)
{
}

bool CTrustedImageBuilder::BuildDescriptorFile()
{
    bool bRet = true;
    char* pReservedBuffer = 0;	
    unsigned int HashSize = 0;
    int TimSizeWithNoDS = 0;
    Ipp8u DsHash[sizeof(Ipp32u)*MAXRSAKEYSIZEWORDS] = {0};

    CTimDescriptor& TimDescriptor = TimDescriptorParser.TimDescriptor();

    // if TRUSTED version of TBB is used to build a non-trusted
    // image, then redirect to the non-trusted image builder
    if ( !TimDescriptor.Trusted() )
        return CImageBuilder::BuildDescriptorFile();

    CTIM& TimHeader = TimDescriptor.TimHeader();
    PLAT_DS& Ds = TimDescriptorParser.Ds;

    if (CommandLineParser.iOption == 1)
    {
        printf ("\nProcessing trusted TIM...\n");
    }
    else
    {
        printf ("\nProcessing trusted TIM without digital signature and\n");
        printf ("building TIM hash binary file...\n");
    }

    if ( CommandLineParser.bIsKeyFile )
    {
        // open new or replace existing tim bin file
        ios_base::openmode mode = ios_base::in | ios_base::out | ios_base::binary | ios_base::trunc;
        fstream& fsTimBinFile = OpenTimBinFile( mode ); // create new file
        if ( fsTimBinFile.bad() || fsTimBinFile.fail() )
            return false;

        TimSizeWithNoDS = GetTimHashSize( &TimHeader, Ds );

        if ( bRet && !ProcessImages( fsTimBinFile, &HashSize, &TimSizeWithNoDS ) )
        {
            printf("  Error: Failed to process TIM images.\n");
            bRet = false;
        }

        if (bRet && ProcessKeyInfo ( fsTimBinFile ) == FALSE)
            bRet = false;

        if ( bRet && !ProcessReservedData( fsTimBinFile ) )
        {
            printf("  Error: Failed to process Reserved Data Area.\n");
            bRet = false;
        }

        if ( bRet && CommandLineParser.bIsReservedDataInFile )
        {
            if ( !ProcessReservedDataInFile( fsTimBinFile ) )
            {
                printf("  Error: Failed to process Reserved Data file.\n");
                bRet = false;
            }
        }

        if ( CommandLineParser.bOneNANDPadding )
        {
            // pad out end of tim image to uiPaddedSize with all 0xFF
            const char PadByte = (char)0xff;
            // note that the (unsigned int) cast is dangerous for very large files but should not 
            // be an issue here
            unsigned int lLoc = (int)fsTimBinFile.tellg();
            int iPadNeeded = CommandLineParser.uiPaddedSize - sizeof(PLAT_DS) - lLoc;
            while ( iPadNeeded-- > 0 )
                fsTimBinFile.write( &PadByte, sizeof(char) );				

        // note that the (long) cast is dangerous for very large files but should not 
        // be an issue here
            lLoc = (long)fsTimBinFile.tellg();
        }

        if (bRet && ProcessDsaInfo ( fsTimBinFile ) == FALSE)
            bRet = false;

        if (bRet && GenerateTimHash ( fsTimBinFile,& TimHeader,(Ipp8u *)&DsHash, HashSize) == FALSE)
            bRet = false;

        // If option 2 is selected the digital signature will be installed later
        if (bRet && CommandLineParser.iOption == 2)
        {
            DumpTimHashToBinFile (&Ds,(Ipp8u *)&DsHash, (char*)CommandLineParser.HashFileName.c_str());
        }
        else if ( bRet )
        {
            if ( Ds.DSAlgorithmID == ECDSA_256 || Ds.DSAlgorithmID == ECDSA_521 )
                bRet = EncryptSignatureECDSA( true, &Ds, (Ipp8u *)&DsHash );
            else
            {
                bRet = EncryptSignatureRSA( true, &Ds, (Ipp8u *)&DsHash );
                if ( bRet )
                    ConvertMSWToLSWAndFlip(&Ds);
            }
            
            if ( bRet )
            {
                if (InstallDigitalSignature (fsTimBinFile) == FALSE)
                    bRet = false;
                else
                {
                    if ( CommandLineParser.bIsTimVerify == true )
                    {
                        if ( Ds.DSAlgorithmID == ECDSA_256 || Ds.DSAlgorithmID == ECDSA_521 )
                        {
                            // tbd need to figure out how to verify ecdsa
                        }
                        else
                        {
                            memset (DsHash,0,sizeof(DsHash));

                            fsTimBinFile.seekg(TimSizeWithNoDS, ios_base::beg); // Set position to DS
                            fsTimBinFile.read((char*)DsHash,sizeof(Ipp32u)*MAXRSAKEYSIZEWORDS);

                            if (EncryptSignatureRSA (false,&Ds,(Ipp8u *)&DsHash) == FALSE)
                                bRet = false;

#if 1
    //						ConvertMSWToLSWAndFlip(&Ds);

                            if ( bRet )
                            {
                                if ( HashSize == TimSizeWithNoDS )
                                {
                                    // Compare full Tim(minus DS) against the decrypted DS.
                                    if (VerifyTimHash (fsTimBinFile, &Ds,(Ipp8u *)&DsHash, TimSizeWithNoDS) == FALSE)
                                        bRet = false;
                                }
                                else
                                {
                                    // Compare partial Tim against the decrypted DS.
                                    if (VerifyTimHash (fsTimBinFile, &Ds,(Ipp8u *)&DsHash, HashSize) == FALSE)
                                        bRet = false;
                                }
                            }

#else
                            ConvertMSWToLSWAndFlip(&Ds);

                            if ( bRet )
                            {
                                // get the max size of the TIM to be hashed
                                unsigned int TotalTimSize = GetTimHashSize (&TimHeader, Ds);

                                if ( TotalTimSize == TimSizeWithNoDS )
                                {
                                    // Compare full Tim(minus DS) against the decrypted DS.
                                    if (VerifyTimHash (fsTimBinFile, &Ds,(Ipp8u *)&DsHash, TimSizeWithNoDS) == FALSE)
                                        bRet = false;
                                }
                                else
                                {
                                    // Compare partial Tim against the decrypted DS.
                                    if (VerifyTimHash (fsTimBinFile, &Ds,(Ipp8u *)&DsHash, TotalTimSize) == FALSE)
                                        bRet = false;
                                }
                            }
#endif
                        }
                    }
                }
            }
        }

        if ( bRet && CommandLineParser.bConcatenate)
        {
            if ( bRet && !ProcessConcatenatedImages (fsTimBinFile) )
            {
                printf("Failed to process concatenated images.\n");
                bRet = false;
            }
        }

        CloseTimBinFile();
    }

    if ( bRet && CommandLineParser.bIsPartitionDataFile 
        && (!CommandLineParser.bIsKeyFile || TimHeader.VersionBind.Version >= TBR_VERSIONS_WITH_PARTITION ) )
    {
        bRet = GeneratePartitionBinary(TimDescriptorParser.PartitionTable(), TimDescriptorParser.PartitionInfo());
    }

    if ( bRet )
    {
        printf ("  Success: Processing has completed successfully!\n\n");
    }
    else
    {
        printf ("  Failed: Processing has failed!\n\n");
    }

    return bRet;
}


bool CTrustedImageBuilder::BuildDigitalSignature ()
{
    char szDword[11] = {0};
    Ipp8u DsHash[sizeof(Ipp32u)*MAXRSAKEYSIZEWORDS] = {0};
    std::streamoff lFileSize = 0;
    PLAT_DS Ds;
    bool IsOk =  false;
    UINT_T IntParam = 0;

    memset( &Ds, 0, sizeof(Ds) );

    // This function is used in option mode 3 where a digital signature
    // is produced from a binary hash of the TIM and a private key file.

    printf ("\nBuilding Digital Signature...\n");

    // Reading TIM hash from a file
    ifstream ifsTimHash;
    ifsTimHash.open( CommandLineParser.HashFileName.c_str(), ios_base::in | ios_base::binary );
    if ( ifsTimHash.bad() || ifsTimHash.fail() )
    {
        printf ("\n  Error: Cannot open file name %s !\n",CommandLineParser.HashFileName.c_str());
        return FALSE;
    }

    ifsTimHash.seekg( 0, ios_base::end );
    ifsTimHash.clear();

    lFileSize = ifsTimHash.tellg();  // Get position of file, thus the file size.
    ifsTimHash.seekg( 0, ios_base::beg );  // Set position to SOF

    if (lFileSize > sizeof(DsHash))
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: BuildDigitalSignature Error:\n");
        printf ("The size of the input hash file is greater than 256 bytes!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        ifsTimHash.close();
        return FALSE;
    }

    memset (DsHash,0,sizeof(DsHash));
    ifsTimHash.read( (char*)DsHash, lFileSize );
    ifsTimHash.close();

    // Reading private key from tim descriptor file
    ifstream ifsPrivateKeyFile;
    ifsPrivateKeyFile.open( CommandLineParser.KeyFileName.c_str(), ios_base::in );
    if ( ifsPrivateKeyFile.bad() || ifsPrivateKeyFile.fail() )
    {
        printf ("\n  Error: Cannot open file name %s !\n",CommandLineParser.KeyFileName.c_str());
        return FALSE;
    }

    if ( CommandLineParser.iOption == 1 )
    {
        if (!GetDWord (ifsPrivateKeyFile, "DSA Algorithm ID: ", &IntParam) )
        {
            printf ("////////////////////////////////////////////////////////////\n");
            printf ("  Error: Private Key File Parsing Error:\n");
            printf ("Key file parsing error reading, DSA Algorithm ID:\n");
            printf ("////////////////////////////////////////////////////////////\n");
            ifsPrivateKeyFile.close();
            return FALSE;
        }
        Ds.DSAlgorithmID = (ENCRYPTALGORITHMID_T)IntParam;
    }

    if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version < TIM_3_3_00 )
    {
        if (!GetDWord (ifsPrivateKeyFile, "Modulus Size in bytes: ",&IntParam) )
        {
            printf ("////////////////////////////////////////////////////////////\n");
            printf ("  Error: Private Key File Parsing Error:\n");
            printf ("Key file parsing error reading, Modulus Size in bytes:\n");
            printf ("////////////////////////////////////////////////////////////\n");
            ifsPrivateKeyFile.close();
            return FALSE;
        }
        Ds.KeySize = IntParam*8;
    }
    else
    {
        if (!GetDWord (ifsPrivateKeyFile, "Key Size in bits: ",&IntParam) )
        {
            printf ("////////////////////////////////////////////////////////////\n");
            printf ("  Error: Private Key File Parsing Error:\n");
            printf ("Key file parsing error reading, Key Size in bits:\n");
            printf ("////////////////////////////////////////////////////////////\n");
            ifsPrivateKeyFile.close();
            return FALSE;
        }
        Ds.KeySize = IntParam;
    }


    if (!GetDWord (ifsPrivateKeyFile, "RSA System Modulus:",0) )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: Private Key File Parsing Error:\n");
        printf ("Key file parsing error reading, RSA System Modulus:\n");
        printf ("////////////////////////////////////////////////////////////\n");
        ifsPrivateKeyFile.close();
        return FALSE;
    }
    
    IsOk = TimDescriptorParser.FillDataArrays ( ifsPrivateKeyFile, (unsigned long *)Ds.Rsa.RSAModulus, Ds.KeySize/8 );
    if (IsOk == FALSE)
    {
        printf ("  Error: Private Key File Parsing Error:\n");
        if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version < TIM_3_3_00 ) 
        {
            printf ("Modulus Size in bytes = %d\n\n",Ds.KeySize/8);
        }
        else
        {
            printf ("Key Size in bits = %d\n\n",Ds.KeySize);
        }

        printf ("The amount of data read from key file doesn't match what is\n");
        if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version < TIM_3_3_00 ) 
        {
            printf ("indicated by 'Modulus Size in bytes' directive!\n");
        }
        else
        {
            printf ("indicated by 'Key Size in bits' directive!\n");
        }
        printf ("Error: RSA System Modulus for DSA Algorithm\n");
        printf ("////////////////////////////////////////////////////////////\n");
        ifsPrivateKeyFile.close();
        return FALSE;
    }

    if (!GetDWord (ifsPrivateKeyFile, "RSA Private Key:",0) )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: Private Key File Parsing Error:\n");
        printf ("Key file parsing error reading, RSA Private Key:\n");
        printf ("////////////////////////////////////////////////////////////\n");
        ifsPrivateKeyFile.close();
        return FALSE;
    }

    IsOk = TimDescriptorParser.FillDataArrays ( ifsPrivateKeyFile,(unsigned long *)TimDescriptorParser.RsaPrivateKey, Ds.KeySize/8 );
    if (IsOk == FALSE)
    {
        printf ("  Error: Private Key File Parsing Error:\n");
        if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version < TIM_3_3_00 ) 
        {
            printf ("Modulus Size in bytes = %d\n\n",Ds.KeySize/8);
        }
        else
        {
            printf ("Key Size in bits = %d\n\n",Ds.KeySize);
        }

        printf ("The amount of data read from key file doesn't match what is\n");
        if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version < TIM_3_3_00 ) 
        {
            printf ("indicated by 'Modulus Size in bytes' directive!\n");
        }
        else
        {
            printf ("indicated by 'Key Size in bits' directive!\n");
        }
        printf ("Error: RSA Private Key for DSA Algorithm\n");
        printf ("////////////////////////////////////////////////////////////\n");
        ifsPrivateKeyFile.close();
        return FALSE;
    }

    ifsPrivateKeyFile.close();

    if (EncryptSignatureRSA (true,&Ds,(Ipp8u *)&DsHash) == FALSE)
    {
        return FALSE;
    }
    ConvertMSWToLSWAndFlip(&Ds);

    // Writing digital signature to a file
    fstream fsDsFile;
    fsDsFile.open( CommandLineParser.DsFileName.c_str(), ios_base::out | ios_base::binary | ios_base::trunc );
    if ( fsDsFile.bad() || fsDsFile.fail() )
    {
        printf ("\n  Error: Cannot open TIM Bin file name <%s> !\n", CommandLineParser.DsFileName.c_str() );
    }

    InstallDigitalSignature (fsDsFile);

    return TRUE;
}

bool CTrustedImageBuilder::AddDigitalSignatureToTim ()
{
    PLAT_DS& Ds = TimDescriptorParser.Ds;

    long lFileSize = 0;
    long lSizeOfDs = 0;
    if ( Ds.DSAlgorithmID == ECDSA_256 || Ds.DSAlgorithmID == ECDSA_521 )
        lSizeOfDs = 2*MAXECCKEYSIZEWORDS * sizeof(Ipp32u);
    else
        MAXRSAKEYSIZEWORDS * sizeof(Ipp32u);

    // This function is used in option mode 4 where a digital signature is read
    // from a binary file and appended at the end of a TIM binary file.

    printf ("\nInstalling Digital Signature...\n");

    // Reading digital signature from a file

    ifstream ifsDsFile;
    ifsDsFile.open( CommandLineParser.DsFileName.c_str(), ios_base::in | ios_base::binary );
    if ( ifsDsFile.bad() || ifsDsFile.fail() )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: AddDigitalSignatureToTim Error:\n");
        printf ("Cannot open file name %s !\n",CommandLineParser.DsFileName.c_str());
        printf ("////////////////////////////////////////////////////////////\n");
        return FALSE;
    }

    ifsDsFile.seekg( 0, ios_base::end );  // Set position to EOF
    ifsDsFile.clear();
    // note that the (long) cast is dangerous for very large files but should not 
    // be an issue here
    lFileSize = (long)ifsDsFile.tellg(); // Get position of file, thus the file size.
    ifsDsFile.seekg( 0, ios_base::beg);  // Set position to SOF

    if (lFileSize != lSizeOfDs)
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: AddDigitalSignatureToTim Error:\n");
        printf ("The size of the digital signature file != %d!\n",lSizeOfDs);
        printf ("////////////////////////////////////////////////////////////\n");
        ifsDsFile.close();
        return FALSE;
    }

    memset (RsaDigitalSignature, 0, sizeof(RsaDigitalSignature) );
    memset (ECDSADigitalSignature, 0, sizeof(ECDSADigitalSignature) );

    if ( Ds.DSAlgorithmID == ECDSA_256 || Ds.DSAlgorithmID == ECDSA_521 )
        ifsDsFile.read( (char*)ECDSADigitalSignature, lFileSize );
    else
        ifsDsFile.read( (char*)RsaDigitalSignature, lFileSize );
    ifsDsFile.close();

    ifstream ifsTimInFile;
    ifsTimInFile.open( CommandLineParser.TimInFileName.c_str(), ios_base::in | ios_base::binary );
    if ( ifsTimInFile.bad() || ifsTimInFile.fail() )
    {
        printf ("\n  Error: Cannot open TIM In file name <%s> !\n", CommandLineParser.TimInFileName.c_str() );
    }

    fstream fsTimOutFile;
    fsTimOutFile.open( CommandLineParser.TimOutFileName.c_str(), ios_base::out | ios_base::binary | ios_base::trunc );
    if ( fsTimOutFile.bad() || fsTimOutFile.fail())
    {
        printf ("\n  Error: Cannot open TIM Out file name <%s> !\n", CommandLineParser.TimOutFileName.c_str() );
    }

    // copy tim in to tim out
    ifsTimInFile.seekg(0, ios::end);
    // note that the (unsigned int) cast is dangerous for very large files but should not 
    // be an issue here
    unsigned int size = (unsigned int)ifsTimInFile.tellg();
    ifsTimInFile.seekg(0, ios::beg);
    
    char* buffer = new char[ size+1 ];
    memset( buffer, 0, size+1 );
    ifsTimInFile.read( buffer, size );
    fsTimOutFile.write( buffer, size );
    delete [] buffer;

    // append ds to time out
    InstallDigitalSignature (fsTimOutFile);

    ifsTimInFile.close();
    fsTimOutFile.close();

    return TRUE;
}

bool CTrustedImageBuilder::ProcessKeyInfo (fstream& fsTimBinFile)
{
    if ( (int)TimDescriptorParser.TimDescriptor().TimHeader().NumKeys != TimDescriptorParser.KeyInfoList.size() )
        return FALSE;

    t_KeyInfoListIter Iter = TimDescriptorParser.KeyInfoList.begin();
    KEY_MOD_3_4_0* pKeyInfo_3_4 = 0;
    while ( Iter != TimDescriptorParser.KeyInfoList.end() )
    {
        pKeyInfo_3_4 = *Iter;
        if (GenerateKeyHash (pKeyInfo_3_4) == FALSE)
        {
            printf ("  Error: Generating the Key Hash has failed!\n");
            return FALSE;
        }

        if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version < TIM_3_3_00 )
        {
            KEY_MOD_3_1_0 KeyInfo;
            memset( &KeyInfo, 0, sizeof(KEY_MOD_3_1_0) );
            KeyInfo.KeyID = pKeyInfo_3_4->KeyID;
            KeyInfo.HashAlgorithmID = pKeyInfo_3_4->HashAlgorithmID;
            KeyInfo.KeySize = pKeyInfo_3_4->KeySize/8;
            KeyInfo.PublicKeySize = pKeyInfo_3_4->PublicKeySize/8;
            memcpy( &KeyInfo.RSAPublicExponent, &pKeyInfo_3_4->Rsa.RSAPublicExponent, KeyInfo.PublicKeySize );
            memcpy( &KeyInfo.RSAModulus, &pKeyInfo_3_4->Rsa.RSAModulus, KeyInfo.KeySize );
            memcpy( &KeyInfo.KeyHash, &pKeyInfo_3_4->KeyHash, sizeof(KeyInfo.KeyHash) );
            fsTimBinFile.write( (char*)&KeyInfo, sizeof(KEY_MOD_3_1_0) );
        }
        else if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version == TIM_3_3_00 )
        {
            KEY_MOD_3_3_0 KeyInfo;
            memset( &KeyInfo, 0, sizeof(KEY_MOD_3_3_0) );
            KeyInfo.KeyID = pKeyInfo_3_4->KeyID;
            KeyInfo.HashAlgorithmID = pKeyInfo_3_4->HashAlgorithmID;
            KeyInfo.KeySize = pKeyInfo_3_4->KeySize;
            KeyInfo.PublicKeySize = pKeyInfo_3_4->PublicKeySize;
            KeyInfo.EncryptAlgorithmID = pKeyInfo_3_4->EncryptAlgorithmID;
            if ( KeyInfo.EncryptAlgorithmID == ECDSA_256 || KeyInfo.EncryptAlgorithmID == ECDSA_521 )
            {
                memcpy( &KeyInfo.Ecdsa.PublicKeyCompX, &pKeyInfo_3_4->Ecdsa.PublicKeyCompX, KeyInfo.KeySize/8 );
                memcpy( &KeyInfo.Ecdsa.PublicKeyCompY, &pKeyInfo_3_4->Ecdsa.PublicKeyCompY, KeyInfo.PublicKeySize/8 );
            }
            else
            {
                memcpy( &KeyInfo.Rsa.RSAModulus, &pKeyInfo_3_4->Rsa.RSAModulus, KeyInfo.KeySize/8 );
                memcpy( &KeyInfo.Rsa.RSAPublicExponent, &pKeyInfo_3_4->Rsa.RSAPublicExponent, KeyInfo.PublicKeySize/8 );
            }
            memcpy( &KeyInfo.KeyHash, &pKeyInfo_3_4->KeyHash, sizeof(KeyInfo.KeyHash) );
            fsTimBinFile.write( (char*)&KeyInfo, sizeof (KEY_MOD_3_3_0) );
        }
        else if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version == TIM_3_4_00 )
        {
            fsTimBinFile.write( (char*)pKeyInfo_3_4, sizeof(KEY_MOD_3_4_0) );
        }

        Iter++;
    }

    return TRUE;
}

bool CTrustedImageBuilder::ProcessDsaInfo (fstream& fsTimBinFile)
{
    unsigned int HashBuffer[16] = {0};
    unsigned int KeyBuffer[(MAXRSAKEYSIZEWORDS+1) * sizeof(Ipp32u)] = {0};
    unsigned int i = 0; 
    unsigned int j = 0;
    unsigned int KeyLength = 0;
    unsigned int SHAHashSize = 0;
    unsigned int scheme = TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version >= TIM_3_4_00 ? 1 : 0;

    PLAT_DS Temp_Ds;
    memcpy( &Temp_Ds, &(TimDescriptorParser.Ds), sizeof(PLAT_DS) );

    if (Temp_Ds.DSAlgorithmID == PKCS1_v1_5_Caddo)
        EnableEndianConvert = true;

    // PKCS_1.5: The data read from the descriptor file is in LSW, Little Endian format. 
    // Endian conversions and word swaps are required for PKCS 1.5 in order to make the
    // data compatible with the Fast signature PI.
    if (Temp_Ds.DSAlgorithmID == PKCS1_v1_5_Caddo || Temp_Ds.DSAlgorithmID == PKCS1_v1_5_Ippcp )
    {
        // PKCS_1.5: Store data in a temporary structure just for PKCS. This structure will  
        // be written into TIM binary.
        KeyLength = Temp_Ds.KeySize / 32;

        if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version < TIM_3_4_00 )
        {
            if ( EnableEndianConvert )
            {
                // PKCS_1.5:Convert Public Exponent, the modulus from LSW to MSW and to Big Endian.
                // BE format is required for all ippcp functions.
                for (i = 0, j = KeyLength - 1; i < KeyLength; i++, j--)
                {
                    KeyBuffer[ j ] = Temp_Ds.Rsa.RSAPublicExponent[ i ];
                    Endian_Convert(*(unsigned int *)&KeyBuffer[ j ], (unsigned int *)&KeyBuffer[ j ]);

                    KeyBuffer[ j + KeyLength ] = Temp_Ds.Rsa.RSAModulus[ i ];
                    Endian_Convert(*(unsigned int *)&KeyBuffer[ j + KeyLength ], (unsigned int *)&KeyBuffer[ j + KeyLength ]);
                }
            }
            else
            {
                for (i = 0; i <(int)KeyLength; i++ )
                {
                    KeyBuffer[ i ] = Temp_Ds.Rsa.RSAPublicExponent[ i ];
                    KeyBuffer[ i + KeyLength ] = Temp_Ds.Rsa.RSAModulus[ i ];
                }
            }
        }
        else if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version >= TIM_3_4_00 )
        {
            // >= TIM_3_4_00
            // add scheme id to KeyBuffer if needed
            if( Temp_Ds.HashAlgorithmID == SHA256 )
                KeyBuffer[0] = (KeyLength == 64) ? PKCSv1_SHA256_2048RSA : PKCSv1_SHA256_1024RSA;
            else  // Defaults to 160
                KeyBuffer[0] = (KeyLength == 64) ? PKCSv1_SHA1_2048RSA : PKCSv1_SHA1_1024RSA;

            // Fill in the modulus
            // Fill in the Exponent
            // Note: modulus before exponent in post 3_4
            for (i = 0; i < KeyLength; i++ )
            {
                KeyBuffer[ i + scheme ] = Temp_Ds.Rsa.RSAModulus[i];
                KeyBuffer[ i + KeyLength + scheme ] = Temp_Ds.Rsa.RSAPublicExponent[i];
            }
        }
    }		 
    else if (Temp_Ds.DSAlgorithmID == ECDSA_256 || Temp_Ds.DSAlgorithmID == ECDSA_521 )
    {
        KeyLength = (Temp_Ds.KeySize + 31) / 32;

        // add scheme id to KeyBuffer if needed
        if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version >= TIM_3_4_00 )
        {
            if( Temp_Ds.HashAlgorithmID == SHA256 )
                KeyBuffer[0] = (KeyLength == 17) ? ECCP521_FIPS_DSA_SHA256 : ECCP256_FIPS_DSA_SHA256;
            else  // Defaults to 160
                KeyBuffer[0] = (KeyLength == 17) ?  ECCP521_FIPS_DSA_SHA1 : ECCP256_FIPS_DSA_SHA1;
        }

        for (i = 0; i <(int)KeyLength; i++ )
        {
            KeyBuffer[ i + scheme ] = Temp_Ds.Ecdsa.ECDSAPublicKeyCompX[i];				
            KeyBuffer[ i + KeyLength + scheme ] = Temp_Ds.Ecdsa.ECDSAPublicKeyCompY[i];
        }
    }

    // Compute SHA hash.
    if (Temp_Ds.HashAlgorithmID == SHA256 )
    {
        ippsSHA256MessageDigest ((Ipp8u *)KeyBuffer, ((KeyLength*2) + scheme)*4, (Ipp8u *)HashBuffer);
        SHAHashSize = 8;
    }
    else
    {
        ippsSHA1MessageDigest ((Ipp8u *)KeyBuffer, ((KeyLength*2) + scheme)*4, (Ipp8u *)HashBuffer);
        SHAHashSize = 5;
    }		
        
    for (i = 0; i < SHAHashSize; i++)
        Temp_Ds.Hash[i] = (UINT_T)HashBuffer[i];

    OutputOtpHash (&TimDescriptorParser.TimDescriptor().TimHeader(),&Temp_Ds);

    //PKCS_1.5:Endian convert the hash and store it in the temp struct.
    if ( EnableEndianConvert )
    {
        for (i = 0; i < SHAHashSize; i++)
        {			   
            Endian_Convert (HashBuffer[i], &HashBuffer[i]);		
            Temp_Ds.Hash[i] = (UINT_T)HashBuffer[i];
        }

        // PKCS_1.5:Store Public exponent in temp struct
        // PKCS_1.5:Store Modulus in temp struct
        for (i = 0; i <KeyLength; i++)
        {
            Temp_Ds.Rsa.RSAPublicExponent[i] = (UINT_T)KeyBuffer[ i ];
            Endian_Convert(*(unsigned int *)&Temp_Ds.Rsa.RSAPublicExponent[ i ], (unsigned int *)&Temp_Ds.Rsa.RSAPublicExponent[ i ] );
            Temp_Ds.Rsa.RSAModulus[i] = (UINT_T)KeyBuffer[ i + KeyLength ];
            Endian_Convert(*(unsigned int *)&Temp_Ds.Rsa.RSAModulus[ i ], (unsigned int *)&Temp_Ds.Rsa.RSAModulus[ i ] );
        }
    }

    if ( Temp_Ds.DSAlgorithmID == ECDSA_256 || Temp_Ds.DSAlgorithmID == ECDSA_521 )
    {
        fsTimBinFile.write( (char*)&Temp_Ds, sizeof (PLAT_DS) - (sizeof (UINT_T) * ((MAXRSAKEYSIZEWORDS*3)-(MAXECCKEYSIZEWORDS*2))));
    }
    else if (Temp_Ds.DSAlgorithmID == PKCS1_v1_5_Caddo )
    {
        // for older tim, keysize is in bytes so convert it
        if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version < TIM_3_3_00 )
            Temp_Ds.KeySize /= 8;
        
        std::streamoff lLoc = fsTimBinFile.tellg();
        fsTimBinFile.write( (char*)&Temp_Ds, (sizeof (PLAT_DS) - (sizeof (UINT_T) * MAXRSAKEYSIZEWORDS) ));
    }
    else
    {
        // for older tim, keysize is in bytes so convert it
        if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version < TIM_3_3_00 )
            Temp_Ds.KeySize /= 8;
        
        fsTimBinFile.write( (char*)&Temp_Ds, (sizeof (PLAT_DS) - (sizeof (UINT_T) * MAXRSAKEYSIZEWORDS) ));
    }

    return fsTimBinFile.good();
}

bool CTrustedImageBuilder::GenerateHash (ifstream& ifsImage, CImageDescription& ImageDesc)
{
    if ( !TimDescriptorParser.TimDescriptor().Trusted() )
        return true;

    int BlockNo = 0;
    int iSize = 0;
    int Rem = 0;
    int i = 0;
    Ipp8u Src[8192] = {0};
    IppStatus Status = ippStsNoErr;
    IppsSHA1State *pState = NULL;
    IppsSHA256State *pState256 = NULL;
    IppsSHA512State *pState512 = NULL;
    PLAT_DS& Ds = TimDescriptorParser.Ds;
    unsigned int SHAHashSize = 0;

    // A hash is generated from each image in its entirety and stored in the
    // Hash array of the IMAGE_INFO struct for that image in the TIM.
//	if (Ds.HashAlgorithmID == PKCS1_v1_5_Caddo) //this code is bogus and fixed on 9/10/2010 RAR
    if ( Ds.DSAlgorithmID == PKCS1_v1_5_Caddo )
        EnableEndianConvert = true;
    
    ifsImage.clear();
    ifsImage.seekg( 0, ios_base::beg );

    BlockNo = ImageDesc.ImageSizeToHash() / 8192;
    Rem = ImageDesc.ImageSizeToHash() % 8192;

    // SHA Initialization
    int HashAlgorithmId = 0;
    if ( ImageDesc.HashAlgorithmId() == "SHA-160" )
        HashAlgorithmId = SHA160;
    else if ( ImageDesc.HashAlgorithmId() == "SHA-256" )
        HashAlgorithmId = SHA256;
    else if ( ImageDesc.HashAlgorithmId() == "SHA-512" )
        HashAlgorithmId = SHA512;

    if (HashAlgorithmId == SHA512 ) 
    {
        Status = ippsSHA512GetSize (&iSize);
        pState512 = (IppsSHA512State *) malloc (iSize);
        if (pState512 == NULL)
        {
            printf ("////////////////////////////////////////////////////////////\n");
            printf ("  Error: Memory allocation for pState512 failed in GenerateHash()!\n");
            printf ("////////////////////////////////////////////////////////////\n");
            return FALSE;
        }
        memset( pState512, 0 , iSize );
        Status = ippsSHA512Init (pState512);
        SHAHashSize = 16;
    } 
    else if (HashAlgorithmId == SHA256 ) 
    {
        Status = ippsSHA256GetSize (&iSize);
        pState256 = (IppsSHA256State *) malloc (iSize);
        if (pState256 == NULL)
        {
            printf ("////////////////////////////////////////////////////////////\n");
            printf ("  Error: Memory allocation for pState256 failed in GenerateHash()!\n");
            printf ("////////////////////////////////////////////////////////////\n");
            return FALSE;
        }
        memset( pState256, 0 , iSize );
        Status = ippsSHA256Init (pState256);
        SHAHashSize = 8;
    } 
    else 
    {
        Status = ippsSHA1GetSize (&iSize);
        pState = (IppsSHA1State *) malloc (iSize);
        if (pState == NULL)
        {
            printf ("////////////////////////////////////////////////////////////\n");
            printf ("  Error: Memory allocation for pState failed in GenerateHash()!\n");
            printf ("////////////////////////////////////////////////////////////\n");
            return FALSE;
        }
        memset( pState, 0 , iSize );
        Status = ippsSHA1Init (pState);
        SHAHashSize = 5;
    }
    
    if (Status != ippStsNoErr)
    {
        if (pState != NULL) free (pState);
        if (pState256 != NULL) free (pState256);
        if (pState512 != NULL) free (pState512);
        printf ("  Error: Failed generating hash data for file: %s\n",ImageDesc.ImageFilePath().c_str());
        return FALSE;
    }

    // SHA Update
    if (HashAlgorithmId == SHA512 ) 
    {
        for (i = 0; i < BlockNo; i++) 
        {
            ifsImage.read( (char*)Src, (sizeof(char)*8192) );
            // note that the (int) cast is dangerous for very large files but should not 
            // be an issue here
            iSize = (int)ifsImage.gcount();
            ippsSHA512Update (Src,iSize,pState512);
        }
        ifsImage.read( (char*)Src, sizeof(char)*Rem );
        // note that the (int) cast is dangerous for very large files but should not 
        // be an issue here
        iSize = (int)ifsImage.gcount();
        Status = ippsSHA512Update (Src,iSize,pState512);
    } 
    else if (HashAlgorithmId == SHA256 ) 
    {
        for (i = 0; i < BlockNo; i++) 
        {
            ifsImage.read( (char*)Src, (sizeof(char)*8192) );
            // note that the (int) cast is dangerous for very large files but should not be an issue here
            iSize = (int)ifsImage.gcount();
            ippsSHA256Update (Src,iSize,pState256);
        }
        ifsImage.read( (char*)Src, sizeof(char)*Rem );
        // note that the (int) cast is dangerous for very large files but should not be an issue here
        iSize = (int)ifsImage.gcount();
        Status = ippsSHA256Update (Src,iSize,pState256);
    } 
    else 
    {
        for (i = 0; i < BlockNo; i++) 
        {
            unsigned int state = ifsImage.rdstate();
            ifsImage.read((char*)Src, (sizeof(char)*8192) );
            // note that the (int) cast is dangerous for very large files but should not be an issue here
            iSize = (int)ifsImage.gcount();
            if ( EnableEndianConvert )
            {
                for(int q=0; q<iSize;q+=4)
                    Endian_Convert(*(unsigned int *)&Src[q], (unsigned int *)&Src[q]);
            }
            ippsSHA1Update (Src,iSize,pState);
        }

        ifsImage.read( (char*)Src, sizeof(char)*Rem );
        // note that the (int) cast is dangerous for very large files but should not be an issue here
        iSize = (int)ifsImage.gcount();
        // determine if Rem size is word aligned
        unsigned int pad = sizeof(UINT_T)-(iSize % sizeof(UINT_T));
        if ( EnableEndianConvert )
        {
            for(int q=0; q<iSize;q+=4)
            {
                Endian_Convert(*(unsigned int *)&Src[q], (unsigned int *)&Src[q]);
                // if the last word does not use all bytes, need to shift bytes accordingly
                if ( q+4 > iSize )
                {
                    *(unsigned int*)&Src[q] >>= pad*8; // shift right to align data
                }
            }
        }
        Status = ippsSHA1Update (Src,iSize,pState);
    }

    if (Status != ippStsNoErr)
    {
        if (pState != NULL) free (pState);
        if (pState256 != NULL) free (pState256);
        if (pState512 != NULL) free (pState512);
        printf ("  Error: Failed generating hash data for file: %s\n",ImageDesc.ImageFilePath().c_str());
        return FALSE;
    }

    // SHA Final
    if (HashAlgorithmId == SHA512) 
    {
        Status = ippsSHA512Final ((Ipp8u *)ImageDesc.Hash,pState512);
        free (pState512);
    } 
    else if (HashAlgorithmId == SHA256) 
    {
        Status = ippsSHA256Final ((Ipp8u *)ImageDesc.Hash,pState256);
        free (pState256);
    } 
    else 
    {
        Status = ippsSHA1Final ((Ipp8u *)ImageDesc.Hash,pState);	
        free (pState);
    }

    if (Status != ippStsNoErr)
    {
        printf ("  Error: Failed generating hash data for file: %s\n",ImageDesc.ImageFilePath().c_str());
        return FALSE;
    }

    if ( EnableEndianConvert )
    {
        for (unsigned int i = 0; i < SHAHashSize; i++)
            Endian_Convert (((UINT_T *)ImageDesc.Hash)[i],&(((UINT_T *)ImageDesc.Hash)[i]));
    }

    printf( "\n" );
#if WTPDEBUG
    DumpHashToFile (ImageDesc);
#endif

    return TRUE;
}

bool CTrustedImageBuilder::DumpHashToFile (CImageDescription& ImageDesc)
{
    string sHashFileName;
    ofstream ofsHashFile;
    string sHashValue;

    sHashFileName = ImageDesc.ImageFilePath();

    if (sHashFileName.find_last_of(".") == string::npos )
    {
        sHashFileName = "_hash.txt";
    }
    else
    {
        sHashFileName.resize(sHashFileName.find_last_of("."));
        sHashFileName += "_hash.bin";
    }

    ofsHashFile.open(sHashFileName.c_str(), ios_base::out | ios_base::trunc );
    if ( ofsHashFile.bad() || ofsHashFile.fail() )
    {
        printf ("\n  Error: fopen failed to open hash file: %s.\n",sHashFileName.c_str());
        return FALSE;
    }

    int SHAHashSize = 0;
    if ( ImageDesc.HashAlgorithmId() == "SHA-160" )
        SHAHashSize = 5;
    else if ( ImageDesc.HashAlgorithmId() == "SHA-256" )
        SHAHashSize = 8;
    else if ( ImageDesc.HashAlgorithmId() == "SHA-512" )
        SHAHashSize = 16;

    for (int i = 0; i < SHAHashSize; i++)
    {
        sHashValue = HexFormattedAscii( ImageDesc.Hash[i] );
        sHashValue += "\n";
        ofsHashFile.write( sHashValue.c_str(), (int)sHashValue.length() );
    }

    ofsHashFile.close();

    return TRUE;
}

bool CTrustedImageBuilder::GenerateKeyHash (KEY_MOD_3_4_0* key)
{
    unsigned int i = 0;
    unsigned int KeyLength = 0;
    unsigned int SHAHashSize = 0;
    IppStatus ippstatus = ippStsNoErr;
    unsigned int KeyData[((2048*2)+32)/sizeof(unsigned int)] = {0}; //Max 2048 bit key * 2  (for exponent and modulus)
                                       // +32 is for schemeID in TIM_3_4_00
    int scheme = TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version >= TIM_3_4_00 ? 1 : 0;

    if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version < TIM_3_3_00 )
    {
        // old code depended on DS algorithm because key did not have an EncryptAlgorithmID
        PLAT_DS& Ds = TimDescriptorParser.Ds;
        key->EncryptAlgorithmID = Ds.DSAlgorithmID;
    }

    if ( key->EncryptAlgorithmID == PKCS1_v1_5_Caddo )
        EnableEndianConvert = true;

    // PKCS_1.5: The data read from the descriptor file is in LSW, Little Endian format. 
    // Endian conversions and word swaps are required for PKCS 1.5 in order to make the
    // data compatible with the Fast signature PI.
    if ( key->EncryptAlgorithmID == PKCS1_v1_5_Caddo || key->EncryptAlgorithmID == PKCS1_v1_5_Ippcp )
    {
        // PKCS_1.5: Store data in a temporary structure just for PKCS. This structure will  
        // be written into TIM binary.
        KeyLength = key->KeySize / 32; 

        if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version < TIM_3_4_00 )
        {        
            if ( EnableEndianConvert )
            {
                // Fill in the Exponent
                // Fill in the modulus
                for (i = 0; i < KeyLength; i++) 
                {
                    Endian_Convert( key->Rsa.RSAPublicExponent[i], &KeyData[ i ] );
                    Endian_Convert( key->Rsa.RSAModulus[i], &KeyData[ KeyLength + i ] );
                }
            }
            else
            {
                // Fill in the Exponent
                // Fill in the modulus
                for (i = 0; i < KeyLength; i++) 
                {
                    KeyData[ i ] = key->Rsa.RSAPublicExponent[i];
                    KeyData[ KeyLength + i ] = key->Rsa.RSAModulus[i];
                }
            }
        } else if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version >= TIM_3_4_00 )
        {
            // >= TIM_3_4_00
            // add scheme id to KeyBuffer if needed
            if( key->HashAlgorithmID == SHA256 )
                KeyData[0] = (KeyLength == 64) ? PKCSv1_SHA256_2048RSA : PKCSv1_SHA256_1024RSA;
            else  // Defaults to 160
                KeyData[0] = (KeyLength == 64) ? PKCSv1_SHA1_2048RSA : PKCSv1_SHA1_1024RSA;

            // Fill in the modulus
            // Fill in the Exponent
            // Note: modulus before exponent in post 3_4
            for (i = 0; i < KeyLength; i++) 
            {
                KeyData[ i + scheme ] = key->Rsa.RSAModulus[i];
                KeyData[ KeyLength + i + scheme ] = key->Rsa.RSAPublicExponent[i];
            }
        }
    }
    else if ( key->EncryptAlgorithmID == ECDSA_256 || key->EncryptAlgorithmID == ECDSA_521 )
    {
        KeyLength = ( key->KeySize + 31) / 32;

        // add scheme id to KeyBuffer if needed
        if ( TimDescriptorParser.TimDescriptor().TimHeader().VersionBind.Version >= TIM_3_4_00 )
        {
            if( key->HashAlgorithmID == SHA256 )
                KeyData[0] = (KeyLength == 17) ? ECCP521_FIPS_DSA_SHA256 : ECCP256_FIPS_DSA_SHA256;
            else  // Defaults to 160
                KeyData[0] = (KeyLength == 17) ?  ECCP521_FIPS_DSA_SHA1 : ECCP256_FIPS_DSA_SHA1;
        }

        for (i = 0; i <(int)KeyLength; i++ )
        {
            KeyData[ i + scheme ] = key->Ecdsa.PublicKeyCompX[i];				
            KeyData[ i + KeyLength + scheme ] = key->Ecdsa.PublicKeyCompY[i];
        }
    }

    if ( key->HashAlgorithmID == SHA256 ) 
    {
        ippstatus = ippsSHA256MessageDigest((Ipp8u *)KeyData, ((KeyLength*2) + scheme)*4, (Ipp8u *)key->KeyHash);
        SHAHashSize = 8;
    } 
    else 
    {
        ippstatus = ippsSHA1MessageDigest((Ipp8u *)KeyData, ((KeyLength*2) + scheme)*4, (Ipp8u *)key->KeyHash);
        SHAHashSize = 5;
    }

    // Do an endian conversion 1 word (4 bytes) at a time
    if ( EnableEndianConvert )
    {
        for ( i = 0; i < SHAHashSize; i++ )
            Endian_Convert( *(UINT_T *)(&key->KeyHash[i]), (UINT_T *)(&key->KeyHash[i]) );
    }

    string sKeyName = HexAsciiToText(HexFormattedAscii(key->KeyID));
    string sKeyFileName = sKeyName + "_KeyHash.txt";

    ofstream ofsKeyHash;
    stringstream ss;
//	ss.setf( ios::internal, ios::adjustfield );
    string value;
    ofsKeyHash.open(sKeyFileName.c_str(), ios_base::out | ios_base::trunc );
    for( i=0; i < SHAHashSize; i++ )
    {
        ss << HexFormattedAscii( key->KeyHash[i] ) << endl; 
    }
    ofsKeyHash << ss.str();
    ofsKeyHash.flush();
    ofsKeyHash.close();
    
    if (ippstatus != ippStsNoErr) return FALSE;

    return TRUE;
}

int CTrustedImageBuilder::GetTimHashSize (CTIM *pTimHeader, PLAT_DS& Ds) 
{
    int iTotalHashSize = TimDescriptorParser.TimDescriptor().GetTimImageSize( CommandLineParser.bOneNANDPadding, CommandLineParser.uiPaddedSize );

    // eliminate the modulus when calculating the tim hash
    int iExpHashSize = 0;
    if ( Ds.DSAlgorithmID == ECDSA_256 || Ds.DSAlgorithmID == ECDSA_521 )
        iExpHashSize += (sizeof (UINT_T) * ((MAXRSAKEYSIZEWORDS*3)-(MAXECCKEYSIZEWORDS*2)));
    else
        iExpHashSize += (sizeof (UINT_T) * (MAXRSAKEYSIZEWORDS));

    iTotalHashSize -= iExpHashSize;

//	if ( iTotalHashSize > MAX_TIM_SIZE )
//	{
//		printf ("////////////////////////////////////////////////////////////\n");
//		printf ("  Error: Total HashSize exceeds 4096 bytes!\n");
//		printf ("////////////////////////////////////////////////////////////\n");
//	}

    return iTotalHashSize;
}

bool CTrustedImageBuilder::GenerateTimHash (fstream& fsTimBinFile, CTIM *pTimHeader, Ipp8u *pDsHash, unsigned int TimSizeToHash)
{
// A hash of the TIM minus the digital signature is generated by 
// this function.
    Ipp8u TimHash[32] = {0};
    Ipp8u PaddedTimHash[MAX_HASH] = {0};
    Ipp8u PaddedTimHash_WordSwapped[MAX_HASH] = {0};
    Ipp8u DER_SHA1_T[] = {0x30, 0x21, 0x30, 0x09, 0x06, 0x05, 0x2B, 0x0E, 0x03, 0x02, 0x1A, 0x05, 0x00, 0x04, 0x14};
    Ipp8u DER_SHA256_T[] = {0x30, 0x31, 0x30, 0x0D, 0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x01, 0x05, 0x00, 0x04, 0x20};
    Ipp8u *DER_T = NULL;
    long lFileSize = 0;
    unsigned char *TimBuffer = NULL;
    ofstream ofsTimHash;
    unsigned int i = 0;
    unsigned int j = 0;
    unsigned int paddingsize = 0;
    unsigned long TimSizeWithNoDS = 0;
    unsigned int SHAHashSize = 0; //size of digest in words
    unsigned int sizeofT = 0;
    string sHashValue;

    PLAT_DS& Ds = TimDescriptorParser.Ds;

    if ( Ds.DSAlgorithmID == PKCS1_v1_5_Caddo)
        EnableEndianConvert = true;

    //get the max size of the TIM to be hashed
    TimSizeWithNoDS = GetTimHashSize (pTimHeader, Ds);
    
    fsTimBinFile.seekg( 0, ios_base::end );  // Set position to EOF
    fsTimBinFile.clear();

    //determine how much of the TIM needs to be read into buffer
    // note that the (long) cast is dangerous for very large files but should not be an issue here
    lFileSize = (long)fsTimBinFile.tellg();

    // determine if hash size is less than the complete file size and adjust lFileSize
    if ( (TimSizeToHash != TimSizeWithNoDS) && ((long)TimSizeToHash < lFileSize) )
        lFileSize = TimSizeToHash;

#if DBGVERIFY
    printf("\nTim size to hash read from file: %d\n", lFileSize);
#endif

    fsTimBinFile.seekg( 0, ios_base::beg );  // Set position SOF

    // allocate a buffer large enough to include the file data + padding and is a size that is word aligned
    unsigned int buffersize = max( lFileSize, (long)TimSizeToHash );
    unsigned int pad = sizeof(UINT_T)-(buffersize % sizeof(UINT_T));
    // if pad is sizeof(UINT_T), then no pad needed
    unsigned int allocSize = ((pad == sizeof(UINT_T)) ? buffersize : buffersize + pad);
    if (NULL == (TimBuffer = (unsigned char *)malloc ( allocSize ) ) ) 
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: Memory allocation for TimBuffer failed in GenerateTimHash()!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return FALSE;
    }
    // pad tim is necessary
    if ( CommandLineParser.bOneNANDPadding )
        memset( TimBuffer, 0xff , allocSize );
    else
        memset( TimBuffer, 0x0 , allocSize );

    //read in TIM file
    fsTimBinFile.read( (char*)TimBuffer, lFileSize );
    if ( fsTimBinFile.bad() || fsTimBinFile.fail() )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: GenerateTimHash() couldn't read all of the %ld bytes\n",lFileSize);
        printf ("from the TIM binary image!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        if (TimBuffer != NULL) free (TimBuffer);
        return FALSE;
    }

    //endian convert TIM to big endian for IPP reasons(LSW->MSW)
    if ( EnableEndianConvert )
    {
        for(unsigned int c=0; c < TimSizeToHash; c+=4)
        {
            Endian_Convert(*(unsigned int *)&TimBuffer[c], (unsigned int *)&TimBuffer[c]);
            // if the last word does not use all bytes, need to shift bytes accordingly
            if ( c+4 > TimSizeToHash )
            {
                *(unsigned int*)&TimBuffer[c] >>= pad*8; // shift right all align data
            }
        }
    }

    memset (PaddedTimHash,0,sizeof(PaddedTimHash));//clear the hash buffers
    memset (TimHash,0,sizeof(TimHash));//clear the hash buffers

#if DBGVERIFY
    stringstream ss;
    string value;
    ofsTimHash.open("Tim_msg.txt", ios_base::out | ios_base::trunc );
    for( unsigned int i=0; i < TimSizeToHash; i++ )
    {
        if (i!=0 && i % 4 == 0)
            ss << endl;
        ss << hex << setw(2) << setfill('0') << (int)TimBuffer[i]; 
    }
    ofsTimHash << ss.str();
    ofsTimHash.flush();
    ofsTimHash.close();
#endif

    //Calculate the SHA hash of the TIM file
    if (Ds.HashAlgorithmID == SHA256)
    {
        ippsSHA256MessageDigest(TimBuffer,TimSizeToHash,TimHash);
        SHAHashSize = 8;
        sizeofT = 19;
        DER_T = DER_SHA256_T;

        IppsBigNumState *pDstSHA256_BN = New_BN( SHAHashSize );	
        ippsSetOctString_BN( TimHash, 256>>3, pDstSHA256_BN );
    }
    else
    {
        ippsSHA1MessageDigest(TimBuffer,TimSizeToHash,TimHash);
        SHAHashSize = 5; 
        sizeofT = 15;
        DER_T = DER_SHA1_T;

        IppsBigNumState *pDstSHA1_BN = New_BN( SHAHashSize );	
        ippsSetOctString_BN( TimHash, 160>>3, pDstSHA1_BN );
    
//		cout << endl << endl;
//		cout <<"TIM Hash input message size =" << lFileSize << endl;
//		cout <<"TIM Hash of the input message is:" << endl;
//		Type_BN(0, pDstSHA1_BN); cout << endl << endl;
    }

    if (TimBuffer != NULL) free (TimBuffer);
    
    if (Ds.DSAlgorithmID == Marvell_DS )
    {
        //endian convert hash back to little endian to satisfy Caddo
        for (i = 0; i < SHAHashSize; i++)
            Endian_Convert (((UINT_T *)TimHash)[i],&((UINT_T *)TimHash)[i]);

        //add TIM hash to the padded TIM hash buffer
        for (i = 0; i < SHAHashSize*4; i++)
            PaddedTimHash[i] = TimHash[i];

        //now add the padding
        for (i = SHAHashSize; i < ((Ds.KeySize / 32) - 1); i++)
            ((Ipp32u *)PaddedTimHash)[i] = 0xBEBEBEBE;

        ((Ipp32u *)PaddedTimHash)[i] = 0x6EBEBEBE;
    }
    else if ( Ds.DSAlgorithmID == PKCS1_v1_5_Caddo || Ds.DSAlgorithmID == PKCS1_v1_5_Ippcp )
    {		
        // Encryption block(EB) formatting for PKCS
        //=================================
        // EB = 00 || BT || PS || 00 || T || D
        //==================================
        // BT = 00, 01(private key operation) or 02(public key operation)
        // PS = 00, FF, randomly generated(based on BT values)
        // T = DER encoding of DigestInfo
        // D = SHA hash
        // All values are in octets (octet means byte)

        //PS must be k-3-|T|-|D| bytes long
        paddingsize = (Ds.KeySize/8) - 3 - sizeofT - (SHAHashSize * 4);

        PaddedTimHash[0] = 0x00; //byte 0 is always 0x0
        PaddedTimHash[1] = 0x01;//private key op, with Padding = 0xFF
        for (j = 0, i = 2; j < paddingsize; j++, i++)
            PaddedTimHash[i] = 0xFF;

        PaddedTimHash[i++] = 0x00;

        for(j = 0; j < sizeofT; j++, i++)
            PaddedTimHash[i] = DER_T[j];

        //now add Tim Hash to the Padded hash
        for (j = 0; j < SHAHashSize * 4; j++, i++)
            PaddedTimHash[i] = TimHash[j];

        for (i = 0; i < Ds.KeySize/8; i++)
            ((Ipp8u *)PaddedTimHash_WordSwapped)[i] = ((Ipp8u *)PaddedTimHash)[(Ds.KeySize/8) - 1 - i];

        for (i = 0; i < Ds.KeySize/8; i++)
            PaddedTimHash[i] = PaddedTimHash_WordSwapped[i];
    }
    else if ( Ds.DSAlgorithmID == ECDSA_256 || Ds.DSAlgorithmID == ECDSA_521 )
    {
        //add TIM hash to the padded TIM hash buffer
        for (i = 0; i < SHAHashSize*4; i++)
            PaddedTimHash[i] = TimHash[i];
    }
    else
    {
       printf("  Error: DSA Algorithm needs to be Marvell method or PKCS!\n\n");
       return FALSE;
    }
    
#if 1 //DBGVERIFY
    ofsTimHash.open("TimHash.txt", ios_base::out | ios_base::trunc );
    if ( ofsTimHash.bad() || ofsTimHash.fail() )
        printf ("\n  Error: fopen failed to TimHash.txt\n");

    if (CommandLineParser.bVerbose)
        printf ("\nTIM Hash prior to Encryption for size (in bits) %d\n", SHAHashSize*32);

    stringstream ss1;
//	ss1.setf( ios::internal, ios::adjustfield );

    for (int i = 0; (unsigned int)i < SHAHashSize; i++)
    {
        sHashValue = HexFormattedAscii(((Ipp32u *) PaddedTimHash)[i]);
//		sHashValue += " ";

//		if ( i!=0 && (i % 4 == 0) )
//		{
//			ss1 << endl;
//			if (CommandLineParser.bVerbose) 
//				cout << endl;
//		}

        if (CommandLineParser.bVerbose) 
            cout << sHashValue.c_str() << endl;

        ss1 << sHashValue << endl;
    }

    ss1 << endl;

    if (CommandLineParser.bVerbose) 
        cout << endl;

    ofsTimHash << ss1.str();
    ofsTimHash.flush();
    ofsTimHash.close();
#endif

    memcpy (pDsHash,PaddedTimHash,sizeof(PaddedTimHash));

    return TRUE;
}

bool CTrustedImageBuilder::DumpTimHashToBinFile (PLAT_DS *pDs,Ipp8u *pDsHash, char *HashFileName)
{
    ofstream ofsTimHash;
    Ipp8u DsHash[MAX_HASH_IN_BITS/8] = {0};

    memcpy (DsHash,pDsHash,sizeof(DsHash));

    ofsTimHash.open( HashFileName, ios_base::out | ios_base::trunc | ios_base::binary );
    if ( ofsTimHash.bad() || ofsTimHash.fail() )
    {
        printf ("\n  Error: fopen failed to TimHash.bin\n");
        return FALSE;
    }

    ofsTimHash.write( (char*)DsHash, (sizeof (Ipp8u) * pDs->KeySize/8) );

    ofsTimHash.close();

    return TRUE;
}

bool CTrustedImageBuilder::EncryptSignatureRSA( bool encrypt, PLAT_DS *pDs, Ipp8u *pDsHash )
{
    int kn = 4096, kp = 2048, size = 0, length = 128;
    IppsRSAState *key = NULL;
    IppsBigNumState *x = NULL, *y = NULL;
    IppsBigNumSGN sgn = IppsBigNumNEG;
    IppStatus Status = ippStsNoErr;
    Ipp8u DsHash[MAX_HASH] = {0};
    ofstream ofsEncrypted;

    // This function encrypts the hash of the entire TIM minus installation
    // of the digital signature. Information from the DSA algorithm module, as
    // defined in the descriptor text file, is also utilized. The starting point
    // in the descriptor would look something like this, ( DSA Algorithm ID: 1 ).
    // The result from this is the digital signature which is appended
    // to the end of the TIM by another function.

    // retrieve the size required to configure IppsRSAState context
    if ( ( Status = ippsRSAGetSize( kn, kp, IppRSApublic, &size )) != ippStsNoErr )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: EncryptSignatureRSA Error:\n");
        printf ("ippsRSAGetSize has failed!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }

    // allocate required memory space for IppsRSAState
    key = (IppsRSAState *)malloc (size);
    memset( key, 0, size );

    // Initialize the context for the RSA encryption with the allocated memory.
    if ( ( Status = ippsRSAInit( kn, kp,IppRSApublic, key )) != ippStsNoErr )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: EncryptSignatureRSA Error:\n");
        printf ("ippsRSAInit has failed!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        if (key != NULL) free (key);
        return false;
    }

    // get size of the ippsBigNumState context
    if ( ( Status = ippsBigNumGetSize( length, &size )) != ippStsNoErr )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: EncryptSignatureRSA Error:\n");
        printf ("ippsBigNumGetSize has failed!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        if (key != NULL) free (key);
        return false;
    }

    x = (IppsBigNumState *)malloc (size);
    y = (IppsBigNumState *)malloc (size);
    memset( x, 0, size );
    memset( y, 0, size );
     
    if ( ( Status = ippsBigNumInit( length, x )) != ippStsNoErr )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: EncryptSignatureRSA Error:\n");
        printf ("ippsBigNumInit has failed!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        if (key != NULL) free (key);
        if (x != NULL) free (x);
        if (y != NULL) free (y);
        return FALSE;
    }

    if ( ( Status = ippsBigNumInit( length, y )) != ippStsNoErr )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: EncryptSignatureRSA Error:\n");
        printf ("ippsBigNumInit has failed!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        if (key != NULL) free (key);
        if (x != NULL) free (x);
        if (y != NULL) free (y);
        return FALSE;
    }

    ippsSet_BN( IppsBigNumPOS, (int)(pDs->KeySize / 32), (Ipp32u *)pDs->Rsa.RSAModulus, x );
    
    if ( ( Status = ippsRSASetKey( x, IppRSAkeyN, key )) != ippStsNoErr )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: EncryptSignatureRSA Error:\n");
        printf ("ippsRSASetKey has failed!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        if (key != NULL) free (key);
        if (x != NULL) free (x);
        if (y != NULL) free (y);
        return FALSE;
    }

    if (encrypt == true )
    {
        ippsSet_BN( IppsBigNumPOS, (int)(pDs->KeySize / 32), (Ipp32u *)TimDescriptorParser.RsaPrivateKey, x);
        Status = ippsRSASetKey( x, IppRSAkeyE, key );
    }
    else
    {
        ippsSet_BN( IppsBigNumPOS, 1, (Ipp32u *)&pDs->Rsa.RSAPublicExponent, x );
        Status = ippsRSASetKey( x, IppRSAkeyE, key );
    }

    if (Status != ippStsNoErr)
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: EncryptSignatureRSA Error:\n");
        printf ("ippsRSASetKey has failed!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        if (key != NULL) free (key);
        if (x != NULL) free (x);
        if (y != NULL) free (y);
        return FALSE;
    }
    
    memcpy ( DsHash, pDsHash, sizeof(DsHash) );
    ippsSet_BN( IppsBigNumPOS, (int)(pDs->KeySize / 32), (Ipp32u *)DsHash, x );

    if ( ( Status = ippsRSAEncrypt( x, y, key )) != ippStsNoErr)
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: EncryptSignatureRSA Error:\n");
        printf ("ippsRSAEncrypt has failed!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        if (key != NULL) free (key);
        if (x != NULL) free (x);
        if (y != NULL) free (y);
        return FALSE;
    }

    memset (RsaDigitalSignature,0,sizeof(RsaDigitalSignature));
    
    if ( ( Status = ippsGet_BN( &sgn, &length, RsaDigitalSignature, y )) != ippStsNoErr )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: EncryptSignatureRSA Error:\n");
        printf ("ippsGet_BN has failed!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        if (key != NULL) free (key);
        if (x != NULL) free (x);
        if (y != NULL) free (y);
        return FALSE;
    }

    if (key != NULL) free (key);
    if (x != NULL) free (x);
    if (y != NULL) free (y);

#if DBGVERIFY
    if ( encrypt == true )
    {
        ofsEncrypted.open( "EncryptedTimHash.txt", ios_base::out | ios_base::trunc );
        if ( ofsEncrypted.bad() || ofsEncrypted.fail() )
        {
            printf ("\n  Error: fopen failed to EncryptedTimHash.txt\n");
            return false;
        }

        if (CommandLineParser.bVerbose) 
            printf ("\nSignature After the Encryption\n");
    }
    else
    {
        ofsEncrypted.open( "DecryptedTimHash.txt", ios_base::out | ios_base::trunc );
        if ( ofsEncrypted.bad() || ofsEncrypted.fail() )
        {
            printf ("\n  Error: fopen failed to DecryptedTimHash.txt\n");
            return false;
        }

        if (CommandLineParser.bVerbose) 
            printf ("\nSignature After the Decryption\n");
    }

    stringstream ss;
//	ss.setf( ios::internal, ios::adjustfield );

    string sDSValue;
    for (int i = 0; i < (int)(pDs->KeySize / 32); i++) 
    {
        sDSValue = HexFormattedAscii( RsaDigitalSignature[i] );

        // four per line
        if ( (i+1) % 4 == 0 )
            sDSValue += "\n";
        else
            sDSValue += " ";

        if (CommandLineParser.bVerbose) 
            printf ( sDSValue.c_str() );
        
        if (i!=0 && i % 4 == 0)
            ss << endl;

        ss <<"0x"<<hex<<setw(8)<<setfill('0') << sDSValue << " "; 
    }

    if (CommandLineParser.bVerbose) 
        printf ( "\n" );

    ofsEncrypted << ss.str();
    ofsEncrypted.flush();
    ofsEncrypted.close();
#endif

    return TRUE;
}

bool CTrustedImageBuilder::EncryptSignatureECDSA( bool encrypt, PLAT_DS *pDs, Ipp8u *pDsHash )
{
    int eccp_field_size_bits = pDs->KeySize;
    int BNsize32 = (eccp_field_size_bits+31)/32;
    int bigN_len32;	
    IppsBigNumSGN BN_sign;
    IppsBigNumState* p_eccp_dsa_signature_r	= New_BN(BNsize32);
    IppsBigNumState* p_eccp_dsa_signature_s	= New_BN(BNsize32);
    IppStatus Status = ippStsNoErr;

    //default private key is ascii of "MarvellWirelessTrustedModuleVer3"
//  	pPriveKey32[7]=0x4d617276;
//   	pPriveKey32[6]=0x656c6c57;
//   	pPriveKey32[5]=0x6972656c;
//   	pPriveKey32[4]=0x65737354;
//   	pPriveKey32[3]=0x72757374;
//   	pPriveKey32[2]=0x65644d6f;
//   	pPriveKey32[1]=0x64756c65;
//   	pPriveKey32[0]=0x56657233;

    IppECCType IppECCTypeID = IppECCArbitrary;
    if ( pDs->DSAlgorithmID == ECDSA_256 )
        IppECCTypeID = IppECCPStd256r1;
    else if ( pDs->DSAlgorithmID == ECDSA_521 )
        IppECCTypeID = IppECCPStd521r1;

    eccp_dsa_sign( pDs->HashAlgorithmID, pDsHash, eccp_field_size_bits, (Ipp32u *)TimDescriptorParser.ECDSAPrivateKey, 
                  p_eccp_dsa_signature_r, p_eccp_dsa_signature_s, IppECCTypeID );

#if DBGVERIFY
    cout << "\nMessage successfully signed, the signature pair is (r, s), where"<< endl << endl;
    cout << "r = "; Type_BN(0, p_eccp_dsa_signature_r); cout << endl;
    cout << "s = "; Type_BN(0, p_eccp_dsa_signature_s); cout << endl << endl;
#endif

    //convert signature pairs (r,s) from BigNumber type to U32
    Status = ippsGet_BN( &BN_sign , &bigN_len32, pDs->Ecdsa.ECDSADigS_R, p_eccp_dsa_signature_r );
    Status = ippsGet_BN( &BN_sign , &bigN_len32, pDs->Ecdsa.ECDSADigS_S, p_eccp_dsa_signature_s );

//	memcpy( ECDSADigitalSignature, p_eccp_dsa_signature_r, sizeof(Ipp32u)*MAXECCKEYSIZEWORDS );
//	memcpy( (ECDSADigitalSignature+MAXECDSAKEYSIZEWORDS), p_eccp_dsa_signature_s, sizeof(Ipp32u)*MAXECCKEYSIZEWORDS );

    memcpy( ECDSADigitalSignature, pDs->Ecdsa.ECDSADigS_R, eccp_field_size_bits / 8 );
    memcpy( (ECDSADigitalSignature + (( eccp_field_size_bits / 8 ) / 4 )), 
                            pDs->Ecdsa.ECDSADigS_S, eccp_field_size_bits / 8 );

    delete[] (Ipp8u*) p_eccp_dsa_signature_r;
    delete[] (Ipp8u*) p_eccp_dsa_signature_s;

    return TRUE;
}


void CTrustedImageBuilder::ConvertMSWToLSWAndFlip(PLAT_DS *pDs)
{
    Ipp32u DigitalSignatureWordSwapped[MAXRSAKEYSIZEWORDS] = {0};

    if (pDs->DSAlgorithmID == PKCS1_v1_5_Caddo)
    {
        //PKCS_1.5:Convert Digital signature from MSW to LSW.
        for (unsigned int i=0, j=(int)(pDs->KeySize / 32)-1; i<(int)(pDs->KeySize / 32);i++,j--)
        {
            DigitalSignatureWordSwapped[j]=RsaDigitalSignature[i];	
        }
        //PKCS_1.5:Copy swapped array into the global array.
        for (int i=0;i<(int)(pDs->KeySize / 32);i++)
        {
            RsaDigitalSignature[i]=DigitalSignatureWordSwapped[i];
        }
    }
}

bool CTrustedImageBuilder::InstallDigitalSignature (fstream& fsFile)
{
    PLAT_DS& Ds = TimDescriptorParser.Ds;

    if ( !fsFile.is_open() || fsFile.bad() || fsFile.fail() )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: InstallDigitalSignature Error:\n");
//		printf ("Cannot open file name %s !\n",fsFile);
        printf ("////////////////////////////////////////////////////////////\n");
    }
    else
    {
        fsFile.seekg( 0, ios_base::end );
        std::streamoff pos = fsFile.tellg();
        fsFile.clear();

        if ( Ds.DSAlgorithmID == ECDSA_256 || Ds.DSAlgorithmID == ECDSA_521 )
        {
            fsFile.write( (char*)&Ds.Ecdsa.ECDSADigS_R, 
                            sizeof(Ds.Ecdsa.ECDSADigS_R) + sizeof(Ds.Ecdsa.ECDSADigS_S) + sizeof(Ds.Ecdsa.Reserved));
        }
        else
            fsFile.write( (char*)RsaDigitalSignature, 
                            sizeof(Ipp32u) * MAXRSAKEYSIZEWORDS );
    }

    return fsFile.good();
}

bool CTrustedImageBuilder::OutputOtpHash (CTIM *pTimHeader,PLAT_DS *pDs)
{
    string sHashValue;
    ofstream ofsOtpHash;
    unsigned int SHAHashSize = /*(pDs->HashAlgorithmID == SHA512) ? 16 :*/ (pDs->HashAlgorithmID == SHA256) ? 8 : 5;

    if (CommandLineParser.bVerbose) 
    {
        printf ("\nOEM Hash\n");
    }
    for (unsigned int i = 0; i < SHAHashSize; i++)
    {
        if (CommandLineParser.bVerbose) 
        {
            printf ("0x%08X\n",pDs->Hash[i]);
        }
    }


    ofsOtpHash.open("OtpHash.txt", ios_base::out | ios_base::trunc );
    if ( ofsOtpHash.bad() || ofsOtpHash.fail() )
    {
        printf ("\n  Error: fopen failed to OtpHash.txt\n");
        return FALSE;
    }

    for (unsigned int i = 0; i < SHAHashSize; i++)
    {
        sHashValue = HexFormattedAscii( pDs->Hash[i] );
        sHashValue += "\n";
        ofsOtpHash.write( sHashValue.c_str(), (int)sHashValue.length() );
    }

    ofsOtpHash.close();

    return TRUE;
}

bool CTrustedImageBuilder::VerifyTimHash ( fstream& fsTimBinFile, PLAT_DS *pDs,Ipp8u *pDsHash, unsigned int TimSizeToHash )
{
#if 1 //RAR WIP
    printf ("////////////////////////////////////////////////////////////\n");
    printf ("  TIM Hash verification is not functional in this release in VerifyTimHash()!\n");
    printf ("////////////////////////////////////////////////////////////\n");
    return true;
#endif

    Ipp8u DsHash[MAX_HASH_IN_BITS/8] = {0};
    Ipp8u DsHashOriginal[MAX_HASH_IN_BITS/8] = {0};
    Ipp8u DsHash_WordSwapped[MAX_HASH_IN_BITS/8] = {0};
    unsigned int SHAHashSize = 0;

    unsigned char *TimBuffer = NULL;
    unsigned int i = 0, j = 0;

    if (pDs->DSAlgorithmID == PKCS1_v1_5_Caddo)
        EnableEndianConvert = true;
    
    if (pDs->DSAlgorithmID == Marvell_DS )
    {
        for (i = 0; (unsigned int)i < ((pDs->KeySize / 32) - 1); i++)
            ((Ipp32u *)DsHash)[i] = 0xBEBEBEBE;

        ((Ipp32u *)DsHash)[i] = 0x6EBEBEBE;
    }
    else if (pDs->DSAlgorithmID == PKCS1_v1_5_Caddo || pDs->DSAlgorithmID == PKCS1_v1_5_Ippcp )
    {
        // Encryption block(EB) formatting for PKCS
        //=================================
        // EB = 00 || BT || PS || 00 || D
        //==================================
        // BT = 00, 01(public key operation) or 02(private key)
        // PS = 00, FF, randomly generated(based on BT values)
        // D = Data quantity
        // All values are in octets
        DsHash[0] = 0x0;
        DsHash[1] = 0x01;
        for (j = 2; (unsigned int)j < ((pDs->KeySize / 32) - 1); j++)
            DsHash[j] = 0xFF;
        DsHash[j] = 0x0;
        DsHash[j+1] = (Ipp8u)TimSizeToHash;
    }
    else
    {
       printf("  Error: DSA Algorithm needs to be Marvell method or PKCS1_v1_5(_Ippcp)!\n\n");
       return FALSE;
    }

    fsTimBinFile.seekg( 0, ios_base::beg );

    // allocate a buffer of a size that is word aligned
    unsigned int pad = sizeof(UINT_T)-(TimSizeToHash % sizeof(UINT_T));
    // if pad is sizeof(UINT_T), then no pad needed
    unsigned int allocSize = ((pad == sizeof(UINT_T)) ? TimSizeToHash : TimSizeToHash + pad);
    // allocate a buffer that is size that is word aligned
    if (NULL == (TimBuffer = (unsigned char *)malloc ( allocSize ) ) )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: Memory allocation for TimBuffer failed in VerifyTimHash()!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }
    memset( TimBuffer, 0x0, allocSize );

    fsTimBinFile.read( (char*)TimBuffer, TimSizeToHash );
    if ( fsTimBinFile.bad() || fsTimBinFile.fail() )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: VerifyTimHash() couldn't read all of the %ld bytes.\n",TimSizeToHash);
        printf ("from the TIM binary image!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        if (TimBuffer != NULL) free (TimBuffer);
        return false;
    }

    if ( EnableEndianConvert )
    {
        for(unsigned int c=0; c<TimSizeToHash; c+=4)
        {
            Endian_Convert(*(unsigned int *)&TimBuffer[c], (unsigned int *)&TimBuffer[c]);
            // if the last word does not use all bytes, need to shift bytes accordingly
            if ( c+4 > TimSizeToHash )
            {
                *(unsigned int*)&TimBuffer[c] >>= pad*8; // shift right all align data
            }
        }
    }

    //Calculate the SHA hash of the TIM file
    if (pDs->HashAlgorithmID == SHA256)
    {
        ippsSHA256MessageDigest (TimBuffer,TimSizeToHash,DsHash);
        SHAHashSize = 8;
    }
    else
    {
        ippsSHA1MessageDigest (TimBuffer,TimSizeToHash,DsHash);
        SHAHashSize = 5; 
    }

    if (TimBuffer != NULL) free (TimBuffer);

    if ( EnableEndianConvert )
    {
        for (i = 0; (unsigned int)i < SHAHashSize; i += 4 )
            Endian_Convert (*(UINT_T *)&DsHash[i],(UINT_T *)&DsHash[i]);
    }

#if 1 // rar wip
    for (unsigned int j = 0; j < SHAHashSize*4; j++)
        ((Ipp8u *)DsHash_WordSwapped)[j] = ((Ipp8u *)DsHash)[(SHAHashSize*4)-1-j];

    for (unsigned int i = 0; i < SHAHashSize*4; i++)
        DsHash[i] = DsHash_WordSwapped[i];
#endif

    // copy previously calculated digital signature to DsHashOriginal
//	memcpy (DsHashOriginal,(Ipp8u *)RsaDigitalSignature,(SHAHashSize*4));
    memcpy (DsHashOriginal,(Ipp8u *)RsaDigitalSignature,MAX_HASH_IN_BITS/8);

//	if (memcmp (DsHash,DsHashOriginal,(SHAHashSize*4)) != 0)
    if (memcmp (DsHash,DsHashOriginal,MAX_HASH_IN_BITS/8) != 0)
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: TIM Hash verification fails in VerifyTimHash()!\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }
    else
    {
        printf("TIM Hash verification successful.\n");
    }

#if DBGVERIFY
    ofstream ofsTimHash;
    string sHashValue;

    ofsTimHash.open( "TimHashVerify.txt", ios_base::out | ios_base::trunc );
    if ( ofsTimHash.bad() || ofsTimHash.fail() )
        printf ("\n  Error: fopen failed to TimHashVerify.txt\n");

    if (CommandLineParser.bVerbose)
        printf ("\nRe-verified hash for size %d\n\n",(SHAHashSize*4));

    for (i = 0; (unsigned int)i < ((SHAHashSize)); i++)
    {
        sHashValue = HexFormattedAscii( ((Ipp32u *)DsHash)[i] );
        // four per line
        if ( (i+1) % 4 == 0 )
            sHashValue += "\n";
        else
            sHashValue += " ";

        if (CommandLineParser.bVerbose) 
            printf ( sHashValue.c_str() );

        ofsTimHash.write( sHashValue.c_str(), (int)sHashValue.length() );
    }

    if (CommandLineParser.bVerbose) 
        printf ("\n");

    ofsTimHash.close();
#endif

    return TRUE;
}

//#define ECCP_MSG
#ifdef ECCP_MSG
#define TEST_MSG_LEN (555)
static  Ipp8u  eccp_dsa_test_msg[TEST_MSG_LEN];

static void eccp_dsa_test_msg_init()
{
    int i;
    for (i = 0; i < TEST_MSG_LEN; i++) 
    {
      eccp_dsa_test_msg[i] = i;
    }
}
#endif

void CTrustedImageBuilder::eccp_dsa_sign(unsigned int HashAlgorithmId, Ipp8u *pDsHash, int fesize, Ipp32u *pPriveKey32, IppsBigNumState* pSignature_r, IppsBigNumState* pSignature_s, IppECCType eccp_curve_type ) 
{
    int FieldSZ_in_WORDS = (fesize+31)/32;
    int BNsize_in_WORDS = FieldSZ_in_WORDS;

    int pBitSize=0;
    int pSize=0;
    int pCofactor=0;
    int pECPointSize=0;
    IppStatus Status = ippStsNoErr;

#ifdef ECCP_MSG
    eccp_dsa_test_msg_init();
#endif

    Status = ippsECCPGetSize(fesize, &pSize);
    Status = ippsECCPPointGetSize(fesize, &pECPointSize);

    IppsECCPState *pECC = (IppsECCPState*)new Ipp8u[pSize];
    Status = ippsECCPInit (fesize, pECC);
    Status = ippsECCPSetStd(eccp_curve_type, pECC);

    IppsBigNumState *pPrime	= New_BN (FieldSZ_in_WORDS); 
    IppsBigNumState *pA		= New_BN (FieldSZ_in_WORDS);
    IppsBigNumState *pB		= New_BN (FieldSZ_in_WORDS);
    IppsBigNumState *pGx	= New_BN (FieldSZ_in_WORDS);
    IppsBigNumState *pGy	= New_BN (FieldSZ_in_WORDS);
    IppsBigNumState *pOrder	= New_BN (FieldSZ_in_WORDS);

    IppsBigNumState* LargeTmp	= New_BN(2 * BNsize_in_WORDS);
    IppsBigNumState* tmp		= New_BN(BNsize_in_WORDS);

    Status = ippsECCPGet(pPrime, pA, pB, pGx, pGy, pOrder, &pCofactor, pECC);	//obtain EC parameters from generated curve
    
    Status = ippsECCPGetOrderBitSize (&pBitSize, pECC);
    //printECCpar(pBitSize, pPrime, pA, pB, pGx, pGy, pOrder, pCofactor); //print the parameter list of EC
    
#ifdef ECCP_MSG
    Ipp8u  *p_msg  = (Ipp8u *)eccp_dsa_test_msg;
#endif

    IppsECCPPointState *pECPointG = (IppsECCPPointState*)new Ipp8u[pECPointSize];
    IppsECCPPointState *pECPoint_DSA_Q = (IppsECCPPointState*)new Ipp8u[pECPointSize];
    IppsECCPPointState *pECPoint_DSA_kG = (IppsECCPPointState*)new Ipp8u[pECPointSize];

    Status = ippsECCPPointInit(fesize, pECPointG);
    Status = ippsECCPPointInit(fesize, pECPoint_DSA_Q);
    Status = ippsECCPPointInit(fesize, pECPoint_DSA_kG);

    Status = ippsECCPSetPoint(pGx, pGy, pECPointG, pECC);

    //Calculate the SHA hash of the TIM file
    int SHAHashSize = 0;
    if ( HashAlgorithmId == SHA256 )
        SHAHashSize = 8;
    else
        SHAHashSize = 5; 

    IppsBigNumState *pDstSHA_BN = 0; 
    pDstSHA_BN = New_BN (SHAHashSize);	
    Status = ippsSetOctString_BN (pDsHash, SHAHashSize*4, pDstSHA_BN);
    
#if DBGVERIFY
    cout << endl << endl;
//	cout << "msg length ="<<sizeof(eccp_dsa_test_msg) << endl;
    cout << "The digest of the input message is:" << endl;
    Type_BN(0, pDstSHA_BN);	cout << endl << endl;
#endif

    IppsBigNumState* pRx			= New_BN(BNsize_in_WORDS);
    IppsBigNumState* pRy			= New_BN(BNsize_in_WORDS);
    IppsBigNumState* pPriveKey_DSA  = New_BN(BNsize_in_WORDS);
    IppsBigNumState* pRandomK;

    Status = ippsSet_BN(IppsBigNumPOS, BNsize_in_WORDS, pPriveKey32, pPriveKey_DSA);
    
    pRandomK		= pGx;

#if DBGVERIFY
    cout << "The private key for DSA is: " << endl;
    Type_BN(0, pPriveKey_DSA); cout << endl << endl;

    cout << "The random number k generated for DSA is: " << endl;
    Type_BN(0, pRandomK); cout << endl << endl;
#endif

    Status = ippsECCPMulPointScalar (pECPointG, pPriveKey_DSA, pECPoint_DSA_Q, pECC);
    Status = ippsECCPMulPointScalar (pECPointG, pRandomK, pECPoint_DSA_kG, pECC);
    Status = ippsECCPGetPoint (LargeTmp, tmp, pECPoint_DSA_kG, pECC); 
    Status = ippsMod_BN(LargeTmp, pOrder, pSignature_r);	
    //tmp1 is y coordinate, not necessary for future computation

    eccp_dsa_sign_s_computation (	pRandomK,  pOrder, pPriveKey_DSA, pSignature_r,
                                    pSignature_s, pDstSHA_BN, BNsize_in_WORDS);
    
    Status = ippsECCPGetPoint (pRx, pRy, pECPoint_DSA_Q, pECC);

#if DBGVERIFY
    cout << endl << "The public key has coordinate pair (Rx1, Ry1):" << endl << endl;
    cout << "Rx1 = ";Type_BN(0, pRx);cout << endl; 
    cout << "Ry1 = ";Type_BN(0, pRy);cout << endl; 
#endif

    delete[] (Ipp8u*) pPrime;
    delete[] (Ipp8u*) pA;
    delete[] (Ipp8u*) pB;
    delete[] (Ipp8u*) pGx;
    delete[] (Ipp8u*) pGy;
    delete[] (Ipp8u*) pOrder;
    delete[] (Ipp8u*) pDstSHA_BN;
    
    delete[] (Ipp8u*) pRx;
    delete[] (Ipp8u*) pRy;
    delete[] (Ipp8u*) pPriveKey_DSA;

    delete[] (Ipp8u*) pECPointG;
    delete[] (Ipp8u*) pECPoint_DSA_Q;
    delete[] (Ipp8u*) pECPoint_DSA_kG;

    delete[] (Ipp8u*) pECC;

    delete[] (Ipp8u*) tmp;
    delete[] (Ipp8u*) LargeTmp;
}

IppsBigNumState* CTrustedImageBuilder::New_BN(int size, const Ipp32u* pData)
{
    // get the size of the Big Number context
    int ctxSize = 0;
    IppStatus Status = ippStsNoErr;
    
    Status = ippsBigNumGetSize(size, &ctxSize);
    // allocate the Big Number context
    IppsBigNumState* pBN = (IppsBigNumState*)( new Ipp8u [ctxSize] );
    // and initialize one
    Status = ippsBigNumInit(size, pBN);
    
    // if any data was supplied, then set up the Big Number value
    if(pData)
        Status = ippsSet_BN(IppsBigNumPOS, size, pData, pBN);
    
    // return pointer to the Big Number context for future use
    return pBN;
}

void CTrustedImageBuilder::Type_BN(const char* pMsg, const IppsBigNumState*  pBN)
{
    // size of Big Number
    int size=0;
    IppStatus Status = ippStsNoErr;
 
    Status = ippsGetSize_BN(pBN, &size);

    // extract Big Number value and convert it to the string presentation
    Ipp8u* bnValue = new Ipp8u [size*4];
    Status = ippsGetOctString_BN(bnValue, size*4, pBN);

    // type header
    if(pMsg)	cout << pMsg;

    // type value
    int tmp;
    int dummyBit=0;

    for(int n=0; n<size*4; n++) //revised by Yingda, add dummyBit for small hex number, to display correctly
    {
        tmp=(int)bnValue[n];
        if (tmp<16) {cout << dummyBit; cout << hex << tmp;}
        else cout << hex << tmp;
        if (n%4==3) cout <<" ";
    }
    cout << dec;

    delete [] bnValue;
}

void CTrustedImageBuilder::eccp_dsa_sign_s_computation ( IppsBigNumState* pRandomK, IppsBigNumState* pOrder,
                                    IppsBigNumState* pPriveKey_DSA, IppsBigNumState* r,
                                    IppsBigNumState* s, IppsBigNumState * e, int BNsize32)
{
    IppsBigNumState* tmp1		= New_BN(BNsize32);
    IppsBigNumState* tmp2		= New_BN(BNsize32);
    IppsBigNumState* tmp3		= New_BN(BNsize32+1);
    IppsBigNumState* LargeTmp	= New_BN(2 * BNsize32);
    IppStatus Status = ippStsNoErr;

    Status = ippsModInv_BN(pRandomK, pOrder, tmp1);	//tmp1 is now the multiplicative inverse of k
//	cout << "inv(k) = "; Type_BN(0, tmp1); cout << endl; 
    Status = ippsMul_BN(pPriveKey_DSA, r, LargeTmp); //LargeTmp stores value of d*r
    Status = ippsMod_BN(LargeTmp, pOrder, tmp2);	// tmp2 stores the modular reduction result of d * r
//	cout << "d * r= "; Type_BN(0, tmp2); cout << endl; 
    Status = ippsAdd_BN(e, tmp2, tmp3);		//tmp3 stores e + d*r
//	cout << "e + d * r= ";Type_BN(0, tmp3); cout << endl; 
    Status = ippsMul_BN(tmp1, tmp3, LargeTmp);	//LargeTmp stores value of inv (k) * (e + d*r)
    Status = ippsMod_BN(LargeTmp, pOrder, s);     //compute signautre s, the modular reduction of LargeTmp
    
    delete[] (Ipp8u*) tmp1;
    delete[] (Ipp8u*) tmp2;
    delete[] (Ipp8u*) tmp3;
    delete[] (Ipp8u*) LargeTmp;
}

#endif //TRUSTED


