/******************************************************************************
 *
 *  (C)Copyright 2005 - 2011 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once

static	char Version[11] =	{"3.3.1.80"};
static	char Date[11] =		{"20110309"};

#include <string>
#include <iostream>
#include <list>
#include <vector>
using namespace std;

#include "wtpdef.h"

// define WTPTP_3_2 only for the WTPTP_3_2_X release stream 
#define WTPTP_3_2_X 1

#if WTPTP_3_2_X // previous version commandline switches now obsolete
#define TIM_BIN_FILE	't'
#define COMM_PORT		'C'
#define DEBUG_BOOT		'D'
#define FBF_FILE		'F'
#define INTERFACE_NAME	'i'
#define HELP			'h'
#define IMAGE_FILE		'f'
#define JTAG_KEY		'k'
#define LOG_FILE		'L'
#define MESSAGE_MODE	'M'
#define DISABLE_FAST_DOWNLOAD	'N'
#define OTP_FLASH_TYPE	'O'
#define PORT_TYPE		'P'
#define PARTITION_TABLE	'p'
#define BAUD_RATE		'B'
#define UART_DELAY		'S'
#define PLATFORM_TYPE	'T'
#define USB_PACKET_SIZE 'U'
#define VERBOSE			'V'
#define UPLOAD_DATA     'W'
#define SCRIPT_FILE		'x'

#else

// new commandline switches (you can use upper or lower case)
#define TIM_BIN_FILE	'B'
#define COMM_PORT		'C'
#define DEBUG_BOOT		'D'
#define FBF_FILE		'F'
#define INTERFACE_NAME	'G'
#define HELP			'H'
#define IMAGE_FILE		'I'
#define JTAG_KEY		'J'
#define LOG_FILE		'L'
#define MESSAGE_MODE	'M'
#define DISABLE_FAST_DOWNLOAD	'N'
#define OTP_FLASH_TYPE	'O'
#define PORT_TYPE		'P'
#define PARTITION_TABLE	'Q'
#define BAUD_RATE		'R'
#define UART_DELAY		'S'
#define PLATFORM_TYPE	'T'
#define USB_PACKET_SIZE 'U'
#define VERBOSE			'V'
#define UPLOAD_DATA     'W'
#define SCRIPT_FILE		'X'
#endif

enum ePlatformType {  PXAMIN_PT = -1,
                      PXA168 = 0, 
                      PXA30x, PXA31x, PXA32x, 
                      PXA688,
                      PXA91x, PXA92x, PXA93x, PXA94x, PXA95x, 
                      ARMADA168,
                      ARMADA610,
                      MG1,
                      TAVOR_PV_MG2,
                      ESHEL,
                      PXA978,
                      ARMADA620,
                      ESHEL_LTE,
                      ARMADA622,
                      WUKONG,
//					  P_88AP001,
                      PXAMAX_PT 
                    };


typedef vector<string*>				t_stringVec;
typedef vector<string*>::iterator	t_stringVecIter;

typedef vector<IMAGEFILE*>           t_ImageFileVec;
typedef vector<IMAGEFILE*>::iterator t_ImageFileVecIter;

class CCommandLineParser
{
public:
    CCommandLineParser(void);
    ~CCommandLineParser(void);

    bool ParseCommandLine (int argc,char* argv[]);
    bool CheckYourOptions ();
    void PrintUsage ();
    void Reset();

    bool ParseTim ();
//	bool GetImageFile (DWORD ImageType,char *TypeName,BYTE Option,int Argc,char *ImageFile);
    bool SetImageId (UINT_T *pImageId,int nImages);

    bool Verbose() { return bVerbose; }
    void Verbose( bool bSet ) { bVerbose = true; }

    string& TimFileName() { return sTimFileName; }
    void TimFileName( string sTimFileName ) { this->sTimFileName = sTimFileName; bTimFile = true; }
    bool TimFile() { return bTimFile; }

    string& ImageFileName( int idx ) { return *ImageFileNames[ idx ]; }
    void ImageFileName( string sImageFN ) { ImageFileNames.push_back( new string( sImageFN ) ); bImageFile = true; }
    bool ImageFile() { return bImageFile; }

    string& FbfFileName() { return sFbfFileName; }
    void FbfFileName( string sFbfFN ) { sFbfFileName = sFbfFN; bFbfFile = true; }
    bool FbfFile() { return bFbfFile; }

    string& JtagFileName() { return sJtagFileName; }
    void JtagFileName( string sJtagFN ) { sJtagFileName = sJtagFN; bJtagFile = true; }
    bool JtagFile() { return bJtagFile; }

    eMessageMode CurrentMessageMode() { return iCurrentMessageMode; }
    void CurrentMessageMode( eMessageMode iMessageMode ) { iCurrentMessageMode = iMessageMode; }
    bool MessageMode() { return bMessageMode; }

    BYTE OtpFlashType() { return iOtpFlashType; }
    void OtpFlashType( BYTE iFlashType ) { iOtpFlashType = iFlashType; bOtpView = true; }

    bool OtpView() { return bOtpView; }
    void OtpView( bool bSet ) { bOtpView = bSet; }

    bool DebugBoot() { return bDebugBoot; }
    void DebugBoot( bool bSet ) { bDebugBoot = bSet; }

    int PortType() { return iPortType; }
    void PortType( int iPortType ) { this->iPortType = iPortType; bPortType = true; }

    int CommPort() { return iCommPort; }
    void CommPort( int iCommPort ) { this->iCommPort = iCommPort; bCommPort = true; }

    string& USBInterfaceName() { return sUSBInterface; }
    void USBInterfaceName( string sUSBIntf ) { sUSBInterface = sUSBIntf; bUSBInterface = true; }
    bool USBInterface() { return bUSBInterface; }

    int USBPacketSize() { return iUSBPacketSize; }
    void USBPacketSize( int iSize );

    int UARTDelay() { return iUARTDelay; }
    void UARTDelay( int iUARTDelay ) { this->iUARTDelay = iUARTDelay; bUARTDelay = true; }

    int DownloadBaudRate() { return iDownloadBaudRate; }
    void DownloadBaudRate( int iBaudRate ) { iDownloadBaudRate = iBaudRate; 
                                             if (iDownloadBaudRate > 115200) iDownloadBaudRate = 115200;
                                             bBaudRate = true; }
    bool BaudRate() { return bBaudRate; }

    ePlatformType PlatformType() { return iPlatformType; }
    void PlatformType( ePlatformType iPlatformType ) { this->iPlatformType = iPlatformType; bPlatform = true; }
    bool Platform() { return bPlatform; }

    string& PartitionFileName() { return sPartitionFileName; }
    void PartitionFileName( string sPartitionFN ) { sPartitionFileName = sPartitionFN; bPartitionFile = true; }
    bool PartitionFile() { return bPartitionFile; }

    bool FastDownload() { return bFastDownload; }
    void FastDownload( bool bSet ) { bFastDownload = bSet; }

    string& LogFileName() { return sLogFileName; }
    void LogFileName( string sLogFN ) { sLogFileName = sLogFN; bLogFile = true; }
    bool LogFile() { return bLogFile; }

    string& UploadSpecFileName() { return sUploadSpecFileName; }
    void UploadSpecFileName( string sFN ) { sUploadSpecFileName = sFN; bUploadSpecFile = true; }
    bool UploadSpecFile() { return bUploadSpecFile; }

    unsigned int NumImageFiles() { return ImageFilesVec.size(); }
    t_ImageFileVec& ImageFiles(){ return ImageFilesVec; }
    IMAGEFILE* ImageFile( int idx ) { return ImageFilesVec[ idx ]; }
    void ImageFile( IMAGEFILE* pImage ) { ImageFilesVec.push_back( pImage ); bImageFile = true; }

	void DumpConfiguration();


private:
    bool bVerbose;
    bool bTimFile;
    bool bImageFile;
    bool bFbfFile;
    bool bJtagFile;
    bool bMessageMode;
    bool bPartitionFile;
    bool bPortType;
    bool bOtpView;
    bool bDebugBoot;
    bool bUSBInterface;
    bool bUSBPacketSize;
    bool bUARTDelay;
    bool bCommPort;
    bool bBaudRate;
    bool bLogFile;
    bool bPlatform;
    bool bFastDownload;
    bool bUploadSpecFile;

    ePlatformType   iPlatformType;
    eMessageMode	iCurrentMessageMode;
    BYTE			iOtpFlashType;

    int iUSBPacketSize;
    int iUARTDelay;
    int iPortType;
    int iCommPort;
    int iDownloadBaudRate;

    string sTimFileName; 
    string sFbfFileName;
    string sJtagFileName;
    string sPartitionFileName;
    string sLogFileName;
    string sUSBInterface;
    string sUploadSpecFileName;

    t_stringVec		ImageFileNames;
    t_ImageFileVec  ImageFilesVec;

    int FileIndex;

#if TOOLS_GUI == 1
    bool ProcessorType( string& sProcessorType );
    string ProcessorType();
#endif

};
