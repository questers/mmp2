/**************************************************************************
**
** (C) Copyright December 2006 Marvell International Ltd.
**
** All Rights Reserved.
**
**
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE   
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR  
** PURPOSE ARE EXPRESSLY DISCLAIMED.  
**
**  FILENAME:	WTPSECURITYLIB.h
**
**  PURPOSE: 	Security LIB routines 
**                  
******************************************************************************/

#pragma once

#include <WTPDEF.h>

#include <vector>
using namespace std;
typedef vector<IMAGEFILE*>           t_ImageFileVec;
typedef vector<IMAGEFILE*>::iterator t_ImageFileVecIter;

#define MAX_PATH 260

#define NoError            			0x0
#define NotFoundError      			0x1
#define KeyLengthReadError			0x0D
#define	FlashTypeReadError			0x0F
#define IssueDateReadError			0x10
#define OEMUniqueIDReadError		0x11
#define HashValueLengthReadError	0x12
#define RSAExponentReadError		0x13
#define RSAPrivateKeyReadError		0x14
#define RSAModulusReadError			0x15

#if TRUSTED
typedef struct 
{
	unsigned char PublicKey[256];
	unsigned char Modulus[256];
	unsigned char PrivateKey[256];
	unsigned int  KeyLength;
} KeyInfo, *pKeyInfo;

int ReadKeyFile (FILE *kFile,unsigned char *pPubKey,unsigned char *pPrivKey,
                 unsigned char *pModulus,unsigned int *pKeyLength);
BOOL RSA (unsigned int *PubKey,unsigned int *pModulus,int modlen,
          unsigned int *pInput,unsigned int *pOutput);
void Sha1Digest (unsigned char *pMesg,int len,unsigned char *pMD);
void PadPasswordToSign (unsigned int *Password,unsigned int KeyLength);
#endif

void Endian_Convert (unsigned char *first);
//BOOL ParseTim (IMAGEFILE *pImageFiles,int nFiles,int *pNumberOfImages, BOOL bVerbose);

