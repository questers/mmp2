/**************************************************************************

** INTEL CONFIDENTIAL
** Copyright 2000-2005 Intel Corporation. All Rights Reserved.
**
** The source code contained or described herein and all documents
** related to the source code (Material) are owned by Intel Corporation
** or its suppliers or licensors.  Title to the Material remains with
** Intel Corporation or its suppliers and licensors. The Material contains
** trade secrets and proprietary and confidential information of Intel
** or its suppliers and licensors. The Material is protected by worldwide
** copyright and trade secret laws and treaty provisions. No part of the
** Material may be used, copied, reproduced, modified, published, uploaded,
** posted, transmitted, distributed, or disclosed in any way without Intel�s
** prior express written permission.

** No license under any patent, copyright, trade secret or other intellectual
** property right is granted to or conferred upon you by disclosure or
** delivery of the Materials, either expressly, by implication, inducement,
** estoppel or otherwise. Any license under such intellectual property rights
** must be express and approved by Intel in writing.
*/

/*++

Module Name:

    $Workfile:: StdAfx.h                  $

Abstract:

    Includes the standard system include files, or project specific include files that are used 
    frequently, but are changed infrequently.

Environment:

    User mode only

Revision History:
    $Date:: 5/15/06 4:08p                 $ Date and time of last check in
    $Revision:: 1                         $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/

// stdafx.h : include file for standard system include files,
//            or project specific include files that are used frequently, but
//            are changed infrequently

#if !defined(AFX_STDAFX_H__9DF8312C_8E51_4B0B_BEE5_3985D71E5DED__INCLUDED_)
#define AFX_STDAFX_H__9DF8312C_8E51_4B0B_BEE5_3985D71E5DED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <WINDOWS.H>
#include <STDIO.H>
#include <IO.H>         // chmod ...
#include <SYS\STAT.H>
#include <STRING.H>     // strtol ...
#include <STDLIB.H>     // exit ...
#include <ERRNO.H>      // EINVAL
#include <WINNT.H>      // For HANDLE
#include <SETUPAPI.H>
#include <INITGUID.H>
//#include <AFX.H>


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__9DF8312C_8E51_4B0B_BEE5_3985D71E5DED__INCLUDED_)
