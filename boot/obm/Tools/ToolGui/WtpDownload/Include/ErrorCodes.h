/**************************************************************************
**
** (C) Copyright December 2006 Marvell International Ltd.
**
** All Rights Reserved.
**
**
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE   
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR  
** PURPOSE ARE EXPRESSLY DISCLAIMED. 
**
**  FILENAME:	ErrorCodes.h
**
**  PURPOSE: 	Contains DKB error definitions
**  
******************************************************************************/

#ifndef ERRORCODES
#define ERRORCODES

#define MAXERRORCODES 54

typedef struct
{
	BYTE ErrorCode;
	char Description[50];
} ERRORCODETABLE;

// General error code definitions			0x0 - 0x1F
#define NoError            					0x0
#define NotFoundError      					0x1
#define GeneralError       					0x2
#define WriteError         					0x3
#define ReadError		   					0x4
#define NotSupportedError  					0x5
#define InvalidPlatformConfigError			0x6
#define PlatformBusy						0x7
#define PlatformReady						0x8
#define PlatformDisconnect					0xC

// Flash Related Errors 					0x20 - 0x3F
#define EraseError		 					0x20
#define ProgramError						0x21
#define InvalidBootTypeError				0x22
#define ProtectionRegProgramError			0x23
#define NoOTPFound							0x24

// DFC Related Errors						0x40 - 0x5F
#define DFCDoubleBitError    				0x40
#define DFCSingleBitError    				0x41
#define DFCCS0BadBlockDetected  			0x42		
#define DFCCS1BadBlockDetected  			0x43
#define	DFCInitFailed						0x44

// Security Related Errors 					0x60 - 0x8F
#define InvalidOEMVerifyKeyError			0x60
#define InvalidOBMImageError				0x61
#define SecureBootFailureError				0x62
#define InvalidSecureBootMethodError		0x63
#define UnsupportedFlashError				0x64
#define InvalidCaddoFIFOEntryError      	0x65
#define InvalidCaddoKeyNumberError			0x66
#define InvalidCaddoKeyTypeError			0x67
#define RSADigitalSignatureDecryptError 	0x68
#define InvalidHashValueLengthError     	0x69
#define InvalidTIMImageError				0x6A
#define HashSizeMismatch					0x6B
#define InvalidKeyHashError					0x6C
#define TIMNotFound							0x6D
#define WTMStateError						0x6E
#define FuseRWError							0x6F
#define InvalidOTPHashError					0x70
#define CRCFailedError						0x71

// Download Protocols						0x90 - 0xAF
#define DownloadPortError					0x90
#define DownloadError						0x91
#define FlashNotErasedError					0x92
#define InvalidKeyLengthError				0x93
#define DownloadImageTooBigError			0x94
#define UsbPreambleError					0x95
#define UsbPreambleTimeOutError				0x96
#define UartReadWriteTimeOutError			0x97
#define UnknownImageError					0x98
#define MessageBufferFullError				0x99
#define NoEnumerationResponseTimeOutError 	0x9A
#define UnknownProtocolCmd					0x9B

//JTAG ReEnable Error Codes					0xB0 - 0xCF
#define JtagReEnableError					0xB0
#define JtagReEnableOEMPubKeyError			0xB1
#define JtagReEnableOEMSignedPassWdError	0xB2
#define JtagReEnableTimeOutError			0xB3
#define JtagReEnableOEMKeyLengthError   	0xB4

ERRORCODETABLE ErrorCodes[MAXERRORCODES] =
{
	{NoError,"NoError"},
	{NotFoundError,"NotFoundError"},
	{GeneralError,"GeneralError"},
	{WriteError,"WriteError"},
	{ReadError,"ReadError"},
	{NotSupportedError,"NotSupportedError"},
	{PlatformBusy,"PlatformBusy"},
	{PlatformReady,"PlatformReady"},
	{EraseError,"EraseError"},
	{ProgramError,"ProgramError"},
	{InvalidBootTypeError,"InvalidBootTypeError"},
	{ProtectionRegProgramError,"ProtectionRegProgramError"},
	{NoOTPFound,"NoOTPFound"},
	{DFCDoubleBitError,"DFCDoubleBitError"},
	{DFCSingleBitError,"DFCSingleBitError"},
	{DFCCS0BadBlockDetected,"DFCCS0BadBlockDetected"},		
	{DFCCS1BadBlockDetected,"DFCCS1BadBlockDetected"},
	{DFCInitFailed,"DFCInitFailed"},
	{InvalidOEMVerifyKeyError,"InvalidOEMVerifyKeyError"},
	{InvalidOBMImageError,"InvalidOBMImageError"},
	{SecureBootFailureError,"SecureBootFailureError"},
	{InvalidSecureBootMethodError,"InvalidSecureBootMethodError"},
	{UnsupportedFlashError,"UnsupportedFlashError"},
	{InvalidCaddoFIFOEntryError,"InvalidCaddoFIFOEntryError"},
	{InvalidCaddoKeyNumberError,"InvalidCaddoKeyNumberError"},
	{InvalidCaddoKeyTypeError,"InvalidCaddoKeyTypeError"},
	{RSADigitalSignatureDecryptError,"RSADigitalSignatureDecryptError"},
	{InvalidHashValueLengthError,"InvalidHashValueLengthError"},
	{InvalidTIMImageError,"InvalidTIMImageError"},
	{HashSizeMismatch,"HashSizeMismatch"},
	{InvalidKeyHashError,"InvalidKeyHashError"},
	{TIMNotFound,"TIMNotFound"},
	{WTMStateError,"WTMStateError"},
	{FuseRWError,"FuseRWError"},
	{InvalidOTPHashError,"InvalidOTPHashError"},
	{CRCFailedError,"CRCFailedError"},
	{DownloadPortError,"DownloadPortError"},
	{DownloadError,"DownloadError"},
	{FlashNotErasedError,"FlashNotErasedError"},
	{InvalidKeyLengthError,"InvalidKeyLengthError"},
	{DownloadImageTooBigError,"DownloadImageTooBigError"},
	{UsbPreambleError,"UsbPreambleError"},
	{UsbPreambleTimeOutError,"UsbPreambleTimeOutError"},
	{UartReadWriteTimeOutError,"UartReadWriteTimeOutError"},
	{UnknownImageError,"UnknownImageError"},
	{MessageBufferFullError,"MessageBufferFullError"},
	{NoEnumerationResponseTimeOutError,"NoEnumerationResponseTimeOutError"},
	{UnknownProtocolCmd,"UnknownProtocolCmd"},
	{JtagReEnableError,"JtagReEnableError"},
	{JtagReEnableOEMPubKeyError,"JtagReEnableOEMPubKeyError"},
	{JtagReEnableOEMSignedPassWdError,"JtagReEnableOEMSignedPassWdError"},
	{JtagReEnableTimeOutError,"JtagReEnableTimeOutError"},
	{JtagReEnableOEMKeyLengthError,"JtagReEnableOEMKeyLengthError"}
};

#endif