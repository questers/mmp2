/**************************************************************************
**
** (C) Copyright December 2006 Marvell International Ltd.
**
** All Rights Reserved.
**
**
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE   
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR  
** PURPOSE ARE EXPRESSLY DISCLAIMED.  
******************************************************************************/
///////////////////////////////////////////////////////////////////////
// FlasherBinary.h: interface for the CFlasherBinary class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FLASHERBINARY_H__6215F19D_307F_4EEA_A502_F449606ED836__INCLUDED_)
#define AFX_FLASHERBINARY_H__6215F19D_307F_4EEA_A502_F449606ED836__INCLUDED_

#include <windows.h>
#include "downloader.h"
#include <list> 


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma pack (1)
typedef struct _WM_Binary_Image_Header_t
{
	char sync_bytes[7];
	UINT32 run_time_image_address;
	UINT32 run_time_image_length;
} WM_Binary_Image_Header_t, *PWM_Binary_Image_Header_t;

typedef struct _WM_Binary_Record_header_t
{
	UINT32 record_address;
	UINT32 record_length;
	UINT32 record_checksum;
} WM_Binary_Record_header_t, *PWM_Binary_Record_header_t;
#pragma pack ()


#pragma pack (1)
typedef struct 
{
	char name[32];					// Null terminate string to uniquely identify the Linux file system image
	unsigned int type;				// type of the file system image
	unsigned int size;				// size of the image,  multiple of ( page + spare size)
	unsigned int page_size;			// the page size of image, 2048 for large page nand
	unsigned int spare_area_size;	// spare area size, 64 for large page nand
	unsigned int base_address;		// the nand base address to burn the image
	unsigned int header_checksum;	// header�s checksum to validate the header
} Customized_Image_Header_t, *PCustomized_Image_Header_t;
/* Notes: 	
	1.The name would be "Marvell_Linux_Filesystem_Image" for the Image Header.
	2. Type field has the following definition for now: 0x01-JFFS2 0x02-YAFFS2 */	
#pragma pack ()

#define MAX_NUM_OF_FLASH_BIN_SECTIONS 32

class CFlasherBinary  
{
	TCHAR *m_Filename;
	UINT16 m_Core_ID;
	UINT32 m_FlashStartAddress;
	UINT32 m_VirtualFBF_Start_Sector;
	UINT32 m_VirtualFBF_Last_Sector;
	UINT32 m_Checksum;
	UINT32 m_Length;

	HANDLE m_hFile;	
	UINT32 m_Filesize;
	UINT32 m_Commands;
	UINT32 m_FBF_Sector_Size;
	UINT32 m_image_type;
	UINT32 m_Flash_Buffer_Size;
	UINT32 m_Flash_Spare_Area_Size;
	UINT32 m_Flash_Block_Size;
	UINT32 m_Flash_Partition_Size;

	BOOL m_IsInAddressSpace;

	/* used for parsing WM Flash.bin */
	UINT32 m_Flash_Bin_section_Record_num[MAX_NUM_OF_FLASH_BIN_SECTIONS];
	UINT32 m_Flash_Bin_section_Flash_start_offset[MAX_NUM_OF_FLASH_BIN_SECTIONS];
	UINT32 m_Flash_Bin_section_length[MAX_NUM_OF_FLASH_BIN_SECTIONS];
	UINT32 m_Flash_Bin_section_checksum[MAX_NUM_OF_FLASH_BIN_SECTIONS];
	UINT32 m_Flash_Bin_section_first_fbf_sector[MAX_NUM_OF_FLASH_BIN_SECTIONS];
	UINT32 m_num_of_sections; 

private:
	void preprocessFlasherBinaryAccordingToType(void);	
	BOOL getRecordNum_and_Offset_from_FBF_Sector_num( UINT32  sector_num, 
													  UINT32  raw_record_size,
													  UINT32& record_num, 
													  UINT32& record_offset );

public:
	CFlasherBinary(TCHAR* Filename, 
					UINT16 Core_ID, 
					UINT32 FlashStartAddress,
					UINT32 FBF_Sector_Size,
					UINT32 VirtualFBF_Start_Sector = 0,
					UINT32 Commands = 0,
					UINT32 imageLength = 0,
					UINT32 Flash_Buffer_Size = 0,
					UINT32 Flash_Spare_Area_Size = 0,
					UINT32 Flash_Block_Size = 0,
					UINT32 Flash_Partition_Size = 0);

	void IsInAddressSpaceOfAnyFlashDeviceSet(BOOL IsInAddressSpace)
	{
		m_IsInAddressSpace = IsInAddressSpace;
	}

	BOOL IsInAddressSpaceOfAnyFlashDeviceGet(void)
	{
		return m_IsInAddressSpace;
	}

	// copy ctor 
	CFlasherBinary(CFlasherBinary& x);

	void CFlasherBinary::Perform_Binary_File_Detection( void );

	void SetFlashHWParameters( UINT32 Flash_Buffer_Size,
							   UINT32 Flash_Spare_Area_Size, 
							   UINT32 Flash_Block_Size);


	/* Reads the file and calculates a simple byte checksum */
	void CFlasherBinary::calculateChecksum(UINT32& checksumFormatVersion1,
										   UINT32& checksumFormatVersion2,
										   UINT32 startOffset,
										   UINT32 imageLength);	

	/* gets the checksum that has already been calculated by the constructor */
	UINT32 getChecksum();

	UINT32 getFileSize();

	virtual ~CFlasherBinary();
	

	TCHAR* getFilenamePtr()
	{
		return m_Filename;
	}
	UINT32 getCommands()
	{
		return m_Commands;
	}
	
	UINT16 getCore_ID()
	{
		return m_Core_ID;
	}
	

	UINT32 getLength()
	{
		return m_Length;

	}

	UINT32 getVirtualFBF_Start_Sector()
	{
		return m_VirtualFBF_Start_Sector;
	}

	UINT32 getVirtualFBF_Last_Sector()
	{
		return m_VirtualFBF_Last_Sector;
	}

	UINT32 getFlashStartAddress()
	{
		return m_FlashStartAddress;
	}

	bool getSector(UINT32 sector_Number, UINT8* ptr32KB_Buffer);

	void setSectorSize(UINT32 Sector_Size)
	{
		m_FBF_Sector_Size = Sector_Size;
	}
	// this function automatically updates the Last sector 
	UINT32 setVirtualFBF_Start_Sector(UINT32 VirtualFBF_First_Sector);
	void setFlashStartAddress(UINT32 FlashStartAddress)
	{
		m_FlashStartAddress = FlashStartAddress;
	}

	bool is_Sector_in_VirtualFBF_File(UINT32 sectorNumber)
	{
#if 0 
		if ( m_num_of_sections > 1 )
		{
			UINT32 VirtualFBF_Last_Sector = m_Flash_Bin_section_first_fbf_sector[m_num_of_sections-1];
			
			VirtualFBF_Last_Sector += ( m_Flash_Bin_section_length[m_num_of_sections-1]/

			if ( (sectorNumber >= m_Flash_Bin_section_first_fbf_sector[0]) && 
				 sectorNumber <= m_VirtualFBF_Last_Sector)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}
		else
		{
#endif // 0 
			if (sectorNumber >= m_VirtualFBF_Start_Sector && sectorNumber <= m_VirtualFBF_Last_Sector)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
//		}

	}	

	bool isFileInitOK()
	{
		return (m_hFile > 0);
	}

	UINT32 getFlashSpareAreaSize( void )
	{
		return m_Flash_Spare_Area_Size;
	}

	UINT32 getPartitionSize( void )
	{
		return m_Flash_Partition_Size;
	}

	UINT32 getNumOfSections( void )
	{
		return m_num_of_sections;
	}

	UINT32 getSectionFlashStartAddress( UINT32 section_num )
	{
		return m_Flash_Bin_section_Flash_start_offset[section_num];
	}

	UINT32 getSectionLength( UINT32 section_num )
	{
		return m_Flash_Bin_section_length[section_num];
	}

	UINT32 getSectionChecksum( UINT32 section_num )
	{
		return m_Flash_Bin_section_checksum[section_num];
	}

	void setSectionFirstFBFSector( UINT32 section_num, UINT32 sector_num )
	{
		m_Flash_Bin_section_first_fbf_sector[section_num] = sector_num; 
	}

	UINT32 getSectionRawLength( UINT32 section_num )
	{
		UINT32 raw_section_length = m_Flash_Bin_section_length[section_num];

		raw_section_length /= (m_Flash_Buffer_Size + m_Flash_Spare_Area_Size);
		raw_section_length *= m_Flash_Buffer_Size;

		return raw_section_length;
	}

	UINT32 getSectionRecordNum( UINT32 section_num )
	{

		return m_Flash_Bin_section_Record_num[section_num];
	}

	UINT32 getFlashBufferSize( void )
	{
		return m_Flash_Buffer_Size;
	}

};

typedef std::list<CFlasherBinary*> CFlasherBinaryList; 

#endif // !defined(AFX_FLASHERBINARY_H__6215F19D_307F_4EEA_A502_F449606ED836__INCLUDED_)
