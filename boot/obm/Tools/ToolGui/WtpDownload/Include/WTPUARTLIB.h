/**************************************************************************
**
** (C) Copyright December 2006 Marvell International Ltd.
**
** All Rights Reserved.
**
**
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE   
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR  
** PURPOSE ARE EXPRESSLY DISCLAIMED.   
**
**  FILENAME:	WTPUARTLIB.h
**
**  PURPOSE: 	Windows Dll routines 
**                  
******************************************************************************/
#pragma once

#include <WTPDEF.h>

typedef struct
{
	int iPortNumber;
	DWORD dwBaudRate;
	char EventChar;
} UartSettings;

BOOL OpenUartPort (UartSettings ComPortConfig);
BOOL WriteUart (WTPCOMMAND* WtpCmd,DWORD dwSize);
BOOL ReadUart (WTPSTATUS* WtpStatus,DWORD dwSize,PDWORD pBytesRead);
BOOL ReadUartCharBuffer (char *WtpMessage,DWORD dwSize,PDWORD pBytesRead);
BOOL UartPortPriority (int iPriority,DWORD dwPriorityClass);
void EnableUartVerboseMode (BOOL OnOff);
void EnableWaitCommEvent (BOOL OnOff);
void CloseUartPort ();
