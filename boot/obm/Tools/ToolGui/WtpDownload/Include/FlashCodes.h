/**************************************************************************
**
** (C) Copyright December 2006 Marvell International Ltd.
**
** All Rights Reserved.
**
**
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE   
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR  
** PURPOSE ARE EXPRESSLY DISCLAIMED.  
**
**  FILENAME:	FlashCodes.h
**
**  PURPOSE: 	Definition file for the flash types currently available.
**  
******************************************************************************/

#ifndef FLASHTYPE // Only make declarations once.
#define FLASHTYPE

// Flash Types
typedef enum
{
	CS0_XIP_FLASH	= 1,
	CS2_XIP_FLASH	= 2,
	NAND_FLASH		= 3,
	ONENAND_FLASH	= 4,
	MSYS_DOC_FLASH	= 5,
	SDMMC_FLASH		= 6,
	SPI_FLASH		= 7
} FlashType_T;

// Flash Fuses
typedef enum
{
	NO_FLASH_P				= 0,
	HSI_FLASH_P				= 1,
	ONENAND_FLASH_P			= 2,
	CS0_XIP_FLASH_P			= 3,
	NAND_FLASH_X16_HM_P		= 4,
	CS2_XIP_SIBLEY_P		= 5,
	NAND_FLASH_X8_HM_P		= 6,
	SDMMC_FLASH_OPT0_P		= 7,		// Used to be CS2 Intel XIP
	SDMMC_FLASH_OPT1_P		= 8,
	SDMMC_FLASH_OPT2_P		= 9,
	SPI_FLASH_P				= 10,
	SDMMC_FLASH_OPT3_P		= 11, 		// Reserved for eMMC Alternate Boot Mode from a platform specific Boot Partition, should not be included in the probe loop.
	SDMMC_FLASH_OPT4_P  	= 12,		// Reserved for eMMC Alternate Boot Mode from a platform specific Boot Partition, should not be included in the probe loop.
	NAND_FLASH_X16_BCH_P	= 13,	
	NAND_FLASH_X8_BCH_P		= 14
} FlashProbeList_T;


#ifdef __cplusplus
	extern "C" {
#endif

typedef struct
{
	BYTE FlashCode;
	char Description[50];
} FLASHCODETABLE;

#define MAXFLASHTYPES 16
static FLASHCODETABLE FlashTypeCodes[MAXFLASHTYPES] =
{
	{ NO_FLASH_P, "Probe Flash - 0x00000000"},
    { HSI_FLASH_P, "HSI1 - 0x48534901"},
    { ONENAND_FLASH_P, "x16 OneNAND - 0x4E414E02"},
    { CS0_XIP_FLASH_P, "XIP Flash x16 - 0x58495003"},
    { NAND_FLASH_X16_HM_P, "x16 NAND Hamming - 0x4E414E04"},
    { CS2_XIP_SIBLEY_P, "x16 XIP SIBLEY - 0x58495005"},
    { NAND_FLASH_X8_HM_P, "NAND x8 Hamming - 0x4E414E06"},
    { SDMMC_FLASH_OPT0_P, "SDMMC Flash Opt0 - 0x4D4D4307"},
    { SDMMC_FLASH_OPT1_P, "SDMMC Flash Opt1 - 0x4D4D4308"},
    { SDMMC_FLASH_OPT2_P, "SDMMC Flash Opt2 - 0x4D4D4309"}, 
	{ SPI_FLASH_P, "SPI Flash � 0x5350490A"},
	{ SDMMC_FLASH_OPT3_P, "SDMMC Flash Opt3 - 0x4D4D430B"},
	{ SDMMC_FLASH_OPT3_P, "SDMMC Flash Opt4 - 0x4D4D430C"},
	{ NAND_FLASH_X16_BCH_P, "x16 NAND BCH - 0x4E414E0D"},
	{ NAND_FLASH_X8_BCH_P, "x8 NAND BCH - 0x4E414E0E"},
};


#ifdef __cplusplus
	}
#endif

#endif

