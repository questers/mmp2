/**************************************************************************
**
** (C) Copyright December 2006 Marvell International Ltd.
**
** All Rights Reserved.
**
**
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE   
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR  
** PURPOSE ARE EXPRESSLY DISCLAIMED.   
**
**  FILENAME:	WTPCOMMCLASS.h
**
**  PURPOSE: 	Contains definitions for CtpComm class. 
**                  
******************************************************************************/

#pragma once

#include <WTPDEF.h>

const PROTOCOL_VERSION TARGET_PROTOCOL_VERSION = {0x01, 0x01, 0x0000}; 

class CCommandLineParser;

class CWtpComm
{
    public:
        CWtpComm ( CCommandLineParser& rCommandLineParser );
        ~CWtpComm ();
        void SetUsbPortTimeout (long int lTimeout);
        bool OpenPort ();
        void ClosePort ();
        bool SendPreamble ();
        bool GetVersion (WTPSTATUS *pWtpStatus);
        bool SelectImage (PDWORD pImageType);
        bool VerifyImage (BYTE AckOrNack);
        bool DataHeader (unsigned int uiRemainingData,WTPSTATUS *pWtpStatus);
        bool Data (PBYTE pData,long Length);
        bool DebugBoot (PBYTE pFlashType);
        bool Done ();
        bool Disconnect (WTPSTATUS *pWtpStatus);
        bool PublicKey ();
        bool Password (WTPSTATUS *pWtpStatus);
        bool SignedPassword ();
        bool GetWtpMessage (WTPSTATUS *pWtpStatus);
        bool GetTargetMessage (WTPSTATUS *pWtpStatus);
        void HandlePendingMessages( WTPSTATUS *pWtpStatus );
        bool OtpView ();
        bool WaitForHostReply (WTPSTATUS *pWtpStatus,int nCharsRead,int nTotalChars);
        void DebugViewPrint (char *szMessage);

        // features added to support upload
        bool GetProtocolVersion ( PROTOCOL_VERSION* pTargetProtocolVersion );
        bool GetParameters ( TARGET_PARAMS* pTargetParams );
        bool UploadDataHeader ( UPLOAD_DATA_PARAMS* pUploadDataParams );
        bool UploadData( PBYTE pData, unsigned int Length );
        bool HandleUploadDataPacket( PBYTE pData, unsigned int Length );

        bool UARTReadCharWithTimeout( PBYTE pData, DWORD StatusLength );

private:
        CCommandLineParser& CommandLineParser;
        BYTE iSequence;
        long int Timeout;
        char szMessage[256];
        int column;        
};

