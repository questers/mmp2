/**************************************************************************
**
** (C) Copyright December 2006 Marvell International Ltd.
**
** All Rights Reserved.
**
**
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE   
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR  
** PURPOSE ARE EXPRESSLY DISCLAIMED. 
**
**  FILENAME:	WTPUARTLIB.h
**
**  PURPOSE: 	Windows Dll routines 
**                  
******************************************************************************/
#pragma once

#include <WTPDEF.h>

BOOL OpenUsbPort (long int DelayTime, char* pInterfaceName );
BOOL GetUsbDeviceName (char *DeviceName,BOOL IsPrint);
BOOL WriteUsb (WTPCOMMAND* WtpCmd,DWORD dwCmdSize,WTPSTATUS *WtpStatus,DWORD dwStatusSize);
void CloseUsbPort ();

BOOL WriteUsbNoResp (PBYTE pData,DWORD dwCmdSize); // Added to do the same as WriteUsb but expect no response.
BOOL ReadUsb (WTPSTATUS *WtpStatus,DWORD dwStatusSize);//Added to read response.
DWORD GetIoControlCode (BYTE CMD);
