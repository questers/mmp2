/**************************************************************************
**
** (C) Copyright December 2006 Marvell International Ltd.
**
** All Rights Reserved.
**
**
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE   
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR  
** PURPOSE ARE EXPRESSLY DISCLAIMED. 
**
**  FILENAME:	WTPDEF.h
**
**  PURPOSE: 	Definition file for the WTPTP software tools..
**  
******************************************************************************/

#pragma once

#ifdef WINDOWS
#undef FAILED
#include "../../Common/Typedef.h"
#endif

#include <string>
using namespace std;

//#define USE_LOG_FILE 0

//#if USE_LOG_FILE
//#define WTP_PRINT(log, string, ...) fprintf(log, string, __VA_ARGS__)
//#else
//#define WTP_PRINT(log, string, ...) printf(string, __VA_ARGS__)
//#endif


#define DATALENGTH    8192 //(4096*2)-8+1
//#define RETURNDATALENGTH (4096)-8+1
#define RETURNDATALENGTH (1024*8)-6
#define MESSAGELENGTH 256
//#define WTPSTATUSLENGTH 3996

#ifndef _WINDEF_
typedef unsigned char BYTE;
#endif

#define MIN_ARGS 2
#define MAX_PATH 260
#define MAX_IMAGE_FILES 20

typedef struct
{
    DWORD FileType;
    char FileName[MAX_PATH];
    char ShortFileName[MAX_PATH];
} IMAGEFILE;

typedef enum {
   DISABLED = 1,		// Standard WTPTP download
   MSG_ONLY = 2,		//Turn on message mode(listen for msgs forever)
   MSG_AND_DOWNLOAD = 3 //Msg mode and WTPTP download
} eMessageMode;

#define USB 0
#define UART 1
#define UNKNOWNPORT -1

//define PACKET_SIZE = 504;  // plus command makes 512
#define PACKET_SIZE  4088;  // plus command makes 4096

#define ACK 0
#define NACK 1
#define MESSAGEPENDING 1
#define MESSAGESTRING 0
#define MESSAGEERRORCODE 1

#define GET           0      //added for fast download proj--flag for fast download.
#define SET           1      //added for fast download proj--flag for fast download.
#define FDMODE        1      //dummy/default value when getting the value for fast download.

typedef enum {
   IMAGE_DOWNLOAD_ERROR = 1,		// Download of binary failed
   } WTPTP_ERRORS;

// Protocol command definitions
#define PREAMBLE 0x00
#define PUBLICKEY 0x24
#define PASSWORD 0x28
#define SIGNEDPASSWORD 0x25
#define GETVERSION 0x20
#define SELECTIMAGE 0x26
#define VERIFYIMAGE 0x27
#define DATAHEADER 0x2A
#define DATA 0x22
#define MESSAGE 0x2B
#define OTPVIEW 0x2C
#define DEBUGBOOT 0x2D
#define DONE 0x30
#define DISCONNECT 0x31
#define UPLOADDATAHEADER 0x32
#define UPLOADDATA 0x33
#define PROTOCOLVERSION 0x34
#define GETPARAMETERS 0x35
#define UNKNOWNCOMMAND 0xFF

#pragma pack(push,1)
typedef struct
{
    BYTE CMD;
    BYTE SEQ;
    BYTE CID;
    BYTE Flags;
    long LEN;
    BYTE Data[DATALENGTH];
} WTPCOMMAND;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{
    BYTE CMD;
    BYTE SEQ;
    BYTE CID;
    BYTE Status;
    BYTE Flags;
    BYTE DLEN;
    BYTE Data[RETURNDATALENGTH];
} WTPSTATUS;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{
    BYTE MajorVersion;
    BYTE MinorVersion;
    short Build;
} PROTOCOL_VERSION; 
#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{
    unsigned int BufferSize;
    unsigned int Rsvd1;
    unsigned int Rsvd2;
    unsigned int Rsvd3;
} TARGET_PARAMS; 
#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{
    BYTE Type;
    BYTE SubType;
    BYTE Partition;
    BYTE Rsvd1;
    unsigned int Offset;
    unsigned int DataSize;
} UPLOAD_DATA_PARAMS; 
#pragma pack(pop)

#pragma pack(push,1)
typedef struct
{
    UPLOAD_DATA_PARAMS Params;
    string sOutputFileName;
    string sComparisonFileName;
} UPLOAD_DATA_SPEC; 
#pragma pack(pop)

