
#ifndef _WTP_LOG_H
#define _WTP_LOG_H
#include <stdio.h>
#undef WARN
#undef ERROR
#undef DBG
#undef INFO

class WtpLogSetting 
{

public:
	enum
	{
		LOG_WARN			=0x1,
		LOG_INFO			=0x2,
		LOG_ERROR			=0x4,
		LOG_ALWAYS			=0x8,
		LOG_DEBUG			=0x800,	
	};
	
    void setLevel(unsigned int level);
	unsigned int getLevel(){return mLogLevel;}

	static WtpLogSetting& getInstance();
	
	protected:
   		WtpLogSetting();
		static const int DEFAULT_LEVEL=LOG_WARN|LOG_ERROR|LOG_ALWAYS;

	private:		
		WtpLogSetting(const WtpLogSetting&);
		WtpLogSetting& operator = (const WtpLogSetting&);
		unsigned int mLogLevel;
		unsigned int mEnabled;

		static WtpLogSetting* singleton;		
		

};

class WtpLog
{
	public:
	static int LOG(const char* fmt,...);
	static WtpLog& getInstance();
	protected:
   		WtpLog();
		~WtpLog();
		static void Init();
		
	private:
		static WtpLog* singleton;
		bool bInit;
		FILE* hLog;
};

#define WARN(...) \
	do{if(WtpLogSetting::getInstance().getLevel()&WtpLogSetting::LOG_WARN) WtpLog::LOG(__VA_ARGS__);}while(0)

#define INFO(...) \
	do{if(WtpLogSetting::getInstance().getLevel()&WtpLogSetting::LOG_INFO) WtpLog::LOG(__VA_ARGS__);}while(0)

#define ERROR(...) \
	do{if(WtpLogSetting::getInstance().getLevel()&WtpLogSetting::LOG_ERROR) WtpLog::LOG(__VA_ARGS__);}while(0)

#define DBG(...) \
	do{if(WtpLogSetting::getInstance().getLevel()&WtpLogSetting::LOG_DEBUG) WtpLog::LOG(__VA_ARGS__);}while(0)

#define ALWAYS(...) \
	do{WtpLog::LOG(__VA_ARGS__);}while(0)


#endif 

