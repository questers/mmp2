/**************************************************************************
**
** (C) Copyright December 2006 Marvell International Ltd.
**
** All Rights Reserved.
**
**
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR
** PURPOSE ARE EXPRESSLY DISCLAIMED. 
**
**  FILENAME:   WtpDownload.h
**
**  PURPOSE:    The main purpose of this file is to share global variables
**              controled by the main program but shared with any other project
**              file if need be.
**
******************************************************************************/
#pragma once

#include "CommandLineParser.h"

CCommandLineParser CommandLineParser;
CWtpComm WtpComm( CommandLineParser );

//int FileIndex = 0;
static TCHAR szFBFFileName[MAX_PATH];
DWORD PrevImageType = 0;
extern FILE *hLog;
bool retry = false;
int column = 0;

typedef struct
{
	MasterBlockHeader MBH;
	DeviceHeader_V2	  DH[MAX_NUMBER_OF_FLASH_DEVICES];
} MasterBlockHeaderAndDeviceHeaders;

static MasterBlockHeaderAndDeviceHeaders FBF_MBH_DH;

bool IsScriptFile (int argc,char* argv[],char *ScriptFileName,bool *IsScript);
bool ParseScriptFile (char *ScriptFileName);
bool GetParameter (char *fLine,char *pParameter,char *pDec,bool IsFile);
bool CheckYourOptions ();
BYTE AckWithImageName (DWORD ImageType,TCHAR *szFileName);
bool DownloadImage (int iFile);
bool DownloadMasterBlockHeader();
bool DownloadFBFImage();
void DebugViewPrint (char *szMessage);

void UserMessagePrintStr( bool bVerbose, char* pMsg, char* pVar );
void UserMessagePrintInt( bool bVerbose, char* pMsg, int iVar );
void SendingDone();
bool DownloadImages( int iNumFiles, bool bWithJTagEnable );

//bool isFastDownload(BOOL mode, BOOL value);
bool isLastData(bool mode, bool value);

bool DoUploads( TARGET_PARAMS* pTargetParams );
bool ParseUploadSpecFile( list<UPLOAD_DATA_SPEC*>* pUploadSpecs );
bool SaveUploadToFile( BYTE* pUploadDataBuffer, UPLOAD_DATA_SPEC* pUploadSpec );
bool CompareUploadToFile( BYTE* pUploadDataBuffer, UPLOAD_DATA_SPEC* pUploadSpec );


#if TRUSTED
bool JtagReEnable ();
extern int ReadKeyFile(FILE *kFile, unsigned char *pPubKey,unsigned char *pPrivKey, unsigned char *pModulus, unsigned int *pKeylength);
#endif

bool OtpView ();
bool DebugBoot ();
