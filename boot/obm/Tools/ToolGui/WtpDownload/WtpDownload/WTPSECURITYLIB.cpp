/**************************************************************************
**
** (C) Copyright December 2006 Marvell International Ltd.
**
** All Rights Reserved.
**
**
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE   
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR  
** PURPOSE ARE EXPRESSLY DISCLAIMED.   
**
**  FILENAME:	WTPSECURITYLIB.c
**
**  PURPOSE: 	Windows Dll routines 
**                  
******************************************************************************/
#include <stdafx.h>

#include <WTPSECURITYLIB.h>

#pragma warning ( disable : 4244 4715 4047 4024 4996) // Disable warning messages

#if TRUSTED
#if IPPV6
#include <ipp_px.h>
#endif
#include <ippcp.h>
#endif

#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }

void Endian_Convert (unsigned char *first)
{
	unsigned char tmp = *first;

	*first = *(first + 3);
	*(first + 3) = tmp;
	tmp = *(first + 1);
	*(first + 1) = *(first + 2);
	*(first + 2) = tmp;
}

#if TRUSTED
void PadPasswordToSign (unsigned int *Password,unsigned int KeyLength) 
{
	unsigned int i;

	for (i = 2; i < (KeyLength - 1); i++)
	{
		Password[i] = 0x00000000;
	}
	Password[(KeyLength - 1)] = 0x00000000;
}

void Sha1Digest (unsigned char *pMesg,int len,unsigned char *pMD) 
{
	IppStatus status;
 
	status = ippsSHA1MessageDigest (pMesg,len,pMD);

	if (status != ippStsNoErr) 
		printf ("  Error: SHA1 fail\n");
}
 
int ReadKeyFile (FILE *kFile,unsigned char *pPubKey,unsigned char *pPrivKey,
                 unsigned char *pModulus,unsigned int *pKeyLength)
{
	unsigned int    i;	
	unsigned int    KeyLength = 0;
	unsigned int    FlashType = 0;
	unsigned int	IssueDate;
	unsigned int	OEMUniqueID;
	unsigned int	HashValueLength;
	unsigned int	RSAPrivateKey[64];
	unsigned int	RSAPublicKey[64];
	unsigned int	RSAModulus[64];
	FILE			*RSAKey;

	RSAKey = kFile;

	// Get RSA key file, scan for fields
	if (fscanf (RSAKey, "RSA Key Length: %d\n", &KeyLength) != 1)
		return KeyLengthReadError;
	printf ("KeyLength is: %d\n", KeyLength);
		
	if (fscanf (RSAKey, "Flash Type: 0x%08x\n", &FlashType) != 1)
		return FlashTypeReadError;
	printf ("FlashType is: 0x%08x\n", FlashType);
	
	if (fscanf (RSAKey, "Issue Date: 0x%x\n", &IssueDate) != 1)
		return IssueDateReadError;
	printf ("IssueDate is: 0x%08x\n", IssueDate);
	
	if (fscanf (RSAKey, "OEM UniqueID: 0x%08x\n", &OEMUniqueID) != 1)
		return OEMUniqueIDReadError;
	printf ("OEMUniqueID is: 0x%08x\n", OEMUniqueID);

	if (fscanf (RSAKey, "Hash Value Length: %d\n", (int)&HashValueLength) != 1)
		return HashValueLengthReadError;
	printf ("HashValueLength is: %d\n", HashValueLength);
	
	fscanf (RSAKey, "RSA Public Key:\n");

	for (i = 0; i < KeyLength / 4; i++) 
	{
		if (fscanf (RSAKey,"0x%x 0x%x 0x%x 0x%x\n",&RSAPublicKey[4 * i],
					&RSAPublicKey[4 * i + 1],&RSAPublicKey[4 * i + 2],
					&RSAPublicKey[4 * i + 3]) != 4)
			return RSAExponentReadError;

		printf ("pubk: 0x%08x 0x%08x 0x%08x 0x%08x\n",RSAPublicKey[4*i],RSAPublicKey[4*i+1],RSAPublicKey[4*i+2],RSAPublicKey[4*i+3]);
	}

	fscanf (RSAKey, "RSA Private Key:\n");

	for (i = 0; i < KeyLength / 4; i++) 
	{
		if (fscanf (RSAKey,"0x%x 0x%x 0x%x 0x%x\n",&RSAPrivateKey[4 * i],
					&RSAPrivateKey[4 * i + 1],&RSAPrivateKey[4 * i + 2],
					&RSAPrivateKey[4 * i + 3]) != 4)
			return RSAPrivateKeyReadError;

		printf ("prik: 0x%08x 0x%08x 0x%08x 0x%08x\n",RSAPrivateKey[4*i],RSAPrivateKey[4*i+1],RSAPrivateKey[4*i+2],RSAPrivateKey[4*i+3]);
	}

	fscanf (RSAKey, "RSA System Modulus:\n");

	for (i = 0; i < KeyLength / 4; i++) 
	{
		if (fscanf (RSAKey,"0x%x 0x%x 0x%x 0x%x\n",&RSAModulus[4 * i],
					&RSAModulus[4 * i + 1],&RSAModulus[4 * i + 2],
					&RSAModulus[4 * i + 3]) != 4)
			return RSAModulusReadError;

		printf ("modu: 0x%08x 0x%08x 0x%08x 0x%08x\n",RSAModulus[4*i],RSAModulus[4*i+1],RSAModulus[4*i+2],RSAModulus[4*i+3]);
	}

	fclose (RSAKey);
	*pKeyLength = KeyLength;
	memset (pPubKey, 0, 256);
	memset (pPrivKey, 0, 256);
	memset (pModulus, 0, 256);
	memcpy (pPubKey, RSAPublicKey, 4 * KeyLength);
	memcpy (pPrivKey, RSAPrivateKey, 4 * KeyLength);
	memcpy (pModulus, RSAModulus, 4 * KeyLength);
		
	return NoError;
}

/**********************************************************************
 * RSA - 
 * API:
 *       void RSA(unsigned long PubKey, unsigned long *pModulus, int modlen,
 *                unsigned long *pInput, unsigned long *pOutput)
 * Input:
 *       PubKey - RSA public key (1-word fixed length)
 *       *pModulus - RSA modulus
 *       modlen - modulus length (in number of words)
 *       *pInput - input data stream (assume its length is same as modlen)
 * Output: 
 *       *pOutput - output data stream (same length as modlen)
 ***********************************************************************/
BOOL RSA (unsigned int *PubKey,unsigned int *pModulus,int modlen,
          unsigned int *pInput,unsigned int *pOutput)
{
	int kn = 4096, kp = 2048, size, length = 128;
	IppsBigNumSGN sgn;
	IppsRSAState *key;
	IppsBigNumState *x, *y;
	IppStatus Status;

	Status = ippsRSAGetSize (kn,kp,IppRSApublic,&size);
	if (Status != ippStsNoErr)
	{
		printf ("////////////////////////////////////////////////////////////\n");
		printf ("  Error: EncryptSignature Error:\n");
		printf ("ippsRSAGetSize has failed!\n");
		printf ("////////////////////////////////////////////////////////////\n");
		return FALSE;
	}
	key = (IppsRSAState *)malloc (size);

	Status = ippsRSAInit (kn,kp,IppRSApublic,key);
	if (Status != ippStsNoErr)
	{
		printf ("////////////////////////////////////////////////////////////\n");
		printf ("  Error: EncryptSignature Error:\n");
		printf ("ippsRSAInit has failed!\n");
		printf ("////////////////////////////////////////////////////////////\n");
		if (key != NULL) free (key);
		return FALSE;
	}
	Status = ippsBigNumGetSize (length,&size);
	if (Status != ippStsNoErr)
	{
		printf ("////////////////////////////////////////////////////////////\n");
		printf ("  Error: EncryptSignature Error:\n");
		printf ("ippsBigNumGetSize has failed!\n");
		printf ("////////////////////////////////////////////////////////////\n");
		if (key != NULL) free (key);
		return FALSE;
	}

	x = (IppsBigNumState *)malloc (size);
	y = (IppsBigNumState *)malloc (size);

	Status = ippsBigNumInit (length,x);
	if (Status != ippStsNoErr)
	{
		printf ("////////////////////////////////////////////////////////////\n");
		printf ("  Error: EncryptSignature Error:\n");
		printf ("ippsBigNumInit has failed!\n");
		printf ("////////////////////////////////////////////////////////////\n");
		if (key != NULL) free (key);
		if (x != NULL) free (x);
		if (y != NULL) free (y);
		return FALSE;
	}
	Status = ippsBigNumInit (length,y);
	if (Status != ippStsNoErr)
	{
		printf ("////////////////////////////////////////////////////////////\n");
		printf ("  Error: EncryptSignature Error:\n");
		printf ("ippsBigNumInit has failed!\n");
		printf ("////////////////////////////////////////////////////////////\n");
		if (key != NULL) free (key);
		if (x != NULL) free (x);
		if (y != NULL) free (y);
		return FALSE;
	}

	Status = ippsSet_BN (IppsBigNumPOS,modlen,(Ipp32u *)pModulus,x);

	Status = ippsRSASetKey (x,IppRSAkeyN,key);
	if (Status != ippStsNoErr)
	{
		printf ("////////////////////////////////////////////////////////////\n");
		printf ("  Error: EncryptSignature Error:\n");
		printf ("ippsRSASetKey has failed!\n");
		printf ("////////////////////////////////////////////////////////////\n");
		if (key != NULL) free (key);
		if (x != NULL) free (x);
		if (y != NULL) free (y);
		return FALSE;
	}
	Status = ippsSet_BN (IppsBigNumPOS,modlen,(Ipp32u *)PubKey,x);
	Status = ippsRSASetKey (x,IppRSAkeyE,key);
	if (Status != ippStsNoErr)
	{
		printf ("////////////////////////////////////////////////////////////\n");
		printf ("  Error: EncryptSignature Error:\n");
		printf ("ippsRSASetKey has failed!\n");
		printf ("////////////////////////////////////////////////////////////\n");
		if (key != NULL) free (key);
		if (x != NULL) free (x);
		if (y != NULL) free (y);
		return FALSE;
	}
	Status = ippsSet_BN (IppsBigNumPOS,/*modlen*/2,(Ipp32u *)pInput,x);

	Status = ippsRSAEncrypt (x,y,key);
	if (Status != ippStsNoErr)
	{
		printf ("////////////////////////////////////////////////////////////\n");
		printf ("  Error: EncryptSignature Error:\n");
		printf ("ippsRSAEncrypt has failed!\n");
		printf ("////////////////////////////////////////////////////////////\n");
		if (key != NULL) free (key);
		if (x != NULL) free (x);
		if (y != NULL) free (y);
		return FALSE;
	}

	Status = ippsGet_BN (&sgn,&length,(Ipp32u *)pOutput,y);
	if (Status != ippStsNoErr)
	{
		printf ("////////////////////////////////////////////////////////////\n");
		printf ("  Error: EncryptSignature Error:\n");
		printf ("ippsGet_BN has failed!\n");
		printf ("////////////////////////////////////////////////////////////\n");
		if (key != NULL) free (key);
		if (x != NULL) free (x);
		if (y != NULL) free (y);
		return FALSE;
	}

	if (key != NULL) free (key);
	if (x != NULL) free (x);
	if (y != NULL) free (y);
	return TRUE;
}
#endif
