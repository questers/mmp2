/**************************************************************************
**
** (C) Copyright December 2006 Marvell International Ltd.
**
** All Rights Reserved.
**
*
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR
** PURPOSE ARE EXPRESSLY DISCLAIMED.
**
**  FILENAME:   WtpDownload.cpp
**
**  PURPOSE:    Provides download capabilities either thru the USB port or
**              the UART port. This is the command line version.
**
******************************************************************************/

#include <stdafx.h>

#pragma warning ( disable : 4800 4996) // Disable warning messages

#include <WTPCOMMCLASS.h>
#include <WTPSECURITYLIB.h>
#include <Downloader.h>
#include <WtpDownload.h>
#include <FlashCodes.h>
#include "../../common/tim.h"

#include <fstream>
#include <strstream>
#include <sstream>
#include <../../common/TimLib.h>
#include <WtpLog.h>

#define PlatformDisconnect	0xC


#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }

#define SLE_TESTING 0
#define MIN_ARGS 2

void dump_array_with_interval(char* prefix,unsigned char* a,int length,int interval);


bool DownloadImage (int iFile)
{
    WTPCOMMAND WtpCmd;
    WTPSTATUS WtpStatus = {0x00, 0x00, 0x00, 0x00, 0x01, 0x00};
    DWORD ImageType = 0;
    BYTE IsImage = NACK;
    unsigned int uiRemainingBytes = 0xAA55FF00;
    TCHAR szFileName[MAX_PATH];
    long lFileSize = 0,BytesRead;
    FILE* hFile;
    int i = 0;

    INFO( "\nSending preamble...\n");

    if (WtpComm.SendPreamble() == false) return FALSE;

    if ( CommandLineParser.CurrentMessageMode() == MSG_AND_DOWNLOAD )
    {
        WtpStatus.Status = ACK;
        WtpComm.GetTargetMessage(&WtpStatus);
        WtpStatus.Flags = 0x0;
    }

    INFO( "\nSending GetVersion command...\n");

    if (WtpComm.GetVersion(&WtpStatus) == false) return FALSE;

    INFO( "\nSending SelectImage command...\n");

    if (WtpComm.SelectImage(&ImageType) == false) return FALSE;

    INFO( "\nImage Type: 0x%08x", ImageType );

    PrevImageType = ImageType;
//    if (!IsTim) ImageType = OBMI + iFile;
    IsImage = AckWithImageName(ImageType,szFileName);

    if (IsImage == ACK)
        INFO( "\nDownloading file: %s\n", szFileName );
    else
    {
        if ( ImageType == PARTIONIDENTIFIER )
        {
            retry = true;
            ALWAYS("  Error: Don't have a PartitionTable image asked for by target: 0x%08x\n", ImageType );
            ALWAYS("  Error: Continuing with next image\n");
        }
        else 
            ALWAYS("  Error: Don't have image type asked for by target: 0x%08x\n",ImageType );
    }

    INFO( "\nSending VerifyImage command...\n");

    if (WtpComm.VerifyImage(IsImage) == false)
    {
        if ( ImageType != PARTIONIDENTIFIER )
            ALWAYS("  Error: Download file number %d has aborted!\n", iFile + 1 );
    
        SendingDone();
        return FALSE;
    }

    if ((hFile = fopen (szFileName,"rb")) == NULL)
    {
        ALWAYS("  Error: fopen failed to open %s.\n", szFileName );
        return FALSE;
    }

    fseek (hFile,0L,SEEK_END); // Set position to EOF
    lFileSize = ftell (hFile); // Get position of file, thus the file size.

    if ( ((ImageType & TYPEMASK) == (TIMIDENTIFIER & TYPEMASK)) || (ImageType == PARTIONIDENTIFIER))
    {
        fseek (hFile,0L,SEEK_SET); // Set position to SOF
        uiRemainingBytes = (unsigned int)lFileSize;
    }
    else
    {
        fseek (hFile,4L,SEEK_SET); // Set position to SOF
        uiRemainingBytes = (unsigned int)lFileSize - 4;
    }

/*----------------------------------------------------------------------------
 * NOTE: DataHeader() needs to be called only once before data transfer.
 */
	INFO( "Sending DataHeader command...\n");

    if (WtpComm.DataHeader(uiRemainingBytes,&WtpStatus) == false)
    {
        ALWAYS("  Error: WtpComm.DataHeader failed to receive an ACK!\n");

        if (hFile != NULL) fclose (hFile);
        return FALSE;
    }
    if (WtpStatus.Flags & 0x4 && CommandLineParser.FastDownload() )
    {
        ALWAYS("\n*******NOTE:: Fast Download is NOW activated!\n");
    }
    else 
        // target does not support FastDownload
        CommandLineParser.FastDownload(false);

    int loop =0;
    unsigned int uiTotalBytes = uiRemainingBytes;
    while (uiRemainingBytes > 0)
    {
        if ( !CommandLineParser.FastDownload() && loop )
        {
             INFO( "Sending DataHeader command...\n");
             if (WtpComm.DataHeader(uiRemainingBytes,&WtpStatus) == false)
             {
                 ALWAYS("  Error: WtpComm.DataHeader failed to receive an ACK!\n");

                 if (hFile != NULL) fclose (hFile);
                 return FALSE;
             }
        }
        loop ++;
        unsigned int uiBufferSize = 0;

        for (int i = 0; i < (int)WtpStatus.DLEN; i++)
               uiBufferSize |= WtpStatus.Data[i] << (8 * i);

        if(uiBufferSize > uiRemainingBytes) uiBufferSize = uiRemainingBytes;
        uiRemainingBytes -= uiBufferSize; // Calculate remaining bytes

        
        /*
        INFO( "Buffer size = %u, ", uiBufferSize );
        INFO( "Bytes remaining = %u\n", uiRemainingBytes );*/
        char step_indicator;
        if(0==loop%4) step_indicator='|';
        if(1==loop%4) step_indicator='/';
        if(2==loop%4) step_indicator='-';
        if(3==loop%4) step_indicator='\\';
        if(uiRemainingBytes)
        {
            ALWAYS("Downloading file %s %c %d%%  \r",szFileName,step_indicator,((uiTotalBytes-uiRemainingBytes)*100)/uiTotalBytes);
        }
        else
        {
            ALWAYS("Downloading file %s    %d%%  \r\n",szFileName,100);
            
        }
            

        if ((BytesRead = fread (&WtpCmd.Data,1,uiBufferSize,hFile)) != (long)uiBufferSize)
        {
            ALWAYS( "  Error: fread only read %ld, ", BytesRead );
            ALWAYS(  "should've read %ld\n", uiBufferSize );

            if (hFile != NULL) fclose (hFile);
            return FALSE;
        }
        else
        {

            if (CommandLineParser.PortType() == USB)
            {
                unsigned int totalSize = 0;
                unsigned int mark = 0;

                // We do not split packets based on packetSize for FastDownload
                unsigned int packetSize = CommandLineParser.USBPacketSize();
                totalSize = uiBufferSize;
                if (totalSize > packetSize )
                {
                    do
                    {
                         if (totalSize <= packetSize)
                        {
                            if ( CommandLineParser.FastDownload() )
                            {  /* Code to see if last data */
                                if(uiRemainingBytes == 0)
                                      isLastData(SET, true);
                            }
                            packetSize = totalSize;
                        }

                        INFO( "Sending data block with buffer size of %d...\n", packetSize );
                        
                        if (WtpComm.Data(&(WtpCmd.Data[mark]),packetSize) == false)
                        {
                            ALWAYS( "  Error: WtpComm.Data failed to receive an ACK!\n");

                            if (hFile != NULL) fclose (hFile);
                            return FALSE;
                        }

                        mark += packetSize;
                        totalSize-=packetSize;

                    }while (totalSize > 0);

                }
                else
                {
                    if ( CommandLineParser.FastDownload() )
                    {  /* Code to see if last data */
                        if(uiRemainingBytes == 0)
                              isLastData(SET, true);
                    }
                    if (WtpComm.Data(WtpCmd.Data,totalSize) == false)
                    {
                        ALWAYS("  Error: WtpComm.Data failed to receive an ACK!\n");

                        if (hFile != NULL) fclose (hFile);
                        return FALSE;
                    }
                }
            }
            else if (CommandLineParser.PortType() == UART)
            {
                INFO( "Sending data block with buffer size of %d...\n", uiBufferSize );

                if (WtpComm.Data(WtpCmd.Data,uiBufferSize) == false)
                {
                    ALWAYS("  Error: WtpComm.Data failed to receive an ACK!\n");
                    
                    if (hFile != NULL) fclose (hFile);
                    return FALSE;
                }
            }
        }
    }

    SendingDone();

    if ( CommandLineParser.FastDownload() )
        isLastData (SET, false);

    if (hFile != NULL) fclose (hFile);

    return TRUE;
}


bool DownloadMasterBlockHeader()
{
    WTPCOMMAND WtpCmd;
    WTPSTATUS WtpStatus = {0x00, 0x00, 0x00, 0x00, 0x01, 0x00};
    DWORD ImageType;
    BYTE IsImage = NACK;
    unsigned int uiRemainingBytes = 0xAA55FF00;
    TCHAR szFileName[MAX_PATH];
    long lFileSize = 0;
    FILE* hFile;
    int i = 0;

    MasterBlockHeaderAndDeviceHeaders* pMBH_DH = &FBF_MBH_DH;

    INFO( "\nSending preamble...\n");

    if (WtpComm.SendPreamble() == false) return FALSE;
    if ( CommandLineParser.CurrentMessageMode() == MSG_AND_DOWNLOAD )
    {
        WtpStatus.Status = ACK;
        WtpComm.GetTargetMessage(&WtpStatus);
        WtpStatus.Flags = 0x0;
    }

    INFO( "\nSending GetVersion command...\n");
    if (WtpComm.GetVersion(&WtpStatus) == false) return FALSE;

    INFO( "\nSending SelectImage command...\n");
    if (WtpComm.SelectImage(&ImageType) == false) return FALSE;

    INFO( "Image Type: 0x%08x\n", ImageType );
    IsImage = AckWithImageName(ImageType,szFileName);
    strcpy( szFBFFileName, szFileName );
    if (IsImage == ACK)
        ALWAYS("Downloading file: %s\n", szFileName );
    else
        ALWAYS("  Error: Don't have image type asked for by target: 0x%08x\n", ImageType );

    INFO( "\nSending VerifyImage command...\n");
    if (WtpComm.VerifyImage(IsImage) == false)
    {
        ALWAYS("  Error: Download FBF MasterBlockHeader has aborted!\n");

        SendingDone();
        return FALSE;
    }

    if ((hFile = fopen (szFileName,"rb")) == NULL)
    {
        ALWAYS("  Error: fopen failed to open %s.\n", szFileName );
        return FALSE;
    }

    if (fread (pMBH_DH,1,sizeof(MasterBlockHeader),hFile) != (long)sizeof(MasterBlockHeader) )
    {
        ALWAYS("  Error: Error reading Master Block Header from %s.\n", szFileName );
        return FALSE;
    }
    
    if (fread ((void*)((int)pMBH_DH + sizeof(MasterBlockHeader)),1,FBF_MBH_DH.MBH.nOfDevices * sizeof(DeviceHeader_V2),hFile) != (long)FBF_MBH_DH.MBH.nOfDevices * sizeof(DeviceHeader_V2) )
    {
        ALWAYS("  Error: Error reading Device Headers from %s.\n", szFileName );
        return FALSE;
    }

    uiRemainingBytes = sizeof(MasterBlockHeader) + FBF_MBH_DH.MBH.nOfDevices * sizeof(DeviceHeader_V2);

    ALWAYS("  MasterBlockHeader and Device Headers size %d loading...\n", uiRemainingBytes );
    
    
    INFO( "Sending DataHeader command...\n");

    if (WtpComm.DataHeader(uiRemainingBytes,&WtpStatus) == false)
    {
        ALWAYS("  Error: WtpComm.DataHeader failed to receive an ACK!\n");

        if (hFile != NULL) fclose (hFile);
        return FALSE;
    }
    if (WtpStatus.Flags & 0x4 && CommandLineParser.FastDownload() )
    {
        ALWAYS(" NOTE:: Fast Download is activated!\n");
    }
    else
        // target does not support FastDownload
        CommandLineParser.FastDownload(false);

    int loop =0;
    while (uiRemainingBytes > 0)
    {
        
        if (!CommandLineParser.FastDownload() && loop )
        {
             INFO( "Sending DataHeader command...\n");
             if (WtpComm.DataHeader(uiRemainingBytes,&WtpStatus) == false)
             {
                 ALWAYS("  Error: WtpComm.DataHeader failed to receive an ACK!\n");

                 if (hFile != NULL) fclose (hFile);
                 return FALSE;
             }
        }
        loop ++;
        unsigned int uiBufferSize = 0;
        
        for (int i = 0; i < (int)WtpStatus.DLEN; i++)
            uiBufferSize |= WtpStatus.Data[i] << (8 * i);

        if(uiBufferSize > uiRemainingBytes) uiBufferSize = uiRemainingBytes;
        uiRemainingBytes -= uiBufferSize; // Calculate remaining bytes

        INFO( "Buffer size = %u, ", uiBufferSize );
        INFO( "Bytes remaining = %u\n", uiRemainingBytes );
                                                          
        memcpy( &WtpCmd.Data, pMBH_DH, uiBufferSize );
        pMBH_DH = (MasterBlockHeaderAndDeviceHeaders*)((int)pMBH_DH + uiBufferSize);
      

        INFO( "Sending data block with buffer size of %d...\n", uiBufferSize );

        short totalSize=0;
        unsigned int mark = 0;
        
        // We do not split packets based on packetSize for FastDownload.
        short packetSize = CommandLineParser.USBPacketSize();
        totalSize = uiBufferSize;
        if (totalSize > packetSize )
        {
            do
            {
                if (totalSize <= packetSize)
                {
                    packetSize = totalSize;
                    if ( CommandLineParser.FastDownload() )
                    {  /* Code to see if last data */
                        if(uiRemainingBytes == 0)
                        {
                            INFO( "LAST PACKET:: --------------------Bytes remaining = %u\n", uiRemainingBytes );  
                            isLastData(SET, true);
                        }
                    }
                }
                INFO( "Sending data block with buffer size of %d...\n", packetSize );

                if (WtpComm.Data(&(WtpCmd.Data[mark]),packetSize) == false)
                {
                    ALWAYS("  Error: WtpComm.Data failed to receive an ACK!\n");

                    if (hFile != NULL) fclose (hFile);
                    return FALSE;
                }

                totalSize -= packetSize;
                mark += packetSize;

            }while (totalSize > 0);
        }
        else
        {
            if ( CommandLineParser.FastDownload() )
            {  /* Code to see if last data */
                if(uiRemainingBytes == 0)
                {
                    INFO( "LAST PACKET:: --------------------Bytes remaining = %u\n", uiRemainingBytes );  
                    isLastData(SET, true);
                }
            }

            if (WtpComm.Data(WtpCmd.Data,uiBufferSize) == false)
            {
                ALWAYS("  Error: WtpComm.Data failed to receive an ACK!\n");

                if (hFile != NULL) fclose (hFile);
                return FALSE;
            }
        }
     
        
    }

    INFO( "MBH Format Version: %d\n", FBF_MBH_DH.MBH.Format_Version );
    INFO( "Number of Devices: %d\n", FBF_MBH_DH.MBH.nOfDevices );

    SendingDone();

    if ( CommandLineParser.FastDownload() )
        isLastData (SET, false);

    if (hFile != NULL) fclose (hFile);
    return TRUE;
}

bool DownloadFBFImage()
{
    WTPCOMMAND WtpCmd;
    WTPSTATUS WtpStatus = {0x00, 0x00, 0x00, 0x00, 0x01, 0x00};
    DWORD DeviceImageNums = 0;
    UINT DeviceNum = 0;
    UINT ImageNum = 0;
    BYTE IsImage = NACK;
    unsigned int uiRemainingBytes = 0xAA55FF00;
    TCHAR szFileName[MAX_PATH];
    long lImageSize = 0,BytesRead = 0;
    FILE* hFile = 0;
    int i = 0;
    PDeviceHeader pDeviceHeader = 0;
    PImageStruct pImageStruct = 0;
    long ImageStartByte = 0L;

#define DEV_IMAGE_NUMS_EXTRACT( devimgnums ) DeviceNum = (devimgnums & 0xFFFF0000)>>16; ImageNum = devimgnums & 0xFFFF;

    INFO( "\nSending preamble...\n");
    if (WtpComm.SendPreamble() == false) return FALSE;
    if ( CommandLineParser.CurrentMessageMode() == MSG_AND_DOWNLOAD )
    {
        WtpStatus.Status = ACK;
        WtpComm.GetTargetMessage(&WtpStatus);
        WtpStatus.Flags = 0x0;
    }

    INFO( "\nSending GetVersion command...\n");
    if (WtpComm.GetVersion(&WtpStatus) == false) return FALSE;
    
    INFO( "\nSending SelectImage command...\n");
    if (WtpComm.SelectImage(&DeviceImageNums) == false)
    {
        INFO( "\nSelectImage failed in WTPTP.\n");
        INFO( "DeviceImageNums: 0x%08x\n", DeviceImageNums );
        return FALSE;
    }

    DEV_IMAGE_NUMS_EXTRACT( DeviceImageNums );
    if ( DeviceImageNums == 0xFFFFFFFF )
    {
        // this may not be the right thing to do but if I don't DKB hangs on the SelectImage command
        // on the other hand, if I do this, DKB hangs on a timeout on HandleDisconnect after WTPTP
        // has already shutdown.....What to do?????
        ALWAYS("  Success: Image Download complete!\n");

        SendingDone();
        // breakout of download image loop
        return FALSE;
    }

    INFO( "DeviceNum: %d   ", DeviceNum );
    INFO( "ImageNum: %d\n", ImageNum );

    DeviceHeader DevHeader;  // temp used only for FBF v2
    pDeviceHeader = &DevHeader;
        
    if ( DeviceNum < FBF_MBH_DH.MBH.nOfDevices )
    {
        if ( FBF_MBH_DH.MBH.Format_Version == 2 )
        {
            pDeviceHeader = &DevHeader;
            
            // only copy over the V2 device header, V3 portions are set to 0
            memset( pDeviceHeader, 0, DEVICE_HEADER_SIZE_IN_BYTES ); // init device header
            memcpy( &pDeviceHeader->nOfImages,
                    (void*)((int)&FBF_MBH_DH.MBH + FBF_MBH_DH.MBH.deviceHeaderOffset[ DeviceNum ] ),
                    DEVICE_HEADER_V2_SIZE_IN_BYTES );
        }
        else
            pDeviceHeader = (PDeviceHeader)(((int)&FBF_MBH_DH.MBH) + FBF_MBH_DH.MBH.deviceHeaderOffset[ DeviceNum ]);
    }
    else
    {
        ALWAYS("Error - Unknown Device Number - DeviceNum: %d\n", DeviceNum );
        return FALSE;
    }

    if ( ImageNum < pDeviceHeader->nOfImages )
         pImageStruct = &pDeviceHeader->imageStruct[ ImageNum ];
    else
    {
        ALWAYS("Error - Unknown Device Number - ImageNum: %d\n", ImageNum );
        return FALSE;
    }

    IsImage = ACK;
    strcpy( szFileName, szFBFFileName );

    INFO( "\nSending VerifyImage command...\n");
    if (WtpComm.VerifyImage(IsImage) == false)
    {
        ALWAYS("  Error: Download FBF image has aborted!\n");

        SendingDone();
        return FALSE;
    }

    if ((hFile = fopen (szFileName,"rb")) == NULL)
    {
        ALWAYS("  Error: fopen failed to open %s.\n", szFileName );
        return FALSE;
    }

    fseek (hFile,0L,SEEK_SET); // Set position to SOF
    // need to download sector 0
    lImageSize = pImageStruct->length;
    INFO( "  Image size %d loading...\n", lImageSize );

    ImageStartByte = pImageStruct->First_Sector*BLOCK_DEVICE_SECTOR_SIZE;
    INFO( "  File start position... %d\n", ImageStartByte );

    fseek (hFile,ImageStartByte,SEEK_SET); // Set position to start of image
    uiRemainingBytes = (unsigned int)lImageSize;

    INFO( "Sending DataHeader command...\n");

    if (WtpComm.DataHeader(uiRemainingBytes,&WtpStatus) == false)
    {
        ALWAYS("  Error: WtpComm.DataHeader failed to receive an ACK!\n");

        if (hFile != NULL) fclose (hFile);
        return FALSE;
    }
    if (WtpStatus.Flags & 0x4 && CommandLineParser.FastDownload() )
    {
        ALWAYS("NOTE:: Fast Download is activated!\n");
    }

    int loop =0;
    while (uiRemainingBytes > 0)
    {
        if ( !CommandLineParser.FastDownload() && loop )
        {
             INFO( "Sending DataHeader command...\n");
             if (WtpComm.DataHeader(uiRemainingBytes,&WtpStatus) == false)
             {
                 ALWAYS("  Error: WtpComm.DataHeader failed to receive an ACK!\n");

                 if (hFile != NULL) fclose (hFile);
                 return FALSE;
             }
        }
        loop ++;
        unsigned int uiBufferSize = 0;
        
        for (int i = 0; i < (int)WtpStatus.DLEN; i++)
               uiBufferSize |= WtpStatus.Data[i] << (8 * i);

        if(uiBufferSize > uiRemainingBytes) uiBufferSize = uiRemainingBytes;
        uiRemainingBytes -= uiBufferSize; // Calculate remaining bytes

        INFO( "Buffer size = %u, ", uiBufferSize );
        INFO( "Bytes remaining = %u\n", uiRemainingBytes );

        if ((BytesRead = fread (&WtpCmd.Data,1,uiBufferSize,hFile)) != (long)uiBufferSize)
        {
            ALWAYS("  Error: fread only read %ld, ", BytesRead );
            ALWAYS("should've read %ld\n", uiBufferSize );

            if (hFile != NULL) fclose (hFile);
            return FALSE;
        }
        else
        {

            short totalSize=0;
            unsigned int mark = 0;

            // We do not split packets based on packetSize for FastDownload.
            short packetSize = CommandLineParser.USBPacketSize();
            totalSize = uiBufferSize;
            if (totalSize > packetSize )
            {
                do
                {
                    if (totalSize <= packetSize)
                    {
                        packetSize = totalSize;
                        if ( CommandLineParser.FastDownload() )
                        {  /* Code to see if last data */
                            if(uiRemainingBytes == 0)
                            {
                                INFO( "LAST PACKET:: --------------------Bytes remaining = %u\n", uiRemainingBytes );  
                                isLastData(SET, true);
                            }
                        }
                    }

                    INFO( "Sending data block with buffer size of %d...\n", packetSize );

                    if (WtpComm.Data(&(WtpCmd.Data[mark]),packetSize) == false)
                    {
                        ALWAYS("  Error: WtpComm.Data failed to receive an ACK!\n");

                        if (hFile != NULL) fclose (hFile);
                        return FALSE;
                    }

                    totalSize-=packetSize;
                    mark += packetSize;

                } while (totalSize > 0);
            }
            else
            {
                if ( CommandLineParser.FastDownload() )
                {  /* Code to see if last data */
                    if(uiRemainingBytes == 0)
                    {
                        INFO( "LAST PACKET:: --------------------Bytes remaining = %u\n", uiRemainingBytes );  
                        isLastData(SET, true);
                    }
                }
                if (WtpComm.Data(WtpCmd.Data,uiBufferSize) == false)
                {
                    ALWAYS("  Error: WtpComm.Data failed to receive an ACK!\n");

                    if (hFile != NULL) fclose (hFile);
                    return FALSE;
                }
            }
        }
    }

    SendingDone();

    if ( CommandLineParser.FastDownload() )
        isLastData (SET, false);

    if (hFile != NULL) fclose (hFile);
    return TRUE;
}

bool OtpView ()
{
    BYTE FT = CommandLineParser.OtpFlashType();

    if (FT >= MAXFLASHTYPES) return FALSE;

    ALWAYS("Flash type selected: %s\n", FlashTypeCodes[FT].Description );

    if (FT == 0 || FT == 1 || FT == 2 || FT == 4 || FT == 6)
    {
        ALWAYS("  Error: OTP registers are not available for NAND flash types!\n");
        return TRUE;
    }

    ALWAYS("\nSending preamble...\n");

    if (WtpComm.SendPreamble() == false) return FALSE;

    ALWAYS("\nSending OtpView command...\n");

    if (WtpComm.OtpView() == false) return FALSE;

    return TRUE;
}

bool DebugBoot ()
{
    BYTE FT = CommandLineParser.OtpFlashType ();

    if (FT >= MAXFLASHTYPES) return FALSE;
    ALWAYS("Flash type selected: %s\n", FlashTypeCodes[FT].Description );

    ALWAYS("\nSending preamble...\n");
    if (WtpComm.SendPreamble() == false) return FALSE;

    ALWAYS("\nSending DebugBOOT command...\n");

    if (WtpComm.DebugBoot(&FT) == false) return FALSE;

    SendingDone();
    return TRUE;
}

#if TRUSTED
bool JtagReEnable ()
{
    WTPCOMMAND WtpCmd;
    WTPSTATUS *pWtpStatus = NULL;
    pKeyInfo pKey = NULL;
    TCHAR szFileName[MAX_PATH];
    FILE *KeyFile;
    int ErrorStatus;
    BYTE Password[256];
    BYTE DigitalSignature[256];

    if (AckWithImageName(JTAG_KEY,szFileName) == NACK)
    {
        ALWAYS("  Error: Did not supply KEY file for JTAG re-enablement!\n");
        return FALSE;
    }

    strcpy( szFileName, CommandLineParser.JtagFileName().c_str() );
    if ((KeyFile = fopen (szFileName,"r")) == NULL)
    {
        ALWAYS("  Error: fopen failed to open key file %s.\n", szFileName );
        return FALSE;
    }

    pKey = new KeyInfo;
    ErrorStatus = ReadKeyFile ( KeyFile,pKey->PublicKey,pKey->PrivateKey,
                                pKey->Modulus,&pKey->KeyLength );
    if (ErrorStatus != NoError)
    {
        ALWAYS("  Error: ReadKeyFile failed while processing file %s.!\n", szFileName );
        fclose (KeyFile);
        delete pKey;
        delete pWtpStatus;
        return FALSE;
    }

    memcpy (&WtpCmd.Data[0],pKey->PublicKey,4 * pKey->KeyLength);
    memcpy (&WtpCmd.Data[4 * pKey->KeyLength],pKey->Modulus,4 * pKey->KeyLength);
    fclose (KeyFile);

    INFO( "\nSending preamble...\n");

    if (WtpComm.SendPreamble() == false) 
    {
        delete pKey;
        return FALSE;
    }

    INFO( "\nSending Public Key...\n");

    if (WtpComm.PublicKey() == false) 
    {
        delete pKey;
        return FALSE;
    }

    INFO( "\nSending Data...\n");

    if (WtpComm.Data(WtpCmd.Data,8 * pKey->KeyLength) == false) 
    {
        delete pKey;
        return FALSE;
    }

    INFO( "\nSending Password...\n");

    pWtpStatus = new WTPSTATUS;
    if (WtpComm.Password(pWtpStatus) == false) 
    {
        delete pKey;
        delete pWtpStatus;
        return FALSE;
    }
    memcpy (&Password,pWtpStatus->Data,8);
    delete pWtpStatus;

    ALWAYS("Password: 0x%08x ", (unsigned int)*((unsigned int*)&Password[0]) );
    ALWAYS("0x%08x\n", (unsigned int)*((unsigned int*)&Password[4]) );

    //  PadPasswordToSign ((unsigned int *)Password,pKey->KeyLength);
#if TRUSTED
    RSA (   (unsigned int *)pKey->PrivateKey,
            (unsigned int *)pKey->Modulus,
            pKey->KeyLength,
            (unsigned int *)Password,
            (unsigned int *)DigitalSignature   );
#endif
    memcpy (&WtpCmd.Data[0],DigitalSignature,4 * pKey->KeyLength);

    ALWAYS("Signed Password:\n");

    for(unsigned int c = 0; c < pKey->KeyLength * 4; c+=8)
    {
        ALWAYS("0x%08x ", (unsigned int)*((unsigned int *)&DigitalSignature[c]) );
        ALWAYS("0x%08x \n", (unsigned int)*((unsigned int *)&DigitalSignature[c+4]));
    }

    INFO( "\nSending Signed Password...\n");
    if (WtpComm.SignedPassword() == false) 
    {
        delete pKey;
        return FALSE;
    }

    INFO( "\nSending Data...\n");
    if (WtpComm.Data(WtpCmd.Data,4 * pKey->KeyLength) == false) return FALSE;

    SendingDone();

    delete pKey;

    return TRUE;
}
#endif

BYTE AckWithImageName (DWORD ImageType,TCHAR *szFileName)
{
    BYTE IsImage = NACK;
    if ( ImageType == TIMIDENTIFIER )
    {
        if ( CommandLineParser.TimFileName().length() > 0 )
        {
            strcpy( szFileName, CommandLineParser.TimFileName().c_str() );
            IsImage = ACK;
        } 
    }
    else if ( ImageType == FBFIDENTIFIER )
    {
        strcpy( szFileName, CommandLineParser.ImageFile(0)->FileName );
        IsImage = ACK;
    }
    else if ( ImageType == JTAG_KEY )
    {
        if ( CommandLineParser.JtagFileName().length() > 0 )
        {
            strcpy( szFileName, CommandLineParser.JtagFileName().c_str() );
            IsImage = ACK;
        }
    }
    else
    {
        for (unsigned int i = 0; i < CommandLineParser.NumImageFiles(); i++)
        {
            if (CommandLineParser.ImageFile(i)->FileType == 0) continue;
            if (CommandLineParser.ImageFile(i)->FileType == ImageType)
            {
                IsImage = ACK;
                strcpy( szFileName, CommandLineParser.ImageFile(i)->FileName );
                break;
            }
        }
    }
    return IsImage;
}

bool IsScriptFile (int argc,char *argv[],char *ScriptFileName,bool *IsScript)
{
    int currentOption = 1;
    *IsScript = false;
    bool optionIsCommandLine = false;

    // This function will determine whether or not a script file is being used
    // to provision the program by looking for a (-x script.ini) parameter in
    // the command line.

    while (currentOption < argc)
    {
        if (*argv[currentOption] == '-' || *argv[currentOption] == '/')
        {
            if (*++argv[currentOption] == 'x')
            {
                if (++currentOption == argc)
                {
                    ALWAYS("  Error: Missing input script file.\n");
                    return FALSE;
                }

                while (*argv[currentOption] != NULL)
                {
                    *ScriptFileName++ = *argv[currentOption]++;
                }
                *ScriptFileName = '\0';
                *IsScript = true;
                //break;
            }
            else
            {
                optionIsCommandLine = true;
            }
            --argv[currentOption++];
        }
        else
        {
            currentOption++;
        }
    }

    if (optionIsCommandLine == true && *IsScript == true )
    {
        ALWAYS("  Error: Missing input script file.\n");
        return FALSE;
    }
    else
        return TRUE;
}

bool ParseScriptFile (char *ScriptFileName)
{
    unsigned int DownloadBaudRate = 115200;
    char DownLoadFile[MAX_PATH];
    char szMessage[100];
    char fLine[100];
    char szTemp[100];
    FILE* ScriptFile;
    int i = 0;

    ScriptFile = fopen (ScriptFileName,"r");
    if (ScriptFile == NULL)
    {
        sprintf (szMessage,"  Error: Can not open %s !",ScriptFileName);
#ifdef _CONSOLE
            printf ("%s\n",szMessage);
#else
// TBD
#if WINDOWS
        MessageBox (NULL,szMessage,"FILE OPEN ERROR",MB_ICONSTOP|MB_OK);
#endif
#endif
        return FALSE;
    }

    if (fgets (fLine,100,ScriptFile) != NULL)
    {
        if (strstr (fLine,"[WTPDOWNLOAD]") == NULL)
        {
            printf ("////////////////////////////////////////////////\n");
            printf ("  Error: Script file, %s, is invalid!\n",ScriptFileName);
            printf ("[WTPDOWNLOAD] must be included on the first line\n");
            printf ("to insure correct file type.\n");
            printf ("////////////////////////////////////////////////\n");
            fclose (ScriptFile);
            return FALSE;
        }
    }
    else
    {
        printf ("////////////////////////////////////////////////\n");
        printf ("  Error: Script file, %s, is completely empty!\n",ScriptFileName);
        printf ("////////////////////////////////////////////////\n");
        fclose (ScriptFile);
        return FALSE;
    }

    while (fgets (fLine,100,ScriptFile) != NULL)
    {
        if (strstr (fLine,"!") != NULL)
        {
            // This must be the first check in the code. If this is
            // found anywhere in the line of the script the line is
            // considered to be commented out.
            continue;
        }

        if (strstr (fLine,"VERBOSE") != NULL)
        {
            if (strstr (fLine,"=") == NULL)
            {
                printf  ("  Error: VERBOSE declaration statement does not contain an = sign in it!\n");
                printf  ("Format: VERBOSE = TRUE \n");
                fclose (ScriptFile);
                return FALSE;
            }

            strtok (fLine,"=");
            char *IsTrue = strtok (NULL,"=");

            if (strstr (IsTrue,"TRUE") != NULL)
            {
                printf ("\nVerbose mode is enabled.\n");
                CommandLineParser.Verbose( true );
            }

            continue;
        }

        if (strstr (fLine,"UARTDELAY") != NULL)
        {
            if (GetParameter (fLine,szTemp,"IMAGESECTIONDELAY",FALSE) == FALSE) {fclose (ScriptFile); return FALSE;}
            i = 0;
            while (szTemp[i] != '\0')
            {
                if ( !(isdigit (szTemp[i])) )
                {
                    printf ("  Error:  IMAGESECTIONDELAY can only be set to numbers!\n");
                    printf ("Format: IMAGESECTIONDELAY = 300\n");
                    fclose (ScriptFile);
                    return FALSE;
                }

                ++i;
            }
            CommandLineParser.UARTDelay(atoi (szTemp));
            continue;
        }

        if (strstr (fLine,"TIM") != NULL)
        {
            if (GetParameter (fLine,DownLoadFile,"TIM",TRUE) == FALSE) {fclose (ScriptFile); return FALSE;}
            CommandLineParser.TimFileName( DownLoadFile );            
            IMAGEFILE* pImageFile = new IMAGEFILE;
            strcpy( pImageFile->FileName, DownLoadFile);
            pImageFile->FileType = TIMIDENTIFIER;
            CommandLineParser.ImageFile(pImageFile);
            continue;
        }

        if (strstr (fLine,"OBM") != NULL)
        {
            if (GetParameter (fLine,DownLoadFile,"OBM",TRUE) == FALSE) {fclose (ScriptFile); return FALSE;}
            CommandLineParser.ImageFileName( DownLoadFile );
            IMAGEFILE* pImageFile = new IMAGEFILE;
            strcpy( pImageFile->FileName,DownLoadFile);
            pImageFile->FileType = OBMIDENTIFIER+CommandLineParser.NumImageFiles();
            CommandLineParser.ImageFile(pImageFile);
            continue;
        }
        if (strstr (fLine,"PARTITION") != NULL)
        {
            if (GetParameter (fLine,DownLoadFile,"PARTITION",TRUE) == FALSE) {fclose (ScriptFile); return FALSE;}
            CommandLineParser.PartitionFileName( DownLoadFile );
            IMAGEFILE* pImageFile = new IMAGEFILE;
            strcpy( pImageFile->FileName,DownLoadFile);
            pImageFile->FileType = PARTIONIDENTIFIER;
            CommandLineParser.ImageFile(pImageFile);
            continue;
        }        

        if (strstr (fLine,"KEY") != NULL)
        {
            if (GetParameter (fLine,DownLoadFile,"KEY",TRUE) == FALSE) {fclose (ScriptFile); return FALSE;}
            CommandLineParser.JtagFileName( DownLoadFile );
            continue;
        }
        if (strstr (fLine,"FBF") != NULL)
        {
            if (GetParameter (fLine,DownLoadFile,"FBF",TRUE) == FALSE) {fclose (ScriptFile); return FALSE;}
            CommandLineParser.FbfFileName( DownLoadFile );
            continue;
        }
        if (strstr (fLine,"OTPVIEW") != NULL)
        {
            if (GetParameter (fLine,szTemp,"OTPVIEW",FALSE) == FALSE) {fclose (ScriptFile); return FALSE;}
            if (strstr (szTemp,"TRUE") != NULL)
            {
                CommandLineParser.OtpView( true );
            }
            continue;
        }
        if (strstr (fLine,"FASTMODE") != NULL)
        {
            if (GetParameter (fLine,szTemp,"FASTMODE",FALSE) == FALSE) {fclose (ScriptFile); return FALSE;}
            if (strstr (szTemp,"TRUE") != NULL)
            {
                CommandLineParser.FastDownload( true );
            }
            else if (strstr (szTemp,"FALSE") != NULL)
            {
                CommandLineParser.FastDownload( false );
            }
            continue;
        }

        if (strstr (fLine,"FLASHTYPE") != NULL)
        {
            if (GetParameter (fLine,szTemp,"FLASHTYPE",FALSE) == FALSE) {fclose (ScriptFile); return FALSE;}
            i = 0;
            while (szTemp[i] != '\0')
            {
                if ( !(isdigit (szTemp[i])) )
                {
                    printf ("  Error:  FLASHTYPE can only be set to numbers 0 thru %d!\n",MAXFLASHTYPES - 1);
                    printf ("Format: FLASHTYPE = 7\n");
                    fclose (ScriptFile);
                    return FALSE;
                }

                ++i;
            }
            if (atoi (szTemp) >= MAXFLASHTYPES)
            {
                printf ("  Error:  FLASHTYPE can only be set to numbers 0 thru %d!\n",MAXFLASHTYPES - 1);
                fclose (ScriptFile);
                return FALSE;
            }
            CommandLineParser.OtpFlashType( atoi(szTemp) );
            continue;
        }

        if (strstr (fLine,"DEBUGBOOT") != NULL)
        {
            if (GetParameter (fLine,szTemp,"DEBUGBOOT",FALSE) == FALSE) {fclose (ScriptFile); return FALSE;}
            if (strstr (szTemp,"TRUE") != NULL)
            {
                CommandLineParser.DebugBoot( TRUE );
            }
            continue;
        }

        if (strstr (fLine,"PORTTYPE") != NULL)
        {
            if (GetParameter (fLine,szTemp,"PORTTYPE",FALSE) == FALSE) {fclose (ScriptFile); return FALSE;}
            if (strstr (szTemp,"USB") != NULL)
            {
                CommandLineParser.PortType(USB);
            }
            else if (strstr (szTemp,"UART") != NULL)
            {
                CommandLineParser.PortType (UART);
            }
            else
            {
                printf ("  Error: Port Type parameter must be USB or UART.\n");
                fclose (ScriptFile);
                return FALSE;
            }
            continue;
        }

        if (strstr (fLine,"COMPORT") != NULL)
        {
            if (GetParameter (fLine,szTemp,"COMPORT",FALSE) == FALSE) {fclose (ScriptFile); return FALSE;}
            CommandLineParser.CommPort( atoi(szTemp ) );
            continue;
        }

        if (strstr (fLine,"BAUDRATE") != NULL)
        {
            if (GetParameter (fLine,szTemp,"BAUDRATE",FALSE) == FALSE) {fclose (ScriptFile); return FALSE;}
            i = 0;
            while (szTemp[i] != '\0')
            {
                if ( !(isdigit (szTemp[i])) )
                {
                    printf ("  Error:  BAUDRATE can only be set to numbers!\n");
                    printf ("Format: BAUDRATE = 115200\n");
                    fclose (ScriptFile);
                    return FALSE;
                }

                ++i;
            }
            CommandLineParser.DownloadBaudRate( atoi(szTemp) );
            continue;
        }
        if (strstr (fLine,"PLATFORM") != NULL)
        {
            if (GetParameter (fLine,szTemp,"PLATFORM",FALSE) == FALSE) {fclose (ScriptFile); return FALSE;}
            CommandLineParser.PlatformType((ePlatformType) atoi(szTemp) );
            continue;
        }
        if (strstr (fLine,"LOGLEVEL") != NULL)
        {
            if (GetParameter (fLine,szTemp,"LOGLEVEL",FALSE) == FALSE) {fclose (ScriptFile); return FALSE;}
            WtpLogSetting::getInstance().setLevel(atoi(szTemp));
            continue;
        }
        
        
    }

    fclose (ScriptFile);

    if (CheckYourOptions () == FALSE) return FALSE;

    return TRUE;
}

bool CheckYourOptions ()
{
    int nOptionsEnabled = 0;

    if (CommandLineParser.TimFile())
    {
        if (CommandLineParser.OtpView() || CommandLineParser.DebugBoot()  || (CommandLineParser.CurrentMessageMode() == MSG_ONLY ))
        {          
            ALWAYS("  Error: Cannot select OTP view, Debug Boot or Message Only\n");
            ALWAYS("options with TIM file downloads!\n");
            return FALSE;
        }
    }
    else if( CommandLineParser.CurrentMessageMode() == MSG_ONLY )
    {
        if ( CommandLineParser.JtagFile() || CommandLineParser.OtpView() || CommandLineParser.DebugBoot() 
             || CommandLineParser.TimFile() || CommandLineParser.ImageFile() )
        {
            ALWAYS("  Error: Can only select port type \n");
            ALWAYS("option with Message Mode!\n");
            return FALSE;
        }
    }
    else if( CommandLineParser.CurrentMessageMode() == MSG_AND_DOWNLOAD)
    {
        if (CommandLineParser.JtagFile() || CommandLineParser.OtpView() || CommandLineParser.DebugBoot() )
        {
            ALWAYS("  Error: Cannot select JTAG reenablement, OTP view or Debug Boot \n");
            ALWAYS("options with Message and Download Mode!\n");
            return FALSE;
        }
    }
    else
    {
        if (CommandLineParser.JtagFile()) nOptionsEnabled++;
        if (CommandLineParser.DebugBoot()) nOptionsEnabled++;
        if (CommandLineParser.OtpView()) nOptionsEnabled++;
        if (CommandLineParser.ImageFile()) nOptionsEnabled++;

        if (nOptionsEnabled > 1)
        {
            ALWAYS("  Error: Cannot select image file downloads, OTP view, Debug Boot\n");
            ALWAYS("or JTAG reenablement options simultaneously!\n");
            return FALSE;
        }
    }

    return TRUE;
}

bool GetParameter (char *fLine,char *pParameter,char *pDec,bool IsFile)
{
    char *pTemp = NULL;

    // This function is used by the ParseScriptFile function to parse out
    // values from lines that contain an equal sign. An equal sign is used as
    // part of a directive. Example directive( PORTTYPE = UART ). In this case
    // the token UART is extracted from the string to signify that the UART
    // will be used to download images.

    if (strstr (fLine,"=") == NULL)
    {
        ALWAYS("  Error: %s declaration statement does not contain an = sign in it!\n", fLine );
        ALWAYS("Format: %s = filename \n", pDec);
        return FALSE;
    }

    strtok (fLine,"=");
    pTemp = strtok (NULL,"=");

    if (pTemp == NULL)
    {
        ALWAYS("  Error: %s declaration statement is invalid!\n", fLine );
        ALWAYS("Format: %s = filename \n", pDec);
        return FALSE;
    }

    if (IsFile)
    {
        while (*pTemp != '\0' && *pTemp != '\n' && *pTemp != '\r')
        {
            if (isprint (*pTemp) && *pTemp != ' ')
            {
                while (*pTemp != '\0' && *pTemp != '\n' && *pTemp != '\r')
                {
                    if (isprint (*pTemp))
                    {
                        *pParameter = *pTemp;
                        ++pParameter;
                    }
                    ++pTemp;
                }
                break;
            }
            ++pTemp;
        }
    }
    else
    {
        while (*pTemp != '\0' && *pTemp != '\n' && *pTemp != '\r')
        {
            if (isprint (*pTemp) && *pTemp != ' ')
            {
                *pParameter = *pTemp;
                ++pParameter;
            }
            ++pTemp;
        }
    }
    *pParameter = '\0';

    return TRUE;
}

void DebugViewPrint (char *szMessage)
{
#if _DEBUG
    OutputDebugString (szMessage);
#endif 
}


void SendingDone()
{
    INFO("\nSending Done Command...\n");
    WtpComm.Done();
}


void dump_array_with_interval(char* prefix,unsigned char* a,int length,int interval)
{
	int i,len;
	unsigned int offset=0;
	unsigned char *iptr = a;
	unsigned char *cptr = a;

	printf("[%s] size[x%x]",prefix,length);
	
	len = length>16?16:length;
	if(length>0x5000) length = 0x5000;
	if(interval<16) interval=16;
	do
	{				
		for (i = 0; i < len; i++) {
			if (i== 0) {
				printf("\r\n0x%08x:", (unsigned int)offset);
			}
			printf(" %02x", *(cptr++));
		}		
		offset+=interval;
		iptr+=interval;
		cptr=iptr;
		if(offset<length)
		{
			len=length-offset;
		}
		else
			len = 0;
		if(len>16) len=16;
	}while(offset<length);

}


bool DownloadImages( int iNumFiles, bool bWithJTagEnable )
{
    int retryAttempts = 3;
    bool bIsOk = true;

    ALWAYS("Number of files to download: %d\n", iNumFiles );
    ALWAYS("Platform Type (-T switch): %d\n", CommandLineParser.PlatformType() );

    if ( CommandLineParser.Platform() && (CommandLineParser.PlatformType() == PXA168 
                                        || CommandLineParser.PlatformType() == ARMADA168) )
    {
        retry = true;
    }

    for (int i = 0; ((i < iNumFiles) && bIsOk); i++)
    {
        if ( CommandLineParser.Platform() && (CommandLineParser.PlatformType() == PXA168 
                                            || CommandLineParser.PlatformType() == ARMADA168) )
        {
            if ( PrevImageType == DKBIDENTIFIER )
                break;
        }
        Sleep(1000);
        if (DownloadImage (i) == FALSE)
        {
            ALWAYS("  Error: Download failed for file number: %d\n", i + 1 );

#if 1
            if ( CommandLineParser.Platform() && (CommandLineParser.PlatformType() == PXA168 
                                                || CommandLineParser.PlatformType() == ARMADA168) )
            {
                if ( PrevImageType == DKBIDENTIFIER )
                    break;
#endif
                if ( retry == true )
                {
                    ALWAYS("\n Retrying download of all images!\n");
                    i = -1; // retry all images
                    if ( --retryAttempts <= 0 )
                        retry = false;
                    else
                        continue;
                }
#if 1
            }
            // retries for other than PXA168 or ARMADA168
            else if ( retry == true )
            {
                // this retry is used when a target requests a PartitionTable
                // but none is provided.  The retry in effect ignores the
                // request and continues to download the remaining TIM imamges
                ALWAYS("\n Retrying this image download!\n" );
                i--; // retry downloading the image that just failed
                if ( --retryAttempts <= 0 )
                    retry = false;
                else
                    continue;
            }
#endif
            ALWAYS("  Error: Download images failed on image #%d!\n", i + 1 );

            bIsOk = false;
            break;
        }
        else
        {
            ALWAYS( "  Success: Download file complete for image #%d!\n", i + 1 );
        }

#if TRUSTED
        // After downloading TIM, allow for a JTAG reenablement.
        if ( i == 0 )
        {
            if ( bIsOk && bWithJTagEnable )
            {
                ALWAYS("\nJTAG Reenablement Mode\n");
                if(JtagReEnable () == FALSE)
                    SendingDone();
            }
        }
#endif
    }

    return bIsOk;
}

bool isLastData(bool mode, bool value)   //added to set/get the last data condition.
{
    static BOOL lastData = GET;

    if (mode)
        lastData = value;
    else
        return lastData;

    return FDMODE;
}

bool DoUploads( TARGET_PARAMS* pTargetParams )
{
    list<UPLOAD_DATA_SPEC*> UploadSpecs;
    bool bVerbose = CommandLineParser.Verbose();

    if ( !ParseUploadSpecFile( &UploadSpecs ) )
        return false;

    // After successful parsing of UploadSpec file, do all uploads
    list<UPLOAD_DATA_SPEC*>::iterator UploadSpecsListIter = UploadSpecs.begin();
    while ( UploadSpecsListIter != UploadSpecs.end() )
    {
        ALWAYS(  "Uploading:\n" );
        ALWAYS( "Type: %d\n", (*UploadSpecsListIter)->Params.Type );
        ALWAYS( "SubType: %d\n", (*UploadSpecsListIter)->Params.SubType );
        ALWAYS( "Partition: %d\n", (*UploadSpecsListIter)->Params.Partition );
        ALWAYS( "Offset: %d\n", (*UploadSpecsListIter)->Params.Offset );
        ALWAYS( "DataSize: %d\n", (*UploadSpecsListIter)->Params.DataSize );
        char szFileName[MAX_PATH] = {0};
        strncpy( szFileName, (*UploadSpecsListIter)->sOutputFileName.c_str(), MAX_PATH );            
        ALWAYS(  "Output File: %s\n", szFileName );
        strncpy( szFileName, (*UploadSpecsListIter)->sComparisonFileName.c_str(), MAX_PATH );            
        ALWAYS( "Comparison File: %s\n", szFileName );
                            
        BYTE* pUploadDataBuffer = new BYTE[ (*(UploadSpecsListIter))->Params.DataSize ];
        if ( pUploadDataBuffer )
        {
            memset( pUploadDataBuffer, 0, (*(UploadSpecsListIter))->Params.DataSize );
            if ( WtpComm.UploadDataHeader( &(*UploadSpecsListIter)->Params ) )
            {
                unsigned int BytesRemaining = (*UploadSpecsListIter)->Params.DataSize;
                unsigned int BytesReceived = 0;
                
                while ( BytesRemaining > 0 )
                {
                    if ( !WtpComm.UploadData( pUploadDataBuffer + BytesReceived, min( BytesRemaining, pTargetParams->BufferSize - 6 ) ) )
                    {
                        ALWAYS( "  Error: Upload Failed!\n" );
                        delete pUploadDataBuffer;
                        return false;
                    }
                    // downcount the bytes remaining, not counting the wtpstatus header in the buffer
                    BytesReceived  += min( BytesRemaining, pTargetParams->BufferSize-6 );
                    if(bVerbose)
                        ALWAYS( "Bytes received = %u\n", BytesReceived );
                    BytesRemaining -= min( BytesRemaining, pTargetParams->BufferSize-6 );
                    if(bVerbose)
                        ALWAYS( "Bytes remaining  = %u\n", BytesRemaining );
                }

                ALWAYS( "Upload Completed!\n");

                SaveUploadToFile( pUploadDataBuffer, *(UploadSpecsListIter) );
                CompareUploadToFile( pUploadDataBuffer, *(UploadSpecsListIter) );            
            }
            else
            {
                ALWAYS( "Upload Failed!\n");
                delete pUploadDataBuffer;
                return false;
            }
                        
            delete [] pUploadDataBuffer;
        }
        
        UploadSpecsListIter++;
    }

    SendingDone();

    return true;
}

bool ParseUploadSpecFile( list<UPLOAD_DATA_SPEC*>* pUploadSpecs )
{
    ifstream ifs;
    pUploadSpecs->clear();

    if ( CommandLineParser.UploadSpecFile() )
    {
        ifs.open(CommandLineParser.UploadSpecFileName().c_str(), ios_base::in );
        if ( ifs.bad() || ifs.fail() )
        {
            char szFileName[MAX_PATH] = {0};
            strncpy( szFileName, CommandLineParser.UploadSpecFileName().c_str(), MAX_PATH );            
            ALWAYS( "Error: Cannot open Upload Spec file name %s !\n", szFileName );
            return false;
        }
    }

    CTimLib TimLib;
    UPLOAD_DATA_SPEC* pNewSpec = 0;
    while ( !ifs.eof() && !ifs.bad() && !ifs.fail() )
    {
        string sLine;
        DWORD dwValue = 0;
        pNewSpec = 0;
        
        if ( TimLib.GetNextLineNoWSSkipComments( ifs, sLine ) )
        {
            if ( ifs.eof() || ifs.bad() || ifs.fail() )
                goto Exit;
            
            if ( sLine.find( "Upload Spec:" ) == string::npos )
                goto Exit;
    
            pNewSpec = new UPLOAD_DATA_SPEC;
            // init the portion before the string objects
            memset( pNewSpec, 0, sizeof(UPLOAD_DATA_PARAMS) );
        }
        else goto Exit;

        if ( TimLib.GetNextLineNoWSSkipComments( ifs, sLine ) )
        {
            if ( ifs.eof() || ifs.bad() || ifs.fail() )
                goto Exit;
            
            if ( TimLib.GetFieldValue( sLine, string("Type"), dwValue ) )
                pNewSpec->Params.Type = BYTE(dwValue);
            else goto Exit;
        }
        else goto Exit;

        if ( TimLib.GetNextLineNoWSSkipComments( ifs, sLine ) )
        {
            if ( ifs.eof() || ifs.bad() || ifs.fail() )
                goto Exit;

            if ( TimLib.GetFieldValue( sLine, string("Subtype"), dwValue ) )
                pNewSpec->Params.SubType = BYTE(dwValue);
            else goto Exit;
        }
        else goto Exit;

        if ( TimLib.GetNextLineNoWSSkipComments( ifs, sLine ) )
        {
            if ( ifs.eof() || ifs.bad() || ifs.fail() )
                goto Exit;

            if ( TimLib.GetFieldValue( sLine, string("Partition"), dwValue ) )
                pNewSpec->Params.Partition = (BYTE)dwValue;
            else goto Exit;
         }
        else goto Exit;

        if ( TimLib.GetNextLineNoWSSkipComments( ifs, sLine ) )
        {
            if ( ifs.eof() || ifs.bad() || ifs.fail() )
                goto Exit;

            if ( TimLib.GetFieldValue( sLine, string("Offset"), dwValue ) )
                pNewSpec->Params.Offset = dwValue;
            else goto Exit;
        }            
        else goto Exit;
         
        if ( TimLib.GetNextLineNoWSSkipComments( ifs, sLine ) )
        {
            if ( ifs.eof() || ifs.bad() || ifs.fail() )
                goto Exit;

            if ( TimLib.GetFieldValue( sLine, string("Data Size"), dwValue ) )
                pNewSpec->Params.DataSize = dwValue;
            else goto Exit;
        }
        else goto Exit;

        if ( TimLib.GetNextLineNoWSSkipComments( ifs, sLine ) )
        {
            if ( ifs.eof() || ifs.bad() || ifs.fail() )
                goto Exit;

            if ( !TimLib.GetFieldValue( sLine, string("Output File"), pNewSpec->sOutputFileName ) )
                goto Exit;
         }
        else goto Exit;

        if ( TimLib.GetNextLineNoWSSkipComments( ifs, sLine ) )
        {
            if ( ifs.eof() || ifs.bad() || ifs.fail() )
                goto Exit;

            if ( !TimLib.GetFieldValue( sLine, string("Compare File"), pNewSpec->sComparisonFileName ) )
                goto Exit;
        }        
        else goto Exit;
         
        if ( TimLib.GetNextLineNoWSSkipComments( ifs, sLine ) )
        {
            if ( ifs.eof() || ifs.bad() || ifs.fail() )
                goto Exit;

            if ( sLine.find( "End Upload Spec:" ) == string::npos )
                goto Exit;
        }
        else goto Exit;

        pUploadSpecs->push_back( pNewSpec );
    }

Exit:
    if ( ifs.eof() )
    {
        // done processing file
        ifs.close();
        return true;
    }

    delete pNewSpec;

    // got an error before reaching eof
    return false;
}

bool SaveUploadToFile( BYTE* pUploadDataBuffer, UPLOAD_DATA_SPEC* pUploadSpec )
{
    if ( pUploadDataBuffer == 0 || pUploadSpec == 0 || pUploadSpec->sOutputFileName.size() == 0 )
        return false;

    char szFileName[MAX_PATH];
    strncpy( szFileName, pUploadSpec->sOutputFileName.c_str(), MAX_PATH );            
    ALWAYS( "Saving Upload to binary file name: %s !\n", szFileName );
    
    ofstream ofs;
    ofs.open( pUploadSpec->sOutputFileName.c_str(), ios_base::out | ios::binary );
    if ( ofs.bad() || ofs.fail() )
    {
        ALWAYS( "Error: Cannot open Upload Output binary file name: %s !\n", szFileName );
        return false;
    }

    ofs.write( (char*)pUploadDataBuffer, pUploadSpec->Params.DataSize );

    bool bRet = (!ofs.bad() || ofs.fail());
    if ( !bRet )
        ALWAYS( "Error: Failed writing Upload Output binary file name %s !\n", szFileName );

    ofs.close();
    
    return bRet;
}

bool CompareUploadToFile( BYTE* pUploadDataBuffer, UPLOAD_DATA_SPEC* pUploadSpec )
{
    if ( pUploadDataBuffer == 0 || pUploadSpec == 0 || pUploadSpec->sComparisonFileName.size() == 0 )
        return false;

    char szFileName[MAX_PATH];
    strncpy( szFileName, pUploadSpec->sComparisonFileName.c_str(), MAX_PATH );            
    ALWAYS( "Comparing Upload to binary file name: %s !\n", szFileName );
    
    ifstream ifs;
    ifs.open( pUploadSpec->sComparisonFileName.c_str(), ios_base::in | ios::binary );
    if ( ifs.bad() || ifs.fail() )
    {
        ALWAYS( "Error: Cannot open Upload Compare binary file name: %s !\n", szFileName );
        return false;
    }

    ifs.seekg( 0, ios_base::end );
    streamoff lFileSize = ifs.tellg();
    ifs.seekg( 0, ios_base::beg );
    bool bRet = (!ifs.bad() || ifs.fail());
    if ( !bRet )
        ALWAYS( "Error: Failed accessing Upload Compare binary file name: %s !\n", szFileName );

    BYTE* CompareAry = new BYTE[(unsigned int)lFileSize];
    memset( CompareAry, 0, (size_t)lFileSize );
    ifs.read( (char*)CompareAry, lFileSize );

    bRet = (!ifs.bad() || ifs.fail());
    if ( !bRet )
        ALWAYS( "Error: Failed reading Upload Compare binary file name: %s !\n", szFileName );

    ifs.close();

    if ( bRet )
    {
        streamoff CompareSize = min( (unsigned int)lFileSize, pUploadSpec->Params.DataSize );
        if ( (unsigned int)lFileSize > pUploadSpec->Params.DataSize )
            ALWAYS( "Upload Compare file is bigger than upload data!\n");
        else if ( (unsigned int)lFileSize > pUploadSpec->Params.DataSize )
            ALWAYS( "Upload Compare file is smaller than upload data!\n");
    
        ALWAYS(  "Comparing %d bytes...\n", (unsigned int)CompareSize );

        for ( long i = 0; i < CompareSize; i++ )
        {
            if ( pUploadDataBuffer[i] != CompareAry[i] )
            {
                ALWAYS( "Comparing failed starting at byte location %d...\n", i );
                bRet = false;
                break;
            }
        }
    }

    if ( bRet )
        ALWAYS( "Compare Upload to File is Successful!\n");

    delete [] CompareAry;
    return bRet;
}

int main (int argc,char *argv[])
{
    int currentOption = 1, i=0;
    bool bScript = false;
    char ScriptFileName[MAX_PATH];
    bool bIsOk = true;
    bool Connected = FALSE;
    bool RetVal = TRUE;

    WTPSTATUS WtpStatus = {0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00};

    if (argc < MIN_ARGS)
    {
        RetVal = FALSE;
        ALWAYS(  "\nCommand line format is not valid!\n\n");
        CommandLineParser.PrintUsage();
        goto Exit;
    }

    if (IsScriptFile (argc,argv,ScriptFileName,&bScript) == FALSE) 
    {
        RetVal = FALSE;
        goto Exit;
    }
    if (bScript)
    {
        if (ParseScriptFile (ScriptFileName) == FALSE)
        {
            RetVal = FALSE;
            goto Exit;
        }
    }
    else
    {
        if (CommandLineParser.ParseCommandLine (argc,argv) == false) 
        {
            RetVal = FALSE;
            CommandLineParser.PrintUsage();
            goto Exit;
        }
    }



#if !(TRUSTED)
    ALWAYS(  "Non-trusted only version for NTIM ");
#else
    ALWAYS(  "Trusted version for TIM and NTIM " );
#endif
    ALWAYS(  "Downloading and Jtag Re-Enablement \n" );
    ALWAYS(  "Version:      %s\n", Version );
    ALWAYS(  "Release Date: %s\n", __DATE__ );
    CommandLineParser.DumpConfiguration();

    if (CommandLineParser.TimFile())
    {
        if (CommandLineParser.ParseTim () == FALSE) 
            return FALSE;
    }

    if ( !CommandLineParser.FbfFile() 
         && !CommandLineParser.TimFile() 
         && !CommandLineParser.UploadSpecFile()
         && !CommandLineParser.OtpView() 
         && !CommandLineParser.JtagFile() 
         && (CommandLineParser.CurrentMessageMode() == MSG_AND_DOWNLOAD) )
    {
        if ( CommandLineParser.NumImageFiles() == 0 )
        {
            ALWAYS( "  Error: Did not supply any files for download!\n" );
            {
                RetVal = FALSE;
                goto Exit;
            }
        }
    }

    if ((CommandLineParser.PortType() != UART)&&(CommandLineParser.PortType() != USB))
    {
        ALWAYS( "\n  Error: Port Selected: UNDEFINED\n");
        {
            RetVal = FALSE;
            goto Exit;
        }
    }
            
    if (CommandLineParser.PortType() == USB)
        WtpComm.SetUsbPortTimeout (30); // Default = 0

    if (WtpComm.OpenPort() == FALSE) 
    {
        ALWAYS( "\n  Error: Unable to Open Selected Port..\n" );
        RetVal = FALSE;
        goto Exit;
    }

    if (CommandLineParser.OtpView())
    {
        OtpView ();
    }

#if TRUSTED
    else if(CommandLineParser.TimFile() && CommandLineParser.JtagFile() && CommandLineParser.ImageFile())
    {
        bIsOk = DownloadImages( CommandLineParser.NumImageFiles(), true );
        if ( !bIsOk )
        {
            RetVal = FALSE;
            goto Exit;
        }
    }
    else if (CommandLineParser.JtagFile())
    {
        ALWAYS(  "JTAG Reenablement Mode\n" );
        if(JtagReEnable () == FALSE)
            SendingDone();
    }
#endif
    else if (CommandLineParser.DebugBoot())
    {
        ALWAYS(  "Debug Boot Mode\n" );
        DebugBoot ();
    }
    else if (CommandLineParser.CurrentMessageMode() == MSG_ONLY )
    {
        ALWAYS(  "******************************\n" );
        ALWAYS(  "*****Message mode enabled*****\n" );
        ALWAYS(  "******************************\n" );

        if(CommandLineParser.Verbose())
        {
            ALWAYS("\nSending preamble...\n");
        }

        while (TRUE)
        {
            if (Connected == FALSE){
                if (WtpComm.SendPreamble() == false)
                {
//					Sleep(1);
                    continue;
                }
                else
                {
                    Connected = TRUE;
                }
            }
            WtpStatus.Status = ACK;
            for(;;)
            {
                if ( (WtpStatus.Flags & 0x1) == MESSAGEPENDING )
                {
                    WtpComm.GetTargetMessage (&WtpStatus);
                }
                else
                {
                    WtpComm.Disconnect(&WtpStatus);
                    if (WtpStatus.Status == ACK)
                    {
                        WtpComm.ClosePort();
                        RetVal = TRUE;
                        goto Exit;
                    }
                }
            }
        }
    }
    else if ( CommandLineParser.FbfFile() )
    {
        if (DownloadMasterBlockHeader() == FALSE)
        {
            ALWAYS(  "  Image type requested by target is not FBF. Retry!" );
            if (DownloadMasterBlockHeader() == FALSE)
            {
                ALWAYS( "  Error: Retry FBF download failed.!\n" );

                bIsOk = false;
                RetVal = FALSE;
                goto Exit;
            }
            else
            {
                // if DKB NACKS first attempt but ACKs 2nd attempt, handle images here
                ALWAYS(  "  Success: MasterBlockHeader Download complete!\n" );
                ALWAYS(  "  Download all images in FBF file!\n");

                while ( DownloadFBFImage() == TRUE )
                    ALWAYS( "\n  Success: FBF Image Download complete!\n" );

                ALWAYS("  End FBF Download!\n");
            }
        }
        else
        {
            // if DKB ACKs the first try for FBF, then handle images here
            ALWAYS( "  Success: MasterBlockHeader Download complete!\n" );
            ALWAYS( "  Download all images in FBF file!\n" );

            while ( DownloadFBFImage() == TRUE )
                ALWAYS( "  Success: FBF Image Download complete!\n" );

            ALWAYS( "  All FBF Image Downloads finished!\n" );
        }
    }
    else
    {
        bIsOk = DownloadImages( CommandLineParser.NumImageFiles(), false );
        if ( !bIsOk )
        {
            RetVal = FALSE;
            goto Exit;
        }

#if SLE_TESTING
        if(i==0)
        Sleep(120000);
#endif
    }

    if ( CommandLineParser.UploadSpecFile() )
    {
        if (WtpComm.SendPreamble() == false)
        {
            RetVal = FALSE;
            goto Exit;
        }

        // ignore if target does not handle ProtocolVersion or GetParameters
        PROTOCOL_VERSION TargetProtocolVersion;
        memset( &TargetProtocolVersion, 0, sizeof(TargetProtocolVersion) );
        if (  WtpComm.GetProtocolVersion ( &TargetProtocolVersion ) )
        {
            if ( TargetProtocolVersion.MajorVersion >= TARGET_PROTOCOL_VERSION.MajorVersion
                && TargetProtocolVersion.MinorVersion >= TARGET_PROTOCOL_VERSION.MinorVersion
                && TargetProtocolVersion.Build >= TARGET_PROTOCOL_VERSION.Build )
            {
                TARGET_PARAMS TargetParams;
                if ( WtpComm.GetParameters( &TargetParams ) )
                {
                    // do uploads
                    if ( !DoUploads( &TargetParams ) )
                    {
                        RetVal = FALSE;
                        goto Exit;
                    }
                }        
            }
        }
    }

Exit:
//   while(1)
//   {
        // spin for a while to try to dump all pending messages
#if 1
        if ( (WtpStatus.Flags & 0x1) == MESSAGEPENDING && !CommandLineParser.UploadSpecFile())
        {
            //WtpComm.GetTargetMessage (&WtpStatus);
            if ( WtpStatus.Data[0] == PlatformDisconnect )
            {
                // iTry is here to prevent a deadlock if target is not responding
                int iTry = 100;
                do{
                    WtpComm.Disconnect(&WtpStatus);
                    Sleep(10);
                    iTry--;
                }while(WtpStatus.Status == NACK && iTry > 0);

                WtpComm.ClosePort();
                RetVal = TRUE;
//                break; //from while(1)
            }
//            else
//            {
//                WtpComm.Disconnect(&WtpStatus);
//                WtpComm.ClosePort();
//                break; //from while(1)
//            }
        }
//    }
//        else 
#endif
        if ( CommandLineParser.CurrentMessageMode() == DISABLED 
             || CommandLineParser.CurrentMessageMode() == MSG_AND_DOWNLOAD )
        {
            if ( CommandLineParser.OtpFlashType() || CommandLineParser.DebugBoot() )
                ;
            else
            {
                // iTry is here to prevent a deadlock if target is not responding
                int iTry = 100;
                do{
                    WtpComm.Disconnect(&WtpStatus);
                    Sleep(10);
                    iTry--;
                }while(WtpStatus.Status == NACK && iTry > 0);

                WtpComm.ClosePort();
//                break; //from while(1)
            }
        }
        else
        {
            WtpComm.Disconnect(&WtpStatus);
            WtpComm.ClosePort();
//            break; //from while(1)
        }
//    }//end if while(1)


    if ( bIsOk && RetVal )
        ALWAYS(  "\nSuccess: WtpDownload Exiting with Success Code!\n" );
    else
        ALWAYS(  "\nFailure: WtpDownload Exiting with Failure Code!\n" );

    return bIsOk && RetVal;
}

