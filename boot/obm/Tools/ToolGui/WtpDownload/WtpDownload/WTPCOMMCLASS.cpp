/**************************************************************************
**
** (C) Copyright December 2006 Marvell International Ltd.
**
** All Rights Reserved.
**
**
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR
** PURPOSE ARE EXPRESSLY DISCLAIMED.
**
**  FILENAME:   WTPCOMMCLASS.cpp
**
**  PURPOSE:    Provides download capabilities either thru the USB port or the
**              UART port. This class insulates the top level API from what type
**              of port is being utilized for the download proceedure. In the
**              future other transfer technologies could be easily added without
**              effecting the top level API.
**
******************************************************************************/
#include <stdafx.h>

#pragma warning ( disable : 4800 4996) // Disable warning messages

#include <WTPCOMMCLASS.h>
#include <WTPUARTLIB.h>
#include <WTPUSBLIB.h>
#include <ErrorCodes.h>
#include <WtpLog.h>
#include <CommandLineParser.h>

extern bool isLastData(bool mode, bool value);

CWtpComm::CWtpComm ( CCommandLineParser& rCommandLineParser )
: CommandLineParser( rCommandLineParser )
{
    Timeout = 0;
    column = 0;
    iSequence = 0;
    memset( szMessage, 0, 256 );
}

CWtpComm::~CWtpComm ()
{
}

void CWtpComm::SetUsbPortTimeout (long int lTimeout)
{
    Timeout = lTimeout;
}

bool CWtpComm::OpenPort ()
{
    bool IsOk = true;
    UartSettings Uart;

    switch (CommandLineParser.PortType() )
    {
        case USB:
            if (OpenUsbPort (Timeout, CommandLineParser.USBInterface() ? (char*)CommandLineParser.USBInterfaceName().c_str() : NULL) == false)
            {
                IsOk = false;
            }
            break;
        
        case UART:
            Uart.iPortNumber = CommandLineParser.CommPort();
            Uart.EventChar = '\001';
            Uart.dwBaudRate = CommandLineParser.DownloadBaudRate(); // zero indicates use autobaud.
            if (OpenUartPort (Uart) == false)
            {
                IsOk = false;
            }
            break;

        default:
#if WINDOWS
            MessageBox (NULL,"  Error: OpenPort failed! Must specify the port type.\n", "WTPCOMMCLASS",MB_ICONSTOP|MB_OK);
#endif
            IsOk = false;
            break;
    }
    return IsOk;
}

void CWtpComm::ClosePort ()
{
    switch (CommandLineParser.PortType())
    {
        case USB:
            CloseUsbPort ();
            break;

        case UART:
            CloseUartPort ();
            break;

        default:
            break;
    }
}

bool CWtpComm::SendPreamble ()
{
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {PREAMBLE,0xD3,0x02,0x2B,0x00D3022B};
    WTPSTATUS* pWtpStatus = new WTPSTATUS;
    DWORD BytesRead = 0;
    DWORD StatusLength = 0;

    memset( pWtpStatus, 0, sizeof(WTPSTATUS) );
 //   HandlePendingMessages( pWtpStatus );

    switch (CommandLineParser.PortType())
    {
        case USB: // Only 4 byte preamble for USB
            if (WriteUsb (&WtpCmd,4,pWtpStatus,4) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,8) == false) IsOk = false;
            if (ReadUart (pWtpStatus,4,&BytesRead) == false)
            {
                DBG( "Sent preamble. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,4);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get preamble response from the target!\n");
                    delete pWtpStatus;
                    return IsOk;
                }
            }
            
            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            IsOk = false;
            break;
    }

    if (pWtpStatus->CMD != 0x00 && pWtpStatus->SEQ != 0xD3 && pWtpStatus->CID != 0x02)
    {
        IsOk = false;
        ERROR( "  Error: SendPreamble returned a NACK!\n");
    }

//    HandlePendingMessages( pWtpStatus );

    delete pWtpStatus;
    return IsOk;
}

bool CWtpComm::GetVersion (WTPSTATUS *pWtpStatus)
{
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {GETVERSION,iSequence,0x00,0x00,0x00000000};
    DWORD BytesRead = 0, StatusLength = 0;
    int i = 0;

    //set to defaults
    memset( pWtpStatus, 0, sizeof(WTPSTATUS) );
    HandlePendingMessages( pWtpStatus );

    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,8,pWtpStatus,30) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,8) == false) IsOk = false;
            if (ReadUart (pWtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent GetVersion. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    return IsOk;
                }
            }

            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                IsOk = false;
            
            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    if (pWtpStatus->Status == NACK)
    {
        IsOk = false;
        sprintf (szMessage,"  Error: GetVersion returned a NACK!");
#ifdef _CONSOLE
        DBG( "%s\n", szMessage );
#else
#if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }
    else if (pWtpStatus->Status == ACK)
    {
        DBG( "\nVersion: %c", pWtpStatus->Data[3] );
        DBG( "%c", pWtpStatus->Data[2] );
        DBG( "%c", pWtpStatus->Data[1] );
        DBG( "%c\n", pWtpStatus->Data[0] );
        DBG( "Date: 0x%08x\n", *(unsigned long *)&pWtpStatus->Data[4] );
        DBG( "Processor: %c", pWtpStatus->Data[11] );
        DBG( "%c", pWtpStatus->Data[10] );
        DBG( "%c", pWtpStatus->Data[9] );
        DBG( "%c\n", pWtpStatus->Data[8] );
    }
    else
    {
        //Do nothing!
    }

    HandlePendingMessages( pWtpStatus );

    return IsOk;
}

bool CWtpComm::SelectImage (PDWORD pImageType)
{
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {SELECTIMAGE,iSequence,0x00,0x00,0x00000000};
    WTPSTATUS* pWtpStatus = new WTPSTATUS;
    DWORD BytesRead = 0;
    DWORD StatusLength = 0;

    memset( pWtpStatus, 0, sizeof(WTPSTATUS) );
    HandlePendingMessages( pWtpStatus );

    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,8,pWtpStatus,6+sizeof(*pImageType)) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,8) == false) IsOk = false;
            if (ReadUart (pWtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent SelectImage. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    delete pWtpStatus;
                    return IsOk;
                }
            }

            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    *pImageType = pWtpStatus->Data[0];
    *pImageType |= (pWtpStatus->Data[1] << 8);
    *pImageType |= (pWtpStatus->Data[2] << 16);
    *pImageType |= (pWtpStatus->Data[3] << 24);

    if (pWtpStatus->Status == NACK)
    {
        IsOk = false;
        sprintf (szMessage,"  Error: SelectImage returned a NACK!");
#ifdef _CONSOLE
        DBG( "%s\n", szMessage );
#else
#if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }

    HandlePendingMessages( pWtpStatus );

    delete pWtpStatus;
    return IsOk;
}

bool CWtpComm::VerifyImage (BYTE AckOrNack)
{
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {VERIFYIMAGE,iSequence,0x00,0x00,0x00000001,AckOrNack};
    WTPSTATUS* pWtpStatus = new WTPSTATUS;
    DWORD BytesRead = 0;
    DWORD StatusLength = 0;

    memset( pWtpStatus, 0, sizeof(WTPSTATUS) );
    HandlePendingMessages( pWtpStatus );

    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,9,pWtpStatus,6) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,9) == false) IsOk = false;
            if (ReadUart (pWtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent VerifyImage. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");

                    delete pWtpStatus;
                    return IsOk;
                }
            }

            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                IsOk = false;

                break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    if (AckOrNack == NACK)
    {
        IsOk = false;
        DBG( "  Error: Image asked for by target does not exist!\n");
    }

    if (pWtpStatus->Status == NACK)
    {
        IsOk = false;
        sprintf (szMessage,"  Error: VerifyImage returned a NACK!");
 #ifdef _CONSOLE
        DBG( "\n%s\n", szMessage );
 #else
 #if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }

    HandlePendingMessages( pWtpStatus );

    delete pWtpStatus;
    return IsOk;
}

bool CWtpComm::DataHeader (unsigned int uiRemainingData, WTPSTATUS *pWtpStatus)
{
    bool IsOk = true;
    WTPCOMMAND WtpCmd = {DATAHEADER,++iSequence,0x00,0x00,0x00000004};
    DWORD BytesRead = 0;
    DWORD StatusLength = 0;

    if ( CommandLineParser.FastDownload() )
        WtpCmd.Flags = 0x4;

    HandlePendingMessages( pWtpStatus );

    WtpCmd.Data[0] = uiRemainingData & 0x000000FF;
    WtpCmd.Data[1] = (uiRemainingData & 0x0000FF00) >> 8;
    WtpCmd.Data[2] = (uiRemainingData & 0x00FF0000) >> 16;
    WtpCmd.Data[3] = (uiRemainingData & 0xFF000000) >> 24;

    pWtpStatus->Status = ACK;
    switch (CommandLineParser.PortType())
    {
        case USB:        
            if (WriteUsb (&WtpCmd,12,pWtpStatus,10) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,12) == false) IsOk = false;
            if (ReadUart (pWtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent DataHeader. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    return IsOk;
                }
            }

            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    if (pWtpStatus->Status == NACK)
    {
        IsOk = false;
        sprintf (szMessage,"  Error: DataHeader returned a NACK!");
 #ifdef _CONSOLE
        DBG( "\n%s\n", szMessage );
 #else
 #if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }

    HandlePendingMessages( pWtpStatus );

    return IsOk;
}

bool CWtpComm::Data (PBYTE pData,long Length)
{
    bool IsOk = true;
    WTPCOMMAND WtpCmd = {DATA,++iSequence,0x00,0x00,Length};
    WTPSTATUS* pWtpStatus = new WTPSTATUS;
    DWORD BytesRead = 0, StatusLength = 0;
    int i = 0;
    
    memset( pWtpStatus, 0, sizeof(WTPSTATUS) );
    HandlePendingMessages( pWtpStatus );

    for (int i = 0; i < (int)Length; i++)
        WtpCmd.Data[i] = pData[i];

    switch (CommandLineParser.PortType())
    {
        case USB:
             // Note: this needs to be added to get rid of the 8 bytes of cmd data.
             // This is part of speeding up the download process.   
            if ( CommandLineParser.FastDownload() )
            {
                if (WriteUsbNoResp (pData,(DWORD)Length) == false) IsOk = false;
                if (isLastData(GET, FDMODE))   
                       ReadUsb(pWtpStatus, 6);
                break;
            }
            if (WriteUsb (&WtpCmd,8 + (DWORD)Length,pWtpStatus,6) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,8 + (DWORD)Length) == false) IsOk = false;
            if (ReadUart (pWtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent Data. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    delete pWtpStatus;
                    return IsOk;
                }
            }

            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
    }

    if ( !CommandLineParser.FastDownload() || isLastData(GET, FDMODE) )
    {
        if (pWtpStatus->Status == NACK)
        {
            IsOk = false;
            sprintf (szMessage,"  Error: Data returned a NACK!");
 #ifdef _CONSOLE
            DBG( "\n%s\n", szMessage );
 #else
 #if WINDOWS
            gThMsg (szMessage);
#endif
#endif
        }
    }

    HandlePendingMessages( pWtpStatus );

    delete pWtpStatus;
    return IsOk;
}

bool CWtpComm::Done ()
{
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {DONE,iSequence,0x00,0x00,0x00000000};
    WTPSTATUS* pWtpStatus = new WTPSTATUS;
    DWORD BytesRead = 0;
    DWORD StatusLength = 0;

    memset( pWtpStatus, 0, sizeof(WTPSTATUS));
    HandlePendingMessages( pWtpStatus );

    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,8,pWtpStatus,6) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,8) == false) IsOk = false;
            if (ReadUart (pWtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent Done. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    delete pWtpStatus;
                    return IsOk;
                }
            }

            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    if (pWtpStatus->Status == NACK)
    {
        IsOk = false;
        sprintf (szMessage,"  Error: Done returned a NACK!");
 #ifdef _CONSOLE
        DBG( "\n%s\n", szMessage );
 #else
 #if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }

//    HandlePendingMessages( pWtpStatus );

    delete pWtpStatus;
    return IsOk;
}

bool CWtpComm::Disconnect (WTPSTATUS* pWtpStatus)
{
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {DISCONNECT,iSequence,0x00,0x00,0x00000000};
    DWORD BytesRead = 0;
    DWORD StatusLength = 0;

//    HandlePendingMessages( pWtpStatus );

    //attempt to disconnect: first try
    pWtpStatus->Status = ACK;
    //pWtpStatus = NULL;
    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,8,pWtpStatus,6) == false) IsOk = false; 
            break;

        case UART:
            if (WriteUart (&WtpCmd,8) == false) IsOk = false;
            if (ReadUart (pWtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent Disconnect. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    return IsOk;
                }
            }

            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    //if there is an NACK, target most likely has messages to send
    //attempt to read messages
    if (pWtpStatus->Status == NACK)
    {
        //now try disconnect once more
        pWtpStatus->Status = ACK;

//        HandlePendingMessages( pWtpStatus );

        switch (CommandLineParser.PortType())
        {
            case USB:
                if (WriteUsb (&WtpCmd,8,pWtpStatus,6) == false) IsOk = false;
                break;

            case UART:
                if (WriteUart (&WtpCmd,8) == false) IsOk = false;
                if (ReadUart (pWtpStatus,6,&BytesRead) == false)
                {
                    DBG( "Sent Disconnect. Host waiting for target to reply!\n");
                    IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                    if (IsOk == false)
                    {
                        DBG( "Did not get a response from the target!\n");
                        return IsOk;
                    }
                }
            
                StatusLength = (DWORD)pWtpStatus->DLEN;
                if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                    IsOk = false;

                break;

            default:
                CommandLineParser.PortType( UNKNOWNPORT );
                break;
        }
    }

//    HandlePendingMessages( pWtpStatus );

    if(IsOk == false)
        pWtpStatus->Status = NACK;

    return IsOk;
}

bool CWtpComm::PublicKey ()
{
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {PUBLICKEY,iSequence,0x00,0x00,0x00000000};
    WTPSTATUS *pWtpStatus = new WTPSTATUS;
    DWORD BytesRead, StatusLength;
    int i = 0;

    memset( pWtpStatus, 0, sizeof(WTPSTATUS) );
    HandlePendingMessages( pWtpStatus );

    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,8,pWtpStatus,10) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd, 8) == false) IsOk = false;
            if (ReadUart (pWtpStatus, 6, &BytesRead) == false)
            {
                DBG( "Sent Disconnect. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    delete pWtpStatus;
                    return IsOk;
                }
            }
            
            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    if (pWtpStatus->Status == NACK)
    {
        IsOk = false;
        sprintf (szMessage,"  Error: PublicKey returned a NACK!");
 #ifdef _CONSOLE
        DBG( "\n%s\n", szMessage );
 #else
 #if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }

    HandlePendingMessages( pWtpStatus );

    delete (pWtpStatus);
    return IsOk;
}

bool CWtpComm::Password (WTPSTATUS *pWtpStatus)
{
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {PASSWORD,iSequence,0x00,0x00,0x00000000};
    DWORD BytesRead = 0;
    DWORD StatusLength = 0;

    HandlePendingMessages( pWtpStatus );

    pWtpStatus->Status = ACK;
    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,8,pWtpStatus,14) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,8) == false) IsOk = false;
            if (ReadUart (pWtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent Disconnect. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    return IsOk;
                }
            }
            
            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    if (pWtpStatus->Status == NACK)
    {
        IsOk = false;
        sprintf (szMessage,"  Error: Password returned a NACK!");
 #ifdef _CONSOLE
        DBG( "\n%s\n", szMessage );
 #else
 #if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }

    HandlePendingMessages( pWtpStatus );

    return IsOk;
}

bool CWtpComm::SignedPassword ()
{
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {SIGNEDPASSWORD,iSequence,0x00,0x00,0x00000000};
    WTPSTATUS *pWtpStatus = new WTPSTATUS;
    DWORD BytesRead = 0;
    DWORD StatusLength = 0;

    memset( pWtpStatus, 0, sizeof(WTPSTATUS) );
    HandlePendingMessages( pWtpStatus );

    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,8,pWtpStatus,6) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,8) == false) IsOk = false;
            if (ReadUart (pWtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent Disconnect. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    delete pWtpStatus;
                    return IsOk;
                }
            }

            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    if (pWtpStatus->Status == NACK)
    {
        IsOk = false;
        sprintf (szMessage,"  Error: SignedPassword returned a NACK!");
 #ifdef _CONSOLE
        DBG( "\n%s\n", szMessage );
 #else
 #if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }

    HandlePendingMessages( pWtpStatus );

    delete (pWtpStatus);
    return IsOk;
}

bool CWtpComm::OtpView ()
{
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {OTPVIEW,iSequence,0x00,0x00,0x00000001,CommandLineParser.OtpFlashType()};
    WTPSTATUS* pWtpStatus = new WTPSTATUS;
    DWORD BytesRead = 0;
    DWORD StatusLength = 0;
    int i;

    memset( pWtpStatus, 0, sizeof(WTPSTATUS) );
    HandlePendingMessages( pWtpStatus );

    // Use this method to view OTP register values. It will be necessary to
    // specify the flash type when using this feature.
    DBG( "Flash Type Selected: %d\n", CommandLineParser.OtpFlashType() );

    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,9,pWtpStatus,46) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,9) == false) IsOk = false;
            if (ReadUart (pWtpStatus,6,&BytesRead) == false) IsOk = false;
            {
                DBG( "Sent OtpView. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    delete pWtpStatus;
                    return IsOk;
                }
            }

            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    if (pWtpStatus->Status == NACK)
    {
        IsOk = true;//don't return an error
        sprintf (szMessage,"  Error: OtpView returned a NACK!");
 #ifdef _CONSOLE
        DBG( "\n%s\n", szMessage );
 #else
 #if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }
    else
    {
        if ((pWtpStatus->Flags & 0x40) == 0x40)
            DBG( "OTP Register 0 is Locked\n");
        else
            DBG( "OTP Register 0 is Unlocked\n");

        if ((pWtpStatus->Flags & 0x80) == 0x80)
            DBG( "OTP Register 1 is Locked\n");
        else
            DBG( "OTP Register 1 is Unlocked\n");

        DBG( "OTP Register 0:");

        for (i = 0; i < 8; i++)
            DBG( "0x%08x\n", pWtpStatus->Data[i] );

        DBG( "OTP Register 1:");
        for (i = 8; i < 24; i++) 
            DBG( "0x%08x\n", pWtpStatus->Data[i] );

        DBG( "OTP Register 2:");
        for (i = 24; i < 40; i++) 
            DBG( "0x%08x\n", pWtpStatus->Data[i] );
    }

    HandlePendingMessages( pWtpStatus );

    delete pWtpStatus;
    return IsOk;
}

bool CWtpComm::GetWtpMessage (WTPSTATUS *pWtpStatus)
{
    bool IsOk = true;
    iSequence = 0x00;
    unsigned long int Seconds = 0, DelayTime = 60;
    WTPCOMMAND WtpCmd = {MESSAGE,iSequence,0x00,0x00,0x00000000};
    DWORD StatusLength, BytesRead;
    char WtpMessage[MESSAGELENGTH];
    int i;

    memset (WtpMessage,'\0',MESSAGELENGTH);

    pWtpStatus->Status = ACK;
    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,8,pWtpStatus,6 + MESSAGELENGTH) == false) IsOk = false;
            if (!(pWtpStatus->DLEN > 0)) {pWtpStatus->Status = NACK; break;}
            if (pWtpStatus->Status != ACK) {pWtpStatus->Status = NACK; break;}

            if (((pWtpStatus->Flags & 0x02) >> 1) == MESSAGESTRING)
            {
                StatusLength = (DWORD)pWtpStatus->DLEN;
                DBG( "---\n");

#ifdef _CONSOLE
                for (i = 0; i < (int)StatusLength; i++)
                    DBG( "%c", pWtpStatus->Data[i] );

                DBG( "---\n");
#else
                memcpy (WtpMessage,pWtpStatus->Data,StatusLength);
#if WINDOWS
                gThMsg (WtpMessage);
#endif
#endif
            }
            else
            {
                for (i = 0; i < MAXERRORCODES; i++)
                {
                    if (ErrorCodes[i].ErrorCode == pWtpStatus->Data[0])
                    {
#ifdef _CONSOLE
                        DBG( "Code: %s ", ErrorCodes[i].Description );
                        DBG( "0x%x\n", pWtpStatus->Data[0] );
#else
                        sprintf (WtpMessage,"Error Code: %s",ErrorCodes[i].Description);
#if WINDOWS
                        gThMsg (WtpMessage);
#endif
#endif
                    }
                }
            }
            break;

        case UART:
            if (WriteUart (&WtpCmd,8) == false) IsOk = false;
            if (ReadUart (pWtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent GetWtpMessage. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    return IsOk;
                }
            }

            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( (BYTE*)&WtpMessage, StatusLength ) )
                IsOk = false;
            
            memcpy ( pWtpStatus->Data, WtpMessage, StatusLength );
            if (((pWtpStatus->Flags & 0x02) >> 1) == MESSAGESTRING)
            {
                DBG( "---\n");
#ifdef _CONSOLE
                for (i = 0; i < (int)StatusLength; i++)
                    if ( WtpMessage[i] != 0 )
                        DBG( "%c", WtpMessage[i] );

                DBG( "---\n");
#else
#if WINDOWS
                gThMsg (WtpMessage);
#endif
#endif
            }
            else
            {
                for (i = 0; i < MAXERRORCODES; i++)
                {
                    if (ErrorCodes[i].ErrorCode == WtpMessage[0])
                    {
#ifdef _CONSOLE
                        DBG( "Code: %s ", ErrorCodes[i].Description );
                        DBG( "0x%x\n", WtpMessage[0] );
#else
                        sprintf (WtpMessage,"Code: %s",ErrorCodes[i].Description);
#if WINDOWS
                        gThMsg (WtpMessage);
#endif
#endif
                    }
                }
            }
            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    if (pWtpStatus->Status == NACK)
    {
        IsOk = false;
//		sprintf (szMessage,"GetWtpMessage returned a NACK!");
 #ifdef _CONSOLE
//		DBG( "\n%s\n", szMessage );
 #else
 #if WINDOWS
//        gThMsg (szMessage);
#endif
#endif
    }

    return IsOk;
}

bool CWtpComm::GetTargetMessage (WTPSTATUS *pWtpStatus)
{
    bool IsOk = true;
    unsigned long int Seconds = 0, DelayTime = 120;

    Seconds = GetTickCount () / 1000L;

    while ( (pWtpStatus->Status == ACK) &&
            ((GetTickCount () / 1000L) < (Seconds + DelayTime)) )
    {
//		DBG( "Message Pending Flag status: %X\n", pWtpStatus->Flags );

        if (GetWtpMessage (pWtpStatus) == false) {IsOk = false; break;}
        
        // This is to add a delay for WTPTP while DKB is busy on the target.
        if (pWtpStatus->Data[0] == PlatformBusy )
        {
            do{
                GetWtpMessage(pWtpStatus);
                if ( pWtpStatus->Data[0] == PlatformDisconnect )
                    break;
                // this Sleep is to allow the target a little time to do work before
                // having to handle the next message
                if ( CommandLineParser.USBPacketSize() < 2000 ) // target running bootrom
                    Sleep(40);
                else
                    Sleep(10); // target running dkb
            }while(pWtpStatus->Data[0] != PlatformReady);
        }

        if ( pWtpStatus->Data[0] == PlatformDisconnect )
        {
            DBG( "Target requested disconnect!!!\n");
            return false;
        }

        if (((pWtpStatus->Flags & 0x1) != MESSAGEPENDING)){ break; }

//		DBG( "Next Message Pending Flag status: 0x%08x\n", pWtpStatus->Flags );
    }

    return IsOk;
}

bool CWtpComm::DebugBoot (PBYTE pFlashType)
{
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {DEBUGBOOT,iSequence,0x00,0x00,0x00000001,CommandLineParser.OtpFlashType()};
    WTPSTATUS *pWtpStatus = new WTPSTATUS;
    DWORD BytesRead = 0;
    DWORD StatusLength = 0;

    memset( pWtpStatus, 0, sizeof(WTPSTATUS) );
    HandlePendingMessages( pWtpStatus );

    WtpCmd.Data[0] = *pFlashType;

    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,9,pWtpStatus,6) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,9) == false) IsOk = false;
            if (ReadUart (pWtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent DebugBoot. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (pWtpStatus, BytesRead, 6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    return IsOk;
                }
            }

            StatusLength = (DWORD)pWtpStatus->DLEN;
            if ( !UARTReadCharWithTimeout( pWtpStatus->Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
    }

    if (pWtpStatus->Status == NACK)
    {
        IsOk = false;
        sprintf (szMessage,"  Error: BootState returned a NACK!");
#ifdef _CONSOLE
        DBG( "\n%s\n", szMessage );
#else
#if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }

    HandlePendingMessages( pWtpStatus );

    delete (pWtpStatus);
    return IsOk;
}

bool CWtpComm::WaitForHostReply (WTPSTATUS *pWtpStatus,int nCharsRead,int nTotalChars)
{
    bool IsOk = true;
    int nCharacters = nTotalChars - nCharsRead;
    unsigned long int Seconds, DelayTime = 360, ReadSeconds, ReadInterval = 10;
    unsigned long int MessageInterval = 150, MsgSeconds;
//  unsigned long int Seconds, DelayTime = 180, ReadSeconds, ReadInterval = 10;
//  unsigned long int MessageInterval = 10, MsgSeconds;
    DWORD BytesRead = 0;
    char Data[RETURNDATALENGTH + 6];

    // This routine is only called if the UART did not read all of the expected
    // characters back from the target device. This will simply give the UART
    // a chance to receive the remaining characters. The DelayTime variable can
    // be changed to set the maximum time allowable for receiving what is left.

    memcpy( Data, pWtpStatus, nCharsRead );

    Seconds = GetTickCount () / 1000L;
    MsgSeconds = GetTickCount () / 1000L;
    ReadSeconds = GetTickCount () / 1000L;

    EnableWaitCommEvent (FALSE);
//    EnableUartVerboseMode (true); // Set to true for UART debug info
    EnableUartVerboseMode (false); // Set to true for UART debug info

    while ((GetTickCount () / 1000L) < (Seconds + DelayTime))
    {
        if ((GetTickCount () / 1000L) > (MsgSeconds + MessageInterval))
        {
            DBG( "Host waiting for target to reply!\n");
            if (CommandLineParser.Verbose())
                DBG( "Number of chars read so far: %d\n", nCharsRead );

            MsgSeconds = GetTickCount () / 1000L;
        }
        if ((GetTickCount () / 1000L) > (ReadSeconds + ReadInterval))
        {
            ReadUartCharBuffer (&Data[nCharsRead],nCharacters,&BytesRead);
            ReadSeconds = GetTickCount () / 1000L;
        }
        nCharacters -= BytesRead; nCharsRead += BytesRead;
        if (nCharacters == 0) { IsOk = true; break; } // We got all the characters.
        if (nCharacters < 0) { IsOk = false; break; } // Shouldn't get here.
    }

    memcpy( pWtpStatus, Data, nTotalChars );

    EnableUartVerboseMode (false);
    EnableWaitCommEvent (TRUE);

    return IsOk;
}

void CWtpComm::HandlePendingMessages( WTPSTATUS *pWtpStatus )
{
    // this is a bogus way to let the bootrom that uses a small packetsize
    // have a little more time to process the command
    if ( CommandLineParser.USBPacketSize() < 2000 )
        Sleep(40);

    pWtpStatus->Status = ACK;
    do
    {
        if ( !((CommandLineParser.CurrentMessageMode() != DISABLED ) && ((pWtpStatus->Flags & 0x1) == MESSAGEPENDING)) )
            break;

        GetTargetMessage (pWtpStatus);
    }
    while( pWtpStatus->Status != ACK );
}

void CWtpComm::DebugViewPrint (char *szMessage)
{
#if _DEBUG
    OutputDebugString (szMessage);
#endif 
}




// features added to support upload
bool CWtpComm::GetProtocolVersion ( PROTOCOL_VERSION* pTargetProtocolVersion )
{
    WTPSTATUS WtpStatus = {0};
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {PROTOCOLVERSION,iSequence,0x00,0x00,0x00000000};
    DWORD BytesRead = 0, StatusLength = 0;
    unsigned long int Seconds = 0, DelayTime = 60;
    int i = 0;

    //set to defaults
    HandlePendingMessages( &WtpStatus );

    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,8,&WtpStatus,6+sizeof(PROTOCOL_VERSION)) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,8) == false) IsOk = false;
            if (ReadUart (&WtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent GetProtocolVersion. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (&WtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    return IsOk;
                }
            }

            StatusLength = (DWORD)WtpStatus.DLEN;
            if ( !UARTReadCharWithTimeout( WtpStatus.Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    if (WtpStatus.Status == NACK)
    {
        IsOk = false;
        sprintf (szMessage,"  Error: GetProtocolVersion returned a NACK!");
#ifdef _CONSOLE
        DBG( "%s\n", szMessage );
#else
#if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }
    else if (WtpStatus.Status == ACK)
    {        
        if ( pTargetProtocolVersion )
        {
            if ( (DWORD)WtpStatus.DLEN != sizeof(PROTOCOL_VERSION) )
                DBG( "nProtocolVersion returned LEN is not correct! LEN == %c\n", (DWORD)WtpStatus.DLEN );

            memset( pTargetProtocolVersion, 0, sizeof(PROTOCOL_VERSION) ); 
            pTargetProtocolVersion->MajorVersion = WtpStatus.Data[0];
            pTargetProtocolVersion->MinorVersion = WtpStatus.Data[1];
            pTargetProtocolVersion->Build =        *((short*)&WtpStatus.Data[2]);

            DBG( "\nProtocolVersion: %d", pTargetProtocolVersion->MajorVersion );
            DBG( ".%d", pTargetProtocolVersion->MinorVersion );
            DBG( ".%d\n", pTargetProtocolVersion->Build );
        }
    }
    else
    {
        //Do nothing!
    }

    HandlePendingMessages( &WtpStatus );

    return IsOk;
}

bool CWtpComm::GetParameters ( TARGET_PARAMS* pTargetParams )
{
    WTPSTATUS WtpStatus = {0}; 
    bool IsOk = true, FoundChar = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {GETPARAMETERS,iSequence,0x00,0x00,0x00000000};
    DWORD BytesRead = 0, StatusLength = 0;
    unsigned long int Seconds = 0, DelayTime = 60;
    int i = 0;

    HandlePendingMessages( &WtpStatus );

    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,8,&WtpStatus,6+sizeof(TARGET_PARAMS)) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,8) == false) IsOk = false;
            if (ReadUart (&WtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent GetParameters. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (&WtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    return IsOk;
                }
            }

            StatusLength = (DWORD)WtpStatus.DLEN;
            if ( !UARTReadCharWithTimeout( WtpStatus.Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    if (WtpStatus.Status == NACK)
    {
        IsOk = false;
        sprintf (szMessage,"  Error: GetParameters returned a NACK!");
#ifdef _CONSOLE
        DBG( "%s\n", szMessage );
#else
#if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }
    else if (WtpStatus.Status == ACK)
    {
        if ( WtpStatus.DLEN != sizeof(TARGET_PARAMS) )
            DBG( "GetParameters returned LEN is not correct! LEN == %c\n", (DWORD)WtpStatus.DLEN );

        DBG( "\nBuffer Size: %d", *(unsigned int*)(&WtpStatus.Data[0]) );
        DBG( "\nRsvd1: %d", *(unsigned int*)(&WtpStatus.Data[4]) );
        DBG( "\nRsvd2: %d", *(unsigned int*)(&WtpStatus.Data[8]) );
        DBG( "\nRsvd3: %d\n", *(unsigned int*)(&WtpStatus.Data[12]) );
        
        pTargetParams->BufferSize = *(unsigned int*)(&WtpStatus.Data[0]);
        pTargetParams->Rsvd1 = *(unsigned int*)(&WtpStatus.Data[4]);
        pTargetParams->Rsvd2 = *(unsigned int*)(&WtpStatus.Data[8]);
        pTargetParams->Rsvd3 =*(unsigned int*)(&WtpStatus.Data[12]);
    }
    else
    {
        //Do nothing!
    }

    HandlePendingMessages( &WtpStatus );

    return IsOk;

}

bool CWtpComm::UploadDataHeader ( UPLOAD_DATA_PARAMS* pUploadDataParams )
{
    WTPSTATUS WtpStatus = {0}; 
    bool IsOk = true;
    iSequence = 0x00;
    unsigned int i = 0;
    WTPCOMMAND WtpCmd = {UPLOADDATAHEADER,iSequence,0x00,0x00,sizeof(UPLOAD_DATA_PARAMS)};
    DWORD BytesRead = 0, StatusLength = 0;

    // put upload parameters in 
    memcpy( WtpCmd.Data, pUploadDataParams, sizeof(UPLOAD_DATA_PARAMS) );

    HandlePendingMessages( &WtpStatus );

    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsb (&WtpCmd,8+sizeof(UPLOAD_DATA_PARAMS),&WtpStatus,6) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,8+sizeof(UPLOAD_DATA_PARAMS)) == false) IsOk = false;
            if (ReadUart (&WtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent UploadData. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (&WtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    return IsOk;
                }
            }

            StatusLength = (DWORD)WtpStatus.DLEN;
            if ( !UARTReadCharWithTimeout( WtpStatus.Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
            break;
    }

    if (WtpStatus.Status == NACK)
    {
        IsOk = false;
        sprintf (szMessage,"  Error: UploadDataHeader returned a NACK!");

#ifdef _CONSOLE
        DBG( "%s\n", szMessage );
        if( WtpStatus.DLEN > 0 )
        {
            DBG( "\nErrorCode: %x\n", *(unsigned int*)&WtpStatus.Data[0] );
            for (i = 0; i < MAXERRORCODES; i++)
            {
                if (ErrorCodes[i].ErrorCode == WtpStatus.Data[0])
                {
                    DBG( "Code: %s ", ErrorCodes[i].Description );
                    DBG( "0x%x\n", WtpStatus.Data[0] );
                }
            }
        }
#else
#if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }

    HandlePendingMessages( &WtpStatus );

    return IsOk;
}


bool CWtpComm::UploadData( PBYTE pData, unsigned int Length )
{
    WTPSTATUS WtpStatus = {0};
    bool IsOk = true;
    iSequence = 0x00;
    WTPCOMMAND WtpCmd = {UPLOADDATA,iSequence,0x00,0x00,0x00000000};
    DWORD BytesRead = 0, StatusLength = 0;
    unsigned long int Seconds = 0, DelayTime = 60;
    unsigned int i = 0;
    
    HandlePendingMessages( &WtpStatus );

    switch (CommandLineParser.PortType())
    {
        case USB:
            if (WriteUsbNoResp ((PBYTE)&WtpCmd,8) == false) IsOk = false;
            if (ReadUsb (&WtpStatus, 6+Length) == false) IsOk = false;
            break;

        case UART:
            if (WriteUart (&WtpCmd,8) == false) IsOk = false;
            if (ReadUart (&WtpStatus,6,&BytesRead) == false)
            {
                DBG( "Sent Data. Host waiting for target to reply!\n");
                IsOk = WaitForHostReply (&WtpStatus,BytesRead,6);
                if (IsOk == false)
                {
                    DBG( "  Error: Did not get a response from the target!\n");
                    return IsOk;
                }
            }
            
            // prevent access out of bounds
            StatusLength = min( Length, RETURNDATALENGTH );
            if ( !UARTReadCharWithTimeout( WtpStatus.Data, StatusLength ) )
                IsOk = false;

            break;

        default:
            CommandLineParser.PortType( UNKNOWNPORT );
    }

    if ( WtpStatus.Status == NACK )
    {
        IsOk = false;
        sprintf (szMessage,"  Error: Data returned a NACK!");
#ifdef _CONSOLE
        DBG( "\n%s\n", szMessage );
        if ( WtpStatus.DLEN > 0 )
        {
            DBG( "\nErrorCode: %x\n", *(unsigned int*)&WtpStatus.Data[0] );
            for (i = 0; i < MAXERRORCODES; i++)
            {
                if (ErrorCodes[i].ErrorCode == WtpStatus.Data[0])
                {
                    DBG( "Code: %s ", ErrorCodes[i].Description );
                    DBG( "0x%x\n", WtpStatus.Data[0] );
                }
            }
        }
#else
#if WINDOWS
        gThMsg (szMessage);
#endif
#endif
    }

    // copy data back to data buffer
    // exclude the 6 bytes used for the wtpstatus at the head of the packet
    memcpy( pData, WtpStatus.Data, Length );

    HandlePendingMessages( &WtpStatus );

    return IsOk;
}

bool CWtpComm::UARTReadCharWithTimeout( PBYTE pData, DWORD DataLength )
{
    unsigned long int Seconds = 0, DelayTime = 60;
    bool bFoundChar = true;
    DWORD BytesRead = 0;
    
    EnableWaitCommEvent (FALSE);

    // Grab the remaining bytes determined by StatusLength
    if ( DataLength > 0 )
    {
        for ( DWORD i = 0; i < DataLength; i++)
        {
            BytesRead = 0;
            ReadUartCharBuffer ((char*)&pData[i],1,&BytesRead);
            if ( BytesRead != 1 ) 
            { 
                Seconds = GetTickCount () / 1000L;
                while ((GetTickCount () / 1000L) < (Seconds + DelayTime))
                {
                    ReadUartCharBuffer( (char*)&pData[i], 1, &BytesRead );
                    if ( BytesRead == 1 ) 
                    { 
                        bFoundChar = true; 
                        break; 
                    }
                }
            }

            if (bFoundChar == false)
            {
                DBG( "  Error: Lost UART syncronization during Status Data Operation!\n");
                return false;
            }
        }
    }

    EnableWaitCommEvent (TRUE);

    return true;
}
