
#include <stdlib.h>
#include <stdarg.h>

#include "WtpLog.h"

WtpLogSetting* WtpLogSetting::singleton(0);	

WtpLogSetting::WtpLogSetting():
mLogLevel(WtpLogSetting::DEFAULT_LEVEL)
{
}



void WtpLogSetting::setLevel(unsigned int level)
{
    mLogLevel = level;
}
WtpLogSetting& WtpLogSetting::getInstance()
{
    
    WtpLogSetting* instance = singleton;
    if (instance == 0) {
        instance = new WtpLogSetting();
        singleton = instance;
    }
    return *instance;
}




WtpLog* WtpLog::singleton(0);

WtpLog& WtpLog::getInstance()
{
    WtpLog* instance = singleton;
    if (instance == 0) {
        instance = new WtpLog();
        singleton = instance;
    }
    return *instance;
    
}
WtpLog::WtpLog():bInit(false),hLog(0){}
WtpLog::~WtpLog(){
    if(hLog){
            fflush(hLog);
            fclose(hLog);
   }
    
}

void WtpLog::Init()
{   
    WtpLog& log = WtpLog::getInstance();

    if(!log.bInit)
    {
        log.hLog = fopen("WtpDownloadLog","a+");
        log.bInit=true;
    }
        
}

int WtpLog::LOG(const char* fmt,...)
{
    WtpLog& log = WtpLog::getInstance();
    int ret=0;
    Init();
    if (log.hLog)
    {
        va_list ap;
        va_start(ap, fmt);
        ret=vfprintf(log.hLog,fmt,ap);
        va_end(ap);
    }
    //always output standard iostream
    {
        
        va_list ap;            
        va_start(ap, fmt);
        ret=vprintf(fmt,ap);
        va_end(ap);

    }
    return ret;

    
}



