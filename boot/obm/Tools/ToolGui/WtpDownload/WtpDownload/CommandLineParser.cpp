/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#include <stdafx.h>
#include <CommandLineParser.h>

#include <../../common/tim.h>
#include <FlashCodes.h>
#include <WtpLog.h>

#pragma warning ( disable : 4244 4715 4047 4024 4996 ) // Disable warning messages

#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }

CCommandLineParser::CCommandLineParser(void)
{
    Reset();
}

CCommandLineParser::~CCommandLineParser(void)
{
    Reset();
}

void CCommandLineParser::Reset()
{
    bVerbose = false;
    bTimFile = false;
    bImageFile = false;
    bFbfFile = false;
    bMessageMode = false;
    bPartitionFile = false;
    bOtpView = false;
    iOtpFlashType = NO_FLASH_P;
    bDebugBoot = false;
    bPortType = false;
    bJtagFile = false;
    bLogFile = false;
    bPlatform = false;
    bUSBInterface = false;
    bUSBPacketSize = false;
    iUSBPacketSize = PACKET_SIZE;
    iUARTDelay = 200;
    bUARTDelay = false;
    iDownloadBaudRate = 0;
    bBaudRate = false;
    bFastDownload = true;
    iPortType = UNKNOWNPORT;
    iCommPort = 0;
    bCommPort = false;
    iPlatformType = PXA30x;
    iCurrentMessageMode = MSG_AND_DOWNLOAD;
    bUploadSpecFile = false;

    t_stringVecIter iter = ImageFileNames.begin();
    while( iter != ImageFileNames.end() )
    {
        delete *iter++;
    }
    ImageFileNames.clear();

    t_ImageFileVecIter iterImage = ImageFilesVec.begin();
    while( iterImage != ImageFilesVec.end() )
    {
        delete *iterImage++;
    }
    ImageFilesVec.clear();

    FileIndex = 0;
} 

bool CCommandLineParser::ParseCommandLine (int argc,char *argv[])
{
    int currentOption = 1;
    BYTE Option = 0;
    IMAGEFILE* pImageFile = 0;

    while (currentOption < argc)
    {
        if ((*argv[currentOption] == '-') || (*argv[currentOption] == '/'))
        {
            Option = *++argv[currentOption++];
#if WTPTP_3_2_X != 1
            Option = toupper(Option);
#endif			
            switch (Option)
            {
                case HELP:
                    PrintUsage();
                    break;

                case VERBOSE:
                    Verbose( true );
                    break;

                case TIM_BIN_FILE:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error: Command Line Parsing Error: -%c switch: ", (char)TIM_BIN_FILE );
                        printf (" Missing input file name parameter.\n");
                        return false;
                    }
                    
                    TimFileName( argv[currentOption] );

                    pImageFile = new IMAGEFILE;
                    strcpy( pImageFile->FileName, TimFileName().c_str() );
                    pImageFile->FileType = TIMIDENTIFIER;
                    ImageFilesVec.push_back( pImageFile );
                    break;

                case IMAGE_FILE:
                    {
                        if (currentOption == argc)
                        {
                            printf ("\n  Error: Command Line Parsing Error: -%c switch: ", (char)IMAGE_FILE );
                            printf (" Missing input file name parameter.\n");
                            return false;
                        }
                        
                        ImageFileName( argv[currentOption] );
                        pImageFile = new IMAGEFILE;
                        strcpy( pImageFile->FileName, argv[currentOption] );
                        pImageFile->FileType = OBMIDENTIFIER + ImageFilesVec.size();
                        ImageFilesVec.push_back( pImageFile );
                    }
                    break;

                case FBF_FILE:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error: Command Line Parsing Error: -%c switch: ", (char)FBF_FILE );
                        printf (" Missing input file name parameter.\n");
                        return false;
                    }
                    FbfFileName( argv[currentOption] );
                    pImageFile = new IMAGEFILE;
                    strcpy( pImageFile->FileName, argv[currentOption] );
                    pImageFile->FileType = FBFIDENTIFIER;
                    ImageFilesVec.push_back( pImageFile );
                    break;

                case JTAG_KEY:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error: Command Line Parsing Error: -%c switch: ", (char)JTAG_KEY );
                        printf (" Missing input file name parameter.\n");
                        return false;
                    }
                    JtagFileName( string(argv[currentOption]) );;
                    pImageFile = new IMAGEFILE;
                    strcpy( pImageFile->FileName, argv[currentOption] );
                    pImageFile->FileType = JTAGIDENTIFIER;
                    ImageFilesVec.push_back( pImageFile );
                    break;

                case MESSAGE_MODE:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error: Missing message mode parameter.\n", 0 );
                        return FALSE;
                    }
                    if ( atoi (argv[currentOption]) == 1 )
                    {
                        CurrentMessageMode( DISABLED );
                        bMessageMode = false;
                    }
                    else if ( atoi (argv[currentOption]) == 2 )
                    {
                        CurrentMessageMode( MSG_ONLY );
                        bMessageMode = true;
                    }
                    else if  ( atoi (argv[currentOption]) == 3 )
                    {
                        CurrentMessageMode( MSG_AND_DOWNLOAD );
                        bMessageMode = true;
                    }
                    else
                    {
                        printf ("\n  Error: Message mode parameter value not recognized.\n", 0 );
                        return FALSE;
                    }
                    break;

                case OTP_FLASH_TYPE:
                    if (currentOption == argc)
                    {
                        printf ( "\n  Error: Missing OTP flash type parameter.\n" );
                        return false;

                    }
                    if (atoi (argv[currentOption]) >= MAXFLASHTYPES)
                    {
                        printf ( "\nError: FLASHTYPE can only be set to numbers 0 thru %d!\n", MAXFLASHTYPES - 1 );
                        return false;
                    }
                    OtpFlashType( atoi(argv[currentOption]) );
                    break;

                case DEBUG_BOOT:
                    DebugBoot( true );
                    break;

                case PORT_TYPE:
                    if (currentOption == argc)
                    {
                        printf ( "\n  Error: Missing Port Type parameter.\n" );
                        return false;
                    }
                    if (strstr(strupr(argv[currentOption]),"USB") != NULL)
                        PortType( USB );
                    else if (strstr(strupr(argv[currentOption]),"UART") != NULL)
                        PortType( UART );
                    else
                    {
                        printf ( "\n  Error: Port Type parameter must be USB or UART.\n" );
                        return false;
                    }
                    break;

                case COMM_PORT:
                    if (currentOption == argc)
                    {
                        printf( "\n  Error: Missing Com port parameter.\n" );
                        return FALSE;
                    }
                    CommPort( atoi(argv[currentOption]) );
                    break;


                case INTERFACE_NAME:
                    if (currentOption == argc)
                    {
                        printf ( "\n  Error: Missing interface name parameter.\n" );
                        return false;
                    }
                    USBInterfaceName( argv[currentOption] );
                    break;

                case USB_PACKET_SIZE:
                    if (currentOption == argc)
                    {
                        printf ( "\n  Error: USB Packet Size parameter.\n" );
                        return false;
                    }
                    USBPacketSize( atoi(argv[currentOption]) );
                    break;

                case UART_DELAY:
                    if (currentOption == argc)
                    {
                        printf ( "\n  Error: Missing Com port parameter.\n" );
                        return false;
                    }
                    UARTDelay( atoi(argv[currentOption]) );
                    break;

                case BAUD_RATE:
                    if (currentOption == argc)
                    {
                        printf ( "\n  Error: Missing Com port baudrate parameter.\n" );
                        return FALSE;
                    }
                    DownloadBaudRate( atoi (argv[currentOption]) );
                    break;

                case SCRIPT_FILE:
                    // script file is not handled here but is parsed separately
                    break;

                case PLATFORM_TYPE:
                    if ( currentOption == argc || (iPlatformType <= PXAMIN_PT || iPlatformType > PXAMAX_PT) )
                    {
                        printf ( "\n  Error:   Command Line Parsing Error." );
                        printf ( "  Missing or invalid platform type parameter.\n" );
                        return FALSE;
                    }
                    PlatformType( (ePlatformType)atoi(argv[currentOption]) );
                    break;

                case PARTITION_TABLE:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error: Command Line Parsing Error: -%c switch: ", (char)PARTITION_TABLE );
                        printf (" Missing input file name parameter.\n");
                        return false;
                    }

                    PartitionFileName( argv[currentOption] );
                    pImageFile = new IMAGEFILE;
                    strcpy( pImageFile->FileName, argv[currentOption] );
                    pImageFile->FileType = PARTIONIDENTIFIER;
                    ImageFilesVec.push_back( pImageFile );
                    break;

                case DISABLE_FAST_DOWNLOAD:
                    printf ( "\n  Warning: Fast Download has been disabled!\n" );
                    FastDownload( false );
                    break;

                case LOG_FILE:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error: Command Line Parsing Error: -%c switch: ", (char)LOG_FILE );
                        printf (" Missing input file name parameter.\n");
                        return false;
                    }
                    
                    LogFileName( argv[currentOption] );
                    break;

                case UPLOAD_DATA:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error: Command Line Parsing Error: -%c switch: ", (char)UPLOAD_DATA );
                        printf (" Missing input file name parameter.\n");
                        return false;
                    }
                    
                    UploadSpecFileName( argv[currentOption] );
                    break;

                default:
                    printf ("\n  Error: Command line option -%c is invalid!\n",Option);
                    return false;
            }
        }
        else
        {
            currentOption++;
        }
    }

    // force the recalc of USBPacketSize depending on bFastDownload
    USBPacketSize( USBPacketSize() );
    // disable fastdownload when using uart or for jtag reenable
    if ( PortType() == UART || JtagFile() )
        FastDownload(false);

    if (CheckYourOptions () == false) return false;

    return true;
}

void CCommandLineParser::USBPacketSize( int iSize ) 
{ 
    if ( bFastDownload )
    {
        iUSBPacketSize = (((iSize+511)/512) * 512);
        bUSBPacketSize = true; 
    }
    else
    {
        iUSBPacketSize = (((iSize+63)/64) * 64)-8; 
        bUSBPacketSize = true; 
    }
}

bool CCommandLineParser::CheckYourOptions ()
{
    int nOptionsEnabled = 0;

    if (bTimFile)
    {
        if (bOtpView || bDebugBoot  || (iCurrentMessageMode == MSG_ONLY))
        {
            printf ( "\n  Error: Cannot select OTP view, JTAG Enablement, Debug Boot with Message Only\n" );
            printf (  "options with TIM file downloads!\n" );
            return false;
        }
    }
    else if( iCurrentMessageMode == MSG_ONLY )
    {
        if (bJtagFile || bOtpView || bDebugBoot || bTimFile || bImageFile )
        {
            printf ( "\n  Error: Can only select port type \n" );
            printf ( "option with Message Mode 3!\n" );
            return FALSE;
        }
    }
    else if( iCurrentMessageMode == MSG_AND_DOWNLOAD)
    {
        if (bOtpView || bDebugBoot )
        {
            printf ( "\n  Error: Cannot select JTAG reenablement, OTP view or Debug Boot \n" );
            printf ( "options with Message and Download Mode!\n" );
            return FALSE;
        }
    }
    else
    {
        if (bJtagFile) nOptionsEnabled++;
        if (bDebugBoot) nOptionsEnabled++;
        if (bOtpView) nOptionsEnabled++;
        if (bImageFile) nOptionsEnabled++;

        if (nOptionsEnabled > 1)
        {
            printf ( "\n  Error: Cannot select image file downloads, OTP view, Debug Boot\n" );
            printf ( "or JTAG reenablement options simultaneously!\n" );
            return FALSE;
        }
    }

    return true;
}

bool CCommandLineParser::ParseTim ()
{
    int i = 0, j = 0;
    UINT_T nImages = 0, *ImageId = 0;
    BOOL IsTim = FALSE;
    DWORD TimId = 0;
    FILE *hTimFile = 0;
    CTIM TimHeader = {0};
    IMAGE_INFO_3_4_0 ImageInfo = {0};
    IMAGEFILE* pTimFile = 0;

    t_ImageFileVecIter Iter = ImageFilesVec.begin();
    while ( Iter != ImageFilesVec.end() )
    {
        if (bVerbose) 
            printf ("\nParseTim opened TIM file: %s, 0x%08x\n", (*Iter)->FileName, (*Iter)->FileType);

        if ( ((*Iter)->FileType & TYPEMASK) == (TIMIDENTIFIER & TYPEMASK ))
        {
            IsTim = TRUE;
            pTimFile = (*Iter);
            break;
        }
        Iter++;
    }

    if (!IsTim)
    {
        printf ("\nA Tim image file was not supplied! Exiting program.\n");
        return false;
    }

    if ((hTimFile = fopen (pTimFile->FileName,"rb")) == NULL)
    {
        printf ("\nfopen failed to open Tim file: %s.\n", pTimFile->FileName);
        return false;
    }

    fseek (hTimFile,sizeof(DWORD),SEEK_SET); // Set position to Version.Identifier
    fread (&TimId,sizeof(DWORD),1,hTimFile);

    if ( (TimId & TYPEMASK) != (TIMIDENTIFIER & TYPEMASK) )
    {
        printf ("\nAn invalid Tim file was supplied\n");
        printf ("Correct ID: 0x%08x, ID of supplied image: %X\n", pTimFile->FileType, TimId);
        if (hTimFile != NULL) fclose (hTimFile);
        return false;
    }

    fseek (hTimFile,0L,SEEK_SET); // Set position to SOF
    fread (&TimHeader,sizeof(CTIM),1,hTimFile);
    nImages = TimHeader.NumImages;

    printf ("Number of Images listed in TIM: %d\n", nImages);

    if (NULL == (ImageId = (UINT_T*)malloc (nImages * sizeof (UINT_T))))
    {
        if (hTimFile != NULL) fclose (hTimFile);
        return false;
    }

    for (i = 0; i < (int)nImages; i++)
    {
        if ( TimHeader.VersionBind.Version >= TIM_3_4_00 )
        {
            // include bigger hash
            fread (&ImageInfo,sizeof(IMAGE_INFO_3_4_0), 1, hTimFile);
        }
        else if ( TimHeader.VersionBind.Version >= TIM_3_3_00 )
        {
            // include bigger hash
            fread (&ImageInfo,sizeof(IMAGE_INFO_3_2_0), 1, hTimFile);
        }
        else if ( TimHeader.VersionBind.Version >= TIM_3_2_00 )
        {
            // include partition number
            fread (&ImageInfo,sizeof(IMAGE_INFO_3_2_0), 1, hTimFile);
        }
        else
        {
            // Remove last word which contains the partition number
            // for versions prior to 0x030202. Not supported.
            fread (&ImageInfo,sizeof(IMAGE_INFO_3_1_0), 1, hTimFile);
        }
        *(ImageId + i) = ImageInfo.ImageID;
    }

    printf ("\nTIM image ID list:\n");
    for (i = 0; i < (int)nImages; i++)
    {
        printf ("Image ID: ");
        for (j = 3; j >= 0; j--)
        {
            printf ("%c",(ImageId[i] >> (j * 8)));
        }
        printf ("\n");
        //printf ("Image ID: 0x%08x\n",ImageId[i]);
    }
    if (hTimFile != NULL) fclose (hTimFile); // Close TIM

    if (SetImageId ( ImageId, (int)nImages ) == FALSE) 
    {
        if (ImageId != NULL) free (ImageId);
        return false;
    }

    if (ImageId != NULL) free (ImageId);
    
    return true;
}

bool CCommandLineParser::SetImageId ( UINT_T *pImageId, int nImages )
{
    int iImageIndex = 0;
    FILE *hFile = 0;
    DWORD FileId = 0;
    BOOL IsImage = FALSE;
    char Message[MAX_PATH] ={0};
    BOOL SECURITYDEBUG = FALSE;

    for (iImageIndex = 0; iImageIndex < nImages; iImageIndex++)
    {
        if ( (pImageId[iImageIndex] & TYPEMASK) == (TIMIDENTIFIER & TYPEMASK) ) continue;
        if (SECURITYDEBUG) printf ("\nID listed in TIM 0x%08x\n",pImageId[iImageIndex]);

        t_ImageFileVecIter Iter = ImageFilesVec.begin();
        while ( Iter != ImageFilesVec.end() )
        {
            if ( ((*Iter)->FileType & TYPEMASK) == (TIMIDENTIFIER & TYPEMASK)) 
            {
                Iter++;
                continue;
            }
            if (SECURITYDEBUG) printf ("Load Image file %s\n", (*Iter)->FileName);
            if ((hFile = fopen ((*Iter)->FileName,"rb")) == NULL)
            {
                sprintf (Message,"\nfopen failed to open %s while obtaining ID.\n", (*Iter)->FileName);
                printf ("%s",Message);
                Iter++;
                continue;
            }

            fseek (hFile,0L,SEEK_SET);
            fread (&FileId,sizeof(DWORD),1,hFile);

            if (SECURITYDEBUG) printf ("ID read from file 0x%08x\n",FileId);
            if (hFile != NULL) fclose (hFile);

            if (FileId == pImageId[iImageIndex])
            {
                (*Iter)->FileType = pImageId[iImageIndex];
                IsImage = TRUE; break;
            }
            Iter++;
        }
        if (!IsImage)
        {
            sprintf (Message,"An image file with ID# 0x%08x was not supplied by user!", pImageId[iImageIndex]);
            printf ("%s\n",Message);
            return FALSE;
        }
        IsImage = FALSE;
    }

    return TRUE;
}

void CCommandLineParser::PrintUsage ()
{
    cout << endl << endl << "Command Line Options and Format: " << endl << endl;

    cout << "Usage:" << endl;
    cout << "======" << endl << endl;

    cout << "Sample command line:" << endl << endl;
    cout << "c:\\wtpdownload.exe <Option>|<Option file> ..." << endl << endl;
    cout << "Options:" << endl;
    cout << "========" << endl << endl;

    cout << " -" << char(VERBOSE)
         << "                         Verbose mode.  Debugging information will be printed." << endl;

    cout << " -" << char(TIM_BIN_FILE)
         << " [TIM.bin]               input TIM file (binary)." << endl;

    cout << " -" << char(IMAGE_FILE)
         << " [Image_h.bin]           input any image file (_h binary)." << endl;

    cout << " -" << char(FBF_FILE)
         << " [FBF.fbf]               input an FBF formatted binary file with multiple images." << endl;

#if TRUSTED
    cout << " -" << char(JTAG_KEY)
         << " [JTAG_key.txt]          input Jtag Key File (ASCII)." << endl;
#endif

    cout << " -" << char(MESSAGE_MODE)
         << " [message mode]          message mode 2 is Messages Only, 3 is Messages and Download." << endl;

    cout << " -" << char(OTP_FLASH_TYPE)
         << " [flash type]            Set flash type between 0 thru 7. Runs the OTP viewer." << endl;

    cout << " -" << char(DEBUG_BOOT)
         << "                         Runs the Debug Boot command." << endl;

    cout << " -" << char(PORT_TYPE)
         << " [Port Type]             Port types: UART or USB." << endl;

    cout << " -" << char(INTERFACE_NAME)
         << " [USB Interface string]  Optional. Valid only when Port type is USB." << endl;

    cout << " -" << char(USB_PACKET_SIZE)
         << " [packet size]           PacketSize: The size of download packets." << endl
         << "                            Actual packet size may be normalized to nearest valid size." << endl;

    cout << " -" << char(UART_DELAY)
         << " [delay value]           The delay time value is in milliseconds. Used with UART." << endl;

    cout << " -" << char(COMM_PORT)
         << " [port number]           Port number. Used with UART." << endl;

    cout << " -" << char(BAUD_RATE)
         << " [baud rate]             Baud rate. Used with UART." << endl;

    cout << " -" << char(SCRIPT_FILE)
         << " [script file name]      Script file." << endl;

    cout << " -" << char(PLATFORM_TYPE)
        << " [processor type]        Platform Type:[<PXA168=0><PXA30x=1><PXA31x=2><PXA32x=3><PXA688=4><PXA91x=5><PXA92x=6>" << endl
        << "                            <PXA93x=7><PXA94x=8><PXA95x=9><ARMADA168=10><ARMADA610=12>" << endl
        << "                            <MG1=13><TAVOR_PV_MG2=14><ESHEL=15><ARMADA620=16><ESHEL_LTE=17><ARMADA622=18><WUKONG=19>]" << endl;

    cout << " -" << char(PARTITION_TABLE)
         << " [partition.bin]         Download partition table bin file." << endl;

    cout << " -" << char(DISABLE_FAST_DOWNLOAD)
         << "                         Disables the fast download mode(if supported)." << endl;

    cout << " -" << char(UPLOAD_DATA)
         << " [uploadspec.txt]        Upload data and save to files as defined in uploadspec.txt ." << endl;

    cout << " -" << char(HELP)
         << "                         Help. Displays this help message." << endl;

    cout << endl << "  NOTE: The -" << char(IMAGE_FILE) << " option is not compatible with the -" << char(JTAG_KEY) << " and -" << char(OTP_FLASH_TYPE) << " options!" << endl;

#if TRUSTED
    cout << endl << "        -" << char(JTAG_KEY) << " is for Jtag Re-Enabling. -" << char(JTAG_KEY) << " is for Downloading." << endl;
#endif

    cout << endl << "        -" << char(OTP_FLASH_TYPE) << " is for the OTP viewer." << endl;
    cout << endl << "        The -" << char(TIM_BIN_FILE) << " option is not compatible with the -" << char(JTAG_KEY) << " and -" << char(OTP_FLASH_TYPE) << " options!" << endl;
    cout << endl << "  Examples:" << endl;
    cout << endl << "  =========" << endl;

#if TRUSTED
    cout << endl << "To use WTPDownload for Jtag Re-Enabling:" << endl;
    cout << endl << "c:\\wtpdownload.exe -"<< char(JTAG_KEY) << " JtagOEMkeyfile.txt" << endl;
#endif

    cout << endl << "To use WTPDownload for Downloading:" << endl;;
    cout << endl << "c:\\wtpdownload.exe -" << char(PORT_TYPE) << " USB -" << char(TIM_BIN_FILE) << " TIM.bin -" << char(IMAGE_FILE) << " OBM_h.bin" << endl << endl;
    cout << endl << "Message-only Mode(Mode 2) Usage:" << endl;
    cout << endl << "================================" << endl;
    cout << endl << "c:\\wtpdownload -" << char(PORT_TYPE) << " USB -" << char(MESSAGE_MODE) << " 2" << endl << endl;
    cout << endl << "Message Mode and Download(Mode 3) Usage:" << endl;
    cout << endl << "========================================" << endl;
    cout << endl << "c:\\wtpdownload -" << char(PORT_TYPE) << " USB -" << char(TIM_BIN_FILE) << " <NTIM/TIM.bin> -" << char(IMAGE_FILE) << " <Image_h.bin> -" << char(MESSAGE_MODE) << " 3" << endl << endl;

#if TRUSTED
    cout << endl << "Download of TIM-JTAG Reenable-Image Usage:" << endl;
    cout << endl << "  ========================" << endl;
    cout << endl << "c:\\wtpdownload -" << char(PORT_TYPE) << " UART -" << char(TIM_BIN_FILE) << " <NTIM/TIM.bin> -" << char(IMAGE_FILE) << " <Image_h.bin> -" << char(JTAG_KEY) << " JtagOEMKeyfile.txt" << endl << endl;
#endif
}

#if TOOLS_GUI == 1
bool CCommandLineParser::ProcessorType( string& sProcessorType )
{
    iProcessorType = PXA_NONE;
    for ( int i = 0; i < PXAMAX_PT; i++ )
    {
        if ( gsProcessorType[i] == sProcessorType )
        {
            iProcessorType = (eProcessorType)i;
            return true;
        }
    }

    return false;
}

string CCommandLineParser::ProcessorType()
{
    if ( iProcessorType > PXA_NONE && iProcessorType < PXAMAX_PT )
        return gsProcessorType[iProcessorType];
    else
        return "";
}

#endif


void CCommandLineParser::DumpConfiguration()
{
    int i;
    ALWAYS("======================================\n");
    ALWAYS("VERBOSE:%s\n",(TRUE==bVerbose)?"TRUE":"FALSE");
    ALWAYS("PORT:%s\n",(UART==iPortType)?"UART":"USB");
    ALWAYS("Message:%s\n",(1==iCurrentMessageMode)?"DOWNLOAD":
            (2==iCurrentMessageMode)?"MSG":"DOWNLOAD+MSG");
    ALWAYS("FASTMODE:%s\n",(TRUE==bFastDownload)?"TRUE":"FALSE");
    ALWAYS("TIM FILE:%s\n",sTimFileName);
    
    t_ImageFileVecIter Iter = ImageFilesVec.begin();
    i=1;
    while ( Iter != ImageFilesVec.end() )
    {
        ALWAYS("IMAGE FILE[%d]:%s\n",i++,(*Iter)->FileName); 
        Iter++;
    }
    
    ALWAYS("======================================\n");
    
}

