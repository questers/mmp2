/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#include <stdafx.h>
#include <WTPUARTLIB.h>

#pragma warning ( disable : 4244 4715 4047 4024 4996) // Disable warning messages

BOOL ISMSGPORT1 = FALSE; // Use this to enable port handshaking errors for debugging.

#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }

HANDLE hUart = INVALID_HANDLE_VALUE;
DCB Dcb;
COMMTIMEOUTS CommTimeOuts = { MAXDWORD,1000,0,1000,0 };
DWORD BytesWritten = 0, BytesRead = 0;
DWORD CommEventMask;
OVERLAPPED Ovl;
COMSTAT ComStat;
BOOL UARTDEBUG = FALSE;
BOOL EnableWaitEvent = TRUE;
COMMCONFIG CommConfig;

BOOL OpenUartPort (UartSettings ComPortConfig)
{
    char pCom[20] = {0};
    char Message[256] = {0};
    DWORD CommConfigSize;
    DWORD ErrorFlags = 0;

//	if ( ComPortConfig.iPortNumber > 9 )
        sprintf (pCom,"\\\\.\\COM%d",ComPortConfig.iPortNumber);
//    else
//	    sprintf (pCom,"COM%d",ComPortConfig.iPortNumber);

    hUart = CreateFile (pCom,GENERIC_READ | GENERIC_WRITE,0,NULL,OPEN_EXISTING,0,NULL);

    if (hUart == INVALID_HANDLE_VALUE) 
    {
        if (ClearCommError (hUart,&ErrorFlags,&ComStat) == NULL)
        {
            sprintf (Message,"  Error: OpenUartPort failed with error %d for %s.\n",GetLastError(),pCom);
            MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        }
        return FALSE;
    }

    if (GetCommState (hUart,&Dcb) == NULL)
    {
        if (ClearCommError (hUart,&ErrorFlags,&ComStat) == NULL)
        {
            sprintf (Message,"  Error: GetCommState failed with error %d for %s.\n",GetLastError(),pCom);
            MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        }
        return FALSE;
    }

    Dcb.EvtChar          = '\001'; // Probably won't be used with new protocal.
    Dcb.fNull            = FALSE;
    Dcb.fInX             = FALSE;
    Dcb.fOutX            = FALSE;
    Dcb.fDsrSensitivity  = FALSE;
    Dcb.fOutxDsrFlow     = FALSE;
    Dcb.fOutxCtsFlow     = FALSE;
    Dcb.fDtrControl      = DTR_CONTROL_DISABLE;
    Dcb.fRtsControl      = RTS_CONTROL_DISABLE;
    Dcb.StopBits         = ONESTOPBIT;
    Dcb.Parity           = NOPARITY;
    Dcb.ByteSize         = 8;

    // baud rate wasn't specified so auto-detect the baud rate.
    if ( ComPortConfig.dwBaudRate == 0 )
    {
        CommConfigSize = sizeof(COMMCONFIG);
        
        if (GetCommConfig (hUart,&CommConfig,&CommConfigSize ) == NULL)
        {
            if (ClearCommError (hUart,&ErrorFlags,&ComStat) == NULL)
            {
                sprintf (Message,"GetCommConfig failed with error %d for %s.\n",GetLastError(),pCom);
                MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
            }

            return FALSE;
        }
        Dcb.BaudRate		 = CommConfig.dcb.BaudRate;
    }
    else
        Dcb.BaudRate         = ComPortConfig.dwBaudRate;

    Dcb.fAbortOnError    = TRUE;   
    Dcb.fParity          = FALSE;
    Dcb.fBinary          = TRUE;
    Dcb.fNull            = FALSE;

    if (SetCommState (hUart,&Dcb) == NULL)
    {
        if (ClearCommError (hUart,&ErrorFlags,&ComStat) == NULL)
        {
            sprintf (Message,"  Error: SetCommState failed with error %d for %s.\n",GetLastError(),pCom);
            MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        }
        return FALSE;
    }

    if (SetCommMask (hUart,EV_RXCHAR) == NULL)
    {
        if (ClearCommError (hUart,&ErrorFlags,&ComStat) == NULL)
        {
            sprintf (Message,"  Error: SetCommMask failed with error %d for %s.\n",GetLastError(),pCom);
            MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        }
        return FALSE;
    }

    if (SetupComm (hUart,4352,4352) == NULL)//4096
    {
        if (ClearCommError (hUart,&ErrorFlags,&ComStat) == NULL)
        {
            sprintf (Message,"  Error: SetupComm failed with error %d for %s.\n",GetLastError(),pCom);
            MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        }
        return FALSE;
    }

    if (SetCommTimeouts (hUart,&CommTimeOuts) == NULL)
    {
        if (ClearCommError (hUart,&ErrorFlags,&ComStat) == NULL)
        {
            sprintf (Message,"  Error: SetCommTimeouts failed with error %d for %s.\n",GetLastError(),pCom);
            MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        }
        return FALSE;
    }
    
    return TRUE;
}

BOOL WriteUart (WTPCOMMAND* pWtpCmd,DWORD dwSize)
{
    char Message[256] = {0};
    DWORD ErrorFlags = 0;

    if (hUart == INVALID_HANDLE_VALUE) 
    {
        sprintf (Message,"  Error: OpenUartPort must be called prior to calling WriteUart!");
        #ifdef _CONSOLE
            printf ("%s\n",Message);
        #else
            MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        #endif
        return FALSE;
    }

    if (WriteFile (hUart,pWtpCmd,dwSize,&BytesWritten,NULL) == NULL)
    {
        if (UARTDEBUG) printf ("%s\n",Message);
        if (ClearCommError (hUart,&ErrorFlags,&ComStat) == NULL)
        {
            sprintf (Message,"  Error: WriteFile failed with error %d.\n",GetLastError());
        }
        return FALSE;
    }

    return TRUE;
}

BOOL ReadUart (WTPSTATUS *WtpStatus,DWORD dwSize,PDWORD pBytesRead)
{
    char Message[256] = {0};
    DWORD ErrorFlags = 0;
    DWORD Mask = 0;

    if (hUart == INVALID_HANDLE_VALUE) 
    {
        sprintf (Message,"  Error: OpenUartPort must be called prior to calling ReadUart!");
        #ifdef _CONSOLE
            printf ("%s\n",Message);
        #else
            MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        #endif
        return FALSE;
    }

    if (EnableWaitEvent) WaitCommEvent (hUart,&Mask,NULL);
    if (ReadFile (hUart,WtpStatus,dwSize,&BytesRead,NULL) == NULL)
    {
        sprintf (Message,"  Error: ReadFile failed with error %d.\n",GetLastError());
        if (UARTDEBUG) printf ("%s\n",Message);
        if (ClearCommError (hUart,&ErrorFlags,&ComStat) == NULL)
        {
            sprintf (Message,"  Error: ClearCommError failed with error %d.\n",GetLastError());
            if (ISMSGPORT1) 
                MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        }
        return FALSE;
    }
    if (UARTDEBUG) printf ("\nBytes read: 0x%08x Bytes expected: 0x%08x\n",BytesRead,dwSize);

    *pBytesRead = BytesRead;
    if (BytesRead != dwSize) return FALSE;
    
    return TRUE;
}

BOOL ReadUartCharBuffer (char *WtpMessage,DWORD dwSize,PDWORD pBytesRead)
{
    char Message[256] = {0};
    DWORD ErrorFlags = 0;

    if (hUart == INVALID_HANDLE_VALUE) 
    {
        sprintf (Message,"  Error: OpenUartPort must be called prior to calling ReadUart!");
        #ifdef _CONSOLE
            printf ("%s\n",Message);
        #else
            MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        #endif
        return FALSE;
    }

    if (ReadFile (hUart,WtpMessage,dwSize,&BytesRead,NULL) == NULL)
    {
        sprintf (Message,"  Error: ReadFile failed with error %d.\n",GetLastError());
        if (UARTDEBUG) printf ("%s\n",Message);
        if (ClearCommError (hUart,&ErrorFlags,&ComStat) == NULL)
        {
            sprintf (Message,"  Error: ClearCommError failed with error %d.\n",GetLastError());
            if (ISMSGPORT1) 
                MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        }
        return FALSE;
    }
    if (ISMSGPORT1) printf ("\nBytes read: 0x%08x Bytes expected: 0x%08x\n",BytesRead,dwSize);

    *pBytesRead = BytesRead;
    if (BytesRead != dwSize) return FALSE;
    
    return TRUE;
}

BOOL UartPortPriority (int iPriority,DWORD dwPriorityClass)
{
    char Message[256] = {0};

    if (SetThreadPriority (GetCurrentThread (),iPriority) == NULL)
    {
        sprintf (Message,"  Error: SetThreadPriority failed!\n");
        MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        return FALSE;
    }

    if (SetPriorityClass (GetCurrentProcess (),dwPriorityClass) == NULL)
    {
        sprintf (Message,"  Error: SetPriorityClass failed!\n");
        MessageBox (NULL,Message,"SERIAL COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        return FALSE;
    }

    return TRUE;
}

void EnableUartVerboseMode (BOOL OnOff)
{
    UARTDEBUG = OnOff;
}

void EnableWaitCommEvent (BOOL OnOff)
{
    EnableWaitEvent = OnOff;
}

void CloseUartPort ()
{
    if (hUart != INVALID_HANDLE_VALUE) CloseHandle (hUart);
}
