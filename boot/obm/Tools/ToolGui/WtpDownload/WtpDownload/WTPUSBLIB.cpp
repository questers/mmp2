/**************************************************************************
**
** (C) Copyright 2006 - 2011 Marvell International Ltd.
**
** All Rights Reserved.
**
**
** THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE   
** IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR  
** PURPOSE ARE EXPRESSLY DISCLAIMED.   
**
**  FILENAME:	WTPUSBLIB.c
**
**  PURPOSE: 	Windows Dll routines 
**                  
******************************************************************************/

#include <stdafx.h>
#include <WtpLog.h>
#include <WTPUSBLIB.h>
#include <WTPIOCTL.h>

#pragma warning ( disable : 4244 4715 4047 4024 4996) // Disable warning messages

extern BOOL ISMSGPORT = FALSE; // Use this to enable port handshaking errors for debugging.

#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }

HANDLE hUsb = INVALID_HANDLE_VALUE;
char DeviceName[_MAX_PATH] ={0};

#if _DEBUG 
BOOL XDB_BREAKPOINT_RETRY = TRUE;
#else
BOOL XDB_BREAKPOINT_RETRY = FALSE;
#endif

BOOL USBDEBUG = FALSE;
BOOL POPUP_WINDOW = FALSE;

BOOL OpenUsbPort (long int DelayTime, char* pInterfaceName)
{
    char Message[256] = {0};
    unsigned long int Seconds;
    BOOL DeviceFound = FALSE,MessageEnable = TRUE;
    DWORD dwAccess = GENERIC_READ | GENERIC_WRITE;
    DWORD dwShareMode = FILE_SHARE_READ | FILE_SHARE_WRITE;
    DWORD dwFlags = FILE_ATTRIBUTE_NORMAL; // | FILE_FLAG_OVERLAPPED;  //See DeviceIoControl!

    if (DelayTime > 0) MessageEnable = FALSE;
    Seconds = GetTickCount () / 1000L;

    if( pInterfaceName != NULL )
    {
        DeviceFound = TRUE;
        strcpy( DeviceName, pInterfaceName );
    }
    else
    {
        do
        {
            if ((DeviceFound = GetUsbDeviceName (DeviceName,MessageEnable))) break;
//			Sleep (5);
        } while ((GetTickCount () / 1000L) < (Seconds + DelayTime));
    }
    

    if (DeviceFound == FALSE) return FALSE;
    if(POPUP_WINDOW)
    {
        MessageBox( NULL, DeviceName, "Selected Interface:", MB_OK);
    }
    hUsb = CreateFile (DeviceName,dwAccess,dwShareMode,NULL,OPEN_EXISTING,dwFlags,NULL);

    if (hUsb == INVALID_HANDLE_VALUE) 
    {
        ERROR("  Error: CreateFile failed with error %d",GetLastError());
        return FALSE;
    }

    return TRUE;
}

BOOL GetUsbDeviceName (char *DeviceName,BOOL IsPrint)
{
    char Message[256] = {0};
    HDEVINFO hDevInfo;
    SP_DEVICE_INTERFACE_DATA SpDeviceInterfaceData;
    PSP_DEVICE_INTERFACE_DETAIL_DATA pSpDeviceInterfaceDetailData = NULL;
    DWORD dwSize = 0, dwFlags = DIGCF_PRESENT | DIGCF_INTERFACEDEVICE;
    BOOL IsOk;

    hDevInfo = SetupDiGetClassDevs ((LPGUID)&GUID_BOOT,NULL,NULL,dwFlags);

    if (hDevInfo == INVALID_HANDLE_VALUE) 
    {
        sprintf (Message,"  Error: SetupDiGetClassDevs failed with error %d.",GetLastError());
        if (IsPrint) MessageBox (NULL,Message,"USB COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        return FALSE;
    }

    SpDeviceInterfaceData.cbSize = sizeof (SpDeviceInterfaceData);

    if (SetupDiEnumDeviceInterfaces (hDevInfo,NULL,(LPGUID)&GUID_BOOT,0,&SpDeviceInterfaceData) == NULL)
    {
        if (hDevInfo) SetupDiDestroyDeviceInfoList (hDevInfo);
        sprintf (Message,"  Error: SetupDiEnumDeviceInterfaces failed with error %d.",GetLastError());
        if (IsPrint) MessageBox (NULL,Message,"USB COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        return FALSE;
    }

    // Get PSP_DEVICE_INTERFACE_DETAIL_DATA struct size to allocate memory for it.
    SetupDiGetDeviceInterfaceDetail (hDevInfo,&SpDeviceInterfaceData,NULL,0,&dwSize,NULL);
    pSpDeviceInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc (dwSize);

    sprintf (Message,"%d",dwSize);
    OutputDebugString (Message);

    if (pSpDeviceInterfaceDetailData == NULL)
    {
        if (hDevInfo) SetupDiDestroyDeviceInfoList (hDevInfo);
        sprintf (Message,"  Error: Failed to allocate memory for PSP_DEVICE_INTERFACE_DETAIL_DATA.");
        if (IsPrint) MessageBox (NULL,Message,"USB COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        return FALSE;
    }

    pSpDeviceInterfaceDetailData->cbSize = sizeof (SP_DEVICE_INTERFACE_DETAIL_DATA);

    IsOk = SetupDiGetDeviceInterfaceDetail (hDevInfo,&SpDeviceInterfaceData,
                                            pSpDeviceInterfaceDetailData,
                                            dwSize,NULL,NULL);

    if (!IsOk)
    {
        if (pSpDeviceInterfaceDetailData) free (pSpDeviceInterfaceDetailData);
        if (hDevInfo) SetupDiDestroyDeviceInfoList (hDevInfo);
        sprintf (Message,"  Error: SetupDiGetDeviceInterfaceDetail failed with error %d.",GetLastError());
        if (IsPrint) MessageBox (NULL,Message,"USB COMMUNICATIONS ERROR",MB_ICONSTOP|MB_OK);
        return FALSE;
    }

    strcpy (DeviceName,pSpDeviceInterfaceDetailData->DevicePath);

    if (USBDEBUG)
    {
        DBG("%s",pSpDeviceInterfaceDetailData->DevicePath);
    }

    if (pSpDeviceInterfaceDetailData) free (pSpDeviceInterfaceDetailData);
    if (hDevInfo) SetupDiDestroyDeviceInfoList (hDevInfo);

    return TRUE;
}

BOOL WriteUsb (WTPCOMMAND* pWtpCmd,DWORD dwCmdSize,WTPSTATUS *WtpStatus,DWORD dwStatusSize)
{
    DWORD IOCTL_CMD, dwActualBytes = 0;
    BOOL IsOk;
    int iMsgCnt = 0;

    if (hUsb == INVALID_HANDLE_VALUE) 
    {
        ERROR("  Error: OpenUsbPort must be called prior to calling WriteUsb!\n");
        return FALSE;
    }
    if ((IOCTL_CMD = GetIoControlCode (pWtpCmd->CMD)) == UNKNOWNCOMMAND)
    {
        ERROR("  Error: An UNKNOWN command type was received! CMD = 0x%08x\n",pWtpCmd->CMD);
        return FALSE;
    }

Retry:
    IsOk = DeviceIoControl (hUsb,IOCTL_CMD,pWtpCmd,dwCmdSize,WtpStatus,
                            dwStatusSize,&dwActualBytes,NULL);
    if (!IsOk)
    {
        DWORD dwErrorCode = GetLastError();
		/*
		********THIS IS A HACK*********
		The BootROM receives the DISCONNECT command but
		does not send back a response as expected by DeviceIoControl().
		Instead BootROM closes the USB connection due to which DeviceIoControl() returns 0x1f error
        The bootloader has a race condition that causes it to sometimes disconnect
        before the DISCONNECT cmd is written. If writing a DISCONNECT gets a 0x48f
        "target is disconnected" error code, just consider the DISCONNECT successful
		*/
		if(pWtpCmd->CMD == DISCONNECT && (dwErrorCode == 0x1f || dwErrorCode == 0x48f) )
		{
            WtpStatus->CMD = DISCONNECT;
            WtpStatus->Status = ACK;
			return true;
		}

//		printf("\nError: WriteUSB returned with errorcode:%d\n",dwErrorCode);
        if (XDB_BREAKPOINT_RETRY)
        {
            // error 995 (operation aborted) occurs when the XDB is at a breakpoint
            // keep retrying until another error occurs
            if ( dwErrorCode == 995 ) 
            {
                iMsgCnt++;
                // on the 1st error only
                if ( iMsgCnt == 1 )
                {
                    ERROR("\n  Error: WriteUsb failed with error %d (Retry cnt=%d)\n", dwErrorCode ,iMsgCnt);
                }

                // on the 10th and every 60th thereafter
                if ( iMsgCnt == 10 || (iMsgCnt % 60)==0 )
                {
                
                    ERROR("  Error: Waiting for Target to continue...\n");
                }

                Sleep(1000);
                goto Retry;
            }
        }

        iMsgCnt = 0;

        if (USBDEBUG)
        {
            DBG("\n  Error: WriteUsb failed with error %d\n", dwErrorCode);        
        }

        // report the error in the WtpStatus->Status
        WtpStatus->Status = NACK;
    }

    if (USBDEBUG)
    {
        DBG("IOCTL code: 0x%08x\n",IOCTL_CMD);        
    }

    return IsOk;
}

DWORD GetIoControlCode (BYTE CMD)
{
    DWORD Code;

    switch (CMD)
    {
        case PREAMBLE:
            Code = IOCTL_BOOT_PREAMBLE;
            break;
        case PUBLICKEY:
            Code = IOCTL_BOOT_PUBLICKEYXMIT;
            break;
        case PASSWORD:
            Code = IOCTL_BOOT_SEEDPASSWORDRECEIVE;
            break;
        case SIGNEDPASSWORD:
            Code = IOCTL_BOOT_SIGNEDPASSWORDXMIT;
            break;
        case GETVERSION:
            Code = IOCTL_BOOT_GETVERSION;
            break;
        case SELECTIMAGE:
            Code = IOCTL_BOOT_SELECTIMAGE;
            break;
        case VERIFYIMAGE:
            Code = IOCTL_BOOT_VERIFYIMAGE;
            break;
        case DATAHEADER:
            Code = IOCTL_BOOT_DATAHEADER;
            break;
        case DATA:
            Code = IOCTL_BOOT_IMAGEDATAXMIT;
            break;
        case OTPVIEW:
            Code = IOCTL_BOOT_OTPVIEW;
            break;
        case DEBUGBOOT:
            Code = IOCTL_BOOT_DEBUGBOOT;
            break;
        case DONE:
            Code = IOCTL_BOOT_DONE;
            break;
        case DISCONNECT:
            Code = IOCTL_BOOT_DISCONNECT;
            break;
        case MESSAGE:
            Code = IOCTL_BOOT_MESSAGE;
            break;
        case PROTOCOLVERSION:
            Code = IOCTL_BOOT_PROTOCOLVERSION;
            break;
        case GETPARAMETERS:
            Code = IOCTL_BOOT_GETPARAMETERS;
            break;
        case UPLOADDATAHEADER:
            Code = IOCTL_BOOT_UPLOADDATAHEADER;
            break;
        case UPLOADDATA:
            Code = IOCTL_BOOT_UPLOADDATA;
            break;
        default:
            Code = UNKNOWNCOMMAND;
            break;
    }

    return Code;
}

void CloseUsbPort ()
{
    if (hUsb != INVALID_HANDLE_VALUE) CloseHandle (hUsb);
}


BOOL WriteUsbNoResp (PBYTE pData,DWORD dwCmdSize) 
{
    char Message[256] = {0};
    DWORD IOCTL_CMD=0, dwActualBytes = 0;
    BOOL IsOk;
    int iMsgCnt = 0;

    if (hUsb == INVALID_HANDLE_VALUE) 
    {
        ERROR("  Error: OpenUsbPort must be called prior to calling WriteUsb!\n");
        return FALSE;
    }

Retry:
    IsOk = DeviceIoControl (hUsb,IOCTL_BOOT_SEND,pData,dwCmdSize,NULL,
                            0,&dwActualBytes,NULL);
    if (!IsOk)
    {
        DWORD dwErrorCode = GetLastError();
//		printf("\nError: WriteUSB returned with errorcode:%d\n",dwErrorCode);
        if (XDB_BREAKPOINT_RETRY)
        {
            // error 995 (operation aborted) occurs when the XDB is at a breakpoint
            // keep retrying until another error occurs
            if ( dwErrorCode == 995 ) 
            {
                iMsgCnt++;
                // on the 1st error only
                if ( iMsgCnt == 1 )
                {                
                    ERROR("\n  Error: WriteUsb failed with error %d (Retry cnt=%d)\n",dwErrorCode,iMsgCnt);
                }

                // on the 10th and every 60th thereafter
                if ( iMsgCnt == 10 || (iMsgCnt % 60)==0 )
                {
                    ERROR("  Error: Waiting for Target to continue...\n",dwErrorCode,iMsgCnt);
                }

                Sleep(1000);
                goto Retry;
            }
        }

        iMsgCnt = 0;

        if (USBDEBUG)
        {
            ERROR("\n  Error: WriteUsb failed with error %d\n",dwErrorCode);
        }
    }

    if (USBDEBUG)
    {
        ERROR("IOCTL code: 0x%08x\n",IOCTL_CMD);
    }

    return IsOk;
}


BOOL ReadUsb (WTPSTATUS *WtpStatus,DWORD dwStatusSize) 
{
    char Message[256] = {0};
    DWORD IOCTL_CMD=0, dwActualBytes = 0;
    BOOL IsOk;
    int iMsgCnt = 0;

    if (hUsb == INVALID_HANDLE_VALUE) 
    {
        ERROR("  Error: OpenUsbPort must be called prior to calling WriteUsb!\n");
        return FALSE;
    }

Retry:
    IsOk = DeviceIoControl (hUsb,IOCTL_BOOT_RECEIVE,NULL,0,WtpStatus,
                            dwStatusSize,&dwActualBytes,NULL);
    if (!IsOk)
    {
        DWORD dwErrorCode = GetLastError();
        if (XDB_BREAKPOINT_RETRY)
        {
            // error 995 (operation aborted) occurs when the XDB is at a breakpoint
            // keep retrying until another error occurs
            if ( dwErrorCode == 995 ) 
            {
                iMsgCnt++;
                // on the 1st error only
                if ( iMsgCnt == 1 )
                {
                    WARN("\n  Error: ReadUsb failed with error %d (Retry cnt=%d)\n",dwErrorCode,iMsgCnt);
                }

                // on the 10th error and every 60th thereafter
                if ( iMsgCnt == 10 || (iMsgCnt % 60)==0 )
                {                
                    INFO("  Error: Waiting for Target to continue...\n");
                }

                Sleep(1000);
                goto Retry;
            }
        }

        iMsgCnt = 0;

        if (USBDEBUG)
        {
            ERROR("\n  Error: ReadUsb failed with error %d\n",dwErrorCode);
        }

        // report the error in the WtpStatus->Status
        WtpStatus->Status = NACK;
    }

    if (USBDEBUG)
    {
        ERROR("IOCTL code: 0x%08x\n",IOCTL_CMD);
    }

    return TRUE;
}
