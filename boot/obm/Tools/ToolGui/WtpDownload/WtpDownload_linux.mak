###############################################################
#	Makefile for the Marvel NonTrusted Boot Builder 
# 
# 
############################################################## 
TRUSTED=0
CC = g++
TOPDIR :=$(shell pwd)
CCFLAGS += -D LINUX=1 -D WINDOWS=0
IPP_PATH = /opt/intel/ipp/5.2/ia32
IPP_INCLUDE_DIR = $(IPP_PATH)/include
IPP_LIBRARY_DIR = $(IPP_PATH)/sharedlib
LD_LIBRARY_PATH :=$(IPP_LIBRARY_DIR):$(LD_LIBRARY_PATH)
export LD_LIBRARY_PATH

INCLUDE = -I $(TOPDIR)/WTPTP/Common/Include -I $(TOPDIR)/Common
SUBDIRS = $(TOPDIR)/WTPTP/WtptpConsole $(TOPDIR)/WTPTP/Common/WTPSECURITYLIB $(TOPDIR)/WTPTP/Common/WTPUARTLIB \
          $(TOPDIR)/WTPTP/Common/WTPCOMMCLASS $(TOPDIR)/WTPTP/Common/WTPUSBLIB
MAKE = make

ifeq "$(TRUSTED)" "1"
    INCLUDE = -I$(INCLUDE) -I$(TOPDIR)/Common/ippcp 
	OUTPUT = wtptp.exe	
	LIBS = $(IPP_LIBRARY_DIR)/libippcp.so \
               $(IPP_LIBRARY_DIR)/libippcore.so \
               $(IPP_LIBRARY_DIR)/libguide.so
        CCFLAGS += -D TRUSTED=1 -I $(IPP_INCLUDE_DIR)
	WTPTP_OBJS = WtpDownload.o \
	             WTPSECURITYLIB.o \
	             TimParser.o \
	             WTPUARTLIB.o \
	             WTPUSBLIB.o \
	             WTPCOMMCLASS.o
else
	OUTPUT = ntwtptp.exe
        CCFLAGS += -D TRUSTED=0
	LIBS =
	WTPTP_OBJS = WtpDownload.o \
	             TimParser.o \
	             WTPUARTLIB.o \
	             WTPUSBLIB.o \
	             WTPCOMMCLASS.o
endif

COMMONOBJS = CommandLineParser.o ImageDescription.o ReservedPackageData.o TimDescriptorParser.o TimLib.o

OBJS = $(COMMONOBJS) $(WTPTP_OBJS)

all: $(OBJS)
	$(CC) -o $(OUTPUT) $(OBJS) $(LIBS)
	@-rm *.o

$(COMMONOBJS): %.o:$(TOPDIR)/Common/%.cpp
	$(CC) -w $(CCFLAGS) -c "$(TOPDIR)/Common/$*.cpp" -o "$(TOPDIR)/$*.o"        

WtpDownload.o:$(TOPDIR)/WTPTP/WtptpConsole/WtpDownload.cpp
	$(CC) -w $(CCFLAGS) -c $(TOPDIR)/WTPTP/WtptpConsole/WtpDownload.cpp -o WtpDownload.o

WTPSECURITYLIB.o:$(TOPDIR)/WTPTP/Common/WTPSECURITYLIB/WTPSECURITYLIB.c
	$(CC) -w $(CCFLAGS) -c $(TOPDIR)/WTPTP/Common/WTPSECURITYLIB/WTPSECURITYLIB.c -o WTPSECURITYLIB.o

TimParser.o:$(TOPDIR)/WTPTP/Common/WTPSECURITYLIB/TimParser.c
	$(CC) -w $(CCFLAGS) -c "$(TOPDIR)/WTPTP/Common/WTPSECURITYLIB/TimParser.c" -o TimParser.o

WTPUARTLIB.o:$(TOPDIR)/WTPTP/Common/WTPUARTLIB/WTPUARTLIB_linux.c
	$(CC) -w $(CCFLAGS) -c "$(TOPDIR)/WTPTP/Common/WTPUARTLIB/WTPUARTLIB_linux.c" -o WTPUARTLIB.o

WTPUSBLIB.o:$(TOPDIR)/WTPTP/Common/WTPUSBLIB/WTPUSBLIB_linux.c
	$(CC) -w $(CCFLAGS) -c "$(TOPDIR)/WTPTP/Common/WTPUSBLIB/WTPUSBLIB_linux.c" -o WTPUSBLIB.o

WTPCOMMCLASS.o:$(TOPDIR)/WTPTP/Common/WTPCOMMCLASS/WTPCOMMCLASS.cpp
	$(CC) -w $(CCFLAGS) -c "$(TOPDIR)/WTPTP/Common/WTPCOMMCLASS/WTPCOMMCLASS.cpp" -o WTPCOMMCLASS.o

.PHONY: clean
clean:
	find . -name "*.o" -exec rm -fr {} \;
	-rm *.o *.exe




