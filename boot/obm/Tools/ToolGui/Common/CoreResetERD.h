/******************************************************************************
 *
 *  (C)Copyright 2005 - 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#include "ErdBase.h"

class CCoreReset : public CErdBase
{
public:
	CCoreReset();
	virtual ~CCoreReset(void);

	// copy constructor
	CCoreReset( const CCoreReset& rhs );
	// assignment operator
	virtual CCoreReset& operator=( const CCoreReset& rhs );

	static const string Begin;	// "Core Reset"
	static const string End;	// "End Core Reset"

	virtual bool ToBinary( ofstream& ofs );
	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize();
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

#if TOOLS_GUI == 1
//	virtual bool LoadState( ifstream& ifs );
//	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	enum eCoreReset { COREID_FIELD, ADDRESS_MAPPING_FIELD, CORE_RESET_MAX };
};

//typedef vector<CCoreReset*>           t_CCoreResetVec;
//typedef vector<CCoreReset*>::iterator t_CCoreResetVecIter;

