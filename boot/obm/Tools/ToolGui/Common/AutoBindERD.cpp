/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma message("Using AutoBind Reserved Data")
#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "AutoBindERD.h"

const string CAutoBind::Begin("AutoBind");
const string CAutoBind::End("End AutoBind");

CAutoBind::CAutoBind()
	: CErdBase( AUTOBIND_ERD, AUTOBIND_MAX )
{
	*m_FieldNames[eAUTOBIND] = "AUTOBIND";
}

CAutoBind::~CAutoBind()
{
}

// copy constructor
CAutoBind::CAutoBind( const CAutoBind& rhs )
: CErdBase( rhs )
{
	// copy constructor
}

// assignment operator
CAutoBind& CAutoBind::operator=( const CAutoBind& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		CErdBase::operator=( rhs );
	}
	return *this;
}

bool CAutoBind::ToBinary( ofstream& ofs )
{
	// validate size
	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
		return false;

	ofs << AUTOBIND;
	ofs << PackageSize();
	ofs << Translate(*m_FieldValues[eAUTOBIND]);

	return ofs.good();
}

int CAutoBind::AddPkgStrings( CReservedPackageData* pRPD )
{
	pRPD->PackageIdTag( HexFormattedAscii(AUTOBIND) );
	pRPD->AddData( new string( *m_FieldValues[ eAUTOBIND ] ), new string( "AUTOBIND" ));

	return PackageSize();
}

CAutoBind* CAutoBind::Migrate( CReservedPackageData* pRPD )
{
	CAutoBind* pBind = new CAutoBind;

	size_t size = pRPD->PackageDataList().size();
	// make sure the data is the correct size for a USB package
	if ( size == 1 )
	{
		t_stringListIter Iter = pRPD->PackageDataList().begin();
		*(pBind->m_FieldValues[ eAUTOBIND ]) = *(*Iter++);
	}

	return pBind;
}

#if TOOLS_GUI == 1
bool CAutoBind::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;

	CErdBase::ToText( TimDescriptor, ss );

	if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;
	return true;
}
#endif

