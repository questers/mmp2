/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "ErdBase.h"

#include <list>
using namespace std;

class CGpio : public CErdBase
{
public:
	CGpio();
	virtual ~CGpio();

	// copy constructor
	CGpio( const CGpio& rhs );
	// assignment operator
	virtual CGpio& operator=( const CGpio& rhs );

	static const string Begin;	// "Gpio"
	static const string End;	// "End Gpio"

	virtual bool ToBinary( ofstream& ofs );

	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize() { return (unsigned int)(m_FieldValues.size()*4); }
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

	void Assign( string& sAddress, string& sValue );

#if TOOLS_GUI == 1
//	virtual bool LoadState( ifstream& ifs );
//	virtual bool SaveState( ofstream& ofs );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	enum Gpio { ADDRESS, VALUE, GPIO_MAX };

};

typedef list<CGpio*>           t_GpioList;
typedef list<CGpio*>::iterator t_GpioListIter;


class CGpioSet : public CErdBase
{
public:
	CGpioSet();
	virtual ~CGpioSet();

	// copy constructor
	CGpioSet( const CGpioSet& rhs );
	// assignment operator
	virtual CGpioSet& operator=( const CGpioSet& rhs );

	static const string	Begin;	// "GpioSet"
	static const string	End;	// "End GpioSet"
	static CGpioSet* Migrate( CReservedPackageData* pRPD );

	virtual void Reset();

	virtual bool Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine );

	virtual bool ToBinary( ofstream& ofs );

	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize() { return (12 + (unsigned int)Gpios.size()*8); }
	virtual int AddPkgStrings( CReservedPackageData* pRPD );
	
#if TOOLS_GUI == 1
	virtual bool LoadState( ifstream& ifs );
	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	t_GpioList& GpiosList() { return Gpios; }
	enum GpioSet { NUM_GPIOS, GPIOSET_MAX };

private:
	t_GpioList		Gpios;
};

typedef list<CGpioSet*>           t_GpioSetList;
typedef list<CGpioSet*>::iterator t_GpioSetListIter;

