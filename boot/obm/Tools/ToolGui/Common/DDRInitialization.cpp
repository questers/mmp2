/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#if TOOLS_GUI == 1
#include "stdafx.h"
#include "MarvellBootUtility.h"
#endif

#include "DDRInitialization.h"
#include "TimDescriptorParser.h"

#if LINUX
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

const string CDDRInitialization::Begin("DDR Initialization");
const string CDDRInitialization::End("End DDR Initialization");

CDDRInitialization::CDDRInitialization()
:	CErdBase( DDR_INITIALIZATION_ERD, DDR_INIT_MAX )
{
    *m_FieldNames[DDR_PID] = "DDR_PID";
    m_sDdrPID = "";
}

CDDRInitialization::~CDDRInitialization(void)
{
#if TOOLS_GUI == 1
        CTimDescriptorLine* pLine = 0;

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, m_FieldNames[DDR_PID] )) != 0 )
            pLine->RemoveRef( m_FieldNames[DDR_PID] );
#endif
}

// copy constructor
CDDRInitialization::CDDRInitialization( const CDDRInitialization& rhs )
: CErdBase( rhs )
{
    m_sDdrPID = rhs.m_sDdrPID;

#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, rhs.m_FieldNames[DDR_PID] )) != 0 )
        pLine->AddRef( m_FieldNames[DDR_PID] );
#endif

    m_DdrOperations = rhs.m_DdrOperations;
    m_DdrInstructions = rhs.m_DdrInstructions;
}

// assignment operator
CDDRInitialization& CDDRInitialization::operator=( const CDDRInitialization& rhs )
{
    // assignment operator
    if ( &rhs != this )
    {
        CErdBase::operator=( rhs );

#if TOOLS_GUI == 1
        CTimDescriptorLine* pLine = 0;

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, m_FieldNames[DDR_PID] )) != 0 )
            pLine->RemoveRef( m_FieldNames[DDR_PID] );
#endif

        m_sDdrPID = rhs.m_sDdrPID;

#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, rhs.m_FieldNames[DDR_PID] )) != 0 )
            pLine->AddRef( m_FieldNames[DDR_PID] );
#endif

        m_DdrOperations = rhs.m_DdrOperations;
        m_DdrInstructions = rhs.m_DdrInstructions;
    }
    return *this;
}

unsigned int CDDRInitialization::PackageSize() 
{
    // 8(WRAH) + 8(NumOps & NumInst) + instructions actual size + ops actual size
    return 8 + 8 + m_DdrInstructions.PackageSize() + m_DdrOperations.PackageSize();
}

bool CDDRInitialization::Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine )
{
    CErdBase* pErd = 0;

    while ( pLine = TimDescriptor.GetNextLineField( pLine ) )
    {
        bool bFound = false;

        for ( unsigned int idx = 0; idx < m_FieldNames.size(); idx++ )
        {
            if ( TrimWS( pLine->m_FieldName ) == *m_FieldNames[ idx ] )
            {
#if TOOLS_GUI == 1
                pLine->AddRef( m_FieldNames[ idx ] ); 
#endif
                if ( idx == DDR_PID )
                    m_sDdrPID = TrimWS( pLine->m_FieldValue );
                else
                    *m_FieldValues[ idx ] = TrimWS( pLine->m_FieldValue );

                bFound = true;
                break;
            }
        }

        if ( bFound )
            continue;

        if ( pLine->m_FieldName.find( CDDROperations::Begin ) != string::npos )
        {			
            CErdBase* pErd = &m_DdrOperations;
            ParseERDPackage( TimDescriptor, pLine, pErd, CDDROperations::Begin, CDDROperations::End );
            continue;
        }
        else if ( pLine->m_FieldName.find( CInstructions::Begin ) != string::npos )
        {					
            CErdBase* pErd = &m_DdrInstructions;
            ParseERDPackage( TimDescriptor, pLine, pErd, CInstructions::Begin, CInstructions::End );
            continue;
        }

        if ( !bFound )
            break;
    }

    // field not found
    return true;
}

bool CDDRInitialization::ParseERDPackage( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine, CErdBase*& pErd, const string& sBegin, const string& sEnd )
{
#if TOOLS_GUI == 1
    // mark start of package
    pLine->AddRef( pErd );
#endif

    if ( !pErd->Parse( TimDescriptor, pLine ) || (pLine->m_FieldName.find( sEnd ) == string::npos) )
    {
//        delete pErd;
//        pErd = 0;
        printf("\nError: Parsing of %s failed near line: '%s'\n", sBegin.c_str(), pLine->m_FieldName.c_str() );
        return false;
    }

#if TOOLS_GUI == 1
    // mark end of package
    pLine->AddRef( pErd );
#endif

    return true;
}

bool CDDRInitialization::ToBinary( ofstream& ofs )
{
    bool bRet = true;
#if 0
    // validate size
    if ( m_FieldValues.size() !=  m_iMaxFieldNum )
        return false;

    ofs << TBR_XFER;
    ofs << PackageSize();
    ofs << Translate(*m_FieldNames[XFER_TABLE_LOC]);
    ofs << Translate(*m_FieldValues[NUM_DATA_PAIRS]);

    t_XferListIter iter = Xfers.begin();
    while ( bRet && iter != Xfers.end() )
        bRet = (*iter++)->ToBinary( ofs );
#endif

    return ( ofs.good() && bRet );
}

int CDDRInitialization::AddPkgStrings( CReservedPackageData* pRPD )
{
    string sData;
    TextToHexFormattedAscii( sData, m_sDdrPID );
    pRPD->PackageIdTag( sData );
    
    string* pData = new string;
    *pData = HexFormattedAscii( m_DdrOperations.NumOps() );
    pRPD->AddData( pData, new string( "NumOps" ) );

    pData = new string;
    *pData = HexFormattedAscii( m_DdrInstructions.NumInst() );
    pRPD->AddData( pData, new string( "NumInst" ) );

    m_DdrOperations.AddPkgStrings( pRPD );
    m_DdrInstructions.AddPkgStrings( pRPD );

    return PackageSize();
}

#if TOOLS_GUI == 1
bool CDDRInitialization::SaveState( stringstream& ss )
{
    unsigned int temp = 0;

    CErdBase::SaveState( ss );

    SaveFieldState( ss, m_sDdrPID );

    m_DdrInstructions.SaveState( ss );
    m_DdrOperations.SaveState( ss );

    return true; // SUCCESS
}
#endif
 
#if TOOLS_GUI == 1
bool CDDRInitialization::LoadState( ifstream& ifs )
{
    bool bRet = true;

    if ( theApp.ProjectVersion >= 0x03021701 )
    {
        Reset();

        CErdBase::LoadState( ifs );

        LoadFieldState( ifs, m_sDdrPID );

        m_DdrInstructions.LoadState( ifs );
        m_DdrOperations.LoadState( ifs );
    }

    return bRet && ifs.good() && !ifs.fail(); // success
}
#endif

#if TOOLS_GUI == 1
bool CDDRInitialization::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
    bool bRet = true;

    if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
        ss << Begin << ":" << endl;

    if ( !TimDescriptor.AttachCommentedObject( ss, m_FieldNames[DDR_PID], m_FieldNames[DDR_PID]->c_str(), m_sDdrPID ) )
    {
        ss << *m_FieldNames[DDR_PID] << ": ";
        ss << m_sDdrPID;
//		ss << "     // *m_FieldComments[i];
        ss << endl;
    }

    m_DdrOperations.ToText( TimDescriptor, ss );
    m_DdrInstructions.ToText( TimDescriptor, ss );

    if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
        ss << End << ":" << endl;

    return bRet;
}
#endif
