/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "TimLib.h"

#include <string>
#include <list>
#include <iterator>
#include <fstream>
#include <iostream>
#include <strstream>
#include <sstream>
using namespace std;

typedef list<string*>           t_stringList;
typedef list<string*>::iterator t_stringListIter;

class CKey : public CTimLib
{
public:
	CKey(void);
	virtual ~CKey(void);

	// copy constructor
	CKey( const CKey& rhs );
	// assignment operator
	CKey& operator=( const CKey& rhs );

public:
	string& KeyId() { return m_sKeyId; }
	void KeyId( string& sKeyId ){ m_sKeyId = sKeyId; m_bChanged = true; }

	string& KeyTag() { return m_sKeyTag; }
	void KeyTag( string& sKeyTag );

	string& HashAlgorithmId() { return m_sHashAlgorithmId; }
	void HashAlgorithmId( string& sHashAlgorithmId ){ m_sHashAlgorithmId = sHashAlgorithmId; m_bChanged = true; }
    void HashAlgorithmId( HASHALGORITHMID_T Id );

	string& EncryptAlgorithmId() { return m_sEncryptAlgorithmId; }
	void EncryptAlgorithmId( string& sEncryptAlgorithmId ){ m_sEncryptAlgorithmId = sEncryptAlgorithmId; m_bChanged = true; }
    void EncryptAlgorithmId( ENCRYPTALGORITHMID_T Id );

	unsigned int ActiveSizeOfList( t_stringList& List );
	unsigned int& PublicKeySize(){ return m_uiPublicKeySize; }
	virtual void PublicKeySize( unsigned int uiPublicKeySize );

	unsigned int& KeySize(){ return m_uiKeySize; }
	virtual void KeySize( unsigned int uiKeySize );

	t_stringList& PublicKeyExponentList(){ return m_PublicKeyExponentList; }
	t_stringList& RsaSystemModulusList(){ return m_RsaSystemModulusList; }
	t_stringList& ECDSAPublicKeyCompXList(){ return m_ECDSAPublicKeyCompXList; }
	t_stringList& ECDSAPublicKeyCompYList(){ return m_ECDSAPublicKeyCompYList; }

	string& PublicKeyExponent();
	string& PublicKeyExponentPacked();
	string& RsaSystemModulus();
	string& RsaSystemModulusPacked();

	string& ECDSAPublicKeyCompX();
	string& ECDSAPublicKeyCompY();
	string& ECDSAPublicKeyCompXPacked();
	string& ECDSAPublicKeyCompYPacked();

	void DiscardAll();
	void ResetExponent();
	void ResetModulus();
	void ResetCompX();
	void ResetCompY();

#if TOOLS_GUI == 1
	bool SaveState( stringstream& ss );
	bool LoadState( ifstream& ifs );
#endif

	bool IsChanged();
	void Changed( bool bSet ){ m_bChanged = bSet; }

protected:
	bool m_bChanged;
	string			m_sEncryptAlgorithmId;

private:
	string			m_sKeyId;
	string			m_sKeyTag;
	string			m_sHashAlgorithmId;

	string  m_sPublicKeyExponent; // temp - do not store
	string  m_sRsaSystemModulus;  // temp - do not store
	string  m_sECDSAPublicKeyCompX;
	string  m_sECDSAPublicKeyCompY;

	unsigned int m_uiPublicKeySize;
	t_stringList m_PublicKeyExponentList;
	unsigned int m_uiKeySize;
	t_stringList m_RsaSystemModulusList;
	t_stringList m_ECDSAPublicKeyCompXList;
	t_stringList m_ECDSAPublicKeyCompYList;
};

typedef list<CKey*>           t_KeyList;
typedef list<CKey*>::iterator t_KeyListIter;
