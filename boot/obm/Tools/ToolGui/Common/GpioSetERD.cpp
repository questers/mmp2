/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma message("Using Gpio Reserved Data")
#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "GpioSetERD.h"
#include "TimDescriptorParser.h"

#if LINUX
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

const string CGpio::Begin("Gpio");
const string CGpio::End("End Gpio");

CGpio::CGpio()
	: CErdBase( GPIO_ERD, GPIO_MAX )
{
	*m_FieldNames[ADDRESS] = "ADDRESS";
	*m_FieldNames[VALUE] = "VALUE";
}

CGpio::~CGpio()
{
}

// copy constructor
CGpio::CGpio( const CGpio& rhs )
: CErdBase( rhs )
{
	// copy constructor
}

// assignment operator
CGpio& CGpio::operator=( const CGpio& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		CErdBase::operator=( rhs );
	}
	return *this;
}

bool CGpio::ToBinary( ofstream& ofs )
{
	// validate size
	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
		return false;

	ofs << Translate(*m_FieldValues[ADDRESS]);
	ofs << Translate(*m_FieldValues[VALUE]);

	return ofs.good();
}


int CGpio::AddPkgStrings( CReservedPackageData* pRPD )
{
	pRPD->AddData( new string( *m_FieldValues[ ADDRESS ] ), new string( "ADDRESS" ) );
	pRPD->AddData( new string( *m_FieldValues[ VALUE ] ), new string( "VALUE" ) );

	return PackageSize();
}

void CGpio::Assign( string& sAddress, string& sValue )
{
	*m_FieldValues[ ADDRESS ] = sAddress;
	*m_FieldValues[ VALUE ] = sValue;
}

#if TOOLS_GUI == 1
bool CGpio::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;
		
	CErdBase::ToText( TimDescriptor, ss );

	if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;
	return true;
}
#endif

const string CGpioSet::Begin("GpioSet");
const string CGpioSet::End("End GpioSet");

CGpioSet::CGpioSet()
	: CErdBase( GPIOSET_ERD, GPIOSET_MAX )
{
	*m_FieldNames[NUM_GPIOS] = "NUM_GPIOS";
}

CGpioSet::~CGpioSet()
{
	Reset();
}

// copy constructor
CGpioSet::CGpioSet( const CGpioSet& rhs )
: CErdBase( rhs )
{
	// copy constructor

	// need to do a deep copy of lists to avoid dangling references
	CGpioSet& nc_rhs = const_cast<CGpioSet&>(rhs);

	t_GpioListIter iter = nc_rhs.Gpios.begin();
	while ( iter != nc_rhs.Gpios.end() )
	{
		Gpios.push_back( new CGpio( *(*iter) ) );
		iter++;
	}
}

// assignment operator
CGpioSet& CGpioSet::operator=( const CGpioSet& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		CErdBase::operator=( rhs );

		Reset();

		// need to do a deep copy of lists to avoid dangling references
		CGpioSet& nc_rhs = const_cast<CGpioSet&>(rhs);

		t_GpioListIter iter = nc_rhs.Gpios.begin();
		while ( iter != nc_rhs.Gpios.end() )
		{
			Gpios.push_back( new CGpio( *(*iter) ) );
			iter++;
		}
	}
	return *this;
}

void CGpioSet::Reset()
{
	t_GpioListIter iter = Gpios.begin();
	while ( iter != Gpios.end() )
	{
		delete *iter;
		iter++;
	}
	Gpios.clear();
}

bool CGpioSet::Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine )
{
	if ( pLine->m_FieldName != CGpioSet::Begin )
	{
		printf( "\nError: Attempt to parse GpioSet failed!" );
		return false;
	}

	if ( CErdBase::Parse( TimDescriptor, pLine ) && pLine->m_FieldName != CGpioSet::End )
	{
		for ( unsigned int i = 0; i < Translate(*m_FieldValues[ NUM_GPIOS ]); i++ )
		{
			CGpio* pGpio = 0;
			if ( pLine->m_FieldName == CGpio::Begin )
			{
				pGpio = new CGpio;
#if TOOLS_GUI == 1
				CTimDescriptorLine* pGpioLine = pLine;
#endif
				if ( !pGpio->Parse( TimDescriptor, pLine ) )
				{
					printf("\nError: Parsing of Gpio failed.");
					delete pGpio;
					return false;
				}
#if TOOLS_GUI == 1
				pGpioLine->AddRef( pGpio );
#endif
			}

			if ( pGpio && pLine->m_FieldName == CGpio::End )
			{
				Gpios.push_back( pGpio );
				pGpio = 0;

				if ( pLine = TimDescriptor.GetNextLineField( pLine ) )
					// see if we have more Gpios in this GpioSet
					continue;
				else
					break;
			}
			else
			{
				// we should be at the end of the GpioSet now
				if ( pLine->m_FieldName == CGpioSet::End )
					break;
				else
				{
					printf("\nError: Parsing of Gpio failed.");
					delete pGpio;
					return false;
				}
			}
		}
	}

	if ( Translate(*m_FieldValues[ NUM_GPIOS ]) != Gpios.size() )
	{
		printf("\nError: Parsing of GpioSet, NUM_GPIOS not equal to actual number ");
		printf("of Gpios defined in the GpioSet\n");
		return false;
	}

	return true;
}

bool CGpioSet::ToBinary( ofstream& ofs )
{
	// validate size
	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
		return false;

	bool bRet = true;
	ofs << GPIOID;
	ofs << PackageSize();
	ofs << Translate(*m_FieldValues[NUM_GPIOS]);
	t_GpioListIter iter = Gpios.begin();
	while ( bRet && iter != Gpios.end() )
		bRet = (*iter++)->ToBinary( ofs );

	return ( ofs.good() && bRet );
}

int CGpioSet::AddPkgStrings( CReservedPackageData* pRPD )
{
	pRPD->PackageIdTag( HexFormattedAscii(GPIOID) );
	pRPD->AddData( new string( *m_FieldValues[ NUM_GPIOS ] ),  new string( "NUM_GPIOS" ) );

	t_GpioListIter iter = Gpios.begin();
	while ( iter != Gpios.end() )
		(*iter++)->AddPkgStrings( pRPD );

	return PackageSize();
}

CGpioSet* CGpioSet::Migrate( CReservedPackageData* pRPD )
{
	CGpioSet* pSet = new CGpioSet;

	size_t size = pRPD->PackageDataList().size();
	// make sure we have some data and that we have num gpios + address/value pairs
	if ( size > 0 && (((size-1) % 2)==0) )
	{
		t_stringListIter Iter = pRPD->PackageDataList().begin();
		*(pSet->m_FieldValues[ NUM_GPIOS ]) = *(*Iter++);
		for ( unsigned int i = 1; i < size; i++,i++ )
		{
			CGpio* pGpio = new CGpio;
			string sAddress = *(*Iter++);
			string sValue = *(*Iter++);
			pGpio->Assign( sAddress, sValue  );
		    pSet->GpiosList().push_back( pGpio );
		}
	}

	return pSet;
}


#if TOOLS_GUI == 1
bool CGpioSet::SaveState( stringstream& ss )
{
	unsigned int temp = 0;
	if ( CErdBase::SaveState( ss ) )
	{
		temp = (unsigned int)Gpios.size();
		SaveFieldState( ss, temp );

		t_GpioListIter iter = Gpios.begin();
		while ( iter != Gpios.end() )
			if ( !(*iter++)->SaveState( ss ) )
				return false;
	}
	else
		return false;

	return true; // SUCCESS
}
#endif

#if TOOLS_GUI == 1
bool CGpioSet::LoadState( ifstream& ifs )
{
	if ( theApp.ProjectVersion >= 0x03021200 )
	{
		Reset();

		if (  CErdBase::LoadState( ifs ) )
		{
			string sbuf;
			LoadFieldState( ifs, sbuf );
			unsigned int GpiosSize = Translate(sbuf);
			for ( unsigned int i = 0; i < GpiosSize; i++ )
			{
				LoadFieldState( ifs, sbuf );
				CErdBase::ERD_PKG_TYPE ErdType = CErdBase::ERD_PKG_TYPE(Translate(sbuf));
				if ( ErdType == GPIO_ERD )
				{
					CGpio* pGpio = new CGpio;
					if ( !pGpio->LoadState( ifs ) )
						return false;

					Gpios.push_back(pGpio);
				}
				else
					return false;
			}
		}
	}
	
	return ifs.good() && !ifs.fail(); // success
}
#endif

#if TOOLS_GUI == 1
bool CGpioSet::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	bool bRet = true;
	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;

	CErdBase::ToText( TimDescriptor, ss );

	t_GpioListIter iter = Gpios.begin();
	while ( bRet && iter != Gpios.end() )
	{
//		if ( !TimDescriptor.AttachCommentedObject( ss, *iter, (*iter)->Begin.c_str(), string("") ) )
//		{
			bRet = (*iter)->ToText( TimDescriptor, ss );
//		}
		iter++;
	}

	if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;

	return bRet;
}
#endif
