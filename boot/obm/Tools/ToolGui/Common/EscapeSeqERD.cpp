/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma message("Using EscapeSeq Reserved Data")
#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "EscapeSeqERD.h"

const string CEscapeSeq::Begin("Escape Sequence");
const string CEscapeSeq::End("End Escape Sequence");

CEscapeSeq::CEscapeSeq()
	: CErdBase( ESCAPESEQ_ERD, ESCAPE_SEQ_MAX )
{
	*m_FieldNames[ESCAPE_SEQ_TIMEOUT_MS] = "ESCAPE_SEQ_TIMEOUT_MS";
}

CEscapeSeq::~CEscapeSeq()
{
}

// copy constructor
CEscapeSeq::CEscapeSeq( const CEscapeSeq& rhs )
: CErdBase( rhs )
{
	// copy constructor
}

// assignment operator
CEscapeSeq& CEscapeSeq::operator=( const CEscapeSeq& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		CErdBase::operator=( rhs );
	}
	return *this;
}

bool CEscapeSeq::ToBinary( ofstream& ofs )
{
	// validate size
	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
		return false;

	ofs << ESCAPESEQID;
	ofs << PackageSize();
	ofs << Translate(*m_FieldValues[ESCAPE_SEQ_TIMEOUT_MS]);

	return ofs.good();
}


int CEscapeSeq::AddPkgStrings( CReservedPackageData* pRPD )
{
	pRPD->PackageIdTag( HexFormattedAscii(ESCAPESEQID) );
	pRPD->AddData( new string( *m_FieldValues[ ESCAPE_SEQ_TIMEOUT_MS ] ), new string( "ESCAPE_SEQ_TIMEOUT_MS" ));

	return PackageSize();
}


#if TOOLS_GUI == 1
bool CEscapeSeq::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;

	CErdBase::ToText( TimDescriptor, ss );

	if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;

	return true;
}
#endif
