/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#if TOOLS_GUI == 1
#include "stdafx.h"
#include "MarvellBootUtility.h"
#endif

#include "TzOperations.h"
#include "TimDescriptorParser.h"

#if LINUX
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif


CTzOperation::CTzOperation()
: CTimLib()
{
	m_OperationId = TZ_NOP;
	m_sOpIdText = "NOP";
	m_Value = 0;
}

CTzOperation::CTzOperation( TZ_OPERATION_SPEC_T OpId, const string & sOpIdText )
: m_OperationId( OpId ), m_sOpIdText( sOpIdText ), CTimLib()
{
	m_Value = 0;
}

CTzOperation::~CTzOperation(void)
{
#if TOOLS_GUI == 1
		CTimDescriptorLine* pLine = 0;

		// update the object line reference
		if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_sOpIdText )) != 0 )
			pLine->RemoveRef( &m_sOpIdText );
#endif
}

CTzOperation::CTzOperation( const CTzOperation& rhs )
: CTimLib( rhs )
{
	// copy constructor
	CTzOperation& nc_rhs = const_cast<CTzOperation&>(rhs);

	m_OperationId = rhs.m_OperationId;
	m_sOpIdText = rhs.m_sOpIdText;

#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;

	// update the object line reference
	if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sOpIdText )) != 0 )
		pLine->AddRef( &m_sOpIdText );
#endif

	m_Value = rhs.m_Value;
}

CTzOperation& CTzOperation::operator=( const CTzOperation& rhs ) 
{
	// assignment operator
	if ( &rhs != this )
	{
		CTimLib::operator=(rhs);

		CTzOperation& nc_rhs = const_cast<CTzOperation&>(rhs);
	
		m_OperationId = rhs.m_OperationId;

#if TOOLS_GUI == 1
		CTimDescriptorLine* pLine = 0;

		// update the object line reference
		if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_sOpIdText )) != 0 )
			pLine->RemoveRef( &m_sOpIdText );
#endif

		m_sOpIdText = rhs.m_sOpIdText;

#if TOOLS_GUI == 1
		// update the object line reference
		if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sOpIdText )) != 0 )
			pLine->AddRef( &m_sOpIdText );
#endif

		m_Value = rhs.m_Value;
	}
	return *this;
}


bool CTzOperation::SetOperationID( TZ_OPERATION_SPEC_T eOpId )
{
	t_TzOperationListIter Iter = CTzOperations::s_DefinedTzOperations.begin();
	while ( Iter != CTzOperations::s_DefinedTzOperations.end() )
	{
		if ( (*Iter)->m_OperationId == eOpId )
		{
			*this = *(*Iter);
			break;
		}
		Iter++;
	}
	return true;
}

bool CTzOperation::SetOperationID( string& sOpIdText )
{
	t_TzOperationListIter Iter = CTzOperations::s_DefinedTzOperations.begin();
	while ( Iter != CTzOperations::s_DefinedTzOperations.end() )
	{
		if ( (*Iter)->m_sOpIdText == sOpIdText )
		{
			*this = *(*Iter);
			break;
		}
		Iter++;
	}
	return true;
}

bool CTzOperation::ToBinary( ofstream& ofs )
{
	// validate size
//	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
//		return false;

//	ofs << Translate(*m_FieldValues[DATA_ID]);
//	ofs << Translate(*m_FieldValues[LOCATION]);

	return ofs.good();
}
		
#if TOOLS_GUI == 1
bool CTzOperation::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	if ( !TimDescriptor.AttachCommentedObject( ss, this, m_sOpIdText.c_str(), HexFormattedAscii(m_Value) ) )
		ss << m_sOpIdText << ":" << HexFormattedAscii(m_Value) << endl;

	return true;
}
#endif

#if TOOLS_GUI == 1
bool CTzOperation::LoadState( ifstream& ifs )
{
	if ( theApp.ProjectVersion >= 0x03021701 )
	{
		string sbuf;
		LoadFieldState( ifs, sbuf );
		m_OperationId = (TZ_OPERATION_SPEC_T)Translate(sbuf);
		LoadFieldState( ifs, m_sOpIdText );
		LoadFieldState( ifs, sbuf );
		m_Value = Translate(sbuf);
	}
	return ifs.good() && !ifs.fail(); // success;
}
#endif

#if TOOLS_GUI == 1
bool CTzOperation::SaveState( stringstream& ss )
{
	SaveFieldState( ss, (unsigned int&)m_OperationId );
	SaveFieldState( ss, m_sOpIdText );
	SaveFieldState( ss, m_Value );

	return true;
}
#endif

t_TzOperationList  CTzOperations::s_DefinedTzOperations;
CTzOperation NOOP_OP( TZ_NOP, "NOP" );
CTzOperation TZ_CONFIG_ENABLE_OP( TZ_CONFIG_ENABLE, "TZ_CONFIG_ENABLE" );

int	CTzOperations::s_TzOpCount = 0;

const string CTzOperations::Begin("TZ Operations");
const string CTzOperations::End("End TZ Operations");

CTzOperations::CTzOperations()
:	CErdBase( TZ_OPERATIONS_ERD, OPERATIONS_MAX )
{
	s_TzOpCount++;

	if ( s_TzOpCount == 1 )
	{
		s_DefinedTzOperations.push_back( &NOOP_OP );
		s_DefinedTzOperations.push_back( &TZ_CONFIG_ENABLE_OP );
	}
}

CTzOperations::~CTzOperations(void)
{
	Reset();
	
	s_TzOpCount--;
}

// copy constructor
CTzOperations::CTzOperations( const CTzOperations& rhs )
: CErdBase( rhs )
{
	// need to do a deep copy of lists to avoid dangling references
	CTzOperations& nc_rhs = const_cast<CTzOperations&>(rhs);

#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;
#endif

	t_TzOperationListIter iter = nc_rhs.m_TzOperations.begin();
	while ( iter != nc_rhs.m_TzOperations.end() )
	{
		CTzOperation* pOp = new CTzOperation( *(*iter) );
		m_TzOperations.push_back( pOp );

#if TOOLS_GUI == 1
		// update the object line reference
		if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
			pLine->AddRef( pOp );
#endif
		iter++;
	}
}

// assignment operator
CTzOperations& CTzOperations::operator=( const CTzOperations& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		Reset();

		CErdBase::operator=( rhs );

		// need to do a deep copy of lists to avoid dangling references
		CTzOperations& nc_rhs = const_cast<CTzOperations&>(rhs);

#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;
#endif

		t_TzOperationListIter iter = nc_rhs.m_TzOperations.begin();
		while ( iter != nc_rhs.m_TzOperations.end() )
		{
			CTzOperation* pOp = new CTzOperation( *(*iter) );
			m_TzOperations.push_back( pOp );

#if TOOLS_GUI == 1
			// update the object line reference
			if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
				pLine->AddRef( pOp );
#endif
			iter++;
		}
	}
	return *this;
}

void CTzOperations::Reset()
{
#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;
#endif

	t_TzOperationListIter iter = m_TzOperations.begin();
	while ( iter != m_TzOperations.end() )
	{
#if TOOLS_GUI == 1
		// update the object line reference
		if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
			pLine->RemoveRef( *iter );
#endif
		delete *iter;
		iter++;
	}
	m_TzOperations.clear();
}

unsigned int CTzOperations::PackageSize() 
{
	unsigned int iSize = (unsigned int)m_TzOperations.size() * 8;

	return iSize;
}

bool CTzOperations::Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine )
{
	while ( pLine = TimDescriptor.GetNextLineField( pLine ) )
	{
		bool bFound = false;
		t_TzOperationListIter iter = s_DefinedTzOperations.begin();
		while ( iter != s_DefinedTzOperations.end() )
		{
			if ( TrimWS( pLine->m_FieldName ) == (*iter)->m_sOpIdText )
			{
				CTzOperation* pOp = new CTzOperation(*(*iter));
				pOp->m_Value = Translate( pLine->m_FieldValue );
				m_TzOperations.push_back( pOp );

#if TOOLS_GUI == 1
				pLine->AddRef( pOp ); 
#endif
				bFound = true;
				break;
			}
			iter++;
		}
		if ( !bFound )
			break;
	}

	// field not found
	return true;
}

bool CTzOperations::ToBinary( ofstream& ofs )
{
	bool bRet = true;
#if 0
	// validate size
	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
		return false;

	ofs << TBR_XFER;
	ofs << PackageSize();
	ofs << Translate(*m_FieldNames[XFER_TABLE_LOC]);
	ofs << Translate(*m_FieldValues[NUM_DATA_PAIRS]);

	t_XferListIter iter = Xfers.begin();
	while ( bRet && iter != Xfers.end() )
		bRet = (*iter++)->ToBinary( ofs );
#endif

	return ( ofs.good() && bRet );
}

int CTzOperations::AddPkgStrings( CReservedPackageData* pRPD )
{
	t_TzOperationListIter iter = m_TzOperations.begin();
	while ( iter != m_TzOperations.end() )
	{
		pRPD->AddData( new string( HexFormattedAscii((*iter)->m_OperationId) ), new string( (*iter)->m_sOpIdText ) );
		pRPD->AddData( new string( HexFormattedAscii((*iter)->m_Value ) ), new string( "Value" ) );
		iter++;
	}

	return PackageSize();
}

#if TOOLS_GUI == 1
bool CTzOperations::SaveState( stringstream& ss )
{
	unsigned int temp = 0;

	CErdBase::SaveState( ss );

	temp = (unsigned int)m_TzOperations.size();
	SaveFieldState( ss, temp );

	t_TzOperationListIter iter = m_TzOperations.begin();
	while ( iter != m_TzOperations.end() )
	{
		if ( !(*iter)->SaveState( ss ) )
			return false;
		
		iter++;
	}

	return true; // SUCCESS
}
#endif

#if TOOLS_GUI == 1
bool CTzOperations::LoadState( ifstream& ifs )
{
	bool bRet = true;

	if ( theApp.ProjectVersion >= 0x03030107 )
	{
		Reset();

		string sbuf;
		CErdBase::ERD_PKG_TYPE ErdType = CErdBase::UNKNOWN_ERD;

		LoadFieldState( ifs, sbuf );  
		ErdType = CErdBase::ERD_PKG_TYPE(Translate(sbuf));
		if ( ErdType != TZ_OPERATIONS_ERD )
			return false;

		CErdBase::LoadState( ifs );

		LoadFieldState( ifs, sbuf );
		unsigned int Size = Translate(sbuf);

		for ( unsigned int i = 0; i < Size; i++ )
		{
			CTzOperation* pOp = new CTzOperation;
			bRet = pOp->LoadState( ifs );
			if ( !bRet )
			{
				delete pOp;
				break;
			}
			m_TzOperations.push_back(pOp);
		}
	}

	return bRet && ifs.good() && !ifs.fail(); // success
}
#endif

#if TOOLS_GUI == 1
bool CTzOperations::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	bool bRet = true;

	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;

	t_TzOperationListIter iter = m_TzOperations.begin();
	while ( bRet && iter != m_TzOperations.end() )
	{
		bRet = (*iter)->ToText( TimDescriptor, ss );
		iter++;
	}

	if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;

	return bRet;
}
#endif
