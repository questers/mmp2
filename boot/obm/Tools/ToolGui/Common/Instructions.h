/******************************************************************************
 *
 *  (C)Copyright 2005 - 2011 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#include "ErdBase.h"

const int MAX_INST_PARAMS = 5;

class CInstruction : public CTimLib
{
public:
	CInstruction();
	CInstruction( INSTRUCTION_OP_CODE_SPEC_T eOpCode, const string sInstructionName, unsigned int uiNumParamsUsed,
					 const string sParam0Name, const string sParam1Name, const string sParam2Name, 
                     const string sParam3Name, const string sParam4Name );
	virtual ~CInstruction(void);

	// copy constructor
	CInstruction( const CInstruction& rhs );
	// assignment operator
	CInstruction& operator=( const CInstruction& rhs );

	bool ToBinary( ofstream& ofs );

	unsigned int Size();

#if TOOLS_GUI == 1
	virtual bool LoadState( ifstream& ifs );
	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	bool SetInstructionType( INSTRUCTION_OP_CODE_SPEC_T eOpCode );
	bool SetInstructionType( string& sInstructionText );

	INSTRUCTION_OP_CODE_SPEC_T	m_InstructionOpCode;
	string						m_InstructionText;
	unsigned int				m_NumParamsUsed;
	string						m_ParamNames[ MAX_INST_PARAMS ];
	unsigned int				m_ParamValues[ MAX_INST_PARAMS ];

    string                      m_sComment;
};

typedef list<CInstruction*>           t_InstructionList;
typedef list<CInstruction*>::iterator t_InstructionListIter;


class CInstructions : public CErdBase
{
public:
	CInstructions();
	virtual ~CInstructions(void);

	// copy constructor
	CInstructions( const CInstructions& rhs );
	// assignment operator
	virtual CInstructions& operator=( const CInstructions& rhs );

	virtual void Reset();
		
	static const string Begin;	// "Instructions"
	static const string End;	// "End Instructions"

	virtual bool Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine );

	virtual bool ToBinary( ofstream& ofs );
	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize();
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

	unsigned int NumInst() { return (unsigned int)m_Instructions.size(); }

#if TOOLS_GUI == 1
	virtual bool LoadState( ifstream& ifs );
	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	enum eNumFields{ INSTRUCTION_MAX = 0 };
	t_InstructionList	m_Instructions;

	static t_InstructionList s_InstFmt;
	static int s_InstCount;
};

