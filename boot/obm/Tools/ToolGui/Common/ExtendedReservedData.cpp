/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#ifdef EXTENDED_RESERVED_DATA
#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "ExtendedReservedData.h"
#include "TimDescriptor.h"

#include "Sdram_Strings.h"
#include "Aspen_Strings.h"
#include "Mmp2_Strings.h"
#include "Mmp3_Strings.h"
#include "Ttc1_Strings.h"
#ifdef MORONA
#include "Morona_Strings.h"
#endif

#if LINUX
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

CExtendedReservedData::CExtendedReservedData(const string& sProcessorType)
    : CTimLib()
{
    ProcessorType( sProcessorType );
    m_bChanged = false;
    
    // initialize common to all processor sdram strings
    SdramStrings SdramStrings(*this);
}

CExtendedReservedData::~CExtendedReservedData(void)
{
    Reset();
    DepopulateExtendedReservedDataFields();
}

CExtendedReservedData::CExtendedReservedData( const CExtendedReservedData& rhs )
    : m_sProcessorType( rhs.m_sProcessorType ), CTimLib(rhs)
{
    // copy constructor
    m_bChanged = rhs.m_bChanged;

    // need to do a deep copy of lists to avoid dangling references
    CExtendedReservedData& nc_rhs = const_cast<CExtendedReservedData&>(rhs);

    CopyFields( ClockEnableFields, nc_rhs.ClockEnableFields );
    CopyFields( DDRGeometryFields, nc_rhs.DDRGeometryFields );
    CopyFields( DDRTimingFields, nc_rhs.DDRTimingFields );
    CopyFields( DDRCustomFields, nc_rhs.DDRCustomFields );
    CopyFields( FrequencyFields, nc_rhs.FrequencyFields );
    CopyFields( VoltagesFields, nc_rhs.VoltagesFields );
    CopyFields( ConfigMemoryControlFields, nc_rhs.ConfigMemoryControlFields );
    CopyFields( TrustZoneFields, nc_rhs.TrustZoneFields );
    CopyFields( TrustZoneRegidFields, nc_rhs.TrustZoneRegidFields );
    CopyFields( OpDivFields, nc_rhs.OpDivFields );
    CopyFields( OpModeFields, nc_rhs.OpModeFields );

    if ( nc_rhs.g_ClockEnableFields.size() > 0 )
    {
        t_stringVectorIter strIter = nc_rhs.g_ClockEnableFields.begin();
        while( strIter != nc_rhs.g_ClockEnableFields.end() )
        {
            g_ClockEnableFields.push_back( new string( *(*strIter )) );
            strIter++;
        }
    }

    if ( nc_rhs.g_SdramSpecFields.size() > 0 )
    {
        t_stringVectorIter strIter = nc_rhs.g_SdramSpecFields.begin();
        while( strIter != nc_rhs.g_SdramSpecFields.end() )
        {
            g_SdramSpecFields.push_back( new string( *(*strIter )) );
            strIter++;
        }
    }

    if ( nc_rhs.g_DDRCustomFields.size() > 0 )
    {
        t_stringVectorIter strIter = nc_rhs.g_DDRCustomFields.begin();
        while( strIter != nc_rhs.g_DDRCustomFields.end() )
        {
            g_DDRCustomFields.push_back( new string( *(*strIter )) );
            strIter++;
        }
    }

    if ( nc_rhs.g_FrequencyFields.size() > 0 )
    {
        t_stringVectorIter strIter = nc_rhs.g_FrequencyFields.begin();
        while( strIter != nc_rhs.g_FrequencyFields.end() )
        {
            g_FrequencyFields.push_back( new string( *(*strIter )) );
            strIter++;
        }
    }

    if ( nc_rhs.g_VoltagesFields.size() > 0 )
    {
        t_stringVectorIter strIter = nc_rhs.g_VoltagesFields.begin();
        while( strIter != nc_rhs.g_VoltagesFields.end() )
        {
            g_VoltagesFields.push_back( new string( *(*strIter )) );
            strIter++;
        }
    }

    if ( nc_rhs.g_ConfigMemoryControlFields.size() > 0 )
    {
        t_stringVectorIter strIter = nc_rhs.g_ConfigMemoryControlFields.begin();
        while( strIter != nc_rhs.g_ConfigMemoryControlFields.end() )
        {
            g_ConfigMemoryControlFields.push_back( new string( *(*strIter )) );
            strIter++;
        }
    }

    if ( nc_rhs.g_TrustZoneFields.size() > 0 )
    {
        t_stringVectorIter strIter = nc_rhs.g_TrustZoneFields.begin();
        while( strIter != nc_rhs.g_TrustZoneFields.end() )
        {
            g_TrustZoneFields.push_back( new string( *(*strIter )) );
            strIter++;
        }
    }
    
    if ( nc_rhs.g_TrustZoneRegidFields.size() > 0 )
    {
        t_stringVectorIter strIter = nc_rhs.g_TrustZoneRegidFields.begin();
        while( strIter != nc_rhs.g_TrustZoneRegidFields.end() )
        {
            g_TrustZoneRegidFields.push_back( new string( *(*strIter )) );
            strIter++;
        }
    }

    if ( nc_rhs.g_OpDivFields.size() > 0 )
    {
        t_stringVectorIter strIter = nc_rhs.g_OpDivFields.begin();
        while( strIter != nc_rhs.g_OpDivFields.end() )
        {
            g_OpDivFields.push_back( new string( *(*strIter )) );
            strIter++;
        }
    }
    
    if ( nc_rhs.g_OpModeFields.size() > 0 )
    {
        t_stringVectorIter strIter = nc_rhs.g_OpModeFields.begin();
        while( strIter != nc_rhs.g_OpModeFields.end() )
        {
            g_OpModeFields.push_back( new string( *(*strIter )) );
            strIter++;
        }
    }

#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;
#endif
    if ( nc_rhs.ErdVec.size() > 0 )
    {
        t_ErdBaseVectorIter ErdIter = nc_rhs.ErdVec.begin();
        while( ErdIter != nc_rhs.ErdVec.end() )
        {
            CErdBase* pErd = CErdBase::Create(*(*ErdIter));
            if ( pErd )
                ErdVec.push_back( pErd );

#if TOOLS_GUI == 1
            // update the object line reference
            if ( (pLine = CTimDescriptor::GetLineField( "", false, *ErdIter )) != 0 )
                pLine->AddRef( pErd );
#endif

            ErdIter++;
        }
    }

#if DDR_CONFIGURATION
    t_ConsumerIDVecIter ConsumerIter = nc_rhs.m_Consumers.begin();
    while( ConsumerIter != nc_rhs.m_Consumers.end() )
    {
        CConsumerID* pConsumer = dynamic_cast<CConsumerID*>(CErdBase::Create( *(*ConsumerIter) ));
        m_Consumers.push_back( pConsumer );
#if TOOLS_GUI == 1
            // update the object line reference
            if ( (pLine = CTimDescriptor::GetLineField( "", false, *ConsumerIter )) != 0 )
                pLine->AddRef( pConsumer );
#endif
        ConsumerIter++;
    }
#endif
}

CExtendedReservedData& CExtendedReservedData::operator=( const CExtendedReservedData& rhs ) 
{
    // assignment operator
    if ( &rhs != this )
    {
        CTimLib::operator=(rhs);

        // delete the existing list and recreate a new one
        Reset();
        DepopulateExtendedReservedDataFields();

        m_bChanged = rhs.m_bChanged;

        m_sProcessorType = rhs.m_sProcessorType;

        // need to do a deep copy of lists to avoid dangling references
        CExtendedReservedData& nc_rhs = const_cast<CExtendedReservedData&>(rhs);

        CopyFields( ClockEnableFields, nc_rhs.ClockEnableFields );
        CopyFields( DDRGeometryFields, nc_rhs.DDRGeometryFields );
        CopyFields( DDRTimingFields, nc_rhs.DDRTimingFields );
        CopyFields( DDRCustomFields, nc_rhs.DDRCustomFields );
        CopyFields( FrequencyFields, nc_rhs.FrequencyFields );
        CopyFields( VoltagesFields, nc_rhs.VoltagesFields );
        CopyFields( ConfigMemoryControlFields, nc_rhs.ConfigMemoryControlFields );
        CopyFields( TrustZoneFields, nc_rhs.TrustZoneFields );
        CopyFields( TrustZoneRegidFields, nc_rhs.TrustZoneRegidFields );
        CopyFields( OpDivFields, nc_rhs.OpDivFields );
        CopyFields( OpModeFields, nc_rhs.OpModeFields );

        if ( nc_rhs.g_ClockEnableFields.size() > 0 )
        {
            t_stringVectorIter strIter = nc_rhs.g_ClockEnableFields.begin();
            while( strIter != nc_rhs.g_ClockEnableFields.end() )
            {
                g_ClockEnableFields.push_back( new string( *(*strIter )) );
                strIter++;
            }
        }

        if ( nc_rhs.g_SdramSpecFields.size() > 0 )
        {
            t_stringVectorIter strIter = nc_rhs.g_SdramSpecFields.begin();
            while( strIter != nc_rhs.g_SdramSpecFields.end() )
            {
                g_SdramSpecFields.push_back( new string( *(*strIter )) );
                strIter++;
            }
        }

        if ( nc_rhs.g_DDRCustomFields.size() > 0 )
        {
            t_stringVectorIter strIter = nc_rhs.g_DDRCustomFields.begin();
            while( strIter != nc_rhs.g_DDRCustomFields.end() )
            {
                g_DDRCustomFields.push_back( new string( *(*strIter )) );
                strIter++;
            }
        }

        if ( nc_rhs.g_FrequencyFields.size() > 0 )
        {
            t_stringVectorIter strIter = nc_rhs.g_FrequencyFields.begin();
            while( strIter != nc_rhs.g_FrequencyFields.end() )
            {
                g_FrequencyFields.push_back( new string( *(*strIter )) );
                strIter++;
            }
        }

        if ( nc_rhs.g_VoltagesFields.size() > 0 )
        {
            t_stringVectorIter strIter = nc_rhs.g_VoltagesFields.begin();
            while( strIter != nc_rhs.g_VoltagesFields.end() )
            {
                g_VoltagesFields.push_back( new string( *(*strIter )) );
                strIter++;
            }
        }

        if ( nc_rhs.g_ConfigMemoryControlFields.size() > 0 )
        {
            t_stringVectorIter strIter = nc_rhs.g_ConfigMemoryControlFields.begin();
            while( strIter != nc_rhs.g_ConfigMemoryControlFields.end() )
            {
                g_ConfigMemoryControlFields.push_back( new string( *(*strIter )) );
                strIter++;
            }
        }

        if ( nc_rhs.g_TrustZoneFields.size() > 0 )
        {
            t_stringVectorIter strIter = nc_rhs.g_TrustZoneFields.begin();
            while( strIter != nc_rhs.g_TrustZoneFields.end() )
            {
                g_TrustZoneFields.push_back( new string( *(*strIter )) );
                strIter++;
            }
        }
        
        if ( nc_rhs.g_TrustZoneRegidFields.size() > 0 )
        {
            t_stringVectorIter strIter = nc_rhs.g_TrustZoneRegidFields.begin();
            while( strIter != nc_rhs.g_TrustZoneRegidFields.end() )
            {
                g_TrustZoneRegidFields.push_back( new string( *(*strIter )) );
                strIter++;
            }
        }

        if ( nc_rhs.g_OpDivFields.size() > 0 )
        {
            t_stringVectorIter strIter = nc_rhs.g_OpDivFields.begin();
            while( strIter != nc_rhs.g_OpDivFields.end() )
            {
                g_OpDivFields.push_back( new string( *(*strIter )) );
                strIter++;
            }
        }
        
        if ( nc_rhs.g_OpModeFields.size() > 0 )
        {
            t_stringVectorIter strIter = nc_rhs.g_OpModeFields.begin();
            while( strIter != nc_rhs.g_OpModeFields.end() )
            {
                g_OpModeFields.push_back( new string( *(*strIter )) );
                strIter++;
            }
        }

#if TOOLS_GUI == 1
        CTimDescriptorLine* pLine = 0;
#endif
        if ( nc_rhs.ErdVec.size() > 0 )
        {
            t_ErdBaseVectorIter ErdIter = nc_rhs.ErdVec.begin();
            while( ErdIter != nc_rhs.ErdVec.end() )
            {
                CErdBase* pErd = CErdBase::Create(*(*ErdIter));
                if ( pErd )
                    ErdVec.push_back( pErd );
#if TOOLS_GUI == 1
                // update the object line reference
                if ( (pLine = CTimDescriptor::GetLineField( "", false, *ErdIter )) != 0 )
                    pLine->AddRef( pErd );
#endif
                ErdIter++;
            }
        }

#if DDR_CONFIGURATION
        t_ConsumerIDVecIter ConsumerIter = nc_rhs.m_Consumers.begin();
        while( ConsumerIter != nc_rhs.m_Consumers.end() )
        {
            CConsumerID* pConsumer = dynamic_cast<CConsumerID*>(CErdBase::Create( *(*ConsumerIter) ));
            m_Consumers.push_back( pConsumer );
#if TOOLS_GUI == 1
            // update the object line reference
            if ( (pLine = CTimDescriptor::GetLineField( "", false, *ConsumerIter )) != 0 )
                pLine->AddRef( pConsumer );
#endif
            ConsumerIter++;
        }
#endif
    }
    return *this;
}

void CExtendedReservedData::CopyFields( t_PairList& Fields, t_PairList& nc_rhsFields )
{
#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;
#endif

    if ( nc_rhsFields.size() > 0 )
    {
        t_PairListIter iter = nc_rhsFields.begin();
        while( iter != nc_rhsFields.end() )
        {
            pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>( (*iter)->first, (*iter)->second );
            Fields.push_back( pPair );
#if TOOLS_GUI == 1
            // update the object line reference
            if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
                pLine->AddRef( pPair );
#endif
            iter++;
        }		
#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhsFields )) != 0 )
            pLine->AddRef( &Fields );
#endif
    }
}


void CExtendedReservedData::Reset(void)
{
    RemoveFieldRefs( ClockEnableFields );
    RemoveFieldRefs( DDRGeometryFields );
    RemoveFieldRefs( DDRTimingFields );
    RemoveFieldRefs( DDRCustomFields );
    RemoveFieldRefs( FrequencyFields );
    RemoveFieldRefs( VoltagesFields );
    RemoveFieldRefs( ConfigMemoryControlFields );
    RemoveFieldRefs( TrustZoneFields );
    RemoveFieldRefs( TrustZoneRegidFields );
    RemoveFieldRefs( OpDivFields );
    RemoveFieldRefs( OpModeFields );

#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;
#endif
    t_ErdBaseVectorIter ErdIter = ErdVec.begin();
    while( ErdIter != ErdVec.end() )
    {
#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, *ErdIter )) != 0 )
            pLine->RemoveRef( *ErdIter );
#endif
        delete *ErdIter;
        ErdIter++;
    }
    ErdVec.clear();

#if DDR_CONFIGURATION
    t_ConsumerIDVecIter ConsumerIter = m_Consumers.begin();
    while( ConsumerIter != m_Consumers.end() )
    {
#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, *ConsumerIter )) != 0 )
            pLine->RemoveRef( *ConsumerIter );
#endif
        delete *ConsumerIter;
        ConsumerIter++;
    }
    m_Consumers.clear();
#endif

    m_bChanged = false;
}

void CExtendedReservedData::RemoveFieldRefs( t_PairList& Fields )
{
#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;
#endif
    t_PairListIter Iter = Fields.begin();
    while( Iter != Fields.end() )
    {
#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, *Iter )) != 0 )
            pLine->RemoveRef( *Iter );
#endif
        delete *Iter;
        Iter++;
    }
    Fields.clear();
}


#if TOOLS_GUI == 1
bool CExtendedReservedData::SaveState( stringstream& ss )
{
    unsigned int temp = 0;

    SaveFieldStatePtr( ss, &ClockEnableFields );
    temp = (unsigned int)ClockEnableFields.size();
    SaveFieldState( ss, temp );
    t_PairListIter iter = ClockEnableFields.begin();
    while( iter != ClockEnableFields.end() )
        SaveFieldState( ss, *(*iter++) );

    SaveFieldStatePtr( ss, &DDRGeometryFields );
    temp = (unsigned int)DDRGeometryFields.size();
    SaveFieldState( ss, temp );
    iter = DDRGeometryFields.begin();
    while( iter != DDRGeometryFields.end() )
        SaveFieldState( ss, *(*iter++) );

    SaveFieldStatePtr( ss, &DDRTimingFields );
    temp = (unsigned int)DDRTimingFields.size();
    SaveFieldState( ss, temp );
    iter = DDRTimingFields.begin();
    while( iter != DDRTimingFields.end() )
        SaveFieldState( ss, *(*iter++) );

    SaveFieldStatePtr( ss, &DDRCustomFields );
    temp = (unsigned int)DDRCustomFields.size();
    SaveFieldState( ss, temp );
    iter = DDRCustomFields.begin();
    while( iter != DDRCustomFields.end() )
        SaveFieldState( ss, *(*iter++) );

    SaveFieldStatePtr( ss, &FrequencyFields );
    temp = (unsigned int)FrequencyFields.size();
    SaveFieldState( ss, temp );
    iter = FrequencyFields.begin();
    while( iter != FrequencyFields.end() )
        SaveFieldState( ss, *(*iter++) );

    SaveFieldStatePtr( ss, &VoltagesFields );
    temp = (unsigned int)VoltagesFields.size();
    SaveFieldState( ss, temp );
    iter = VoltagesFields.begin();
    while( iter != VoltagesFields.end() )
        SaveFieldState( ss, *(*iter++) );

    SaveFieldStatePtr( ss, &ConfigMemoryControlFields );
    temp = (unsigned int)ConfigMemoryControlFields.size();
    SaveFieldState( ss, temp );
    iter = ConfigMemoryControlFields.begin();
    while( iter != ConfigMemoryControlFields.end() )
        SaveFieldState( ss, *(*iter++) );

    SaveFieldStatePtr( ss, &TrustZoneFields );
    temp = (unsigned int)TrustZoneFields.size();
    SaveFieldState( ss, temp );
    iter = TrustZoneFields.begin();
    while( iter != TrustZoneFields.end() )
        SaveFieldState( ss, *(*iter++) );

    SaveFieldStatePtr( ss, &TrustZoneRegidFields );
    temp = (unsigned int)TrustZoneRegidFields.size();
    SaveFieldState( ss, temp );
    iter = TrustZoneRegidFields.begin();
    while( iter != TrustZoneRegidFields.end() )
        SaveFieldState( ss, *(*iter++) );

    SaveFieldStatePtr( ss, &OpDivFields );
    temp = (unsigned int)OpDivFields.size();
    SaveFieldState( ss, temp );
    iter = OpDivFields.begin();
    while( iter != OpDivFields.end() )
        SaveFieldState( ss, *(*iter++) );

    SaveFieldStatePtr( ss, &OpModeFields );
    temp = (unsigned int)OpModeFields.size();
    SaveFieldState( ss, temp );
    iter = OpModeFields.begin();
    while( iter != OpModeFields.end() )
        SaveFieldState( ss, *(*iter++) );

    SaveFieldState( ss, m_sProcessorType );

    temp = (unsigned int)ErdVec.size();
    SaveFieldState( ss, temp );

    t_ErdBaseVectorIter ErdIter = ErdVec.begin();
    while( ErdIter != ErdVec.end() )
    {
        (*ErdIter)->SaveState(ss);
        ErdIter++;
    }

#if DDR_CONFIGURATION
    temp = (unsigned int)m_Consumers.size();
    SaveFieldState( ss, temp );

    t_ConsumerIDVecIter ConsumerIter = m_Consumers.begin();
    while( ConsumerIter != m_Consumers.end() )
    {
        (*ConsumerIter)->SaveState(ss);
        ConsumerIter++;
    }
#endif

    return true;
}
#endif

#if TOOLS_GUI == 1
bool CExtendedReservedData::LoadState( ifstream& ifs )
{
    string sbuf;
    int iPackageSize = 0;

    Reset();

    if ( theApp.ProjectVersion > 0x03021401 )
        LoadFieldStatePtr( ifs, &ClockEnableFields );
    LoadFieldState( ifs, sbuf );  
    int nClockEnableFields = Translate(sbuf);
    while ( nClockEnableFields > 0 )
    {
        pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>;
        LoadFieldState( ifs, *pPair );  
        ClockEnableFields.push_back( pPair );
        nClockEnableFields--;
    }

    if ( theApp.ProjectVersion > 0x03021401 )
        LoadFieldStatePtr( ifs, &DDRGeometryFields );
    LoadFieldState( ifs, sbuf );  
    int nDDRGeometryFields = Translate(sbuf);
    while ( nDDRGeometryFields > 0 )
    {
        pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>;
        LoadFieldState( ifs, *pPair );  
        DDRGeometryFields.push_back( pPair );
        nDDRGeometryFields--;
    }

    if ( theApp.ProjectVersion > 0x03021401 )
        LoadFieldStatePtr( ifs, &DDRTimingFields );
    LoadFieldState( ifs, sbuf);  
    int nDDRTimingFields = Translate(sbuf);
    while ( nDDRTimingFields > 0 )
    {
        pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>;
        LoadFieldState( ifs, *pPair );  
        DDRTimingFields.push_back( pPair );
        nDDRTimingFields--;
    }

    if ( theApp.ProjectVersion > 0x03021401 )
        LoadFieldStatePtr( ifs, &DDRCustomFields );
    LoadFieldState( ifs, sbuf );  
    int nDDRCustomFields = Translate(sbuf);
    while ( nDDRCustomFields > 0 )
    {
        pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>;
        LoadFieldState( ifs, *pPair );  
        DDRCustomFields.push_back( pPair );
        nDDRCustomFields--;
    }

    if ( theApp.ProjectVersion > 0x03021401 )
        LoadFieldStatePtr( ifs, &FrequencyFields );
    LoadFieldState( ifs, sbuf );  
    int nFrequencyFields = Translate(sbuf);
    while ( nFrequencyFields > 0 )
    {
        pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>;
        LoadFieldState( ifs, *pPair );  
        FrequencyFields.push_back( pPair );
        nFrequencyFields--;
    }

    if ( theApp.ProjectVersion > 0x03021401 )
        LoadFieldStatePtr( ifs, &VoltagesFields );
    LoadFieldState( ifs, sbuf );  
    int nVoltagesFields = Translate(sbuf);
    while ( nVoltagesFields > 0 )
    {
        pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>;
        LoadFieldState( ifs, *pPair );  
        VoltagesFields.push_back( pPair );
        nVoltagesFields--;
    }

    if ( theApp.ProjectVersion >= 0x03021100 )
    {
        if ( theApp.ProjectVersion > 0x03021401 )
            LoadFieldStatePtr( ifs, &ConfigMemoryControlFields );
        LoadFieldState( ifs, sbuf );  
        int nCMCCFields = Translate(sbuf);
        while ( nCMCCFields > 0 )
        {
            pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>;
            LoadFieldState( ifs, *pPair );  
            ConfigMemoryControlFields.push_back( pPair );
            nCMCCFields--;
        }
    }

    if ( theApp.ProjectVersion >= 0x03021203 )
    {
        if ( theApp.ProjectVersion > 0x03021401 )
            LoadFieldStatePtr( ifs, &TrustZoneFields );
        LoadFieldState( ifs, sbuf );  
        int nTrustZoneFields = Translate(sbuf);
        while ( nTrustZoneFields > 0 )
        {
            pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>;
            LoadFieldState( ifs, *pPair );  
            TrustZoneFields.push_back( pPair );
            nTrustZoneFields--;
        }
    }

    if ( theApp.ProjectVersion >= 0x03021204 )
    {
        if ( theApp.ProjectVersion > 0x03021401 )
            LoadFieldStatePtr( ifs, &TrustZoneRegidFields );
        LoadFieldState( ifs, sbuf );  
        int nTrustZoneRegidFields = Translate(sbuf);
        while ( nTrustZoneRegidFields > 0 )
        {
            pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>;
            LoadFieldState( ifs, *pPair );  
            TrustZoneRegidFields.push_back( pPair );
            nTrustZoneRegidFields--;
        }
    }

    if ( theApp.ProjectVersion >= 0x03021203 )
    {
        if ( theApp.ProjectVersion > 0x03021401 )
            LoadFieldStatePtr( ifs, &OpDivFields );
        LoadFieldState( ifs, sbuf );  
        int nOpDivFields = Translate(sbuf);
        while ( nOpDivFields > 0 )
        {
            pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>;
            LoadFieldState( ifs, *pPair );  
            OpDivFields.push_back( pPair );
            nOpDivFields--;
        }

        if ( theApp.ProjectVersion > 0x03021401 )
            LoadFieldStatePtr( ifs, &OpModeFields );
        LoadFieldState( ifs, sbuf );  
        int nOpModeFields = Translate(sbuf);
        while ( nOpModeFields > 0 )
        {
            pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>;
            LoadFieldState( ifs, *pPair );  
            OpModeFields.push_back( pPair );
            nOpModeFields--;
        }
    }

    if ( theApp.ProjectVersion >= 0x03020206 )
        LoadFieldState( ifs, m_sProcessorType );  

    if ( theApp.ProjectVersion < 0x03021400 )
        ProcessorSpecificFields(m_sProcessorType);
        
    if ( theApp.ProjectVersion >= 0x03021200 )
    {
        LoadFieldState( ifs, sbuf );  
        unsigned int ErdSize = Translate(sbuf);		
        CErdBase::ERD_PKG_TYPE ErdType = CErdBase::UNKNOWN_ERD;
        CErdBase* pErd = 0;
        for ( unsigned int i = 0; i < ErdSize; i++ )
        {
            LoadFieldState( ifs, sbuf );  
            ErdType = CErdBase::ERD_PKG_TYPE(Translate(sbuf));
            pErd = CErdBase::Create(ErdType);
            if ( pErd )
            {
                pErd->LoadState( ifs );
                ErdVec.push_back( pErd );
                pErd = 0;
                sbuf = "";
            }
            else
            {
                // error parsing project file
                pErd = 0;
                sbuf = "";
                return false;
            }
        }
    }

#if DDR_CONFIGURATION
    if ( theApp.ProjectVersion >= 0x03021701 )
    {
        LoadFieldState( ifs, sbuf );  
        unsigned int ConsumerIdSize = Translate(sbuf);		

        CConsumerID* pConsumer = 0;
        for ( unsigned int i = 0; i < ConsumerIdSize; i++ )
        {
            pConsumer = new CConsumerID;
            if ( pConsumer )
            {
                pConsumer->LoadState( ifs );
                m_Consumers.push_back( pConsumer );
            }
            else
            {
                // error parsing project file
                pConsumer = 0;
                return false;
            }
        }
    }
#endif

    return ifs.good() && !ifs.fail(); // success
}
#endif

int CExtendedReservedData::Combine(t_ReservedDataList& ReservedDataList)
{
    int nBytesAdded = 0;
    // add ClockEnable package
    if ( ClockEnableFields.size() > 0 )
        nBytesAdded += AddPkg(sCLKE, ClockEnableFields, ReservedDataList);

    if ( DDRGeometryFields.size() > 0 )
        nBytesAdded += AddPkg(sDDRG, DDRGeometryFields, ReservedDataList);

    if ( DDRTimingFields.size() > 0 )
        nBytesAdded += AddPkg(sDDRT, DDRTimingFields, ReservedDataList);
    
    if ( DDRCustomFields.size() > 0 )
        nBytesAdded += AddPkg(sDDRC, DDRCustomFields, ReservedDataList);

    if ( FrequencyFields.size() > 0 )
        nBytesAdded += AddPkg(sFREQ, FrequencyFields, ReservedDataList);

    if ( VoltagesFields.size() > 0 )
        nBytesAdded += AddPkg(sVOLT, VoltagesFields, ReservedDataList);

    if ( ConfigMemoryControlFields.size() > 0 )
        nBytesAdded += AddPkg(sCMCC, ConfigMemoryControlFields, ReservedDataList);

    if ( TrustZoneFields.size() > 0 )
        nBytesAdded += AddPkg(sTZID, TrustZoneFields, ReservedDataList);

    if ( TrustZoneRegidFields.size() > 0 )
        nBytesAdded += AddPkg(sTZON, TrustZoneRegidFields, ReservedDataList);

    if ( OpDivFields.size() > 0 )
        nBytesAdded += AddPkg(sOPDV, OpDivFields, ReservedDataList);

    if ( OpModeFields.size() > 0 )
        nBytesAdded += AddPkg(sMODE, OpModeFields, ReservedDataList);

#if DDR_CONFIGURATION
    if ( m_Consumers.size() > 0 )
    {
        CReservedPackageData* pRPD = new CReservedPackageData;
        pRPD->PackageIdTag( HexFormattedAscii(CIDPID) );		

        nBytesAdded +=8; // WRAH; 
        
        string* pData = new string;
        *pData = HexFormattedAscii( (unsigned int)m_Consumers.size() );
        pRPD->AddData( pData,  new string( "num CIDs" ) );

        nBytesAdded += 4; // num CIDs 

        t_ConsumerIDVecIter ConsumerIter = m_Consumers.begin();
        while( ConsumerIter != m_Consumers.end() )
        {
            nBytesAdded += (*ConsumerIter)->AddPkgStrings( pRPD );
            ConsumerIter++;	
        }

        ReservedDataList.push_back(pRPD);
    }			
#endif

    t_ErdBaseVectorIter ErdIter = ErdVec.begin();
    while( ErdIter != ErdVec.end() )
    {
        CReservedPackageData* pRPD = new CReservedPackageData;
        nBytesAdded += (*ErdIter)->AddPkgStrings( pRPD );
        ReservedDataList.push_back(pRPD);
        ErdIter++;
    }

    return nBytesAdded;
}

int CExtendedReservedData::AddPkg(const string& sSectionTag, t_PairList& Fields, t_ReservedDataList& ReservedDataList)
{
    int nBytesAdded = 0;
    CReservedPackageData* pPkg = new CReservedPackageData;
    pPkg->PackageIdTag( sSectionTag ); 
    t_PairListIter iter = Fields.begin();
    while ( iter != Fields.end() )
    {
        pPkg->PackageDataList().push_back( new string( HexFormattedAscii((*iter)->first )) );
        pPkg->PackageDataList().push_back( new string( HexFormattedAscii((*iter)->second )) );
        iter++;
    }
    nBytesAdded += pPkg->Size();
    ReservedDataList.push_back( pPkg );

    m_bChanged = true;

    return nBytesAdded;
}


#if TOOLS_GUI == 1
stringstream& CExtendedReservedData::Text( stringstream& ss, CTimDescriptor& TimDescriptor )
{
    string sEnd("End ");
    size_t iFields = ClockEnableFields.size() + DDRGeometryFields.size() + DDRTimingFields.size() 
        + DDRCustomFields.size() + FrequencyFields.size() + VoltagesFields.size() 
        + OpDivFields.size() + OpModeFields.size()
        + ConfigMemoryControlFields.size() + TrustZoneFields.size() + TrustZoneRegidFields.size();
         
    // quick exit if no fields to output
    if ( iFields == 0 && ( ErdVec.size() == 0 ) && m_Consumers.size() == 0 )
        return ss;

    if ( m_sProcessorType.empty() )
    {
        printf( "No Processor Type for Extended Reserved Data...\n" );
        return ss;
    }

    if ( !TimDescriptor.AttachCommentedObject( ss, this, sExtendedReservedData.c_str(), string("") ) )
        ss << sExtendedReservedData << ":" << endl;

    t_PairListIter iter;

    if ( ClockEnableFields.size() > 0 )
        ERDPackageText( TimDescriptor, ss, ClockEnableFields, sClockEnable, g_ClockEnableFields );

    if ( DDRGeometryFields.size() > 0 )
        ERDPackageText( TimDescriptor, ss, DDRGeometryFields, sDDRGeometry, g_SdramSpecFields );

    if ( DDRTimingFields.size() > 0 )
        ERDPackageText( TimDescriptor, ss, DDRTimingFields, sDDRTiming, g_SdramSpecFields );

    if ( DDRCustomFields.size() > 0 )
        ERDPackageText( TimDescriptor, ss, DDRCustomFields, sDDRCustom, g_DDRCustomFields );

    if ( FrequencyFields.size() > 0 )
        ERDPackageText( TimDescriptor, ss, FrequencyFields, sFrequency, g_FrequencyFields );

    if ( VoltagesFields.size() > 0 )
        ERDPackageText( TimDescriptor, ss, VoltagesFields, sVoltages, g_VoltagesFields );

    if ( ConfigMemoryControlFields.size() > 0 )
        ERDPackageText( TimDescriptor, ss, ConfigMemoryControlFields, sConfigMemoryControl, g_ConfigMemoryControlFields );

    if ( TrustZoneFields.size() > 0 )
        ERDPackageText( TimDescriptor, ss, TrustZoneFields, sTrustZone, g_TrustZoneFields );

    if ( TrustZoneRegidFields.size() > 0 )
        ERDPackageText( TimDescriptor, ss, TrustZoneRegidFields, sTrustZoneRegid, g_TrustZoneRegidFields );

    if ( OpDivFields.size() > 0 )
        ERDPackageText( TimDescriptor, ss, OpDivFields, sOpDiv, g_OpDivFields );

    if ( OpModeFields.size() > 0 )
        ERDPackageText( TimDescriptor, ss, OpModeFields, sOpMode, g_OpModeFields );

    t_ConsumerIDVecIter ConsumerIter = m_Consumers.begin();
    while( ConsumerIter != m_Consumers.end() )
    {
        (*ConsumerIter)->ToText( TimDescriptor, ss );
        ConsumerIter++;
    }

    if ( ErdVec.size() > 0 )
    {
        t_ErdBaseVectorIter ErdIter = ErdVec.begin();
        while( ErdIter != ErdVec.end() )
        {
            (*ErdIter)->ToText( TimDescriptor, ss );
            ErdIter++;
        }
    }

    if ( !TimDescriptor.AttachCommentedObject( ss, this, (sEnd+sExtendedReservedData).c_str(), string("") ) )
        ss << (sEnd+sExtendedReservedData) << ":" << endl;

    return ss;
}


void CExtendedReservedData::ERDPackageText( CTimDescriptor& TimDescriptor, stringstream& ss,
                                            t_PairList& Fields, const string& sPkgName, t_stringVector& g_Fields )
{
    string sText("");
    string sEnd("End ");
    if ( !TimDescriptor.AttachCommentedObject( ss, &Fields, sPkgName.c_str(),sText ) )
    {
        ss << sPkgName << ":" << endl;
    }

    t_PairListIter iter = Fields.begin();
    while( iter != Fields.end() )
    {
        if ( (*iter)->first < g_Fields.size() )
        {
            if ( !TimDescriptor.AttachCommentedObject( ss, (*iter), (*(g_Fields[(*iter)->first])).c_str(), HexFormattedAscii((*iter)->second) ) )
            {
                ss << *(g_Fields[(*iter)->first]) << ": ";
                ss << HexFormattedAscii((*iter)->second) << endl;
            }
        }
        else
        {
            ss << ";" << HexFormattedAscii((*iter)->first) << ": ";
            ss << HexFormattedAscii((*iter)->second);
            ss << " **** field number not legal for processor type ****" << endl;
        }

        iter++;
    }
    if ( !TimDescriptor.AttachCommentedObject( ss, &Fields, (sEnd+sPkgName).c_str(), sText ) )
    {
        ss << sEnd << sPkgName << ":" << endl;
    }
}
#endif

void CExtendedReservedData::DepopulateExtendedReservedDataFields()
{
    Depopulate(g_ClockEnableFields);
    Depopulate(g_FrequencyFields);
    Depopulate(g_SdramSpecFields);
    Depopulate(g_DDRCustomFields);
    Depopulate(g_VoltagesFields);
    Depopulate(g_ConfigMemoryControlFields);
    Depopulate(g_TrustZoneFields);
    Depopulate(g_TrustZoneRegidFields);
    Depopulate(g_OpDivFields);
    Depopulate(g_OpModeFields);
}

void CExtendedReservedData::Depopulate( t_stringVector& Fields )
{
    for ( unsigned int i = 0; i < Fields.size(); i++ )
        delete Fields[i];
    Fields.clear();
}

bool CExtendedReservedData::ProcessorSpecificFields( const string& sProcessor )
{
    DepopulateExtendedReservedDataFields();

    if ( sProcessor.size() > 0 )
    {
        if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[PXA30x]) )
            return true;
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[PXA31x]) )
            return true;
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[PXA32x]) )
            return true;
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[PXA168]) )
            //initialize ExtReservedData with Aspen specific strings;
            AspenStrings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[PXA688]) )
            //initialize ExtReservedData with MMP2 specific strings;
            MMP2Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[PXA91x]) )
            //initialize ExtReservedData with TTC1 specific strings;
            TTC1Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[PXA92x]) )
            //initialize ExtReservedData with TTC1 specific strings;
            TTC1Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[PXA93x]) )
            //initialize ExtReservedData with TTC1 specific strings;
            TTC1Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[PXA94x]) )
            //initialize ExtReservedData with TTC1 specific strings;
            TTC1Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[PXA95x]) )
            //initialize ExtReservedData with TTC1 specific strings;
            TTC1Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[ARMADA168]) )
            //initialize ExtReservedData with Aspen specific strings;
            AspenStrings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[ARMADA610]) )
            //initialize ExtReservedData with MMP2 specific strings;
            MMP2Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[MG1]) )
            //initialize ExtReservedData with MMP3 specific strings;
            MMP3Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[TAVOR_PV_MG2]) )
            //initialize ExtReservedData with MMP3 specific strings;
            MMP3Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[ESHEL]) )
            //initialize ExtReservedData with MMP3 specific strings;
            MMP3Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[PXA978]) )
            //initialize ExtReservedData with MMP3 specific strings;
            MMP3Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[ARMADA620]) )
            //initialize ExtReservedData with MMP3 specific strings;
            MMP3Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[ESHEL_LTE]) )
            //initialize ExtReservedData with MMP3 specific strings;
            MMP3Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[ARMADA622]) )
            //initialize ExtReservedData with MMP3 specific strings;
            MMP3Strings CustomFields(*this);
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[WUKONG]) )
            //initialize ExtReservedData with MMP3 specific strings;
            MMP3Strings CustomFields(*this);
    #ifdef MORONA
        else if ( ToUpper(sProcessor) == ToUpper(gsProcessorType[P_88AP001]) )
            //initialize ExtReservedData with Morona specific strings;
            MoronaStrings CustomFields(*this);
    #endif
        else
        {
             printf( "Error: Processor Type not recognized...Parsing \n" );
             return false;
        }

    }
    // initialize common to all processor sdram strings
    SdramStrings SdramStrings(*this);

    return true;
}

int CExtendedReservedData::Size()
{
    int iSize = (int)(
                // size indicate number of pairs in list
                // *8 for number of bytes in field id & field value pair
                // + 8 for package tag & package size
          (ClockEnableFields.size() > 0 ? (ClockEnableFields.size()*8) + 8 : 0)
        + (DDRGeometryFields.size() > 0 ? (DDRGeometryFields.size()*8) + 8 : 0)
        + (DDRTimingFields.size() > 0 ? (DDRTimingFields.size()*8) + 8 : 0)
        + (DDRCustomFields.size() > 0 ? (DDRCustomFields.size()*8) + 8 : 0)
        + (FrequencyFields.size() > 0 ? (FrequencyFields.size()*8) + 8 : 0)
        + (VoltagesFields.size() > 0 ? (VoltagesFields.size()*8) + 8 : 0)
        + (ConfigMemoryControlFields.size() > 0 ? (ConfigMemoryControlFields.size()*8) + 8 : 0)
        + (TrustZoneFields.size() > 0 ? (TrustZoneFields.size()*8) + 8 : 0)
        + (TrustZoneRegidFields.size() > 0 ? (TrustZoneRegidFields.size()*8) + 8 : 0)
        + (OpDivFields.size() > 0 ? (OpDivFields.size()*8) + 8 : 0)
        + (OpModeFields.size() > 0 ? (OpModeFields.size()*8) + 8 : 0)
            );	
    
    t_ErdBaseVectorIter ErdIter = ErdVec.begin();
    while( ErdIter != ErdVec.end() )
    {
        iSize += (*ErdIter)->PackageSize();
        ErdIter++;
    }

#if DDR_CONFIGURATION
    if ( m_Consumers.size() > 0 )
    {
        iSize += 12; // 8(WRAH) + 4 (num CIDs)
        t_ConsumerIDVecIter ConsumerIter = m_Consumers.begin();
        while( ConsumerIter != m_Consumers.end() )
        {
            iSize += (*ConsumerIter)->PackageSize();
            ConsumerIter++;
        }
    }
#endif

    return iSize;
}

#endif //EXTENDED_RESERVED_DATA
