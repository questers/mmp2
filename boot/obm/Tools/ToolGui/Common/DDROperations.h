/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#include "ErdBase.h"
#include "Tim.h"

class CDDROperation : public CTimLib
{
public:
	CDDROperation();
	CDDROperation( DDR_OPERATION_SPEC_T OpId, const string & sOpIdText );
	virtual ~CDDROperation(void);

	// copy constructor
	CDDROperation( const CDDROperation& rhs );
	// assignment operator
	CDDROperation& operator=( const CDDROperation& rhs );

	bool ToBinary( ofstream& ofs );

#if TOOLS_GUI == 1
	virtual bool LoadState( ifstream& ifs );
	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	bool SetOperationID( DDR_OPERATION_SPEC_T eOpId );
	bool SetOperationID( string& sOpIdText );

	DDR_OPERATION_SPEC_T	m_OperationId;
	string					m_sOpIdText;
	unsigned int			m_Value;

	enum eDDROperationFields { DDR_OP_ID, DDR_OP_FIELDS_MAX };
};

typedef list<CDDROperation*>           t_DDROperationList;
typedef list<CDDROperation*>::iterator t_DDROperationListIter;


class CDDROperations : public CErdBase
{
public:
	CDDROperations();
	virtual ~CDDROperations(void);

	// copy constructor
	CDDROperations( const CDDROperations& rhs );
	// assignment operator
	virtual CDDROperations& operator=( const CDDROperations& rhs );

	virtual void Reset();
		
	static const string Begin;	// "Operations"
	static const string End;	// "End Operations"

	virtual bool Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine );

	virtual bool ToBinary( ofstream& ofs );
	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize();
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

	unsigned int NumOps() { return (unsigned int)m_DdrOperations.size(); }

#if TOOLS_GUI == 1
	virtual bool LoadState( ifstream& ifs );
	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	enum eNumFields{ OPERATIONS_MAX = 0 };
	t_DDROperationList	m_DdrOperations;

	static t_DDROperationList s_DefinedDDROperations;
	static int				  s_DDROpCount;
};

