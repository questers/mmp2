/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#include "ErdBase.h"

#include "Instructions.h"
#include "DDROperations.h"

class CDDRInitialization : public CErdBase
{
public:
	CDDRInitialization();
	virtual ~CDDRInitialization(void);

	// copy constructor
	CDDRInitialization( const CDDRInitialization& rhs );
	// assignment operator
	virtual CDDRInitialization& operator=( const CDDRInitialization& rhs );

	static const string Begin;	// "DDR Initialization"
	static const string End;	// "End DDR Initialization"

	virtual bool Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine );

	virtual bool ToBinary( ofstream& ofs );
	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize();
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

#if TOOLS_GUI == 1
	virtual bool LoadState( ifstream& ifs );
	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	enum DDRInit { DDR_PID, DDR_INIT_MAX };

	string			m_sDdrPID;

	CDDROperations	m_DdrOperations;
	CInstructions	m_DdrInstructions;

private:
	bool ParseERDPackage( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine, CErdBase*& pErd, const string& sBegin, const string& sEnd );

};

