/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#if TOOLS_GUI == 1
#include "stdafx.h"
#include "MarvellBootUtility.h"
#endif

#include "ConsumerID.h"
#include "TimDescriptorParser.h"

#if LINUX
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif


const string CConsumerID::Begin("Consumer ID");
const string CConsumerID::End("End Consumer ID");

CConsumerID::CConsumerID()
:	CErdBase( CONSUMER_ID_ERD, CONSUMERID_MAX )
{
	*m_FieldNames[CID] = "CID";
	*m_FieldNames[PID] = "PID";
}

CConsumerID::~CConsumerID(void)
{
	Reset();
}

void CConsumerID::Reset()
{
#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;
#endif

	t_stringVectorIter iter = m_PIDs.begin();
	while ( iter != m_PIDs.end() )
	{
#if TOOLS_GUI == 1
		// update the object line reference
		if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
			pLine->RemoveRef( *iter );
#endif
		delete *iter;
		iter++;
	}
	m_PIDs.clear();
}

CConsumerID::CConsumerID( const CConsumerID& rhs )
: CErdBase( rhs )
{
	// copy constructor

	// need to do a deep copy of lists to avoid dangling references
	CConsumerID& nc_rhs = const_cast<CConsumerID&>(rhs);

#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;
#endif

	t_stringVectorIter iter = nc_rhs.m_PIDs.begin();
	while ( iter != nc_rhs.m_PIDs.end() )
	{
		string* psPID = new string(*(*iter));
		m_PIDs.push_back( psPID );
#if TOOLS_GUI == 1
		// update the object line reference
		if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
			pLine->AddRef( psPID );
#endif
		iter++;
	}
}

CConsumerID& CConsumerID::operator=( const CConsumerID& rhs ) 
{
	// assignment operator
	if ( &rhs != this )
	{
		Reset();

		CErdBase::operator=( rhs );

		// need to do a deep copy of lists to avoid dangling references
		CConsumerID& nc_rhs = const_cast<CConsumerID&>(rhs);

#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;
#endif

		t_stringVectorIter iter = nc_rhs.m_PIDs.begin();
		while ( iter != nc_rhs.m_PIDs.end() )
		{
			string* psPID = new string(*(*iter));
			m_PIDs.push_back( psPID );
#if TOOLS_GUI == 1
			// update the object line reference
			if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
				pLine->AddRef( psPID );
#endif
			iter++;
		}
	}
	return *this;
}

bool CConsumerID::ToBinary( ofstream& ofs )
{
	// validate size
//	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
//		return false;

//	ofs << Translate(*m_FieldValues[DATA_ID]);
//	ofs << Translate(*m_FieldValues[LOCATION]);

	return ofs.good();
}
		
#if TOOLS_GUI == 1
bool CConsumerID::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;
	
	if ( !TimDescriptor.AttachCommentedObject( ss, m_FieldNames[CID], m_FieldNames[CID]->c_str(), *m_FieldValues[CID] ) )
		ss << *m_FieldNames[CID] << ": " << *m_FieldValues[CID] << endl;

	t_stringVectorIter iter = m_PIDs.begin();
	while ( iter != m_PIDs.end() )
	{
		if ( !TimDescriptor.AttachCommentedObject( ss, *iter, m_FieldNames[PID]->c_str(), *(*iter) ) )
			ss << *m_FieldNames[PID] << ": " << *(*iter) << endl;
	
		iter++;
	}

	if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;

	return true;
}
#endif


unsigned int CConsumerID::PackageSize() 
{
	// 4 (CID) + 4(num PIDS) + actual size of PIDs
	unsigned int iSize = 4 + 4 + ((unsigned int)m_PIDs.size() * 4); 
	return iSize;
}

bool CConsumerID::Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine )
{
	while ( pLine = TimDescriptor.GetNextLineField( pLine ) )
	{
		bool bFound = false;
		if ( TrimWS( pLine->m_FieldName ) == *m_FieldNames[ CID ] )
		{
#if TOOLS_GUI == 1
			pLine->AddRef( m_FieldNames[ CID ] ); 
#endif
			*m_FieldValues[ CID ] = TrimWS( pLine->m_FieldValue );
			bFound = true;
		}
		else if ( TrimWS( pLine->m_FieldName ) == *m_FieldNames[ PID ] )
		{
			string* pPID = new string( TrimWS( pLine->m_FieldValue ) );
			m_PIDs.push_back( pPID );
#if TOOLS_GUI == 1
			pLine->AddRef( pPID ); 
#endif
			bFound = true;
		}

		if ( !bFound )
			break;
	}

	// field not found
	return true;
}


int CConsumerID::AddPkgStrings( CReservedPackageData* pRPD )
{
	string* pData = new string;
	TextToHexFormattedAscii( *pData, *m_FieldValues[ CID ]);
	pRPD->AddData( pData,  new string( *m_FieldValues[ CID ] ) );

	pData = new string;
	*pData = HexFormattedAscii( (unsigned int)m_PIDs.size() );
	pRPD->AddData( pData,  new string( "num PIDs" ) );

	t_stringVectorIter iter = m_PIDs.begin();
	while ( iter != m_PIDs.end() )
	{
		pData = new string;
		TextToHexFormattedAscii( *pData, *(*iter) );
		pRPD->AddData( pData,  new string( *(*iter) ) );
		iter++;
	}

	return 8+((int)(pRPD->PackageDataList().size())*4) ; // CID +  num PIDS + PIDS data
}

#if TOOLS_GUI == 1
bool CConsumerID::SaveState( stringstream& ss )
{
	unsigned int temp = 0;
	
	CErdBase::SaveState( ss );

	temp = (unsigned int)m_PIDs.size();
	SaveFieldState( ss, temp );

	t_stringVectorIter iter = m_PIDs.begin();
	while ( iter != m_PIDs.end() )
		SaveFieldState( ss, *(*iter++) );

	return true; // SUCCESS
}
#endif

#if TOOLS_GUI == 1
bool CConsumerID::LoadState( ifstream& ifs )
{
	bool bRet = true;

	if ( theApp.ProjectVersion >= 0x03021701 )
	{
		Reset();

		string sbuf;
		CErdBase::ERD_PKG_TYPE ErdType = CErdBase::UNKNOWN_ERD;

		LoadFieldState( ifs, sbuf );  
		ErdType = CErdBase::ERD_PKG_TYPE(Translate(sbuf));
		if ( ErdType != CONSUMER_ID_ERD )
			return false;

		CErdBase::LoadState( ifs );

		LoadFieldState( ifs, sbuf );
		unsigned int Size = Translate(sbuf);

		for ( unsigned int i = 0; i < Size; i++ )
		{
			string* psPID = new string;
			if ( LoadFieldState( ifs, *psPID ) )
				m_PIDs.push_back( psPID );
			else
			{
				delete psPID;
				return false;
			}
		}
	}

	return bRet && ifs.good() && !ifs.fail(); // success
}
#endif

