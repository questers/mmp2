/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "ErdBase.h"

#include <list>
using namespace std;

class CXfer : public CErdBase
{
public:
	CXfer();
	virtual ~CXfer();

	// copy constructor
	CXfer( const CXfer& rhs );
	// assignment operator
	virtual CXfer& operator=( const CXfer& rhs );

	static const string Begin;	// "Xfer"
	static const string End;	// "End Xfer"

	virtual bool ToBinary( ofstream& ofs );

	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize() { return (unsigned int)(m_FieldValues.size()*4); }
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

#if TOOLS_GUI == 1
//	virtual bool LoadState( ifstream& ifs );
//	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	enum Xfer { DATA_ID, LOCATION, XFER_MAX };

};

typedef list<CXfer*>           t_XferList;
typedef list<CXfer*>::iterator t_XferListIter;


class CTBRXferSet : public CErdBase
{
public:
	CTBRXferSet();
	virtual ~CTBRXferSet();

	// copy constructor
	CTBRXferSet( const CTBRXferSet& rhs );
	// assignment operator
	virtual CTBRXferSet& operator=( const CTBRXferSet& rhs );

	static const string	Begin;	// "TBR Xfer Set"
	static const string	End;	// "End TBR Xfer Set"

	virtual void Reset();
		
	virtual bool Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine );

	virtual bool ToBinary( ofstream& ofs );

	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize();
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

#if TOOLS_GUI == 1
	virtual bool LoadState( ifstream& ifs );
	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	t_XferList& XfersList() { return Xfers; }

	enum TBRXferSet { XFER_LOC, NUM_DATA_PAIRS, TBR_XFER_SET_MAX };

private:
	t_XferList		Xfers;
};

typedef list<CTBRXferSet*>           t_TBRXferSetList;
typedef list<CTBRXferSet*>::iterator t_TBRXferSetListIter;

