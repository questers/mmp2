/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "ErdBase.h"

#include <list>
using namespace std;

class CResumeDdr : public CErdBase
{
public:
	CResumeDdr();
	virtual ~CResumeDdr();

	// copy constructor
	CResumeDdr( const CResumeDdr& rhs );
	// assignment operator
	virtual CResumeDdr& operator=( const CResumeDdr& rhs );

	static const string Begin;	// "Resume DDR"
	static const string End;	// "End Resume DDR"

	virtual bool ToBinary( ofstream& ofs );

	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize() { return (unsigned int)(8+(m_FieldValues.size()*4)); }
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

#if TOOLS_GUI == 1
	virtual bool LoadState( ifstream& ifs );
	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	enum ResumeDdr { RESUME_DDR_ADDR, RESUME_DDR_PARAM, RESUME_DDR_FLAG, RESUME_DDR_MAX };
};

typedef list<CResumeDdr*>           t_ResumeDdrList;
typedef list<CResumeDdr*>::iterator t_ResumeDdrListIter;

