/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "TimLib.h"

#include <string>
#include <list>
#include <iterator>
#include <fstream>
#include <iostream>
#include <strstream>
#include <sstream>
using namespace std;

class CPartitionData : public CTimLib
{
public:
	CPartitionData(void);
	virtual ~CPartitionData(void);

	// copy constructor
	CPartitionData( const CPartitionData& rhs );
	// assignment operator
	CPartitionData& operator=( const CPartitionData& rhs );

	void PartitionId( const unsigned int Id ) { m_PartitionId = Id; m_bChanged = true; }
	unsigned int PartitionId() { return m_PartitionId; }

	void PartitionAttributes( const unsigned int Attributes ) { m_PartitionAttributes = Attributes; m_bChanged = true; }
	unsigned int PartitionAttributes() { return m_PartitionAttributes; }

	void PartitionUsage( const string& sUsage ) { m_sPartitionUsage = sUsage; m_bChanged = true; }
	string& PartitionUsage() { return m_sPartitionUsage; }

	void PartitionType( const string& sType ) { m_sPartitionType = sType; m_bChanged = true; }
	string& PartitionType() { return m_sPartitionType; }

	void PartitionStartAddress( const unsigned __int64 Address ) { m_PartitionStartAddress = Address; m_bChanged = true; }
	unsigned __int64 PartitionStartAddress() { return m_PartitionStartAddress; }

	void PartitionEndAddress( const unsigned __int64 Address ) { m_PartitionEndAddress = Address; m_bChanged = true; }
	unsigned __int64 PartitionEndAddress() { return m_PartitionEndAddress; }

	void RuntimeBBTType( const string& sType ) { m_sRuntimeBBTType = sType; m_bChanged = true; }
	string& RuntimeBBTType() { return m_sRuntimeBBTType; }

	void RuntimeBBTLocation( const unsigned int Location ) { m_RuntimeBBTLocation = Location; m_bChanged = true; }
	unsigned int RuntimeBBTLocation() { return m_RuntimeBBTLocation; }

	void BackupRuntimeBBTLocation( const unsigned int Location ) { m_BackupRuntimeBBTLocation = Location; m_bChanged = true; }
	unsigned int BackupRuntimeBBTLocation() { return m_BackupRuntimeBBTLocation; }

	void ReservedPoolStartAddress( const unsigned __int64 Address ) { m_ReservedPoolStartAddress = Address; m_bChanged = true; }
	unsigned __int64 ReservedPoolStartAddress() { return m_ReservedPoolStartAddress; }

	void ReservedPoolSize( const unsigned int Size ) { m_ReservedPoolSize = Size; m_bChanged = true; }
	unsigned int ReservedPoolSize() { return m_ReservedPoolSize; }

	void ReservedPoolAlgorithm( const string& Algorithm ) { m_sReservedPoolAlgorithm = Algorithm; m_bChanged = true; }
	string& ReservedPoolAlgorithm() { return m_sReservedPoolAlgorithm; }

#if TOOLS_GUI == 1
	bool SaveState( ofstream& ofs );
	bool LoadState( ifstream& ifs );
#endif

	bool IsChanged(){ return m_bChanged; }
	void Changed( bool bSet ){ m_bChanged = bSet; }

private:
	unsigned int	m_PartitionId;
	unsigned int	m_PartitionAttributes;
	string			m_sPartitionUsage;
	string			m_sPartitionType;
	unsigned __int64 m_PartitionStartAddress;
	unsigned __int64 m_PartitionEndAddress;
	string			m_sRuntimeBBTType;
	unsigned int	m_RuntimeBBTLocation;
	unsigned int	m_BackupRuntimeBBTLocation;
	unsigned __int64 m_ReservedPoolStartAddress;
	unsigned int	m_ReservedPoolSize;
	string			m_sReservedPoolAlgorithm;

	bool m_bChanged;
};

typedef list<CPartitionData*>           t_PartitionList;
typedef list<CPartitionData*>::iterator t_PartitionListIter;
