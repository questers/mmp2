/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "Key.h"

CKey::CKey(void)
: CTimLib()
{
    m_uiPublicKeySize = 0;
    m_uiKeySize = 0;
    m_bChanged = false;
    m_sEncryptAlgorithmId = "";
    m_sHashAlgorithmId = "";
}

CKey::~CKey(void)
{
    DiscardAll();
}

CKey::CKey( const CKey& rhs )
: CTimLib( rhs )
{
    // copy constructor
    CKey& nc_rhs = const_cast<CKey&>(rhs);

#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sKeyId )) != 0 )
        pLine->AddRef( &m_sKeyId );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sKeyTag )) != 0 )
        pLine->AddRef( &m_sKeyTag );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sHashAlgorithmId )) != 0 )
        pLine->AddRef( &m_sHashAlgorithmId );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sEncryptAlgorithmId )) != 0 )
        pLine->AddRef( &m_sEncryptAlgorithmId );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_uiPublicKeySize )) != 0 )
        pLine->AddRef( &m_uiPublicKeySize );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_uiKeySize )) != 0 )
        pLine->AddRef( &m_uiKeySize );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_PublicKeyExponentList )) != 0 )
        pLine->AddRef( &m_PublicKeyExponentList );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_RsaSystemModulusList )) != 0 )
        pLine->AddRef( &m_RsaSystemModulusList );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_ECDSAPublicKeyCompXList )) != 0 )
        pLine->AddRef( &m_ECDSAPublicKeyCompXList );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_ECDSAPublicKeyCompYList )) != 0 )
        pLine->AddRef( &m_ECDSAPublicKeyCompYList );
#endif

    m_sKeyId = rhs.m_sKeyId;
    m_sKeyTag = rhs.m_sKeyTag;
    m_sHashAlgorithmId = rhs.m_sHashAlgorithmId;
    m_sEncryptAlgorithmId = rhs.m_sEncryptAlgorithmId;
    m_uiPublicKeySize = rhs.m_uiPublicKeySize;
    m_uiKeySize = rhs.m_uiKeySize;

    m_bChanged = rhs.m_bChanged;

    // need to do a deep copy of lists to avoid dangling references
    t_stringListIter iterExponent = nc_rhs.m_PublicKeyExponentList.begin();
    while( iterExponent != nc_rhs.m_PublicKeyExponentList.end() )
    {
        string* pData = new string( *(*iterExponent ));
        m_PublicKeyExponentList.push_back( pData );
        iterExponent++;
    }

    t_stringListIter iterModulus = nc_rhs.m_RsaSystemModulusList.begin();
    while( iterModulus != nc_rhs.m_RsaSystemModulusList.end() )
    {
        string* pData = new string( *(*iterModulus ));
        m_RsaSystemModulusList.push_back( pData ) ;
        iterModulus++;
    }

    t_stringListIter iterCompX = nc_rhs.m_ECDSAPublicKeyCompXList.begin();
    while( iterCompX != nc_rhs.m_ECDSAPublicKeyCompXList.end() )
    {
        string* pData = new string( *(*iterCompX ));
        m_ECDSAPublicKeyCompXList.push_back( pData ) ;
        iterCompX++;
    }

    t_stringListIter iterCompY = nc_rhs.m_ECDSAPublicKeyCompYList.begin();
    while( iterCompY != nc_rhs.m_ECDSAPublicKeyCompYList.end() )
    {
        string* pData = new string( *(*iterCompY ));
        m_ECDSAPublicKeyCompYList.push_back( pData ) ;
        iterCompY++;
    }
}

CKey& CKey::operator=( const CKey& rhs ) 
{
    // assignment operator
    if ( &rhs != this )
    {
        CTimLib::operator=(rhs);

        CKey& nc_rhs = const_cast<CKey&>(rhs);
        DiscardAll();

#if TOOLS_GUI == 1
        CTimDescriptorLine* pLine = 0;

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sKeyId )) != 0 )
            pLine->AddRef( &m_sKeyId );

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sKeyTag )) != 0 )
            pLine->AddRef( &m_sKeyTag );

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sHashAlgorithmId )) != 0 )
            pLine->AddRef( &m_sHashAlgorithmId );

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sEncryptAlgorithmId )) != 0 )
            pLine->AddRef( &m_sEncryptAlgorithmId );

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_uiPublicKeySize )) != 0 )
            pLine->AddRef( &m_uiPublicKeySize );

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_uiKeySize )) != 0 )
            pLine->AddRef( &m_uiKeySize );

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_PublicKeyExponentList )) != 0 )
            pLine->AddRef( &m_PublicKeyExponentList );

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_RsaSystemModulusList )) != 0 )
            pLine->AddRef( &m_RsaSystemModulusList );

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_ECDSAPublicKeyCompXList )) != 0 )
            pLine->AddRef( &m_ECDSAPublicKeyCompXList );

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_ECDSAPublicKeyCompYList )) != 0 )
            pLine->AddRef( &m_ECDSAPublicKeyCompYList );
#endif

        m_sKeyId = rhs.m_sKeyId;
        m_sKeyTag = rhs.m_sKeyTag;
        m_sHashAlgorithmId = rhs.m_sHashAlgorithmId;
        m_sEncryptAlgorithmId = rhs.m_sEncryptAlgorithmId;
        m_uiPublicKeySize = rhs.m_uiPublicKeySize;
        m_uiKeySize = rhs.m_uiKeySize;

        m_bChanged = rhs.m_bChanged;

        // need to do a deep copy of lists to avoid dangling references
        t_stringListIter iterExponent = nc_rhs.m_PublicKeyExponentList.begin();
        while( iterExponent != nc_rhs.m_PublicKeyExponentList.end() )
        {
            string* pData = new string( *(*iterExponent ));
            m_PublicKeyExponentList.push_back( pData );
            iterExponent++;
        }

        t_stringListIter iterModulus = nc_rhs.m_RsaSystemModulusList.begin();
        while( iterModulus != nc_rhs.m_RsaSystemModulusList.end() )
        {
            string* pData = new string( *(*iterModulus ));
            m_RsaSystemModulusList.push_back( pData ) ;
            iterModulus++;
        }

        t_stringListIter iterCompX = nc_rhs.m_ECDSAPublicKeyCompXList.begin();
        while( iterCompX != nc_rhs.m_ECDSAPublicKeyCompXList.end() )
        {
            string* pData = new string( *(*iterCompX ));
            m_ECDSAPublicKeyCompXList.push_back( pData ) ;
            iterCompX++;
        }

        t_stringListIter iterCompY = nc_rhs.m_ECDSAPublicKeyCompYList.begin();
        while( iterCompY != nc_rhs.m_ECDSAPublicKeyCompYList.end() )
        {
            string* pData = new string( *(*iterCompY ));
            m_ECDSAPublicKeyCompYList.push_back( pData ) ;
            iterCompY++;
        }
    }
    return *this;
}

void CKey::DiscardAll()
{
#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_sKeyId )) != 0 )
        pLine->RemoveRef( &m_sKeyId );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_sKeyTag )) != 0 )
        pLine->RemoveRef( &m_sKeyTag );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_sHashAlgorithmId )) != 0 )
        pLine->RemoveRef( &m_sHashAlgorithmId );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_sEncryptAlgorithmId )) != 0 )
        pLine->RemoveRef( &m_sEncryptAlgorithmId );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_uiPublicKeySize )) != 0 )
        pLine->RemoveRef( &m_uiPublicKeySize );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_uiKeySize )) != 0 )
        pLine->RemoveRef( &m_uiKeySize );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_PublicKeyExponentList )) != 0 )
        pLine->RemoveRef( &m_PublicKeyExponentList );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_RsaSystemModulusList )) != 0 )
        pLine->RemoveRef( &m_RsaSystemModulusList );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_ECDSAPublicKeyCompXList )) != 0 )
        pLine->AddRef( &m_ECDSAPublicKeyCompXList );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_ECDSAPublicKeyCompYList )) != 0 )
        pLine->AddRef( &m_ECDSAPublicKeyCompYList );
#endif

    // delete all exponent strings
    t_stringListIter iterExponent = m_PublicKeyExponentList.begin();
    while( iterExponent != m_PublicKeyExponentList.end() )
    {
        delete *iterExponent;
        iterExponent++;
    }
    m_PublicKeyExponentList.clear();

    // delete all modulus strings
    t_stringListIter iterModulus = m_RsaSystemModulusList.begin();
    while( iterModulus != m_RsaSystemModulusList.end() )
    {
        delete *iterModulus;
        iterModulus++;
    }
    m_RsaSystemModulusList.clear();

    t_stringListIter iterCompX = m_ECDSAPublicKeyCompXList.begin();
    while( iterCompX != m_ECDSAPublicKeyCompXList.end() )
    {
        delete *iterCompX;
        iterCompX++;
    }
    m_ECDSAPublicKeyCompXList.clear();

    t_stringListIter iterCompY = m_ECDSAPublicKeyCompYList.begin();
    while( iterCompY != m_ECDSAPublicKeyCompYList.end() )
    {
        delete *iterCompY;
        iterCompY++;
    }
    m_ECDSAPublicKeyCompYList.clear();
}

void CKey::KeyTag( string& sKeyTag )
{ 
    m_sKeyTag = ToUpper(sKeyTag, true); 

    if ( m_sKeyTag == HexFormattedAscii(JTAGIDENTIFIER) )// JTAG
        m_sKeyId = "JTAG";	
    else if ( m_sKeyTag == HexFormattedAscii(PATCHIDENTIFIER) )// PATC
        m_sKeyId = "PATC";
    else if ( m_sKeyTag == HexFormattedAscii(TCAIDENTIFIER) )// TCAK
        m_sKeyId = "TCAK";
    else
    {
        m_sKeyTag = sKeyTag;
        m_sKeyId = sKeyTag;// custom
    }
    m_bChanged = true;
}

void CKey::HashAlgorithmId( HASHALGORITHMID_T Id )
{
    switch( Id )
    {
        case SHA256:
            m_sHashAlgorithmId = "SHA-256";
            break;

        case SHA512:
            m_sHashAlgorithmId = "SHA-512";
            break;

        case SHA160:
        default:
            m_sHashAlgorithmId = "SHA-160";
            break;
    }
}

void CKey::EncryptAlgorithmId( ENCRYPTALGORITHMID_T Id )
{
    switch( Id )
    {
        case PKCS1_v1_5_Caddo:
            m_sEncryptAlgorithmId = "PKCS1_v1_5";
            break;

        case PKCS1_v2_1_Caddo:
            m_sEncryptAlgorithmId = "PKCS1_v2_1";
            break;

        case PKCS1_v1_5_Ippcp:
            m_sEncryptAlgorithmId = "PKCS1_v1_5 (Ippcp)";
            break;

        case PKCS1_v2_1_Ippcp:
            m_sEncryptAlgorithmId = "PKCS1_v2_1 (Ippcp)";
            break;

        case ECDSA_256:
            m_sEncryptAlgorithmId = "ECDSA_256";
            break;

        case ECDSA_521:
            m_sEncryptAlgorithmId = "ECDSA_521";
            break;

        case Marvell_DS:
        default:
            m_sEncryptAlgorithmId = "Marvell";
            break;
    }
}

void CKey::ResetExponent()
{
    t_stringListIter iterExponent = m_PublicKeyExponentList.begin();
    while( iterExponent != m_PublicKeyExponentList.end() )
    {
        *(*iterExponent) = "0x00000000";
        iterExponent++;
    }

    m_bChanged = true;
}

void CKey::ResetModulus()
{
    t_stringListIter iterModulus = m_RsaSystemModulusList.begin();
    while( iterModulus != m_RsaSystemModulusList.end() )
    {
        *(*iterModulus) = "0x00000000";
        iterModulus++;
    }

    m_bChanged = true;
}

void CKey::ResetCompX()
{
    t_stringListIter iterCompX = m_ECDSAPublicKeyCompXList.begin();
    while( iterCompX != m_ECDSAPublicKeyCompXList.end() )
    {
        *(*iterCompX) = "0x00000000";
        iterCompX++;
    }

    m_bChanged = true;
}

void CKey::ResetCompY()
{
    t_stringListIter iterCompY = m_ECDSAPublicKeyCompYList.begin();
    while( iterCompY != m_ECDSAPublicKeyCompYList.end() )
    {
        *(*iterCompY) = "0x00000000";
        iterCompY++;
    }

    m_bChanged = true;
}

#if TOOLS_GUI == 1
bool CKey::SaveState( stringstream& ss )
{
    unsigned int temp = 0;

    SaveFieldStatePtr( ss, this );

    SaveFieldState( ss, m_sKeyId );
    SaveFieldState( ss, m_sKeyTag );
    SaveFieldState( ss, m_sHashAlgorithmId );
    SaveFieldState( ss, m_sEncryptAlgorithmId );
    SaveFieldState( ss, m_uiPublicKeySize );
    SaveFieldState( ss, m_uiKeySize );

    temp = (unsigned int)m_PublicKeyExponentList.size();
    SaveFieldState( ss, temp );
    t_stringListIter iterExponent = m_PublicKeyExponentList.begin();
    while( iterExponent != m_PublicKeyExponentList.end() )
        SaveFieldState( ss, *(*iterExponent++) );

    temp = (unsigned int)m_RsaSystemModulusList.size();
    SaveFieldState( ss, temp );
    t_stringListIter iterModulus = m_RsaSystemModulusList.begin();
    while( iterModulus != m_RsaSystemModulusList.end() )
        SaveFieldState( ss, *(*iterModulus++) );

    temp = (unsigned int)m_ECDSAPublicKeyCompXList.size();
    SaveFieldState( ss, temp );
    t_stringListIter iterCompX = m_ECDSAPublicKeyCompXList.begin();
    while( iterCompX != m_ECDSAPublicKeyCompXList.end() )
        SaveFieldState( ss, *(*iterCompX++) );

    temp = (unsigned int)m_ECDSAPublicKeyCompYList.size();
    SaveFieldState( ss, temp );
    t_stringListIter iterCompY = m_ECDSAPublicKeyCompYList.begin();
    while( iterCompY != m_ECDSAPublicKeyCompYList.end() )
        SaveFieldState( ss, *(*iterCompY++) );

    return true; // SUCCESS
}

bool CKey::LoadState( ifstream& ifs )
{
    DiscardAll();

    string sbuf;
    
    if ( theApp.ProjectVersion >= 0x03021400 )
        LoadFieldStatePtr( ifs, this ); 

    LoadFieldState( ifs, m_sKeyId );  
    LoadFieldState( ifs, m_sKeyTag );  
    LoadFieldState( ifs, m_sHashAlgorithmId );  

    if ( theApp.ProjectVersion >= 0x03021602 )
        LoadFieldState( ifs, m_sEncryptAlgorithmId );  

    if ( theApp.ProjectVersion > 0x03021301 )
    {
        LoadFieldState( ifs, m_uiPublicKeySize );
        LoadFieldState( ifs, m_uiKeySize );
    }

    LoadFieldState( ifs, sbuf );
    int iPublicKeyExponentListSize = Translate(sbuf);
    while ( iPublicKeyExponentListSize > 0 )
    {
        string* psData = new string;
        LoadFieldState( ifs, *psData );
        m_PublicKeyExponentList.push_back( psData );
        iPublicKeyExponentListSize--;
    }

    LoadFieldState( ifs, sbuf );
    int iRsaSystemModulusListSize = Translate(sbuf);
    while ( iRsaSystemModulusListSize > 0 )
    {
        string* psData = new string;
        LoadFieldState( ifs, *psData );
        m_RsaSystemModulusList.push_back( psData );
        iRsaSystemModulusListSize--;
    }

    if ( theApp.ProjectVersion >= 0x03021600 )
    {
        LoadFieldState( ifs, sbuf );
        int iECDSAPublicKeyCompX = Translate(sbuf);
        while ( iECDSAPublicKeyCompX > 0 )
        {
            string* psData = new string;
            LoadFieldState( ifs, *psData );
            m_ECDSAPublicKeyCompXList.push_back( psData );
            iECDSAPublicKeyCompX--;
        }

        LoadFieldState( ifs, sbuf );
        int iECDSAPublicKeyCompY = Translate(sbuf);
        while ( iECDSAPublicKeyCompY > 0 )
        {
            string* psData = new string;
            LoadFieldState( ifs, *psData );
            m_ECDSAPublicKeyCompYList.push_back( psData );
            iECDSAPublicKeyCompY--;
        }
    }

    return ifs.good() && !ifs.fail(); // success
}
#endif

string& CKey::PublicKeyExponent()
{
    // returns a complete list of values, including all trailing 0x00000000
    m_sPublicKeyExponent = "";
    int nPerLine = 0;

    t_stringListIter iterExponent = m_PublicKeyExponentList.begin();
    while( iterExponent != m_PublicKeyExponentList.end() )
    {
        nPerLine++;
        m_sPublicKeyExponent += *(*iterExponent);

        if ( (nPerLine != 0) && (nPerLine %4 == 0) )
            m_sPublicKeyExponent += "\n";  // 4 is # of words per line
        else
            m_sPublicKeyExponent += " ";

        iterExponent++;
    }

    return m_sPublicKeyExponent;
}

string& CKey::PublicKeyExponentPacked()
{
    // returns a truncated list of values, excluding all trailing 0x00000000
    m_sPublicKeyExponent = "";
    int nPerLine = 0;

    unsigned int uiActiveSize = ActiveSizeOfList( m_PublicKeyExponentList );

    t_stringListIter iterExponent = m_PublicKeyExponentList.begin();
    while( iterExponent != m_PublicKeyExponentList.end() && uiActiveSize > 0 )
    {
        uiActiveSize -= 32;
        m_sPublicKeyExponent += *(*iterExponent);
        nPerLine++;
        if ( (nPerLine != 0) && (nPerLine % 4 == 0) || uiActiveSize == 0 ) // 4 is # of words per line
            m_sPublicKeyExponent += "\n";
        else
            m_sPublicKeyExponent += " ";

        iterExponent++;
    }

    return m_sPublicKeyExponent;
}

string& CKey::RsaSystemModulus()
{
    // returns a complete list of values, including all trailing 0x00000000
    m_sRsaSystemModulus = "";
    int nPerLine = 0;

    t_stringListIter iterModulus = m_RsaSystemModulusList.begin();
    while( iterModulus != m_RsaSystemModulusList.end() )
    {
        m_sRsaSystemModulus += *(*iterModulus);
        nPerLine++;
        if ( (nPerLine != 0) && (nPerLine %4 == 0) ) // 4 is # of words per line
            m_sRsaSystemModulus += "\n";
        else
            m_sRsaSystemModulus += " ";  

        iterModulus++;
    }
    
    return m_sRsaSystemModulus;
}

string& CKey::RsaSystemModulusPacked()
{
    // returns a truncated list of values, excluding all trailing 0x00000000
    m_sRsaSystemModulus = "";
    int nPerLine = 0;

    unsigned int uiActiveSize = ActiveSizeOfList( m_RsaSystemModulusList );

    t_stringListIter iterModulus = m_RsaSystemModulusList.begin();
    while( iterModulus != m_RsaSystemModulusList.end() && uiActiveSize > 0 )
    {
        uiActiveSize -= 32;
        m_sRsaSystemModulus += *(*iterModulus);
        nPerLine++;
        if ( (nPerLine != 0) && (nPerLine % 4 == 0) || uiActiveSize == 0 ) // 4 is # of words per line
            m_sRsaSystemModulus += "\n";
        else
            m_sRsaSystemModulus += " ";

        iterModulus++;
    }

    return m_sRsaSystemModulus;
}

string& CKey::ECDSAPublicKeyCompX()
{
    // returns a complete list of values, including all trailing 0x00000000
    m_sECDSAPublicKeyCompX = "";
    int nPerLine = 0;

    t_stringListIter iterCompX = m_ECDSAPublicKeyCompXList.begin();
    while( iterCompX != m_ECDSAPublicKeyCompXList.end() )
    {
        nPerLine++;
        m_sECDSAPublicKeyCompX += *(*iterCompX);

        if ( (nPerLine != 0) && (nPerLine %4 == 0) ) // 4 is # of words per line
            m_sECDSAPublicKeyCompX += "\n";
        else
            m_sECDSAPublicKeyCompX += "\n";

        iterCompX++;
    }

    return m_sECDSAPublicKeyCompX;
}

string& CKey::ECDSAPublicKeyCompXPacked()
{
    // returns a truncated list of values, excluding all trailing 0x00000000
    m_sECDSAPublicKeyCompX = "";
    int nPerLine = 0;

    unsigned int uiActiveSize = ActiveSizeOfList( m_ECDSAPublicKeyCompXList );

    t_stringListIter iterCompX = m_ECDSAPublicKeyCompXList.begin();
    while( iterCompX != m_ECDSAPublicKeyCompXList.end() )
    {
        uiActiveSize -= 32;

        m_sECDSAPublicKeyCompX += *(*iterCompX);
        nPerLine++;
        if ( (nPerLine != 0) && (nPerLine %4 == 0) || uiActiveSize == 0 ) // 4 is # of words per line
            m_sECDSAPublicKeyCompX += "\n";
        else
            m_sECDSAPublicKeyCompX += " ";

        iterCompX++;
    }

    return m_sECDSAPublicKeyCompX;
}

string& CKey::ECDSAPublicKeyCompY()
{
    // returns a complete list of values, including all trailing 0x00000000
    m_sECDSAPublicKeyCompY = "";
    int nPerLine = 0;

    t_stringListIter iterCompY = m_ECDSAPublicKeyCompYList.begin();
    while( iterCompY != m_ECDSAPublicKeyCompYList.end() )
    {
        m_sECDSAPublicKeyCompY += *(*iterCompY);
        nPerLine++;
        if ( (nPerLine != 0) && (nPerLine %4 == 0) ) // 4 is # of words per line
            m_sECDSAPublicKeyCompY += "\n";
        else
            m_sECDSAPublicKeyCompY += " ";

        iterCompY++;
    }

    return m_sECDSAPublicKeyCompY;
}

string& CKey::ECDSAPublicKeyCompYPacked()
{
    // returns a truncated list of values, excluding all trailing 0x00000000
    m_sECDSAPublicKeyCompY = "";
    int nPerLine = 0;

    unsigned int uiActiveSize = ActiveSizeOfList( m_ECDSAPublicKeyCompYList );

    t_stringListIter iterCompY = m_ECDSAPublicKeyCompYList.begin();
    while( iterCompY != m_ECDSAPublicKeyCompYList.end() )
    {
        uiActiveSize -= 32;

        m_sECDSAPublicKeyCompY += *(*iterCompY);
        nPerLine++;
        if ( (nPerLine != 0) && (nPerLine %4 == 0) || uiActiveSize == 0 ) // 4 is # of words per line
            m_sECDSAPublicKeyCompY += "\n";
        else
            m_sECDSAPublicKeyCompY += " ";

        iterCompY++;
    }

    return m_sECDSAPublicKeyCompX;
}

void CKey::PublicKeySize( unsigned int uiPublicKeySize )
{
    // changing the size results in expansion or reduction in exponent and modulus
//	if ( uiPublicKeySize > PublicKeySize() )
    if ( m_PublicKeyExponentList.size() < ((uiPublicKeySize+31)/32) )
    {
        // expand the lists
        while ( m_PublicKeyExponentList.size() < ((uiPublicKeySize+31)/32) )
            m_PublicKeyExponentList.push_back( new string("0x00000000") );

        while ( m_ECDSAPublicKeyCompXList.size() < ((uiPublicKeySize+31)/32) )
            m_ECDSAPublicKeyCompXList.push_back( new string("0x00000000") );

        while ( m_ECDSAPublicKeyCompYList.size() < ((uiPublicKeySize+31)/32) )
            m_ECDSAPublicKeyCompYList.push_back( new string("0x00000000") );

        m_bChanged = true;
    }
//	else if ( uiPublicKeySize < PublicKeySize() )
    else if ( m_PublicKeyExponentList.size() > ((uiPublicKeySize+31)/32) )
    {
        // shrink the lists
        while ( m_PublicKeyExponentList.size() > ((uiPublicKeySize+31)/32) )
        {
            delete m_PublicKeyExponentList.back();
            m_PublicKeyExponentList.pop_back();
        }

        while ( m_ECDSAPublicKeyCompXList.size() > ((uiPublicKeySize+31)/32) )
        {
            delete m_ECDSAPublicKeyCompXList.back();
            m_ECDSAPublicKeyCompXList.pop_back();
        }

        while ( m_ECDSAPublicKeyCompYList.size() > ((uiPublicKeySize+31)/32) )
        {
            delete m_ECDSAPublicKeyCompYList.back();
            m_ECDSAPublicKeyCompYList.pop_back();
        }

        m_bChanged = true;
    }

    m_uiPublicKeySize = uiPublicKeySize;
}

void CKey::KeySize( unsigned int uiKeySize )
{
    // changing the size results in expansion or reduction in exponent and modulus
    if ( m_RsaSystemModulusList.size() < ((uiKeySize+31)/32) )
    {
        while ( m_RsaSystemModulusList.size() < ((uiKeySize+31)/32) )
            m_RsaSystemModulusList.push_back( new string("0x00000000") );

        m_bChanged = true;
    }
    else if ( m_RsaSystemModulusList.size() > ((uiKeySize+31)/32) )
    {
        // shrink the lists
        while ( m_RsaSystemModulusList.size() > ((uiKeySize+31)/32) )
        {
            delete m_RsaSystemModulusList.back();
            m_RsaSystemModulusList.pop_back();
        }
        m_bChanged = true;
    }

    m_uiKeySize = uiKeySize;
}

unsigned int CKey::ActiveSizeOfList( t_stringList& List )
{
    // returns a size excluding all trailing 0x00000000 values

    // get bit size of entire list
    unsigned int uiSize = ((int)List.size())*32;
    string sItem;

    if ( uiSize > 0 )
    {
        t_stringListIter iter = List.end();
        while( --iter != List.begin() )
        {
            sItem = *(*iter);
            if ( ToUpper(sItem, true) == "0x00000000" ) 
                uiSize -= 32;
            else 
                break;
        }
    }
    return uiSize;
}

bool CKey::IsChanged()
{
    return m_bChanged;
}
