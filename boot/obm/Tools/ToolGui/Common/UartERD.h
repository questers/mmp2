/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "ErdBase.h"

#include <list>
using namespace std;

class CUart : public CErdBase
{
public:
	CUart();
	virtual ~CUart();

	// copy constructor
	CUart( const CUart& rhs );
	// assignment operator
	virtual CUart& operator=( const CUart& rhs );

	static const string Begin;	// "Uart"
	static const string End;	// "End Uart"
	static CUart* Migrate( CReservedPackageData* pRPD );

	virtual bool ToBinary( ofstream& ofs );

	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize() { return (unsigned int)(8+(m_FieldValues.size()*4)); }
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

#if TOOLS_GUI == 1
//	virtual bool LoadState( ifstream& ifs );
//	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	enum Uart { PORT, ENABLED, UART_MAX };

};

typedef list<CUart*>           t_UartList;
typedef list<CUart*>::iterator t_UartListIter;

