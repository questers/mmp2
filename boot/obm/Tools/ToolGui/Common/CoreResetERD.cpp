/******************************************************************************
 *
 *  (C)Copyright 2005 - 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#if TOOLS_GUI == 1
#include "stdafx.h"
#include "MarvellBootUtility.h"
#endif

#include "CoreResetERD.h"
#include "TimDescriptorParser.h"

#if LINUX
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif


const string CCoreReset::Begin("Core Reset");
const string CCoreReset::End("End Core Reset");

CCoreReset::CCoreReset()
:	CErdBase( CORE_ID_ERD, CORE_RESET_MAX )
{
	*m_FieldNames[COREID_FIELD] = "CORE ID";
    *m_FieldNames[ADDRESS_MAPPING_FIELD] = "ADDRESS_MAPPING";
}

CCoreReset::~CCoreReset(void)
{
}


CCoreReset::CCoreReset( const CCoreReset& rhs )
: CErdBase( rhs )
{
	// copy constructor

	// need to do a deep copy of lists to avoid dangling references
	CCoreReset& nc_rhs = const_cast<CCoreReset&>(rhs);

#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;
#endif

}

CCoreReset& CCoreReset::operator=( const CCoreReset& rhs ) 
{
	// assignment operator
	if ( &rhs != this )
	{
		CErdBase::operator=( rhs );

		// need to do a deep copy of lists to avoid dangling references
		CCoreReset& nc_rhs = const_cast<CCoreReset&>(rhs);

#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;
#endif

	}
	return *this;
}

bool CCoreReset::ToBinary( ofstream& ofs )
{
	// validate size
	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
		return false;

	ofs << COREID;
	ofs << PackageSize();
	ofs << Translate(*m_FieldValues[COREID_FIELD]);
	ofs << Translate(*m_FieldValues[ADDRESS_MAPPING_FIELD]);

	return ofs.good();
}
		
#if TOOLS_GUI == 1
bool CCoreReset::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;
	
	if ( !TimDescriptor.AttachCommentedObject( ss, m_FieldNames[COREID_FIELD], m_FieldNames[COREID_FIELD]->c_str(), *m_FieldValues[COREID_FIELD] ) )
		ss << *m_FieldNames[COREID_FIELD] << ": " << *m_FieldValues[COREID_FIELD] << endl;

	if ( !TimDescriptor.AttachCommentedObject( ss, m_FieldNames[ADDRESS_MAPPING_FIELD], m_FieldNames[ADDRESS_MAPPING_FIELD]->c_str(), *m_FieldValues[ADDRESS_MAPPING_FIELD] ) )
		ss << *m_FieldNames[ADDRESS_MAPPING_FIELD] << ": " << *m_FieldValues[ADDRESS_MAPPING_FIELD] << endl;

    if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;

	return true;
}
#endif


unsigned int CCoreReset::PackageSize() 
{
	// 8(WRAH) + 4(CoreID) + 4{AddressMapping)
	unsigned int iSize = 8 + 4 + 4;
	return iSize;
}

int CCoreReset::AddPkgStrings( CReservedPackageData* pRPD )
{
	pRPD->PackageIdTag( HexFormattedAscii(COREID) );
	pRPD->AddData( new string( *m_FieldValues[ COREID_FIELD ] ), new string( "Core ID" ));
	pRPD->AddData( new string( *m_FieldValues[ ADDRESS_MAPPING_FIELD ] ), new string( "Address Mapping" ));

	return PackageSize();
}

