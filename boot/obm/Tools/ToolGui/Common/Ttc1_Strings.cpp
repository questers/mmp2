/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma message("Using PXA91x, PXA92x, PXA93x, PXA94x, PXA95x (TTC1) Extended Reserved Data strings")
#if TOOLS_GUI == 1
#include "StdAfx.h"
#endif

#pragma warning ( disable : 4200 ) // Disable warning messages
 
#include "Ttc1_Strings.h"
#include <Ttc1_Processor_Enums.h>	// need to change to get bootloader version

#define STRINGIZE(x) #x
#define FIELDSTRING( array, field_enum ) array[field_enum] = new string( STRINGIZE(field_enum) );

TTC1Strings::TTC1Strings(CExtendedReservedData& Ext)
: m_Ext(Ext)
{
	// make sure deallocated before allocate
	Depopulate(m_Ext.g_ClockEnableFields);
	Depopulate(m_Ext.g_FrequencyFields);
	Depopulate(m_Ext.g_DDRCustomFields);
	Depopulate(m_Ext.g_VoltagesFields);

	PopulateClockEnableFields(m_Ext.g_ClockEnableFields);
	PopulateFrequencyFields(m_Ext.g_FrequencyFields);
	PopulateDDRCustomFields(m_Ext.g_DDRCustomFields);
	PopulateVoltagesFields(m_Ext.g_VoltagesFields);
}

TTC1Strings::~TTC1Strings()
{
}

// to deallocate the g_ClockEnableFields strings call this 
void TTC1Strings::Depopulate( t_stringVector& Fields )
{
	for ( unsigned int i = 0; i < Fields.size(); i++ )
		delete Fields[i];
	Fields.clear();
}

// MUST call PopulateClockEnableFields to fill g_ClockEnableFields array with enum
// field strings before trying to access g_ClockEnableFields
void TTC1Strings::PopulateClockEnableFields(t_stringVector& g_ClockEnableFields)
{
	// stringize the enums into field name strings
	// allocate the full array beased on the enum max
	g_ClockEnableFields.resize(TTC_CLOCK_ID_E_MAX);
	
	// populate strings for each field
	FIELDSTRING( g_ClockEnableFields, TTC_CLOCK_ID_MCU );
	FIELDSTRING( g_ClockEnableFields, TTC_CLOCK_ID_I2C );
}
	
// MUST call PopulateFrequencyFields to fill g_FrequencyFields array with enum
// field strings before trying to access g_FrequencyFields
void TTC1Strings::PopulateFrequencyFields(t_stringVector& g_FrequencyFields)
{
	// stringize the enums into field name strings
	// allocate the full array beased on the enum max
	g_FrequencyFields.resize(TTC_FREQ_ID_E_MAX);
	
	// populate strings for each field
	FIELDSTRING( g_FrequencyFields, TTC_FREQ_ID_MCU );
}


// MUST call PopulateDDRCustomFields to fill g_DDRCustomFields array with enum
// field strings before trying to access g_DDRCustomFields
void TTC1Strings::PopulateDDRCustomFields(t_stringVector& g_DDRCustomFields)
{
	//
	// Morona custom fields
	//
	// stringize the enums into field name strings
	// allocate the full array beased on the enum max
	
	g_DDRCustomFields.resize(TTC_MCU_REGID_E_MAX);

	// populate strings for each field
	FIELDSTRING( g_DDRCustomFields, TTC_SDRREVREG_ID );		// revision
	FIELDSTRING( g_DDRCustomFields, TTC_SDRADCREG_ID );		// address decode
	FIELDSTRING( g_DDRCustomFields, TTC_SDRCFGREG0_ID );	// sdram config reg 0
	FIELDSTRING( g_DDRCustomFields, TTC_SDRCFGREG1_ID );	// sdram config reg 1
	FIELDSTRING( g_DDRCustomFields, TTC_SDRCFGREG2_ID );	// sdram config reg 2
	FIELDSTRING( g_DDRCustomFields, TTC_SDRTMGREG1_ID );	// sdram timing reg 1
	FIELDSTRING( g_DDRCustomFields, TTC_SDRTMGREG2_ID );	// sdram timing reg 2
	FIELDSTRING( g_DDRCustomFields, TTC_SDRTMGREG3_ID );	// sdram timing reg 3
	FIELDSTRING( g_DDRCustomFields, TTC_SDRTMGREG4_ID );	// sdram timing reg 4
	FIELDSTRING( g_DDRCustomFields, TTC_SDRTMGREG5_ID );	// sdram timing reg 5
	FIELDSTRING( g_DDRCustomFields, TTC_SDRCTLREG1_ID );	// sdram control reg 1
	FIELDSTRING( g_DDRCustomFields, TTC_SDRCTLREG2_ID );	// sdram control reg 2
	FIELDSTRING( g_DDRCustomFields, TTC_SDRCTLREG3_ID );	// sdram control reg 3
	FIELDSTRING( g_DDRCustomFields, TTC_SDRCTLREG4_ID );	// sdram control reg 4
	FIELDSTRING( g_DDRCustomFields, TTC_SDRCTLREG5_ID );	// sdram control reg 5
	FIELDSTRING( g_DDRCustomFields, TTC_SDRCTLREG14_ID );	// sdram control reg 14
	FIELDSTRING( g_DDRCustomFields, TTC_SDRPADREG_ID );		// sdram pad calibration reg
	FIELDSTRING( g_DDRCustomFields, TTC_ADRMAPREG0_ID );	// address map cs0
	FIELDSTRING( g_DDRCustomFields, TTC_ADRMAPREG1_ID );	// address map cs1
	FIELDSTRING( g_DDRCustomFields, TTC_ADRMAPREG2_ID );	// address map cs2
	FIELDSTRING( g_DDRCustomFields, TTC_USRCMDREG0_ID );	// user initiated command registers
	FIELDSTRING( g_DDRCustomFields, TTC_SDRSTAREG_ID );		// sdram status register
	FIELDSTRING( g_DDRCustomFields, TTC_PHYCTLREG3_ID );	// phy control reg 3
	FIELDSTRING( g_DDRCustomFields, TTC_PHYCTLREG7_ID );	// phy control reg 7
	FIELDSTRING( g_DDRCustomFields, TTC_PHYCTLREG8_ID );	// phy control reg 8
	FIELDSTRING( g_DDRCustomFields, TTC_PHYCTLREG9_ID );	// phy control reg 9
	FIELDSTRING( g_DDRCustomFields, TTC_PHYCTLREG10_ID );	// phy control reg 10
	FIELDSTRING( g_DDRCustomFields, TTC_PHYCTLREG13_ID );	// phy control reg 13
	FIELDSTRING( g_DDRCustomFields, TTC_PHYCTLREG14_ID );	// phy control reg 14
	FIELDSTRING( g_DDRCustomFields, TTC_DLLCTLREG1_ID );	// dll control reg 1
	FIELDSTRING( g_DDRCustomFields, TTC_DLLCTLREG2_ID );	// dll control reg 2
	FIELDSTRING( g_DDRCustomFields, TTC_DLLCTLREG3_ID );	// dll control reg 3
	FIELDSTRING( g_DDRCustomFields, TTC_MCBCTLREG1_ID );	// mcb control reg 1
	FIELDSTRING( g_DDRCustomFields, TTC_MCBCTLREG2_ID );	// mcb control reg 2
	FIELDSTRING( g_DDRCustomFields, TTC_MCBCTLREG3_ID );	// mcb control reg 3
	FIELDSTRING( g_DDRCustomFields, TTC_MCBCTLREG4_ID );	// mcb control reg 4
	FIELDSTRING( g_DDRCustomFields, TTC_OPDELAY_ID );		// operation delay reg
	FIELDSTRING( g_DDRCustomFields, TTC_OPREAD_ID );		// operation read reg
}

// MUST call PopulateVoltagesFields to fill g_VoltagesFields array with enum
// field strings before trying to access g_VoltagesFields
void TTC1Strings::PopulateVoltagesFields(t_stringVector& g_VoltagesFields)
{
	// TBD


	// stringize the enums into field name strings
	// allocate the full array beased on the enum max
//	g_VoltagesFields.resize(MMP2_VOLTAGES_E_MAX);
	
	// populate strings for each field
//	FIELDSTRING( g_VoltagesFields,  );
}

