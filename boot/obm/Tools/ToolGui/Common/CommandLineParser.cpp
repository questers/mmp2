/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "CommandLineParser.h"

#if LINUX
#include <stdlib.h>
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

CCommandLineParser::CCommandLineParser(void)
{
    Reset();
}

CCommandLineParser::~CCommandLineParser(void)
{
}

void CCommandLineParser::Reset()
{
    bIsReservedDataInFile = false;
    bIsKeyFile = false;
    bIsHashFile = false;
    bIsDsFile = false;
    bIsTimInFile = false;
    bIsTimOutFile = false;
    bIsNonTrusted = true;
    bIsTimVerify = false;
    bIsPartitionDataFile = false;
    iOption = 1;
    bVerbose = false;
    bConcatenate = false;
    bOneNANDPadding = false;
    iProcessorType = PXA_NONE;
    uiPaddedSize = 0;
} 

#if TOOLS_GUI == 0
bool CCommandLineParser::ParseCommandLine (int argc,char *argv[])
{
    int currentOption = 1;
    BYTE Option = 0;

    while (currentOption < argc)
    {
#if WIN32
        if ((*argv[currentOption] == '-') || (*argv[currentOption] == '/'))
#else
        if (*argv[currentOption] == '-')
#endif
        {
            Option = *++argv[currentOption++];

            switch (Option)
            {
                case OPTION:
                    iOption = atoi (argv[currentOption]);
                    if (iOption > 4 || iOption < 1)
                    {
                        iOption = 1;
                        printf("\n  Error: Parameter Invalid or Missing: -%c switch: ", (char)OPTION );
                        printf("Defaulting to Option Mode 1...\n");
                    }
                    break;
                case KEY:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error:   Command Line Parsing Error: -%c switch: ", (char)KEY );
                        printf (" Missing Key file name parameter.\n");
                        return false;
                    }
                    KeyFileName = argv[currentOption];
                    bIsKeyFile = true;
                    break;
                case HASH:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error:   Command Line Parsing Error: -%c switch: ", (char)HASH );
                        printf (" Missing hash file name parameter.\n");
                        return false;
                    }
                    HashFileName = argv[currentOption];
                    bIsHashFile = true;
                    break;
                case DS:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error:   Command Line Parsing Error: -%c switch: ", (char)DS );
                        printf (" Missing hash file name parameter.\n");
                        return false;
                    }
                    DsFileName = argv[currentOption];
                    bIsDsFile = true;
                    break;
                case TIMIN:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error:   Command Line Parsing Error: -%c switch: ", (char)TIMIN );
                        printf (" Missing input file name parameter.\n");
                        return false;
                    }
                    TimInFileName = argv[currentOption];
                    bIsTimInFile = true;
                    break;
                case TIMOUT:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error:   Command Line Parsing Error: -%c switch: ", (char)TIMOUT );
                        printf (" Missing output file name parameter.\n");
                        return false;
                    }
                    TimOutFileName = argv[currentOption];
                    bIsTimOutFile = true;
                    break;
                case VERBOSE:
                    bVerbose = true;
                    break;
                case CONCATENATE:
                    bConcatenate = true;
                    break;
                case TIMVERIFY:
                    bIsTimVerify = true;
                    break;
                case RESERVED:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error:   Command Line Parsing Error: -%c switch: ", (char)RESERVED );
                        printf (" Missing reserved data file name parameter.\n");
                        return false;
                    }
                    ReservedFileName = argv[currentOption];
                    bIsReservedDataInFile = true;
                    break;
                case PARTITION:
                    if (currentOption == argc)
                    {
                        printf ("\n  Error:   Command Line Parsing Error: -%c switch: ", (char)PARTITION );
                        printf (" Missing partition data file name parameter.\n");
                        return FALSE;
                    }
                    PartitionFileName = argv[currentOption];
                    bIsPartitionDataFile = true;
                    break;
                case PROCESSOR_TYPE:
                    iProcessorType = (eProcessorType)atoi (argv[currentOption]);
                    if (iProcessorType <= PXA_NONE || iProcessorType > PXAMAX_PT )
                    {
                        printf ("\n  Error:   Command Line Parsing Error: -%c switch: ", (char)PROCESSOR_TYPE );
                        printf (" Missing or invalid processor type parameter.\n");
                        return false;
                    }
                    break;
                case PADDED_TIM_IMAGE:
                    uiPaddedSize = atoi(argv[currentOption]);
                    // normalize padded size to a mod 256 value
                    uiPaddedSize = ((uiPaddedSize + 255)/256) * 256;
                    if ( uiPaddedSize > 8192 )
                    {
                        printf ("\n  Error:   Command Line Parsing Error: -%c switch: ", (char)PADDED_TIM_IMAGE );
                        printf (" Missing or invalid padded TIM image size parameter. Value must not be less than 8447.\n");
                        return false;
                    }
                    bOneNANDPadding = true;
                    break;

                case HELP:
                    PrintUsage();
                    break;

                default:
                    printf ("\n  Error: Command line option -%c is invalid!\n",Option);
                    return false;
            }
        }
        else
        {
            currentOption++;
        }
    }

    if (CheckYourOptions () == false) return false;

    return true;
}
#endif

bool CCommandLineParser::CheckYourOptions ()
{
    // Check for TIM verify functionality for trusted operation.
    if ( ( iOption != 1 ) && ( bIsNonTrusted && bIsTimVerify ) )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: Invalid Options Error:\n");
        printf ("Missing switch, non-trusted descriptor file or incorrect option mode.\n");
        printf ("Example: tbb -m 1 -r timdescriptorfile.txt -V\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }
    switch (iOption)
    {
        case 1:
            if (!bIsKeyFile && !bIsPartitionDataFile)
            {
                printf ("////////////////////////////////////////////////////////////\n");
                printf ("  Error: Invalid Options Error:\n");
                printf ("Missing TIM descriptor file name parameter. (-r filename.txt)\n");
                printf ("Example: tbb -m 1 -r timdescriptorfile.txt\n");
                printf ("////////////////////////////////////////////////////////////\n");
                return false;
            }
            break;
        case 2:
            if (!bIsKeyFile || !bIsHashFile)
            {
                printf ("////////////////////////////////////////////////////////////\n");
                printf ("  Error: Invalid Options Error:\n");
                printf ("Missing TIM descriptor file name parameter. (-r filename.txt) OR\n");
                printf ("Missing output hash key file name parameter. (-h filename.bin)\n");
                printf ("Example: tbb -m 2 -r timdescriptorfile.txt -h timhash.bin\n");
                printf ("////////////////////////////////////////////////////////////\n");
                return false;
            }
            break;
        case 3:
            if (!bIsKeyFile || !bIsHashFile || !bIsDsFile)
            {
                printf ("////////////////////////////////////////////////////////////\n");
                printf ("  Error: Invalid Options Error:\n");
                printf ("Missing private key file name parameter. (-r filename.txt) OR\n");
                printf ("Missing input hash key file name parameter. (-h filename.bin) OR\n");
                printf ("Missing output digital signature file name parameter. (-s filename.bin)\n");
                printf ("////////////////////////////////////////////////////////////\n");
                return false;
            }
            break;
        case 4:
            if (!bIsTimInFile || !bIsTimOutFile || !bIsDsFile)
            {
                printf ("////////////////////////////////////////////////////////////\n");
                printf ("  Error: Invalid Options Error:\n");
                printf ("Missing input TIM file name parameter. (-i filename.bin) OR\n");
                printf ("Missing output TIM file name parameter. (-o filename.bin) OR\n");
                printf ("Missing input digital signature file name parameter. (-s filename.bin)\n");
                printf ("////////////////////////////////////////////////////////////\n");
                return false;
            }
            break;
        default:
            printf ("////////////////////////////////////////////////////////////\n");
            printf ("  Error: Invalid Options Error:\n");
            printf ("You entered an incorrect value for the -m option or\n");
            printf ("Example: tbb -m 1 -r TimDescriptorFile.txt\n");
            printf ("Note: If you leave out the -m option the program will\n");
            printf ("      default this option to 1.\n");
            printf ("////////////////////////////////////////////////////////////\n");
            return false;
    }

    return true;
}


void CCommandLineParser::PrintUsage ()
{
    printf ("\n WTP Trusted Image Module Builder\n\n");
#if !TRUSTED
    printf ("  ***Note: This is the non-trusted version only***\n\n");
#endif
    printf ("  Usage:\n");
    printf ("  ======\n");
    printf ("  >> TBB.exe <Option>|<Option file> ...\n\n");
    printf ("  Options:\n");
    printf ("  ========\n");
    printf ("  -v\t\tVerbose mode.  Debugging information will be printed.\n");

    printf ("  -T\t\t[<PXA168=0><PXA30x=1><PXA31x=2><PXA32x=3><PXA688=4><PXA91x=5><PXA92x=6>\n");
    printf ("    \t\t <PXA93x=7><PXA94x=8><PXA95x=9><ARMADA168=10><ARMADA610=12>\n");
    printf ("    \t\t <MG1=13><TAVOR_PV_MG2=14><ESHEL=15><ARMADA620=16><ESHEL_LTE=17><ARMADA622=18><WUKONG=19>]\n");

    printf ("  -P <Partition.txt>\t\tParition.txt partition table data text file to process. Outputs Partition.bin\n");
    printf ("  -r <TIM.txt file path>\tTIM.txt file to process.\n\n");
    printf ("  -R <ReservedData file path>\tReserved Data binary file to append it to the TIM image.\n\n");
    printf ("  -m 1\t\tBuild a trusted or non trusted image module:\n\tTBB -m 1 -r sample_tim.txt\n\n");
#if TRUSTED
    printf ("  -i <input TIM.bin file path>\tTIM.bin file to read.\n");
    printf ("  -o <output TIM.bin file path>\tTIM.bin file to write.\n");
    printf ("  -h <intput TIMhash.bin file path>\tTIMhash.bin file to read.\n");
    printf ("  -s <input DS.bin file path>\tDigitalSignature.bin file to read.\n");
    printf ("  -O <n>\t\t\tONEnand TIM padding (0xff) in binary. Use values for <n> in the range 0-8192.\n"); 
    printf ("\n");	
    printf ("  -m 2\t\tBuild an image module without digital signature and a hash of that image:\n");
    printf ("  \tTBB -m 2 -r sample_tim.txt -h timhash.bin\n\n");
    printf ("  -m 3\t\tBuild a digital signature from image module hash and private key:\n");
    printf ("  \tTBB -m 3 -r privatekey.txt -h timhash.bin -s ds.bin\n\n");
    printf ("  -m4\t\tBuild a trusted image module from digital signature and image module:\n");
    printf ("  \tTBB -m 4 -s ds.bin -i TIMtestTrustNoDs.bin -o TIMtestTrustFromDs.bin\n\n");
    printf (" -V \t\tTIM Verify Digital Signature:\n");
    printf (" ===========\n");
    printf (" >>TBB.exe -m 1 -r TIM.txt -V\n\n");
#endif
    printf (" Reading reserved data from external file:\n");
    printf (" =========================================\n");
    printf (" >>TBB.exe -r NTIM.txt -R ReservedDataFile.bin\n");

    printf (" Processing partitioning information:\n");
    printf (" =========================================\n");
    printf (" >>TBB.exe -r NTIM.txt -P Partition.txt\n");
}

#if TOOLS_GUI == 1
bool CCommandLineParser::ProcessorType( string& sProcessorType )
{
    iProcessorType = PXA_NONE;
    for ( int i = 0; i < PXAMAX_PT; i++ )
    {
        if ( gsProcessorType[i] == sProcessorType )
        {
            iProcessorType = (eProcessorType)i;
            return true;
        }
    }

    return false;
}

string CCommandLineParser::ProcessorType()
{
    if ( iProcessorType > PXA_NONE && iProcessorType < PXAMAX_PT )
        return gsProcessorType[iProcessorType];
    else
        return "";
}

#endif
