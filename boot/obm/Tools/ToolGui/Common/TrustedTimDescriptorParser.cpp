/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#if TRUSTED

#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif
#pragma warning ( disable : 4996 )

#include "TrustedTimDescriptorParser.h"
#if TOOLS_GUI == 0
#include "CommandLineParser.h"
#endif

#if LINUX
#include <cstring>
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

CTrustedTimDescriptorParser::CTrustedTimDescriptorParser(void)
: CTimDescriptorParser()
{
    memset( RsaPrivateKey, 0, sizeof(RsaPrivateKey) );
    memset( ECDSAPrivateKey, 0, sizeof(ECDSAPrivateKey) );
    memset ( &Ds, 0, sizeof(Ds) );
}

CTrustedTimDescriptorParser::~CTrustedTimDescriptorParser(void)
{
    Reset();
}

void CTrustedTimDescriptorParser::Reset()
{
    CTimDescriptorParser::Reset();

    memset( RsaPrivateKey, 0, sizeof(RsaPrivateKey) );
    memset( ECDSAPrivateKey, 0, sizeof(ECDSAPrivateKey) );
    memset (&Ds,0,sizeof (Ds));

    // delete all KeyInfo items
    t_KeyInfoListIter iter = KeyInfoList.begin();
    while( iter != KeyInfoList.end() )
    {
        delete *iter;
        iter++;
    }
    KeyInfoList.clear();
}

#if TOOLS_GUI == 1
bool CTrustedTimDescriptorParser::ParseTrustedDescriptor (CDownloaderInterface& DownloaderInterface)
#else
bool CTrustedTimDescriptorParser::ParseTrustedDescriptor (CCommandLineParser& CommandLineParser)
#endif
{
    UINT_T IntParam = 0;
    string NextImageId;
    unsigned int HashSize = 0;
    unsigned int TimSizeWithNoDS = 0;
    string tbuf;
    
#if TOOLS_GUI == 1
    DownloaderInterface.DiscardAll();
    if (DownloaderInterface.ImageBuild.Mode() == 1)
#else
    if (CommandLineParser.iOption == 1)
#endif
    {
        printf ("\nParsing trusted TIM...\n\n");
    }
    else
    {
        printf ("\nParsing trusted TIM without digital signature.\n");
    }

    CTimDescriptorLine* pLine = 0;
    if ( pLine = CTimDescriptor::GetLineField( "Issue Date" ) )
    {
        m_TimDescriptor.TimHeader().VersionBind.IssueDate = Translate(pLine->m_FieldValue);
#if TOOLS_GUI == 1
        pLine->AddRef( &(m_TimDescriptor.TimHeader().VersionBind.IssueDate) );
#endif
    }

    if ( pLine = CTimDescriptor::GetNextLineField( "OEM UniqueID", pLine ) )
    {
        m_TimDescriptor.TimHeader().VersionBind.OEMUniqueID = Translate(pLine->m_FieldValue);
#if TOOLS_GUI == 1
        pLine->AddRef( &(m_TimDescriptor.TimHeader().VersionBind.OEMUniqueID) );
#endif
    }

    // get Processor Type, but if not found reposition to get WTM Save State Flash Signature
    if ( pLine = CTimDescriptor::GetNextLineField( "Processor Type", pLine, false ) )
    {
        m_TimDescriptor.ProcessorType( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &(m_TimDescriptor.ProcessorType()) );
#endif
    }
    
    // since processor type may be in two places we need to find "Size.." starting at the beginning again
    if ( pLine = CTimDescriptor::GetLineField( "WTM Save State Flash Signature" ) )
    {
        m_TimDescriptor.TimHeader().FlashInfo.WTMFlashSign = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &(m_TimDescriptor.TimHeader().FlashInfo.WTMFlashSign) );
#endif
    }
    
    if ( pLine = CTimDescriptor::GetNextLineField( "WTM Save State Entry", pLine ) )
    {
        m_TimDescriptor.TimHeader().FlashInfo.WTMEntryAddr = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &(m_TimDescriptor.TimHeader().FlashInfo.WTMEntryAddr) );
#endif
    }
    
    if ( pLine = CTimDescriptor::GetNextLineField( "WTM Save State Backup Entry", pLine ) )
    {
        m_TimDescriptor.TimHeader().FlashInfo.WTMEntryAddrBack  = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &(m_TimDescriptor.TimHeader().FlashInfo.WTMEntryAddrBack) );
#endif
    }

    m_TimDescriptor.TimHeader().FlashInfo.WTMPatchSign = 0xFFFFFFFF;
    m_TimDescriptor.TimHeader().FlashInfo.WTMPatchAddr = 0xFFFFFFFF;

    if ( pLine = CTimDescriptor::GetNextLineField( "Boot Flash Signature", pLine ) )
    {
        m_TimDescriptor.TimHeader().FlashInfo.BootFlashSign  = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &(m_TimDescriptor.TimHeader().FlashInfo.BootFlashSign) );
#endif
    }

    if ( pLine = CTimDescriptor::GetNextLineField( "Number of Images", pLine ) )
    {
        m_TimDescriptor.TimHeader().NumImages  = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &(m_TimDescriptor.TimHeader().NumImages) );
#endif
    }

    if ( pLine = CTimDescriptor::GetNextLineField( "Number of Keys", pLine ) )
    {
        m_TimDescriptor.TimHeader().NumKeys  = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &(m_TimDescriptor.TimHeader().NumKeys) );
#endif
    }

    if ( pLine = CTimDescriptor::GetNextLineField( "Size of Reserved in bytes", pLine ) )
    {
        m_TimDescriptor.TimHeader().SizeOfReserved  = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &(m_TimDescriptor.TimHeader().SizeOfReserved) );
#endif
    }

#if TOOLS_GUI == 0
    if (CommandLineParser.bIsReservedDataInFile)
    {
        std::streamoff sizeOfReservedDataFile = 0;
        ifstream ifsReservedDataFile;
        ifsReservedDataFile.open( CommandLineParser.ReservedFileName.c_str(), ios_base::binary );
        if ( ifsReservedDataFile.bad() ||ifsReservedDataFile.fail() )
        {	
            printf ("\n  Error: Cannot open file name <%s> !\n",CommandLineParser.ReservedFileName.c_str());
            return false;
        }

        // get file size
        ifsReservedDataFile.seekg( 0, ios_base::end );
        ifsReservedDataFile.clear();
        sizeOfReservedDataFile = ifsReservedDataFile.tellg();
        // reset to SOF
        ifsReservedDataFile.seekg( 0, ios_base::beg );

        // note that the (UINT_T) cast is dangerous for very large files but should not be an issue here
        m_TimDescriptor.TimHeader().SizeOfReserved += (UINT_T)sizeOfReservedDataFile;
    }
#endif

    //return error if number of images > 10
    if ((int)m_TimDescriptor.TimHeader().NumImages > MAX_IMAGE_FILES )
    {
        printf("\n  Error: Maximum allowable number of images in the descriptor file is 10!\n");
        return false;
    }
    
    for (int i = 0; i < (int)m_TimDescriptor.TimHeader().NumImages; i++)
    {
        CImageDescription* pImageDesc = new CImageDescription();

        if (ParseImageInfo(*pImageDesc, pLine) == false) return false;

        if (i == 0)
        {
            if ( (Translate(pImageDesc->ImageIdTag()) & TYPEMASK) != (TIMIDENTIFIER & TYPEMASK) ) 
            {
                printf ("\n  Error: The ImageID value is incorrect for the TIM image!\n");
                return false;
            }

            if ( pLine = CTimDescriptor::GetNextLineField( "Image Filename", pLine ) )
            {
                m_sTimBinFilename = pLine->m_FieldValue;
#if TOOLS_GUI == 1
                pLine->AddRef( &m_sTimBinFilename );
#endif
            }
            else
            {
                printf ("\n  Error: Key file parsing error reading, TIM Bin Filename: <%s>\n", m_sTimBinFilename.c_str());
                return false;
            }

            // prepend current directory if no path on file
            PrependPathIfNone( m_sTimBinFilename  );
            pImageDesc->ImageFilePath( m_sTimBinFilename );

#if TOOLS_GUI == 1
            // keep image build and image build dlg in sync with TIM descriptor
            DownloaderInterface.ImageBuild.TimDescriptorFilePath( m_sTimBinFilename );
            DownloaderInterface.DiscardAll();
            DownloaderInterface.TimFilePath( m_sTimBinFilename );
            pImageDesc->ImageSize( m_TimDescriptor.GetTimImageSize( DownloaderInterface.ImageBuild.CommandLineParser.bOneNANDPadding, DownloaderInterface.ImageBuild.CommandLineParser.uiPaddedSize ) );
#else
            pImageDesc->ImageSize( m_TimDescriptor.GetTimImageSize( CommandLineParser.bOneNANDPadding, CommandLineParser.uiPaddedSize ) );
#endif

            if(pImageDesc->ImageSizeToHash() == 0xFFFFFFFF ) 
                pImageDesc->ImageSizeToHash( pImageDesc->ImageSize() );
        }
        else
        {
            if (pImageDesc->ImageId() != NextImageId)
            {
                printf ("\n  Error: The ImageID doesn't = NextImageID from the previous image!\n");
                return false;
            }

            if ( pLine = CTimDescriptor::GetNextLineField( "Image Filename", pLine ) )
            {
                pImageDesc->ImageFilePath( pLine->m_FieldValue );
#if TOOLS_GUI == 1
                pLine->AddRef( &(pImageDesc->ImageFilePath()) );
#endif
            }
            else
            {
                printf ("\n  Error: Key file parsing error reading, Image Filename: <%>s\n", pImageDesc->ImageFilePath().c_str());
                return false;
            }

            // prepend current directory if no path on file
            PrependPathIfNone( pImageDesc->ImageFilePath() );

            ifstream ifsImage;
            ifsImage.open( pImageDesc->ImageFilePath().c_str(), ios_base::in | ios_base::binary );
            if ( ifsImage.bad() || ifsImage.fail() || !ifsImage.is_open() )
            {
                printf ("\n  Error: Cannot open image file name <%s>.\n", pImageDesc->ImageFilePath().c_str());
                pImageDesc->ImageSize( 0 );
#if TOOLS_GUI == 0
                return false;
#endif
            }
            else
            {
                // Get position of file, thus the file size.
                ifsImage.seekg( 0, ios_base::end );
                ifsImage.clear();
                pImageDesc->ImageSize( (unsigned int)ifsImage.tellg() ); 
                //printf("Size: %d\n", m_pImageInfo[i].ImageSize);
            }
            ifsImage.close();

#if TOOLS_GUI == 1
            // Output image file name
            if (CreateOutputImageName (pImageDesc->ImageFilePath(), m_sImageOutFilename) == false)
                return false;

            DownloaderInterface.AddBinImage( m_sImageOutFilename ); 
#endif

            if ((pImageDesc->ImageSizeToHash() > pImageDesc->ImageSize() ) ||
                (pImageDesc->ImageSizeToHash() == 0xFFFFFFFF))
            {
                pImageDesc->ImageSizeToHash( pImageDesc->ImageSize() );
            }
        }

        m_TimDescriptor.ImagesList().push_back( pImageDesc );

        NextImageId = pImageDesc->NextImageId();
    }

    if ( m_TimDescriptor.ImagesList().size() != (int)m_TimDescriptor.TimHeader().NumImages )
        cout << "Number of Images field is not consistent with image structs in TIM text file." << endl;

    //check to make sure the images do not overlap each other
    if(CheckImageOverlap(m_TimDescriptor.ImagesList(), m_TimDescriptor.TimHeader().FlashInfo.BootFlashSign) == false)
    {
#if TOOLS_GUI == 0
        return false;
#endif
    }

    KEY_MOD_3_4_0* pKeyInfo = 0;
    for (int i = 0; i < (int)m_TimDescriptor.TimHeader().NumKeys; i++)
    {
        pKeyInfo = new KEY_MOD_3_4_0;
        memset (pKeyInfo,0,sizeof (KEY_MOD_3_4_0));
        if (ParseKeyInfo (pKeyInfo, pLine) == false) 
        {
            delete pKeyInfo;
            return false;
        }
        KeyInfoList.push_back( pKeyInfo );
    }

    // skip the reserved section if size is 0.
    if(m_TimDescriptor.TimHeader().SizeOfReserved != 0){
        if (ParseReservedData() == false)
            return false;
    }

#if EXTENDED_RESERVED_DATA
#if TOOLS_GUI == 1
    if (ParseExtendedReservedData(DownloaderInterface) == false) return false;
#else
    if (ParseExtendedReservedData(CommandLineParser) == false) return false;
#endif
#endif

#if TOOLS_GUI == 1
    if (ParseDsaInfo (DownloaderInterface) == false) 
        return false;
#else

    if (ParseDsaInfo (CommandLineParser) == false) 
        return false;

    if (VerifySizeOfTim(CommandLineParser) == false)
        return false;
#endif

#if TOOLS_GUI == 1
    if (DownloaderInterface.ImageBuild.Concatenate())
#else
    if (CommandLineParser.bConcatenate)
#endif
    {
        ifstream ifsImage;
        t_ImagesIter iter = m_TimDescriptor.ImagesList().begin();
        while ( iter != m_TimDescriptor.ImagesList().end() )
        {
            ifsImage.open( (*iter)->ImageFilePath().c_str(), ios_base::in | ios_base::binary );
            if ( ifsImage.bad() || ifsImage.fail() )
            {
                printf ("\n  Error: Cannot open image file name <%s>.\n",(*iter)->ImageFilePath().c_str());
#if TOOLS_GUI == 0
                return false;
#endif
            }
            ifsImage.close();
            iter++;
        }

    }

//#if TOOLS_GUI == 1
//	UpdateTimDescriptorObject();
//	printf ("  Success: Trusted Tim Descriptor file parsing has completed successfully!\n");
//#else
    printf ("  Trusted Tim Descriptor file parsing has completed!\n");
//#endif
    m_TimDescriptor.Changed(false);

    return true;
}

bool CTrustedTimDescriptorParser::ParseKeyInfo (KEY_MOD_3_4_0 *pKeyInfo, CTimDescriptorLine*& pLine)
{
    UINT_T IntParam = 0;
    bool bRet = true;
    string sValue;
    unsigned int i = 0;
    bool IsOk = true;

    // This is a helper function to parse key info data from the descriptor
    // text file supplied by the user.

    t_KeyList& KeyList = m_TimDescriptor.KeyList();
    CKey * pKey = new CKey();

    if ( (pLine = CTimDescriptor::GetNextLineField( "Key ID", pLine )) == 0 ) 
    {
        bRet = false;
        goto Exit;
    }
    pKeyInfo->KeyID = Translate(pLine->m_FieldValue);
    pKey->KeyTag( HexFormattedAscii(pKeyInfo->KeyID) );
#if TOOLS_GUI == 1
    pLine->AddRef( &pKey->KeyTag() );
#endif
    if ( (pLine = CTimDescriptor::GetNextLineField( "Hash Algorithm ID", pLine )) == 0 ) 
    {
        bRet = false;
        goto Exit;
    }
    pKeyInfo->HashAlgorithmID = (HASHALGORITHMID_T)Translate(pLine->m_FieldValue);
    if ( pKeyInfo->HashAlgorithmID == 2 || pKeyInfo->HashAlgorithmID == 256 || pKeyInfo->HashAlgorithmID == 32 )
    {
        pKeyInfo->HashAlgorithmID = SHA256; // SHA256
        sValue = "SHA-256";
        pKey->HashAlgorithmId( sValue );
    }
//	else if ( pKeyInfo->HashAlgorithmID == 64 || pKeyInfo->HashAlgorithmID == 512 )
//	{
//		pKeyInfo->HashAlgorithmID = SHA512; // SHA512
//		sValue = "SHA-512";
//		pKey->HashAlgorithmId( sValue );
//	}
    else
    {
        pKeyInfo->HashAlgorithmID = SHA160;
        sValue = "SHA-160";
        pKey->HashAlgorithmId( sValue );
    }

#if TOOLS_GUI == 1
    pLine->AddRef( &pKey->HashAlgorithmId() );
#endif

    if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
    {
        if ( (pLine = CTimDescriptor::GetNextLineField( "Modulus Size in bytes", pLine )) == 0 ) 
        {
            bRet = false;
            goto Exit;
        }
        pKeyInfo->KeySize = Translate(pLine->m_FieldValue)*8;
    }
    else
    {
        if ( (pLine = CTimDescriptor::GetNextLineField( "Key Size in bits", pLine )) == 0 ) 
        {
            bRet = false;
            goto Exit;
        }
        pKeyInfo->KeySize = Translate(pLine->m_FieldValue);
    }
    pKey->KeySize( pKeyInfo->KeySize ); 

#if TOOLS_GUI == 1
    pLine->AddRef( &pKey->KeySize() );
#endif

    if (pKeyInfo->KeySize != 1024 && pKeyInfo->KeySize != 256 && pKeyInfo->KeySize != 512 
        && pKeyInfo->KeySize != 2048 && pKeyInfo->KeySize != 521 )
    {
        printf ("  Error: The Key Size is incorrect for Key ID 0x%08X\n",pKeyInfo->KeyID);
        return false;
    }

    if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
    {
        if ( (pLine = CTimDescriptor::GetNextLineField( "Public Key Size in bytes", pLine )) == 0 )
        {
            bRet = false;
            goto Exit;
        }
        pKeyInfo->PublicKeySize = Translate(pLine->m_FieldValue)*8;
    }
    else
    {
        if ( (pLine = CTimDescriptor::GetNextLineField( "Public Key Size in bits", pLine )) == 0 )
        {
            bRet = false;
            goto Exit;
        }
        pKeyInfo->PublicKeySize = Translate(pLine->m_FieldValue);
    }
    pKey->PublicKeySize( pKeyInfo->PublicKeySize );

#if TOOLS_GUI == 1
    pLine->AddRef( &pKey->PublicKeySize() );
#endif

    if ( m_TimDescriptor.TimHeader().VersionBind.Version >= TIM_3_3_00 )
    {
        if ( (pLine = CTimDescriptor::GetNextLineField( "Encrypt Algorithm ID", pLine )) == 0 )
        {
            printf ("  Error: Key file parsing error reading, Encrypt Algorithm ID:\n");
            bRet = false;
            goto Exit;
        }
        else
            pKeyInfo->EncryptAlgorithmID = (ENCRYPTALGORITHMID_T)Translate(pLine->m_FieldValue);

        pKey->EncryptAlgorithmId( pKeyInfo->EncryptAlgorithmID );

#if TOOLS_GUI == 1
        pLine->AddRef( &pKey->EncryptAlgorithmId() );
#endif

        if ( pKeyInfo->EncryptAlgorithmID < PKCS1_v1_5_Caddo || pKeyInfo->EncryptAlgorithmID > ECDSA_521 ) 
            pKeyInfo->EncryptAlgorithmID = Marvell_DS;

        if ( (pKeyInfo->EncryptAlgorithmID == ECDSA_256 || pKeyInfo->EncryptAlgorithmID == ECDSA_521 ) 
            && m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 ) 
        {
            printf( "TIM version %s does not support ECDSA algorithms for Key.", HexFormattedAscii(m_TimDescriptor.TimHeader().VersionBind.Version ).c_str());
            return false;
        }   
    }

    if ( (pKeyInfo->EncryptAlgorithmID != ECDSA_256) && (pKeyInfo->EncryptAlgorithmID != ECDSA_521) ) 
    {
        bRet = ParseRSAKey( pKey, pKeyInfo, pLine );
    }
    else
        bRet = ParseECCKey( pKey, pKeyInfo, pLine );

Exit:
    if ( bRet == false )
        delete pKey;
    else
        KeyList.push_back( pKey );
    
    return bRet;
}

bool CTrustedTimDescriptorParser::ParseRSAKey(CKey * pKey, KEY_MOD_3_4_0 *pKeyInfo, CTimDescriptorLine*& pLine) 
{
    bool IsOk = false;

    if ( (pLine = CTimDescriptor::GetNextLineField( "RSA Public Key Exponent", pLine )) == 0 )
    {
        printf ("  Error: Key file parsing error reading, RSA Public Key Exponent:\n");
        return false;
    }

#if TOOLS_GUI == 1
    pLine->AddRef( &pKey->PublicKeyExponentList() );
#endif
    IsOk = FillDataArrays ( pLine,(unsigned long *)pKeyInfo->Rsa.RSAPublicExponent, pKeyInfo->PublicKeySize/8 );
    if (IsOk == false)
    {
        if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
        {
            printf ("Public Key Size in bytes = %d\n\n", pKeyInfo->PublicKeySize/8);
        }
        else
        {
            printf ("Public Key Size in bits = %d\n\n", pKeyInfo->PublicKeySize);
        }
        printf ("  Error: The amount of data read from key file doesn't match what is\n");
        if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
        {
            printf ("indicated by 'Public Key Size in bytes' directive!\n");
        }
        else
        {
            printf ("indicated by 'Public Key Size in bits' directive!\n");
        }
        printf ("Error: Public Exponent for Key\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }

    if ( (pLine = CTimDescriptor::GetNextLineField( "RSA System Modulus", pLine )) == 0 )
    {
        printf ("  Error: Key file parsing error reading, RSA System Modulus:\n");
        return false;
    }

#if TOOLS_GUI == 1
    pLine->AddRef( &pKey->RsaSystemModulusList() );
#endif
    IsOk = FillDataArrays ( pLine,(unsigned long *)pKeyInfo->Rsa.RSAModulus, pKeyInfo->KeySize/8 );
    if (IsOk == false)
    {
        if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
        {
            printf ("Modulus Size in bytes = %d\n\n", pKeyInfo->KeySize/8);
        }
        else
        {
            printf ("Key Size in bits = %d\n\n", pKeyInfo->KeySize);
        }
        printf ("  Error: The amount of data read from key file doesn't match what is\n");
        if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
        {
            printf ("indicated by 'Modulus Size in bytes' directive!\n");
        }
        else
        {
            printf ("indicated by 'Key Size in bits' directive!\n");
        }
        printf ("Error: RsaModulus for Key\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }

    unsigned int i = 0;
    t_stringListIter iterPubExp = pKey->PublicKeyExponentList().begin();
    while ( iterPubExp != pKey->PublicKeyExponentList().end() )
    {
        if ( i < pKeyInfo->KeySize/32 )
        {
            *(*iterPubExp) = HexFormattedAscii(pKeyInfo->Rsa.RSAPublicExponent[i]);
        }
        iterPubExp++;
        i++;
    }

    i = 0;
    t_stringListIter iterSysMod = pKey->RsaSystemModulusList().begin();
    while ( iterSysMod != pKey->RsaSystemModulusList().end() )
    {
        if ( i < pKeyInfo->KeySize/32 )
        {
            *(*iterSysMod) = HexFormattedAscii(pKeyInfo->Rsa.RSAModulus[i]);
        }
        iterSysMod++;
        i++;
    }

    return true;
}

bool CTrustedTimDescriptorParser::ParseECCKey(CKey * pKey, KEY_MOD_3_4_0 *pKeyInfo, CTimDescriptorLine*& pLine) 
{
    bool IsOk = false;
    if ( (pLine = CTimDescriptor::GetNextLineField( "ECC Public Key Comp X", pLine )) == 0 )
    {
        printf ("  Error: Key file parsing error reading, ECC Public Key Comp X:\n");
        return false;
    }

#if TOOLS_GUI == 1
    pLine->AddRef( &pKey->ECDSAPublicKeyCompX() );
#endif	

    if ( pKeyInfo->KeySize > 0 )
    {
        IsOk = FillDataArrays ( pLine, (unsigned long *)pKeyInfo->Ecdsa.PublicKeyCompX, ((pKeyInfo->KeySize+31)/32*4) );
        if (IsOk == false)
        {
            printf ("Key Size in bits = %d\n\n",pKeyInfo->KeySize);
            printf ("  Error: The amount of data read from key file doesn't match what is\n");
            printf ("indicated by 'Key Size in bits' directive!\n");
            printf ("Error: Key file parsing error reading, ECC Public Key Comp X\n");
            printf ("////////////////////////////////////////////////////////////\n");
            return false;
        }
    }

    if ( (pLine = CTimDescriptor::GetNextLineField( "ECC Public Key Comp Y", pLine )) == 0 )
    {
        printf ("  Error: Key file parsing error reading, ECC Public Key Comp Y:\n");
        return false;
    }

#if TOOLS_GUI == 1
    pLine->AddRef( &pKey->ECDSAPublicKeyCompYList() );
#endif
    if ( pKeyInfo->KeySize > 0 )
    {
        IsOk = FillDataArrays ( pLine, (unsigned long *)pKeyInfo->Ecdsa.PublicKeyCompY, ((pKeyInfo->KeySize+31)/32*4) );
        if (IsOk == false)
        {
            printf ("Key Size in bits = %d\n\n",pKeyInfo->KeySize);
            printf ("  Error: The amount of data read from key file doesn't match what is\n");
            printf ("indicated by 'Key Size in bits' directive!\n");
            printf ("Error: Key file parsing error reading, ECC Public Key Comp Y:\n");
            printf ("////////////////////////////////////////////////////////////\n");
            return false;
        }
    }

    unsigned int i = 0;
    t_stringListIter iterCompX = pKey->ECDSAPublicKeyCompXList().begin();
    while ( iterCompX != pKey->ECDSAPublicKeyCompXList().end() )
    {
        if ( i < (pKeyInfo->KeySize+31)/32 )
        {
            *(*iterCompX) = HexFormattedAscii(pKeyInfo->Ecdsa.PublicKeyCompX[i]);
        }
        iterCompX++;
        i++;
    }

    i = 0;
    t_stringListIter iterCompY = pKey->ECDSAPublicKeyCompYList().begin();
    while ( iterCompY != pKey->ECDSAPublicKeyCompYList().end() )
    {
        if ( i < (pKeyInfo->KeySize+31)/32 )
        {
            *(*iterCompY) = HexFormattedAscii(pKeyInfo->Ecdsa.PublicKeyCompY[i]);
        }
        iterCompY++;
        i++;
    }

    return true;
}


bool CTrustedTimDescriptorParser::VerifyNumberOfKeys ( )
{
    unsigned int i = 0, NumberFound = 0;
    string sLine;
    string sField;
    unsigned int iNumKeys = 0;

    CTimDescriptorLine* pLine = 0;
    if ( (pLine = CTimDescriptor::GetLineField( "Number of Keys" )) == 0  )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: Key file parsing error reading, Number of Keys:\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }
    iNumKeys = Translate(pLine->m_FieldValue);

    while ( ( pLine = CTimDescriptor::GetNextLineField( "Key ID", pLine, false ) ) )
        NumberFound++;

    if (NumberFound != iNumKeys)
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Error: The number of keys found does not match the number of keys\n");
        printf ("set in Number of Keys: declaration statement!\n\n");
        printf ("Number of Keys: %d\n", iNumKeys);
        printf ("Number of keys found = %d\n", NumberFound);
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }

    return true;
}


#if TOOLS_GUI == 1
bool CTrustedTimDescriptorParser::ParseDsaInfo (CDownloaderInterface& DownloaderInterface)
#else
bool CTrustedTimDescriptorParser::ParseDsaInfo (CCommandLineParser& CommandLineParser)
#endif
{
    UINT_T IntParam = 0;
    char szTemp[100] = {0};
    string sLine;
    string sField;
    bool bFound = false;

#if TOOLS_GUI == 1
    CDigitalSignature& DigitalSignature = m_TimDescriptor.DigitalSignature();
#endif

    CTimDescriptorLine* pLine = 0;
    if ( ( pLine = CTimDescriptor::GetLineField( "DSA Algorithm ID" )) == 0 )
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Warning: DSA Algorithm not found in trusted TIM descriptor text file.\n");
        printf ("  Parsing continuing....\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return true;
    }
    Ds.DSAlgorithmID = (ENCRYPTALGORITHMID_T)Translate(pLine->m_FieldValue);
#if TOOLS_GUI == 1
    pLine->AddRef( &DigitalSignature.EncryptAlgorithmId() );
#endif

    if ( Ds.DSAlgorithmID < PKCS1_v1_5_Caddo || Ds.DSAlgorithmID > ECDSA_521 ) 
        Ds.DSAlgorithmID = Marvell_DS;

    if ( (Ds.DSAlgorithmID == ECDSA_256 || Ds.DSAlgorithmID == ECDSA_521 ) 
        && m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 ) 
    {
        printf( "TIM version %s does not support ECDSA algorithms.", HexFormattedAscii(m_TimDescriptor.TimHeader().VersionBind.Version ).c_str());
        return false;
    }

    if ( ( pLine = CTimDescriptor::GetNextLineField( "Hash Algorithm ID", pLine )) == 0 ) return false;
    Ds.HashAlgorithmID = (HASHALGORITHMID_T)Translate(pLine->m_FieldValue);
#if TOOLS_GUI == 1
    pLine->AddRef( &DigitalSignature.HashAlgorithmId() );
#endif
    if (Ds.HashAlgorithmID == 2 || Ds.HashAlgorithmID == 256 || Ds.HashAlgorithmID == 32 )
    {
        Ds.HashAlgorithmID = SHA256; // SHA256
    }
    else if ( Ds.HashAlgorithmID == 64 || Ds.HashAlgorithmID == 512 )
    {
        Ds.HashAlgorithmID = SHA512; // SHA512
    }
    else
        Ds.HashAlgorithmID = SHA160;

    if ( Ds.DSAlgorithmID == ECDSA_256 || Ds.DSAlgorithmID == ECDSA_521 ) 
#if TOOLS_GUI == 1
        ParseECDSA( DownloaderInterface, pLine );
    else
        ParseRSA( DownloaderInterface, pLine );
#else
        ParseECDSA( CommandLineParser, pLine );
    else
        ParseRSA( CommandLineParser, pLine );
#endif


    return true;
}


#if TOOLS_GUI == 1
bool CTrustedTimDescriptorParser::ParseRSA(CDownloaderInterface& DownloaderInterface, CTimDescriptorLine*& pLine)
#else
bool CTrustedTimDescriptorParser::ParseRSA (CCommandLineParser& CommandLineParser, CTimDescriptorLine*& pLine)
#endif
{
    CDigitalSignature& DigitalSignature = m_TimDescriptor.DigitalSignature();

    bool IsOk = false;
    unsigned int i = 0;

    if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
    {
        if ( (pLine = CTimDescriptor::GetNextLineField( "Modulus Size in bytes", pLine )) == 0 ) return false;
        Ds.KeySize = Translate(pLine->m_FieldValue)*8;
    }
    else
    {
        if ( (pLine = CTimDescriptor::GetNextLineField( "Key Size in bits", pLine )) == 0 ) return false;
        Ds.KeySize = Translate(pLine->m_FieldValue);
    }

#if TOOLS_GUI == 1
    pLine->AddRef( &DigitalSignature.KeySize() );
#endif

    if ( (pLine = CTimDescriptor::GetNextLineField( "RSA Public Exponent", pLine )) == 0 )
    {
        printf ("  Error: Key file parsing error reading, RSA Public Exponent:\n");
        return false;
    }

#if TOOLS_GUI == 1
    pLine->AddRef( &DigitalSignature.PublicKeyExponentList() );
#endif

    if ( Ds.KeySize > 0 )
    {
        IsOk = FillDataArrays ( pLine, (unsigned long *)Ds.Rsa.RSAPublicExponent, Ds.KeySize/8 );
        if (IsOk == false)
        {
            if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
            {
                printf ("Modulus Size in bytes = %d\n\n",Ds.KeySize/8);
            }
            else
            {
                printf ("Key Size in bits = %d\n\n",Ds.KeySize);
            }
            printf ("  Error: The amount of data read from key file doesn't match what is\n");
            if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
            {
                printf ("indicated by 'Modulus Size in bytes' directive!\n");
            }
            else
            {
                printf ("indicated by 'Key Size in bits' directive!\n");
            }
            printf ("Error: RSA Public Exponent for DSA Algorithm\n");
            printf ("////////////////////////////////////////////////////////////\n");
            return false;
        }

        DigitalSignature.IncludeInTim( Ds.KeySize > 0 ? true : false );
        DigitalSignature.EncryptAlgorithmId( Ds.DSAlgorithmID );
        
        // recreate keys of correct size
        DigitalSignature.KeySize( Ds.KeySize );

        t_stringListIter iterPubExp = DigitalSignature.PublicKeyExponentList().begin();
        while ( iterPubExp != DigitalSignature.PublicKeyExponentList().end() )
        {
            if ( i < Ds.KeySize/32 )
            {
                *(*iterPubExp) = HexFormattedAscii(Ds.Rsa.RSAPublicExponent[i]);
            }
            iterPubExp++;
            i++;
        }
    }

    if ( (pLine = CTimDescriptor::GetNextLineField( "RSA System Modulus", pLine )) == 0 )
    {
        printf ("  Error: Key file parsing error reading, RSA System Modulus:\n");
        return false;
    }

#if TOOLS_GUI == 1
    pLine->AddRef( &DigitalSignature.RsaSystemModulusList() );
#endif
    if ( Ds.KeySize > 0 )
    {
        IsOk = FillDataArrays ( pLine, (unsigned long *)Ds.Rsa.RSAModulus, Ds.KeySize/8 );
        if (IsOk == false)
        {
            if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
            {
                printf ("Modulus Size in bytes = %d\n\n",Ds.KeySize/8);
            }
            else
            {
                printf ("Key Size in bits = %d\n\n",Ds.KeySize);
            }

            printf ("  Error: The amount of data read from key file doesn't match what is\n");
            if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
            {
                printf ("indicated by 'Modulus Size in bytes' directive!\n");
            }
            else
            {
                printf ("indicated by 'Key Size in bits' directive!\n");
            }
            printf ("Error: RSA System Modulus for DSA Algorithm\n");
            printf ("////////////////////////////////////////////////////////////\n");
            return false;
        }

        i = 0;
        t_stringListIter iterRsaMod = DigitalSignature.RsaSystemModulusList().begin();
        while ( iterRsaMod != DigitalSignature.RsaSystemModulusList().end() )
        {
            if ( i < Ds.KeySize/32 )
            {
                *(*iterRsaMod) = HexFormattedAscii(Ds.Rsa.RSAModulus[i]);
            }
            iterRsaMod++;
            i++;
        }
    }

#if TOOLS_GUI == 1
    if (DownloaderInterface.ImageBuild.Mode() == 1 )
#else
    if (CommandLineParser.iOption == 1  )
#endif
    {
        if ( (pLine = CTimDescriptor::GetNextLineField( "RSA Private Key", pLine )) == 0 )
        {
            printf ("Key file parsing error reading, RSA Private Key:\n");
            return false;
        }

#if TOOLS_GUI == 1
        pLine->AddRef( &DigitalSignature.RsaPrivateKeyList() );
#endif
        if ( Ds.KeySize > 0 )
        {
            IsOk = FillDataArrays ( pLine, (unsigned long *)RsaPrivateKey, Ds.KeySize/8 );
            if (IsOk == false)
            {
                if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
                {
                    printf ("Modulus Size in bytes = %d\n\n", Ds.KeySize/8);
                }
                else
                {
                    printf ("Key Size in bits = %d\n\n", Ds.KeySize);
                }

                printf ("  Error: The amount of data read from key file doesn't match what is\n");	
                if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
                {
                    printf ("indicated by 'Modulus Size in bytes' directive!\n");
                }
                else
                {
                    printf ("indicated by 'Key Size in bits' directive!\n");
                }
                printf ("Error: RSA Private Key for DSA Algorithm\n");
                printf ("////////////////////////////////////////////////////////////\n");
                return false;
            }

            i = 0;
            t_stringListIter iterPrivKey = DigitalSignature.RsaPrivateKeyList().begin();
            while ( iterPrivKey != DigitalSignature.RsaPrivateKeyList().end() )
            {
                if ( i < Ds.KeySize/32 )
                {
                    *(*iterPrivKey) = HexFormattedAscii(RsaPrivateKey[i]);
                }
                iterPrivKey++;
                i++;
            }
        }
    }

    return true;
}


#if TOOLS_GUI == 1
bool CTrustedTimDescriptorParser::ParseECDSA (CDownloaderInterface& DownloaderInterface, CTimDescriptorLine*& pLine)
#else
bool CTrustedTimDescriptorParser::ParseECDSA (CCommandLineParser& CommandLineParser, CTimDescriptorLine*& pLine)
#endif
{
    CDigitalSignature& DigitalSignature = m_TimDescriptor.DigitalSignature();
    bool IsOk = false;
    unsigned int i = 0;

    if ( (pLine = CTimDescriptor::GetNextLineField( "Key Size in bits", pLine )) == 0 ) return false;
    Ds.KeySize = Translate(pLine->m_FieldValue);

#if TOOLS_GUI == 1
    pLine->AddRef( &DigitalSignature.KeySize() );
#endif

    if ( (pLine = CTimDescriptor::GetNextLineField( "ECDSA Public Key CompX", pLine )) == 0 )
    {
        printf ("  Error: Key file parsing error reading, ECDSA Public Key CompX:\n");
        return false;
    }

#if TOOLS_GUI == 1
    pLine->AddRef( &DigitalSignature.ECDSAPublicKeyCompX() );
#endif	

    if ( Ds.KeySize > 0 )
    {
        IsOk = FillDataArrays ( pLine, (unsigned long *)Ds.Ecdsa.ECDSAPublicKeyCompX, ((Ds.KeySize+31)/32*4) );
        if (IsOk == false)
        {
            printf ("Key Size in bits = %d\n\n",Ds.KeySize);
            printf ("  Error: The amount of data read from key file doesn't match what is\n");
            printf ("indicated by 'Key Size in bits' directive!\n");
            printf ("Error: ECDSA Public Key CompX for DSA Algorithm\n");
            printf ("////////////////////////////////////////////////////////////\n");
            return false;
        }

        DigitalSignature.IncludeInTim( Ds.KeySize > 0 ? true : false );
        DigitalSignature.EncryptAlgorithmId( Ds.DSAlgorithmID );
        
        // recreate keys of correct size
        DigitalSignature.KeySize( Ds.KeySize );

        t_stringListIter iterCompX = DigitalSignature.ECDSAPublicKeyCompXList().begin();
        while ( iterCompX != DigitalSignature.ECDSAPublicKeyCompXList().end() )
        {
            if ( i < (Ds.KeySize+31)/32 )
            {
                *(*iterCompX) = HexFormattedAscii(Ds.Ecdsa.ECDSAPublicKeyCompX[i]);
            }
            iterCompX++;
            i++;
        }
    }

    if ( (pLine = CTimDescriptor::GetNextLineField( "ECDSA Public Key CompY", pLine )) == 0 )
    {
        printf ("  Error: Key file parsing error reading, ECDSA Public Key CompY:\n");
        return false;
    }

#if TOOLS_GUI == 1
    pLine->AddRef( &DigitalSignature.ECDSAPublicKeyCompYList() );
#endif
    if ( Ds.KeySize > 0 )
    {
        IsOk = FillDataArrays ( pLine, (unsigned long *)Ds.Ecdsa.ECDSAPublicKeyCompY, ((Ds.KeySize+31)/32*4) );
        if (IsOk == false)
        {
            printf ("Key Size in bits = %d\n\n",Ds.KeySize);
            printf ("  Error: The amount of data read from key file doesn't match what is\n");
            printf ("indicated by 'Key Size in bits' directive!\n");
            printf ("Error: ECDSA Public Key CompY for ECDSA Algorithm\n");
            printf ("////////////////////////////////////////////////////////////\n");
            return false;
        }

        i = 0;
        t_stringListIter iterCompY = DigitalSignature.ECDSAPublicKeyCompYList().begin();
        while ( iterCompY != DigitalSignature.ECDSAPublicKeyCompYList().end() )
        {
            if ( i < (Ds.KeySize+31)/32 )
            {
                *(*iterCompY) = HexFormattedAscii(Ds.Ecdsa.ECDSAPublicKeyCompY[i]);
            }
            iterCompY++;
            i++;
        }

#if TOOLS_GUI == 1
        if (DownloaderInterface.ImageBuild.Mode() == 1 )
#else
        if (CommandLineParser.iOption == 1  )
#endif
        {
            if ( (pLine = CTimDescriptor::GetNextLineField( "ECDSA Private Key", pLine )) == 0 )
            {
                printf ("Key file parsing error reading, ECDSA Private Key:\n");
                return false;
            }

#if TOOLS_GUI == 1
            pLine->AddRef( &DigitalSignature.ECDSAPrivateKeyList() );
#endif
            if ( Ds.KeySize > 0 )
            {
                IsOk = FillDataArrays ( pLine, (unsigned long *)ECDSAPrivateKey, ((Ds.KeySize+31)/32*4) );
                if (IsOk == false)
                {
                    printf ("Key Size in bits = %d\n\n", Ds.KeySize);
                    printf ("  Error: The amount of data read from key file doesn't match what is\n");
                    printf ("indicated by 'Key Size in bits' directive!\n");
                    printf ("Error: ECDSA Private Key for DSA Algorithm\n");
                    printf ("////////////////////////////////////////////////////////////\n");
                    return false;
                }

                i = 0;
                t_stringListIter iterPrivKey = DigitalSignature.ECDSAPrivateKeyList().begin();
                while ( iterPrivKey != DigitalSignature.ECDSAPrivateKeyList().end() )
                {
                    if ( i < (Ds.KeySize+31)/32 )
                    {
                        *(*iterPrivKey) = HexFormattedAscii(ECDSAPrivateKey[i]);
                    }
                    iterPrivKey++;
                    i++;
                }
            }
        }
    }

    return true;
}

#if TOOLS_GUI == 1
bool CTrustedTimDescriptorParser::ParsePrivateKeyFile (CDownloaderInterface& DownloaderInterface, CCommandLineParser& CommandLineParser)
#else
bool CTrustedTimDescriptorParser::ParsePrivateKeyFile (CCommandLineParser& CommandLineParser)
#endif
{
    bool IsOk = false;
    UINT_T IntParam = 0;
    char szTemp[100] = {0};
    unsigned int i = 0;
    string sLine;
    bool bFound = false;

    memset (&Ds,0,sizeof(Ds));

    ifstream m_ifsPrivateKeyTxtFile;
    m_ifsPrivateKeyTxtFile.open( CommandLineParser.KeyFileName.c_str(), ios_base::in );
    if ( m_ifsPrivateKeyTxtFile.bad() || m_ifsPrivateKeyTxtFile.fail() )
        return false;

    if (!GetDWord (m_ifsPrivateKeyTxtFile, "DSA Algorithm ID",&IntParam)) return false;
    Ds.DSAlgorithmID = (ENCRYPTALGORITHMID_T)IntParam;

    if (!GetDWord (m_ifsPrivateKeyTxtFile, "Hash Algorithm ID",&IntParam)) return false;
    Ds.HashAlgorithmID = (HASHALGORITHMID_T)IntParam;

    if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
    {
        if (!GetDWord (m_ifsPrivateKeyTxtFile, "Modulus Size in bytes",&IntParam)) return false;
        Ds.KeySize = IntParam*8;
    }
    else
    {
        if (!GetDWord (m_ifsPrivateKeyTxtFile, "Key Size in bits",&IntParam)) return false;
        Ds.KeySize = IntParam;
    }

    CDigitalSignature& DigitalSignature = m_TimDescriptor.DigitalSignature();
    
    bool bVer0303 = ( m_TimDescriptor.TimHeader().VersionBind.Version >= TIM_3_3_00 );
    bool bEC = ( Ds.DSAlgorithmID == ECDSA_256 || Ds.DSAlgorithmID == ECDSA_521 );

    if ( CommandLineParser.iOption == 1 )
    {
        if ( bVer0303 && bEC )
        {
            if (!GetDWord (m_ifsPrivateKeyTxtFile, "ECDSA Public Key CompX",0) )
            {
                printf ("  Error: Key file parsing error reading, ECDSA Public Key CompX:\n");
                return false;
            }
        }
        else
        {
            if (!GetDWord (m_ifsPrivateKeyTxtFile, "RSA Public Exponent",0) )
            {
                printf ("  Error: Key file parsing error reading, RSA Public Exponent:\n");
                return false;
            }
        }

        if ( Ds.KeySize > 0 )
        {
            if ( bEC )
                IsOk = FillDataArrays ( m_ifsPrivateKeyTxtFile,(unsigned long *)Ds.Ecdsa.ECDSAPublicKeyCompX, (Ds.KeySize+7)/8 );
            else
                IsOk = FillDataArrays ( m_ifsPrivateKeyTxtFile,(unsigned long *)Ds.Rsa.RSAPublicExponent, Ds.KeySize/8 );

            if (IsOk == false)
            {
                if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
                {
                    printf ("Modulus Size in bytes = %d\n\n", (Ds.KeySize+7)/8);
                }
                else
                {
                    printf ("Key Size in bits = %d\n\n",Ds.KeySize);
                }
                printf ("  Error: The amount of data read from key file doesn't match what is\n");
                if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
                {
                    printf ("indicated by 'Modulus Size in bytes' directive!\n");
                    printf ("Error: RSA Public Exponent for DSA Algorithm\n");
                }
                else
                {
                    printf ("indicated by 'Key Size in bits' directive!\n");
                    printf ("Error: ECDSA Public Key CompX for DSA Algorithm\n");
                }
                printf ("////////////////////////////////////////////////////////////\n");
                return false;
            }
        }
        DigitalSignature.IncludeInTim( Ds.KeySize > 0 ? true : false );
        DigitalSignature.EncryptAlgorithmId( Ds.DSAlgorithmID );
    
        // recreate keys of correct size
        DigitalSignature.KeySize( Ds.KeySize );
        if ( bEC )
        {
            t_stringListIter iterCompX = DigitalSignature.ECDSAPublicKeyCompXList().begin();
            while ( iterCompX != DigitalSignature.ECDSAPublicKeyCompXList().end() )
            {
                if ( i < (Ds.KeySize+31)/32 )
                {
                    *(*iterCompX) = HexFormattedAscii(Ds.Ecdsa.ECDSAPublicKeyCompX[i]);
                }
                iterCompX++;
                i++;
            }
        }
        else
        {
            t_stringListIter iterPubExp = DigitalSignature.PublicKeyExponentList().begin();
            while ( iterPubExp != DigitalSignature.PublicKeyExponentList().end() )
            {
                if ( i < (Ds.KeySize+31)/32 )
                {
                    *(*iterPubExp) = HexFormattedAscii(Ds.Rsa.RSAPublicExponent[i]);
                }
                iterPubExp++;
                i++;
            }
        }
    }

    if ( bVer0303 && bEC )
    {
        if (!GetDWord (m_ifsPrivateKeyTxtFile, "ECDSA Public Key CompY",0) )
        {
            printf ("  Error: Key file parsing error reading, ECDSA Public Key CompY:\n");
            return false;
        }
    }
    else
    {
        if (!GetDWord (m_ifsPrivateKeyTxtFile, "RSA System Modulus",0) )
        {
            printf ("  Error: Key file parsing error reading, RSA System Modulus:\n");
            return false;
        }
    }

    if ( Ds.KeySize > 0 )
    {
        if ( bEC )
            IsOk = FillDataArrays ( m_ifsPrivateKeyTxtFile,(unsigned long *)Ds.Ecdsa.ECDSAPublicKeyCompY, (Ds.KeySize+7)/8 );
        else
            IsOk = FillDataArrays ( m_ifsPrivateKeyTxtFile,(unsigned long *)Ds.Rsa.RSAModulus, Ds.KeySize/8 );

        if (IsOk == false)
        {
            if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
            {
                printf ("Modulus Size in bytes = %d\n\n", (Ds.KeySize+7)/8 );
            }
            else
            {
                printf ("Key Size in bits = %d\n\n",Ds.KeySize);
            }
            printf ("  Error: The amount of data read from key file doesn't match what is\n");
            if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
            {
                printf ("indicated by 'Modulus Size in bytes' directive!\n");
                printf ("Error: RSA System Modulus for DSA Algorithm\n");
            }
            else
            {
                printf ("indicated by 'Key Size in bits' directive!\n");
                printf ("Error: RSA or ECDSA Public Key CompY for DSA Algorithm\n");
            }
            printf ("////////////////////////////////////////////////////////////\n");
            return false;
        }

        i = 0;

        if ( bEC )
        {
            t_stringListIter iterCompY = DigitalSignature.ECDSAPublicKeyCompYList().begin();
            while ( iterCompY != DigitalSignature.ECDSAPublicKeyCompYList().end() )
            {
                if ( i < (Ds.KeySize+31)/32 )
                {
                    *(*iterCompY) = HexFormattedAscii(Ds.Ecdsa.ECDSAPublicKeyCompY[i]);
                }
                iterCompY++;
                i++;
            }
        }
        else
        {
            t_stringListIter iterRsaMod = DigitalSignature.RsaSystemModulusList().begin();
            while ( iterRsaMod != DigitalSignature.RsaSystemModulusList().end() )
            {
                if ( i < Ds.KeySize/32 )
                {
                    *(*iterRsaMod) = HexFormattedAscii(Ds.Rsa.RSAModulus[i]);
                }
                iterRsaMod++;
                i++;
            }
        }

#if TOOLS_GUI == 1
        if (DownloaderInterface.ImageBuild.Mode() == 1 )
#else
        if (CommandLineParser.iOption == 1 || CommandLineParser.iOption == 3 )
#endif
        {
            if ( bVer0303 && bEC )
            {
                if (!GetDWord (m_ifsPrivateKeyTxtFile, "ECDSA Private Key",0) )
                {
                    printf ("Key file parsing error reading, ECDSA Private Key:\n");
                    return false;
                }
            }
            else
            {
                if (!GetDWord (m_ifsPrivateKeyTxtFile, "RSA Private Key",0) )
                {
                    printf ("Key file parsing error reading, RSA Private Key:\n");
                    return false;
                }
            }

            if ( Ds.KeySize > 0 )
            {
                if ( bEC )
                {
                    IsOk = FillDataArrays ( m_ifsPrivateKeyTxtFile,(unsigned long *)ECDSAPrivateKey, (Ds.KeySize+7)/8 );
                }
                else
                    IsOk = FillDataArrays ( m_ifsPrivateKeyTxtFile,(unsigned long *)RsaPrivateKey, Ds.KeySize/8 );
    
                if (IsOk == false)
                {
                    if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
                    {
                        printf ("Modulus Size in bytes = %d\n\n", (Ds.KeySize+7)/8 );
                    }
                    else
                    {
                        printf ("Key Size in bits = %d\n\n",Ds.KeySize);
                    }
                    printf ("  Error: The amount of data read from key file doesn't match what is\n");
                    if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
                    {
                        printf ("indicated by 'Modulus Size in bytes' directive!\n");
                        printf ("Error: RSA Private Key for DSA Algorithm\n");
                    }
                    else
                    {
                        printf ("indicated by 'Key Size in bits' directive!\n");
                        printf ("Error: RSA or ECDSA Private Key for DSA Algorithm\n");
                    }
                    printf ("////////////////////////////////////////////////////////////\n");
                    return false;
                }

                i = 0;
                if ( bEC )
                {
                    t_stringListIter iterECDSA = DigitalSignature.ECDSAPrivateKeyList().begin();
                    while ( iterECDSA != DigitalSignature.ECDSAPrivateKeyList().end() )
                    {
                        if ( i < (Ds.KeySize+31)/32 )
                        {
                            *(*iterECDSA) = HexFormattedAscii(Ds.Ecdsa.ECDSADigS_R[i]);
                        }
                        iterECDSA++;
                        i++;
                    }
                }
                else
                {
                    t_stringListIter iterPrivKey = DigitalSignature.RsaPrivateKeyList().begin();
                    while ( iterPrivKey != DigitalSignature.RsaPrivateKeyList().end() )
                    {
                        if ( i < Ds.KeySize/32 )
                        {
                            *(*iterPrivKey) = HexFormattedAscii(RsaPrivateKey[i]);
                        }
                        iterPrivKey++;
                        i++;
                    }
                }
            }
        }
    }

    return true;
}

#endif // TRUSTED
