/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "TimLib.h"
#include "ImageDescription.h"

#include <algorithm>
#include <cctype>
#include <functional>

#pragma warning ( disable : 4996 )

#if LINUX
#include <string.h>
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

CTimLib::CTimLib(void)
{
}

CTimLib::~CTimLib(void)
{
}

bool CTimLib::GetDWord (ifstream& ifs, char *szString, unsigned int* pValue)
{
    size_t nPos = 0;
    string sValue;

    if ( pValue != 0 )
        *pValue = 0;

    while ( ifs.good() && GetNextLine( ifs, sValue ) )
    {
        // if 1st char (after leading white space) is a ;, 
        //  then line is a comment so skip
        if ( sValue[0] != ';' )
            break;
    }

    if ( ifs.bad() || ifs.fail() )
        return false;

    if ( pValue == 0 )
    {
        if ( sValue != szString )
            return false;
    }
    else if ( nPos = sValue.find( szString ) == string::npos )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Warning: Declaration statement is missing or corrupted in descriptor file!\n");
        printf ("  <%s>\n", szString);
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }
    else
    {
        nPos += strlen( szString );
        sValue = sValue.substr( nPos );
        *pValue = Translate(sValue);
    }
        
    return true;
}

bool CTimLib::GetSValue (ifstream& ifs, const char *szString, string& sValue, bool bErrMsg )
{
    size_t nPos = 0;
    sValue = "";

    while ( ifs.good() && GetNextLine( ifs, sValue ) )
    {
        // if 1st char (after leading white space) is a ;, 
        //  then line is a comment so skip
        if ( sValue[0] != ';' )
            break;
    }

    if ( ifs.bad() || ifs.fail() )
        return false;

    if ( nPos = sValue.find( szString ) == string::npos )
    {
        if ( bErrMsg )
        {
            printf ("////////////////////////////////////////////////////////////\n");
            printf ("  Warning: Declaration statement is missing or corrupted in descriptor file!\n");
            printf ("  <%s>\n", szString);
            printf ("////////////////////////////////////////////////////////////\n");
        }
        return false;
    }
    nPos += strlen( szString );

    // get the value following the szString
    sValue = sValue.substr( nPos );

    sValue = TrimWS( sValue );

    return true;
}

bool CTimLib::GetFieldValue (string& sLine, string& sField, DWORD& dwValue)
{
    size_t nPos = 0;
    string sValue = "";
    dwValue = 0;

    // if 1st char (after leading white space) is a ;, 
    //  then line is a comment so skip
    if ( sLine[0] == ';' )
        return false;

    if ( sLine.find( sField ) == string::npos )
        return false;

    if ( string::npos == ( nPos = sLine.find_first_not_of( ": \n\t", sField.length() ) ) )
        return false;
    else
        sValue = sLine.substr(nPos);
    
    if ( sValue.length() > 0 )
    {
        dwValue = Translate (sValue); 
        return true;
    }

    return false;
}

bool CTimLib::GetFieldValue (string& sLine, string& sField, string& sValue)
{
    size_t nPos = 0;
    sValue = "";

    // if 1st char (after leading white space) is a ;, 
    //  then line is a comment so skip
    if ( sLine[0] == ';' )
        return false;

    if ( sLine.find( sField ) == string::npos )
        return false;

    if ( string::npos == ( nPos = sLine.find_first_not_of( ": \n\t", sField.length() ) ) )
        return false;
    else
        sValue = sLine.substr(nPos);
    
    sValue = TrimWS( sValue );

    if ( sValue.length() > 0 )
        return true;

    return false;
}

bool CTimLib::CheckGetSValue (ifstream& ifs, char *szString, string& sValue)
{
    // same functionality as GetSValue but with no error warning if match not found

    size_t nPos = 0;
    sValue = "";

    while ( ifs.good() && GetNextLine( ifs, sValue ) )
    {
        // if 1st char (after leading white space) is a ;, 
        //  then line is a comment so skip
        if ( sValue[0] != ';' )
            break;
    }

    if ( ifs.bad() || ifs.fail() )
        return false;

    if ( nPos = sValue.find( szString ) == string::npos )
    {
//		printf ("////////////////////////////////////////////////////////////\n");
//		printf ("  Warning: Declaration statement is missing or corrupted in descriptor file!\n");
//		printf ("  <%s>\n", szString);
//		printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }
    nPos += strlen( szString );

    // get the value following the szString
    sValue = sValue.substr( nPos );

    sValue = TrimWS( sValue );

    return true;
}

unsigned int CTimLib::Translate (const char* pValue)
{
    unsigned int iRetVal = 0;
#if LINUX
    if (pValue[0]== '0' && pValue[1]== 'x')
        sscanf( pValue, "%x", &iRetVal );
    else
        sscanf( pValue, "%i", &iRetVal );
#else
    sscanf( pValue, "%i", &iRetVal );
#endif
    return iRetVal;
}

unsigned int CTimLib::Translate (string sValue)
{
    unsigned int iRetVal = 0;

    sValue = TrimWS( sValue );

#if LINUX
    if ( sValue.find("0x")!=string::npos || sValue.find("0X")!=string::npos)
    {
        sscanf( sValue.c_str(), "%x", &iRetVal );
    }
    else
        sscanf( sValue.c_str(), "%i", &iRetVal );
#else
    sscanf( sValue.c_str(), "%i", &iRetVal );
#endif
    return iRetVal;
}

void CTimLib::Endian_Convert (UINT_T x,UINT_T *y)
{
    UINT_T a0, a1, a2, a3;

    a0 = x & 0xFF;
    a1 = (x & 0xFF00) >> 8;
    a2 = (x & 0xFF0000) >> 16;
    a3 = (x & 0xFF000000) >> 24;
//	*y = (a0 << 24) + (a1 << 16) + (a2 << 8) + a3;
    *y = (a0 << 24) | (a1 << 16) | (a2 << 8) | a3;
}

UINT32_T CTimLib::CheckSum (unsigned char *Address, UINT_T length)
{
    UINT32_T Key = 0x04C11DB7;
    UINT32_T Val = 0xFFFFFFFF;
    UINT_T i = 0, t = 0;

    // This routine is used when building Non Trusted images where a
    // check sum is generated from an entire image or buffer of data.

    while (i < length)
    {
        if (((( Val >> 31 ) & 0x1 ) ^ ((Address[i] >> t) & 0x1)) == 1)
            Val = ( Val << 1) ^ Key;
        else
            Val = Val << 1;

        if (t == 7)
        {
            t = 0;
            i++;
        }
        else t++;
    }

    return Val;
}


bool CTimLib::CreateOutputImageName (string& sImageFilename, string& sImageOutFilename)
{
#if TOOLS_GUI == 1
    char path_buffer[_MAX_PATH]={0};
    char drive[_MAX_DRIVE]={0};
    char dir[_MAX_DIR]={0};
    char fname[_MAX_FNAME]={0};
    char ext[_MAX_EXT]={0};
    errno_t err;

    strcpy_s(path_buffer,MAX_PATH, sImageFilename.c_str());
    err = _splitpath_s( path_buffer, drive, _MAX_DRIVE, dir, _MAX_DIR, fname,
                        _MAX_FNAME, ext, _MAX_EXT );
    if (err != 0)
    {
        printf("Error splitting the path. Error code %d.\n", err);
        return false;
    }

    sImageOutFilename = drive;
    sImageOutFilename += dir;
    sImageOutFilename += fname;
    sImageOutFilename += "_h";
    if ( strlen(ext) == 0 )
        sImageOutFilename += ".bin";
    else
        sImageOutFilename += ext;

    return true;

#else

    string sTemp( sImageFilename );
    string::size_type slashpos = sImageFilename.find_last_of("\\");
    string::size_type dotpos = sImageFilename.find_last_of(".");
    if ( dotpos == string::npos )
    {
        // no ext
        sTemp += "_h.bin";
        sImageOutFilename = sTemp;
    }
    else
    {
        if ( (slashpos == string::npos) || (slashpos < dotpos) )
            sTemp.resize( dotpos );
        sTemp += "_h";
        sTemp += sImageFilename.substr( dotpos );
        sImageOutFilename = sTemp;
    }

    return true;

#endif
}

bool CTimLib::CreateOutputTimBinImageName (string& sImageFilename, string& sImageOutFilename)
{
#if TOOLS_GUI == 1
    char path_buffer[_MAX_PATH]={0};
    char drive[_MAX_DRIVE]={0};
    char dir[_MAX_DIR]={0};
    char fname[_MAX_FNAME]={0};
    char ext[_MAX_EXT]={0};
    errno_t err;

    strcpy_s(path_buffer,MAX_PATH, sImageFilename.c_str());
    err = _splitpath_s( path_buffer, drive, _MAX_DRIVE, dir, _MAX_DIR, fname,
                        _MAX_FNAME, ext, _MAX_EXT );
    if (err != 0)
    {
        printf("Error splitting the path. Error code %d.\n", err);
        return false;
    }

    sImageOutFilename = drive;
    sImageOutFilename += dir;
    sImageOutFilename += fname;
    if ( strlen(ext) == 0 )
        sImageOutFilename += ".bin";
    else
        sImageOutFilename += ext;

    return true;

#else

    string sTemp( sImageFilename );
    string::size_type slashpos = sImageFilename.find_last_of("\\");
    string::size_type dotpos = sImageFilename.find_last_of(".");
    if ( dotpos == string::npos )
    {
        // no ext
        sTemp += ".bin";
        sImageOutFilename = sTemp;
    }
    else
    {
        if ( slashpos < dotpos )
            sTemp.resize( dotpos );
        sTemp += sImageFilename.substr( dotpos );
        sImageOutFilename = sTemp;
    }
    return true;

#endif
}

bool CTimLib::CheckImageOverlap(t_ImagesList& Images, unsigned int BootFlashSign)
{
    unsigned int i = 0, j = 0;
    unsigned int s1 = 0, s2 = 0, e1 = 0, e2 = 0;//starting and ending addresses of the two images being compared

    t_ImagesIter iter = Images.begin();
    while( iter != Images.end() )
    { 
        t_ImagesIter innerIter = Images.begin();
        while( innerIter != Images.end() )
        {
            if ( innerIter != iter )
            {
                if ( (*iter)->PartitionNumber() == (*innerIter)->PartitionNumber() )
                {
                    s1 = Translate((*iter)->FlashEntryAddress());
                    s2 = Translate((*innerIter)->FlashEntryAddress());

                    e1 = s1 + (*iter)->ImageSize();
                    e2 = s2 + (*innerIter)->ImageSize();
                    if (s1 > s2)//if image 2 is at a lower location in flash
                    {
                        if(e2 > s1)//make sure the end of image 2 is also at a lower location in flash
                        {
                            printf("Error: Images %s and %s overlap in flash!\n", (*iter)->ImageIdTag().c_str(), (*innerIter)->ImageIdTag().c_str());
                            //printf("Bad: i1 %d i2 %d s1 %d s2 %d e1 %d e2 %d", i, j, s1, s2, e1, e2);
                            return false;
                        }
                    }
                    else		//if image 1 is at a lower location in flash
                    {
                        if(e1 > s2)//make sure the end of image 1 is also at a lower location in flash
                        {
                            printf("Error: Images %s and %s overlap in flash!\n", (*iter)->ImageIdTag().c_str(), (*innerIter)->ImageIdTag().c_str());
                            //printf("Bad: i1 %d i2 %d s1 %d s2 %d e1 %d e2 %d", i, j, s1, s2, e1, e2);
                            return false;
                        }
                    }
                }
            }
            innerIter++;
        }
        iter++;
    }
    return true;
}

bool CTimLib::GetQWord (ifstream& ifs, char *szString, UINT64 *pValue)
{
    size_t nPos = 0;
    string sValue;

    if ( pValue != 0 )
        *pValue = 0;

    while ( ifs.good() && GetNextLine( ifs, sValue ) )
    {
        // if 1st char (after leading white space) is a ;, 
        //  then line is a comment so skip
        if ( sValue[0] != ';' )
            break;
    }

    if ( ifs.bad() || ifs.fail() )
        return false;

    if ( pValue == 0 )
    {
        if ( sValue != szString )
            return false;
    }
    else if ( nPos = sValue.find( szString ) == string::npos )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("  Warning: Declaration statement is missing or corrupted in descriptor file!\n");
        printf ("  <%s>\n", szString);
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }
    else
    {
        nPos += strlen( szString );
        sValue = sValue.substr( nPos );
        *pValue = TranslateQWord(sValue.c_str());
    }

    return true;
}

unsigned long long CTimLib::TranslateQWord (const char *pValue)
{
    // This function converts a string to a number.
    unsigned long long result = 0;

#if WIN32
    sscanf( pValue, "%I64i", &result );
    return result;
#endif

#if LINUX
    int i;
    char *end = NULL;

    // If pValue contains "0x" then treat it as a hexidecimal value
    // otherwise treat it as a regular integer.

    //printf("***************%s",pValue);
    if (strstr (pValue,"0x") == NULL)
    { 
        return (unsigned long long) strtoul(pValue,&end,18);
    }
    else if (strstr (pValue,"0X") == NULL)
    { 
        return (unsigned long long) strtoul(pValue,&end,18);
    }
    else
    {
        for (i = 2; i < 18 && pValue[i] != '\0'; i++)
        {
//			printf("result h: %X\n", *(unsigned int*)(((unsigned int)&result) + 4));
//			printf("result l: %X\n", *(unsigned int*)&result);
            result = (result << 4);

            if (pValue[i] >= 'a' && pValue[i] <= 'f') 
                result += (unsigned long long)(pValue[i] - 'a' + 0xA);
            else if (pValue[i] >= 'A' && pValue[i] <= 'F') 
                result += (unsigned long long)(pValue[i] - 'A' + 0xA);
            else if (pValue[i] >= '0' && pValue[i] <= '9') 
                result += (unsigned long long)(pValue[i] - '0');
            else if (pValue[i] == ' ') 
                return (result >> 4);
            else return 0;
        }
    }
    return result;
#endif
}


void CTimLib::PrependPathIfNone( string& sFilePath )
{
#if TOOLS_GUI == 1
    // prepend current directory if no path on file
    char szCurDir[ _MAX_DIR ] = {0};
    GetCurrentDirectory( _MAX_DIR, szCurDir );
    char path_buffer[_MAX_PATH]={0};
    char filename[_MAX_DIR]={0};
    char drive[_MAX_DIR]={0};
    char dir[_MAX_DIR]={0};
    char ext[_MAX_DIR]={0};
    errno_t err;
    strcpy(path_buffer,sFilePath.c_str());
    err = _splitpath_s ( path_buffer, drive, _MAX_DRIVE, dir, _MAX_DIR, 0, 0, 0, 0 );// filename, ext );
//	_get_errno( &err );
    if (err != 0 && err != ENOENT)
    {
      printf("Error splitting the path. Error code %d.\n", err);
    }
    string sTemp( drive );
    sTemp += dir;
    if ( sTemp.length() == 0 )
    {
        sTemp += szCurDir;
        sTemp += "\\";
        sTemp += sFilePath;
        sFilePath = sTemp;
    }
#endif

    return;
}

string& CTimLib::HexFormattedAscii( unsigned int iNum )
{
    static string sRet;
    sRet = "0x"; // instead of using showbase because we want 0x in lowercase
                 // but the hex alpha in uppercase

    // create a string for hex formatted ASCII string creation
    stringstream ss;
    ss << hex << setw( 8 ) << uppercase << setfill('0') << iNum; 
    sRet += ss.str();
    return sRet;
}

void CTimLib::TextToHexFormattedAscii( string& sRet, string& sText )
{
    // only converts the first 4 bytes of sText to a HexFormattedAscii string
    // turn first 4 ascii chars of Id into a hex encoded ascii tag
    string sIdReversed; 
    size_t i = min((int)sText.length(),4);
    for ( ; i > 0; i-- )
        sIdReversed += sText[i-1];
    while( sIdReversed.length() < 4 ) sIdReversed += '\0'; // nulls reserve bytes for int* dereference
        
    sRet = HexFormattedAscii(*((int*)sIdReversed.c_str()));
}

string& CTimLib::HexFormattedAscii64( UINT64 iNum )
{
    static string sRet;
    sRet = "0x"; // instead of using showbase because we want 0x in lowercase
                 // but the hex alpha in uppercase

    // create a string for hex formatted ASCII string creation
    stringstream ss;
    ss << hex << setw( 16 ) << uppercase << setfill('0') << iNum; 
    sRet += ss.str();
    return sRet;
}


string CTimLib::QuotedFilePath( string& sFilePath )
{
    string sTemp(sFilePath);
    if ( sFilePath.length() > 0 )
    {
        // is there a space in path?
        if ( sFilePath.find(" ") != string::npos )
        {
            // is path already quoted?
            if ( sFilePath.find("\"") == string::npos )
            {
                sTemp = "\"" + sFilePath;
                sTemp += "\"";
            }
        }
    }
    return sTemp;
}

string CTimLib::HexAsciiToText( const string& sHexAscii )
{
    string sText = "0x0";

    if ( (sHexAscii.find("0x") == 0) || (sHexAscii.find("0X") == 0) )
    {
        char text[5]={0};
        unsigned int uHex = Translate( sHexAscii.c_str() );
        if ( uHex != 0 )
        {
            text[0] = ((char)((uHex & 0xFF000000)>>24));
            text[1] = ((char)((uHex & 0x00FF0000)>>16));
            text[2] = ((char)((uHex & 0x0000FF00)>>8));
            text[3] = ((char)((uHex & 0x000000FF)));
            sText = text;
        }

        // if result produced invalid characters, then reset to original
        if ( !IsAlphaNumeric( sText ) )
            sText = sHexAscii;
    }

    return sText;
}

string CTimLib::ToUpper( const string& instring, bool bHexNum )
{
    string upper;
    if ( instring.size() > 0 )
    {
        transform( instring.begin(), instring.end(), back_inserter( upper ), ptr_fun<int,int>( toupper ));

        // special handling for hex numbers
        if ( bHexNum )
        {
            if ( upper[0] == '0' && upper[1] == 'X' )
                upper[1] = 'x';
        }
    }
    return upper;
}

string CTimLib::ToLower( const string& instring, bool bHexNum )
{
    string lower;
    transform( instring.begin(), instring.end(), back_inserter( lower ), ptr_fun<int,int>( tolower ));

    // special handling for hex numbers
    if ( bHexNum )
    {
        if ( lower[0] == '0' && lower[1] == 'X' )
            lower[1] = 'x';
    }
    return lower;
}

bool CTimLib::IsAlpha( const string& instring )
{
    return (instring.find_first_not_of("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz") == string::npos );
}

bool CTimLib::IsAlphaNumeric( const string& instring )
{
    return (instring.find_first_not_of("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz") == string::npos );
}

ifstream& CTimLib::GetNextLineNoWSSkipComments( ifstream& ifs, string& sLine )
{	
    while ( ifs.good() && GetNextLine( ifs, sLine ) )
    {
        // if 1st char (after leading white space) is a ;, 
        //  then line is a comment so skip
        if ( sLine[0] == ';' )
            continue;

        // parse out post comment
       	size_t nPos = 0;
    	if ( (nPos = sLine.find( ';' )) != string::npos )
	    {
		    // include ws between field and post comment
		    while( nPos-1 > 0 )
		    {
			    if ( !isspace(0x000000FF & sLine[nPos-1]) )
				    break;
			    nPos--;
		    }

    		sLine = sLine.substr(0, nPos);
	    }
        
        if ( sLine.length() == 0 )
            continue;
        else 
            break;
    }
    return ifs;
}

ifstream& CTimLib::GetNextLine( ifstream& ifs, string& sLine, bool bIncludeWS )
{	
    // makes sure we only get a new data
    while(ifs.good() && !ifs.eof() && !ifs.bad() )
    {
#if LINUX
        //TBD THIS SECTION IS TO KEEP LINUX COMPILER HAPPY
        //{{
        char szStr[1024] = {0};
        ifs.getline( szStr, 1023, '\n');
        sLine = szStr;
        //}}
        // THE ABOVE SECTION SHOULD BE REPLACED WITH THIS AFTER LINUX COMPILER IS FIXED
#else
        ::getline(ifs, sLine, '\n');
#endif
        if ( !bIncludeWS )
        {
            // remove leading whitespace
            while ( sLine.length() > 0 && isspace(0x000000FF & sLine[0]) )
                sLine.erase(0,1);

            // remove any trailing whitespace
            while ( sLine.length() > 0 && isspace(0x000000FF & sLine[sLine.length()-1]) )
                sLine.erase(sLine.length()-1,1);

            if ( sLine.length() > 0 )
                break;
        }
        else
            // include ws so return even if it's a blank line
            break;
    }
    return ifs;
}


#if TOOLS_GUI == 1
bool CTimLib::SaveFieldState( stringstream& ss, string& sField )
{
    // save the field to the project file
    ss << sField << endl;

    // save associated CTimDescriptorLine if any 
    CTimDescriptorLine* pLine = 0;
    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &sField )) != 0 )
    {
        ss << true << endl;
        return pLine->SaveState( ss );
    }
    else
        ss << false << endl;

    return true;
}

bool CTimLib::SaveFieldState( stringstream& ss, unsigned int& iField )
{
    // save the field to the project file
    ss << iField << endl;

    // save associated CTimDescriptorLine if any 
    CTimDescriptorLine* pLine = 0;
    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &iField )) != 0 )
    {
        ss << true << endl;
        return pLine->SaveState( ss );
    }
    else
        ss << false << endl;

    return true;
}

bool CTimLib::SaveFieldState( stringstream& ss, pair<unsigned int, unsigned int>& rPair )
{
    // save the field to the project file
    ss << rPair.first << endl;
    ss << rPair.second << endl;

    // save associated CTimDescriptorLine if any 
    CTimDescriptorLine* pLine = 0;
    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &rPair )) != 0 )
    {
        ss << true << endl;
        return pLine->SaveState( ss );
    }
    else
        ss << false << endl;

    return true;
}

bool CTimLib::LoadFieldState( ifstream& ifs, string& sField )
{
    ::getline( ifs, sField );
    if ( theApp.ProjectVersion >= 0x03021400 )
    {
        string sbuf;
        ::getline( ifs, sbuf ); 
        if ( Translate(sbuf) == 1 )
        {
            CTimDescriptorLine* pLine = new CTimDescriptorLine;
            pLine->LoadState( ifs );
            CTimDescriptor::g_TimDescriptorLines.push_back( pLine );
            pLine->AddRef( &sField );
        }
    }

    return ifs.good() && !ifs.fail();
}

bool CTimLib::SaveFieldStatePtr( stringstream& ss, void* pObj )
{
    // save the field to the project file
    ss << pObj << endl;

    // save associated CTimDescriptorLine if any 
    CTimDescriptorLine* pLine = 0;
    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, pObj )) != 0 )
    {
        ss << true << endl;
        return pLine->SaveState( ss );
    }
    else
        ss << false << endl;

    return true;
}

bool CTimLib::LoadFieldStatePtr( ifstream& ifs, void* pObj )
{
    string sbuf;
    
    if ( theApp.ProjectVersion >= 0x03021400 )
    {
        // get's the saved This pointer and ignore it
        ::getline( ifs, sbuf );

        ::getline( ifs, sbuf ); 
        if ( Translate(sbuf) == 1 )
        {
            CTimDescriptorLine* pLine = new CTimDescriptorLine;
            pLine->LoadState( ifs );
            CTimDescriptor::g_TimDescriptorLines.push_back( pLine );
            pLine->AddRef( pObj );
        }
    }
    else 
        return false;

    return ifs.good() && !ifs.fail();
}

bool CTimLib::LoadFieldState( ifstream& ifs, unsigned int& iField )
{
    string sbuf;
    
    ::getline( ifs, sbuf );
    iField = Translate(sbuf);

    if ( theApp.ProjectVersion >= 0x03021400 )
    {
        ::getline( ifs, sbuf ); 
        if ( Translate(sbuf) == 1 )
        {
            CTimDescriptorLine* pLine = new CTimDescriptorLine;
            pLine->LoadState( ifs );
            CTimDescriptor::g_TimDescriptorLines.push_back( pLine );
            pLine->AddRef( &iField );
        }
    }

    return ifs.good() && !ifs.fail();
}


bool CTimLib::LoadFieldState( ifstream& ifs, pair<unsigned int, unsigned int>& rPair )
{
    string sbuf;
    
    ::getline( ifs, sbuf );
    rPair.first = Translate(sbuf);

    ::getline( ifs, sbuf );
    rPair.second = Translate(sbuf);

    if ( theApp.ProjectVersion >= 0x03021400 )
    {
        ::getline( ifs, sbuf ); 
        if ( Translate(sbuf) == 1 )
        {
            CTimDescriptorLine* pLine = new CTimDescriptorLine;
            pLine->LoadState( ifs );
            CTimDescriptor::g_TimDescriptorLines.push_back( pLine );
            pLine->AddRef( &rPair );
        }
    }

    return ifs.good() && !ifs.fail();
}

#endif

string CTimLib::TrimWS( string& sValue )
{
    string sTLWS = TrimLeadingWS( sValue );
    return TrimTrailingWS( sTLWS );
}

string CTimLib::TrimLeadingWS( string& sValue )
{
    string sNoLeadingWs = sValue;
    // remove leading whitespace
    while ( sNoLeadingWs.length() > 0 && isspace(0x000000FF & sNoLeadingWs[0]) )
        sNoLeadingWs.erase(0,1);

    return sNoLeadingWs;
}

string CTimLib::TrimTrailingWS( string& sValue )
{
    string sNoTrailingWs = sValue;
    // remove any trailing whitespace
    while ( sNoTrailingWs.length() > 0 && isspace(0x000000FF & sNoTrailingWs[sNoTrailingWs.length()-1]) )
        sNoTrailingWs.erase(sNoTrailingWs.length()-1,1);

    return sNoTrailingWs;
}
