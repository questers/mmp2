/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "ReservedPackageData.h"
#include "TimDescriptor.h"

#if EXTENDED_RESERVED_DATA
#include "ExtendedReservedData.h"
#endif

CReservedPackageData::CReservedPackageData(void)
: CTimLib()
{
    m_sPackageId = "";
    m_sPackageIdTag = "0x00000000";
    m_bChanged = false;
}

CReservedPackageData::~CReservedPackageData(void)
{
    DiscardAll();
}

CReservedPackageData::CReservedPackageData( const CReservedPackageData& rhs )
: CTimLib(rhs)
{
    // copy constructor
    CReservedPackageData& nc_rhs = const_cast<CReservedPackageData&>(rhs);

#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sPackageId )) != 0 )
        pLine->AddRef( &m_sPackageId );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sPackageIdTag )) != 0 )
        pLine->AddRef( &m_sPackageIdTag );
#endif

    m_sPackageId = rhs.m_sPackageId;
    m_sPackageIdTag = rhs.m_sPackageIdTag;
    m_bChanged = rhs.m_bChanged;
    
    // need to do a deep copy of lists to avoid dangling references
    t_stringListIter iterData = nc_rhs.m_PackageDataList.begin();
    while( iterData != nc_rhs.m_PackageDataList.end() )
    {
        string* psData = new string( *(*iterData ) );
        m_PackageDataList.push_back( psData );
#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, (*iterData ) )) != 0 )
            pLine->AddRef( psData );
#endif
        iterData++;
    }

    t_stringListIter iterComment = nc_rhs.m_PackageCommentList.begin();
    while( iterComment != nc_rhs.m_PackageCommentList.end() )
    {
        string* psData = new string( *(*iterComment ) );
        m_PackageCommentList.push_back( psData );
#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, (*iterComment ) )) != 0 )
            pLine->AddRef( psData );
#endif
        iterComment++;
    }
}

CReservedPackageData& CReservedPackageData::operator=( const CReservedPackageData& rhs ) 
{
    // assignment operator
    if ( &rhs != this )
    {
        CTimLib::operator=(rhs);

        CReservedPackageData& nc_rhs = const_cast<CReservedPackageData&>(rhs);

        // delete the existing list and recreate a new one
        DiscardAll();

#if TOOLS_GUI == 1
        CTimDescriptorLine* pLine = 0;

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sPackageId )) != 0 )
            pLine->AddRef( &m_sPackageId );

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sPackageIdTag )) != 0 )
            pLine->AddRef( &m_sPackageIdTag );
#endif

        m_sPackageId = rhs.m_sPackageId;
        m_sPackageIdTag = rhs.m_sPackageIdTag;
        m_bChanged = rhs.m_bChanged;

        // need to do a deep copy of lists to avoid dangling references
        t_stringListIter iterData = nc_rhs.m_PackageDataList.begin();
        while( iterData != nc_rhs.m_PackageDataList.end() )
        {
            string* psData = new string( *(*iterData ) );
            m_PackageDataList.push_back( psData );
#if TOOLS_GUI == 1
            // update the object line reference
            if ( (pLine = CTimDescriptor::GetLineField( "", false, (*iterData ) )) != 0 )
                pLine->AddRef( psData );
#endif
            iterData++;
        }

        t_stringListIter iterComment = nc_rhs.m_PackageCommentList.begin();
        while( iterComment != nc_rhs.m_PackageCommentList.end() )
        {
            string* psData = new string( *(*iterComment ) );
            m_PackageCommentList.push_back( psData );
#if TOOLS_GUI == 1
            // update the object line reference
            if ( (pLine = CTimDescriptor::GetLineField( "", false, (*iterComment ) )) != 0 )
                pLine->AddRef( psData );
#endif
            iterComment++;
        }
    }
    return *this;
}


void CReservedPackageData::DiscardAll()
{
#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_sPackageId )) != 0 )
        pLine->RemoveRef( &m_sPackageId );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_sPackageIdTag )) != 0 )
        pLine->RemoveRef( &m_sPackageIdTag );
#endif

    DiscardDataAndComments();
}

void CReservedPackageData::DiscardDataAndComments()
{
#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;
#endif

    // delete all package data strings
    t_stringListIter iterData = m_PackageDataList.begin();
    while( iterData != m_PackageDataList.end() )
    {
#if TOOLS_GUI == 1
        if ( (pLine = CTimDescriptor::GetLineField( "", false, (*iterData ) )) != 0 )
            pLine->RemoveRef( (*iterData) );
#endif
        delete *iterData;
        iterData++;
    }
    m_PackageDataList.clear();

    // delete all package data strings
    t_stringListIter iterComment = m_PackageCommentList.begin();
    while( iterComment != m_PackageCommentList.end() )
    {
#if TOOLS_GUI == 1
        if ( (pLine = CTimDescriptor::GetLineField( "", false, (*iterComment ) )) != 0 )
            pLine->RemoveRef( (*iterComment) );
#endif
        delete *iterComment;
        iterComment++;
    }
    m_PackageCommentList.clear();
}

int CReservedPackageData::Size()
{
    return (int)(m_PackageDataList.size()*4) + 8; // +4 (PID) add packageid & package size
}

void CReservedPackageData::AddData( string* psData, string *psComment )
{
    m_PackageDataList.push_back( psData );
    if ( *psComment == "" )
        PredefinedPackageComments( (int)m_PackageDataList.size(), psComment );

    m_PackageCommentList.push_back( psComment );
    m_bChanged = true;
}


void CReservedPackageData::DeleteData( string* psData )
{
#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;
    t_stringListIter iterData = m_PackageDataList.begin();
    t_stringListIter iterComment = m_PackageCommentList.begin();
    while( iterData != m_PackageDataList.end() )
    {
        if ( psData == (*iterData ) )
        {
            if ( (pLine = CTimDescriptor::GetLineField( "", false, (*iterData ) )) != 0 )
                pLine->RemoveRef( (*iterData) );
            delete (*iterData);
            m_PackageDataList.remove(*iterData);

            if ( (pLine = CTimDescriptor::GetLineField( "", false, (*iterComment ) )) != 0 )
                pLine->RemoveRef( (*iterComment) );
            delete (*iterComment);
            m_PackageCommentList.remove(*iterComment);

            break;
        }
        iterData++;
        iterComment++;
    }
    m_bChanged = true;
#endif
}


int CReservedPackageData::DataBlock( string& sDataBlock )
{
    sDataBlock = "";
    // tally payload size
    t_stringListIter iterData = m_PackageDataList.begin();
    while( iterData != m_PackageDataList.end() )
    {
        sDataBlock += (*iterData)->c_str();
        sDataBlock += " ";
        iterData++;
    }
    return Size();
}


#if TOOLS_GUI == 1
bool CReservedPackageData::SaveState( stringstream& ss )
{
    SaveFieldStatePtr( ss, this );

    SaveFieldState( ss, m_sPackageId );
    SaveFieldState( ss, m_sPackageIdTag );
    
    unsigned int temp = 0;
    temp = (unsigned int)m_PackageDataList.size();
    SaveFieldState( ss, temp );
    t_stringListIter iterData = m_PackageDataList.begin();
    while( iterData != m_PackageDataList.end() )
        SaveFieldState( ss, *(*iterData++) );

    temp = (unsigned int)m_PackageCommentList.size();
    SaveFieldState( ss, temp );
    t_stringListIter iterComment = m_PackageCommentList.begin();
    while( iterComment != m_PackageCommentList.end() )
        SaveFieldState( ss, *(*iterComment++) );

    return true; // SUCCESS
}

bool CReservedPackageData::LoadState( ifstream& ifs )
{
    string sbuf;

    DiscardAll();
    
    if ( theApp.ProjectVersion >= 0x03021400 )
        LoadFieldStatePtr( ifs, this ); 

    LoadFieldState( ifs, m_sPackageId ); 
    LoadFieldState( ifs, m_sPackageIdTag ); 

    LoadFieldState( ifs, sbuf ); 
    int iPackageListSize = Translate(sbuf);
    while ( iPackageListSize > 0 )
    {
        string* psData = new string;
        LoadFieldState( ifs, *psData ); 
        m_PackageDataList.push_back( psData );
        iPackageListSize--;
    }

    LoadFieldState( ifs, sbuf ); 
    int iCommentListSize = Translate(sbuf);
    while ( iCommentListSize > 0 )
    {
        string* psData = new string;
        LoadFieldState( ifs, *psData ); 
        m_PackageCommentList.push_back( psData );
        iCommentListSize--;
    }

    return ifs.good() && !ifs.fail(); // success
}
#endif

bool CReservedPackageData::IsChanged()
{
    return m_bChanged;
}

void CReservedPackageData::PackageIdTag( const string& sPackageIdTag )
{ 
    m_sPackageIdTag = ToUpper(sPackageIdTag, true);

    // update PackageId if a predefined PackageIdTag
    // this is necessary when parsing a descriptor file since only the tag
    // is stored in the descriptor file
    if ( Translate(m_sPackageIdTag) == AUTOBIND ) 
        m_sPackageId = "AUTOBIND";
    else if ( Translate(m_sPackageIdTag) == DDRID ) 
        m_sPackageId = "DDRID";
    else if ( Translate(m_sPackageIdTag) == GPIOID ) 
        m_sPackageId = "GPIOID";
    else if ( Translate(m_sPackageIdTag) == RESUMEBLID ) 
        m_sPackageId = "RESUMEBLID";
    else if ( Translate(m_sPackageIdTag) == TBR_XFER ) 
        m_sPackageId = "TBR_XFER";
    else if ( Translate(m_sPackageIdTag) == UARTID ) 
        m_sPackageId = "UARTID";
    else if ( Translate(m_sPackageIdTag) == USBID ) 
        m_sPackageId = "USBID";
    else if ( Translate(m_sPackageIdTag) == USB_CONFIG_DESCRIPTOR ) 
        m_sPackageId = "USB_CONFIG_DESCRIPTOR";
    else if ( Translate(m_sPackageIdTag) == USB_DEFAULT_STRING_DESCRIPTOR ) 
        m_sPackageId = "USB_DEFAULT_STRING_DESCRIPTOR";
    else if ( Translate(m_sPackageIdTag) == USB_DEVICE_DESCRIPTOR ) 
        m_sPackageId = "USB_DEVICE_DESCRIPTOR";
    else if ( Translate(m_sPackageIdTag) == USB_ENDPOINT_DESCRIPTOR ) 
        m_sPackageId = "USB_ENDPOINT_DESCRIPTOR";
    else if ( Translate(m_sPackageIdTag) == USB_INTERFACE_DESCRIPTOR ) 
        m_sPackageId = "USB_INTERFACE_DESCRIPTOR";
    else if ( Translate(m_sPackageIdTag) == USB_INTERFACE_STRING_DESCRIPTOR ) 
        m_sPackageId = "USB_INTERFACE_STRING_DESCRIPTOR";
    else if ( Translate(m_sPackageIdTag) == USB_LANGUAGE_STRING_DESCRIPTOR ) 
        m_sPackageId = "USB_LANGUAGE_STRING_DESCRIPTOR";
    else if ( Translate(m_sPackageIdTag) == USB_MANUFACTURER_STRING_DESCRIPTOR ) 
        m_sPackageId = "USB_MANUFACTURER_STRING_DESCRIPTOR";
    else if ( Translate(m_sPackageIdTag) == USB_PRODUCT_STRING_DESCRIPTOR ) 
        m_sPackageId = "USB_PRODUCT_STRING_DESCRIPTOR";
    else if ( Translate(m_sPackageIdTag) == USB_SERIAL_STRING_DESCRIPTOR ) 
        m_sPackageId = "USB_SERIAL_STRING_DESCRIPTOR";
    else if ( Translate(m_sPackageIdTag) == USBVENDORREQ ) 
        m_sPackageId = "USB_VENDORREQ";
    else if ( Translate(m_sPackageIdTag) == ESCAPESEQID ) 
        m_sPackageId = "ESCAPE_SEQUENCE";
    else if ( Translate(m_sPackageIdTag) == OEMCUSTOMID ) 
        m_sPackageId = "CUST";
    else if ( Translate(m_sPackageIdTag) == NOMONITORID ) 
        m_sPackageId = "NOMONITORID";
    else if ( Translate(m_sPackageIdTag) == COREID ) 
        m_sPackageId = "COREID";
    
#if EXTENDED_RESERVED_DATA
    // extended reserved data packages
    else if ( m_sPackageIdTag == ToUpper(sCLKE, true) ) 
        m_sPackageId = sClockEnable;
    else if ( m_sPackageIdTag == ToUpper(sDDRG, true) ) 
        m_sPackageId = sDDRGeometry;
    else if ( m_sPackageIdTag == ToUpper(sDDRT, true) ) 
        m_sPackageId = sDDRTiming;
    else if ( m_sPackageIdTag == ToUpper(sDDRC, true) ) 
        m_sPackageId = sDDRCustom;
    else if ( m_sPackageIdTag == ToUpper(sFREQ, true) ) 
        m_sPackageId = sFrequency;
    else if ( m_sPackageIdTag == ToUpper(sVOLT, true) ) 
        m_sPackageId = sVoltages;
    else if ( m_sPackageIdTag == ToUpper(sCMCC, true) ) 
        m_sPackageId = sConfigMemoryControl;
    else if ( m_sPackageIdTag == ToUpper(sTZID, true) ) 
        m_sPackageId = sTrustZone;
    else if ( m_sPackageIdTag == ToUpper(sTZON, true) ) 
        m_sPackageId = sTrustZoneRegid;
    else if ( m_sPackageIdTag == ToUpper(sOPDV, true) ) 
        m_sPackageId = sOpDiv;
    else if ( m_sPackageIdTag == ToUpper(sMODE, true) ) 
        m_sPackageId = sOpMode;
#endif
#if DDR_CONFIGURATION
    else if ( Translate(m_sPackageIdTag) == CIDPID ) 
        m_sPackageId = "CIPD";
    else if ( m_sPackageIdTag.compare(0,8,"0x444452") == 0 ) // DDRx
        m_sPackageId = HexAsciiToText(m_sPackageIdTag);
    else if ( Translate(m_sPackageIdTag) == TZRI ) 
        m_sPackageId = "TZRI";
#endif
    else
    // totally unknown custom user defined reserved data
        m_sPackageId = HexAsciiToText( m_sPackageIdTag );

    m_bChanged = true;
}


void CReservedPackageData::PredefinedPackageComments( int idx, string *psComment )
{
    if ( Translate(m_sPackageIdTag) == AUTOBIND ) // AUTOBIND
    {
        switch ( idx )
        {
            case 1: *psComment = "Autobind"; return;
            break;
        }
        return;
    }
    else if ( Translate(m_sPackageIdTag) == DDRID ) // DDRID
    {
        switch ( idx )
        {
            case 1: *psComment = "ACCR_VALUE"; return;
            case 2: *psComment = "MDCNFG_VALUE"; return;
            case 3: *psComment = "DDR_HCAL_VALUE"; return;
            case 4: *psComment = "MDREFR_VALUE"; return;
        }
        return;
    }
    else if ( Translate(m_sPackageIdTag) == GPIOID ) // GPIOID
    {
        switch ( idx )
        {
            case 1: *psComment = "Number of GPIO Address & Value Pairs below"; return;
            case 2: *psComment = "Address"; return;
            case 3: *psComment = "Value"; return;
        }
        return;
    }
    else if ( Translate(m_sPackageIdTag) == TBR_XFER ) // TBR_XFER
    {
        switch ( idx )
        {
            case 1: *psComment = "XFER_TABLE_LOCATION"; return;
            case 2: *psComment = "NUM_PAIRS"; return;
        }
        return;
    }
    else if ( Translate(m_sPackageIdTag) == RESUMEBLID ) // RESUMEBLID
    {
        switch ( idx )
        {
            case 1: *psComment = "DDRResumeRecordAddr"; return;
            case 2: *psComment = "DDRScratchAreaAddr"; return;
            case 3: *psComment = "DDRScratchAreaLength"; return;
        }
        return;
    }
    else if ( Translate(m_sPackageIdTag) == UARTID ) // UARTID
    {
        switch ( idx )
        {
            case 1: *psComment = "Port"; return;
            case 2: *psComment = "Enabled"; return;
        }
        return;
    }
    else if ( Translate(m_sPackageIdTag) == USBID ) // USBID
    {
        switch ( idx )
        {
            case 1: *psComment = "Port"; return;
            case 2: *psComment = "Enabled"; return;
        }
        return;
    }
    else if ( Translate(m_sPackageIdTag) == USB_CONFIG_DESCRIPTOR ) // USB_CONFIG_DESCRIPTOR
        return;
    else if ( Translate(m_sPackageIdTag) == USB_DEFAULT_STRING_DESCRIPTOR ) // USB_DEFAULT_STRING_DESCRIPTOR
        return;
    else if ( Translate(m_sPackageIdTag) == USB_DEVICE_DESCRIPTOR ) // USB_DEVICE_DESCRIPTOR
        return;
    else if ( Translate(m_sPackageIdTag) == USB_ENDPOINT_DESCRIPTOR ) // USB_ENDPOINT_DESCRIPTOR
        return;
    else if ( Translate(m_sPackageIdTag) == USB_INTERFACE_DESCRIPTOR ) // USB_INTERFACE_DESCRIPTOR
        return;
    else if ( Translate(m_sPackageIdTag) == USB_INTERFACE_STRING_DESCRIPTOR ) // USB_INTERFACE_STRING_DESCRIPTOR
        return;
    else if ( Translate(m_sPackageIdTag) == USB_LANGUAGE_STRING_DESCRIPTOR ) // USB_LANGUAGE_STRING_DESCRIPTOR
        return;
    else if ( Translate(m_sPackageIdTag) == USB_MANUFACTURER_STRING_DESCRIPTOR ) // USB_MANUFACTURER_STRING_DESCRIPTOR
        return;
    else if ( Translate(m_sPackageIdTag) == USB_PRODUCT_STRING_DESCRIPTOR ) // USB_PRODUCT_STRING_DESCRIPTOR
        return;
    else if ( Translate(m_sPackageIdTag) == USB_SERIAL_STRING_DESCRIPTOR ) // USB_SERIAL_STRING_DESCRIPTOR
        return;
    else if ( Translate(m_sPackageIdTag) == USBVENDORREQ ) // USB_VENDORREQ
    {
        switch ( idx )
        {
            case 1: *psComment = "bm Request Type"; return;
            case 2: *psComment = "bm Request"; return;
            case 3: *psComment = "wValue"; return;
            case 4: *psComment = "wIndex"; return;
            case 5: *psComment = "wLength"; return;
            case 6: *psComment = "wData"; return;
        }
        return;
    }
    else if ( Translate(m_sPackageIdTag) == ESCAPESEQID ) // escape sequence
    {
        switch ( idx )
        {
            case 1: *psComment = "EscSeqTimeOutMS"; return;
        }
        return;
    }
    else if ( Translate(m_sPackageIdTag) == COREID ) // core id package
    {
        switch ( idx )
        {
            case 1: *psComment = "Core ID"; return;
        }
        return;
    }
    else if ( Translate(m_sPackageIdTag) == NOMONITORID ) // NOMONITORID package
        return;
    else if ( Translate(m_sPackageIdTag) == OEMCUSTOMID ) // OEM CUSTOM package
        return;
}
