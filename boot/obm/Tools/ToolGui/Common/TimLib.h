/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once

#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <string>
#include <list>
using namespace std;

#include "Tim.h"

// forward declaration
class CImageDescription;

typedef list<CImageDescription*>           t_ImagesList;
typedef list<CImageDescription*>::iterator t_ImagesIter;

const unsigned int MSysSectorSize = 512;
#define TBR_VERSIONS_WITH_PARTITION 0x30200

class CTimLib
{
public:
	CTimLib(void);
	virtual ~CTimLib(void);

	bool GetDWord (ifstream& ifs, char *szString, unsigned int* pValue);
	bool GetSValue (ifstream& ifs, const char *szString, string& sValue, bool bErrMsg = true );

	bool GetFieldValue (string& sLine, string& sField, DWORD& dwValue);
	bool GetFieldValue (string& sLine, string& sField, string& sValue);

	bool CheckGetSValue (ifstream& ifs, char *szString, string& sValue);
	
	unsigned int Translate (const char *pValue);
	unsigned int Translate (string sValue);

	void Endian_Convert (UINT_T x,UINT_T *y);
	UINT32_T CheckSum (unsigned char *Address, UINT_T length);

	bool CheckImageOverlap(t_ImagesList& Images, unsigned int BootFlashSign);

	bool CreateOutputImageName (string& sImageFilename, string& sImageOutFilename);
	bool CreateOutputTimBinImageName (string& sImageFilename, string& sImageOutFilename);

	bool GetQWord (ifstream& ifs, char *szString, UINT64 *pValue);
	unsigned long long TranslateQWord (const char *pValue);

	void PrependPathIfNone( string& sFilePath );
	string QuotedFilePath( string& sFilePath );
	string& HexFormattedAscii( unsigned int iNum );
	string& HexFormattedAscii64( UINT64 iNum );
	string HexAsciiToText( const string& sHexAscii );
//	bool TrimAndSkipLine( string& sLine );
	void TextToHexFormattedAscii( string& sRet, string& sText );

	ifstream& GetNextLine( ifstream& ifs, string& sLine, bool bIncludeWS = false );
    ifstream& GetNextLineNoWSSkipComments( ifstream& ifs, string& sLine );

	// std::string helpers
	bool IsAlpha( const string& instring );
	bool IsAlphaNumeric( const string& instring );
	string ToLower( const string& instring, bool bHexNum = false );
	string ToUpper( const string& instring, bool bHexNum = false );
	string TrimWS( string& sValue );
	string TrimLeadingWS( string& sValue );
	string TrimTrailingWS( string& sValue );

#if TOOLS_GUI == 1
	bool SaveFieldState( stringstream& ss, string& sField );
	bool LoadFieldState( ifstream& ifs, string& sField );

	bool SaveFieldState( stringstream& ss, unsigned int& iField );
	bool LoadFieldState( ifstream& ifs, unsigned int& iField );

	bool SaveFieldState( stringstream& ss, pair<unsigned int, unsigned int>& rPair );
	bool LoadFieldState( ifstream& ifs, pair<unsigned int, unsigned int>& rPair );

	bool SaveFieldStatePtr( stringstream& ss, void* pObj );
	bool LoadFieldStatePtr( ifstream& ifs, void* pObj );
#endif
};
