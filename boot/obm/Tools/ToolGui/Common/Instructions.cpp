/******************************************************************************
 *
 *  (C)Copyright 2005 - 2011 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#if TOOLS_GUI == 1
#include "stdafx.h"
#include "MarvellBootUtility.h"
#endif

#include "Instructions.h"
#include "TimDescriptorParser.h"

#if LINUX
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

CInstruction::CInstruction()
: CTimLib()
{
    // empty nop instruction
    m_InstructionOpCode = INSTR_NOP;
    m_InstructionText = "NOP";
    m_NumParamsUsed = 0;
    m_ParamNames[ MAX_INST_PARAMS ];
    for ( int i = 0; i < MAX_INST_PARAMS; i++ )
        m_ParamValues[ i ] = 0;
}

CInstruction::CInstruction( INSTRUCTION_OP_CODE_SPEC_T eOpCode, const string sInstructionName, unsigned int uiNumParamsUsed,
    const string sParam0Name, const string sParam1Name, const string sParam2Name, const string sParam3Name, const string sParam4Name )
: m_InstructionOpCode(eOpCode), m_InstructionText(sInstructionName), 
  m_NumParamsUsed( uiNumParamsUsed ), CTimLib()
{
    m_ParamNames[0] = sParam0Name;
    m_ParamNames[1] = sParam1Name; 
    m_ParamNames[2] = sParam2Name;
    m_ParamNames[3] = sParam3Name;
    m_ParamNames[4] = sParam4Name;
    m_ParamValues[0] = 0;
    m_ParamValues[1] = 0;
    m_ParamValues[2] = 0;
    m_ParamValues[3] = 0;
    m_ParamValues[4] = 0;
}

CInstruction::~CInstruction(void)
{
#if TOOLS_GUI == 1
        CTimDescriptorLine* pLine = 0;

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_InstructionText )) != 0 )
            pLine->RemoveRef( &m_InstructionText );
#endif
}

CInstruction::CInstruction( const CInstruction& rhs )
: CTimLib( rhs )
{
    // copy constructor
    CInstruction& nc_rhs = const_cast<CInstruction&>(rhs);

    m_InstructionOpCode = rhs.m_InstructionOpCode;
    m_InstructionText = rhs.m_InstructionText;

#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_InstructionText )) != 0 )
        pLine->AddRef( &m_InstructionText );
#endif
    
    m_NumParamsUsed = rhs.m_NumParamsUsed;
    for ( int i = 0; i < MAX_INST_PARAMS; i++ )
    {
        m_ParamNames[ i ] = rhs.m_ParamNames[ i ];	
        m_ParamValues[ i ] = rhs.m_ParamValues[ i ];
    }

    m_sComment = rhs.m_sComment;
}

CInstruction& CInstruction::operator=( const CInstruction& rhs ) 
{
    // assignment operator
    if ( &rhs != this )
    {
        CTimLib::operator=(rhs);

        CInstruction& nc_rhs = const_cast<CInstruction&>(rhs);
    
        m_InstructionOpCode = rhs.m_InstructionOpCode;

#if TOOLS_GUI == 1
        CTimDescriptorLine* pLine = 0;

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_InstructionText )) != 0 )
            pLine->RemoveRef( &m_InstructionText );
#endif

        m_InstructionText = rhs.m_InstructionText;

#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_InstructionText )) != 0 )
            pLine->AddRef( &m_InstructionText );
#endif

        m_NumParamsUsed = rhs.m_NumParamsUsed;
        for ( int i = 0; i < MAX_INST_PARAMS; i++ )
        {
            m_ParamNames[ i ] = rhs.m_ParamNames[ i ];
            m_ParamValues[ i ] = rhs.m_ParamValues[ i ];
        }
    
        m_sComment = rhs.m_sComment;
    }
    return *this;
}

bool CInstruction::SetInstructionType( INSTRUCTION_OP_CODE_SPEC_T eOpCode )
{
    t_InstructionListIter Iter = CInstructions::s_InstFmt.begin();
    while ( Iter != CInstructions::s_InstFmt.end() )
    {
        if ( (*Iter)->m_InstructionOpCode == eOpCode )
        {
            *this = *(*Iter);
            break;
        }
        Iter++;
    }
    return true;
}

bool CInstruction::SetInstructionType( string& sInstructionText )
{
    t_InstructionListIter Iter = CInstructions::s_InstFmt.begin();
    while ( Iter != CInstructions::s_InstFmt.end() )
    {
        if ( (*Iter)->m_InstructionText == sInstructionText )
        {
            *this = *(*Iter);
            break;
        }
        Iter++;
    }
    return true;
}

bool CInstruction::ToBinary( ofstream& ofs )
{
    // validate size
//	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
//		return false;

//	ofs << Translate(*m_FieldValues[DATA_ID]);
//	ofs << Translate(*m_FieldValues[LOCATION]);

    return ofs.good();
}
        
unsigned int CInstruction::Size()
{
    return (m_NumParamsUsed * 4) + 4; 
}

#if TOOLS_GUI == 1
bool CInstruction::LoadState( ifstream& ifs )
{
    if ( theApp.ProjectVersion >= 0x03021701 )
    {
        string sbuf;
        LoadFieldState( ifs, sbuf );
        m_InstructionOpCode = (INSTRUCTION_OP_CODE_SPEC_T)Translate(sbuf);
        LoadFieldState( ifs, m_InstructionText );
        LoadFieldState( ifs, sbuf );
        m_NumParamsUsed = Translate(sbuf);

        if ( theApp.ProjectVersion >= 0x03030121 ) 
        {
            for ( int i = 0; i < MAX_INST_PARAMS; i++ )
                LoadFieldState( ifs, m_ParamNames[ i ] );

            for ( int i = 0; i < MAX_INST_PARAMS; i++ )
            {
                LoadFieldState( ifs, sbuf );
                m_ParamValues[ i ] = Translate(sbuf);
            }
        }
        else
        {
            // MAX_INST_PARAMS was 3 for earlier versions
            for ( int i = 0; i < 3; i++ )
                LoadFieldState( ifs, m_ParamNames[ i ] );

            for ( int i = 0; i < 3; i++ )
            {
                LoadFieldState( ifs, sbuf );
                m_ParamValues[ i ] = Translate(sbuf);
            }
        }

        if ( theApp.ProjectVersion >= 0x03030124 ) 
        {
            LoadFieldState( ifs, m_sComment );
        }
    }
    return ifs.good() && !ifs.fail(); // success;
}
#endif

#if TOOLS_GUI == 1
bool CInstruction::SaveState( stringstream& ss )
{
    SaveFieldState( ss, (unsigned int&)m_InstructionOpCode );
    SaveFieldState( ss, m_InstructionText );
    SaveFieldState( ss, m_NumParamsUsed );

    for ( int i = 0; i < MAX_INST_PARAMS; i++ )
        SaveFieldState( ss, m_ParamNames[ i ] );

    for ( int i = 0; i < MAX_INST_PARAMS; i++ )
        SaveFieldState( ss, m_ParamValues[ i ] );

    SaveFieldState( ss, m_sComment );

   return true;
}
#endif

#if TOOLS_GUI == 1
bool CInstruction::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
    string sValue;
    for ( unsigned int i = 0; i < m_NumParamsUsed; i++, sValue += " "  )
        sValue += HexFormattedAscii(m_ParamValues[i]);

    if ( !TimDescriptor.AttachCommentedObject( ss, this, m_InstructionText.c_str(), sValue ) )
        ss << m_InstructionText << ":" << sValue << " ;" << m_sComment << endl;

    return true;
}
#endif


t_InstructionList	CInstructions::s_InstFmt;
CInstruction NOOP_INST( INSTR_NOP, "NOP", 0, "", "", "", "", "" );
CInstruction WRITE_INST( INSTR_WRITE, "WRITE", 2, "Address", "Value", "", "", "" );
CInstruction READ_INST( INSTR_READ, "READ", 2, "Address", "Number of Reads", "", "", "" );
CInstruction DELAY_INST( INSTR_DELAY, "DELAY", 1, "Value", "", "", "", "" );
CInstruction WAIT_FOR_BIT_SET_INST( INSTR_WAIT_FOR_BIT_SET, "WAIT_FOR_BIT_SET", 3, "Address", "Mask", "TimeOut Value", "", "" );
CInstruction WAIT_FOR_BIT_CLEAR_INST( INSTR_WAIT_FOR_BIT_CLEAR, "WAIT_FOR_BIT_CLEAR", 3, "Address", "Mask", "TimeOut Value", "", "" );
CInstruction AND_VAL_INST( INSTR_AND_VAL, "AND_VAL", 2, "Address", "Value", "", "", "" );
CInstruction OR_VAL_INST( INSTR_OR_VAL, "OR_VAL", 2, "Address", "Value", "", "", "" );
// new DDR script instructions
CInstruction SET_BITFIELD_INST( INSTR_SET_BITFIELD, "SET_BITFIELD", 3, "Address", "Mask", "Value", "", "" );
CInstruction WAIT_FOR_BIT_PATTERN_INST( INSTR_WAIT_FOR_BIT_PATTERN, "WAIT_FOR_BIT_PATTERN", 4, "Address", "Mask", "Value", "TimeoutValue", "" );
CInstruction TEST_IF_ZERO_AND_SET_INST( INSTR_TEST_IF_ZERO_AND_SET, "TEST_IF_ZERO_AND_SET", 5, "TestAddress", "TestMask", "SetAddress", "SetMask", "SetValue" );
CInstruction TEST_IF_NOT_ZERO_AND_SET_INST( INSTR_TEST_IF_NOT_ZERO_AND_SET, "TEST_IF_NOT_ZERO_AND_SET", 5, "TestAddress", "TestMask", "SetAddress", "SetMask", "SetValue" );
CInstruction LOAD_SM_ADDR_INST( INSTR_LOAD_SM_ADDR, "LOAD_SM_ADDR", 2, "ScratchMemory", "Address", "", "", "" );
CInstruction LOAD_SM_VAL_INST( INSTR_LOAD_SM_VAL, "LOAD_SM_VAL", 2, "ScratchMemory", "Value", "", "", "" );
CInstruction STORE_SM_ADDR_INST( INSTR_STORE_SM_ADDR, "STORE_SM_ADDR", 2, "ScratchMemory", "Address", "", "", "" );
CInstruction MOV_SM_SM_INST( INSTR_MOV_SM_SM, "MOV_SM_SM", 2, "DestScratchMemory", "SrcScratchMemory", "", "", "" );
CInstruction RSHIFT_SM_VAL_INST( INSTR_RSHIFT_SM_VAL, "RSHIFT_SM_VAL", 2, "ScratchMemory", "Value", "", "", "" );
CInstruction LSHIFT_SM_VAL_INST( INSTR_LSHIFT_SM_VAL, "LSHIFT_SM_VAL", 2, "ScratchMemory", "Value", "", "", "" );
CInstruction AND_SM_VAL_INST( INSTR_AND_SM_VAL, "AND_SM_VAL", 2, "ScratchMemory", "Value", "", "", "" );
CInstruction OR_SM_VAL_INST( INSTR_OR_SM_VAL, "OR_SM_VAL", 2, "ScratchMemory", "Value", "", "", "" );
CInstruction OR_SM_SM_INST( INSTR_OR_SM_SM, "OR_SM_SM", 2, "DestScratchMemory", "SrcScratchMemory", "", "", "" );
CInstruction AND_SM_SM_INST( INSTR_AND_SM_SM, "AND_SM_SM", 2, "DestScratchMemory", "SrcScratchMemory", "", "", "" );
CInstruction TEST_SM_IF_ZERO_AND_SET_INST( INSTR_TEST_SM_IF_ZERO_AND_SET, "TEST_SM_IF_ZERO_AND_SET", 5, "TestScratchMemoryID", "TestMask", "SetScratchMemoryID", "SetMask", "SetValue" );
CInstruction TEST_SM_IF_NOT_ZERO_AND_SET_INST( INSTR_TEST_SM_IF_NOT_ZERO_AND_SET, "TEST_SM_IF_NOT_ZERO_AND_SET", 5, "TestScratchMemoryID", "TestMask", "SetScratchMemoryID", "SetMask", "SetValue" );

int	CInstructions::s_InstCount = 0;

const string CInstructions::Begin("Instructions");
const string CInstructions::End("End Instructions");

CInstructions::CInstructions()
:	CErdBase( INSTRUCTIONS_ERD, INSTRUCTION_MAX )
{
    s_InstCount++;

    if ( s_InstCount == 1 )
    {
        s_InstFmt.push_back( &NOOP_INST );
        s_InstFmt.push_back( &WRITE_INST );
        s_InstFmt.push_back( &READ_INST );
        s_InstFmt.push_back( &DELAY_INST );
        s_InstFmt.push_back( &WAIT_FOR_BIT_SET_INST );
        s_InstFmt.push_back( &WAIT_FOR_BIT_CLEAR_INST );
        s_InstFmt.push_back( &AND_VAL_INST );
        s_InstFmt.push_back( &OR_VAL_INST );
        // new DDR script instructions
        s_InstFmt.push_back( &SET_BITFIELD_INST );
        s_InstFmt.push_back( &WAIT_FOR_BIT_PATTERN_INST );
        s_InstFmt.push_back( &TEST_IF_ZERO_AND_SET_INST );
        s_InstFmt.push_back( &TEST_IF_NOT_ZERO_AND_SET_INST );
        s_InstFmt.push_back( &LOAD_SM_ADDR_INST );
        s_InstFmt.push_back( &LOAD_SM_VAL_INST );
        s_InstFmt.push_back( &STORE_SM_ADDR_INST );
        s_InstFmt.push_back( &MOV_SM_SM_INST );
        s_InstFmt.push_back( &RSHIFT_SM_VAL_INST );
        s_InstFmt.push_back( &LSHIFT_SM_VAL_INST );
        s_InstFmt.push_back( &AND_SM_VAL_INST );
        s_InstFmt.push_back( &OR_SM_VAL_INST );
        s_InstFmt.push_back( &OR_SM_SM_INST );
        s_InstFmt.push_back( &AND_SM_SM_INST );
        s_InstFmt.push_back( &TEST_SM_IF_ZERO_AND_SET_INST );
        s_InstFmt.push_back( &TEST_SM_IF_NOT_ZERO_AND_SET_INST );
    }
}

CInstructions::~CInstructions(void)
{
    Reset();

    s_InstCount--;
}

// copy constructor
CInstructions::CInstructions( const CInstructions& rhs )
: CErdBase( rhs )
{
    // need to do a deep copy of lists to avoid dangling references
    CInstructions& nc_rhs = const_cast<CInstructions&>(rhs);

#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;
#endif

    t_InstructionListIter iter = nc_rhs.m_Instructions.begin();
    while ( iter != nc_rhs.m_Instructions.end() )
    {
        CInstruction* pInst = new CInstruction( *(*iter) );
        m_Instructions.push_back( pInst );

#if TOOLS_GUI == 1
            // update the object line reference
            if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
                pLine->AddRef( pInst );
#endif
        iter++;
    }
}

// assignment operator
CInstructions& CInstructions::operator=( const CInstructions& rhs )
{
    // assignment operator
    if ( &rhs != this )
    {
        Reset();

        CErdBase::operator=( rhs );

        // need to do a deep copy of lists to avoid dangling references
        CInstructions& nc_rhs = const_cast<CInstructions&>(rhs);

#if TOOLS_GUI == 1
        CTimDescriptorLine* pLine = 0;
#endif

        t_InstructionListIter iter = nc_rhs.m_Instructions.begin();
        while ( iter != nc_rhs.m_Instructions.end() )
        {
            CInstruction* pInst = new CInstruction( *(*iter) );
            m_Instructions.push_back( pInst );

#if TOOLS_GUI == 1
            // update the object line reference
            if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
                pLine->AddRef( pInst );
#endif
            iter++;
        }
    }
    return *this;
}

void CInstructions::Reset()
{
#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;
#endif

    t_InstructionListIter iter = m_Instructions.begin();
    while ( iter != m_Instructions.end() )
    {
#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
            pLine->RemoveRef( *iter );
#endif
        delete *iter;
        iter++;
    }
    m_Instructions.clear();
}

unsigned int CInstructions::PackageSize() 
{
    unsigned int iSize = 0;

    t_InstructionListIter iter = m_Instructions.begin();
    while ( iter != m_Instructions.end() )
        iSize += (*iter++)->Size();

    return iSize;
}

bool CInstructions::Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine )
{
    while ( pLine = TimDescriptor.GetNextLineField( pLine ) )
    {
        bool bFound = false;
        t_InstructionListIter iter = s_InstFmt.begin();
        while ( iter != s_InstFmt.end() )
        {
            if ( TrimWS( pLine->m_FieldName ) == (*iter)->m_InstructionText )
            {
                CInstruction* pInstr = new CInstruction( *(*iter) );
                string sValues = TrimWS( pLine->m_FieldValue );
                
#if TOOLS_GUI == 1
                // strip out the comment and add it to the instruction
                stringstream ss;
                pLine->PostCommentLinesText(ss);
                string sComment = ss.str(); 
                sComment = TrimWS( sComment );
                while ( sComment.find(';')==0 )
                    sComment = sComment.substr(1,-1);
                pInstr->m_sComment = sComment;
#endif
                for ( unsigned int i=0; i < (*iter)->m_NumParamsUsed; i++)
                {
                    pInstr->m_ParamValues[i] = Translate(sValues);
                    // remove value that is now stored in m_ParamValues to access next value
                    sValues = sValues.substr(sValues.find_first_of(" \t\r\n")+1);
                }
                m_Instructions.push_back( pInstr );

#if TOOLS_GUI == 1
                pLine->AddRef( pInstr ); 
#endif
                bFound = true;
                break;
            }
            iter++;
        }
        if ( !bFound )
            break;
    }

    // field not found
    return true;
}

bool CInstructions::ToBinary( ofstream& ofs )
{
    bool bRet = true;
#if 0
    // validate size
    if ( m_FieldValues.size() !=  m_iMaxFieldNum )
        return false;

    ofs << TBR_XFER;
    ofs << PackageSize();
    ofs << Translate(*m_FieldNames[XFER_TABLE_LOC]);
    ofs << Translate(*m_FieldValues[NUM_DATA_PAIRS]);

    t_XferListIter iter = Xfers.begin();
    while ( bRet && iter != Xfers.end() )
        bRet = (*iter++)->ToBinary( ofs );
#endif

    return ( ofs.good() && bRet );
}

int CInstructions::AddPkgStrings( CReservedPackageData* pRPD )
{
    t_InstructionListIter iter = m_Instructions.begin();
    while ( iter != m_Instructions.end() )
    {
        pRPD->AddData( new string( HexFormattedAscii((*iter)->m_InstructionOpCode) ),  new string( (*iter)->m_InstructionText ) );
        for ( unsigned int j = 0; j < (*iter)->m_NumParamsUsed; j++ )
        {
            pRPD->AddData( new string( HexFormattedAscii((*iter)->m_ParamValues[j] ) ),  new string( (*iter)->m_ParamNames[j] ) );
        }
        iter++;
    }

    return PackageSize();
}

#if TOOLS_GUI == 1
bool CInstructions::SaveState( stringstream& ss )
{
    unsigned int temp = 0;

    CErdBase::SaveState( ss );

    temp = (unsigned int)m_Instructions.size();
    SaveFieldState( ss, temp );

    t_InstructionListIter iter = m_Instructions.begin();
    while ( iter != m_Instructions.end() )
    {
        if ( !(*iter++)->SaveState( ss ) )
            return false;
    }
    return true; // SUCCESS
}
#endif

#if TOOLS_GUI == 1
bool CInstructions::LoadState( ifstream& ifs )
{
    bool bRet = true;

    if ( theApp.ProjectVersion >= 0x03021701 )
    {
        Reset();

        string sbuf;
        CErdBase::ERD_PKG_TYPE ErdType = CErdBase::UNKNOWN_ERD;

        LoadFieldState( ifs, sbuf );  
        ErdType = CErdBase::ERD_PKG_TYPE(Translate(sbuf));
        if ( ErdType != INSTRUCTIONS_ERD )
            return false;

        CErdBase::LoadState( ifs );

        LoadFieldState( ifs, sbuf );
        unsigned int Size = Translate(sbuf);

        for ( unsigned int i = 0; i < Size; i++ )
        {
            CInstruction* pInst = new CInstruction;
            bRet = pInst->LoadState( ifs );
            if ( !bRet )
            {
                delete pInst;
                break;
            }
            m_Instructions.push_back(pInst);
        }
    }

    return bRet && ifs.good() && !ifs.fail(); // success
}
#endif

#if TOOLS_GUI == 1
bool CInstructions::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
    bool bRet = true;

    if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
        ss << Begin << ":" << endl;

    t_InstructionListIter iter = m_Instructions.begin();
    while ( bRet && iter != m_Instructions.end() )
    {
        bRet = (*iter)->ToText( TimDescriptor, ss );
        iter++;
    }

    if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
        ss << End << ":" << endl;

    return bRet;
}
#endif
