/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "ErdBase.h"
#include "TimDescriptorParser.h"

#include "AutoBindERD.h"
#include "CoreResetERD.h"
#include "EscapeSeqERD.h"
#include "GpioSetERD.h"
#include "ResumeDdrERD.h"
#include "TbrXferERD.h"
#include "UartERD.h"
#include "UsbERD.h"
#include "UsbVendorReqERD.h"
#include "ConsumerID.h"
#include "TzInitialization.h"
#include "TzOperations.h"

CErdBase::CErdBase( ERD_PKG_TYPE ErdType, int iMaxFieldNum )
:	m_eErdType( ErdType ), m_iMaxFieldNum( iMaxFieldNum ), 	CTimLib()
{
    Reset();
}

CErdBase::~CErdBase(void)
{
#if TOOLS_GUI == 1
        CTimDescriptorLine* pLine = 0;
#endif
    for ( unsigned int i = 0; i < m_FieldNames.size(); i++ )
    {
#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, m_FieldNames[i] )) != 0 )
            pLine->RemoveRef( m_FieldNames[i] );
#endif
        delete m_FieldNames[i];
    }
    m_FieldNames.clear();

    for ( unsigned int i = 0; i < m_FieldValues.size(); i++ )
        delete m_FieldValues[i];
    m_FieldValues.clear();

    for ( unsigned int i = 0; i < m_FieldComments.size(); i++ )
        delete m_FieldComments[i];
    m_FieldComments.clear();
}

// copy constructor
CErdBase::CErdBase( const CErdBase& rhs )
: CTimLib(rhs)
{
    // copy constructor
    m_eErdType = rhs.m_eErdType;
    m_iMaxFieldNum = rhs.m_iMaxFieldNum;

    Reset();

#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;
#endif
    // need to do a deep copy of lists to avoid dangling references
    for ( unsigned int i = 0; i < m_iMaxFieldNum; i++ )
    {
#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, rhs.m_FieldNames[i] )) != 0 )
            pLine->AddRef( m_FieldNames[i] );
#endif
        *m_FieldNames[i] = *rhs.m_FieldNames[i];

        *m_FieldValues[i] = *rhs.m_FieldValues[i];
        *m_FieldComments[i] = *rhs.m_FieldComments[i];
    }
}

// assignment operator
CErdBase& CErdBase::operator=( const CErdBase& rhs )
{
    // assignment operator
    if ( &rhs != this )
    {
        CTimLib::operator=(rhs);

        m_eErdType = rhs.m_eErdType;
        m_iMaxFieldNum = rhs.m_iMaxFieldNum;

        Reset();

#if TOOLS_GUI == 1
        CTimDescriptorLine* pLine = 0;
#endif
        // need to do a deep copy of lists to avoid dangling references
        for ( unsigned int i = 0; i < m_iMaxFieldNum; i++ )
        {
#if TOOLS_GUI == 1
            // update the object line reference
            if ( (pLine = CTimDescriptor::GetLineField( "", false, rhs.m_FieldNames[i] )) != 0 )
                pLine->AddRef( m_FieldNames[i] );
#endif
            *m_FieldNames[i] = *rhs.m_FieldNames[i];

            *m_FieldValues[i] = *rhs.m_FieldValues[i];
            *m_FieldComments[i] = *rhs.m_FieldComments[i];
        }
    }
    return *this;
}

void CErdBase::Reset()
{
#if TOOLS_GUI == 1
        CTimDescriptorLine* pLine = 0;
#endif

    for ( unsigned int i = 0; i < m_FieldNames.size(); i++ )
    {
#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, m_FieldNames[i] )) != 0 )
            pLine->RemoveRef( m_FieldNames[i] );
#endif
        delete m_FieldNames[i];
    }
    m_FieldNames.clear();

    for ( unsigned int i = 0; i < m_FieldValues.size(); i++ )
        delete m_FieldValues[i];
    m_FieldValues.clear();

    for ( unsigned int i = 0; i < m_FieldComments.size(); i++ )
        delete m_FieldComments[i];
    m_FieldComments.clear();

    m_FieldNames.resize(m_iMaxFieldNum);
    m_FieldValues.resize(m_iMaxFieldNum);
    m_FieldComments.resize(m_iMaxFieldNum);

    for ( unsigned int i=0; i< m_iMaxFieldNum; i++ )
    {
        m_FieldNames[i] = new string("");
        m_FieldValues[i] = new string("0");
        m_FieldComments[i] = new string("");
    }
}

CErdBase* CErdBase::Create( ERD_PKG_TYPE ErdPkgType )
{
    CErdBase* pErdBase = 0;

    switch ( ErdPkgType )
    {
        case AUTOBIND_ERD:
            pErdBase = new CAutoBind();
            break;
        case ESCAPESEQ_ERD:
            pErdBase = new CEscapeSeq();
            break;
        case GPIOSET_ERD:
            pErdBase = new CGpioSet();
            break;
        case GPIO_ERD:
            pErdBase = new CGpio();
            break;
        case RESUME_DDR_ERD:
            pErdBase = new CResumeDdr();
            break;
        case TBRXFER_ERD:
            pErdBase = new CTBRXferSet();
            break;
        case XFER_ERD:
            pErdBase = new CXfer();
            break;
        case UART_ERD:
            pErdBase = new CUart();
            break;
        case USB_ERD:
            pErdBase = new CUsb();
            break;
        case USBVENDORREQ_ERD:
            pErdBase = new CUsbVendorReq();
            break;
        case CONSUMER_ID_ERD:
            pErdBase = new CConsumerID();
            break;
        case DDR_INITIALIZATION_ERD:
            pErdBase = new CDDRInitialization();
            break;
        case INSTRUCTIONS_ERD:
            pErdBase = new CInstructions();
            break;
        case DDR_OPERATIONS_ERD:
            pErdBase = new CDDROperations();
            break;
        case CORE_ID_ERD:
            pErdBase = new CCoreReset();
            break;
        case TZ_INITIALIZATION_ERD:
            pErdBase = new CTzInitialization();
            break;
        case TZ_OPERATIONS_ERD:
            pErdBase = new CTzOperations();
            break;
        default:
            pErdBase = 0;
            break;
    }

    return pErdBase;
}

CErdBase* CErdBase::Create( CErdBase& src )
{
    CErdBase* pErdBase = 0;

    switch ( src.ErdPkgType() )
    {
        case AUTOBIND_ERD:
            pErdBase = new CAutoBind(dynamic_cast<CAutoBind&>(src));
            break;
        case ESCAPESEQ_ERD:
            pErdBase = new CEscapeSeq(dynamic_cast<CEscapeSeq&>(src));
            break;
        case GPIOSET_ERD:
            pErdBase = new CGpioSet(dynamic_cast<CGpioSet&>(src));
            break;
        case GPIO_ERD:
            pErdBase = new CGpio(dynamic_cast<CGpio&>(src));
            break;
        case RESUME_DDR_ERD:
            pErdBase = new CResumeDdr(dynamic_cast<CResumeDdr&>(src));
            break;
        case TBRXFER_ERD:
            pErdBase = new CTBRXferSet(dynamic_cast<CTBRXferSet&>(src));
            break;
        case XFER_ERD:
            pErdBase = new CXfer(dynamic_cast<CXfer&>(src));
            break;		
        case UART_ERD:
            pErdBase = new CUart(dynamic_cast<CUart&>(src));
            break;
        case USB_ERD:
            pErdBase = new CUsb(dynamic_cast<CUsb&>(src));
            break;
        case USBVENDORREQ_ERD:
            pErdBase = new CUsbVendorReq(dynamic_cast<CUsbVendorReq&>(src));
            break;
        case CONSUMER_ID_ERD:
            pErdBase = new CConsumerID(dynamic_cast<CConsumerID&>(src));
            break;
        case DDR_INITIALIZATION_ERD:
            pErdBase = new CDDRInitialization(dynamic_cast<CDDRInitialization&>(src));
            break;
        case INSTRUCTIONS_ERD:
            pErdBase = new CInstructions(dynamic_cast<CInstructions&>(src));
            break;
        case DDR_OPERATIONS_ERD:
            pErdBase = new CDDROperations(dynamic_cast<CDDROperations&>(src));
            break;
        case CORE_ID_ERD:
            pErdBase = new CCoreReset(dynamic_cast<CCoreReset&>(src));
            break;
        case TZ_INITIALIZATION_ERD:
            pErdBase = new CTzInitialization(dynamic_cast<CTzInitialization&>(src));
            break;
        case TZ_OPERATIONS_ERD:
            pErdBase = new CTzOperations(dynamic_cast<CTzOperations&>(src));
            break;
        default:
            pErdBase = 0;
    }

    return pErdBase;
}

CErdBase* CErdBase::Create( string& sPackageName )
{
    CErdBase* pErdBase = 0;

    if ( sPackageName == "AutoBind" )
        pErdBase = new CAutoBind;
    else if ( sPackageName == "Escape Sequence" )
        pErdBase = new CEscapeSeq;
    else if ( sPackageName == "GpioSet" )
        pErdBase = new CGpioSet;
    else if ( sPackageName == "Gpio" )
        pErdBase = new CGpio;
    else if ( sPackageName == "Resume DDR" )
        pErdBase = new CResumeDdr;
    else if ( sPackageName == "TBR Xfer Set" )
        pErdBase = new CTBRXferSet;
    else if ( sPackageName == "Xfer" )
        pErdBase = new CXfer;
    else if ( sPackageName == "Uart" )
        pErdBase = new CUart;
    else if ( sPackageName == "Usb" )
        pErdBase = new CUsb;
    else if ( sPackageName == "Usb Vendor Request" )
        pErdBase = new CUsbVendorReq;
    else if ( sPackageName == "Consumer ID" )
        pErdBase = new CConsumerID;
    else if ( sPackageName == "DDR Initialization" )
        pErdBase = new CDDRInitialization;
    else if ( sPackageName == "Instruction" )
            pErdBase = new CInstructions();
    else if ( sPackageName == "DDR Operations" )
            pErdBase = new CDDROperations();
    else if ( sPackageName == "Core Reset" )
        pErdBase = new CCoreReset;
    else if ( sPackageName == "TZ Initialization" )
        pErdBase = new CTzInitialization;
    else if ( sPackageName == "TZ Operations" )
            pErdBase = new CTzOperations();

    return pErdBase;
}

bool CErdBase::Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine )
{
    while ( pLine = TimDescriptor.GetNextLineField( pLine ) )
    {
        bool bFound = false;
        for ( unsigned int idx = 0; idx < m_FieldNames.size(); idx++ )
        {
            if ( pLine->m_FieldName == *m_FieldNames[ idx ] )
            {
#if TOOLS_GUI == 1
                pLine->AddRef( m_FieldNames[ idx ] ); 
#endif
                *m_FieldValues[ idx ] = pLine->m_FieldValue;
                bFound = true;
                break;
            }
        }
        if ( !bFound )
            break;
    }

    // field not found
    return true;
}

#if TOOLS_GUI == 1
bool CErdBase::SaveState( stringstream& ss )
{
    unsigned int temp = 0;
    // save package type so load project can recreate object before 
    // calling LoadState

    temp = m_eErdType;
    SaveFieldState( ss, temp );

    SaveFieldStatePtr( ss, this );

    SaveFieldState( ss, m_iMaxFieldNum );
    
    t_stringVectorIter iter = m_FieldNames.begin();
    while( iter != m_FieldNames.end() )
        SaveFieldState( ss, *(*iter++) );

    iter = m_FieldValues.begin();
    while( iter != m_FieldValues.end() )
        SaveFieldState( ss, *(*iter++) );

    iter = m_FieldComments.begin();
    while( iter != m_FieldComments.end() )
        SaveFieldState( ss, *(*iter++) );

    return true; // SUCCESS
}
#endif

#if TOOLS_GUI == 1
bool CErdBase::LoadState( ifstream& ifs )
{
    if ( theApp.ProjectVersion >= 0x03021400 )
        LoadFieldStatePtr( ifs, this ); 

    if ( theApp.ProjectVersion >= 0x03021200 )
    {
        LoadFieldState( ifs, m_iMaxFieldNum );  

        // resizes arrays according to m_iMaxFieldNum
        Reset();

        for ( unsigned int i = 0; i < m_iMaxFieldNum; i++ )
            LoadFieldState( ifs, *m_FieldNames[i] );   // field name

        for ( unsigned int i = 0; i < m_iMaxFieldNum; i++ )
            LoadFieldState( ifs, *m_FieldValues[i] );   // field value

        for ( unsigned int i = 0; i < m_iMaxFieldNum; i++ )
            LoadFieldState( ifs, *m_FieldComments[i] );   // field comment
    }
    
    return ifs.good() && !ifs.fail(); // success
}
#endif

#if TOOLS_GUI == 1
bool CErdBase::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
    for ( unsigned int i = 0; i < m_iMaxFieldNum; i++ )
    {
        if ( !TimDescriptor.AttachCommentedObject( ss, m_FieldNames[i], m_FieldNames[i]->c_str(), *m_FieldValues[i] ) )
        {
            ss << *m_FieldNames[i] << ": ";
            ss << *m_FieldValues[i];
    //		ss << "     // *m_FieldComments[i];
            ss << endl;
        }
    }
    return true;
}
#endif
