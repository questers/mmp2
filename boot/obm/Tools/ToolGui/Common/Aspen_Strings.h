/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "ExtendedReservedData.h"

#include <string>
#include <vector>
using namespace std;

typedef vector<string*>           t_stringVector;
typedef vector<string*>::iterator t_stringVectorIter;

class AspenStrings 
{
public:
	AspenStrings(CExtendedReservedData& Ext);
	~AspenStrings();

	// prototypes
	void PopulateClockEnableFields(t_stringVector& g_ClockEnableFields);
	void PopulateFrequencyFields(t_stringVector& g_FrequencyFields);
	void PopulateDDRCustomFields(t_stringVector& g_DDRCustomFields);
	void PopulateVoltagesFields(t_stringVector& g_VoltagesFields);
	void PopulateOpDivFields(t_stringVector& g_OpDivFields);
	void PopulateOpModeFields(t_stringVector& g_OpModeFields);

	void Depopulate(t_stringVector& Fields);
	CExtendedReservedData& m_Ext;
};
