/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "Key.h"

#include <string>
#include <list>
#include <iterator>
#include <fstream>
#include <iostream>
#include <strstream>
#include <sstream>
using namespace std;

class CDigitalSignature : public CKey
{
public:
	CDigitalSignature(void);
	virtual ~CDigitalSignature(void);

	CDigitalSignature( const CDigitalSignature& rhs );
	CDigitalSignature& operator=( const CDigitalSignature& rhs );

public:
	bool IncludeInTim() { return m_bIncludeInTim; }
	void IncludeInTim( bool bIncludeInTim ) { m_bIncludeInTim = bIncludeInTim; m_bChanged = true; }

	t_stringList& RsaPrivateKeyList(){ return m_RsaPrivateKeyList; }
	string& RsaPrivateKey();
	string& RsaPrivateKeyPacked();
	
	t_stringList& ECDSAPrivateKeyList(){ return m_ECDSAPrivateKeyList; }
	string& ECDSAPrivateKey();
	string& ECDSAPrivateKeyPacked();

	void DiscardAll();
	void ResetPrivateKey();

	unsigned int& KeySize();
	void KeySize( unsigned int uiKeySize );

#if TOOLS_GUI == 1
	bool SaveState( stringstream& ss );
	bool LoadState( ifstream& ifs );
#endif

private:
	bool	m_bIncludeInTim;

	string  m_sRsaPrivateKey;  // temp - do not store
	t_stringList m_RsaPrivateKeyList;

	string  m_sECDSAPrivateKey;  // temp - do not store
	t_stringList m_ECDSAPrivateKeyList;
};

typedef list<CDigitalSignature*>           t_DigitalSignatureList;
typedef list<CDigitalSignature*>::iterator t_DigitalSignatureListIter;
