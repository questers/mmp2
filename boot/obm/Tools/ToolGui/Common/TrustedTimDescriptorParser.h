/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#include "TimDescriptorParser.h"

#if TRUSTED
#if IPPV6
#include <ipp_px.h>
#include <ippdefs.h>
#endif
#include <ippcp.h>


#include <list>
#include <iterator>
using namespace std;

typedef list<pKEY_MOD_3_4_0>           t_KeyInfoList;
typedef list<pKEY_MOD_3_4_0>::iterator t_KeyInfoListIter;

class CCommandLineParser;

class CTrustedTimDescriptorParser : public CTimDescriptorParser
{
public:
	CTrustedTimDescriptorParser(void);
	virtual ~CTrustedTimDescriptorParser(void);

	virtual void Reset(); // use to reset parser if tim descriptor read fails
	virtual bool VerifyNumberOfKeys ();
	
#if TOOLS_GUI == 1
	virtual bool ParseTrustedDescriptor (CDownloaderInterface& DownloaderInterface);
	bool ParsePrivateKeyFile (CDownloaderInterface& DownloaderInterface, CCommandLineParser& CommandLineParser);
#else
	virtual bool ParseTrustedDescriptor (CCommandLineParser& CommandLineParser);
	bool ParsePrivateKeyFile (CCommandLineParser& CommandLineParser);
#endif

	t_KeyInfoList KeyInfoList;
	PLAT_DS Ds;
	UINT_T RsaPrivateKey[MAXRSAKEYSIZEWORDS];
	UINT_T ECDSAPrivateKey[MAXECCKEYSIZEWORDS];

protected:
	bool ParseKeyInfo (KEY_MOD_3_4_0 *pKeyInfo, CTimDescriptorLine*& pLine);
    bool ParseRSAKey(CKey * pKey, KEY_MOD_3_4_0 *pKeyInfo, CTimDescriptorLine*& pLine);
    bool ParseECCKey(CKey * pKey, KEY_MOD_3_4_0 *pKeyInfo, CTimDescriptorLine*& pLine);

#if TOOLS_GUI == 1
	bool ParseDsaInfo (CDownloaderInterface& DownloaderInterface);
	bool ParseRSA (CDownloaderInterface& DownloaderInterface, CTimDescriptorLine*& pLine);
	bool ParseECDSA (CDownloaderInterface& DownloaderInterface, CTimDescriptorLine*& pLine);
#else
	bool ParseDsaInfo (CCommandLineParser& CommandLineParser);
	bool ParseRSA (CCommandLineParser& CommandLineParser, CTimDescriptorLine*& pLine);
	bool ParseECDSA (CCommandLineParser& CommandLineParser, CTimDescriptorLine*& pLine);
#endif
};

#endif // TRUSTED
