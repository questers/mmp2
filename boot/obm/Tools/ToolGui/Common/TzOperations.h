/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#include "ErdBase.h"
#include "Tim.h"

class CTzOperation : public CTimLib
{
public:
	CTzOperation();
	CTzOperation( TZ_OPERATION_SPEC_T OpId, const string & sOpIdText );
	virtual ~CTzOperation(void);

	// copy constructor
	CTzOperation( const CTzOperation& rhs );
	// assignment operator
	CTzOperation& operator=( const CTzOperation& rhs );

	bool ToBinary( ofstream& ofs );

#if TOOLS_GUI == 1
	virtual bool LoadState( ifstream& ifs );
	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	bool SetOperationID( TZ_OPERATION_SPEC_T eOpId );
	bool SetOperationID( string& sOpIdText );

	TZ_OPERATION_SPEC_T	    m_OperationId;
	string					m_sOpIdText;
	unsigned int			m_Value;

	enum eTzOperationFields { TZ_OP_ID, TZ_OP_FIELDS_MAX };
};

typedef list<CTzOperation*>           t_TzOperationList;
typedef list<CTzOperation*>::iterator t_TzOperationListIter;


class CTzOperations : public CErdBase
{
public:
	CTzOperations();
	virtual ~CTzOperations(void);

	// copy constructor
	CTzOperations( const CTzOperations& rhs );
	// assignment operator
	virtual CTzOperations& operator=( const CTzOperations& rhs );

	virtual void Reset();
		
	static const string Begin;	// "TZ Operations"
	static const string End;	// "End TZ Operations"

	virtual bool Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine );

	virtual bool ToBinary( ofstream& ofs );
	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize();
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

	unsigned int NumOps() { return (unsigned int)m_TzOperations.size(); }

#if TOOLS_GUI == 1
	virtual bool LoadState( ifstream& ifs );
	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	enum eNumFields{ OPERATIONS_MAX = 0 };
	t_TzOperationList	m_TzOperations;

	static t_TzOperationList s_DefinedTzOperations;
	static int				  s_TzOpCount;
};

