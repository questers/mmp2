/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma message("Using TBR Xfer Reserved Data")
#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "TbrXferERD.h"
#include "TimDescriptorParser.h"

#if LINUX
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

const string CXfer::Begin("Xfer");
const string CXfer::End("End Xfer");

CXfer::CXfer()
	: CErdBase( XFER_ERD, XFER_MAX )
{
	*m_FieldNames[DATA_ID] = "DATA_ID";
	*m_FieldNames[LOCATION] = "LOCATION";
}

CXfer::~CXfer()
{
}

// copy constructor
CXfer::CXfer( const CXfer& rhs )
: CErdBase( rhs )
{
	// copy constructor
}

// assignment operator
CXfer& CXfer::operator=( const CXfer& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		CErdBase::operator=( rhs );
	}
	return *this;
}


bool CXfer::ToBinary( ofstream& ofs )
{
	// validate size
	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
		return false;

	ofs << Translate(*m_FieldValues[DATA_ID]);
	ofs << Translate(*m_FieldValues[LOCATION]);

	return ofs.good();
}


int CXfer::AddPkgStrings( CReservedPackageData* pRPD )
{
	pRPD->AddData( new string( *m_FieldValues[ DATA_ID ] ), new string( "DATA_ID" ) );
	pRPD->AddData( new string( *m_FieldValues[ LOCATION ] ), new string( "LOCATION" ) );

	return PackageSize();
}

#if TOOLS_GUI == 1
bool CXfer::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;
	
	CErdBase::ToText( TimDescriptor, ss );

	if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;
	return true;
}
#endif

const string CTBRXferSet::Begin("TBR Xfer Set");
const string CTBRXferSet::End("End TBR Xfer Set");

CTBRXferSet::CTBRXferSet()
	: CErdBase( TBRXFER_ERD, TBR_XFER_SET_MAX )
{
	*m_FieldNames[XFER_LOC] = "XFER_LOC";
	*m_FieldNames[NUM_DATA_PAIRS] = "NUM_DATA_PAIRS";
}

CTBRXferSet::~CTBRXferSet()
{
	Reset();
}

// copy constructor
CTBRXferSet::CTBRXferSet( const CTBRXferSet& rhs )
: CErdBase( rhs )
{
	// need to do a deep copy of lists to avoid dangling references
	CTBRXferSet& nc_rhs = const_cast<CTBRXferSet&>(rhs);

	t_XferListIter iter = nc_rhs.Xfers.begin();
	while ( iter != nc_rhs.Xfers.end() )
	{
		Xfers.push_back( new CXfer( *(*iter) ) );
		iter++;
	}
}

// assignment operator
CTBRXferSet& CTBRXferSet::operator=( const CTBRXferSet& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		CErdBase::operator=( rhs );

		Reset();

		// need to do a deep copy of lists to avoid dangling references
		CTBRXferSet& nc_rhs = const_cast<CTBRXferSet&>(rhs);

		t_XferListIter iter = nc_rhs.Xfers.begin();
		while ( iter != nc_rhs.Xfers.end() )
		{
			Xfers.push_back( new CXfer( *(*iter) ) );
			iter++;
		}
	}
	return *this;
}

void CTBRXferSet::Reset()
{
	t_XferListIter iter = Xfers.begin();
	while ( iter != Xfers.end() )
	{
		delete *iter;
		iter++;
	}
	Xfers.clear();
}


bool CTBRXferSet::Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine )
{
	if ( pLine->m_FieldName != CTBRXferSet::Begin )
	{
		printf( "\nError: Attempt to parse TBR Xfer Set failed!" );
		return false;
	}

	if ( CErdBase::Parse( TimDescriptor, pLine ) && pLine->m_FieldName != CTBRXferSet::End )
	{
		for ( unsigned int i = 0; i < Translate(*m_FieldValues[ NUM_DATA_PAIRS ]); i++ )
		{
			CXfer* pXfer = 0;
			if ( pLine->m_FieldName == CXfer::Begin )
			{
				pXfer = new CXfer;
				if ( !pXfer->Parse( TimDescriptor, pLine ) )
				{
					printf("\nError: Parsing of Xfer failed.");
					delete pXfer;
					return false;
				}
			}
		
			if ( pXfer && pLine->m_FieldName == CXfer::End )
			{
				Xfers.push_back( pXfer );
				pXfer = 0;

				if ( pLine = TimDescriptor.GetNextLineField( pLine ) )
					// see if we have more Gpios in this GpioSet
					continue;
				else
					break;
			}
			else
			{
				// we should be at the end of the TBRXferSet now
				if ( pLine->m_FieldName == CTBRXferSet::End ) 
					break;
				else
				{
					printf("\nError: Parsing of TBR Xfer Set failed.");
					delete pXfer;
					return false;
				}
			}
		}
	}

	if ( Translate(*m_FieldValues[ NUM_DATA_PAIRS ]) != Xfers.size() )
	{
		printf("\nError: Parsing of TBR Xfer Set, NUM_DATA_PAIRS not equal to actual number ");
		printf("of Xfers defined in the TBR Xfer Set\n");
		return false;
	}

	return true;
}

bool CTBRXferSet::ToBinary( ofstream& ofs )
{
	// validate size
	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
		return false;

	bool bRet = true;
	ofs << TBR_XFER;
	ofs << PackageSize();

	ofs << Translate(*m_FieldNames[XFER_LOC]);
	ofs << Translate(*m_FieldNames[NUM_DATA_PAIRS]);

	t_XferListIter iter = Xfers.begin();
	while ( bRet && iter != Xfers.end() )
		bRet = (*iter++)->ToBinary( ofs );

	return ( ofs.good() && bRet );
}

int CTBRXferSet::AddPkgStrings( CReservedPackageData* pRPD )
{
	pRPD->PackageIdTag( HexFormattedAscii(TBR_XFER) );
	pRPD->AddData( new string( *m_FieldValues[ XFER_LOC ] ),  new string( "XFER_LOC" ) );
	pRPD->AddData( new string( *m_FieldValues[ NUM_DATA_PAIRS ] ),  new string( "NUM_DATA_PAIRS" ) );

	t_XferListIter iter = Xfers.begin();
	while ( iter != Xfers.end() )
		(*iter++)->AddPkgStrings( pRPD );

	return PackageSize();
}

unsigned int CTBRXferSet::PackageSize() 
{ 
	return (unsigned int)( 8 + // package tag id + size
						   (m_FieldValues.size()*4) // all fields
						   + Xfers.size()*8);		 // all data in data list
}


#if TOOLS_GUI == 1
bool CTBRXferSet::SaveState( stringstream& ss )
{
	unsigned int temp = 0;
	if ( CErdBase::SaveState( ss ) )
	{
		temp = (unsigned int)Xfers.size();
		SaveFieldState( ss, temp );

		t_XferListIter iter = Xfers.begin();
		while ( iter != Xfers.end() )
			if ( !(*iter++)->SaveState( ss ) )
				return false;
	}
	else
		return false;

	return true; // SUCCESS
}
#endif

#if TOOLS_GUI == 1
bool CTBRXferSet::LoadState( ifstream& ifs )
{
	bool bRet = true;

	if ( theApp.ProjectVersion >= 0x03021200 )
	{
		Reset();

		bRet = CErdBase::LoadState( ifs );

		string sbuf;
		LoadFieldState( ifs, sbuf );
		unsigned int XfersSize = Translate(sbuf);

		for ( unsigned int i = 0; i < XfersSize; i++ )
		{
			LoadFieldState( ifs, sbuf );
			CErdBase::ERD_PKG_TYPE ErdType = CErdBase::ERD_PKG_TYPE(Translate(sbuf));
			if ( ErdType == XFER_ERD )
			{
				CXfer* pXfer = new CXfer;
				bRet = pXfer->LoadState( ifs );
				if ( !bRet )
					break;
				
				Xfers.push_back(pXfer);
			}
			else
				return false;
		}
	}
	
	return bRet && ifs.good() && !ifs.fail(); // success
}
#endif

#if TOOLS_GUI == 1
bool CTBRXferSet::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	bool bRet = true;
	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;

	CErdBase::ToText( TimDescriptor, ss );
	
	t_XferListIter iter = Xfers.begin();
	while ( bRet && iter != Xfers.end() )
	{
//		if ( !TimDescriptor.AttachCommentedObject( ss, *iter, (*iter)->Begin.c_str(), string("") ) )
//		{
			bRet = (*iter)->ToText( TimDescriptor, ss );
//		}
		iter++;
	}

	if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;

	return bRet;
}
#endif
