/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once

#include "TimDescriptor.h"
#include "ReservedPackageData.h"
#include "ImageDescription.h"
#include "Partition.h"

#if EXTENDED_RESERVED_DATA
#include "ExtendedReservedData.h"
#include "ErdBase.h"
#endif

#include "TimLib.h"
#include "TimDescriptorLine.h"

// forward declarations
class CCommandLineParser;
class CDownloaderInterface;

//The following general steps will be performed in order to parse the non-trusted/trusted descriptor text file:
//1. Read version information. Store in data struct.
//2. Check to see if non-trusted or trusted.
//3. Read and store image information.
//Non-trusted only:
//4. If there is any reserved data, read, verify and store reserved data.
//Trusted only:
//5. If there are any keys, read and store key information.
//6. If there is any reserved data, read, verify and store reserved data. 
//7. Read and store DSA algorithm.

class CTimDescriptorParser : public CTimLib
{
public:
	CTimDescriptorParser(void);
	virtual ~CTimDescriptorParser(void);

	CTimDescriptor& TimDescriptor(){ return m_TimDescriptor; }
	PartitionTable_T& PartitionTable(){ return m_PartitionHeader; }
	P_PartitionInfo_T& PartitionInfo(){ return m_pPartitionInfo; }
	virtual void Reset(); // use to reset parser if tim descriptor read fails

	bool GetTimDescriptorLines(CCommandLineParser& CommandLineParser);
	bool GetTimDescriptorLines(t_stringList& Lines);
#if TOOLS_GUI == 1
	virtual bool ParseDescriptor(CDownloaderInterface& DownloaderInterface);
#else
	virtual bool ParseDescriptor(CCommandLineParser& CommandLineParser);
#endif
	bool ParsePartitionTextFile(ifstream& ifsPartitionFile, PartitionTable_T& PartitionHeader, P_PartitionInfo_T& pPartitionInfo);

	bool FillDataArrays (CTimDescriptorLine*& pLine,DWORD *dwArray,int iSize);
	bool FillDataArrays (ifstream& ifs,DWORD *dwArray,int iSize); 

	string& TimDescriptorBinFilePath (){ return m_sTimBinFilename; }

protected:
	ifstream& OpenTimDescriptorTextFile ( ios_base::openmode mode = ios_base::in );
	void CloseTimDescriptorTextFile();

#if TOOLS_GUI == 1
	virtual bool ParseTrustedDescriptor (CDownloaderInterface& DownloaderInterface);
#else
	virtual bool ParseTrustedDescriptor (CCommandLineParser& CommandLineParser);
#endif
	virtual bool VerifyNumberOfKeys (){ return false; }
	
#if TOOLS_GUI == 1
	bool ParseNonTrustedDescriptor (CDownloaderInterface& DownloaderInterface);
#else
	bool ParseNonTrustedDescriptor (CCommandLineParser& CommandLineParser);
#endif

	// common Functions
	bool VerifySizeOfTim( CCommandLineParser& CommandLineParser );
	bool VerifyNonTrusted();
	bool VerifyNumberOfImages();
	bool VerifyDescriptorIntegrity();
	bool VerifyReservedData();
	bool ParseReservedData();
	bool ParseImageInfo (CImageDescription& ImageDesc, CTimDescriptorLine*& pLine);
	bool ValidFieldIdx( string& sFieldName, unsigned int& idx, t_stringVector& Fields );	

#if EXTENDED_RESERVED_DATA
#if 0 // migration disabled for now
	bool MigrateToExtendedReservedData( CReservedPackageData* pPackageData );
#endif
	bool ParseERDPackage( CTimDescriptorLine*& pLine, CErdBase*& pErd, const string& sBegin, const string& sEnd );

#if TOOLS_GUI == 1
	bool ParseExtendedReservedData(CDownloaderInterface& DownloaderInterface);
#else
	bool ParseExtendedReservedData(CCommandLineParser& CommandLineParser);
#endif

	bool SearchFieldValue( string& sLine, unsigned int& idx, t_stringVector& Fields, DWORD& dwValue );
	bool ParseDDRPackage( CTimDescriptorLine*& pLine, t_PairList& Fields, t_stringVector& g_FieldsNames, 
						  unsigned int& idx, CExtendedReservedData& Ext,
						  void (CExtendedReservedData::*AddField)( std::pair< unsigned int, unsigned int >*& ),
						  string& sEndField, bool& bRetry);
#endif

	CTimDescriptor			m_TimDescriptor;

//#if TOOLS_GUI == 0
	PartitionTable_T		m_PartitionHeader;
	P_PartitionInfo_T		m_pPartitionInfo;
//#endif

	string			m_sTimBinFilename;
	string			m_sTimFilePath;
	string			m_sImageFilename; 
	string			m_sImageOutFilename;

	ifstream		m_ifsTimDescriptorTxtFile;
};
