/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once

#include "TimLib.h"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

typedef list<string*>           t_stringList;
typedef list<string*>::iterator t_stringListIter;

typedef vector<string*>           t_stringVector;
typedef vector<string*>::iterator t_stringVectorIter;

typedef vector<void*>           t_ObjectVector;
typedef vector<void*>::iterator t_ObjectVectorIter;

class CTimDescriptorParser;

class CTimDescriptorLine : public CTimLib
{
public:
	CTimDescriptorLine(void);
	virtual ~CTimDescriptorLine(void);

	// copy constructor
	CTimDescriptorLine( const CTimDescriptorLine& rhs );
	// assignment operator
	CTimDescriptorLine& operator=( const CTimDescriptorLine& rhs );

	bool ParseLine( ifstream& ifs, CTimDescriptorParser& Parser );
	bool ParseLine( t_stringList& Lines );

#if TOOLS_GUI == 1
	bool AddRef( void* pObject );
	bool RemoveRef( void* pObject );
	void PreCommentLinesText( stringstream& ss );
	void PostCommentLinesText( stringstream& ss );
	bool IsObject( void* pTestObj );
	bool SaveState( stringstream& ss );
	bool LoadState( ifstream& ifs );
	bool HasObjects() { return ( m_Objects.size() > 0 ); }
#else
	bool IsObject( void* pTestObj ){ return false; }
#endif

	void DiscardAll();

#if TOOLS_GUI == 1
	t_stringVector	m_PreCommentLines;
	t_stringVector	m_PostCommentLines;

	string			m_LeadPadding;	// the white space preceding the FieldName or comment
	string			m_ValuePadding;	// the white space between the FieldName and the FieldValue
#endif

	string			m_FieldName;
	string			m_FieldValue;

#if TOOLS_GUI == 1
private:
	t_ObjectVector	m_Objects;
#endif
};

typedef vector<CTimDescriptorLine*>	t_TimDescriptorLines;
typedef vector<CTimDescriptorLine*>::iterator t_TimDescriptorLinesIter;
