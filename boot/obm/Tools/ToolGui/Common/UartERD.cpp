/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma message("Using Uart Reserved Data")
#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "UartERD.h"

const string CUart::Begin("Uart");
const string CUart::End("End Uart");

CUart::CUart()
	: CErdBase( UART_ERD, UART_MAX )
{
	*m_FieldNames[PORT] = "PORT";
	*m_FieldNames[ENABLED] = "ENABLED";
}

CUart::~CUart()
{
}

// copy constructor
CUart::CUart( const CUart& rhs )
: CErdBase( rhs )
{
	// copy constructor
}

// assignment operator
CUart& CUart::operator=( const CUart& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		CErdBase::operator=( rhs );
	}
	return *this;
}

bool CUart::ToBinary( ofstream& ofs )
{
	// validate size
	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
		return false;

	ofs << UARTID;
	ofs << PackageSize();
	ofs << Translate(*m_FieldValues[PORT]);
	ofs << Translate(*m_FieldValues[ENABLED]);

	return ofs.good();
}

int CUart::AddPkgStrings( CReservedPackageData* pRPD )
{
	pRPD->PackageIdTag( HexFormattedAscii(UARTID) );
	pRPD->AddData( new string( *m_FieldValues[ PORT ] ), new string( "PORT" ) );
	pRPD->AddData( new string( *m_FieldValues[ ENABLED ] ), new string( "ENABLED" ) );

	return PackageSize();
}

CUart* CUart::Migrate( CReservedPackageData* pRPD )
{
	CUart* pUart = new CUart;

	size_t size = pRPD->PackageDataList().size();
	// make sure the data is the correct size for a USB package
	if ( size == 2 )
	{
		t_stringListIter Iter = pRPD->PackageDataList().begin();
		*(pUart->m_FieldValues[ PORT ]) = *(*Iter++);
		*(pUart->m_FieldValues[ ENABLED ]) =*(*Iter++);
	}

	return pUart;
}

#if TOOLS_GUI == 1
bool CUart::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;

	CErdBase::ToText( TimDescriptor, ss );

	if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;
	return true;
}
#endif


