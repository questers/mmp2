/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#if TOOLS_GUI == 1
#include <stdafx.h>
#endif

#include "TimDescriptorLine.h"
#include "TimDescriptorParser.h"

CTimDescriptorLine::CTimDescriptorLine(void)
: CTimLib()
{
}

CTimDescriptorLine::~CTimDescriptorLine(void)
{
	DiscardAll();
}

void CTimDescriptorLine::DiscardAll()
{
#if TOOLS_GUI == 1
	m_Objects.clear();

	t_stringVectorIter Iter = m_PreCommentLines.begin();
	while ( Iter != m_PreCommentLines.end() )
	{
		delete *Iter;
		Iter++;
	}
	m_PreCommentLines.clear();

	Iter = m_PostCommentLines.begin();
	while ( Iter != m_PostCommentLines.end() )
	{
		delete *Iter;
		Iter++;
	}
	m_PostCommentLines.clear();

	m_LeadPadding = "";
	m_ValuePadding = "";
#endif
}


// copy constructor
CTimDescriptorLine::CTimDescriptorLine( const CTimDescriptorLine& rhs )
: CTimLib( rhs )
{
	m_FieldName = rhs.m_FieldName;
	m_FieldValue = rhs.m_FieldValue;
#if TOOLS_GUI == 1
	m_LeadPadding = rhs.m_LeadPadding;
	m_ValuePadding = rhs.m_ValuePadding;

	CTimDescriptorLine& nc_rhs = const_cast<CTimDescriptorLine&>(rhs);

	// m_pObject is only for comparison and should not be deep copied
	// all copies of this object will have the same pObject value
	t_ObjectVectorIter ObjIter = nc_rhs.m_Objects.begin();
	while ( ObjIter != nc_rhs.m_Objects.end() )
	{
		m_Objects.push_back( *ObjIter );
		ObjIter++;
	}

	// deep copy the rest
	t_stringVectorIter Iter = nc_rhs.m_PreCommentLines.begin();
	while ( Iter != nc_rhs.m_PreCommentLines.end() )
	{
		m_PreCommentLines.push_back( new string( *(*Iter) ) );
		Iter++;
	}

	Iter = nc_rhs.m_PostCommentLines.begin();
	while ( Iter != nc_rhs.m_PostCommentLines.end() )
	{
		m_PostCommentLines.push_back( new string( *(*Iter) ) );
		Iter++;
	}
#endif
}

// assignment operator
CTimDescriptorLine& CTimDescriptorLine::operator=( const CTimDescriptorLine& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		CTimLib::operator=(rhs);

		DiscardAll();

		m_FieldName = rhs.m_FieldName;
		m_FieldValue = rhs.m_FieldValue;
#if TOOLS_GUI == 1
		m_LeadPadding = rhs.m_LeadPadding;
		m_ValuePadding = rhs.m_ValuePadding;
		CTimDescriptorLine& nc_rhs = const_cast<CTimDescriptorLine&>(rhs);

		// m_pObject is only for comparison and should not be deep copied
		// all copies of this object will have the same pObject value
		t_ObjectVectorIter ObjIter = nc_rhs.m_Objects.begin();
		while ( ObjIter != nc_rhs.m_Objects.end() )
		{
			m_Objects.push_back( *ObjIter );
			ObjIter++;
		}

		// deep copy the rest
		t_stringVectorIter Iter = nc_rhs.m_PreCommentLines.begin();
		while ( Iter != nc_rhs.m_PreCommentLines.end() )
		{
			m_PreCommentLines.push_back( new string( *(*Iter) ) );
			Iter++;
		}

		Iter = nc_rhs.m_PostCommentLines.begin();
		while ( Iter != nc_rhs.m_PostCommentLines.end() )
		{
			m_PostCommentLines.push_back( new string( *(*Iter) ) );
			Iter++;
		}
#endif
	}

	return *this;
}


bool CTimDescriptorLine::ParseLine( ifstream& ifs, CTimDescriptorParser& Parser )
{
	string sValue;
	size_t nPos = 0;
	size_t nEndPos = 0;

	// get PreCommentLines
	while ( !ifs.eof() && ifs.good() && Parser.GetNextLine( ifs, sValue, true ) )
	{
		// if 1st char (after leading white space) is a ;, 
		//  then line is a comment so ignore leading whitespace
		nPos = sValue.find_first_of( " \n\r\t" );
		nEndPos = sValue.find_first_not_of( " \n\r\t" );
		if ( nPos == 0 && nEndPos != string::npos )
		{
#if TOOLS_GUI == 1
			// save leading white space
			m_LeadPadding = sValue.substr(0, nEndPos );
#endif
			// remove leading whitespace
			sValue = sValue.substr( nEndPos );
		}

		if (!(nPos == 0 && nEndPos == string::npos))
			if ( sValue.length() > 0 && sValue[0] != ';')
				break; // not a comment, so process as a field/value

#if TOOLS_GUI == 1
		m_PreCommentLines.push_back( new string( sValue ) );
#endif
	}

	if ( !ifs.eof() && (ifs.bad() || ifs.fail()) )
		return false;

	// parse out post comment
	if ( (nPos = sValue.find( ';' )) != string::npos )
	{
		// include ws between field and post comment
		while( nPos-1 > 0 )
		{
			if ( !isspace(0x000000FF & sValue[nPos-1]) )
				break;
			nPos--;
		}

#if TOOLS_GUI == 1
		string* pPostComment = new string(sValue.substr(nPos));
		m_PostCommentLines.push_back( pPostComment );
#endif
		sValue = sValue.substr(0, nPos);
	}

	// parse out field name and field value
	if ( (nPos = sValue.find( ':' )) != string::npos )
	{
		m_FieldName = sValue.substr(0, nPos);
		m_FieldValue = sValue.substr(nPos+1);
		if ( string::npos != ( nPos = m_FieldValue.find_first_not_of( ": \n\r\t" ) ) )
		{
#if TOOLS_GUI == 1
			if ( nPos > 0 )
				m_ValuePadding = m_FieldValue.substr(0, nPos-1);
#endif
			m_FieldValue = m_FieldValue.substr(nPos);
		}
	}
	else 
	{
		// no fieldname delimiter, so assume it is all hex value(s)
		// like in the reserved data or the key data areas
		m_FieldName = "";
		m_FieldValue = sValue;
	}

	// this ensures no white space gets into field name and values
	// and fixes issues with Linux line endings
	m_FieldName = TrimWS( m_FieldName );
	m_FieldValue = TrimWS( m_FieldValue );

	return true;
}

bool CTimDescriptorLine::ParseLine( t_stringList& Lines )
{
	string sValue;
	size_t nPos = 0;
	size_t nEndPos = 0;

	// get PreCommentLines
	t_stringListIter iter;
	//reset to begin of list on each loop
	while ( iter = Lines.begin(), iter != Lines.end() )
	{
		sValue = *(*iter);
		//remove the line being processed
		delete (*iter);
		Lines.remove(*iter);

		// if 1st char (after leading white space) is a ;, 
		//  then line is a comment so ignore leading whitespace
		nPos = sValue.find_first_of( " \n\r\t" );
		nEndPos = sValue.find_first_not_of( " \n\r\t" );
		if ( nPos == 0 && nEndPos != string::npos )
		{
#if TOOLS_GUI == 1
			// save leading white space
			m_LeadPadding = sValue.substr(0, nEndPos );
#endif
			// remove leading whitespace
			sValue = sValue.substr( nEndPos );
		}

		if (!(nPos == 0 && nEndPos == string::npos))
			if ( sValue.length() > 0 && sValue[0] != ';')
				break; // not a comment, so process as a field/value

#if TOOLS_GUI == 1
		m_PreCommentLines.push_back( new string( sValue ) );
#endif
	}

	// parse out post comment
	if ( (nPos = sValue.find( ';' )) != string::npos )
	{
		// include ws between field and post comment
		while( nPos-1 > 0 )
		{
			if ( !isspace(0x000000FF & sValue[nPos-1]) )
				break;
			nPos--;
		}

#if TOOLS_GUI == 1
		string* pPostComment = new string(sValue.substr(nPos));
		m_PostCommentLines.push_back( pPostComment );
#endif
		sValue = sValue.substr(0, nPos);
	}

	// parse out field name and field value
	if ( (nPos = sValue.find( ':' )) != string::npos )
	{
		m_FieldName = sValue.substr(0, nPos);
		m_FieldValue = sValue.substr(nPos+1);
		if ( string::npos != ( nPos = m_FieldValue.find_first_not_of( ": \n\r\t" ) ) )
		{
#if TOOLS_GUI == 1
			if ( nPos > 0 )
				m_ValuePadding = m_FieldValue.substr(0, nPos-1);
#endif
			m_FieldValue = m_FieldValue.substr(nPos);
		}
	}
	else 
	{
		// no fieldname delimiter, so assume it is all hex value(s)
		// like in the reserved data or the key data areas
		m_FieldName = "";
		m_FieldValue = sValue;
	}

	// this ensures no white space gets into field name and values
	// and fixes issues with Linux line endings
	m_FieldName = TrimWS( m_FieldName );
	m_FieldValue = TrimWS( m_FieldValue );

	return true;
}


#if TOOLS_GUI == 1
bool CTimDescriptorLine::IsObject( void* pTestObj )
{
	t_ObjectVectorIter ObjIter = m_Objects.begin();
	while ( ObjIter != m_Objects.end() )
	{
		if ( *ObjIter == pTestObj )
			return true;
		ObjIter++;
	}

	return false;
}

void CTimDescriptorLine::PreCommentLinesText( stringstream& ss )
{
	t_stringVectorIter Iter = m_PreCommentLines.begin();
	while ( Iter != m_PreCommentLines.end() )
		ss << *(*Iter++) << endl;
}

void CTimDescriptorLine::PostCommentLinesText( stringstream& ss )
{
	t_stringVectorIter Iter = m_PostCommentLines.begin();
	while ( Iter != m_PostCommentLines.end() )
	{
		ss << *(*Iter++);
		// add a NL if there is another comment line to output
		if ( Iter != m_PostCommentLines.end() )
			ss << endl;
	}
}

bool CTimDescriptorLine::AddRef( void* pObject )
{
	t_ObjectVectorIter ObjIter = m_Objects.begin();
	while ( ObjIter != m_Objects.end() )
	{
		// if already in list, no need to add
		if ( *ObjIter == pObject )
			return false;

		ObjIter++;
	}

	// pObject not found, so add
	m_Objects.push_back( pObject );

	return true;
}

bool CTimDescriptorLine::RemoveRef( void* pObject )
{
	t_ObjectVectorIter ObjIter = m_Objects.begin();
	while ( ObjIter != m_Objects.end() )
	{
		if ( *ObjIter == pObject )
		{
			m_Objects.erase( ObjIter );
			return true;
		}

		ObjIter++;
	}

	return false;
}

bool CTimDescriptorLine::SaveState( stringstream& ss )
{
	ss << m_PreCommentLines.size() << endl;
	t_stringVectorIter Iter = m_PreCommentLines.begin();
	while ( Iter != m_PreCommentLines.end() )
		ss << *(*Iter++) << endl;

	ss << m_PostCommentLines.size() << endl;
	Iter = m_PostCommentLines.begin();
	while ( Iter != m_PostCommentLines.end() )
		ss << *(*Iter++) << endl;

	ss << m_LeadPadding << endl;	// the white space preceding the FieldName or comment
	ss << m_ValuePadding << endl;	// the white space between the FieldName and the FieldValue
	ss << m_FieldName << endl;
	ss << m_FieldValue << endl;

	return true;
}

bool CTimDescriptorLine::LoadState( ifstream& ifs )
{
	string sbuf;
	::getline( ifs, sbuf );
	int PreCommentLinesSize = Translate(sbuf);
	for ( int i = 0; i < PreCommentLinesSize; i++ )
	{
		::getline( ifs, sbuf );
		m_PreCommentLines.push_back( new string( sbuf ) );
	}
	
	::getline( ifs, sbuf );
	int PostCommentLinesSize = Translate(sbuf);
	for ( int i = 0; i < PostCommentLinesSize; i++ )
	{
		::getline( ifs, sbuf );
		m_PostCommentLines.push_back( new string( sbuf ) );
	}

	::getline( ifs, m_LeadPadding );	// the white space preceding the FieldName or comment
	::getline( ifs, m_ValuePadding );	// the white space between the FieldName and the FieldValue
	::getline( ifs, m_FieldName );
	::getline( ifs, m_FieldValue );

	return true;
}

#endif
