/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif
#include "DigitalSignature.h"

CDigitalSignature::CDigitalSignature(void)
    : CKey()
{
    string HashId ("SHA-160");
    CKey::HashAlgorithmId( HashId );

    m_bIncludeInTim = false;
    m_bChanged = false;
}

CDigitalSignature::~CDigitalSignature(void)
{
    DiscardAll();
}

CDigitalSignature::CDigitalSignature( const CDigitalSignature& rhs )
    : CKey(rhs)
{
    // copy constructor
    CDigitalSignature& nc_rhs = const_cast<CDigitalSignature&>(rhs);

#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_RsaPrivateKeyList )) != 0 )
        pLine->AddRef( &m_RsaPrivateKeyList );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_ECDSAPrivateKeyList )) != 0 )
        pLine->AddRef( &m_ECDSAPrivateKeyList );
#endif

    m_sRsaPrivateKey = rhs.m_sRsaPrivateKey;
    m_sECDSAPrivateKey = rhs.m_sECDSAPrivateKey;
    m_bIncludeInTim = rhs.m_bIncludeInTim;

    // need to do a deep copy of lists to avoid dangling references
    t_stringListIter iterDS = nc_rhs.m_RsaPrivateKeyList.begin();
    while( iterDS != nc_rhs.m_RsaPrivateKeyList.end() )
    {
        string* pData = new string( *(*iterDS ));
        m_RsaPrivateKeyList.push_back( pData );
        iterDS++;
    }

    // need to do a deep copy of lists to avoid dangling references
    t_stringListIter iterECDSA = nc_rhs.m_ECDSAPrivateKeyList.begin();
    while( iterECDSA != nc_rhs.m_ECDSAPrivateKeyList.end() )
    {
        string* pData = new string( *(*iterECDSA ));
        m_ECDSAPrivateKeyList.push_back( pData );
        iterECDSA++;
    }
}

CDigitalSignature& CDigitalSignature::operator=( const CDigitalSignature& rhs ) 
{
    // assignment operator
    if ( &rhs != this )
    {
        CKey::operator=(rhs);

        CDigitalSignature& nc_rhs = const_cast<CDigitalSignature&>(rhs);

        // delete the existing list and recreate a new one
        DiscardAll();

#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_RsaPrivateKeyList )) != 0 )
            pLine->AddRef( &m_RsaPrivateKeyList );

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_ECDSAPrivateKeyList )) != 0 )
            pLine->AddRef( &m_ECDSAPrivateKeyList );
#endif

        m_sRsaPrivateKey = rhs.m_sRsaPrivateKey;
        m_sECDSAPrivateKey = rhs.m_sECDSAPrivateKey;
        m_bIncludeInTim = rhs.m_bIncludeInTim;

        // need to do a deep copy of lists to avoid dangling references
        t_stringListIter iterDS = nc_rhs.m_RsaPrivateKeyList.begin();
        while( iterDS != nc_rhs.m_RsaPrivateKeyList.end() )
        {
            string* pData = new string( *(*iterDS ));
            m_RsaPrivateKeyList.push_back( pData );
            iterDS++;
        }

        // need to do a deep copy of lists to avoid dangling references
        t_stringListIter iterECDSA = nc_rhs.m_ECDSAPrivateKeyList.begin();
        while( iterECDSA != nc_rhs.m_ECDSAPrivateKeyList.end() )
        {
            string* pData = new string( *(*iterECDSA ));
            m_ECDSAPrivateKeyList.push_back( pData );
            iterECDSA++;
        }
    }

    return *this;
}


void CDigitalSignature::DiscardAll()
{
#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_RsaPrivateKeyList )) != 0 )
        pLine->RemoveRef( &m_RsaPrivateKeyList );

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_ECDSAPrivateKeyList )) != 0 )
        pLine->RemoveRef( &m_ECDSAPrivateKeyList );
#endif

    // delete all RsaPrivateKeyList strings
    t_stringListIter iterDS = m_RsaPrivateKeyList.begin();
    while( iterDS != m_RsaPrivateKeyList.end() )
    {
        delete *iterDS;
        iterDS++;
    }
    m_RsaPrivateKeyList.clear();

    // delete all ECDSAPrivateKeyList strings
    t_stringListIter iterECDSA = m_ECDSAPrivateKeyList.begin();
    while( iterECDSA != m_ECDSAPrivateKeyList.end() )
    {
        delete *iterECDSA;
        iterECDSA++;
    }
    m_ECDSAPrivateKeyList.clear();

    // do not call CKey::DiscardAll from here
}

void CDigitalSignature::ResetPrivateKey()
{
    t_stringListIter iterDS = m_RsaPrivateKeyList.begin();
    while( iterDS != m_RsaPrivateKeyList.end() )
    {
        *(*iterDS) = "0x00000000";
        iterDS++;
    }

    t_stringListIter iterECDSA = m_ECDSAPrivateKeyList.begin();
    while( iterECDSA != m_ECDSAPrivateKeyList.end() )
    {
        *(*iterECDSA) = "0x00000000";
        iterECDSA++;
    }

    m_bChanged = true;
}

#if TOOLS_GUI == 1
bool CDigitalSignature::SaveState( stringstream& ss )
{
    unsigned int temp = 0;

    SaveFieldState( ss, (unsigned int&)m_bIncludeInTim );

    temp = (unsigned int)m_RsaPrivateKeyList.size();
    SaveFieldState( ss, temp );
    t_stringListIter iterDS = m_RsaPrivateKeyList.begin();
    while( iterDS != m_RsaPrivateKeyList.end() )
        SaveFieldState( ss, *(*iterDS++) );

    temp = (unsigned int)m_ECDSAPrivateKeyList.size();
    SaveFieldState( ss, temp );
    t_stringListIter iterECDSA = m_ECDSAPrivateKeyList.begin();
    while( iterECDSA != m_ECDSAPrivateKeyList.end() )
        SaveFieldState( ss, *(*iterECDSA++) );

    return CKey::SaveState( ss );
}

bool CDigitalSignature::LoadState( ifstream& ifs )
{
    DiscardAll();

    string sbuf;

    if ( theApp.ProjectVersion < 0x03030115 )
        LoadFieldState( ifs, sbuf ); // m_DsaAlgorithm not needed anymore, m_KeyAlgorithm used instead

    if ( theApp.ProjectVersion < 0x03020208 )
        LoadFieldState( ifs, sbuf ); //m_uiModulusSize = atoi(sbuf.c_str());

    LoadFieldState( ifs, (unsigned int&)m_bIncludeInTim );
    
    LoadFieldState( ifs, sbuf ); 
    int iRsaPrivateKeyListSize = Translate(sbuf);
    while ( iRsaPrivateKeyListSize > 0 )
    {
        string* psData = new string(sbuf);
        LoadFieldState( ifs, *psData ); 
        m_RsaPrivateKeyList.push_back( psData  );
        iRsaPrivateKeyListSize--;
    }

    if ( theApp.ProjectVersion >= 0x03021600 )
    {
        LoadFieldState( ifs, sbuf ); 
        int iECDSAPrivateKeyListSize = Translate(sbuf);
        while ( iECDSAPrivateKeyListSize > 0 )
        {
            string* psData = new string(sbuf);
            LoadFieldState( ifs, *psData ); 
            m_ECDSAPrivateKeyList.push_back( psData  );
            iECDSAPrivateKeyListSize--;
        }
    }

    return CKey::LoadState( ifs );
}
#endif

string& CDigitalSignature::RsaPrivateKey()
{
    m_sRsaPrivateKey = "";
    int nPerLine = 0;

    t_stringListIter iterDS = m_RsaPrivateKeyList.begin();
    while( iterDS != m_RsaPrivateKeyList.end() )
    {
        m_sRsaPrivateKey += *(*iterDS);
        if ( ++nPerLine % 4 == 0 ) // 4 is # of words per line
        {
            m_sRsaPrivateKey += "\n";
        }
        else
            m_sRsaPrivateKey += " ";

        iterDS++;
    }

    return m_sRsaPrivateKey;
}


string& CDigitalSignature::RsaPrivateKeyPacked()
{
    m_sRsaPrivateKey = "";
    int nPerLine = 0;

    unsigned int uiActiveSize = ActiveSizeOfList( m_RsaPrivateKeyList );

    t_stringListIter iterDS = m_RsaPrivateKeyList.begin();
    while( iterDS != m_RsaPrivateKeyList.end() && uiActiveSize > 0 )
    {
        uiActiveSize -= 32;

        m_sRsaPrivateKey += *(*iterDS);
        if ( ++nPerLine % 4 == 0 || uiActiveSize == 0 ) // 4 is # of words per line
        {
            m_sRsaPrivateKey += "\n";
        }
        else
            m_sRsaPrivateKey += " ";

        iterDS++;
    }

    return m_sRsaPrivateKey;
}

string& CDigitalSignature::ECDSAPrivateKey()
{
    m_sECDSAPrivateKey = "";
    int nPerLine = 0;

    t_stringListIter iterECDSA = m_ECDSAPrivateKeyList.begin();
    while( iterECDSA != m_ECDSAPrivateKeyList.end() )
    {
        m_sECDSAPrivateKey += *(*iterECDSA);
        if ( ++nPerLine % 4 == 0 ) // 4 is # of words per line
        {
            m_sECDSAPrivateKey += "\n";
        }
        else
            m_sECDSAPrivateKey += " ";

        iterECDSA++;
    }

    return m_sECDSAPrivateKey;
}


string& CDigitalSignature::ECDSAPrivateKeyPacked()
{
    m_sECDSAPrivateKey = "";
    int nPerLine = 0;

    unsigned int uiActiveSize = ActiveSizeOfList( m_ECDSAPrivateKeyList );

    t_stringListIter iterECDSA = m_ECDSAPrivateKeyList.begin();
    while( iterECDSA != m_ECDSAPrivateKeyList.end() && uiActiveSize > 0 )
    {
        uiActiveSize -= 32;

        m_sECDSAPrivateKey += *(*iterECDSA);
        if ( ++nPerLine % 4 == 0 || uiActiveSize == 0 ) // 4 is # of words per line
        {
            m_sECDSAPrivateKey += "\n";
        }
        else
            m_sECDSAPrivateKey += " ";

        iterECDSA++;
    }

    return m_sECDSAPrivateKey;
}


unsigned int& CDigitalSignature::KeySize() 
{ 
    return CKey::KeySize(); 
}

void CDigitalSignature::KeySize( unsigned int uiKeySize )
{
    // changing the size results in expansion or reduction in exponent and modulus
    if ( (unsigned int)(m_RsaPrivateKeyList.size()) < ((uiKeySize+31)/32) )
    {
        // expand the lists
        while ( (unsigned int)(m_RsaPrivateKeyList.size()) < ((uiKeySize+31)/32) )
        {
            m_RsaPrivateKeyList.push_back( new string("0x00000000") );
            m_bChanged = true;
        }

        while ( (unsigned int)(m_ECDSAPrivateKeyList.size()) < ((uiKeySize+31)/32) )
        {
            m_ECDSAPrivateKeyList.push_back( new string("0x00000000") );
            m_bChanged = true;
        }
    }
    else if ( (unsigned int)(m_RsaPrivateKeyList.size()) > ((uiKeySize+31)/32) )
    {
        // shrink the lists
        while ( (unsigned int)(m_RsaPrivateKeyList.size()) > ((uiKeySize+31)/32) )
        {
            delete m_RsaPrivateKeyList.back();
            m_RsaPrivateKeyList.pop_back();

            m_bChanged = true;
        }

        // shrink the lists
        while ( (unsigned int)(m_ECDSAPrivateKeyList.size()) > ((uiKeySize+31)/32) )
        {
            delete m_ECDSAPrivateKeyList.back();
            m_ECDSAPrivateKeyList.pop_back();

            m_bChanged = true;
        }
    }

    // update the CKey Public Key Exponent and RSA Modulus lists
    CKey::KeySize( uiKeySize );
    CKey::PublicKeySize( uiKeySize );
}
