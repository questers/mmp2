/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#ifndef __MORONA_PROCESSOR_ENUMS_H__
#define __MORONA_PROCESSOR_ENUMS_H__

#ifdef MORONA
typedef enum MORONA_CLOCK_ID_E
{
	MORONA_CLOCK_ID_MCU,
	MORONA_CLOCK_ID_I2C,
	MORONA_CLOCK_ID_E_MAX	// MUST BE FINAL ENUM IN MORONA_CLOCK_ID_T
} MORONA_CLOCK_ID_T;

typedef enum MORONA_FREQ_ID_E
{
	MORONA_FREQ_ID_MCU,
	MORONA_FREQ_ID_E_MAX	// MUST BE FINAL ENUM IN MORONA_FREQ_ID_T
} MORONA_FREQ_ID_T;

typedef enum MORONA_MCU_REGID_E
{
	MORONA_SDRCFGREG0_ID,
	MORONA_SDRCFGREG1_ID,
	MORONA_SDRTMGREG1_ID,
	MORONA_SDRTMGREG2_ID,
	MORONA_SDRTMGREG3_ID,
	MORONA_SDRTMGREG4_ID,
	MORONA_SDRTMGREG5_ID,
	MORONA_SDRCTLREG1_ID,
	MORONA_SDRCTLREG2_ID,
	MORONA_SDRCTLREG3_ID,
	MORONA_SDRCTLREG4_ID,
	MORONA_SDRCTLREG5_ID,
	MORONA_SDRCTLREG6_ID,
	MORONA_SDRCTLREG7_ID,
	MORONA_MCBCTLREG1_ID,
	MORONA_PHYCTLREG3_ID,
	MORONA_PHYCTLREG7_ID,
	MORONA_PHYCTLREG8_ID,
	MORONA_PHYCTLREG9_ID,
	MORONA_PHYCTLREG11_ID,
	MORONA_PHYCTLREG13_ID,
	MORONA_PHYCTLREG14_ID,
	MORONA_DLLCTLREG1_ID,
	MORONA_DLLCTLREG2_ID,
	MORONA_DLLCTLREG3_ID,
	MORONA_ADRMAPREG0_ID,
	MORONA_ADRMAPREG1_ID,
	MORONA_USRCMDREG0_ID,
	MORONA_MCU_REGID_E_MAX	// MUST BE FINAL ENUM IN MORONA_MCU_REGID_T
} MORONA_MCU_REGID_T;

#endif
#endif

