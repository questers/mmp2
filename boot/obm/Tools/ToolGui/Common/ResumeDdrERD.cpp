/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma message("Using Resume DDR Reserved Data")
#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "ResumeDdrERD.h"

const string CResumeDdr::Begin("Resume DDR");
const string CResumeDdr::End("End Resume DDR");

CResumeDdr::CResumeDdr()
	: CErdBase( RESUME_DDR_ERD, RESUME_DDR_MAX )
{
	*m_FieldNames[RESUME_DDR_ADDR] = "RESUME_DDR_ADDRESS";
	*m_FieldNames[RESUME_DDR_PARAM] = "RESUME_DDR_PARAM";
	*m_FieldNames[RESUME_DDR_FLAG] = "RESUME_DDR_FLAG";
}

CResumeDdr::~CResumeDdr()
{
}

// copy constructor
CResumeDdr::CResumeDdr( const CResumeDdr& rhs )
: CErdBase( rhs )
{
	// copy constructor
}

// assignment operator
CResumeDdr& CResumeDdr::operator=( const CResumeDdr& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		CErdBase::operator=( rhs );
	}
	return *this;
}

bool CResumeDdr::ToBinary( ofstream& ofs )
{
	// validate size
	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
		return false;

	ofs << RESUMEBLID;
	ofs << PackageSize();
	ofs << Translate(*m_FieldValues[RESUME_DDR_ADDR]);
	ofs << Translate(*m_FieldValues[RESUME_DDR_PARAM]);
	ofs << Translate(*m_FieldValues[RESUME_DDR_FLAG]);

	return ofs.good();
}


int CResumeDdr::AddPkgStrings( CReservedPackageData* pRPD )
{
	pRPD->PackageIdTag( HexFormattedAscii(RESUMEBLID) );

	pRPD->AddData( new string( *m_FieldValues[ RESUME_DDR_ADDR ] ), new string( "RESUME_DDR_ADDR" ));
	pRPD->AddData( new string( *m_FieldValues[ RESUME_DDR_PARAM ] ), new string( "RESUME_DDR_PARAM" ));
	pRPD->AddData( new string( *m_FieldValues[ RESUME_DDR_FLAG ] ), new string( "RESUME_DDR_FLAG" ));

	return PackageSize();
}

#if TOOLS_GUI == 1
bool CResumeDdr::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;
	
	CErdBase::ToText( TimDescriptor, ss );
	
	if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;
	return true;
}
#endif

#if TOOLS_GUI == 1
bool CResumeDdr::LoadState( ifstream& ifs )
{
	if ( theApp.ProjectVersion >= 0x03021201 )
		return CErdBase::LoadState( ifs );
	else
		return true;
}

bool CResumeDdr::SaveState( stringstream& ss )
{
	if ( theApp.ProjectVersion >= 0x03021201 )
		return CErdBase::SaveState( ss );
	else
		return true;
}
#endif
