/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#ifdef MORONA

#pragma message("Using 88AP001 (Morona) Extended Reserved Data strings")
#if TOOLS_GUI == 1
#include "StdAfx.h"
#endif

#pragma warning ( disable : 4200 ) // Disable warning messages
 
#include "Morona_Strings.h"
#include <Morona_Processor_Enums.h>	// need to change to get bootloader version

#define STRINGIZE(x) #x
#define FIELDSTRING( array, field_enum ) array[field_enum] = new string( STRINGIZE(field_enum) );

MoronaStrings::MoronaStrings(CExtendedReservedData& Ext)
: m_Ext(Ext)
{
	// make sure deallocated before allocate
	Depopulate(m_Ext.g_ClockEnableFields);
	Depopulate(m_Ext.g_FrequencyFields);
	Depopulate(m_Ext.g_DDRCustomFields);
	Depopulate(m_Ext.g_VoltagesFields);

	PopulateClockEnableFields(m_Ext.g_ClockEnableFields);
	PopulateFrequencyFields(m_Ext.g_FrequencyFields);
	PopulateDDRCustomFields(m_Ext.g_DDRCustomFields);
	PopulateVoltagesFields(m_Ext.g_VoltagesFields);
}

MoronaStrings::~MoronaStrings()
{
}

// to deallocate the g_ClockEnableFields strings call this 
void MoronaStrings::Depopulate( t_stringVector& Fields )
{
	for ( unsigned int i = 0; i < Fields.size(); i++ )
		delete Fields[i];
	Fields.clear();
}

// MUST call PopulateClockEnableFields to fill g_ClockEnableFields array with enum
// field strings before trying to access g_ClockEnableFields
void MoronaStrings::PopulateClockEnableFields(t_stringVector& g_ClockEnableFields)
{
	// stringize the enums into field name strings
	// allocate the full array beased on the enum max
	g_ClockEnableFields.resize(MORONA_CLOCK_ID_E_MAX);
	
	// populate strings for each field
	FIELDSTRING( g_ClockEnableFields, MORONA_CLOCK_ID_MCU );
	FIELDSTRING( g_ClockEnableFields, MORONA_CLOCK_ID_I2C );
}
	
// MUST call PopulateFrequencyFields to fill g_FrequencyFields array with enum
// field strings before trying to access g_FrequencyFields
void MoronaStrings::PopulateFrequencyFields(t_stringVector& g_FrequencyFields)
{
	// stringize the enums into field name strings
	// allocate the full array beased on the enum max
	g_FrequencyFields.resize(MORONA_FREQ_ID_E_MAX);
	
	// populate strings for each field
	FIELDSTRING( g_FrequencyFields, MORONA_FREQ_ID_MCU );
}


// MUST call PopulateDDRCustomFields to fill g_DDRCustomFields array with enum
// field strings before trying to access g_DDRCustomFields
void MoronaStrings::PopulateDDRCustomFields(t_stringVector& g_DDRCustomFields)
{
	//
	// Morona custom fields
	//
	// stringize the enums into field name strings
	// allocate the full array beased on the enum max
	
	g_DDRCustomFields.resize(MORONA_MCU_REGID_E_MAX);

	// populate strings for each field
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRCFGREG0_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRCFGREG1_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRTMGREG1_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRTMGREG2_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRTMGREG3_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRTMGREG4_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRTMGREG5_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRCTLREG1_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRCTLREG2_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRCTLREG3_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRCTLREG4_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRCTLREG5_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRCTLREG6_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_SDRCTLREG7_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_MCBCTLREG1_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_PHYCTLREG3_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_PHYCTLREG7_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_PHYCTLREG8_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_PHYCTLREG9_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_PHYCTLREG11_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_PHYCTLREG13_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_PHYCTLREG14_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_DLLCTLREG1_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_DLLCTLREG2_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_DLLCTLREG3_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_ADRMAPREG0_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_ADRMAPREG1_ID );
	FIELDSTRING( g_DDRCustomFields, MORONA_USRCMDREG0_ID );
}

// MUST call PopulateVoltagesFields to fill g_VoltagesFields array with enum
// field strings before trying to access g_VoltagesFields
void MoronaStrings::PopulateVoltagesFields(t_stringVector& g_VoltagesFields)
{
	// TBD


	// stringize the enums into field name strings
	// allocate the full array beased on the enum max
//	g_VoltagesFields.resize(MORONA_VOLTAGES_E_MAX);
	
	// populate strings for each field
//	FIELDSTRING( g_VoltagesFields,  );
}

#endif
