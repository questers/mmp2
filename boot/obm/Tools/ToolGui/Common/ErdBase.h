/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "ReservedPackageData.h"
#include "TimLib.h"
#include "TimDescriptorLine.h"

#include <string>
#include <vector>
using namespace std;

typedef vector<string*>           t_stringVector;
typedef vector<string*>::iterator t_stringVectorIter;

// forward declaration
class CTimDescriptor; 

class CErdBase : public CTimLib
{
public:

enum ERD_PKG_TYPE { UNKNOWN_ERD = 0,
                    AUTOBIND_ERD = 100,
                    ESCAPESEQ_ERD,
                    GPIOSET_ERD,
                    GPIO_ERD,
                    RESUME_ERD, // obsolete but kept to maintain pkg type id
                    RESUME_DDR_ERD,
                    TBRXFER_ERD,
                    XFER_ERD,
                    UART_ERD,
                    USB_ERD,
                    USBVENDORREQ_ERD,
                    CONSUMER_ID_ERD,
                    DDR_INITIALIZATION_ERD,
                    INSTRUCTIONS_ERD,
                    DDR_OPERATIONS_ERD,
                    CORE_ID_ERD,
                    TZ_INITIALIZATION_ERD,
                    TZ_OPERATIONS_ERD,
                    MAX_ERD_PACKAGES = ( TZ_OPERATIONS_ERD - 100 + 2 )
                   };

    CErdBase( ERD_PKG_TYPE ErdType, int iMaxFieldNum );
    virtual ~CErdBase(void);

    // copy constructor
    CErdBase( const CErdBase& rhs );
    // assignment operator
    virtual CErdBase& operator=( const CErdBase& rhs );

    virtual void Reset();

    const ERD_PKG_TYPE ErdPkgType(){ return m_eErdType; }
    static CErdBase* Create( ERD_PKG_TYPE ErdPkgType );
    static CErdBase* Create( CErdBase& src );
    static CErdBase* Create( string& sPackageName );

    virtual bool Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine );

    virtual bool ToBinary( ofstream& ofs ) = 0;
    virtual unsigned int PackageSize(){ return 0; }
    virtual const string& PackageName() = 0;
    virtual int AddPkgStrings( CReservedPackageData* pRPD ) = 0;

    unsigned int MaxFieldNum(){ return m_iMaxFieldNum; }
    t_stringVector& FieldNames(){ return m_FieldNames; }
    t_stringVector& FieldValues(){ return m_FieldValues; }
    t_stringVector& FieldComments(){ return m_FieldComments; }

#if TOOLS_GUI == 1
    virtual bool LoadState( ifstream& ifs );
    virtual bool SaveState( stringstream& ss );
    virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );

#endif

protected:
    ERD_PKG_TYPE	m_eErdType;

    unsigned int	m_iMaxFieldNum;
    t_stringVector	m_FieldNames;
    t_stringVector	m_FieldValues;
    t_stringVector	m_FieldComments;
};

typedef vector<CErdBase*>           t_ErdBaseVector;
typedef vector<CErdBase*>::iterator t_ErdBaseVectorIter;

