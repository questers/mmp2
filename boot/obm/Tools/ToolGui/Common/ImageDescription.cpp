/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#if TOOLS_GUI == 1
#include "StdAfx.h"
#endif

#include "ImageDescription.h"
#include "TimDescriptor.h"
#include "TimDescriptorLine.h"

#if LINUX
#include <string.h>
#endif

CImageDescription::CImageDescription()
: CTimLib()
{
	Reset();
	m_bChanged = false;
}

CImageDescription::~CImageDescription(void)
{
#if TOOLS_GUI == 1
	RemoveRefs();
#endif
}
	
CImageDescription::CImageDescription( const CImageDescription& rhs )
: CTimLib( rhs )
{
	// copy constructor
	memcpy ( Hash, rhs.Hash, sizeof(Hash) );

	m_sImageId = rhs.m_sImageId;
	m_sImageIdTag = rhs.m_sImageIdTag;
	m_sNextImageId = rhs.m_sNextImageId;
	m_sNextImageIdTag = rhs.m_sNextImageIdTag;
	m_sFlashEntryAddress = rhs.m_sFlashEntryAddress;
	m_sLoadAddress = rhs.m_sLoadAddress;
	m_sImageSizeToCrc = rhs.m_sImageSizeToCrc;
	m_sImageFilePath = rhs.m_sImageFilePath; 
	m_sHashAlgorithmId = rhs.m_sHashAlgorithmId;
	m_uiImageSize = rhs.m_uiImageSize;
	m_uiImageSizeToHash = rhs.m_uiImageSizeToHash;
	m_uiPartitionNumber = rhs.m_uiPartitionNumber;
	m_bChanged = rhs.m_bChanged;

#if TOOLS_GUI == 1
	AddRefs( const_cast<CImageDescription&>(rhs) );
#endif
}

// assignment operator
CImageDescription& CImageDescription::operator=( const CImageDescription& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		CTimLib::operator=(rhs);

#if TOOLS_GUI == 1
		RemoveRefs();
#endif
		memcpy ( Hash, rhs.Hash, sizeof(Hash) );

		m_sImageId = rhs.m_sImageId;
		m_sImageIdTag = rhs.m_sImageIdTag;
		m_sNextImageId = rhs.m_sNextImageId;
		m_sNextImageIdTag = rhs.m_sNextImageIdTag;
		m_sFlashEntryAddress = rhs.m_sFlashEntryAddress;
		m_sLoadAddress = rhs.m_sLoadAddress;
		m_sImageSizeToCrc = rhs.m_sImageSizeToCrc;
		m_sImageFilePath = rhs.m_sImageFilePath; 
		m_sHashAlgorithmId = rhs.m_sHashAlgorithmId;
		m_uiImageSize = rhs.m_uiImageSize;
		m_uiImageSizeToHash = rhs.m_uiImageSizeToHash;
		m_uiPartitionNumber = rhs.m_uiPartitionNumber;
		m_bChanged = rhs.m_bChanged;

#if TOOLS_GUI == 1
		AddRefs( const_cast<CImageDescription&>(rhs) );
#endif
	}

	return *this;
}

#if TOOLS_GUI == 1
void CImageDescription::AddRefs( CImageDescription& nc_rhs )
{
	CTimDescriptorLine* pLine = 0;

	// update the object line reference
	if ( (pLine = CTimDescriptor::GetLineField( "Image ID", false, &nc_rhs.m_sImageIdTag )) != 0 )
		pLine->AddRef( &m_sImageIdTag );

	if ( (pLine = CTimDescriptor::GetLineField( "Next Image ID", false, &nc_rhs.m_sNextImageIdTag )) != 0 )
		pLine->AddRef( &m_sNextImageIdTag );

	if ( (pLine = CTimDescriptor::GetLineField( "Flash Entry Address", false, &nc_rhs.m_sFlashEntryAddress )) != 0 )
		pLine->AddRef( &m_sFlashEntryAddress );

	if ( (pLine = CTimDescriptor::GetLineField( "Load Address", false, &nc_rhs.m_sLoadAddress )) != 0 )
		pLine->AddRef( &m_sLoadAddress );

	if ( (pLine = CTimDescriptor::GetLineField( "Image Size To CRC in bytes", false, &nc_rhs.m_sImageSizeToCrc )) != 0 )
		pLine->AddRef( &m_sImageSizeToCrc );

	if ( (pLine = CTimDescriptor::GetLineField( "Image Filename", false, &nc_rhs.m_sImageFilePath )) != 0 )
		pLine->AddRef( &m_sImageFilePath );

	if ( (pLine = CTimDescriptor::GetLineField( "Hash Algorithm ID", false, &nc_rhs.m_sHashAlgorithmId )) != 0 )
		pLine->AddRef( &m_sHashAlgorithmId );

	if ( (pLine = CTimDescriptor::GetLineField( "Image Size To Hash in bytes", false, &nc_rhs.m_uiImageSizeToHash )) != 0 )
		pLine->AddRef( &m_uiImageSizeToHash );

	if ( (pLine = CTimDescriptor::GetLineField( "Partition Number", false, &nc_rhs.m_uiPartitionNumber )) != 0 )
		pLine->AddRef( &m_uiPartitionNumber );
}

void CImageDescription::RemoveRefs()
{
	CTimDescriptorLine* pLine = 0;

	// update the object line reference
	if ( (pLine = CTimDescriptor::GetLineField( "Image ID", false, &m_sImageIdTag )) != 0 )
		pLine->RemoveRef( &m_sImageIdTag );

	if ( (pLine = CTimDescriptor::GetLineField( "Next Image ID", false, &m_sNextImageIdTag )) != 0 )
		pLine->RemoveRef( &m_sNextImageIdTag );

	if ( (pLine = CTimDescriptor::GetLineField( "Flash Entry Address", false, &m_sFlashEntryAddress )) != 0 )
		pLine->RemoveRef( &m_sFlashEntryAddress );

	if ( (pLine = CTimDescriptor::GetLineField( "Load Address", false, &m_sLoadAddress )) != 0 )
		pLine->RemoveRef( &m_sLoadAddress );

	if ( (pLine = CTimDescriptor::GetLineField( "Image Size To CRC in bytes", false, &m_sImageSizeToCrc )) != 0 )
		pLine->RemoveRef( &m_sImageSizeToCrc );

	if ( (pLine = CTimDescriptor::GetLineField( "Image Filename", false, &m_sImageFilePath )) != 0 )
		pLine->RemoveRef( &m_sImageFilePath );

	if ( (pLine = CTimDescriptor::GetLineField( "Hash Algorithm ID", false, &m_sHashAlgorithmId )) != 0 )
		pLine->RemoveRef( &m_sHashAlgorithmId );

	if ( (pLine = CTimDescriptor::GetLineField( "Image Size To Hash in bytes", false, &m_uiImageSizeToHash )) != 0 )
		pLine->RemoveRef( &m_uiImageSizeToHash );

	if ( (pLine = CTimDescriptor::GetLineField( "Partition Number", false, &m_uiPartitionNumber )) != 0 )
		pLine->RemoveRef( &m_uiPartitionNumber );
}
#endif

void CImageDescription::Reset()
{
	m_sImageId = "";
	m_sImageIdTag = "";
	m_sNextImageId = "<None>";
	m_sNextImageIdTag = "0xFFFFFFFF";
	m_sFlashEntryAddress = "0x00000000";
	m_sLoadAddress = "0x00000000";
	m_sImageFilePath = "";
	m_sHashAlgorithmId = "";
	m_sImageSizeToCrc = "0";	// obsolete but needed for compatible tim descriptor files
	m_uiImageSizeToHash = 0xFFFFFFFF;
	m_uiImageSize = 0;
	m_uiPartitionNumber = 0;

	memset( Hash,0,sizeof(Hash) );
}

void CImageDescription::ImageId( string& sImageId )
{ 
	m_sImageId = sImageId;

	if ( m_sImageId == "<None>" || m_sImageId.length() == 0 )
		m_sImageIdTag = "0xFFFFFFFF";
	else
	{
		m_sImageId = ToUpper(sImageId, true); 
		// turn first 4 ascii chars of Id into a hex encoded ascii tag
		string sIdReversed; 
		size_t i = min((int)m_sImageId.length(),4);
		for ( ; i > 0; i-- )
			sIdReversed += m_sImageId[i-1];
		while( sIdReversed.length() < 4 ) sIdReversed += '\0'; // nulls reserve bytes for int* dereference
		
		m_sImageIdTag = HexFormattedAscii(*((int*)sIdReversed.c_str()));
	}
	m_bChanged = true;
}

void CImageDescription::ImageIdTag( string& sTag ) 
{ 
	m_sImageIdTag = sTag; 

	int iId = Translate( m_sImageIdTag );
	char szId[5]={0};
	*((int*)(szId)) = iId;

	string sIdReversed; 
	size_t i = 4;
	for ( ; i > 0; i-- )
		sIdReversed += szId[i-1];
	m_sImageId = sIdReversed;
	m_bChanged = true;
}

void CImageDescription::NextImageId( string& sImageId ) 
{ 
	m_sNextImageId = sImageId; 

	if ( m_sNextImageId == "0xFFFFFFFF" )
		m_sNextImageId = "<None>";

	if ( m_sNextImageId == "<None>" || m_sNextImageId.length() == 0 )
		m_sNextImageIdTag = "0xFFFFFFFF";
	else
	{
		m_sNextImageId = ToUpper(sImageId, true); 
	
		// turn first 4 ascii chars of Id into a hex encoded ascii tag
		string sIdReversed;  
		size_t i = min((int)m_sNextImageId.length(),4);
		for ( ; i > 0; i-- )
			sIdReversed += m_sNextImageId[i-1];
		while( sIdReversed.length() < 4 ) sIdReversed += '\0'; // nulls reserve bytes for int* dereference

		m_sNextImageIdTag = HexFormattedAscii(*((int*)sIdReversed.c_str()));
	}
	m_bChanged = true;
}

void CImageDescription::NextImageIdTag( string& sTag ) 
{ 
	m_sNextImageIdTag = sTag; 

	int iId = Translate( m_sNextImageIdTag );
	char szId[5]={0};
	*((int*)(szId)) = iId;

	string sIdReversed; 
	size_t i = 4;
	for ( ; i > 0; i-- )
		sIdReversed += szId[i-1];
	m_sNextImageId = sIdReversed;
	m_bChanged = true;
}

#if TOOLS_GUI == 1
bool CImageDescription::SaveState( stringstream& ss )
{
	SaveFieldState( ss, m_sImageId );
	SaveFieldState( ss, m_sImageIdTag );
	SaveFieldState( ss, m_sNextImageId );
	SaveFieldState( ss, m_sNextImageIdTag );
	SaveFieldState( ss, m_sFlashEntryAddress );
	SaveFieldState( ss, m_sLoadAddress );
	SaveFieldState( ss, m_sImageFilePath );
	SaveFieldState( ss, m_sHashAlgorithmId );
	SaveFieldState( ss, m_uiImageSizeToHash );
	SaveFieldState( ss, m_uiPartitionNumber );

return true; // SUCCESS
}

bool CImageDescription::LoadState( ifstream& ifs )
{
	string sbuf;

	LoadFieldState( ifs, m_sImageId );
	LoadFieldState( ifs, m_sImageIdTag );
	LoadFieldState( ifs, m_sNextImageId );
	LoadFieldState( ifs, m_sNextImageIdTag );
	LoadFieldState( ifs, m_sFlashEntryAddress );
	LoadFieldState( ifs, m_sLoadAddress );
	LoadFieldState( ifs, m_sImageFilePath );
	LoadFieldState( ifs, m_sHashAlgorithmId );
	LoadFieldState( ifs, m_uiImageSizeToHash ); //ImageSizeToHash( atoi(sbuf.c_str()) );
	LoadFieldState( ifs, m_uiPartitionNumber ); //PartitionNumber( atoi(sbuf.c_str()) );

	return ifs.good() && !ifs.fail(); // success
}
#endif
