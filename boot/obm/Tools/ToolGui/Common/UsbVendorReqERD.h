/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "ErdBase.h"

#include <list>
using namespace std;

typedef list<string*>           t_stringList;
typedef list<string*>::iterator t_stringListIter;

class CUsbVendorReq : public CErdBase
{
public:
	CUsbVendorReq();
	virtual ~CUsbVendorReq();

	// copy constructor
	CUsbVendorReq( const CUsbVendorReq& rhs );
	// assignment operator
	virtual CUsbVendorReq& operator=( const CUsbVendorReq& rhs );

	virtual void Reset();

	static const string Begin;	// "Usb Vendor Request"
	static const string End;	// "End Usb Vendor Request"

	virtual bool Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine );

	virtual bool ToBinary( ofstream& ofs );

	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize();
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

	t_stringVector& DataList() { return m_DataList; }
	t_stringVector& DataFieldNames();

#if TOOLS_GUI == 1
	virtual bool LoadState( ifstream& ifs );
	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	enum UsbVendorReq { REQUEST_TYPE, REQUEST, VALUE, INDEX, LENGTH, DATA, USB_VENDOR_REQ_MAX };

private:
	t_stringVector	m_DataList;
	t_stringVector	m_DataFieldNames;
};

typedef list<CUsbVendorReq*>           t_UsbVendorReqList;
typedef list<CUsbVendorReq*>::iterator t_UsbVendorReqListIter;

