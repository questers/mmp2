/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#if TOOLS_GUI == 1
#include "stdafx.h"
#include "MarvellBootUtility.h"
#endif

#include "DDROperations.h"
#include "TimDescriptorParser.h"

#if LINUX
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif


CDDROperation::CDDROperation()
: CTimLib()
{
	m_OperationId = DDR_NOP;
	m_sOpIdText = "NOP";
	m_Value = 0;
}

CDDROperation::CDDROperation( DDR_OPERATION_SPEC_T OpId, const string & sOpIdText )
: m_OperationId( OpId ), m_sOpIdText( sOpIdText ), CTimLib()
{
	m_Value = 0;
}

CDDROperation::~CDDROperation(void)
{
#if TOOLS_GUI == 1
		CTimDescriptorLine* pLine = 0;

		// update the object line reference
		if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_sOpIdText )) != 0 )
			pLine->RemoveRef( &m_sOpIdText );
#endif
}

CDDROperation::CDDROperation( const CDDROperation& rhs )
: CTimLib( rhs )
{
	// copy constructor
	CDDROperation& nc_rhs = const_cast<CDDROperation&>(rhs);

	m_OperationId = rhs.m_OperationId;
	m_sOpIdText = rhs.m_sOpIdText;

#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;

	// update the object line reference
	if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sOpIdText )) != 0 )
		pLine->AddRef( &m_sOpIdText );
#endif

	m_Value = rhs.m_Value;
}

CDDROperation& CDDROperation::operator=( const CDDROperation& rhs ) 
{
	// assignment operator
	if ( &rhs != this )
	{
		CTimLib::operator=(rhs);

		CDDROperation& nc_rhs = const_cast<CDDROperation&>(rhs);
	
		m_OperationId = rhs.m_OperationId;

#if TOOLS_GUI == 1
		CTimDescriptorLine* pLine = 0;

		// update the object line reference
		if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_sOpIdText )) != 0 )
			pLine->RemoveRef( &m_sOpIdText );
#endif

		m_sOpIdText = rhs.m_sOpIdText;

#if TOOLS_GUI == 1
		// update the object line reference
		if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sOpIdText )) != 0 )
			pLine->AddRef( &m_sOpIdText );
#endif

		m_Value = rhs.m_Value;
	}
	return *this;
}


bool CDDROperation::SetOperationID( DDR_OPERATION_SPEC_T eOpId )
{
	t_DDROperationListIter Iter = CDDROperations::s_DefinedDDROperations.begin();
	while ( Iter != CDDROperations::s_DefinedDDROperations.end() )
	{
		if ( (*Iter)->m_OperationId == eOpId )
		{
			*this = *(*Iter);
			break;
		}
		Iter++;
	}
	return true;
}

bool CDDROperation::SetOperationID( string& sOpIdText )
{
	t_DDROperationListIter Iter = CDDROperations::s_DefinedDDROperations.begin();
	while ( Iter != CDDROperations::s_DefinedDDROperations.end() )
	{
		if ( (*Iter)->m_sOpIdText == sOpIdText )
		{
			*this = *(*Iter);
			break;
		}
		Iter++;
	}
	return true;
}

bool CDDROperation::ToBinary( ofstream& ofs )
{
	// validate size
//	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
//		return false;

//	ofs << Translate(*m_FieldValues[DATA_ID]);
//	ofs << Translate(*m_FieldValues[LOCATION]);

	return ofs.good();
}
		
#if TOOLS_GUI == 1
bool CDDROperation::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	if ( !TimDescriptor.AttachCommentedObject( ss, this, m_sOpIdText.c_str(), HexFormattedAscii(m_Value) ) )
		ss << m_sOpIdText << ":" << HexFormattedAscii(m_Value) << endl;

	return true;
}
#endif

#if TOOLS_GUI == 1
bool CDDROperation::LoadState( ifstream& ifs )
{
	if ( theApp.ProjectVersion >= 0x03021701 )
	{
		string sbuf;
		LoadFieldState( ifs, sbuf );
		m_OperationId = (DDR_OPERATION_SPEC_T)Translate(sbuf);
		LoadFieldState( ifs, m_sOpIdText );
		LoadFieldState( ifs, sbuf );
		m_Value = Translate(sbuf);
	}
	return ifs.good() && !ifs.fail(); // success;
}
#endif

#if TOOLS_GUI == 1
bool CDDROperation::SaveState( stringstream& ss )
{
	SaveFieldState( ss, (unsigned int&)m_OperationId );
	SaveFieldState( ss, m_sOpIdText );
	SaveFieldState( ss, m_Value );

	return true;
}
#endif

t_DDROperationList  CDDROperations::s_DefinedDDROperations;
static CDDROperation NOOP_OP( DDR_NOP, "NOP" );
CDDROperation DDR_INIT_ENABLE_OP( DDR_INIT_ENABLE, "DDR_INIT_ENABLE" );
CDDROperation DDR_MEMTEST_ENABLE_OP( DDR_MEMTEST_ENABLE, "DDR_MEMTEST_ENABLE" );
CDDROperation DDR_MEMTEST_START_ADDR_OP( DDR_MEMTEST_START_ADDR, "DDR_MEMTEST_START_ADDR" );
CDDROperation DDR_MEMTEST_SIZE_OP( DDR_MEMTEST_SIZE, "DDR_MEMTEST_SIZE" );
CDDROperation DDR_INIT_LOOP_COUNT_OP( DDR_INIT_LOOP_COUNT, "DDR_INIT_LOOP_COUNT" );
CDDROperation DDR_IGNORE_INST_TO_OP( DDR_IGNORE_INST_TO, "DDR_IGNORE_INST_TO" );

int				    CDDROperations::s_DDROpCount = 0;

const string CDDROperations::Begin("Operations");
const string CDDROperations::End("End Operations");

CDDROperations::CDDROperations()
:	CErdBase( DDR_OPERATIONS_ERD, OPERATIONS_MAX )
{
	s_DDROpCount++;

	if ( s_DDROpCount == 1 )
	{
		s_DefinedDDROperations.push_back( &NOOP_OP );
		s_DefinedDDROperations.push_back( &DDR_INIT_ENABLE_OP );
		s_DefinedDDROperations.push_back( &DDR_MEMTEST_ENABLE_OP );
		s_DefinedDDROperations.push_back( &DDR_MEMTEST_START_ADDR_OP );
		s_DefinedDDROperations.push_back( &DDR_MEMTEST_SIZE_OP );
		s_DefinedDDROperations.push_back( &DDR_INIT_LOOP_COUNT_OP );
		s_DefinedDDROperations.push_back( &DDR_IGNORE_INST_TO_OP );
	}
}

CDDROperations::~CDDROperations(void)
{
	Reset();
	
	s_DDROpCount--;
}

// copy constructor
CDDROperations::CDDROperations( const CDDROperations& rhs )
: CErdBase( rhs )
{
	// need to do a deep copy of lists to avoid dangling references
	CDDROperations& nc_rhs = const_cast<CDDROperations&>(rhs);

#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;
#endif

	t_DDROperationListIter iter = nc_rhs.m_DdrOperations.begin();
	while ( iter != nc_rhs.m_DdrOperations.end() )
	{
		CDDROperation* pOp = new CDDROperation( *(*iter) );
		m_DdrOperations.push_back( pOp );

#if TOOLS_GUI == 1
		// update the object line reference
		if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
			pLine->AddRef( pOp );
#endif
		iter++;
	}
}

// assignment operator
CDDROperations& CDDROperations::operator=( const CDDROperations& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		Reset();

		CErdBase::operator=( rhs );

		// need to do a deep copy of lists to avoid dangling references
		CDDROperations& nc_rhs = const_cast<CDDROperations&>(rhs);

#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;
#endif

		t_DDROperationListIter iter = nc_rhs.m_DdrOperations.begin();
		while ( iter != nc_rhs.m_DdrOperations.end() )
		{
			CDDROperation* pOp = new CDDROperation( *(*iter) );
			m_DdrOperations.push_back( pOp );

#if TOOLS_GUI == 1
			// update the object line reference
			if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
				pLine->AddRef( pOp );
#endif
			iter++;
		}
	}
	return *this;
}

void CDDROperations::Reset()
{
#if TOOLS_GUI == 1
	CTimDescriptorLine* pLine = 0;
#endif

	t_DDROperationListIter iter = m_DdrOperations.begin();
	while ( iter != m_DdrOperations.end() )
	{
#if TOOLS_GUI == 1
		// update the object line reference
		if ( (pLine = CTimDescriptor::GetLineField( "", false, *iter )) != 0 )
			pLine->RemoveRef( *iter );
#endif
		delete *iter;
		iter++;
	}
	m_DdrOperations.clear();
}

unsigned int CDDROperations::PackageSize() 
{
	unsigned int iSize = (unsigned int)m_DdrOperations.size() * 8;

	return iSize;
}

bool CDDROperations::Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine )
{
	while ( pLine = TimDescriptor.GetNextLineField( pLine ) )
	{
		bool bFound = false;
		t_DDROperationListIter iter = s_DefinedDDROperations.begin();
		while ( iter != s_DefinedDDROperations.end() )
		{
			if ( TrimWS( pLine->m_FieldName ) == (*iter)->m_sOpIdText )
			{
				CDDROperation* pOp = new CDDROperation(*(*iter));
				pOp->m_Value = Translate( pLine->m_FieldValue );
				m_DdrOperations.push_back( pOp );

#if TOOLS_GUI == 1
				pLine->AddRef( pOp ); 
#endif
				bFound = true;
				break;
			}
			iter++;
		}
		if ( !bFound )
			break;
	}

	// field not found
	return true;
}

bool CDDROperations::ToBinary( ofstream& ofs )
{
	bool bRet = true;
#if 0
	// validate size
	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
		return false;

	ofs << TBR_XFER;
	ofs << PackageSize();
	ofs << Translate(*m_FieldNames[XFER_TABLE_LOC]);
	ofs << Translate(*m_FieldValues[NUM_DATA_PAIRS]);

	t_XferListIter iter = Xfers.begin();
	while ( bRet && iter != Xfers.end() )
		bRet = (*iter++)->ToBinary( ofs );
#endif

	return ( ofs.good() && bRet );
}

int CDDROperations::AddPkgStrings( CReservedPackageData* pRPD )
{
	t_DDROperationListIter iter = m_DdrOperations.begin();
	while ( iter != m_DdrOperations.end() )
	{
		pRPD->AddData( new string( HexFormattedAscii((*iter)->m_OperationId) ), new string( (*iter)->m_sOpIdText ) );
		pRPD->AddData( new string( HexFormattedAscii((*iter)->m_Value ) ), new string( "Value" ) );
		iter++;
	}

	return PackageSize();
}

#if TOOLS_GUI == 1
bool CDDROperations::SaveState( stringstream& ss )
{
	unsigned int temp = 0;

	CErdBase::SaveState( ss );

	temp = (unsigned int)m_DdrOperations.size();
	SaveFieldState( ss, temp );

	t_DDROperationListIter iter = m_DdrOperations.begin();
	while ( iter != m_DdrOperations.end() )
	{
		if ( !(*iter)->SaveState( ss ) )
			return false;
		
		iter++;
	}

	return true; // SUCCESS
}
#endif

#if TOOLS_GUI == 1
bool CDDROperations::LoadState( ifstream& ifs )
{
	bool bRet = true;

	if ( theApp.ProjectVersion >= 0x03021701 )
	{
		Reset();

		string sbuf;
		CErdBase::ERD_PKG_TYPE ErdType = CErdBase::UNKNOWN_ERD;

		LoadFieldState( ifs, sbuf );  
		ErdType = CErdBase::ERD_PKG_TYPE(Translate(sbuf));
		if ( ErdType != DDR_OPERATIONS_ERD )
			return false;

		CErdBase::LoadState( ifs );

		LoadFieldState( ifs, sbuf );
		unsigned int Size = Translate(sbuf);

		for ( unsigned int i = 0; i < Size; i++ )
		{
			CDDROperation* pOp = new CDDROperation;
			bRet = pOp->LoadState( ifs );
			if ( !bRet )
			{
				delete pOp;
				break;
			}
			m_DdrOperations.push_back(pOp);
		}
	}

	return bRet && ifs.good() && !ifs.fail(); // success
}
#endif

#if TOOLS_GUI == 1
bool CDDROperations::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	bool bRet = true;

	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;

	t_DDROperationListIter iter = m_DdrOperations.begin();
	while ( bRet && iter != m_DdrOperations.end() )
	{
		bRet = (*iter)->ToText( TimDescriptor, ss );
		iter++;
	}

	if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;

	return bRet;
}
#endif
