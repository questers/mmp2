/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#pragma warning ( disable : 4996 )

#include "TimDescriptorParser.h"
#if TOOLS_GUI == 0
#include "CommandLineParser.h"
#endif

#include "Partition.h"
#include "AutoBindERD.h"
#include "CoreResetERD.h"
#include "EscapeSeqERD.h"
#include "GpioSetERD.h"
#include "ResumeDdrERD.h"
#include "TbrXferERD.h"
#include "UsbERD.h"
#include "UartERD.h"
#include "UsbVendorReqERD.h"
#include "ConsumerID.h"
#include "TzInitialization.h"

#include "assert.h"

#if LINUX
#include <string.h>
#include <stdlib.h>
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

CTimDescriptorParser::CTimDescriptorParser(void)
{
    Reset();
}

CTimDescriptorParser::~CTimDescriptorParser(void)
{
    Reset();
}

void CTimDescriptorParser::Reset()
{
    m_sTimFilePath = "";	
    m_sTimBinFilename = "";
    m_sImageOutFilename = "";
    
    memset (&m_PartitionHeader,0,sizeof (PartitionTable_T));
    m_pPartitionInfo = 0;
}

ifstream& CTimDescriptorParser::OpenTimDescriptorTextFile ( ios_base::openmode mode )
{
    // if already open, close it and reopen in correct mode
    CloseTimDescriptorTextFile();

    m_ifsTimDescriptorTxtFile.open( m_sTimFilePath.c_str(), mode );
    if ( m_ifsTimDescriptorTxtFile.bad() || m_ifsTimDescriptorTxtFile.fail() )
    {
        printf ("\n  Error: Cannot open file name <%s> !", m_sTimFilePath.c_str());
    }
    return m_ifsTimDescriptorTxtFile;
}

void CTimDescriptorParser::CloseTimDescriptorTextFile ()
{
    if ( m_ifsTimDescriptorTxtFile.is_open() )
        m_ifsTimDescriptorTxtFile.close();
    m_ifsTimDescriptorTxtFile.clear();
}

bool CTimDescriptorParser::GetTimDescriptorLines(CCommandLineParser& CommandLineParser)
{
    Reset();

    // save file path
#if TOOLS_GUI == 1
    m_sTimFilePath = m_TimDescriptor.TimDescriptorFilePath();
#else
    m_sTimFilePath = CommandLineParser.KeyFileName;
#endif

    m_TimDescriptor.Reset();
    m_TimDescriptor.DiscardAll();

#if TOOLS_GUI == 1
    m_TimDescriptor.DiscardTimDescriptorLines();
#endif

    // restore file path after m_TimDescriptor.Reset()
    m_TimDescriptor.TimDescriptorFilePath( m_sTimFilePath );

    if ( m_sTimFilePath.length() == 0 )
    {
//		printf ("\n  Error: No TIM Descriptor file path!" );
        return false;
    }

    printf ("\n  TIM Descriptor file path: %s", m_sTimFilePath.c_str() );

    OpenTimDescriptorTextFile( ios_base::in );

    if ( m_ifsTimDescriptorTxtFile.is_open() )
    {
        while( !m_ifsTimDescriptorTxtFile.eof() )
        {
            CTimDescriptorLine* pLine = new CTimDescriptorLine;

            if ( pLine->ParseLine( m_ifsTimDescriptorTxtFile, *this ) )
            {
                // if we have a field or just a comment, save the line, else delete it
                if ( pLine->m_FieldName.empty() && pLine->m_FieldValue.empty()
#if TOOLS_GUI == 1
                     && pLine->m_PostCommentLines.empty() && pLine->m_PostCommentLines.empty() 
#endif
                )
                    delete pLine;
                else
                    m_TimDescriptor.TimDescriptorLines().push_back( pLine );
            }
            else
                delete pLine;
        }
    }
    else
        return false;

    CloseTimDescriptorTextFile();
    return true;
}

bool CTimDescriptorParser::GetTimDescriptorLines(t_stringList& Lines)
{
    Reset();

    m_sTimFilePath = m_TimDescriptor.TimDescriptorFilePath();

    m_TimDescriptor.Reset();
    m_TimDescriptor.DiscardAll();
    m_TimDescriptor.DiscardTimDescriptorLines();

    // restore file path after m_TimDescriptor.Reset()
    m_TimDescriptor.TimDescriptorFilePath( m_sTimFilePath );

    t_stringListIter iter; 
    // restart at beginning each time
    while ( iter = Lines.begin(), iter != Lines.end() )
    {
        CTimDescriptorLine* pLine = new CTimDescriptorLine;

        if ( pLine->ParseLine( Lines ) )
        {
            // if we have a field or just a comment, save the line, else delete it
            if ( pLine->m_FieldName.empty() && pLine->m_FieldValue.empty()
#if TOOLS_GUI == 1
                 && pLine->m_PostCommentLines.empty() && pLine->m_PostCommentLines.empty() 
#endif
            )
                delete pLine;
            else
                m_TimDescriptor.TimDescriptorLines().push_back( pLine );
        }
        else
            delete pLine;
    }

    return true;
}

#if TOOLS_GUI == 1
bool CTimDescriptorParser::ParseDescriptor(CDownloaderInterface& DownloaderInterface)
#else
bool CTimDescriptorParser::ParseDescriptor(CCommandLineParser& CommandLineParser)
#endif
{
    bool bRet = true;

    if ( !VerifyDescriptorIntegrity () )
    {
        printf ("\n  Error: TIM Descriptor file format incorrect. Cannot process file name <%s> !", m_sTimFilePath.c_str());
        return FALSE;
    }

    CTimDescriptorLine* pLine = 0;
    // get first Image ID which must be the TIM*
    if ( pLine = CTimDescriptor::GetLineField( "Image ID" ) )
    {
        m_TimDescriptor.TimHeader().VersionBind.Identifier = TIMIDENTIFIER; // default

        if ( (Translate(pLine->m_FieldValue) & TYPEMASK) == (TIMIDENTIFIER & TYPEMASK) )
            m_TimDescriptor.TimHeader().VersionBind.Identifier = Translate(pLine->m_FieldValue);
        else
            printf ("  Warning: TIM* type is not the first Image ID in descriptor txt file.\n");
    }

    if ( pLine = CTimDescriptor::GetLineField( "Version" ) )
    {
        m_TimDescriptor.TimHeader().VersionBind.Version = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &m_TimDescriptor.TimHeader().VersionBind.Version );
#endif
    }
    else
    {
        printf ("  Error: Key file parsing error reading, Version:\n");
        bRet = false;
        goto Exit;
    }

    if ( pLine = CTimDescriptor::GetNextLineField( "Trusted", pLine ) )
    {
        m_TimDescriptor.TimHeader().VersionBind.Trusted = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &m_TimDescriptor.TimHeader().VersionBind.Trusted );
        DownloaderInterface.ImageBuild.Trusted(m_TimDescriptor.TimHeader().VersionBind.Trusted == 0 ? false : true);
#endif
    }
    else
    {
        printf ("  Error: Key file parsing error reading, Trusted:\n");
        bRet = false;
        goto Exit;
    }

#if TRUSTED
    // ok to open both trusted and non trusted TIM Descriptors file in trusted version
#else
    if (m_TimDescriptor.TimHeader().VersionBind.Trusted == 1)
    {
        // do not open trusted TIM with non-trusted tools
        printf ("  Error:  Cannot parse Trusted TIM Descriptor with non-trusted tools.\n");
        bRet = false;
        goto Exit;
    }
#endif

#if TOOLS_GUI == 0
    if ( !m_TimDescriptor.Trusted() )
    {
        if (CommandLineParser.CheckYourOptions () == false) return false;
        int iOption = CommandLineParser.iOption;
        if (iOption > 1) 
        {
            printf ("////////////////////////////////////////////////////////////\n");
            printf ("  Error: You have selected option mode 2, 3 or 4!\n");
            printf ("You can only build a Non Trusted Image in option mode 1!\n");
            printf ("Example: tbb -m 1 -k ntimkey.txt\n\n");
            printf ("Also check the Trusted directive in the TIM descriptor file.\n");
            printf ("Example: Trusted: 0  (for Non Trusted Image)\n");
            printf ("Example: Trusted: 1  (for Trusted Image)\n");
            printf ("////////////////////////////////////////////////////////////\n");
            bRet = false;
            goto Exit;
        }
    }
#endif

#if TOOLS_GUI == 0
    if ( CommandLineParser.bIsPartitionDataFile 
        && (!CommandLineParser.bIsKeyFile || m_TimDescriptor.TimHeader().VersionBind.Version >= TBR_VERSIONS_WITH_PARTITION ) )
    {
        ifstream ifsPartitionFile;
        ifsPartitionFile.open( CommandLineParser.PartitionFileName.c_str(), ios_base::in );
        if ( ifsPartitionFile.fail() || ifsPartitionFile.bad() )
        {
            printf ("\n  Error: Cannot open file name <%s> !",CommandLineParser.PartitionFileName.c_str());
            bRet = false;
            goto Exit;
        }	

        ParsePartitionTextFile(ifsPartitionFile, m_PartitionHeader, m_pPartitionInfo);
        ifsPartitionFile.close();
    }
#endif

    if ( !m_TimDescriptor.Trusted() )
    {
#if TOOLS_GUI == 1
        if ( !ParseNonTrustedDescriptor (DownloaderInterface) )
#else
        if ( !ParseNonTrustedDescriptor (CommandLineParser) )
#endif
        {
            bRet = false;
            goto Exit;
        }
    }
    else
    {
#if TOOLS_GUI == 1
        if ( !ParseTrustedDescriptor (DownloaderInterface) )
#else
        if ( !ParseTrustedDescriptor (CommandLineParser) )
#endif
        {
            bRet = false;
            goto Exit;
        }
    }

Exit:

#if TOOLS_GUI == 1
    // keep image build and image build dlg in sync with TIM descriptor
    DownloaderInterface.ImageBuild.TimDescriptorFilePath( m_sTimFilePath );
#endif

    return bRet;
}

#if TOOLS_GUI == 1
bool CTimDescriptorParser::ParseNonTrustedDescriptor (CDownloaderInterface& DownloaderInterface)
#else
bool CTimDescriptorParser::ParseNonTrustedDescriptor (CCommandLineParser& CommandLineParser)
#endif
{
#if TOOLS_GUI == 1
    DownloaderInterface.DiscardAll();
#endif

    printf ("\nParsing non trusted TIM...\n\n");

    CTimDescriptorLine* pLine = 0;
    if ( pLine = CTimDescriptor::GetLineField( "Issue Date" ) )
    {
        m_TimDescriptor.TimHeader().VersionBind.IssueDate = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &m_TimDescriptor.TimHeader().VersionBind.IssueDate );
#endif
    }
    if ( pLine = CTimDescriptor::GetNextLineField( "OEM UniqueID", pLine ) )
    {
        m_TimDescriptor.TimHeader().VersionBind.OEMUniqueID = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &m_TimDescriptor.TimHeader().VersionBind.OEMUniqueID );
#endif
    }
    if ( pLine = CTimDescriptor::GetNextLineField( "Processor Type", pLine, false ) )
    {
        m_TimDescriptor.ProcessorType( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &m_TimDescriptor.ProcessorType() );
#endif
    }

    // since processor type may be in two places we need to find "Boot Flash Signature" 
    // starting at the beginning again
    if ( pLine = CTimDescriptor::GetLineField( "Boot Flash Signature" ) )
    {
        m_TimDescriptor.TimHeader().FlashInfo.BootFlashSign = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &m_TimDescriptor.TimHeader().FlashInfo.BootFlashSign );
#endif
    }
    if ( pLine = CTimDescriptor::GetNextLineField( "Number of Images", pLine ) )
    {
        m_TimDescriptor.TimHeader().NumImages = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &m_TimDescriptor.TimHeader().NumImages );
#endif
    }
    if ( pLine = CTimDescriptor::GetNextLineField( "Size of Reserved in bytes", pLine ) )
    {
        m_TimDescriptor.TimHeader().SizeOfReserved = Translate( pLine->m_FieldValue );
#if TOOLS_GUI == 1
        pLine->AddRef( &m_TimDescriptor.TimHeader().SizeOfReserved );
#endif
    }
    
#if TOOLS_GUI == 0
    if (CommandLineParser.bIsReservedDataInFile)
    {
        int sizeOfReservedDataFile = 0;
        ifstream ifsReservedDataFile;
        ifsReservedDataFile.open( CommandLineParser.ReservedFileName.c_str(), ios_base::in | ios_base::binary );
        if ( ifsReservedDataFile.bad() || ifsReservedDataFile.fail() )
        {	
            printf ("\n  Error: Cannot open file name <%s> !\n",CommandLineParser.ReservedFileName.c_str());
            return false;
        }

        // get file size
        ifsReservedDataFile.seekg( 0, ios_base::end );
        ifsReservedDataFile.clear();
        // note that the (int) cast is dangerous for very large files but should not be an issue here
        sizeOfReservedDataFile = (int)ifsReservedDataFile.tellg();
        // reset to SOF
        ifsReservedDataFile.seekg( 0, ios_base::beg );

        // note that the (UINT_T) cast is dangerous for very large files but should not be an issue here
        m_TimDescriptor.TimHeader().SizeOfReserved += (UINT_T)sizeOfReservedDataFile;
    }
#endif

    m_TimDescriptor.TimHeader().FlashInfo.WTMFlashSign = 0xFFFFFFFF;
    m_TimDescriptor.TimHeader().FlashInfo.WTMEntryAddr = 0xFFFFFFFF;
    m_TimDescriptor.TimHeader().FlashInfo.WTMEntryAddrBack = 0xFFFFFFFF;
    m_TimDescriptor.TimHeader().FlashInfo.WTMPatchSign = 0xFFFFFFFF;
    m_TimDescriptor.TimHeader().FlashInfo.WTMPatchAddr = 0xFFFFFFFF;

    string NextImageId;
    for (int i = 0; i < (int)m_TimDescriptor.TimHeader().NumImages; i++)
    {
        CImageDescription* pImageDesc = new CImageDescription();

        if (ParseImageInfo(*pImageDesc, pLine) == false) return false;

        if (i == 0)
        {
            if ( (Translate(pImageDesc->ImageIdTag()) & TYPEMASK) != (TIMIDENTIFIER & TYPEMASK) ) 
            {
                printf ("\n  Error: The ImageID value is incorrect for the TIM image!\n");
                return false;
            }

            if ( pLine = CTimDescriptor::GetNextLineField( "Image Filename", pLine ) )
            {
                m_sTimBinFilename = pLine->m_FieldValue;
#if TOOLS_GUI == 1
                pLine->AddRef( &pImageDesc->ImageFilePath() );
#endif
            }
            else
            {
                printf ("\n  Error: Key file parsing error reading, Image Filename: ImageId = <%s>\n", pImageDesc->ImageId().c_str());
                return false;
            }

            // prepend current directory if no path on file
            PrependPathIfNone( m_sTimBinFilename );
            pImageDesc->ImageFilePath( m_sTimBinFilename );
            
#if TOOLS_GUI == 1
            // keep image build and image build dlg in sync with TIM descriptor
            DownloaderInterface.ImageBuild.TimDescriptorFilePath( m_sTimBinFilename );
            pImageDesc->ImageSize( m_TimDescriptor.GetTimImageSize( DownloaderInterface.ImageBuild.CommandLineParser.bOneNANDPadding, DownloaderInterface.ImageBuild.CommandLineParser.uiPaddedSize ) );
#else
            pImageDesc->ImageSize( m_TimDescriptor.GetTimImageSize( CommandLineParser.bOneNANDPadding, CommandLineParser.uiPaddedSize ) );
#endif
            pImageDesc->ImageSizeToHash( 0 );
        }
        else
        {
            if (pImageDesc->ImageId() != NextImageId)
            {
                printf ("\n  Error: The ImageID doesn't = NextImageID from the previous image!");
                return false;
            }

            if ( pLine = CTimDescriptor::GetNextLineField( "Image Filename", pLine ) )
            {
                pImageDesc->ImageFilePath( pLine->m_FieldValue );
#if TOOLS_GUI == 1
                pLine->AddRef( &pImageDesc->ImageFilePath() );
#endif
            }
            else
            {
                printf ("\n  Error: Key file parsing error reading, Image Filename: ImageId = <%>s\n", pImageDesc->ImageId().c_str());
                return false;
            }

            // prepend current directory if no path on file
            PrependPathIfNone( pImageDesc->ImageFilePath() );

            ifstream ifsImage;
            ifsImage.open( pImageDesc->ImageFilePath().c_str(), ios_base::in | ios_base::binary );
            if ( ifsImage.bad() || ifsImage.fail() || !ifsImage.is_open() )
            {
                printf ("\n  Error: Cannot open image file name <%s>.\n",pImageDesc->ImageFilePath().c_str());
                pImageDesc->ImageSize( 0 );

#if TOOLS_GUI == 0
                return false;
#endif
            }
            else
            {
                // Get position of file, thus the file size.
                ifsImage.seekg( 0, ios_base::end );
                ifsImage.clear();

                // note that the (unsigned int) cast is dangerous for very large files 
                // but should not be an issue here
                pImageDesc->ImageSize( (unsigned int)ifsImage.tellg() ); 
                //printf("Size: %d\n", m_pImageInfo[i].ImageSize);
            }
            ifsImage.close();

#if TOOLS_GUI == 1
            // Output image file name
            if (CreateOutputImageName (pImageDesc->ImageFilePath(), m_sImageOutFilename) == false)
                return false;

            DownloaderInterface.AddBinImage( m_sImageOutFilename ); 
#endif

            pImageDesc->ImageSizeToHash( 0 );
            
            if(pImageDesc->ImageSizeToHash() != 0){
                //disable CRC FOR NOW
                /*if (GenerateImageCrc (hImage,m_pImageInfo[i].ImageSizeToHash,&ImageCrc) == false)
                {
                    if (m_hTimDescriptorTxtFile != NULL)
                    {
                        fclose (m_hTimDescriptorTxtFile);
                        m_hTimDescriptorTxtFile = NULL;
                    }
                    if (hImage != NULL)
                    {
                        fclose (hImage);
                        hImage = NULL;
                    }
                    return false;
                }
                m_pImageInfo[i].Hash[0] = ImageCrc;
                */
            }
        }

        m_TimDescriptor.ImagesList().push_back( pImageDesc );

        NextImageId = pImageDesc->NextImageId();
    }

    if ( m_TimDescriptor.ImagesList().size() != (int)m_TimDescriptor.TimHeader().NumImages )
        printf("Number of Images field is not consistent with image structs in TIM text file.\n");

    //check to make sure the images do not overlap each other
    if(CheckImageOverlap(m_TimDescriptor.ImagesList(), m_TimDescriptor.TimHeader().FlashInfo.BootFlashSign) == false)
    {
        return false;
    } 

    // skip the reserved section if size is 0.
    if(m_TimDescriptor.TimHeader().SizeOfReserved != 0)
    {
        if (ParseReservedData () == false) return false;
    }

    // if reserved data auto migrated then make sure to reset SizeOfReserved
    if ( m_TimDescriptor.ReservedDataList().size() == 0 )
        m_TimDescriptor.TimHeader().SizeOfReserved = 0;

#if EXTENDED_RESERVED_DATA
#if TOOLS_GUI == 1
    if (ParseExtendedReservedData(DownloaderInterface) == false) return false;
#else
    if (ParseExtendedReservedData(CommandLineParser) == false) return false;
#endif
#endif

#if TOOLS_GUI == 1
    if (DownloaderInterface.ImageBuild.Concatenate())
#else
    if (CommandLineParser.bConcatenate)
#endif
    {
        ifstream ifsImage;
        t_ImagesIter iter = m_TimDescriptor.ImagesList().begin();
        while ( iter != m_TimDescriptor.ImagesList().end() )
        {
            ifsImage.open( (*iter)->ImageFilePath().c_str(), ios_base::in | ios_base::binary );
            if ( ifsImage.bad() || ifsImage.fail() )
            {
                printf ("\n  Error: Cannot open image file name <%s>.\n",(*iter)->ImageFilePath().c_str());
#if TOOLS_GUI == 0
                return false;
#endif
            }
            ifsImage.close();
            iter++;
        }
    }

    m_TimDescriptor.Changed(false);

    printf ("  Success: Tim Descriptor file parsing has completed successfully!\n");
    return true;
}


bool CTimDescriptorParser::ParseImageInfo(CImageDescription& ImageDesc, CTimDescriptorLine*& pLine)
{
    // This is a helper function to parse image info data from the descriptor
    // text file supplied by the user.
    if ( pLine = CTimDescriptor::GetNextLineField( "Image ID", pLine ) )
    {
        ImageDesc.ImageIdTag(pLine->m_FieldValue);
#if TOOLS_GUI == 1
        pLine->AddRef( &ImageDesc.ImageIdTag() );
#endif
    }

    if ( pLine = CTimDescriptor::GetNextLineField( "Next Image ID", pLine ) )
    {
        ImageDesc.NextImageIdTag(pLine->m_FieldValue);
#if TOOLS_GUI == 1
        pLine->AddRef( &ImageDesc.NextImageIdTag() );
#endif
    }

    if ( pLine = CTimDescriptor::GetNextLineField( "Flash Entry Address", pLine ) )
    {
        ImageDesc.FlashEntryAddress(pLine->m_FieldValue);
#if TOOLS_GUI == 1
        pLine->AddRef( &ImageDesc.FlashEntryAddress() );
#endif
    }

    if ( pLine = CTimDescriptor::GetNextLineField( "Load Address", pLine ) )
    {
        ImageDesc.LoadAddress(pLine->m_FieldValue);
#if TOOLS_GUI == 1
        pLine->AddRef( &ImageDesc.LoadAddress() );
#endif
    }

    if(m_TimDescriptor.TimHeader().VersionBind.Trusted)
    {
        if ( pLine = CTimDescriptor::GetNextLineField( "Image Size To Hash in bytes", pLine ) )
        {
            ImageDesc.ImageSizeToHash( Translate( pLine->m_FieldValue ) );
#if TOOLS_GUI == 1
            pLine->AddRef( &ImageDesc.ImageSizeToHash() );
#endif
        }

        if ( pLine = CTimDescriptor::GetNextLineField( "Hash Algorithm ID", pLine ) )
        {
            if (pLine->m_FieldValue == "2" || pLine->m_FieldValue == "32" || pLine->m_FieldValue == "256")
                pLine->m_FieldValue = "SHA-256";
            else if (pLine->m_FieldValue == "64" || pLine->m_FieldValue == "512")
                pLine->m_FieldValue = "SHA-512";
            else
                pLine->m_FieldValue = "SHA-160";

            ImageDesc.HashAlgorithmId(pLine->m_FieldValue);
#if TOOLS_GUI == 1
            pLine->AddRef( &ImageDesc.HashAlgorithmId() );
#endif
        }
    }
    else
    {
        if ( pLine = CTimDescriptor::GetNextLineField( "Image Size To CRC in bytes", pLine ) )
        {
            ImageDesc.ImageSizeToCrc( pLine->m_FieldValue );
#if TOOLS_GUI == 1
            pLine->AddRef( &ImageDesc.ImageSizeToCrc() );
#endif
        }
    }

    if ( m_TimDescriptor.TimHeader().VersionBind.Version >= TBR_VERSIONS_WITH_PARTITION )
    {
        if ( pLine = CTimDescriptor::GetNextLineField( "Partition Number", pLine ) )
        {
            ImageDesc.PartitionNumber( Translate( pLine->m_FieldValue ) );
#if TOOLS_GUI == 1
            pLine->AddRef( &ImageDesc.PartitionNumber() );
#endif
        }
    }

    return true;
}


bool CTimDescriptorParser::VerifyDescriptorIntegrity ()
{
    bool IsOk = true;
    
    string sVersion;
    string sTrusted;
    string sLine;
    string sSizeOfReserved;

    // This function is the main entry point for verifying integrity of the
    // descriptor text file before any work is done. It is meant to be a quick
    // sanity check.

    printf ("\nVerifying Descriptor Integrity...\n\n");

    CTimDescriptorLine* pLine = 0;
    if ( pLine = CTimDescriptor::GetLineField( "Version" ) )
        sVersion = pLine->m_FieldValue;
    else
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: Version: declaration statement is missing or corrupted!\n");
        printf ("This statement must be on the first line!\n");
        printf ("Format: Version: 0x030101\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }

    if ( pLine = CTimDescriptor::GetLineField( "Trusted" ) )
        sTrusted = pLine->m_FieldValue;
    else
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: Trusted: declaration statement is missing or corrupted!\n");
        printf ("Format: Trusted: 0\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }

    if ( pLine = CTimDescriptor::GetLineField( "Size of Reserved in bytes" ) )
        sSizeOfReserved = pLine->m_FieldValue;

    if ((Translate (sSizeOfReserved) != 0) && (Translate (sVersion) >= (0x030102)))
        if (VerifyReservedData () == false) 
            IsOk = false;

    if (Translate (sTrusted) == 0)
    {
        if (VerifyNonTrusted () == false) IsOk = false;
        if (VerifyNumberOfImages () == false) IsOk = false;
    }
    else
    {
        if (VerifyNumberOfImages () == false) IsOk = false;
#if TRUSTED 
        if (VerifyNumberOfKeys () == false) IsOk = false;
#endif
    }

    return IsOk;
}


bool CTimDescriptorParser::VerifyNumberOfImages ()
{
    unsigned int i = 0;
    int NumberFound = 0;
    unsigned int iNumImages = 0;

    CTimDescriptorLine* pLine = 0;
    if ( (pLine = CTimDescriptor::GetLineField( "Number of Images", false )) == 0 )
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: Key file parsing error reading, Number of Images:\n");
        printf ("////////////////////////////////////////////////////////////\n");
#if TOOLS_GUI == 0
        return false;
#endif
    }

    if ( pLine )
        iNumImages = Translate(pLine->m_FieldValue);

    if ( iNumImages < 1 || iNumImages > MAX_IMAGES )
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: Number of Images: declaration statement must be > 0 and < 10!\n");
        printf ("////////////////////////////////////////////////////////////\n");
#if TOOLS_GUI == 0
        return false;
#endif
    }

    while ( (pLine = CTimDescriptor::GetNextLineField( "Image ID", pLine, false )) != 0 )
        NumberFound++;

    if (NumberFound != iNumImages)
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: The number of images found does not match the number of images\n");
        printf ("set in Number of Images: declaration statement!\n\n");
        printf ("Number of Images: %d\n", iNumImages);
        printf ("Number of images found = %d\n", NumberFound);
        printf ("////////////////////////////////////////////////////////////\n");
#if TOOLS_GUI == 0
        return false;
#endif
    }

    return true;
}


bool CTimDescriptorParser::VerifyNonTrusted ()
{
    bool bRet = true;

    CTimDescriptorLine* pLine = 0;
    if ( (pLine = CTimDescriptor::GetLineField( "WTM Save State", false )) != 0 )
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: The non trusted descriptor file should not contain any\n");
        printf ("WTM Save State declaration statements!\n\n");
        printf ("////////////////////////////////////////////////////////////\n");
        bRet = false;
    }

    if ( (pLine = CTimDescriptor::GetLineField( "Number of Keys", false )) != 0 )
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: The non trusted descriptor file should not contain a\n");
        printf ("Number of Keys: declaration statement!\n\n");
        printf ("////////////////////////////////////////////////////////////\n");
        bRet = false;
    }

    if ( (pLine = CTimDescriptor::GetLineField( "Key ID", false )) != 0 )
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: The non trusted descriptor file should not contain a\n");
        printf ("Key ID: declaration statement!\n\n");
        printf ("////////////////////////////////////////////////////////////\n");
        bRet = false;
    }

    if ( m_TimDescriptor.TimHeader().VersionBind.Version < TIM_3_3_00 )
    {
        if ( (pLine = CTimDescriptor::GetLineField( "Modulus Size in bytes", false )) != 0 )
        {
            printf ("\n////////////////////////////////////////////////////////////\n");
            printf ("  Error: The non trusted descriptor file should not contain a\n");
            printf ("Modulus Size in bytes: declaration statement!\n\n");
            printf ("////////////////////////////////////////////////////////////\n");
            bRet = false;
        }

        if ( (pLine = CTimDescriptor::GetLineField( "Public Key Size in bytes", false )) != 0 )
        {
            printf ("\n////////////////////////////////////////////////////////////\n");
            printf ("  Error: The non trusted descriptor file should not contain a\n");
            printf ("Public Key Size in bytes: declaration statement!\n\n");
            printf ("////////////////////////////////////////////////////////////\n");
            bRet = false;
        }
    }
    else
    {
        if ( (pLine = CTimDescriptor::GetLineField( "Key Size in bits", false )) != 0 )
        {
            printf ("\n////////////////////////////////////////////////////////////\n");
            printf ("  Error: The non trusted descriptor file should not contain a\n");
            printf ("Key Size in bits: declaration statement!\n\n");
            printf ("////////////////////////////////////////////////////////////\n");
            bRet = false;
        }

        if ( (pLine = CTimDescriptor::GetLineField( "Public Key Size in bits", false )) != 0 )
        {
            printf ("\n////////////////////////////////////////////////////////////\n");
            printf ("  Error: The non trusted descriptor file should not contain a\n");
            printf ("Public Key Size in bits: declaration statement!\n\n");
            printf ("////////////////////////////////////////////////////////////\n");
            bRet = false;
        }
    }
        
    if ( (pLine = CTimDescriptor::GetLineField( "RSA Public Key Exponent", false )) != 0 )
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: The non trusted descriptor file should not contain a\n");
        printf ("RSA Public Key Exponent: declaration statement!\n\n");
        printf ("////////////////////////////////////////////////////////////\n");
        bRet = false;
    }

    if ( (pLine = CTimDescriptor::GetLineField( "RSA System Modulus", false )) != 0 )
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: The non trusted descriptor file should not contain a\n");
        printf ("RSA System Modulus: declaration statement!\n\n");
        printf ("////////////////////////////////////////////////////////////\n");
        bRet = false;
    }

    if ( (pLine = CTimDescriptor::GetLineField( "DSA Algorithm ID", false )) != 0 )
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: The non trusted descriptor file should not contain a\n");
        printf ("DSA Algorithm ID: declaration statement!\n\n");
        printf ("////////////////////////////////////////////////////////////\n");
        bRet = false;
    }

    if ( (pLine = CTimDescriptor::GetLineField( "RSA Public Exponent", false )) != 0 )
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: The non trusted descriptor file should not contain a\n");
        printf ("RSA Public Exponent: declaration statement!\n\n");
        printf ("////////////////////////////////////////////////////////////\n");
        bRet = false;
    }

    if ( (pLine = CTimDescriptor::GetLineField( "RSA Private Key", false )) != 0 )
    {
        printf ("\n////////////////////////////////////////////////////////////\n");
        printf ("  Error: The non trusted descriptor file should not contain a\n");
        printf ("RSA Private Key: declaration statement!\n\n");
        printf ("////////////////////////////////////////////////////////////\n");
        bRet = false;
    }

    return bRet;
}


bool CTimDescriptorParser::VerifyReservedData ()
{
    char *pData = NULL;
    DWORD dwValue = 0; 
    int numOfHeaders = 0;
    int iTotal = 0;
    int headerCount = 0;
    bool termIdHeader = false;
    bool isWtpReservedAreaId = false;
    char* pToken = 0;
    DWORD dwPackageSize = 0;
    bool bPackageSizeNext = false;
    bool bRet = true;
    string sData;

    CTimDescriptorLine* pLine = 0;
    if ( (pLine = CTimDescriptor::GetLineField( "Reserved Data" )) )
    {
        while ( pLine = CTimDescriptor::GetNextLineField( pLine ) )
        {
            if ( !pLine->m_FieldName.empty() )
                break;
            dwValue = Translate(pLine->m_FieldValue); 
            iTotal++;
            // Check for reserved area ID
            if ((iTotal == 1) && (dwValue == WTPRESERVEDAREAID) )
            {
                isWtpReservedAreaId = true;
                continue;
            }

            if ( isWtpReservedAreaId == true )
            {
                // If there is a reserved area ID, next field should contain the number of package header ID's
                if ( iTotal == 2 )
                {
                    numOfHeaders = (int)dwValue;
                    continue;
                }
                //Count the number of package Header ID's 
                else if ((dwPackageSize == 0 ) &
                        (	// predefined package IDs
                            (dwValue == DDRID)		||
                            (dwValue == GPIOID) 	|| 
                            (dwValue == AUTOBIND)	||
                            (dwValue == UARTID) 	|| 
                            (dwValue == USBID) 		|| 
                            (dwValue == DDRGID)		||
                            (dwValue == DDRTID)		||
                            (dwValue == DDRCID)		||
                            (dwValue == CMCCID)		||
                            (dwValue == TZID)       ||
                            (dwValue == TZON)       ||
                            (dwValue == OPDIVID)    ||
                            (dwValue == OPMODEID)   ||
                            (dwValue == RESUMEBLID) ||
                            (dwValue == TBR_XFER)   ||
                            (dwValue == USB_DEVICE_DESCRIPTOR)	||
                            (dwValue == USB_CONFIG_DESCRIPTOR)	||
                            (dwValue == USB_INTERFACE_DESCRIPTOR)	||
                            (dwValue == USB_LANGUAGE_STRING_DESCRIPTOR)	||
                            (dwValue == USB_MANUFACTURER_STRING_DESCRIPTOR)	||
                            (dwValue == USB_PRODUCT_STRING_DESCRIPTOR)	||
                            (dwValue == USB_SERIAL_STRING_DESCRIPTOR)	||
                            (dwValue == USB_INTERFACE_STRING_DESCRIPTOR)	||
                            (dwValue == USB_DEFAULT_STRING_DESCRIPTOR)	||
                            (dwValue == USB_ENDPOINT_DESCRIPTOR) ||
                            (dwValue == USBVENDORREQ) 	||
                            (dwValue == ESCAPESEQID)	||
                            (dwValue == OEMCUSTOMID)	||
                            (dwValue == NOMONITORID)	||
                            (dwValue == COREID)	        ||
                            (dwValue == TZRI)           ||
                            (((dwValue & TYPEMASK)>>8) == DDRTYPE)
                        ) )
                {
                    headerCount++;
                    bPackageSizeNext = true;
                    continue;
                }
                // ReservedArea has to contain a terminator ID if it contains a reserved area ID as 1st field.
                else if (dwValue == TERMINATORID)
                {
                    termIdHeader = true;
                    headerCount++;
                    break;
                }
                else if ( bPackageSizeNext )
                {
                        dwPackageSize = dwValue - 8; // -8 because size includes tag and pkg len fields
                        bPackageSizeNext = false;
                        continue;
                }
                else
                {
                    // we are adding a data field to a package
                    if ( dwPackageSize > 0 )
                    {
                        dwPackageSize -= 4;								
                        continue;
                    }
                    else 
                    {
                        // finished adding data to the package
                        // reinit for next package
                        dwPackageSize = 0;
                    }
                }

                // if we get here, then we are not currently processing
                // a package and we didn't recognize the dwValue as a predefined
                // package id, so we assume it is a custom package id
                // immediately followed by a package size field
                if ( dwPackageSize == 0 )
                {
                    headerCount++;
                    bPackageSizeNext = true;
                }
            }
        }
    }

    if ( dwPackageSize != 0 )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf("  Error: Package size not consistent with supplied field values.\n");
        printf ("////////////////////////////////////////////////////////////\n");
    }

    if ( !termIdHeader && ( isWtpReservedAreaId == true) )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf("  Error: Terminator ID is incorrect or is missing.\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }

    if ( ( headerCount != numOfHeaders ) && ( isWtpReservedAreaId == true))
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf("  Error: Number of Reserved Area Packages %d does not equal the number of headers %d\n", 
                headerCount, numOfHeaders);
        printf ("////////////////////////////////////////////////////////////\n");
    }

    return true;
}


bool CTimDescriptorParser::VerifySizeOfTim ( CCommandLineParser& CommandLineParser  ) 
{
    int iTotalImageSize = m_TimDescriptor.GetTimImageSize( CommandLineParser.bOneNANDPadding, 
                                                           CommandLineParser.uiPaddedSize );
//	if (iTotalImageSize > TIMBUFFER)
//	{
//		printf ("  Error: The size of the Tim image is larger than the maximum allowable!\n");
//		printf ("The current Tim size allocated is set at %d bytes.\n",TIMBUFFER);
//		printf ("The current Tim image size is at %d bytes.\n",iTotalImageSize);
//		return false;
//	}

    return true;
}

#if TOOLS_GUI == 1
bool CTimDescriptorParser::ParseTrustedDescriptor (CDownloaderInterface& DownloaderInterface)
#else
bool CTimDescriptorParser::ParseTrustedDescriptor (CCommandLineParser& CommandLineParser)
#endif
{
    printf("  Error: You can only build a Non Trusted Image \n");
    printf("\twith the Non Trusted only version of the TBB Tool! \n\n");
    printf("Processing has stopped!\n");
    return false;
}

bool CTimDescriptorParser::ParseReservedData ()
{
    DWORD dwValue = 0; 
    int numOfHeaders = 0;
    int iTotal = 0, headerCount = 0;
    bool termIdHeader = false;
    bool isWtpReservedAreaId = false;
    DWORD dwPackageSize = 0;
    CReservedPackageData* pPackageData = 0;
    bool bPackageSizeNext = false;
    bool bRet = true;

    CTimDescriptorLine* pLine = 0;
    if ( (pLine = CTimDescriptor::GetLineField( "Reserved Data" )) )
    {
        while ( pLine = CTimDescriptor::GetNextLineField( pLine ) )
        {
            if ( !pLine->m_FieldName.empty() )
                break;
            
            dwValue = Translate(pLine->m_FieldValue); 
            iTotal++;

            // Check for reserved area ID
            if ((iTotal == 1) && (dwValue == WTPRESERVEDAREAID) )
            {
#if TOOLS_GUI == 1
                pLine->AddRef( (void*)WTPRESERVEDAREAID );
#endif
                isWtpReservedAreaId = true;
                continue;
            }

            if ( isWtpReservedAreaId == true )
            {
                // If there is a reserved area ID, next field should contain the number of package header ID's
                if ( iTotal == 2 )
                {
//#if TOOLS_GUI == 1
//					pLine->AddRef( &m_TimDescriptor.ReservedDataList());
//#endif
                    numOfHeaders = (int)dwValue;
                    continue;
                }
                //Count the number of package Header ID's 
                else 
                    if ( (dwPackageSize == 0 ) &
                        (	// predefined package IDs
                            (dwValue == DDRID)		|| 
                            (dwValue == GPIOID) 	|| 
                            (dwValue == AUTOBIND)	||
                            (dwValue == UARTID) 	|| 
                            (dwValue == USBID) 		|| 
                            (dwValue == DDRGID)		||
                            (dwValue == DDRTID)		||
                            (dwValue == DDRCID)		||
                            (dwValue == CMCCID)		||
                            (dwValue == TZID)       ||
                            (dwValue == TZON)       ||
                            (dwValue == OPDIVID)    ||
                            (dwValue == OPMODEID)   ||
                            (dwValue == RESUMEBLID) ||
                            (dwValue == TBR_XFER)   ||
                            (dwValue == USB_DEVICE_DESCRIPTOR)	||
                            (dwValue == USB_CONFIG_DESCRIPTOR)	||
                            (dwValue == USB_INTERFACE_DESCRIPTOR)	||
                            (dwValue == USB_LANGUAGE_STRING_DESCRIPTOR)	||
                            (dwValue == USB_MANUFACTURER_STRING_DESCRIPTOR)	||
                            (dwValue == USB_PRODUCT_STRING_DESCRIPTOR)	||
                            (dwValue == USB_SERIAL_STRING_DESCRIPTOR)	||
                            (dwValue == USB_INTERFACE_STRING_DESCRIPTOR)	||
                            (dwValue == USB_DEFAULT_STRING_DESCRIPTOR)	||
                            (dwValue == USB_ENDPOINT_DESCRIPTOR) ||
                            (dwValue == USBVENDORREQ) 	||
                            (dwValue == ESCAPESEQID)	||
                            (dwValue == OEMCUSTOMID)	||
                            (dwValue == NOMONITORID)	||
                            (dwValue == COREID)         ||
                            (dwValue == TZRI)           ||
                            (((dwValue & TYPEMASK)>>8) == DDRTYPE)
                        ) )
                {
                    // save the previous package if any
                    if ( pPackageData )
                    {
#if 0 // migration disabled for now
#if EXTENDED_RESERVED_DATA
                        if ( MigrateToExtendedReservedData( pPackageData ) )
                        {
                            m_TimDescriptor.TimHeader().SizeOfReserved -= pPackageData->Size();
                            delete pPackageData;
                            pPackageData = 0;
                        }
                        else
#endif
#endif
                            m_TimDescriptor.ReservedDataList().push_back( pPackageData );
                    }

                    headerCount++;
                    pPackageData = new CReservedPackageData;
#if TOOLS_GUI == 1
                    pLine->AddRef( pPackageData );
#endif
//					m_TimDescriptor.ReservedDataList().push_back( pPackageData );

                    // make hex digits all upper case, ignore leading 0x
                    pPackageData->PackageIdTag( HexFormattedAscii(dwValue) );
                    bPackageSizeNext = true;
                    continue;
                }
                // ReservedArea has to contain a terminator ID if it contains a reserved area ID as 1st field.
                else if (dwValue == TERMINATORID)
                {
#if TOOLS_GUI == 1
                    pLine->AddRef( (void*)TERMINATORID );
#endif
                    termIdHeader = true;
                    headerCount++;

                    // save the previous package if any
                    if ( pPackageData )
                    {
#if 0 // migration disabled for now
#if EXTENDED_RESERVED_DATA
                        if ( MigrateToExtendedReservedData( pPackageData ) )
                        {
                            m_TimDescriptor.TimHeader().SizeOfReserved -= pPackageData->Size();
                            delete pPackageData;
                        }
                        else
#endif
#endif
                            m_TimDescriptor.ReservedDataList().push_back( pPackageData );
                    }

                    pPackageData = 0;
                    break;
                }
                else if ( pPackageData != 0 )
                {
                    // we do not actually store the size in the package
                    // but use it to track if the package is fully loaded
                    if ( bPackageSizeNext )
                    {
                        dwPackageSize = dwValue - 8; // -8 because size includes tag and pkg len fields
                        bPackageSizeNext = false;
#if TOOLS_GUI == 1
                        pLine->AddRef( &(pPackageData->PackageDataList()) );
#endif
                        continue;
                    }
                    else
                    {
                        // we are adding a data field to a package
                        if ( dwPackageSize > 0 )
                        {
                            string* psValue = new string( HexFormattedAscii( dwValue ));
                            pPackageData->AddData( psValue, new string("") );
                            dwPackageSize -= 4;				
#if TOOLS_GUI == 1
                            pLine->AddRef( psValue );
#endif
                            continue;
                        }
                        else 
                        {
                            // save the previous package if any
                            if ( pPackageData )
                            {
#if 0 // migration disabled for now
#if EXTENDED_RESERVED_DATA
                                if ( MigrateToExtendedReservedData( pPackageData ) )
                                {
                                    m_TimDescriptor.TimHeader().SizeOfReserved -= pPackageData->Size();
                                    delete pPackageData;
                                }
                                else
#endif
#endif
                                    m_TimDescriptor.ReservedDataList().push_back( pPackageData );
                            }

                            // finished adding data to the package
                            // reinit for next package
                            pPackageData = 0;
                            dwPackageSize = 0;
                        }
                    }
                }

                // if we get here, then we are not currently processing
                // a package and we didn't recognize the dwValue as a predefined
                // package id, so we assume it is a custom package id
                // immediately followed by a package size field
                if ( pPackageData == 0 && dwPackageSize == 0 )
                {
                    headerCount++;

                    // unrecognized custom field tags 
                    pPackageData = new CReservedPackageData;
//					m_TimDescriptor.ReservedDataList().push_back( pPackageData );
#if TOOLS_GUI == 1
                    pLine->AddRef( pPackageData );
#endif
                    // make hex digits all upper case, ignore leading 0x
                    pPackageData->PackageIdTag( HexFormattedAscii(dwValue) );
                    bPackageSizeNext = true;
                }
            }
        }
    }
    
    assert( pPackageData == 0 );

    if ( dwPackageSize != 0 )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf("  Error: Package size not consistent with supplied field values.\n");
        printf ("////////////////////////////////////////////////////////////\n");
    }

    if ( !termIdHeader && ( isWtpReservedAreaId == true) )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf("  Error: Terminator ID is incorrect or is missing.\n");
        printf ("////////////////////////////////////////////////////////////\n");
        return false;
    }

    if ( ( headerCount != numOfHeaders ) && ( isWtpReservedAreaId == true))
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf("  Error: Number of Reserved Area Packages %d does not equal the number of headers %d\n", 
            headerCount, numOfHeaders);
        printf ("////////////////////////////////////////////////////////////\n");
    }

    return true;
}


#if EXTENDED_RESERVED_DATA
#if 0 // migration disabled for now
bool CTimDescriptorParser::MigrateToExtendedReservedData( CReservedPackageData* pPackageData )
{
    if ( pPackageData )
    {
        // create a proxy reference to the ExtendedReservedData
        CExtendedReservedData& Ext = m_TimDescriptor.ExtendedReservedData();
        CErdBase* pErd = 0;
        switch ( Translate(pPackageData->PackageIdTag()) )
        {
            case GPIOID:
                pErd = CGpioSet::Migrate( pPackageData );
                if ( pErd )
                {
                    Ext.ErdVec.push_back( pErd );
                    return true;
                }
                break;

            case AUTOBIND:
                pErd = CAutoBind::Migrate( pPackageData );
                if ( pErd )
                {
                    Ext.ErdVec.push_back( pErd );
                    return true;
                }
                break;

            case UARTID:
                pErd = CUart::Migrate( pPackageData );
                if ( pErd )
                {
                    Ext.ErdVec.push_back( pErd );
                    return true;
                }
                break;

            case USBID:
                pErd = CUsb::Migrate( pPackageData );
                if ( pErd )
                {
                    Ext.ErdVec.push_back( pErd );
                    return true;
                }
                break;

            case COREID:
                pErd = CCoreReset::Migrate( pPackageData );
                if ( pErd )
                {
                    Ext.ErdVec.push_back( pErd );
                    return true;
                }
                break;
            case TZRI:
                pErd = CTzInitialization::Migrate( pPackageData );
                if ( pErd )
                {
                    Ext.ErdVec.push_back( pErd );
                    return true;
                }
                break;

            // fall through all cases, no breaks
            case CMCCID:
            case TZID:
            case TZON:
            case OPDIVID:
            case OPMODEID:
            case RESUMEBLID:
            case TBR_XFER:
            case ESCAPESEQID:
            case NOMONITORID:
            case DDRGID:
            case DDRTID:
            case DDRCID:
            case DDRID:
            case USB_DEVICE_DESCRIPTOR:
            case USB_CONFIG_DESCRIPTOR:
            case USB_INTERFACE_DESCRIPTOR:
            case USB_LANGUAGE_STRING_DESCRIPTOR:
            case USB_MANUFACTURER_STRING_DESCRIPTOR:
            case USB_PRODUCT_STRING_DESCRIPTOR:
            case USB_SERIAL_STRING_DESCRIPTOR:
            case USB_INTERFACE_STRING_DESCRIPTOR:
            case USB_DEFAULT_STRING_DESCRIPTOR:
            case USB_ENDPOINT_DESCRIPTOR:
            case USBVENDORREQ:
                printf ("\n////////////////////////////////////////////////////////////\n");
                printf( "Reserved Data package migration to Extended Reserved Data format not supported!\n" );
                printf( "Migration failed for package: %s\n", pPackageData->PackageIdTag().c_str() );
                printf ("////////////////////////////////////////////////////////////\n");
                return false;

            // fall through all cases, no breaks
            case OEMCUSTOMID:
            default:
                return false;
        }
    }

    return false;
}
#endif

#if TOOLS_GUI == 1
bool CTimDescriptorParser::ParseExtendedReservedData (CDownloaderInterface& DownloaderInterface)
#else
bool CTimDescriptorParser::ParseExtendedReservedData (CCommandLineParser& CommandLineParser)
#endif
{
    bool bRet = true;
    string sData;
    DWORD dwValue = 0; 
    string sProcessorType;
    CErdBase* pErd = 0;
    string sEnd( "End " );

    // create a proxy reference to the ExtendedReservedData
    CExtendedReservedData& Ext = m_TimDescriptor.ExtendedReservedData();

    CTimDescriptorLine* pLine = 0;
    // parse optional extended reserved data area if it is found
    if ( pLine = CTimDescriptor::GetLineField( sExtendedReservedData, false ) )
    {
#if TOOLS_GUI == 1
        pLine->AddRef( &m_TimDescriptor.ExtendedReservedData() );
#endif
#if _DEBUG
        printf("\nParsing Extended Reserved Data area.\n");
#endif
        sProcessorType = TimDescriptor().ProcessorType();
        Ext.ProcessorType( sProcessorType );
    
        bool bRetry = false;
        unsigned int idx = 0;
        string sPackageEndLabel;

        while ( pLine = CTimDescriptor::GetNextLineField( pLine ) )
        {
            idx = 0;
            bRetry = false;
Retry:
            if ( pLine->m_FieldName.length() == 0 )
                continue;

            if ( pLine->m_FieldName.find( (sEnd+sExtendedReservedData) ) != string::npos )
                break;

            if ( pLine->m_FieldName.find("Processor Type") != string::npos )
            {
                if ( m_TimDescriptor.ProcessorType().empty() )
                    m_TimDescriptor.ProcessorType( pLine->m_FieldValue );
                Ext.ProcessorType( pLine->m_FieldValue );
                continue;
            }
            // parse Clock Enable section
            if ( pLine->m_FieldName.find( sClockEnable ) != string::npos )
            {
                sPackageEndLabel = (sEnd + sClockEnable);
                ParseDDRPackage( pLine, Ext.ClockEnableFields, Ext.g_ClockEnableFields, idx,
                    Ext, &CExtendedReservedData::AddClockEnableField, sPackageEndLabel, bRetry );
            }
            else // parse DDR Geometry section
                if ( pLine->m_FieldName.find( sDDRGeometry ) != string::npos )
            {
                sPackageEndLabel = (sEnd + sDDRGeometry);
                ParseDDRPackage( pLine, Ext.DDRGeometryFields, Ext.g_SdramSpecFields, idx,
                    Ext, &CExtendedReservedData::AddDDRGeometryField, sPackageEndLabel, bRetry );
            }
            else // parse DDR Timing section
                if ( pLine->m_FieldName.find( sDDRTiming ) != string::npos )
            {
                sPackageEndLabel = (sEnd + sDDRTiming);
                ParseDDRPackage( pLine, Ext.DDRTimingFields, Ext.g_SdramSpecFields, idx,
                    Ext, &CExtendedReservedData::AddDDRTimingField, sPackageEndLabel, bRetry );
            }
            else // parse DDR Custom section
                if ( pLine->m_FieldName.find( sDDRCustom ) != string::npos )
            {
                sPackageEndLabel = (sEnd + sDDRCustom);
                ParseDDRPackage( pLine, Ext.DDRCustomFields, Ext.g_DDRCustomFields, idx,
                    Ext, &CExtendedReservedData::AddDDRCustomField, sPackageEndLabel, bRetry );
            }
            else // parse Frequency section
                if ( pLine->m_FieldName.find( sFrequency ) != string::npos )
            {
                sPackageEndLabel = (sEnd + sFrequency);
                ParseDDRPackage( pLine, Ext.FrequencyFields, Ext.g_FrequencyFields, idx,
                    Ext, &CExtendedReservedData::AddFrequencyField, sPackageEndLabel, bRetry );
            }
            else // parse Voltages section
                if ( pLine->m_FieldName.find( sVoltages ) != string::npos )
            {
                sPackageEndLabel = (sEnd + sVoltages);
                ParseDDRPackage( pLine, Ext.VoltagesFields, Ext.g_VoltagesFields, idx,
                    Ext, &CExtendedReservedData::AddVoltagesField, sPackageEndLabel, bRetry );
            }
            else // parse CCMC section
                if ( pLine->m_FieldName.find( sConfigMemoryControl ) != string::npos )
            {
                sPackageEndLabel = (sEnd + sConfigMemoryControl);
                ParseDDRPackage( pLine, Ext.ConfigMemoryControlFields, Ext.g_ConfigMemoryControlFields, idx,
                    Ext, &CExtendedReservedData::AddConfigMemoryControlField, sPackageEndLabel, bRetry );
            }
            else // parse Trust Zone Regidsection
                if ( pLine->m_FieldName.find( sTrustZoneRegid ) != string::npos )
            {
                sPackageEndLabel = (sEnd + sTrustZoneRegid);
                ParseDDRPackage( pLine, Ext.TrustZoneRegidFields, Ext.g_TrustZoneRegidFields, idx,
                    Ext, &CExtendedReservedData::AddTrustZoneRegidField, sPackageEndLabel, bRetry );
            }
            else // parse Trust Zone section
                if ( pLine->m_FieldName.find( sTrustZone ) != string::npos )
            {
                sPackageEndLabel = (sEnd + sTrustZone);
                ParseDDRPackage( pLine, Ext.TrustZoneFields, Ext.g_TrustZoneFields, idx,
                    Ext, &CExtendedReservedData::AddTrustZoneField, sPackageEndLabel, bRetry );
            }
            else // parse OpDiv section
                if ( pLine->m_FieldName.find( sOpDiv ) != string::npos )
            {
                sPackageEndLabel = (sEnd + sOpDiv);
                ParseDDRPackage( pLine, Ext.OpDivFields, Ext.g_OpDivFields, idx,
                    Ext, &CExtendedReservedData::AddOpDivField, sPackageEndLabel, bRetry );
            }
            else 
                // parse OpMode section
                if ( pLine->m_FieldName.find( sOpMode ) != string::npos )
            {
                sPackageEndLabel = (sEnd + sOpMode);
                ParseDDRPackage( pLine, Ext.OpModeFields, Ext.g_OpModeFields, idx,
                    Ext, &CExtendedReservedData::AddOpModeField, sPackageEndLabel, bRetry );
            }
            else if ( pLine->m_FieldName.find( CGpioSet::Begin ) != string::npos )
            {
                pErd = new CGpioSet;
                ParseERDPackage( pLine, pErd, CGpioSet::Begin, CGpioSet::End );
            }
            else if ( pLine->m_FieldName.find( CUsbVendorReq::Begin ) != string::npos )
            {					
                pErd = new CUsbVendorReq;
                ParseERDPackage( pLine, pErd, CUsbVendorReq::Begin, CUsbVendorReq::End );
            }
            else if ( pLine->m_FieldName.find( CUsb::Begin ) != string::npos )
            {					
                pErd = new CUsb;
                ParseERDPackage( pLine, pErd, CUsb::Begin, CUsb::End );
            }
            else if ( pLine->m_FieldName.find( CUart::Begin ) != string::npos )
            {					
                pErd = new CUart;
                ParseERDPackage( pLine, pErd, CUart::Begin, CUart::End );
            }
            else if ( pLine->m_FieldName.find( CAutoBind::Begin ) != string::npos )
            {					
                pErd = new CAutoBind;
                ParseERDPackage( pLine, pErd, CAutoBind::Begin, CAutoBind::End );
            }
            else if ( pLine->m_FieldName.find( CResumeDdr::Begin ) != string::npos )
            {					
                pErd = new CResumeDdr;
                ParseERDPackage( pLine, pErd, CResumeDdr::Begin, CResumeDdr::End );
            }
            else if ( pLine->m_FieldName.find( CTBRXferSet::Begin ) != string::npos )
            {					
                pErd = new CTBRXferSet;
                ParseERDPackage( pLine, pErd, CTBRXferSet::Begin, CTBRXferSet::End );
            }
            else if ( pLine->m_FieldName.find( CEscapeSeq::Begin ) != string::npos )
            {					
                pErd = new CEscapeSeq;
                ParseERDPackage( pLine, pErd, CEscapeSeq::Begin, CEscapeSeq::End );
            }
            else if ( pLine->m_FieldName.find( CCoreReset::Begin ) != string::npos )
            {					
                pErd = new CCoreReset;
                ParseERDPackage( pLine, pErd, CCoreReset::Begin, CCoreReset::End );
            }
#if DDR_CONFIGURATION
            else if ( pLine->m_FieldName.find( CConsumerID::Begin ) != string::npos )
            {					
                CConsumerID* pConsumer = new CConsumerID;
                pErd = pConsumer;
                ParseERDPackage( pLine, pErd, CConsumerID::Begin, CConsumerID::End );
                // save the object
                Ext.m_Consumers.push_back( pConsumer );
                // clear temp ptr
                pErd = 0;
            }
            else if ( pLine->m_FieldName.find( CDDRInitialization::Begin ) != string::npos )
            {					
                pErd = new CDDRInitialization;
                ParseERDPackage( pLine, pErd, CDDRInitialization::Begin, CDDRInitialization::End );
            }
            else if ( pLine->m_FieldName.find( CInstructions::Begin ) != string::npos )
            {					
                pErd = new CInstructions;
                ParseERDPackage( pLine, pErd, CInstructions::Begin, CInstructions::End );
            }
            else if ( pLine->m_FieldName.find( CDDROperations::Begin ) != string::npos )
            {					
                pErd = new CDDROperations;
                ParseERDPackage( pLine, pErd, CDDROperations::Begin, CDDROperations::End );
            }
            else if ( pLine->m_FieldName.find( CTzInitialization::Begin ) != string::npos )
            {					
                pErd = new CTzInitialization;
                ParseERDPackage( pLine, pErd, CTzInitialization::Begin, CTzInitialization::End );
            }
            else if ( pLine->m_FieldName.find( CTzOperations::Begin ) != string::npos )
            {					
                pErd = new CTzOperations;
                ParseERDPackage( pLine, pErd, CTzOperations::Begin, CTzOperations::End );
            }
#endif
            else
            {
                printf("\nError: Unrecognized field in Extended Reserved Data in line <%s> <%s>\n", pLine->m_FieldName.c_str(), pLine->m_FieldValue.c_str());
                printf("Make sure all field and values in the Extended Reserved Data area are correct.\n");
                printf("Make sure 'Extended Reserved Data' area ends with 'End Extended Reserved Data:' tag in TIM descriptor file!\n");
                return false;
            }

            if ( pErd )
            {
                // save the object
                Ext.ErdVec.push_back( pErd );
                // clear temp ptr
                pErd = 0;
            }

            if ( bRetry )
                goto Retry;
        }

#if _DEBUG
        printf("\nCompleted parsing Extended Reserved Data area!\n");
#endif
    }

    int nBytesAdded = Ext.Size();
    if ( nBytesAdded > 0 )
    {
        // fix tim header sizes
        if ( m_TimDescriptor.TimHeader().SizeOfReserved == 0 )
            nBytesAdded += 16;  // + header & terminator pkgs

        m_TimDescriptor.TimHeader().SizeOfReserved += nBytesAdded;

        if ( m_TimDescriptor.ImagesList().size() > 0 )
        {
            CImageDescription* pImage = (*(m_TimDescriptor.ImagesList().begin()));
            if ( pImage > 0 && ( (Translate(pImage->ImageIdTag()) & TYPEMASK) == (TIMIDENTIFIER & TYPEMASK) ) )
            {
                if ( pImage->ImageSizeToHash() == pImage->ImageSize() )
                    pImage->ImageSizeToHash( pImage->ImageSizeToHash() + nBytesAdded );

#if TOOLS_GUI == 1
                pImage->ImageSize( m_TimDescriptor.GetTimImageSize( DownloaderInterface.ImageBuild.OneNANDPadding(), 
                                                                    DownloaderInterface.ImageBuild.PaddedSize() ) );
#else
                pImage->ImageSize( m_TimDescriptor.GetTimImageSize( CommandLineParser.bOneNANDPadding, 
                                                                    CommandLineParser.uiPaddedSize ) );
#endif
            }
        }
    }

    return true;
}

bool CTimDescriptorParser::ParseDDRPackage( 
    CTimDescriptorLine*& pLine, t_PairList& Fields, t_stringVector& g_FieldsNames, 
    unsigned int& idx, CExtendedReservedData& Ext,
    void (CExtendedReservedData::*AddField)( std::pair< unsigned int, unsigned int >*& ),
    string& sEndField, bool& bRetry)
{
#if TOOLS_GUI == 1
    pLine->AddRef( &Fields );
#endif
    bRetry = false;
    while ( pLine = CTimDescriptor::GetNextLineField( pLine ) )
    {
        if ( pLine->m_FieldName.length() == 0 )
            continue;

        if( ValidFieldIdx( pLine->m_FieldName, idx, g_FieldsNames ) )
        {
            pair<unsigned int, unsigned int>* pPair = new pair<unsigned int, unsigned int>( idx, Translate( pLine->m_FieldValue ));
#if TOOLS_GUI == 1
            pLine->AddRef( pPair );
#endif
            (Ext.*AddField)(pPair);
        }
        else
        {
            if ( pLine->m_FieldName != sEndField )
            {
                if ( !bRetry )
                {
                    bRetry = true;
                    break;
                }
                printf( "Warning: Found unrecognized field <%s> in <%s> package.\n", pLine->m_FieldName.c_str(), sClockEnable.c_str() );
            }
            break;
        }
    }
    return bRetry;
}

bool CTimDescriptorParser::ParseERDPackage( CTimDescriptorLine*& pLine, CErdBase*& pErd, const string& sBegin, const string& sEnd )
{
#if TOOLS_GUI == 1
    pLine->AddRef( pErd );
#endif
    if ( !pErd->Parse( m_TimDescriptor, pLine ) || (pLine->m_FieldName.find( sEnd ) == string::npos) )
    {
//        delete pErd;
//        pErd = 0;
        printf("\nError: Parsing of %s failed near line: '%s'\n", sBegin.c_str(), pLine->m_FieldName.c_str() );
        return false;
    }

    return true;
}


bool CTimDescriptorParser::SearchFieldValue( string& sLine, unsigned int& idx, t_stringVector& Fields, DWORD& dwValue )
{
    bool bFound = false;
    for ( idx=0; !bFound && (idx < Fields.size()); idx++ )
    {
        // find a valid field and get the value
        if ( GetFieldValue( sLine, *Fields[idx], dwValue ) )
        {
            bFound = true;
            sLine = "";
            break;
        }
    }

    return bFound;
}

bool CTimDescriptorParser::ValidFieldIdx( string& sFieldName, unsigned int& idx, t_stringVector& Fields )
{
    bool bFound = false;
    for ( idx=0; !bFound && (idx < Fields.size()); idx++ )
    {
        // find a valid field and get the value
        if ( sFieldName == *Fields[idx] )
        {
            bFound = true;
            break;
        }
    }

    return bFound;
}
#endif

bool CTimDescriptorParser::ParsePartitionTextFile(ifstream& ifsPartitionFile, PartitionTable_T& PartitionHeader, P_PartitionInfo_T& pPartitionInfo)
{
    string sValue;
    UINT_T IntParam = 0;
    UINT64 Int64Param = 0;
    UINT_T i = 0;

//	printf("\n\nProcessing partitioning information...\n");

    ifsPartitionFile.seekg(0, ios_base::beg);

    PartitionHeader.Identifier0 = MARVELL_PARTITION_TABLE_ID0;
    PartitionHeader.Identifier1 = MARVELL_PARTITION_TABLE_ID1;

    if ( !GetSValue (ifsPartitionFile, "Version:", sValue ) )
    {
        printf ("  Error: Key file parsing error reading, Version:\n");
        return false;
    }

    PartitionHeader.Version = Translate (sValue);

//printf ("  Version: 0x%X\n",PartitionHeader.Version );
    if ( !GetDWord( ifsPartitionFile, "Number of Partitions:", &IntParam ) )
    {
        printf ("  Error: Key file parsing error reading, Number of Partitions:\n");
        return false;
    }

    PartitionHeader.NumPartitions = IntParam;
    pPartitionInfo = (P_PartitionInfo_T) calloc((unsigned int)PartitionHeader.NumPartitions, sizeof(PartitionInfo_T));
    memset( pPartitionInfo, 0, (unsigned int)PartitionHeader.NumPartitions * sizeof(PartitionInfo_T) );

    for (int i = 0; i < (int)PartitionHeader.NumPartitions; i++)
    {
//ID
        if ( !GetDWord (ifsPartitionFile, "ID:", &IntParam) )
        {
            printf ("  Error: Key file parsing error reading, Partition ID:\n");
            return false;
        }
        pPartitionInfo[i].Indentifier = IntParam;

//printf ("  ID: %X\n", pPartitionInfo[i].PartitionInfoId );

//Usage		
        if ( !GetSValue (ifsPartitionFile, "Usage:", sValue ) )
        {
            printf ("  Error: Key file parsing error reading, Usage:\n");
            return false;
        }

//printf ("  Usage: 0x%X\n", pPartitionInfo[i].Usage );
        if ( ToUpper(sValue) == "BOOT" )
            pPartitionInfo[i].Usage = PI_USAGE_BOOT;
        else if ( ToUpper(sValue) == "OSLD" )
            pPartitionInfo[i].Usage = PI_USAGE_OSLD;
        else if ( ToUpper(sValue) == "KRNL" )
            pPartitionInfo[i].Usage = PI_USAGE_KRNL;
        else if ( ToUpper(sValue) == "FFOS" )
            pPartitionInfo[i].Usage = PI_USAGE_FFOS;
        else if ( ToUpper(sValue) == "FSYS" )
            pPartitionInfo[i].Usage = PI_USAGE_FSYS;
        else
        {
            string sIdReversed; 
            size_t i = min((int)sValue.length(),4);
            for ( ; i > 0; i-- )
                sIdReversed += sValue[i-1];
            while( sIdReversed.length() < 4 ) sIdReversed += '\0'; // nulls reserve bytes for int* dereference
            
            pPartitionInfo[i].Usage = *((int*)sIdReversed.c_str());
        }

//Type
        if ( !GetSValue (ifsPartitionFile, "Type:", sValue ) )
        {
            printf ("  Error: Key file parsing error reading, Type:\n");
            return false;
        }

        if ( sValue == "Logical" || ToUpper(sValue) == "LOGI" )
        {
            pPartitionInfo[i].Type = PI_TYPE_LOGICAL;
        }
        else if ( sValue == "Physical" || ToUpper(sValue) == "PHYS" )
        {
            pPartitionInfo[i].Type = PI_TYPE_PHYSICAL;
        }
        else
        {
            pPartitionInfo[i].Type = 0;
        }

//printf ("  Type: 0x%X\n",pPartitionInfo[i].Type );
 
//Attributes
        if ( !GetDWord(ifsPartitionFile, "Attributes:", &IntParam ) )
        {
            printf ("  Error: Key file parsing error reading, Partition Attributes:\n");
            return false;
        }
        *(UINT*)&pPartitionInfo[i].Attributes = IntParam;

//printf ("  Attributes: 0x%X\n", pPartitionInfo[i].PartitionAttr );
//Start Address

        if ( !GetDWord (ifsPartitionFile, "Start Address:", &IntParam) )
        {
            printf ("  Error: Key file parsing error reading, Partition Start Address:\n");
            return false;
        }
        pPartitionInfo[i].StartAddr = IntParam;

//printf ("  Start Address: 0x%X\n", pPartitionInfo[i].StartAddr );
//End Address
        if ( !GetDWord (ifsPartitionFile, "End Address:", &IntParam) )
        {
            printf ("  Error: Key file parsing error reading, Partition End Address:\n");
            return false;
        }
        pPartitionInfo[i].EndAddr = IntParam;

//printf ("  End Address: 0x%X\n", pPartitionInfo[i].EndAddr );

//RP Start Address
        // remember location in file
        streamoff ifsPos = ifsPartitionFile.tellg();
        if ( !GetDWord (ifsPartitionFile, "RP Start Address:", &IntParam) )
        {
            pPartitionInfo[i].ReserveStartAddr = 0;
            // ignore line not found and reposition to parse line again
            ifsPartitionFile.seekg( ifsPos, ios_base::beg );
        }
        else
        {
            pPartitionInfo[i].ReserveStartAddr = IntParam;
        }
//	printf ("  RP Start Address: 0x%X\n", pPartitionInfo[i].RsvdPoolStartAddr );

//RP Size
        // remember location in file
        ifsPos = ifsPartitionFile.tellg();
        if ( !GetDWord (ifsPartitionFile, "RP Size:", &IntParam) )
        {
            pPartitionInfo[i].ReserveSize = 0;
            // ignore line not found and reposition to parse line again
            ifsPartitionFile.seekg( ifsPos, ios_base::beg );
        }
        else
        {
            pPartitionInfo[i].ReserveSize = IntParam;
        }
//			printf ("  RP Size: 0x%X\n", pPartitionInfo[i].RsvdPoolSize );

//RP Algorithm
        // remember location in file
        ifsPos = ifsPartitionFile.tellg();
        if ( !GetSValue (ifsPartitionFile, "RP Algorithm:", sValue ) )
        {
            pPartitionInfo[i].ReserveAlgorithm = 0;
            // ignore line not found and reposition to parse line again
            ifsPartitionFile.seekg( ifsPos, ios_base::beg );
        }
        else
        {
            if (  ToUpper(sValue) == "UPWARD" || sValue == "UPWD" )
                pPartitionInfo[i].ReserveAlgorithm = PI_RP_ALGO_UPWD;
            else if ( ToUpper(sValue) == "DOWNWARD" || sValue == "DNWD" )
                pPartitionInfo[i].ReserveAlgorithm = PI_RP_ALGO_DNWD;
            else
                pPartitionInfo[i].ReserveAlgorithm = 0;
        }
//			printf ("  RP Algorithm: 0x%X\n", pPartitionInfo[i].RsvdPoolAlg );

//RT BBT Type
        // remember location in file
        ifsPos = ifsPartitionFile.tellg();
        if ( !GetSValue (ifsPartitionFile, "Runtime BBT Type:", sValue ) )
        {
            *(UINT*)&pPartitionInfo[i].BBT_Type = 0;
            // ignore line not found and reposition to parse line again
            ifsPartitionFile.seekg( ifsPos, ios_base::beg );
        }
        else
        {
            if ( ToUpper(sValue) == "MBBT" )
                *(UINT*)&pPartitionInfo[i].BBT_Type = BBT_TYPE_MBBT;
            else if ( ToUpper(sValue) == "WNCE" )
                *(UINT*)&pPartitionInfo[i].BBT_Type = BBT_TYPE_WNCE;
            else if ( ToUpper(sValue) == "LINXID" )
                *(UINT*)&pPartitionInfo[i].BBT_Type = BBT_TYPE_LINX;
            else
                *(UINT*)&pPartitionInfo[i].BBT_Type = 0;
         }

//RT BBT Location
        // remember location in file
        ifsPos = ifsPartitionFile.tellg();
        if ( !GetDWord (ifsPartitionFile, "Runtime BBT Location:", &IntParam) )
        {
            pPartitionInfo[i].RBBT_Location = 0;
            // ignore line not found and reposition to parse line again
            ifsPartitionFile.seekg( ifsPos, ios_base::beg );
        }
        else
            pPartitionInfo[i].RBBT_Location = IntParam;

//Backup BBT Location
        // remember location in file
        ifsPos = ifsPartitionFile.tellg();
        if ( !GetDWord (ifsPartitionFile, "Backup BBT Location:", &IntParam) )
        {
            pPartitionInfo[i].BackupRBBT_Location = 0;
            // ignore line not found and reposition to parse line again
            ifsPartitionFile.seekg( ifsPos, ios_base::beg );
        }
        else
            pPartitionInfo[i].BackupRBBT_Location = IntParam;
    
//printf ("  Runtime BBT Type: 0x%X\n", pPartitionInfo[i].RuntimeBBTType );
//printf ("  Runtime BBT Location: 0x%X\n", pPartitionInfo[i].RuntimeBBTStartAddr );
//printf ("  Backup BBT Location: 0x%X\n", pPartitionInfo[i].BackupRuntimeBBTStartAddr );
//printf("\n\n");
    }
    //end for loop 

//	printf("\n  Success: Partition file parsing has completed successfully!\n");
    return true;
}


#if TRUSTED
bool CTimDescriptorParser::FillDataArrays (CTimDescriptorLine*& pLine,DWORD *dwArray,int iSize) 
{
    int iTotal = 0;
    string sLine;
    string sData;
    size_t nBeg = 0;
    size_t nEnd = 0;
    CTimDescriptorLine* pPrevLine = pLine;

    
    // This function grabs one line at a time and parses out tokens that
    // are delimited by spaces, tabs or carriage returns. This process will
    // continue until it reads a line containing a semicolon or it reaches
    // the end of the file. The semicolon signifies a new section of the 
    // descriptor text file.

    while ( 1 )
    {
        // iTotal is number of ints in array
        // normalize iSize(bytes) to the min # of ints required to hold iSize bytes
        // some bytes may not be used, i.e. ECDSA_521
        if ((iTotal * 4) > (((iSize+3)/4)*4) )
        {
            printf ("////////////////////////////////////////////////////////////\n");
            printf ("Data bytes read > max allowable\n");
            return false;
        }

        // fail if the line has a field, but load it if it is just values

        if ( (pLine = CTimDescriptor::GetNextLineField( pLine )) == 0 )
            break;

        // done with list of values
        if ( !pLine->m_FieldName.empty() )
        {
            pLine = pPrevLine;
            return true;
        }
        pPrevLine = pLine;

        sLine = pLine->m_FieldValue;

        if ( sLine.length() > 0 )
        {
            nBeg = 0;
            nEnd = 0;
            // parse one or more values per line, separated by white space
            do
            {
                if ( string::npos == ( nEnd = sLine.find_first_of( " \n\t", nBeg ) ) )
                    sData = sLine.substr(nBeg);
                else
                    sData = sLine.substr(nBeg, nEnd - nBeg);

                if ( sData.length() == 0 )
                    // skip white space until next data
                    nBeg++;
                else
                {
                    *dwArray++ = Translate (sData); 
                    nBeg += (nEnd - nBeg)+1;
                    iTotal++;
                }
            }
            while ( nEnd != string::npos );
        }
    }

    // iTotal is number of ints in array
    // normalize iSize(bytes) to the min # of ints required to hold iSize bytes
    // some bytes may not be used, i.e. ECDSA_521
    if ((iTotal * 4) > (((iSize+3)/4)*4) )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("Data bytes read = %d\n",iTotal * 4);
        return false;
    }

    return true;
}

bool CTimDescriptorParser::FillDataArrays (ifstream& ifs,DWORD *dwArray,int iSize) 
{
    DWORD ImageOffset = 0;
    int iTotal = 0;
    string sLine;
    string sData;
    size_t nBeg = 0;
    size_t nEnd = 0;

    // This function grabs one line at a time and parses out tokens that
    // are delimited by spaces, tabs or carriage returns. This process will
    // continue until it reads a line containing a semicolon or it reaches
    // the end of the file. The semicolon signifies a new section of the 
    // descriptor text file.

    while ( ifs.good() )
    {
        while ( ifs.good() && GetNextLine( ifs, sLine ) )
        {
            // if 1st char (after leading white space) is a ;, 
            //  then line is a comment so skip
            if ( sLine[0] != ';' )
                break;
        }

        if ( ifs.eof() )
            break; 

        if ( ifs.bad() || ifs.fail() )
            return false;

        // iTotal is number of ints in array
        // normalize iSize(bytes) to the min # of ints required to hold iSize bytes
        // some bytes may not be used, i.e. ECDSA_521
        if ((iTotal * 4) > (((iSize+3)/4)*4) )
        {
            printf ("////////////////////////////////////////////////////////////\n");
            printf ("Data bytes read > max allowable\n");
            return false;
        }

        if ( sLine.length() > 0 )
        {
            // if there is a ":" then there are no more values to parse
            if ( sLine.find(":" ) != string::npos )
            {
                ifs.seekg( ImageOffset, ios::beg );
                break;
            }

            nBeg = 0;
            nEnd = 0;
            // parse one or more values per line, separated by white space
            do
            {
                if ( string::npos == ( nEnd = sLine.find_first_of( " \n\t", nBeg ) ) )
                    sData = sLine.substr(nBeg);
                else
                    sData = sLine.substr(nBeg, nEnd - nBeg);

                if ( sData.length() == 0 )
                    // skip white space until next data
                    nBeg++;
                else
                {
                    *dwArray++ = Translate (sData); 
                    nBeg += (nEnd - nBeg)+1;
                    iTotal++;
                }
            }
            while ( nEnd != string::npos );
        }

        // note that the (DWORD) cast is dangerous for very large files but should not be an issue here
        ImageOffset = (DWORD)ifs.tellg();
    }

    // iTotal is number of ints in array
    // normalize iSize(bytes) to the min # of ints required to hold iSize bytes
    // some bytes may not be used, i.e. ECDSA_521
    if ((iTotal * 4) > (((iSize+3)/4)*4) )
    {
        printf ("////////////////////////////////////////////////////////////\n");
        printf ("Data bytes read = %d\n",iTotal * 4);
        return false;
    }

    return true;
}
#endif
