/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "ReservedPackageData.h"
#include "TimLib.h"
#include "ImageDescription.h"
#include "TimDescriptorLine.h"

#if EXTENDED_RESERVED_DATA
#include "ExtendedReservedData.h"
#endif

#if TRUSTED
#include "Key.h"
#include "DigitalSignature.h"
#endif

enum eProcessorType { PXA_NONE = -1,
					  PXA168 = 0, 
					  PXA30x, PXA31x, PXA32x, 
					  PXA688,
					  PXA91x, PXA92x, PXA93x, PXA94x, PXA95x, 
					  ARMADA168,
					  ARMADA610,
                      MG1,
                      TAVOR_PV_MG2,
                      ESHEL,
                      PXA978,
                      ARMADA620,
                      ESHEL_LTE,
                      ARMADA622,
                      WUKONG,
//					  P_88AP001,
					  PXAMAX_PT 
				    };

const string gsProcessorType[PXAMAX_PT] = 
					{ "PXA168", 
					  "PXA30x","PXA31x","PXA32x",
					  "PXA688",
					  "PXA91x", "PXA92x", "PXA93x", "PXA94x", "PXA95x",
					  "ARMADA168",
					  "ARMADA610",
                      "MG1",
                      "TAVOR_PV_MG2",
                      "ESHEL",
                      "PXA978",
                      "ARMADA620",
                      "ESHEL_LTE",
                      "ARMADA622",
                      "WUKONG",
//					  "88AP001",
					};

const int TIMVersion	= 0x00030400;  // 
const int TIMDate		= 0x20101209;

class CTimDescriptor : public CTimLib
{
public:
	CTimDescriptor(void);
	virtual ~CTimDescriptor(void);

	// copy constructor
	CTimDescriptor( const CTimDescriptor& rhs );
	// assignment operator
	CTimDescriptor& operator=( const CTimDescriptor& rhs );

	void TimDescriptorFilePath( string& sFilePath ) { m_sTimDescriptorFilePath = sFilePath; m_bNotWritten = m_bChanged = true; }
	string& TimDescriptorFilePath() { return m_sTimDescriptorFilePath; }

	void Version( string& sVersion ) { m_TimHeader.VersionBind.Version = Translate(sVersion);  m_bNotWritten = m_bChanged = true;}
	string& Version() { return HexFormattedAscii(m_TimHeader.VersionBind.Version); }

	void Trusted( bool bSet ) { m_TimHeader.VersionBind.Trusted = (int)bSet;  m_bNotWritten = m_bChanged = true; }
	bool Trusted() { return m_TimHeader.VersionBind.Trusted == 0 ? false : true; }

	void IssueDate( string& sIssueDate ) { m_TimHeader.VersionBind.IssueDate = Translate(sIssueDate);  m_bNotWritten = m_bChanged = true;}
	string& IssueDate() { return HexFormattedAscii(m_TimHeader.VersionBind.IssueDate); }

	void OemUniqueId( string& sOemUniqueId ) { m_TimHeader.VersionBind.OEMUniqueID = Translate(sOemUniqueId);  m_bNotWritten = m_bChanged = true; }
	string& OemUniqueId() { return HexFormattedAscii(m_TimHeader.VersionBind.OEMUniqueID); }

	void BootRomFlashSignature( string& sBootRomFlashSignature ) { m_TimHeader.FlashInfo.BootFlashSign = Translate(sBootRomFlashSignature);  m_bNotWritten = m_bChanged = true; }
	string& BootRomFlashSignature() { return HexFormattedAscii(m_TimHeader.FlashInfo.BootFlashSign); }

	CImageDescription* Image( int idx );
	bool AddImage( CImageDescription* pImage );
	bool DeleteImage( CImageDescription* pImage );
	size_t ImagesCount() { return m_Images.size(); }

	bool KeysIsChanged() { return m_bKeysChanged; }
	void KeysChanged( bool bSet ) { m_bKeysChanged = bSet; }

	void WtmSaveStateFlashSignature( string& sSignature ) { m_TimHeader.FlashInfo.WTMFlashSign = Translate(sSignature); m_bNotWritten = m_bChanged = true; }
	string& WtmSaveStateFlashSignature() { return HexFormattedAscii(m_TimHeader.FlashInfo.WTMFlashSign); }

	void WtmSaveStateFlashEntryAddress( string& sAddress ) { m_TimHeader.FlashInfo.WTMEntryAddr = Translate(sAddress); m_bNotWritten = m_bChanged = true; }
	string& WtmSaveStateFlashEntryAddress() { return HexFormattedAscii(m_TimHeader.FlashInfo.WTMEntryAddr); }

	void WtmSaveStateBackupEntryAddress( string& sAddress ) { m_TimHeader.FlashInfo.WTMEntryAddrBack = Translate(sAddress); m_bNotWritten = m_bChanged = true; }
	string& WtmSaveStateBackupEntryAddress() { return HexFormattedAscii(m_TimHeader.FlashInfo.WTMEntryAddrBack); }

	CTIM& TimHeader() { return m_TimHeader; }

	void ProcessorType( const string& sProcessor );
	string& ProcessorType(){ return m_sProcessorType; }

	t_ImagesList& ImagesList(){ return m_Images; }
	bool UpdateNextImageIds();

	t_ReservedDataList& ReservedDataList(){ return m_ReservedDataList; }
	int ReservedDataTotalSize();

#if EXTENDED_RESERVED_DATA
	CExtendedReservedData& ExtendedReservedData(){ return m_ExtendedReservedData; }
#endif

#if TRUSTED
	t_KeyList& KeyList(){ return m_KeyList; }
	CDigitalSignature& DigitalSignature() { return m_DigitalSignature; }
#endif

#if TOOLS_GUI == 1
	bool SaveState( ofstream& ofs );
	bool LoadState( ifstream& ifs );

	string& TimDescriptorText();
	bool WriteTimDescriptorFile();
#endif

	bool AttachCommentedObject( stringstream& ss, void* pObject, const char* pszField, string& sValue );
	void DiscardTimDescriptorLines();
	
	static t_TimDescriptorLines& TimDescriptorLines() { return g_TimDescriptorLines; }
	static CTimDescriptorLine* GetLineField( string sFieldName, bool bNotFoundMsg = true, void* pObject = 0 );
	static CTimDescriptorLine* GetNextLineField( CTimDescriptorLine* pPrev, bool bNotFoundMsg = true );
	static CTimDescriptorLine* GetNextLineField( string sFieldName, CTimDescriptorLine* pPrev, bool bNotFoundMsg = true );
	static t_TimDescriptorLines g_TimDescriptorLines;

	bool IsChanged();
	void Changed( bool bSet );
	bool IsNotWritten() { return m_bNotWritten; };
	void NotWritten( bool bSet ) { m_bNotWritten = bSet; }

	bool ImagesIsChanged() { return m_bImagesChanged; }
	void ImagesChanged( bool bSet ) { m_bImagesChanged = bSet; }

	bool ReservedIsChanged() { return m_bReservedChanged; }
	void ReservedChanged( bool bSet ) { m_bReservedChanged = bSet; }

	void Reset();
	void DiscardAll();

	unsigned int GetTimImageSize( bool bOneNANDPadding = false, unsigned int uiPaddedSize = 0 ); 

private:
	CTIM	m_TimHeader;
	string	m_sTimDescriptorFilePath;
	string  m_sTimDescriptor;
	string	m_sProcessorType;

	t_ImagesList			m_Images;
	t_ReservedDataList		m_ReservedDataList;

#if EXTENDED_RESERVED_DATA
	CExtendedReservedData	m_ExtendedReservedData;
#endif

#if TRUSTED
	CDigitalSignature		m_DigitalSignature;
	t_KeyList				m_KeyList;
#endif

	bool m_bChanged;
	bool m_bImagesChanged;
	bool m_bReservedChanged;
	bool m_bKeysChanged;
	bool m_bNotWritten;
};

