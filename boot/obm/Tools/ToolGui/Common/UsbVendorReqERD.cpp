/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma message("Using UsbVendorReq Reserved Data")
#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif

#include "UsbVendorReqERD.h"
#include "TimDescriptorParser.h"

#if LINUX
#else
#if TOOLS_GUI == 1
static char buf[1024]={0};
#define printf(a, ...)  { sprintf_s( buf, 1024, a, __VA_ARGS__); theApp.DisplayMsg( CString(buf) ); }
#else
#define printf(a, ...)  { printf(a, __VA_ARGS__); fflush(stdout); }
#endif
#endif

const string CUsbVendorReq::Begin("Usb Vendor Request");
const string CUsbVendorReq::End("End Usb Vendor Request");

CUsbVendorReq::CUsbVendorReq()
	: CErdBase( USBVENDORREQ_ERD, USB_VENDOR_REQ_MAX )
{
	*m_FieldNames[REQUEST_TYPE] = "REQUEST_TYPE";
	*m_FieldNames[REQUEST] = "REQUEST";
	*m_FieldNames[VALUE] = "VALUE";
	*m_FieldNames[INDEX] = "INDEX";
	*m_FieldNames[LENGTH] = "LENGTH";
	*m_FieldNames[DATA] = "DATA";
}

CUsbVendorReq::~CUsbVendorReq()
{
	Reset();
}

// copy constructor
CUsbVendorReq::CUsbVendorReq( const CUsbVendorReq& rhs )
: CErdBase( rhs )
{
	// need to do a deep copy of lists to avoid dangling references
	CUsbVendorReq& nc_rhs = const_cast<CUsbVendorReq&>(rhs);

	t_stringVectorIter iter = nc_rhs.m_DataList.begin();
	while ( iter != nc_rhs.m_DataList.end() )
	{
		m_DataList.push_back( new string( *(*iter) ) );
		iter++;
	}
}

// assignment operator
CUsbVendorReq& CUsbVendorReq::operator=( const CUsbVendorReq& rhs )
{
	// assignment operator
	if ( &rhs != this )
	{
		CErdBase::operator=( rhs );

		Reset();

		// need to do a deep copy of lists to avoid dangling references
		CUsbVendorReq& nc_rhs = const_cast<CUsbVendorReq&>(rhs);

		t_stringVectorIter iter = nc_rhs.m_DataList.begin();
		while ( iter != nc_rhs.m_DataList.end() )
		{
			m_DataList.push_back( new string( *(*iter) ) );
			iter++;
		}
	}
	return *this;
}


void CUsbVendorReq::Reset()
{
	t_stringVectorIter iter = m_DataList.begin();
	while ( iter != m_DataList.end() )
	{
		delete *iter;
		iter++;
	}
	m_DataList.clear();

	t_stringVectorIter NamesIter = m_DataFieldNames.begin();
	while ( NamesIter != m_DataFieldNames.end() )
	{
		delete *NamesIter;
		NamesIter++;
	}
	m_DataFieldNames.clear();
}


bool CUsbVendorReq::ToBinary( ofstream& ofs )
{
	// validate size
	if ( m_FieldValues.size() !=  m_iMaxFieldNum )
		return false;

	ofs << USBVENDORREQ;
	ofs << PackageSize();
	unsigned int nField = ( Translate(*m_FieldValues[VALUE]) & 0xFFFF ) << 16;
	nField |= ( Translate(*m_FieldValues[REQUEST]) & 0xFF )<< 8;
	nField |= ( Translate(*m_FieldValues[REQUEST_TYPE]) & 0xFF );
	ofs << nField;

	nField = ( Translate(*m_FieldValues[LENGTH]) & 0xFFFF )<< 16;
	nField |= ( Translate(*m_FieldValues[INDEX]) & 0xFFFF );
	ofs << nField;

	t_stringVectorIter iter = m_DataList.begin();
	while ( iter != m_DataList.end() )
		ofs << Translate(*(*iter++));

	return ofs.good();
}


int CUsbVendorReq::AddPkgStrings( CReservedPackageData* pRPD )
{
	pRPD->PackageIdTag( HexFormattedAscii(USBVENDORREQ) );

	unsigned int nField = ( Translate(*m_FieldValues[VALUE]) & 0xFFFF ) << 16;
	nField |= ( Translate(*m_FieldValues[REQUEST]) & 0xFF )<< 8;
	nField |= ( Translate(*m_FieldValues[REQUEST_TYPE]) & 0xFF );
	
	pRPD->AddData( new string( HexFormattedAscii( nField )), new string( "wVALUE, bREQUEST, bREQUEST_TYPE" ) );

	nField = ( Translate(*m_FieldValues[LENGTH]) & 0xFFFF )<< 16;
	nField |= ( Translate(*m_FieldValues[INDEX]) & 0xFFFF );

	pRPD->AddData( new string( HexFormattedAscii( nField )), new string( "wLENGTH, wINDEX" ) );

	t_stringVectorIter iter = m_DataList.begin();
	while ( iter != m_DataList.end() )
	{
		pRPD->AddData( new string( *(*iter) ), new string( "data" ) );
		iter++;
	}

	return PackageSize();
}


bool CUsbVendorReq::Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine )
{
	string sValue;
	string sData;
	size_t nPos = 0;
	size_t nBeg = 0;
	size_t nEnd = 0;
	
	if ( pLine->m_FieldName != CUsbVendorReq::Begin )
	{
		printf( "\nError: Attempt to parse Usb Vendor Request failed!" );
		return false;
	}

	while ( pLine = TimDescriptor.GetNextLineField( pLine ) )
	{
		if ( pLine->m_FieldName == CUsbVendorReq::End )
			break;

		bool bFound = false;
		for ( unsigned int idx = 0; idx < m_FieldNames.size(); idx++ )
		{
			if ( pLine->m_FieldName == *m_FieldNames[ idx ] )
			{
				if ( idx == DATA )
				{
					*m_FieldValues[ idx ] = pLine->m_FieldValue;

					sValue = pLine->m_FieldValue;

					nBeg = 0;
					nEnd = 0;
					// parse one or more values per line, separated by white space
					do
					{
						if ( string::npos == ( nEnd = sValue.find_first_of( ": \n\t", nBeg ) ) )
							sData = sValue.substr(nBeg);
						else
							sData = sValue.substr(nBeg, nEnd - nBeg);

						if ( sData.length() == 0 )
							// skip white space until next data
							nBeg++;
						else
						{
							m_DataList.push_back( new string( sData ) );
							nBeg += (nEnd - nBeg)+1;
						}
					}
					while ( nEnd != string::npos );						
				}
				else
					*m_FieldValues[ idx ] = pLine->m_FieldValue;

				bFound = true;
				break;
			}
		}

		if ( !bFound )
			break;
	}

	// field not found
	return true;
}


unsigned int CUsbVendorReq::PackageSize() 
{ 
	return (unsigned int)( 8 + // package tag id + size
						   8 + // all fields packed except DATA
						   + m_DataList.size()*4);		// all data in data list
}


#if TOOLS_GUI == 1
bool CUsbVendorReq::SaveState( stringstream& ss )
{
	unsigned int temp = 0;
	if ( CErdBase::SaveState( ss ) )
	{
		temp = (unsigned int)m_DataList.size();
		SaveFieldState( ss, temp );

		t_stringVectorIter iter = m_DataList.begin();
		while ( iter != m_DataList.end() )
			if ( !SaveFieldState( ss, *(*iter++) ) )
				return false;
	}
	else
		return false;

	return true; // SUCCESS
}
#endif

#if TOOLS_GUI == 1
bool CUsbVendorReq::LoadState( ifstream& ifs )
{
	if ( theApp.ProjectVersion >= 0x03021200 )
	{
		string sbuf;

		Reset();

		if ( CErdBase::LoadState( ifs ) )
		{
			LoadFieldState( ifs, sbuf ); 
			unsigned int DataSize = Translate(sbuf);
		
			for ( unsigned int i = 0; i < DataSize; i++ )
			{
				string* psData = new string;
				LoadFieldState( ifs, *psData ); 
				m_DataList.push_back( psData );
			}
		}
	}
	
	return ifs.good() && !ifs.fail(); // success
}
#endif

#if TOOLS_GUI == 1
bool CUsbVendorReq::ToText( CTimDescriptor& TimDescriptor, stringstream& ss )
{
	if ( !TimDescriptor.AttachCommentedObject( ss, this, Begin.c_str(), string("") ) )
		ss << Begin << ":" << endl;

	for ( unsigned int i = 0; i < m_iMaxFieldNum-1; i++ )
	{
		if ( !TimDescriptor.AttachCommentedObject( ss, m_FieldNames[i], m_FieldNames[i]->c_str(), *m_FieldValues[i] ) )
		{
			ss << *m_FieldNames[i] << ": ";
			ss << *m_FieldValues[i];
	//		ss << "     // *m_FieldComments[i];
			ss << endl;
		}
	}

	if ( !TimDescriptor.AttachCommentedObject( ss, m_FieldNames[DATA], m_FieldNames[DATA]->c_str(), string("") ) )
	{
		ss << *m_FieldNames[DATA] << ": ";
	}

	t_stringVectorIter iter = m_DataList.begin();
	while ( iter != m_DataList.end() )
		ss << *(*iter++) << " ";

	ss << endl;

	if ( !TimDescriptor.AttachCommentedObject( ss, this, End.c_str(), string("") ) )
		ss << End << ":" << endl;
	return true;
}
#endif

t_stringVector& CUsbVendorReq::DataFieldNames()
{
	if ( m_DataFieldNames.size() != m_DataList.size() )
	{
		t_stringVectorIter NamesIter = m_DataFieldNames.begin();
		while ( NamesIter != m_DataFieldNames.end() )
		{
			delete *NamesIter;
			NamesIter++;
		}
		m_DataFieldNames.clear();

		t_stringVectorIter iter = m_DataList.begin();
		int idx = 0;
		while ( iter != m_DataList.end() )
		{
			stringstream ss;
			ss << "DATA[" << idx << "]";
			m_DataFieldNames.push_back( new string( ss.str() ));
			iter++;
			idx++;
		}
	}

	return m_DataFieldNames; 
}

