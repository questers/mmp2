/******************************************************************************
 *
 *  (C)Copyright 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "ErdBase.h"

#include <list>
using namespace std;

class CAutoBind : public CErdBase
{
public:
	CAutoBind();
	virtual ~CAutoBind();

	// copy constructor
	CAutoBind( const CAutoBind& rhs );
	// assignment operator
	virtual CAutoBind& operator=( const CAutoBind& rhs );

	static const string Begin;	// "AutoBind"
	static const string End;	// "End AutoBind"
	static CAutoBind* Migrate( CReservedPackageData* pRPD );

	virtual bool ToBinary( ofstream& ofs );

	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize() { return (unsigned int)(8+(m_FieldValues.size()*4)); }
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

#if TOOLS_GUI == 1
//	virtual bool LoadState( ifstream& ifs );
//	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	//AUTOBIND collides with tim.h so using eAUTOBIND instead
	enum AutoBind { eAUTOBIND, AUTOBIND_MAX };
};

typedef list<CAutoBind*>           t_AutoBindList;
typedef list<CAutoBind*>::iterator t_AutoBindListIter;

