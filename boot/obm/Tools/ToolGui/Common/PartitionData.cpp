/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#include "PartitionData.h"

CPartitionData::CPartitionData(void)
: CTimLib()
{
	m_PartitionId = 0x0;
	m_PartitionAttributes = 0x0;
	m_sPartitionUsage = "BOOT";
	m_sPartitionType = "Logi";
	m_PartitionStartAddress = 0UL;
	m_PartitionEndAddress = 0UL;
	m_ReservedPoolStartAddress = 0UL;
	m_ReservedPoolSize = 0x0;
	m_sReservedPoolAlgorithm = "UPWD";
	m_sRuntimeBBTType = "MBBT";
	m_RuntimeBBTLocation = 0x0;
	m_BackupRuntimeBBTLocation = 0x0;
	m_bChanged = false;
}

CPartitionData::~CPartitionData(void)
{
}

CPartitionData::CPartitionData( const CPartitionData& rhs )
: CTimLib(rhs)
{
	// copy constructor
	m_PartitionId = rhs.m_PartitionId;
	m_PartitionAttributes = rhs.m_PartitionAttributes;
	m_sPartitionUsage = rhs.m_sPartitionUsage;
	m_sPartitionType = rhs.m_sPartitionType;
	m_PartitionStartAddress = rhs.m_PartitionStartAddress;
	m_PartitionEndAddress = rhs.m_PartitionEndAddress;
	m_sRuntimeBBTType = rhs.m_sRuntimeBBTType;
	m_RuntimeBBTLocation = rhs.m_RuntimeBBTLocation;
	m_BackupRuntimeBBTLocation = rhs.m_BackupRuntimeBBTLocation;
	m_ReservedPoolStartAddress = rhs.m_ReservedPoolStartAddress;
	m_ReservedPoolSize = rhs.m_ReservedPoolSize;
	m_sReservedPoolAlgorithm = rhs.m_sReservedPoolAlgorithm;
	m_bChanged = rhs.m_bChanged;
}

CPartitionData& CPartitionData::operator=( const CPartitionData& rhs ) 
{
	// assignment operator
	if ( &rhs != this )
	{
		CTimLib::operator=(rhs);

		m_PartitionId = rhs.m_PartitionId;
		m_PartitionAttributes = rhs.m_PartitionAttributes;
		m_sPartitionUsage = rhs.m_sPartitionUsage;
		m_sPartitionType = rhs.m_sPartitionType;
		m_PartitionStartAddress = rhs.m_PartitionStartAddress;
		m_PartitionEndAddress = rhs.m_PartitionEndAddress;
		m_sRuntimeBBTType = rhs.m_sRuntimeBBTType;
		m_RuntimeBBTLocation = rhs.m_RuntimeBBTLocation;
		m_BackupRuntimeBBTLocation = rhs.m_BackupRuntimeBBTLocation;
		m_ReservedPoolStartAddress = rhs.m_ReservedPoolStartAddress;
		m_ReservedPoolSize = rhs.m_ReservedPoolSize;
		m_sReservedPoolAlgorithm = rhs.m_sReservedPoolAlgorithm;
		m_bChanged = rhs.m_bChanged;
	}
	return *this;
}

#if TOOLS_GUI == 1
bool CPartitionData::SaveState( ofstream& ofs )
{
	stringstream ss;
	ss << m_PartitionId << endl;
	ss << m_PartitionAttributes << endl;
	ss << m_sPartitionUsage << endl;
	ss << m_sPartitionType << endl;
	ss << m_PartitionStartAddress << endl;
	ss << m_PartitionEndAddress << endl;
	ss << m_sRuntimeBBTType << endl;
	ss << m_RuntimeBBTLocation << endl;
	ss << m_BackupRuntimeBBTLocation << endl;
	ss << m_ReservedPoolStartAddress << endl;
	ss << m_ReservedPoolSize << endl;
	ss << m_sReservedPoolAlgorithm << endl;

	ofs << ss.str();
	ofs.flush();

	return ofs.good(); // SUCCESS
}
#endif

#if TOOLS_GUI == 1
bool CPartitionData::LoadState( ifstream& ifs )
{
	string sbuf;

	::getline( ifs, sbuf );
	m_PartitionId = Translate(sbuf);

	::getline( ifs, sbuf );
	m_PartitionAttributes = Translate(sbuf);

	::getline( ifs, m_sPartitionUsage );
	::getline( ifs, m_sPartitionType );

	::getline( ifs, sbuf );
	m_PartitionStartAddress = TranslateQWord(sbuf.c_str());

	::getline( ifs, sbuf );
	m_PartitionEndAddress = TranslateQWord(sbuf.c_str());

	::getline( ifs, m_sRuntimeBBTType );

	::getline( ifs, sbuf );
	m_RuntimeBBTLocation = Translate(sbuf.c_str());

	::getline( ifs, sbuf );
	m_BackupRuntimeBBTLocation = Translate(sbuf.c_str());

	::getline( ifs, sbuf );
	m_ReservedPoolStartAddress = TranslateQWord(sbuf.c_str());

	::getline( ifs, sbuf );
	m_ReservedPoolSize = Translate(sbuf.c_str());

	::getline( ifs, m_sReservedPoolAlgorithm );

	return ifs.good() && !ifs.fail(); // success
}
#endif
