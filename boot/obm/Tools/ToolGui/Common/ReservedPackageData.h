/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once
#include "TimLib.h"

#include <string>
#include <list>
#include <iterator>
#include <fstream>
#include <iostream>
#include <strstream>
#include <sstream>
using namespace std;

typedef list<string*>           t_stringList;
typedef list<string*>::iterator t_stringListIter;

class CTimDescriptor;

class CReservedPackageData : public CTimLib
{
public:
	CReservedPackageData(void);
	virtual ~CReservedPackageData(void);

	// copy constructor
	CReservedPackageData( const CReservedPackageData& rhs );
	// assignment operator
	CReservedPackageData& operator=( const CReservedPackageData& rhs );

public:
	string& PackageId() { return m_sPackageId; }
	void PackageId( const string& sPackageId ){ m_sPackageId = sPackageId; m_bChanged = true; }

	string& PackageIdTag() { return m_sPackageIdTag; }
	void PackageIdTag( const string& sPackageIdTag );

	int Size();

	t_stringList& PackageDataList(){ return m_PackageDataList; }
	t_stringList& PackageCommentList(){ return m_PackageCommentList; }

	void DiscardAll();
	void DiscardDataAndComments();
	void AddData( string* psData, string* psComment );
	void DeleteData( string* psData );
	int DataBlock( string& sDataBlock );

#if TOOLS_GUI == 1
	bool LoadState( ifstream& ifs );
	bool SaveState( stringstream& ss );
#endif

	bool IsChanged();
	void Changed( bool bSet ){ m_bChanged = bSet; }

private:
	void PredefinedPackageComments( int idx, string *psComment );

private:
	string	m_sPackageId;
	string	m_sPackageIdTag;

	t_stringList m_PackageDataList;
	t_stringList m_PackageCommentList;

	bool m_bChanged;
};

typedef list<CReservedPackageData*>           t_ReservedDataList;
typedef list<CReservedPackageData*>::iterator t_ReservedDataListIter;

