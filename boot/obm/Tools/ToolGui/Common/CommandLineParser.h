/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once

#include "Typedef.h"
#include "TimDescriptor.h"

#include <string>
using namespace std;

#if TOOLS_GUI == 0
#define VERBOSE 'v'
#define HELP '?'
#define KEY 'r'
#define TIMIN 'i'
#define TIMOUT 'o'
#define HASH 'h'
#define DS 's'
#define OPTION 'm'
#define TIMVERIFY 'V'
#define RESERVED 'R'
#define CONCATENATE 'C'
#define PARTITION 'P' 
#define PROCESSOR_TYPE 'T'
#define PADDED_TIM_IMAGE 'O'
#define MIN_ARGS 2
#endif

#define MAX_IMAGE_FILES 10
#define MAX_PATH 260

class CCommandLineParser
{
public:
	CCommandLineParser(void);
	~CCommandLineParser(void);

#if TOOLS_GUI == 0
	bool ParseCommandLine (int argc,char* argv[]);
#endif

	bool CheckYourOptions ();
	void PrintUsage ();
	void Reset();

	bool bVerbose;
	bool bConcatenate;
	int iOption;
	bool bIsTimVerify;
	bool bOneNANDPadding;

	bool bIsNonTrusted;
	bool bIsReservedDataInFile;
	bool bIsKeyFile;
	bool bIsHashFile;
	bool bIsDsFile;
	bool bIsTimInFile;
	bool bIsTimOutFile;
	bool bIsPartitionDataFile;

	string KeyFileName; // TIM Descriptor text file
	string ReservedFileName;
	string HashFileName;
	string DsFileName;
	string TimInFileName;
	string TimOutFileName;
	string PartitionFileName;

	eProcessorType iProcessorType;
	unsigned int uiPaddedSize;

#if TOOLS_GUI == 1
	bool ProcessorType( string& sProcessorType );
	string ProcessorType();
#endif
};
