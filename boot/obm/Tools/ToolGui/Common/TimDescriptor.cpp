/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#if TOOLS_GUI == 1
#include "StdAfx.h"
#include "MarvellBootUtility.h"
#endif
#include "TimDescriptor.h"
#include "TimDescriptorLine.h"

#include <fstream>
#include <iostream>
#include <strstream>
#include <sstream>
#include "Tim.h"

#if LINUX
#include <string.h>
#endif

#pragma warning ( disable : 4996 ) // Disable warning messages

// static object to hold all the TIM.txt parsed lines and object references
t_TimDescriptorLines CTimDescriptor::g_TimDescriptorLines;

CTimDescriptor::CTimDescriptor(void)
: CTimLib()
#if EXTENDED_RESERVED_DATA
, m_ExtendedReservedData( m_sProcessorType )
#endif
{
    Reset();
    m_bChanged = false;
    m_bImagesChanged = false;
    m_bReservedChanged = false;
    m_bKeysChanged = false;
    m_bNotWritten = false;
}

CTimDescriptor::~CTimDescriptor(void)
{
    DiscardAll();
}

// copy constructor
CTimDescriptor::CTimDescriptor( const CTimDescriptor& rhs )
: CTimLib(rhs)
#if EXTENDED_RESERVED_DATA
, m_ExtendedReservedData( rhs.m_sProcessorType )
#endif
{
    // copy constructor
    CTimDescriptor& nc_rhs = const_cast<CTimDescriptor&>(rhs);

#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sTimDescriptorFilePath )) != 0 )
        pLine->AddRef( &m_sTimDescriptorFilePath );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sProcessorType )) != 0 )
        pLine->AddRef( &m_sProcessorType );
#endif

    m_sTimDescriptorFilePath = rhs.m_sTimDescriptorFilePath;
    m_sProcessorType = rhs.m_sProcessorType;
    m_sTimDescriptor = rhs.m_sTimDescriptor;

    memcpy( &m_TimHeader, &rhs.m_TimHeader, sizeof(CTIM) ); 

#if TOOLS_GUI == 1
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.VersionBind.Version )) != 0 )
        pLine->AddRef( &m_TimHeader.VersionBind.Version );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.VersionBind.Trusted )) != 0 )
        pLine->AddRef( &m_TimHeader.VersionBind.Trusted );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.VersionBind.IssueDate )) != 0 )
        pLine->AddRef( &m_TimHeader.VersionBind.IssueDate );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.VersionBind.OEMUniqueID )) != 0 )
        pLine->AddRef( &m_TimHeader.VersionBind.OEMUniqueID );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sProcessorType )) != 0 )
        pLine->AddRef( &m_sProcessorType );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.FlashInfo.BootFlashSign )) != 0 )
        pLine->AddRef( &m_TimHeader.FlashInfo.BootFlashSign  );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.FlashInfo.WTMFlashSign )) != 0 )
        pLine->AddRef( &m_TimHeader.FlashInfo.WTMFlashSign  );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.FlashInfo.WTMEntryAddr )) != 0 )
        pLine->AddRef( &m_TimHeader.FlashInfo.WTMEntryAddr  );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.FlashInfo.WTMEntryAddrBack )) != 0 )
        pLine->AddRef( &m_TimHeader.FlashInfo.WTMEntryAddrBack  );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.NumImages )) != 0 )
        pLine->AddRef( &m_TimHeader.NumImages  );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.SizeOfReserved )) != 0 )
        pLine->AddRef( &m_TimHeader.SizeOfReserved  );
#endif


    // need to do a deep copy of the lists to prevent dangling references
    t_ImagesIter iterImage = nc_rhs.m_Images.begin();
    while( iterImage != nc_rhs.m_Images.end() )
    {
        CImageDescription* pImage = new CImageDescription( *(*iterImage) );
        m_Images.push_back( pImage );
        iterImage++;
    }

    t_ReservedDataListIter iterData = nc_rhs.m_ReservedDataList.begin();
    while( iterData != nc_rhs.m_ReservedDataList.end() )
    {
        CReservedPackageData* pResData = new CReservedPackageData( *(*iterData) );
        m_ReservedDataList.push_back( pResData );
#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = GetLineField( "", false, *iterData )) != 0 )
            pLine->AddRef( pResData );
#endif
        iterData++;
    }

#if EXTENDED_RESERVED_DATA
    m_ExtendedReservedData = nc_rhs.m_ExtendedReservedData;
#endif

#if TRUSTED
    t_KeyListIter iterKey = nc_rhs.m_KeyList.begin();
    while( iterKey != nc_rhs.m_KeyList.end() )
    {
        CKey* pKey = new CKey( *(*iterKey ) );
        m_KeyList.push_back( pKey );
#if TOOLS_GUI == 1
        // update the object line reference
//			if ( (pLine = GetLineField( "", false, *iterKey )) != 0 )
//				pLine->AddRef( pKey );
#endif
        iterKey++;
    }

    m_DigitalSignature = nc_rhs.m_DigitalSignature;
#endif    
    
    m_bChanged = rhs.m_bChanged;
    m_bImagesChanged = rhs.m_bImagesChanged;
    m_bReservedChanged = rhs.m_bReservedChanged;
    m_bKeysChanged = rhs.m_bKeysChanged;
    m_bNotWritten = rhs.m_bNotWritten;
}

// assignment operator
CTimDescriptor& CTimDescriptor::operator=( const CTimDescriptor& rhs )
{
    // assignment operator
    if ( &rhs != this )
    {
        CTimLib::operator=(rhs);
        CTimDescriptor& nc_rhs = const_cast<CTimDescriptor&>(rhs);

        // delete the existing list and recreate a new one
        DiscardAll();

#if TOOLS_GUI == 1
        CTimDescriptorLine* pLine = 0;

        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sTimDescriptorFilePath )) != 0 )
            pLine->AddRef( &m_sTimDescriptorFilePath );

        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sProcessorType )) != 0 )
            pLine->AddRef( &m_sProcessorType );
#endif

        m_sTimDescriptorFilePath = rhs.m_sTimDescriptorFilePath;
        m_sProcessorType = rhs.m_sProcessorType;
        m_sTimDescriptor = rhs.m_sTimDescriptor;

        memcpy( &m_TimHeader, &rhs.m_TimHeader, sizeof(CTIM) ); 

#if TOOLS_GUI == 1
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.VersionBind.Version )) != 0 )
            pLine->AddRef( &m_TimHeader.VersionBind.Version );

        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.VersionBind.Trusted )) != 0 )
            pLine->AddRef( &m_TimHeader.VersionBind.Trusted );

        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.VersionBind.IssueDate )) != 0 )
            pLine->AddRef( &m_TimHeader.VersionBind.IssueDate );

        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.VersionBind.OEMUniqueID )) != 0 )
            pLine->AddRef( &m_TimHeader.VersionBind.OEMUniqueID );

        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_sProcessorType )) != 0 )
            pLine->AddRef( &m_sProcessorType );

        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.FlashInfo.BootFlashSign )) != 0 )
            pLine->AddRef( &m_TimHeader.FlashInfo.BootFlashSign  );

        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.FlashInfo.WTMFlashSign )) != 0 )
            pLine->AddRef( &m_TimHeader.FlashInfo.WTMFlashSign  );

        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.FlashInfo.WTMEntryAddr )) != 0 )
            pLine->AddRef( &m_TimHeader.FlashInfo.WTMEntryAddr  );

        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.FlashInfo.WTMEntryAddrBack )) != 0 )
            pLine->AddRef( &m_TimHeader.FlashInfo.WTMEntryAddrBack  );

        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.NumImages )) != 0 )
            pLine->AddRef( &m_TimHeader.NumImages  );
    
        if ( (pLine = CTimDescriptor::GetLineField( "", false, &nc_rhs.m_TimHeader.SizeOfReserved )) != 0 )
            pLine->AddRef( &m_TimHeader.SizeOfReserved  );
#endif

        // need to do a deep copy of the lists to prevent dangling references
        t_ImagesIter iterImage = nc_rhs.m_Images.begin();
        while( iterImage != nc_rhs.m_Images.end() )
        {
            CImageDescription* pImage = new CImageDescription( *(*iterImage) );
            m_Images.push_back( pImage );
            iterImage++;
        }

        t_ReservedDataListIter iterData = nc_rhs.m_ReservedDataList.begin();
        while( iterData != nc_rhs.m_ReservedDataList.end() )
        {
            CReservedPackageData* pResData = new CReservedPackageData( *(*iterData) );
            m_ReservedDataList.push_back( pResData );
#if TOOLS_GUI == 1
            // update the object line reference
            if ( (pLine = GetLineField( "", false, *iterData )) != 0 )
                pLine->AddRef( pResData );
#endif
            iterData++;
        }

#if EXTENDED_RESERVED_DATA
        m_ExtendedReservedData = nc_rhs.m_ExtendedReservedData;
#endif

#if TRUSTED
        t_KeyListIter iterKey = nc_rhs.m_KeyList.begin();
        while( iterKey != nc_rhs.m_KeyList.end() )
        {
            CKey* pKey = new CKey( *(*iterKey ) );
            m_KeyList.push_back( pKey );
#if TOOLS_GUI == 1
            // update the object line reference
//			if ( (pLine = GetLineField( "", false, *iterKey )) != 0 )
//				pLine->AddRef( pKey );
#endif
            iterKey++;
        }

        m_DigitalSignature = nc_rhs.m_DigitalSignature;
#endif	
        m_bChanged = rhs.m_bChanged;		
        m_bImagesChanged = rhs.m_bImagesChanged;
        m_bReservedChanged = rhs.m_bReservedChanged;
        m_bKeysChanged = rhs.m_bKeysChanged;
        m_bNotWritten = rhs.m_bNotWritten;
    }

    return *this;
}

bool CTimDescriptor::IsChanged()
{
#if EXTENDED_RESERVED_DATA
    return ( m_bChanged || m_bImagesChanged || m_bReservedChanged
#if TRUSTED
         || m_bKeysChanged || m_DigitalSignature.IsChanged() 
#endif
         || m_bKeysChanged || m_ExtendedReservedData.IsChanged() );
#else
    return ( m_bChanged || m_bImagesChanged || m_bReservedChanged 
#if TRUSTED
         || m_bKeysChanged || m_DigitalSignature.IsChanged() 
#endif
        );
#endif
}

void CTimDescriptor::Changed( bool bSet )
{ 
    m_bChanged = bSet; 
    m_bImagesChanged = bSet;
    m_bReservedChanged = bSet;
    m_bKeysChanged = bSet;
#if TRUSTED
    m_DigitalSignature.Changed( bSet );
#endif
    
#if EXTENDED_RESERVED_DATA
    m_ExtendedReservedData.Changed( bSet );
#endif

    t_ImagesIter iterImage = m_Images.begin();
    while( iterImage != m_Images.end() )
    {
        (*iterImage)->Changed( bSet );
        iterImage++;
    }

    t_ReservedDataListIter iterData = m_ReservedDataList.begin();
    while( iterData != m_ReservedDataList.end() )
    {
        (*iterData)->Changed( bSet );
        iterData++;
    }

#if TRUSTED
    t_KeyListIter iterKey = m_KeyList.begin();
    while( iterKey != m_KeyList.end() )
    {
        (*iterKey)->Changed( bSet );
        iterKey++;
    }
#endif

    if ( bSet )
        m_bNotWritten = true;
}

void CTimDescriptor::Reset()
{
    memset (&m_TimHeader,0,sizeof (CTIM));

    m_sTimDescriptorFilePath = "";
    m_sTimDescriptor = "";
    m_sProcessorType = "";
    m_TimHeader.VersionBind.Version = TIMVersion;
    m_TimHeader.VersionBind.IssueDate = TIMDate;
    m_TimHeader.VersionBind.Trusted = 0;
    m_TimHeader.VersionBind.OEMUniqueID = 0xFEDCBA98;
    m_TimHeader.FlashInfo.BootFlashSign = 0xFFFFFFFF; //i.e., 0x4E414E04; 'NAN'04
    m_TimHeader.FlashInfo.WTMFlashSign = 0xFFFFFFFF;  //i.e. 0x4E414E04;  'NAN'04
    m_TimHeader.FlashInfo.WTMEntryAddr = 0x00000000;
    m_TimHeader.FlashInfo.WTMEntryAddrBack = 0x00000000;
    
    m_bNotWritten = true;
    m_bChanged = false;
    m_bImagesChanged = false;
    m_bReservedChanged = false;
    m_bKeysChanged = false;
}

void CTimDescriptor::DiscardAll()
{
#if TOOLS_GUI == 1
    CTimDescriptorLine* pLine = 0;

    // update the object line reference
    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_sTimDescriptorFilePath )) != 0 )
        pLine->RemoveRef( &m_sTimDescriptorFilePath );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_TimHeader.VersionBind.Version )) != 0 )
        pLine->RemoveRef( &m_TimHeader.VersionBind.Version );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_TimHeader.VersionBind.Trusted )) != 0 )
        pLine->RemoveRef( &m_TimHeader.VersionBind.Trusted );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_TimHeader.VersionBind.IssueDate )) != 0 )
        pLine->RemoveRef( &m_TimHeader.VersionBind.IssueDate );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_TimHeader.VersionBind.OEMUniqueID )) != 0 )
        pLine->RemoveRef( &m_TimHeader.VersionBind.OEMUniqueID );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_sProcessorType )) != 0 )
        pLine->RemoveRef( &m_sProcessorType );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_TimHeader.FlashInfo.BootFlashSign )) != 0 )
        pLine->RemoveRef( &m_TimHeader.FlashInfo.BootFlashSign  );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_TimHeader.FlashInfo.WTMFlashSign )) != 0 )
        pLine->RemoveRef( &m_TimHeader.FlashInfo.WTMFlashSign  );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_TimHeader.FlashInfo.WTMEntryAddr )) != 0 )
        pLine->RemoveRef( &m_TimHeader.FlashInfo.WTMEntryAddr  );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_TimHeader.FlashInfo.WTMEntryAddrBack )) != 0 )
        pLine->RemoveRef( &m_TimHeader.FlashInfo.WTMEntryAddrBack  );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_TimHeader.NumImages )) != 0 )
        pLine->RemoveRef( &m_TimHeader.NumImages  );

    if ( (pLine = CTimDescriptor::GetLineField( "", false, &m_TimHeader.SizeOfReserved )) != 0 )
        pLine->RemoveRef( &m_TimHeader.SizeOfReserved  );
#endif

    t_ImagesIter iterImage = m_Images.begin();
    while( iterImage != m_Images.end() )
    {
        delete *iterImage;
        iterImage++;
    }
    m_Images.clear();

    t_ReservedDataListIter iterData = m_ReservedDataList.begin();
    while( iterData != m_ReservedDataList.end() )
    {
#if TOOLS_GUI == 1
        // update the object line reference
        if ( (pLine = CTimDescriptor::GetLineField( "", false, *iterData )) != 0 )
            pLine->RemoveRef( *iterData );
#endif
        delete *iterData;
        iterData++;
    }
    m_ReservedDataList.clear();

#if EXTENDED_RESERVED_DATA
    m_ExtendedReservedData.Reset();
#endif

#if TRUSTED
    t_KeyListIter iterKey = m_KeyList.begin();
    while( iterKey != m_KeyList.end() )
    {
#if TOOLS_GUI == 1
        // update the object line reference
//		if ( (pLine = CTimDescriptor::GetLineField( "", false, *iterKey )) != 0 )
//			pLine->RemoveRef( *iterKey );
#endif
        delete *iterKey;
        iterKey++;
    }
    m_KeyList.clear();

    m_DigitalSignature.DiscardAll();
    m_DigitalSignature.CKey::DiscardAll();
#endif
}

bool CTimDescriptor::AddImage( CImageDescription* pImage )
{
    string sValue("<None>");
    if ( pImage )
    {
        CImageDescription* pPrevImage = 0;
        if ( !m_Images.empty() )
            pPrevImage = m_Images.back();

        if ( pImage->ImageId().compare(0,3,"TIM") == 0 )
        {
            t_ImagesIter iterImage = m_Images.begin();
            while( iterImage != m_Images.end() )
                if ( (*iterImage++)->ImageId().compare(0,3,"TIM") == 0 )
                    return false;

            m_Images.push_front( pImage );
        }
        else
            m_Images.push_back( pImage );

        // new image is at end of list so there is no next image yet
        pImage->NextImageId( sValue );

        // prevous image needs to be updated
        if ( pPrevImage )
            pPrevImage->NextImageId( pImage->ImageId() );

        m_bImagesChanged = true;
        return true;
    }

    return false;
}

bool CTimDescriptor::DeleteImage( CImageDescription* pImage )
{
    bool bRet = false;
    if ( pImage )
    {
        t_ImagesIter iter = m_Images.begin();
        CImageDescription* pPrevImage = 0;
        while( iter != m_Images.end() )
        {
            if ( pImage == *iter )
            {
                if ( pPrevImage != 0 )
                    pPrevImage->NextImageId( pImage->NextImageId() );

                m_Images.remove(*iter);
                delete pImage;
                bRet = true;
                break;
            }
            pPrevImage = *iter;
            iter++;
        }
    }
    m_bImagesChanged = true;
    return bRet;
}

bool CTimDescriptor::UpdateNextImageIds()
{
    t_ImagesIter iter = m_Images.begin();
    CImageDescription* pImage = 0;
    CImageDescription* pPrevImage = 0;
    string sValue("<None>");
    while( iter != m_Images.end() )
    {
        // mark last accessed image with no next image id
        (*iter)->NextImageId( sValue );
        
        // previous image needs to be updated
        if ( pPrevImage )
        {
            pPrevImage->NextImageId( (*iter)->ImageId() );
        }
        pPrevImage = (*iter);
        iter++;
    }
    m_bImagesChanged = true;
    return true;
}

CImageDescription* CTimDescriptor::Image( int idx )
{
#if TOOLS_GUI == 1
    ASSERT( idx >= 0 && idx < (int)m_Images.size() ); 
#endif
    // increment over the first image which is the TIMx
    CImageDescription* pImage = 0;
    t_ImagesIter iter = m_Images.begin();
    while( idx > 0 && iter != m_Images.end() )
    {
        idx--; iter++;
    }
    if ( iter != m_Images.end() )
        pImage = *iter;

    return pImage;
}

void CTimDescriptor::ProcessorType( const string& sProcessor )
{ 
    m_sProcessorType = sProcessor;

#if TOOLS_GUI
    theApp.CommandLineParser.ProcessorType( m_sProcessorType );
#endif

#if EXTENDED_RESERVED_DATA
    ExtendedReservedData().ProcessorType( m_sProcessorType );
#endif
}

int CTimDescriptor::ReservedDataTotalSize()
{
    t_ReservedDataListIter iter = m_ReservedDataList.begin();
    int size = 0;
    while ( iter != m_ReservedDataList.end() )
        size += (*iter++)->Size();
    if ( size > 0 )
        size += 16;  // add OPTH header and TERM package sizes
    return size;
}

unsigned int CTimDescriptor::GetTimImageSize( bool bOneNANDPadding /* = false */, unsigned int uiPaddedSize /* = 0 */ ) 
{
    unsigned int iTimImageSize = 0;

    iTimImageSize = sizeof(CTIM);

    if ( m_TimHeader.VersionBind.Version < TIM_3_2_00 )
        iTimImageSize += (sizeof(IMAGE_INFO_3_1_0) * m_TimHeader.NumImages);
    else if ( m_TimHeader.VersionBind.Version < TIM_3_4_00 )
        iTimImageSize += (sizeof(IMAGE_INFO_3_2_0) * m_TimHeader.NumImages);
    else 
        iTimImageSize += (sizeof(IMAGE_INFO_3_4_0) * m_TimHeader.NumImages);

    iTimImageSize += m_TimHeader.SizeOfReserved;

    if ( Trusted() )
    {
        if ( m_TimHeader.VersionBind.Version < TIM_3_3_00 )
            iTimImageSize += (sizeof(KEY_MOD_3_1_0) * m_TimHeader.NumKeys);
        else if ( m_TimHeader.VersionBind.Version == TIM_3_3_00 )
            iTimImageSize += (sizeof(KEY_MOD_3_3_0) * m_TimHeader.NumKeys);
        else 
            iTimImageSize += (sizeof(KEY_MOD_3_4_0) * m_TimHeader.NumKeys);

        iTimImageSize += sizeof (PLAT_DS);
    }

    if ( bOneNANDPadding )
    {
        if ( iTimImageSize < uiPaddedSize )
            iTimImageSize = uiPaddedSize;
    }

//	if ( iTimImageSize > MAX_TIM_SIZE )
//	{
//		printf ("////////////////////////////////////////////////////////////\n");
//		printf ("  Error: Total TIM image Size exceeds 4096 bytes!\n");
//		printf ("////////////////////////////////////////////////////////////\n");
//	}

    return iTimImageSize;
}


void CTimDescriptor::DiscardTimDescriptorLines()
{
    t_TimDescriptorLinesIter Iter = g_TimDescriptorLines.begin();
    while( Iter != g_TimDescriptorLines.end() )
    {
        delete *Iter;
        Iter++;
    }
    g_TimDescriptorLines.clear();
}

CTimDescriptorLine* CTimDescriptor::GetNextLineField( string sFieldName, CTimDescriptorLine* pPrev, bool bNotFoundMsg )
{
    if ( g_TimDescriptorLines.size() > 0 )
    {
        t_TimDescriptorLinesIter Iter = g_TimDescriptorLines.begin();
        while ( Iter != g_TimDescriptorLines.end() )
        { 
            // find the starting point to search
            if ( (*Iter++) == pPrev )
            {
                // now find the next field with sFieldName
                while ( Iter != g_TimDescriptorLines.end() )
                {
                    if ( (*Iter)->m_FieldName == sFieldName )
                        return (*Iter);

                    // found a field following a list of hex value fields, as after a key
                    if ( sFieldName.empty() && !((*Iter)->m_FieldName.empty()) )
                        return (*Iter);

                    Iter++;
                }
            }
        }

        if ( bNotFoundMsg )
            printf ("\n  Warning: TIM Descriptor file parsing failed to find field <%s> near line following <%s> ", sFieldName.c_str(), pPrev != 0 ? pPrev->m_FieldName.c_str() : "<unknown>");
    }
    return 0;
}


CTimDescriptorLine* CTimDescriptor::GetNextLineField( CTimDescriptorLine* pPrev, bool bNotFoundMsg )
{
    if ( g_TimDescriptorLines.size() > 0 )
    {
        t_TimDescriptorLinesIter Iter = g_TimDescriptorLines.begin();
        while ( Iter != g_TimDescriptorLines.end() )
        { 
            // find the starting point to search
            if ( (*Iter++) == pPrev )
                return (Iter != g_TimDescriptorLines.end() ? (*Iter) : 0);
        }

        if ( bNotFoundMsg )
            printf ("\n  Warning: TIM Descriptor file parsing failed to find field near line following <%s> ", pPrev != 0 ? pPrev->m_FieldName.c_str() : "<unknown>");
    }
    return 0;
}

CTimDescriptorLine* CTimDescriptor::GetLineField( string sFieldName, bool bNotFoundMsg, void* pObject )
{
    if ( g_TimDescriptorLines.size() > 0 )
    {
        t_TimDescriptorLinesIter Iter = g_TimDescriptorLines.begin();
        while ( Iter != g_TimDescriptorLines.end() )
        {
            // match fieldname (or lack of fieldname) and object pointer
            if ( ((*Iter)->m_FieldName == sFieldName) && (pObject != 0 && (*Iter)->IsObject( pObject )) )  
                return (*Iter);

            // ignore fieldname but use object pointer
            if ( sFieldName.length() == 0 && (pObject != 0 && (*Iter)->IsObject( pObject )) )  
                return (*Iter);

            // match fieldname only, if object pointer not provided
            if ( pObject == 0 && (*Iter)->m_FieldName == sFieldName )
                return (*Iter);

            Iter++;
        }

        if ( bNotFoundMsg )
            printf ("\n  Warning: TIM Descriptor file parsing failed to find field: <%s>", sFieldName.c_str());
    }
    return 0;
}


#if TOOLS_GUI == 1
bool CTimDescriptor::AttachCommentedObject( stringstream& ss, void* pObject, const char* pszField, string& sValue )
{
    if ( pObject )
    {
        CTimDescriptorLine* pLine = 0;
        if ( (pLine = GetLineField(pszField, false, pObject)) != 0 )
        {
            pLine->PreCommentLinesText( ss );
            ss << pLine->m_LeadPadding;
            ss << pLine->m_FieldName;
            if ( pLine->m_FieldName != "" )
                ss << ": ";
            ss << pLine->m_ValuePadding;
            ss << sValue;
            pLine->PostCommentLinesText( ss );
            ss << endl;
            return true;
        }
    }
    return false;
}

string& CTimDescriptor::TimDescriptorText()
{
    char buf[40]={0};
    m_sTimDescriptor.clear();

    stringstream ss;
    if ( !AttachCommentedObject( ss, &m_TimHeader.VersionBind.Version, "Version", Version() ) )
        ss << "Version: " << Version() << endl;

    if ( !AttachCommentedObject( ss, &m_TimHeader.VersionBind.Trusted, "Trusted", HexFormattedAscii(Trusted()) ) )
        ss << "Trusted: " << HexFormattedAscii(Trusted()) << endl;

    if ( !AttachCommentedObject( ss, &m_TimHeader.VersionBind.IssueDate, "Issue Date", IssueDate() ) )
        ss << "Issue Date: " << IssueDate() << endl;

    if ( !AttachCommentedObject( ss, &m_TimHeader.VersionBind.OEMUniqueID, "OEM UniqueID", OemUniqueId() ) )
        ss << "OEM UniqueID: " << OemUniqueId() << endl;

    if ( !AttachCommentedObject( ss, &m_sProcessorType, "Processor Type", m_sProcessorType ) )
        ss << "Processor Type: " << m_sProcessorType << endl;

#if TRUSTED
    if ( Trusted() )
    {
        if ( !AttachCommentedObject( ss, &m_TimHeader.FlashInfo.WTMFlashSign, "WTM Save State Flash Signature", WtmSaveStateFlashSignature() ) )
            ss << "WTM Save State Flash Signature: " << WtmSaveStateFlashSignature() << endl;

        if ( !AttachCommentedObject( ss, &m_TimHeader.FlashInfo.WTMEntryAddr, "WTM Save State Entry", WtmSaveStateFlashEntryAddress() ) )
            ss << "WTM Save State Entry: " << WtmSaveStateFlashEntryAddress() << endl;

        if ( !AttachCommentedObject( ss, &m_TimHeader.FlashInfo.WTMEntryAddrBack, "WTM Save State Backup Entry", WtmSaveStateBackupEntryAddress() ) )
            ss << "WTM Save State Backup Entry: " << WtmSaveStateBackupEntryAddress() << endl;
    }
#endif

    if ( !AttachCommentedObject( ss, &m_TimHeader.FlashInfo.BootFlashSign, "Boot Flash Signature", BootRomFlashSignature() ) )
        ss << "Boot Flash Signature: " << BootRomFlashSignature() << endl;

    sprintf(buf, "%d", (int)m_Images.size());
    if ( !AttachCommentedObject( ss, &m_TimHeader.NumImages, "Number of Images", string(buf) ) )
        ss << "Number of Images: " << buf << endl;

#if TRUSTED
    if ( Trusted() )
    {		
        buf[0] = 0;
        sprintf(buf, "%d", (int)m_KeyList.size());
        if ( !AttachCommentedObject( ss, &m_TimHeader.NumKeys, "Number of Keys", string(buf) ) )
            ss << "Number of Keys: " << buf << endl << endl;
    }
#endif

    buf[0] = 0;
    sprintf(buf, "%d", ReservedDataTotalSize());
    if ( !AttachCommentedObject( ss, &m_TimHeader.SizeOfReserved, "Size of Reserved in bytes", string(buf) ) )
        ss << "Size of Reserved in bytes: " << buf << endl;

    // [Image Block]
    CImageDescription* pImage = 0;
    t_ImagesIter iter = m_Images.begin();
    while( iter != m_Images.end() )
    {
        pImage = *iter;

        // Image Tag is the hex ascii to use for the Image ID
        if ( !AttachCommentedObject( ss, &pImage->ImageIdTag(), "Image ID", pImage->ImageIdTag() ) )
            ss << "Image ID: " << pImage->ImageIdTag() << endl;

        // Next Image Tag is the hex ascii to use for the Next Image ID
        if ( !AttachCommentedObject( ss, &pImage->NextImageIdTag(), "Next Image ID", pImage->NextImageIdTag() ) )
            ss << "Next Image ID: " << pImage->NextImageIdTag() << endl;

        if ( !AttachCommentedObject( ss, &pImage->FlashEntryAddress(), "Flash Entry Address", pImage->FlashEntryAddress() ) )
            ss << "Flash Entry Address: " << pImage->FlashEntryAddress() << endl;

        if ( !AttachCommentedObject( ss, &pImage->LoadAddress(), "Load Address", pImage->LoadAddress() ) )
            ss << "Load Address: " << pImage->LoadAddress() << endl;

        if ( !Trusted() )
        {
            if ( !AttachCommentedObject( ss, &pImage->ImageSizeToCrc(), "Image Size To CRC in bytes", pImage->ImageSizeToCrc() ) )
                ss << "Image Size To CRC in bytes: " << pImage->ImageSizeToCrc() << endl;
        }
#if TRUSTED
        else // trusted
        {
            buf[0] = 0;
            if ( pImage->ImageId().compare(0,3,"TIM") == 0 )
            {
                if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_256"
                     || m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_521" )
                {
                    if ( (pImage->ImageSize() - (sizeof (UINT_T) * ((MAXRSAKEYSIZEWORDS*3)-(MAXECCKEYSIZEWORDS*2)))) > pImage->ImageSizeToHash() )
                    {
                        // preserve custom imagesize to hash in tim descriptor
                        sprintf(buf, "%d", pImage->ImageSizeToHash());
                    }
                    else
                    {
                        // else use 0xFFFFFFFF to force recalc on read
                        strcpy( buf, "0xFFFFFFFF" );
                    }
                }
                else
                {
                    if ( pImage->ImageSize() - (MAXRSAKEYSIZEWORDS * 2) > pImage->ImageSizeToHash() )
                    {
                        // preserve custom imagesize to hash in tim descriptor
                        sprintf(buf, "%d", pImage->ImageSizeToHash());
                    }
                    else
                    {
                        // else use 0xFFFFFFFF to force recalc on read
                        strcpy( buf, "0xFFFFFFFF" );
                    }
                }
            }
            else if ( pImage->ImageSize() >= pImage->ImageSizeToHash() )
            {
                sprintf(buf, "%d", pImage->ImageSizeToHash());
            }
            else
            {
                // else use 0xFFFFFFFF to force recalc on read
                strcpy( buf, "0xFFFFFFFF" );
            }

            if ( !AttachCommentedObject( ss, &pImage->ImageSizeToHash(), "Image Size To Hash in bytes", string(buf) ) )
                ss << "Image Size To Hash in bytes: " << buf << endl;

            buf[0] = 0;
            if ( pImage->HashAlgorithmId() == "SHA-160" )
                sprintf(buf, "%d", SHA160 );
            else if ( pImage->HashAlgorithmId() == "SHA-256" )
                sprintf(buf, "%d", SHA256 );
            else if ( pImage->HashAlgorithmId() == "SHA-512" )
                sprintf(buf, "%d", SHA512 );
            if ( !AttachCommentedObject( ss, &pImage->HashAlgorithmId(), "Hash Algorithm ID", string(buf) ) )
                ss << "Hash Algorithm ID: " << buf << endl;
        }
#endif

        if ( Version() >= "0x00030200" )
        {
            buf[0] = 0;
            sprintf(buf, "%d", pImage->PartitionNumber());
            if ( !AttachCommentedObject( ss, &pImage->PartitionNumber(), "Partition Number", string(buf) ) )
                ss << "Partition Number: " << buf << endl;
        }

        if ( !AttachCommentedObject( ss, &pImage->ImageFilePath(), "Image Filename", pImage->ImageFilePath() ) )
            ss << "Image Filename: " << pImage->ImageFilePath() << endl;

        iter++;
    }

#if TRUSTED
    if ( Trusted() && m_KeyList.size() > 0 )
    {
        // [Key Block]
        t_KeyListIter iterKey = m_KeyList.begin();
        CKey* pKey = 0;
        while( iterKey != m_KeyList.end() )
        {
            pKey = *iterKey;

            // Key Tag is the  hex ascii to use for the Key ID
            if ( !AttachCommentedObject( ss, &pKey->KeyTag(), "Key ID", pKey->KeyTag() ) )
                ss << "Key ID: " << pKey->KeyTag() << endl;

            buf[0] = 0;
            if ( pKey->HashAlgorithmId() == "SHA-160" )
                sprintf(buf, "%d", SHA160 );
            else if ( pKey->HashAlgorithmId() == "SHA-256" )
                sprintf(buf, "%d", SHA256 );
            else if ( pKey->HashAlgorithmId() == "SHA-512" )
                sprintf(buf, "%d", SHA512 );
            if ( !AttachCommentedObject( ss, &pKey->HashAlgorithmId(), "Hash Algorithm ID", string(buf) ) )
                ss << "Hash Algorithm ID: " << buf << endl;

            buf[0] = 0;
            if ( m_TimHeader.VersionBind.Version < TIM_3_3_00 )
            {
                sprintf(buf, "%d", (int)pKey->KeySize()/8);
                if ( !AttachCommentedObject( ss, &pKey->RsaSystemModulusList(), "Modulus Size in bytes", string(buf) ) )
                    ss << "Modulus Size in bytes: " << buf << endl;

                buf[0] = 0;
                sprintf(buf, "%d", (int)pKey->ActiveSizeOfList(pKey->PublicKeyExponentList())/8);
                if ( !AttachCommentedObject( ss, &pKey->PublicKeyExponentList(), "Public Key Size in bytes", string(buf) ) )
                    ss << "Public Key Size in bytes: " << buf << endl;
            }
            else
            {
                sprintf(buf, "%d", (int)pKey->KeySize());
                if ( !AttachCommentedObject( ss, &pKey->RsaSystemModulusList(), "Key Size in bits", string(buf) ) )
                    ss << "Key Size in bits: " << buf << endl;

                buf[0] = 0;
                sprintf(buf, "%d", (int)pKey->ActiveSizeOfList(pKey->PublicKeyExponentList()));
                if ( !AttachCommentedObject( ss, &pKey->PublicKeyExponentList(), "Public Key Size in bits", string(buf) ) )
                    ss << "Public Key Size in bits: " << buf << endl;
            }

            if ( Version() >= "0x00030400" )
            {
                buf[0] = 0;
                if ( pKey->EncryptAlgorithmId() == "PKCS1_v1_5" )
                    sprintf(buf, "%d", PKCS1_v1_5_Caddo );
                else if ( pKey->EncryptAlgorithmId() == "PKCS1_v2_1" )
                    sprintf(buf, "%d", PKCS1_v2_1_Caddo );
                else if ( pKey->EncryptAlgorithmId() == "PKCS1_v1_5 (Ippcp)" )
                    sprintf(buf, "%d", PKCS1_v1_5_Ippcp );
                else if ( pKey->EncryptAlgorithmId() == "PKCS1_v2_1 (Ippcp)" )
                    sprintf(buf, "%d", PKCS1_v2_1_Ippcp );
                else if ( pKey->EncryptAlgorithmId() == "ECDSA_256" )
                    sprintf(buf, "%d", ECDSA_256 );
                else if ( pKey->EncryptAlgorithmId() == "ECDSA_521" )
                    sprintf(buf, "%d", ECDSA_521 );
                else 
                    sprintf(buf, "%d", Marvell_DS );

                if ( !AttachCommentedObject( ss, &pKey->EncryptAlgorithmId(), "Encrypt Algorithm ID", string(buf) ) )
                    ss << "Encrypt Algorithm ID: " << buf << endl;
            }

            if ( pKey->EncryptAlgorithmId() == "ECDSA_256" || pKey->EncryptAlgorithmId() == "ECDSA_521" ) 
            {
                if ( !AttachCommentedObject( ss, &pKey->ECDSAPublicKeyCompXPacked(), "ECC Public Key Comp X", string("") ) )
                    ss << "ECC Public Key Comp X:" << endl; 
                ss << pKey->ECDSAPublicKeyCompXPacked(); 

                if ( !AttachCommentedObject( ss, &pKey->ECDSAPublicKeyCompYPacked(), "ECC Public Key Comp Y", string("") ) )
                    ss << "ECC Public Key Comp Y:" << endl; 
                ss << pKey->ECDSAPublicKeyCompYPacked();
            }
            else
            {
                if ( !AttachCommentedObject( ss, &pKey->PublicKeyExponentPacked(), "RSA Public Key Exponent", string("") ) )
                    ss << "RSA Public Key Exponent:" << endl; 
                ss << pKey->PublicKeyExponentPacked(); 

                if ( !AttachCommentedObject( ss, &pKey->RsaSystemModulus(), "RSA System Modulus", string("") ) )
                    ss << "RSA System Modulus:" << endl; 
                ss << pKey->RsaSystemModulus();
            }

            iterKey++;
        }
    }
#endif

    if ( ReservedDataTotalSize() > 0 )
    {
        // [Reserved Data]
        if ( !AttachCommentedObject( ss, &m_ReservedDataList, "Reserved Data", string("") ) )
            ss << "Reserved Data:" << endl;

        if ( !AttachCommentedObject( ss, (void*)WTPRESERVEDAREAID, "", HexFormattedAscii(WTPRESERVEDAREAID) ) )
            ss << HexFormattedAscii(WTPRESERVEDAREAID) << endl;	// OPTH

//		if ( !AttachCommentedObject( ss, &m_ReservedDataList, "", HexFormattedAscii((int)m_ReservedDataList.size()+1) ) )
//		{
            //+1 adds terminator package
            ss << HexFormattedAscii((int)m_ReservedDataList.size()+1) << endl;
//		}

        t_ReservedDataListIter iter = m_ReservedDataList.begin();
        while ( iter != m_ReservedDataList.end() )
        {
            if ( !AttachCommentedObject( ss, (*iter), "", (*iter)->PackageIdTag() ) )
                ss << (*iter)->PackageIdTag() << endl;

            if ( !AttachCommentedObject( ss, &((*iter)->PackageDataList()), "", HexFormattedAscii((*iter)->Size()) ) )
                ss << HexFormattedAscii((*iter)->Size()) << endl;

            t_stringListIter dataIter = (*iter)->PackageDataList().begin();
            while ( dataIter != (*iter)->PackageDataList().end() )
            {
                if ( !AttachCommentedObject( ss, &(*(*dataIter)), "", *(*dataIter)) )
                    ss << *(*dataIter) << endl;

                dataIter++;
            }
            
            iter++;
        }

        // add term package
        if ( !AttachCommentedObject( ss, (void*)TERMINATORID, "", HexFormattedAscii(TERMINATORID) ) )
            ss << HexFormattedAscii(TERMINATORID) << endl;	// TERM

        // term package size
        ss << "0x00000008" << endl;	
    }

#if EXTENDED_RESERVED_DATA
    // append the extended reserved data
    m_ExtendedReservedData.Text( ss, *this );
#endif

    // [ Digital Signature Block ]
#if TRUSTED
    if ( Trusted() && m_DigitalSignature.IncludeInTim() )
    {
        buf[0] = 0;
        if ( m_DigitalSignature.EncryptAlgorithmId() == "PKCS1_v1_5" )
            sprintf(buf, "%d", PKCS1_v1_5_Caddo);
        else if ( m_DigitalSignature.EncryptAlgorithmId() == "PKCS1_v2_1" )
            sprintf(buf, "%d", PKCS1_v2_1_Caddo);
        else if ( m_DigitalSignature.EncryptAlgorithmId() == "PKCS1_v1_5 (Ippcp)" )
            sprintf(buf, "%d", PKCS1_v1_5_Ippcp);
        else if ( m_DigitalSignature.EncryptAlgorithmId() == "PKCS1_v2_1 (Ippcp)" )
            sprintf(buf, "%d", PKCS1_v2_1_Ippcp);
        else if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_256" )
            sprintf(buf, "%d", ECDSA_256);
        else if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_521" )
            sprintf(buf, "%d", ECDSA_521);
        else
            sprintf(buf, "%d", 0);// default "Marvell"

        if ( !AttachCommentedObject( ss, &m_DigitalSignature.EncryptAlgorithmId(), "DSA Algorithm ID", string(buf) ) )
            ss << "DSA Algorithm ID: " << buf << endl;

        buf[0] = 0;
        sprintf(buf, "%d", (m_DigitalSignature.HashAlgorithmId() == "SHA-160" ? SHA160 : SHA256));
        if ( !AttachCommentedObject( ss, &m_DigitalSignature.HashAlgorithmId(), "Hash Algorithm ID", string(buf) ) )
            ss << "Hash Algorithm ID: " << buf << endl;

        buf[0] = 0;
        if ( m_TimHeader.VersionBind.Version < TIM_3_3_00 )
        {
            sprintf(buf, "%d", (m_DigitalSignature.KeySize()/8));// /8 = bits to bytes
            if ( !AttachCommentedObject( ss, &m_DigitalSignature.KeySize(), "Modulus Size in bytes", string(buf)) )
                ss << "Modulus Size in bytes: " << buf << endl;
        }
        else
        {
            sprintf(buf, "%d", (m_DigitalSignature.KeySize()));
            if ( !AttachCommentedObject( ss, &m_DigitalSignature.KeySize(), "Key Size in bits", string(buf)) )
                ss << "Key Size in bits: " << buf << endl;
        }

        if ( m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_256" || m_DigitalSignature.EncryptAlgorithmId() == "ECDSA_521" ) 
        {
            if ( !AttachCommentedObject( ss, &m_DigitalSignature.ECDSAPublicKeyCompX(), "ECDSA Public Key CompX", string("") ) )
                ss << "ECDSA Public Key CompX:" << endl;
            ss << m_DigitalSignature.ECDSAPublicKeyCompX();

            if ( !AttachCommentedObject( ss, &m_DigitalSignature.ECDSAPublicKeyCompY(), "ECDSA Public Key CompY", string("") ) )
                ss << "ECDSA Public Key CompY:" << endl; 
            ss << m_DigitalSignature.ECDSAPublicKeyCompY();

            if ( !AttachCommentedObject( ss, &m_DigitalSignature.ECDSAPrivateKey(), "ECDSA Private Key", string("") ) )
                ss << "ECDSA Private Key:" << endl; 
            ss << m_DigitalSignature.ECDSAPrivateKey();
        }
        else
        {
            if ( !AttachCommentedObject( ss, &m_DigitalSignature.PublicKeyExponent(), "RSA Public Exponent", string("") ) )
                ss << "RSA Public Exponent:" << endl;
            ss << m_DigitalSignature.PublicKeyExponent();

            if ( !AttachCommentedObject( ss, &m_DigitalSignature.RsaSystemModulus(), "RSA System Modulus", string("") ) )
                ss << "RSA System Modulus:" << endl; 
            ss << m_DigitalSignature.RsaSystemModulus();

            if ( !AttachCommentedObject( ss, &m_DigitalSignature.RsaPrivateKey(), "RSA Private Key", string("") ) )
                ss << "RSA Private Key:" << endl; 
            ss << m_DigitalSignature.RsaPrivateKey();
        }

        // add blank line
        ss << endl;
    }
#endif

    m_sTimDescriptor = ss.str();

    return m_sTimDescriptor;
}

bool CTimDescriptor::SaveState( ofstream& ofs )
{
    unsigned int temp = 0;
    stringstream ss;

    SaveFieldState( ss, m_sTimDescriptorFilePath );
    SaveFieldState( ss, m_TimHeader.VersionBind.Version );
    SaveFieldState( ss, m_TimHeader.VersionBind.Trusted );
    SaveFieldState( ss, m_TimHeader.VersionBind.IssueDate );
    SaveFieldState( ss, m_TimHeader.VersionBind.OEMUniqueID );
    SaveFieldState( ss, m_sProcessorType );
    SaveFieldState( ss, m_TimHeader.FlashInfo.BootFlashSign );
    SaveFieldState( ss, m_TimHeader.FlashInfo.WTMFlashSign );
    SaveFieldState( ss, m_TimHeader.FlashInfo.WTMEntryAddr );
    SaveFieldState( ss, m_TimHeader.FlashInfo.WTMEntryAddrBack );
    SaveFieldState( ss, m_TimHeader.NumImages );
    SaveFieldState( ss, m_TimHeader.NumKeys );
    SaveFieldState( ss, m_TimHeader.SizeOfReserved );
        
    temp = (unsigned int)m_Images.size();
    SaveFieldState( ss, temp );
    t_ImagesIter iter = m_Images.begin();
    while( iter != m_Images.end() )
        (*iter++)->SaveState( ss );

    temp = (unsigned int)m_ReservedDataList.size();
    SaveFieldState( ss, temp );
    t_ReservedDataListIter iterData = m_ReservedDataList.begin();
    while( iterData != m_ReservedDataList.end() )
        (*iterData++)->SaveState( ss );

#if EXTENDED_RESERVED_DATA
    ExtendedReservedData().SaveState( ss );
#endif

#if TRUSTED
    if ( Trusted() )
    {
        temp = (unsigned int)m_KeyList.size();
        SaveFieldState( ss, temp );

        t_KeyListIter iterKey = m_KeyList.begin();
        while( iterKey != m_KeyList.end() )
            (*iterKey++)->SaveState( ss );
    }
#endif


#if TRUSTED
    if ( Trusted() )
    {
        m_DigitalSignature.SaveState( ss );
    }
#endif

    ofs << ss.str();
    ofs.flush();

    return ofs.good() && !ofs.fail(); // SUCCESS
}

bool CTimDescriptor::LoadState( ifstream& ifs )
{
    string sbuf;

    // remove the existing lists and load new ones
    DiscardAll();
    Reset();

    LoadFieldState( ifs, m_sTimDescriptorFilePath );
    LoadFieldState( ifs, m_TimHeader.VersionBind.Version );
    LoadFieldState( ifs, m_TimHeader.VersionBind.Trusted );

#if TRUSTED
    // ok to load trusted or non-trusted project with trusted version
#else
    if ( Trusted() )
    {
        AfxMessageBox("Error:  Cannot open trusted project with non-trusted tools.");
        return false;
    }
#endif
    
    LoadFieldState( ifs, m_TimHeader.VersionBind.IssueDate ); 
    LoadFieldState( ifs, m_TimHeader.VersionBind.OEMUniqueID ); 

    if ( theApp.ProjectVersion >= 0x03021300 )
        LoadFieldState( ifs, m_sProcessorType );

    if ( theApp.ProjectVersion >= 0x03021400 )
        ExtendedReservedData().ProcessorType(m_sProcessorType);

    LoadFieldState( ifs, m_TimHeader.FlashInfo.BootFlashSign ); 
    LoadFieldState( ifs, m_TimHeader.FlashInfo.WTMFlashSign ); 
    LoadFieldState( ifs, m_TimHeader.FlashInfo.WTMEntryAddr );
    LoadFieldState( ifs, m_TimHeader.FlashInfo.WTMEntryAddrBack ); 
    LoadFieldState( ifs, m_TimHeader.NumImages );
    LoadFieldState( ifs, m_TimHeader.NumKeys);  
    LoadFieldState( ifs, m_TimHeader.SizeOfReserved ); 

    unsigned int ImagesListSize = 0;
    LoadFieldState( ifs, ImagesListSize );
    while ( ImagesListSize > 0 )
    {
        CImageDescription* pImage = new CImageDescription();
        if ( pImage->LoadState( ifs ) )
            m_Images.push_back( pImage );

        ImagesListSize--;
    }

    unsigned int ReservedDataListSize = 0;
    LoadFieldState( ifs, ReservedDataListSize );
    while ( ReservedDataListSize > 0 )
    {
        CReservedPackageData* pData = new CReservedPackageData;
        if ( pData->LoadState( ifs ) )
            m_ReservedDataList.push_back( pData );

        ReservedDataListSize--;
    }

#if EXTENDED_RESERVED_DATA
    ExtendedReservedData().LoadState( ifs );
#endif

#if TRUSTED
    if ( Trusted() )
    {
        unsigned int KeyListSize = 0;
        LoadFieldState( ifs, KeyListSize );  
        while ( KeyListSize > 0 )
        {
            CKey* pKey = new CKey;
            if ( pKey->LoadState( ifs ) )
                m_KeyList.push_back( pKey );

            KeyListSize--;
        }
    }
#endif

#if TRUSTED
    if ( Trusted() && !m_DigitalSignature.LoadState( ifs ) )
    {
        AfxMessageBox( "Error:  LoadState for Digital Signature failed." );
        return false;
    }
#endif


    return ifs.good() && !ifs.fail(); // success
}

bool CTimDescriptor::WriteTimDescriptorFile()
{
    if ( m_sTimDescriptorFilePath.empty() )
        return false;

    if ( m_Images.size() == 0 )
    {
        AfxMessageBox( "No images have been added to TIM descriptor." );
//		return false;
    }

    t_ImagesIter iter = m_Images.begin();
    if ( iter != m_Images.end() )
    {
        if ( (*iter)->ImageId().compare(0,3,"TIM") != 0 )
        {
            AfxMessageBox( "TIM Descriptor file not correct: First Image in ImageList MUST be a TIM*!" );
//			return false;
        }
    }
//	else
//	{
//		AfxMessageBox( "Cannot write TIM Descriptor file: No Images in ImageList!" );
//		return false;
//	}

    ofstream ofs;

    ofs.open( m_sTimDescriptorFilePath.c_str() );
    if ( ofs.good() )
    {	
        ofs << TimDescriptorText();
        NotWritten(false);
    }
    else
    {
        CString sMsg( "Error: Cannot open file name <" );
        sMsg += m_sTimDescriptorFilePath.c_str();
        sMsg += ">";
        AfxMessageBox( sMsg );
    }

    ofs.flush();
    ofs.close();

    return ofs.good();
}

#endif
