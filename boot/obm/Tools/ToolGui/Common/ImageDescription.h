/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#pragma once

#include "TimLib.h"

#include <string>
#include <list>
#include <iterator>
#include <fstream>
#include <iostream>
#include <strstream>
#include <sstream>
using namespace std;

const int MAX_HASH = 256;
const int MAX_HASH_IN_BITS = 512;	// 512 bits

class CImageDescription : public CTimLib
{
public:
	CImageDescription();
	virtual ~CImageDescription(void);

	// copy constructor
	CImageDescription( const CImageDescription& rhs );
	// assignment operator
	CImageDescription& operator=( const CImageDescription& rhs );

	string& ImageId() { return m_sImageId; }
	void ImageId( string& sImageId );

	string& ImageIdTag() { return m_sImageIdTag; }
	void ImageIdTag( string& sTag );

	string& NextImageId() { return m_sNextImageId; }
	void NextImageId( string& sImageId );

	string& NextImageIdTag() { return m_sNextImageIdTag; }
	void NextImageIdTag( string& sImageIdTag );

	void FlashEntryAddress( string& sAddress ) { m_sFlashEntryAddress = sAddress; m_bChanged = true; }
	string& FlashEntryAddress() { return m_sFlashEntryAddress; }

	void LoadAddress( string& sAddress ) { m_sLoadAddress = sAddress; m_bChanged = true; }
	string& LoadAddress() { return m_sLoadAddress; }

	void ImageSizeToCrc( string& sImageSizeToCrc ) { m_sImageSizeToCrc = sImageSizeToCrc; m_bChanged = true; }
	string& ImageSizeToCrc() { return m_sImageSizeToCrc; }

	void ImageFilePath( string& sImageFilePath ) { m_sImageFilePath = sImageFilePath; m_bChanged = true; }
	string& ImageFilePath() { return m_sImageFilePath; }

	void HashAlgorithmId( string& sHashAlgorithmId ) { m_sHashAlgorithmId = sHashAlgorithmId; m_bChanged = true; }
	string& HashAlgorithmId() { return m_sHashAlgorithmId; }

	void PartitionNumber( unsigned int uiPartitionNumber ) { m_uiPartitionNumber = uiPartitionNumber; m_bChanged = true; }
	unsigned int& PartitionNumber() { return m_uiPartitionNumber; }

	void ImageSizeToHash( unsigned int uiImageSizeToHash ) { m_uiImageSizeToHash = uiImageSizeToHash; m_bChanged = true; }
	unsigned int& ImageSizeToHash() { return m_uiImageSizeToHash; }

	void ImageSize( unsigned int uiImageSize ) { m_uiImageSize = uiImageSize; m_bChanged = true; }
	unsigned int& ImageSize() { return m_uiImageSize; }

	void Reset();

#if TOOLS_GUI == 1
	bool SaveState( stringstream& ss );
	bool LoadState( ifstream& ifs );
	void RemoveRefs();
	void AddRefs( CImageDescription& nc_rhs );
#endif

	bool IsChanged(){ return m_bChanged; }
	void Changed( bool bSet ){ m_bChanged = bSet; }

	unsigned int Hash[MAX_HASH_IN_BITS/32];		// Reserve 512 bits for the largest hash

private:
	string	m_sImageId;
	string	m_sImageIdTag;
	string	m_sNextImageId;
	string	m_sNextImageIdTag;
	string	m_sFlashEntryAddress;
	string	m_sLoadAddress;
	string	m_sImageSizeToCrc;
	string	m_sImageFilePath; // no path, assume sTimDescriptorFilePath
	string	m_sHashAlgorithmId;

	unsigned int m_uiImageSize;
	unsigned int m_uiImageSizeToHash;
	unsigned int m_uiPartitionNumber;
	
	bool m_bChanged;
};

typedef list<CImageDescription*>           t_ImagesList;
typedef list<CImageDescription*>::iterator t_ImagesIter;
