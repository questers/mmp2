/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#pragma once
#include "ErdBase.h"

class CConsumerID : public CErdBase
{
public:
	CConsumerID();
	virtual ~CConsumerID(void);

	// copy constructor
	CConsumerID( const CConsumerID& rhs );
	// assignment operator
	virtual CConsumerID& operator=( const CConsumerID& rhs );

	void Reset();

	static const string Begin;	// "Consumer ID"
	static const string End;	// "End Consumer ID"

	virtual bool Parse( CTimDescriptor& TimDescriptor, CTimDescriptorLine*& pLine );

	virtual bool ToBinary( ofstream& ofs );
	virtual const string& PackageName(){ return Begin; }
	virtual unsigned int PackageSize();
	virtual int AddPkgStrings( CReservedPackageData* pRPD );

#if TOOLS_GUI == 1
	virtual bool LoadState( ifstream& ifs );
	virtual bool SaveState( stringstream& ss );
	virtual bool ToText( CTimDescriptor& TimDescriptor, stringstream& ss );
#endif

	void psCID( string& sCID ) { *m_FieldValues[CID] = sCID; }
	string* psCID() { return m_FieldValues[CID]; }
	enum eConsumerID { CID, PID, CONSUMERID_MAX };

	t_stringVector	m_PIDs;
};

typedef vector<CConsumerID*>           t_ConsumerIDVec;
typedef vector<CConsumerID*>::iterator t_ConsumerIDVecIter;

