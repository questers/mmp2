/*++

Copyright (c) 1998-1999 Intel Corporation

Module Name:

    $Workfile:: WTPDFU.c                         $

Abstract:

    Handles all main entry points of driver as described in
    DriverEntry()

Environment:

    Kernel mode only

Notes:

    Copyright (c) 1998-1999 Intel Corporation.  All Rights Reserved.

    The receipt of or possession of this program does not convey
    any rights to reproduce its contents or to manufacture,
    use, or sell anything that it may describe, in whole, or 
    in part, without specific written consent of Intel Corporation.


Revision History:
    $Date:: 8/24/00 11:42a                $ Date and time of last check in
    $Revision:: 10                        $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/
// Copyright (c) Marvell International Ltd. All Rights Reserved.

// use this manifest constant to deactivate bluetooth specific areas.
// once this driver has been debugged, all of the deactivated areas
// can be permanently deleted.



#include <wdm.h>
#include <wmistr.h>
#include <usbdi.h>
#include <usbdlib.h>
#include "wtpdbg.h"
#include "wtpdfu.h"
#include "wtpguid.h"
#include "rtlreg.h"

// RegistryName used to store Registry Name across instances and used by WMI & dbg libraries
UNICODE_STRING registryName;

#ifdef ALLOC_PRAGMA
#pragma alloc_text (INIT, DriverEntry)
#pragma alloc_text (PAGE, WTPUSB_Create)
#pragma alloc_text (PAGE, WTPUSB_Close)
#pragma alloc_text (PAGE, WTPUSB_Unload)
#pragma alloc_text (PAGE, WTPUSB_AddDevice)
//#pragma alloc_text (PAGE, WTPUSB_ProcessPowerIrp)
//#pragma alloc_text (PAGE, WTPUSB_ProcessFDOPowerIrp)
//#pragma alloc_text (PAGE, WTPUSB_ProcessPDOPowerIrp)
#pragma alloc_text (PAGE, WTPUSB_ProcessPnPIrp)
#pragma alloc_text (PAGE, WTPUSB_ProcessFDOPnPIrp)
#pragma alloc_text (PAGE, WTPUSB_ProcessPDOPnPIrp)
#pragma alloc_text (PAGE, WTPUSB_ProcessSystemControl)
// #pragma alloc_text (PAGE, WTPUSB_ProcessIOCTL) // RH - could not be pageable
#pragma alloc_text (PAGE, WTPUSB_StopDevice)
#pragma alloc_text (PAGE, WTPUSB_StartDevice)
#pragma alloc_text (PAGE, WTPUSB_RemoveDevice)
#pragma alloc_text (PAGE, WTPUSB_LoadUpperDriver)
#pragma alloc_text (PAGE, WTPUSB_UnLoadUpperDriver)
#pragma alloc_text (PAGE, WTPUSB_GetAndSetCapabilitiesStructure)
#pragma alloc_text (PAGE, WTPUSB_SendDFUDetachMessage)
#endif // ALLOC_PRAGMA

unsigned long gDebugFeature = 0;

#if DBG
unsigned long	gDebugDelay;

void
WTPDFU_CustomDbgFunc(
	void
	)
{
	// this custom debug func will delay for the number of milliseconds specified in gDebugDelay

	// used for pacing messages to the firmware while module is writing flash
	KTIMER						ktPollTimeoutTimer;
	LARGE_INTEGER				liDueTime;



	if( gDebugDelay )
	{
		// can't use any kernel object until it is initialized....
		KeInitializeTimer( &ktPollTimeoutTimer );

		// use a timer object for the pacing
		liDueTime.QuadPart = gDebugDelay;
		liDueTime.QuadPart *= 10000;	// convert gDebugDelay millisecs to (100 nano) secs
		liDueTime.QuadPart *= -1;		// this converts the timer value to a relative time.

		KeSetTimer( &ktPollTimeoutTimer, liDueTime, NULL );

		KeWaitForSingleObject(
			&ktPollTimeoutTimer,
			Executive,
			KernelMode,
			TRUE,
			NULL
			);

	}

}
#endif

NTSTATUS
DriverEntry(
    IN PDRIVER_OBJECT DriverObject,
    IN PUNICODE_STRING RegistryPath
    )
/*++

Routine Description:

    Installable driver initialization entry point.
    This entry point is called directly by the I/O system.

Arguments:

    DriverObject - pointer to the driver object

    RegistryPath - pointer to a unicode string representing the path
                   to driver-specific key in the registry

Return Value:

    STATUS_SUCCESS 
    STATUS_INSUFFICIENT_RESOURCES

--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    DBGHDR_ASSERT(NULL != RegistryPath);


	DbgPrint( "WTPDFU: DriverEntry: built on %s at %s. enter drvro: %08x reg: %08x.\r\n", __DATE__, __TIME__, DriverObject, RegistryPath );

	registryName.Buffer = ExAllocatePoolWithTag(NonPagedPool, RegistryPath->MaximumLength * sizeof(WCHAR), WTP_POOL_TAG);
    if (NULL == registryName.Buffer)
        ntStatus = STATUS_INSUFFICIENT_RESOURCES;
    else
    {
        registryName.MaximumLength = RegistryPath->MaximumLength;
        registryName.Length = RegistryPath->Length;
        RtlZeroMemory(registryName.Buffer, registryName.MaximumLength);
        RtlMoveMemory(registryName.Buffer, RegistryPath->Buffer, RegistryPath->Length);
        
        // Unhandled dispatch routines:
        DriverObject->MajorFunction[IRP_MJ_CREATE_NAMED_PIPE] = 
            DriverObject->MajorFunction[IRP_MJ_READ] =
            DriverObject->MajorFunction[IRP_MJ_WRITE] = 
            DriverObject->MajorFunction[IRP_MJ_QUERY_INFORMATION] = 
            DriverObject->MajorFunction[IRP_MJ_SET_INFORMATION] = 
            DriverObject->MajorFunction[IRP_MJ_QUERY_EA] = 
            DriverObject->MajorFunction[IRP_MJ_SET_EA] = 
            DriverObject->MajorFunction[IRP_MJ_FLUSH_BUFFERS] = 
            DriverObject->MajorFunction[IRP_MJ_QUERY_VOLUME_INFORMATION] = 
            DriverObject->MajorFunction[IRP_MJ_SET_VOLUME_INFORMATION] = 
            DriverObject->MajorFunction[IRP_MJ_DIRECTORY_CONTROL] = 
            DriverObject->MajorFunction[IRP_MJ_FILE_SYSTEM_CONTROL] = 
            DriverObject->MajorFunction[IRP_MJ_INTERNAL_DEVICE_CONTROL] = 
            DriverObject->MajorFunction[IRP_MJ_SHUTDOWN] = 
            DriverObject->MajorFunction[IRP_MJ_LOCK_CONTROL] = 
            DriverObject->MajorFunction[IRP_MJ_CREATE_MAILSLOT] = 
            DriverObject->MajorFunction[IRP_MJ_QUERY_SECURITY] = 
            DriverObject->MajorFunction[IRP_MJ_SET_SECURITY] = 
            DriverObject->MajorFunction[IRP_MJ_DEVICE_CHANGE] = 
            DriverObject->MajorFunction[IRP_MJ_QUERY_QUOTA] = 
            DriverObject->MajorFunction[IRP_MJ_SET_QUOTA] = 
            DriverObject->MajorFunction[IRP_MJ_SYSTEM_CONTROL] = 
            WTPUSB_DefaultDispatch;
        
        // Handled dispatch routines
        DriverObject->MajorFunction[IRP_MJ_CREATE] = WTPUSB_Create;
        DriverObject->MajorFunction[IRP_MJ_CLOSE] = WTPUSB_Close;
        DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = WTPUSB_ProcessIOCTL;
        //DriverObject->MajorFunction[IRP_MJ_SYSTEM_CONTROL] = WTPUSB_ProcessSystemControl;
        DriverObject->MajorFunction[IRP_MJ_POWER] = WTPUSB_ProcessPowerIrp;
        DriverObject->MajorFunction[IRP_MJ_PNP] = WTPUSB_ProcessPnPIrp;
        DriverObject->MajorFunction[IRP_MJ_CLEANUP] = WTPUSB_Cleanup;
        DriverObject->DriverExtension->AddDevice = WTPUSB_AddDevice;
        DriverObject->DriverUnload = WTPUSB_Unload;
    } // end of check that registryName was properly allocated

    DBGHDR_Init(RegistryPath);
    DBGHDR_KdPrint( DBGMASK_TRACE,("DriverEntry() Exit with %x\n", ntStatus));

    return ntStatus;
}

NTSTATUS
WTPUSB_AddDevice(
    IN PDRIVER_OBJECT DriverObject,
    IN PDEVICE_OBJECT BusPhysicalDeviceObject
)
/*++

Routine Description:

    This routine is called to create and initialize our Functional Device Object (FDO).
    This code is pageable.

Arguments:

    DriverObject - pointer to the driver object 

    PhysicalDeviceObject - pointer to a device object created by the bus

Return Value:

    An NTSTATUS code

--*/
{
    NTSTATUS                ntStatus = STATUS_SUCCESS;
    PDEVICE_OBJECT          deviceObject = NULL;
    PFDO_DEVICE_EXTENSION   FDODeviceExtension = NULL;
    NTSTATUS                ntRegLookup = STATUS_SUCCESS;
	BOOLEAN					bWin2000;

	DBGHDR_KdPrint (DBGMASK_TRACE | DBGMASK_INFO, ("WTPUSB_AddDevice(): Enter drvro: %08x bpdo: %08x.\n",DriverObject,BusPhysicalDeviceObject));
    PAGED_CODE ();

    //
    // Call IoCreateDevice and register the device interface
    //
    ntStatus = IoCreateDevice (
                    DriverObject, 
                    sizeof (FDO_DEVICE_EXTENSION), 
                    NULL, 
                    FILE_DEVICE_UNKNOWN,
                    0,
                    FALSE, 
                    &deviceObject); 

    if (NT_SUCCESS(ntStatus)) 
    {
		DBGHDR_KdPrint (DBGMASK_TRACE | DBGMASK_INFO, ("WTPUSB_AddDevice(): new devo: %08x, %d.\n",deviceObject, sizeof(FDO_DEVICE_EXTENSION)));
        FDODeviceExtension = (PFDO_DEVICE_EXTENSION) (deviceObject->DeviceExtension);


		// The regular WTPDFU device....
        ntStatus = IoRegisterDeviceInterface(
            BusPhysicalDeviceObject,
            (LPGUID)&GUID_WTPDFU,
            NULL,
            &FDODeviceExtension->interfaceNameWTPDFU);
        
        if (NT_SUCCESS(ntStatus)) 
        {
            DBGHDR_KdPrint( DBGMASK_TRACE,
                ("WTPUSB_AddDevice(): IoRegisterDeviceInterface(WTPDFU) SUCCESS creating symbolic name\n   %ws\n",
                FDODeviceExtension->interfaceNameWTPDFU.Buffer));
        }
        else 
        {
            IoDeleteDevice (deviceObject);
            DBGHDR_KdPrint(DBGMASK_ERROR, ("WTPUSB_AddDevice(WTPDFU) FAILED on IoRegisterDeviceInterface() with %x\n", ntStatus));
        }

        
        //
        //  Initialize our device extension for our function device object.
        // 
        if (NT_SUCCESS(ntStatus)) 
        {
			// initialize the irpSyncEvent that is used to serialize the Irps sent to the lower 
			// layer driver. Every call to IoCallDriver() is wrapped between KeWaitForSingleObject()
			// and KeSetEvent(). This is done because the USDB is reporting errors when concurrent
			// URBs are sent to the lower layer driver. The exact reason for this failure is not 
			// known, since the USBD is supposed to handle concurrent URBs. However, this fix 
			// is used since it provides a much reliable performance from the driver. 
			// WATCH: Since, we are not sending concurrent URBs to the lower layer driver, this 
			// may impact performance a little bit.
			// bpc FIXME: try disabling this feature...
			// bpc: moved these two lines into this condition. was before condition. no need to create event if driver is failing.
			KeInitializeEvent(&(FDODeviceExtension->irpSyncEvent), SynchronizationEvent, FALSE); 
			KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 

            ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->self = deviceObject;
            ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->isFDO = TRUE;
            FDODeviceExtension->DeviceRemoved = FALSE;
            FDODeviceExtension->lowerPhysicalDeviceObject = BusPhysicalDeviceObject;
            FDODeviceExtension->pdo = NULL;
            FDODeviceExtension->DeviceStarted = FALSE;
            FDODeviceExtension->RemoveDeviceRequested = FALSE;
            FDODeviceExtension->StopDeviceRequested = FALSE;
            FDODeviceExtension->DFURequested = FALSE;
            FDODeviceExtension->PendingIoCount = 0;
            FDODeviceExtension->registryPath = &registryName;
            
			ntRegLookup = DBGHDR_IsWindows2000( &bWin2000 );
			if( NT_SUCCESS(ntRegLookup) && bWin2000==TRUE )
			{
				// the registry path passed to DriverEntry is OK
				FDODeviceExtension->registryServicePath = *FDODeviceExtension->registryPath;
				FDODeviceExtension->bRegistryServicePathAllocated = FALSE;
			}
			else
			{
				// can't use the registry path passed to driver entry - 
				// need to use a hardcoded one.
				FDODeviceExtension->registryServicePath.Buffer = ExAllocatePoolWithTag(NonPagedPool, sizeof(POSSIBLE_SERVICE_REGISTRY_PATH), WTP_POOL_TAG);
				if (NULL == FDODeviceExtension->registryServicePath.Buffer)
					ntStatus = STATUS_INSUFFICIENT_RESOURCES;
				else
				{
					FDODeviceExtension->bRegistryServicePathAllocated = TRUE;
					FDODeviceExtension->registryServicePath.MaximumLength = sizeof(POSSIBLE_SERVICE_REGISTRY_PATH);
					FDODeviceExtension->registryServicePath.Length = sizeof(POSSIBLE_SERVICE_REGISTRY_PATH);
					RtlMoveMemory(FDODeviceExtension->registryServicePath.Buffer, POSSIBLE_SERVICE_REGISTRY_PATH, sizeof(POSSIBLE_SERVICE_REGISTRY_PATH));
				}
			}

            // this event is triggered when there is no pending io of any kind and device is removed
            KeInitializeEvent(&FDODeviceExtension->RemoveEvent, NotificationEvent, FALSE);
            KeInitializeEvent(&FDODeviceExtension->NoPendingIoEvent, NotificationEvent, FALSE);
            KeInitializeEvent(&FDODeviceExtension->SelfRequestedPowerIrpEvent, NotificationEvent, FALSE);


            // Initialize our debug stuff
#if DBG
            RtlZeroMemory( (((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->dbgTrace), DBG_QUEUE_LEN);
            ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->dbgTraceIndex = 0;
            KeInitializeSpinLock(&((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->dbgTraceLock);
#endif

            
            // Initialize our WMI performance stuff
            RtlZeroMemory(&FDODeviceExtension->perfInfo, sizeof(WTPUSB_PERF_WMI_INFO));
			FDODeviceExtension->perfInfo.ulBlockNum		= 0;
			FDODeviceExtension->perfInfo.ulBytesSent	= 0;
			FDODeviceExtension->perfInfo.ulDeviceState	= 0;
			FDODeviceExtension->perfInfo.ulDeviceStatus	= 0;
			FDODeviceExtension->perfInfo.ulErrors		= 0;
			FDODeviceExtension->perfInfo.ulFWImageSize	= 0;
			FDODeviceExtension->perfInfo.ulPacketsSent	= 0;

			// Initialize our WMI configuration stuff
			ntRegLookup = RegistryGetValueDword( 
							&FDODeviceExtension->registryServicePath, RTLREG_PARAM_UNI,	// root path
							L"Parameters",												// subkey
							L"SendUpdates",												// value name
							&FDODeviceExtension->configInfo.ulSendUpdates
							);
			if( ntRegLookup != STATUS_SUCCESS )	// not found: use default
				FDODeviceExtension->configInfo.ulSendUpdates = FALSE;
			
			// Initialize our WMI GUIDs
            WTPUSB_InitializeGuidList(FDODeviceExtension);
           
            // Initialize our USB stuff
            FDODeviceExtension->UsbConfigurationDescriptor = NULL;
            FDODeviceExtension->UsbDeviceDescriptor = NULL;
            FDODeviceExtension->UsbInterface = NULL;
            FDODeviceExtension->UsbIsochInterface = NULL;
            FDODeviceExtension->UsbDFUInterface = NULL;
            FDODeviceExtension->UsbDFUDescriptor = NULL;
            RtlZeroMemory(&(FDODeviceExtension->CMDPipe), sizeof(USBD_PIPE_INFORMATION));
            FDODeviceExtension->CMDPipe.PipeType = UsbdPipeTypeControl;
            FDODeviceExtension->bIsoOutStreamStarted = FALSE;
            FDODeviceExtension->bIsoInStreamStarted = FALSE;
            FDODeviceExtension->frameNumberIsoIn = 0;
            FDODeviceExtension->frameNumberIsoOut = 0;

            // Initialize the variables used to stop ISO transfers temporarily (to select a different alternate setting)
            KeInitializeEvent(&FDODeviceExtension->IsoInTrafficStoppedEvent, NotificationEvent, FALSE);
            KeInitializeEvent(&FDODeviceExtension->IsoOutTrafficStoppedEvent, NotificationEvent, FALSE);
            KeInitializeEvent(&FDODeviceExtension->IsoInStreamStartCompleteEvent, NotificationEvent, FALSE);
            KeInitializeEvent(&FDODeviceExtension->IsoOutStreamStartCompleteEvent, NotificationEvent, FALSE);
            
            // Load our upper driver
            // ntStatus = WTPUSB_LoadUpperDriver(deviceObject);
            DBGHDR_ASSERT (NT_SUCCESS(ntStatus));
            if (NT_SUCCESS(ntStatus))
            {
                deviceObject->Flags &= ~DO_DEVICE_INITIALIZING;
                
                //
                // we support direct io for read/write
                //
                deviceObject->Flags |= DO_DIRECT_IO;
                
				
				// required so no page faults occur going into suspend...
				deviceObject->Flags |= DO_POWER_PAGABLE;
                //
                // Attach to the PDO
                //
                FDODeviceExtension->topOfStackDeviceObject = IoAttachDeviceToDeviceStack(deviceObject, BusPhysicalDeviceObject);
				DBGHDR_KdPrint (DBGMASK_TRACE | DBGMASK_INFO, ("WTPUSB_AddDevice(): top of stk devo: %08x for devo: %08x bpdo: %08x.\n",FDODeviceExtension->topOfStackDeviceObject,deviceObject, BusPhysicalDeviceObject));
                
                // Initialize the basic power stuff
                FDODeviceExtension->EnabledForWakeup = FALSE;
                FDODeviceExtension->powerIrp = NULL;
                FDODeviceExtension->SelfPowerIrp = FALSE;
                FDODeviceExtension->powerQueryLock = FALSE;
                FDODeviceExtension->PowerDownLevel = PowerDeviceD1;
                FDODeviceExtension->CurrentSystemState = PowerSystemWorking;
                FDODeviceExtension->CurrentDeviceState = PowerDeviceD3;
                
                // Get a copy of the physical device's capabilities into a
                // DEVICE_CAPABILITIES struct in our device extension;
                // We are most interested in learning which system power states
                // are to be mapped to which device power states for handling
                // IRP_MJ_SET_POWER Irps.
                ntStatus = WTPUSB_GetAndSetCapabilitiesStructure(FDODeviceExtension);
                
                if (NT_SUCCESS(ntStatus))
                {
                    //
                    // Register for WMI
                    //
#if 0
// bpc: wmi's not necessary for this driver...
                    ntStatus = IoWMIRegistrationControl(deviceObject, WMIREG_ACTION_REGISTER);
                    if (!NT_SUCCESS(ntStatus))
                        DBGHDR_KdPrint(DBGMASK_ERROR, ("IoWMIRegistration failed with %x\n", ntStatus));
                    DBGHDR_ASSERT(NT_SUCCESS(ntStatus));        
#endif                    
                    if (NT_SUCCESS(ntStatus))
                    {
                        // We keep a pending IO count ( extension->PendingIoCount )  in the device extension.
                        // The first increment of this count is done on adding the device.
                        // Subsequently, the count is incremented for each new IRP received and
                        // decremented when each IRP is completed or passed on.
                        
                        // Transition to 'one' therefore indicates no IO is pending and signals
                        // FDODeviceExtension->NoPendingIoEvent. This is needed for processing
                        // IRP_MN_QUERY_REMOVE_DEVICE
                        
                        // Transition to 'zero' signals an event ( FDODeviceExtension->RemoveEvent )
                        // to enable device removal. This is used in processing for IRP_MN_REMOVE_DEVICE
                        //
                        WTPUSB_IncrementIoCount(deviceObject);
                    } // end of success from result of IoWMIRegistrationControl()
                    else
                    {
                        // unload the upper driver
                        WTPUSB_UnLoadUpperDriver(deviceObject);	// FIXME: not needed. upper driver is not loaded.
                        // delete the device
                        IoDeleteDevice(deviceObject);
                    }
                } // end of success from result of WTPUSB_GetAndSetCapabilitiesStructure()
                else
                {
                    // unload the upper driver
                    WTPUSB_UnLoadUpperDriver(deviceObject);
                    // delete the device
                    IoDeleteDevice(deviceObject); // FIXME: not needed. upper driver is not loaded.
                }
            } // end of check when loading the upper driver
            else
            {
                // delete the device
                IoDeleteDevice(deviceObject);
                DBGHDR_KdPrint(DBGMASK_ERROR, ("WTPUSB_AddDevice(): Error returned when loading upper driver\n"));
            }
        } // end of success test against ntStatus
    } // end of success creating the device object
    else
    {
        DBGHDR_KdPrint(DBGMASK_ERROR, ("AddDevice() FAILED on IoCreateDevice() with %x\n", ntStatus));
    }
    


    DBGHDR_KdPrint (DBGMASK_TRACE, ("AddDevice() exited with: %x\n", ntStatus));
    return ntStatus;
}


//
// PNP minor function codes.
//
char	*StringIrpPNPMN[]=
{
	"PNP IRP_MN_START_DEVICE",
	"PNP IRP_MN_QUERY_REMOVE_DEVICE",
	"PNP IRP_MN_REMOVE_DEVICE",
	"PNP IRP_MN_CANCEL_REMOVE_DEVICE",
	"PNP IRP_MN_STOP_DEVICE",
	"PNP IRP_MN_QUERY_STOP_DEVICE",
	"PNP IRP_MN_CANCEL_STOP_DEVICE",
	"PNP IRP_MN_QUERY_DEVICE_RELATIONS",
	"PNP IRP_MN_QUERY_INTERFACE",
	"PNP IRP_MN_QUERY_CAPABILITIES",
	"PNP IRP_MN_QUERY_RESOURCES",
	"PNP IRP_MN_QUERY_RESOURCE_REQUIREMENTS",
	"PNP IRP_MN_QUERY_DEVICE_TEXT",
	"PNP IRP_MN_FILTER_RESOURCE_REQUIREMENTS",
	"PNP RESERVED",
	"PNP IRP_MN_READ_CONFIG",
	"PNP IRP_MN_WRITE_CONFIG",
	"PNP IRP_MN_EJECT",
	"PNP IRP_MN_SET_LOCK",
	"PNP IRP_MN_QUERY_ID",
	"PNP IRP_MN_QUERY_PNP_DEVICE_STATE",
	"PNP IRP_MN_QUERY_BUS_INFORMATION",
	"PNP IRP_MN_DEVICE_USAGE_NOTIFICATION",
	"PNP IRP_MN_SURPRISE_REMOVAL",
	"PNP UNKNOWN"
};

NTSTATUS
WTPUSB_ProcessPnPIrp(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP           Irp
    )
/*++

Routine Description:

    Looks at the incoming PnP Irp and determines whether to send
    it to the FDO handler or the PDO handler.
    This code is pageable.

Arguments:

    DeviceObject    - pointer to the device object

    Irp             - pointer to an I/O Request Packet

Return Value:

    NT status code

--*/
{
    PIO_STACK_LOCATION          irpStack = NULL;
    NTSTATUS                    ntStatus = STATUS_SUCCESS;
    PCOMMON_DEVICE_EXTENSION    commonDeviceExtension = NULL;

	int							istringindex;

    PAGED_CODE ();

    DBGHDR_ASSERT(NULL != Irp);
    DBGHDR_ASSERT(NULL != DeviceObject);
    irpStack = IoGetCurrentIrpStackLocation (Irp);
    DBGHDR_ASSERT(NULL != irpStack);

	istringindex=irpStack->MinorFunction;
	if(istringindex>0x18) istringindex=0x18;
	DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessPnPIrp() Enter WTPUSB_ProcessPnPIrp devo: %08x irp: %08x mf/mf: %08x %08x = %s.\n", DeviceObject,Irp,irpStack->MajorFunction, irpStack->MinorFunction,StringIrpPNPMN[istringindex]));
    
	DBGHDR_ASSERT(IRP_MJ_PNP == irpStack->MajorFunction);

    commonDeviceExtension = (PCOMMON_DEVICE_EXTENSION) DeviceObject->DeviceExtension;
    DBGHDR_ASSERT(NULL != commonDeviceExtension);

    if (commonDeviceExtension->isFDO) 
        ntStatus = WTPUSB_ProcessFDOPnPIrp (
                    DeviceObject,
                    Irp,
                    irpStack,
                    (PFDO_DEVICE_EXTENSION)commonDeviceExtension);
    else 
        ntStatus = WTPUSB_ProcessPDOPnPIrp(
                    DeviceObject,
                    Irp,
                    irpStack,
                    (PPDO_DEVICE_EXTENSION)commonDeviceExtension);

    DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessPnPIrp() Exit with %x\n", ntStatus));
    return ntStatus;
}

VOID
WTPUSB_Unload(
    IN PDRIVER_OBJECT DriverObject
    )
/*++

Routine Description:

    Frees the resources allocated in driverentry before unloading

Arguments:

    DriverObject - pointer to a driver object

Return Value:


--*/
{
	DBGHDR_KdPrint( DBGMASK_TRACE, ("WTPUSB_Unload() Enter drvro: %08x\n", DriverObject));

    if (NULL != registryName.Buffer)
        ExFreePool(registryName.Buffer);

    DBGHDR_KdPrint( DBGMASK_TRACE | DBGMASK_INFO,("WTPUSB_Unload() Exit\n"));
}

NTSTATUS
WTPUSB_Create(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp
    )
/*++

Routine Description:

    This should never be called - except perhaps from user mode during testing.
    This routine is pageable.

Arguments:

    DeviceObject - pointer to our FDO ( Functional Device Object )

Return Value:

    NT status code

--*/
{
    NTSTATUS                        ntStatus = STATUS_SUCCESS; // RH - change this to be STATUS_ACCESS_DENIED
    PFDO_DEVICE_EXTENSION           deviceExtension = NULL;

	DBGHDR_KdPrint( DBGMASK_TRACE,("WTPUSB_Create() Entering devo: %08x irp: %08x\n", DeviceObject, Irp));
    PAGED_CODE();

    WTPUSB_IncrementIoCount(DeviceObject);
    deviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT( ((PCOMMON_DEVICE_EXTENSION)deviceExtension)->isFDO );

    Irp->IoStatus.Status = ntStatus;
    IoCompleteRequest (Irp, IO_NO_INCREMENT);
    WTPUSB_DecrementIoCount(DeviceObject);                             

    DBGHDR_KdPrint( DBGMASK_TRACE,("WTPUSB_Create() Exiting with %x\n", ntStatus));
    return ntStatus;
}


NTSTATUS
WTPUSB_Close(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp
)
/*++

Routine Description:

    This should never be called - except perhaps from user mode during testing.
    This routine is pageable.

Arguments:

    DeviceObject - pointer to our FDO (Functional Device Object )


Return Value:

    NT status code

--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS; // RH - change this to be STATUS_ACCESS_DENIED
    PFDO_DEVICE_EXTENSION deviceExtension = NULL;

	DBGHDR_KdPrint( DBGMASK_TRACE,("WTPUSB_Close() Entering devo: %08x, irp: %08x\n", DeviceObject, Irp));
    PAGED_CODE();

    WTPUSB_IncrementIoCount(DeviceObject);
    deviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT( ((PCOMMON_DEVICE_EXTENSION)deviceExtension)->isFDO );

    Irp->IoStatus.Status = ntStatus;
    IoCompleteRequest (Irp, IO_NO_INCREMENT);
    WTPUSB_DecrementIoCount(DeviceObject);
                       
    DBGHDR_KdPrint( DBGMASK_TRACE,("WTPUSB_Close() Exiting with status %x\n", ntStatus));
    return ntStatus;
}


//
// POWER minor function codes
//
char	*StringIrpPowerMinor[]=
{
	"POWER IRP_MN_WAIT_WAKE",
	"POWER IRP_MN_POWER_SEQUENCE",
	"POWER IRP_MN_SET_POWER",
	"POWER IRP_MN_QUERY_POWER",
	"POWER UNKNOWN"
};

NTSTATUS
WTPUSB_ProcessPowerIrp(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp
)
/*++

Routine Description:

    Looks at the incoming PnP Irp and determines whether to send
    it to the FDO handler or the PDO handler.
    
Arguments:

    DeviceObject - pointer to the device object

    Irp          - pointer to an I/O Request Packet

Return Value:

    An NTSTATUS code indicating success / failure
    
--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    PIO_STACK_LOCATION irpStack = NULL;
    PCOMMON_DEVICE_EXTENSION commonDeviceExtension = NULL;
	
	int	istringindex;

    DBGHDR_ASSERT(NULL != DeviceObject);
    
    irpStack = IoGetCurrentIrpStackLocation(Irp);
    DBGHDR_ASSERT(NULL != irpStack);

	istringindex=irpStack->MinorFunction;
	if(istringindex>4) istringindex=4;
	DBGHDR_KdPrint(DBGMASK_TRACE, ("Enter WTPUSB_ProcessPowerIrp devo: %08x irp: %08x mf/mf: %08x %08x = %s.\n", DeviceObject,Irp,irpStack->MajorFunction, irpStack->MinorFunction,StringIrpPowerMinor[istringindex]));

	DBGHDR_ASSERT(IRP_MJ_POWER == irpStack->MajorFunction);

    commonDeviceExtension = (PCOMMON_DEVICE_EXTENSION) DeviceObject->DeviceExtension;

    if (commonDeviceExtension->isFDO) 
        ntStatus = WTPUSB_ProcessFDOPowerIrp (
                    DeviceObject,
                    Irp,
                    irpStack,
                    (PFDO_DEVICE_EXTENSION)commonDeviceExtension);
    else 
        ntStatus = WTPUSB_ProcessPDOPowerIrp(
                    DeviceObject,
                    Irp,
                    irpStack,
                    (PPDO_DEVICE_EXTENSION)commonDeviceExtension);

    DBGHDR_KdPrint( DBGMASK_TRACE,("WTPUSB_ProcessPowerIrp() Exit with %x\n", ntStatus));

    return ntStatus;
}






NTSTATUS
WTPUSB_ProcessIOCTL(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp
)
/*++

Routine Description:
    
    IOCTL handler - uppper layer requests come through this handler.
    This code is pageable.

Arguments:

    DeviceObject - pointer to the device object

    Irp - pointer to the IRP_MJ_IOCTL

Return Value:

    STATUS_SUCCESS, STATUS_PENDING, or STATUS_NOT_SUPPORTED

--*/
{
    NTSTATUS					ntStatus = STATUS_SUCCESS;
    PIO_STACK_LOCATION			irpStack = NULL;
    //PFDO_DEVICE_EXTENSION		FDODeviceExtension = NULL;
    KIRQL						oldIrql;
    BOOLEAN						bCompleteIrp = TRUE;
    BOOLEAN						bQueueIrp = FALSE;
    PWTPUSB_TRANSFER_OBJECT		pTransferObject = NULL;
    PIRP						newIrp = NULL;
    PIO_STACK_LOCATION			newStack = NULL;
    PCOMMON_DEVICE_EXTENSION	commonDeviceExtension = NULL;

	//PBOOTROM_COMMAND			pbrCmd;
	PWTPCOMMAND					pbrCmd;
    
    // PAGED_CODE(); Removed from paged code - because of page faults - see pragma comment

    DBGHDR_ASSERT(NULL != DeviceObject);
    DBGHDR_ASSERT(NULL != Irp);
    irpStack = IoGetCurrentIrpStackLocation (Irp);
    DBGHDR_ASSERT(NULL != irpStack);
	DBGHDR_KdPrint(DBGMASK_TRACE, ("Enter WTPUSB_ProcessIOCTL devo: %08x irp: %08x mf/mf: %08x %08x ioctl: %08x\n", DeviceObject,Irp,irpStack->MajorFunction, irpStack->MinorFunction,irpStack->Parameters.DeviceIoControl.IoControlCode));
    DBGHDR_ASSERT (IRP_MJ_DEVICE_CONTROL == irpStack->MajorFunction);

    commonDeviceExtension = (PCOMMON_DEVICE_EXTENSION) DeviceObject->DeviceExtension;
    DBGHDR_ASSERT(NULL != commonDeviceExtension);

    if (!commonDeviceExtension->isFDO)
    {

		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL: sending Irp down\n"));

        IoSkipCurrentIrpStackLocation (Irp);
        KeWaitForSingleObject (&(((PPDO_DEVICE_EXTENSION)commonDeviceExtension)->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
        ntStatus = IoCallDriver( 
              ((PPDO_DEVICE_EXTENSION)commonDeviceExtension)->parentFdo,
              Irp );
        KeSetEvent(&(((PPDO_DEVICE_EXTENSION)commonDeviceExtension)->irpSyncEvent), 0, FALSE); 
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL: back from sending Irp down. exit %08x\n", ntStatus));
        return ntStatus; 
    }

    // Need to increment the IoCount only when we are sure we are in our FDO
    WTPUSB_IncrementIoCount(DeviceObject);
  
    //FDODeviceExtension = (PFDO_DEVICE_EXTENSION) DeviceObject->DeviceExtension;
    //DBGHDR_ASSERT(NULL != FDODeviceExtension);

    if (!NT_SUCCESS(WTPUSB_CanAcceptIoRequests(DeviceObject)))
        bQueueIrp = TRUE;

    switch (irpStack->Parameters.DeviceIoControl.IoControlCode)
    {
    case IOCTL_WTPDFU_SEND:
        DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Incoming IOCTL_WTPDFU_COMMAND\n"));
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - SystemBuffer:     %08x.\n",Irp->AssociatedIrp.SystemBuffer));
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - UserBuffer:       %08x.\n",Irp->UserBuffer));
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Status:           %08x.\n",Irp->IoStatus.Status));
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Information:      %08x.\n",Irp->IoStatus.Information));
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Information:      %08x.\n",Irp->IoStatus.Information));
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - OutputLength:     %08x.\n",irpStack->Parameters.DeviceIoControl.OutputBufferLength));		
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - InputLength:      %08x.\n",irpStack->Parameters.DeviceIoControl.InputBufferLength));		
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - IoControlCode:    %08x.\n",irpStack->Parameters.DeviceIoControl.IoControlCode));		
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Type3InputBuffer: %08x.\n",irpStack->Parameters.DeviceIoControl.Type3InputBuffer));		
            {
                PURB						pUrb;
				ULONG						siz;
                unsigned char               *pbrData;
				unsigned char				*pucData;
				PUSBD_PIPE_INFORMATION		PipeInfo;
				PUSBD_INTERFACE_INFORMATION	usbInterface;
				PFDO_DEVICE_EXTENSION		FDODeviceExtension;
				ULONG						cmdLength;
                
                                
                pbrData = (unsigned char*)Irp->AssociatedIrp.SystemBuffer;              
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - DeviceObject:       %08x\n",DeviceObject));

				FDODeviceExtension = DeviceObject->DeviceExtension;
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - FDODeviceExtension: %08x\n",FDODeviceExtension));
				
				usbInterface = FDODeviceExtension->UsbInterface;
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - usbInterface:       %08x\n",usbInterface));
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pipeNumACLOut:      %08x\n",FDODeviceExtension->pipeNumACLOut));
				
				PipeInfo = &(usbInterface->Pipes[FDODeviceExtension->pipeNumACLOut]);
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - PipeInfo:           %08x\n",PipeInfo));
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - PipeHandle:         %08x\n",PipeInfo->PipeHandle));

				cmdLength =  irpStack->Parameters.DeviceIoControl.InputBufferLength;
				
				pucData = ExAllocatePoolWithTag( NonPagedPool, cmdLength, WTP_POOL_TAG );
                if (pucData == NULL)
                {
                    DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_ProcessIOCTL() pucData = ExAllocatePoolWithTag() failed\n  returning STATUS_INSUFFICIENT_RESOURCES\n"));
                    ntStatus = STATUS_INSUFFICIENT_RESOURCES;
                    break;
                }
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pucData:            %08x for %d bytes.\n",pucData, cmdLength));
				RtlCopyMemory( pucData, pbrData, cmdLength );

				siz = sizeof( struct _URB_BULK_OR_INTERRUPT_TRANSFER );
				pUrb = ExAllocatePoolWithTag( NonPagedPool, siz, WTP_POOL_TAG );
                if (pUrb == NULL)
                {
                    DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_ProcessIOCTL() pUrb = ExAllocatePoolWithTag() failed\n  returning STATUS_INSUFFICIENT_RESOURCES\n"));
                    ExFreePool( pucData );
                    ntStatus = STATUS_INSUFFICIENT_RESOURCES;
                    break;
                }
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pUrb:               %08x\n",pUrb));

				RtlZeroMemory( pUrb, siz );

				pUrb->UrbBulkOrInterruptTransfer.Hdr.Length = (USHORT)siz;
				pUrb->UrbBulkOrInterruptTransfer.Hdr.Function = URB_FUNCTION_BULK_OR_INTERRUPT_TRANSFER;

				// FIXME: Where to get the bulk out pipe handle from?
				pUrb->UrbBulkOrInterruptTransfer.PipeHandle = PipeInfo->PipeHandle;

				pUrb->UrbBulkOrInterruptTransfer.TransferFlags = USBD_TRANSFER_DIRECTION_OUT;

				pUrb->UrbBulkOrInterruptTransfer.UrbLink = NULL;

				// FIXME: Where to get buffer from? Does it have any alignment or paging requirements?
				pUrb->UrbBulkOrInterruptTransfer.TransferBuffer = pucData;

				// FIXME: Where to get length from?
				pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength = cmdLength;

				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Sending COMMAND out via WTPUSB_CallUSBD\n"));
				
				ntStatus = WTPUSB_CallUSBD( DeviceObject, pUrb, IOCTL_INTERNAL_USB_SUBMIT_URB );
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - WTPUSB_CallUSBD returns:               st: %08x, ln: %08x\n",ntStatus, pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength));

				ExFreePool( pUrb );
				ExFreePool( pucData );
            }
                
        bCompleteIrp = TRUE; 
        break;
    case IOCTL_WTPDFU_RECEIVE:
        	// this is for receiving the response.
			{
				PURB						pUrb;
				ULONG						siz;
				unsigned char				*pucData;
				PUSBD_PIPE_INFORMATION		PipeInfo;
				PUSBD_INTERFACE_INFORMATION	usbInterface;
				PFDO_DEVICE_EXTENSION		FDODeviceExtension;
				ULONG						rspLength;
				ULONG						i;

				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - DeviceObject:       %08x\n",DeviceObject));

				FDODeviceExtension = DeviceObject->DeviceExtension;
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - FDODeviceExtension: %08x\n",FDODeviceExtension));
				
				usbInterface = FDODeviceExtension->UsbInterface;
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - usbInterface:       %08x\n",usbInterface));
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pipeNumACLOut:      %08x\n",FDODeviceExtension->pipeNumACLIn));
				
				PipeInfo = &(usbInterface->Pipes[FDODeviceExtension->pipeNumACLIn]);
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - PipeInfo:           %08x\n",PipeInfo));
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - PipeHandle:         %08x\n",PipeInfo->PipeHandle));

                rspLength = irpStack->Parameters.DeviceIoControl.OutputBufferLength;
				//rspLength = 262; // 6 + 256 (the maximum size of response packet)
				pucData = ExAllocatePoolWithTag( NonPagedPool, rspLength, WTP_POOL_TAG ); 
                if (pucData == NULL)
                {
                    DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_ProcessIOCTL() pucData = ExAllocatePoolWithTag() failed\n  returning STATUS_INSUFFICIENT_RESOURCES\n"));
                    ntStatus = STATUS_INSUFFICIENT_RESOURCES;
                    break;
                }
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pucData:            %08x for %d bytes.\n",pucData, rspLength));
				
				//pucData[0] = 1, pucData[1] = 2, pucData[2] = 3, pucData[3] = 4;
				//pucData[4] = 5, pucData[5] = 6;
				RtlZeroMemory( pucData, rspLength);

				siz = sizeof( struct _URB_BULK_OR_INTERRUPT_TRANSFER );
				pUrb = ExAllocatePoolWithTag( NonPagedPool, siz, WTP_POOL_TAG );
                if (pUrb == NULL)
                {
                    DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_ProcessIOCTL() pUrb = ExAllocatePoolWithTag() failed\n  returning STATUS_INSUFFICIENT_RESOURCES\n"));
                    ExFreePool( pucData );
                    ntStatus = STATUS_INSUFFICIENT_RESOURCES;
                    break;
                }
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pUrb:               %08x\n",pUrb));

				RtlZeroMemory( pUrb, siz );

				pUrb->UrbBulkOrInterruptTransfer.Hdr.Length = (USHORT)siz;
				pUrb->UrbBulkOrInterruptTransfer.Hdr.Function = URB_FUNCTION_BULK_OR_INTERRUPT_TRANSFER;

				// FIXME: Where to get the bulk out pipe handle from?
				pUrb->UrbBulkOrInterruptTransfer.PipeHandle = PipeInfo->PipeHandle;

				pUrb->UrbBulkOrInterruptTransfer.TransferFlags = USBD_TRANSFER_DIRECTION_IN | USBD_SHORT_TRANSFER_OK;

				pUrb->UrbBulkOrInterruptTransfer.UrbLink = NULL;

				// FIXME: Where to get buffer from? Does it have any alignment or paging requirements?
				pUrb->UrbBulkOrInterruptTransfer.TransferBuffer = pucData;

				// FIXME: Where to get length from?
				pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength = rspLength;

				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Got a single ACK back IN via WTPUSB_CallUSBD\n"));
				
				ntStatus = WTPUSB_CallUSBD( DeviceObject, pUrb, IOCTL_INTERNAL_USB_SUBMIT_URB );
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - WTPUSB_CallUSBD returns:               st: %08x, ln: %08x\n",ntStatus, pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength));
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pucData:            %02x %02x %02x %02x %02x %02x %02x %02x - %02x %02x %02x %02x %02x %02x %02x %02x.\n",pucData[0], pucData[1], pucData[2], pucData[3], pucData[4], pucData[5], pucData[6], pucData[7], pucData[8], pucData[9], pucData[10], pucData[11], pucData[12], pucData[13], pucData[14], pucData[15]));
				//DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pucData->Data: "));
				//for (i=0; i<pucData[5]; i++)
				//	DBGHDR_KdPrint(DBGMASK_TRACE, ("%02x ", pucData[6+i]));
				//DBGHDR_KdPrint(DBGMASK_TRACE, ("\n"));
                
                /* Commenting out in test version
                DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - ntStatus: %08x, CMD: %02x\n", ntStatus, pbrCmd->CMD));
                */
                
                // if CallUSBD() returns an error or if the target and host loose sync in the protocol, we don't 
                // want to copy anything to the system buffer since we don't know the actual size of the response.
				// bpc: FIXME: remove the comparison of the cmd w/ response. that type of checking shouldn't be done at the driver level.
				
                /****Removed if received command is actually sent command check********************************/
                
                if ((ntStatus != STATUS_SUCCESS) )
                {
                    /*if (pbrCmd->CMD != 0x31)
                    {
                        DBGHDR_KdPrint(DBGMASK_TRACE, ("We have an error getting back response from the target, send a bogus NACK.\n"));
                        pucData[0] = pbrCmd->CMD, pucData[1] = 0, pucData[2] = 0, pucData[3] = 1, pucData[4] = 0, pucData[5] = 0;
                    }
                    else
                    {
                        // we don't want to send a NACK in case of disconnect response
                        DBGHDR_KdPrint(DBGMASK_TRACE, ("We have an error getting back response from the target, send a bogus disconnect response.\n"));
                        pucData[0] = pbrCmd->CMD, pucData[1] = 0, pucData[2] = 0, pucData[3] = 0, pucData[4] = 0, pucData[5] = 0;
                    }*/
                    pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength = 0;
                }

                // these next two steps: RtlCopyMemory plus setting Irp->IoStatus.Information, get the info back to the user
				DBGHDR_KdPrint(DBGMASK_TRACE, ("copying data to system buffer.\n"));

				// bpc fixme: is pbrCmd really pointing to the user buffer?
				//            seems like it. it is the SystemBuffer. For this type of IOCTL,
				//			  the output and input data use the same buffer.
				RtlCopyMemory( Irp->AssociatedIrp.SystemBuffer, pucData, pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength );
				{
					/*unsigned char *lucData = (unsigned char*)pbrCmd;
					DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - usrData:            %02x, %02x, %02x, %02x %02x, %02x, %02x, %02x \n",lucData[0], lucData[1], lucData[2], lucData[3], lucData[4], lucData[5], lucData[6], lucData[7]));
                    */
				}

				Irp->IoStatus.Information = pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength;
               
				ExFreePool( pUrb );
				ExFreePool( pucData );
			}
            bCompleteIrp = TRUE; 
        break;
	case IOCTL_WTPDFU_IMAGEDATAXMIT:
	case IOCTL_WTPDFU_COMMAND:
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Incoming IOCTL_WTPDFU_COMMAND\n"));
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - SystemBuffer:     %08x.\n",Irp->AssociatedIrp.SystemBuffer));
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - UserBuffer:       %08x.\n",Irp->UserBuffer));
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Status:           %08x.\n",Irp->IoStatus.Status));
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Information:      %08x.\n",Irp->IoStatus.Information));
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Information:      %08x.\n",Irp->IoStatus.Information));
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - OutputLength:     %08x.\n",irpStack->Parameters.DeviceIoControl.OutputBufferLength));		
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - InputLength:      %08x.\n",irpStack->Parameters.DeviceIoControl.InputBufferLength));		
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - IoControlCode:    %08x.\n",irpStack->Parameters.DeviceIoControl.IoControlCode));		
		DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Type3InputBuffer: %08x.\n",irpStack->Parameters.DeviceIoControl.Type3InputBuffer));		

		//pbrCmd = (PBOOTROM_COMMAND)Irp->AssociatedIrp.SystemBuffer;
		pbrCmd = (PWTPCOMMAND)Irp->AssociatedIrp.SystemBuffer;

		if( pbrCmd )
		{
			DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pbrCmd->CMD:   %02x.\n",pbrCmd->CMD));
			DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pbrCmd->SEQ:   %02x.\n",pbrCmd->SEQ));
			DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pbrCmd->CID:   %02x.\n",pbrCmd->CID));		
			DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pbrCmd->Flags: %02x.\n",pbrCmd->Flags));
			DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pbrCmd->LEN:   %08x.\n",pbrCmd->LEN));
			DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pbrCmd->Data:  %02x %02x %02x %02x %02x %02x %02x %02x.\n",pbrCmd->Data[0], pbrCmd->Data[1], pbrCmd->Data[2], pbrCmd->Data[3], pbrCmd->Data[4], pbrCmd->Data[5], pbrCmd->Data[6], pbrCmd->Data[7]));
		}

		if( pbrCmd )
		{
			{   // this is for sending the command
				PURB						pUrb;
				ULONG						siz;
				unsigned char				*pucData;
				PUSBD_PIPE_INFORMATION		PipeInfo;
				PUSBD_INTERFACE_INFORMATION	usbInterface;
				PFDO_DEVICE_EXTENSION		FDODeviceExtension;
				ULONG						cmdLength;
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - DeviceObject:       %08x\n",DeviceObject));

				FDODeviceExtension = DeviceObject->DeviceExtension;
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - FDODeviceExtension: %08x\n",FDODeviceExtension));
				
				usbInterface = FDODeviceExtension->UsbInterface;
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - usbInterface:       %08x\n",usbInterface));
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pipeNumACLOut:      %08x\n",FDODeviceExtension->pipeNumACLOut));
				
				PipeInfo = &(usbInterface->Pipes[FDODeviceExtension->pipeNumACLOut]);
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - PipeInfo:           %08x\n",PipeInfo));
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - PipeHandle:         %08x\n",PipeInfo->PipeHandle));

				if (pbrCmd->CMD == 0x00)	// preamble
					cmdLength = 4;
				else
					cmdLength = 8 + pbrCmd->LEN; // overflow?
				pucData = ExAllocatePoolWithTag( NonPagedPool, cmdLength, WTP_POOL_TAG );
                if (pucData == NULL)
                {
                    DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_ProcessIOCTL() pucData = ExAllocatePoolWithTag() failed\n  returning STATUS_INSUFFICIENT_RESOURCES\n"));
                    ntStatus = STATUS_INSUFFICIENT_RESOURCES;
                    break;
                }
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pucData:            %08x for %d bytes.\n",pucData, cmdLength));
				RtlCopyMemory( pucData, pbrCmd, cmdLength );

				siz = sizeof( struct _URB_BULK_OR_INTERRUPT_TRANSFER );
				pUrb = ExAllocatePoolWithTag( NonPagedPool, siz, WTP_POOL_TAG );
                if (pUrb == NULL)
                {
                    DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_ProcessIOCTL() pUrb = ExAllocatePoolWithTag() failed\n  returning STATUS_INSUFFICIENT_RESOURCES\n"));
                    ExFreePool( pucData );
                    ntStatus = STATUS_INSUFFICIENT_RESOURCES;
                    break;
                }
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pUrb:               %08x\n",pUrb));

				RtlZeroMemory( pUrb, siz );

				pUrb->UrbBulkOrInterruptTransfer.Hdr.Length = (USHORT)siz;
				pUrb->UrbBulkOrInterruptTransfer.Hdr.Function = URB_FUNCTION_BULK_OR_INTERRUPT_TRANSFER;

				// FIXME: Where to get the bulk out pipe handle from?
				pUrb->UrbBulkOrInterruptTransfer.PipeHandle = PipeInfo->PipeHandle;

				pUrb->UrbBulkOrInterruptTransfer.TransferFlags = USBD_TRANSFER_DIRECTION_OUT;

				pUrb->UrbBulkOrInterruptTransfer.UrbLink = NULL;

				// FIXME: Where to get buffer from? Does it have any alignment or paging requirements?
				pUrb->UrbBulkOrInterruptTransfer.TransferBuffer = pucData;

				// FIXME: Where to get length from?
				pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength = cmdLength;

				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - Sending COMMAND out via WTPUSB_CallUSBD\n"));
				
				ntStatus = WTPUSB_CallUSBD( DeviceObject, pUrb, IOCTL_INTERNAL_USB_SUBMIT_URB );
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - WTPUSB_CallUSBD returns:               st: %08x, ln: %08x\n",ntStatus, pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength));

				ExFreePool( pUrb );
				ExFreePool( pucData );
			}

			// this is for receiving the response.
			{
				PURB						pUrb;
				ULONG						siz;
				unsigned char				*pucData;
				PUSBD_PIPE_INFORMATION		PipeInfo;
				PUSBD_INTERFACE_INFORMATION	usbInterface;
				PFDO_DEVICE_EXTENSION		FDODeviceExtension;
				ULONG						rspLength;
				ULONG						i;

				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - DeviceObject:       %08x\n",DeviceObject));

				FDODeviceExtension = DeviceObject->DeviceExtension;
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - FDODeviceExtension: %08x\n",FDODeviceExtension));
				
				usbInterface = FDODeviceExtension->UsbInterface;
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - usbInterface:       %08x\n",usbInterface));
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pipeNumACLOut:      %08x\n",FDODeviceExtension->pipeNumACLIn));
				
				PipeInfo = &(usbInterface->Pipes[FDODeviceExtension->pipeNumACLIn]);
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - PipeInfo:           %08x\n",PipeInfo));
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - PipeHandle:         %08x\n",PipeInfo->PipeHandle));

				rspLength = 262; // 6 + 256 (the maximum size of response packet)
				pucData = ExAllocatePoolWithTag( NonPagedPool, rspLength, WTP_POOL_TAG ); 
                if (pucData == NULL)
                {
                    DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_ProcessIOCTL() pucData = ExAllocatePoolWithTag() failed\n  returning STATUS_INSUFFICIENT_RESOURCES\n"));
                    ntStatus = STATUS_INSUFFICIENT_RESOURCES;
                    break;
                }
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pucData:            %08x for %d bytes.\n",pucData, rspLength));
				
				//pucData[0] = 1, pucData[1] = 2, pucData[2] = 3, pucData[3] = 4;
				//pucData[4] = 5, pucData[5] = 6;
				RtlZeroMemory( pucData, rspLength);

				siz = sizeof( struct _URB_BULK_OR_INTERRUPT_TRANSFER );
				pUrb = ExAllocatePoolWithTag( NonPagedPool, siz, WTP_POOL_TAG );
                if (pUrb == NULL)
                {
                    DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_ProcessIOCTL() pUrb = ExAllocatePoolWithTag() failed\n  returning STATUS_INSUFFICIENT_RESOURCES\n"));
                    ExFreePool( pucData );
                    ntStatus = STATUS_INSUFFICIENT_RESOURCES;
                    break;
                }
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pUrb:               %08x\n",pUrb));

				RtlZeroMemory( pUrb, siz );

				pUrb->UrbBulkOrInterruptTransfer.Hdr.Length = (USHORT)siz;
				pUrb->UrbBulkOrInterruptTransfer.Hdr.Function = URB_FUNCTION_BULK_OR_INTERRUPT_TRANSFER;

				// FIXME: Where to get the bulk out pipe handle from?
				pUrb->UrbBulkOrInterruptTransfer.PipeHandle = PipeInfo->PipeHandle;

				pUrb->UrbBulkOrInterruptTransfer.TransferFlags = USBD_TRANSFER_DIRECTION_IN | USBD_SHORT_TRANSFER_OK;

				pUrb->UrbBulkOrInterruptTransfer.UrbLink = NULL;

				// FIXME: Where to get buffer from? Does it have any alignment or paging requirements?
				pUrb->UrbBulkOrInterruptTransfer.TransferBuffer = pucData;

				// FIXME: Where to get length from?
				pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength = rspLength;

				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - getting COMMAND response IN via WTPUSB_CallUSBD\n"));
				
				ntStatus = WTPUSB_CallUSBD( DeviceObject, pUrb, IOCTL_INTERNAL_USB_SUBMIT_URB );
				
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - WTPUSB_CallUSBD returns:               st: %08x, ln: %08x\n",ntStatus, pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength));
				DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pucData:            %02x %02x %02x %02x %02x %02x %02x %02x - %02x %02x %02x %02x %02x %02x %02x %02x.\n",pucData[0], pucData[1], pucData[2], pucData[3], pucData[4], pucData[5], pucData[6], pucData[7], pucData[8], pucData[9], pucData[10], pucData[11], pucData[12], pucData[13], pucData[14], pucData[15]));
				//DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - pucData->Data: "));
				//for (i=0; i<pucData[5]; i++)
				//	DBGHDR_KdPrint(DBGMASK_TRACE, ("%02x ", pucData[6+i]));
				//DBGHDR_KdPrint(DBGMASK_TRACE, ("\n"));

                DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - ntStatus: %08x, CMD: %02x\n", ntStatus, pbrCmd->CMD));
                // if CallUSBD() returns an error or if the target and host loose sync in the protocol, we don't 
                // want to copy anything to the system buffer since we don't know the actual size of the response.
				// bpc: FIXME: remove the comparison of the cmd w/ response. that type of checking shouldn't be done at the driver level.
				if ((ntStatus != STATUS_SUCCESS) || (pbrCmd->CMD != pucData[0]))
                {
                    /*if (pbrCmd->CMD != 0x31)
                    {
                        DBGHDR_KdPrint(DBGMASK_TRACE, ("We have an error getting back response from the target, send a bogus NACK.\n"));
                        pucData[0] = pbrCmd->CMD, pucData[1] = 0, pucData[2] = 0, pucData[3] = 1, pucData[4] = 0, pucData[5] = 0;
                    }
                    else
                    {
                        // we don't want to send a NACK in case of disconnect response
                        DBGHDR_KdPrint(DBGMASK_TRACE, ("We have an error getting back response from the target, send a bogus disconnect response.\n"));
                        pucData[0] = pbrCmd->CMD, pucData[1] = 0, pucData[2] = 0, pucData[3] = 0, pucData[4] = 0, pucData[5] = 0;
                    }*/
                    pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength = 0;
                }

                // these next two steps: RtlCopyMemory plus setting Irp->IoStatus.Information, get the info back to the user
				DBGHDR_KdPrint(DBGMASK_TRACE, ("copying data to system buffer.\n"));

				// bpc fixme: is pbrCmd really pointing to the user buffer?
				//            seems like it. it is the SystemBuffer. For this type of IOCTL,
				//			  the output and input data use the same buffer.
				RtlCopyMemory( pbrCmd, pucData, pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength );
				{
					unsigned char *lucData = (unsigned char*)pbrCmd;
					DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() - usrData:            %02x, %02x, %02x, %02x %02x, %02x, %02x, %02x \n",lucData[0], lucData[1], lucData[2], lucData[3], lucData[4], lucData[5], lucData[6], lucData[7]));
				}

				Irp->IoStatus.Information = pUrb->UrbBulkOrInterruptTransfer.TransferBufferLength;
               
				ExFreePool( pUrb );
				ExFreePool( pucData );
			}
		}

		//ntStatus = WTPUSB_UpgradeFirmware( DeviceObject, Irp );
        bCompleteIrp = TRUE; 
        break;
	
	default:
        ntStatus = STATUS_NOT_SUPPORTED;
        break;
    }

    if (bCompleteIrp)
    {
        Irp->IoStatus.Status = ntStatus;
        IoCompleteRequest (Irp, IO_NO_INCREMENT);
        WTPUSB_DecrementIoCount(DeviceObject);
    }
    
    DBGHDR_KdPrint(DBGMASK_TRACE, ("WTPUSB_ProcessIOCTL() exiting with %x\n", ntStatus));
    return ntStatus;
}

//
// WMI minor function codes under IRP_MJ_SYSTEM_CONTROL
//
char	*StringIrpWMIMN[]=
{
	"WMI IRP_MN_QUERY_ALL_DATA",
	"WMI IRP_MN_QUERY_SINGLE_INSTANCE",
	"WMI IRP_MN_CHANGE_SINGLE_INSTANCE",
	"WMI IRP_MN_CHANGE_SINGLE_ITEM",
	"WMI IRP_MN_ENABLE_EVENTS",
	"WMI IRP_MN_DISABLE_EVENTS",
	"WMI IRP_MN_ENABLE_COLLECTION",
	"WMI IRP_MN_DISABLE_COLLECTION",
	"WMI IRP_MN_REGINFO",
	"WMI IRP_MN_EXECUTE_METHOD",
	"WMI RESERVED",
	"WMI IRP_MN_REGINFO_EX",
	"WMI UNKNOWN"
};

NTSTATUS
WTPUSB_ProcessSystemControl(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP           Irp
    )
/*++

Routine Description:

    Main dispatch table routine for IRP_MJ_SYSTEM_CONTROL.
    Function calls WmiSystemControl who will call into the WMI
    routines in wmi.c. These routines were setup in AddDevice.
    This routine is pageable
    
Arguments:

    DeviceObject - pointer to FDO device object

    Irp          - pointer to an I/O Request Packet

Return Value:

    
--*/
{
    PIO_STACK_LOCATION          irpStack = NULL;
    NTSTATUS                    ntStatus = STATUS_SUCCESS;
    PCOMMON_DEVICE_EXTENSION    commonDeviceExtension = NULL;
    
	int							istringindex;

    PAGED_CODE ();

    DBGHDR_ASSERT(NULL != Irp);
    DBGHDR_ASSERT(NULL != DeviceObject);
    irpStack = IoGetCurrentIrpStackLocation (Irp);
    DBGHDR_ASSERT(NULL != irpStack);
	
	istringindex= irpStack->MinorFunction;
	if(istringindex>0x0c) istringindex=0x0c;
	DBGHDR_KdPrint(DBGMASK_TRACE, ("Enter WTPUSB_ProcessSystemControl devo: %08x irp: %08x mf/mf: %08x %08x = %s.\n", DeviceObject,Irp,irpStack->MajorFunction, irpStack->MinorFunction, StringIrpWMIMN[istringindex]));
    DBGHDR_ASSERT(IRP_MJ_SYSTEM_CONTROL == irpStack->MajorFunction);

    commonDeviceExtension = (PCOMMON_DEVICE_EXTENSION) DeviceObject->DeviceExtension;
    DBGHDR_ASSERT(NULL != commonDeviceExtension);
    
    //
    // If this request is not for this device object then we need to
    // pass it down for the next device in the stack to handle.
    // RH - But then how are PDOs handled? Experiment.
    //
    if (irpStack->Parameters.WMI.ProviderId != (unsigned long)commonDeviceExtension->self)
    {
		DBGHDR_KdPrint(DBGMASK_TRACE, ("Enter WTPUSB_ProcessSystemControl: passing irp down.\n"));
        IoSkipCurrentIrpStackLocation (Irp);
        KeWaitForSingleObject (&(((PFDO_DEVICE_EXTENSION)commonDeviceExtension)->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
        ntStatus = IoCallDriver (((PFDO_DEVICE_EXTENSION)commonDeviceExtension)->topOfStackDeviceObject, Irp);
        KeSetEvent(&(((PFDO_DEVICE_EXTENSION)commonDeviceExtension)->irpSyncEvent), 0, FALSE); 
		DBGHDR_KdPrint(DBGMASK_TRACE, ("Enter WTPUSB_ProcessSystemControl: back from passing irp down %08x.\n",ntStatus));
    }
	else
	{
        ntStatus = WTPUSB_ProcessFDOWMIIrp(
                    DeviceObject,
                    Irp,
                    irpStack,
                    (PFDO_DEVICE_EXTENSION)commonDeviceExtension);
	}

    DBGHDR_KdPrint( DBGMASK_TRACE,("WTPUSB_ProcessSystemControl() Exiting with %x\n", ntStatus));
    
    return ntStatus;

}

NTSTATUS
WTPUSB_GetAndSetCapabilitiesStructure(
    IN OUT PFDO_DEVICE_EXTENSION FDODeviceExtension
)
/*++

Routine Description:

    This routine generates an internal IRP from this driver to the PDO
    to obtain information on the Physical Device Object's capabilities.
    We are most interested in learning which system power states
    are to be mapped to which device power states for honoring IRP_MJ_SET_POWER Irps.

    This is a blocking call which waits for the IRP completion routine
    to set an event on finishing.

    This code is pageable
    
Arguments:

    FDODeviceExtension - pointer to the current device extension

Return Value:

    STATUS_SUCCESS, STATUS_INSUFFICIENT_RESOURCES
    
--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    PIO_STACK_LOCATION nextStack = NULL;
    PIRP irp = NULL;
    KEVENT event;
    CHAR stackSize = 0;
    
	DBGHDR_KdPrint (DBGMASK_TRACE, ("WTPUSB_GetAndSetCapabilitiesStructure(): Enter fdoext: %08x.\n", FDODeviceExtension));
    PAGED_CODE();
    DBGHDR_ASSERT(NULL != FDODeviceExtension);

    // Build an IRP for us to generate an internal query request to the PDO
    stackSize = FDODeviceExtension->topOfStackDeviceObject->StackSize + 1;
    irp = IoAllocateIrp(stackSize, FALSE);
    DBGHDR_ASSERT (1 < stackSize);
    
    if (NULL == irp) 
    {
        ntStatus = STATUS_INSUFFICIENT_RESOURCES;
    }
    else
    {
        // IoGetNextIrpStackLocation gives a higher level driver access to the next-lower
        // driver's I/O stack location in an IRP so the caller can set it up for the lower driver.
        nextStack = IoGetNextIrpStackLocation(irp);
        DBGHDR_ASSERT(NULL != nextStack);
        nextStack->MajorFunction= IRP_MJ_PNP;
        nextStack->MinorFunction= IRP_MN_QUERY_CAPABILITIES;
        
        // init an event to tell us when the completion routine's been called
        KeInitializeEvent(&event, NotificationEvent, FALSE);
        
        // Set a completion routine so it can signal our event when
        //  the next lower driver is done with the Irp
        IoSetCompletionRoutine(irp,
            WTPUSB_OnRequestComplete,
            &event,  // pass the event as Context to completion routine
            TRUE,    // invoke on success
            TRUE,    // invoke on error
            TRUE);   // invoke on cancellation of the Irp
        
		DBGHDR_KdPrint (DBGMASK_TRACE, ("WTPUSB_GetAndSetCapabilitiesStructure(): irp: %08x ev: %08x will be passed to completion.\n", irp, &event));
        
        // set our pointer to the DEVICE_CAPABILITIES struct
        nextStack->Parameters.DeviceCapabilities.Capabilities = &(FDODeviceExtension->deviceCapabilities);
        
        KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
		DBGHDR_KdPrint (DBGMASK_TRACE, ("WTPUSB_GetAndSetCapabilitiesStructure(): sending down new irp %08x.\n", irp));
        ntStatus = IoCallDriver(FDODeviceExtension->topOfStackDeviceObject, irp);
		DBGHDR_KdPrint (DBGMASK_TRACE, ("WTPUSB_GetAndSetCapabilitiesStructure(): sent down new irp %08x.\n", ntStatus));
                
        if (STATUS_PENDING == ntStatus) 
        {
            ntStatus = KeWaitForSingleObject(
                &event,
                Executive,
                KernelMode,
                FALSE,
                NULL);

            ntStatus = irp->IoStatus.Status;
        }
        KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 
        
        IoFreeIrp(irp);
		DBGHDR_KdPrint (DBGMASK_TRACE, ("WTPUSB_GetAndSetCapabilitiesStructure(): irp: %08x freed.\n",irp));
        
        if (NT_SUCCESS(ntStatus))
        {
			DBGHDR_KdPrint (DBGMASK_TRACE, ("WTPUSB_GetAndSetCapabilitiesStructure(): initializing the power states.\n"));
            // Set the devcaps structure to be what we can support
            FDODeviceExtension->deviceCapabilities.SurpriseRemovalOK = TRUE; // If the system should popup a window when the device is removed unexpectedly
            FDODeviceExtension->deviceCapabilities.SilentInstall = TRUE; // Stop install dialog boxes from appearing
            
            // The equivalence is to the highest power state the device can maintain for each system state
            FDODeviceExtension->deviceCapabilities.DeviceState[PowerSystemWorking] = PowerDeviceD0;
            FDODeviceExtension->deviceCapabilities.DeviceState[PowerSystemSleeping1] = PowerDeviceD1; 
            FDODeviceExtension->deviceCapabilities.DeviceState[PowerSystemSleeping2] = PowerDeviceD1;
            FDODeviceExtension->deviceCapabilities.DeviceState[PowerSystemSleeping3] = PowerDeviceD1;
            FDODeviceExtension->deviceCapabilities.DeviceState[PowerSystemHibernate] = PowerDeviceD2;
            FDODeviceExtension->deviceCapabilities.DeviceState[PowerSystemShutdown] = PowerDeviceD3;

            // The device can wake up a system that is in S4            
            FDODeviceExtension->deviceCapabilities.SystemWake = PowerSystemHibernate;

            // The device can still wake up the system when it is in D2
            FDODeviceExtension->deviceCapabilities.DeviceWake = PowerDeviceD2;
        } // end of success from iocalldriver
    } // else from insufficient resources on the allocation of the irp

    DBGHDR_KdPrint( DBGMASK_TRACE,("WTPUSB_GetAndSetCapabilitiesStructure() Exit with %x\n", ntStatus));

    return ntStatus;
}



NTSTATUS
WTPUSB_Cleanup(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP           Irp
    )
/*++

Routine Description:

    Called by IRP_MJ_CLEANUP
    This routine is pageable
    Cancels all outstanding requests (with STATUS_CANCELLED) & then completes this IRP
    
Arguments:

    DeviceObject - pointer to FDO device object

    Irp          - pointer to an I/O Request Packet

Return Value:

    
--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    KIRQL oldIrql;
    PFDO_DEVICE_EXTENSION FDODeviceExtension = NULL;
    BOOLEAN isFDO = FALSE;

    PAGED_CODE();
    DBGHDR_ASSERT(NULL != DeviceObject);

    dbgT(DBG_DEVEXT(DeviceObject), 'U', oldIrql);
	DBGHDR_KdPrint ( DBGMASK_TRACE, ( "WTPUSB_Cleanup() Entering devo: %08x irp: %08x\n", DeviceObject, Irp) );

    FDODeviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT(NULL != FDODeviceExtension);
    isFDO = ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->isFDO;

    if (isFDO)
    {
        WTPUSB_IncrementIoCount(DeviceObject);

    }

    Irp->IoStatus.Status = ntStatus;
    Irp->IoStatus.Information = 0;
    IoCompleteRequest( Irp, IO_NO_INCREMENT );
    if (isFDO)
        WTPUSB_DecrementIoCount(DeviceObject);
    
    DBGHDR_KdPrint ( DBGMASK_TRACE, ( "WTPUSB_Cleanup() Exiting\n") );
    dbgT(DBG_DEVEXT(DeviceObject), 'u', oldIrql);

    return ntStatus;
}

NTSTATUS
WTPUSB_DefaultDispatch(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP           Irp
)
/*++

Routine Description:

    Called by the IO Manager for those functions that do not
    have their own dispatch routine - meaning that they are 
    currently unsupported.
    
Arguments:

    DeviceObject - pointer to device object

    Irp          - pointer to an I/O Request Packet

Return Value:

    
--*/
{
    NTSTATUS ntStatus = STATUS_NOT_SUPPORTED;
    PIO_STACK_LOCATION irpStack = NULL;

    PAGED_CODE();
    DBGHDR_ASSERT(NULL != DeviceObject);
    DBGHDR_ASSERT(NULL != Irp);

    WTPUSB_IncrementIoCount(DeviceObject);

    irpStack = IoGetCurrentIrpStackLocation(Irp);
    DBGHDR_ASSERT(NULL != irpStack);
	DBGHDR_KdPrint ( DBGMASK_TRACE, ( "WTPUSB_DefaultDispatch() Entering devo: %08x, irp: %08x, mf/mf: %08x, %08x.\n", DeviceObject, Irp, irpStack->MajorFunction, irpStack->MinorFunction) );


    DBGHDR_KdPrint( DBGMASK_INFO | DBGMASK_TRACE, 
        ("WTPUSB_DefaultDispatch() called for %s with major function code: %X\n",
        ((PCOMMON_DEVICE_EXTENSION)DeviceObject->DeviceExtension)->isFDO ? "FDO":"PDO",
        irpStack->MajorFunction));

    Irp->IoStatus.Status = ntStatus;
    Irp->IoStatus.Information = 0;
    IoCompleteRequest( Irp, IO_NO_INCREMENT );

    WTPUSB_DecrementIoCount(DeviceObject);
    
    DBGHDR_KdPrint ( DBGMASK_TRACE, ( "WTPUSB_DefaultDispatch() Exiting\n") );

    return ntStatus;
}

NTSTATUS 
WTPUSB_CompleteIrp(
    IN PDEVICE_OBJECT fdo, 
    IN PIRP Irp, 
    IN PKEVENT pev
    )
{
    DBGHDR_ASSERT(NULL != pev);

	DBGHDR_KdPrint ( DBGMASK_TRACE, ( "WTPUSB_CompleteIrp() Entering: fdo: %08x, Irp: %08x, pev: %08x\n", fdo, Irp, pev) );
   
    KeSetEvent((PKEVENT) pev, IO_NO_INCREMENT, FALSE);
    return STATUS_MORE_PROCESSING_REQUIRED;
}

