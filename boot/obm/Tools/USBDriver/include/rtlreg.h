// Copyright (c) Marvell International Ltd. All Rights Reserved.

/* 
	rtlreg.h:
	required for rtlreg.c
*/

#ifndef __RTLREG_H__

#define __RTLREG_H__

#define RTLREG_PARAM_WSTR		0
#define RTLREG_PARAM_UNI		1


NTSTATUS
RegistryGetValueDword(
	PVOID		pvPath,				// can be unicode or wstr
	ULONG		ulPathFlags,		// identifies pvPath type
	PWSTR		pwstrKey,			// can be wstr or not used (NULL)
	PWSTR		pwstrValueName,
	PULONG		pulValueData		// pts to dword buffer that will receive the data
);

NTSTATUS
RegistryGetValueString(
);

NTSTATUS
RegistryGetValueBinary(
);

NTSTATUS
RegistrySetValueDword(
	PVOID		pvPath,				// can be unicode or wstr
	ULONG		ulPathFlags,		// identifies pvPath type
	PWSTR		pwstrKey,			// can be wstr or not used (NULL)
	PWSTR		pwstrValueName,
	ULONG		ulValueData			// dword buffer that will contains the data
);

NTSTATUS
RegistrySetValueString(
);

NTSTATUS
RegistrySetValueBinary(
);


// auxilliary routines....
NTSTATUS
MakeAbsolutePath(
	PWSTR		*pPwstrAbsolutePath,	// ptr to pwstr that points to wstr that contains absolute path
	PBOOLEAN	pbFreeAbsolutePath,		// ptr to boolean that indiciates of absolute path needs to be freed
	PVOID		pvPath,					// can be unicode or wstr
	ULONG		ulPathFlags,			// identifies pvPath type
	PWSTR		pwstrKey				// can be wstr or not used (NULL)
);

#endif
