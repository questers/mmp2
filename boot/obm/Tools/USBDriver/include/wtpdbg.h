/*++

Copyright (c) 1998-1999 Intel Corporation

Module Name:

    $Workfile:: wtpdbg.h                   $

Abstract:

    A typical debug hdr file that all c files include. It must be named
    wtpdbg.h

Environment:

    Kernel mode only

Notes:


    Copyright (c) 1998-1999 Intel Corporation.  All Rights Reserved.


Revision History:
    $Date:: 7/17/00 5:28p                 $ Date and time of last check in
    $Revision:: 4                         $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/
// Copyright (c) Marvell International Ltd. All Rights Reserved.

#ifndef WTPDBGH_INC
#define WTPDBGH_INC

#define DBGMASK_USB_DATA             0x00001000
#define DBGMASK_USB_TRACE            0x00002000
#define DBGMASK_USBD_IRP_TRACE       0x00004000
#define DBGMASK_WMI_TRACE            0x00008000
#define DBGMASK_USB_ISO_TRACE        0x00010000

#define DBGSTR_PREFIX "WTPDFU: " 
#define WTP_POOL_TAG 'PTW'
#define POSSIBLE_REGISTRY_PATH L"\\REGISTRY\\Machine\\System\\CurrentControlSet\\SERVICES\\WTPDFU\\PARAMETERS"
#define POSSIBLE_SERVICE_REGISTRY_PATH L"\\REGISTRY\\Machine\\System\\CurrentControlSet\\SERVICES\\WTPDFU"

#include "dbghdr.h"

#endif
