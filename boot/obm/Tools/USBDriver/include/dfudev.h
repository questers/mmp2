/*++

Copyright (c) 1998-1999 Intel Corporation

Module Name:

    $Workfile:: DFUDEV.H                  $

Abstract:

    Header file with all device-layer related definitions.

Environment:

    Kernel mode only

Notes:

    Copyright (c) 1998-1999 Intel Corporation.  All Rights Reserved.

    The receipt of or possession of this program does not convey
    any rights to reproduce its contents or to manufacture,
    use, or sell anything that it may describe, in whole, or 
    in part, without specific written consent of Intel Corporation.


Revision History:
    $Date:: 5/12/00 2:29p                 $ Date and time of last check in
    $Revision:: 1                         $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/
// Copyright (c) Marvell International Ltd. All Rights Reserved.

#ifndef DFUDEVH_INC
    #define DFUDEVH_INC

    #define     DFU_REQUEST_TYPE_DETACH                     0x21
    #define     DFU_REQUEST_TYPE_DNLOAD                     0x21
    #define     DFU_REQUEST_TYPE_CLRSTATUS                  0x21
    #define     DFU_REQUEST_TYPE_ABORT                      0x21

    #define     DFU_REQUEST_TYPE_UPLOAD                     0xA1
    #define     DFU_REQUEST_TYPE_GETSTATUS                  0xA1
    #define     DFU_REQUEST_TYPE_GETSTATE                   0xA1

    #define     DFU_XFER_STATUS_POLL_TIME_OUT_SIZE          3

    #pragma pack(push,1)
    typedef struct  tagPAYLOAD_INFO {
        PMDL            pPayload;
        ULONG           uPayloadSize;
        USHORT          wBlockNum;
    } PAYLOAD_INFO, *PPAYLOAD_INFO;

    typedef union   tagDEVICE_STATUS {
        struct {
            UCHAR           bStatus;
            UCHAR           uPollTimeOut [DFU_XFER_STATUS_POLL_TIME_OUT_SIZE];
            UCHAR           bState;
            UCHAR           iString;
        };
        int                 bwPollTimeOut;
    } DEVICE_STATUS, *PDEVICE_STATUS;

    typedef struct   tagDFUDEVICE_STATUS {
        UCHAR           bStatus;
        UCHAR           uPollTimeOut [DFU_XFER_STATUS_POLL_TIME_OUT_SIZE];
        UCHAR           bState;
        UCHAR           iString;
    } DFUDEVICE_STATUS, *PDFUDEVICE_STATUS;

    typedef struct   tagDFUDEVICE_STATE {
        UCHAR           ucState;
    } DFUDEVICE_STATE, *PDFUDEVICE_STATE;

    typedef struct tagCOMMAND_INFO {
        PVOID   pCommandBuffer;
        UCHAR   uDFURequest;
    } COMMAND_INFO, *PCOMMAND_INFO;
    #pragma pack(pop)

    typedef  enum    tagDFUDOWNLOADERROR {
        DFU_DOWNLOAD_ERROR_OK   =   0,
        DFU_DOWNLOAD_ERROR_TARGET,
        DFU_DOWNLOAD_ERROR_FILE,
        DFU_DOWNLOAD_ERROR_WRITE,
        DFU_DOWNLOAD_ERROR_ERASE,
        DFU_DOWNLOAD_ERROR_CHECK_ERASED,
        DFU_DOWNLOAD_ERROR_PROG,
        DFU_DOWNLOAD_ERROR_VERIFY,
        DFU_DOWNLOAD_ERROR_ADDRESS,
        DFU_DOWNLOAD_ERROR_NOTDONE,
        DFU_DOWNLOAD_ERROR_FIRMWARE,
        DFU_DOWNLOAD_ERROR_VENDOR,
        DFU_DOWNLOAD_ERROR_USBR,
        DFU_DOWNLOAD_ERROR_POR,
        DFU_DOWNLOAD_ERROR_UNKNOWN,
        DFU_DOWNLOAD_ERROR_STALLEDPKT
    } DFU_DOWNLOAD_ERROR, *PDFU_DOWNLOAD_ERROR;

    typedef  enum   tagDFUDEVICESTATE {
         DFU_DEVICE_STATE_APP_IDLE  =   0,
         DFU_DEVICE_STATE_APP_DETACH, // 1
         DFU_DEVICE_STATE_DFU_IDLE, // 2
         DFU_DEVICE_STATE_DFU_DNLOAD_SYNC,
         DFU_DEVICE_STATE_DFU_DN_BUSY,
         DFU_DEVICE_STATE_DFU_DNLOAD_IDLE, //5
         DFU_DEVICE_STATE_DFU_MANIFEST_SYNC,
         DFU_DEVICE_STATE_DFU_MANIFEST,
         DFU_DEVICE_STATE_DFU_MANIFEST_WAIT_RESET,
         DFU_DEVICE_STATE_DFU_UPLOAD_IDLE,
         DFU_DEVICE_STATE_DFU_ERROR //A
    } DFU_DEVICE_STATE, *PDFU_DEVICE_STATE;

    typedef  enum   tagDFUREQUEST {
        DFU_DETACH  =   0,
        DFU_DNLOAD, //  1
        DFU_UPLOAD,
        DFU_GETSTATUS,  //  3
        DFU_CLRSTATUS,  //  4
        DFU_GETSTATE,
        DFU_ABORT
    } DFU_REQUEST, *PDFU_REQUEST;

    NTSTATUS
    WTPDFU_BuildDFUCommand (
        IN      PDEVICE_OBJECT  pDeviceObject,
        IN  OUT PCOMMAND_INFO   pCommandInfo
    );
#endif // DFUDEVH_INC