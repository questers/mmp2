/*++

Copyright (c) 1998-2000 Intel Corporation

Module Name:

    $Workfile:: dbghdr.h                  $

Abstract:

    Common debug macros. In order to get this to work follow these steps:
    1 - make a wtpdbg.h file (NAME IS IMPORTANT) - see sample in common.
    2 - Include wtpdbg.h in all files needing debug information
    3 - Add dbghdr.c to SOURCES file

Environment:

    Kernel mode only

Notes:


    Copyright (c) 1998-2000 Intel Corporation.  All Rights Reserved.


Revision History:
    $Date:: 8/05/00 8:49a                 $ Date and time of last check in
    $Revision:: 26                        $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/
// Copyright (c) Marvell International Ltd. All Rights Reserved.

#ifndef __DBGHDR_H__
#define __DBGHDR_H__

#if DBG

// The only thing that must be defined before this file is included:
#ifndef DBGSTR_PREFIX
#error DBGSTR_PREFIX must be defined in WTPDBG.H before dbghdr.h is included!
// For example: #define DBGSTR_PREFIX "WTPUSB: " 
#endif

#ifndef WTP_POOL_TAG 
#error WTP_POOL_TAG must be defined in WTPDBG.H before dbghdr.h is included!
// For example: #define WTP_POOL_TAG 'PTW'
#endif

#define PARAM_STRING L"Parameters"

//
// Masks that apply to all drivers
//
#define DBGMASK_ERROR                   0x00000001
#define DBGMASK_INFO                    0x00000002
#define DBGMASK_TRACE                   0x00000004
#define DBGMASK_PNP_TRACE               0x00000008
#define DBGMASK_POWER_TRACE             0x00000010
#define DBGMASK_TIMESTAMP				0x00000020
#define DBGMASK_TIMEOFDAYSTAMP			0x00000040
#define DBGMASK_DEFAULT                 DBGMASK_ERROR | DBGMASK_INFO
#define DBGMASK_MAX                     0xFFFFFFFF

#define DPRINT DbgPrint
#define TRAP() DbgBreakPoint();

// Defined in dbghdr.c -- If not using dbghdr.c, then must be defined
extern ULONG			gDebugLevel; 
extern ULONG			gulTotalDriverBytes;
extern void				(*gCustomDbgFunc)(void);

// this value required for timestamp support in debug messages.
extern LARGE_INTEGER	gFirstSystemTime;

VOID
DBGHDR_Init(
    IN      PUNICODE_STRING psRegPath
    );

NTSTATUS
DBGHDR_SetDebugLevel(
	IN		PUNICODE_STRING psRegPath
	);

void
DBGHDR_SetCustomDbgFunc(
	IN		void	(*pvCustomDbgFunc)(void)
	);

NTSTATUS
DBGHDR_Win98WorkaroundGetRegPath(
        IN OUT PUNICODE_STRING psRegPath );

// 
// Macros for kdprint out to console
//
#define DBGHDR_KdPrintCond( ilev, cond, _x_) \
    if( gDebugLevel && ( (ilev) & gDebugLevel ) && ( cond )) { \
            DPRINT( DBGSTR_PREFIX ); \
            DPRINT _x_ ; \
    }

// If DBGMASK_TIMESTAMP is set in gDebugLevel the prepend a timestamp
// to the debug message. The timestamp format is seconds.milliseconds.
//
// Here's an explanation of the math involved:
//
//   Absolute system time from KeQuerySystemTime is used.
//   Absolute system time units is 100 nanoseconds.
//
//   So, the first step is to calculate the elapsed time by getting
//   the current absolute system time from KeQuerySystemTime and
//   subtracting from it the absolute system time stored from when
//   the driver was loaded (see DBGHDR_Init()).
//
//   Since milliseconds and then seconds are being printed out, the
//   first division by 10,000 converts the 100 nanosecond units to
//   milliseconds. This value, the elapsed time in milliseconds, is
//   stored in the temp variable __gCurrentSystemTime1000ths for use
//   in generating the seconds part of the formatted time. In the 
//   meantime, the mod 1000 of __gCurrentSystemTime10000ths is pushed
//   on the stack for use in generating the 1000ths part of the 
//   formatted time.
//
//   The last step is to push integer results of __gCurrentSystemTime100ths
//   divided by 1000 onto the stack for use in generating the seconds
//   part of the formatted time.
//
// Since C evaluates parameter lists from right to left, the value of
// __gCurrentSystemTime1000ths is gauranteed to have been set prior to
// its being used in the divide by 1000 step.
//
// Some miscellaneous notes: 
//
// What takes the most time of all, regardless of which timer is
// selected (see description of timers below) is (in order of time hit):
//   1. Scrolling on the monochrome monitor
//   2. Writing to the monochrome monitor
//   3. Writing to SoftIce' svga buffer when not using the 
//      monochrome monitor.
// So, in order to minimize the performance hit from debug msgs, do the
// following ( in order of most benefit gained):
//   1. if possible, use SoftIce on your primary display
//      instead of with the monochrome monitor.
//   2. If you must use the monochrome monitor, make the 
//      scrolling DbgPrint window at the bottom as small 
//      as possible until you need to examine the print log.
//
// KeQueryInterruptTime could have been used. It has the same units, so
// the calculations would stay the same, except since it is a ULONGLONG
// value, the syntax Name.QuadPart would be replaced with just Name.
// However, KeQueryInterruptTime becomes inaccurate during sleeps and
// suspends. I'm not sure about the resolution of KeQueryInterruptTime,
// eg. how often is the interrupt time updated?
//
// KeQueryTickCount could have been used, but the  resolution is less
// fine.
// 
// KeQueryPerformanceCounter could have been used, but DDK warns that
// using this call extensively can impact the performance since interrupts
// are disabled during the call to KeQueryPerformanceCounter.

#define DBGHDR_KdPrint(ilev, _x_)									\
    if( gDebugLevel && ( (ilev) & gDebugLevel ) ) {					\
        DPRINT _x_ ;												\
        if( gCustomDbgFunc ) gCustomDbgFunc();						\
	}                                                               \

#if 0
// this version contains timestamp & module id support. but some debug viewers break that into multiple lines...so disable that.
#define DBGHDR_KdPrint(ilev, _x_)									\
    if( gDebugLevel && ( (ilev) & gDebugLevel ) ) {					\
		if( gDebugLevel & DBGMASK_TIMESTAMP )						\
		{															\
			LARGE_INTEGER	__gCurrentSystemTime1000ths;			\
			KeQuerySystemTime(&__gCurrentSystemTime1000ths);		\
																	\
			DbgPrint(												\
				"%5d.%03d %02x ",									\
				(ULONG)__gCurrentSystemTime1000ths.QuadPart / 1000,	\
				(ULONG)(__gCurrentSystemTime1000ths.QuadPart = (__gCurrentSystemTime1000ths.QuadPart - gFirstSystemTime.QuadPart ) / 10000 ) % 1000, \
				KeGetCurrentIrql()									\
				);													\
		}															\
	    DPRINT( DBGSTR_PREFIX );									\
        DPRINT _x_ ;												\
        if( gCustomDbgFunc ) gCustomDbgFunc();						\
	}                                                               
#endif


#define DBGHDR_PrintNoName(ilev, _x_) \
    if( gDebugLevel && ( (ilev) & gDebugLevel ) ) { \
            DPRINT _x_ ; \
	}


#define DBGHDR_TrapCond( ilev, cond ) if ( gDebugLevel && \
( (ilev) & gDebugLevel ) && (cond) ) TRAP()
#define DBGHDR_Trap( ilev )   DBGHDR_TrapCond( ilev, TRUE )
#define DBGHDR_ASSERT( cond ) ASSERT( cond )



//
// SpinLock Macros
//

#else // if not DBG

VOID
DBGHDR_Init(
    IN      PUNICODE_STRING psRegPath
    );

NTSTATUS
DBGHDR_SetDebugLevel(
                     IN PUNICODE_STRING psRegPath
                     );
// dummy definitions that go away in the retail build

#define DBGHDR_KdPrintCond( ilev, cond, _x_) 
#define DBGHDR_KdPrint( ilev, _x_)  
#define DBGHDR_TrapCond( ilev, cond ) 
#define DBGHDR_Trap( ilev )
#define DBGHDR_ASSERT( cond )
#define DBGHDR_PrintNoName(ilev, _x_)

#endif //DBG

// Function to test if this is Win2k or not
extern NTSTATUS DBGHDR_IsWindows2000( BOOLEAN *pbRes );

#endif // __DBGHDR_H__

