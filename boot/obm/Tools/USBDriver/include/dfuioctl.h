/*++

Copyright (c) 1998-1999 Intel Corporation

Module Name:

    $Workfile:: DFUIOCTL.H                $

Abstract:

    This contains the IoCtl code(s) and other external interfaces supported in WTPDFU.

Environment:

    Kernel mode only

Notes:

    Copyright (c) 1998-1999 Intel Corporation.  All Rights Reserved.

    The receipt of or possession of this program does not convey
    any rights to reproduce its contents or to manufacture,
    use, or sell anything that it may describe, in whole, or 
    in part, without specific written consent of Intel Corporation.


Revision History:
    $Date:: 10/14/99 2:29p                $ Date and time of last check in
    $Revision:: 18                        $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/
// Copyright (c) Marvell International Ltd. All Rights Reserved.

#ifndef DFUIOCTLH_INC
#define DFUIOCTLH_INC

    #define DATALENGTH 4090
	#define RETURNDATALENGTH 4089
	#define MESSAGELENGTH 256
	#define WTPSTATUSLENGTH 3996

	#define WTPDFU_IOCTL_INDEX                               0xDF0 // Device Firmware Upgrade
    #define WTPDFU_IOCTL_METHOD_NEITHER(function)            CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                                (WTPDFU_IOCTL_INDEX + function), \
                                                                 METHOD_NEITHER, FILE_ANY_ACCESS)

    #define WTPDFU_IOCTL_METHOD_IN_DIRECT(function)          CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                                (WTPDFU_IOCTL_INDEX + function), \
                                                                 METHOD_IN_DIRECT, FILE_ANY_ACCESS)

    #define WTPDFU_IOCTL_METHOD_OUT_DIRECT(function)         CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                                (WTPDFU_IOCTL_INDEX + function), \
                                                                 METHOD_OUT_DIRECT, FILE_ANY_ACCESS)

    #define WTPDFU_IOCTL_METHOD_BUFFERED(function)           CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                                                (WTPDFU_IOCTL_INDEX + function), \
                                                                 METHOD_BUFFERED, FILE_ANY_ACCESS)

    #define IOCTL_WTPDFU_UPGRADE_DEVICE_FIRMWARE                WTPDFU_IOCTL_METHOD_IN_DIRECT(0) 

    #define IOCTL_WTPDFU_IMAGEDATAXMIT							WTPDFU_IOCTL_METHOD_BUFFERED(8)	
	#define IOCTL_WTPDFU_COMMAND								WTPDFU_IOCTL_METHOD_BUFFERED(9)

	// test commands:
	#define IOCTL_WTPDFU_GETDEVDESC								WTPDFU_IOCTL_METHOD_BUFFERED(33)
	#define IOCTL_WTPDFU_GETCFGDESC								WTPDFU_IOCTL_METHOD_BUFFERED(34)
    
    #define IOCTL_WTPDFU_SEND                                     WTPDFU_IOCTL_METHOD_BUFFERED(14)
    #define IOCTL_WTPDFU_RECEIVE                                  WTPDFU_IOCTL_METHOD_BUFFERED(15)

	#pragma pack(push,1)
	typedef struct
	{
		UCHAR CMD;
		UCHAR SEQ;
		UCHAR CID;
		UCHAR Flags;
		ULONG LEN;
		UCHAR Data[DATALENGTH];
	}WTPCOMMAND, *PWTPCOMMAND;
	#pragma pack(pop)

	#pragma pack(push,1)
	typedef struct
	{
		UCHAR CMD;
		UCHAR SEQ;
		UCHAR CID;
		UCHAR Status;
		UCHAR Flags;
		UCHAR DLEN;
		UCHAR Data[RETURNDATALENGTH];
	}WTPSTATUS, *PWTPCSTATUS;
	#pragma pack(pop)

    #pragma pack(push,1)
    typedef struct _DFU_FUNCTIONAL_DESCRIPTOR {
        UCHAR   bLength;
        UCHAR   bDescriptorType;
        UCHAR   bmAttributes;
        USHORT  wDetachTimeOut;
        USHORT  wTransferSize;
    } DFU_FUNCTIONAL_DESCRIPTOR, *PDFU_FUNCTIONAL_DESCRIPTOR;
    #pragma pack(pop)

    typedef struct _FIRMWARE_INFO {
        PVOID       pImageBuffer;
        ULONG       uImageBufferSize;
    } FIRMWARE_INFO, *PFIRMWARE_INFO;

// {6F40032F-99BE-47c0-B19C-BDB4BF322F49}
    DEFINE_GUID(GUID_WTPDFU,
                0x6f40032f, 0x99be, 0x47c0, 0xb1, 0x9c, 0xbd, 0xb4,
                0xbf, 0x32, 0x2f, 0x49);
#endif // DFUIOCTLH_INC