/*++

Copyright (c) 1998-1999 Intel Corporation

Module Name:

    $Workfile:: wtpdfu.h                         $

Abstract:

    The main header file for the driver    

Environment:

    Kernel mode only

Notes:

    Copyright (c) 1998-1999 Intel Corporation.  All Rights Reserved.

    The receipt of or possession of this program does not convey
    any rights to reproduce its contents or to manufacture,
    use, or sell anything that it may describe, in whole, or 
    in part, without specific written consent of Intel Corporation.


Revision History:
    $Date:: 5/18/00 4:53p                 $ Date and time of last check in
    $Revision:: 2                         $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/
// Copyright (c) Marvell International Ltd. All Rights Reserved.

#ifndef WTPDFUH_INC
#define WTPDFUH_INC

#include "dfuioctl.h"
#include "dfudev.h"

// Using METHOD_BUFFERED because very little data is passed using these ioctls.
#define WTPUSB_IOCTL(_index_) \
    CTL_CODE (FILE_DEVICE_UNKNOWN, _index_, METHOD_BUFFERED, FILE_ANY_ACCESS)

#ifdef DBG
#define IOCTL_WTPUSB_ENUMERATE WTPUSB_IOCTL (0x10)
#define IOCTL_WTPUSB_REMOVEPDO WTPUSB_IOCTL (0x20)
#endif 

//
// Trace debug macro definition
//
#if DBG
#define DBG_QUEUE_LEN 0x1000
#define DBG_DEVEXT(A) ((PCOMMON_DEVICE_EXTENSION)((A)->DeviceExtension))

// A = DeviceObject, B = KIRQL
#define dbgT(A,_dbgchar,B)                                                              \
{                                                                                       \
    KeAcquireSpinLock( &((A)->dbgTraceLock),&(B));                                      \
    (A)->dbgTrace[((A)->dbgTraceIndex)] = _dbgchar;                                     \
    (A)->dbgTraceIndex = (((A)->dbgTraceIndex + 1) & (DBG_QUEUE_LEN-1));                \
    (A)->dbgTrace[((A)->dbgTraceIndex)] = '~';                                          \
    KeReleaseSpinLock(&(A)->dbgTraceLock, (B));                                         \
}
#else
# define dbgT(A,_dbgchar,B) 
#endif

// 
// WMI Info
//
#define MAX_WMI_GUIDS                   0x02
#define WTPUSB_PERF_WMI_IRP_BLOCK        0x00
#define WTPUSB_CONFIG_WMI_IRP_BLOCK      0x01

typedef struct _WTPUSB_PERF_WMI_INFO {
	ULONG	ulFWImageSize;
	ULONG	ulBytesSent;
	ULONG	ulPacketsSent;
	ULONG	ulErrors;
	ULONG	ulBlockNum;
	ULONG	ulDeviceStatus;
	ULONG	ulDeviceState;
} WTPUSB_PERF_WMI_INFO, *PWTPUSB_PERF_WMI_INFO;

typedef struct _WTPUSB_CONFIG_WMI_INFO {
	ULONG	ulSendUpdates;			// if set, dfu will send update status msgs to ring3
} WTPUSB_CONFIG_WMI_INFO, *PWTPUSB_CONFIG_WMI_INFO;

// Interfaces
#define WTPUSB_INTERFACE_DEFAULT         0x00
#define WTPUSB_INTERFACE_ISOCH           0x01
#define WTPUSB_INTERFACE_DFU             0x02

// Device firmware upgrade (DFU) defines
#define DFU_DETACH_REQUEST              0x00
#define DFU_TIMEOUT_BEFORE_RESET        0x1000

// Invalid Pipe Number declaration
#define INVALID_PIPE                    0x09

// Iso Stream Defines
#define WTPUSB_NUM_OUTSTANDING_STREAM_OBJECTS 0x03
#define WTPUSB_ISO_STREAM_OUT_BYTE_0 0x03
#define WTPUSB_ISO_STREAM_OUT_BYTE_1 0xFE
#define MIN_STREAM_REQUESTS_FOR_START 0x04

//
// Hardware & Compatible IDs
//
#define RFBD_COMPATIBLE_ID  L"DEVICE\\INTCBTCOMPAT\0"
#define RFBD_PDO_NAME       L"\\Device\\INTCRFT_0"
#define RFBD_HARDWARE_ID    L"DEVICE\\INTCRFT\0"
//#define RFBD_PDO_NAME       L"\\Device\\INTCRFBD_0"
//#define RFBD_HARDWARE_ID    L"DEVICE\\INTCRFBD\0"
/**/

//
// Enum used by the transfer object structure so that the completion
// routine knows whether the request was a read or write
//
typedef enum _WTPUSB_READ_OR_WRITE {
    Read,
    Write
} WTPUSB_READ_OR_WRITE;

//
// Enum used in call to WTPUSB_SelfSuspendOrActivate
//
typedef enum _WTPUSB_SUSPEND_OR_ACTIVATE{
    Suspend,
    Activate,
    ForceActivate
} WTPUSB_SUSPEND_OR_ACTIVATE;

//
// Transfer object 
// When an IRP arrives in process_ioctl, it is saved in a transfer object.
// The transfer objects are saved by the driver and accessed by a set of routines.
// The transfer object is recycled when the IRP is cancelled or completed.
//
// HeadOfCurrentList is used by the USBDSubmittal, cancel, and completion routines
// to know what queue this transfer object is in. It is set by the APPQueueHandler.
//
typedef struct _WTPUSB_TRANSFER_OBJECT {
    LIST_ENTRY              Link;
    PIRP                    pIrp;
    PURB                    pUrb;
    PDEVICE_OBJECT          DeviceObject;
    PIRP                    BaseIrp;
    WTPUSB_READ_OR_WRITE     Direction;
    PUSBD_PIPE_INFORMATION  PipeInfo;
    PLIST_ENTRY             HeadOfCurrentList;
    PKSPIN_LOCK             LockForCurrentList;
    PULONG                  pCurrentCounter;
    KDPC                    dpc;
} WTPUSB_TRANSFER_OBJECT, *PWTPUSB_TRANSFER_OBJECT;


//
// Common Device Extension
//
typedef struct _COMMON_DEVICE_EXTENSION
{
    PDEVICE_OBJECT      self;
    BOOLEAN             isFDO;
    SYSTEM_POWER_STATE  CurrentSystemState;
    DEVICE_POWER_STATE  CurrentDeviceState;
    // An event to serialize IRPs sent to the lower device objects
    KEVENT irpSyncEvent; 

    #if DBG
        ULONG dbgTraceIndex;
        UCHAR dbgTrace[DBG_QUEUE_LEN];
        KSPIN_LOCK dbgTraceLock;
    #endif 

} COMMON_DEVICE_EXTENSION, *PCOMMON_DEVICE_EXTENSION;

//
// PDO Device Extension
//
typedef struct _PDO_DEVICE_EXTENSION
{
    COMMON_DEVICE_EXTENSION;

    // An array of (zero terminated wide character strings).
    // The array itself also null terminated
    PWCHAR HardwareIDs;

    PDEVICE_OBJECT parentFdo;

    BOOLEAN DeviceStarted;
    BOOLEAN DeviceAttached;
    BOOLEAN DeviceRemoved;

    // When a device (PDO) is found on a bus and presented as a device relation
    // to the PlugPlay system, Attached is set to TRUE, and Removed to FALSE.
    // When the bus driver determines that this PDO is no longer valid, because
    // the device has gone away, it informs the PlugPlay system of the new
    // device relastions, but it does not delete the device object at that time.
    // The PDO is deleted only when the PlugPlay system has sent a remove IRP,
    // and there is no longer a device on the bus.
    //
    // If the PlugPlay system sends a remove IRP then the Removed field is set
    // to true, and all client (non PlugPlay system) accesses are failed.
    // If the device is removed from the bus Attached is set to FALSE.
    //
    // Durring a query relations Irp Minor call, only the PDOs that are
    // attached to the bus (and all that are attached to the bus) are returned
    // (even if they have been removed).
    //
    // Durring a remove device Irp Minor call, if and only if, attached is set
    // to FALSE, the PDO is deleted.

} PDO_DEVICE_EXTENSION, *PPDO_DEVICE_EXTENSION;

//
// FDO Device Extension
//
typedef struct _FDO_DEVICE_EXTENSION
{
    COMMON_DEVICE_EXTENSION;

    // flag set when device has been successfully started
    BOOLEAN DeviceStarted;

    //flag set when processing IRP_MN_REMOVE_DEVICE
    BOOLEAN DeviceRemoved;

    // flag set when driver has answered success to IRP_MN_QUERY_REMOVE_DEVICE
    BOOLEAN RemoveDeviceRequested;

    // flag set when driver has answered success to IRP_MN_QUERY_STOP_DEVICE
    BOOLEAN StopDeviceRequested;

    // flag set when DFU is requested. Cleared if there is an error.
    BOOLEAN DFURequested;

    // incremented when device is added and any IO request is received;
    // decremented when any io request is completed or passed on, and when device is removed
    ULONG PendingIoCount;

    // set when PendingIoCount goes to 0; flags device can be removed
    KEVENT RemoveEvent;

    // set when PendingIoCount goes to 1 ( 1st increment was on add device )
    // this indicates no IO requests outstanding, either user, system, or self-staged
    KEVENT NoPendingIoEvent;

    // The RFBD PDO
    // RH - Do I need to protect this object with a spinlock?
    PDEVICE_OBJECT  pdo;

    // The interface
    UNICODE_STRING interfaceName;

    // The WTPDFU interface
    UNICODE_STRING interfaceNameWTPDFU;

    // Device capabilities structure
    DEVICE_CAPABILITIES deviceCapabilities;
    
    // Device object we call
    PDEVICE_OBJECT topOfStackDeviceObject;

    // The bus driver object
    PDEVICE_OBJECT lowerPhysicalDeviceObject;

    // An event on which to wait for IRPs sent to the lower device objects
    // to complete.
    KEVENT callEvent;
    
    // Our registry path (for WMI)
    PUNICODE_STRING registryPath;

    // Our fixed up registry path:
	// For win98, this is hard coded because the input parameter to DriverEntry
	// references a hardware or enum registry path, not the services registry key.
	// For non-win98 platforms, this will be the same as the path passed to DriverEntry.
	// Any modules that need persistent storage should make use of this variable...
    UNICODE_STRING	registryServicePath;
	BOOLEAN			bRegistryServicePathAllocated;

	WTPUSB_PERF_WMI_INFO		perfInfo;
	WTPUSB_CONFIG_WMI_INFO	configInfo;

    //
    // USB Specific Variables
    //
    PUSB_CONFIGURATION_DESCRIPTOR UsbConfigurationDescriptor;
    USBD_CONFIGURATION_HANDLE UsbConfigurationHandle;
    PUSB_DEVICE_DESCRIPTOR UsbDeviceDescriptor;
    PUSBD_INTERFACE_INFORMATION UsbInterface;
    PUSBD_INTERFACE_INFORMATION UsbIsochInterface;
    PUSBD_INTERFACE_INFORMATION UsbDFUInterface;
    PDFU_FUNCTIONAL_DESCRIPTOR UsbDFUDescriptor;
    USBD_PIPE_INFORMATION CMDPipe;

    // Pipe numbers
    USHORT pipeNumACLIn;     // bulk endpoint (IN)
    USHORT pipeNumACLOut;    // bulk endpoint (OUT)
    USHORT pipeNumSCOIn;     // iso endpoint (IN)
    USHORT pipeNumSCOOut;    // iso endpoint (OUT)
    USHORT pipeNumCMDIn;     // the interrupt endpoint (IN)

    // Variables specific to stopping SCO traffic (for when a new alternate setting needs to be selected)
    BOOLEAN PauseSCOTrafficRequested;
    KEVENT SCOTrafficStoppedEvent;

    // Iso stream variables
    BOOLEAN bIsoOutStreamStarted;
    BOOLEAN bIsoInStreamStarted;
    KEVENT IsoInTrafficStoppedEvent;
    KEVENT IsoOutTrafficStoppedEvent;
    KEVENT IsoInStreamStartCompleteEvent;
    KEVENT IsoOutStreamStartCompleteEvent;

    ULONG frameNumberIsoIn;
    ULONG frameNumberIsoOut;
    
    //
    // Queues & their synchronization objects
    //

    // APP Submittal variables
    LIST_ENTRY APPSubmittalQueue;
    KSPIN_LOCK spinForSubmittalQueue;

    // Transfer Object Queues
    LIST_ENTRY FreeTransferObjectQueue;
    LIST_ENTRY PendingSCOInQueue;
    LIST_ENTRY PendingSCOOutQueue;
    LIST_ENTRY PendingACLQueue;
    LIST_ENTRY PendingCMDQueue;
    LIST_ENTRY WaitingForInStreamQueue;
    LIST_ENTRY WaitingForOutStreamQueue;

    // Transfer Object Queue Spinlocks
    KSPIN_LOCK spinForFreeTransferObjectQueue;
    KSPIN_LOCK spinForPendingSCOInQueue;
    KSPIN_LOCK spinForPendingSCOOutQueue;
    KSPIN_LOCK spinForPendingACLQueue;
    KSPIN_LOCK spinForPendingCMDQueue;
    KSPIN_LOCK spinForWaitingForInStreamQueue;
    KSPIN_LOCK spinForWaitingForOutStreamQueue;

#ifdef SUPPORTWMI
    // 
    // WMI GUIDs
    // 
    WMIGUIDREGINFO WmiGuidArray[MAX_WMI_GUIDS];
    WMILIB_CONTEXT WmiLibContext;
#endif
    //
    // WMI Counters
    //
    ULONG numTotalTransferObjects;
    ULONG numFreeTransferObjects;
    ULONG numPendingSCOInIrps;
    ULONG numPendingSCOOutIrps;
    ULONG numPendingACLIrps;
    ULONG numPendingCMDIrps;
    ULONG totalIrpsReceivedForSCO;
    ULONG totalIrpsReceivedForACL;
    ULONG totalIrpsReceivedForCMD;
    ULONG numWaitingForInStream;
    ULONG numWaitingForOutStream;
    ULONG numWaitingInAPPQueue;

    // 
    // Counters for the various open pipes (only use ExInterlocked routines to modify)
    //      Can also be reported to WMI
    //
    ULONG ACLInstanceCount;
    ULONG SCOInstanceCount;
    ULONG ControlInstanceCount;

    // 
    // Power
    //

    // used to save the currently-being-handled system-requested power irp request
    PIRP powerIrp;

    // used to flag that we're currently handling a self-generated power request
    BOOLEAN SelfPowerIrp;

    // set to signal driver-generated power request is finished
    KEVENT SelfRequestedPowerIrpEvent;

    // Are we currently in a query power state?
    BOOLEAN powerQueryLock;

    // flag set when IRP_MN_WAIT_WAKE is received and we're in a power state
    // where we can signal a wait
    BOOLEAN EnabledForWakeup;

    // default power state to power down to on self-suspend 
    ULONG PowerDownLevel; 

} FDO_DEVICE_EXTENSION, *PFDO_DEVICE_EXTENSION;

//
// Function declarations
//
NTSTATUS
DriverEntry(
    IN PDRIVER_OBJECT DriverObject,
    IN PUNICODE_STRING RegistryPath
    );

NTSTATUS
WTPUSB_ProcessPnPIrp(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP           Irp
    );

NTSTATUS
WTPUSB_DefaultDispatch(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP           Irp
    );

NTSTATUS
WTPUSB_ProcessFDOPnPIrp (    // Entry 'N' Exit 'n'
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp,
    IN PIO_STACK_LOCATION irpStack,
    IN PFDO_DEVICE_EXTENSION FDODeviceExtension
    );

NTSTATUS
WTPUSB_ProcessPDOPnPIrp(     // Entry 'M' Exit 'm'
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp,
    IN PIO_STACK_LOCATION irpStack,
    IN PPDO_DEVICE_EXTENSION PDODeviceExtension
    );

NTSTATUS 
WTPUSB_OnRequestComplete(
    IN PDEVICE_OBJECT fdo, 
    IN PIRP Irp, 
    IN PKEVENT pev
    );

NTSTATUS
WTPUSB_StartDevice(
    IN  PDEVICE_OBJECT DeviceObject
    );

NTSTATUS
WTPUSB_CallUSBD(
    IN PDEVICE_OBJECT DeviceObject,
    IN PURB Urb,
    IN OPTIONAL ULONG ulControlCode
    );

NTSTATUS
WTPUSB_ConfigureDevice(
    IN  PDEVICE_OBJECT DeviceObject
    );

NTSTATUS
WTPUSB_SelectInterface(
    IN PDEVICE_OBJECT DeviceObject,
    IN PUSB_CONFIGURATION_DESCRIPTOR ConfigurationDescriptor
    );

NTSTATUS
WTPUSB_SelectDFUInterface(
    IN PDEVICE_OBJECT DeviceObject,
    IN PUSB_CONFIGURATION_DESCRIPTOR ConfigurationDescriptor
    );

NTSTATUS
WTPUSB_AddDevice(        
    IN PDRIVER_OBJECT DriverObject,
    IN PDEVICE_OBJECT BusPhysicalDeviceObject
    );

NTSTATUS
WTPUSB_GetAndSetCapabilitiesStructure(
    IN OUT PFDO_DEVICE_EXTENSION FDODeviceExtension
    );

LONG
WTPUSB_DecrementIoCount(
    IN PDEVICE_OBJECT DeviceObject
    );

VOID
WTPUSB_IncrementIoCount(
    IN PDEVICE_OBJECT DeviceObject
    );

NTSTATUS
WTPUSB_StopDevice(
    IN  PDEVICE_OBJECT DeviceObject
    );

NTSTATUS
WTPUSB_RemoveDevice(
    IN  PDEVICE_OBJECT DeviceObject
    );

NTSTATUS
WTPUSB_CanAcceptIoRequests(
    IN PDEVICE_OBJECT DeviceObject
    );

NTSTATUS
WTPUSB_CanQueueIoRequests(
    IN PDEVICE_OBJECT DeviceObject
    );

NTSTATUS
WTPUSB_Close(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp
    );

NTSTATUS
WTPUSB_Create(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp
    );

VOID
WTPUSB_Unload(       
    IN PDRIVER_OBJECT DriverObject
    );

NTSTATUS
WTPUSB_ProcessIOCTL(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp
    );

NTSTATUS
WTPUSB_ProcessSystemControl(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp
);

NTSTATUS
WTPUSB_LoadUpperDriver(
    IN PDEVICE_OBJECT DeviceObject
);

NTSTATUS
WTPUSB_UnLoadUpperDriver(
    IN PDEVICE_OBJECT DeviceObject
);

NTSTATUS
WTPUSB_CancelPendingIO(      // Entry 'Z' Exit 'z'
    IN PDEVICE_OBJECT DeviceObject
);

NTSTATUS
WTPUSB_AbortPipes(
    IN PDEVICE_OBJECT DeviceObject,
    IN PUSBD_INTERFACE_INFORMATION UsbInterface
);

VOID
WTPUSB_ServiceAPPQueue( 
    IN PFDO_DEVICE_EXTENSION FDODeviceExtension
);

NTSTATUS
WTPUSB_DispatchAPPRequest(         // Entry 'H' Exit 'h'
    IN PWTPUSB_TRANSFER_OBJECT pTransferObject
);

NTSTATUS
WTPUSB_ReadWrite_BulkOrInterrupt(    // Entry 'B' Exit 'b'
    IN PWTPUSB_TRANSFER_OBJECT pTransferObject
);

NTSTATUS
WTPUSB_Write_Control(        // Entry 'W' Exit 'w'
    IN PWTPUSB_TRANSFER_OBJECT pTransferObject
);

NTSTATUS
WTPUSB_ReadWrite_Isoch(      // Entry 'I' Exit 'i'
    IN PWTPUSB_TRANSFER_OBJECT pTransferObject
);

VOID
WTPUSB_APPQueueCancelRoutine( // Entry 'Q'
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP           Irp
);

VOID
WTPUSB_CMDCancelRoutine(     // Entry 'D'
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP           Irp
);

VOID
WTPUSB_ACLCancelRoutine(     // Entry 'L'
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP           Irp
);

VOID
WTPUSB_SCOCancelRoutine(     // Entry 'O'
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP           Irp
);

NTSTATUS
WTPUSB_CancelSubmittedAPP (     // Entry 'X' Exit 'x'
    IN PDEVICE_OBJECT DeviceObject,
    IN PLIST_ENTRY  ListHead,
    IN PKSPIN_LOCK  Lock,
    IN BOOLEAN  bCancelBaseIrp
);

NTSTATUS
WTPUSB_USBDCompletedAnIrp(   // Entry 'C' Exit 'c'
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp,
    IN PVOID Context
);

NTSTATUS
WTPUSB_SubmitTransferObjectToUSBD(   
    IN PWTPUSB_TRANSFER_OBJECT pTransferObject
);

NTSTATUS
WTPUSB_SelectIsochInterface(
    IN PDEVICE_OBJECT DeviceObject,
    IN PUSB_CONFIGURATION_DESCRIPTOR ConfigurationDescriptor,
    IN UCHAR alternateSetting
);

//
// Power Handling routines
//
NTSTATUS
WTPUSB_ProcessPowerIrp(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp
);

NTSTATUS
WTPUSB_ProcessPDOPowerIrp(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp,
    IN PIO_STACK_LOCATION irpStack,
    IN PPDO_DEVICE_EXTENSION PDODeviceExtension
);

NTSTATUS
WTPUSB_ProcessFDOPowerIrp (
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp,
    IN PIO_STACK_LOCATION irpStack,
    IN PFDO_DEVICE_EXTENSION FDODeviceExtension
);

BOOLEAN
WTPUSB_SetDevicePowerState(      // Entry 'P' Exit 'p'
    IN PDEVICE_OBJECT DeviceObject,
    IN DEVICE_POWER_STATE DeviceState
);

NTSTATUS
WTPUSB_SelfSuspendOrActivate(
    IN PDEVICE_OBJECT DeviceObject,
    IN WTPUSB_SUSPEND_OR_ACTIVATE suspendOrActivate
);

NTSTATUS
WTPUSB_PoRequestCompletion(
    IN PDEVICE_OBJECT       DeviceObject,
    IN UCHAR                MinorFunction,
    IN POWER_STATE          PowerState,
    IN PVOID                Context,
    IN PIO_STATUS_BLOCK     IoStatus
);

NTSTATUS
WTPUSB_PoSelfRequestCompletion(
    IN PDEVICE_OBJECT       DeviceObject,
    IN UCHAR                MinorFunction,
    IN POWER_STATE          PowerState,
    IN PVOID                Context,
    IN PIO_STATUS_BLOCK     IoStatus
);

NTSTATUS
WTPUSB_PowerIrp_Complete(
    IN PDEVICE_OBJECT NullDeviceObject,
    IN PIRP Irp,
    IN PVOID Context
);

NTSTATUS
WTPUSB_SelfRequestPowerIrp(
    IN PDEVICE_OBJECT DeviceObject,
    IN POWER_STATE PowerState
);

NTSTATUS
WTPUSB_Cleanup(          // Entry 'U' Exit 'u'
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP           Irp
);

// WMI Functions
VOID
WTPUSB_InitializeGuidList(
    IN OUT PFDO_DEVICE_EXTENSION FDODeviceExtension
);

NTSTATUS
WTPUSB_ProcessFDOWMIIrp (
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp,
    IN PIO_STACK_LOCATION irpStack,
    IN PFDO_DEVICE_EXTENSION FDODeviceExtension
);


NTSTATUS
WTPUSB_QueryWmiDataBlock(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP pIrp,
    IN ULONG GuidIndex, 
    IN ULONG InstanceIndex,
    IN ULONG InstanceCount,
    IN OUT PULONG InstanceLengthArray,
    IN ULONG OutBufferSize,
    OUT PUCHAR pBuffer
);

NTSTATUS
WTPUSB_QueryWmiRegInfo(
   IN PDEVICE_OBJECT DeviceObject,
   OUT PULONG pRegFlags,
   OUT PUNICODE_STRING pInstanceName,
   OUT PUNICODE_STRING *pRegistryPath,
   OUT PUNICODE_STRING MofResourceName,
   OUT PDEVICE_OBJECT *pdo
);

#ifdef SUPPORTWMI
NTSTATUS
WTPUSB_WmiFunctionControl(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP pIrp,
    IN ULONG GuidIndex,
    IN WMIENABLEDISABLECONTROL Function,
    IN BOOLEAN bEnable
);
#endif

//
// Iso streaming routines
//
NTSTATUS
WTPUSB_StartIsoStream(   // Entry 'S' Exit 's'
    IN PFDO_DEVICE_EXTENSION FDODeviceExtension,
    IN USHORT numTransferObjects,
    IN PUSBD_PIPE_INFORMATION PipeInfo
);

NTSTATUS
WTPUSB_StopIsoStream(    // Entry 'T' Exit 't'
    IN PFDO_DEVICE_EXTENSION FDODeviceExtension,
    IN WTPUSB_READ_OR_WRITE direction
);

NTSTATUS
WTPUSB_GetNextTransferBuffer(
    IN OUT PWTPUSB_TRANSFER_OBJECT pTransferObject,
    IN PFDO_DEVICE_EXTENSION FDODeviceExtension
);

NTSTATUS
WTPUSB_ResetIsoTransferObject(   // Entry 'V' Exit 'v'
    IN OUT PWTPUSB_TRANSFER_OBJECT pTransferObject
);

NTSTATUS
WTPUSB_SubmitIsoRequestDpc(      // Entry 'E' Exit 'e'
    IN PKDPC Dpc,
    IN PVOID DeferredContext,
    IN PVOID SystemArgument1,
    IN PVOID SystemArgument2
);

NTSTATUS
WTPUSB_ResetPipe(
    IN PDEVICE_OBJECT DeviceObject,
    IN PUSBD_PIPE_INFORMATION PipeInfo
);

NTSTATUS
WTPUSB_GetCurrentFrame(
    PDEVICE_OBJECT DeviceObject,
    PULONG pulCurrentFrameNumber
);

NTSTATUS
WTPUSB_SendDFUDetachMessage(
    PDEVICE_OBJECT DeviceObject
);

NTSTATUS
WTPUSB_UpgradeFirmware(
    PDEVICE_OBJECT	pDeviceObject,
	PIRP			pIrp
);

NTSTATUS
WTPUSB_GetXferStatus (
    IN      PDEVICE_OBJECT  pDeviceObject,
    IN  OUT PDEVICE_STATUS  pDeviceStatus,
    IN      PCOMMAND_INFO   pCommandInfo
);

NTSTATUS
WTPUSB_BuildDFUCommand (
    IN      PDEVICE_OBJECT  pDeviceObject,
    IN  OUT PCOMMAND_INFO   pCommandInfo
);


NTSTATUS
WTPUSB_DFUDetach(
	PDEVICE_OBJECT		pDeviceObject,
	unsigned short		usTimeout
	);

NTSTATUS
WTPUSB_DFUDnload(
	PDEVICE_OBJECT		pDeviceObject,
	unsigned short		usBlockNum,
	unsigned short		usLength,
	PMDL				pmdlFirmware
	);

NTSTATUS
WTPUSB_DFUUpload(
	PDEVICE_OBJECT		pDeviceObject,
	unsigned short		usLength,
	unsigned char		*pucFirmware
	);

NTSTATUS
WTPUSB_DFUGetStatus(
	PDEVICE_OBJECT		pDeviceObject,
	PDFUDEVICE_STATUS	pDfuDeviceStatus
	);

NTSTATUS
WTPUSB_DFUClrStatus(
	PDEVICE_OBJECT		pDeviceObject
	);

NTSTATUS
WTPUSB_DFUGetState(
	PDEVICE_OBJECT		pDeviceObject,
	PDFUDEVICE_STATE	pDfuDeviceState
	);

NTSTATUS
WTPUSB_DFUAbort(
	PDEVICE_OBJECT		pDeviceObject
	);


NTSTATUS 
WTPUSB_ClearStall (
    IN  PDEVICE_OBJECT			pDeviceObject,
	IN	PUSBD_PIPE_INFORMATION	pPipeInfo
);

NTSTATUS
WTPUSB_ClearFeature(
	IN PDEVICE_OBJECT		DeviceObject,
 	IN USHORT				usFeature,		// should be 0 for Stall feature
	IN USHORT				usRecipient,	// URB_FUNCTION_(SET|CLEAR)_(DEVICE|INTERFACE|ENDPOINT)_FEATURE
	IN USHORT				usIndex			// endpoint
    );

NTSTATUS
WTPUSB_DetachFromDevice (
    IN PDEVICE_OBJECT   pDeviceObject
);

NTSTATUS 
WTPUSB_ClearControlEndpointStall( 
	IN PDEVICE_OBJECT		pDeviceObject 
);

NTSTATUS 
WTPUSB_ConfigureNullDevice( 
	IN PDEVICE_OBJECT	pDeviceObject 
);

NTSTATUS 
WTPUSB_CyclePort( 
	IN PDEVICE_OBJECT	pDeviceObject 
);

NTSTATUS 
WTPUSB_CompleteIrp(
    IN PDEVICE_OBJECT fdo, 
    IN PIRP Irp, 
    IN PKEVENT pev
    );

/*
A,a - WTPUSB_AllocateTransferObject
B,b - WTPUSB_ReadWrite_BulkOrInterrupt
C,c - WTPUSB_USBDCompletedAnIrp
D,d - WTPUSB_CMDCancelRoutine (entry only)
E,e - WTPUSB_SubmitIsoRequestDpc
F,f - 
G,g - WTPUSB_GetTransferObject
H,h - WTPUSB_DispatchAPPRequest
I,i - WTPUSB_ReadWrite_Isoch
J,j - 
K,k - 
L,l - WTPUSB_ACLCancelRoutine (entry only)
M,m - WTPUSB_ProcessPDOPnPIrp
N,n - WTPUSB_ProcessFDOPnPIrp
O,o - WTPUSB_SCOCancelRoutine (entry only)
P,p - WTPUSB_SetDevicePowerState
Q,q - WTPUSB_APPQueueCancelRoutine (entry only)
R,r - WTPUSB_RecycleTransferObject
S,s - WTPUSB_StartIsoStream
T,t - WTPUSB_StopIsoStream
U,u - WTPUSB_Cleanup
V,v - WTPUSB_ResetIsoTransferObject
W,w - WTPUSB_Write_Control
X,x - WTPUSB_CancelSubmittedAPP
Y,y - 
Z,z - WTPUSB_CancelPendingIO
1 - StopIsoStream made it inside the Read conditional
2 - StopIsoStream made it inside the Write conditional
3 - 
4 - 
5 - 
6 - 
7 - 
8 - 
9 - 

*/

#endif 

