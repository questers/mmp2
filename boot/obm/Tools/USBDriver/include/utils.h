/*++

Copyright (c) 1998-1999 Intel Corporation

Module Name:

    $Workfile:: utils.h                   $

Abstract:

    Header file for utility functions

Environment:

    Kernel mode only

Notes:

    Copyright (c) 1998-1999 Intel Corporation.  All Rights Reserved.

    The receipt of or possession of this program does not convey
    any rights to reproduce its contents or to manufacture,
    use, or sell anything that it may describe, in whole, or 
    in part, without specific written consent of Intel Corporation.


Revision History:
    $Date:: 5/12/00 2:29p                 $ Date and time of last check in
    $Revision:: 1                         $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/
// Copyright (c) Marvell International Ltd. All Rights Reserved.

#ifndef UTILSH_INC
#define UTILSH_INC

NTSTATUS
WTPUSB_FlushQueue(      
    IN PLIST_ENTRY  ListHead,
    IN PKSPIN_LOCK  Lock
);

NTSTATUS
WTPUSB_GetTransferObject(    // Entry 'G' Exit 'g'
    IN PDEVICE_OBJECT DeviceObject,
    IN OUT PWTPUSB_TRANSFER_OBJECT *ppTransferObject
);

NTSTATUS
WTPUSB_RecycleTransferObject(    // Entry 'R' Exit 'r'
    IN PWTPUSB_TRANSFER_OBJECT pTransferObject
);

NTSTATUS
WTPUSB_GetNumberOfTransferObjectsInQueue(
    IN PDEVICE_OBJECT DeviceObject,
    IN OUT PULONG pCount
);

NTSTATUS
WTPUSB_AllocateXTransferObjects(
    IN PDEVICE_OBJECT DeviceObject,
    IN ULONG Count
);

NTSTATUS
WTPUSB_FreeAllTransferObjects(
    IN PDEVICE_OBJECT DeviceObject
);

NTSTATUS
WTPUSB_AllocateTransferObject(   // Entry 'A' Exit 'a'
    IN PFDO_DEVICE_EXTENSION FDODeviceExtension,
    IN OUT PWTPUSB_TRANSFER_OBJECT *ppTransferObject
);

VOID
WTPUSB_CleanQueue(      
    IN PLIST_ENTRY  ListHead,
    IN PKSPIN_LOCK  Lock,
    IN NTSTATUS     completeWithStatus
);

#endif // UTILSH_INC
