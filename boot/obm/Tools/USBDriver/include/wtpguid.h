/*++

Copyright (c) 1997-1998  Microsoft Corporation

Module Name:

    $Workfile:: wtpguid.h                  $

Abstract:

 The below GUID is used to generate symbolic links to
  driver instances created from user mode

Environment:

    Kernel & user mode

Notes:

  THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
  KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
  PURPOSE.

  Copyright (c) 1997-1998 Microsoft Corporation.  All Rights Reserved.

Revision History:
    $Date:: 5/12/00 2:29p                 $ Date and time of last check in
    $Revision:: 1                         $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/
// Copyright (c) Marvell International Ltd. All Rights Reserved.

#ifndef WTPGUIDH_INC
#define WTPGUIDH_INC

#include <initguid.h>

// {99E5A8CD-EF5F-11d1-A35A-00A0C903BBA2}
DEFINE_GUID(GUID_CLASS_WTPUSB, 
0x99e5a8cd, 0xef5f, 0x11d1, 0xa3, 0x5a, 0x0, 0xa0, 0xc9, 0x3, 0xbb, 0xa2);

#endif 
