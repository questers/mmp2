/*++

Copyright (c) 1998-2000 Intel Corporation

Module Name:

    $Workfile:: dbghdr.c                  $

Abstract:

    Debug output logic 
    This entire module is a noop in the free build (except for IsWindows2000 function)

Environment:

    Kernel mode only

Notes:


    Copyright (c) 1998-2000 Intel Corporation.  All Rights Reserved.


Revision History:
    $Date:: 8/03/00 4:01p                 $ Date and time of last check in
    $Revision:: 43                        $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/




// pragma warning disabled for WDM warning C4514: "unreferenced inline function has been removed"  
#pragma warning( disable :4514)
#pragma warning( disable :4127)
#pragma warning( push, 3 )
#include <wdm.h>
#pragma warning( pop ) 
#include "wtpdbg.h"

// Defines for IsWindows2000
#define DBG_MAX_STR_LEN 256
#define NT5_VERSION_STRING L"5.0"
#define CURRENT_VERSION_REG_KEY_NAME L"CurrentVersion"

#ifdef ALLOC_PRAGMA
#pragma alloc_text (PAGE, DBGHDR_SetDebugLevel)
#pragma alloc_text (PAGE, DBGHDR_IsWindows2000)
#endif // ALLOC_PRAGMA

#if DBG
#ifdef ALLOC_PRAGMA
#pragma alloc_text (PAGE, DBGHDR_Win98WorkaroundGetRegPath)
#endif
#define DEBUG_LEVEL_STRING_NAME L"DebugLevel"

//  may be overridden  in registry in DBG builds only
// higher == more verbose, default is 1, 0 turns off all
//ULONG gDebugLevel = DBGMASK_DEFAULT ; 
ULONG gDebugLevel = DBGMASK_MAX ; 
ULONG gulTotalDriverBytes = 0;

void	(*gCustomDbgFunc)() = NULL;

// some stuff to facilitate timestamping
LARGE_INTEGER	gFirstSystemTime;

// These are private routines not exposed to any other module.
static NTSTATUS
DBGHDR_Win98WorkaroundGetRegPath(
             IN OUT PUNICODE_STRING psRegPath );

/*
#ifdef ALLOC_PRAGMA
#pragma alloc_text (INIT, DBGHDR_Win98WorkaroundGetRegPath)
#pragma alloc_text (INIT, DBGHDR_Init)
#endif
*/
#ifndef POSSIBLE_REGISTRY_PATH
#error POSSIBLE_REGISTRY_PATH must be defined in BTDBG.H before dbghdr.h is included!
#endif


// pragama warning enbalbed. Warning C4213: nonstandard extension used : cast on l-value
#pragma warning( disable :4213)

//----------------------------------------------------------------------
// 
// Memory Allocation Macros
//
PVOID
ExAllocatePoolWtp(
			   IN POOL_TYPE	PoolType,
			   IN SIZE_T NumberOfBytes
			   )
{
	PVOID pvBuffer = NULL;

	pvBuffer = ExAllocatePoolWithTag( PoolType, NumberOfBytes + sizeof(ULONG), WTP_POOL_TAG);

	if( NULL != pvBuffer )
	{
        *((PULONG)(pvBuffer) ) = NumberOfBytes + sizeof(ULONG);
        gulTotalDriverBytes += *((PULONG)(pvBuffer) );
        ((PCHAR)(pvBuffer)) += sizeof(ULONG);
	}
	return( pvBuffer );
}


VOID
ExFreePoolWtp(
			PVOID pvBuffer
		  )
{

	if( NULL != pvBuffer ) 
	{ 
        ((PCHAR)(pvBuffer)) -= sizeof(ULONG);
		gulTotalDriverBytes -= *((PULONG)(pvBuffer));
		*((PULONG)(pvBuffer)) = 0;
        ExFreePool( pvBuffer );
	}
}
// pragama warning enbalbed. Warning C4213: nonstandard extension used : cast on l-value
#pragma warning( default :4213)

void
DBGHDR_SetCustomDbgFunc(
	IN	void	(*pvCustomDbgFunc)(void)
	)
{
	gCustomDbgFunc = pvCustomDbgFunc;
}


VOID
DBGHDR_Init(
    IN      PUNICODE_STRING  incomingRegPath
    )
/*++

Routine Description:

    Obtain a Dword value from the registry


Arguments:

    RegPath  -- Pointer to a registry path

Return Value:

    None.
--*/
{
    NTSTATUS status = STATUS_SUCCESS;
    BOOLEAN bRes = FALSE;
	//needed cause incomingRegPath may not be null terminated
    UNICODE_STRING regPath;
    PUNICODE_STRING psRegPath = &regPath;


	// all timestamping is relative to driver load
	// unless overridden by the DBGMASK_TIMEOFDAYSTAMP flag (checked below) 
	KeQuerySystemTime(&gFirstSystemTime);
	
	// make room for UNICODE_NULL at the end
	regPath.MaximumLength = (USHORT)(incomingRegPath->MaximumLength + sizeof(UNICODE_NULL));
	regPath.Buffer = ExAllocatePoolWithTag(NonPagedPool, regPath.MaximumLength, WTP_POOL_TAG);
    if (NULL == regPath.Buffer)
        status = STATUS_INSUFFICIENT_RESOURCES;
    else
    {
        DBGHDR_KdPrint( DBGMASK_ERROR, ("BUILD DATE: %s, BUILD TIME: %s\n", 
										__DATE__, __TIME__));

        regPath.Length = incomingRegPath->Length;
        RtlZeroMemory(regPath.Buffer, regPath.MaximumLength);
        RtlMoveMemory(regPath.Buffer, incomingRegPath->Buffer, incomingRegPath->Length);
        
        // This has to be called with  DBGMASK_ERROR since DebugLevel is not yet set!
        DBGHDR_KdPrint( DBGMASK_TRACE,("Enter DbgHdr_GetRegistryDword() RegPath = %ws\n", 
            psRegPath->Buffer));
    } // end of else from check against regPath.Buffer

    if( NULL == psRegPath->Buffer || !NT_SUCCESS(status)) 
    {
        DBGHDR_ASSERT( NULL == psRegPath );
        if (NULL != regPath.Buffer)
            ExFreePool(regPath.Buffer);
        return;
    }

    // Are we running on windows 2000?
    status = DBGHDR_IsWindows2000(&bRes);
    if( NT_SUCCESS(status) ) 
    {
        if ( bRes == FALSE )
        {
            // No, must be Windows 98. Get the workaround Registry path.
            // Note: we will overwrite and lose the registry path that the system 
            //       passed in to us!
            status = DBGHDR_Win98WorkaroundGetRegPath( psRegPath );
        } // bRes == FALSE
        if ( ( bRes == TRUE ) || ( bRes == FALSE && NT_SUCCESS( status ) ) ) 
        {
            // The workaround path is available or we are on a Win 2K machine.
            // Get the DebugLevel from the Registry and set the gDebugLevel global var.
            status = DBGHDR_SetDebugLevel( psRegPath );

            // DebugLevel should have been set from here on out. So start using DBGMASK_TRACE.
            if ( NT_SUCCESS(status) ) 
            {
                // we have successfully set the variable.
                DBGHDR_KdPrint( DBGMASK_TRACE,("Exit DbgHdr_GetRegistryDWord() SUCCESS, \
                                        value = decimal %d 0x%x\n", gDebugLevel, gDebugLevel));
            } // If Success on SetDebugLevel
            else 
            {
                // Either we did not find the variable or the API call to read the reg failed.
                DBGHDR_KdPrintCond( DBGMASK_ERROR, (status == STATUS_INVALID_PARAMETER) ,
                    ("DbgHdr_GetRegistryDWord() STATUS_INVALID_PARAMETER\n"));
                DBGHDR_KdPrintCond( DBGMASK_ERROR, (status == STATUS_OBJECT_NAME_NOT_FOUND),
                    ("DbgHdr_GetRegistryDWord() STATUS_OBJECT_NAME_NOT_FOUND\n"));
            } // Error on SetDebugLevel.
        } // bRes == TRUE || bRes==FALSE && NTSUCCESS(STATUS)
        else 
        {
            // Windows 98 workaround failed.
            DBGHDR_KdPrint( DBGMASK_ERROR, ("Error returned from DBGHDR_Win98WorkaroundGetRegPath() %x\n", 
                                            status ));
        } // Failure on Win98Workaround.
    } // Windows2000 Success
    else
    {
        DBGHDR_KdPrint( DBGMASK_ERROR, ("Error returned from DBGHDR_IsWindows2000() %x\n", 
                                        status ));
    } // Failure on IsWindows2000

    if (NULL != regPath.Buffer)
        ExFreePool(regPath.Buffer);

    DBGHDR_KdPrint( DBGMASK_TRACE,("Exit DbgHdr_GetRegistryDword()\n"));

	// if the timeofday timestamping is selected so that all timestamps are relative
	// to the start of the day the driver is loaded on then adjust the gFirstSystemTime
	// variable to contain the absolute system time corresponding to midnight.
	if( gDebugLevel & DBGMASK_TIMEOFDAYSTAMP )
	{
		// convert gFirstSystemTime to a time_fields struct
		// zero out the hours, minutes, etc. fields
		// convert the time_fields struct to an absolute system time.
		// adjust the absolute system time for timezone

		TIME_FIELDS		tf;


		// expand the absolute system time to the time_fields structure...
		RtlTimeToTimeFields( &gFirstSystemTime, &tf );

		// change fields in the time_fields structure so it represents midnight
		tf.Hour = tf.Minute = tf.Second = tf.Milliseconds = 0;

		// convert the time_fields structure back into absolute system time format
		RtlTimeFieldsToTime( &tf, &gFirstSystemTime );

		// Finally, adjust for being in the Arizona time zone.
		// Add 7 hours worth of 100 nanosecond units to our time base.
		// 7 * 3600 * 10000000 = 252000000000
		gFirstSystemTime.QuadPart += 252000000000;

	}


} // DBGHDR_GetRegistryDword


NTSTATUS
DBGHDR_Win98WorkaroundGetRegPath(
        IN OUT PUNICODE_STRING psRegPath )
/*++

Routine Description:

    For Win 98, force the registry path to the known path. We need to fix this to
    get the right version of the path.

Arguments:
    PUNICODE_STRING psRegPath - The current Registry Path
                                This will be modified to reflect the new path.

Return Value:

    STATUS_SUCCESS - if all succeeds
    STATUS_INSUFFICIENT_RESOURCES - if not enough memory

--*/
{
    NTSTATUS status = STATUS_SUCCESS;
    PVOID pvTemp = NULL;

    DBGHDR_KdPrint( DBGMASK_TRACE,("Enter DBGHDR_Win98WorkaroundGetRegPath()\n "));
    DBGHDR_ASSERT( NULL != psRegPath );
    PAGED_CODE ();
    //
    // I don't really understand why but...
    // Win98 shows that RegsistryPath->Buffer = RFUSB.SYS (Ansi)
    // However, when it is copied to another unicode string, the copied version has a 
    // registry path to \services\class\USB\0003 - in unicode.
    // 
    // So, for now, let's just copy in what we know the registry key to be.
    // RH - Need to fix this!
    //
    pvTemp = ExAllocatePoolWithTag(NonPagedPool, sizeof(POSSIBLE_REGISTRY_PATH), WTP_POOL_TAG );
    if ( NULL != pvTemp )
    {
        if( psRegPath->MaximumLength < sizeof(POSSIBLE_REGISTRY_PATH) ) 
        {
            if( NULL != psRegPath->Buffer ) 
            {
                ExFreePool( psRegPath->Buffer );
            } // Non null RegPath->Buffer
            psRegPath->Buffer = pvTemp;
            psRegPath->MaximumLength = sizeof( POSSIBLE_REGISTRY_PATH );
        } // MaxLength < POSSIBLE_REGiSTry_PATH
        else 
        {
            ExFreePool(pvTemp);
        } 
        psRegPath->Length = sizeof( POSSIBLE_REGISTRY_PATH );
        RtlCopyMemory( psRegPath->Buffer, POSSIBLE_REGISTRY_PATH, psRegPath->Length );
        DBGHDR_KdPrint( DBGMASK_TRACE, ("RegPath = %ws\n", psRegPath->Buffer));
    } // Valid pvTemp
    else
    {
        status = STATUS_INSUFFICIENT_RESOURCES;
    } // NULL pvTemp
    DBGHDR_KdPrint( DBGMASK_TRACE,("Exit DBGHDR_Win98WorkaroundGetRegPath() returning %x\n",
                                status));
    return ( status );
} //DBGHDR_Win98WorkaroundGetRegPath


NTSTATUS
DBGHDR_SetDebugLevel(
                     IN PUNICODE_STRING psRegPath
                     )
/*++

Routine Description:

    Reads the registry for the possible place where we can acquire the DebugLevel
    var that has been set in the registry. This routine will set the global variable
    gDebugLevel to the value set in the registry. This will be used later on to output
    the DebugString.

Arguments:
    PUNICODE_STRING psRegPath - The current Registry Path

Return Value:

    STATUS_SUCCESS - if all succeeds
    STATUS_OBJECT_NAME_NOT_FOUND - if there is an error with the input RegistryPath
                                    or an error is returned from RtlQueryRegistryValues
    STATUS_INVALID_PARAMETER - if this value is returned from RtlQueryRegistryValues
                                or if Input parameter is NULL

--*/
{
    RTL_QUERY_REGISTRY_TABLE paramTable[2];
    NTSTATUS    status = STATUS_SUCCESS;

    DBGHDR_KdPrint( DBGMASK_TRACE,("Enter DBGHDR_GetDebugLevel()\n"));
    DBGHDR_ASSERT( NULL != psRegPath );
    PAGED_CODE ();

    if ( NULL != psRegPath ) 
    {
        // zero'd second table terminates parms
        RtlZeroMemory(paramTable, sizeof(paramTable));
        
        paramTable[0].Flags = RTL_QUERY_REGISTRY_DIRECT;
        paramTable[0].Name = DEBUG_LEVEL_STRING_NAME;
        paramTable[0].EntryContext = &gDebugLevel;
        paramTable[0].DefaultType = REG_DWORD;
        paramTable[0].DefaultData = &gDebugLevel;
        paramTable[0].DefaultLength = sizeof(REG_DWORD);

        status = RtlQueryRegistryValues( RTL_REGISTRY_ABSOLUTE | RTL_REGISTRY_OPTIONAL,
                                        psRegPath->Buffer, paramTable, NULL, NULL);
        DBGHDR_KdPrint( DBGMASK_TRACE, ("Getting Debug Level from : %ws\n DebugLevel = %x\n", 
                                        psRegPath->Buffer, gDebugLevel));
    } // Non-null RegPath
    else 
    {
        status = STATUS_INVALID_PARAMETER;
    } // RegPath is NULL

    DBGHDR_KdPrint( DBGMASK_TRACE, ("Exit DBGHDR_GetDebugLevel() returning %x\n",
                                status));
    return( status );
} //DBGHDR_SetDebugLevel

#else
VOID 
DBGHDR_Init(
            IN PUNICODE_STRING  psRegPath
            ) 
{ 
	UNREFERENCED_PARAMETER( psRegPath );
    return; 
}

NTSTATUS 
DBGHDR_SetDebugLevel(
                     IN PUNICODE_STRING psRegPath
                     ) 
{ 
	UNREFERENCED_PARAMETER( psRegPath );
    return (STATUS_SUCCESS); 
}


PVOID
ExAllocatePoolWtp( 
				 IN POOL_TYPE Pool, 
				 IN ULONG Bytes 
				 ) 
{
	return( ExAllocatePoolWithTag( Pool, Bytes, WTP_POOL_TAG ) );
}

VOID
ExFreePoolWtp( 
			 IN PVOID pvBuffer 
			 ) 
{
	ExFreePool( pvBuffer );
}

#endif // end , if DBG

NTSTATUS
DBGHDR_IsWindows2000( 
                     OUT BOOLEAN *pbRes) 
/*++

Routine Description:

    Checks the registry to find the current version of the Operating System.


Arguments:
    BOOLEAN bRes - TRUE if Windows 2000
                    FALSE if not.

Return Value:

    STATUS_SUCCESS - If no failures
    STATUS_INSUFFICIENT_RESOURCES - if errors alloc'ing memory.
    STATUS_INVALID_PARAMETER - Returned from RtlQueryRegistryValues

--*/
{
    NTSTATUS status = STATUS_SUCCESS;
    PAGED_CODE ();
    DBGHDR_KdPrint( DBGMASK_TRACE,("Enter DBGHDR_IsWindows2000()\n"));

	*pbRes = IoIsWdmVersionAvailable( WDM_MAJORVERSION, WDM_MINORVERSION ); // challa commented
    //*pbRes = RtlIsNtDdiVersionAvailable(NTDDI_WIN2K); // challa


    DBGHDR_KdPrint( DBGMASK_TRACE,("Exit DBGHDR_IsWindows2000() returning %s, status = %x\n",
                        *pbRes == TRUE ? "TRUE":"FALSE", status));
    return ( status );
} // DBGHDR_IsWindows2000
