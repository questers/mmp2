/*++

Copyright (c) 1998-1999 Intel Corporation

Module Name:

    $Workfile:: wmi.c                     $

Abstract:

    Handles all main entry points of driver as described in
    DriverEntry()

Environment:

    Kernel mode only

Notes:

    Copyright (c) 1998-1999 Intel Corporation.  All Rights Reserved.

    The receipt of or possession of this program does not convey
    any rights to reproduce its contents or to manufacture,
    use, or sell anything that it may describe, in whole, or 
    in part, without specific written consent of Intel Corporation.


Revision History:
    $Date:: 5/18/00 4:53p                 $ Date and time of last check in
    $Revision:: 2                         $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/

#include <wdm.h>
#include <wmistr.h>
#include <usbdi.h>
#include <usbdlib.h>
#include <initguid.h>
#include "wtpdbg.h"
#include "wtpdfu.h"


// this is the identifier for the Managed Object Format resource
#define WTPUSB_MofResourceName		L"MofResource"
#define WTPUSB_InstNamePerf			L"Marvell WTPTP Driver Performance Data"
#define WTPUSB_InstNameConfig		L"Marvell WTPTP Driver Configuration Data"


// {01D53C0A-9873-4068-8433-7F2CBE2F6C7C}
DEFINE_GUID(GUID_WTPUSB_PERF_WMI_IRP_BLOCK, 
0x1d53c0a, 0x9873, 0x4068, 0x84, 0x33, 0x7f, 0x2c, 0xbe, 0x2f, 0x6c, 0x7c);


// {BC4E9244-36D3-403a-B8DA-220508D5F1B1}
DEFINE_GUID(GUID_WTPUSB_CONFIG_WMI_IRP_BLOCK, 
0xbc4e9244, 0x36d3, 0x403a, 0xb8, 0xda, 0x22, 0x5, 0x8, 0xd5, 0xf1, 0xb1);


// this definition is needed because all of the counted string definitions
// in the ddk header files include a MaxLength field, which is not what the
// wmi server wants when we are returning "counted unicode strings" (see
// wmi documentation).

typedef struct CUnicodeString_s
{
	USHORT	usLength;
	WCHAR	pwcStr[];
} CUNICODESTR, *PCUNICODESTR;



#ifdef ALLOC_PRAGMA
#pragma alloc_text (PAGE, WTPUSB_InitializeGuidList)
#endif

VOID
WTPUSB_InitializeGuidList(
    IN OUT PFDO_DEVICE_EXTENSION FDODeviceExtension
    )
/*++

Routine Description:

    Initialize the array of GUIDS with our WMI data.
    This code is pageable.

Arguments:

    FDODeviceExtension - a pointer to our device extension

Return Value:

    None

--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;

    DBGHDR_KdPrint ((DBGMASK_TRACE | DBGMASK_WMI_TRACE), ("WTPUSB_InitializeGuidList(): Enter\n"));
    PAGED_CODE ();
    DBGHDR_ASSERT(NULL != FDODeviceExtension);
#ifdef SUPPORTWMI
    FDODeviceExtension->WmiGuidArray[WTPUSB_PERF_WMI_IRP_BLOCK].Guid = &GUID_WTPUSB_PERF_WMI_IRP_BLOCK;
    FDODeviceExtension->WmiGuidArray[WTPUSB_PERF_WMI_IRP_BLOCK].InstanceCount = 1;
    FDODeviceExtension->WmiGuidArray[WTPUSB_CONFIG_WMI_IRP_BLOCK].Guid = &GUID_WTPUSB_CONFIG_WMI_IRP_BLOCK;
    FDODeviceExtension->WmiGuidArray[WTPUSB_CONFIG_WMI_IRP_BLOCK].InstanceCount = 1;
#endif
    DBGHDR_KdPrint (DBGMASK_TRACE | DBGMASK_WMI_TRACE, ("WTPUSB_InitializeGuidList(): Exit with ntStatus:%X\n", ntStatus));
}

/*
NTSTATUS
WTPUSB_QueryWmiDataBlock(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP pIrp,
    IN ULONG GuidIndex, 
    IN ULONG InstanceIndex,
    IN ULONG InstanceCount,
    IN OUT PULONG InstanceLengthArray,
    IN ULONG OutBufferSize,
    OUT PUCHAR pBuffer
    )
/*++

Routine Description:

  This routine is a callback into the driver to query for the contents of
  a data block. When the driver has finished filling the data block it
  must call ClassWmiCompleteRequest to complete the irp. The driver can
  return STATUS_PENDING if the irp cannot be completed immediately.
  This routine is pageable.

Arguments:

    pDevObj     -  the device whose data block is being queried

    pIrp        - the Irp that made this request

    GuidIndex   - the index into the list of guids provided when the device registered

    InstanceIndex - the index that denotes which instance of the data block is being queried.

    InstanceCount - the number of instnaces expected to be returned for the data block.

    InstanceLengthArray - a pointer to an array of ULONG that returns the lengths of each 
                            instance of the data block. If this is NULL then there was not 
                            enough space in the output buffer to fufill the request
                            so the irp should be completed with the buffer needed.        

    BufferAvail - has the maximum size available to write the data block.

    pBuffer     - filled with the returned data block

Return Value:

    NtStatus value

--*/
/*
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    ULONG size = 0;
    PFDO_DEVICE_EXTENSION FDODeviceExtension = NULL;
   
    PAGED_CODE();
    DBGHDR_KdPrint (DBGMASK_TRACE | DBGMASK_WMI_TRACE, ("WTPUSB_QueryWmiDataBlock(): Enter\n"));

    DBGHDR_ASSERT(NULL != DeviceObject);
    DBGHDR_ASSERT(NULL != pIrp);

    FDODeviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT(NULL != FDODeviceExtension);
    
    switch (GuidIndex) 
    {   
    case WTPUSB_PERF_WMI_IRP_BLOCK:
        size = sizeof(WTPUSB_PERF_WMI_INFO);
        if (OutBufferSize < size) {
            ntStatus = STATUS_BUFFER_TOO_SMALL;
            break;
        }
        *InstanceLengthArray = size;
        RtlZeroMemory(pBuffer, size);
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->ACLInstanceCount = FDODeviceExtension->perfInfo.ACLInstanceCount;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->SCOInstanceCount = FDODeviceExtension->perfInfo.SCOInstanceCount;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->ControlInstanceCount = FDODeviceExtension->perfInfo.ControlInstanceCount;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->numFreeTransferObjects = FDODeviceExtension->perfInfo.numFreeTransferObjects;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->numTotalTransferObjects = FDODeviceExtension->perfInfo.numTotalTransferObjects;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->numPendingACLIrps = FDODeviceExtension->perfInfo.numPendingACLIrps;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->numPendingSCOInIrps = FDODeviceExtension->perfInfo.numPendingSCOInIrps;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->numPendingSCOOutIrps = FDODeviceExtension->perfInfo.numPendingSCOOutIrps;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->numPendingCMDIrps = FDODeviceExtension->perfInfo.numPendingCMDIrps;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->totalIrpsReceivedForACL = FDODeviceExtension->perfInfo.totalIrpsReceivedForACL;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->totalIrpsReceivedForSCO = FDODeviceExtension->perfInfo.totalIrpsReceivedForSCO;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->totalIrpsReceivedForCMD = FDODeviceExtension->perfInfo.totalIrpsReceivedForCMD;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->numWaitingForInStream = FDODeviceExtension->perfInfo.numWaitingForInStream;
        ((PWTPUSB_PERF_WMI_INFO)pBuffer)->numWaitingForOutStream = FDODeviceExtension->perfInfo.numWaitingForOutStream;
        break;

    case WTPUSB_CONFIG_WMI_IRP_BLOCK:
        size = sizeof(WTPUSB_CONFIG_WMI_INFO);
        if (OutBufferSize < size) {
            ntStatus = STATUS_BUFFER_TOO_SMALL;
            break;
        }
        *InstanceLengthArray = size;
        RtlZeroMemory(pBuffer, size);
        ((PWTPUSB_CONFIG_WMI_INFO)pBuffer)->pipeNumACLIn = FDODeviceExtension->configInfo.pipeNumACLIn;
        ((PWTPUSB_CONFIG_WMI_INFO)pBuffer)->pipeNumACLOut = FDODeviceExtension->configInfo.pipeNumACLOut;
        ((PWTPUSB_CONFIG_WMI_INFO)pBuffer)->pipeNumSCOIn = FDODeviceExtension->configInfo.pipeNumSCOIn;
        ((PWTPUSB_CONFIG_WMI_INFO)pBuffer)->pipeNumSCOOut = FDODeviceExtension->configInfo.pipeNumSCOOut;
        ((PWTPUSB_CONFIG_WMI_INFO)pBuffer)->pipeNumCMDIn = FDODeviceExtension->configInfo.pipeNumCMDIn;
        break;
        
    default:
        ntStatus = STATUS_WMI_GUID_NOT_FOUND;
        size = 0;
        break;
    }
     
    ntStatus = WmiCompleteRequest( 
        DeviceObject,
        pIrp,
        ntStatus, 
        size, 
        IO_NO_INCREMENT);

    DBGHDR_KdPrint (DBGMASK_TRACE | DBGMASK_WMI_TRACE, ("(): Exit with ntStatus:%X\n", ntStatus));
    
    return ntStatus;
}
*/

NTSTATUS
WTPUSB_ProcessFDOWMIIrp (
    IN PDEVICE_OBJECT			DeviceObject,
    IN PIRP						Irp,
    IN PIO_STACK_LOCATION		irpStack,
    IN PFDO_DEVICE_EXTENSION	FDODeviceExtension
)
{
    NTSTATUS	ntStatus		= STATUS_SUCCESS;
    PVOID		pCounterStruct	= NULL;
    ULONG		ulSize			= 0;

    PAGED_CODE ();

    //dbgT(DBG_DEVEXT(DeviceObject), 'N', oldIrql);
    DBGHDR_KdPrint(DBGMASK_WMI_TRACE, ("WTPUSB_ProcessFDOWMIIrp() Enter with Irp code:%x\n", irpStack->MinorFunction));

    WTPUSB_IncrementIoCount(DeviceObject);
    
    switch (irpStack->MinorFunction)
    {
    case IRP_MN_REGINFO:
        //
        // Check to see if this is an update or a request
        //
        if (WMIUPDATE == (const int)(irpStack->Parameters.WMI.DataPath))
        {
            DBGHDR_KdPrint(DBGMASK_WMI_TRACE, ("IRP_MN_REGINFO - WMI wants to update\n"));
        }
        else if (WMIREGISTER == (const int)(irpStack->Parameters.WMI.DataPath))
        {
			PWMIREGINFO WmiRegInfo;

            ULONG		regInfoSize		= 0;
            GUID		guidPerf		= GUID_WTPUSB_PERF_WMI_IRP_BLOCK;
            GUID		guidConfig		= GUID_WTPUSB_CONFIG_WMI_IRP_BLOCK;

			
			// regInfoPtr is used to format counted unicode strings
			// into the buffer being returned to the caller.
			// the ".address" field is used to initially point to the
			// buffer, and also to move along the buffer as we add
			// more counted unicode string to it.
			// the pcusStr.usLength and pcusStr.pwcStr fields are used
			// to initialize the length and string fields of the counted
			// unicode string.
			union 
			{
				PUCHAR			address;		// used to increment regInfoPtr 
				PCUNICODESTR	pcusStr;
			} regInfoPtr;


            
            DBGHDR_KdPrint(DBGMASK_WMI_TRACE, ("IRP_MN_REGINFO - WMI wants to get info\n"));
            ntStatus = STATUS_SUCCESS;
            
            //
            // Set buffer size of WMIREGINFO + extra registration stuff
            //

			// note: sizes of the resource and instance name wide strings are calculated as: 
			//	the sizeof the wide character array - 
			//	the sizeof the wide character null termination + 
			//	the sizeof the "length" field that precedes the string.

			// note: sizes of the registry path strings are calculated as: 
			//	the sizeof the wide character array in the unicode string + 
			//	the sizeof the "length" field that precedes the string.
			// (eg. there is no null terminating wide character at the end to adjust for)


            regInfoSize =
                sizeof(WMIREGINFO) +
                sizeof(WMIREGGUID) +	// guid for PERF
				sizeof(WMIREGGUID) +	// guid for CONFIG
                FDODeviceExtension->registryPath->Length	  + sizeof(USHORT) +	// unicode
                sizeof(WTPUSB_MofResourceName) - sizeof(WCHAR) + sizeof(USHORT) +	// wstr
                sizeof(WTPUSB_InstNamePerf)    - sizeof(WCHAR) + sizeof(USHORT) +	// wstr
                sizeof(WTPUSB_InstNameConfig)  - sizeof(WCHAR) + sizeof(USHORT) ;	// wstr
            
            //
            // Ensure the supplied data buffer is large enough for the registration
            // information.
            //
            if (irpStack->Parameters.WMI.BufferSize < regInfoSize) {
                //
                // Supplied buffer is too small.  Inform WMI of the required size,
                // and return success as per WMI protocol.
                //
                *((PULONG)(irpStack->Parameters.WMI.Buffer)) = regInfoSize;
                ntStatus = STATUS_BUFFER_TOO_SMALL;
            }
            
            if (NT_SUCCESS(ntStatus))
            {
                //
                // Initialize the WMIREGINFO structure.
                //
                WmiRegInfo = irpStack->Parameters.WMI.Buffer;
                RtlZeroMemory(WmiRegInfo, regInfoSize);

                WmiRegInfo->BufferSize     = regInfoSize;
                WmiRegInfo->GuidCount      = 2;    // [two WMIREGGUIDs to register (PERF & CONFIG)]
                WmiRegInfo->NextWmiRegInfo = 0;    // [only one WMIREGINFO structure]
                
                //
                // Initialize the first WMIREGGUID structure (currently, PERF).
                // Set guid count & write an array of WMIREGGUID in WMIREGINFO
                //
                RtlCopyMemory(&WmiRegInfo->WmiRegGuid[0].Guid, &guidPerf, sizeof(GUID));
                WmiRegInfo->WmiRegGuid[0].Flags = WMIREG_FLAG_INSTANCE_LIST;
                WmiRegInfo->WmiRegGuid[0].InstanceCount = 1;
                
                //
                // Initialize the second WMIREGGUID structure (currently, CONFIG).
                // Set guid count & write an array of WMIREGGUID in WMIREGINFO
                //
                RtlCopyMemory(&WmiRegInfo->WmiRegGuid[1].Guid, &guidConfig, sizeof(GUID));
                WmiRegInfo->WmiRegGuid[1].Flags = WMIREG_FLAG_INSTANCE_LIST;
                WmiRegInfo->WmiRegGuid[1].InstanceCount = 1;
                

				// all of our counted unicode strings will begin here....
                regInfoPtr.address = (PUCHAR)&WmiRegInfo->WmiRegGuid[2];
                
                
				//
                // Initialize the MOF resource name information: offset, length and data
                //

				// now that we know what the offset of the MOF resource name string is,
				// store that offset into the WmiRegInfo structure....
                WmiRegInfo->MofResourceName = (ULONG)(regInfoPtr.address - (PUCHAR)WmiRegInfo); // yields offset


				// copy the MofResourceName length and text to the 
				// counted unicode string buffer pointed to by regInfoPtr.
                regInfoPtr.pcusStr->usLength = sizeof(WTPUSB_MofResourceName) - sizeof(WCHAR);	// unicode.length
                RtlCopyMemory( 
					regInfoPtr.pcusStr->pwcStr, 
					WTPUSB_MofResourceName, 
					sizeof(WTPUSB_MofResourceName) - sizeof(WCHAR) 
					);
                
				// update our pointer into the buffer to point to where we'll put the next string
				// see note (above) about calculating lengths of our counted unicode strings...
                regInfoPtr.address += sizeof(USHORT) + sizeof(WTPUSB_MofResourceName) - sizeof(WCHAR);
                
                
				//
                // Set registry path
                //

				// now that we know what the offset of the registry path string is,
				// store that offset into the WmiRegInfo structure....
                WmiRegInfo->RegistryPath = (ULONG)(regInfoPtr.address - (PUCHAR)WmiRegInfo);	// yields offset

				// copy the RegistryPath length and text to the 
				// counted unicode string buffer pointed to by regInfoPtr.
                regInfoPtr.pcusStr->usLength = FDODeviceExtension->registryPath->Length;		// unicode.length
                RtlCopyMemory(
					regInfoPtr.pcusStr->pwcStr,
                    FDODeviceExtension->registryPath->Buffer,
                    FDODeviceExtension->registryPath->Length
					);

				// update our pointer into the buffer to point to where we'll put the next string
				// see note (above) about calculating lengths from real unicode strings
                regInfoPtr.address += sizeof(USHORT) + FDODeviceExtension->registryPath->Length;
                

				//
				// append the instance name for the perf guid
				//

				// now that we know what the offset of the perf instance name list string is,
				// store that offset into the WmiRegGuid structure....
#if 0
// bpc: FIXME. InstanceNameList not defined in the headers anywhere!
				WmiRegInfo->WmiRegGuid[0].InstanceNameList = (ULONG)(regInfoPtr.address - (PUCHAR)WmiRegInfo);	// yeilds offset
#endif
				// copy the InstNamePerf length and text to the 
				// counted unicode string buffer pointed to by regInfoPtr.
                regInfoPtr.pcusStr->usLength = sizeof(WTPUSB_InstNamePerf) - sizeof(WCHAR);	// unicode.length
                RtlCopyMemory(
					regInfoPtr.pcusStr->pwcStr,
                    WTPUSB_InstNamePerf,
                    sizeof(WTPUSB_InstNamePerf) - sizeof(WCHAR)
					);
                
				// update our pointer into the buffer to point to where we'll put the next string
				// see note (above) about calculating lengths of our counted unicode strings...
                regInfoPtr.address += sizeof(USHORT) + sizeof(WTPUSB_InstNamePerf) - sizeof(WCHAR);
				

				//
				// add in the instance name for the config guid
				//

				// now that we know what the offset of the config instance name list string is,
				// store that offset into the WmiRegGuid structure....
#if 0
// bpc: FIXME! InstanceNameList is not defined in the headers anywhere!
				WmiRegInfo->WmiRegGuid[1].InstanceNameList = (ULONG)(regInfoPtr.address - (PUCHAR)WmiRegInfo);	// yeilds offset
#endif
				// copy the InstNameConfig length and text to the 
				// counted unicode string buffer pointed to by regInfoPtr.
                regInfoPtr.pcusStr->usLength = sizeof(WTPUSB_InstNameConfig) - sizeof(WCHAR);	// unicode.length
                RtlCopyMemory(
					regInfoPtr.pcusStr->pwcStr,
                    WTPUSB_InstNameConfig,
                    sizeof(WTPUSB_InstNameConfig) - sizeof(WCHAR)
					);
                
                
                Irp->IoStatus.Information = regInfoSize;
            } // End of success on having a large enough buffer to fill into

        } // End of conditional on WMI_REGISTER
        break;

    case IRP_MN_QUERY_ALL_DATA:
        //
        // Check the GUID that this is for & set local variables to point to the proper structure
        //
        ulSize = sizeof(GUID);
        if (ulSize == RtlCompareMemory(irpStack->Parameters.WMI.DataPath, 
            &GUID_WTPUSB_PERF_WMI_IRP_BLOCK, 
            ulSize))
        {
            ulSize = sizeof(WTPUSB_PERF_WMI_INFO);
            pCounterStruct = &FDODeviceExtension->perfInfo;
        }
		else
        if (ulSize == RtlCompareMemory(irpStack->Parameters.WMI.DataPath, 
            &GUID_WTPUSB_CONFIG_WMI_IRP_BLOCK, 
            sizeof(GUID)))
        {
            ulSize = sizeof(WTPUSB_CONFIG_WMI_INFO);
            pCounterStruct = &FDODeviceExtension->configInfo;
        }
        else
        {
            Irp->IoStatus.Status = STATUS_WMI_GUID_NOT_FOUND;
            ntStatus = STATUS_WMI_GUID_NOT_FOUND;
        }
        
        if (NT_SUCCESS(ntStatus))
        {
            //
            // If the buffersize is too big, complete the IRP with an error
            //
            if (irpStack->Parameters.WMI.BufferSize < sizeof(WNODE_TOO_SMALL))
            {
                // Fail the IRP
                ntStatus = STATUS_BUFFER_TOO_SMALL;
            }
            else if ((sizeof(WNODE_ALL_DATA)+ulSize) > irpStack->Parameters.WMI.BufferSize)
            {
                PWNODE_TOO_SMALL pTooSmall;
                pTooSmall = (PWNODE_TOO_SMALL)irpStack->Parameters.WMI.Buffer;
                // RH - Additional work to fill this in?
                pTooSmall->SizeNeeded = ulSize + sizeof(WNODE_ALL_DATA);
                ntStatus = STATUS_BUFFER_TOO_SMALL;
            }
            else
            {
                PWNODE_ALL_DATA pAllData = (PWNODE_ALL_DATA)irpStack->Parameters.WMI.Buffer;

                // Set the sizes
                pAllData->WnodeHeader.BufferSize = ulSize + sizeof(WNODE_ALL_DATA);
                KeQuerySystemTime(&pAllData->WnodeHeader.TimeStamp);
//                pAllData->InstanceCount = MAX_WMI_GUIDS;
                pAllData->InstanceCount = 1;
                pAllData->WnodeHeader.Flags |= WNODE_FLAG_FIXED_INSTANCE_SIZE;
                pAllData->FixedInstanceSize = ulSize;

                //
                // Copy in the instance data
                //
                if (0 == pAllData->DataBlockOffset)
                    pAllData->DataBlockOffset = sizeof(struct _WNODE_HEADER) + (4 * sizeof(ULONG));

                pAllData->OffsetInstanceNameOffsets = 0;
                RtlCopyMemory(((CHAR*)pAllData)+pAllData->DataBlockOffset, 
                    pCounterStruct, 
                    ulSize);

                Irp->IoStatus.Information = pAllData->WnodeHeader.BufferSize; 
                Irp->IoStatus.Status = STATUS_SUCCESS;
                ntStatus = STATUS_SUCCESS;

                
            } // end of else from buffer size from (ulSize > Parameters.WMI.BufferSize)
        } // end of success finding a GUID 

        break;
    case IRP_MN_QUERY_SINGLE_INSTANCE:
        /*
        //
        // Check the GUID that this is for & set local variables to point to the proper structure
        //
        ulSize = sizeof(GUID);
        if (ulSize == RtlCompareMemory(irpStack->Parameters.WMI.DataPath, 
            &GUID_WTPUSB_PERF_WMI_IRP_BLOCK, 
            ulSize))
        {
            ulSize = sizeof(WTPUSB_PERF_WMI_INFO);
            pCounterStruct = &FDODeviceExtension->perfInfo;
        }
        else
        {
            Irp->IoStatus.Status = STATUS_WMI_GUID_NOT_FOUND;
            ntStatus = STATUS_WMI_GUID_NOT_FOUND;
        }
        
        if (NT_SUCCESS(ntStatus))
        {
            //
            // If the buffersize is too big, complete the IRP with an error
            //
            if (ulSize < sizeof(WNODE_TOO_SMALL))
            {
                // Fail the IRP
                Irp->IoStatus.Status = STATUS_BUFFER_TOO_SMALL;
                ntStatus = STATUS_BUFFER_TOO_SMALL;
            }
            else if (ulSize < irpStack->Parameters.WMI.BufferSize)
            {
                PWNODE_TOO_SMALL pTooSmall;
                pTooSmall = (PWNODE_TOO_SMALL)irpStack->Parameters.WMI.Buffer;
                // RH - Additional work to fill this in?
                pTooSmall->SizeNeeded = ulSize;
            }
            else
            {
                PWNODE_SINGLE_INSTANCE pInstanceStruct = (PWNODE_SINGLE_INSTANCE)irpStack->Parameters.WMI.Buffer;

                // Do not Zero the memory! Leave data block offset

                // Set the sizes
                pInstanceStruct->WnodeHeader.BufferSize = ulSize + sizeof(WNODE_SINGLE_INSTANCE);
                pInstanceStruct->SizeDataBlock = ulSize;

                //
                // Copy in the instance data
                //
                RtlCopyMemory(pInstanceStruct->VariableData, pCounterStruct, ulSize);

                Irp->IoStatus.Information = ulSize; // RH - Correct?
                Irp->IoStatus.Status = STATUS_SUCCESS;
                ntStatus = STATUS_SUCCESS;

                
            } // end of else from buffer size from (ulSize > Parameters.WMI.BufferSize)
        } // end of success finding a GUID 

        break;
*/
    case IRP_MN_EXECUTE_METHOD:
    case IRP_MN_CHANGE_SINGLE_INSTANCE:
    case IRP_MN_CHANGE_SINGLE_ITEM:
    case IRP_MN_ENABLE_EVENTS:
    case IRP_MN_DISABLE_EVENTS:
    case IRP_MN_ENABLE_COLLECTION:
    case IRP_MN_DISABLE_COLLECTION:
        {
            ntStatus = STATUS_INVALID_DEVICE_REQUEST;
        }
        
    default:
        {
            ntStatus = STATUS_INVALID_DEVICE_REQUEST;
        }
    } // end of switch

    //
    // Complete the request
    //
    Irp->IoStatus.Status = ntStatus;
    IoCompleteRequest(Irp, IO_NO_INCREMENT);
    WTPUSB_DecrementIoCount(DeviceObject);
    
    DBGHDR_KdPrint(DBGMASK_WMI_TRACE, ("WTPUSB_ProcessFDOWMIIrp() Exit with status %X\n", ntStatus));    
    return ntStatus;
}


