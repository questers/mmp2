/*++

Copyright (c) 1998-1999 Intel Corporation

Module Name:

    $Workfile:: usbspec.c                 $

Abstract:

    Handles all the USB Specific (USBSPEC) requests for the WTPUSB
    driver.

Environment:

    Kernel mode only

Notes:

    Copyright (c) 1998-1999 Intel Corporation.  All Rights Reserved.

    The receipt of or possession of this program does not convey
    any rights to reproduce its contents or to manufacture,
    use, or sell anything that it may describe, in whole, or 
    in part, without specific written consent of Intel Corporation.


Revision History:
    $Date:: 8/24/00 11:42a                $ Date and time of last check in
    $Revision:: 4                         $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/
// Copyright (c) Marvell International Ltd. All Rights Reserved.

// use this manifest constant to deactivate bluetooth specific areas.
// once this driver has been debugged, all of the deactivated areas
// can be permanently deleted.

#include <wdm.h>
#include <usbdi.h>
#include <usbdlib.h>
#include <wmistr.h>
#include "wtpdbg.h"
#include "wtpdfu.h"

#ifdef ALLOC_PRAGMA
#pragma alloc_text (PAGE, WTPUSB_ConfigureDevice)
#pragma alloc_text (PAGE, WTPUSB_SelectInterface)
#pragma alloc_text (PAGE, WTPUSB_AbortPipes)
#pragma alloc_text (PAGE, WTPUSB_SelectIsochInterface)
#pragma alloc_text (PAGE, WTPUSB_SelectDFUInterface)
#pragma alloc_text (PAGE, WTPUSB_CallUSBD)
#endif // ALLOC_PRAGMA

NTSTATUS
WTPUSB_CallUSBD(
    IN PDEVICE_OBJECT DeviceObject,
    IN PURB Urb,
    IN OPTIONAL ULONG ulControlCode
)
/*++

Routine Description:

    Passes an URB to USBD 
    The client device driver passes USB request block (URB) structures 
    to the class driver as a parameter in an IRP with Irp->MajorFunction
    set to IRP_MJ_INTERNAL_DEVICE_CONTROL and the next IRP stack location 
    Parameters.DeviceIoControl.IoControlCode field set to 
    the ulControlCode. 

    This code is non-pageable

Arguments:

    DeviceObject - pointer to the FDO submitting the call

    Urb - pointer to an already-formatted Urb request block

    ulControlCode - ulong indicating the control code in case the caller needs to specify it.

Return Value:

    STATUS_SUCCESS if successful,
    STATUS_UNSUCCESSFUL otherwise

--*/
{
    NTSTATUS            ntStatus = STATUS_SUCCESS;
    PFDO_DEVICE_EXTENSION FDODeviceExtension = NULL;
    PIRP                irp = NULL;
    KEVENT              event;
    IO_STATUS_BLOCK     ioStatus;
    PIO_STACK_LOCATION  nextStack = NULL;
    LARGE_INTEGER       timeout;
        
    PAGED_CODE();
    DBGHDR_KdPrint(DBGMASK_USB_TRACE,("WTPUSB_CallUSBD() Enter\n"));
    DBGHDR_ASSERT(NULL != DeviceObject);
    // Because the caller can specify the control code, it is okay if the urb is NULL

    FDODeviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT(NULL != FDODeviceExtension);

    //
    // issue a synchronous request
    //
    KeInitializeEvent(&event, NotificationEvent, FALSE);

    irp = IoBuildDeviceIoControlRequest(
        ulControlCode,
        FDODeviceExtension->topOfStackDeviceObject, //Points to the next-lower driver's device object
        NULL, // optional input bufer; none needed here
        0,    // input buffer len if used
        NULL, // optional output bufer; none needed here
        0,    // output buffer len if used
        TRUE, // If InternalDeviceControl is TRUE the target driver's Dispatch
        //  outine for IRP_MJ_INTERNAL_DEVICE_CONTROL or IRP_MJ_SCSI 
        // is called; otherwise, the Dispatch routine for 
        // IRP_MJ_DEVICE_CONTROL is called.
        &event,     // event to be signalled on completion
        &ioStatus);  // Specifies an I/O status block to be set when the request is completed the lower driver. 
    
    if (NULL != irp)
    {
        // Set a completion routine so it can signal our event when
        //  the next lower driver is done with the Irp
        IoSetCompletionRoutine(irp,
            WTPUSB_CompleteIrp,
            &event,  // pass the event as Context to completion routine
            TRUE,    // invoke on success
            TRUE,    // invoke on error
            TRUE);   // invoke on cancellation of the Irp
    
        //
        // Call the class driver to perform the operation.  If the returned status
        // is PENDING, wait for the request to complete.
        //
        nextStack = IoGetNextIrpStackLocation(irp);
        DBGHDR_ASSERT(NULL != nextStack);
        
        //
        // pass the URB to the USB driver stack
        //
        nextStack->Parameters.Others.Argument1 = Urb;
        KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
        ntStatus = IoCallDriver(FDODeviceExtension->topOfStackDeviceObject, irp);
                
        if (STATUS_PENDING == ntStatus) 
        {
            // We want to set a timeout because if the call CallUSBD() hangs (which happens sometimes), 
            // the irpSyncEvent lock that this thread holds will cause all other downloads to hang.
            // To avoid and isolate the error to that particular device, we set a timeout (to a quite 
            // high value of 3 seconds). The target that fails to download just has to be detached and
            // attached.
            timeout.QuadPart = -10000 * 1000; // 1000 milliseconds 
            // timeout.QuadPart *= 300; // 5 Minutes	// bpc: restore the timeout to a one second, but allow retries.
            ntStatus = KeWaitForSingleObject(
                &event,
                Executive,
                KernelMode,
                FALSE,
                &timeout);

            if (STATUS_TIMEOUT == ntStatus)
            {
                // Mark the irp for cancellation. We have set up a 
                // completion routine, which returns STATUS_MORE_PROCESSING_REQUIRED
                // therefore the IRP won't be freed until we call IoCompleteRequest()
                // We can therefore be safe in calling IoCancelIrp
                IoCancelIrp(irp); 
                KeWaitForSingleObject( // wait for the event from the completion routine
                    &event,
                    Executive,
                    KernelMode,
                    FALSE,
                    NULL);
                ioStatus.Status = ntStatus;
                ioStatus.Information = 0;
            }
        } 
        else 
        {
            ioStatus.Status = ntStatus;
            ioStatus.Information = 0;
        }
        KeClearEvent(&event); // clear the event set by the completion routine
        
		IoCompleteRequest(irp, IO_NO_INCREMENT);	// bpc: fixme: does this need to be done if the irp was cancelled?
        KeWaitForSingleObject( // wait for the I/O manger to signal completion
                    &event,
                    Executive,
                    KernelMode,
                    FALSE,
                    NULL);
        KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 
        
        DBGHDR_KdPrint(DBGMASK_USB_TRACE,("WTPUSB_CallUSBD() URB status = %x irp status = %x\n",
            Urb?Urb->UrbHeader.Status:0, ioStatus.Status));
        //
        // USBD maps the error code for us
        //
        ntStatus = ioStatus.Status;
    } // end of IoBuildDeviceIoControlRequest succeeding
    else
        ntStatus = STATUS_UNSUCCESSFUL;
    
    DBGHDR_KdPrint(DBGMASK_USB_TRACE,("WTPUSB_CallUSBD() exiting with %x\n", ntStatus));
	if( ntStatus != STATUS_SUCCESS )
	{
//		_asm int 3

	}

    return ntStatus;
}


NTSTATUS
WTPUSB_ConfigureDevice(
    IN  PDEVICE_OBJECT DeviceObject
)
/*++
        
Routine Description:

    Initializes a given instance of the device on the USB and
    selects and saves the configuration.
    This code is pageable
            
Arguments:
             
    DeviceObject - pointer to the FDO that is calling for the information
                
Return Value:
                  
    NT status code
                    
--*/
{
    PFDO_DEVICE_EXTENSION   FDODeviceExtension = NULL;
    NTSTATUS                ntStatus = STATUS_SUCCESS;
    PURB                    urb = NULL;
    ULONG                   siz = 0;
    
    DBGHDR_KdPrint(DBGMASK_USB_TRACE,("WTPUSB_ConfigureDevice() Enter\n"));
    PAGED_CODE();
    DBGHDR_ASSERT(NULL != DeviceObject);
    
    FDODeviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT(NULL != FDODeviceExtension);
    DBGHDR_ASSERT(NULL == FDODeviceExtension->UsbConfigurationDescriptor);
    
    urb = ExAllocatePoolWithTag(NonPagedPool, sizeof(struct _URB_CONTROL_DESCRIPTOR_REQUEST), WTP_POOL_TAG);
    
    if (NULL != urb)
    {
        // When USB_CONFIGURATION_DESCRIPTOR_TYPE is specified for DescriptorType
        // in a call to UsbBuildGetDescriptorRequest(),
        // all interface, endpoint, class-specific, and vendor-specific descriptors 
        // for the configuration also are retrieved. 
        // The caller must allocate a buffer large enough to hold all of this 
        // information or the data is truncated without error.
        // Therefore the 'siz' set below is just a 'good guess', and we may have to retry
        
        siz = sizeof(USB_CONFIGURATION_DESCRIPTOR) + 512;  
        
        // We will break out of this 'retry loop' when UsbBuildGetDescriptorRequest()
        // has a big enough FDODeviceExtension->UsbConfigurationDescriptor buffer not to truncate
        while( 1 ) 
        {
            
            FDODeviceExtension->UsbConfigurationDescriptor = ExAllocatePoolWithTag(NonPagedPool, siz, WTP_POOL_TAG);
            
            if (NULL == FDODeviceExtension->UsbConfigurationDescriptor) 
            {
                ExFreePool(urb);
                ntStatus = STATUS_INSUFFICIENT_RESOURCES;
                break;
            }
            
			{
				int retry;

				for(retry=0;retry<1024;retry++)	// high retry count to accomodate mhlv control msg handling anomalies
				{
					UsbBuildGetDescriptorRequest(
						urb,
						(USHORT) sizeof (struct _URB_CONTROL_DESCRIPTOR_REQUEST),
						USB_CONFIGURATION_DESCRIPTOR_TYPE,
						0,
						0,
						FDODeviceExtension->UsbConfigurationDescriptor,
						NULL,
						siz,
						NULL);
		            
					ntStatus = WTPUSB_CallUSBD(DeviceObject, urb, (IOCTL_INTERNAL_USB_SUBMIT_URB));

					if(NT_SUCCESS(ntStatus)) break;
					DBGHDR_KdPrint(DBGMASK_TRACE,("WTPUSB_ConfigureDevice: status %08x for try %d on get descriptor.\n", ntStatus, retry));

					// fixme: some errors are fatal. don't loop on all errors. exit loop on fatal errors.
				}

			}

            //
            // if we got some data see if it was enough.
            // NOTE: we may get an error in URB because of buffer overrun
            if (urb->UrbControlDescriptorRequest.TransferBufferLength > 0 &&
                FDODeviceExtension->UsbConfigurationDescriptor->wTotalLength > siz) 
            {
                siz = FDODeviceExtension->UsbConfigurationDescriptor->wTotalLength;
                ExFreePool(FDODeviceExtension->UsbConfigurationDescriptor);
                FDODeviceExtension->UsbConfigurationDescriptor = NULL;
            } 
            else 
            {
                break;  // we got it on the first try
            }
        } // end, while (retry loop )

        // Always free the urb - success or not
        if (NULL != urb)
            ExFreePool(urb);

        if (NT_SUCCESS(ntStatus)) 
        {
            //
            // We have the configuration descriptor for the configuration we want.
            // Now we issue the select configuration command to get
            // the  pipes associated with this configuration.
            //
            DBGHDR_ASSERT(FDODeviceExtension->UsbConfigurationDescriptor);
            ntStatus = WTPUSB_SelectInterface(
							DeviceObject,
							FDODeviceExtension->UsbConfigurationDescriptor
							);

        } // end of NT_SUCCESS(ntStatus) after the while loop
        
    } // end of NULL != urb
    else
    {
        DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_ConfigureDevice() Unable to allocate memory for urb\n"));
        ntStatus = STATUS_INSUFFICIENT_RESOURCES;
    }
    
    DBGHDR_KdPrint(DBGMASK_USB_TRACE,("WTPUSB_ConfigureDevice() exiting with %x\n", ntStatus));
    return ntStatus;
} 

NTSTATUS
WTPUSB_SelectInterface(
    IN PDEVICE_OBJECT DeviceObject,
    IN PUSB_CONFIGURATION_DESCRIPTOR ConfigurationDescriptor
)
/*++

Routine Description:

    Initializes the interface of the device
    This code is pageable

Arguments:

    DeviceObject - pointer to the device object 

    ConfigurationDescriptor - pointer to the USB configuration
                    descriptor containing the interface and endpoint
                    descriptors.

Return Value:

    NT status code

--*/
{
    PFDO_DEVICE_EXTENSION       FDODeviceExtension = NULL;
    NTSTATUS                    ntStatus = STATUS_SUCCESS;
    PURB                        urb = NULL;
    ULONG                       i = 0;
    PUSB_INTERFACE_DESCRIPTOR   interfaceDescriptor = NULL;
    PUSBD_INTERFACE_LIST_ENTRY  pInterfaceList = NULL;
    PUSBD_INTERFACE_INFORMATION Interface = NULL;
    USHORT                      siz = 0;
    
    DBGHDR_KdPrint(DBGMASK_USB_TRACE,("WTPUSB_SelectInterface() Enter\n"));
    PAGED_CODE();

    FDODeviceExtension = DeviceObject->DeviceExtension;

    siz = (ConfigurationDescriptor->bNumInterfaces + 1) * sizeof(struct _USBD_INTERFACE_LIST_ENTRY);
    pInterfaceList = (PUSBD_INTERFACE_LIST_ENTRY) ExAllocatePoolWithTag(NonPagedPool, siz, WTP_POOL_TAG);
    
    if (NULL != pInterfaceList) 
    {
        RtlZeroMemory (pInterfaceList, siz);
        //
        // USBD_ParseConfigurationDescriptorEx searches a given configuration
        // descriptor and returns a pointer to an interface that matches the 
        //  given search criteria. 
        //
        interfaceDescriptor = USBD_ParseConfigurationDescriptorEx(
                                  ConfigurationDescriptor,
                                  ConfigurationDescriptor, //search from start of config descriptor
                                  WTPUSB_INTERFACE_DEFAULT,   // Only want to get the first interface
                                  -1,   // not interested in alternate setting 
                                  -1,   // interface class not a criteria
                                  -1,   // interface subclass not a criteria
                                  -1    // interface protocol not a criteria
                                  );

        if (NULL != interfaceDescriptor) 
        {
            pInterfaceList[0].InterfaceDescriptor = interfaceDescriptor;
            pInterfaceList[0].Interface = NULL;
            pInterfaceList[1].InterfaceDescriptor = NULL;
            pInterfaceList[1].Interface = NULL;

            urb = USBD_CreateConfigurationRequestEx(ConfigurationDescriptor, pInterfaceList);

            if (NULL != urb)
            {
                Interface = &urb->UrbSelectConfiguration.Interface;
                for (i=0; i< Interface->NumberOfPipes; i++) 
                {
                    //
                    // perform any pipe initialization here
                    //
                    Interface->Pipes[i].PipeFlags = 0;
                }

				{
					int	retry;

					for(retry=0;retry<1024;retry++)	// high retry count to accomodate the firmware missing a msg bug.
					{								// a large retry is OK as long as the connectivity is still there.
													// but if some one pulls out the cable during this loop, it will
													// take a long time to finish the retry loop.
						UsbBuildSelectConfigurationRequest(
							urb,
							(USHORT) sizeof(struct _URB_SELECT_CONFIGURATION),
							ConfigurationDescriptor);
		            
						ntStatus = WTPUSB_CallUSBD(DeviceObject, urb, (IOCTL_INTERNAL_USB_SUBMIT_URB));

						if(NT_SUCCESS(ntStatus)) break;
						DBGHDR_KdPrint(DBGMASK_TRACE,("WTPUSB_SelectInterface: status %08x for try %d on get descriptor.\n", ntStatus, retry));
						// fixme: should only retry if error is timeout...
					}
				}

            }
            else
            {
                DBGHDR_KdPrint( DBGMASK_ERROR,("WTPUSB_SelectInterface() USBD_CreateConfigurationRequest() failed\n  returning STATUS_INSUFFICIENT_RESOURCES\n"));
                ExFreePool(pInterfaceList);
                ntStatus = STATUS_INSUFFICIENT_RESOURCES;
            }
        } // end of if (interfaceDescriptor)
        else
        {
            DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_SelectInterface() ParseConfigurationDescriptorEx() failed\n  returning STATUS_INSUFFICIENT_RESOURCES\n"));
            ExFreePool(pInterfaceList);
            ntStatus = STATUS_INSUFFICIENT_RESOURCES;
        }
    } 
    else 
    {
        DBGHDR_KdPrint( DBGMASK_ERROR,("WTPUSB_SelectInterface() USBD_CreateConfigurationRequest() failed\n  returning STATUS_INSUFFICIENT_RESOURCES\n"));
        ntStatus = STATUS_INSUFFICIENT_RESOURCES;
    }

    if (NT_SUCCESS(ntStatus)) 
    {
        FDODeviceExtension->UsbConfigurationHandle = urb->UrbSelectConfiguration.ConfigurationHandle;

        // Allocate memory to save the interface information
        FDODeviceExtension->UsbInterface = ExAllocatePoolWithTag(NonPagedPool, Interface->Length, WTP_POOL_TAG);

        if (NULL != FDODeviceExtension->UsbInterface) 
        {
            USHORT j = 0;

            //
            // save a copy of the interface information returned
            //
            RtlCopyMemory(FDODeviceExtension->UsbInterface, Interface, Interface->Length);

            //
            // Dump the interface to the debugger
            //
            DBGHDR_KdPrint( DBGMASK_USB_DATA,("---------\n"));
            DBGHDR_KdPrint( DBGMASK_USB_DATA,("NumberOfPipes 0x%x\n", FDODeviceExtension->UsbInterface->NumberOfPipes));
            DBGHDR_KdPrint( DBGMASK_USB_DATA,("Length 0x%x\n", FDODeviceExtension->UsbInterface->Length));
            DBGHDR_KdPrint( DBGMASK_USB_DATA,("Alt Setting 0x%x\n", FDODeviceExtension->UsbInterface->AlternateSetting));
            DBGHDR_KdPrint( DBGMASK_USB_DATA,("Interface Number 0x%x\n", FDODeviceExtension->UsbInterface->InterfaceNumber));
            DBGHDR_KdPrint( DBGMASK_USB_DATA,("Class, subclass, protocol 0x%x 0x%x 0x%x\n",
                FDODeviceExtension->UsbInterface->Class,
                FDODeviceExtension->UsbInterface->SubClass,
                FDODeviceExtension->UsbInterface->Protocol));

            // Dump the pipe info

            for (j=0; j<Interface->NumberOfPipes; j++) 
            {
                PUSBD_PIPE_INFORMATION pipeInformation = NULL;
                pipeInformation = &FDODeviceExtension->UsbInterface->Pipes[j];
                switch (pipeInformation->PipeType)
                {
                case UsbdPipeTypeBulk:
                    if (0x80 & pipeInformation->EndpointAddress)
                        FDODeviceExtension->pipeNumACLIn = j;
                    else
                        FDODeviceExtension->pipeNumACLOut = j;
                    break;
                case UsbdPipeTypeInterrupt:
                    FDODeviceExtension->pipeNumCMDIn = j;
                    break;
                default:
                    break;
                } // end of switch on pipeType

                DBGHDR_KdPrint( DBGMASK_USB_DATA,("---------\n"));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("PipeType 0x%x\n", pipeInformation->PipeType));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("EndpointAddress 0x%x\n", pipeInformation->EndpointAddress));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("MaxPacketSize 0x%x\n", pipeInformation->MaximumPacketSize));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("Interval 0x%x\n", pipeInformation->Interval));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("Handle 0x%x\n", pipeInformation->PipeHandle));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("MaximumTransferSize 0x%x\n", pipeInformation->MaximumTransferSize));
            } // End of for-loop through endpoints

#if 0
			// perform this assertion only if this is not a dfu device
			if( FDODeviceExtension->UsbDeviceDescriptor->idProduct != 0xabbb )
			{
				DBGHDR_ASSERT(INVALID_PIPE != FDODeviceExtension->pipeNumACLIn);
				DBGHDR_ASSERT(INVALID_PIPE != FDODeviceExtension->pipeNumACLOut);
				DBGHDR_ASSERT(INVALID_PIPE != FDODeviceExtension->pipeNumCMDIn);
			}
            // RH - should we error out here in case these are not set? Yes?
#endif

            DBGHDR_KdPrint( DBGMASK_USB_DATA,("---------\n"));
        } // end of check that FDODeviceExtension->UsbInterface was allocated properly
        else
        {
            DBGHDR_KdPrint( DBGMASK_ERROR,("WTPUSB_SelectInterface() Could not allocate memory for usbInterface\n"));
            ntStatus = STATUS_INSUFFICIENT_RESOURCES;
        }
    } // end of check NT_SUCCESS(ntStatus)

    if (NULL != urb) 
        ExFreePool(urb);
    if (NULL != pInterfaceList)
        ExFreePool(pInterfaceList);

    DBGHDR_KdPrint(DBGMASK_USB_TRACE,("WTPUSB_SelectInterface() exiting with %x\n", ntStatus));
    return ntStatus; 
}


