/*++

Copyright (c) 1998-1999 Intel Corporation

Module Name:

    $Workfile:: rfpnp.c                   $

Abstract:

    Handles Plug and Play requests for FDO and any created
    PDOs

Environment:

    Kernel mode only

Notes:

    Copyright (c) 1998-1999 Intel Corporation.  All Rights Reserved.

    The receipt of or possession of this program does not convey
    any rights to reproduce its contents or to manufacture,
    use, or sell anything that it may describe, in whole, or 
    in part, without specific written consent of Intel Corporation.


Revision History:
    $Date:: 7/19/00 12:12p                $ Date and time of last check in
    $Revision:: 2                         $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/

// use this manifest constant to deactivate bluetooth specific areas.
// once this driver has been debugged, all of the deactivated areas
// can be permanently deleted.

#include <wdm.h>
#include <usbdi.h>
#include <usbdlib.h>
#include <wmistr.h>
#include "wtpdbg.h"
#include "wtpdfu.h"

NTSTATUS
WTPUSB_ProcessFDOPnPIrp (
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp,
    IN PIO_STACK_LOCATION irpStack,
    IN PFDO_DEVICE_EXTENSION FDODeviceExtension
    )
/*++

Routine Description:

    Handles all Irps sent to this FDO.
    This code is pageable.

Arguments:

    DeviceObject    - pointer to our device object (FDO)

    Irp             - pointer to an I/O Request Packet

    irpStack        - Location on the stack

    FDODeviceExtension - pointer to our FDODeviceExtension

Return Value:

    NT status code

--*/
{
    NTSTATUS                ntStatus = STATUS_SUCCESS;
    ULONG                   i = 0;
    ULONG                   length = 0;
    PLIST_ENTRY             entry = NULL;
    PPDO_DEVICE_EXTENSION   pdoExtension = NULL;
    PDEVICE_RELATIONS       relations = NULL;
    KEVENT                  event;
    KIRQL                   oldIrql;

    PAGED_CODE ();

    dbgT(DBG_DEVEXT(DeviceObject), 'N', oldIrql);
	DBGHDR_KdPrint(DBGMASK_PNP_TRACE, ("WTPUSB_ProcessFDOPnPIrp() Enter devo: %08x Irp: %08x irpStk: %08x fdodevx: %08x with Irp code:%x\n", DeviceObject, Irp, irpStack, FDODeviceExtension, irpStack->MinorFunction));

    WTPUSB_IncrementIoCount(DeviceObject);
    
    switch (irpStack->MinorFunction)
    {   
    case IRP_MN_START_DEVICE:
        //
        // Wait for our bus driver to process the irp
        //
        KeInitializeEvent(&event, SynchronizationEvent, FALSE);
        IoCopyCurrentIrpStackLocationToNext(Irp);
        IoSetCompletionRoutine(
            Irp, 
            (PIO_COMPLETION_ROUTINE) WTPUSB_OnRequestComplete,
            (PVOID) &event, 
            TRUE, 
            TRUE, 
            TRUE);
        
		DBGHDR_KdPrint(DBGMASK_TRACE,("WTPUSB_ProcessFDOPnPIrp: passing start down the stack before calling start device: Irp: %08x, ev: %08x.\n",Irp,&event));

		KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
        ntStatus = IoCallDriver(
            FDODeviceExtension->topOfStackDeviceObject, 
            Irp);
        
        if (STATUS_PENDING == ntStatus)
        {                       
            KeWaitForSingleObject(
                &event, 
                Executive, 
                KernelMode, 
                FALSE, 
                NULL);
            
            ntStatus = Irp->IoStatus.Status;
        }
        KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 
        
		DBGHDR_KdPrint(DBGMASK_TRACE,("WTPUSB_ProcessFDOPnPIrp: event %08x signalled, status is %08x. calling start device.\n", &event, ntStatus));

        if (NT_SUCCESS(ntStatus))
        {
            // 
            // Start, configure, and select the interface on the device
            //
            // RH - Only do this if the device has not already been started 
            //      Perhaps check usbdescriptor in the devext?
            ntStatus = WTPUSB_StartDevice(DeviceObject);
            
			DBGHDR_KdPrint(DBGMASK_TRACE,("WTPUSB_ProcessFDOPnPIrp: back from StartDevice, status is %08x. calling start device.\n", ntStatus));
            
			if (NT_SUCCESS (ntStatus))
            {

                // Set the started flag after servicing the queue so that other requests
                // can now be handled
                FDODeviceExtension->DeviceStarted = TRUE;

				//bpc: check if this is necessary...
                //WTPUSB_SelfSuspendOrActivate(DeviceObject, ForceActivate);
				// bpc: fixme..seems like it is not necessary. enumeration, unload and data transfer all work without this...


				//
				// Turn on the WTPDFU interface to this device object
				//
				// this is necessary in order for the application to be able to find our device and issue a createfile call.
				ntStatus = IoSetDeviceInterfaceState( &FDODeviceExtension->interfaceNameWTPDFU, TRUE);

                if (!NT_SUCCESS (ntStatus)) 
				{
                    DBGHDR_KdPrint (DBGMASK_ERROR,("WTPUSB_ProcessFDOPnPIrp():IRP_MN_START_DEVICE: IoSetDeviceInterfaceState failed %x\n", ntStatus));
				}

                // Drop through to the exit - even if there was an error
                Irp->IoStatus.Information = 0;
            } // Success returned from startDevice
        } // Success returned from calling the lower driver's start routine
        //
        // Complete the request
        //      Add any new start information above FDODeviceExtension->DeviceStarted = TRUE
        //      because the error condition from IoSetDeviceInterfaceState falls through
        //      this section of code.
        //
        Irp->IoStatus.Status = ntStatus;
        IoCompleteRequest (Irp, IO_NO_INCREMENT);
        
        WTPUSB_DecrementIoCount(DeviceObject);
        break;
    case IRP_MN_QUERY_STOP_DEVICE:

        // The IRP_MN_QUERY_STOP_DEVICE/IRP_MN_STOP_DEVICE sequence only occurs
        // during "polite" shutdowns, such as the user explicitily requesting the
        // service be stopped in, or requesting unplug from the Pnp tray icon.
        // This sequence is NOT received during "impolite" shutdowns,
        // such as someone suddenly yanking the cord or otherwise 
        // unexpectedly disabling/resetting the device.

        // If a driver sets STATUS_SUCCESS for this IRP,
        // the driver must not start any operations on the device that
        // would prevent that driver from successfully completing an IRP_MN_STOP_DEVICE
        // for the device.
        // For mass storage devices such as disk drives, while the device is in the
        // stop-pending state,the driver holds IRPs that require access to the device,
        // but for most devices, there is no 'persistent storage', so we will just
        // refuse any more IO until restarted or the stop is cancelled

        // If a driver in the device stack determines that the device cannot be
        // stopped for resource reconfiguration, the driver is not required to pass
        // the IRP down the device stack. If a query-stop IRP fails,
        // the PnP Manager sends an IRP_MN_CANCEL_STOP_DEVICE to the device stack,
        // notifying the drivers for the device that the query has been cancelled
        // and that the device will not be stopped.

        // It is possible to receive this irp when the device has not been started
        //  ( as on a boot device )
        if (!FDODeviceExtension->DeviceStarted) 
        { // if get when never started, just pass on
            DBGHDR_KdPrint( DBGMASK_PNP_TRACE,("WTPUSB_ProcessFDOPnPIrp() IRP_MN_QUERY_STOP_DEVICE when device not started\n"));
            ntStatus = STATUS_SUCCESS;
        }
        else
        {
            // RH - fix this - may need something else in the device extension
            // fail the request if we have any read/write IRPS pending
            if(0 < FDODeviceExtension->numPendingACLIrps ||
               0 < FDODeviceExtension->numPendingCMDIrps ||
               0 < FDODeviceExtension->numPendingSCOInIrps ||
               0 < FDODeviceExtension->numPendingSCOOutIrps) 
            {
                ntStatus = STATUS_UNSUCCESSFUL;
            }
            else 
            { 
                // We'll not veto it; pass it on and flag that stop was requested.
                // Once StopDeviceRequested is set no new IOCTL or read/write irps will be passed
                // down the stack to lower drivers; all will be quickly failed
                FDODeviceExtension->StopDeviceRequested = TRUE;
            }
        } // end of else from if !deviceStarted

        if (!NT_SUCCESS(ntStatus)) 
        {
            // if anything went wrong, return failure without passing Irp down
            Irp->IoStatus.Status = ntStatus;
            IoCompleteRequest (Irp, IO_NO_INCREMENT);
        }
        else
        {
            IoSkipCurrentIrpStackLocation (Irp);
            KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
            ntStatus = IoCallDriver (FDODeviceExtension->topOfStackDeviceObject, Irp);
            KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 
        }

        WTPUSB_DecrementIoCount(DeviceObject);

        break; // end, case IRP_MN_QUERY_STOP_DEVICE

    case IRP_MN_CANCEL_STOP_DEVICE:

        // The PnP Manager uses this IRP to inform the drivers for a device
        // that the device will not be stopped for resource reconfiguration.
        // This should only be received after a successful IRP_MN_QUERY_STOP_DEVICE.

        DBGHDR_ASSERT(!FDODeviceExtension->DeviceRemoved);

        // It is possible to receive this irp when the device has not been started
        if (!FDODeviceExtension->DeviceStarted) 
        { 
            // if get when never started, just pass on
            DBGHDR_KdPrint(DBGMASK_PNP_TRACE,("WTPUSB_ProcessFDOPnPIrp() IRP_MN_CANCEL_STOP_DEVICE when device not started\n"));
            IoSkipCurrentIrpStackLocation (Irp);
            KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
            ntStatus = IoCallDriver (FDODeviceExtension->topOfStackDeviceObject, Irp);
            KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 
        }
        else
        {
            //
            // Wait for our bus driver to process the irp
            //
            Irp->IoStatus.Status = STATUS_SUCCESS;
            KeInitializeEvent(&event, SynchronizationEvent, FALSE);
            IoCopyCurrentIrpStackLocationToNext(Irp);
            IoSetCompletionRoutine(
                Irp, 
                (PIO_COMPLETION_ROUTINE) WTPUSB_OnRequestComplete,
                (PVOID) &event, 
                TRUE, 
                TRUE, 
                TRUE);
            
            KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
            ntStatus = IoCallDriver(
                FDODeviceExtension->topOfStackDeviceObject, 
                Irp);
                        
            if (STATUS_PENDING == ntStatus)
            {                       
                KeWaitForSingleObject(
                    &event, 
                    Executive, 
                    KernelMode, 
                    FALSE, 
                    NULL);
                
                ntStatus = Irp->IoStatus.Status;
            }
            KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 

            // Reset this flag so new IOCTL and IO Irp processing will be re-enabled
            FDODeviceExtension->RemoveDeviceRequested = FALSE;

            //
            // We must now complete the IRP, since we stopped it in the
            // completetion routine with MORE_PROCESSING_REQUIRED.
            //
            Irp->IoStatus.Status = ntStatus;
            Irp->IoStatus.Information = 0;
            IoCompleteRequest (Irp, IO_NO_INCREMENT);
        } // end of else        
        
        WTPUSB_DecrementIoCount(DeviceObject);
        break; // end, case IRP_MN_CANCEL_STOP_DEVICE

    case IRP_MN_STOP_DEVICE:

        // The PnP Manager sends this IRP to stop a device so it can reconfigure
        // its hardware resources. The PnP Manager only sends this IRP if a prior
        // IRP_MN_QUERY_STOP_DEVICE completed successfully.
        // Because we closed all the pipes, we need to reset all of the counters
        FDODeviceExtension->ACLInstanceCount = 0;
        FDODeviceExtension->ControlInstanceCount = 0;
        FDODeviceExtension->SCOInstanceCount = 0;
        
        ntStatus = IoSetDeviceInterfaceState(&(FDODeviceExtension->interfaceNameWTPDFU), FALSE);
        DBGHDR_ASSERT(NT_SUCCESS(ntStatus));
        // RH - losing the return status?
        
        //
        // Send the select configuration urb with a NULL pointer for the configuration
        // handle, this closes the configuration and puts the device in the 'unconfigured'
        // state. Drop the return status on the ground because the DDK claims we should
        // never fail this irp.
        //
        WTPUSB_StopDevice(DeviceObject);

        IoSkipCurrentIrpStackLocation (Irp);
        KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
        ntStatus = IoCallDriver (FDODeviceExtension->topOfStackDeviceObject, Irp);
        KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 
        WTPUSB_DecrementIoCount(DeviceObject);
        break; // end, case IRP_MN_STOP_DEVICE
 
    case IRP_MN_QUERY_REMOVE_DEVICE:

        //  In response to this IRP, drivers indicate whether the device can be
        //  removed without disrupting the system.
        //  If a driver determines it is safe to remove the device,
        //  the driver completes any outstanding I/O requests, arranges to hold any subsequent
        //  read/write requests, and sets Irp->IoStatus.Status to STATUS_SUCCESS. Function
        //  and filter drivers then pass the IRP to the next-lower driver in the device stack.
        //  The underlying bus driver calls IoCompleteRequest.

        //  If a driver sets STATUS_SUCCESS for this IRP, the driver must not start any
        //  operations on the device that would prevent that driver from successfully completing
        //  an IRP_MN_REMOVE_DEVICE for the device. If a driver in the device stack determines
        //  that the device cannot be removed, the driver is not required to pass the
        //  query-remove IRP down the device stack. If a query-remove IRP fails, the PnP Manager
        //  sends an IRP_MN_CANCEL_REMOVE_DEVICE to the device stack, notifying the drivers for
        //  the device that the query has been cancelled and that the device will not be removed.

        // It is possible to receive this irp when the device has not been started
        if (!FDODeviceExtension->DeviceStarted) 
        { 
            // if get when never started, just pass on
            DBGHDR_KdPrint( DBGMASK_PNP_TRACE,("WTPUSB_ProcessFDOPnPIrp() IRP_MN_QUERY_REMOVE_DEVICE when device not started\n"));
        }
        else
        {
            // If we have an outstanding bus reference (RFBD) or we have any other
            // condition that would cause a loss of data (outstanding IO) then we 
            // must fail the request.
            // However, note that we may always fail this request then.
            if (NULL != FDODeviceExtension->pdo || 
                0 < FDODeviceExtension->numPendingACLIrps ||
                0 < FDODeviceExtension->numPendingCMDIrps ||
                0 < FDODeviceExtension->numPendingSCOInIrps ||
                0 < FDODeviceExtension->numPendingSCOOutIrps) 
            {
                ntStatus = STATUS_UNSUCCESSFUL;
                WTPUSB_DecrementIoCount(DeviceObject);
            }
            else
            {
                // Once RemoveDeviceRequested is set no new IOCTL or read/write irps will be passed
                // down the stack to lower drivers; all will be quickly failed
                FDODeviceExtension->RemoveDeviceRequested = TRUE;
                WTPUSB_DecrementIoCount(DeviceObject);                

                // Wait for any io request pending in our driver to
                // complete before returning success.
                // This  event is set when FDODeviceExtension->PendingIoCount goes to 1
                // RH - is this a problem? This piece of IO is still active - shouldn't the count be one greater?
                //          How should this be fixed?
                ntStatus = KeWaitForSingleObject(
                    &FDODeviceExtension->NoPendingIoEvent,
                    Suspended,
                    KernelMode,
                    FALSE,
                    NULL);

                KeClearEvent(&FDODeviceExtension->NoPendingIoEvent);
            } // end of else from testing pending IO and outstanding bus reference
        }

        if (NT_SUCCESS(ntStatus))
        {
            IoSkipCurrentIrpStackLocation (Irp);
            KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
            ntStatus = IoCallDriver (FDODeviceExtension->topOfStackDeviceObject, Irp);
            KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 
        }
        else 
        {
            Irp->IoStatus.Status = ntStatus;
            IoCompleteRequest (Irp, IO_NO_INCREMENT);
        }
        break; // end, case IRP_MN_QUERY_REMOVE_DEVICE

    case IRP_MN_CANCEL_REMOVE_DEVICE:

        if (!FDODeviceExtension->DeviceStarted) 
        {
            // if get when never started, just pass on
            DBGHDR_KdPrint( DBGMASK_PNP_TRACE,("WTPUSB_ProcessFDOPnPIrp() IRP_MN_CANCEL_REMOVE_DEVICE when device not started\n"));
            IoSkipCurrentIrpStackLocation (Irp);
            KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
            ntStatus = IoCallDriver (FDODeviceExtension->topOfStackDeviceObject, Irp);
            KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 
        }
        else
        {
            //
            // Wait for our bus driver to process the irp
            //
            Irp->IoStatus.Status = STATUS_SUCCESS;
            KeInitializeEvent(&event, SynchronizationEvent, FALSE);
            IoCopyCurrentIrpStackLocationToNext(Irp);
            IoSetCompletionRoutine(
                Irp, 
                (PIO_COMPLETION_ROUTINE) WTPUSB_OnRequestComplete,
                (PVOID) &event, 
                TRUE, 
                TRUE, 
                TRUE);
            
            KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
            ntStatus = IoCallDriver(
                FDODeviceExtension->topOfStackDeviceObject, 
                Irp);
                        
            if (STATUS_PENDING == ntStatus)
            {                       
                KeWaitForSingleObject(
                    &event, 
                    Executive, 
                    KernelMode, 
                    FALSE, 
                    NULL);
                
                ntStatus = Irp->IoStatus.Status;
            }
            KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 

            // Reset this flag so new IOCTL and IO Irp processing will be re-enabled
            FDODeviceExtension->RemoveDeviceRequested = FALSE;

            //
            // We must now complete the IRP, since we stopped it in the
            // completetion routine with MORE_PROCESSING_REQUIRED.
            //
            Irp->IoStatus.Status = ntStatus;
            Irp->IoStatus.Information = 0;
            IoCompleteRequest (Irp, IO_NO_INCREMENT);
        } // end of else        
        
        WTPUSB_DecrementIoCount(DeviceObject);
        break; // end, case IRP_MN_CANCEL_REMOVE_DEVICE

    case IRP_MN_REMOVE_DEVICE:

        // The PnP Manager uses this IRP to direct drivers to remove a device. 
        // For a "polite" device removal, the PnP Manager sends an 
        // IRP_MN_QUERY_REMOVE_DEVICE prior to the remove IRP. In this case, 
        // the device is in the remove-pending state when the remove IRP arrives.
        // For a surprise-style device removal ( i.e. sudden cord yank ), 
        // the physical device has already been removed so the PnP Manager sends 
        // the remove IRP without a prior query-remove. A device can be in any state
        // when it receives a remove IRP as a result of a surprise-style removal.

        // Match the increment at the beginning of the routine
        WTPUSB_DecrementIoCount(DeviceObject);

        // Prevent new requests from being serviced
        FDODeviceExtension->DeviceRemoved = TRUE;

        // Unload the upper driver (we don't care about the status - we have to remove)
        //WTPUSB_UnLoadUpperDriver(DeviceObject);

        
        // Because we closed all the pipes, we need to reset all of the counters
        FDODeviceExtension->ACLInstanceCount = 0;
        FDODeviceExtension->ControlInstanceCount = 0;
        FDODeviceExtension->SCOInstanceCount = 0;
#if 0
		// bpc: trying to defeature wmi
        // Unregister with WMI
        ntStatus = IoWMIRegistrationControl(DeviceObject, WMIREG_ACTION_DEREGISTER);
        if (!NT_SUCCESS(ntStatus))
            DBGHDR_KdPrint(DBGMASK_ERROR, ("IoWMIRegistration failed with %x\n", ntStatus));
        DBGHDR_ASSERT(NT_SUCCESS(ntStatus));        
#endif

        IoCopyCurrentIrpStackLocationToNext(Irp);

        KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
        ntStatus = IoCallDriver(FDODeviceExtension->topOfStackDeviceObject, Irp);
        KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 

        //
        // The final decrement to device extension PendingIoCount == 0
        // will set FDODeviceExtension->RemoveEvent, enabling device removal.
        // If there is no pending IO at this point, the below decrement will be it.
        // If there is still pending IO, 
        WTPUSB_DecrementIoCount(DeviceObject);

        // wait for any io request pending in our driver to
        // complete for finishing the remove
        KeWaitForSingleObject(
                    &FDODeviceExtension->RemoveEvent,
                    Suspended,
                    KernelMode,
                    FALSE,
                    NULL);

        //
        // Delete the link and FDO we created
        //
        WTPUSB_RemoveDevice(DeviceObject);
        IoDetachDevice(FDODeviceExtension->topOfStackDeviceObject);
        IoDeleteDevice (DeviceObject);
        break;

    case IRP_MN_QUERY_DEVICE_RELATIONS:
        // RH - What about TargetRelations & PowerRelations?
        // Removal relations is not necessary - handled by BusRelations
        if (BusRelations == irpStack->Parameters.QueryDeviceRelations.Type) 
        {
            DBGHDR_KdPrint (DBGMASK_PNP_TRACE, ("WTPUSB_ProcessFDOPnPIrp() entering Bus Query Relations\n"));
            
            //
            // Tell the plug and play system about all the PDOs.
            //
            // There might also be device relations below and above this FDO,
            // so, be sure to propagate the relations from the upper drivers.
            //
            // No Completion routine is needed so long as the ntStatus is preset
            // to success.  (PDOs complete plug and play irps with the current
            // IoStatus.Status and IoStatus.Information as the default.)
            //

            if (NULL != FDODeviceExtension->pdo)
            {
                i = (0 == Irp->IoStatus.Information) ? 0 :
            ((PDEVICE_RELATIONS) Irp->IoStatus.Information)->Count;
            length = sizeof(DEVICE_RELATIONS) + ((1 + i) * sizeof (PDEVICE_OBJECT));
            relations = (PDEVICE_RELATIONS) ExAllocatePoolWithTag(NonPagedPool, length, WTP_POOL_TAG);
            
            if (NULL != relations) 
            {
                //
                // Copy in the device objects so far
                //
                RtlZeroMemory(relations, length);
                if (0 != i) 
                {
                    RtlCopyMemory (
                        relations->Objects,
                        ((PDEVICE_RELATIONS) Irp->IoStatus.Information)->Objects,
                        i * sizeof (PDEVICE_OBJECT));
                }
                relations->Count = 1 + i;
                
                //
                // For our PDO on this bus add a pointer to the device relations
                // buffer, being sure to take out a reference to that object.
                // The PlugPlay system will dereference the object when it is done with
                // it and free the device relations buffer.
                //
                relations->Objects[i] = FDODeviceExtension->pdo;
                ObReferenceObject (FDODeviceExtension->pdo);
                
                //
                // Set up and pass the IRP further down the stack
                //
                Irp->IoStatus.Status = STATUS_SUCCESS;
                
                if (0 != Irp->IoStatus.Information) 
                {
                    ExFreePool ((PVOID) Irp->IoStatus.Information);
                }
                Irp->IoStatus.Information = (ULONG) relations;
                    
                } // end of memory allocation check
                else
                {
                    ntStatus = STATUS_INSUFFICIENT_RESOURCES;
                } // end of else
            } // end of no child pdo
            else 
            {
                Irp->IoStatus.Information = 0;
                Irp->IoStatus.Status = STATUS_SUCCESS;
            }
        } // end of not a bus relations query
        else
        {
            //
            // If this is not a bus relations query, then return STATUS_NOT_SUPPORTED
            //
            ntStatus = STATUS_NOT_SUPPORTED;
            Irp->IoStatus.Status = ntStatus;
            Irp->IoStatus.Information = 0;
        }

        WTPUSB_DecrementIoCount(DeviceObject);
        IoSkipCurrentIrpStackLocation (Irp);
        KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
        ntStatus = IoCallDriver (FDODeviceExtension->topOfStackDeviceObject, Irp);
        KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 
        break;
    default:
        WTPUSB_DecrementIoCount(DeviceObject);
        IoSkipCurrentIrpStackLocation(Irp);
        KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
        ntStatus = IoCallDriver(FDODeviceExtension->topOfStackDeviceObject, Irp);
        KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 
        break;
    }

    //
    // Exit point of function - even errors come through this section
    //      so be careful what is added here.
    //
    dbgT(DBG_DEVEXT(DeviceObject), 'n', oldIrql);
    DBGHDR_KdPrint(DBGMASK_PNP_TRACE, ("WTPUSB_ProcessFDOPnPIrp() Exit with %x\n", ntStatus));
    return ntStatus;
}

NTSTATUS
WTPUSB_ProcessPDOPnPIrp(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp,
    IN PIO_STACK_LOCATION irpStack,
    IN PPDO_DEVICE_EXTENSION PDODeviceExtension
    )
/*++

Routine Description:

    Handles all PnP Irps sent to this PDO.
    This code is pageable

Arguments:

    DeviceObject    - pointer to one of our device objects (PDO)

    Irp             - pointer to an I/O Request Packet

    irpStack        - Location on the stack

    deviceExtension - pointer to our deviceExtension

Return Value:

    NT status code

--*/
{
    PDEVICE_CAPABILITIES    deviceCapabilities = NULL;
    ULONG                   information = 0;
    PWCHAR                  buffer = NULL, buffer2 = NULL;
    ULONG                   length = 0;
    NTSTATUS                ntStatus = STATUS_SUCCESS;
    KIRQL                   oldIrql;

    PAGED_CODE ();

    dbgT(DBG_DEVEXT(DeviceObject), 'M', oldIrql);
    DBGHDR_KdPrint(DBGMASK_PNP_TRACE, ("ProcessPDOPnPIrp() Enter PDO Handler with Irp code:%x\n", irpStack->MinorFunction));

    ntStatus = Irp->IoStatus.Status;

    switch (irpStack->MinorFunction) {
    case IRP_MN_QUERY_CAPABILITIES:
        //
        // Get the packet.
        //
        deviceCapabilities=irpStack->Parameters.DeviceCapabilities.Capabilities;

        //
        // Set the capabilities.
        //
        deviceCapabilities->Version = 1;
        deviceCapabilities->Size = sizeof (DEVICE_CAPABILITIES);

        deviceCapabilities->SurpriseRemovalOK = TRUE; // If the system should popup a window when the device is removed unexpectedly
        deviceCapabilities->SilentInstall = FALSE; 
        
        // The equivalence is to the highest power state the device can maintain for each system state
        deviceCapabilities->DeviceState[PowerSystemWorking] = PowerDeviceD0;
        deviceCapabilities->DeviceState[PowerSystemSleeping1] = PowerDeviceD1; 
        deviceCapabilities->DeviceState[PowerSystemSleeping2] = PowerDeviceD1;
        deviceCapabilities->DeviceState[PowerSystemSleeping3] = PowerDeviceD1;
        deviceCapabilities->DeviceState[PowerSystemHibernate] = PowerDeviceD2;
        deviceCapabilities->DeviceState[PowerSystemShutdown] = PowerDeviceD3;
        
        // The device can wake up a system that is in S4            
        deviceCapabilities->SystemWake = PowerSystemHibernate;
        
        // The device can still wake up the system when it is in D2
        deviceCapabilities->DeviceWake = PowerDeviceD2;

        // We have no latencies
        deviceCapabilities->D1Latency = 0;
        deviceCapabilities->D2Latency = 0;
        deviceCapabilities->D3Latency = 0;

        // No locking or ejection
        deviceCapabilities->LockSupported = FALSE;
        deviceCapabilities->EjectSupported = FALSE;

        // Device can be physically removed.
        // Technically there is no physical device to remove, but this bus
        // driver can yank the PDO from the PlugPlay system
        deviceCapabilities->Removable = TRUE;

        // not Docking device
        deviceCapabilities->DockDevice = FALSE;

        deviceCapabilities->UniqueID = FALSE;
        ntStatus = STATUS_SUCCESS;
        break;

    case IRP_MN_QUERY_ID:
        // Query the IDs of the device

        switch (irpStack->Parameters.QueryId.IdType) {

        case BusQueryDeviceID:
            // return a WCHAR (null terminated) string describing the device
            // For simplicity we make it exactly the same as the Hardware ID.
        case BusQueryHardwareIDs:
            // return a multi WCHAR (null terminated) string (null terminated)
            // array for use in matching hardare ids in inf files;
            //
            buffer = PDODeviceExtension->HardwareIDs;

            length = (wcslen(PDODeviceExtension->HardwareIDs) + 2) * sizeof (WCHAR);

            buffer = ExAllocatePoolWithTag (PagedPool, length, WTP_POOL_TAG);
            if (NULL != buffer) 
            {
                RtlZeroMemory(buffer, length);
                RtlCopyMemory(buffer, PDODeviceExtension->HardwareIDs, length-2);
                ntStatus = STATUS_SUCCESS;
            }
            else
            {
                ntStatus = STATUS_INSUFFICIENT_RESOURCES;
            }
            Irp->IoStatus.Information = (ULONG) buffer;
            break;


        case BusQueryInstanceID:
            // 
            // Just return 1 - there should only ever be one instance
            //
            length = 3 * sizeof(WCHAR); // 1 number + 2 EOLs
            buffer = ExAllocatePoolWithTag (PagedPool, length, WTP_POOL_TAG);
            if (NULL != buffer) 
            {
                RtlZeroMemory(buffer, length);
                ntStatus = STATUS_SUCCESS;
                RtlCopyMemory (buffer, "1", 1);
            }
            else
            {
                ntStatus = STATUS_INSUFFICIENT_RESOURCES;
            }
            Irp->IoStatus.Information = (ULONG) buffer;
            break;

        case BusQueryCompatibleIDs:
            // The generic ids for installation of this pdo.
            length = (sizeof(RFBD_COMPATIBLE_ID) + 1) * sizeof (WCHAR);
            buffer = ExAllocatePoolWithTag (PagedPool, length, WTP_POOL_TAG);
            if (NULL != buffer) 
            {
                RtlZeroMemory(buffer, length);
                RtlCopyMemory(buffer, RFBD_COMPATIBLE_ID, length);
            }
            Irp->IoStatus.Information = (ULONG) buffer;
            break;
        }
        ntStatus = STATUS_SUCCESS;
        break;

    case IRP_MN_START_DEVICE:
        // Here we do what ever initialization and ``turning on'' that is
        // required to allow others to access this device.
        // RH - Should I wait until the FDO has been started?
        PDODeviceExtension->DeviceStarted = TRUE;
        ntStatus = STATUS_SUCCESS;
        break;

    case IRP_MN_STOP_DEVICE:
        // Here we shut down the device.  The opposite of start.
        PDODeviceExtension->DeviceStarted = FALSE;
        ntStatus = STATUS_SUCCESS;
        break;

    case IRP_MN_REMOVE_DEVICE:
        //
        // The remove IRP code for a PDO uses the following steps:
        //
        // - Complete any requests queued in the driver
        // - If the device is still attached to the system,
        //   then complete the request and return.
        // - Otherwise, cleanup device specific allocations, memory, events...
        // - Call IoDeleteDevice
        // - Return from the dispatch routine.
        //
        //
        // Complete any outsanding requests with STATUS_DELETE_PENDING.
        //
        if (FALSE == PDODeviceExtension->DeviceRemoved)
        {
            PDODeviceExtension->DeviceRemoved = TRUE;
            PDODeviceExtension->DeviceStarted = FALSE;
            ExFreePool (PDODeviceExtension->HardwareIDs);
            IoDeleteDevice (DeviceObject);
        }
        
        ntStatus = STATUS_SUCCESS;
        break;

    case IRP_MN_QUERY_STOP_DEVICE:
        // No reason here why we can't stop the device.
        // If there were a reason we should speak now for answering success
        // here may result in a stop device irp.
        ntStatus = STATUS_SUCCESS;
        break;

    case IRP_MN_CANCEL_STOP_DEVICE:
        //
        // The stop was canceled.  Whatever state we set, or resources we put
        // on hold in anticipation of the forcoming STOP device IRP should be
        // put back to normal.  Someone, in the long list of concerned parties,
        // has failed the stop device query.
        //
        ntStatus = STATUS_SUCCESS;
        break;

    case IRP_MN_QUERY_REMOVE_DEVICE:
        //
        // Just like Query Stop only now the impending doom is the remove irp
        //
        ntStatus = STATUS_SUCCESS;
        break;

    case IRP_MN_CANCEL_REMOVE_DEVICE:
        //
        // Clean up a remove that did not go through, just like cancel STOP.
        //
        ntStatus = STATUS_SUCCESS;
        break;

    case IRP_MN_QUERY_RESOURCE_REQUIREMENTS:
    case IRP_MN_QUERY_DEVICE_RELATIONS:
    case IRP_MN_READ_CONFIG:
    case IRP_MN_WRITE_CONFIG: // we have no config space
    case IRP_MN_EJECT:
    case IRP_MN_SET_LOCK:
    case IRP_MN_QUERY_INTERFACE: // We do not have any non IRP based interfaces.
    default:
        DBGHDR_KdPrint( DBGMASK_PNP_TRACE, ("ProcessPDOPnPIrp() PnP Irp not handled %x\n", irpStack->MinorFunction));
        // For PnP requests to the PDO that we do not understand we should
        // return the IRP WITHOUT setting the status or information fields.
        // They may have already been set by a filter (eg acpi).
        break;
    }

    Irp->IoStatus.Status = ntStatus;
    IoCompleteRequest (Irp, IO_NO_INCREMENT);

    dbgT(DBG_DEVEXT(DeviceObject), 'm', oldIrql);
    DBGHDR_KdPrint (DBGMASK_PNP_TRACE, ("ProcessPDOPnPIrp() Exiting with status %x\n", ntStatus));
    return ntStatus;
}


NTSTATUS 
WTPUSB_OnRequestComplete(
    IN PDEVICE_OBJECT fdo, 
    IN PIRP Irp, 
    IN PKEVENT pev
    )
/*++

Routine Description:

    Used as a call back routine for PnP Irps.
    
    When called back - it sets the notification event passed in

    This code is non-pageable

Arguments:

    DeviceObject    - pointer to one of our device objects (FDO)

    Irp             - pointer to an I/O Request Packet

    pev             - pointer to an event

Return Value:

    Always returns STATUS_MORE_PROCESSING_REQUIRED

--*/
{
    DBGHDR_ASSERT(NULL != pev);

    KeSetEvent(
        pev, 
        0, 
        FALSE);

    return STATUS_MORE_PROCESSING_REQUIRED;
}


LONG
WTPUSB_DecrementIoCount(
    IN PDEVICE_OBJECT DeviceObject
)
/*++

Routine Description:

    We keep a pending IO count ( extension->PendingIoCount )  in the device extension.
    The first increment of this count is done on adding the device.
    Subsequently, the count is incremented for each new IRP received and
    decremented when each IRP is completed or passed on.

    Transition to 'one' therefore indicates no IO is pending and signals
    deviceExtension->NoPendingIoEvent. This is needed for processing
    IRP_MN_QUERY_REMOVE_DEVICE

    Transition to 'zero' signals an event ( deviceExtension->RemoveEvent )
    to enable device removal. This is used in processing for IRP_MN_REMOVE_DEVICE

    This code is non-pageable.
 
Arguments:

    DeviceObject -- ptr to our FDO

Return Value:

    deviceExtension->PendingIoCount


--*/

{
    PFDO_DEVICE_EXTENSION FDODeviceExtension = NULL;
    LONG ioCount = 0;

    DBGHDR_ASSERT(NULL != DeviceObject);
    FDODeviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT( ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->isFDO );

    ioCount = InterlockedDecrement(&FDODeviceExtension->PendingIoCount);

    DBGHDR_KdPrint(DBGMASK_TRACE,("WTPUSB_DecrementIoCount() Pending io count = %x\n", ioCount));
    DBGHDR_ASSERT(-1 < ioCount);

    if (1 == ioCount) 
    {
        // trigger no pending io
        DBGHDR_KdPrint(DBGMASK_TRACE,("WTPUSB_DecrementIoCount() setting NoPendingIoEvent\n"));
        KeSetEvent(&FDODeviceExtension->NoPendingIoEvent,
                   1,
                   FALSE);
    }

    if (0 == ioCount) 
    {
        // trigger remove-device event
        DBGHDR_KdPrint(DBGMASK_TRACE,("WTPUSB_DecrementIoCount() setting RemoveEvent\n"));
        KeSetEvent(&FDODeviceExtension->RemoveEvent,
                   1,
                   FALSE);
    }

    return ioCount;
}


VOID
WTPUSB_IncrementIoCount(
    IN PDEVICE_OBJECT DeviceObject
)
/*++

Routine Description:

    We keep a pending IO count ( extension->PendingIoCount )  in the device extension.
    The first increment of this count is done on adding the device.
    Subsequently, the count is incremented for each new IRP received and
    decremented when each IRP is completed or passed on.

    This code is non-pageable.

Arguments:

    DeviceObject -- ptr to our FDO

Return Value:

    None

--*/
{
    PFDO_DEVICE_EXTENSION FDODeviceExtension = NULL;
    DBGHDR_ASSERT(NULL != DeviceObject);

    // RH - Should I reset the two events here when iocount > 2?
    FDODeviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT( ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->isFDO );

    InterlockedIncrement(&FDODeviceExtension->PendingIoCount);
    DBGHDR_KdPrint( DBGMASK_TRACE,("WTPUSB_IncrementIoCount() Exiting - Pending io count = %x\n", FDODeviceExtension->PendingIoCount));
}

NTSTATUS
WTPUSB_StartDevice(
    IN  PDEVICE_OBJECT DeviceObject
)
/*++

Routine Description:

    Request the device descriptor, store it, configure the device, and
    select the device.
    This code is pageable.

Arguments:

    DeviceObject - pointer to the FDO 

Return Value:

    NT status code

--*/
{
    PFDO_DEVICE_EXTENSION   FDODeviceExtension = NULL;
    NTSTATUS                ntStatus = STATUS_SUCCESS;
    PUSB_DEVICE_DESCRIPTOR  deviceDescriptor = NULL;
    PURB                    urb = NULL;
    ULONG                   siz = 0;

    DBGHDR_KdPrint(DBGMASK_USB_TRACE,("WTPUSB_StartDevice() Enter\n"));
    PAGED_CODE();
    DBGHDR_ASSERT(NULL != DeviceObject);

    FDODeviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT(NULL != FDODeviceExtension);

    urb = ExAllocatePoolWithTag(NonPagedPool, sizeof(struct _URB_CONTROL_DESCRIPTOR_REQUEST), WTP_POOL_TAG);

    if (NULL != urb) 
    {
		RtlZeroMemory( urb, sizeof(struct _URB_CONTROL_DESCRIPTOR_REQUEST) );

        siz = sizeof(USB_DEVICE_DESCRIPTOR);
        deviceDescriptor = ExAllocatePoolWithTag(NonPagedPool, siz, WTP_POOL_TAG);
        if (NULL != deviceDescriptor) 
        {
			int	retry;

			// bpc: the firmware will occassionally lose a message, resulting in a timeout on this request.
			//      accomodate that by retrying the request many times.
			for(retry=0;retry<1024;retry++)
			{
				UsbBuildGetDescriptorRequest(urb,
											 (USHORT) sizeof (struct _URB_CONTROL_DESCRIPTOR_REQUEST),
											 USB_DEVICE_DESCRIPTOR_TYPE,
											 0,
											 0,
											 deviceDescriptor,
											 NULL,
											 siz,
											 NULL);

				ntStatus = WTPUSB_CallUSBD(DeviceObject, urb, (IOCTL_INTERNAL_USB_SUBMIT_URB));

				if(NT_SUCCESS(ntStatus)) break;
				DBGHDR_KdPrint(DBGMASK_TRACE,("WTPUSB_StartDevice: status %08x for try %d on get descriptor.\n", ntStatus, retry));

			}

                        
            if (NT_SUCCESS(ntStatus)) 
            {
                DBGHDR_KdPrint(DBGMASK_USB_DATA,("Device Descriptor = %x, len %x\n",
                                deviceDescriptor,
                                urb->UrbControlDescriptorRequest.TransferBufferLength));

                DBGHDR_KdPrint( DBGMASK_USB_DATA,("Device Descriptor:\n"));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("-------------------------\n"));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("bLength %d\n", deviceDescriptor->bLength));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("bDescriptorType 0x%x\n", deviceDescriptor->bDescriptorType));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("bcdUSB 0x%x\n", deviceDescriptor->bcdUSB));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("bDeviceClass 0x%x\n", deviceDescriptor->bDeviceClass));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("bDeviceSubClass 0x%x\n", deviceDescriptor->bDeviceSubClass));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("bDeviceProtocol 0x%x\n", deviceDescriptor->bDeviceProtocol));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("bMaxPacketSize0 0x%x\n", deviceDescriptor->bMaxPacketSize0));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("idVendor 0x%x\n", deviceDescriptor->idVendor));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("idProduct 0x%x\n", deviceDescriptor->idProduct));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("bcdDevice 0x%x\n", deviceDescriptor->bcdDevice));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("iManufacturer 0x%x\n", deviceDescriptor->iManufacturer));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("iProduct 0x%x\n", deviceDescriptor->iProduct));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("iSerialNumber 0x%x\n", deviceDescriptor->iSerialNumber));
                DBGHDR_KdPrint( DBGMASK_USB_DATA,("bNumConfigurations 0x%x\n", deviceDescriptor->bNumConfigurations));
            }
            else
            {
                // we failed on the call to WTPUSB_CallUSBD
                DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_StartDevice() FAILED WTPUSB_CallUSBD with %x\n", ntStatus));
                // Memory is freed below
            }
        } // end of NULL != DeviceDescriptor
        else 
        {
            // if we got here we failed to allocate deviceDescriptor
            DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_StartDevice() FAILED ExAllocatePool() for deviceDescriptor\n"));
            ntStatus = STATUS_INSUFFICIENT_RESOURCES;
            // urb is freed below
        }

        if (NT_SUCCESS(ntStatus)) 
        {
            FDODeviceExtension->UsbDeviceDescriptor = deviceDescriptor;
        } 
        else if (deviceDescriptor) 
        {
            ExFreePool(deviceDescriptor);
        }

        // Free the urb - whether there was an error on not
        ExFreePool(urb);
    } 
    else 
    {
        // if we got here we failed to allocate the urb
        DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_StartDevice() FAILED ExAllocatePool() for URB\n"));
        ntStatus = STATUS_INSUFFICIENT_RESOURCES;
    }

    if (NT_SUCCESS(ntStatus)) 
        ntStatus = WTPUSB_ConfigureDevice(DeviceObject);

    DBGHDR_KdPrint(DBGMASK_USB_TRACE,("WTPUSB_StartDevice() Exit with %x\n", ntStatus));
    return ntStatus;
}


NTSTATUS
WTPUSB_StopDevice(
    IN  PDEVICE_OBJECT DeviceObject
)
/*++

Routine Description:

    Stops a given instance of a device on the USB.
    We basically just tell USB this device is now 'unconfigured'

    This code is pageable.

Arguments:

    DeviceObject - pointer to the device object for this instance

Return Value:

    NT status code

--*/
{
    PFDO_DEVICE_EXTENSION   FDODeviceExtension = NULL;
    NTSTATUS                ntStatus = STATUS_SUCCESS;
    PURB                    urb = NULL;
    ULONG                   siz = 0;

    DBGHDR_KdPrint( DBGMASK_PNP_TRACE,("WTPUSB_StopDevice Enter\n"));
    PAGED_CODE();
    DBGHDR_ASSERT(NULL != DeviceObject);

    FDODeviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT( ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->isFDO );

    //
    // Send the select configuration urb with a NULL pointer for the configuration
    // handle. This closes the configuration and puts the device in the 'unconfigured'
    // state.
    //
    siz = sizeof(struct _URB_SELECT_CONFIGURATION);
    urb = ExAllocatePoolWithTag(NonPagedPool, siz, WTP_POOL_TAG);
    if (NULL != urb) 
    {
        UsbBuildSelectConfigurationRequest(urb, (USHORT) siz, NULL);
        ntStatus = WTPUSB_CallUSBD(DeviceObject, urb, (IOCTL_INTERNAL_USB_SUBMIT_URB));

        if (!NT_SUCCESS(ntStatus))
            DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_StopDevice() FAILURE Configuration Closed status = %x usb status = %x.\n", ntStatus, urb->UrbHeader.Status));

        ExFreePool(urb);
    } 
    else 
    {
        DBGHDR_KdPrint(DBGMASK_ERROR,("WTPUSB_StopDevice() FAILURE allocating memory for urb\n"));
        ntStatus = STATUS_INSUFFICIENT_RESOURCES;
    }

    // Because IRP_MN_STOP_DEVICE cannot be failed, we always set these variables
    // RH - does this make sense?
    FDODeviceExtension->DeviceStarted = FALSE;
    FDODeviceExtension->StopDeviceRequested = FALSE;

    DBGHDR_KdPrint( DBGMASK_PNP_TRACE,("WTPUSB_StopDevice() Exiting with %x\n", ntStatus));
    return ntStatus;
}


NTSTATUS
WTPUSB_RemoveDevice(
    IN  PDEVICE_OBJECT DeviceObject
)
/*++

Routine Description:

    Called from WTPUSB_ProcessPnPIrp() to
    clean up our device instance's allocated buffers; free symbolic links

    This code is pageable.

Arguments:

    DeviceObject - pointer to the FDO

Return Value:

    NT status code from free symbolic link operation

--*/
{
    PFDO_DEVICE_EXTENSION   FDODeviceExtension = NULL;
    NTSTATUS                ntStatus = STATUS_SUCCESS;

    DBGHDR_KdPrint( DBGMASK_PNP_TRACE,("WTPUSB_RemoveDevice Enter\n"));
    PAGED_CODE();
    DBGHDR_ASSERT(NULL != DeviceObject);

    FDODeviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT( ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->isFDO );

    // remove the GUID-based symbolic link
    ntStatus = IoSetDeviceInterfaceState(&(FDODeviceExtension->interfaceNameWTPDFU), FALSE);
    DBGHDR_ASSERT( NT_SUCCESS( ntStatus ) );

    //
    // Free the unicode string
    //
    RtlFreeUnicodeString(&(FDODeviceExtension->interfaceName));

    //
    // Free device descriptor structure
    //
    if (NULL != FDODeviceExtension->UsbDeviceDescriptor) 
    {
        ExFreePool(FDODeviceExtension->UsbDeviceDescriptor);
        FDODeviceExtension->UsbDeviceDescriptor = NULL;
    }

    //
    // Free up the other descriptors
    //
    if (NULL != FDODeviceExtension->UsbInterface) 
    {
        ExFreePool(FDODeviceExtension->UsbInterface);
        FDODeviceExtension->UsbInterface = NULL;
    }

    if (NULL != FDODeviceExtension->UsbIsochInterface)
    {
        ExFreePool(FDODeviceExtension->UsbIsochInterface);
        FDODeviceExtension->UsbIsochInterface = NULL;
    }

    if (NULL != FDODeviceExtension->UsbDFUInterface)
    {
        ExFreePool(FDODeviceExtension->UsbDFUInterface);
        FDODeviceExtension->UsbDFUInterface = NULL;
        FDODeviceExtension->UsbDFUDescriptor = NULL;
    }

    // free up the USB config discriptor
    if (NULL != FDODeviceExtension->UsbConfigurationDescriptor) 
    {
        ExFreePool(FDODeviceExtension->UsbConfigurationDescriptor);
        FDODeviceExtension->UsbConfigurationDescriptor = NULL;
    }

    DBGHDR_KdPrint( DBGMASK_PNP_TRACE,("WTPUSB_RemoveDevice() exting with status = 0x%x\n", ntStatus ));

    return ntStatus;
}


NTSTATUS
WTPUSB_CanQueueIoRequests(
    IN PDEVICE_OBJECT DeviceObject
)
/*++

Routine Description:

  Check device extension status flags; 

     Can't queue a new io request if device:
      1) is removed
      2) has a remove request pending

   This code is non-pageable.

Arguments:

    DeviceObject - pointer to the device object for this instance of the 82930
                    device.


Return Value:

    return STATUS_SUCCESS if can accept new io requests, else an NTSTATUS code

--*/
{
    PFDO_DEVICE_EXTENSION FDODeviceExtension = NULL;
    NTSTATUS ntStatus = STATUS_SUCCESS;

    DBGHDR_ASSERT(NULL != DeviceObject);
    FDODeviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT( ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->isFDO );

    if (FDODeviceExtension->DeviceRemoved)
        ntStatus = STATUS_DELETE_PENDING;

    return ntStatus;
}

NTSTATUS
WTPUSB_CanAcceptIoRequests(
    IN PDEVICE_OBJECT DeviceObject
)
/*++

Routine Description:

  Check device extension status flags; 

     Can't accept a new io request if device:
       removed or has a remove device requested,
       is stopped,
       has a stop device pending, or
       has a power query lock set

   This code is non-pageable.

Arguments:

    DeviceObject - pointer to the device object for this instance of the 82930
                    device.


Return Value:

    return STATUS_SUCCESS if can accept new io requests, else an NTSTATUS code

--*/
{
    PFDO_DEVICE_EXTENSION FDODeviceExtension = NULL;
    NTSTATUS ntStatus = STATUS_UNSUCCESSFUL;

    DBGHDR_ASSERT(NULL != DeviceObject);
    FDODeviceExtension = DeviceObject->DeviceExtension;
    DBGHDR_ASSERT( ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->isFDO );

    if (FDODeviceExtension->DeviceRemoved || FDODeviceExtension->RemoveDeviceRequested )
        ntStatus = STATUS_DELETE_PENDING;
    else if (FDODeviceExtension->StopDeviceRequested)
        ntStatus = STATUS_UNSUCCESSFUL;
    else if (FDODeviceExtension->powerQueryLock)
        ntStatus = STATUS_UNSUCCESSFUL;
    else if (FDODeviceExtension->DFURequested)
        ntStatus = STATUS_UNSUCCESSFUL;
    else if (FDODeviceExtension->DeviceStarted)
        ntStatus = STATUS_SUCCESS;

    return ntStatus;
}

NTSTATUS
WTPUSB_LoadUpperDriver(
    IN PDEVICE_OBJECT DeviceObject
    )
/*++

Routine Description:

    Called from IRP_MN_START_DEVICE handler.
    Loads RFBD.

    This code is pageable.

Arguments:

    DeviceObject - pointer to the FDO

Return Value:

    NT status code 

--*/
{
    NTSTATUS                ntStatus = STATUS_SUCCESS;
    PPDO_DEVICE_EXTENSION   PDODeviceExtension = NULL;
    PFDO_DEVICE_EXTENSION   FDODeviceExtension = NULL;
    WCHAR                   pdoName[] = RFBD_PDO_NAME;
    UNICODE_STRING          pdoUniName;
    ULONG                   length = 0;
    
    DBGHDR_KdPrint (DBGMASK_TRACE, ("WTPUSB_LoadUpperDriver(): Enter.\n"));
    PAGED_CODE();
    
    DBGHDR_ASSERT(NULL != DeviceObject);
    FDODeviceExtension = DeviceObject->DeviceExtension;

//    return ntStatus;

    // If you fail to pass the pdo name to IoCreateDevice, then you won't be able
    // to set the device interface to true (IoSetDeviceInterfaceState returns 
    // STATUS_OBJECT_NAME_NOT_FOUND ). So that's why we set this - because we want
    // ring 3 apps to access the bus driver's interface.
    RtlInitUnicodeString (&pdoUniName, pdoName);

    //
    // Create the PDO
    //
    ntStatus = IoCreateDevice( 
        ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->self->DriverObject,
            sizeof (PDO_DEVICE_EXTENSION),
        &pdoUniName,
        FILE_DEVICE_UNKNOWN,
        0,
        FALSE,
        &FDODeviceExtension->pdo);
    
    if (NT_SUCCESS(ntStatus))
    {
        PDODeviceExtension = (PPDO_DEVICE_EXTENSION)FDODeviceExtension->pdo->DeviceExtension;
        
        //
        // Initialize the rest of the PDO extension
        //
        ((PCOMMON_DEVICE_EXTENSION)PDODeviceExtension)->isFDO = FALSE;
        ((PCOMMON_DEVICE_EXTENSION)PDODeviceExtension)->self = FDODeviceExtension->pdo;
        
        PDODeviceExtension->parentFdo = FDODeviceExtension->self;
        
        PDODeviceExtension->DeviceStarted = FALSE; // irp_mn_start has yet to be received
        PDODeviceExtension->DeviceAttached = TRUE; // attached to the bus
        PDODeviceExtension->DeviceRemoved = FALSE; // no irp_mn_remove as of yet
        
        FDODeviceExtension->pdo->Flags &= ~DO_DEVICE_INITIALIZING;
        FDODeviceExtension->pdo->Flags |= DO_POWER_PAGABLE;
        FDODeviceExtension->pdo->Flags |= DO_DIRECT_IO;
        
        //
        // Get this information from the IOCTL parameters
        //
        length = sizeof(RFBD_HARDWARE_ID);  // * sizeof (WCHAR); sizeof returns the number of bytes in the string, 
                                            // check prefast warning 6260
        PDODeviceExtension->HardwareIDs = ExAllocatePoolWithTag(PagedPool, length, WTP_POOL_TAG);
        if (NULL != PDODeviceExtension->HardwareIDs)
        {
            RtlCopyMemory (PDODeviceExtension->HardwareIDs, RFBD_HARDWARE_ID, length);
            
            //
            // Tell the PnP manager that device relations have changed
            //
            IoInvalidateDeviceRelations (FDODeviceExtension->lowerPhysicalDeviceObject, BusRelations);
        } // End of test for memory allocation of HardwareIDs
        else
        {
            IoDeleteDevice(FDODeviceExtension->pdo);
            ntStatus = STATUS_INSUFFICIENT_RESOURCES;
        }
    } // end of success calling IoCreateDevice
    
    DBGHDR_KdPrint( DBGMASK_TRACE,("WTPUSB_LoadUpperDriver() Exit with %x\n", ntStatus));
    
    return ntStatus;
}

NTSTATUS
WTPUSB_UnLoadUpperDriver(
    IN PDEVICE_OBJECT DeviceObject
    )
/*++

Routine Description:

    Called from IRP_MN_REMOVE_DEVICE handler.
    Unloads RFBD.

    This code is pageable.

Arguments:

    DeviceObject - pointer to the FDO

Return Value:

    STATUS_SUCCESS or STATUS_UNSUCCESSFUL (if no pdo)

--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    PPDO_DEVICE_EXTENSION PDODeviceExtension = NULL;
    PFDO_DEVICE_EXTENSION FDODeviceExtension = NULL;

    DBGHDR_KdPrint (DBGMASK_TRACE, ("WTPUSB_UnLoadUpperDriver(): Enter.\n"));
    PAGED_CODE();
    DBGHDR_ASSERT(NULL != DeviceObject);

//    return ntStatus;

    FDODeviceExtension = DeviceObject->DeviceExtension;

    //
    // Get the PnP manager to remove the PDO
    //
    if (NULL != FDODeviceExtension->pdo)
    {
        PDODeviceExtension = FDODeviceExtension->pdo->DeviceExtension;
        PDODeviceExtension->DeviceAttached = FALSE;
        FDODeviceExtension->pdo = NULL;
        IoInvalidateDeviceRelations (FDODeviceExtension->lowerPhysicalDeviceObject, BusRelations);
        ntStatus = STATUS_SUCCESS;
    }
    else
    {
        // It may be that the PDO has already been removed
        ntStatus = STATUS_SUCCESS;
    }

    DBGHDR_KdPrint( DBGMASK_TRACE,("WTPUSB_UnLoadUpperDriver() Exit with %x\n", ntStatus));

    return ntStatus;
}

