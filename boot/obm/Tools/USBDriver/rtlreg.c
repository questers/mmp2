// Copyright (c) Marvell International Ltd. All Rights Reserved.

#include <wdm.h>
#include "rtlreg.h"
#include "wtpdbg.h"

/* 
 * RtlReg.c: provide get and set wrappers for the kernel Rtl... registry routines
 */



NTSTATUS
RegistryGetValueDword(
	PVOID		pvPath,				// can be unicode string or wstr
	ULONG		ulPathType,			// identifies type of pvPath: unicode or wstr
	PWSTR		pwstrSubKey,		// can be wstr or not used (NULL)
	PWSTR		pwstrValueName,		
	PULONG		pulValueData		// pts to dword buffer that will receive the data
)

/*
	Return the dword data associated with the registry value.

	This routine is written with the idea that most drivers have a registry key passed into them,
	and that each driver may want to make use of a subkey under this input registry key, eg. Parameters,
	and that the driver is interested in values kept under that sub key.

	That way, minimal registry key name manipulation needs to be done by callers of this routine.
	This routine will do all of the ugly unicode, wstr, and cstr manipulation for you.

	
 */
{

	PWSTR						pwstrAbsolutePath;
	BOOLEAN						bFreeAbsolutePath;
	RTL_QUERY_REGISTRY_TABLE	qrtQueryTable[2];
	ULONG						dwDefaultData = 0xffffffff;
	NTSTATUS					ntStatus;



	// we need to prepare Path parameter for the call to RtlQueryRegistryValues.

	ntStatus = MakeAbsolutePath( 
					&pwstrAbsolutePath, 
					&bFreeAbsolutePath, 
					pvPath, 
					ulPathType, 
					pwstrSubKey 
					);
	if( ntStatus != STATUS_SUCCESS )
		return ntStatus;



	// we need to prepare the QueryTable parameter for the call to RtlQueryRegistryValues
	RtlZeroMemory( qrtQueryTable, sizeof(qrtQueryTable) );

	qrtQueryTable[0].QueryRoutine	= NULL;
	qrtQueryTable[0].Flags			= RTL_QUERY_REGISTRY_DIRECT | RTL_QUERY_REGISTRY_REQUIRED;
	qrtQueryTable[0].Name			= pwstrValueName;
	qrtQueryTable[0].EntryContext	= pulValueData;


	// Defaultxxx fieldss are NULL since RTL_QUERY_REGISTRY_REQUIRED is specified...
	// Important Note: if query table 'Defaultxxx' fields are not NULL then
	// RtlQueryRegistryValues will never return STATUS_OBJECT_NOT_FOUND.


	// the following qrtQueryTable element identifies the end of our QueryTable array
	qrtQueryTable[1].QueryRoutine	= NULL;
	qrtQueryTable[1].Name			= NULL;


	// ok, ready to query the registry.
	ntStatus = RtlQueryRegistryValues( 
					RTL_REGISTRY_ABSOLUTE,
					pwstrAbsolutePath,
					qrtQueryTable,
					NULL,				// context: not used with RTL_QUERY_REGISTRY_DIRECT
					NULL				// string expand environment: not used with REG_DWORD
					);

	
	// free up the resources and return the status
	if( bFreeAbsolutePath )
		ExFreePool( pwstrAbsolutePath );

	return ntStatus;
}






NTSTATUS
RegistryGetValueString(
);

NTSTATUS
RegistryGetValueBinary(
);

NTSTATUS
RegistrySetValueDword(
	PVOID		pvPath,				// can be unicode or wstr
	ULONG		ulPathType,			// identifies pvPath type
	PWSTR		pwstrSubKey,		// can be wstr or not used (NULL)
	PWSTR		pwstrValueName,
	ULONG		ulValueData			// dword buffer that will contains the data
)
{
	PWSTR			pwstrAbsolutePath;
	BOOLEAN			bFreeAbsolutePath;
	NTSTATUS		ntStatus;



	// we need to prepare Path parameter for the call to RtlQueryRegistryValues.

	ntStatus = MakeAbsolutePath( 
					&pwstrAbsolutePath, 
					&bFreeAbsolutePath, 
					pvPath, 
					ulPathType, 
					pwstrSubKey 
					);
	if( ntStatus != STATUS_SUCCESS )
		return ntStatus;

	ntStatus = RtlWriteRegistryValue(
					RTL_REGISTRY_ABSOLUTE,
					pwstrAbsolutePath,
					pwstrValueName,
					REG_DWORD,
					&ulValueData,
					sizeof(ULONG)
					);

	if( bFreeAbsolutePath )
		ExFreePool( pwstrAbsolutePath );

	return ntStatus;

}


NTSTATUS
RegistrySetValueString(
);

NTSTATUS
RegistrySetValueBinary(
);





// auxilliary routines

NTSTATUS
MakeAbsolutePath(
	PWSTR		*pPwstrAbsolutePath,	// ptr to pwstr that points to wstr that contains absolute path
	PBOOLEAN	pbFreeAbsolutePath,		// ptr to boolean that indiciates of absolute path needs to be freed
	PVOID		pvPath,					// can be unicode or wstr
	ULONG		ulPathType,				// identifies pvPath type
	PWSTR		pwstrSubKey				// can be wstr or not used (NULL)
)

/*
	build an absolute path from one or two components.

	the first component can be a unicode string or a wstr.
	if it is a unicode string, then a unicode string to wstr conversion is performed.

	if the first component is a complete path, the the second component should be NULL.
	otherwise, the second component will be concatenated with the first component.
	
 */
{

	PWSTR			pwstrAbsolutePath;
	BOOLEAN			bFreeAbsolutePath;
	ULONG			ulAbsolutePathLength;
	NTSTATUS		ntStatus;



	// we need to prepare Path parameter for the call to RtlQueryRegistryValues.
	
	// if pvPath is a wstr and pvSubKey is null, then we can use pvPath without conversion.
	// otherwise we need to build an absolute registry path from pvPath and pwstrSubKey.
	if( ulPathType == RTLREG_PARAM_WSTR && pwstrSubKey == NULL )
	{
		pwstrAbsolutePath = (PWSTR)pvPath;
		bFreeAbsolutePath = FALSE;
	}
	else
	{
		// we need to build pwstrAbsolutePath from pvPath and pwstrSubKey
		
		// pwstrAbsolutePath will be length of wstr version of pvPath 
		// + (if pwstrSubKey not null) length of wchar(\) + length of pwstrSubKey
		if( RTLREG_PARAM_WSTR == ulPathType )
			ulAbsolutePathLength = sizeof(wchar_t) * wcslen( (PWSTR)pvPath );
		else
			ulAbsolutePathLength = ((PUNICODE_STRING)pvPath)->Length;

		if( pwstrSubKey != NULL )
			ulAbsolutePathLength += sizeof(wchar_t) * wcslen( pwstrSubKey ) + sizeof(wchar_t);	// space for wchar(\)

		ulAbsolutePathLength += sizeof(wchar_t);	// room for wchar null terminator

		// get memory for pwstrAbsolutePath
		pwstrAbsolutePath = (PWSTR)ExAllocatePoolWithTag( NonPagedPool, ulAbsolutePathLength, WTP_POOL_TAG );
		if( pwstrAbsolutePath == NULL )
		{
			// error
			return STATUS_NO_MEMORY;
		}

		bFreeAbsolutePath = TRUE;

		// initialize pwstrAbsolutePath with the string from pvPath
		if( RTLREG_PARAM_WSTR == ulPathType )
			wcscpy( pwstrAbsolutePath, (PWSTR)pvPath );
		else
		{
			wcsncpy( pwstrAbsolutePath, ((PUNICODE_STRING)pvPath)->Buffer, (((PUNICODE_STRING)pvPath)->Length)/sizeof(wchar_t));
			pwstrAbsolutePath[(((PUNICODE_STRING)pvPath)->Length)/sizeof(wchar_t)] = L'\0';
		}

		// if pwstrSubKey is not null, then concatenate a path separator and the pwstrSubKey
		if( pwstrSubKey != NULL )
		{
			wcscat( pwstrAbsolutePath, L"\\" );
			wcscat( pwstrAbsolutePath, pwstrSubKey );
		}
	}


	// done.
	*pPwstrAbsolutePath = pwstrAbsolutePath;
	*pbFreeAbsolutePath = bFreeAbsolutePath;

	return STATUS_SUCCESS;
}



