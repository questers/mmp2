/*++

Copyright (c) 1998-1999 Intel Corporation

Module Name:

    $Workfile:: power.c                   $

Abstract:

    Implementation of the power specific routines

Environment:

    Kernel mode only

Notes:

    Copyright (c) 1998-1999 Intel Corporation.  All Rights Reserved.

    The receipt of or possession of this program does not convey
    any rights to reproduce its contents or to manufacture,
    use, or sell anything that it may describe, in whole, or 
    in part, without specific written consent of Intel Corporation.


Revision History:
    $Date:: 5/12/00 2:29p                 $ Date and time of last check in
    $Revision:: 1                         $ Visual SourceSafe version number
    $Nokeywords::                         $

--*/

#include <wdm.h>
#include <wmistr.h>
#include <usbdi.h>
#include <usbdlib.h>
#include "wtpdbg.h"
#include "wtpdfu.h"

NTSTATUS
WTPUSB_ProcessFDOPowerIrp (
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp,
    IN PIO_STACK_LOCATION irpStack,
    IN PFDO_DEVICE_EXTENSION FDODeviceExtension
    )
/*++

Routine Description:

    Handles all Power Irps sent to this FDO.

Arguments:

    DeviceObject    - pointer to our device object (FDO)

    Irp             - pointer to an I/O Request Packet

    irpStack        - Location on the stack

    FDODeviceExtension - pointer to our FDODeviceExtension

Return Value:

    NT status code

--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    POWER_STATE desiredDevicePowerState;
    BOOLEAN bGoingToD0 = FALSE;
    KEVENT event;

    DBGHDR_KdPrint (DBGMASK_POWER_TRACE, ("WTPUSB_ProcessFDOPowerIrp(): Enter.\n"));

    WTPUSB_IncrementIoCount(DeviceObject);

    switch (irpStack->MinorFunction)
    {
    case IRP_MN_WAIT_WAKE:

		// A driver sends IRP_MN_WAIT_WAKE to indicate that the system should 
		// wait for its device to signal a wake event. The exact nature of the event
		// is device-dependent. 
		// Drivers send this IRP for two reasons: 
		// 1) To allow a device to wake the system
		// 2) To wake a device that has been put into a sleep state to save power
		//    but still must be able to communicate with its driver under certain circumstances. 
		// When a wake event occurs, the driver completes the IRP and returns 
		// STATUS_SUCCESS. If the device is sleeping when the event occurs, 
		// the driver must first wake up the device before completing the IRP. 
		// In a completion routine, the driver calls PoRequestPowerIrp to send a 
		// PowerDeviceD0 request. When the device has powered up, the driver can 
		//  handle the IRP_MN_WAIT_WAKE request.

        // deviceExtension->DeviceCapabilities.DeviceWake specifies the lowest device power state (least powered)
        // from which the device can signal a wake event 
        FDODeviceExtension->PowerDownLevel = FDODeviceExtension->deviceCapabilities.DeviceWake;
        
        if  ( ( PowerDeviceD0 == FDODeviceExtension->CurrentDeviceState )  ||
            ( FDODeviceExtension->deviceCapabilities.DeviceWake > ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->CurrentDeviceState ) ) {
            //
            //    STATUS_INVALID_DEVICE_STATE is returned if the device in the PowerD0 state
            //    or a state below which it can support waking, or if the SystemWake state
            //    is below a state which can be supported. A pending IRP_MN_WAIT_WAKE will complete
            //    with this error if the device's state is changed to be incompatible with the wake 
            //    request.
            
            //  If a driver fails this IRP, it should complete the IRP immediately without
            //  passing the IRP to the next-lower driver.
            ntStatus = STATUS_INVALID_DEVICE_STATE;
            Irp->IoStatus.Status = ntStatus;
            IoCompleteRequest (Irp,IO_NO_INCREMENT );
            DBGHDR_KdPrint( DBGMASK_ERROR, ( "WTPUSB_ProcessPowerIrp(), ntStatus STATUS_INVALID_DEVICE_STATE\n" ) );
            WTPUSB_DecrementIoCount(DeviceObject);
        }
        else
        {
            // flag we're enabled for wakeup
            FDODeviceExtension->EnabledForWakeup = TRUE;
            
            // init an event for our completion routine to signal when PDO is done with this Irp
            KeInitializeEvent(&event, NotificationEvent, FALSE);
            
            // If not failing outright, pass this on to our PDO for further handling
            IoCopyCurrentIrpStackLocationToNext(Irp);
            
            // Set a completion routine so it can signal our event when
            //  the PDO is done with the Irp
            IoSetCompletionRoutine(Irp,
                WTPUSB_OnRequestComplete,
                &event,  // pass the event to the completion routine as the Context
                TRUE,    // invoke on success
                TRUE,    // invoke on error
                TRUE);   // invoke on cancellation
            
            PoStartNextPowerIrp(Irp);
            ntStatus = PoCallDriver(FDODeviceExtension->topOfStackDeviceObject, Irp);
            
            // if PDO is not done yet, wait for the event to be set in our completion routine
            if (STATUS_PENDING == ntStatus) {
                // wait for irp to complete
                NTSTATUS waitStatus = KeWaitForSingleObject(
                    &event,
                    Suspended,
                    KernelMode,
                    FALSE,
                    NULL);
            }
            
            // now tell the device to actually wake up
            WTPUSB_SelfSuspendOrActivate( DeviceObject, Activate);
            
            // flag we're done with wakeup irp
            FDODeviceExtension->EnabledForWakeup = FALSE;
            WTPUSB_DecrementIoCount(DeviceObject);
        } // end of test for invalid device state
        break;
        
    case IRP_MN_SET_POWER:
        // Check to see if this was a system state or device state request
        switch (irpStack->Parameters.Power.Type) 
        {
        case SystemPowerState:
            // If we were sent a system power state && we are enabled for wakeup
            // then get our corresponding device state and set it.
            if (FDODeviceExtension->EnabledForWakeup)
            {
                desiredDevicePowerState.DeviceState =
                    FDODeviceExtension->deviceCapabilities.DeviceState[irpStack->Parameters.Power.State.SystemState];
            }
            else
            {
                // We are not enabled for wakeup - so if we're not in S0 then turn off
                if (PowerSystemWorking != irpStack->Parameters.Power.State.SystemState)
                    desiredDevicePowerState.DeviceState = PowerDeviceD3;
                else
                    desiredDevicePowerState.DeviceState = PowerDeviceD0;
            }

            // RH - Do we call PoSetPowerState here? Or only when we are actually about to set it?

            //
            // Now we know what our desired device state is, so set it
            //
            if (desiredDevicePowerState.DeviceState != ((PCOMMON_DEVICE_EXTENSION)FDODeviceExtension)->CurrentDeviceState) {
                
                WTPUSB_IncrementIoCount(DeviceObject);
                FDODeviceExtension->powerIrp = Irp;
                ntStatus = PoRequestPowerIrp(FDODeviceExtension->topOfStackDeviceObject,
                    IRP_MN_SET_POWER,
                    desiredDevicePowerState,
                    // completion routine will pass the Irp down to the PDO
                    WTPUSB_PoRequestCompletion, 
                    DeviceObject,
                    NULL);
            } // end if if desired state != current state
            else 
            {
                // Pass the irp on down - we're already in the desired state
                IoCopyCurrentIrpStackLocationToNext(Irp);
                PoStartNextPowerIrp(Irp);
                ntStatus = PoCallDriver(FDODeviceExtension->topOfStackDeviceObject, Irp);
                WTPUSB_DecrementIoCount(DeviceObject);
            }
            break;
        case DevicePowerState:
            // If we were sent a device power state
            // and it was for D1, D2, or D3 - set the state in the device extension.
            bGoingToD0 = WTPUSB_SetDevicePowerState(DeviceObject, irpStack->Parameters.Power.State.DeviceState);
            IoCopyCurrentIrpStackLocationToNext(Irp);
            
            if (bGoingToD0)
            {
                // If we were sent a device power state
                // and it was for D0 - set a completion routine and update the state in
                // the device extension only when the lower driver has powered on. This
                // ensures that no IO will be attempted before we're ready to deal with it.
                IoSetCompletionRoutine(Irp,
                    WTPUSB_PowerIrp_Complete,
                    DeviceObject,
                    TRUE,            // invoke on success
                    TRUE,            // invoke on error
                    TRUE);           // invoke on cancellation of the Irp
            }
            
            PoStartNextPowerIrp(Irp);
            ntStatus = PoCallDriver(FDODeviceExtension->topOfStackDeviceObject, Irp);
            
            if ( !bGoingToD0 ) // completion routine will decrement
                WTPUSB_DecrementIoCount(DeviceObject);
            
            break;
        default:
            break;
        } // end of switch on power.type
        
        break;
        case IRP_MN_QUERY_POWER:
            FDODeviceExtension->powerQueryLock = TRUE;
            IoCopyCurrentIrpStackLocationToNext(Irp);
            PoStartNextPowerIrp(Irp);
            ntStatus = PoCallDriver(FDODeviceExtension->topOfStackDeviceObject, Irp);
            break;
        default:
            // We don't know what it is, so pass it to our bus driver (USBD)
            IoCopyCurrentIrpStackLocationToNext(Irp);
            PoStartNextPowerIrp(Irp);
            ntStatus = PoCallDriver(FDODeviceExtension->topOfStackDeviceObject, Irp);
            break;
    } // end of switch on the MinorFunction

    DBGHDR_KdPrint (DBGMASK_POWER_TRACE, ("WTPUSB_ProcessFDOPowerIrp(): Exit with %X\n", ntStatus));
    return ntStatus;
} 

NTSTATUS
WTPUSB_ProcessPDOPowerIrp(
    IN PDEVICE_OBJECT DeviceObject,
    IN PIRP Irp,
    IN PIO_STACK_LOCATION irpStack,
    IN PPDO_DEVICE_EXTENSION PDODeviceExtension
    )
/*++

Routine Description:

    Handles all Power Irps sent to this PDO.

Arguments:

    DeviceObject    - pointer to one of our device objects (PDO)

    Irp             - pointer to an I/O Request Packet

    irpStack        - Location on the stack

    PDODeviceExtension - pointer to our deviceExtension

Return Value:

    NT status code

--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    POWER_STATE         powerState;
    POWER_STATE_TYPE    powerType;
    
    DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_ProcessPDOPowerIrp() Enter\n"));

    powerType = irpStack->Parameters.Power.Type;
    powerState = irpStack->Parameters.Power.State;

    switch (irpStack->MinorFunction)
    {
    case IRP_MN_SET_POWER:
        switch (powerType) 
        {
        case DevicePowerState:
            PoSetPowerState (PDODeviceExtension->self, powerType, powerState);
            PDODeviceExtension->CurrentDeviceState = powerState.DeviceState;
            break;
        case SystemPowerState:
            // RH - How do we handle devices that need to be in the wait_wake state?
            //  -- this is definitely NOT what we want to do:
            if (PowerSystemWorking == powerState.SystemState)
            {
                powerState.DeviceState = PowerDeviceD0;
                PoRequestPowerIrp (PDODeviceExtension->self,
                                   IRP_MN_SET_POWER,
                                   powerState,
                                   NULL, // no completion
                                   NULL, // no context
                                   NULL); // no return IRP
            } 
            else 
            {
                powerState.DeviceState = PowerDeviceD3;
                PoRequestPowerIrp (PDODeviceExtension->self,
                                   IRP_MN_SET_POWER,
                                   powerState,
                                   NULL, // no completion
                                   NULL, // no context
                                   NULL); // no return IRP
            }
            PDODeviceExtension->CurrentSystemState = powerState.SystemState;
            break;
        } // end of switch on type of power state
        break;
    case IRP_MN_WAIT_WAKE:
    case IRP_MN_QUERY_POWER:
    default:
        ntStatus = STATUS_NOT_IMPLEMENTED;
        break;
    } // end of switch on the MinorFunction

    PoStartNextPowerIrp (Irp);
    Irp->IoStatus.Status = ntStatus;
    // RH - Do I need to raise to dispatch before I complete this?
    IoCompleteRequest (Irp, IO_NO_INCREMENT);
    
    DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_ProcessPDOPowerIrp() Exit with %x\n", ntStatus));
    
    return ntStatus;
}

BOOLEAN
WTPUSB_SetDevicePowerState(
    IN PDEVICE_OBJECT DeviceObject,
    IN DEVICE_POWER_STATE DeviceState
    )
/*++

Routine Description:

    This routine is called when An IRP_MN_SET_POWER of type 'DevicePowerState'
    has been received by WTPUSB_ProcessPowerIrp().

Arguments:

    DeviceObject - Pointer to the device object for the class device.

    DeviceState - Device specific power state to set the device in to.


Return Value:

    For requests to DeviceState D0 ( fully on ), returns TRUE to signal caller 
    that we must set a completion routine and finish there.

--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    PFDO_DEVICE_EXTENSION FDODeviceExtension = NULL;
    BOOLEAN bResult = FALSE;
    KIRQL oldIrql;
    POWER_STATE DeviceStateStruct;
    
    DBGHDR_KdPrint (DBGMASK_POWER_TRACE, ("WTPUSB_SetDevicePowerState(): Enter.\n"));
    dbgT(DBG_DEVEXT(DeviceObject), 'P', oldIrql);

    DeviceStateStruct.DeviceState = DeviceState;
    PoSetPowerState(DeviceObject, DevicePowerState, DeviceStateStruct);    
    FDODeviceExtension = (PFDO_DEVICE_EXTENSION) DeviceObject->DeviceExtension;
    
    switch (DeviceState) 
    {
    case PowerDeviceD3:
        //
        // Device will be going OFF, 
        // RH - TODO: add any needed device-dependent code to save state here.
        //
        DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_SetDevicePowerState() PowerDeviceD3 (OFF)\n"));
        FDODeviceExtension->CurrentDeviceState = DeviceState;
        break;
    case PowerDeviceD1:
    case PowerDeviceD2:
        //
        // power states D1,D2 translate to USB suspend
        DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_SetDevicePowerState() PowerDeviceD1 or PowerDeviceD2\n"));
        FDODeviceExtension->CurrentDeviceState = DeviceState;
        break;
    case PowerDeviceD0:
        DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_SetDevicePowerState() PowerDeviceD0 (ON)\n"));
        // We'll need to finish the rest in the completion routine;
        //   signal caller we're going to D0 and will need to set a completion routine
        bResult = TRUE;
        break;
    default:
        DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_SetDevicePowerState() Bad DeviceState %d\n",DeviceState));
        DBGHDR_ASSERT(FALSE);
    } // end of switch on DeviceState

    dbgT(DBG_DEVEXT(DeviceObject), 'p', oldIrql);
    DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_SetDevicePowerState() Exit with %x\n", ntStatus));

    return bResult;
}


NTSTATUS
WTPUSB_SelfSuspendOrActivate(
    IN PDEVICE_OBJECT DeviceObject,
    IN WTPUSB_SUSPEND_OR_ACTIVATE suspendOrActivate
    )
/*++

Routine Description:

    Called by AddDevice to power down until needed (i.e., till a pipe is actually opened).
    Called when the last pipe is closed.
    Called when the first pipe is opened

Arguments:

    DeviceObject - Pointer to the device object

    suspendOrActivate - Enumerated type to activate or suspend the device

Return Value:

    If the operation is not attemtped, SUCCESS is returned.
    If the operation is attemtped, the value is the final status from the operation.

--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    POWER_STATE PowerState;
    PFDO_DEVICE_EXTENSION FDODeviceExtension = NULL;

    DBGHDR_KdPrint (DBGMASK_POWER_TRACE, ("WTPUSB_SelfSuspendOrActivate(): Enter.\n"));
    FDODeviceExtension = DeviceObject->DeviceExtension;

	// Can't accept request if:
    //  1) device is removed, 
    //  2) has never been started, 
    //  3) is stopped,
    //  4) has a remove request pending,
    //  5) has a stop device pending
    ntStatus = WTPUSB_CanAcceptIoRequests(DeviceObject);
    if (!NT_SUCCESS(ntStatus)) 
    {
        DBGHDR_KdPrint( DBGMASK_ERROR,("WTPUSB_SelfSuspendOrActivate(): Cannot accept IO requests!\n"));
    }
    
    // don't do anything if any System-generated Device Pnp irps are pending
    else if ( NULL != FDODeviceExtension->powerIrp ) 
    {
        DBGHDR_KdPrint( DBGMASK_ERROR,("WTPUSB_SelfSuspendOrActivate(): Pending system power IRP\n"));
        ntStatus = STATUS_UNSUCCESSFUL;
    }
    
    // don't do anything if any self-generated Device Pnp irps are pending
    else if (FDODeviceExtension->SelfPowerIrp) 
    {
        DBGHDR_KdPrint( DBGMASK_ERROR,("WTPUSB_SelfSuspendOrActivate(): Pending self power IRP\n"));
        ntStatus = STATUS_UNSUCCESSFUL;
    }
    
    // don't auto-suspend if any pipes are open
    // RH - If uppler layers still havereferences to open pipes - will suspending do something bad
    //      to the device? 
    else if (Suspend == suspendOrActivate && 
        (0 != FDODeviceExtension->ACLInstanceCount) &&
        (0 != FDODeviceExtension->SCOInstanceCount) &&
        (0 != FDODeviceExtension->ControlInstanceCount)) 
    {
        DBGHDR_KdPrint( DBGMASK_ERROR,("WTPUSB_SelfSuspendOrActivate(): Still have open pipes\n"));
        ntStatus = STATUS_UNSUCCESSFUL;
    }
    
    // don't auto-activate if no pipes are open  (ForceActivate skips this step)
    else if ( Activate == suspendOrActivate && 
        (0 == FDODeviceExtension->ACLInstanceCount) &&
        (0 == FDODeviceExtension->SCOInstanceCount) &&
        (0 == FDODeviceExtension->ControlInstanceCount)) 
    {
        DBGHDR_KdPrint( DBGMASK_ERROR,("WTPUSB_SelfSuspendOrActivate(): No pipes open - no need to activate\n"));
        ntStatus = STATUS_UNSUCCESSFUL;
    }
    
    else if (Suspend == suspendOrActivate)
        PowerState.DeviceState = FDODeviceExtension->PowerDownLevel;
    else
        PowerState.DeviceState = PowerDeviceD0;  
    
    ntStatus = WTPUSB_SelfRequestPowerIrp(DeviceObject, PowerState);
    DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_SelfSuspendOrActivate() Exit with %x\n", ntStatus));

    return ntStatus;

}

NTSTATUS
WTPUSB_SelfRequestPowerIrp(
    IN PDEVICE_OBJECT DeviceObject,
    IN POWER_STATE PowerState
    )
/*++

Routine Description:

    This routine is called by WTPUSB_SelfSuspendOrActivate() to
    actually make the system request for a powerdown/up to PowerState.
    It first checks to see if we are already in Powerstate and immediately
    returns  SUCCESS with no further processing if so


Arguments:

    DeviceObject - Pointer to the device object

    PowerState. power state requested, e.g PowerDeviceD0.


Return Value:

    The function value is the final status from the operation.

--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    PFDO_DEVICE_EXTENSION FDODeviceExtension;
    PIRP pIrp = NULL;

    DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_SelfRequestPowerIrp() Entering\n"));
    PAGED_CODE();

    FDODeviceExtension =  DeviceObject->DeviceExtension;

    // This should have been reset in completion routine
    DBGHDR_ASSERT( !FDODeviceExtension->SelfPowerIrp );
    
    if (FDODeviceExtension->CurrentDeviceState ==  PowerState.DeviceState)
    {
        ntStatus = STATUS_SUCCESS;  // nothing to do
    }
    else
    {
        WTPUSB_IncrementIoCount(DeviceObject);
        
        // flag we're handling a self-generated power irp
        FDODeviceExtension->SelfPowerIrp = TRUE;
        
        // request the Irp
        ntStatus = PoRequestPowerIrp(FDODeviceExtension->topOfStackDeviceObject, 
            IRP_MN_SET_POWER,
            PowerState,
            WTPUSB_PoSelfRequestCompletion,
            DeviceObject,
            NULL);
        
        if  (STATUS_PENDING == ntStatus) 
        { 
            // We only need to wait for completion if we're powering up
            if ( (ULONG) PowerState.DeviceState < FDODeviceExtension->PowerDownLevel ) 
            {
                NTSTATUS waitStatus;
                waitStatus = KeWaitForSingleObject(
                    &FDODeviceExtension->SelfRequestedPowerIrpEvent,
                    Suspended,
                    KernelMode,
                    FALSE,
                    NULL);
            }
            KeClearEvent(&FDODeviceExtension->SelfRequestedPowerIrpEvent);
            ntStatus = STATUS_SUCCESS;
            FDODeviceExtension->SelfPowerIrp = FALSE;
            FDODeviceExtension->powerQueryLock = FALSE;
        } // end of if status_pending
    } // end of else from check against current power state

    DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_SelfRequestPowerIrp() Exit with %x\n", ntStatus));
    return ntStatus;
}


NTSTATUS
WTPUSB_PoRequestCompletion(
    IN PDEVICE_OBJECT       DeviceObject,
    IN UCHAR                MinorFunction,
    IN POWER_STATE          PowerState,
    IN PVOID                Context,
    IN PIO_STATUS_BLOCK     IoStatus
    )
/*++

Routine Description:

	This is the completion routine set in a call to PoRequestPowerIrp()
	that was made in WTPUSB_ProcessPowerIrp() in response to receiving
    an IRP_MN_SET_POWER of type 'SystemPowerState' when the device was
	not in a compatible device power state. In this case, a pointer to
	the IRP_MN_SET_POWER Irp is saved into the FDO device extension 
	(deviceExtension->PowerIrp), and then a call must be
	made to PoRequestPowerIrp() to put the device into a proper power state,
	and this routine is set as the completion routine.

    We decrement our pending io count and pass the saved IRP_MN_SET_POWER Irp
	on to the next driver

Arguments:

    DeviceObject - Pointer to the device object for the class device.
        Note that we must get our own device object from the Context

    Context - Driver defined context, in this case our own functional device object ( FDO )

Return Value:

    The function value is the final status from the operation.

--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    PIRP irp = NULL;
    PFDO_DEVICE_EXTENSION FDODeviceExtension = NULL;
    PDEVICE_OBJECT deviceObject = NULL;

    DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_PoRequestCompletion() Entering\n"));

    deviceObject = Context;
    FDODeviceExtension = deviceObject->DeviceExtension;

	// Get the Irp we saved for later processing in WTPUSB_ProcessPowerIrp()
	// when we decided to request the Power Irp that this routine 
	// is the completion routine for.
    irp = FDODeviceExtension->powerIrp;

	// We will return the status set by the PDO for the power request we're completing
    ntStatus = IoStatus->Status;

    // we should not be in the midst of handling a self-generated power irp
    DBGHDR_ASSERT (FALSE == FDODeviceExtension->SelfPowerIrp);

    // we must pass down to the next driver in the stack
    IoCopyCurrentIrpStackLocationToNext(irp);

    // Calling PoStartNextPowerIrp() indicates that the driver is finished
    // with the previous power IRP, if any, and is ready to handle the next power IRP.
    // It must be called for every power IRP.Although power IRPs are completed only once,
    // typically by the lowest-level driver for a device, PoStartNextPowerIrp must be called
    // for every stack location. Drivers must call PoStartNextPowerIrp while the current IRP
    // stack location points to the current driver. Therefore, this routine must be called
    // before IoCompleteRequest, IoSkipCurrentStackLocation, and PoCallDriver.

    PoStartNextPowerIrp(irp);

    // PoCallDriver is used to pass any power IRPs to the PDO instead of IoCallDriver.
    // When passing a power IRP down to a lower-level driver, the caller should use
    // IoSkipCurrentIrpStackLocation or IoCopyCurrentIrpStackLocationToNext to copy the IRP to
    // the next stack location, then call PoCallDriver. Use IoCopyCurrentIrpStackLocationToNext
    // if processing the IRP requires setting a completion routine, or IoSkipCurrentStackLocation
    // if no completion routine is needed.

    KeWaitForSingleObject (&(FDODeviceExtension->irpSyncEvent), Executive, KernelMode, FALSE, NULL); 
    PoCallDriver(FDODeviceExtension->topOfStackDeviceObject, irp);
    KeSetEvent(&(FDODeviceExtension->irpSyncEvent), 0, FALSE); 
    WTPUSB_DecrementIoCount(deviceObject);
    FDODeviceExtension->powerIrp = NULL;

    DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_PoRequestCompletion() Exit with %x\n", ntStatus));
    return ntStatus;
}


NTSTATUS
WTPUSB_PowerIrp_Complete(
    IN PDEVICE_OBJECT NullDeviceObject,
    IN PIRP Irp,
    IN PVOID Context
    )
/*++

Routine Description:

    This routine is called when An IRP_MN_SET_POWER of type 'DevicePowerState'
    has been received by WTPUSB_ProcessPowerIrp(), and that routine has  determined
        1) the request is for full powerup ( to PowerDeviceD0 ), and
        2) We are not already in that state
    A call is then made to PoRequestPowerIrp() with this routine set as the completion routine.


Arguments:

    DeviceObject - Pointer to the device object for the class device.

    Irp - Irp completed.

    Context - Driver defined context.

Return Value:

    The function value is the final status from the operation.

--*/
{
    NTSTATUS ntStatus = STATUS_SUCCESS;
    PDEVICE_OBJECT deviceObject = NULL;
    PIO_STACK_LOCATION irpStack = NULL;
    PFDO_DEVICE_EXTENSION FDODeviceExtension = NULL;

    DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_PowerIrp_Complete() Entering\n"));

    deviceObject = (PDEVICE_OBJECT)Context;
    FDODeviceExtension = (PFDO_DEVICE_EXTENSION)deviceObject->DeviceExtension;

    //  If the lower driver returned PENDING, mark our stack location as pending also.
    if (Irp->PendingReturned) {
        IoMarkIrpPending(Irp);
    }

    irpStack = IoGetCurrentIrpStackLocation (Irp);

    // We can assert that we're a  device powerup-to D0 request,
    // because that was the only type of request we set a completion routine
    // for in the first place
    DBGHDR_ASSERT(IRP_MJ_POWER == irpStack->MajorFunction);
    DBGHDR_ASSERT(IRP_MN_SET_POWER == irpStack->MinorFunction);
    DBGHDR_ASSERT(DevicePowerState == irpStack->Parameters.Power.Type);
    DBGHDR_ASSERT(PowerDeviceD0 == irpStack->Parameters.Power.State.DeviceState);

    // Now that we know we've let the lower drivers do what was needed to power up,
    //  we can set our device extension flags accordingly
    FDODeviceExtension->CurrentDeviceState = PowerDeviceD0;
    Irp->IoStatus.Status = ntStatus;
    WTPUSB_DecrementIoCount(deviceObject);
    
    DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_PowerIrp_Complete() Exit with %x\n", ntStatus));
    return ntStatus;
}

NTSTATUS
WTPUSB_PoSelfRequestCompletion(
    IN PDEVICE_OBJECT       DeviceObject,
    IN UCHAR                MinorFunction,
    IN POWER_STATE          PowerState,
    IN PVOID                Context,
    IN PIO_STATUS_BLOCK     IoStatus
    )
/*++

Routine Description:

    This routine is called when the driver completes a self-originated power IRP 
	that was generated by a call to WTPUSB_SelfSuspendOrActivate().
    We power down whenever the last pipe is closed and power up when the first pipe is opened.

    For power-up , we set an event in our FDO extension to signal this IRP done
    so the power request can be treated as a synchronous call.
    We need to know the device is powered up before opening the first pipe, for example.
    For power-down, we do not set the event, as no caller waits for powerdown complete.

Arguments:

    DeviceObject - Pointer to the device object for the class device. ( Physical Device Object )

    Context - Driver defined context, in this case our FDO ( functional device object )

Return Value:

    The function value is the final status from the operation.

--*/
{
    PDEVICE_OBJECT deviceObject = Context;
    PFDO_DEVICE_EXTENSION FDODeviceExtension = deviceObject->DeviceExtension;
    NTSTATUS ntStatus = IoStatus->Status;

    DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_PoSelfRequestCompletion() Entering\n"));

    // we should not be in the midst of handling a system-generated power irp
    DBGHDR_ASSERT( NULL == FDODeviceExtension->powerIrp );

    deviceObject = Context;
    FDODeviceExtension = deviceObject->DeviceExtension;
    ntStatus = IoStatus->Status;

    // We only need to set the event if we're powering up;
    // No caller waits on power down complete
    if ( (ULONG) PowerState.DeviceState < FDODeviceExtension->PowerDownLevel ) {

        // Trigger Self-requested power irp completed event;
        //  The caller is waiting for completion
        KeSetEvent(&FDODeviceExtension->SelfRequestedPowerIrpEvent, 1, FALSE);
    }

    WTPUSB_DecrementIoCount(deviceObject);
    DBGHDR_KdPrint( DBGMASK_POWER_TRACE,("WTPUSB_PoSelfRequestCompletion() Exit with %x\n", ntStatus));

    return ntStatus;
}

