/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into
 *  products for purposes authorized by the license agreement provided they
 *  include this notice and the associated copyright notice with any such
 *  product.
 *  The information in this file is provided "AS IS" without warranty.
 *
 *
 *  FILENAME:	ipc.c
 *
 *  PURPOSE: 	IPC message passing routines.
 *
******************************************************************************/

#include "Errors.h"
#include "tim.h"
#include "PlatformConfig.h"
#if BOOTROM
#include "BootROM.h"
#endif
#include "predefines.h"
#include "IPC.h"
#include "ipc_interface.h"

void IPCInit()
{
      // Enable APBC_IPC_CLK_RST. This enables IPCA and IPCB
      //set clk bits and reset
      reg_write(APBC_IPC_CLK_RST, (APBC_IPC_CLK_RST_APBCLK | APBC_IPC_CLK_RST_FNCLK | APBC_IPC_CLK_RST_RST));
      //then take out of reset
      reg_bit_clr(APBC_IPC_CLK_RST, APBC_IPC_CLK_RST_RST);
}

void IPCWrite( unsigned AB, unsigned long data )
{
      IPC_T *IPCBase;

      IPCBase = (IPC_T*)( (AB==IPC_A)? IPCA_BASE : IPCB_BASE );

      IPCBase->Ipc_Wdr  = data;
      IPCBase->Ipc_Isrw = IPC_SET_MSG_INT;
}

unsigned long IPCRead( unsigned AB )
{
      IPC_T *IPCBase;

      IPCBase = (IPC_T*)( (AB==IPC_A)? IPCA_BASE : IPCB_BASE );

      // note: before reading the IPC, software must force the IPC to latch the
      // current data. Any write to the IPC unit will cause the data to be latched.
      IPCBase->Ipc_Isrr = 0;  // use the RO ISRR as a target for the write. it's non-destructive.

      return IPCBase->Ipc_Rdr;
}
