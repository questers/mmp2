/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 *
 *  FILENAME:	RegInstructions.c
 *
 *  PURPOSE: 	Contain code to handle register manipulations from 
 *				op codes contained in the TIM header.
 *
******************************************************************************/

#include "RegInstructions.h"

int ValidAddress( volatile unsigned int *Address){

	return TRUE;
} 

void WriteInstruction(unsigned int *Address, unsigned int Value){
	
	if( ValidAddress(Address))
		*Address = Value;
	return;

}

void ReadInstruction(volatile unsigned int *Address, unsigned int NumReads){
	unsigned int bitbucket;
	if( ValidAddress(Address)){
		while(NumReads){
			bitbucket = *Address;
			NumReads--;
		}
	}
 	return;
}


//
//	This routine will poll the address and check to see if a bit is set according to the mask
//     the read will occur once every ms until the time out is hit or the bit is set.  
//
//		Address - register address to poll
//		Mask	- bit to check
//		Timeout - time out value in milli seconds
//
unsigned int  WaitForBitSetInstruction(volatile unsigned int *Address, unsigned int Mask, unsigned int Timeout){
	unsigned int bitbucket;
	unsigned int Retval = NoError;
	if( ValidAddress(Address))
	{
		while(Timeout)
		{
			bitbucket = *Address;
			if ((bitbucket & Mask) == Mask)
				break;
			Delay(1000);
			Timeout--;
		}
		if(Timeout == 0)
			Retval = InstructionTimeout;
	}
 	return Retval;
}
//
//	This routine will poll the address and check to see if a bit is clear according to the mask
//     the read will occur once every ms until the time out is hit or the bit is cleared.  
//
//		Address - register address to poll
//		Mask	- bit to check
//		Timeout - time out value in milli seconds
//
unsigned int WaitForBitClearInstruction(unsigned int *Address, unsigned int Mask, unsigned int Timeout){
	unsigned int bitbucket; 
	unsigned int Retval = NoError;
	if( ValidAddress(Address))
	{
		bitbucket = *Address;
		while((bitbucket & Mask) == Mask)
		{
			if (Timeout == 0)
			{
				Retval = InstructionTimeout;
				break;
			}
			Delay(1000);
			Timeout--;
			bitbucket = *Address;
		}
	}
 	return Retval;
}

void AndInstruction(unsigned int *Address, unsigned int Value)
{
	unsigned int RegVal;
	if( ValidAddress(Address))
	{
			RegVal = *Address;
			RegVal = RegVal & Value;
			*Address = RegVal;
	}
 	return;
}

void OrInstruction(unsigned int *Address, unsigned int Value)
{
	unsigned int RegVal;
	if( ValidAddress(Address))
	{
			RegVal = *Address;
			RegVal = RegVal | Value;
			*Address = RegVal;
	}
 	return;
}


// 
//	This funciton will procees and array of Instruction Operations
//
//	Instructions - pointer to start of the array must be pInstruction_S type
//  Num - number op operations in the array
//
unsigned int ProcessInstructions(pINSTRUCTION_S pInstructions, unsigned int Num, unsigned int TimeoutFlag)
{
	int i;
	unsigned int Retval = NoError;
	unsigned int *pParam; 

	if(pInstructions == NULL)
		return NULLPointer;  
	pParam = &pInstructions->Parameters;
	for(i = 0;i < Num; i++)
	{
		switch(pInstructions->InstructionId)
		{
			case INSTR_NOP:
				break;
			case INSTR_WRITE:
				 WriteInstruction( (unsigned int *)pParam[0], pParam[1]);
				 pInstructions = (pINSTRUCTION_S)&pParam[2];
				 break;
			case INSTR_READ:
				ReadInstruction( (unsigned int *)pParam[0], pParam[1]);
				 pInstructions = (pINSTRUCTION_S)&pParam[2];
				break;
			case INSTR_DELAY:
				 Delay(pParam[0]);
				 pInstructions = (pINSTRUCTION_S)&pParam[1];
				break;
			case INSTR_WAIT_FOR_BIT_SET:
				Retval = WaitForBitSetInstruction((unsigned int *)pParam[0], pParam[1], pParam[2]); 
				 pInstructions = (pINSTRUCTION_S)&pParam[3];
				if ((Retval == InstructionTimeout) && (TimeoutFlag == 0))
					Retval = NoError; //ignore error				 
				break;
			case INSTR_WAIT_FOR_BIT_CLEAR:
				Retval = WaitForBitClearInstruction((unsigned int *)pParam[0], pParam[1], pParam[2]); 
				 pInstructions = (pINSTRUCTION_S)&pParam[3];
				if ((Retval == InstructionTimeout) && (TimeoutFlag == 0))
					Retval = NoError; //ignore error				 
				break;
			case INSTR_AND_VAL:
				AndInstruction( (unsigned int *)pParam[0], pParam[1]);
				 pInstructions = (pINSTRUCTION_S)&pParam[2];
				break;
			case INSTR_OR_VAL:
				OrInstruction( (unsigned int *)pParam[0], pParam[1]);
				 pInstructions = (pINSTRUCTION_S)&pParam[2];
				break;
			default:
				AddMessage((UINT8_T*) ("*** Unknown Instruction...\0"));
				Retval = UnknownInstruction;
		 }
		 if(Retval != NoError)
			break;
		pParam = &pInstructions->Parameters; 		
	 }
	return Retval;
}
 