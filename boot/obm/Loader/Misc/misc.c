/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into
 *  products for purposes authorized by the license agreement provided they
 *  include this notice and the associated copyright notice with any such
 *  product.
 *  The information in this file is provided "AS IS" without warranty.
 *
 *
 *  FILENAME:	Misc.c
 *
 *  PURPOSE: 	Contain helpful misc. functions
 *
******************************************************************************/

#include "misc.h"


static unsigned char* 		StrPtr8;
static unsigned int   		DataWidth;

// Very simple routine to reverse bytes on up to a 512 byte buffer.
UINT_T ReverseBytes (UINT8_T* Address, UINT_T Size)
{
	UINT8_T Buffer[512];
	UINT8_T i;

	if (Size > 512)
		return InvalidSizeError;

	// First copy to a Buffer
	for (i=0; (i < Size); i++)
		Buffer[i] = Address[i];

	// Next copy to Address in reverse order
	for (i=0; (i < Size); i++)
		Address[Size - 1 - i] = Buffer[i];

	return NoError;
}

//endian conversion function: self explanatory
unsigned int Endian_Convert (unsigned int in)
{
	unsigned int out;
	out = in << 24;
	out |= (in & 0xFF00) << 8;
	out |= (in & 0xFF0000) >> 8;
	out |= (in & 0xFF000000) >> 24;
	return out;
}

/**
 * Memory set function
 **/
void memset(void *Addr, unsigned char Val, unsigned long Size)
{

	unsigned long i;
	for(i=0; i < Size; i++ )
	{
		((unsigned char*)Addr)[i] = Val;
	}
}

void *memcpy(void *dest, const void *src, unsigned int n)
{
	const unsigned char *s = (const unsigned char*)src;
	unsigned char *d = (unsigned char *)dest;

	while(n-- > 0)
		*d++ = *s++;

	return dest;
}

int memcmp( const void *buffer1, const void *buffer2, int count)
{
    UINT8_T* buf1 = (UINT8_T*)buffer1;
    UINT8_T* buf2 = (UINT8_T*)buffer2;

  while(count)
  {
    if( *(buf1) != *(buf2) )
      return( (*buf1 < *buf2) ? -1 : 1 );

    buf1 += sizeof(UINT8_T);
    buf2 += sizeof(UINT8_T);
    count--;
  }

  return 0;
}

int ConvertStringToInteger8(const unsigned char* StringPtr, unsigned int Count)
{
	int  Output = 0;
	unsigned char c;
	while (*StringPtr)
	{
		Output *= Count;
		c = *StringPtr++;
		if (c >= '0' && c <= '9')
			Output += (c-'0');
		else
			Output += ((c|' ')-'a'+10);
	}
	return (Output);
}

int DivideTwoNumbers(int Numerator,int Denominator)
{
	int Tmp    = 0;
	int Output = 0;
	int Count;
	for (Count = 28; Count >= 0; Count -= 4)
	{
		Tmp    <<= 4;
		Output <<= 4;
		Tmp |= (Numerator >> Count)&0xf;
		while(Tmp >= 0)
		{
			Tmp -= Denominator;
			Output++;
		}
		Tmp += Denominator;
		Output--;
	}
	return(Output);
}

int ModOfTwoNumbers(int Numerator, int Denominator)
{
	//return(Numerator - (Denominator * DivideTwoNumbers(Numerator, Denominator)));
	int Tmp;
	Tmp = DivideTwoNumbers(Numerator, Denominator);
	Tmp = Tmp * Denominator;
	Tmp = Numerator - Tmp;
#if LINUX_BUILD
	// The GCC compiler assigns the value in R1(Quotient) to variable on the left side of the "="
	// in the calling code. So we need to move the Remainder (in Tmp) to R1 so we get the Remainder
	// returned and not the quotient.
	// Syntax below informs the compiler we modified R1 intentionally and don't mess with it.
	asm("mov r1, %[value]" :: [value] "r" (Tmp): "r1");   // this move the contents of temp into r1
#endif
	return Tmp;
}


void ConvertLongIntToBuf8(int DataValue, int Count)
{
	if (DataValue >= Count)
	{
		ConvertLongIntToBuf8(DivideTwoNumbers(DataValue, Count), Count);
		StrPtr8++;
		DataWidth--;
		DataValue = ModOfTwoNumbers(DataValue, Count);
	}
	if (DataWidth > 0)
		*StrPtr8 = (unsigned char)(DataValue + (DataValue < 10 ? '0' : ('a'-10)));
}


void ConvertIntToBuf8(unsigned char* StringPtr,unsigned int Value, int Width,unsigned int Count)
{
	int i;
	StrPtr8    = StringPtr;
	DataWidth  = Width;
	if (Value >= Count)
	{
		ConvertLongIntToBuf8(DivideTwoNumbers(Value,Count),Count);
		StrPtr8++;
		DataWidth--;
		Value=ModOfTwoNumbers(Value,Count);
	}
	if (DataWidth-->0)
		*StrPtr8=(unsigned char)(Value + (Value < 10 ? '0' : ('a'-10)));

	// If DataWidth=0, the following will do nothing, so just return here
	if (DataWidth == 0) return;

	// Shift the contents of the buffer to right by DataWidth to achieve right alignment
	for (i=Width; i > DataWidth; i--)
	{ 
		*(StrPtr8+DataWidth) = *(StrPtr8);
		StrPtr8--;
	}
	// Prepend leading zeros if necessary
	while (DataWidth-- > 0)
		*++StrPtr8 = '0';
}

#if 0
void __aeabi_idiv0( void ){

	//dummy function for the compiler
	// need to investigate the in32t_divide.o and x0_a000.o libraries from XDB
	return;
}

//The below functions are pulled in from a library when built by RVCT or SDT compiler (NDT flag)
//When built by the linux compiler, we need to define the calls ourselves
#if LINUX_BUILD

void __aeabi_idiv(void){
//void __divsi3(void){
	asm("b DivideTwoNumbers");
	return;
}

void __aeabi_uidiv(void){
//void __udivsi3(void){
	asm("b DivideTwoNumbers");
	return;
}

void __aeabi_uidivmod(void){
	asm("b ModOfTwoNumbers");
	return;
}

#endif
#endif

/*
   String Compare function - case sensitive
   Will compare str1 to str2, character to character, until a NULL character is hit
   or until maxlength is reached
   If no NULL character is present, there could be serious issues

      Return values:
      -1 = str1 < str2
       0 = Match
       1 = str1 > str2

 */
int strcmpl( char* str1, char* str2, int maxlength )
{
    int length = maxlength;
    while((*str1 != NULL) && (*str2 != NULL) && --length)
    {
        if (*str1 != *str2)
        {
            // check the char that was different to determine results
            if ( *str1 > *str2 )
                return 1;
            else
                return -1;
        }
        str1++; str2++;
    }

    // strings compared so far, but did we reach the end of both?
    if ( *str1 != NULL && *str2 != NULL )
        // did not find the NULLs but have reached the maxlength to compare
        return 0;

    if ( *str1 == NULL && *str2 == NULL )
        return 0;

    if ( *str1 != NULL && *str2 == NULL )
        // "abcde" > "abcd"
        return 1;
    else
        // "abcd" < "abcde"
        return -1;
}


/*
   String Compare function - case insensitive
   Will compare str1 to str2 ignoring case, character to character, until a NULL character is hit
   or until maxlength is reached
   If no NULL character is present, there could be serious issues

      Return values:
      -1 = str1 < str2
       0 = Match
       1 = str1 > str2
 */
int stricmpl( char* str1, char* str2, int maxlength)
{
    int length = maxlength;
    while((*str1 != NULL) && (*str2 != NULL) && --length)
    {
        if ( ((*str1) & 0xDF) != ((*str2) & 0xDF) )
        {
            // check the char that was different to determine results
            if ( ((*str1) & 0xDF) > ((*str2) & 0xDF) )
                return 1;
            else
                return -1;
        }
        str1++; str2++;
    }

    // strings compared so far, but did we reach the end of both?
    if ( *str1 != NULL && *str2 != NULL )
        // did not find the NULLs but have reached the maxlength to compare
        return 0;

    if ( *str1 == NULL && *str2 == NULL )
        return 0;

    if ( *str1 != NULL && *str2 == NULL )
        // "abcde" > "abcd"
        return 1;
    else
        // "abcd" < "abcde"
        return -1;
}


INT_T strlen (const char * str)
{
    int i = 0;
    while(str[i++] != '\0')
        if(i > 1000)
            break;
    return --i;
}

INT_T memcmpFF( const void *buffer, INT_T count)
{
    UINT8_T* buf = (UINT8_T*)buffer;

    while(count--)
    {
        if( *(buf) != 0xFF )
          return -1;

        buf += sizeof(UINT8_T);
    }

    return 0;
}

/*
 * Mini printf impl
*/

#define putchar(c) OutputByte(c)
extern int OutputByte(int b);

static void printchar(char **str, int c)
{
	if (str) {
		**str = c;
		++(*str);
	}
	else (void)putchar(c);
}

#define PAD_RIGHT 1
#define PAD_ZERO 2

static int prints(char **out, const char *string, int width, int pad)
{
	register int pc = 0, padchar = ' ';

	if (width > 0) {
		register int len = 0;
		register const char *ptr;
		for (ptr = string; *ptr; ++ptr) ++len;
		if (len >= width) width = 0;
		else width -= len;
		if (pad & PAD_ZERO) padchar = '0';
	}
	if (!(pad & PAD_RIGHT)) {
		for ( ; width > 0; --width) {
			printchar (out, padchar);
			++pc;
		}
	}
	for ( ; *string ; ++string) {
		printchar (out, *string);
		++pc;
	}
	for ( ; width > 0; --width) {
		printchar (out, padchar);
		++pc;
	}

	return pc;
}

/* the following should be enough for 32 bit int */
#define PRINT_BUF_LEN 12

static int printi(char **out, int i, int b, int sg, int width, int pad, int letbase)
{
	char print_buf[PRINT_BUF_LEN];
	register char *s;
	register int t, neg = 0, pc = 0;
	register unsigned int u = i;

	if (i == 0) {
		print_buf[0] = '0';
		print_buf[1] = '\0';
		return prints (out, print_buf, width, pad);
	}

	if (sg && b == 10 && i < 0) {
		neg = 1;
		u = -i;
	}

	s = print_buf + PRINT_BUF_LEN-1;
	*s = '\0';

	while (u) {
		t = u % b;
		if( t >= 10 )
			t += letbase - '0' - 10;
		*--s = t + '0';
		u /= b;
	}

	if (neg) {
		if( width && (pad & PAD_ZERO) ) {
			printchar (out, '-');
			++pc;
			--width;
		}
		else {
			*--s = '-';
		}
	}

	return pc + prints (out, s, width, pad);
}

static int print(char **out, int *varg)
{
	register int width, pad;
	register int pc = 0;
	register char *format = (char *)(*varg++);
	char scr[2];

	for (; *format != 0; ++format) {
		if (*format == '%') {
			++format;
			width = pad = 0;
			if (*format == '\0') break;
			if (*format == '%') goto out;
			if (*format == '-') {
				++format;
				pad = PAD_RIGHT;
			}
			while (*format == '0') {
				++format;
				pad |= PAD_ZERO;
			}
			for ( ; *format >= '0' && *format <= '9'; ++format) {
				width *= 10;
				width += *format - '0';
			}
			if( *format == 's' ) {
				register char *s = *((char **)varg++);
				pc += prints (out, s?s:"(null)", width, pad);
				continue;
			}
			if( *format == 'd' ) {
				pc += printi (out, *varg++, 10, 1, width, pad, 'a');
				continue;
			}
			if( *format == 'x' ) {
				pc += printi (out, *varg++, 16, 0, width, pad, 'a');
				continue;
			}
			if( *format == 'X' ) {
				pc += printi (out, *varg++, 16, 0, width, pad, 'A');
				continue;
			}
			if( *format == 'u' ) {
				pc += printi (out, *varg++, 10, 0, width, pad, 'a');
				continue;
			}
			if( *format == 'c' ) {
				/* char are converted to int then pushed on the stack */
				scr[0] = *varg++;
				scr[1] = '\0';
				pc += prints (out, scr, width, pad);
				continue;
			}
		}
		else {
		out:
			printchar (out, *format);
			++pc;
		}
	}
	if (out) **out = '\0';
	return pc;
}

/* assuming sizeof(void *) == sizeof(int) */

int printf(const char *format, ...)
{
	register int *varg = (int *)(&format);
	return print(0, varg);
}

int sprintf(char *out, const char *format, ...)
{
	register int *varg = (int *)(&format);
	return print(&out, varg);
}

