/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 *  FILENAME:       xllp_clkmgr.c
 *
 *  PURPOSE: contains clock enable/disable function.
 *           
*/

#include "xllp_clkmgr.h"

/**
This function enable/disable special peripheral clock in D0(Run) mode.

@param
	pReg  	    Virtual Base Address of Clock Module 
	Module  	Module name. It is enumeration of all Monahans��s peripheral.
	enable		- true : Enable peripheral ��module�� clock
				- false: Disable peripheral ��module�� clock
@return
	Old peripheral ��module�� clock status(disable/enable) before call this function.

*/

XLLP_BOOL_T XllpClockEnable(P_XLLP_CLKMGR_T  pReg, XLLP_CLK_MODULE module, XLLP_BOOL_T enable)
{
	
	XLLP_BOOL_T ret;
    XLLP_UINT32_T dump;

	if(module<32)
	{
		ret = (XLLP_BOOL_T)(pReg->d0cken_a & (0x1u << module));
		
		if(enable)
		{
			pReg->d0cken_a |= (0x1u << module);
			if( XLLP_CLK_AC97 == module){
				//fix me: make out clock for ac97 codec be 24.576MHZ. 
				pReg->ac97_div = 1625<<12 | 128;
			}

		}else
		{
			pReg->d0cken_a &= ~(0x1u << module);
		}
	}
	else
	{
		ret = (XLLP_BOOL_T) (pReg->d0cken_b & (0x1u << (module-32) ));

		if(enable)
		{
			pReg->d0cken_b |= (0x1u << (module-32) );
		
		}else
		{
			pReg->d0cken_b &= ~(0x1u << (module-32) );
		}
	}
	dump = pReg->d0cken_b;  //dump read to make sure write register successfully
	return ret;

}

/**
This function restore state after system wakeup

@param
    pVirtualAddress     Virtual address of clock module.
    pSaveBuff           buff of saving state. 

@return
    XLLP_STATUS_SUCCESS             -   Success
    XLLP_STATUS_WRONG_PARAMETER     -   fail, input address is zero

@remark
    This function will NOT issue frequency change. 
    Only restore d0cken_a d0cken_b ac97_div and aicsr control bit. Status bit will be keeped. 
    OS Driver must restore working frequency. 
*/               
XLLP_ERROR_STATUS_T XllpClockRestore(P_XLLP_CLKMGR_T  pVirtualAddress, P_XLLP_CLKMGR_T pSaveBuff)
{
    XLLP_UINT32_T aicsr;
    if(!pVirtualAddress)
        return XLLP_STATUS_WRONG_PARAMETER;

    if(!pSaveBuff)
        return XLLP_STATUS_WRONG_PARAMETER;

    aicsr = pSaveBuff->aicsr;
    aicsr &=~ (XLLP_AICSR_PCIS|XLLP_AICSR_TCIS|XLLP_AICSR_FCIS);

    pVirtualAddress->d0cken_a = pSaveBuff->d0cken_a;
    pVirtualAddress->d0cken_b = pSaveBuff->d0cken_b;
    pVirtualAddress->ac97_div = pSaveBuff->ac97_div;
    pVirtualAddress->aicsr    = aicsr;

    aicsr=pVirtualAddress->aicsr ;  //dump read to make sure write register successfully

    return XLLP_STATUS_SUCCESS;
}

