/******************************************************************************
**  (C) Copyright 2007 Marvell International Ltd.
**  All Rights Reserved
******************************************************************************/

/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document.
**  Except as permitted by such license, no part of this document may be
**  reproduced, stored in a retrieval system, or transmitted in any form or by
**  any means without the express written consent of Intel Corporation.
**
**  FILENAME:   keypad.c
**
**  PURPOSE:    keypad support: init, read
**
**  History:    Initial Creation 11/08
******************************************************************************/

#include "misc.h"
#include "KPC.h"


#define KPC_PCAS        (1 << 30)    // Force Automatic Scan
#define KPC_ASACT       (1 << 29)    // Automatic Scan on Activity bit
#define KPC_MKRN        (7 << 26)    // Number of Keypad Rows - 1 
#define KPC_MKCN        (7 << 23)    // Number of Keypad Columns - 1
#define KPC_MI          (1 << 22)    // Matrix interupt bit
#define KPC_IMKP        (1 << 21)    // Ignore Multiple Keypad Press bit
#define KPC_MS7         (1 << 20)    // Matrix Scan Line 7
#define KPC_MS6         (1 << 19)    // Matrix Scan Line 6
#define KPC_MS5         (1 << 18)    // Matrix Scan Line 5
#define KPC_MS4         (1 << 17)    // Matrix Scan Line 4
#define KPC_MS3         (1 << 16)    // Matrix Scan Line 3
#define KPC_MS2         (1 << 15)    // Matrix Scan Line 2
#define KPC_MS1         (1 << 14)    // Matrix Scan Line 1
#define KPC_MS0         (1 << 13)    // Matrix Scan Line 0
#define KPC_ME          (1 << 12)    // Matrix Keypad Enable bit
#define KPC_MIE         (1 << 11)    // Matrix Keypad Interrupt Enable bit
#define KPC_DKN         (2 <<  6)    // Number of direct keypad inputs -1
#define KPC_DI          (1 <<  5)    // Direct keypad interrupt bit
#define KPC_REE0        (1 <<  2)    // Rotary encoder 0 enable bit
#define KPC_DE          (1 <<  1)    // Direct Keypad Enable bit

#define	KPC_FNCLKSEL_32KHZ	(0<<4)		// functional clock select: 0=32KHz, 1=16KHz, 2=26MHz
#define	KPC_RST				(0<<2)		// reset: 0=active, 1=hold in reset
#define	KPC_FNCLK			(1<<1)		// kpc functional clock: 0=clock off, 1=clock on
#define	KPC_APBCLK			(1<<0)		// kpc apb clock: 0=clock off, 1=clock on

#define KPC_PC_ASPEN	\
(						\
	KPC_ASACT    |      \
	KPC_MKRN     |      \
	KPC_MKCN     |      \
	KPC_MS7      |      \
	KPC_MS6      |      \
	KPC_MS5      |      \
	KPC_MS4      |      \
	KPC_MS3      |      \
	KPC_MS2      |      \
	KPC_MS1      |      \
	KPC_MS0      |      \
	KPC_ME              \
)

void InitializeKeypad() 
{
	// steps
	// 1. ensure clocks are on and unit is out of reset
	// 2. set up the keypad control register
	// 3. set up the debounce interval
	// 4. set up rotary encode initial value
	*(volatile unsigned long*)APBC_KPC_CLK_RST = 
		KPC_FNCLKSEL_32KHZ |
		KPC_RST |
		KPC_FNCLK |
		KPC_APBCLK;

	Delay(20 * 63 );	// 63 uS = 1 16KHz clock. spec calls for 10 clocks between each register write. wait for 20 to be safe.

	*(volatile unsigned long*)KPC_PC = KPC_PC_ASPEN;
	
	Delay(20 * 63 );	// 63 uS = 1 16KHz clock. spec calls for 10 clocks between each register write. wait for 20 to be safe.

	*(volatile unsigned long*)KPC_KDI = 1;				// 1 millisecond debug;

}



// return 1 if a key is available.
unsigned long ReadKeypad( unsigned int *k ) 
{ 
	unsigned long	kpc_as;			// busy, multiple keys, row, col

	int				keyavailable = 0;
#if WAYLAND
	volatile unsigned long	*pgpio_lr;	// points to level reg.
	unsigned long			l;			// current levels

	pgpio_lr = (volatile unsigned long	*)(GPIO1_BASE+GPIO_PLR); // GPIO1_BASE is d4019004, GPIO_PLR is 0x00.
	l = *pgpio_lr;
	if( l & (1u<<(47%32)) )	// signal is high?
	{
		keyavailable = 0;				//		if yes, then no software upgrade request.
	}
	else
	{
		keyavailable = 1;				//		if no, signal is low. that means a software upgrade request.
	}

#else
	// steps:
	// 1. initiate a scan
	// 2. wait for the scan to complete
	// 3. examine the register for indications and details of key present

	// force a scan operation

	Delay(20 * 63 );	// 63 uS = 1 16KHz clock. spec calls for 10 clocks between each register write. wait for 20 to be safe.

	// noted that some times the kpc_pcas fails to clear.
	// need to debug. for now, since auto scan is on, just read the auto scan result register (kpc_as)
	*(volatile unsigned long*)KPC_PC = ( *(volatile unsigned long*)KPC_PC ) | (KPC_PCAS);

	Delay(20 * 63 );	// 63 uS = 1 16KHz clock. spec calls for 10 clocks between each register write. wait for 20 to be safe.

	// wait for it to complete.
	// note: sometimes bit 31 is failing to complete, indicating that an auto scan never finishes.
	//       maybe due to not waiting 16 kpc clocks after setting pcas...just added a delay (above)
	//       if the problem occurs again, will need to debug further. 
	//       a temporary workaround would be to just return the contents of kpc_as, since the row/col
	//       always seems to be valid if even if the bit 31 doesn't clear...(actually, that's not true.
	//       in the failure cases, the ldr r3,[r1], where r1=&kpc_as=0xd4012020, always store a stale
	//       value of 0x800000ff in r3 - even though an examination of that memory using xdb show mem
	//       shows that 0xd4012020 has the new row/col in it! is the read transaction to the kpc being
	//       aborted?
	//       interesting observation: writing a value to kpc_as fixes the problem. the next read to 
	//       kpc_as returns the correct data. is this a cache problem? kpc_as should not be marked as
	//       a cacheable address. mmu is not on in this test... but since restart / reset doesn't work,
	//       it's possible the mmu could still have "valid" cache lines in it...
	//       also note that only using a str from the code will work. xdb set val will not do the trick.
	//
	// two possible workarounds: don't write to kpc_pc here, or add a delay after writing kpc_pc
	while( (kpc_as = *(volatile unsigned long*)KPC_AS) & (1u<<31) );	// monitor the scan-on (SO) bit


	// kpc_as.rp & kpc_as.cp should not be 15. they should contain a valid row.
	*k = kpc_as;

	if( kpc_as != 0xff ) keyavailable = 1;
 #endif
	return keyavailable; 
}
