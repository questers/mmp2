/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document.
**  Except as permitted by such license, no part of this document may be
**  reproduced, stored in a retrieval system, or transmitted in any form or by
**  any means without the express written consent of Intel Corporation.
**
**  FILENAME:   Flash.c
**
**  PURPOSE:    Contain template OEM boot code flash operations
**
******************************************************************************/

#include "Flash.h"
#include "nand.h"
#include "general.h"
#include "xllp_dfc.h"
#include "loadoffsets.h"
#include "FM.h"
#include "tim.h"
#include "ProtocolManager.h"
#if !BOOTROM
#include "BootLoader.h"
#endif

NAND_Properties_T NAND_Prop;    // Only need one

P_NAND_Properties_T GetNANDProperties(void)
{
    return &NAND_Prop;
}

/*
 * Function initializes NAND device and fills out Flash Properties Struct
 */

UINT_T InitializeNANDDevice(UINT8_T FlashNum, FlashBootType_T FlashBootType, UINT8_T* P_DefaultPartitionNum)
{
	UINT_T Retval = NoError;
	P_NAND_Properties_T pNandP = GetNANDProperties();
	P_FlashProperties_T pFlashP = GetFlashProperties(FlashBootType);
    UINT_T RunningAsIdentifier;

	// Set Current FlashBootType so XllpDfcInit can determine why it is called.
	SetCurrentFlashBootType(FlashBootType);

	// Pick correct bus width and set the ECC type
	switch (FlashNum)
	{
		case NAND_FLASH_X16_HM_P:
			pNandP->FlashBusWidth = FlashBusWidth16;
			pNandP->ECCMode = ECC_HAMMING;
			break;
		case NAND_FLASH_X16_BCH_P:
			pNandP->FlashBusWidth = FlashBusWidth16;
			pNandP->ECCMode = ECC_BCH;
			break;
		case NAND_FLASH_X8_HM_P:
			pNandP->FlashBusWidth = FlashBusWidth8;
			pNandP->ECCMode = ECC_HAMMING;
			break;
		case NAND_FLASH_X8_BCH_P:
			pNandP->FlashBusWidth = FlashBusWidth8;
			pNandP->ECCMode = ECC_BCH;
			break;
		default:
			return DFCInitFailed;
	}

    #if !BOOTROM && !SLC_NONFI
    RunningAsIdentifier = WhoAmI();
    if(RunningAsIdentifier != DKBIDENTIFIER)
        xdfc_get_bootrom_ecc(); //get ECC settings that bootrom set.
    #endif
    
	Retval = XllpDfcInit(pNandP->FlashBusWidth, pNandP);


	if (Retval != NoError)
		//return DFCInitFailed;
		return NANDNotFound;

	
    //set ECC with the correct ECCMode
	xdfc_enable_ecc( pNandP->ECCMode );

	//define functions
	pFlashP->ReadFromFlash = &ReadNAND;
	pFlashP->WriteToFlash = &WriteNAND;
	pFlashP->EraseFlash =  &EraseNAND;
	pFlashP->ResetFlash = &ResetNAND;
	pFlashP->BlockSize = pNandP->BlockSize;
	pFlashP->PageSize = pNandP->PageSize;
	pFlashP->FlashSettings.UseBBM = 1;
	pFlashP->FlashSettings.UseSpareArea = 0;
	pFlashP->FlashSettings.SASize = 0;
	pFlashP->FlashSettings.UseHwEcc = 1;  // default is HW ECC ON
	pFlashP->FlashType = NAND_FLASH;
    pFlashP->NumBlocks = pNandP->NumOfBlocks;
	// init the TIM load address
	//---------------------------------------
	pFlashP->TimFlashAddress = TIMOffset_NAND;
	pFlashP->StreamingFlash = FALSE;
	pFlashP->StagedInitRequired = TRUE;			 // TBD - Look for this flag in TIMLoad and call back again if set.
	*P_DefaultPartitionNum = NAND_DEFAULT_PART;
	pFlashP->FinalizeFlash = NULL;
	return Retval;
}

/*
 This routine will read in data from the DFC.
*/
UINT_T ReadNAND (UINT_T Address, UINT_T Destination, UINT_T ReadAmount, FlashBootType_T FlashBootType)
{
    P_NAND_Properties_T pNAND_Prop;
    pNAND_Prop = GetNANDProperties();

    // For monolithic operation call legacy read routine
    if (pNAND_Prop->PageSize <= TWO_KB)
        return xdfc_read((UINT_T *)Destination, Address, ReadAmount, NULL, pNAND_Prop);
    else    // Call the new routine that reads page at a time.
        return xdfc_read_LP((UINT_T *)Destination, Address, ReadAmount, NULL, pNAND_Prop);
}

/*
    WriteNAND - This function is a wrapper to work with DFC
 */

UINT_T WriteNAND (UINT_T flash_addr, UINT_T source, UINT_T WriteAmount, FlashBootType_T FlashBootType)
{
    UINT_T Retval = NoError;
    P_NAND_Properties_T pNandP = GetNANDProperties();
    P_FlashProperties_T pFlashP = GetFlashProperties(FlashBootType);
    if (pNandP->PageSize <= TWO_KB)
    {
        Retval = xdfc_write((UINT_T *) source,
                        flash_addr,
                        WriteAmount,
                        pFlashP->FlashSettings.UseSpareArea,
                        pFlashP->FlashSettings.UseHwEcc,
                        pNandP);
    }
    else
    {
        Retval = xdfc_write_LP((UINT_T *) source,
                            flash_addr,
                            WriteAmount,
                            pNandP);
    }
    return Retval;  // Return value
}

/*
 *
 *  Erases one block at a time
 *      note:  if user inputs determine that a partial block should be erased,
 *              this function will erase that WHOLE block: no partial blocks allowed
 *
 */
UINT_T EraseNAND (UINT_T flashoffset, UINT_T size, FlashBootType_T fbt)
{
    return xdfc_erase(flashoffset, GetNANDProperties());
}

//Wrapper to link the dfc reset routine to the flash API
UINT_T ResetNAND(FlashBootType_T fbt)
{
    return xdfc_reset(GetNANDProperties());
}

#if !BOOTROM
UINT_T ScanNANDForBadBlocks(FlashBootType_T fbt)
{
    UINT_T Retval = NoError;
    UINT_T SA;
    UINT_T i, j, temp, count;
    UINT_T ReadBuffer;
    UINT_T ReadBuffer2;
    UINT_T *data;
    USHORT *pReloEntry;
    P_NAND_Properties_T pNandP = GetNANDProperties();
    P_FlashProperties_T pFlashProp = GetFlashProperties(fbt);
    UINT_T PageSizeWithSpare = pNandP->PageSize + pNandP->SpareAreaSize;
    UINT_T SpareAreaOffset = pNandP->PageSize;
    UINT_T BlockNum;
    UINT_T bUseHwEcc = FALSE;

    ReadBuffer = TEMP_BUFFER_AREA;
    ReadBuffer2 = TEMP_BUFFER_AREA + PageSizeWithSpare;

    AddMessageError(PlatformBusy);
    bUseHwEcc = GetUseHwEcc( BOOT_FLASH );
    SetUseSpareArea( 1, BOOT_FLASH );
    SetUseHwEcc( FALSE, BOOT_FLASH ); //Turn Off ECC
    for (i=0; i < pFlashProp->NumBlocks; i++)
    {
        // address of 1st page in each block
        SA = i * pFlashProp->BlockSize;
        // Read first page(including spare area) and second page of the block.
        Retval = ReadNAND (SA, ReadBuffer, PageSizeWithSpare, fbt);
        Retval |= ReadNAND (SA + pNandP->PageSize, ReadBuffer2, PageSizeWithSpare, fbt);
        if(Retval != NoError) break;
        count = 0;
        data = (UINT_T *)TEMP_BUFFER_AREA;
        do{
             temp = *data & 0xFFFFFFFF;
             data++;
             count += 4;
             if (count >  (PageSizeWithSpare * 2))
                break;
         }while(temp == 0xFFFFFFFF);
         if ( count <= (PageSizeWithSpare * 2)){
             //check first page spare area
             if(count >=  SpareAreaOffset && count < PageSizeWithSpare){
                // Check byte 0 and byte 6 of spare area for marked block
                    if ( (((unsigned short *)ReadBuffer)[SpareAreaOffset + 5] != 0xFF)  ||
                          (((unsigned short *)ReadBuffer)[SpareAreaOffset] != 0xFF))
                    {
                        BlockNum = i;
                        RelocateBlock( BlockNum, &BlockNum, fbt );
                    }
              //check second page spare area
              }else if((count >=  (PageSizeWithSpare + SpareAreaOffset)) && (count < (PageSizeWithSpare * 2))){
                    if ((((unsigned short *)ReadBuffer2)[SpareAreaOffset] != 0xFF) ||
                        (((unsigned short *)ReadBuffer2)[SpareAreaOffset + 5] != 0xFF))
                    {
                        BlockNum = i;
                        RelocateBlock( BlockNum, &BlockNum, fbt );
                    }
              }else{
                  // Low level format - erase and program first page
                    Retval = EraseNAND (SA, pNandP->PageSize, fbt);
                    if(Retval == NoError){
                        for( j=0; j < pNandP->PageSize ; j++ )
                            ((unsigned short *)ReadBuffer)[j] = 0x0;
                        Retval = WriteNAND (SA, ReadBuffer, pNandP->PageSize , fbt);
                        Retval = EraseNAND (SA, pNandP->PageSize, fbt);
                    }
                    if (Retval != NoError)
                    {
                        BlockNum = i;
                        RelocateBlock( BlockNum, &BlockNum, fbt );
                    }
              }
          }

    }
    SetUseHwEcc( bUseHwEcc, BOOT_FLASH ); //Turn On ECC
    AddMessageError(PlatformReady);
    SetUseSpareArea( 0, BOOT_FLASH );
    return Retval;
}
#endif

