/******************************************************************************
**  COPYRIGHT  2007 Marvell Inernational Ltd.
**  All Rights Reserved
******************************************************************************/

#include "OneNAND_Driver.h"
#include "FlexOneNAND_Cmd.h"
#include "OneNAND_Cmd.h"
#include "Errors.h"
#include "loadoffsets.h"

P_FLEX_REGISTERS_T pFlexRegs = (P_FLEX_REGISTERS_T)FLEX_ONENAND_REG_BASE;
P_ONE_NAND_REGISTERS_T pOneNANDRegs = (P_ONE_NAND_REGISTERS_T)FLEX_ONENAND_REG_BASE;

extern UINT_T ResetOneNAND();
extern UINT_T ResetFlexOneNAND();
extern UINT_T LoadSector(UINT_T FlashOffset, UINT_T buffer, UINT_T size, FlashBootType_T fbt);
extern UINT_T LoadFlexPage(UINT_T FlashOffset, UINT_T buffer, UINT_T size, FlashBootType_T fbt);
extern UINT32 ComputeDivisorShiftFromPowerOf2( UINT32 power );

UINT noOfBlocks;
FlexOneNAND_Properties_T FlexOneNAND_Prop;  // Only need one

#define LOAD_ADDRESS 0x0
#define BLOCK_SIZE 0x20000
#define DATA_BUFFER_SIZE 250

//extern UINT32 RelocateBlock( UINT32 *badBlock, P_RelocTable_T pBBT );

P_FlexOneNAND_Properties_T GetFlexOneNANDProperties(void)
{
    return &FlexOneNAND_Prop;
}

 //-----------------------------------------------------------------------
// ONENAND_Init()
//
//     Bring-up the OneNAND infrastructure
//
//     RETURNS 0 if all went well
//-----------------------------------------------------------------------
UINT_T InitializeFlexOneNANDDevice(UINT8_T FlashNum, FlashBootType_T FlashBootType, UINT8_T* P_DefaultPartitionNum)
{

	UINT16 FlexManID=0, OneNANDManID=0, DevID = 0, DevType, DevDensity, status;
	UINT_T total_blks;
	UINT_T deviceFound = FALSE;
	UINT_T Retval = NoError;
	int i;

	//Initialize Flash properties
	P_FlashProperties_T pFlashProp = GetFlashProperties(FlashBootType);
	P_FlexOneNAND_Properties_T pFlexProp = GetFlexOneNANDProperties();

	//Setup flash interface data & function pointers
	//No Erase or Finalize functions for Bootrom
	//----------------------------------------------
	pFlashProp->ReadFromFlash			= &Read_Flash;
	pFlashProp->WriteToFlash			= &Write_Flash;
	pFlashProp->EraseFlash				= &Erase_Flash;
	pFlashProp->ResetFlash				= &ResetONENAND;
	pFlashProp->FlashSettings.UseBBM	= 1;
	//ECC is defaulted to 'ON' for OneNAND
	pFlashProp->FlashSettings.UseHwEcc	= 1;
	pFlashProp->TimFlashAddress			= TIMOffset_ONENAND;
	pFlashProp->FlashType				= ONENAND_FLASH;
	pFlashProp->StreamingFlash = FALSE;
	pFlashProp->StagedInitRequired = FALSE;
	pFlashProp->FinalizeFlash = NULL;

	//Setup the OneNAND chip select
	ChipSelectOneNAND();

	// Read manufacturer ID for Flex and OneNAND devices.
	FlexManID = pFlexRegs->MANFID;
	OneNANDManID = pOneNANDRegs->MANFID;

	//if we didn't read a correct ID, try issuing a reset
	if(FlexManID != 0x00EC)
	{	//try Flex Reset first, then a Mux Reset if the previous failed
		Retval = ResetFlexOneNAND();
		if(Retval != NoError)
			Retval = ResetOneNAND();

		//if both resets failed, the driver won't come up.  return with error
		if(Retval != NoError)
			return FlexOneNANDNotFound;
	}

	//now that we know the driver is accessable, initialize our variables
	DevID = pFlexRegs->DEVID;
	//setup mux type
	if (DevID & 0x8)
		pFlexProp->MuxType = DUAL_CHIP;
	else
		pFlexProp->MuxType = SINGLE_CHIP;

	//figure out device type, and then setup block and device sizes
	DevType = (DevID >> 8) & 0x3; //only capture 2 bits
   	DevDensity = (DevID >> 4) & 0xF;

	if(DevType == 0x2) //flex part
	{
		pFlexProp->DeviceType = FLEX_ONE_NAND;
		//need to read PI info and save off the SLC/MLC boundary
		Retval = PIRead(&status);
		//if the call fails, can we still boot?
		if(Retval != NoError)
			status = 0;
		//store the boundary
		pFlexProp->BlkBoundary = status & 0x3FF;
		//LINK in the Change Partition function FOR FLEX ONLY!!
		pFlashProp->ChangePartition = &ChangeFlexPartition;

		//if all non-PI blocks are MLC, set block size to 256K
		if(pFlexProp->BlkBoundary == 0)
			pFlashProp->BlockSize = FLEX_MLC_BLOCK_SIZE;
		else  //intialize the device for the SLC section
			pFlashProp->BlockSize = FLEX_SLC_BLOCK_SIZE;

		pFlashProp->PageSize = 4*KBYTES;
		//DevDensity assumes all blocks are MLC
		//long version: 16*MBYTES * (DevDensity + 1) / 512*KBYTES;
		pFlashProp->NumBlocks = 32 << DevDensity;
	}
	else //mux part
	{
		pFlexProp->DeviceType = ONE_NAND;
		//check to see how many data buffers per 4KB
		//This will tell us how big the page size is
		if((pOneNANDRegs->BUFAMT & ONENAND_NUM_DATA_BUF) == 0x100)
		{	//4K page size
			pFlashProp->BlockSize = FLEX_SLC_BLOCK_SIZE;
			pFlashProp->PageSize = 4*KBYTES;
			pFlashProp->NumBlocks = 64 << DevDensity;
		}
		else
		{	//2K page size
			pFlashProp->BlockSize = MUX_SLC_BLOCK_SIZE;
			pFlashProp->PageSize = 2*KBYTES;
			pFlashProp->NumBlocks = 128 << DevDensity;
		}
		//set such that it will never be reached
		pFlexProp->BlkBoundary = pFlashProp->NumBlocks;

	}

	*P_DefaultPartitionNum = ONENAND_DEFAULT_PART;
	return NoError;
}

UINT_T ResetONENAND(FlashBootType_T fbt)
{
    P_FlexOneNAND_Properties_T pFlexProp = GetFlexOneNANDProperties();
    if(pFlexProp->DeviceType == ONE_NAND)
        ResetOneNAND();
    else
        ResetFlexOneNAND();
    return NoError;
}

UINT_T WaitForCommandCompletion( UINT16 eventGroup, UINT32 TimeOutMillisec )
{
  UINT32 startTime;
  UINT_T Retval = NoError;
  UINT16 IntStatReg;

  startTime = GetOSCR0();  //Dummy read to flush potentially bad data
  startTime = GetOSCR0();

  do
  {
    if (pFlexRegs->INTRPT == eventGroup )
     return Retval;

  }while( OSCR0IntervalInMilli(startTime, GetOSCR0()) < TimeOutMillisec );

  return TimeOutError;
}

/* Read_Flash
 *  This function calls the relative Flash device read command to read data.
 *  This function will call the lower level driver to read on a block basis, in
 *  which to use the FlashManagement BBT
 *
 *  Inputs:
 *      flash_addr - absolute address of where to begin read in flash device
 *      buffer - pointer to location of where to load the data to
 *      total_size - amount of data, in bytes, to read
 *      fbt - flash boot type:  BOOT_FLASH or SAVE_STATE_FLASH
 *  Outputs:
 *      None
 *  Retval value:
 *      NoError - on successful completion
 *      <Other> - Flash device specific read error
 */
UINT_T Read_Flash(UINT_T FlashOffset, UINT_T buffer, UINT_T size, FlashBootType_T fbt)
{
    UINT_T Retval = NoError;
	UINT_T Status = NoError;
    P_FlashProperties_T pFlashProp = GetFlashProperties(fbt);
    P_FlexOneNAND_Properties_T pFlexProp = GetFlexOneNANDProperties();
    UINT_T PageMask, ByteMask, read_size;

    PageMask = pFlashProp->BlockSize - pFlashProp->PageSize;
    ByteMask = pFlashProp->PageSize - 1;
    do
    {
        //calculate how much to read from buffer
        //  note: this normally should be a page size,
        //      Exceptions:
        //          - Start reading in the middle of a page
        //          - End reading in the middle of a page
        read_size = pFlashProp->PageSize - (FlashOffset & ByteMask);
        read_size = read_size > size ? size : read_size;

        //Write the page

        if(pFlexProp->DeviceType == ONE_NAND)
            //Retval = ProgramSector(address, buffer, write_size, fbt);
            Retval = LoadSector(FlashOffset, buffer, read_size, fbt);
        else
            //Retval = ProgramFlexPage(address, buffer, write_size, fbt);
            Retval = LoadFlexPage(FlashOffset, buffer, read_size, fbt);

        //Check and handle errors
        switch(Retval)
        {
		//if we have a read disturb, save the error, but continue as normal
        case ReadDisturbError:
			Status = ReadDisturbError;
			Retval = NoError;
        case NoError:
            FlashOffset += read_size;
            size -= read_size;
            buffer += read_size;
            break;
        case TimeOutError:
        default:
            //Reset device
            if(pFlexProp->DeviceType == FLEX_ONE_NAND)
                ResetFlexOneNAND();
            else
                ResetOneNAND();
            // No need to loop around again. Return after the reset.
            return Retval;
        }
    }
    while ((size > 0) && (Retval == NoError));

	Retval = Status;
    return Retval;
}

UINT_T Write_Flash(UINT_T FlashOffset, UINT_T buffer, UINT_T size, FlashBootType_T fbt)
{
    UINT_T Retval = NoError;
    UINT_T PageMask, ByteMask, write_size;
    P_FlashProperties_T pFlashProp = GetFlashProperties(fbt);
    P_FlexOneNAND_Properties_T pFlexProp = GetFlexOneNANDProperties();

    PageMask = pFlashProp->BlockSize - pFlashProp->PageSize;
    ByteMask = pFlashProp->PageSize - 1;
    do{
        //calculate how much to write from buffer
        //  note: this normally should be a page size,
        //      Exceptions:
        //          - Start writing in the middle of a page
        //          - End writing in the middle of a page
        write_size = pFlashProp->PageSize - (FlashOffset & ByteMask);
        write_size = write_size > size ? size : write_size;

        //Write the page

        if(pFlexProp->DeviceType == ONE_NAND)
            Retval = ProgramSector(FlashOffset, buffer, write_size, fbt);
        else
            Retval = ProgramFlexPage(FlashOffset, buffer, write_size, fbt);

        //Check and handle errors
        switch(Retval)
        {
        case NoError:
            FlashOffset += write_size;
            size -= write_size;
            buffer += write_size;
            break;
        case ProgramError:
            //Relocate Block?
            break;
        case FlashLockError:
            //Unlock Device
            Retval =  UnlockNANDArrays();
            break;
        case TimeOutError:
        default:
            //Reset device
            if(pFlexProp->DeviceType == FLEX_ONE_NAND)
                ResetFlexOneNAND();
            else
                ResetOneNAND();
            // The device has been reset. No need to loop around again.
            return Retval;
        }
    }while ((size > 0) && (Retval == NoError));

    return Retval;
}

UINT_T Erase_Flash(UINT_T FlashOffset, UINT_T size, FlashBootType_T fbt)
{
    UINT_T Retval = NoError;
    UINT_T BlkNum, dummy;
    P_FlexOneNAND_Properties_T pDeviceProp;

    //Get Device properties pointer
    //-----------------------------
    pDeviceProp = GetFlexOneNANDProperties();

    //Calc block num
    //------------------------------------
    if(pDeviceProp->DeviceType == ONE_NAND)
    	BlkNum = FlashOffset / GetBlockSize(fbt);
	else
		GetFlexBlockInfo(FlashOffset, &BlkNum, &dummy);

    do{
        //Erase block
        if(pDeviceProp->DeviceType == ONE_NAND)
            Retval = BlockErase(BlkNum);
        else
            Retval = FlexBlockErase(BlkNum);

        //If there is a Flash Lock error,
        //we need to Unlock device and try erase again
        if(Retval == FlashLockError)
        {
            //Unlock Device
            Retval = UnlockNANDArrays();
            if(Retval == NoError)
                continue;
        }
        break;
    } while (TRUE);

    return Retval;
}

/* SetECCUsage turns the ECC correction on or off, and returns that state
 *OnOff:	1 = turn ECC On
 *			0 = turn ECC Off
 */
UINT_T SetECCUsage(UINT_T OnOff, VUINT16_T* SysCfgReg)
{
	if(OnOff == 1)
		*SysCfgReg &= ~CONF1_ECC_BIT;	//clear the bit to turn it on
	else
		*SysCfgReg |= CONF1_ECC_BIT;	//set the bit to turn it off

	return OnOff;
}

//This function changes the block size when changing between SLC and MLC part
//This function makes sure this is a Flex part before changing anything
void ChangeFlexPartition(UINT_T PartNum, FlashBootType_T fbt)
{
	P_FlashProperties_T pFlashProp = GetFlashProperties(fbt);

	if(GetFlexOneNANDProperties()->DeviceType == FLEX_ONE_NAND)
		if(PartNum == 0) //SLC section
			pFlashProp->BlockSize = FLEX_SLC_BLOCK_SIZE;
		else			 //MLC section
			pFlashProp->BlockSize = FLEX_MLC_BLOCK_SIZE;

	return;
}


void GetFlexBlockInfo(UINT_T Offset, UINT_T* BlockNum, UINT_T* BlockSize)
{
	UINT_T blknum, blksize;

    P_FlexOneNAND_Properties_T pFlexP = GetFlexOneNANDProperties();

	//first check if we're inside SLC bounds
	blksize = FLEX_SLC_BLOCK_SIZE;
	//if so, just return with the correct SLC block
	if(Offset < (pFlexP->BlkBoundary * blksize))
	{
		*BlockNum = Offset / blksize;
		*BlockSize = blksize;
		return;
	}

	//if not, 'chop off' SLC portion, and redo calculations for MLC section
	blknum = pFlexP->BlkBoundary;
	Offset -= blknum * blksize;
	blksize = FLEX_MLC_BLOCK_SIZE;

	//SLC blocks + MLC blocks
	*BlockNum = blknum + Offset / blksize;
	*BlockSize = blksize;

    return;
}

UINT_T UnlockNANDArrays()
{
	UINT_T Retval = NoError;
	P_FlexOneNAND_Properties_T pFlexProp = GetFlexOneNANDProperties();

	Retval = UnlockNANDArray(0);
	if ((pFlexProp->MuxType == DUAL_CHIP) && (Retval == NoError))
		Retval = UnlockNANDArray(1);

	return Retval;
}


UINT_T UnlockNANDArray(UINT8_T Core)
{
	UINT_T Retval = NoError;

	if (Core)
	{
		//Step 1: Write DFS to Start Address 1 Register
		pOneNANDRegs->SA1 = FLEX_SA1R_DFS_FLAG;

		//Step 2: Set DBS for Start Address 2 Register
		pOneNANDRegs->SA2 = FLEX_SA1R_DFS_FLAG;
	}
	else
	{
		//Step 1: Write DFS to Start Address 1 Register
		pOneNANDRegs->SA1 = 0;

		//Step 2: Set DBS for Start Address 2 Register
		pOneNANDRegs->SA2 = 0;
	}

	//Step 3: Write SBA to SBA Register
	pOneNANDRegs->SBA = 0;

	//Step 4: Clear Interrupt Register
	pOneNANDRegs->INTRPT = INT_STAT_REG_CLEAR;

	//Step 5: Write all block unlock command
	pOneNANDRegs->COMMAND = FLEX_ALL_BLOCK_UNLOCK;

	//Step 6: Wait for Interrupt
	Retval = WaitForCommandCompletion(INT_STAT_REG, (UINT32)ONENAND_TIMEOUT_MS*3 );
    if (Retval != NoError)
		return Retval;

	//Step 7: Read Controller Status Register
	if ((pOneNANDRegs->CNTSTAT & FLEX_CSR_ERROR) == FLEX_CSR_ERROR)
		return GeneralError;

	return Retval;
}














