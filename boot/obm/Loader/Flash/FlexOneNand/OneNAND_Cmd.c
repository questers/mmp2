/******************************************************************************
**	(C)Copyright Marvell. All Rights Reserved. 2007  
**
**  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
**	The copyright notice above does not evidence any actual or intended  
**  publication of such source code.	
**
**  This Module contains Proprietary Information of Marvell and should be treated 
**  as Confidential.
**	  
**  The information in this file is provided for the exclusive use of the licensees 
**  of Marvell.	
**	Such users have the right to use, modify, and incorporate this code into products 
**  for purposes authorized by the license agreement provided they include this notice 
**  and the associated copyright notice with any such product. 
**  The information in this file is provided "AS IS" without warranty.
**
**  FILENAME: OneNAND_Cmd.c
**
**  PURPOSE:  The driver for the One NAND device.
**
******************************************************************************/
#include "OneNAND_Cmd.h"
#include "OneNAND_Driver.h"
#include "Errors.h"
#include "loadoffsets.h"

extern P_FlexOneNAND_Properties_T GetFlexOneNANDProperties(void);
UINT_T CheckReadDisturb(UINT8 *mainBuffer, UINT_T noSectors, UINT16* ECCResultsMain);
extern void memset(void *Addr, unsigned char Val, unsigned long Size) ;
extern P_ONE_NAND_REGISTERS_T pOneNANDRegs;

/*
 * LoadSector:  
 *	This function uses the Load command to retrive
 *	up to one full page of data into the OneNAND part
 *
 * Inputs:
 *	address - location in flash to burn data
 *	buffer - pointer to location of data to be written
 *	size - number of bytes to be written from buffer
 *			
 *		Note: address + size CANNOT span multiple pages
 *			This MUST be handled in upper level driver
 *
 * Outputs:
 *	Return value - returns NoError on success
 *		Failures can be:
 *			Timeout Error
 *			Read Error
 */
UINT_T LoadSector(UINT_T FlashOffset, UINT_T buffer, UINT_T size, FlashBootType_T fbt)
{
	UINT_T Retval = NoError;
	UINT_T BlockNum, BlockOffsetMask, BlockByteOffset, SectorAddr, PageAddr, SectorCount;
	UINT_T BlkSize, i, EccFlag;
	VUINT16_T *pDataRAM, *pBuff, EccResults;
	UINT16_T device_select = 0;
	P_FlexOneNAND_Properties_T pFlexProp = GetFlexOneNANDProperties();

	//make sure ECC is turned on/off
	EccFlag = SetECCUsage(GetFlashProperties(fbt)->FlashSettings.UseHwEcc, &pOneNANDRegs->SYSCONF1);

	//Setup Pointers	
	pDataRAM = (volatile unsigned short *) FLEX_DATARAM_MAIN_BASE;
	pBuff = (volatile unsigned short *) buffer;
	
	// Block size of 4096 = 2^12
	BlkSize = GetBlockSize(fbt);
	BlockNum = FlashOffset / BlkSize;
	BlockOffsetMask  = BlkSize - 1;
	BlockByteOffset = FlashOffset & BlockOffsetMask;

	//get the sector address WITHIN a PAGE (that is what the '& 0x3' does)
	SectorAddr = (BlockByteOffset/SECTOR_SIZE_IN_BYTES) & 0x3;
	PageAddr = (BlockByteOffset/(GetPageSize(fbt))); 
	
	//figure out how many sectors to read
	//NOTE: this routine can only read up to 4 sectors at a time
	SectorCount = size / SECTOR_SIZE_IN_BYTES;
	//if we are not on a sector boundary, increment count for partitial sector
	if(SectorCount * SECTOR_SIZE_IN_BYTES != size)
		SectorCount++;

	//only set the DBS and DFS if using a DUAL CHIP package
	// AND the block num is greater than 2048 blocks (meaning its in the second Chip)
	if( (pFlexProp->MuxType == DUAL_CHIP) && ( BlockNum > MUX_SA1R_FBA_MASK) )
		device_select = FLEX_SA2R_DBS_FLAG;

    pOneNANDRegs->SA1 = (device_select) | (BlockNum & MUX_SA1R_FBA_MASK);

	pOneNANDRegs->SA2 = device_select;


	pOneNANDRegs->SA8 = (PageAddr << 2) | SectorAddr;
	
	//ignore FSA (sector count):  Always load a full page into DataBuff
	pOneNANDRegs->SBR = (UINT16_T) (SBR_WRITE_DEFAULT);	

			
	pOneNANDRegs->INTRPT = INT_STAT_REG_CLEAR;
	pOneNANDRegs->COMMAND =ONE_NAND_LOAD_SECTOR_INTO_BUFFER;

    if( WaitForCommandCompletion(PEND_READ, (UINT32)ONENAND_TIMEOUT_MS )!= NoError)
    {
       return ReadError;
    }

	for (i=0;i<size;i+=2)
	{
		*pBuff++ = *pDataRAM++; 
	}
	//don't worry about reading any leftover bytes out of DataRam.  OneNAND driver 
	//does not care, as it is not a fifo

	//if ECC is turned on, check the read disturb levels
	if ( EccFlag )
	{
		EccResults = pOneNANDRegs->ECCSR;
		for(i = 0; i < SectorCount; i++)
		{
			//chop off spare area ECC results
			EccResults >>= 2;
			//1bit correctable error
			if(EccResults & 0x1)
				Retval = ReadDisturbError;
			//2bit uncorrectable error
			if(EccResults & 0x2)
				return ReadError;

			//shift over for next sector status
			EccResults >>= 2;
		}
	}
	return Retval;

}

UINT_T LoadSpareSector()
{
	UINT_T Retval = NoError;
	return Retval;
}

/*
 * ProgramSector:  
 *	This function uses the Program command to program
 *	up to one full page of data into the OneNAND part
 *
 * Inputs:
 *	address - location in flash to burn data
 *	buffer - pointer to location of data to be written
 *	size - number of bytes to be written from buffer
 *			
 *		Note: address + size CANNOT span multiple pages
 *			This MUST be handled in upper level driver
 *
 * Outputs:
 *	Return value - returns NoError on success
 *		Failures can be:
 *			Flash is Locked
 *			Timeout Error
 *			Program Error
 */
UINT_T ProgramSector(UINT_T address, UINT_T buffer, UINT_T size, FlashBootType_T fbt)
{
	UINT_T Retval = NoError;
	UINT_T BlkSize, PageSize, BlockNum, PageNum, PageMask, ByteMask, SectorMask;
	UINT_T pre_bytes, post_bytes, BufferRAMSectorAddr, SectorAddr;
	VUINT16_T *pDataRAM, *pBuff;
	UINT16_T device_select = 0;
	P_FlashProperties_T pFlashProp = GetFlashProperties(fbt);
	P_FlexOneNAND_Properties_T pFlexProp = GetFlexOneNANDProperties();

	//make sure ECC is turned on/off
	SetECCUsage(pFlashProp->FlashSettings.UseHwEcc, &pOneNANDRegs->SYSCONF1);
	
	//Initial Calculations:
	BlkSize = pFlashProp->BlockSize;
	PageSize = pFlashProp->PageSize;
	PageMask = BlkSize - PageSize;
	SectorMask = PageSize - SECTOR_SIZE_IN_BYTES;
	ByteMask = SECTOR_SIZE_IN_BYTES - 1;
	
	//Determine the block we are on
	BlockNum = address / BlkSize;
	//Determine the page we are on
	PageNum	= (address & PageMask) / PageSize;
	//Determine pre-bytes for this sector
	pre_bytes = address & ByteMask;
	//Determine left over bytes (fill out to end of last sector)
	post_bytes = (PageSize - size - pre_bytes) & ByteMask;

	//Setup Pointers	
	pDataRAM = (volatile unsigned short *) FLEX_DATARAM_MAIN_BASE;
	pBuff = (volatile unsigned short *) buffer;

	//Step 1:
	//Set DBS bit in Start Address 2 register
	if((pFlexProp->MuxType == DUAL_CHIP) && (BlockNum > MUX_SA1R_FBA_MASK))
		device_select = FLEX_SA2R_DBS_FLAG;

	pOneNANDRegs->SA2 = device_select;
	//pOneNANDRegs->SA2 = 0; //make sure DBS bit is clear

	//Step 2: Load data into BufferRAM
	//Fill any initial values with 1's
	for( ; pre_bytes > 0 ; pre_bytes -= 2 )
		*pDataRAM++ = 0xFFFF;
	
	//Write the data 16 bits at a time
	for( ; size > 0 ; size -= 2 )
		*pDataRAM++ = *pBuff++;

	//If there is any leftover space in the page, fill it with 1's
	for( ; post_bytes > 0 ; post_bytes -= 2 )
		*pDataRAM++ = 0xFFFF;

	//Step 3:
	//Fill out Start Address 1 Register 
	pOneNANDRegs->SA1 = (device_select) | (BlockNum & MUX_SA1R_FBA_MASK);
	//pOneNANDRegs->SA1 = BlockNum & MUX_SA1R_FBA_MASK;

	//Step 4:
	//Read Write Protection Status
	//if (pFlexRegs->WPS & FLEX_WPSR_UNLOCKED != FLEX_WPSR_UNLOCKED)
	//	return FlashLockError;

	//Step 5:
	//Write Page and Sector Addres (must be 0) to Start Address 8
	pOneNANDRegs->SA8 = (PageNum << 2) | 0x0;

	//Step 6:
	//Fill out Start Buffer Register info
	pOneNANDRegs->SBR = (UINT16_T) (SBR_WRITE_DEFAULT);

	//Step 7:
	//Set System Config Register- leave at default 
	//uncomment next line to turn of ECC		
	//pFlexRegs->SYSCONF1 = CONF1_WRITE_DEFAULT | CONF1_ECC_OFF; 

	//Step 8:
	//Clear Interrupt Register
	pOneNANDRegs->INTRPT = INT_STAT_REG_CLEAR;
	
	//Step 9:
	//Write PROGRAM Cmd to command register
	pOneNANDRegs->COMMAND = ONE_NAND_PROGRAM_SECTOR_FROM_BUFFER;

	//Step 10:
	//Wait for WRITE interrupt
	Retval = WaitForCommandCompletion(PEND_WRITE, (UINT32)ONENAND_TIMEOUT_MS );
    if (Retval != NoError)
		return Retval;

	//Step 11:
	//Read Controller status register
	if ((pOneNANDRegs->CNTSTAT & FLEX_CSR_ERROR) == FLEX_CSR_ERROR)
		Retval = ProgramError;

	return Retval;

}

UINT_T ProgramSpareSector()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT_T CopyBackProgram()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT_T LockNANDArray()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT_T LockTightNANDArray()
{
	UINT_T Retval = NoError;
	return Retval;
}
UINT_T EraseVerifyRead()
{
	UINT_T Retval = NoError;
	return Retval;
}
UINT_T BlockErase( UINT32 blockNumber )
{
	UINT_T Retval;
    ONE_NAND_SA1_REG sa1reg;
    
    //First unlock Blocks
    //-------------------
    Retval = UnlockNANDArrays();
	if (Retval != NoError)
		return Retval;
    
    // setup the FBA in SA1
    // bits0-10 = blockNumber
    // bit 15 = blocks #'s above 2047
    //-------------------------------
    if( blockNumber > 2047 )
    {
      sa1reg.bits.ONE_NAND_DFS = 1;
      sa1reg.bits.ONE_NAND_RESERVED = 0;
      sa1reg.bits.ONE_NAND_FBA = blockNumber-2048;
    }
    else
    {
      sa1reg.bits.ONE_NAND_DFS = 0;
      sa1reg.bits.ONE_NAND_RESERVED = 0;
      sa1reg.bits.ONE_NAND_FBA = blockNumber;   
    }
      
    //Write FBA & DFS
    //-------------------------------
    pOneNANDRegs->SA1 = sa1reg.value;
    
    //set DBS in SA2
    //----------------
    if(blockNumber > 2047)
      pOneNANDRegs->SA2 = ( 1 << 15);
    else
      pOneNANDRegs->SA2 = 0;
    
	//Clear Interrupt Register
    //ensure write complete
    //------------------------
	pOneNANDRegs->INTRPT = INT_STAT_REG_CLEAR;
    sa1reg.value = pOneNANDRegs->INTRPT;
    
    //Write the command
    pOneNANDRegs->COMMAND = ONE_NAND_BLOCK_ERASE;
    
    //Wait for operation complete
    //---------------------------
	Retval = WaitForCommandCompletion(PEND_ERASE, (UINT32)ONENAND_TIMEOUT_MS*3 );
    if (Retval != NoError)
		return EraseError;

	//Read Controller Status Register
	if ((pOneNANDRegs->CNTSTAT & ONENAND_CSR_ERROR) == ONENAND_CSR_ERROR)
		Retval = EraseError;
        
	return Retval;
}

UINT_T MultiBlockErase()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT_T EraseSuspend()
{
	UINT_T Retval = NoError;
	return Retval;
}
UINT_T EraseResume()
{
	UINT_T Retval = NoError;
	return Retval;
}
UINT_T ResetNANDCore()
{
	UINT_T Retval = NoError;
	UINT16 IntStatReg=0;
	int count=0;

	do{
		 // This will return if there's no FlexOneNAND device connected.
		 if(count > INT_CLEAR_RETRY_CNT)
		 {
		   return GeneralError;
		 }
		// clear interrupt register
		pOneNANDRegs->INTRPT = INT_STAT_REG_CLEAR;
		IntStatReg = pOneNANDRegs->INTRPT;
		count++;
	// This checks for interrupt status register bit 15(INT)
	}while (IntStatReg & INT_STAT_REG);	
	 
	pOneNANDRegs->COMMAND = ONE_NAND_CORE_RESET;

	if( WaitForCommandCompletion(INT_STAT_REG_RSTI_PEND, (UINT32)RESET_TIMEOUT_MS )!= NoError)
	{
	   return GeneralError;
	}

	return Retval;
}
UINT_T ResetOneNAND()
{
	UINT_T Retval = NoError;
	UINT16 IntStatReg=0;
	int count=0;

	do{
		 // This will return if there's no FlexOneNAND device connected.
		 if(count > INT_CLEAR_RETRY_CNT)
		 {
	       return FlexOneNANDError;
		 }
		// clear interrupt register
		pOneNANDRegs->INTRPT = INT_STAT_REG_CLEAR;
		IntStatReg = pOneNANDRegs->INTRPT;
		count++;
	// This checks for interrupt status register bit 15(INT)
	}while (IntStatReg & INT_STAT_REG);	
	 
	pOneNANDRegs->COMMAND = ONE_NAND_RESET;

	if( WaitForCommandCompletion(INT_STAT_REG_RSTI_PEND, (UINT32)RESET_TIMEOUT_MS )!= NoError)
	{
	   return FlexOneNANDError;
	}

	return Retval;
}
UINT_T OTPAccess()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT32 ComputeDivisorShiftFromPowerOf2( UINT32 power )
{
  UINT32 shift = 0;

  if( (power == 0) || (power & 0x1))
    return 0;

  while( !(power & 0x01) )
  {
    shift +=1;
    power >>= 1;
  }


  //Only one bit can be set in power...
  //otherwise it's not a power of 2...
  //----------------------------------
  power >>=1; //Power should now == 0 if only one bit set
  if( power )
    return 0;

  return shift;
}
