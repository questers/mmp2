/******************************************************************************
**	(C)Copyright Marvell. All Rights Reserved. 2007  
**
**  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
**	The copyright notice above does not evidence any actual or intended  
**  publication of such source code.	
**
**  This Module contains Proprietary Information of Marvell and should be treated 
**  as Confidential.
**	  
**  The information in this file is provided for the exclusive use of the licensees 
**  of Marvell.	
**	Such users have the right to use, modify, and incorporate this code into products 
**  for purposes authorized by the license agreement provided they include this notice 
**  and the associated copyright notice with any such product. 
**  The information in this file is provided "AS IS" without warranty.
**
**  FILENAME: FlexOneNAND_Cmd.c
**
**  PURPOSE:  The driver for Flex One NAND device.
**
******************************************************************************/
#include "FlexOneNAND_Cmd.h"
#include "OneNAND_Driver.h"
#include "Errors.h"
#include "loadoffsets.h"

UINT_T FlexOneNAND_HotReset();
extern void memset(void *Addr, unsigned char Val, unsigned long Size);

extern P_FlexOneNAND_Properties_T GetFlexOneNANDProperties(void);
extern P_FLEX_REGISTERS_T pFlexRegs;

/*
 * LoadFlexPage:  
 *	This function uses the Load command to retrive
 *	up to one full page of data into the Flex OneNAND part
 *
 * Inputs:
 *	FlashOffset - location in flash to burn data
 *	buffer - pointer to location of data to be written
 *	size - number of bytes to be written from buffer
 *			
 *		Note: address + size CANNOT span multiple pages
 *			This MUST be handled in upper level driver
 *
 * Outputs:
 *	Return value - returns NoError on success
 *		Failures can be:
 *			Timeout Error
 *			Read Error
 */
UINT_T LoadFlexPage(UINT_T FlashOffset, UINT_T buffer, UINT_T size, FlashBootType_T fbt)
{
	UINT_T Retval = NoError;
	UINT_T BlockNum, BlockSize, OffsetIntoBlock, SectorCount, SectorAddr, PageAddr;
	VUINT16_T *pDataRAM, *pBuff;
	VUINT8 *pECCStatus;
	UINT_T EccFlag, i; 
	UINT16_T device_select = 0;
	P_FlexOneNAND_Properties_T pFlexProp = GetFlexOneNANDProperties();

	//Make sure ECC usage is correct
	EccFlag = SetECCUsage(GetFlashProperties(fbt)->FlashSettings.UseHwEcc, &pFlexRegs->SYSCONF1);

	//Setup Pointers	
	pDataRAM = (volatile unsigned short *) FLEX_DATARAM_MAIN_BASE;
	pBuff = (volatile unsigned short *) buffer;
	
	//get the block number
	GetFlexBlockInfo(FlashOffset, &BlockNum, &BlockSize);
	//get the offset into the block - need to find out which page and sector we're reading from
	OffsetIntoBlock = FlashOffset & (BlockSize - 1);

	//get the sector address WITHIN a PAGE (that is what the '& 0x3' does)
	SectorAddr = (OffsetIntoBlock/SECTOR_SIZE_IN_BYTES) & 0x3;
	PageAddr = (OffsetIntoBlock/(GetPageSize(fbt))); 
	
	//figure out how many sectors to read
	//NOTE: this routine can only read up to 8 sectors at a time
	SectorCount = size / SECTOR_SIZE_IN_BYTES;
	//if we are not on a sector boundary, increment count for partitial sector
	if(SectorCount * SECTOR_SIZE_IN_BYTES != size)
		SectorCount++;

	//only set the DBS and DFS if using a DUAL CHIP package
	// AND the block num is greater than 1024 blocks (meaning its in the second Chip)
	if( (pFlexProp->MuxType == DUAL_CHIP) && ( BlockNum > FLEX_SA1R_FBA_MASK) )
		device_select = FLEX_SA2R_DBS_FLAG;

    pFlexRegs->SA1 = (device_select) | (BlockNum & MUX_SA1R_FBA_MASK);

	pFlexRegs->SA2 = device_select;


	pFlexRegs->SA8 = (PageAddr << 2) | SectorAddr;

	//ignore FSA (sector count):  Always load a full page into DataBuff
	pFlexRegs->SBR = (UINT16_T) (SBR_WRITE_DEFAULT);	

			
	pFlexRegs->INTRPT = INT_STAT_REG_CLEAR;
	pFlexRegs->COMMAND =ONE_NAND_LOAD_SECTOR_INTO_BUFFER;

    if( WaitForCommandCompletion(PEND_READ, (UINT32)ONENAND_TIMEOUT_MS )!= NoError)
    {
       return ReadError;
    }

	// Host reads data from DataRAM
	for (i=0;i<size;i+=2)
	{
		*pBuff++ = *pDataRAM++; 
	}
	
	if(EccFlag)
	{
		pECCStatus = (VUINT8*)&pFlexRegs->ECCSR1;
		//we allow 1 and 2 bit errrors to go without catching them
		for( i = 0; i < SectorCount; i++)
		{
			//3bit uncorrectable error
			if(pECCStatus[i] & 0x4)
				Retval = ReadDisturbError;
			//4bit uncorrectable error
			if(pECCStatus[i] & 0x8)
				return ReadError;
		}
	}
	return Retval;
}

UINT_T SuperLoadFlexPage()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT_T LSBPageRecoveryRead()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT_T PIAccess()
{
	UINT_T Retval = NoError;
	P_FlexOneNAND_Properties_T pFlexProp = GetFlexOneNANDProperties();
	
	//Step 1: Enter PI Block Access mode using the following sequence
	//Step 1A: Write DFS, FBA of Flash (FBA can be omitted in this case)
	//         NOTE: Not clear from docs if DFS/DDP bits need to be set or not
	pFlexRegs->SA1 = pFlexProp->MuxType << 15;

	//Step 1B: Select DataRAM for DDP
	pFlexRegs->SA2 = pFlexProp->MuxType << 15;

	//Step 1C: Clear Interrupt Register
	pFlexRegs->INTRPT = INT_STAT_REG_CLEAR;

	//Step 1D: Write PI Access Command
	pFlexRegs->COMMAND = FLEX_ACCESS_PARTITION_BLOCK;

	//Step 1E: Wait for Interrupt
	Retval = WaitForCommandCompletion(INT_STAT_REG, (UINT32)ONENAND_TIMEOUT_MS*3 );

	return Retval;
}

//----------------------------------------------------------------------
// PIUpdate() - Updates the Partition Information between SLC and MLC
//              areas within the Flex-OneNAND device.
//----------------------------------------------------------------------
//
// INPUTS:
//
//	blockNumber - Block number after which the SLC area ends and the
//                MLC area begins. NOTE: Block 0 is ALWAYS a SLC block
//                as a result.
//
UINT_T PIUpdate( UINT32 blockNumber )
{
	UINT_T Retval = NoError, count;
	VUINT16_T *pDataRAM, *pBuff;
	P_FlexOneNAND_Properties_T pFlexProp = GetFlexOneNANDProperties();
	pDataRAM = (volatile unsigned short *) FLEX_DATARAM_MAIN_BASE;

	//Step 1: Enter PI Block Access mode using the following sequence
	//Step 1A: Write DFS, FBA of Flash (FBA can be omitted in this case)
	//         NOTE: Not clear from docs if DFS/DDP bits need to be set or not
	pFlexRegs->SA1 = pFlexProp->MuxType << 15;

	//Step 1B: Select DataRAM for DDP
	pFlexRegs->SA2 = pFlexProp->MuxType << 15;

	//Step 1C: Clear Interrupt Register
	pFlexRegs->INTRPT = INT_STAT_REG_CLEAR;

	//Step 1D: Write PI Access Command
	pFlexRegs->COMMAND = FLEX_ACCESS_PARTITION_BLOCK;

	//Step 1E: Wait for Interrupt
	Retval = WaitForCommandCompletion(INT_STAT_REG, (UINT32)ONENAND_TIMEOUT_MS*3 );
	if (Retval != NoError)
		return Retval;

	//PI Block Access should have been sucessful if we get this far
	//Step 2: Erase PI Block

	Retval = FlexBlockErase(0);
	if (Retval != NoError)
		return Retval;

	//PI Erase should have been sucessful if the code got this far
	//Step 3: Program SLC/MLC boundry address
	//Step 3A: Write data into DataRAM at offset 0x0
	//         Bits 15:14 are lock bits (optional)
	//         Bits 13:10 are reserved and should always be set

	*pDataRAM++ = 0x3C00 | blockNumber;   // Set the SLC/MLC boundry block
	// Fill the rest of the buffer with 1's
	count = 510;
	for( ; count > 0 ; count -= 2 )
		*pDataRAM++ = 0xFFFF;

	//Step 3B: Write DFS to Start Address 1 Register
	pFlexRegs->SA1 = pFlexProp->MuxType << 15;

	//Step 3C: Set DBS for Start Address 2 Register
	pFlexRegs->SA2 = pFlexProp->MuxType << 15;

	//Step 3D: Set BSA, BAC for Start Address 8 Register
	pFlexRegs->SA8 = 0;

	//Set System Config Register	
	//pFlexRegs->SYSCONF1 = CONF1_WRITE_DEFAULT | CONF1_ECC_OFF; //Turn ECC OFF for now

	//Step 3C: Set BSA for Start Buffer Register
	pFlexRegs->SBR = (UINT16_T) SBR_WRITE_DEFAULT;

	//Step 3E: Clear Interrupt Register
	pFlexRegs->INTRPT = INT_STAT_REG_CLEAR;

	//Step 3F: Write Program Page From Buffer command
	pFlexRegs->COMMAND = FLEX_PROGRAM_PAGE_FROM_BUFFER;

	//Step 3G: Wait for Interrupt
	Retval = WaitForCommandCompletion(PEND_WRITE, (UINT32)ONENAND_TIMEOUT_MS*3 );
	if (Retval != NoError)
		return Retval;

	//Step 3H: Read Controller Status Register if interrupt error
	if ((pFlexRegs->CNTSTAT & FLEX_CSR_ERROR) == FLEX_CSR_ERROR)
	{
		Retval = GeneralError;
		return Retval;
	}

	//PI programming should have been sucessful if the code got this far
	//Step 4: PI Block Update - Update the partition information so it takes effect.
	//        Normally this is done through a cold reset but it can also be done by
	//        issuing the Partition Information Update command (0x05) after PI Access
	//        mode entry. 
	//
	//Step 4A: Write DFS to Start Address 1 Register
	pFlexRegs->SA1 = pFlexProp->MuxType << 15;

	//Step 4B: Set BSA, BAC for Start Address 8 Register
	pFlexRegs->SA8 = 0;

	//Step 4C: Clear Interrupt Register
	pFlexRegs->INTRPT = INT_STAT_REG_CLEAR;

	//Step 4D: Write PI Update command
	pFlexRegs->COMMAND = FLEX_LSB_READ_PI_UPDATE;

	//Step 4E: Wait for Interrupt
	Retval = WaitForCommandCompletion(INT_STAT_REG, (UINT32)ONENAND_TIMEOUT_MS*3 );
	if (Retval != NoError)
		return Retval;

	//PI Block Update should have been sucessful if the code got this far
	//Step 5: Do a NAND Flash Reset to disable access to PI block

	Retval = ResetFlexOneNAND();

	return Retval;
}

//----------------------------------------------------------------------
// PIRead() - Returns the PI Block information to the caller
//----------------------------------------------------------------------
//
// INPUT:
//
//      None
//
// OUTPUT:
//
//      16 bit data - value of the PI Configuration register
//
//        Bits 15:14 - Lock bits
//        Bits 13:10 - Reserved - always set to 1
//        Bits 9:0   - Block number where SLC area ends and MLC area begins
//
UINT_T PIRead(UINT16_T *Status)
{
	UINT_T Retval = NoError;
	VUINT16_T *pDataRAM, *pBuff;
	P_FlexOneNAND_Properties_T pFlexProp = GetFlexOneNANDProperties();
	pDataRAM = (volatile unsigned short *) FLEX_DATARAM_MAIN_BASE;

	//Step 1: Enter PI Block Access mode using the following sequence:
	//Step 1A: Write DFS, FBA of Flash (FBA can be omitted in this case)
	//         NOTE: Not clear from docs if DFS/DDP bits need to be set or not
	pFlexRegs->SA1 = pFlexProp->MuxType << 15;

	//Step 1B: Select DataRAM for DDP
	pFlexRegs->SA2 = pFlexProp->MuxType << 15;

	//Step 1C: Clear Interrupt Register
	pFlexRegs->INTRPT = INT_STAT_REG_CLEAR;

	//Step 1D: Write PI Access Command
	pFlexRegs->COMMAND = FLEX_ACCESS_PARTITION_BLOCK;

	//Step 1E: Wait for Interrupt
	Retval = WaitForCommandCompletion(INT_STAT_REG, (UINT32)ONENAND_TIMEOUT_MS*3 );
	if (Retval != NoError)
		return Retval;

	//Step 2: Read (load) PI Block dat into buffer using the following sequence
	//Step 2A: Write DFS, FBA of Flash (FBA can be omitted in this case)
	//         NOTE: Not clear from docs if DFS/DDP bits need to be set or not
	pFlexRegs->SA1 = pFlexProp->MuxType << 15;

	//Step 2B: Select DataRAM for DDP
	pFlexRegs->SA2 = pFlexProp->MuxType << 15;

	//Step 2C: Write FPA, FSA of flash (Start Address 8 Register)
	pFlexRegs->SA8 = 0;

	//Step 2D: Set System Config Register	
	//pFlexRegs->SYSCONF1 = CONF1_WRITE_DEFAULT | CONF1_ECC_OFF; //Turn ECC OFF for now   

	//Step 2E: Clear Interrupt Register
	pFlexRegs->INTRPT = INT_STAT_REG_CLEAR;

	//Step 2F: Write Load Page Command
	pFlexRegs->COMMAND = FLEX_LOAD_PAGE_INTO_BUFFER;

	//Step 2G: Wait for Interrupt
	Retval = WaitForCommandCompletion(PEND_READ, (UINT32)ONENAND_TIMEOUT_MS*3 );
	if (Retval != NoError)
		return Retval;

	//Step 3: Read data from internal buffer
	*Status = *pDataRAM;

	//Step 4: Do a Warm/Hot NAND Flash Reset to disable access to PI block

	Retval = ResetFlexOneNAND();
	if (Retval != NoError)
		return Retval;

	return NoError;
}

/*
 * ProgramFlexPage:  
 *	This function uses the Program command to program
 *	one full page of data into the OneNAND part
 *
 * Inputs:
 *	address - location in flash to burn data
 *	buffer - pointer to location of data to be written
 *	size - number of bytes to be written from buffer
 *			
 *		Note: address + size CANNOT span multiple pages
 *			This MUST be handled in upper level driver
 *
 * Outputs:
 *	Return value - returns NoError on success
 *		Failures can be:
 *			Flash is Locked
 *			Timeout Error
 *			Program Error
 */
UINT_T ProgramFlexPage(UINT_T address, UINT_T buffer, UINT_T size, FlashBootType_T fbt)
{
	UINT_T Retval = NoError;
	UINT_T BlockSize, PageSize, BlockNum, PageNum, pre_bytes, post_bytes;
	VUINT16_T *pDataRAM, *pBuff;
	UINT16_T device_select = 0;
	P_FlashProperties_T pFlashProp = GetFlashProperties(fbt);
	P_FlexOneNAND_Properties_T pFlexProp = GetFlexOneNANDProperties();

	//Initial Calculations:
	PageSize = GetPageSize(fbt);
	//Determine the block we are on
	//get the block number
	GetFlexBlockInfo(address, &BlockNum, &BlockSize);
	//determine which page we are writing
	PageNum	= (address & (BlockSize - 1)) / PageSize;
	
	//Determine pre-bytes for this page
	pre_bytes = address & (PageSize - 1);
	//Determine left over bytes for this page
	post_bytes = PageSize - size - pre_bytes;

	//Setup Pointers	
	pDataRAM = (volatile unsigned short *) FLEX_DATARAM_MAIN_BASE;
	pBuff = (volatile unsigned short *) buffer;

	//Step 1:
	//Set DBS bit in Start Address 2 register
	//only set the DBS and DFS if using a DUAL CHIP package
	// AND the block num is greater than 1024 blocks (meaning its in the second Chip)
	if( (pFlexProp->MuxType == DUAL_CHIP) && ( BlockNum > FLEX_SA1R_FBA_MASK) )
		device_select = FLEX_SA2R_DBS_FLAG;

	pFlexRegs->SA2 = device_select;
			   
	//Step 2: Load data into BufferRAM
	//Fill any initial values with 1's
	for( ; pre_bytes > 0 ; pre_bytes -= 2 )
		*pDataRAM++ = 0xFFFF;
	
	//Write the data 16 bits at a time
	for( ; size > 0 ; size -= 2 )
		*pDataRAM++ = *pBuff++;

	//If there is any leftover space in the page, fill it with 1's
	for( ; post_bytes > 0 ; post_bytes -= 2 )
		*pDataRAM++ = 0xFFFF;

	//Step 3:
	//Fill out Start Address 1 Register 
	pFlexRegs->SA1 = (device_select) | (BlockNum & FLEX_SA1R_FBA_MASK);

	//Step 4:
	//Read Write Protection Status
	if (pFlexRegs->WPS & FLEX_WPSR_UNLOCKED != FLEX_WPSR_UNLOCKED)
		return FlashLockError;

	//Step 5:
	//Write Page and Sector Addres (must be 0) to Start Address 8
	pFlexRegs->SA8 = (PageNum << 2)| 0x0;

	//Step 6:
	//Fill out Start Buffer Register info (must be 0x0800 for Program)
	pFlexRegs->SBR = (UINT16_T) SBR_WRITE_DEFAULT;

	//Step 7:
	//Set System Config Register - leave at default 
	//uncomment next line to turn of ECC	
   //	pFlexRegs->SYSCONF1 = CONF1_WRITE_DEFAULT | CONF1_ECC_OFF; 

	//Step 8:
	//Clear Interrupt Register
	pFlexRegs->INTRPT = INT_STAT_REG_CLEAR;
	
	//Step 9:
	//Write PROGRAM Cmd to command register
	pFlexRegs->COMMAND = FLEX_PROGRAM_PAGE_FROM_BUFFER;

	//Step 10:
	//Wait for WRITE interrupt
	Retval = WaitForCommandCompletion(PEND_WRITE, (UINT32)ONENAND_TIMEOUT_MS );
    if (Retval != NoError)
		return Retval;

	//Step 11:
	//Read Controller status register
	if ((pFlexRegs->CNTSTAT & FLEX_CSR_ERROR) == FLEX_CSR_ERROR)
		Retval = ProgramError;

	return Retval;
}


UINT_T FinishCacheProgram()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT_T CacheProgram()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT_T UnlockFlexArray()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT_T LockFlexArray()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT_T LockTightFlexArray()
{
	UINT_T Retval = NoError;
	return Retval;
}

//------------------------------------------
// FlexBlockErase()
//------------------------------------------
UINT_T FlexBlockErase( UINT32 blockNumber )
{
	UINT_T Retval;
    FLEX_SA1_REG sa1reg;
    
    //First unlock Blocks
    //-------------------
	Retval = UnlockNANDArrays();
	if (Retval != NoError)
		return Retval;
    
    // setup the FBA in SA1
    // bits0-9 = blockNumber
    // bit 15 = blocks #'s above 1023
    //-------------------------------
    if( blockNumber > 1023 )
    {
      sa1reg.bits.FLEX_DFS = 1;
      sa1reg.bits.FLEX_RESERVED = 0;
      sa1reg.bits.FLEX_FBA = blockNumber-1024;
    }
    else
    {
      sa1reg.bits.FLEX_DFS = 0;
      sa1reg.bits.FLEX_RESERVED = 0;
      sa1reg.bits.FLEX_FBA = blockNumber;   
    }
      
    //Write FBA & DFS
    //----------------------------
    pFlexRegs->SA1 = sa1reg.value;

	//Step 4:
	//Read Write Protection Status
	if (pFlexRegs->WPS & FLEX_WPSR_UNLOCKED != FLEX_WPSR_UNLOCKED)
		return FlashLockError;
    
    //set DBS in SA2
    //----------------
    if(blockNumber > 1023)
      pFlexRegs->SA2 = ( 1 << 15);
    else
      pFlexRegs->SA2 = 0;
    
	//Clear Interrupt Register
    //ensure write complete
    //------------------------
	pFlexRegs->INTRPT = INT_STAT_REG_CLEAR;
    sa1reg.value = pFlexRegs->INTRPT;
    
    //Write the command
    pFlexRegs->COMMAND = FLEX_BLOCK_ERASE;
    
    //Wait for operation complete
    //---------------------------
	Retval = WaitForCommandCompletion(PEND_ERASE, (UINT32)ONENAND_TIMEOUT_MS*3 );
    if (Retval != NoError)
		return EraseError;

	//Read Controller Status Register
	if ((pFlexRegs->CNTSTAT & FLEX_CSR_ERROR) == FLEX_CSR_ERROR)
		Retval = EraseError;
        
	return Retval;
}

UINT_T FlexEraseSuspend()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT_T FlexEraseResume()
{
	UINT_T Retval = NoError;
	return Retval;
}

UINT_T ResetFlexNANDCore()
{
	UINT_T Retval = NoError;
	UINT16 IntStatReg=0;
	int count=0;

	do{
		 // This will return if there's no FlexOneNAND device connected.
		 if(count > INT_CLEAR_RETRY_CNT)
		 {
		   return GeneralError;
		 }
		// clear interrupt register
		pFlexRegs->INTRPT = INT_STAT_REG_CLEAR;
		IntStatReg = pFlexRegs->INTRPT;
		count++;
	// This checks for interrupt status register bit 15(INT)
	}while (IntStatReg & INT_STAT_REG);	
	 
	pFlexRegs->COMMAND = FLEX_NAND_CORE_RESET;

	if( WaitForCommandCompletion(INT_STAT_REG_RSTI_PEND, (UINT32)RESET_TIMEOUT_MS )!= NoError)
	{
	   return GeneralError;
	}

	return Retval;
}

UINT_T ResetFlexOneNAND()
{
	UINT_T Retval = NoError;
	UINT16 IntStatReg=0;
	int count=0;

	do{
		 // This will return if there's no FlexOneNAND device connected.
		 if(count > INT_CLEAR_RETRY_CNT)
		 {
	       return FlexOneNANDError;
		 }
		// clear interrupt register
		pFlexRegs->INTRPT = INT_STAT_REG_CLEAR;
		IntStatReg = pFlexRegs->INTRPT;
		count++;
	// This checks for interrupt status register bit 15(INT)
	}while (IntStatReg & INT_STAT_REG);	
	 
	pFlexRegs->COMMAND = FLEX_MUX_ONENAND_RESET;

	if( WaitForCommandCompletion(INT_STAT_REG_RSTI_PEND, (UINT32)RESET_TIMEOUT_MS )!= NoError)
	{
	   return FlexOneNANDError;
	}

	return Retval;

}
