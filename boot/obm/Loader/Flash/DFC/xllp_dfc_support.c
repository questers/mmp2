/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into
 *  products for purposes authorized by the license agreement provided they
 *  include this notice and the associated copyright notice with any such
 *  product.
 *  The information in this file is provided "AS IS" without warranty.
 *
 *
 *  FILENAME: xllp_dfc_support.c
 *
 *  PURPOSE:  XLLP DFC support routines.
 *
 ******************************************************************************/

#include "xllp_dfc.h"
#include "xllp_dfc_defs.h"
#include "PlatformConfig.h"
#include "misc.h"
#include "nand.h"
#include "Errors.h"
#include "Typedef.h"
#include "predefines.h"

//++
//************************************************************
// Unit Test Support
#ifdef _DFC_EM
#include "em_devlib.h"
#endif
//************************************************************
//--

//#ifdef _DFC_EM
#define MIN(x, y)  (x < y) ? x : y
/*#else
#define MIN(x, y)           \
   ({ typeof (x) x_ = (x);  \
      typeof (y) y_ = (y);  \
      (x_ < y_) ? x_ : y_;  \
    })
#endif*/

// ONFI Parameter Page Offsets (in Bytes)
#define  K_ONFI_FEATURES_SUPPORTED_OFFSET 6
#define  K_ONFI_MANUFACTURER_ID_OFFSET 64
#define  K_ONFI_PAGE_SIZE_OFFSET   80
#define  K_ONFI_SPARE_BYTES_PER_PAGE_OFFSET 84
#define  K_ONFI_PAGES_PER_BLOCK_OFFSET      92
#define  K_ONFI_BLOCKS_PER_LUN  96
#define  K_ONFI_NUMBER_BITS_PER_CELL_OFFSET  102
#define  K_ONFI_NUMBER_BITS_ECC_CORRECTABILITY_OFFSET 112

//Prototypes
UINT_T xdfc_calculateOnfiCrc(UINT8_T *Address, UINT_T length);
UINT_T xdfc_getInt(UINT8_T * BaseAddress, unsigned int ByteOffset, unsigned int NumBytes);

////////
/////////
//////       S U P P O R T   R O U T I N E S
////////
///////


// xdfc_stop: this routine clears the run bit in the dfc
//            this is usually only called in error handling,
//            specifically, when the WRCMDRUN bit does not assert.
//            the assertion failure is usually due to the dfc
//            already being in a run state when the NDRUN bit
//            is written.

// this routine does not handle draining the fifo, which may
// be necessary if an error occurred in the middle of an operation.
//
// it may be better to reset the controller than to try to
// drain the fifo since there are no underflow indicators.
unsigned long xdfc_stop()
{
    NDCR_REG        control;
    unsigned long   temp;

    DFC_REG_READ(DFC_CONTROL, control.value);

    if( control.value & DFC_CTRL_ND_RUN )   // only do this if the dfc is running.
    {
        control.value &= ~DFC_CTRL_ND_RUN;          // force the run bit off.
        DFC_REG_WRITE(DFC_CONTROL, control.value);

        DFC_REG_READ(DFC_CONTROL, control.value);   // read back to ensure write completion
        temp = control.value + 1;                   // second part of ensuring write completion: use the read result in any way.
    }

    return control.value;
}




// xdfc_write_LP: xdfc_write for large pages (page size > 2k)
//
// MLC devices typically have have large pages.
//
// This routine will use a multi-stage write sequence to write large pages to flash.
// For each page, the multi-stage program sequence is:
//  1. Issue program command, with extended type set to command_dispatch, to start the write process
//  2. Then issue a program command again, with extended type set to "naked_write", each 2k chunk of data to be written.
//  use the NC bit to chain those commands together until the whole page has been programmed.
//
// notes:
//  1. All flash offsets must be page aligned. that will eliminate the need to do any "pre-byte" handling.
//  2. If the last chunk does not fill a page completely, the remainder of the page will be padded. that
//     efectively removes "post-byte" support, also.
//  The reason for removing pre-byte and post-byte support is so that the stack does not have to keep
//  buffers for scratch pages on the stack.
//
//  If pre-byte and post-byte do need to be supported, then only a single scratch page area of memory
//  is required (not two separate ones for pre-byte and post-byte). Use a single buffer. Only read into it
//  when necessary: load it at the beginning for pre-byte handling. Load it during the last page write for
//  post byte handling. Also handle the case where the pre-byte, real data, and post-byte data are all in
//  the same page. (Would only need one scratch page area of memory in that case, too.)

// note:
//  since pages for MLC can be so large, its not possible to keep a pre-byte
//  and post-byte page on the stack. result:
//  - all programs must start at a page boundary. eliminates "pre-byte" support.
//  - all pages will be filled to end with the "erase pattern" (0xff). eliminates "post-byte" support.



UINT_T xdfc_write_LP(P_DFC_BUFFER buffer, unsigned int address, unsigned int bytes, P_NAND_Properties_T pNAND_Prop)
{
    // variables for programming the dfc controller
    CMD_BLOCK       *default_cmds;
    NDCR_REG        control;
    NDCB0_REG       command;
    NDCB1_REG       addr1234;
    NDCB2_REG       addr5;

    // variables for keeping track of buffers & lengths.
    unsigned int    dfc_internal_buffer_size;       // this value determines how much data to transfer during
                                                    // the data transfer phase of a naked command transaction.
    unsigned int    page, pages, page_size;
    unsigned int    stage, last_stage, stages_per_page;         // number of data transfer stages per page.
    unsigned int    bytes_to_write, words_to_write;
    unsigned int    status;

    unsigned int    buf[2];     // used for read status commands.
    UINT_T          Retval;
    unsigned int    i;

    // these masks will be initialized according to which chip select the target device is on.
    unsigned int    ready_mask;
    unsigned int    page_done_mask;
    unsigned int    command_done_mask;
    unsigned int    bad_block_detect_mask;



    /*
    *   Preliminary stuff.
    */

    // stop any command in progress.
    // FIXME: this is temporary, until more robust and complete error handling is
    // implemented. each routine should ensure that the dfc is returned to a known
    // state before returning - even (or especially!) in the case of error.
    DFC_REG_READ(DFC_CONTROL, control.value);
    control.value &= ~DFC_CTRL_ND_RUN;
    DFC_REG_WRITE(DFC_CONTROL, control.value);


    dfc_internal_buffer_size = 2048;        // this could be platform dependent. so far, all of our processors use 2k internal buffer.

    default_cmds = pNAND_Prop->device_cmds; // used to find the specific command code for the nand media being accessed.
    // bpc: fix: turn off auto_rs for now. FIXME: investigate why auto_rs doesn't work.
    default_cmds->pg_program.bits.AUTO_RS = 0;

    // each chip select has its own set of status bits.
    // determine which status bits to observer.
    if( default_cmds->pg_program.bits.CSEL == 0 )   // FIXME: look for a more flexible way to determine chip select.
    {
        ready_mask              = DFC_SR_RDY0;
        page_done_mask          = DFC_SR_CS0_PAGED;
        command_done_mask       = DFC_SR_CS0_CMDD;
        bad_block_detect_mask   = DFC_SR_CS0_BBD;
    }
    else
    {
        ready_mask              = DFC_SR_RDY1;
        page_done_mask          = DFC_SR_CS1_PAGED;
        command_done_mask       = DFC_SR_CS1_CMDD;
        bad_block_detect_mask   = DFC_SR_CS1_BBD;
    }

    page_size = pNAND_Prop->PageSize;               // eg, 2k for SLC NAND, 4k & > for MLC.
    pages = (bytes+(page_size-1)) / page_size;
    stages_per_page = page_size / dfc_internal_buffer_size;
    last_stage = stages_per_page-1;                 // used to determine when to clear the next command NC flag in the NDCB0 register.


    // start the programming process:
    for(page=0;page<pages;page++)
    {
        // (start the multi-stage naked write sequence with a dispatch command)

        // Clear status bits and set DFC to expect a command.
        DFC_REG_READ(DFC_STATUS, status);
	   	status |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
		DFC_REG_WRITE(DFC_STATUS, status);
		        
        // Wait till status bits are cleared
        Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);
        
        if (Retval != NoError)
        {
            xdfc_stop();
            return ProgramError;
        }

        // set the run bit so the dfc enters the wait for command state.
        DFC_REG_READ(DFC_CONTROL, control.value);
        control.value |= DFC_CTRL_ND_RUN;
        DFC_REG_WRITE(DFC_CONTROL, control.value);

        // Wait till DFC is ready for next command to be issued.
        Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);     // wait for write command request bit to assert
        if (Retval != NoError)
        {
            xdfc_stop();
            return DFC_WRCMD_TO;
        }
        DFC_REG_WRITE(DFC_STATUS, DFC_SR_WRCMDREQ);
        
        // Wait till status bit is cleared
        Retval = _WaitForDFCOperationPulldown(DFC_SR_WRCMDREQ, 50);
        
        if (Retval != NoError)
        {
            xdfc_stop();
            return ProgramError;
        }

        // dfc is ready for a command now...


        /*
         *  convert the flash address into values appropriate for the address fields of the dfc NDCBx registers.
         */
        getAddr(address, &addr1234, &addr5, pNAND_Prop);

        // build a multistage command: operation (program), extended_type = dispatch, and address
        command.value = default_cmds->pg_program.value;
        command.bits.CMD_XTYPE = DFC_CMDXT_COMMAND_DISPATCH;
        command.bits.NC = 1; // valid command following this one

        // first we send the dispatch command
        *DFC_COMMAND0 = command.value;    // send program command with command dispatch
        *DFC_COMMAND0 = addr1234.value;   // page address 1-4
        *DFC_COMMAND0 = addr5.value;      // page address 5
        Retval = _WaitForDFCOperationComplete(command_done_mask, 10);       // wait for write command request bit to assert
        if (Retval != NoError)
        {
            xdfc_stop();
            return DFCCS0CommandDoneError;  // FIXME: should select between cs0 & cs1 as appropriate.
        }
        DFC_REG_WRITE(DFC_STATUS, command_done_mask);
        
        // Wait till status bit is cleared
        Retval = _WaitForDFCOperationPulldown(command_done_mask, 50);
        
        if (Retval != NoError)
        {
            xdfc_stop();
            return ProgramError;
        }


        // now a multi-stage write has been initiated.
        // send a xtype write command for each stage.

        for(stage=0;stage<stages_per_page;stage++)
        {
            // Wait till DFC is ready for next command to be issued.
            Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);     // wait for write command request bit to assert
            if (Retval != NoError)
            {
                xdfc_stop();
                return DFC_WRCMD_TO;
            }
            DFC_REG_WRITE(DFC_STATUS, DFC_SR_WRCMDREQ);
            
            // Wait till status bit is cleared
            Retval = _WaitForDFCOperationPulldown(DFC_SR_WRCMDREQ, 50);
        
            if (Retval != NoError)
            {
                xdfc_stop();
                return ProgramError;
            }

            // set the dfc to begin filling the internal buffer from software.
            // now we send the naked write command
            command.value = default_cmds->pg_program.value;
            if( stage != last_stage )
            {
                command.bits.CMD_XTYPE = DFC_CMDXT_NAKED_WRITE; // Naked write, not last
                command.bits.NC = 1;                // another stage/command following this one
            }
            else
            {
                command.bits.CMD_XTYPE = DFC_CMDXT_NAKED_WRITE_WITH_FINAL_COMMAND;
                command.bits.NC = 0;                // another stage/command is not following this one
            }
            *DFC_COMMAND0 = command.value;          // send naked write command
            *DFC_COMMAND0 = addr1234.value;         // dummy write
            *DFC_COMMAND0 = addr5.value;            // dummy write

            // Wait for dfc to indicate it is ready
            Retval = _WaitForDFCOperationComplete(DFC_SR_WRDREQ, 10);   // wait for write data request
            if (Retval != NoError)
            {
                xdfc_stop();
                return DFC_WRREQ_TO;
            }
            DFC_REG_WRITE(DFC_STATUS, DFC_SR_WRDREQ);
            
            // Wait till status bit is cleared
            Retval = _WaitForDFCOperationPulldown(DFC_SR_WRDREQ, 50);
        
            if (Retval != NoError)
            {
                xdfc_stop();
                return ProgramError;
            }


            // fill the dfc internal fifo

            // determine how many real bytes need to be sent.
            bytes_to_write = bytes;
            if(bytes_to_write>dfc_internal_buffer_size)
                bytes_to_write=dfc_internal_buffer_size;            // limit amount to write to dfc_internal_buffer_size.
            bytes -= bytes_to_write;                                // adjust remaining bytes to write counter.

            words_to_write=bytes_to_write/sizeof(unsigned int);     // convert byte length counter to word length counter.

            for(i=0; i<words_to_write;i++)                          // fill the dfc internal fifo
            {
                *DFC_DATA = *buffer++;
            }
            // end of requested page data write for this stage...



            // see if any padding (to end of internal buffer) needs to be done.
            while(bytes_to_write < dfc_internal_buffer_size)
            {
                *DFC_DATA = 0xffffffff; // fixme: commented out during debug. uncomment this in release code.
                bytes_to_write+=sizeof(unsigned int);
            }
            // end of all page data (requested and pad) for this stage.




            // write the spare areas, too.

            /*
             *  Write the spare area with the "erased pattern"
             */
            for (i = xdfc_getSpareArea_LP(pNAND_Prop); i > 0; i--)
            {
                *DFC_DATA = 0xffffffff; // fixme: commented out during debug. uncomment this in release code.
            }




            // the fifo is full, and now the dfc is writing the contents to flash.
            // after all page and spare area written, pgdn & cmdd should assert
            Retval = _WaitForDFCOperationComplete(page_done_mask, 10);
            if (Retval != NoError)
            {
                xdfc_stop();
                return DFC_PGDN_TO;
            }
            DFC_REG_WRITE(DFC_STATUS, page_done_mask);
            
            // Wait till status bit is cleared
            Retval = _WaitForDFCOperationPulldown(page_done_mask, 50);
        
            if (Retval != NoError)
            {
                xdfc_stop();
                return ProgramError;
            }

            /*
            *  Wait for write to be done.
            *  FIXME: which asserts first? cmdd or pgdn?
            */
            Retval = _WaitForDFCOperationComplete(command_done_mask, 10);
            if (Retval != NoError)
            {
                xdfc_stop();
                return DFCCS0CommandDoneError;
            }
            DFC_REG_WRITE(DFC_STATUS, command_done_mask);
            
            // Wait till status bit is cleared
            Retval = _WaitForDFCOperationPulldown(command_done_mask, 50);
        
            if (Retval != NoError)
            {
                xdfc_stop();
                return ProgramError;
            }

            // Check for double bit error.
            if (*DFC_STATUS & DFC_SR_DBERR)
            {
                xdfc_stop();
                return DFCDoubleBitError;
            }

            // fixme: need to wait for RDY here? in the debugger, see if it is asserting.
        }

        // end of page: do a readstatus and check for successful programming.

        // need to wait for RDY before sending the readstatus command...
        // fixme: make this wait for rdy code check which cs should be used.
        Retval = _WaitForDFCOperationComplete(ready_mask, 10);
        if (Retval != NoError)
        {
            xdfc_stop();
            return DFC_RDY_TO;
        }
        DFC_REG_WRITE(DFC_STATUS, ready_mask);
        // Wait till status bit is cleared
        Retval = _WaitForDFCOperationPulldown(ready_mask, 50);
        
        if (Retval != NoError)
        {
            xdfc_stop();
            return ProgramError;
        }

        xdfc_readstatus( &buf[0], pNAND_Prop);
        // check for an error here...
        if( buf[0] != 0xe0 && buf[0] != 0xc0 )
        {
            xdfc_stop();
            return ProgramError;
        }

        // a little more checking... FIXME: should this be done before calling read status?
        if (*DFC_STATUS & bad_block_detect_mask)
        {
            xdfc_stop();
            return DFCCS0BadBlockDetected;
        }

        // the page was written successfully...go to the next one.
        address += page_size;
    }


   return NoError;
}

//uses flags to control spare area writes for SLC NAND and FBF downloading
int
xdfc_write(P_DFC_BUFFER buffer, UINT_T address, UINT_T bytes, UINT_T bUseSA, UINT_T bUseHwEcc, P_NAND_Properties_T pNAND_Prop)
{
   CMD_BLOCK    *default_cmds;
   NDCR_REG   control;
   NDCB1_REG    addr1234;
   NDCB2_REG    addr5;
   unsigned int  i;
   unsigned int  pages, page_mask, page_first, page_last, page_size, page_extra;
   unsigned int  byte_count = 0, longwords;
   unsigned int  pre_bytes, post_bytes;
   unsigned int  status;
   unsigned int  page1[2048/4], pageN[2048/4];
   unsigned int  buf[2];
   UINT_T Retval;
   unsigned int page_SASize;
   unsigned int eccbytes = 0;
   unsigned int lw;

   //make sure bytes is word aligned
   bytes += (bytes & 3) == 0 ? 0 : 4 - (bytes & 3);

   /*
   *   Preliminary stuff.
    */
   // stop any command in progress.
   // FIXME: this is temporary, until more robust and complete error handling is
   // implemented. each routine should ensure that the dfc is returned to a known
   // state before returning - even (or especially!) in the case of error.
   DFC_REG_READ(DFC_CONTROL, control.value);
   control.value &= ~DFC_CTRL_ND_RUN;
   DFC_REG_WRITE(DFC_CONTROL, control.value);


   page_size  = pNAND_Prop->PageSize;
   page_SASize = bUseSA == 1 ? pNAND_Prop->SpareAreaSize : 0;

   page_mask  = ~(page_size - 1);
   page_first =  address              & page_mask;
   page_last  = (address + bytes - 1) & page_mask;
   page_extra = ((address + bytes) > page_last) ? 1 : 0;

   pages = (bytes / page_size) + page_extra;
   getAddr(address, &addr1234, &addr5, pNAND_Prop);


   //////////////////////////////////////////////////////////////////////////////////
   ///////////                                                           ////////////
   ///////////            P R E S E R V A T I O N   S T E P S            ////////////
   ///////////                                                           ////////////
   //////////////////////////////////////////////////////////////////////////////////
   /*
   *  Calculate the pre and post padding.
    */
   pre_bytes  = address - page_first;
   post_bytes = (page_last + page_size) - (address + bytes);

   if (pre_bytes)  xdfc_read(&page1[0], page_first, page_size, NULL, pNAND_Prop);
   if (post_bytes) xdfc_read(&pageN[0], page_last,  page_size, NULL, pNAND_Prop);


   //////////////////////////////////////////////////////////////////////////////////
   ///////////                                                           ////////////
   ///////////               S T A R T   W R I T I N G                   ////////////
   ///////////                                                           ////////////
   //////////////////////////////////////////////////////////////////////////////////

   /*
   *  SET UP THE COMMAND
    */

    default_cmds = pNAND_Prop->device_cmds;
    getAddr(page_first, &addr1234, &addr5, pNAND_Prop);
#if 0
    // bc: seems like auto_rs is not working, so force it off for now.
    // If read status command is not 0x70, do not use auto read status method.
    if( default_cmds->read_status.bits.CMD1 == 0x70)
        default_cmds->pg_program.bits.AUTO_RS = 1;
#else
    // bc: seems like auto_rs is not working, so force it off for now.
    default_cmds->pg_program.bits.AUTO_RS = 0;
#endif

    // Clear write status bit and set DFC to expect a command.
    DFC_REG_READ(DFC_STATUS, status);
 	status |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
	DFC_REG_WRITE(DFC_STATUS, status);	

	// Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);
        
    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }

    //*DFC_CONTROL |= DFC_CTRL_ND_RUN;
    DFC_REG_READ(DFC_CONTROL, control.value);
    control.value |= DFC_CTRL_ND_RUN;
    DFC_REG_WRITE(DFC_CONTROL, control.value)

    /*
    *  Wait for write command request bit.
    */
    Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
    if (Retval != NoError)
    {
        xdfc_stop();
        return DFC_WRCMD_TO;
    }

    *DFC_COMMAND0 = default_cmds->pg_program.value;
    *DFC_COMMAND0 = addr1234.value;
    *DFC_COMMAND0 = addr5.value;

    /*
    *  Wait for write command request bit.
    */
    Retval = _WaitForDFCOperationComplete(DFC_SR_WRDREQ, 10);
    if (Retval != NoError)
    {
        xdfc_stop();
        return DFC_WRREQ_TO;
    }

    /*
    *  Write the pre padding. (No need to check for a page boundary.)
    */
    byte_count = 0;
    lw  = 0;
    while (pre_bytes)
    {
       *DFC_DATA = page1[lw++];
       byte_count += 4;
       pre_bytes  -= 4;
    }

    /*
    *  Write the buffer contents. (Check for page boundaries.)
    */
    longwords = 0;
    while (bytes)
    {
       *DFC_DATA = buffer[longwords++];
       byte_count += 4;
       bytes      -= 4;

       /*
       *  Check for page boundary. Reissue the write command all over again.
       */
       if (bytes && ((byte_count & ~page_mask) == 0))
       {
          /*
          *  Write the status area padding.
          */
          if ( bUseSA )
          {
             // output the spare area
             for (i = xdfc_getStatusPadding(pNAND_Prop); i > 0; i--)
             {
                // pull the spare area data from the buffer
                *DFC_DATA = buffer[longwords++];
                bytes -= 4;
             }

             // output the ECC bytes if HW ecc is off
             if ( !bUseHwEcc )
             {
                 for (i = eccbytes/4; i > 0; i--)
                 {
                    *DFC_DATA = 0xFFFFFFFF;
                 }
             }
          }
          else
          {
              i = xdfc_getStatusPadding(pNAND_Prop) + ((bUseHwEcc == 1) ? 0 : eccbytes/4);
              for (; i > 0; i--)
                 *DFC_DATA = 0xFFFFFFFF;
          }

          /*
          *  Wait for write to be done.
          */
          if (default_cmds->pg_program.bits.CSEL == 0)
             Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);

          if (Retval != NoError)
          {
                xdfc_stop();
                return DFCCS0CommandDoneError;
          }

          if (default_cmds->pg_program.bits.CSEL == 1)
            Retval = _WaitForDFCOperationComplete(DFC_SR_CS1_CMDD, 10);

          if (Retval != NoError)
          {
                xdfc_stop();
                return DFCCS1CommandDoneError;
          }

          // Check for double bit error.
          if (*DFC_STATUS & DFC_SR_DBERR)
          {
             xdfc_stop();
             return DFCDoubleBitError;
          }

          /*
          *  Wait for rdy to assert.
          */
          if (default_cmds->pg_program.bits.CSEL == 0)
          {
             Retval = _WaitForDFCOperationComplete(DFC_SR_RDY0, 10);
             if (Retval != NoError)
             {
                xdfc_stop();
                return DFC_RDY_TO;
             }
          }

          if (default_cmds->pg_program.bits.CSEL == 1)
          {
             Retval = _WaitForDFCOperationComplete(DFC_SR_RDY1, 10);
             if (Retval != NoError)
             {
                xdfc_stop();
                return DFC_RDY_TO;
             }
          }

          // If using auto status read, read the status returned.
          if (default_cmds->pg_program.bits.AUTO_RS)
          {
              // NOTE: Use the data returned by the NAND in some way...
              buf[0] = *DFC_DATA;
              status = *DFC_DATA;
          }
          else
          {
            xdfc_readstatus( &buf[0], pNAND_Prop);
          }

          // note: in the next few checks, there is no need to stop the controller
          // it's already stopped because the program page command has finished.

          // check the status of the program operation
          if( buf[0] != 0xe0 && buf[0] != 0xc0 )
          {
             return ProgramError;
          }

          if ((default_cmds->pg_program.bits.CSEL == 0) && (*DFC_STATUS & DFC_SR_CS0_BBD))
              return DFCCS0BadBlockDetected;
          if ((default_cmds->pg_program.bits.CSEL == 1) && (*DFC_STATUS & DFC_SR_CS1_BBD))
              return DFCCS1BadBlockDetected;


          // resuming state where controller needs to be stopped on any failure...

          // Increment page address.
          page_first += page_size;
          getAddr(page_first, &addr1234, &addr5, pNAND_Prop);

          /*
          *  SET UP THE COMMAND
          */
          // Clear write status bit and set DFC to expect a command.
          *DFC_STATUS  = DFC_SR_WRCMDREQ;
          
          // Wait till status bit is cleared
          Retval = _WaitForDFCOperationPulldown(DFC_SR_WRCMDREQ, 50);
        
          if (Retval != NoError)
          {
            xdfc_stop();
            return ProgramError;
          }
          
          DFC_REG_READ(DFC_CONTROL, control.value);
          control.value |= DFC_CTRL_ND_RUN;
          DFC_REG_WRITE(DFC_CONTROL, control.value);

          //
          // Wait till DFC is ready for the next command to be issued.
          Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
          if (Retval != NoError)
          {
             xdfc_stop();
             return DFC_WRCMD_TO;
          }

          *DFC_COMMAND0 = default_cmds->pg_program.value;
          *DFC_COMMAND0 = addr1234.value;
          *DFC_COMMAND0 = addr5.value;

          //
          // Wait till the DFC is ready for us to write.
          Retval = _WaitForDFCOperationComplete(DFC_SR_WRDREQ, 10);
          if (Retval != NoError)
          {
             xdfc_stop();
             return DFC_WRREQ_TO;
          }
       }
    }

    /*
    *  Write the post padding. (No need to check for a page boundary.)
    */
    // finish writing out the last page
    lw = (page_size - post_bytes) / 4;
    while (post_bytes)
    {
       *DFC_DATA = pageN[lw++];
       byte_count += 4;
       post_bytes -= 4;
    }

    /*
    *  Write the status area padding.
    */
    if ( bUseSA )
    {
         // output the spare area
         for (i = xdfc_getStatusPadding(pNAND_Prop); i > 0; i--)
         {
            // pull the spare area data from the buffer
            *DFC_DATA = buffer[longwords++];
         }

         // output the ECC bytes if HW ecc is off
         if ( !bUseHwEcc )
         {
             for (i = eccbytes/4; i > 0; i--)
                *DFC_DATA = 0xFFFFFFFF;
        }
    }
    else
    {
         i = xdfc_getStatusPadding(pNAND_Prop) + ((bUseHwEcc == 1) ? 0 : eccbytes/4);
         for (; i > 0; i--)
            *DFC_DATA = 0xFFFFFFFF;
    }

    /*
    *  Wait for write to be done.
    */
    if (default_cmds->pg_program.bits.CSEL == 0)
    {
       Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);
       if (Retval != NoError)
       {
          xdfc_stop();
          return DFCCS0CommandDoneError;
       }
    }

    if (default_cmds->pg_program.bits.CSEL == 1)
    {
       Retval = _WaitForDFCOperationComplete(DFC_SR_CS1_CMDD, 10);
       if (Retval != NoError)
       {
          xdfc_stop();
          return DFCCS1CommandDoneError;
       }
    }

    // wait for the controller to stop running.
    // you would think that the run bit would clear at the same time as CMDD asserts,
    // but that is not the case. if you don't wait for NDCR:RUN to deassert, then
    // the dfc will not be able to issue the readstatus commands that follow.
    //
    // examining a debug log shows that rdy does assert around the same time as run deasserts.
    // so put in a wait for rdy first, before trying to do a read status
    //
    // Question: if must wait for run bit to clear before sending the next command,
    // is overlapping commands support really useful? almost all flash devices allow a read
    // status command to be sent at any time. but it looks like our controller must wait for
    // the command to complete (eg. CMDD asserts, then RDY asserts [and NDRUN clear at the
    // same time] before it will accept the readstatus command. Is software supposed to force
    // the run bit clear if it wants to send overlapping commands like readstatus?

    /*
    *  Wait for rdy to assert.
    */
    if (default_cmds->pg_program.bits.CSEL == 0)
    {
        Retval = _WaitForDFCOperationComplete(DFC_SR_RDY0, 10);
        if (Retval != NoError)
        {
            xdfc_stop();
            return DFC_RDY_TO;
        }
    }

    if (default_cmds->pg_program.bits.CSEL == 1)
    {
        Retval = _WaitForDFCOperationComplete(DFC_SR_RDY1, 10);
        if (Retval != NoError)
        {
            xdfc_stop();
            return DFC_RDY_TO;
        }
    }


    /*
    *  If we used auto status read, the bad block status is in the status register.
    *  Use read status as auto status does not work as expected
    */
    if (default_cmds->pg_program.bits.AUTO_RS)
    {
       // NOTE: Use the data returned by the NAND in some way...
       buf[0] = *DFC_DATA;
       status = *DFC_DATA;
    }
    else
    {
       xdfc_readstatus( &buf[0], pNAND_Prop);
    }

    // check the status of the program operation
    if( buf[0] != 0xe0 && buf[0] != 0xc0 )
    {
        return ProgramError;
    }

    if ((default_cmds->pg_program.bits.CSEL == 0) && (*DFC_STATUS & DFC_SR_CS0_BBD))
         return DFCCS0BadBlockDetected;
    if ((default_cmds->pg_program.bits.CSEL == 1) && (*DFC_STATUS & DFC_SR_CS1_BBD))
         return DFCCS1BadBlockDetected;

    return NoError;
}

UINT_T xdfc_bch_pace( P_NAND_Properties_T pNAND_Prop )
{
   UINT_T Retval;

   //
   // Wait for page to be ready to read.
   Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
   if (Retval != NoError)
   {
       // catastropic error.
       // stop the dfc operation and clear the status bits before returning.
       // FIXME: do that after debug is complete. during debug it may be
       // interesting to examine these bits before they're cleared.
          xdfc_stop();
          return DFC_RDDREQ_TO;
   }
   DFC_REG_WRITE(DFC_STATUS, DFC_SR_RDDREQ);
   // Wait till status bit is cleared
   Retval = _WaitForDFCOperationPulldown(DFC_SR_RDDREQ, 50);
       
   if (Retval != NoError)
   {
       xdfc_stop();
       return ProgramError;
   }
   return NoError;
}



// SpareArea for the last page is returned only. However it could be 1st page and or last page of an erased block for SLC.
// MLC - Bad Block table no longer maintained in spare area. Must be created on first scan of device and saved.
UINT_T xdfc_read(P_DFC_BUFFER buffer, unsigned int address, unsigned int bytes, unsigned int* SpareArea, P_NAND_Properties_T pNAND_Prop)
{
    CMD_BLOCK   *default_cmds;
    NDCR_REG    control;
    NDCB0_REG   command;
    NDCB1_REG   addr1234;
    NDCB2_REG   addr5;
    unsigned int  page_first, page_last, page_mask;
    unsigned int  byte_count = 0, longwords;
    unsigned int  page_size, pre_bytes, post_bytes;
    unsigned int  i, status;
    UINT_T Retval, ReadStatus = NoError;

    // mlc/bch support:
   unsigned int  bch_en;
   unsigned int  bch_boundary = 0x0000001f;         // at bch boundary when none of these bits are set in the address.
   unsigned int  bch_rc;
   NDECCCTRL_REG dfc_ecc_ctrl_reg;

   // get the bch_en setting...
   DFC_REG_READ(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );
   bch_en = dfc_ecc_ctrl_reg.bits.BCH_EN;


    /*
    *  Some preliminary information.
    *    Determine if we need to use the default READ command or Naked READ command
    *    If page size is larger than 2KB (DFC FIFO size) then multiple 2KB reads must be issued
    *   using the naked read sematics.
    */
    default_cmds = pNAND_Prop->device_cmds;
    command.value = default_cmds->read.value;
    page_size  = pNAND_Prop->PageSize;
    page_mask  = ~(page_size - 1);
    page_first =  address              & page_mask;
    page_last  = (address + bytes - 1) & page_mask;
    pre_bytes  = address - page_first;
    post_bytes = (page_last + page_size) - (address + bytes);

    /*
    *  Set up the address for the command
    */
    getAddr(page_first, &addr1234, &addr5, pNAND_Prop);

   // stop any command in progress.
   // FIXME: this is temporary, until more robust and complete error handling is
   // implemented. each routine should ensure that the dfc is returned to a known
   // state before returning - even (or especially!) in the case of error.
   DFC_REG_READ(DFC_CONTROL, control.value);
   control.value &= ~DFC_CTRL_ND_RUN;
   DFC_REG_WRITE(DFC_CONTROL, control.value);


    // Clear read status bit and set DFC to expect a command.
    DFC_REG_READ(DFC_STATUS, status);
   	status |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
	DFC_REG_WRITE(DFC_STATUS, status);

	// Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);
       
    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }
   
    DFC_REG_READ(DFC_CONTROL, control.value);
    control.value |= DFC_CTRL_ND_RUN;
    DFC_REG_WRITE(DFC_CONTROL, control.value);

    //
    // Wait till DFC is ready for next command to be issued.
    Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
    if (Retval != NoError)
    {
        xdfc_stop();
        return DFC_WRCMD_TO;
    }

    *DFC_COMMAND0 = command.value;
    *DFC_COMMAND0 = addr1234.value;
    *DFC_COMMAND0 = addr5.value;

    //
    // Wait for page to be ready to read.
    Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
    if (Retval != NoError)
    {
        xdfc_stop();
        return DFC_RDDREQ_TO;
    }


    /*
    *  Read and discard the pre padding. (No need to check for a page boundary.)
    */
    byte_count = 0;
    while (pre_bytes)
    {
        i = *DFC_DATA;
        byte_count += 4;
        pre_bytes  -= 4;            // Possible infinite loop

        if( bch_en && ((byte_count & bch_boundary)==0)) if( (bch_rc=xdfc_bch_pace(pNAND_Prop)) ) return bch_rc;
    }

    /*
    *  Read into the buffer. (Check for page boundaries.)
    */
    longwords = 0;
    while (bytes)
    {
        buffer[longwords++] = *DFC_DATA;
        byte_count += 4;
        bytes      -= 4;

        /*
        *  Check for page boundary. Reissue the read command all over again.
        */
        if (bytes && ((byte_count & ~page_mask) == 0))
        {
            // check for bch pacing (every 32 bytes, wait for rddreq)
            if( bch_en && ((byte_count & bch_boundary)==0) ) if( (bch_rc=xdfc_bch_pace(pNAND_Prop)) ) return bch_rc;

            /*
            *  Read the status area padding. // TBD MLC uses BCH ECC so padding is different - MLC returns 8 (LB only)
            */
            for (i = (unsigned int)xdfc_getStatusPadding(pNAND_Prop); i > 0; i--)
                status = *DFC_DATA;

            // Check for uncorrectable bit error.
            if (*DFC_STATUS & DFC_SR_DBERR)
            {
                xdfc_stop();
                return DFCDoubleBitError;
            }

            // Check for ECC threshold (if we hit 14 or more bits)
            if ((*DFC_STATUS & DFC_SR_ECC_CNT) >= (14 << 16))
                ReadStatus = ReadDisturbError;

            // Increment page address.
            page_first += page_size;
            getAddr(page_first, &addr1234, &addr5, pNAND_Prop);

            /*
            *   SET UP THE COMMAND
            */
            // Clear read status bit and set DFC to expect a command.
        	DFC_REG_READ(DFC_STATUS, status);
		   	status |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
			DFC_REG_WRITE(DFC_STATUS, status);

			// Wait till status bit is cleared
            Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);
       
            if (Retval != NoError)
            {
                xdfc_stop();
                return ProgramError;
            }
            //*DFC_CONTROL |= DFC_CTRL_ND_RUN;
            DFC_REG_READ(DFC_CONTROL, control.value);
            control.value |= DFC_CTRL_ND_RUN;
            DFC_REG_WRITE(DFC_CONTROL, control.value);

            //
            // Wait till DFC is ready for next command to be issued.
            Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
            if (Retval != NoError)
            {
                xdfc_stop();
                return DFC_WRCMD_TO;
            }

            *DFC_COMMAND0 = command.value;
            *DFC_COMMAND0 = addr1234.value;
            *DFC_COMMAND0 = addr5.value;

            //
            // Wait for page to be ready to read.
            Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
            if (Retval != NoError)
            {
                xdfc_stop();
                return DFC_RDDREQ_TO;
            }
        }
        else if( bch_en && ((byte_count & bch_boundary)==0)) if( (bch_rc=xdfc_bch_pace(pNAND_Prop)) ) return bch_rc;


    }

    /*
    *  Read and discard the post padding.
    */
    while (post_bytes)
    {
        i = *DFC_DATA;
        post_bytes -= 4;
        byte_count += 4;
        if( bch_en && ((byte_count & bch_boundary)==0)) if( (bch_rc=xdfc_bch_pace(pNAND_Prop)) ) return bch_rc;
    }


    // Read the spare area.
    if (SpareArea != NULL)
    {
        for (i = xdfc_getStatusPadding(pNAND_Prop); i > 0; i--)
            SpareArea[i] = *DFC_DATA;
    }
    else
    {
        for (i = xdfc_getStatusPadding(pNAND_Prop); i > 0; i--)
            status = *DFC_DATA;
    }

    // Check for ECC threshold (if we hit 14 or more bits)
    if ((*DFC_STATUS & DFC_SR_ECC_CNT) >= (14 << 16))
            ReadStatus = ReadDisturbError;

    // the uncorrectable error status is more important than the
    // readdisturberror, so the return value should be uncorrectable error
    // in the case that they're both set.
    if (*DFC_STATUS & DFC_SR_DBERR)
        ReadStatus = DFCDoubleBitError;


    return ReadStatus;
}

UINT_T xdfc_read_LP(P_DFC_BUFFER buffer, unsigned int address, unsigned int bytes, unsigned int* SpareArea, P_NAND_Properties_T pNAND_Prop)
{
   CMD_BLOCK     *default_cmds;
   NDCR_REG      control;
   NDCB0_REG     command;
   NDCB1_REG     addr1234;
   NDCB2_REG     addr5;
   unsigned int  page_first, page_last, page_mask;
   unsigned int  byte_count = 0, longwords;
   unsigned int  page_size, pre_bytes, post_bytes;
   unsigned int  i, status;
   UINT_T        Retval, ReadStatus = NoError;
   unsigned int  bch_en;
   unsigned int  bch_boundary = 0x0000001f;         // at bch boundary when none of these bits are set in the address.
   unsigned int  bch_rc;
   NDECCCTRL_REG dfc_ecc_ctrl_reg;

   // get the bch_en setting...
   DFC_REG_READ(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );
   bch_en = dfc_ecc_ctrl_reg.bits.BCH_EN;

   page_size = pNAND_Prop->PageSize;
   page_mask  = ~(page_size - 1);
   page_first =  address              & page_mask;
   page_last  = (address + bytes - 1) & page_mask;
   pre_bytes  = address - page_first;
   post_bytes = (page_last + page_size) - (address + bytes);

   /*
   *  Set up the address for the command
   */
   getAddr(page_first, &addr1234, &addr5, pNAND_Prop);

   // stop any command in progress.
   // FIXME: this is temporary, until more robust and complete error handling is
   // implemented. each routine should ensure that the dfc is returned to a known
   // state before returning - even (or especially!) in the case of error.
   DFC_REG_READ(DFC_CONTROL, control.value);
   control.value &= ~DFC_CTRL_ND_RUN;
   DFC_REG_WRITE(DFC_CONTROL, control.value);


   // Clear read status bit and set DFC to expect a command.
   DFC_REG_READ(DFC_STATUS, status);
   status |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
   DFC_REG_WRITE(DFC_STATUS, status);

   // Wait till status bit is cleared
   Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);

   if (Retval != NoError)
   {
       xdfc_stop();
       return ProgramError;
   }
            
   DFC_REG_READ(DFC_CONTROL, control.value);
   control.value |= DFC_CTRL_ND_RUN;
   DFC_REG_WRITE(DFC_CONTROL, control.value);

   //
   // Wait till DFC is ready for next command to be issued.
   Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
   if (Retval != NoError)
   {
       xdfc_stop();
       return DFC_WRCMD_TO;
   }
   DFC_REG_WRITE(DFC_STATUS, DFC_SR_WRCMDREQ);
   // Wait till status bit is cleared
   Retval = _WaitForDFCOperationPulldown(DFC_SR_WRCMDREQ, 50);

   if (Retval != NoError)
   {
       xdfc_stop();
       return ProgramError;
   }

   default_cmds = pNAND_Prop->device_cmds;
   command.value = default_cmds->read.value;
   command.bits.CMD_XTYPE = DFC_CMDXT_COMMAND_DISPATCH;
   command.bits.NC = 1; // valid command following this one

   // first we send the dispatch command
   *DFC_COMMAND0 = command.value;    // send read command with command dispatch
   *DFC_COMMAND0 = addr1234.value;   // page address 1-4
   *DFC_COMMAND0 = addr5.value;      // page address 5

   // Wait til the command is done.
   Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);
   if (Retval != NoError)
   {
       xdfc_stop();
       return DFCCS0CommandDoneError;
   }
   DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_CMDD);
   // Wait till status bit is cleared
   Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_CMDD, 50);

   if (Retval != NoError)
   {
       xdfc_stop();
       return ProgramError;
   }

   // Wait til the data is available.
    Retval = _WaitForDFCOperationComplete(DFC_SR_RDY0, 10); // FIXME: should check which CS this is.
    if (Retval != NoError)
    {
        xdfc_stop();
        return DFCCS0CommandDoneError;
    }
    DFC_REG_WRITE(DFC_STATUS, DFC_SR_RDY0);     // clear CS0RDY, also. FIXME: should check which CS this is.
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(DFC_SR_RDY0, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }

   // Wait till DFC is ready for next command to be issued.
   Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
   if (Retval != NoError)
   {
       xdfc_stop();
       return DFC_WRCMD_TO;
   }
   DFC_REG_WRITE(DFC_STATUS, DFC_SR_WRCMDREQ);
   // Wait till status bit is cleared
   Retval = _WaitForDFCOperationPulldown(DFC_SR_WRCMDREQ, 50);

   if (Retval != NoError)
   {
       xdfc_stop();
       return ProgramError;
   }

   // now we send the naked read command
   command.value = default_cmds->read.value;
   command.bits.CMD_XTYPE = DFC_CMDXT_NAKED_READ;
   command.bits.NC = 1; // valid command following this one
   command.bits.CMD1 = 0xff;
   command.bits.CMD2 = 0xff;
   *DFC_COMMAND0 = command.value;    // send naked read command
   *DFC_COMMAND0 = addr1234.value;          // dummy write
   *DFC_COMMAND0 = addr5.value;             // dummy write

   //
   // Wait for page to be ready to read.
   Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
   if (Retval != NoError)
   {
       xdfc_stop();
       return DFC_RDDREQ_TO;
   }
   DFC_REG_WRITE(DFC_STATUS, DFC_SR_RDDREQ);
   // Wait till status bit is cleared
   Retval = _WaitForDFCOperationPulldown(DFC_SR_RDDREQ, 50);

   if (Retval != NoError)
   {
       xdfc_stop();
       return ProgramError;
   }

    // FIXME: this is the place to check for an error, not til after the data's been read in...

   /*
   *  Read and discard the pre padding. (No need to check for a page boundary.)
   */
   byte_count = 0;
   while (pre_bytes)
   {
          i = *DFC_DATA;
          byte_count += 4;
          pre_bytes  -= 4;

          if(byte_count > 0 && (byte_count & (TWO_KB_MASK)) == 0)
          {

                // check for bch pacing (every 32 bytes, wait for rddreq)
                if( bch_en && ((byte_count & bch_boundary)==0) ) if( (bch_rc=xdfc_bch_pace(pNAND_Prop)) ) return bch_rc;

               //  Read the status area padding.
                for (i = (unsigned int)xdfc_getSpareArea_LP(pNAND_Prop); i>0; i--)
                    status = *DFC_DATA;


               // PGDN & CMDD should assert here. clear of those ndsr bits.
               // FIXME: make these aware of which chip select is being used.
               Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);
               if (Retval != NoError)
               {
                   xdfc_stop();
                   return DFCCS0CommandDoneError;
               }
               DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_CMDD);
               // Wait till status bit is cleared
               Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_CMDD, 50);

               if (Retval != NoError)
               {
                   xdfc_stop();
                   return ProgramError;
               }

               Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_PAGED, 10);
               if (Retval != NoError)
               {
                   xdfc_stop();
                   return DFCCS0CommandDoneError;
               }
               DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_PAGED);
               // Wait till status bit is cleared
               Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_PAGED, 50);

               if (Retval != NoError)
               {
                   xdfc_stop();
                   return ProgramError;
               }


                // Check for double bit error.
                if (*DFC_STATUS & DFC_SR_DBERR)
                {
                    xdfc_stop();
                    return DFCDoubleBitError;
                }

               // Check for ECC threshold (if we hit 14 or more bits)
               if ((*DFC_STATUS & DFC_SR_ECC_CNT) >= (14 << 16))
                    ReadStatus = ReadDisturbError;

               // Wait till DFC is ready for next command to be issued.
               Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
               if (Retval != NoError)
               {
                   xdfc_stop();
                    return DFC_WRCMD_TO;
               }
               DFC_REG_WRITE(DFC_STATUS, DFC_SR_WRCMDREQ);
               // Wait till status bit is cleared
               Retval = _WaitForDFCOperationPulldown(DFC_SR_WRCMDREQ, 50);

               if (Retval != NoError)
               {
                   xdfc_stop();
                   return ProgramError;
               }

               // reissue the naked read command for the next 2KB
               command.value = default_cmds->read.value;
               command.bits.CMD_XTYPE = DFC_CMDXT_NAKED_READ;
               command.bits.NC = 1; // valid command following this one
               command.bits.CMD1 = 0xff;
               command.bits.CMD2 = 0xff;
               *DFC_COMMAND0 = command.value;    // send naked read command
               *DFC_COMMAND0 = addr1234.value;          // dummy
               *DFC_COMMAND0 = addr5.value;             // dummy

               // Wait till DFC is ready for next command to be issued.
               Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
               if (Retval != NoError)
               {
                   xdfc_stop();
                   return DFC_RDDREQ_TO;
               }
               DFC_REG_WRITE(DFC_STATUS, DFC_SR_RDDREQ);
               // Wait till status bit is cleared
               Retval = _WaitForDFCOperationPulldown(DFC_SR_RDDREQ, 50);

               if (Retval != NoError)
               {
                   xdfc_stop();
                   return ProgramError;
               }

               // FIXME: this is the place to check for an error, not til after the data's been read in...

          }
          // check for bch pacing (every 32 bytes, wait for rddreq)
          else if( bch_en && ((byte_count & bch_boundary)==0)) if( (bch_rc=xdfc_bch_pace(pNAND_Prop)) ) return bch_rc;


   }

   /*
   *  Read into the buffer. (Check for page boundaries.)
   */
   longwords = 0;
   while (bytes)
   {
          buffer[longwords++] = *DFC_DATA;
          byte_count += 4;
          bytes      -= 4;

          /*
          *  Check for page boundary. Reissue the read command all over again.
          */
          if (bytes && ((byte_count & ~page_mask) == 0))
          {

                // check for bch pacing (every 32 bytes, wait for rddreq)
                if( bch_en && ((byte_count & bch_boundary)==0)) if( (bch_rc=xdfc_bch_pace(pNAND_Prop)) ) return bch_rc;

                //  Read the status area padding.
                //  return the spare area contents even if it is between 2k buffers...
                for (i = (unsigned int)xdfc_getSpareArea_LP(pNAND_Prop); i>0; i--)
                {
                  if (SpareArea != NULL)
                         *SpareArea++ = *DFC_DATA;
                  else
                         status = *DFC_DATA;
                }

                // PGDN & CMDD will be asserted here.
                // FIXME: make these aware of which chip select is being used.
                Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);
                if (Retval != NoError)
                {
                   xdfc_stop();
                   return DFCCS0CommandDoneError;
                }
                DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_CMDD);
                // Wait till status bit is cleared
                Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_CMDD, 50);

                if (Retval != NoError)
                {
                    xdfc_stop();
                    return ProgramError;
                }

                Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_PAGED, 10);
                if (Retval != NoError)
                {
                   xdfc_stop();
                   return DFCCS0CommandDoneError;
                }
                DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_PAGED);
                // Wait till status bit is cleared
                Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_PAGED, 50);

                if (Retval != NoError)
                {
                    xdfc_stop();
                    return ProgramError;
                }

                // Check for ECC threshold (if we hit 14 or more bits)
                if ((*DFC_STATUS & DFC_SR_ECC_CNT) >= (14 << 16))
                    ReadStatus = ReadDisturbError;

                // Check for double bit error.
                if (*DFC_STATUS & DFC_SR_DBERR)
                {
                    xdfc_stop();
                    return DFCDoubleBitError;
                }

                // Increment page address.
                page_first += page_size;
                getAddr(page_first, &addr1234, &addr5, pNAND_Prop);

                 //
                 // Wait till DFC is ready for next command to be issued.
                 Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
                 if (Retval != NoError)
                 {
                     xdfc_stop();
                     return DFC_WRCMD_TO;
                 }
                 DFC_REG_WRITE(DFC_STATUS, DFC_SR_WRCMDREQ);
                 // Wait till status bit is cleared
                 Retval = _WaitForDFCOperationPulldown(DFC_SR_WRCMDREQ, 50);

                 if (Retval != NoError)
                 {
                     xdfc_stop();
                     return ProgramError;
                 }

                 default_cmds = pNAND_Prop->device_cmds;
                 command.value = default_cmds->read.value;
                 command.bits.CMD_XTYPE = DFC_CMDXT_COMMAND_DISPATCH;
                 command.bits.NC = 1; // valid command following this one

                 // first we send the dispatch command
                 *DFC_COMMAND0 = command.value;    // send read command with command dispatch
                 *DFC_COMMAND0 = addr1234.value;   // page address 1-4
                 *DFC_COMMAND0 = addr5.value;      // page address 5


                 // Wait til the command is done.
                 Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);
                 if (Retval != NoError)
                 {
                    xdfc_stop();
                    return DFCCS0CommandDoneError;
                 }
                 DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_CMDD);
                 // Wait till status bit is cleared
                 Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_CMDD, 50);

                 if (Retval != NoError)
                 {
                     xdfc_stop();
                     return ProgramError;
                 }

                 // Wait til the data is available.
                 Retval = _WaitForDFCOperationComplete(DFC_SR_RDY0, 10); // FIXME: should check which CS this is.
                 if (Retval != NoError)
                 {
                    xdfc_stop();
                    return DFCCS0CommandDoneError;
                 }
                 DFC_REG_WRITE(DFC_STATUS, DFC_SR_RDY0);        // clear CS0RDY, also. FIXME: should check which CS this is.
                 // Wait till status bit is cleared
                 Retval = _WaitForDFCOperationPulldown(DFC_SR_RDY0, 50);

                 if (Retval != NoError)
                 {
                     xdfc_stop();
                     return ProgramError;
                 }


                 // Wait till DFC is ready for next command to be issued.
                 Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
                 if (Retval != NoError)
                 {
                     xdfc_stop();
                     return DFC_WRCMD_TO;
                 }
                 DFC_REG_WRITE(DFC_STATUS, DFC_SR_WRCMDREQ);
                 // Wait till status bit is cleared
                 Retval = _WaitForDFCOperationPulldown(DFC_SR_WRCMDREQ, 50);

                 if (Retval != NoError)
                 {
                     xdfc_stop();
                     return ProgramError;
                 }

                 // now we send the naked read command
                 command.value = default_cmds->read.value;
                 command.bits.CMD_XTYPE = DFC_CMDXT_NAKED_READ;
                 command.bits.NC = 1; // valid command following this one
                 command.bits.CMD1 = 0xff;
                 command.bits.CMD2 = 0xff;
                 *DFC_COMMAND0 = command.value;    // send naked read command
                 *DFC_COMMAND0 = addr1234.value;          // dummy write
                 *DFC_COMMAND0 = addr5.value;             // dummy write


                 Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
                 if (Retval != NoError)
                 {
                     xdfc_stop();
                     return DFC_RDDREQ_TO;
                 }
                 DFC_REG_WRITE(DFC_STATUS, DFC_SR_RDDREQ);
                 // Wait till status bit is cleared
                 Retval = _WaitForDFCOperationPulldown(DFC_SR_RDDREQ, 50);

                 if (Retval != NoError)
                 {
                     xdfc_stop();
                     return ProgramError;
                 }

          }
          else if( (bytes||post_bytes) && ((byte_count & (TWO_KB_MASK)) == 0))  // need another naked_read to re-fill the internal fifo?
          {                                                                     // answer is 'yes' if there are more bytes (data or post) to be read
                // check for bch pacing (every 32 bytes, wait for rddreq)
                if( bch_en && ((byte_count & bch_boundary)==0)) if( (bch_rc=xdfc_bch_pace(pNAND_Prop)) ) return bch_rc;

                //  Read the status area padding.
                //  return the spare area contents even if it is between 2k buffers...
                for (i = (unsigned int)xdfc_getSpareArea_LP(pNAND_Prop); i>0; i--)
                {
                  if (SpareArea != NULL)
                         *SpareArea++ = *DFC_DATA;
                  else
                         status = *DFC_DATA;
                }
                // pgdn & cmdd assert here.
                // FIXME: make these aware of which chip select is being used.
                Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);
                if (Retval != NoError)
                {
                   xdfc_stop();
                   return DFCCS0CommandDoneError;
                }
                DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_CMDD);
                
                // Wait till status bit is cleared
                Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_CMDD, 50);

                if (Retval != NoError)
                {
                    xdfc_stop();
                    return ProgramError;
                }

                Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_PAGED, 10);
                if (Retval != NoError)
                {
                   xdfc_stop();
                   return DFCCS0CommandDoneError;
                }
                DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_PAGED);
                // Wait till status bit is cleared
                Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_PAGED, 50);

                if (Retval != NoError)
                {
                    xdfc_stop();
                    return ProgramError;
                }


                // Check for double bit error.
                if (*DFC_STATUS & DFC_SR_DBERR)
                {
                    xdfc_stop();
                    return DFCDoubleBitError;
                }

                // Check for ECC threshold (if we hit 14 or more bits)
                if ((*DFC_STATUS & DFC_SR_ECC_CNT) >= (14 << 16))
                    ReadStatus = ReadDisturbError;

                 // Wait till DFC is ready for next command to be issued.
                 Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
                 if (Retval != NoError)
                 {
                     xdfc_stop();
                     return DFC_WRCMD_TO;
                 }
                 DFC_REG_WRITE(DFC_STATUS, DFC_SR_WRCMDREQ);
                 // Wait till status bit is cleared
                 Retval = _WaitForDFCOperationPulldown(DFC_SR_WRCMDREQ, 50);

                 if (Retval != NoError)
                 {
                     xdfc_stop();
                     return ProgramError;
                 }

                // reissue the naked read command for the next 2KB
                // now we send the naked read command
                command.value = default_cmds->read.value;
                command.bits.CMD_XTYPE = DFC_CMDXT_NAKED_READ;
                command.bits.NC = (page_first >= page_last) ? 0 : 1; // valid command following this one
                command.bits.CMD1 = 0xff;
                command.bits.CMD2 = 0xff;
                *DFC_COMMAND0 = command.value;    // send naked read command
                *DFC_COMMAND0 = addr1234.value;   // dummy
                *DFC_COMMAND0 = addr5.value;      // dummy

                //
                // Wait till DFC is ready for next command to be issued.
                Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
                if (Retval != NoError)
                {
                    xdfc_stop();
                    return DFC_RDDREQ_TO;
                }
                DFC_REG_WRITE(DFC_STATUS, DFC_SR_RDDREQ);
                // Wait till status bit is cleared
                Retval = _WaitForDFCOperationPulldown(DFC_SR_RDDREQ, 50);

                if (Retval != NoError)
                {
                    xdfc_stop();
                    return ProgramError;
                }
          }
          else  if( bch_en && ((byte_count & bch_boundary)==0)) if( (bch_rc=xdfc_bch_pace(pNAND_Prop)) ) return bch_rc;


   }

   /*
   *  Read and discard the post padding.
   */
   while (post_bytes)
   {
          i = *DFC_DATA;
          post_bytes -= 4;
          byte_count += 4;

          if(((byte_count & (TWO_KB_MASK)) == 0) && (byte_count & (~page_mask)) != 0)
          {
                // check for bch pacing (every 32 bytes, wait for rddreq)
                if( bch_en && ((byte_count & bch_boundary)==0)) if( (bch_rc=xdfc_bch_pace(pNAND_Prop)) ) return bch_rc;

                //  Read the status area padding.
                for (i = (unsigned int)xdfc_getSpareArea_LP(pNAND_Prop); i>0; i--)
                              status = *DFC_DATA;
                // pgdn & cmdd assert here.
                // FIXME: make these aware of which chip select is being used.
                Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);
                if (Retval != NoError)
                {
                   xdfc_stop();
                   return DFCCS0CommandDoneError;
                }
                DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_CMDD);
                
                // Wait till status bit is cleared
                Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_CMDD, 50);

                if (Retval != NoError)
                {
                    xdfc_stop();
                    return ProgramError;
                }

                Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_PAGED, 10);
                if (Retval != NoError)
                {
                   xdfc_stop();
                   return DFCCS0CommandDoneError;
                }
                DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_PAGED);
                // Wait till status bit is cleared
                Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_PAGED, 50);

                if (Retval != NoError)
                {
                    xdfc_stop();
                    return ProgramError;
                }


                // Check for ECC threshold (if we hit 14 or more bits)
                if ((*DFC_STATUS & DFC_SR_ECC_CNT) >= (14 << 16))
                ReadStatus = ReadDisturbError;

                // Wait till DFC is ready for next command to be issued.
                Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
                if (Retval != NoError)
                {
                    xdfc_stop();
                    return DFC_WRCMD_TO;
                }
                // pgdn & cmdd assert here.
                DFC_REG_WRITE(DFC_STATUS, DFC_SR_WRCMDREQ); // PGDN. FIXME: check which CS. Also, use the constant instead of 400
                // Wait till status bit is cleared
                Retval = _WaitForDFCOperationPulldown(DFC_SR_WRCMDREQ, 50);

                if (Retval != NoError)
                {
                    xdfc_stop();
                    return ProgramError;
                }

                // reissue the naked read command for the next 2KB
                // now we send the naked read command
                command.value = default_cmds->read.value;
                command.bits.CMD_XTYPE = DFC_CMDXT_NAKED_READ;
                command.bits.NC = 0; // no valid command following this one
                command.bits.CMD1 = 0xff;
                command.bits.CMD2 = 0xff;
                *DFC_COMMAND0 = command.value;    // send naked read command
                *DFC_COMMAND0 = addr1234.value;          // dummy
                *DFC_COMMAND0 = addr5.value;             // dummy

                //
                // Wait till DFC is ready for next command to be issued.
                Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
                if (Retval != NoError)
                {
                    xdfc_stop();
                    return DFC_RDDREQ_TO;
                }
                DFC_REG_WRITE(DFC_STATUS, DFC_SR_RDDREQ);
                
                // Wait till status bit is cleared
                Retval = _WaitForDFCOperationPulldown(DFC_SR_RDDREQ, 50);

                if (Retval != NoError)
                {
                    xdfc_stop();
                    return ProgramError;
                }
          }
          // check for bch pacing (every 32 bytes, wait for rddreq)
          else if( bch_en && ((byte_count & bch_boundary)==0)) if( (bch_rc=xdfc_bch_pace(pNAND_Prop)) ) return bch_rc;
    }


    //  Read the status area padding.
    for (i = (unsigned int)xdfc_getSpareArea_LP(pNAND_Prop); i>0; i--)
    {
          if (SpareArea != NULL)
                 *SpareArea++ = *DFC_DATA;
          else
                 status = *DFC_DATA;
    }

    // pgdn & cmdd assert here.
    // FIXME: make these aware of which chip select is being used.
    Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);
    if (Retval != NoError)
    {
       xdfc_stop();
       return DFCCS0CommandDoneError;
    }
    DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_CMDD);
    
                
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_CMDD, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }

    Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_PAGED, 10);
    if (Retval != NoError)
    {
       xdfc_stop();
       return DFCCS0CommandDoneError;
    }
    DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_PAGED);
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_PAGED, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }



    // Check for ECC threshold (if we hit 14 or more bits)
    if ((*DFC_STATUS & DFC_SR_ECC_CNT) >= (14 << 16))
        ReadStatus = ReadDisturbError;

    // the uncorrectable error status is more important than the
    // readdisturberror, so the return value should be uncorrectable error
    // in the case that they're both set.
    if (*DFC_STATUS & DFC_SR_DBERR)
        ReadStatus = DFCDoubleBitError;

 return ReadStatus;
}

//int xdfc_read(P_DFC_BUFFER buffer, unsigned int address, unsigned int bytes, unsigned int* SpareArea, P_NAND_Properties_T pNAND_Prop)
UINT_T xdfc_read_ONFI_ParameterPages(P_DFC_BUFFER buffer, P_NAND_Properties_T pNAND_Prop)
{
    CMD_BLOCK   *default_cmds;
    NDSR_REG     status;
    NDCR_REG     control;
    NDCB1_REG    addr1234;
    NDCB2_REG    addr5;
    NDCB3_REG    ndlencnt;
    UINT_T data, temp, i;
    int DfcPad = 0;
    UINT_T DfcBufferReadCount = 0;
    int ParamPageFound = FALSE;
    UINT_T Retval, ReturnStatus, SignatureStatus;

    addr1234.value = 0;
    addr5.value = 0;
    ndlencnt.value = 2048;
    /*
    *  SET UP THE COMMAND
    */
    default_cmds = pNAND_Prop->device_cmds;
    //getAddr(page_first, &addr1234, &addr5, pNAND_Prop);

    // stop any command in progress.
    // FIXME: this is temporary, until more robust and complete error handling is
    // implemented. each routine should ensure that the dfc is returned to a known
    // state before returning - even (or especially!) in the case of error.
    DFC_REG_READ(DFC_CONTROL, control.value);
    control.value &= ~DFC_CTRL_ND_RUN;
    DFC_REG_WRITE(DFC_CONTROL, control.value);

    // Clear any/all status bits (write 1 to clear)
    //*DFC_STATUS  = *DFC_STATUS;
    DFC_REG_READ(DFC_STATUS, status.value);
	status.value |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
	DFC_REG_WRITE(DFC_STATUS, status.value);
    
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }

    // Set DFC to expect a command.
    //*DFC_CONTROL |= DFC_CTRL_ND_RUN;
    DFC_REG_READ(DFC_CONTROL, control.value);
    control.value |= DFC_CTRL_ND_RUN;
    DFC_REG_WRITE(DFC_CONTROL, control.value);

    // Wait till DFC is ready for next command to be issued.
    Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
    if (Retval != NoError)
    {
        xdfc_stop();
        return DFC_WRCMD_TO;
    }

    // Set up for Parameter Page Read Command
    //*DFC_COMMAND0 = (default_cmds->read_onfi_parameter_pages.value); //NDCB0
    DFC_REG_WRITE(DFC_COMMAND0, default_cmds->read_onfi_parameter_pages.value); //NDCB0
    //*DFC_COMMAND0 = addr1234.value;    // NDCB1
    DFC_REG_WRITE(DFC_COMMAND0, addr1234.value); //NDCB1
    //*DFC_COMMAND0 = addr5.value;       // NDCB2
    DFC_REG_WRITE(DFC_COMMAND0, addr5.value); //NDCB2
    //*DFC_COMMAND0 = ndlencnt.value;    // NDCB3
    DFC_REG_WRITE(DFC_COMMAND0, ndlencnt.value); //NDCB3

    // Wait for page to be ready to read.
    Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
    if (Retval != NoError)
    {
        xdfc_stop();
        return DFC_RDDREQ_TO;
    }

    // Now read the ONFI parameter page data from the DFC data buffer
    //Notes:::  The ONFI parameter page will be 256 bytes. However, we have to read either 256 bytes
    //      or 512 bytes (we won't know which) from the DFC to get the 256 bytes of data due to only
    //      partial ONFI support from the DFC. ONFI reads are supposed to be byte oriented regardless
    //      of the flash bus width. If the bus width is 8 bit, a READ from the DFC data register
    //      return 4 bytes of data. If the bus width is 16 bits, a READ will return only one byte of
    //      data for each 16 bit word (upper byte being zero, or two bytes of data per 32 bit READ.
    //
    //      The Flash device contains multiple copies of the parameter page, up to 8 or more, we don't
    //      know how many.
    //      Therefore, this function will read a total of 2048 Bytes (size of DFC buffer), of the ONFI
    //      parameter page data (which contain some number of copies depending on the DFC to flash bus
    //      width. Once we strip the padded bytes out (if there are any) we search in 256 byte increments,
    //      for a valid ONFI signature in the first four bytes of a potential parameter page.  Once it
    //      we find a valid ID ("O", "N", "F", "I", with two or more characters matching),the next 252 bytes
    //      will be read from the DFC into a buffer and a CRC check will be done. If the CRC check is OK
    //      we'll read the remainder of the bytes up to the 2048 total into a bitbucket, allowing the
    //      DFC read operation to complete. Otherwise, if there are 256 or more bytes remaining,
    //      we'll try to read the next parameter page (next 256 bytes), repeating the above steps.

    while (DfcBufferReadCount < ndlencnt.value)
        {
        //read 4 bytes of data from DFC data buffer
        DFC_REG_READ(DFC_DATA, data);

        if((ParamPageFound == FALSE) && ((DfcBufferReadCount & 0xFF) == 0))
            {
                // If we haven't found a parameter page AND we're on the 256 boundary,
                // check for an ONFI signature.

                // Check for a DFC bug that pads data on a 16 bit bus (DFC to flash)
                if ((data & 0xFF00FF00)== 0)
                {   //DFC is padding data
                    DfcPad = 1;
                    DFC_REG_READ(DFC_DATA, temp);
                    data = xdfc_stripPad(temp, data);
                    // There are now 4 bytes in "data" that could be an ONFI signature
                }
                //check to see if this is a valid ONFI page
                SignatureStatus = xdfc_validateOnfiSignature(data);

                //copy the signature into the buffer
                buffer[0] = data;

                // We need to read the rest of the ONFI page into the buffer (count in longwords)
                // This is done whether the signature is valid or not.
                for(i=1; i < (K_ONFI_PARAMETER_PAGE_SIZE/4); i++)
                {
                    DFC_REG_READ(DFC_DATA, data);
                    if(DfcPad)
                    {
                        DFC_REG_READ(DFC_DATA, temp);
                        data = xdfc_stripPad(temp, data);
                    }
                    buffer[i] = data;
                }//EndFor to read in Paramter Page (valid or not)

                if(SignatureStatus == 0)
                {
                    // If the signature was valid, check the CRC
                    ReturnStatus = xdfc_calculateOnfiCrc((UINT8_T *)buffer, K_ONFI_PARAMETER_PAGE_SIZE);
                    if (ReturnStatus == 0)
                    {
                         ParamPageFound = TRUE;
                    }
                }
                //increment how many bytes we read
                if (DfcPad)
                {
                    DfcBufferReadCount += (K_ONFI_PARAMETER_PAGE_SIZE)*2;
                }
                else
                {
                    DfcBufferReadCount += (K_ONFI_PARAMETER_PAGE_SIZE);
                }
                continue;
          }//EndIf Parameter Page Processing

        // update read count
        // Note: If a Parameter Page was processed we skip this increment
        DfcBufferReadCount += 4;
        }//EndWhile

    // all the data has been read:
    // Clear any/all status bits (write 1 to clear)
    //*DFC_STATUS  = *DFC_STATUS;
    DFC_REG_READ(DFC_STATUS, status.value);
    status.value |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
    DFC_REG_WRITE(DFC_STATUS, status.value);
    
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }

    return (ParamPageFound) ? NoError : NotFoundError;
}//End Routine xdfc_read_ONFI_ParameterPages

UINT_T xdfc_stripPad(unsigned int upper, unsigned int lower)
{   //example:
    //upper = 0x00120034    lower = 0x00560078
    //value = 0x12345678

    unsigned int value = 0;
    value = (upper << 8) & 0xFF000000;
    value |= (upper << 16) & 0x00FF0000;
    value |= (lower >> 8) & 0x0000FF00;
    value |= lower & 0x000000FF;

    return value;
}

UINT_T xdfc_readID(P_DFC_BUFFER buffer, DEVICE_TYPE dev_type)
{
    NDCB0_REG       cmd0;
    NDCB1_REG       cmd1;
    NDCB2_REG       cmd2;
    NDCR_REG        control;
    NDSR_REG        status;

    unsigned int    longwords = 0;
    UINT_T          Retval;

    NDECCCTRL_REG   dfc_ecc_ctrl_reg;
    NDECCCTRL_REG   dfc_ecc_ctrl_reg_save;

    // stop any command in progress.
    // FIXME: this is temporary, until more robust and complete error handling is
    // implemented. each routine should ensure that the dfc is returned to a known
    // state before returning - even (or especially!) in the case of error.
    DFC_REG_READ(DFC_CONTROL, control.value);
    control.value &= ~DFC_CTRL_ND_RUN;
    DFC_REG_WRITE(DFC_CONTROL, control.value);

    // make sure bch is off during readid
    DFC_REG_READ(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );
    dfc_ecc_ctrl_reg_save = dfc_ecc_ctrl_reg;
    dfc_ecc_ctrl_reg.bits.BCH_EN = 0;
    DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );


    /*
    *  Clear any previous Read Status
    *
    */
    //*DFC_STATUS = *DFC_STATUS;  // clear only *non* reserved bits
    DFC_REG_READ(DFC_STATUS, status.value);
	status.value |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
	DFC_REG_WRITE(DFC_STATUS, status.value);
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }


    /*
     *  Kick off the command
     *      Note that you should be able to fill the command buffer
     *           and then start the command but this does not seem
     *           to work with the DFC controller as documented in
     *           the IAS.  So we kick off the command and wait for
     *           the DFC to request the next command like in DMA mode.
     */

    //control.value = *DFC_CONTROL;
    DFC_REG_READ(DFC_CONTROL, control.value);

    if(dev_type == LARGE)
        control.bits.RD_ID_CNT = 4;
    else
        control.bits.RD_ID_CNT = 2;

    control.bits.ND_RUN = 1;
    DFC_REG_WRITE(DFC_CONTROL, control.value);

    cmd0.value = 0; //take care of reserved bits, etc.
    cmd0.bits.CMD1 = 0x90;
    cmd0.bits.ADDR_CYC = 1;
    cmd0.bits.CMD_TYPE = DFC_CMDTYPE_READID;

    cmd1.value = 0x0;
    cmd2.value = 0x0;


   /*
    *  Wait for write command request bit.
    */
    Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
    if (Retval != NoError)
    {
        DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg_save.value );
        xdfc_stop();
        return DFC_WRCMD_TO;
    }
    DFC_REG_WRITE(DFC_STATUS, DFC_SR_WRCMDREQ);
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(DFC_SR_WRCMDREQ, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }


   /*
    *  Write commands to command buffer 0.
    *
    */
   DFC_REG_WRITE(DFC_COMMAND0, cmd0.value);
   DFC_REG_WRITE(DFC_COMMAND0, cmd1.value);
   DFC_REG_WRITE(DFC_COMMAND0, cmd2.value);

   /*
    *  Wait for data in the buffer and check command done in the control register.
    */
    Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
    if (Retval != NoError)
    {
        DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg_save.value );
        xdfc_stop();
        return DFC_RDDREQ_TO;
    }
    DFC_REG_WRITE(DFC_STATUS, DFC_SR_RDDREQ);
    
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(DFC_SR_RDDREQ, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }

   /*
    *  Read the read ID bytes. + read 4 bogus bytes
    */
    DFC_REG_READ(DFC_DATA, buffer[0]);      // this read returns the data of interest
    DFC_REG_READ(DFC_DATA, buffer[1]);      // this read required by spec (sections x.4.1.2, x.3.4.3, x.4.6)

   /*
    *  Wait for command to complete (it's probably already finished). FIXME: make this aware of which chip select is being used.
    */
    Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);
    if (Retval != NoError)
    {
        DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg_save.value );
        xdfc_stop();
        return DFCCS0CommandDoneError;
    }
    DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_CMDD);
    
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_CMDD, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }

    // check for a run bit stuck on. this is an error - but try to handle it gracefully.
    DFC_REG_READ(DFC_CONTROL, control.value);
    if (control.bits.ND_RUN == 1)
    {
        //DFC_REG_READ(DFC_CONTROL, control.value);
        //control.value &= ~DFC_CTRL_ND_RUN;
        //DFC_REG_WRITE(DFC_CONTROL, control.value);
        xdfc_stop();
    }

   DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg_save.value );
   return NoError;
}

UINT_T xdfc_read_ONFI_ID(P_DFC_BUFFER buffer)
{
    NDCB0_REG cmd0;
    NDCB1_REG cmd1;
    NDCB2_REG cmd2;
    NDCR_REG control;
    NDSR_REG status;
    //unsigned int  i, byte_count, longwords = 0;
    unsigned int longwords = 0;
    UINT_T Retval;

    NDECCCTRL_REG   dfc_ecc_ctrl_reg;
    NDECCCTRL_REG   dfc_ecc_ctrl_reg_save;

    // stop any command in progress.
    // FIXME: this is temporary, until more robust and complete error handling is
    // implemented. each routine should ensure that the dfc is returned to a known
    // state before returning - even (or especially!) in the case of error.
    DFC_REG_READ(DFC_CONTROL, control.value);
    control.value &= ~DFC_CTRL_ND_RUN;
    DFC_REG_WRITE(DFC_CONTROL, control.value);

    // make sure bch is off during read_ONFI_ID
    DFC_REG_READ(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );
    dfc_ecc_ctrl_reg_save = dfc_ecc_ctrl_reg;
    dfc_ecc_ctrl_reg.bits.BCH_EN = 0;
    DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );


    /*
    *  Clear any previous Read Status
    *
    */
    //*DFC_STATUS = *DFC_STATUS;  // clear only *non* reserved bits
    DFC_REG_READ(DFC_STATUS, status.value);
	status.value |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
	DFC_REG_WRITE(DFC_STATUS, status.value);
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }
    /*
     *  Kick off the command
     *      Note that you should be able to fill the command buffer
     *           and then start the command but this does not seem
     *           to work with the DFC controller as documented in
     *           the IAS.  So we kick off the command and wait for
     *           the DFC to request the next command like in DMA mode.
     */

    //control.value = *DFC_CONTROL;
    DFC_REG_READ(DFC_CONTROL, control.value);

    //if(dev_type == LARGE)
        control.bits.RD_ID_CNT = 4;
    //else
    //  control.bits.RD_ID_CNT = 2;
    control.bits.ND_RUN = 1;
    //*DFC_CONTROL = control.value;
    DFC_REG_WRITE(DFC_CONTROL, control.value)

    cmd0.value = 0; //take care of reserved bits, etc.
    cmd0.bits.CMD1 = 0x90;
    cmd0.bits.ADDR_CYC = 1;
    cmd0.bits.CMD_TYPE = DFC_CMDTYPE_READID;

    cmd1.value = 0x20;
    cmd2.value = 0x0;


   /*
    *  Wait for write command request bit.
    */
    Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
    if (Retval != NoError)
    {
        DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg_save.value );
        xdfc_stop();
        return DFC_WRCMD_TO;
    }


   /*
    *  Write commands to command buffer 0.
    *
    */
    //*DFC_COMMAND0 = cmd0.value;
    DFC_REG_WRITE(DFC_COMMAND0, cmd0.value);
    //*DFC_COMMAND0 = cmd1.value;
    DFC_REG_WRITE(DFC_COMMAND0, cmd1.value);
    //*DFC_COMMAND0 = cmd2.value;
    DFC_REG_WRITE(DFC_COMMAND0, cmd2.value);

   /*
    *  Wait for data in the buffer and check command done in the control register.
    */
    Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
    if (Retval != NoError)
    {
        DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg_save.value );
        xdfc_stop();
        return DFC_RDDREQ_TO;
    }

   /*
    *  Read the ONFI ID bytes. + read 4 bogus bytes
    */
    //buffer[0] = *DFC_DATA;
    DFC_REG_READ(DFC_DATA, buffer[0]);
    //buffer[1] = *DFC_DATA;
    DFC_REG_READ(DFC_DATA, buffer[1]);


    /*
     *  Clear the Status and make sure the command completed.
     */
   //*DFC_STATUS  = *DFC_STATUS;
    DFC_REG_READ(DFC_STATUS, status.value);
	status.value |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
	DFC_REG_WRITE(DFC_STATUS, status.value);
   // Wait till status bit is cleared
   Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);

   if (Retval != NoError)
   {
       xdfc_stop();
       return ProgramError;
   }

   //control.value = *DFC_CONTROL;
   DFC_REG_READ(DFC_CONTROL, control.value);

   if (control.bits.ND_RUN == 1)
   {
        //*DFC_CONTROL &= ~DFC_CTRL_ND_RUN;
        //DFC_REG_READ(DFC_CONTROL, control.value);
        //control.value &= ~DFC_CTRL_ND_RUN;
        //DFC_REG_WRITE(DFC_CONTROL, control.value);
       xdfc_stop();
   }

   DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg_save.value );
   return NoError;
}

UINT_T    xdfc_unlock(UINT_T start_addr, UINT_T end_addr, UINT_T invert, P_NAND_Properties_T pNAND_Prop)
{
    CMD_BLOCK   *default_cmds;
    NDCB0_REG   cmd0;
    NDCB1_REG   addr1234;
    NDCB2_REG   addr5;
    unsigned int buffer[2], Retval;
   /*
   *  Setting ND_RUN bit will get the transfer going.
    *
   */
    #if DFC_WORKAROUND  // Clock OFF
    *ADDR_CCU_D0CKEN_A &= ADDR_DOCKEN_A_NAND_MASK;
    *ADDR_CCU_D0CKEN_A;
    #endif

   *DFC_STATUS  = 0xFFF;
   *DFC_CONTROL |= DFC_CTRL_ND_RUN;

    #if DFC_WORKAROUND  // Clock ON
    *ADDR_CCU_D0CKEN_A |= 0x10;
    *ADDR_CCU_D0CKEN_A;
    #endif

   /*
   *  Setup Commands.
    *
   */

    cmd0.value = 0; //take care of reserved bits, etc.
    cmd0.bits.CMD1 = 0x23;
    cmd0.bits.CMD_TYPE = DFC_CMDTYPE_READID;
    cmd0.bits.ADDR_CYC  = 3;
    cmd0.bits.NC = 1;
    cmd0.bits.DBC = 1;

    addr1234.value = 0;
    addr5.value = 0;
    getEraseAddr(start_addr, &addr1234, &addr5, pNAND_Prop);

    /*
   *  Wait for write command request bit.
    */

    Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
    CheckErrorReturn(Retval);
   /*
   *  Write commands to command buffer 0.
    *
   */
    #if DFC_WORKAROUND  // Clock OFF
    *ADDR_CCU_D0CKEN_A &= ADDR_DOCKEN_A_NAND_MASK;
    *ADDR_CCU_D0CKEN_A;
    #endif

   *DFC_COMMAND0 = cmd0.value;
   *DFC_COMMAND0 = addr1234.value;
   *DFC_COMMAND0 = addr5.value;

    #if DFC_WORKAROUND  // Clock ON
    *ADDR_CCU_D0CKEN_A |= 0x10;
    *ADDR_CCU_D0CKEN_A;
    #endif

    Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
    CheckErrorReturn(Retval);
    buffer[0] = *DFC_DATA;
    buffer[1] = *DFC_DATA;

    //update for ending command
    Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
    CheckErrorReturn(Retval);

    cmd0.value = 0; //take care of reserved bits, etc.
    cmd0.bits.CMD1 = 0x24;
    cmd0.bits.CMD_TYPE = DFC_CMDTYPE_READID;
    cmd0.bits.ADDR_CYC  = 3;
    cmd0.bits.DBC = 1;

    addr1234.value = 0;
    addr5.value = 0;
    getEraseAddr(end_addr, &addr1234, &addr5, pNAND_Prop);

    /*
   *  Write commands to command buffer 0.
    *
   */
    #if DFC_WORKAROUND  // Clock OFF
    *ADDR_CCU_D0CKEN_A &= ADDR_DOCKEN_A_NAND_MASK;
    *ADDR_CCU_D0CKEN_A;
    #endif

   *DFC_COMMAND0 = cmd0.value;
   *DFC_COMMAND0 = addr1234.value;
   *DFC_COMMAND0 = addr5.value;

    #if DFC_WORKAROUND  // Clock ON
    *ADDR_CCU_D0CKEN_A |= 0x10;
    *ADDR_CCU_D0CKEN_A;
    #endif

    Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
    CheckErrorReturn(Retval);

    buffer[0] = *DFC_DATA;
    buffer[1] = *DFC_DATA;


   return 0;
}

UINT_T xdfc_reset(P_NAND_Properties_T pNAND_Prop)
{
    NDCR_REG control;
    //NDTR1CS0_REG  dfc_timing1_reg;
    NDCB0_REG cmd0;
    NDSR_REG status;
    UINT_T Retval;

    // stop any command in progress.
    // FIXME: this is temporary, until more robust and complete error handling is
    // implemented. each routine should ensure that the dfc is returned to a known
    // state before returning - even (or especially!) in the case of error.
    DFC_REG_READ(DFC_CONTROL, control.value);
    control.value &= ~DFC_CTRL_ND_RUN;
    DFC_REG_WRITE(DFC_CONTROL, control.value);

    /*
    *  Clear any previous Read Status
    */
    //*DFC_STATUS = *DFC_STATUS;  // clear only *non* reserved bits/*
    DFC_REG_READ(DFC_STATUS, status.value);
	status.value |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
	DFC_REG_WRITE(DFC_STATUS, status.value);
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }

    /*
    *  Setting ND_RUN bit will get the transfer going.
    */
    DFC_REG_READ(DFC_CONTROL, control.value);
    control.value |= DFC_CTRL_ND_RUN;
    DFC_REG_WRITE(DFC_CONTROL, control.value);

    // Wait for WRCMDREQ
    Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
    if (Retval != NoError)
    {
        xdfc_stop();
        return DFC_WRREQ_TO;
    }

   /*
   *  Setup Commands.
    *
   */
    cmd0.value = 0; //take care of reserved bits, etc.
    cmd0.bits.CMD1 = 0xFF;
    cmd0.bits.CMD_TYPE = DFC_CMDTYPE_RESET;

    /*
    *  Write commands to command buffer 0.
    *
    */
    /* Send Command */
    //*DFC_COMMAND0 = cmd0.value;
    DFC_REG_WRITE(DFC_COMMAND0, cmd0.value);
    //*DFC_COMMAND0 = 0;
    DFC_REG_WRITE(DFC_COMMAND0, 0);
    //*DFC_COMMAND0 = 0;
    DFC_REG_WRITE(DFC_COMMAND0, 0);

    // Wait for CMDD
    Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);
    if (Retval != NoError)
    {
        xdfc_stop();
        return DFCCS0CommandDoneError;
    }

    /*
    *  Make sure RDY is set at this point
    */
    Retval = _WaitForDFCOperationComplete(DFC_SR_RDY0, 10);
    if(Retval != NoError)
    {
        xdfc_stop();
        return DFC_RDY_TO;
    }
     /* Clear RDY */
    //*DFC_STATUS  = DFC_SR_RDY0 | DFC_SR_RDY1 | DFC_SR_CS0_CMDD;
    DFC_REG_WRITE(DFC_STATUS,DFC_SR_RDY0 | DFC_SR_RDY1 | DFC_SR_CS0_CMDD );
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(DFC_SR_RDY0 | DFC_SR_RDY1 | DFC_SR_CS0_CMDD, 50);
    
    if(Retval != NoError)
    {
        xdfc_stop();
        return DFC_RDY_TO;
    }
    return Retval;
}

UINT_T xdfc_readstatus(P_DFC_BUFFER buffer, P_NAND_Properties_T pNAND_Prop)
{
    CMD_BLOCK       *default_cmds;
    NDCB1_REG       cmd1;
    NDCB2_REG       cmd2;
    NDCR_REG        control;
    NDCR_REG        status;
    UINT_T          Retval;

    NDECCCTRL_REG   dfc_ecc_ctrl_reg;
    NDECCCTRL_REG   dfc_ecc_ctrl_reg_save;

    // stop any command in progress.
    // FIXME: this is temporary, until more robust and complete error handling is
    // implemented. each routine should ensure that the dfc is returned to a known
    // state before returning - even (or especially!) in the case of error.
    DFC_REG_READ(DFC_CONTROL, control.value);
    control.value &= ~DFC_CTRL_ND_RUN;
    DFC_REG_WRITE(DFC_CONTROL, control.value);

    // make sure bch is off during readstatus
    DFC_REG_READ(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );
    dfc_ecc_ctrl_reg_save = dfc_ecc_ctrl_reg;
    dfc_ecc_ctrl_reg.bits.BCH_EN = 0;
    DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );


    /*
    *  Clear any previous Read Status
    */
    DFC_REG_READ(DFC_STATUS, status.value);
	status.value |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
	DFC_REG_WRITE(DFC_STATUS, status.value);
    
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }

    /*
     *  Setting ND_RUN bit will get the transfer going.
    */
    DFC_REG_READ(DFC_CONTROL, control.value);
    control.value |= DFC_CTRL_ND_RUN;
    DFC_REG_WRITE(DFC_CONTROL, control.value);


   /*
   *  Setup Commands.
    *
   */
    default_cmds = pNAND_Prop->device_cmds;
    cmd1.value = 0;
    cmd2.value = 0;
   /*
   *  Wait for write command request bit.
    */
    Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
    if (Retval != NoError)
    {
        DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg_save.value );
        xdfc_stop();
        return DFC_WRCMD_TO;
    }
    DFC_REG_WRITE(DFC_STATUS, DFC_SR_WRCMDREQ);
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(DFC_SR_WRCMDREQ, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }

   /*
   *  Write commands to command buffer 0.
   */
   DFC_REG_WRITE(DFC_COMMAND0, default_cmds->read_status.value);
   DFC_REG_WRITE(DFC_COMMAND0, cmd1.value);
   DFC_REG_WRITE(DFC_COMMAND0, cmd2.value);

   /*
   *  Wait for read data to appear
   */
    Retval = _WaitForDFCOperationComplete(DFC_SR_RDDREQ, 10);
    if (Retval != NoError)
    {
        DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg_save.value );
        xdfc_stop();
        return DFC_RDDREQ_TO;
    }
    DFC_REG_WRITE(DFC_STATUS, DFC_SR_RDDREQ);
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(DFC_SR_RDDREQ, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }


   DFC_REG_READ(DFC_DATA, buffer[0]);       // this read returns the status data
   DFC_REG_READ(DFC_DATA, buffer[1]);       // this read required by spec (sections x.4.1.2, x.3.4.3, x.4.6)

   /*
   *  Wait for CMDD. FIXME: make this aware of which chip select is really being used.
   */
    Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);
    if (Retval != NoError)
    {
        DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg_save.value );
        xdfc_stop();
        return DFCCS0CommandDoneError;
    }
    DFC_REG_WRITE(DFC_STATUS, DFC_SR_CS0_CMDD);
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(DFC_SR_CS0_CMDD, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }


   // restore the bch_en state.
   DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg_save.value );
   return NoError;
}

/*
*  NOTE: Introduce a generic erase. One that lets you erase any random slice of memory.
 *       Do this by reading from the segments of the block(s) that are to be preserved
*        before issuing the erase command. (Of couse, with about 32 pages per block,
 *       this would be an expensive operation...)
*/

//
// NOTE: To delete a page, is the address the start of the page, or can it be
//       any address within the page? [02aug2004agapito]
//

UINT_T xdfc_erase(unsigned int address, P_NAND_Properties_T pNAND_Prop)
{
   CMD_BLOCK    *default_cmds;
   NDCR_REG     control;
   NDSR_REG     status;
   NDCB1_REG    addr1234;
   NDCB2_REG    addr5;
   unsigned int discarded_data;
   unsigned int buf[2];
   //unsigned int tmp;
   UINT_T Retval;

   // stop any command in progress.
   // FIXME: this is temporary, until more robust and complete error handling is
   // implemented. each routine should ensure that the dfc is returned to a known
   // state before returning - even (or especially!) in the case of error.
   DFC_REG_READ(DFC_CONTROL, control.value);
   control.value &= ~DFC_CTRL_ND_RUN;
   DFC_REG_WRITE(DFC_CONTROL, control.value);


   /*
    *  Setting ND_RUN bit will get the transfer going.
   */
    //
    //This is the original: *DFC_STATUS  |= 0xFFF;      //clear only *non* reserved bits
    //This is the usual:    *DFC_STATUS = *DFC_STATUS;  // clear only *non* reserved bits/*
    DFC_REG_READ(DFC_STATUS, status.value);
	status.value |= NFU_DSR_ERR_CNT_MSK;		// We need to write 0x1f to the ERR_CNT field in order to clear it completely. 
	DFC_REG_WRITE(DFC_STATUS, status.value);
    // Wait till status bit is cleared
    Retval = _WaitForDFCOperationPulldown(0xFFFFFFFF, 50);

    if (Retval != NoError)
    {
        xdfc_stop();
        return ProgramError;
    }

    //*DFC_CONTROL |= DFC_CTRL_ND_RUN;
    DFC_REG_READ(DFC_CONTROL, control.value);
    control.value |= DFC_CTRL_ND_RUN;
    DFC_REG_WRITE(DFC_CONTROL, control.value);

    /*
     *  Generate command
    *
    */
    default_cmds = pNAND_Prop->device_cmds;
    getEraseAddr(address, &addr1234, &addr5, pNAND_Prop);

#if 0
    // bc: seems like auto_rs is not working, so force it off for now.
    // If read status command is not 0x70, do not use auto read status method.
    if( default_cmds->read_status.bits.CMD1 == 0x70)
        default_cmds->pg_program.bits.AUTO_RS = 1;
#else
    // bc: seems like auto_rs is not working, so force it off for now.
    default_cmds->pg_program.bits.AUTO_RS = 0;
#endif

   /*
    *  Wait for write command request bit.
   */

   Retval = _WaitForDFCOperationComplete(DFC_SR_WRCMDREQ, 10);
   if (Retval != NoError)
   {
       xdfc_stop();
       return DFC_WRCMD_TO;
   }

   /*
    *  Write commands to command buffer 0.
    *
   */
   DFC_REG_WRITE(DFC_COMMAND0, default_cmds->blk_erase.value);

   // Write out the address cycles
   DFC_REG_WRITE(DFC_COMMAND0, addr1234.value);
   DFC_REG_WRITE(DFC_COMMAND0, addr5.value);


   /*
   *  Erasing the block... Wait for ready to check status.
   *
   */
   // Check Device ready.
   Retval = _WaitForDFCOperationComplete(DFC_SR_RDY0, 10);
   if (Retval != NoError)
   {
        xdfc_stop();
        return DFC_RDY_TO;
   }
   if (default_cmds->pg_program.bits.CSEL == 0)
    Retval = _WaitForDFCOperationComplete(DFC_SR_CS0_CMDD, 10);
   if (Retval != NoError)
   {
       xdfc_stop();
       return DFCCS0CommandDoneError;
   }
   if (default_cmds->pg_program.bits.CSEL == 1)
    Retval = _WaitForDFCOperationComplete(DFC_SR_CS1_CMDD, 10);
   if (Retval != NoError)
   {
       xdfc_stop();
        return DFCCS1CommandDoneError;
   }

  /*
   *  If we used auto status read get the status else issue read status
   *    Then check the bad block status is in the status register.
    *
   */
    if (default_cmds->pg_program.bits.AUTO_RS)
    {
      DFC_REG_READ(DFC_DATA, buf[0]);
      DFC_REG_READ(DFC_DATA, discarded_data);
    }
    else
    {
        xdfc_readstatus( &buf[0], pNAND_Prop);
    }

    // examine the result of the erase operation
    if( buf[0] != 0xe0 && buf[0] != 0xc0 )
    {
        // the flash device reported that the erase failed.
        return EraseError;
    }

    if ((default_cmds->pg_program.bits.CSEL == 0) && (*DFC_STATUS & DFC_SR_CS0_BBD))
         return DFCCS0BadBlockDetected;
    if ((default_cmds->pg_program.bits.CSEL == 1) && (*DFC_STATUS & DFC_SR_CS1_BBD))
         return DFCCS1BadBlockDetected;


   /*
   *  Check for double bit error.
    *
   */
   DFC_REG_READ(DFC_STATUS, status.value);
   if (status.value & DFC_SR_DBERR)
   {
      return DFCDoubleBitError;
   }
   return NoError;
}


void xdfc_setTiming(FLASH_TIMING *flash)
{
   NDTR0CS0_REG timing0;
   NDTR1CS0_REG timing1;

   //
   // NOTE: Confirm these calculations make sense! [25jun2004agapito]
   // METHOD:
   //    - integer division will truncate the result, so add a 1 in all cases
   //    - subtract the extra 1 cycle added to all register timing values
   //                                          1000 / NAND_CONTROLLER_CLOCK
   timing0.bits.tCH = MIN(((int) (flash->tCH * NAND_CONTROLLER_CLOCK / 1000) + 1), TIMING_MAX_tCH);
   timing0.bits.tCS = MIN(((int) (flash->tCS * NAND_CONTROLLER_CLOCK / 1000) + 1), TIMING_MAX_tCS);
   timing0.bits.tWH = MIN(((int) (flash->tWH * NAND_CONTROLLER_CLOCK / 1000) + 1), TIMING_MAX_tWH);
   timing0.bits.tWP = MIN(((int) (flash->tWP * NAND_CONTROLLER_CLOCK / 1000) + 1), TIMING_MAX_tWP);
   timing0.bits.tRH = MIN(((int) (flash->tRH * NAND_CONTROLLER_CLOCK / 1000) + 1), TIMING_MAX_tRH);
   timing0.bits.tRP = MIN(((int) (flash->tRP * NAND_CONTROLLER_CLOCK / 1000) + 1), TIMING_MAX_tRP);

   timing1.bits.tR   = MIN(((int) (flash->tR   * NAND_CONTROLLER_CLOCK / 1000) + 1), TIMING_MAX_tR);
   timing1.bits.tWHR = MIN(((int) (flash->tWHR * NAND_CONTROLLER_CLOCK / 1000) + 1), TIMING_MAX_tWHR);
   timing1.bits.tAR  = MIN(((int) (flash->tAR  * NAND_CONTROLLER_CLOCK / 1000) + 1), TIMING_MAX_tAR);

   //
   // Write timing registers to corresponding memory mapped registers.
   //
   //*DFC_TIMING_0 = timing0.value;
   DFC_REG_WRITE(DFC_TIMING_0, timing0.value);
   //*DFC_TIMING_1 = timing1.value;
   DFC_REG_WRITE(DFC_TIMING_1, timing1.value);

   return;
}

void getAddr(unsigned int address, NDCB1_REG * addr1234, NDCB2_REG * addr5, P_NAND_Properties_T pNAND_Prop)
{
   unsigned int column_address;
   unsigned int row_address;

   addr1234->value = 0;
   addr5->value    = 0;



   switch (pNAND_Prop->NAND_type)
   {
      case SMALL:
        addr1234->bits.ADDR1  = address & 0x00000FF;  // A0-A7
        addr1234->bits.ADDR2  = (address & 0x001FE00) >> 9; // A9-A16
        addr1234->bits.ADDR3  = (address & 0x1FE0000) >> 17; // A17-A24
        addr1234->bits.ADDR4  = (address & 0x6000000) >> 25; // A25-A26
        addr5->bits.ADDR5     = 0;
        break;

      case LARGE:
      default:
        // NOTE: The DFC should handle incrementing the column address
              //            we just need to worry about the row address to get
              //            to the correct page.  Row address starts in addr cycle 3.
        column_address = address & (pNAND_Prop->PageSize - 1);
        // if this is a x16 device, then convert the input "byte" address into
        // a "word" address appropriate for indexing a word-oriented device.
        if( pNAND_Prop->FlashBusWidth == FlashBusWidth16 ) column_address /= 2; // word to byte conversion

        row_address    = address / pNAND_Prop->PageSize;

        addr1234->bits.ADDR1  = (column_address & 0x000000FF);      // A0-A7   (column address)
        addr1234->bits.ADDR2  = (column_address & 0x0000FF00) >> 8; // A8-A12  (column address)
        addr1234->bits.ADDR3  = (row_address & 0x000000FF);         // A13-A20 (row address)
        addr1234->bits.ADDR4  = (row_address & 0x0000FF00) >> 8;    // A21-A28 (row address)
        addr5->bits.ADDR5     = (row_address & 0x00FF0000) >> 16;   // A29-A31 (row address)

        break;
      }
   return;
}

// getEraseAddress:
//   Convert a byte address into the format used during a Block Erase command.
//
//   The Block Erase command gets the block and page info in three address cycles.
//
//   This routine basically removes the column bits from the address and
//   puts the result into the three bytes used during the address cycle.

void getEraseAddr(unsigned int address, NDCB1_REG * addr1234, NDCB2_REG * addr5, P_NAND_Properties_T pNAND_Prop)
{
   unsigned int row_address;

   addr1234->value = 0;
   addr5->value    = 0;

   switch (pNAND_Prop->NAND_type)
   {
      case SMALL:
         addr1234->bits.ADDR1  = (address & 0x001FE00) >> 9;    // A9-A16
         addr1234->bits.ADDR2  = (address & 0x1FE0000) >> 17;   // A17-A24
         addr1234->bits.ADDR3  = (address & 0x2000000) >> 25;   // A25-A26
         addr1234->bits.ADDR4  = 0;
         addr5->bits.ADDR5     = 0;
         break;

      default:
        row_address = address / pNAND_Prop->PageSize;
        addr1234->bits.ADDR1  = (row_address & 0x000000FF);         // A13-A20 (row address)
        addr1234->bits.ADDR2  = (row_address & 0x0000FF00) >> 8;    // A21-A28 (row address)
        addr1234->bits.ADDR3  = (row_address & 0x00FF0000) >> 16;   // A29-A31 (row address)
        addr1234->bits.ADDR4  = 0;
        addr5->bits.ADDR5     = 0;
       break;
   }
   return;
}

#if 0
// Spare area, returns number of words to read
// bc: use getSpareArea because getSpareArea
//     knows about page size and it checks ecc & bch settings.
UINT_T xdfc_getStatusPadding(P_NAND_Properties_T pNAND_Prop)
{
   if (pNAND_Prop->PageSize > 512)
        return 10;
   else
        return  2;
}
#else
UINT_T xdfc_getStatusPadding(P_NAND_Properties_T pNAND_Prop)
{
    UINT_T  spareareasize;

    spareareasize=xdfc_getSpareArea_LP(pNAND_Prop);
    return spareareasize;
}

#endif


// Spare area, returns number of words to read
UINT_T xdfc_getSpareArea_LP(P_NAND_Properties_T pNAND_Prop)
{
    NDCR_REG dfc_control_reg;
    NDCB0_REG dfc_ndcb0_reg;
    NDECCCTRL_REG dfc_ecc_reg;
    UINT_T count = 0;

    DFC_REG_READ(DFC_CONTROL, dfc_control_reg.value);
    DFC_REG_READ(DFC_COMMAND0, dfc_ndcb0_reg.value);
    DFC_REG_READ(DFC_ECCCTRL, dfc_ecc_reg.value);


    if ((dfc_control_reg.bits.SPARE_EN) && !(dfc_ndcb0_reg.bits.LEN_OVRD))
    {
        if ((dfc_control_reg.bits.PAGE_SZ == 0) && (!dfc_control_reg.bits.ECC_EN))
            count = 4;
        else if ((dfc_control_reg.bits.PAGE_SZ == 0) && (dfc_control_reg.bits.ECC_EN))
            count = 2;
        else if ((dfc_control_reg.bits.PAGE_SZ == 1) && (!dfc_control_reg.bits.ECC_EN))
            count = 16;
        else if ((dfc_control_reg.bits.PAGE_SZ == 1) && (dfc_control_reg.bits.ECC_EN) && (!dfc_ecc_reg.bits.BCH_EN))
            count = 10;
        else if ((dfc_control_reg.bits.PAGE_SZ == 1) && (dfc_control_reg.bits.ECC_EN) && (dfc_ecc_reg.bits.BCH_EN))
            count = 8;
    }

    return count;
}


// Spare area, returns number of words to read, for normal reads only.
// this routine will ignore the setting of length override.
// it can be safely used if it is known that the following reads
// will be for normal page sizes, and not for something like parameter pages.
UINT_T xdfc_getSpareArea_LPnormal(P_NAND_Properties_T pNAND_Prop)
{
    NDCR_REG dfc_control_reg;
    NDECCCTRL_REG dfc_ecc_reg;
    UINT_T count = 0;

    DFC_REG_READ(DFC_CONTROL, dfc_control_reg.value);
    DFC_REG_READ(DFC_ECCCTRL, dfc_ecc_reg.value);


    if(dfc_control_reg.bits.SPARE_EN)
    {
        if ((dfc_control_reg.bits.PAGE_SZ == 0) && (!dfc_control_reg.bits.ECC_EN))
            count = 4;                                      // 512 byte page, no ecc. 16 byte sp
        else if ((dfc_control_reg.bits.PAGE_SZ == 0) && (dfc_control_reg.bits.ECC_EN))
            count = 2;                                      // 512 byte page, hamming ecc. 8 byte sp, 6 byte hamming, 2 byte unav.
        else if ((dfc_control_reg.bits.PAGE_SZ == 1) && (!dfc_control_reg.bits.ECC_EN))
            count = 16;                                     // 2048 byte page, no ecc. 64 byte sp.
        else if ((dfc_control_reg.bits.PAGE_SZ == 1) && (dfc_control_reg.bits.ECC_EN) && (!dfc_ecc_reg.bits.BCH_EN))
            count = 10;                                     // 2048 byte page, hamming ecc. 40 byte sp, 4*6 byte hamming
        else if ((dfc_control_reg.bits.PAGE_SZ == 1) && (dfc_control_reg.bits.ECC_EN) && (dfc_ecc_reg.bits.BCH_EN))
            count = 8;                                      // 2048 byte page, bch ecc. 32 byte sp, 30 byte bch, 2 byte unav.
    }

    return count;
}


// ECC area, returns number of words to read
UINT_T xdfc_getECCArea_LP(P_NAND_Properties_T pNAND_Prop)
{
    NDCR_REG dfc_control_reg;
    NDECCCTRL_REG dfc_ecc_reg;
    int count = 0;

    DFC_REG_READ(DFC_CONTROL, dfc_control_reg.value);
    DFC_REG_READ(DFC_ECCCTRL, dfc_ecc_reg.value);


    if (dfc_control_reg.bits.ECC_EN)
    {
        if (dfc_control_reg.bits.PAGE_SZ == 0)
            count = 2;
        else if ((dfc_control_reg.bits.PAGE_SZ == 1) && (!dfc_ecc_reg.bits.BCH_EN))
            count = 6;
        else if ((dfc_control_reg.bits.PAGE_SZ == 1) && (dfc_ecc_reg.bits.BCH_EN))
            count = 8;
    }

    return count;
}


UINT_T xdfc_validateOnfiSignature(unsigned int signature)
{
    // two or more characters must be valid per ONFI spec
    int count = 0;
    if (((signature >> 0) & 0xFF) == 0x4F) {count++;}  // "O"
    if (((signature >> 8) & 0xFF) == 0x4E) {count++;}  // "N"
    if (((signature >> 16) & 0xFF) == 0x46) {count++;} // "F"
    if (((signature >> 24) & 0xFF) == 0x49) {count++;} // "I"
    if (count >= 2)
        {return NoError;}
    else
        {return NotFoundError;}
}

UINT_T xdfc_save_ONFI_Parameters(P_DFC_BUFFER OnfiParameterPageBuffer,
                         NAND_Properties_T * pNAND_Prop)
{
    UINT_T pages_per_block;
    UINT_T features_supported;
    NDCR_REG     dfc_control_reg;

    // Flash Bus Width
    // For now,  do not overide probe setting
    features_supported = xdfc_getInt((UINT8_T *)&OnfiParameterPageBuffer[0],
                                       K_ONFI_FEATURES_SUPPORTED_OFFSET, 2); //Bytes 6-7

    // PageSize
    pNAND_Prop->PageSize = xdfc_getInt((UINT8_T *)&OnfiParameterPageBuffer[0],
                                       K_ONFI_PAGE_SIZE_OFFSET, 4); //Bytes 80-83

    // SpareArea Size
    pNAND_Prop->SpareAreaSize = xdfc_getInt((UINT8_T *)&OnfiParameterPageBuffer[0],
                                       K_ONFI_SPARE_BYTES_PER_PAGE_OFFSET, 2); //Bytes 84-85
    // BlockSize
    pages_per_block = xdfc_getInt((UINT8_T *)&OnfiParameterPageBuffer[0],
                                       K_ONFI_PAGES_PER_BLOCK_OFFSET, 4);//Bytes 92-95
    pNAND_Prop->PagesPerBlock = pages_per_block;
    pNAND_Prop->BlockSize = pages_per_block * pNAND_Prop->PageSize;

    //NumOfBlocks
    pNAND_Prop->NumOfBlocks = xdfc_getInt((UINT8_T *)&OnfiParameterPageBuffer[0],
                                       K_ONFI_BLOCKS_PER_LUN, 4);//Bytes 96-99

    // Manufacturer Code
    pNAND_Prop->ManufacturerCode = xdfc_getInt((UINT8_T *)&OnfiParameterPageBuffer[0],
                                       K_ONFI_MANUFACTURER_ID_OFFSET, 1); //Byte 64
    //Nand Type - ONFI hardcoded to LARGE
    pNAND_Prop->NAND_type = LARGE;

    //
    // Now configure the DFC control register
    //

    DFC_REG_READ(DFC_CONTROL, dfc_control_reg.value);
    // Set Page Size
    switch (pNAND_Prop->PageSize)
    {
         case(512):
         {
            dfc_control_reg.bits.PAGE_SZ = 0;
            break;
         }
         case(2048):
         case(4096):
         case(8192):
         {
            dfc_control_reg.bits.PAGE_SZ = 1;
            break;
         }
         default:
         {
             return(DFCONFIConfigError);
             break;
         }
    }

    //Set Pages Per Block
    switch (pNAND_Prop->PagesPerBlock)
        {
        case(32):
            {
            dfc_control_reg.bits.PG_PER_BLK = 0;
            break;
            }
        case(64):
            {
            dfc_control_reg.bits.PG_PER_BLK = 2;
            break;
            }
        case(128):
            {
            dfc_control_reg.bits.PG_PER_BLK = 1;
            break;
            }
        case(256):
            {
            dfc_control_reg.bits.PG_PER_BLK = 3;
            break;
            }
        default:
            {
            return (NotSupportedError);
            break;
            }
        }
    // RowAddress Start Position(Same as LARGE)
    dfc_control_reg.bits.RA_START = 1;

    // DWIDTH_C and DWIDTH_M [bits 27:26] set from probe

    DFC_REG_WRITE(DFC_CONTROL,dfc_control_reg.value);

    return NoError;
}

UINT_T xdfc_getInt(unsigned char* BaseAddress, unsigned int ByteOffset, unsigned int NumBytes)
{
    unsigned int temp = 0;
    unsigned int i;

    for (i=0; i<NumBytes; i++)
        {
        temp =  temp | (BaseAddress[ByteOffset+i] << (i*8));
        }
    return temp;
}

// The CRC calculation covers all of the data between byte 0 and
// byte 253 of the parameter page inclusive.
// The CRC is calculated on word (16-bit) quantities starting with
// bytes 1:0 in the parameter page. The bits in the 16-bit quantity
// are processed from the most significant bit(bit15) to the least
// significant bit (bit 0). Even bytes of the parameter page
// (i.e. 0, 2, 4, etc) shall be used in the lower byte of the CRC
// calculation (bits 7:0).  Odd bytes of the parameter page (i.e. 1,
// 3, 5, etc) shall be used in the upper byte of the CRC calculation
// (bits 15:8).
// The CRC shall be generated using the following polynomial
//    G(x) = X16 + X15 + X2 + X1
// The polynomial in hex may be represented as 8005h.
// The CRC value shall be initialized with a value of 4F4Eh before the
// calulation begins. There is no XOR applied to the final CRC value
// after it is calculated. There is not reversal of the data bytes
// or the CRC calculated value.

UINT_T xdfc_calculateOnfiCrc(UINT8_T *Address, UINT_T length)
{
    UINT16_T Key = 0x8005;  //Polynomial Key
    UINT16_T Crc = 0x4F4E;  //Initial CRC Value
    UINT_T i; //i is the index into the parameter page data (even bytes)
    UINT_T t; //t keeps track of bit position in the Data value
    UINT16_T Data_rev, Crc_cpy;
    UINT16_T Data = (Address[0] << 8) | Address[1];
    UINT_T Retval = DFCONFIConfigError;

    i = 0; // Byte index into the parameter page
    t = 15; // bit position

    // Process the first 254 bytes and calculate CRC
    while(i < (length-2))
    {
        if(((( Crc >> 15)& 0x1) ^ ((Data >> t) & 0x1))==1)
            {//if either (but not both) MSB bit is set
            Crc = (Crc << 1) ^ Key;
            }
        else
            {
            Crc = Crc << 1;
            }
        if(t == 0)
            {
            t = 15;
            i += 2;
            Data = (Address[i] << 8) | Address[i+1];
            }
        else t--;
    }
    Data_rev = (Address[i+1] << 8) | Address[i];
    Crc_cpy = Crc;
    // Process CRC in three ways
    // Check with data as passed in and see if CRC goes to zero
    // Reverse data and check that CRC goes to Zero
    // compare calculated CRC against Data CRC
    while(i < length)
    {
        if(((( Crc >> 15)& 0x1) ^ ((Data >> t) & 0x1))==1)
            {//if either (but not both) MSB bit is set
            Crc = (Crc << 1) ^ Key;
            }
        else
            {
            Crc = Crc << 1;
            }
        if(((( Crc_cpy >> 15)& 0x1) ^ ((Data_rev >> t) & 0x1))==1)
            {//if either (but not both) MSB bit is set
            Crc_cpy = (Crc_cpy << 1) ^ Key;
            }
        else
            {
            Crc_cpy = Crc_cpy << 1;
            }
        if(t == 0)
            {
            t = 15;
            i += 2;
            Data = (Address[i] << 8) | Address[i+1];
            Data_rev = (Address[i+1] << 8) | Address[i];
            }
        else t--;
    }
    if ( Crc == 0 ) Retval = NoError;
    if ( Crc_cpy == 0 ) Retval = NoError;
    if ( Crc == Data_rev ) Retval = NoError;
    return Retval;
}//End Routine xdfc_validateOnfiCrc

//----------------------------------------------------------------------------
// _WaitForDFCOperationPulldown
// Polls the status register to check if a particular status bit got cleared  
//
//
//----------------------------------------------------------------------------
UINT_T _WaitForDFCOperationPulldown( unsigned int statusMask, UINT32 TimeOutMicrosec )
{
  UINT32 startTime, endTime;
  NDSR_REG     status;

  DFC_REG_READ(DFC_STATUS, status.value);
  if( (statusMask & status.value) == 0 )
        return NoError;
        
  startTime = GetOSCR0(); //Dummy read to flush potentially bad data
  startTime = GetOSCR0();

  do
  {
    DFC_REG_READ(DFC_STATUS, status.value);
    if( (statusMask & status.value) == 0 )
        return NoError;
    endTime = GetOSCR0();
    if (endTime < startTime)
        endTime += (0x0 - startTime);

  }
  while( OSCR0IntervalInMicro(startTime, endTime) < TimeOutMicrosec );
  
  return TimeOutError;
}

//----------------------------------------------------------------------------
// WaitForDFCOperationComplete
// Polls the status register to check if a particular status bit got
// set after writing a command.
//
//----------------------------------------------------------------------------

UINT_T _WaitForDFCOperationComplete( unsigned int statusMask, UINT32 TimeOutMillisec )
{
  UINT32 startTime, endTime;
  NDSR_REG     status;
  
  DFC_REG_READ(DFC_STATUS, status.value);
  if(statusMask & status.value)
         return NoError;

  startTime = GetOSCR0(); //Dummy read to flush potentially bad data
  startTime = GetOSCR0();
  do
  {
    DFC_REG_READ(DFC_STATUS, status.value);
    if(statusMask & status.value)
         return NoError;
    endTime = GetOSCR0();
    if (endTime < startTime)
        endTime += (0x0 - startTime);
  }
  while( OSCR0IntervalInMilli(startTime, endTime) < TimeOutMillisec );

  return TimeOutError;
}


void xdfc_enable_ecc( unsigned int bEnable )
{
    NDCR_REG dfc_control_reg;
    NDECCCTRL_REG   dfc_ecc_ctrl_reg;
    P_NAND_Properties_T pNandP = GetNANDProperties();

    DFC_REG_READ(DFC_CONTROL, dfc_control_reg.value );
    DFC_REG_READ(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );
    
    if ( pNandP->ECCMode == ECC_HAMMING ) // slc
    {
        dfc_control_reg.bits.ECC_EN = ((bEnable == 0) ? 0 : 1);
        DFC_REG_WRITE(DFC_CONTROL, dfc_control_reg.value );
        dfc_ecc_ctrl_reg.bits.BCH_EN = 0;
        DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );
    }
    if( pNandP->ECCMode == ECC_BCH ) // slc
    {
        dfc_control_reg.bits.ECC_EN = ((bEnable == 0) ? 0 : 1);
        DFC_REG_WRITE(DFC_CONTROL, dfc_control_reg.value );
        dfc_ecc_ctrl_reg.bits.BCH_EN = ((bEnable == 0) ? 0 : 1);
        DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );
    }
}

void xdfc_set_ecc_mode(ECC_MODE eccMode)
{
    NDCR_REG dfc_control_reg;
    NDECCCTRL_REG   dfc_ecc_ctrl_reg;
	P_NAND_Properties_T pNandP = GetNANDProperties();

	DFC_REG_READ(DFC_CONTROL, dfc_control_reg.value );
	if(dfc_control_reg.bits.ECC_EN)
	{
	    DFC_REG_READ(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );
	    dfc_ecc_ctrl_reg.bits.BCH_EN = ((eccMode == ECC_HAMMING) ? 0 : 1);
        DFC_REG_WRITE(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );
	    pNandP->ECCMode = eccMode;
	}
}

//----------------------------------------------------------
// xdfc_get_bootrom_ecc
// Sets Nand ECC Properties to that left over by BootRom
//----------------------------------------------------------
#if !BOOTROM
void xdfc_get_bootrom_ecc()
{
    NDCR_REG dfc_control_reg;
    NDECCCTRL_REG   dfc_ecc_ctrl_reg;
    P_NAND_Properties_T pNandP = GetNANDProperties();

    DFC_REG_READ(DFC_CONTROL, dfc_control_reg.value );
    DFC_REG_READ(DFC_ECCCTRL, dfc_ecc_ctrl_reg.value );
    
    if(dfc_control_reg.bits.ECC_EN)
        pNandP->ECCMode = dfc_ecc_ctrl_reg.bits.BCH_EN ? ECC_BCH : ECC_HAMMING;
    else
        pNandP->ECCMode = ECC_DISABLE;
        
}
#endif
