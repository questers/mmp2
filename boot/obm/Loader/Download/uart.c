/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by  
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:	Uart.c
**
**  PURPOSE: 	Contain useful uart functions
**                  
******************************************************************************/


#include "uart.h"

//extern UINT_T EnablePeripheralIRQInterrupt(PLATFORM_INTC_T InterruptID);
extern void EnableIrqInterrupts();
extern ProtocolISR GProtocolISR;

void ShutdownFFUART()
{
	*FFMCR = 0;
	*FFIER = 0;
}

void ShutdownALTUART()
{
	*FFMCR = 0;
	*FFIER = 0;
}

// FFUART configuration
void InitializeFFUART(int BaudRate)
{
	int DivisorVal;
	unsigned int Regval = 0;

	// Unmask FFUART interrupts
	EnablePeripheralIRQInterrupt(FFUART_INT);
	
  	// Configure the UART registers to Reset Values.
	// Ignore TBR, IIR, FOR and ACR.
	*FFLCR = 0;
	*FFIER = 0;

	//Reset Tx/Rx FIFOs
	*FFFCR = (UartFcrResetRf | UartFcrResetTf);
	*FFSPR = 0;
	Regval = *FFRBR;	// Read RBR to empty.
	Regval = *FFLSR;	// Read LSR to clear status bits.
	Regval = *FFMSR;	// Read MSR to clear status bits.

	// DLAB = 1, 1 Stop bit, 8-bit character
    *FFLCR  = (UartLcrWls | UartLcrDlab);		
    	
	// Setup divisor
   DivisorVal = UARTCLK /(BaudRate * 16);

	// Setup Divisor Latch Register
#if JTAG_PROTOCOL_DEBUG
	// Deniz: Temporarily use these values for debugging until we find out why..
	*FFDLL  = 0xa; //DivisorVal & 0xff;
	*FFDLH  = 0x0; //(DivisorVal & 0xff00) >> 8;
#else
	*FFDLL  = DivisorVal & 0xff;
	*FFDLH  = (DivisorVal & 0xff00) >> 8;
#endif
	
	// Set DLAB in LCR = 0 to allow access to THR and RBR
	*FFLCR &= 0x7f;
	*FFMCR  |= UartMcrEi;	// Enable FFUART interrupts

	// Enable and clear FIFOs and set INT trigger Level to be 8 bytes
	*FFFCR  = (UartFcrFe | UartFcrTIL | UartFcrITL_8);    

	//	enable FFUART and individual interrupts
	*FFIER  = (UartIerRavie | UartIerRlse | UartIerUue);		

}



