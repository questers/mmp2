/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into
 *  products for purposes authorized by the license agreement provided they
 *  include this notice and the associated copyright notice with any such
 *  product.
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#include "HSI.h"
#include "HSI_A0.h"
#include "Errors.h"
#include "timer.h"
#include "platform_interrupts.h"
#include "PlatformConfig.h"

/* Main HSI Properties structure */
HSI_Properties_T HSIProperties;


//Predefined MIPI handshake command list
// 24 bits DATA, 4 bits CHANNEL, 4 bits COMMAND
MIPI_Command_T HSI_Handshake_Commands[] =
{
    {MIPI_ECHO_DATA_PATTERN, 	HSI_COMMAND_CHANNEL, 	ECHO},
    {HSI_CONFIG << 16,			HSI_COMMAND_CHANNEL, 	INFO_REQ},
    {0x10001, 					HSI_COMMAND_CHANNEL, 	CONFIGURE}, // 1 Data Tx Channel, 0 Data Rx Channel, STREAM MODE
    {MIPI_ECHO_DATA_PATTERN,	HSI_COMMAND_CHANNEL, 	ECHO},
    {0x10400, 					HSI_DATA_CHANNEL, 		ALLOCATE_CH}, // A-device Channel:MPU Data, B-device Channel: DSP Data, Channel 1 
	{NULL,						NULL,					NULL}		//NULL terminate the array
};


/****** Generic routines *******/

/*
 Get pointer to HSI Properties
 */
P_HSI_Properties_T GetHSIProperties()
{
    return &HSIProperties;
}

/****** Service mode routines *******/

/*  HSI Init
 This routine configures the HSI operation:
  - sets the pin mux
  - enables HSI interrupts at core (but *not* HSI fifo interupts - that is done later)
  - sets HSI clock speeds and modes
  - enables the HSI unit
 */
void HSI_Init()
{
	P_HSI_Properties_T pHSIProps = GetHSIProperties();
	
	//configure the pins
	ChipSelectHSI( );

	//initialize the properties structure
	Set_HSI_Connection_Status(HSI_UNINITIALIZED);
	pHSIProps->handshake_index = 0;
	pHSIProps->buffer_address = 0;
	pHSIProps->size_left = 0;
	pHSIProps->pdu_size_left = 0;
	pHSIProps->pHSIRegisters = (P_HSI_REGISTERS_T)HSI_BASE;

	//Map HSI interrupt to core (bit position 55) and enable interrupts at the core.
	EnablePeripheralIRQInterrupt(INT_MIPI_HSI);
	*(unsigned int *)0xD4282184 = 0x1;

	//Configure the HSI clock divider ratios through HSI configuration register 2.
	pHSIProps->pHSIRegisters->HSI_CONFIG_R2 = HSI_CONFIG2_TX_CLK_DIV_3; //Set RX divider = 0; TX divider = 3;

	//Set the HSI transfer mode to "synchronized mode" through HSI Config 1.
	//Set number of Channel Bits per frame to 1.
	//Set Data Transfer Mode to Stream Transmission Mode.
	pHSIProps->pHSIRegisters->HSI_CONFIG_R1 = (HSI_CONFIG1_NUMB_CHAN_BITS);

	//make sure all the interrupts are off
	pHSIProps->pHSIRegisters->CHNL_INT_MASK[HSI_COMMAND_CHANNEL] = 0;
	pHSIProps->pHSIRegisters->CHNL_INT_MASK[HSI_DATA_CHANNEL] = 0;
	pHSIProps->pHSIRegisters->HSI_INT_MASK[0] = 0;
	
}

/* HSI Reset Fifos
 This routine resets the COMMAND channel RX and TX fifos and the DATA channel TX fifo 
 to default states.  This reset will clear any entries in the fifos.

 Interrupts will be unmasked for: COMMAND channel RX fifo
 Interrupts will *NOT* be unmasked for: DATA channel TX fifo
 */
void HSI_Reset_Fifos()
{
	P_HSI_Properties_T pHSIProps = GetHSIProperties();
	HSI_XX_FIFO_CONFIG_T temp_reg;

	//disable any interrupts that are on
	pHSIProps->pHSIRegisters->CHNL_INT_MASK[HSI_COMMAND_CHANNEL] = 0;
	pHSIProps->pHSIRegisters->CHNL_INT_MASK[HSI_DATA_CHANNEL] = 0;
	pHSIProps->pHSIRegisters->HSI_INT_MASK[0] = 0;

	//Configure RX FIFO register 0.
	// .	FIFO Base = 0.
	// .	FIFO Size = 32. This means 64 frames = 256 bytes.
	// .	FIFO Threshold  = 1. Trigger an interrupt after the first frame which should be an "ECHO" command.
	// .	Set Req_En to enable FIFO interrupts.
	// .	Enable the channel through the FIFO_EN bit.
	temp_reg.value = 0;
	temp_reg.FIFO_SIZE = 32;
	temp_reg.FIFO_THRS = 1;
	temp_reg.REQ_EN = 1;
	pHSIProps->pHSIRegisters->RX_FIFO_CONFIG[HSI_COMMAND_CHANNEL].value = temp_reg.value;
	pHSIProps->pHSIRegisters->RX_FIFO_CONFIG[HSI_COMMAND_CHANNEL].value |= 0x1;

	//Configure TX FIFO register 0.
	// .	FIFO Base = 0.
	// .	FIFO Size = 32. This means 64 frames = 256 bytes.
	// .	Set Req_En to enable FIFO interrupts.
	// .	Enable the channel through the FIFO_EN bit.
	temp_reg.value = 0;
	temp_reg.FIFO_SIZE = 32;
	temp_reg.REQ_EN = 1;
	pHSIProps->pHSIRegisters->TX_FIFO_CONFIG[HSI_COMMAND_CHANNEL].value = temp_reg.value;
	pHSIProps->pHSIRegisters->TX_FIFO_CONFIG[HSI_COMMAND_CHANNEL].value |= 0x1;
	
	//Configure TX FIFO register 1 for channel 1.
	// .	FIFO Base = 0x40. (256 bytes offset)
	// .	FIFO Size = 32. This means 64 frames = 256 bytes.
	// .	Set SAFETY_VALVE_EN to force the transmitting side to stall when the B side RX FIFO is full.
	// .	Set Req_En to enable FIFO interrupts.
	// .	Enable the channel through the FIFO_EN bit.
	temp_reg.value = 0;
	temp_reg.FIFO_BASE = 0x40;
	temp_reg.FIFO_SIZE = 32;
	temp_reg.REQ_EN = 1;
	temp_reg.FIFO_THRS = 32*2 - 1;
	pHSIProps->pHSIRegisters->TX_FIFO_CONFIG[HSI_DATA_CHANNEL].value = temp_reg.value;
	pHSIProps->pHSIRegisters->TX_FIFO_CONFIG[HSI_DATA_CHANNEL].value |= 0x1;

	//Clear all flags in the CHNL_STATUS register.
	pHSIProps->pHSIRegisters->CHNL_STATUS[HSI_COMMAND_CHANNEL] |= HSI_CHNL_STATUS_CLEAR;
	pHSIProps->pHSIRegisters->CHNL_STATUS[HSI_DATA_CHANNEL] |= HSI_CHNL_STATUS_CLEAR;

	//Set Up CHNL_INT_MASK for Channel 0 
	//	.	RX_FIFO_NOT_EMPTY
	pHSIProps->pHSIRegisters->CHNL_INT_MASK[HSI_COMMAND_CHANNEL] = CHNL_INT_STATUS_RX_FIFO_NOT_EMPTY;

	//Enable the HSI Interrupt Mask Registers (HSI_INT_MASK).
	pHSIProps->pHSIRegisters->HSI_INT_MASK[0] = (	HSI_INT_MASK_CHNL_0			| 
													HSI_INT_MASK_ERR_INT_MASK	|
													HSI_INT_MASK_BRK_INT_MASK	);	//0x30005;

	//NOTE: COMMAND channel interrupts are enabled; DATA channel interrupts are *not*
	//Set Oversampling mode ?
	pHSIProps->pHSIRegisters->HSI_CNTRL |= HSI_CNTRL_RCVR_MODE;

	//Enable HSI. Set bit 0 in the HSI Enable Register.
	pHSIProps->pHSIRegisters->HSI_ENABLE[0] = HSI_ENABLE_HSI_ENABLE;

}

/* HSI_Start
  This routine kicks off the HandShake process
 */
unsigned int HSI_Start()
{
	unsigned int start_time;
    P_MIPI_Command_T p_command;
	P_HSI_Properties_T pHSIProps = GetHSIProperties();
	
	//make sure COMMAND channel TX and RX fifo are clear
	//make sure DATA channel TX fifo is clear
	HSI_Reset_Fifos();

	/** Kick off ECHO cmd **/
	//update state info
	Set_HSI_Connection_Status(HSI_HANDSHAKE);
	
	//get the first array entry
	pHSIProps->handshake_index = 0;
	p_command = Get_HSI_Handshake_Cmd(pHSIProps->handshake_index);
	pHSIProps->handshake_index++;
	
	//wait five seconds before sending ECHO
	start_time = GetOSCR0();
	//if we timeout, fail completely
    while( OSCR0IntervalInMilli(start_time, GetOSCR0()) < 5000 );

	//send the command
	return HSI_SendCmd(p_command);
}

/* HSI_SendImage
  This routines sends the OPEN_CONN cmd, and waits while the image is uploaded
  Once the CONN_CLOSED is recieved, this function will cleanup the interrups and return
  Return value == number of bytes uploaded

	NOTES: 
	  'address' must be 4 byte aligned
	  'size' must be in BYTES and MUST be a multiple of 4
*/
unsigned int HSI_SendImage (unsigned int address, unsigned int size)
{
	unsigned int Retval, start_time;
    MIPI_Command_T command;
	P_HSI_Properties_T pHSIProps = GetHSIProperties();

	//make sure handshake is finished
	start_time = GetOSCR0();
	Retval = NOTDONE;
	do {
		switch(Get_HSI_Connection_Status())
		{
			case HSI_IDLE:
				//break out of do-while
				Retval = NoError;
				break;
			case HSI_HANDSHAKE:
			case HSI_TRANSFER:
				//wait
				break;
			case HSI_ERROR_STATE:
				//need to finish the break-break-break transactions
				//keep track because after 3 errors, HSI is probably dead
			case HSI_UNINITIALIZED:
				//issue the ECHO and then wait
				//if the HSI_Start routine fails, then fail completely 
				if( HSI_Start() )
					return TimeOutError;
				break;
		};

		//if we timeout, fail completely
        if( OSCR0IntervalInMilli(start_time, GetOSCR0()) > HSI_CMD_WAIT_TIME_MS*100 )
			return TimeOutError;

	} while(Retval != NoError);

	//update property variables for current upload
	pHSIProps->buffer_address = address;
	//can only send a maximum of 1 PDU (16MB-4) per OPEN_CONN
	size = size;
	pHSIProps->pdu_size_left = size > HSI_PDU_MAX_SIZE ? HSI_PDU_MAX_SIZE : size;
	//keep track of how much data is left after the first PDU
	pHSIProps->size_left = size - pHSIProps->pdu_size_left;

	//send the OPEN_CONN command
	command.command = OPEN_CONN;
	command.channel = HSI_DATA_CHANNEL;
	command.data = (pHSIProps->pdu_size_left/4);

	//do we need to get the current time stamp for checking on a timeout?
	Set_HSI_Connection_Status(HSI_TRANSFER);
	return HSI_SendCmd(&command);
}

/* HSI_CheckImage
  This routine checks to make sure the current image is finished being uploaded
*/
unsigned int HSI_CheckImage()
{
	unsigned int Retval;
    P_HSI_Properties_T pHSIProps = GetHSIProperties();

	if((pHSIProps->size_left == 0) && (pHSIProps->pdu_size_left == 0))
		Retval = 0;		//finished
	else
		Retval = 1;		//not done yet

	//is there is a timeout to check?
	
	return Retval;
}

/* HSI_Shutdown
 This routine disables the HSI interrupts and restores the controller to default state
*/
void HSI_Shutdown()
{
	P_HSI_Properties_T pHSIProps = GetHSIProperties();
	
	//disable interrupts
	pHSIProps->pHSIRegisters->CHNL_INT_MASK[HSI_COMMAND_CHANNEL] = 0;
	pHSIProps->pHSIRegisters->CHNL_INT_MASK[HSI_DATA_CHANNEL] = 0;
	pHSIProps->pHSIRegisters->HSI_INT_MASK[0] = 0;
	DisablePeripheralIRQInterrupt(INT_MIPI_HSI);

	//disable FIFOs (this clears fifos and status bits)
	pHSIProps->pHSIRegisters->RX_FIFO_CONFIG[HSI_COMMAND_CHANNEL].value &= ~0x1;
	pHSIProps->pHSIRegisters->TX_FIFO_CONFIG[HSI_COMMAND_CHANNEL].value &= ~0x1;
	pHSIProps->pHSIRegisters->TX_FIFO_CONFIG[HSI_DATA_CHANNEL].value &= ~0x1;

	//disable HSI
	pHSIProps->pHSIRegisters->HSI_ENABLE[0] &= ~HSI_ENABLE_HSI_ENABLE;

	//restore pits
	RestoreHSIPins( );
}


/******  Interrupt Mode routines ******/

/********************************************************************
* Main HSI Interrupt routine
* We branch from here to the service routine specific to the channels
********************************************************************/
void HSI_ISR()
{
    unsigned int Retval;
    P_HSI_Properties_T pHSIProps = GetHSIProperties();
    
	//handle any errors first
	Retval = pHSIProps->pHSIRegisters->HSI_INT_STATUS[0];
	Retval = pHSIProps->pHSIRegisters->CHNL_INT_STATUS[HSI_COMMAND_CHANNEL];

    // is it from Channel 0 Rx Fifo. Threshold should have been set to 32 bits or 1 frame.
    if(pHSIProps->pHSIRegisters->CHNL_INT_STATUS[HSI_COMMAND_CHANNEL] & (CHNL_INT_STATUS_RX_FIFO_THRS |
																		 CHNL_INT_STATUS_RX_FIFO_NOT_EMPTY |
																		 CHNL_INT_STATUS_RX_FIFO_FULL))
    {
        HSI_ISR_COMMAND_CHNL();
    }

    // Is Channel 1 Tx Fifo empty
    if(pHSIProps->pHSIRegisters->CHNL_INT_STATUS[HSI_DATA_CHANNEL] & CHNL_INT_STATUS_TX_FIFO_EMPTY)
    {
        HSI_ISR_DATA_CHNL();
    }
}

/********************************************************************
* HSI Interrupt Service routine for Command Channel
* If we got a CONN_READY command, then target is ready to receive data/
* CONN_CLOSE indicates the target is done receiving data. 
* All other commands, if Handshake not done, then we are in middle of
* handshake
********************************************************************/
void HSI_ISR_COMMAND_CHNL()
{
    P_HSI_Properties_T pHSIProps;
    MIPI_Command_T command;
    
    pHSIProps =  GetHSIProperties();
    
    // Read command
    command.value = (pHSIProps->pHSIRegisters->RX_FIFO[HSI_COMMAND_CHANNEL]).HSI_DATA[0];
    
    // Reading should ideally clear status bits but we ignore ACK from Open_Conn cmd! 
    pHSIProps->pHSIRegisters->CHNL_STATUS[HSI_COMMAND_CHANNEL] |= CHNL_INT_STATUS_RX_FIFO_THRS;
    
    switch(command.command)
    {
		//need a BREAK case?

        case CONN_READY:
            // Target indicated ready to receive data.
			HSI_Start_Data_Tx();
            break; 
        case CONN_CLOSE:
			Set_HSI_Connection_Status(HSI_IDLE);
            // Done transmitting PDU
            break;
        default:
            HSI_Handshake();
    }
}

/********************************************************************
* HSI Start Transfer routine
* Sets Channel Interrupt Mask for Data Channel to interrupt on 
* TX_EMPTY
********************************************************************/
void HSI_Start_Data_Tx()
{
    P_HSI_Properties_T pHSIProps;
    
    pHSIProps =  GetHSIProperties();
    
	// Clear any previous statuses (statii ?)
	pHSIProps->pHSIRegisters->CHNL_STATUS[HSI_DATA_CHANNEL] |= HSI_CHNL_STATUS_CLEAR;

	// Unmask Channel 1 interrupts
	pHSIProps->pHSIRegisters->HSI_INT_MASK[0] |= HSI_INT_MASK_CHNL_1;

	// Unmask TX FIFO Empty interrupt on channel 1 (data channel)
	pHSIProps->pHSIRegisters->CHNL_INT_MASK[HSI_DATA_CHANNEL] |= CHNL_INT_STATUS_TX_FIFO_EMPTY;
}

/********************************************************************
* HSI Interrupt service routine for Data Channel
* We got here means Tx FIFO is empty. We fill the Tx fifo with FIFO 
* worth data or remaining data.
********************************************************************/
void HSI_ISR_DATA_CHNL()
{
    MIPI_Command_T command;
    int transmit_size, i;
    P_HSI_Properties_T pHSIProps = GetHSIProperties();
    
	//load the smaller of: fifo size/remaining data
    transmit_size = pHSIProps->pdu_size_left < HSI_DATA_FIFO_SIZE_BYTE ? pHSIProps->pdu_size_left : HSI_DATA_FIFO_SIZE_BYTE;
    pHSIProps->pdu_size_left -= transmit_size;

	//load data into TX Fifo
    for(i=0; i < transmit_size/4; i++)
	{
        pHSIProps->pHSIRegisters->TX_FIFO[HSI_DATA_CHANNEL].HSI_DATA[0] = *(unsigned int *)(pHSIProps->buffer_address);
		pHSIProps->buffer_address+=4;
    }
	
	//finished a pdu download
    if(pHSIProps->pdu_size_left == 0)
    {
		//mask the DATA channel ints for now
		pHSIProps->pHSIRegisters->HSI_INT_MASK[0] &= ~HSI_INT_MASK_CHNL_1;
        pHSIProps->pHSIRegisters->CHNL_INT_MASK[HSI_DATA_CHANNEL] &= ~CHNL_INT_STATUS_TX_FIFO_EMPTY;
		//prepare size counters for next PDU
		pHSIProps->pdu_size_left = pHSIProps->size_left > HSI_PDU_MAX_SIZE ? HSI_PDU_MAX_SIZE : pHSIProps->size_left;
		pHSIProps->size_left -= pHSIProps->pdu_size_left;
		//NOTE: if this was last PDU (no more data), then previous two lines just do '0 = 0 - 0' (i.e. nothing)
    }
}

/********************************************************************
 * HSI Send Command Routine
 * Description: This routine is used to send a HSI command
 * Output: ErrorCode
 *******************************************************************/
unsigned int HSI_SendCmd(P_MIPI_Command_T p_hsi_cmd)
{
    unsigned int Retval =  NoError;
    P_HSI_Properties_T pHSIProps;
    volatile unsigned int *pCmd_Channel_Status;
    unsigned int start_time;
    
    pHSIProps =  GetHSIProperties();
    pCmd_Channel_Status = &(pHSIProps->pHSIRegisters->CHNL_STATUS[HSI_COMMAND_CHANNEL]);
    
    //set up timer
	start_time = GetOSCR0();
    
    while(!(*pCmd_Channel_Status & HSI_CHNL_STATUS_TX_EMPTY))
    {
        if( OSCR0IntervalInMilli(start_time, GetOSCR0()) > HSI_CMD_WAIT_TIME_MS )
			{
                Retval = TimeOutError;
                Set_HSI_Connection_Status(HSI_ERROR_STATE);
                return Retval;
            }
    }
    
    pHSIProps->pHSIRegisters->TX_FIFO[HSI_COMMAND_CHANNEL].HSI_DATA[0] = p_hsi_cmd->value;
    
    return Retval;
}

/********************************************************************
 * HSI HandShake Routine.
 * Output: ErrorCode
 * Description: This routine sends the commands in the Handshake_Command 
 * array.
 * Entry point 1. HSI_Init. Loop on HSIProperties->connection_status 
 * till HANDSHAKE_DONE
 * Entry point 2. HSI_ISR_CHNL_0 if HSIProperties->connection_status 
 * is not HANDSHAKE_DONE
 ********************************************************************/ 
unsigned int HSI_Handshake()
{
    unsigned int Retval = NoError;
    P_MIPI_Command_T p_hsi_cmd;
	P_HSI_Properties_T pHSIProps = GetHSIProperties();

    if(HSI_ERROR_STATE == Get_HSI_Connection_Status())
	{
		pHSIProps->handshake_index = 0;
		Set_HSI_Connection_Status(HSI_HANDSHAKE);
	}
    
    if(pHSIProps->handshake_index < HSI_NUMBER_HANDSHAKE_COMMANDS)
    {
        p_hsi_cmd = Get_HSI_Handshake_Cmd(pHSIProps->handshake_index);
        Retval = HSI_SendCmd(p_hsi_cmd);
        pHSIProps->handshake_index++;
    }
    else
        Set_HSI_Connection_Status(HSI_IDLE); // HSI handshake done
        
    return Retval;
}

/********************************************************************
* GET HSI Handshake array
* Returns a pointer to HSI_Handshake array
********************************************************************/
P_MIPI_Command_T Get_HSI_Handshake_Cmd(unsigned int index)
{
    return &(HSI_Handshake_Commands[index]);
}    
        
/*******************************************************************
Get HSI connection status
* Possible values: 
* 1. UNINITIALIZED
* 2. IN_PROGRESS
* 3. HANDSHAKE_DONE
* 4. ERROR_STATE
*******************************************************************/
CONNECTION_STATUS Get_HSI_Connection_Status()
{
    return GetHSIProperties()->connection_status;
}

void Set_HSI_Connection_Status(CONNECTION_STATUS conn_stat)
{
	GetHSIProperties()->connection_status = conn_stat;
}
