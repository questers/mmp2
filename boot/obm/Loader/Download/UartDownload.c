/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document.
**  Except as permitted by such license, no part of this document may be
**  reproduced, stored in a retrieval system, or transmitted in any form or by
**  any means without the express written consent of Intel Corporation.
**
**  FILENAME:	UartDownload.c
**
**  PURPOSE: 	Bulverde Boot ROM UART download program
**
**  History: 	4/28/05 - First version for Monahans
******************************************************************************/


#include "UartDownload.h"
#include "timer.h"

UINT_T  UartReadWriteWaitTime = 10;

/***********************************************************************************
* FFUART Routines
*
***********************************************************************************/

UINT_T ReadFFUartLineStatus()
{
    return (*(UINT_T *)FFLSR);
}


UINT8_T ReadFFUartReceiveFIFO()
{
     return (*(UINT8_T *)FFRBR);
}

UINT_T WriteCharToFFUart(INT8_T WriteChar)
{
	UINT_T StartTime, EndTime, WaitTime = 1;
	UINT_T Retval = NoError;

	StartTime  = GetOSCR0();  //Dummy read to flush potentially bad data
    StartTime  = GetOSCR0();

	for (;;)
	{
		if ((ReadFFUartLineStatus() & 0x40) == 0x40)
		{
		  	*FFTHR = WriteChar;	// Write to Transmit FIFO
			break;
		}

		EndTime  = GetOSCR0();
		if (EndTime < StartTime)
			EndTime = ((0x0 - StartTime) + EndTime);
		WaitTime = OSCR0IntervalInSec(StartTime, EndTime);
		if (WaitTime >= UartReadWriteWaitTime)

		{
			Retval = UartReadWriteTimeOutError;
			break;
		}
	}
  	return (Retval);
}
#define CMD_SIZE 8

UINT_T FFUARTReadCommand(UINT_T size) {
	UINT_T Retval = 0;
	UINT_T length = 0;
	UINT_T i = 0;
	UINT8_T tmpChar;
	UINT_T FOR_Value;
    UINT8_T *TmpAddr = (UINT8_T *)&(getProtocolCmd()->Command);
	pProtocolISR pISR = getProtocolISR();

 	// read command (everthing but data field)
	for(i = 0 ;i < CMD_SIZE; i++)
		*TmpAddr++ = ReadFFUartReceiveFIFO();

	length = getProtocolCmd()->Length;
	pISR->CurrentCommand = getProtocolCmd()->Command;
	if(pISR->CurrentCommand == DownloadDataCmd)
		TmpAddr = (UINT8_T*)pISR->CurrentTransferAddress;
	i = 0;
	// read in data field
	while (i < length){
		if((ReadFFUartLineStatus() & 0x1) == 0x1){
			*TmpAddr++ = ReadFFUartReceiveFIFO();
			i++;
		}
	}
	if(pISR->CurrentCommand == DownloadDataCmd)
		pISR->CurrentTransferAddress = (UINT32_T)TmpAddr;

	Retval = NoError;

	return Retval;
}

extern UINT8_T preambleString[];

void  FFUARTInterruptHandler(void)
{
	UINT_T IIR_Value;
	UINT_T LSR_Value;
	UINT_T FOR_Value;
	UINT_T FCR_Value;
	UINT8_T RecChar[8];
	UINT8_T Retval = NoError;
	UINT8_T	i;
	pProtocolISR pISR = getProtocolISR();

	// Check out IID[IIR] to see the cause of the interrupt
	IIR_Value = *FFIIR;

	// Handle Line Status Error
	if ((IIR_Value & UartIirEseRE) == UartIirEseRE)
	{
		LSR_Value = *FFLSR; // Read in Line Status to clear error
		ClearPortInterruptFlag();
		return;
	}

	// Handle Transmit FIFO Ready
	if ((IIR_Value & UartIirEseTF) == UartIirEseTF)
	{
		ClearPortInterruptFlag();
		// Reading IIR already cleared it, implementation TBD
		return;
	}

	//read how many bytes are in FIFO
	FOR_Value = *FFFOR & 0x0000001F;

	// Are we connected to the host? Check for a preamble in the fifo...
	if (pISR->PreambleReceived == FALSE)
	{
		for(i = 0; i < FOR_Value ; i++)
			RecChar[i] = ReadFFUartReceiveFIFO();

		//clear out rest of FIFO
		*FFFCR = (UartFcrFe | UartFcrTIL | UartFcrITL_8 | UartFcrResetRf);

		//the preamble will be either in the first 4 bytes or last 4 bytes
		if(ProtocolVerifyPreamble(&RecChar[0]) || ProtocolVerifyPreamble(&RecChar[4])){
			pISR->PreambleReceived = TRUE;
			pISR->InterruptPort = FFUART_D;
			//turn off any ports that may be on
			ShutdownPort(SE_USB_D);
			ShutdownPort(DIFF_USB_D);
			ShutdownPort(U2D_USB_D);
            ShutdownPort(CI2_USB_D);
			ClearPortInterruptFlag();
			//send preamble back.  don't clear interrupt flag
			Retval = SendResponse(preambleString, PREAMBLE_SIZE);
		} else if (RecChar[0] == DisconnectCmd) {
			memcpy((void *)&getProtocolCmd()->Command, &RecChar[0], 8);
			pISR->CommandReceived = TRUE;
			pISR->PreambleReceived = TRUE;
		} else if (RecChar[0] == MessageCmd)
			Retval = HandleMessageCmd();
	}
	else	// read command
	{
		
		Retval = FFUARTReadCommand(FOR_Value);
        if(pISR->CurrentCommand != MessageCmd 
        #if !BOOTROM && DOWNLOAD_USED
        && pISR->CurrentCommand != ProtocolVersionCmd && pISR->CurrentCommand != GetParametersCmd
        #endif
        )
            pISR->CommandReceived = TRUE;
		//if we get a message cmd, try to reply
		if(pISR->CurrentCommand == MessageCmd)
		{
			ClearPortInterruptFlag();
			Retval = HandleMessageCmd();
		}
        #if !BOOTROM && DOWNLOAD_USED
        if(pISR->CurrentCommand == ProtocolVersionCmd)
		{
			ClearPortInterruptFlag();
			Retval = HandleProtocolVersionCmd();
		}
        if(pISR->CurrentCommand == GetParametersCmd)
		{
			ClearPortInterruptFlag();
			Retval = HandleGetParametersCmd();
		}
        #endif
	}

	return;
}

UINT_T WriteStringToFFUart(UINT8_T* message){
	UINT_T Retval = NoError;
#if VERBOSE_DEBUG
	UINT_T size;
	UINT_T i;

	if(GetPortState(FFUART_D))
	{
		size = strlen((char *)message);
		for(i = 0; i < size ; i++) {
			Retval = WriteCharToFFUart((char)message[i]);
			if (Retval != NoError) break;
		}
	}
#endif
	return Retval;
}

UINT_T WriteAliveCharToFFUart(UINT8_T character, UINT_T *UartMsgBuffIndex)
{
	UINT_T Retval = NoError;
#if VERBOSE_DEBUG
	UINT8_T UartMsgBuff[UARTMSGBUFFINDEXMAX];
	INT_T i;

	Delay(100000);	// microseconds(us) 100,000 = 0.1 seconds
	UartMsgBuff[0] = character; // "."
	*UartMsgBuffIndex = *UartMsgBuffIndex + 1;
	UartMsgBuff[1]=0x0;
	WriteStringToFFUart(UartMsgBuff);
	if (*UartMsgBuffIndex >=  UARTMSGBUFFINDEXMAX-4)
	{
	    UartMsgBuff[0]= 0xD; // CR
	   	UartMsgBuff[1]= 0x0;
		WriteStringToFFUart(UartMsgBuff);
		for (i=0; i<(UARTMSGBUFFINDEXMAX);i++)
		{
			UartMsgBuff[i] = 0x20 ;	//" " space
		}
		UartMsgBuff[UARTMSGBUFFINDEXMAX-1]=0x0;
				WriteStringToFFUart(UartMsgBuff);
		UartMsgBuff[0]= 0xD; // CR
	   	UartMsgBuff[1]= 0x0;
		WriteStringToFFUart(UartMsgBuff);
		*UartMsgBuffIndex = 0;
	}
#endif
	return Retval;
}

