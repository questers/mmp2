# (C) Copyright 2007 Marvell International Ltd.  
#  All Rights Reserved
#
##############################################################
#
#   Top level make file for the bootloader
#
##############################################################

##############################################################
# Global Macros
#   These values are defaults and can be platform.mak
#   will override these values if desired
#
# Note the defaults;
#  build type: release
#  board: aspenite
#  enable NOR, NAND, OneNAND, MMC and MMC Partitions
#  enable USB using CI2
#
##############################################################
LOADERVERSION = 3_2_19

BASE = ..
SDT = $(MARVELL_SDT_HOME)
XDB = $(MARVELL_XDB_HOME)

################# FLAGS START ################################
# ALL COMBINATION FLAGS
RELEASE = 1
#Tool Chain
RVCT_BUILD = 1  # 1=RVCT ToolChain, 0=SDT
#Build type
ARM_MODE = 1	# 1=ARM, 0=Thumb mode

IMAGE_TYPE = BL	 # Default to BootLoader (alternative is BS=BootStrap)

#Flash Types
NOR = 0
NAND = 1
ONENAND = 0
SPI = 0
MMC = 1
TRUSTED = 0
# END ALL COMBINATION FLAGS

HSI = 0
FBF = 1
I2C = 0
DOWNLOAD_USED = 1
VERIFY_WRITE = 0
LOAD_ALL_IMAGES = 0

# NEW DDR CONFIGURATION
NEW_DDRCNFG = 0

#USB types
USB1 = 0
USB2 = 0
# DEPENDENT FLAGS USBCI
USBCI = 0
CI2_USB_DDR_BUF = 1
UART_UPLOAD = 0
OBM_CONF_USB_CLASS_ACM=0

# SPI FLAGS
SPI_PIO = 0

# MMC FLAGS
MMC_SDMA_MODE = 1
SDHC_CNTL = 1
MMC_FAST_INIT_FREQ = 0
# set to one if performing eMMC configuration for alternate boot mode
CONFIG_EMMC_ALTERNATE_BOOT = 0

# TRUSTED FLAGS
PROTOCOL_JTAG_USED = 0
# Crypto and fuse programming module selection
# must be overrided in platform make file
BL_USE_IPPCP_CRYPTO = 0
BL_USE_GEU_FUSE_PROG = 0
BL_USE_WTM_CRYPTO = 0
BL_USE_WTM_FUSE_PROG = 0
LOCK_FUSE_BLOCK0_ECC = 0

######################### DEBUG FLAGS ########################
OBMTEST = 0
VERBOSE_DEBUG = 0
VERBOSE_MODE = 1
JTAG_BREAKIN = 0
COPYIMAGESTOFLASH = 1
# BOARD_DEBUG will override fuses. For now, override fuses.
BOARD_DEBUG = 0
MMC_DEBUG = 0

############## PLATFORM SPECIFIC FLAGS. FIX ME ################
TVTD_NTCFG = 0       # set to 1 if performing platform config under non-trusted build
# Set this to 1 for DKB to program with correct ECC for ASPNB0 Bootrom
ASPN_MLC_BCH = 0
SLC_NONFI = 0
JTAG_PROTOCOL_OVER_JTAG_SUPPORTED = 0

################# FLAGS END ##################################

##############################################################
# Global Tools
#   These values are defaults and can be platform.mak
#   will override these values if desired
##############################################################
Symbol_Converter = "$(XDB)\xdbbin\dwarf2bd.exe"

!if $(RVCT_BUILD)			## RVCT ##
ToolChain = RVCT
C_Compiler = "C:\Program Files\ARM\RVCT\Programs\3.1\569\win_32-pentium\armcc.exe"
Cxx_Compiler = "C:\Program Files\ARM\RVCT\Programs\3.1\569\win_32-pentium\armcpp.exe"
Assembler = "C:\Program Files\ARM\RVCT\Programs\3.1\569\win_32-pentium\armasm.exe"
Linker = "C:\Program Files\ARM\RVCT\Programs\3.1\569\win_32-pentium\armlink.exe"
elf2bin = "C:\Program Files\ARM\RVCT\Programs\3.1\569\win_32-pentium\fromelf.exe"

!if $(ARM_MODE) 			# ARM Mode
InstructionType = ARM
AS_InstructionType = --arm
CC_InstructionType = --arm
!else						# Thumb Mode
InstructionType = THUMB
AS_InstructionType = --arm --apcs=/interwork
CC_InstructionType = --thumb --apcs=/interwork
!endif

!else						## SDT ##
ToolChain = SDT
Assembler = "$(SDT)\marvellpxa\bin\asxsc.exe"
Linker = "$(SDT)\marvellpxa\bin\ldxsc.exe"
elf2bin = "$(SDT)\xdbbin\elf2bin.exe"

!if $(ARM_MODE)				# ARM Mode
InstructionType = ARM
AS_InstructionType = -code32
C_Compiler = "$(SDT)\marvellpxa\bin\ccxsc.exe"
Cxx_Compiler = "$(SDT)\marvellpxa\bin\ccxsc.exe"
!else 						# Thumb Mode
InstructionType = THUMB
AS_InstructionType = -code16
C_Compiler = "$(SDT)\marvellpxa\bin\ccxscthb.exe"
Cxx_Compiler = "$(SDT)\marvellpxa\bin\ccxscthb.exe"
!endif

!endif 	# end if SDT #

##############################################################
# Default compile flags
#   These values are defaults and can be platform.mak
#   will override these values if desired
##############################################################
A_FLAGS = $(A_FLAGS) -PD "BOOTROM SETA 0" -PD "$(PLATFORM) SETA 1" $(AS_InstructionType)

CC_FLAGS = $(CC_FLAGS) \
		$(CC_InstructionType) \
		-D$(PLATFORM)=1 \
		-DI2C=$(I2C) \
		-DONENAND_CODE=$(ONENAND) \
		-DSPI_CODE=$(SPI) \
		-DSPI_PIO=$(SPI_PIO) \
		-DMMC_CODE=$(MMC) \
		-DMMC_DEBUG=$(MMC_DEBUG) \
		-DMMC_SDMA_MODE=$(MMC_SDMA_MODE) \
		-DSDHC_CNTL=$(SDHC_CNTL) \
		-DMMC_FAST_INIT_FREQ=$(MMC_FAST_INIT_FREQ) \
		-DNAND_CODE=$(NAND) -DNOR_CODE=$(NOR) \
		-DBOOTROM=0 -DHSI=$(HSI) \
		-DJTAG_PROTOCOL_OVER_JTAG_SUPPORTED=0 \
		-DPROTOCOL_JTAG_USED=0 -DTRUSTED=$(TRUSTED) \
		-DOBMTEST=$(OBMTEST) -DVERBOSE_MODE=$(VERBOSE_MODE) \
		-DVERBOSE_DEBUG=$(VERBOSE_DEBUG) -DDOWNLOAD_USED=$(DOWNLOAD_USED) \
		-DCOPYIMAGESTOFLASH=$(COPYIMAGESTOFLASH) -DDEVICE_KEY=$(DEVICE_KEY) \
		-DUSB1=$(USB1) -DUSB2=$(USB2) -DUSBCI=$(USBCI) -DBOARD_DEBUG=$(BOARD_DEBUG) -DLOAD_ALL_IMAGES=$(LOAD_ALL_IMAGES) \
		-DJTAG_BREAKIN=$(JTAG_BREAKIN) -DCI2_USB_DDR_BUF=$(CI2_USB_DDR_BUF) -DFBF=$(FBF) -DVERIFY_WRITE=$(VERIFY_WRITE) \
		-DBL_USE_IPPCP_CRYPTO=$(BL_USE_IPPCP_CRYPTO) -DBL_USE_WTM_CRYPTO=$(BL_USE_WTM_CRYPTO) \
		-DBL_USE_GEU_FUSE_PROG=$(BL_USE_GEU_FUSE_PROG) -DBL_USE_WTM_FUSE_PROG=$(BL_USE_WTM_FUSE_PROG) \
		-DLOCK_FUSE_BLOCK0_ECC=$(LOCK_FUSE_BLOCK0_ECC) \
		-DRVCT=$(RVCT_BUILD) -DNEW_DDRCNFG=$(NEW_DDRCNFG) -DCONFIG_EMMC_ALTERNATE_BOOT=$(CONFIG_EMMC_ALTERNATE_BOOT) \
        -DASPN_MLC_BCH=$(ASPN_MLC_BCH) -DSLC_NONFI=$(SLC_NONFI) -DUART_UPLOAD=$(UART_UPLOAD) -DOBM_CONF_USB_CLASS_ACM=$(OBM_CONF_USB_CLASS_ACM) \

##############################################################
# Default Inlcudes additional files can be included
#   in the platform.mak file if desired
##############################################################

INCLUDES = \
 -I$(BASE)\Include \
 -I$(BASE)\Platforms\$(PLATFORM) \
 -I$(BASE)\Include\$(TECHNOLOGY)\$(PLATFORM) \
 -I$(BASE)\Include\$(TECHNOLOGY) \
 -I$(BASE)\Include\Flash \
 -I$(BASE)\Include\Flash\SDMMC \
 -I$(BASE)\Include\Flash\XIP \
 -I$(BASE)\Include\Flash\M-SYS \
 -I$(BASE)\Include\Flash\FlexOneNand \
 -I$(BASE)\Include\Flash\NAND \
 -I$(BASE)\Include\Flash\DFC \
 -I$(BASE)\Include\Flash\SPI \
 -I$(BASE)\Include\Download \
 -I$(BASE)\Include\Download\UART \
 -I$(BASE)\Include\Download\CI2 \
 -I$(BASE)\Include\Download\U2D \
 -I$(BASE)\Include\Download\USB1 \
 -I$(BASE)\Include\Download\HSI \
 -I$(BASE)\Include\TTC \
 -I$(BASE)\Include\TIM \
 -I$(BASE)\Include\SecureBoot \
 -I$(BASE)\Include\SecureBoot\DSVL \
 -I$(BASE)\Include\SecureBoot\GEU \
 -I$(BASE)\Include\SecureBoot\FuseBlock \
 -I$(BASE)\Include\SecureBoot\IppCp \
 -I$(BASE)\Include\SDRam    \
 -I$(BASE)\Include\SecureBoot\WTM3 \
 -I$(BASE)\SecureBoot\wtm_mbox \

##############################################################
# Pull in the platform specific information
#   This includes object definitions and overrides
##############################################################

!if $(RELEASE)
#release directory
OUTDIR = $(BASE)\Platforms\$(PLATFORM)\release
!else
#debug directory
OUTDIR = $(BASE)\Platforms\$(PLATFORM)\debug
!endif

!INCLUDE "$(BASE)\Platforms\$(PLATFORM)\bl_platform.mak"

!if ($(SDHC_CNTL) == 1)
INCLUDES = $(INCLUDES) -I$(BASE)\Include\Flash\SDMMC\SDHC1
!else if ($(SDHC_CNTL) == 2)
INCLUDES = $(INCLUDES) -I$(BASE)\Include\Flash\SDMMC\SDHC2
!endif

DIR = mkdir

TARGETNAME = $(OUTDIR)\$(BOARD)\$(PLATFORM)_$(ToolChain)_$(InstructionType)_$(IMAGE_TYPE)_$(LOADERVERSION)

################################################################
#  Inference Rules
#   - List of how to build .o files from any basic file
#		- Exception: Startup.s, as there are many many different input flags
#	- These will get invokes when the 'ALL: $(OBJS)' checks the dependents (OBJS)
#	- Only those dependents listed in OBJS will be invoked
#	- In this way, it doesn't matter if you are building 'out' any code, USBCI for example,
#		since the USBCI.o files will not be listed in $(OBJS), their rules won't be invoked
#   - When adding a new directory, follow the convention below and just add a new line.
################################################################
.SUFFIXES : .s

{$(BASE)\I2C}.c{$(OUTDIR)}.o:						; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Flash\SDMMC}.c{$(OUTDIR)}.o:				; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Flash\SDMMC\SDHC1}.c{$(OUTDIR)}.o:			; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Flash\SDMMC\SDHC2}.c{$(OUTDIR)}.o:			; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Download}.c{$(OUTDIR)}.o:					; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Flash\DFC}.c{$(OUTDIR)}.o:					; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Flash\NAND}.c{$(OUTDIR)}.o:				; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Flash\XIP}.c{$(OUTDIR)}.o:					; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Flash\SPI}.c{$(OUTDIR)}.o:					; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\MainProgram}.c{$(OUTDIR)}.o:				; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Dma}.c{$(OUTDIR)}.o:						; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Flash}.c{$(OUTDIR)}.o:						; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Misc}.c{$(OUTDIR)}.o:						; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\SDRam}.c{$(OUTDIR)}.o:						; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Tim}.c{$(OUTDIR)}.o:						; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Platforms\$(PLATFORM)}.c{$(OUTDIR)}.o:		; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\Platforms\$(PLATFORM)}.s{$(OUTDIR)}.o:		; $(Assembler)  $(A_FLAGS) $(INCLUDES) -o $@ $< -list $*.lis
{$(BASE)\Flash\FlexOneNand}.c{$(OUTDIR)}.o:			; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\SECUREBOOT}.c{$(OUTDIR)}.o:				; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\SECUREBOOT\GEU}.c{$(OUTDIR)}.o:			; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\SECUREBOOT\IPPCP}.c{$(OUTDIR)}.o:			; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\SECUREBOOT\wtm_mbox}.c{$(OUTDIR)}.o:		; $(C_Compiler) $(CC_FLAGS) $(PREDEFINES) $(INCLUDES) -c $< -o $@
{$(BASE)\STARTUP}.s{$(OUTDIR)}.o:					; $(Assembler)  $(A_FLAGS) $(INCLUDES) -o $@ $< -list $*.lis


#########################################
###         Linker Rules
#########################################

ALL: DIRECTORY $(OBJ)
    @$(Linker) $(LD_FLAGS) $(OBJ) $(LIBLIST) $(FUSELIB)
	$(ELF2BIN_LINE)
	@$(Symbol_Converter)  -hx "$(TARGETNAME).elf"
	@echo Build Success!


DIRECTORY:
!if (!(EXIST("$(OUTDIR)")))
	$(DIR) "$(OUTDIR)"
!endif
!if (!(EXIST("$(OUTDIR)\$(BOARD)")))
	$(DIR) "$(OUTDIR)\$(BOARD)"
!endif


clean :
    -del /F/Q $(BASE)\Platforms\$(PLATFORM)\debug\$(BOARD)\*.*
    -del /F/Q $(BASE)\Platforms\$(PLATFORM)\debug\*.*
    -del /F/Q $(BASE)\Platforms\$(PLATFORM)\release\$(BOARD)\*.*
    -del /F/Q $(BASE)\Platforms\$(PLATFORM)\release\*.*
    -del /F/Q $(BASE)\*.scc
    -del /F/Q $(BASE)\*.bak
