# (C) Copyright 2007 Marvell International Ltd.  
#  All Rights Reserved
#
##############################################################
#
#   Top level make file for the bootloader
#
##############################################################


.EXPORT_ALL_VARIABLES:


# environment variablesplat
# the following values are frequently overridden on the command line.
# here we'll set some reasonable defaults so a build will work with no parameters.
# the defaults will do a:
#   nontrusted release ttc build for linux that supports:
#   usbci download, uart download,
#   nand, onenand and nor flash

LOADERVERSION = 3_2_19

# directory information about this source tree...
TOPDIR :=$(shell pwd)/..

# Flags
#######
RELEASE = 1
TRUSTED = 0

LINUX = 1
WINCE = 0
ARM_MODE = 1
ToolChain = GNU
IMAGE_TYPE = BL

# BOARD_DEBUG will override fuses.
BOARD_DEBUG = 0
MMC_DEBUG = 0

I2C = 0
OBMTEST =   0
VERBOSE_DEBUG = 1
VERBOSE_MODE = 1
DOWNLOAD_USED = 1
COPYIMAGESTOFLASH = 1
VERIFY_WRITE = 0
DEVICE_KEY  =   0
PERFORM_OTP_LOCKING =   0
PERFORM_FUSE_PROGRAM =  0
LOAD_ALL_IMAGES = 0
JTAG_BREAKIN = 0

#KEYPAD
KEYPAD = 0

#USB types
USB1 = 0
USB2 = 0
USBCI = 0
CI2_USB_DDR_BUF = 1
FBF = 1
UART_UPLOAD = 0

#Flash Types
NOR = 0
NAND = 1
ONENAND = 0
MMC = 1
SPI = 0
SPI_PIO = 0
SDMMC_PARTITION_EN = 1
MMC_SDMA_MODE = 1
MMC_FAST_INIT_FREQ = 0
CONFIG_EMMC_ALTERNATE_BOOT = 0
SLC_NONFI = 0
HSI = 0

# Must be set when using DKB to program on ASPNB0 BOOTROM with too few FLASH TYPES
# and using MLC Device in BCH mode.
ASPN_MLC_BCH = 0
ASPNB0 = 0

TVTD_NTCFG=0

A0_BOARD=0
Y0_BOARD=1

# Crypto and fuse programming module selection
# must be overrided in platform make file
BL_USE_IPPCP_CRYPTO = 0
BL_USE_GEU_FUSE_PROG = 0
BL_USE_WTM_CRYPTO = 0
BL_USE_WTM_FUSE_PROG = 0
LOCK_FUSE_BLOCK0_ECC = 0

# New DDR Configuration
NEW_DDRCNFG = 0

OBM_CONF_USB_CLASS_ACM=0

LIBS += $(LIB_DIRS)/libgcc.a

####################################################
# Global Tools
#   These values are defaults and can be platform.mak
#   will override these values if desired
##############################################################
CROSS_COMPILE =  arm-marvell-linux-gnueabi-
AS = $(CROSS_COMPILE)as $(AFLAGS)
LD = $(CROSS_COMPILE)ld $(LDFLAGS)
CC = $(CROSS_COMPILE)gcc $(CFLAGS)
STRIP = $(CROSS_COMPILE)strip -R .note -R .comment -S
OBJCOPY	= $(CROSS_COMPILE)objcopy -O binary -R .note -R .comment -S
OBJDUMP	= $(CROSS_COMPILE)objdump
MAKE = make
RM = rm -f
DIR = mkdir


##############################################################
# Default compile flags
#   These values are defaults and can be platform.mak
#   will override these values if desired
##############################################################
#generic between Windows and Linux (easy to update both)
CFLAGS = -DI2C=$(I2C) \
		-DONENAND_CODE=$(ONENAND) -DNAND_CODE=$(NAND) -DNOR_CODE=$(NOR) \
        -D$(PLATFORM)=$(PLATFORM) \
		-DSPI_CODE=$(SPI) -DSPI_PIO=$(SPI_PIO) \
		-DMMC_CODE=$(MMC) -DMMC_DEBUG=$(MMC_DEBUG) -DMMC_SDMA_MODE=$(MMC_SDMA_MODE) \
		-DSDHC_CNTL=$(SDHC_CNTL) -DMMC_FAST_INIT_FREQ=$(MMC_FAST_INIT_FREQ) \
		-DBOOTROM=0  -DTRUSTED=$(TRUSTED) -DOBMTEST=$(OBMTEST) -DBOARD_DEBUG=$(BOARD_DEBUG) \
		-DJTAG_PROTOCOL_OVER_JTAG_SUPPORTED=0 -DPROTOCOL_JTAG_USED=0 -DJTAG_BREAKIN=$(JTAG_BREAKIN) \
		-DVERBOSE_MODE=$(VERBOSE_MODE) -DVERBOSE_DEBUG=$(VERBOSE_DEBUG) -DDOWNLOAD_USED=$(DOWNLOAD_USED) \
		-DCOPYIMAGESTOFLASH=$(COPYIMAGESTOFLASH) \
		-DKEYPAD=$(KEYPAD) \
		-DUSB1=$(USB1) -DUSB2=$(USB2) -DUSBCI=$(USBCI) -DCI2_USB_DDR_BUF=$(CI2_USB_DDR_BUF) \
		-DLOAD_ALL_IMAGES=$(LOAD_ALL_IMAGES) -DFBF=$(FBF) -DVERIFY_WRITE=$(VERIFY_WRITE) \
		-DBL_USE_IPPCP_CRYPTO=$(BL_USE_IPPCP_CRYPTO) -DBL_USE_WTM_CRYPTO=$(BL_USE_WTM_CRYPTO) \
		-DBL_USE_GEU_FUSE_PROG=$(BL_USE_GEU_FUSE_PROG) -DBL_USE_WTM_FUSE_PROG=$(BL_USE_WTM_FUSE_PROG) \
		-DLOCK_FUSE_BLOCK0_ECC=$(LOCK_FUSE_BLOCK0_ECC) \
		-DCONFIG_EMMC_ALTERNATE_BOOT=$(CONFIG_EMMC_ALTERNATE_BOOT) \
        -DNEW_DDRCNFG=$(NEW_DDRCNFG) -DASPN_MLC_BCH=$(ASPN_MLC_BCH) -DSLC_NONFI=$(SLC_NONFI)\
        -DASPNB0=$(ASPNB0) -DUART_UPLOAD=$(UART_UPLOAD) -DOBM_CONF_USB_CLASS_ACM=$(OBM_CONF_USB_CLASS_ACM) \


#linux unique

ifeq "$(ARM_MODE)" "1"
CFLAGS += -DLINUX_BUILD=1 -g -Os -fomit-frame-pointer -fasm -nostdinc -fno-builtin -mcpu=marvell-f
else
CFLAGS += -DLINUX_BUILD=1 -g -Os -fomit-frame-pointer -fasm -nostdinc -fno-builtin -mcpu=marvell-f -mthumb -mthumb-interwork
endif

AFLAGS = -Wall -mcpu=marvell-f -mapcs-stack-check -defsym BOOTROM=0 -defsym GCC=1

LFLAGS = -e __main -nostdlib -format elf32-littlearm

#LIBS = -l /armtool/arm-marvell-linux-gnueabi/lib/gcc/arm-marvell-linux-gnueabi/4.2.0/marvell-f/libgcc.a

#####################################
# include directories
#####################################
INCLUDE_DIRS = \
	-I $(TOPDIR)/Include \
	-I $(TOPDIR)/Platforms/$(PLATFORM) \
	-I $(TOPDIR)/Include/Download \
	-I $(TOPDIR)/Include/Download/CI2 \
	-I $(TOPDIR)/Include/Download/HSI \
	-I $(TOPDIR)/Include/Download/UART \
	-I $(TOPDIR)/Include/Download/USB1 \
	-I $(TOPDIR)/Include/Download/U2D \
	-I $(TOPDIR)/Include/Flash \
	-I $(TOPDIR)/Include/Flash/DFC \
	-I $(TOPDIR)/Include/Flash/FlexOneNand \
	-I $(TOPDIR)/Include/Flash/NAND \
	-I $(TOPDIR)/Include/Flash/SDMMC \
	-I $(TOPDIR)/Include/Flash/XIP \
	-I $(TOPDIR)/Include/Flash/SPI \
	-I $(TOPDIR)/Include/SDRam \
	-I $(TOPDIR)/Include/SecureBoot \
    -I $(TOPDIR)/Include/SecureBoot/DSVL \
	-I $(TOPDIR)/Include/SecureBoot/GEU \
	-I $(TOPDIR)/Include/SecureBoot/FuseBlock \
	-I $(TOPDIR)/Include/SecureBoot/WTM3 \
	-I $(TOPDIR)/Include/SecureBoot/IppCp \
    -I $(TOPDIR)/SecureBoot/wtm_mbox \
	-I $(TOPDIR)/Include/TIM \
	-I $(TOPDIR)/Include/$(TECHNOLOGY) \
	-I $(TOPDIR)/Include/$(TECHNOLOGY)/$(PLATFORM) \
	-I $(TOPDIR)/StartUp


##############################################################
# Pull in the platform specific information
#   This includes object definitions and overrides
##############################################################

ifeq "$(RELEASE)" "1"
OUTDIR = $(TOPDIR)/Platforms/$(PLATFORM)/ReleaseLinux/$(BOARD)
else
OUTDIR = $(TOPDIR)/Platforms/$(PLATFORM)/DebugLinux/$(BOARD)
endif

include $(TOPDIR)/Platforms/$(PLATFORM)/bl_platform_linux.mak


ifeq "$(SDHC_CNTL)" "1"
INCLUDE_DIRS += -I $(TOPDIR)/Include/Flash/SDMMC/SDHC1
else
INCLUDE_DIRS += -I $(TOPDIR)/Include/Flash/SDMMC/SDHC2
endif

ifeq "$(ARM_MODE)" "1"
InstructionType = ARM
else
InstructionType = THUMB
endif

AFLAGS += $(PLATFORMASMFLAGS)
CFLAGS += $(PLATFORMCFLAGS) $(BOARDDEF)
LFLAGS += $(LIBS)
LFLAGS += $(LDCMD)

#################################
# Build Process
#################################
# ensure the output directories exist
junk :=$(shell mkdir -p -v $(OUTDIR))

## Call perl script to make sure assembly files are in GNU format ##
junk :=$(shell perl makegnuassembly.pl ../)

## Create OBJECTS list (changes foo.o to ../Platforms/XXX/Release/foo.o)
OBJECTS = $(patsubst %.o, $(OUTDIR)/%.o, $(OBJS))

ifeq "$(TRUSTED)" "1"
LOADER = $(OUTDIR)/$(PLATFORM)_TLOADER_$(LOADERVERSION)
else
LOADER = $(OUTDIR)/$(PLATFORM)_NTLOADER_$(LOADERVERSION)
endif

##################################################
# Implicit Rules
##################################################
.SUFFIXES: .c .S
%.o : $(TOPDIR)/DMA/%.c ;                       @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Download/%.c ;                  @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Flash/%.c ;                     @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Flash/DFC/%.c ;                 @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Flash/FlexOneNand/%.c ;         @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Flash/NAND/%.c ;                @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Flash/SDMMC/%.c ;               @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Flash/SDMMC/SDHC1/%.c ;         @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Flash/SDMMC/SDHC2/%.c ;         @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Flash/SPI/%.c ;                 @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Flash/XIP/%.c ;                 @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/I2C/%.c ;                       @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/MainProgram/%.c ;               @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Misc/%.c ;                      @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Platforms/$(PLATFORM)/%.c ;     @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Platforms/$(PLATFORM)/%.S ;     @$(AS) $(INCLUDE_DIRS) $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/SDRam/%.c ;                     @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/SecureBoot/%.c ;                @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/SecureBoot/FuseBlock/%.c ;      @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/SecureBoot/GEU/%.c ;            @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/SecureBoot/IppCp/%.c ;          @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/SecureBoot/TrustZone/%.c ;      @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/SecureBoot/WTM3/%.c ;           @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/SecureBoot/wtm_mbox/%.c ;       @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/StartUp/%.S ;                   @$(AS) $(INCLUDE_DIRS) $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt
%.o : $(TOPDIR)/Tim/%.c ;                       @$(CC) $(INCLUDE_DIRS) -c $< -o "$(OUTDIR)/$@" >log.txt 2>err.txt

#######################################################
#  Build Process... FINALLY!
#######################################################
"$(LOADER).bin" : $(OBJS)
	@echo assembly/compile done. linking.
	@$(LD) -o "$(LOADER)-elf32" $(OBJECTS) $(LFLAGS) -Map "$(LOADER).map" >log.txt 2>err.txt
	@echo linking done. strip.
	@$(STRIP) "$(LOADER)-elf32" -o "$(LOADER)-elf32-stripped"
	@echo strip done. objcopy.
	@$(OBJCOPY) "$(LOADER)-elf32-stripped" "$(LOADER).bin"
	@echo Build Successful!

clean:
	@-rm -f $(OUTDIR)/*.o "$(LOADER).bin" "$(LOADER)-elf32" "$(LOADER)-elf32-stripped" "$(LOADER).map"

