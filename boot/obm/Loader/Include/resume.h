/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by  
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:	Resume.h
**
**  PURPOSE: 	Resume definitions for BootROM
**                  
******************************************************************************/
#ifndef __RESUME_H__
#define __RESUME_H__

#include "Typedef.h"
#include "tim.h"
#include "general.h"

#define PLATFORM_ISRAM_RESUME_PKT_LOCATION ISRAM_IMAGE_LOAD_BASE
#define PLATFORM_ISRAM_MASK ISRAM_PHY_ADDR
#define PLATFORM_MAX_RESUME_PARAMS 4
#define ARM926ID 0x00009260

typedef struct
{
  UINT32 valueIsPointer;
  UINT32 valuePointer;
} RESUME_ARG, *P_RESUME_ARG;

typedef struct
{
  UINT32 identifier;
  UINT32 packetSize;
  UINT32 resumeValue;
  UINT32 resumeValueIsPointer;
  UINT32 reserved0;
  UINT32 reserved1;
  UINT32 reserved2;
  UINT32 numResumeParams;
  RESUME_ARG ResumeArg[4];
} RESUME_PACKAGE, *P_RESUME_PACKAGE;

typedef struct 
{
 volatile unsigned long* pASCR;			// Application Subsystem Power Status/Configuration Register
 volatile unsigned long* pARSR;			// Application Subsystem Reset Status Register
 volatile unsigned long* pAD3SR;		// Application Subsystem Wake-Up from D3 Status Register
 volatile unsigned long* pAD3R;			// Application Subsystem D3 Configuration Register
 volatile unsigned long* pPSR; 			
 P_RESUME_PACKAGE pResumePkg;
} RESUME_PARAM, *P_RESUME_PARAM; 

typedef UINT_T (*pQuick_Resume_Function)(UINT_T* pTransferAddr, pFUSE_SET pFuses, P_RESUME_PARAM pResParam ); 
typedef UINT_T (*pTim_Resume_Function)(UINT_T* pTransferAddr, pTIM pTIM_h, P_RESUME_PARAM pResParam);
  
// Prototypes
void SetupPowerRegisterPointers (P_RESUME_PARAM pResRegs);
UINT_T TIMQuickBoot_TTC(UINT_T* pTransferAddr, pTIM pTIM_h, P_RESUME_PARAM pResParam);
UINT_T QuickBoot_NotSupported(UINT_T* pTransferAddr, pFUSE_SET pFuses, P_RESUME_PARAM pResParam );
void ResumePackageParamTransfer(P_TRANSFER_STRUCT pTS_h);
void getCPUMode(UINT_T *, UINT_T *);

#endif
