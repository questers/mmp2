/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by  
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:	BootROM.h
**
**  PURPOSE: 	Contain Boot ROM definitions
**              
******************************************************************************/
 
#ifndef __bootrom_h
#define __bootrom_h

#include "Typedef.h"
#include "general.h"
#include "resume.h"

#define MY_CONSUMER_ID	TBRIDENTIFIER

// BootROM's STATE FLOW
typedef enum
{
	STARTUP = 0, 
	RESTARTREASON = 1,
	SECURITYINIT = 2,
	CONFIGUREFLASH = 3,
	TIMLOAD = 4,
	CONFIGURESAVESTATEFLASH = 5,
	READSAVESTATE = 6,
	TIMVALIDATE = 7,
	RESERVEDAREA = 8, 
	IMAGELOAD = 9,
	IMAGEVALIDATE = 10,
	XFER = 11,
	MAX_STATES
} STATE_FLOW_T;

// This is the new master BootROM structure. It should only be visible within the BootRom Flow
typedef struct
{
	UINT_T  Date;
	UINT_T  TBRVersion;
	UINT_T  PlatformType;
	UINT_T  PlatformSubType;
	UINT_T* pState;				  			// Current State
	UINT_T* pPrevState; 			  		// Previous State
	FUSE_SET Fuses;
	UINT_T ErrorCode;						// Any errors detected in state machine preventing normal boot	
	UINT_T ErrorCode2;
	UINT_T TransferAddr; 					// The address we are requested to transfer to.	
	UINT_T *pLoadImages;					// Pointer to the array of LoadImages.
	UINT8_T FlashProbeIndex; 	

	TIM TIM_h; 								// Contains pointers of TIM structures
	UINT_T  EscSeqStartTime;
	UINT_T  EscSeqWaitTime;					// Escape sequence is disabled when this value is 0
	UINT_T	MonitorCodeAddr;				// Internal BootROM MonitorCode address
	UINT_T  MonitorCodeSize;				// Internal BootROM MonitorCode size
	UINT8_T SPIFlashBackupBootPossible;			
	UINT8_T SecurityInitialized;			// If we initialized the security engine
	UINT8_T JTAGEnabled;					// If we initialized the security engine
	RESUME_PARAM  ResumeParam;		  		// A structure that contains resume pertainint information

	/* Add Externel Events below TBD 
	One option is to develop an event manager. The event manager would allocate an event resource when a task is requested. The event 
	would then be passed to the appropriate driver as a resource. When the event has reached an outcome the event manager would
	then be re-invoked to un-allocate the event resource.	*/

} TBR_Env, *P_TBR_Env;

// Helper Prototypes
void SetupEnvironment (UINT_T platformFuses);
void SetState(STATE_FLOW_T State);
void UpdateStatus(void);
void IncrementState();
void PrepareXferStruct(P_TRANSFER_STRUCT pTS_h);
void EnableEscapeSequence(P_TBR_Env pTBR);
void InitDefaultPort(pFUSE_SET pFuses);
UINT_T InitTIMPort( pFUSE_SET pFuses, pTIM pTIM_h);
void PROTOCOL_Check();
void JTAG_Check();
P_TBR_Env GetTBREnvStruct();
void DUMP_TBR_Error_To_Port();

// State Control Prototypes
void RESTARTREASON_State();
void CONFIGUREFLASH_State();
void READSAVESTATE_State();
void INITIALIZESECURITY_State();
void TIMLOAD_State();
void TIMVALIDATE_State();
void RESERVEDAREA_State();
void IMAGELOAD_State();
void IMAGEVALIDATE_State();
void JTAGENABLE_State();
void XFER_State();

// External Assembly Proto
extern DDRInit(void*, void*);
extern void EnableIrqInterrupts();
extern void DisableIrqInterrupts(void);
extern void TransferControl(VUINT_T, VUINT_T);
extern void vint_low();
extern void PlatformPrepareXfer(P_TRANSFER_STRUCT pTS_h, P_TBR_Env pTBR);
extern void TBRSetupLoadImagesPointers (P_TBR_Env pTBR);
extern UINT_T TBRImageBoundryScan (pTIM pTIM_h, UINT_T ImageID);
#endif
