/*************************************************************
 * Misc.h
 *
 * Contents:
 *      Definitions and functions declarations used fin the 
 *      Boot ROM development
 *
 *************************************************************/
#ifndef Misc_prototypes_h
#define Misc_prototypes_h

int strlen(const char *String);
void memset(void *Addr, unsigned char Val, unsigned long Size); 
void *memcpy(void *dest, const void *src, unsigned int n);
int memcmp( const void *buffer1, const void *buffer2, int count);
int strcpy(char *dest, char *src);
UINT_T ReverseBytes (UINT8_T* Address, UINT_T Size);
unsigned int Endian_Convert (unsigned int in);
int strlen(const char *String);
int strcmpl( char* str1, char* str2, int maxlength );
int stricmpl( char* str1, char* str2, int maxlength );
void ConvertIntToBuf8(unsigned char* StringPtr,unsigned int Value, int Width,unsigned int Count);
INT_T memcmpFF( const void *buffer, INT_T count);


int printf(const char *format, ...);
int sprintf(char *out, const char *format, ...);



#endif // Misc_h
