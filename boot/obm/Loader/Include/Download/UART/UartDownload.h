/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
/***********************************************************************
 * File:		UartDownload.h
 *
 * Description:	Header file for Bulverde Boot ROM UART download program
 *
 ***********************************************************************/
#ifndef _uart_download_h
#define _uart_download_h

#include "Typedef.h"
#include "uart.h"
#include "Errors.h"
#include "Typedef.h"
#include "ProtocolManager.h"
#include "Interrupts.h"
#include "general.h"
#include "misc.h"

#define UARTMSGBUFFINDEXMAX 20

/**
 * Function Prototypes
 **/

UINT_T ReadFFUartLineStatus();
UINT8_T ReadFFUartReceiveFIFO();
UINT_T WriteCharToFFUart(INT8_T );
UINT_T FFUARTReadCommand(UINT_T size);
void  FFUARTInterruptHandler(void);
UINT_T WriteStringToFFUart(UINT8_T* message);
UINT_T WriteAliveCharToFFUart(UINT8_T character, UINT_T *UartMsgBuffIndex);

#endif
