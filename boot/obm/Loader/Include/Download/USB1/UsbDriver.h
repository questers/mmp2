/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
/******************************************************************************
**
**  COPYRIGHT (C) 2000, 2001 Intel Corporation.
**
**  This software as well as the software described in it is furnished under 
**  license and may only be used or copied in accordance with the terms of the 
**  license. The information in this file is furnished for informational use 
**  only, is subject to change without notice, and should not be construed as 
**  a commitment by Intel Corporation. Intel Corporation assumes no 
**  responsibility or liability for any errors or inaccuracies that may appear 
**  in this document or any software that may be provided in association with 
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by 
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:  		UsbDriver.h
**
**  PURPOSE:   		This files contains the BootROM specific definitions for  
**					USB Device controller (UDC)
**
**  LAST MODIFIED:  6/27/2003
**
******************************************************************************/

#ifndef _UsbDriver_h
#define _UsbDriver_h

#include "Typedef.h"
#include "xllp_udc.h"
#include "xllp_defs.h"
#include "Interrupts.h"
#include "Errors.h"


#define UDC_GLOBALS

#ifdef UDC_GLOBALS
#define EXTRN
#else
#define EXTRN extern
#endif


// Error sub-location codes for ERR_L_XSUDC location code

#define ERR_S_XSUDC_HWSETUP        	0x01 // XsUdcHWSetup
#define ERR_S_XSUDC_INT_HANDLER     0x02 // XsUdc Register Interrupt Handler
#define ERR_S_XSUDC_ENABLE_INT      0x03 // XsUdc Enable Interrupt
#define ERR_S_XSUDC_FREE_MEM        0x04 // XsUdc error returning the memory
#define ERR_S_XSUDC_GET_MEM         0x05 // XsUdc no memory has been allocated
#define ERR_S_XSUDC_SHUT            0x06 // XsUdcHWShutdown 
#define ERR_S_XSUDC_DMA_IN          0x07 // XsUdcSetupInEndpointDma
#define ERR_S_XSUDC_DMA_OUT         0x08 // XsUdcSetupOutEndpointDma
#define ERR_S_XSUDC_CONFIG_DMA_IN   0x09 // XsUdcConfigureDmaIn
#define ERR_S_XSUDC_MAIN_INT        0x0a // XsUdc Main Interrupt Handler
#define ERR_S_XSUDC_EP0_INT         0x0b // XsUdc EP0 Interrupt Handler

#define ERR_S_XSUDC_STARTCLIENT     0x80 // Test code used by Start Client Test
#define ERR_S_XSUDC_CABLE           0x81 // USB Cable attachment failed
#define ERR_S_XSUDC_ENUMERATE       0x82 // USB Client Enumerate failed
#define ERR_S_XSUDC_TRANSMIT        0x83 // USB Client Transmit Test
#define ERR_S_XSUDC_RECEIVE         0x84 // USB Client Receive Test
#define XSUDC_TEMP_OUT_BUFF_SIZE    4096

/*
************************************************************************************
*                            FUNCTION PROTOTYPES 
************************************************************************************
*/
void BootRomUdcSetupInEndpoint(P_XLLP_UDC_T pUdcHandle,
         	                   XLLP_UDC_EP_T udcEndpointNum,
                           	   P_XLLP_UINT32_T pTxBuff,
                           	   XLLP_UINT32_T xferLength);
void BootRomUdcSetupOutEndpoint (P_XLLP_UDC_T pUdcHandle,
            	                 XLLP_UDC_EP_T udcEndpointNum,
                 	             XLLP_UINT32_T numBytes);

void BootRomUdcSend (P_XLLP_UDC_T pUdcHandle, 
             	     XLLP_UDC_USB_EP_T usbEndpointNum,
                	 P_XLLP_UINT32_T pTxBuff,
                	 XLLP_UINT32_T xferLength,
                	 P_XLLP_UINT32_T pXferCompleteFlag);
void BootRomUdcRead (P_XLLP_UDC_T pUdcHandle, 
                	 XLLP_UDC_USB_EP_T usbEndpointNum,
                	 P_XLLP_UINT32_T pRxBuff,
                	 P_XLLP_UINT32_T pNumBytes);

XLLP_STATUS_T XllpUdcEnableFirstLevelInterrupt (void);
XLLP_STATUS_T XllpUdcUnRegisterFirstLevelInterrupt(void);
XLLP_STATUS_T XllpUdcDisableFirstLevelInterrupt(void);
void XllpUdcInterruptHandler (P_XLLP_UDC_T pUdcHandle);
UINT32 BootRomUdcHWShutdown (void);
void UdcEndpoint0InterruptHandler(void);
#undef EXTRN

#endif 	
