/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
/******************************************************************************
**
**  COPYRIGHT (C) 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by  
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:	UsbDownload.h
**
**  PURPOSE: 	USB Download header file
**                  
******************************************************************************/

#include "Typedef.h"
#include "general.h"
#include "UsbDriver.h"
#include "xllp_dmac.h"	// DMA header
#include "xllp_udc.h"
#include "ProtocolManager.h"

/**
 * Function Prototypes
 **/
void UdcInit(void);
void UdcDmaHandler(void);
void UdcInterruptHandler(void);
void UdcTransmit(UINT_T, UINT8_T*);
void UdcReceiveDmaUpdate(void);
void UdcReceiveInterruptHandler(void);
void UdcHWShutdown(void);
