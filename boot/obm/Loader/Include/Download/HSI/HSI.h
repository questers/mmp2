/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into
 *  products for purposes authorized by the license agreement provided they
 *  include this notice and the associated copyright notice with any such
 *  product.
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/



#ifndef __HSIFLASH_H__
#define __HSIFLASH_H__

/* HSI Bit patterns */
#define HSI_INT_MASK_CHNL_0				0x10000
#define HSI_INT_MASK_CHNL_1				0x20000

#define HSI_NUMBER_HANDSHAKE_COMMANDS   5
#define HSI_NUMBER_OF_CHANNELS			16
#define HSI_COMMAND_CHANNEL				0
#define HSI_DATA_CHANNEL				1
#define HSI_DATA_FIFO_SIZE				64		//64 frames = 256 bytes
#define HSI_DATA_FIFO_SIZE_BYTE			256		//64 frames = 256 bytes
#define HSI_PDU_MAX_SIZE				0xFFFFFF	//16MB-1 Words
#define HSI_CMD_WAIT_TIME_MS			0x100
#define HSI_IMG_WAIT_TIME_MS			10000	// Wait up to 10 sec's for an image download.

#define MIPI_ECHO_DATA_PATTERN			0x000FFF
#define MIPI_ALLOCATE_CH_B_MASK			0x00FF00

//important bit patterns found in HSI registers
#define HSI_CONFIG1_NUMB_CHAN_BITS		(0x00000001 << 16)
#define HSI_CONFIG1_TRAN_MODE_STREAM	0x00000000
#define HSI_CONFIG2_TX_CLK_DIV_3		0x00000003
#define HSI_CHNL_STATUS_CLEAR			0xFF107A7A
#define HSI_CHNL_STATUS_RX_THRS			0x200
#define HSI_RX_FIFO_CFG_THRS_MASK		0xFFFFFDFF
#define HSI_CHNL_STATUS_RX_FULL			0x400
#define HSI_RX_FIFO_CFG_FULL_MASK		0xFFFFFBFF
#define HSI_CHNL_STATUS_TX_EMPTY		0x1
#define HSI_INT_STATUS_ERRORS			0xF

#if RVCT
#pragma anon_unions
#endif
/* MIPI Command */
typedef union
{
	struct
	{
		unsigned int data		: 24;	//Bits 23-0
		unsigned int channel	: 4;	//Bits 27-24
		unsigned int command	: 4;	//Bits 31-28
	};
	unsigned int value;			//full value
} MIPI_Command_T, *P_MIPI_Command_T;

// list of MIPI commands
typedef enum
{
	BREAK = 0,
	ECHO,
	INFO_REQ,
	INFO,
	CONFIGURE,
	ALLOCATE_CH,
	RELEASE_CH,
	OPEN_CONN,
	CONN_READY,
	CONN_CLOSE,
	CANCEL_CONN,
	ACK,
	NACK
} MIPI_COMMANDS;

//MIPI info class list
typedef enum
{
	RESERVED = 0,
	ASIC_TYPE,
	ASIC_VERSION,
	HSI_VERSION,
	HSI_CONFIG,
	SW_VERSION,
	LANGUAGE,
	OPERATOR,
	BREAK_REASON,
	FUTURE
} MIPI_INFO_CLASS_T;

/* HSI FIFO CONFIG register layout (same for both TX and RX) */
typedef union
{
	unsigned int value;			//full value
	struct
	{
		unsigned int FIFO_EN			: 1;	//Bit 0
		unsigned int REQ_EN				: 1;
		unsigned int REQ_SEL			: 1;
		unsigned int SAFETY_VALVE_EN	: 1;
		unsigned int reserved			: 8;
		unsigned int FIFO_SIZE			: 6;
		unsigned int FIFO_THRS			: 6;
		unsigned int FIFO_BASE			: 8;	//Bits 31-24
	};
} HSI_XX_FIFO_CONFIG_T;

typedef struct
{
	volatile unsigned int HSI_DATA[2];
} HSI_8BYTE_DATA_FIFO;

//Register Map
typedef struct
{
	volatile unsigned int HSI_ENABLE[4];									// 0x0000 - 0x000C
	volatile unsigned int HSI_CNTRL;										// 0x0010
	volatile unsigned int HSI_CONFIG_R1;									// 0x0014
	volatile unsigned int HSI_CONFIG_R2;									// 0x0018
	volatile unsigned int BRK_CONFIG;										// 0x001C
	volatile unsigned int BURST_CONFIG;										// 0x0020
	volatile unsigned int FTO_CONFIG;										// 0x0024
	volatile unsigned int RTBC_CONFIG;										// 0x0028
	volatile unsigned int HSI_RIPC;											// 0x002C
	unsigned int Reserved0[4];												//filler
	volatile unsigned int TLTC_CONFIG[HSI_NUMBER_OF_CHANNELS];				// 0x0040 - 0x007C
	volatile HSI_XX_FIFO_CONFIG_T RX_FIFO_CONFIG[HSI_NUMBER_OF_CHANNELS]; 	// 0x0080 - 0x00BC
	volatile HSI_XX_FIFO_CONFIG_T TX_FIFO_CONFIG[HSI_NUMBER_OF_CHANNELS];	// 0x00C0 - 0x00FC
	volatile unsigned int HSI_IPC_DATA[HSI_NUMBER_OF_CHANNELS];				// 0x0100 - 0x013C
	volatile unsigned int HSI_IPC_DATA_MASK[HSI_NUMBER_OF_CHANNELS];		// 0x0140 - 0x017C
	volatile unsigned int CHNL_FIFO_STATUS[HSI_NUMBER_OF_CHANNELS];			// 0x0180 - 0x01BC
	unsigned int Reserved1[HSI_NUMBER_OF_CHANNELS];							//filler
	volatile unsigned int CHNL_STATUS[HSI_NUMBER_OF_CHANNELS];				// 0x0200 - 0x023C
	volatile unsigned int CHNL_INT_MASK[HSI_NUMBER_OF_CHANNELS];			// 0x0240 - 0x027C
	volatile unsigned int CHNL_INT_STATUS[HSI_NUMBER_OF_CHANNELS];			// 0x0280 - 0x02BC
	unsigned int Reserved2[16];												//filler
	volatile unsigned int HSI_INT_MASK[4];									// 0x0300 - 0x030C
	volatile unsigned int HSI_INT_STATUS[4];								// 0x0310 - 0x031C
	unsigned int Reserved3[56];												//filler
	volatile unsigned int RX_DMA_NDPTR[HSI_NUMBER_OF_CHANNELS];				// 0x0400 - 0x043C
	volatile unsigned int RX_DMA_DADR[HSI_NUMBER_OF_CHANNELS];				// 0x0440 - 0x047C
	volatile unsigned int RX_DMA_DLEN[HSI_NUMBER_OF_CHANNELS];				// 0x0480 - 0x04BC
	volatile unsigned int RX_DMA_CTRL[HSI_NUMBER_OF_CHANNELS];				// 0x04C0 - 0x04FC
	volatile unsigned int TX_DMA_NDPTR[HSI_NUMBER_OF_CHANNELS];				// 0x0500 - 0x053C
	volatile unsigned int TX_DMA_SADR[HSI_NUMBER_OF_CHANNELS];				// 0x0540 - 0x057C
	volatile unsigned int TX_DMA_DLEN[HSI_NUMBER_OF_CHANNELS];				// 0x0580 - 0x05BC
	volatile unsigned int TX_DMA_CTRL[HSI_NUMBER_OF_CHANNELS];				// 0x05C0 - 0x05FC
 	volatile unsigned int DMA_ARB_MODE; 									// 0x0600
	unsigned int Reserved4[255]; 											//filler
	volatile HSI_8BYTE_DATA_FIFO TX_FIFO[HSI_NUMBER_OF_CHANNELS];			// 0x0A00 - 0x0A78
	unsigned int Reserved5[32]; 											//filler
	volatile HSI_8BYTE_DATA_FIFO RX_FIFO[HSI_NUMBER_OF_CHANNELS];			// 0x0B00 - 0x0B78
} HSI_REGISTERS_T, *P_HSI_REGISTERS_T;

//connection status list
typedef enum
{
    HSI_UNINITIALIZED,	//indicates either handshake hasn't started or there was an error
	HSI_HANDSHAKE,		//indicates the handshake is in progress
    HSI_IDLE,			//indicates handshake is done and waiting for an image DL
	HSI_TRANSFER,		//indicates an image DL is in progress
	HSI_ERROR_STATE
} CONNECTION_STATUS;

// HSI PROPERTIES
typedef struct
{
    volatile CONNECTION_STATUS	connection_status;	//connection status
	volatile unsigned int 		handshake_index;	//current step into the HandShake
	volatile unsigned int 		buffer_address; 	// Current TX data address
    volatile unsigned int 		size_left; 			// Remaining size of TX data
    volatile unsigned int 		pdu_size_left; 		// Remaining size of current PDU TX data;
	P_HSI_REGISTERS_T			pHSIRegisters;
} HSI_Properties_T, *P_HSI_Properties_T;

/*  Function prototypes  */
P_HSI_Properties_T GetHSIProperties();
void HSI_Init();
void HSI_Reset_Fifos();
unsigned int HSI_Start();
unsigned int HSI_SendImage (unsigned int address, unsigned int size);
unsigned int HSI_CheckImage();
void HSI_Shutdown();
void HSI_ISR();
void HSI_ISR_COMMAND_CHNL();
void HSI_ISR_DATA_CHNL();
void HSI_Start_Data_Tx();
unsigned int HSI_SendCmd(P_MIPI_Command_T);
unsigned int HSI_Handshake();
P_MIPI_Command_T Get_HSI_Handshake_Cmd(unsigned int index);
CONNECTION_STATUS Get_HSI_Connection_Status();
void Set_HSI_Connection_Status(CONNECTION_STATUS);

#endif


