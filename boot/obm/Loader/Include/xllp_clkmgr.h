/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __XLLP_CLKMGR_H__
#define __XLLP_CLKMGR_H__

/**
**  FILENAME:   xllp_clkmgr.h
**
**  PURPOSE:    contains all XLLP CLKMGR typedefs and bit definitions..
**              Valid for processor codenamed Bulverde, stepping B0
**
******************************************************************************/

#include "xllp_defs.h"

#define CLOCK_CTL_BASE_ADDR		0x41340000

//
// Current to Bulverde EAS 3.0
//


/// Clock Manager (CLKMGR) Register Bank
typedef struct
{
    XLLP_VUINT32_T    accr;         	///< Application Subsystem Clock Configure register
    XLLP_VUINT32_T    acsr;         	///< Application Subsystem Clock Status register
    XLLP_VUINT32_T    aicsr;         	///< Application Subsystem interrupt Control/Status register
    XLLP_VUINT32_T    d0cken_a;        	///< D0 Mode Clock Enable register A
	XLLP_VUINT32_T	  d0cken_b;			///< D0 Mode Clock Enable register B
	XLLP_VUINT32_T	  ac97_div;			///< AC97 clock divisor value register

 } XLLP_CLKMGR_T, *P_XLLP_CLKMGR_T;  


/// Clock Enable Register (CLKEN) Enum
typedef enum
{
	XLLP_CLK_LCD = 1, 		///< LCD Clock Enable
	XLLP_CLK_USBH,			///< USB host clock enable
	XLLP_CLK_CAMERA,		///< Camera interface clock enable
	XLLP_CLK_NAND,			///< NAND Flash Controller Clock Enable
	XLLP_CLK_USB2=6,		///< USB 2.0 client clock enable.
	XLLP_CLK_GRAPHICS,		///< Graphics controller clock enable
	XLLP_CLK_DMC,			///< Dynamic Memory Controller clock enable
	XLLP_CLK_SMC,			///< Static Memory Controller clock enable
	XLLP_CLK_ISC,			///< Internal SRAM Controller clock enable
	XLLP_CLK_BOOT,			///< Boot rom clock enable
	XLLP_CLK_MMC0,			///< MMC0 Clock enable
	XLLP_CLK_MMC1,			///< MMC1 clock enable
	XLLP_CLK_KEYPAD,			///< Keypand Controller Clock Enable
	XLLP_CLK_CIR,			///< Consumer IR Clock Enable
	XLLP_CLK_USIM0=17,		///< USIM[0] Clock Enable
	XLLP_CLK_USIM1,			///< USIM[1] Clock Enable
	XLLP_CLK_TPM,			///< TPM clock enable
	XLLP_CLK_UDC,			///< UDC clock enable
	XLLP_CLK_BTUART,		///< BTUART clock enable
	XLLP_CLK_FFUART,		///< FFUART clock enable
	XLLP_CLK_STUART,		///< STUART clock enable
	XLLP_CLK_AC97,			///< AC97 clock enable
	XLLP_CLK_TOUCH,			///< Touch screen Interface Clock Enable
	XLLP_CLK_SSP1,			///< SSP1 clock enable
	XLLP_CLK_SSP2,			///< SSP2 clock enable
	XLLP_CLK_SSP3,			///< SSP3 clock enable
	XLLP_CLK_SSP4,			///< SSP4 clock enable
	XLLP_CLK_MSL0,			///< MSL0 clock enable

	XLLP_CLK_PWM0=32,		///< PWM[0] clock enable
	XLLP_CLK_PWM1,			///< PWM[1] clock enable
	XLLP_CLK_I2C=32+4,		///< I2C clock enable
	XLLP_CLK_INTC=32+6,		///< Interrupt controller clock enable
	XLLP_CLK_GPIO,			///< GPIO clock enable
	XLLP_CLK_1WIRE,			///< 1-wire clock enable
	XLLP_CLK_MINI_IM=32+16,	///< Mini-IM
	XLLP_CLK_MINI_LCD		///< Mini LCD

} XLLP_CLK_MODULE;



//
// Application Subsystem clock configuration register(ACCR) Bits
//


#define XLLP_ACCR_XPDIS			(0x1u << 31)	
#define XLLP_ACCR_SPDIS			(0x1u << 30)	
#define XLLP_ACCR_13MEND1		(0x1u << 27)
#define XLLP_ACCR_D0CS			(0x1u << 26)
#define XLLP_ACCR_13MEND2		(0x1u << 21)
#define XLLP_ACCR_PCCE			(0x1u << 11)


//Application Subsystem Clock Status Register (ACSR)
#define XLLP_ACSR_XPDIS_S		(0x1u << 31)
#define XLLP_ACSR_SPDIS_S		(0x1u << 30)
#define XLLP_ACSR_XPLCK			(0x1u << 29)
#define XLLP_ACSR_SPLCK			(0x1u << 28)
#define XLLP_ACSR_RO_S			(0x1u << 26)


//Application Subsystem Interrupt Control/Status Register(AICSR)
#define XLLP_AICSR_PCIS			(0x1u << 5)
#define XLLP_AICSR_PCIE			(0x1u << 4)
#define XLLP_AICSR_TCIS			(0x1u << 3)
#define XLLP_AICSR_TCIE			(0x1u << 2)
#define XLLP_AICSR_FCIS			(0x1u << 1)
#define XLLP_AICSR_FCIE			(0x1u << 0)

#ifdef __cplusplus
extern "C"
{
#endif

/**
	This routine reads the designated CLKCFG register via CoProcessor 14 

@return				CLKCFG register value
*/
extern unsigned int XllpReadClkCfg(void);
/**
	This routine Writes the designated CLKCFG register via CoProcessor 14 

@param	  aValue	Value to write to CLKCFG register
			
*/
extern void XllpWriteClkCfg( unsigned int aValue);

extern XLLP_BOOL_T XllpClockEnable(P_XLLP_CLKMGR_T  pReg, XLLP_CLK_MODULE module, XLLP_BOOL_T enable);
extern XLLP_ERROR_STATUS_T XllpClockSave(P_XLLP_CLKMGR_T  pVirtualAddress, P_XLLP_CLKMGR_T pSaveBuff);
extern XLLP_ERROR_STATUS_T XllpClockRestore(P_XLLP_CLKMGR_T  pVirtualAddress, P_XLLP_CLKMGR_T pSaveBuff);

#ifdef __cplusplus
}

#endif


#endif
