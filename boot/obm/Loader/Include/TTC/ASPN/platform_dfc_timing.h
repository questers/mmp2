/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by  
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:	PlatformConfig.h
**
**  PURPOSE: 	Platform specific header to set parameters
** 			
**                  
******************************************************************************/

//#include "dfc_defs.h"
#include "xllp_dfc_defs.h"

/*************** Flash Timing Override Examples ************************
* By default, very conservative timings are programmed - ie. BootROM
* These timings may be optimized.
* Below are some optimized flash timings for certain flash.
* Note that at it is impossible at the time of creation to cover all possible parts.
* Simply create a new structure for the desired part and call xdfc_setTiming in xllpdfcinit
*/

FLASH_TIMING SAMSUNG_MFG_TIMING[4] = 
{
	{10, 0, 20, 40, 20, 40, 0, 60, 10},   		// Small Block Full Speed
	{20, 10, 40, 80, 40, 80, 15000, 120, 20},  	// Small Block Conservative 
	{10, 35, 15, 25, 15, 25, 0, 60, 10}, 	  	// Large Block Full Speed
	{20, 70, 30, 50, 30, 50, 25000, 120, 20} 	// Large Block Conservative
};

// Micron MT29F1G08ABA, MT29F1G16ABA
FLASH_TIMING MICRON_MFG_TIMING[2] = 
{
	{10, 25, 15, 25, 15, 25, 0, 60, 10},   		// Large Block Full Speed
	{20, 50, 30, 50, 30, 50, 50000, 120, 20}  	// Large Block Conservative
};
