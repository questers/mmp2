;******************************************************************************
;**
;**  COPYRIGHT (C) 2007 Marvell Corporation.
;**	 All Rights Reserved
;**
;**  FILENAME:	Platform_defs.inc
;**
;**  PURPOSE: 	Platform defines required for staturp code
;**                  
;******************************************************************************
;
; HS35            EQU               1
;Define Processor variant for version information
Processor			EQU		   (0x4153504E)	; ASPN 
ProcessorSubType	EQU		   (0x00004130)	; A0

ISRAM_START			EQU (0xD1020000)
ISRAM_SIZE			EQU (0x00020000)	; 128k
L2_START 			EQU		0xD1020000
STACK_START 		EQU		0xD1028000
STACK_SIZE 			EQU		0x00002000
BL_STACK_START 		EQU		0xD1020000
BL_STACK_SIZE		EQU		0x00002800




;
; I/O pins common for all flash on DFI 
;
DFI_ND_IO0 EQU	(0xd401e088)
DFI_ND_IO1 EQU	(0xd401e084)
DFI_ND_IO2 EQU	(0xd401e080)
DFI_ND_IO3 EQU	(0xd401e07C)
DFI_ND_IO4 EQU	(0xd401e078)
DFI_ND_IO5 EQU	(0xd401e074)
DFI_ND_IO6 EQU	(0xd401e070)
DFI_ND_IO7 EQU	(0xd401e06C)
DFI_ND_IO8 EQU	(0xd401e068)
DFI_ND_IO9 EQU	(0xd401e064)
DFI_ND_IO10 EQU	(0xd401e060)
DFI_ND_IO11 EQU	(0xd401e05C)
DFI_ND_IO12 EQU	(0xd401e058)
DFI_ND_IO13 EQU	(0xd401e054)
DFI_ND_IO14 EQU	(0xd401e050)
DFI_ND_IO15 EQU	(0xd401e04C)
;
; SMEMC pins and registers for CS0
;
SM_nWE 		EQU	(0xd401e0A0)
SM_nOE		EQU (0xd401e0A4)
SM_NADV		EQU (0xd401e090)
SM_SCLK		EQU	(0xd401e0C0)
SM_nCS0		EQU	(0xd401e094)
SM_ADV		EQU	(0xd401e0AC)
SM_CSDFICFG0	EQU (0xD4283890)
SM_CSADRMAPx	EQU	(0xD42838C0)
;
; Values for MFPR 
;
VAL_DFI_ND_IO0 EQU	(0x1000)
VAL_DFI_ND_IO1 EQU	(0x1000)
VAL_DFI_ND_IO2 EQU	(0x1000)
VAL_DFI_ND_IO3 EQU	(0x1000)
VAL_DFI_ND_IO4 EQU	(0x1000)
VAL_DFI_ND_IO5 EQU	(0x1000)
VAL_DFI_ND_IO6 EQU	(0x1000)
VAL_DFI_ND_IO7 EQU	(0x1000)
VAL_DFI_ND_IO8 EQU	(0x1040)
VAL_DFI_ND_IO9 EQU	(0x1040)
VAL_DFI_ND_IO10 EQU	(0x1040)
VAL_DFI_ND_IO11 EQU	(0x1040)
VAL_DFI_ND_IO12 EQU	(0x1040)
VAL_DFI_ND_IO13 EQU	(0x1040)
VAL_DFI_ND_IO14 EQU	(0x1040)
VAL_DFI_ND_IO15 EQU	(0x1040)
VAL_SM_nWE 		EQU	(0x1000)
VAL_SM_nOE		EQU (0x1000)
VAL_SM_NADV		EQU (0x1000)
VAL_SM_SCLK		EQU	(0x1000)
VAL_SM_nCS0		EQU	(0x1003)
VAL_SM_ADV		EQU	(0x1000)
VAL_SM_CSDFICFG0	EQU (0x51890009)
VAL_SM_CSADRMAPx	EQU	(0x10000F00)

nCS0_OFFSET		EQU (0x80000000)

      END
