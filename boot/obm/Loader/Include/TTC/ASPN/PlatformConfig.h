/******************************************************************************
** (C) Copyright 2007 Marvell International Ltd.  
**  All Rights Reserved
**
**  FILENAME:	PlatformConfig.h
**
**  PURPOSE: 	Platform specific header to set parameters
**
**
******************************************************************************/

#ifndef __platform_config_h
#define __platform_config_h

#include "Typedef.h"
#include "Errors.h"
#include "ProtocolManager.h"
#include "misc.h"
#include "GPIO.h"
//#include "GEU.h"
#include "general.h"
#include "HM2uart.h"
#include "xllp_ciu2d.h"
#include "smemc.h"
#include "CIU.h"
#include "PMUM.h"
#include "APBC.h"
#if BOOTROM
	#include "resume.h"
    #include "bootrom.h"
#endif
#include "sdmmc_api.h"
#include "SD.h"


#if ZYLONITE2
#define KEYID_SOFTWAREUPGRADEREQUEST 0x04000016		// this is the key with green phone on it.
#elif ASPENITE || WAYLAND
#define KEYID_SOFTWAREUPGRADEREQUEST 0x04000006		// this is the button labelled sw4
#if ASPNB0
#define VOLTAGE_0 0x3B1 // 945 mV
#define VOLTAGE_1 0x3E8 // 1000 mV
#define VOLTAGE_2 0x460 // 1120 mV
#else
#define VOLTAGE_0 0x3E8 // 1000 mV
#define VOLTAGE_1 0x44C // 1100 mV
#endif
#elif AVLITE
#define VOLTAGE_0 0x384 // 900 mV
#define VOLTAGE_1 0x3FA // 1018 mV
#define VOLTAGE_2 0x470 // 1136 mV
#define VOLTAGE_3 0x4E4 // 1254 mV
#define KEYID_SOFTWAREUPGRADEREQUEST 0x04000006		// FIXME. Not actual keypad value.
#endif

//Number of images to load

#define K_MAX_IMAGES_TO_LOAD_FROM_FLASH 2	// TIM, OBM
#define K_MAX_IMAGES_TO_LOAD_FROM_ROM   0
#define K_MAX_IMAGES_TO_LOAD   (K_MAX_IMAGES_TO_LOAD_FROM_FLASH + K_MAX_IMAGES_TO_LOAD_FROM_ROM)
#define K_MAX_MEMORY_BOUNDS_CHECK_ENTRIES  (K_MAX_IMAGES_TO_LOAD + 1) // Add 1 for stack and heap in ISRAM

// Timers
#define APB_CU_ENABLE (*APB_CU_CR = 0x33)
#define TIMER_FREQ(A) (((A)*13) >> 2)

// SPI
#define spi_reg_bit_set(reg, val) reg_bit_set(reg, val)
#define spi_reg_bit_clr(reg, val) reg_bit_clr(reg, val)
#define APBC_SSP2_CLK_RST	(volatile unsigned int *)(APBC_BASE+0x820)
#define APBC_SSP5_CLK_RST	(volatile unsigned int *)(APBC_BASE+0x85C)
//force SSP_BASE_FOR_SPI to the correct SSPx peripheral base
#define SSP_BASE_FOR_SPI		0xD401C000
#define SSP_RX_DMA_DEVICE		DMAC_SSP_1_RX
#define SSP_TX_DMA_DEVICE		DMAC_SSP_1_TX
//
// Clock in nanoseconds. Not likely to be changing all that often...
//
#define NAND_CONTROLLER_CLOCK  78 // By default it is 78 MHZ but can be configured to 156. OBM should set parameters based on 156
#define UARTCLK    14745600

//
// Clock in nanoseconds. Not likely to be changing all that often...
//

// Bit 19 of control register must be written as 0
#define DFC_INITIAL_CONTROL 0xCC021FFF
#define	DFC_INITIAL_TIMING0	0x003F3F3F
#define DFC_INITIAL_TIMING1 0x100080FF //tR set to 52.4 usec, enable wait_mode

#define TIMOffset_CS0 0x00000000 // offset for TIM on CS0

#define CHIPBOOTTYPE (volatile unsigned long *)GEU_FUSE_VAL_APCFG1


#define ASCR	(volatile unsigned long *)PMUM_CPSR //See PMUM.h
#define ARSR	(volatile unsigned long *)PMUM_CRSR
#define AD3SR	(volatile unsigned long *)PMUM_AWUCRS
#define AD3R	(volatile unsigned long *)PMUM_AWUCRM

#define APPS_PAD_BASE  0xd401e000
#define SMEMC_BASE	   SMC_BASE

// OneNand boots from CS0 only
#define FLEX_BOOTRAM_MAIN_BASE	CS0Base


//
// Multifunction Padring Structures
//
typedef struct
{
  volatile int *    registerAddr;
  unsigned int      regValue;
  unsigned int      defaultValue;
}CS_REGISTER_PAIR_S, *P_CS_REGISTER_PAIR_S;

// Bounds Checking Memory Allocation
typedef struct MemAllocEntry_S
{
	unsigned long StartAddress;
	unsigned long EndAddress;
}MemAllocEntry_T, *pMemAllocEntry_T;

typedef struct MemAllocList_S
{
	unsigned long    MaxEntries;
	unsigned long    ActiveEntries;
    MemAllocEntry_T  MemAllocEntry[K_MAX_MEMORY_BOUNDS_CHECK_ENTRIES];
}MemAllocList_T, *pMemAllocList_T;
//Prototypes
UINT_T ChipSelect2(void);
UINT_T ChipSelect0(void);
void ChipSelectDFC(void);
void RestoreDefaultConfig(void);
void SaveDefaultConfig(void);
void ChipSelectSPI( void );
void ChipSelectOneNAND( void );
void ChipSelectMsys( void );
UINT32 PlatformMdocRdyForXIPAccess( void );
#if BOOTROM
void PlatformResumeSetup(pQuick_Resume_Function *, pTim_Resume_Function *);
#endif
UINT_T getPlatformPortSelection(unsigned int *);
const UINT8_T* GetProbeOrder();
UINT_T PlatformUARTConfig(void);
UINT_T PlatformAltUARTConfig(void);
UINT_T PlatformSaveStateRequired(void);//UDC 1.1 Functions
UINT_T SetupSingleEndedPort2(void);
UINT_T PlatformUsbTransmit(UINT_T, UINT8_T*);
void PlatformUsbInit(UINT_T);
void PlatformUsbShutdown(void);
void PlatformUsbDmaHandler(void);
void PlatformUsbInterruptHandler(void);
//U2D Functions
void PlatformU2DInit(void);
UINT_T PlatformU2DTransmit(UINT_T, UINT8_T*, UINT_T);
void PlatformU2DShutdown(void);
void PlatformU2DInterruptHandler(void);

#if BOOTROM
//Fuse API wrapper
UINT_T ReadOemHashKeyFuseBits(UINT_T* pbuffer, UINT_T size);
#endif

// Platform specific check for Vid and Pid in fuses
UINT_T GetUSBIDFuseBits(unsigned short* VID, unsigned short* PID );

// Platform specific JTAG
UINT_T JTAGEnable(pTIM pTIM_h);

//CI2 USB Functions
void PlatformCI2Init(void);
UINT_T PlatformCI2Transmit(UINT_T, UINT8_T*, UINT_T);
void PlatformCI2Shutdown(void);
void PlatformCI2InterruptHandler(void);

// DCB added these to get things compiling
//------------------------------------------
#define PLATFORM_CHIPBOOTTYPE_FUSE_REG 2
UINT_T CheckSuperSetPortEnablement(UINT_T Port);
UINT_T GetPlatformFuses(UINT_T platformFuses, pFUSE_SET pTBR_Fuse);
void CheckDefaultClocks(void);
UINT_T ReturnServicesFuses(UINT_T ServiceNumber);
UINT_T PlatformUARTConfig(void);
UINT_T PlatformAltUARTConfig(void);

// MMC
CONTROLLER_TYPE ConfigureMMC(UINT8_T FlashNum, UINT_T *pBaseAddress, UINT_T *pInterruptMask, UINT_T *FusePartitionNumber);
void DisableMMCSlots(void);

//external prototypes

extern UINT_T GetOSCR0(void);
extern UINT_T OSCR0IntervalInSec(UINT_T Before, UINT_T After);
extern UINT_T OSCR0IntervalInMilli(UINT_T Before, UINT_T After);
extern UINT_T OSCR0IntervalInMicro(UINT_T Before, UINT_T After);

/*
*****************************************************************************************
** GEU Fuse Library code used for ASPEN
**
** Since this is the only fnction required it has been cut from the the Library code and
** put here.
** Notes:
** 1. The OEM Hash Key is stored in RKEK Fuse Block (Fuse Block 1)
** 2. ASPEN has no GEU so the OEM Hash Key registers (originally RKEK)
**    are located in the CIU address space.
**
*****************************************************************************************
*/

// OEM Hash register addresses
#define CIU_BASE                    0xD4282C00

#define	GEU_FUSE_VAL_ROOT_KEY1		(CIU_BASE+0xA0)
#define	GEU_FUSE_VAL_ROOT_KEY2		(CIU_BASE+0xA4)
#define	GEU_FUSE_VAL_ROOT_KEY3		(CIU_BASE+0xA8)
#define	GEU_FUSE_VAL_ROOT_KEY4		(CIU_BASE+0xAC)
#define	GEU_FUSE_VAL_ROOT_KEY5		(CIU_BASE+0xB0)
#define	GEU_FUSE_VAL_ROOT_KEY6		(CIU_BASE+0xB4)
#define	GEU_FUSE_VAL_ROOT_KEY7		(CIU_BASE+0xB8)
#define	GEU_FUSE_VAL_ROOT_KEY8		(CIU_BASE+0xBC)

// GEU Register access macros
#define GEU_REG_WRITE(regAddress, wval) \
        ( (* ( (volatile UINT32*)(regAddress) ) ) = wval);

#define GEU_REG_READ(regAddress, rval) \
        rval = (* ( (volatile UINT32*)(regAddress)));


#define K_SHA1_SIZE     		 	20
#define K_SHA256_SIZE	        	32

// OEM Hash Key Function Prototype
UINT_T GEU_ReadOemHashKeyFuseBits(UINT_T* pbuffer, UINT_T size);

void PlatformI2CClocksEnable();

#endif //__platform_config_h
