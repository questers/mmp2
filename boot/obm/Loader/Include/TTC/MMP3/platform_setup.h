/******************************************************************************
**	(C) Copyright 2007 Marvell International Ltd.  
**	All Rights Reserved
******************************************************************************/

#ifndef __platform_setup_h
#define __platform_setup_h

/*
 *  Slave Address definitions for I2C devices
 */

#if (ZYLONITE)
#define GPIO_EX_I2C_WRITE_SLAVE_ADDRESS          0xEA
#define GPIO_EX_I2C_READ_SLAVE_ADDRESS           0xEB
#endif
#if (LITTLETON)
#define GPIO_EX_I2C_WRITE_SLAVE_ADDRESS          0xA0
#define GPIO_EX_I2C_READ_SLAVE_ADDRESS           0xA1
#endif

/*
 * Prototypes
 */
UINT_T PlatformPreFlashSetup(void);
UINT_T PlatformPreCodeSetup(pTIM pTIM_h, P_FlashProperties_T pFlashProp);
UINT_T PlatformPostCodeSetup(pTIM pTIM_h, P_FlashProperties_T pFlashProp);
I2C_ReturnCode PMICSetup ( void );
I2C_ReturnCode ResetULPI ( void );
#endif



