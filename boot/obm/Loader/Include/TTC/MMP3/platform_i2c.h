/*--------------------------------------------------------------------------------------------------------------------
(C) Copyright 2006, 2007 Marvell DSPC Ltd. All Rights Reserved.
-------------------------------------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------------------------------------------------
INTEL CONFIDENTIAL
Copyright 2006 Intel Corporation All Rights Reserved.
The source code contained or described herein and all documents related to the source code (�Material�) are owned
by Intel Corporation or its suppliers or licensors. Title to the Material remains with Intel Corporation or
its suppliers and licensors. The Material contains trade secrets and proprietary and confidential information of
Intel or its suppliers and licensors. The Material is protected by worldwide copyright and trade secret laws and
treaty provisions. No part of the Material may be used, copied, reproduced, modified, published, uploaded, posted,
transmitted, distributed, or disclosed in any way without Intel�s prior express written permission.

No license under any patent, copyright, trade secret or other intellectual property right is granted to or
conferred upon you by disclosure or delivery of the Materials, either expressly, by implication, inducement,
estoppel or otherwise. Any license under such intellectual property rights must be express and approved by
Intel in writing.
-------------------------------------------------------------------------------------------------------------------*/
#define I2C_IBMR_REG                0x0000  //
#define I2C_IDBR_REG                0x0008  // Data Byte Register
#define I2C_ICR_REG                 0x0010  // Control Register
#define I2C_ISR_REG                 0x0018  // Status Register
#define I2C_ISAR_REG                0x0020  // Slave Address Register

#define	TWSIPWR_BASE_REGS			0xd4011000	// used to drive the i2c bus designed for connection to a pmic.
#define TWSICI_BASE_REGS			0xd4031000	// used to drive the i2c bus designed for connection to a ci.
#define TWSIGI_BASE_REGS			0xd4032000	// used to drive the i2c bus designed for connection to a ci.
#define	TWSI1_BASE_REGS				0xd4011000	// used to drive the i2c bus designed for connection to a pmic.
#define TWSI2_BASE_REGS				0xd4031000	// used to drive the i2c bus designed for connection to a ci.
#define TWSI3_BASE_REGS				0xd4032000	// used to drive the i2c bus designed for connection to a ci.
#define TWSI4_BASE_REGS				0xd4033000	// generic i2c bus
#define TWSI5_BASE_REGS				0xd4033800	// another generic i2c bus
#define TWSI6_BASE_REGS				0xd4034000	// yet another generic i2c bus. don't they know this is a bus?

