/******************************************************************************
** (C) Copyright 2007 Marvell International Ltd.  
**  All Rights Reserved
**
**  FILENAME:	PlatformConfig.h
**
**  PURPOSE: 	Platform specific header to set parameters
**
**
******************************************************************************/

#ifndef __platform_config_h
#define __platform_config_h

#include "Typedef.h"
#if BOOTROM
#include "BootROM.h"
#endif
#include "Errors.h"
#include "ProtocolManager.h"
#include "misc.h"
#include "GPIO.h"
//#include "GEU.h"
//#include "geu_interface.h"
#include "general.h"
#include "HM2uart.h"
#include "xllp_ciu2d.h"
#include "smemc.h"
#include "CIU.h"
#include "PMUM.h"
#include "APBC.h"
#include "resume.h"
#include "sdmmc_api.h"
#include "SD.h"
#include "interrupts.h"

//Number of images to load
#define K_MAX_IMAGES_TO_LOAD_FROM_FLASH 4	// TIM, WTM, OBM, TZSW, plus MONITOR
#define K_MAX_IMAGES_TO_LOAD_FROM_ROM   1
#define K_MAX_IMAGES_TO_LOAD   (K_MAX_IMAGES_TO_LOAD_FROM_FLASH + K_MAX_IMAGES_TO_LOAD_FROM_ROM)
#define K_MAX_MEMORY_BOUNDS_CHECK_ENTRIES  (K_MAX_IMAGES_TO_LOAD + 1) // Add 1 for stack and heap in ISRAM

#define KEYID_SOFTWAREUPGRADEREQUEST 0x04000033		// this is the top left key of the TTC EVB keypad

// Timers
#define APB_CU_ENABLE (*APB_CU_CR = 0x33) 		// 26 MHz
#define TIMER_FREQ(A) ((A)*26)       				// in MHz

// MMC5 / ICU
#define INT_55_MASK_REGISTER	(ICU_BASE+0x17C)

// SPI
#define spi_reg_bit_set(reg, val)
#define spi_reg_bit_clr(reg, val)

//force SSP_BASE_FOR_SPI to the correct SSPx peripheral base
#define SSP_BASE_FOR_SPI		SSP1_BASE

#define SSP_RX_DMA_DEVICE		DMAC_SSP_1_RX
#define SSP_TX_DMA_DEVICE		DMAC_SSP_1_TX

//
// Clock in nanoseconds. Not likely to be changing all that often...
//
#define NAND_CONTROLLER_CLOCK  78  // MHz
#define UARTCLK    14745600

//
// Clock in nanoseconds. Not likely to be changing all that often...
//

// Bit 19 of control register must be written as 0
#define DFC_INITIAL_CONTROL 0xCC021FFF
#define	DFC_INITIAL_TIMING0	0x003F3F3F
#define DFC_INITIAL_TIMING1 0x100080FF //tR set to 52.4 usec

#define TIMOffset_CS0 0x00000000 // offset for TIM on CS0

#define CHIPBOOTTYPE (volatile unsigned long *)GEU_FUSE_VAL_APCFG1

#define APPS_PAD_BASE  0xd401e000
#define SMEMC_BASE	   SMC_BASE

// OneNand boots from CS0 only
#define FLEX_BOOTRAM_MAIN_BASE	CS2Base


//
// Multifunction Padring Structures
//
typedef struct
{
  volatile int *    registerAddr;
  unsigned int      regValue;
  unsigned int      defaultValue;
}CS_REGISTER_PAIR_S, *P_CS_REGISTER_PAIR_S;

// Bounds Checking Memory Allocation
typedef struct MemAllocEntry_S
{
	unsigned long StartAddress;
	unsigned long EndAddress;
}MemAllocEntry_T, *pMemAllocEntry_T;

typedef struct MemAllocList_S
{
	unsigned long    MaxEntries;
	unsigned long    ActiveEntries;
    MemAllocEntry_T  MemAllocEntry[K_MAX_MEMORY_BOUNDS_CHECK_ENTRIES];
}MemAllocList_T, *pMemAllocList_T;

//Prototypes
void PlatformI2CClocksEnable();
UINT_T ChipSelect2(void);
UINT_T ChipSelect0(void);
void ChipSelectDFC(void);
void RestoreDefaultConfig(void);
void SaveDefaultConfig(void);
void ChipSelectSPI( void );
void ChipSelectHSI( void );
void RestoreHSIPins( void );
void ChipSelectOneNAND( void );
void ChipSelectMsys( void );
UINT32 PlatformMdocRdyForXIPAccess( void );
void PlatformResumeSetup(pQuick_Resume_Function *, pTim_Resume_Function *);
UINT_T getPlatformPortSelection(unsigned int *);
const UINT8_T* GetProbeOrder(void);
UINT_T PlatformUARTConfig(void);
UINT_T PlatformAltUARTConfig(void);
//UDC 1.1 Functions
UINT_T SetupSingleEndedPort2(void);
UINT_T PlatformUsbTransmit(UINT_T, UINT8_T*);
void PlatformUsbInit(UINT_T);
void PlatformUsbShutdown(void);
void PlatformUsbDmaHandler(void);
void PlatformUsbInterruptHandler(void);
//U2D Functions
void PlatformU2DInit(void);
UINT_T PlatformU2DTransmit(UINT_T, UINT8_T*, UINT_T);
void PlatformU2DShutdown(void);
void PlatformU2DInterruptHandler(void);

//Fuse API wrapper
UINT_T ReadOemHashKeyFuseBits(UINT_T* pbuffer, UINT_T size);

// Platform specific JTAG
UINT_T JTAGEnable(pTIM pTIM_h);

// Platform specific check for Vid and Pid in fuses
UINT_T GetUSBIDFuseBits(unsigned short* VID, unsigned short* PID );

//CI2 USB Functions
void PlatformCI2Init(void);
UINT_T PlatformCI2Transmit(UINT_T, UINT8_T*, UINT_T);
void PlatformCI2Shutdown(void);
void PlatformCI2InterruptHandler(void);

// DCB added these to get things compiling
//------------------------------------------
#define PLATFORM_CHIPBOOTTYPE_FUSE_REG 2
UINT_T CheckSuperSetPortEnablement(UINT_T Port);
UINT_T  GetPlatformFuses(UINT_T platformFuses, pFUSE_SET pTBR_Fuse);
void CheckDefaultClocks(void);
UINT_T ReturnServicesFuses(UINT_T ServiceNumber);
UINT_T PlatformUARTConfig(void);
UINT_T PlatformAltUARTConfig(void);

// MMC
CONTROLLER_TYPE ConfigureMMC(UINT8_T FlashNum, UINT_T *pBaseAddress, UINT_T *pInterruptMask, UINT_T *FusePartitionNumber);
void DisableMMCSlots(void);

//external prototypes

extern UINT_T GetOSCR0(void);
extern UINT_T OSCR0IntervalInSec(UINT_T Before, UINT_T After);
extern UINT_T OSCR0IntervalInMilli(UINT_T Before, UINT_T After);
extern UINT_T OSCR0IntervalInMicro(UINT_T Before, UINT_T After);

#endif
