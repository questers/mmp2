/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into
 *  products for purposes authorized by the license agreement provided they
 *  include this notice and the associated copyright notice with any such
 *  product.
 *  The information in this file is provided "AS IS" without warranty.
 *
 *  Platform Interrupts.h
 ******************************************************************************/

#ifndef _PLATFORM_INTERRUPTS_H_
#define _PLATFORM_INTERRUPTS_H_

#define	CORE_AP		0x0
#define	CORE_CP		0x1

#define IPC_PRIORITY 	15
#define	DMA_PRIORITY	15
#define	BROM_PRIORITY	15

// definition for bit 4 of ICU_INT_0_63_CONF
#define	FIR_INT		0x0
#define	IRQ_INT		0x1

#define	INT_NUMS	64


// MMP2 ICU Hardware Interrupt Assignment
//			Description		INT_REQ#

#define		INT_SSP1		0
#define		INT_SSP2		1
#define		INT_SSPA1		2		//(HiFi)
#define		INT_SSPA2		3
#define		INT_PMIC		4
#define		INT_RTC_1HZ		5
#define		INT_RTC_ALARM	6
#define		INT_TWSI1		7
#define		INT_GC600		8
#define		INT_KEYPAD		9
#define		INT_ROTARY		10
#define		INT_TRACKBALL	11
#define		INT_1WIRE 		12		//Battery Monitor (OneWire)
#define		INT_TMR1		13		//timer_1_irq
#define		INT_TMR2		14		//timer_2_irq
#define		INT_TMR3		15		//timer_3_irq
#define		INT_RIPC		16

#define		INT_TWSI2_6		17		// Ored	TWSI 2-6
#define		INT_PJ4_PERF	18
#define		INT_HDMI		19
#define		INT_SSP3		20
#define		INT_SSP4		21
#define		INT_USB_HSIC1	22
#define		INT_USB_HSIC2	23
#define		INT_UART3		24
#define		INT_DDR_L2_IRQ	25
#define		INT_DDR_L2_FIQ	26
#define		INT_UART1		27
#define		INT_UART2		28
#define		INT_MIPI_DSI	29
#define		INT_CCIC2		30
#define		INT_PMU_TMR1	31
#define		INT_PMU_TMR2	32

#define		INT_PMU_TMR3	33
#define		INT_USB_FSIC	34
#define		INT_WDT			35
#define		INT_MAIN_PMU	36
#define		INT_NAND_SMEM	37
#define		INT_USIM		38
#define		INT_MMC1		39
#define		INT_WTM			40
#define		INT_LCD_INTF1	41
#define		INT_CCIC1		42
#define		INT_IRE			43
#define		INT_USB			44		// USB-OTG
#define		INT_NAND		45
#define		INT_UART4		46		// need double check
#define		INT_DMA_FIQ		47
#define		INT_DMA_IRQ		48
#define		INT_GPIO		49
#define		INT_JTAG		50

#define		INT_MMC2		52
#define		INT_MMC3		53
#define		INT_MMC4		54
#define		INT_MMC5		55
#define		INT_MIPI_HSI	55

#define		INT_XD_CARD		57
#define		INT_MSP_CARD	58
#define		INT_MIPI_SB_DMA	59
#define		INT_PJ_FRQ_CHG	60
#define		INT_FAB_TO		61		//Fabric timeout ( 1,2,3,4)
#define		INT_MIPI_SBUS	62
#define		INT_PIN_MUX		63		// INT_SM from PinMux



// Interrupt translation table for TTC
//----------------------------------------
#define USB0_OTG_INT (INT_USB)
#define FFUART_INT (INT_UART1)
#define USB_CLIENT_INT (INT_USB)
#define U2D_CLIENT_INT (INT_USB)
#define DMA_CNTL_INT (INT_DMA_IRQ)

//Core determination
#define PJ4_PART_NUMBER 0x581
typedef enum
{
	invalid_core	= 0,
	Dragonite_core	= 1,
	PJ4_core		= 2
} CoreType;

//Prototypes
void DetermineCore();

//Prototypes
unsigned char GetPortInterruptFlag(void);
void ClearPortInterruptFlag(void);
unsigned int EnablePeripheralIRQInterrupt(unsigned int InterruptID);
unsigned int DisablePeripheralIRQInterrupt(unsigned int InterruptID);
void INT_init(void);
void IRQ_Glb_Ena(void);
unsigned int GetIrqStatus(void);
#endif /* _BU_INTERRUPT_H_ */
