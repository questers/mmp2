/******************************************************************************
**  COPYRIGHT  2010 Marvell Inernational Ltd.
**  All Rights Reserved
******************************************************************************/

#ifndef __TRUSTZONE_CONFIG_H__
#define __TRUSTZONE_CONFIG_H__

void ConfigureTrustZone( void *pTIM );
void ConfigureDefaultTrustzone();
UINT_T FindTrustZonePackageInTim( void *pTIM );
UINT_T TzGetRegion(UINT_T region,  UINT_T ChipSelect, UINT_T* pBaseAddress, UINT_T* pSize, UINT_T* pPermission);
#endif

