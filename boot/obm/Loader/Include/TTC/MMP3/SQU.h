/******************************************************************************
**  COPYRIGHT  2010 Marvell Inernational Ltd.
**  All Rights Reserved
******************************************************************************/

// SQU.h:

#ifndef __SQU__
#define __SQU__

#include "predefines.h"

#define SQU_BASE	0xD42A0000

#define SQU_CTRL_0				((unsigned volatile long*)(SQU_BASE + 0x0000))  
#define SQU_CTRL_1				((unsigned volatile long*)(SQU_BASE + 0x0008))

#define SQU_SECURE_WRITE_BK0_MSK	  SHIFT22(0x1)
#define SQU_SECURE_READ_BK0_MSK		  SHIFT21(0x1)

#define SQU_SECURE_WRITE_BK1_MSK	  SHIFT6(0x1)
#define SQU_SECURE_READ_BK1_MSK		  SHIFT5(0x1)

#define SQU_SECURE_WRITE_BK2_MSK	  SHIFT22(0x1)
#define SQU_SECURE_READ_BK2_MSK		  SHIFT21(0x1)

  
#endif
