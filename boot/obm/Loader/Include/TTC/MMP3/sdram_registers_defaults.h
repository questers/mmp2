/******************************************************************************
**  COPYRIGHT  2007 Marvell Inernational Ltd.
**  All Rights Reserved
******************************************************************************/



#ifndef __SDRAM_REGISTERS_DEFAULTS_H__
#define __SDRAM_REGISTERS_DEFAULTS_H__

// default mcu register values for MMP2
#include "MCU.h"

    // SDRAM Configuration Register 0 - CS0, Type 1
	// offset 0x00000020
    // MMP2 value:      0x00001330
#define MMP2_SDRCFGREG0	 0x00001330

	// SDRAM Configuration Register 1 - CS0, Type 1
	// offset 0x00000030
	// MMP2 value:	0x00002330  (default per spec)
#define MMP2_SDRCFGREG1 0x00002330														 

	// SDRAM Timing Register 1
	// offset 0x00000050
	// MMP2 value:	0x4842009C
#define MMP2_SDRTMGREG1 0x4842009C

	// SDRAM Timing Register 2
	// offset 0x00000060
	// MMP2 value:	0x11111032
#define MMP2_SDRTMGREG2 	0x11111032		

	// SDRAM Timing Register 3
	// offset 0x00000190
	// MMP2 value:	0xC0C81CFA (default per spec)
#define MMP2_SDRTMGREG3 0xC0C81CFA

	// SDRAM Timing Register 4
	// offset 0x000001c0
	// MMP2 value:	0x3282B050 (default per spec)
#define MMP2_SDRTMGREG4  0x3282B050 

	// SDRAM Timing Register 5
	// offset 0x00000650
	// Morona value:	0x001000A1 (default per spec)
#define MMP2_SDRTMGREG5	0x001000A1 

	// SDRAM Control Register 1
	// offset 0x00000080
	// MMP2 value:	0x00000000 (default per spec)
#define MMP2_SDRCTLREG1	 0x0

	// SDRAM Control Register 2
	// offset 0x00000090
	// MMP2 value:	0x00080000 (default per spec)
#define MMP2_SDRCTLREG2  0x00080000

	// SDRAM Control Register 3
	// offset 0x000000f0
	// MMP2 value:	0x80000001
#define MMP2_SDRCTLREG3 0x80000001

	// SDRAM Control Register 4
	// offset 0x000001a0
	// MMP2 value:	0x00810005  (default per spec)
#define MMP2_SDRCTLREG4  0x00810005 

	// SDRAM Control Register 5
	// offset 0x00000280
	// MMP2 value:	0x01010101 (default per spec)
#define MMP2_SDRCTLREG5	0x01010101

	// SDRAM Control Register 6
	// offset 0x00000760
	// MMP2 value:	0x00000000 (default per spec)
#define MMP2_SDRCTLREG6  0x00000000

	// SDRAM Control Register 7
	// offset 0x00000770
	// MMP2 value: 0x00000000  (default per spec)
#define MMP2_SDRCTLREG7	0x00000000

	// SDRAM Control Register 8
	// offset 0x00000780
	// MMP2 value: 0x00000133  (default per spec)
#define MMP2_SDRCTLREG8	0x00000133

	// MCB Control Register 1
	// offset 0x00000580
	// MMP2 value:	0x00000000  (default per spec)
#define MMP2_MCBCTLREG1	0x00000000

	// PHY Control Register 3
	// offset 0x00000140
	// MMP2 value:	0x000050A0  (default per spec)
#define MMP2_PHYCTLREG3	0x20004411

	// PHY Control Register 7
	// offset 0x000001d0
	// MMP value:	0x03300331  (default per spec)
#define MMP2_PHYCTLREG7 	0x03300331

	// PHY Control Register 8
	// offset 0x000001e0
	// MMP2 value:	0x03300770 (default per spec)
#define MMP2_PHYCTLREG8	0x03300770

    // PHY Control Register 9
	// offset 0x000001f0
	// MMP2 value:	0x00000077 (default per spec)
#define MMP2_PHYCTLREG9	0x00000077

	// PHY Control Register 11
	// offset 0x00000210
	// MMP2 value:	0x00300003 (default per spec)
#define MMP2_PHYCTLREG11	0x00300003

	// PHY Control Register 13
	// offset 0x00000230
	// MMP2 value:	0x20000104 (default per spec)
#define MMP2_PHYCTLREG13	0x20000104

	// PHY Control Register 14
	// offset 0x00000240
	// MMP2 value:	0x00000000
#define MMP2_PHYCTLREG14	0x00000000

	// FIXME: where's the settings for dq[7:0]?
	// PHY DLL Control Register 1
	// offset 0x00000e10
	// MMP2 value:	0x00007D04  (default per spec)
#define MMP2_DLLCTLREG1	0x00007D04

	// PHY DLL Control Register 2
	// offset 0x00000e20
	// MMP2 value:	0x00007D04 (default per spec)
#define MMP2_DLLCTLREG2	0x00007D04

	// PHY DLL Control Register 3
	// offset 0x00000e30
	// MMP2 value:	0x00007D04  (default per spec)
#define MMP2_DLLCTLREG3	0x00007D04

	// Mememory Address Map Register 0
	// offset 0x00000100
	// MMP2 value:	0x00000000 (default per spec)
#define MMP2_ADRMAPREG0	0x000a0701

	// Mememory Address Map Register 1
	// offset 0x00000110
	// MMP2 value: 0x00000000 (default per spec)
#define MMP2_ADRMAPREG1 0x100a0701

	// User Initiated Command Register 0
	// offset 0x00000120
	// MMP2 value:	0x00000001
#define MMP2_USRCMDREG0	0x00000001

// Default Trust Zone Register values for MMP2

   // TrustZone Select Register
   // offset 0x000003B0
   // MMP2 value:  0x0
#define MMP2_TRUSTZONE_SELECT_REG_DEFAULT 0x0

   // TrustZone Range0 Register
   // offset 0x000003C0
   // MMP2 value:  0x0
#define MMP2_TRUSTZONE_RANGE0_REG_DEFAULT 0x0

   // TrustZone Range1 Register
   // offset 0x000003D0
   // MMP2 value:  0x0
#define MMP2_TRUSTZONE_RANGE1_REG_DEFAULT 0x0

   // TrustZone Permission Register
   // offset 0x000003E0
   // MMP2 value:  0x0
#define MMP2_TRUSTZONE_PERMISSION_REG_DEFAULT 0x0

#endif
