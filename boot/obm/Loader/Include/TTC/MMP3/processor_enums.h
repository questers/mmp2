/******************************************************************************
**  COPYRIGHT  2007 Marvell Inernational Ltd.
**  All Rights Reserved
******************************************************************************/

#ifndef __PROCESSOR_ENUMS_H__
#define __PROCESSOR_ENUMS_H__

typedef enum TRUSTZONE_REGID_E
{
	// Register IDs
	MMP3_TZSELECT_ID,
	MMP3_TZRANGE0_ID,
    MMP3_TZRANGE1_ID,
	MMP3_TZPERMISSION_ID
} TRUSTZONE_REGID_T;

typedef enum TRUSTZONE_SPEC_E
{
	// Field IDs
	TZLOCK_ID,        // Lock Trust Zones
	TZCS0_RANGE0_ID,  // Size of Region 0 (64K chunks)
	TZCS0_RANGE1_ID,  // Size of Region 1 (64K chunks)
	TZCS0_RANGE2_ID,  // Size of Region 2 (64K chunks)
	TZCS0_RANGE3_ID,  // Size of Region 3 (64K chunks)
	TZCS0_PERMR0_ID,  // Access permission for Region 0 
	TZCS0_PERMR1_ID,  // Access permission for Region 1 
	TZCS0_PERMR2_ID,  // Access permission for Region 2
	TZCS0_PERMR3_ID,  // Access permission for Region 3 
	TZCS0_PERMRU_ID,  // Access permission for Region U
	TZ_SPEC_E_MAX
} TRUSTZONE_SPEC_T;
#endif

