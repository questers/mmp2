;******************************************************************************
;**
;**  COPYRIGHT (C) 2007 Marvell Corporation.
;**	 All Rights Reserved
;**
;**  FILENAME:	Platform_defs.inc
;**
;**  PURPOSE: 	Platform defines required for staturp code
;**                  
;******************************************************************************
;
; HS35            EQU               1
;Define Processor variant for version information
Processor			EQU		   (0x4d4d5033)	; MMP3
ProcessorSubType	EQU		   (0x00004130)	; A0


L2_START 		EQU		0xD1000000
STACK_START 	EQU		0xD104E000
STACK_SIZE 		EQU		0x00002000

	IF BOOTROM=0
; stack start is actuall stack bottom: the lowest stack address.
; make sure this is compatible with the settings in bl_platform.mak
BL_STACK_SIZE	EQU		0x00002800
	IF DDRBASE=1
BL_STACK_START	EQU		(0x00020000-BL_STACK_SIZE)
	ELSE
BL_STACK_START	EQU		(0xD1020000-BL_STACK_SIZE)
	ENDIF
	ENDIF


      END
