/******************************************************************************
** (C) Copyright 2007 Marvell International Ltd.  
**  All Rights Reserved
**
**  FILENAME:	pinmux.h
**
**  PURPOSE: 	pinmux related definitions
** 			
**                  
******************************************************************************/

#ifndef __PINMUX_H
#define __PINMUX_H

//
// Clock in nanoseconds. Not likely to be changing all that often...
//
#define	PULL_SEL	0x8000
#define	PULL_UP		0x4000
#define	PULL_DN		0x2000

#define	DRV_SLOW	0x0800
#define	DRV_MED		0x1000
#define	DRV_FAST	0x1800

#define	EDGE_DIS	0x0040

#define	AF_SEL0		0x0000
#define	AF_SEL1		0x0001
#define	AF_SEL2		0x0002
#define	AF_SEL3		0x0003
#define	AF_SEL4		0x0004
#define	AF_SEL5		0x0005
#define	AF_SEL6		0x0006
#define	AF_SEL7		0x0007


// 		Ball Name		0ffset 

#define	USIM_UCLK		0x0000       
#define	USIM_UIO		0x0004
#define	USIM_nURST		0x0008
#define	GPIO_124		0x000C
#define	GPIO_125		0x0010
#define	GPIO_126		0x0014
#define	GPIO_127		0x0018
#define	GPIO_128		0x001C
#define	GPIO_129		0x0020
#define	GPIO_130		0x0024
#define	GPIO_131		0x0028
#define	GPIO_132		0x002C
#define	GPIO_133		0x0030
#define	GPIO_134		0x0034
#define	GPIO_135		0x0038
#define	GPIO_136		0x003C
#define	GPIO_137		0x0040
#define	GPIO_138		0x0044
#define	GPIO_139		0x0048
#define	GPIO_140		0x004C
#define	GPIO_141		0x0050

#define	GPIO_00			0x0054
#define	GPIO_01			0x0058
#define	GPIO_02			0x005C
#define	GPIO_03			0x0060
#define	GPIO_04			0x0064
#define	GPIO_05			0x0068
#define	GPIO_06			0x006C
#define	GPIO_07			0x0070
#define	GPIO_08			0x0074
#define	GPIO_09			0x0078
#define	GPIO_10			0x007C
#define	GPIO_11			0x0080
#define	GPIO_12			0x0084
#define	GPIO_13			0x0088
#define	GPIO_14			0x008C
#define	GPIO_15			0x0090
#define	GPIO_16			0x0094
#define	GPIO_17			0x0098
#define	GPIO_18			0x009C
#define	GPIO_19			0x00A0
#define	GPIO_20			0x00A4
#define	GPIO_21			0x00A8
#define	GPIO_22			0x00AC
#define	GPIO_23			0x00B0
#define	GPIO_24			0x00B4
#define	GPIO_25			0x00B8
#define	GPIO_26			0x00BC
#define	GPIO_27			0x00C0
#define	GPIO_28			0x00C4
#define	GPIO_29			0x00C8
#define	GPIO_30			0x00CC
#define	GPIO_31			0x00D0
#define	GPIO_32			0x00D4
#define	GPIO_33			0x00D8
#define	GPIO_34			0x00DC
#define	GPIO_35			0x00E0
#define	GPIO_36			0x00E4
#define	GPIO_37			0x00E8
#define	GPIO_38			0x00EC
#define	GPIO_39			0x00F0
#define	GPIO_40			0x00F4
#define	GPIO_41			0x00F8
#define	GPIO_42			0x00FC
#define	GPIO_43			0x0100
#define	GPIO_44			0x0104
#define	GPIO_45			0x0108
#define	GPIO_46			0x010C
#define	GPIO_47			0x0110
#define	GPIO_48			0x0114
#define	GPIO_49			0x0118
#define	GPIO_50			0x011C
#define	GPIO_51			0x0120
#define	GPIO_52			0x0124
#define	GPIO_53			0x0128
#define	GPIO_54			0x012C
#define	GPIO_55			0x0130
#define	GPIO_56			0x0134
#define	GPIO_57			0x0138
#define	GPIO_58			0x013C
#define	TWSI1_SCL		0x0140
#define	TWSI1_SDA		0x0144
#define	GPIO_123		0x0148
#define	PRI_TDI			0x014C
#define	PRI_TMS			0x0150
#define	PRI_TCK			0x0154
#define	PRI_TDO			0x0158
#define	SLAVE_RESET_OUT	0x015C
#define	CLK_REQ			0x0160
#define	GPIO_114		0x0164
#define	VCXO_REQ		0x0168
#define	VCXO_OUT		0x016C

#define	GPIO_74			0x0170
#define	GPIO_75			0x0174
#define	GPIO_76			0x0178
#define	GPIO_77			0x017C
#define	GPIO_78			0x0180
#define	GPIO_79			0x0184
#define	GPIO_80			0x0188
#define	GPIO_81			0x018C
#define	GPIO_82			0x0190
#define	GPIO_83			0x0194
#define	GPIO_84			0x0198
#define	GPIO_85			0x019C
#define	GPIO_86			0x01A0
#define	GPIO_87			0x01A4
#define	GPIO_88			0x01A8
#define	GPIO_89			0x01AC
#define	GPIO_90			0x01B0
#define	GPIO_91			0x01B4
#define	GPIO_92			0x01B8
#define	GPIO_93			0x01BC
#define	GPIO_94			0x01C0
#define	GPIO_95			0x01C4
#define	GPIO_96			0x01C8
#define	GPIO_97			0x01CC
#define	GPIO_98			0x01D0
#define	GPIO_99			0x01D4
#define	GPIO_100		0x01D8
#define	GPIO_101		0x01DC
#define	ND_IO0			0x01E0
#define	ND_IO1			0x01E4
#define	ND_IO2			0x01E8
#define	ND_IO3			0x01EC
#define	ND_IO4			0x01F0
#define	ND_IO5			0x01F4
#define	ND_IO6			0x01F8
#define	ND_IO7			0x01FC
#define	ND_IO8			0x0200
#define	ND_IO9			0x0204
#define	ND_IO10			0x0208
#define	ND_IO11			0x020C
#define	ND_IO12			0x0210
#define	ND_IO13			0x0214
#define	ND_IO14			0x0218
#define	ND_IO15			0x021C
#define	ND_nCS0			0x0220
#define	ND_nCS1			0x0224
#define	SM_nCS0			0x0228
#define	SM_nCS1			0x022C
#define	ND_nWE_SM_nWE	0x0230
#define	ND_nRE_SM_nOE	0x0234
#define	ND_CLE			0x0238
#define	ND_ALE_SM_ADV	0x023C
#define	SM_SCLK			0x0240
#define	ND_RDY0			0x0244
#define	SM_nBE0			0x0248
#define	SM_nBE1			0x024C
#define	ND_RDY1			0x0250
#define	SM_INT			0x0254
#define	EXT_DMA_REQ0	0x0258
#define	SM_RDY			0x025C
#define	GPIO_115		0x0260
#define	GPIO_116		0x0264
#define	GPIO_117		0x0268
#define	GPIO_118		0x026C
#define	GPIO_119		0x0270
#define	GPIO_120		0x0274
#define	GPIO_121		0x0278
#define	GPIO_122		0x027C
#define	GPIO_59			0x0280
#define	GPIO_60			0x0284
#define	GPIO_61			0x0288
#define	GPIO_62			0x028C
#define	GPIO_63			0x0290
#define	GPIO_64			0x0294
#define	GPIO_65			0x0298
#define	GPIO_66			0x029C
#define	GPIO_67			0x02A0
#define	GPIO_68			0x02A4
#define	GPIO_69			0x02A8
#define	GPIO_70			0x02AC
#define	GPIO_71			0x02B0
#define	GPIO_72			0x02B4
#define	GPIO_73			0x02B8
#define	TWSI4_SCL		0x02BC
#define	TWSI4_SDA		0x02C0


			
//
// Multifunction Padring Structures
//
typedef struct 
{
  BU_U32	mfpr_addr;
  BU_U32	val;
  BU_U32    def_val;
} MFPR_S, *P_MFPR_S;


//Prototypes

void switch_alt_uart2_pinmux(void);
void switch_cw_pinmux(void);		// conflict with Kaypad pins
void switch_tv_pinmux(void);		// conflict with MMC1 pins

void mmp2_pinmux_set( void );

//external prototypes


#endif
