/******************************************************************************
**  COPYRIGHT  2007 Marvell Inernational Ltd.
**  All Rights Reserved
******************************************************************************/

// mcu_extras.h:
// these are definitions that should have been in mcu.h.
// remove these from here if they appear in updates to mcu.h


#ifndef __MCU_EXTRAS__
#define __MCU_EXTRAS__

//#define MCB_SLFST_SEL  (MCU_BASE+0x570)	//DELETED
//#define ERROR_ID       (MCU_BASE+0x490)	//DELETED
//#define ERROR_ADDR     (MCU_BASE+0x4A0)	//DELETED

#include "MCU.h"																								//Old/new
#define MCU_REG_DECODE_ADDR					((unsigned volatile long*)( CONFIG_DECODE_ADDR		   ))  //offset 0x010/004
#define MCU_REG_SDRAM_CONFIG_0				((unsigned volatile long*)( L1_SDRAM_CONFIG_TYPE1      ))  //offset 0x020/020
#define MCU_REG_SDRAM_CONFIG_1				((unsigned volatile long*)( L2_SDRAM_CONFIG_TYPE1      ))  //offset 0x030/024
#define MCU_REG_SDRAM_CONFIG_2				((unsigned volatile long*)( L3_SDRAM_CONFIG_TYPE1      ))  //offset 0x040/028
#define MCU_REG_SDRAM_CONFIG_3				((unsigned volatile long*)( L4_SDRAM_CONFIG_TYPE1	   ))  //offset 0xB300/02c
#define MCU_REG_SDRAM_CONFIG_4				((unsigned volatile long*)( L1_SDRAM_CONFIG_TYPE2      ))  //offset 0xB40/030
#define MCU_REG_SDRAM_CONFIG_5				((unsigned volatile long*)( L2_SDRAM_CONFIG_TYPE2      ))  //offset 0xB50/034
#define MCU_REG_SDRAM_CONFIG_6				((unsigned volatile long*)( L3_SDRAM_CONFIG_TYPE2      ))  //offset 0xB60/038
#define MCU_REG_SDRAM_CONFIG_7				((unsigned volatile long*)( L4_SDRAM_CONFIG_TYPE2      ))  //offset 0xB70/03C

#define MCU_REG_SDRAM_TIMING_1				((unsigned volatile long*)( SDRAM_TIMING1              ))  //offset 0x050/080
#define MCU_REG_SDRAM_TIMING_2				((unsigned volatile long*)( SDRAM_TIMING2              ))  //offset 0x060/084
#define MCU_REG_SDRAM_CONTROL_1				((unsigned volatile long*)( SDRAM_CTRL1                ))  //offset 0x080/050
#define MCU_REG_SDRAM_CONTROL_2				((unsigned volatile long*)( SDRAM_CTRL2                ))  //offset 0x090/054
#define MCU_REG_PHY_CONTROL_3				((unsigned volatile long*)( PHY_CTRL3                  ))  //offset 0x140/220
#define MCU_REG_SDRAM_TIMING_3				((unsigned volatile long*)( SDRAM_TIMING3              ))  //offset 0x190/088
//#define MCU_REG_SDRAM_CONTROL_3	 		((unsigned volatile long*)( SDRAM_CTRL3                ))  //offset 0x0f0/deleted
#define MCU_REG_SDRAM_CONTROL_4				((unsigned volatile long*)( SDRAM_CTRL4                ))  //offset 0x1a0/058
#define MCU_REG_SDRAM_TIMING_4				((unsigned volatile long*)( SDRAM_TIMING4              ))  //offset 0x1c0/08C
#define MCU_REG_PHY_CONTROL_7				((unsigned volatile long*)( PHY_CTRL7                  ))  //offset 0x1d0/230
#define MCU_REG_PHY_CONTROL_8				((unsigned volatile long*)( PHY_CTRL8                  ))  //offset 0x1e0/234
#define MCU_REG_PHY_CONTROL_9				((unsigned volatile long*)( PHY_CTRL9                  ))  //offset 0x1f0/238
#define MCU_REG_PHY_CONTROL_10				((unsigned volatile long*)( PHY_CTRL10                 ))  //offset 0x200/23C
#define MCU_REG_PHY_CONTROL_11				((unsigned volatile long*)( PHY_CTRL11                 ))  //offset 0x210/240
#define MCU_REG_PHY_CONTROL_12				((unsigned volatile long*)( PHY_CTRL12                 ))  //offset 0x220/244
#define MCU_REG_PHY_CONTROL_13				((unsigned volatile long*)( PHY_CTRL13                 ))  //offset 0x230/248
#define MCU_REG_PHY_CONTROL_14				((unsigned volatile long*)( PHY_CTRL14                 ))  //offset 0x240/24C
#define MCU_REG_PHY_CONTROL_15				((unsigned volatile long*)( PHY_CTRL15                 ))  //offset 0x250/250

//#define MCU_REG_SDRAM_CONTROL_5			((unsigned volatile long*)( SDRAM_CTRL5_ARB_WEIGHTS    ))  //offset 0x280/deleted
//#define MCU_REG_MCB_SLFST_SEL				((unsigned volatile long*)( MCB_SLFST_SEL              ))  //offset 0x570/deleted 
//#define MCU_REG_MCB_SLFST_CTRL0			((unsigned volatile long*)( MCB_CTRL                   ))  //offset 0x580/deleted
//#define MCU_REG_MCB_SLFST_CTRL1			((unsigned volatile long*)( MCB_CTRL1                  ))  //offset 0x590/deleted
//#define MCU_REG_MCB_SLFST_CTRL2			((unsigned volatile long*)( MCB_CTRL2                  ))  //offset 0x5A0/deleted
//#define MCU_REG_MCB_SLFST_CTRL3			((unsigned volatile long*)( MCB_CTRL3                  ))  //offset 0x5B0/deleted
//#define MCU_REG_MCB_CONTROL_4				((unsigned volatile long*)( MCB_CNTRL4                 ))  //offset 0x540/deleted

#define MCU_REG_SDRAM_TIMING_5				((unsigned volatile long*)( SDRAM_TIMING5              ))  //offset 0x650/090
#define MCU_REG_SDRAM_TIMING_6				((unsigned volatile long*)( SDRAM_TIMING6              ))  //offset 0x660/094
#define MCU_REG_SDRAM_CONTROL_6				((unsigned volatile long*)( SDRAM_CTRL6_SDRAM_ODT_CTRL ))  //offset 0x760/05C
#define MCU_REG_SDRAM_CONTROL_7				((unsigned volatile long*)( SDRAM_CTRL7_SDRAM_ODT_CTRL2 )) //offset 0x770/060
//#define MCU_REG_SDRAM_CONTROL_8			((unsigned volatile long*)( SDRAM_CTRL8_SDRAM_ODT_CTRL2 )) //offset 0x780/deleted
//#define MCU_REG_SDRAM_CONTROL_11			((unsigned volatile long*)( SDRAM_CTRL11_ARB_WEIGHTS_FAST_QUEUE )) //offset 0x7B0/deleted
#define MCU_REG_SDRAM_CONTROL_13			((unsigned volatile long*)( SDRAM_CTRL13               )) //offset 0x7D0/064
#define MCU_REG_SDRAM_CONTROL_14			((unsigned volatile long*)( SDRAM_CTRL14               )) //offset 0x7E0/068
#define MCU_REG_PHY_DLL_CONTROL_1			((unsigned volatile long*)( L1_PHY_DLL_CTRL            )) //offset 0xe10/304
#define MCU_REG_PHY_DLL_CONTROL_2			((unsigned volatile long*)( L2_PHY_DLL_CTRL            )) //offset 0xe20/308
//#define MCU_REG_PHY_DLL_CONTROL_3			((unsigned volatile long*)( L3_PHY_DLL_CTRL            )) //offset 0xe30/deleted
#define MCU_REG_PHY_DLL_CONTROL_4			((unsigned volatile long*)( L1_PHY_CTRL_WL_SELECT      )) //offset 0xe40/380
#define MCU_REG_PHY_DLL_CONTROL_5			((unsigned volatile long*)( L1_PHY_CTRL_WL_CTRL0       )) //offset 0xe50/384
#define MCU_REG_PHY_CONTROL_TEST			((unsigned volatile long*)( PHY_CTRL_TESTMODE		   )) //offset 0xe80/400

#define MCU_REG_MMU_MMAP0					((unsigned volatile long*)( L1_MEMORY_ADDRESS_MAP      ))  //offset 0x100/010
#define MCU_REG_MMU_MMAP1					((unsigned volatile long*)( L2_MEMORY_ADDRESS_MAP      ))  //offset 0x110/014
#define MCU_REG_MMU_MMAP2					((unsigned volatile long*)( L3_MEMORY_ADDRESS_MAP      ))  //offset 0x130/018
#define MCU_REG_MMU_MMAP3					((unsigned volatile long*)( L4_MEMORY_ADDRESS_MAP      ))  //offset 0xA30/01C

#define MCU_REG_USER_INITIATED_COMMAND0		((unsigned volatile long*)( USER_INITIATED_COMMAND0	   ))  //offset 0x120/160
#define MCU_REG_USER_INITIATED_COMMAND1		((unsigned volatile long*)( USER_INITIATED_COMMAND1	   ))  //offset 0x410/164

//#define MCU_REG_CM_WRITE_PROTECTION       ((unsigned volatile long*)( CM_WRITE_PROTECTION        ))  //offset 0x180/deleted
#define	MCU_REG_DRAM_STATUS					((unsigned volatile long*)( DRAM_STATUS                ))  //offset 0x1B0/008
//#define MCU_REG_ERROR_STATUS				((unsigned volatile long*)( ERROR_STATUS               ))  //offset 0x0D0/deleted
//#define MCU_REG_SYS							((unsigned volatile long*)( SYS                        ))  //offset 0x2C0/deleted
#define MCU_REG_EXCLUSIVE_MONITOR_CTRL		((unsigned volatile long*)( EXCLUSIVE_MONITOR_CTRL     ))  //offset 0x380/100


#define MCU_REG_TRUSTZONE_SEL               ((unsigned volatile long*)( MCU_BASE+0x120            ))  //offset 0x3B0/120
#define MCU_REG_TRUSTZONE_RANGE0            ((unsigned volatile long*)( TRUSTZONE_RANGE0           ))  //offset 0x3C0/124
#define MCU_REG_TRUSTZONE_RANGE1            ((unsigned volatile long*)( TRUSTZONE_CTRL1            ))  //offset 0x3D0/128
#define MCU_REG_TRUSTZONE_PERMISSION        ((unsigned volatile long*)( TRUSTZONE_PERMISSION       ))  //offset 0x3B0/12C

#define MCU_REG_MODE_RD_DATA				((unsigned volatile long*)( MODE_RD_DATA               ))  //offset 0x440/170
//#define MCU_REG_TEST_MODE0				((unsigned volatile long*)( TEST_MODE0                 ))  //offset 0x4C0/deleted
//#define MCU_REG_TEST_MODE1				((unsigned volatile long*)( TEST_MODE1                 ))  //offset 0x4D0/deleted
#define MCU_REG_REGISTER_TABLE_CTRL_0   	((unsigned volatile long*)( REGISTER_TABLE_CTRL_0      ))  //offset 0xC00/1C0
#define MCU_REG_REGISTER_TABLE_DATA_0   	((unsigned volatile long*)( REGISTER_TABLE_DATA_0      ))  //offset 0xC20/1C8
#define MCU_REG_REGISTER_TABLE_DATA_1   	((unsigned volatile long*)( REGISTER_TABLE_DATA_1      ))  //offset 0xC30/1CC

//#define MCU_REG_ERROR_ID	   				((unsigned volatile long*)( ERROR_ID                   ))  //offset 0x490/deleted
//#define MCU_REG_ERROR_ADDR   				((unsigned volatile long*)( ERROR_ADDR                 ))  //offset 0x4A0/deleted

#define MMP2_DEFAULT_DCLK					156000000
#define MMP2_DEFAULT_FCLK					26000000		// for refresh cycle calculations

// Mememory Address Map Register 0   offset 0x00000100
// Field Definitions  
#define MMP2_MEMADDRMAPR0_CS_VALID			MEMORY_ADDRESS_MAP_CS_VALID          //BIT 0
//	DRAM Status register	 offset 0x01B0	
#define MCU_DRAM_STATUS_INIT_DONE           DRAM_STATUS_INIT_DONE			     //BIT_0

//TrustZone Select Register fields
#define TZ_LOCK_BASE    31
#define TZ_REGSEL_BASE   0
#define TZ_REGSEL_SIZE   2
#define TZ_CHIPSELECT0   0
#define TZ_CHIPSELECT1   1
//TrustZone Range Register(s) fields
#define TZ_RANGE0_BASE   4
#define TZ_RANGE1_BASE  20
#define TZ_RANGE2_BASE   4
#define TZ_RANGE3_BASE  20
#define TZ_RANGE_SIZE   12
// TrustZone Permission Register fields
#define TZ_PERM_BASE_R0  0
#define TZ_PERM_BASE_R1  3
#define TZ_PERM_BASE_R2  6
#define TZ_PERM_BASE_R3  9
#define TZ_PERM_BASE_RU 12
#define TZ_PERM_SIZE     3
#define TZ_ENABLE_BASE  31
// TrustZone Region Access Permissions
#define TZ_PERM_RW       0
#define TZ_PERM_RO       1
#define TZ_PERM_WO       2
#define TZ_PERM_ABORT    3
//TrustZone Regions
#define TZ_REGION0       0
#define TZ_REGION1       1
#define TZ_REGION2       2
#define TZ_REGION3       3
#define TZ_REGIONU       4
#define TZ_MAXREGIONS    5
#endif
