/******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 *
 *  FILENAME:	RegInstructions.h
 *
 *  PURPOSE: 	Header for RegInstructions.c
 *
******************************************************************************/


#include "tim.h"
#include "Errors.h"
#include "misc.h"
#include "ProtocolManager.h"
#include "PlatformConfig.h"

//Protoypes
void WriteInstruction(unsigned int *Address, unsigned int Value);
void ReadInstruction(volatile unsigned int *Address, unsigned int NumReads);
unsigned int WaitForBitSetInstruction(volatile unsigned int *Address, unsigned int Mask, unsigned int Timeout);
unsigned int WaitForBitClearInstruction(unsigned int *Address, unsigned int Mask, unsigned int Timeout);
void AndInstruction(unsigned int *Address, unsigned int Value);
void OrInstruction(unsigned int *Address, unsigned int Value);
unsigned int ProcessInstructions(pINSTRUCTION_S pInstructions, unsigned int Num, unsigned int TimeoutFlag);

