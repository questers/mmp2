/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#ifndef __ipc_interface_h
#define __ipc_interface_h


#define IPC_A     0
#define IPC_B     1

typedef struct
{
      VUINT32_T Ipc_Isrr;         // interrupt status read (written by other ipc)
      VUINT32_T Ipc_Wdr;          // write 32 bit data (to be received by other ipc)
      VUINT32_T Ipc_Isrw;         // interrupt status write (to be received by other ipc)
      VUINT32_T Ipc_Ocr;          // clear interrupt status bits
      VUINT32_T Ipc_Iir;          // interrupt status ID bits (written by other ipc)
      VUINT32_T Ipc_Rdr;          // read 32 bit data (written by other ipc)
} IPC_T;

#define IPC_SET_GP_INT  (1u<<10)
#define IPC_SET_MSG_INT (1u<< 9)
#define IPC_SET_CMD_INT (1u<< 8)

void IPCInit();
void IPCWrite( unsigned AB, unsigned long data );
unsigned long IPCRead( unsigned AB );

#endif 
