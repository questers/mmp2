/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * History:
 *
 ********* PLEASE INSERT THE CVS HISTORY OF THE PREVIOUS VERSION HERE. *********
 *******************************************************************************/

#ifndef	__INC_REGS_H
#define	__INC_REGS_H

#include "APBC.h"

/*
 *
 *	THE BASE ADDRESSES
 *
 */
#define	UART1_BASE	0xD4030000
#define	UART2_BASE	0xD4017000
#define	UART3_BASE	0xD4018000
#define	UART4_BASE	0xD4016000

#if FPGA
	#define UART_X_BASE		  UART2_BASE
	#define UART_X_CLK_BASE	  APBC_UART1_CLK_RST
#else
	#define UART_X_BASE		  UART3_BASE
	#define UART_X_CLK_BASE	  APBC_UART3_CLK_RST
#endif
	
/*
 *
 *	THE REGISTER DEFINES
 *
 */
#define	UART_RBR	(0x0000)	/* 32 bit	Receive Buffer Register */
#define	UART_THR	(0x0000)	/* 32 bit	Transmit Holding Register */
#define	UART_DLL	(0x0000)	/* 32 bit	Divisor Latch Low Byte
										 *			Register
										 */
#define	UART_DLH	(0x0004)	/* 32 bit	Divisor Latch High Byte
										 *			Register
										 */
#define	UART_IER	(0x0004)	/* 32 bit	Interrupt Enable Register */
#define	UART_IIR	(0x0008)	/* 32 bit	Interrupt Identification
										 *			Register
										 */
#define	UART_FCR	(0x0008)	/* 32 bit	FIFO Control Register */
#define	UART_LCR	(0x000C)	/* 32 bit	Line Control Register */
#define	UART_MCR	(0x0010)	/* 32 bit	Modem Control Register */
#define	UART_LSR	(0x0014)	/* 32 bit	Line Status Register */
#define	UART_MSR	(0x0018)	/* 32 bit	Modem Status Register */
#define	UART_SCR	(0x001C)	/* 32 bit	Scratchpad Register */
#define	UART_ISR	(0x0020)	/* 32 bit	Infrared Selection Register */
#define	UART_FOR	(0x0024)	/* 32 bit	Receive FIFO Occupancy
										 *			Register
										 */
#define	UART_ABR	(0x0028)	/* 32 bit	Auto-Baud Control Register */
#define	UART_ACR	(0x002C)	/* 32 bit	Auto-Baud Count Register */

/*
 *
 *	THE BIT DEFINES
 *
 */
/*	UART_RBR	0x0000	Receive Buffer Register */
#define	UART_RBR_BYTE_3_MSK			SHIFT24(0xff)	/* Byte 3 */
#define	UART_RBR_BYTE_3_BASE		24
#define	UART_RBR_BYTE_1_MSK			SHIFT8(0xff)	/* Byte 1 */
#define	UART_RBR_BYTE_1_BASE		8
#define	UART_RBR_BYTE_0_MSK			SHIFT0(0xff)	/* Byte 0 */
#define	UART_RBR_BYTE_0_BASE		0

/*	UART_THR	0x0000	Transmit Holding Register */
#define	UART_THR_BYTE_3_MSK			SHIFT24(0xff)	/* Byte 3 */
#define	UART_THR_BYTE_3_BASE		24
#define	UART_THR_BYTE_1_MSK			SHIFT8(0xff)	/* Byte 1 */
#define	UART_THR_BYTE_1_BASE		8
#define	UART_THR_BYTE_0_MSK			SHIFT0(0xff)	/* Byte 0 */
#define	UART_THR_BYTE_0_BASE		0

/*	UART_DLL	0x0000	Divisor Latch Low Byte Register */
/*		Bit(s) UART_DLL_RSRV_31_8 reserved */
#define	UART_DLL_DLL_MSK			SHIFT0(0xff)		/* Divisor Latch Low */
#define	UART_DLL_DLL_BASE			0

/*	UART_DLH	0x0004	Divisor Latch High Byte Register */
/*		Bit(s) UART_DLH_RSRV_31_8 reserved */
#define	UART_DLH_DLH_MSK			SHIFT0(0xff)		/* Divisor Latch High */
#define	UART_DLH_DLH_BASE			0

/*	UART_IER	0x0004	Interrupt Enable Register */
/*		Bit(s) UART_IER_RSRV_31_9 reserved */
#define	UART_IER_HSE				BIT_8				/* High Speed UART Enable */
#define	UART_IER_DMAE				BIT_7				/* DMA Requests Enable */
#define	UART_IER_UUE				BIT_6				/* UART Unit Enable */
#define	UART_IER_NRZE				BIT_5				/* NRZ Coding Enable */
/* Receiver Time-out Interrupt Enable */
#define	UART_IER_RTOIE				BIT_4				
#define	UART_IER_MIE				BIT_3				/* Modem Interrupt Enable */
/* Receiver Line Status Interrupt Enable */
#define	UART_IER_RLSE				BIT_2				
/* Transmit Data Request Interrupt Enable */
#define	UART_IER_TIE				BIT_1				
/* Receiver Data Available Interrupt Enable */
#define	UART_IER_RAVIE				BIT_0				

/*	UART_IIR	0x0008	Interrupt Identification Register */
/*		Bit(s) UART_IIR_RSRV_31_8 reserved */
#define	UART_IIR_FIFOES10_MSK		SHIFT6(0x3)			/* FIFO Mode Enable Status */
#define	UART_IIR_FIFOES10_BASE		6
/* DMA End of Descriptor Chain */
#define	UART_IIR_EOC				BIT_5				
#define	UART_IIR_ABL				BIT_4				/* Auto-baud Lock */
#define	UART_IIR_TOD				BIT_3				/* Time Out Detected */
#define	UART_IIR_IID10_MSK			SHIFT1(0x3)			/* Interrupt Source Encoded */
#define	UART_IIR_IID10_BASE			1
#define	UART_IIR_NIP				BIT_0				/* Interrupt Pending */

/*	UART_FCR	0x0008	FIFO Control Register */
/*		Bit(s) UART_FCR_RSRV_31_8 reserved */
/* Interrupt Trigger Level (threshold) */
#define	UART_FCR_ITL_MSK			SHIFT6(0x3)			
#define	UART_FCR_ITL_BASE			6
#define	UART_FCR_BUS				BIT_5				/* 32-Bit Peripheral Bus */
#define	UART_FCR_TRAIL				BIT_4				/* Trailing Bytes */
/* Transmitter Interrupt Level */
#define	UART_FCR_TIL				BIT_3				
#define	UART_FCR_RESETTF			BIT_2				/* Reset Transmit FIFO */
#define	UART_FCR_RESETRF			BIT_1				/* Reset Receive FIFO */
/* Transmit and Receive FIFO Enable */
#define	UART_FCR_TRFIFOE			BIT_0				

/*	UART_LCR	0x000C	Line Control Register */
/*		Bit(s) UART_LCR_RSRV_31_8 reserved */
#define	UART_LCR_DLAB				BIT_7				/* Divisor Latch Access Bit */
#define	UART_LCR_SB					BIT_6				/* Set Break */
#define	UART_LCR_STKYP				BIT_5				/* Sticky Parity */
#define	UART_LCR_EPS				BIT_4				/* Even Parity Select */
#define	UART_LCR_PEN				BIT_3				/* Parity Enable */
#define	UART_LCR_STB				BIT_2				/* Stop Bits */
#define	UART_LCR_WLS10_MSK			SHIFT0(0x3)			/* Word Length Select */
#define	UART_LCR_WLS10_BASE			0

/*	UART_MCR	0x0010	Modem Control Register */
/*		Bit(s) UART_MCR_RSRV_31_6 reserved */
#define	UART_MCR_AFE				BIT_5				/* Auto-flow Control Enable */
#define	UART_MCR_LOOP				BIT_4				/* Loopback Mode */
#define	UART_MCR_OUT2				BIT_3				/* OUT2 Signal Control */
#define	UART_MCR_OUT1				BIT_2				/* Test Bit */
#define	UART_MCR_RTS				BIT_1				/* Request to Send */
#define	UART_MCR_DTR				BIT_0				/* Data Terminal Ready */

/*	UART_LSR	0x0014	Line Status Register */
/*		Bit(s) UART_LSR_RSRV_31_8 reserved */
#define	UART_LSR_FIFOE				BIT_7				/* FIFO Error Status */
#define	UART_LSR_TEMT				BIT_6				/* Transmitter Empty */
#define	UART_LSR_TDRQ				BIT_5				/* Transmit Data Request */
#define	UART_LSR_BI					BIT_4				/* Break Interrupt */
#define	UART_LSR_FE					BIT_3				/* Framing Error */
#define	UART_LSR_PE					BIT_2				/* Parity Error */
#define	UART_LSR_OE					BIT_1				/* Overrun Error */
#define	UART_LSR_DR					BIT_0				/* Data Ready */

/*	UART_MSR	0x0018	Modem Status Register */
/*		Bit(s) UART_MSR_RSRV_31_8 reserved */
#define	UART_MSR_DCD				BIT_7				/* Data Carrier Detect */
#define	UART_MSR_RI					BIT_6				/* Ring Indicator */
#define	UART_MSR_DSR				BIT_5				/* Data Set Ready */
#define	UART_MSR_CTS				BIT_4				/* Clear to Send */
/* Delta Data Carrier Detect */
#define	UART_MSR_DDCD				BIT_3				
/* Trailing Edge Ring Indicator */
#define	UART_MSR_TERI				BIT_2				
#define	UART_MSR_DDSR				BIT_1				/* Delta Data Set Ready */
#define	UART_MSR_DCTS				BIT_0				/* Delta Clear to Send */

/*	UART_SCR	0x001C	Scratchpad Register */
/*		Bit(s) UART_SCR_RSRV_31_8 reserved */
#define	UART_SCR_SCRATCHPAD_MSK			SHIFT0(0xff)		/* SCRATCHPAD */
#define	UART_SCR_SCRATCHPAD_BASE		0

/*	UART_ISR	0x0020	Infrared Selection Register */
/*		Bit(s) UART_ISR_RSRV_31_5 reserved */
#define	UART_ISR_RXPL				BIT_4				/* Receive Data Polarity */
#define	UART_ISR_TXPL				BIT_3				/* Transmit Data Polarity */
/* Transmit Pulse Width Select */
#define	UART_ISR_XMODE				BIT_2				
#define	UART_ISR_RCVEIR				BIT_1				/* Receiver SIR Enable */
#define	UART_ISR_XMITIR				BIT_0				/* Transmitter SIR Enable */

/*	UART_FOR	0x0024	Receive FIFO Occupancy Register */
/*		Bit(s) UART_FOR_RSRV_31_6 reserved */
#define	UART_FOR_BYTE_COUNT_MSK			SHIFT0(0x3f)		/* BYTE COUNT */
#define	UART_FOR_BYTE_COUNT_BASE		0

/*	UART_ABR	0x0028	Auto-Baud Control Register */
/*		Bit(s) UART_ABR_RSRV_31_4 reserved */
#define	UART_ABR_ABT				BIT_3				/* ABT */
#define	UART_ABR_ABUP				BIT_2				/* ABUP */
#define	UART_ABR_ABLIE				BIT_1				/* ABLIE */
#define	UART_ABR_ABE				BIT_0				/* ABE */

/*	UART_ACR	0x002C	Auto-Baud Count Register */
/*		Bit(s) UART_ACR_RSRV_31_16 reserved */
#define	UART_ACR_COUNT_VALUE_MSK		SHIFT0(0xffff)	/* COUNT VALUE */
#define	UART_ACR_COUNT_VALUE_BASE		0

// UART registers        
#define FFRBR      (volatile unsigned long *)(UART_X_BASE + UART_RBR)  // Receive Buffer Register (read only)     
#define FFTHR      (volatile unsigned long *)(UART_X_BASE + UART_THR)  // Transmit Holding Register (write only)     
#define FFIER      (volatile unsigned long *)(UART_X_BASE + UART_IER)  // Interrupt Enable Register (read/write)     
#define FFIIR      (volatile unsigned long *)(UART_X_BASE + UART_IIR)  // Interrupt ID Register (read only)     
#define FFFCR      (volatile unsigned long *)(UART_X_BASE + UART_FCR)  // FIFO Control Register (write only)     
#define FFLCR      (volatile unsigned long *)(UART_X_BASE + UART_LCR)  // Line Control Register (read/write)     
#define FFMCR      (volatile unsigned long *)(UART_X_BASE + UART_MCR)  // Modem Control Register (read/write)     
#define FFLSR      (volatile unsigned long *)(UART_X_BASE + UART_LSR)  // Line Status Register (read only)     
#define FFMSR      (volatile unsigned long *)(UART_X_BASE + UART_MSR)  // Modem Status Register (read only)     
#define FFSPR      (volatile unsigned long *)(UART_X_BASE + UART_SCR)  // Scratch Pad Register (read/write)     
#define FFFOR      (volatile unsigned long *)(UART_X_BASE + UART_FOR)  // Receive FIFO Occupancy Register (read/write)
#define FFDLL      (volatile unsigned long *)(UART_X_BASE + UART_DLL)  // baud divisor lower byte (read/write)     
#define FFDLH      (volatile unsigned long *)(UART_X_BASE + UART_DLH)  // baud divisor higher byte (read/write)  
#define FFISR	   (volatile unsigned long *)(UART_X_BASE + UART_ISR) // Infrared Selection Register


/* -------------------- */


#endif	/* __INC_UART_H */
