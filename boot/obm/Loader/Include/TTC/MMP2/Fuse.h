#ifndef __FUSE_H
#define __FUSE_H
/******************************************************************************
** (C) Copyright 2009 Marvell International Ltd.  
**  All Rights Reserved
**
**  FILENAME:	Fuse.h
**
**  PURPOSE: 	Abstract the fuse block hardware definitions
**              (This file will change when the hardware changes - code that uses this file should not)
** 			                  
******************************************************************************/
#include <FuseHw.h>

#define FUSE_SOC_CFG1              WTM_FUSE_VAL_CFG1    // Bits  31:0  of Fuse Block 0
#define FUSE_SOC_CFG2              WTM_FUSE_VAL_CFG2    // Bits  63:32 of Fuse Block 0
#define FUSE_SOC_CFG3              WTM_FUSE_VAL_CFG3    // Bits  95:64 of Fuse Block 0
#define FUSE_SOC_CFG4              WTM_FUSE_VAL_CFG4    // Bits 127:96 of Fuse Block 0

#define FUSE_VAL_ROOT_KEY1         WTM_FUSE_VAL_ROOT_KEY1          
#define FUSE_VAL_OEM_HASH_KEY      WTM_FUSE_VAL_OEM_HASH_KEY
#define FUSE_VAL_OEM_JTAG_HASH     WTM_FUSE_VAL_OEM_JTAG_HASH
#define FUSE_STATUS                WTM_FUSE_STATUS

#define FUSE_USB_ID_1			   WTM_USB_ID_1
#define FUSE_OEM_KEY_HASH_ECC      WTM_OEM_KEY_HASH_ECC
#define FUSE_JTAG_KEY_HASH_ECC     WTM_FUSE_VAL_OEM_JTAG_KEY_HASH_ECC  // Bits 31:16       
#define FUSE_SOC_CONFIG_ECC        WTM_BLOCK_0_FUSE_VAL_REG_4  // ECC in Bits 144:128 of Fuse Block 0
#define FUSE_USBID_ECC             BLOCK7_RESERVED0     // Bits  95:64 of Fuse Block 7 
#define FUSE_ECC_STATUS            WTM_ECC_STATUS 

#define FUSE_SOC_CONFIG_ECC_MASK   0x0000FFFF

// USB ID ECC is Block 7 bits [87:80]
#define FUSE_USBID_ECC_MASK		   0x00FF0000 // Bits[87:80]in Register BLOCK7_RESERVED_0 [95:64] 
#define FUSE_USBID_ECC_OFFSET	   16

	
// SOC Config ECC Status Field Definitions(Fuse Block 0 [127:0]) 
#define FUSE_ECC_STATUS_SOC_127_64_MSK        WTM_ECC_STATUS_ERR_TYPE_7_MSK      
#define FUSE_ECC_STATUS_SOC_127_64_BASE       WTM_ECC_STATUS_ERR_TYPE_7_BASE     
#define FUSE_ECC_STATUS_SOC_63_0_MSK          WTM_ECC_STATUS_ERR_TYPE_6_MSK      
#define FUSE_ECC_STATUS_SOC_63_0_BASE         WTM_ECC_STATUS_ERR_TYPE_6_BASE     

// OEM Hash Key ECC Status Field Definitions(Fuse Block 2[255:0]) 
#define FUSE_ECC_STATUS_OEM_255_192_MSK       WTM_ECC_STATUS_ERR_TYPE_3_MSK
#define FUSE_ECC_STATUS_OEM_255_192_BASE      WTM_ECC_STATUS_ERR_TYPE_3_BASE
#define FUSE_ECC_STATUS_OEM_191_128_MSK       WTM_ECC_STATUS_ERR_TYPE_2_MSK
#define FUSE_ECC_STATUS_OEM_191_128_BASE      WTM_ECC_STATUS_ERR_TYPE_2_BASE   
#define FUSE_ECC_STATUS_OEM_127_64_MSK        WTM_ECC_STATUS_ERR_TYPE_1_MSK  
#define FUSE_ECC_STATUS_OEM_127_64_MSK_BASE   WTM_ECC_STATUS_ERR_TYPE_1_BASE
#define FUSE_ECC_STATUS_OEM_63_0_MSK          WTM_ECC_STATUS_ERR_TYPE_0_MSK 
#define FUSE_ECC_STATUS_OEM_63_0_BASE         WTM_ECC_STATUS_ERR_TYPE_0_BASE 

// JTAG Hash Key ECC Status Definitions for (FuseBlock 6 [255:0]) 
#define FUSE_ECC_STATUS_JTAG_255_192_MSK      WTM_ECC_STATUS_ERR_TYPE_9_MSK
#define FUSE_ECC_STATUS_JTAG_255_192_BASE     WTM_ECC_STATUS_ERR_TYPE_9_BASE
#define FUSE_ECC_STATUS_JTAG_191_128_MSK      WTM_ECC_STATUS_ERR_TYPE_8_MSK
#define FUSE_ECC_STATUS_JTAG_191_128_BASE     WTM_ECC_STATUS_ERR_TYPE_8_BASE
#define FUSE_ECC_STATUS_JTAG_127_64_MSK       WTM_ECC_STATUS_ERR_TYPE_5_MSK
#define FUSE_ECC_STATUS_JTAG_127_64_BASE      WTM_ECC_STATUS_ERR_TYPE_5_BASE
#define FUSE_ECC_STATUS_JTAG_63_0_MSK         WTM_ECC_STATUS_ERR_TYPE_4_MSK
#define FUSE_ECC_STATUS_JTAG_63_0_BASE        WTM_ECC_STATUS_ERR_TYPE_4_BASE

// USB ID ECC Status Definitions for (FuseBlock 7 [191:128]) 
#define	FUSE_ECC_STATUS_USBID_63_0_MSK		  WTM_ECC_STATUS_ERR_TYPE_USB_MSK
#define	FUSE_ECC_STATUS_USBID_63_0_BASE		  WTM_ECC_STATUS_ERR_TYPE_USB_BASE

#endif	/* __FUSE_H */
