/*************************************************************
 * PlatformJtag.h
 *
 * Contents:
 *      Definitions and functions declarations used to support 
 *      JTAG Communications Protocol 
 *
 *************************************************************/
#ifndef PlatformJtag_h
#define PlatformJtag_h
#include "FuseHw.h"

// Enable one of the following:
#define NUM_JTAG_DEBUGKEY_REGISTERS	8 		//implies 8 unidirectional registers each way(16 total)
//#define NUM_JTAG_DEBUGKEY_REGISTERS	2	//implies 2 bidirectional registers

#define JTAG_TRANSACTION_SIZE_IN_BYTES  NUM_JTAG_DEBUGKEY_REGISTERS * 4	  // 4 bytes/register
#define JTAG_REENABLE_MAGIC_NUMBER 		0x5B50
#define JTAG_PJ4_ZSP_JTAG_BYPASS_MASK	BIT0

#define MESSAGE_FROM_SP_TO_JTAG_1_REG	  WTM_BASE + 0x28D8
#define MESSAGE_FROM_SP_TO_JTAG_2_REG	  WTM_BASE + 0x28DC
#define MESSAGE_FROM_SP_TO_JTAG_3_REG	  WTM_BASE + 0x28E0
#define MESSAGE_FROM_SP_TO_JTAG_4_REG	  WTM_BASE + 0x28E4
#define MESSAGE_FROM_SP_TO_JTAG_5_REG	  WTM_BASE + 0x28E8
#define MESSAGE_FROM_SP_TO_JTAG_6_REG	  WTM_BASE + 0x28EC
#define MESSAGE_FROM_SP_TO_JTAG_7_REG	  WTM_BASE + 0x28F0
#define MESSAGE_FROM_SP_TO_JTAG_8_REG	  WTM_BASE + 0x28F4

#define JTAG_DEBUGKEY_1_REG	  WTM_BASE + 0x29C8
#define JTAG_DEBUGKEY_2_REG	  WTM_BASE + 0x29CC
#define JTAG_DEBUGKEY_3_REG	  WTM_BASE + 0x29D0
#define JTAG_DEBUGKEY_4_REG	  WTM_BASE + 0x29D4
#define JTAG_DEBUGKEY_5_REG	  WTM_BASE + 0x29D8
#define JTAG_DEBUGKEY_6_REG	  WTM_BASE + 0x29DC
#define JTAG_DEBUGKEY_7_REG	  WTM_BASE + 0x29E0
#define JTAG_DEBUGKEY_8_REG	  WTM_BASE + 0x29E4

#define JTAG_PJ4_ZSP_DEBUG_VALID_CODE_REG 		WTM_BASE + 0x2858
#define JTAG_PJ4_ZSP_JTAG_BYPASS_COPNTROL_REG 	WTM_BASE + 0x285C


#define USER_DFND_INT_0		0x00004000
#define SP_INTERRUPT_MASK 	WTM_BASE + 0x021C
#define SP_INTERRUPT_RESET_REGISTER  WTM_BASE + 0x0218

//A0
#define ADDRESS_DECODER_INTERRUPT		WTM_BASE + 0x040C
#define ADDRESS_DECODER_INTERRUPT_MASK	WTM_BASE + 0x0410

void JTAG_Enable_DebugKey_Interrupts();
void JTAG_Disable_DebugKey_Interrupts();
void JTAG_Clear_DebugKey_InterruptRequest();
UINT_T JTAG_Initialize_DebugKey_Access();
UINT_T JTAG_CalculateHashOfPublicKey();
UINT_T JTAG_VerifyPassword();
UINT_T JTAG_GetNonce();
UINT_T JTAG_GetNonceSize();
UINT_T JTAG_IsHashSupported(UINT_T JTAG_HASH_ID);
UINT_T JTAG_IsKeySupported(UINT_T JTAG_KEY_ID);
void JTAG_WriteDebugKeys(UINT_T* pDebugKeyData);
void JTAG_ReadDebugKeys(UINT_T * pDebugKeyData);
void JTAG_EnableJTAG();
void JTAG_SetUseJTAGInterrupts();
void JTAG_SetForceDownload();
UINT_T JTAG_SetBootState(UINT_T BootState);
UINT_T JTAG_SetGPIO(UINT_T GPIO_Sel);
UINT_T JTAG_SetWDT (UINT_T WDT);
#if BOARD_DEBUG
void JTAG_Substitute_Fuses_For_BoardDebug();
#endif

#endif PlatformJtag_h
