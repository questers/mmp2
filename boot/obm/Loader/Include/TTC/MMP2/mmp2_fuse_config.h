/******************************************************************************
 *
 *  (C)Copyright 2005 - 2009 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into
 *  products for purposes authorized by the license agreement provided they
 *  include this notice and the associated copyright notice with any such
 *  product.
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
#ifndef __MMP2_FUSE_CONFIG_H__
#define __MMP2_FUSE_CONFIG_H__

#include "Typedef.h"
#include "bl_provisioning_if.h"

/**
 * The setting are as follow.
 *  - Soc configuration fuse block 0 is defined by
 *        * first halfword followed by 3 word.
 *    The reason of doing so is that it makes easier to set them following a default
 *    strap configuration following the datasheet.
 */

// following macro are defined for code annotation
//
#define BIT32 0
#define BIT33 0
#define BIT34 0
#define BIT35 0
#define BIT36 0
#define BIT37 0
#define BIT38 0
#define BIT39 0
#define BIT40 0
#define BIT41 0
#define BIT42 0
#define BIT43 0
#define BIT44 0
#define BIT45 0
#define BIT46 0
#define BIT47 0
#define BIT48 0
#define BIT49 0
#define BIT50 0
#define BIT51 0
#define BIT52 0
#define BIT53 0
#define BIT54 0
#define BIT55 0
#define BIT56 0
#define BIT57 0
#define BIT58 0
#define BIT59 0
#define BIT60 0
#define BIT61 0
#define BIT62 0
#define BIT63 0
#define BIT64 0
#define BIT65 0
#define BIT66 0
#define BIT67 0
#define BIT68 0
#define BIT69 0
#define BIT70 0
#define BIT71 0
#define BIT72 0
#define BIT73 0
#define BIT74 0
#define BIT75 0
#define BIT76 0
#define BIT77 0
#define BIT78 0
#define BIT79 0


#define MMP2_PLATFORM_ID_FOR_FUSE_CONFIGURATION 0x4D4D5032 /* "MMP2" */

// Configuration fuses first half word for fuse block 0
//
#define MMP2_FUSECFG_PLL1_DEFAULT_MULTIPLICATION_FACTOR_0 (0)
#define MMP2_FUSECFG_PLL1_DEFAULT_MULTIPLICATION_FACTOR_1 (0)
#define MMP2_FUSECFG_PLL1_BYPASS_2                        (0)
#define MMP2_FUSECFG_PLL2_BYPASS_3                        (0)
#define MMP2_FUSECFG_CONTROL_4_TO_5                       (0 | 0)
#define MMP2_FUSECFG_NMFI_ENABLE_AP_STRAP_6               (0)
#define MMP2_FUSECFG_V7_MODE_AP_STRAP_7                   (0)
#define MMP2_FUSECFG_RESERVED_8                           (0)
#define MMP2_FUSECFG_SPIDEN_AP_STRAP_9                    (0)
#define MMP2_FUSECFG_SPNIDEN_AP_STRAP_10                  (0)
#define MMP2_FUSECFG_RESERVED_11_TO_15                    (0 | 0 | 0 | 0 | 0)
#define MMP2_DISABLE_CLOCK_GATING_16                      (0)
#define MMP2_FUSECFG_RESERVED_17_TO_19                    (0 | 0 | 0)
#define MMP2_NAND_CHIP_SELECT_HIGH_20_23				  (0 | 0 | 0 | 0)
#define MMP2_FUSECFG_RESERVED_24_TO_31                    (0)

#define MMP2_FUSE_VALUE_CONFIGURATION_HW0 0		// 	Bits 0-31


// configuration fuse first word after very first half word
// - first word
#define MMP2_FUSECFG_DISABLE_CLOCK_GATING_AT_POR_16          (0)
#define MMP2_FUSECFG_RESERVED_17_TO_19                       (0 | 0 | 0)
#define MMP2_FUSECFG_AND_CHIP_SELECT_IS_ACTIVE_HIGH_20_TO_23 (0 | 0 | 0 | 0)
#define MMP2_FUSECFG_RESERVED_24_TO_35                       (0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0)


#define MMP2_FUSECFG_RESERVED_32_36                           0
#define MMP2_FUSECFG_INTERNAL_ASYNC_CONTROL2_37              (BIT21)
#define MMP2_FUSECFG_RESERVED_38_TO_39                       (0)
#define MMP2_FUSECFG_INTERNAL_ASYNC_CONTROL5_40              (BIT24)
#define MMP2_FUSECFG_POR_ACLK_DIV_41_TO_43                   (BIT25 | BIT26 | 0)
#define MMP2_FUSECFG_POR_DCLK_DIV_44_TO_46                   (BIT28 | 0 | 0)
#define MMP2_FUSECFG_PRO_PJ4_XPCLK_DIV_47                    (BIT31)

#define MMP2_FUSE_VALUE_CONFIGURATION_WD0 (0xD96D57F0) // Mode 1 Default setting		Bits 32-63

//
// - next word
#define MMP2_FUSECFG_POR_PJ4_XPCLK_DIV_48_TO_49                  (0)
#define MMP2_FUSECFG_POR_PJ4_BUSCLK_DIV_50_TO_52                 (0)
#define MMP2_FUSECFG_POR_PJ4_MEMCLK_DIV_53_TO_55                 (0)
#define MMP2_FUSECFG_POR_PJ4_PCLK_DIV_56_TO_58                   (0)
#define MMP2_FUSECFG_POR_SECURE_PROCESSOR_XPCLK_DIV_59_TO_61     (0)
#define MMP2_FUSECFG_POR_SECURE_PROCESSOR_BUSCLK_DIV_62_TO_64    (0)
#define MMP2_FUSECFG_POR_SECURE_PROCESSOR_MEMCLK_DIV_65_TO_67    (0)
#define MMP2_FUSECFG_POR_SECURE_PROCESSOR_PCLK_DIV_68_TO_70      (0)
#define MMP2_FUSECFG_RESERVED_71_TO_73                           (0)
#define MMP2_FUSECFG_BOOT_ROM_OPERATION_MODE_74_TO_75            (0)		// Trusted is 10, non-trusted 01
#define MMP2_FUSECFG_TRUSTED_BOOT_76                             (BIT12)	// 1 = Use WTM, 0 = IppCp
#define MMP2_FUSECFG_SECURE_KEY_ACCESS_DISABLE_77                (0)
#define MMP2_FUSECFG_PLATFORM_BOOT_STATE_78_TO_81                (0)
#define MMP2_FUSECFG_GLOBAL_FUSE_DONT_USE_82                     (0)
#define MMP2_FUSECFG_DOWNLOAD_FROM_UART_PORT_DISABLE_83          (0)
#define MMP2_FUSECFG_DOWNLOAD_FROM_USB_PORT_DISABLE_84           (0)
#define MMP2_FUSECFG_BLOCK_PJ4_SECURE_ACCESS_TO_MC_SIDEBAND_85   (0)
#define MMP2_FUSECFG_DOWNLOAD_DISABLE_86                         (0)
#define MMP2_FUSECFG_DOWNLOAD_FROM_MMC_PORT_DISABLE_87           (0)
#define MMP2_FUSECFG_BOOT_ROM_PJ4_88                             (0)
#define MMP2_FUSECFG_BOOT_FROM_LOW_ADDRESS_89                    (0)
#define MMP2_FUSECFG_BLOCK_PJ4_SECURE_ACCESS_TO_AXI_90           (0)
#define MMP2_FUSECFG_NO_FLASH_DEVICE_91                          (0)
#define MMP2_FUSECFG_DISABLE_INSTRUCTION_BLOCKING_92             (0)
#define MMP2_FUSECFG_GLOBOL_SECURITY_ENABLE_93                   (0)
#define MMP2_FUSECFG_DIS_AIB_OVERRIDE_94                         (BIT30)
#define MMP2_FUSECFG_DIS_STRAP_OVERRIDE_95                       (BIT31)

#define MMP2_FUSE_VALUE_CONFIGURATION_WD1  (0x016 | \
                                                 MMP2_FUSECFG_BOOT_ROM_OPERATION_MODE_74_TO_75 | \
                                                 MMP2_FUSECFG_TRUSTED_BOOT_76 | \
                                                 MMP2_FUSECFG_SECURE_KEY_ACCESS_DISABLE_77 | \
                                                 MMP2_FUSECFG_PLATFORM_BOOT_STATE_78_TO_81 | \
                                                 MMP2_FUSECFG_DIS_AIB_OVERRIDE_94 | \
                                                 MMP2_FUSECFG_DIS_STRAP_OVERRIDE_95) 		 // Bits 64-95

//
// - next word

#define MMP2_FUSE_VALUE_CONFIGURATION_WD2  0					// Bits 96-128

/**
 * Attention:
 *
 * GEU fuse library requires CP, AP, and USB be programmed currently if block 0
 * ECC is programmed. For supporting block 0 ECC programming, the fuse programming
 * function constructed as such that:
 *   - CP, AP, and USB ID is programmed only when programming AP configuration.
 * As the result, tvtd_fuse_conf_spec needs abide by the following rule:
 *   1. If CP, or USB ID is not programmed, the corresponding "go programming field"
 *      prg_cp_conf, and pro_usb_id still need be set to 1; however their "value field"
 *      plat_cp_conf[0], and plat_usb_id{0] must be set to 0.
 *   2. Depending the setting of prg_blk0_ecc, the block 0 ECC will be programmed
 *      while programming AP configuration.
 */
const fuse_conf_spec_t mmp2_fuse_conf_spec = {  \
    MMP2_PLATFORM_ID_FOR_FUSE_CONFIGURATION, /* platform id for configuration      */ \
    1,                                       /* go program bit[0:15] configuration */ \
    {                                                                                 \
        MMP2_FUSE_VALUE_CONFIGURATION_HW0    /* platform configuration bit[0:15]   */ \
    },                                                                                \
    16,                                      /* bit count                          */ \
    1,                                       /* go program bit[16:95]              */ \
    {                                                                                 \
        MMP2_FUSE_VALUE_CONFIGURATION_WD0,   /* platform configuration bit[16:95]  */ \
        MMP2_FUSE_VALUE_CONFIGURATION_WD1,                                            \
        MMP2_FUSE_VALUE_CONFIGURATION_WD2                                             \
    },                                                                                \
    80,                                      /* bit count                          */ \
    1,                                       /* do not program usb id              */ \
    {                                                                                 \
        0x81121286                           /* usb id (pid|vid) in UINT32         */ \
    },                                                                                \
    4,                                       /* usb id byte count                  */ \
    {                                                                                 \
        0,                                   /* advance version number             */ \
    },                                       /* - not used; wtm can only           */ \
    {                                        /*   advance version in init state.   */ \
        0,                                   /* advance life cycle number          */ \
    },                                                                                \
    0,                                       /* go program block 0 ecc             */ \
};


#endif //__MMP2_FUSE_CONFIG_H

