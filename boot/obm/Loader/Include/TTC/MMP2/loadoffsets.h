/******************************************************************************
 *
 *	(C)Copyright 2005 - 2007 Marvell.
 *
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *	The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/
/******************************************************************************
 *
 *	loadoffsets.h
 *
 *	This file contains all of the processor specific memory load addresses
 *	 used by the boot ROM or OBM code
 *
 ******************************************************************************/

#ifndef _LOADOFFSETS_H_
#define _LOADOFFSETS_H_


/* BootROM ISRAM Memory Map
0xD10000000 : WTM
0xD10000000 + sizeof(WTM) : TIM
0xD1026000: Run Time Error Collection
0xD1026100: Data + BSS, Transfer Struct
0xD1030000: Stack
*/

// ISRAM
#define ISRAM_PHY_ADDR						0xD1000000
#define ISRAM_PHY_SIZE						0x30000				// 192 KB
#define PLATFORM_TBR_RUNTIME_RESERVED_SIZE	0xA000				// 40 KB

#define PLATFORMISRAMLIMIT					ISRAM_PHY_SIZE - PLATFORM_TBR_RUNTIME_RESERVED_SIZE 	//192KB - 40KB
#define ISRAM_IMAGE_LOAD_BASE				ISRAM_PHY_ADDR
#define FLASH_STREAM_SIZE					(128 * 1024)		// This is the amount of data we allow eMMC to stream automatically for this platform.
#define FLASH_STREAM_ADDRESS				0xD1000000		// Not Supported

//  Download address for images and data
#define CS0Base								0x80000000
#define CS1Base								0x90000000
#define CS2Base								0xA0000000
#define CS3Base								0xB0000000

//DDR
#define DDR_CS0_TBR							0xC0000000
#define DDR_CS1_TBR							0x10000000
#define DDR_PHY_ADDR						0x00000000

#define OBM_RESTRICTED_DDR_SPACE			0x00040000 //should be named RESTRICED_SIZE, space is a misnomer
#define DDR_SPARE_AREA_ADDR					0x00040000
#define DDR_BLK_ZERO_BUF_ADDR				0x00060000	// 512 KB buffer to store blk 0
#define DDR_DOWNLOAD_AREA_ADDR				0x00100000	// 512 K aligned so mmc sdma int doesn't assert too frequently.
#define DDR_VERIFY_BUFFER_OFFSET			0x01000000  // 16 MB offset

/**************************************************************************/
/* mDOC device physical address window location                           */
/**************************************************************************/
#define MDOC_START_ADDRESS	CS2Base   /* Used for P2Sample platform */

#endif
