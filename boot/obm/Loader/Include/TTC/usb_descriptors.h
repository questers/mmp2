/******************************************************************************
**	(C) Copyright 2007 Marvell International Ltd.  
**	All Rights Reserved
******************************************************************************/

/******************************************************************************
**
**  COPYRIGHT (C) 2000, 2001 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document.
**  Except as permitted by such license, no part of this document may be
**  reproduced, stored in a retrieval system, or transmitted in any form or by
**  any means without the express written consent of Intel Corporation.
**
**  FILENAME:       usb_descriptors.h
**
**  PURPOSE:   		USB descriptor defs
**
**  LAST MODIFIED: Modtime: 2/14/2007
**
******************************************************************************/
#ifndef _USB_DESCRIPTORS_H
#define _USB_DESCRIPTORS_H

#include "Typedef.h"
typedef struct
{
 unsigned int DesSize;
 unsigned int * pDesPayload;
}
OPT_USB_DESCRIPTOR_LOADS, *pOPT_USB_DESCRIPTOR_LOADS;

typedef struct
{
	UINT8_T 	bLength;
	UINT8_T 	bDescriptorType;
	UINT8_T		bcdUSB[2];
	UINT8_T		bDeviceClass;
	UINT8_T		bDeviceSubClass;
	UINT8_T		bDeviceProtocol;
	UINT8_T		bMaxPacketSize;
	UINT8_T		idVendor[2];
	UINT8_T		idProduct[2];
	UINT8_T		bcdDevice[2];
	UINT8_T		iManufacturer;
	UINT8_T		iProduct;
	UINT8_T		iSerialNumber;
	UINT8_T		bNumConfigurations;
} USBDeviceDescriptor, *pUSBDeviceDescriptor;

typedef struct
{
 	UINT8_T		bLength;
 	UINT8_T		bDescriptorType;
	UINT8_T		wTotalLength[2];
	UINT8_T		bNumInterfaces;
	UINT8_T		bConfigurationValue;
	UINT8_T		Configuration;
	UINT8_T		bmAttributes;
	UINT8_T		bMaxPower;
} USBConfigurationDesc, *pUSBConfigurationDesc;

typedef struct
{
 	UINT8_T		bLength;
 	UINT8_T		bDescriptorType;
 	UINT8_T		bInterfaceNumber;
 	UINT8_T		bAlternateSetting;
 	UINT8_T		bNumEndpoints;
 	UINT8_T		bInterfaceClass;
 	UINT8_T		bInterfaceSubClass;
 	UINT8_T		bInterfaceProtocol;
 	UINT8_T		iInterface;
} USBInterfaceDesc, *pUSBInterfaceDesc;

typedef struct
{
	UINT8_T		bLength;
	UINT8_T		bDescriptorType;
	UINT8_T		bEndpointAddress;
	UINT8_T		bmAttributes;
	UINT8_T		wMaxPacketSize[2];
	UINT8_T		bInterval;
} USBEndPointDesc, *pUSBEndPointDesc;

typedef struct
{
	USBConfigurationDesc 	ConfigDesc;
	USBInterfaceDesc		InterDesc;
	USBEndPointDesc			EndDesc[2];
} USBConfigDescriptor, *pUSBConfigDescriptor;

UINT_T SetUpUSBDescriptors (pTIM pTIM_h, UINT_T TIM_Use, UINT_T PortType);

#endif
