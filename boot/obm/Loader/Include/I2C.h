/*--------------------------------------------------------------------------------------------------------------------
(C) Copyright 2006, 2007 Marvell DSPC Ltd. All Rights Reserved.
-------------------------------------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------------------------------------------------
INTEL CONFIDENTIAL
Copyright 2006 Intel Corporation All Rights Reserved.
The source code contained or described herein and all documents related to the source code (�Material�) are owned
by Intel Corporation or its suppliers or licensors. Title to the Material remains with Intel Corporation or
its suppliers and licensors. The Material contains trade secrets and proprietary and confidential information of
Intel or its suppliers and licensors. The Material is protected by worldwide copyright and trade secret laws and
treaty provisions. No part of the Material may be used, copied, reproduced, modified, published, uploaded, posted,
transmitted, distributed, or disclosed in any way without Intel�s prior express written permission.

No license under any patent, copyright, trade secret or other intellectual property right is granted to or
conferred upon you by disclosure or delivery of the Materials, either expressly, by implication, inducement,
estoppel or otherwise. Any license under such intellectual property rights must be express and approved by
Intel in writing.
-------------------------------------------------------------------------------------------------------------------*/

/************************************************************************/
/*                                                                      */
/* Title: I2C pacakge header file                                       */
/*                                                                      */
/* Filename:  I2C.h                                                    */
/*                                                                      */
/* Target, subsystem: Common Platform, HAL                              */
/************************************************************************/
#ifndef _I2C_H_

#define _I2C_H_

#include "Errors.h"

#define I2C_SLAVE_WRITE(slv)        ( (slv) | 0x00000000 )			/* Master is writing to the slave */
#define I2C_SLAVE_READ(slv)         ( (slv) | 0x00000001 )      	/* Master is reading from the slave */

#define I2C_REG_WRITE(context, reg,wval) \
        ( (* ( (volatile UINT32*)((context->I2CRegsBase) + (reg)) ) ) = wval);

#define I2C_REG_READ(context, reg,rval) \
        rval = (* ( (volatile UINT32*)((context->I2CRegsBase) + (reg)) ) );


// i2c_reg_bit_write: set only one bit in a register
#define I2C_REG_BIT_WRITE(context,reg,btNm,sc) \
            {   UINT32 regVl; \
                I2C_REG_READ(context,reg,regVl); \
                sc ? ( regVl = (1 << btNm) | regVl ) : (regVl = ((regVl) & (~(1 << btNm)))); \
                I2C_REG_WRITE(context,reg,regVl); \
            }

// i2c_reg_bit_clear: does a write-one-to-clear operation on only one bit in a register
#define I2C_STATUS_REG_CLEAR_BIT(context,bitNum)   			\
            {   UINT32 regVal; 						\
                regVal = (1 << bitNum); 			\
                I2C_REG_WRITE(context,I2C_ISR_REG,regVal); 	\
                I2C_REG_READ(context,I2C_ISR_REG,regVal); 	/* read back to ensure write completion */ \
			}



#define I2C_REG_BIT_READ(regVl,btNm)  ( (regVl >> btNm) & 0x00000001 )

#define I2C_REG_BIT(btNm) (((UINT32)1)<<(btNm))


#define CHECK_IF_CHIP_BUSY(context,st) \
    {   UINT32_T  rv; \
        I2C_REG_READ(context,I2C_ISR_REG,rv); \
        st = I2C_REG_BIT_READ(rv,I2C_ISR_UNIT_BUSY_BIT) ? TRUE : FALSE; \
    }


#define CHECK_FOR_EARLY_BUS_BUSY(context,st) \
    {   UINT32_T  rv; \
        I2C_REG_READ(context,I2C_ISR_REG,rv); \
        st = I2C_REG_BIT_READ(rv,I2C_ISR_EARLY_IC_BUS_BUSY_BIT) ? TRUE : FALSE; \
    }


#define CHECK_IF_GENERAL_CALL(ad)       ( ((ad <= 0x7) || (ad >= 0xf0)) ? TRUE : FALSE )  // general call is made using the slave address 0x00

#define I2C_REG_CLEAR_VALUE         					0x0000
#define I2C_ICR_CLEAR_ALL_CONTROL_BITS					0xfff0		// Clear the TB, STOP, START, ACK bits in the ICR register
#define I2C_ICR_REG_INIT_VALUE							0x14E0		//all interrupts are disabled, except of the Bus Error and arbitration lost

/** bit map of the ICR register **/
#define I2C_ICR_FAST_MODE                           	15
#define I2C_ICR_UNIT_RESET_BIT                      	14
#define I2C_ICR_IDBR_RECEIVE_FULL_INT_ENABLE_BIT    	9
#define I2C_ICR_UNIT_ENABLE_BIT                     	6
#define I2C_ICR_SCLEA_ENABLE_BIT                    	5
#define I2C_ICR_MASTER_ABORT_BIT                    	4
#define I2C_ICR_TRANSFER_BYTE_BIT                   	3
#define I2C_ICR_ACK_NACK_CONTROL_BIT                	2
#define I2C_ICR_STOP_BIT                            	1
#define I2C_ICR_START_BIT                           	0


#define I2C_ICR_UNIT_RESET                            	(1 << I2C_ICR_UNIT_RESET_BIT)
#define I2C_ICR_IDBR_RECEIVE_FULL_INT_ENABLE          	(1 << I2C_ICR_IDBR_RECEIVE_FULL_INT_ENABLE_BIT)
#define I2C_ICR_UNIT_ENABLE                           	(1 << I2C_ICR_UNIT_ENABLE_BIT)
#define I2C_ICR_SCLEA_ENABLE                          	(1 << I2C_ICR_SCLEA_ENABLE_BIT)
#define I2C_ICR_MASTER_ABORT                          	(1 << I2C_ICR_MASTER_ABORT_BIT)
#define I2C_ICR_TRANSFER_BYTE                         	(1 << I2C_ICR_TRANSFER_BYTE_BIT)
#define I2C_ICR_ACK_NACK_CONTROL                      	(1 << I2C_ICR_ACK_NACK_CONTROL_BIT)
#define I2C_ICR_STOP                                  	(1 << I2C_ICR_STOP_BIT)
#define I2C_ICR_START                                 	(1 << I2C_ICR_START_BIT)


/** bit map of the ISR register **/
#define I2C_ISR_UNEXPECTED_INTERRUPTS_REPORT			0xff
#define I2C_ISR_UNEXPECTED_INTERRUPTS					0x00000310
#define I2C_ISR_CLEAR_ALL_INTERRUPTS					0x000007f0

#define I2C_ISR_EARLY_IC_BUS_BUSY_BIT					11
#define I2C_ISR_BUS_ERROR_DETECTED_BIT                  10
#define I2C_ISR_SLAVE_ADDRESS_DETECTED_BIT              9
#define I2C_ISR_GENERAL_CALL_ADDRESS_DETECTED_BIT       8
#define I2C_ISR_IDBR_RECEIVE_FULL_BIT                   7
#define I2C_ISR_IDBR_TRANSMIT_EMPTY_BIT                 6
#define I2C_ISR_ARBITRATION_LOSS_DETECTED_BIT           5
#define I2C_ISR_SLAVE_STOP_DETECTED_BIT                 4
#define I2C_ISR_I2C_BUS_BUSY_BIT                        3
#define I2C_ISR_UNIT_BUSY_BIT                           2
#define I2C_ISR_ACK_NACK_STATUS_BIT                     1
#define I2C_ISR_READ_WRITE_MODE_BIT                     0


/** bit map of the IBMR register **/
#define I2C_IBMR_SDA_BIT                     			0
#define I2C_IBMR_SCL_BIT                     			1
#define I2C_IBMR_SCL_AND_SDA_LINES_HIGH					0x3

#define I2C_IBMR_SDA_STATUS_BIT							(1 << I2C_IBMR_SDA_BIT)
#define I2C_IBMR_SCL_STATUS_BIT							(1 << I2C_IBMR_SCL_BIT)



#define I2C_BIT_SET     								1
#define I2C_BIT_CLEAR   								0


typedef enum
{
    SLOW_MODE_ENABLE = 0x7fff,
    FAST_MODE_ENABLE = 0x8000
}I2C_FAST_MODE;


//ICAT EXPORTED ENUM
typedef enum
{
    I2C_RC_OK,
    I2C_RC_NOT_OK,
    I2C_RC_INVALID_DATA_SIZE,
    I2C_RC_INVALID_DATA_PTR,
    I2C_RC_TOO_MANY_REGISTERS,
    I2C_RC_TIMEOUT_ERROR,                               // 5
    I2C_RC_CHIP_BUSY,                                   // 6
    I2C_RC_INVALID_GENERAL_CALL_SLAVE_ADDRESS,
	I2C_RC_UNREGISTER_ERR,
	I2C_RC_MESSAGE_QUEUE_IS_FULL,
    I2C_ISR_UNEXPECTED_INTERRUPT,                       // 0xA
	I2C_ISR_BUS_ERROR,                                  // 0xB
	I2C_ISR_BUS_BUSY,                                   // 0xC
	I2C_ISR_CALL_BACK_FUNCTION_ERR,
	I2C_ISR_ARBITRATION_LOSS,
	I2C_RC_ILLEGAL_USE_OF_API
}I2C_ReturnCode;


typedef struct
{
    UINT8_T 			  			scl;    	// i2c_clk
    UINT8_T 			  			sda; 		// i2c_data
}  I2C_IBMR;


typedef struct I2C_CONTEXT_S
{
	unsigned long			I2CRegsBase;
	volatile unsigned long	*MfprSCLAddr;
	volatile unsigned long	*MfprSDAAddr;
	unsigned long			MfprSCLorigVal;
	unsigned long			MfprSDAorigVal;
	unsigned long			MySlaveAddress;
} I2C_CONTEXT_T;


typedef struct
{
    UINT8 							activeSlaveAddress;
    UINT8 							*RxBufferPtr;
    UINT16 							dataSize;
    UINT16 							ID;
} I2C_ReceiveRequestParams;


/*************** Prototypes *****************/

// use this function to initialize the unit.
I2C_ReturnCode I2CInit( 
	I2C_CONTEXT_T			*pI2CContext,			// memory provided by caller.
	unsigned long			 I2CRegsBase,			// platform/unit dependent.
	volatile unsigned long	*MfprSCLAddr,			// address of mfpr for scl
	volatile unsigned long	*MfprSDAAddr,			// address of mfpr for sda
	unsigned long			 MfprSCLVal,			// mfpr value to select scl functionality
	unsigned long			 MfprSDAVal,			// mfpr value to select sda functionality.
	unsigned long			 SlaveAddress
	);

// use this function to issue write-only transactions.
I2C_ReturnCode I2CMasterSendDataDirect( 
	I2C_CONTEXT_T			*pI2CContext,			// same structure passed to I2CInit
	UINT8_T					*data ,					// data bytes to send. Target slave address is always first.
	UINT16_T				 dataSize ,				// number of bytes, including slave address, to send.
	UINT8_T					 slaveAddress			// target of write
);

// use this function to issue read-only transactions (set cmdLength to 0).
// also use this transaction to issue write-read transactions.
I2C_ReturnCode I2CMasterReceiveDataDirect ( 
	I2C_CONTEXT_T			*pI2CContext,			// same structure passed to I2CInit
	UINT8_T 				*cmd,					// data bytes to send. Target slave address is always first.
	UINT16_T 	 			 cmdLength,				// number of bytes, including slave address, to send. for read-only, set to 0.
	UINT8_T 				 writeSlaveAddress,		// target of write
	UINT8_T 				*designatedRxBufferPtr,	// buffer to hold received bytes.
	UINT16_T 	 			 dataSize,				// number of bytes to receive.
	UINT8_T 				 readSlaveAddress		// target of read request.
	);


// internal routines, not likely to be called from the application level:
I2C_ReturnCode I2CConfigure( I2C_CONTEXT_T *pI2CContext );
void I2CBusStateGetDirect ( I2C_CONTEXT_T *pI2CContext, I2C_IBMR   *i2c_ibmr );
UINT16_T I2CIntLISRDirect ( I2C_CONTEXT_T *context );


/*----------------------------------------------------------------------*/

#endif  /* _I2C_H_ */
