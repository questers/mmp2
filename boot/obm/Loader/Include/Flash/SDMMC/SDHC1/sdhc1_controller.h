/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/


#ifndef __SDHC1_CONTROLLER_H
#define __SDHC1_CONTROLLER_H

#include "Typedef.h"

// MMC controller registers definition
typedef struct 
{
  VUINT32_T mm4_sysaddr;                  		// DMA system address				 		0x0
  VUINT32_T mm4_blk_cntl;                       // Block size control register		 		0x4
  VUINT32_T mm4_arg;                      		// Command argument					 		0x8
  VUINT32_T mm4_cmd_xfrmd;                      // Command and transfer mode		 		0xC
  VUINT32_T mm4_resp0;                          // cmd response 0					 		0x10
  VUINT32_T mm4_resp1;                          // cmd response 1					 		0x14
  VUINT32_T mm4_resp2;                          // cmd response 2					 		0x18
  VUINT32_T mm4_resp3;                          // cmd response 3					 		0x1C
  VUINT32_T mm4_dp;                             // buffer data port					 		0x20
  VUINT32_T mm4_state;                          // mm4 state						 		0x24
  VUINT32_T mm4_cntl1;                          // host control 1					 		0x28
  VUINT32_T mm4_cntl2;                          // host control 2					 		0x2C
  VUINT32_T mm4_i_stat;                         // index of current command 		 		0x30
  VUINT32_T mm4_i_stat_en;                      // interrupt status enable			 		0x34
  VUINT32_T mm4_i_sig_en;                       // interrupt signal enable			 		0x38
  VUINT32_T mm4_acmd12_er;                      // auto cmd12 error status			 		0x3C
  VUINT32_T mm4_cap0;                           // capabilities 0					 		0x40
  VUINT32_T reserved_0;                         // reserved field							0x44
  VUINT32_T mm4_cur_cap0;                       // max current capabilities 0				0x48
  VUINT32_T reserved_1;                         // reserved field							0x4C
  VUINT32_T mm4_force_event;              		// force event for AutoCMD12 Error Status	0x50
  VUINT32_T mm4_adma_err_stat;                  // ADMA Error Status						0x54
  VUINT32_T mm4_adma_system_address[2];      	// ADMA Address 63:0						0x58
  VUINT32_T reserved_X1[32];                    // reserved fields							0x60
  VUINT32_T mm4_sd_fifo_param;          		// FIFO Parameters Register					0xE0
  VUINT32_T mm4_sd_spi_mode;                   	// SD/SPI Mode								0xE4
  VUINT32_T mm4_sd_ce_ata;                   	// SD CE ATA Mode							0xE8
  VUINT32_T mm4_sd_pad_io_setup;                // SD_PAD_IO_SETUP							0xEC
  VUINT32_T reserved_X2[3];                     // reserved fields							0xF0
  VUINT32_T mm4_sd_slot_int_stat_ctrl_ver;      // SD Interrupt Line and Version Support	0xFC
} MM4_SDMMC_T, *P_MM4_SDMMC_T;

/*************************** Register Masks ********************************/
/**************************************************************************/
// ******************** MM4_BLK_CNTL **********************************
typedef struct
{
 unsigned int xfr_blksz       : 12;       // Transfer Block Size
 unsigned int dma_bufsz       : 4;        // Host DMA buffer size
 unsigned int blk_cnt         : 16;       // Block Count for Current Transfer
} MM4_BLK_CNTL, *P_MM4_BLK_CNTL;

#define MM4_512_HOST_DMA_BDRY  0x7

// ********************* MM4_CMD_XFRMD ********************************
typedef struct
{
 unsigned int dma_en          : 1;        // DMA enable							0
 unsigned int blkcbten        : 1;        // Block Count Enable					1
 unsigned int autocmd12       : 1;        // AutoCMD12      					2
 unsigned int reserved1       : 1;        //									3
 unsigned int dxfrdir         : 1;        // Data Transfer Direction Select 	4
 unsigned int ms_blksel       : 1;        // Multi Block Select					5
 unsigned int reserved2       : 10;        //									6
 unsigned int res_type        : 2;        // Response Type						16
 unsigned int reserved3       : 1;  	  //									18
 unsigned int crcchken        : 1;        // CRC check enable					19
 unsigned int idxchken        : 1;        // Command Index Check Enable			20
 unsigned int dpsel           : 1;        // Data present   select				21
 unsigned int cmd_type        : 2;        // Cmd Type							22
 unsigned int cmd_idx         : 6;        // Cmd Index							24
 unsigned int reserved4       : 2;        //									30
} MM4_CMD_XFRMD, *P_MM4_CMD_XFRMD;

typedef union
{
	MM4_CMD_XFRMD mm4_cmd_xfrmd_bits;
	UINT_T		  mm4_cmd_xfrmd_value;
} MM4_CMD_XFRMD_UNION, *P_MM4_CMD_XFRMD_UNION; 

#define NO_ARGUMENT					0xffffffff
#define MM4_CMD_TYPE_NORMAL 		0
#define MM4_CMD_TYPE_SUSPEND  		1
#define MM4_CMD_TYPE_RESUME 		2
#define MM4_CMD_TYPE_ABORT 			3
#define MM4_CMD_DATA 				1
#define MM4_CMD_NODATA				0
#define MM4_NO_RES					0
#define MM4_136_RES					1
#define MM4_48_RES					2
#define MM4_48_RES_WITH_BUSY		3

// this information will be included in the response type argument of relevant apis.
// it will occupy bits 15:8 of the RespType parameter.
#define MM4_RT_MASK					0x7f00
#define MM4_RT_NONE					0x0000
#define	MM4_RT_R1					0x0100
#define	MM4_RT_R2					0x0200
#define	MM4_RT_R3					0x0300
#define	MM4_RT_R4					0x0400
#define	MM4_RT_R5					0x0500
#define	MM4_RT_R6					0x0600
#define	MM4_RT_R7					0x0700		// sd card interface condition

#define MM4_RT_BUSYMASK				0x8000
#define MM4_RT_BUSY					0x8000


#define MM4_MULTI_BLOCK_TRAN		1
#define MM4_SINGLE_BLOCK_TRAN		0
#define MM4_HOST_TO_CARD_DATA		0
#define MM4_CARD_TO_HOST_DATA		1

// ********************* MM4_STATE ********************************
typedef struct
{
 unsigned int ccmdinhbt		: 1;
 unsigned int dcmdinhbt		: 1;
 unsigned int datactv		: 1;
 unsigned int reserved0		: 5;
 unsigned int wractv		: 1;
 unsigned int rdactv		: 1;
 unsigned int bufwren		: 1;
 unsigned int bufrden		: 1;
 unsigned int reserved1		: 4;
 unsigned int cdinstd		: 1;
 unsigned int cdstbl		: 1;
 unsigned int cddetlvl		: 1;
 unsigned int wpswlvl		: 1;
 unsigned int lwrdatlvl		: 4;
 unsigned int cmdlvl		: 1;
 unsigned int uprdatlvl		: 4;
 unsigned int reserved2		: 3;
} MM4_STATE, *P_MM4_STATE;

// ********************* MM4_CNTL1 ********************************
typedef struct
{
 unsigned int ledcntl 		: 1;
 unsigned int fourbitmd		: 1;
 unsigned int hispeed		: 1;
 unsigned int reserved0		: 2;
 unsigned int eightbitmd	: 1;
 unsigned int reserved1		: 2;
 unsigned int buspwr		: 1;
 unsigned int vltgsel		: 3;
 unsigned int reserved2		: 4;
 unsigned int bgreqstp		: 1;
 unsigned int contreq		: 1;
 unsigned int rdwcntl		: 1;
 unsigned int bgirqen		: 1;
 unsigned int reserved3		: 12;
} MM4_CNTL1, *P_MM4_CNTL1;

typedef union
{
	MM4_CNTL1     mm4_cntl1_bits;
	UINT_T		  mm4_cntl1_value;
} MM4_CNTL1_UNION, *P_MM4_CNTL1_UNION; 

#define MM4_VLTGSEL_1_8		0x5
#define MM4_VLTGSEL_3_0		0x6
#define MM4_VLTGSEL_3_3		0x7

// ********************* MM4_CNTL2 ********************************
typedef struct
{
 unsigned int inter_clk_en		: 1;	// Internal Clock Enable
 unsigned int inter_clk_stable	: 1;	// Internal Clock Stable
 unsigned int mm4clken			: 1;	// Clock Enable
 unsigned int reserved1			: 5;	//
 unsigned int sdfreq			: 8;	// Clock Frequency Select
 unsigned int dtocntr			: 4;	// Data Timeout Counter Value
 unsigned int reserved2			: 4;	//
 unsigned int mswrst			: 1;	// Software Reset for All
 unsigned int cmdswrst			: 1;	// Software Reset for MM4CMD Line
 unsigned int datswrst			: 1;	// Software Reset for MM4DATx Lines
 unsigned int reserved3			: 5;	//
} MM4_CNTL2, *P_MM4_CNTL2;

#define MM4CLKEN_EN_CLOCK    1
#define MM4CLOCK187KHZRATE	0x80	
#define MM4CLOCK374KHZRATE	0x40	
#define MM4CLOCK52MHZRATE	0x0
#define MM4CLOCK26MHZRATE	0x1
#define MM4CLOCK13MHZRATE	0x2
#define MM4CLOCK6MHZRATE	0x3

#define CLOCK_27_MULT		0xE

typedef union
{
	MM4_CNTL2     mm4_cntl2_bits;
	UINT_T		  mm4_cntl2_value;
} MM4_CNTL2_UNION, *P_MM4_CNTL2_UNION; 

// ********************* MM4_I_STAT, MM4_I_STAT_EN, MM4_I_SIGN_EN ************
typedef struct
{
 unsigned int cmdcomp 		: 1;   //0
 unsigned int xfrcomp		: 1;   //1
 unsigned int bgevnt		: 1;   //2
 unsigned int dmaint		: 1;   //3
 unsigned int bufwrrdy		: 1;   //4
 unsigned int bufrdrdy		: 1;   //5
 unsigned int cdins			: 1;   //6
 unsigned int cdrem			: 1;   //7
 unsigned int cdint			: 1;   //8
 unsigned int reserved0		: 6;   //9
 unsigned int errint		: 1;   //15
 unsigned int ctoerr		: 1;   //16
 unsigned int ccrcerr		: 1;   //17
 unsigned int cenderr		: 1;   //18
 unsigned int cidxerr		: 1;   //19
 unsigned int dtoerr		: 1;   //20
 unsigned int dcrcerr		: 1;   //21
 unsigned int denderr		: 1;   //22
 unsigned int ilmterr		: 1;   //23
 unsigned int ac12err		: 1;   //24
 unsigned int admaerr		: 1;   //25
 unsigned int reserved1		: 2;   //26
 unsigned int spierr		: 1;   //28
 unsigned int axi_resp_err	: 1;   //29
 unsigned int cpl_tout_err  : 1;   //30
 unsigned int crc_stat_err	: 1;   //31
} MM4_I_STAT, *P_MM4_I_STAT, MM4_I_STAT_EN, *P_MM4_I_STAT_EN, MM4_I_SIGN_EN, *P_MM4_I_SIGN_EN;

#define DISABLE_INTS 	0
#define ENABLE_INTS		1

typedef union
{
	MM4_I_STAT 	  mm4_i_stat_bits;
	UINT_T		  mm4_i_stat_value;
} MM4_I_STAT_UNION, *P_MM4_I_STAT_UNION; 
//#define CLEAR_INTS_MASK		0xFFFF7FFD
#define CLEAR_INTS_MASK		0xFFFF7FCD


// ********************* MM4_ACMD12_ER *******************************************
typedef struct
{
 unsigned int ac12nexe		: 1;
 unsigned int ac12toer		: 1;
 unsigned int ac12crcer		: 1;
 unsigned int ac12ender		: 1;
 unsigned int ac12idxer		: 1;
 unsigned int reserved0		: 2;
 unsigned int cmdnisud		: 1;
 unsigned int reserved1		: 24;
} MM4_ACMD12_ER, *P_MM4_ACMD12_ER;

// ********************* MM4_CAP0 *******************************************
typedef struct
{
 unsigned int toclkfreq		: 6;
 unsigned int reserved0		: 1;
 unsigned int toclkunit		: 1;
 unsigned int bsclkfreq		: 6;
 unsigned int reserved1 	: 18;
} MM4_CAP0, *P_MM4_CAP0;

typedef union
{
	MM4_CAP0 	  mm4_cap0_bits;
	UINT_T		  mm4_cap0_value;
} MM4_CAP0_UNION, *P_MM4_CAP0_UNION; 

// ********************* MM4_CUR_CAP0 *******************************************
typedef struct
{
 unsigned int v3_3vmaxi		: 8;
 unsigned int v3_0vmaxi		: 8;
 unsigned int v1_8vmaxi		: 8;
 unsigned int reserved0 	: 8;
} MM4_CUR_CAP0, *P_MM4_CUR_CAP0;

typedef union
{
	MM4_CUR_CAP0 	  	mm4_cur_cap0_bits;
	UINT_T		  		mm4_cur_cap0_value;
} MM4_CUR_CAP0_UNION, *P_MM4_CUR_CAP0_UNION; 

// ********************* MM4_SD_CE_ATA *******************************************

typedef struct
{
 unsigned int cpl_timeout			: 14;	// Bits 0-13
 unsigned int reserved0				: 2;	// Bit 14,15
 unsigned int reserved1				: 4;	// Bits 0-3
 unsigned int cpl_complete_int_en 	: 1;	// Bit 4
 unsigned int cpl_complete_en 		: 1;	// Bit 5
 unsigned int cpl_complete	 		: 1;	// Bit 6
 unsigned int reserved2				: 1;	// Bit 7
 unsigned int mmc_width				: 1;	// Bit 8
 unsigned int mmc_ddr				: 1;	// Bit 9
 unsigned int reserved3				: 2;	// Bit 10,11
 unsigned int mmc_card				: 1;	// Bit 12
 unsigned int mmc_ceata_card		: 1;	// Bit 13
 unsigned int snd_cpl				: 1;	// Bit 14
 unsigned int chk_cpl				: 1;	// Bit 15
} MM4_SD_CE_ATA, *P_MM4_SD_CE_ATA;

typedef union
{
	MM4_SD_CE_ATA 	  	MM4_SD_CE_ATA_bits;
	UINT_T		  		MM4_SD_CE_ATA_value;
} MM4_SD_CE_ATA_UNION, *PMM4_SD_CE_ATA_UNION; 

// ********************* MM4_SD_FIFO_PARAM *******************************************

#define DISABLE_PAD_SD_CLOCK_GATING_BIT			(1 << 10)		// 1 means disable dynamic clock gating.
#define ENABLE_DYNAMIC_CLOCK_GATING_MASK		~(DISABLE_PAD_SD_CLOCK_GATING_BIT)

/*********** End MM4 Register Def's **************************************/
// response types
enum {
  MMC_RESPONSE_NONE = 1L<<8,
  MMC_RESPONSE_R1 = 2L<<8,
  MMC_RESPONSE_R1B = 3L<<8,
  MMC_RESPONSE_R2 = 4L<<8,
  MMC_RESPONSE_R3 = 5L<<8,
  MMC_RESPONSE_R4 = 6L<<8,
  MMC_RESPONSE_R5 = 7L<<8,
  MMC_RESPONSE_R5B = 8L<<8,
  MMC_RESPONSE_R6 = 9L<<8,
  MMC_RESPONSE_R7 = 0xAL<<8,
  MMC_RESPONSE_MASK = 0x0000FF00
};

#define SD_OCR_VOLTAGE_3_3_TO_3_6 	0xE00000
#define SD_OCR_VOLTAGE_1_8_TO_3_3 	0x1F8000
#define SD_OCR_VOLTAGE_1_8		 	0x80
#define MMC_OCR_VOLTAGE_ALL			0xFF8000
#define	MM4FIFOWORDSIZE				0x80

// device context for MMC API, containing anything needed to operate on 
// this API. It is always first parameter for all APIs.
typedef struct 
{
  P_MM4_SDMMC_T            	pMMC4Reg;				// MMC1 register base
} MM4_SDMMC_CONTEXT_T, *P_MM4_SDMMC_CONTEXT_T;


//Function Prototypes
void MMC4StopInternalBusClock(P_MM4_SDMMC_CONTEXT_T pContext);
void MMC4StartInternalBusClock(P_MM4_SDMMC_CONTEXT_T pContext);
void MMC4StartBusClock(P_MM4_SDMMC_CONTEXT_T pContext);
void MMC4StopBusClock (P_MM4_SDMMC_CONTEXT_T pContext);
void MMC4SetBusRate(P_MM4_SDMMC_CONTEXT_T pContext, UINT_T rate);
void MMC4EnableDisableIntSources(P_MM4_SDMMC_CONTEXT_T pContext, UINT8_T Desire);
void MMC4SetDataTimeout(P_MM4_SDMMC_CONTEXT_T pContext, UINT8_T CounterValue);
void MMC4CMDSWReset(P_MM4_SDMMC_CONTEXT_T pContext);
void MMC4DataSWReset(P_MM4_SDMMC_CONTEXT_T pContext);
void MMC4FullSWReset(P_MM4_SDMMC_CONTEXT_T pContext);
void MMC4SendDataCommand(P_MM4_SDMMC_CONTEXT_T pContext, 
                  UINT_T Cmd,
                  UINT_T  Argument, 
                  UINT_T BlockType,
                  UINT_T DataDirection,
                  UINT_T ResType, 
                  UINT_T SDMAMode);

void MMC4SendDataCommandNoAuto12(P_MM4_SDMMC_CONTEXT_T pContext, 
                  UINT_T Cmd,
                  UINT_T  Argument, 
                  UINT_T BlockType,
                  UINT_T DataDirection,
                  UINT_T ResType, 
                  UINT_T SDMAMode);

void MMC4SendSetupCommand(P_MM4_SDMMC_CONTEXT_T pContext, 
                  UINT_T Cmd,
                  UINT_T CmdType,  
                  UINT_T  Argument, 
                  UINT_T ResType);
#endif