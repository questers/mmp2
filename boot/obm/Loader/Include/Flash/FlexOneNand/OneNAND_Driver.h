/******************************************************************************
**  COPYRIGHT  2007 Marvell Inernational Ltd.
**  All Rights Reserved
******************************************************************************/

#ifndef __ONENAND_DRIVER_H__
#define __ONENAND_DRIVER_H__

#include "Typedef.h"
#include "Flash.h"
#include "Errors.h"
#include "PlatformConfig.h"
#include "tim.h"
#include "misc.h"

#define MUX_SLC_BLOCK_SIZE		128*KBYTES
#define FLEX_SLC_BLOCK_SIZE		256*KBYTES
#define FLEX_MLC_BLOCK_SIZE		512*KBYTES


#define FLEX_DATARAM_MAIN_BASE	(FLEX_BOOTRAM_MAIN_BASE + 0x400)
#define FLEX_BOOTRAM_SPARE_BASE	(FLEX_BOOTRAM_MAIN_BASE + 0x10000)
#define FLEX_DATARAM_SPARE_BASE	(FLEX_BOOTRAM_MAIN_BASE + 0x10020)
#define FLEX_ONENAND_REG_BASE   (FLEX_BOOTRAM_MAIN_BASE + 0x1E000)

#define NUMBER_OF_DEVICES = 4;

#define SBR_WRITE_DEFAULT		0x0800
#define INT_STAT_REG_CLEAR		0x0000
#define INT_STAT_REG			0x8000
#define INT_RSTI_PEND			0x10
#define INT_READ                0x0080
#define INT_WRITE				0x0040
#define INT_ERASE				0x0020
#define INT_STAT_REG_RSTI_PEND	(INT_STAT_REG | INT_RSTI_PEND)
#define PEND_READ               (INT_STAT_REG | INT_READ)
#define PEND_WRITE				(INT_STAT_REG | INT_WRITE)
#define PEND_ERASE				(INT_STAT_REG | INT_ERASE)
#define RESET_TIMEOUT_MS		2
#define INT_CLEAR_RETRY_CNT 	3
#define ONENAND_TIMEOUT_MS 		1000
#define ONENAND_DEFAULT_PART 	0

//Start Address 1 Register Flags and Masks
#define FLEX_SA1R_DFS_FLAG		0x8000
#define FLEX_SA1R_FBA_MASK		0x03FF
#define MUX_SA1R_FBA_MASK		0x07FF

//Flex Start Address 2 Register Flags and Masks
#define FLEX_SA2R_DBS_FLAG		0x8000

//Flex Start Buffer Register Flags and Masks
#define FLEX_SBR_BSA_MASK		0x0F00
#define FLEX_SBR_BSC_MASK		0x0007


//Flex Controller Status Register Flags
#define FLEX_CSR_ONGO			0x8000
#define FLEX_CSR_ERROR			0x0400
#define ONENAND_CSR_ERROR       0x0400
#define FLEX_CSR_PI_LOCK		0x0100
#define FLEX_CSR_PREV_PROG		0x0004
#define FLEX_CSR_CURR_PROG		0x0002
#define FLEX_CSR_TIMEOUT		0x0001

//Flex Write Protection Status Register Flags
#define FLEX_WPSR_UNLOCKED		0x0004
#define FLEX_WPSR_LOCKED		0x0002
#define FLEX_WPSR_LOCKTIGHT		0x0001

#define MEGABYTES 				1024 * 1024
#define KBYTES					1024
#define SECTOR_SIZE_IN_BYTES	512

#define ONENAND_NUM_DATA_BUF	0xff00

#define CONF1_ECC_BIT               0x0100
#define CONF1_WRITE_DEFAULT			0x40C0

#define ECC_OFF_FLAG			(0 << 1)
#define ECC_ON_FLAG				(1 << 1)

#define ECC_BLOCK_SIZE 		4

#define VALID_BLK_MARK      0xFFFF

#define FLEX_ECCSR_ER_MASK	0xF1

typedef struct FLEX_REGISTERS_S
{
    VUINT16_T MANFID;				// Manufacturer ID Register		  			000
    VUINT16_T DEVID;				// Device ID Register			  			002
    VUINT16_T VERSID;         		// Version ID Register			  			4
    VUINT16_T DBS;		            // Data Buffer Size Register	  			6
    VUINT16_T BBS;         			// Boot Buffer Size Register	  			8
    VUINT16_T BUFAMT;            	// Amount of data/boot buffers Register		A
    VUINT16_T TECH;					// Technology Register						C
	VUINT16_T RESERVED[249];		// Reserved for user						E
	VUINT16_T SA1;					// Start address 1 Register					200
	VUINT16_T SA2;					// Start address 2 Register					202
	VUINT16_T RESERVED1[5];			// Reserved									204
    VUINT16_T SA8;       			// NAND Flash Page & Sector Address			20E
    VUINT16_T RESERVED2[248];		// Reserved									210
    VUINT16_T SBR;       		// Sector number for the page data transfer from the memory and the BufferRAM	400
    VUINT16_T RESERVED3[31];     	// Reserved									402
    VUINT16_T COMMAND;            	// Host control and memory operation commands	440
    VUINT16_T SYSCONF1;     		// Memory and host interface configuration		442
    VUINT16_T RESERVED4[14];       	// Reserved for user							444
    VUINT16_T RESERVED5[16];     	// Reserved for vendor specific purposes		460
    VUINT16_T CNTSTAT;            	// Controller status							480
    VUINT16_T INTRPT;        		// Command completion interrupt status			482
    VUINT16_T RESERVED6[10];    	// Reserved for user							484
    VUINT16_T SBA;					// Start block address							498
    VUINT16_T RESERVED8;  		   	// Reserved										49A
    VUINT16_T WPS;           		// Write protection status						49C
    VUINT16_T RESERVED9[3249];     	// Reserved for user							49E
    VUINT16_T ECCSR1;          		// ECC status of sector 0 and sector 1
    VUINT16_T ECCSR2;     			// ECC status of sector 2 and sector 3
    VUINT16_T ECCSR3;				// ECC status of sector 4 and sector 5
    VUINT16_T ECCSR4;				// ECC status of sector 6 and sector 7
    VUINT16_T RESERVED10[251]; 		// Reserved for vendor specific purposes
                                         
} FLEX_REGISTERS_T, *P_FLEX_REGISTERS_T;

typedef struct ONE_NAND_REGISTERS_S
{
    VUINT16_T MANFID;				// Manufacturer ID Register
    VUINT16_T DEVID;				// Device ID Register
    VUINT16_T VERSID;         		// Version ID Register
    VUINT16_T DBS;		            // Data Buffer Size Register
    VUINT16_T BBS;         			// Boot Buffer Size Register
    VUINT16_T BUFAMT;            	// Amount of data/boot buffers Register
    VUINT16_T TECH;					// Technology Register
	VUINT16_T RESERVED[249];		// Reserved for user
	VUINT16_T SA1;					// Start address 1 Register
	VUINT16_T SA2;					// Start address 2 Register
	VUINT16_T SA3;					// Start address 3 Register
	VUINT16_T SA4;					// Start address 4 Register
	VUINT16_T SA5;					// Start address 5 Register
	VUINT16_T SA6;					// Start address 6 Register
	VUINT16_T SA7;					// Start address 7 Register
    VUINT16_T SA8;       			// NAND Flash Page & Sector Address
    VUINT16_T RESERVED2[248];		// Reserved
    VUINT16_T SBR;       		// Sector number for the page data transfer from the memory and the BufferRAM
    VUINT16_T RESERVED3[7];     	// Reserved
	VUINT16_T RESERVED4[24];     	// Reserved
    VUINT16_T COMMAND;            	// Host control and memory operation commands
    VUINT16_T SYSCONF1;     		// Memory and host interface configuration
	VUINT16_T SYSCONF2;     		// NA
    VUINT16_T RESERVED5[13];       	// Reserved for user
    VUINT16_T RESERVED6[16];     	// Reserved for vendor specific purposes
    VUINT16_T CNTSTAT;            	// Controller status
    
    VUINT16_T INTRPT;        		// Command completion interrupt status
    VUINT16_T RESERVED7[10];    		// Reserved for user
    VUINT16_T SBA;					// Start block address
    VUINT16_T RESERVED8;  		   	// Reserved
    VUINT16_T WPS;           		// Write protection status
    VUINT16_T RESERVED9[3244];     	// Reserved for user
    VUINT16_T ECCSR;          		// ECC status of sector
    VUINT16_T ECCRMAD1;     		// ECC error position of Main area data error for 1st selected sector
    VUINT16_T ECCRSAD1;				// ECC error position of Spare area data error for 1st selected sector
	VUINT16_T ECCRMAD2;     		// ECC error position of Main area data error for 2nd selected sector
    VUINT16_T ECCRSAD2;				// ECC error position of Spare area data error for 2nd selected sector
	VUINT16_T ECCRMAD3;     		// ECC error position of Main area data error for 3rd selected sector
    VUINT16_T ECCRSAD3;				// ECC error position of Spare area data error for 3rd selected sector
	VUINT16_T ECCRMAD4;     		// ECC error position of Main area data error for 4th selected sector
    VUINT16_T ECCRSAD4;				// ECC error position of Spare area data error for 4th selected sector
    VUINT16_T ECCSR4;				// ECC status of sector 6 and sector 7
    VUINT16_T RESERVED10[247]; 		// Reserved for vendor specific purposes
                                         
} ONE_NAND_REGISTERS_T, *P_ONE_NAND_REGISTERS_T;

// Device ID Register F001h
//*************************** 
struct ONENAND_DEVICEID_DEF  {
	int ONENAND_VCC					:2;
	unsigned int ONENAND_MUXED		:1;
	unsigned int ONENAND_SINGLE		:1;
	int ONENAND_DENSITY				:4;
	unsigned int ONENAND_BOTTOMBOOT	:1;

};

typedef union {
	unsigned int value;
	struct ONENAND_DEVICEID_DEF bits;
}ONENAND_DEVICEID_REG;

struct FLEX_DEVICEID_DEF  {
	int FLEX_VCC					:2;
	unsigned int FLEX_MUXED			:1;
	unsigned int FLEX_SINGLE		:1;
	int FLEX_DENSITY				:4;
	int FLEX_SEPARATION				:2;

};

typedef union {
	unsigned int value;
	struct FLEX_DEVICEID_DEF bits;
}FLEX_DEVICEID_REG;

// Start Address 1 Register F100h
//*******************************
struct FLEX_SA1_DEF  {
	int FLEX_FBA					:10;
	int FLEX_RESERVED				:5;
	unsigned FLEX_DFS 				:1;
};

typedef union {
	unsigned int value;
	struct FLEX_SA1_DEF bits;
}FLEX_SA1_REG;

struct ONE_NAND_SA1_DEF  {
	int ONE_NAND_FBA		  		:11;
	int ONE_NAND_RESERVED	  		:4;
	unsigned ONE_NAND_DFS		 	:1;
};

typedef union {
	unsigned int value;
	struct ONE_NAND_SA1_DEF bits;
}ONE_NAND_SA1_REG;

// Start Address 2 Register F101h
//*******************************
struct FLEX_SA2_DEF  {
	int FLEX_RESERVED2				:15;
	unsigned FLEX_DBS 				:1;
};

typedef union {
	unsigned int value;
	struct FLEX_SA2_DEF bits;
}FLEX_SA2_REG;

struct ONE_NAND_SA2_DEF  {
	int ONE_NAND_RESERVED	  		:15;
	unsigned ONE_NAND_DBS		 	:1;
};

typedef union {
	unsigned int value;
	struct ONE_NAND_SA2_DEF bits;
}ONE_NAND_SA2_REG;

// Start Address 8 Register F107h
//*******************************
struct FLEX_SA8_DEF  {
	int FLEX_FSA					:2;
	int FLEX_FPA					:7;
	int FLEX_RESERVED3				:7;
};

typedef union {
	unsigned int value;
	struct FLEX_SA8_DEF bits;
}FLEX_SA8_REG;

struct ONE_NAND_SA8_DEF  {
	int ONE_NAND_FSA				:2;
	int ONE_NAND_FPA				:6;
	int ONE_NAND_RESERVED3			:8;
};

typedef union {
	unsigned int value;
	struct ONE_NAND_SA8_DEF bits;
}ONE_NAND_SA8_REG;

// Start Buffer Register F200h
//****************************
struct FLEX_SB_DEF  {
	int FLEX_BSC					:3;
	int FLEX_RESERVED4				:5;
	int FLEX_BSA					:4;
	int FLEX_RESERVED5				:4;
};

typedef union {
	unsigned int value;
	struct FLEX_SB_DEF bits;
}FLEX_SB_REG;

struct ONE_NAND_SB_DEF  {
	int ONE_NAND_BSC				:2;
	int ONE_NAND_RESERVED4			:6;
	int ONE_NAND_BSA				:4;
	int ONE_NAND_RESERVED5			:4;
};

typedef union {
	unsigned int value;
	struct FLEX_SB_DEF bits;
}ONE_NAND_SB_REG;

// System Configuration 1 Register F221h
//**************************************
struct SYS_CONFIG1_DEF  {
	unsigned int BWPS				:1;
	unsigned int WM					:1;
	unsigned int HF   				:1;
	unsigned int RESERVED8			:1;
	unsigned int RDYConf			:1;
	unsigned int IOBE				:1;
	unsigned int INTpol				:1;
	unsigned int RDYpol				:1;
	unsigned int ECC				:1;
	int BL							:3;
	int BRWL				   		:3;
	unsigned int RM		   			:1;
};

typedef union {
	unsigned int value;
	struct SYS_CONFIG1_DEF bits;
}SYS_CONFIG1_REG;

// Interrupt Status Register F241h
//********************************
struct INTERRUPT_DEF  {
	int RESERVED6					:4;
	unsigned int RSTI 				:1;
	unsigned int EI 				:1;
	unsigned int WI					:1;
	unsigned int RI					:1;
	int RESERVED7					:7;
	int INT							:1;
};

typedef union {
	unsigned int value;
	struct INTERRUPT_DEF bits;
}INTERRUPT_REG;														   

//Commands
typedef	enum
{
	FLEX_LOAD_PAGE_INTO_BUFFER		= 0x0,
   	FLEX_LOAD_PAGE_FROM_BUFFER		= 0x3,
	FLEX_LSB_READ_PI_UPDATE			= 0x5,	
	FLEX_PROGRAM_PAGE_FROM_BUFFER	= 0x0080,
	FLEX_READ_ID					= 0x90,
	FLEX_CACHE_PROGRAM				= 0x7F,
	FLEX_UNLOCK_NAND_BLOCK 			= 0x23,
	FLEX_LOCK_NAND_BLOCK	  		= 0x2A,
	FLEX_LOCK_TIGHT_NAND_BLOCK 		= 0x2C,
	FLEX_ALL_BLOCK_UNLOCK 			= 0x27,
	FLEX_BLOCK_ERASE		 		= 0x94,
	FLEX_ERASE_SUSPEND	 			= 0xB0,
	FLEX_ERASE_RESUME	 			= 0x30,
	FLEX_NAND_CORE_RESET			= 0xF0,
	FLEX_MUX_ONENAND_RESET 			= 0xF3,
	FLEX_OTP_ACCESS		 			= 0x65,
	FLEX_ACCESS_PARTITION_BLOCK		= 0x66

} FLEX_CMD;

typedef	enum
{
	ONE_NAND_LOAD_SECTOR_INTO_BUFFER		= 0x0000,
	ONE_NAND_LOAD_SPARE_SECTOR_INTO_BUFFER 	= 0x13,
	ONE_NAND_PROGRAM_SECTOR_FROM_BUFFER		= 0x0080,
	ONE_NAND_READ_ID						= 0x90,
	ONE_NAND_PROGRAM_SPARE_FROM_BUFFER		= 0x1A,
	ONE_NAND_COPY_BACK_PROGRAM_OPERATION	= 0x1B,
	ONE_NAND_PROGRAM_OPERATION_2X			= 0x7D,
	ONE_NAND_CACHE_PROGRAM_OPERATION_2X		= 0x7F,
	ONE_NAND_UNLOCK_NAND_BLOCK 				= 0x23,
	ONE_NAND_LOCK_NAND_BLOCK	  			= 0x2A,
	ONE_NAND_LOCK_TIGHT_NAND_BLOCK 			= 0x2C,
	ONE_NAND_ALL_BLOCK_UNLOCK 				= 0x27,
	ONE_NAND_ERASE_VERIFY_READ				= 0x71,
	ONE_NAND_CACHE_READ						= 0xE,
	ONE_NAND_FINISH_CACHE_READ				= 0xF,
	ONE_NAND_SYNC_BURST_BLOCK_READ			= 0xA,
	ONE_NAND_BLOCK_ERASE		 			= 0x94,
	ONE_NAND_MULTI_BLOCK_ERASE				= 0x95,
	ONE_NAND_ERASE_SUSPEND	 				= 0xB0,
	ONE_NAND_ERASE_RESUME	 				= 0x30,
	ONE_NAND_CORE_RESET		 				= 0xF0,
	ONE_NAND_RESET			 				= 0xF3,
	OTP_ACCESS		 						= 0x65

} ONE_NAND_CMD;

typedef enum {ONE_NAND, FLEX_ONE_NAND} FLEX_DEVICE_TYPE;

typedef enum {SINGLE_CHIP = 0, DUAL_CHIP} FLEX_MUX_TYPE;

typedef struct
{
 	FLEX_DEVICE_TYPE		DeviceType;
	FLEX_MUX_TYPE			MuxType;
	unsigned int			BlkBoundary;
}FlexOneNAND_Properties_T, *P_FlexOneNAND_Properties_T;

UINT_T InitializeFlexOneNANDDevice(UINT8_T FlashNumber, FlashBootType_T FlashBootType, UINT8_T* P_DefaultPartitionNum);
UINT_T Finalize_Flashes (FlashBootType_T FlashBootType);
UINT_T Read_Flash(UINT_T FlashOffset, UINT_T buffer, UINT_T size, FlashBootType_T fbt);
UINT_T Write_Flash(UINT_T FlashOffset, UINT_T buffer, UINT_T size, FlashBootType_T fbt);
UINT_T Erase_Flash(UINT_T FlashOffset, UINT_T size, FlashBootType_T fbt);
UINT_T ResetONENAND(FlashBootType_T fbt);
UINT_T ReadOTP(UINT_T FlashOffset, UINT_T buffer, UINT_T size, FlashBootType_T fbt);
UINT_T Write_OTP(UINT_T FlashOffset, UINT_T buffer, UINT_T size, FlashBootType_T fbt);
UINT_T LockOTP(FlashBootType_T fbt);
UINT_T UnlockNANDArrays();
UINT_T UnlockNANDArray(UINT8_T Core);

UINT_T WaitForCommandCompletion( UINT16 eventGroup, UINT32 TimeOutMillisec );
void GetFlexBlockInfo(UINT_T Offset, UINT_T* BlockNum, UINT_T* BlockSize);
void ChangeFlexPartition(UINT_T PartNum, FlashBootType_T fbt);
UINT_T SetECCUsage(UINT_T OnOff, VUINT16_T* SysCfgReg);

#endif 
//__ONENAND_DRIVER_H_
//
//_
