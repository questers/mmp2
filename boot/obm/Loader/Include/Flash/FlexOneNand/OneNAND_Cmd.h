/******************************************************************************
**	(C)Copyright Marvell. All Rights Reserved. 2007  
**
**  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
**	The copyright notice above does not evidence any actual or intended  
**  publication of such source code.	
**
**  This Module contains Proprietary Information of Marvell and should be treated 
**  as Confidential.
**	  
**  The information in this file is provided for the exclusive use of the licensees 
**  of Marvell.	
**	Such users have the right to use, modify, and incorporate this code into products 
**  for purposes authorized by the license agreement provided they include this notice 
**  and the associated copyright notice with any such product. 
**  The information in this file is provided "AS IS" without warranty.
**
**  FILENAME: FlexOneNAND_Cmd.c
**
**  PURPOSE:  The driver for Flex One NAND device.
**
******************************************************************************/
#ifndef __ONENAND_CMD_H__
#define __ONENAND_CMD_H__

#include "Typedef.h"
#include "Flash.h"

UINT_T LoadSector(UINT_T FlashOffset, UINT_T buffer, UINT_T size, FlashBootType_T fbt);
UINT_T LoadSpareSector();
UINT_T ProgramSector(UINT_T address, UINT_T buffer, UINT_T size, FlashBootType_T fbt);
UINT_T ProgramSpareSector();
UINT_T CopyBackProgram();
UINT_T LockNANDArray();
UINT_T LockTightNANDArray();
UINT_T EraseVerifyRead();
UINT_T BlockErase( UINT32 blockNumber);
UINT_T MultiBlockErase();
UINT_T EraseSuspend();
UINT_T EraseResume();
UINT_T ResetNANDCore();
UINT_T ResetOneNAND();
UINT_T OTPAccess();
UINT32 ComputeDivisorShiftFromPowerOf2( UINT32 power );

#endif __ONENAND_CMD_H__

