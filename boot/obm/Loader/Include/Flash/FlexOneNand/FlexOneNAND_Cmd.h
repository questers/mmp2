/******************************************************************************
**	(C)Copyright Marvell. All Rights Reserved. 2007  
**
**  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
**	The copyright notice above does not evidence any actual or intended  
**  publication of such source code.	
**
**  This Module contains Proprietary Information of Marvell and should be treated 
**  as Confidential.
**	  
**  The information in this file is provided for the exclusive use of the licensees 
**  of Marvell.	
**	Such users have the right to use, modify, and incorporate this code into products 
**  for purposes authorized by the license agreement provided they include this notice 
**  and the associated copyright notice with any such product. 
**  The information in this file is provided "AS IS" without warranty.
**
**  FILENAME: FlexOneNAND_Cmd.c
**
**  PURPOSE:  The driver for Flex One NAND device.
**
******************************************************************************/
#ifndef __FLEXONENAND_CMD_H__
#define __FLEXONENAND_CMD_H__

#include "Typedef.h"
#include "Flash.h"

UINT_T LoadFlexPage(UINT_T FlashOffset, UINT_T buffer, UINT_T size, FlashBootType_T fbt);
UINT_T SuperLoadFlexPage();
UINT_T LSBPageRecoveryRead();
UINT_T PIUpdate( UINT32 blockNumber );
UINT_T PIRead(UINT16_T *Status);
UINT_T ProgramFlexPage(UINT_T address, UINT_T buffer, UINT_T size, FlashBootType_T fbt);
UINT_T FinishCacheProgram();
UINT_T CacheProgram();
UINT_T UnlockFlexArray();
UINT_T LockFlexArray();
UINT_T LockTightFlexArray();
UINT_T FlexBlockErase( UINT32 BlockNumber);
UINT_T FlexEraseSuspend();
UINT_T FlexEraseResume();
UINT_T ResetFlexNANDCore();
UINT_T ResetFlexOneNAND();

#endif __FLEXONENAND_CMD_H__
