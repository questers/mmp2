
/******************************************************************************
**
** INTEL CONFIDENTIAL
** Copyright 2003-2004 Intel Corporation All Rights Reserved.
**
** The source code contained or described herein and all documents
** related to the source code (Material) are owned by Intel Corporation
** or its suppliers or licensors.  Title to the Material remains with
** Intel Corporation or its suppliers and licensors. The Material contains
** trade secrets and proprietary and confidential information of Intel
** or its suppliers and licensors. The Material is protected by worldwide
** copyright and trade secret laws and treaty provisions. No part of the
** Material may be used, copied, reproduced, modified, published, uploaded,
** posted, transmitted, distributed, or disclosed in any way without Intel's
** prior express written permission.
**
** No license under any patent, copyright, trade secret or other intellectual
** property right is granted to or conferred upon you by disclosure or
** delivery of the Materials, either expressly, by implication, inducement,
** estoppel or otherwise. Any license under such intellectual property rights
** must be express and approved by Intel in writing.
**
**  FILENAME: xllp_dfc_config.h
**
**  PURPOSE:  XLLP DFC configuration file.
**            Anything configurable should be placed here.
**
******************************************************************************/

#ifndef __XLLP_DFC_CONFIG_H__
#define __XLLP_DFC_CONFIG_H__

#define DFC_TIMEOUT 10000000 // Wait for a while...


//
//	Structures for setting padring values
//
//		One holds the address offsets of the multifunction pad control registers
//		One holds the values for the particular platform.
//	If space is an issue reduce the array size to the minimum number of processors
//	the structures take up about 184 bytes per processor.
//

typedef enum {
	MHP,
	TAVOR,
//	MHL,
//	MHLV,
//	MHG,
	NUMBER_PROCESSORS
} PROCESSOR_TYPE;

//
// Stuff to handle specific devices
//
typedef enum {
   SAMSUNG_CODE = 0xEC,
//   SANDISK_CODE = 0xFF,  // NOTE: find this out
   TOSHIBA_CODE = 0x98,
   HYNIX_CODE = 0XAD,
   ST_CODE = 0X20,
   MICRON_CODE = 0X2C
   } MAKER_CODE;

typedef enum {LARGE, SMALL, DEVICE_TYPE_SIZE} DEVICE_TYPE;


/*
 *  Pre-set structures for commands
 * 			Since the boot ROM only uses a few commands they 
 *			can be kept generic.  If necessary commands could
 * 			be defined for a particular manufacture.
 *
 */

typedef struct {
	NDCB0_REG read;
	NDCB0_REG read_id;
	NDCB0_REG read_status;
	NDCB0_REG pg_program;	
	NDCB0_REG blk_erase;
	NDCB0_REG reset;
}CMD_BLOCK;




#endif



