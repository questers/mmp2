/****************************************************************************
 *
 *  (C)Copyright 2005 - 2010 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code. This Module contains Proprietary 
 *  Information of Marvell and should be treated as Confidential. The 
 *  information in this file is provided for the exclusive use of the
 *  licensees of Marvell. Such users have the right to use, modify, and 
 *  incorporate this code into products for purposes authorized by the 
 *  license agreement provided they include this notice and the associated 
 *  copyright notice with any such product.
 *
 *  The information in this file is provided "AS IS" without warranty.
 *
 ***************************************************************************/
#ifndef INCLUDE_BOOT_LOADER_SECURITY_H
#define INCLUDE_BOOT_LOADER_SECURITY_H

#include "tim.h"

typedef UINT_T (*func_compute_digest)(const UINT8_T* msg, INT_T mlen, 
                                     UINT8_T* digest, UINT_T algo);
typedef UINT_T (*func_verify_platform)(pTIM ptim);
typedef UINT_T (*func_verify_jtag_key)(pTIM ptim);
typedef UINT_T (*func_validate_tim)(pTIM ptim);
typedef UINT_T (*func_validate_image)(const UINT8_T* data, UINT_T img_id, 
                                     pTIM ptim, void* paras);
typedef UINT_T (*func_perform_platbind)(pTIM ptim, void* param);
typedef UINT_T (*func_perform_autoconf)(pTIM ptim, void* param);
typedef UINT_T (*func_read_profile_fuse)(pTIM ptim, UINT_T addr);

typedef struct bl_security_funcs 
{
   func_compute_digest		compute_digest;
   func_verify_platform		verify_platform;
   func_verify_jtag_key		verify_jtag_key;
   func_validate_tim		validate_tim;
   func_validate_image		validate_image;
   func_perform_platbind	perform_platbind;
   func_perform_autoconf	perform_autoconf;
   func_read_profile_fuse	read_profile_fuse;
} bl_security_funcs;

extern bl_security_funcs bl_security_funcs_api;

UINT_T bl_security_api_init(void* para);
UINT_T bl_security_api_shutdown(void* para);

#endif /* INCLUDE_BOOT_LOADER_SECURITY_H */
