
/******************************************************************************
**
**  COPYRIGHT (C) 2006 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by  
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:	TrustedBoot.h
**
**  PURPOSE: 	Contain structures for trusted boot operation
**              
******************************************************************************/

#ifndef __trustedboot_h
#define __trustedboot_h

#include "tim.h"

//****************************************************************************
//
//B0 Integrated Verification Module definitions
//
//****************************************************************************

typedef struct 
{
 UINT_T KeyLength;
 UINT_T RSAPubExp_Mod_SPW_Pad[208];		// Contents depend on PublicKeySize, up to 2K bits	
 UINT8_T GeneratedPassword[8];			// 64 bits of generated password
 UINT8_T JTAGKeySHAOutput[40];
}JTAGKey, *pJTAGKey;


#endif
