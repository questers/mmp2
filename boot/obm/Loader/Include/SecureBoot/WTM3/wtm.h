/////////////////////////////////////////////////////////////////////////////
//                                    NOTICE                               //
//                                                                         //
//            COPYRIGHT MARVELL INTERNATIONAL LTD. AND ITS AFFILIATES      //
//                             ALL RIGHTS RESERVED                         //
//                                                                         //
//    The source code for this computer program is  CONFIDENTIAL  and a    //
//    TRADE SECRET of MARVELL  INTERNATIONAL  LTD. AND  ITS  AFFILIATES    //
//    ('MARVELL'). The receipt or possession of  this  program does not    //
//    convey any rights to  reproduce or  disclose  its contents, or to    //
//    manufacture,  use, or  sell  anything  that it  may  describe, in    //
//    whole or in part, without the specific written consent of MARVELL.   //
//    Any  reproduction  or  distribution  of this  program without the    //
//    express written consent of MARVELL is a violation of the copyright   //
//    laws and may subject you to criminal prosecution.                    //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// $Id: wtm#design#microcode#include#wtm.h,v 1.60 2009-02-13 12:09:01-08 lgalasso Exp $
/////////////////////////////////////////////////////////////////////////////
#ifndef INCLUDE_WTM_H
#define INCLUDE_WTM_H

#include "wtm_typedef.h"

#define WTM_RELEASE  "0.00.16"

#if PRODUCTION_RELEASE
#define WTM_VERSION WTM_RELEASE
#else
#define WTM_VERSION WTM_RELEASE " of "__DATE__ " at "__TIME__
#endif

#define SZ_VERS_NUM sizeof( WTM_VERSION )

// absolute maximums; all other aspects will scale from these
//
#define NUM_CONTEXT_SLOTS     8
#define MAX_KEYSIZE           2048
#define MAX_HMAC_KEYSIZE      512
#define MAX_AES_KEYSIZE       256
/* original definition
#define MAX_RANDOM_BIT_STREAM      (2048/BYTES_PER_WORD)
*/
#define MAX_RANDOM_BIT_STREAM      ((256/8)/BYTES_PER_WORD) /* 256-bit */

// definitive list of primitives.  See sched1.c and sched2.c for parsing tables
typedef enum _PRIMITIVE
{
    WTM_RESET = 2,
    WTM_INIT,
    WTM_SELF_TEST,
    WTM_CLEAR,
    WTM_SLEEP,
    WTM_GET_TSR,
    WTM_LIFECYCLE_ADVANCE,
    WTM_LIFECYCLE_READ,

    WTM_GET_CONTEXT_INFO,         // 10
    WTM_LOAD_ENGINE_CONTEXT,
    WTM_STORE_ENGINE_CONTEXT,
    WTM_WRAP_CONTEXT,
    WTM_UNWRAP_CONTEXT,
    WTM_LOAD_ENGINE_CONTEXT_EXTERNAL,
    WTM_STORE_ENGINE_CONTEXT_EXTERNAL,
    WTM_PURGE_CONTEXT_CACHE,
    WTM_AES_INIT,
    WTM_AES_ZERORIZE,

    WTM_AES_PROCESS,              // 20
    WTM_AES_LOAD_KEY,
    WTM_AES_KEY_GEN,
    WTM_HASH_INIT,
    WTM_HASH_ZERORIZE,
    WTM_HASH_UPDATE,
    WTM_HASH_FINAL,
    WTM_HMAC_INIT,
    WTM_HMAC_ZERORIZE,
    WTM_HMAC_UPDATE,

    WTM_HMAC_FINAL,               // 30
    WTM_HMAC_LOAD_KEY,
    WTM_HMAC_KEY_GEN,
    WTM_DRBG_GEN_RAN_BITS,
    WTM_FIPS_TEST_DRBG,
    WTM_SET_BATCH_COUNT,
    WTM_ENABLE_TESTCHIP,
    WTM_SET_FIPS_MODE_PERMANENT,
    WTM_SET_NON_FIPS_MODE_PERMANENT,
    WTM_SWITCH_FIPS_MODE,

    WTM_DRBG_RESEED,              // 40
    WTM_RSASSA_PKCS1_V15_VERIFY,

#ifndef _PLATFORM_MORONA
    //Morona does not have the following PIs.
    WTM_FW_AUTHORIZE,
    WTM_OTP_WRITE,                      // 43
    WTM_OTP_READ,
    WTM_OTP_LOCK,
    WTM_OTP_GET_INFO,
    WTM_OEM_OTP_WRITE,
    WTM_OEM_OTP_READ,
    WTM_JTAG_KEY_BIND,
    WTM_JTAG_KEY_VERIFY,                // 50
#endif

    WTM_CONFIGURE_DMA,                  // 51

    WTM_JTAG_AUTH_REQUEST,
    WTM_JTAG_AUTH_RESPONSE,

    WTM_CREATE_KEY,
    WTM_LOAD_KEY,
    WTM_ENROLL_KEY,

    WTM_OEM_PLATFORM_BIND,              // 57
    WTM_OEM_PLATFORM_VERIFY,
    WTM_RKEK_PROVISION,

    WTM_EMSA_PKCS1_V15_VERIFY_INIT,   // 60
    WTM_EMSA_PKCS1_V15_VERIFY_UPDATE,
    WTM_EMSA_PKCS1_V15_VERIFY_FINAL,

    WTM_MC_INIT,                        // 63
    WTM_MC_LOAD,
    WTM_MC_READ,
    WTM_MC_SAVE,

    WTM_DRBG_GEN_RAN_BITS_BOUNDED,      // 67
    WTM_AES_LOAD_IV,

#ifdef _PLATFORM_MORONA
    //new for Morona
    WTM_SOFTWARE_VERSION_ADVANCE,       // 69
    WTM_SOFTWARE_VERSION_READ,

    WTM_OEM_JTAG_UNLOCKING_KEY_WRITE,   // 71
    WTM_OEM_JTAG_UNLOCKING_KEY_READ,
    WTM_OEM_UNIQUE_ID_WRITE,
    WTM_OEM_UNIQUE_ID_READ,
    WTM_OTP_WRITE_PLATFORM_CONFIG,
    WTM_OTP_READ_PLATFORM_CONFIG,
    WTM_OEM_USBID_PROVISION,
    WTM_OEM_USBID_READ,                // 78
#endif

    WTM_PRIMITIVE_LAST_ONE
} PRIMITIVE;

// generic status code
typedef enum _STATUS
{
    STATUS_SUCCESS = 0,
    STATUS_FAILURE = 255,
    STATUS_NO_RESOURCES,                              // FIPS context
    STATUS_INVALID_CRYPTO_SCHEME,                     // non-FIPS context
    STATUS_UNSUPPORTED_FUNCTION,                      // GENERAL
    STATUS_UNSUPPORTED_PARAMETER,                     // GENERAL
    STATUS_PARAMETER_OUT_OF_RANGE,                    // GENERAL                 //260
    STATUS_PARITY_ERROR,                              // NA
    STATUS_FRAMING_ERROR,                             // NA
    STATUS_OVERRUN_ERROR,                             // NA
    STATUS_OFFSET_ERROR,                              // NA
    STATUS_IN_PROGRESS,                               // NA
    STATUS_RETRY_COUNT_EXCEEDED,                      // NA
    STATUS_DEVICE_CORRUPTED,                          // NA
    STATUS_BAD_ENGINE_OP,                             // engine
    STATUS_MODE_ERROR,                                // FIPS context/ECP
    STATUS_BAD_TRANSFER_SIZE,                         // DMA/HASH                //270
    STATUS_CONVERGENCE_ERROR,                         // RNG
    STATUS_FATAL_INTERNAL_ERROR,                      // Platform
    STATUS_IN_USE,                                    // AES/HASH/ECC
    STATUS_SHORT_XFR,                                 // AES
    STATUS_ECB_MODE_WITH_PARTIAL_CODEWORD,            // AES
    STATUS_AES_INVALID_BLOCK,                         // AES
    STATUS_NO_KEY,                                    // AES/HASH/FIPS context
    STATUS_NO_IV,                                     // AES
    STATUS_NO_CONTEXT,                                // HASH/ECP
    STATUS_INVALID_KEY_LENGTH,                        // HASH                    //280
    STATUS_INVALID_HASH_UPDATE_MESSAGE_LENGTH,        // HASH
    STATUS_HASH_MESSAGE_OVERFLOW,                     // HASH
    STATUS_UNSUPPORTED_DIGEST_TYPE,                   // HASH/ECP
    STATUS_INVALID_BINDING,                           // RSA/platform
    STATUS_INVALID_SIGNATURE,                         // RSA
    STATUS_SIGNATURE_REPRESENTATIVE_OUT_OF_RANGE,     // RSA
    STATUS_HASH_NOT_SUPPORTED,                        // RSA
    STATUS_INTENDED_ENCODED_MESSAGE_LENGTH_TOO_SHORT, // RSA
    STATUS_INTEGER_TOO_LARGE,                         // RSA
    STATUS_RSA_KEYGEN_EVEN_INPUT,                     // RSA                     //290
    STATUS_RSA_KEYGEN_ZMODP,                          // RSA
    STATUS_RSA_KEYGEN_TOO_MANY_TRIALS,                // RSA
    STATUS_RSA_KEYGEN_SUB_BN,                         // RSA
    STATUS_RSA_KEYGEN_MOD_INV_BAD_ARG,                // RSA
    STATUS_RSA_KEYGEN_MOD_INV_BAD_MOD,                // RSA
    STATUS_RSA_KEYGEN_MOD_INV_NULL_PTR,               // RSA
    STATUS_RSA_KEYGEN_MOD_INV_OUT_OF_RANGE,           // RSA
    STATUS_RSA_KEYGEN_GET_BN,                         // RSA
    STATUS_RSA_KEYGEN_DIV,                            // RSA
    STATUS_PENDING,                                   // ECP                     //300
    STATUS_ILLEGAL_KEY,                               // ECP/RNG/platform/AES/HMAC
    STATUS_ECC_ERROR,                                 // ECP/platform
    STATUS_ENGINE_CONTEXT_MISMATCH,                   // engine/key management
    STATUS_DMA_TIMEOUT,                               // DMA
    STATUS_DMA_BUS_ERROR,                             // DMA
    STATUS_DMA_PARITY_ERROR,                          // DMA
    STATUS_DMA_LINKED_LIST_ACCESS_ERROR,              // DMA
    STATUS_HASH_TIMEOUT,                              // HASH - time out
    STATUS_AES_TIMEOUT,                               // AES - time out
    STATUS_ZMODP_TIMEOUT,                             // RSA - time out	         //310
    STATUS_EC_TIMEOUT,                                // ECP - time out
    STATUS_DES_TIMEOUT,                               // DES - time out
    STATUS_MD5_TIMEOUT,                               // MD5 - time out
    STATUS_MCT_TIMEOUT,                               // MCT - time out
    STATUS_EBG_TIMEOUT,                               // EBG - time out
    STATUS_OTP_TIMEOUT,                               // OTP - time out
    STATUS_UNEXPECTED_IRQ,                            // Reserved
    STATUS_UNEXPECTED_FIQ,                            // Reserved
    STATUS_UNEXPECTED_DATA_ABORT,                     // Reserved
    STATUS_UNEXPECTED_PREFETCH_ABORT,                 // Reserved                //320
    STATUS_UNEXPECTED_SWI,                            // Reserved
    STATUS_UNEXPECTED_UNUSED,                         // Reserved
    STATUS_INVALID_REQUEST,                           // Reserved
    STATUS_ERROR_PERMANENT_FIPS,                      // FIPS management
    STATUS_ERROR_PERMANENT_NON_FIPS,                  // FIPS management
    STATUS_PERMISSION_VIOLATION,                      // FIPS management
    STATUS_LIFECYCLE_SPENT,                           // State management
    STATUS_SW_OUT_OF_RANGE,                           // State management
    STATUS_WTM_NONCE_NOT_GENERATED,                   // Key management
    STATUS_INVALID_KEY_ID,                            // Key management           //330
    STATUS_MISSING_ENDORSEMENT_KEY,                   // Key management
    STATUS_DIGEST_MISMATCH,                           // FIPS context/key management
    STATUS_BAD_THREAD_ID,                             // FIPS context
    STATUS_CONTEXT_CACHE_FULL,                        // FIPS context/key management
    STATUS_BAD_CACHE_ID,                              // FIPS context/key management
    STATUS_INVALID_CONTEXT_SLOT,                      // FIPS context
    STATUS_NO_SIGNING_KEY,                            // FIPS key management
    STATUS_SKEK_NOT_LOADED,                           // Reserved
    STATUS_TIM_R_SEGMENT_COMPROMISED,                 // Sliding window
    STATUS_DES_INVALID_BLOCK,                         // DES	                  //340
    STATUS_COMMAND_TIMEOUT = 4094,  // added for WTM_Test_Driver only
    STATUS_USER_DEFINED_1 = 5000,
    STATUS_USER_DEFINED_2,
    STATUS_USER_DEFINED_3,
    STATUS_LAST_ONE
} STATUS;


// DMA data-transfer descriptor, used for linked-list processing.
// Client constructs a queue of these and points
// 'primitive_command_parameter14' or 'primitive_command_parameter15'
// to the first one, thereby allowing a scatter-gather function to
// many primtives.
typedef struct _DTD
{
    U32 transfer_address;               // to/from address
    U32 transfer_size;                  // words to transfer
    struct _DTD *next;                  // pointer to next (or NULL)
    U32 reserved;                       // for microcode use
} DTD;

#define SZ_DTD sizeof( DTD )
#define SZW_DTD (SZ_DTD / BYTES_PER_WORD)

// the command mailbox, which appears in the Client's memory space
typedef struct _MAILBOX
{
    U32 primitive_command_parameter0;                  // 0x0 Write Only
    U32 primitive_command_parameter1;                  // 0x4 Write Only
    U32 primitive_command_parameter2;                  // 0x8 Write Only
    U32 primitive_command_parameter3;                  // 0xc Write Only
    U32 primitive_command_parameter4;                  // 0x10 Write Only
    U32 primitive_command_parameter5;                  // 0x14 Write Only
    U32 primitive_command_parameter6;                  // 0x18 Write Only
    U32 primitive_command_parameter7;                  // 0x1c Write Only
    U32 primitive_command_parameter8;                  // 0x20 Write Only
    U32 primitive_command_parameter9;                  // 0x24 Write Only
    U32 primitive_command_paramete10;                  // 0x28 Write Only
    U32 primitive_command_parameter11;                 // 0x2c Write Only
    U32 primitive_command_parameter12;                 // 0x30 Write Only
    U32 primitive_command_parameter13;                 // 0x34 Write Only
    U32 primitive_command_parameter14;                 // 0x38 Write Only: linked-list dma host-read pointer
    U32 primitive_command_parameter15;                 // 0x3c Write Only: linked-list dma host-write pointer
    U32 secure_processor_command;                      // 0x40 Write Only
    U8  reserved_0x44[60];
    U32 command_return_status;                         // 0x80 Read Only
    U32 command_status_0;                              // 0x84 Read Only
    U32 command_status_1;                              // 0x88 Read Only
    U32 command_status_2;                              // 0x8c Read Only
    U32 command_status_3;                              // 0x90 Read Only
    U32 command_status_4;                              // 0x94 Read Only
    U32 command_status_5;                              // 0x98 Read Only
    U32 command_status_6;                              // 0x9c Read Only
    U32 command_status_7;                              // 0xa0 Read Only
    U32 command_status_8;                              // 0xa4 Read Only
    U32 command_status_9;                              // 0xa8 Read Only
    U32 command_status_10;                             // 0xac Read Only
    U32 command_status_11;                             // 0xb0 Read Only
    U32 command_status_12;                             // 0xb4 Read Only
    U32 command_status_13;                             // 0xb8 Read Only
    U32 command_status_14;                             // 0xbc Read Only
    U32 command_status_15;                             // 0xc0 Read Only
    U32 command_fifo_status;                           // 0xc4 Read Only
    U32 host_interrupt_register;                       // 0xc8 Write-to-Clear
    U32 host_interrupt_mask;                           // 0xcc Write-to-Clear
    U32 host_exception_address;                        // 0xd0 Write-to-Clear
    U32 sp_trust_register;                             // 0xd4 Read Only
    U32 wtm_identification;                            // 0xd8 Read Only
    U32 wtm_revision;                                  // 0xdc Read Only
} MAILBOX;

#define SZ_MAILBOX sizeof( MAILBOX )
#define SZW_MAILBOX (SZ_MAILBOX/BYTES_PER_WORD)

// engine id, used to index into g-> engine_state[]
typedef enum _ENGINE_ID
{
    ENGINE_ID_NULL = 0,                       //  must be first and eval to zero
    ENGINE_ID_HASH,
    ENGINE_ID_AES,
    ENGINE_ID_ZMODP,
    ENGINE_ID_RC4,
    ENGINE_ID_ENTROPY,
    ENGINE_ID_PRNG,
    ENGINE_ID_ECC,
    ENGINE_ID_PRIMALITY,
    ENGINE_ID_DMA,
    ENGINE_ID_MTC,
    ENGINE_ID_SCRATCH_PAD,
    ENGINE_ID_LAST_ONE
} ENGINE_ID;

// how many engines to manage
#define NUM_ENGINES ENGINE_ID_LAST_ONE

// bitmapped engine IDs, derived from engine IDs

#define ENGINE_HASH        BIT( ENGINE_ID_HASH )
#define ENGINE_AES         BIT( ENGINE_ID_AES )
#define ENGINE_ZMODP       BIT( ENGINE_ID_ZMODP )
#define ENGINE_RC4         BIT( ENGINE_ID_AES )
#define ENGINE_ENTROPY     BIT( ENGINE_ID_RC4 )
#define ENGINE_PRNG        BIT( ENGINE_ID_PRNG )
#define ENGINE_ECC         BIT( ENGINE_ID_ECC )
#define ENGINE_PRIMALITY   BIT( ENGINE_ID_PRIMALITY )
#define ENGINE_DMA         BIT( ENGINE_ID_DMA )
#define ENGINE_MTC         BIT( ENGINE_ID_MTC )

#define ENGINE_ALL (ENGINE_HASH|ENGINE_AES|ENGINE_ZMODP|ENGINE_ENTROPY|ENGINE_PRNG|ENGINE_DMA)

typedef enum _LCS_STATE
{
    LCS_STATE_CM = 0,
    LCS_STATE_DM = 1,
    LCS_STATE_DD = 2,
    LCS_STATE_FA = 3 
} LCS_STATE;


// Trust Status Register Fields
#define TSR_OTP_RKEK_SECURE_OFFSET          0
#define TSR_OTP_RKEK_SECURE_MASK            0x1

#define TSR_OTP_PLATFORM_KEY_SECURE_OFFSET  1
#define TSR_OTP_PLATFORM_KEY_SECURE_MASK    0x2

#define TSR_OTP_JTAG_SECURE_OFFSET          2
#define TSR_OTP_JTAG_SECURE_MASK            0x4

#define TSR_OTP_JTAG_PERM_DISABLE_OFFSET    3
#define TSR_OTP_JTAG_PERM_DISABLE_MASK      0x8

//LCS: [5:4]
#define TSR_OTP_LCS_OFFSET                  4
#define TSR_OTP_LCS_MASK                    0x30

#ifndef _PLATFORM_MORONA
//Not for MORONA
//
#define TSR_OTP_DEGUB_OFFSET                6
#define TSR_OTP_DEGUB_MASK                  0x40

#define TSR_OTP_RKEK_UNLOCK_OFFSET          7
#define TSR_OTP_RKEK_UNLOCK_MASK            0x80

#define TSR_OTP_RKEK_ZEROIZE_OFFSET         8
#define TSR_OTP_RKEK_ZEROIZE_MASK           0x100

#define TSR_FOTA_PERM_DISABLE_OFFSET        9
#define TSR_FOTA_PERM_DISABLE_MASK          0x200

#define TSR_OEM_USER_PERM_DISABLE_OFFSET    10
#define TSR_OEM_USER_PERM_DISABLE_MASK      0x400

#define TSR_OTP_FOTA_SECURE_OFFSET            11
#define TSR_OTP_FOTA_SECURE_MASK            0x800

#define TSR_OTP_OEM_USER_SECURE_OFFSET      12
#define TSR_OTP_OEM_USER_SECURE_MASK        0x1000
//
#endif //#ifndef _PLATFORM_MORONA

#define TSR_OTP_PERM_FIPS_OFFSET            13
#define TSR_OTP_PERM_FIPS_MASK              0x2000

#define TSR_OTP_FIPS_PERM_DISABLE_OFFSET    14
#define TSR_OTP_FIPS_PERM_DISABLE_MASK      0x4000

//REG_OP_STATE: [17:15]
#define TSR_REG_OP_STATE_OFFSET             15
#define TSR_REG_OP_STATE_MASK               0x38000

#define TSR_REG_OP_TIM_R_OFFSET             18
#define TSR_REG_OP_TIM_R_MASK               0x40000

//[31:19] bits are reserved for future use.

////////////////////////////////////////////////////////////////////////////////
//// |<-- CAT -->|<--- PKCS -->|<--- HASH -->|<-- AES -->|
////               * *   KeyLen  * Mode Algo  Mode KeyLen
////    [15:12]   [11:10] [9:8] [7] [6] [5:4] [3:2] [1:0]
////
////  CAT[15:12]: 8 - AES
////              9 - HASH/HMAC
////              A - PKCS
////
////  [NOTE]  * means "reserved"
////
////  AES[3:0]:  Fields "KeyLen" and "Mode" match definitions of "KeyLen" and
////             "Mode" in register "AES_CFG_REG."
////
////      KeyLen[1:0]  00 - 128-bit
////                   01 - 256-bit
////                   10 - 192-bit
////                   11 - ERROR
////
////      Mode[3:2]    00 - ECB
////                   01 - CBC
////                   10 - CTR
////                   11 - XTS
////
////  HASH[7:4]: Fields "Algo" and "Mode" match definitions of "alg_select"
////             and "hash_mode" in register "Hash_Configure."
////
////      Algo[5:4]    00 - SHA1
////                   01 - SHA256
////                   10 - SHA224
////                   11 - MD5
////
////      Mode[6:6]    0  - Normal hash mode
////                   1  - HMAC
////
////  PKCS[11:8]
////      KeyLen[9:8]  01 - 1024-bit
////                   10 - 2048-bit
////////////////////////////////////////////////////////////////////////////////
typedef enum _Cryptographic_Scheme
{                                           //  CAT PKCS HASH  AES
    //AES                                        |    |    |    |
    AES_ECB128 = 0x00008000,                // 1000-0000-0000-0000
    AES_ECB192 = 0x00008002,                // 1000-0000-0000-0010
    AES_ECB256 = 0x00008001,                // 1000-0000-0000-0001
    AES_CBC128 = 0x00008004,                // 1000-0000-0000-0100
    AES_CBC192 = 0x00008006,                // 1000-0000-0000-0110
    AES_CBC256 = 0x00008005,                // 1000-0000-0000-0101
    AES_CTR128 = 0x00008008,                // 1000-0000-0000-1000
    AES_CTR192 = 0x0000800A,                // 1000-0000-0000-1010
    AES_CTR256 = 0x00008009,                // 1000-0000-0000-1001
    AES_XTS256 = 0x0000800C,                // 1000-0000-0000-1100
    AES_XTS512 = 0x0000800D,                // 1000-0000-0000-1101

#if 0
    //Z0 HASH/HMAC
    SHA_1        = 0x00009000,              // 1001-0000-0000-0000
    SHA_256      = 0x00009010,              // 1001-0000-0001-0000
    SHA_224      = 0x00009020,              // 1001-0000-0010-0000
    MD_5         = 0x00009030,              // 1001-0000-0011-0000
    HMAC_SHA_1   = 0x00009040,              // 1001-0000-0100-0000
    HMAC_SHA_224 = 0x00009060,              // 1001-0000-0110-0000
    HMAC_SHA_256 = 0x00009050,              // 1001-0000-0101-0000
    HMAC_MD_5    = 0x00009070,              // 1001-0000-0111-0000
#else
    //A0 HASH/HMAC
    SHA_1        = 0x00009000,              // 1001-0000-0000-0000
    SHA_256      = 0x00009010,              // 1001-0000-0001-0000
    SHA_224      = 0x00009020,              // 1001-0000-0010-0000
    MD_5         = 0x00009030,              // 1001-0000-0011-0000
    SHA_512      = 0x00009040,              // 1001-0000-0100-0000
    SHA_384      = 0x00009050,              // 1001-0000-0101-0000
    HMAC_SHA_1   = 0x00009080,              // 1001-0000-1000-0000
    HMAC_SHA_256 = 0x00009090,              // 1001-0000-1001-0000
    HMAC_SHA_224 = 0x000090A0,              // 1001-0000-1010-0000
    HMAC_MD_5    = 0x000090B0,              // 1001-0000-1011-0000
    HMAC_SHA_512 = 0x000090C0,              // 1001-0000-1100-0000
    HMAC_SHA_384 = 0x000090D0,              // 1001-0000-1101-0000
#endif

    //PKCS#1v1.5 Digital Signature
    PKCSv1_SHA1_1024RSA   = 0x0000A100,     // 1010-0001-0000-0000
    PKCSv1_SHA224_1024RSA = 0x0000A120,     // 1010-0001-0010-0000
    PKCSv1_SHA256_1024RSA = 0x0000A110,     // 1010-0001-0001-0000
    PKCSv1_SHA1_2048RSA   = 0x0000A200,     // 1010-0010-0000-0000
    PKCSv1_SHA224_2048RSA = 0x0000A220,     // 1010-0010-0010-0000
    PKCSv1_SHA256_2048RSA = 0x0000A210,     // 1010-0010-0001-0000

    //                                          ECC
    //                                          CAT FIELD HASH DH/DSA/MQV
    //ECCP                                       |    |    |    |
    ECCP224_FIPS_DH         = 0x0000B000,   // 1011-0000-0000-0000
    ECCP256_FIPS_DH         = 0x0000B100,   // 1011-0001-0000-0000
    ECCP384_FIPS_DH         = 0x0000B200,   // 1011-0010-0000-0000
    ECCP521_FIPS_DH         = 0x0000B300,   // 1011-0011-0000-0000

    ECCP224_FIPS_DSA_SHA1   = 0x0000B001,   // 1011-0000-0000-0001
    ECCP224_FIPS_DSA_SHA224 = 0x0000B021,   // 1011-0000-0010-0001
    ECCP224_FIPS_DSA_SHA256 = 0x0000B011,   // 1011-0000-0001-0001
	ECCP224_FIPS_DSA_SHA384 = 0x0000B051,   // 1011-0000-0101-0001
	ECCP224_FIPS_DSA_SHA512 = 0x0000B041,   // 1011-0000-0100-0001
	
    ECCP256_FIPS_DSA_SHA1   = 0x0000B101,   // 1011-0001-0000-0001
    ECCP256_FIPS_DSA_SHA224 = 0x0000B121,   // 1011-0001-0010-0001
    ECCP256_FIPS_DSA_SHA256 = 0x0000B111,   // 1011-0001-0001-0001
    ECCP256_FIPS_DSA_SHA384 = 0x0000B151,   // 1011-0001-0101-0001
	ECCP256_FIPS_DSA_SHA512 = 0x0000B141,   // 1011-0001-0100-0001
	
    ECCP384_FIPS_DSA_SHA1   = 0x0000B201,   // 1011-0010-0000-0001
    ECCP384_FIPS_DSA_SHA224 = 0x0000B221,   // 1011-0010-0010-0001
    ECCP384_FIPS_DSA_SHA256 = 0x0000B211,   // 1011-0010-0001-0001
	ECCP384_FIPS_DSA_SHA384 = 0x0000B251,   // 1011-0010-0101-0001
    ECCP384_FIPS_DSA_SHA512 = 0x0000B241,   // 1011-0010-0100-0001
    
    ECCP521_FIPS_DSA_SHA1   = 0x0000B301,   // 1011-0011-0000-0001
    ECCP521_FIPS_DSA_SHA224 = 0x0000B321,   // 1011-0011-0010-0001
    ECCP521_FIPS_DSA_SHA256 = 0x0000B311,   // 1011-0011-0001-0001
	ECCP521_FIPS_DSA_SHA384 = 0x0000B351,   // 1011-0011-0101-0001
    ECCP521_FIPS_DSA_SHA512 = 0x0000B341,   // 1011-0011-0100-0001

    //DES-TDES
    DES_ECB  = 0x0000C000,
    DES_CBC  = 0x0000C001,
    TDES_ECB = 0x0000C002,
    TDES_CBC = 0x0000C003,

    CRYPTOGRAPHIC_SCHEME_LAST_ONE,
} CRYPTO_SCHEME_ENUM;

typedef enum KEY_SELECTION
{
    AES_RKEK         = 0,
    AES_INTERNAL_KEY = 1,
    AES_EXTERNAL_KEY = 2,
} KEY_SELECTION;

#define ECCP521_ENABLE 1

#ifdef ECCP521_ENABLE
#define ECC_MAX_COEF_LEN 17
#else
#define ECC_MAX_COEF_LEN 8
#endif


typedef enum ECC_POINT_MULT_MODE
{
    ECC_MULT_RANDOM_MODE	= 0x0,
    ECC_MULT_FIXED_MODE 	= 0x1,
} ECC_POINT_MULT_MODE;

typedef struct _ECC_point{
	U32 FieldSize_in_bits;					// 1 word, Domain size in bits
	U32 CoefLength;							// 1 word, Coefficients length in WORDS
	U32 X[ECC_MAX_COEF_LEN];		// max 17 words, X cooridinate
	U32 Y[ECC_MAX_COEF_LEN];		// max 17 words, Y cooridinate
}ECC_point;
#define SZ_ECCP_POINT (  sizeof(ECC_point) )
#define SZW_ECCP_POINT (  sizeof(ECC_point)/BYTES_PER_WORD   )

#endif


/////////////////////////////////////////////////////////////////////////////
// $Log: wtm#design#microcode#include#wtm.h,v $
// Revision 1.60  2009-02-13 12:09:01-08  lgalasso
// ...No comments entered during checkin...
//
// Revision 1.59  2009-02-11 16:05:21-08  lgalasso
// added a new status.
//
// Revision 1.58  2009-02-11 11:26:49-08  lgalasso
// DTD modified to what hardware is actually using (again).
//
// Revision 1.57  2009-02-10 16:53:29-08  lgalasso
// DTD modified to what hardware is actually using.
//
// Revision 1.56  2009-02-04 11:52:01-08  lgalasso
// load_aes_key().
//
// Revision 1.55  2009-02-03 15:52:52-08  lgalasso
// added wtm_aes_load_iv(); fixed source/dest;
//
// Revision 1.54  2009-02-02 16:34:21-08  lgalasso
// renamed three pkcs PIs.
//
// Revision 1.53  2009-02-02 15:59:31-08  lgalasso
// added fifth parameter to dbrg() and wtm_drbg_test().
//
// Revision 1.52  2009-02-02 14:22:56-08  lgalasso
// was dbrg; is drbg
//
// Revision 1.51  2009-02-02 13:46:06-08  lgalasso
// dbrg implementation per fips standard.
//
// Revision 1.50  2009-01-30 17:57:49-08  lgalasso
// replaced one pi with another for fips testing.
//
// Revision 1.49  2009-01-27 17:30:59-08  lgalasso
// added new digest type.
//
// Revision 1.48  2009-01-23 17:01:18-08  lgalasso
// added new status for hash overflow.
//
// Revision 1.47  2009-01-20 08:54:12-08  lgalasso
// was sizeof( 32 ); is BYTES_PER_WORD
//
// Revision 1.46  2009-01-19 15:03:27-08  lgalasso
// added ALG_AES_XTS128
//
// Revision 1.45  2009-01-16 12:21:13-08  lgalasso
// added new digest types.
//
// Revision 1.44  2009-01-15 15:29:16-08  lgalasso
// ...No comments entered during checkin...
//
// Revision 1.43  2009-01-13 12:39:31-08  lgalasso
// added new timeout status codes.
//
// Revision 1.42  2009-01-12 16:54:46-08  lgalasso
// STATUS_ILLEGAL_KEY
//
// Revision 1.41  2009-01-06 10:27:37-08  lgalasso
// added 192-bit key-type.
//
// Revision 1.40  2008-12-19 16:34:35-08  lgalasso
// added primitive enums.
//
// Revision 1.39  2008-12-19 11:37:57-08  lgalasso
// removed all void pointers; removed all U8s; added a statii; tweaked some typos.
//
// Revision 1.38  2008-12-17 10:58:44-08  lgalasso
// added wtm_jtag_authorize_request() and
// wtm_jtag_authorize_response().
//
// Revision 1.37  2008-12-17 09:56:04-08  lgalasso
// recoded otp memory map; moved defined into platform.h;
// reordered otp enums.
//
// Revision 1.36  2008-12-16 14:00:29-08  lgalasso
// defect in otp_read(); added pkcs11 status; added erom load/entry-point;
// added forward_flag to wtm_hash_init().
//
// Revision 1.35  2008-12-12 16:33:58-08  lgalasso
// was OTP_OEM_PLATFORM_KEY_HASH; is OTP_PLATFORM_KEY_HASH.
//
// Revision 1.34  2008-12-12 16:21:15-08  lgalasso
// OTP physical map changed; refactored OTP enums to match map.
//
// Revision 1.33  2008-12-12 09:27:19-08  lgalasso
// added otp enums.
//
// Revision 1.32  2008-12-11 18:07:40-08  lgalasso
// added primitives
//
// Revision 1.31  2008-12-10 11:40:54-08  lgalasso
// otp read/write unit test; first-phase oiap.
//
// Revision 1.30  2008-12-08 12:04:18-08  lgalasso
// wtm_aes_process() now waits for aes to complete and then
// dma to complete; if aes has an error, abort dma operation.
//
// Revision 1.29  2008-12-08 11:14:01-08  lgalasso
// enable AES before starting it; added load/save aes iv; wtm_aes_process() now
// passed words to dma routine (was passing bytes).
//
// Revision 1.28  2008-12-05 14:41:39-08  lgalasso
// first phase of RSA integration.
//
// Revision 1.27  2008-12-03 13:38:31-08  lgalasso
// panic handling changes.
//
// Revision 1.26  2008-12-03 11:05:35-08  lgalasso
// moved engine id enum into here, so client may use it more readily.
//
// Revision 1.25  2008-12-02 14:27:32-08  lgalasso
// added wtm_dbrg_reseed.
//
// Revision 1.24  2008-11-25 16:44:20-08  lgalasso
// added/modified PI enums.
//
// Revision 1.23  2008-11-24 16:54:51-08  lgalasso
// added AESIV type and some error statii.
//
// Revision 1.22  2008-11-19 14:48:02-08  lgalasso
// added THREAD_ID type.
//
// Revision 1.21  2008-11-13 15:59:41-08  lgalasso
// status code.
//
// Revision 1.20  2008-11-13 09:36:00-08  lgalasso
// nonce tweak.
//
// Revision 1.12  2008-10-16 17:30:54-07  lgalasso
// synched up MAILBOX with WTM_BIU.
//
// Revision 1.10  2008-10-14 10:10:14-07  lgalasso
// updated mailbox.
//
// Revision 1.8  2008-09-23 16:23:43-07  lgalasso
// updated list of primitive instruction enums.
//
// Revision 1.6  2008-09-17 09:14:20-07  lgalasso
// added KEY_TYPE.
//
// Revision 1.5  2008-08-15 16:52:37-07  lgalasso
// added MAX_KEYSIZE; removed OTP_CELL_WIDTH.
//
// Revision 1.3  2008-08-12 17:09:34-07  lgalasso
// added new panic code.
//
// Revision 1.2  2008-07-17 14:49:34-07  lgalasso
// added support for a third set of microcode for simulation-only purposes.
//
// Revision 1.1  2008-07-07 14:35:44-07  lgalasso
// include file for all external (to WTM) code.
//
/////////////////////////////////////////////////////////////////////////////
