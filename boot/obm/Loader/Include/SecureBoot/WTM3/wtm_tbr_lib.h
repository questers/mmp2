/////////////////////////////////////////////////////////////////////////////
//                                    NOTICE                               //
//                                                                         //
//            COPYRIGHT MARVELL INTERNATIONAL LTD. AND ITS AFFILIATES      //
//                             ALL RIGHTS RESERVED                         //
//                                                                         //
//    The source code for this computer program is  CONFIDENTIAL  and a    //
//    TRADE SECRET of MARVELL  INTERNATIONAL  LTD. AND  ITS  AFFILIATES    //
//    ('MARVELL'). The receipt or possession of  this  program does not    //
//    convey any rights to  reproduce or  disclose  its contents, or to    //
//    manufacture,  use, or  sell  anything  that it  may  describe, in    //
//    whole or in part, without the specific written consent of MARVELL.   //
//    Any  reproduction  or  distribution  of this  program without the    //
//    express written consent of MARVELL is a violation of the copyright   //
//    laws and may subject you to criminal prosecution.                    //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
#ifndef WTM_TBR_LIB_H_
#define WTM_TBR_LIB_H_

#include "wtm_typedef.h"


/********************** Lib\WTM3\Platforms\mmp2\processor.h **********************/
#ifndef INCLUDE_CORTEX_H
#define BYTES_PER_WORD (sizeof( U32 ))
#endif // INCLUDE_CORTEX_H


/********************** Lib\WTM3\Include\core.h **********************/
#ifndef INCLUDE_CORE_H

typedef enum _ALGORITHM_TYPE
{
    ALG_SHA1,
    ALG_SHA224,
    ALG_SHA256,
    ALG_MD5,
    ALG_AES_ECB128,
    ALG_AES_ECB256,
    ALG_AES_CBC128,
    ALG_AES_CBC192,
    ALG_AES_CBC256,
    ALG_AES_CTR128,
    ALG_AES_CTR192,
    ALG_AES_CTR256,
    ALG_AES_XTS128,
    ALG_AES_XTS256,
} ALGORITHM_TYPE;

typedef enum _LIFECYCLE_STATE
{
    STATE_VIRGIN = 100, 
    STATE_CM     = 200, 
    STATE_DM     = 300, 
    STATE_DD     = 400, 
    STATE_FA     = 500
} LIFECYCLE_STATE;

// types of AES ops
typedef enum _AES_OP
{
    AES_ENCRYPT,
    AES_DECRYPT,
    AES_OP_LAST_ONE
} AES_OP;

#endif // INCLUDE_CORE_H

/**********************   Lib\WTM3\Include\hash.h **********************/
#ifndef INCLUDE_HASH_H

// various types of digests
typedef struct  _DIGEST_MD5
{
   U32 array[4];
} DIGEST_MD5;

#define SZW_DIGEST_MD5 (sizeof( DIGEST_MD5 ) / BYTES_PER_WORD)

typedef struct  _DIGEST_SHA1
{
    U32 array[5];
} DIGEST_SHA1;

#define SZW_DIGEST_SHA1 (sizeof( DIGEST_SHA1 ) / BYTES_PER_WORD)

typedef struct  _DIGEST_SHA224
{
    U32 array[7];
} DIGEST_SHA224;

#define SZW_DIGEST_SHA224 (sizeof( DIGEST_SHA224 ) / BYTES_PER_WORD)

typedef struct  _DIGEST_SHA256
{
    U32 array[8];
} DIGEST_SHA256;

#define SZW_DIGEST_SHA256 (sizeof( DIGEST_SHA256 ) / BYTES_PER_WORD)
 
typedef struct  _DIGEST_SHA384
{
    U32 array[12];
} DIGEST_SHA384;

#define SZW_DIGEST_SHA384 (sizeof( DIGEST_SHA384 ) / BYTES_PER_WORD)

typedef struct  _DIGEST_SHA512
{
    U32 array[16];
} DIGEST_SHA512;

#define SZW_DIGEST_SHA512 (sizeof( DIGEST_SHA512 ) / BYTES_PER_WORD)

typedef struct  _DIGEST
{
    union
    {
        DIGEST_MD5 digest_md5;
		DIGEST_SHA1 digest_sha1;
        DIGEST_SHA224 digest_sha224;
        DIGEST_SHA256 digest_sha256;
        DIGEST_SHA384 digest_sha384;
		DIGEST_SHA512 digest_sha512;
    } types;
} DIGEST;

#define SZ_DIGEST sizeof( DIGEST )   
#define SZW_DIGEST (SZ_DIGEST / BYTES_PER_WORD)

#endif // INCLUDE_HASH_H

/**********************   Lib\WTM3\Include\rng.h **********************/

#ifndef INCLUDE_RNG_H

// number of 32-bit words in a nonce 
#define SZW_WTM_NONCE (SZ_U256 / BYTES_PER_WORD)

// a generic, swiss-army-style nonce
// it's a 1024-bit element, which can
// be referenced bunches of ways
typedef struct _WTM_NONCE
{
    union
    {
        U32 nonce256[8];
        U32 nonce128[4];
        U64 nonce64;
        U32 nonce32;
        U16 nonce16;
        U8 nonce8;
        U32 array32[SZW_WTM_NONCE];
    } parts;
} WTM_NONCE;

// number of bytes in a U1024
#define SZ_WTM_NONCE sizeof( WTM_NONCE )
#define SZ_WTM_NONCE_IN_BITS SZ_WTM_NONCE*8

#endif // INCLUDE_RNG_H

/**********************   Lib\WTM3\Include\aes.h **********************/

#ifndef INCLUDE_AES_H
typedef struct _AESIV
{
    union
    {
        U128 iv128;
        U32 iv32[4];
    } parts;
} AESIV;

#define SZ_AESIV sizeof( AESIV )
#define SZW_AESIV (SZ_AESIV / BYTES_PER_WORD )

#endif // INCLUDE_AES_H

/**** The following typedefs are not used, should be safe to delete (Deniz)
// types of AES modes
typedef enum _AES_MODE
   {
   AES_ECB,
   AES_CBC,
   AES_CTR,
   AES_XTS,
   AES_MODES_LAST_ONE 
   } AES_MODE; 


// the OIAP packet
typedef struct _OIAP_PACKET
   {
   WTM_NONCE nonce;
   DIGEST digest;
   U32 handle;
   WTM_NONCE cient_nonce;
   U32 session_authorize;
   } OIAP_PACKET;
****************************************/




#endif /* WTM_TBR_LIB_H_ */
