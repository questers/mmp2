/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document.
**  Except as permitted by such license, no part of this document may be
**  reproduced, stored in a retrieval system, or transmitted in any form or by
**  any means without the express written consent of Intel Corporation.
**
**  FILENAME:   wtm_typedef.h
**
**  PURPOSE:    Contain WTM typedefs
**
******************************************************************************/

#ifndef _WTM_TYPEDEF_H_
#define _WTM_TYPEDEF_H_

// basic data types
typedef signed char             S8;				// signed 8-bit
typedef unsigned char           U8;				// unsigned 8-bit
typedef unsigned short          U16;			// unsigned 16-bit (half-word)
typedef short                   S16;			// signed 16-bit
typedef unsigned long           U32;			// unsigned 32-bit (word)
typedef long                    S32;			// signed 32-bit
typedef unsigned long long      U64;			// unsigned 64-bit
typedef signed long long        S64;			// signed 64-bit
typedef float                   FLOAT;			// floating-point number

// wtm specific types
typedef U32 U2048[64];                 // unsigned 2048-bit integer
typedef U32 U1024[32];                 // unsigned 1024-bit integer
typedef U32 U512[16];
typedef U32 U256[8];
typedef U32 U128[4];                   // unsigned 128-bit integer
typedef S32 S2048[64];                 // signed 2048-bit integer
typedef S32 S1024[32];                 // signed 1024-bit integer
typedef S32 S512[16];
typedef S32 S128[16];                  // signed 128-bit integer
typedef U2048 K2048;                   // 2048-bit keyblob
typedef U128 K1024;                    // 1024-bit keyblob
typedef U512 K512;                     // 256-bit keyblob
typedef U256 K256;                     // 256-bit keyblob
typedef U128 K128;                     // 128-bit keyblob
typedef U128 THREAD_ID;                // a thread id
typedef U32* KEYBLOB;                  // a non-descript whole or piece of key material

// sizes of above, in bytes
#define SZ_U2048  sizeof( U2048 )
#define SZ_U1024  sizeof( U1024 )
#define SZ_U256   sizeof( U256 )
#define SZ_U128   sizeof( U128 )
#define SZ_S2048  sizeof( S2048 )
#define SZ_S1024  sizeof( S1024 )
#define SZ_S128   sizeof( S128 )
#define SZ_K2048  sizeof( K2048 )
#define SZ_K1024  sizeof( K1024 )
#define SZ_K256   sizeof( K256 )
#define SZ_K128   sizeof( K128 )
#define SZ_THREAD_ID sizeof( THREAD_ID )

// same in words
#define SZW_U2048       (SZ_U2048 / BYTES_PER_WORD)
#define SZW_U1024       (SZ_U1024 / BYTES_PER_WORD)
#define SZW_U256        (SZ_U256 / BYTES_PER_WORD)
#define SZW_U128        (SZ_U128 / BYTES_PER_WORD)
#define SZW_S2048       (SZ_S2048 / BYTES_PER_WORD)
#define SZW_S1024       (SZ_S1024 / BYTES_PER_WORD)
#define SZW_S128        (SZ_S128 / BYTES_PER_WORD)
#define SZW_K2048       (SZ_K2048 / BYTES_PER_WORD)
#define SZW_K1024       (SZ_K1024 / BYTES_PER_WORD)
#define SZW_K256        (SZ_K256 / BYTES_PER_WORD)
#define SZW_K128        (SZ_K128 / BYTES_PER_WORD)
#define SZW_THREAD_ID   (SZ_THREAD_ID / BYTES_PER_WORD)


#endif //_WTM_TYPEDEF_H_
