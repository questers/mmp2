#ifndef _WTM3_SECURITY_H_
#define _WTM3_SECURITY_H_
/******************************************************************************
 *
 *  (C)Copyright 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 *
 ******************************************************************************/

UINT_T WTM3_SHAMessageDigest(const UINT8_T* pSrcMesg, INT_T mesglen, UINT8_T* pMD, HASHALGORITHMID_T HashID);
UINT_T WTM3_ValidateTIM(pTIM pTIM_h);
UINT_T WTM3_SecurityInitialization();
UINT_T WTM3_SecurityShutdown();
UINT_T WTM3_Platform_Verify (pTIM pTIM_h);
UINT_T WTM3_ValidateJTAG(UINT8_T *JTAG_key, UINT8_T *Debug_key);
void WTM3_ISR();

#endif //_GEU_INTERFACE_H_
