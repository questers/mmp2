/****************************************************************************
 *
 *  (C)Copyright 2005 - 2010 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code. This Module contains Proprietary 
 *  Information of Marvell and should be treated as Confidential. The 
 *  information in this file is provided for the exclusive use of the
 *  licensees of Marvell. Such users have the right to use, modify, and 
 *  incorporate this code into products for purposes authorized by the 
 *  license agreement provided they include this notice and the associated 
 *  copyright notice with any such product.
 *
 *  The information in this file is provided "AS IS" without warranty.
 *
 ***************************************************************************/
#ifndef INCLUDE_BL_PROVISIONING_IF_H
#define INCLUDE_BL_PROVISIONING_IF_H

#include "tim.h"
#include "bl_provisioning_if.h"

typedef struct _fuse_conf_spec_t {
    UINT_T  platform_id;           /* platform type                    */
    UINT_T  prg_cp_conf;           /* program cp config [true, false]  */
    UINT_T  plat_cp_conf[1];       /* cp config                        */
    UINT_T  cp_bit_cnt;            /* cp config bit count              */
    UINT_T  prg_ap_conf;           /* program ap config [true, false]  */
    UINT_T  plat_ap_conf[3];       /* ap config                        */
    UINT_T  ap_bit_cnt;            /* ap config bit count              */
    UINT_T  prg_usb_id;            /* program usb id [true, false]     */
    UINT_T  plat_usb_id[1];        /* usb id                           */
    UINT_T  usb_byte_cnt;          /* usb id bit count                 */
    UINT_T  plat_version[1];       /* advance version [true, false]    */
    UINT_T  plat_lifecycle[1];     /* advance life cycle [true, false] */
    UINT_T  prg_blk0_ecc;          /* program block 0 ecc              */
} fuse_conf_spec_t;


typedef enum _fuse_type_enum {
    enum_fuse_type_dummy = 1,
    enum_fuse_type_ap,
    enum_fuse_type_cp,
} fuse_type_enum;


typedef fuse_conf_spec_t (*func_fuse_conf_spec)(void* param);
typedef UINT_T (*func_bind_platform)(pTIM ptim, void* param);
typedef UINT_T (*func_platform_binded)(pTIM ptim, void* param);
typedef UINT_T (*func_config_platform)(UINT_T* conf, UINT_T bit_cnt, fuse_type_enum type, pTIM ptim, void* para);
typedef UINT_T (*func_read_platform_config)(UINT_T* conf, UINT_T* bit_cnt, fuse_type_enum type, void* para);
typedef UINT_T (*func_program_jtag_key)(UINT8_T* key, UINT_T len, pTIM ptim, void* para);
typedef UINT_T (*func_read_jtag_key)(UINT8_T* key, UINT_T* len, void* para); 
typedef UINT_T (*func_program_usb_id)(UINT8_T* usb, UINT_T len, void* para); 
typedef UINT_T (*func_read_usb_id)(UINT8_T* usb, UINT_T* len, void* para);
typedef UINT_T (*func_program_oem_unique_id)(UINT8_T* oem_id, UINT_T len, void* para);
typedef UINT_T (*func_read_oem_unique_id)(UINT8_T* oem_id, UINT_T* len, void* para);
typedef UINT_T (*func_advance_life_cycle)(UINT_T* lc, void* para);
typedef UINT_T (*func_read_life_cycle)(UINT_T* lc, void* para);
typedef UINT_T (*func_advance_version)(UINT_T* ver, void* para); 
typedef UINT_T (*func_read_version)(UINT_T* ver, void* para);   
typedef UINT_T (*func_autocfg_platform)(pTIM ptim, void* param);
typedef UINT_T (*func_setup_aib_override)(void* param);   
typedef UINT_T (*func_program_rkek)(void* param);   
typedef UINT_T (*func_program_ec521_dk)(void* param);


typedef struct bl_provisioning_funcs 
{
   func_fuse_conf_spec			fuse_conf_spec;
   func_bind_platform			bind_platform;
   func_platform_binded			platform_binded;
   func_config_platform			config_platform;
   func_read_platform_config	read_platform_config;
   func_program_jtag_key		program_jtag_key;
   func_read_jtag_key			read_jtag_key;
   func_program_usb_id			program_usb_id;
   func_read_usb_id				read_usb_id;
   func_program_oem_unique_id	program_oem_unique_id;
   func_read_oem_unique_id		read_oem_unique_id;
   func_advance_life_cycle		advance_life_cycle;
   func_read_life_cycle			read_life_cycle;
   func_advance_version			advance_version;
   func_read_version			read_version;
   func_autocfg_platform		autocfg_platform;
   func_setup_aib_override		setup_aib_override;
   func_program_rkek			program_rkek;
   func_program_ec521_dk		program_ec521_dk;
} bl_provisioning_funcs;


extern bl_provisioning_funcs bl_provisioning_funcs_api;

UINT bl_provisioning_api_init(void);
UINT bl_provisioning_api_shutdown(void);

#endif /* INCLUDE_BL_PROVISIONING_IF_H */
