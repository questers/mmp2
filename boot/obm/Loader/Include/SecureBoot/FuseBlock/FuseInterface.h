#ifndef _FUSE_INTERFACE_H_
#define _FUSE_INTERFACE_H_
/******************************************************************************
 *
 *  (C)Copyright 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 *
 ******************************************************************************/

// Fuse field sizes in bytes
#define K_SOC_CONFIG_FUSE_SIZE      16
#define K_USBID_FUSE_SIZE            4
#define K_LIFECYCLE_FUSE_SIZE        2
#define K_SOFTWARE_VERSION_FUSE_SIZE 4
#define K_OEM_HASH_FUSE_SIZE        32
#define K_JTAG_HASH_FUSE_SIZE       32
#define K_RKEK_FUSE_SIZE            32
#define K_FUSEBLOCK_SIZE            32

#define K_SHA1_SIZE     		 	20
#define K_SHA256_SIZE	        	32
#define K_JTAG_FIELD_LOW128_SIZE    16
#define K_JTAG_FIELD_HIGH128_SIZE   16   

#define K_DEBUG_KEY_SIZE            16

// JTAG fuse block fields
#define K_JTAG_FIELD_HASH_KEY        0   // Returns SHA1 or SHA256 size bits of JTAG fuse block
#define K_JTAG_FIELD_LOW128          1   // Returns bits [127:0] of JTAG fuse block
#define K_JTAG_FIELD_HIGH128         2   // Returns bits [255:128] of JTAG fuse block

// Fuse Block Numbers
#define K_SOC_CONFIG_FUSEBLOCK        0
//#define K_AP_CONFIG_FUSEBLOCK         0
//#define K_CP_CONFIG_FUSEBLOCK         0
#define K_USBID_FUSEBLOCK             0
//#define K_APCPUSB_FUSEBLOCK           0
#define K_LIFECYCLE_FUSEBLOCK         0
#define K_SOFTWARE_VERSION_FUSEBLOCK  0
#define K_RKEK_FUSEBLOCK              1
#define K_OEM_FUSEBLOCK               2
#define K_SQU0_FUSEBLOCK              3 // Not Supported
#define K_SQU1_FUSEBLOCK              4 // Not Supported
#define K_SQU2_FUSEBLOCK              5 // Not Supported
#define K_JTAG_FUSEBLOCK              6
#define K_ECC_FUSEBLOCK               7
#define K_NO_FUSEBLOCK              0xF
#define K_UNDEFINED_FUSEBLOCK         K_NO_FUSEBLOCK

// Burn Status Bit Masks/bits
#define K_AP_CONFIG_STATUS_BIT_MASK           0x001
#define K_CP_CONFIG_STATUS_BIT_MASK           0x002
#define K_USBID_STATUS_BIT_MASK               0x004
#define K_LIFECYCLE_STATUS_BIT_MASK           0x008
#define K_SOFTWARE_VERSION_STATUS_BIT_MASK    0x010
#define K_RKEK_STATUS_BIT_MASK                0x020
#define K_OEM_STATUS_BIT_MASK                 0x040
#define K_JTAG_STATUS_BIT_MASK                0x080
#define K_APCPUSB_ECC_STATUS_BIT_MASK         0x100
#define K_OEM_ECC_STATUS_BIT_MASK             0x200
#define K_JTAG_ECC_STATUS_BIT_MASK            0x400
#define K_LOCK_STATUS_BIT_MASK                0x800
// DebugStatus bits
#define K_FUSE_READY_TIMEOUT                  0x10000000
#define K_MULTIPLE_BURN_COUNT                 0x20000000
#define K_FUSE_BURN_TIMEOUT                   0x40000000


// Lock Bit Masks
#define K_FB0_FUSE_LOCK_BIT_MASK       0x01  //Bit 0/Block0
#define K_RKEK_FUSE_LOCK_BIT_MASK      0x02  //Bit 1/Block1
#define K_OEM_FUSE_LOCK_BIT_MASK       0x04  //Bit 2/block2
#define K_SQU0_FUSE_LOCK_BIT_MASK      0x08  //Bit 3/block3
#define K_SQU1_FUSE_LOCK_BIT_MASK      0x10  //Bit 4/block4
#define K_SQU2_FUSE_LOCK_BIT_MASK      0x20  //Bit 5/block5
#define K_JTAG_FUSE_LOCK_BIT_MASK      0x40  //Bit 6/Block6
#define K_ECC_FUSE_LOCK_BIT_MASK       0x80  //Bit 7/Block7

// LifeCycle/Software Version 
#define K_PHYSICAL_BITS_PER_LOGICAL_BIT       3
//
#define K_LIFECYCLE_BITSIZE                  16
#define K_MAX_LIFECYCLE_VALUE                K_LIFECYCLE_BITSIZE/K_PHYSICAL_BITS_PER_LOGICAL_BIT
#define K_MAX_LIFECYCLE_LOGICAL_BIT_OFFSET   K_MAX_LIFECYCLE_VALUE - 1
//
#define K_SOFTWARE_VERSION_BITSIZE           64
#define K_MAX_SOFTWARE_VERSION_VALUE         K_SOFTWARE_VERSION_BITSIZE/K_PHYSICAL_BITS_PER_LOGICAL_BIT
#define K_MAX_SOFTWARE_VERSION_LOGICAL_BIT_OFFSET   K_MAX_SOFTWARE_VERSION_VALUE - 1

// Fuse Burn Status
typedef struct FUSE_FuseBurnStatus
{
UINT_T FinalBurnStatus;
UINT_T CorrectedBurnStatus;
UINT_T RawBurnStatus;
UINT_T SavedBurnRequest;
UINT_T LastUsedLogicalBitOffset;
UINT_T Oem_EccStatus;           
UINT_T Jtag_EccStatus;          
UINT_T ApCpUsb_EccStatus;       
UINT_T DebugStatus;
}FUSE_FuseBurnStatus_t;


/***********************************************************
*    Function:
*           GEU_ReadxxxFuseBits
*
*    Description:
*		    Reads the specified (xxx) fuse bits.
*         	
*    Input:
*       	pBuffer - Pointer to buffer to write the requested data.
*           Size - Size of buffer in bytes must be larger than the 
*                  specified field (must be larger than K_xxx_FUSE_SIZE).
*    Output:
*       	none
*    Returns:
*            NoError
*            GEU_BufferTooSmall
*
************************************************************/
// GEU Fuse Read APIs
UINT_T FUSE_ReadSocConfigFuseBits(UINT_T* pBuffer, UINT_T Size);
UINT_T FUSE_ReadOemHashKeyFuseBits(UINT_T* pBuffer, UINT_T Size);
UINT_T FUSE_ReadOemJtagHashKeyFuseBits(UINT_T* pBuffer, UINT_T Size);
UINT_T FUSE_ReadUsbIdFuseBits(UINT_T* pBuffer, UINT_T Size);


#endif //_FUSE_INTERFACE_H_
