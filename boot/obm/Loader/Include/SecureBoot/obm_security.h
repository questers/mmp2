/******************************************************************************
**	(C) Copyright 2007 Marvell International Ltd.  
**	All Rights Reserved
******************************************************************************/

/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document.
**  Except as permitted by such license, no part of this document may be
**  reproduced, stored in a retrieval system, or transmitted in any form or by
**  any means without the express written consent of Intel Corporation.
**
**  FILENAME:	Security.h
**
**  PURPOSE: 	Holds security related flash definitions
**
******************************************************************************/

#ifndef _security_h
#define _security_h

#include "tim.h"
//#include "savestate.h"
#include "general.h"
#include "errors.h"
#include "flash.h"
#include "dsvl.h"
#include "protocolmanager.h"

//UINT_T ValidateImage(UINT_T ImageID, UINT_T* TIM_Hash, pTIM pTIM_h);
//UINT_T ValidateTIM(pTIM pTIM_h);
//UINT_T FastSignatureValidate(pTIM pTIM_h, pFastSignatureVerificationImages pFSVI_h);
//UINT_T UpdateSaveState(UINT_T Code, pTIM pTIM_h, P_FlashProperties_T pFlashProp);
//UINT_T MicroCode_Patching(P_FlashProperties_T pFlashProp, pSaveState pSaveState_h, pTIM pTIM_h);
UINT_T AutoBindPlatform (pTIM pTIM_h, P_FlashProperties_T pFlashP, UINT_T PlatformSettings);

#endif
