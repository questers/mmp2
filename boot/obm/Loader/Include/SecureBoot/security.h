/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document.
**  Except as permitted by such license, no part of this document may be
**  reproduced, stored in a retrieval system, or transmitted in any form or by
**  any means without the express written consent of Intel Corporation.
**
**  FILENAME:	Security.h
**
**  PURPOSE: 	Holds security related flash definitions
**
******************************************************************************/

#ifndef _security_h
#define _security_h

#include "Test.h"
#include "tim.h"
#include "general.h"
#include "Errors.h"
#include "Flash.h"
#include "TrustedBoot.h"
#include "ProtocolManager.h"

// Key Lengths
#define WordLengthOf_PKCS1024			  	32
#define WordLengthOf_PKCS2048				64
#define WordLengthOf_ECDSA256				8
#define WordLengthOf_ECDSA521				17
#define WordLengthOf_SHA1		 			5
#define WordLengthOf_SHA256		 			8
#define WordLengthOf_SHA512		 			16

#define ByteLengthOf_SHA1		 			20
#define ByteLengthOf_SHA256		 			32
#define ByteLengthOf_SHA512		 			64

//constant offsets of OEM hash and Jtag hash in the OTP section (in bytes)
#define OEMHASH_OFFSET_OTP		0
#define JTAGHASH_OFFSET_OTP		WordLengthOf_SHA1*4

typedef struct
{
UINT_T (*pSInitialize_F)();
UINT_T (*pSShutdown_F)();
UINT_T (*pSHAMessageDigest) (const UINT8_T* pSrcMesg, INT_T mesglen, UINT8_T* pMD, HASHALGORITHMID_T HashID);
UINT_T (*pValidateTIM)(pTIM pTIM_h);
UINT_T (*pPlatformVerify)(pTIM pTIM_h);
} SECURITY_FUNCTIONS;
typedef enum {
	OEMJTAGKey =0x0,
	OEMBINDKey,
	OEMMicroCodeKey,
	OEMTCAKey
}WtmKeyType_T;

typedef enum {
	HashData,
	KeyData,
	NoKeyData
}SetTrustData_T;

// Defined in security.c
UINT_T ValidateImage(UINT_T ImageID, pTIM pTIM_h);
UINT_T KeyVerify(KEYMODULES_T KeyType, pTIM pTIM_h);

// These are specific to Security Technology and are defined in xxx_security.c
UINT_T SecurityInitialization(pFUSE_SET pFuses);
UINT_T SecurityShutdown(pFUSE_SET pFuses);
#endif
