/******************************************************************************
**	(C) Copyright 2007 Marvell International Ltd.  
**	All Rights Reserved
******************************************************************************/

/******************************************************************************
**
** INTEL CONFIDENTIAL
** Copyright 2003-2004 Intel Corporation All Rights Reserved.
**
** The source code contained or described herein and all documents
** related to the source code (Material) are owned by Intel Corporation
** or its suppliers or licensors.  Title to the Material remains with
** Intel Corporation or its suppliers and licensors. The Material contains
** trade secrets and proprietary and confidential information of Intel
** or its suppliers and licensors. The Material is protected by worldwide
** copyright and trade secret laws and treaty provisions. No part of the
** Material may be used, copied, reproduced, modified, published, uploaded,
** posted, transmitted, distributed, or disclosed in any way without Intel is
** prior express written permission.
**
** No license under any patent, copyright, trade secret or other intellectual
** property right is granted to or conferred upon you by disclosure or
** delivery of the Materials, either expressly, by implication, inducement,
** estoppel or otherwise. Any license under such intellectual property rights
** must be express and approved by Intel in writing.
**
**  FILENAME:   DSVL.h
**
**  PURPOSE:    DSVL or IPP Library functions
**
******************************************************************************/
#ifndef __DSVL_H__
#define __DSVL_H__
#include "WtmFastSignatureVerification.h"



// From Libraries
// WTM - Security H/W



#if 1
// This enum is used to select enabling/disabling
// of the Caddo Unit
typedef enum
{
	DISABLE_CADDO = 0,
	ENABLE_CADDO  = 1
}CaddoEnable_T;  
#endif





/********************************************************************************
************************* List of PI's provided by DSVL *******************************
***************************************************************************************/



extern int WtmRSAEncrypt_Int(unsigned int *pEncrypt, unsigned int * modulus, int mod_len, unsigned int *PublicExponent, 
						 int exp_len, unsigned int *pDecrypt);
extern int WtmSHAInit(void);
extern int WtmSHA1MessageDigest(const unsigned char *pSrcMesg, int mesglen, unsigned char *pMD); 
extern int WtmPlatform_Config (unsigned int DataValue, unsigned int *pOutput, unsigned int, unsigned int, unsigned int );
extern int WtmPlatform_Bind (unsigned int *OEMKeyDataSrcAddr, int KeyLength, unsigned int *HashValue, unsigned int, unsigned int, unsigned int);
extern int WtmPlatform_Verify (unsigned int *OEMKeyDataSrcAddr, int KeyLength, unsigned int *HashData);
extern int WtmJtagRequest(unsigned int *ChallengeBuf);
extern int WtmJtagResponse(unsigned int *Data, unsigned int DataLength, unsigned int JtagKeyLength);
extern int WtmPurgeKey (unsigned int KeyCacheID, unsigned int KeyType);
extern int WtmSaveState (unsigned int KeySlotID, unsigned int * SavedState);
extern int WtmCreateSubkey (unsigned int KeySlotID, unsigned int KeyID, unsigned int KeyRestrict, 
					 unsigned int *password, unsigned int *wrappedkey);
extern int WtmUnwrapSubkey (unsigned int KeySlotID, unsigned int KeyType, unsigned int KeySize, unsigned int KeyID, 
					 unsigned int *wrappedkey);
extern int WtmInit (unsigned int size, unsigned int *SaveState, unsigned int *rngseed, unsigned int slotID);
extern int WtmReset (unsigned int selftest);
extern int WtmSet_Trusted_Key (unsigned int DataType, int KeyId, int ExpLength, int ModLength, unsigned int *KeyData );
extern int EnableDisableCaddo(CaddoEnable_T); // Enable the Caddo device
extern int WtmRNGLoadSeed (unsigned int *pSeed);
extern int WtmSramBoundry (unsigned int SRAMBoundrySize);
extern int WtmGetCapabilities(unsigned int* CapabilityOutput);
extern int WtmLoadMicroCodePatch(int WrappedPatchWordLength, int OEMHeaderWordLength, int PatchSubKeySlot, unsigned int LocalPatchAddress);
extern void WTMInterruptHandler();
extern int WtmDebug(unsigned int Fault0, unsigned int Fault1, unsigned int Fault2, unsigned int* pDebugOutput);
extern int WTMFastSignatureVerify (pFastSignatureVerify pFastSignIO);
extern int WtmTrustedBoot(void);



#endif