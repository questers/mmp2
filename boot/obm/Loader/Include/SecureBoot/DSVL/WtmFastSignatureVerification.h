/******************************************************************************
**
** INTEL CONFIDENTIAL
** Copyright 2003-2004 Intel Corporation All Rights Reserved.
**
** The source code contained or described herein and all documents
** related to the source code (Material) are owned by Intel Corporation
** or its suppliers or licensors.  Title to the Material remains with
** Intel Corporation or its suppliers and licensors. The Material contains
** trade secrets and proprietary and confidential information of Intel
** or its suppliers and licensors. The Material is protected by worldwide
** copyright and trade secret laws and treaty provisions. No part of the
** Material may be used, copied, reproduced, modified, published, uploaded,
** posted, transmitted, distributed, or disclosed in any way without Intel is
** prior express written permission.
**
** No license under any patent, copyright, trade secret or other intellectual
** property right is granted to or conferred upon you by disclosure or
** delivery of the Materials, either expressly, by implication, inducement,
** estoppel or otherwise. Any license under such intellectual property rights
** must be express and approved by Intel in writing.
**
**  FILENAME:   WTMFastSignatureVerification.h
**
**  PURPOSE:    Helper file for WTMFastSignatureVerification.c
**
******************************************************************************/
#ifndef __WTMFASTSIGNATUREVERIFICATION_H__
#define __WTMFASTSIGNATUREVERIFICATION_H__

/********* This struct must be filled in and passed to WTMFastSignatureVerify ***************
* Originator 			= 2 lower bytes = originator (TBR=0,Else =1) 2 upper bytes (has exported)
* KeyLength  		 	= Number of Words in OEM Platform Key (16,32,64)
* ImageByteLength[7]	= Byte Length of the messages 
* pImageAddress[7]		= Message StartAddresses
* pImageSHAAddress[7]	= Message SHA-1 Digest StartAddresses
* Padding				= Padding and endianess
* pDigitalSignature		= Address pointing to the Digital Signature of the TIM
* pProvisionedKeyDigest	= From Save State
* pRSAPublicKey			= Pointer to Public Key		 		
* pOutput				= What address output should go to. Caller should reserve 11 words (44 bytes)
********* WtmPlatform_Bind_Verify ******************************************/
typedef struct
{
	// Inputs
	unsigned int Originator;
	unsigned int KeyLength;
	unsigned int ImageAndSHAByteLength[7]; 
	unsigned int *pImageAddress[7];
	unsigned int *pImageSHAAddress[7];
	unsigned int PaddingType;
	unsigned int ByteSwap;
	unsigned int *pDigitalSignature;
	unsigned int *pProvisionedKeyDigest;
	unsigned int *pRSAPublicKey;		 		 	
	// Outputs
	unsigned int *pOutput;
} 	FastSignatureVerify, *pFastSignatureVerify;

// Fill this out as a helper in preperation
typedef struct
{
	unsigned int NumberOfImages;
	unsigned int ImageIdentifiers[6];
} FastSignatureVerificationImages, *pFastSignatureVerificationImages;	


typedef enum
{
	TBR_NoBindExport 							= 0x0,
	TBR_BindExport								= 0x10000,
	SW_NoVerify_NoBindExport					= 0x1,
	SW_NoVerify_NoBindExport_NoPublicKeyVerify	= 0x11
} ORIGINATOR_T;

typedef enum
{
	IntelPad_LittleEndian		= 0x0,
	IntelPad_BigEndian			= 0x10000,
	PKCS_v15Pad_LittleEndian	= 0x1,
	PKCS_v15Pad_BigEndian		= 0x10001
} PADDING_T;

typedef enum
{
	No_Swap		= 0x0,
	Swap		= 0x1
} BYTESWAP_T;

#endif