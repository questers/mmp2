/*************************************************************
 * Misc.h
 *
 * Contents:
 *      Definitions and functions declarations used fin the 
 *      Boot ROM development
 *
 *************************************************************/
#ifndef Misc_h
#define Misc_h

#include "tim.h"
#include "Errors.h"
#include "timer.h"
#include "misc_prototypes.h"

// a structure useful to contain all the info required to control gpio pins:
typedef struct GPIO_CFG_S
{
	unsigned long			 gpio_num;		// used to create masks for gpdr, gpsr, gpcr.
	volatile unsigned long	*mfpr_addr;		// used to select gpio alternate function
	unsigned long			 mfpr_val;		// used to select gpio alternate function
	volatile unsigned long	*gpio_addr;		// used to find gplr, gpdr, gpsr and gpcr.
	unsigned long			 gpio_dir;		
} GPIO_CFG_T;

#define GPLR_WORD			0				// word offset from gpio base of gpio level reg
#define	GPDR_WORD			3				// word offset from gpio base of gpio direction reg
#define	GPSR_WORD			6				// word offset from gpio base of gpio set reg
#define	GPCR_WORD			9				// word offset from gpio base of gpio clear reg

/* moved to misc_prototypes.h
int strlen(const char *String);
void memset(void *Addr, unsigned char Val, unsigned long Size); 
void *memcpy(void *dest, const void *src, unsigned int n);
int memcmp( const void *buffer1, const void *buffer2, int count);
int strcpy(char *dest, char *src);
UINT_T ReverseBytes (UINT8_T* Address, UINT_T Size);
unsigned int Endian_Convert (unsigned int in);
int strlen(const char *String);
int strcmpl( char* str1, char* str2, int maxlength );
int stricmpl( char* str1, char* str2, int maxlength );
void ConvertIntToBuf8(unsigned char* StringPtr,unsigned int Value, int Width,unsigned int Count);
INT_T memcmpFF( const void *buffer, INT_T count);
*/
#endif // Misc_h
