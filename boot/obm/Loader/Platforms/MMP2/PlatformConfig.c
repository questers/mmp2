/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into
 *  products for purposes authorized by the license agreement provided they
 *  include this notice and the associated copyright notice with any such
 *  product.
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#include "PlatformConfig.h"
#include "platform_interrupts.h"
#include "predefines.h"
//#include "USB1.h"
#include "UTMI.h"
#include "PMUA.h"
#include "APBC.h"
#include "AIBaux.h"
#include "usbPal.h"
#include "Flash.h"
#include "timer.h"
#include "uart_regs.h"
#include "pinmux.h"
#include "FuseInterface.h"
#include "ipc_interface.h"
#if BOOTROM
	#include "bootrom.h"
#endif
//++ AES test
#include "wtm3_security.h"

#include "misc.h"
#if (SDHC_CNTL == 2)
	#include "sdhc2.h"
#endif

//--

// probe order for MMP2  CS0 XIP, x16NAND, x8NAND, OneNAND, mDOC, MMC.  Note mDoc and MMC are disabled presently
#if FPGA
const UINT8_T ProbeOrder[] = {NAND_FLASH_X16_HM_P,NAND_FLASH_X8_HM_P,NAND_FLASH_X16_BCH_P,NAND_FLASH_X8_BCH_P, NO_FLASH_P};
#else
const UINT8_T ProbeOrder[] = {NAND_FLASH_X16_HM_P,NAND_FLASH_X8_HM_P,NAND_FLASH_X16_BCH_P,NAND_FLASH_X8_BCH_P, SDMMC_FLASH_OPT1_P, CS2_XIP_SIBLEY_P, ONENAND_FLASH_P, SPI_FLASH_P, SDMMC_FLASH_OPT2_P, NO_FLASH_P};
#endif

//-----------------------------------------------------------------------
// ConfigRegSetup
//
//
//-----------------------------------------------------------------------
static void ConfigRegSetup( P_CS_REGISTER_PAIR_S regPtr)
{
    UINT32_T i,tmp;

	while(regPtr->registerAddr != 0x0)
    {
	  tmp = *(regPtr->registerAddr);
	  tmp &= 0x000003F8;
      *(regPtr->registerAddr) = regPtr->regValue | tmp;
	  tmp = *(regPtr->registerAddr);  // ensure write complete
      regPtr++;
    }
}


//-----------------------------------------------------------------------
// ConfigRegWrite
//
//
//-----------------------------------------------------------------------
static void ConfigRegWrite( P_CS_REGISTER_PAIR_S regPtr)
{
    UINT32_T i,tmp;
	while(regPtr->registerAddr != 0x0)
    {
      *(regPtr->registerAddr) = regPtr->regValue;
	  tmp = *(regPtr->registerAddr);  // ensure write complete
      regPtr++;
    }
}

//-----------------------------------------------------------------------
// ConfigRegResume
//
//
//-----------------------------------------------------------------------
void ConfigRegResume( P_CS_REGISTER_PAIR_S regPtr)
{
   UINT32_T i,tmp;

	while(regPtr->registerAddr != 0x0)
    {
      *(regPtr->registerAddr) &= 0xFFFFFFF7;
      tmp = *(regPtr->registerAddr);  // ensure write complete
      regPtr++;
    }
}

//-----------------------------------------------------------------------
// ConfigRegSave
//
//
//-----------------------------------------------------------------------
void ConfigRegSave( P_CS_REGISTER_PAIR_S regPtr)
{
    UINT32_T i,tmp;

	while(regPtr->registerAddr != 0x0)
    {
      regPtr->defaultValue = *(regPtr->registerAddr);
      regPtr++;
    }
}

//-----------------------------------------------------------------------
// ConfigRegRestore
//
//
//-----------------------------------------------------------------------
void ConfigRegRestore( P_CS_REGISTER_PAIR_S regPtr)
{
    UINT32_T i,tmp;

	while(regPtr->registerAddr != 0x0)
    {
      *(regPtr->registerAddr) = regPtr->defaultValue;
      tmp = *(regPtr->registerAddr);  // ensure write complete
      regPtr++;
    }
}


/*
 *  Table definitions for multi function pin registers
 */
// keypad pins are gpio0->19
CS_REGISTER_PAIR_S keypad_pins[] =
{
 		(int *) (APPS_PAD_BASE | GPIO_00), PULL_SEL | PULL_DN | DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_MKIN[0]
 		(int *) (APPS_PAD_BASE | GPIO_01), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0,	//KP_MKOUT[0]
 		(int *) (APPS_PAD_BASE | GPIO_02), PULL_SEL | PULL_DN | DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_MKIN[1]
 		(int *) (APPS_PAD_BASE | GPIO_03), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0,	//KP_MKOUT[1]
		(int *) (APPS_PAD_BASE | GPIO_04), PULL_SEL | PULL_DN | DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_MKIN[2]
 		(int *) (APPS_PAD_BASE | GPIO_05), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_MKOUT[2]
 		(int *) (APPS_PAD_BASE | GPIO_06), PULL_SEL | PULL_DN | DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0,	//KP_MKIN[3]
 		(int *) (APPS_PAD_BASE | GPIO_07), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_MKOUT[3]
		(int *) (APPS_PAD_BASE | GPIO_08), PULL_SEL | PULL_DN | DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0,	//KP_MKIN[4]
 		(int *) (APPS_PAD_BASE | GPIO_09), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_MKOUT[4]
 		(int *) (APPS_PAD_BASE | GPIO_10), PULL_SEL | PULL_DN | DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0,	//KP_MKIN[5]
 		(int *) (APPS_PAD_BASE | GPIO_11), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_MKOUT[5]
		(int *) (APPS_PAD_BASE | GPIO_12), PULL_SEL | PULL_DN | DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_MKIN[6]
 		(int *) (APPS_PAD_BASE | GPIO_13), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0,	//KP_MKOUT[6]
 		(int *) (APPS_PAD_BASE | GPIO_14), PULL_SEL | PULL_DN | DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_MKIN[7]
 		(int *) (APPS_PAD_BASE | GPIO_15), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0,	//KP_MKOUT[7]
		(int *) (APPS_PAD_BASE | GPIO_16), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_DKIN[0]
 		(int *) (APPS_PAD_BASE | GPIO_17), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_DKIN[1]
 		(int *) (APPS_PAD_BASE | GPIO_18), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0,	//KP_DKIN[2]
 		(int *) (APPS_PAD_BASE | GPIO_19), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_DKIN[3]
		(int *) (APPS_PAD_BASE | GPIO_20), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_DKIN[4]
		(int *) (APPS_PAD_BASE | GPIO_21), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_DKIN[5]
		(int *) (APPS_PAD_BASE | GPIO_22), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0,	//KP_DKIN[6]
		(int *) (APPS_PAD_BASE | GPIO_23), DRV_SLOW | EDGE_DIS | AF_SEL1, 0x0, 	//KP_DKIN[7]
		0x0,0x0,0x0 // termination
};

CS_REGISTER_PAIR_S io_pins[] =
{
 		(int *) (APPS_PAD_BASE | ND_IO0),  DRV_MED | AF_SEL0, 0x0,	        	//ND_IO0
		(int *) (APPS_PAD_BASE | ND_IO1),  DRV_MED | AF_SEL0, 0x0,		    	//ND_IO1
		(int *) (APPS_PAD_BASE | ND_IO2),  DRV_MED | AF_SEL0, 0x0,		    	//ND_IO2
		(int *) (APPS_PAD_BASE | ND_IO3),  DRV_MED | AF_SEL0, 0x0,		    	//ND_IO3
		(int *) (APPS_PAD_BASE | ND_IO4),  DRV_MED | AF_SEL0, 0x0,		    	//ND_IO4
		(int *) (APPS_PAD_BASE | ND_IO5),  DRV_MED | AF_SEL0, 0x0,		    	//ND_IO5
		(int *) (APPS_PAD_BASE | ND_IO6),  DRV_MED | AF_SEL0, 0x0,		    	//ND_IO6
		(int *) (APPS_PAD_BASE | ND_IO7),  DRV_MED | AF_SEL0, 0x0,		    	//ND_IO7
		(int *) (APPS_PAD_BASE | ND_IO8),  DRV_MED | EDGE_DIS | AF_SEL0, 0x0,	//ND_IO8
		(int *) (APPS_PAD_BASE | ND_IO9),  DRV_MED | EDGE_DIS | AF_SEL0, 0x0,	//ND_IO9
		(int *) (APPS_PAD_BASE | ND_IO10), DRV_MED | EDGE_DIS | AF_SEL0, 0x0,	//ND_IO10
		(int *) (APPS_PAD_BASE | ND_IO11), DRV_MED | EDGE_DIS | AF_SEL0, 0x0,	//ND_IO11
		(int *) (APPS_PAD_BASE | ND_IO12), DRV_MED | EDGE_DIS | AF_SEL0, 0x0,	//ND_IO12
		(int *) (APPS_PAD_BASE | ND_IO13), DRV_MED | EDGE_DIS | AF_SEL0, 0x0,	//ND_IO13
		(int *) (APPS_PAD_BASE | ND_IO14), DRV_MED | EDGE_DIS | AF_SEL0, 0x0,	//ND_IO14
		(int *) (APPS_PAD_BASE | ND_IO15), DRV_MED | EDGE_DIS | AF_SEL0, 0x0,	//ND_IO15
		0x0,0x0,0x0 // termination
};

CS_REGISTER_PAIR_S common_pins[]=
{
		(int *) (APPS_PAD_BASE | ND_ALE_SM_ADV), (DRV_MED | AF_SEL0), 0x0,
		(int *) (APPS_PAD_BASE | ND_CLE), 		 (DRV_MED | AF_SEL0), 0x0,
		(int *) (APPS_PAD_BASE | ND_nWE_SM_nWE), (DRV_MED | AF_SEL0), 0x0, 		//ND_ALE_SM_WEn	now muxed with ND_nWE = ND_nWE_SM_nWE
		(int *) (APPS_PAD_BASE | ND_nRE_SM_nOE), (DRV_MED | AF_SEL0), 0x0,		//ND_CLE_SM_OEn	now muxed with ND_nRE = ND_nRE_SM_nOE
		0x0,0x0,0x0 //termination
};

CS_REGISTER_PAIR_S dfc_pins[]=
{
		(int *) (APPS_PAD_BASE | ND_nCS0), (DRV_MED | AF_SEL0), 0x0, 						//ND_nCS0
		(int *) (APPS_PAD_BASE | ND_RDY0), (PULL_UP | DRV_MED | AF_SEL0 | PULL_SEL), 0x0,	//ND_RDY0
		0x0,0x0,0x0 //termination
};

CS_REGISTER_PAIR_S cs2_common_pins[]=
{
  		(int *) (APPS_PAD_BASE | ND_RDY0), (DRV_MED | AF_SEL3), 0x0,		//SM_RDY
		(int *) (APPS_PAD_BASE | ND_nCS0), (DRV_MED | AF_SEL2), 0x0, 		//SM_CSn[2]
		(int *) (APPS_PAD_BASE | SM_SCLK), (DRV_MED | AF_SEL0), 0x0, 		//SM_SCLK
		0x0,0x0,0x0 //termination
};

CS_REGISTER_PAIR_S cs2_reg_xip[]=
{
        (int *) (SMC_CSDFICFG2), 0x51890009, 0x0,   // SMC_CSDFICFG2
        (int *) (SMC_CSADRMAP2), 0x10200F00, 0x0,   // SMC_CSADRMAP2
		(int *) (SMC_ADV_Apx + 0x8), 0xCF3, 0x0,	// Change ADVn to latch twice in address phase, instead of once
		(int *) (SMC_SMC_WE_Apx + 0x8), 0xFFD, 0x0,
		(int *) (SMC_OE_Apx + 0x8), 0xC3F, 0x0,  	// Change OEn to go low in upper address
													// (first) and remain high in lower address (second), instead of remaining high in address phase
		0x0,0x0,0x0 //termination
};

CS_REGISTER_PAIR_S cs2_reg_OneNAND[]=
{
        (int *) (SMC_CSDFICFG2), 0x51890008, 0x0,   // SMC_CSDFICFG2
        (int *) (SMC_CSADRMAP2), 0x10200F00, 0x0,   // SMC_CSADRMAP2
		(int *) (SMC_ADV_Apx + 0x8), 0xCFF, 0x0,	// High Address, set ADV low during the width of the address cycle
		(int *) (SMC_SMC_WE_Apx + 0x8), 0xFFF, 0x0,
		(int *) (SMC_OE_Apx + 0x8), 0xFFF, 0x0,  	// Single Strobe on low address
		0x0,0x0,0x0 //termination
};


CS_REGISTER_PAIR_S spi_pins[]=
{
  		(int *) (APPS_PAD_BASE | GPIO_43), (DRV_MED | AF_SEL3), 0x0, 	//SSP1_RXD
  		(int *) (APPS_PAD_BASE | GPIO_44), (DRV_MED | AF_SEL3), 0x0, 	//SSP1_TXD
  		(int *) (APPS_PAD_BASE | GPIO_45), (DRV_MED | AF_SEL3), 0x0, 	//SSP1_CLK
  		(int *) (APPS_PAD_BASE | GPIO_46), (DRV_MED | AF_SEL3), 0x0,	//SSP1_FRM
 		0x0,0x0,0x0 //termination
};

// HSI pins
CS_REGISTER_PAIR_S hsi_pins[]=
{
  		(int *) (APPS_PAD_BASE | GPIO_115), (DRV_MED | AF_SEL2), 0x0, 	//TX Wake
  		(int *) (APPS_PAD_BASE | GPIO_116), (DRV_MED | AF_SEL2), 0x0, 	//TX Rdy
  		(int *) (APPS_PAD_BASE | GPIO_117), (DRV_MED | AF_SEL2), 0x0, 	//TX Flag
  		(int *) (APPS_PAD_BASE | GPIO_118), (DRV_MED | AF_SEL2), 0x0, 	//TX Data
  		(int *) (APPS_PAD_BASE | GPIO_119), (DRV_MED | AF_SEL2), 0x0, 	//RX Wake
  		(int *) (APPS_PAD_BASE | GPIO_120), (DRV_MED | AF_SEL2), 0x0, 	//RX Rdy
  		(int *) (APPS_PAD_BASE | GPIO_121), (DRV_MED | AF_SEL2), 0x0, 	//RX Flag
		(int *) (APPS_PAD_BASE | GPIO_122), (DRV_MED | AF_SEL2), 0x0, 	//RX Data 		
		0x0,0x0,0x0 //termination
};

// MMC3 top interface
CS_REGISTER_PAIR_S mmc3_pins_top[]=
{
      (int *) (APPS_PAD_BASE | ND_IO3), 	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL2, 0x0,	//MMC3_DAT[7]
      (int *) (APPS_PAD_BASE | ND_IO11), 	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL2, 0x0,	//MMC3_DAT[6]
      (int *) (APPS_PAD_BASE | ND_IO2), 	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL2, 0x0, 	//MMC3_DAT[5]
      (int *) (APPS_PAD_BASE | ND_IO10), 	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL2, 0x0,	//MMC4_DAT[4]
      (int *) (APPS_PAD_BASE | ND_IO1),		PULL_SEL | PULL_UP | DRV_FAST | AF_SEL2, 0x0, 	//MMC3_DAT[3]
      (int *) (APPS_PAD_BASE | ND_IO9),		PULL_SEL | PULL_UP | DRV_FAST | AF_SEL2, 0x0,	//MMC3_DAT[2]
      (int *) (APPS_PAD_BASE | ND_IO0),		PULL_SEL | PULL_UP | DRV_FAST | AF_SEL2, 0x0, 	//MMC3_DAT[1]
      (int *) (APPS_PAD_BASE | ND_IO8),		PULL_SEL | PULL_UP | DRV_FAST | AF_SEL2, 0x0,	//MMC3_DAT[0]
      (int *) (APPS_PAD_BASE | SM_SCLK), 	DRV_FAST | AF_SEL2, 0x0, 						//MMC3_CLK     gpio 151
      (int *) (APPS_PAD_BASE | ND_RDY0), 	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL2, 0x0,	//MMC3_CMD     gpio 112
      0x0,0x0,0x0 //termination
};

// MMC3 bottom interface
CS_REGISTER_PAIR_S mmc3_pins_bottom[]=
{
      (int *) (APPS_PAD_BASE | GPIO_128), 	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL3, 0x0, 	//MMC3_DAT[7]
      (int *) (APPS_PAD_BASE | GPIO_124), 	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL3, 0x0,	//MMC3_DAT[6]
      (int *) (APPS_PAD_BASE | GPIO_129), 	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL3, 0x0, 	//MMC3_DAT[5]
      (int *) (APPS_PAD_BASE | GPIO_125), 	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL3, 0x0,	//MMC4_DAT[4]
      (int *) (APPS_PAD_BASE | GPIO_130),	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL3, 0x0, 	//MMC3_DAT[3]
      (int *) (APPS_PAD_BASE | GPIO_126),	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL3, 0x0,	//MMC3_DAT[2]
      (int *) (APPS_PAD_BASE | GPIO_135),	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL3, 0x0, 	//MMC3_DAT[1]
      (int *) (APPS_PAD_BASE | GPIO_127),	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL3, 0x0,	//MMC3_DAT[0]
      (int *) (APPS_PAD_BASE | GPIO_138), 	DRV_FAST | AF_SEL3, 0x0, 						//MMC3_CLK
      (int *) (APPS_PAD_BASE | SM_RDY), 	PULL_SEL | PULL_UP | DRV_FAST | AF_SEL3, 0x0,	//MMC3_CMD
      0x0,0x0,0x0 //termination
};


// MMC1 interface
// MMC SDHC0 controller compatible pinout
CS_REGISTER_PAIR_S mmc1_pins[]=
{
		(int *) (APPS_PAD_BASE | 0x003c), 0xcC1, 0x0,	//MMC1_CMD	GPIO136	gpio 136
		(int *) (APPS_PAD_BASE | 0x0048), 0xcC1, 0x0,	//MMC1_CLK	GPIO139 gpio 139
		(int *) (APPS_PAD_BASE | 0x0034), 0xcC1, 0x0,	//MMC1_DAT0	GPIO134
		(int *) (APPS_PAD_BASE | 0x0030), 0xcC1, 0x0,	//MMC1_DAT1	GPIO133
		(int *) (APPS_PAD_BASE | 0x002c), 0xcC1, 0x0,	//MMC1_DAT2	GPIO132
		(int *) (APPS_PAD_BASE | 0x0028), 0xcC1, 0x0,	//MMC1_DAT3	GPIO131
		(int *) (APPS_PAD_BASE | 0x0024), 0xcC1, 0x0,	//MMC1_DAT4	GPIO130
		(int *) (APPS_PAD_BASE | 0x0020), 0xcC1, 0x0,	//MMC1_DAT5	GPIO129
		(int *) (APPS_PAD_BASE | 0x0010), 0xcC1, 0x0,	//MMC1_DAT6	GPIO125
		(int *) (APPS_PAD_BASE | 0x000c), 0xcC1, 0x0,	//MMC1_DAT7	GPIO124
		0x0,0x0,0x0 //termination
};

// UART 3
CS_REGISTER_PAIR_S uart_pins[]=
{
      (int *) (APPS_PAD_BASE | GPIO_51), (PULL_UP | DRV_MED | AF_SEL1), 0x0,	//UART3_RXD
      (int *) (APPS_PAD_BASE | GPIO_52), (PULL_UP | DRV_MED | AF_SEL1), 0x0,	//UART3_TXD
	  0x0,0x0,0x0 //termination
};

UINT_T ChipSelect0(void)
{
	return InvalidPlatformConfigError;
}

UINT_T ChipSelect2(void)
{
#if !FPGA
	ConfigRegWrite(cs2_reg_xip);
	ConfigRegSetup(io_pins);
	ConfigRegSetup(common_pins);
	ConfigRegSetup(cs2_common_pins);
#endif
	return NoError;
}

void ChipSelectOneNAND( void)
{
#if !FPGA
   ConfigRegWrite(cs2_reg_OneNAND);
   ConfigRegSetup(io_pins);
   ConfigRegSetup(common_pins);
   ConfigRegSetup(cs2_common_pins);
#endif
   return;
}

// ChipSelect for DFC
void ChipSelectDFC( void )
{
#if !FPGA
   	ConfigRegSetup(dfc_pins);
   	ConfigRegSetup(io_pins);
   	ConfigRegSetup(common_pins);
#endif
	return;
}

// SPI
void ChipSelectSPI( void )
{
#if !FPGA
	ConfigRegSetup(spi_pins);
#endif
	return;
}

// HSI
void ChipSelectHSI( void )
{
	ConfigRegSave(hsi_pins );
	ConfigRegSetup(hsi_pins);

}
void RestoreHSIPins( void )
{
	ConfigRegRestore(hsi_pins);
}

//------------------------------------------------------------------------
//
//
//------------------------------------------------------------------------
void RestoreDefaultConfig(void)
{
#if !FPGA
	ConfigRegRestore(dfc_pins);
	ConfigRegRestore(cs2_reg_xip);
    ConfigRegRestore(cs2_reg_OneNAND);
	ConfigRegRestore(common_pins );
	ConfigRegRestore(cs2_common_pins );
	ConfigRegRestore(io_pins );
    ConfigRegRestore(mmc3_pins_top);
    ConfigRegRestore(mmc3_pins_bottom);
    ConfigRegRestore(spi_pins);
#endif
}

void SaveDefaultConfig(void)
{
#if !FPGA
	ConfigRegSave(dfc_pins);
	ConfigRegSave(cs2_reg_xip);
    ConfigRegSave(cs2_reg_OneNAND);
	ConfigRegSave(common_pins );
    ConfigRegSave(cs2_common_pins );
	ConfigRegSave(io_pins );
    ConfigRegSave(mmc3_pins_top);
    ConfigRegSave(mmc3_pins_bottom);
    ConfigRegSave(spi_pins );
#endif
}
//--------------------------------------------------
// mDOC data sheet specifies that BUSY signal
// means that the mDOC device should not be
// accessed.  The data sheet indicates "ACTIVE LOW"
// which I interpret to mean the device is "BUSY"
// when the BUSY signal level is 0 (low)
//--------------------------------------------------
UINT32 PlatformMdocRdyForXIPAccess( void )
{
   //GPIO 152
   //----------------------------------------------
   return NoError; //(*GPLRb & 0x01000000);  // GPIO152

}


#if BOOTROM
	// not used on ttc?
//
// Setup proper function pointers for resume state handler
//  		For TTC this will handle dual core boot
//
void PlatformResumeSetup(pQuick_Resume_Function *Quick_f, pTim_Resume_Function *Tim_f){

	*Quick_f = &QuickBoot_NotSupported;
	*Tim_f = &TIMQuickBoot_TTC;
   return;
}
#endif

const UINT8_T* GetProbeOrder()
{
	return ProbeOrder;
}


UINT_T getPlatformPortSelection(unsigned int *default_usb_port)
{
	UINT_T		port = 0;
	FUSE_SET	lFuses;

	GetPlatformFuses( 0, &lFuses );

	if (!lFuses.bits.UARTDisable && !lFuses.bits.USBDisable)
	{
  		*default_usb_port = CI2_USB_D;
    	port = 6; // send default configuration USB + FFUART
	}
	else if (!lFuses.bits.UARTDisable)
	{
		port = 0x5; // send default configuration for UART 1
	}
	else if (!lFuses.bits.USBDisable)
	{
  		*default_usb_port = CI2_USB_D;
    	port = 7; // send default configuration USB only
	}
	return port;
}

UINT_T PlatformKeypadConfig(void)
{

#if !FPGA
   ConfigRegSetup(keypad_pins);
#endif
   // note: any clock and reset enabling for the keypad unit is done in InitKeypad
	return (NoError);
}


UINT_T PlatformUARTConfig(void)
{
#if !FPGA
   ConfigRegWrite(uart_pins);
#endif
	#if 0
   *(VUINT_T *)UART_X_CLK_BASE = (BIT1 | BIT0);	// 24mhz, no reset, fx clk ena, apb clk ena
	#else
	/*26MHz clock*/
	//porting from uboot code
	*(VUINT_T *)UART_X_CLK_BASE = BIT2;
	*(VUINT_T *)UART_X_CLK_BASE = 0x7|(1<<4);
	*(VUINT_T *)UART_X_CLK_BASE = 0x3|(1<<4);
	
	
	#endif
	return (NoError);
}


UINT_T PlatformAltUARTConfig(void)
{
	return NotSupportedError;
}
/*****************************************
**		UDC 1.1 Specific Functions		**
******************************************/

/**
 * Configure USB PORT_2 for Single Ended USB Operation
 **/
UINT_T SetupSingleEndedPort2(void)
{

	return(NotSupportedError);
}

void PlatformUsbInit(UINT_T port)
{

 return;
}

UINT_T PlatformUsbTransmit(UINT_T length, UINT8_T* Buff)
{
	return NoError;
}

void PlatformUsbDmaHandler()
{
	return;
}

void PlatformUsbInterruptHandler()
{
	return;
}

void PlatformUsbShutdown()
{
	return;
}

/**********************************
**		U2D Specific Calls       **
***********************************/

void PlatformU2DInit()
{
	return;
}

UINT_T PlatformU2DTransmit(UINT_T length, UINT8_T* Buff, UINT_T WaitState)
{
   return 0;
}


void PlatformU2DShutdown()
{
	return;
}

/**********************************
**      CI2 Specific Calls       **
***********************************/
static UINT32 ci2D0TurnOns = 0;
static UINT32 ci2ACCR1TurnOns = 0;
static UINT32 ci2DriverUp = 0;

//------------------------------------
//
//------------------------------------
void PlatformCI2InterruptEnable()
{
  EnablePeripheralIRQInterrupt(USB0_OTG_INT);
}

//---------------------------------
//
//---------------------------------
void PlatformCI2InterruptDisable()
{
  DisablePeripheralIRQInterrupt(USB0_OTG_INT);
}

//----------------------------------
//
//----------------------------------
UINT_T PlatformCI2Transmit(UINT_T length, UINT8_T* Buff, UINT_T WaitState)
{
#if FPGA || !USBCI
	return NoError;
#else
    return CI2Transmit(length, Buff, WaitState);
#endif
}

//--------------------------------
//
//--------------------------------
void PlatformCI2InterruptHandler()
{
#if FPGA || !USBCI
	return;
#else
    ImageCI2InterruptHandler();
#endif
}

//-------------------------------------
// PlatformCI2Shutdown
//
//   Shut down OTG periperhal
//-------------------------------------
void PlatformCI2Shutdown()
{
#if FPGA || !USBCI
	return;
#else
    // Don't do anything if the driver
    // isn't UP
    //--------------------------------
    if(ci2DriverUp == 0 )
      return;

    BootRomCI2HWShutdown();

    // Power down PHY and PLL
    // only turn off only those that BR
    // had to turn on
    //------------------------------------
    //*ACCR1 &= (~ci2ACCR1TurnOns);   //DCB TTC-bringup

    // Use D0CKEN_C reg to turn off 26Mhz
    // clock, for PHY PLL input
    // only turn-off only those that BR
    // had to turn on
    //------------------------------------
    //*D0CKEN_C &= (~ci2D0TurnOns);   //DCB TTC-bringup

    //*OSCC &= ~0x800; // turn off CLK_POUT
    *(VUINT_T *)PMUA_USB_CLK_RES_CTRL = 0x0;  //turn off USB AXI clock
    ci2DriverUp = 0;
#endif
}

//------------------------------------------------------
// PlatformCI2Ready()
//
//       Wait max of 400us for PLL's to be locked and ready
//------------------------------------------------------
void PlatformCI2Ready( void )
{
#if FPGA || !USBCI
	return;
#else
  UINT32 startTime, endTime;

  startTime = GetOSCR0(); // Dummy read to flush potentially bad data
  startTime = GetOSCR0();

  do
  {
    if(((*(volatile UINT_T *)UTMI_PLL) & UTMI_PLL_PLL_READY) == UTMI_PLL_PLL_READY)
    	break;

	endTime = GetOSCR0();
	if (endTime < startTime)
		endTime += (0x0 - startTime);
  }
  while( OSCR0IntervalInMicro(startTime, endTime) < 400 );

  //Ensure that phy control comes from OTG
  //-----------------------------------------
 // *USB2_PHY_T0 |= USB2_PHY_T0_OTG_CONTROL_BIT;
 // startTime = *USB2_PHY_T0;  // ensure write complete
#endif
}

//----------------------------------------------
// OTG PHY setup MMP2
//
//----------------------------------------------
void PlatformCI2Init()
{
	VUINT_T *pTemp;
	UINT_T Retval = NoError;


#if FPGA || !USBCI
	return;
#else

    // Prevent re-init if already up
    //------------------------------
    if( ci2DriverUp != 0 )
       return;

    // USB OTG AXI: Enable the clocks and bring the link out of reset
    *(VUINT_T *)PMUA_USB_CLK_RES_CTRL = (PMUA_USB_CLK_RES_CTRL_USB_AXICLK_EN);
    *(VUINT_T *)PMUA_USB_CLK_RES_CTRL = (PMUA_USB_CLK_RES_CTRL_USB_AXICLK_EN | PMUA_USB_CLK_RES_CTRL_USB_AXI_RST);


	BU_REG_WRITE( PMUA_HSIC1_CLK_RES_CTRL, PMUA_HSIC1_CLK_RES_CTRL_HSIC1_SPH_AXICLK_EN |
                  PMUA_HSIC1_CLK_RES_CTRL_HSIC1_AXICLK_EN | PMUA_HSIC1_CLK_RES_CTRL_HSIC1_SPH_AXI_RST |
                  PMUA_HSIC1_CLK_RES_CTRL_HSIC1_AXI_RST );        // 0x1b

    BU_REG_WRITE( PMUA_HSIC2_CLK_RES_CTRL, PMUA_HSIC2_CLK_RES_CTRL_HSIC2_SPH_AXICLK_EN |
                  PMUA_HSIC2_CLK_RES_CTRL_HSIC2_AXICLK_EN | PMUA_HSIC2_CLK_RES_CTRL_HSIC2_SPH_AXI_RST |
                  PMUA_HSIC2_CLK_RES_CTRL_HSIC2_AXI_RST );        // 0x1b

    BU_REG_WRITE( PMUA_FSIC3_CLK_RES_CTRL, PMUA_FSIC3_CLK_RES_CTRL_FSIC3_SPH_AXICLK_EN |
                  PMUA_FSIC3_CLK_RES_CTRL_FSIC3_AXICLK_EN | PMUA_FSIC3_CLK_RES_CTRL_FSIC3_SPH_AXI_RST |
                  PMUA_FSIC3_CLK_RES_CTRL_FSIC3_AXI_RST );        // 0x1b

    /* Initialize the USB PHY */
    BU_REG_WRITE(UTMI_CTRL, BU_REG_READ(UTMI_CTRL) |(1<<UTMI_CTRL_USB_CTL_29_28_BASE));
    BU_REG_WRITE(UTMI_CTRL, BU_REG_READ(UTMI_CTRL) | (UTMI_CTRL_PU)|(UTMI_CTRL_PU_PLL)|(UTMI_CTRL_PU_REF));
    Delay(1000);

    BU_REG_WRITE(UTMI_PLL, BU_REG_READ(UTMI_PLL) & (~UTMI_PLL_FBDIV_MSK) &(~UTMI_PLL_REFDIV_MSK));
    BU_REG_WRITE(UTMI_PLL, BU_REG_READ(UTMI_PLL) | (0xee<<UTMI_PLL_FBDIV_BASE)|(0xb<<UTMI_PLL_REFDIV_BASE));

    BU_REG_WRITE(UTMI_PLL, BU_REG_READ(UTMI_PLL) & (~UTMI_PLL_ICP_MSK));
    BU_REG_WRITE(UTMI_PLL, BU_REG_READ(UTMI_PLL) | (0x3<<UTMI_PLL_ICP_BASE));
    BU_REG_WRITE(UTMI_TX, BU_REG_READ(UTMI_TX) & (~UTMI_TX_TXVDD12_MSK));
    BU_REG_WRITE(UTMI_TX, BU_REG_READ(UTMI_TX) | (0x3<<UTMI_TX_TXVDD12_BASE));

    BU_REG_WRITE(UTMI_TX, BU_REG_READ(UTMI_TX) & (~UTMI_TX_IMPCAL_VTH_MSK));
    BU_REG_WRITE(UTMI_TX, BU_REG_READ(UTMI_TX) | (0x2<<UTMI_TX_IMPCAL_VTH_BASE));

    /* calibrate */
	Retval =  WaitForOperationComplete(500, WAITINMICROSECONDS, (VUINT_T *) UTMI_PLL, UTMI_PLL_PLL_READY, UTMI_PLL_PLL_READY);
	if (Retval != NoError)
		return;

    /* toggle VCOCAL_START bit of U2PPLL */
    Delay(200);
    BU_REG_WRITE(UTMI_PLL, BU_REG_READ(UTMI_PLL) | (UTMI_PLL_VCOCAL_START) );
    Delay(200);
    BU_REG_WRITE(UTMI_PLL, BU_REG_READ(UTMI_PLL) & (~UTMI_PLL_VCOCAL_START) );

    /* toggle REG_RCAL_START bit of U2PTX */
    Delay(200);
    BU_REG_WRITE(UTMI_TX, BU_REG_READ(UTMI_TX) | (UTMI_TX_REG_RCAL_START) );
    Delay(400);
    BU_REG_WRITE(UTMI_TX, BU_REG_READ(UTMI_TX) & (~UTMI_TX_REG_RCAL_START) );

    /* make sure phy is ready */
	Retval =  WaitForOperationComplete(500, WAITINMICROSECONDS, (VUINT_T *) UTMI_PLL, UTMI_PLL_PLL_READY, UTMI_PLL_PLL_READY);
	if (Retval != NoError)
		return;

    // end of hw setup

    // Step 6: Initialize OTG software and hardware
    // need to poll the PLL_LOCK_RDY bit in ACCR1
    // before turning on OTG "RUN" bit
    //--------------------------------------------

#if CI2_USB_DDR_BUF
	BootRomCI2SWInit( PlatformCI2InterruptEnable, PlatformCI2Ready,
                     ( CI_OTG_OPTIONS_8BIT_XCVR_WIDTH  |
                       CI_OTG_OPTIONS_UTMI_XCVR_SELECT
					 ));
	// ensure full spead is not being forced:
	// we use 512 byte packets and must run in USB 2.0 mode
	*(unsigned long*)0xd4208184 &= ~(1u<<24);
#else
	BootRomCI2SWInit( PlatformCI2InterruptEnable, PlatformCI2Ready,
                     ( CI_OTG_OPTIONS_8BIT_XCVR_WIDTH  |
                       CI_OTG_OPTIONS_UTMI_XCVR_SELECT |
                       CI_OTG_OPTIONS_FULL_SPEED
                     ));
	// full speed works, so don't force it anymore. try highspeed (if the chirp passed)
	// force full speed mode temporarily
	*(unsigned long*)0xd4208184 |= (1u<<24);
#endif

    ci2DriverUp = 1;
#endif
}

/*-----------------------------------------------------------------------
/ This function will read service and platform fuses.
/
/-----------------------------------------------------------------------*/
UINT_T  GetPlatformFuses(UINT_T platformFuses, pFUSE_SET pTBR_Fuse)
{
  UINT_T ServiceFuses, SysBootCtrl;
  UINT_T Retval = NoError;
  UINT_T SOCFuses[4];				// We need enough space for 128 bits

#if BOOTROM
	P_TBR_Env pTBR;
	UINT_T JTAG_key_buff[8];
	UINT_T oem_buff[8];
	UINT_T zero[8];
#endif

  pTBR_Fuse->value = 0x0;

  // Unsupported options: default to respective OFF position for that option
  pTBR_Fuse->bits.EscapeSeqDisable = 1;

#if BOARD_DEBUG
  pTBR_Fuse->bits.PlatformState = NO_FLASH_P;
  pTBR_Fuse->bits.SBE = 0;
  pTBR_Fuse->bits.UARTDisable = 0;		// Bits 13 UARTDisable
  pTBR_Fuse->bits.USBDisable = 0;		// Bits 12 USBDisable
  pTBR_Fuse->bits.MMCDisable = 0;
  pTBR_Fuse->bits.Download_Disable = 0;
  pTBR_Fuse->bits.UARTPort = FFUART_PORT;
  pTBR_Fuse->bits.USBPort = USB_CI2_PORT;
  pTBR_Fuse->bits.Resume = 0;
  pTBR_Fuse->bits.DDRInitialized = 0;
  pTBR_Fuse->bits.BootPlatformState = SDMMC_FLASH_OPT3_P;
  pTBR_Fuse->bits.OverrideDisable = 0;
  pTBR_Fuse->bits.DebugDisable = 0;
  pTBR_Fuse->bits.UseWTM = 1;
  pTBR_Fuse->bits.TBROpMode = 2;	// 2=TRUSTED, 1=NON-TRUSTED
#else

  // Dummy Test, read fuses from the CIU
  SysBootCtrl = BU_REG_READ(SYS_BOOT_CNTRL);

  // First Read in AP Fuses from GEU.
  FUSE_ReadSocConfigFuseBits((UINT_T *)SOCFuses, sizeof(SOCFuses));

  pTBR_Fuse->bits.OverrideDisable = (SOCFuses[2] >> 30) & 1;	 //dis_aib_override
  pTBR_Fuse->bits.DebugDisable =(SOCFuses[2] >> 31) & 1;	//dis_strap_override

  if (pTBR_Fuse->bits.OverrideDisable && pTBR_Fuse->bits.DebugDisable)
  {
	 // overrides have been disabled so use the Fuseblock 0 Fuses
	 pTBR_Fuse->bits.PlatformState = (SOCFuses[2] >> 14) & 0xF;	// All four bits 78:81
	 pTBR_Fuse->bits.UseWTM = 1; 					// Must use WTM for MMP2
	 pTBR_Fuse->bits.Download_Disable = (SOCFuses[2] >> 22) & 1;
	 pTBR_Fuse->bits.UARTDisable = (SOCFuses[2] >> 19) & 1;
	 pTBR_Fuse->bits.USBDisable = (SOCFuses[2] >> 20) & 1;
	 pTBR_Fuse->bits.MMCDisable = (SOCFuses[2] >> 23) & 1;
	 pTBR_Fuse->bits.TBROpMode = (SOCFuses[2] >> 10) & 3;
	 pTBR_Fuse->bits.JTAG_Disable = (SysBootCtrl >> 6) & 0x1; //still comes from CIU
  }
  else
  {
   	 // Override are allowed use the CIU
	 pTBR_Fuse->bits.PlatformState = ((SysBootCtrl >> 22) & 0xf);
	 pTBR_Fuse->bits.JTAG_Disable =(SysBootCtrl >> 6) & 0x1;
	 pTBR_Fuse->bits.USBDisable =(SysBootCtrl >> 13) & 0x1;
	 pTBR_Fuse->bits.UARTDisable =(SysBootCtrl >> 12) & 0x1;
	 pTBR_Fuse->bits.USBWakeup =(SysBootCtrl >> 16) & 0x1;
	 pTBR_Fuse->bits.Download_Disable =(SysBootCtrl >> 19) & 0x1;
	 pTBR_Fuse->bits.UseWTM = 1; 					// Must use WTM for MMP2
     pTBR_Fuse->bits.MMCDisable = (SysBootCtrl >> 21) & 0x1;
	 pTBR_Fuse->bits.TBROpMode = (SysBootCtrl >> 10) & 0x3;
  }
#endif

#if BOOTROM
	pTBR = GetTBREnvStruct();
	pTBR->SPIFlashBackupBootPossible = FALSE;

	if (pTBR_Fuse->bits.TBROpMode == 0)
	{
		//check to see if all fuses are unprogrammed
		memset(zero, 0, 32);//clear out 32 bytes
		//FUSE_ReadDebugKey(JTAG_key_buff, 16);
		FUSE_ReadOemJtagHashKeyFuseBits(JTAG_key_buff,32);
		FUSE_ReadOemHashKeyFuseBits(oem_buff, 32);
		if (memcmp(zero, oem_buff, 32) == 0)				  // Do we force checking OEM Verification keys when probing?
			pTBR_Fuse->bits.CheckOEMKeyOnProbe = FALSE;
		else
			pTBR_Fuse->bits.CheckOEMKeyOnProbe = TRUE;

		if((memcmp(zero, JTAG_key_buff, 16) == 0) && (memcmp(zero, oem_buff, 32) == 0) &&
		   (pTBR_Fuse->bits.PlatformState == 0) && (pTBR_Fuse->bits.TBROpMode == 0))
		{
			pTBR_Fuse->bits.TBROpMode = 1;		// Default to non-trusted Boot.
		}
	}

	// TBR Operation Mode 0 or 3 means tamper, don't boot
	if ((pTBR_Fuse->bits.TBROpMode == 0) || (pTBR_Fuse->bits.TBROpMode == 3))
	{
		pTBR->ErrorCode = pTBR->ErrorCode2 = FUSE_TamperError;
		return FUSE_TamperError;
	}

	if (pTBR_Fuse->bits.TBROpMode == 2)
		pTBR_Fuse->bits.SBE = 1;			// Trusted
	else
		pTBR_Fuse->bits.SBE = 0;			// Non-Trusted

#endif

  return Retval;
}

/*
 * Set NAND and UART clocks in the PMUM Clock Gating Register
 */
void CheckDefaultClocks(void)
{
	UINT_T Temp;

	//Enable the APBC_AIB_CLK_RST clock for MFPR configuration
	//set clk bits and reset
	reg_write(APBC_AIB_CLK_RST, (APBC_AIB_CLK_RST_APBCLK | APBC_AIB_CLK_RST_FNCLK | APBC_AIB_CLK_RST_RST));
	//then take out of reset
	reg_bit_clr(APBC_AIB_CLK_RST, APBC_AIB_CLK_RST_RST);

	// Get the GPIO unit out of reset, too.
	*(VUINT_T *)APBC_GPIO_CLK_RST = 0x7;
	Temp = *(VUINT_T *)APBC_GPIO_CLK_RST;
	*(VUINT_T *)APBC_GPIO_CLK_RST = 0x3;
	Temp = *(VUINT_T *)APBC_GPIO_CLK_RST;


#if BOOTROM
	// Enable APBC_MPMU_CLK_RST
	//set clk bits and reset
	reg_write(APBC_MPMU_CLK_RST, (APBC_MPMU_CLK_RST_APBCLK | APBC_MPMU_CLK_RST_FNCLK | APBC_MPMU_CLK_RST_RST));
	//then take out of reset
	reg_bit_clr(APBC_MPMU_CLK_RST, APBC_AIB_CLK_RST_RST);

	// Enable Marvell SP Clock gating registeCheckDefaultClocks hardr
	*(VUINT_T *)PMUM_CGR_SP = (PMUM_CGR_SP_TIMER_26M | PMUM_CGR_SP_TIMER_13M | PMUM_CGR_SP_APMU_PLL1 | 					PMUM_CGR_SP_APMU_PLL2 | PMUM_CGR_SP_APMU_PLL1_2 |
		PMUM_CGR_SP_GPC | PMUM_CGR_SP_FUART | PMUM_CGR_SP_AP_26M | PMUM_CGR_SP_AP_13M | PMUM_CGR_SP_AP_6_5M);

	Temp = *(VUINT_T *)PMUM_CGR_SP;

	// WTM Clock/Reset Control Register
	*(VUINT32_T *) PMUA_WTM_CLK_RES_CTRL = 0x8;
	Temp = *(VUINT32_T *) PMUA_WTM_CLK_RES_CTRL;

	*(VUINT32_T *) PMUA_WTM_CLK_RES_CTRL = 0x9;
	Temp = *(VUINT32_T *) PMUA_WTM_CLK_RES_CTRL;

	*(VUINT32_T *) PMUA_WTM_CLK_RES_CTRL = 0x19;
	Temp = *(VUINT32_T *) PMUA_WTM_CLK_RES_CTRL;

	*(VUINT32_T *) PMUA_WTM_CLK_RES_CTRL = 0x1b;
	Temp = *(VUINT32_T *) PMUA_WTM_CLK_RES_CTRL;
#else
	// Enable Marvell PJ Clock gating registeCheckDefaultClocks hardr
	*(VUINT_T *)PMUM_CGR_PJ = (PMUM_CGR_PJ_TIMER_26M | PMUM_CGR_PJ_TIMER_13M | PMUM_CGR_PJ_APMU_PLL1 | 					PMUM_CGR_PJ_APMU_PLL2 | PMUM_CGR_PJ_APMU_PLL1_2 |
		PMUM_CGR_PJ_GPC | PMUM_CGR_PJ_FUART | PMUM_CGR_PJ_AP_26M | PMUM_CGR_PJ_AP_13M | PMUM_CGR_PJ_AP_6_5M);

	Temp = *(VUINT_T *)PMUM_CGR_PJ;
#endif

	// Enable DFC clocks to make sure.
	Temp = *(VUINT_T *)PMUA_NF_CLK_RES_CTRL;
	*(VUINT_T *)PMUA_NF_CLK_RES_CTRL |= (PMUA_NF_CLK_RES_CTRL_NF_AXICLK_EN | PMUA_NF_CLK_RES_CTRL_NF_CLK_EN | PMUA_NF_CLK_RES_CTRL_NF_ECC_CLK_EN);
	Temp = *(VUINT_T *)PMUA_NF_CLK_RES_CTRL;

	//Enable AIB NAND IO Reg - fast drive
	*(VUINT_T *)APBC_ASFAR = 0xBABA;
	*(VUINT_T *)APBC_ASSAR = 0xEB10;
	*(VUINT_T *)AIB_NAND_IO = (0xFC00 | AIB_NAND_IO_PDB);

	//Enable AIB GPIO3 (UART/SPI) IO Reg - medium drive, 3V interface
	*(VUINT_T *)APBC_ASFAR = 0xBABA;
	*(VUINT_T *)APBC_ASSAR = 0xEB10;
	*(VUINT_T *)AIB_GPIO3_IO = (0xA001 | AIB_GPIO3_IO_PDB);

	//enabled SSP1 clock, then take out of reset
  	*(volatile unsigned int *)APBC_SSP1_CLK_RST = 0x7;
  	*(volatile unsigned int *)APBC_SSP1_CLK_RST = 0x3;

#if BOOTROM
	// Enable IPC (but only if this is BootROM.)
	// if this is BootLoader, assume the BootROM has already done this.
	IPCInit();
#endif

	return;
}

/*
/ This function checks to see if the desired port to configure is actually supported in the
/ flavor of the superset
*/
UINT_T CheckSuperSetPortEnablement(UINT_T Port)
{
	FUSE_SET		lFuses;
	UINT_T Retval	= NotSupportedError;

	GetPlatformFuses( 0, &lFuses );

	switch (Port)
	{
		case BTUART_D:
            break;

		// USB Reserved Packet Port Identifiers
		case SEIDENTIFIER:
		case DIFFIDENTIFIER:
		case U2DIDENTIFIER:
		case CI2IDENTIFIER:
			if (!lFuses.bits.USBDisable)
				Retval = NoError;
  			break;
        // UART Reserved Packet Port Identifiers
		case FFIDENTIFIER:
			if (!lFuses.bits.UARTDisable)
				Retval = NoError;
  			break;

		case ALTIDENTIFIER:
             break;
		default:
			 break;
	}
	return Retval;
}

INT_T Mem2Mem(flash_addr, dest, size)
{
 return NotSupportedError;
}

// MMC
CONTROLLER_TYPE ConfigureMMC(UINT8_T FlashNum, UINT_T *pBaseAddress, UINT_T *pInterruptMask, UINT_T *pFusePartitionNumber)
{
#if MMC_CODE
	// now configure the specifc MMC unit for operation.
  	if ((FlashNum == SDMMC_FLASH_OPT1_P) || (FlashNum == SDMMC_FLASH_OPT3_P))  		//MMC3 Top.
	{
		ConfigRegSetup(mmc3_pins_top);

		*pBaseAddress = SD3_BASE;
		*pInterruptMask = INT_MMC3;
		*pFusePartitionNumber = MMC_SD_USER_PARTITION;

		// Enable Clocks.
		*(UINT_T *) PMUA_SDH0_CLK_RES_CTRL = (PMUA_SDH0_CLK_RES_CTRL_SDH0_AXI_RST | PMUA_SDH0_CLK_RES_CTRL_SDH0_RST | PMUA_SDH0_CLK_RES_CTRL_SDH0_AXICLK_EN |
			PMUA_SDH0_CLK_RES_CTRL_SDH0_CLK_EN | (1 << PMUA_SDH0_CLK_RES_CTRL_SDIO_CLK_DIV_SEL_BASE));	//Enable PMUA clock for this interface
		*(UINT_T *) PMUA_SDH2_CLK_RES_CTRL = (PMUA_SDH2_CLK_RES_CTRL_SDH2_AXI_RST | PMUA_SDH2_CLK_RES_CTRL_SDH2_RST | PMUA_SDH2_CLK_RES_CTRL_SDH2_AXICLK_EN |
			PMUA_SDH2_CLK_RES_CTRL_SDH2_CLK_EN);	//Enable PMUA clock for this interface
		return MMCSDHC0_1;
	}
  	else if ((FlashNum == SDMMC_FLASH_OPT2_P) || (FlashNum == SDMMC_FLASH_OPT4_P))  		//MMC3 Bottom.
	{
		ConfigRegSetup(mmc3_pins_bottom);

		*pBaseAddress = SD3_BASE;
		*pInterruptMask = INT_MMC3;
		*pFusePartitionNumber = MMC_SD_USER_PARTITION;

		// Enable Clocks.
		*(UINT_T *) PMUA_SDH0_CLK_RES_CTRL = (PMUA_SDH0_CLK_RES_CTRL_SDH0_AXI_RST | PMUA_SDH0_CLK_RES_CTRL_SDH0_RST | PMUA_SDH0_CLK_RES_CTRL_SDH0_AXICLK_EN |
			PMUA_SDH0_CLK_RES_CTRL_SDH0_CLK_EN | (1 << PMUA_SDH0_CLK_RES_CTRL_SDIO_CLK_DIV_SEL_BASE));	//Enable PMUA clock for this interface
		*(UINT_T *) PMUA_SDH2_CLK_RES_CTRL = (PMUA_SDH2_CLK_RES_CTRL_SDH2_AXI_RST | PMUA_SDH2_CLK_RES_CTRL_SDH2_RST | PMUA_SDH2_CLK_RES_CTRL_SDH2_AXICLK_EN |
			PMUA_SDH2_CLK_RES_CTRL_SDH2_CLK_EN);	//Enable PMUA clock for this interface
		return MMCSDHC0_1;
	}
#if MMC_DEBUG
	else if (FlashNum == SDMMC_FLASH_OPT0_P)	//MMC1
	{
		ConfigRegSetup(mmc1_pins);

		*pBaseAddress = SD1_BASE;
		*pInterruptMask = INT_MMC1;
		*pFusePartitionNumber = MMC_SD_USER_PARTITION;

		//Enable PMUA clock for this interface
		*(UINT_T *) PMUA_SDH0_CLK_RES_CTRL = (PMUA_SDH0_CLK_RES_CTRL_SDH0_AXI_RST | PMUA_SDH0_CLK_RES_CTRL_SDH0_RST | PMUA_SDH0_CLK_RES_CTRL_SDH0_AXICLK_EN |
			PMUA_SDH0_CLK_RES_CTRL_SDH0_CLK_EN | (1 << PMUA_SDH0_CLK_RES_CTRL_SDIO_CLK_DIV_SEL_BASE));

		return MMCSDHC0_1;
	}
#endif
	else
		return MMCNOTENABLED;
#else
	return MMCNOTENABLED;
#endif
}

void DisableMMCSlots()
{
	DisablePeripheralIRQInterrupt(INT_MMC3);

	// Disable PMUA clocks for the SD controllers
	*(UINT_T *) PMUA_SDH2_CLK_RES_CTRL = 0x0;	//Disable PMUA clock for this interface
	*(UINT_T *) PMUA_SDH0_CLK_RES_CTRL = 0x0;	//Disable PMUA clock for this interface

	// Restores the MMC GPIO's back to their default values
    ConfigRegRestore(mmc3_pins_top);
    ConfigRegRestore(mmc3_pins_bottom);

}

// Wrapper for IppCp to call Fuse Library (GEU or FUSE)
#if BOOTROM    // JLC: seems to be a bootrom thing; will verify
UINT_T ReadOemHashKeyFuseBits(UINT_T* pBuffer, UINT_T Size )
{
	UINT status;
	status = FUSE_ReadOemHashKeyFuseBits(pBuffer, Size);
	return status;
}
#endif

// Get the VID and PID from fuses
UINT_T GetUSBIDFuseBits(unsigned short* VID, unsigned short* PID )
{
	unsigned int Retval;
	unsigned short IDs[2];

	Retval = FUSE_ReadUsbIdFuseBits((unsigned int *)&IDs, 4);
	if(Retval == NoError)
	{
		*VID = IDs[0];
		*PID = IDs[1];
	}

	return Retval;
}

/*******************************************************************************************/
/*                                                                                         */
/*                                                                                         */
/*   I2C Stuff                                                                             */
/*                                                                                         */
/*                                                                                         */
/*******************************************************************************************/
void PlatformI2CClocksEnable()
{
	unsigned long	temp;

	// the I2C clock must be running for the PJ:
	*(volatile unsigned long*)PMUM_CGR_PJ |= ( (1u<<6) | (1u<<4) );	// not sure if the correct bit is 4 or 6.
	temp = *(volatile unsigned long*)PMUM_CGR_PJ;					// read back to ensure write completes.

	// do the SP, too: the I2C clock must be running
	*(volatile unsigned long*)PMUM_CGR_SP |= ( (1u<<6) | (1u<<4) );	// not sure if the correct bit is 4 or 6.
	temp = *(volatile unsigned long*)PMUM_CGR_SP;					// read back to ensure write completes.

	// clock the twsi & pull it out of reset
	*(volatile unsigned long*)APBC_TWSI1_CLK_RST=7;					// still in reset, but get clocks going.
	temp = *(volatile unsigned long*)APBC_TWSI1_CLK_RST;			// read back to ensure write completes.
	*(volatile unsigned long*)APBC_TWSI1_CLK_RST=3;					// out of reset, with clocks running.
	temp = *(volatile unsigned long*)APBC_TWSI1_CLK_RST;			// read back to ensure write completes.

	// clock the twsi & pull it out of reset
	*(volatile unsigned long*)APBC_TWSI2_CLK_RST=7;					// still in reset, but get clocks going.
	temp = *(volatile unsigned long*)APBC_TWSI2_CLK_RST;			// read back to ensure write completes.
	*(volatile unsigned long*)APBC_TWSI2_CLK_RST=3;					// out of reset, with clocks running.
	temp = *(volatile unsigned long*)APBC_TWSI2_CLK_RST;			// read back to ensure write completes.

	// clock the twsi & pull it out of reset
	*(volatile unsigned long*)APBC_TWSI3_CLK_RST=7;					// still in reset, but get clocks going.
	temp = *(volatile unsigned long*)APBC_TWSI3_CLK_RST;			// read back to ensure write completes.
	*(volatile unsigned long*)APBC_TWSI3_CLK_RST=3;					// out of reset, with clocks running.
	temp = *(volatile unsigned long*)APBC_TWSI3_CLK_RST;			// read back to ensure write completes.

	// clock the twsi & pull it out of reset
	*(volatile unsigned long*)APBC_TWSI4_CLK_RST=7;					// still in reset, but get clocks going.
	temp = *(volatile unsigned long*)APBC_TWSI4_CLK_RST;			// read back to ensure write completes.
	*(volatile unsigned long*)APBC_TWSI4_CLK_RST=3;					// out of reset, with clocks running.
	temp = *(volatile unsigned long*)APBC_TWSI4_CLK_RST;			// read back to ensure write completes.

	// clock the twsi & pull it out of reset
	*(volatile unsigned long*)APBC_TWSI5_CLK_RST=7;					// still in reset, but get clocks going.
	temp = *(volatile unsigned long*)APBC_TWSI5_CLK_RST;			// read back to ensure write completes.
	*(volatile unsigned long*)APBC_TWSI5_CLK_RST=3;					// out of reset, with clocks running.
	temp = *(volatile unsigned long*)APBC_TWSI5_CLK_RST;			// read back to ensure write completes.

	// clock the twsi & pull it out of reset
	*(volatile unsigned long*)APBC_TWSI6_CLK_RST=7;					// still in reset, but get clocks going.
	temp = *(volatile unsigned long*)APBC_TWSI6_CLK_RST;			// read back to ensure write completes.
	*(volatile unsigned long*)APBC_TWSI6_CLK_RST=3;					// out of reset, with clocks running.
	temp = *(volatile unsigned long*)APBC_TWSI6_CLK_RST;			// read back to ensure write completes.


}

#if BOOTROM
void   PlatformShutdown()
{
	P_TBR_Env  pTBR;

	pTBR = GetTBREnvStruct();

	if (pTBR->Fuses.bits.PortEnabled)
	{
		ShutdownPorts();
	}

#if JTAG_PROTOCOL_OVER_JTAG_SUPPORTED
	JTAG_Disable_DebugKey_Interrupts();
#endif
	DisablePeripheralIRQInterrupt(INT_MMC3);
	// Disable Interrupts
	DisableIrqInterrupts();

	return;
}
#endif
