/******************************************************************************
**  COPYRIGHT  2007 Marvell Inernational Ltd.
**  All Rights Reserved
******************************************************************************/


#include "predefines.h"
#include "MCU.h"
#include "mcu_extras.h"
#include "processor_enums.h"
#include "sdram_specs.h"
#include "sdram_support.h"
#include "sdram_registers_defaults.h"
#include "tim.h"
#include "Errors.h"
#include "Typedef.h"
#include "sdram_config.h"
#include "timer.h"
#if BOOTROM
#include "bootrom.h"
#else
#include "BootLoader.h"
#endif

UINT_T TzGetRegion(UINT_T region,  UINT_T ChipSelect, UINT_T* pBaseAddress, UINT_T* pSize, UINT_T* pPermission);

void ConfigureTrustZone( void *pTIM );

// This routine will configure and enable SDRam DDR.
// It has the ability to select different DDR frequencies and geometries if the TIM area contains these specifiers.
// If no specifiers are found, then default values will be used.

// Return value: the base address of the ddr region for CS0.

// Overview of the SDRam DDR initialization:
//
// If TIM SDRam configuration records are available:
// 0. Make sure the required clocks are enabled. (requires TIM Clocks Enable records)
// 1. Make sure the memory voltage planes are up. (requires TIM Voltage records)
// 2. Make sure the clocks are at the correct frequency. (requires TIM Frequency records)
// 3. Program the MCU registers. (can override default values using optional TIM SDRam records
//
// otherwise (when no TIM SDRam configuration records are available)
// 1. Program the MCU registers using hardcoded default values.
//    - no clock enable, voltage or frequency changing occurs in this mode.
//


UINT_T ConfigureMemoryController( 
	void			*pTIM, 
	void			*pDDRScratchAreaAddress, 
	unsigned long	ulDDRScratchAreaLength
	)
{
	// variables for walking through the ddr records
	DDRCSpecList_T	*pDDRCStartAddress;
	DDRCSpecList_T	*pDDRCSpecs;
	DDRCSpec_T		*pDDRCSpec;
	CMCCSpecList_T	*pCMCCSpecs;
	CMCCSpec_T		*pCMCCSpec;
	UINT_T TZDDRBaseAddr, Size, Permissions; // Used for TrustZone.

	int				nRecs;
	int				r;
	UINT_T			status;
	UINT_T			value;
	unsigned long	faultaddr;
	unsigned long	baseaddr = 0xffffffff;
    unsigned long 	readTarget;
	unsigned long	loop;
	unsigned long	retval;
	
	// variables for doing ddr record to register field value conversions and updates
/*
	unsigned long	*pReg;
	unsigned long	(*pCnvFx)();
	int				bfs, bfs_low, bfs_hi;
	int				bfl, bfl_low, bfl_hi;
	unsigned long	args[4];

	unsigned long	dclk = MMP2_DEFAULT_DCLK;		// can be overridden with DDR record.
	unsigned long	fclk = MMP2_DEFAULT_FCLK;		// used for refresh calcs, can be overrideen with DDR record.
*/

	// some flags that enable / disable functionality within this routine.
	// these can be overriden by time records.
	unsigned long	fDoSDRamInit = 0;				// usually only overridden for testing on first silicon.
	unsigned long	fDoMemoryCheck = 0;				// usually only overridden for testing on first silicon.
	unsigned long	fDoConsumeDDRPackages = 0;      // Default

	retval = DDR_NotInitializedError;   // Initialize the return value
	
/*
	unsigned long	loop;

	// override the init value for the base address register that was set above.
	// just use the power on reset default values, plus the CS_VALID bit.
	adrmapreg0 = ( *MCU_REG_MMU_MMAP0 ) | MMP2_MEMADDRMAPR0_CS_VALID;

	// default values for all the registers have been loaded.
	// now see if any of these fields are overriden by a tim record.
*/
	if( pTIM )
	{
		// Look for CMCC package in TIM
		pCMCCSpecs = (CMCCSpecList_T*)FindPackageInReserved(&status, pTIM, CMCCID);	// 'CMCC'
		if( status == NoError )
		{
			nRecs = ConfigRecordCount(pCMCCSpecs);
			pCMCCSpec=pCMCCSpecs->CMCCSpecs;
			for(r=0; r<nRecs; r++, pCMCCSpec++)
			{
				value = pCMCCSpec->KeyValue;
				switch(pCMCCSpec->KeyId)
				{
					case CMCC_CONFIG_ENA_ID:
						{fDoSDRamInit	= value;	break;}	// can enable / disable memory controller configuration
					case CMCC_MEMTEST_ENA_ID:
						{fDoMemoryCheck	= value;	break;}	// can enable / disable memory test.
					case CMCC_CONSUMER_ID:
						{
							if (value == MY_CONSUMER_ID)
								{fDoConsumeDDRPackages = 1;}
							else
								{fDoConsumeDDRPackages = 0;}
							break;
						}
					default: break;
				}//EndSwitch
			}//EndForor
		}//End processing the list of CMCC records

		if (fDoConsumeDDRPackages && fDoSDRamInit)
		{
			pDDRCStartAddress = (DDRCSpecList_T*)FindPackageInReserved(&status, pTIM, DDRCID);	// 'DDRC'
			if( status == NoError )
			{
				loop=0;		// loop counter.
				while(loop < 1000)
				{
					pDDRCSpecs = pDDRCStartAddress;
					nRecs = ConfigRecordCount(pDDRCSpecs);
					pDDRCSpec=pDDRCSpecs->DDRCSpecs;
					for(r=0; r<nRecs; r++, pDDRCSpec++)
					{
						value = pDDRCSpec->KeyValue;
						switch(pDDRCSpec->KeyId)
						{
					
						// Address Register
						case MMP2_SDRADCREG_ID:		*MCU_REG_DECODE_ADDR	= value;	break;	//(0x010)address decode 
						// Config Registers
						case MMP2_SDRCFGREG0_ID: 	*MCU_REG_SDRAM_CONFIG_0	= value;	break; //(0x020)config reg 0
						case MMP2_SDRCFGREG1_ID: 	*MCU_REG_SDRAM_CONFIG_1	= value;	break; //(0x030)config reg 1
						case MMP2_SDRCFGREG2_ID: 	*MCU_REG_SDRAM_CONFIG_2	= value;	break; //(0x040)config reg 2
						case MMP2_SDRCFGREG3_ID: 	*MCU_REG_SDRAM_CONFIG_3	= value;	break; //(0xB30)config reg 2
						case MMP2_SDRCFGREG4_ID: 	*MCU_REG_SDRAM_CONFIG_4	= value;	break; //(0xB40)config reg 2
						case MMP2_SDRCFGREG5_ID: 	*MCU_REG_SDRAM_CONFIG_5	= value;	break; //(0xB50)config reg 2
						case MMP2_SDRCFGREG6_ID: 	*MCU_REG_SDRAM_CONFIG_6	= value;	break; //(0xB60)config reg 2
						case MMP2_SDRCFGREG7_ID: 	*MCU_REG_SDRAM_CONFIG_7	= value;	break; //(0xB70)config reg 2
						//Timing Registers
						case MMP2_SDRTMGREG1_ID: 	*MCU_REG_SDRAM_TIMING_1	= value;	break; //(0x050)timing reg 1
						case MMP2_SDRTMGREG2_ID: 	*MCU_REG_SDRAM_TIMING_2	= value;	break; //(0x060)timing reg 2
						case MMP2_SDRTMGREG3_ID: 	*MCU_REG_SDRAM_TIMING_3	= value;	break; //(0x190)timing reg 3
						case MMP2_SDRTMGREG4_ID: 	*MCU_REG_SDRAM_TIMING_4	= value;	break; //(0x1C0)timing reg 4
						case MMP2_SDRTMGREG5_ID: 	*MCU_REG_SDRAM_TIMING_5	= value;	break; //(0x650)timing reg 5
						case MMP2_SDRTMGREG6_ID:	*MCU_REG_SDRAM_TIMING_6 = value;	break; //(0x660)timing reg 6
						//Control Registers
						case MMP2_SDRCTLREG1_ID: 	*MCU_REG_SDRAM_CONTROL_1 = value;	break; //(0x080)control reg 1
						case MMP2_SDRCTLREG2_ID: 	*MCU_REG_SDRAM_CONTROL_2 = value;	break; //(0x090)control reg 2
						case MMP2_SDRCTLREG3_ID: 	*MCU_REG_SDRAM_CONTROL_3 = value;	break; //(0x0F0)control reg 3
						case MMP2_SDRCTLREG4_ID: 	*MCU_REG_SDRAM_CONTROL_4 = value;	break; //(0x1A0)control reg 4
						case MMP2_SDRCTLREG5_ID: 	*MCU_REG_SDRAM_CONTROL_5 = value;	break; //(0x280)control reg 5
						case MMP2_SDRCTLREG6_ID: 	*MCU_REG_SDRAM_CONTROL_6 = value;	break; //(0x760)control reg 6
						case MMP2_SDRCTLREG7_ID: 	*MCU_REG_SDRAM_CONTROL_7 = value;	break; //(0x770)control reg 7
						case MMP2_SDRCTLREG8_ID:	*MCU_REG_SDRAM_CONTROL_8 = value;	break; //(0x780)control reg 8
						case MMP2_SDRCTLREG11_ID:	*MCU_REG_SDRAM_CONTROL_11 = value;	break; //(0x7B0)control reg 11
						case MMP2_SDRCTLREG13_ID:	*MCU_REG_SDRAM_CONTROL_13 = value;	break; //(0x7D0)control reg 13
						case MMP2_SDRCTLREG14_ID:	*MCU_REG_SDRAM_CONTROL_14 = value;	break; //(0x7E0)control reg 14
						//MCB Control Registers
						case MMP2_MCBSLFSTSELREG_ID:	*MCU_REG_MCB_SLFST_SEL   = value; break; //(0x570)mcb slfst_sel
						case MMP2_MCBSLFSTCTRLREG0_ID:	*MCU_REG_MCB_SLFST_CTRL0 = value; break; //(0x580)mcb slfst ctrl reg 0
						case MMP2_MCBSLFSTCTRLREG1_ID:	*MCU_REG_MCB_SLFST_CTRL1 = value; break; //(0x590)mcb slfst ctrl reg 1
						case MMP2_MCBSLFSTCTRLREG2_ID:	*MCU_REG_MCB_SLFST_CTRL2 = value; break; //(0x5A0)mcb slfst ctrl reg 2
						case MMP2_MCBSLFSTCTRLREG3_ID:	*MCU_REG_MCB_SLFST_CTRL3 = value; break; //(0x5B0)mcb slfst ctrl reg 3
						case MMP2_MCBCNTRLREG4_ID: 		*MCU_REG_MCB_CONTROL_4	 = value; break; //(0x540)mcb control reg 4
						//PHY Control Registers
						case MMP2_PHYCTLREG3_ID: 	*MCU_REG_PHY_CONTROL_3	  = value;	break; //(0x140)phy control reg 3
						case MMP2_PHYCTLREG7_ID: 	*MCU_REG_PHY_CONTROL_7	  = value;	break; //(0x1D0)phy control reg 7
						case MMP2_PHYCTLREG8_ID: 	*MCU_REG_PHY_CONTROL_8	  = value;	break; //(0x1E0)phy control reg 8
						case MMP2_PHYCTLREG9_ID: 	*MCU_REG_PHY_CONTROL_9	  = value;	break; //(0x1F0)phy control reg 9
						case MMP2_PHYCTLREG10_ID:   *MCU_REG_PHY_CONTROL_10	  = value;	break; //(0x200)phy control reg 10
						case MMP2_PHYCTLREG11_ID:   *MCU_REG_PHY_CONTROL_11	  = value;	break; //(0x210)phy control reg 11
						case MMP2_PHYCTLREG12_ID:   *MCU_REG_PHY_CONTROL_12	  = value;	break; //(0x220)phy control reg 12
						case MMP2_PHYCTLREG13_ID:   *MCU_REG_PHY_CONTROL_13	  = value;  break; //(0x230)phy control reg 13
						case MMP2_PHYCTLREG14_ID:   *MCU_REG_PHY_CONTROL_14	  = value;  break; //(0x240)phy control reg 14
						case MMP2_PHYCTLREG15_ID:   *MCU_REG_PHY_CONTROL_15	  = value;  break; //(0x250)phy control reg 15

						case MMP2_DLLCTLREG1_ID: 	*MCU_REG_PHY_DLL_CONTROL_1 = value; break; //(0xE10)phy dll control reg 1
						case MMP2_DLLCTLREG2_ID: 	*MCU_REG_PHY_DLL_CONTROL_2 = value; break; //(0xE20)phy dll control reg 2
						case MMP2_DLLCTLREG3_ID: 	*MCU_REG_PHY_DLL_CONTROL_3 = value; break; //(0xE30)phy dll control reg 3
						case MMP2_DLLCTLREG4_ID: 	*MCU_REG_PHY_DLL_CONTROL_4 = value; break; //(0xE40)phy dll control reg 4
						case MMP2_DLLCTLREG5_ID: 	*MCU_REG_PHY_DLL_CONTROL_5 = value; break; //(0xE50)phy dll control reg 5
						case MMP2_PHYCTLTEST_ID:	*MCU_REG_PHY_CONTROL_TEST  = value; break; //(0xE80)phy dll control reg 5
						
						case MMP2_ADRMAPREG0_ID: 	*MCU_REG_MMU_MMAP0		   = value; break; //(0x100)address map cs0
						case MMP2_ADRMAPREG1_ID: 	*MCU_REG_MMU_MMAP1		   = value;	break; //(0x110)address map cs1
						case MMP2_ADRMAPREG2_ID: 	*MCU_REG_MMU_MMAP2		   = value;	break; //(0x130)address map cs2
						case MMP2_ADRMAPREG3_ID: 	*MCU_REG_MMU_MMAP3		   = value;	break; //(0xA30)address map cs3
						
						case MMP2_USRCMDREG0_ID: 	*MCU_REG_USER_INITIATED_COMMAND0 = value; break; //(0x120) user command 0
						case MMP2_USRCMDREG1_ID: 	*MCU_REG_USER_INITIATED_COMMAND1 = value; break; //(0x410) user command 1

						case MMP2_CMWRPROTREG_ID:	*MCU_REG_CM_WRITE_PROTECTION = value;	break; //(0x180)address map cs2

						case MMP2_DRAMSTATUS_ID:	*MCU_REG_DRAM_STATUS         = value;	break; //(0x1B0)dram status
						case MMP2_ERRORSTATUS_ID:	*MCU_REG_ERROR_STATUS        = value;	break; //(0x0D0)error status
						case MMP2_SYS_ID:			*MCU_REG_SYS		         = value;	break; //(0x2C0)sys
						case MMP2_EXCLMONCTRL_ID:	*MCU_REG_EXCLUSIVE_MONITOR_CTRL = value;	break; //(0x380)error status
						
						case MMP2_MODERDDATA_ID:	*MCU_REG_MODE_RD_DATA         = value;	break; //(0x440)Mode read data
						case MMP2_TESTMODE0_ID:		*MCU_REG_TEST_MODE0           = value;	break; //(0x4C0)Test Mode0
						case MMP2_TESTMODE1_ID:		*MCU_REG_TEST_MODE1           = value;	break; //(0x4D0)Test Mode1
						case MMP2_REGTBLCTRL0_ID:	*MCU_REG_REGISTER_TABLE_CTRL_0 = value;	break; //(0xC00)RegisterTable CTRL 0
						case MMP2_REGTBLDATA0_ID:	*MCU_REG_REGISTER_TABLE_DATA_0 = value;	break; //(0xC20)RegisterTable Data 0
						case MMP2_REGTBLDATA1_ID:	*MCU_REG_REGISTER_TABLE_DATA_1 = value;	break; //(0xC30)RegisterTable Data 1
						case MMP2_ERROR_IDREG_ID:	*MCU_REG_ERROR_ID              = value; break; //(0x490)Error ID register
						case MMP2_ERROR_ADDRREG_ID:	*MCU_REG_ERROR_ADDR            = value; break; //(0x4A0)Error Address register
						case MMP2_OPDELAY_ID:		Delay(value); break;//TBD
						case MMP2_OPREAD_ID:		readTarget = *(unsigned long*) value; break;//TBD

						default: break;
						}//EndSwitch
					}//EndFor - Done processing DDRC package
					// wait for DDR Initialization to complete:
					while( ((*MCU_REG_DRAM_STATUS) & MCU_DRAM_STATUS_INIT_DONE ) == 0 )
					{
						// wait for hardware to assert the INIT_DONE bit
					}

					baseaddr = ( *MCU_REG_MMU_MMAP0	) & 0xff800000;

					// finally, do some dummy reads in (and discard the values)
					r = *(unsigned volatile long*)baseaddr;
					r = *(unsigned volatile long*)baseaddr;
					r = *(unsigned volatile long*)baseaddr;
					r = *(unsigned volatile long*)baseaddr;
					r = *(unsigned volatile long*)baseaddr;

					// this is the offical end of the MCU initialization.

					if( !fDoMemoryCheck ) 
					{
						retval = NoError; // Memory test disabled.
						break;		
					}

					// now do an integrity check to ensure the memory is reliable
					// loop, and the save location for loop, is for debug use, to see how many times
					// the mcu must be init before working OK.
					status = 0;
					status = CheckMemoryReliability( (unsigned long)pDDRScratchAreaAddress, ulDDRScratchAreaLength, &faultaddr);		// start, length, ptr to ulong for failing address
					if( status == 0 ) 
					{
						retval = NoError; // Memory test OK
						break;		
					}
					// an error occurred at the address contained in 'value'.
					// does memory need to be reinitialized?
					// or maybe try again.
					//   for now, see if just trying again will do the trick.
					// stay in this init / validate loop until the memory check returns good results.

					loop++; // keep track of how many times through.	
								
				}//EndWhile
			}//EndIf Found DDRC

            // Check if we should initialize Trustzone
			// If ((we tried to init DDR) AND (CheckMemoryReliability didn't fail (maybe because we didn't try))
			// If we didn't attempt to init DDR, status would NOT be zero because we did not find a DDRC package
			// Then - Configure Trustzone
			if (fDoSDRamInit && (status == 0))
			{
				// Configure Trustzone
				ConfigureTrustZone( pTIM );

				// Make sure that region A (0) is protected
				retval = TzGetRegion (TZ_REGION0, TZ_CHIPSELECT0, &TZDDRBaseAddr, &Size, &Permissions);

				//Make sure that the region is accessible by Secure Processor only
				if ((Permissions != TZ_PERM_ABORT) || (retval != NoError))
				{
					retval = TZ_Region0NotProtected;
				}
			}
			else
			{
				// Either we didn't try to initialize DDR
				// Or we tried and failed the CheckMemoryReliability test.
				// So Do not configure trustzone and return an error.
				retval = DDR_NotInitializedError;
			}
		}//EndIf (fDoConsumeDDRPackages && fDoSDRamInit)
        else
            return DDR_NotInitializedError;
	}//EndIf pTim
	return retval;
}//EndConfigureMemoryController


void ConfigureTrustZone( void *pTIM )
{
    TZSpecList_T	*pTZSpecs;   // Trustzone
	TZSpec_T		*pTZSpec;    // Trustzone
    int				nRecs;
	int				r;
	UINT_T			status;
	UINT_T			value;
    //TZID support
	UINT_T			cs0, cs1;         //Chip Select found flags
	unsigned long tzSelReg = 0;
	unsigned long tzCs0RangeReg0 = 0;
	unsigned long tzCs0RangeReg1 = 0;
	unsigned long tzCs0PermReg0 = 0;

	// variables for doing  record to register field value conversions and updates
	unsigned long	*pReg;
	unsigned long	(*pCnvFx)();
	int				bfs;         //Bit Field Start Offset
	int				bfl;         // Bit Field Length
	unsigned long	args[4];
    unsigned long	trustzonesetup = 0;
	//
    // Set up Trust Zones
	//
    pTZSpecs = (TZSpecList_T*)FindPackageInReserved(&status, pTIM, TZON);	// 'TZON'
	if( status == NoError )
	{
		nRecs = ConfigRecordCount(pTZSpecs);
		pTZSpec=pTZSpecs->TZSpecs;
		for(r=0; r<nRecs; r++, pTZSpec++)
		{
			// start building the conversion argument list:
			args[0] = pTZSpec->KeyValue;
			for(r=0; r<nRecs; r++, pTZSpec++)
			{
				value = pTZSpec->KeyValue;
				switch(pTZSpec->KeyId)
				{
					case MMP2_TZSELECT_ID:		*MCU_REG_TRUSTZONE_SEL        = value;	break; //(0x3B0)trustzone select
					case MMP2_TZRANGE0_ID:		*MCU_REG_TRUSTZONE_RANGE0     = value;	break; //(0x3C0)trustzone range0
					case MMP2_TZRANGE1_ID:		*MCU_REG_TRUSTZONE_RANGE1     = value;	break; //(0x3D0)trustzone range1
					case MMP2_TZPERMISSION_ID:	*MCU_REG_TRUSTZONE_PERMISSION = value;	break; //(0x3E0)trustzone permission
					default: break;
				}//Endswitch
			}//EndFor
		}//EndFor
		trustzonesetup = 1;
	}//EndIf TZON found

	if (!trustzonesetup)
	{
    	pTZSpecs = (TZSpecList_T*)FindPackageInReserved(&status, pTIM, TZID);	// 'TZID'
		if( status == NoError )
		{
			nRecs = ConfigRecordCount(pTZSpecs);
			pTZSpec=pTZSpecs->TZSpecs;
			for(r=0; r<nRecs; r++, pTZSpec++)
			{
				// start building the conversion argument list:
				args[0] = pTZSpec->KeyValue;
				// set subsequent args, register selection and register field info depending on the ID in the record.
				cs0 = 0;
				cs1 = 0;
				switch(pTZSpec->KeyId)
				{
					// fields in Trustzone Select Register
					case TZLOCK_ID:
						{pReg = &tzSelReg; pCnvFx = CR_none; bfs = TZ_LOCK_BASE; bfl = 1; break;}

					// Fields in TrustZone Range Register 0
					case TZCS0_RANGE0_ID:
						{pReg = &tzCs0RangeReg0; pCnvFx = CR_none; bfs = TZ_RANGE0_BASE; bfl = TZ_RANGE_SIZE; cs0=1; break;}
					case TZCS0_RANGE1_ID:
						{pReg = &tzCs0RangeReg0; pCnvFx = CR_none; bfs = TZ_RANGE1_BASE; bfl = TZ_RANGE_SIZE; cs0=1; break;}

						// Fields in TrustZone Range Register 1
					case TZCS0_RANGE2_ID:
						{pReg = &tzCs0RangeReg1; pCnvFx = CR_none; bfs = TZ_RANGE2_BASE; bfl = TZ_RANGE_SIZE; cs0=1; break;}
					case TZCS0_RANGE3_ID:
						{pReg = &tzCs0RangeReg1; pCnvFx = CR_none; bfs = TZ_RANGE3_BASE; bfl = TZ_RANGE_SIZE; cs0=1; break;}

						// Fields in TrustZone Permission Register
					case TZCS0_PERMR0_ID:
						{pReg = &tzCs0PermReg0; pCnvFx = CR_none; bfs = TZ_PERM_BASE_R0; bfl = TZ_PERM_SIZE; cs0=1; break;}
					case TZCS0_PERMR1_ID:
						{pReg = &tzCs0PermReg0; pCnvFx = CR_none; bfs = TZ_PERM_BASE_R1; bfl = TZ_PERM_SIZE; cs0=1; break;}
					case TZCS0_PERMR2_ID:
						{pReg = &tzCs0PermReg0; pCnvFx = CR_none; bfs = TZ_PERM_BASE_R2; bfl = TZ_PERM_SIZE; cs0=1; break;}
					case TZCS0_PERMR3_ID:
						{pReg = &tzCs0PermReg0; pCnvFx = CR_none; bfs = TZ_PERM_BASE_R3; bfl = TZ_PERM_SIZE; cs0=1; break;}
					case TZCS0_PERMRU_ID:
						{pReg = &tzCs0PermReg0; pCnvFx = CR_none; bfs = TZ_PERM_BASE_RU; bfl = TZ_PERM_SIZE; cs0=1; break;}

					// Add CS1 case statements here

					default: pCnvFx = 0;
				}//Endswitch

				// Set the specified bitfield in the target register (local image tobe copied to the real register later)
				if( pCnvFx ) SetBitField( pReg, pCnvFx( args ), bfs, bfl );
			}//EndFor

			// Now write the values to the hardware registers
			if(cs0)
			{
				// Set TZ Enable bit in the shadow permission register for CS0
				{pReg = &tzCs0PermReg0; pCnvFx = CR_none; bfs = TZ_ENABLE_BASE; bfl = 1; cs0 = 1; }
				if( pCnvFx ) SetBitField( pReg, pCnvFx( args ), bfs, bfl );
				// Set chip select 0
				*MCU_REG_TRUSTZONE_SEL = TZ_CHIPSELECT0;
				// Write CS0 Trustzone Registers from local copy
				*MCU_REG_TRUSTZONE_RANGE0 = tzCs0RangeReg0;
				*MCU_REG_TRUSTZONE_RANGE1 = tzCs0RangeReg1;
				// Permission register has the enable bit and the lock bit(write it last)
				*MCU_REG_TRUSTZONE_PERMISSION   = tzCs0PermReg0;
			}
			/*
			if (cs1) // if enabling CS1 trustzone, do it here
			{
    			// Set TZ Enable bit in the shadow permission register for CS1
				{pReg = &tzCs1PermReg0; pCnvFx = CR_none; bfs = TZ_ENABLE_BASE; bfl = 1; cs0 = 1; }
				if( pCnvFx ) SetBitField( pReg, pCnvFx( args ), bfs, bfl );
				// Set chip select 1
				*MCU_REG_TRUSTZONE_SEL = TZ_CHIPSELECT1;
				// Write CS1 Trustzone Registers from local copy
				*MCU_REG_TRUSTZONE_RANGE0 = tzCs1RangeReg0;
				*MCU_REG_TRUSTZONE_RANGE1 = tzCs1RangeReg1;
				// Permission register has the enable bit (write it last)
				*MCU_REG_TRUSTZONE_PERMISSION   = tzCs1PermReg0;
			}
			*/

		   // Lock the trustzone registers down (what is the scope of the lock - current TZ_REG_SEL or global?)
			pReg = &tzSelReg;
			pCnvFx = CR_none;
			bfs = TZ_LOCK_BASE;
			bfl = 1;
			args[0] = 1;
			// Set Lock bit field in temporary register image to be copied to the real register next)
			if( pCnvFx ) SetBitField( pReg, pCnvFx( args ), bfs, bfl );

			// Lock the trustzone registers down
			*MCU_REG_TRUSTZONE_SEL = tzSelReg;
			trustzonesetup = 1;
		}//Endif TZID found
	}//Endif !trustzonesetup    

    if (!trustzonesetup)
	{
       	//Set up default trustzone if none of the above were found
    	// Setup the range 0 to be 64Kb (1 64Kb chunk)
		pReg = &tzCs0RangeReg0;
		pCnvFx = CR_none;
		bfs = TZ_RANGE0_BASE;
		bfl = TZ_RANGE_SIZE;
		args[0] = 1; // One 64K chunk of memory
        // Set Range 0 field in the temporary register image to be copied to the real register later)
	    if( pCnvFx ) SetBitField( pReg, pCnvFx( args ), bfs, bfl );

		// Set permission to abort
		pReg = &tzCs0PermReg0;
		pCnvFx = CR_none;
		bfs = TZ_PERM_BASE_R0;
		bfl = TZ_PERM_SIZE;
		args[0] = TZ_PERM_ABORT; // permission = abort
		// Set Permission field in temporary register image to be copied to the real register later)
	    if( pCnvFx ) SetBitField( pReg, pCnvFx( args ), bfs, bfl );

       	// Now write the values to the hardware registers
		// First set TZ Enable bit in the shadow permission register for CS0
		pReg = &tzCs0PermReg0;
		pCnvFx = CR_none;
		bfs = TZ_ENABLE_BASE;
		bfl = 1;
		args[0] = 1; // Enable bit value
		if( pCnvFx ) SetBitField( pReg, pCnvFx( args ), bfs, bfl );

		// Set chip select 0
		*MCU_REG_TRUSTZONE_SEL = TZ_CHIPSELECT0; // Select Chip Select 0
		// Write CS0 Trustzone Registers from local copy
		*MCU_REG_TRUSTZONE_RANGE0 = tzCs0RangeReg0;
	    // Permission register has the permission and enable bit (write it last)
	    *MCU_REG_TRUSTZONE_PERMISSION   = tzCs0PermReg0;

		// Lock the trustzone registers down(what is the scope of the lock - current TZ_REG_SEL or global?)
		pReg = &tzSelReg;
		pCnvFx = CR_none;
		bfs = TZ_LOCK_BASE;
		bfl = 1;
        args[0] = 1;
        // Set Lock bit field in temporary register image to be copied to the real register next)
	    if( pCnvFx ) SetBitField( pReg, pCnvFx( args ), bfs, bfl );

		// Lock the trustzone registers down
		*MCU_REG_TRUSTZONE_SEL = tzSelReg;
	}//Endif !trustzonesetup    
	return;
}//End ConfigureTrustZone

UINT_T TzGetRegion(UINT_T region,  UINT_T ChipSelect, UINT_T* pBaseAddress, UINT_T* pSize, UINT_T* pPermission)
{
	UINT_T RegionSize[TZ_MAXREGIONS];
	UINT_T RegionPermission[TZ_MAXREGIONS];
	UINT_T RegionStartOffset[TZ_MAXREGIONS];
	UINT_T MemoryGranularity = 64*1024;
	int i;
	unsigned volatile long* pReg;
	UINT_T bfs; // lsb location of bit field
	UINT_T bfl; // number of bits in the fields
	UINT_T DDRSize;

    if (ChipSelect != 0) return NotSupportedError;
	if (region > TZ_REGIONU) return NotSupportedError;

    // Set chip select 0
	*MCU_REG_TRUSTZONE_SEL = TZ_CHIPSELECT0;

	//Calculate the region start offset
	RegionStartOffset[0] = 0;
	for (i=0; i<=region; i++)
	{
		switch(i)
		{
			case TZ_REGION0:
				{
					RegionStartOffset[0] = 0;
					pReg = MCU_REG_TRUSTZONE_RANGE0;
					bfs = TZ_RANGE0_BASE;
					bfl = TZ_RANGE_SIZE;
					RegionSize[i] = (ReadBitField(pReg, bfs, bfl))<< 16; //64Kb chunks
					break;
				}
			case TZ_REGION1:
				{
					RegionStartOffset[i] = RegionStartOffset[i-1] + RegionSize[i-1];
					pReg = MCU_REG_TRUSTZONE_RANGE0;
					bfs = TZ_RANGE1_BASE;
					bfl = TZ_RANGE_SIZE;
					RegionSize[i] = (ReadBitField(pReg, bfs, bfl))<<16;//64Kb chunks
					break;
				}
			case TZ_REGION2:
				{
	            RegionStartOffset[i] = RegionStartOffset[i-1] + RegionSize[i-1];
				pReg = MCU_REG_TRUSTZONE_RANGE1;
				bfs = TZ_RANGE2_BASE;
				bfl = TZ_RANGE_SIZE;
				RegionSize[i] = (ReadBitField(pReg, bfs, bfl))<<16;//64Kb chunks

				break;
				}
			case TZ_REGION3:
				{
	            RegionStartOffset[i] = RegionStartOffset[i-1] + RegionSize[i-1];
				pReg = MCU_REG_TRUSTZONE_RANGE1;
				bfs = TZ_RANGE3_BASE;
				bfl = TZ_RANGE_SIZE;
				RegionSize[i] = (ReadBitField(pReg, bfs, bfl))<<16;//64Kb chunks
				break;
				}
			case TZ_REGIONU:
				{
                RegionStartOffset[i] = RegionStartOffset[i-1] + RegionSize[i-1];
                DDRSize = GetDDRSize();
                RegionSize[i] = DDRSize - RegionStartOffset[i];
				}
		}//EndSwitch
	}//EndFor
    *pBaseAddress = RegionStartOffset[region];
	*pSize = RegionSize[region];

	// Get the region permission
	switch(region)
	{
		case TZ_REGION0: {pReg = MCU_REG_TRUSTZONE_PERMISSION; bfs = TZ_PERM_BASE_R0; bfl = TZ_PERM_SIZE; break;}
		case TZ_REGION1: {pReg = MCU_REG_TRUSTZONE_PERMISSION; bfs = TZ_PERM_BASE_R1; bfl = TZ_PERM_SIZE; break;}
		case TZ_REGION2: {pReg = MCU_REG_TRUSTZONE_PERMISSION; bfs = TZ_PERM_BASE_R2; bfl = TZ_PERM_SIZE; break;}
		case TZ_REGION3: {pReg = MCU_REG_TRUSTZONE_PERMISSION; bfs = TZ_PERM_BASE_R3; bfl = TZ_PERM_SIZE; break;}
	    case TZ_REGIONU: {pReg = MCU_REG_TRUSTZONE_PERMISSION; bfs = TZ_PERM_BASE_RU; bfl = TZ_PERM_SIZE; break;}
	}
    *pPermission = ReadBitField(pReg, bfs, bfl);

	return NoError;
}//EndRoutine


UINT_T GetDDRSize()
{
    // This supports CS0 only
	UINT_T size;
	unsigned volatile long * pReg;
	UINT_T bfs;		// lsb location of bit field
	UINT_T bfl;		// number of bits in the fields
	// Read Memory Address Map Register 0 - CS0 (offset 0x0100)
    // (MEMORY_ADDRESS_MAP)
    pReg = MCU_REG_MMU_MMAP0;
	bfs = 16;
	bfl = 4;
	size = ReadBitField(pReg, bfs, bfl);
    switch(size)
	{
		case(0xF): {size = 0x80000000; break;} //(UINT)(2048*(1024*1024))
		case(0xE): {size = 1024*(1024*1024); break;}
		case(0xD): {size =  512*(1024*1024); break;}
		case(0xC): {size =  256*(1024*1024); break;}
		case(0xB): {size =  128*(1024*1024); break;}
		case(0xA): {size =   64*(1024*1024); break;}
		case(0x9): {size =   32*(1024*1024); break;}
		case(0x8): {size =   16*(1024*1024); break;}
		case(0x7): {size =    8*(1024*1024); break;}
		default:   {size =    0; break;}
	}
    return size;
}

UINT_T GetDDRBaseAddress()
{
    // This supports CS0 only
	UINT_T baseAddress;
	
	// Read Memory Address Map Register 0 (MMU_MMAP0)- CS0 (offset 0x0100)
	// Keep the upper 9 bits (Start Address)
   	baseAddress = (*(unsigned volatile long *)MCU_REG_MMU_MMAP0)& MEMORY_ADDRESS_MAP_START_ADDR_MSK;
	return baseAddress;
} 



UINT_T PlatformSpecificPostDDRConfig(pTIM pTIM_h, pFUSE_SET pFuses)
{
	// Platform Specific stub for DDR init post processing
	UINT_T retval = NoError;
  
	// Configure Trustzone
	ConfigureTrustZone( pTIM_h );

 	return retval;
}
