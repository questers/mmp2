/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into
 *  products for purposes authorized by the license agreement provided they
 *  include this notice and the associated copyright notice with any such
 *  product.
 *  The information in this file is provided "AS IS" without warranty.
 *
 ******************************************************************************/

#include "PlatformConfig.h"
#include "platform_interrupts.h"
#include "predefines.h"
#include "USB1.h"
#include "PMUA.h"
#include "PMUM.h"
#include "usbPal.h"
#include "timer.h"
#include "uart_regs.h"
#if BOOTROM
#include "bootrom.h"
#endif
#include "misc.h"
#include "sdhc1.h"

// probe order for TTC  CS0 XIP, x16NAND, x8NAND, OneNAND, mDOC, MMC.  Note mDoc and MMC are disabled presently
#if FPGA
const UINT8_T ProbeOrder[] = {CS0_XIP_FLASH_P,NAND_FLASH_X16_HM_P,NAND_FLASH_X8_HM_P,NAND_FLASH_X16_BCH_P,NAND_FLASH_X8_BCH_P, SPI_FLASH_P, NO_FLASH_P};
#else
const UINT8_T ProbeOrder[] = {CS0_XIP_FLASH_P,NAND_FLASH_X16_HM_P,NAND_FLASH_X8_HM_P,NAND_FLASH_X16_BCH_P,NAND_FLASH_X8_BCH_P,ONENAND_FLASH_P,SDMMC_FLASH_OPT1_P, SDMMC_FLASH_OPT2_P, SDMMC_FLASH_OPT3_P, SPI_FLASH_P, NO_FLASH_P};
#endif

/*
 *  Table definitions for multi function pin registers
 */


// on the aspen, only mkin0, mkin1, mkin4, mkout6 and mkout7 are used.
CS_REGISTER_PAIR_S keypad_pins[] =
{
 		(int *) (APPS_PAD_BASE | 0x01b8), 0xa8c7, 0x0,	//GPIO_110 kp_mkin[0],  af7, pd, ds_slow, no-edge
 		(int *) (APPS_PAD_BASE | 0x01b4), 0xa8c7, 0x0,	//GPIO_109 kp_mkin[1],  af7, pd, ds_slow, no-edge
 		(int *) (APPS_PAD_BASE | 0x01e4), 0xa8c7, 0x0,	//GPIO_121 kp_mkin[4],  af7, pd, ds_slow, no-edge

 		(int *) (APPS_PAD_BASE | 0x01c0), 0x08c7, 0x0,	//GPIO_112 kp_mkout[6], af7,   , ds_slow, no-edge
 		(int *) (APPS_PAD_BASE | 0x01bc), 0x08c7, 0x0,	//GPIO_111 kp_mkout[7], af7,   , ds_slow, no-edge
};

CS_REGISTER_PAIR_S io_pins[] =
{
 		(int *) (APPS_PAD_BASE | 0x0088), 0x880, 0x0,	//ND_IO0
		(int *) (APPS_PAD_BASE | 0x0084), 0x880, 0x0,	//ND_IO1
		(int *) (APPS_PAD_BASE | 0x0080), 0x880, 0x0,	//ND_IO2
		(int *) (APPS_PAD_BASE | 0x007c), 0x880, 0x0,	//ND_IO3
		(int *) (APPS_PAD_BASE | 0x0078), 0x880, 0x0,	//ND_IO4
		(int *) (APPS_PAD_BASE | 0x0074), 0x880, 0x0,	//ND_IO5
		(int *) (APPS_PAD_BASE | 0x0070), 0x880, 0x0,	//ND_IO6
		(int *) (APPS_PAD_BASE | 0x006c), 0x880, 0x0,	//ND_IO7
		(int *) (APPS_PAD_BASE | 0x0068), 0x8C0, 0x0,	//ND_IO8
		(int *) (APPS_PAD_BASE | 0x0064), 0x8C0, 0x0,	//ND_IO9
		(int *) (APPS_PAD_BASE | 0x0060), 0x8C0, 0x0,	//ND_IO10
		(int *) (APPS_PAD_BASE | 0x005c), 0x8C0, 0x0,	//ND_IO11
		(int *) (APPS_PAD_BASE | 0x0058), 0x8C0, 0x0,	//ND_IO12
		(int *) (APPS_PAD_BASE | 0x0054), 0x8C0, 0x0,	//ND_IO13
		(int *) (APPS_PAD_BASE | 0x0050), 0x8C0, 0x0,	//ND_IO14
		(int *) (APPS_PAD_BASE | 0x004c), 0x8C0, 0x0,	//ND_IO15
		0x0,0x0,0x0 // termination
};

CS_REGISTER_PAIR_S common_pins[]=
{
		(int *) (APPS_PAD_BASE | 0x00A0), 0x0880, 0x0, 	//ND_ALE_SM_nWE
		(int *) (APPS_PAD_BASE | 0x00A4), 0x0880, 0x0,	//ND_CLE_SM_nOE
		0x0,0x0,0x0 //termination
};

CS_REGISTER_PAIR_S cs0_common_pins[]=
{
	(int *) (APPS_PAD_BASE | 0x0090), 0x0880, 0x0, 	//SM_ADVMUX / NADV#
	(int *) (APPS_PAD_BASE | 0x00C0), 0x0880, 0x0, 	//SM_SCLK
	(int *) (APPS_PAD_BASE | 0x0094), 0x0883, 0x0, 	//GPIO_18 -> map as SMC_nCS0
	0x0,0x0,0x0 //termination
};

CS_REGISTER_PAIR_S remap_mfp19_23_pins[]=
{
	(int *) (APPS_PAD_BASE | 0x0098), 0x0885, 0x0, 	//SMC_nCS0 -> Remap as MFP_19
	(int *) (APPS_PAD_BASE | 0x00A8), 0x0885, 0x0, 	//SMC_nLUA -> Remap as MFP 23
	0x0,0x0,0x0 //termination
};


CS_REGISTER_PAIR_S cs0_reg[]=
{
        (int *) (SMC_CSDFICFGx), 0x51890009, 0x0,     // SMC_CSDFICFG0
        (int *) (SMC_CSADRMAPx), 0x10000F00, 0x0,     // SMC_CSADRMAP0
		0x0,0x0,0x0 //termination
};

CS_REGISTER_PAIR_S dfc_pins[]=
{
		(int *) (APPS_PAD_BASE | 0x008c), 0x0881, 0x0, 	//ND_nCS0_SM_nCS2
		(int *) (APPS_PAD_BASE | 0x0090), 0x0880, 0x0, 	//ND_nWE
		(int *) (APPS_PAD_BASE | 0x00AC), 0x0880, 0x0,	//ND_nRE
		(int *) (APPS_PAD_BASE | 0x00B4), 0x4881, 0x0, 	//ND_RnB0
		0x0,0x0,0x0 //termination
};

CS_REGISTER_PAIR_S xip_pins[]=
{
  		(int *) (APPS_PAD_BASE | 0x00AC), 0x880, 0x0,	//SM_ADV
  		0x0,0x0,0x0 //termination
};

CS_REGISTER_PAIR_S spi_pins[]=
{
  		(int *) (APPS_PAD_BASE | 0x01AC), 0x884, 0x0,	//SSP2 RXDIN  (GPIO 107)
  		(int *) (APPS_PAD_BASE | 0x01B0), 0x884, 0x0,	//SSP2 TXDIN  (GPIO 108)
  		(int *) (APPS_PAD_BASE | 0x01B8), 0x880, 0x0,	//SSP2 CS	  (GPIO 110)
		(int *) (APPS_PAD_BASE | 0x01BC), 0x884, 0x0,	//SSP_SCLK	  (GPIO 111)
		(int *) (APPS_PAD_BASE | 0x01C0), 0x880, 0x0,	//~SSP2_FRM	  (GPIO 112)
  		0x0,0x0,0x0 //termination
};

#if WAYLAND
CS_REGISTER_PAIR_S wayland_pins[]=
{
		(int *) (APPS_PAD_BASE | 0x0028), 0xCC0, 0x0,	//GPIO47: no pull-ups, max ds, no edge detects, af 0
		0x0,0x0,0x0 //termination
};

CS_REGISTER_PAIR_S uart_pins[]=
{
  		(int *) (APPS_PAD_BASE | 0x01B0), 0xCCC1, 0x0,	//RX
  		(int *) (APPS_PAD_BASE | 0x01AC), 0xCCC1, 0x0,	//TX
  		0x0,0x0,0x0 //termination
};
#else
CS_REGISTER_PAIR_S uart_pins[]=
{
  		(int *) (APPS_PAD_BASE | 0x01AC), 0x4882, 0x0,	//RX
  		(int *) (APPS_PAD_BASE | 0x01B0), 0x4882, 0x0,	//TX
  		0x0,0x0,0x0 //termination
};
#endif

CS_REGISTER_PAIR_S alt_uart_pins[]=
{
  		(int *) (APPS_PAD_BASE | 0x01F4), 0x4882, 0x0,	//RX
  		(int *) (APPS_PAD_BASE | 0x01F8), 0x4882, 0x0,	//TX
  		0x0,0x0,0x0 //termination
};

// MMC SDHC1 controller compatible pinout
CS_REGISTER_PAIR_S mmc3_pins[]=
{
		(int *) (APPS_PAD_BASE | 0x0070), 0x8C6, 0x0,	//MMC3_CMD	   // MFP 9
		(int *) (APPS_PAD_BASE | 0x006c), 0x8C6, 0x0,	//MMC3_CLK     // MFP 8
		(int *) (APPS_PAD_BASE | 0x0068), 0x8C6, 0x0,	//MMC3_DAT0
		(int *) (APPS_PAD_BASE | 0x0064), 0x8C6, 0x0,	//MMC3_DAT1
		(int *) (APPS_PAD_BASE | 0x0060), 0x8C6, 0x0,	//MMC3_DAT2
		(int *) (APPS_PAD_BASE | 0x005c), 0x8C6, 0x0,	//MMC3_DAT3
		(int *) (APPS_PAD_BASE | 0x0058), 0x8C6, 0x0,	//MMC3_DAT4
		(int *) (APPS_PAD_BASE | 0x0054), 0x8C6, 0x0,	//MMC3_DAT5
		(int *) (APPS_PAD_BASE | 0x0050), 0x8C6, 0x0,	//MMC3_DAT6
		(int *) (APPS_PAD_BASE | 0x004c), 0x8C6, 0x0,	//MMC3_DAT7
		0x0,0x0,0x0 //termination
};

// MMC SDHC0 controller compatible pinout
CS_REGISTER_PAIR_S mmc3_alternate_pins[]=
{
		(int *) (APPS_PAD_BASE | 0x00D8), 0x8C6, 0x0,	//MMC3_CMD	   // MFP 35
		(int *) (APPS_PAD_BASE | 0x00DC), 0x8C6, 0x0,	//MMC3_CLK	   // MFP 36
		(int *) (APPS_PAD_BASE | 0x0068), 0x8C6, 0x0,	//MMC3_DAT0
		(int *) (APPS_PAD_BASE | 0x0064), 0x8C6, 0x0,	//MMC3_DAT1
		(int *) (APPS_PAD_BASE | 0x0060), 0x8C6, 0x0,	//MMC3_DAT2
		(int *) (APPS_PAD_BASE | 0x005c), 0x8C6, 0x0,	//MMC3_DAT3
		(int *) (APPS_PAD_BASE | 0x0058), 0x8C6, 0x0,	//MMC3_DAT4
		(int *) (APPS_PAD_BASE | 0x0054), 0x8C6, 0x0,	//MMC3_DAT5
		(int *) (APPS_PAD_BASE | 0x0050), 0x8C6, 0x0,	//MMC3_DAT6
		(int *) (APPS_PAD_BASE | 0x004c), 0x8C6, 0x0,	//MMC3_DAT7
		0x0,0x0,0x0 //termination
};

// MMC SDHC0 controller compatible pinout
CS_REGISTER_PAIR_S mmc1_pins[]=
{
		(int *) (APPS_PAD_BASE | 0x0030), 0x8C1, 0x0,	//MMC1_CMD		// MFP 49
		(int *) (APPS_PAD_BASE | 0x0018), 0x8C1, 0x0,	//MMC1_CLK		// MFP 43
		(int *) (APPS_PAD_BASE | 0x0010), 0x8C1, 0x0,	//MMC1_DAT0
		(int *) (APPS_PAD_BASE | 0x000C), 0x8C1, 0x0,	//MMC1_DAT1
		(int *) (APPS_PAD_BASE | 0x003C), 0x8C1, 0x0,	//MMC1_DAT2
		(int *) (APPS_PAD_BASE | 0x0038), 0x8C1, 0x0,	//MMC1_DAT3
		(int *) (APPS_PAD_BASE | 0x002C), 0x8C1, 0x0,	//MMC1_DAT4
		(int *) (APPS_PAD_BASE | 0x0044), 0x8C1, 0x0,	//MMC1_DAT5
		(int *) (APPS_PAD_BASE | 0x0004), 0x8C1, 0x0,	//MMC1_DAT6
		(int *) (APPS_PAD_BASE | 0x0000), 0x8C1, 0x0,	//MMC1_DAT7
		0x0,0x0,0x0 //termination
};

CS_REGISTER_PAIR_S msys_one_nand_pins[]=
{
  /* CS for M-SYSTEMS H3 DEVICE and OneNAND */
  		(int *) (APPS_PAD_BASE | 0x00BC), 0x2880, 0x0,	//SM_RDY should be pull down....
		0x0,0x0,0x0 //termination
};

//-----------------------------------------------------------------------
// ConfigRegSetup
//
//
//-----------------------------------------------------------------------
static void ConfigRegSetup( P_CS_REGISTER_PAIR_S regPtr)
{
    UINT32_T i,tmp;

	while(regPtr->registerAddr != 0x0)
    {
	  tmp = *(regPtr->registerAddr);
	  tmp &= 0x000003F8;
      *(regPtr->registerAddr) = regPtr->regValue | tmp;
	  tmp = *(regPtr->registerAddr);  // ensure write complete
      regPtr++;
    }
}


//-----------------------------------------------------------------------
// ConfigRegSetup
//
//
//-----------------------------------------------------------------------
static void ConfigRegWrite( P_CS_REGISTER_PAIR_S regPtr)
{
    UINT32_T i,tmp;
	while(regPtr->registerAddr != 0x0)
    {
      *(regPtr->registerAddr) = regPtr->regValue;
	  tmp = *(regPtr->registerAddr);  // ensure write complete
      regPtr++;
    }
}

//-----------------------------------------------------------------------
// ConfigRegResume
//
//
//-----------------------------------------------------------------------
void ConfigRegResume( P_CS_REGISTER_PAIR_S regPtr)
{
   UINT32_T i,tmp;

	while(regPtr->registerAddr != 0x0)
    {
      *(regPtr->registerAddr) &= 0xFFFFFFF7;
      tmp = *(regPtr->registerAddr);  // ensure write complete
      regPtr++;
    }
}

//-----------------------------------------------------------------------
// ConfigRegSave
//
//
//-----------------------------------------------------------------------
void ConfigRegSave( P_CS_REGISTER_PAIR_S regPtr)
{
    UINT32_T i,tmp;

	while(regPtr->registerAddr != 0x0)
    {
      regPtr->defaultValue = *(regPtr->registerAddr);
      regPtr++;
    }
}

//-----------------------------------------------------------------------
// ConfigRegRestore
//
//
//-----------------------------------------------------------------------
void ConfigRegRestore( P_CS_REGISTER_PAIR_S regPtr)
{
    UINT32_T i,tmp;

	while(regPtr->registerAddr != 0x0)
    {
      *(regPtr->registerAddr) = regPtr->defaultValue;
      tmp = *(regPtr->registerAddr);  // ensure write complete
      regPtr++;
    }
}

UINT_T ChipSelect2(void)
{
	return InvalidPlatformConfigError;
}

UINT_T ChipSelect0(void)
{
	ConfigRegWrite(cs0_reg);
	ConfigRegSetup(remap_mfp19_23_pins);
	ConfigRegSetup(io_pins);
	ConfigRegSetup(common_pins);
	ConfigRegSetup(cs0_common_pins);
	ConfigRegSetup(xip_pins);
	return NoError;

}

void ChipSelectOneNAND( void)
{
   ConfigRegSetup(remap_mfp19_23_pins);
   ConfigRegWrite(cs0_reg );
   ConfigRegSetup(io_pins);
   ConfigRegSetup(common_pins);
   ConfigRegSetup(cs0_common_pins);
   ConfigRegSetup(msys_one_nand_pins);
   return;
}

//-----------------------------------------------------------------------
// ChipSelect for DFC
//
//
//-----------------------------------------------------------------------

void ChipSelectDFC( void )
{
   ConfigRegSetup(remap_mfp19_23_pins);
   ConfigRegSetup(dfc_pins);
   ConfigRegSetup(io_pins);
   ConfigRegSetup(common_pins);
   return;
}

//-----------------------------------------------------------------------
// Setting up SSP for SPI Flash
//
//
//-----------------------------------------------------------------------

void ChipSelectSPI( void )
{
	//need to turn on both 52 and 6.5 MHz clocks
   	*(volatile unsigned int *)PMUM_ACGR |= PMUM_ACGR_AP_52M | PMUM_ACGR_AP_6P5M;

	//enabled SSP2 clock, then take out of reset
  	*(APBC_SSP2_CLK_RST) = 0x7;
  	*(APBC_SSP2_CLK_RST) = 0x3;
	//bring GPIO unit OOR
	reg_bit_set((APBC_BASE | 0x8), 0x7);
	reg_bit_clr((APBC_BASE | 0x8), 0x4);

	ConfigRegSetup(remap_mfp19_23_pins);
	ConfigRegSetup(spi_pins);
	ConfigRegSetup(io_pins);
	ConfigRegSetup(common_pins);
	return;
}

//------------------------------------------------------------------------
//
//
//------------------------------------------------------------------------
void RestoreDefaultConfig(void)
{

	ConfigRegRestore(dfc_pins);
	ConfigRegRestore(cs0_reg );
 	ConfigRegRestore(common_pins );
	ConfigRegRestore(cs0_common_pins );
	ConfigRegRestore(io_pins );
    ConfigRegRestore(msys_one_nand_pins );
    ConfigRegRestore(xip_pins );
    ConfigRegRestore(spi_pins );
    ConfigRegRestore(mmc3_pins );
    ConfigRegRestore(mmc3_alternate_pins );
    ConfigRegRestore(mmc1_pins );
}

void SaveDefaultConfig(void)
{
	ConfigRegSave(dfc_pins);
	ConfigRegSave(cs0_reg );
	ConfigRegSave(common_pins );
	ConfigRegSave(io_pins );
    ConfigRegSave(msys_one_nand_pins );
    ConfigRegSave(xip_pins );
    ConfigRegSave(spi_pins );
    ConfigRegSave(cs0_common_pins );
    ConfigRegSave(mmc3_pins);
    ConfigRegSave(mmc3_alternate_pins);
    ConfigRegSave(mmc1_pins);
}

//--------------------------------------------------
// mDOC data sheet specifies that BUSY signal
// means that the mDOC device should not be
// accessed.  The data sheet indicates "ACTIVE LOW"
// which I interpret to mean the device is "BUSY"
// when the BUSY signal level is 0 (low)
//--------------------------------------------------
UINT32 PlatformMdocRdyForXIPAccess( void )
{
   //GPIO 152
   //----------------------------------------------
   return NoError; //(*GPLRb & 0x01000000);  // GPIO152

}

#if BOOTROM
//
// Setup proper function pointers for resume state handler
//  		For TTC this will handle dual core boot
//
void PlatformResumeSetup(pQuick_Resume_Function *Quick_f, pTim_Resume_Function *Tim_f)
{
	*Quick_f = &QuickBoot_NotSupported;
	*Tim_f = &TIMQuickBoot_TTC;
   return;
}
#endif

const UINT8_T* GetProbeOrder()
{
	return ProbeOrder;
}

UINT_T getPlatformPortSelection(unsigned int *default_usb_port)
{
	UINT_T		port = 0;
	FUSE_SET	lFuses;

	GetPlatformFuses(0, &lFuses);

	if (!lFuses.bits.UARTDisable && !lFuses.bits.USBDisable)
	{
  		*default_usb_port = CI2_USB_D;
    	port = 6; // send default configuration USB + FFUART
	}
	else if (!lFuses.bits.UARTDisable)
	{
		port = 0x5; // send default configuration for UART 1
	}
	else if (!lFuses.bits.USBDisable)
	{
  		*default_usb_port = CI2_USB_D;
    	port = 7; // send default configuration USB only
	}
	return port;
}

UINT_T PlatformKeypadConfig(void)
{

#if WAYLAND
	// configuring for wayland
	// for wayland software upgrade detect, gpio unit needs to be on.
	//  get gpio 2470 configured as an input
	*(volatile unsigned long*)(APBC_BASE+0x08) = 3;						// pull gpio unit out of reset.			APBC_BASE is d4015000.
  	ConfigRegSave(wayland_pins);										// set af, part 1						MFPR_BASE is d401e000, gpio47 is 0x28, val is 0x0cc0.
	ConfigRegSetup(wayland_pins);										// set af, part 2						MFPR_BASE is d401e000, gpio47 is 0x28, val is 0x0cc0.
	*(volatile unsigned long*)(GPIO1_BASE+GPIO_PDR)&=~(1u<<(47%32));	// after setting af, set the direction.	GPIO0_BASE is d4019000, GPIO_PDR is 0x0c.
#else
  ConfigRegSetup(keypad_pins);
   // note: any clock and reset enabling for the keypad unit is done in InitKeypad
#endif
   // note: any clock and reset enabling for the keypad unit is done in InitKeypad
	return (NoError);
}


UINT_T PlatformUARTConfig(void)
{

   ConfigRegSetup(uart_pins);
   *(VUINT_T *)APBC_UART2_CLK_RST = (BIT4 | BIT1 | BIT0);
	return (NoError);
}


UINT_T PlatformAltUARTConfig(void)
{
   	ConfigRegSetup(alt_uart_pins);
	return (NoError);
}
/*****************************************
**		UDC 1.1 Specific Functions		**
******************************************/

/**
 * Configure USB PORT_2 for Single Ended USB Operation
 **/
UINT_T SetupSingleEndedPort2(void)
{

	return(NotSupportedError);
}

void PlatformUsbInit(UINT_T port)
{

 return;
}

UINT_T PlatformUsbTransmit(UINT_T length, UINT8_T* Buff)
{
	return NoError;
}

void PlatformUsbDmaHandler()
{
	return;
}

void PlatformUsbInterruptHandler()
{
	return;
}

void PlatformUsbShutdown()
{
	return;
}

/**********************************
**		U2D Specific Calls       **
***********************************/

void PlatformU2DInit()
{
	return;
}

UINT_T PlatformU2DTransmit(UINT_T length, UINT8_T* Buff, UINT_T WaitState)
{
   return 0;
}


void PlatformU2DShutdown()
{
	return;
}

#if USBCI
/**********************************
**      CI2 Specific Calls       **
***********************************/
static UINT32 ci2D0TurnOns = 0;
static UINT32 ci2ACCR1TurnOns = 0;
static UINT32 ci2DriverUp = 0;

//------------------------------------
//
//------------------------------------
void PlatformCI2InterruptEnable()
{
  EnablePeripheralIRQInterrupt(USB0_OTG_INT);
}

//---------------------------------
//
//---------------------------------
void PlatformCI2InterruptDisable()
{
  DisablePeripheralIRQInterrupt(USB0_OTG_INT);
}

//----------------------------------
//
//----------------------------------
UINT_T PlatformCI2Transmit(UINT_T length, UINT8_T* Buff, UINT_T WaitState)
{
#if FPGA
	return NoError;
#else
    return CI2Transmit(length, Buff, WaitState);
#endif
}

//--------------------------------
//
//--------------------------------
void PlatformCI2InterruptHandler()
{
#if FPGA
	return;
#else
    ImageCI2InterruptHandler();
#endif
}

//-------------------------------------
// PlatformCI2Shutdown
//
//   Shut down OTG periperhal
//-------------------------------------
void PlatformCI2Shutdown()
{
#if FPGA
	return;
#else
    // Don't do anything if the driver
    // isn't UP
    //--------------------------------
    if(ci2DriverUp == 0 )
      return;

    BootRomCI2HWShutdown();

    // Power down PHY and PLL
    // only turn off only those that BR
    // had to turn on
    //------------------------------------
    //*ACCR1 &= (~ci2ACCR1TurnOns);   //DCB TTC-bringup

    // Use D0CKEN_C reg to turn off 26Mhz
    // clock, for PHY PLL input
    // only turn-off only those that BR
    // had to turn on
    //------------------------------------
    //*D0CKEN_C &= (~ci2D0TurnOns);   //DCB TTC-bringup

    //*OSCC &= ~0x800; // turn off CLK_POUT
    *(VUINT_T *)PMUA_USB_CLK_RES_CTRL = 0x0;  //turn off USB AXI clock
    ci2DriverUp = 0;
#endif
}

//------------------------------------------------------
// PlatformCI2Ready()
//
//       Wait max of 400us for PLL's to be locked and ready
//------------------------------------------------------
void PlatformCI2Ready( void )
{
#if FPGA
	return;
#else
  UINT32 startTime, endTime;

  startTime = GetOSCR0();  // Dummy read to flush potentially bad data
  startTime = GetOSCR0();

  do
  {
    if(((*(volatile UINT_T *)UTMI_PLL) & UTMI_PLL_PLL_READY) == UTMI_PLL_PLL_READY)
    	break;

	endTime = GetOSCR0();
	if (endTime < startTime)
		endTime += (0x0 - startTime);
  }
  while( OSCR0IntervalInMicro(startTime, endTime) < 400 );

  //Ensure that phy control comes from OTG
  //-----------------------------------------
 // *USB2_PHY_T0 |= USB2_PHY_T0_OTG_CONTROL_BIT;
 // startTime = *USB2_PHY_T0;  // ensure write complete
#endif
}

//----------------------------------------------
// OTG PHY setup for TPV
//
//----------------------------------------------
void PlatformCI2Init()
{
	UINT_T Temp;
	UINT32 startTime, endTime;

#if FPGA
	return;
#else
    // Prevent re-init if already up
    //------------------------------
    if( ci2DriverUp != 0 )
       return;

	// Note: the USB single port host (sph) must be clocked and
	//       enabled in order for the usb2.0 device (otg) to work.

	// The basic steps are:
	//	enable the USB SPH AXI clocks
	//	bring the USB SPH AXI link out of reset
	//	enable the USB OTG clocks
	//	bring the USB OTG link out of reset
	//	enable the power to the analog circuits in the USB SPH PHY
	//	enable the UTMI PLL, and finally
	//	power up the UTMI PHY.
	//
	// some calibration follows the power up and initialization sequence.

	// would it help to configure the usb dynamic clock gate control register PMUA_USB_CLK_GATE_CTRL at 0xd4282834?
	// set the low order 16 bits to 1 to disable dynamic clock gating...
    *(VUINT_T *)PMUA_USB_CLK_RES_CTRL |= (PMUA_USB_CLK_RES_CTRL_USB_SPH_AXICLK_EN | PMUA_USB_CLK_RES_CTRL_USB_AXICLK_EN);
	Delay(1000);
    *(VUINT_T *)PMUA_USB_CLK_RES_CTRL |= (PMUA_USB_CLK_RES_CTRL_USB_SPH_AXI_RST | PMUA_USB_CLK_RES_CTRL_USB_AXI_RST);


	// Enable the ANA_GRP bit in the PU_REF register
	//	The ANA_GRP bit enables the analog logic blocks
	//	The PU_REF is in the USB SPH PHY register block.
	*(VUINT_T *) PU_REF_REG |= (1 << ANA_GRP_BIT_POSITION);
	Delay(1000);

	// Enable the UTMI PLL and power up the UTMI unit
    *(volatile UINT_T *)UTMI_CTRL |= (UTMI_CTRL_PU_PLL | UTMI_CTRL_PU);
	Delay(1000);

	// Wait for PLL SIGNAL ready before proceeding. Maxiumum 5 msec
	startTime = GetOSCR0();   // Dummy read to flush potentially bad data
	startTime = GetOSCR0();   // get the start time
 	endTime = startTime;

 	while (!((*(volatile UINT_T *)UTMI_PLL) & UTMI_PLL_PLL_READY))
 	{
       endTime = GetOSCR0();
       if (endTime < startTime)
              endTime += (0x0 - startTime);

       if (OSCR0IntervalInMilli(startTime, endTime) > 5)
			return;
 	}

    // Step 1: Turn on 3.3V supply from PMIC
    // Don't know if S/W has to do anything
    //--------------------------------------

    // Step 2: Turn on 1.8V pad ring supply
    //         and 1.2V DVM supply
    // Don't know if S/W has to do anything
    //-------------------------------------

    // Step 3: Use D0CKEN_C reg to turn on 26Mhz
    //         clock, for PHY PLL input
    //         only hit bits not set
    //------------------------------------------
    //ci2D0TurnOns = *D0CKEN_C & D0CKEN_C_OTG_CLK_ENABLE_MASK;
    //ci2D0TurnOns = (~ci2D0TurnOns) & D0CKEN_C_OTG_CLK_ENABLE_MASK;
    //*D0CKEN_C |= ci2D0TurnOns;

    // Step 4: turn off firewall bit in ASCR
    //--------------------------------------
    //*ASCR  &= 0x7fffffff;  // clear RDH bit

    // Step 5: Power up PHY and PLL
    // only hit bits not set
    //-----------------------------
    //ci2ACCR1TurnOns = *ACCR1 & ACCR1_OTG_PHY_ENABLE_MASK;
    //ci2ACCR1TurnOns = (~ci2ACCR1TurnOns) & ACCR1_OTG_PHY_ENABLE_MASK;
    //*ACCR1 |= ci2ACCR1TurnOns;
    //*USB2_PHY_T0 |= USB2_PHY_T0_OTG_CONTROL_BIT;

	//wait for 200 us
	Delay( 200 );

	// calibration begins here...
	//! utmi_test_group_0
	//set val /size=long 0xd4207018= *(unsigned long*)0xd4207018 & 0xffff7fff
	*(volatile UINT_T *)UTMI_TEST_GRP_0 &= ~(UTMI_TEST_GRP_0_REG_FIFO_SQ_RST);

	//! utmi_pll
	*(volatile UINT_T *)UTMI_PLL = 0xabc22eeb;
	*(volatile UINT_T *)UTMI_TX = 0x65410FC3;	// external tx calibration from hilbert


	//! utmi_rx
	//set val /size=long 0xd4207010= 0x631c82a3
	*(volatile UINT_T *)UTMI_RX = 0x631c82a3;

	//! utmi_ivref
	//set val /size=long 0xd4207014= 0x000004a3
	*(volatile UINT_T *)UTMI_IVREF = 0x000004a3;
	Delay(1000);

	// Set VCOCAL_START field to 0x1 for 40 us
	// bpc: enabling an auto cal cycle...
	*(volatile UINT_T *)UTMI_PLL |= UTMI_PLL_VCOCAL_START;
	Delay ( 40 );
	*(volatile UINT_T *)UTMI_PLL &= ~UTMI_PLL_VCOCAL_START;

	// bpc: now set bit five in the reserved register at 0xd4207030
	//*(unsigned long*)0xd4207030 |= (1u<<5);
	*(volatile UINT_T*)UTMI_CTL1 |= (1u<<5);

	//wait 400 us
	Delay ( 400 );

	//Set the reg_rcal_start bit for 40 us
	*(volatile UINT_T *)UTMI_TX |= UTMI_TX_REG_RCAL_START;
	Delay ( 40 );
	*(volatile UINT_T *)UTMI_TX &= ~UTMI_TX_REG_RCAL_START;



    // setup ULPI Pads
    // ULPI not available in TPV
    //--------------------------
    // ConfigRegSetup(otg_pins);

	// end of hw setup

	// configure the pll n & m fields for 26mhz. (default was set for 40mhz)
	//*(unsigned long*)0xd4207008 = ((*(unsigned long*)0xd4207008) & 0xfffff000 ) | 0x00000eeb;
	*(volatile UINT_T *)UTMI_PLL = ((*(volatile UINT_T*)UTMI_PLL) & 0xfffff000 ) | 0x00000eeb;

	// specific for aspen: set the OTG_ON bit in the UTMI_OTG_ADDON field...
	//*(unsigned long*)0xd420703c = 1;
	*(volatile UINT_T *)UTMI_OTG_ADDON = UTMI_OTG_ADDON_OTG_ON;


    // Step 6: Initialize OTG software and hardware
    // need to poll the PLL_LOCK_RDY bit in ACCR1
    // before turning on OTG "RUN" bit
	// bpc: removed flag CI_OTG_OPTIONS_FULL_SPEED from the parameter list below
    //--------------------------------------------
    BootRomCI2SWInit(
		PlatformCI2InterruptEnable,
		PlatformCI2Ready,
        ( CI_OTG_OPTIONS_8BIT_XCVR_WIDTH  | CI_OTG_OPTIONS_UTMI_XCVR_SELECT )
	);

#if CI2_USB_DDR_BUF
	// ensure full spead is not being forced:
	// we use 512 byte packets and must run in USB 2.0 mode
	*(unsigned long*)0xd4208184 &= ~(1u<<24);
#else
	// full speed works, so don't force it anymore. try highspeed (if the chirp passed)
	// force full speed mode temporarily
	*(unsigned long*)0xd4208184 |= (1u<<24);
#endif
    ci2DriverUp = 1;
#endif
}
#endif

/*-----------------------------------------------------------------------
/ This function will read service and platform fuses.
/
/-----------------------------------------------------------------------*/
UINT_T  GetPlatformFuses(UINT_T platformFuses, pFUSE_SET pTBR_Fuse)
{
  UINT_T ServiceFuses, Temp, SysBootCtrl;
  UINT_T Retval = NoError;
  UINT_T APFuses[3];				// We need enough space for 80 bits

#if BOOTROM
	P_TBR_Env pTBR;
#endif

  pTBR_Fuse->value = 0x0;

  //Enable the PMUA AES clock
  *(VUINT_T *)PMUA_AES_CLK_RES_CTRL = (PMUA_AES_CLK_RES_CTRL_AES_AXICLK_EN | PMUA_AES_CLK_RES_CTRL_AES_AXI_RST);
  //Enable the APBC_AIB_CLK_RST clock for GPIO configuration
  *(VUINT_T *)APBC_AIB_CLK_RST = 0x7;
  Temp = *(VUINT_T *)APBC_AIB_CLK_RST;
  *(VUINT_T *)APBC_AIB_CLK_RST = 0x3;

  // Unsupported options: default to respective OFF position for that option
  pTBR_Fuse->bits.UseWTM = 0;
  pTBR_Fuse->bits.EscapeSeqDisable = 1;

#if BOARD_DEBUG
  pTBR_Fuse->bits.PlatformState = 8;
  pTBR_Fuse->bits.SBE = 1;
  pTBR_Fuse->bits.UARTDisable = 0;		// Bits 13 UARTDisable - For ASPEN S0 read this as UARTEnable (Must=1 to enable)
  pTBR_Fuse->bits.USBDisable = 0;		// Bits 12 USBDisable  - For ASPEN S0 read this as USBEnable (Must=1 to enable)
  pTBR_Fuse->bits.MMCDisable = 0;
  pTBR_Fuse->bits.Download_Disable = 0;
  pTBR_Fuse->bits.UARTPort = FFUART_PORT;
  pTBR_Fuse->bits.USBPort = USB_CI2_PORT;
  pTBR_Fuse->bits.Resume = 0;
  pTBR_Fuse->bits.DDRInitialized = 0;
  pTBR_Fuse->bits.BootPlatformState = 0;
#else
  // Dummy Test, read fuses from the CIU
  SysBootCtrl = BU_REG_READ(SYS_BOOT_CNTRL);

  // User System Boot Control Register - ASPEN is non-trusted only
  //  no need to worry about attacks on trusted operation
  pTBR_Fuse->bits.PlatformState = ((SysBootCtrl >> 22) & 0xf);
  pTBR_Fuse->bits.SBE = (SysBootCtrl >> 20) & 0x1;
  pTBR_Fuse->bits.UARTDisable =(SysBootCtrl >> 12) & 0x1;
  pTBR_Fuse->bits.USBDisable =(SysBootCtrl >> 13) & 0x1;
  pTBR_Fuse->bits.MMCDisable = (SysBootCtrl >> 21) & 0x1;
  pTBR_Fuse->bits.Download_Disable =(SysBootCtrl >> 19) & 0x1;

  pTBR_Fuse->bits.JTAG_Disable =(SysBootCtrl >> 6) & 0x1;
  pTBR_Fuse->bits.USBWakeup =(SysBootCtrl >> 16) & 0x1;
#endif


#if BOOTROM
	pTBR = GetTBREnvStruct();
	pTBR->SPIFlashBackupBootPossible = FALSE;
#endif

  pTBR_Fuse->bits.UseWTM = 0; // This is not an available option

  //option 0xE in the fuses is a signal to loop indefinetly so a debugger can break in
  if((pTBR_Fuse->bits.PlatformState & 0xF) == 0xE)
  	while(TRUE);

  return Retval;
}

/*
 * Set NAND and UART clocks in the PMUM Clock Gating Register
 */
void CheckDefaultClocks(void)
{
  UINT_T Temp;

  *(VUINT_T *)PMUM_ACGR |= (
		PMUM_ACGR_APMU_52M |
		PMUM_ACGR_APMU_48M |
		PMUM_ACGR_AP_SUART |
		PMUM_ACGR_AP_26M   |
		PMUM_ACGR_G_6P5M			// this is really the 156MHz clock control. Required for DFC BCH support.
		);

  // Enable DFC clocks to make sure.
  Temp = *(VUINT_T *)PMUA_NF_CLK_RES_CTRL;
  *(VUINT_T *)PMUA_NF_CLK_RES_CTRL |= (PMUA_NF_CLK_RES_CTRL_NF_AXICLK_EN | PMUA_NF_CLK_RES_CTRL_NF_CLK_EN | PMUA_NF_CLK_RES_CTRL_NF_ECC_CLK_EN);
  Temp = *(VUINT_T *)PMUA_NF_CLK_RES_CTRL;

  // AIB unit must be out of reset for MFPR set up
  *(VUINT_T *)APBC_AIB_CLK_RST = 0x7;
  Temp = *(VUINT_T *)APBC_AIB_CLK_RST;
  *(VUINT_T *)APBC_AIB_CLK_RST = 0x3;
  Temp = *(VUINT_T *)APBC_AIB_CLK_RST;

  // Get the GPIO unit out of reset, too.
  *(VUINT_T *)APBC_GPIO_CLK_RST = 0x7;
  Temp = *(VUINT_T *)APBC_GPIO_CLK_RST;
  *(VUINT_T *)APBC_GPIO_CLK_RST = 0x3;
  Temp = *(VUINT_T *)APBC_GPIO_CLK_RST;

  return;
}

/*
/ This function checks to see if the desired port to configure is actually supported in the
/ flavor of the superset
*/
UINT_T CheckSuperSetPortEnablement(UINT_T Port)
{
	FUSE_SET lFuses;
	UINT_T Retval = NotSupportedError;

	GetPlatformFuses( 0, &lFuses );

	switch (Port)
	{
		case BTUART_D:
            break;

		// USB Reserved Packet Port Identifiers
		case SEIDENTIFIER:
		case DIFFIDENTIFIER:
		case U2DIDENTIFIER:
		case CI2IDENTIFIER:
			if (!lFuses.bits.USBDisable)
				Retval = NoError;
  			break;
        // UART Reserved Packet Port Identifiers
		case FFIDENTIFIER:
			if (!lFuses.bits.UARTDisable)
				Retval = NoError;
  			break;

		case ALTIDENTIFIER:
             break;
		default:
			 break;
	}
	return Retval;
}

INT_T Mem2Mem(flash_addr, dest, size)
{
 return NotSupportedError;
}

UINT_T PlatformSaveStateRequired(void)
{
 return FALSE;
}

CONTROLLER_TYPE ConfigureMMC(UINT8_T FlashNum, UINT_T *pBaseAddress, UINT_T *pInterruptMask, UINT_T *pFusePartitionNumber)
{
	// now configure the specifc MMC unit for operation.
  	if (FlashNum == SDMMC_FLASH_OPT1_P)	  		//MMC3
	{
  		ConfigRegSave(mmc3_pins);
		ConfigRegSetup(mmc3_pins);
		*pBaseAddress = SDHC1_1_BASE;
		*pInterruptMask = INT_SDHC1;
		*(UINT_T *) PMUA_SDH1_CLK_RES_CTRL = 0x1B;	//Enable PMUA clock for this interface
		*pFusePartitionNumber = MMC_SD_USER_PARTITION;
		return MMCSDHC1_1;
	}
	else if (FlashNum == SDMMC_FLASH_OPT2_P)	//MMC3 alternate
	{
  		ConfigRegSave(mmc3_alternate_pins);
		ConfigRegSetup(mmc3_alternate_pins);
		*pBaseAddress = SDHC1_1_BASE;
		*pInterruptMask = INT_SDHC1;
		*pFusePartitionNumber = MMC_SD_USER_PARTITION;
		*(UINT_T *) PMUA_SDH1_CLK_RES_CTRL = 0x1B;	//Enable PMUA clock for this interface
		return MMCSDHC1_1;
	}
	else if (FlashNum == SDMMC_FLASH_OPT3_P)	//MMC1
	{
  		ConfigRegSave(mmc1_pins);
		ConfigRegSetup(mmc1_pins);
		*pBaseAddress = SDHC0_1_BASE;
		*pInterruptMask = INT_SDHC0;
		*pFusePartitionNumber = MMC_SD_USER_PARTITION;
		*(UINT_T *) PMUA_SDH0_CLK_RES_CTRL = 0x1B;	//Enable PMUA clock for this interface
		return MMCSDHC0_1;
	}
	else
		return MMCNOTENABLED;
}

void DisableMMCSlots()
{
	DisablePeripheralIRQInterrupt(INT_SDHC0);
	DisablePeripheralIRQInterrupt(INT_SDHC1);

	// Disable PMUA clocks for the SD controllers
	*(UINT_T *) PMUA_SDH0_CLK_RES_CTRL = 0x0;	//Disable PMUA clock for this interface
	*(UINT_T *) PMUA_SDH1_CLK_RES_CTRL = 0x0;	//Disable PMUA clock for this interface

	// Restores the MMC GPIO's back to their default values
	ConfigRegRestore(mmc3_pins);
	// Restores the MMC GPIO's back to their default values
	ConfigRegRestore(mmc3_alternate_pins);
	// Restores the MMC GPIO's back to their default values
	ConfigRegRestore(mmc1_pins);
}



#if (BOOTROM || TRUSTED)
// Wrapper for IppCp to call Fuse Library (GEU or FUSE)
UINT_T ReadOemHashKeyFuseBits(UINT_T* pBuffer, UINT_T Size )
{
	UINT status;
	status = GEU_ReadOemHashKeyFuseBits(pBuffer, Size);
	return status;
}
/*
*****************************************************************************************
** GEU Fuse Library code used for ASPEN
**
** Since this is the only fnction required it has been cut from the the Library code and
** put here.
** Notes:
** 1. The OEM Hash Key is stored in RKEK Fuse Block (Fuse Block 1)
** 2. ASPEN has no GEU so the OEM Hash Key registers (originally RKEK)
**    are located in the CIU address space.
**
*****************************************************************************************
*/
UINT_T GEU_ReadOemHashKeyFuseBits(UINT_T* pBuffer, UINT_T Size)
{
	if (!((Size == K_SHA1_SIZE)||(Size == K_SHA256_SIZE)))
	{
        return (GEU_InvalidBufferSize);
	}
	// return 20 bytes regardless
    GEU_REG_READ(GEU_FUSE_VAL_ROOT_KEY1, pBuffer[0] );
	GEU_REG_READ(GEU_FUSE_VAL_ROOT_KEY2, pBuffer[1] );
	GEU_REG_READ(GEU_FUSE_VAL_ROOT_KEY3, pBuffer[2] );
	GEU_REG_READ(GEU_FUSE_VAL_ROOT_KEY4, pBuffer[3] );
	GEU_REG_READ(GEU_FUSE_VAL_ROOT_KEY5, pBuffer[4] );
    if (Size == K_SHA256_SIZE)//32
	{
		GEU_REG_READ(GEU_FUSE_VAL_ROOT_KEY6, pBuffer[5] );
		GEU_REG_READ(GEU_FUSE_VAL_ROOT_KEY7, pBuffer[6] );
		GEU_REG_READ(GEU_FUSE_VAL_ROOT_KEY8, pBuffer[7] );
	}

	return NoError;
};
/*******************************************************************************************/
#endif

// Get the VID and PID from fuses
UINT_T GetUSBIDFuseBits(unsigned short* VID, unsigned short* PID )
{
	return NotSupportedError;
}


/*******************************************************************************************/
/*                                                                                         */
/*                                                                                         */
/*   I2C Stuff                                                                             */
/*                                                                                         */
/*                                                                                         */
/*******************************************************************************************/
void PlatformI2CClocksEnable()
{
	unsigned long	temp;

	// the AP_I2C clock must be running:
	*(volatile unsigned long*)PMUM_ACGR |= PMUM_ACGR_AP_I2C;
	temp = *(volatile unsigned long*)PMUM_ACGR;			// read back to ensure write completes.

	// clock the twsi & pull it out of reset
	*(volatile unsigned long*)APBC_TWSI_CLK_RST=7;			// still in reset, but get clocks going.
	temp = *(volatile unsigned long*)APBC_TWSI_CLK_RST;		// read back to ensure write completes.
	*(volatile unsigned long*)APBC_TWSI_CLK_RST=3;			// out of reset, with clocks running.
	temp = *(volatile unsigned long*)APBC_TWSI_CLK_RST;		// read back to ensure write completes.

	// clock the pwr twsi & pull it out of reset
	*(volatile unsigned long*)APBC_PWRTWSI_CLK_RST=7;			// still in reset, but get clocks going.
	temp = *(volatile unsigned long*)APBC_PWRTWSI_CLK_RST;		// read back to ensure write completes.
	*(volatile unsigned long*)APBC_PWRTWSI_CLK_RST=3;			// out of reset, with clocks running.
	temp = *(volatile unsigned long*)APBC_PWRTWSI_CLK_RST;		// read back to ensure write completes.}

}

#if BOOTROM
void   PlatformShutdown()
{
	P_TBR_Env  pTBR;

	pTBR = GetTBREnvStruct();

	if (pTBR->Fuses.bits.PortEnabled)
	{
		ShutdownPorts();
	}

	// Disable Interrupts
	DisableIrqInterrupts();

	return;
}
#endif
