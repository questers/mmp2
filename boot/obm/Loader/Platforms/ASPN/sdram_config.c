/******************************************************************************
**  COPYRIGHT  2007 Marvell Inernational Ltd.
**  All Rights Reserved
******************************************************************************/


#include "predefines.h"
#include "mcu.h"
#include "mcu_extras.h"
#include "processor_enums.h"
#include "sdram_specs.h"
#include "sdram_support.h"
#include "sdram_registers_defaults.h"
#include "tim.h"
#include "Errors.h"
#include "timer.h"
#if BOOTROM
#include "BootROM.h"
#else 
#include "BootLoader.h"
#endif

// This routine will configure and enable SDRam DDR.
// It has the ability to select different DDR frequencies and geometries if the TIM area contains these specifiers.
// If no specifiers are found, then default values will be used.

// Overview of the SDRam DDR initialization:
//
// If TIM SDRam configuration records are available:
// 0. Make sure the required clocks are enabled. (requires TIM Clocks Enable records)
// 1. Make sure the memory voltage planes are up. (requires TIM Voltage records)
// 2. Make sure the clocks are at the correct frequency. (requires TIM Frequency records)
// 3. Program the MCU registers. (can override default values using optional TIM SDRam records
//
// otherwise (when no TIM SDRam configuration records are available)
// 1. Program the MCU registers using hardcoded default values.
//    - no clock enable, voltage or frequency changing occurs in this mode.
//


// FIXME: need to run the integrity check after programming the registers.
UINT_T ConfigureMemoryController( 
	void			*pTIM, 
	void			*pDDRScratchAreaAddress, 
	unsigned long	ulDDRScratchAreaLength
	)
{
	// variables for walking through the ddr records
	DDRCSpecList_T	*pDDRCStartAddress;
	DDRCSpecList_T	*pDDRCSpecs;
	DDRCSpec_T		*pDDRCSpec;
	CMCCSpecList_T	*pCMCCSpecs;
	CMCCSpec_T		*pCMCCSpec;
	int				nRecs;
	int				r;
	UINT_T			status;
	UINT_T			value;
	unsigned long	faultaddr;
	unsigned long	baseaddr = 0xffffffff;
	unsigned long 	readTarget;
	unsigned long	loop;
	unsigned long	retval;

	// some flags that enable / disable functionality within this routine.
	// these can be overriden by the TIM CMCC record.
	unsigned long	fDoSDRamInit = 0;				// usually only overridden for testing on first silicon.
    unsigned long	fDoMemoryCheck = 0;				// usually only overridden for testing on first silicon.
    unsigned long	fDoConsumeDDRPackages = 0;      // Default

	retval = DDR_NotInitializedError;   // Initialize the return value
    
	// first see if the DDR has already been configured
    // This is to fix an A0 bug which sets MCU_REG_MMU_MMAP0 CS valid bit W/O configuring DDR
    // So for A0, force DDR config
    #if ASPNB0 
    if( *MCU_REG_MMU_MMAP0 && 0x1 ) return NoError;
    #endif
    
	if( pTIM )
	{
		// Look for CMCC package in TIM
        #if ASPNB0
		pCMCCSpecs = (CMCCSpecList_T*)FindPackageInReserved(&status, pTIM, CMCCID);	// 'CMCC'
		if( status == NoError )
		{
			nRecs = ConfigRecordCount(pCMCCSpecs);
			pCMCCSpec=pCMCCSpecs->CMCCSpecs;
			for(r=0; r<nRecs; r++, pCMCCSpec++)
			{
				value = pCMCCSpec->KeyValue;
				switch(pCMCCSpec->KeyId)
				{
				// Update default flags. The updated values may or may not be used depending on whether a 
				// matching Consumer ID is found.
				case CMCC_CONFIG_ENA_ID: 	fDoSDRamInit	= value;	break;	// can enable / disable memory controller configuration
				case CMCC_MEMTEST_ENA_ID:	fDoMemoryCheck	= value;	break;	// can enable / disable memory test.
				case CMCC_CONSUMER_ID:
					{
					if (value == MY_CONSUMER_ID)
						{fDoConsumeDDRPackages = 1;}
					else
						{fDoConsumeDDRPackages = 0;}
					break;
					}
				default: break;
				}//EndSwitch
			}//EndFor
		}//End processing the list of CMCC records
        #else
        //force DDR init for ASPNA0
        fDoConsumeDDRPackages = 1;
        fDoSDRamInit = 1;
		#endif
		if (fDoConsumeDDRPackages && fDoSDRamInit)
		{
			pDDRCStartAddress = (DDRCSpecList_T*)FindPackageInReserved(&status, pTIM, DDRCID);
			if( status == NoError )
			{
				loop=0;		// loop counter.
				while(loop < 1000)
				{
					pDDRCSpecs = pDDRCStartAddress;
					nRecs = ConfigRecordCount(pDDRCSpecs);
					pDDRCSpec=pDDRCSpecs->DDRCSpecs;
					for(r=0; r<nRecs; r++, pDDRCSpec++)
					{
						value = pDDRCSpec->KeyValue;
						switch(pDDRCSpec->KeyId)
						{
							case ASPEN_SDRCFGREG0_ID: 	*MCU_REG_SDRAM_CONFIG_0  = value;	break;
							case ASPEN_SDRCFGREG1_ID: 	*MCU_REG_SDRAM_CONFIG_1  = value;	break;
							case ASPEN_SDRTMGREG1_ID: 	*MCU_REG_SDRAM_TIMING_1  = value;	break;
							case ASPEN_SDRTMGREG2_ID: 	*MCU_REG_SDRAM_TIMING_2  = value;	break;
							case ASPEN_SDRTMGREG3_ID: 	*MCU_REG_SDRAM_TIMING_3  = value;	break;
							case ASPEN_SDRTMGREG4_ID: 	*MCU_REG_SDRAM_TIMING_4  = value;	break;
							case ASPEN_SDRTMGREG5_ID: 	*MCU_REG_SDRAM_TIMING_5  = value;	break;
							case ASPEN_SDRTMGREG6_ID: 	*MCU_REG_SDRAM_TIMING_6  = value;	break;
							case ASPEN_SDRCTLREG1_ID: 	*MCU_REG_SDRAM_CONTROL_1  = value;	break;
							case ASPEN_SDRCTLREG2_ID: 	*MCU_REG_SDRAM_CONTROL_2  = value;	break;
							case ASPEN_SDRCTLREG3_ID: 	*MCU_REG_SDRAM_CONTROL_3  = value;	break;
							case ASPEN_SDRCTLREG4_ID: 	*MCU_REG_SDRAM_CONTROL_4  = value;	break;
							case ASPEN_SDRCTLREG5_ID: 	*MCU_REG_SDRAM_CONTROL_5  = value;	break;
							case ASPEN_SDRCTLREG6_ID: 	*MCU_REG_SDRAM_CONTROL_6  = value;	break;
							case ASPEN_SDRCTLREG7_ID: 	*MCU_REG_SDRAM_CONTROL_7  = value;	break;
							case ASPEN_SDRCTLREG13_ID: 	*MCU_REG_SDRAM_CONTROL_13  = value;	break;
							case ASPEN_SDRCTLREG14_ID: 	*MCU_REG_SDRAM_CONTROL_14  = value;	break;
							case ASPEN_SDRERRREG_ID:	*MCU_REG_ERROR_STATUS  = value;	break;
							case ASPEN_ADRMAPREG0_ID:	*MCU_REG_MMU_MMAP0 = value; break;
							case ASPEN_ADRMAPREG1_ID:	*MCU_REG_MMU_MMAP1 = value; break;
							case ASPEN_USRCMDREG0_ID:	*MCU_REG_USER_INITIATED_COMMAND = value; break;
							case ASPEN_SDRSTAREG_ID:	*MCU_REG_DRAM_STATUS = value; break;
							case ASPEN_PHYCTLREG3_ID:	*MCU_REG_PHY_CONTROL_3 = value; break;
							case ASPEN_PHYCTLREG7_ID:	*MCU_REG_PHY_CONTROL_7 = value; break;
							case ASPEN_PHYCTLREG8_ID:	*MCU_REG_PHY_CONTROL_8 = value; break;
							case ASPEN_PHYCTLREG9_ID:	*MCU_REG_PHY_CONTROL_9 = value; break;
							case ASPEN_PHYCTLREG10_ID:	*MCU_REG_PHY_CONTROL_10 = value; break;
							case ASPEN_PHYCTLREG11_ID:	*MCU_REG_PHY_CONTROL_11 = value; break;
							case ASPEN_PHYCTLREG12_ID:	*MCU_REG_PHY_CONTROL_12 = value; break;
							case ASPEN_PHYCTLREG13_ID:	*MCU_REG_PHY_CONTROL_13 = value; break;
							case ASPEN_PHYCTLREG14_ID:	*MCU_REG_PHY_CONTROL_14 = value; break;
							case ASPEN_DLLCTLREG1_ID:	*MCU_REG_PHY_DLL_CONTROL_1 = value; break;
							case ASPEN_PHYCTLREGTST_ID:	*MCU_REG_PHY_CONTROL_TEST = value; break;
							case ASPEN_TSTMODREG0_ID:	*MCU_REG_TEST_MODE_0 = value; break;
							case ASPEN_TSTMODREG1_ID:	*MCU_REG_TEST_MODE_1 = value; break;
							case ASPEN_MCBCTLREG1_ID:	*MCU_REG_MCB_CONTROL_1 = value; break;
							case ASPEN_MCBCTLREG2_ID:	*MCU_REG_MCB_CONTROL_2 = value; break;
							case ASPEN_MCBCTLREG3_ID:	*MCU_REG_MCB_CONTROL_3 = value; break;
							case ASPEN_MCBCTLREG4_ID:	*MCU_REG_MCB_CONTROL_4 = value; break;
							case ASPEN_PRFCTLREG0_ID:	*MCU_REG_PERF_COUNT_CONTROL_0 = value; break;
							case ASPEN_PRFCTLREG1_ID:	*MCU_REG_PERF_COUNT_CONTROL_1 = value; break;
							case ASPEN_PRFSTAREG_ID:	*MCU_REG_PERF_COUNT_STAT = value; break;
							case ASPEN_PRFSELREG_ID:	*MCU_REG_PERF_COUNT_SEL = value; break;
							case ASPEN_OPDELAY_ID:		Delay(value); break;//TBD
							case ASPEN_OPREAD_ID:		readTarget = *(unsigned long*) value; break;//TBD
							default: break;
						}//EndSwitch
					}//EndFor - Done processing DDRC package
				
					// wait for DDR Initialization to complete:
					while( ((*MCU_REG_DRAM_STATUS) & MCU_DRAM_STATUS_INIT_DONE ) == 0 )
					{
						// wait for hardware to assert the INIT_DONE bit
					}

					baseaddr = ( *MCU_REG_MMU_MMAP0	) & 0xff800000;

					// finally, do some dummy reads in (and discard the values)
					r = *(unsigned volatile long*)baseaddr;
					r = *(unsigned volatile long*)baseaddr;
					r = *(unsigned volatile long*)baseaddr;
					r = *(unsigned volatile long*)baseaddr;
					r = *(unsigned volatile long*)baseaddr;

					// this is the offical end of the MCU initialization.

					if( !fDoMemoryCheck ) 
					{
						retval = NoError; // Memory test disabled.
						break;		
					}

					// now do an integrity check to ensure the memory is reliable
					// loop, and the save location for loop, is for debug use, to see how many times
					// the mcu must be init before working OK.
					status = 0;
					status = CheckMemoryReliability( (unsigned long)pDDRScratchAreaAddress, ulDDRScratchAreaLength, &faultaddr);		// start, length, ptr to ulong for failing address
					if( status == 0 ) 
					{
						retval = NoError; // Memory test OK
						break;		
					}
					// an error occurred at the address contained in 'value'.
					// does memory need to be reinitialized?
					// or maybe try again.
					//   for now, see if just trying again will do the trick.
					// stay in this init / validate loop until the memory check returns good results.

					loop++; // keep track of how many times through.	
								
				}//EndWhile
			}//EndIf Found DDRC
            
            if(status != NoError) // Could be DDR package not found with fDoSDRamInit=1 OR MemoryTest failed.
                return DDR_NotInitializedError;
		}//EndIf Consume DDR Init package
        else
            return DDR_InitDisabled;
	}//EndIf non-null pTIM
	return retval;
	
}

UINT_T GetDDRSize()
{
    // This supports CS0 only
	UINT_T size;
	unsigned volatile long * pReg;
	UINT_T bfs;		// lsb location of bit field
	UINT_T bfl;		// number of bits in the fields
	// Read Memory Address Map Register 0 - CS0 (offset 0x0100)
    // (MEMORY_ADDRESS_MAP)
    pReg = MCU_REG_MMU_MMAP0;
	bfs = 16;
	bfl = 4;
	size = ReadBitField(pReg, bfs, bfl);
    switch(size)
	{
		case(0xF): {size = 0x80000000; break;} //(UINT)(2048*(1024*1024))
		case(0xE): {size = 1024*(1024*1024); break;}
		case(0xD): {size =  512*(1024*1024); break;}
		case(0xC): {size =  256*(1024*1024); break;}
		case(0xB): {size =  128*(1024*1024); break;}
		case(0xA): {size =   64*(1024*1024); break;}
		case(0x9): {size =   32*(1024*1024); break;}
		case(0x8): {size =   16*(1024*1024); break;}
		case(0x7): {size =    8*(1024*1024); break;}
		default:   {size =    0; break;}
	}
    return size;
}

UINT_T PlatformSpecificPostDDRConfig(pTIM pTIM_h, pFUSE_SET pFuses)
{
	// Platform Specific stub for DDR init post processing
	UINT_T retval = NoError;
 	return retval;
}
