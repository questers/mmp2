/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document.
**  Except as permitted by such license, no part of this document may be
**  reproduced, stored in a retrieval system, or transmitted in any form or by
**  any means without the express written consent of Intel Corporation.
**
**  FILENAME:	Interrupts.c
**
**  PURPOSE: 	Contain Interrupts/Exception handlers for Bulverde B0 Boot ROM
**
******************************************************************************/

//#if (BOOTROM || TRUSTED)
// #include "dsvl.h"
//#endif

#include "predefines.h"
#include "Interrupts.h"
#include "PlatformConfig.h"
#include "ProtocolManager.h"
#include "UartDownload.h"
#include "sdmmc_api.h"

static UINT8_T UdcDmaUsageFlag   = FALSE;  // Used to enable/disable DMA usage
static UINT8_T PortInterrupt	 = FALSE;

UINT8_T GetPortInterruptFlag(void){ return PortInterrupt;}

void SetPortInterruptFlag(void){ PortInterrupt = TRUE;}

void ClearPortInterruptFlag(void){
	PortInterrupt = FALSE;
	return;
}

void IRQ_Glb_Ena()
{
	BU_U32	reg;

    {
    	reg = BU_REG_READ(ICU_AP_GBL_IRQ_MSK);
    	reg &= (~ICU_AP_GBL_IRQ_MSK_IRQ_MSK);
		BU_REG_WRITE( ICU_AP_GBL_IRQ_MSK, reg);
	}

}


void EnableInt(unsigned int int_num)
{

//    *(volatile unsigned int *)0x90008008 = (0x1 << intMap);
    BU_REG_WRITE(ICU_INT_0_TO_63_CONF+int_num*4,
    				ICU_INT_0_TO_63_CONF_AP_INT | ( IRQ_INT<<4) | BROM_PRIORITY );


}
void DisableInt(BU_U32 int_num)
{
//    *(volatile unsigned int *)0x9000800c = (0x1 << intMap);
   	BU_REG_WRITE(ICU_INT_0_TO_63_CONF+int_num*4, 0 );

}

UINT_T EnablePeripheralIRQInterrupt(unsigned int InterruptID)
{
    EnableInt(InterruptID);
	return(NoError);
}

UINT_T DisablePeripheralIRQInterrupt(unsigned int InterruptID)
{
    DisableInt(InterruptID);
	return(NoError);
}

unsigned int GetIrqStatus(void)
{
	UINT_T Regval = 0;
	Regval = BU_REG_READ(ICU_AP_IRQ_SEL_INT_NUM);
    return Regval;
}

void IrqHandler(void)
{
    volatile unsigned int int_num;
	void *handle;

    int_num = GetIrqStatus();
    if ( int_num &  ICU_AP_IRQ_SEL_INT_NUM_INT_PENDING )
    {
 		/* mask out the pending bit to get low 5 bit INT num */
		int_num &= ~ICU_AP_IRQ_SEL_INT_NUM_INT_PENDING;

		switch (int_num)
		{
			case INT_UART2_Fast:
			{
				PortInterrupt = TRUE;
				FFUARTInterruptHandler();
				break;
			}
#if USBCI
			case INT_USB:
			{
				PortInterrupt = TRUE;
	            PlatformCI2InterruptHandler();
				break;
			}
#endif
#if MMC_CODE
			case INT_SDHC0:
			case INT_SDHC1:
			{
				SDMMC_ISR();
				break;
			}
#endif
		}
    }

}


void UndefinedHandler(void)
{
	// Terminate, simply loop forever for now
	while (1){}
}

void SwiHandler(void)
{
	// Terminate, simply loop forever for now
	while (1){}
}

void PrefetchHandler(void)
{
	// Terminate, simply loop forever for now
	while (1){}
}

void AbortHandler(void)
{
	// Terminate, simply loop forever for now
	while (1){}
}


void FiqHandler(void)
{
   while (1){}
   //return;
}


void INT_init()
{
	int i;

   	/* disable global IRQ+FIR */

		BU_REG_WRITE(ICU_AP_GBL_IRQ_MSK, ICU_AP_GBL_IRQ_MSK_IRQ_MSK | ICU_AP_GBL_IRQ_MSK_FIQ_MSK);

	/* clean up 32 interrupt config register */
	for ( i=0; i<INT_NUMS; i++ )
	{
		BU_REG_WRITE(ICU_INT_0_TO_63_CONF+i*4,
		             (BU_REG_READ(ICU_INT_0_TO_63_CONF+i*4)
		              & ICU_INT_0_TO_63_CONF_AP_INT));
	}

}

