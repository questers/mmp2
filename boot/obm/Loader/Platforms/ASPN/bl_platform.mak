# (C) Copyright 2007 Marvell International Ltd.  
#  All Rights Reserved
#
##############################################################
#
#  Platform Specific macros and options	- TTC
#
###########################################################

################### FLAGS START #############################

################### Global Overrides ########################
SDHC_CNTL = 1				# Old MMC controller = 1, the new one starting with MMP2 A0 = 2
SPI_PIO = 1					#DMA is the default when this flag

################### Platform Specific ########################
BOARD = ASPENITE
ASPNB0 = 1
# Fix me. I exist in global Makefile too.
ASPN_MLC_BCH = 0
SLC_NONFI = 0
USBCI = 1
CI2_USB_DDR_BUF = 1
################### FLAGS END ################################

# the .bld file makes use of these rom addresses...
ROMCODESTART = 0xd102ba00
ROMDATASTART = 0xd1023000

TECHNOLOGY = TTC

##############################################################
#  Libraries to Link for ASPN
##############################################################

LIBLIST = \
#!if $(USE_IPPCP)
#	"$(BASE)\SecureBoot\ippcp\IppCP_$(ToolChain)_$(InstructionType).a"	\
#!endif
!if $(USBCI)
	"$(BASE)\Download\USB2CI\USB2CI_$(ToolChain)_$(InstructionType).a" \
!endif

#End of LIBLIST build

##############################################################
# Compile flag Macros
##############################################################

!if $(ASPNB0)
PLAT_AFLAGS = -PD "ASPNB0 SETA 1"
!else
PLAT_AFLAGS = -PD "ASPNA0 SETA 1"
!endif

# create a define to identify the board.
BOARDDEF =
!if ("$(BOARD)" == "ZYLONITE2" )
BOARDDEF = -DZYLONITE2=1
!endif
!if ("$(BOARD)" == "ASPENITE" )
BOARDDEF = -DASPENITE=1
!endif
!if ("$(BOARD)" == "WAYLAND" )
BOARDDEF = -DWAYLAND=1
!endif
!if ("$(BOARD)" == "AVLITE" )
BOARDDEF = -DAVLITE=1
!endif

##############################################################
# Tool Specific Compile flags
##############################################################

!if $(RVCT_BUILD)	###### RVCT ######

!if $(RELEASE)
#release flags
OPTIMIZE_LVL = -O3 -Ospace
!else
#debug flags
OPTIMIZE_LVL = -O1 --debug
!endif

A_FLAGS = $(A_FLAGS) $(PLAT_AFLAGS) --diag_suppress=A3910W -g --pd "RVCT SETA 1"
CC_FLAGS = -W -DRVCT --library_interface=aeabi_clib	$(OPTIMIZE_LVL) $(BOARDDEF) $(CC_FLAGS) -DASPNB0=$(ASPNB0)
LD_FLAGS = --first __main --entry __main --diag_suppress=L6238 --map \
		   --list "$(TARGETNAME).map" \
		   --output "$(TARGETNAME).elf" \
		   --autoat --callgraph --symbols --list_mapping_symbols \
		   --info sizes,summarysizes,totals,unused \
		   --ro-base $(ROMCODESTART) --rw-base $(ROMDATASTART) \
		   --userlibpath "$(SDT)\marvellpxa\lib","$(BASE)\SecureBoot\FuseBlock"

ELF2BIN_LINE = $(elf2bin) --bin -o "$(TARGETNAME).bin" "$(TARGETNAME).elf"



!else  				###### SDT ######

!if $(RELEASE)
#release flags
OPTIMIZE_LVL = -O1
!else
#debug flags
OPTIMIZE_LVL = -Od -Zi
!endif

CC_FLAGS = -W2 -nologo -Gy -use_msasm $(OPTIMIZE_LVL) $(BOARDDEF) $(CC_FLAGS)
A_FLAGS = $(A_FLAGS) $(PLAT_AFLAGS) -g -arm -PD "SDT SETA 1"
LD_FLAGS = -remove -noinfo -debug -Init DATA \
			-output "$(TARGETNAME).elf"  \
			-listing="$(TARGETNAME).map" \
			-bm ROMCODESTART=$(ROMCODESTART) -bm ROMDATASTART=$(ROMDATASTART) \
			-bf="$(BASE)\Build\BootLoader.bld" \
			"$(SDT)\marvellpxa\lib\x0__ac30.a"

ELF2BIN_LINE = $(elf2bin) -map CODE=0x0 -keeponly CODE "$(TARGETNAME).elf" -bin "$(TARGETNAME).bin"

!endif ####################### End tool type.

#############################################################
#
# Platform Object Definitions
#
#############################################################

SDHCOBJS = 	$(OUTDIR)\sdhc1_controller.o \
			$(OUTDIR)\sdhc1.o

SDMMCOBJS = $(OUTDIR)\sdmmc_api.o

ONENANDOBJS = $(OUTDIR)\FlexOneNAND_Cmd.o \
              $(OUTDIR)\OneNAND_Cmd.o \
              $(OUTDIR)\OneNAND_Driver.o

DFCOBJS =  	$(OUTDIR)\xllp_dfc.o \
           	$(OUTDIR)\xllp_dfc_support.o

NANDOBJS = $(OUTDIR)\nand.o

FLASHAPIOBJS = 	$(OUTDIR)\flash.o \
				$(OUTDIR)\fm.o

XIPOBJS = $(OUTDIR)\xip.o

!if $(USBCI)
DOWNLOADOBJS = $(OUTDIR)\ProtocolManager.o \
      	$(OUTDIR)\UartDownload.o \
      	$(OUTDIR)\uart.o \
      	$(OUTDIR)\usb_descriptors.o \
      	$(OUTDIR)\CI2Download.o \
        $(OUTDIR)\CI2Driver.o
!else
DOWNLOADOBJS = $(OUTDIR)\ProtocolManager.o \
      	$(OUTDIR)\UartDownload.o \
      	$(OUTDIR)\uart.o \
      	$(OUTDIR)\usb_descriptors.o
!endif

MAINOBJS = \
	  $(OUTDIR)\OBM_3_1.o

TIMOBJS =  $(OUTDIR)\tim.o

MISCOBJS = $(OUTDIR)\misc.o \
		$(OUTDIR)\keypad.o \
		$(OUTDIR)\xllp_clkmgr.o \
		$(OUTDIR)\timer.o \
!if $(NEW_DDRCNFG)
        $(OUTDIR)\RegInstructions.o \
!endif

I2COBJS = $(OUTDIR)\I2C.o

SDRAMOBJS = $(OUTDIR)\sdram_support.o \
            $(OUTDIR)\DDR_Cfg.o

DMAOBJS = $(OUTDIR)\dma.o


PLATFORMOBJS = $(OUTDIR)\PlatformConfig.o	\
            $(OUTDIR)\sdram_config.o \
            $(OUTDIR)\freq_config.o \
            $(OUTDIR)\platform_interrupts.o

ASSEMBLYOBJS = $(OUTDIR)\bl_StartUp_$(TECHNOLOGY).o \
	  $(OUTDIR)\platform_arch.o

SPIOBJS = $(OUTDIR)\SPIFlash.o


OBJ = $(ASSEMBLYOBJS)\
      $(FLASHAPIOBJS) \
!if $(NOR)
      $(XIPOBJS) \
!endif
!if $(NAND)
      $(NANDOBJS) \
      $(DFCOBJS) \
!endif
!if $(I2C)
	  $(I2COBJS) \
!endif
	  $(MISCOBJS) \
	  $(DMAOBJS) \
	  $(SDRAMOBJS) \
	  $(TIMOBJS) \
	  $(MAINOBJS) \
	  $(DOWNLOADOBJS) \
	  $(PLATFORMOBJS) \
!if $(MMC)
	  $(SDMMCOBJS) \
	  $(SDHCOBJS) \
!endif
!if $(ONENAND)
	  $(ONENANDOBJS) \
!endif
!if $(SPI)
	  $(SPIOBJS) \
!endif



