/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 *
 *  FILENAME:	JTAGEnable.c
 *
 *  PURPOSE: 	Main Jtag ReEnabling program.  Handles all aspects of the jtag
 * 				re-enabling process
 *                  
******************************************************************************/
#include "Errors.h"
#include "TIM.h"

UINT_T JTAGEnable(pTIM pTIM_h)
{
	return NoError;
}

