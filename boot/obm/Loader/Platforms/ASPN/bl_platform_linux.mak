# (C) Copyright 2007 Marvell International Ltd.  
#  All Rights Reserved
#
##############################################################
#
#  Platform Specific macros and options	- TTC
#
###########################################################

#############################################################
#
# Use these line to override global definition
#
#############################################################

####################################################

BOARD = ASPENITE
TECHNOLOGY = TTC

# follow processor flags are mutually exclusive
# set to 1 if build for ASPN B0 processor
ASPNB0 = 1

# Crypto and fuse programming module selection
BL_USE_IPPCP_CRYPTO=0
BL_USE_GEU_FUSE_PROG=0

MMC_SDMA_MODE = 0
MMC_FAST_INIT_FREQ = 0

DSPI_PIO=1

# Old MMC controller = 1, the new one starting with MMP2 A0 = 2
SDHC_CNTL = 1

# New DDR Configuration
NEW_DDRCNFG = 0

# Must be set when using DKB to program on ASPNB0 BOOTROM with too few
# FLASH TYPES and using MLC Device in BCH mode.
ASPN_MLC_BCH = 0

##############################################################
#
#  Library Selection
#
##############################################################
ifeq "$(BL_USE_GEU_FUSE_PROG)" "1"
LIBS += $(TOPDIR)/SecureBoot/GEU/GEULib_GNU_$(InstructionType)_TVTD.a
endif

ifeq "$(USBCI)" "1"
LIBS += "$(TOPDIR)/Download/USB2CI/USB2CI_$(ToolChain)_$(InstructionType).a"
endif

LDCMD = -T $(TOPDIR)/Platforms/$(PLATFORM)/startup.lds

##############################################################
#
#  Configure Board Name Macro
#
##############################################################
ifeq "$(BOARD)" "ASPENITE"
BOARD_DEFINES = -DASPENITE=1 -DASPNB0=$(ASPNB0) -DASPN=1
endif

ifeq "$(BOARD)" "WAYLAND"
BOARD_DEFINES = -DWAYLAND=1 -DASPNB0=$(ASPNB0) -DASPN=1
endif

ifeq "$(BOARD)" "AVLITE"
BOARD_DEFINES = -DAVLITE=1 -DASPNB0=$(ASPNB0) -DASPN=1
endif

PLATFORMCFLAGS += $(BOARD_DEFINES)

PLATFORMASMFLAGS += -defsym ASPN=1

ifeq "$(ASPNB0)" "1"
PLATFORMASMFLAGS += -defsym ASPNB0=1
endif


#############################################################
#
# Platform Object Definitions
#
#############################################################
#MMC
SDHCOBJS =  sdhc1_controller.o \
            sdhc1.o

SDMMCOBJS = sdmmc_api.o \

#ONENAND
ONENANDOBJS = FlexOneNAND_Cmd.o \
              OneNAND_Cmd.o \
              OneNAND_Driver.o

NANDOBJS =  xllp_dfc.o \
            xllp_dfc_support.o \
            nand.o

DMAOBJS = 	dma.o

FLASHAPIOBJS = 	Flash.o FM.o

XIPOBJS = xip.o

SPIOBJS = SPIFlash.o

DOWNLOADOBJS = ProtocolManager.o \
               UartDownload.o \
               uart.o \
               usb_descriptors.o \
               CI2Download.o \
               CI2Driver.o

MAINOBJS = OBM_3_1.o

TIMOBJS =  tim.o

MISCOBJS = misc.o \
           keypad.o \
           xllp_clkmgr.o \
           Timer.o

I2COBJS = I2C.o

SDRAMOBJS = sdram_support.o \
            DDR_Cfg.o \

ifeq "$(NEW_DDRCNFG)" "1"
MISCOBJS += RegInstructions.o
endif

ifeq "$(TRUSTED)" "1"
SECUREOBJS = bl_security.o \
             bl_security_if.o \
             bl_provisioning_if.o
ifeq "$(BL_USE_IPPCP_CRYPTO)" "1"
SECUREOBJS += ippcp_security.o
endif

ifeq "$(BL_USE_GEU_FUSE_PROG)" "1"
SECUREOBJS += geu_provisioning_impl.o
endif
else
# -- non-trusted
ifeq "$(TVTD_NTCFG)" "1"
SECUREOBJS = $(OUTDIR)\geu_provisioning_impl.o
endif
endif

PLATFORMOBJS = PlatformConfig.o \
               sdram_config.o \
               freq_config.o \
               platform_interrupts.o

ASSEMBLYOBJS = bl_StartUp_ttc_linux.o \
               platform_arch_linux.o \
               topofcode_linux.o

#####################################################
################ OBJECT LIST ####################
#
# Note: The list below is where to capture what files
#		are to be built into a particular build
#
#####################################################
OBJS = \
      $(ASSEMBLYOBJS)\
      $(SECUREOBJS) \
      $(FLASHAPIOBJS) \
      $(DMAOBJS) \
      $(MISCOBJS) \
      $(TIMOBJS) \
      $(SDRAMOBJS) \
      $(MAINOBJS) \
      $(DOWNLOADOBJS) \
      $(PLATFORMOBJS) \

ifeq  "$(NOR)" "1"
OBJS += $(XIPOBJS)
endif

ifeq "$(NAND)" "1"
OBJS += $(NANDOBJS)
endif

ifeq "$(I2C)" "1"
OBJS += $(I2COBJS)
endif

ifeq "$(ONENAND)" "1"
OBJS += $(ONENANDOBJS)
endif

ifeq "$(SPI)" "1"
OBJS += $(SPIOBJS)
endif

ifeq "$(MMC)" "1"
OBJS += $(SDMMCOBJS) $(SDHCOBJS)
endif




