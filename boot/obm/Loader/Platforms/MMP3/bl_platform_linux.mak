# (C) Copyright 2007 Marvell International Ltd.  
#  All Rights Reserved
#
##############################################################
#
#  Platform Specific macros and options	- TTC
#
###########################################################

#############################################################
#
# Use these line to override global definition
#
#############################################################

BOARD = JASPER
TECHNOLOGY = TTC

DDRBASE = 1
ISRAMBASE = 0

ifeq "$(DDRBASE)" "1"
# the .bld file makes use of these rom addresses...
# make sure platform_defs.inc stack info is compatbile with this
# memory map:
# use ddr from 00000000 to 00020000
# code  00000000 - 00016000
# data  00016000 - 00016800
# bss   00016800 - 00019e00
#       00019e00 - 0001d800  3a00 available
# stack 0001d800 - 00020000  2800 combined stacks size
ROMCODESTART = 0x00000000
ROMDATASTART = 0x00016000
# loadoffsets.inc test this variable.
PLATFORMASMFLAGS = -defsym DDRBASE=1 -defsym ISRAMBASE=0
endif

ifeq "$(ISRAMBASE)" "1"
# the .bld file makes use of these rom addresses...
# make sure platform_defs.inc stack info is compatbile with this
# memory map:
# use isram from d1000000 to d1020000
# code  d1000000 - d1016000
# data  d1016000 - d1016800
# bss   d1016800 - d1019e00
#       d1019e00 - d101d800  3a00 available
# stack d101d800 - d1020000  2800 combined stacks size
ROMCODESTART = 0xd1000000
ROMDATASTART = 0xd1016000
# loadoffsets.inc test this variable.
PLATFORMASMFLAGS = -defsym DDRBASE=0 -defsym ISRAMBASE=1
endif

# Crypto and fuse programming module selection
BL_USE_WTM_CRYPTO = 1
BL_USE_WTM_FUSE_PROG = 1

MMC_SDMA_MODE = 0
SDHC_CNTL = 2				# Old MMC controller = 1, the new one starting with MMP2 A0 = 2
MMC_FAST_INIT_FREQ = 0

# New DDR Configuration
NEW_DDRCNFG = 1

##############################################################
#
#  Library Selection 
#  	- selected by RVCT_BUILD define, and ARM_MODE possibly 
#
##############################################################
LIBS +=	"$(TOPDIR)/SecureBoot/FuseBlock/FuseLib_SDT_ARM_$(PLATFORM).a"
ifeq "$(USBCI)" "1"
LIBS +=	"$(TOPDIR)/Download/USB2CI/USB2CI_$(ToolChain)_$(InstructionType).a"
endif

LDCMD = -T $(TOPDIR)/Platforms/$(PLATFORM)/startup.lds

##############################################################
#
#  Configure Board Name Macro 
#
##############################################################
ifeq "$(BOARD)" "Jasper"
BOARDDEF = -DJASPER=1
endif
ifeq "$(BOARD)" "EVBII"
BOARDDEF = -DEVBII=1
endif
ifeq "$(BOARD)" "WAYLAND"
BOARDDEF = -DWAYLAND=1
endif

#############################################################
#
# Platform Object Definitions
#
#############################################################
#MMC
SDHCOBJS = 	sdhc2_controller.o \
			sdhc2.o
SDMMCOBJS = sdmmc_api.o

#ONENAND
ONENANDOBJS = FlexOneNAND_Cmd.o \
              OneNAND_Cmd.o \
              OneNAND_Driver.o

NANDOBJS = 	xllp_dfc.o \
           	xllp_dfc_support.o \
			nand.o

DMAOBJS = 	dma.o

FLASHAPIOBJS = 	Flash.o FM.o

XIPOBJS = xip.o

SPIOBJS = SPIFlash.o

DOWNLOADOBJS = ProtocolManager.o \
      	UartDownload.o \
      	uart.o \
      	usb_descriptors.o \
        CI2Download.o \
        CI2Driver.o

MAINOBJS = OBM_3_1.o

TIMOBJS =  tim.o

MISCOBJS = misc.o \
		keypad.o \
		xllp_clkmgr.o \
		ipc.o \
		Timer.o
ifeq "$(NEW_DDRCNFG)" "1"
MISCOBJS += RegInstructions.o
endif       

I2COBJS = I2C.o

SDRAMOBJS = sdram_support.o \
            DDR_Cfg.o

ifeq "$(TRUSTED)" "1"
SECUREOBJS = bl_security.o \
		 bl_security_if.o \
		 bl_provisioning_if.o \
		 PlatformJtag.o	

ifeq "$(BL_USE_IPPCP_CRYPTO)" "1"
SECUREOBJS += ippcp_security.o 
endif
ifeq "$(BL_USE_GEU_FUSE_PROG)" "1"
SECUREOBJS += geu_provisioning_impl.o 
endif
ifeq "$(BL_USE_WTM_CRYPTO)" "1"
SECUREOBJS += linked_dtd_list.o wtm_mbox_impl.o 
endif

endif

PLATFORMOBJS = PlatformConfig.o	\
           sdram_config.o \
           freq_config.o \
           platform_interrupts.o

ASSEMBLYOBJS = bl_StartUp_ttc_linux.o \
	           platform_arch_linux.o \
		   topofcode_linux.o

#####################################################
################ OBJECT LIST ####################
#
# Note: The list below is where to capture what files
#		are to be built into a particular build
#		
#####################################################
OBJS = $(ASSEMBLYOBJS)\
	$(MISCOBJS) \
	$(FLASHAPIOBJS) \
	$(DMAOBJS) \
	$(MAINOBJS) \
	$(PLATFORMOBJS) \
	$(DOWNLOADOBJS) \
	$(SDRAMOBJS) \
	$(TIMOBJS) 

ifeq "$(TRUSTED)" "1"
OBJS += $(SECUREOBJS) 
endif

ifeq "$(NOR)" "1"
OBJS += $(XIPOBJS) 
endif

ifeq "$(NAND)" "1"
OBJS += $(NANDOBJS)
endif
ifeq "$(I2C)" "1"
OBJS += $(I2COBJS)
endif

ifeq "$(SPI)" "1"
OBJS += $(SPIOBJS)
endif

ifeq "$(ONENAND)" "1"
OBJS += $(ONENANDOBJS)
endif

ifeq "$(MMC)" "1"
OBJS += $(SDMMCOBJS) $(SDHCOBJS)
endif



