/******************************************************************************
**  COPYRIGHT  2007 Marvell Inernational Ltd.
**  All Rights Reserved
******************************************************************************/

/*
Sample extended reserved area to specify operating mode 3, 800_400, is below:
*
      Extended Reserved Data:
      Processor Type: PXA168
      Frequency:
      ASPEN_FREQ_ID_MCU: 3
      End Extended Reserved Data:
*
Operating mode specifiers follow the "ASPEN_FREQ_ID_MCU:" KeyId
Valid values to follow ASPEN_FREQ_ID_MCU are:
 0 for 156 MHz
 1 for 400 MHz
 2 for 624 MHz
 3 for 800 MHz
 4 for 1066 MHz
 5 for 1200 MHz
 6 for 1600 MHz
*/


#include "predefines.h"
#include "MCU.h"
#include "mcu_extras.h"
#include "processor_enums.h"
#include "sdram_specs.h"
#include "sdram_support.h"
#include "sdram_registers_defaults.h"
#include "tim.h"
#include "Errors.h"

#include "PMUM.h"
// missing from PMUM.h:
#define	PMUM_PLL2REG1	(PMUM_BASE+0x0060)	/* 32 bit	PLL2 Register 1: KVCO, etc. */
#define	PMUM_PLL2REG2	(PMUM_BASE+0x0064)	/* 32 bit	PLL2 Register 2: DIFFCLK, etc */

#include "PMUA.h"


// FIXME: move these to an include file....

// Operating Mode Specifier
typedef struct OPMDSpec_S
{
	unsigned long	KeyId;
	unsigned long	KeyValue;
} OPMDSpec_T;

typedef struct OPMDSpecList_S
{
	unsigned long	PID;			// package id: 'OPMD' 0x4f504d44
	int				NumBytes;
	OPMDSpec_T		OPMDSpecs[];
} OPMDSpecList_T;

// FIXME: allow voltage, pll2reg1 and pll2reg2 to be specified.
// need KeyID enums:
//	'voltage': mV
//  pll2reg1:  32 bit value
//  pll2reg2:  32 bit value
//  opmode:    0 - 6




// forward decs...defined in this file.
void SetVoltage(unsigned long mV);
void SetOpMode( unsigned long om);
void DelayEx(unsigned long d);




// This routine will set different operating modes.
// It has the ability to select different operating modes dependent on TIM records.
// Note some operating modes require higher voltage. this is a silicon bug still under investigation.

unsigned long
ConfigureOperatingMode( void *pTIM )
{

	return 0;
}

// This routine is based on an XDB script used to set the voltage.
// FIXME: add the I2C driver back in and make use of it.
void SetVoltage(unsigned long mV)
{

}



// assume starting in PLL1.
// if using PLL2, must switch from PLL2 to PLL1 before calling this routine.
// FIXME: add delays where necessary
// FIXME: replace hard coded constants with meaningful names & values.

#define PACE	1000

void SetOpMode( unsigned long om )
{

}

void DelayEx( unsigned long d )
{
	volatile unsigned long l = d;
	while(l) --l;
}

