/******************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 *
 *  FILENAME:	JTAGEnable.c
 *
 *  PURPOSE: 	Main Jtag ReEnabling program.  Handles all aspects of the jtag
 * 				re-enabling process
 *                  
******************************************************************************/
#include "Errors.h"
#include "Typedef.h"
#include "FuseInterface.h"
#include "wtm_tbr_lib.h"
#include "wtm.h"
#include "security.h"
#include "PlatformJtag.h"
#include "Jtag.h"
#include "predefines.h"
#include "platform_interrupts.h"
#if BOOTROM
#include "BootROM.h"
#endif

#if JTAG_PROTOCOL_DEBUG
#include "UartDownload.h"
#endif

extern SECURITY_FUNCTIONS SFs;
extern UINT_T g_JTAG_UseInterrupts;

#if JTAG_PROTOCOL_OVER_JTAG_SUPPORTED

void JTAG_Enable_DebugKey_Interrupts()
{
	UINT_T reg;
	// Enable JTAG DebugKey Interrupts to WTM
	// Clear bit 14 in the WTM ADDRESS_DECODER_INTERRUPT_MASK register
	reg = BU_REG_READ(ADDRESS_DECODER_INTERRUPT_MASK);
	reg = reg & ~(BIT_14);
	BU_REG_WRITE(ADDRESS_DECODER_INTERRUPT_MASK, reg);

	// Enable JTAG DebugKey WTM interrupts to SP
	// Clear bit 14 in the WTM SP_INTERRUPT_MASK register
	reg = BU_REG_READ(SP_INTERRUPT_MASK);
	reg = reg & ~(BIT_0);
	BU_REG_WRITE(SP_INTERRUPT_MASK, reg);

	EnableInt(INT_JTAG);

	return;
}

void JTAG_Disable_DebugKey_Interrupts()
{
   	DisablePeripheralIRQInterrupt(INT_JTAG);
	return;
}

void JTAG_Clear_DebugKey_InterruptRequest()
{
	UINT_T reg;
	// To clear the JTAG DebugKey interrupt
	// Write 1 to bit 14 of the  ADDRESS_DECODER_INTERRUPT register
   	reg = BU_REG_READ(ADDRESS_DECODER_INTERRUPT);
	reg = reg | (BIT_14); 
	BU_REG_WRITE(ADDRESS_DECODER_INTERRUPT, reg);	
	
	reg = BU_REG_READ(SP_INTERRUPT_RESET_REGISTER);
	reg = reg | (BIT_0);
	BU_REG_WRITE(SP_INTERRUPT_RESET_REGISTER, reg); 			
	return;
}

UINT_T JTAG_Initialize_DebugKey_Access()
{
	// TBD - Enable WTM clocks
	// Be careful, they get enabled in SetupEnvironment() at the beginning of BootRomMain
	return NoError;
}

UINT_T JTAG_CalculateHashOfPublicKey(UINT_T * pPublicKey, UINT_T KeyId, UINT_T HashId, UINT_T UseCryptoSchemeIDInHash)
{
	// Calulate hash of public key
	// Compare to hash stored in fuses
	// return: NoError if compare successful
	// inputs:
	//    ptr to public key
	//    key id
	// 	  Hash id

	UINT_T Retval = NoError;
	HASHALGORITHMID_T HashAlg;
	UINT_T HashInputSizeInBytes;
	UINT_T HashInputData[WordLengthOf_PKCS2048*2+1];	// +1 for Crypto Scheme ID
	UINT_T FusedJTAGKeyHash[WordLengthOf_SHA512];
	UINT_T CalculatedHash[WordLengthOf_SHA512];
	UINT_T i;

#if JTAG_PROTOCOL_DEBUG
	UINT8_T UartMsgBuff[9];
	UartMsgBuff[8] = '\0';
	AddMessage("JTAG_CalculateHashOfPublicKey");
#endif

	switch (HashId)	{
		case JTAGID_SHA160 : HashAlg = SHA160; break;
		case JTAGID_SHA256 : HashAlg = SHA256; break;
		case JTAGID_SHA512 : HashAlg = SHA512; break;
		default : return HashSizeMismatch;
	} 

	switch (KeyId) {
		case JTAGID_PKCS1024 :
			if (UseCryptoSchemeIDInHash==1) {
				HashInputSizeInBytes = (WordLengthOf_PKCS1024*2+1)*BYTES_PER_WORD;
				// 1) Pack the crypto_scheme
				//crypto_scheme:	0x0000A100	PKCSv1_SHA1_1024RSA  
				//					0x0000A110	PKCSv1_SHA256_1024RSA
				HashInputData[0] = (HashAlg == SHA256) ? PKCSv1_SHA256_1024RSA :
													     PKCSv1_SHA1_1024RSA;
				// 2) Pack the Modulus and Exponent
				for (i=0; i < WordLengthOf_PKCS1024; i++)
				{
					HashInputData[i+1] = *(pPublicKey+WordLengthOf_PKCS1024);
					HashInputData[i+WordLengthOf_PKCS1024+1] = *(pPublicKey++);
				}
			}
			else { // DO NOT include crypto_scheme and DO NOT swap exponent and modulus !!!
				HashInputSizeInBytes = WordLengthOf_PKCS1024*2*BYTES_PER_WORD;
				// Pack the Exponent and Modulus
				for (i=0; i < WordLengthOf_PKCS1024*2; i++)
					HashInputData[i] = *(pPublicKey++);
			}								 
			break;
		case JTAGID_PKCS2048 :  
			if (UseCryptoSchemeIDInHash==1) {
				HashInputSizeInBytes = (WordLengthOf_PKCS2048*2+1)*BYTES_PER_WORD;
				// 1) Pack the crypto_scheme
				//crypto_scheme:	0x0000A200	PKCSv1_SHA1_2048RSA  
				//					0x0000A210	PKCSv1_SHA256_2048RSA
				HashInputData[0] = (HashAlg == SHA256) ? PKCSv1_SHA256_2048RSA :
													     PKCSv1_SHA1_2048RSA;
				// 2) Pack the Modulus and Exponent
				for (i=0; i < WordLengthOf_PKCS2048; i++)
				{
					HashInputData[i+1] = *(pPublicKey+WordLengthOf_PKCS2048);
					HashInputData[i+WordLengthOf_PKCS2048+1] = *(pPublicKey++);
				}
			}
			else { // DO NOT include crypto_scheme and DO NOT swap exponent and modulus !!!
				HashInputSizeInBytes = WordLengthOf_PKCS2048*2*BYTES_PER_WORD;
				// Pack the Exponent and Modulus
				for (i=0; i < WordLengthOf_PKCS2048*2; i++)
					HashInputData[i] = *(pPublicKey++);
			}				 
			break;
		case JTAGID_ECDSA256 :
			if (UseCryptoSchemeIDInHash==1) {
				HashInputSizeInBytes = (WordLengthOf_ECDSA256*2+1)*BYTES_PER_WORD;
			    // 1) Pack the crypto_scheme
				//crypto_scheme:	0x0000B101	ECCP256_FIPS_DSA_SHA1  
				//					0x0000B111	ECCP256_FIPS_DSA_SHA256
				//					0x0000B141	ECCP256_FIPS_DSA_SHA512								
				HashInputData[0] = (HashAlg == SHA512) ? ECCP256_FIPS_DSA_SHA512 :
								   (HashAlg == SHA256) ? ECCP256_FIPS_DSA_SHA256 :
								   						 ECCP256_FIPS_DSA_SHA1;
				// 2) Pack the X and Y coordinates
				for (i=0; i < WordLengthOf_ECDSA256*2; i++)
					HashInputData[i+1] = *(pPublicKey++);
			}
			else { // DO NOT include crypto_scheme
				HashInputSizeInBytes = WordLengthOf_ECDSA256*2*BYTES_PER_WORD;
				// 2) Pack the X and Y coordinates
				for (i=0; i < WordLengthOf_ECDSA256*2; i++)
					HashInputData[i] = *(pPublicKey++);
			}
			break;
		case JTAGID_ECDSA521 : 
			if (UseCryptoSchemeIDInHash==1) {
				HashInputSizeInBytes = (WordLengthOf_ECDSA521*2+1)*BYTES_PER_WORD;
			    // 1) Pack the crypto_scheme
				//crypto_scheme:	0x0000B301	ECCP521_FIPS_DSA_SHA1  
				//					0x0000B311	ECCP521_FIPS_DSA_SHA256
				//					0x0000B341	ECCP521_FIPS_DSA_SHA512				
				HashInputData[0] = (HashAlg == SHA512) ? ECCP521_FIPS_DSA_SHA512 :
								   (HashAlg == SHA256) ? ECCP521_FIPS_DSA_SHA256 :
								   						 ECCP521_FIPS_DSA_SHA1;
				// 2) Pack the X and Y coordinates
				for (i=0; i < WordLengthOf_ECDSA521*2; i++)
					HashInputData[i+1] = *(pPublicKey++);
			}
			else { // DO NOT include crypto_scheme
				HashInputSizeInBytes = WordLengthOf_ECDSA521*2*BYTES_PER_WORD;
				// 2) Pack the X and Y coordinates
				for (i=0; i < WordLengthOf_ECDSA521*2; i++)
					HashInputData[i] = *(pPublicKey++);
			}
			break;
		default: 
			return JtagReEnableOEMKeyLengthError;
	}	
  
	// 1. Calulate hash of public key
	Retval = SFs.pSHAMessageDigest((UINT8_T *)HashInputData, HashInputSizeInBytes, (UINT8_T *)CalculatedHash, HashAlg);
	if (Retval != NoError)	return Retval;

	// 2. Get the JTAG hash fuse 
	Retval = FUSE_ReadOemJtagHashKeyFuseBits(FusedJTAGKeyHash, (UINT_T)HashAlg);
	if (Retval != NoError)	return Retval;	
#if BOARD_DEBUG
	JTAG_Substitute_Fuses_For_BoardDebug(FusedJTAGKeyHash, KeyId, HashAlg, UseCryptoSchemeIDInHash);
#endif

	// 3. Compare the 2 hashes
#if JTAG_PROTOCOL_DEBUG
	AddMessage("--------------------------\0");
#endif
    for (i = 0; i < ((UINT_T)HashAlg / BYTES_PER_WORD); i++)
	{
#if JTAG_PROTOCOL_DEBUG	
		WriteStringToFFUart("Word: ");
		ConvertIntToBuf8(UartMsgBuff,i, 8,0x10);
		WriteStringToFFUart(UartMsgBuff);
		WriteStringToFFUart("; CalculatedHash= 0x\0");
		ConvertIntToBuf8(UartMsgBuff,CalculatedHash[i], 8,0x10);
		WriteStringToFFUart(UartMsgBuff);
		WriteStringToFFUart("; FusedJTAGKeyHash= 0x\0");
		ConvertIntToBuf8(UartMsgBuff,FusedJTAGKeyHash[i], 8,0x10);
		WriteStringToFFUart(UartMsgBuff);
		WriteStringToFFUart("\r\n\0");
#endif
		if (CalculatedHash[i] != FusedJTAGKeyHash[i])
			return (InvalidKeyHashError);
	}
#if JTAG_PROTOCOL_DEBUG
	AddMessage("--------------------------\0");
#endif
	
	return Retval;
}

UINT_T JTAG_VerifyPassword(UINT_T* pPublicKey, UINT_T KeyId, UINT_T HashId, UINT_T *pPassword, UINT_T* pNonce, UINT_T nonce_size)
{
	// Decrypts Encrypted nonce (password)
	// return: NoError if decrypted nonce == original nonce
	// inputs: 
	//  ptr to public key
	//  Key Id
	//  Hash Id
	//  ptr to password (encrypted nonce)
	//  ptr to original nonce
	//  nonce size in bits 

	UINT_T JtagKeyLengthInWords;
	UINT_T Retval = NoError;
	CRYPTO_SCHEME_ENUM crypto_scheme; 
	ECC_point eccp_dsa_public_key;
	int i;

	
#if JTAG_PROTOCOL_DEBUG	
    AddMessage("JTAG_VerifyPassword");
#endif
	
	// Note: Since the nonce size is small enough, we are not using verify_update		
	switch (KeyId)
	{
		case JTAGID_PKCS1024 :  
			JtagKeyLengthInWords = 32;
			switch (HashId)
			{
				//crypto_scheme:	0x0000A100	PKCSv1_SHA1_1024RSA
				//					0x0000A110	PKCSv1_SHA256_1024RSA
				case JTAGID_SHA160 : crypto_scheme = PKCSv1_SHA1_1024RSA;	break;
				case JTAGID_SHA256 : crypto_scheme = PKCSv1_SHA256_1024RSA;	break;
				default:			 return HashSizeMismatch;
			}

			wtm_emsa_pkcs1_v15_verify_init(crypto_scheme,						// CRYPTO_SCHEME_ENUM
										   &pPublicKey[JtagKeyLengthInWords],	// K2048 pub_key_mod (2nd half of pPublicKey) 
										   &pPublicKey[0]);						// K2048 pub_key_exp (1st half of pPublicKey)

			Retval = wtm_emsa_pkcs1_v15_verify_final(&pPassword[0],				// U32 *sig 
													 JtagKeyLengthInWords*4,	// U32 sig_len in bytes
													 pNonce,					// U32 *msg 
													 nonce_size/8);				// U32 msg_len in bytes

        // The following works with SHA160, but does not work with SHA256
		/*
			Retval = wtm_emsa_pkcs1_v15_verify (JtagKeyLengthInWords * 32,	// U32 key_len in bits
                                 		&pPublicKey[JtagKeyLengthInWords],	// K2048 pub_key_mod (2nd half of pPublicKey)
                                  		&pPublicKey[0],						// K2048 pub_key_exp (1st half of pPublicKey)
                                  		HashAlg,							// ALGORITHM_TYPE hash
                                  		pNonce,								// U32 *msg
                                  		nonce_size/8,						// U32 msg_len in bytes
                                  		&pPassword[0],						// U32 *sig
                                  		JtagKeyLengthInWords * 4);			// U32 sig_len in bytes
		*/
			break;

		case JTAGID_PKCS2048 : 
			JtagKeyLengthInWords = 64;
			switch (HashId)
			{
				//crypto_scheme:	0x0000A200	PKCSv1_SHA1_2048RSA  
				//					0x0000A210	PKCSv1_SHA256_2048RSA
				case JTAGID_SHA160 : crypto_scheme = PKCSv1_SHA1_2048RSA;	break;
				case JTAGID_SHA256 : crypto_scheme = PKCSv1_SHA256_2048RSA;	break;
				default:			 return HashSizeMismatch;
			}

			wtm_emsa_pkcs1_v15_verify_init(crypto_scheme,						// CRYPTO_SCHEME_ENUM
										   &pPublicKey[JtagKeyLengthInWords],	// K2048 pub_key_mod (2nd half of pPublicKey) 
										   &pPublicKey[0]);						// K2048 pub_key_exp (1st half of pPublicKey)

			Retval = wtm_emsa_pkcs1_v15_verify_final(&pPassword[0],				// U32 *sig 
													 JtagKeyLengthInWords*4,	// U32 sig_len in bytes
													 pNonce,					// U32 *msg 
													 nonce_size/8);				// U32 msg_len in bytes

        // The following works with SHA160, but does not work with SHA256
		/*
			Retval = wtm_emsa_pkcs1_v15_verify (JtagKeyLengthInWords * 32,	// U32 key_len in bits
                                 		&pPublicKey[JtagKeyLengthInWords],	// K2048 pub_key_mod (2nd half of pPublicKey)
                                  		&pPublicKey[0],						// K2048 pub_key_exp (1st half of pPublicKey)	
                                  		HashAlg,							// ALGORITHM_TYPE hash
                                  		pNonce,								// U32 *msg
                                  		nonce_size/8,						// U32 msg_len in bytes
                                  		&pPassword[0],						// U32 *sig
                                  		JtagKeyLengthInWords * 4);			// U32 sig_len in bytes
		*/
			break;
					 
		case JTAGID_ECDSA256 : 
			JtagKeyLengthInWords = 8;
			switch (HashId)
			{
				case JTAGID_SHA160 : crypto_scheme = ECCP256_FIPS_DSA_SHA1;	break;
				case JTAGID_SHA256 : crypto_scheme = ECCP256_FIPS_DSA_SHA256;	break;
				case JTAGID_SHA512 : crypto_scheme = ECCP256_FIPS_DSA_SHA512;	break;
				default:			 return HashSizeMismatch;
			}
			eccp_dsa_public_key.CoefLength = JtagKeyLengthInWords;
			eccp_dsa_public_key.FieldSize_in_bits = 256;
			for (i=0; i<JtagKeyLengthInWords; i++)
			{
				eccp_dsa_public_key.X[i] = pPublicKey[i];						// X: 1st half of pPublicKey
				eccp_dsa_public_key.Y[i] = pPublicKey[JtagKeyLengthInWords+i];	// Y: 2nd half of pPublicKey
			}

			Retval = wtm_eccp_dsa_verify_init ( crypto_scheme,				// CRYPTO_SCHEME_ENUM
										&eccp_dsa_public_key,				// ECC_point *pPublicKey
										NULL,								// EC_domain_parameters *ptr_eccp_dp
										ECC_MULT_RANDOM_MODE);				// U32 mult_mode
			if (Retval != NoError)  return Retval;

			Retval = wtm_eccp_dsa_verify_final ( crypto_scheme,				// CRYPTO_SCHEME_ENUM
										pNonce,								// void * pdata
										nonce_size/8,						// U32 total_data_len
										&pPassword[0],						// U32 *p_DSA_sign_r (1st half)
										&pPassword[JtagKeyLengthInWords]);	// U32 *p_DSA_sign_s (2nd half)

        // The following does not work
		/*
			Retval = wtm_eccp_dsa_verify( crypto_scheme,				// CRYPTO_SCHEME_ENUM
			                 		pNonce,								// void * pdata
			                 		nonce_size/8,						// U32 total_data_len
			                 		&eccp_dsa_public_key,				// ECC_point *pPublicKey
			                 		NULL,								// EC_domain_parameters *ptr_eccp_dp
			                 		ECC_MULT_RANDOM_MODE				// U32 mult_mode
			                 		&pPassword[0],						// U32 *p_DSA_sign_r (1st half)
			                 		&pPassword[JtagKeyLengthInWords]);	// U32 *p_DSA_sign_s (2nd half)
		*/
			break;

		case JTAGID_ECDSA521 : 
			JtagKeyLengthInWords = 17;
			switch (HashId)
			{
				case JTAGID_SHA160 : crypto_scheme = ECCP521_FIPS_DSA_SHA1;	break;
				case JTAGID_SHA256 : crypto_scheme = ECCP521_FIPS_DSA_SHA256;	break;
				case JTAGID_SHA512 : crypto_scheme = ECCP521_FIPS_DSA_SHA512;	break;
				default:			 return HashSizeMismatch;
			}
			eccp_dsa_public_key.CoefLength = JtagKeyLengthInWords;
			eccp_dsa_public_key.FieldSize_in_bits = 521;
			for (i=0; i<JtagKeyLengthInWords; i++)
			{
				eccp_dsa_public_key.X[i] = pPublicKey[i];						// X: 1st half of pPublicKey
				eccp_dsa_public_key.Y[i] = pPublicKey[JtagKeyLengthInWords+i];	// Y: 2nd half of pPublicKey
			}

			Retval = wtm_eccp_dsa_verify_init ( crypto_scheme,				// CRYPTO_SCHEME_ENUM
										&eccp_dsa_public_key,				// ECC_point *pPublicKey
										NULL,								// EC_domain_parameters *ptr_eccp_dp
										ECC_MULT_RANDOM_MODE);				// U32 mult_mode
			if (Retval != NoError) return Retval;

			Retval = wtm_eccp_dsa_verify_final ( crypto_scheme,				// CRYPTO_SCHEME_ENUM
										pNonce,								// void * pdata
										nonce_size/8,						// U32 total_data_len
										&pPassword[0],						// U32 *p_DSA_sign_r (1st half)
										&pPassword[JtagKeyLengthInWords]);	// U32 *p_DSA_sign_s (2nd half)

        // The following does not work
		/*
			Retval = wtm_eccp_dsa_verify( crypto_scheme,				// CRYPTO_SCHEME_ENUM
			                 		pNonce,								// void * pdata
			                 		nonce_size/8,						// U32 total_data_len
			                 		&eccp_dsa_public_key,				// ECC_point *pPublicKey
			                 		NULL,								// EC_domain_parameters *ptr_eccp_dp
			                 		ECC_MULT_RANDOM_MODE				// U32 mult_mode
			                 		&pPassword[0],						// U32 *p_DSA_sign_r (1st half)
			                 		&pPassword[JtagKeyLengthInWords]);	// U32 *p_DSA_sign_s (2nd half)
		*/
			break;

		default: 
			return JtagReEnableOEMKeyLengthError;
	}

	return Retval;
}

UINT_T JTAG_GetNonce(UINT_T* pNonce, UINT_T nonce_size)
{
	// returns a nonce (random number) of the given size
	// inputs: 
	//     nonce size in bits: 
	//     ptr to buffer to put nonce


	UINT_T Retval = NoError;

#if JTAG_PROTOCOL_DEBUG
	AddMessage("JTAG_GetNonce");
#endif
	if (nonce_size != SZ_WTM_NONCE_IN_BITS)
		Retval = JtagReEnableError;
	else {
		wtm_drbg_init();	// always returns STATUS_SUCCESS
		Retval = get_nonce(pNonce);
	}
	return Retval;
}

UINT_T JTAG_GetNonceSize()
{
	// returns size of nonce in bits for this platform
	//		eschel: 64 bits
	//      MMP2:  256 bits

	UINT_T nonce_size = SZ_WTM_NONCE_IN_BITS;
	return nonce_size;
}

void JTAG_EnableJTAG()
{
	// Enable JTAG on non-secure PJ4 core(s) only.
#if JTAG_PROTOCOL_DEBUG
   	AddMessage("Enabling JTAG ");
#endif
	BU_REG_WRITE(JTAG_PJ4_ZSP_DEBUG_VALID_CODE_REG, JTAG_REENABLE_MAGIC_NUMBER);
	BU_REG_WRITE(JTAG_PJ4_ZSP_JTAG_BYPASS_COPNTROL_REG,JTAG_PJ4_ZSP_JTAG_BYPASS_MASK);
	return;
}



UINT_T JTAG_IsHashSupported(UINT_T JTAG_HASH_ID)
{
	// Returns TRUE if JTAG hash algorithm is supported on this platform
   	UINT_T Retval = NoError;
	switch (JTAG_HASH_ID)
	{
		case JTAGID_SHA160 : Retval = NoError;	break;
		case JTAGID_SHA256 : Retval = NoError;	break;
		case JTAGID_SHA512 : Retval = HashSizeMismatch;	break; // Not supported yet
		default:			 Retval = HashSizeMismatch;
	}
	return Retval;
}

UINT_T JTAG_IsKeySupported(UINT_T JTAG_KEY_ID)
{
	//// Returns TRUE if JTAG Key algorithm is supported on this platform 
   	UINT_T Retval = NoError;
	switch (JTAG_KEY_ID)
	{
		case JTAGID_PKCS1024 : Retval = NoError;	break;
		case JTAGID_PKCS2048 : Retval = NoError;	break;
		case JTAGID_ECDSA256 : Retval = NoError;	break;
		case JTAGID_ECDSA521 : Retval = NoError;	break;
		default:			   Retval = JtagReEnableOEMKeyLengthError;
	}
	return Retval;
}

void JTAG_SetUseJTAGInterrupts()
{
	// Called by SetParameter
	g_JTAG_UseInterrupts = 1;
	return;
}

void JTAG_SetForceDownload()
{
	// Called by SetParameter

	//Disable until TBD
	//P_TBR_Env pTBR;
	//pTBR = GetTBREnvStruct();
	//pTBR->ErrorCode = ForceDownloadPseudoError;	
	return;
}

UINT_T JTAG_SetBootState(UINT_T BootState)
{
	//// TBD Called by SetParameter

	P_TBR_Env pTBR;

	pTBR = GetTBREnvStruct();
	pTBR->Fuses.bits.PlatformState = BootState;
	return NoError;
}

UINT_T JTAG_SetGPIO(UINT_T GPIO_Sel)
{
	//// TBD  Called by SetParameter

	return NoError;
}

UINT_T JTAG_SetWDT (UINT_T WDT)
{
	//// Called by SetParameter

	return NoError;
}

void JTAG_WriteDebugKeys(UINT_T* pDebugKeyData)
{
	//Write registers in reverse order so Reg 1 (command reg) is written last

#if (NUM_JTAG_DEBUGKEY_REGISTERS == 8) // (8 Registers, Uni-directional)
	BU_REG_WRITE(MESSAGE_FROM_SP_TO_JTAG_8_REG, pDebugKeyData[7]);
	BU_REG_WRITE(MESSAGE_FROM_SP_TO_JTAG_7_REG, pDebugKeyData[6]);
	BU_REG_WRITE(MESSAGE_FROM_SP_TO_JTAG_6_REG, pDebugKeyData[5]);
	BU_REG_WRITE(MESSAGE_FROM_SP_TO_JTAG_5_REG, pDebugKeyData[4]);
	BU_REG_WRITE(MESSAGE_FROM_SP_TO_JTAG_4_REG, pDebugKeyData[3]);
	BU_REG_WRITE(MESSAGE_FROM_SP_TO_JTAG_3_REG, pDebugKeyData[2]);
	BU_REG_WRITE(MESSAGE_FROM_SP_TO_JTAG_2_REG, pDebugKeyData[1]);
	BU_REG_WRITE(MESSAGE_FROM_SP_TO_JTAG_1_REG, pDebugKeyData[0]);
#else	// (2 Registers, Bi-directional)
	BU_REG_WRITE(JTAG_DEBUGKEY_2_REG, pDebugKeyData[1]);
	BU_REG_WRITE(JTAG_DEBUGKEY_1_REG, pDebugKeyData[0]);
#endif //NUM_JTAG_DEBUGKEY_REGISTERS
	return;
}

void JTAG_ReadDebugKeys(UINT_T * pDebugKeyData)
{
	// Read the control register first. 
	// If it's valid then the rest of registers are valid.

#if (NUM_JTAG_DEBUGKEY_REGISTERS == 8) // (8 Registers, Uni-directional)
	pDebugKeyData[0]= BU_REG_READ(JTAG_DEBUGKEY_1_REG);
	pDebugKeyData[1]= BU_REG_READ(JTAG_DEBUGKEY_2_REG);
	pDebugKeyData[2]= BU_REG_READ(JTAG_DEBUGKEY_3_REG);
	pDebugKeyData[3]= BU_REG_READ(JTAG_DEBUGKEY_4_REG);
	pDebugKeyData[4]= BU_REG_READ(JTAG_DEBUGKEY_5_REG);
	pDebugKeyData[5]= BU_REG_READ(JTAG_DEBUGKEY_6_REG);
	pDebugKeyData[6]= BU_REG_READ(JTAG_DEBUGKEY_7_REG);
	pDebugKeyData[7]= BU_REG_READ(JTAG_DEBUGKEY_8_REG);
#else	// (2 Registers, Bi-directional)
	pDebugKeyData[0]= BU_REG_READ(JTAG_DEBUGKEY_1_REG);
	pDebugKeyData[1]= BU_REG_READ(JTAG_DEBUGKEY_2_REG);
#endif //NUM_JTAG_DEBUGKEY_REGISTERS
	return;
}

#if BOARD_DEBUG
void JTAG_Substitute_Fuses_For_BoardDebug(UINT_T * FusedJTAGKeyHash, UINT_T KeyId, HASHALGORITHMID_T HashAlg, UINT_T UseCryptoSchemeIDInHash)
{
    // For board debug, if the fuses are zero, use the hash values of OEM sample key as fused JTAG key hash
	UINT_T i;
	UINT_T nonzero_count = 0; 

    for (i = 0; i < ((UINT_T)HashAlg / BYTES_PER_WORD); i++)
	{
		if (FusedJTAGKeyHash[i]) {nonzero_count++;}
	}
	if (nonzero_count == 0)
	{
		switch (KeyId) {
			case JTAGID_PKCS1024 :
					switch (HashAlg)	{
						case SHA160 : 
							if (UseCryptoSchemeIDInHash==1) {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for PKCSv1_SHA1_1024RSA with SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0x932BB3DE;
								FusedJTAGKeyHash[1]= 0x2814E42A;
								FusedJTAGKeyHash[2]= 0x3BA49289;
								FusedJTAGKeyHash[3]= 0xA4D9DF8E;
								FusedJTAGKeyHash[4]= 0xA027FD7D;
							}
							else {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for PKCSv1_SHA1_1024RSA without SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0x65A6CC2A;
								FusedJTAGKeyHash[1]= 0xE06211DA;
								FusedJTAGKeyHash[2]= 0xEFC8F908;
								FusedJTAGKeyHash[3]= 0x008731C9;
								FusedJTAGKeyHash[4]= 0xAAB8672B;
							}
							break;
						case SHA256 : 
							if (UseCryptoSchemeIDInHash==1) {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for PKCSv1_SHA256_1024RSA with SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0xB6DFB0D0;
								FusedJTAGKeyHash[1]= 0xD75A2F4C;
								FusedJTAGKeyHash[2]= 0x584F21C9;
								FusedJTAGKeyHash[3]= 0x582D9BCC;
								FusedJTAGKeyHash[4]= 0xA3C8BDCC;
								FusedJTAGKeyHash[5]= 0x15376227;
								FusedJTAGKeyHash[6]= 0xAE782C28;
								FusedJTAGKeyHash[7]= 0x85499389;
							}
							else {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for PKCSv1_SHA256_1024RSA without SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0x4E4956DB;
								FusedJTAGKeyHash[1]= 0x392EB22B;
								FusedJTAGKeyHash[2]= 0x51FA2024;
								FusedJTAGKeyHash[3]= 0xD16EDB45;
								FusedJTAGKeyHash[4]= 0xFFAD6EEC;
								FusedJTAGKeyHash[5]= 0x0933FE6D;
								FusedJTAGKeyHash[6]= 0x4E9ACA60;
								FusedJTAGKeyHash[7]= 0x4E0B9803;
							}
							break;
						default: break;
					} //switch (HashAlg) 							 
				break;
			case JTAGID_PKCS2048 :
					switch (HashAlg)	{
						case SHA160 : 
							if (UseCryptoSchemeIDInHash==1) {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for PKCSv1_SHA1_2048RSA with SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0x7FA3837A;
								FusedJTAGKeyHash[1]= 0xF8702045;
								FusedJTAGKeyHash[2]= 0x0D51F187;
								FusedJTAGKeyHash[3]= 0x97362CAD;
								FusedJTAGKeyHash[4]= 0xD47949D8;
							}
							else {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for PKCSv1_SHA1_2048RSA without SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0x727C8380;
								FusedJTAGKeyHash[1]= 0xECC6798D;
								FusedJTAGKeyHash[2]= 0x85038E01;
								FusedJTAGKeyHash[3]= 0x452D8674;
								FusedJTAGKeyHash[4]= 0x646C413E;
							}
							break;
						case SHA256 : 
							if (UseCryptoSchemeIDInHash==1) {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for PKCSv1_SHA256_2048RSA with SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0xD0B658E3;
								FusedJTAGKeyHash[1]= 0xE66CEED3;
								FusedJTAGKeyHash[2]= 0xB2B95CED;
								FusedJTAGKeyHash[3]= 0x9AD6331D;
								FusedJTAGKeyHash[4]= 0x625C404D;
								FusedJTAGKeyHash[5]= 0x11747D62;
								FusedJTAGKeyHash[6]= 0x4CB01C6C;
								FusedJTAGKeyHash[7]= 0x8B3A1436;
							}
							else {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for PKCSv1_SHA256_2048RSA without SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0x0669DD2C;
								FusedJTAGKeyHash[1]= 0xC282FA8C;
								FusedJTAGKeyHash[2]= 0x1E576B0F;
								FusedJTAGKeyHash[3]= 0x68DA76F9;
								FusedJTAGKeyHash[4]= 0xB2165DB8;
								FusedJTAGKeyHash[5]= 0x4C2C419E;
								FusedJTAGKeyHash[6]= 0x39D6C1F2;
								FusedJTAGKeyHash[7]= 0xE068BF94;
							}
							break;
						default: break;
					} //switch (HashAlg) 							 
				break;
			case JTAGID_ECDSA256 :
					switch (HashAlg)	{
						case SHA160 : 
							if (UseCryptoSchemeIDInHash==1) {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for ECCP256_FIPS_DSA_SHA1 with SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0x3C2AE3AF;
								FusedJTAGKeyHash[1]= 0x5F238D17;
								FusedJTAGKeyHash[2]= 0xC7A5414F;
								FusedJTAGKeyHash[3]= 0x5E1A7AC2;
								FusedJTAGKeyHash[4]= 0x7DEA5384;
							}
							else {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for ECCP256_FIPS_DSA_SHA1 without SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0xEEE11F84;
								FusedJTAGKeyHash[1]= 0x9841B81A;
								FusedJTAGKeyHash[2]= 0x84570C41;
								FusedJTAGKeyHash[3]= 0xF7E91710;
								FusedJTAGKeyHash[4]= 0xB757DAA4;
							}
							break;
						case SHA256 : 
							if (UseCryptoSchemeIDInHash==1) {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for ECCP256_FIPS_DSA_SHA256 with SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0x8496FE1E;
								FusedJTAGKeyHash[1]= 0xFBB04A5C;
								FusedJTAGKeyHash[2]= 0x42FF5AD1;
								FusedJTAGKeyHash[3]= 0x5173F8A9;
								FusedJTAGKeyHash[4]= 0x821B9D6C;
								FusedJTAGKeyHash[5]= 0xE9F11405;
								FusedJTAGKeyHash[6]= 0xFDC05C9F;
								FusedJTAGKeyHash[7]= 0x7B3ED60F;
							}
							else {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for ECCP256_FIPS_DSA_SHA256 without SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0x29AFE602;
								FusedJTAGKeyHash[1]= 0xDD9F072F;
								FusedJTAGKeyHash[2]= 0x563A6013;
								FusedJTAGKeyHash[3]= 0x2B9781E5;
								FusedJTAGKeyHash[4]= 0x8D4A8C5A;
								FusedJTAGKeyHash[5]= 0xA8F8B9D2;
								FusedJTAGKeyHash[6]= 0xADE39F42;
								FusedJTAGKeyHash[7]= 0xFF5C863B;
							}
							break;
						default: break;
					} //switch (HashAlg) 							 
				break;
			case JTAGID_ECDSA521 :
					switch (HashAlg)	{
						case SHA160 : 
							if (UseCryptoSchemeIDInHash==1) {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for ECCP521_FIPS_DSA_SHA1 with SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0x59010CE2;
								FusedJTAGKeyHash[1]= 0x50024AE1;
								FusedJTAGKeyHash[2]= 0x30A1BEA0;
								FusedJTAGKeyHash[3]= 0xF599B006;
								FusedJTAGKeyHash[4]= 0x479B75A3;
							}
							else {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for ECCP521_FIPS_DSA_SHA1 without SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0x9E2C42F3;
								FusedJTAGKeyHash[1]= 0x89AEC08E;
								FusedJTAGKeyHash[2]= 0x3A62233F;
								FusedJTAGKeyHash[3]= 0xBCFB6B5B;
								FusedJTAGKeyHash[4]= 0x843ECE51;
							}
							break;
						case SHA256 : 
							if (UseCryptoSchemeIDInHash==1)  {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for ECCP521_FIPS_DSA_SHA256 with SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0x21D3CE93;
								FusedJTAGKeyHash[1]= 0x1235A658;
								FusedJTAGKeyHash[2]= 0x7C547551;
								FusedJTAGKeyHash[3]= 0xC9D0AF51;
								FusedJTAGKeyHash[4]= 0x84D51DD5;
								FusedJTAGKeyHash[5]= 0xB275F222;
								FusedJTAGKeyHash[6]= 0xDE3ACE64;
								FusedJTAGKeyHash[7]= 0x86244D52;
							}
							else {
#if JTAG_PROTOCOL_DEBUG
	AddMessage("Sample Key Hash Values Used for ECCP521_FIPS_DSA_SHA256 without SchemeID");
#endif
								FusedJTAGKeyHash[0]= 0xAA688CE6;
								FusedJTAGKeyHash[1]= 0x2A82CE51;
								FusedJTAGKeyHash[2]= 0x52F5A941;
								FusedJTAGKeyHash[3]= 0x2BCC056A;
								FusedJTAGKeyHash[4]= 0xCBDF87E4;
								FusedJTAGKeyHash[5]= 0x129576F3;
								FusedJTAGKeyHash[6]= 0x4F24C979;
								FusedJTAGKeyHash[7]= 0x05C08C22;
							}
							break;
						default: break;
					} //switch (HashAlg) 							 
				break;
			default: break;
		} //switch (KeyId)
	}
}
#endif // BOARD_DEBUG
	 
#endif // JTAG_PROTOCOL_OVER_JTAG_SUPPORTED