/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document.
**  Except as permitted by such license, no part of this document may be
**  reproduced, stored in a retrieval system, or transmitted in any form or by
**  any means without the express written consent of Intel Corporation.
**
**  FILENAME:	Interrupts.c
**
**  PURPOSE: 	Contain Interrupts/Exception handlers for Bulverde B0 Boot ROM
**
******************************************************************************/
#include "predefines.h"
#include "interrupts.h"
#include "PlatformConfig.h"
#include "ProtocolManager.h"
#include "UartDownload.h"
#include "Jtag.h"
#if TRUSTED
#if (BL_USE_WTM_CRYPTO || BL_USE_WTM_FUSE_PROG) /* used by bootloader */
#include "wtm_mbox_impl.h"
#endif
#if USE_WTM3                                    /* used by bootrom    */
	#include "wtm3_security.h"
#endif
#endif

#if !BOOTROM
extern unsigned int ObmXsGetProcessorVersion();
#endif

static UINT8_T UdcDmaUsageFlag   = FALSE;  // Used to enable/disable DMA usage
static UINT8_T PortInterrupt	 = FALSE;
static CoreType core			 = invalid_core;

UINT8_T GetPortInterruptFlag(void){ return PortInterrupt;}

void SetPortInterruptFlag(void){ PortInterrupt = TRUE;}

void ClearPortInterruptFlag(void){
	PortInterrupt = FALSE;
	return;
}

#if !BOOTROM
void DetermineCore()
{
	unsigned int id;
	id = ObmXsGetProcessorVersion();
	id = 0xFFF & (id >> 4);  //need to check Part Number [15:4]
	core = (id == PJ4_PART_NUMBER) ? PJ4_core : Dragonite_core;
}
#endif

void IRQ_Glb_Ena()
{
	BU_U32	reg;

#if BOOTROM
	reg = BU_REG_READ(ICU_GBL_IRQ0_MSK);
	reg &= (~ICU_GBL_IRQ_MSK_IRQ_MSK);
	BU_REG_WRITE(ICU_GBL_IRQ0_MSK, reg);
#else
	if(core == invalid_core)
		DetermineCore();

	if(core == Dragonite_core)
	{
		reg = BU_REG_READ(ICU_GBL_IRQ0_MSK);
		reg &= (~ICU_GBL_IRQ_MSK_IRQ_MSK);
		BU_REG_WRITE(ICU_GBL_IRQ0_MSK, reg);
	} else {
		reg = BU_REG_READ(ICU_GBL_IRQ1_MSK);
		reg &= (~ICU_GBL_IRQ_MSK_IRQ_MSK);
		BU_REG_WRITE(ICU_GBL_IRQ1_MSK, reg);
	}
#endif

}


void EnableInt(unsigned int int_num)
{
#if BOOTROM
   BU_REG_WRITE(ICU_IRQ_0_TO_63_CONF + int_num*4, ( IRQ_INT<<4) | BROM_PRIORITY );		// bpc: this is appropriate for BootROM, which is running from core 0 <<4 targets core 0
#else
	if(core == invalid_core)
		DetermineCore();

	if(core == Dragonite_core)
		BU_REG_WRITE(ICU_IRQ_0_TO_63_CONF + int_num*4, ( IRQ_INT<<4) | BROM_PRIORITY );		// bpc: this is appropriate for BootROM, which is running from core 0 <<4 targets core 0
	else
		BU_REG_WRITE(ICU_IRQ_0_TO_63_CONF + int_num*4, ( IRQ_INT<<5) | BROM_PRIORITY );		// bpc: this is appropriate for BootLoader, which is running from core 1. <<5 targets core 1
#endif
}

void DisableInt(BU_U32 int_num)
{
   	BU_REG_WRITE(ICU_IRQ_0_TO_63_CONF + int_num*4, 0 );
}

UINT_T EnablePeripheralIRQInterrupt(unsigned int InterruptID)
{
    EnableInt(InterruptID);
	return(NoError);
}

UINT_T DisablePeripheralIRQInterrupt(unsigned int InterruptID)
{
    DisableInt(InterruptID);
	return(NoError);
}

unsigned int GetIrqStatus(void)
{
	UINT_T Regval = 0;
#if BOOTROM
	Regval = BU_REG_READ(ICU_IRQ_SEL_INT_NUM_CORE0);
#else
	if(core == invalid_core)
		DetermineCore();

	if(core == Dragonite_core)
		Regval = BU_REG_READ(ICU_IRQ_SEL_INT_NUM_CORE0);
	else
		Regval = BU_REG_READ(ICU_IRQ_SEL_INT_NUM_CORE1);
#endif
    return Regval;
}

void IrqHandler(void)
{
    volatile unsigned int int_num;
	void *handle;

    int_num = GetIrqStatus();
    if ( int_num &  ICU_IRQ_SEL_INT_NUM_INT_PENDING )
    {
 		/* mask out the pending bit to get low 5 bit INT num */
		int_num &= ~ICU_IRQ_SEL_INT_NUM_INT_PENDING;

		switch (int_num)
		{

			case FFUART_INT:
			{
				PortInterrupt = TRUE;
				FFUARTInterruptHandler();
				break;
			}
		#if USBCI
			case INT_USB:
			{
				PortInterrupt = TRUE;
	            PlatformCI2InterruptHandler();
				break;
			}
		#endif

		#if MMC_CODE
			case INT_MMC5:
			case INT_MMC3:
			{
				SDMMC_ISR();
				break;
			}
		#endif
#if TRUSTED
		#if (USE_WTM3 || BL_USE_WTM_CRYPTO || BL_USE_WTM_FUSE_PROG)
			case INT_WTM:
			{
                #if USE_WTM3
				WTM3_ISR();               /* used by bootrom               */
                #endif

		        #if (BL_USE_WTM_CRYPTO || BL_USE_WTM_FUSE_PROG)
                wtm3_interrupt_handler();  /* used by wtm mailbox interface */
                #endif
				break;
			}
		#endif
#endif
#if JTAG_PROTOCOL_OVER_JTAG_SUPPORTED
		   	case INT_JTAG:
			{
				JTAG_DebugKey_InterruptHandler();
				break;
			}
#endif //#if JTAG_PROTOCOL_OVER_JTAG_SUPPORTED

		}
    }
}//End IrqHandler


void UndefinedHandler(void)
{
	// Terminate, simply loop forever for now
	while (1){}
}

void SwiHandler(void)
{
	// Terminate, simply loop forever for now
	while (1){}
}

void PrefetchHandler(void)
{
	// Terminate, simply loop forever for now
	while (1){}
}

void AbortHandler(void)
{
	// Terminate, simply loop forever for now
	while (1){}
}


void FiqHandler(void)
{
   while (1){}
}


void INT_init()
{
	int i;

   	/* disable global IRQ+FIR */
#ifdef BOOTROM
	BU_REG_WRITE(ICU_GBL_IRQ0_MSK, ICU_GBL_IRQ_MSK_IRQ_MSK);
	BU_REG_WRITE(ICU_GBL_IRQ0_MSK, ICU_GBL_FIQ_MSK_FIQ_MSK);
#else
   	/* disable global IRQ+FIR */
	if(core == invalid_core)
		DetermineCore();

	if(core == Dragonite_core)
	{
		BU_REG_WRITE(ICU_GBL_IRQ0_MSK, ICU_GBL_IRQ_MSK_IRQ_MSK);
		BU_REG_WRITE(ICU_GBL_IRQ0_MSK, ICU_GBL_FIQ_MSK_FIQ_MSK);
	}
	else
	{
		BU_REG_WRITE(ICU_GBL_IRQ1_MSK, ICU_GBL_IRQ_MSK_IRQ_MSK);
		BU_REG_WRITE(ICU_GBL_IRQ1_MSK, ICU_GBL_FIQ_MSK_FIQ_MSK);
	}

#endif
}
