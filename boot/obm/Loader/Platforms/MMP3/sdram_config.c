/******************************************************************************
**  COPYRIGHT  2007 Marvell Inernational Ltd.
**  All Rights Reserved
******************************************************************************/


#include "predefines.h"
#include "MCU.h"
#include "mcu_extras.h"
#include "processor_enums.h"
#include "sdram_specs.h"
#include "sdram_support.h"
#include "sdram_registers_defaults.h"
#include "tim.h"
#include "Errors.h"
#include "Typedef.h"
#include "sdram_config.h"
#include "timer.h"
#include "FuseHw.h"
#include "trustzone_config.h"
#if BOOTROM
#include "bootrom.h"
#else
#include "BootLoader.h"
#endif


// This routine will configure and enable SDRam DDR.
// It has the ability to select different DDR frequencies and geometries if the TIM area contains these specifiers.
// If no specifiers are found, then default values will be used.

// Return value: the base address of the ddr region for CS0.

// Overview of the SDRam DDR initialization:
//
// If TIM SDRam configuration records are available:
// 0. Make sure the required clocks are enabled. (requires TIM Clocks Enable records)
// 1. Make sure the memory voltage planes are up. (requires TIM Voltage records)
// 2. Make sure the clocks are at the correct frequency. (requires TIM Frequency records)
// 3. Program the MCU registers. (can override default values using optional TIM SDRam records
//
// otherwise (when no TIM SDRam configuration records are available)
// 1. Program the MCU registers using hardcoded default values.
//    - no clock enable, voltage or frequency changing occurs in this mode.
//


UINT_T ConfigureMemoryController( 
	void			*pTIM, 
	void			*pDDRScratchAreaAddress, 
	unsigned long	ulDDRScratchAreaLength
	)
{
	// variables for walking through the ddr records
	DDRCSpecList_T	*pDDRCStartAddress;
	DDRCSpecList_T	*pDDRCSpecs;
	DDRCSpec_T		*pDDRCSpec;
	CMCCSpecList_T	*pCMCCSpecs;
	CMCCSpec_T		*pCMCCSpec;
	UINT_T TZDDRBaseAddr, Size, Permissions; // Used for TrustZone.

	int				nRecs;
	int				r;
	UINT_T			status;
	UINT_T			value;
	unsigned long	faultaddr;
	unsigned long	baseaddr = 0xffffffff;
    unsigned long 	readTarget;
	unsigned long	loop;
	unsigned long	retval;
	
	// variables for doing ddr record to register field value conversions and updates
/*
	unsigned long	*pReg;
	unsigned long	(*pCnvFx)();
	int				bfs, bfs_low, bfs_hi;
	int				bfl, bfl_low, bfl_hi;
	unsigned long	args[4];

	unsigned long	dclk = MMP2_DEFAULT_DCLK;		// can be overridden with DDR record.
	unsigned long	fclk = MMP2_DEFAULT_FCLK;		// used for refresh calcs, can be overrideen with DDR record.
*/

	// some flags that enable / disable functionality within this routine.
	// these can be overriden by time records.
	unsigned long	fDoSDRamInit = 0;				// usually only overridden for testing on first silicon.
	unsigned long	fDoMemoryCheck = 0;				// usually only overridden for testing on first silicon.
	unsigned long	fDoConsumeDDRPackages = 0;      // Default

	retval = DDR_NotInitializedError;   // Initialize the return value
	
	if( pTIM )
	{
		// Look for CMCC package in TIM
		pCMCCSpecs = (CMCCSpecList_T*)FindPackageInReserved(&status, pTIM, CMCCID);	// 'CMCC'
		if( status == NoError )
		{
		return (DDR_CMCC_Package_Obsolete);
		}
	}//End Looking for CMCC package

 return retval;

}//EndConfigureMemoryController


UINT_T GetDDRSize()
{
    // This supports CS0 only
	UINT_T size;
	unsigned volatile long * pReg;
	UINT_T bfs;		// lsb location of bit field
	UINT_T bfl;		// number of bits in the fields
	// Read Memory Address Map Register 0 - CS0 (offset 0x010)
    // (MEMORY_ADDRESS_MAP)
    pReg = MCU_REG_MMU_MMAP0;
	bfs = 16;
	bfl = 4;
	size = ReadBitField(pReg, bfs, bfl);
    switch(size)
	{
		case(0xF): {size = 0x80000000; break;} //(UINT)(2048*(1024*1024))
		case(0xE): {size = 1024*(1024*1024); break;}
		case(0xD): {size =  512*(1024*1024); break;}
		case(0xC): {size =  256*(1024*1024); break;}
		case(0xB): {size =  128*(1024*1024); break;}
		case(0xA): {size =   64*(1024*1024); break;}
		case(0x9): {size =   32*(1024*1024); break;}
		case(0x8): {size =   16*(1024*1024); break;}
		case(0x7): {size =    8*(1024*1024); break;}
		default:   {size =    0; break;}
	}
    return size;
}

UINT_T GetDDRBaseAddress()
{
    // This supports CS0 only
	UINT_T baseAddress;
	
	// Read Memory Address Map Register 0 (MMU_MMAP0)- CS0 (offset 0x010)
	// Keep the upper 9 bits (Start Address)
   	baseAddress = (*(unsigned volatile long *)MCU_REG_MMU_MMAP0)& MEMORY_ADDRESS_MAP_START_ADDR_MSK;
	return baseAddress;
} 

UINT_T PlatformSpecificPostDDRConfig(pTIM pTIM_h, pFUSE_SET pFuses)
{
	UINT_T retval = NoError;
#if BOOTROM
	UINT_T SOCFuses[4];
	UINT_T Fuse93, Fuse90, Fuse85;
	UINT_T TZDDRBaseAddr, Size, Permissions; // Used for TrustZone.

	retval = FindTrustZonePackageInTim(pTIM_h);
	if (retval != NoError)
	{
		// No Trustzone package found
		// Nothing to do
		return NoError;
	}
	// Trustzone package exists in TIM
	// Check that fuses appopriately configured to support Trustzone 
	// Fuses 93/90/85 = 1,1,0 respectively

	FUSE_ReadSocConfigFuseBits((UINT_T *)SOCFuses, sizeof(SOCFuses));
	Fuse93 = (SOCFuses[2]>>29) & 1;
	Fuse90 = (SOCFuses[2]>>26) & 1;
	Fuse85 = (SOCFuses[2]>>21) & 1;

	// If we have a trustzone package and the fuses are not configured 
	// appropriately, return an error to stop the boot.
	if (!((Fuse93==1)&&(Fuse90==1)&&(Fuse85==0)))
	{
		return DDR_FusesNotConfiguredForTrustzone;
	}
	// Fuses are set correctly to support trustzone.
	// Enable Trustzone for PJ4	(Set ENABLE_AP_TZ)

	*SEC_ENA_CTRL_SEC_ROM_SET = *SEC_ENA_CTRL_SEC_ROM_SET | ENABLE_AP_TZ_MSK;
	
	// Configure Trustzone
	ConfigureTrustZone( pTIM_h );
#endif
	return retval;
}
