# (C) Copyright 2007 Marvell International Ltd.  
#  All Rights Reserved
#
##############################################################
#
#  Platform Specific macros and options	- TTC
#
#############################################################

################### FLAGS START #############################

################### Global Overrides ########################

# Crypto and fuse programming module selection
BL_USE_IPPCP_CRYPTO = 0
BL_USE_GEU_FUSE_PROG = 0
BL_USE_WTM_CRYPTO = 1
BL_USE_WTM_FUSE_PROG = 1

SDHC_CNTL = 2				# Old MMC controller = 1, the new one starting with MMP2 A0 = 2

# New DDR Configuration
NEW_DDRCNFG = 1

################### Platform Specific ########################
BOARD = JASPER
# Fix me. I exist in global Makefile too.
JTAG_PROTOCOL_OVER_JTAG_SUPPORTED = 0

################### Makefile flags ###########################
DDRBASE = 1
ISRAMBASE = 0

################### FLAGS END ################################

TECHNOLOGY = TTC

!if $(DDRBASE)
# the .bld file makes use of these rom addresses...
# make sure platform_defs.inc stack info is compatbile with this
# memory map:
# use ddr from 00000000 to 00020000
# code  00000000 - 00016000
# data  00016000 - 00016800
# bss   00016800 - 00019e00
#       00019e00 - 0001d800  3a00 available
# stack 0001d800 - 00020000  2800 combined stacks size
ROMCODESTART = 0x00000000
ROMDATASTART = 0x00016000
# loadoffsets.inc test this variable.
PLAT_AFLAGS = $(PLAT_AFLAGS) -PD "DDRBASE SETA 1" -PD "ISRAMBASE SETA 0"
!endif

!if $(ISRAMBASE)
# the .bld file makes use of these rom addresses...
# make sure platform_defs.inc stack info is compatbile with this
# memory map:
# use isram from d1000000 to d1020000
# code  d1000000 - d1016000
# data  d1016000 - d1016800
# bss   d1016800 - d1019e00
#       d1019e00 - d101d800  3a00 available
# stack d101d800 - d1020000  2800 combined stacks size
ROMCODESTART = 0xd1000000
ROMDATASTART = 0xd1016000
# loadoffsets.inc test this variable.
PLAT_AFLAGS = $(PLAT_AFLAGS) -PD "ISRAMBASE SETA 1" -PD "DDRBASE SETA 0"
!endif

##############################################################
#
#  Library Selection 
#  	- selected by RVCT_BUILD define, and ARM_MODE possibly 
#
##############################################################
LIBLIST = \
	"$(BASE)\SecureBoot\FuseBlock\FuseLib_$(ToolChain)_$(InstructionType)_$(PLATFORM).a" \
!if $(USBCI)
	"$(BASE)\Download\USB2CI\USB2CI_$(ToolChain)_$(InstructionType).a" \
!endif

##############################################################
#
#  Configure Board Name Macro 
#
##############################################################
!if ("$(BOARD)" == "Jasper" )
BOARDDEF = -DJASPER=1
!endif
!if ("$(BOARD)" == "EVBII" )
BOARDDEF = -DEVBII=1
!endif
!if ("$(BOARD)" == "WAYLAND" )
BOARDDEF = -DWAYLAND=1
!endif

##############################################################
#  Compiler Tool Configuration
##############################################################

!if $(RVCT_BUILD)	###### RVCT ######

!if $(RELEASE)
#release flags
OPTIMIZE_LVL = -O3 -Ospace
!else
#debug flags
OPTIMIZE_LVL = -O1 --debug
!endif

A_FLAGS = $(A_FLAGS) $(PLAT_AFLAGS) --cpu 5TE --diag_suppress=A3910W -g --pd "RVCT SETA 1"
CC_FLAGS = -W -DRVCT --library_interface=aeabi_clib	$(OPTIMIZE_LVL) $(BOARDDEF) $(CC_FLAGS) 
LD_FLAGS = --first __main --entry __main --diag_suppress=L6238 --map \
		   --list "$(TARGETNAME).map" \
		   --output "$(TARGETNAME).elf" \
		   --autoat --callgraph --symbols --list_mapping_symbols \
		   --info sizes,summarysizes,totals,unused \
		   --ro-base $(ROMCODESTART) --rw-base $(ROMDATASTART) 
ELF2BIN_LINE = $(elf2bin) --bin -o "$(TARGETNAME).bin" "$(TARGETNAME).elf" 

!else  				###### SDT ######

!if $(RELEASE)
#release flags
OPTIMIZE_LVL = -O1
!else
#debug flags
OPTIMIZE_LVL = -Od -Zi
!endif

CC_FLAGS = -W2 -nologo -Gy -use_msasm $(OPTIMIZE_LVL) $(BOARDDEF) $(CC_FLAGS)
A_FLAGS = $(A_FLAGS) $(PLAT_AFLAGS) -g -arm -PD "SDT SETA 1"
LD_FLAGS = -remove -noinfo -debug -Init DATA \
			-output "$(TARGETNAME).elf"  \
			-listing="$(TARGETNAME).map" \
			-bm ROMCODESTART=$(ROMCODESTART) -bm ROMDATASTART=$(ROMDATASTART) \
			-bf="$(BASE)\Build\BootLoader.bld" \
			"$(SDT)\marvellpxa\lib\x0__ac30.a"

ELF2BIN_LINE = $(elf2bin) -map CODE=0x0 -keeponly CODE "$(TARGETNAME).elf" -bin "$(TARGETNAME).bin"

!endif ####################### End tool type.


#############################################################
#
# Platform Object Definitions
#
#############################################################
#MMC
SDHCOBJS = 	$(OUTDIR)\sdhc2_controller.o \
			$(OUTDIR)\sdhc2.o
SDMMCOBJS = $(OUTDIR)\sdmmc_api.o

#ONENAND
ONENANDOBJS = $(OUTDIR)\FlexOneNAND_Cmd.o \
              $(OUTDIR)\OneNAND_Cmd.o \
              $(OUTDIR)\OneNAND_Driver.o

NANDOBJS = 	$(OUTDIR)\xllp_dfc.o \
           	$(OUTDIR)\xllp_dfc_support.o \
			$(OUTDIR)\nand.o

DMAOBJS = 	$(OUTDIR)\dma.o

FLASHAPIOBJS = 	$(OUTDIR)\flash.o \
				$(OUTDIR)\fm.o

XIPOBJS = $(OUTDIR)\xip.o

SPIOBJS = $(OUTDIR)\SPIFlash.o

DOWNLOADOBJS = $(OUTDIR)\ProtocolManager.o \
      	$(OUTDIR)\UartDownload.o \
      	$(OUTDIR)\uart.o \
      	$(OUTDIR)\usb_descriptors.o \
        $(OUTDIR)\CI2Download.o \
        $(OUTDIR)\CI2Driver.o

MAINOBJS = $(OUTDIR)\OBM_3_1.o

TIMOBJS =  $(OUTDIR)\tim.o

MISCOBJS = $(OUTDIR)\misc.o \
		$(OUTDIR)\keypad.o \
		$(OUTDIR)\xllp_clkmgr.o \
		$(OUTDIR)\ipc.o \
		$(OUTDIR)\timer.o \
!if $(NEW_DDRCNFG)
        $(OUTDIR)\RegInstructions.o \
!endif       

I2COBJS = $(OUTDIR)\I2C.o

SDRAMOBJS = $(OUTDIR)\sdram_support.o \
            $(OUTDIR)\DDR_Cfg.o

!if $(TRUSTED)
SECUREOBJS = $(OUTDIR)\bl_security.o \
			 $(OUTDIR)\bl_security_if.o \
			 $(OUTDIR)\bl_provisioning_if.o \
!if $(BL_USE_IPPCP_CRYPTO)
			$(OUTDIR)\ippcp_security.o \
!endif
!if $(BL_USE_GEU_FUSE_PROG)
			$(OUTDIR)\geu_provisioning_impl.o \
!endif
!if $(BL_USE_WTM_CRYPTO) || $(BL_USE_WTM_FUSE_PROG)
			$(OUTDIR)\linked_dtd_list.o \
			$(OUTDIR)\wtm_mbox_impl.o \
!endif
            $(OUTDIR)\PlatformJtag.o
!endif

PLATFORMOBJS = $(OUTDIR)\PlatformConfig.o	\
           $(OUTDIR)\sdram_config.o \
           $(OUTDIR)\freq_config.o \
           $(OUTDIR)\platform_interrupts.o

ASSEMBLYOBJS = $(OUTDIR)\bl_StartUp_$(TECHNOLOGY).o \
           $(OUTDIR)\platform_arch.o

#####################################################
################ OBJECT LIST ####################
#
# Note: The list below is where to capture what files
#		are to be built into a particular build
#		
#####################################################
OBJ = $(ASSEMBLYOBJS)\
!if $(TRUSTED)
      $(SECUREOBJS) \
!endif
      $(FLASHAPIOBJS) \
!if $(NOR)
      $(XIPOBJS) \
!endif
!if $(NAND)
      $(NANDOBJS) \
!endif
!if $(I2C)
	  $(I2COBJS) \
!endif
	  $(MISCOBJS) \
	  $(DMAOBJS) \
	  $(SDRAMOBJS) \
	  $(TIMOBJS) \
	  $(MAINOBJS) \
	  $(DOWNLOADOBJS) \
	  $(PLATFORMOBJS) \
!if $(SPI)
      $(SPIOBJS) \
!endif
!if $(ONENAND)
	  $(ONENANDOBJS) \
!endif
!if $(MMC)
	  $(SDMMCOBJS) \
	  $(SDHCOBJS) \
!endif
