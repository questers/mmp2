
/*--------------------------------------------------------------------------------------------------------------------
(C) Copyright 2006, 2007 Marvell DSPC Ltd. All Rights Reserved.
-------------------------------------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------------------------------------------------
INTEL CONFIDENTIAL
Copyright 2006 Intel Corporation All Rights Reserved.
The source code contained or described herein and all documents related to the source code (�Material�) are owned
by Intel Corporation or its suppliers or licensors. Title to the Material remains with Intel Corporation or
its suppliers and licensors. The Material contains trade secrets and proprietary and confidential information of
Intel or its suppliers and licensors. The Material is protected by worldwide copyright and trade secret laws and
treaty provisions. No part of the Material may be used, copied, reproduced, modified, published, uploaded, posted,
transmitted, distributed, or disclosed in any way without Intel�s prior express written permission.

No license under any patent, copyright, trade secret or other intellectual property right is granted to or
conferred upon you by disclosure or delivery of the Materials, either expressly, by implication, inducement,
estoppel or otherwise. Any license under such intellectual property rights must be express and approved by
Intel in writing.
-------------------------------------------------------------------------------------------------------------------*/

/*********************************************************************
*                      M O D U L E     B O D Y                       *
**********************************************************************
* Title: I2C package                                                 *
*                                                                    *
* Filename: I2C.c                                                    *
*                                                                    *
* Target, platform: Common Platform, HW platform                     *
*                                                                    *
* Description:                                                       *
*                                                                    *
* Notes:                                                             *
**********************************************************************/


#include "Typedef.h"
#include "I2C.h"
#include "platform_i2c.h"
#include "PlatformConfig.h"

/***************************   macros   *******************************************/

#define NULL        				0

static I2C_ReceiveRequestParams    _receiveReqParams;

/*********************************************************************************************/


#define LOOP_COUNTER_LIMIT              4000L



/* MASTERS_COLLISION_SILICON_BUG_WORKAROUND*/
/* In 2 chip there was a silicon I2c issue which cause the I2c bus to get stuck
 * The define bellow is a SW W/A for that.
 * For Tavor this should be modify becuase CP14 is not exist on Habell - !!! does not work for Harbell !!!:
 * CP14 - time stamp taken should be replace by time stamp from other free-running timer
 *         resolution should be in 1usec or less, TIME_TO_SAMPLE & CONVERT_TO_MICRO_SEC should be modify.*/

#if defined(_TAVOR_BOERNE_)
   #if !defined(_TAVOR_B0_EARLY_BUS_BUSY_NEW_BIT_)
       #define MASTERS_COLLISION_SILICON_BUG_WORKAROUND
   #endif
#else
	//No need for single chip platfrom , this is only for existing of 2 MASTERS on the I2C bus
#endif




static unsigned long			_repeat_start;
static unsigned long 			_firstByteToReceive;
static volatile unsigned long   _error_was_detected = FALSE;
static volatile unsigned long   _arbitration_loss_was_detected = FALSE;


/************************************************************************
* Function: I2CResetUnit
*************************************************************************
* Description:
*
* Parameters:
*
* Return value:
*
* Notes:
************************************************************************/
static void I2CResetUnit ( I2C_CONTEXT_T *context )
{
    UINT32_T  		ICRRegValue;

    // Reset the RX parameter
    _firstByteToReceive = TRUE;

	// Read the control register
	I2C_REG_READ(context,I2C_ICR_REG,ICRRegValue);

	ICRRegValue &= ~I2C_ICR_MASTER_ABORT;        // Clear the abort bit
  	ICRRegValue &= ~I2C_ICR_START;               // Clear the start bit
  	ICRRegValue &= ~I2C_ICR_STOP;                // Clear the stop bit
	ICRRegValue &= ~I2C_ICR_ACK_NACK_CONTROL;    // Clear the ACK bit
  	ICRRegValue &= ~I2C_ICR_TRANSFER_BYTE;       // Clear the transfer_byte bit

#ifdef MASTERS_COLLISION_SILICON_BUG_WORKAROUND
	ICRRegValue &= ~I2C_ICR_UNIT_ENABLE;         // Clear the unit enable
	ICRRegValue &= ~I2C_ICR_SCLEA_ENABLE;        // Clear the SCL enable
#endif
    // clear the ICR register
    I2C_REG_WRITE(context,I2C_ICR_REG, I2C_REG_CLEAR_VALUE);

    // Set the RESET bit in the ICR rewgister
    I2C_REG_BIT_WRITE(context, I2C_ICR_REG, I2C_ICR_UNIT_RESET_BIT, I2C_BIT_SET);

    // clear the ISR register after reset
    I2C_REG_WRITE(context,I2C_ISR_REG, I2C_ISR_CLEAR_ALL_INTERRUPTS);
	{
		unsigned long	temp;
		I2C_REG_READ(context, I2C_ISR_REG, temp);	// read back to force write completion.
	}
    // Clear the RESET bit in the ICR rewgister
    I2C_REG_BIT_WRITE(context, I2C_ICR_REG, I2C_ICR_UNIT_RESET_BIT, I2C_BIT_CLEAR);

    // Restore the ICR register
    I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue);

}

/************************************************************************
* Function: I2CConfigure
*************************************************************************
* Description:
*
* Parameters:
*
* Return value:
*
* Notes:
************************************************************************/
I2C_ReturnCode I2CConfigure( I2C_CONTEXT_T *context )
{
    	UINT32 			controlRegValue = I2C_ICR_REG_INIT_VALUE;

		// reset the I2C unit
    	I2CResetUnit(context);

#if TAVOR_BOERNE
    	controlRegValue |= FAST_MODE_ENABLE;
#endif

    	I2C_REG_WRITE(context,I2C_ICR_REG, controlRegValue);

        I2C_REG_WRITE(context,I2C_ISAR_REG, context->MySlaveAddress);

    	return I2C_RC_OK;

}

I2C_ReturnCode I2CInit( 
	I2C_CONTEXT_T			*pI2CContext,
	unsigned long			I2CRegsBase,
	volatile unsigned long	*MfprSCLAddr,
	volatile unsigned long	*MfprSDAAddr,
	unsigned long			MfprSCLVal,
	unsigned long			MfprSDAVal,
	unsigned long			SlaveAddress
	)
{
	pI2CContext->I2CRegsBase	= I2CRegsBase;
	pI2CContext->MySlaveAddress	= SlaveAddress;

	PlatformI2CClocksEnable();

	// mfpr pad setup: get current values,
	// then set mfpr to select the i2c function.
	pI2CContext->MfprSCLorigVal = *MfprSCLAddr;
	pI2CContext->MfprSDAorigVal = *MfprSDAAddr;
	*MfprSCLAddr = MfprSCLVal;
	*MfprSDAAddr = MfprSDAVal;

	I2CConfigure(pI2CContext);

	return I2C_RC_OK;
}

/************************************************************************
* Function: i2cWaitStatusDirect
*************************************************************************
* Description:  - waits for specific status register value
*               - time is limited
*               - breaks when _error_was_detected indicates error detected by interrupt
*               - breaks when _arbitration_loss_was_detected indicates error detected by interrupt
*
* Parameters:   bitsSet     contains '1' where 1 is expected
*               bitsCleared contains '1' where 0 is expected
* Return value: I2C_RC_OK if got it or error code otherwise
*
* Notes:
************************************************************************/
static I2C_ReturnCode i2cWaitStatusDirect ( I2C_CONTEXT_T *context, UINT32_T bitsSet, UINT32_T bitsCleared )
{


	UINT32_T 		statusRegValue;
    UINT32_T 		countLimit = LOOP_COUNTER_LIMIT;
	UINT32_T 		mask = bitsSet|bitsCleared;    // all bits we care of
	UINT32_T 		value = bitsSet;

	do
    {
        I2C_REG_READ(context,I2C_ISR_REG,statusRegValue);

		if ( I2C_REG_BIT_READ(statusRegValue, I2C_ISR_BUS_ERROR_DETECTED_BIT) )
		{
			// clear 'Bus Error Detected' bit (write '1')
			I2C_STATUS_REG_CLEAR_BIT(context, I2C_ISR_BUS_ERROR_DETECTED_BIT);

			_error_was_detected = TRUE;
		}

		if ( I2C_REG_BIT_READ(statusRegValue, I2C_ISR_ARBITRATION_LOSS_DETECTED_BIT) )
		{
			// clear 'Arbitration Loss Detected' bit (write '1')
			I2C_STATUS_REG_CLEAR_BIT(context, I2C_ISR_ARBITRATION_LOSS_DETECTED_BIT);

			_arbitration_loss_was_detected = TRUE;
		}

		if ( statusRegValue & I2C_ISR_UNEXPECTED_INTERRUPTS )
		{
			// clear 'I2C_ISR_UNEXPECTED_INTERRUPTS ' bits (write '1')
			I2C_REG_WRITE(context,I2C_ISR_REG,I2C_ISR_UNEXPECTED_INTERRUPTS);
			{
				unsigned long	temp;
				I2C_REG_READ(context, I2C_ISR_REG, temp);	// read back to force write completion.
			}

		}

        countLimit--;
    }
    while ((( statusRegValue & mask ) != value ) && ( _error_was_detected == FALSE ) && (countLimit > 0) && (_arbitration_loss_was_detected == FALSE) );

    if ( _arbitration_loss_was_detected )
		return I2C_ISR_ARBITRATION_LOSS;

	if ( _error_was_detected )
        return I2C_ISR_BUS_ERROR;

	// check for timeout, reset the I2C device for next operations and return error if needed
    if ( countLimit == 0 )
    {
		UINT32_T  ICRRegValue = 0;

		I2C_REG_READ(context,I2C_ICR_REG,ICRRegValue);
		ICRRegValue |= I2C_ICR_MASTER_ABORT;
		ICRRegValue &= ~I2C_ICR_TRANSFER_BYTE;
    	I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue );
    	return I2C_RC_TIMEOUT_ERROR;
	}

	return I2C_RC_OK;
}

/************************************************************************
* Function: masterSendDirect
*************************************************************************
* Description: This function is used to write data to I2C slaves in the
* 			   boot stage only while the OS system and INTC are not up.
*
* Parameters:
*
* Return value:
*
* Notes:
************************************************************************/
I2C_ReturnCode masterSendDirect(
	I2C_CONTEXT_T	*context, 
	UINT8_T			*data, 
	UINT16_T		 dataSize, 
	UINT8_T			 slaveAddress, 
	unsigned long	 repeatedStart , 
	unsigned long	 MasterReceiveCalled 
	)
{
    UINT32_T  		ICRRegValue = 0;
    int 			i;
	I2C_ReturnCode 	I2CReturnCode = I2C_RC_OK;


	/* Check the bus for free - If there is an arbitration loss, stay until it become free */
    do
	{
        // check if 'Arbitration Loss' was detected
       	if ( I2CReturnCode == I2C_ISR_ARBITRATION_LOSS )
		{
             I2C_STATUS_REG_CLEAR_BIT(context, I2C_ISR_ARBITRATION_LOSS_DETECTED_BIT);


			 // Read the control register
			 I2C_REG_READ(context,I2C_ICR_REG,ICRRegValue);

		  	 ICRRegValue &= ~I2C_ICR_START;               // Clear the start bit
		  	 ICRRegValue &= ~I2C_ICR_TRANSFER_BYTE;       // Clear the transfer_byte bit

    		 // Write to the ICR register
    		 I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue);

			 for ( i = 0; i < 50000; i++ );           	/* A little delay to enable others master to finish without interruption */
														/* the bus arbiter, and reduce the arbitration sequences */
		}

    	// write IDBR register: target slave address and R/W# bit=0 for write transaction.
    	I2C_REG_WRITE(context, I2C_IDBR_REG, I2C_SLAVE_WRITE(slaveAddress) );

    	// write ICR register: set START bit, clear STOP bit, set Transfer Byte bit to initiate the access
        I2C_REG_READ(context,I2C_ICR_REG,ICRRegValue);
		ICRRegValue &= ~I2C_ICR_STOP;
		ICRRegValue |= (I2C_ICR_START | I2C_ICR_TRANSFER_BYTE);
    	I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue );
	    I2CReturnCode = i2cWaitStatusDirect ( context, (I2C_REG_BIT(I2C_ISR_IDBR_TRANSMIT_EMPTY_BIT) | I2C_REG_BIT(I2C_ISR_UNIT_BUSY_BIT)),
			    	                     I2C_REG_BIT(I2C_ISR_READ_WRITE_MODE_BIT)  );

    	// clear 'IDBR Transmit Empty' bit (write '1')
    	I2C_STATUS_REG_CLEAR_BIT(context, I2C_ISR_IDBR_TRANSMIT_EMPTY_BIT);

		if ( ( I2CReturnCode != I2C_ISR_ARBITRATION_LOSS ) && ( I2CReturnCode != I2C_RC_OK ))
		{
        	I2C_REG_READ(context,I2C_ICR_REG,ICRRegValue);		
			ICRRegValue &= 0xfff0;								 
   			I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue );	

			return I2CReturnCode;
		}
    }
    // check if 'Arbitration Loss' was detected
	while ( I2CReturnCode == I2C_ISR_ARBITRATION_LOSS );

	// at this point we own the bus. no other master can get legally the bus until we send a stop.
    // write ICR register: clear START bit, clear STOP bit
    I2C_REG_READ(context,I2C_ICR_REG,ICRRegValue);
  	ICRRegValue &= ~I2C_ICR_STOP;
  	ICRRegValue &= ~I2C_ICR_START;
   	I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue );

    /*** Send data bytes - all, except the last byte  ***/
    for ( i = 0; i < ( dataSize - 1 ); i++ )
    {
        // write data byte to the IDBR register
        I2C_REG_WRITE(context,I2C_IDBR_REG, *data);
        data++;

        // Set 'Tranfer Byte' bit to intiate the access
        I2C_REG_BIT_WRITE(context, I2C_ICR_REG, I2C_ICR_TRANSFER_BYTE_BIT, I2C_BIT_SET);
	    if((I2CReturnCode = i2cWaitStatusDirect(context, I2C_REG_BIT(I2C_ISR_IDBR_TRANSMIT_EMPTY_BIT)|I2C_REG_BIT(I2C_ISR_UNIT_BUSY_BIT),
				                          I2C_REG_BIT(I2C_ISR_READ_WRITE_MODE_BIT))) != I2C_RC_OK)
						return I2CReturnCode;

        // clear 'IDBR Transmit Empty' bit (write '1')
        I2C_STATUS_REG_CLEAR_BIT(context, I2C_ISR_IDBR_TRANSMIT_EMPTY_BIT);
    }


    /*** Send the last byte with STOP bit ***/

    // write the last data byte to the IDBR register
    I2C_REG_WRITE(context,I2C_IDBR_REG, *data);

    // write ICR: clear START bit															
    I2C_REG_BIT_WRITE(context, I2C_ICR_REG, I2C_ICR_START_BIT, I2C_BIT_CLEAR);				

	// the stop bit will release the bus. 
	// check if another transaction is planned for immediately 
	// following this one. if so, don't release the bus (eg. don't set the stop bit).
    if (( repeatedStart == FALSE ) || ( MasterReceiveCalled == FALSE ))
        I2C_REG_BIT_WRITE(context, I2C_ICR_REG, I2C_ICR_STOP_BIT, I2C_BIT_SET);

    I2C_REG_BIT_WRITE(context, I2C_ICR_REG, I2C_ICR_TRANSFER_BYTE_BIT, I2C_BIT_SET);

	if (repeatedStart)
	{
	  if ((I2CReturnCode = i2cWaitStatusDirect (context, I2C_REG_BIT(I2C_ISR_IDBR_TRANSMIT_EMPTY_BIT)|I2C_REG_BIT(I2C_ISR_UNIT_BUSY_BIT),
	  	    	                          I2C_REG_BIT(I2C_ISR_READ_WRITE_MODE_BIT)))!=I2C_RC_OK)
				return I2CReturnCode;
	}
    else
	{
	  if ((I2CReturnCode = i2cWaitStatusDirect (context, I2C_REG_BIT(I2C_ISR_IDBR_TRANSMIT_EMPTY_BIT),
	                                      I2C_REG_BIT(I2C_ISR_READ_WRITE_MODE_BIT)))!=I2C_RC_OK)
				return I2CReturnCode;
	}

	// clear 'IDBR Transmit Empty' bit (write '1')
    I2C_STATUS_REG_CLEAR_BIT(context, I2C_ISR_IDBR_TRANSMIT_EMPTY_BIT);
    I2C_REG_BIT_WRITE(context, I2C_ICR_REG, I2C_ICR_STOP_BIT, I2C_BIT_CLEAR);


    return I2C_RC_OK;
}


/************************************************************************/
/************************************************************************/
#define PMU_XCGR_READ	 			(( volatile UINT32_T *)(0x40f00824))
#define I2C_FUN_CLK_ADD       		(( volatile UINT32_T *)(0x40f00024))
#define I2C_APB_CLK_ADD				(( volatile UINT32_T *)(0x4200000c))
#define PMU_XCGR_EN_I2C_FUN_CLK		(0x40000)
#define I2C_APB_CLK_ENABLE			0x1
/************************************************************************
* Function: I2CBusStateGetDirect
*************************************************************************
* Description: This function is used to read the I2C Bus Monitor
* 			   Register - IBMR.
*
* Parameters:
*
* Return value:
*
* Notes:
************************************************************************/
void I2CBusStateGetDirect ( I2C_CONTEXT_T *context, I2C_IBMR   *bus_state )
{
		UINT32_T     statusRegValue , XCGR_val;


//		XCGR_val = *PMU_XCGR_READ;
//		*I2C_FUN_CLK_ADD = XCGR_val | PMU_XCGR_EN_I2C_FUN_CLK;              // I2c Functional Clock
//		*I2C_APB_CLK_ADD = I2C_APB_CLK_ENABLE;                              // I2c APB Clock

		I2C_REG_READ(context,I2C_IBMR_REG,statusRegValue);

		bus_state->sda = (statusRegValue & I2C_IBMR_SDA_STATUS_BIT);
		bus_state->scl = ((statusRegValue & I2C_IBMR_SCL_STATUS_BIT ) >> 1);

}
/************************************************************************
* Function: I2CMasterSendDataDirect
*************************************************************************
* Description: This function is used to write data to I2C slaves in the
* 			   boot stage only while the OS system and INTC are not up.
*
* Parameters:
*
* Return value:
*
* Notes:
************************************************************************/
I2C_ReturnCode I2CMasterSendDataDirect( I2C_CONTEXT_T *context, UINT8_T *data , UINT16_T dataSize , UINT8_T slaveAddress )
{
    UINT32_T 			XCGR_val;
    I2C_ReturnCode 		I2CReturnCode;
    unsigned long		chipBusy;



//	XCGR_val = *PMU_XCGR_READ;
//	*I2C_FUN_CLK_ADD = XCGR_val | PMU_XCGR_EN_I2C_FUN_CLK;              // I2c Functional Clock
//	*I2C_APB_CLK_ADD = I2C_APB_CLK_ENABLE;                              // I2c APB Clock

	// check if the chip is busy.
	// if back-to-back transactions are performed,
	// this routine may find the chip still busy while
	// the chip is wrapping up the previous transaction.
	// if a wait-for-not-busy were added to the end of
	// the read or write functions, then this routine
	// would probably never find the chip busy at this point.
	{
		int	count;

		for(count=0;count<100;count++)
		{
			CHECK_IF_CHIP_BUSY(context,chipBusy);
			if(!chipBusy) 
				break;
		}
		if( chipBusy )
			return I2C_RC_CHIP_BUSY;
	}


    if ( CHECK_IF_GENERAL_CALL(slaveAddress) )
		return I2C_RC_INVALID_GENERAL_CALL_SLAVE_ADDRESS;

    I2CReturnCode = masterSendDirect(context, data, dataSize, slaveAddress, _repeat_start , FALSE );

    return I2CReturnCode;
}
/************************************************************************/
/************************************************************************/
UINT16_T I2CIntLISRDirect ( I2C_CONTEXT_T *context )
{
    UINT32_T 			statusRegValue , ICRRegValue = 0;



    //check interrupt source
    I2C_REG_READ(context,I2C_ISR_REG,statusRegValue);

    if ( I2C_REG_BIT_READ(statusRegValue, I2C_ISR_IDBR_RECEIVE_FULL_BIT) )
    {
        // clear 'IDBR Receive Full' bit (write '1')
        I2C_STATUS_REG_CLEAR_BIT(context, I2C_ISR_IDBR_RECEIVE_FULL_BIT);
        {
            if ( _firstByteToReceive == TRUE )
            {
            	_firstByteToReceive = FALSE;
            }

            // read the received byte
            I2C_REG_READ(context,I2C_IDBR_REG,*_receiveReqParams.RxBufferPtr);
            _receiveReqParams.RxBufferPtr++;
            _receiveReqParams.dataSize--;

            // write ICR: clear STOP bit(1), clear ACK/NACK bit(2)
    		I2C_REG_READ(context,I2C_ICR_REG,ICRRegValue);
			ICRRegValue &= ~I2C_ICR_STOP;
			ICRRegValue &= ~I2C_ICR_ACK_NACK_CONTROL;
    		I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue );


            if ( _receiveReqParams.dataSize > 1 )
            {
            	// write ICR: clear START bit, clear STOP bit, send ACK bit (0 for ACK),
				//             set Transfer Byte bit to initiate the access
    			I2C_REG_READ(context,I2C_ICR_REG,ICRRegValue);
				ICRRegValue &= ~I2C_ICR_START;					
				ICRRegValue &= ~I2C_ICR_STOP;					
				ICRRegValue &= ~I2C_ICR_ACK_NACK_CONTROL;		// this means: ACK the byte. required when receiving not last byte.
				ICRRegValue |= I2C_ICR_TRANSFER_BYTE;			// this means: get the next byte.
    			I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue );
            }
            else if ( _receiveReqParams.dataSize == 1 ) //last byte remaing to read (with STOP signal)
            {
            	// write ICR: clear START bit, set STOP bit, send NACK bit (1 for NACK),
				//            set Transfer Byte bit to initiate the access
    			I2C_REG_READ(context,I2C_ICR_REG,ICRRegValue);
				ICRRegValue &= ~I2C_ICR_START;					
				ICRRegValue |= I2C_ICR_STOP;					
				ICRRegValue |= I2C_ICR_ACK_NACK_CONTROL;		// this means: ?
				ICRRegValue |= I2C_ICR_TRANSFER_BYTE;			// this means: do the stop transaction on the bus.
    			I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue );
            }
            else
            {
        		// disable 'IDBR Buffer Full' interrupt
        		I2C_REG_BIT_WRITE(context, I2C_ICR_REG, I2C_ICR_IDBR_RECEIVE_FULL_INT_ENABLE_BIT, I2C_BIT_CLEAR);
            	_firstByteToReceive = TRUE;
            }
        }
    }

    if ( I2C_REG_BIT_READ(statusRegValue, I2C_ISR_BUS_ERROR_DETECTED_BIT) )
    {
        // clear 'Bus Error Detected' bit (write '1')
        I2C_STATUS_REG_CLEAR_BIT(context, I2C_ISR_BUS_ERROR_DETECTED_BIT);

        _error_was_detected = TRUE;
    }

    if ( I2C_REG_BIT_READ(statusRegValue, I2C_ISR_ARBITRATION_LOSS_DETECTED_BIT) )
    {
        // clear 'Arbitration Loss Detected' bit (write '1')
        I2C_STATUS_REG_CLEAR_BIT(context, I2C_ISR_ARBITRATION_LOSS_DETECTED_BIT);

        _arbitration_loss_was_detected = TRUE;
    }

    if ( statusRegValue & I2C_ISR_UNEXPECTED_INTERRUPTS )
    {
        // clear 'I2C_ISR_UNEXPECTED_INTERRUPTS ' bits (write '1')
        I2C_REG_WRITE(context,I2C_ISR_REG,I2C_ISR_UNEXPECTED_INTERRUPTS);
		{
			unsigned long	temp;
			I2C_REG_READ(context, I2C_ISR_REG, temp);	// read back to force write completion.
		}

    }

	return _receiveReqParams.dataSize;

}
/************************************************************************
* Function: I2CMasterReceiveDataDirect
*************************************************************************
* Description: This function is used to read data from I2C slaves in the
* 			   boot stage only while the OS system and INTC are not up.
*
* Parameters:
*
* Return value:
*
* Notes:
************************************************************************/
I2C_ReturnCode I2CMasterReceiveDataDirect ( I2C_CONTEXT_T						   *context,
										    UINT8_T 						   *cmd,
											UINT16_T 	 						cmdLength,
											UINT8_T 							writeSlaveAddress,
		                                    UINT8_T 						   *designatedRxBufferPtr,
											UINT16_T 	 						dataSize,
											UINT8_T 							readSlaveAddress )
{
    UINT32_T 			XCGR_val;
    I2C_ReturnCode 		I2CReturnCode;
    UINT32_T 			ICRRegValue = 0;
    unsigned long		chipBusy;



    // store the request parameters (to be used from the ISR and by the I2C task when error)
    _receiveReqParams.RxBufferPtr = designatedRxBufferPtr;
    _receiveReqParams.dataSize = dataSize;


    if ( dataSize == 0 )
        return I2C_RC_INVALID_DATA_SIZE;

//	XCGR_val = *PMU_XCGR_READ;
//	*I2C_FUN_CLK_ADD = XCGR_val | PMU_XCGR_EN_I2C_FUN_CLK;              // I2c Functional Clock
//	*I2C_APB_CLK_ADD = I2C_APB_CLK_ENABLE;                              // I2c APB Clock

    if ( CHECK_IF_GENERAL_CALL(writeSlaveAddress) )
		return I2C_RC_INVALID_GENERAL_CALL_SLAVE_ADDRESS;

    if ( CHECK_IF_GENERAL_CALL(readSlaveAddress) )
		return I2C_RC_INVALID_GENERAL_CALL_SLAVE_ADDRESS;

	// check if the chip is busy.
	// if back-to-back transactions are performed,
	// this routine may find the chip still busy while
	// the chip is wrapping up the previous transaction.
	// if a wait-for-not-busy were added to the end of
	// the read or write functions, then this routine
	// would probably never find the chip busy at this point.
	{
		int	count;

		for(count=0;count<100;count++)
		{
			CHECK_IF_CHIP_BUSY(context,chipBusy);
			if(!chipBusy) 
				break;
		}
		if( chipBusy )
			return I2C_RC_CHIP_BUSY;
	}

    if ( (cmdLength != 0) && (cmd != NULL) )  /* if command is associated with the received request */
    {
        I2CReturnCode = masterSendDirect(context, cmd, cmdLength, writeSlaveAddress, 1/*_repeat_start*/, TRUE);	// bpc: use repeat start mode

        if( I2CReturnCode != I2C_RC_OK )
            return I2CReturnCode;
    }

    /*** send Read Request ***/
    // write IDBR: target slave address and R/W# bit (1 for read)
    I2C_REG_WRITE(context,I2C_IDBR_REG, I2C_SLAVE_READ(readSlaveAddress) );

    // write ICR: set START bit, clear STOP bit, set Transfer Byte bit to initiate the access
    I2C_REG_READ(context,I2C_ICR_REG,ICRRegValue);
	ICRRegValue &= ~I2C_ICR_STOP;
	ICRRegValue |= (I2C_ICR_START | I2C_ICR_TRANSFER_BYTE);	
    I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue );

	// bpc: check if these are the correct status flags.
	I2CReturnCode = i2cWaitStatusDirect(context, I2C_REG_BIT(I2C_ISR_IDBR_TRANSMIT_EMPTY_BIT)|I2C_REG_BIT(I2C_ISR_UNIT_BUSY_BIT)|I2C_REG_BIT(I2C_ISR_READ_WRITE_MODE_BIT), 0 );

    // clear 'IDBR Transmit Empty' bit (write '1')
    I2C_STATUS_REG_CLEAR_BIT(context, I2C_ISR_IDBR_TRANSMIT_EMPTY_BIT);

	if ( I2CReturnCode != I2C_RC_OK )
        return I2CReturnCode;


    // Initiate the read process:

    if ( dataSize == 1 )   // only one byte to read
    {
        // write ICR: set STOP bit, set ACK/NACK bit (1 for NACK)
        I2C_REG_READ(context,I2C_ICR_REG,ICRRegValue);
  		ICRRegValue &= ~I2C_ICR_START;
  	    ICRRegValue |= (I2C_ICR_STOP | I2C_ICR_ACK_NACK_CONTROL);
   	    I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue );
    }
    else   // more than one byte to read
    {
        // write ICR: clear STOP bit, clear ACK/NACK bit (0 for ACK)
    	I2C_REG_READ(context, I2C_ICR_REG,ICRRegValue);
		ICRRegValue &= ~I2C_ICR_STOP;
		ICRRegValue &= ~I2C_ICR_ACK_NACK_CONTROL;
    	I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue );
    }

    I2C_REG_READ(context,I2C_ICR_REG,ICRRegValue);
  	ICRRegValue &= ~I2C_ICR_START;
  	ICRRegValue |= I2C_ICR_TRANSFER_BYTE;
   	I2C_REG_WRITE(context,I2C_ICR_REG, ICRRegValue );

	while ( I2CIntLISRDirect(context) );	// polled mode read...

    return I2C_RC_OK;
}
/***************************************************************************/
/***************************************************************************/
