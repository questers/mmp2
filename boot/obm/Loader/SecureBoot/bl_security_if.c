/****************************************************************************
 *
 *  (C)Copyright 2005 - 2010 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code. This Module contains Proprietary
 *  Information of Marvell and should be treated as Confidential. The
 *  information in this file is provided for the exclusive use of the
 *  licensees of Marvell. Such users have the right to use, modify, and
 *  incorporate this code into products for purposes authorized by the
 *  license agreement provided they include this notice and the associated
 *  copyright notice with any such product.
 *
 *  The information in this file is provided "AS IS" without warranty.
 *
 ***************************************************************************/

#include "bl_security_if.h"
#include "bl_provisioning_if.h"
#include "Errors.h"
#if BL_USE_IPPCP_CRYPTO
#include "ippcp_security.h"
#endif
#if BL_USE_GEU_FUSE_PROG
#include "geu_provisioning_impl.h"
#endif
#if BL_USE_WTM_CRYPTO
#include "wtm_mbox_impl.h"
#endif

bl_security_funcs bl_security_funcs_api;


/*
 * following functions are non-primitive in nature, therefore they are implemented here
 */
static UINT_T perform_platbind(pTIM ptim, void* param);
static UINT_T perform_autoconf(pTIM ptim, void* param);


/**
 * Initialize boot loader security interface
 */
UINT_T bl_security_api_init(void* adv_ver)
{
    UINT_T wtm_timeout_in_ms = 500;
    UINT_T wtm_perform_version_advance = *((UINT_T*)adv_ver);
#if BL_USE_IPPCP_CRYPTO
    bl_security_funcs_api.compute_digest = (func_compute_digest)IPPCP_SHAMessageDigest;
    bl_security_funcs_api.validate_tim   = IPPCP_ValidateTIM;
    bl_security_funcs_api.validate_image = ippcp_validate_image;
#endif

#if BL_USE_WTM_CRYPTO
    wtm3_funcs_init();
    wtm3_crypto_funcs_impl.host_interface_init(wtm_timeout_in_ms, wtm_perform_version_advance);
    bl_security_funcs_api.compute_digest	= wtm3_crypto_funcs_impl.compute_digest;
	bl_security_funcs_api.verify_platform	= wtm3_crypto_funcs_impl.platform_authentication;
	bl_security_funcs_api.verify_jtag_key	= wtm3_crypto_funcs_impl.verify_jtag_key;
    bl_security_funcs_api.validate_tim		= wtm3_crypto_funcs_impl.tim_validation;
    bl_security_funcs_api.validate_image	= (func_validate_image)wtm3_crypto_funcs_impl.image_validation;
	bl_security_funcs_api.read_profile_fuse	= wtm3_crypto_funcs_impl.read_profile_fuse;
#endif
#if BL_USE_GEU_FUSE_PROG
    bl_provisioning_funcs_api.fuse_conf_spec        = geu_provisioning_funcs_impl.fuse_conf_spec;
    bl_provisioning_funcs_api.bind_platform         = geu_provisioning_funcs_impl.bind_platform;
    bl_provisioning_funcs_api.platform_binded       = geu_provisioning_funcs_impl.platform_binded;
    bl_provisioning_funcs_api.config_platform       = geu_provisioning_funcs_impl.config_platform;
    bl_provisioning_funcs_api.read_platform_config  = geu_provisioning_funcs_impl.read_platform_config;
    bl_provisioning_funcs_api.program_jtag_key      = geu_provisioning_funcs_impl.program_jtag_key;
    bl_provisioning_funcs_api.read_jtag_key         = geu_provisioning_funcs_impl.read_jtag_key;
    bl_provisioning_funcs_api.program_usb_id        = geu_provisioning_funcs_impl.program_usb_id;
    bl_provisioning_funcs_api.read_usb_id           = geu_provisioning_funcs_impl.read_usb_id;
    bl_provisioning_funcs_api.program_oem_unique_id = geu_provisioning_funcs_impl.program_oem_unique_id;
    bl_provisioning_funcs_api.read_oem_unique_id    = geu_provisioning_funcs_impl.read_oem_unique_id;
    bl_provisioning_funcs_api.advance_life_cycle    = geu_provisioning_funcs_impl.advance_life_cycle;
    bl_provisioning_funcs_api.read_life_cycle       = geu_provisioning_funcs_impl.read_life_cycle;
    bl_provisioning_funcs_api.advance_version       = geu_provisioning_funcs_impl.advance_version;
    bl_provisioning_funcs_api.read_version          = geu_provisioning_funcs_impl.read_version;
    bl_provisioning_funcs_api.autocfg_platform      = geu_provisioning_funcs_impl.autocfg_platform;
    bl_provisioning_funcs_api.setup_aib_override    = geu_provisioning_funcs_impl.setup_aib_override;
#endif
#if BL_USE_WTM_FUSE_PROG
    bl_provisioning_funcs_api.fuse_conf_spec        = wtm3_provisioning_funcs_impl.fuse_conf_spec;
    bl_provisioning_funcs_api.bind_platform         = wtm3_provisioning_funcs_impl.bind_platform;
    bl_provisioning_funcs_api.platform_binded       = wtm3_provisioning_funcs_impl.platform_binded;
    bl_provisioning_funcs_api.config_platform       = wtm3_provisioning_funcs_impl.config_platform;
    bl_provisioning_funcs_api.read_platform_config  = wtm3_provisioning_funcs_impl.read_platform_config;
    bl_provisioning_funcs_api.program_jtag_key      = wtm3_provisioning_funcs_impl.program_jtag_key;
    bl_provisioning_funcs_api.read_jtag_key         = wtm3_provisioning_funcs_impl.read_jtag_key;
    bl_provisioning_funcs_api.program_usb_id        = wtm3_provisioning_funcs_impl.program_usb_id;
    bl_provisioning_funcs_api.read_usb_id           = wtm3_provisioning_funcs_impl.read_usb_id;
    bl_provisioning_funcs_api.read_oem_unique_id    = wtm3_provisioning_funcs_impl.read_oem_unique_id;
    bl_provisioning_funcs_api.advance_life_cycle    = wtm3_provisioning_funcs_impl.advance_life_cycle;
    bl_provisioning_funcs_api.read_life_cycle       = wtm3_provisioning_funcs_impl.read_life_cycle;
    bl_provisioning_funcs_api.advance_version       = wtm3_provisioning_funcs_impl.advance_version;
    bl_provisioning_funcs_api.read_version          = wtm3_provisioning_funcs_impl.read_version;
    bl_provisioning_funcs_api.autocfg_platform      = wtm3_provisioning_funcs_impl.autocfg_platform;
    bl_provisioning_funcs_api.program_rkek      	= wtm3_provisioning_funcs_impl.program_rkek;
    bl_provisioning_funcs_api.program_ec521_dk     	= wtm3_provisioning_funcs_impl.program_ec521_dk;
#endif

    bl_security_funcs_api.perform_platbind = perform_platbind;
    bl_security_funcs_api.perform_autoconf = perform_autoconf;

    return NoError;
};


/**
 * Shutdown boot loader security interface
 */
UINT_T bl_security_api_shutdown(void* para)
{
#if BL_USE_WTM_CRYPTO
    wtm3_crypto_funcs_impl.host_interface_shutdown(para);
#endif
    return NoError;
}


/**
 * perform platform binding
 */
static UINT_T perform_platbind(pTIM ptim, void* param)
{
    UINT_T result;
    result = bl_provisioning_funcs_api.bind_platform(ptim, param);
    return result;
}


/**
 * perform platform configuration
 */
static UINT_T perform_autoconf(pTIM ptim, void* param)
{
    UINT_T result;
    result = bl_provisioning_funcs_api.autocfg_platform(ptim, param);
    return result;
}

