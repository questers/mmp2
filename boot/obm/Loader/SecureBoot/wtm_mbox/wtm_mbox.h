////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//         COPYRIGHT 2010 MARVELL INTERNATIONAL LTD. AND ITS AFFILIATES       //
//                          ALL RIGHTS RESERVED                               //
//                                                                            //
//     The source code for this computer program is CONFIDENTIAL and a        //
//     TRADE SECRET of MARVELL INTERNATIONAL LTD. AND ITS AFFILIATES          //
//     ('MARVELL'). The receipt or possession of this program does not        //
//     convey any rights to reproduce or disclose its contents, or to         //
//     manufacture, use, or sell anything that it may describe, in            //
//     whole or in part, without the specific written consent of MARVELL.     //
//     Any reproduction or distribution of this program without the           //
//     express written consent of MARVELL is a violation of the copyright     //
//     laws and may subject you to criminal prosecution.                      //
//                                                                            //
//////////////////////////////////////////////////////////////////////////////// 


#ifndef INCLUDE_WTM_H
#define INCLUDE_WTM_H

#define MAX_SW_VERSION_NUMBER      15

// absolute maximums; all other aspects will scale from these
//
#define WTM_PRIMITIVE_CATEGORY     13
#define WTM_PRIMITIVE_TOTAL        (10+7+15+4+4+5+8+3+12+8+16+16+5)

typedef unsigned int 			U32;


////////////////////////////////////////////////////////////////////////////////
//
// Instruction-ID: 4-bit
//
//     0           0          00
//     -           -          --
//     Category    Reserved   id
//
// 11 Categories and each can have up to 16 PIs.
//
typedef enum _PRIMITIVE
{
    //System Setup: 10 PIs
    WTM_RESET                          = 0x0000,
    WTM_INIT                           = 0x0001,
    WTM_SELF_TEST                      = 0x0002,
    WTM_SLEEP                          = 0x0003,
    WTM_CONFIGURE_DMA                  = 0x0004,
    WTM_SET_BATCH_COUNT                = 0x0005,
    WTM_ACK_BATCH_ERROR                = 0x0006,
    WTM_RESUME                         = 0x0007,
    WTM_JTAG_START                     = 0x0008,
    WTM_JTAG_AUTHORIZATION             = 0x0009,

    //State Management: 7 PIs
    WTM_GET_TRUST_STATUS_REGISTER      = 0x1000,
    WTM_LIFECYCLE_ADVANCE              = 0x1001,
    WTM_LIFECYCLE_READ                 = 0x1002,
    WTM_SOFTWARE_VERSION_ADVANCE       = 0x1003,
    WTM_SOFTWARE_VERSION_READ          = 0x1004,
    WTM_KERNEL_VERSION_READ            = 0x1005,
    WTM_GET_SOC_POWER_POINT            = 0x1006,

    //Platform Provision: 15 PIs
    WTM_RKEK_PROVISION                 = 0x2000,
    WTM_EC521_DK_PROVISION             = 0x2001,
    WTM_OEM_PLATFORM_BIND              = 0x2002,
    WTM_OEM_PLATFORM_VERIFY            = 0x2003,
    WTM_OEM_JTAG_KEY_BIND              = 0x2004,
    WTM_OEM_JTAG_KEY_VERIFY            = 0x2005,
    WTM_SET_JTAG_PERMANENT_DISABLE     = 0x2006,
    WTM_SET_TEMPORARY_FA_DISABLE       = 0x2007,
    WTM_SOC_UNIQUE_ID_READ             = 0x2008,
    WTM_OTP_WRITE_PLATFORM_CONFIG      = 0x2009,
    WTM_OTP_READ_PLATFORM_CONFIG       = 0x200A,
    WTM_OEM_USBID_PROVISION            = 0x200B,
    WTM_OEM_USBID_READ                 = 0x200C,
    WTM_HDCP_WRAP_KEY                  = 0x200D,
    WTM_HDCP_LOAD_KEY                  = 0x200E,

    //Sliding Window: 4 PIs
    WTM_SET_TIM_R                      = 0x3000,
    WTM_LAUNCH_IMG_SECURITY_CHECK      = 0x3001,
    WTM_HALT_IMG_SECURITY_CHECK        = 0x3002,
    WTM_RESUME_IMG_SECURITY_CHECK      = 0x3003,

    //FIPS Mode Management: 4 PIs
    WTM_SWITCH_FIPS_MODE               = 0x4000,
    WTM_SET_FIPS_MODE_PERMANENT        = 0x4001,
    WTM_SET_NONFIPS_MODE_PERMANENT     = 0x4002,
	WTM_SET_NONFIPS_MODE_UNTIL_RESET   = 0x4003,

    //Key Management: 5 PIs
    WTM_ENROLL_KEY                     = 0x5000,
    WTM_CREATE_KEY                     = 0x5001,
    WTM_LOAD_KEY                       = 0x5002,
    WTM_OIAP_START                     = 0x5003,
    WTM_CREATE_RSA_KEYPAIR             = 0x5004,

    //Context Cache Management: 8 PIs
    WTM_GET_CONTEXT_INFO               = 0x6000,
    WTM_LOAD_ENGINE_CONTEXT            = 0x6001,
    WTM_STORE_ENGINE_CONTEXT           = 0x6002,
    WTM_WRAP_KEYCONTEXT                = 0x6003,
    WTM_UNWRAP_KEYCONTEXT              = 0x6004,
    WTM_LOAD_ENGINE_CONTEXT_EXTERNAL   = 0x6005,
    WTM_STORE_ENGINE_CONTEXT_EXTERNAL  = 0x6006,
    WTM_PURGE_CONTEXT                  = 0x6007,

    //HW-RNG: 3 PIs
    WTM_DRBG_INIT                      = 0x7000,
    WTM_DRBG_RESEED                    = 0x7001,
    WTM_DRBG_GEN_RAN_BITS              = 0x7002,

    //Symmetric Cipher: 12 PIs
    WTM_AES_INIT                       = 0x8000,
    WTM_AES_ZEROIZE                    = 0x8001,
    WTM_AES_PROCESS                    = 0x8002,
    WTM_AES_FINISH                     = 0x8003,
    WTM_DES_INIT                       = 0x8004,
    WTM_DES_ZEROIZE                    = 0x8005,
    WTM_DES_PROCESS                    = 0x8006,
    WTM_DES_FINISH                     = 0x8007,
    WTM_RC4_INIT                       = 0x8008,
    WTM_RC4_ZEROIZE                    = 0x8009,
    WTM_RC4_PROCESS                    = 0x800A,
    WTM_RC4_FINISH                     = 0x800B,

    //HASH/HMAC: 8 PIs
    WTM_HASH_INIT                      = 0x9000,
    WTM_HASH_ZEROIZE                   = 0x9001,
    WTM_HASH_UPDATE                    = 0x9002,
    WTM_HASH_FINAL                     = 0x9003,
    WTM_HMAC_INIT                      = 0x9004,
    WTM_HMAC_ZEROIZE                   = 0x9005,
    WTM_HMAC_UPDATE                    = 0x9006,
    WTM_HMAC_FINAL                     = 0x9007,

    //RDSA: 9 PIs
    WTM_EMSA_PKCS1_V15_VERIFY          = 0xA000,
    WTM_EMSA_PKCS1_V15_VERIFY_INIT     = 0xA001,
    WTM_EMSA_PKCS1_V15_VERIFY_UPDATE   = 0xA002,
    WTM_EMSA_PKCS1_V15_VERIFY_FINAL    = 0xA003,
    WTM_EMSA_PKCS1_V15_ZEROIZE         = 0xA004,
    WTM_EMSA_PKCS1_V15_SIGN            = 0xA005,
    WTM_EMSA_PKCS1_V15_SIGN_INIT       = 0xA006,
    WTM_EMSA_PKCS1_V15_SIGN_UPDATE     = 0xA007,
    WTM_EMSA_PKCS1_V15_SIGN_FINAL      = 0xA008,
    WTM_RSA_KEY_GEN                    = 0xA009,
    WTM_DH_SYSPARAM_GEN                = 0xA00A,
    WTM_DH_PUBLIC_KEY_DERIVE           = 0xA00B,
    WTM_DH_SHARED_KEY_GEN              = 0xA00C,
    WTM_DH_SAVE_SHARED_KEY             = 0xA00D,
    WTM_DH_DK_PUBLIC_KEY_DERIVE        = 0xA00E,
    WTM_DH_DK_SHARED_KEY_GEN           = 0xA00F,

    // ECCP: 16 PIs
    WTM_ECCP_DSA_ZEROIZE               = 0xB000,
    WTM_ECCP_KEYPAIR_GEN               = 0xB001,
    WTM_ECCP_PUBLIC_KEY_DERIVATION     = 0xB002,
    WTM_ECCP_DH_SHARED_KEY             = 0xB003,
    WTM_ECCP_DSA_SIGN_INIT             = 0xB004,
    WTM_ECCP_DSA_SIGN_UPDATE           = 0xB005,
    WTM_ECCP_DSA_SIGN_FINAL            = 0xB006,
    WTM_ECCP_DSA_VERIFY_INIT           = 0xB007,
    WTM_ECCP_DSA_VERIFY_UPDATE         = 0xB008,
    WTM_ECCP_DSA_VERIFY_FINAL          = 0xB009,
    WTM_ECCP_DSA_SIGN                  = 0xB00A,
    WTM_ECCP_DSA_VERIFY                = 0xB00B,
    WTM_ECCP_POINT_MAC                 = 0xB00C,
    WTM_ECCP_SAVE_SHARED_KEY           = 0xB00D,
    WTM_ECCP_CUSTOMIZED_KEA            = 0xB00E,
    WTM_MODULAR_MAC                    = 0xB00F,
    
    // DK/DSK: 4 PIs
    WTM_OTP_DK_LOAD                    = 0XC000,
    WTM_OTP_DK_DISABLE                 = 0XC001,
    WTM_OTP_DSK_LOAD                   = 0XC002,
    WTM_OTP_DSK_REMOVE                 = 0XC003,
    
    // RESERVED
    WTM_RESERVED_TEST                  = 0xC004,
    
} PRIMITIVE;

// generic status code
typedef enum _STATUS
{
    STATUS_SUCCESS = 0,
    STATUS_FAILURE = 255,
    STATUS_NO_RESOURCES,                              // FIPS context
    STATUS_INVALID_CRYPTO_SCHEME,                     // non-FIPS context
    STATUS_UNSUPPORTED_FUNCTION,                      // GENERAL
    STATUS_UNSUPPORTED_PARAMETER,                     // GENERAL
    STATUS_PARAMETER_OUT_OF_RANGE,                    // GENERAL                 
    STATUS_PARITY_ERROR,                              // NA
    STATUS_FRAMING_ERROR,                             // NA
    STATUS_OVERRUN_ERROR,                             // NA
    STATUS_OFFSET_ERROR,                              // NA
    STATUS_IN_PROGRESS,                               // NA
    STATUS_RETRY_COUNT_EXCEEDED,                      // NA
    STATUS_DEVICE_CORRUPTED,                          // NA
    STATUS_BAD_ENGINE_OP,                             // engine
    STATUS_MODE_ERROR,                                // FIPS context/ECP        
    STATUS_BAD_TRANSFER_SIZE,                         // DMA/HASH                
    STATUS_CONVERGENCE_ERROR,                         // RNG
    STATUS_FATAL_INTERNAL_ERROR,                      // Platform
    STATUS_IN_USE,                                    // AES/HASH/ECC
    STATUS_SHORT_XFR,                                 // AES
    STATUS_ECB_MODE_WITH_PARTIAL_CODEWORD,            // AES
    STATUS_AES_INVALID_BLOCK,                         // AES
    STATUS_NO_KEY,                                    // AES/HASH/FIPS context
    STATUS_NO_IV,                                     // AES
    STATUS_NO_CONTEXT,                                // HASH/ECP                
    STATUS_INVALID_KEY_LENGTH,                        // HASH                    
    STATUS_INVALID_HASH_UPDATE_MESSAGE_LENGTH,        // HASH
    STATUS_HASH_MESSAGE_OVERFLOW,                     // HASH
    STATUS_UNSUPPORTED_DIGEST_TYPE,                   // HASH/ECP
    STATUS_INVALID_BINDING,                           // RSA/platform
    STATUS_INVALID_SIGNATURE,                         // RSA
    STATUS_SIGNATURE_REPRESENTATIVE_OUT_OF_RANGE,     // RSA
    STATUS_HASH_NOT_SUPPORTED,                        // RSA
    STATUS_INTENDED_ENCODED_MESSAGE_LENGTH_TOO_SHORT, // RSA
    STATUS_INTEGER_TOO_LARGE,                         // RSA                     
    STATUS_RSA_KEYGEN_EVEN_INPUT,                     // RSA                     
    STATUS_ZMODP_FAILED,                              // RSA
    STATUS_PRIMALITY_TEST_TOO_MANY_TRIALS,            // RSA/DH
    STATUS_RSA_KEYGEN_MOD_INV_BAD_ARG,                // RSA
    STATUS_RSA_KEYGEN_MOD_INV_BAD_MOD,                // RSA
    STATUS_RSA_KEYGEN_MOD_INV_NULL_PTR,               // RSA
    STATUS_RSA_KEYGEN_MOD_INV_OUT_OF_RANGE,           // RSA
    STATUS_DH_BN_FAILURE,                             // DH
    STATUS_IPPCP_MONT_INIT_FAILED,                    // DH
    STATUS_IPPCP_MONT_BUFSIZE_FAILED,                 // DH
    STATUS_IPPCP_MONT_SET_FAILED,                     // DH
    STATUS_IPPCP_MONT_EXP_FAILED,                     // DH
    STATUS_IPPCP_BN_BUFSIZE_FAILED,                   // DH
    STATUS_IPPCP_BN_INIT_FAILED,                      // DH
    STATUS_IPPCP_SET_BN_FAILED,                       // DH
    STATUS_IPPCP_GET_BN_FAILED,                       // DH
    STATUS_IPPCP_SUB_BN_FAILED,                       // RSA
    STATUS_IPPCP_MUL_BN_FAILED,                       // RSA
    STATUS_PENDING,                                   // ECP                        
    STATUS_ILLEGAL_KEY,                               // ECP/RNG/platform/AES/HMAC
    STATUS_ECC_ERROR,                                 // ECP/platform
    STATUS_ENGINE_CONTEXT_MISMATCH,                   // engine/key management
    STATUS_DMA_TIMEOUT,                               // DMA
    STATUS_DMA_BUS_ERROR,                             // DMA
    STATUS_DMA_PARITY_ERROR,                          // DMA
    STATUS_DMA_LINKED_LIST_ACCESS_ERROR,              // DMA
    STATUS_HASH_TIMEOUT,                              // HASH - time out
    STATUS_AES_TIMEOUT,                               // AES - time out          
    STATUS_ZMODP_TIMEOUT,                             // RSA - time out             
    STATUS_EC_TIMEOUT,                                // ECP - time out
    STATUS_DES_TIMEOUT,                               // DES - time out
    STATUS_MCT_TIMEOUT,                               // MCT - time out
    STATUS_EBG_TIMEOUT,                               // EBG - time out
    STATUS_OTP_TIMEOUT,                               // OTP - time out
    STATUS_UNEXPECTED_IRQ,                            // Reserved
    STATUS_UNEXPECTED_FIQ,                            // Reserved
    STATUS_UNEXPECTED_DATA_ABORT,                     // Reserved                
    STATUS_UNEXPECTED_PREFETCH_ABORT,                 // Reserved                
    STATUS_UNEXPECTED_SWI,                            // Reserved
    STATUS_UNEXPECTED_UNUSED,                         // Reserved
    STATUS_INVALID_REQUEST,                           // Reserved
    STATUS_ERROR_PERMANENT_FIPS,                      // FIPS management
    STATUS_ERROR_PERMANENT_NON_FIPS,                  // FIPS management
    STATUS_ERROR_NON_FIPS_UNTIL_RESET, 				  // FIPS management
    STATUS_PERMISSION_VIOLATION,                      // FIPS management
    STATUS_LIFECYCLE_SPENT,                           // State management
    STATUS_SW_OUT_OF_RANGE,                           // State management
    STATUS_WTM_NONCE_NOT_GENERATED,                   // Key management          
    STATUS_INVALID_KEY_ID,                            // Key management           
    STATUS_MISSING_ENDORSEMENT_KEY,                   // Key management
    STATUS_DIGEST_MISMATCH,                           // FIPS context/key management
    STATUS_BAD_THREAD_ID,                             // FIPS context
    STATUS_CONTEXT_CACHE_FULL,                        // FIPS context/key management
    STATUS_BAD_CACHE_ID,                              // FIPS context/key management
    STATUS_INVALID_CONTEXT_SLOT,                      // FIPS context
    STATUS_NO_SIGNING_KEY,                            // FIPS key management
    STATUS_SKEK_NOT_LOADED,                           // Reserved
    STATUS_TIM_R_SEGMENT_COMPROMISED,                 // Sliding window          
    STATUS_DES_INVALID_BLOCK,                         // DES                      
    STATUS_INVALID_HDCP_WOF,                          // HDCP
    STATUS_RC4_TIMEOUT,                               // RC4 - time out
    STATUS_RC4_INVALID_BLOCK,                         // RC4                      
    STATUS_OTP_DIRTY,
    STATUS_COMMAND_TIMEOUT = 4094,  // added for WTM_Test_Driver only
    STATUS_USER_DEFINED_1 = 5000,
    STATUS_USER_DEFINED_2,
    STATUS_USER_DEFINED_3,
    STATUS_LAST_ONE
} STATUS;


// DMA data-transfer descriptor, used for linked-list processing.
// Client constructs a queue of these and points
// 'primitive_command_parameter14' or 'primitive_command_parameter15'
// to the first one, thereby allowing a scatter-gather function to
// many primtives.
typedef struct _DTD
{
    U32 transfer_address;               // to/from address
    U32 transfer_size;                  // words to transfer
    struct _DTD *next;                  // pointer to next (or NULL)
    U32 reserved;                       // for microcode use
} DTD;

#define SZ_DTD sizeof( DTD )
#define SZW_DTD (SZ_DTD / BYTES_PER_WORD)

// engine id, used to index into g-> engine_state[]
typedef enum _ENGINE_ID
{
    ENGINE_ID_NULL = 0,        //  must be first and eval to zero
    ENGINE_ID_HASH,
    ENGINE_ID_AES,
    ENGINE_ID_DES,
    ENGINE_ID_RC4,
    ENGINE_ID_ZMODP,           // RSA
    ENGINE_ID_ENTROPY,
    ENGINE_ID_PRNG,
    ENGINE_ID_ECC,
    ENGINE_ID_PRIMALITY,
    ENGINE_ID_DMA,
    ENGINE_ID_MTC,
    ENGINE_ID_SCRATCH_PAD,
    ENGINE_ID_LAST_ONE
} ENGINE_ID;

// how many engines to manage
#define NUM_ENGINES ENGINE_ID_LAST_ONE

// bitmapped engine IDs, derived from engine IDs

#define ENGINE_HASH        BIT( ENGINE_ID_HASH )
#define ENGINE_AES         BIT( ENGINE_ID_AES )
#define ENGINE_DES         BIT( ENGINE_ID_DES )
#define ENGINE_ZMODP       BIT( ENGINE_ID_ZMODP )
#define ENGINE_RC4         BIT( ENGINE_ID_RC4 )
#define ENGINE_ENTROPY     BIT( ENGINE_ID_RC4 )
#define ENGINE_PRNG        BIT( ENGINE_ID_PRNG )
#define ENGINE_ECC         BIT( ENGINE_ID_ECC )
#define ENGINE_PRIMALITY   BIT( ENGINE_ID_PRIMALITY )
#define ENGINE_DMA         BIT( ENGINE_ID_DMA )
#define ENGINE_MTC         BIT( ENGINE_ID_MTC )

#define ENGINE_ALL (ENGINE_HASH|ENGINE_AES|ENGINE_ZMODP|ENGINE_ENTROPY|ENGINE_PRNG|ENGINE_DMA)

typedef enum _LCS_STATE
{
    LCS_STATE_CM = 0,
    LCS_STATE_DM = 1,
    LCS_STATE_DD = 2,
    LCS_STATE_FA = 3
} LCS_STATE;

// Trust Status Register Fields
#define TSR_OTP_RKEK_SECURE_OFFSET          0
#define TSR_OTP_RKEK_SECURE_MASK            BIT(0)

#define TSR_OTP_PLATFORM_KEY_SECURE_OFFSET  1
#define TSR_OTP_PLATFORM_KEY_SECURE_MASK    BIT(1)

#define TSR_OTP_JTAG_SECURE_OFFSET          2
#define TSR_OTP_JTAG_SECURE_MASK            BIT(2)

#define TSR_OTP_JTAG_PERM_DISABLE_OFFSET    3
#define TSR_OTP_JTAG_PERM_DISABLE_MASK      BIT(3)

#define TSR_OTP_LCS_OFFSET                  4
#define TSR_OTP_LCS_MASK                    (BIT(5) | BIT(4))

#define TSR_OTP_DIS_TMP_FA_OFFSET           6
#define TSR_OTP_DIS_TMP_FA_MASK             BIT(6)

#define TSR_LCS_VIRGIN_OFFSET               7
#define TSR_LCS_VIRGIN_MASK                 BIT(7)

#define TSR_REAL_FUSE_OFFSET                8
#define TSR_REAL_FUSE_MASK                  BIT(8)

#define TSR_EC521_DK_DISABLE_OFFSET         9
#define TSR_EC521_DK_DISABLE_MASK           BIT(9)

#define TSR_OTP_EC521_DK_SECURE_OFFSET      10
#define TSR_OTP_EC521_DK_SECURE_MASK        BIT(10)

#define TSR_HDCP_KEY_WRAPPED_OFFSET         11
#define TSR_HDCP_KEY_WRAPPED_MASK           BIT(11)

#define TSR_HDCP_KEY_LOADED_OFFSET          12
#define TSR_HDCP_KEY_LOADED_MASK            BIT(12)

#define TSR_OTP_PERM_FIPS_OFFSET            13
#define TSR_OTP_PERM_FIPS_MASK              BIT(13)

#define TSR_OTP_FIPS_PERM_DISABLE_OFFSET    14
#define TSR_OTP_FIPS_PERM_DISABLE_MASK      BIT(14)

#define TSR_REG_OP_STATE_OFFSET             15
#define TSR_REG_OP_STATE_MASK               (BIT(17) | BIT(16) | BIT(15))

#define TSR_REG_OP_TIM_R_OFFSET             18
#define TSR_REG_OP_TIM_R_MASK               BIT(18)

#define TSR_OTP_RKEK_EBG_GENERATED_OFFSET       19
#define TSR_OTP_RKEK_EBG_GENERATED_MASK         BIT(19)

#define TSR_OTP_EC521_DK_EBG_GENERATED_OFFSET   20 
#define TSR_OTP_EC521_DK_EBG_GENERATED_MASK     BIT(20)

#define TSR_OTP_USBID_SECURE_OFFSET  		21
#define TSR_OTP_USBID_SECURE_MASK    		BIT(21)

#define TSR_UNEXPECTED_SC_IRQ_OFFSET  		22
#define TSR_UNEXPECTED_SC_IRQ_MASK    		BIT(22)

#define TSR_REG_SET_NONFIPS_MODE_UNTIL_RESET_OFFSET  		23
#define TSR_REG_SET_NONFIPS_MODE_UNTIL_RESET_MASK   		BIT(23)

#define TSR_OTP_SOC_FINALIZED_OFFSET            24  
#define TSR_OTP_SOC_FINALIZED_MASK              BIT(24)

////////////////////////////////////////////////////////////////////////////////
//// |<-- CAT -->|<--- PKCS -->|<--- HASH -->|<-- AES -->|
////               * *   KeyLen  * Mode Algo  Mode KeyLen
////    [15:12]   [11:10] [9:8] [7] [6] [5:4] [3:2] [1:0]
////
////  CAT[15:12]: 8 - AES
////              9 - HASH/HMAC
////              A - PKCS
////
////  [NOTE]  * means "reserved"
////
////  AES[3:0]:  Fields "KeyLen" and "Mode" match definitions of "KeyLen" and
////             "Mode" in register "AES_CFG_REG."
////
////      KeyLen[1:0]  00 - 128-bit
////                   01 - 256-bit
////                   10 - 192-bit
////                   11 - ERROR
////
////      Mode[3:2]    00 - ECB
////                   01 - CBC
////                   10 - CTR
////                   11 - XTS
////
////  HASH[7:4]: Fields "Algo" and "Mode" match definitions of "alg_select"
////             and "hash_mode" in register "Hash_Configure."
////
////      Algo[5:4]    00 - SHA1
////                   01 - SHA256
////                   10 - SHA224
////                   11 - MD5
////
////      Mode[6:6]    0  - Normal hash mode
////                   1  - HMAC
////
////  PKCS[11:8]
////      KeyLen[9:8]  01 - 1024-bit
////                   10 - 2048-bit
////  ECCP[11:0]
////      Field[9:8]   00 - 224-bit
////                   01 - 256-bit
////                   10 - 384-bit
////                   11 - 521-bit
////      Hash [5:4]   00 - SHA1
////                   01 - SHA256
////                   10 - SHA224
////      Algo [1:0]   00 - ECDH
////                   01 - ECDSA
////                   10 - ECMQV
////       
////////////////////////////////////////////////////////////////////////////////
typedef enum _Cryptographic_Scheme
{                                           //  CAT PKCS HASH  AES
    //AES                                        |    |    |    |
    AES_ECB128 = 0x00008000,                // 1000-0000-0000-0000
    AES_ECB192 = 0x00008002,                // 1000-0000-0000-0010
    AES_ECB256 = 0x00008001,                // 1000-0000-0000-0001
    AES_CBC128 = 0x00008004,                // 1000-0000-0000-0100
    AES_CBC192 = 0x00008006,                // 1000-0000-0000-0110
    AES_CBC256 = 0x00008005,                // 1000-0000-0000-0101
    AES_CTR128 = 0x00008008,                // 1000-0000-0000-1000
    AES_CTR192 = 0x0000800A,                // 1000-0000-0000-1010
    AES_CTR256 = 0x00008009,                // 1000-0000-0000-1001
    AES_XTS256 = 0x0000800C,                // 1000-0000-0000-1100
    AES_XTS512 = 0x0000800D,                // 1000-0000-0000-1101

#if 0
    //Z0 HASH/HMAC
    SHA_1        = 0x00009000,              // 1001-0000-0000-0000
    SHA_256      = 0x00009010,              // 1001-0000-0001-0000
    SHA_224      = 0x00009020,              // 1001-0000-0010-0000
    MD_5         = 0x00009030,              // 1001-0000-0011-0000
    HMAC_SHA_1   = 0x00009040,              // 1001-0000-0100-0000
    HMAC_SHA_224 = 0x00009060,              // 1001-0000-0110-0000
    HMAC_SHA_256 = 0x00009050,              // 1001-0000-0101-0000
    HMAC_MD_5    = 0x00009070,              // 1001-0000-0111-0000
#else
    //A0 HASH/HMAC
    SHA_1        = 0x00009000,              // 1001-0000-0000-0000
    SHA_256      = 0x00009010,              // 1001-0000-0001-0000
    SHA_224      = 0x00009020,              // 1001-0000-0010-0000
    MD_5         = 0x00009030,              // 1001-0000-0011-0000
    SHA_512      = 0x00009040,              // 1001-0000-0100-0000
    SHA_384      = 0x00009050,              // 1001-0000-0101-0000
    HMAC_SHA_1   = 0x00009080,              // 1001-0000-1000-0000
    HMAC_SHA_256 = 0x00009090,              // 1001-0000-1001-0000
    HMAC_SHA_224 = 0x000090A0,              // 1001-0000-1010-0000
    HMAC_MD_5    = 0x000090B0,              // 1001-0000-1011-0000
    HMAC_SHA_512 = 0x000090C0,              // 1001-0000-1100-0000
    HMAC_SHA_384 = 0x000090D0,              // 1001-0000-1101-0000
#endif

    //PKCS#1v1.5 Digital Signature
    PKCSv1_SHA1_1024RSA   = 0x0000A100,     // 1010-0001-0000-0000
    PKCSv1_SHA224_1024RSA = 0x0000A120,     // 1010-0001-0010-0000
    PKCSv1_SHA256_1024RSA = 0x0000A110,     // 1010-0001-0001-0000
    PKCSv1_SHA1_2048RSA   = 0x0000A200,     // 1010-0010-0000-0000
    PKCSv1_SHA224_2048RSA = 0x0000A220,     // 1010-0010-0010-0000
    PKCSv1_SHA256_2048RSA = 0x0000A210,     // 1010-0010-0001-0000

    //RSA_DH
    RSA_DH_1024           = 0x0000A001,     // 1010-0000-0000-0001
    RSA_DH_2048           = 0x0000A002,     // 1010-0000-0000-0010

    //                                          ECC
    //                                          CAT FIELD HASH DH/DSA/MQV
    //ECCP                                       |    |    |    |
    ECCP224_FIPS_DH         = 0x0000B000,   // 1011-0000-0000-0000
    ECCP256_FIPS_DH         = 0x0000B100,   // 1011-0001-0000-0000
    ECCP384_FIPS_DH         = 0x0000B200,   // 1011-0010-0000-0000
    ECCP521_FIPS_DH         = 0x0000B300,   // 1011-0011-0000-0000

    ECCP224_FIPS_DSA_SHA1   = 0x0000B001,   // 1011-0000-0000-0001
    ECCP224_FIPS_DSA_SHA224 = 0x0000B021,   // 1011-0000-0010-0001
    ECCP224_FIPS_DSA_SHA256 = 0x0000B011,   // 1011-0000-0001-0001
	ECCP224_FIPS_DSA_SHA384 = 0x0000B051,   // 1011-0000-0101-0001
	ECCP224_FIPS_DSA_SHA512 = 0x0000B041,   // 1011-0000-0100-0001
	
    ECCP256_FIPS_DSA_SHA1   = 0x0000B101,   // 1011-0001-0000-0001
    ECCP256_FIPS_DSA_SHA224 = 0x0000B121,   // 1011-0001-0010-0001
    ECCP256_FIPS_DSA_SHA256 = 0x0000B111,   // 1011-0001-0001-0001
    ECCP256_FIPS_DSA_SHA384 = 0x0000B151,   // 1011-0001-0101-0001
	ECCP256_FIPS_DSA_SHA512 = 0x0000B141,   // 1011-0001-0100-0001
	
    ECCP384_FIPS_DSA_SHA1   = 0x0000B201,   // 1011-0010-0000-0001
    ECCP384_FIPS_DSA_SHA224 = 0x0000B221,   // 1011-0010-0010-0001
    ECCP384_FIPS_DSA_SHA256 = 0x0000B211,   // 1011-0010-0001-0001
	ECCP384_FIPS_DSA_SHA384 = 0x0000B251,   // 1011-0010-0101-0001
    ECCP384_FIPS_DSA_SHA512 = 0x0000B241,   // 1011-0010-0100-0001
    
    ECCP521_FIPS_DSA_SHA1   = 0x0000B301,   // 1011-0011-0000-0001
    ECCP521_FIPS_DSA_SHA224 = 0x0000B321,   // 1011-0011-0010-0001
    ECCP521_FIPS_DSA_SHA256 = 0x0000B311,   // 1011-0011-0001-0001
	ECCP521_FIPS_DSA_SHA384 = 0x0000B351,   // 1011-0011-0101-0001
    ECCP521_FIPS_DSA_SHA512 = 0x0000B341,   // 1011-0011-0100-0001
  
    //DES-TDES
    DES_ECB  = 0x0000C000,
    DES_CBC  = 0x0000C001,
    TDES_ECB = 0x0000C002,
    TDES_CBC = 0x0000C003,

    //RC4
    RC4 = 0x0000D000,

    CRYPTOGRAPHIC_SCHEME_LAST_ONE,
} CRYPTO_SCHEME_ENUM;

typedef enum KEY_SELECTION
{
    AES_RKEK         = 0,
    AES_INTERNAL_KEY = 1,
    AES_EXTERNAL_KEY = 2,
} KEY_SELECTION;

// types of AES ops
typedef enum AES_OP
{
    AES_ENCRYPT,
    AES_DECRYPT,
    AES_OP_LAST_ONE,
} AES_OP;

// types of DES ops
typedef enum DES_OP
{
    DES_ENCRYPT,
    DES_DECRYPT,
    DES_OP_LAST_ONE,
} DES_OP;

typedef enum RC4_OP
{
    RC4_ENCRYPT,
    RC4_DECRYPT,
    RC4_OP_LAST_ONE,
} RC4_OP;

#define MAX_RC4_KEY_LENGTH 32

// ECCP related

#define SLOT_ID_USE_DSK    0xFFFFFFF0
//#define CURRENT_ECC_MAX_COEF_SIZE 8 //support up to p256 on Z0/Z1

typedef enum ECC_POINT_MULT_MODE
{
    ECC_MULT_RANDOM_MODE    = 0x0,
    ECC_MULT_FIXED_MODE     = 0x1,
} ECC_POINT_MULT_MODE;

#define ECCP521_ENABLE 1
#define ECCP_USE_OTP_RKEK 1 // use real OTP FUSE key

#ifdef ECCP521_ENABLE
    #define ECC_MAX_COEF_LEN 17     
#else
    #define ECC_MAX_COEF_LEN 8     //support up to p256 on Z0/Z1
#endif

////////////////////////////////////////////////////////////////////////
typedef struct _EC_domain_parameters
{
    U32 FieldSize_in_bits;                  
    U32 Cofactor;                            
    U32 pEcc[ECC_MAX_COEF_LEN];    
    U32 n[ECC_MAX_COEF_LEN];       
    U32 a[ECC_MAX_COEF_LEN];       
    U32 b[ECC_MAX_COEF_LEN];         
    U32 G_x[ECC_MAX_COEF_LEN];       
    U32 G_y[ECC_MAX_COEF_LEN];       
}EC_domain_parameters;
#define SZ_ECCP_DOMAIN_PARA (  sizeof(EC_domain_parameters) )
#define SZW_ECCP_DOMAIN_PARA (  sizeof(EC_domain_parameters)/BYTES_PER_WORD  )

typedef struct _ECC_point
{
    U32 FieldSize_in_bits;               
    U32 X[ECC_MAX_COEF_LEN];    
    U32 Y[ECC_MAX_COEF_LEN];    
}ECC_point;
#define SZ_ECCP_POINT (  sizeof(ECC_point) )
#define SZW_ECCP_POINT (  sizeof(ECC_point)/BYTES_PER_WORD   )
////////////////////////////////////////////////////////////////////////

typedef enum CONTEXT_TYPE
{
    SYMMETRIC  = 1,
    ASYMMETRIC = 2,
} CONTEXT_TYPE;

// The context cache slot resides either in external DDR or internal SRAM on
// secure processor.
typedef enum CONTEXT_CACHE_SELECTION
{
    SELECTION_INTERNAL = 0,
    SELECTION_EXTERNAL = 1,
} CONTEXT_CACHE_SELECTION;


/******************** AUTOGENERATED: PLEASE DO NOT EDIT *********************/

/*************************** AUTOGENERATED: END *****************************/

#endif
