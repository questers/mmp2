/****************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code. This Module contains Proprietary
 *  Information of Marvell and should be treated as Confidential. The
 *  information in this file is provided for the exclusive use of the
 *  licensees of Marvell. Such users have the right to use, modify, and
 *  incorporate this code into products for purposes authorized by the
 *  license agreement provided they include this notice and the associated
 *  copyright notice with any such product.
 *
 *  The information in this file is provided "AS IS" without warranty.
 *
 ***************************************************************************/


#ifndef INCLUDE_WTM_MBOX_H
#define INCLUDE_WTM_MBOX_H

#include "Typedef.h"
#include "tim.h"
#include "wtm_mbox.h"
#include "bl_provisioning_if.h"

/*
 * WTM register file for host communication
 */
typedef struct
{
    UINT_T  prim_cmd_parm0;          // 0x0
    UINT_T  prim_cmd_parm1;          // 0x4
    UINT_T  prim_cmd_parm2;          // 0x8
    UINT_T  prim_cmd_parm3;          // 0xc
    UINT_T  prim_cmd_parm4;          // 0x10
    UINT_T  prim_cmd_parm5;          // 0x14
    UINT_T  prim_cmd_parm6;          // 0x18
    UINT_T  prim_cmd_parm7;          // 0x1c
    UINT_T  prim_cmd_parm8;          // 0x20
    UINT_T  prim_cmd_parm9;          // 0x24
    UINT_T  prim_cmd_parm10;         // 0x28
    UINT_T  prim_cmd_parm11;         // 0x2c
    UINT_T  prim_cmd_parm12;         // 0x30
    UINT_T  prim_cmd_parm13;         // 0x34
    UINT_T  prim_cmd_parm14;         // 0x38
    UINT_T  prim_cmd_parm15;         // 0x3c
    UINT_T  secure_processor_cmd;    // 0x40
    UINT8_T reserved_0x44[60];
    UINT_T  cmd_return_status;       // 0x80
    UINT_T  cmd_status_0;            // 0x84
    UINT_T  cmd_status_1;            // 0x88
    UINT_T  cmd_status_2;            // 0x8c
    UINT_T  cmd_status_3;            // 0x90
    UINT_T  cmd_status_4;            // 0x94
    UINT_T  cmd_status_5;            // 0x98
    UINT_T  cmd_status_6;            // 0x9c
    UINT_T  cmd_status_7;            // 0xa0
    UINT_T  cmd_status_8;            // 0xa4
    UINT_T  cmd_status_9;            // 0xa8
    UINT_T  cmd_status_10;           // 0xac
    UINT_T  cmd_status_11;           // 0xb0
    UINT_T  cmd_status_12;           // 0xb4
    UINT_T  cmd_status_13;           // 0xb8
    UINT_T  cmd_status_14;           // 0xbc
    UINT_T  cmd_status_15;           // 0xc0
    UINT_T  cmd_fifo_status;         // 0xc4
    UINT_T  host_interrupt_register; // 0xc8
    UINT_T  host_interrupt_mask;     // 0xcc
    UINT_T  host_exception_address;  // 0xd0
    UINT_T  sp_trust_register;       // 0xd4
    UINT_T  wtm_identification;      // 0xd8
    UINT_T  wtm_revision;            // 0xdc
} WTM_HOST_BIU;

typedef UINT_T (*wtm3_host_interface_init)(UINT_T timeout_ms, UINT_T adv_ver);
typedef UINT_T (*wtm3_compute_digest)(const UINT8_T* msg, INT32 len, UINT8_T* md, UINT_T algo);
typedef UINT_T (*wtm3_platform_authentication)(pTIM ptim);
typedef UINT_T (*wtm3_verify_jtag_key)(pTIM ptim);
typedef UINT_T (*wtm3_tim_validation)(pTIM ptim);
typedef UINT_T (*wtm3_image_validation)(UINT_T start_addr, UINT_T image_id, pTIM ptim, void* param);
typedef UINT_T (*wtm3_read_profile_fuse)(pTIM ptim, UINT_T addr);
typedef UINT_T (*wtm3_host_interface_shutdown)(void* param);

typedef struct wtm3_crypto_funcs
{
    wtm3_host_interface_init		host_interface_init;
    wtm3_compute_digest				compute_digest;
    wtm3_platform_authentication	platform_authentication;
	wtm3_verify_jtag_key			verify_jtag_key;
    wtm3_tim_validation				tim_validation;
    wtm3_image_validation			image_validation;
    wtm3_read_profile_fuse			read_profile_fuse;
    wtm3_host_interface_shutdown	host_interface_shutdown;
} wtm3_crypto_funcs;

extern wtm3_crypto_funcs wtm3_crypto_funcs_impl;

/*
 * TBD: this function need be integrated in platform_interrupt.c for MMP2
 */
extern void wtm3_interrupt_handler(void);

typedef fuse_conf_spec_t (*wtm3_fuse_conf_spec)(void* param);
typedef UINT_T (*wtm3_bind_platform)(pTIM ptim, void* param);
typedef UINT_T (*wtm3_platform_binded)(pTIM ptim, void* param);
typedef UINT_T (*wtm3_config_platform)(UINT_T* conf, UINT_T bit_cnt, fuse_type_enum type, pTIM ptim, void* para);
typedef UINT_T (*wtm3_read_platform_config)(UINT_T* conf, UINT_T* bit_cnt, fuse_type_enum type, void* para);
typedef UINT_T (*wtm3_program_jtag_key)(UINT8_T* key, UINT_T len, pTIM ptim, void* para);
typedef UINT_T (*wtm3_read_jtag_key)(UINT8_T* key, UINT_T* len, void* para);
typedef UINT_T (*wtm3_program_usb_id)(UINT8_T* usb, UINT_T len, void* para);
typedef UINT_T (*wtm3_read_usb_id)(UINT8_T* usb, UINT_T* len, void* para);
typedef UINT_T (*wtm3_program_oem_unique_id)(UINT8_T* oem_id, UINT_T len, void* para);
typedef UINT_T (*wtm3_read_oem_unique_id)(UINT8_T* oem_id, UINT_T* len, void* para);
typedef UINT_T (*wtm3_advance_life_cycle)(UINT_T* lc, void* para);
typedef UINT_T (*wtm3_read_life_cycle)(UINT_T* lc, void* para);
typedef UINT_T (*wtm3_advance_version)(UINT_T* ver, void* para);
typedef UINT_T (*wtm3_read_version)(UINT_T* ver, void* para);
typedef UINT_T (*wtm3_autocfg_platform)(pTIM ptim, void* param);
typedef UINT_T (*wtm3_program_rkek)(void* para);
typedef UINT_T (*wtm3_program_ec521_dk)(void* para);


typedef struct wtm3_provisioning_funcs
{
   wtm3_fuse_conf_spec			fuse_conf_spec;
   wtm3_bind_platform			bind_platform;
   wtm3_platform_binded			platform_binded;
   wtm3_config_platform			config_platform;
   wtm3_read_platform_config	read_platform_config;
   wtm3_program_jtag_key		program_jtag_key;
   wtm3_read_jtag_key			read_jtag_key;
   wtm3_program_usb_id			program_usb_id;
   wtm3_read_usb_id				read_usb_id;
   wtm3_read_oem_unique_id		read_oem_unique_id;
   wtm3_advance_life_cycle		advance_life_cycle;
   wtm3_read_life_cycle			read_life_cycle;
   wtm3_advance_version			advance_version;
   wtm3_read_version			read_version;
   wtm3_autocfg_platform		autocfg_platform;
   wtm3_program_rkek 			program_rkek;
   wtm3_program_ec521_dk		program_ec521_dk;
} wtm3_provisioning_funcs;

void wtm3_funcs_init();

extern wtm3_provisioning_funcs wtm3_provisioning_funcs_impl;

#endif /* INCLUDE_WTM_MBOX_H */

