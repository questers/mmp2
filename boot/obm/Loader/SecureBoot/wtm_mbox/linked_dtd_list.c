////////////////////////////////////////////////////////////////////////////////
//                                     NOTICE                                 //
//                                                                            //
//             COPYRIGHT MARVELL INTERNATIONAL LTD. AND ITS AFFILIATES        //
//                              ALL RIGHTS RESERVED                           //
//                                                                            //
//     The source code for this computer program is  CONFIDENTIAL  and a      //
//     TRADE SECRET of MARVELL  INTERNATIONAL  LTD. AND  ITS  AFFILIATES      //
//     ('MARVELL'). The receipt or possession of  this  program does not      //
//     convey any rights to  reproduce or  disclose  its contents, or to      //
//     manufacture,  use, or  sell  anything  that it  may  describe, in      //
//     whole or in part, without the specific written consent of MARVELL.     //
//     Any  reproduction  or  distribution  of this  program without the      //
//     express written consent of MARVELL is a violation of the copyright     //
//     laws and may subject you to criminal prosecution.                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include "linked_dtd_list.h"
#include "misc.h"


void zerolize_dtd(dtd_t *dtd)
{
    memset(dtd, 0, sizeof(dtd_t));
}


void pack_dtd(dtd_t* dtd, UINT_T transfer_addr, UINT_T data_words)
{
    dtd->transfer_addr  = transfer_addr;
    dtd->transfer_words = data_words;
    dtd->next           = NULL;
    dtd->reserved       = 0;
}


void linked_dtd_list_init(linked_dtd_list_t* list)
{
    list->num_dtd = 0;
    list->head    = list->tail = NULL;
}


int linked_dtd_list_insert_after(linked_dtd_list_t* list, dtd_t* dtd, UINT_T pos)
{
    UINT_T i;
    dtd_t* p;

    if (pos >= list->num_dtd) {
        return -1;
    }

    if (list->num_dtd == 0) {
        list->head = list->tail = dtd;
        list->num_dtd++;
        return 0;
    }

    for (p = list->head, i = 0; i < pos; i++) {
        p = p->next;
    }

    if (p->next == NULL) {
        p->next    = dtd;
        list->tail = dtd;
    } 
    else {
        dtd->next = p->next;
        p->next   = dtd;
    }

    list->num_dtd++;

    return 0;
}

void linked_dtd_list_append(linked_dtd_list_t* list, dtd_t* dtd)
{
    if (list->num_dtd == 0) {
        list->head = list->tail = dtd;
        list->num_dtd++;
        return;
    }
    list->tail->next = dtd;
    list->tail       = dtd;
    list->num_dtd++;
}
