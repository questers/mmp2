/****************************************************************************
 *
 *  (C)Copyright 2005 - 2008 Marvell. All Rights Reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended
 *  publication of such source code. This Module contains Proprietary
 *  Information of Marvell and should be treated as Confidential. The
 *  information in this file is provided for the exclusive use of the
 *  licensees of Marvell. Such users have the right to use, modify, and
 *  incorporate this code into products for purposes authorized by the
 *  license agreement provided they include this notice and the associated
 *  copyright notice with any such product.
 *
 *  The information in this file is provided "AS IS" without warranty.
 *
 ***************************************************************************/

#include "wtm_mbox_impl.h"
#include "linked_dtd_list.h"
#include "timer.h"
#include "block0_fuse_config.h"
#include "FuseHw.h"     		// WTM_BASE
#include "FuseInterface.h"		// K_JTAG_HASH_FUSE_SIZE and FUSE_ReadOemJtagHashKeyFuseBits

#include "keys.h" 		// Workaround until SetTIMPointers works correctly for KEY_MOD

/* FixMe: map error code */
#define error_cmd_timeout    0x1000
#define error_invalid_param  0x1001
#define error_wtm_time_out   0x1002
#define error_invalid_algo   0x1003
#define error_invalid_tim    0x1004


#define BYTE_PER_WORD        4
#define WTM_PRIM_TIMEOUT_US  10000  /* need find the right values for them   */

#define WTM_CMD_STS_MASK              (1 << 8)

#define WTM_PRIM_CMD_COMPLETE_MASK    (1 << 0)
#define WTM_PJ4_ADDR_EXCPTION_MASK    (1 << 16)
#define WTM_QUE_BAD_ACCESS_MASK       (1 << 17)
#define WTM_QUE_QUE_FULL_MASK         (1 << 18)

//#define WTM_BASE                    0xD4290000
#define WTM_HOST_IF                     ((WTM_HOST_BIU*)(WTM_BASE + 0x0))

#define WTM_PJ_INTERRUPT_MASK_REG       (WTM_BASE + 0x00C8)
#define _enable_wtm_cmd_complete_int    do {(*(volatile UINT_T*)WTM_PJ_INTERRUPT_MASK_REG) &= ~WTM_PRIM_CMD_COMPLETE_MASK;} while (0)
#define _disable_wtm_cmd_complete_int   do {(*(volatile UINT_T*)WTM_PJ_INTERRUPT_MASK_REG) |= WTM_PRIM_CMD_COMPLETE_MASK;} while (0)
#define _enable_wtm_addr_exception_int  do {(*(volatile UINT_T*)WTM_PJ_INTERRUPT_MASK_REG) &= ~WTM_PJ4_ADDR_EXCPTION_MASK;} while (0)
#define _disable_wtm_addr_exception_int do {(*(volatile UINT_T*)WTM_PJ_INTERRUPT_MASK_REG) |= WTM_PJ4_ADDR_EXCPTION_MASK;} while (0)
#define _enable_wtm_bad_access_int      do {(*(volatile UINT_T*)WTM_PJ_INTERRUPT_MASK_REG) &= ~WTM_QUE_BAD_ACCESS_MASK;} while (0)
#define _disable_wtm_bad_access_int     do {(*(volatile UINT_T*)WTM_PJ_INTERRUPT_MASK_REG) |= WTM_QUE_BAD_ACCESS_MASK;} while (0)
#define _enable_wtm_que_full_int        do {(*(volatile UINT_T*)WTM_PJ_INTERRUPT_MASK_REG) &= ~WTM_QUE_QUE_FULL_MASK;} while (0)
#define _disable_wtm_que_full_int       do {(*(volatile UINT_T*)WTM_PJ_INTERRUPT_MASK_REG) |= WTM_QUE_QUE_FULL_MASK;} while (0)
#define _disable_wtm_ints               do {(*(volatile UINT_T*)WTM_PJ_INTERRUPT_MASK_REG)  = 0xFFFFFFFF;} while (0)

#define ICU_BASE             0xD4282000
#define ICU_IRQ_0_TO_63_CONF (ICU_BASE+0x0000)
#define ICU_IRQ40_CONF       (ICU_IRQ_0_TO_63_CONF + 40 *4)
#define IRQ1_ENABLE_MASK     (1 << 5)
#define _enable_irq1         do { *(volatile UINT_T*)ICU_IRQ40_CONF |= IRQ1_ENABLE_MASK;} while (0)
#define _disable_irq1        do { *(volatile UINT_T*)ICU_IRQ40_CONF &= ~IRQ1_ENABLE_MASK;} while (0)

#define	ICU_IRQ_SEL_INT_NUM_CORE1	     (ICU_BASE+0x0104)
#define	ICU_IRQ_SEL_INT_NUM_INT_PENDING	 (1 << 6)

#define DTD_NUM_MAX           10
static linked_dtd_list_t      _dtd_list;
static __attribute__((aligned(16)))dtd_t _dtds[DTD_NUM_MAX];
static volatile UINT_T        _prim_op_complete_flag;

typedef struct {
    UINT_T prim_cmd_parm0;          // 0x0
    UINT_T prim_cmd_parm1;          // 0x4
    UINT_T prim_cmd_parm2;          // 0x8
    UINT_T prim_cmd_parm3;          // 0xc
    UINT_T prim_cmd_parm4;          // 0x10
    UINT_T prim_cmd_parm5;          // 0x14
    UINT_T prim_cmd_parm6;          // 0x18
    UINT_T prim_cmd_parm7;          // 0x1c
    UINT_T prim_cmd_parm8;          // 0x20
    UINT_T prim_cmd_parm9;          // 0x24
    UINT_T prim_cmd_parm10;         // 0x28
    UINT_T prim_cmd_parm11;         // 0x2c
    UINT_T prim_cmd_parm12;         // 0x30
    UINT_T prim_cmd_parm13;         // 0x34
    UINT_T prim_cmd_parm14;         // 0x38
    UINT_T prim_cmd_parm15;         // 0x3c
    UINT_T secure_processor_cmd;    // 0x40
} CMD, *pCMD;

static CMD cmd;

static UINT_T host_interface_init(UINT_T timeout_ms, UINT_T adv_ver);
static UINT_T compute_digest(const UINT8_T* msg, INT_T len, UINT8_T* md, UINT_T algo);
static UINT_T platform_authentication(pTIM ptim);
static UINT_T verify_jtag_key(pTIM ptim);
static UINT_T tim_validation(pTIM ptim);
static UINT_T image_validation(UINT_T start_addr, UINT_T image_id, pTIM ptim, void* param);
static UINT_T read_profile_fuse(pTIM ptim, UINT_T addr);
static UINT_T host_interface_shutdown(void* para);

wtm3_crypto_funcs wtm3_crypto_funcs_impl = {
    &host_interface_init,
    &compute_digest,
    &platform_authentication,
	&verify_jtag_key,
    &tim_validation,
    &image_validation,
    &read_profile_fuse,
    &host_interface_shutdown,
};

void wtm3_interrupt_handler(void);
static void wtm3_error_proc(UINT_T err);

typedef UINT_T (*func_rtn_none_zero)(void);
static void reset_cmd(pCMD cmd);
static UINT_T exe_cmd(pCMD cmd);

static UINT_T wtm3_ready(void);
static UINT_T wtm3_tsr(UINT_T* tsr);
static UINT_T wtm3_prim_op_complete(void);
static UINT_T wait_func_none_zero(UINT_T timeout_us, func_rtn_none_zero func);
static UINT_T adv2dmstate(void);
static UINT_T wtm_reset(void);
static UINT_T wtm_init(void);

static fuse_conf_spec_t fuse_conf_spec(void* param);
static UINT_T bind_platform(pTIM ptim, void* param);
static UINT_T platform_binded(pTIM ptim, void* param);
static UINT_T config_platform(UINT_T* conf, UINT_T bit_cnt, fuse_type_enum type, pTIM ptim, void* para);
static UINT_T read_platform_config(UINT_T* conf, UINT_T* bit_cnt, fuse_type_enum type, void* para);
static UINT_T program_jtag_key(pTIM ptim, void* param);
static UINT_T read_jtag_key(UINT8_T* key, UINT_T* len, void* para);
static UINT_T program_usb_id(UINT8_T* usb, UINT_T len, void* para);
static UINT_T read_usb_id(UINT8_T* usb, UINT_T* len, void* para);
static UINT_T read_oem_unique_id(UINT8_T* oem_id, UINT_T len, void* para);
static UINT_T advance_life_cycle(UINT_T* lc, void* para);
static UINT_T read_life_cycle(UINT_T* lc, void* para);
static UINT_T advance_version(UINT_T* ver, void* para);
static UINT_T read_version(UINT_T* ver, void* para);
static UINT_T autocfg_platform(pTIM ptim, void* para);
static UINT_T program_rkek(void* para);
static UINT_T program_ec521_dk(void* para);

wtm3_provisioning_funcs wtm3_provisioning_funcs_impl = {
    &fuse_conf_spec,
    &bind_platform,
    &platform_binded,
    &config_platform,
    &read_platform_config,
    &program_jtag_key,
    &read_jtag_key,
    &program_usb_id,
    &read_usb_id,
    &read_oem_unique_id,
    &advance_life_cycle,
    &read_life_cycle,
    &advance_version,
    &read_version,
    &autocfg_platform,
	&program_rkek,
	&program_ec521_dk,
};

/**
 * This function is to initialize the wtm3 crypto and provisioning structs with
 * the corresponding function runtime locations. This is required for GNU compiler,
 * otherwise the functions in the struct aren't assigned their code start address correctly.
 * @param: timeout_ms - wait timeout in miliseconds.
 */

void wtm3_funcs_init()
{
   wtm3_crypto_funcs_impl.host_interface_init = host_interface_init;
   wtm3_crypto_funcs_impl.compute_digest = compute_digest;
   wtm3_crypto_funcs_impl.tim_validation = tim_validation;
   wtm3_crypto_funcs_impl.image_validation = image_validation;
   wtm3_crypto_funcs_impl.host_interface_shutdown = host_interface_shutdown;
   wtm3_provisioning_funcs_impl.advance_life_cycle = advance_life_cycle;
   wtm3_provisioning_funcs_impl.advance_version = advance_version;
   wtm3_provisioning_funcs_impl.autocfg_platform = autocfg_platform;
   wtm3_provisioning_funcs_impl.bind_platform = bind_platform;
   wtm3_provisioning_funcs_impl.config_platform = config_platform;
   wtm3_provisioning_funcs_impl.fuse_conf_spec = fuse_conf_spec;
   wtm3_provisioning_funcs_impl.platform_binded = platform_binded;
   wtm3_provisioning_funcs_impl.program_jtag_key = program_jtag_key;
   wtm3_provisioning_funcs_impl.read_life_cycle = read_life_cycle;
   wtm3_provisioning_funcs_impl.read_oem_unique_id = read_oem_unique_id;
   wtm3_provisioning_funcs_impl.read_platform_config = read_platform_config;
   wtm3_provisioning_funcs_impl.read_usb_id = read_usb_id;
   wtm3_provisioning_funcs_impl.read_version = read_version;
   wtm3_provisioning_funcs_impl.program_rkek = program_rkek;
   wtm3_provisioning_funcs_impl.program_ec521_dk = program_ec521_dk;

}

/**
 * wait wtm ready to accept command
 * @param: timeout_ms - wait timeout in miliseconds.
 */
static UINT_T host_interface_init(UINT_T timeout_ms, UINT_T adv_ver)
{
    UINT_T status;
    UINT_T ver;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;
    _prim_op_complete_flag = 0;

    /* wait wtm for ready */
    if (wait_func_none_zero(timeout_ms, wtm3_ready)) {
        return error_wtm_time_out;
    }
// WTM_INT is not used yet
#if 0
    _disable_irq1;
    _enable_wtm_addr_exception_int;
    _enable_wtm_bad_access_int;
    _enable_wtm_que_full_int;
    _enable_irq1;
#endif

    status = wtm_reset();
    if (status != STATUS_SUCCESS) {
        return status;
    }
    if (adv_ver) {
        /* version advance can only be done at initialization state */
        status = advance_version(&ver, NULL);
        if (status != STATUS_SUCCESS) {
            return status;
        }
    }
    status = wtm_init();
    return status;
}

/**
 * compute message digest
 * @param: msg  - pointer to message
 * @param: len  - message size
 * @param: md   - pointer to digeset
 * @param: algo - algorithm id
 *                SHA160(20) => SHA1
 *                SHA224(28) => SHA224
 *                SHA256(32) => SHA256
 * @return: 0 if success; error code otherwise
 *
 */
static UINT_T compute_digest(const UINT8_T* msg, INT_T len, UINT8_T* md, UINT_T algo)
{
    UINT_T                    status;
    UINT_T                    scheme;
    UINT_T                    i;
    UINT_T                    start_addr;
    UINT_T                    update_sz;
    UINT_T                    final_sz;
    UINT_T                    digest[8];
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    switch (algo) {
        case 20: /* SHA160 */
            scheme = SHA_1;
            break;
        case 28: /* SHA_224*/
            scheme = SHA_224;
            break;
        case 32: /* SHA256 */
            scheme = SHA_256;
            break;
        default:
            return error_invalid_algo;
    }

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0 = scheme;
    cmd.secure_processor_cmd = WTM_HASH_INIT;
    status = exe_cmd(&cmd);
    if (status != STATUS_SUCCESS) {
        return status;
    }

    start_addr = (UINT_T)msg;
    final_sz   = len;
    #define DTD_DATA_BYTE_SIZE 1024
    while (final_sz > DTD_DATA_BYTE_SIZE) {
        linked_dtd_list_init(&_dtd_list);
        i         = 0;
        update_sz = 0;
        do {
            update_sz += DTD_DATA_BYTE_SIZE;
            final_sz  -= DTD_DATA_BYTE_SIZE;
            pack_dtd(&(_dtds[i]), (UINT_T)start_addr, DTD_DATA_BYTE_SIZE / BYTE_PER_WORD);
            linked_dtd_list_append(&_dtd_list, &(_dtds[i]));
            i++;
            start_addr += DTD_DATA_BYTE_SIZE;
        } while (i < DTD_NUM_MAX && final_sz > DTD_DATA_BYTE_SIZE);

        reset_cmd(&cmd);
        cmd.prim_cmd_parm0       = NULL;
        cmd.prim_cmd_parm1       = update_sz;
        cmd.prim_cmd_parm14      = (UINT_T)_dtd_list.head;
        cmd.secure_processor_cmd = WTM_HASH_UPDATE;
        status = exe_cmd(&cmd);
        if (status != STATUS_SUCCESS) {
            return status;
        }
    }

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0       = start_addr;
    cmd.prim_cmd_parm1       = final_sz;
    cmd.prim_cmd_parm2       = (UINT_T)digest;
    cmd.secure_processor_cmd = WTM_HASH_FINAL;
    status = exe_cmd(&cmd);
    if (status != STATUS_SUCCESS) {
        return status;
    }
    for (i = 0; i < (algo / 4); i++) {
	    ((UINT_T*)md)[i] = digest[i];
    }

    return status;
}

/**
 * perform platform authentication
 * @param: ptim - pointer to TIM with OBM image.
 * @return: - STATUS_SUCCESS
 *          - STATUS_PERMISSION_VIOLATION
 *          - STATUS_INVALID_BINDING
 *          - STATUS_HW_FAILURE
 *          - error_cmd_timeout
 *          - error_invalid_param
 *          - error_invalid_algo
 */
static UINT_T platform_authentication(pTIM ptim)
{
    UINT_T                    status;
    UINT_T*                   publicKeyComp1;
    UINT_T*                   publicKeyComp2;
    UINT_T                    scheme;
    UINT_T                    keybitsz;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    /* wtm requires sha256 according to the doc */
    if (ptim == NULL) {
        return error_invalid_param;
    }

 	if (ptim->pConsTIM->VersionBind.Version < (TIM_3_3_00)) {
        /* before and with TIM version 3.2.xx all key seize is in byte */
 		keybitsz = ptim->pTBTIM_DS->KeySize * 8;
    }
 	else {
 		keybitsz = ptim->pTBTIM_DS->KeySize;
    }
	
	if ((ptim->pTBTIM_DS->DSAlgorithmID == PKCS1_v1_5_Ippcp) || (ptim->pTBTIM_DS->DSAlgorithmID == PKCS1_v2_1_Ippcp))
	{
		//crypto_scheme:	0x0000A100	PKCSv1_SHA1_1024RSA  
		//					0x0000A110	PKCSv1_SHA256_1024RSA
		//					0x0000A200	PKCSv1_SHA1_2048RSA  
		//					0x0000A210	PKCSv1_SHA256_2048RSA												
	    if (ptim->pTBTIM_DS->HashAlgorithmID == SHA256) {
	        scheme = (keybitsz == 1024) ? PKCSv1_SHA256_1024RSA : PKCSv1_SHA256_2048RSA;
	    }
	    else if (ptim->pTBTIM_DS->HashAlgorithmID == SHA160) {
	        scheme = (keybitsz == 1024) ? PKCSv1_SHA1_1024RSA : PKCSv1_SHA1_2048RSA;
	    }
	    else {
	        return error_invalid_algo;
	    }
	    publicKeyComp1 = ptim->pTBTIM_DS->Rsa.RSAModulus;
	    publicKeyComp2 = ptim->pTBTIM_DS->Rsa.RSAPublicExponent;
	}
	else if ((ptim->pTBTIM_DS->DSAlgorithmID == ECDSA_256) || (ptim->pTBTIM_DS->DSAlgorithmID == ECDSA_521))
	{
		//crypto_scheme:	0x0000B101	ECCP256_FIPS_DSA_SHA1  
		//					0x0000B111	ECCP256_FIPS_DSA_SHA256
		//					0x0000B141	ECCP256_FIPS_DSA_SHA512  
		//					0x0000B301	ECCP521_FIPS_DSA_SHA1  
		//					0x0000B311	ECCP521_FIPS_DSA_SHA256
		//					0x0000B341	ECCP521_FIPS_DSA_SHA512

		//SHA 512 in pTBTIM_DS is not supported yet
		/*if(ptim->pTBTIM_DS->HashAlgorithmID == SHA512) {
			scheme = (keybitsz == 256) ? ECCP256_FIPS_DSA_SHA512 : ECCP521_FIPS_DSA_SHA512;
	    }
	    else*/ if (ptim->pTBTIM_DS->HashAlgorithmID == SHA256) {
			scheme = (keybitsz == 256) ? ECCP256_FIPS_DSA_SHA256 : ECCP521_FIPS_DSA_SHA256;
	    }
	    else if (ptim->pTBTIM_DS->HashAlgorithmID == SHA160) {
			scheme = (keybitsz == 256) ? ECCP256_FIPS_DSA_SHA1 : ECCP521_FIPS_DSA_SHA1;
	    }
	    else {
	        return error_invalid_algo;
	    }
	    publicKeyComp1 = ptim->pTBTIM_DS->Ecdsa.ECDSAPublicKeyCompX;
	    publicKeyComp2 = ptim->pTBTIM_DS->Ecdsa.ECDSAPublicKeyCompY;
	}
	else {
		return error_invalid_algo;
	}

    reset_cmd(&cmd);	
	cmd.prim_cmd_parm0       = scheme;
	cmd.prim_cmd_parm1       = (UINT_T)publicKeyComp1;
	cmd.prim_cmd_parm2       = (UINT_T)publicKeyComp2;
    cmd.secure_processor_cmd = WTM_OEM_PLATFORM_VERIFY;
    status = exe_cmd(&cmd);
    return status;

}

/**
 * verify jtag key
 * @param: ptim - pointer to TIM with OBM image.
 * @return: - STATUS_SUCCESS
 *          - STATUS_PERMISSION_VIOLATION
 *          - STATUS_INVALID_BINDING
 *          - STATUS_HW_FAILURE
 *          - error_cmd_timeout
 *          - error_invalid_param
 *          - error_invalid_algo
 */	

static UINT_T verify_jtag_key(pTIM ptim)
{
    UINT_T                    status;
	pKEY_MOD_3_4_0			  pkey;
    UINT_T*                   publicKeyComp1;
    UINT_T*                   publicKeyComp2;
    UINT_T                    scheme;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

	pkey = FindKeyInTIM(ptim, JTAGIDENTIFIER);

	if (pkey == NULL) {
#ifndef __KEYS_H__
        return error_invalid_param;
#else
		// Workaround until SetTIMPointers works correctly for KEY_MOD
		pkey = pOEMJTAGKey;
#endif		
	}	 
	
	
	if ((pkey->EncryptAlgorithmID == PKCS1_v1_5_Ippcp) || (pkey->EncryptAlgorithmID == PKCS1_v2_1_Ippcp))
	{
		//crypto_scheme:	0x0000A100	PKCSv1_SHA1_1024RSA  
		//					0x0000A110	PKCSv1_SHA256_1024RSA
		//					0x0000A200	PKCSv1_SHA1_2048RSA  
		//					0x0000A210	PKCSv1_SHA256_2048RSA												
	    if (pkey->HashAlgorithmID == SHA256) {
	        scheme = (pkey->KeySize == 1024) ? PKCSv1_SHA256_1024RSA : PKCSv1_SHA256_2048RSA;
	    }
	    else if (pkey->HashAlgorithmID == SHA160) {
	        scheme = (pkey->KeySize == 1024) ? PKCSv1_SHA1_1024RSA : PKCSv1_SHA1_2048RSA;
	    }
	    else {
	        return error_invalid_algo;
	    }
	    publicKeyComp1 = pkey->Rsa.RSAModulus;
	    publicKeyComp2 = pkey->Rsa.RSAPublicExponent;
	}
	else if ((pkey->EncryptAlgorithmID == ECDSA_256) || (pkey->EncryptAlgorithmID == ECDSA_521))
	{
		//crypto_scheme:	0x0000B101	ECCP256_FIPS_DSA_SHA1  
		//					0x0000B111	ECCP256_FIPS_DSA_SHA256
		//					0x0000B141	ECCP256_FIPS_DSA_SHA512  
		//					0x0000B301	ECCP521_FIPS_DSA_SHA1  
		//					0x0000B311	ECCP521_FIPS_DSA_SHA256
		//					0x0000B341	ECCP521_FIPS_DSA_SHA512

		//SHA 512 in pTBTIM_DS is not supported yet
		/*if(pkey->HashAlgorithmID == SHA512) {
			scheme = (keybitsz == 256) ? ECCP256_FIPS_DSA_SHA512 : ECCP521_FIPS_DSA_SHA512;
	    }
	    else*/ if (pkey->HashAlgorithmID == SHA256) {
			scheme = (pkey->KeySize == 256) ? ECCP256_FIPS_DSA_SHA256 : ECCP521_FIPS_DSA_SHA256;
	    }
	    else if (pkey->HashAlgorithmID == SHA160) {
			scheme = (pkey->KeySize == 256) ? ECCP256_FIPS_DSA_SHA1 : ECCP521_FIPS_DSA_SHA1;
	    }
	    else {
	        return error_invalid_algo;
	    }
	    publicKeyComp1 = pkey->Ecdsa.PublicKeyCompX;
	    publicKeyComp2 = pkey->Ecdsa.PublicKeyCompY;
	}
	else { 
		return error_invalid_algo;
	}

	reset_cmd(&cmd);
    cmd.prim_cmd_parm0       = scheme;
    cmd.prim_cmd_parm1       = (UINT_T)publicKeyComp1;
    cmd.prim_cmd_parm2       = (UINT_T)publicKeyComp2;
    cmd.secure_processor_cmd = WTM_OEM_JTAG_KEY_VERIFY;
    status = exe_cmd(&cmd);
    return status;
}

/**
 * validate tim
 * @param: ptim - pointer to TIM
 * @return: - VALID_SIGNATURE
 *          - INVALID_SIGNATURE
 *          - error_invalid_param
 *          - error_cmd_timeout
 *          - error_invalid_tim
 */
static UINT_T tim_validation(pTIM ptim)
{
    UINT_T                 status;
    CRYPTO_SCHEME_ENUM          scheme;
    UINT_T                 keybitsz;
    UINT_T                 signsz;
    UINT_T                 start_addr;
    UINT_T                 update_sz;
    UINT_T                 final_sz;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    if (ptim == NULL) {
        return error_invalid_param;
    }
 	if (ptim->pConsTIM->VersionBind.Version < (TIM_3_3_00)) {
 		keybitsz = ptim->pTBTIM_DS->KeySize * 8;
    }
 	else {
 		keybitsz = ptim->pTBTIM_DS->KeySize;
    }

    switch (ptim->pTBTIM_DS->HashAlgorithmID) {
        case 20: /* SHA160 */
            signsz = 20;
            scheme = (keybitsz == 1024) ? PKCSv1_SHA1_1024RSA : PKCSv1_SHA1_2048RSA;
            break;

        case 28: /* SHA224 */
            signsz = 28;
            scheme = (keybitsz == 1024) ? PKCSv1_SHA224_1024RSA : PKCSv1_SHA224_2048RSA;
            break;

        case 32: /* SHA256 */
            signsz = 32;
            scheme = (keybitsz == 1024) ? PKCSv1_SHA256_1024RSA : PKCSv1_SHA256_2048RSA;
            break;

        default:
            return error_invalid_tim;
    }

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0       = scheme;
    cmd.prim_cmd_parm1       = (UINT_T)(ptim->pTBTIM_DS->Rsa.RSAModulus);
    cmd.prim_cmd_parm2       = (UINT_T)(ptim->pTBTIM_DS->Rsa.RSAPublicExponent);
    cmd.secure_processor_cmd = WTM_EMSA_PKCS1_V15_VERIFY_INIT;
    status = exe_cmd(&cmd);
    if (status != STATUS_SUCCESS) {
        return status;
    }
    start_addr = (UINT_T)ptim->pConsTIM;
    final_sz   = (UINT_T)ptim->pImg[0].ImageSizeToHash;
    #define UPDATE_DATA_SIZE (4 * 64)
    while (final_sz > UPDATE_DATA_SIZE) {
        reset_cmd(&cmd);
        cmd.prim_cmd_parm0       = (UINT_T)start_addr;
        cmd.prim_cmd_parm1       = (UINT_T)UPDATE_DATA_SIZE;
        cmd.secure_processor_cmd = WTM_EMSA_PKCS1_V15_VERIFY_UPDATE;
        status = exe_cmd(&cmd);
        if (status != STATUS_SUCCESS) {
            return status;
        }
        final_sz   -= UPDATE_DATA_SIZE;
        start_addr += UPDATE_DATA_SIZE;
    }

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0       = (UINT_T)start_addr;
    cmd.prim_cmd_parm1       = final_sz;
    cmd.prim_cmd_parm2       = (UINT_T)(ptim->pTBTIM_DS->Rsa.RSADigS);
    cmd.secure_processor_cmd = WTM_EMSA_PKCS1_V15_VERIFY_FINAL;
    status = exe_cmd(&cmd);
    if (status != STATUS_SUCCESS) {
        return status;
    }
    if (hi->cmd_status_0 != 1) {
       return STATUS_FAILURE;
    }
    else {
       return STATUS_SUCCESS;
    }
}


/**
 * valid a image defined in TIM
 * @param start_addr - start address of image;
 *                     0, if use the one defined from TIM
 * @param image_id   - the image id
 * @param ptim       - pointer to TIM
 * @param param      - not used shall be NULL
 * @return
 *                   - STATUS_HW_FAILURE
 *                   - NoError
 *                       . either no image found which considered success, or
 *                       . validation is passed
 *                   - InvalidImageHash
 *                   - error_invalid_algo
 *                   - error_cmd_timeout
 *                   - error_invalid_tim
 */
static UINT_T image_validation(UINT_T start_addr, UINT_T image_id, pTIM ptim, void* param)
{
    UINT_T                    status;
    UINT_T                    scheme;
    UINT_T                    i;
    pIMAGE_INFO_3_4_0            img_info;
    UINT_T                    update_sz;
    UINT_T                    final_sz;
    UINT_T                    digest[8];
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    img_info = FindImageInTIM(ptim, image_id);
    if (img_info == NULL) {
        return NoError;
    }

    switch (img_info->HashAlgorithmID) {
        case 20: /* SHA160 */
            scheme = SHA_1;
            break;
        case 28: /* SHA_224*/
            scheme = SHA_224;
            break;
        case 32: /* SHA256 */
            scheme = SHA_256;
            break;
        default:
            return error_invalid_algo;
    }

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0 = scheme;
    cmd.secure_processor_cmd = WTM_HASH_INIT;
    status = exe_cmd(&cmd);
    if (status != STATUS_SUCCESS) {
       return status;
    }

    if (start_addr == 0) {
        start_addr = img_info->LoadAddr;
    }
    final_sz = img_info->ImageSizeToHash;
    #define DTD_DATA_BYTE_SIZE 1024
    while (final_sz > DTD_DATA_BYTE_SIZE) {
    linked_dtd_list_init(&_dtd_list);
        i         = 0;
        update_sz = 0;
        do {
            update_sz += DTD_DATA_BYTE_SIZE;
            final_sz  -= DTD_DATA_BYTE_SIZE;
            pack_dtd(&(_dtds[i]), (UINT_T)start_addr, DTD_DATA_BYTE_SIZE / BYTE_PER_WORD);
            linked_dtd_list_append(&_dtd_list, &(_dtds[i]));
            i++;
            start_addr += DTD_DATA_BYTE_SIZE;
        } while (i < DTD_NUM_MAX && final_sz > DTD_DATA_BYTE_SIZE);

        reset_cmd(&cmd);
        cmd.prim_cmd_parm0       = NULL;
        cmd.prim_cmd_parm1       = update_sz;
        cmd.prim_cmd_parm14      = (UINT_T)_dtd_list.head;
        cmd.secure_processor_cmd = WTM_HASH_UPDATE;
        status = exe_cmd(&cmd);
        if (status != STATUS_SUCCESS) {
            return status;
        }
    }

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0       = start_addr;
    cmd.prim_cmd_parm1       = final_sz;
    cmd.prim_cmd_parm2       = (UINT_T)digest;
    cmd.secure_processor_cmd = WTM_HASH_FINAL;
    status = exe_cmd(&cmd);
    if (status != STATUS_SUCCESS) {
        return status;
    }
    for (i = 0; i < (img_info->HashAlgorithmID / 4); i++) {
	    if (img_info->Hash[i] != digest[i]) {
		    return InvalidImageHash;
        }
    }
    return NoError;
}

/**
 * read_profile_fuse
 * @return - STATUS_SUCCESS
 *         - STATUS_INVALID_REQUEST
 *         - error_cmd_timeout
 */
static UINT_T read_profile_fuse(pTIM ptim, UINT_T addr)
{
    UINT_T                    status;
	volatile UINT_T* pAddr = (volatile UINT_T*) addr;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0 = 0; // Valid request
    cmd.secure_processor_cmd = WTM_GET_SOC_POWER_POINT;
    status = exe_cmd(&cmd);
    *pAddr++	= hi->cmd_status_0;
    *pAddr++	= hi->cmd_status_1;
    *pAddr++	= hi->cmd_status_2;
    *pAddr		= hi->cmd_status_3;
    return status;
}

static UINT_T host_interface_shutdown(void* para)
{
    _disable_wtm_ints;
    return NoError;
}


void wtm3_interrupt_handler(void)
{
    UINT_T int_num;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    int_num = *(volatile UINT_T *)ICU_IRQ_SEL_INT_NUM_CORE1;
    if (int_num & ICU_IRQ_SEL_INT_NUM_INT_PENDING)
    {
        /* mask out pending status */
        int_num &= ~ICU_IRQ_SEL_INT_NUM_INT_PENDING;
        if (int_num != 40) {
            /* mis-fired */
            return;
        }
        if (hi->host_interrupt_register & WTM_PRIM_CMD_COMPLETE_MASK) {
            hi->host_interrupt_register |= WTM_PRIM_CMD_COMPLETE_MASK;
            _prim_op_complete_flag = 1;
        }
        else if (hi->host_interrupt_register & WTM_PJ4_ADDR_EXCPTION_MASK) {
            hi->host_interrupt_register |= WTM_PJ4_ADDR_EXCPTION_MASK;
            wtm3_error_proc(WTM_PJ4_ADDR_EXCPTION_MASK);
        }
        else if (hi->host_interrupt_register & WTM_QUE_BAD_ACCESS_MASK) {
            hi->host_interrupt_register |= WTM_QUE_BAD_ACCESS_MASK;
            wtm3_error_proc(WTM_QUE_BAD_ACCESS_MASK);
        }
        else if (hi->host_interrupt_register & WTM_QUE_QUE_FULL_MASK) {
            hi->host_interrupt_register |= WTM_QUE_QUE_FULL_MASK;
            wtm3_error_proc(WTM_QUE_QUE_FULL_MASK);
        }
    }
}


static void wtm3_error_proc(UINT_T err)
{
    UINT_T temp = err; while (1);
}


static void reset_cmd(pCMD cmd)
{
    UINT_T i;
    UINT_T* pcmd = &cmd->prim_cmd_parm0;
    for (i = 0; i <= 15; i++) {
        *pcmd++ = 0;
    }
}


static UINT_T exe_cmd(pCMD cmd)
{
    UINT_T i;
    UINT_T status;
    WTM_HOST_BIU* hi = WTM_HOST_IF;
    UINT_T* pcmd = &cmd->prim_cmd_parm0;
    UINT_T* phi  = &hi->prim_cmd_parm0;
    for (i = 0; i <= 16; i++) {
        *phi++ = *pcmd++;
    }
    status =  wait_func_none_zero(1000000, wtm3_prim_op_complete);
    if (status) {
        return error_cmd_timeout;
    }
    hi->host_interrupt_register = 0xFFFFFFFF;
    return hi->cmd_return_status;
}


static UINT_T wtm3_ready(void)
{
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;
    return  hi->cmd_fifo_status & WTM_CMD_STS_MASK;
}


static UINT_T wtm3_tsr(UINT_T* tsr)
{
    UINT_T status;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    reset_cmd(&cmd);
    cmd.secure_processor_cmd = WTM_GET_TRUST_STATUS_REGISTER;
    status = exe_cmd(&cmd);
    if (status != STATUS_SUCCESS) {
        return status;
    }
    *tsr = hi->cmd_status_0;
    return hi->cmd_return_status;
}


static UINT_T wtm3_prim_op_complete(void)
{
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;
	return hi->host_interrupt_register & WTM_PRIM_CMD_COMPLETE_MASK;

// WTM_INT is not used yet
#if 0
    if (hi->host_interrupt_mask & WTM_PRIM_CMD_COMPLETE_MASK == 0)
    {
        /* if operation complete interrupt is on use complete flag */
        _disable_wtm_cmd_complete_int;
        if (_prim_op_complete_flag != 0)
        {
            _prim_op_complete_flag = 0;
            _enable_wtm_cmd_complete_int;
            return 1;
        }
        else
        {
            _enable_wtm_cmd_complete_int;
            return 0;
        }
    }
    else
    {
        /* if operation complete interrupt is off use status check */
        return hi->host_interrupt_register & WTM_PRIM_CMD_COMPLETE_MASK;
    }
#endif
}


static UINT_T wait_func_none_zero(UINT_T timeout_us, func_rtn_none_zero func)
{
    UINT_T start_cnt;
    UINT_T end_cnt;

    start_cnt = GetOSCR0();
    do {
        if (func() != 0) {
	        return 0;
        }
	    end_cnt = GetOSCR0();
	    if (end_cnt < start_cnt) {
		    end_cnt += (0x0 - start_cnt);
        }
    } while (OSCR0IntervalInMicro(start_cnt, end_cnt) < timeout_us);
    return 1;
}


static UINT_T adv2dmstate(void)
{
    UINT_T status;
    UINT_T lc;

    status = read_life_cycle(&lc, NULL);
    if (status != STATUS_SUCCESS) {
        return status;
    }
    if (lc == LCS_STATE_DM) {
        return STATUS_SUCCESS;
    }
    else if (lc == LCS_STATE_CM) {
        /* wtm reports CM state for both VIRGIN and CM states */
        do {
            status = advance_life_cycle(&lc, NULL);
            if (status != STATUS_SUCCESS) {
                return status;
            }
        } while (lc < LCS_STATE_DM);
        return status;
    }
    else {
        return STATUS_FAILURE;
    }
}


static UINT_T wtm_reset(void)
{
    UINT_T status;
    reset_cmd(&cmd);
    cmd.secure_processor_cmd = WTM_RESET;
    status = exe_cmd(&cmd);
    return status;
}


static UINT_T wtm_init(void)
{
    UINT_T status;
    reset_cmd(&cmd);
    cmd.prim_cmd_parm0       = 0;  //Accelerator Operational State (Non-FIPS mode)
    cmd.secure_processor_cmd = WTM_INIT;
    status = exe_cmd(&cmd);
    return status;
}


/**
 * get fuse configuration specification
 *
 */
static fuse_conf_spec_t fuse_conf_spec(void* param)
{
    return block0_fuse_conf_spec;
}


/**
 * bind platform
 * @param ptim - pointer to TIM
 * @return - STATUS_SUCCESS
 *         - STATUS_FAILURE
 *         - STATUS_PERMISSION_VIOLATION
 *         - error_invalid_param
 *         - error_invalid_algo
 *         - error_cmd_timeout
 */
static UINT_T bind_platform(pTIM ptim, void* param)
{
    UINT_T                    status;
    UINT_T*                   publicKeyComp1;
    UINT_T*                   publicKeyComp2;
    UINT_T                    scheme;
    UINT_T                    keybitsz;
    UINT_T                    lc;
    UINT_T                    force_platform_bind;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    if (ptim == NULL)
        return error_invalid_param;

 	if (ptim->pConsTIM->VersionBind.Version < (TIM_3_3_00))
 		keybitsz = ptim->pTBTIM_DS->KeySize * 8;
 	else
 		keybitsz = ptim->pTBTIM_DS->KeySize;
	
	
	if ((ptim->pTBTIM_DS->DSAlgorithmID == PKCS1_v1_5_Ippcp) || (ptim->pTBTIM_DS->DSAlgorithmID == PKCS1_v2_1_Ippcp))
	{
		//crypto_scheme:	0x0000A100	PKCSv1_SHA1_1024RSA  
		//					0x0000A110	PKCSv1_SHA256_1024RSA
		//					0x0000A200	PKCSv1_SHA1_2048RSA  
		//					0x0000A210	PKCSv1_SHA256_2048RSA												
	    if (ptim->pTBTIM_DS->HashAlgorithmID == SHA256) {
	        scheme = (keybitsz == 1024) ? PKCSv1_SHA256_1024RSA : PKCSv1_SHA256_2048RSA;
	    }
	    else if (ptim->pTBTIM_DS->HashAlgorithmID == SHA160) {
	        scheme = (keybitsz == 1024) ? PKCSv1_SHA1_1024RSA : PKCSv1_SHA1_2048RSA;
	    }
	    else {
	        return error_invalid_algo;
	    }
	    publicKeyComp1 = ptim->pTBTIM_DS->Rsa.RSAModulus;
	    publicKeyComp2 = ptim->pTBTIM_DS->Rsa.RSAPublicExponent;
	}
	else if ((ptim->pTBTIM_DS->DSAlgorithmID == ECDSA_256) || (ptim->pTBTIM_DS->DSAlgorithmID == ECDSA_521))
	{
		//crypto_scheme:	0x0000B101	ECCP256_FIPS_DSA_SHA1  
		//					0x0000B111	ECCP256_FIPS_DSA_SHA256
		//					0x0000B141	ECCP256_FIPS_DSA_SHA512  
		//					0x0000B301	ECCP521_FIPS_DSA_SHA1  
		//					0x0000B311	ECCP521_FIPS_DSA_SHA256
		//					0x0000B341	ECCP521_FIPS_DSA_SHA512

		//SHA 512 in pTBTIM_DS is not supported yet
		/*if(ptim->pTBTIM_DS->HashAlgorithmID == SHA512) {
			scheme = (keybitsz == 256) ? ECCP256_FIPS_DSA_SHA512 : ECCP521_FIPS_DSA_SHA512;
	    }
	    else*/ if (ptim->pTBTIM_DS->HashAlgorithmID == SHA256) {
			scheme = (keybitsz == 256) ? ECCP256_FIPS_DSA_SHA256 : ECCP521_FIPS_DSA_SHA256;
	    }
	    else if (ptim->pTBTIM_DS->HashAlgorithmID == SHA160) {
			scheme = (keybitsz == 256) ? ECCP256_FIPS_DSA_SHA1 : ECCP521_FIPS_DSA_SHA1;
	    }
	    else {
	        return error_invalid_algo;
	    }
	    publicKeyComp1 = ptim->pTBTIM_DS->Ecdsa.ECDSAPublicKeyCompX;
	    publicKeyComp2 = ptim->pTBTIM_DS->Ecdsa.ECDSAPublicKeyCompY;
	}
	else {
		return error_invalid_algo;
	}

	// Advance to DM state
    status = adv2dmstate();
    if (status != STATUS_SUCCESS)
        return status;

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0       = scheme;
    cmd.prim_cmd_parm1       = (UINT_T)publicKeyComp1;
    cmd.prim_cmd_parm2       = (UINT_T)publicKeyComp2;
    cmd.secure_processor_cmd = WTM_OEM_PLATFORM_BIND;
    status = exe_cmd(&cmd);
    return status;
}


/**
 * Check if platform is binded
 * return non-zero if binded
 * As no binding key hash reading support from mailbox interface
 * the platform authentication function is used instead.
 */
static UINT_T platform_binded(pTIM ptim, void* param)
{
    UINT_T result = platform_authentication(ptim);
    if (result == STATUS_SUCCESS) {
      return 1;
    }
    else {
      return 0;
    }
}


/**
 * config platform
 * @param conf - pointer to configuration data buffer
 * @param bit_cnt - configuration data bit count
 * @param type - configuration type
 * @param pint - pointer to TIM
 * @return - STATUS_SUCCESS
 *         - STATUS_FAILURE
 *         - STATUS_PERMISSION_VIOLATION
 *         - error_invalid_param
 *         - error_cmd_timeout
 */
static UINT_T config_platform(UINT_T* conf, UINT_T bit_cnt, fuse_type_enum type, pTIM ptim, void* para)
{
    UINT_T                    status;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    if (conf == NULL || bit_cnt != 128)
        return error_invalid_param;

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0 = (UINT_T)conf;
#if LOCK_FUSE_BLOCK0_ECC
    cmd.prim_cmd_parm1 = 1;
#endif

    cmd.secure_processor_cmd = WTM_OTP_WRITE_PLATFORM_CONFIG;
    status = exe_cmd(&cmd);
    return status;
}

/**
 * program rkek
 * @return - STATUS_SUCCESS
 *         - STATUS_FAILURE
 *         - STATUS_PERMISSION_VIOLATION
 *         - error_cmd_timeout
 */
static UINT_T program_rkek(void* para)
{
    UINT_T                    status;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0 = 1; //Requests the WTM internal generation using its DRBG module, no reseeding
    cmd.secure_processor_cmd = WTM_RKEK_PROVISION;
    status = exe_cmd(&cmd);
    return status;
}

/**
 * program ec521 dk
 * @return - STATUS_SUCCESS
 *         - STATUS_FAILURE
 *         - STATUS_PERMISSION_VIOLATION
 *         - error_cmd_timeout
 */
static UINT_T program_ec521_dk(void* para)
{
    UINT_T                    status;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0 = 1; //Requests the WTM internal generation using its DRBG module, no reseeding
    cmd.secure_processor_cmd = WTM_EC521_DK_PROVISION;
    status = exe_cmd(&cmd);
    return status;
}


/**
 * read platform configuration
 * @param conf - pointer to configuration data buffer
 * @param bit_cnt - pointer to configuration data buffer length in bit count
 * @param type - configuration type
 * @param pint - pointer to TIM
 * @return - STATUS_SUCCESS
 *         - STATUS_FAILURE
 *         - STATUS_PERMISSION_VIOLATION
 *         - error_invalid_param
 *         - error_cmd_timeout
 */
static UINT_T read_platform_config(UINT_T* conf, UINT_T* bit_cnt, fuse_type_enum type, void* para)
{
    UINT_T                    status;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    if (conf == NULL || *bit_cnt < 128)
        return error_invalid_param;

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0 = (UINT_T)conf;
    cmd.secure_processor_cmd = WTM_OTP_READ_PLATFORM_CONFIG;
    status = exe_cmd(&cmd);
    *bit_cnt = 128;
    return status;
}


/**
 * bind jtag key
 * @param ptim - pointer to TIM
 * @return - STATUS_SUCCESS
 *         - STATUS_FAILURE
 *         - STATUS_PERMISSION_VIOLATION
 *         - error_invalid_param
 *         - error_invalid_algo
 *         - error_cmd_timeout
 */
static UINT_T program_jtag_key(pTIM ptim, void* param)
{
    UINT_T                    status;
	pKEY_MOD_3_4_0			  pkey;
    UINT_T*                   publicKeyComp1;
    UINT_T*                   publicKeyComp2;
    UINT_T                    scheme;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

	pkey = FindKeyInTIM(ptim, JTAGIDENTIFIER);

	if (pkey == NULL) {
#ifndef __KEYS_H__
        return error_invalid_param;
#else
		// Workaround until SetTIMPointers works correctly for KEY_MOD
		pkey = pOEMJTAGKey;
#endif		
	}	 
	
	
	if ((pkey->EncryptAlgorithmID == PKCS1_v1_5_Ippcp) || (pkey->EncryptAlgorithmID == PKCS1_v2_1_Ippcp))
	{
		//crypto_scheme:	0x0000A100	PKCSv1_SHA1_1024RSA  
		//					0x0000A110	PKCSv1_SHA256_1024RSA
		//					0x0000A200	PKCSv1_SHA1_2048RSA  
		//					0x0000A210	PKCSv1_SHA256_2048RSA												
	    if (pkey->HashAlgorithmID == SHA256) {
	        scheme = (pkey->KeySize == 1024) ? PKCSv1_SHA256_1024RSA : PKCSv1_SHA256_2048RSA;
	    }
	    else if (pkey->HashAlgorithmID == SHA160) {
	        scheme = (pkey->KeySize == 1024) ? PKCSv1_SHA1_1024RSA : PKCSv1_SHA1_2048RSA;
	    }
	    else {
	        return error_invalid_algo;
	    }
	    publicKeyComp1 = pkey->Rsa.RSAModulus;
	    publicKeyComp2 = pkey->Rsa.RSAPublicExponent;
	}
	else if ((pkey->EncryptAlgorithmID == ECDSA_256) || (pkey->EncryptAlgorithmID == ECDSA_521))
	{
		//crypto_scheme:	0x0000B101	ECCP256_FIPS_DSA_SHA1  
		//					0x0000B111	ECCP256_FIPS_DSA_SHA256
		//					0x0000B141	ECCP256_FIPS_DSA_SHA512  
		//					0x0000B301	ECCP521_FIPS_DSA_SHA1  
		//					0x0000B311	ECCP521_FIPS_DSA_SHA256
		//					0x0000B341	ECCP521_FIPS_DSA_SHA512

		//SHA 512 in pTBTIM_DS is not supported yet
		/*if(pkey->HashAlgorithmID == SHA512) {
			scheme = (keybitsz == 256) ? ECCP256_FIPS_DSA_SHA512 : ECCP521_FIPS_DSA_SHA512;
	    }
	    else*/ if (pkey->HashAlgorithmID == SHA256) {
			scheme = (pkey->KeySize == 256) ? ECCP256_FIPS_DSA_SHA256 : ECCP521_FIPS_DSA_SHA256;
	    }
	    else if (pkey->HashAlgorithmID == SHA160) {
			scheme = (pkey->KeySize == 256) ? ECCP256_FIPS_DSA_SHA1 : ECCP521_FIPS_DSA_SHA1;
	    }
	    else {
	        return error_invalid_algo;
	    }
	    publicKeyComp1 = pkey->Ecdsa.PublicKeyCompX;
	    publicKeyComp2 = pkey->Ecdsa.PublicKeyCompY;
	}
	else { 
		return error_invalid_algo;
	}

	reset_cmd(&cmd);
    cmd.prim_cmd_parm0       = scheme;
    cmd.prim_cmd_parm1       = (UINT_T)publicKeyComp1;
    cmd.prim_cmd_parm2       = (UINT_T)publicKeyComp2;
    cmd.secure_processor_cmd = WTM_OEM_JTAG_KEY_BIND;
    status = exe_cmd(&cmd);
    return status;
}

/**
 * read jtag key fuse bits
 * @param key: pointer to key buffer
 * @param len: buffer len
 * @return:
 */
static UINT_T read_jtag_key(UINT8_T* key, UINT_T* len, void* para)
{
    if (*len >= K_JTAG_HASH_FUSE_SIZE) {
        *len = K_JTAG_HASH_FUSE_SIZE;
    }
    return FUSE_ReadOemJtagHashKeyFuseBits((UINT_T*)key, *len);
}



/**
 * write usb id
 * @param usb - pointer to usb id buffer
 * @param len - length of usb id buffer
 * @return - STATUS_SUCCESS
 *           STATUS_FAILURE
 *           STATUS_PERMISSION_VIALIATION
 *           error_cmd_timeout
 *           error_invalid_param
 */
static UINT_T program_usb_id(UINT8_T* usb, UINT_T len, void* para)
{
    UINT_T                    status;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    if (usb == NULL || len != 4) {
        return error_invalid_param;
    }
    reset_cmd(&cmd);
    cmd.prim_cmd_parm0 = (UINT_T)usb;
    cmd.secure_processor_cmd = WTM_OEM_USBID_PROVISION;
    status = exe_cmd(&cmd);
    return status;
}


/**
 * read 64 bit usb id
 * @param usb - pointer to usb id buffer
 * @param len - pointer to usb id buffer length
 * @return - STATUS_SUCCESS
 *           STATUS_FAILURE
 *           STATUS_PERMISSION_VIALIATION
 *           error_cmd_timeout
 *           error_invalid_param
 */
static UINT_T read_usb_id(UINT8_T* usb, UINT_T* len, void* para)
{
    UINT_T                    status;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    if (usb == NULL || *len < 4) {
        return error_invalid_param;
    }
    reset_cmd(&cmd);
    cmd.prim_cmd_parm0 = (UINT_T)usb;
    cmd.secure_processor_cmd = WTM_OEM_USBID_READ;
    status = exe_cmd(&cmd);
    if (status != STATUS_SUCCESS) {
        return status;
    }
    *len = 4;
    return status;
}


/**
 * read oem unique id
 * @param oem_id - pointer to oem_id buffer
 * @param len - pointer to oem_id buffer length (has to be 64 bits)
 * @return - STATUS_SUCCESS
 *         - STATUS_FAILURE
 */
static UINT_T read_oem_unique_id(UINT8_T* oem_id, UINT_T len, void* para)
{
    UINT_T                    status;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    if (len != 16) {
        return error_invalid_param;
    }

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0 = (UINT_T)oem_id;
    cmd.secure_processor_cmd = WTM_SOC_UNIQUE_ID_READ;
    status = exe_cmd(&cmd);
    return status;
}


/**
 * advance life cycle
 * @param lc - pointer to lc
 * @return - STATUS_SUCCESS
 *         - STATUS_INVALID_REQUEST
 *         - STATUS_HW_FAILURE
 *         - STATUS_LIFECYCLE_SPENT
 */
static UINT_T advance_life_cycle(UINT_T* lc, void* para)
{
    UINT_T                    status;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0       = 0;  // Valid argument is 0
    cmd.secure_processor_cmd = WTM_LIFECYCLE_ADVANCE;
    status = exe_cmd(&cmd);
    *lc = hi->cmd_status_0;
    return status;
}


/**
 * read life cycle
 * @param lc - pointer to life cycle number
 * @return - STATUS_SUCCESS
 *         - STATUS_INVALID_REQUEST
 */
static UINT_T read_life_cycle(UINT_T* lc, void* para)
{
    UINT_T                    status;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    reset_cmd(&cmd);
    cmd.prim_cmd_parm0       = 0;  // Valid argument is 0
    cmd.secure_processor_cmd = WTM_LIFECYCLE_READ;
    status = exe_cmd(&cmd);
    *lc = hi->cmd_status_0;
    return status;
}


/**
 * advance version number
 * @param  ver - pointer to return version number
 * @return - STATUS_SUCCESS
 *         - STATUS_INVALID_REQUEST
 *         - STATUS_HW_FAILURE
 *         - STATUS_OUT_OF_RANGE
 */
static UINT_T advance_version(UINT_T* ver, void* para)
{
    UINT_T                    status;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    reset_cmd(&cmd);
    cmd.secure_processor_cmd = WTM_SOFTWARE_VERSION_ADVANCE;
    status = exe_cmd(&cmd);
    *ver = hi->cmd_status_0;
    return status;
}


/**
 * read software version
 * @param ver - pointer to return version number
 * @return - STATUS_SUCCESS
 *         - STATUS_INVALID_REQUEST
 *         - error_cmd_timeout
 */
static UINT_T read_version(UINT_T* ver, void* para)
{
    UINT_T                    status;
    volatile WTM_HOST_BIU* hi = WTM_HOST_IF;

    reset_cmd(&cmd);
    cmd.secure_processor_cmd = WTM_SOFTWARE_VERSION_READ;
    status = exe_cmd(&cmd);
    *ver = hi->cmd_status_0;
    return status;
}


/**
 * perform auto-platform configuration
 * @param ptim: pointer to TIM
 * @param para: NULL
 * @return - STATUS_SUCCESS, or
 *           other error code
 */
static UINT_T autocfg_platform(pTIM ptim, void* para)
{
    UINT_T           status = STATUS_SUCCESS;
    UINT_T           cfg[4];
    UINT_T           cfg_bitcnt = sizeof(cfg)* 8;
    fuse_conf_spec_t fuse_conf;
    UINT_T           flash_number;


    fuse_conf = bl_provisioning_funcs_api.fuse_conf_spec(NULL);

    /* Program JTAG key hash */
    status = program_jtag_key(ptim, para);
	if (status == STATUS_SUCCESS) { 	// if we programmed JTAG key (we don't have to)
		status = verify_jtag_key(ptim);
        if (status != STATUS_SUCCESS)	// fail if we programmed but could not verify
            return status;
	}


    // Program USB id
    if (fuse_conf.prg_usb_id != 0)
    {
        status = program_usb_id((UINT8_T*)fuse_conf.plat_usb_id,
                                fuse_conf.usb_byte_cnt, NULL);
        if (status != STATUS_SUCCESS)
            return status;

	}

	//Program Block 0
    if (fuse_conf.prg_cp_conf != 0 || fuse_conf.prg_ap_conf != NULL)
    {
        status = read_platform_config(cfg, &cfg_bitcnt, enum_fuse_type_dummy, NULL);
        if (status != STATUS_SUCCESS)
            return status;

        if (fuse_conf.prg_cp_conf != NULL)
            cfg[0] |= fuse_conf.plat_cp_conf[0];    // Bits 0-31

        if (fuse_conf.prg_ap_conf != NULL)
        {
            cfg[1] |= fuse_conf.plat_ap_conf[0];    // Bits 32-63
            cfg[2] |= fuse_conf.plat_ap_conf[1];    // Bits 64-95
            cfg[3] |= fuse_conf.plat_ap_conf[2];    // Bits 96-127

		   // Get Flash number from TIM
     	   if (ptim != NULL)
		   {
		   		// Get BootROM Operational Mode from TIM
				if (ptim->pConsTIM->VersionBind.Trusted)
					cfg[2] |= ( 2 << 10);		// Trusted
				else
					cfg[2] |= ( 1 << 10);		// Non-Trusted

		        // Get Flash number from TIM
		        flash_number = ((ptim->pConsTIM->FlashInfo.BootFlashSign) & 0x0F);
		        cfg[2] |= (flash_number << 14);
		   }
        }

        status = config_platform(cfg, cfg_bitcnt, enum_fuse_type_dummy, NULL, NULL);
        if (status != STATUS_SUCCESS)
            return status;
    }

/*	WTM does not support these yet.. */
    // Program EC521 DK
    status = program_ec521_dk(NULL);
    if (status != STATUS_SUCCESS)
    	return status;

	// Program RKEK ( - advances to DM state!)
    status = program_rkek(NULL);
    if (status != STATUS_SUCCESS)
    	return status;

	// Make sure we advance to DM state
    status = adv2dmstate();
    if (status != STATUS_SUCCESS)
    	return status;

	// At this point we are in DM state
	// If you want to advance to DD state, call advance_life_cycle one more time

    return status;
}
