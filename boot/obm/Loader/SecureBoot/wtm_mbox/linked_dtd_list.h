////////////////////////////////////////////////////////////////////////////////
//                                     NOTICE                                 //
//                                                                            //
//             COPYRIGHT MARVELL INTERNATIONAL LTD. AND ITS AFFILIATES        //
//                              ALL RIGHTS RESERVED                           //
//                                                                            //
//     The source code for this computer program is  CONFIDENTIAL  and a      //
//     TRADE SECRET of MARVELL  INTERNATIONAL  LTD. AND  ITS  AFFILIATES      //
//     ('MARVELL'). The receipt or possession of  this  program does not      //
//     convey any rights to  reproduce or  disclose  its contents, or to      //
//     manufacture,  use, or  sell  anything  that it  may  describe, in      //
//     whole or in part, without the specific written consent of MARVELL.     //
//     Any  reproduction  or  distribution  of this  program without the      //
//     express written consent of MARVELL is a violation of the copyright     //
//     laws and may subject you to criminal prosecution.                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#ifndef INCLUDE_LINKED_DTD_LIST_H
#define INCLUDE_LINKED_DTD_LIST_H

#include "Typedef.h"

typedef struct _dtd_t
{
    UINT_T            transfer_addr;
    UINT_T            transfer_words;
    struct _dtd_t* next;
    UINT_T            reserved;
} dtd_t;

typedef struct _linked_dtd_list_t
{
    dtd_t* head;
    dtd_t* tail;
    UINT_T  num_dtd;
} linked_dtd_list_t;


void zerolize_dtd(dtd_t *node);
void pack_dtd(dtd_t *node, UINT_T buf, UINT_T data_words);

void linked_dtd_list_init(linked_dtd_list_t* list);
int  linked_dtd_list_insert_after(linked_dtd_list_t* list, dtd_t* dtd, UINT_T pos);
void linked_dtd_list_append(linked_dtd_list_t* list, dtd_t* dtd);


#endif /* INCLUDE_LINKED_DTD_LIST_H */

