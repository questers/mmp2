/******************************************************************************
**	(C) Copyright 2007 Marvell International Ltd.  
**	All Rights Reserved
******************************************************************************/

/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document. 
**  Except as permitted by such license, no part of this document may be 
**  reproduced, stored in a retrieval system, or transmitted in any form or by  
**  any means without the express written consent of Intel Corporation. 
**
**  FILENAME:	Security.c
**
**  PURPOSE: 	Contain template OEM code for secure functions using IppCp
**                  
******************************************************************************/
#include "security.h"
#include "misc.h"
#include "Errors.h"
#if USE_IPPCP
#include "ippcp_security.h"
#endif
#if USE_WTM3
#include "wtm3_security.h"
#endif

// Generic pointers to security functions dynamically bound based on underlying security architecture.
SECURITY_FUNCTIONS SFs;

/********************	SecurityInitialization ***************************************
*	 This routine is in charge of doing any security Initialization needed
********************************************************************************************/
UINT_T SecurityInitialization(pFUSE_SET  pFuses)
{
 	UINT_T Retval = NoError;

	// Initialize to NULL value
	SFs.pSInitialize_F = NULL;

#if USE_WTM3
	if(pFuses->bits.UseWTM)
	{
		SFs.pSInitialize_F = &WTM3_SecurityInitialization;		
		SFs.pSShutdown_F = &WTM3_SecurityShutdown;		
		SFs.pSHAMessageDigest = &WTM3_SHAMessageDigest;
		SFs.pValidateTIM = &WTM3_ValidateTIM;
		SFs.pPlatformVerify = &WTM3_Platform_Verify;
	}
#endif
#if USE_IPPCP
	if(!(pFuses->bits.UseWTM))
	{
		SFs.pSInitialize_F = NULL;		
		SFs.pSShutdown_F = NULL;		
		SFs.pSHAMessageDigest = &IPPCP_SHAMessageDigest;
		SFs.pValidateTIM = &IPPCP_ValidateTIM;
		SFs.pPlatformVerify = &IPPCP_Platform_Verify;
	}
#endif

	if (SFs.pSInitialize_F != NULL)
		Retval = SFs.pSInitialize_F();
		 	
 	return Retval;
}


/******************** SecurityShutdown*********************************************************
/* This routine should do Security Driver related cleanup and hardware shutdown 
************************************************************************************************/

UINT_T SecurityShutdown(pFUSE_SET pFuses)
{
	UINT_T Retval = NoError;
	
	if (SFs.pSShutdown_F != NULL)
		Retval = SFs.pSShutdown_F();

	return Retval;
}



/********************	ValidateImage ******************************************************
*  Validate the Image based on a Hash passed in for the TIM or the hash for individual images
*  can be determined from IMAGE_INFO structure.
*
********************************************************************************************/
UINT_T ValidateImage(UINT_T ImageID, pTIM pTIM_h)
{
 UINT_T Retval = NoError;
 UINT_T StartAddr = 0;
 UINT_T EndAddr = 0;
 UINT_T	HashSize = 0;
 UINT_T	i, ImageSize;
 UINT_T CalculatedHash[WordLengthOf_SHA512];			// Contains 16 32-bit words
 UINT_T* HashInTIM;
 pIMAGE_INFO_3_4_0 pImageInfo;

 // If we are trying to Validate the TIM, it requires checking the DS. Special method to do that.
 if (ImageID == TIMIDENTIFIER)
 {
	Retval = SFs.pValidateTIM(pTIM_h);
	if (Retval != NoError)
		return InvalidTIMImageError;
	else
		return Retval;
 }

 // First find the image in the TIM, then get relavent data.  We always validate 
 // from the load address and require that the image has been loaded first
 pImageInfo = FindImageInTIM(pTIM_h, ImageID);
 if (pImageInfo == NULL) 		  // If we don't find an entry than it must not be a required image.
 	return NoError;

 StartAddr = pImageInfo->LoadAddr;		
 ImageSize = pImageInfo->ImageSize;
 HashInTIM = pImageInfo->Hash;
 HashSize  = pImageInfo->ImageSizeToHash;

 if(ImageSize < HashSize)
	return (HashSizeMismatch);
 if( HashSize == 0 ) 
 	return NoError;

 // Do the SHA Hash
 Retval = SFs.pSHAMessageDigest((unsigned char *)StartAddr, HashSize, (unsigned char *)CalculatedHash, (HASHALGORITHMID_T) pImageInfo->HashAlgorithmID);
 if (Retval != NoError)
	 return Retval;

 // Compare the 2 hashes
 for (i = 0; i < (pImageInfo->HashAlgorithmID / 4); i++)
 {
	if (HashInTIM[i] != CalculatedHash[i])
		return (InvalidImageHash);
 }

 return Retval;
}

/********************	ValidateKeyHashes ******************************************************
*  Validate the Keys based on a OTP Hash passed or from the Hash stored in TIM
*	If the OEM keys are being verified (TIMIDENTIFIER) do the platform verify PI as well
********************************************************************************************/
UINT_T KeyVerify(KEYMODULES_T KeyType, pTIM pTIM_h)
{
	UINT_T Retval = NoError;
	
	// Check if we are to Perform Platform Verification.
	if (KeyType == PlatformVerificationKey)
		Retval = SFs.pPlatformVerify(pTIM_h);

	// TBD Add support to verify other keys as well as needed.

	return Retval;
}


