/******************************************************************************
**  (C) Copyright 2007 Marvell International Ltd.  
**  All Rights Reserved
******************************************************************************/

/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document.
**  Except as permitted by such license, no part of this document may be
**  reproduced, stored in a retrieval system, or transmitted in any form or by
**  any means without the express written consent of Intel Corporation.
**
**  FILENAME:   Security.c
**
**  PURPOSE:    OBM specific security routines
**
**  History:    Initial Creation, 3/23/06
**
******************************************************************************/
#include "security.h"
#include "bl_security.h"
#include "Flash.h"
//#include "msys.h"
//#include "dsvl.h"
//#include "onenand_obm_interface.h"
#if VERBOSE_DEBUG
#include "ProtocolManager.h"
extern void ConvertIntToBuf8(unsigned char* StringPtr,unsigned int Value, int Width,unsigned int Count);
#endif

/****************** Program Platform Configuration Fuses *************
* This routine is called only if trusted boot is successful so far...
*********************************************************************/
UINT_T ProgramPlatformFuses(pFLASH_I pFLASH_I_h, UINT_T PlatformSettings)
{

 return NoError;
}
// This routine will program TIM verification keys to Caddo via Bind and program OTP registers for XIP.
// JTAG verification keys are also programmed to OTP.
UINT_T ProgramOBMImage(pTIM pTIM_h, P_FlashProperties_T pFlashP)
{
    UINT_T      i, Regval, Retval;
    UINT_T      ProtectionRegNum;
    UINT_T      ProgramStatus;
    UINT16_T    Lock0, Lock1;
    UINT16_T    ProtectionDataRead[26];
    UINT_T      WordData;
    USHORT      KeyIndex;
    static      UINT_T  FlashID= 0;
    UINT_T      JtagKeyFound = FALSE;

    return NoError;
}

UINT_T AutoBindPlatform(pTIM pTIM_h, P_FlashProperties_T pFlashP, UINT_T PlatformSettings)
{
  return NoError;
}

