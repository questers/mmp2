/******************************************************************************
**  (C) Copyright 2007 Marvell International Ltd.
**  All Rights Reserved
******************************************************************************/

/******************************************************************************
**
**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
**
**  This software as well as the software described in it is furnished under
**  license and may only be used or copied in accordance with the terms of the
**  license. The information in this file is furnished for informational use
**  only, is subject to change without notice, and should not be construed as
**  a commitment by Intel Corporation. Intel Corporation assumes no
**  responsibility or liability for any errors or inaccuracies that may appear
**  in this document or any software that may be provided in association with
**  this document.
**  Except as permitted by such license, no part of this document may be
**  reproduced, stored in a retrieval system, or transmitted in any form or by
**  any means without the express written consent of Intel Corporation.
**
**  FILENAME:   OBM.c
**
**  PURPOSE:    OBM main routine
**
**  History:    Initial Creation 4/23/06
******************************************************************************/

#include "Flash.h"
#include "ProtocolManager.h"
#include "Interrupts.h"
#include "tim.h"
#include "misc.h"
#include "PlatformConfig.h"
#if !FPGA
    //#include "platform_setup.h"
#endif
#include "timer.h"
#include "platform_interrupts.h"
#include "downloader.h"
#include "misc.h"
#include "FlasherBinary.h"
#include "loadoffsets.h"

#if TRUSTED
    #include "bl_security_if.h"
#endif
#if TVTD_NTCFG  /* Tavor TD non-trusted configuration */
    #include "geu_provisioning_impl.h"
#endif

#include "BootLoader.h"

// flash management library support.
#include "FM.h"
#include "DDR_Cfg.h"
#include "PMUA.h"

#if HSI
#include "hsi.h"
#endif

#if defined MMP2 || defined MMP3
#include "ipc_interface.h"
#endif

// Nand BCH workaround. Need the ECC mode enum
#if ASPNB0
#if SLC_NONFI
#include "nand.h"
#endif
#endif
#if ASPN
#include "mcu.h"
#include "mcu_extras.h"
#endif
#include "predefines.h"

#if USE_Y0_DC_BOARD
#include "board_config.h"
#endif

#if CONFIG_EMMC_ALTERNATE_BOOT
#include "sdmmc_api.h"
#include "Flash.h"
#endif

//Starting point for DL capability in OBM
//----------------------------------------
#define OBM31_DOWNLOAD_ENABLED 0
#define OBM_SIZ 0x400

#define TIMLOADADDRESS                      0x30000

#define MC_SLP_REG  ((volatile int *) PMUA_MC_SLP_REQ_AP)   // Memory Controller Sleep Req Reg
#define MC_SLP_ACK_MASK 0x2

// Routines defined outside
extern void EnableIrqInterrupts(void);
extern void DisableIrqInterrupts(void);
extern void   TransferControl(UINT_T, UINT_T, UINT_T, UINT_T);
void OBMFinalize(UINT_T Code, UINT8 *Tim_Area, pTIM pTIM_h, const UINT8 flashAlreadyConfigured, pFUSE_SET pFuses );
extern UINT_T SetUpUSBDescriptors (pTIM, UINT_T, UINT_T);
extern void CheckHibernate(pFUSE_SET pFuses, pTIM pTIM_h,  UINT8_T *TIM_Area);
extern UINT_T CheckResumeBLPackage(void *pTIM);

// {{ RAR
extern UINT_T GetUseSpareArea(FlashBootType_T fbt);
extern UINT_T SetUseSpareArea(UINT_T bUseSpareArea, FlashBootType_T fbt);
extern UINT_T GetSpareAreaSize(FlashBootType_T fbt);
extern UINT_T SetSpareAreaSize(UINT_T SASize, FlashBootType_T fbt);
extern PlatformInterruptInit();
extern UINT_T PlatformPreFlashSetup(void);
extern TransferSODTimerValue( UINT_T brValue);
//extern CheckDefaultClocks();

#if FBF
//pIMAGE_INFO DownloadFBFImages( UINT8_T *TIMArea, pTIM pTIM_h, pFUSE_SET pFuses, FlashBootType_T fbt );
pIMAGE_INFO_3_4_0 DownloadFBFImage( TIM *pTIM_h, pFUSE_SET pFuses );
#endif

UINT_T SetTIMPointers( UINT8_T * StartAddr, TIM *pTIM_h );
UINT CalcImageChecksum( UINT_T* DownloadArea, UINT_T ImageLength, UINT_T ImageType, UINT_T PageSize, UINT_T SpareAreaSize );
UINT_T CopyImageToFlash( UINT_T* DownloadArea, UINT_T FlashStartAddr, UINT_T Length, UINT_T ImageType,
                         UINT_T PageSize, UINT_T SpareAreaSize, FUSE_SET* pFuses);
UINT_T WriteFlash(UINT_T flash_addr, UINT_T buffer, UINT_T size, FlashBootType_T fbt);
void CheckForErrors(UINT_T Code);

// Reference
unsigned int LoadHSIImages( FUSE_SET *pFuses, pTIM pTIM_h );
pIMAGE_INFO_3_4_0 DownloadImages( UINT8 *TIM_Area, pTIM pTIM_h, pFUSE_SET pFuses, FlashBootType_T fbt );
pIMAGE_INFO_3_4_0 LoadOSLoader( pFUSE_SET pFuses, pTIM pTIM_h);
UINT_T validateLoadAddr(UINT_T LoadAddr, UINT_T Size);
pIMAGE_INFO_3_4_0 LoadAllImages(FUSE_SET *, TIM *); // loads all images in tim.
UINT_T ProbeFlash(pFUSE_SET pFuses, pTIM pTIM_h,  UINT8_T *TIM_Area);

// exception vector hooks: redirects vectors from ddr to local routines
void SetInterruptVector(void(*)());                                 // bridges from exception vector memory to isr
extern void SaveProgramState();

// for software upgrade support:
IMAGE_INFO_3_4_0 *SoftwareUpgrade( FUSE_SET *, TIM *, FlashBootType_T);   // initiates download if keypad pressed
void PlatformKeypadConfig();                                        // sets up pads and gpios for keypad
void InitializeKeypad();                                            // keypad controller initialization
unsigned long ReadKeypad(unsigned int *k);                          // return value of any key pressed.
void SetupTransferStruct(pTIM pTIM_h, UINT_T FusesValue);
unsigned int EraseImageAreaFromTIM(pTIM pTIM_h, unsigned int OktoErase, unsigned int FlashNumber, FUSE_SET* pFuses);

// for Loading images
pIMAGE_INFO_3_4_0 BootFromFlash(FUSE_SET *pFuses, pTIM pTIM_h);

// for frequency change support
void ConfigureOperatingMode( void *pTIM );

//Determine operational mode
// download and upgrade, signle TIM boot, or dual TIM boot
OPERATING_MODE_T DetermineOperatingMode(FUSE_SET *pFuses, pTIM pTIM_h);

#if USE_Y0_DC_BOARD
extern void bcfg_set_ddr_voltage(BOARD_TYPE bt);
#endif

// fatal error handler
void FatalError(UINT_T Code, pTIM pTIM_h, FUSE_SET* pFuses);
void _modv(int argcnt,...)  // halt, allows passing parameters for inspection. set pc to lr to resume...
{
    unsigned int odd=1;
    while(odd&1)odd+=2;     // need debugger to step beyond this loop after inspecting input args...
}

UINT_T  flash_write_verify(UINT_T flash_entry_addr, UINT_T ram_addr, UINT_T size, FlashBootType_T flash_type, void* info);

// SoftwareUpgrade:
IMAGE_INFO_3_4_0 *SoftwareUpgrade(
    FUSE_SET        *fuses,                     // the platformsettings contain info about which port to use
    TIM             *pTIM,                      // the tim can contain port overrides
    FlashBootType_T fbt                         // BOOT_FLASH or SAVE_STATE_FLASH
    )
{
    int             portopened = 0;

    UINT_T          imageRequested;

    UINT_T          retval = NoError;
    UINT_T          isDownload = FALSE;
    FUNC_STATUS     fs_Retval;

    IMAGE_INFO_3_4_0      *pBootImageInfo = NULL;
    volatile pProtocolISR pPortInterrupt;
    volatile pProtocolCmd pCommand;
    // select and open the port to be used to download the images.
    //   check the tim first for a port override.
    //   if no port override is specified in the tim, then just use the value in the fuses/platformsettings
    retval = InitTIMPort(fuses, pTIM);
    if (retval == NoError) {
        portopened = 1;     // download port from tim override is now connected.
    }
    // port opened yet? if not, try using the port identified in the platformsettings/fuses
    if( !portopened )
    {
        InitDefaultPort( fuses );
        portopened = 1;     // download port from platformsettings/fuses is now connected.
    }

    // the download port is opened. get all the images and burn them to flash.
    // start by trying to get a tim followed by individual files.
    // if that fails, then try getting an fbf bundle.
    // if that fails, then treat this a a fatal error.
    do
    {
        pPortInterrupt = getProtocolISR();
                
    }while (pPortInterrupt->PreambleReceived == FALSE || pPortInterrupt->CommandReceived == FALSE);
    
    pCommand = getProtocolCmd();
    
    switch(pCommand->Command)
    {
    case GetVersionCmd:
    
    // request the TIM first as originally implemented
        isDownload =  TRUE;
        fs_Retval = HandleRequest( (UINT_T)pTIM->pConsTIM, TIMIDENTIFIER);      // new TIM stored at pTIM->pConstTIM...probably BootROMDataArea
        if (fs_Retval.ErrorCode == NoError)
        {
            // pTIM is the newly downloaded tim.
            // It has a list of all the images that need to be downloaded and burned.
            // the DownloadImages function will use DDR to hold the images while writing them to flash.
            pBootImageInfo = DownloadImages( (UINT8_T*)pTIM->pConsTIM, pTIM, fuses, fbt );
        }
#if FBF
        else
        {
            // the download host doesn't have a tim to send.
            // reset the link and try again, but this time requesting an fbf bundle.
            ClearPortInterruptFlag();
            fs_Retval = HandleRequest((UINT_T)MASTER_BLOCK_HEADER_ADDR, FBFIDENTIFIER); // new header stored at MASTER_BLOCK_HEADER_ADDR
            if (fs_Retval.ErrorCode == NoError)
            {
                // MASTER_BLOCK_HEADER_ADDR is the newly downloaded fbf header.
                // It has a description of all the images that need to be downloaded and burned.
                // the DownloadFBFImages function will use DDR to hold the images while writing them to flash.
                pBootImageInfo = DownloadFBFImage( pTIM, fuses );
            }
        }
#endif
        break;
    case UploadDataHeaderCmd:
        fs_Retval = HandleRequest( (UINT_T)UPLOAD_DATA_AREA, NULL);      // UPLOAD_DATA_AREA in DDR
        // Does not matter if we had an error. Boot anyway.
        break;
    }
    // at this point, the images have been downloaded and burnt to flash.
    // also note that the last image downloaded is still in ddr. upon
    // return from this routine, that image will be copied to its base
    // location and executed.

    // We either did what we needed or failed. Either way disconnect with the tool until the next request.
   // now any download operation is complete.
    // put the system into as close to default state as possible by shutting down activated units.
    HandleDisconnect();
    //ShutdownPort(FFUART_D);
    ShutdownPort(ALTUART_D);
    ShutdownPort(DIFF_USB_D);
    ShutdownPort(U2D_USB_D);
    ShutdownPort(CI2_USB_D);
    if(!isDownload && (WhoAmI() == OBMIDENTIFIER) )
        pBootImageInfo = BootFromFlash(fuses, pTIM);
    return pBootImageInfo;

}
static UINT_T sRunningAsIdentifier; // DKB or OBM

void BootLoaderMain(UINT_T PlatformSettings, UINT_T startOfDayTimerVal, UINT_T ctimAddress )
{
    FUSE_SET                fuses;                  // Fuse information passed in from BootROM
    IMAGE_INFO_3_4_0        *pBootImageInfo = NULL;
    UINT_T                  retval = NoError;
    volatile unsigned int   count = 0;
    FlashBootType_T         fbt;                    // BOOT_FLASH or SAVE_STATE_FLASH. downstream flash routines need to know.
	OPERATING_MODE_T		bootMode = UPGRADESW;   // initalized so that coverity doesn't complain
    int                     upgradesw = 0;
	pOPT_SOCPROFILEID		pSocProfileID = NULL;

    UINT_T                  perform_wtm_ver_adv = 0;  /* Instuct WTM to perform version advance	*/

    pWTP_RESERVED_AREA_HEADER pWRAH = NULL;
    pOPT_GPIO_SET    pGPIO;                 // GPIO
    pGPIO_DEF pGPIOStruct;                  // GPIO pair structure
    UINT_T i;

    pIMAGE_INFO_3_4_0 pImage;
    UINT_T TimSize = 0;
    // Get a pointer to the TIM
    pTIM pTIM_h = GetTimPointer();

#if CONFIG_EMMC_ALTERNATE_BOOT
    UINT_T flash_number;
    UINT_T cont = 0;
    unsigned char ext_csd[256];
#endif

#if 0
	// Enable this infinite Loop to Avoid Accidental Burn of The Fuses	
	UINT_T odd = 1; 
	 while (odd & 1) {
		odd+=2;     // need debugger to step beyond this loop
	}
#endif

	// Start of BootLoaderMain executable section:


    // Initialize the SOD timer interface
    InitSODTimer();

	// enable the clocks for the peripherals that will be used
    CheckDefaultClocks();

	// also, the gpio unit should be clocked and pulled out of reset...
	*(volatile unsigned long*)(APBC_BASE+0x08) = 3;						// pull gpio unit out of reset.			APBC_BASE is d4015000.

	// check if the aib is being clocked and pulled out of reset, also. that's at offset 3c.


#if defined MMP2 || defined MMP3
	{
		P_TRANSFER_STRUCT	pTS_h;
		int					tim_found = 0;

		// MMP2 gets a pointer to the input parameters from the IPC.
		// IPCInit();	// can't call this again! the reset will cause all the bootrom info to be lost.
		Delay( 100 * 1000 );	// BootROM may not have written the IPC right away, so delay a bit.
		pTS_h = (P_TRANSFER_STRUCT)IPCRead(IPC_B);

		if( pTS_h )
		{
			PlatformSettings   = pTS_h->FuseVal;
			startOfDayTimerVal = pTS_h->SOD_ACCR0;

			for(i=0;i<pTS_h->num_data_pairs;i++)
			{
				if(pTS_h->data_pairs[i].data_id == TIM_DATA )
				{
					ctimAddress = pTS_h->data_pairs[i].location;
					tim_found = 1;
					break;
				}
			}
		}

		if( !tim_found )
		{
			// The pTS_h was not found via IPC.
			// Some options at this point:
			//	1. Probe flash, then locate TIM.
			//	2. Create a TBRX tim package with a well-know address (eg. hard coded)
			//
			// For now, selecting option 2.
			// Here's the TIM TBRX package:
			//		0x54425258			; "TBRX"
			//		0x00000018			; number of bytes in this package
			//		0x00020000			; location of TRANSFER_STRUCT
			//		0x00000001			; number of pairs
			//		0x54494D48			; "TIMH" - request a copy of the TIM.
			//		0xd1020000			; address to copy the TIM <= that's the important one.
			// Bottom line: We'll set ctimAddress to the same value
			// that's in the TIM TBRX package, which is: 0x00020100
			tim_found = 1;
			ctimAddress = 0xd1020000;
		}

	}
#endif


    // Give loader full download capability:
    PlatformSettings &= ~0x00020000;    // clear the port enable bits: for some reason, this bit is set from bootrom.
    PlatformSettings &= ~0x00003000;    // clear the download enable bits, set only one of these bits next.
    PlatformSettings |=  0x00001000;    // add usb download enable. wanted both, but it seems that if both are set, usb will never be checked.
    //PlatformSettings |=  0x00002000;    // add usb download enable. wanted both, but it seems that if both are set, usb will never be checked.


    // Initialize any fuse information passed in by the BootROM
    fuses.value = PlatformSettings;

    //_modv(TBR.Fuses.value, &TBR, &TBR.Fuses );

    #if OBMTEST
     fuses.bits.PlatformState = 0x6;    // Default to FLEX ONENAND
     fuses.bits.BootPlatformState = 0x6;
     fuses.bits.UARTDisable = 1;
     fuses.bits.USBDisable = 0;
	 ctimAddress = 0xD100A000;
    #endif

    fbt = BOOT_FLASH;

	#if KEYPAD
    // keypad initialization: required for detecting software upgrade request from the keypad.
    PlatformKeypadConfig();
    InitializeKeypad();
	#endif

    // initialize structures required by the protocol / download manager
    InitMessageQueue();
	#if VERBOSE_DEBUG
	InitPort(FFUART_D, &fuses);
	#endif
    #if JTAG_BREAKIN
    while(TRUE)
    {
        count+= 2;
        if( count == 0x1) break;
     };
    #endif
	AddMessage("===Hello OBM===");
    // set pointers to the TIM left in memory by the bootROM
    // we need this early in case the DDR needs to be configured
    //
    // NOTE: temporary workaround for Linux where the compiler is optimizing out the
    //       third parameter of the LoadTim call (TRUE).  THis forces us to load
    //       from flash and no flash is configured at this time.
    //  retval = LoadTim((UINT8_T*)ctimAddress, pTIM_h, TRUE );
    //  if( retval == TIMNotFound)

    SetTIMPointers((UINT8_T*)ctimAddress, pTIM_h);
    if( pTIM_h->pConsTIM->VersionBind.Identifier != TIMIDENTIFIER )
    {
		// set error to indicate no tim found and make it a fatal error
		//  Allow for a flash probe to occur if we are debugging.
        retval = TIMNotFound;
		#if OBMTEST
        retval = ProbeFlash(&fuses, &tim, (UINT8_T *)ctimAddress);
		#endif
	    if( retval != NoError)
		     FatalError(TIMNotFound, NULL, &fuses);
    }
    else
    {
		//find out our operating mode and load the correct TIM if we are in dual TIM mode
		bootMode = DetermineOperatingMode(&fuses, pTIM_h);
		if (UPGRADESW == bootMode)
			AddMessage("UpgradeSW mode");
		else if(DOWNLOAD == bootMode)
			AddMessage("Download mode");
     }

    // Now that we have finally loaded the tim, find out and set who we are. Should never be called again in BootLoader.
    SetRunningAsIdentifier(pTIM_h);
    
	// Go to the correct operating mode and configure DDR here.
	// Both Hibernate code and cold-boot handling may need DDR.
	// so it is better to check for and set it up in one place.
	// Cold-boot DDR users:
	//	USB needs interrupts.
	//	MMC needs interrupts.
	//	iSRAM may not be available, etc.

	// Operating mode should be set before the DDR is configured
	// so the DClk used by the memory controller is at the correct frequency.
	ConfigureOperatingMode( pTIM_h );

#if USE_Y0_DC_BOARD
    /* set up power IC for DDR */
    bcfg_set_ddr_voltage(PXA920_Y0_DC);
#endif
    CheckAndConfigureDDR(pTIM_h, &fuses);
	//ConfigureMemoryController( &tim, pvDDRScratchAreaAddr, ulDDRScratchAreaLen );

	// if this is a resume request, there will be no return from CheckHibernate....
    CheckHibernate(&fuses, pTIM_h, (UINT8_T *)ctimAddress);


    // Interrupts may be required, so enable them.
    //  For example, downloading over USB requires interrupts.
    SetInterruptVector( SaveProgramState );     // point the interrupt exception handler to our routine.
                                                // this allows us to run in-place out of isram
                                                // without having to enable the enable the mmu.
    INT_init();                 // turns off all interrupts...
    EnableIrqInterrupts();      // Enable Interrupts at the core
    IRQ_Glb_Ena();              // Enable Global Ints using the AP Global Interrupt Mask Register

    #if TRUSTED
    bl_security_api_init(&perform_wtm_ver_adv);
    #endif


    //if we get this far we found a TIM and know if we are downloading or booting
    //  with upgradesw set we attempt a download but if the download times out we
    //  still try a boot if configured as an OBMI
	//  We will need to differentiate between downloading and upgrading to allow
	// future functionality.

    if ((bootMode == UPGRADESW) ||(bootMode == DOWNLOAD))
    {
        pBootImageInfo = SoftwareUpgrade( &fuses, pTIM_h, fbt );
        #if TRUSTED

		// Check to see if there is an SOCPROFILEID packet in TIM
		pSocProfileID = (pOPT_SOCPROFILEID) FindPackageInReserved (&retval, pTIM_h, SOCPROFILEID);
		if(pSocProfileID != NULL)
			bl_security_funcs_api.read_profile_fuse(pTIM_h, pSocProfileID->StoreAddress);

        if (pBootImageInfo != NULL && fuses.bits.PlatformState == 0x0)
        {
			/* GEU: replace (void*)AddMessage with ``(void*)0xFFFFFFFF" to perform ATE config simulation */
			/* WTM: 2nd parameter has no effect! */
            retval = bl_security_funcs_api.perform_platbind(pTIM_h, (void*)AddMessage);
            if (retval != NoError) {
                FatalError(retval, pTIM_h, &fuses);
            }
            retval = bl_security_funcs_api.verify_platform(pTIM_h);
            if (retval != NoError) {
                FatalError(retval, pTIM_h, &fuses);
            }
			/* GEU: replace (void*)AddMessage with ``(void*)0xFFFFFFFF" to perform ATE config simulation */
			/* WTM: 2nd parameter has no effect! */
            retval = bl_security_funcs_api.perform_autoconf(pTIM_h, (void*)AddMessage);
            if( retval != NoError && retval != GEU_LastLogicalBitBurned)
            {
                FatalError(retval, pTIM_h, &fuses);
            }
        }
        #else
            #if TVTD_NTCFG
            /* allow TVTD to perform configurations, such as
             *  - platform boot state,
             *  - usb id, etc.
             * following the fuse config spec "tvtd_fuse_conf_spec".
             */
            if (pBootImageInfo != NULL && fuses.bits.PlatformState == 0x0) {
                retval = tvtd_autocfg_platform(pTIM_h, NULL);
                if (retval != NoError && retval != GEU_LastLogicalBitBurned) {
                    FatalError(retval, pTIM_h, &fuses);
                }
            }
            #endif
            #endif
    }
    else
    {
        pBootImageInfo = BootFromFlash(&fuses, pTIM_h);   
    }

#if HSI
	if (bootMode == HSI_BOOT)
	{
		LoadHSIImages( &fuses, pTIM_h);
	}
#endif

    #if TRUSTED
    bl_security_api_shutdown(NULL);
    #endif


    //first, set the partition back to TIM partition.  If there was an outstanding BBT, it would have been
    //written out in that call
    if((pTIM_h->pConsTIM->VersionBind.Version) >= TIM_3_2_00)
    {
        AddMessageError(PlatformBusy);
        AddMessage((UINT8_T*) ("*** Set Partition for Image...\0"));
        SetPartition(ReturnImgPartitionNumber(pTIM_h, pTIM_h->pImg), BOOT_FLASH);
        AddMessageError(PlatformReady);
    }

    // ready to execute the OS image.
	#if ASPNB0
    #if SLC_NONFI
	SetEccMode(ECC_HAMMING);
    #endif
	#endif
    Finalize_Flashes( BOOT_FLASH );    // this will flush the BBT information if necessary.


	printf("*** OBM BOOT OKAY ...\r\n");

    if( pBootImageInfo == NULL){
        // No image found for booting
        FatalError(ImageNotFound, pTIM_h, &fuses);
        return;
    }
    DisableIrqInterrupts();

	// We need this function for the DKB mode. 
	if ((bootMode == UPGRADESW) ||(bootMode == DOWNLOAD)){
		SetupTransferStruct(pTIM_h, fuses.value);
	}

	printf("*** OBM Jump to OS loader ...\r\n");
    TransferControl(pBootImageInfo->LoadAddr, pBootImageInfo->ImageSize, DDR_DOWNLOAD_AREA_ADDR, TEMP_BUFFER_AREA);

}
//
// SetupTransferStruct ()
//
//  When transferring to another image we need to setup the transfer structure corrrectly
//      May need to become a platform specific routine if the transfer data is different.
//      In particular when operating in DKB mode we need to satisfy the requirements
//      of the bl_startup.s init code.
//
void SetupTransferStruct(pTIM pTIM_h, UINT_T FusesValue)
{
    UINT_T *TransferArea = (UINT_T *)TEMP_BUFFER_AREA;

    *TransferArea++ = GetSODTimerValue();
    *TransferArea++ = FusesValue;
	*TransferArea++ = 0; // was error code from BootROM, for A0 and beyond
	*TransferArea++ = 0; // was reserved field from BootROM, for A0 and beyond
    *TransferArea = (UINT_T) pTIM_h->pConsTIM;
}


//------------------------------------------------------------------
// We write Pass/Fail code
// Write Block 0 for NAND flash
//-------------------------------------------------------------------
void FatalError(UINT_T Code, pTIM pTIM_h, FUSE_SET* pFuses)
{
 UINT_T retVal;
 pIMAGE_INFO_3_4_0 pImageInfo;
 UINT_T odd = 1;
 // Print out why we failed
 if (Code != DownloadPortError)
    AddMessageError(Code);
 while(odd&1)
 {
	 odd+=2;     // need debugger to step beyond this loop after inspecting input args...
 }

 return;
}

OPERATING_MODE_T DetermineOperatingMode(FUSE_SET *pFuses, pTIM pTIM_h){

  OPERATING_MODE_T mode = SINGLE_TIM_BOOT;
  unsigned int retval = NoError;
  pFLASH_I pFlashInfo = &pTIM_h->pConsTIM->FlashInfo;
  IMAGE_INFO_3_4_0          *pImageInfo;
  UINT8_T FlashNumber = 0;
  unsigned long loops;
  unsigned int            gotkey, k;
   #if KEYPAD
  // wait for a key to be hit. This will take precedence over TIM based operations.
    // experimentation has shown that a key won't register immediately -
    // - even if it was held down during power up. after about 0x13 loops
    // the key is detected. (that turns out to be about 0x6d3 oscr0 ticks).
    // use that information to set a limit on the amount of checking...
    // about 100 loops should be fine.
    for(loops=0;loops<100;loops++)
    {
        gotkey = ReadKeypad(&k);
        if( gotkey ) break;
        if( k != 0xff ) break;
    }
	#if WAYLAND
   	if( gotkey )
   	#else
    if( gotkey && ( k == KEYID_SOFTWAREUPGRADEREQUEST ) )
   	#endif
    {
        mode = UPGRADESW;
    }else
    #endif
    {
	    //No user request for software update
		//  Deterimine from TIM headers what to do.
		//   If DKB Identifier found then we load images
		//   If TIMD identifier found then we will load secondary TIM header
		//	 else dual a standard single tim boot.
	    if (FindImageInTIM( pTIM_h, DKBIDENTIFIER ))
	    {
	        mode = DOWNLOAD;
	    }else if (FindImageInTIM( pTIM_h, TIMDUALBOOTID )){
	        pImageInfo = LoadOSLoader( pFuses, pTIM_h );
			if(pImageInfo->ImageID != TIMDUALBOOTID)
				FatalError(TIMNotFound, NULL, pFuses);
		    mode =DUAL_TIM_BOOT;
		}
	}
  return mode;
}
//-----------------------------------------------------------------
// LoadAllImages()
// Loads all images except TIMH, OBMI, and OPTH (WTPRESERVEDAREAID)
// could optionally skip over other things like DKBI, JTAG, etc.
// but those probably won't be in the tims used at this time.
// Note: TIM must already be set up before calling this routine.
// FIXME: find a better mechanism to identify which images should be loaded by OBM.
//        for example a flag that indicates load by bootrom, loader or os.
// FIXME: is there any special handling required for loading the os image?
//        in LoadOSImages, there's a lot of checking, for exmaple platformstate, etc.
//-----------------------------------------------------------------
pIMAGE_INFO_3_4_0 LoadAllImages(FUSE_SET *pFuses, pTIM pTIM_h)
{
    UINT_T              Retval = NoError;
    int                 ImageOrder;
    int                 NumImages;
    IMAGE_INFO_3_4_0          *pImgInfo;
    IMAGE_INFO_3_4_0          *pBootImgInfo=NULL;
    int                 iInfoStructSize;    // depends on tim version: 3.2 and beyond vs. 3.1 and before
    unsigned long       loadAddr;
    pFLASH_I pFlashInfo = &pTIM_h->pConsTIM->FlashInfo;
    UINT8_T FlashNumber = 0;
#if LOAD_ALL_IMAGES
    if (pTIM_h->pConsTIM->VersionBind.Version >= TIM_3_4_00)
		iInfoStructSize = sizeof(IMAGE_INFO_3_4_0);    
	else if	(pTIM_h->pConsTIM->VersionBind.Version >= TIM_3_2_00)
		iInfoStructSize = sizeof(IMAGE_INFO_3_2_0);        
	else
	    iInfoStructSize = sizeof(IMAGE_INFO_3_1_0);
    
    pImgInfo        = pTIM_h->pImg;
    NumImages       = pTIM_h->pConsTIM->NumImages;

   // configure the flash that we booted from
    FlashNumber = (UINT8_T)pFlashInfo->BootFlashSign & 0xF;
    Retval = Configure_Flashes (FlashNumber, BOOT_FLASH);
    if (Retval != NoError) return pImgInfo;
    //turn on the flash management
    if(pTIM_h->pConsTIM->VersionBind.Version < TIM_3_2_00)
    {
        InitializeFM(LEGACY_METHOD, BOOT_FLASH);
    }else{
        InitializeFM(ALL_METHODS, BOOT_FLASH);
    }

    for( ImageOrder = 0; ImageOrder < NumImages; ImageOrder++ )
    {
        if( (pImgInfo->ImageID != OBMIDENTIFIER)
        &&  (pImgInfo->ImageID != WTPRESERVEDAREAID) )
        {
            // load this image.
            if (pImgInfo->ImageID == OSLOADERID)
            {
                pBootImgInfo = pImgInfo;
                loadAddr = DDR_DOWNLOAD_AREA_ADDR;
            }
            else
            {
                loadAddr = pImgInfo->LoadAddr;
            }
            AddMessageError(PlatformBusy);
            AddMessage((UINT8_T*) ("*** Set Partition for Image...\0"));
            SetPartition(ReturnImgPartitionNumber(pTIM_h, pImgInfo), BOOT_FLASH);
            AddMessageError(PlatformReady);
            Retval = ReadFlash(pImgInfo->FlashEntryAddr, loadAddr, pImgInfo->ImageSize, BOOT_FLASH);
            // FIXME: is BOOT_FLASH always OK? Should it be save_state_flash sometimes?
        }
        pImgInfo = (IMAGE_INFO_3_4_0*)((unsigned char*)pImgInfo + iInfoStructSize);   // index to next image info record.
    }
#endif
    return pBootImgInfo;
}

//-----------------------------------------------------------------
// LoadOSLoader()
//
//-----------------------------------------------------------------
pIMAGE_INFO_3_4_0 LoadOSLoader( FUSE_SET *pFuses, pTIM pTIM_h )
{
    UINT_T Retval = NoError;
    pIMAGE_INFO_3_4_0 pImageInfo = NULL;
    UINT_T  ImageID = 0;                                   /* initialize it as an invalidate image id */
    pFLASH_I pFlashInfo = &pTIM_h->pConsTIM->FlashInfo;
    UINT8_T FlashNumber = 0;
	unsigned long newTimLoadAddr;

    // If we get to this routine the bootROM loaded us as an OBM image and the TIM is located in
    // ISRAM at location passed in.  First we will do some basic checking to verify our setup
    #if TRUSTED
    AddMessage((UINT8_T*) ("*** Verifying TIM/NTIM ..."));
    Retval = bl_security_funcs_api.validate_tim(pTIM_h);

	if (Retval != NoError)
    {
		AddMessage((UINT8_T*) ("*** TIM validation failed\0"));
    	FatalError(InvalidTIMImageError, NULL, NULL);
    }
    #endif

    // Configure the flash that we booted from
    FlashNumber = (UINT8_T)pFlashInfo->BootFlashSign & 0xF;
    Retval = Configure_Flashes (FlashNumber, BOOT_FLASH);
    if (Retval != NoError) {
        return pImageInfo;
    }
    // Turn on the flash management
    if(pTIM_h->pConsTIM->VersionBind.Version < TIM_3_2_00)
    {
        InitializeFM(LEGACY_METHOD, BOOT_FLASH);
    }else{
        InitializeFM(ALL_METHODS, BOOT_FLASH);
    }
     // Print out who we are
    AddMessage((UINT8_T*) ("*** Running as OBM"));


   // Copy the image
    AddMessage((UINT8_T*) ("*** Loading Next Image..."));
    pImageInfo = FindImageInTIM(pTIM_h, OSLOADERID);
     if(pImageInfo == NULL)
    {
        pImageInfo = FindImageInTIM(pTIM_h, OBMIDENTIFIER);
        ImageID =  pImageInfo->NextImageID;
        pImageInfo = FindImageInTIM(pTIM_h, ImageID);
    }
     if(pImageInfo != NULL)
    {
        // copy the image to the Download area in DDR located at 0x60000.
        // the transfer control routine will then copy the image to the proper location
        // this is done to avoid conflict when an image needs to load where we are currently running from
        AddMessageError(PlatformBusy);
        AddMessage((UINT8_T*) ("*** Set Partition for Image...\0"));
       
        SetPartition(ReturnImgPartitionNumber(pTIM_h, pImageInfo), BOOT_FLASH);
        //SetPartition(pImageInfo->PartitionNumber, BOOT_FLASH);
        AddMessageError(PlatformReady);
		if (ImageID == TIMDUALBOOTID){
			//store the address of the new TIM in case the old one is overwritten.
			// set the image info pointer to the TIM image in the new tim header
			newTimLoadAddr = pImageInfo->LoadAddr;
        	Retval = ReadFlash(pImageInfo->FlashEntryAddr, pImageInfo->LoadAddr, pImageInfo->ImageSize, BOOT_FLASH);
			SetTIMPointers((UINT8_T*)newTimLoadAddr, pTIM_h);
			pImageInfo = FindImageInTIM(pTIM_h, TIMDUALBOOTID);
		}else{
        	Retval = ReadFlash(pImageInfo->FlashEntryAddr, DDR_DOWNLOAD_AREA_ADDR, pImageInfo->ImageSize, BOOT_FLASH);
		}
        if (Retval != NoError)
            pImageInfo = NULL;
        #if TRUSTED
        else{
             AddMessage((UINT8_T*) ("*** Validate Image..."));
             Retval = bl_security_funcs_api.validate_image((UINT8_T*)DDR_DOWNLOAD_AREA_ADDR, pImageInfo->ImageID, pTIM_h, NULL);
             if (Retval != NoError) {
                pImageInfo = NULL;
             }
        }
        #endif
    }
   return pImageInfo;

}


unsigned int LoadHSIImages( FUSE_SET *pFuses, pTIM pTIM_h )
{
#if HSI
    UINT_T Retval = NoError;
    TIM HSI_tim;
    pIMAGE_INFO_3_4_0 pImageInfo = NULL;
    UINT_T  buffer, start_time;

    // Copy the image
    AddMessage((UINT8_T*) ("*** Loading Next Image..."));
    pImageInfo = FindImageInTIM(pTIM_h, HSIBOOTID);
    if(pImageInfo == NULL)
		return ImageNotFound;
	
	//turn on HSI
	HSI_Init();
	Retval =  HSI_Start();
	if(Retval != NoError)
		return Retval;

    AddMessageError(PlatformBusy);
    AddMessage((UINT8_T*) ("*** Set Partition for Image...\0"));
    SetPartition(ReturnImgPartitionNumber(pTIM_h, pImageInfo), BOOT_FLASH);
    AddMessageError(PlatformReady);
	//read the HSI tim
	Retval = ReadFlash(pImageInfo->FlashEntryAddr, DDR_DOWNLOAD_AREA_ADDR, pImageInfo->ImageSize, BOOT_FLASH);
	buffer = DDR_DOWNLOAD_AREA_ADDR + pImageInfo->ImageSize;
	buffer = (buffer & 0xFFFFFFFC) + 4; //aligned to next word
	SetTIMPointers((UINT8_T*)DDR_DOWNLOAD_AREA_ADDR, &HSI_tim);
	
	//send the tim
	Retval = HSI_SendImage(DDR_DOWNLOAD_AREA_ADDR, pImageInfo->ImageSize);
	if(Retval != NoError)
		return Retval;

	//read the HSI OBM
	pImageInfo = FindImageInTIM(&HSI_tim, OBMIDENTIFIER);
	Retval = ReadFlash(pImageInfo->FlashEntryAddr, buffer, pImageInfo->ImageSize, BOOT_FLASH);
	//send the obm
	Retval = HSI_SendImage(buffer, pImageInfo->ImageSize);

	//wait for OBM to complete
	start_time = GetOSCR0();
	while(HSI_CheckImage() == 1)
	{	
		if(OSCR0IntervalInMilli(start_time, GetOSCR0()) > HSI_CMD_WAIT_TIME_MS*100 )
		{
			Retval = TimeOutError;
			break;
		}		
	}
	//disable hsi
	HSI_Shutdown();
#endif
	return NoError;

}


#if (SDHC_CNTL == 1)
	#include "sdhc1.h"
#endif
#if (SDHC_CNTL == 2)
	#include "sdhc2.h"
#endif

// ensure a partition exists for each entry in the partition table.
// FIXME: for now, only Samsung MMC is supported. To do: add ability to check partitions of any vendor, and both SD and MMC devices.
// FIXME: for now, only the existence of boot partitions is checked. To do: add ability to check for multiple user partitions and boot partitions.
UINT_T
ValidatePartitions(
	UINT_T	FlashNumber,
	void	*PartitionTable
	)
{
#if MMC_CODE
	// Initialize Flash Properties
	P_SDMMC_Properties_T	pSDMMCP = GetSDMMCProperties();
	P_PartitionTable_T		pPT = (P_PartitionTable_T)PartitionTable;
	P_PartitionInfo_T		pPI;
	int						pts;		// partitions counter.
	UINT_T					PartitionSizeFromTable;
	UINT_T					ActualPartitionSize;
	UINT_T					Retval;
	unsigned char			pBuffer[512];

	// Don't check SD cards...
	if( pSDMMCP->SD != XLLP_MMC )
		return NoError;	// treat this as a non-fatal situation.


	// Find the partition info entry in the table that needs to be validated...
	// boot partitions are 1 and 2. (The user partition is number 0).
	pts = pPT->NumPartitions;
	if( pts < 3 )						// make sure there are entries for partitions 1 and 2.
	{
		return InvalidSizeError;
	}

	// access the info for partition table entry 1 (or 2...it should be the same)
	pPI = (P_PartitionInfo_T)((unsigned char*)PartitionTable+sizeof(PartitionTable_T));		// first entry, for partition number "0"
	++pPI;																	// second entry, for partition number "1"

	PartitionSizeFromTable = pPI->EndAddr - pPI->StartAddr + 1;				// this is the value that needs to be matched.

	// now get the info from the card....
	// right now, only samsung boot partition validation is supported.
//	if( ( pSDMMCP->CardReg.CID.CID_VALUE[3] & 0x00ff0000 ) != 0x00150000 )	// FIXME: this requirement can be eliminated.
//		return NoError;	// treat this as a non-fatal error.					// it would be better to check the card spec version number.

	// get the contents of the extended CSD register. it has the boot partition size info.
	Retval = MM4_MMCReadEXTCSD((UINT_T*)pBuffer);
	if( Retval != NoError )
		return Retval;

	// examine the boot_size_mult field at offset 226 into the extcsd.
	ActualPartitionSize = (UINT_T)pBuffer[226] * 128 * 1024;				// see table 66, mmc4.4 spec for boot_size_mult units. (it is units of 128k)




	// enough information has been gathered to make a comparison.
	if( ActualPartitionSize == PartitionSizeFromTable )
		Retval = NoError;		// non zero: that's OK.
	else
		Retval = InvalidPlatformConfigError;
	return Retval;
#else
	return NotSupportedError;
#endif
}


UINT_T SamsungSetBootPartitionSize
(
	UINT_T	PartitionSize		// units is same as in partition table: bytes
)
{
#if MMC_CODE
	UINT_T					Retval;
	unsigned char			pBuffer[512];

	UINT_T					PartitionSizeArg;
	UINT_T					SuperBlockSize;


	// Initialize Flash Properties
	P_SDMMC_Properties_T	pSDMMCP = GetSDMMCProperties();


	// Calcuate the boot partition size argument.
	// It is based on Super Block Size.
	// The super block size is found within the "Smart Report" data block.
	// Get the "Smart Report" data block now...


	// Issue CMD62: Enter Smart Report mode, step 1
	MM4_SendSetupCommand(pSDMMCP, 62, MM4_CMD_TYPE_NORMAL, 0xEFAC62EC, MM4_48_RES_WITH_BUSY);
	Retval = MM4_Interpret_Response(pSDMMCP, MMC_RESPONSE_R1, 0x1000);

	// Issue CMD62: Enter Smart Report mode, step 2
	MM4_SendSetupCommand(pSDMMCP, 62, MM4_CMD_TYPE_NORMAL, 0x0000CCEE, MM4_48_RES_WITH_BUSY);
	Retval = MM4_Interpret_Response(pSDMMCP, MMC_RESPONSE_R1, 0x1000);

	// Read the Smart Report. (This sends a CMD18, read multiple block. Will the Smart Report be sent? The spec says to use a CMD17...)
	Retval = ReadFlash( 0, (UINT_T)pBuffer, 512, BOOT_FLASH );

	// Issue CMD62: Exit Smart Report mode, step 1
	MM4_SendSetupCommand(pSDMMCP, 62, MM4_CMD_TYPE_NORMAL, 0xEFAC62EC, MM4_48_RES_WITH_BUSY);
	Retval = MM4_Interpret_Response(pSDMMCP, MMC_RESPONSE_R1, 0x1000);

	// Issue CMD62: Exit Smart Report mode, step 2
	MM4_SendSetupCommand(pSDMMCP, 62, MM4_CMD_TYPE_NORMAL, 0x00DECCEE, MM4_48_RES_WITH_BUSY);
	Retval = MM4_Interpret_Response(pSDMMCP, MMC_RESPONSE_R1, 0x1000);

	// make sure the Smart Report data is valid...
	if( *(unsigned long*)(&pBuffer[0]) != 0xd2d2d2d2 )					// section 4.2.2 of data sheet.
		return NotFoundError;											// Smart Report signature not found.

	SuperBlockSize		= *(unsigned long*)(&pBuffer[4]);				// section 4.2.2 of data sheet.

	// FIXME: What to do if the requested PartitionSize is not a multiple of SuperBlockSize?
	// FIXME: For now, treat this as a fatal error: invalid partition size request.
	if( PartitionSize % (2*SuperBlockSize) )							// any remainder indicates PartitionSize
		return InvalidSizeError;										// is not a multiple of 2xSuperBlockSize.

	PartitionSizeArg	= ( PartitionSize / SuperBlockSize ) / 2;		// section 4.1.3 of data sheet. arg=#superblocks/2.

	// another requirement from section 4.1.3 is that the partition
	// is at least two super blocks in size.
	if( PartitionSizeArg < 1 )											// enforce minimum boot partition size
		return InvalidSizeError;										// see section 4.1.3 of data sheet.


	// Issue CMD62: Create Boot Partition, step 1
	MM4_SendSetupCommand(pSDMMCP, 62, MM4_CMD_TYPE_NORMAL, 0xEFAC62EC, MM4_48_RES_WITH_BUSY);
	Retval = MM4_Interpret_Response(pSDMMCP, MMC_RESPONSE_R1, 0x1000);

	// Issue CMD62: Create Boot Partition, step 2
	MM4_SendSetupCommand(pSDMMCP, 62, MM4_CMD_TYPE_NORMAL, 0xCBAEA7, MM4_48_RES_WITH_BUSY);
	Retval = MM4_Interpret_Response(pSDMMCP, MMC_RESPONSE_R1, 0x1000);

	// Issue CMD62: Create Boot Partition, step 3: provide the size argument.
	MM4_SendSetupCommand(pSDMMCP, 62, MM4_CMD_TYPE_NORMAL, PartitionSizeArg, MM4_48_RES_WITH_BUSY);
	Retval = MM4_Interpret_Response(pSDMMCP, MMC_RESPONSE_R1, 0x1000);

	// Check Card Status. This also waits for the ready bit to assert (bit 8)
	Retval = MM4_CheckCardStatus(pSDMMCP, 0x900, R1_LOCKEDCARDMASK);         // Make sure card is transfer mode



	// Retval = MM4_MMCReadEXTCSD (pBuffer);
	// pSDMMCP->State = READY;

	return Retval;
#else
	return NotSupportedError;
#endif

}


UINT_T
CreatePartitions(
	UINT_T	FlashNumber,
	void	*PartitionTable
	)
{
#if MMC_CODE
	// Initialize Flash Properties
	P_SDMMC_Properties_T	pSDMMCP = GetSDMMCProperties();
	P_PartitionTable_T		pPT = (P_PartitionTable_T)PartitionTable;
	P_PartitionInfo_T		pPI;
	int						pts;		// partitions counter.
	UINT_T					PartitionSize;
	UINT_T					Retval;


	// right now, only samsung boot partition creation is supported.
	if( ( pSDMMCP->CardReg.CID.CID_VALUE[3] & 0x00ff0000 ) != 0x00150000 )
		return NotSupportedError;


	// boot partitions are 1 and 2. (The user partition is number 0).
	pts = pPT->NumPartitions;
	if( pts < 3 )						// make sure there are entries for partitions 2 and 3.
	{
		return InvalidSizeError;
	}

	// access the info for partition table entry 1 (or 2...it should be the same)
	pPI = (P_PartitionInfo_T)((unsigned char*)PartitionTable+sizeof(PartitionTable_T));		// first entry, for partition number "0"
	++pPI;																	// second entry, for partition number "1"

	// Both boot partitions are the same size. So we just need to issue one create call.
	PartitionSize =  pPI->EndAddr - pPI->StartAddr + 1;
	Retval = SamsungSetBootPartitionSize( PartitionSize );

	return Retval;
#else
	return NotSupportedError;
#endif

}

/**
 * Verify flash write operation though comparision with read back.
 *
 * @param flash_entry_addr: Flash entry address
 * @param ram_addr:  RAM address of comparision data
 * @param size: Size of data for comparision in bytes
 * @param flash_type: Flash type
 * @param info: Used for backward compatibility
 *
 * @return: NoError if success; an error code otherwise.
 */
UINT_T  flash_write_verify(UINT_T flash_entry_addr,
                           UINT_T ram_addr,
                           UINT_T size,
                           FlashBootType_T flash_type,
                           void* info)
{

        UINT_T verify_addr;
        UINT_T rv;

       	// Note preset offset is located in ..\Loader\Include\TTC\ASPN\loadoffsets.h
	    // it is currently set to 16MB limiting the size of the image that can be verified
        verify_addr = DDR_DOWNLOAD_AREA_ADDR + DDR_VERIFY_BUFFER_OFFSET;
        AddMessageError(PlatformBusy);
        rv = ReadFlash(flash_entry_addr, verify_addr, size, flash_type);
        if (rv != NoError ) {
                FatalError(rv, (pTIM)info, (FUSE_SET*)info);
                return rv;
        }

	    while ( size > 0 ) {
		    if (*(UINT_T*)ram_addr == *(UINT_T*)verify_addr) {
			    size        -= 4;
			    ram_addr    += 4;
			    verify_addr += 4;
			    continue;
		    }
            else {
		      	AddMessage((UINT8_T*) ("***Image verify from Flash Failed...\0"));
                FatalError(FlashReadError, (pTIM)info, (FUSE_SET*)info);
                return FlashReadError;   // FixMe: no proper error code available
		    }
	    }
	    AddMessage((UINT8_T*) ("***Image verify from Flash Successful...\0"));
        AddMessageError(PlatformReady);

        return NoError;
}

/*
 * 
#define TIMIDENTIFIER		0x54494D48		// "TIMH"
#define TIMDUALBOOTID		0x54494D44		// "TIMD"
#define WTMIDENTIFIER		0x57544D49		// "WTMI"
#define OBMIDENTIFIER		0x4F424D49		// "OBMI"
#define MONITORIDENTIFIER	0x4D4F4E49		// "MONI"
#define TZSWIDENTIFIER		0x545A5349		// "TZSI"    This is also used as consumer ID
#define TBRIDENTIFIER		0x54425249		// "TBRI"	 This is also used as consumer ID
#define DKBIDENTIFIER		0x444B4249		// "DKBI"
#define JTAGIDENTIFIER		0x4A544147		// "JTAG"
#define PATCHIDENTIFIER		0x50415443		// "PATC"
#define TCAIDENTIFIER		0x5443414B		// "TCAK"
#define OSLOADERID			0x4F534C4F		// "OSLO"
#define PARTIONIDENTIFIER	0x50415254      // "PART"
#define FBFIDENTIFIER       0x46424649      // "FBFI" Should never appear in actual TIM file
#define HSIBOOTID			0x48533939		// "HSII"
*/
static char* imageid(UINT IMG_ID)
{
	static char* id[]=
	{
		"TIMH",
		"WTMI",
		"OBMI",
		"DKBI",
		"OSLO",
		"PART",
		"FBFI",
		"UNKNOWN"
	};
	if(TIMIDENTIFIER==IMG_ID) return id[0];
	else if(WTMIDENTIFIER==IMG_ID) return id[1];
	else if(OBMIDENTIFIER==IMG_ID) return id[2];
	else if(DKBIDENTIFIER==IMG_ID) return id[3];
	else if(OSLOADERID==IMG_ID) return id[4];
	else if(PARTIONIDENTIFIER==IMG_ID) return id[5];
	else if(FBFIDENTIFIER==IMG_ID) return id[6];
	else return id[7];	

}
#if 0
void dump_array(char* prefix,unsigned char* a,int length)
{
	int i;
	unsigned int offset=0;
	unsigned char *cptr = a;
	if(prefix) printf(prefix);
	for (i = 0; i < length; i++) {
		if (i % 16 == 0) {
			printf("\r\n0x%08x:", (unsigned int)offset);
			offset+=16;
		}
		printf(" %02x", *(cptr++));
	}
	printf("\r\n");
}
void dump_array_with_interval(char* prefix,unsigned char* a,pIMAGE_INFO_3_4_0 image,int interval)
{
	int i,len;
	int length = image->ImageSize;
	unsigned int offset=0;
	unsigned char *iptr = a;
	unsigned char *cptr = a;
	if(prefix) printf(prefix);
	printf("[%s] size[x%x]",imageid(image->ImageID),image->ImageSize);
	
	len = length>16?16:length;
	if(length>0x5000) length = 0x5000;
	if(interval<16) interval=16;
	do
	{				
		for (i = 0; i < len; i++) {
			if (i== 0) {
				printf("\r\n0x%08x:", (unsigned int)offset);
			}
			printf(" %02x", *(cptr++));
		}		
		offset+=interval;
		iptr+=interval;
		cptr=iptr;
		if(offset<length)
		{
			len=length-offset;
		}
		else
			len = 0;
		if(len>16) len=16;
	}while(offset<length);

}
#endif

//-----------------------------------------------------------------
// DownloadImages()
//   We get passed pointers to the current DKB TIM, and eventually
//   replace them with the OBM TIM
//
//
//-----------------------------------------------------------------
pIMAGE_INFO_3_4_0 DownloadImages( UINT8 *TIM_Area, pTIM pTIM_h, pFUSE_SET pFuses, FlashBootType_T fbt )
{
  FUNC_STATUS   FS_Retval;
  UINT_T        Retval = GeneralError;
  pIMAGE_INFO_3_4_0   pImageInfo = NULL;
  UINT8_T       FlashNumber = 0;
  UINT_T page, PageSize, BlkSize;
  UINT_T size;
  int                 NumImages;
  int                 iInfoStructSize;    // depends on tim version: 3.2 and beyond vs. 3.1 and before
  unsigned long       loadAddr, verifyAddr, blkZeroBuf, length, remainingLength;
  int         		  OKtoErase = 1;              // SPI flashes only need one erase operations
  unsigned int	      blkZeroPageIndex = 0;
  unsigned int	      blkZeroDataLength = 0;
  P_PartitionInfo_T pPI;

    // Now look to see if the TIM format is valid
    // and if so, setup the TIM pointers in pTIM_h
    //--------------------------------------------
    Retval = LoadTim(TIM_Area, pTIM_h, TRUE);
    if (Retval != NoError)
    {
        AddMessage((UINT8_T*) ("*** TIM not recognized...\0"));
        pImageInfo = NULL;
        return pImageInfo;
    }
     #if TRUSTED
    // Validate TIM
    AddMessage((UINT8_T*) ("*** Validating TIM...\0"));
    Retval = bl_security_funcs_api.validate_tim(pTIM_h);

	if (Retval != NoError)
    {
		AddMessage((UINT8_T*) ("*** TIM validation failed\0"));
    	FatalError(InvalidTIMImageError, NULL, NULL);
    }
    #endif


    FlashNumber = (pTIM_h->pConsTIM->FlashInfo.BootFlashSign) & 0xFF;

    // mmc devices automatically erase on writes, so disable the erase enable flag
    if( FlashNumber == 0x08 ) OKtoErase = 0;
    if( FlashNumber == 0x09 ) OKtoErase = 0;
    if( FlashNumber == 0x0b ) OKtoErase = 0;

    // Is this platform initialized? If yes, ensure BootFlashSign in TIM matches PlatformState.
    if( (pFuses->bits.PlatformState != 0x0) && (pFuses->bits.PlatformState != FlashNumber) )
    {
        pImageInfo = NULL;
        return pImageInfo;
    }

	AddMessageError(PlatformBusy);

    Retval = Configure_Flashes (FlashNumber, BOOT_FLASH);
    #if ASPN_MLC_BCH
    SetEccMode(ECC_BCH);
    #endif 
    if( Retval != NoError)
    {
       AddMessage((UINT8_T*) ("*** Flash config failed in download\0"));
       pImageInfo = NULL;
       return pImageInfo;
    }
	// Initialize the block zero buffer
	// this is used to pre-load the TIM, Partition table, FBBT, and image
	// It is written out last in order to comply with write ordering for MLC
	BlkSize			= GetBlockSize(BOOT_FLASH);
 	blkZeroBuf 		= (unsigned long)DDR_BLK_ZERO_BUF_ADDR;
	memset((UINT8_T *)blkZeroBuf, 0xFF, BlkSize);

	// Copy TIM to block zero buffer region and adjust page index
	// Using a while loop to avoid issue with GNU compiler and certain operators
    PageSize = GetPageSize(BOOT_FLASH);
	memcpy((UINT_T *)blkZeroBuf, (UINT_T *)TIM_Area, pTIM_h->pImg->ImageSize);
    // advance blkZeroPageIndex to next blank page index
    size          = 0;
    while (size < pTIM_h->pImg->ImageSize) {
		 blkZeroPageIndex++;
        size += PageSize;
    }
	AddMessageError(PlatformReady);
    //turn on the flash management
    if(pTIM_h->pConsTIM->VersionBind.Version < TIM_3_2_00)
    {
        AddMessageError(PlatformBusy);
        InitializeFM(LEGACY_METHOD, BOOT_FLASH);
        AddMessageError(PlatformReady);
    }
    else
    {
        loadAddr = DDR_DOWNLOAD_AREA_ADDR;
        //check to see if there is a partition table
        FS_Retval = HandleRequest(loadAddr, PARTIONIDENTIFIER);
        //if there is a partition table, load it into Flash management
        if(FS_Retval.ErrorCode == NoError)
        {
			// for MMC, make sure the requested partitions exist.
			if( (FlashNumber == 0x08 ) || (FlashNumber == 0x09) || (FlashNumber == 0x0b) )
			{
				AddMessageError(PlatformBusy);
				AddMessage((UINT8_T*) ("*** Validating partitions\0"));
				Retval = ValidatePartitions( FlashNumber, (void*)loadAddr );	// compare existing partitions with partition table.
				if( Retval != NoError )											// existing partitions don't match the partition table?
				{
					AddMessage((UINT8_T*) ("*** Creating partitions (partitions did not match table)\0"));
					Retval = CreatePartitions( FlashNumber, (void*)loadAddr );	// issue commands to create partitions.

					if( Retval == NoError )
					{
						// Successfully created partitions.
						// The create partition process requires a power cycle.
						// Display a message and halt.
						AddMessage((UINT8_T*) ("*** Partitions created.\0"));
						AddMessage((UINT8_T*) ("*** Please power cycle platform to resume DKB operation.\0"));

					}
					else
					{
						// Failed to create partitions.
						// Display a message and halt.
						AddMessage((UINT8_T*) ("*** Fail to create partition.\0"));
                                                AddMessage((UINT8_T*) ("*** Revalidate images after DKB operation completes.\0"));
					}
					// disconnect from the host session. all the queued up log messages will not be displayed until disconnected.
					AddMessageError(PlatformReady);
					AddMessageError(PlatformDisconnect);
					HandleDisconnect();
					ShutdownPort(CI2_USB_D);	// try to break the hosts connection.
					FatalError(Retval, pTIM_h, pFuses);
				}
				else
				{
					AddMessage((UINT8_T*) ("*** Existing partitions will be used.\0"));
					AddMessageError(PlatformReady);
				}
			}
            AddMessageError(PlatformBusy);
            ClearFM(BOOT_FLASH);
            //figure out the first page AFTER the TIM
            pImageInfo = FindImageInTIM(pTIM_h, TIMIDENTIFIER);
            page = pImageInfo->ImageSize / PageSize;
            if((page * PageSize) != pImageInfo->ImageSize) page++;
            LoadExternalPartitionTable((UINT*)DDR_DOWNLOAD_AREA_ADDR, page * PageSize);
            AddMessageError(PlatformReady);

        }else{
           AddMessageError(PlatformBusy);
		   pPI = NULL;
		   CreateBBT_Marvell(pPI);
           AddMessageError(PlatformReady);
	    }

        AddMessageError(PlatformBusy);
		length =  CopyTablesToBuffer( (UINT_T *)blkZeroBuf, blkZeroPageIndex);
        blkZeroDataLength = size + length;
		while (size < blkZeroDataLength) {
			 size += PageSize;
			 blkZeroPageIndex++;
		}

	    AddMessageError(PlatformReady);

      }

    //added:    We want to download the images in reverse order
    //          This will make sure we download the OBM (or Boot Image) last
    //          Which means a copy of this image will be in the scratch area, and we can
    //          move it to it's load address without having to read it back from flash
	//
	// Due to various flash requirements we also need to erase the flash prior to writng images
	//  this allows images to be written back to back within one block.  Previous code would
	//  write and image	then erase it later if another image was on the same block
	//
	// The blk 0 buffer is to allow us to write the TIM header last in case any other image fails
	//    we do not want the platform to get stuck with a valid TIM but no OBM leaving it crippled
	//
	if (pTIM_h->pConsTIM->VersionBind.Version >= TIM_3_4_00)
		iInfoStructSize = sizeof(IMAGE_INFO_3_4_0);    
	else if	(pTIM_h->pConsTIM->VersionBind.Version >= TIM_3_2_00)
		iInfoStructSize = sizeof(IMAGE_INFO_3_2_0);        
	else
	    iInfoStructSize = sizeof(IMAGE_INFO_3_1_0);

    NumImages       = pTIM_h->pConsTIM->NumImages;
    pImageInfo      = (IMAGE_INFO_3_4_0*)((unsigned char*)pTIM_h->pImg + (NumImages-1)*iInfoStructSize);  // index to last image info record.
    // Erase blocks based on the TIM images
    //
    AddMessageError(PlatformBusy);
   
	if(OKtoErase)
		EraseImageAreaFromTIM(pTIM_h, OKtoErase, FlashNumber, pFuses);
    AddMessageError(PlatformReady);
    NumImages       = pTIM_h->pConsTIM->NumImages;
    pImageInfo      = (IMAGE_INFO_3_4_0*)((unsigned char*)pTIM_h->pImg + (NumImages-1)*iInfoStructSize);  // index to last image info record.

    while (NumImages && (Retval == NoError))
    {
        // Download the image described by pImageInfo
        // if this is the first image (the TIM), it has already been downloaded. so skip the download
        if(NumImages>1)
        {
            loadAddr = DDR_DOWNLOAD_AREA_ADDR;
            printf("*** Downloading Image [%s]...\r\n",imageid(pImageInfo->ImageID));
            FS_Retval = HandleRequest(loadAddr, pImageInfo->ImageID);
			/*if(WTMIDENTIFIER==pImageInfo->ImageID)
			{
			//||OBMIDENTIFIER
				//WTMIDENTIFIER==pImageInfo->ImageID||
				//OSLOADERID==pImageInfo->ImageID
				//dump_array_with_interval("dump image",loadAddr,pImageInfo,0);
			}*/
            printf("*** Image Download Complete\r\n");
            AddMessageError(PlatformBusy);

            Retval = FS_Retval.ErrorCode;
            if (Retval != NoError)
                break;

            #if TRUSTED
            // Validate the Image
            AddMessage((UINT8_T*) ("*** Validating Image...\0"));
            Retval = bl_security_funcs_api.validate_image((UINT8_T*)loadAddr, pImageInfo->ImageID, pTIM_h, NULL);
            if (Retval != NoError)
                break;
            AddMessage((UINT8_T*) ("*** Image Validation Successful\0"));
            #endif

            AddMessageError(PlatformReady);

        }
        else
        {
	        loadAddr = (unsigned long)TIM_Area;    // first image, TIM. use the address provided by caller.
	    }

        #if COPYIMAGESTOFLASH
        // Copy the Image
        printf("*** Copy Image to Flash...\r\n");
        if ((pTIM_h->pConsTIM->VersionBind.Version) >= TIM_3_2_00)
        {
            AddMessageError(PlatformBusy);
			printf("*** Set Partition[%d] for Image[%s]...\r\n",
				ReturnImgPartitionNumber(pTIM_h, pImageInfo),
				imageid(pImageInfo->ImageID));
            SetPartition(ReturnImgPartitionNumber(pTIM_h, pImageInfo), BOOT_FLASH);
            AddMessageError(PlatformReady);
        }
        AddMessageError(PlatformBusy);

		// Write out the image
		//	If the flash uses BBM the use a buffer for Block 0 so that we can adhere to write ordering
		//      else just write out the image
		if(GetFlashProperties(BOOT_FLASH)->FlashSettings.UseBBM){

			if((UINT_T)loadAddr == (UINT_T)TIM_Area)
			{
				// only write out the number of pages required
				// At this point the TIM, partition table and FBBT are in the buffer when MBBM is used
				// for Legacy BBT need to use the update BBT function
				#if ASPNB0
                #if SLC_NONFI
				SetEccMode(ECC_BCH);
                #endif
				#endif
	        	Retval = WriteFlash( pImageInfo->FlashEntryAddr, blkZeroBuf, blkZeroPageIndex * PageSize, BOOT_FLASH );
                #if VERIFY_WRITE
                Retval = flash_write_verify( pImageInfo->FlashEntryAddr, blkZeroBuf, blkZeroPageIndex * PageSize, BOOT_FLASH, (void*)pFuses);
                #endif
			    if(pTIM_h->pConsTIM->VersionBind.Version < TIM_3_2_00)
			    {
				        #if ASPNB0
                        #if SLC_NONFI
						SetEccMode(ECC_HAMMING);
                        #endif
						#endif
				        UpdateBBT();
				}

			}
            else if(pImageInfo->FlashEntryAddr >= BlkSize)
            {
			    #if ASPNB0
                #if SLC_NONFI
				if(pImageInfo->ImageID == OBMIDENTIFIER)
				    SetEccMode(ECC_BCH);
				else
				    SetEccMode(ECC_HAMMING);
                #endif
				#endif
	            Retval = WriteFlash( pImageInfo->FlashEntryAddr, loadAddr, pImageInfo->ImageSize, BOOT_FLASH );
                #if VERIFY_WRITE
                Retval = flash_write_verify( pImageInfo->FlashEntryAddr, loadAddr, pImageInfo->ImageSize, BOOT_FLASH, (void*)pFuses);
                #endif
			}
            else
            {
			    if(pTIM_h->pConsTIM->VersionBind.Version < TIM_3_2_00)
			    {
					AddMessage((UINT8_T*) ("***Cannot load image in block zero with Legacy BBM...\0"));
					FatalError(InvalidPlatformConfigError, pTIM_h, pFuses);
				}
                else
                {
					if( (pImageInfo->FlashEntryAddr) < (blkZeroPageIndex * PageSize))
			        {
						AddMessage((UINT8_T*) ("***Cannot over write FBBT or Partition Table...\0"));
						FatalError(InvalidPlatformConfigError, pTIM_h, pFuses);
					}
					if((pImageInfo->FlashEntryAddr + pImageInfo->ImageSize) < BlkSize )
					{
			 			memcpy((UINT_T *)(blkZeroBuf + pImageInfo->FlashEntryAddr), (UINT_T *)loadAddr, pImageInfo->ImageSize);
					    blkZeroDataLength = pImageInfo->FlashEntryAddr + pImageInfo->ImageSize + 1;
			 		}
                    else
                    {
			 			remainingLength = pImageInfo->FlashEntryAddr + pImageInfo->ImageSize - BlkSize;
			 			memcpy((UINT_T *)(blkZeroBuf + pImageInfo->FlashEntryAddr), (UINT_T *)loadAddr, (BlkSize - pImageInfo->FlashEntryAddr));
                        blkZeroDataLength = BlkSize;
		        		Retval = WriteFlash(BlkSize, loadAddr + (BlkSize- pImageInfo->FlashEntryAddr), remainingLength, BOOT_FLASH );
                        #if VERIFY_WRITE
                        Retval = flash_write_verify(BlkSize, loadAddr + (BlkSize - pImageInfo->FlashEntryAddr), remainingLength, BOOT_FLASH, (void*)pFuses);
                        #endif
					}
		            while (size < blkZeroDataLength) {
			            size += PageSize;
						 blkZeroPageIndex++;
		            }
				}
			}
		}
        else
        {
		    #if ASPNB0
            #if SLC_NONFI
			if(pImageInfo->ImageID == OBMIDENTIFIER || pImageInfo->ImageID == TIMIDENTIFIER)
				SetEccMode(ECC_BCH);
			else
			    SetEccMode(ECC_HAMMING);
		    #endif
            #endif
	        Retval = WriteFlash( pImageInfo->FlashEntryAddr, loadAddr, pImageInfo->ImageSize, BOOT_FLASH );
            #if VERIFY_WRITE
            Retval = flash_write_verify(pImageInfo->FlashEntryAddr, loadAddr, pImageInfo->ImageSize, BOOT_FLASH, (void*)pFuses);
            #endif
		}

        if(Retval != NoError )
        {
            FatalError(Retval, pTIM_h, pFuses);
		}
        AddMessageError(PlatformReady);
		#endif		// end of #if copyimagestoflash

		NumImages--;
        pImageInfo = (IMAGE_INFO_3_4_0*)((unsigned char*)pImageInfo - iInfoStructSize);   // index to previous image info record.
    }                        // end WHILE

    if(Retval != NoError) pImageInfo = NULL;  // notify the caller that the download was not successful.
    else pImageInfo = FindImageInTIM(pTIM_h, OBMIDENTIFIER);

    ClearPortInterruptFlag();	// shut down the port used for downloading.

    return pImageInfo;
}

//
// Erase all of the blocks in the flash based on the images
// in the TIM header
//
unsigned int EraseImageAreaFromTIM(pTIM pTIM_h, unsigned int OKtoErase, unsigned int FlashNumber, FUSE_SET* pFuses){
    pIMAGE_INFO_3_4_0   pImageInfo = NULL;
    int                 NumImages;
    int                 iInfoStructSize;    // depends on tim version: 3.2 and beyond vs. 3.1 and before
	unsigned int	Retval = NoError;

	if (pTIM_h->pConsTIM->VersionBind.Version >= TIM_3_4_00)
		iInfoStructSize = sizeof(IMAGE_INFO_3_4_0);    
	else if	(pTIM_h->pConsTIM->VersionBind.Version >= TIM_3_2_00)
		iInfoStructSize = sizeof(IMAGE_INFO_3_2_0);        
	else
	    iInfoStructSize = sizeof(IMAGE_INFO_3_1_0);

    NumImages       = pTIM_h->pConsTIM->NumImages;
    pImageInfo      = (IMAGE_INFO_3_4_0*)((unsigned char*)pTIM_h->pImg + (NumImages-1)*iInfoStructSize);  // index to last image info record.


   while (NumImages && (Retval == NoError))
	{
       if((pTIM_h->pConsTIM->VersionBind.Version) >= TIM_3_2_00)
        {
            AddMessage((UINT8_T*) ("*** Set Partition for Image...\0"));
            SetPartition(ReturnImgPartitionNumber(pTIM_h, pImageInfo), BOOT_FLASH);
        }
	    if( OKtoErase )
	    {
			if( FlashNumber == 0x0a ){
				AddMessage((UINT8_T*) ("*** Erasing SPI device...\0"));
			    Retval = WipeFlash( BOOT_FLASH );
   			    OKtoErase = 0;    // SPI erase clears the whole device. so limit to one erase per download.
			}else{
				AddMessage((UINT8_T*) ("*** Erasing Block for Image...\0"));
			   //	if(pImageInfo->FlashEntryAddr != 0x0)
		        Retval = EraseFlash( pImageInfo->FlashEntryAddr, pImageInfo->ImageSize, BOOT_FLASH );   // does this function understand about bbt?
	        }
	        if(Retval != NoError )
	            FatalError(Retval, pTIM_h, pFuses);
		}
       	NumImages--;
        pImageInfo = (IMAGE_INFO_3_4_0*)((unsigned char*)pImageInfo - iInfoStructSize);
   	}
  return Retval;
}


UINT_T validateLoadAddr(UINT_T LoadAddr, UINT_T Size)
{
    UINT_T Retval = NoError;
    UINT_T Range = 0x0;

    Range = (LoadAddr & 0xFF000000) >> 24;
    switch(Range){
        case 0x5C:
            if ((LoadAddr + Size) > (ISRAM_IMAGE_LOAD_BASE + PLATFORMISRAMLIMIT))
                Retval = InvalidAddressRangeError;
            break;
        case 0x80:  // FIXME: this seems to work only for DDR based at 0x80000000.
            if (LoadAddr < (DDR_PHY_ADDR + OBM_RESTRICTED_DDR_SPACE))
                Retval = InvalidAddressRangeError;
            break;
        default:
            Retval = NoError;
    }

    return Retval;

}

#if FBF
pIMAGE_INFO_3_4_0 DownloadFBFImage( TIM *pTIM_h, FUSE_SET *pFuses )
{
//    FBF Download Algorithm:
//        download MasterBlockHeader
//        compare FBF fuses to platform fuses
//        configure flashes
//
//        download tim and partition table image loop
//            download image
//            checksum image
//            check if image == TIM
//                copy image to TIMLOADADDRESS
//                check version
//                InitializeFM according to tim version
//                continue;
//            if((pTIM_h->pConsTIM->VersionBind.Version) >= TIM_3_2_00)
//                check if image == partitiontable
//                LoadExternalPartitionTable
//                continue;
//        end loop
//        image other images loop:
//                if trusted validate image
//                if((pTIM_h->pConsTIM->VersionBind.Version) >= TIM_3_2_00)
//                   if image partition is non-zero validate partition number with partition table
//                    SetPartition
//                erase flash
//                write flash
//                if verify write then compare image burned to download
//        end loop
//        write tim to flash
//        return

#define DEV_IMAGE_NUMS( dev, img ) (((dev & 0xFFFF)<<16) | (img & 0xFFFF))
#define IMAGE_TYPE(cmds) (cmds>>DLCMD_IMAGE_TYPE_FIELD_BIT) & ((1<<DLCMD_IMAGE_TYPE_FIELD_SIZE_BITS)-1)
#define SPARE_AREA_SIZE(cmds) (cmds>>DLCMD_IMAGE_SPARE_AREA_SZ_BIT) & ((1<<DLCMD_IMAGE_SPARE_AREA_SZ_FIELD_BITS)-1)
#define BLOCK_SIZE 0x20000

    // MasterBlock for FBF file
    MasterBlockHeader* pMasterHeader = (MasterBlockHeader*)MASTER_BLOCK_HEADER_ADDR;
    UINT_T Retval = NoError;
    PDeviceHeader pDevHeader = (PDeviceHeader)FBF_DEVICE_HEADER_TEMP_AREA;
    PImageStruct pImage = 0;
    int devnum = 0;
    int imagenum = 0;
    FUNC_STATUS FS_Retval;
    UINT_T DeviceImageID = DEV_IMAGE_NUMS( 0, 0  ); // device 0, image 0;
    pIMAGE_INFO_3_4_0 pImageInfo = 0;
    UINT_T ImageType = 0;
    UINT_T BlockSize = 0;
    UINT_T PageSize = 0;
    UINT_T SpareAreaSize = 0;
    UINT_T ImageChecksum = 0;
    UINT8_T FlashNumber = 0;
    UINT_T PartBinTag0 = 0;
    UINT_T PartBinTag1 = 0;
    UINT_T NumPartitions = 0;
    int i;
    UINT_T bNeedTim = TRUE;
    UINT_T bNeedPartitionTable = FALSE;
    UINT_T TimImageNum = -1;
    UINT_T *TimIDOffset;
    UINT_T PartitionTableImageNum = -1;
    UINT_T PartitionNumber = 0;
    UINT_T page = -1;
    UINT_T eMMC = 0;
	UINT_T eMMCChunkSize = 128 * 1024; // 16K
    UINT_T LeftToWrite = 0;
    UINT_T FlashAddress = 0;
    UINT_T DownloadAddress = 0;

    P_FlashProperties_T pFlashProps = GetFlashProperties( BOOT_FLASH );

    TimIDOffset = ((UINT_T*)(MBH_IMAGE_DOWNLOAD_AREA + 4));

    if ( stricmpl( pMasterHeader->Unique, "Marvell_FBF", UNIQUE_SIZE ) !=0 )
    {
        Retval = UnknownImageError;
        return 0;
    }

    // At this point we have already downloaded the MasterBlockHeader
    if ( pMasterHeader->nOfDevices < 1 )
    {
        Retval = UnknownImageError;
        return 0;
    }

    // For now, we will only support one flash device.  In the future we can support more.  To add support for
    // multiple flash devices, it is necessary to change PlatformState to manage the state for each device.
    if ( pMasterHeader->nOfDevices > 1 )
    {
        Retval = UnknownImageError;
        return 0;
    }

    // We can now proceed to download the individual images from the FBF file on the host.
    // This requires coordination between the host and the target both using the MasterBlockHeader
    // to refer to the image info for each download.
    for ( devnum = 0; devnum < pMasterHeader->nOfDevices; devnum++ )
    {
        // perhaps there is more than flash device to program, so we will iterate for all devices

        if ( pMasterHeader->Format_Version == 2 )
        {
            // only copy over the V2 device header, V3 portions are set to 0
            memset( pDevHeader, 0, DEVICE_HEADER_SIZE_IN_BYTES ); // init device header
            memcpy( &pDevHeader->nOfImages,
                    (VOID_PTR)((int)pMasterHeader + pMasterHeader->deviceHeaderOffset[ devnum ] ),
                    DEVICE_HEADER_V2_SIZE_IN_BYTES );
        }
        else if ( pMasterHeader->Format_Version == 3 )
        {
            memcpy( pDevHeader, (PDeviceHeader)((int)pMasterHeader + pMasterHeader->deviceHeaderOffset[ devnum ] ), DEVICE_HEADER_SIZE_IN_BYTES );
        }

//      if need tim or need partitiontable
//      loop
//        check if image == TIM
//            copy it to TIMLOADADDRESS
//            check version
//            InitializeFM according to tim version
//            continue;
//        check if image == partitiontable
//            LoadExternalPartitionTable
//            continue;
//      end loop

        // do we need a partition table?
        for ( imagenum = 0; imagenum < pDevHeader->nOfImages; imagenum++ )
        {
            pImage = &pDevHeader->imageStruct[ imagenum ];
            PartitionNumber = ((pImage->commands & 0xFF000000)>>24);
            if ( PartitionNumber > 0 )
            {
                bNeedPartitionTable = TRUE;
                break;
            }
        }

        // get the TIM image and initialize FM
        for ( imagenum = 0; imagenum < pDevHeader->nOfImages; imagenum++ )
        {
            if ( !bNeedTim && !bNeedPartitionTable )
                break;

            pImage = &pDevHeader->imageStruct[ imagenum ];
            DeviceImageID = DEV_IMAGE_NUMS( devnum, imagenum );

            FS_Retval = HandleRequest((UINT_T)MBH_IMAGE_DOWNLOAD_AREA, DeviceImageID); //, Interrupt_Off_Flag);
            if (FS_Retval.ErrorCode != NoError)
            {
                AddMessage((UINT8_T*) ("*** Download of image in FBF file failed...\0"));
                Retval = UnknownImageError;
                FatalError(Retval, NULL, pFuses);
            }

            if ( bNeedTim )
            {
                if ( *TimIDOffset == TIMIDENTIFIER )
                {
                    TimImageNum = imagenum;
                    AddMessage((UINT8_T*) ("*** Download TIM successful...\0"));
                    bNeedTim = FALSE;

                    // downloaded TIM header, so save it
                    memcpy( pTIM_h->pConsTIM, (UINT8_T*)MBH_IMAGE_DOWNLOAD_AREA, pImage->length );
                    // Fill in TIM pointers
                    SetTIMPointers( (UINT8_T *)pTIM_h->pConsTIM, pTIM_h );

#if TRUSTED
                    Retval = bl_security_funcs_api.validate_tim(pTIM_h);
                    if (Retval != NoError) FatalError(InvalidTIMImageError, NULL, pFuses);
                    AddMessage("TIM Validation Successful!\0");
#endif

                    FlashNumber = (UINT8_T)(pTIM_h->pConsTIM->FlashInfo.BootFlashSign) & 0xFF;
                    if((pFuses->bits.PlatformState != 0x0) && (FlashNumber != pFuses->bits.PlatformState))
                    {
                        AddMessage((UINT8_T*) ("*** ERROR: Platform Fuses do NOT match flash settings in TIM!\0"));
                        AddMessage((UINT8_T*) ("*** ERROR: Initialize halted here!\0"));
                        Retval = InvalidPlatformConfigError;
                        return 0;
                    }

                    // mmc devices automatically erase on writes, so disable the erase enable flag
                    if( FlashNumber == 0x08 ) eMMC = 1;
                    else if( FlashNumber == 0x09 ) eMMC = 1;
                    else if( FlashNumber == 0x0b ) eMMC = 1;

                    // Configure Flashes Based on TIM
                    AddMessage((UINT8_T*) ("*** Preparing Flash for first use (may take several minutes)...\0"));
                    AddMessageError(PlatformBusy);
                    AddMessage((UINT8_T*) ("*** Preparing Flash...\0"));
                    Retval = Configure_Flashes( FlashNumber, BOOT_FLASH );
                    if (Retval != NoError) FatalError(Retval, NULL, pFuses);
 #if FORCE_BAD_BLOCKS
    //force some bad blocks for testing
     ForceBadBlocks();
 #endif
                    AddMessageError(PlatformReady);

                    //turn on the flash management
                    AddMessage((UINT8_T*) ("*** Initializing FM...\0"));
                    AddMessageError(PlatformBusy);
                    if( pTIM_h->pConsTIM->VersionBind.Version < TIM_3_2_00 )
                        InitializeFM(LEGACY_METHOD, BOOT_FLASH);
                    else
                    {
                        InitializeFM(ALL_METHODS, BOOT_FLASH);
                        bNeedPartitionTable = TRUE;
                    }
                    AddMessageError(PlatformReady);

                    BlockSize = GetBlockSize( BOOT_FLASH );
                    PageSize = GetPageSize( BOOT_FLASH );
                }
            }
            else if( bNeedPartitionTable )
            {
                // if image is a partition table, store number of partitions
                // if image is not a partition table,
                //      the check that the image partition number is consistent with the partition table
                memcpy( &PartBinTag0, (UINT8_T*)MBH_IMAGE_DOWNLOAD_AREA, 4 );
                memcpy( &PartBinTag1, (UINT8_T*)MBH_IMAGE_DOWNLOAD_AREA+4, 4 );
                if ( PartBinTag0 == MARVELL_PARTITION_TABLE_ID0 && PartBinTag1 == MARVELL_PARTITION_TABLE_ID1 )
                {
                    PartitionTableImageNum = imagenum;
                    AddMessage((UINT8_T*) ("*** Download Partition Table successful...\0"));
                    bNeedPartitionTable = FALSE;


					// for MMC, make sure the requested partitions exist.
					if( (FlashNumber == 0x08 ) || (FlashNumber == 0x09) || (FlashNumber == 0x0b) )
					{
						AddMessageError(PlatformBusy);
						AddMessage((UINT8_T*) ("*** Validating partitions\0"));
						Retval = ValidatePartitions( FlashNumber, (void*)MBH_IMAGE_DOWNLOAD_AREA );	// compare existing partitions with partition table.
						if( Retval != NoError )									// existing partitions don't match the partition table?
						{
							AddMessage((UINT8_T*) ("*** Creating partitions (partitions did not match table)\0"));
							Retval = CreatePartitions( FlashNumber, (void*)MBH_IMAGE_DOWNLOAD_AREA );	// issue commands to create partitions.

							if( Retval == NoError )
							{
								// Successfully created partitions.
								// The create partition process requires a power cycle.
								// Display a message and halt.
								AddMessage((UINT8_T*) ("*** Partitions created.\0"));
								AddMessage((UINT8_T*) ("*** Please power cycle platform to resume DKB operation.\0"));

							}
							else
							{
								// Failed to create partitions.
								// Display a message and halt.
								AddMessage((UINT8_T*) ("*** Fail to create partition.\0"));
								AddMessage((UINT8_T*) ("*** Revalidate images after DKB operation completes.\0"));
							}
							// disconnect from the host session. all the queued up log messages will not be displayed until disconnected.
							AddMessageError(PlatformReady);
							AddMessageError(PlatformDisconnect);
							HandleDisconnect();
							ShutdownPort(CI2_USB_D);	// try to break the hosts connection.
							FatalError(Retval, pTIM_h, pFuses);
						}
						else
						{
							AddMessage((UINT8_T*) ("*** Existing partitions will be used.\0"));
							AddMessageError(PlatformReady);
						}
					}
					// partition validation complete.




		    AddMessageError(PlatformBusy);
                    ClearFM(BOOT_FLASH);
                    //figure out the first page AFTER the TIM
                    PageSize = GetPageSize(BOOT_FLASH);
                    pImageInfo = FindImageInTIM(pTIM_h, TIMIDENTIFIER);
                    page = pImageInfo->ImageSize / PageSize;
                    if((page * PageSize) != pImageInfo->ImageSize) page++;
                    LoadExternalPartitionTable((UINT*)MBH_IMAGE_DOWNLOAD_AREA, page * PageSize);

                    NumPartitions = ((P_PartitionTable_T)(MBH_IMAGE_DOWNLOAD_AREA))->NumPartitions;
		    AddMessageError(PlatformReady);
                }
            }
        }

        if ( bNeedTim )
        {
            AddMessage((UINT8_T*) ("*** ERROR: Did not find TIM in FBF!\0"));
            Retval = InvalidPlatformConfigError;
            return 0;
        }


        //  erase flash loop:
        //     if((pTIM_h->pConsTIM->VersionBind.Version) >= TIM_3_2_00)
        //          if image partition is non-zero validate partition number with partition table
        //               SetPartition
        //     erase flash
        //  end loop

        // erase area for the TIM
        pImageInfo = FindImageInTIM(pTIM_h, TIMIDENTIFIER);
        if ( pImageInfo == 0 )
        {
            AddMessage((UINT8_T*) ("*** Failure: No TIM Image found...\0"));
            Retval = ImageNotFound;
            return 0;
        }

        if((pTIM_h->pConsTIM->VersionBind.Version) >= TIM_3_2_00)
        {
            AddMessageError(PlatformBusy);
            AddMessage((UINT8_T*) ("*** Set Partition for Image...\0"));
            SetPartition(ReturnImgPartitionNumber(pTIM_h, pImageInfo), BOOT_FLASH);
            AddMessageError(PlatformReady);
        }

        if ( eMMC == 0 )
        {
#if COPYIMAGESTOFLASH
            // Erase block 0
            AddMessageError(PlatformBusy);
            AddMessage((UINT8_T*) ("*** Erasing Flash Block 0...\0"));
            Retval = EraseFlash(0x0, pFlashProps->BlockSize, BOOT_FLASH);
            if (Retval != NoError) FatalError(Retval, NULL, pFuses);
            AddMessageError(PlatformReady);
 #endif

            for ( imagenum = 0; imagenum < pDevHeader->nOfImages; imagenum++ )
            {
                pImage = &pDevHeader->imageStruct[ imagenum ];

                // Erase flash for image
                if ( pImage->commands & DLCMD_DO_ERASE_BLOCKS )
                {
                    PartitionNumber = ((pImage->commands & 0xFF000000)>>24);
                    if ( (PartitionNumber != 0) && (PartitionNumber > NumPartitions-1 ))
                    {
                        // (pImage->commands & 0xFF000000)>>24 gets the partition number out of the commands field
                        // partition number in pImage->commands is <= NumPartitions
                        AddMessage((UINT8_T*) ("*** Partition Number for downloaded image not in partition table...\0"));
                        Retval = InvalidAddressRangeError;
                        FatalError(Retval, NULL, pFuses);
                    }

                   if((pTIM_h->pConsTIM->VersionBind.Version) >= TIM_3_2_00)
                    {
                        AddMessageError(PlatformBusy);
                        AddMessage((UINT8_T*) ("*** Set Partition for Image...\0"));
                        SetPartition(PartitionNumber, BOOT_FLASH);
                        AddMessageError(PlatformReady);
                    }

                    // set up use of spare area bytes according to pImage->commands field
                    SpareAreaSize = SPARE_AREA_SIZE(pImage->commands);
                    SetUseSpareArea( (SpareAreaSize > 0), BOOT_FLASH );
                    SetSpareAreaSize( SpareAreaSize, BOOT_FLASH );

#if COPYIMAGESTOFLASH
                    AddMessageError(PlatformBusy);
                    AddMessage((UINT8_T*) ("*** Erase Flash before write...\0"));
                    Retval = EraseFlash(pImage->Flash_Start_Address, pImage->length, BOOT_FLASH);
                    if (Retval != NoError) FatalError(Retval, NULL, pFuses);
                    AddMessageError(PlatformReady);
 #endif
                }
            }
        }

        //  image other images loop:
        //     if trusted
        //          validate image
        //     if((pTIM_h->pConsTIM->VersionBind.Version) >= TIM_3_2_00)
        //          if image partition is non-zero validate partition number with partition table
        //               SetPartition
        //     write flash
        //     if verify write
        //          compare image burned to download
        //  end loop
        for ( imagenum = 0; imagenum < pDevHeader->nOfImages; imagenum++ )
        {
            // skip TIM
            if ( imagenum == TimImageNum )
                continue;

            if ( imagenum == PartitionTableImageNum )
                AddMessage((UINT8_T*) ("*** Downloading Partition image...\0"));

            pImage = &pDevHeader->imageStruct[ imagenum ];

            DeviceImageID = DEV_IMAGE_NUMS( devnum, imagenum );
            ImageType = IMAGE_TYPE(pImage->commands);

            // Download image
            if ( pImage->commands & DLCMD_WRITE_IMAGE )
            {
                // when downloading images from an FBF file, the DeviceImageID refers to the arrays in the
                // MasterBlockHeader.  The host has a copy.  DeviceImageID indicates the array indicies for
                // the device header array and the image struct array in the MasterBlockHeader.
                // The macro DEVIMAGE_ID takes care of putting the 16-bit fields in the right bit positions.
                FS_Retval = HandleRequest((UINT_T)MBH_IMAGE_DOWNLOAD_AREA, DeviceImageID); //, Interrupt_Off_Flag);
                if (FS_Retval.ErrorCode != NoError)
                {
                    AddMessage((UINT8_T*) ("*** Download of image in FBF file failed...\0"));
                    Retval = UnknownImageError;
                    FatalError(Retval, NULL, pFuses);
                }

#if TRUSTED
                AddMessage((UINT8_T*) ("*** Validating Image...\0"));
                pImageInfo = FindImageInTIM(pTIM_h, *((UINT_T*)MBH_IMAGE_DOWNLOAD_AREA));
                if (pImageInfo->ImageID == TIMIDENTIFIER) {
                     Retval = bl_security_funcs_api.validate_tim(pTIM_h);
                }
                else {
                     Retval = bl_security_funcs_api.validate_image(NULL, pImageInfo->ImageID, pTIM_h, NULL);
                }
                if (Retval != NoError) FatalError(Retval, NULL, pFuses);
                AddMessage((UINT8_T*) ("*** Image Validation Successful\0"));
#endif

                PartitionNumber = ((pImage->commands & 0xFF000000)>>24);
                if ( (PartitionNumber != 0) && (PartitionNumber > NumPartitions-1 ))
                {
                    // (pImage->commands & 0xFF000000)>>24 gets the partition number out of the commands field
                    // partition number in pImage->commands is <= NumPartitions
                    AddMessage((UINT8_T*) ("*** Partition Number for downloaded image not in partition table...\0"));
                    Retval = InvalidAddressRangeError;
                    FatalError(Retval, NULL, pFuses);
                }

                if((pTIM_h->pConsTIM->VersionBind.Version) >= TIM_3_2_00)
                {
                    AddMessageError(PlatformBusy);
                    AddMessage((UINT8_T*) ("*** Set Partition for Image...\0"));
                    SetPartition(PartitionNumber, BOOT_FLASH);
                    AddMessageError(PlatformReady);
                }

                // set up use of spare area bytes according to pImage->commands field
                SpareAreaSize = SPARE_AREA_SIZE(pImage->commands);
                if ( SpareAreaSize > 0 )
                    AddMessage((UINT8_T*) ("*** Using Spare Area for image...\0"));

                SetUseSpareArea( (SpareAreaSize > 0), BOOT_FLASH );
                SetSpareAreaSize( SpareAreaSize, BOOT_FLASH );

                AddMessage((UINT8_T*) ("*** Verifying Download Image Checksum...\0"));
                AddMessageError(PlatformBusy);
                // verify downloaded checksum matches FBF image header
                ImageChecksum = CalcImageChecksum( (UINT_T*)MBH_IMAGE_DOWNLOAD_AREA, pImage->length, ImageType, PageSize, SpareAreaSize);

                if ( ImageChecksum != pImage->ChecksumFormatVersion2 )
                {
                   AddMessage((UINT8_T*) ("*** Download of image in FBF file failed, checksum error...\0"));
                   Retval = CRCFailedError;
                   FatalError(Retval, NULL, pFuses);
                }

                // write the image to flash and optionally verify flash written matches download
#if COPYIMAGESTOFLASH
                AddMessage((UINT8_T*) ("*** Write Image to Flash...\0"));

                if ( eMMC == 0 ) {
                    #if ASPNB0
                    #if SLC_NONFI
                    pImageInfo = FindImageInTIM(pTIM_h, OBMIDENTIFIER);
                    if ( pImageInfo == 0 )
                    {
                        AddMessage((UINT8_T*) ("*** Failure: No OBM Image found...\0"));
                        Retval = ImageNotFound;
                        return 0;
                    }
                    if (pImageInfo->FlashEntryAddr == pImage->Flash_Start_Address)
                        SetEccMode(ECC_BCH);
                    else
                        SetEccMode(ECC_HAMMING);
                    #endif
                    #endif
                    Retval = CopyImageToFlash((UINT_T*)MBH_IMAGE_DOWNLOAD_AREA, pImage->Flash_Start_Address, pImage->length, ImageType, PageSize, SpareAreaSize, pFuses);
                }
                else
                {
                    // have eMMC so just do a straight write
                    // break up image write into no more that an eMMMCChunkSize at a time
                    LeftToWrite = pImage->length;
                    FlashAddress = pImage->Flash_Start_Address;
                    DownloadAddress = (UINT_T)MBH_IMAGE_DOWNLOAD_AREA;
                    while ( LeftToWrite > 0 )
                    {
                        if ( LeftToWrite >= eMMCChunkSize )
                        {
                            // NORMAL WRITE - write data page with no Spare Area, ECC ON
                            Retval = WriteFlash(FlashAddress, DownloadAddress, eMMCChunkSize, BOOT_FLASH);
                            if (Retval != NoError)
                            {
                                AddMessage((UINT8_T*) ("*** Write Image to Flash failed...\0"));
                                break;
                            }

                            LeftToWrite -= eMMCChunkSize;
                            FlashAddress += eMMCChunkSize;
                            DownloadAddress += eMMCChunkSize;
                        }
                        else
                        {
                            // NORMAL WRITE - write data page with no Spare Area, ECC ON
                            Retval = WriteFlash(FlashAddress, DownloadAddress, LeftToWrite, BOOT_FLASH);
                            if (Retval != NoError)
                            {
                                AddMessage((UINT8_T*) ("*** Write Image to Flash failed...\0"));
                                break;
                            }

                            break;
                        }
                    }
                }

                if (Retval != NoError) FatalError(Retval, NULL, pFuses);
                AddMessageError(PlatformReady);

#ifdef VERIFY_WRITE  // for debug only as this takes a while and should not be necessary in production code
                if ( (pImage->commands & DLCMD_DO_VERIFY_WRITE) && (pImage->commands & DLCMD_WRITE_IMAGE) )
                {
                    // verify the Image written to flash
        AddMessageError(PlatformBusy);
        AddMessage((UINT8_T*) ("*** Verify Image Written to Flash...\0"));
                    //Retval = ReadFlash(FlashAddress, 0x00090000, pImage->length, BOOT_FLASH);
                    if (Retval != NoError) FatalError(Retval, NULL, pFuses);

                    AddMessageError(PlatformReady);
                }
#endif
//In case COPYIMAGESTOFLASH is not defined, then we will have to send a 
//PlatformReady to complement Busy sent before ValidateImage
#else
        AddMessageError(PlatformReady);
#endif
            }
        }
    }


    // turn off use of spare area since we are done with OS images
    // any write to flash from here on do not use the spare area
    SetUseSpareArea( 0, BOOT_FLASH );
    SetSpareAreaSize( 0, BOOT_FLASH );

    //Lastly, write the TIM to flash
#if COPYIMAGESTOFLASH
    pImageInfo = FindImageInTIM(pTIM_h, TIMIDENTIFIER);
    if ( pImageInfo == 0 )
    {
        AddMessage((UINT8_T*) ("*** Failure: No TIM Image found...\0"));
        Retval = ImageNotFound;
        return 0;
    }

    if((pTIM_h->pConsTIM->VersionBind.Version) >= TIM_3_2_00)
    {
        AddMessageError(PlatformBusy);
        AddMessage((UINT8_T*) ("*** Set Partition for Image...\0"));
        SetPartition(ReturnImgPartitionNumber(pTIM_h, pImageInfo), BOOT_FLASH);
        AddMessageError(PlatformReady);
    }

    // Write TIM Image to Flash block 0 (at least pImageInfo->FlashEntryAddr ought to be 0)
    AddMessage((UINT8_T*) ("*** Copying TIM to Flash...\0"));
    AddMessageError(PlatformBusy);
	#if ASPNB0
    #if SLC_NONFI
	SetEccMode(ECC_BCH);
    #endif
	#endif
    Retval = WriteFlash(pImageInfo->FlashEntryAddr, (UINT_T)pTIM_h->pConsTIM, pImageInfo->ImageSize, BOOT_FLASH);
    if (Retval != NoError) FatalError(Retval, NULL, pFuses);
    AddMessageError(PlatformReady);

    AddMessage((UINT8_T*) ("*** All images written to flash successfully...\0"));
#endif

    AddMessage((UINT8_T*) ("*** Load OBM to download area...\0"));
    pImageInfo = FindImageInTIM(pTIM_h, OBMIDENTIFIER);
    if ( pImageInfo == 0 )
    {
        AddMessage((UINT8_T*) ("*** Failure: No OBM Image found...\0"));
        Retval = ImageNotFound;
        return 0;
    }
    AddMessageError(PlatformBusy);
    Retval = ReadFlash(pImageInfo->FlashEntryAddr, IMAGE_DOWNLOAD_AREA, pImageInfo->ImageSize, BOOT_FLASH);
    AddMessageError(PlatformReady);
    if (Retval != NoError) FatalError(Retval, NULL, pFuses);

#if TRUSTED
    Retval = bl_security_funcs_api.validate_image((UINT8_T*)IMAGE_DOWNLOAD_AREA, pImageInfo->ImageID, pTIM_h, NULL);
    if (Retval != NoError) FatalError(Retval, NULL, pFuses);
#endif
    AddMessage("Validation Successful!\0");

    // notify WTPTP that no more image requests will be coming
    FS_Retval = HandleRequest(0, 0xFFFFFFFF);

    return pImageInfo;
}

UINT CalcImageChecksum( UINT_T* DownloadArea, UINT_T ImageLength, UINT_T ImageType, UINT_T PageSize, UINT_T SpareAreaSize )
{
    UINT ImageChecksum = 0;
    UINT32* ptr32 = DownloadArea;
    UINT32* pEnd = ptr32 + (ImageLength / sizeof(UINT32));
    UINT BytesSummed = 0;
    UINT bUseSpareArea = 0;

    // position to skip over spare area bytes when calculating checksum
    if ( ImageType == DLCMD_WINDOWS_MOBILE_IMAGE_TYPE || ImageType == DLCMD_CUSTOMIZED_IMAGE_TYPE)
        if ( GetUseSpareArea( BOOT_FLASH ) )
            bUseSpareArea = 1;

    while ( ptr32 < pEnd )
    {
        // skip over spare area bytes
        if ( bUseSpareArea && (BytesSummed > 0) && (0 == (BytesSummed % PageSize)) )
        {
            ptr32 += SpareAreaSize/sizeof(UINT32);
            BytesSummed = 0;
            continue;
        }

        // checksum format version 2 algorithm as defined by flasher
        ImageChecksum ^= (*ptr32);
        ptr32++;
        BytesSummed += sizeof(UINT32);
    }
    return ImageChecksum;
}


UINT_T CopyImageToFlash( UINT_T* DownloadArea, UINT_T FlashStartAddr, UINT_T Length, UINT_T ImageType, UINT_T PageSize, UINT_T SpareAreaSize, FUSE_SET* pFuses)
{
    UINT_T Retval = NoError;
    UINT32* ptr32 = DownloadArea;
    UINT32* pEnd = ptr32 + (Length / sizeof(UINT32));
    UINT_T FlashWriteAddr = FlashStartAddr;
    UINT_T bSkipSpareArea = FALSE;
    UINT_T bUseHwEcc = FALSE;

    // ECC is calculated for page content, not spare area.
    //Main area + spare area have data  Write the Main area and spare area with ECC written.
    //Main area has data, spare area has no data   Write the Main area with ECC written and leave spare area empty
    //Main area has no data, spare area has data   Write spare area, left Main area empty and ECC empty.
    //Main area has no data, spare area has no data Do not write any thing, ECC is empty as well.

    // Copy the Image to flash
    AddMessage((UINT8_T*) ("*** Copy Image to Flash...\0"));

    while ( ptr32 < pEnd )
    {
        if ( ImageType == DLCMD_WINDOWS_MOBILE_IMAGE_TYPE || ImageType == DLCMD_CUSTOMIZED_IMAGE_TYPE)
        {
            if ( GetUseSpareArea( BOOT_FLASH ) )
            {
                // SPARE AREA SPECIAL HANDLING AND SKIPPED PAGES

                // if normally writing SA, check if SA is all 0xFF's and turn off SA for this page if so
                if ( 0 == memcmpFF( ptr32+(PageSize/sizeof(UINT32)), SpareAreaSize) )
                {
                    // skip writing any page that contains all 0xFF's
                    // SA does not contain any data so skip writing it
                    SetUseSpareArea( FALSE, BOOT_FLASH );
                    bSkipSpareArea = TRUE;
                }
                if ( 0 != memcmpFF( ptr32, PageSize ) )
                {
                    // write a page that has data, with or without Spare Area, with ECC ON
                    Retval = WriteFlash(FlashWriteAddr, (UINT_T)ptr32, PageSize, BOOT_FLASH);
                    if (Retval != NoError) FatalError(Retval, NULL, pFuses);
                }
                else if ( bSkipSpareArea == FALSE )  // have all 0xFFs data
                {
                    // Spare Area is not all 0xFFs so ECC must be turned OFF to
                    // overwrite data without changing it, and no ECC, so SA can be written

                    // get hw ECC state
                    bUseHwEcc = GetUseHwEcc( BOOT_FLASH );

                    SetUseHwEcc( FALSE, BOOT_FLASH );

                    // write the 0xFF's to data page and SA with ECC turned OFF
                    Retval = WriteFlash(FlashWriteAddr, (UINT_T)ptr32, PageSize, BOOT_FLASH);

                    // restore ECC state
                    SetUseHwEcc( bUseHwEcc, BOOT_FLASH );

                    if (Retval != NoError) FatalError(Retval, NULL, pFuses);
                }
                else if ( bSkipSpareArea == TRUE )
                {
                    // skipping entire write
                    // FOR DEBUG ONLY
                }

                if ( bSkipSpareArea )
                {
                    // make sure to reset flag for next page
                    SetUseSpareArea( TRUE, BOOT_FLASH );
                    bSkipSpareArea = FALSE;
                }
            }
            else
            {
//                // NORMAL WRITE - write data page with no Spare Area, ECC ON
//                // skip writing any page that contains all 0xFF's,
//                if ( 0 != memcmpFF( ptr32, PageSize ) )
//                {
                    Retval = WriteFlash(FlashWriteAddr, (UINT_T)ptr32, PageSize, BOOT_FLASH);
                    if (Retval != NoError) FatalError(Retval, NULL, pFuses);
//                }
            }
        }
        else
        {
            Retval = WriteFlash(FlashWriteAddr, (UINT_T)ptr32, PageSize, BOOT_FLASH);
            if (Retval != NoError) FatalError(Retval, NULL, pFuses);
        }

        ptr32 += (PageSize+SpareAreaSize)/sizeof(UINT32);
        FlashWriteAddr += PageSize;
    }

    return Retval;
}
#endif

//
//Support functions
//
/* ProbeFlash routine in the OBM
*
*   This would be done for Non-Trusted only and when being used in conjunction with older variants of the TBR where probing
*   took place but the information was not passed to the OBM.
*/

UINT_T ProbeFlash(pFUSE_SET pFuses, pTIM pTIM_h,  UINT8_T *TIM_Area)
{
    UINT_T Retval = NoError;
    UINT_T count = 0;
    FUSE_SET  TempFuses;

    SaveDefaultConfig();
    do
    {
        switch (count)
        {
            // cycle through x8 NAND, CS2, x16 NAND, OneNand, M-Systems, CS0.
              case 0:TempFuses.bits.PlatformState = 0x4;  // x16 NAND
                     break;
              case 1:TempFuses.bits.PlatformState = 0x5;  // x16 XIP on CS2
                     break;
              case 2:TempFuses.bits.PlatformState = 0x6;  // x8 NAND
                     break;
              case 3:TempFuses.bits.PlatformState = 0x2;  // OneNAND
                     break;
              case 4:TempFuses.bits.PlatformState = 0x1;  // MSystems
                     break;
              case 5:TempFuses.bits.PlatformState = 0x7;  // x16 Tyax on CS2
                     break;
              case 6:TempFuses.bits.PlatformState = 0x3;  // x16 Sibley on CS0
                     break;
              case 7:TempFuses.bits.PlatformState = 0x8;  // SDMMC
                     break;
              case 8:TempFuses.bits.PlatformState = 0x9;  // SDMMC
                     break;
              case 9:TempFuses.bits.PlatformState = 0xA;  // SPIFlash
                     break;
              case 10:TempFuses.bits.PlatformState = 0xB;  // SDMMC
                     break;
        }

        // configure the boot flash
        Retval = Configure_Flashes( TempFuses.bits.PlatformState, BOOT_FLASH);
        if(Retval == NoError)
        {
            //first determine if we have a TIM or Image module present
            Retval = LoadTim(TIM_Area, pTIM_h,FALSE);
        }
        if(Retval != NoError)
        {
            count++;
            RestoreDefaultConfig();
        }
        else
        {
            if(((pTIM_h->pConsTIM->FlashInfo.BootFlashSign & 0xFF) == 0x7) && (TempFuses.bits.PlatformState == 0x5))
                TempFuses.bits.PlatformState = 0x7;
            if((pTIM_h->pConsTIM->FlashInfo.BootFlashSign & 0xFF) != TempFuses.bits.PlatformState)
            {
                count++;
                continue;
            }
            pFuses->bits.BootPlatformState = TempFuses.bits.PlatformState;
            pFuses->bits.PlatformState = TempFuses.bits.PlatformState;
            pFuses->value = TempFuses.bits.PlatformState << 1;
            break;
        }
    }
    while(count < 11);

    if (count < 11)
        return NoError;
    else
        return ReadError;
 }

void CheckHibernate(pFUSE_SET pFuses, pTIM pTIM_h,  UINT8_T *TIM_Area)
{
    //volatile INT                *pTemp = (volatile int *) PMUA_MC_SLP_REQ_AP;
    UINT_T                      Retval = NoError;
    unsigned long               fDoMemoryCheck = FALSE;
    UINT_T baseaddr = 0xffffffff;
    int				r;
    pWTP_RESERVED_AREA_HEADER   pWRAH = NULL;
    pOPT_TIM_RESUME_SET         pOPT_TIMResumeSet = NULL;		// points to resume record in TIM
    pOPT_RESUME_DDR_INFO        pOPT_ResumeDDRInfo = NULL;		// points to resume record in DDR
    UINT_T (*HibernateResumeFx) ();                 // pointer to the hibernate resume function - address is in the DDR hibernate resume struct

	#if VERBOSE_DEBUG
    AddMessage((UINT8_T*) ("*** Checking for Resume from Hibernate Request...\0"));
	#endif

/*
    2. find the address of the (ddr-based) resume package from the TIM (if none, normal boot)
    4. examine the (ddr-based) resume flag (if not correct, normal boot)
    5. ok to resume
*/
    // 2. Check TIM for resume package
    pWRAH = FindPackageInReserved (&Retval, pTIM_h, RESUMEBLID);
    if (Retval == NoError)
    {
		#if VERBOSE_DEBUG
		AddMessage((UINT8_T*) ("***Resume package found in TIM...\0"));
		#endif
        pOPT_TIMResumeSet = (pOPT_TIM_RESUME_SET) pWRAH;
        if (pOPT_TIMResumeSet != NULL)
        {
	   		#if VERBOSE_DEBUG
	   		AddMessage((UINT8_T*) ("*** Check Resume Struct in DDR ...\0"));
			#endif
            // step 4.
            pOPT_ResumeDDRInfo = (pOPT_RESUME_DDR_INFO)pOPT_TIMResumeSet->TimResumeDDRInfo.DDRResumeRecordAddr;
            if( pOPT_ResumeDDRInfo->ResumeFlag == RESUME_FLAG_MASK )
            {
            	#if VERBOSE_DEBUG
				AddMessage((UINT8_T*) ("*** Jumping to OS ...\0"));
				#endif
                // step 5: ok to resume
                pOPT_ResumeDDRInfo->ResumeFlag = ~pOPT_ResumeDDRInfo->ResumeFlag;

				// Jump to the address in the resume package in DDR.
				// OS saves the jump address in DDR before hibernate.
                HibernateResumeFx = (UINT_T(*)())pOPT_ResumeDDRInfo->ResumeAddr;
                HibernateResumeFx();
            }
            #if ASPN
            else
            {
                // This is probably a cold boot. We need to issue a DDR init cmd to controller.
                *MCU_REG_USER_INITIATED_COMMAND = 0x1;
                // DDR should be initialized hopefully
                while( ((*MCU_REG_DRAM_STATUS) & MCU_DRAM_STATUS_INIT_DONE ) == 0 )
                {
                    // wait for hardware to assert the INIT_DONE bit
                }

                baseaddr = ( *MCU_REG_MMU_MMAP0	) & 0xff800000;

                // finally, do some dummy reads in (and discard the values)
                r = *(unsigned volatile long*)baseaddr;
                r = *(unsigned volatile long*)baseaddr;
                r = *(unsigned volatile long*)baseaddr;
                r = *(unsigned volatile long*)baseaddr;
                r = *(unsigned volatile long*)baseaddr;
            }
            #endif
			#if VERBOSE_DEBUG
			AddMessage((UINT8_T*) ("*** Failed check on ResumeFlag in DDR  ...\0"));
			#endif
        }
    }
}

/* BootFromFlash
 * Here we attempt a boot from flash
 * compile option to load all TIM inages or just the OS Loader
 * default as stated in the WTP reference manual is the OSLoader
 * to adhere to the chain of trust boot method
 */
pIMAGE_INFO_3_4_0 BootFromFlash(pFUSE_SET pFuses, pTIM pTIM_h)
{
    UINT_T flash_number, retval;
    pIMAGE_INFO_3_4_0 pBootImageInfo = NULL;
	pOPT_SOCPROFILEID	pSocProfileID;

#if LOAD_ALL_IMAGES
        pBootImageInfo = LoadAllImages( pFuses, pTIM_h );
#else
        pBootImageInfo = LoadOSLoader( pFuses, pTIM_h );
#endif
        #if CONFIG_EMMC_ALTERNATE_BOOT
        flash_number = ((pTIM_h->pConsTIM->FlashInfo.BootFlashSign) & 0x0F);
        if (flash_number == SDMMC_FLASH_OPT3_P || flash_number == SDMMC_FLASH_OPT4_P) {
            retval = MM4_MMCReadEXTCSD((UINT_T*)ext_csd);
            if (retval != NoError) {
			    AddMessage((UINT8_T*) ("*** Config eMMC cannot read Ext CSD...\0"));
                FatalError(retval, pTIM_h, &fuses);
            }
            if (ext_csd[PARTITION_CONFIG_MMC_EXT_CSD_OFFSET] != BOOT_FROM_PARTITION_1_WITH_BOOTACK ||
                ext_csd[BOOT_BUS_WIDTH_MMC_EXT_CSD_OFFSET] != BUS_WIDTH_8_WITH_SDR) {
                retval = MM4SwitchPartitionForAlternateBootMode(); 
                if (retval != NoError) {
			        AddMessage((UINT8_T*) ("*** Config eMMC error...\0"));
                    FatalError(retval, pTIM_h, &fuses);
                }
            }
        }
        #endif 

        #if TRUSTED

		// Check to see if there is an SOCPROFILEID packet in TIM
		pSocProfileID = (pOPT_SOCPROFILEID) FindPackageInReserved (&retval, pTIM_h, SOCPROFILEID);
		if(pSocProfileID != NULL)
			bl_security_funcs_api.read_profile_fuse(pTIM_h, pSocProfileID->StoreAddress);

        /* if platform is not binded and there exist auto-bind package
         * go bind platform
         */
        if (pFuses->bits.PlatformState == 0x0) {
            retval = CheckAutoBindPacket(pTIM_h);
            if (retval == NoError) {
                /* GEU: replace (void*)AddMessage with ``(void*)0xFFFFFFFF" to perform ATE config simulation */
                /* WTM: 2nd parameter has no effect! */
                retval = bl_security_funcs_api.perform_platbind(pTIM_h, (void*)AddMessage);
                if (retval != NoError) {
                    FatalError(retval, pTIM_h, pFuses);
                }
				retval = bl_security_funcs_api.verify_platform(pTIM_h);
                if (retval != NoError) {
                    FatalError(retval, pTIM_h, pFuses);
                }
                /* GEU: replace (void*)AddMessage with ``(void*)0xFFFFFFFF" to perform ATE config simulation */
                /* WTM: 2nd parameter has no effect! */
                retval = bl_security_funcs_api.perform_autoconf(pTIM_h, (void*)AddMessage);
                if( retval != NoError && retval != GEU_LastLogicalBitBurned) {
                    FatalError(retval, pTIM_h, pFuses);
                }
            }
        }
        #else
            #if TVTD_NTCFG
            /* allow TVTD to perform configurations, such as
             *  - platform boot state,
             *  - usb id, etc.
             * following the fuse config spec "tvtd_fuse_conf_spec".
             */
            if (pFuses->bits.PlatformState == 0x0) {
                retval = tvtd_autocfg_platform(pTIM_h, NULL);
                if (retval != NoError && retval != GEU_LastLogicalBitBurned) {
                    FatalError(retval, pTIM_h, pFuses);
                }
            }
            #endif
        #endif
    return pBootImageInfo;
}
    
// Set if OBM or DKB after tim pointers have been setup
void SetRunningAsIdentifier(pTIM pTIM_h)
{
    pIMAGE_INFO_3_4_0 pImage;
    
    pImage = FindImageInTIM(pTIM_h, DKBIDENTIFIER);
    if(pImage)
        sRunningAsIdentifier = DKBIDENTIFIER;
    else
        sRunningAsIdentifier = OBMIDENTIFIER;
        
}
// Find out if OBM or DKB
UINT_T WhoAmI()
{
    return sRunningAsIdentifier;
}
