;currentbaseline
;/******************************************************************************
;**	(C) Copyright 2007 Marvell International Ltd. ?
;**	All Rights Reserved
;******************************************************************************/

;******************************************************************************
;**
;**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
;**
;**  This software as well as the software described in it is furnished under
;**  license and may only be used or copied in accordance with the terms of the
;**  license. The information in this file is furnished for informational use
;**  only, is subject to change without notice, and should not be construed as
;**  a commitment by Intel Corporation. Intel Corporation assumes no
;**  responsibility or liability for any errors or inaccuracies that may appear
;**  in this document or any software that may be provided in association with
;**  this document.
;**  Except as permitted by such license, no part of this document may be
;**  reproduced, stored in a retrieval system, or transmitted in any form or by
;**  any means without the express written consent of Intel Corporation.
;**
;**  FILENAME:	OBM_StartUp.s
;**
;**  PURPOSE: 	StartUp code for Monahans variant OBM code
;**
;******************************************************************************

	GET bl_StartUp_ttc._S

   INCLUDE Platform_defs.INC
;   INCLUDE xlli_platform_defs.INC

   AREA    Init, CODE, READONLY

   ; External Functions
   IF :DEF: USE_BOOTSTRAP
   EXTERN boot_strap_main
   ELSE
   EXTERN BootLoaderMain
   ENDIF

   EXTERN UndefinedHandler
   EXTERN SwiHandler
   EXTERN PrefetchHandler
   EXTERN AbortHandler
   EXTERN IrqHandler
   EXTERN FiqHandler


   EXPORT TransferControl
   EXPORT EnableIrqInterrupts
   EXPORT DisableIrqInterrupts
   EXPORT __main

   EXPORT boot_strap_transfer_control

;  IMPORT DDRInit
;   IMPORT _seg_CODE_end_
;************************************************************
;* DKB/OBM entry point										*
;************************************************************
__main
    ENTRY
        GLOBAL ExceptionVectors
ExceptionVectors
    	B ResetHandler
    	B UndefinedHandler
    	B SwiHandler
    	B PrefetchHandler
    	B AbortHandler
    	NOP    	    			; Reserved Vector
    	B SaveProgramState 		; B IRQ Interrupt SaveProgramState
    	B FiqHandler ; FIQ interrupts not anabled

		NOP
 ;
 ; Table used for version information
 ; and parameters passed up from the boot ROM
 ;
		GLOBAL	ExecutableVersionInfo
        GLOBAL IniRWROMBase
        GLOBAL IniRWRAMBase
        GLOBAL IniRWRAMLimit
        GLOBAL IniZIRAMBase
        GLOBAL IniZIRAMLimit
;        GLOBAL _seg_BSS_end_
ExecutableVersionInfo
		DCD	MajorMinor, Date, Processor
__Fuses
		DCD 0x01
__OBM_Size
 IF :DEF: GCC
		DCD __data_size
 ELSE
		DCD IniRWROMLimit			; This is the size of the OBM
 ENDIF
__OSCR0
		DCD 0x0
__TIM_location
		DCD 0xd1008000
__TBR_version
		DCD 0x0

        ALIGN 32

; redirection vectors
; SetXXXVector will store the address of exception handlers here...
        GLOBAL RedirectionVectors
RedirectionVectors
    	DCD		ResetHandler
    	DCD		UndefinedHandler
    	DCD		SwiHandler
    	DCD		PrefetchHandler
    	DCD		AbortHandler
    	DCD		0    	    			; Reserved Vector
    	DCD		SaveProgramState 		; B IRQ Interrupt SaveProgramState
    	DCD		FiqHandler ; FIQ interrupts not anabled

	; RESET entry point



;************************************************************
;* Initialize INT. Cntl. Mask all Ints, and set them to IRQ
;************************************************************

	IF :DEF: ASPNA0
;------------------------------------------------------------------------------------------------
;   Start of PLL1 Reconfig via WFI
;------------------------------------------------------------------------------------------------
;;
;; some macros for use by the WFI handler.

		MACRO
		DELAY
		mov	r8, #0x00010000
; loop here
		subs r8, r8, #1
		subne pc, pc, #12   	; repeat decrement if not 0
		MEND

	ENDIF


	; RESET entry point

ResetHandler
   IF :DEF: PXA9XX              ; Tavor TD or TTC1
      IF :DEF: A0_BOARD             ; For A0 stepping
         LDR     r9, [r10, #4]
         STR     r9,  __OSCR0
         LDR     r9, [r10, #8]
         STR     r9,  __Fuses
         LDR     r9, [r10, #32]!     ; pair count
lbl_search_tim
         LDR     r8, [r10, #4]!
         LDR     r7, =0x54494D48     ; TIMH
         CMP     r8, r7
         BNE     lb_search_cont
         LDR     r8, [r10, #4]!
         STR     r8, __TIM_location
         B       lb_tim_found
lb_search_cont
         ADD     r10, r10, #8
         SUB     r9,  r9,  #1
         CMP     r9,  #0
         BNE     lbl_search_tim
lb_tim_found
         LDR     r10, =0xFFE00024
         LDR     r9,  [r10]
         STR     r9, __TBR_version
      ENDIF
      IF :DEF: Y0_BOARD              ; For Y0 stepping
         LDR     r9, [r10]
         STR     r9,  __OSCR0
         LDR     r9, [r10, #4]
         STR     r9,  __Fuses
         LDR     r8, [r10, #8]!
         STR     r8, __TIM_location
         LDR     r10, =0xFFE00024
         LDR     r9,  [r10]
         STR     r9, __TBR_version
      ENDIF
	ENDIF
	IF :DEF: ASPN
	LDR     r9, [r10]
    STR     r9,  __OSCR0		; Store the OSCR0
	LDR     r9, [r10, #0x4]
    STR     r9,  __Fuses		; Store the passed up fuses
	LDR     r9, [r10, #0x10]	; Aspen A0 has error code & rsvd preceding Tim Location.
    STR     r9, __TIM_location	; Store the TIM location
	LDR     r9, =0xFFE00024		; location of bootROM version
	STR		r9, __TBR_version	; Store the TBR version
	ENDIF

	IF :DEF: ASPNA0

;
; there's a relationship between the MCU and the ability to wake from idle:
; the MCU must be running. take care of that here.
	ldr		r3,		=0xb0000000		; mcu_base
	mov		r4,		#1				; sdram_init_req bit.
	str		r4,		[r3, #0x0120]	; mcu:user_initiated_command register
									; if the mcu is already running,
									; this shouldn't hurt anything.

	; wait for the MCU to finish initializing.
mcu0
	ldr		r4,		[r3, #0x01b0]	; mcu:dram_status register
	tst		r4,		#1				; test the "init done" bit
	beq		mcu0						; branch if "init done" isn't set yet.



;
; PLL Control and Divisor Setup
;
	; start of PLL1 configuration and idle mode entry preparation.



;;
;; 1) MPMU_PLL1_REG1: Configure the VCO and VCO divisor for proper operation at 1.248 GHz.
	ldr		r3,		=0xd4050000		; MPMU base
	ldr		r4,		=0x91040664		; 1 0 010 0010 0000 100 0000 0110 01 10 010 0
									; 31         1   pll1_ovrd_input (override input bits from pll1_1_reg, pll1_2_reg and pll1_ssc_reg)
									; 30         0   reserved
									; 29:27    010   intpl (interpolater bias current selection)
									; 26:23   0010   vco_div_sel_se (post divider for single ended output)
									; 22:19   0000   vcodiv_se_diff [1] (post divider for differential output)
									; 18:16    100   vco_vrng (pll v to i gain)
									; 15:12   0000   test_mon (dc points test control register)
									; 11:08   0110   kvco (VCO select)
									; 07:06     01   vddl (internal VDD supply control)
									; 05:04     10   vddm (VCO supply control)
									; 03:01    010   icp [7.5 uA] (charge pump current control)
									; 00         0   vcofbst
	str		r4,		[r3, #0x50]		; MPMU_PLL1_REG1
	ldr		r5,		[r3, #0x50]		; make sure write completes.



;;
;; 2) MPMU_PLL1_REG2: Configure the capacitor selection
	ldr		r4,		=0x84000030		; 1 0 0 0 0 1 0 00000000000000000 0 0 1 1 0 0 0 0
									; 31         1   cap_sel
									; 30         0   gate_clk_ctrl_en
									; 29         0   gate_clk_ctrl
									; 28         0   freq_offset_mode_selection
									; 27         0   freq_offset_valid
									; 26         1   reset_ext
									; 25         1(0)clk_det_en
									; 24:08      0   freq_offset
									; 07         0   freq_offset_en
									; 06         0   diffclk_en
									; 05         1   sel_vco_clk_se
									; 04         1   sel_vco_clk_diff
									; 03         0   reserved
									; 02         0   bypass_en
									; 01         0   bypass_fbdiv
									; 00         0   reserved
	str		r4,		[r3, #0x54]		; MPMU_PLL1_REG2
	ldr		r5,		[r3, #0x54]		; make sure write completes.



;;
;; 3) MPMU_FCCR: enable an MFC after entering system sleep mode, 
;;			  and configure the PLL1REFD and PLL1FBD fields for a 1.248 GHz VCO frequency.
	ldr		r4,		=0x2000C290		; 001 000 000 0000000 1 1 00001 010010000
									; 31:29    001   coreclksel
									; 28:26    000   reserved
									; 25:23    000   axiclksel
									; 22:16      0   reserved
									; 15         1   MFC
									; 14         1   pll1cen
									; 13:09      1   pll1refd
									; 08:00   0x90   pll1fbd
	str		r4,		[r3, #0x08]		; MPMU_FCCR
	ldr		r5,		[r3, #0x08]		; make sure write completes.

;; 4) Set up for and enter System Sleep mode. 
;;
;; 4a) ICU_INT_CONF36: Unmask Interrupt 36 to automatically wakeup after MFC completes
	ldr		r3,		=0xD4282000		; ICU base
	ldr		r4,		=0x0000005F		; [31-7 : RESERVED] 101 1111
									; 31:07      0   reserved
									; 06         1   Marvell Sheeva Interrupt
									; 05         0   Reserved
									; 04         1   IRQ/FIQ (1:IRQ, 0: FIQ)
									; 03:00    0XF   Priority/Masking
	str		r4,		[r3, #0x90]		; ICU_INT_CONF36
	ldr		r5,		[r3, #0x90]		; make sure write completes.



;;
;; 4b) APMU_IDLE_CFG: ensure required clocks are enabled
	ldr		r3,		=0xD4282800		; APMU base
	ldr		r4,		=0x00300302		; 0000000000 1 1 00000000 0 0 1 1000000 1 0
									; 31:22      0   reserved
									; 21         1   Marvell Sheeva Disable MC SW REQ
									; 20         1   Marvell Sheeva MC Wake EN
									; 19:12      0   reserved
									; 11         0   Marvell Sheeva Mask JTAG Idle 
									; 10         0   Marvell Sheeva L2 Clock Enable
									; 09         1   Marvell Sheeva L2 Assert Reset
									; 08:02   0x40   reserved
									; 01         1   Marvell Sheeva Idle
									; 00         0   reserved
	str		r4,		[r3, #0x18]		; APMU_IDLE_CFG
	ldr		r5,		[r3, #0x18]		; make sure write completes.



;;
;; 4c) MPMU_POCR: set osc stabilization and PLL lock-wait parameters
	ldr		r3,		=0xd4050000		; MPMU base
	ldr		r4,		=0x00070220		; 0 0000000 00000111 0000 001100110000
									; 31         0   force
									; 30:24      0   reserved
									; 23:16    0x7   osc25m
									; 15:12      0   reserved
									; 11:00  0x220   plllock
	str		r4,		[r3, #0x0c]		; MPMU_POCR
	ldr		r5,		[r3, #0x0c]		; make sure write completes.


;; 5) When the processor exits system sleep mode the PLL1 VCO frequency will be 1.248GHz
;;    and the output will be 624 MHz. After exiting system sleep software should clear interrupt 36. 

;;
;; 5a) MPMU_APCR: Set INTCLR
	ldr		r3,		=0xd4051000		; MPMU_APCR address
	ldr		r4,		[r3]			; get the current contents of MPMU_APCR.

	orr		r4,		#0x01000000		; x x x x  x x x 1  x x x x  x x x x  x x xxxxxxxxxxxxxx
									; 31         x   axisdd
									; 30         x   reserved
									; 29         x   slpen
									; 28         x   set always

									; 27         x   ddrcorsd
									; 26         x   apbsd
									; 25         x   setalways
									; 24         1   intclr

									; 23         x   slpwp0
									; 22         x   slpwp1
									; 21         x   slpwp2
									; 20         x   slpwp3

									; 19         x   reserved
									; 18         x   slpwp4
									; 17         x   slpwp5
									; 16         x   slpwp6

									; 15         x   slpwp7
									; 14         x   setalways
									; 13:00      X   reserved
	str		r4,		[r3]			; MPMU_APCR
	ldr		r5,		[r3]			; make sure write completes.

;;
;; 5b) MPMU_APCR: Clear INTCLR
	ldr		r4,		[r3]			; get the current contents of MPMU_APCR.

	and		r4,		#0xFEFFFFFF		; x x x x  x x x 0  x x x x  x x x x  x x xxxxxxxxxxxxxx
									; 31         x   axisdd
									; 30         x   reserved
									; 29         x   slpen
									; 28         x   set always

									; 27         x   ddrcorsd
									; 26         x   apbsd
									; 25         x   setalways
									; 24         0   intclr

									; 23         x   slpwp0
									; 22         x   slpwp1
									; 21         x   slpwp2
									; 20         x   slpwp3

									; 19         x   reserved
									; 18         x   slpwp4
									; 17         x   slpwp5
									; 16         x   slpwp6

									; 15         x   slpwp7
									; 14         x   setalways
									; 13:00      X   reserved
	str		r4,		[r3]			; MPMU_APCR
	ldr		r5,		[r3]			; make sure write completes.

;------------------------------------------------------------------------------------------------
;   End of PLL1 Reconfig via WFI
;------------------------------------------------------------------------------------------------
	ENDIF


;------------------------------------------------------------------------------------------------
;   Workaround for Aspen B0 DPF-845, Clock gating needs to be enabled (by clearing bit 6)
;------------------------------------------------------------------------------------------------
	IF :DEF: ASPNB0
	ldr		r0, =0xd4282c08		; AP_CPU_CONF, or just CPU_CONF
	ldr		r1, [r0]			; get current CPU_CONF contents
	bic		r1, r1, #(1<<6)		; CG_BYPASS0 is the bit that needs to be cleared.
	str		r1, [r0]			; update the CPU_CONF register.

	ldr		r2, [r0]			; read value back and make use of it
	add		r2, r2, #1			; to ensure the write completes.
	ELSE
	MOV		r0, r0              ;filler instruction
	ENDIF


	IF :LNOT: :DEF: MMP2
	IF :LNOT: :DEF: MMP3
	LDR		r0, =InterruptsBase
	MOV		r1, #0
	STR		r1, [r0, #IntsIcmr]
	STR		r1, [r0, #IntsIclr]
	ENDIF
	ENDIF

;*************************************************************************
; Save off passed parameters to allocated space above:
;	3.2.xx bootROM will pass an address to a structure in memory
; 	with the following entries:
; 		 	   OSCR0, Fuse values, TIM Addr
;
; Note: for MMP2 and MMP3, BootROM passes the transfer struct in a different way.
;*************************************************************************
;	IF :DEF: ASPN
;	LDR     r9, [r10]
;    STR     r9,  __OSCR0		; Store the OSCR0
;	LDR     r9, [r10, #0x4]
;    STR     r9,  __Fuses		; Store the passed up fuses
;	LDR     r9, [r10, #0x10]	; Aspen A0 has error code & rsvd preceding Tim Location.
;    STR     r9, __TIM_location	; Store the TIM location
;	LDR     r9, =0xFFE00024		; location of bootROM version
;	STR		r9, __TBR_version	; Store the TBR version
;	ENDIF
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; turn off the MMU when using the MMU testing code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	mrc		p15, 0, r1, c1, c0, 0	; Read CP15 Control Register (R1)
	ldr		r2, =0x3005			; turn off MMU, I/D caches,	and
	bic		r1, r1, r2			;   map Exception Vectors to low
  	mcr     p15, 0, r1, c1, c0, 0
	MOV 	r1, r1        		; CPWAIT
 	SUB 	pc, pc, #4
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Flush TLB's
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   	mov      r1, #0xFFFFFFFF
   	mcr      p15, 0, r1, c3, c0, 0   ; set up access to domain 0
   	mcr      p15, 0, r2, c8, c7, 0   ; flush I + D TLBs
   	mcr      p15, 0, r1, c7, c10, 4  ; drain write and fill buffers
   	MOV 	r2, r2        		; CPWAIT
 	SUB 	pc, pc, #4    		; CPWAIT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ContinueBoot
;************************************************************
;* Assign SP locations for all Modes of operation 			*
;* Reserve space for image that is downloaded by reducing   *
;* the size of internal RAM
;************************************************************
	LDR		r0, =BL_STACK_START			; from platform_defs.inc
	LDR 	r1, =BL_STACK_SIZE

   	MOV    	r2, r0		 ; Get memory for stack pointer
   	ADD    	r2, r2, r1	 ; Go length of that memory
  	SUB    	r2, r2, #8   ; Back off by two words

; Enter IRQ mode and set up the IRQ stack pointer
    MOV     R0, #Mode_IRQ          	; IRQ mode
    MSR     CPSR_c, r0
	; Initialize register r8-r14 in this mode with zeros
    MOV		r1, #0
    MOV		r8, r1
    MOV		r9, r1
;	MOV		r10, r1
    MOV		r11, r1
    MOV		r12, r1
    MOV		r13, r1
    MOV		r14, r1
    MOV     SP, r2                 	; Assign stack pointer for IRQ handler

; Enter SVC mode and set up the SVC stack pointer
    MOV     r0, #Mode_SVC          	; SVC mode, No interrupts
    MSR     CPSR_c, r0
	; Initialize register r8-r14 in this mode with zeros
    MOV		r1, #0
    MOV		r8, r1
    MOV		r9, r1
;   MOV		r10, r1
    MOV		r11, r1
    MOV		r12, r1
    MOV		r13, r1
    MOV		r14, r1
    SUB     r2, r2, #IrqStackSize
    MOV     SP, r2                 	; Assign stack pointer for SVC handler

;***********************************************************************************
;  Initialize the DDR
;
; Only Initialize DDR if it hasn't already been initialized by the TBR
; if we are running in DDR space, then we need to reload the whole OS
;***********************************************************************************
;  B copy_done


	IF :DEF: CORE624
	IMPORT CoreFreqChange624
	bl CoreFreqChange624
	ENDIF


;***********************************************************************************
;  Move OBM to DDR for more space
;***********************************************************************************

__CopyOBM
		IF :DEF: _DO_COPYOBM
        mov     r1,	#0			   	; Destination address in DDR
		adr		r0, __main
		cmp		r1, r0				; if the start of the OBM is at destination
		beq		__CopyTIM			;    then we do not need to copy

        ldr		r2, __OBM_Size		; get the size of the OBM
		sub r2, r2, r0

;
;       Copy the DKB code into the destination (load) address (iSRAM)
;
obm_cd   ldr     r3,     [r0],   #4      	; Get data in source address
        str     r3,     [r1],   #4      	; Place data in destination address
        sub		r2,     r2,     #4      	; Decrement size by one word
		cmp		r2,		#0
        bgt     obm_cd                   	; Copy data until it is all moved
		ENDIF

;***********************************************************************************
;  Move TIM to correct ISRAM location, so we don't wipe
;  it out when we initialize the stack
;***********************************************************************************

__CopyTIM
		IF :DEF: _DO_COPYTIM_
		ldr     r1,	=__TIM_location		; get the stored address of the TIMM
        ldr     r0, [r1]
		ldr     r1,	=_TIM_Default_Loc	; Destination address in DDR
		ldr		r2, =_TIM_Max_Size		; current max TIM size

		cmp		r0,	r1				; if the start of the OBM is at destination
		beq		copy_done			;    then we do not need to copy

;
;       Copy the DKB code into the destination (load) address (iSRAM)
;
tim_cd
		ldr     r3,     [r0],   #4      	; Get data in source address
        str     r3,     [r1],   #4      	; Place data in destination address
        sub		r2,     r2,     #4      	; Decrement size by one word
		cmp		r2,		#0
        bgt     tim_cd                   	; Copy data until it is all moved
		ENDIF

copy_done


;***********************************************************************************
;  Enable all coprocessor registers
;***********************************************************************************

   ORR		r0, r0, #0x3f00
   MCR 		p15, 0, r0, c15, c1, 0
   MOV 		r0, r0
   MOV 		r0, r0
   MOV 		r0, r0
   MOV 		r0, r0


;***********************************************************************************
;  Enable MMU
;
;***********************************************************************************

;__ENABLE_MMU
;   INCLUDE mmuenable.INC


;*********************************************************************************
; Initialize memory required by C code
;
;	r0 = Start of the RW section in ROM (where the RW resides inside the image itself)
;	r1 = Start location to put RW in RAM (runtime location of RW section)
;	r2 = End location of RW section in RAM (runtime location of RW section)
;	r3 = Start location of zero initialized section in RAM
;	r4 = End location of zero initialized section in RAM
;
;	r5 is used when copying RW and when zeroing out BSS section
;
;**********************************************************************************
__JUMP_TO_C_CODE

    ; GCC memory init
    IF :DEF: GCC
        LDR    r0, =__egot          ; Get pointer to ROM data characteristics
        LDR    r1, =__data_start    ; Get pointer to RAM data characteristics
        LDR		r2, =__data_size		; RAM section length
		add		r2, r2, r1				; Add start + size to get end of RW section
        LDR		r3, =__bss_start		; get the start of the BSS section
		LDR		r4, =__bss_end			; get the end of the BSS section

	; RVCT & SDT memory init
	ELSE
	LDR		r0, =IniRWROMBase		; Start Local of RW data in ROM
	LDR		r0, [r0]
  		IF :DEF: SDT
  			add		r0, r0, #8			; for SDT, add 8 bytes to skip header info in init_data section
   		ENDIF
	LDR		r1, =IniRWRAMBase		; Start Local of RW data in RAM
	LDR		r1, [r1]
	LDR		r2, =IniRWRAMLimit		; End Local of RW data in RAM
	LDR		r2, [r2]
		LDR		r3, =IniZIRAMBase		; Start Local of ZI data in RAM
		LDR		r3, [r3]
		LDR		r4, =IniZIRAMLimit		; End Local of ZI data in RAM
		LDR		r4, [r4]
	ENDIF

		; 1. Copy RW data
MoveRWSection
   	CMP	   r1, r2				    ; have we reached the end?
	   	BGE		ClearBSS	 			; branch if the start is greater than end
		LDR		r5, [r0], #4 			; move ROM copy source
	   	STR		r5, [r1], #4 			; to RAM target
	   	B		MoveRWSection 			;

 ; 2. Clear out BSS
ClearBSS
		MOV	   r5, #0
WriteZeroes
		STR	   r5, [r3], #4				; write a zero, then increment 'start address' by 4
		CMP	   r3, r4					; compare the 'start' and 'end' addresses
		BLT	   WriteZeroes				; continue writing zeros till we finish clearing the area


;************************************************************
;* Branch to OEM Boot Main program							*
;************************************************************
	IF :LNOT: :DEF: MMP2
	IF :LNOT: :DEF: MMP3
	LDR     r1, =__Fuses				; Restore platform configuration passed in by the BootROM
	LDR		r0, [r1]					; fuses
	LDR     r1, =__TIM_location
    LDR     r2, [r1]					; Tim location
	LDR		r1, =__OSCR0
    LDR     r1, [r1]					; OSCR0
    ENDIF
    ENDIF

    IF :DEF: USE_BOOTSTRAP              ; Tavor TD
	B	 	boot_strap_main
    ELSE
	B	 	BootLoaderMain
    ENDIF

LOOPFOREVER
    B	LOOPFOREVER			            ; forever loop



	IF :DEF: USE_BOOTROM_TRANSFERCONTROL
	;This routine transfers control of the processor to the image
	;loaded at the offset stored in r0.  It is assumed that
	;all integrity checking, copying, and setting up chip selects
	; has been completed prior to calling this routine.
TransferControl
	MOV 	r4, r0				; r4 = Image Entry address
	MOV		r5, r1				; r5 = Structure Address
	MCR	p15, 0, r0, c7, c5, 0 	; Invalidate I Cache & BTB
	MCR	p15, 0, r0, c7, c10, 4  ; Drain the Write & Fill buffers
   	MRS 	r0, CPSR		 	; Disable both IRQ and FIQ interrupts
	BIC	r0, r0, #(MaskInts | MaskMode)
	ORR	r0, r0, #(IntsDisable | SVCMode) ; SVC32 mode with IRQs + FIQs disabled
   	MSR 	cpsr_cf, r0

	; Load up resume params
	LDR		r0, [r5]			; Param 0
	LDR		r1, [r5, #0x4]		; Param 1
	LDR 	r2, [r5, #0x8]		; Param 2
	LDR		r3, [r5, #0xC]		; Param 3

	; Load up transfer structure address for OBM
	MOV		r10, r5
	ADD		r10, r10, #0x10
   	MOV 	pc, r4				; Bye Bye

;************************************************************
;* 2nd Step - Turn OFF MMU, Flush TLB's
;************************************************************
__TransferControlCode
; turn off the MMU when using the MMU testing code
	mrc		p15, 0, r1, c1, c0, 0	; Read CP15 Control Register (R1)
	ldr		r2, =0x3005			; turn off MMU, I/D caches,	and
	bic		r1, r1, r2			;   map Exception Vectors to low
  	mcr     p15, 0, r1, c1, c0, 0
	MOV 	r1, r1        		; CPWAIT
 	SUB 	pc, pc, #4

; Flush TLB's
   	mov      r1, #0xFFFFFFFF
   	mcr      p15, 0, r1, c3, c0, 0   ; set up access to domain 0
   	mcr      p15, 0, r2, c8, c7, 0   ; flush I + D TLBs
   	mcr      p15, 0, r1, c7, c10, 4  ; drain write and fill buffers
   	MOV 	r2, r2        		; CPWAIT
 	SUB 	pc, pc, #4    		; CPWAIT


;************************************************************
;* 3nd Step - Copy the boot image to it's load address
;************************************************************
	cmp		r7,	r9				; compare the load address (LA) to current address (CA) of image
	beq		__JumpToBootImage	;  if they are the same, we don't need to move any data
	bls		__CopyFromStart		;  if (LA < CA) CopyFromStart
	add		r1,	r9,	r8			;
	cmp		r7,	r1				;  if (LA > CA + Size) CopyFromStart
	bhi		__CopyFromStart		;
								;  else CopyFromEnd
__CopyFromEnd					;  __CopyFromEnd : copies images starting at end
	sub		r8, r8, #4			; decrement size by 1 word
	ldr     r3, [r9, r8]      	; Get data in current address
	str     r3, [r7, r8]     	; Place data in load address
	cmp		r8, #0
	bgt		__CopyFromEnd		; continue copying until size is less than 0
	b		__JumpToBootImage

__CopyFromStart					;  __CopyFromStart: copies images starting at beginning
	ldr     r3, [r9], #4      	; Get data in current address
	str     r3, [r7], #4      	; Place data in load address
	sub		r8, r8, #4			; decrement size by 1 word
	cmp		r8, #0
	bgt		__CopyFromStart		; continue copying until size is less than 0
	b		__JumpToBootImage

__JumpToBootImage
	MOV  	pc, r0				; Jump next desired code after DKB (ie. OBM, OS Loader)

	ENDIF




 ; bpc: the new TransferControl method...note the new parameter list.
;***********************************************************************************
;  1st step - Move Transfer Control to a Non-MMU translated area of the DDR
;
;  r0 = Boot Image Load Address
;  r1 = Boot Image Size
;  r2 = Boot Image Current Address
;  r3 = Fuse Value
;
;***********************************************************************************
		; force the assembler to emit any constants that it has accumulated up til now.
		; otherwise those constants will unnecessarily be included in the relocation.
		LTORG

TransferControl
		ldr		r4,		=0xFFFFFFF8
		and		r7,		r0,		r4			; Save the boot image load address in r7 (make sure it is word aligned)
		mov		r8, 	r1					; Save the boot image size in r8.
		and		r9,		r2,		r4			; Save the boot image current address in r9 (make it word aligned)
		mov		r10, 	r3					; Save off platform settings to pass to the OBM in r10.

		ldr     r4, 	=__TransferControlCode		; source address: start of function
        ldr		r2,		=__TransferControlCodeEnd	; used to compute length of function
        sub		r2,		r2,		r4					; end - start = len. r4=start, r2=len

        ldr     r1,     =0x5f000				; Destination address in DDR
;        add		r1,		r1,	#ISRAM_SIZE
;        sub		r1,		r1,		r2					; back off by length of function
        mov		r5,		r1
;
;       Copy the DKB code into the destination (load) address (iSRAM)
;		r4 = start, r2 = length, r1 = destination
;
bc_cd1
		ldr     r3,     [r4],   #4      	; Get data in source address
        str     r3,     [r1],   #4      	; Place data in destination address
        subs    r2,     r2,     #4      	; Decrement doubleword count
        bne     bc_cd1                   	; Copy data until it is all moved
		MOV  	pc, 	r5					; Jump to 1:1 Mapped portion of DDR area

		LTORG
		; the constants for __TransferControlCode, __TransferControlCodeEnd and
		; 0xd1020000 will be generated here. the "mov pc, r5" jumps around the constants...

;************************************************************
;* 2nd Step - Turn OFF MMU, Flush TLB's
;************************************************************
__TransferControlCode
; turn off the MMU when using the MMU testing code
	mrc		p15, 0, r1, c1, c0, 0	; Read CP15 Control Register (R1)
	mov		r2, #0x3000			    ; turn off MMU, I/D caches, and
	orr		r2, r2, #0x0005			; turn off MMU, I/D caches, and
	bic		r1, r1, r2			    ;   map Exception Vectors to low
  	mcr     p15, 0, r1, c1, c0, 0
	MOV 	r1, r1        		; CPWAIT
 	SUB 	pc, pc, #4

; Flush TLB's
   	mov      r1, #0xFFFFFFFF
   	mcr      p15, 0, r1, c3, c0, 0   ; set up access to domain 0
   	mcr      p15, 0, r2, c8, c7, 0   ; flush I + D TLBs
   	mcr      p15, 0, r1, c7, c10, 4  ; drain write and fill buffers
   	MOV 	r2, r2        		; CPWAIT
 	SUB 	pc, pc, #4    		; CPWAIT


;************************************************************
;* 3nd Step - Copy the boot image to it's load address
;************************************************************
	cmp		r7,	r9				; compare the load address (LA) to current address (CA) of image
	beq		__JumpToBootImage	;  if they are the same, we don't need to move any data
	bls		__CopyFromStart		;  if (LA < CA) CopyFromStart
	add		r1,	r9,	r8			;
	cmp		r7,	r1				;  if (LA > CA + Size) CopyFromStart
	bhi		__CopyFromStart		;
								;  else CopyFromEnd
__CopyFromEnd					;  __CopyFromEnd : copies images starting at end
	sub		r8, r8, #4			; decrement size by 1 word
	ldr     r3, [r9, r8]      	; Get data in current address
	str     r3, [r7, r8]     	; Place data in load address
	cmp		r8, #0
	bgt		__CopyFromEnd		; continue copying until size is less than 0
	b		__JumpToBootImage

__CopyFromStart					;  __CopyFromStart: copies images starting at beginning
	ldr     r3, [r9], #4      	; Get data in current address
	str     r3, [r7], #4      	; Place data in load address
	sub		r8, r8, #4			; decrement size by 1 word
	cmp		r8, #0
	bgt		__CopyFromStart		; continue copying until size is less than 0
	b		__JumpToBootImage

__JumpToBootImage
	MOV  	pc, r0				; Jump next desired code after DKB (ie. OBM, OS Loader)

	LTORG
	; the LTORG directive will cause any constants implicitly created by the code
	; above to be generated right here. that way the function length calculation
	; will include the space taken up by these constants. for example, a
	; ldr r2, =0x3005 instruction will implicitly create the constant 0x3005.
__TransferControlCodeEnd


;************************************************************
; IRQ Vector Handler - Save Program State
;************************************************************
    GLOBAL SaveProgramState
SaveProgramState
	SUB	R14, R14, #4	; Adjust return address before saving it
	STMFD	SP!, {R14}
	MRS	R14, SPSR
	STMFD   SP!, {R0-R12,R14}

	BL 	IrqHandler

	; Modify CPS register
	MRS	r12, CPSR
	ORR    	r12,r12,#0x80    ; Disable IRQ interrupts
	MSR	CPSR_c, r12

	LDMFD 	SP!, {R0-R12,R14}
	MSR	SPSR_cxsf, R14
	LDMFD 	SP!, {PC}^



; VectorRedirector: jump to a location stored exactly 0x18 bytes from current pc.
;                   as long as the redirected vector table immediately follows the
;                   actual exception handlers, this instruction can be used for all.
VectorRedirector
	ldr		pc, [pc,#(RedirectionVectors-ExceptionVectors-0x08)]	; vector is 0x18. pc=current_addr+8. irq ptr is at 0x38.

;************************************************************
; SetInterruptVector
; input args:
;  r0 = address of IrqHandler
;
; save the IrqHandler address right after the vector table.
; assumes address 0 is mapped.
;************************************************************
	EXPORT SetInterruptVector
SetInterruptVector
	STMFD		SP!, {R0-R4, LR}

	; first save the address to the irq handler
	; so the vector redirector can find it
	mov			r1, #0
	str			r0, [r1, #(RedirectionVectors-ExceptionVectors+0x18) ]	; 0x18 = offset of irq vector

	; now set up the vector redirector
	; the vector redirector will exist immediately after the location of the execption handlers: 0x40
	ldr			r2, =VectorRedirector
	ldr			r0, [r2]
	str			r0, [r1, #(0x00+0x18) ];	0x00=modify actual vector table. 0x18 = offset of irq vector



	LDMFD   	SP!,    {R0-R4, PC}


;************************************************************
; EnableIrqInterrupts
;************************************************************

EnableIrqInterrupts
	STMFD		SP!, {R0-R4, LR}
   	MRS    	r0,CPSR        ; get the processor status
   	BIC    	r0,r0,#0x80    ; Enable IRQ interrupts
   	MSR    	cpsr_cf,r0     ; SVC 32 mode with interrupts disabled
	LDMFD   	SP!,    {R0-R4, PC}

;************************************************************
; DisableIrqInterrupts
;************************************************************

DisableIrqInterrupts
	STMFD		SP!, {R0-R4, LR}
   	MRS    	r0,CPSR        ; get the processor status
   	ORR    	r0,r0,#0x80    ; Disable IRQ interrupts
   	MSR    	cpsr_cf,r0     ; SVC 32 mode with interrupts disabled
	LDMFD   	SP!,    {R0-R4, PC}




;************************************************************
; raise: for handling divide exceptions. called from libgcc
;************************************************************
        EXPORT raise
raise
        b       raise




;----------------------------
; UINT32 ObmXsGetProcessorVersion (void)
;
;   Get processor version info
;
;   Input:   None
;   Return:  R0:  ID Register contents
;
;   Note: For first silicon of Cotulla, should be 0x69052000
;
;   Nonsymbolic core instruction:
;       MRC p15, 0, Rd, 0, 0, 0

CP15    			CP  15 ; Alias for Coprocessor 15 (rather than just "p15")
CP15IDReg     CN  0

    ALIGN 4
    EXPORT  ObmXsGetProcessorVersion
ObmXsGetProcessorVersion

    ;MRC     CP15,0,r0,CP15IDReg,CP15IDReg,0
       MRC p15, 0, r0, c0, c0, 0

		BX  LR



;----------------------------
; UINT32 ObmXsGetDebuggerStatus (void)
;
;   Get debugger status
;
;   Input:   None
;   Return:  R0:  The value of cp14 -- bits [30,31] reflects if the debugger is connected or not  (1 == connect)
;


    ALIGN 4
    EXPORT  ObmXsGetDebuggerStatus
ObmXsGetDebuggerStatus

       MRC p14, 0, r0, c10, c0, 0

		BX  LR


;------------------------------------------------------------------------------------
; boot_strap_transfer_control
;   it is used by first stage bootstrap to transfer control to second stage bootstrap
;
boot_strap_transfer_control
	    MOV 	r4, r0				             ; r4 = Image Entry address
	    MOV		r5, r1				             ; r5 = Structure Address
	    MCR	    p15, 0, r0, c7, c5, 0 	         ; Invalidate I Cache & BTB
	    MCR	    p15, 0, r0, c7, c10, 4           ; Drain the Write & Fill buffers
   	    MRS 	r0, CPSR		 	             ; Disable both IRQ and FIQ interrupts
	    BIC	    r0, r0, #(MaskInts | MaskMode)
	    ORR	    r0, r0, #(IntsDisable | SVCMode) ; SVC32 mode with IRQs + FIQs disabled
   	    MSR 	cpsr_cf, r0
	    ; Load up transfer structure address for OBM
	    MOV		r10, r5
   	    MOV 	pc, r4				             ; Bye Bye



;RVCT
 IF :LNOT: :DEF: GCC
  IMPORT  ||Load$$ER_RW$$Base||		; ROM address	 - base of RW data
  IMPORT  ||Image$$RO$$Limit||		; size of Code section (Image - RW)
  IMPORT  ||Image$$ER_RW$$Base||		; ISRAM address  - base of RW data
  IMPORT  ||Image$$ER_RW$$Limit||	; ISRAM address  - end of RW data
  IMPORT  ||Image$$ZI$$Base||		; zero init start
  IMPORT  ||Image$$ZI$$Limit||		; zero init end

IniRWROMBase	DCD ||Load$$ER_RW$$Base||
IniRWROMLimit   DCD ||Image$$RO$$Limit||
IniRWRAMBase	DCD ||Image$$ER_RW$$Base||
IniRWRAMLimit	DCD ||Image$$ER_RW$$Limit||
IniZIRAMBase	DCD ||Image$$ZI$$Base||
IniZIRAMLimit	DCD ||Image$$ZI$$Limit||

;    ALIGN    0x00040
    LTORG
 ENDIF
   END

