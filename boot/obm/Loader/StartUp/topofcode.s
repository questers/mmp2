@/******************************************************************************
@**	(C) Copyright 2007 Marvell International Ltd.  
@**	All Rights Reserved
@******************************************************************************/

@******************************************************************************
@**
@**  COPYRIGHT (C) 2002, 2003 Intel Corporation.
@**
@**  This software as well as the software described in it is furnished under
@**  license and may only be used or copied in accordance with the terms of the
@**  license. The information in this file is furnished for informational use
@**  only, is subject to change without notice, and should not be construed as
@**  a commitment by Intel Corporation. Intel Corporation assumes no
@**  responsibility or liability for any errors or inaccuracies that may appear
@**  in this document or any software that may be provided in association with
@**  this document.
@**  Except as permitted by such license, no part of this document may be
@**  reproduced, stored in a retrieval system, or transmitted in any form or by
@**  any means without the express written consent of Intel Corporation.
@**
@**  FILENAME:	OBM_StartUp.s
@**
@**  PURPOSE: 	StartUp code for Monahans variant OBM code
@**
@******************************************************************************

   .global _seg_CODE_end_
_seg_CODE_end_

   .global __topofcode__
__topofcode__

		.align 4
		.word	0xbc
		.word	0xfc
		.word	__bss_start
		.word	__bss_end

