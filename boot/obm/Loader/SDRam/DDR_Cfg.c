
	   /******************************************************************************
 *
 *  (C)Copyright 2010 Marvell. All Rights Reserved.
 *  
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF MARVELL.
 *  The copyright notice above does not evidence any actual or intended 
 *  publication of such source code.
 *  This Module contains Proprietary Information of Marvell and should be
 *  treated as Confidential.
 *  The information in this file is provided for the exclusive use of the 
 *  licensees of Marvell.
 *  Such users have the right to use, modify, and incorporate this code into 
 *  products for purposes authorized by the license agreement provided they 
 *  include this notice and the associated copyright notice with any such
 *  product. 
 *  The information in this file is provided "AS IS" without warranty.
 *
 *
 *  FILENAME:	DDR_Init.c
 *
 *  PURPOSE: 	Contain code to handle DDR configuration from 
 *				op codes contained in the TIM header.
 *
******************************************************************************/

#include "DDR_Cfg.h"
#include "sdram_config.h"
#if BOOTROM
#include "bootrom.h"
#else
#include "BootLoader.h"
#endif


#if NEW_DDRCNFG
//
//ConfigureDDRMemeory routine
//
//	 This routine will process a ddr configuration package from the TIM header
//
//
//
//
unsigned int ConfigureDDRMemory(pDDR_PACKAGE DDR_PID){
	DDR_OPS_S Operations;
	pDDR_OPS_S pOperations;
	pINSTRUCTION_S pDDR_Instructions;
	unsigned int Retval = NoError;
	unsigned long faultaddr;
	unsigned int *ptemp = NULL;

   	pOperations = &Operations;
	pDDR_Instructions = NULL;
	memset(pOperations, 0, sizeof( DDR_OPS_S ));
	 
	if( DDR_PID == NULL )
		return GeneralError;
		 
	if(((DDR_PID->WRAH.Identifier & 0xFFFFFF00 )>> 8) !=  DDRTYPE)
		return UnknownReservedPackage; 	

	if( (DDR_PID->WRAH.Identifier ==	DDRID)  ||
		(DDR_PID->WRAH.Identifier ==	DDRGID) || 
		(DDR_PID->WRAH.Identifier ==	DDRTID) ||
		(DDR_PID->WRAH.Identifier ==	DDRCID))
		return DDR_Package_Obsolete;

	//Get pointer to start of instructions array
	ptemp = (unsigned int *)(&DDR_PID->DDR_Operations);
	ptemp += (DDR_PID->NumberOperations * 2);
	pDDR_Instructions = (pINSTRUCTION_S)ptemp; 
    Retval = ProcessDDROps(&DDR_PID->DDR_Operations, DDR_PID->NumberOperations, pOperations);
	// Check some defaults here in case they are not set by user
	if(	pOperations->Loops <= 0)
		pOperations->Loops = 1;
	// allow for a 64MB window (BIT26) for mem testing default to 2K (BIT11), if not set by user
	if(	pOperations->MemTest == 1 ){
		if ( (pOperations->MemTestStartAddr < DDR_PHY_ADDR) || 
			 (pOperations->MemTestStartAddr > DDR_PHY_ADDR + BIT26))
			  pOperations->MemTestStartAddr =  DDR_PHY_ADDR; 
		if ( (pOperations->MemTestSize + pOperations->MemTestStartAddr ) > (DDR_PHY_ADDR + BIT26)||
			  (pOperations->MemTestSize <= 0 ))
			  pOperations->MemTestSize =  BIT11; 
	 }		  	

	if (pOperations->Init == 1 )
	{
		// Start loop to intialize DDR Memory
		while( Operations.Loops ){
		   	Retval = ProcessInstructions(pDDR_Instructions, DDR_PID->NumberInstructions, Operations.IgnoreTimeOuts);
			if(Retval == NoError){
				// DDR OK, Check if we do a Memory Test
			  	if( Operations.MemTest ){
			 		// Memory Test Enabled
			 		Retval = CheckMemoryReliability( (unsigned long)Operations.MemTestStartAddr, Operations.MemTestSize, &faultaddr);
					if(Retval != NoError){
				  		// Memory Test failed, try again
				  		Operations.Loops--;
				  		continue;
			  		}
			  		else {
					  	// Memory Test OK
						Operations.Loops = 0;
					  	break;
					}
				}
				else {
					// Memory Test Disabled
					Operations.Loops = 0;
					break;	
				}
			}
			else {
				// DDR Init Error - Try again
				Operations.Loops--;
				continue;
			}//End DDR Init
		}//Endwhile
	}
	else
	{
	   Retval = DDR_InitDisabled;
	}  
  	return Retval;
}
		
//
//ProcessDDROps routine
//
//	 This routine will process a ddr operations structure in the TIM
//
//
//
//
unsigned int ProcessDDROps(pDDR_OPERATION pDDR_OPS, unsigned int Num, pDDR_OPS_S Operations){
	unsigned int i;
	unsigned int Retval = NoError;

	if(pDDR_OPS == NULL)
		return NULLPointer; 
	for(i = 0; i < Num; i++){
		switch(pDDR_OPS->OpId){
			case DDR_NOP:
				break;
			case DDR_INIT_ENABLE:
				 Operations->Init =  pDDR_OPS->OpValue;				
				 break;
			case DDR_MEMTEST_ENABLE:
				 Operations->MemTest =  pDDR_OPS->OpValue;				
				break;
			case DDR_MEMTEST_START_ADDR:
				 Operations->MemTestStartAddr =  pDDR_OPS->OpValue;				
				break;
			case DDR_MEMTEST_SIZE:
				 Operations->MemTestSize =  pDDR_OPS->OpValue;				
				break;
			case DDR_INIT_LOOP_COUNT:
				 Operations->Loops =  pDDR_OPS->OpValue;				
				break;
			case DDR_IGNORE_INST_TO:
				 Operations->IgnoreTimeOuts =  pDDR_OPS->OpValue;				
				break;
 			default:
				AddMessage((UINT8_T*) ("*** Unknown Operation...\0"));
				Retval = DDR_Unknown_Operation; 
		 }
		 if (Retval != NoError)
			break;
		 pDDR_OPS++;		
	
	}	
	return Retval;
}
#endif
/* This routine checks to see if a DDR entry is present in the Reserved Area of the TIM
*  If present it will attempt to configure it.
*/

unsigned int CheckAndConfigureDDR(pTIM pTIM_h, pFUSE_SET pFuses)
{
	UINT_T Retval = NoError;
	pWTP_RESERVED_AREA_HEADER pWRAH = NULL;
    // for ddr configuration and operating mode configuration:
	void			   *pvDDRScratchAreaAddr = 0;		// default scratch area address for memory test.
	unsigned long		ulDDRScratchAreaLen  = 2048;	// default scratch area length for memory test.
    pOPT_TIM_RESUME_SET	pOPT_TIMResumeSet    = NULL;	// points to resume record in TIM

    #if NEW_DDRCNFG
	pCIDP_ENTRY  MyConsumerEntry = NULL;
	unsigned int *pPackageID = NULL;
	int count;
    
    pFuses->bits.DDRInitialized = FALSE; // Initializing to false;

	// first check for the new CIDP entry
	MyConsumerEntry = FindMyConsumerArray(pTIM_h, MY_CONSUMER_ID);
	if(MyConsumerEntry != NULL)
	{
	   count = MyConsumerEntry->NumPackagesToConsume;
	   pPackageID = (unsigned int *)(&MyConsumerEntry->PackageIdentifierList);	
	   while(count)
	   {
	   		if (((*pPackageID & TYPEMASK)>> 8) == DDRTYPE )
	   		{
				pWRAH = FindPackageInReserved (&Retval, pTIM_h, *pPackageID);
				if (Retval != NoError)
					return DDR_PackageNotFound;
				Retval = ConfigureDDRMemory((pDDR_PACKAGE) pWRAH);
	   			if (Retval == NoError)
				{
					pFuses->bits.DDRInitialized = TRUE;
					count = 0;
					break;
				}
				else
				{
					// Errors that are not real errors
					if (Retval == DDR_InitDisabled)
					{
						// This was requested in the DDR package so it is not an error
						// pFuses->bits.DDRInitialized = FALSE (already set as default value above)
						Retval = NoError;
						count = 0;
						break;
					}//Endif

				}//Endifelse
			}//Endif
			pPackageID++;	// Increment to the next package!
			count --; // Even if we do not find the package type decrement count to avoid infinite loop
	   }//Endwhile count
	}
	else
    #endif
	{
        // Check the TIM for a DDR entry in Reserved area
        // If there's a resume package, use the ddr scratch area identified three.
        pWRAH = FindPackageInReserved (&Retval, pTIM_h, RESUMEBLID);
        if (Retval == NoError)
        {
            pOPT_TIMResumeSet = (pOPT_TIM_RESUME_SET) pWRAH;
            if (pOPT_TIMResumeSet != NULL)
            {
                pvDDRScratchAreaAddr = pOPT_TIMResumeSet->TimResumeDDRInfo.DDRScratchAreaAddr;
                ulDDRScratchAreaLen  = pOPT_TIMResumeSet->TimResumeDDRInfo.DDRScratchAreaLength;
            }
        }
		Retval = ConfigureMemoryController(pTIM_h, pvDDRScratchAreaAddr, ulDDRScratchAreaLen);	// 0, 2048: default address & length for memory test region.
        if (Retval == NoError)
            pFuses->bits.DDRInitialized = TRUE;
        else
            pFuses->bits.DDRInitialized = FALSE;
	}
    
    if (Retval == DDR_NotInitializedError) // Disabling DDR Initialization through TIM is not an Error.
        Retval = NoError;
    
	return Retval;
}
