/*
 * Copyright (C) 2008 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Copyright (c) 2010 Marvell Inc.
 * Modified by Lei Wen <leiwen@marvell.com>
 */
#include <common.h>
#include <fastboot/fastboot.h>
#include <fastboot/flash.h>
#include <jffs2/load_kernel.h>
#include <linux/mtd/mtd.h>
#include <mmc.h>
#include <timestamp.h>
#include <bmpmanager.h>

#define dprintf printf
#define mdelay(x) do{udelay(x*1000);}while(0)


#define FASTBOOT_VERSION "1.0"
DECLARE_GLOBAL_DATA_PTR;

//
//Google nexus use 2k page
//
#define USB_LDR_FLASH_PAGE_SIZE 2048

#define UDC_OUT_PACKET_SIZE 512
static unsigned int fb_mem = USB_LOADADDR;// - 2048;
static unsigned int ramdisk_addr;
static unsigned int ramdisk_size = 0;
static unsigned int kernel_addr = 0;
static unsigned int kernel_size = 0;
static int running = 0;
static unsigned rx_addr;
static unsigned int rx_length;
static unsigned int step=0;

#define BOOT_MAGIC "ANDROID!"
#define BOOT_MAGIC_SIZE 8
#define BOOT_NAME_SIZE 16
#define BOOT_ARGS_SIZE 512

struct boot_img_hdr
{
	unsigned char magic[BOOT_MAGIC_SIZE];

	unsigned kernel_size;  /* size in bytes */
	unsigned kernel_addr;  /* physical load addr */

	unsigned ramdisk_size; /* size in bytes */
	unsigned ramdisk_addr; /* physical load addr */

	unsigned second_size;  /* size in bytes */
	unsigned second_addr;  /* physical load addr */

	unsigned tags_addr;    /* physical addr for kernel tags */
	unsigned page_size;    /* flash page size we assume */
	unsigned unused[2];    /* future expansion: should be 0 */

	unsigned char name[BOOT_NAME_SIZE]; /* asciiz product name */

	unsigned char cmdline[BOOT_ARGS_SIZE];

	unsigned id[8]; /* timestamp / checksum / sha1 / etc */
};

typedef struct boot_img_hdr boot_img_hdr;
static int init_boot_linux(void)
{
	boot_img_hdr *hdr = (void*) fb_mem;
	unsigned page_mask = 2047;
	unsigned kernel_actual;
	unsigned ramdisk_actual;
	unsigned second_actual;

	if((kernel_size < 2048) || memcmp(hdr->magic, BOOT_MAGIC, BOOT_MAGIC_SIZE)){
		printf("bootimg: bad header\n");
		return -1;
	}

	if(hdr->page_size != 2048) {
		printf("bootimg: invalid page size\n");
		return -1;
	}

	kernel_actual = (hdr->kernel_size + page_mask) & (~page_mask);
	ramdisk_actual = (hdr->ramdisk_size + page_mask) & (~page_mask);
	second_actual = (hdr->second_size + page_mask) & (~page_mask);

	if(kernel_size != (kernel_actual + ramdisk_actual + second_actual + 2048)) {
		printf("bootimg: invalid image size");
		return -1;
	}

	/* XXX process commandline here */
	if(hdr->cmdline[0]){
		hdr->cmdline[BOOT_ARGS_SIZE - 1] = 0;
		printf("cmdline is: %s\n", hdr->cmdline);
		setenv("bootargs", hdr->cmdline);
	}

	/* XXX how to validate addresses? */

	kernel_addr = (void *)hdr + 2048;
	kernel_size = hdr->kernel_size;
	ramdisk_size = hdr->ramdisk_size;
	if (ramdisk_size > 0)
		ramdisk_addr = kernel_addr + 2048 + kernel_size;
	else
		ramdisk_addr = 0;

	printf("bootimg: kernel addr=%x size=%x\n",
			kernel_addr, kernel_size);
	printf("bootimg: ramdisk addr=%x size=%x\n",
			ramdisk_addr, ramdisk_size);

	return 0;
}

static void boot_linux(void)
{
	bootm_headers_t images;
	images.os.os = IH_OS_LINUX;
	images.ep = kernel_addr+USB_LDR_FLASH_PAGE_SIZE;
	images.rd_start = kernel_addr + USB_LDR_FLASH_PAGE_SIZE + kernel_size;
	images.rd_end = kernel_addr + USB_LDR_FLASH_PAGE_SIZE + kernel_size + ramdisk_size;
	do_bootm_linux(0, 0, 0 , &images);
	for(;;);
}

static void board_reboot(char* phase)
{
	if(phase&&!strcmp(phase,"bootloader"))
	{
		ptentry* misc;		
		printf("reboot to bootloader\n");
		//FIXME,hard code
		*(volatile unsigned int *)(0xd4010024)=0x77665500;	

		//also support misc partition command		
		misc = flash_find_ptn("misc");
		if(misc)
		{
			char buffer[512];
			memset(buffer,0,512);
			strcpy(buffer,"bootloader");
			flash_write(misc,0,buffer,512);
		}
		
	}
	udelay(1000*1000);
	reset_cpu(0);
}

static void board_getvar(char *arg1, char *arg2)
{
	char* env=getenv(arg1);

	if(env)
		strcpy(arg2,env);
	else
		strcpy(arg2,"");
}

static int board_oem(const char* args)
{
	return run_command(args,0);
}


static unsigned int hex2unsigned(char *x)
{
    unsigned int n = 0;

    while(*x) {
        switch(*x) {
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            n = (n << 4) | (*x - '0');
            break;
        case 'a': case 'b': case 'c':
        case 'd': case 'e': case 'f':
            n = (n << 4) | (*x - 'a' + 10);
            break;
        case 'A': case 'B': case 'C':
        case 'D': case 'E': case 'F':
            n = (n << 4) | (*x - 'A' + 10);
            break;
        default:
            return n;
        }
        x++;
    }

    return n;
}
static void num_to_hex8(unsigned n, char *out)
{
    static char tohex[16] = "0123456789abcdef";
    int i;
    for(i = 7; i >= 0; i--) {
        out[i] = tohex[n & 15];
        n >>= 4;
    }
    out[8] = 0;
}

static unsigned kernel_size_kB;
static void update_progress(void)
{
	if(!rx_length) return;
	//
	//START
	//
	if(0==(step%512))
	{
		printf("	%d%%	\r",((kernel_size_kB-rx_length/1024)*100)/kernel_size_kB);
		
	}
	step++;

	//
	//END
	//

}
static void cmd_handler(u8* cmdbuf,int len)
{
	char status[UDC_OUT_PACKET_SIZE];
	if (rx_length) {
		if(len!=FASTBOOT_RECV_BUF_SIZE)
		{
			printf("malform %d\n",len);
		}
		rx_length -= len;
		rx_addr +=len;
		fb_set_buf (rx_addr);
		if (!rx_length) {
			fb_tx_status("OKAY");
			printf("	100%    \n");
		}
		return;
	}
	
	if(len >= UDC_OUT_PACKET_SIZE)
		len = UDC_OUT_PACKET_SIZE - 1;
	cmdbuf[len] = 0;

	status[0]='\0';

	printf(">>%s\n",cmdbuf);

	if(memcmp(cmdbuf, "download:", 9) == 0) {
		rx_addr = fb_mem;
		fb_set_buf(rx_addr);
		
		rx_length = hex2unsigned(cmdbuf + 9);
		if (rx_length > (256*1024*1024)) {
			fb_tx_status("FAILdata too large");
			rx_length = 0;
			return;
		}
		kernel_addr = fb_mem;
		kernel_size = rx_length;
		kernel_size_kB = rx_length/1024;
		step=0;
		printf("recv data addr=%x size=%dKB\n", rx_addr, kernel_size_kB);
		strcpy(status,"DATA");
		num_to_hex8(rx_length, status + 4);
		fb_tx_status(status);
		return;
	}

	if(memcmp(cmdbuf, "boot", 4) == 0) {
		if(init_boot_linux())
			fb_tx_status("FAILinvalid boot image");
		else {
			printf("booting linux...\n");
			fb_tx_status("OKAY");
			boot_linux();
			//should not reach here
			fb_tx_status("FAILNot zImage, use bootsp oem cmd to boot special app");
			return;
		}
		return;
	}
	if (memcmp(cmdbuf, "flash", 5) == 0) {
		struct ptentry *ptn;		
		int extra = 0;
		ptn = flash_find_ptn(cmdbuf + 6);
		if(kernel_size == 0) {
			fb_tx_status("FAILno image downloaded");
			return;
		}
		if(ptn == 0) {
		    //bmp
		    if(!memcmp(cmdbuf + 6,"bmp.",4)){
		        int result;
		        dprintf("write bmp %s\n",(const char*)(cmdbuf + 6));
		        result = bmp_manager_writebmp((const char*)(cmdbuf + 6),kernel_addr,kernel_size);
		        if(result)
                    fb_tx_status("FAILpartition does not exist");
                else                    
                    fb_tx_status("OKAY");   
                return;		            
		    }
			fb_tx_status("FAILpartition does not exist");
			return;
		}
		if(!strcmp(ptn->name,"system") || !strcmp(ptn->name,"userdata")) {
			extra = 64;
		} else {
			kernel_size = (kernel_size + 2047) & (~2047);
		}
		

		dprintf("writing %d bytes to '%s'\n", 
				kernel_size, ptn->name);
		dprintf("writing '%s' (%d bytes)\n", ptn->name, kernel_size);
		if(flash_write(ptn, 0, (void*) kernel_addr, kernel_size)) {
			fb_tx_status("FAILflash write failure");
			dprintf(" - FAIL\n");
			return;
		} else {
			dprintf("partition '%s' updated\n", ptn->name);
			dprintf(" - OKAY\n");
		}
		fb_tx_status("OKAY");	
		return;
	}

	if (memcmp(cmdbuf, "erase", 5) == 0) {
        struct ptentry *ptn;
        ptn = flash_find_ptn(cmdbuf + 6);
        if(ptn == 0) {
            fb_tx_status("FAILpartition does not exist");
            return;
        }
        dprintf("erasing '%s'\n", ptn->name);
        if(flash_erase(ptn)) {
            fb_tx_status("FAILfailed to erase partition");
            dprintf(" - FAIL\n");
            return;
        } else {
            dprintf("partition '%s' erased\n", ptn->name);
            dprintf(" - OKAY\n");
        }
        fb_tx_status("OKAY");
        return;
	}

	if(memcmp(cmdbuf, "getvar", 6) == 0) {
		strcpy(status,"OKAY");
		if(!strcmp(cmdbuf + 7, "version")) {
			strcpy(status + 4, FASTBOOT_VERSION);
		} else if(!strcmp(cmdbuf + 7, "product")) {
			strcpy(status + 4, getenv("product"));
		} else if(!strcmp(cmdbuf + 7, "serialno")) {
			strcpy(status + 4, getenv("serial_number"));
		}else if (!strcmp(cmdbuf + 7, "part")){		
			int i;
			int length;
			ptentry* partition;
			strcpy(status + 4, "see fastboot screen for partition details");
			//status lenght can't be over 64 bytes per fastboot protocol
			printf("\nFlash Partiions\n");
			printf("physical     partition   start   size\n");
			printf("----------------------------------\n");
			
			for (i = 0; i < flash_get_ptn_count(); i++) 
			{
				partition = flash_get_ptn(i);
				if(partition)
				{
					printf("%8d %12s %#x  %#x\n",partition->flags&FLASH_PARTITION_MASK,partition->name,partition->start,partition->length);
				}
			}
			printf("----------------------------------\n");
		}
		else {
			//fall back system variables
			board_getvar(cmdbuf + 7, status + 4);
		}
		
		fb_tx_status(status);
		return;
		
	}
	if(memcmp(cmdbuf, "oem", 3) == 0) {
		char *oem_command=cmdbuf+4;
		printf("oemcommand:%s:\n", oem_command);
		if(!strcmp(oem_command, "exit")) {			
			running=0;
			fb_tx_status("OKAY");
			return;
		}
		if(board_oem(oem_command)<0)
			fb_tx_status("FAILcommand execute with failure");
		else
			fb_tx_status("OKAY");
		return;		
	}
	if(memcmp(cmdbuf, "reboot", 6) == 0) {
		fb_tx_status("OKAY");
		mdelay(100);
		if(memcmp(cmdbuf + 6, "-bootloader", 11) == 0) {			
			//hardcoding 
			printf("reboot into bootloader");
			board_reboot("bootloader");
		}
		else
			board_reboot(NULL);
		return;
	}

	fb_tx_status("FAILinvalid command");
}

static int init_partitions(char *argv)
{

	return 0;
}

static int do_fastboot(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	extern int video_console_refresh(void);
    char *serial_num=getenv("serial_number");
	
	rx_length = 0;
	if(init_partitions(argv[1]))
		return 1;

#ifdef CONFIG_CFB_CONSOLE
	//switch console to lcd 	
	video_console_refresh();
	console_assign(stdout, "vga");
#endif

	printf("Welcome to use fastboot loader\n");
    printf("\nUSB FastBoot:  V%s\n", FASTBOOT_VERSION);
    printf("Build Date:    "U_BOOT_DATE", "U_BOOT_TIME"\n\n");
    printf("Serial Number: %s\n\n", serial_num?serial_num:"unknown");

	fb_init(cmd_handler);
	fb_set_buf(fb_mem);
	
	running=1;
	while(running)
	{
		fb_run();
		update_progress();
		if(ctrlc())
			break;
	}
	
	printf("fastboot halt\n");
	fb_halt();
	return 0;
}

U_BOOT_CMD(
		fb,	CONFIG_SYS_MAXARGS,	0,	do_fastboot,
		"android fastboot client application",
		"[mmc[num]]\n    - start fastboot process"
	  );
