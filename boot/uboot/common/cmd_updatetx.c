/*
 * (C) Copyright 2010 TANXUN
 *
 * Written by: Ellie Cao
 *
 * Automaticallly update of firmware images through tftp.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 */

#include <common.h>
#include <command.h>
#include <mmc.h>
#include <fastboot/flash.h>

#define MAX_IMAGE_FILENAME_LEN 30
#define MAX_POSTFIX_LEN 10
#define AUTOPG_BUFFER_LEN 50

struct PGIMG
{
    char *filename;
    u8 nocontent;
};

#define UBOOT_FILENAME "u-boot.bin"
#define PRIMARY_GPT_FILENAME "primary_gpt"
#define SECOND_GPT_FILENAME "secondary_gpt"
#define ZIMAGE_FILENAME "zImage"
#define SYSTEM_IMAGE_FILENAME "system.img"
#define USERDATA_IMAGE_FILENAME "userdata.img"
#define CUSTOM_IMAGE_FILENAME "custom.img"
#define SPLASH_IMAGE_FILENAME "splash.img"
#define ZIMAGE_RECOVERY_FILENAME "zImage_recovery"
#define CACHE_IMAGE_FILENAME   "cache.img"
#define STORAGE_IMAGE_FILENAME   "storage.img"

static struct PGIMG imgArray[]=
{
    {UBOOT_FILENAME,0},
    {PRIMARY_GPT_FILENAME,0},
    {SECOND_GPT_FILENAME,0},
    {ZIMAGE_FILENAME,0},
    {SYSTEM_IMAGE_FILENAME,0},
    {USERDATA_IMAGE_FILENAME,1},
    {SPLASH_IMAGE_FILENAME,0},
    {CUSTOM_IMAGE_FILENAME,0},
    {CACHE_IMAGE_FILENAME,1},
    {ZIMAGE_RECOVERY_FILENAME,0},
    {STORAGE_IMAGE_FILENAME,1},
};

static u8 autopgAllowed=1;

#define MEMADDR 0x1100000

static int dl_pg_img(char *origfilename, char *postfix,u8 nocontent)
{
	char cmdbuffer[AUTOPG_BUFFER_LEN];
	char filename[MAX_IMAGE_FILENAME_LEN];
	int ret;
	struct mmc *mmc;
	u8 curpart;
	u32 address, size=0,partition;

	//unify interface to fastboot flash
	if(!strcmp(origfilename,UBOOT_FILENAME))
	{
		ptentry* entry = flash_find_ptn("boot");
		if(entry)
		{
			address = entry->start;
			size	 = entry->length;
			partition = entry->flags&FLASH_PARTITION_MASK;
		}
	}
	else if(!strcmp(origfilename,PRIMARY_GPT_FILENAME))
	{
		ptentry* entry = flash_find_ptn("pgpt");
		if(entry)
		{
			address = entry->start;
			size	 = entry->length;
			partition = entry->flags&FLASH_PARTITION_MASK;
		}
	}
	else if(!strcmp(origfilename,SECOND_GPT_FILENAME))
	{
		ptentry* entry = flash_find_ptn("agpt");
		if(entry)
		{
			address = entry->start;
			size	 = entry->length;
			partition = entry->flags&FLASH_PARTITION_MASK;
		}
	}
	else if(!strcmp(origfilename,ZIMAGE_FILENAME))
	{
		ptentry* entry = flash_find_ptn("kernel");
		if(entry)
		{
			address = entry->start;
			size	 = entry->length;
			partition = entry->flags&FLASH_PARTITION_MASK;
		}
	}
	else if(!strcmp(origfilename,SYSTEM_IMAGE_FILENAME))
	{
		ptentry* entry = flash_find_ptn("system");
		if(entry)
		{
			address = entry->start;
			size	 = entry->length;
			partition = entry->flags&FLASH_PARTITION_MASK;
		}
	}
	else if(!strcmp(origfilename,USERDATA_IMAGE_FILENAME))
	{
		ptentry* entry = flash_find_ptn("userdata");
		if(entry)
		{
			address = entry->start;
			size	 = entry->length;
			partition = entry->flags&FLASH_PARTITION_MASK;
		}
	}
	else if(!strcmp(origfilename,CUSTOM_IMAGE_FILENAME))
	{
		ptentry* entry = flash_find_ptn("custom");
		if(entry)
		{
			address = entry->start;
			size	 = entry->length;
			partition = entry->flags&FLASH_PARTITION_MASK;
		}
	}
	else if(!strcmp(origfilename,SPLASH_IMAGE_FILENAME))
	{
		ptentry* entry = flash_find_ptn("splash");
		if(entry)
		{
			address = entry->start;
			size	 = entry->length;
			partition = entry->flags&FLASH_PARTITION_MASK;
		}
	}
	else if(!strcmp(origfilename,ZIMAGE_RECOVERY_FILENAME))
	{
		ptentry* entry = flash_find_ptn("recovery");
		if(entry)
		{
			address = entry->start;
			size	 = entry->length;
			partition = entry->flags&FLASH_PARTITION_MASK;
		}
	}
	else if(!strcmp(origfilename,CACHE_IMAGE_FILENAME))
	{
		ptentry* entry = flash_find_ptn("cache");
		if(entry)
		{
			address = entry->start;
			size	 = entry->length;
			partition = entry->flags&FLASH_PARTITION_MASK;
		}
	}
	else if(!strcmp(origfilename,STORAGE_IMAGE_FILENAME))
	{
		ptentry* entry = flash_find_ptn("storage");
		if(entry)
		{
			address = entry->start;
			size	 = entry->length;
			partition = entry->flags&FLASH_PARTITION_MASK;
		}
	}

	if(size==0)
	{
		printf("partition not found\n");
		return -1;
	}
	
	// check active partition
	// we assume dev 0 is boot device
	run_command("mmc sw_dev 0",0);
	
	mmc = find_mmc_device(0);
	if (!mmc) 
	{
		printf("mmc not found\n");
		return -1;
	} 
	if (!mmc->block_dev.initialized && mmc_init(mmc)) 
	{
		printf("mmc initializing failed\n");
		return -1;
	} 
	mmc->block_dev.initialized = 1;


	if (IS_SD(mmc))
	{
		printf("mmc is SD\n");
		return -1;
	} 
	curpart= mmc->ext_csd_part_config & 0x7;
	if(curpart!=partition)
	{
		sprintf(cmdbuffer,"mmc sw_part %d",partition);
		ret=run_command(cmdbuffer,0);
		if(ret <0)
		{
			printf("Failed to switch partition to %d, ret %d\n",partition,ret);
			return -1;
		}
	}
	
	// get file name
	if(strcmp(origfilename,PRIMARY_GPT_FILENAME)&&strcmp(origfilename,SECOND_GPT_FILENAME))
		strcpy(filename,origfilename);
	else		
		sprintf(filename, "%s_%dg", origfilename, (int)(mmc->capacity / 1024 / 1024 /1000)+1);

	if(postfix)
	{
		strcat(filename,postfix);
	}

	// download file content
	if(nocontent)
	{
		printf("clearing memory starting from 0x1100000 for 0x80000 bytes...\n");
		sprintf(cmdbuffer,"mw.b 0x1100000 0 0x80000");
		run_command(cmdbuffer,0);
		if(size>0x400)
			size=0x400;
	}
	else
	{
		printf("Downloading  %s...\n",filename);
		sprintf(cmdbuffer,"tftpboot %s",filename);
		run_command(cmdbuffer,0);
	}

	// program image to flash
	sprintf(cmdbuffer,"mmc write 0x%x 0x%x 0x1100000",address/512,size/512);
	if(run_command(cmdbuffer,0)== -1)
	{
		printf("Failed to program image to flash\n");
		return -1;
	}
	printf("programming %s ok.\n",filename);
	return 0;
}

/************************************************************************
 * Convert an integer string to its equivalent value.
 */
static u8
atoi(uchar * string)
{
	u8 res = 0;
	while (*string >= '0' && *string <= '9') {
		res *= 10;
		res += *string - '0';
		string++;
	}

	return res;
}

static int do_autoprogram (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int arg_idx=1;
	int file_idx=-1;
	char *postfix=NULL;


	while(argc>1) // parameters are provided
	{
		if((strcmp(argv[arg_idx],"-p")==0)&&(argc>=3))   // postfix is provided
		{
			arg_idx++;
			if(strlen(argv[arg_idx])>MAX_POSTFIX_LEN)  // postfix is too long
			{
				printf("Failed: Length of postfix exceeds the limit of %d characters\n",MAX_POSTFIX_LEN-1);
				return -1;
			}
			postfix=argv[arg_idx];
			arg_idx++;
			argc-=2;
		}
		else if((strcmp(argv[arg_idx],"-i")==0)&&(argc>=3))   // file index is provided
		{
			arg_idx++;
			file_idx=atoi(argv[arg_idx]);
			if(file_idx<0||file_idx>=(sizeof(imgArray)/sizeof(struct PGIMG)))    // invalid file index
			{
				printf("Failed: invalid file selected\n");
				return -1;
			}
			arg_idx++;
			argc-=2;
		}
		else
		{
			printf("Usage:\n%s\n", cmdtp->help);
			return -1;
		}
	}

	// a single file is specified
	if(file_idx>=0)
	{
		if(dl_pg_img(imgArray[file_idx].filename,postfix,imgArray[file_idx].nocontent)<0)
		{
			return -1;
		}
	}
	else
	{
		for(file_idx=1;file_idx<=8;file_idx++)
		{
			if(dl_pg_img(imgArray[file_idx].filename,postfix,imgArray[file_idx].nocontent)<0)
			{
				return -1;
			}
		}
	}

	printf("Auto program OK!\n");
	return 0;
}

U_BOOT_CMD(
	autopg,	6,  1,	do_autoprogram,
	"autopg\n",
	"[-p][postfix of image file names]\n"
	"[-i][file to program]\n"
	"    [0:u-boot.bin]\n"
	"    [1:primary_gpt_*g] (* equals to 2,4,8 or 16,depending on flash size)\n"
	"    [2:secondary_gpt_*g] (* equals to 2,4,8 or 16,depending on flash size)\n"
	"    [3:zImage]\n"
	"    [4:system.img]\n"
	"    [5:userdata.img]\n"
	"    [6:splash.img]\n"
	"    [7:custom.img]\n"
	"    [8:cache.img]\n"
	"    [9:zImage_recovery]\n"
	"    [10:storage.img]\n"
	"    [default:file 1~8 will be programmed]\n");


