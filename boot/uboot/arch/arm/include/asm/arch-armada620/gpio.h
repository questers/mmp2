/*
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

/*
 * Based on (mostly copied from) plat-pxa based Linux 2.6 kernel driver.
 *
 * Lei Wen <leiwen@marvell.com>
 */

#ifndef __ARMADA6XX_GPIO_H
#define __ARMADA6XX_GPIO_H

#define BANK_OFF(n)     (((n) < 3) ? (n) << 2 : 0x100 + (((n) - 3) << 2))
#define GPIO_REG(x)     (*((volatile u32 *)(CONFIG_GPIO_REGBASE + (x))))

#define NR_BUILTIN_GPIO (128)

#define GPIO_bit(gpio)  (1 << ((gpio) & 0x1f))

#define GPLR(x)         GPIO_REG(BANK_OFF((x) >> 5) + 0x00)
#define GPDR(x)         GPIO_REG(BANK_OFF((x) >> 5) + 0x0c)
#define GPSR(x)         GPIO_REG(BANK_OFF((x) >> 5) + 0x18)
#define GPCR(x)         GPIO_REG(BANK_OFF((x) >> 5) + 0x24)

int gpio_get_value(unsigned gpio);
void gpio_set_value(unsigned gpio, int value);
#endif
