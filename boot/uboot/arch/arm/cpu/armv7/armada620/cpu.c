/*
 * (C) Copyright 2010
 * Marvell Semiconductor <www.marvell.com>
 * Kiran Vedere <kvedere@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */
#include <asm/io.h>

#define CPUID_ID	        0

#define __stringify_1(x)        #x
#define __stringify(x)          __stringify_1(x)

#define read_cpuid(reg)							\
	({								\
		unsigned int __val;					\
		asm("mrc	p15, 0, %0, c0, c0, " __stringify(reg)	\
		    : "=r" (__val)					\
		    :							\
		    : "cc");						\
		__val;							\
	})

#ifdef CONFIG_ARMADA620
static int __cpu_is_armada620(id)
{
		unsigned int _id = ((id) >> 8) & 0xff;
		return (_id == 0xb7 || _id == 0xc0  || _id == 0x58);
}
#else
static int __cpu_is_armada620(id)
{
		return 0;
}
#endif

int cpu_is_armada620(void)
{
	unsigned int id = read_cpuid(CPUID_ID);
	return __cpu_is_armada620(id);
}

int cpu_is_armada620_z0(void)
{
	unsigned int chip_id = __raw_readl(0xd4282c00);
	unsigned int id = read_cpuid(CPUID_ID);
	return (__cpu_is_armada620(id) && ((chip_id & 0x00ff0000) == 0x00f00000));
}

int cpu_is_armada620_z1(void)
{
	unsigned int chip_id = __raw_readl(0xd4282c00);
	unsigned int id = read_cpuid(CPUID_ID);
	return (__cpu_is_armada620(id) && ((chip_id & 0x00ff0000) == 0x00e00000));
}

int cpu_is_armada620_a0(void)
{
	 unsigned int chip_id = __raw_readl(0xd4282c00);
	 unsigned int id = read_cpuid(CPUID_ID);
	 int ret = 0;

	 if (__cpu_is_armada620(id) && ((chip_id & 0x00ff0000) == 0x00a00000)) {
			unsigned int soc_stepping = __raw_readl(0xffe00030);
			if (soc_stepping == 0x4130)
				ret = 1;
	 }

	 return ret;
}

int cpu_is_armada620_a1(void)
{
	unsigned int chip_id = __raw_readl(0xd4282c00);
	unsigned int id = read_cpuid(CPUID_ID);
	int ret = 0;

	if (__cpu_is_armada620(id) && ((chip_id & 0x00ff0000) == 0x00a00000)) {
		unsigned int soc_stepping = __raw_readl(0xffe00030);
		if (soc_stepping == 0x4131)
			ret = 1;
	}
	return ret;
}

int dram_init(void)
{
	return 0;
}

/*
 * Reset the cpu by setting up the watchdog timer and let him time out
 */
void reset_cpu(ulong ignored)
{
	/* TO DO */
}

int arch_cpu_init(void)
{
	volatile unsigned int reg;
	/* 0xd4282c94 - CIU_MOLT_PERI_CFG_CTL1 */
	__asm__ __volatile__ ("				\n\
	@ Configure private address to 0xE000_0000	\n\
	ldr	r0, =0xd4282c94				\n\
	ldr	r1, =0xe0000000				\n\
	str	r1, [r0]				\n\
							\n\
	@ Enable					\n\
	ldr	r0, =0xd4282c9c				\n\
	ldr	r1, =0xffffe001				\n\
	str	r1, [r0]				\n\
							\n\
	@ PJ4 MP0 timer clock enable			\n\
	ldr	r0, =0xD4282988				\n\
	ldr	r1, [r0]				\n\
	orr	r1, r1, #0x10				\n\
	orr	r1, r1, #0x18 @ bit4=clken, bit3=resetn	\n\
	str	r1, [r0]				\n\
							\n\
	@ Treat WFE as a NOP				\n\
	mrc	p15, 1, r0, c15, c1, 1			\n\
	orr	r0, r0, #(1<<13)			\n\
	mcr	p15, 1, r0, c15, c1, 1			\n\
	" : "=r" (reg));

	return 0;
}
