/*
 * (C) Copyright 2010
 * Marvell Semiconductor <www.marvell.com>
 * Lei Wen <leiwen@marvell.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA
 */

#include <common.h>
#include <asm/io.h>
DECLARE_GLOBAL_DATA_PTR;

#define CHIP_ID		(0xd4282c00)
unsigned int read_cpuid_id(void)
{
	unsigned int __val;
	asm("mrc       p15, 0, %0, c0, c0, 0"
			: "=r" (__val)
			:
			: "cc");
	return __val;
}
#define __cpu_is_pxa910(id, cid)	\
	({ unsigned int _id = ((id) >> 8) & 0xff; \
	 unsigned int _cid = (cid) & 0xfff; \
	 (_id == 0x84 || _id == 0x80) && (_cid == 0x910 || _cid == 0x920); })
#define cpu_is_pxa910()		({ __cpu_is_pxa910(read_cpuid_id(), __raw_readl(CHIP_ID)); })
int cpu_is_pxa910_Ax(void)
{
	unsigned int revision = (__raw_readl(CHIP_ID) >> 16) & 0xff;

	if (cpu_is_pxa910()) {
		if ((revision >= 0xf1) || (revision == 0xa0)
			|| (revision == 0xa1))
			return 1;
	}
	return 0;
}

int dram_init(void)
{
	int area1, area2;
	area1 = 1 << (((*(volatile unsigned int *)0xb0000100 >> 16) & 0xf) - 4);
	area2 = 1 << (((*(volatile unsigned int *)0xb0000110 >> 16) & 0xf) - 4);
	gd->ram_size = (area1 + area2) * 1024 * 1024;
	return 0;
}

#ifdef CONFIG_ARCH_CPU_INIT
int arch_cpu_init(void)
{
	/*
	 * set default cpu configuration in MOHAWK_CPU_CONF:
	 * - set SEL_MRVL_ID to select Marvell ID in cp15 c0
	 * - set l2c_write_alloc_ap[0] to force NOT WA
	 * - set AFIFO_MERGE_DIS to disable write buffer merge
	 * - set CG_BPASS0 to 1 to disable global auto clock gating
	 */
	*(volatile u32 *)0xD4282c08 = *(volatile u32 *)0xD4282c08 | (1<<8) | (1<<22) | (1<<6) | (1<<5);

	/* ALL MCB go to slowQ */
	*(volatile u32 *)0xD4282C8C = 0; /* mcb_config2_reg */

	/* FABRIC2 S7 posted write response enable */
	*(volatile u32 *)0xD4282C8C = *(volatile u32 *)0xD4282C8C | (1<<10);

	/* Seagull/PJ1/LCD go to slowQ */
	*(volatile u32 *)0xD4282C04 = *(volatile u32 *)0xD4282C04 & ~(1<<19); /* seagull_cpu_conf */
	*(volatile u32 *)0xD4282C08 = *(volatile u32 *)0xD4282C08 & ~(1<<19); /* mohawk_cpu_conf */
	*(volatile u32 *)0xD4282C5C = *(volatile u32 *)0xD4282C5C & ~(1<<11); /* axi2mc_ctrl */

	/* weighed round robin for MC slowQ */
	*(volatile u32 *)0xB0000280 = 0x02020102; /* Seagull/PJ1/LCD has higher priority than MCB */

	/* Disable page_block_access in AXI fabric2 */
	*(volatile u32 *)0xD4200260 = *(volatile u32 *)0xD4200260 | (1<<3);

	/*config L2 cache*/
	*(volatile unsigned int *)0xd4282c14 =
		(7<<0) + (3<<3) + (7<<5) + (3<<8) + (7<<10) +
		(7<<13) + (1<<16) + (2<<18) + (7<<21) +
		(3<<24) + (1<<26) + (2<<28) + (0<<31);
	if (cpu_is_pxa910_Ax())
		*(volatile unsigned int *)0xd4282c18 =
			(7<<0) + (3<<3) + (7<<5) + (3<<8) + (6<<10) +
			(6<<13) + (1<<16) + (2<<18) + (7<<21) +
			(3<<24) + (1<<26) + (2<<28) + (0<<31);
	else
		*(volatile unsigned int *)0xd4282c18 =
			(7<<0) + (3<<3) + (7<<5) + (3<<8) + (7<<10) +
			(7<<13) + (1<<16) + (2<<18) + (7<<21) +
			(3<<24) + (1<<26) + (2<<28) + (0<<31);
	return 0;
}
#endif /* CONFIG_ARCH_CPU_INIT */
