/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  publishhed by the Free Software Foundation.
 */
#include <mfp.h>
#include <common.h>
#include <pxa168fb.h>
#include <asm/io.h>
#include "tc35876x.h"

/* dsi phy timing */
static struct dsi_phy phy = {
	.hs_prep_constant       = 60,    /* Unit: ns. */
	.hs_prep_ui             = 5,
	.hs_zero_constant       = 85,
	.hs_zero_ui             = 5,
	.hs_trail_constant      = 0,
	.hs_trail_ui            = 64,
	.hs_exit_constant       = 100,
	.hs_exit_ui             = 0,
	.ck_zero_constant       = 300,
	.ck_zero_ui             = 0,
	.ck_trail_constant      = 60,
	.ck_trail_ui            = 0,
	.req_ready              = 0x3c,
};

#define dsi_ex_pixel_cnt                0
#define dsi_hex_en                      0
/* (Unit: Mhz) */
#define dsi_hsclk                       260    //  sclk_src/1000000
#define dsi_lpclk                       3

#define to_dsi_bcnt(timing, bpp)        (((timing) * (bpp)) >> 3)

static unsigned int dsi_lane[5] = {0, 0x1, 0x3, 0x7, 0xf};


#define GPIO83_LCD_RST		MFP_CFG_DRV_LPM_DRV_LOW(GPIO83, AF0, FAST)
#define GPIO89_LCD_5V_BIAS 	MFP_CFG_DRV_LPM_DRV_LOW(GPIO89, AF0, FAST)
extern int pxa168fb_init(struct pxa168fb_mach_info *mi);
static unsigned long brownstone_pin_config[] = {
	/* LCD */
	GPIO83_LCD_RST,
	GPIO89_LCD_5V_BIAS,
};

void dsi_cclk_set(struct pxa168fb_info *fbi, int en)
{
	struct pxa168fb_mach_info *mi = fbi->mi;
	struct dsi_regs *dsi = (struct dsi_regs *)mi->dsi->regs;

	if (en) {
		writel(0x1, &dsi->phy_ctrl1);
	} else {
		writel(0x0, &dsi->phy_ctrl1);
	}
	udelay(100000);
}

void dsi_set_dphy(struct pxa168fb_info *fbi)
{
	struct pxa168fb_mach_info *mi = fbi->mi;
	struct dsi_regs *dsi = (struct dsi_regs *)mi->dsi->regs;
	u32 ui, lpx_clk, lpx_time, ta_get, ta_go, wakeup, reg;
	u32 hs_prep, hs_zero, hs_trail, hs_exit, ck_zero, ck_trail, ck_exit;

	// in kernel: ui = 1000/dsi_hsclk + 1;
	ui = 1000/dsi_hsclk + 1;  
	lpx_clk = (DSI_ESC_CLK / dsi_lpclk / 2) - 1;
	lpx_time = (lpx_clk + 1) * DSI_ESC_CLK_T;
	/* Below is for NT35451 */
	ta_get = ((lpx_time * 10 + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;
	ta_go  = ((lpx_time * 4 + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;
	wakeup = 0xfff0;

	hs_prep = phy.hs_prep_constant + phy.hs_prep_ui * ui;
	hs_prep = ((hs_prep + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;

	/* Our hardware added 3-byte clk automatically.
	 * 3-byte 3 * 8 * ui.
	 */
	hs_zero = phy.hs_zero_constant + phy.hs_zero_ui * ui;
	if(hs_zero > (24 * ui))
		hs_zero -= (24 * ui);
	else
		hs_zero = DSI_ESC_CLK_T;

	if(hs_zero > (DSI_ESC_CLK_T * 2))
		hs_zero = ((hs_zero + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;
	else
		hs_zero = 1;

	hs_trail = phy.hs_trail_constant + phy.hs_trail_ui * ui;
	hs_trail = ((hs_trail + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;

	hs_exit = phy.hs_exit_constant + phy.hs_exit_ui * ui;
	hs_exit = ((hs_exit + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;

	ck_zero = phy.ck_zero_constant + phy.ck_zero_ui * ui;
	ck_zero = ((ck_zero + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;

	ck_trail = phy.ck_trail_constant + phy.ck_trail_ui * ui;
	ck_trail = ((ck_trail + (DSI_ESC_CLK_T >> 1)) / DSI_ESC_CLK_T) - 1;
						
	ck_exit = hs_exit;

	/* bandgap ref enable */
	reg = readl(&dsi->phy_rcomp0);
	reg |= (1<<9);
	//reg = 0x0f800200;
	writel(reg, &dsi->phy_rcomp0);
	/* timing_0 */
	reg = (hs_exit << DSI_PHY_TIME_0_CFG_CSR_TIME_HS_EXIT_SHIFT)
		| (hs_trail << DSI_PHY_TIME_0_CFG_CSR_TIME_HS_TRAIL_SHIFT)
		| (hs_zero << DSI_PHY_TIME_0_CDG_CSR_TIME_HS_ZERO_SHIFT)
		| (hs_prep);
	
	writel(reg, &dsi->phy_timing0);
	reg = (ta_get << DSI_PHY_TIME_1_CFG_CSR_TIME_TA_GET_SHIFT)
		| (ta_go << DSI_PHY_TIME_1_CFG_CSR_TIME_TA_GO_SHIFT)
		| wakeup;
	writel(reg, &dsi->phy_timing1);
	reg = (ck_exit << DSI_PHY_TIME_2_CFG_CSR_TIME_CK_EXIT_SHIFT)
		| (ck_trail << DSI_PHY_TIME_2_CFG_CSR_TIME_CK_TRAIL_SHIFT)
		| (ck_zero << DSI_PHY_TIME_2_CFG_CSR_TIME_CK_ZERO_SHIFT)
		| lpx_clk;
	writel(reg, &dsi->phy_timing2);

	reg = (lpx_clk << DSI_PHY_TIME_3_CFG_CSR_TIME_LPX_SHIFT) | \
	      phy.req_ready;
	writel(reg, &dsi->phy_timing3);
	/* calculated timing on brownstone:
	 * DSI_PHY_TIME_0 0x06080204
	 * DSI_PHY_TIME_1 0x6d2bfff0
	 * DSI_PHY_TIME_2 0x603130a
	 * DSI_PHY_TIME_3 0xa3c
	 */
}

void dsi_reset(struct pxa168fb_info *fbi, int hold)
{
	struct pxa168fb_mach_info *mi = fbi->mi;
	struct dsi_regs *dsi = (struct dsi_regs *)mi->dsi->regs;
	volatile unsigned int reg;

	writel(0x0, &dsi->ctrl0);
	reg = readl(&dsi->ctrl0);
	reg |= DSI_CTRL_0_CFG_SOFT_RST | DSI_CTRL_0_CFG_SOFT_RST_REG;

	if (!hold) {
		writel(reg, &dsi->ctrl0);
		reg &= ~(DSI_CTRL_0_CFG_SOFT_RST | DSI_CTRL_0_CFG_SOFT_RST_REG);
		udelay(1000);
	}
	writel(reg, &dsi->ctrl0);
}

void dsi_set_controller(struct pxa168fb_info *fbi)
{
	struct fb_var_screeninfo *var = fbi->var;
	struct pxa168fb_mach_info *mi = fbi->mi;
	struct dsi_regs *dsi = (struct dsi_regs *)mi->dsi->regs;
	struct dsi_lcd_regs *dsi_lcd = &dsi->lcd1;
	struct dsi_info *di = mi->dsi;
	unsigned hsync_b, hbp_b, hact_b, hex_b, hfp_b, httl_b;
	unsigned hsync, hbp, hact, hfp, httl, h_total, v_total;
	unsigned hsa_wc, hbp_wc, hact_wc, hex_wc, hfp_wc, hlp_wc;
	int bpp = di->bpp, hss_bcnt = 4, hse_bct = 4, lgp_over_head = 6, reg;

	if (di->id & 2)
		dsi_lcd = &dsi->lcd2;

	h_total = var->xres + var->left_margin + var->right_margin + var->hsync_len;
	v_total = var->yres + var->upper_margin + var->lower_margin + var->vsync_len;

	hact_b = to_dsi_bcnt(var->xres, bpp);
	hfp_b = to_dsi_bcnt(var->right_margin, bpp);
	hbp_b = to_dsi_bcnt(var->left_margin, bpp);
	hsync_b = to_dsi_bcnt(var->hsync_len, bpp);
	hex_b = to_dsi_bcnt(dsi_ex_pixel_cnt, bpp);
	httl_b = hact_b + hsync_b + hfp_b + hbp_b + hex_b;

	hact = hact_b / di->lanes;
	hfp = hfp_b / di->lanes;
	hbp = hbp_b / di->lanes;
	hsync = hsync_b / di->lanes;
	httl = hact + hfp + hbp + hsync;
	/* word count in the unit of byte */
	hsa_wc = (di->burst_mode == DSI_BURST_MODE_SYNC_PULSE) ? \
		(hsync_b - hss_bcnt - lgp_over_head) : 0;

	/* Hse is with backporch */
	hbp_wc = (di->burst_mode == DSI_BURST_MODE_SYNC_PULSE) ? \
		(hbp_b - hse_bct - lgp_over_head) \
		: (hsync_b + hbp_b - hss_bcnt - lgp_over_head);

	hfp_wc = ((di->burst_mode == DSI_BURST_MODE_BURST) && (dsi_hex_en == 0)) ? \
		(hfp_b + hex_b - lgp_over_head - lgp_over_head) : \
		(hfp_b - lgp_over_head - lgp_over_head);

	hact_wc =  ((var->xres) * bpp) >> 3;

	/* disable Hex currently */
	hex_wc = 0;

	/*  There is no hlp with active data segment.  */
	hlp_wc = (di->burst_mode == DSI_BURST_MODE_SYNC_PULSE) ? \
		(httl_b - hsync_b - hse_bct - lgp_over_head) : \
		(httl_b - hss_bcnt - lgp_over_head);

	/* FIXME - need to double check the (*3) is bytes_per_pixel from input data or output to panel */
	/* dsi_lane_enable - Set according to specified DSI lane count */
	writel(dsi_lane[di->lanes] << DSI_PHY_CTRL_2_CFG_CSR_LANE_EN_SHIFT, &dsi->phy_ctrl2);
	writel(dsi_lane[di->lanes] << DSI_CPU_CMD_1_CFG_TXLP_LPDT_SHIFT, &dsi->cmd1);
	if (mi->phy_type == DSI)
		mi->dsi_set(fbi);

	/* SET UP LCD1 TIMING REGISTERS FOR DSI BUS */
	/* NOTE: Some register values were obtained by trial and error */
	writel((hact << 16) | httl, &dsi_lcd->timing0);
	writel((hsync << 16) | hbp, &dsi_lcd->timing1);
	/*
	 * For now the active size is set really low (we'll use 10) to allow
	 * the hardware to attain V Sync. Once the DSI bus is up and running,
	 * the final value will be put in place for the active size (this is
	 * done below). In a later stepping of the processor this workaround
	 * will not be required.
	 */
	writel(((var->yres)<<16) | (v_total), &dsi_lcd->timing2);

	writel(((var->vsync_len) << 16) | (var->upper_margin), &dsi_lcd->timing3);
	/* SET UP LCD1 WORD COUNT REGISTERS FOR DSI BUS */
	/* Set up for word(byte) count register 0 */
	writel((hbp_wc << 16) | hsa_wc, &dsi_lcd->wc0);
	writel((hfp_wc << 16) | hact_wc, &dsi_lcd->wc1);
	writel((hex_wc << 16) | hlp_wc, &dsi_lcd->wc2);
	/* calculated value on brownstone:
	 * WC0: 0x1a0000
	 * WC1: 0x1500f00
	 * WC2: 0x1076 */

	/* Configure LCD control register 1 FOR DSI BUS */
	reg = ((di->rgb_mode << DSI_LCD2_CTRL_1_CFG_L1_RGB_TYPE_SHIFT)
		| (di->burst_mode << DSI_LCD1_CTRL_1_CFG_L1_BURST_MODE_SHIFT)
		| (di->lpm_line_en ? DSI_LCD1_CTRL_1_CFG_L1_LPM_LINE_EN : 0)
		| (di->lpm_frame_en ? DSI_LCD1_CTRL_1_CFG_L1_LPM_FRAME_EN : 0)
		| (di->last_line_turn ? DSI_LCD1_CTRL_1_CFG_L1_LAST_LINE_TURN :0)
		| (di->hex_slot_en ? 0 :0)   //disable Hex slot;
		| (di->all_slot_en ? 0 :0)   //disable all slots;
		| (di->hbp_en ? DSI_LCD1_CTRL_1_CFG_L1_HBP_PKT_EN :0)
		| (di->hact_en ? DSI_LCD1_CTRL_1_CFG_L1_HACT_PKT_EN :0)
		| (di->hfp_en ? DSI_LCD1_CTRL_1_CFG_L1_HFP_PKT_EN : 0)
		| (di->hex_en ? 0 : 0)      // Hex packet is disabled
		| (di->hlp_en ? DSI_LCD1_CTRL_1_CFG_L1_HLP_PKT_EN : 0));

	reg |= (di->burst_mode == DSI_BURST_MODE_SYNC_PULSE) ? \
		(((di->hsa_en) ? DSI_LCD1_CTRL_1_CFG_L1_HSA_PKT_EN :0)
		| (DSI_LCD1_CTRL_1_CFG_L1_HSE_PKT_EN ))  // Hse is always eabled;
		:
		(((di->hsa_en) ? 0 :0)   // Hsa packet is disabled;
		| ((di->hse_en) ? 0 :0));     // Hse packet is disabled;

	reg |=  DSI_LCD1_CTRL_1_CFG_L1_VSYNC_RST_EN;
	writel(reg, &dsi_lcd->ctrl1);
	/*Start the transfer of LCD data over the DSI bus*/
	/* DSI_CTRL_1 */
	reg = readl(&dsi->ctrl1);
	reg &= ~(DSI_CTRL_1_CFG_LCD2_VCH_NO_MASK | DSI_CTRL_1_CFG_LCD1_VCH_NO_MASK);
	reg |= 0x1 << ((di->id & 1) ? DSI_CTRL_1_CFG_LCD2_VCH_NO_SHIFT : DSI_CTRL_1_CFG_LCD1_VCH_NO_SHIFT);

	reg &= ~(DSI_CTRL_1_CFG_EOTP);
	if (di->eotp_en)
		reg |= DSI_CTRL_1_CFG_EOTP;	/* EOTP */

	writel(reg, &dsi->ctrl1);
	/* DSI_CTRL_0 */
	reg = DSI_CTRL_0_CFG_LCD1_SLV | DSI_CTRL_0_CFG_LCD1_TX_EN | DSI_CTRL_0_CFG_LCD1_EN;
	if (di->id & 2)
		reg = reg << 1;
	writel(reg, &dsi->ctrl0);
	udelay(100000);

	/* FIXME - second part of the workaround */
	writel(((var->yres)<<16) | (v_total), &dsi_lcd->timing2);
}

static int tc358765_reset(struct pxa168fb_info *fbi)
{
	int gpio = mfp_to_gpio(GPIO83_LCD_RST);

	gpio_set_output(gpio);
	gpio_set_value(gpio, 0);
	udelay(100000);

	gpio_set_output(gpio);
	gpio_set_value(gpio, 1);
	udelay(100000);

	return 0;
}

int tc358765_init(void)
{
	int gpio = mfp_to_gpio(GPIO89_LCD_5V_BIAS);
	int tmp;

	gpio_set_output(gpio);
	gpio_set_value(gpio, 1);
	udelay(1000);

	return 0;
}

#define TC358765_CHIPID_REG 	0x0580
#define TC358765_CHIPID		0x6500
#define EIO			5
static void tc358765_dump(void)
{
#if 0
	u32 val_32;

	tc35876x_read32(PPI_TX_RX_TA, &val_32);
	printf("tc35876x - PPI_TX_RX_TA = 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(PPI_LPTXTIMECNT, &val_32);
	printf("tc35876x - PPI_LPTXTIMECNT = 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(PPI_D0S_CLRSIPOCOUNT, &val_32);
	printf("tc35876x - PPI_D0S_CLRSIPOCOUNT = 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(PPI_D1S_CLRSIPOCOUNT, &val_32);
	printf("tc35876x - PPI_D1S_CLRSIPOCOUNT = 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(PPI_LANEENABLE, &val_32);
	printf("tc35876x - PPI_LANEENABLE = 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(DSI_LANEENABLE, &val_32);
	printf("tc35876x - DSI_LANEENABLE = 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(PPI_STARTPPI, &val_32);
	printf("tc35876x - PPI_STARTPPI = 0x%x\n", val_32);
	tc35876x_read32(DSI_STARTDSI, &val_32);
	printf("tc35876x - DSI_STARTDSI = 0x%x\n", val_32);

	udelay(10000);
	tc35876x_read32(VPCTRL, &val_32);
	printf("tc35876x - VPCTRL = 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(HTIM1, &val_32);
	printf("tc35876x - HTIM1 = 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(HTIM2, &val_32);
	printf("tc35876x - HTIM2 = 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(VTIM1, &val_32);
	printf("tc35876x - VTIM1 = 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(VTIM2, &val_32);
	printf("tc35876x - VTIM2 = 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(VFUEN, &val_32);
	printf("tc35876x - VFUEN = 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(LVCFG, &val_32);
	printf("tc35876x - LVCFG = 0x%x\n", val_32);
	udelay(10000);


	tc35876x_read32(DSI_INTSTAUS, &val_32);
	printf("!!tc35876x - DSI_INTSTAUS= 0x%x BEFORE\n", val_32);
	udelay(10000);
	tc35876x_write32(DSI_INTCLR, 0xFFFFFFFF);
	udelay(10000);
	tc35876x_read32(DSI_INTSTAUS, &val_32);
	printf("!!tc35876x - DSI_INTSTAUS= 0x%x AFTER\n", val_32);
	udelay(10000);

	tc35876x_read32(DSI_LANESTATUS0, &val_32);
	printf("tc35876x - DSI_LANESTATUS0= 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(DSIERRCNT, &val_32);
	printf("tc35876x - DSIERRCNT= 0x%x\n", val_32);
	udelay(10000);
	tc35876x_read32(SYSSTAT, &val_32);
	printf("tc35876x - SYSSTAT= 0x%x\n", val_32);
	udelay(10000);
#endif
}

static int dsi_set_tc358765(struct pxa168fb_info *fbi)
{
	/* struct fb_var_screeninfo *var = &(fbi->fb_info->var); */
	struct pxa168fb_mach_info *mi = fbi->mi;
	struct dsi_info *di = mi->dsi;
	u16 chip_id = 0;
	int status;

	i2c_init_twsi5(0);
	status = tc35876x_read16(TC358765_CHIPID_REG, &chip_id);
	/* REG 0x13C,DAT 0x000C000F */
	tc35876x_write32(PPI_TX_RX_TA, 0x00040004);
	/* REG 0x114,DAT 0x0000000A */
	tc35876x_write32(PPI_LPTXTIMECNT,0x00000004);
	/* get middle value of mim-max value
	 * 0-0x13 for 2lanes-rgb888, 0-0x26 for 4lanes-rgb888
	 * 0-0x21 for 2lanes-rgb565, 0-0x25 for 4lanes-rgb565
	 */
	if (di->lanes == 4)
		status = 0x13;
	else if (di->bpp == 24)
		status = 0xa;
	else
		status = 0x11;
	/* REG 0x164,DAT 0x00000005 */
	tc35876x_write32(PPI_D0S_CLRSIPOCOUNT, status);
	/* REG 0x168,DAT 0x00000005 */
	tc35876x_write32(PPI_D1S_CLRSIPOCOUNT, status);
	if (di->lanes == 4) {
		/* REG 0x16C,DAT 0x00000005 */
		tc35876x_write32(PPI_D2S_CLRSIPOCOUNT, status);
		/* REG 0x170,DAT 0x00000005 */
		tc35876x_write32(PPI_D3S_CLRSIPOCOUNT, status);
	}

	/* REG 0x134,DAT 0x00000007 */
	tc35876x_write32(PPI_LANEENABLE, (di->lanes == 4) ? 0x1f : 0x7);
	/* REG 0x210,DAT 0x00000007 */
	tc35876x_write32(DSI_LANEENABLE, (di->lanes == 4) ? 0x1f : 0x7);

	/* REG 0x104,DAT 0x00000001 */
	tc35876x_write32(PPI_STARTPPI, 0x0000001);
	/* REG 0x204,DAT 0x00000001 */
	tc35876x_write32(DSI_STARTDSI, 0x0000001);
	/* REG 0x450,DAT 0x00012020, VSDELAY = 8 pixels */
	tc35876x_write32(VPCTRL, 0x00800020);

	/* REG 0x454,DAT 0x00200008
	tc35876x_write32(HTIM1, ((var->left_margin + var->hsync_len) << 16)
			| var->hsync_len); */
	tc35876x_write32(HTIM1, 0x00200008);
	/* REG 0x45C,DAT 0x00040004
	tc35876x_write32(VTIM1, ((var->upper_margin + var->hsync_len) << 16)
			| var->vsync_len); */
	tc35876x_write32(VTIM1, 0x00040004);

	/* REG 0x49C,DAT 0x00000201 */
	tc35876x_write32(LVCFG, 0x00000001);

	/* dump register value */
	tc358765_dump();

	return 0;
}

static struct mutex ldo_mutex;
static int control_ldo(char *i, int flag, int vol)
{
	// 8925 have been start up before,so do nothing here
	return 0;
}

static struct dsi_info brownstone_dsi = {
	.id			= 1,
	.lanes			= 4,
	.bpp			= 16,
	.burst_mode		= DSI_BURST_MODE_BURST,
	.hbp_en			= 1,
	.hfp_en			= 1,
};

static int dsi_power_set(int on)
{
	if (on)
		control_ldo("v_ldo3", 1, 1200000);
	else
		control_ldo("v_ldo3", 0, 1200000);
	return 0;
}

static int v5p_enable_set(int on)
{
	int gpio = mfp_to_gpio(GPIO89_LCD_5V_BIAS);
	static int v5p_enabled;

        /* set panel 5V power */
	if (on) {
		if (v5p_enabled++ == 0) {
			gpio_set_output(gpio);
			gpio_set_value(gpio, 1);	
		}
	} else {
		if (--v5p_enabled == 0) {
			gpio_set_output(gpio);
			gpio_set_value(gpio, 0);	
		}
	}

	return 0;
}

static int brownstone_lcd_power(struct pxa168fb_info *fbi,
	unsigned int spi_gpio_cs, unsigned int spi_gpio_reset, int on)
{
	int lcd_rst_n = mfp_to_gpio(GPIO83_LCD_RST);
	/* panel reset */
	if (on) {
		/* enable AVDD12_DSI voltage */
		dsi_power_set(1);
		/* enable 5V power supply */
		v5p_enable_set(1);

		/* release reset */
		gpio_set_output(lcd_rst_n);
		gpio_set_value(lcd_rst_n, 1);
	} else {
		/* keep reset */
		gpio_set_output(lcd_rst_n);
		gpio_set_value(lcd_rst_n, 0);
		/* disable 5V power supply */
		v5p_enable_set(0);

		/* disable AVDD12_DSI voltage */
		dsi_power_set(0);
	}

	return 1;
}

static int brownstone_dsi_init(struct pxa168fb_info *fbi)
{
	struct pxa168fb_mach_info *mi = fbi->mi;
	/* reset DSI controller */
	dsi_reset(fbi, 1);
	udelay(1000);

	/* disable continuous clock */
	dsi_cclk_set(fbi, 0);

	/*  reset the bridge */
	if (mi->xcvr_reset)
		mi->xcvr_reset(fbi);

	/* dsi out of reset */
	dsi_reset(fbi, 0);

	/* set dphy */
	dsi_set_dphy(fbi);

	/* set dsi controller */
	dsi_set_controller(fbi);

	/* turn on DSI continuous clock */
	dsi_cclk_set(fbi, 1);

	/* set dsi to dpi conversion chip */
	if (mi->phy_type == DSI2DPI)
		mi->dsi2dpi_set(fbi);

	return 0;
}

static struct fb_videomode video_modes[] = {
	[0] = {
		.refresh	= 60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 2,
		.left_margin	= 12, 
		.right_margin	= 315,  
		.vsync_len	= 2,
		.upper_margin	= 10,
		.lower_margin	= 4,
		.sync		= FB_SYNC_VERT_HIGH_ACT | FB_SYNC_HOR_HIGH_ACT,
	},
};

static struct pxa168fb_mach_info mmp2_mipi_lcd_info = {
	.id			= "GFX Layer",
	.sclk_src		= 260000000,	/* 260MHz */
	.sclk_div		= 0x40000104,
	.num_modes		= ARRAY_SIZE(video_modes),
	.modes			= video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.burst_len		= 16,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the panel is hard connected with lcd panel path and dsi1 output
	 */
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.active			= 1,
	.enable_lcd             = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.max_fb_size		= FB_XRES * FB_YRES * 8 + 4096,
	.phy_type		= DSI2DPI,
	.phy_init		= brownstone_dsi_init,
	.dsi2dpi_set		= dsi_set_tc358765,
	.xcvr_reset		= tc358765_reset,
	.xcvr_init		= tc358765_init,
	.dsi			= &brownstone_dsi,
	.pxa168fb_lcd_power     = &brownstone_lcd_power,
};


static void rect_fill(unsigned short *addr, int left, int up, int right,
                      int down, int width, unsigned short color)
{
	int i, j;
	for (j = up; j < down; j++)
		for (i = left; i < right; i++)
			*(addr + j * width + i) = color;
}

static void twsi_clk_enable(unsigned int clk_reg)
{
	uint32_t clk_rst;

	clk_rst = __raw_readl(clk_reg);
	clk_rst &= ~(0x7 << 4);
	clk_rst |= (1 << 1);
	__raw_writel(clk_rst, clk_reg);
	udelay(1000);
	clk_rst |= (1 << 0);
	__raw_writel(clk_rst, clk_reg);
	udelay(1000);
	clk_rst &= ~(1 << 2);
	__raw_writel(clk_rst, clk_reg);

	return;
}

/*
 * first, we need to disable all the power for tc chip,
 * for the power cycle is caused by the hot-reset;
 * so make sure turn off them firstly,
 * and then turn on sequencely.
 */
#define GPIO99_LOW	MFP_CFG(GPIO99, AF0)
#define GPIO100_LOW	MFP_CFG(GPIO100, AF0)

static uint32_t gpio99_mfp_save;
static uint32_t gpio100_mfp_save;

static void turn_off_tc358765(void)
{
	int gpio;

	/* set GPIO99/GPIO100 output to low */
	gpio99_mfp_save  = readl(0xD401E000 + 0x1d4);
	gpio100_mfp_save = readl(0xD401E000 + 0x1d8);

	__raw_writel(0xd0e0, 0xD401E000 + 0x1d4);
	__raw_writel(0xd0e0, 0xD401E000 + 0x1d8);
	udelay(1000);

	gpio = mfp_to_gpio(GPIO99_LOW);
	gpio_set_output(gpio);
	gpio_set_value(gpio, 0);

	gpio = mfp_to_gpio(GPIO100_LOW);
	gpio_set_output(gpio);
	gpio_set_value(gpio, 0);
	udelay(1000);

	/* turn off v_3vlcd */
	gpio = mfp_to_gpio(GPIO83_LCD_RST);
	gpio_set_output(gpio);
	gpio_set_value(gpio, 0);
	udelay(1000);

	/* turn off ldo3 output */
	send_cmd_to_max8925(0x20, 0x1e);
	udelay(1000);

	/* turn off ldo17 output */
	send_cmd_to_max8925(0x14, 0x1e);

	return;
}

/*
 * enable power supply one by one:
 * ldo17 -> ldo3 -> v_3vlcd;
 */
static void turn_on_tc358765(void)
{
	int gpio;

	/* set ldo17 to 1.2v */
	send_cmd_to_max8925(0x16, 0x16);
	udelay(1000);

	/* set ldo3 to 1.2v */
	send_cmd_to_max8925(0x22, 0x16);
	udelay(1000);

	/* set SDV3 voltage to 2.8v*/
	send_cmd_to_max8925(0x0c, 0x29);
	udelay(1000);

	/* SD3 ctrl*/
	send_cmd_to_max8925(0x0a, 0x03);

	/* enable ldo2 output */
	send_cmd_to_max8925(0x1c, 0x03);

	/* enable ldo17 output */
	send_cmd_to_max8925(0x14, 0x1f);

	/* enable ldo3 output */
	send_cmd_to_max8925(0x20, 0x1f);
	udelay(1000);

	/* enalbe v_3vlcd */
	gpio = mfp_to_gpio(GPIO83_LCD_RST);
	gpio_set_value(gpio, 1);
	udelay(1000);

	/* restore twsi5 mfp config */
	__raw_writel(gpio99_mfp_save,  0xD401E000 + 0x1d4);
	__raw_writel(gpio100_mfp_save, 0xD401E000 + 0x1d8);
	udelay(1000);

	return;
}

static void lcd_hold_in_reset()
{
	int gpio = mfp_to_gpio(GPIO83_LCD_RST);

	gpio_set_output(gpio);
	gpio_set_value(gpio, 0);

}

static void turn_off_backlight()
{
	uint32_t clk_rst;
	int len, duty_ns = 1000000, period_ns = 2000000;
	unsigned long period_cycles, prescale, pv, dc;
	int reg;

	/* config GPIO53 as pwm3 */
	reg = readl(0xD401E000 + 0x128);
	__raw_writel(0x3085, 0xD401E000 + 0x128);
	udelay(1000);

	/* enable clock */
	clk_rst = __raw_readl(0xD4015000 + 0x0044);
	clk_rst &= ~(((0x7) & 0xf) << 4);
	clk_rst |= (1 << 1) | (((0x0) & 0xf) << 4);
	__raw_writel(clk_rst, 0xD4015000 + 0x0044);
	udelay(1000);
	clk_rst |= (1 << 0);
	__raw_writel(clk_rst, 0xD4015000 + 0x0044);
	udelay(1000);
	clk_rst &= ~(1 << 2);

	__raw_writel(clk_rst, 0xD4015000 + 0x0044);
	__raw_writel(0, 0xD401A800 + 0x00);
	__raw_writel(0, 0xD401A800 + 0x04);
}

void turn_on_backlight()
{
	int len, duty_ns = 1000000, period_ns = 2000000;
	unsigned long period_cycles, prescale, pv, dc;

	period_cycles = 52000;
	if (period_cycles < 1)
		period_cycles = 1;

	prescale = (period_cycles - 1) / 1024;
	pv = period_cycles / (prescale + 1) - 1;

	if (prescale > 63)
		return ;

	if (duty_ns == period_ns)
		dc = (1 << 10);
	else
		dc = (pv + 1) * duty_ns / period_ns;

	__raw_writel(prescale, 0xD401A800 + 0x00);
	__raw_writel(dc, 0xD401A800 + 0x04);
	__raw_writel(pv, 0xD401A800 + 0x08);

}

#if defined(CONFIG_PXA168_FB)
void *lcd_init(void)
{
	void *ret;

	/* enable twsi1 (for pmic max8925) */
	twsi_clk_enable(0xD4015000 + 0x0004);

	/* enable twsi5 (for tc chip) */
	twsi_clk_enable(0xD4015000 + 0x007c);

	turn_off_backlight();
	lcd_hold_in_reset();

	/* turn off tc chip */
	turn_off_tc358765();

	/* turn on tc chip */
	turn_on_tc358765();

	tc358765_init();

	/* init touch screen bridge chip (TC358765) */
	ret = (void *)pxa168fb_init(&mmp2_mipi_lcd_info);
			
	turn_on_backlight();
	return ret;	
}
#endif

void *video_hw_init(void)
{
	return NULL;
}

