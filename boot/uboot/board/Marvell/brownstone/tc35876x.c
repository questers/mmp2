#include "tc35876x.h"

typedef unsigned char			u8;
typedef unsigned short 			u16;
typedef unsigned int			u32;

int tc35876x_read16(u16 reg, u32 *pval)
{
	u8 address[2];

	address[0] = (reg >> 8) & 0xff;
	address[1] = reg & 0xff;
	i2c_write_twsi5(0x0f ,&address ,2, 1);
	i2c_read_twsi5(0x0f ,pval ,2, 1);

	return 0;
}

int tc35876x_read32(u16 reg, u32 *pval)
{
	int ret;
	int status;
	u8 address[2];


	address[0] = (reg >> 8) & 0xff;
	address[1] = reg & 0xff;

	i2c_write_twsi5(0x0f ,&address ,2, 1);
	i2c_read_twsi5(0x0f ,pval ,4, 1);
	status = 0;

	return status;
}

int tc35876x_write32(u16 reg, u32 val)
{
	u8 data[6];

	data[0] = (reg >> 8) & 0xff;
	data[1] = reg & 0xff;
	data[2]=  val & 0xff;
	data[3] = (val >> 8) & 0xff;
	data[4] = (val >> 16) & 0xff;
	data[5]=  (val >> 24) & 0xff;
	
	i2c_write_twsi5(0x0f ,&data ,6 ,1 );
	return 0;
}


