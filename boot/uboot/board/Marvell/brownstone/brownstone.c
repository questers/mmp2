/*
 * (C) Copyright 2005
 * Marvell Semiconductors Ltd. <www.marvell.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/common.h>
#include <mfp.h>
#include <usb/mmp_udc.h>
#include <asm/arch/mfp-pxa688.h>
#include <mmc.h>

DECLARE_GLOBAL_DATA_PTR;

static int wtm_read_profile(void);
static int wtm_read_stepping(void);
static void wtm_dump_info(void);

#ifdef CONFIG_TRUST_BOOT
unsigned int mrvl_mag;
unsigned int mrvl_validation_flag;
unsigned int mrvl_residue_flag;
unsigned int R_uboot = 0;
#endif

static mfp_cfg_t jasper_pin_config[] __initdata = {
	/*TWSI1*/
	TWSI1_SCL,
	TWSI1_SDA,

	/* UART3 */
	GPIO51_UART3_RXD,
	GPIO52_UART3_TXD,

	/* SMC */
	SM_nCS0_nCS0,
	SM_ADV_SM_ADV,
	SM_SCLK_SM_SCLK,
	SM_SCLK_SM_SCLK,
	SM_BE0_SM_BE0,
	SM_BE1_SM_BE1,

	/* DFI */
	GPIO168_DFI_D0,	
	GPIO167_DFI_D1,	
	GPIO166_DFI_D2,	
	GPIO165_DFI_D3,	
	GPIO107_DFI_D4,	
	GPIO106_DFI_D5,	
	GPIO105_DFI_D6,	
	GPIO104_DFI_D7,	
	GPIO111_DFI_D8,	
	GPIO164_DFI_D9,	
	GPIO163_DFI_D10,
	GPIO162_DFI_D11,
	GPIO161_DFI_D12,
	GPIO110_DFI_D13,
	GPIO109_DFI_D14,
	GPIO108_DFI_D15,
	GPIO143_ND_nCS0,
	GPIO144_ND_nCS1,	
	GPIO147_ND_nWE,			
	GPIO148_ND_nRE,					
	GPIO150_ND_ALE,				
	GPIO149_ND_CLE,			
	GPIO112_ND_RDY0,	
	GPIO160_ND_RDY1,
	GPIO154_SMC_IRQ,

	/* twsi5 */
	GPIO99_TWSI5_SCL,
	GPIO100_TWSI5_SDA,
};

//static struct pxa3xx_mfp_addr_map pxa688_mfp_addr_map[] __initdata = {
static struct mfp_addr_map pxa688_mfp_addr_map[] __initdata = {

	MFP_ADDR_X(GPIO0, GPIO58, 0x54),
	MFP_ADDR_X(GPIO59, GPIO73, 0x280),
	MFP_ADDR_X(GPIO74, GPIO101, 0x170),
	MFP_ADDR_X(GPIO115, GPIO122, 0x260),
	MFP_ADDR_X(GPIO124, GPIO141, 0xc),
	MFP_ADDR_X(GPIO143, GPIO151, 0x220),
	MFP_ADDR_X(GPIO152, GPIO153, 0x248),
	MFP_ADDR_X(GPIO154, GPIO155, 0x254),

	MFP_ADDR(GPIO114, 0x164),
	MFP_ADDR(GPIO123, 0x148),

	MFP_ADDR(GPIO102, 0x0),
	MFP_ADDR(GPIO103, 0x4),
	MFP_ADDR(GPIO104, 0x142),
	MFP_ADDR(GPIO168, 0x1e0),
	MFP_ADDR(GPIO167, 0x1e4),
	MFP_ADDR(GPIO166, 0x1e8),
	MFP_ADDR(GPIO165, 0x1ec),
	MFP_ADDR(GPIO107, 0x1f0),
	MFP_ADDR(GPIO106, 0x1f4),
	MFP_ADDR(GPIO105, 0x1f8),
	MFP_ADDR(GPIO104, 0x1fc),
	MFP_ADDR(GPIO111, 0x200),
	MFP_ADDR(GPIO164, 0x204),
	MFP_ADDR(GPIO163, 0x208),
	MFP_ADDR(GPIO162, 0x20c),
	MFP_ADDR(GPIO161, 0x210),
	MFP_ADDR(GPIO110, 0x214),
	MFP_ADDR(GPIO109, 0x218),
	MFP_ADDR(GPIO108, 0x21c),
	MFP_ADDR(GPIO110, 0x214),
	MFP_ADDR(GPIO109, 0x218),
	MFP_ADDR(GPIO112, 0x244),
	MFP_ADDR(GPIO160, 0x250),
	MFP_ADDR(GPIO113, 0x25c),

	MFP_ADDR(TWSI1_SCL, 0x140),
	MFP_ADDR(TWSI1_SDA, 0x144),
	MFP_ADDR(CLK_REQ, 0x160),

	MFP_ADDR_END,
};

void
dummy_delay(unsigned  int delay)
{
    volatile unsigned int i;
    for(i = 0; i < delay; i++);
}

static void twsi1_init(void)
{
	/* set twsi pins alternate function */
	*(volatile u32 *)0xD401E140 =
		*(volatile u32 *)0xD401E140 & 0xFFFFFFF8;
	*(volatile u32 *)0xD401E144 =
		*(volatile u32 *)0xD401E144 & 0xFFFFFFF8;

	/* setup twsi unit clocks - reset the unit */
	*(volatile u32 *)0xD4015004 = 0x4;
	*(volatile u32 *)0xD4015004 = 0x7;
	*(volatile u32 *)0xD4015004 = 0x3;
	dummy_delay(10000);

	/* initialize the twsi interface - reset the controller */
	*(volatile u32 *)0xD4011010 = 0x4060;
	*(volatile u32 *)0xD4011010 = 0x0060;
	*(volatile u32 *)0xD4011020 = 0x0000;
	dummy_delay(10000);

	return;
}

void send_cmd_to_max8925(unsigned long addr, unsigned int val)
{
	/* send pmic slave address with start bit */
	*(volatile u32 *)0xD4011008 = 0x78;
	*(volatile u32 *)0xD4011010 = 0x69;

	while (!(*(volatile u32 *)0xD4011018 & 0x40))
		dummy_delay(1);
	*(volatile u32 *)0xD4011018 |= 0x40;
	if (*(volatile u32 *)0xD4011018 & 0x20)
		*(volatile u32 *)0xD4011018 |= 0x20;

	*(volatile u32 *)0xD4011008 = addr;
	*(volatile u32 *)0xD4011010 = 0x68;

	while (!(*(volatile u32 *)0xD4011018 & 0x40))
		dummy_delay(1);
	*(volatile u32 *)0xD4011018 |= 0x40;
	if (*(volatile u32 *)0xD4011018 & 0x20)
		*(volatile u32 *)0xD4011018 |= 0x20;

	*(volatile u32 *)0xD4011008 = val;
	*(volatile u32 *)0xD4011010 = 0x6A;

	while (!(*(volatile u32 *)0xD4011018 & 0x40))
		dummy_delay(1);
	*(volatile u32 *)0xD4011018 |= 0x40;
	if (*(volatile u32 *)0xD4011018 & 0x20)
		*(volatile u32 *)0xD4011018 |= 0x20;

	*(volatile u32 *)0xD4011010 = *(volatile u32 *)0xD4011010 & ~0x2;
	return;
}

static void twsi1_ldos_reset(void)
{
	/* disable ldos one by one */
	send_cmd_to_max8925(0x20, 0x1E);	/* ldo3  */
	send_cmd_to_max8925(0x28, 0x1E);	/* ldo5  */
	send_cmd_to_max8925(0x2c, 0x1E);	/* ldo6  */
	send_cmd_to_max8925(0x30, 0x1E);	/* ldo7  */
	send_cmd_to_max8925(0x34, 0x1E);	/* ldo8  */
	send_cmd_to_max8925(0x3c, 0x1E);	/* ldo10 */
	send_cmd_to_max8925(0x40, 0x1E);	/* ldo11 */
	send_cmd_to_max8925(0x48, 0x1E);	/* ldo13 */
	send_cmd_to_max8925(0x4c, 0x1E);	/* ldo14 */
	send_cmd_to_max8925(0x50, 0x1E);	/* ldo15 */
	send_cmd_to_max8925(0x10, 0x1E);	/* ldo16 */
	send_cmd_to_max8925(0x14, 0x1E);	/* ldo17 */
	send_cmd_to_max8925(0x5c, 0x1E);	/* ldo19 */

	return;
}

static void twsi1_ldo11_enable(void)
{
	send_cmd_to_max8925(0x42, 0x29);	/* set ldo11 voltage */
	send_cmd_to_max8925(0x40, 0x1f);	/* enable ldo11 */

	return;
}

#ifdef CONFIG_PXA9XX_SDH
int board_mmc_init(bd_t *bd)
{
	return pxa9xx_mmc_init(bd);
}
#endif

unsigned long initdram (int board_type)
{
    return (PHYS_SDRAM_SIZE_DEC*1024*1024);
}

#ifdef CONFIG_PXA27X_KEYPAD
int pxa27x_keypad_init(void)
{
	*(volatile unsigned int *)0xd401e094 = 0xd001;
	*(volatile unsigned int *)0xd401e098 = 0xd001;
	*(volatile unsigned int *)0xd401e09c = 0xd001;
	*(volatile unsigned int *)0xd401e0a0 = 0xd001;
	*(volatile unsigned int *)0xd4012000 = 0xd3;
	*(volatile unsigned int *)0xd4012010 = 0x7f007f;
	*(volatile unsigned int *)0xd4012048 = 0x1e;
	return 0;

}
#endif

#ifdef CONFIG_RECOVERY_MODE
int recovery = 0;
#endif

#ifdef CONFIG_FREQ_CHG
void init_freq(void)
{
	if (cpu_is_pxa688_z1()) {
		/* set voltage to 1350mV by default */
		set_volt(1350);
		dummy_delay(99999);
		set_volt(1350);

		freq_init_sram(0xd1020000);
		freq_chg_seq(0xd1020000, 0xd1028000, 2, 1);
	}else{
		/* set voltage to 1350mV by default */
		set_volt(1350);
		dummy_delay(99999);
		set_volt(1350);

		freq_init_sram(0xd1020000);
		if (cpu_is_pxa688_a0())
			freq_chg_seq(0xd1020000, 0xd1028000, 3, 2);
		else
			freq_chg_seq(0xd1020000, 0xd1028000, 3, 3);
	}
}
#endif

int board_init (void)
{
	volatile unsigned int reg;

	wtm_read_profile();
	wtm_read_stepping();

#if defined (CONFIG_TRUST_BOOT) && defined (CONFIG_RECOVERY_MODE)
#ifdef CONFIG_NO_TRUST_OBM
	/* initialize magic and flag num since there is no trusted obm */
	mrvl_mag = CONFIG_TRUST_BOOT_MAGIC;
	mrvl_validation_flag = 0;
	mrvl_residue_flag = 0;
#else
	/* read trust boot flag */
	mrvl_mag = *(volatile unsigned int *)0xd1020100;
	mrvl_validation_flag = *(volatile unsigned int *)0xd1020104;
	mrvl_residue_flag = *(volatile unsigned int *)0xd1020108;
#endif

	/* for R_uboot only */
	if (*(volatile unsigned int *)0xd4010014 == 0x2 || \
		(mrvl_validation_flag & 0x15) != 0x0) {
		recovery = 1;
		R_uboot = 1;
	}
	else
		R_uboot = 0;
#endif

	/* Turn on clock gating (PMUM_CGR_PJ) */
	*(volatile unsigned int *)0xd4051024 = 0xffffffff;
	*(volatile unsigned int *)0xd4050038 = 0x5;
	*(volatile unsigned int *)0xD42828dc = 0x1FFFF;

	/* AIB clock */
	*(volatile unsigned int *)0xD4015064 = 0x7;
	*(volatile unsigned int *)0xD4015064 = 0x3;

	/* UART3 clk */
	*(volatile unsigned int *)0xD4015034 = 0x4 ; /*26MHz clock*/
	*(volatile unsigned int *)0xD4015034 = 0x7 | (1<<4) ; /*26MHz clock*/
	*(volatile unsigned int *)0xD4015034 = 0x3 | (1<<4); /*26MHz clock*/

	reg = *(volatile unsigned int *)0xD401E160;
	reg |= 1<<14; 
	*(volatile unsigned int *)0xD401E160 = reg;

	BU_REG_WRITE(0xd4015000, 0x83 );

	//APB TIMER clock
	*(volatile unsigned int *)0xd4015024 = 0x5;
	*(volatile unsigned int *)0xd4015024 = 0x33;

	reg = *(volatile unsigned int *)0xd4282c08;
	/*
	 * Bit 6: 0 - enable PJ4 global clock gating.
	 * Bit 14: 0 - enable L2 clock gating.
	 * Enable PJ4 and L2 clock gating for A1. Maybe A2 there after.
	 */
	if(cpu_is_pxa688_a1() || cpu_is_pxa688_a2())
		reg |= (1 << 19) | (1 << 13);
	else
		reg |= (1<<6) | (1<<14) | (1<<19) | (1<<13);

	reg |= (1 << 19) | (1 << 13);
	reg &= ~(1<<23);
	*(volatile unsigned int *)0xd4282c08 = reg;

	*(volatile unsigned int *)0xd4282110 = 0x0;

	*(volatile unsigned int *)0xd428290c = 0x600;
	*(volatile unsigned int *)0xd428290c = 0x610;
	*(volatile unsigned int *)0xd428290c = 0x710;
	*(volatile unsigned int *)0xd428290c = 0x712;

	*(volatile unsigned int *)0xd4050040 = 0xd0080040;
	*(volatile unsigned int *)0xd4050044 = 0xd0040040;
	*(volatile unsigned int *)0xd42a0c3c = 0x10800;
	*(volatile unsigned int *)0xd42a0c34 = 0x211921;

	*(volatile unsigned int *)0xd428285c = 0x8;
	*(volatile unsigned int *)0xd428285c = 0x9;
	*(volatile unsigned int *)0xd42828f8 = 0x18;
	*(volatile unsigned int *)0xd42828f8 = 0x1b;

	/* disable usb clock */
	/* usb3 clock cannot be disabled on z0/z1 in case of tftp error */
	*(volatile unsigned int *)0xd42828fc = 0x0;
	if (cpu_is_pxa688_z0() || cpu_is_pxa688_z1()) {
		*(volatile unsigned int *)0xd4282900 = 0x18;
		*(volatile unsigned int *)0xd4282900 = 0x1b;
	}
	else
		*(volatile unsigned int *)0xd4282900 = 0x0;

	/* Below is for IRE */
	if (cpu_is_pxa688_z0() || cpu_is_pxa688_z1()) {
		*(volatile unsigned int *)0xd4282848 = 0x00000008;
		*(volatile unsigned int *)0xd4282848 = 0x00000009;
	} else
        *(volatile unsigned int *)0xd4282848 = 0x0;

	/* disable memory stick clock*/
	*(volatile unsigned int *)0xd42828d8 = 0x0;

#ifdef CONFIG_MMC
	/* disable SMC clock*/
	*(volatile unsigned int *)0xd42828d4 = 0x0;
#endif

	/* TWSI1 clk*/
	*(volatile unsigned int *)0xD4015004 = 0x3; 
	dummy_delay(99999);

	/* SDHC clk*/
	if (cpu_is_pxa688_z0() || cpu_is_pxa688_z1())
		*(volatile unsigned int *)0xd4282854 |= 0x1b;
	else
		*(volatile unsigned int *)0xd4282854 |= 0x1b | (1<<10);
	*(volatile unsigned int *)0xd42828e8 |= 0x1b; 

	*(volatile unsigned int *)0xd428284c=(1<<3);
	*(volatile unsigned int *)0xd428284c=(1<<3) | 1;
	*(volatile unsigned int *)0xd428284c=(1<<3) | 1 | (1<<4);
	*(volatile unsigned int *)0xd428284c=(1<<3) | 1 | (1<<4) | (1<<1);

	/* SSP clk */
	*(volatile unsigned int *)0xd4015050=0x04;

	/* IPC clk */
	*(volatile unsigned int *)0xd4015078=0x04;

	/* enable GPIO clock */
	*(volatile unsigned int *)0xd4015038=0x03;

	/* Allow access of performance counters(PMU) from user space*/
	__asm__("mrc p15, 0, %0, c9, c14, 0" : "=r" (reg));
	reg |= 0x1;
	__asm__("mcr p15, 0, %0, c9, c14, 0" : : "r" (reg));

	/* Ensure branch prediction is enabled - BPU (Default) */
	__asm__("mrc p15, 0, %0, c1, c0, 0" : "=r" (reg));
	reg |= (1<<11);
	__asm__("mcr p15, 0, %0, c1, c0, 0" : : "r" (reg));

	if(cpu_is_pxa688_z0() || cpu_is_pxa688_z1()){
		__asm__("mrc p15, 1, %0, c15, c1, 0" : "=r" (reg));
		reg |= (1<<15);
		__asm__("mcr p15, 1, %0, c15, c1, 0" : : "r" (reg));
	}

	gd->bd->bi_dram[0].start = PHYS_SDRAM_1;
	gd->bd->bi_dram[0].size  = PHYS_SDRAM_1_SIZE;

	/* arch number of FPGA Board */
	gd->bd->bi_arch_number = 2957;  // MACH_TYPE_BROWNSTONE

	/* adress of boot parameters */
#ifdef CONFIG_PJ4_NON_SECURE_MODE
	gd->bd->bi_boot_params = 0x3C00;
#else
	gd->bd->bi_boot_params = 0x3C00;
#endif
	gd->baudrate = CONFIG_BAUDRATE;

	BU_REG_WRITE( APBC_AIB_CLK_RST, APBC_AIB_CLK_RST_FNCLK  |
		APBC_AIB_CLK_RST_APBCLK );

#ifndef CONFIG_MMC
	BU_REG_WRITE(0xd4283894, 0x11000008 );
#endif

	BU_REG_WRITE(0xd4015000, 0x83 );
	BU_REG_WRITE(0xd4015074, 0x3 );
	BU_REG_WRITE(0xd4282864, 0x9 );
#ifndef CONFIG_MMC
	BU_REG_WRITE(0xd4282860, 0xb8);
	BU_REG_WRITE(0xd4282860, 0xbf);
#else
	BU_REG_WRITE(0xd4282860, 0x0);
#endif

	/*configure for MFP*/
#if 0
	pxa3xx_init_mfp();
	pxa3xx_mfp_init_addr(pxa688_mfp_addr_map);
	pxa3xx_mfp_config(ARRAY_AND_SIZE(jasper_pin_config));
#endif
	mfp_init_base(CONFIG_MFPR_MMIOBASE);
	mfp_init_addr(pxa688_mfp_addr_map);
	mfp_config(jasper_pin_config, ARRAY_SIZE(jasper_pin_config));
	//mfp_config(nand_config, ARRAY_SIZE(nand_config));

#ifndef CONFIG_MMC
    *(volatile unsigned int *)0xd4283024=0;
#endif

#ifdef CONFIG_PXA27X_KEYPAD
	/* enable KEYPAD clock */
	*(volatile unsigned int *)0xd4015018=0x03;
	pxa27x_keypad_init();
#endif
	twsi1_init();
	twsi1_ldos_reset();
	twsi1_ldo11_enable();

	return 0;
}

int misc_init_r (void)
{
	char *env;

	/* primary network interface */
	env = getenv("ethprime");
	if(!env)
		setenv("ethprime","eth0");

	/* default usb mode */
	env = getenv("usbMode");
	if(!env)
		setenv("usbMode","host");

	/* linux boot arguments */
	env = getenv("default_load_addr");
	if(!env)
		setenv("default_load_addr",CONFIG_SYS_DEF_LOAD_ADDR);

	env = getenv("image_name");
	if(!env)
		setenv("image_name",CONFIG_SYS_IMG_NAME);

	env = getenv("bootfile");
	if(!env)
		setenv("bootfile",CONFIG_SYS_IMG_NAME);
	        
	env = getenv("initrd_name");
	if(!env)
	setenv("initrd_name",CONFIG_SYS_INITRD_NAME);

	env = getenv("initrd_load_addr");
	if(!env)
		setenv("initrd_load_addr",CONFIG_SYS_INITRD_LOAD_ADDR);

	env = getenv("initrd_size");
	if(!env)
		setenv("initrd_size",CONFIG_SYS_INITRD_SIZE);

	env = getenv("standalone_mtd");
	if(!env)
		setenv("standalone_mtd","fsload $(default_load_addr) $(image_name);setenv bootargs $(bootargs) root=/dev/mtdblock0 rw rootfstype=jffs2 ip=$(ipaddr):$(serverip)$(bootargs_end);bootm $(default_load_addr);");

	env = getenv("standalone_initrd");
	if(!env)
		setenv("standalone_initrd","fsload $(default_load_addr) $(image_name);fsload $(initrd_load_addr) $(initrd_name);setenv bootargs $(bootargs) root=/dev/ram0 rw initrd=0x$(initrd_load_addr),0x$(initrd_size) ip=$(ipaddr):$(serverip)$(bootargs_end); bootm $(default_load_addr);");

	return 0;
}


int dram_init (void)
{
	return 0;
}

#ifdef CONFIG_RECOVERY_MODE
int display_recovery_request(void)
{
	int i;

	i2c_init(0);
	ldo6_enable();
	printf("\nTo enter recovery mode: press and hold HOME button\n");
	printf("To enter fast boot mode: press and hold MENU button\n");
	for (i = 0; i < 5; i++) {
		printf(".");
		udelay(3000000);
	}
	return 0;
}
#endif

int checkboard (void)
{
#ifdef CONFIG_RECOVERY_MODE
#ifdef CONFIG_TRUST_BOOT
	if (!R_uboot)
		display_recovery_request();
#else
	display_recovery_request();
#endif
#endif
	display_marvell_banner();
	wtm_dump_info();

	return 0;
}

#ifdef CONFIG_PXA27X_KEYPAD
static enum key{
	HOME,
	MENU,
	BACK,
	NONE,
};

inline enum key pxa27x_key_read()
{
	char r;
	unsigned int val;

	r = (*(volatile unsigned int *)0xd4012008) & 0xff;
	if (r == 0x0b)
		return HOME;
	if (r == 0x0d)
		return MENU;
	if (r == 0x0e)
		return BACK;

	return NONE;
}
#endif

#ifdef CONFIG_RECOVERY_MODE

/* block address in misc partition */
#define MISC_BLK_ADDR			0xaac00
#if 0
#define NTIM_BLK_ADDR			MISC_BLK_ADDR + 0x03		/* 3 blocks */
#define PARTITION_BLK_ADDR		MISC_BLK_ADDR + 0x06		/* 1 block */
#define WTM_BLK_ADDR			MISC_BLK_ADDR + 0x07		/* 256 block */
#define MMP2_NTLOADER_BLK_ADDR		MISC_BLK_ADDR + 0x107		/* 150 blocks */
#endif
#define UBOOT_BLK_ADDR			MISC_BLK_ADDR + 0x03		/* 640 blocks */

/* burn address in emmc */
#if 0
#define NTIM_BURN_ADDR			0x0
#define PARTITION_BURN_ADDR		0x3
#define WTM_BURN_ADDR			0x8
#define MMP2_NTLOADER_BURN_ADDR		0x190
#endif
#define UBOOT_BURN_ADDR			0x300
#define UBOOT_BURN_SIZE			0x280

void uboot_update(void)
{
	struct mmc *mmc;
	char buffer[512 * UBOOT_BURN_SIZE];

	mmc = find_mmc_device(0);

	printf("=========================================\n");
	printf("==========Now update BootLoader==========\n");
	/* update ntim */
/*
	mmc->block_dev.switch_part(0, 0);
	mmc->block_dev.block_read(0, NTIM_BLK_ADDR, 3, buffer);
	mmc->block_dev.switch_part(0, 1);
	mmc->block_dev.block_write(0, NTIM_BURN_ADDR, 3, buffer);
*/
	/* update partition */
/*
	mmc->block_dev.switch_part(0, 0);
	mmc->block_dev.block_read(0, PARTITION_BLK_ADDR, 1, buffer);
	mmc->block_dev.switch_part(0, 1);
	mmc->block_dev.block_write(0, PARTITION_BURN_ADDR, 1, buffer);
*/
	/* update wtm */
/*
	mmc->block_dev.switch_part(0, 0);
	mmc->block_dev.block_read(0, WTM_BLK_ADDR, 256, buffer);
	mmc->block_dev.switch_part(0, 1);
	mmc->block_dev.block_write(0, WTM_BURN_ADDR, 256, buffer);
*/
	/* update mmp2_ntloader */
/*
	mmc->block_dev.switch_part(0, 0);
	mmc->block_dev.block_read(0, MMP2_NTLOADER_BLK_ADDR, 150, buffer);
	mmc->block_dev.switch_part(0, 1);
	mmc->block_dev.block_write(0, MMP2_NTLOADER_BURN_ADDR, 150, buffer);
*/

	/* update uboot */
	mmc->block_dev.switch_part(0, 0);
	mmc->block_dev.block_read(0, UBOOT_BLK_ADDR, UBOOT_BURN_SIZE, buffer);
	mmc->block_dev.switch_part(0, 1);
	mmc->block_dev.block_write(0, UBOOT_BURN_ADDR, UBOOT_BURN_SIZE, buffer);

	printf("\ndone.\n");
	printf("=========================================\n");
}

#ifdef CONFIG_TRUST_BOOT
void mv_trustboot_tag(void)
{
	*(volatile unsigned int *)0xd1020100 = mrvl_mag;
	*(volatile unsigned int *)0xd1020104 = mrvl_validation_flag;
	*(volatile unsigned int *)0xd1020108 = mrvl_residue_flag;
	return ;
}

int trustboot_test(int flag)
{
	struct mmc *mmc;
	char buffer[512];

	if (recovery) {
		/* bit 1, bit 3 and bit 5 of validation flag guarantee
		   the recovery images are trusted, including R_uboot,
		   R_kernel and R_ramdisk */
		if ((mrvl_validation_flag & 0x2a) && (mrvl_mag == CONFIG_TRUST_BOOT_MAGIC)) {
			/* Recovery images are not trusted */
			/* clean recovery flag */
			*(volatile unsigned int *)0xd4010014 = 0;
			mmc = find_mmc_device(0);
			strcpy(buffer, "reserved");
			mmc->block_dev.switch_part(0, 0);
			mmc->block_dev.block_write(0, MISC_BLK_ADDR, 1, buffer);

			/* trigger normal boot */
			printf("Bad recovery images! Try normal boot up.\n");
			recovery = 0;
		}
		else if (R_uboot && flag) {
			uboot_update();

			/* flush command field in misc with no use command */
			mmc = find_mmc_device(0);
			strcpy(buffer, "reserved");
			mmc->block_dev.switch_part(0, 0);
			mmc->block_dev.block_write(0, MISC_BLK_ADDR, 1, buffer);
			*(volatile unsigned int *)0xd4010014 = 0x0;

			/* reboot system */
			reset_cpu(0);
		}
		/* trigger recovery uboot */
		else {
			*(volatile unsigned int *)0xd4010014 = 0x2;
			return 0;
		}
	}

	/* go here to trigger normal boot */
	/* bit 0, bit 2 and bit 4 of validation flag guarantee
	   the nomarl images are trusted, including uboot,
	   kernel and ramdisk */
	if (mrvl_validation_flag & 0x15)
		/* normal images are not trusted */
		return -1;
	else
		/* normal images are trusted */
		return 0;
}
#endif

void magic_test(void)
{
	struct mmc *mmc;
	enum key state = NONE;
	int t = 0;
	char buffer[512];
	int flag = 0;

	/* test recovery by android reboot */
	if (*(volatile unsigned int *)0xd4010014 == 0x1)
		recovery = 1;
	/* test recovery by Magic Key */
#ifdef CONFIG_TRUST_BOOT
	else if (!R_uboot) {
#else
	else {
#endif
		printf("Magic Key testing");
		if (pxa27x_key_read() == HOME)
			state = HOME;
		else if (pxa27x_key_read() == MENU)
			state = MENU;

		/* test magic key holding for 3 seconds */
		if (state != NONE)
			while (state == pxa27x_key_read() && t < 3) {
				udelay(3000000);
				printf(".");
				t++;
			}
		printf(".\n");
		recovery = 0;
		if (t == 3)
			if (state == HOME)
				recovery = 1;
#ifdef CONFIG_CMD_FASTBOOT
			else if (state == MENU) {
				printf("Enter Fastboot mode\n");
				run_command("fb mmc", 0);
			}
#endif
	}

	/* test recovery by misc partition */
	mmc = find_mmc_device(0);
	mmc->block_dev.block_read(0, MISC_BLK_ADDR, 1, buffer);

	if (strcmp(buffer, "boot-recovery") == 0)
		recovery = 1;
	else if (strcmp(buffer, "update-firmware") == 0) {
		recovery = 1;
		flag = 1;
	}

	if (recovery) {
		printf("Enter Recovery mode\n");
	}

#ifdef CONFIG_TRUST_BOOT
	/* If trustboot_test() returns non-zero, stop the boot sequence
	   and go to dead loop */
	if (trustboot_test(flag)) {
		if (mrvl_mag == CONFIG_TRUST_BOOT_MAGIC) {
			printf("Images are not trusted!!!\n");
			while(1);
		}
		else
			printf("Marvell magic number broken!!!\n");
	}
#endif
}
#endif

int display_marvell_banner (void)
{
	printf("\n");
	printf(" __  __                      _ _\n");
	printf("|  \\/  | __ _ _ ____   _____| | |\n");
	printf("| |\\/| |/ _` | '__\\ \\ / / _ \\ | |\n");
	printf("| |  | | (_| | |   \\ V /  __/ | |\n");
	printf("|_|  |_|\\__,_|_|    \\_/ \\___|_|_|\n");
	printf(" _   _     ____              _\n");
	printf("| | | |   | __ )  ___   ___ | |_ \n");
	printf("| | | |___|  _ \\ / _ \\ / _ \\| __| \n");
	printf("| |_| |___| |_) | (_) | (_) | |_ \n");
	printf(" \\___/    |____/ \\___/ \\___/ \\__| ");
	printf("\n\nMARVELL MMP2 AP.");
   if(cpu_is_pxa688_a2())
   	printf("\nBased on MMP2 A2 CPU.\n\n"); 
	if(cpu_is_pxa688_a1())
		printf("\nBased on MMP2 A1 CPU.\n\n"); 
	if(cpu_is_pxa688_a0())
		printf("\nBased on MMP2 A0 CPU.\n\n"); 
	else if(cpu_is_pxa688_z1())
		printf("\nBased on MMP2 Z1 CPU.\n\n"); 
	else if(cpu_is_pxa688_z0())
		printf("\nBased on MMP2 Z0 CPU.\n\n"); 

#if defined (CONFIG_RECOVERY_MODE) && defined (CONFIG_TRUST_BOOT)
	if (R_uboot)
		printf("\nEntering Maverll Recovery Uboot.\n");
#endif
	return 0;
}

#ifdef CONFIG_CMD_NET
int board_eth_init(bd_t *bis)
{
	int res = -1;

#if defined(CONFIG_MMPUDC)
	if (usb_eth_initialize(bis) >= 0)
		res = 0;
#endif
	return res;
}
#endif

#ifdef CONFIG_MMPUDC
int usb_lowlevel_init(void)
{
	int base = CONFIG_USB_PHY_BASE;
	int count = 0;
	int reg;

	/* enable u2o clock */
	writel(0x18, PMUA_USB_CLK_RES_CTRL);
	writel(0x1b, PMUA_USB_CLK_RES_CTRL);

	/* initialize the usb phy power */
	writel(0x10101003, base + UTMI_CTRL);
	/* UTMI_PLL settings */
	writel(0x7e819eeb, base + UTMI_PLL);
	/* UTMI_TX */
	writel(0x41e947c3, base + UTMI_TX);
	/* UTMI_RX */
	writel(0xec1d0271, base + UTMI_RX);
	/* UTMI_IVREF */
	writel(0x2000007e, base + UTMI_IVREF);
	/* calibrate */
	count = 10000;
	while(((readl(base + UTMI_PLL) & PLL_READY)==0) && count--);
	if (count <= 0)
		printf("Calibrate timeout, UTMI_PLL:%x\n",
				readl(base + UTMI_PLL));

	/* toggle VCOCAL_START bit of UTMI_PLL */
	udelay(200);
	reg = readl(base + UTMI_PLL);
	writel(reg | VCOCAL_START, base + UTMI_PLL);
	udelay(40);
	writel(reg & (~VCOCAL_START), base + UTMI_PLL);

	/* toggle REG_RCAL_START bit of UTMI_TX */
	udelay(200);
	reg = readl(base + UTMI_TX);
	writel(reg | REG_RCAL_START, base + UTMI_TX);
	udelay(40);
	writel(reg & (~REG_RCAL_START), base + UTMI_TX);
	udelay(200);

	/* make sure phy is ready */
	count = 1000;
	while(((readl(base + UTMI_PLL) & PLL_READY)==0) && count--);
	if (count <= 0)
		printf("Calibrate timeout, UTMI_PLL %x\n",
				readl(base + UTMI_PLL));

	return 0;
}
#endif

void lowlevel_init()
{
	return;
}

struct wtm_cmd {
    unsigned int prim_cmd_parm0;          // 0x0
    unsigned int prim_cmd_parm1;          // 0x4
    unsigned int prim_cmd_parm2;          // 0x8
    unsigned int prim_cmd_parm3;          // 0xc
    unsigned int prim_cmd_parm4;          // 0x10
    unsigned int prim_cmd_parm5;          // 0x14
    unsigned int prim_cmd_parm6;          // 0x18
    unsigned int prim_cmd_parm7;          // 0x1c
    unsigned int prim_cmd_parm8;          // 0x20
    unsigned int prim_cmd_parm9;          // 0x24
    unsigned int prim_cmd_parm10;         // 0x28
    unsigned int prim_cmd_parm11;         // 0x2c
    unsigned int prim_cmd_parm12;         // 0x30
    unsigned int prim_cmd_parm13;         // 0x34
    unsigned int prim_cmd_parm14;         // 0x38
    unsigned int prim_cmd_parm15;         // 0x3c
    unsigned int secure_processor_cmd;    // 0x40
};

/*
 * WTM register file for host communication
 */
struct wtm_mail_box {
	unsigned int prim_cmd_parm0;          // 0x0
	unsigned int prim_cmd_parm1;          // 0x4
	unsigned int prim_cmd_parm2;          // 0x8
	unsigned int prim_cmd_parm3;          // 0xc
	unsigned int prim_cmd_parm4;          // 0x10
	unsigned int prim_cmd_parm5;          // 0x14
	unsigned int prim_cmd_parm6;          // 0x18
	unsigned int prim_cmd_parm7;          // 0x1c
	unsigned int prim_cmd_parm8;          // 0x20
	unsigned int prim_cmd_parm9;          // 0x24
	unsigned int prim_cmd_parm10;         // 0x28
	unsigned int prim_cmd_parm11;         // 0x2c
	unsigned int prim_cmd_parm12;         // 0x30
	unsigned int prim_cmd_parm13;         // 0x34
	unsigned int prim_cmd_parm14;         // 0x38
	unsigned int prim_cmd_parm15;         // 0x3c
	unsigned int secure_processor_cmd;    // 0x40
	unsigned char reserved_0x44[60];
	unsigned int cmd_return_status;       // 0x80
	unsigned int cmd_status_0;            // 0x84
	unsigned int cmd_status_1;            // 0x88
	unsigned int cmd_status_2;            // 0x8c
	unsigned int cmd_status_3;            // 0x90
	unsigned int cmd_status_4;            // 0x94
	unsigned int cmd_status_5;            // 0x98
	unsigned int cmd_status_6;            // 0x9c
	unsigned int cmd_status_7;            // 0xa0
	unsigned int cmd_status_8;            // 0xa4
	unsigned int cmd_status_9;            // 0xa8
	unsigned int cmd_status_10;           // 0xac
	unsigned int cmd_status_11;           // 0xb0
	unsigned int cmd_status_12;           // 0xb4
	unsigned int cmd_status_13;           // 0xb8
	unsigned int cmd_status_14;           // 0xbc
	unsigned int cmd_status_15;           // 0xc0
	unsigned int cmd_fifo_status;         // 0xc4
	unsigned int host_interrupt_register; // 0xc8
	unsigned int host_interrupt_mask;     // 0xcc
	unsigned int host_exception_address;  // 0xd0
	unsigned int sp_trust_register;       // 0xd4
	unsigned int wtm_identification;      // 0xd8
	unsigned int wtm_revision;            // 0xdc
};


#define WTM_BASE			0xD4290000
#define WTM_GET_SOC_POWER_POINT		0x1006
#define WTM_GET_SOC_STEPPING		0x1007
#define WTM_PRIM_CMD_COMPLETE_MASK	(1 << 0)

unsigned int mmp2_ack_from_wtm   = 0x0;
unsigned int mmp2_profile_adjust = 0x0;
unsigned int mmp2_profile	 = 0x0;
unsigned int mmp2_max_freq	 = 0x0;
unsigned int mmp2_ts_calibration = 0x0;

unsigned int mmp2_stepping	 = 0x0;

static volatile struct wtm_mail_box *wtm_mb =
	(volatile struct wtm_mail_box *)(WTM_BASE);

static int wtm_exe_cmd(struct wtm_cmd *cmd)
{
	int i;

	unsigned int *pcmd = &cmd->prim_cmd_parm0;
	volatile unsigned int *phi = &wtm_mb->prim_cmd_parm0;

	for (i = 0; i <= 16; i++) {
		*phi++ = *pcmd++;
	}

	/* try 1000 times */
	for (i = 0; i < 10000; i++) {
		if (wtm_mb->host_interrupt_register &
		    WTM_PRIM_CMD_COMPLETE_MASK) {
			/* clean interrupt */
			wtm_mb->host_interrupt_register = 0xFFFFFFFF;
			return wtm_mb->cmd_return_status;
		}
	}

	/* read fail */
	return -1;
}

static int wtm_read_profile(void)
{
	struct wtm_cmd cmd;
	int status;

	memset(&cmd, 0, sizeof(cmd));

	/* valid request */
	cmd.prim_cmd_parm0 = 0;
	cmd.secure_processor_cmd = WTM_GET_SOC_POWER_POINT;
	status = wtm_exe_cmd(&cmd);
	if (status < 0) {
		mmp2_ack_from_wtm = 0;
		goto out;
	}

	mmp2_ack_from_wtm   = 1;
	mmp2_profile_adjust = wtm_mb->cmd_status_0;
	mmp2_profile	    = wtm_mb->cmd_status_1;
	mmp2_max_freq	    = wtm_mb->cmd_status_2;
	mmp2_ts_calibration = wtm_mb->cmd_status_3;

out:
	return status;
}

static int wtm_read_stepping(void)
{
	struct wtm_cmd cmd;
	int status;

	unsigned int chip_id;
	unsigned int id;
	unsigned int soc_stepping;

	memset(&cmd, 0, sizeof(cmd));

	/* valid request */
	cmd.prim_cmd_parm0 = 0;
	cmd.secure_processor_cmd = WTM_GET_SOC_STEPPING;
	status = wtm_exe_cmd(&cmd);
	if (status < 0) {
		goto out;
	}

	soc_stepping = wtm_mb->cmd_status_0;
	chip_id      = __raw_readl(0xd4282c00);
	id           = read_cpuid(CPUID_ID);
	if ((chip_id & 0x00ff0000) != 0x00a00000)
		goto out;

	if (soc_stepping == 0x4130)
		mmp2_stepping = 0xa0;
	else if (soc_stepping == 0x4131)
		mmp2_stepping = 0xa1;
	else if (soc_stepping == 0x4132)
		mmp2_stepping = 0xa2;

out:
	return status;
}

static void wtm_dump_info(void)
{
	printk("----- wtm info -----\n");

	if (!mmp2_ack_from_wtm)
		printf("wtm has NO ack.\n");
	else
		printf("get ack from wtm.\n");

	printf("profile_adjust = 0x%08x\n", mmp2_profile_adjust);
	printf("profile        = 0x%08x\n", mmp2_profile);
	printf("max_freq       = 0x%08x\n", mmp2_max_freq);
	printf("ts_calibration = 0x%08x\n", mmp2_ts_calibration);
	printf("stepping       = 0x%08x\n", mmp2_stepping);

	printk("-----   end    -----\n\n");

	return;
}

