#include <common.h>
#include <command.h>
#include <mmc.h>
#include <part.h>
#include <fastboot/flash.h>
#include "efi_table.c"

#define alignment_down(a, size) (a & (~(size-1)) )
#define alignment_up(a, size) ((a+size-1) & (~ (size-1))) 

#if 0
struct flash_partition_map
{
	//basically key is capacity even this is some stupid
	int key;

	struct ptentry* boot;
	struct ptentry* user;
};

#ifdef CONFIG_EFI_PARTITION
//for simplicity ,these array must be sorted 
struct flash_partition_map flash_map[] = 
{
	{2000/*1868*/,boot_block,sem02g_user},
	{4000/*3776*/,boot_block,sem04g_user},
	{8000/*7576*/,boot_block,sem08g_user},
	{16000/*15188*/,boot_block,sem016g_user},
};

#endif 
#endif


#define MAX_PTN 32

static ptentry ptable[MAX_PTN];
static unsigned pcount = 0;

void flash_add_ptn(ptentry *ptn)
{
    if(pcount < MAX_PTN){
        memcpy(ptable + pcount, ptn, sizeof(*ptn));
        pcount++;
    }
}

void flash_dump_ptn(void)
{
    unsigned n;

	printf("\n");
    for(n = 0; n < pcount; n++) {
        ptentry *ptn = ptable + n;
        printf("ptn %d name='%s' start=%#x len=%#x\n",
                n, ptn->name, ptn->start, ptn->length);
    }
}

ptentry *flash_find_ptn(const char *name)
{
    unsigned n;
    char* pname = name;
    for(n = 0; n < pcount; n++) {
        if(!strcmp(ptable[n].name, pname)) {
            return ptable + n;
        }
    }
    //reach here means not found
    if(!strcmp(pname,"recovery"))
        pname = "kernel_recovery";
    else if(!strcmp(pname,"system"))
        pname = "android_system";
    else if(!strcmp(pname,"data"))
        pname = "android_data";
        
    for(n = 0; n < pcount; n++) {
        if(!strcmp(ptable[n].name, pname)) {
            return ptable + n;
        }
    }
    
		
    return 0;
}

ptentry *flash_get_ptn(unsigned n)
{
    if(n < pcount) {
        return ptable + n;
    } else {
        return 0;
    }
}

unsigned flash_get_ptn_count(void)
{
    return pcount;
}

int flash_erase(ptentry *ptn)
{
	char cmdbuffer[128];

	//switch booting mmc device
	run_command("mmc sw_dev 0",0);

	//mmc erase function does not work properly,we use alternate write function to erase 		
	run_command("mw.b 0x1100000 0 0x80000",0);
 	// erase flash
	//sprintf(cmdbuffer,"mmc erase 0x%x 0x%x",ptn->start,ptn->length);
	sprintf(cmdbuffer,"mmc write 0x%x 0x%x 0x1100000",ptn->start/512,0x400);
	return run_command(cmdbuffer,0);	
}

int flash_write(ptentry *ptn, unsigned long offset, const void *data, unsigned bytes)
{
	char cmdbuffer[128];
	int blks,writeblks,startblks,offsetblks;

	if(!ptn) return -1;
	bytes = alignment_up(bytes,512);
	blks = ptn->length/512;
	writeblks = bytes/512;
	offsetblks = offset/512;
	startblks = (ptn->start+offset)/512;
	if(writeblks>(blks-offsetblks)) return -1;

	//switch booting mmc device
	run_command("mmc sw_dev 0",0);

	// switch partition
	sprintf(cmdbuffer,"mmc sw_part %d",ptn->flags&FLASH_PARTITION_MASK);
	run_command(cmdbuffer,0);
	// write flash,we assume sector(block) size 512 bytes since our mmc driver only support block size of 512 bytes
	//format :mmc write start_block blockcount offset	
	sprintf(cmdbuffer,"mmc write 0x%x 0x%x 0x%x",startblks,writeblks,(unsigned int*)data);
	printf("%s\n",cmdbuffer);
	return run_command(cmdbuffer,0);

}

int flash_read(ptentry *ptn, unsigned long offset, const void *data, unsigned bytes)
{
    char cmdbuffer[128];
    int blks,readblks,startblks,offsetblks;

    if(!ptn) return -1;

    bytes = alignment_up(bytes,512);

    blks = ptn->length/512;
    startblks = (ptn->start+offset)/512;
    readblks = alignment_up(bytes,512)/512;
    offsetblks = offset/512;
    if(readblks>(blks-offsetblks))
        readblks = (blks-offsetblks);
    
    //switch booting mmc device
    run_command("mmc sw_dev 0",0);

    // switch partition
    sprintf(cmdbuffer,"mmc sw_part %d",ptn->flags&FLASH_PARTITION_MASK);
    run_command(cmdbuffer,0);
    // read  flash,we assume sector(block) size 512 bytes since our mmc driver only support block size of 512 bytes
    //format :mmc read start_block blockcount offset

    sprintf(cmdbuffer,"mmc read 0x%x 0x%x 0x%x",startblks,readblks,(unsigned int*)data);
    printf("%s\n",cmdbuffer);
    return run_command(cmdbuffer,0);
	
}



int flash_init(void)
{
	
	ptentry* entry;	
	block_dev_desc_t *block_dev;
	struct mmc *mmc = find_mmc_device(0);
	int i;
	pcount = 0;
	//add boot partition entries
	for(i=0;;i++)
	{
		entry = &boot_block[i];
		if(entry->length)
		{
			flash_add_ptn(entry);
		}
		else
			break;
	}

	if(mmc)
		mmc_init(mmc);
	//now try to init partition rely on partition table
	block_dev = mmc_get_dev(0);//block device 0 is user data block
	if (block_dev){	
        ptentry entry;
		init_part(block_dev);
		//add pgpt and agpt 
		#ifdef CONFIG_EFI_PARTITION		
        memset(&entry,0,sizeof(ptentry));
        strcpy(entry.name,"pgpt");
        entry.start = 0;
        entry.length = 0x22 * block_dev->blksz;
        flash_add_ptn(&entry);
        memset(&entry,0,sizeof(ptentry));
        strcpy(entry.name,"agpt");
        entry.start = (block_dev->lba-0x21)*block_dev->blksz;
        entry.length = 0x21 * block_dev->blksz;
        flash_add_ptn(&entry);        
		#endif
		if(PART_TYPE_UNKNOWN!=block_dev->part_type){
			disk_partition_t info;
			ptentry newentry;
			//fetch partitions
			for(i=1;i<=16;i++){				
				memset(&info,0,sizeof(disk_partition_t));
				if(!get_partition_info (block_dev,i, &info)){
					memset(&newentry,0,sizeof(ptentry));
					strcpy(newentry.name,(char*)info.name);
					newentry.start = info.start * info.blksz;
					newentry.length = info.size * info.blksz;
					flash_add_ptn(&newentry);
				}
				else break;
			}
		}
		else
			return -1;
	}else {
		return -1;	
	}

	
	return 0;
}

static inline int str2long(char *p, ulong *num)
{
	char *endptr;

	*num = simple_strtoul(p, &endptr, 16);
	return (*p != '\0' && *endptr == '\0') ? 1 : 0;
}

static int
arg_off_size(int argc, char *argv[],ulong *off, size_t *size)
{
	if (argc >= 1) {
		if (!(str2long(argv[0], off))) {
			printf("'%s' is not a number\n", argv[0]);
			return -1;
		}
	}
	if (argc >= 2) {
		if (!(str2long(argv[1], (ulong *)size))) {
			printf("'%s' is not a number\n", argv[1]);
			return -1;
		}
	}

	return 0;
}


static int do_uflash(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	int i, dev, ret = 0;
	ulong addr, off;
	size_t size;
	char *cmd, *s;

	/* at least two arguments please */
	if (argc < 2)
		goto usage;


	cmd = argv[1];

	if (strcmp(cmd, "info") == 0) {
		putc('\n');
		flash_dump_ptn();
		return 0;
	}
	
	if (strcmp(cmd, "reset") == 0) {
		putc('\n');
		flash_init();
		return 0;
	}


	if (strncmp(cmd, "read", 4) != 0 && strncmp(cmd, "write", 5) != 0  )
		goto usage;


	if (strncmp(cmd, "read", 4) == 0 || strncmp(cmd, "write", 5) == 0) {
    	ptentry* ptn;
		int read;

		if (argc < 6)
			goto usage;

		ptn = flash_find_ptn(argv[2]);
		if(!ptn){
		    printf("part [%s] not found\n",argv[2]);
		}

		addr = (ulong)simple_strtoul(argv[3], NULL, 16);
		off = 0;
		size = ptn->length;

		read = strncmp(cmd, "read", 4) == 0; /* 1 = read, 0 = write */
		printf("\nuflash %s: ", read ? "read" : "write");
		if (arg_off_size(argc - 4, argv + 4, &off, &size) != 0)
			return 1;
			
        if(read){
            ret = flash_read(ptn,off,addr,size);
        }else
            ret = flash_write(ptn,off,addr,size);

		return ret == 0 ? 0 : 1;
	}
usage:
	printf("Usage:\n%s\n", cmdtp->usage);
	return 1;
}

U_BOOT_CMD(
	uflash,	6,	1,	do_uflash,
	"uflash    - universal flash device interface\n",
	"info  - show available uflash devices\n"
	"reset - reinit uflash partitions\n"
	"read part addr off size\n"
	"write part addr off size - read/write `size' bytes starting\n"
	"    at offset `off' to/from memory address `addr'\n"
);




