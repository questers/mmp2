#include <common.h>
#include <command.h>
#include <bmpmanager.h>
#include <part.h>
#include <fat.h>
#include <fastboot/flash.h>

static u8 RES_upgrade[]=
{
#include "upgrade_logo.inc"
};

#define MAX_BUFFER_SIZE             ( 4096 )
#define MAX_CMD_LEN                 ( 256 )


typedef struct {
    const char* devprepare;
    const char* devname;
    const int devidx;
}AUTO_UPDATE_DEVICE_DESC_T;
static void parse_cmd_and_run( char *buf, int size )
{
    char        command[ MAX_CMD_LEN ];
    int         i, j, count;

    count = 0;

    for( i = 0; i < size; i++ )
    {
        switch( buf[ i ] )
        {
            case '\n':
                command[ count ] = '\0';
                for( j = 0; j < count; j++ )
                {
                    if( command[ j ] == '#' )
                        command[ j ] = '\0';
                }

                if( strlen( command ) )
                    run_command( command, 0 );

                count = 0;
                break;

            case '\r':
                /* Do nothing , just skip it */
                break;

            case ' ':
                if( count )
                    command[ count++ ] = buf[ i ];
                break;

            default:
                command[ count++ ] = buf[ i ];
        }
    }
}

static int do_autoupdate( cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[] )
{
    AUTO_UPDATE_DEVICE_DESC_T devs[] = {
        #ifdef CONFIG_USB_EHCI 
        {"usb start","usb",0}, 
        #endif 
        #ifdef CONFIG_MMC 
        {"mmc sw_dev 1;mmc rescan","mmc",1}, 
        #endif 
    };
    char desc_file[256]={0};
	block_dev_desc_t    *dev_desc = NULL;
	int                 size;
	char                buffer[ MAX_BUFFER_SIZE ];
	int dev_count = ARRAY_SIZE(devs);
	int dev=0;

    sprintf(desc_file,"%supdate.txt",CONFIG_AUTOUPDATER_DIRECTORY);
	if(argc>1){
	    strcpy(desc_file,argv[1]);
	}

    if(!dev_count) return -1;


    
    do {
        if(devs[dev].devprepare)
            run_command( devs[dev].devprepare, 0 );
    	dev_desc = get_dev( devs[dev].devname, devs[dev].devidx);
    	if( dev_desc == NULL )
    		printf("device [%s:%d] not found\n",devs[dev].devname,devs[dev].devidx);
    	else
    	{
    		if( fat_register_device( dev_desc, 1 ) != 0 )
    			printf ("\n*No fat device found on %s:%d*\n", devs[dev].devname,1);
    		else if(file_fat_detectfs()){
    			printf ("\n*No fat fs found on %s:%d*\n", devs[dev].devname,1);
    		}
    		else
    		{
    			size = file_fat_read( desc_file, buffer, MAX_BUFFER_SIZE );
				if(size<=0){
					//try device desc file
					sprintf(desc_file,"%supdate_%s.txt",CONFIG_AUTOUPDATER_DIRECTORY,devs[dev].devname);
					size = file_fat_read( desc_file, buffer, MAX_BUFFER_SIZE );
					
				}
    			if( size && (size!=-1))
    			{
                    void* bmp = CONFIG_SYS_LOAD_ADDR;	
                    lcd_clear();
                    if(!bmp_manager_readbmp("bmp.autoupdater",bmp,DEFAULT_FB_SIZE)){
                        lcd_draw_bmp(bmp,0);
                    }else
						lcd_draw_bmp(RES_upgrade,0);
    				buffer[ size ] = '\0';
    				parse_cmd_and_run( buffer, size );
    				return 0;
    			}    
    		}
    	}
    	dev++;
	}while(dev<dev_count);

	return -1;
}

U_BOOT_CMD(
	autoupdate,	2,	1,	do_autoupdate,
	"autoupdate - run autoupdater\n",
	"Read in"
	CONFIG_AUTOUPDATER_DIRECTORY "from usb/sd disk then update system"
);

