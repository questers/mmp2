/*
 *  U-Boot command for frequency change support
 *
 *  Copyright (C) 2008, 2009 Marvell International Ltd.
 *  All Rights Reserved
 *  Ning Jiang <ning.jiang@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <common.h>
#if (defined(CONFIG_TAVOREVB) || defined(CONFIG_TTC_DKB) || \
     defined(CONFIG_ASPENITE) || defined(CONFIG_ZYLONITE2) || \
     defined(CONFIG_MMP2_JASPER) || defined(CONFIG_MMP2_FLINT)) || \
     defined(CONFIG_MMP2_BROWNSTONE) || defined(CONFIG_MMP2_G50)
#include <command.h>
#include <asm/io.h>
#include "pmua.h"
#include "pmuc.h"
#include "pmud.h"
#include "pmum.h"
#include "predefines.h"
#include <asm/arch/icu.h>
#include <asm/arch/common.h>
#include <mfp.h>
#include <asm/arch/mfp-pxa688.h>


#define CONFIG_MIPS
#define CONFIG_OPTEST
#define CONFIG_CPUID
#define CONFIG_SETVOL
#define CONFIG_WFI

#define u32_t	unsigned int
#define u16_t	unsigned short
#define u8_t	unsigned char

#define reg_read(x) (*(volatile u32_t *)(x))
#define reg_write(x,y) ((*(volatile u32_t *)(x)) = y )

void freq_init_sram(u32 sram_code_addr);
/*
@******************************************************************************
@
@ freq_chg_seq
@
@ frequency change sequence
@
@ Inputs:
@	r0 = Start address of relocated program
@	r1 = Start address of relocated stack
@	r2 = operating points
@	r3 = chip steppings: 0 - z0, 1 - z1, 2 - a0 and 3 - a1
@
@ Outputs:
@	None
@

*/
void freq_chg_seq(u32 sram_code_addr, u32 sram_stack_addr, u32 op, u32 chip_stepping);

void hibernate_init_sram(u32 sram_code_addr);
void hibernate_seq(u32 sram_code_addr, u32 sram_stack_addr, u32 op);

static void switch_op(int op)
{
	freq_init_sram(0xd1020000);
	if (cpu_is_pxa688_z1()) {
		freq_chg_seq(0xd1020000, 0xd1028000, op, 1);
	} else if (cpu_is_pxa688_a0()) {
		freq_chg_seq(0xd1020000, 0xd1028000, op, 2);
	} else {
		freq_chg_seq(0xd1020000, 0xd1028000, op, 3);
	}

}

int do_op(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	int op;

	if (argc != 2) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	op = simple_strtoul(argv[1], NULL, 0);
	if(op<1||op>6)
	{
		eprintf("op number invalid,must be between 1-6\n");
		return 2;
	}
	switch_op(op);

	return 0;
}

U_BOOT_CMD(
		op,	6,	1,	do_op,
		"op <op_num>  change operating point number(1-6)",
		"op <op_num>  change operating point number(1-6)"
	  );

#ifdef CONFIG_MIPS

static unsigned long loops_per_sec;

#define READ_TIMER \
		(*(volatile ulong *)(CONFIG_SYS_TIMERBASE+0xa4) = 0x1, \
		*(volatile ulong *)(CONFIG_SYS_TIMERBASE+0xa4))

#ifdef CONFIG_CPU_PXA688
#define PMNC_MASK	0x3f		/* Mask for writable bits */
#define PMNC_E		(1 << 0)	/* Enable all counters */
#define PMNC_P		(1 << 1)	/* Reset all counters */
#define PMNC_C		(1 << 2)	/* Cycle counter reset */
#define PMNC_D		(1 << 3)	/* CCNT counts every 64th cpu cycle */
#define PMNC_X		(1 << 4)	/* Export to ETM */
#define PMNC_DP		(1 << 5)	/* Disable CCNT if non-invasive debug*/

#define FLAG_C		(1 << 31)
#define FLAG_MASK	0x8000000f	/* Mask for writable bits */

#define CNTENC_C	(1 << 31)
#define CNTENC_MASK	0x8000000f	/* Mask for writable bits */

#define CNTENS_C	(1 << 31)
#define CNTENS_MASK	0x8000000f	/* Mask for writable bits */

static inline u32 armv7_pmnc_read(void)
{
	u32 val;

	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r" (val));
	return val;
}

static inline void armv7_pmnc_write(u32 val)
{
	val &= PMNC_MASK;
	asm volatile("mcr p15, 0, %0, c9, c12, 0" : : "r" (val));
}

static inline void armv7_pmnc_disable_counter()
{
	u32 val;

	val = CNTENC_C;

	val &= CNTENC_MASK;
	asm volatile("mcr p15, 0, %0, c9, c12, 2" : : "r" (val));
}

static void armv7_pmnc_reset_counter()
{
	u32 val = 0;

	asm volatile("mcr p15, 0, %0, c9, c13, 0" : : "r" (val));
}


static inline void armv7_pmnc_enable_counter()
{
	u32 val;

	val = CNTENS_C;
	val &= CNTENS_MASK;
	asm volatile("mcr p15, 0, %0, c9, c12, 1" : : "r" (val));
}

static inline void armv7_start_pmnc(void)
{
	armv7_pmnc_write(armv7_pmnc_read() | PMNC_E);
}

static inline void armv7_stop_pmnc(void)
{
	armv7_pmnc_write(armv7_pmnc_read() & ~PMNC_E);
}

static inline u32 armv7_counter_read(void)
{
	u32 val;

	asm volatile("mrc p15, 0, %0, c9, c13, 0" : "=r" (val));
	return val;
}

static inline u32 armv7_pmnc_getreset_flags(void)
{
	u32 val;

	/* Read */
	asm volatile("mrc p15, 0, %0, c9, c12, 3" : "=r" (val));

	/* Write to clear flags */
	val &= FLAG_MASK;
	asm volatile("mcr p15, 0, %0, c9, c12, 3" : : "r" (val));

	return val;
}

int do_calibrate_delay(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	ulong oldtimer, timer;
	unsigned int val, val_new;
	unsigned int flags;

	loops_per_sec = 0;
	printf("Calibrating delay loop.. ");

	armv7_pmnc_write(PMNC_P | PMNC_C);
	armv7_pmnc_disable_counter();
	armv7_pmnc_reset_counter();
	armv7_pmnc_enable_counter();
	val = armv7_counter_read();
	armv7_start_pmnc();

	oldtimer = READ_TIMER;
	oldtimer = READ_TIMER;
	while (1) {
		timer = READ_TIMER;
		timer = READ_TIMER;
		timer = timer - oldtimer;
		if (timer >= CONFIG_SYS_HZ)
			break;
	}

	armv7_stop_pmnc();
	val_new = armv7_counter_read();
	flags = armv7_pmnc_getreset_flags();
	if(flags & FLAG_C)
		printf("counter overflow\n");
	loops_per_sec = val_new - val;

	printf("ok - %lu.%02lu BogoMips\n",
			loops_per_sec/1000000,
			(loops_per_sec/10000) % 100);

	return 0;
}
#else
static inline void __delay(unsigned long loops)
{
	__asm__ __volatile__ ("1:\n" "subs %0, %1, #1\n"
			"bne 1b":"=r" (loops):"0"(loops));
}

int do_calibrate_delay(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	ulong oldtimer, timer, left;

	loops_per_sec = 1;
	printf("Calibrating delay loop.. ");
	while (loops_per_sec <<= 1) {
		oldtimer = READ_TIMER;
		oldtimer = READ_TIMER;
		__delay(loops_per_sec);
		timer = READ_TIMER;
		timer = READ_TIMER;
		timer = timer - oldtimer;
		if (timer >= CONFIG_SYS_HZ) {
			left = loops_per_sec % timer;
			loops_per_sec = loops_per_sec / timer * CONFIG_SYS_HZ;
			while (left > 100000) {
				loops_per_sec += CONFIG_SYS_HZ / (timer / left);
				left = timer % left;
			}
			printf("ok - %lu.%02lu BogoMips\n",
					loops_per_sec/500000,
					(loops_per_sec/5000) % 100);
			return 0;
		}
	}
	printf("failed\n");
	return -1;
}
#endif


U_BOOT_CMD(
		mips,	6,	1,	do_calibrate_delay,
		"calculating BogoMips",
		"mips	- calculating BogoMips\n"
	  );

#endif

#ifdef CONFIG_CPUID

#define __stringify_1(x)	#x
#define __stringify(x)		__stringify_1(x)

#define read_cpuid(reg)						\
	({							\
	 unsigned int __val;					\
	 asm("mrc	p15, 0, %0, c0, c0, " __stringify(reg)	\
		 : "=r" (__val)					\
		 :						\
		 : "cc");					\
	 __val;							\
	 })

int do_cpuid(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	printf("cpu id:		%x\n", read_cpuid(0));
	printf("cache type:	%x\n", read_cpuid(1));
	printf("chip id:	%x\n", *(volatile unsigned int *)0xd4282c00);
	return 0;
}

U_BOOT_CMD(
		cpuid,	6,	1,	do_cpuid,
		"read cpu id",
		"cpuid - read cpu id\n"
	  );

int do_setid(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	*(volatile unsigned int *)0xD4282c08 = *(volatile unsigned int *)0xD4282c08 | 0x100;
	return 0;
}

U_BOOT_CMD(
		setid,	6,	1,	do_setid,
		"set SEL_MRVL_ID bit in MOHAWK_CPU_CONF register",
		"setid	-   set SEL_MRVL_ID bit in MOHAWK_CPU_CONF register\n"
	  );

int do_unsetid(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	*(volatile unsigned int *)0xD4282c08 = *(volatile unsigned int *)0xD4282c08 & ~0x100;
	return 0;
}

U_BOOT_CMD(
		unsetid,	6,	1,	do_unsetid,
		"unset SEL_MRVL_ID bit in MOHAWK_CPU_CONF register",
		"unsetid - unset SEL_MRVL_ID bit in MOHAWK_CPU_CONF register\n"
	  );
#endif

#ifdef CONFIG_SETVOL

int set_volt(u32 vol)
{
	/*
	 * ADJ0(GPIO2),ADJ1(GPIO3)    core vol
	 * =============================
	 *  0			0				1.14V
	 *  1               0				1.21V
	 *  0			1				1.28V
	 *  1			1				1.35V
	*/
	int adj0=GPIO(2);
	int adj1=GPIO(3);
	gpio_set_output(adj0);
	gpio_set_output(adj1);
	
	if(vol>=1350)
	{
		gpio_set_value(adj0, 1);
		gpio_set_value(adj1, 1);	
	}
	else if(vol>=1280)
	{
		gpio_set_value(adj0, 0);
		gpio_set_value(adj1, 1);		
	}
	else if(vol>=1210)
	{
		gpio_set_value(adj0, 1);
		gpio_set_value(adj1, 0);		
	}
	else
	{
		gpio_set_value(adj0, 0);
		gpio_set_value(adj1, 0);		
	}

	return 0;

	
}


int do_setvol(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	u32 vol;

	if ((argc < 1) || (argc > 2))
		return -1;

	if (argc == 1) {
		printf( "usage: setvol xxxx\n"
			"for mmp2 bonnell, xxxx can be 1140-1350, step 10\n");
		return 0;
	}

	vol = simple_strtoul(argv[1], NULL, 0);
	if (set_volt(vol) < 0)
		return -1;
	printf("voltage change successfully\n");
	return 0;
}

U_BOOT_CMD(
		setvol,	6,	1,	do_setvol,
		"set VCC_MAIN (1140mV - 1350mv)",
		"set VCC_MAIN (1140mV - 1350mv)"
	  );
#endif


#ifdef CONFIG_WFI

#define cp15_reg_write(primary_reg, secondary_reg, opcode, value) \
	__asm__ __volatile__( \
		"mcr	p15, 0, %0, " \
		#primary_reg ", " #secondary_reg ", " #opcode "\n" \
		: : "r" (value) : "memory");

void issue_wfi(void)
{
	volatile unsigned int temp;

	temp = reg_read(ICU_MOHAWK_GBL_IRQ_MSK);
	reg_write(ICU_MOHAWK_GBL_IRQ_MSK, temp|0x3);
	temp = 0x0;
	/* need delay here, or console will be messed up, no root cause now */
	udelay(100);
	/* Issue wfi here */
#if 1
	cp15_reg_write(c7,c0,4,temp);
#else
	/* not workable when system sleep */
	__asm__ __volatile__ ("\n\t\
		mov     r0, #0\n\t\
		mrc     p15, 0, r1, c1, c0, 0   @ read control register\n\t\
		mcr     p15, 0, r0, c7, c10, 4  @ Drain write buffer\n\t\
		bic     r2, r1, #1 << 12\n\t\
		mrs     r3, cpsr                @ Disable FIQs while Icache\n\t\
		orr     ip, r3, #0x00000040     @ is disabled\n\t\
		msr     cpsr_c, ip\n\t\
		mcr     p15, 0, r2, c1, c0, 0   @ Disable I cache\n\t\
		mcr     p15, 0, r0, c7, c0, 4   @ Wait for interrupt\n\t\
		mcr     p15, 0, r1, c1, c0, 0   @ Restore ICache enable\n\t\
		msr     cpsr_c, r3              @ Restore FIQ state\n\t"
	);
#endif
	reg_write(ICU_MOHAWK_GBL_IRQ_MSK, temp);
}

int do_wfi(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	disable_interrupts();
	issue_wfi();
	enable_interrupts();
	return 0;
}

U_BOOT_CMD(
               wfi,    6,      1,      do_wfi,
               "wfi	- Wait For Interrupt\n",
               " - Wait For Interrupt\n"
         );
#endif

static void hibernate(void)
{
	reg_write(0xd42A0030, reg_read(0xd42A0030) | 0x00000001);
	reg_write(0xd4282C08, reg_read(0xd4282C08) | 0x00000010);

	hibernate_init_sram(0xd1020000);
	hibernate_seq(0xd1020000, 0xd1022000, 0);
}

int do_hibernate(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	if (argc != 1) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	hibernate();

	return 0;
}

U_BOOT_CMD(
		hibernate,	6,	1,	do_hibernate,
		"hibernate	- put system into hibernate mode\n",
		"hibernate - put system into hibernate mode\n"
	  );

#if (!defined(CONFIG_MMP2_JASPER) && !defined(CONFIG_MMP2_FLINT) && !defined(CONFIG_MMP2_BROWNSTONE) && !defined(CONFIG_MMP2_G50))
void set_timer_match(ulong secs);

int do_timeout(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	ulong secs;

	if (argc != 2) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	secs = simple_strtoul(argv[1], NULL, 0);
	set_timer_match(secs);

	return 0;
}

U_BOOT_CMD(
		timeout,	6,	1,	do_timeout,
		"timeout	- set timeout for the system\n",
		"timeout secs - set timeout secs\n"
	  );
#endif

void releasecp(void)
{
	printf("release cp ...");
	/* put a jump instr to 96KB for CP in ddr loc 0x0 */
	reg_write(0x0, 0xe3a0fb60);
	/* CP vector table placed low */
	reg_write(0xd4282c04, reg_read(0xd4282c04) & ~0x1);
	/* release cp */
	reg_write(0xd4051020, reg_read(0xd4051020) | 0x1);   /* reset */
	reg_write(0xd4051020, reg_read(0xd4051020) & ~0x41); /* release */
	printf("done\n");
}

int do_releasecp(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	if (argc != 1) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	releasecp();

	return 0;
}

U_BOOT_CMD(
		releasecp,	6,	1,	do_releasecp,
		"releasecp	- release seagull image at 0x18000\n",
		"releasecp - release seagull image at 0x18000\n"
	  );

static void reset_pmu_reg(void)
{
#if 0
	printf("MOHAWK: Resetting baseline values \r\n");
	//reg_write(MCB_CONF,0xC0000);
	//reg_write(LCD_SPU_DMA_CTRL1,0x100000);
	//reg_write(GEU_CONFIG,0x800000);
	reg_write(APBC_TIMERS_CLK_RST,0x30);
	reg_write(APBC_TIMERS1_CLK_RST,0x30);
	reg_write(APBC_AIB_CLK_RST,0);
	//reg_write(APBC_RTC_CLK_RST,0x0);
	reg_write(APBC_SW_JTAG_CLK_RST,0x0);
	reg_write(APBC_GPIO_CLK_RST,0x0);
	reg_write(APBC_PWM0_CLK_RST,0x0);
	reg_write(APBC_PWM1_CLK_RST,0x0);
	reg_write(APBC_PWM2_CLK_RST,0x0);
	reg_write(APBC_PWM3_CLK_RST,0x0);
	reg_write(APBC_SSP0_CLK_RST,0x0);
	reg_write(APBC_SSP1_CLK_RST,0x0);
	reg_write(APBC_SSP2_CLK_RST,0x0);
	reg_write(APBC_TWSI_CLK_RST,0x0);
	reg_write(APBC_KPC_CLK_RST,0x0);
	reg_write(APBC_TB_ROTARY_CLK_RST,0x0);
	reg_write(APBC_ONEWIRE_CLK_RST,0x0);
	reg_write(APBC_ASFAR,0x0);
	reg_write(APBC_ASSAR,0x0);
	reg_write(0xd407000c,0x0);//COEL_APB_CLK_GATE
	reg_write(0xd4070014,0x0);//COEL_APB_RTU_CLKEN
	reg_write(SCCR,0x0);
#endif
}

static void power_mode_init_AP(void)
{
    /*Turn on max power switches */
    /* reg_write(PMUA_MOH_IDLE_CFG,
                 reg_read(PMUA_MOH_IDLE_CFG)|(PMUA_MOH_IDLE_CFG_MOH_PWR_SW_MSK|
                 PMUA_MOH_IDLE_CFG_MOH_L2_PWR_SW_MSK));*/
}

static void core_idle_AP(unsigned int idle)
{
	if ( idle )
		reg_write(PMUA_MOH_IDLE_CFG,
			reg_read(PMUA_MOH_IDLE_CFG)|0x2);
	else
		reg_write(PMUA_MOH_IDLE_CFG,
			reg_read(PMUA_MOH_IDLE_CFG) & ~0x2);
}

static void AP_PMU_timer_wakeup_Init(void)
{
	volatile unsigned int temp;

	// enable the wakeup 4 decoder
	temp = reg_read(PMUM_APCR);
	temp &= ~(1<<18);
	reg_write(PMUM_APCR,temp);

	//Writing Wakeup and Clock Resume Lines Mask Register
	//to disable the mask for RTC alarm and AP timer
	reg_write(PMUM_AWUCRM,0x00020110);
}

static void AP_PMU_Sleep_Init(int state)
{
	volatile unsigned int apcr;

	apcr = reg_read(PMUM_APCR);

	apcr &= ~(1<<29) & ~(1<<27) & ~(1<<26) & ~(1u<<31);

	switch (state) {
	case 5:
		apcr |= (1<<29);		/* set the SLPEN bit */
		AP_PMU_timer_wakeup_Init();
		/* fall through */
	case 4:
		apcr |= (1<<27) | (1<<26);	/* set DDRCORSD and APBSD */
		/* fall through */
	case 3:
		apcr |= (1u<<31);		/* set AXISDD bit */
		break;
	default:
		printf("only state 3, 4, 5 is supported\n");
		break;
	}

	apcr |= (1<<30) | (1<<28) | (1<<25) | (1<<19) | (1<<14);

	reg_write(PMUM_APCR,apcr);
}

static void initsleep(int state)
{
	ulong status;

	reset_pmu_reg();

	//graphics pwr dwn
	status = *(volatile unsigned int *)0xd42828d0;
	status &= (~0x10);
	*(volatile unsigned int *)0xd42828d0 = status;
	*(volatile unsigned int *)0xd42828d0 = 0x0;

	AP_PMU_Sleep_Init(state);
	power_mode_init_AP();
	//drampowerdownmodes_AP(WFI_SR|AUTO_WAKE_SR);
	core_idle_AP(1);
	//core_powerdown_AP(PWR_DWN);
}

int do_initsleep(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	ulong state;

	if (argc != 2) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	state = simple_strtoul(argv[1], NULL, 0);
	initsleep(state);

	return 0;
}

U_BOOT_CMD(
		initsleep,	6,	1,	do_initsleep,
		"initsleep	- initialize for system sleep mode\n",
		"initsleep - initialize for system sleep mode\n"
	  );

#if (!defined(CONFIG_MMP2_JASPER) && !defined(CONFIG_MMP2_FLINT) && !defined(CONFIG_MMP2_BROWNSTONE) && !defined(CONFIG_MMP2_G50))
void rtc_alarm_to_pmu(int alarm_secs);

int do_setalarm(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	ulong sec;

	if (argc != 2) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	sec = simple_strtoul(argv[1], NULL, 0);
	rtc_alarm_to_pmu(sec);

	return 0;
}

U_BOOT_CMD(
		setalarm,	6,	1,	do_setalarm,
		"setalarm	- set rtc alarm\n",
		"setalarm nsecs - set rtc alarm to nsecs\n"
	  );
#endif

#endif
