#include <nand.h>
#include <spi_flash.h>
#include <common.h>
#include <linux/types.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/nand.h>
#include <linux/mtd/partitions.h>
#include <asm/errno.h>
#include <asm/string.h>
#ifdef CONFIG_CPU_MONAHANS
#include <asm/arch/pxa-regs.h>
#else
#ifdef CONFIG_CPU_PXA688
#include <asm/arch/regs-pxa688.h>
#else
#include <asm/arch/regs-pxa168.h>
#endif
#endif

#ifdef CONFIG_CPU_PXA688
#include <asm/arch-pxa688/pxa3xx_bbm.h>
#include <asm/arch-pxa688/pxa3xx_nand.h>
#else
#include <asm/arch-pxa168/pxa3xx_bbm.h>
#include <asm/arch-pxa168/pxa3xx_nand.h>
#endif

int nand_curr_device = -1;
#ifdef CONFIG_CMD_NAND
nand_info_t nand_info[CONFIG_SYS_MAX_NAND_DEVICE];
#endif
#ifdef CONFIG_CMD_ONENAND
extern struct mtd_info onenand_mtd;
#endif

void nand_init()
{
	struct pxa3xx_nand_platform_data pxa_nandinfo;
	struct pxa3xx_nand *nand;
	int chip;

	pxa_nandinfo.mmio_base		= CONFIG_SYS_NAND_BASE;
	pxa_nandinfo.enable_arbiter 	= 1;
	pxa_nandinfo.RD_CNT_DEL		= 0;

	nand = pxa3xx_nand_probe(&pxa_nandinfo);
	if (!nand) {
		printf("pxa3xx-nand probe failed!!\n");
		return;
	}

	for (chip = 0; chip < CONFIG_SYS_MAX_NAND_DEVICE; chip ++) {
		if (nand->mtd[chip]) {
			memcpy(&(nand_info[chip]), nand->mtd[chip], sizeof(struct mtd_info));

			if (nand_curr_device < 0)
				nand_curr_device = chip;
		}
	}

	if (nand_curr_device < 0)
		printf("No NAND dev is found !!!\n\n");
}

struct yaffs_oob_desc {
	int     offset;
	int     size;
};
static struct yaffs_oob_desc yaffs_oob = {2, 38};
extern int mtd_direct_write_addr;
extern int callback_device_is_nand;
extern int is_yaffs;
int mtd_burn_callback(ulong load_addr, int *loop_offset, int force)
{
	struct mtd_info *mtd = NULL;
	struct mtd_oob_ops ops;
	struct erase_info instr = {
		.callback	= NULL,
	};
	size_t retlen;
	int ret, length, blocksize, is_nand;

#ifdef CONFIG_CMD_NAND
	if (callback_device_is_nand == 1) {
		is_nand = 1;
		mtd = &nand_info[nand_curr_device];
	}
#endif
#ifdef CONFIG_CMD_ONENAND
	if (callback_device_is_nand == 0) {
		is_nand = 0;
		mtd = &onenand_mtd;
	}
#endif

	if (!mtd)
		return 0;

	if (is_yaffs)
		blocksize = mtd->erasesize + (mtd->oobsize
			<< (mtd->erasesize_shift - mtd->writesize_shift));
	else
		blocksize = mtd->erasesize;

	if (*loop_offset >= blocksize || force){
		instr.addr = mtd_direct_write_addr;
		instr.len = mtd->erasesize;

		ret = mtd->erase(mtd, &instr);
		if (ret) {
			printf("\nerase one block at %x failed\n", instr.addr);
			return 1;
		}

		if (force) {
			length = *loop_offset;
			if (length < 0) {
				printf("Error, seems buffer overflow...\n");
				return 1;
			}

			if (is_nand || !is_yaffs)
				length = (length + mtd->writesize - 1) & ~mtd->writesize_mask;
			else {
				length = length + mtd->writesize + mtd->oobsize - 1;
				length /= (mtd->writesize + mtd->oobsize);
				length *= (mtd->writesize + mtd->oobsize);
			}

			memset(load_addr + *loop_offset, 0xff, length - *loop_offset);
		}
		else {
			if (is_nand)
				length = mtd->erasesize;
			else
				length = blocksize;
		}

		if (unlikely(is_yaffs)) {
			ops.datbuf = load_addr;
			ops.oobbuf = load_addr;
			ops.mode = MTD_OOB_PLACE;
			ops.len = length;
			ops.ooboffs = yaffs_oob.offset;
			ops.ooblen = yaffs_oob.size;
			ret = mtd->write_oob(mtd, mtd_direct_write_addr, &ops);
		}
		else
			ret = mtd->write(mtd, mtd_direct_write_addr, length,
					&retlen, (void *)load_addr);

		if (ret) {
			printf("\nWrite one block to %x failed!!!\n", instr.addr);
			return 1;
		}

		*loop_offset -= blocksize;
		if (*loop_offset > 0) {
			memcpy(load_addr, load_addr + blocksize, *loop_offset);
		}
		mtd_direct_write_addr += mtd->erasesize;
	}

	return 0;
}

#ifdef CONFIG_SPI_COPYBACK_NAND
#define BACK_NTIM_OF		0x1000
#define BACK_OBM_OF		0x30000
#define BACK_UBOOT_OF		0x40000
#define BACK_ZIMAGE_OF		0x100000
#define BURN_ZIMAGE_OF		0x400000
#define MAX_NTIM_SIZE		0x1000
#define MAX_OBM_SIZE		0xf000
#define MAX_UBOOT_SIZE		0x30000
#define MAX_ZIMAGE_SIZE		0x300000
#define NTIM_HEADER_1		0x30102
#define NTIM_HEADER_2		0x54494d48
#define BBT_INIT_COMMAND	"bbt init nand new"
int nand_update_callback(void)
{
	void *buf = (void *)0x500000;
	size_t retlen;
	int ret;
	unsigned int header_1, header_2;
	struct mtd_info *mtd;

	if (nand_curr_device != 0)
		return -1;

	mtd = &nand_info[0];
	mtd->read(mtd, 0, mtd->writesize, &retlen, buf);
	header_1 = *(unsigned int*)buf;
	header_2 = *((unsigned int*)buf + 1);
	if (header_1 == NTIM_HEADER_1 && header_2 == NTIM_HEADER_2) {
		return 1;
	}
	else {
		extern int not_updating;
		extern struct spi_flash *flash;
		struct erase_info instr = {
			.callback	= NULL,
		};

		printf("\n### begin to update nand from spi nor ###\n");
		run_command("nand device 1", 0);
		not_updating = 1;
		run_command(BBT_INIT_COMMAND, 0);
		run_command("nand device 0", 0);
		not_updating = 0;
		run_command(BBT_INIT_COMMAND, 0);
		not_updating = 1;
		instr.addr = mtd->erasesize;
		instr.len = instr.addr * 3;
		printf("\nerasing..");
		ret = mtd->erase(mtd, &instr);
		if (ret) {
			printf("erase 1-3 block failed!!!\n");
			goto UPDATE_FAILED;
		}

		instr.addr = BACK_ZIMAGE_OF;
		instr.len = MAX_ZIMAGE_SIZE * 2;
		ret = mtd->erase(mtd, &instr);
		if (ret) {
			printf("erase 0x100000-0x700000 failed!!!\n");
			goto UPDATE_FAILED;
		}

		printf("spi read 1..");
		ret = spi_flash_read(flash, BACK_NTIM_OF, MAX_NTIM_SIZE, buf);
		if (ret) {
			printf("read ntim failed\n");
			goto UPDATE_FAILED;
		}

		printf("2..");
		buf += MAX_NTIM_SIZE;
		ret = spi_flash_read(flash, BACK_OBM_OF, MAX_OBM_SIZE, buf);
		if (ret) {
			printf("read obm failed\n");
			goto UPDATE_FAILED;
		}

		printf("3..");
		buf += MAX_OBM_SIZE;
		ret = spi_flash_read(flash, BACK_UBOOT_OF, MAX_UBOOT_SIZE,
				buf);
		if (ret) {
			printf("read uboot failed\n");
			goto UPDATE_FAILED;
		}

		printf("4..");
		buf += MAX_UBOOT_SIZE;
		ret = spi_flash_read(flash, BACK_ZIMAGE_OF, MAX_ZIMAGE_SIZE,
				buf);
		if (ret) {
			printf("read zImage failed\n");
			goto UPDATE_FAILED;
		}

		printf("nand write 1..");
		buf = (void *)0x500000;
		ret = mtd->write(mtd, 0, MAX_NTIM_SIZE, &retlen, buf);
		if (ret) {
			printf("update ntim failed!!\n");
			goto UPDATE_FAILED;
		}

		printf("2..");
		buf += MAX_NTIM_SIZE;
		ret = mtd->write(mtd, mtd->erasesize, MAX_OBM_SIZE, &retlen,
				buf);
		if (ret) {
			printf("update obm failed!!\n");
			goto UPDATE_FAILED;
		}

		printf("3..");
		buf += MAX_OBM_SIZE;
		ret = mtd->write(mtd, mtd->erasesize * 2, MAX_UBOOT_SIZE,
				&retlen, buf);
		if (ret) {
			printf("update uboot failed!!\n");
			goto UPDATE_FAILED;
		}

		printf("4..");
		buf += MAX_UBOOT_SIZE;
		ret = mtd->write(mtd, BURN_ZIMAGE_OF, MAX_ZIMAGE_SIZE,
				&retlen, buf);
		if (ret) {
			printf("update zImage failed!!\n");
			goto UPDATE_FAILED;
		}

		printf("update done!!\n");
		return 0;
UPDATE_FAILED:

		printf("!!!update failed!!!\n");
		return -1;
	}
}
#endif
