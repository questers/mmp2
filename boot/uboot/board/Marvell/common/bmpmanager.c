#include <common.h>
#include <command.h>
#include <bmpmanager.h>
#include <fastboot/flash.h>

#define alignment_up(value, align) ((value+align-1) & (~ (align-1))) 
#define DEFAULT_BMPMNGR_PART "splash"

#define BMP_MNGR_MAGIC 0x626d6772 /*BMGR*/
#define BMP_MNGR_MAX_ELEMENTS 10
//store size is packed to 512bytes
typedef struct bmp_store{
	unsigned long magic;
	unsigned long count;
	bmp_t bmps[BMP_MNGR_MAX_ELEMENTS];//max 10 pictures is enough for us	
}bmp_store_t __attribute__((aligned (512)));
typedef struct bmpmanager_obj{
	u8 init;
	ptentry*   part;
	bmp_store_t store;
}bmpmanager_t ;

static bmpmanager_t mngr={0};


static void bmp_manager_dump(void){
	bmp_store_t* store = &mngr.store;
    int i;
    //dump all 
	printf("==============================\n");
	printf("found %u bmps\n",store->count);
	for(i=0;i<store->count;i++){
		printf("%s,start:0x%lx,size:0x%lx\n",store->bmps[i].name,
			store->bmps[i].start,
			store->bmps[i].size);
	}
	printf("==============================\n");

}

static int bmp_manager_reset(void){
    bmp_store_t* store = &mngr.store;
	
	ptentry* bmppart = mngr.part;
	if(!bmppart)	{
	    bmppart = flash_find_ptn(DEFAULT_BMPMNGR_PART);
	    mngr.part = bmppart;
    }
	if(!bmppart) {
    	return -1;
	}

    store->magic = BMP_MNGR_MAGIC;
    store->count = 0;
		

    //writeback 
    flash_write(bmppart,bmppart->length-alignment_up(sizeof(bmp_store_t),512),
        store,alignment_up(sizeof(bmp_store_t),512));

    return 0;
}
//bmp manager utilize flash driver to fetch bmp data,part is flash partition name
int bmp_manager_init(const char* part){
	bmp_store_t* store = &mngr.store;
	int i;
	ptentry* bmppart = flash_find_ptn(part?part:DEFAULT_BMPMNGR_PART);
	if(!bmppart) return -1;

	mngr.part = bmppart;
	//read from last location
	flash_read(bmppart,
		bmppart->length-alignment_up(sizeof(bmp_store_t),512),
		(void*)store,512);
	if(store->magic!=BMP_MNGR_MAGIC){ /* skip check. -guang */
		int ret;
		int bmpsize=0;
		char head[512];
		//never inited
		store->magic = BMP_MNGR_MAGIC;
		store->count = 0;

		//check start already store one splash bmp
		
		ret = flash_read(bmppart,0,head,512);
		if(!ret&&(head[0]==0x42)&&(head[1]==0x4d))
		{
			bmpsize=(head[5]<<24)|(head[4]<<16)|(head[3]<<8)|head[2];
			store->count++;
			store->bmps[0].start = 0;
			store->bmps[0].size  = alignment_up(bmpsize,512);
			memset(store->bmps[0].name,0,32);
			strcpy(store->bmps[0].name,"splash");

		}
		//writeback 
		flash_write(bmppart,bmppart->length-alignment_up(sizeof(bmp_store_t),512),
			store,alignment_up(sizeof(bmp_store_t),512));
	}
	if(store->count>BMP_MNGR_MAX_ELEMENTS) store->count=10;
	//safe bmpname handle
    for(i=0;i<store->count;i++){
        store->bmps[i].name[32-1] = '\0';
    }
	
	mngr.init++;

	return 0;
}
int bmp_manager_getbmp(const char* name,bmp_t* bmp){
	bmp_store_t* store = &mngr.store;
	int i;	
	for(i=0;i<store->count&&i<BMP_MNGR_MAX_ELEMENTS;i++){
		if(!strcmp(store->bmps[i].name,name)){
			if(bmp)	memcpy(bmp,&store->bmps[i],sizeof(bmp_t));
			return 0;
		}
			
	}

	return -1;
	
}

int bmp_manager_readbmp(const char* name,void* data,unsigned long size){
	ptentry* entry;
	bmp_t bmp;
	int ret;
	if(!mngr.init)
		bmp_manager_init(0);
	entry = mngr.part;
	if(!entry||!name) return -1;
	ret = bmp_manager_getbmp(name,&bmp);
	if(ret<0){
	    if(!strcmp(name,"splash"))
	        ret = bmp_manager_getbmp("bmp.splash",&bmp);
	    if(ret<0)
	        return -1;
    }
	return flash_read(entry,bmp.start,data,(size>bmp.size)?bmp.size:size);
}

int bmp_manager_writebmp(const char* name,void* data,unsigned long size){
	bmp_store_t* store = &mngr.store;
	ptentry* entry;
	bmp_t bmp;
	int i;
	if(!mngr.init)
		bmp_manager_init(0);	
	entry = mngr.part;		
	if(!entry||!name) return -1;;
	if(!bmp_manager_getbmp(name,&bmp)){
		//update ,some complicated but change to be simple
		//we used more higher memory space to store latter bmp data
		//0x1500000 normally as initrd memory
		int location=0;
		unsigned long used,space;
		used=0;
		for(i=0;i<store->count;i++){
			if(!strcmp(store->bmps[i].name,name)) break;
			used+=store->bmps[i].size;
		}
		location = i;
		if(store->count>(location+1)){
			//read latter bmp to temp memory
			unsigned long movestart = store->bmps[location+1].start;
			unsigned long movesize=0;
			unsigned long mystart = bmp.start;
			unsigned long mysize = bmp.size;
			for(i=location+1;i<store->count;i++){
				movesize+=store->bmps[i].size;
			}
			//test space is enough
			space = entry->length-used-movesize-alignment_up(sizeof(bmp_store_t),512);
			size = alignment_up(size,512);
			if(space<size) return -1;

			//start to move
			//FIXME: hard code for higher memory 
			flash_read(entry,movestart,(void*)0x1500000,movesize);

			//update store
			store->count--;
			memmove(&store->bmps[location],&store->bmps[location+1],store->count-location);
			for(i=location;i<store->count;i++){
				store->bmps[i].start-=mysize;
			}

			//write data
			flash_write(entry,mystart,(void*)0x1500000,movesize);
			flash_write(entry,mystart+movesize,data,size);

			//update store for me
			memset(store->bmps[store->count].name,0,32);		
			strncpy(store->bmps[store->count].name,name,31);
			store->bmps[store->count].start = movestart+movesize-mysize;
			store->bmps[store->count].size = size;
			store->count++;
			//update store
			flash_write(entry,entry->length-alignment_up(sizeof(bmp_store_t),512),
				store,alignment_up(sizeof(bmp_store_t),512));

		
			
		}else{//the last one
			space=entry->length-used-alignment_up(sizeof(bmp_store_t),512);
			size = alignment_up(size,512);
			if(space<size) return -1;
			
			//write bmp data
			flash_write(entry,used,data,size);
			//update new size
			store->bmps[store->count-1].size = size;
			
			//writeback 
			flash_write(entry,entry->length-alignment_up(sizeof(bmp_store_t),512),
				store,alignment_up(sizeof(bmp_store_t),512));
			
		}
		
		
	}else {
		unsigned long used,space;
		int i;
		if(store->count>=BMP_MNGR_MAX_ELEMENTS) return -1;
		//calcute left space
		for(i=0,used=0;i<store->count;i++){
			used+=store->bmps[i].size;
		}
		space = entry->length-used-alignment_up(sizeof(bmp_store_t),512);
		size = alignment_up(size,512);
		if(space<size) return -1;//no enough space

		//write bmp data
		flash_write(entry,used,data,size);
		//update manager
		memset(store->bmps[store->count].name,0,32);		
		strncpy(store->bmps[store->count].name,name,31);
		store->bmps[store->count].start = used;
		store->bmps[store->count].size = size;
		store->count++;

		//writeback 
		flash_write(entry,entry->length-alignment_up(sizeof(bmp_store_t),512),
			store,alignment_up(sizeof(bmp_store_t),512));
		
		
	}

	return 0;
}


static int do_bmpmanager(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
    if(argc<2){
        bmp_manager_dump();
    }else {
        //allow short name
    	if (!strcmp(argv[1],"reset")) {
    		bmp_manager_reset();
    	}else if(!strcmp(argv[1],"read")&&argc>=4){
    		char* bmpname = argv[2];
    		unsigned long addr,size;
			addr = simple_strtoul(argv[3], NULL, 0);
			size = 0xA00000;
			if(argc>4)
				size = simple_strtoul(argv[4], NULL, 0);
			printf("reading %s to %#x ... %d\n",bmpname,addr,bmp_manager_readbmp(bmpname,addr,size));			
    	}else if(!strcmp(argv[1],"write")&&argc>=5){
    		char* bmpname = argv[2];
    		unsigned long addr,size;
			addr = simple_strtoul(argv[3], NULL, 0);
			size = simple_strtoul(argv[4], NULL, 0);
			size = alignment_up(size,512);
			printf("writing %s from %#x ... %d\n",bmpname,addr,bmp_manager_writebmp(bmpname,addr,size));			
    	}else {
    	    printf ("Usage:\n%s\n", cmdtp->usage);
    	    return 1;
    	}
	}
	return 0;
}

U_BOOT_CMD(
	bmpmngr,	5,	1,	do_bmpmanager,
	"bmpmngr [info|reset|write|read] <bmpname> <addr> <size>     - manipulate bmp managed\n",
	""
);

