#include <common.h>
//
//partition entry mapping format
//name  start sector	sector number	   flags
//
//All entries sector number zero is invalid and termination
//Generally eMMC boot partition is 2*1024K=2MB
//We only utilized first boot partition 1024KB = 2048blocks
//
//part name   start	size		flags
//
const struct ptentry boot_block[] =
{
	{"ntim",0x0,	0x4000,	1&FLASH_PARTITION_MASK},	//0x20blks = 16384B=16KB
	{"wtm",	0x4000,	0x2c000,1&FLASH_PARTITION_MASK},	//0x160blks=180224B=176KB
	{"obm",	0x30000,0x30000,1&FLASH_PARTITION_MASK},	//0x180blks=196608B=192KB
	{"boot",0x60000,0x80000,1&FLASH_PARTITION_MASK},//0x400blks=524288B=512KB
	{"env",	0xE0000,0x8000,	1&FLASH_PARTITION_MASK},	//Be sure to keep consistent with config file
	{"bootreserved",0xE8000,0x18000,1&FLASH_PARTITION_MASK},	//for 1MB space,about 0xC0 blocks reserved	
	{"nwob",0x0,0xE0000,1&FLASH_PARTITION_MASK},	//comboed ntim+wtm+obm+boot
	{"bcb",0x0,0x100000,1&FLASH_PARTITION_MASK},	//comboed ntim+wtm+obm+boot+env+reserved
	{"null",0,0,0},
};


//
//IMPORTANT,if u want to use these hardcode partition table,please recalculate the offset/size value ,currently it mmc blocks based.
//
#if 0

//
//SEM02G efi mapping
//
#if 1
const struct ptentry sem02g_user[] =
{
	{"pgpt",0x0,0x22,0},
	{"agpt",0x3a4fdf,0x21,0},
	{"kernel",0x4c00,0x2000,0},
	{"system",0x8c00,0x4b000,0},
	{"userdata",0x53c00,0x2e7000,0},
	{"splash",0x33ac00,0x1000,0},
	{"custom",0x33bc00,0x18c00,0},
	{"recovery",0x354800,0x4000,0}, 
	{"misc",0x359800,0x400,0},
	{"cache",0x359c00,0x4b000,0},
	{"null",0,0,0},
};
#else // for internal storage test
const struct ptentry sem02g_user[] =
{
	{"pgpt",0x0,0x22,0},
	{"agpt",0x3a4fdf,0x21,0},
	{"kernel",0x4c00,0x2000,0},
	{"system",0x8c00,0x4b000,0},
	{"userdata",0x53c00,0x4b000,0},
	{"splash",0x9ec00,0x1000,0},
	{"custom",0x9fc00,0x18c00,0},
	{"misc",0xbd800,0x400,0},
	{"cache",0xbdc00,0x4b000,0},
	{"recovery",0xb8800,0x4000,0},	
	{"storage",0x108c00,0x29c000,0},
	{"null",0,0,0},
};
#endif

//
//SEM04G efi mapping
//
const struct ptentry sem04g_user[] =
{
	{"pgpt",0x0,0x22,0},
	{"agpt",0x75efdf,0x21,0},
	{"kernel",0x4c00,0x2000,0},
	{"system",0x8c00,0x4b000,0},
	{"userdata",0x53c00,0x2e7000,0},
	{"splash",0x33ac00,0x1000,0},
	{"custom",0x33bc00,0x18c00,0},
	{"recovery",0x354800,0x4000,0}, 
	{"misc",0x359800,0x400,0},
	{"cache",0x359c00,0x4b000,0},
	{"storage",0x3a4c00,0x3ba000,0},
	{"null",0,0,0},
};


//
//SEM08G efi mapping
//
const struct ptentry sem08g_user[] =
{
	{"pgpt",0x0,0x22,0},
	{"agpt",0xecafdf,0x21,0},
	{"kernel",0x4c00,0x2000,0},
	{"system",0x8c00,0x4b000,0},
	{"userdata",0x53c00,0x2e7000,0},
	{"splash",0x33ac00,0x1000,0},
	{"custom",0x33bc00,0x18c00,0},
	{"recovery",0x354800,0x4000,0}, 
	{"misc",0x359800,0x400,0},
	{"cache",0x359c00,0x4b000,0},
	{"storage",0x3a4c00,0xb26000,0},
	{"null",0,0,0},
};

//
//SEM016G efi mapping
//
const struct ptentry sem016g_user[] =
{
	{"pgpt",0x0,0x22,0},
	{"agpt",0x1da8fdf,0x21,0},
	{"kernel",0x4c00,0x2000,0},
	{"system",0x8c00,0x4b000,0},
	{"userdata",0x53c00,0x2e7000,0},
	{"splash",0x33ac00,0x1000,0},
	{"custom",0x33bc00,0x18c00,0},
	{"misc",0x359800,0x400,0},
	{"cache",0x359c00,0x4b000,0},
	{"recovery",0x354800,0x4000,0},	
	{"storage",0x3a4c00,0x1a04000,0},
	{"null",0,0,0},
};
#endif

