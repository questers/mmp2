/*
 * Boot support
 */
#include <common.h>
#include <watchdog.h>
#include <command.h>
#include <image.h>
#include <malloc.h>
#include <environment.h>
#include <linux/ctype.h>
#include <nand.h>
#include <onenand_uboot.h>
#include <mmc.h>
#include <part.h>
#include <fat.h>
#include <fastboot/flash.h>

extern int do_reset (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]);
DECLARE_GLOBAL_DATA_PTR;
ulong load_addr = CONFIG_SYS_LOAD_ADDR;	/* Default Load Address */
static bootm_headers_t images;		/* pointers to os/initrd/fdt images */

typedef int boot_os_fn (int flag, int argc, char *argv[],
			bootm_headers_t *images); /* pointers to os/initrd/fdt */

#define CONFIG_BOOTM_LINUX 1
#ifdef CONFIG_BOOTM_LINUX
extern boot_os_fn do_bootm_linux;
#endif

boot_os_fn * boot_os[] = {
#ifdef CONFIG_BOOTM_LINUX
	[IH_OS_LINUX] = do_bootm_linux,
#endif
#ifdef CONFIG_BOOTM_NETBSD
	[IH_OS_NETBSD] = do_bootm_netbsd,
#endif
#ifdef CONFIG_LYNXKDI
	[IH_OS_LYNXOS] = do_bootm_lynxkdi,
#endif
#ifdef CONFIG_BOOTM_RTEMS
	[IH_OS_RTEMS] = do_bootm_rtems,
#endif
#if defined(CONFIG_CMD_ELF)
	[IH_OS_VXWORKS] = do_bootm_vxworks,
	[IH_OS_QNX] = do_bootm_qnxelf,
#endif
#ifdef CONFIG_INTEGRITY
	[IH_OS_INTEGRITY] = do_bootm_integrity,
#endif
};

/*******************************************************************/
/* bootz - boot zImage in memory */
/*******************************************************************/
static int relocated = 0;

int do_bootz (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	boot_os_fn	*boot_fn;
	int		is_zImage, is_autoboot;

	if(argc < 2)
		images.ep = CONFIG_SYS_LOAD_ADDR;
	else {
		if (isxdigit(*(argv[1])))
			images.ep = simple_strtoul(argv[1], NULL, 16);
		else {
			printf("Input address is not digital, ERROR!!\n");
			return -1;
		}
	}

	is_autoboot = (strncmp(getenv("autoboot"), "boot", 4) == 0) ? 1 : 0;

	/* Autoboot sequence is NAND, OneNAND .. */
	if (is_autoboot) {
#if !defined (BOOT_FROM_EMMC)
#ifdef CONFIG_CMD_NAND
		if (nand_curr_device >= 0) {
			/* NAND Device Exist, try to boot from NAND */
			printf("read zImage from NAND\n");
			run_command(CONFIG_NANDBOOT, 0);
#ifdef CONFIG_CMD_ONENAND
		} else if (onenand_mtd.size > 0) {
			/* OneNAND Device Exist, try to boot from OneNAND */
			printf("read zImage from OneNAND\n");
			run_command(CONFIG_ONENANDBOOT, 0);
#endif
		} else
#endif
			printf("No flash device, fail to AUTOBOOT!!!\n\n");
#else
		printf("read zImage from eMMC\n");
#ifdef CONFIG_CMD_MMC
		if (find_mmc_device(0)){
			run_command("mmc sw_part 0", 0);
			run_command(getenv("mmcboot"),0);
		}
		else
#endif
			printf("No eMMC device, fail to AUTOBOOT!!!\n\n");
#endif
	}

	is_zImage = (((ulong *)images.ep)[9] == 0x016f2818) ? 1 : 0;
	/* AUTOBOOT support, check if the sepecified addr contain valid zImage */
	if (is_autoboot && !is_zImage) {
		printf("The address[%p] specified contains no valid zImage.\n AutoBOOT failed!!!\n\n",
			images.ep);
		#ifdef CONFIG_CMD_MMC
		if (!strcmp(CONFIG_BOOT_PRIMARY,getenv(CONFIG_BOOT_PHASE))) 
		{
			#ifdef CONFIG_CMD_FASTBOOT
			char buffer[128];
			ptentry *recovery = flash_find_ptn("recovery");
			//set mmcboot environment per mmc device
			if(recovery)
			{
				sprintf(buffer,"mmc read 0x%x 0x%x 0x%x",recovery->start/512,recovery->length/512,CONFIG_SYS_LOAD_ADDR);
				setenv("mmcboot",buffer);
			}
			else
			#endif
			{
				//fall back
				setenv("mmcboot",CONFIG_MMCBOOT_RECOVERY);
			}
			setenv(CONFIG_BOOT_PHASE,CONFIG_BOOT_ALTERNATE);
			
			printf("switch to recovery mode\n");
			run_command(CONFIG_BOOTCOMMAND, 0);					
		}
		if(!strcmp(CONFIG_BOOT_ALTERNATE,getenv(CONFIG_BOOT_PHASE)))
		{
			int ret;
			
			//swtich to graphics mode
			#ifdef CONFIG_CFB_CONSOLE			
			extern int video_console_refresh(void);
			//switch console to lcd 	
			video_console_refresh();
			console_assign(stdout, "vga");
			#endif

			printf("no valid images on normal and recovery booting sectors,trying auto updater ...\n");
			
			udelay(1000*2000);
			ret = run_command("autoupdater",0);
			if(0==ret)
			{			
				#ifdef CONFIG_CMD_FASTBOOT
				char buffer[128];
				ptentry *kernel = flash_find_ptn("kernel");
				//set mmcboot environment per mmc device
				if(kernel)
				{
					sprintf(buffer,"mmc read 0x%x 0x%x 0x%x",kernel->start/512,kernel->length/512,CONFIG_SYS_LOAD_ADDR);
					setenv("mmcboot",buffer);
				}
				else
				#endif
				{
					//fall back
					setenv("mmcboot",CONFIG_MMCBOOT);
				}
				
				#ifdef CONFIG_CFB_CONSOLE			
				//switch console to serial 	
				console_assign(stdout, "serial");
				#endif
				lcd_clear();
				lcd_draw_default_bmp();
				setenv(CONFIG_BOOT_PHASE,CONFIG_BOOT_PRIMARY);
				run_command(CONFIG_BOOTCOMMAND, 0);		
			}

			//fall back to android fastboot mode
			printf("fallback to fastboot mode\n");			
			run_command("fb mmc",0);
		}
		#endif

		return 0;
		
	}

	images.os.os = IH_OS_LINUX;

#ifndef CONFIG_SYS_ARM_WITHOUT_RELOC
	/* relocate boot function table */
	if (!relocated) {
		int i;
		for (i = 0; i < ARRAY_SIZE(boot_os); i++)
			boot_os[i] += gd->reloc_off;
		relocated = 1;
	}
#endif

	printf("Ready to boot %s from %lx\n\n", is_zImage ? "zImage" : "Image", images.ep);
	boot_fn = boot_os[images.os.os];
	boot_fn(0, argc, argv, &images);

	show_boot_progress (-9);
#ifdef DEBUG
	puts ("\n## Control returned to monitor - resetting...\n");
#endif
	do_reset (cmdtp, flag, argc, argv);

	return 1;
}

U_BOOT_CMD(
        bootz,   CONFIG_SYS_MAXARGS,      1,      do_bootz,
        "bootz   - boot zImge from memory\n",          
        "[addr]"
);



