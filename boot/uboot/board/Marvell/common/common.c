#include <asm/arch/common.h>

void complete(struct completion *completion)
{
	completion->done = 1;
}

inline unsigned long msecs_to_jiffies(int time)
{
	return time * HZ / USEC_PER_SEC;
}
