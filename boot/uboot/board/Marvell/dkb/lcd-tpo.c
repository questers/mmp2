/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  publishhed by the Free Software Foundation.
 */
#include <mfp.h>
#include <common.h>
#include <pxa910fb.h>
#include <asm/io.h>
#include <i2c.h>

extern int pxa910fb_init(struct pxa910fb_mach_info *mi);
static unsigned long tpo_lcd_gpio_pin_config[] = {
	MFP_CFG(GPIO104, AF0),	//Data out
	MFP_CFG(GPIO106, AF0),	//Reset
	MFP_CFG(GPIO107, AF0),	//CS
	MFP_CFG(GPIO108, AF0),	//SCLK
};

#define ARRAY_AND_SIZE(x)	(x), ARRAY_SIZE(x)

static u16 tpo_spi_cmdon[] = {
	0x0801,
	0x0800,
	0x0200,
	0x0304,
	0x040e,
	0x0903,
	0x0b18,
	0x0c53,
	0x0d01,
	0x0ee0,
	0x0f01,
	0x1058,
	0x201e,
	0x210a,
	0x220a,
	0x231e,
	0x2400,
	0x2532,
	0x2600,
	0x27ac,
	0x2904,
	0x2aa2,
	0x2b45,
	0x2c45,
	0x2d15,
	0x2e5a,
	0x2fff,
	0x306b,
	0x310d,
	0x3248,
	0x3382,
	0x34bd,
	0x35e7,
	0x3618,
	0x3794,
	0x3801,
	0x395d,
	0x3aae,
	0x3bff,
	0x07c9,			//auto power on
};

static u16 tpo_spi_cmdoff[] = {
	0x07d9,			//auto power off
};

/*
	 SPI emulated by GPIO for TPO LCD
	 CS GPIO107
	 DATA GPIO104
	 CLK  GPIO108
 */
static int gpio_spi_send_tpolcd(u16 * dat, u32 size, u32 bit_len)
{
	u32 bit_cnt = bit_len;
	u16 *val = dat;
	unsigned int cs_gpio = mfp_to_gpio(MFP_PIN_GPIO107);
	unsigned int clk_gpio = mfp_to_gpio(MFP_PIN_GPIO108);
	unsigned int d_gpio = mfp_to_gpio(MFP_PIN_GPIO104);

	gpio_set_output(clk_gpio);
	gpio_set_output(d_gpio);
	gpio_set_output(cs_gpio);
	//printf("GPIO 96-127 Direction %08x \n", readl(0xd401910c));

	gpio_set_value(cs_gpio, 1);
	udelay(20);

	for (; size; size--) {
		bit_cnt = bit_len;
		gpio_set_value(cs_gpio, 0);
		udelay(20);
		while (bit_cnt) {
			bit_cnt--;
			if ((*val >> bit_cnt) & 1) {
				gpio_set_value(d_gpio, 1);
			} else {
				gpio_set_value(d_gpio, 0);
			}

			udelay(20);
			gpio_set_value(clk_gpio, 0);

			udelay(20);
			gpio_set_value(clk_gpio, 1);
			udelay(20);
		}
		val++;
		udelay(20);

		gpio_set_value(cs_gpio, 1);
	}

	return 0;
}

static int tpo_lcd_power(struct pxa910fb_info *fbi,
			 unsigned int spi_gpio_cs,
			 unsigned int spi_gpio_reset, int on)
{
	int err = 0;

	/* turn on backlight on ttc dkb */
	gpio_set_output(spi_gpio_reset);

	if (on) {
		if (spi_gpio_reset != -1) {
			gpio_set_value(spi_gpio_reset, 0);
			udelay(100 * 1000);
			gpio_set_value(spi_gpio_reset, 1);
			udelay(100 * 1000);
		}
		err =
		    gpio_spi_send_tpolcd(tpo_spi_cmdon,
					 ARRAY_SIZE(tpo_spi_cmdon), 16);

	} else {
		err =
		    gpio_spi_send_tpolcd(tpo_spi_cmdoff,
					 ARRAY_SIZE(tpo_spi_cmdoff), 16);
		if (err) {
			return -1;
		}
		gpio_set_value(spi_gpio_reset, 0);
	}
	return err;
}

static struct fb_videomode video_modes[] = {
	/* lpj032l001b HVGA mode info */
	[0] = {
	       .pixclock = 100000,
	       .refresh = 60,
	       .xres = 320,
	       .yres = 480,
	       .hsync_len = 10,
	       .left_margin = 15,
	       .right_margin = 10,
	       .vsync_len = 2,
	       .upper_margin = 4,
	       .lower_margin = 2,
	       .sync = 0,
	       },
};

/* SPI Control Register. */
#define     CFG_SCLKCNT(div)                    (div<<24)	/* 0xFF~0x2 */
#define     CFG_RXBITS(rx)                      ((rx - 1)<<16)	/* 0x1F~0x1 */
#define     CFG_TXBITS(tx)                      ((tx - 1)<<8)	/* 0x1F~0x1, 0x1: 2bits ... 0x1F: 32bits */
#define     CFG_SPI_ENA(spi)                    (spi<<3)
#define     CFG_SPI_SEL(spi)                    (spi<<2)	/* 1: port1; 0: port0 */
#define     CFG_SPI_3W4WB(wire)                 (wire<<1)	/* 1: 3-wire; 0: 4-wire */

static struct pxa910fb_mach_info dkb_tpo_lcd_info = {
	.id = "Base",
	.modes = video_modes,
	.num_modes = ARRAY_SIZE(video_modes),
	.pix_fmt = PIX_FMT_RGB565,
	.io_pin_allocation_mode = PIN_MODE_DUMB_18_SPI,
	.dumb_mode = DUMB_MODE_RGB666,
	.active = 1,
	.spi_ctrl =
	    CFG_SCLKCNT(16) | CFG_TXBITS(16) | CFG_SPI_SEL(1) |
	    CFG_SPI_3W4WB(1) | CFG_SPI_ENA(1),
	.spi_gpio_cs = -1,
	.spi_gpio_reset = mfp_to_gpio(MFP_PIN_GPIO106),
	.panel_rbswap = 1,
	.pxa910fb_lcd_power = tpo_lcd_power,
	.invert_pixclock = 1,
	.max_fb_size = 1280 * 720 * 4,
};

static void rect_fill(unsigned short *addr, int left, int up, int right,
		      int down, int width, unsigned short color)
{
	int i, j;
	for (j = up; j < down; j++)
		for (i = left; i < right; i++)
			*(addr + j * width + i) = color;
}

void test_panel(int stop)
{
	int w = dkb_tpo_lcd_info.modes->xres, h =
	    dkb_tpo_lcd_info.modes->yres;
	int x = w / 8, y = h / 8;
	printf("panel test: white background, test RGB\r\n");
	do {
		memset((unsigned short *) DEFAULT_FB_BASE, 0xff,
		       w * h * 2);
		udelay(50 * 1000);
		rect_fill((unsigned short *) DEFAULT_FB_BASE, x, y, w - x,
			  h - y, w, 0x1f);
		udelay(50 * 1000);
		rect_fill((unsigned short *) DEFAULT_FB_BASE, x * 2, y * 2,
			  w - x * 2, h - y * 2, w, 0x7e0);
		udelay(50 * 1000);
		rect_fill((unsigned short *) DEFAULT_FB_BASE, x * 3, y * 3,
			  w - x * 3, h - y * 3, w, 0xf800);
		udelay(50 * 1000);
	} while (stop);

}

#if defined(CONFIG_PXA910_FB)
void *lcd_init(void)
{
	void *ret;
	u8 data;

	/* turn on BOOST_EN to enable lcd backlight */
	/* allow WLED strings you need to allow reference voltage */
	data = 0;
	i2c_read(0x11, 0x15, 1, &data, 1);
	if (!(data & (1 << 1))) {
		data |= (1 << 1);
		i2c_write(0x11, 0x15, 1, &data, 1);
	}

	/* allow WLED strings you need to allow reference frequency */
	i2c_read(0x11, 0x16, 1, &data, 1);
	if (!(data & (1 << 4))) {
		data |= (1 << 4);
		i2c_write(0x11, 0x16, 1, &data, 1);
	}

	data = 0xff;
	i2c_write(0x11, 2, 1, &data, 1);
	i2c_write(0x11, 4, 1, &data, 1);
	data = 0x31;		//0x9;
	i2c_write(0x11, 3, 1, &data, 1);
	i2c_write(0x11, 5, 1, &data, 1);

	/* Initialize LCD Controller */
	ret = (void *) pxa910fb_init(&dkb_tpo_lcd_info);

	//test_panel(0);

	return ret;

}
#endif

#if defined(CONFIG_VIDEO) || defined(CONFIG_CFB_CONSOLE)
#include <stdio_dev.h>
#include <video_fb.h>
/*
 * The Graphic Device
 */
static GraphicDevice ctfb;

void *video_hw_init(void)
{
	struct pxa910fb_info *fbi = (struct pxa910fb_info *) lcd_init();
	struct fb_var_screeninfo *var = &fbi->var;
	unsigned long t1, hsynch, vsynch;

	ctfb.winSizeX = var->xres;
	ctfb.winSizeY = var->yres;

	/* calculate hsynch and vsynch freq (info only) */
	t1 = (var->left_margin + var->xres +
	      var->right_margin + var->hsync_len) / 8;
	t1 *= 8;
	t1 *= var->pixclock;
	t1 /= 1000;
	hsynch = 1000000000L / t1;
	t1 *= (var->upper_margin + var->yres +
	       var->lower_margin + var->vsync_len);
	vsynch = 1000000000L / t1;

	/* fill in Graphic device struct */
	sprintf(ctfb.modeIdent, "%dx%dx%d %ldkHz %ldHz", ctfb.winSizeX,
		ctfb.winSizeY, var->bits_per_pixel, (hsynch / 1000),
		vsynch);

	ctfb.frameAdrs = (unsigned int) fbi->fb_start;
	ctfb.plnSizeX = ctfb.winSizeX;
	ctfb.plnSizeY = ctfb.winSizeY;

	ctfb.gdfBytesPP = 2;
	ctfb.gdfIndex = GDF_16BIT_565RGB;

	ctfb.isaBase = 0x9000000;
	ctfb.pciBase = (unsigned int) fbi->fb_start;
	ctfb.memSize = fbi->fb_size;	//320*480*2;

	/* Cursor Start Address */
	ctfb.dprBase = (unsigned int) fbi->fb_start + (ctfb.winSizeX * ctfb.winSizeY * ctfb.gdfBytesPP);	//0;
	if ((ctfb.dprBase & 0x0fff) != 0) {
		/* allign it */
		ctfb.dprBase &= 0xfffff000;
		ctfb.dprBase += 0x00001000;
	}
	ctfb.vprBase = (unsigned int) fbi->fb_start;
	ctfb.cprBase = (unsigned int) fbi->fb_start;

	return &ctfb;
}
#endif				/* defined(CONFIG_VIDEO) || defined(CONFIG_CFB_CONSOLE) */
