#ifndef _BATTERY_H_
#define _BATTERY_H_

#ifdef CONFIG_BATTERY_MONITOR

//bus:i2c bus number (0 based),dc_in: gpio of dc detect
int battery_init(int bus,int dc_in);

//return battery voltage mv
int battery_voltage(void);

//return battery capacity
int battery_capacity(void);

//return dc in state :
//1: dc in ,0:no dc in
int battery_dc_in(void);


//lvl: 0:emergency,1:low,currently only 0 supported
u8* battery_bmp(int lvl);

#endif

#endif 

