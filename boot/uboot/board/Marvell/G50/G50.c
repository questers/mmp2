/*
 * (C) Copyright 2005
 * Marvell Semiconductors Ltd. <www.marvell.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
#include <common.h>
#include <asm/io.h>
#include <asm/arch/common.h>
#include <mfp.h>
#include <usb/mmp_udc.h>
#include <asm/arch/mfp-pxa688.h>
#include <mmc.h>
#include <fastboot/flash.h>
#ifdef CONFIG_SUPPORT_TOUCHKEY
#include "touchkey.h"
#endif
#include "battery.h"
#include "lcd-auo.h"
DECLARE_GLOBAL_DATA_PTR;

static int wtm_read_profile(void);
static int wtm_read_stepping(void);
static void wtm_dump_info(void);

#ifdef CONFIG_TRUST_BOOT
static unsigned int mrvl_mag;
static unsigned int mrvl_validation_flag;
static unsigned int mrvl_residue_flag;
static unsigned int R_uboot = 0;
#endif
static int get_board_revision(void);

static mfp_cfg_t jasper_pin_config[] __initdata = {
	/* UART1:88W8787 (bluetooth) */
	GPIO29_UART1_RXD,
	GPIO30_UART1_TXD,
	GPIO31_UART1_CTS,
	GPIO32_UART1_RTS,

	/* UART4:GPS */
	GPIO117_UART4_RXD,
	GPIO118_UART4_TXD,
	GPIO11_GPIO11,     /* GPS_PEN */
	GPIO135_GPIO135,     /* GPS_RESETn */
	GPIO122_GPIO122,     /* GPS_ON */
	GPIO138_GPIO138,     /* GPS_RE_FLASH */

	/* UART3:debug */
	GPIO51_UART3_RXD,
	GPIO52_UART3_TXD,

	/*CCIC1 - camera */
	GPIO20_CAM_HSYNC,		
	GPIO21_CAM_VSYNC,		
	GPIO22_CAM_MCLK,	
	GPIO23_CAM_PCLK,	
	GPIO12_CCIC_IN7,	
	GPIO13_CCIC_IN6,	
	GPIO14_CCIC_IN5,	
	GPIO15_CCIC_IN4,
	GPIO16_CCIC_IN3,	
	GPIO17_CCIC_IN2,	
	GPIO18_CCIC_IN1,	
	GPIO19_CCIC_IN0,
	GPIO53_GPIO53,	      /* low sensor power enable */
	GPIO137_GPIO137,	  /* CAM_PWR:reset is done by HW */
	
	/* touch switch */
	GPIO49_GPIO49,    /* TSI_INT_CAP */
	
	/* NAS capacitive touch screen INT */
	GPIO115_GPIO115,	  /* PENIRQ */
	GPIO141_GPIO141,		/*touch reset*/
		
	/*gsensor*/
	GPIO129_GPIO129,   /* GS_INT */
	
	/*magnetic sensor*/
	GPIO45_GPIO45,   /* MAGNETIC_DRDY */
	GPIO46_GPIO46,   /* MAGNETIC_INT */

	/*gyroscope*/
	GPIO43_GPIO43,   /* GYROSCOPE_DRDY */
	GPIO44_GPIO44,   /* GYROSCOPE_INT */

	/*RTC*/
	GPIO36_GPIO36,   /* RTC_INT */

	/* TWSI1: ADS1000 */
	TWSI1_SCL,
	TWSI1_SDA,

	/* TWSI2: RT5625, 88W8787(FM), magnetic sensor, RTC */
	GPIO55_TWSI2_SCL,
	GPIO56_TWSI2_SDA,

	/* TWSI3: camera, gyroscope, gsensor */
	GPIO71_TWSI3_SCL,
	GPIO72_TWSI3_SDA,

	/* TWSI4: touch screen, touch switch, light sensor stk2201 */
	TWSI4_SCL,
	TWSI4_SDA,

	/* TWSI6: HDMI DDC */
	GPIO47_TWSI6_SCL,
	GPIO48_TWSI6_SDA,
 
	/* RT5625 */
	GPIO33_GPIO33,         /* CODEC_INT */
	GPIO50_GPIO50,		   /* AUDIO_RST */

	GPIO125_GPIO125,		   /* JACK_DET */

	/* SSPA1 (I2S, connected between MMP2 and RT5625) */
 	GPIO24_I2S_SYSCLK,	
 	GPIO25_I2S_BITCLK,		
 	GPIO26_I2S_SYNC,	
 	GPIO27_I2S_DATA_OUT,	
 	GPIO28_I2S_SDATA_IN,
 	/* PCM is connected between RT5625 and 88W8787 (bluetooth) */ 
 	/* LINE is connected between RT5625 and 88W8787 (fm) */ 

	/*  LCD */
	GPIO74_LCD_FCLK,
	GPIO75_LCD_LCLK,
	GPIO76_LCD_PCLK,
	GPIO77_LCD_DENA,
	GPIO78_LCD_DD0,
	GPIO79_LCD_DD1,
	GPIO80_LCD_DD2,
	GPIO81_LCD_DD3,
	GPIO82_LCD_DD4,
	GPIO83_LCD_DD5,
	GPIO84_LCD_DD6,
	GPIO85_LCD_DD7,
	GPIO86_LCD_DD8,
	GPIO87_LCD_DD9,
	GPIO88_LCD_DD10,
	GPIO89_LCD_DD11,
	GPIO90_LCD_DD12,
	GPIO91_LCD_DD13,
	GPIO92_LCD_DD14,
	GPIO93_LCD_DD15,
	GPIO94_LCD_DD16,
	GPIO95_LCD_DD17,

#if !defined(TARGET_PRODUCT_p200)
	GPIO96_LCD_DD18,
	GPIO97_LCD_DD19,
	GPIO98_LCD_DD20,
	GPIO99_LCD_DD21,
	GPIO100_LCD_DD22,
	GPIO101_LCD_DD23,
#else /* p200 */
	GPIO96_GPIO96,		/* spi sdi */
	GPIO97_GPIO97,		/* spi sdo */
	GPIO100_GPIO100,	/* spi clk */
	GPIO101_GPIO101,	/* spi cs  */
#endif

	GPIO120_GPIO120,	 /* LCD_PWR_ENn */
	GPIO116_GPIO116,	 /* LCD_BL_PWR_EN */

	/*ULPI*/
	GPIO59_ULPI_DAT7,
	GPIO60_ULPI_DAT6,
	GPIO61_ULPI_DAT5,
	GPIO62_ULPI_DAT4,
	GPIO63_ULPI_DAT3,
	GPIO64_ULPI_DAT2,
	GPIO65_ULPI_DAT1,
	GPIO66_ULPI_DAT0,
	GPIO67_ULPI_STP,
	GPIO68_ULPI_NXT,
	GPIO69_ULPI_DIR,
	GPIO70_ULPI_CLK,
	/*hub reset*/
	GPIO124_GPIO124,
	/*ULPI reset*/
	GPIO4_GPIO4,

	/* VBUS_FAULT_N */
	GPIO7_GPIO7,
	/* VBUS_EN */
	GPIO6_GPIO6,

	/* pwm3: Backlight */
	GPIO73_LCM_BIAS_PWM,	

	/* HDMI */
	GPIO34_HDMI_DET_PULL_HIGH,
	GPIO54_HDMI_CEC,
	
	/* 88W8787 */
	GPIO105_GPIO105,      /* 88W8787 power down */  
	GPIO58_GPIO58,      /* 88W8787 reset */
	GPIO5_GPIO5,      /* FM wake */
	GPIO0_GPIO0,      /* wifi wake */

	/* USIM unused, set as GPIO input, Pull-high */
	GPIO102_GPIO102,
	GPIO103_GPIO103,
	GPIO142_GPIO142,

	/* VCORE control */
	VCXO_REQ_AF2,    /*not used*/
	VCXO_OUT_AF2,    /*not used*/
	GPIO02_VOLADJ0,     /* V_ADJ0 */
	GPIO03_VOLADJ1,	 /* V_ADJ1 */
	/*mobile modem power control*/
	GPIO121_GPIO121,
	GPIO126_GPIO126,
	GPIO127_GPIO127,
	
	/*w1*/
	G_CLK_REQ_W1,

	/*gpio key*/
	GPIO8_GPIO8,    /*volumep*/
	GPIO9_GPIO9,    /*volumen*/

	/* MMC1:SD card */
	GPIO131_MMC1_DAT3_PULL_HIGH,
	GPIO132_MMC1_DAT2_PULL_HIGH,
	GPIO133_MMC1_DAT1_PULL_HIGH,
	GPIO134_MMC1_DAT0_PULL_HIGH,
	GPIO136_MMC1_CMD_PULL_HIGH,
	GPIO139_MMC1_CLK,
	GPIO140_MMC1_CD_PULL_LOW,

	/* MMC2 */
	GPIO37_MMC2_DAT3_PULL_HIGH,
	GPIO38_MMC2_DAT2_PULL_HIGH,
	GPIO39_MMC2_DAT1_PULL_HIGH,
	GPIO40_MMC2_DAT0_PULL_HIGH,
	GPIO41_MMC2_CMD_PULL_HIGH,
	GPIO42_MMC2_CLK,
	
	/* MMC3:emmc flash */
	GPIO165_MMC3_DAT7_PULL_HIGH,
	GPIO162_MMC3_DAT6_PULL_HIGH,
	GPIO166_MMC3_DAT5_PULL_HIGH,
	GPIO163_MMC3_DAT4_PULL_HIGH,
	GPIO167_MMC3_DAT3_PULL_HIGH,
	GPIO164_MMC3_DAT2_PULL_HIGH,
	GPIO168_MMC3_DAT1_PULL_HIGH,
	GPIO111_MMC3_DAT0_PULL_HIGH,
	GPIO112_MMC3_CMD_PULL_HIGH,
	GPIO151_MMC3_CLK,
	GPIO149_MMC_RESET,
	GPIO57_GPIO57,
	GPIO10_REBOOT,
#if defined(TARGET_PRODUCT_dragonfly)
	GPIO104_GPIO104, /* home */
	GPIO109_GPIO109, /* back */
	GPIO110_GPIO110, /* menu */
#else
    GPIO47_GPIO47,
    GPIO104_GPIO104,
#endif
    //for board id
    GPIO108_GPIO108,
    GPIO109_GPIO109,
    GPIO110_GPIO110,
    
    

};

//static struct pxa3xx_mfp_addr_map pxa688_mfp_addr_map[] __initdata = {
static struct mfp_addr_map pxa688_mfp_addr_map[] __initdata = {

	MFP_ADDR_X(GPIO0, GPIO58, 0x54),
	MFP_ADDR_X(GPIO59, GPIO73, 0x280),
	MFP_ADDR_X(GPIO74, GPIO101, 0x170),

	MFP_ADDR(GPIO102, 0x0),
	MFP_ADDR(GPIO103, 0x4),
	MFP_ADDR(GPIO104, 0x1fc),
	MFP_ADDR(GPIO105, 0x1f8),
	MFP_ADDR(GPIO106, 0x1f4),
	MFP_ADDR(GPIO107, 0x1f0),
	MFP_ADDR(GPIO108, 0x21c),
	MFP_ADDR(GPIO109, 0x218),
	MFP_ADDR(GPIO110, 0x214),
	MFP_ADDR(GPIO111, 0x200),
	MFP_ADDR(GPIO112, 0x244),
	MFP_ADDR(GPIO113, 0x25c),
	MFP_ADDR(GPIO114, 0x164),
	MFP_ADDR_X(GPIO115, GPIO122, 0x260),

	MFP_ADDR(GPIO123, 0x148),
	MFP_ADDR_X(GPIO124, GPIO141, 0xc),

	MFP_ADDR(GPIO142, 0x8),
	MFP_ADDR_X(GPIO143, GPIO151, 0x220),
	MFP_ADDR_X(GPIO152, GPIO153, 0x248),
	MFP_ADDR_X(GPIO154, GPIO155, 0x254),
	MFP_ADDR_X(GPIO156, GPIO159, 0x14c),

	MFP_ADDR(GPIO160, 0x250),
	MFP_ADDR(GPIO161, 0x210),
	MFP_ADDR(GPIO162, 0x20c),
	MFP_ADDR(GPIO163, 0x208),
	MFP_ADDR(GPIO164, 0x204),
	MFP_ADDR(GPIO165, 0x1ec),
	MFP_ADDR(GPIO166, 0x1e8),
	MFP_ADDR(GPIO167, 0x1e4),
	MFP_ADDR(GPIO168, 0x1e0),

	MFP_ADDR_X(TWSI1_SCL, TWSI1_SDA, 0x140),
	MFP_ADDR_X(TWSI4_SCL, TWSI4_SDA, 0x2bc),

	MFP_ADDR(CLK_REQ, 0x160),

	MFP_ADDR_END,
};

void
dummy_delay(unsigned  int delay)
{
    volatile unsigned int i;
    for(i = 0; i < delay; i++);
}


#ifdef CONFIG_PXA9XX_SDH
int board_mmc_init(bd_t *bd)
{
	return pxa9xx_mmc_init(bd);
}
#endif

unsigned long initdram (int board_type)
{
    return (PHYS_SDRAM_SIZE_DEC*1024*1024);
}

#ifdef CONFIG_PXA27X_KEYPAD
int pxa27x_keypad_init(void)
{
	*(volatile unsigned int *)0xd401e004 = 0xd001;
	*(volatile unsigned int *)0xd401e008 = 0xd001;
	return 0;

}
#endif

int board_init (void)
{
	volatile unsigned int reg;

	wtm_read_profile();
	wtm_read_stepping();

#if defined (CONFIG_TRUST_BOOT) && defined (CONFIG_RECOVERY_MODE)
#ifdef CONFIG_NO_TRUST_OBM
	/* initialize magic and flag num since there is no trusted obm */
	mrvl_mag = CONFIG_TRUST_BOOT_MAGIC;
	mrvl_validation_flag = 0;
	mrvl_residue_flag = 0;
#else
	/* read trust boot flag */
	mrvl_mag = *(volatile unsigned int *)0xd1020100;
	mrvl_validation_flag = *(volatile unsigned int *)0xd1020104;
	mrvl_residue_flag = *(volatile unsigned int *)0xd1020108;
#endif

	/* for R_uboot only */
	if (*(volatile unsigned int *)0xd4010014 == 0x2 || \
		(mrvl_validation_flag & 0x15) != 0x0) {
		R_uboot = 1;
	}
	else
		R_uboot = 0;
#endif

	/* Turn on clock gating (PMUM_CGR_PJ) */
	*(volatile unsigned int *)0xd4051024 = 0xffffffff;
	*(volatile unsigned int *)0xd4050038 = 0x5;
	*(volatile unsigned int *)0xD42828dc = 0x1FFFF;

	/* AIB clock */
	*(volatile unsigned int *)0xD4015064 = 0x7;
	*(volatile unsigned int *)0xD4015064 = 0x3;

	/* UART3 clk */
	*(volatile unsigned int *)0xD4015034 = 0x4 ; /*26MHz clock*/
	*(volatile unsigned int *)0xD4015034 = 0x7 | (1<<4) ; /*26MHz clock*/
	*(volatile unsigned int *)0xD4015034 = 0x3 | (1<<4); /*26MHz clock*/

	reg = *(volatile unsigned int *)0xD401E160;
	reg |= 1<<14; 
	*(volatile unsigned int *)0xD401E160 = reg;

	BU_REG_WRITE(0xd4015000, 0x83 );

	//APB TIMER clock
	*(volatile unsigned int *)0xd4015024 = 0x5;
	*(volatile unsigned int *)0xd4015024 = 0x33;

	reg = *(volatile unsigned int *)0xd4282c08;
	/*
	 * Bit 6: 0 - enable PJ4 global clock gating.
	 * Bit 14: 0 - enable L2 clock gating.
	 * Enable PJ4 and L2 clock gating for A1. Maybe A2 there after.
	 */
	if(cpu_is_pxa688_a1() || cpu_is_pxa688_a2())
		reg |= (1 << 19) | (1 << 13);
	else
		reg |= (1<<6) | (1<<14) | (1<<19) | (1<<13);

	reg |= (1 << 19) | (1 << 13);
	reg &= ~(1<<23);
	*(volatile unsigned int *)0xd4282c08 = reg;

	*(volatile unsigned int *)0xd4282110 = 0x0;

	*(volatile unsigned int *)0xd428290c = 0x600;
	*(volatile unsigned int *)0xd428290c = 0x610;
	*(volatile unsigned int *)0xd428290c = 0x710;
	*(volatile unsigned int *)0xd428290c = 0x712;

	*(volatile unsigned int *)0xd4050040 = 0xd0080040;
	*(volatile unsigned int *)0xd4050044 = 0xd0040040;
	*(volatile unsigned int *)0xd42a0c3c = 0x10800;
	*(volatile unsigned int *)0xd42a0c34 = 0x211921;

	*(volatile unsigned int *)0xd428285c = 0x8;
	*(volatile unsigned int *)0xd428285c = 0x9;
	*(volatile unsigned int *)0xd42828f8 = 0x18;
	*(volatile unsigned int *)0xd42828f8 = 0x1b;

	/* disable usb clock */
	/* usb3 clock cannot be disabled on z0/z1 in case of tftp error */
	*(volatile unsigned int *)0xd42828fc = 0x0;
	if (cpu_is_pxa688_z0() || cpu_is_pxa688_z1()) {
		*(volatile unsigned int *)0xd4282900 = 0x18;
		*(volatile unsigned int *)0xd4282900 = 0x1b;
	}
	else
		*(volatile unsigned int *)0xd4282900 = 0x0;

	/* Below is for IRE */
	if (cpu_is_pxa688_z0() || cpu_is_pxa688_z1()) {
		*(volatile unsigned int *)0xd4282848 = 0x00000008;
		*(volatile unsigned int *)0xd4282848 = 0x00000009;
	} else
        *(volatile unsigned int *)0xd4282848 = 0x0;

	/* disable memory stick clock*/
	*(volatile unsigned int *)0xd42828d8 = 0x0;

#ifdef CONFIG_MMC
	/* disable SMC clock*/
	*(volatile unsigned int *)0xd42828d4 = 0x0;
#endif

	/* TWSI1 clk*/
	*(volatile unsigned int *)0xD4015004 = 0x3; 
	dummy_delay(99999);

	/* SDHC clk*/
	if (cpu_is_pxa688_z0() || cpu_is_pxa688_z1())
		*(volatile unsigned int *)0xd4282854 |= 0x1b;
	else
		*(volatile unsigned int *)0xd4282854 |= 0x1b | (1<<10);
	*(volatile unsigned int *)0xd42828e8 |= 0x1b; 

	*(volatile unsigned int *)0xd428284c=(1<<3);
	*(volatile unsigned int *)0xd428284c=(1<<3) | 1;
	*(volatile unsigned int *)0xd428284c=(1<<3) | 1 | (1<<4);
	*(volatile unsigned int *)0xd428284c=(1<<3) | 1 | (1<<4) | (1<<1);

	/* SSP clk */
	*(volatile unsigned int *)0xd4015050=0x04;

	/* IPC clk */
	*(volatile unsigned int *)0xd4015078=0x04;

	/* enable GPIO clock */
	*(volatile unsigned int *)0xd4015038=0x03;

	/* Allow access of performance counters(PMU) from user space*/
	__asm__("mrc p15, 0, %0, c9, c14, 0" : "=r" (reg));
	reg |= 0x1;
	__asm__("mcr p15, 0, %0, c9, c14, 0" : : "r" (reg));

	/* Ensure branch prediction is enabled - BPU (Default) */
	__asm__("mrc p15, 0, %0, c1, c0, 0" : "=r" (reg));
	reg |= (1<<11);
	__asm__("mcr p15, 0, %0, c1, c0, 0" : : "r" (reg));

	if(cpu_is_pxa688_z0() || cpu_is_pxa688_z1()){
		__asm__("mrc p15, 1, %0, c15, c1, 0" : "=r" (reg));
		reg |= (1<<15);
		__asm__("mcr p15, 1, %0, c15, c1, 0" : : "r" (reg));
	}

	gd->bd->bi_dram[0].start = PHYS_SDRAM_1;
	gd->bd->bi_dram[0].size  = PHYS_SDRAM_1_SIZE;

	/* arch number of G50 Board */
	gd->bd->bi_arch_number = 3498;  // MACH_TYPE_G50

	/* adress of boot parameters */
#ifdef CONFIG_PJ4_NON_SECURE_MODE
	gd->bd->bi_boot_params = 0x3C00;
#else
	gd->bd->bi_boot_params = 0x3C00;
#endif
	gd->baudrate = CONFIG_BAUDRATE;

	BU_REG_WRITE( APBC_AIB_CLK_RST, APBC_AIB_CLK_RST_FNCLK  |
		APBC_AIB_CLK_RST_APBCLK );

#ifndef CONFIG_MMC
	BU_REG_WRITE(0xd4283894, 0x11000008 );
#endif

	BU_REG_WRITE(0xd4015000, 0x83 );
	BU_REG_WRITE(0xd4015074, 0x3 );
	BU_REG_WRITE(0xd4282864, 0x9 );
#ifndef CONFIG_MMC
	BU_REG_WRITE(0xd4282860, 0xb8);
	BU_REG_WRITE(0xd4282860, 0xbf);
#else
	BU_REG_WRITE(0xd4282860, 0x0);
#endif

	/*configure for MFP*/
	mfp_init_base(CONFIG_MFPR_MMIOBASE);
	mfp_init_addr(pxa688_mfp_addr_map);
	mfp_config(jasper_pin_config, ARRAY_SIZE(jasper_pin_config));
	
#ifndef CONFIG_MMC
    *(volatile unsigned int *)0xd4283024=0;
#endif



	return 0;
}

#ifdef CONFIG_FREQ_CHG
int init_freq(void)
{
	if (cpu_is_pxa688_z1()) {
		/* set voltage to 1350mV by default */
		set_volt(1350);
		dummy_delay(99999);
		set_volt(1350);

		freq_init_sram(0xd1020000);
		freq_chg_seq(0xd1020000, 0xd1028000, 2, 1);
	}else{
		/* set voltage to 1350mV by default */
               //set_volt(1350);
               //need change for G50
		//dummy_delay(99999);
               //set_volt(1350);
               //need change for G50
		freq_init_sram(0xd1020000);
		freq_chg_seq(0xd1020000, 0xd1028000, 3, 3);

	}
	return 0;
}
#endif


static int do_shutdown(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]);
static int get_board_revision(void)
{
	static int revision=-1;
	int vers0 = mfp_to_gpio(GPIO110_GPIO110);
	int vers1 = mfp_to_gpio(GPIO109_GPIO109);
	int vers2 = mfp_to_gpio(GPIO108_GPIO108);

	if(revision>=0)
		return revision;
	
	revision  = ((!!!!gpio_get_value(vers2) << 2) |
			(!!!!gpio_get_value(vers1) << 1) |
			(!!!!gpio_get_value(vers0)));
	//revision is 1 based
	revision++;
	return revision;
}

#ifdef CONFIG_BOOTARGS
enum nv_ret
{
   eValid=0,
   eNotFound,
   eInvalid,   
};
static int check_nv(char *nv, char* arg)
{
	char *value = strchr(nv, '=');
	if (*nv == 0) return eNotFound;
	if(0!=value)
	{
		*value=0;
		if(!strcmp(nv,arg))  
		{
			*value='=';
			return eValid;
		}
		*value='=';		
  }
  else
  {
  	if(strlen(arg)!=strlen(nv))
  		return eNotFound;
  	if(!strncmp(nv,arg,strlen(arg)))
  		return eValid;
  }
	
	return eNotFound;
}

static int check_android_argument( char* cmdline, char* name)
{	
   char*  ptr;
   char* space,*next;

	/* eat leading white space */
	for (ptr = cmdline; *ptr == ' '; ptr++);
   
    while (ptr && *ptr) {
       space = strchr(ptr, ' ');       
	if (space != 0)
	{
		*space= 0;
		next = space+1;
	}
	else
	{
		next=NULL;
	}
	if(!check_nv(ptr,name)) {
	  	//resume space
	  	if(space!=0)
	  	{
			*space = ' ';		
	  	}
	  	return eValid;
	}     
		
	if(space!=0)
	{
		*space = ' ';    
	}

      ptr = next;
    }

   return eInvalid;

}

static int add_android_argument(char* cmdline,int max_length,char* name,char* val)
{
	char*  ptr = cmdline;
	int addspace=1;
	int cmdline_length,nv_length;
	if(!cmdline||!name) return eInvalid;
	cmdline_length = strlen(cmdline);
	nv_length = strlen(name)+val?(strlen(val)+2):0/*for sign '=' and ' ' */;
	//check if last is space
	if(ptr[cmdline_length-1]==' '){
			nv_length--;
			addspace=0;
	}
	if((cmdline_length+nv_length)>=max_length) return eInvalid;
	ptr+=cmdline_length;
	if(val)
		sprintf(ptr,"%s%s=%s",addspace?" ":"",name,val);
	else
		sprintf(ptr,"%s%s",addspace?" ":"",name);
		
	return eValid;

}

static int init_android_arguments(void)
{
	char cmdline[1024]={0};
	char buffer[256];
	const char* android_nvs[]={
		"reserve_dram","audiobuf_resv",//necessary		
		"fb_base",
	    "fb_size",
		"androidboot.console",
        "console",//
		"ethaddr",//for ethernet mac address
		"androidboot.serialno","androidboot.mode","androidboot.baseband","androidboot.carrier","androidboot.bootloader","androidboot.hardware","androidboot.revision",	//optional
		NULL,
	};
	char* env,*bootargs,*nv;
	int i;
	//get boot arguments ,it should not fail
	env = getenv("bootargs");
	if(!env)	setenv("bootargs",CONFIG_BOOTARGS);
	env = getenv("console");
	if(!env)	setenv("console",CONFIG_DEFAULT_CONSOLE);
	
	env = getenv("androidboot.console");
	if(!env)	setenv("androidboot.console",CONFIG_ANDROID_CONSOLE);
	env = getenv("fb_base");
	if(!env)	{
		sprintf(buffer,"0x%x",DEFAULT_FB_BASE);
	    setenv("fb_base",buffer);
    }
	env = getenv("fb_size");
	if(!env)	{
		sprintf(buffer,"0x%x",DEFAULT_FB_SIZE);
	    setenv("fb_size",buffer);
	}
	
	env = getenv("reserve_dram");
	if(!env)	setenv("reserve_dram",CONFIG_ANDROID_RESERVED_DRAM);
	env = getenv("audiobuf_resv");
	if(!env)	setenv("audiobuf_resv",CONFIG_ANDROID_AUDIOBUF);
	env = getenv("audiobuf_resv");
	if(!env)	setenv("audiobuf_resv",CONFIG_ANDROID_AUDIOBUF);	

	//standard android arguments
	env = getenv("androidboot.revision");
	if(!env)
	{
		sprintf(buffer,"%d",get_board_revision());
		setenv("androidboot.revision",buffer);
	}
	env = getenv("androidboot.hardware");if(!env)	setenv("androidboot.hardware","g50");
	env = getenv("androidboot.bootloader");if(!env)	setenv("androidboot.bootloader","uboot");
	
	bootargs = getenv("bootargs");
	strcpy(cmdline,bootargs);
	//check android arguments,	
	for(i=0;android_nvs[i]!=NULL;i++)
	{
		nv = android_nvs[i];
		env = getenv(nv);
		if(env)
		{
			if(eValid!=check_android_argument(cmdline,nv))
			{
				add_android_argument(cmdline,1024,nv,env);		
			}
		}
	}

	env = getenv("extargs");
	if(env){
        add_android_argument(cmdline,1024,env,0);      
	}
	//now reassign boot arguments
	setenv("bootargs",cmdline);

	return 0;
	
}
#endif

static void sdelay(int s){
 unsigned long long start,end,usleft;
  unsigned long long tmo = CONFIG_SYS_HZ;
  while(s>0){
  	s--;
  	start = get_ticks();
	do {
	     end = get_ticks();
	     usleft = (start < end) ? (end - start) : (end + ~start);
	  } while (usleft < tmo);
	
  }
}

int misc_init_r (void)
{
	char *env;	
	flash_init();
	lcd_draw_default_bmp(0);

#ifdef CONFIG_BATTERY_MONITOR
	int batt_cap,dc_in,low_thres,batt_force_low;
	int batt_cap0,batt_cap1,batt_cap2;

	*(volatile unsigned int *)0xd401e148 = 0x9000;
	
	battery_init(0,GPIO(123));
	batt_cap0 = battery_capacity();
	batt_cap1 = battery_capacity();
	batt_cap2 = battery_capacity();
	batt_cap = (batt_cap0>batt_cap1)?batt_cap0:batt_cap1;
	batt_cap = (batt_cap>batt_cap2)?batt_cap:batt_cap2;
	dc_in = battery_dc_in();	
	env = getenv("batt_threshold");
	if(!env)
	{	
		setenv("batt_threshold","5");
	}
	env = getenv("batt_threshold");
	low_thres = simple_strtoul(env,NULL,10);
	batt_force_low = getenv("batt_low")?1:0;
	printf("confirm battery cap=%d,dc_in=%d,thres=%d,force_low=%d\n",
					batt_cap,dc_in,low_thres,batt_force_low);
	if((batt_cap<=low_thres&&!dc_in)||batt_force_low)
	{
		void *bat0 = battery_bmp(0);
		if(bat0){
			int i=0;
			lcd_clear();
			lcd_draw_bmp(bat0,0);	
			
			sdelay(5);
			//only shutdown while not force low
			if(!batt_force_low)
				do_shutdown(0,0,0,0);
		}
		
	}
#endif

#ifdef CONFIG_SUPPORT_TOUCHKEY	
	touchkey_init(3,GPIO(49));
#endif


	/* primary network interface */
	env = getenv("ethprime");
	if(!env)
		setenv("ethprime","eth0");

	/* default usb mode */
	env = getenv("usbMode");
	if(!env)
		setenv("usbMode","host");

	/* linux boot arguments */
	env = getenv("default_load_addr");
	if(!env)
		setenv("default_load_addr",CONFIG_SYS_DEF_LOAD_ADDR);

	env = getenv("image_name");
	if(!env)
		setenv("image_name",CONFIG_SYS_IMG_NAME);

	env = getenv("bootfile");
	if(!env)
		setenv("bootfile",CONFIG_SYS_IMG_NAME);
	        
	env = getenv("initrd_name");
	if(!env)
	setenv("initrd_name",CONFIG_SYS_INITRD_NAME);

	env = getenv("initrd_load_addr");
	if(!env)
		setenv("initrd_load_addr",CONFIG_SYS_INITRD_LOAD_ADDR);

	env = getenv("initrd_size");
	if(!env)
		setenv("initrd_size",CONFIG_SYS_INITRD_SIZE);


	env = getenv("serial_number");
	if (!env) 
		setenv("serial_number", CONFIG_SERIAL_NUM);

	env = getenv("product");
	if(!env)
		setenv("product",CONFIG_USBD_PRODUCT_NAME);

	env = getenv("bootdelay");
	if (env){ 
	    int bootdelay = (int)simple_strtol(env, NULL, 10);
	    if(bootdelay<1){
	        char buf[64];
	        bootdelay = CONFIG_BOOTDELAY;
	        sprintf(buf,"%d",bootdelay);
		    setenv("bootdelay", buf);
		}
	}

	#ifdef CONFIG_BOOTARGS
	//always reconstruct bootargs
	//env = getenv("bootargs");
	//if(!env)
	//	setenv("bootargs",CONFIG_BOOTARGS);	
	setenv("bootargs",NULL);
	init_android_arguments();
	#endif

	env = getenv("mmcboot");
	if(!env)
		setenv("mmcboot",CONFIG_MMCBOOT);

	env = getenv("preboot");
	if(!env)
		setenv("preboot","preboot");

#if defined(MACHINE_ID)
	env = getenv("machid");
	if (!env)
		setenv("machid", MACHINE_ID);	
#endif

	return 0;
}


int dram_init (void)
{
	return 0;
}

int checkboard (void)
{
	int hwversion = get_board_revision();
	
	/*modify for hardware version3 LCD power pin change*/
	if(hwversion >= 3)
		*(volatile unsigned int *)0xD401e078 = 0xd0c0; //set gpio9 gpio function
	/*Add finger print power pin control for hw version3 */
	if(hwversion >= 3)
	{
	    volatile unsigned int v;
	    v = *(volatile unsigned int *)(0xD401910c);
		/*finger print power pin init*/
		*(volatile unsigned int *)0xD401910c = v|0x100; //set gpio104 output
		*(volatile unsigned int *)0xD4019124 = 0x100; //clear gpio104
		/*flash light power pin init*/
		v = *(volatile unsigned int *)(0xD4019010);
		*(volatile unsigned int *)0xD4019010 = v|0x8000; //set gpio47 output
		*(volatile unsigned int *)0xD4019028 = 0x8000; //clear gpio47 
	}	
	
	display_marvell_banner();
	wtm_dump_info();	

	printf("board id=%d\n",hwversion);
	return 0;
}

static void fastboot_mode(void)
{
	#ifdef CONFIG_CFB_CONSOLE
	lcd_clear();
	//switch console to lcd 	
	console_assign(stdout, "vga");
	#endif	
	run_command("fb", 0);	
}


int display_marvell_banner (void)
{
	printf("\n");
	printf(" __  __                      _ _\n");
	printf("|  \\/  | __ _ _ ____   _____| | |\n");
	printf("| |\\/| |/ _` | '__\\ \\ / / _ \\ | |\n");
	printf("| |  | | (_| | |   \\ V /  __/ | |\n");
	printf("|_|  |_|\\__,_|_|    \\_/ \\___|_|_|\n");
	printf(" _   _     ____              _\n");
	printf("| | | |   | __ )  ___   ___ | |_ \n");
	printf("| | | |___|  _ \\ / _ \\ / _ \\| __| \n");
	printf("| |_| |___| |_) | (_) | (_) | |_ \n");
	printf(" \\___/    |____/ \\___/ \\___/ \\__| ");
	printf("\n\nMARVELL MMP2 AP.");
   	if(cpu_is_pxa688_a2())
   	printf("\nBased on MMP2 A2 CPU.\n\n"); 
	if(cpu_is_pxa688_a1())
		printf("\nBased on MMP2 A1 CPU.\n\n"); 
	if(cpu_is_pxa688_a0())
		printf("\nBased on MMP2 A0 CPU.\n\n"); 
	else if(cpu_is_pxa688_z1())
		printf("\nBased on MMP2 Z1 CPU.\n\n"); 
	else if(cpu_is_pxa688_z0())
		printf("\nBased on MMP2 Z0 CPU.\n\n"); 

#if defined (CONFIG_RECOVERY_MODE) && defined (CONFIG_TRUST_BOOT)
	if (R_uboot)
		printf("\nEntering Maverll Recovery Uboot.\n");
#endif
	return 0;
}

#ifdef CONFIG_CMD_NET
int board_eth_init(bd_t *bis)
{
	int res = -1;

#if defined(CONFIG_MMPUDC)
	if (usb_eth_initialize(bis) >= 0)
		res = 0;
#endif
	return res;
}
#endif

#ifdef CONFIG_MMPUDC
int usb_lowlevel_init(void)
{
	int base = CONFIG_USB_PHY_BASE;
	int count = 0;
	int reg;

	/* enable u2o clock */
	writel(0x18, PMUA_USB_CLK_RES_CTRL);
	writel(0x1b, PMUA_USB_CLK_RES_CTRL);

	/* initialize the usb phy power */
	writel(0x10101003, base + UTMI_CTRL);
	/* UTMI_PLL settings */
	writel(0x7e819eeb, base + UTMI_PLL);
	/* UTMI_TX */
	writel(0x41e947c3, base + UTMI_TX);
	/* UTMI_RX */
	writel(0xec1d0271, base + UTMI_RX);
	/* UTMI_IVREF */
	writel(0x2000007e, base + UTMI_IVREF);
	/* calibrate */
	count = 10000;
	while(((readl(base + UTMI_PLL) & PLL_READY)==0) && count--);
	if (count <= 0)
		printf("Calibrate timeout, UTMI_PLL:%x\n",
				readl(base + UTMI_PLL));

	/* toggle VCOCAL_START bit of UTMI_PLL */
	udelay(200);
	reg = readl(base + UTMI_PLL);
	writel(reg | VCOCAL_START, base + UTMI_PLL);
	udelay(40);
	writel(reg & (~VCOCAL_START), base + UTMI_PLL);

	/* toggle REG_RCAL_START bit of UTMI_TX */
	udelay(200);
	reg = readl(base + UTMI_TX);
	writel(reg | REG_RCAL_START, base + UTMI_TX);
	udelay(40);
	writel(reg & (~REG_RCAL_START), base + UTMI_TX);
	udelay(200);

	/* make sure phy is ready */
	count = 1000;
	while(((readl(base + UTMI_PLL) & PLL_READY)==0) && count--);
	if (count <= 0)
		printf("Calibrate timeout, UTMI_PLL %x\n",
				readl(base + UTMI_PLL));

	return 0;
}
#endif

void lowlevel_init()
{
	return;
}

struct wtm_cmd {
    unsigned int prim_cmd_parm0;          // 0x0
    unsigned int prim_cmd_parm1;          // 0x4
    unsigned int prim_cmd_parm2;          // 0x8
    unsigned int prim_cmd_parm3;          // 0xc
    unsigned int prim_cmd_parm4;          // 0x10
    unsigned int prim_cmd_parm5;          // 0x14
    unsigned int prim_cmd_parm6;          // 0x18
    unsigned int prim_cmd_parm7;          // 0x1c
    unsigned int prim_cmd_parm8;          // 0x20
    unsigned int prim_cmd_parm9;          // 0x24
    unsigned int prim_cmd_parm10;         // 0x28
    unsigned int prim_cmd_parm11;         // 0x2c
    unsigned int prim_cmd_parm12;         // 0x30
    unsigned int prim_cmd_parm13;         // 0x34
    unsigned int prim_cmd_parm14;         // 0x38
    unsigned int prim_cmd_parm15;         // 0x3c
    unsigned int secure_processor_cmd;    // 0x40
};

/*
 * WTM register file for host communication
 */
struct wtm_mail_box {
	unsigned int prim_cmd_parm0;          // 0x0
	unsigned int prim_cmd_parm1;          // 0x4
	unsigned int prim_cmd_parm2;          // 0x8
	unsigned int prim_cmd_parm3;          // 0xc
	unsigned int prim_cmd_parm4;          // 0x10
	unsigned int prim_cmd_parm5;          // 0x14
	unsigned int prim_cmd_parm6;          // 0x18
	unsigned int prim_cmd_parm7;          // 0x1c
	unsigned int prim_cmd_parm8;          // 0x20
	unsigned int prim_cmd_parm9;          // 0x24
	unsigned int prim_cmd_parm10;         // 0x28
	unsigned int prim_cmd_parm11;         // 0x2c
	unsigned int prim_cmd_parm12;         // 0x30
	unsigned int prim_cmd_parm13;         // 0x34
	unsigned int prim_cmd_parm14;         // 0x38
	unsigned int prim_cmd_parm15;         // 0x3c
	unsigned int secure_processor_cmd;    // 0x40
	unsigned char reserved_0x44[60];
	unsigned int cmd_return_status;       // 0x80
	unsigned int cmd_status_0;            // 0x84
	unsigned int cmd_status_1;            // 0x88
	unsigned int cmd_status_2;            // 0x8c
	unsigned int cmd_status_3;            // 0x90
	unsigned int cmd_status_4;            // 0x94
	unsigned int cmd_status_5;            // 0x98
	unsigned int cmd_status_6;            // 0x9c
	unsigned int cmd_status_7;            // 0xa0
	unsigned int cmd_status_8;            // 0xa4
	unsigned int cmd_status_9;            // 0xa8
	unsigned int cmd_status_10;           // 0xac
	unsigned int cmd_status_11;           // 0xb0
	unsigned int cmd_status_12;           // 0xb4
	unsigned int cmd_status_13;           // 0xb8
	unsigned int cmd_status_14;           // 0xbc
	unsigned int cmd_status_15;           // 0xc0
	unsigned int cmd_fifo_status;         // 0xc4
	unsigned int host_interrupt_register; // 0xc8
	unsigned int host_interrupt_mask;     // 0xcc
	unsigned int host_exception_address;  // 0xd0
	unsigned int sp_trust_register;       // 0xd4
	unsigned int wtm_identification;      // 0xd8
	unsigned int wtm_revision;            // 0xdc
};


#define WTM_BASE			0xD4290000
#define WTM_GET_SOC_POWER_POINT		0x1006
#define WTM_GET_SOC_STEPPING		0x1007
#define WTM_PRIM_CMD_COMPLETE_MASK	(1 << 0)

unsigned int mmp2_ack_from_wtm   = 0x0;
unsigned int mmp2_profile_adjust = 0x0;
unsigned int mmp2_profile	 = 0x0;
unsigned int mmp2_max_freq	 = 0x0;
unsigned int mmp2_ts_calibration = 0x0;

unsigned int mmp2_stepping	 = 0x0;

static volatile struct wtm_mail_box *wtm_mb =
	(volatile struct wtm_mail_box *)(WTM_BASE);

static int wtm_exe_cmd(struct wtm_cmd *cmd)
{
	int i;

	unsigned int *pcmd = &cmd->prim_cmd_parm0;
	volatile unsigned int *phi = &wtm_mb->prim_cmd_parm0;

	for (i = 0; i <= 16; i++) {
		*phi++ = *pcmd++;
	}

	/* try 1000 times */
	for (i = 0; i < 10000; i++) {
		if (wtm_mb->host_interrupt_register &
		    WTM_PRIM_CMD_COMPLETE_MASK) {
			/* clean interrupt */
			wtm_mb->host_interrupt_register = 0xFFFFFFFF;
			return wtm_mb->cmd_return_status;
		}
	}

	/* read fail */
	return -1;
}

static int wtm_read_profile(void)
{
	struct wtm_cmd cmd;
	int status;

	memset(&cmd, 0, sizeof(cmd));

	/* valid request */
	cmd.prim_cmd_parm0 = 0;
	cmd.secure_processor_cmd = WTM_GET_SOC_POWER_POINT;
	status = wtm_exe_cmd(&cmd);
	if (status < 0) {
		mmp2_ack_from_wtm = 0;
		goto out;
	}

	mmp2_ack_from_wtm   = 1;
	mmp2_profile_adjust = wtm_mb->cmd_status_0;
	mmp2_profile	    = wtm_mb->cmd_status_1;
	mmp2_max_freq	    = wtm_mb->cmd_status_2;
	mmp2_ts_calibration = wtm_mb->cmd_status_3;

out:
	return status;
}

static int wtm_read_stepping(void)
{
	struct wtm_cmd cmd;
	int status;

	unsigned int chip_id;
	unsigned int id;
	unsigned int soc_stepping;

	memset(&cmd, 0, sizeof(cmd));

	/* valid request */
	cmd.prim_cmd_parm0 = 0;
	cmd.secure_processor_cmd = WTM_GET_SOC_STEPPING;
	status = wtm_exe_cmd(&cmd);
	if (status < 0) {
		goto out;
	}

	soc_stepping = wtm_mb->cmd_status_0;
	chip_id      = __raw_readl(0xd4282c00);
	id           = read_cpuid(CPUID_ID);
	if ((chip_id & 0x00ff0000) != 0x00a00000)
		goto out;

	if (soc_stepping == 0x4130)
		mmp2_stepping = 0xa0;
	else if (soc_stepping == 0x4131)
		mmp2_stepping = 0xa1;
	else if (soc_stepping == 0x4132)
		mmp2_stepping = 0xa2;

out:
	return status;
}

static void wtm_dump_info(void)
{
	printk("----- wtm info -----\n");

	if (!mmp2_ack_from_wtm)
		printf("wtm has NO ack.\n");
	else
		printf("get ack from wtm.\n");

	printf("profile_adjust = 0x%08x\n", mmp2_profile_adjust);
	printf("profile        = 0x%08x\n", mmp2_profile);
	printf("max_freq       = 0x%08x\n", mmp2_max_freq);
	printf("ts_calibration = 0x%08x\n", mmp2_ts_calibration);
	printf("stepping       = 0x%08x\n", mmp2_stepping);

	printf("-----   end    -----\n\n");

	return;
}
enum {
	NONE=0x00,
	HOME=0x01,
	MENU=0x02,
	BACK=0x04,
	KEY_MASK=HOME|MENU|BACK,
};
enum {
	eBootModeNormal=0,
	eBootModeRecovery,
	eBootModeFastboot,
	eBootModeAutoupdate,
	eBootModesNum,
};
	
static int get_keyboard_state(void){
	unsigned int ret=NONE;

#if defined(TARGET_PRODUCT_butterfly)
#define KEY_PRESSED(gpio) (!((*(volatile unsigned int *)(0xd4019000)) & (1 << (gpio-0))))
    //for reachgood board
    if(KEY_PRESSED(8)) ret|=MENU;
    if(KEY_PRESSED(9)) ret|=HOME;
#elif defined(TARGET_PRODUCT_dragonfly)
#define KEY_PRESSED(gpio) (!((*(volatile unsigned int *)(0xd4019100)) & (1 << (gpio-96))))
	//for reachgood board
	if(KEY_PRESSED(110)) ret|=MENU;
	if(KEY_PRESSED(104)) ret|=HOME;
#else /*default to p200*/	
#define KEY_PRESSED(gpio) (!((*(volatile unsigned int *)(0xd4019004)) & (1 << (gpio-32))))
    if(KEY_PRESSED(45)) ret|=MENU;
#endif

	printf("keyboard state =%#x\n",ret);
	return ret;
}
/*
 *design schema:
 * 1.check hardware keyboard
 * 2.check reasonable register
 * 3.check reasonable partition
*/
static int get_bootmode(void){
	int bootmode=eBootModeNormal;
	int keyboard_state;
	keyboard_state = get_keyboard_state();
	
	if(NONE!=keyboard_state){
		/* test magic key holding for 3 seconds */
        #if defined(TARGET_PRODUCT_p200)
		int t=0;while (keyboard_state == get_keyboard_state() && t < 3) {udelay(3000000);t++;}
		if(t>1) keyboard_state = HOME;
		else keyboard_state = MENU;
        
        #else
		int t=0;while (keyboard_state == get_keyboard_state() && t < 3) {udelay(3000000);t++;}
		#endif
	}
	if(NONE!=keyboard_state){
		if(HOME==keyboard_state)
			bootmode=eBootModeRecovery;
		else if(MENU==keyboard_state)
			bootmode=eBootModeFastboot;
		else if((HOME|MENU)==keyboard_state)
			bootmode=eBootModeAutoupdate;
		if(bootmode){
			printf("bootmode=%d in keypad state\n",bootmode);
		}
	}
	//test for reasonable registers
	if(eBootModeNormal==bootmode){
		#define REBOOT_REASON_REG 0xd4010024		
		int reason =*(volatile unsigned int *)(REBOOT_REASON_REG);
		switch(reason){
			case 0x77665500://bootloader
			case 0x04:
				bootmode = eBootModeFastboot;
				//reset reason
				*(volatile unsigned int *)(REBOOT_REASON_REG)=0;		
				break;	
			case 0x77665502://recovery
			case 0x01:
				bootmode = eBootModeRecovery;
				*(volatile unsigned int *)(REBOOT_REASON_REG)=0;				
				break;
			case 0x77665503://autoupdate
				bootmode = eBootModeAutoupdate;				
				*(volatile unsigned int *)(REBOOT_REASON_REG)=0;				
			default:break;				
		}
		if(bootmode){
			printf("bootmode=%d in reg state\n",bootmode);
		}
		
	}
	//test for reasonable misc partitions
	if(eBootModeNormal==bootmode){
		ptentry* misc = flash_find_ptn("misc");
		if(misc){
			int ret,clear_cmd=0;
			char buffer[512];
			memset(buffer,0,512);
			ret = flash_read(misc,0,buffer,512);
			if(!ret){
				if(!strncmp(buffer,"boot-loader",11)){
					bootmode = eBootModeFastboot;
					clear_cmd = 1;
				}
				else if(!strncmp(buffer,"boot-recovery",13)){
					bootmode = eBootModeRecovery;
					//don't erase since this is recovery's work to do
				}
				else if(!strncmp(buffer,"update-firmware",15)){
					bootmode = eBootModeRecovery;
				}				
				else if(!strncmp(buffer,"eraseflash",10)){
					clear_cmd = 1;
				}
				else if(!strncmp(buffer,"autoupdate",10)){
					bootmode=eBootModeAutoupdate;
					clear_cmd = 1;
				}

				if(clear_cmd)
				{
					memset(buffer,0,512);
					flash_write(misc,0,buffer,512);
				}
				
			}
			
		}
		if(bootmode){
			printf("bootmode=%d in misc state\n",bootmode);
		}

		
	}
	
	//check system partition table is okay,if it does not,try
	if(eBootModeNormal==bootmode){
		//entry name is hard coding
		ptentry *kernel = flash_find_ptn("kernel");
		if(!kernel){
			bootmode = eBootModeAutoupdate;
		}

		if(bootmode){
			printf("bootmode=%d in partition state\n",bootmode);
		}
		
	}

	return bootmode;

	
}
#ifdef CONFIG_PREBOOT
static int do_preboot(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	static u8 res_recovery[]={
		#include "recovery_logo.inc"
	};
	static const char* bootmodes[]={
		"normal",
		"recovery",
		"fastboot",
		"autoupdate"
	};
	int ret;
	int bootmode = get_bootmode();
	printf("boot mode=%s\n",bootmodes[bootmode]);
 	if(eBootModeFastboot == bootmode){
	 	fastboot_mode();
	}else if(eBootModeAutoupdate == bootmode){
		ret = run_command("autoupdate",0);
		if(!ret) bootmode = eBootModeNormal;
		else {
			//if auto update failure,fall back to fastboot mode
			fastboot_mode();
		}
	}

	//turn to standard boot mode,normal&recovey these two modes will leave os loader.
	if(eBootModeRecovery == bootmode){
        void* bmp = CONFIG_SYS_LOAD_ADDR;               
		printf("Entering recovery mode...\n");
		lcd_clear();		
        if(!bmp_manager_readbmp("bmp.recovery",bmp,DEFAULT_FB_SIZE)){
            lcd_draw_bmp(bmp,0);
        }else
            lcd_draw_bmp(res_recovery,0);
		#ifdef BOOT_FROM_EMMC	
		//FIXME: hard code for recovery kernel
		setenv("mmcboot",CONFIG_MMCBOOT_RECOVERY);		
		#else
		#error "please define boot config for non mmc booting mode"
		#endif		
		setenv(CONFIG_BOOT_PHASE,CONFIG_BOOT_ALTERNATE);
	}else {		
		#ifdef BOOT_FROM_EMMC	
		//FIXME: hard code for  kernel
		setenv("mmcboot",CONFIG_MMCBOOT);		
		#else
		#error "please define boot config for non mmc booting mode"		
		#endif		
		setenv(CONFIG_BOOT_PHASE,CONFIG_BOOT_PRIMARY);
		
	}

	return 0;
}

U_BOOT_CMD(
        preboot, CONFIG_SYS_MAXARGS, 0, do_preboot, NULL, NULL
);

#endif

#ifdef CONFIG_SYS_CONSOLE_OVERWRITE_ROUTINE
int overwrite_console(void)
{
	char* ov=getenv("console");
	if(ov&&(strcmp(ov,"serial") == 0))
		return 1;
	return 0;
}
#endif

void board_reset()
{
	*(volatile unsigned int *)0xd4019018 |= 0x400;//dir
	*(volatile unsigned int *)0xd401900c |= 0x400;//set	
}


static int do_shutdown(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int count=100;
	
	*(volatile unsigned int *)0xd4019110 |= 0x1; //dir
	*(volatile unsigned int *)0xd4019128 |= 0x1; //clr
	while(count--)
		udelay(50000);
	printf("shutdown failed\n");	
	return 0;
}

U_BOOT_CMD(
        shutdown, CONFIG_SYS_MAXARGS, 0, do_shutdown, NULL, NULL
);

