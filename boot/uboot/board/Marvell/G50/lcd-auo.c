/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  publishhed by the Free Software Foundation.
 */
#include <asm/arch/gpio.h>
#include <mfp.h>
#include <common.h>
#include <pxa168fb.h>
#include <asm/io.h>
#include <i2c.h>
#include "tc35876x.h"
#include <errno.h>
#include <bmp_layout.h>
#include <video_fb.h>
#include <stdio_dev.h>
#include <mmc.h>
#include <fastboot/flash.h>
#include <bmpmanager.h>
#include "lcd-auo.h"
#include "lcd_init.h"

#define mdelay(x) udelay(1000*x)

extern int pxa168fb_init(struct pxa168fb_mach_info *mi);

static int set_lcd_power(int on)
{
#if defined(TARGET_PRODUCT_p200)
	printf("lcd init using spi interface...\n");
	lcd_init(on);
//	mdelay(100);
//	lcd_init(on);
#else    
	int lcd_pwr_en;

	lcd_pwr_en = mfp_to_gpio(MFP_PIN_GPIO120);


	if (on) {
		/* re-config lcd_lr/ud pin to enable lcd */
		printf("set lcd power on...\n");
		gpio_set_output(lcd_pwr_en);
		gpio_set_value(lcd_pwr_en, 0);/*active low*/
	} else {
		printf("set lcd power off...\n");

		/* config lcd_lr/ud pin to gpio for power optimization */
		gpio_set_output(lcd_pwr_en);
		gpio_set_value(lcd_pwr_en, 1);/*active low*/
		//gpio_set_output(lcd_bl_pwr_en);
		//gpio_set_value(lcd_bl_pwr_en, 1);/*active low*/
	}
#endif
	return 0;
}

static int brownstone_lcd_power(struct pxa168fb_info *fbi,
	unsigned int spi_gpio_cs, unsigned int spi_gpio_reset, int on)
{
	return set_lcd_power(on);
}

#ifdef TARGET_PRODUCT_butterfly
#define FB_XRES		1024
#define FB_YRES		600

static struct fb_videomode video_modes_parallel[] = {
	[0] = {
		.refresh	= 60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 2,
		.left_margin	= 160,
		.right_margin	=  158,
		.vsync_len	= 2,
		.upper_margin	= 23,
		.lower_margin	= 10,
		.sync		= 0,
	},
};
#elif defined(TARGET_PRODUCT_p200)
#define FB_XRES		480
#define FB_YRES		800

static struct fb_videomode video_modes_parallel[] = {
	[0] = {
		.refresh	= 60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 10,
		.left_margin	= 40,
		.right_margin	= 30,
		.vsync_len	= 6,
		.upper_margin	= 6,
		.lower_margin	= 18,
	},
};
#elif defined(TARGET_PRODUCT_phoenix)
#define FB_XRES		800
#define FB_YRES		600

static struct fb_videomode video_modes_parallel[] = {
	[0] = {
		.refresh	= 60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 2,
		.left_margin	= 46,
		.right_margin	=  30,
		.vsync_len	= 2,
		.upper_margin	= 23,
		.lower_margin	= 5,
		.sync		= 0,
	},
};
#elif defined(TARGET_PRODUCT_dragonfly)
#define FB_XRES		800
#define FB_YRES		480

static struct fb_videomode video_modes_parallel[] = {
	[0] = {
		.refresh	= 60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 2,
		.left_margin	= 46,
		.right_margin	= 30,
		.vsync_len	= 2,
		.upper_margin	= 23,
		.lower_margin	= 5,
		.sync		= 0,
	},
};
#else
#define FB_XRES		1024
#define FB_YRES		600

static struct fb_videomode video_modes_parallel[] = {
	[0] = {
		.refresh	= 60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 2,
		.left_margin	= 160,
		.right_margin	=  158,
		.vsync_len	= 2,
		.upper_margin	= 23,
		.lower_margin	= 10,
		.sync		= 0,
	},
};
#endif

static struct pxa168fb_mach_info mmp2_parallel_lcd_info = {
	.id			= "GFX Layer",
	.sclk_src		= 400000000,	/* 400MHz for ar0922e lpddr1 + emmc*/
#if defined(TARGET_PRODUCT_p200)	
	.sclk_div		= 0x4000111c,   //14.28MHz
#else
	.sclk_div		= 0x4000110c,   // 0x4000110a
#endif	
	.num_modes		= ARRAY_SIZE(video_modes_parallel),
	.modes			= video_modes_parallel,
	//.pix_fmt		= PIX_FMT_RGB888UNPACK,
	.pix_fmt		= PIX_FMT_RGB565,
	.burst_len		= 16,
#if defined(TARGET_PRODUCT_p200)
	.io_pin_allocation_mode = PIN_MODE_DUMB_18_GPIO,
	.dumb_mode		= DUMB_MODE_RGB666,
#else
	.io_pin_allocation_mode = PIN_MODE_DUMB_24,
	.dumb_mode		= 6,  /*RGB888*/
#endif
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the panel is hard connected with lcd panel path and dsi1 output
	 */
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.active			= 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.max_fb_size		= FB_XRES * FB_YRES * 8 + 4096,
	.vdma_enable            = 1,
	.phy_type		= DPI,
	.pxa168fb_lcd_power     = &brownstone_lcd_power,
	.default_fbbase = 0,
	.default_fbsize = 0,
};

static void turn_on_backlight(void)
{
	/* lcd */
	uint32_t clk_rst;
	int lcd_bl_pwr_en;
	lcd_bl_pwr_en = mfp_to_gpio(MFP_PIN_GPIO116);
	
	int duty_ns = 1200000, period_ns = 2000000;
	unsigned long period_cycles, prescale, pv, dc;

	// Set up the back light
	clk_rst = __raw_readl(0xD4015000 + 0x0044);
	clk_rst &= ~(((0x7) & 0xf) << 4);
	clk_rst |= (1 << 1) | (((0x0) & 0xf) << 4);
	__raw_writel(clk_rst, 0xD4015000 + 0x0044);
	udelay(1000);
	clk_rst |= (1 << 0);
	__raw_writel(clk_rst, 0xD4015000 + 0x0044);
	udelay(1000);
	clk_rst &= ~(1 << 2);
	__raw_writel(clk_rst, 0xD4015000 + 0x0044);

	period_cycles = 52000;
	if (period_cycles < 1)
		period_cycles = 1;

	prescale = (period_cycles - 1) / 1024;
	pv = period_cycles / (prescale + 1) - 1;

	if (prescale > 63)
		return ;

	if (duty_ns == period_ns)
		dc = (1 << 10);
	else
		dc = (pv + 1) * duty_ns / period_ns;

	__raw_writel(prescale, 0xD401A800 + 0x00);
	__raw_writel(dc, 0xD401A800 + 0x04);
	__raw_writel(pv, 0xD401A800 + 0x08);

	//
	mdelay(5);
	gpio_set_output(lcd_bl_pwr_en);
	gpio_set_value(lcd_bl_pwr_en, 0);/*active low*/
	

}

static void fill_buffer_rgb888pack_checks(void* p, unsigned int w, unsigned int h, unsigned int wc, unsigned int hc, unsigned int c1, unsigned int c2)
{
    unsigned int i, j;
    unsigned char * pp;

    pp = p;
    for (j = 0; j < h; j++)
    {
        for (i = 0; i < w; i++)
        {
            *pp++ = 0xff;
            *pp++ = 0x00;
            *pp++ = 0x00;
        }
    }
}

static void fill_buffer_rgb565_checks(void* p, unsigned int w, unsigned int h, unsigned int wc, unsigned int hc, unsigned int c1, unsigned int c2)
{
    unsigned int i, j;
    unsigned short * pp;

    pp = p;
    for (j = 0; j < h; j++)
    {
        for (i = 0; i < w; i++)
        {
            *pp++ = ((((i/wc)+(j/hc))%2 == 0)?((unsigned short)c1):((unsigned short)c2));
        }
    }
}

static void draw_pattern(int w,int h,struct pxa168fb_mach_info* fbinfo){
    if(PIX_FMT_RGB565==fbinfo->pix_fmt)
        fill_buffer_rgb565_checks(fbinfo->default_fbbase, w, h, 20, 20, 0xFFFF, 0x0000);
    else if(PIX_FMT_RGB888PACK==fbinfo->pix_fmt)
        fill_buffer_rgb888pack_checks(fbinfo->default_fbbase, w, h, 20, 20, 0xFFFF, 0x0000);
}

#define VIDEO_PIXEL_SIZE 2
#define VIDEO_DATA_FORMAT GDF_16BIT_565RGB

#if defined(CONFIG_PXA168_FB)

static void *mmp2_lcd_init(void* base,int size)
{
	static void *ret;
	static int lcd_inited=0;

	if(lcd_inited)
		return ret;


	mmp2_parallel_lcd_info.default_fbbase= base?base:DEFAULT_FB_BASE;
	mmp2_parallel_lcd_info.default_fbsize = size?size:DEFAULT_FB_SIZE;

	
	ret = (void *)pxa168fb_init(&mmp2_parallel_lcd_info);
	//enable backlight after bitmap draw finished
	//turn_on_backlight();

	return ret;	
}
#else
static void *mmp2_lcd_init(void* base,int size){return NULL;}
#endif

#if defined(CONFIG_VIDEO) || defined(CONFIG_CFB_CONSOLE)
/*
 * The Graphic Device
 */

void *video_hw_init(void)
{
	static GraphicDevice ctfb;
	struct pxa168fb_info *fbi = (struct pxa168fb_info *) mmp2_lcd_init(0,0);
	struct fb_var_screeninfo *var = fbi->var;
	unsigned long t1, hsynch, vsynch;

	ctfb.winSizeX = var->xres;
	ctfb.winSizeY = var->yres;

	/* calculate hsynch and vsynch freq (info only) */
	t1 = (var->left_margin + var->xres +
		var->right_margin + var->hsync_len) / 8;
	t1 *= 8;
	t1 *= var->pixclock;
	t1 /= 1000;
	hsynch = 1000000000L / t1;
	t1 *= (var->upper_margin + var->yres +
		var->lower_margin + var->vsync_len);
	vsynch = 1000000000L / t1;

	/* fill in Graphic device struct */
	sprintf(ctfb.modeIdent, "%dx%dx%d %ldkHz %ldHz", ctfb.winSizeX,
		ctfb.winSizeY, var->bits_per_pixel, (hsynch / 1000),
		vsynch);

	ctfb.frameAdrs = (unsigned int) fbi->fb_start;
	ctfb.plnSizeX = ctfb.winSizeX;
	ctfb.plnSizeY = ctfb.winSizeY;

	ctfb.gdfBytesPP = VIDEO_PIXEL_SIZE;
	ctfb.gdfIndex = VIDEO_DATA_FORMAT;

	ctfb.isaBase = 0x9000000;
	ctfb.pciBase = (unsigned int) fbi->fb_start;
	ctfb.memSize = fbi->fb_size;    //320*480*2;

	/* Cursor Start Address */
	ctfb.dprBase = (unsigned int) fbi->fb_start + (ctfb.winSizeX * ctfb.winSizeY * ctfb.gdfBytesPP);        
	if ((ctfb.dprBase & 0x0fff) != 0) {
		/* allign it */
		ctfb.dprBase &= 0xfffff000;
		ctfb.dprBase += 0x00001000;
	}
	ctfb.vprBase = (unsigned int) fbi->fb_start;
	ctfb.cprBase = (unsigned int) fbi->fb_start;

	//background color
	ctfb.bg = 0;

	return &ctfb;
}

int lcd_draw_bmp(u8* bmp_image,int mode)
{
	bmp_image_t *bmp = (bmp_image_t *) bmp_image;
	int screen_w = FB_XRES;
	int screen_h = FB_YRES;
	int x,y;
	if (!((bmp->header.signature[0] == 'B') &&
		  (bmp->header.signature[1] == 'M'))) {
		  printf("invalid bmp file\n");
		  return -1;
	}
	if (mode)
	{
		x = y = 0;
	}
	else
	{
        int width, height;
	    width = height = 0;
        if (((bmp->header.signature[0] == 'B') &&
              (bmp->header.signature[1] == 'M'))) {
            width = le32_to_cpu (bmp->header.width);
            height = le32_to_cpu (bmp->header.height);
        }

		//fix issue that height value may be negative 
		if(height<0) height=-height;
		x = max(0, (screen_w- width) / 2);
		y = max(0, (screen_h- height) / 2);
	}

	printf("draw bmp from 0x%x with mode[%d] @[%d,%d]\n",bmp_image,mode,x,y);
	
	return video_display_bitmap((unsigned long)bmp_image,x,y);
}


int lcd_draw_default_bmp(int mode)
{
#define LOGO_READ_ADDRESS 0x1500000
#define BLOCK_SIZE 512
	void* splash = LOGO_READ_ADDRESS;
	
	if(bmp_manager_readbmp("splash",splash,CONFIG_SYS_VIDEO_LOGO_MAX_SIZE)||lcd_draw_bmp(splash,mode)){
        //draw pattern
        draw_pattern(FB_XRES,FB_YRES,&mmp2_parallel_lcd_info);
    }
 	mdelay(200);
    //turn on bl
    turn_on_backlight();

	
}

int lcd_clear(void)
{
	memset((unsigned short *) DEFAULT_FB_BASE, 0x00, FB_XRES * FB_YRES *4);

	return 0;

}

static int do_disp(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]){
    if(argc<2){
        draw_pattern(FB_XRES,FB_YRES,&mmp2_parallel_lcd_info);
    }else if(!strcmp(argv[1],"vga")){        
        #ifdef CONFIG_CFB_CONSOLE
        lcd_clear();    
        video_console_refresh();
        console_assign(stdout, "vga");
        #endif  
    }else if(!strcmp(argv[1],"serial")){        
            console_assign(stdout, "serial");
    }else if(!strcmp(argv[1],"bmp")){
        if(argc<3)
            draw_pattern(FB_XRES,FB_YRES,&mmp2_parallel_lcd_info);        
        else{            
            void* splash = LOGO_READ_ADDRESS;
            bmp_manager_readbmp(argv[2],splash,DEFAULT_FB_SIZE);
            lcd_draw_bmp(splash,0);
            mdelay(200);
            turn_on_backlight();
        }
        
    }else{    
        printf ("Usage:\n%s\n", cmdtp->usage);
        return 1;
    }

    
	return 0;
}

U_BOOT_CMD(
        disp, CONFIG_SYS_MAXARGS, 0, do_disp, 
        "disp [serial|vga] - show disp program\n", 
        "disp [serial|vga]\n"
        "    - show disp program\n"
        "disp bmp [bmpname]\n"
);


#else
int lcd_draw_bmp(u8* bmp_image,int mode){return -1;}
int lcd_draw_default_bmp(int mode){return -1;}
int lcd_clear(void){return 0;}

#endif 						/* defined(CONFIG_VIDEO) || defined(CONFIG_CFB_CONSOLE) */

