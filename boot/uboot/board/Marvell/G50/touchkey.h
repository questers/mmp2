#ifndef _TOUCHKEY_H_
#define _TOUCHKEY_H_

#define TOUCHKEY_HOME	0x1
#define TOUCHKEY_MENU	0x2
#define TOUCHKEY_BACK	0x4
#define TOUCHKEY_SEARCH	0x8


//bus:i2c bus number (0 based)
//attention:gpio of attention
int touchkey_init(int bus,int attention);

//return touchkey mask if checked within timeout_ms time
//return 0 if no any key 
int touchkey_check(int timeout_ms);



#endif 

