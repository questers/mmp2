#include <common.h>
#include <i2c.h>
#include "tc35876x.h"

#define TC35876x_I2C_ADDR_7BIT 0x0b
#define SWAP16(x) __swab16(x)

int tc35876x_read16(u16 reg, u16 *pval)
{
	uint addr = SWAP16(reg);
	#ifdef CONFIG_I2C_MULTI_BUS
	i2c_set_bus_num(4);
	#endif
	i2c_read(TC35876x_I2C_ADDR_7BIT,reg,2,(uchar*)pval,2);
	
	return 0;
}

int tc35876x_read32(u16 reg, u32 *pval)
{
	uint addr = SWAP16(reg);
#ifdef CONFIG_I2C_MULTI_BUS
	i2c_set_bus_num(4);
#endif
	i2c_read(TC35876x_I2C_ADDR_7BIT,reg,2,(uchar*)pval,4);
	return 0;
}

int tc35876x_write32(u16 reg, u32 val)
{
	uint addr = SWAP16(reg);
	uchar buffer[4];
#ifdef CONFIG_I2C_MULTI_BUS
	i2c_set_bus_num(4);
#endif
	buffer[0]=  val & 0xff;
	buffer[1] = (val >> 8) & 0xff;
	buffer[2] = (val >> 16) & 0xff;
	buffer[3]=  (val >> 24) & 0xff;

	i2c_write(TC35876x_I2C_ADDR_7BIT,reg,2,buffer,4);
	return 0;
}

int tc35876x_write16(u16 reg, u16 val)
{
	uint addr = SWAP16(reg);
	uchar buffer[4];
#ifdef CONFIG_I2C_MULTI_BUS
	i2c_set_bus_num(4);
#endif
	buffer[0]=  val & 0xff;
	buffer[1] = (val >> 8) & 0xff;

	i2c_write(TC35876x_I2C_ADDR_7BIT,reg,2,buffer,2);
	return 0;
}

