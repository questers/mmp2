#include <common.h>
#include <asm/io.h>
#include <asm/arch/common.h>
#include <mfp.h>
#include "lcd_init.h"


#undef mdelay
#define mdelay(a) ldelay(a*5000)

static int lcd_power = -1;
static int spi_sdi = -1;
static int spi_sdo = -1;
static int spi_sclk = -1;
static int spi_cs = -1;
static int lcd_reset = -1;

static void
ldelay(unsigned  int delay)
{
	volatile unsigned int i;
	for(i = 0; i < delay; i++);
}

static void spi_addr(unsigned short addr)
{
    unsigned int i;
    int j;

    //mmc_print("addr - %02x\n", addr);
    gpio_set_value(spi_cs, 0);    
    gpio_set_value(spi_sclk, 1);    
    
    i = addr | 0x7000;
    for (j = 0; j < 9; j++) {
        gpio_set_value(spi_sclk, 0);    
        udelay(2);

        if (i & 0x0100)
            gpio_set_value(spi_sdo, 1);    
        else
            gpio_set_value(spi_sdo, 0);    

        udelay(2);
        gpio_set_value(spi_sclk, 1);    
        udelay(2);
        i <<= 1;
    }

    gpio_set_value(spi_cs, 1);    
}

static void spi_data(unsigned short data)
{
    unsigned int i;
    int j;

    //mmc_print("\tdata - %02x\n", data);
    gpio_set_value(spi_cs, 0);    
    gpio_set_value(spi_sclk, 1);    

    i = data | 0x7100;

    for (j = 0; j < 9; j++) {
        gpio_set_value(spi_sclk, 0);
        udelay(2);
        
        if (i & 0x0100)
            gpio_set_value(spi_sdo, 1);    
        else
            gpio_set_value(spi_sdo, 0);    

        udelay(2);
        gpio_set_value(spi_sclk, 1);    
        udelay(2);
        i <<= 1;
    }

    gpio_set_value(spi_cs, 1);   
}

void init_mcu(void)
{
    spi_addr(0xB9);  // SET password
    spi_data(0xFF);
    spi_data(0x83);
    spi_data(0x69);

    spi_addr(0xB1);  //Set Power
    spi_data(0x85);
    spi_data(0x00);
    spi_data(0x34);
    spi_data(0x06);
    spi_data(0x00);
    spi_data(0x10);
    spi_data(0x10);
    spi_data(0x2F);
    spi_data(0x3A);
    spi_data(0x3F);
    spi_data(0x3F);
    spi_data(0x07);
    spi_data(0x23); //23
    spi_data(0x01);
    spi_data(0xE6);
    spi_data(0xE6);
    spi_data(0xE6);
    spi_data(0xE6);
    spi_data(0xE6);

    spi_addr(0xB2);  // SET Display  480x800
    spi_data(0x00);
    spi_data(0x2B);
    spi_data(0x08);
    spi_data(0x2A);
    spi_data(0x70);
    spi_data(0x00);
    spi_data(0xFF);
    spi_data(0x00);
    spi_data(0x00);
    spi_data(0x00);
    spi_data(0x00);
    spi_data(0x03);
    spi_data(0x03);
    spi_data(0x00);
    spi_data(0x01);

    //spi_addr(0x2C);

    spi_addr(0xB3);
    spi_data(0x0F);
    
    //spi_addr(0xB0);
    //spi_data(0x01);
    //spi_data(0x0b);

    spi_addr(0xB4);  // SET Display  480x800
    spi_data(0x00);
    spi_data(0x0C);   //0C
    spi_data(0xA0); //A0
    spi_data(0x0E);  // 0E
    spi_data(0x06);   //06

    spi_addr(0xB6);  // SET VCOM
    spi_data(0x31);
    spi_data(0x31);

    spi_addr(0xD5);
    spi_data(0x00);
    spi_data(0x05);
    spi_data(0x03);
    spi_data(0x00);
    spi_data(0x01);
    spi_data(0x09);
    spi_data(0x10);
    spi_data(0x80);
    spi_data(0x37);
    spi_data(0x37);
    spi_data(0x20);
    spi_data(0x31);
    spi_data(0x46);
    spi_data(0x8A);
    spi_data(0x57);
    spi_data(0x9B);
    spi_data(0x20);
    spi_data(0x31);
    spi_data(0x46);
    spi_data(0x8A);
    spi_data(0x57);
    spi_data(0x9B);
    spi_data(0x07);
    spi_data(0x0F);
    spi_data(0x02);
    spi_data(0x00);

    spi_addr(0xE0);
    spi_data(0x01);
    spi_data(0x08);
    spi_data(0x0D);
    spi_data(0x2D);
    spi_data(0x34);
    spi_data(0x37);
    spi_data(0x21);
    spi_data(0x3C);
    spi_data(0x0E);
    spi_data(0x10);
    spi_data(0x0E);
    spi_data(0x12);
    spi_data(0x14);
    spi_data(0x12);
    spi_data(0x14);
    spi_data(0x13);
    spi_data(0x19);

    spi_data(0x01);
    spi_data(0x08);
    spi_data(0x0D);
    spi_data(0x2D);
    spi_data(0x34);
    spi_data(0x37);
    spi_data(0x21);
    spi_data(0x3C);
    spi_data(0x0E);
    spi_data(0x10);
    spi_data(0x0E);
    spi_data(0x12);
    spi_data(0x14);
    spi_data(0x12);
    spi_data(0x14);
    spi_data(0x13);
    spi_data(0x19);

    spi_addr(0x36);  //Set_address_mode
    spi_data(0x10); //����X�᾵��
    spi_addr(0x2C);

    spi_addr(0xCC); //Set_address_mode
    spi_data(0x02);  //SS,REV,BGR  0x0A
    spi_addr(0x2C);

    spi_addr(0x3A);   //Set_pixel_format
    spi_data(0x66);

    spi_addr(0x11);
    mdelay(120);

    spi_addr(0x29);

    spi_addr(0x2C);
}

void lcd_init(int on)
{
    int gpio;

    lcd_power = mfp_to_gpio(MFP_PIN_GPIO120);
    spi_sdi = mfp_to_gpio(MFP_PIN_GPIO96);
    spi_sdo = mfp_to_gpio(MFP_PIN_GPIO97);
    spi_cs = mfp_to_gpio(MFP_PIN_GPIO101);
    spi_sclk = mfp_to_gpio(MFP_PIN_GPIO100);
   	lcd_reset = mfp_to_gpio(MFP_PIN_GPIO141);

    //gpio_set_output(lcd_power);
	*(volatile unsigned int *)0xd4019154 = 0x1000000; // powerpin output
    gpio_set_output(spi_sdi);
    gpio_set_output(spi_sdo);
    gpio_set_output(spi_cs);
    gpio_set_output(spi_sclk);

	//gpio_set_value(lcd_power, !on);
	if(on)
		*(volatile unsigned int *)0xd4019124 = 0x1000000; // powerpin clear value
	else
		*(volatile unsigned int *)0xd4019118 = 0x1000000; // powerpin clear value
	
	////gpio_set_value(lcd_power, 0);
    if (on)
    {
    	if(-1 != lcd_reset)
		{
			//gpio_set_output(lcd_reset);
			*(volatile unsigned int *)0xd4019158 = 0x2000;	// resetpin output
			//gpio_set_value(lcd_reset, 1);
			*(volatile unsigned int *)0xd401911c = 0x2000;	// resetpin set value
        	ldelay(2500);
			//gpio_set_value(lcd_reset,0);
			*(volatile unsigned int *)0xd4019128 = 0x2000;	//resetpin clear value
        	ldelay(2500);
        	//gpio_set_value(lcd_reset, 1);
			*(volatile unsigned int *)0xd401911c = 0x2000;	// resetpin set value
        	mdelay(200);
    	}
        init_mcu();
		init_mcu();
    }
    else
    {
		if(-1 != lcd_reset)
		{
			//gpio_set_value(lcd_reset, 0);
			*(volatile unsigned int *)0xd4019128 = 0x2000;	//resetpin clear value
		}
	}
    printf("lcd init done\n");
}

static int cmd_get_unit(char* arg, int default_size)
{
	/* Check for a size specification .b, .w or .l.
	 */
	int len = strlen(arg);
	if (len > 2 && arg[len-2] == '.') {
		switch(arg[len-1]) {
		case 'u':
			return 1;
		case 'm':
			return 1000;
		case 's':
			return 1000000;
		default:
			break;
		}
	}
	return default_size;
}

static int do_delay (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	ulong us;
    int unit;
	if (argc != 2)
		return cmd_usage(cmdtp);
		
    unit = cmd_get_unit(argv[0], 1);
    //use lcd power pin to test
   // *(volatile unsigned int *)0xd4019154 = 0x1000000; // powerpin output

	us = simple_strtoul(argv[1], NULL, 10);
	us*=unit;

	printf("delay %d us\n",us);

   // *(volatile unsigned int *)0xd4019118 = 0x1000000; // powerpin set value
	udelay (us);
  //  *(volatile unsigned int *)0xd4019124 = 0x1000000; // powerpin clear value

	return 0;
}

U_BOOT_CMD(
	delay ,    2,    1,     do_delay,
	"delay execution for some time",
	"N\n"
	"[.u, .m, .s]    - delay execution for N unit(default is us)time (N is _decimal_ !!!)"
);


