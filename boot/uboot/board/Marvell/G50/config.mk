#
# image should be loaded at 0x00F00000
#
# UBOOT relocate at 0x1100000
# and we could safely download to 0x1200000
# For UBOOT less than 0x100000 = (512K)
#

ifeq ($(CONFIG_PJ4_NON_SECURE_MODE),y)
TEXT_BASE = 0x1100000
else
TEXT_BASE = 0xF00000
endif

# refer to <kernel_root>/arch/arm/tools/mach-types
MACHINE_ID=\""daa"\"

PLATFORM_CPPFLAGS += -DTARGET_PRODUCT_$(TARGET_PRODUCT) -DMACHINE_ID=$(MACHINE_ID)

