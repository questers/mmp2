#include <asm/arch/gpio.h>
#include <common.h>
#include <command.h>
#include <i2c.h>
#include "touchkey.h"

#undef TOUCHKEY_DEBUG

//comment next line to disable verbose debug messages
//#define TOUCHKEY_DEBUG 

/*Configuration Registers*/
#define REG_CONFIG_INTERFACE 	0x0000
#define REG_CONFIG_GENERAL   	0x0001
#define REG_CONFIG_BUTTON    	0x0004
#define REG_CONFIG_GPIO			0x000E
#define REG_CONFIG_SENSITIVITY1	0x0010
#define REG_CONFIG_SENSITIVITY2	0x0011
#define REG_CONFIG_MAPPING		0x001E
#define REG_CONFIG_TIMER		0x001F
#define REG_CONFIG_LED_ENABLE	0x0022
#define REG_CONFIG_LED_EFFECT	0x0023
#define REG_CONFIG_LED_CONTROL1	0x0024
#define REG_CONFIG_LED_CONTROL2	0x0025
/*Data Registers*/
#define REG_DATA_GPIO			0x0108
#define REG_DATA_BUTTON			0x0109
#define REG_DATA_TIMER			0x010B
#define REG_DATA_PRESSURE1		0x010C
#define REG_DATA_PRESSURE2		0x010D
/*Reset Registers*/
#define REG_RESET				0x0300


#define LED_MODE_COMBO			0x1

#define LED_MODE (LED_MODE_COMBO)

#define LED_MAX_BRIGHTNESS		24
#define MAX_TOUCH_BUTTONS		4

#define LED_IDLE_TIME  2000
#define LED_ONOFF_TIME 100 

#define LED_COMMAND_NONE 0
#define LED_COMMAND_SET_BRIGHTNESS 1

#define LED_BRIGHTNESS_OFFSET      3

/*driver private object*/
struct so340010_priv
{
	int init;

	u8  bus;
	u8  addr;
	int att;

	//led control
	u8 led_mode;	
	u8 max_brightness;
	u8 led_on;
	

	u32 touch_state;
};
static struct so340010_priv touchkey;



static int so340010_slave_read(struct so340010_priv* priv, u16 addr, u16* data)
{
	uint data_addr;
	u8 buf[2];	

	if(i2c_read(priv->addr,addr,2,buf,2))
	{
		printf("Error reading so340010 slave\n");
		return -1;
	}

	if(data)
		*data = (buf[0]<<8)+buf[1];
	
	return 0;
}

static int so340010_slave_write(struct so340010_priv* priv, u16 addr, u16 data)
{
	uint data_addr;
	u8 buf[2];	

	buf[0] = (data>>8)&0xff;
	buf[1] =  data&0xff;	
	if (i2c_write(priv->addr,addr,2,buf,2))
	{
		printf("Error writting so340010 slave\n");
		return -1;
	}
	
	return 0;
}

static int so340010_state_process(struct so340010_priv* priv)
{
	int i;
	int ret=0;
	u16 data;
	int code[4] = {TOUCHKEY_HOME, TOUCHKEY_MENU, TOUCHKEY_BACK, TOUCHKEY_SEARCH}; /* MENU, HOME, BACK,SEARCH */
	#ifdef TOUCHKEY_DEBUG
	static const char* scan_code[]={"home","menu","back","search"};
	#endif
	ret = so340010_slave_read(priv, REG_DATA_BUTTON, &data); /* button state register */
	if (ret)
	{
		printf("read touchkey failed\n");
		return 0;
	}

	priv->touch_state = data;

	
	#ifdef TOUCHKEY_DEBUG
	printf("so340010_state_process state=%08x\n",priv->touch_state);
	#endif
	for (i=0; i<MAX_TOUCH_BUTTONS; i++)
	{

		if (priv->touch_state&(1<<i))
		{
			#ifdef TOUCHKEY_DEBUG
			printf("Button %s pressed.\n", scan_code[i]);
			#endif
			ret|=code[i];

		}

	}

	
	return ret;
}





//show a key checked magic,
static void so340010_show_magic(struct so340010_priv* priv,int magic_keys,int time_ms)
{
	u16 on=0x0f00+~((u8)magic_keys);
	u16 off=0x0f0f;
	do
	{
		//light all leds
		so340010_slave_write(priv,REG_CONFIG_GPIO,on);	

		udelay(50*1000);
		//run on timer
		//priv->onoff_running = 1;
		//ramp dwon to 0
		so340010_slave_write(priv,REG_CONFIG_GPIO,off);
		udelay(50*1000);
		time_ms-=100;
	}while(time_ms>0);
	
}
	


static int so340010_poll_attention(struct so340010_priv* priv,int timeout_ms)
{
	int state;
	do
	{
		state = gpio_get_value(priv->att);
		if(!state)
			return 0;
		if(!timeout_ms) break;
		udelay(1000);
		
	}while(--timeout_ms);

	printf("polling touchkey attention failed\n");
	return 0;

}

static int so340010_chip_init(struct so340010_priv* so340010,int timeout_ms)
{
	u16 data,brightness;

	if(so340010_poll_attention(so340010,timeout_ms))
		goto init_fail;
	/* configure cap button */
	if(so340010_slave_write(so340010, REG_CONFIG_INTERFACE, 0x0004)) /* data enable mode */
		goto init_fail;
	/*normal mode,strongest button only mode*/
	if(so340010_slave_write(so340010, REG_CONFIG_GENERAL, 0x0010))
		goto init_fail;

	/* enable s0,s1,s2 buttons */
	if(so340010_slave_write(so340010, REG_CONFIG_BUTTON, 0x0007))
		goto init_fail;

	/*
	 * For unused pin,use low driving output;
	 *	low to driver led on
	 */
	if(so340010_slave_write(so340010, REG_CONFIG_GPIO, 0x0f0f))
		goto init_fail;

	/*with maximum sensitivity for all sensor pins,ignored by disabled sensor pins*/
	if(so340010_slave_write(so340010, REG_CONFIG_SENSITIVITY1, 0x8888))
		goto init_fail;;
	if(so340010_slave_write(so340010, REG_CONFIG_SENSITIVITY2, 0x8888))
		goto init_fail;
	/*in combo mode,led is controlled by driver,otherwise it's controlled by chip itself*/
	if(LED_MODE_COMBO&so340010->led_mode)
	{
		if(so340010_slave_write(so340010, REG_CONFIG_MAPPING, 0x0000))
			goto init_fail;
	}
	else
	{
		/*toggle on touch mode,map to gpio data bit,s0,s1,s2 mapped gpio0,gpio1,gpio2*/
		if(so340010_slave_write(so340010, REG_CONFIG_MAPPING, 0x400f))
			goto init_fail;
	}

	/*disable timer function*/
	if(so340010_slave_write(so340010, REG_CONFIG_TIMER, 0x0000))
		goto init_fail;

	/*enable GPO0,GPO1,GPO2 led driving*/	
	if(so340010_slave_write(so340010, REG_CONFIG_LED_ENABLE, 0x0/*0x000f*/))
		goto init_fail;

	/* period A and B, 80*12.5ms = 1000ms */
	if(so340010_slave_write(so340010, REG_CONFIG_LED_EFFECT, 0x5050))
		goto init_fail;

	/* effect/brightness */	
	brightness = (so340010->max_brightness<<8)+so340010->max_brightness;
	if(so340010_slave_write(so340010,REG_CONFIG_LED_CONTROL1,brightness))
		goto init_fail;
	if(so340010_slave_write(so340010,REG_CONFIG_LED_CONTROL2,brightness))
		goto init_fail;

	/* dummy read to deassert ATTN pin*/
	if(so340010_slave_read(so340010, REG_DATA_GPIO, &data))
		goto init_fail;

	return 0;
init_fail:
	return -1;
	
}

static void touchkey_regdump(struct so340010_priv* touch)
{
	u16 regs[]=
	{
		0,1,4,0xe,
		0x10,0x11,0x1e,0x1f,
		0x22,0x23,0x24,0x25,
		0xffff
	}; 
	u16 *addr=&regs[0];
	u16 data=0;
	printf("\n\n=====touchkey regdump:======\n");
	do
	{
		if(!so340010_slave_read(touch,*addr,&data))
		{
			printf("reg[0x%x]=0x%x\n",*addr,data);
		}
		addr++;
	}while(*addr!=0xffff);
	printf("==================================\n");
}
int touchkey_init(int bus,int attention)
{
	struct so340010_priv* touch=&touchkey;
	u16 interface_status=0;
	int ret;
	memset(touch,0,sizeof(struct so340010_priv));

	touch->bus = bus;
	touch->att = attention;
	touch->addr= 0x2c;

	touch->led_mode = LED_MODE;
	touch->max_brightness = LED_MAX_BRIGHTNESS;
	touch->led_on = 0;
	
	//init onetouch now
	#ifdef CONFIG_I2C_MULTI_BUS
	i2c_set_bus_num(touch->bus);
	#endif
	ret = so340010_chip_init(touch,50);
	if(ret)
	{
		printf("touchkey init failed, ret = %d\n", ret);	
		return -1;
	}

	touch->init = 1;

	printf("touchkey init okay\n");
	return 0;
}

int touchkey_check(int timeout_ms)
{
	int touch_keys=0;
	struct so340010_priv* touch=&touchkey;
	if(!touch->init)
		return 0;
	if(timeout_ms<0)
		timeout_ms = 0;
	do
	{
		//check attention
		//if(!gpio_get_value(touch->att))
		{
		//	printf("touchkey event \n");
			touch_keys = so340010_state_process(touch);
			if(touch_keys)
			{
				so340010_show_magic(touch,touch_keys,1000);
				break;
			}
		}
		udelay(50000);
		timeout_ms-=50;
	}while(timeout_ms>0);

	return touch_keys;
}

static int do_keycheck(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	int keys;
	int timeout=100;
	if(argc>1)		
		timeout = simple_strtoul(argv[1], NULL, 10);
	keys=touchkey_check(timeout);
	if(!keys)
		printf("no keys pressed\n");
	if(keys&TOUCHKEY_HOME)
		printf("home pressed\n");
	if(keys&TOUCHKEY_MENU)
		printf("menu pressed\n");
	if(keys&TOUCHKEY_BACK)
		printf("back pressed\n");
	if(keys&TOUCHKEY_SEARCH)
		printf("search pressed\n");
	
	return 0;
}

U_BOOT_CMD(
               key,    2,      1,      do_keycheck,
               "[timeout_ms]	- Check touchkey state",
               "[timeout_ms]	- Check touchkey state"
);

static int do_touch(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	struct so340010_priv* touch=&touchkey;
	u16 regs[]=
	{
		0,1,4,0xe,
		0x10,0x11,0x1e,0x1f,
		0x22,0x23,0x24,0x25,
		0xffff
	}; 
	u16 *addr=&regs[0];
	u16 data=0;
	if(argc>1)		
		*addr = simple_strtoul(argv[1], NULL, 10);
	if(argc>2)
		data = simple_strtoul(argv[2], NULL, 10);
	if(argc>2) 
	{
		if(!so340010_slave_write(touch,*addr,data))
		{
			printf("write reg[0x%x]=0x%x\n",*addr,data);
		}
		if(!so340010_slave_read(touch,*addr,&data))
		{
			printf("reg[0x%x]=0x%x\n",*addr,data);
		}		
		
		
	}
	else if(argc>1)
	{
		if(!so340010_slave_read(touch,*addr,&data))
		{
			printf("reg[0x%x]=0x%x\n",*addr,data);
		}		
	}
	else
	{
		printf("\n\n=====touchkey regdump:======\n");
		do
		{
			if(!so340010_slave_read(touch,*addr,&data))
			{
				printf("reg[0x%x]=0x%x\n",*addr,data);
			}
			addr++;
		}while(*addr!=0xffff);
		printf("==================================\n");
	}
	
	return 0;
}

U_BOOT_CMD(
               touch,    CONFIG_SYS_MAXARGS,      1,      do_touch,
               "touch [addr] [value] - read/write touch register",
               "touch [addr] - read/write touch register"
);


