#include <asm/arch/gpio.h>
#include <malloc.h>
#include <common.h>
#include <command.h>
#include <i2c.h>
#include <bmp_layout.h>
#include <bmpmanager.h>
#include "battery.h"

#ifdef CONFIG_BATTERY_MONITOR

#define ads_dbg(fmt, arg...) do { if (debug) printf(fmt, ##arg); } while(0)
#define msleep(a) udelay(a * 1000)

struct ads1000_device_info;
struct ads1000_access_methods {
	int (*read)(struct ads1000_device_info* thiz,uint8_t *buf, int len);
	int (*write)(struct ads1000_device_info *thiz,uint8_t *buf, int len);
};

struct ads1000_device_info {
	struct device *dev;
	struct ads1000_access_methods *bus_op;
	u8 addr;
	u8 bus_num;
	unsigned int interval;
	/* battery parameters */
	int bat_volt;
	int bat_cap;
	int conv_mode;		/* conversion mode: 1: single, 0: continuous */
	int dc_in_st;		/* DC_IN status: DC online: 1, offline: 0 */
	int vdd;
	/* GPIOs: DC_IN, STAT1 and STAT2 */
	int dc_in;

	int sample_last;
};

/* Charging status indicated by GPIO STAT1&STAT2 */
enum charge_states {
	CHARGE_STAT_CHARGING,	/* Precharge or fast charge in progress */
	CHARGE_STAT_CHG_COMP,	/* Charge Complete */
	CHARGE_STAT_FAULT,	/* Fault */
	CHARGE_STAT_SUSPEND,	/* Suspend */
};


#define VDD_VOLTAGE	(2940)
#define VALID_VOLT(v) ((v)>=3000&&(v)<=5000)
#define DISCHARGE_JITTER_THRESHOLD 25
#define DISCHARGE_JITTER_COUNT 5

static int debug = 0;

struct capacity_percent {
	int voltage;
	int percent;
};
/* Battery Capacity Table: Voltage vs Capacity */
static struct capacity_percent cap_table[] = {
	{3400, 0},
	{3500, 5},
	{3600, 15},
	{3650, 35},
	{3740, 60},		
	{3870, 80},		
	{4060, 99},
};

struct capacity_correct {
	int voltage;
	int correct;
};
#define CORRECT(a, b) {(b), ((a)-(b))}
/* Battery Correct Table: Voltage vs Correction */
static struct capacity_correct correct_table[] = {
	CORRECT(3700, 3400),
	CORRECT(3716, 3495),
	CORRECT(3816, 3595),
	CORRECT(3916, 3695),
	CORRECT(3930, 3710),
	CORRECT(3960, 3740),
	CORRECT(4000, 3780),
	CORRECT(4010, 3890),
	CORRECT(4060, 3920),
	CORRECT(4100, 4000),
	CORRECT(4130, 4040),
	CORRECT(4160, 4080),
};

/* global device info for ads1000 */
static struct ads1000_device_info *g_di;


static int abs_val(int val)
{
	if(val>=0) return val;
	return -val;
}
/* Return the voltage in milivolts */
static int calculate_voltage(int out_code, struct ads1000_device_info *di)
{
	int offset = 0, val = 0;
	int vdd = di->vdd;
	int i, size;
	int valid = 0;

	/* Output Code = 2048(PGA)((VIN+ - VIN-)/VDD)
	 * For G50 platform:
	 * VIN- = 0, PGA = 1, voltage divider: 2/3
	 */
	val = out_code * vdd / 2048;
	val *= 3;
	val /= 2;
	ads_dbg("val: %d\n",val);

	if (!VALID_VOLT(val))
		return 0;

	/*
	 * possible sample sequence:
	 * (a) 4069, 3882, dc_in(0), 3882, 3892, 3898, 3903, dc_in(1), 3993, 4069, 4069, 4069, ...
	 * (b) 3610, 3610, dc_in(0), 3318, 3357, 3352, 3348, ..., 3330, dc_in(1), 3759, 3592, 3592, ...
	 */
	valid = 0;
	if (di->sample_last == -1) valid = 1;
	if (abs_val(val - di->sample_last) <= 5) valid = 1;
	ads_dbg("sample: %d\n", val);
	di->sample_last = val;

	if (!valid) 
		return 0;

	if (di->dc_in_st)
	{
		size = ARRAY_SIZE(correct_table);
		if (val >= correct_table[size - 1].voltage) {
			offset = correct_table[size - 1].correct;
		} else if (val <= correct_table[0].voltage) {
			offset = correct_table[0].correct;
		} else {
			for (i = size - 2; i >= 0; i--) {
				int c1 = correct_table[i+1].correct;
				int c2 = correct_table[i+0].correct;
				int v1 = correct_table[i+1].voltage;
				int v2 = correct_table[i+0].voltage;

				if (val < correct_table[i].voltage)
					continue;

				offset = (c1-c2)*(val-v2)/(v1-v2)+c2;
				break;
			}
		}
	}
	else
	{
	    offset = 0;
	}

    ads_dbg("sample: %d, correction: %d\n", val, offset);

	/* calculate actual battery voltage */
	val -= offset;
	if (val < 0)
		val = 0;

	return val;
}

/* Return capacity of battery, in percent */
static int get_bat_capacity(int volt)
{
	int i, size;
	int ret = 0;

	size = ARRAY_SIZE(cap_table);
	if (volt >= cap_table[size - 1].voltage) {
		ret = 100;
		return ret;
	} else if (volt <= cap_table[0].voltage) {
		ret = 0;
		return ret;
	}
	for (i = size - 2; i >= 0; i--) {
		int p1 = cap_table[i+1].percent;
		int p2 = cap_table[i+0].percent;
		int v1 = cap_table[i+1].voltage;
		int v2 = cap_table[i+0].voltage;
		
		if (volt < cap_table[i].voltage)
			continue;
		
		ret = (p1-p2)*(volt-v2)/(v1-v2)+p2;
		break;
	}
	return ret;
}

/*
 * Return the battery Voltage in milivolts
 * Or < 0 if something fails.
 */
static int get_bat_voltage(struct ads1000_device_info *di)
{
	int ret = 0, volt = 0;
	u8 cfg_reg, buf[3];
	int time_out = 100;
	int out_code;

	if (di->conv_mode) {	/* single mode */
		/* Using single conversion mode to save power */
		ret = di->bus_op->read(di,buf, 3);
		if (ret)
			goto err;
		cfg_reg = buf[2];
		cfg_reg |= (1 << 7);
		/* Start a conversion */
		ret = di->bus_op->write(di,&cfg_reg, 1);
		if (ret)
			goto err;
		/* Wait conversion complete */
		while (time_out >= 0) {
			ret = di->bus_op->read(di,buf, 3);
			if (ret)
				goto err;
			/* check ST/BSY bit */
			if (!(buf[2] & (1 << 7)))
				break;
			time_out--;
			msleep(1);
		}
		if (time_out < 0) {
			ads_dbg("ADC conversion timeout\n");
			goto err;
		}
	} else {		/* continuous mode */
		ret = di->bus_op->read(di,buf, 2);
		if (ret)
			goto err;
	}
	out_code = (buf[0] << 8) | buf[1];
	
	volt = calculate_voltage(out_code, di);

	return volt;
err:	/* error: return voltage zero */
	ads_dbg("get battery voltage failed\n");
	return 0;
}

#define to_ads1000_device_info(x) container_of((x), \
				struct ads1000_device_info, bat);




/*
 * i2c specific code
 */

static int ads1000_read_i2c(struct ads1000_device_info *thiz,uint8_t *buf, int len )
{

	#ifdef CONFIG_I2C_MULTI_BUS
	i2c_set_bus_num(thiz->bus_num);
	#endif

	if(i2c_read(thiz->addr,0,0,buf,len))
	{
		printf("Error reading ads1000\n");
		return -1;
	}
	
	return 0;
}

static int ads1000_write_i2c(struct ads1000_device_info *thiz,uint8_t *buf, int len )
{
	#ifdef CONFIG_I2C_MULTI_BUS
	i2c_set_bus_num(thiz->bus_num);
	#endif

	if (i2c_write(thiz->addr,0,0,buf,len))
	{
		printf("Error writting ads1000\n");
		return -1;
	}
	
	return 0;

}

static void ads1000_battery_update_state(struct ads1000_device_info *di)
{
	int volt;

	/* update battery voltage and capacity */
	volt = get_bat_voltage(di);
	if (VALID_VOLT(volt))
	{
		di->bat_volt = volt;
		di->bat_cap = get_bat_capacity(di->bat_volt);
		if (di->bat_cap > 100)
			di->bat_cap = 100;

		ads_dbg("capacity: %d%%\n", di->bat_cap);
	}
}

/* AD converter setup */
static int adc_setup(struct ads1000_device_info *di)
{
	u8 cfg_reg, buf[3] = { 0 };
	u8 ads1000_addr[2] = { 0x48,0x49 };
	int ret = 0;
	/* Set vdd voltage */
	di->vdd = VDD_VOLTAGE;
	/* Set as single conversion mode to save power */
	di->conv_mode = 1;
	/* Init ADC chip */
	//mach the IIC device addr
	//BD1 addr:1001001;BD0 addr:1001000
	int i,size;
	size = ARRAY_SIZE(ads1000_addr);
	for(i=0;i<size;i++)
	{
		di->addr = ads1000_addr[i];
		ret = ads1000_read_i2c(di,buf, 3);
		if (!ret) {
			ads_dbg("Mach the ads1000 addr:0x%x\n",di->addr);
			break;
		}
		if(i == (size-1))
		{
			ads_dbg("ERROR:no mach ads1000 addr,please check!\n");
			return -1;
		}
	}

	/* Third byte is the configuration register contents */
	cfg_reg = buf[2];

	/* Configuration register:
	 * bit7: ST/BSY
	 * bit4: conversion mode(1: single, 0: continuous)
	 * bit[1,0]: PGA(Gain) setting */
	if (di->conv_mode)
		cfg_reg |= (1 << 4);	/* single */
	else
		cfg_reg &= ~(1 << 4);	/* continuous */
	/* set gain to 1 */
	cfg_reg &= (~0x3);
	/* configure adc */
	ret = ads1000_write_i2c(di,&cfg_reg, 1);
	if (ret < 0) {
		ads_dbg("failed to configure ads1000\n");
		return ret;
	}
	return 0;
}

//bus:i2c bus number (0 based)
int battery_init(int bus,int dc_in)
{
	struct ads1000_device_info *di;
	struct ads1000_access_methods *bus_op;
	int ret = 0;

	/* Allocate device info data */
	di = malloc(sizeof(*di));
	bus_op = malloc(sizeof(*bus_op));
	if (!di||!bus_op) {
		ads_dbg("%s out of memory\n",__func__);
		return -1;
	}

	memset(di,0,sizeof(*di));
	memset(bus_op,0,sizeof(*bus_op));
	
	/* Init client data */
	bus_op->read = ads1000_read_i2c;
	bus_op->write = ads1000_write_i2c;
	di->bus_op = bus_op;
	di->bus_num = bus;

	/* Init platform data */
	di->dc_in = dc_in;

	/* AD converter init */
	ret = adc_setup(di);
	if (ret) {
		ads_dbg("failed to setup ADC\n");
		return -2;
	}
	
	/* Init battery ststus */
	ads1000_battery_update_state(di);
	di->dc_in_st = !!gpio_get_value(di->dc_in);

	g_di = di;

	return 0;

}

//return battery voltage mv
int battery_voltage(void)
{
	if(g_di)
	{	
		struct ads1000_device_info *di = g_di;
		di->dc_in_st = !!gpio_get_value(di->dc_in);
		ads1000_battery_update_state(di);

		return di->bat_volt;
	}
	return -1;

}

int battery_capacity(void)
{
	if(g_di)
	{	
		struct ads1000_device_info *di = g_di;
		di->dc_in_st = !!gpio_get_value(di->dc_in);
		ads1000_battery_update_state(di);

		return di->bat_cap;
	}
	return -1;

}
int battery_dc_in(void)
{
	if(g_di)
	{	
		struct ads1000_device_info *di = g_di;
		di->dc_in_st = !!gpio_get_value(di->dc_in);
		return di->dc_in_st;
	}
	return -1;
}
#if 0
#include "battery_bmp.c"

#ifdef CONFIG_GZIP

int  gunzip (void *, int, unsigned char *, unsigned long *);
static bmp_image_t *gunzip_bmp(unsigned long addr, unsigned long *lenp)
{
	void *dst;
	unsigned long len;
	bmp_image_t *bmp;

	/*
	 * Decompress bmp image
	 */
	len = CONFIG_SYS_VIDEO_LOGO_MAX_SIZE;
	dst = CONFIG_GZIP_BMP_ADDR;
	if (dst == NULL) {
		puts("Error: malloc in gunzip failed!\n");
		return NULL;
	}
	if (gunzip(dst, CONFIG_SYS_VIDEO_LOGO_MAX_SIZE, (uchar *)addr, &len) != 0) {
		free(dst);
		return NULL;
	}
	if (len == CONFIG_SYS_VIDEO_LOGO_MAX_SIZE)
		puts("Image could be truncated"
				" (increase CONFIG_SYS_VIDEO_LOGO_MAX_SIZE)!\n");

	bmp = dst;

	/*
	 * Check for bmp mark 'BM'
	 */
	if (!((bmp->header.signature[0] == 'B') &&
		  (bmp->header.signature[1] == 'M'))) {
		return NULL;
	}

	puts("Gzipped BMP image detected!\n");

	return bmp;
}
#endif
#endif
u8* battery_bmp(int lvl)
{
#if 0
	if(0==lvl)
	{	
		#ifdef CONFIG_GZIP
		unsigned long len;
		return gunzip_bmp(battery_bmp_level_0,&len);
		#else
		return battery_bmp_level_0;
		#endif
	}
#endif
	void * dst = CONFIG_GZIP_BMP_ADDR;
	char buffer[16];
	sprintf(buffer,"bmp.bat%d",lvl);
	if(!bmp_manager_readbmp(buffer,dst,CONFIG_SYS_VIDEO_LOGO_MAX_SIZE))
		return dst;

	return NULL;

}

static int do_battery(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	int volt = battery_voltage();
	int cap = battery_capacity();
	printf("battery volt=%d,capacity=%d\n",volt,cap);
	return 0;
}

U_BOOT_CMD(
               battery,    CONFIG_SYS_MAXARGS,      1,      do_battery,
               "battery  - read battery capacity",
               "battery  - read battery capacity"
);


static int do_batterybmp(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
    int lvl=0;
    u8* bmp;
    if(argc>1)
        lvl = simple_strtol(argv[1],0,10);
    bmp = battery_bmp(lvl);
    if(bmp){
    	lcd_clear();
    	lcd_draw_bmp(bmp,0);
	}
	return 0;
}

U_BOOT_CMD(
        batterybmp, CONFIG_SYS_MAXARGS, 0, do_batterybmp, NULL, NULL
);


#endif

