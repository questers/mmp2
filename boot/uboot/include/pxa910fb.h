/* default fb buffer size WVGA-32bits */
#define DEFAULT_FB_SIZE		(1280 * 720 * 4)
/* 32MB RAM address as FB buffer address */
#define DEFAULT_FB_BASE			0x2000000

/* Dumb interface */
#define PIN_MODE_DUMB_24		0
#define PIN_MODE_DUMB_18_SPI		1
#define PIN_MODE_DUMB_18_GPIO		2
#define PIN_MODE_DUMB_16_SPI		3
#define PIN_MODE_DUMB_16_GPIO		4
#define PIN_MODE_DUMB_12_SPI_GPIO	5
#define PIN_MODE_SMART_18_SPI		6
#define PIN_MODE_SMART_16_SPI		7
#define PIN_MODE_SMART_8_SPI_GPIO	8

/* Dumb interface pin allocation */
#define DUMB_MODE_RGB565		0
#define DUMB_MODE_RGB565_UPPER		1
#define DUMB_MODE_RGB666		2
#define DUMB_MODE_RGB666_UPPER		3
#define DUMB_MODE_RGB444		4
#define DUMB_MODE_RGB444_UPPER		5
#define DUMB_MODE_RGB888		6

/*
 * Buffer pixel format
 * bit0 is for rb swap.
 * bit12 is for Y UorV swap
 */
#define PIX_FMT_RGB565		0
#define PIX_FMT_BGR565		1
#define PIX_FMT_RGB1555		2
#define PIX_FMT_BGR1555		3
#define PIX_FMT_RGB888PACK	4
#define PIX_FMT_BGR888PACK	5
#define PIX_FMT_RGB888UNPACK	6
#define PIX_FMT_BGR888UNPACK	7
#define PIX_FMT_RGBA888		8
#define PIX_FMT_BGRA888		9
#define PIX_FMT_YUV422PACK	10
#define PIX_FMT_YVU422PACK	11
#define PIX_FMT_YUV422PLANAR	12
#define PIX_FMT_YVU422PLANAR	13
#define PIX_FMT_YUV420PLANAR	14
#define PIX_FMT_YVU420PLANAR	15
#define PIX_FMT_PSEUDOCOLOR	20
#define PIX_FMT_YUYV422PACK	(0x1000|PIX_FMT_YUV422PACK)
#define PIX_FMT_YUV422PACK_IRE_90_270	(0x1000|PIX_FMT_RGB888UNPACK)


#define FB_SYNC_HOR_HIGH_ACT	1	/* horizontal sync high active  */
#define FB_SYNC_VERT_HIGH_ACT	2	/* vertical sync high active    */
#define FB_SYNC_EXT		4	/* external sync                */
#define FB_SYNC_COMP_HIGH_ACT	8	/* composite sync high active   */
#define FB_SYNC_BROADCAST	16	/* broadcast video timings      */

struct fb_bitfield {
	__u32 offset;		/* beginning of bitfield        */
	__u32 length;		/* length of bitfield           */
	__u32 msb_right;	/* != 0 : Most significant bit is */
	/* right */
};

struct fb_var_screeninfo {
	__u32 xres;		/* visible resolution           */
	__u32 yres;
	__u32 xres_virtual;	/* virtual resolution           */
	__u32 yres_virtual;
	__u32 xoffset;		/* offset from virtual to visible */
	__u32 yoffset;		/* resolution                   */

	__u32 bits_per_pixel;	/* guess what                   */
	__u32 grayscale;	/* != 0 Graylevels instead of colors */

	struct fb_bitfield red;	/* bitfield in fb mem if true color, */
	struct fb_bitfield green;	/* else only length is significant */
	struct fb_bitfield blue;
	struct fb_bitfield transp;	/* transparency                 */

	__u32 nonstd;		/* != 0 Non standard pixel format */

	__u32 activate;		/* see FB_ACTIVATE_*            */

	__u32 height;		/* height of picture in mm    */
	__u32 width;		/* width of picture in mm     */

	__u32 accel_flags;	/* (OBSOLETE) see fb_info.flags */

	/* Timing: All values in pixclocks, except pixclock (of course) */
	__u32 pixclock;		/* pixel clock in ps (pico seconds) */
	__u32 left_margin;	/* time from sync to picture    */
	__u32 right_margin;	/* time from picture to sync    */
	__u32 upper_margin;	/* time from sync to picture    */
	__u32 lower_margin;
	__u32 hsync_len;	/* length of horizontal sync    */
	__u32 vsync_len;	/* length of vertical sync      */
	__u32 sync;		/* see FB_SYNC_*                */
	__u32 vmode;		/* see FB_VMODE_*               */
	__u32 rotate;		/* angle we rotate counter clockwise */
	__u32 reserved[5];	/* Reserved for future compatibility */
};

/*
 * PXA LCD controller private state.
 */
struct pxa910fb_info {
	int id;
	void *reg_base;
	unsigned long new_addr[3];	/* three addr for YUV planar */
	dma_addr_t fb_start_dma;
	void *fb_start;
	int fb_size;

	//struct fb_videomode   dft_vmode;
	//struct fb_videomode     out_vmode;

	int fixed_output;
	unsigned char *hwc_buf;
	unsigned int pseudo_palette[16];

	char *mode_option;
	//struct fb_info          *fb_info;
	int io_pin_allocation;
	int pix_fmt;
	unsigned is_blanked:1;
	unsigned edid:1;
	unsigned cursor_enabled:1;
	unsigned cursor_cfg:1;
	unsigned panel_rbswap:1;
	unsigned debug:1;
	unsigned active:1;
	unsigned enabled:1;
	unsigned edid_en:1;

	/*
	 * 0: DMA mem is from DMA region.
	 * 1: DMA mem is from normal region.
	 */
	unsigned mem_status:1;
	unsigned wait_vsync;

	struct fb_var_screeninfo var;
	struct pxa950fb_mach_info *mi;
};

struct fb_videomode {
	const char *name;	/* optional */
	u32 refresh;		/* optional */
	u32 xres;
	u32 yres;
	u32 pixclock;
	u32 left_margin;
	u32 right_margin;
	u32 upper_margin;
	u32 lower_margin;
	u32 hsync_len;
	u32 vsync_len;
	u32 sync;
	u32 vmode;
	u32 flag;
};

/*
 * PXA fb machine information
 */
struct pxa910fb_mach_info {
	char id[16];
	unsigned int sclk_clock;

	int num_modes;
	struct fb_videomode *modes;
	unsigned int max_fb_size;

	/*
	 * Pix_fmt
	 */
	unsigned pix_fmt;

	/*
	 * I/O pin allocation.
	 */
	unsigned io_pin_allocation_mode:4;

	/*
	 * Dumb panel -- assignment of R/G/B component info to the 24
	 * available external data lanes.
	 */
	unsigned dumb_mode:4;
	unsigned panel_rgb_reverse_lanes:1;

	/*
	 * Dumb panel -- GPIO output data.
	 */
	unsigned gpio_output_mask:8;
	unsigned gpio_output_data:8;

	/*
	 * Dumb panel -- configurable output signal polarity.
	 */
	unsigned invert_composite_blank:1;
	unsigned invert_pix_val_ena:1;
	unsigned invert_pixclock:1;
	unsigned invert_vsync:1;
	unsigned invert_hsync:1;
	unsigned panel_rbswap:1;
	unsigned active:1;
	unsigned enable_lcd:1;
	/*
	 * SPI control
	 */
	unsigned int spi_ctrl;
	unsigned int spi_gpio_cs;
	unsigned int spi_gpio_reset;
	/*
	 * panel interface
	 */
	unsigned int display_interface;

	/*
	 * power on/off function.
	 */
	int (*pxa910fb_lcd_power) (struct pxa910fb_info *, unsigned int,
				   unsigned int, int);
};
