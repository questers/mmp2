/*
 * PXA27x register declarations and HCD data structures
 *
 * Copyright (C) 2007 Rodolfo Giometti <giometti@linux.it>
 * Copyright (C) 2007 Eurotech S.p.A. <info@eurotech.it>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */


#ifndef __PXA270X_UDC_H__
#define __PXA270X_UDC_H__

#include <asm/byteorder.h>

/* Endpoint 0 states */
#define EP0_IDLE		0
#define EP0_IN_DATA		1
#define EP0_OUT_DATA		2
#define EP0_XFER_COMPLETE	3


/* Endpoint parameters */
#define MAX_ENDPOINTS		4
#define EP_MAX_PACKET_SIZE	0x200

#define EP0_MAX_PACKET_SIZE	64
#define UDC_OUT_ENDPOINT        0x02
#define UDC_OUT_PACKET_SIZE	EP_MAX_PACKET_SIZE
#define UDC_IN_ENDPOINT         0x01
#define UDC_IN_PACKET_SIZE      EP_MAX_PACKET_SIZE
#define UDC_INT_ENDPOINT        0x05
#define UDC_INT_PACKET_SIZE     EP_MAX_PACKET_SIZE
#define UDC_BULK_PACKET_SIZE    EP_MAX_PACKET_SIZE

#define UTMI_CTRL               0x4
#define UTMI_PLL                0x8
#define UTMI_TX                 0xc
#define UTMI_RX                 0x10
#define UTMI_IVREF              0x14
#define UTMI_RESERVE            0x30
#define UTMI_OTG_ADDON          0x3c
#define PLL_READY                               0x00800000

#define USB_USBCMD           (CONFIG_USB_REG_BASE + 0x0140)
#define USB_USBSTS           (CONFIG_USB_REG_BASE + 0x0144)
#define USB_DEVICEADDR       (CONFIG_USB_REG_BASE + 0x0154)
#define USB_ENDPOINTLISTADDR (CONFIG_USB_REG_BASE + 0x0158)
#define USB_PORTSC           (CONFIG_USB_REG_BASE + 0x0184)
#define USB_USBMODE          (CONFIG_USB_REG_BASE + 0x01A8)
#define USB_ENDPTSETUPSTAT   (CONFIG_USB_REG_BASE + 0x01AC)
#define USB_ENDPTPRIME       (CONFIG_USB_REG_BASE + 0x01B0)
#define USB_ENDPTFLUSH       (CONFIG_USB_REG_BASE + 0x01B4)
#define USB_ENDPTCOMPLETE    (CONFIG_USB_REG_BASE + 0x01BC)
#define USB_ENDPTCTRL(n)     (CONFIG_USB_REG_BASE + 0x01C0 + (4 * (n)))

/* USB_USBCMD bits */
#define USBCMD_ITC(x)		(((x > 0xff) ? 0xff : x) << 16)
#define USBCMD_FS2		(1 << 15)
#define USBCMD_RST		(1 << 1)
#define USBCMD_RUN		(1)

void udc_irq(void);
/* Flow control */
void udc_set_nak(int epid);
void udc_unset_nak(int epid);

/* Higher level functions for abstracting away from specific device */
int udc_endpoint_write(struct usb_endpoint_instance *endpoint);

int  udc_init(void);

void udc_enable(struct usb_device_instance *device);
void udc_disable(void);

void udc_connect(void);
void udc_disconnect(void);

void udc_startup_events(struct usb_device_instance *device);
void udc_setup_ep(struct usb_device_instance *device,
	 unsigned int ep, struct usb_endpoint_instance *endpoint);

#define        NUM_ENDPOINTS   6
#define		REQ_COUNT	12
struct mmp_udc {
       struct usb_gadget		gadget;
       struct usb_gadget_driver		*driver;
};

struct mmp_ep {
	struct usb_ep ep;
	struct usb_request req;
	struct list_head queue;
	const struct usb_endpoint_descriptor *desc;
};
#endif
