#ifndef _INCLUDE_BOOT_FLASH_H_
#define _INCLUDE_BOOT_FLASH_H_

typedef struct ptentry ptentry;

#define FLASH_PARTITION_MASK 0xf


/* flash partitions are defined in terms of blocks
** (flash erase units)
*/
struct ptentry
{
    char name[32];
    unsigned start;
    unsigned length;
    unsigned flags;
};

/* tools to populate and query the partition table */
void flash_add_ptn(ptentry *ptn);
ptentry *flash_find_ptn(const char *name);
ptentry *flash_get_ptn(unsigned n);
unsigned flash_get_ptn_count(void);
void flash_dump_ptn(void);

int flash_init(void);
int flash_erase(ptentry *ptn);
int flash_write(ptentry *ptn, unsigned long offset, const void *data, unsigned bytes);

int flash_read(ptentry *ptn, unsigned long offset,const void *data, unsigned bytes);


#endif

