#ifndef _FASTBOOT_H_
#define _FASTBOOT_H_

#define FASTBOOT_RECV_BUF_SIZE 512
//4096
//512
//64


typedef void (*fastboot_cmd_handler)(unsigned char* buf,int len);

void fb_init(fastboot_cmd_handler cb);

void fb_tx_status(const char *status);

void fb_run(void);
void fb_halt(void);

int fb_get_rcv_len(void);

void *fb_get_buf(void);
void fb_set_buf (void *buf);



#endif

