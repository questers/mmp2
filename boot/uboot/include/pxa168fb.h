/*
 * linux/arch/arm/mach-mmp/include/mach/pxa168fb.h
 *
 *  Copyright (C) 2009 Marvell International Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef __ASM_MACH_PXA168FB_H
#define __ASM_MACH_PXA168FB_H

//#include <linux/fb.h>
//#include <linux/interrupt.h>
/*
 * panel interface
 */
#define DPI             0
#define DSI2DPI         1
#define DSI             2

#define fb_base         0
#define fb_dual         1
#define FB_MODE_DUP     ((fbi->id == fb_base) && fb_mode && gfx_info.fbi[fb_dual])

/* DSI burst mode */
#define DSI_BURST_MODE_SYNC_PULSE                       0x0
#define DSI_BURST_MODE_SYNC_EVENT                       0x1
#define DSI_BURST_MODE_BURST                            0x2

/* DSI input data RGB mode */
#define DSI_LCD_INPUT_DATA_RGB_MODE_565                 0
#define DSI_LCD_INPUT_DATA_RGB_MODE_666PACKET           1
#define DSI_LCD_INPUT_DATA_RGB_MODE_666UNPACKET         2
#define DSI_LCD_INPUT_DATA_RGB_MODE_888                 3

/*VDMA*/
#define EOF_TIMEOUT 20

/* ------------< LCD register >------------ */
struct lcd_regs {
/* TV patch register for MMP2 */
#define LCD_TVD_START_ADDR_Y0            (0x0000)       /* 32 bit       TV Video Frame0 Y Starting Address */
#define LCD_TVD_START_ADDR_U0            (0x0004)       /* 32 bit       TV Video Frame0 U Starting Address */
#define LCD_TVD_START_ADDR_V0            (0x0008)       /* 32 bit       TV Video Frame0 V Starting Address */
#define LCD_TVD_START_ADDR_C0            (0x000C)       /* 32 bit       TV Video Frame0 Command Starting Address */
#define LCD_TVD_START_ADDR_Y1            (0x0010)       /* 32 bit       TV Video Frame1 Y Starting Address Register*/
#define LCD_TVD_START_ADDR_U1            (0x0014)       /* 32 bit       TV Video Frame1 U Starting Address Register*/
#define LCD_TVD_START_ADDR_V1            (0x0018)       /* 32 bit       TV Video Frame1 V Starting Address Register*/
#define LCD_TVD_START_ADDR_C1            (0x001C)       /* 32 bit       TV Video Frame1 Command Starting Address Register*/
#define LCD_TVD_PITCH_YC                 (0x0020)       /* 32 bit       TV Video Y andC Line Length(Pitch)Register*/
#define LCD_TVD_PITCH_UV                 (0x0024)       /* 32 bit       TV Video U andV Line Length(Pitch)Register*/
#define LCD_TVD_OVSA_HPXL_VLN            (0x0028)       /* 32 bit     TV Video Starting Point on Screen Register*/
#define LCD_TVD_HPXL_VLN                 (0x002C)       /* 32 bit       TV Video Source Size Register*/
#define LCD_TVDZM_HPXL_VLN               (0x0030)/* 32 bit      TV Video Destination Size (After Zooming)Register*/
	u32 v_y0;			/* Video Frame 0/1 Y/U/V/Command Starting Addr */
	u32 v_u0;
	u32 v_v0;
	u32 v_c0;
	u32 v_y1;
	u32 v_u1;
	u32 v_v1;
	u32 v_c1;
	u32 v_pitch_yc;			/* Video Y and C Line Length (Pitch) */
	u32 v_pitch_uv;			/* Video U and V Line Length (Pitch) */
	u32 v_start;			/* Video Starting Point on Screen */
	u32 v_size;			/* Video Source Size */
	u32 v_size_z;			/* Video Destination Size (After Zooming) */

#define LCD_TVG_START_ADDR0              (0x0034)/* 32 bit      TV Graphic Frame 0 Starting Address Register*/
#define LCD_TVG_START_ADDR1              (0x0038)/* 32 bit      TV Graphic Frame 1 Starting Address Register*/
#define LCD_TVG_PITCH                    (0x003C)       /* 32 bit       TV Graphic Line Length(Pitch)Register*/
#define LCD_TVG_OVSA_HPXL_VLN            (0x0040)       /* 32 bit       TV Graphic Starting Point on Screen Register*/
#define LCD_TVG_HPXL_VLN                 (0x0044)       /* 32 bit       TV Graphic Source Size Register*/
#define LCD_TVGZM_HPXL_VLN               (0x0048)       /* 32 bit       TV Graphic Destination size (after Zooming)Register*/
	u32 g_0;			/* Graphic Frame 0/1 Starting Address */
	u32 g_1;
	u32 g_pitch;			/* Graphic Line Length (Pitch) */
	u32 g_start;			/* Graphic Starting Point on Screen */
	u32 g_size;			/* Graphic Source Size */
	u32 g_size_z;			/* Graphic Destination Size (After Zooming) */

#define LCD_TVC_OVSA_HPXL_VLN            (0x004C)/* 32 bit      TV Hardware Cursor Starting Point on screen Register*/
#define LCD_TVC_HPXL_VLN                 (0x0050)       /* 32 bit       TV Hardware Cursor Size Register */
	u32 hc_start;			/* Hardware Cursor */
	u32 hc_size;			/* Hardware Cursor */

#define LCD_TV_V_H_TOTAL                 (0x0054)       /* 32 bit       TV Total Screen Size Register*/
#define LCD_TV_V_H_ACTIVE                (0x0058)       /* 32 bit       TV Screen Active Size Register*/
#define LCD_TV_H_PORCH                   (0x005C)       /* 32 bit       TV Screen Horizontal Porch Register*/
#define LCD_TV_V_PORCH                   (0x0060)       /* 32 bit       TV Screen Vertical Porch Register*/
	u32 screen_size;		/* Screen Total Size */
	u32 screen_active;		/* Screen Active Size */
	u32 screen_h_porch;		/* Screen Horizontal Porch */
	u32 screen_v_porch;		/* Screen Vertical Porch */

#define LCD_TV_BLANKCOLOR                (0x0064)       /* 32 bit       TV Screen Blank Color Register*/
#define LCD_TV_ALPHA_COLOR1              (0x0068)       /* 32 bit       TV Hardware Cursor Color1 Register*/
#define LCD_TV_ALPHA_COLOR2              (0x006C)       /* 32 bit       TV Hardware Cursor Color2 Register*/
	u32 blank_color;		/* Screen Blank Color */
	u32 hc_Alpha_color1;	/* Hardware Cursor Color1 */
	u32 hc_Alpha_color2;	/* Hardware Cursor Color2 */

#define LCD_TV_COLORKEY_Y                (0x0070)       /* 32 bit       TV Video Y Color Key Control*/
#define LCD_TV_COLORKEY_U                (0x0074)       /* 32 bit       TV Video U Color Key Control*/
#define LCD_TV_COLORKEY_V                (0x0078)       /* 32 bit       TV Video V Color Key Control*/
	u32 v_colorkey_y;		/* Video Y Color Key Control */
	u32 v_colorkey_u;		/* Video U Color Key Control */
	u32 v_colorkey_v;		/* Video V Color Key Control */

#define LCD_TV_SEPXLCNT                  (0x007C)       /* 32 bit       TV VSYNC PulsePixel Edge Control Register*/
	u32 vsync_ctrl;			/* VSYNC PulsePixel Edge Control */
};

#define intf_ctrl(id)		((id) ? (((id) & 1) ? LCD_TVIF_CTRL : LCD_DUMB2_CTRL) : LCD_SPU_DUMB_CTRL)
#define dma_ctrl0(id)           ((id) ? (((id) & 1) ? LCD_TV_CTRL0 : LCD_PN2_CTRL0) : LCD_SPU_DMA_CTRL0)
#define dma_ctrl1(id)           ((id) ? (((id) & 1) ? LCD_TV_CTRL1 : LCD_PN2_CTRL1) : LCD_SPU_DMA_CTRL1)
#define dma_ctrl(ctrl1, id)     (ctrl1 ? dma_ctrl1(id) : dma_ctrl0(id))

#define LCD_TV_CTRL0                     (0x0080)       /* 32 bit       TV Path DMA Control 0*/
#define LCD_TV_CTRL1                     (0x0084)       /* 32 bit       TV Path DMA Control 1*/
#define LCD_TV_CONTRAST                  (0x0088)       /* 32 bit       TV Path Video Contrast*/
#define LCD_TV_SATURATION                (0x008C)       /* 32 bit       TV Path Video Saturation*/
#define LCD_TV_CBSH_HUE                  (0x0090)       /* 32 bit       TV Path Video Hue Adjust*/
#define LCD_TVIF_CTRL                    (0x0094)       /* 32 bit TV Path TVIF Control  Register */

#define LCD_TVIOPAD_CTRL                 (0x0098)       /* 32 bit TV Path I/O Pad Control*/

/* Video Frame 0&1 start address registers */
#define	LCD_SPU_DMA_START_ADDR_Y0	0x00C0
#define	LCD_SPU_DMA_START_ADDR_U0	0x00C4
#define	LCD_SPU_DMA_START_ADDR_V0	0x00C8
#define LCD_CFG_DMA_START_ADDR_0	0x00CC /* Cmd address */
#define	LCD_SPU_DMA_START_ADDR_Y1	0x00D0
#define	LCD_SPU_DMA_START_ADDR_U1	0x00D4
#define	LCD_SPU_DMA_START_ADDR_V1	0x00D8
#define LCD_CFG_DMA_START_ADDR_1	0x00DC /* Cmd address */

/* YC & UV Pitch */
#define LCD_SPU_DMA_PITCH_YC		0x00E0
#define     SPU_DMA_PITCH_C(c)		((c)<<16)
#define     SPU_DMA_PITCH_Y(y)		(y)
#define LCD_SPU_DMA_PITCH_UV		0x00E4
#define     SPU_DMA_PITCH_V(v)		((v)<<16)
#define     SPU_DMA_PITCH_U(u)		(u)

/* Video Starting Point on Screen Register */
#define LCD_SPUT_DMA_OVSA_HPXL_VLN		0x00E8
#define     CFG_DMA_OVSA_VLN(y)			((y)<<16) /* 0~0xfff */
#define     CFG_DMA_OVSA_HPXL(x)		(x)     /* 0~0xfff */

/* Video Size Register */
#define LCD_SPU_DMA_HPXL_VLN			0x00EC
#define     CFG_DMA_VLN(y)			((y)<<16)
#define     CFG_DMA_HPXL(x)			(x)

/* Video Size After zooming Register */
#define LCD_SPU_DZM_HPXL_VLN			0x00F0
#define     CFG_DZM_VLN(y)			((y)<<16)
#define     CFG_DZM_HPXL(x)			(x)

/* Graphic Frame 0&1 Starting Address Register */
#define LCD_CFG_GRA_START_ADDR0			0x00F4
#define LCD_CFG_GRA_START_ADDR1			0x00F8

/* Graphic Frame Pitch */
#define LCD_CFG_GRA_PITCH			0x00FC

/* Graphic Starting Point on Screen Register */
#define LCD_SPU_GRA_OVSA_HPXL_VLN		0x0100
#define     CFG_GRA_OVSA_VLN(y)			((y)<<16)
#define     CFG_GRA_OVSA_HPXL(x)		(x)

/* Graphic Size Register */
#define LCD_SPU_GRA_HPXL_VLN			0x0104
#define     CFG_GRA_VLN(y)			((y)<<16)
#define     CFG_GRA_HPXL(x)			(x)

/* Graphic Size after Zooming Register */
#define LCD_SPU_GZM_HPXL_VLN			0x0108
#define     CFG_GZM_VLN(y)			((y)<<16)
#define     CFG_GZM_HPXL(x)			(x)

/* HW Cursor Starting Point on Screen Register */
#define LCD_SPU_HWC_OVSA_HPXL_VLN		0x010C
#define     CFG_HWC_OVSA_VLN(y)			((y)<<16)
#define     CFG_HWC_OVSA_HPXL(x)		(x)

/* HW Cursor Size */
#define LCD_SPU_HWC_HPXL_VLN			0x0110
#define     CFG_HWC_VLN(y)			((y)<<16)
#define     CFG_HWC_HPXL(x)			(x)

/* Total Screen Size Register */
#define LCD_SPUT_V_H_TOTAL			0x0114
#define     CFG_V_TOTAL(y)			((y)<<16)
#define     CFG_H_TOTAL(x)			(x)

/* Total Screen Active Size Register */
#define LCD_SPU_V_H_ACTIVE			0x0118
#define     CFG_V_ACTIVE(y)			((y)<<16)
#define     CFG_H_ACTIVE(x)			(x)

/* Screen H&V Porch Register */
#define LCD_SPU_H_PORCH				0x011C
#define     CFG_H_BACK_PORCH(b)			((b)<<16)
#define     CFG_H_FRONT_PORCH(f)		(f)
#define LCD_SPU_V_PORCH				0x0120
#define     CFG_V_BACK_PORCH(b)			((b)<<16)
#define     CFG_V_FRONT_PORCH(f)		(f)

/* Screen Blank Color Register */
#define LCD_SPU_BLANKCOLOR			0x0124
#define     CFG_BLANKCOLOR_MASK			0x00FFFFFF
#define     CFG_BLANKCOLOR_R_MASK		0x000000FF
#define     CFG_BLANKCOLOR_G_MASK		0x0000FF00
#define     CFG_BLANKCOLOR_B_MASK		0x00FF0000

/* HW Cursor Color 1&2 Register */
#define LCD_SPU_ALPHA_COLOR1			0x0128
#define     CFG_HWC_COLOR1			0x00FFFFFF
#define     CFG_HWC_COLOR1_R(red)		((red)<<16)
#define     CFG_HWC_COLOR1_G(green)		((green)<<8)
#define     CFG_HWC_COLOR1_B(blue)		(blue)
#define     CFG_HWC_COLOR1_R_MASK		0x000000FF
#define     CFG_HWC_COLOR1_G_MASK		0x0000FF00
#define     CFG_HWC_COLOR1_B_MASK		0x00FF0000
#define LCD_SPU_ALPHA_COLOR2			0x012C
#define     CFG_HWC_COLOR2			0x00FFFFFF
#define     CFG_HWC_COLOR2_R_MASK		0x000000FF
#define     CFG_HWC_COLOR2_G_MASK		0x0000FF00
#define     CFG_HWC_COLOR2_B_MASK		0x00FF0000

/* Video YUV Color Key Control */
#define LCD_SPU_COLORKEY_Y			0x0130
#define     CFG_CKEY_Y2(y2)			((y2)<<24)
#define     CFG_CKEY_Y2_MASK			0xFF000000
#define     CFG_CKEY_Y1(y1)			((y1)<<16)
#define     CFG_CKEY_Y1_MASK			0x00FF0000
#define     CFG_CKEY_Y(y)			((y)<<8)
#define     CFG_CKEY_Y_MASK			0x0000FF00
#define     CFG_ALPHA_Y(y)			(y)
#define     CFG_ALPHA_Y_MASK			0x000000FF
#define LCD_SPU_COLORKEY_U			0x0134
#define     CFG_CKEY_U2(u2)			((u2)<<24)
#define     CFG_CKEY_U2_MASK			0xFF000000
#define     CFG_CKEY_U1(u1)			((u1)<<16)
#define     CFG_CKEY_U1_MASK			0x00FF0000
#define     CFG_CKEY_U(u)			((u)<<8)
#define     CFG_CKEY_U_MASK			0x0000FF00
#define     CFG_ALPHA_U(u)			(u)
#define     CFG_ALPHA_U_MASK			0x000000FF
#define LCD_SPU_COLORKEY_V			0x0138
#define     CFG_CKEY_V2(v2)			((v2)<<24)
#define     CFG_CKEY_V2_MASK			0xFF000000
#define     CFG_CKEY_V1(v1)			((v1)<<16)
#define     CFG_CKEY_V1_MASK			0x00FF0000
#define     CFG_CKEY_V(v)			((v)<<8)
#define     CFG_CKEY_V_MASK			0x0000FF00
#define     CFG_ALPHA_V(v)			(v)
#define     CFG_ALPHA_V_MASK			0x000000FF

/* Graphics/Video DMA color key enable bits in LCD_TV_CTRL1 */
#define     CFG_CKEY_GRA			0x2
#define     CFG_CKEY_DMA			0x1

#define LCD_PN_SEPXLCNT				0x013c	//MMP2

/* SPI Read Data Register */
#define LCD_SPU_SPI_RXDATA			0x0140

/* Smart Panel Read Data Register */
#define LCD_SPU_ISA_RSDATA			0x0144
#define     ISA_RXDATA_16BIT_1_DATA_MASK	0x000000FF
#define     ISA_RXDATA_16BIT_2_DATA_MASK	0x0000FF00
#define     ISA_RXDATA_16BIT_3_DATA_MASK	0x00FF0000
#define     ISA_RXDATA_16BIT_4_DATA_MASK	0xFF000000
#define     ISA_RXDATA_32BIT_1_DATA_MASK	0x00FFFFFF

#define LCD_SPU_DBG_ISA                  (0x0148)       //TTC
#define LCD_SPU_DMAVLD_YC                (0x014C)
#define LCD_SPU_DMAVLD_UV                (0x0150)
#define LCD_SPU_DMAVLD_UVSPU_GRAVLD      (0x0154)

#define LCD_READ_IOPAD                   (0x0148)       //MMP2
#define LCD_DMAVLD_YC                    (0x014C)
#define LCD_DMAVLD_UV                    (0x0150)
#define LCD_TVGGRAVLD_HLEN               (0x0154)

/* HWC SRAM Read Data Register */
#define LCD_SPU_HWC_RDDAT			0x0158

/* Gamma Table SRAM Read Data Register */
#define LCD_SPU_GAMMA_RDDAT			0x015c
#define     CFG_GAMMA_RDDAT_MASK		0x000000FF

/* Palette Table SRAM Read Data Register */
#define LCD_SPU_PALETTE_RDDAT			0x0160
#define     CFG_PALETTE_RDDAT_MASK		0x00FFFFFF

#define LCD_SPU_DBG_DMATOP               (0x0164)       //TTC
#define LCD_SPU_DBG_GRATOP               (0x0168)
#define LCD_SPU_DBG_TXCTRL               (0x016C)
#define LCD_SPU_DBG_SLVTOP               (0x0170)
#define LCD_SPU_DBG_MUXTOP               (0x0174)

#define LCD_SLV_DBG                      (0x0164)       //MMP2
#define LCD_TVDVLD_YC                    (0x0168)
#define LCD_TVDVLD_UV                    (0x016C)
#define LCD_TVC_RDDAT                    (0x0170)
#define LCD_TV_GAMMA_RDDAT               (0x0174)


/* Dumb interface */
#define PIN_MODE_DUMB_24		0
#define PIN_MODE_DUMB_18_SPI		1
#define PIN_MODE_DUMB_18_GPIO		2
#define PIN_MODE_DUMB_16_SPI		3
#define PIN_MODE_DUMB_16_GPIO		4
#define PIN_MODE_DUMB_12_SPI_GPIO	5
#define PIN_MODE_SMART_18_SPI		6
#define PIN_MODE_SMART_16_SPI		7
#define PIN_MODE_SMART_8_SPI_GPIO	8

/* Dumb interface pin allocation */
#define DUMB_MODE_RGB565		0
#define DUMB_MODE_RGB565_UPPER		1
#define DUMB_MODE_RGB666		2
#define DUMB_MODE_RGB666_UPPER		3
#define DUMB_MODE_RGB444		4
#define DUMB_MODE_RGB444_UPPER		5
#define DUMB_MODE_RGB888		6

/*
 * Buffer pixel format
 * bit0 is for rb swap.
 * bit12 is for Y UorV swap
 */
#define PIX_FMT_RGB565		0
#define PIX_FMT_BGR565		1
#define PIX_FMT_RGB1555		2
#define PIX_FMT_BGR1555		3
#define PIX_FMT_RGB888PACK	4
#define PIX_FMT_BGR888PACK	5
#define PIX_FMT_RGB888UNPACK	6
#define PIX_FMT_BGR888UNPACK	7
#define PIX_FMT_RGBA888		8
#define PIX_FMT_BGRA888		9
#define PIX_FMT_YUV422PACK	10
#define PIX_FMT_YVU422PACK	11
#define PIX_FMT_YUV422PLANAR	12
#define PIX_FMT_YVU422PLANAR	13
#define PIX_FMT_YUV420PLANAR	14
#define PIX_FMT_YVU420PLANAR	15
#define PIX_FMT_PSEUDOCOLOR	20
#define PIX_FMT_UYVY422PACK	(0x1000|PIX_FMT_YUV422PACK)


//typedef unsigned int __u32;
//typedef unsigned int u32;
//typedef unsigned long long u64;


struct fb_fix_screeninfo {
	char id[16];			/* identification string eg "TT Builtin" */
	unsigned long smem_start;	/* Start of frame buffer mem */
					/* (physical address) */
	__u32 smem_len;			/* Length of frame buffer mem */
	__u32 type;			/* see FB_TYPE_*                */
	__u32 type_aux;			/* Interleave for interleaved Planes */
	__u32 visual;			/* see FB_VISUAL_*              */
	__u16 xpanstep;			/* zero if no hardware panning  */
	__u16 ypanstep;			/* zero if no hardware panning  */
	__u16 ywrapstep;		/* zero if no hardware ywrap    */
	__u32 line_length;		/* length of a line in bytes    */
	unsigned long mmio_start;	/* Start of Memory Mapped I/O   */
					/* (physical address) */
	__u32 mmio_len;			/* Length of Memory Mapped I/O  */
	__u32 accel;			/* Indicate to driver which     */
					/*  specific chip/card we have  */
	__u16 reserved[3];		/* Reserved for future compatibility */
};

struct fb_bitfield {
	__u32 offset;                   /* beginning of bitfield        */
	__u32 length;                   /* length of bitfield           */
	__u32 msb_right;                /* != 0 : Most significant bit is */
					/* right */
};


struct fb_var_screeninfo {
	__u32 xres;                     /* visible resolution           */
	__u32 yres;
	__u32 xres_virtual;             /* virtual resolution           */
	__u32 yres_virtual;
	__u32 xoffset;                  /* offset from virtual to visible */
	__u32 yoffset;                  /* resolution                   */

	__u32 bits_per_pixel;           /* guess what                   */
	__u32 grayscale;                /* != 0 Graylevels instead of colors */

	struct fb_bitfield red;         /* bitfield in fb mem if true color, */
	struct fb_bitfield green;       /* else only length is significant */
	struct fb_bitfield blue;
	struct fb_bitfield transp;      /* transparency                 */

	__u32 nonstd;                   /* != 0 Non standard pixel format */

	__u32 activate;                 /* see FB_ACTIVATE_*            */

	__u32 height;                   /* height of picture in mm    */
	__u32 width;                    /* width of picture in mm     */

	__u32 accel_flags;              /* (OBSOLETE) see fb_info.flags */

	/* Timing: All values in pixclocks, except pixclock (of course) */
	__u32 pixclock;                 /* pixel clock in ps (pico seconds) */
	__u32 left_margin;              /* time from sync to picture    */
	__u32 right_margin;             /* time from picture to sync    */
	__u32 upper_margin;             /* time from sync to picture    */
	__u32 lower_margin;
	__u32 hsync_len;                /* length of horizontal sync    */
	__u32 vsync_len;                /* length of vertical sync      */
	__u32 sync;                     /* see FB_SYNC_*                */
	__u32 vmode;                    /* see FB_VMODE_*               */
	__u32 rotate;                   /* angle we rotate counter clockwise */
	__u32 reserved[5];              /* Reserved for future compatibility */
};

struct fb_videomode {
	const char *name;       /* optional */
	u32 refresh;            /* optional */
	u32 xres;
	u32 yres;
	u32 pixclock;
	u32 left_margin;
	u32 right_margin;
	u32 upper_margin;
	u32 lower_margin;
	u32 hsync_len;
	u32 vsync_len;
	u32 sync;
	u32 vmode;
	u32 flag;
};

struct dsi_phy {
	unsigned int hs_prep_constant;    /* Unit: ns. */
	unsigned int hs_prep_ui;
	unsigned int hs_zero_constant;
	unsigned int hs_zero_ui;
	unsigned int hs_trail_constant;
	unsigned int hs_trail_ui;
	unsigned int hs_exit_constant;
	unsigned int hs_exit_ui;
	unsigned int ck_zero_constant;
	unsigned int ck_zero_ui;
	unsigned int ck_trail_constant;
	unsigned int ck_trail_ui;
	unsigned int req_ready;
};

struct dsi_info {
	unsigned        id;
	unsigned        regs;
	unsigned        lanes;
	unsigned        bpp;
	unsigned        rgb_mode;
	unsigned        burst_mode;
	unsigned        lpm_line_en;
	unsigned        lpm_frame_en;
	unsigned        last_line_turn;
	unsigned        hex_slot_en;
	unsigned        all_slot_en;
	unsigned        hbp_en;
	unsigned        hact_en;
	unsigned        hfp_en;
	unsigned        hex_en;
	unsigned        hlp_en;
	unsigned        hsa_en;
	unsigned        hse_en;
	unsigned        eotp_en;
	struct dsi_phy  *phy;
};

struct cmu_calibration {
	int left;
	int right;
	int top;
	int bottom;
};


struct pxa168fb_info;

struct pxa168fb_mach_info {
	char    id[16];
	unsigned int    sclk_src;
	unsigned int    sclk_div;

	int             num_modes;
	struct fb_videomode *modes;
	void *		 default_fbbase;
	unsigned int default_fbsize;
	unsigned int 	max_fb_size;

	/*
	 * Pix_fmt
	 */
	unsigned        pix_fmt;

	/*
	 * Burst length
	 */
	unsigned        burst_len;

	/*
	 * I/O pin allocation.
	 */
	unsigned int    io_pin_allocation_mode;

	/*
	 * Dumb panel -- assignment of R/G/B component info to the 24
	 * available external data lanes.
	 */
	unsigned        dumb_mode:4;
	unsigned        panel_rgb_reverse_lanes:1;

	/*
	 * Dumb panel -- GPIO output data.
	 */
	unsigned        gpio_output_mask:8;
	unsigned        gpio_output_data:8;

	/*
	 * Dumb panel -- configurable output signal polarity.
	 */
	unsigned        invert_composite_blank:1;
	unsigned        invert_pix_val_ena:1;
	unsigned        invert_pixclock:1;
	unsigned        invert_vsync:1;
	unsigned        invert_hsync:1;
	unsigned        panel_rbswap:1;
	unsigned        active:1;
	unsigned        enable_lcd:1;
	/*
	 * SPI control
	 */
	unsigned int    spi_ctrl;
	unsigned int    spi_gpio_cs;
	unsigned int    spi_gpio_reset;

	/*
	 * panel interface
	 */
	unsigned int    phy_type;
	int             (*phy_init)(struct pxa168fb_info *);

	/*
	 * vdma option
	 */
	unsigned int vdma_enable;

	/*
	 * power on/off function.
	 */
	int (*pxa168fb_lcd_power)(struct pxa168fb_info *, unsigned int, unsigned int, int);

	/*
	 * dsi to dpi setting function
	 */
	int (*dsi2dpi_set)(struct pxa168fb_info *);
	int (*xcvr_reset)(struct pxa168fb_info *);
	int (*xcvr_init)(void);
	/*
	 * dsi setting function
	 */
	void (*dsi_set)(struct pxa168fb_info *);
	struct dsi_info *dsi;
	/*
	 * special ioctls
	 */
	int (*ioctl)(struct fb_info *info, unsigned int cmd, unsigned long arg);
	/*CMU platform calibration*/
	struct cmu_calibration cmu_cal[3];
	struct cmu_calibration cmu_cal_letter_box[3];
};

#define MAX_QUEUE_NUM 30

typedef struct {
	int counter;
} atomic_t;

//move to config file
/*to support 1024x600 resolution,increase reserved memory for primary framebuffer
  ,Now reserve 6M bytes
*/
#ifndef DEFAULT_FB_BASE
#define DEFAULT_FB_BASE		0x3FA00000
#endif
//0x3fc00000 // 0x18800000
#define BMP_DOWNLOAD_BASE	0x15000000
#ifndef DEFAULT_FB_SIZE
#define DEFAULT_FB_SIZE		1280*720*4
#endif
struct pxa168fb_info {
	struct device           *dev;
	struct clk              *clk;
	int                     id;
	void                    *reg_base;
	void                    *dsi1_reg_base;
	void                    *dsi2_reg_base;
	unsigned long           new_addr[3];    /* three addr for YUV planar */
	unsigned char           *filterBufList[MAX_QUEUE_NUM][3];
	unsigned char           *buf_freelist[MAX_QUEUE_NUM];
	unsigned char           *buf_waitlist[MAX_QUEUE_NUM];
	unsigned char           *buf_current;
	dma_addr_t              fb_start_dma;
	void                    *fb_start;
	int                     fb_size;
	dma_addr_t              fb_start_dma_bak;
	void                    *fb_start_bak;
	int                     fb_size_bak;
	atomic_t                w_intr;
	int                     dma_ctrl0;
	int                     fixed_output;
	unsigned char           *hwc_buf;
	unsigned int            pseudo_palette[16];
	char                    *mode_option;
	struct fb_info          *fb_info;
	int                     io_pin_allocation;
	int                     pix_fmt;
	unsigned                is_blanked:1;
	unsigned                edid:1;
	unsigned                cursor_enabled:1;
	unsigned                cursor_cfg:1;
	unsigned                panel_rbswap:1;
	unsigned                debug:1;
	unsigned                active:1;
	unsigned                enabled:1;
	unsigned                edid_en:1;

	/*
	 * 0: DMA mem is from DMA region.
	 * 1: DMA mem is from normal region.
	 */
	unsigned                mem_status:1;
	unsigned                wait_vsync;
	
	struct pxa168fb_mach_info	*mi;
	struct fb_var_screeninfo	*var;
	struct fb_fix_screeninfo	*fix;
};

#define FB_AUX_VGA_PLANES_VGA4          0       /* 16 color planes (EGA/VGA) */
#define FB_AUX_VGA_PLANES_CFB4          1       /* CFB4 in planes (VGA) */
#define FB_AUX_VGA_PLANES_CFB8          2       /* CFB8 in planes (VGA) */

#define FB_VISUAL_MONO01                0       /* Monochr. 1=Black 0=White */
#define FB_VISUAL_MONO10                1       /* Monochr. 1=White 0=Black */
#define FB_VISUAL_TRUECOLOR             2       /* True color   */
#define FB_VISUAL_PSEUDOCOLOR           3       /* Pseudo color (like atari) */
#define FB_VISUAL_DIRECTCOLOR           4       /* Direct color */
#define FB_VISUAL_STATIC_PSEUDOCOLOR    5       /* Pseudo color readonly */

#define FB_ACCELF_TEXT          1       /* (OBSOLETE) see fb_info.flags and vc_mode */

#define FB_SYNC_HOR_HIGH_ACT    1       /* horizontal sync high active  */
#define FB_SYNC_VERT_HIGH_ACT   2       /* vertical sync high active    */
#define FB_SYNC_EXT             4       /* external sync                */
#define FB_SYNC_COMP_HIGH_ACT   8       /* composite sync high active   */
#define FB_SYNC_BROADCAST       16      /* broadcast video timings      */
                                        /* vtotal = 144d/288n/576i => PAL  */
                                        /* vtotal = 121d/242n/484i => NTSC */
#define FB_SYNC_ON_GREEN        32      /* sync on green */

#define FB_VMODE_NONINTERLACED  0       /* non interlaced */
#define FB_VMODE_INTERLACED     1       /* interlaced   */
#define FB_VMODE_DOUBLE         2       /* double scan */
#define FB_VMODE_ODD_FLD_FIRST  4       /* interlaced: top line first */

/*       DSI Controller Registers       */
struct dsi_lcd_regs {
#define DSI_LCD1_CTRL_0  0x100   /* DSI Active Panel 1 Control register 0 */
#define DSI_LCD1_CTRL_1  0x104   /* DSI Active Panel 1 Control register 1 */
	u32 ctrl0;
	u32 ctrl1;
	u32 reserved1[2];

#define DSI_LCD1_TIMING_0        0x110   /* Timing register 0 */
#define DSI_LCD1_TIMING_1        0x114   /* Timing register 1 */
#define DSI_LCD1_TIMING_2        0x118   /* Timing register 2 */
#define DSI_LCD1_TIMING_3        0x11C   /* Timing register 3 */
#define DSI_LCD1_WC_0            0x120   /* Word Count register 0 */
#define DSI_LCD1_WC_1            0x124   /* Word Count register 1 */
#define DSI_LCD1_WC_2		 0x128	 /* Word Count register 2 */
	u32 timing0;
	u32 timing1;
	u32 timing2;
	u32 timing3;
	u32 wc0;
	u32 wc1;
	u32 wc2;
	u32 reserved[1];
	u32 slot_cnt0;
	u32 slot_cnt1;
};



struct dsi_regs {
#define DSI_CTRL_0      0x000   /* DSI control register 0 */
#define DSI_CTRL_1      0x004   /* DSI control register 1 */
	u32 ctrl0;
	u32 ctrl1;
	u32 reserved1[2];
	u32 irq_status;
	u32 irq_mask;
	u32 reserved2[2];

#define DSI_CPU_CMD_0   0x020   /* DSI CPU packet command register 0 */
#define DSI_CPU_CMD_1   0x024   /* DSU CPU Packet Command Register 1 */
#define DSI_CPU_CMD_3    0x02C   /* DSU CPU Packet Command Register 3 */
#define DSI_CPU_WDAT_0   0x030   /* DSI CUP */
	u32 cmd0;
	u32 cmd1;
	u32 cmd2;
	u32 cmd3;
	u32 dat0;
	u32 reserved3[7];

	u32 smt_cmd;
	u32 smt_ctrl0;
	u32 smt_ctrl1;
	u32 reserved4[1];

	u32 rx0_status;
#define DSI_RX_PKT_HDR_0 0x064   /* Rx Packet Header - data from slave device */
	u32 rx0_header;
	u32 rx1_status;
	u32 rx1_header;
	u32 rx_ctrl;
	u32 rx_ctrl1;
	u32 rx2_status;
	u32 rx2_header;
	u32 reserved5[1];

	u32 phy_ctrl1;
#define DSI_PHY_CTRL_2   0x088   /* DSI DPHI Control Register 2 */
#define DSI_PHY_CTRL_3   0x08C   /* DPHY Control Register 3 */
	u32 phy_ctrl2;
	u32 phy_ctrl3;
	u32 phy_status0;
	u32 reserved6[7];

#define DSI_PHY_RCOMP_0         0x0B0   /* DPHY Rcomp Control Register */
	u32 phy_rcomp0;
	u32 reserved7[3];
#define DSI_PHY_TIME_0   0x0C0   /* DPHY Timing Control Register 0 */
#define DSI_PHY_TIME_1   0x0C4   /* DPHY Timing Control Register 1 */
#define DSI_PHY_TIME_2   0x0C8   /* DPHY Timing Control Register 2 */
#define DSI_PHY_TIME_3   0x0CC   /* DPHY Timing Control Register 3 */
#define DSI_PHY_TIME_4   0x0D0   /* DPHY Timing Control Register 4 */
#define DSI_PHY_TIME_5   0x0D4   /* DPHY Timing Control Register 5 */
	u32 phy_timing0;
	u32 phy_timing1;
	u32 phy_timing2;
	u32 phy_timing3;
	u32 phy_timing4;
	u32 phy_timing5;
	u32 reserved8[2];
	u32 mem_ctrl;
	u32 tx_timer;
	u32 rx_timer;
	u32 turn_timer;
	u32 reserved9[4];

#define DSI_LCD1_CTRL_0  0x100   /* DSI Active Panel 1 Control register 0 */
#define DSI_LCD1_CTRL_1  0x104   /* DSI Active Panel 1 Control register 1 */
#define DSI_LCD1_TIMING_0        0x110   /* Timing register 0 */
#define DSI_LCD1_TIMING_1        0x114   /* Timing register 1 */
#define DSI_LCD1_TIMING_2        0x118   /* Timing register 2 */
#define DSI_LCD1_TIMING_3        0x11C   /* Timing register 3 */
#define DSI_LCD1_WC_0            0x120   /* Word Count register 0 */
#define DSI_LCD1_WC_1            0x124   /* Word Count register 1 */
#define DSI_LCD1_WC_2            0x128   /* Word Count register 2 */
	struct dsi_lcd_regs lcd1;
	u32 reserved10[18];
	struct dsi_lcd_regs lcd2;
};

#define DSI_LCD2_CTRL_0  0x180   /* DSI Active Panel 2 Control register 0 */
#define DSI_LCD2_CTRL_1  0x184   /* DSI Active Panel 2 Control register 1 */
#define DSI_LCD2_TIMING_0        0x190   /* Timing register 0 */
#define DSI_LCD2_TIMING_1        0x194   /* Timing register 1 */
#define DSI_LCD2_TIMING_2        0x198   /* Timing register 2 */
#define DSI_LCD2_TIMING_3        0x19C   /* Timing register 3 */
#define DSI_LCD2_WC_0            0x1A0   /* Word Count register 0 */
#define DSI_LCD2_WC_1            0x1A4   /* Word Count register 1 */
#define DSI_LCD2_WC_2		 0x1A8	 /* Word Count register 2 */

/*	DSI_CTRL_0		0x0000	DSI Control Register 0 */
#define DSI_CTRL_0_CFG_SOFT_RST				(1<<31)
#define DSI_CTRL_0_CFG_SOFT_RST_REG			(1<<30)
#define DSI_CTRL_0_CFG_LCD1_TX_EN			(1<<8)
#define DSI_CTRL_0_CFG_LCD1_SLV				(1<<4)
#define DSI_CTRL_0_CFG_LCD1_EN				(1<<0)

/*	DSI_CTRL_1		0x0004	DSI Control Register 1 */
#define DSI_CTRL_1_CFG_EOTP				(1<<8)
#define DSI_CTRL_1_CFG_RSVD				(2<<4)
#define DSI_CTRL_1_CFG_LCD2_VCH_NO_MASK			(3<<2)
#define DSI_CTRL_1_CFG_LCD2_VCH_NO_SHIFT		2
#define DSI_CTRL_1_CFG_LCD1_VCH_NO_MASK			(3<<0)
#define DSI_CTRL_1_CFG_LCD1_VCH_NO_SHIFT		0

/*	DSI_LCD1_CTRL_1		0x0104	DSI Active Panel 1 Control Register 1 */
/* LCD 1 Vsync Reset Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_VSYNC_RST_EN		(1<<31)
/* LCD 1 2K Pixel Buffer Mode Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_M2K_EN			(1<<30)
/*		Bit(s) DSI_LCD1_CTRL_1_RSRV_29_23 reserved */
/* Long Blanking Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HLP_PKT_EN		(1<<22)
/* Extra Long Blanking Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HEX_PKT_EN		(1<<21)
/* Front Porch Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HFP_PKT_EN		(1<<20)
/* hact Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HACT_PKT_EN		(1<<19)
/* Back Porch Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HBP_PKT_EN		(1<<18)
/* hse Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HSE_PKT_EN		(1<<17)
/* hsa Packet Enable */
#define	DSI_LCD1_CTRL_1_CFG_L1_HSA_PKT_EN		(1<<16)
/* All Item Enable after Pixel Data */
#define	DSI_LCD1_CTRL_1_CFG_L1_ALL_SLOT_EN		(1<<15)
/* Extra Long Packet Enable after Pixel Data */
#define	DSI_LCD1_CTRL_1_CFG_L1_HEX_SLOT_EN		(1<<14)
/*		Bit(s) DSI_LCD1_CTRL_1_RSRV_13_11 reserved */
/* Turn Around Bus at Last h Line */
#define	DSI_LCD1_CTRL_1_CFG_L1_LAST_LINE_TURN		(1<<10)
/* Go to Low Power Every Frame */
#define	DSI_LCD1_CTRL_1_CFG_L1_LPM_FRAME_EN		(1<<9)
/* Go to Low Power Every Line */
#define	DSI_LCD1_CTRL_1_CFG_L1_LPM_LINE_EN		(1<<8)
/*		Bit(s) DSI_LCD1_CTRL_1_RSRV_7_4 reserved */
/* DSI Transmission Mode for LCD 1 */
#define DSI_LCD1_CTRL_1_CFG_L1_BURST_MODE_SHIFT		2
#define DSI_LCD1_CTRL_1_CFG_L1_BURST_MODE_MASK		(3<<2)
/* LCD 1 Input Data RGB Mode for LCD 1 */
#define DSI_LCD2_CTRL_1_CFG_L1_RGB_TYPE_SHIFT		0
#define DSI_LCD2_CTRL_1_CFG_L1_RGB_TYPE_MASK		(3<<2)

/*	DSI_PHY_CTRL_2		0x0088	DPHY Control Register 2 */
/*		Bit(s) DSI_PHY_CTRL_2_RSRV_31_12 reserved */
/* DPHY LP Receiver Enable */
#define	DSI_PHY_CTRL_2_CFG_CSR_LANE_RESC_EN_MASK	(0xf<<8)
#define	DSI_PHY_CTRL_2_CFG_CSR_LANE_RESC_EN_SHIFT	8
/* DPHY Data Lane Enable */
#define	DSI_PHY_CTRL_2_CFG_CSR_LANE_EN_MASK		(0xf<<4)
#define	DSI_PHY_CTRL_2_CFG_CSR_LANE_EN_SHIFT		4
/* DPHY Bus Turn Around */
#define	DSI_PHY_CTRL_2_CFG_CSR_LANE_TURN_MASK		(0xf)
#define	DSI_PHY_CTRL_2_CFG_CSR_LANE_TURN_SHIFT		0

/*	DSI_CPU_CMD_1		0x0024	DSI CPU Packet Command Register 1 */
/*		Bit(s) DSI_CPU_CMD_1_RSRV_31_24 reserved */
/* LPDT TX Enable */
#define	DSI_CPU_CMD_1_CFG_TXLP_LPDT_MASK		(0xf<<20)
#define	DSI_CPU_CMD_1_CFG_TXLP_LPDT_SHIFT		20
/* ULPS TX Enable */
#define	DSI_CPU_CMD_1_CFG_TXLP_ULPS_MASK		(0xf<<16)
#define	DSI_CPU_CMD_1_CFG_TXLP_ULPS_SHIFT		16
/* Low Power TX Trigger Code */
#define	DSI_CPU_CMD_1_CFG_TXLP_TRIGGER_CODE_MASK	(0xffff)
#define	DSI_CPU_CMD_1_CFG_TXLP_TRIGGER_CODE_SHIFT	0

/*	DSI_PHY_TIME_0		0x00c0	DPHY Timing Control Register 0 */
/* Length of HS Exit Period in tx_clk_esc Cycles */
#define	DSI_PHY_TIME_0_CFG_CSR_TIME_HS_EXIT_MASK		(0xff<<24)
#define	DSI_PHY_TIME_0_CFG_CSR_TIME_HS_EXIT_SHIFT	24
/* DPHY HS Trail Period Length */
#define	DSI_PHY_TIME_0_CFG_CSR_TIME_HS_TRAIL_MASK	(0xff<<16)
#define	DSI_PHY_TIME_0_CFG_CSR_TIME_HS_TRAIL_SHIFT	16
/* DPHY HS Zero State Length */
#define	DSI_PHY_TIME_0_CDG_CSR_TIME_HS_ZERO_MASK		(0xff<<8)
#define	DSI_PHY_TIME_0_CDG_CSR_TIME_HS_ZERO_SHIFT	8
/* DPHY HS Prepare State Length */
#define	DSI_PHY_TIME_0_CFG_CSR_TIME_HS_PREP_MASK		(0xff)
#define	DSI_PHY_TIME_0_CFG_CSR_TIME_HS_PREP_SHIFT	0

/*	DSI_PHY_TIME_1		0x00c4	DPHY Timing Control Register 1 */
/* Time to Drive LP-00 by New Transmitter */
#define	DSI_PHY_TIME_1_CFG_CSR_TIME_TA_GET_MASK		(0xff<<24)
#define	DSI_PHY_TIME_1_CFG_CSR_TIME_TA_GET_SHIFT	24
/* Time to Drive LP-00 after Turn Request */
#define	DSI_PHY_TIME_1_CFG_CSR_TIME_TA_GO_MASK		(0xff<<16)
#define	DSI_PHY_TIME_1_CFG_CSR_TIME_TA_GO_SHIFT		16
/* DPHY HS Wakeup Period Length */
#define	DSI_PHY_TIME_1_CFG_CSR_TIME_WAKEUP_MASK		(0xffff)
#define	DSI_PHY_TIME_1_CFG_CSR_TIME_WAKEUP_SHIFT	0

/*	DSI_PHY_TIME_2		0x00c8	DPHY Timing Control Register 2 */
/* DPHY CLK Exit Period Length */
#define	DSI_PHY_TIME_2_CFG_CSR_TIME_CK_EXIT_MASK		(0xff<<24)
#define	DSI_PHY_TIME_2_CFG_CSR_TIME_CK_EXIT_SHIFT	24
/* DPHY CLK Trail Period Length */
#define	DSI_PHY_TIME_2_CFG_CSR_TIME_CK_TRAIL_MASK	(0xff<<16)
#define	DSI_PHY_TIME_2_CFG_CSR_TIME_CK_TRAIL_SHIFT	16
/* DPHY CLK Zero State Length */
#define	DSI_PHY_TIME_2_CFG_CSR_TIME_CK_ZERO_MASK		(0xff<<8)
#define	DSI_PHY_TIME_2_CFG_CSR_TIME_CK_ZERO_SHIFT	8
/* DPHY CLK LP Length */
#define	DSI_PHY_TIME_2_CFG_CSR_TIME_CK_LPX_MASK		(0xff)
#define	DSI_PHY_TIME_2_CFG_CSR_TIME_CK_LPX_SHIFT	0

/*	DSI_PHY_TIME_3		0x00cc	DPHY Timing Control Register 3 */
/*		Bit(s) DSI_PHY_TIME_3_RSRV_31_16 reserved */
/* DPHY LP Length */
#define	DSI_PHY_TIME_3_CFG_CSR_TIME_LPX_MASK		(0xff<<8)
#define	DSI_PHY_TIME_3_CFG_CSR_TIME_LPX_SHIFT		8
/* DPHY HS req to rdy Length */
#define	DSI_PHY_TIME_3_CFG_CSR_TIME_REQRDY_MASK		(0xff)
#define	DSI_PHY_TIME_3_CFG_CSR_TIME_REQRDY_SHIFT	0

/* DSI timings */
#define DSI_ESC_CLK                 66  /* Unit: Mhz */
#define DSI_ESC_CLK_T               15  /* Unit: ns */


/* VDMA */
#define VDMA_ARBR_CTRL		0x300
#define VDMA_IRQR		0x304
#define VDMA_IRQM		0x308
#define VDMA_IRQS		0x30C
#define VDMA_MDMA_ARBR_CTRL	0x310
#define VDMA_DC_SADDR_1		0x320
#define VDMA_DC_SADDR_2         0x3A0
#define VDMA_DC_SZ_1		0x324
#define VDMA_DC_SZ_2            0x3A4
#define VDMA_CTRL_1		0x328
#define VDMA_CTRL_2             0x3A8
#define VDMA_SRC_SZ_1		0x32C
#define VDMA_SRC_SZ_2           0x3AC
#define VDMA_SA_1		0x330
#define VDMA_SA_2               0x3B0
#define VDMA_DA_1		0x334
#define VDMA_DA_2               0x3B4
#define VDMA_SZ_1		0x338
#define VDMA_SZ_2               0x3B8
#define VDMA_PITCH_1		0x33C
#define VDMA_PITCH_2            0x3BC
#define VDMA_ROT_CTRL_1		0x340
#define VDMA_ROT_CTRL_2         0x3C0
#define VDMA_RAM_CTRL0_1	0x344
#define VDMA_RAM_CTRL0_2        0x3C4
#define VDMA_RAM_CTRL1_1	0x348
#define VDMA_RAM_CTRL1_2        0x3C8

/* CMU */
#define CMU_PIP_DE_H_CFG	0x0008
#define CMU_PRI1_H_CFG		0x000C
#define CMU_PRI2_H_CFG		0x0010
#define CMU_ACE_MAIN_DE1_H_CFG	0x0014
#define CMU_ACE_MAIN_DE2_H_CFG	0x0018
#define CMU_ACE_PIP_DE1_H_CFG	0x001C
#define CMU_ACE_PIP_DE2_H_CFG	0x0020
#define CMU_PIP_DE_V_CFG	0x0024
#define CMU_PRI_V_CFG		0x0028
#define CMU_ACE_MAIN_DE_V_CFG	0x002C
#define CMU_ACE_PIP_DE_V_CFG	0x0030
#define CMU_BAR_0_CFG		0x0034
#define CMU_BAR_1_CFG		0x0038
#define CMU_BAR_2_CFG		0x003C
#define CMU_BAR_3_CFG		0x0040
#define CMU_BAR_4_CFG		0x0044
#define CMU_BAR_5_CFG		0x0048
#define CMU_BAR_6_CFG		0x004C
#define CMU_BAR_7_CFG		0x0050
#define CMU_BAR_8_CFG		0x0054
#define CMU_BAR_9_CFG		0x0058
#define CMU_BAR_10_CFG		0x005C
#define CMU_BAR_11_CFG		0x0060
#define CMU_BAR_12_CFG		0x0064
#define CMU_BAR_13_CFG		0x0068
#define CMU_BAR_14_CFG		0x006C
#define CMU_BAR_15_CFG		0x0070
#define CMU_BAR_CTRL		0x0074
#define PATTERN_TOTAL		0x0078
#define PATTERN_ACTIVE		0x007C
#define PATTERN_FRONT_PORCH	0x0080
#define PATTERN_BACK_PORCH	0x0084
#define CMU_CLK_CTRL		0x0088

#define CMU_ICSC_M_C0_L		0x0900
#define CMU_ICSC_M_C0_H		0x0901
#define CMU_ICSC_M_C1_L		0x0902
#define CMU_ICSC_M_C1_H		0x0903
#define CMU_ICSC_M_C2_L		0x0904
#define CMU_ICSC_M_C2_H		0x0905
#define CMU_ICSC_M_C3_L		0x0906
#define CMU_ICSC_M_C3_H		0x0907
#define CMU_ICSC_M_C4_L		0x0908
#define CMU_ICSC_M_C4_H		0x0909
#define CMU_ICSC_M_C5_L		0x090A
#define CMU_ICSC_M_C5_H		0x090B
#define CMU_ICSC_M_C6_L		0x090C
#define CMU_ICSC_M_C6_H		0x090D
#define CMU_ICSC_M_C7_L		0x090E
#define CMU_ICSC_M_C7_H		0x090F
#define CMU_ICSC_M_C8_L		0x0910
#define CMU_ICSC_M_C8_H		0x0911
#define CMU_ICSC_M_O1_0		0x0914
#define CMU_ICSC_M_O1_1		0x0915
#define CMU_ICSC_M_O1_2		0x0916
#define CMU_ICSC_M_O2_0		0x0918
#define CMU_ICSC_M_O2_1		0x0919
#define CMU_ICSC_M_O2_2		0x091A
#define CMU_ICSC_M_O3_0		0x091C
#define CMU_ICSC_M_O3_1		0x091D
#define CMU_ICSC_M_O3_2		0x091E
#define CMU_ICSC_P_C0_L		0x0920
#define CMU_ICSC_P_C0_H		0x0921
#define CMU_ICSC_P_C1_L		0x0922
#define CMU_ICSC_P_C1_H		0x0923
#define CMU_ICSC_P_C2_L		0x0924
#define CMU_ICSC_P_C2_H		0x0925
#define CMU_ICSC_P_C3_L		0x0926
#define CMU_ICSC_P_C3_H		0x0927
#define CMU_ICSC_P_C4_L		0x0928
#define CMU_ICSC_P_C4_H		0x0929
#define CMU_ICSC_P_C5_L		0x092A
#define CMU_ICSC_P_C5_H		0x092B
#define CMU_ICSC_P_C6_L		0x092C
#define CMU_ICSC_P_C6_H		0x092D
#define CMU_ICSC_P_C7_L		0x092E
#define CMU_ICSC_P_C7_H		0x092F
#define CMU_ICSC_P_C8_L		0x0930
#define CMU_ICSC_P_C8_H		0x0931
#define CMU_ICSC_P_O1_0		0x0934
#define CMU_ICSC_P_O1_1		0x0935
#define CMU_ICSC_P_O1_2		0x0936
#define CMU_ICSC_P_O2_0		0x0938
#define CMU_ICSC_P_O2_1		0x0939
#define CMU_ICSC_P_O2_2		0x093A
#define CMU_ICSC_P_O3_0		0x093C
#define CMU_ICSC_P_O3_1		0x093D
#define CMU_ICSC_P_O3_2		0x093E
#define CMU_BR_M_EN		0x0940
#define CMU_BR_M_TH1_L		0x0942
#define CMU_BR_M_TH1_H		0x0943
#define CMU_BR_M_TH2_L		0x0944
#define CMU_BR_M_TH2_H		0x0945
#define CMU_ACE_M_EN		0x0950
#define CMU_ACE_M_WFG1		0x0951
#define CMU_ACE_M_WFG2		0x0952
#define CMU_ACE_M_WFG3		0x0953
#define CMU_ACE_M_TH0		0x0954
#define CMU_ACE_M_TH1		0x0955
#define CMU_ACE_M_TH2		0x0956
#define CMU_ACE_M_TH3		0x0957
#define CMU_ACE_M_TH4		0x0958
#define CMU_ACE_M_TH5		0x0959
#define CMU_ACE_M_OP0_L		0x095A
#define CMU_ACE_M_OP0_H		0x095B
#define CMU_ACE_M_OP5_L		0x095C
#define CMU_ACE_M_OP5_H		0x095D
#define CMU_ACE_M_GB2		0x095E
#define CMU_ACE_M_GB3		0x095F
#define CMU_ACE_M_MS1		0x0960
#define CMU_ACE_M_MS2		0x0961
#define CMU_ACE_M_MS3		0x0962
#define CMU_BR_P_EN		0x0970
#define CMU_BR_P_TH1_L		0x0972
#define CMU_BR_P_TH1_H		0x0973
#define CMU_BR_P_TH2_L		0x0974
#define CMU_BR_P_TH2_H		0x0975
#define CMU_ACE_P_EN		0x0980
#define CMU_ACE_P_WFG1		0x0981
#define CMU_ACE_P_WFG2		0x0982
#define CMU_ACE_P_WFG3		0x0983
#define CMU_ACE_P_TH0		0x0984
#define CMU_ACE_P_TH1		0x0985
#define CMU_ACE_P_TH2		0x0986
#define CMU_ACE_P_TH3		0x0987
#define CMU_ACE_P_TH4		0x0988
#define CMU_ACE_P_TH5		0x0989
#define CMU_ACE_P_OP0_L		0x098A
#define CMU_ACE_P_OP0_H		0x098B
#define CMU_ACE_P_OP5_L		0x098C
#define CMU_ACE_P_OP5_H		0x098D
#define CMU_ACE_P_GB2		0x098E
#define CMU_ACE_P_GB3		0x098F
#define CMU_ACE_P_MS1		0x0990
#define CMU_ACE_P_MS2		0x0991
#define CMU_ACE_P_MS3		0x0992
#define CMU_FTDC_M_EN		0x09A0
#define CMU_FTDC_P_EN		0x09A1
#define CMU_FTDC_INLOW_L	0x09A2
#define CMU_FTDC_INLOW_H	0x09A3
#define CMU_FTDC_INHIGH_L	0x09A4
#define CMU_FTDC_INHIGH_H	0x09A5
#define CMU_FTDC_OUTLOW_L	0x09A6
#define CMU_FTDC_OUTLOW_H	0x09A7
#define CMU_FTDC_OUTHIGH_L	0x09A8
#define CMU_FTDC_OUTHIGH_H	0x09A9
#define CMU_FTDC_YLOW		0x09AA
#define CMU_FTDC_YHIGH		0x09AB
#define CMU_FTDC_CH1		0x09AC
#define CMU_FTDC_CH2_L		0x09AE
#define CMU_FTDC_CH2_H		0x09AF
#define CMU_FTDC_CH3_L		0x09B0
#define CMU_FTDC_CH3_H		0x09B1
#define CMU_FTDC_1_C00_6	0x09B2
#define CMU_FTDC_1_C01_6	0x09B8
#define CMU_FTDC_1_C11_6	0x09BE
#define CMU_FTDC_1_C10_6	0x09C4
#define CMU_FTDC_1_OFF00_6	0x09CA
#define CMU_FTDC_1_OFF10_6	0x09D0
#define CMU_HS_M_EN		0x0A00
#define CMU_HS_M_AX1_L		0x0A02
#define CMU_HS_M_AX1_H		0x0A03
#define CMU_HS_M_AX2_L		0x0A04
#define CMU_HS_M_AX2_H		0x0A05
#define CMU_HS_M_AX3_L		0x0A06
#define CMU_HS_M_AX3_H		0x0A07
#define CMU_HS_M_AX4_L		0x0A08
#define CMU_HS_M_AX4_H		0x0A09
#define CMU_HS_M_AX5_L		0x0A0A
#define CMU_HS_M_AX5_H		0x0A0B
#define CMU_HS_M_AX6_L		0x0A0C
#define CMU_HS_M_AX6_H		0x0A0D
#define CMU_HS_M_AX7_L		0x0A0E
#define CMU_HS_M_AX7_H		0x0A0F
#define CMU_HS_M_AX8_L		0x0A10
#define CMU_HS_M_AX8_H		0x0A11
#define CMU_HS_M_AX9_L		0x0A12
#define CMU_HS_M_AX9_H		0x0A13
#define CMU_HS_M_AX10_L		0x0A14
#define CMU_HS_M_AX10_H		0x0A15
#define CMU_HS_M_AX11_L		0x0A16
#define CMU_HS_M_AX11_H		0x0A17
#define CMU_HS_M_AX12_L		0x0A18
#define CMU_HS_M_AX12_H		0x0A19
#define CMU_HS_M_AX13_L		0x0A1A
#define CMU_HS_M_AX13_H		0x0A1B
#define CMU_HS_M_AX14_L		0x0A1C
#define CMU_HS_M_AX14_H		0x0A1D
#define CMU_HS_M_H1_H14		0x0A1E
#define CMU_HS_M_S1_S14		0x0A2C
#define CMU_HS_M_GL		0x0A3A
#define CMU_HS_M_MAXSAT_RGB_Y_L	0x0A3C
#define CMU_HS_M_MAXSAT_RGB_Y_H	0x0A3D
#define CMU_HS_M_MAXSAT_RCR_L	0x0A3E
#define CMU_HS_M_MAXSAT_RCR_H	0x0A3F
#define CMU_HS_M_MAXSAT_RCB_L	0x0A40
#define CMU_HS_M_MAXSAT_RCB_H	0x0A41
#define CMU_HS_M_MAXSAT_GCR_L	0x0A42
#define CMU_HS_M_MAXSAT_GCR_H	0x0A43
#define CMU_HS_M_MAXSAT_GCB_L	0x0A44
#define CMU_HS_M_MAXSAT_GCB_H	0x0A45
#define CMU_HS_M_MAXSAT_BCR_L	0x0A46
#define CMU_HS_M_MAXSAT_BCR_H	0x0A47
#define CMU_HS_M_MAXSAT_BCB_L	0x0A48
#define CMU_HS_M_MAXSAT_BCB_H	0x0A49
#define CMU_HS_M_ROFF_L		0x0A4A
#define CMU_HS_M_ROFF_H		0x0A4B
#define CMU_HS_M_GOFF_L		0x0A4C
#define CMU_HS_M_GOFF_H		0x0A4D
#define CMU_HS_M_BOFF_L		0x0A4E
#define CMU_HS_M_BOFF_H		0x0A4F
#define CMU_HS_P_EN		0x0A50
#define CMU_HS_P_AX1_L		0x0A52
#define CMU_HS_P_AX1_H		0x0A53
#define CMU_HS_P_AX2_L		0x0A54
#define CMU_HS_P_AX2_H		0x0A55
#define CMU_HS_P_AX3_L		0x0A56
#define CMU_HS_P_AX3_H		0x0A57
#define CMU_HS_P_AX4_L		0x0A58
#define CMU_HS_P_AX4_H		0x0A59
#define CMU_HS_P_AX5_L		0x0A5A
#define CMU_HS_P_AX5_H		0x0A5B
#define CMU_HS_P_AX6_L		0x0A5C
#define CMU_HS_P_AX6_H		0x0A5D
#define CMU_HS_P_AX7_L		0x0A5E
#define CMU_HS_P_AX7_H		0x0A5F
#define CMU_HS_P_AX8_L		0x0A60
#define CMU_HS_P_AX8_H		0x0A61
#define CMU_HS_P_AX9_L		0x0A62
#define CMU_HS_P_AX9_H		0x0A63
#define CMU_HS_P_AX10_L		0x0A64
#define CMU_HS_P_AX10_H		0x0A65
#define CMU_HS_P_AX11_L		0x0A66
#define CMU_HS_P_AX11_H		0x0A67
#define CMU_HS_P_AX12_L		0x0A68
#define CMU_HS_P_AX12_H		0x0A69
#define CMU_HS_P_AX13_L		0x0A6A
#define CMU_HS_P_AX13_H		0x0A6B
#define CMU_HS_P_AX14_L		0x0A6C
#define CMU_HS_P_AX14_H		0x0A6D
#define CMU_HS_P_H1_H14		0x0A6E
#define CMU_HS_P_S1_S14		0x0A7C
#define CMU_HS_P_GL		0x0A8A
#define CMU_HS_P_MAXSAT_RGB_Y_L	0x0A8C
#define CMU_HS_P_MAXSAT_RGB_Y_H	0x0A8D
#define CMU_HS_P_MAXSAT_RCR_L	0x0A8E
#define CMU_HS_P_MAXSAT_RCR_H	0x0A8F
#define CMU_HS_P_MAXSAT_RCB_L	0x0A90
#define CMU_HS_P_MAXSAT_RCB_H	0x0A91
#define CMU_HS_P_MAXSAT_GCR_L	0x0A92
#define CMU_HS_P_MAXSAT_GCR_H	0x0A93
#define CMU_HS_P_MAXSAT_GCB_L	0x0A94
#define CMU_HS_P_MAXSAT_GCB_H	0x0A95
#define CMU_HS_P_MAXSAT_BCR_L	0x0A96
#define CMU_HS_P_MAXSAT_BCR_H	0x0A97
#define CMU_HS_P_MAXSAT_BCB_L	0x0A98
#define CMU_HS_P_MAXSAT_BCB_H	0x0A99
#define CMU_HS_P_ROFF_L		0x0A9A
#define CMU_HS_P_ROFF_H		0x0A9B
#define CMU_HS_P_GOFF_L		0x0A9C
#define CMU_HS_P_GOFF_H		0x0A9D
#define CMU_HS_P_BOFF_L		0x0A9E
#define CMU_HS_P_BOFF_H		0x0A9F
#define CMU_GLCSC_M_C0_L	0x0AA0
#define CMU_GLCSC_M_C0_H	0x0AA1
#define CMU_GLCSC_M_C1_L	0x0AA2
#define CMU_GLCSC_M_C1_H	0x0AA3
#define CMU_GLCSC_M_C2_L	0x0AA4
#define CMU_GLCSC_M_C2_H	0x0AA5
#define CMU_GLCSC_M_C3_L	0x0AA6
#define CMU_GLCSC_M_C3_H	0x0AA7
#define CMU_GLCSC_M_C4_L	0x0AA8
#define CMU_GLCSC_M_C4_H	0x0AA9
#define CMU_GLCSC_M_C5_L	0x0AAA
#define CMU_GLCSC_M_C5_H	0x0AAB
#define CMU_GLCSC_M_C6_L	0x0AAC
#define CMU_GLCSC_M_C6_H	0x0AAD
#define CMU_GLCSC_M_C7_L	0x0AAE
#define CMU_GLCSC_M_C7_H	0x0AAF
#define CMU_GLCSC_M_C8_L	0x0AB0
#define CMU_GLCSC_M_C8_H	0x0AB1
#define CMU_GLCSC_M_O1_1	0x0AB4
#define CMU_GLCSC_M_O1_2	0x0AB5
#define CMU_GLCSC_M_O1_3	0x0AB6
#define CMU_GLCSC_M_O2_1	0x0AB8
#define CMU_GLCSC_M_O2_2	0x0AB9
#define CMU_GLCSC_M_O2_3	0x0ABA
#define CMU_GLCSC_M_O3_1	0x0ABC
#define CMU_GLCSC_M_O3_2	0x0ABD
#define CMU_GLCSC_M_O3_3	0x0ABE
#define CMU_GLCSC_P_C0_L	0x0AC0
#define CMU_GLCSC_P_C0_H	0x0AC1
#define CMU_GLCSC_P_C1_L	0x0AC2
#define CMU_GLCSC_P_C1_H	0x0AC3
#define CMU_GLCSC_P_C2_L	0x0AC4
#define CMU_GLCSC_P_C2_H	0x0AC5
#define CMU_GLCSC_P_C3_L	0x0AC6
#define CMU_GLCSC_P_C3_H	0x0AC7
#define CMU_GLCSC_P_C4_L	0x0AC8
#define CMU_GLCSC_P_C4_H	0x0AC9
#define CMU_GLCSC_P_C5_L	0x0ACA
#define CMU_GLCSC_P_C5_H	0x0ACB
#define CMU_GLCSC_P_C6_L	0x0ACC
#define CMU_GLCSC_P_C6_H	0x0ACD
#define CMU_GLCSC_P_C7_L	0x0ACE
#define CMU_GLCSC_P_C7_H	0x0ACF
#define CMU_GLCSC_P_C8_L	0x0AD0
#define CMU_GLCSC_P_C8_H	0x0AD1
#define CMU_GLCSC_P_O1_1	0x0AD4
#define CMU_GLCSC_P_O1_2	0x0AD5
#define CMU_GLCSC_P_O1_3	0x0AD6
#define CMU_GLCSC_P_O2_1	0x0AD8
#define CMU_GLCSC_P_O2_2	0x0AD9
#define CMU_GLCSC_P_O2_3	0x0ADA
#define CMU_GLCSC_P_O3_1	0x0ADC
#define CMU_GLCSC_P_O3_2	0x0ADD
#define CMU_GLCSC_P_O3_3	0x0ADE
#define CMU_PIXVAL_M_EN		0x0AE0
#define CMU_PIXVAL_P_EN		0x0AE1

#define CMU_CLK_CTRL_TCLK	0x0
#define CMU_CLK_CTRL_SCLK	0x2
#define CMU_CLK_CTRL_MSK	0x2
#define CMU_CLK_CTRL_ENABLE 0x1

#define LCD_TOP_CTRL_TV		0x2
#define LCD_TOP_CTRL_PNL	0x0
#define LCD_TOP_CTRL_SEL_MSK	0x2
#define LCD_IO_CMU_IN_SEL_MSK	(0x3 << 20)
#define LCD_IO_CMU_IN_SEL_TV	0
#define LCD_IO_CMU_IN_SEL_PN	1
#define LCD_IO_CMU_IN_SEL_PN2	2
#define LCD_IO_TV_OUT_SEL_MSK	(0x3 << 26)
#define LCD_IO_PN_OUT_SEL_MSK	(0x3 << 24)
#define LCD_IO_PN2_OUT_SEL_MSK	(0x3 << 28)
#define LCD_IO_TV_OUT_SEL_NON	3
#define LCD_IO_PN_OUT_SEL_NON	3
#define LCD_IO_PN2_OUT_SEL_NON	3
#define LCD_TOP_CTRL_CMU_ENABLE 0x1
#define LCD_IO_OVERL_MSK	0xC00000
#define LCD_IO_OVERL_TV		0x0
#define LCD_IO_OVERL_LCD1	0x400000
#define LCD_IO_OVERL_LCD2	0xC00000
#define HINVERT_MSK			0x4
#define VINVERT_MSK			0x8
#define HINVERT_LEN			0x2
#define VINVERT_LEN			0x3

#define CMU_CTRL			0x88
#define CMU_CTRL_A0_MSK		0x6
#define CMU_CTRL_A0_TV		0x0
#define CMU_CTRL_A0_LCD1	0x1
#define CMU_CTRL_A0_LCD2	0x2
#define CMU_CTRL_A0_HDMI	0x3

#define ICR_DRV_ROUTE_OFF 0x0
#define ICR_DRV_ROUTE_TV 0x1
#define ICR_DRV_ROUTE_LCD1 0x2
#define ICR_DRV_ROUTE_LCD2 0x3

int pxa168fb_init(struct pxa168fb_mach_info *mi);
#endif /* __ASM_MACH_PXA168FB_H */
