/*
 * (C) Copyright 2010
 * Marvell Semiconductors Ltd.
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * Configuration for Avengers Soc.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CONFIG_H
#define __CONFIG_H

#define CONFIG_MMP			1 /*  mmp family */
#define CONFIG_AVENGERS			1
#define CONFIG_IDENT_STRING   		"\nMarvell Avengers SOC"
#define CONFIG_DISPLAY_BOARDINFO
#define CONFIG_SKIP_LOWLEVEL_INIT	/* disable board lowlevel_init */

#define CONFIG_MFPR_MMIOBASE		(0xd401e000)
#define	CONFIG_MARVELL_MFP		1
#define CONFIG_SYS_HZ   		(3250000)      /* KV - Timer 0 is clocked at 3.25 MHz */
#define CONFIG_SYS_TIMERBASE 		0xD4014000
#define CONFIG_SYS_WATCHDOG_TIMER	0xD4014000
#define CONFIG_GPIO_REGBASE		0xD4019000
#define CONFIG_BOARD_EARLY_INIT_F	1

/*-----------------------------------------------------------------------
 * Physical Memory Map
 */
#define CONFIG_NR_DRAM_BANKS    	1   /* we have 1 bank of DRAM */
#define PHYS_SDRAM_1            	0x00000000   /* SDRAM Bank #1 */
#define CONFIG_SYS_LOAD_ADDR		0x1000000	/* default load adr- 8M */
#define CONFIG_SYS_ENV_SIZE            	0x10000   /* Total Size of Environment Sector */
#define CONFIG_STACKSIZE       		(128*1024)
#define CONFIG_SYS_MALLOC_LEN      	(CONFIG_SYS_ENV_SIZE + 1024*1024)
#define CONFIG_SYS_GBL_DATA_SIZE   	128   /* size in bytes reserved for initial data */
#define CONFIG_SYS_CBSIZE         	512
#define CONFIG_SYS_PBSIZE         	(CONFIG_SYS_CBSIZE+sizeof(CONFIG_SYS_PROMPT)+16)
#define CONFIG_SYS_BARGSIZE       	CONFIG_SYS_CBSIZE
#define CONFIG_SYS_BOOTMAPSZ      	(8<<20)   /* Initial Memory map for Linux */

#define CONFIG_SYS_SDRAM_BASE		PHYS_SDRAM_1
#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_SYS_SDRAM_BASE + 0x1000 - CONFIG_SYS_GBL_DATA_SIZE)

/***************************
 *  Console configuration
 */
#define CONFIG_SYS_NS16550
#define CONFIG_SYS_NS16550_SERIAL
#define CONFIG_SYS_NS16550_REG_SIZE   	-4
#define CONFIG_SYS_NS16550_CLK        	14745600
#define CONFIG_BAUDRATE        		115200
#define CONFIG_SYS_BAUDRATE_TABLE     	{ 9600, 19200, 38400, 57600, 115200 }
#define CONFIG_CONS_INDEX     		2
#define CONFIG_SYS_NS16550_COM2       	0xD4018000

#define CONFIG_SYS_PROMPT         	"Avengers>> "
#define CONFIG_SYS_PROMPT_HUSH_PS2     	"> "
#define CONFIG_SYS_LONGHELP
#define CONFIG_SYS_HUSH_PARSER		1
#define CONFIG_CMD_RUN			1
#define CONFIG_AUTO_COMPLETE
#define CONFIG_CMDLINE_EDITING
#define CONFIG_SYS_64BIT_VSPRINTF

#define CONFIG_SYS_MAXARGS        	16
#define CONFIG_CMDLINE_TAG       	1
#define CONFIG_SETUP_MEMORY_TAGS 	1
#define CONFIG_MISC_INIT_R         	1

/*-----------------------------------------------------------------------
 * MMC configuration
 */
#define CONFIG_CMD_MMC		1
#define CONFIG_MMC		1
#define CONFIG_GENERIC_MMC	1
#define CONFIG_PXA9XX_SDH	1
#define CONFIG_PXA9XX_FREQ_FS	0x80
#define CONFIG_DOS_PARTITION    1
#define CONFIG_SYS_MMC_NUM	1
#define CONFIG_SYS_MMC_BASE	{0xd4281000}

/*-----------------------------------------------------------------------
 * NAND and DFC configuration
 */
#define CONFIG_CMD_NAND                 1
#define CONFIG_SYS_NAND_MAX_CHIPS	1
#define CONFIG_SYS_MAX_NAND_DEVICE      2         /* Max number of NAND devices */
#define CONFIG_SYS_NAND_BASE            0xD4283000
#define CONFIG_SYS_NAND_QUIET_TEST
#define PXA3XX_NAND_NAKED_CMD_SUPPORT
#define CONFIG_PXA3XX_NAND_DEF_CLOCK	(156 * 1000000)
#define CONFIG_NAND_PXA3XX
#define CONFIG_PXA3XX_BBM

/*-----------------------------------------------------------------------
 * SPI NOR configuration
 */
#if defined(CONFIG_MMP_SPI)
#define CONFIG_SSP_CLK			6500000
#define CONFIG_SYS_SSP_BASE		0xD401C000
#define CONFIG_ENV_SPI_BUS		0
#define CONFIG_ENV_SPI_CS		110
#define CONFIG_ENV_SPI_MAX_HZ		26000
#define CONFIG_ENV_SPI_MODE		0
#define CONFIG_CMD_SF
#define CONFIG_SPI_FLASH
#define CONFIG_SPI_FLASH_STMICRO	1
#define CONFIG_SPI_FLASH_MACRONIX	1
#define SPI_ENV_SETTINGS \
	"flashboot=sf probe " MK_STR(CONFIG_ENV_SPI_CS) " " \
	MK_STR(CONFIG_ENV_SPI_MAX_HZ) " "\
	MK_STR(CONFIG_ENV_SPI_MODE)  ";" \
	"sf read 0x1000000 0x100000 0x300000\0"
#else
#define SPI_ENV_SETTINGS
#endif /*CONFIG_MMP_SPI*/

/*-----------------------------------------------------------------------
 * USB configuration
 */
#define CONFIG_MMPUDC		1
#define CONFIG_URB_BUF_SIZE	256
#define CONFIG_USB_DEVICE	1
#define CONFIG_USB_REG_BASE	0xd4208000
#define CONFIG_USB_PHY_BASE	0xd4207000

/*-----------------------------------------------------------------------
 * FASTBOOT configuration
 */
#define CONFIG_USBD_VENDORID		0x18d1
#define CONFIG_USBD_PRODUCTID		0x4e11
#define CONFIG_USBD_MANUFACTURER 	"Marvell Inc."
#define CONFIG_USBD_PRODUCT_NAME 	"Android 2.1"
#define CONFIG_SERIAL_NUM	 	"MRUUUVLs001"
#define CONFIG_USBD_CONFIGURATION_STR	"fastboot"
#define CONFIG_MTD_DEVICE	1
#define CONFIG_CMD_MTDPARTS	1
#define CONFIG_FB_FOR_MMC	1
#define CONFIG_SYS_FASTBOOT_PARTNUM	3
#define CONFIG_SYS_FASTBOOT_PARTS	{"system", "userdata","telephony"}
#define CONFIG_SYS_FASTBOOT_USE_YAFFS	{	0,	    0,	    0}
#define CONFIG_SYS_FASTBOOT_ONFLY_SZ	0x20000
#define CONFIG_CMD_FASTBOOT	1
#define USB_LOADADDR		0x1000000

/***************************************/
/* LINUX BOOT and other ENV PARAMETERS */
/***************************************/
#define CONFIG_ZERO_BOOTDELAY_CHECK
#define CONFIG_SHOW_BOOT_PROGRESS
#define CONFIG_BOOTDELAY                3
#define CONFIG_BOOTCOMMAND              "bootm 0x1000000"

#define CONFIG_CMD_ENV                  1
#define CONFIG_ENV_OVERWRITE            /* allow to change env parameters */
#define CONFIG_ENV_SIZE                 0x20000
#define CONFIG_ENV_OFFSET               0x80000
#define CONFIG_ENV_IS_NOWHERE           1
#define CONFIG_SYS_NO_FLASH             1

#define MTDPARTS_DEFAULT		"mtdparts=pxa3xx_nand-0:1m(bootloader)ro," \
					"112m(system),64m(userdata),-(reserved)"
#define MTDIDS_DEFAULT			"nand0=pxa3xx_nand-0"
#define CONFIG_EXTRA_ENV_SETTINGS \
	        SPI_ENV_SETTINGS \
		"mtdids=" MTDIDS_DEFAULT "\0" \
		"mtdparts=" MTDPARTS_DEFAULT "\0"
#endif /* __CONFIG_H */
