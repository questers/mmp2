/*
 * (C) Copyright 2008
 * Marvell Semiconductors Ltd. Shanghai, China.
 *
 * Configuration for Tavor EVB board.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CONFIG_MMP2_JASPER_EMMC_H
#define __CONFIG_MMP2_JASPER_EMMC_H

#include <configs/mmp2_g50.h>

/* MMC configuration */
#define CONFIG_CMD_MMC
#define CONFIG_MMC
#define CONFIG_GENERIC_MMC
#define CONFIG_PXA9XX_SDH
#define CONFIG_MMC_SDMA
#define CONFIG_DOS_PARTITION
#define CONFIG_CMD_FAT
#define CONFIG_CMD_EXT2

#define BOOT_FROM_EMMC
#define CONFIG_MMCBOOT			"mmc read 0x4c00 0x2000 0x1100000"

//#define CONFIG_MBR_SECTOR		0x4800

//for legacy fat sd card,0 sector is mbr
#define CONFIG_MBR_SECTOR		0

/*
 * Recovery Mode options
 */
#define CONFIG_RECOVERY_MODE		1
#ifdef CONFIG_RECOVERY_MODE
#define CONFIG_PXA27X_KEYPAD		1
#define CONFIG_MMCBOOT_RECOVERY		"mmc read 0xa2c00 0x2000 0x1100000"
#endif

/*
 * Fastboot Mode options
 */
#ifdef CONFIG_CMD_FASTBOOT
#define CONFIG_EFI_PARTITION		1
#endif

/*
 * Trustboot options
 */
#define CONFIG_TRUST_BOOT		1
#define CONFIG_MMCBOOT_R_UBOOT		"mmc read 0x580 0x280 0x100000"
#define CONFIG_R_UBOOT_LOAD_ADDR	0x100000
#define CONFIG_TRUST_BOOT_MAGIC		0x54525354

/* enviroment is in MMC */
//#undef CONFIG_ENV_IS_NOWHERE
//#define CONFIG_SYS_MMC_ENV_DEV	0 /* env stored in dev#0, part#0 */
//#define CONFIG_SYS_MMC_ENV_PART	0
//#define CONFIG_CMD_SAVEENV
//#define CONFIG_ENV_IS_IN_MMC
//#define CONFIG_CMD_ENV

/***************************************/

/* size and offset of environment */
#define CONFIG_ENV_SIZE			0x8000	/* environment size: 32KB */
#define CONFIG_ENV_OFFSET		0x20000	/* environment offset: 128KB */

#define CONFIG_PXA9XX_FREQ_FS		0x3FF

#undef CONFIG_NAND_PXA3XX
#undef CONFIG_CMD_NAND
#undef CONFIG_CMD_ONENAND
#undef CONFIG_BBM
//#define CONFIG_I2C_MMP2
#endif
/* __CONFIG_MMP2_JASPER_EMMC_H */
