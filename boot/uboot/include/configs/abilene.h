/*
 * (C) Copyright 2010
 * Marvell Semiconductors Ltd.
 * Kiran Vedere <kvedere@marvell.com>
 *
 * Configuration for Abilene Platform
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CONFIG_H
#define __CONFIG_H

#define CONFIG_ARMADA6XX		1 /* ARMADA 6xx Processor family */
#define CONFIG_ARMADA620		1 /* ARMADA 620 CPU (MMP3) */
#define CONFIG_MACH_ABILENE		1
#define CONFIG_IDENT_STRING   		"\nMarvell Abilene Platform"
#define CONFIG_DISPLAY_BOARDINFO
#define CONFIG_SKIP_LOWLEVEL_INIT	/* disable board lowlevel_init */

#define CONFIG_MFPR_MMIOBASE		(0xd401e000)
#define	CONFIG_MARVELL_MFP		1
#define CONFIG_SYS_HZ   		(6500000)      /* Timer 0 is clocked at 6.5 MHz */
#define CONFIG_SYS_TIMERBASE 		0xD4014000
#define CONFIG_SYS_WATCHDOG_TIMER	0xD4014000
#define CONFIG_GPIO_REGBASE		0xD4019000
#define CONFIG_SYS_ARM_WITHOUT_RELOC	1
#define	CONFIG_ARCH_CPU_INIT		1

/* Keep L2 Cache Disabled */
#define CONFIG_L2_OFF			1

/*-----------------------------------------------------------------------
 * Physical Memory Map
 */
#define CONFIG_NR_DRAM_BANKS    	1   /* we have 1 bank of DRAM */
#define PHYS_SDRAM_1            	0x00000000   /* SDRAM Bank #1 */
#define PHYS_SDRAM_1_SIZE		0x20000000   /* 512 MB */
#define PHYS_SDRAM_SIZE_DEC		512
#define CONFIG_SYS_UBOOT_BASE		0x00f00000
#define CONFIG_SYS_LOAD_ADDR		0x01200000
#define CONFIG_SYS_ENV_SIZE            	0x10000   /* Total Size of Environment Sector */
#define CONFIG_STACKSIZE       		(128*1024)
#define CONFIG_SYS_MALLOC_LEN      	(CONFIG_SYS_ENV_SIZE + 1024*1024)
#define CONFIG_SYS_GBL_DATA_SIZE   	128 /* size in bytes reserved for initial data */
#define CONFIG_SYS_CBSIZE         	512
#define CONFIG_SYS_PBSIZE         	(CONFIG_SYS_CBSIZE+sizeof(CONFIG_SYS_PROMPT)+16)
#define CONFIG_SYS_BARGSIZE       	CONFIG_SYS_CBSIZE
#define CONFIG_SYS_BOOTMAPSZ      	(8<<20) /* Initial Memory map for Linux */

#define CONFIG_SYS_SDRAM_BASE		PHYS_SDRAM_1
#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_SYS_SDRAM_BASE + 0x1000 - CONFIG_SYS_GBL_DATA_SIZE)

/***************************
 *  Console configuration
 */
#define CONFIG_SYS_NS16550
#define CONFIG_SYS_NS16550_SERIAL
#define CONFIG_SYS_NS16550_REG_SIZE   	-4
#define CONFIG_SYS_NS16550_CLK        	26000000
#define CONFIG_BAUDRATE        		115200
#define CONFIG_SYS_BAUDRATE_TABLE     	{ 9600, 19200, 38400, 57600, 115200 }
#define CONFIG_CONS_INDEX     		1
#define CONFIG_SYS_NS16550_COM1       	0xD4030000

#define CONFIG_SYS_PROMPT         	"Abilene>> "
#define CONFIG_SYS_PROMPT_HUSH_PS2     	"> "
#define CONFIG_SYS_LONGHELP
#define CONFIG_SYS_HUSH_PARSER		1
#define CONFIG_CMD_RUN			1
#define CONFIG_AUTO_COMPLETE
#define CONFIG_CMDLINE_EDITING
#define CONFIG_SYS_64BIT_VSPRINTF

#define CONFIG_SYS_MAXARGS        	16
#define CONFIG_CMDLINE_TAG       	1
#define CONFIG_SETUP_MEMORY_TAGS 	1
#define CONFIG_MISC_INIT_R         	1

/***************************************/
/* LINUX BOOT and other ENV PARAMETERS */
/***************************************/
#define CONFIG_ZERO_BOOTDELAY_CHECK
#define CONFIG_SHOW_BOOT_PROGRESS
#define CONFIG_BOOTDELAY                3
#define CONFIG_BOOTCOMMAND              "bootm 0x1000000"

#define CONFIG_CMD_ENV                  1
#define CONFIG_ENV_OVERWRITE            /* allow to change env parameters */
#define CONFIG_ENV_SIZE                 0x20000
#define CONFIG_ENV_OFFSET               0x80000
#define CONFIG_ENV_IS_NOWHERE           1
#define CONFIG_SYS_NO_FLASH             1

/*-----------------------------------------------------------------------
 * NAND and DFC configuration
 */
#define CONFIG_CMD_NAND                 1
#define CONFIG_SYS_NAND_MAX_CHIPS	1
#define CONFIG_SYS_MAX_NAND_DEVICE      2         /* Max number of NAND devices */
#define CONFIG_SYS_NAND_BASE            0xD4283000
#define CONFIG_SYS_NAND_QUIET_TEST
#define PXA3XX_NAND_NAKED_CMD_SUPPORT
#define CONFIG_PXA3XX_NAND_DEF_CLOCK	(156 * 1000000)
#define CONFIG_NAND_PXA3XX
#define CONFIG_PXA3XX_BBM

#endif /* __CONFIG_H */
