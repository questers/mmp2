/*
 * (C) Copyright 2008
 * Marvell Semiconductors Ltd. Shanghai, China.
 *
 * Configuration for Tavor EVB board.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CONFIG_H
#define __CONFIG_H

#define CONFIG_MMP			1 /*  mmp family */
#define CONFIG_TTC			1
#define CONFIG_IDENT_STRING   		"\nMarvell TTC_TD SOC"
#define CONFIG_DISPLAY_BOARDINFO
#define CONFIG_SKIP_LOWLEVEL_INIT	/* disable board lowlevel_init */

#define CONFIG_MFPR_MMIOBASE		(0xd401e000)
#define	CONFIG_MARVELL_MFP		1
#define CONFIG_SYS_HZ   		(3250000)      /* KV - Timer 0 is clocked at 3.25 MHz */
#define CONFIG_SYS_TIMERBASE 		0xD4014000
#define CONFIG_SYS_WATCHDOG_TIMER	0xd4080000
#define CONFIG_GPIO_REGBASE		0xD4019000
#define CONFIG_ARCH_CPU_INIT		1
#define CONFIG_BOARD_EARLY_INIT_F	1
#define CONFIG_CMD_MIPS			1
#define CONFIG_MMP_POWER		1
#define CONFIG_SYS_I2C_POWER_ADDR	0x34		/* sanremo base addr */
#define CONFIG_SYS_I2C_SPEED		0
#define CONFIG_SYS_I2C_SLAVE		0
#define CONFIG_CMD_I2C			1
#define CONFIG_CMD_MEMORY		1
#define CONFIG_SYS_MEMTEST_START	0
#define CONFIG_SYS_MEMTEST_END		0x100000

/*-----------------------------------------------------------------------
 * Physical Memory Map
 */
#define CONFIG_NR_DRAM_BANKS    	1   /* we have 1 bank of DRAM */
#define PHYS_SDRAM_1            	0x00000000   /* SDRAM Bank #1 */
#define PHYS_SDRAM_SIZE_DEC     	128

#define CONFIG_SYS_LOAD_ADDR		0x800000	/* default load adr- 8M */
#define CONFIG_STACKSIZE       		(128*1024)
#define CONFIG_ENV_SIZE		 	0x20000
#define CONFIG_SYS_MALLOC_LEN      	(CONFIG_ENV_SIZE + 5*1024*1024)
#define CONFIG_SYS_GBL_DATA_SIZE   	128   /* size in bytes reserved for initial data */
#define CONFIG_SYS_CBSIZE         	512
#define CONFIG_SYS_PBSIZE         	(CONFIG_SYS_CBSIZE+sizeof(CONFIG_SYS_PROMPT)+16)
#define CONFIG_SYS_BARGSIZE       	CONFIG_SYS_CBSIZE
#define CONFIG_SYS_BOOTMAPSZ      	(8<<20)   /* Initial Memory map for Linux */

#define CONFIG_SYS_SDRAM_BASE		PHYS_SDRAM_1
#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_SYS_SDRAM_BASE + 0x1000 - CONFIG_SYS_GBL_DATA_SIZE)
/***************************
 *  Console configuration
 */
#define CONFIG_SYS_NS16550
#define CONFIG_SYS_NS16550_SERIAL
#define CONFIG_SYS_NS16550_REG_SIZE   	-4
#define CONFIG_SYS_NS16550_CLK        	14745600
#define CONFIG_BAUDRATE        		115200
#define CONFIG_SYS_BAUDRATE_TABLE     	{ 9600, 19200, 38400, 57600, 115200 }
#define CONFIG_CONS_INDEX     		1
#define CONFIG_SYS_NS16550_COM1       	0xD4017000
#define CONFIG_DISABLE_CONSOLE		1	/* enable silent startup */

#define CONFIG_SYS_PROMPT         	"TTC_TD>> "
#define CONFIG_SYS_PROMPT_HUSH_PS2     	"> "
#define CONFIG_SYS_LONGHELP
#define CONFIG_SYS_HUSH_PARSER		1
#define CONFIG_CMD_RUN			1
#define CONFIG_AUTO_COMPLETE
#define CONFIG_CMDLINE_EDITING
#define CONFIG_SYS_64BIT_VSPRINTF

#define CONFIG_SYS_MAXARGS        	16
#define CONFIG_CMDLINE_TAG       	1
#define CONFIG_SETUP_MEMORY_TAGS 	1
#define CONFIG_MISC_INIT_R         	1

/*-----------------------------------------------------------------------
 * NAND and DFC configuration
 */
#ifdef CONFIG_NAND_PXA3XX
#define CONFIG_CMD_NAND                 1
#define CONFIG_SYS_NAND_MAX_CHIPS	1
#define CONFIG_SYS_MAX_NAND_DEVICE      2         /* Max number of NAND devices */
#define CONFIG_SYS_NAND_BASE            0xD4283000
#define CONFIG_SYS_NAND_QUIET_TEST
#define PXA3XX_NAND_NAKED_CMD_SUPPORT
#define CONFIG_PXA3XX_NAND_DEF_CLOCK	(156 * 1000000)
#define CONFIG_MTD_NAME			"pxa3xx_nand-0"
#define CONFIG_MTD_ID			"nand0"
#endif

/*-----------------------------------------------------------------------
 * ONENAND configuration
 */
#ifdef CONFIG_CMD_ONENAND
#define CONFIG_SYS_ONENAND_BASE         0x80000000
#define CONFIG_MTD_NAME			"onenand"
#define CONFIG_MTD_ID			"onenand0"
#endif

#if defined(CONFIG_NAND_PXA3XX) || defined(CONFIG_CMD_ONENAND)
#define CONFIG_MTD_DEVICE	1
#define CONFIG_CMD_MTDPARTS	1
#define CONFIG_PXA3XX_BBM
#define MTDPARTS_DEFAULT		"mtdparts=" CONFIG_MTD_NAME ":1m(bootloader)ro," \
	"256k(ramdisk)ro,256k(nvm),8m(cpbinary)ro,3m(kernel)ro,3m(maintenance)ro," \
	"5m(recovery),256k(misc),39m(cache),112m(system),64m(userdata),15616k(telephony),-(reserved)"
#define MTDPARTS_MAINT			"mtdparts=" CONFIG_MTD_NAME ":1m(bootloader)," \
	"256k(ramdisk),256k(nvm),8m(cpbinary),3m(kernel),3m(maintenance)," \
	"5m(recovery),256k(misc),39m(cache),112m(system),64m(userdata),15616k(telephony),-(reserved)"
#define MTDIDS_DEFAULT			CONFIG_MTD_ID "=" CONFIG_MTD_NAME
#else
#define MTDPARTS_DEFAULT
#define MTDIDS_DEFAULT
#endif

/*-----------------------------------------------------------------------
 * MMC configuration
 */
#define CONFIG_CMD_FAT          1
#define CONFIG_CMD_MMC          1
#define CONFIG_MMC              1
#define CONFIG_MMC_SDMA		1
#define CONFIG_GENERIC_MMC      1
#define CONFIG_PXA9XX_SDH	1
#define CONFIG_PXA9XX_FREQ_FS	0x80
#define CONFIG_DOS_PARTITION    1
#ifndef CONFIG_HAS_EMMC
#define CONFIG_SYS_MMC_NUM	1
#define CONFIG_SYS_MMC_BASE     {0xD4280000}
#else
#define CONFIG_SYS_MMC_NUM	2
#define CONFIG_SYS_MMC_BASE     {0xD4281000, 0xD4280000}
#endif

/*-----------------------------------------------------------------------
 * FASTBOOT configuration
 */
#define CONFIG_USB_GADGET_MMP	1
#define CONFIG_USB_ETHER	1
#define CONFIG_USB_GADGET_DUALSPEED	1
#ifndef CONFIG_USB_ETHER
#define CONFIG_URB_BUF_SIZE	256
#define CONFIG_USB_DEVICE	1
#else
#define CONFIG_CMD_NET		1
#define CONFIG_IPADDR      		192.168.1.101
#define CONFIG_SERVERIP    		192.168.1.100
#define CONFIG_ETHADDR			"0a:fa:63:8b:e8:0a"
#define CONFIG_USBNET_DEV_ADDR          "00:0a:fa:63:8b:e8"
#define CONFIG_USBNET_HOST_ADDR         "0a:fa:63:8b:e8:0a"
#define CONFIG_NET_MULTI	1
#define CONFIG_CMD_FASTBOOT	1
#endif
#define CONFIG_MMPUDC		1
#define CONFIG_URB_BUF_SIZE	256
#define CONFIG_USB_DEVICE	1
#define CONFIG_USB_REG_BASE	0xd4208000
#define CONFIG_USB_PHY_BASE	0xd4207000
#define CONFIG_USBD_VENDORID	0x18d1
#define CONFIG_USBD_PRODUCTID	0x4e11
#define CONFIG_USBD_MANUFACTURER "Marvell Inc."
#define CONFIG_USBD_PRODUCT_NAME "Android 2.1"
#define CONFIG_SERIAL_NUM	 "MRUUUVLs001"
#define CONFIG_USBD_CONFIGURATION_STR "fastboot"
#define CONFIG_FB_FOR_MMC	1
#define CONFIG_SYS_FASTBOOT_PARTNUM	6
#define CONFIG_SYS_FASTBOOT_PARTS	{"ramdisk", "kernel", "cache", "system", "userdata","telephony"}
#if (defined(CONFIG_NAND_PXA3XX) || defined(CONFIG_CMD_ONENAND)) && !defined(CONFIG_GENERIC)
#define CONFIG_SYS_FASTBOOT_USE_YAFFS	{0, 0, 1, 1, 1, 1}
#else
#define CONFIG_SYS_FASTBOOT_USE_YAFFS	{0, 0, 0, 0, 0, 0}
#endif
#define CONFIG_SYS_FASTBOOT_ONFLY_SZ	0x20000
#define USB_LOADADDR		0x100000

/*-----------------------------------------------------------------------
 * I2C configuration
 */
#define CONFIG_I2C_MMP		1
#define CONFIG_I2C_REG_BASE	0xd4011000
#define CONFIG_TWSI_CLOCK_BASE	0xd401502c
#endif

/* LCD configuration */
/***************************************/
#ifdef CONFIG_PXA910_FB
#define CONFIG_CMD_BMP                 1
#define CONFIG_VIDEO                   1
#define CONFIG_CFB_CONSOLE             1
#define VIDEO_KBD_INIT_FCT             -1
#define VIDEO_TSTC_FCT                 serial_tstc
#define VIDEO_GETC_FCT                 serial_getc
#define CONFIG_VIDEO_LOGO
#endif
/***************************************/

/***************************************/
/* LINUX BOOT and other ENV PARAMETERS */
/***************************************/
#define CONFIG_ZERO_BOOTDELAY_CHECK
#define CONFIG_SHOW_BOOT_PROGRESS
#define CONFIG_BOOTDELAY        	1
#ifdef CONFIG_NAND_PXA3XX
#define CONFIG_BOOTCOMMAND              "nboot kernel"
#define CONFIG_MAINTAIN_BOOT		"nboot maintenance"
#else
#define CONFIG_BOOTCOMMAND              "mmc read 0 $loadaddr 0x4c0000 0x1800;bootm $loadaddr"
#define CONFIG_MAINTAIN_BOOT		"mmc read 0 $loadaddr 0x640000 0x1800;bootm $loadaddr"
#endif

#define CONFIG_CMD_ENV			1
#define CONFIG_CMD_SAVEENV		1
#define CONFIG_ENV_OVERWRITE    	/* allow to change env parameters */
#ifdef CONFIG_NAND_PXA3XX
#define CONFIG_ENV_OFFSET        	0xc0000
#define CONFIG_ENV_IS_IN_NAND		1
#elif defined(CONFIG_CMD_ONENAND)
#define CONFIG_ENV_ADDR			0xc0000
#define CONFIG_ENV_IS_IN_ONENAND	1
#elif defined(CONFIG_HAS_EMMC)
#define CONFIG_ENV_IS_IN_MMC		1
#define CONFIG_ENV_OFFSET        	0xc0000
#define CONFIG_SYS_MMC_ENV_DEV		0
#else
#undef CONFIG_CMD_SAVEENV
#define CONFIG_ENV_IS_NOWHERE		1
#endif
#define CONFIG_SYS_NO_FLASH		1

#define CONFIG_EXTRA_ENV_SETTINGS						\
		"autostart=yes\0"						\
		"mtdids=" MTDIDS_DEFAULT "\0"					\
		"mtdparts=" MTDPARTS_DEFAULT "\0"				\
		"console=androidboot.console=ttyS0 console=ttyS0,115200"
/* __CONFIG_H */
