/*
 * (C) Copyright 2008
 * Marvell Semiconductors Ltd. Shanghai, China.
 *
 * Configuration for Tavor EVB board.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CONFIG_H
#define __CONFIG_H

/************/
/* VERSIONS */
/************/
#define CONFIG_IDENT_STRING		"[bootloader]v1.0.1"

/* version number passing when loading Kernel */
#define VER_NUM 			0x01010101	/* 1.1.1.18 */
/*
 * High Level Configuration Options
 * (easy to change)
 */
#define CONFIG_CPU_PXA910		1 /* This is an pxa910 core*/
#define CONFIG_CPU_PXA688		1 /* This is an pxa410 core*/
#define CONFIG_PXAXXX			1 /*  pxa family */
#define CONFIG_MMP
#define CONFIG_MMP2_G50			1
#define CONFIG_MFPR_MMIOBASE		(0xd401e000)
#define CONFIG_FFUART
#define CONFIG_MARVELL_MFP		1

/* DDR Type and Size on Brownstone Platform*/
#undef CONFIG_DDR_MICRON_256M
#undef CONFIG_DDR_EPD_512M
#undef CONFIG_DDR3_EPD_1G
#undef CONFIG_DDR3_EPD_512M
#define CONFIG_DDR3_EPD_1G

#define CONFIG_SYS_BOARD_NAME		"ARM1176JZF based"
#define CONFIG_SYS_VENDOR_NAME     	"MARVELL"

#define CONFIG_SYS_ARM_WITHOUT_RELOC	1

#ifdef CONFIG_PJ4_NON_SECURE_MODE
#define CONFIG_SYS_UBOOT_BASE		0x1100000	
#define CONFIG_SYS_MEMTEST_START      	0x00300000
#define CONFIG_SYS_MEMTEST_END        	0x01100000
#else
#define CONFIG_SYS_UBOOT_BASE		0xf00000	
#define CONFIG_SYS_MEMTEST_START      	0x00100000
#define CONFIG_SYS_MEMTEST_END        	0x00f00000
#endif

#define CONFIG_SYS_HZ   		(6500000)      /* KV - Timer 0 is clocked at 3.25 MHz */
#define CONFIG_SYS_TIMERBASE 		0xD4014000 
#define CONFIG_SYS_CPUSPEED		0x161		/* set core clock to 400/200/100 MHz */
#define CONFIG_DISPLAY_BOARDINFO

#define CONFIG_MARVELL_TAG		1
#define CONFIG_CMDLINE_TAG         	1   /* enable passing of ATAGs  */
#define CONFIG_SETUP_MEMORY_TAGS   	1
#define CONFIG_MISC_INIT_R         	1   /* call misc_init_r during start up */

/* Enable Memory Test for Brownstone Platform */
#define CONFIG_CMD_MEMORY			/* enable memory test command for ddr test */
#define CONFIG_SYS_ALT_MEMTEST		/* enable complete memory test loop */

/*
 * Size of malloc() pool
 */
#define CONFIG_SYS_MALLOC_LEN      	(CONFIG_SYS_ENV_SIZE + 512*1024)
#define CONFIG_SYS_GBL_DATA_SIZE   	128   /* size in bytes reserved for initial data */


/*
 *  Configuration
 */
#define CONFIG_CONS_INDEX     		1
#undef  CONFIG_SERIAL_SOFTWARE_FIFO
#define CONFIG_SYS_NS16550
#define CONFIG_SYS_NS16550_SERIAL
#define CONFIG_SYS_NS16550_REG_SIZE   	-4
#define CONFIG_SYS_NS16550_CLK        	26000000
#define CONFIG_BAUDRATE        		115200
#define CONFIG_SYS_BAUDRATE_TABLE     	{ 9600, 19200, 38400, 57600, 115200 }
#define CONFIG_SYS_NS16550_COM1       	0xD4018000 /* configure for PXA910*/

#define CONFIG_SHOW_BOOT_PROGRESS

#define CONFIG_CMD_PING
#define CONFIG_CMD_NET
#define CONFIG_NET_MULTI
#define CONFIG_LOOP_WRITE_MTD
#define MV_ETH_DEVS 			1

#define CONFIG_IPADDR      		50.1.1.10

#define CONFIG_SERVERIP    		50.1.1.1

/* enable passing of ATAGs  */
#define CONFIG_CMDLINE_TAG       	1
#define CONFIG_SETUP_MEMORY_TAGS 	1
#define CONFIG_SYS_TCLK         	0 /* not in use */
#define CONFIG_SYS_BUS_CLK         	0 /* not in use */
//#define CONFIG_ENV_SIZE				0x4000
//#define CONFIG_ENV_OFFSET			0x40000
/***************************************/
/* LINUX BOOT and other ENV PARAMETERS */
/***************************************/
#define CONFIG_SYS_BOOTARGS_END     	":::MMP2:eth0:none"
#define CONFIG_SYS_BOOTARGS_ROOT    	"root=/dev/nfs rw init=/linuxrc"
#define CONFIG_ZERO_BOOTDELAY_CHECK
#ifdef CONFIG_PJ4_NON_SECURE_MODE
#define CONFIG_SYS_LOAD_ADDR        	0x01100000   /* default load address   */
#define CONFIG_SYS_DEF_LOAD_ADDR    	"0x01100000"
#else
#define CONFIG_SYS_LOAD_ADDR        	0x01100000   /* default load address   */
#define CONFIG_SYS_DEF_LOAD_ADDR    	"0x01100000"
#endif
#define CONFIG_SYS_IMG_NAME		"zImage"
#define CONFIG_SYS_INITRD_NAME      	"ramdisk.image.gz"
#ifdef CONFIG_PJ4_NON_SECURE_MODE
#define CONFIG_SYS_INITRD_LOAD_ADDR 	"1500000"
#else
#define CONFIG_SYS_INITRD_LOAD_ADDR 	"1500000"
#endif
#define CONFIG_SYS_INITRD_SIZE      	"400000"
#undef  CONFIG_BOOTARGS


/*
 * Preboot
*/
#define	CONFIG_PREBOOT				"preboot"
#define CONFIG_BOOTDELAY        	1
#define CONFIG_AUTOBOOT_KEYED	    1
#define CONFIG_AUTOBOOT_PROMPT 	    \
	"Autobooting in %d seconds\n", bootdelay
#define CONFIG_AUTOBOOT_DELAY_STR "\x1b\x1b"
#define CONFIG_AUTOBOOT_STOP_STR "questers" /* password*/

#if (CONFIG_BOOTDELAY >= 0)

/* boot arguments" */
#ifdef CONFIG_PJ4_NON_SECURE_MODE
#define CONFIG_ONENANDBOOT	     	"onenand read 0x1100000 0x980000 0x400000"
#define CONFIG_NANDBOOT 	     	"nand read 0x1100000 0x980000 0x400000"
#define CONFIG_BOOTCOMMAND      	"setenv autoboot boot; bootz 0x1100000; setenv autoboot none"
#else
#define CONFIG_ONENANDBOOT	     	"onenand read 0x1100000 0x980000 0x400000"
#define CONFIG_NANDBOOT 	     	"nand read 0x1100000 0x980000 0x400000"
#define CONFIG_BOOTCOMMAND      	"setenv autoboot boot; bootm; setenv autoboot none"
#endif
#define CONFIG_ROOTPATH   		/tftpboot/rootfs_arm

#endif /* #if (CONFIG_BOOTDELAY >= 0) */

#define CONFIG_SYS_BARGSIZE   		CONFIG_SYS_CBSIZE   /* Boot Argument Buffer Size   */

#define CONFIG_BOOT_PHASE			"bootseq"
#define CONFIG_BOOT_PRIMARY			"kernel"
#define CONFIG_BOOT_ALTERNATE		"recovery"

/*
 * For booting Linux, the board info and command line data
 * have to be in the first 8 MB of memory, since this is
 * the maximum mapped by the Linux kernel during initialization.
 */
#define CONFIG_SYS_BOOTMAPSZ      	(8<<20)   /* Initial Memory map for Linux */

/*
 * Command Interpreter
 */
#define CONFIG_CMD_RUN					1
#define CONFIG_AUTO_COMPLETE
#define CONFIG_SYS_LONGHELP        		/* undef to save memory     */
#define CONFIG_SYS_PROMPT         		"MMP2$ "   /* Monitor Command Prompt   */
#define CONFIG_SYS_PROMPT_HUSH_PS2     	"> "
#define CONFIG_SYS_CBSIZE				1024 /* Console I/O Buffer Size  */
#define CONFIG_SYS_PBSIZE				(CONFIG_SYS_CBSIZE+sizeof(CONFIG_SYS_PROMPT)+16) /* Print Buffer Size */
#define CONFIG_SYS_MAXARGS				16 /* max number of command args   */ 
#define CONFIG_SYS_BARGSIZE				CONFIG_SYS_CBSIZE /* Boot Argument Buffer Size    */
#undef  CONFIG_SYS_CLKS_IN_HZ         	/* everything, incl board info, in Hz */
#define CONFIG_ENV_OVERWRITE    		/* allow to change env parameters */
#undef  CONFIG_INIT_CRITICAL
#define CONFIG_CMDLINE_EDITING
#define CONFIG_COMMAND_HISTORY
#define CONFIG_COMMAND_EDIT
#define CONFIG_SYS_64BIT_VSPRINTF


/*-----------------------------------------------------------------------
 * Stack sizes
 *
 * The stack sizes are set up in start.S using the settings below
 */
#define CONFIG_STACKSIZE       		(128*1024)   /* regular stack */
#ifdef  CONFIG_USE_IRQ
#define CONFIG_STACKSIZE_IRQ   		(4*1024)   /* IRQ stack */
#define CONFIG_STACKSIZE_FIQ   		(4*1024)   /* FIQ stack */
#endif

/*-----------------------------------------------------------------------
 * Physical Memory Map
 */
#define CONFIG_NR_DRAM_BANKS    	1   /* we have 1 bank of DRAM */
#ifdef CONFIG_PJ4_NON_SECURE_MODE
#define PHYS_SDRAM_1            	0x00200000   /* SDRAM Bank #1 */
#define PHYS_SDRAM_1_SIZE       	0x3fe00000   /* 1022 MB */
#define PHYS_SDRAM_SIZE_DEC     	1022
#else
#define PHYS_SDRAM_1            	0x00000000   /* SDRAM Bank #1 */
#define PHYS_SDRAM_1_SIZE       	0x40000000   /* 1024 MB */
#define PHYS_SDRAM_SIZE_DEC     	1024
#endif

/*
  *System Environment
  *Default no environment support,in derived config file,CONFIG_ENV_IS_NOWHERE will be undefined
  * and new environment location will be defined (e.g. CONFIG_ENV_IS_IN_MMC in mmc flash);
*/
#define CONFIG_SYS_ENV_SIZE			0x10000   /* Total Size of Environment Sector */
#define	CONFIG_ENV_IS_NOWHERE		1


#define CONFIG_SYS_NO_FLASH			1
#define CONFIG_BBM					1
#define CONFIG_SYS_SDRAM_BASE		PHYS_SDRAM_1
#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_SYS_SDRAM_BASE + 0x1000 - CONFIG_SYS_GBL_DATA_SIZE)

/*-----------------------------------------------------------------------
 * NAND and DFC configuration
 */
#define CONFIG_NAND_PXA3XX
#define CONFIG_CMD_NAND 		1
#define CONFIG_SYS_MAX_NAND_DEVICE	1         /* Max number of NAND devices */
#define CONFIG_SYS_NAND_BASE		0xD4283000

/*-----------------------------------------------------------------------
 * ONENAND configuration
 */
#define CONFIG_CMD_ONENAND 		1
#define CONFIG_SYS_ONENAND_BASE 	0x80000000  /* configure for PXA910*/

/*-----------------------------------------------------------------------
 * FREQUENCE configuration
 */
#define CONFIG_CMD_FREQUENCE		1

#undef TURN_ON_ETHERNET
//#define TURN_ON_ETHERNET
#ifdef TURN_ON_ETHERNET
#define CONFIG_USB_ETH			1
#define CONFIG_U2O_REG_BASE		0xd4208000
#define CONFIG_U2O_PHY_BASE		0xd4207000
#define CONFIG_DRIVER_SMC91111 		1
#define CONFIG_SMC91111_BASE   		0x90000300 /* PXA910*/
#define CONFIG_SMC_USE_16_BIT
#undef CONFIG_SMC_USE_IOFUNCS          /* just for use with the kernel */
#endif
#define CONFIG_NET_RETRY_COUNT 		10000
#undef CONFIG_DRIVER_SMC91111
/*-----------------------------------------------------------------------
 * FASTBOOT configuration
 */
#define CONFIG_USB_GADGET_MMP	1
#define CONFIG_USB_ETHER	1
#define CONFIG_USB_GADGET_DUALSPEED	1
#ifndef CONFIG_USB_ETHER
#define CONFIG_URB_BUF_SIZE	256
#define CONFIG_USB_DEVICE	1
#else
//using customer specified mac
#define CONFIG_ETHADDR					00:17:61:00:00:05
#define CONFIG_USBNET_DEV_ADDR          "00:0a:fa:63:8b:e8"
#define CONFIG_USBNET_HOST_ADDR         "0a:fa:63:8b:e8:0a"
#define CONFIG_CMD_FASTBOOT	1
#endif
#define CONFIG_MMPUDC					1
#define CONFIG_URB_BUF_SIZE				256
#define CONFIG_USB_DEVICE				1
#define CONFIG_USB_REG_BASE				0xd4208000
#define CONFIG_USB_PHY_BASE				0xd4207000
#define CONFIG_USBD_VENDORID			0x18d1
#define CONFIG_USBD_PRODUCTID			0x4e11
#define CONFIG_USBD_MANUFACTURER 		"Questers Tech,Inc."
#define CONFIG_USBD_PRODUCT_NAME		"Android Device"
#define CONFIG_SERIAL_NUM				"QSTDEV88888888"
#define CONFIG_USBD_CONFIGURATION_STR	"fastboot"
#define CONFIG_FB_FOR_MMC				1
#define USB_LOADADDR					0x1100000


/*-----------------------------------------------------------------------
 * cache set/way configuration
 */
#undef CONFIG_FLUSH_ALL_REPLACEMENT

/*
 * I2C configuration
 */
#define CONFIG_HARD_I2C						//i2c hardware 
#define CONFIG_I2C_MV						//marvell i2c driver
#define CONFIG_I2C_MULTI_BUS 				//multi i2c bus support

/*-----------------------------------------------------------------------
 * FREQ configuration
 */
#define CONFIG_FREQ_CHG		1
#define CONFIG_SYS_I2C_SPEED 		100000	//i2c default speed 100kHz
#define CONFIG_MV_I2C_NUM 6
#define CONFIG_MV_I2C_REG {0xd4011000,0xd4031000,0xd4032000,0xd4033000,0xd4033800,0xd4034000}
#define CONFIG_MV_I2C_CLOCK_REG {0xd4015004,0xd4015008,0xd401500c,0xd4015010,0xd401507c,0xd40150080}
#define CONFIG_CMD_I2C

#define CONFIG_MARVELL_I2C_API

/*Console*/
#define CONFIG_SYS_CONSOLE_IS_IN_ENV
#define CONFIG_SYS_CONSOLE_OVERWRITE_ROUTINE

/*Auto update*/
#define CONFIG_AUTOUPDATER
#define CONFIG_AUTOUPDATER_DIRECTORY           "/autoupdate/"

/* LCD configuration */
/***************************************/
#define CONFIG_PXA168_FB
#define CONFIG_CMD_BMP                 1
#define CONFIG_VIDEO                   1
#define CONFIG_CFB_CONSOLE             1
#define CONFIG_VGA_AS_SINGLE_DEVICE
#ifndef CONFIG_VGA_AS_SINGLE_DEVICE
#define VIDEO_KBD_INIT_FCT             -1
#define VIDEO_TSTC_FCT                 serial_tstc
#define VIDEO_GETC_FCT                 serial_getc
#endif
/*logo only used while fasboot mode enabled*/
#define CONFIG_VIDEO_LOGO
#define CONFIG_LCD_NO_INIT_PLOT
#define CONFIG_VIDEO_BMP_LOGO
#define CONFIG_VIDEO_BMP_GZIP
#define CONFIG_VIDEO_BMP_RLE8
#define VIDEO_FB_16BPP_WORD_SWAP

//
//
//define framebuffer base ,size
//Here is just the default parameters,boot loader will calculate the actual fb base from CONFIG_BOOT_MEMORYSIZE
//
//
#ifdef CONFIG_DDR3_EPD_1G
#define DEFAULT_FB_BASE 0x3f000000
#define DEFAULT_FB_SIZE 0x1000000
//1280*720*4  /* 1280*720*4 */
//#define CONFIG_BOOT_MEMORYSIZE 1024
#else
#define DEFAULT_FB_BASE 0x1f000000
#define DEFAULT_FB_SIZE 0x1000000
//1280*720*4  /*  1280*720*4 */
//#define CONFIG_BOOT_MEMORYSIZE 512
#endif


/***************************************/
//battery monitor
#define CONFIG_BATTERY_MONITOR
#define CONFIG_ZLIB
#define CONFIG_GZIP
#define CONFIG_GZIP_BMP_ADDR CONFIG_SYS_LOAD_ADDR  /* for decompressed img */
#define CONFIG_SYS_VIDEO_LOGO_MAX_SIZE	(4<<20)

#ifdef CONFIG_DDR3_EPD_1G
#define CONFIG_BOOTARGS "root=/dev/mmcblk0p1 mem=1008M android vmalloc=0x18000000"
#else
#define CONFIG_BOOTARGS "root=/dev/mmcblk0p1 mem=496M android"
#endif
#define CONFIG_DEFAULT_CONSOLE "null"
//console=ttyS2,115200
#define CONFIG_ANDROID_CONSOLE "null"
//"ttyS2"
#define CONFIG_ANDROID_RESERVED_DRAM "128M"
#define CONFIG_ANDROID_AUDIOBUF "1M"

#endif
/* __CONFIG_H */
