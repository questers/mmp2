/*
 * (C) Copyright 2008
 * Marvell Semiconductors Ltd. Shanghai, China.
 *
 * Configuration for Tavor EVB board.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CONFIG_H
#define __CONFIG_H

/************/
/* VERSIONS */
/************/
#define CONFIG_IDENT_STRING		"\nMarvell version: 1.1.1.1 MMP2"

/* version number passing when loading Kernel */
#define VER_NUM 			0x01010101	/* 1.1.1.18 */
/*
 * High Level Configuration Options
 * (easy to change)
 */
#define CONFIG_CPU_PXA910		1 /* This is an pxa910 core*/
#define CONFIG_CPU_PXA688		1 /* This is an pxa410 core*/
#define CONFIG_PXAXXX			1 /*  pxa family */
#define CONFIG_MMP
#define CONFIG_MMP2_BROWNSTONE		1
#define CONFIG_MFPR_MMIOBASE		(0xd401e000)
#define CONFIG_FFUART
#define CONFIG_MARVELL_MFP		1

/* DDR Type and Size on Brownstone Platform*/
#undef CONFIG_DDR_MICRON_256M
#undef CONFIG_DDR_EPD_512M
#define CONFIG_DDR3_EPD_1G

#define CONFIG_SYS_BOARD_NAME		"ARM1176JZF based"
#define CONFIG_SYS_VENDOR_NAME     	"MARVELL"

#define CONFIG_SYS_ARM_WITHOUT_RELOC	1

#ifdef CONFIG_PJ4_NON_SECURE_MODE
#define CONFIG_SYS_UBOOT_BASE		0x1100000	
#define CONFIG_SYS_MEMTEST_START      	0x00300000
#define CONFIG_SYS_MEMTEST_END        	0x01100000
#else
#define CONFIG_SYS_UBOOT_BASE		0xf00000	
#define CONFIG_SYS_MEMTEST_START      	0x00100000
#define CONFIG_SYS_MEMTEST_END        	0x00f00000
#endif

#define CONFIG_SYS_HZ   		(6500000)      /* KV - Timer 0 is clocked at 3.25 MHz */
#define CONFIG_SYS_TIMERBASE 		0xD4014000 
#define CONFIG_SYS_CPUSPEED		0x161		/* set core clock to 400/200/100 MHz */
#define CONFIG_DISPLAY_BOARDINFO

#define CONFIG_MARVELL_TAG		1
#define CONFIG_CMDLINE_TAG         	1   /* enable passing of ATAGs  */
#define CONFIG_SETUP_MEMORY_TAGS   	1
#define CONFIG_MISC_INIT_R         	1   /* call misc_init_r during start up */

/* Enable Memory Test for Brownstone Platform */
#define CONFIG_CMD_MEMORY		/* enable memory test command for ddr test */
#define CONFIG_SYS_ALT_MEMTEST		/* enable complete memory test loop */

/*
 * Size of malloc() pool
 */
#define CONFIG_SYS_MALLOC_LEN      	(CONFIG_SYS_ENV_SIZE + 512*1024)
#define CONFIG_SYS_GBL_DATA_SIZE   	128   /* size in bytes reserved for initial data */

/*
 *  Configuration
 */
#define CONFIG_AUTO_COMPLETE
#define CONFIG_CONS_INDEX     		1
#undef  CONFIG_SERIAL_SOFTWARE_FIFO
#define CONFIG_SYS_NS16550
#define CONFIG_SYS_NS16550_SERIAL
#define CONFIG_SYS_NS16550_REG_SIZE   	-4
#define CONFIG_SYS_NS16550_CLK        	26000000
#define CONFIG_BAUDRATE        		38400
#define CONFIG_SYS_BAUDRATE_TABLE     	{ 9600, 19200, 38400, 57600, 115200 }
#define CONFIG_SYS_NS16550_COM1       	0xD4018000 /* configure for PXA910*/

#define CONFIG_SHOW_BOOT_PROGRESS

#define CONFIG_CMD_PING
#define CONFIG_CMD_NET
#define CONFIG_NET_MULTI
#define CONFIG_LOOP_WRITE_MTD
#define MV_ETH_DEVS 			1

#define CONFIG_IPADDR      		192.168.1.101

#define CONFIG_SERVERIP    		192.168.1.100

/* enable passing of ATAGs  */
#define CONFIG_CMDLINE_TAG       	1
#define CONFIG_SETUP_MEMORY_TAGS 	1
#define CONFIG_SYS_TCLK         	0 /* not in use */
#define CONFIG_SYS_BUS_CLK         	0 /* not in use */
//#define CONFIG_ENV_SIZE				0x4000
//#define CONFIG_ENV_OFFSET			0x40000
/***************************************/
/* LINUX BOOT and other ENV PARAMETERS */
/***************************************/
#define CONFIG_SYS_BOOTARGS_END     	":::MMP2:eth0:none"
#define CONFIG_SYS_BOOTARGS_ROOT    	"root=/dev/nfs rw init=/linuxrc"
#define CONFIG_ZERO_BOOTDELAY_CHECK
#ifdef CONFIG_PJ4_NON_SECURE_MODE
#define CONFIG_SYS_LOAD_ADDR        	0x01100000   /* default load address   */
#define CONFIG_SYS_DEF_LOAD_ADDR    	"0x01100000"
#else
#define CONFIG_SYS_LOAD_ADDR        	0x01100000   /* default load address   */
#define CONFIG_SYS_DEF_LOAD_ADDR    	"0x01100000"
#endif
#define CONFIG_SYS_IMG_NAME		"zImage"
#define CONFIG_SYS_INITRD_NAME      	"ramdisk.image.gz"
#ifdef CONFIG_PJ4_NON_SECURE_MODE
#define CONFIG_SYS_INITRD_LOAD_ADDR 	"1500000"
#else
#define CONFIG_SYS_INITRD_LOAD_ADDR 	"1500000"
#endif
#define CONFIG_SYS_INITRD_SIZE      	"400000"
#undef  CONFIG_BOOTARGS

#define CONFIG_BOOTDELAY        	3

#if (CONFIG_BOOTDELAY >= 0)

/* boot arguments" */
#ifdef CONFIG_PJ4_NON_SECURE_MODE
#define CONFIG_ONENANDBOOT	     	"onenand read 0x1100000 0x980000 0x400000"
#define CONFIG_NANDBOOT 	     	"nand read 0x1100000 0x980000 0x400000"
#define CONFIG_BOOTCOMMAND      	"setenv autoboot boot; bootz 0x1100000; setenv autoboot none"
#else
#define CONFIG_ONENANDBOOT	     	"onenand read 0x1100000 0x980000 0x400000"
#define CONFIG_NANDBOOT 	     	"nand read 0x1100000 0x980000 0x400000"
#define CONFIG_BOOTCOMMAND      	"setenv autoboot boot; bootz 0x1100000; setenv autoboot none"
#endif
#define CONFIG_ROOTPATH   		/tftpboot/rootfs_arm

#endif /* #if (CONFIG_BOOTDELAY >= 0) */

#define CONFIG_SYS_BARGSIZE   		CONFIG_SYS_CBSIZE   /* Boot Argument Buffer Size   */

/*
 * For booting Linux, the board info and command line data
 * have to be in the first 8 MB of memory, since this is
 * the maximum mapped by the Linux kernel during initialization.
 */
#define CONFIG_SYS_BOOTMAPSZ      	(8<<20)   /* Initial Memory map for Linux */

/*
 * Miscellaneous configurable options
 */
#define CONFIG_SYS_LONGHELP        	/* undef to save memory     */
#define CONFIG_SYS_PROMPT         	"MMP2>> "   /* Monitor Command Prompt   */
#define CONFIG_SYS_PROMPT_HUSH_PS2     	"> "
/* Console I/O Buffer Size  */
#define CONFIG_SYS_CBSIZE         	1024
/* Print Buffer Size */
#define CONFIG_SYS_PBSIZE         	(CONFIG_SYS_CBSIZE+sizeof(CONFIG_SYS_PROMPT)+16)
/* max number of command args   */
#define CONFIG_SYS_MAXARGS        	16
/* Boot Argument Buffer Size    */
#define CONFIG_SYS_BARGSIZE       	CONFIG_SYS_CBSIZE
#undef  CONFIG_SYS_CLKS_IN_HZ         	/* everything, incl board info, in Hz */
#define CONFIG_ENV_OVERWRITE    	/* allow to change env parameters */
#undef  CONFIG_INIT_CRITICAL
#define CONFIG_CMDLINE_EDITING
#define CONFIG_COMMAND_HISTORY
#define CONFIG_COMMAND_EDIT
#define CONFIG_SYS_64BIT_VSPRINTF


/*-----------------------------------------------------------------------
 * Stack sizes
 *
 * The stack sizes are set up in start.S using the settings below
 */
#define CONFIG_STACKSIZE       		(128*1024)   /* regular stack */
#ifdef  CONFIG_USE_IRQ
#define CONFIG_STACKSIZE_IRQ   		(4*1024)   /* IRQ stack */
#define CONFIG_STACKSIZE_FIQ   		(4*1024)   /* FIQ stack */
#endif

/*-----------------------------------------------------------------------
 * Physical Memory Map
 */
#define CONFIG_NR_DRAM_BANKS    	1   /* we have 1 bank of DRAM */
#ifdef CONFIG_PJ4_NON_SECURE_MODE
#define PHYS_SDRAM_1            	0x00200000   /* SDRAM Bank #1 */
#define PHYS_SDRAM_1_SIZE       	0x3fe00000   /* 1022 MB */
#define PHYS_SDRAM_SIZE_DEC     	1022
#else
#define PHYS_SDRAM_1            	0x00000000   /* SDRAM Bank #1 */
#define PHYS_SDRAM_1_SIZE       	0x40000000   /* 1024 MB */
#define PHYS_SDRAM_SIZE_DEC     	1024
#endif
#define CONFIG_SYS_ENV_SIZE            	0x10000   /* Total Size of Environment Sector */
#define	CONFIG_ENV_IS_NOWHERE		1
#define CONFIG_SYS_NO_FLASH		1
#define CONFIG_BBM			1
#define CONFIG_SYS_SDRAM_BASE		PHYS_SDRAM_1
#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_SYS_SDRAM_BASE + 0x1000 - CONFIG_SYS_GBL_DATA_SIZE)

/*-----------------------------------------------------------------------
 * NAND and DFC configuration
 */
#define CONFIG_NAND_PXA3XX
#define CONFIG_CMD_NAND 		1
#define CONFIG_SYS_MAX_NAND_DEVICE	1         /* Max number of NAND devices */
#define CONFIG_SYS_NAND_BASE		0xD4283000

/*-----------------------------------------------------------------------
 * ONENAND configuration
 */
#define CONFIG_CMD_ONENAND 		1
#define CONFIG_SYS_ONENAND_BASE 	0x80000000  /* configure for PXA910*/

/*-----------------------------------------------------------------------
 * FREQUENCE configuration
 */
#define CONFIG_CMD_FREQUENCE		1

#undef TURN_ON_ETHERNET
//#define TURN_ON_ETHERNET
#ifdef TURN_ON_ETHERNET
#define CONFIG_USB_ETH			1
#define CONFIG_U2O_REG_BASE		0xd4208000
#define CONFIG_U2O_PHY_BASE		0xd4207000
#define CONFIG_DRIVER_SMC91111 		1
#define CONFIG_SMC91111_BASE   		0x90000300 /* PXA910*/
#define CONFIG_SMC_USE_16_BIT
#undef CONFIG_SMC_USE_IOFUNCS          /* just for use with the kernel */
#endif
#define CONFIG_NET_RETRY_COUNT 		10000
#undef CONFIG_DRIVER_SMC91111
/*-----------------------------------------------------------------------
 * FASTBOOT configuration
 */
#define CONFIG_USB_GADGET_MMP	1
#define CONFIG_USB_ETHER	1
#define CONFIG_USB_GADGET_DUALSPEED	1
#ifndef CONFIG_USB_ETHER
#define CONFIG_URB_BUF_SIZE	256
#define CONFIG_USB_DEVICE	1
#else
#define CONFIG_ETHADDR			"0a:fa:63:8b:e8:0a"
#define CONFIG_USBNET_DEV_ADDR          "00:0a:fa:63:8b:e8"
#define CONFIG_USBNET_HOST_ADDR         "0a:fa:63:8b:e8:0a"
#define CONFIG_CMD_FASTBOOT	1
#endif
#define CONFIG_MMPUDC		1
#define CONFIG_URB_BUF_SIZE	256
#define CONFIG_USB_DEVICE	1
#define CONFIG_USB_REG_BASE	0xd4208000
#define CONFIG_USB_PHY_BASE	0xd4207000
#define CONFIG_USBD_VENDORID	0x18d1
#define CONFIG_USBD_PRODUCTID	0x4e11
#define CONFIG_USBD_MANUFACTURER "Marvell Inc."
#define CONFIG_USBD_PRODUCT_NAME "Android 2.1"
#define CONFIG_SERIAL_NUM	 "MRVL_MMP2"
#define CONFIG_USBD_CONFIGURATION_STR "fastboot"
#define CONFIG_FB_FOR_MMC	1
#define CONFIG_SYS_FASTBOOT_PARTNUM	9
#define CONFIG_SYS_FASTBOOT_PARTS	{"ramdisk", "system", "userdata", "storage", "recovery", "misc", "cache", "kernel", "reckernel"}
#define CONFIG_SYS_FASTBOOT_ONFLY_SZ	0x20000
#define USB_LOADADDR		0x1100000


/*-----------------------------------------------------------------------
 * cache set/way configuration
 */
#undef CONFIG_FLUSH_ALL_REPLACEMENT

/*-----------------------------------------------------------------------
 * I2C configuration
 */
#define CONFIG_I2C_MMP2          1
#define CONFIG_I2C_REG_BASE     0xd4011000
#define CONFIG_TWSI_CLOCK_BASE  0xd401502c

/*-----------------------------------------------------------------------
 * FREQ configuration
 */
#define CONFIG_FREQ_CHG		1

/* LCD configuration */
/***************************************/
#define CONFIG_SD_UPGRADE
#define UPGRADE_FILE_NAME           "brownstone.txt"
#define CONFIG_PXA168_FB
#ifdef CONFIG_PXA168_FB
#define FB_XRES		1280
#define FB_YRES		720
#define CONFIG_CMD_BMP                 1
#define CONFIG_VIDEO                   1
#define CONFIG_CFB_CONSOLE             1
#define VIDEO_KBD_INIT_FCT             -1
#define VIDEO_TSTC_FCT                 serial_tstc
#define VIDEO_GETC_FCT                 serial_getc
#define CONFIG_VIDEO_LOGO
#endif

#endif
/* __CONFIG_H */
