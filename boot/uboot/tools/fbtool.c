#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int i, ret;
	FILE *fd, *image;
	char prefix[200], filename[100], partname[100], imagename[100], tmp, cmdline[200];
	if (argc == 1)
		sprintf(prefix, "./");
	else if (argc == 2)
		sprintf(prefix, "%s", argv[1]);
	else {
		printf("Usage:: fbtool [Image directory name]\n");
		return 1;
	}
	printf("Image directory set as::%s\n", prefix);
	i = strlen(prefix) + 1;
#ifdef WIN32
	prefix [i - 1] = '\\';
#else
	prefix [i - 1] = '/';
#endif

	prefix[i] = '\0';
	sprintf(filename, "%sfb.conf", prefix);
	printf("fiile name %s\n", filename);
	fd = fopen(filename, "r");
	if (!fd) {
		printf("There is no fb.conf under the directory of \"%s\"\n", prefix);
		return 1;
	}
	printf("The conf layout is:\n");

	/* Check the conf file whether it is correct formatted */
	while (!feof(fd)) {
		if (!fscanf(fd, "%s\t%s\n", partname, imagename)) {
			printf("The fb.conf file syntax error\n");
			return 1;
		}
#ifdef WIN32
		sprintf(cmdline, "fastboot oem onfly %s 2>&1 | find \"OKAY\"", partname);
#else
		sprintf(cmdline, "fastboot oem onfly %s 2>&1 | grep OKAY", partname);
#endif
		printf("cmdline is %s\n", cmdline);
		ret = system(cmdline);
		if (ret) {
			printf("The part \"%s\" is not existed on board\n", partname);
			return 1;
		}

		prefix[i] = '\0';
		strcat(prefix, imagename);
		image = fopen(prefix, "r");
		if (!image) {
			prefix[i] = '\0';
			printf("\nImage \"%s\" don't contain in the directory of \"%s\"\n", imagename, prefix);
			return 1;
		}
		printf("%s\t\t%s\n", partname, imagename);
		fclose(image);
	}
	fclose(fd);
	system("fastboot oem onfly");
	printf("\n\nAre you sure you want to do the fastboot update?[y/n]");
	tmp = getchar();
	if (tmp == 'n' || tmp == 'N')
		return 0;
	fd = fopen(filename, "r");
	while (!feof(fd)) {
		fscanf(fd, "%s\t%s\n", partname, imagename);
#ifdef WIN32
		sprintf(cmdline, "fastboot erase %s 2>&1 | find \"OKAY\"", partname);
#else
		sprintf(cmdline, "fastboot erase %s 2>&1 | grep OKAY", partname);
#endif
		printf("ERASEING::%s\n",cmdline);
		ret = system(cmdline);
		if (ret) {
			printf("Erasing fail at part %s\n", partname);
			return 1;
		}
		prefix[i] = '\0';
		strcat(prefix, imagename);
		sprintf(cmdline, "fastboot oem onfly %s", partname);
		printf("CHANGE PART::%s\n",cmdline);
		system(cmdline);
#ifdef WIN32
		sprintf(cmdline, "fastboot flash %s %s 2>&1 | find \"OKAY\"", partname, prefix);
#else
		sprintf(cmdline, "fastboot flash %s %s 2>&1 | grep OKAY", partname, prefix);
#endif
		printf("FLASHING::%s\n",cmdline);
		ret = system(cmdline);
		if (ret) {
			printf("Flashing fail at part %s\n", partname);
			return 1;
		}
	}
	fclose(fd);
	sprintf(cmdline, "fastboot reboot");
	system(cmdline);

	return 0;
}
