/*
 *  U-Boot command for frequency change support
 *
 *  Copyright (C) 2008, 2009 Marvell International Ltd.
 *  All Rights Reserved
 *  Ning Jiang <ning.jiang@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <common.h>
#include <command.h>
#include <asm/io.h>

#define	PMUA_BASE		0xD4282800
#define	PMUA_DM_CC_MOH		(PMUA_BASE+0x000C)
#define	PMUA_CC_MOH		(PMUA_BASE+0x0004)
#define	PMUA_DM_CC_SEA		(PMUA_BASE+0x0008)
#define	PMUA_CC_SEA		(PMUA_BASE+0x0000)
#define	PMUA_PLL_SEL_STATUS	(PMUA_BASE+0x00C4)
#define	PMUA_MOH_IMR		(PMUA_BASE+0x0098)
#define	PMUA_MOH_ISR		(PMUA_BASE+0x00A0)
#define	PMUM_BASE		0xD4050000
#define	PMUM_PLL2CR		(PMUM_BASE+0x0034)
#define	PMUM_FCCR		(PMUM_BASE+0x0008)

void freq_init_sram(u32 sram_code_addr);
void freq_chg_seq(u32 sram_code_addr, u32 sram_stack_addr, u32 op);

u32 get_volt(void);
int set_volt(u32 vol);

typedef union {
	struct {
		unsigned int pll1fbd:		9;
		unsigned int pll1refd:		5;
		unsigned int pll1cen:		1;
		unsigned int mfc:		1;
		unsigned int reserved0:		1;
		unsigned int gcaclksel:		2;
		unsigned int axiclksel0:	1;
		unsigned int reserved1:		3;
		unsigned int ddrclksel:		2;
		unsigned int axiclksel1:	1;
		unsigned int seaclksel:		3;
		unsigned int mohclksel:		3;
	} b;
	unsigned int v;
} pmum_fccr;

typedef union {
	struct {
		unsigned int reserved0:		6;
		unsigned int reserved1:		2;
		unsigned int en:		1;
		unsigned int ctrl:		1;
		unsigned int pll2fbd:		9;
		unsigned int pll2refd:		5;
		unsigned int reserved2:		8;
	} b;
	unsigned int v;
} pmum_pll2cr;

typedef union {
	struct {
		unsigned int cpclksel:		2;
		unsigned int apclksel:		2;
		unsigned int ddrclksel:		2;
		unsigned int axiclksel:		2;
		unsigned int gcaclksel:		2;
		unsigned int reserved0:		22;
	} b;
	unsigned int v;
} pmua_pllsel;

typedef union {
	struct {
		unsigned int core_clk_div:	3;
		unsigned int bus_mc_clk_div:	3;
		unsigned int biu_clk_div:	3;
		unsigned int xp_clk_div:	3;
		unsigned int ddr_clk_div:	3;
		unsigned int bus_clk_div:	3;
		unsigned int async1:		1;
		unsigned int async2:		1;
		unsigned int async3:		1;
		unsigned int async3_1:		1;
		unsigned int async4:		1;
		unsigned int async5:		1;
		unsigned int core_freq_chg_req:	1;
		unsigned int ddr_freq_chg_req:	1;
		unsigned int bus_freq_chg_req:	1;
		unsigned int core_allow_spd_chg:1;
		unsigned int core_dyn_fc:	1;
		unsigned int dclk_dyn_fc:	1;
		unsigned int aclk_dyn_fc:	1;
		unsigned int core_rd_st_clear:	1;
	} b;
	unsigned int v;
} pmua_cc;

typedef union {
	struct {
		unsigned int core_clk_div:	3;
		unsigned int bus_mc_clk_div:	3;
		unsigned int biu_clk_div:	3;
		unsigned int xp_clk_div:	3;
		unsigned int ddr_clk_div:	3;
		unsigned int bus_clk_div:	3;
		unsigned int async1:		1;
		unsigned int async2:		1;
		unsigned int async3:		1;
		unsigned int async3_1:		1;
		unsigned int async4:		1;
		unsigned int async5:		1;
		unsigned int sea_rd_status:	1;
		unsigned int moh_rd_status:	1;
		unsigned int cp_fc_done:	1;
		unsigned int ap_fc_done:	1;
		unsigned int dclk_fc_done:	1;
		unsigned int aclk_fc_done:	1;
		unsigned int reserved:		2;
	} b;
	unsigned int v;
} pmua_dm_cc;

struct operating_point {
	u32 pclk;
	u32 pdclk;
	u32 baclk;
	u32 xpclk;
	u32 dclk;
	u32 aclk;
	u32 cp_pclk;
	u32 cp_pdclk;
	u32 cp_baclk;
	u32 cp_xpclk;
	u32 cp_clk_src;
	u32 ap_clk_src;
	u32 ddr_clk_src;
	u32 axi_clk_src;
	u32 gc_clk_src;
	u32 pll2freq;
	u32 vcore;
};

#define CLK_SRC_VCTCXO		(1u<<0)
#define CLK_SRC_PLL1_312	(1u<<1)
#define CLK_SRC_PLL1_624	(1u<<2)
#define CLK_SRC_PLL2		(1u<<3)

static int fc_lock_ref_cnt;

static void get_fc_lock(void)
{
	pmua_dm_cc dm_cc_ap;

	fc_lock_ref_cnt++;

	if (fc_lock_ref_cnt == 1) {
		int timeout = 100000;

		/* AP-CP FC mutual exclusion */
		dm_cc_ap.v = __raw_readl(PMUA_DM_CC_MOH);
		while (dm_cc_ap.b.sea_rd_status && timeout) {
			dm_cc_ap.v = __raw_readl(PMUA_DM_CC_MOH);
			timeout--;
		}
		if (timeout <= 0)
			printf("cp does not release its fc lock\n");
	}
}

static void put_fc_lock(void)
{
	pmua_cc cc_ap;

	fc_lock_ref_cnt--;

	if (fc_lock_ref_cnt < 0)
		printf("unmatched put_fc_lock\n");

	if (fc_lock_ref_cnt == 0) {
		/* write 1 to MOH_RD_ST_CLEAR to clear MOH_RD_STATUS */
		cc_ap.v = __raw_readl(PMUA_CC_MOH);
		cc_ap.b.core_rd_st_clear = 1;
		__raw_writel(cc_ap.v, PMUA_CC_MOH);
		cc_ap.b.core_rd_st_clear = 0;
		__raw_writel(cc_ap.v, PMUA_CC_MOH);
	}
}

static unsigned int to_refd_div(unsigned int refd)
{
	unsigned int div;
	switch (refd) {
	case 0x00: div = 1; break;
	case 0x10: div = 2; break;
	case 0x01: div = 3; break;
	default: div = refd + 2; break;
	}
	return div;
}

static unsigned int to_fbd_div(unsigned int fbd)
{
	return fbd + 2;
}

static u32 get_pll2_freq(void)
{
	pmum_pll2cr pll2cr;

	pll2cr.v = __raw_readl(PMUM_PLL2CR);
	if ((pll2cr.b.ctrl == 1) && (pll2cr.b.en == 0))
		return 0;
	return 26 * to_fbd_div(pll2cr.b.pll2fbd) / to_refd_div(pll2cr.b.pll2refd);
}

static void turn_off_pll2(void)
{
	pmum_pll2cr pll2cr;

	pll2cr.v = __raw_readl(PMUM_PLL2CR);
	pll2cr.b.ctrl = 1; /* Let SW control PLL2 */
	pll2cr.b.en = 0;   /* disable PLL2 by en bit */
	__raw_writel(pll2cr.v, PMUM_PLL2CR);
}

static void turn_on_pll2(u32 pll2freq)
{
	pmum_pll2cr pll2cr;

	pll2cr.v = 0;
	pll2cr.b.pll2refd = 4;
	pll2cr.b.pll2fbd = pll2freq*(pll2cr.b.pll2refd+2)/26 - 2;
	if (pll2freq % 26)
		pll2cr.b.pll2fbd += 1;
	/* we must lock refd/fbd first before enabling PLL2 */
	pll2cr.b.ctrl = 1;
	__raw_writel(pll2cr.v, PMUM_PLL2CR);
	/* PMU controlls PLL2 activation, pll2cr.b.en don't care */
	pll2cr.b.ctrl = 0;
	__raw_writel(pll2cr.v, PMUM_PLL2CR);
	udelay(200);
}

static unsigned int to_clk_src(unsigned int sel, u32 pll2freq)
{
	unsigned int clk = 0;

	switch (sel) {
	case 0: clk = 312; break;
	case 1: clk = 624; break;
	case 2: clk = pll2freq; break;
	case 3: clk = 26; break;
	default: printf("Wrong clock source\n"); break;
	}
	return clk;
}

static void get_current_op(struct operating_point *cop)
{
	pmua_pllsel pllsel;
	pmua_dm_cc dm_cc_cp, dm_cc_ap;
	pmua_cc cc_cp;

	get_fc_lock();
	dm_cc_ap.v = __raw_readl(PMUA_DM_CC_MOH);
	dm_cc_cp.v = __raw_readl(PMUA_DM_CC_SEA);
	cc_cp.v = __raw_readl(PMUA_CC_SEA);
	cc_cp.b.core_rd_st_clear = 1;
	__raw_writel(cc_cp.v, PMUA_CC_SEA);
	cc_cp.b.core_rd_st_clear = 0;
	__raw_writel(cc_cp.v, PMUA_CC_SEA);
	pllsel.v = __raw_readl(PMUA_PLL_SEL_STATUS);

	cop->pll2freq = get_pll2_freq();
	cop->ap_clk_src = to_clk_src(pllsel.b.apclksel, cop->pll2freq);
	cop->cp_clk_src = to_clk_src(pllsel.b.cpclksel, cop->pll2freq);
	cop->axi_clk_src = to_clk_src(pllsel.b.axiclksel, cop->pll2freq);
	cop->ddr_clk_src = to_clk_src(pllsel.b.ddrclksel, cop->pll2freq);
	cop->gc_clk_src = to_clk_src(pllsel.b.gcaclksel, cop->pll2freq);
	cop->pclk = cop->ap_clk_src/(dm_cc_ap.b.core_clk_div+1);
	cop->pdclk = cop->ap_clk_src/(dm_cc_ap.b.bus_mc_clk_div+1);
	cop->baclk = cop->ap_clk_src/(dm_cc_ap.b.biu_clk_div+1);
	cop->xpclk = cop->ap_clk_src/(dm_cc_ap.b.xp_clk_div+1);
	cop->cp_pclk = cop->cp_clk_src/(dm_cc_cp.b.core_clk_div+1);
	cop->cp_pdclk = cop->cp_clk_src/(dm_cc_cp.b.bus_mc_clk_div+1);
	cop->cp_baclk = cop->cp_clk_src/(dm_cc_cp.b.biu_clk_div+1);
	cop->cp_xpclk = cop->cp_clk_src/(dm_cc_cp.b.xp_clk_div+1);
	cop->dclk = cop->ddr_clk_src/(dm_cc_ap.b.ddr_clk_div+1)/2;
	cop->aclk = cop->axi_clk_src/(dm_cc_ap.b.bus_clk_div+1);

	if ((cop->cp_clk_src != 312))
		printf("Wrong cp clock source\n");
	if ((cop->gc_clk_src != 312) && (cop->gc_clk_src != 624))
		printf("Wrong gc clock source\n");
	if (cpu_is_pxa910_Ax() && dm_cc_ap.b.biu_clk_div)
		printf("biu_clk_div should be 0 for Ax stepping\n");

	cop->vcore = get_volt();
	if (cop->vcore < 1000 || cop->vcore > 1400)
		printf("vcore is not in the [1000,1400] voltage range\n");
	put_fc_lock();
}

static u32 choose_clk_src(u32 clk)
{
	u32 choice = 0;

	if ((26 % clk) == 0) {
		choice |= CLK_SRC_VCTCXO;
	} else {
		if ((312/clk <= 8) && ((312 % clk) == 0))
			choice |= CLK_SRC_PLL1_312;
		if ((624/clk <= 8) && ((624 % clk) == 0))
			choice |= CLK_SRC_PLL1_624;
		/* we assume the clk can always be generated from PLL2 */
		choice |= CLK_SRC_PLL2;
	}
	return choice;
}

/*
 * Clock source selection principle:
 *   1. do not use PLL2 if possible
 *   2. use the same clock source for core and ddr if possible
 *   3. try to use the lowest PLL source for core/ddr/axi
 *
 * FIXME: the code does not follow the principle right now
 */
static void select_clk_src(struct operating_point *top)
{
	u32 dclk2x = top->dclk*2;
	u32 ap_clk_choice, axi_clk_choice, ddr_clk_choice;

	ap_clk_choice = choose_clk_src(top->pclk);
	axi_clk_choice = choose_clk_src(top->aclk);
	ddr_clk_choice = choose_clk_src(dclk2x);

	if ((ap_clk_choice == 0) || (axi_clk_choice == 0) || (ddr_clk_choice == 0))
		printf("Wrong clock combination\n");

	top->pll2freq = top->ap_clk_src = top->axi_clk_src = top->ddr_clk_src = 0;

	if (ap_clk_choice & axi_clk_choice & ddr_clk_choice & CLK_SRC_PLL1_312) {
		top->ap_clk_src = top->axi_clk_src = top->ddr_clk_src = 312;
	} else if (ap_clk_choice & axi_clk_choice & ddr_clk_choice & CLK_SRC_PLL1_624) {
		top->ap_clk_src = top->axi_clk_src = top->ddr_clk_src = 624;
	} else if ((ap_clk_choice & CLK_SRC_PLL1_312) &&
		   (axi_clk_choice & ddr_clk_choice & CLK_SRC_PLL1_624)) {
		top->ap_clk_src = 312; top->axi_clk_src = top->ddr_clk_src = 624;
	} else if ((ap_clk_choice & CLK_SRC_PLL1_624) &&
		   (axi_clk_choice & ddr_clk_choice & CLK_SRC_PLL1_312)) {
		top->ap_clk_src = 624; top->axi_clk_src = top->ddr_clk_src = 312;
	} else if ((axi_clk_choice & CLK_SRC_PLL1_312) &&
		   (ap_clk_choice & ddr_clk_choice & CLK_SRC_PLL1_624)) {
		top->axi_clk_src = 312; top->ap_clk_src = top->ddr_clk_src = 624;
	} else if ((axi_clk_choice & CLK_SRC_PLL1_624) &&
		   (ap_clk_choice & ddr_clk_choice & CLK_SRC_PLL1_312)) {
		top->axi_clk_src = 624; top->ap_clk_src = top->ddr_clk_src = 312;
	} else if ((ddr_clk_choice & CLK_SRC_PLL1_312) &&
		   (ap_clk_choice & axi_clk_choice & CLK_SRC_PLL1_624)) {
		top->ddr_clk_src = 312; top->ap_clk_src = top->axi_clk_src = 624;
	} else if ((ddr_clk_choice & CLK_SRC_PLL1_624) &&
		   (ap_clk_choice & axi_clk_choice & CLK_SRC_PLL1_312)) {
		top->ddr_clk_src = 624; top->ap_clk_src = top->axi_clk_src = 312;
	} else if (ap_clk_choice & CLK_SRC_PLL2) {
		top->pll2freq = top->pclk;
		top->ap_clk_src = top->pll2freq;
		if ((top->pll2freq/dclk2x <= 8) && (top->pll2freq % dclk2x) == 0)
			top->ddr_clk_src = top->pll2freq;
		else if (ddr_clk_choice & CLK_SRC_PLL1_312)
			top->ddr_clk_src = 312;
		else if (ddr_clk_choice & CLK_SRC_PLL1_624)
			top->ddr_clk_src = 624;
		if (top->ddr_clk_src == 0)
			top->ddr_clk_src = top->pll2freq;
		/* try to use lowest PLL source for axi bus */
		if (axi_clk_choice & CLK_SRC_PLL1_312)
			top->axi_clk_src = 312;
		else if (axi_clk_choice & CLK_SRC_PLL1_624)
			top->axi_clk_src = 624;
		else if ((top->pll2freq/top->aclk <= 8) && (top->pll2freq % top->aclk) == 0)
			top->axi_clk_src = top->pll2freq;
		if (top->axi_clk_src == 0)
			top->axi_clk_src = top->pll2freq;
	} else if (axi_clk_choice & CLK_SRC_PLL2) {
		top->pll2freq = top->aclk;
		top->axi_clk_src = top->pll2freq;
		if (ap_clk_choice & CLK_SRC_PLL1_312)
			top->ap_clk_src = 312;
		else if (ap_clk_choice & CLK_SRC_PLL1_624)
			top->ap_clk_src = 624;
		if ((top->pll2freq/dclk2x <= 8) && (top->pll2freq % dclk2x) == 0)
			top->ddr_clk_src = top->pll2freq;
		else if (ddr_clk_choice & CLK_SRC_PLL1_312)
			top->ddr_clk_src = 312;
		else if (ddr_clk_choice & CLK_SRC_PLL1_624)
			top->ddr_clk_src = 624;
	} else if (ddr_clk_choice & CLK_SRC_PLL2) {
		top->pll2freq = dclk2x;
		top->ddr_clk_src = top->pll2freq;
		if (ap_clk_choice & CLK_SRC_PLL1_312)
			top->ap_clk_src = 312;
		else if (ap_clk_choice & CLK_SRC_PLL1_624)
			top->ap_clk_src = 624;
		if (axi_clk_choice & CLK_SRC_PLL1_312)
			top->axi_clk_src = 312;
		else if (axi_clk_choice & CLK_SRC_PLL1_624)
			top->axi_clk_src = 624;
	} else if (ap_clk_choice & axi_clk_choice & ddr_clk_choice & CLK_SRC_VCTCXO) {
		top->ap_clk_src = top->axi_clk_src = top->ddr_clk_src = 26;
	}

	if ((top->ap_clk_src == 0) || (top->axi_clk_src == 0) || (top->ddr_clk_src == 0))
		printf("Wrong clock combination\n");
}

static void set_ap_clk_sel(struct operating_point *top)
{
	pmum_fccr fccr;

	fccr.v = __raw_readl(PMUM_FCCR);
	if (top->ap_clk_src == 312)
		fccr.b.mohclksel = 0;
	else if (top->ap_clk_src == 624)
		fccr.b.mohclksel = 1;
	else if (top->ap_clk_src == top->pll2freq)
		fccr.b.mohclksel = 2;
	else if (top->ap_clk_src == 26)
		fccr.b.mohclksel = 3;
	__raw_writel(fccr.v, PMUM_FCCR);
}

static void set_axi_clk_sel(struct operating_point *top)
{
	pmum_fccr fccr;

	fccr.v = __raw_readl(PMUM_FCCR);
	if (top->axi_clk_src == 312) {
		fccr.b.axiclksel1 = 0;
		fccr.b.axiclksel0 = 0;
	} else if (top->axi_clk_src == 624) {
		fccr.b.axiclksel1 = 0;
		fccr.b.axiclksel0 = 1;
	} else if (top->axi_clk_src == top->pll2freq) {
		fccr.b.axiclksel1 = 1;
		fccr.b.axiclksel0 = 0;
	} else if (top->axi_clk_src == 26) {
		fccr.b.axiclksel1 = 1;
		fccr.b.axiclksel0 = 1;
	}
	__raw_writel(fccr.v, PMUM_FCCR);
}

static void set_ddr_clk_sel(struct operating_point *top)
{
	pmum_fccr fccr;

	fccr.v = __raw_readl(PMUM_FCCR);
	if (top->ddr_clk_src == 312)
		fccr.b.ddrclksel = 0;
	else if (top->ddr_clk_src == 624)
		fccr.b.ddrclksel = 1;
	else if (top->ddr_clk_src == top->pll2freq)
		fccr.b.ddrclksel = 2;
	else if (top->ddr_clk_src == 26)
		fccr.b.ddrclksel = 3;
	__raw_writel(fccr.v, PMUM_FCCR);
}

static void enable_fc_intr(void)
{
	u32 fc_int_msk;

	fc_int_msk = __raw_readl(PMUA_MOH_IMR);
	fc_int_msk |= (7<<3);
	__raw_writel(fc_int_msk, PMUA_MOH_IMR);
}

static void wait_for_ap_fc_done(void)
{
	while (!((1<<3) & __raw_readl(PMUA_MOH_ISR)))
		;
	__raw_writel(0x0, PMUA_MOH_ISR);
}

static void wait_for_axi_fc_done(void)
{
	while (!((1<<5) & __raw_readl(PMUA_MOH_ISR)))
		;
	__raw_writel(0x0, PMUA_MOH_ISR);
}

static void wait_for_ddr_fc_done(void)
{
	while (!((1<<4) & __raw_readl(PMUA_MOH_ISR)))
		;
	__raw_writel(0x0, PMUA_MOH_ISR);
}

static void PMUcore2_fc_seq(struct operating_point *cop, struct operating_point *top)
{
	pmua_cc cc_ap;
	u32 dclk2x = top->dclk*2;

	if (top->vcore > cop->vcore)
		set_volt(top->vcore);

	cc_ap.v = __raw_readl(PMUA_CC_MOH);

	if (top->pll2freq)
		turn_on_pll2(top->pll2freq);

	/* async5 async4 async3_1 async3 are always 1 */
	cc_ap.b.async5 = 1;
	cc_ap.b.async4 = 1;
	cc_ap.b.async3_1 = 1;
	cc_ap.b.async3 = 1;

	/* use async mode when doing core/axi frequency change */
	cc_ap.b.async2 = 1;
	cc_ap.b.async1 = 1;

	if ((cop->ap_clk_src != top->ap_clk_src) || (cop->pclk != top->pclk) ||
	    (cop->pdclk != top->pdclk) || (cop->baclk != top->baclk)) {
		set_ap_clk_sel(top);
		cc_ap.b.core_clk_div = top->ap_clk_src / top->pclk - 1;
		cc_ap.b.bus_mc_clk_div = top->ap_clk_src / top->pdclk - 1;
		if (cpu_is_pxa910_Ax())
			cc_ap.b.biu_clk_div = 0;
		else
			cc_ap.b.biu_clk_div = top->ap_clk_src / top->baclk - 1;
		cc_ap.b.xp_clk_div = top->ap_clk_src / top->xpclk - 1;
		cc_ap.b.core_freq_chg_req = 1;
		__raw_writel(cc_ap.v, PMUA_CC_MOH);
		wait_for_ap_fc_done();
		cc_ap.b.core_freq_chg_req = 0;
	}

	if ((cop->axi_clk_src != top->axi_clk_src) || (cop->aclk != top->aclk)) {
		set_axi_clk_sel(top);
		cc_ap.b.bus_clk_div = top->axi_clk_src / top->aclk - 1;
		cc_ap.b.bus_freq_chg_req = 1;
		__raw_writel(cc_ap.v, PMUA_CC_MOH);
		wait_for_axi_fc_done();
		cc_ap.b.bus_freq_chg_req = 0;
	}

	/* set sync mode if possible when doing ddr frequency change */
	if ((top->ap_clk_src == top->ddr_clk_src) && (top->pdclk == top->dclk))
		cc_ap.b.async2 = 0;
	else
		cc_ap.b.async2 = 1;
	if ((cop->cp_clk_src == top->ddr_clk_src) && (cop->cp_pdclk == top->dclk))
		cc_ap.b.async1 = 0;
	else
		cc_ap.b.async1 = 1;

	if ((cop->ddr_clk_src != top->ddr_clk_src) || (cop->dclk != top->dclk) ||
	    (cc_ap.b.async2 == 0) || (cc_ap.b.async1 == 0)) {
		set_ddr_clk_sel(top);
		cc_ap.b.ddr_clk_div = top->ddr_clk_src / dclk2x - 1;
		cc_ap.b.ddr_freq_chg_req = 1;
		__raw_writel(cc_ap.v, PMUA_CC_MOH);
		wait_for_ddr_fc_done();
		cc_ap.b.ddr_freq_chg_req = 0;
	}

	/* turn off pll2 if not used */
	if (top->pll2freq == 0)
		turn_off_pll2();

	if (top->vcore < cop->vcore)
		set_volt(top->vcore);
}

#define OP(p, pd, ba, xp, d, a, v) \
	{			\
	.pclk	= p,		\
	.pdclk	= pd,		\
	.baclk	= ba,		\
	.xpclk	= xp,		\
	.dclk	= d,		\
	.aclk	= a,		\
	.vcore	= v,		\
	}

struct operating_point op[] = {
	/* pclk pdclk baclk xpclk  dclk  aclk vcore */
	OP(156,   78,   78,  156,   78,   78, 1200), /* op0 */
	OP( 78,   39,   39,   78,   78,   78, 1200),
	OP(156,   78,   78,  156,  104,  104, 1200),
	OP(312,  156,  156,  312,  156,  156, 1200),
	OP(624,  156,  156,  312,  156,  156, 1200),
	OP(  0,    0,    0,    0,    0,    0,    0), /* op5 */
	OP(  0,    0,    0,    0,    0,    0,    0),
	OP(  0,    0,    0,    0,    0,    0,    0),
	OP(  0,    0,    0,    0,    0,    0,    0),
	OP(  0,    0,    0,    0,    0,    0,    0),
	OP(  0,    0,    0,    0,    0,    0,    0), /* op10 */
	OP(  0,    0,    0,    0,    0,    0,    0),
	OP(  0,    0,    0,    0,    0,    0,    0),
	OP(806,  201,  201,  403,  201,  201, 1300),
	OP(780,  195,  195,  390,  195,  156, 1300),
	OP(801,  200,  200,  400,  200,  208, 1300), /* op15 */
	OP(797,  199,  199,  398,  199,  208, 1300),
	OP(936,  156,  156,  312,  156,  156, 1400),
	OP(988,  164,  164,  329,  164,  197, 1400),
};

int setop(int num)
{
	u32 temp;
	/*u32 gc_clk_res, lcd_clk_res;*/
	struct operating_point cop;
	pmua_cc cc_cp, cc_ap;

	if (op[num].pclk == 0) {
		printf("op is not supported\n");
		return -1;
	}

	/*
	 * Release LCD and GC reset so that AXI2MC reset is de-asserted,
	 * and ddr frequency change can be done.
	 * In order to disable GC/LCD setting when they are working,
	 * read back and OR.
	 */
	/*gc_clk_res = __raw_readl(PMUA_GC_CLK_RES_CTRL);
	__raw_writel(gc_clk_res | 0x3F, PMUA_GC_CLK_RES_CTRL);
	lcd_clk_res = __raw_readl(PMUA_LCD_CLK_RES_CTRL);
	__raw_writel(lcd_clk_res | 0x1, PMUA_LCD_CLK_RES_CTRL);*/

	temp = __raw_readl(0xd4282888);
	/*
	 * Change the AP freq clk while CP is at reset or idle by setting
	 * AP_WFI_FC, CP_WFI_FC, MASK_CP_CLK_OFF_ACK, MASK_CP_HALT
	 * To skip DDR ACK while lcd and gc are in reset by setting
	 * MASK_MC_HALT
	 */
	__raw_writel(temp|(1<<22)|(1<<21)|(1<<3)|(1<<2)|(1<<0), 0xd4282888);

	cc_cp.v = __raw_readl(PMUA_CC_SEA);
	cc_cp.b.core_allow_spd_chg = 1;
	__raw_writel(cc_cp.v, PMUA_CC_SEA);

	cc_ap.v = __raw_readl(PMUA_CC_MOH);
	cc_ap.b.core_allow_spd_chg = 1;
	__raw_writel(cc_ap.v, PMUA_CC_MOH);

	enable_fc_intr();

	get_fc_lock();

	get_current_op(&cop);
	select_clk_src(&op[num]);

	/* If pll2 source frequency is changed, use startup op(op0) as a bridge */
	if (cop.pll2freq && op[num].pll2freq && (op[num].pll2freq != cop.pll2freq)) {
		select_clk_src(&op[0]);
		PMUcore2_fc_seq(&cop, &op[0]);
		get_current_op(&cop);
		PMUcore2_fc_seq(&cop, &op[num]);
	} else {
		PMUcore2_fc_seq(&cop, &op[num]);
	}

	get_current_op(&cop);
	if ((cop.pclk != op[num].pclk) || (cop.pdclk != op[num].pdclk) ||
	    (cop.xpclk != op[num].xpclk) ||
	    (!cpu_is_pxa910_Ax() && (cop.baclk != op[num].baclk)) ||
	    (cop.aclk != op[num].aclk) || (cop.dclk != op[num].dclk) ||
	    (cop.ap_clk_src != op[num].ap_clk_src) ||
	    (cop.axi_clk_src != op[num].axi_clk_src) ||
	    (cop.ddr_clk_src != op[num].ddr_clk_src))
		printf("unsuccessful frequency change!\n");

	put_fc_lock();

	/* restore PMUA_DEBUG register */
	__raw_writel(temp, 0xd4282888);

	/* restore gc and lcd reset control setting */
	/*__raw_writel(gc_clk_res, PMUA_GC_CLK_RES_CTRL);
	__raw_writel(lcd_clk_res, PMUA_LCD_CLK_RES_CTRL);*/

	return 0;
}

void show_op(void)
{
	struct operating_point cop;

	get_current_op(&cop);
	printf("op:    CPU at %3u MHz DDR at %3u MHz AXI at %3u MHz\n",
		cop.pclk, cop.dclk, cop.aclk);
}

int do_op(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	ulong num;

	if (argc == 2) {
		num = simple_strtoul(argv[1], NULL, 0);
		if (setop(num) < 0)
			return -1;
	}
	if (argc <= 2) {
		show_op();
	}
	return 0;
}

U_BOOT_CMD(
	op,	2,	1,	do_op,
	"change operating point",
	"[ op number ]"
);

#define READ_TIMER \
		(*(volatile ulong *)(CONFIG_SYS_TIMERBASE+0xa4) = 0x1, \
		*(volatile ulong *)(CONFIG_SYS_TIMERBASE+0xa4))

int do_calibrate_delay(cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	ulong oldtimer, timer;
	unsigned int c0_l, c0_u;
	unsigned int cc0_l, cc0_u;
	unsigned int e0 = 0x3;
	unsigned int cpu_cycles;


	cpu_cycles = 0;
	printf("Calibrating delay loop.. ");

	// init performance counter
	__asm__ __volatile__("\n\t\
		mcr p15, 0, %0, c15, c12, 0\n\t\
		mcr p15, 0, %0, c15, c13, 0\n\t\
		mcr p15, 0, %0, c15, c13, 1\n\t\
		mcr p15, 0, %1, c15, c12, 0\n\t"
		:
		:"r"(0),"r"(e0)
	);
	// read counter
	__asm__ __volatile__("\n\t\
		mrc p15, 0, %0, c15, c13, 0\n\t\
		mrc p15, 0, %1, c15, c13, 1\n\t"
		:"=&r"(c0_l),"=&r"(c0_u)
		:
		:"memory", "cc"
	);

	oldtimer = READ_TIMER;
	oldtimer = READ_TIMER;
	while (1) {
		timer = READ_TIMER;
		timer = READ_TIMER;
		timer = timer - oldtimer;
		if (timer >= CONFIG_SYS_HZ)
			break;
	}

	// stop performace counter
	__asm__ __volatile__("\n\t\
		mcr p15, 0, %0, c15, c12, 0\n\t"
		:
		:"r"(0)
	);
	// read counter
	__asm__ __volatile__("\n\t\
		mrc p15, 0, %0, c15, c13, 0\n\t\
		mrc p15, 0, %1, c15, c13, 1\n\t"
		:"=&r"(cc0_l),"=&r"(cc0_u)
		:
		:"memory", "cc"
	);

	if (cc0_u != c0_u)
		printf("counter overflow\n");

	cpu_cycles = cc0_l - c0_l;

	printf("ok - %lu.%02lu BogoMips\n",
			cpu_cycles/1000000,
			(cpu_cycles/10000) % 100);

	return 0;
}

U_BOOT_CMD(
	mips,	1,	1,	do_calibrate_delay,
	"calculating BogoMips",
	""
);

#define VBUCK1_CNT_2(x) ((x < 0) ? -1 :                 \
		((x < 800) ? (x / 25 + 0x20) :  \
		 ((x < 1525) ? ((x - 725) / 25)  \
		  : -1)))
#define VBUCK1_MV_2(x)  ((x < 0) ? -1 :                 \
		((x < 0x1F) ? (x * 25 + 725) :  \
		 ((x < 0x3F) ? ((x - 0x20) * 25) \
		  : -1)))
u32 get_volt(void)
{
	u8 cnt = 0;
	u32 vol = 0;

	if (!i2c_read(CONFIG_SYS_I2C_POWER_ADDR, 0x24, 1, &cnt, 1))
		vol = VBUCK1_MV_2(cnt);
	else
		printf("i2c_read: failed!\n");

	return vol;
}

int set_volt(u32 vol)
{
	int cnt = -1 , res = 0;
	u8 data;

	cnt = VBUCK1_CNT_2(vol);
	if (cnt < 0)
		return -1;

	data = 0;
	res = i2c_write(CONFIG_SYS_I2C_POWER_ADDR, 0x40, 1, &data, 1);	/* enable GI2C access to BUCK1_SET */
	if (res)
		return res;
	data = cnt & 0xff;
	res = i2c_write(CONFIG_SYS_I2C_POWER_ADDR, 0x24, 1, &data, 1);	/* set BUCK1 voltage */
	if (res)
		return res;
	data = 0x1;
	res = i2c_write(CONFIG_SYS_I2C_POWER_ADDR, 0x20, 1, &data, 1);	/* let it go ramping */
	if (res)
		return res;
	return 0;
}
