/*
 * Driver for simulating ssp as spi device for pxa
 *
 * Copyright (c) 2009 Marvell Inc.
 * Lei Wen <leiwen@marvell.com>
 *
 * Licensed under the GPL-2 or later.
 */


#include <common.h>
#include <malloc.h>
#include <spi.h>

#include <asm/io.h>
#include <asm/arch/gpio.h>

#define SSSR_TNF	(1 << 2)	/* Transmit FIFO Not Full */
#define SSSR_RNE	(1 << 3)	/* Receive FIFO Not Empty */
#define SSSR_BSY	(1 << 4)	/* SSP Busy */
#define SSSR_ROR	(1 << 7)	/* Receive FIFO Overrun */
#define SSSR_TINT	(1 << 19)	/* Receiver Time-out Interrupt */
#define SSCR0_SSE	(1 << 7)	/* Synchronous Serial Port Enable */
#define SSCR1_RIE	(1 << 0)	/* Receive FIFO Interrupt Enable */
#define SSCR1_TIE	(1 << 1)	/* Transmit FIFO Interrupt Enable */
#define SSCR1_SPO	(1 << 3)	/* Motorola SPI SSPSCLK polarity setting */
#define SSCR1_SPH	(1 << 4)	/* Motorola SPI SSPSCLK phase setting */
#define SSCR1_TINTE	(1 << 19)	/* Receiver Time-out Interrupt enable */
#define SSCR1_TFT	(0x000003c0)	/* Transmit FIFO Threshold (mask) */
#define SSCR1_RFT	(0x00003c00)	/* Receive FIFO Threshold (mask) */

#define SSCR0_SerClkDiv(x) ((((x) - 2)/2) << 8) /* Divisor [2..512] */
#define SSCR0_DataSize(x)  ((x) - 1)	/* Data Size Select [4..16] */
#define SSCR1_RxTresh(x) (((x) - 1) << 10) /* level [1..16] */
#define SSCR1_TxTresh(x) (((x) - 1) << 6) /* level [1..16] */
struct pxa_spi_slave {
	struct spi_slave slave;
	u32 cr0, cr1;
	u32 int_cr1;
	u32 clear_sr;
	const void *tx;
	void *rx;
	int gpio_cs_inverted;

	void (*write)(struct pxa_spi_slave *pss);
	void (*read)(struct pxa_spi_slave *pss);
};

#define to_pxa_spi_slave(s) container_of(s, struct pxa_spi_slave, slave)

#define DEFINE_SSP_REG(reg, off) \
	static inline u32 read_##reg(void) \
{ return __raw_readl(CONFIG_SYS_SSP_BASE + (off)); } \
\
static inline void write_##reg(u32 v) \
{ __raw_writel(v, CONFIG_SYS_SSP_BASE + (off)); }

DEFINE_SSP_REG(SSCR0, 0x00)
DEFINE_SSP_REG(SSCR1, 0x04)
DEFINE_SSP_REG(SSSR, 0x08)
DEFINE_SSP_REG(SSITR, 0x0c)
DEFINE_SSP_REG(SSDR, 0x10)
DEFINE_SSP_REG(SSTO, 0x28)
DEFINE_SSP_REG(SSPSP, 0x2c)

#define DEFAULT_WORD_LEN	8
#define SSP_FLUSH_NUM		0x2000
#define RX_THRESH_DFLT		8
#define TX_THRESH_DFLT		8
#define TIMOUT_DFLT		1000

void spi_cs_activate(struct spi_slave *slave)
{
	struct pxa_spi_slave *pss = to_pxa_spi_slave(slave);
	gpio_set_value(slave->cs, pss->gpio_cs_inverted);
}

void spi_cs_deactivate(struct spi_slave *slave)
{
	struct pxa_spi_slave *pss = to_pxa_spi_slave(slave);
	gpio_set_value(slave->cs, !pss->gpio_cs_inverted);
}

static void null_writer(struct pxa_spi_slave *pss)
{
	while (!(read_SSSR() & SSSR_TNF));

	write_SSDR(0);
}

static void null_reader(struct pxa_spi_slave *pss)
{
	while (!(read_SSSR() & SSSR_RNE));
	read_SSDR();
}

static void u8_writer(struct pxa_spi_slave *pss)
{
	while (!(read_SSSR() & SSSR_TNF));

	write_SSDR(*(u8 *)(pss->tx));
	++pss->tx;
}

static void u8_reader(struct pxa_spi_slave *pss)
{
	while (!(read_SSSR() & SSSR_RNE));
	*(u8 *)(pss->rx) = read_SSDR();
	++pss->rx;
}

static int flush(void)
{
	unsigned long limit = SSP_FLUSH_NUM;

	do {
		while (read_SSSR() & SSSR_RNE) {
			read_SSDR();
		}
	} while ((read_SSSR() & SSSR_BSY) && limit--);
	write_SSSR(SSSR_ROR);

	return limit;
}

struct spi_slave *spi_setup_slave(unsigned int bus, unsigned int cs,
		unsigned int max_hz, unsigned int mode)
{
	struct pxa_spi_slave *pss;
	unsigned int clk_div;

	clk_div = ((CONFIG_SSP_CLK / max_hz - 1) & 0xfff) << 8;

	pss = malloc(sizeof(*pss));
	if (!pss)
		return NULL;

	pss->slave.bus = bus;
	pss->slave.cs = cs;

	pss->cr0 = clk_div
		| SSCR0_DataSize(DEFAULT_WORD_LEN)
		| SSCR0_SSE;

	pss->cr1 = (SSCR1_RxTresh(RX_THRESH_DFLT) & SSCR1_RFT) |
		(SSCR1_TxTresh(TX_THRESH_DFLT) & SSCR1_TFT);
	pss->cr1 &= ~(SSCR1_SPO | SSCR1_SPH);
	pss->cr1 |= (((mode & SPI_CPHA) != 0) ? SSCR1_SPH : 0)
		| (((mode & SPI_CPOL) != 0) ? SSCR1_SPO : 0);

	pss->int_cr1 = SSCR1_TIE | SSCR1_RIE | SSCR1_TINTE;
	pss->clear_sr = SSSR_ROR | SSSR_TINT;

	pss->gpio_cs_inverted = mode & SPI_CS_HIGH;
	gpio_set_value(cs, !pss->gpio_cs_inverted);

	return &pss->slave;
}

void spi_free_slave(struct spi_slave *slave)
{
	struct pxa_spi_slave *pss = to_pxa_spi_slave(slave);
	free(pss);
}

void spi_init()
{
	/* Load default SSP configuration */
	write_SSCR0(0);
	write_SSCR1(SSCR1_RxTresh(RX_THRESH_DFLT) |
			SSCR1_TxTresh(TX_THRESH_DFLT));
	write_SSCR0(SSCR0_SerClkDiv(2)
			| SSCR0_DataSize(DEFAULT_WORD_LEN));
	write_SSTO(0);
	write_SSPSP(0);
}

int spi_claim_bus(struct spi_slave *slave)
{
	debug("%s: bus:%i cs:%i\n", __func__, slave->bus, slave->cs);
	if (flush() == 0) {
		return -1;
	}

	return 0;
}

void spi_release_bus(struct spi_slave *slave)
{
}

int spi_xfer(struct spi_slave *slave, unsigned int bitlen, const void *dout,
		void *din, unsigned long flags)
{
	struct pxa_spi_slave *pss = to_pxa_spi_slave(slave);
	uint bytes = bitlen / 8;
	unsigned long limit;
	int ret = 0;

	if (bitlen == 0)
		goto done;

	/* we can only do 8 bit transfers */
	if (bitlen % 8) {
		flags |= SPI_XFER_END;
		goto done;
	}

	if (dout) {
		pss->tx = dout;
		pss->write = u8_writer;
	}
	else
		pss->write = null_writer;

	if (din) {
		pss->rx = din;
		pss->read = u8_reader;
	}
	else
		pss->read = null_reader;

	if (flags & SPI_XFER_BEGIN) {
		spi_cs_activate(slave);
		write_SSCR1(pss->cr1 | pss->int_cr1);
		write_SSTO(TIMOUT_DFLT);
		write_SSCR0(pss->cr0);
	}

	while (bytes--) {
		limit = SSP_FLUSH_NUM;
		pss->write(pss);
		while ((read_SSSR() & SSSR_BSY) && limit--)
			udelay(1);
		pss->read(pss);
	}

 done:
	if (flags & SPI_XFER_END) {
		/* Stop SSP */
		write_SSSR(pss->clear_sr);
		write_SSCR1(read_SSCR1() & ~pss->int_cr1);
		write_SSTO(0);
		spi_cs_deactivate(slave);
	}

	return ret;
}
