#include <common.h>
#include <i2c.h>
#include <asm/errno.h>
#include <asm/io.h>

#define IBMR		*(volatile u32 *)(CONFIG_I2C_REG_BASE+0x00)
#define IDBR		*(volatile u32 *)(CONFIG_I2C_REG_BASE+0x08)
#define ICR		*(volatile u32 *)(CONFIG_I2C_REG_BASE+0x10)
#define ISR		*(volatile u32 *)(CONFIG_I2C_REG_BASE+0x18)
#define ISAR		*(volatile u32 *)(CONFIG_I2C_REG_BASE+0x20)
#define ILCR		*(volatile u32 *)(CONFIG_I2C_REG_BASE+0x28)

/* data buffer regiter mask */
#define	IDBR_MODE	(1 << 0)

/* Control Register */
#define	ICR_START	(1 << 0)  /* 1:send a Start condition to the I2C when in master mode */
#define	ICR_STOP	(1 << 1)  /* 1:send a Stop condition after next data byte transferred on I2C bus in master mode */
#define	ICR_ACKNACK	(1 << 2)  /* Ack/Nack control: 1:Nack, 0:Ack (negative or positive pulse) */
#define	ICR_TB		(1 << 3)  /* 1:send/receive byte, 0:cleared by I2C unit when done */
#define	ICR_SCLEA	(1 << 5)  /* I2C clock output: 1:Enabled, 0:Disabled. ICCR configured before ! */
#define	ICR_UIE		(1 << 6)  /* I2C unit: 1:Enabled, 0:Disabled */
#define	ICR_ALDIE	(1 << 12) /* 1: Arbitration Loss Detected Interrupt Enable */

/* Status Register */
#define	ISR_ALD		(1 << 5)  /* 1: Arbitration Loss Detected */
#define	ISR_ITE		(1 << 6)  /* 1: Transfer finished on I2C bus. If enabled in ICR, interrupt signaled */
#define	ISR_IRF		(1 << 7)  /* 1: IDBR received new byte from I2C bus. If ICR, interrupt signaled */

int i2c_probe(uchar chip)
{
	/*
		not implemented
	*/

	printf("i2c_probe chip %d\n", (int) chip);
	return -1;
}

int i2c_rx_full(int timeout)
{
	unsigned int temp;
	while (timeout--) {
		temp = ISR;
		if ((temp & ISR_IRF) == ISR_IRF) {
			ISR = temp | ISR_IRF;
			return 0;
		}
		 udelay(200);
	}
	printf("i2c_rx_full timeout\n");
	return -1;
}

int i2c_tx_empty(int timeout)
{
	unsigned int temp;

	while (timeout--) {
		temp = ISR;
		if ((temp & ISR_ITE) == ISR_ITE) {
			ISR = temp | ISR_ITE;
			if ((temp & ISR_ALD) == ISR_ALD) {
				ISR |= ISR_ALD;
			}
			return 0;
		}
		udelay(200);
	}
	printf("i2c_tx_empty timeout\n");
	return -1;
}

int __i2c_write(unsigned char slave_addr, u8 *bytes_buf,
	   unsigned int bytes_count, int stop)
{
	unsigned int reg;

	IDBR = (slave_addr << 1) & ~IDBR_MODE;
	reg = ICR;
	reg |= (ICR_START | ICR_TB);
	reg &= ~(ICR_STOP | ICR_ALDIE);
	ICR = reg;
	if (i2c_tx_empty(20) == -1) {
		return -1;
	}

	/* Send all the bytes */
	while (bytes_count--) {
		IDBR = (unsigned int) (*bytes_buf++);
		reg = ICR;
		reg &= ~ICR_START;
		reg |= (ICR_ALDIE | ICR_TB);
		if ((bytes_count == 0) && stop)
			reg |= ICR_STOP;
		else
			reg &= ~ICR_STOP;
		ICR = reg;
		if (i2c_tx_empty(250) == -1) {
			return -1;
		}
	}

	/* Clear the STOP bit always */
	ICR &= ~ICR_STOP;
	return 0;
}

int __i2c_read(unsigned char slave_addr, unsigned char * bytes_buf,
		unsigned int bytes_count, int stop)
{
	unsigned int reg;

	IDBR = (slave_addr << 1) | IDBR_MODE;
	reg = ICR;
	reg |= (ICR_START | ICR_TB);
	reg &= ~(ICR_STOP | ICR_ALDIE);
	ICR = reg;
	if (i2c_tx_empty(20) == -1) {
		return -1;
	}
	while (bytes_count--) {
		reg = ICR;
		reg &= ~ICR_START;
		reg |= ICR_ALDIE | ICR_TB;
		if (bytes_count == 0) {
			reg |= ICR_ACKNACK;
			if (stop)
				reg |= ICR_STOP;
			else
				reg &= ~ICR_STOP;
		} else {
			reg &= ~ICR_ACKNACK;
		}
		ICR = reg;
		if (i2c_rx_full(60) == -1) {
			return -1;
		}
		reg = IDBR & 0xFF;
		*bytes_buf++ = (unsigned char) reg;
	}
	ICR &= ~(ICR_STOP | ICR_ACKNACK);
	return 0;
}

int i2c_write(u8 dev, uint addr, int alen, u8 *data, int length)
{
	unsigned char buffer[2];
	int status;
	buffer[0] = addr;
	buffer[1] = data[0];

	status = __i2c_write(dev, buffer, 2, 1);
	return status;
}

int i2c_read(u8 dev, uint addr, int alen, u8 *data, int length)
{
	int status;

	status = __i2c_write(dev, (u8 *)&addr, alen, 1);
	if (!status)
		status = __i2c_read(dev, data, length, 1);

	return status;
}

void i2c_init(int speed, int slaveadd)
{
	__raw_writel(0x7, CONFIG_TWSI_CLOCK_BASE);
	udelay(10);
	/* enable i2c clock */
	__raw_writel(0x3, CONFIG_TWSI_CLOCK_BASE);

	ICR = 0;
	/* Setup I2C slave address */
	ISAR = slaveadd;
	ICR = ICR_SCLEA;
	ICR |= ICR_UIE;
}
