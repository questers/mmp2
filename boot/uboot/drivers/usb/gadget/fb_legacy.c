/*
 * Copyright (C) 2008 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Copyright (c) 2010 Marvell Inc.
 * Modified by Lei Wen <leiwen@marvell.com>
 */
#include <usbdevice.h>
#include <usb/mmp_udc.h>
static struct usb_endpoint_instance *eptout, *eptin;
extern struct usb_string_descriptor **usb_strings;
static struct usb_string_descriptor *fb_string_table[5];
typedef struct boot_img_hdr boot_img_hdr;
static struct usb_device_instance device_instance[1];
static struct usb_configuration_descriptor	*configuration_descriptor = 0;
static struct usb_bus_instance bus_instance[1];
static struct usb_configuration_instance config_instance[1];
/* one extra for control endpoint */
static struct usb_endpoint_instance endpoint_instance[4];

void fb_tx_status(const char *status)
{
	struct usb_endpoint_instance *ept = &bus_instance[0].endpoint_array[1];
	int len = strlen(status);
	printf("TX:%s\n", status);
	ept->tx_urb->actual_length = len;
	if (len > 0)
		memcpy(ept->tx_urb->buffer, status, len);
	udc_endpoint_write(ept);
}

static void fb_event_handler (struct usb_device_instance *device,
				  usb_device_event_t event, int data)
{
	switch (event) {
	case DEVICE_RESET:
	case DEVICE_BUS_INACTIVE:
		break;
	case DEVICE_CONFIGURED:
		printf("usb: online \n");
		break;

	case DEVICE_ADDRESS_ASSIGNED:

	default:
		break;
	}
}

static struct usb_device_descriptor device_desc_1 = {
	.bLength = sizeof(struct usb_device_descriptor),
	.bDescriptorType =	USB_DT_DEVICE,
	.bcdUSB =		cpu_to_le16(USB_BCD_VERSION),
	.bMaxPacketSize0 =	EP0_MAX_PACKET_SIZE,
	.idVendor =		cpu_to_le16(CONFIG_USBD_VENDORID),
	.idProduct = 		cpu_to_le16(CONFIG_USBD_PRODUCTID),
	.iManufacturer =	0x1,
	.iProduct =		0x2,
	.iSerialNumber =	0x3,
	.bNumConfigurations =	1
};

static struct fb_config_desc {
	struct usb_configuration_descriptor configuration_desc;
	struct usb_interface_descriptor interface_desc;
	struct usb_endpoint_descriptor data_endpoints[2];
};

static struct fb_config_desc config_desc_1 = {
	.configuration_desc = {
		.bLength = sizeof(struct usb_configuration_descriptor),
		.bDescriptorType = USB_DT_CONFIG,
		.wTotalLength = cpu_to_le16(sizeof(struct fb_config_desc)),
		.bNumInterfaces = 1,
		.bConfigurationValue = 1,
		.iConfiguration = 4,
		.bmAttributes = BMATTRIBUTE_RESERVED,
		.bMaxPower = 0x80
	},
	.interface_desc = {
		.bLength  = sizeof(struct usb_interface_descriptor),
		.bDescriptorType = USB_DT_INTERFACE,
		.bNumEndpoints = 0x2,
		.bInterfaceClass = COMMUNICATIONS_INTERFACE_CLASS_VENDOR,
		.bInterfaceSubClass = 0x42,
		.bInterfaceProtocol = COMMUNICATIONS_TCM_SUBCLASS,
	},
	.data_endpoints = {
		{
			.bLength = sizeof(struct usb_endpoint_descriptor),
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = UDC_IN_ENDPOINT | USB_DIR_IN,
			.bmAttributes =	USB_ENDPOINT_XFER_BULK,
			.wMaxPacketSize	= cpu_to_le16(UDC_BULK_PACKET_SIZE),
			.bInterval = 0x0,
		},
		{
			.bLength = sizeof(struct usb_endpoint_descriptor),
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = UDC_OUT_ENDPOINT | USB_DIR_OUT,
			.bmAttributes =	USB_ENDPOINT_XFER_BULK,
			.wMaxPacketSize	= cpu_to_le16(UDC_BULK_PACKET_SIZE),
			.bInterval = 0x1,
		},
	}
};

static void fb_init_instances (void)
{
	int i;
	/* initialize device instance */
	memset (device_instance, 0, sizeof (struct usb_device_instance));
	device_instance->device_state = STATE_INIT;
	device_instance->device_descriptor = &device_desc_1;
	device_instance->event = fb_event_handler;
	device_instance->bus = bus_instance;
	device_instance->configurations = 1;
	device_instance->configuration_instance_array = config_instance;

	/* initialize bus instance */
	memset (bus_instance, 0, sizeof (struct usb_bus_instance));
	bus_instance->device = device_instance;
	bus_instance->endpoint_array = endpoint_instance;
	bus_instance->max_endpoints = 4;
	bus_instance->maxpacketsize = 64;
	/* configuration instance */
	memset (config_instance, 0,
		sizeof (struct usb_configuration_instance));
	config_instance->configuration_descriptor = (struct usb_configuration_descriptor*)&config_desc_1;
	/* endpoint instances */
	memset (&endpoint_instance[0], 0,
		sizeof (struct usb_endpoint_instance));
	endpoint_instance[0].endpoint_address = 0;
	endpoint_instance[0].rcv_packetSize = EP0_MAX_PACKET_SIZE;
	endpoint_instance[0].rcv_attributes = USB_ENDPOINT_XFER_CONTROL;
	endpoint_instance[0].tx_packetSize = EP0_MAX_PACKET_SIZE;
	endpoint_instance[0].tx_attributes = USB_ENDPOINT_XFER_CONTROL;
	udc_setup_ep (device_instance, 0, &endpoint_instance[0]);

	for (i = 1; i <= 2; i++) {
		memset (&endpoint_instance[i], 0,
			sizeof (struct usb_endpoint_instance));

		endpoint_instance[i].endpoint_address =i;
		if (i % 2)
			endpoint_instance[i].endpoint_address |= USB_DIR_IN;

		urb_link_init (&endpoint_instance[i].rcv);
		urb_link_init (&endpoint_instance[i].rdy);
		urb_link_init (&endpoint_instance[i].tx);
		urb_link_init (&endpoint_instance[i].done);

		if (endpoint_instance[i].endpoint_address & USB_DIR_IN)
			endpoint_instance[i].tx_urb =
				usbd_alloc_urb (device_instance,
						&endpoint_instance[i]);
		else
			endpoint_instance[i].rcv_urb =
				usbd_alloc_urb (device_instance,
						&endpoint_instance[i]);
		udc_setup_ep (device_instance, i, &endpoint_instance[i]);
	}
}

static u8 wstrLang[4] = {4,USB_DT_STRING,0x9,0x4};
static u8 wstrManufacturer[2 + 2*(sizeof(CONFIG_USBD_MANUFACTURER)-1)];
static u8 wstrProduct[2 + 2*(sizeof(CONFIG_USBD_PRODUCT_NAME)-1)];
static u8 wstrSerial[2 + 2*(sizeof(CONFIG_SERIAL_NUM) - 1)];
static u8 wstrConfiguration[2 + 2*(sizeof(CONFIG_USBD_CONFIGURATION_STR)-1)];
static void str2wide (char *str, u16 * wide)
{
	int i;
	for (i = 0; i < strlen (str) && str[i]; i++){
		#if defined(__LITTLE_ENDIAN)
			wide[i] = (u16) str[i];
		#elif defined(__BIG_ENDIAN)
			wide[i] = ((u16)(str[i])<<8);
		#else
			#error "__LITTLE_ENDIAN or __BIG_ENDIAN undefined"
		#endif
	}
}
static void fb_init_strings (void)
{
	struct usb_string_descriptor *string;
	fb_string_table[0] = (struct usb_string_descriptor*)wstrLang;

	string = (struct usb_string_descriptor *) wstrManufacturer;
	string->bLength = sizeof(wstrManufacturer);
	string->bDescriptorType = USB_DT_STRING;
	str2wide (CONFIG_USBD_MANUFACTURER, string->wData);
	fb_string_table[1] = string;

	string = (struct usb_string_descriptor *) wstrProduct;
	string->bLength = sizeof(wstrProduct);
	string->bDescriptorType = USB_DT_STRING;
	str2wide (CONFIG_USBD_PRODUCT_NAME, string->wData);
	fb_string_table[2]=string;


	string = (struct usb_string_descriptor *) wstrSerial;
	string->bLength = sizeof(wstrSerial);
	string->bDescriptorType = USB_DT_STRING;
	str2wide (CONFIG_SERIAL_NUM, string->wData);
	fb_string_table[3]=string;

	string = (struct usb_string_descriptor *) wstrConfiguration;
	string->bLength = sizeof(wstrConfiguration);
	string->bDescriptorType = USB_DT_STRING;
	str2wide (CONFIG_USBD_CONFIGURATION_STR, string->wData);
	fb_string_table[4]=string;
	usb_strings = fb_string_table;
}

void fb_init(void)
{
	udc_init();
	fb_init_strings();
	fb_init_instances ();

	udc_startup_events(device_instance);
	udc_connect();
	eptout = &bus_instance[0].endpoint_array[2];
	eptin = &bus_instance[0].endpoint_array[1];
}

void fb_run(void)
{
	udc_irq();
	if (eptout->rcv_urb->actual_length > 0)
		rcv_cmd(eptout);
	udc_irq();
	udc_endpoint_write(eptin);
	udc_irq();
}

int fb_get_rcv_len(void)
{
	int len;
	len = eptout->rcv_urb->actual_length;
	eptout->rcv_urb->actual_length = 0;
	return len;
}

void *fb_get_buf(void)
{
	return eptout->rcv_urb->buffer;
}

void fb_set_buf (void *buf)
{
	eptout->rcv_urb->buffer = buf;
}

void fb_halt(void)
{
	udc_disconnect();
}
