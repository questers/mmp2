/*
 * Copyright (c) 2010 Marvell Inc.
 * Modified by Lei Wen <leiwen@marvell.com>
 */
#include <common.h>
#include <command.h>
#include <config.h>
#include <net.h>
#include <malloc.h>
#include <asm/io.h>
#include <asm/errno.h>
#include <linux/types.h>
#include <linux/usb/ch9.h>
#include <linux/usb/gadget.h>
#include <usb/mmp_udc_new.h>
#if 1
#define DBG(x...) do {} while(0)
#else
#define DBG(x...) printf(x)
#endif

#define USBMODE_DEVICE 2
DECLARE_GLOBAL_DATA_PTR;

struct ept_queue_head
{
    unsigned config;
    unsigned current; /* read-only */

    unsigned next;
    unsigned info;
    unsigned page0;
    unsigned page1;
    unsigned page2;
    unsigned page3;
    unsigned page4;
    unsigned reserved_0;

    unsigned char setup_data[8];

    unsigned reserved_1;
    unsigned reserved_2;
    unsigned reserved_3;
    unsigned reserved_4;
};

#define CONFIG_MAX_PKT(n)     ((n) << 16)
#define CONFIG_ZLT            (1 << 29)    /* stop on zero-len xfer */
#define CONFIG_IOS            (1 << 15)    /* IRQ on setup */

struct ept_queue_item
{
    unsigned next;
    unsigned info;
    unsigned page0;
    unsigned page1;
    unsigned page2;
    unsigned page3;
    unsigned page4;
    unsigned reserved;
};

#define TERMINATE 1
#define INFO_BYTES(n)         ((n) << 16)
#define INFO_IOC              (1 << 15)
#define INFO_ACTIVE           (1 << 7)
#define INFO_HALTED           (1 << 6)
#define INFO_BUFFER_ERROR     (1 << 5)
#define INFO_TX_ERROR         (1 << 3)


#define STS_SLI               (1 << 8)   /* R/WC - suspend state entered */
#define STS_SRI               (1 << 7)   /* R/WC - SOF recv'd */
#define STS_URI               (1 << 6)   /* R/WC - RESET recv'd - write to clear */
#define STS_FRI               (1 << 3)   /* R/WC - Frame List Rollover */
#define STS_PCI               (1 << 2)   /* R/WC - Port Change Detect */
#define STS_UEI               (1 << 1)   /* R/WC - USB Error */
#define STS_UI                (1 << 0)   /* R/WC - USB Transaction Complete */


/* bits used in all the endpoint status registers */
#define EPT_TX(n) (1 << ((n) + 16))
#define EPT_RX(n) (1 << (n))


#define CTRL_TXE              (1 << 23)
#define CTRL_TXR              (1 << 22)
#define CTRL_RXE              (1 << 7)
#define CTRL_RXR              (1 << 6)
#define CTRL_TXT_BULK         (2 << 18)
#define CTRL_RXT_BULK         (2 << 2)

static struct usb_endpoint_descriptor ep0_out_desc = {
	.bLength = sizeof(struct usb_endpoint_descriptor),
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = 0,
	.bmAttributes =	USB_ENDPOINT_XFER_CONTROL,
};

static struct usb_endpoint_descriptor ep0_in_desc = {
	.bLength = sizeof(struct usb_endpoint_descriptor),
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = USB_DIR_IN,
	.bmAttributes =	USB_ENDPOINT_XFER_CONTROL,
};

static volatile struct ept_queue_head *epts = 0;
static volatile struct ept_queue_item *items[2* NUM_ENDPOINTS];
static int mmp_pullup(struct usb_gadget *gadget, int is_on);
static int mmp_ep_enable(struct usb_ep *ep,
                               const struct usb_endpoint_descriptor *desc);
static int mmp_ep_disable(struct usb_ep * ep);
static int mmp_ep_queue(struct usb_ep *ep,
                       struct usb_request *req, gfp_t gfp_flags);
static struct usb_request *
mmp_ep_alloc_request(struct usb_ep *ep, unsigned int gfp_flags);
static void mmp_ep_free_request(struct usb_ep *ep, struct usb_request *_req);

static struct usb_gadget_ops mmp_udc_ops = {
	.pullup = mmp_pullup,
};

static struct usb_ep_ops mmp_ep_ops = {
       .enable         = mmp_ep_enable,
       .disable        = mmp_ep_disable,
       .queue          = mmp_ep_queue,
       .alloc_request  = mmp_ep_alloc_request,
       .free_request   = mmp_ep_free_request,
};

static struct mmp_ep ep[NUM_ENDPOINTS];
static struct mmp_udc controller = {
	.gadget = {
		.ep0 = &ep[0].ep,
		.ops = &mmp_udc_ops,
		.name = "mmp_udc",
	},
};

static struct usb_request *
mmp_ep_alloc_request(struct usb_ep *ep, unsigned int gfp_flags)
{
	struct mmp_ep *mmp_ep = container_of(ep, struct mmp_ep, ep);
	return &mmp_ep->req;
}

static void mmp_ep_free_request(struct usb_ep *ep, struct usb_request *_req)
{
	return;
}

static void ep_enable(int num, int in)
{
	struct ept_queue_head *head;
	unsigned n;
	head = epts + 2*num + in;

	n = readl(USB_ENDPTCTRL(num));
	if(in) {
		n |= (CTRL_TXE | CTRL_TXR | CTRL_TXT_BULK);
	} else {
		n |= (CTRL_RXE | CTRL_RXR | CTRL_RXT_BULK);
	}

	if(num != 0)
		head->config = CONFIG_MAX_PKT(EP_MAX_PACKET_SIZE) | CONFIG_ZLT;
	writel(n, USB_ENDPTCTRL(num));
}

static int mmp_ep_enable(struct usb_ep *ep,
                               const struct usb_endpoint_descriptor *desc)
{
	struct mmp_ep *mmp_ep = container_of(ep, struct mmp_ep, ep);
	int num, in;
	num = desc->bEndpointAddress & USB_ENDPOINT_NUMBER_MASK;
	in = (desc->bEndpointAddress & USB_DIR_IN) != 0;
	ep_enable(num, in);
	mmp_ep->desc = desc;
	return 0;
}

static int mmp_ep_disable(struct usb_ep * ep)
{
	return 0;
}

static int mmp_ep_queue(struct usb_ep *ep,
                       struct usb_request *req, gfp_t gfp_flags)
{
	struct mmp_ep *mmp_ep = container_of(ep, struct mmp_ep, ep);
	struct ept_queue_item *item;
	struct ept_queue_head *head;
	unsigned phys;
	int bit, num, len, in;
	num = mmp_ep->desc->bEndpointAddress & USB_ENDPOINT_NUMBER_MASK;
	in = (mmp_ep->desc->bEndpointAddress & USB_DIR_IN) != 0;
	item = items[2 * num + in];
	head = epts + 2 * num + in;
	phys = (unsigned)req->buf;
	len = req->length;

	item->next = TERMINATE;
	item->info = INFO_BYTES(len) | INFO_IOC | INFO_ACTIVE;
	item->page0 = phys;
	item->page1 = (phys & 0xfffff000) + 0x1000;

	head->next = (unsigned) item;
	head->info = 0;

	DBG("ept%d %s queue len %x, buffer %x\n",
			num, in ? "in" : "out", len, phys);

	if(in)
		bit = EPT_TX(num);
	else
		bit  = EPT_RX(num);
	writel(bit, USB_ENDPTPRIME);
	return 0;
}

static void handle_ep_complete(struct mmp_ep *ep)
{
	struct ept_queue_item *item;
	int num, in, len;
	num = ep->desc->bEndpointAddress & USB_ENDPOINT_NUMBER_MASK;
	in = (ep->desc->bEndpointAddress & USB_DIR_IN) != 0;
	if (num == 0)
		ep->desc = &ep0_out_desc;
	item = items[2 * num + in];

	if(item->info & 0xff) {
		printf("EP%d/%s FAIL nfo=%x pg0=%x\n",
				num, in ? "in" : "out", item->info, item->page0);
		ep->req.actual = 0;
		ep->req.status = -1;
	}
	else
	{
		len = (item->info >> 16) & 0x7fff;
		//ep->req.length -= len;
		ep->req.actual = ep->req.length-len;
		ep->req.status = 0;
	}
	DBG("ept%d %s complete %x\n",
			num, in ? "in" : "out", len);
	ep->req.complete(&ep->ep, &ep->req);
	if (num == 0) {
		ep->req.length = 0;
		usb_ep_queue(&ep->ep, &ep->req, 0);
		ep->desc = &ep0_in_desc;
	}
}

#define SETUP(type,request) (((type) << 8) | (request))

static const char *reqname(unsigned r)
{
	switch(r) {
	case USB_REQ_GET_STATUS: return "GET_STATUS";
	case USB_REQ_CLEAR_FEATURE: return "CLEAR_FEATURE";
	case USB_REQ_SET_FEATURE: return "SET_FEATURE";
	case USB_REQ_SET_ADDRESS: return "SET_ADDRESS";
	case USB_REQ_GET_DESCRIPTOR: return "GET_DESCRIPTOR";
	case USB_REQ_SET_DESCRIPTOR: return "SET_DESCRIPTOR";
	case USB_REQ_GET_CONFIGURATION: return "GET_CONFIGURATION";
	case USB_REQ_SET_CONFIGURATION: return "SET_CONFIGURATION";
	case USB_REQ_GET_INTERFACE: return "GET_INTERFACE";
	case USB_REQ_SET_INTERFACE: return "SET_INTERFACE";
	default: return "*UNKNOWN*";
	}
}
static void handle_setup(void)
{
	struct mmp_udc *udc = &controller;
	struct usb_request *req = &ep[0].req;
	struct ept_queue_head *head;
	struct usb_ctrlrequest r;
	int status = 0;
	int num, in;
	char *buf;
	head = epts;

	memcpy(&r, head->setup_data, sizeof(struct usb_ctrlrequest));
	num = r.wIndex & USB_ENDPOINT_NUMBER_MASK;
	in = r.wIndex & USB_DIR_IN;
	writel(EPT_RX(0), USB_ENDPTSETUPSTAT);
	DBG("handle setup %s, %x, %x index %x value %x\n", reqname(r.bRequest), r.bRequestType, r.bRequest, r.wIndex, r.wValue);

	switch(SETUP(r.bRequestType, r.bRequest)) {
	case SETUP(USB_RECIP_DEVICE, USB_REQ_SET_ADDRESS):
		/* write address delayed (will take effect
		 ** after the next IN txn)
		 */
		writel((r.wValue << 25) | (1 << 24), USB_DEVICEADDR);
		req->length = 0;
		usb_ep_queue(udc->gadget.ep0, req, 0);
		return;
	case SETUP(USB_DIR_IN | USB_RECIP_DEVICE, USB_REQ_GET_STATUS):
	req->length = 2;
	buf = (char *)req->buf;
	buf[0] = 1 << USB_DEVICE_SELF_POWERED;
	buf[1] = 0;
	usb_ep_queue(udc->gadget.ep0, req, 0);
	return;
	}
       /* pass request up to the gadget driver */
       if (udc->driver)
               status = udc->driver->setup(&udc->gadget, &r);
       else
               status = -ENODEV;

       if (!status)
	       return;
	DBG("STALL reqname %s type %x value %x, index %x\n",
			reqname(r.bRequest), r.bRequestType, r.wValue, r.wIndex);
stall:
	writel((1<<16) | (1 << 0), USB_ENDPTCTRL(0));
}

void stop_activity()
{
	int i, num, in;
	struct ept_queue_head *head;
	writel(readl(USB_ENDPTCOMPLETE), USB_ENDPTCOMPLETE);
	writel(readl(USB_ENDPTSETUPSTAT), USB_ENDPTSETUPSTAT);
	writel(0xffffffff, USB_ENDPTFLUSH);

	/* error out any pending reqs */
	for (i = 0; i < NUM_ENDPOINTS; i ++) {
		if (i != 0)
			writel(0, USB_ENDPTCTRL(i));
		num = ep[i].desc->bEndpointAddress & USB_ENDPOINT_NUMBER_MASK;
		in = (ep[i].desc->bEndpointAddress & USB_DIR_IN) != 0;
		head = epts + (num * 2) + (in);
		head->info = INFO_ACTIVE;
	}
}

void udc_irq(void)
{
	struct mmp_udc *udc = &controller;
	unsigned n = readl(USB_USBSTS);
	writel(n, USB_USBSTS);
	int bit,i, num, in;

	n &= (STS_SLI | STS_URI | STS_PCI | STS_UI | STS_UEI);
	if (n == 0)
		return;

	if(n & STS_URI) {
		DBG("-- reset --\n");
		stop_activity();
	}
	if(n & STS_SLI) {
		DBG("-- suspend --\n");
	}
	if(n & STS_PCI) {
		DBG("-- portchange --\n");
		unsigned spd = (readl(USB_PORTSC) >> 26) & 3;
		if(spd == 2) {
			udc->gadget.speed = USB_SPEED_HIGH;
			for (i = 1; i < NUM_ENDPOINTS && n; i ++)
				if (ep[i].desc)
					ep[i].ep.maxpacket = 512;
		} else {
			udc->gadget.speed = USB_SPEED_FULL;
		}
	}
	if(n & STS_UEI) printf("<UEI %x>\n", readl(USB_ENDPTCOMPLETE));
	if((n & STS_UI) || (n & STS_UEI)) {
		n = readl(USB_ENDPTSETUPSTAT);
		if(n & EPT_RX(0)) {
			handle_setup();
		}

		n = readl(USB_ENDPTCOMPLETE);
		if(n != 0) {
			writel(n, USB_ENDPTCOMPLETE);
		}

		for (i = 0; i < NUM_ENDPOINTS && n; i ++) {
			if (ep[i].desc) {
				num = ep[i].desc->bEndpointAddress & USB_ENDPOINT_NUMBER_MASK;
				in = (ep[i].desc->bEndpointAddress & USB_DIR_IN) != 0;
				bit = (in) ? EPT_TX(num) : EPT_RX(num);
				if(n & bit)
					handle_ep_complete(&ep[i]);
			}
		}
	}
}

int usb_gadget_handle_interrupts(void)
{
       u32 value;

       value = readl(USB_USBSTS);
       if (value)
               udc_irq();

       return (value);
}

static int mmp_pullup(struct usb_gadget *gadget, int is_on)
{
	if (is_on) {
		/* RESET */
		writel(USBCMD_ITC(0x8) | USBCMD_RST, USB_USBCMD);
		udelay(200);

		writel((unsigned) epts, USB_ENDPOINTLISTADDR);

		/* select DEVICE mode */
		writel(USBMODE_DEVICE, USB_USBMODE);

		writel(0xffffffff, USB_ENDPTFLUSH);

		/* Turn on the USB connection by enabling the pullup resistor */
		writel(USBCMD_ITC(0x8) | USBCMD_RUN, USB_USBCMD);
	}
	else {
		stop_activity();
		writel(USBCMD_FS2, USB_USBCMD);
		udelay(800);
		if (controller.driver)
			controller.driver->disconnect(gadget);
	}
	return 0;
}

void udc_disconnect(void)
{
	/* disable pullup */
	stop_activity();
	writel(0x0008000, USB_USBCMD);
	udelay(800);
	if (controller.driver)
		controller.driver->disconnect(&controller.gadget);
}

static int mmpudc_init = 0;
static int mmpudc_probe()
{
	struct mmp_udc *udc = &controller;
	struct ept_queue_head *head;
	int i;
	if (!mmpudc_init)
		mmpudc_init = 1;
	else
		return 1;
#ifndef CONFIG_RELOC_FIXUP_WORKS
	mmp_udc_ops.pullup = (void *)mmp_udc_ops.pullup + gd->reloc_off;
	udc->gadget.ops = &mmp_udc_ops;
	udc->gadget.ep0 = (void *)udc->gadget.ep0 + gd->reloc_off;
	udc->gadget.name += gd->reloc_off;

	mmp_ep_ops.enable = (void *)mmp_ep_ops.enable + gd->reloc_off;
	mmp_ep_ops.disable = (void *)mmp_ep_ops.disable + gd->reloc_off;
	mmp_ep_ops.queue = (void *)mmp_ep_ops.queue + gd->reloc_off;
	mmp_ep_ops.alloc_request = (void *)mmp_ep_ops.alloc_request + gd->reloc_off;
	mmp_ep_ops.free_request = (void *)mmp_ep_ops.free_request + gd->reloc_off;
#endif
	epts = memalign(4096, 4096);
	memset(epts, 0, 32 * sizeof(struct ept_queue_head));
	for (i = 0; i < NUM_ENDPOINTS; i ++) {
		/* For item0 and item1, they are served as ep0 out&in seperately */
		head = epts + i;
		if (i < 2)
			head->config = CONFIG_MAX_PKT(EP0_MAX_PACKET_SIZE) | CONFIG_ZLT | CONFIG_IOS;
		else
			head->config = CONFIG_MAX_PKT(EP_MAX_PACKET_SIZE) | CONFIG_ZLT;

		items[i] = memalign(2 * sizeof(struct ept_queue_item), sizeof(struct ept_queue_item));
	}

	INIT_LIST_HEAD(&udc->gadget.ep_list);
	ep[0].ep.maxpacket = 64;
	ep[0].ep.name = "ep0";
	ep[0].desc = &ep0_in_desc;
	INIT_LIST_HEAD(&udc->gadget.ep0->ep_list);
	for (i = 0; i < NUM_ENDPOINTS; i ++) {
		if (i != 0) {
			ep[i].ep.maxpacket = 512;
			ep[i].ep.name = "ep-";
			list_add_tail(&ep[i].ep.ep_list, &udc->gadget.ep_list);
			ep[i].desc = NULL;
		}
		ep[i].ep.ops = &mmp_ep_ops;
	}
	return 0;
}

int usb_gadget_register_driver (struct usb_gadget_driver *driver)
{
	struct mmp_udc *udc = &controller;
	int             retval;
	unsigned value;

	if (!driver
			|| driver->speed < USB_SPEED_FULL
			|| !driver->bind
			|| !driver->setup) {
		DBG("bad parameter.\n");
		return -EINVAL;
	}

	if (!mmpudc_probe())
		usb_lowlevel_init();
	retval = driver->bind(&udc->gadget);
	if (retval) {
		DBG("driver->bind() returned %d\n", retval);
		return retval;
	}
	udc->driver = driver;

	return 0;
}
