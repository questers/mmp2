/*
 * Copyright (C) 2008 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Copyright (c) 2010 Marvell Inc.
 * Modified by Lei Wen <leiwen@marvell.com>
 */
#include <common.h>
#include <asm/errno.h>
#include <linux/usb/ch9.h>
#include <linux/usb/cdc.h>
#include <linux/usb/gadget.h>
#include "gadget_chips.h"
#include <fastboot/fastboot.h>
DECLARE_GLOBAL_DATA_PTR;

#ifdef CONFIG_USB_GADGET_DUALSPEED
#define DEVSPEED	USB_SPEED_HIGH
#else
#define DEVSPEED	USB_SPEED_FULL
#endif

struct fb_dev {
	struct usb_gadget	*gadget;
	struct usb_request	*req;		/* for control responses */
	u8			config;
	struct usb_ep		*in_ep, *out_ep;
	const struct usb_endpoint_descriptor
				*in, *out;
	struct usb_request	*tx_req, *rx_req;

	fastboot_cmd_handler handler;
};

static u8 control_req[512];
static struct fb_dev l_fbdev;

static unsigned fb_packet_sent,fb_packet_recv;
static void tx_complete(struct usb_ep *ep, struct usb_request *req)
{
	fb_packet_sent = 1;
}

void fb_tx_status(const char *status)
{
	struct fb_dev *dev = &l_fbdev;
	struct usb_request *req = dev->tx_req;
	int len = strlen(status);
	req->length = len;
	req->buf = (void *)status;
	req->context = NULL;
	req->complete = tx_complete;
	fb_packet_sent = 0;
	usb_ep_queue(dev->in_ep, req, 0);
	while (!fb_packet_sent)
		usb_gadget_handle_interrupts();

}

static struct usb_device_descriptor device_desc = {
	.bLength = sizeof(struct usb_device_descriptor),
	.bDescriptorType =	USB_DT_DEVICE,
	.bcdUSB =		__constant_cpu_to_le16(0x0200),

	.idVendor =		__constant_cpu_to_le16(CONFIG_USBD_VENDORID),
	.idProduct = 		__constant_cpu_to_le16(CONFIG_USBD_PRODUCTID),
	.iManufacturer =	0x1,
	.iProduct =		0x2,
	.iSerialNumber =	0x3,
	.bNumConfigurations =	1
};

static struct usb_config_descriptor fb_config = {
	.bLength = sizeof(struct usb_config_descriptor),
	.bDescriptorType = USB_DT_CONFIG,

	.bNumInterfaces = 1,
	.bConfigurationValue = 1,
	.iConfiguration = 4,
	.bmAttributes = USB_CONFIG_ATT_ONE,
	.bMaxPower = 0x80
};

static struct usb_interface_descriptor control_intf = {
	.bLength  = sizeof(struct usb_interface_descriptor),
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = 0,
	.bNumEndpoints = 0x2,
	.bInterfaceClass = USB_CLASS_VENDOR_SPEC,
	.bInterfaceSubClass = 0x42,
	.bInterfaceProtocol = USB_CLASS_HID,
};

static struct usb_endpoint_descriptor fb_source_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,

	.bEndpointAddress = USB_DIR_IN,
	.bmAttributes =	USB_ENDPOINT_XFER_BULK,
};

static struct usb_endpoint_descriptor fb_sink_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,

	.bEndpointAddress = USB_DIR_OUT,
	.bmAttributes =	USB_ENDPOINT_XFER_BULK,
};

static const struct usb_descriptor_header *fb_function[4] = {
	(struct usb_descriptor_header *)&control_intf,
	(struct usb_descriptor_header *)&fb_source_desc,
	(struct usb_descriptor_header *)&fb_sink_desc,
	NULL,
};

static struct usb_qualifier_descriptor dev_qualifier = {
	.bLength =		sizeof dev_qualifier,
	.bDescriptorType =	USB_DT_DEVICE_QUALIFIER,

	.bcdUSB =		__constant_cpu_to_le16(0x0200),
	.bDeviceClass =		USB_CLASS_VENDOR_SPEC,
	.bMaxPacketSize0 =	64,

	.bNumConfigurations =	1,
};

static struct usb_string strings[] = {
	{ 1,	CONFIG_USBD_MANUFACTURER, },
	{ 2,	CONFIG_USBD_PRODUCT_NAME, },
	{ 3,	CONFIG_SERIAL_NUM, },
	{ 4,	CONFIG_USBD_CONFIGURATION_STR, },
	{0, NULL},
};

static struct usb_gadget_strings	stringtab = {
	.language	= 0x0409,	/* en-us */
	.strings = strings,
};

static void fb_setup_complete(struct usb_ep *ep, struct usb_request *req)
{
}

static int fb_bind(struct usb_gadget *gadget)
{
	struct fb_dev *dev = &l_fbdev;
	struct usb_ep	*in_ep, *out_ep;
	int gcnum;
	gcnum = usb_gadget_controller_number(gadget);
	if (gcnum >= 0)
		device_desc.bcdDevice = cpu_to_le16(0x0300 + gcnum);
	else {
		/*
		 * can't assume CDC works.  don't want to default to
		 * anything less functional on CDC-capable hardware,
		 * so we fail in this case.
		 */
		error("controller '%s' not recognized",
			gadget->name);
		return -ENODEV;
	}

	/* all we really need is bulk IN/OUT */
	usb_ep_autoconfig_reset(gadget);
	in_ep = usb_ep_autoconfig(gadget, &fb_source_desc);
	if (!in_ep) {
		error("can't autoconfigure on %s\n",
			gadget->name);
		return -ENODEV;
	}
	in_ep->driver_data = in_ep;	/* claim */

	out_ep = usb_ep_autoconfig(gadget, &fb_sink_desc);
	if (!out_ep) {
		error("can't autoconfigure on %s\n",
			gadget->name);
		return -ENODEV;
	}
#ifdef CONFIG_USB_GADGET_DUALSPEED
	fb_sink_desc.wMaxPacketSize = fb_source_desc.wMaxPacketSize = __constant_cpu_to_le16(512);
#endif
	out_ep->driver_data = out_ep;	/* claim */
	device_desc.bMaxPacketSize0 = gadget->ep0->maxpacket;
	usb_gadget_set_selfpowered(gadget);
	dev->in_ep = in_ep;
	dev->out_ep = out_ep;

	dev->gadget = gadget;
	set_gadget_data(gadget, dev);
	gadget->ep0->driver_data = dev;
	dev->req = usb_ep_alloc_request(gadget->ep0, 0);
	dev->req->buf = control_req;
	dev->req->complete = fb_setup_complete;
	dev->tx_req = usb_ep_alloc_request(dev->in_ep, 0);
	dev->rx_req = usb_ep_alloc_request(dev->out_ep, 0);
	return 0;
}

static void fb_unbind(struct usb_gadget *gadget)
{
	return;
}

static void rx_complete(struct usb_ep *ep, struct usb_request *req)
{
	fb_packet_recv=1;
}

static int
fb_setup(struct usb_gadget *gadget, const struct usb_ctrlrequest *ctrl)
{
	struct fb_dev		*dev = get_gadget_data(gadget);
	struct usb_request	*req = dev->req;
	int			value = -EOPNOTSUPP;
	u16			wIndex = le16_to_cpu(ctrl->wIndex);
	u16			wValue = le16_to_cpu(ctrl->wValue);
	u16			wLength = le16_to_cpu(ctrl->wLength);

	switch (ctrl->bRequest) {
	case USB_REQ_GET_DESCRIPTOR:
		if (ctrl->bRequestType != USB_DIR_IN)
			break;
		switch (wValue >> 8) {

		case USB_DT_DEVICE:
			value = min(wLength, (u16) sizeof device_desc);
			memcpy(req->buf, &device_desc, value);
			break;
		case USB_DT_CONFIG:
			value  = usb_gadget_config_buf(&fb_config, req->buf, 512, fb_function);
			if (value >= 0)
				value = min(wLength, (u16) value);
			break;

		case USB_DT_STRING:
			value = usb_gadget_get_string(&stringtab,
					wValue & 0xff, req->buf);

			if (value >= 0)
				value = min(wLength, (u16) value);

			break;
		case USB_DT_DEVICE_QUALIFIER:
			value = min(wLength, (u16) sizeof dev_qualifier);
			memcpy(req->buf, &dev_qualifier, value);
			break;
		}
		break;
	case USB_REQ_SET_CONFIGURATION:
		dev->out = &fb_sink_desc;
		dev->in = &fb_source_desc;
		dev->rx_req->length = FASTBOOT_RECV_BUF_SIZE;
		dev->rx_req->complete = rx_complete;
		usb_ep_enable(dev->out_ep, dev->out);
		usb_ep_enable(dev->in_ep, dev->in);
		value = usb_ep_queue(dev->out_ep, dev->rx_req, 0);
		if (value)
			error("rx submit --> %d", value);
		value = 0;
		break;
	}

	/* respond with data transfer before status phase? */
	if (value >= 0) {
		req->length = value;
		req->zero = value < wLength
				&& (value % gadget->ep0->maxpacket) == 0;
		value = usb_ep_queue(gadget->ep0, req, 0);
		if (value < 0) {
			debug("ep_queue --> %d\n", value);
			req->status = 0;
		}
	}

	/* host either stalls (value < 0) or reports success */
	return value;
}

static void fb_disconnect(struct usb_gadget *gadget)
{
	return;
}

void fb_halt(void)
{
	struct fb_dev *dev = &l_fbdev;
	struct usb_gadget *gadget = dev->gadget;
	usb_gadget_disconnect(gadget);
}

static struct usb_gadget_driver fb_driver = {
	.speed		= DEVSPEED,

	.bind		= fb_bind,
	.unbind		= fb_unbind,

	.setup		= fb_setup,
	.disconnect	= fb_disconnect,
};


void fb_init(fastboot_cmd_handler handler)
{
	struct fb_dev *dev = &l_fbdev;
	struct usb_gadget *gadget;
	#ifndef CONFIG_RELOC_FIXUP_WORKS
	if (fb_driver.speed < gd->reloc_off) {
		int i;
		fb_driver.speed += gd->reloc_off;
		fb_driver.bind += gd->reloc_off;
		fb_driver.unbind += gd->reloc_off;
		fb_driver.setup += gd->reloc_off;
		fb_driver.disconnect += gd->reloc_off;
		for (i = 0; i < 4 && fb_function[i]; i ++)
			fb_function[i] = (void *)fb_function[i] + gd->reloc_off;
		stringtab.strings = (void *)stringtab.strings + gd->reloc_off;
		for (i = 0; i < ARRAY_SIZE(strings); i ++)
			strings[i].s += gd->reloc_off;
	}
	#endif

	usb_gadget_register_driver(&fb_driver);
	gadget = dev->gadget;
	usb_gadget_connect(dev->gadget);
	
	if(handler)	dev->handler = handler;

}

void fb_run(void)
{
	usb_gadget_handle_interrupts();
	if (fb_packet_recv) {
		l_fbdev.handler(l_fbdev.rx_req->buf,l_fbdev.rx_req->actual);
		fb_packet_recv = 0;
		
		//submit later request
		l_fbdev.rx_req->length = FASTBOOT_RECV_BUF_SIZE;
		l_fbdev.rx_req->complete = rx_complete;
		usb_ep_queue(l_fbdev.out_ep, l_fbdev.rx_req, 0);
	}
}

int fb_get_rcv_len(void)
{
	return l_fbdev.rx_req->actual;
}


void *fb_get_buf(void)
{
	return l_fbdev.rx_req->buf;
}

void fb_set_buf (void *buf)
{
	l_fbdev.rx_req->buf = buf;
}

