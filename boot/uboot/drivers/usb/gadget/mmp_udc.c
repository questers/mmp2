/*
 * Copyright (c) 2008, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Copyright (c) 2010 Marvell Inc.
 * Modified by Lei Wen <leiwen@marvell.com>
 */
#include <common.h>
#include <command.h>
#include <asm/io.h>
#include <linux/types.h>
#include <asm/errno.h>
#include <asm/io.h>
#include <common.h>
#include <config.h>
#include <net.h>
#include <command.h>
#include <malloc.h>
#include <usbdevice.h>
#include <usb/mmp_udc.h>
#if 1
#define DBG(x...) do {} while(0)
#else
#define DBG(x...) printf(x)
#endif

#define USBMODE_DEVICE 2

struct ept_queue_head
{
    unsigned config;
    unsigned current; /* read-only */

    unsigned next;
    unsigned info;
    unsigned page0;
    unsigned page1;
    unsigned page2;
    unsigned page3;
    unsigned page4;
    unsigned reserved_0;

    unsigned char setup_data[8];

    unsigned reserved_1;
    unsigned reserved_2;
    unsigned reserved_3;
    unsigned reserved_4;
};

#define CONFIG_MAX_PKT(n)     ((n) << 16)
#define CONFIG_ZLT            (1 << 29)    /* stop on zero-len xfer */
#define CONFIG_IOS            (1 << 15)    /* IRQ on setup */

struct ept_queue_item
{
    unsigned next;
    unsigned info;
    unsigned page0;
    unsigned page1;
    unsigned page2;
    unsigned page3;
    unsigned page4;
    unsigned reserved;
};

#define TERMINATE 1

#define INFO_BYTES(n)         ((n) << 16)
#define INFO_IOC              (1 << 15)
#define INFO_ACTIVE           (1 << 7)
#define INFO_HALTED           (1 << 6)
#define INFO_BUFFER_ERROR     (1 << 5)
#define INFO_TX_ERROR         (1 << 3)


#define STS_SLI               (1 << 8)   /* R/WC - suspend state entered */
#define STS_SRI               (1 << 7)   /* R/WC - SOF recv'd */
#define STS_URI               (1 << 6)   /* R/WC - RESET recv'd - write to clear */
#define STS_FRI               (1 << 3)   /* R/WC - Frame List Rollover */
#define STS_PCI               (1 << 2)   /* R/WC - Port Change Detect */
#define STS_UEI               (1 << 1)   /* R/WC - USB Error */
#define STS_UI                (1 << 0)   /* R/WC - USB Transaction Complete */


/* bits used in all the endpoint status registers */
#define EPT_TX(n) (1 << ((n) + 16))
#define EPT_RX(n) (1 << (n))


#define CTRL_TXE              (1 << 23)
#define CTRL_TXR              (1 << 22)
#define CTRL_RXE              (1 << 7)
#define CTRL_RXR              (1 << 6)
#define CTRL_TXT_BULK         (2 << 18)
#define CTRL_RXT_BULK         (2 << 2)

struct ept_queue_head *epts = 0;

static int usb_highspeed = 0;
static struct usb_device_instance *udc_device;
static struct usb_endpoint_instance *ep0;

void udc_setup_ep(struct usb_device_instance *device, unsigned int id,
				struct usb_endpoint_instance *endpoint)
{
	struct ept_queue_head *head;
	struct urb *urb;
	void *item;
	unsigned cfg;
	int in, num;

	num = endpoint->endpoint_address & USB_ENDPOINT_NUMBER_MASK;
	if (num == 0)
		cfg = CONFIG_MAX_PKT(EP0_MAX_PACKET_SIZE) | CONFIG_ZLT;
	else
		cfg = CONFIG_MAX_PKT(EP_MAX_PACKET_SIZE) | CONFIG_ZLT;
	in = !!(endpoint->endpoint_address & USB_DIR_IN);
	item = memalign(2 * sizeof(struct ept_queue_item), sizeof(struct ept_queue_item));
	memset(item, 0, sizeof(struct ept_queue_item));

	if(num == 0) {
		cfg |= CONFIG_IOS;
		ep0 = endpoint;
		ep0->tx_urb = usbd_alloc_urb(device, ep0);
		ep0->rcv_urb = usbd_alloc_urb(device, ep0);
		ep0->rcv_urb->buffer_length = 0;
		head = epts;
		head->config = cfg;
		head = epts + 1;
		head->config = cfg;
		ep0->rcv_urb->data = item;
		ep0->tx_urb->data = item;
		return;
	}

	if (!in)
		endpoint->rcv_urb->buffer_length = 0;
	head = epts + (2* num + in);
	head->config = cfg;
	urb = (in) ? endpoint->tx_urb : endpoint->rcv_urb;
	urb->data = item;

	printf("ept%d %s @%p/%p \n",
			num, in ? "in":"out", endpoint, head);
}

static void endpoint_enable(struct usb_endpoint_instance *ept, unsigned yes)
{
    unsigned n;
    struct ept_queue_head *head;
    int num, in;
    num = ept->endpoint_address & USB_ENDPOINT_NUMBER_MASK;
    in = !!(ept->endpoint_address & USB_DIR_IN);
    head = epts + 2*num + in;

    n = readl(USB_ENDPTCTRL(num));
    if(yes) {
        if(in) {
            n |= (CTRL_TXE | CTRL_TXR | CTRL_TXT_BULK);
        } else {
            n |= (CTRL_RXE | CTRL_RXR | CTRL_RXT_BULK);
        }

        if(num != 0) {
                /* XXX should be more dynamic... */
            if(usb_highspeed) {
                head->config = CONFIG_MAX_PKT(EP_MAX_PACKET_SIZE) | CONFIG_ZLT;
            } else {
                head->config = CONFIG_MAX_PKT(EP0_MAX_PACKET_SIZE) | CONFIG_ZLT;
            }
        }
    }
    writel(n, USB_ENDPTCTRL(num));
}

int usb_queue_req(struct usb_endpoint_instance *ept, int in)
{
    struct ept_queue_item *item;
    unsigned phys;
    struct ept_queue_head *head;
    struct urb *urb;
    int bit;
    int num, len;
    num = ept->endpoint_address & USB_ENDPOINT_NUMBER_MASK;
    urb = (in) ? ept->tx_urb : ept->rcv_urb;
    item = urb->data;
    phys = (unsigned)urb->buffer;
    len = (in) ? urb->actual_length : urb->buffer_length;
    head = epts + 2 * num + in;

    item->next = TERMINATE;
    item->info = INFO_BYTES(len) | INFO_IOC | INFO_ACTIVE;
    item->page0 = phys;
    item->page1 = (phys & 0xfffff000) + 0x1000;

    head->next = (unsigned) item;
    head->info = 0;

    DBG("ept%d %s queue len %x, buffer %x\n",
            num, in ? "in" : "out", len, phys);

    if(in)
	    bit = EPT_TX(num);
    else
	    bit  = EPT_RX(num);
    writel(bit, USB_ENDPTPRIME);
    return 0;
}

static void handle_ept_complete(struct usb_endpoint_instance *ept, int tt)
{
    struct ept_queue_item *item;
    struct urb *urb;
    unsigned actual;
    int status;
    int num, in, len;
    num = ept->endpoint_address & USB_ENDPOINT_NUMBER_MASK;
    in = !!(ept->endpoint_address & USB_DIR_IN);
    urb = (in) ? ept->tx_urb : ept->rcv_urb;

    item = urb->data;

    if(item->info & 0xff) {
	    urb->actual_length = 0;
	    status = -1;
	    printf("EP%d/%s FAIL nfo=%x pg0=%x\n",
			    num, in ? "in" : "out", item->info, item->page0);
    } else {
	    status = 0;
    }
    len = (item->info >> 16) & 0x7fff;
    DBG("ept%d %s complete %x\n",
		    num, in ? "in" : "out", len);
    if (num == 0) {
	    if (ep0->tx_urb->actual_length > 0)
		    usb_queue_req(ep0, 0);
	    ep0->tx_urb->actual_length = 0;
	    ep0->rcv_urb->actual_length = 0;
	    return;
    }
    if (tt)
	    urb->actual_length = (in)? len :(urb->buffer_length - len);
    else
	    urb->actual_length = 0;

    if (!in)
	    urb->buffer_length = 0;
}

static void setup_tx(void *buf, unsigned len)
{
	struct usb_bus_instance *bus = udc_device->bus;
	struct usb_endpoint_instance *ept = &bus->endpoint_array[0];
	DBG("setup_tx %p %d\n", buf, len);
	if (len > 0)
		memcpy(ept->tx_urb->buffer, buf, len);
	ept->tx_urb->actual_length = len;
	usb_queue_req(ept, 1);
}

#define SETUP(type,request) (((type) << 8) | (request))
static const char *reqname(unsigned r)
{
    switch(r) {
    case USB_REQ_GET_STATUS: return "GET_STATUS";
    case USB_REQ_CLEAR_FEATURE: return "CLEAR_FEATURE";
    case USB_REQ_SET_FEATURE: return "SET_FEATURE";
    case USB_REQ_SET_ADDRESS: return "SET_ADDRESS";
    case USB_REQ_GET_DESCRIPTOR: return "GET_DESCRIPTOR";
    case USB_REQ_SET_DESCRIPTOR: return "SET_DESCRIPTOR";
    case USB_REQ_GET_CONFIGURATION: return "GET_CONFIGURATION";
    case USB_REQ_SET_CONFIGURATION: return "SET_CONFIGURATION";
    case USB_REQ_GET_INTERFACE: return "GET_INTERFACE";
    case USB_REQ_SET_INTERFACE: return "SET_INTERFACE";
    default: return "*UNKNOWN*";
    }
}

static void handle_setup(void)
{
	struct usb_bus_instance *bus = udc_device->bus;
	struct usb_device_request *urq;
	struct usb_endpoint_instance *ept;
	struct ept_queue_head *head;
	int num, in;
	head = epts;
	urq = &ep0->tx_urb->device_request;

	memcpy(urq, head->setup_data, sizeof(*urq));
	writel(EPT_RX(0), USB_ENDPTSETUPSTAT);

	DBG("handle_setup type %x, req %x, (%s)\n", urq->bmRequestType, urq->bRequest, reqname(urq->bRequest));
	if (!ep0_recv_setup(ep0->tx_urb)) {
		switch(SETUP(urq->bmRequestType, urq->bRequest)) {
		case SETUP(USB_RECIP_DEVICE, USB_REQ_SET_CONFIGURATION):
			if(urq->wValue == 1) {
				int i;
				/* enable endpoints */
				for(i =1; i < 3; i ++){
					ept = &bus->endpoint_array[i];
					endpoint_enable(ept, urq->wValue);
					if (!(ept->endpoint_address & USB_DIR_IN))
						usb_queue_req(ept, !!(ept->endpoint_address & USB_DIR_IN));
				}
				usbd_device_event_irq(udc_device, DEVICE_CONFIGURED, 0);
			} else
				writel(0, USB_ENDPTCTRL(1));

			setup_tx(0, 0);
			//        usb_status(s.value ? 1 : 0, usb_highspeed);
			return;
		case SETUP(USB_RECIP_DEVICE, USB_REQ_SET_ADDRESS):
			/* write address delayed (will take effect
			 ** after the next IN txn)
			 */
			writel((urq->wValue << 25) | (1 << 24), USB_DEVICEADDR);
			setup_tx(0, 0);
			return;
		case SETUP(USB_RECIP_INTERFACE, USB_REQ_SET_INTERFACE):
			goto stall;
		}
		if (ep0->tx_urb->actual_length) {
			usb_queue_req(ep0, 1);
		}
		return;
	}
	switch(SETUP(urq->bmRequestType, urq->bRequest)) {
	case SETUP(USB_RECIP_ENDPOINT, USB_REQ_CLEAR_FEATURE):
		{
		/* enable endpoints */
		unsigned _num = urq->wIndex & 15;
		unsigned _in = !!(urq->wIndex & 0x80);
		int i;

		if((urq->wValue == 0) && (urq->wLength == 0)) {
			for(i =0; i < 4; i ++){
				num = i;
				in = (i % 2) ? 1 : 0;
				if((num == _num) && (in == _in)) {
					endpoint_enable(&bus->endpoint_array[i], 1);
					setup_tx(0, 0);
					return;
				}
			}
		}
		}
	deafult:
		usberr("Invalid setup packet");
	}

stall:
	writel((1<<16) | (1 << 0), USB_ENDPTCTRL(0));
}

/* Connect the USB device to the bus */
void udc_connect(void)
{
	printf("connect\n");
	/* Turn on the USB connection by enabling the pullup resistor */
	writel(0x00080001, USB_USBCMD);
}

/* Allow udc code to do any additional startup */
void udc_startup_events(struct usb_device_instance *device)
{
	printf("startup\n");
	/* The DEVICE_INIT event puts the USB device in the state STATE_INIT */
	usbd_device_event_irq(device, DEVICE_INIT, 0);

	/* The DEVICE_CREATE event puts the USB device in the state
	 * STATE_ATTACHED */
	usbd_device_event_irq(device, DEVICE_CREATE, 0);

	/* Some USB controller driver implementations signal
	 * DEVICE_HUB_CONFIGURED and DEVICE_RESET events here.
	 * DEVICE_HUB_CONFIGURED causes a transition to the state
	 * STATE_POWERED, and DEVICE_RESET causes a transition to
	 * the state STATE_DEFAULT.
	 */


	/* RESET */
	writel(0x00080002, USB_USBCMD);
	udelay(200);

	writel((unsigned) epts, USB_ENDPOINTLISTADDR);

	/* select DEVICE mode */
	writel(USBMODE_DEVICE, USB_USBMODE);

	writel(0xffffffff, USB_ENDPTFLUSH);

	udc_device = device;
}

int udc_init(void)
{
	epts = memalign(4096, 4096);
	memset(epts, 0, 32 * sizeof(struct ept_queue_head));

	return usb_lowlevel_init();
}

void udc_disconnect(void)
{
        /* disable pullup */
    writel(0x0008000, USB_USBCMD);
    udelay(800);
}

void udc_irq(void)
{
	struct usb_bus_instance *bus = udc_device->bus;
	struct usb_endpoint_instance *ept;
	struct urb *urb;
	unsigned n = readl(USB_USBSTS);
	writel(n, USB_USBSTS);
	struct ept_queue_head *head;
	int bit,i, num, in;

	n &= (STS_SLI | STS_URI | STS_PCI | STS_UI | STS_UEI);
	if (n == 0)
		return;

	if(n & STS_URI) {
		writel(readl(USB_ENDPTCOMPLETE), USB_ENDPTCOMPLETE);
		writel(readl(USB_ENDPTSETUPSTAT), USB_ENDPTSETUPSTAT);
		writel(0xffffffff, USB_ENDPTFLUSH);
		writel(0, USB_ENDPTCTRL(1));
		DBG("-- reset --\n");

		/* error out any pending reqs */
		for (i = 0; i < 4; i ++) {
			num = i /2;
			in = i - (num * 2);
			head = epts + (num * 2) + (in);
			head->info = INFO_ACTIVE;
			ept = &bus->endpoint_array[num];
			handle_ept_complete(ept, 0);
		}
		usbd_device_event_irq(udc_device, DEVICE_RESET, 0);
	}
	if(n & STS_SLI) {
		DBG("-- suspend --\n");
	}
	if(n & STS_PCI) {
		DBG("-- portchange --\n");
		unsigned spd = (readl(USB_PORTSC) >> 26) & 3;
		if(spd == 2) {
			usb_highspeed = 1;
		} else {
			usb_highspeed = 0;
		}
	}
	if(n & STS_UEI) printf("<UEI %x>\n", readl(USB_ENDPTCOMPLETE));
	if((n & STS_UI) || (n & STS_UEI)) {
		n = readl(USB_ENDPTSETUPSTAT);
		if(n & EPT_RX(0)) {
			handle_setup();
		}

		n = readl(USB_ENDPTCOMPLETE);
		if(n != 0) {
			writel(n, USB_ENDPTCOMPLETE);
		}

		for (i = 0; i < 6 && n; i ++) {
			num = i /2;
			in = i - (num * 2);
			if(in)
				bit = EPT_TX(num);
			else
				bit  = EPT_RX(num);
			if(n & bit) {
				ept = &bus->endpoint_array[num];
				handle_ept_complete(ept, 1);
			}
		}
	}
}

/*
 * udc_set_nak
 *
 * Allow upper layers to signal lower layers should not accept more RX data
 */
void udc_set_nak(int ep_num)
{
	/* TODO */
}

/*
 * udc_unset_nak
 *
 * Suspend sending of NAK tokens for DATA OUT tokens on a given endpoint.
 * Switch off NAKing on this endpoint to accept more data output from host.
 */
void udc_unset_nak(int ep_num)
{
	/* TODO */
}

/*
 * This should be considered as a sync api
 * Send all data before this return
 */
int udc_endpoint_write(struct usb_endpoint_instance *ept)
{
	struct usb_bus_instance *bus = udc_device->bus;
	int i;

	for(i =1; i < 3; i ++){
		ept = &bus->endpoint_array[i];
		if (ept->endpoint_address & USB_DIR_IN) {
			if (ept->tx_urb->actual_length > 0) {
				usb_queue_req(ept, !!(ept->endpoint_address & USB_DIR_IN));
				while (ept->tx_urb->actual_length >0)
					udc_irq();
			}
		}
		else {
			if (!ept->rcv_urb->buffer_length && !ept->rcv_urb->actual_length) {
				ept->rcv_urb->buffer_length = EP_MAX_PACKET_SIZE;
				usb_queue_req(ept, !!(ept->endpoint_address & USB_DIR_IN));
			}
		}
	}
	return 0;
}
