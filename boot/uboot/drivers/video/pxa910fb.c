/*
 * linux/drivers/video/pxa910fb.c -- Marvell PXA910 LCD Controller
 *
 *  Copyright (C) 2008 Marvell International Ltd.
 *  All rights reserved.
 *
 *  2009-02-16  adapted from original version for PXA910
 *		Green Wan <gwan@marvell.com>
 *              Jun Nie <njun@marvell.com>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

#include <config.h>
#include <div64.h>
#include <malloc.h>
#include <linux/types.h>
#include <asm/io.h>
#include <common.h>
#include <asm/io.h>
#include <pxa910fb.h>

#include "pxa910fb.h"

static struct pxa910fb_info pxa_fb_info;

#define DEFAULT_REFRESH		60	/* Hz */

/* Compatibility mode global switch .....
 *
 * This is a secret switch for user space programs that may want to
 * select color spaces and set resolutions the same as legacy PXA display
 * drivers. The switch is set and unset by setting a specific value in the
 * var_screeninfo.nonstd variable.
 *
 * To turn on compatibility with older PXA, set the MSB of nonstd to 0xAA.
 * To turn off compatibility with older PXA, set the MSB of nonstd to 0x55.
 */

static void set_pix_fmt(struct fb_var_screeninfo *var, int pix_fmt)
{
	switch (pix_fmt) {
	case PIX_FMT_RGB565:
		var->bits_per_pixel = 16;
		var->red.offset = 11;
		var->red.length = 5;
		var->green.offset = 5;
		var->green.length = 6;
		var->blue.offset = 0;
		var->blue.length = 5;
		var->transp.offset = 0;
		var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		break;
	case PIX_FMT_BGR565:
		var->bits_per_pixel = 16;
		var->red.offset = 0;
		var->red.length = 5;
		var->green.offset = 5;
		var->green.length = 6;
		var->blue.offset = 11;
		var->blue.length = 5;
		var->transp.offset = 0;
		var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		break;
	case PIX_FMT_RGB1555:
		var->bits_per_pixel = 16;
		var->red.offset = 10;
		var->red.length = 5;
		var->green.offset = 5;
		var->green.length = 5;
		var->blue.offset = 0;
		var->blue.length = 5;
		var->transp.offset = 15;
		var->transp.length = 1;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 5 << 20;
		break;
	case PIX_FMT_BGR1555:
		var->bits_per_pixel = 16;
		var->red.offset = 0;
		var->red.length = 5;
		var->green.offset = 5;
		var->green.length = 5;
		var->blue.offset = 10;
		var->blue.length = 5;
		var->transp.offset = 15;
		var->transp.length = 1;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 5 << 20;
		break;
	case PIX_FMT_RGB888PACK:
		var->bits_per_pixel = 24;
		var->red.offset = 16;
		var->red.length = 8;
		var->green.offset = 8;
		var->green.length = 8;
		var->blue.offset = 0;
		var->blue.length = 8;
		var->transp.offset = 0;
		var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 6 << 20;
		break;
	case PIX_FMT_BGR888PACK:
		var->bits_per_pixel = 24;
		var->red.offset = 0;
		var->red.length = 8;
		var->green.offset = 8;
		var->green.length = 8;
		var->blue.offset = 16;
		var->blue.length = 8;
		var->transp.offset = 0;
		var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 6 << 20;
		break;
	case PIX_FMT_RGB888UNPACK:
		var->bits_per_pixel = 32;
		var->red.offset = 16;
		var->red.length = 8;
		var->green.offset = 8;
		var->green.length = 8;
		var->blue.offset = 0;
		var->blue.length = 8;
		var->transp.offset = 0;
		var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 7 << 20;
		break;
	case PIX_FMT_BGR888UNPACK:
		var->bits_per_pixel = 32;
		var->red.offset = 0;
		var->red.length = 8;
		var->green.offset = 8;
		var->green.length = 8;
		var->blue.offset = 16;
		var->blue.length = 8;
		var->transp.offset = 0;
		var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 7 << 20;
		break;
	case PIX_FMT_RGBA888:
		var->bits_per_pixel = 32;
		var->red.offset = 16;
		var->red.length = 8;
		var->green.offset = 8;
		var->green.length = 8;
		var->blue.offset = 0;
		var->blue.length = 8;
		var->transp.offset = 24;
		var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 8 << 20;
		break;
	case PIX_FMT_BGRA888:
		var->bits_per_pixel = 32;
		var->red.offset = 0;
		var->red.length = 8;
		var->green.offset = 8;
		var->green.length = 8;
		var->blue.offset = 16;
		var->blue.length = 8;
		var->transp.offset = 24;
		var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 8 << 20;
		break;
	case PIX_FMT_YUYV422PACK:
		var->bits_per_pixel = 16;
		var->red.offset = 8;
		var->red.length = 16;
		var->green.offset = 4;
		var->green.length = 16;
		var->blue.offset = 0;
		var->blue.length = 16;
		var->transp.offset = 0;
		var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 9 << 20;
		break;
	case PIX_FMT_YVU422PACK:
		var->bits_per_pixel = 16;
		var->red.offset = 0;
		var->red.length = 16;
		var->green.offset = 8;
		var->green.length = 16;
		var->blue.offset = 12;
		var->blue.length = 16;
		var->transp.offset = 0;
		var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 9 << 20;
		break;
	case PIX_FMT_YUV422PACK:
		var->bits_per_pixel = 16;
		var->red.offset = 4;
		var->red.length = 16;
		var->green.offset = 12;
		var->green.length = 16;
		var->blue.offset = 0;
		var->blue.length = 16;
		var->transp.offset = 0;
		var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 9 << 20;
		break;

	case PIX_FMT_PSEUDOCOLOR:
		var->bits_per_pixel = 8;
		var->red.offset = 0;
		var->red.length = 8;
		var->green.offset = 0;
		var->green.length = 8;
		var->blue.offset = 0;
		var->blue.length = 8;
		var->transp.offset = 0;
		var->transp.length = 0;
		break;
	}
}

/*
 * The hardware clock divider has an integer and a fractional
 * stage:
 *
 *	clk2 = clk_in / integer_divider
 *	clk_out = clk2 * (1 - (fractional_divider >> 12))
 *
 * Calculate integer and fractional divider for given clk_in
 * and clk_out.
 */
static void set_clock_divider(struct pxa910fb_info *fbi,
			      const struct fb_videomode *m)
{
	int divider_int;
	int needed_pixclk;
	u64 div_result;
	u32 x = 0;

	/*
	 * Notice: The field pixclock is used by linux fb
	 * is in pixel second. E.g. struct fb_videomode &
	 * struct fb_var_screeninfo
	 */

	/*
	 * Check input values.
	 */
	//dev_dbg(fbi->fb_info->dev, "Enter %s\n", __FUNCTION__);
	if (!m || !m->pixclock || !m->refresh) {
		printf("Input refresh or pixclock is wrong.\n");
		return;
	}

	if (fbi->id <= 0) {
		x = 0x40000000;	/* select LCD display clock1 */
	}

	/*
	 * Calc divider according to refresh rate.
	 */
	div_result = 1000000000000ll;
	do_div(div_result, m->pixclock);
	needed_pixclk = (u32) div_result;

	divider_int = 312000000 / needed_pixclk;	//312MHz

	/* check whether divisor is too small. */
	if (divider_int < 2) {
		printf("Warning: clock source is too slow."
		       "Try smaller resolution\n");
		divider_int = 2;
	}

	/*
	 * Set setting to reg.
	 */
	x |= divider_int;

	writel(x, fbi->reg_base + LCD_CFG_SCLK_DIV);
}

static void set_dma_control0(struct pxa910fb_info *fbi)
{
	u32 x;

	/*
	 * Set bit to enable graphics DMA.
	 */

	x = readl(fbi->reg_base + LCD_SPU_DMA_CTRL0);
	x |= fbi->active ? 0x00000100 : 0;

	/*
	 * If we are in a pseudo-color mode, we need to enable
	 * palette lookup.
	 */
	if (fbi->pix_fmt == PIX_FMT_PSEUDOCOLOR)
		x |= 0x10000000;

	/*
	 * Configure hardware pixel format.
	 */
	x &= ~(0xF << 16);
	x |= (fbi->pix_fmt >> 1) << 16;

	/*
	 * Check YUV422PACK
	 */
	x &= ~((1 << 9) | (1 << 11) | (1 << 10) | (1 << 12));
	if (((fbi->pix_fmt >> 1) == 5) || (fbi->pix_fmt & 0x1000)) {
		x |= 1 << 9;
		x |= (fbi->panel_rbswap) << 12;
		if (fbi->pix_fmt == 11)
			x |= 1 << 11;
		if (fbi->pix_fmt & 0x1000)
			x |= 1 << 10;
	} else {

		/*
		 * Check red and blue pixel swap.
		 * 1. source data swap. BGR[M:L] rather than RGB[M:L] is stored in memeory as source format.
		 * 2. panel output data swap
		 */
		x |= (((fbi->
			pix_fmt & 1) ^ 1) ^ (fbi->panel_rbswap)) << 12;
	}
	x |= CFG_ARBFAST_ENA(1);	/* FIXME */
	writel(x, fbi->reg_base + LCD_SPU_DMA_CTRL0);
}

static void set_dma_control1(struct pxa910fb_info *fbi, int sync)
{
	u32 x;

	if (fbi->id <= 0) {
		/*
		 * Configure default bits: vsync triggers DMA, gated clock
		 * enable, power save enable, configure alpha registers to
		 * display 100% graphics, and set pixel command.
		 */
		x = readl(fbi->reg_base + LCD_SPU_DMA_CTRL1);
		/*
		 * We trigger DMA on the falling edge of vsync if vsync is
		 * active low, or on the rising edge if vsync is active high.
		 */
		if (!(sync & FB_SYNC_VERT_HIGH_ACT))
			x |= 0x08000000;
		writel(x, fbi->reg_base + LCD_SPU_DMA_CTRL1);
	}
}

static void set_graphics_start(struct pxa910fb_info *fbi, int xoffset,
			       int yoffset)
{
	struct fb_var_screeninfo *var = &fbi->var;
	int pixel_offset;
	unsigned long addr;
	pixel_offset = (yoffset * var->xres_virtual) + xoffset;
	addr = fbi->fb_start_dma +
	    (pixel_offset * (var->bits_per_pixel >> 3));

	fbi->new_addr[0] = addr;
	writel(addr, fbi->reg_base + LCD_CFG_GRA_START_ADDR0);
}

static void set_dumb_panel_control(struct pxa910fb_info *fbi, struct pxa910fb_mach_info *mi)	//(struct fb_info *info)
{
	u32 x;

	/*
	 * Preserve enable flag.
	 */

	x = readl(fbi->reg_base + LCD_SPU_DUMB_CTRL) & 0x00000001;
	x |= 1 << 8;		/* GPIO control pin */
	x |= (fbi->is_blanked ? 0x7 : mi->dumb_mode) << 28;
	x |= mi->gpio_output_data << 20;
	x |= mi->gpio_output_mask << 12;
	x |= mi->panel_rgb_reverse_lanes ? 0x00000080 : 0;
	x |= mi->invert_composite_blank ? 0x00000040 : 0;
	x |= (fbi->var.sync & FB_SYNC_COMP_HIGH_ACT) ? 0x00000020 : 0;
	x |= mi->invert_pix_val_ena ? 0x00000010 : 0;
	x |= (fbi->var.sync & FB_SYNC_VERT_HIGH_ACT) ? 0x00000008 : 0;
	x |= (fbi->var.sync & FB_SYNC_HOR_HIGH_ACT) ? 0x00000004 : 0;
	x |= mi->invert_pixclock ? 0x00000002 : 0;

	writel(x, fbi->reg_base + LCD_SPU_DUMB_CTRL);
}

static void set_dumb_screen_dimensions(struct pxa910fb_info *fbi, struct pxa910fb_mach_info *mi)	//(struct fb_info *info)
{

	struct fb_var_screeninfo *v = &fbi->var;

	int x;
	int y;

	x = v->xres + v->right_margin + v->hsync_len + v->left_margin;

	y = v->yres + v->lower_margin + v->vsync_len + v->upper_margin;

	writel((y << 16) | x, fbi->reg_base + LCD_SPUT_V_H_TOTAL);

}

static int pxa910fb_set_par(struct pxa910fb_info *fbi,
			    struct pxa910fb_mach_info *mi)
{

	struct fb_var_screeninfo *var = &fbi->var;
	u32 x;
	volatile unsigned int reg1;

	fbi->pix_fmt = mi->pix_fmt;
	set_pix_fmt(&fbi->var, fbi->pix_fmt);

	/* Calculate clock divisor for dumb panel. */
	set_clock_divider(fbi, mi->modes);

	reg1 = readl(fbi->reg_base + LCD_TOP_CTRL);
	reg1 |= 0xfff0;		/* FIXME */
	writel(reg1, fbi->reg_base + LCD_TOP_CTRL);

	/*
	 * Configure graphics DMA parameters.
	 * Configure global panel parameters.
	 */

	writel((var->yres << 16) | var->xres,
	       fbi->reg_base + LCD_SPU_V_H_ACTIVE);
	x = readl(fbi->reg_base + LCD_CFG_GRA_PITCH);
	x = (x & ~0xFFFF) | ((var->xres_virtual * var->bits_per_pixel) >>
			     3);
	writel(x, fbi->reg_base + LCD_CFG_GRA_PITCH);
	writel((var->yres << 16) | var->xres,
	       fbi->reg_base + LCD_SPU_GRA_HPXL_VLN);
	writel((var->yres << 16) | var->xres,
	       fbi->reg_base + LCD_SPU_GZM_HPXL_VLN);
	writel((var->left_margin << 16) | var->right_margin,
	       fbi->reg_base + LCD_SPU_H_PORCH);
	writel((var->upper_margin << 16) | var->lower_margin,
	       fbi->reg_base + LCD_SPU_V_PORCH);

	writel(((var->width + var->left_margin) << 16) | (var->width +
							  var->left_margin),
	       fbi->reg_base + LCD_PN_SEPXLCNT);

	writel(0x00000000, fbi->reg_base + LCD_SPU_BLANKCOLOR);


	/* Configure dma ctrl regs. */
	set_dma_control0(fbi);
	set_dma_control1(fbi, var->sync);

	/*
	 * Configure dumb panel ctrl regs & timings.
	 */
	set_dumb_panel_control(fbi, mi);
	set_dumb_screen_dimensions(fbi, mi);

	x = readl(fbi->reg_base + LCD_SPU_DUMB_CTRL);
	if ((x & 1) == 0)
		writel(x | 1, fbi->reg_base + LCD_SPU_DUMB_CTRL);

	set_graphics_start(fbi, var->xoffset, var->yoffset);
	return 0;
}


static int pxa910fb_init_mode(struct pxa910fb_info *info,
			      struct pxa910fb_mach_info *mi)
{
	struct fb_var_screeninfo *var = &info->var;

	int ret = 0;
	u32 total_w, total_h, refresh;
	u64 div_result;
	memset(var, 0x00, sizeof(struct fb_var_screeninfo));
	/*
	 * Set default value
	 */
	refresh = DEFAULT_REFRESH;

	/*
	 * Initialize mode
	 */
	var->xres = mi->modes->xres;
	var->yres = mi->modes->yres;
	var->xres_virtual = mi->modes->xres;
	var->yres_virtual = mi->modes->yres;
	var->xoffset = 0;
	var->yoffset = 0;
	var->pixclock = mi->modes->pixclock;
	var->left_margin = mi->modes->left_margin;
	var->right_margin = mi->modes->right_margin;
	var->upper_margin = mi->modes->upper_margin;
	var->lower_margin = mi->modes->lower_margin;
	var->hsync_len = mi->modes->hsync_len;
	var->vsync_len = mi->modes->vsync_len;
	var->sync = mi->modes->sync;

	/* Init settings. */
	var->xres_virtual = var->xres;
	var->yres_virtual = var->yres;


	total_w = var->xres + var->left_margin + var->right_margin +
	    var->hsync_len;
	total_h = var->yres + var->upper_margin + var->lower_margin +
	    var->vsync_len;

	div_result = 1000000000000ll;
	do_div(div_result, total_w * total_h * refresh);
	var->pixclock = (u32) div_result;

	return ret;
}

static void pxa910fb_set_default(struct pxa910fb_info *fbi,
				 struct pxa910fb_mach_info *mi)
{
	/*
	 * Configure default register values.
	 */

	writel(0x00000000, fbi->reg_base + LCD_SPU_BLANKCOLOR);
	writel(mi->io_pin_allocation_mode,
	       fbi->reg_base + SPU_IOPAD_CONTROL);
	writel(0x00000000, fbi->reg_base + LCD_CFG_GRA_START_ADDR1);
	writel(0x00000000, fbi->reg_base + LCD_SPU_GRA_OVSA_HPXL_VLN);
	writel(0x0, fbi->reg_base + LCD_SPU_SRAM_PARA0);
	writel(CFG_CSB_256x32(0x1) | CFG_CSB_256x24(0x1) |
	       CFG_CSB_256x8(0x1), fbi->reg_base + LCD_SPU_SRAM_PARA1);
	writel(VIDEO_FIFO(1) | GRAPHIC_FIFO(1),
	       fbi->reg_base + LCD_FIFO_DEPTH);
	/*
	 * Configure default bits: vsync triggers DMA,
	 * power save enable, configure alpha registers to
	 * display 100% graphics, and set pixel command.
	 */
	writel(CFG_VSYNC_TRIG(2) | CFG_PWRDN_ENA(1) |
	       CFG_ALPHA_MODE(1) | CFG_ALPHA(0xFF) | 0x81,
	       fbi->reg_base + LCD_SPU_DMA_CTRL1);

}

static int pxa910fb_power(struct pxa910fb_info *fbi,
			  struct pxa910fb_mach_info *mi, int on)
{
	int ret = 0;

	if ((mi->spi_ctrl != -1) && (mi->spi_ctrl & CFG_SPI_ENA_MASK))
		writel(mi->spi_ctrl, fbi->reg_base + LCD_SPU_SPI_CTRL);

	if (mi->pxa910fb_lcd_power)
		ret =
		    mi->pxa910fb_lcd_power(fbi, mi->spi_gpio_cs,
					   mi->spi_gpio_reset, on);

	return ret;
}

void dump_lcd_registers(struct pxa910fb_info *fbi)
{
	int reg = 0;
	//Dump all the lcd registers
	for (reg = 0xc0; reg < 0x1ec; reg += 4)
		printf("Reg: 0x%x = 0x%x\n", reg,
		       readl(fbi->reg_base + reg));
}

struct pxa910fb_info *pxa910fb_init(struct pxa910fb_mach_info *mi)
{
	struct pxa910fb_info *fbi = &pxa_fb_info;

	/* Initialize private data */
	fbi->panel_rbswap = mi->panel_rbswap;
	fbi->is_blanked = 0;
	fbi->active = mi->active;
	fbi->mi = mi;

	/*
	 * Map LCD controller registers.
	 */
	fbi->reg_base = PXA910_LCD_REGS_BASE;

	/*
	 * Allocate framebuffer memory.
	 */
	fbi->fb_size = DEFAULT_FB_SIZE;
	fbi->fb_start = (unsigned int *) DEFAULT_FB_BASE;
	fbi->fb_start_dma = DEFAULT_FB_BASE;
	memset(fbi->fb_start, 0x00, fbi->fb_size);

	/*
	 * init video mode data.
	 */
	pxa910fb_init_mode(fbi, mi);

	/*
	 * enable controller clock
	 */
	writel(0x3f, PMUA_LCD_CLK_RES_CTRL);

	/*
	 * Fill in sane defaults.
	 */
	pxa910fb_set_default(fbi, mi);	/* FIXME */

	if (pxa910fb_power(fbi, mi, 1)) {
		printf("\n\npxa910fb_init! power on failed!.\n");
	}
	pxa910fb_set_par(fbi, mi);

	//dump_lcd_registers(fbi);
	return fbi;
}
