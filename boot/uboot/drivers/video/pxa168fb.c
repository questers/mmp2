/*
 * linux/drivers/video/pxa168fb.c -- Marvell PXA168 LCD Controller
 *
 *  Copyright (C) 2008 Marvell International Ltd.
 *  All rights reserved.
 *
 *  2009-02-16  adapted from original version for PXA168
 *		Green Wan <gwan@marvell.com>
 *              Jun Nie <njun@marvell.com>
 *		Kevin Liu <kliu5@marvell.com>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

#include <config.h>
#include <div64.h>
#include <malloc.h>
#include <linux/types.h>
#include <asm/io.h>
#include <common.h>

#include <pxa168fb.h>
#include "pxa168fb.h"

#define DEFAULT_REFRESH		60 /* Hz */

static struct pxa168fb_info pxa_fb_info;

struct lcd_regs *get_regs(struct pxa168fb_info *fbi)
{
	struct lcd_regs *regs = (struct lcd_regs *)((unsigned)fbi->reg_base);

	if (fbi->id == 0)
		regs = (struct lcd_regs *)((unsigned)fbi->reg_base + 0xc0);
	if (fbi->id == 2)
	regs = (struct lcd_regs *)((unsigned)fbi->reg_base + 0x200);

	return regs;
}

u32 dma_ctrl_read(struct pxa168fb_info *fbi, int ctrl1)
{
	u32 reg = (u32)fbi->reg_base + dma_ctrl(ctrl1, fbi->id);
	return __raw_readl(reg);
}

void dma_ctrl_write(struct pxa168fb_info *fbi, int ctrl1, u32 value)
{
	u32 reg = (u32)fbi->reg_base + dma_ctrl(ctrl1, fbi->id);
	__raw_writel(value, reg);
}

void dma_ctrl_set(struct pxa168fb_info *fbi, int ctrl1, u32 mask, u32 value)
{
	u32 reg = (u32)fbi->reg_base + dma_ctrl(ctrl1, fbi->id);
	u32 tmp1, tmp2;

	tmp1 = tmp2 = __raw_readl(reg); tmp2 &= ~mask; tmp2 |= value;
	if (tmp1 != tmp2) __raw_writel(tmp2, reg);
}

static void set_pix_fmt(struct fb_var_screeninfo *var, int pix_fmt)
{
	switch (pix_fmt) {
	case PIX_FMT_RGB565:
		var->bits_per_pixel = 16;
		var->red.offset = 11;    var->red.length = 5;
		var->green.offset = 5;   var->green.length = 6;
		var->blue.offset = 0;    var->blue.length = 5;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		break;
	case PIX_FMT_BGR565:
		var->bits_per_pixel = 16;
		var->red.offset = 0;     var->red.length = 5;
		var->green.offset = 5;   var->green.length = 6;
		var->blue.offset = 11;   var->blue.length = 5;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		break;
	case PIX_FMT_RGB1555:
		var->bits_per_pixel = 16;
		var->red.offset = 10;    var->red.length = 5;
		var->green.offset = 5;   var->green.length = 5;
		var->blue.offset = 0;    var->blue.length = 5;
		var->transp.offset = 15; var->transp.length = 1;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 5 << 20;
		break;
	case PIX_FMT_BGR1555:
		var->bits_per_pixel = 16;
		var->red.offset = 0;     var->red.length = 5;
		var->green.offset = 5;   var->green.length = 5;
		var->blue.offset = 10;   var->blue.length = 5;
		var->transp.offset = 15; var->transp.length = 1;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 5 << 20;
		break;
	case PIX_FMT_RGB888PACK:
		var->bits_per_pixel = 24;
		var->red.offset = 16;    var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 0;    var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 6 << 20;
		break;
	case PIX_FMT_BGR888PACK:
		var->bits_per_pixel = 24;
		var->red.offset = 0;     var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 16;   var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 6 << 20;
		break;
	case PIX_FMT_RGB888UNPACK:
		var->bits_per_pixel = 32;
		var->red.offset = 16;    var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 0;    var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 7 << 20;
		break;
	case PIX_FMT_BGR888UNPACK:
		var->bits_per_pixel = 32;
		var->red.offset = 0;     var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 16;   var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 7 << 20;
		break;
	case PIX_FMT_RGBA888:
		var->bits_per_pixel = 32;
		var->red.offset = 16;    var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 0;    var->blue.length = 8;
		var->transp.offset = 24; var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 8 << 20;
		break;
	case PIX_FMT_BGRA888:
		var->bits_per_pixel = 32;
		var->red.offset = 0;     var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 16;   var->blue.length = 8;
		var->transp.offset = 24; var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 8 << 20;
		break;
/*	case PIX_FMT_YUYV422PACK:
		var->bits_per_pixel = 16;
		var->red.offset = 8;     var->red.length = 16;
		var->green.offset = 4;   var->green.length = 16;
		var->blue.offset = 0;   var->blue.length = 16;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 9 << 20;
		break;
	case PIX_FMT_YVU422PACK:
		var->bits_per_pixel = 16;
		var->red.offset = 0;     var->red.length = 16;
		var->green.offset = 8;   var->green.length = 16;
		var->blue.offset = 12;   var->blue.length = 16;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 9 << 20;
		break;
	case PIX_FMT_YUV422PACK:
		var->bits_per_pixel = 16;
		var->red.offset = 4;     var->red.length = 16;
		var->green.offset = 12;   var->green.length = 16;
		var->blue.offset = 0;    var->blue.length = 16;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 9 << 20;
		break;
	case PIX_FMT_PSEUDOCOLOR:
		var->bits_per_pixel = 8;
		var->red.offset = 0;     var->red.length = 8;
		var->green.offset = 0;   var->green.length = 8;
		var->blue.offset = 0;    var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 0;
		break;*/
	}
}

#define LCD_TCLK_DIV		(0x009C)
#define LCD_PN2_SCLK_DIV	(0x01EC)
#define LCD_CFG_SCLK_DIV	(0x01A8)
#define clk_div(id)     ((id) ? ((id) & 1 ? LCD_TCLK_DIV : LCD_PN2_SCLK_DIV) : LCD_CFG_SCLK_DIV)
/*
 * The hardware clock divider has an integer and a fractional
 * stage:
 *
 *	clk2 = clk_in / integer_divider
 *	clk_out = clk2 * (1 - (fractional_divider >> 12))
 *
 * Calculate integer and fractional divider for given clk_in
 * and clk_out.
 */
static void set_clock_divider(struct pxa168fb_info *fbi,
			      const struct fb_videomode *m)
{
	struct pxa168fb_mach_info *mi = fbi->mi;

	writel(mi->sclk_div, fbi->reg_base + clk_div(fbi->id));
}

static u32 dma_ctrl0_update(int active, struct pxa168fb_mach_info *mi, u32 x, u32 pix_fmt)
{
	if (active)
		x |= 0x00000100;
	else
		x &= ~0x00000100;

	/* If we are in a pseudo-color mode, we need to enable
	 * palette lookup  */
	if (pix_fmt == PIX_FMT_PSEUDOCOLOR)
		x |= 0x10000000;

	/* Configure hardware pixel format */
	x &= ~(0xF << 16);
	x |= (pix_fmt >> 1) << 16;

	/* Check YUV422PACK */
	x &= ~((1 << 9) | (1 << 11) | (1 << 10) | (1 << 12));
	if (((pix_fmt >> 1) == 5) || (pix_fmt & 0x1000)) {
		x |= 1 << 9;
		x |= (mi->panel_rbswap) << 12;
		if (pix_fmt == 11)
			x |= 1 << 11;
		if (pix_fmt & 0x1000)
			x |= 1 << 10;
	} else {
		/* Check red and blue pixel swap.
		 * 1. source data swap. BGR[M:L] rather than RGB[M:L] is stored in memeory as source format.
		 * 2. panel output data swap
		 */
		x |= (((pix_fmt & 1) ^ 1) ^ (mi->panel_rbswap)) << 12;
	}
	/* enable horizontal smooth filter for both graphic and video layers */
	x |= CFG_GRA_HSMOOTH(1) | CFG_DMA_HSMOOTH(1);

	return x;
}

static void set_dma_control0(struct pxa168fb_info *fbi)
{
	struct pxa168fb_mach_info *mi;
	u32 x = 0, active, pix_fmt = fbi->pix_fmt;

	/* Set bit to enable graphics DMA */
	if (fbi->id != 1)
		dma_ctrl_set(fbi, 0, CFG_ARBFAST_ENA(1), CFG_ARBFAST_ENA(1));

	mi = fbi->mi;
	active = fbi->active;
	x = dma_ctrl_read(fbi, 0);
	active = 1;
	x = dma_ctrl0_update(active, mi, x, pix_fmt);
	dma_ctrl_write(fbi, 0, x);
	/* enable multiple burst request in DMA AXI bus arbiter for faster read */
	x = readl(fbi->reg_base + LCD_SPU_DMA_CTRL0);
	x |= CFG_ARBFAST_ENA(1);
	writel(x, fbi->reg_base + LCD_SPU_DMA_CTRL0);
}

static void set_dma_control1(struct pxa168fb_info *fbi, int sync)
{
	u32 x;

	/* Configure default bits: vsync triggers DMA, gated clock
	 * enable, power save enable, configure alpha registers to
	 * display 100% graphics, and set pixel command.
	 */
	x = dma_ctrl_read(fbi, 1);
	/* We trigger DMA on the falling edge of vsync if vsync is
	 * active low, or on the rising edge if vsync is active high.
	 */
	if (!(sync & FB_SYNC_VERT_HIGH_ACT))
		x |= 0x08000000;
	else
		x &= ~0x08000000;
	dma_ctrl_write(fbi, 1, x);
}


static void set_graphics_start(struct pxa168fb_info *fb, int xoffset, int yoffset)
{
	struct pxa168fb_info *fbi = fb;
	struct fb_var_screeninfo *var = fb->var;
	int pixel_offset;
	unsigned long addr;
	static int debugcount = 0;
	struct lcd_regs *regs;

	if (debugcount < 10)
		debugcount++;

	pixel_offset = (yoffset * var->xres_virtual) + xoffset;
	addr = fbi->fb_start_dma + (pixel_offset * (var->bits_per_pixel >> 3));

	regs = get_regs(fbi);
	writel(addr, &regs->g_0);
}

int fb_mode = 0;
int fb_share = 0;

static void set_screen(struct pxa168fb_info *fbi, struct pxa168fb_mach_info *mi)
{
	struct fb_var_screeninfo *var = fbi->var;
	struct lcd_regs *regs = get_regs(fbi);
	struct dsi_info *di = mi->dsi;
	u32 x, h_porch, vsync_ctrl, vec = 10;
	u32 xres = var->xres, yres = var->yres;
	u32 xres_z = var->xres, yres_z = var->yres;
	u32 xres_virtual = var->xres_virtual;
	u32 bits_per_pixel = var->bits_per_pixel;

	if (mi->phy_type & (DSI2DPI | DSI))
		vec = ((di->lanes <= 2) ? 1 : 2) * 10 * di->bpp / 8 / di->lanes;

	/* resolution, active */
	writel((var->yres << 16) | var->xres, &regs->screen_active);
	/* pitch, pixels per line */
	x = readl(&regs->g_pitch);
	x = (x & ~0xFFFF) | ((xres_virtual * bits_per_pixel) >> 3);
	writel(x, &regs->g_pitch);

	/* resolution, src size */
	writel((yres << 16) | xres, &regs->g_size);
	/* resolution, dst size */
	writel((yres_z << 16) | xres_z, &regs->g_size_z);

	/* h porch, left/right margin */
	if (mi->phy_type & (DSI2DPI | DSI)) {
		h_porch = (var->xres + var->right_margin) * vec / 10 - var->xres;
		h_porch = (var->left_margin * vec / 10) << 16 | h_porch;
	} else
		h_porch = (var->left_margin) << 16 | var->right_margin;
	writel(h_porch, &regs->screen_h_porch);
	/* v porch, upper/lower margin */
	writel((var->upper_margin << 16) | var->lower_margin,
		&regs->screen_v_porch);

	/* vsync ctrl */
	if (mi->phy_type & (DSI2DPI | DSI))
		vsync_ctrl = 0x01330133;
	else {
		if ((fbi->id == 0) || (fbi->id == 2))
			vsync_ctrl = ((var->width + var->left_margin) << 16)
				| (var->width + var->left_margin);
		else
			vsync_ctrl = ((var->xres + var->right_margin) << 16)
				| (var->xres + var->right_margin);
	}
	writel(vsync_ctrl, &regs->vsync_ctrl);  /* FIXME */

        /* blank color */
        writel(0x00000000, &regs->blank_color);
}

static void set_dumb_panel_control(struct pxa168fb_info *fbi, struct pxa168fb_mach_info *mi)
{
	u32 x;

	x = readl(fbi->reg_base + intf_ctrl(fbi->id)) & 0x00000001;
	x |= (fbi->is_blanked ? 0x7 : mi->dumb_mode) << 28;
	if (fbi->id == 1) {
		/* enable AXI urgent flag */
		x |= 0xff << 16;
	} else {
		x |= mi->gpio_output_data << 20;
		x |= mi->gpio_output_mask << 12;
	}
	x |= mi->panel_rgb_reverse_lanes ? 0x00000080 : 0;
	x |= mi->invert_composite_blank ? 0x00000040 : 0;
	x |= (fbi->var->sync & 8) ? 0x00000020 : 0;
	x |= mi->invert_pix_val_ena ? 0x00000010 : 0;
	x |= (fbi->var->sync & 2) ? 0x00000008 : 0;
	x |= (fbi->var->sync & 1) ? 0x00000004 : 0;
	x |= mi->invert_pixclock ? 0x00000002 : 0;

	writel(x, fbi->reg_base + intf_ctrl(fbi->id));  /* FIXME */
}

static void set_dumb_screen_dimensions(struct pxa168fb_info *fbi, struct pxa168fb_mach_info *mi)
{
	struct lcd_regs *regs = get_regs(fbi);
	struct fb_var_screeninfo *v = fbi->var;
	struct dsi_info *di = mi->dsi;
	int x;
	int y;
	int vec = 10;

	/* FIXME - need to double check (*3) and (*2) */
	if (mi->phy_type & (DSI2DPI | DSI))
		vec = ((di->lanes <= 2) ? 1 : 2) * 10 * di->bpp / 8 / di->lanes;

	x = v->xres + v->right_margin + v->hsync_len + v->left_margin;
	x = x * vec / 10;
	y = v->yres + v->lower_margin + v->vsync_len + v->upper_margin;

	writel((y << 16) | x, &regs->screen_size);
}

#define FB_ACCEL_NONE 	0
#define FB_ROTATE_UR	0
static int pxa168fb_set_par(struct pxa168fb_info *fb, struct pxa168fb_mach_info *mi)
{
	struct pxa168fb_info *fbi = fb;
	struct fb_var_screeninfo *var = fb->var;
	const struct fb_videomode *mode;
	struct dsi_info *di;
	int pix_fmt;
	u32 x;

	di = mi->dsi;

	/* Determine which pixel format we're going to use */
	pix_fmt = mi->pix_fmt;    // choose one 

	if (pix_fmt < 0)
		return pix_fmt;
	fbi->pix_fmt = pix_fmt;

	/* Set additional mode info */
	if (pix_fmt == PIX_FMT_PSEUDOCOLOR)
		fb->fix->visual = FB_VISUAL_PSEUDOCOLOR;
	else
		fb->fix->visual = FB_VISUAL_TRUECOLOR;

	/* convet var to video mode */
	set_pix_fmt(var, pix_fmt);
	if (!var->xres_virtual)
		var->xres_virtual = var->xres;
	if (!var->yres_virtual)
		var->yres_virtual = var->yres * 2;
	var->grayscale = 0;
	var->accel_flags = FB_ACCEL_NONE;
	var->rotate = FB_ROTATE_UR;
	/* Calculate clock divisor. */
	set_clock_divider(fbi, mode);
	/* Configure dma ctrl regs. */
	set_dma_control1(fbi, fb->var->sync);

	/* Configure graphics DMA parameters.
	 * Configure global panel parameters.
	 */
	set_screen(fbi, mi);
	/* Configure dumb panel ctrl regs & timings */
	set_dumb_panel_control(fbi, mi);
	set_dumb_screen_dimensions(fbi, mi);
	x = readl(fbi->reg_base + intf_ctrl(fbi->id));
	if ((x & 1) == 0)
		writel(x | 1, fbi->reg_base + intf_ctrl(fbi->id));

	set_graphics_start(fbi, fbi->var->xoffset, fbi->var->yoffset);
	set_dma_control0(fbi);
	return 0;
}

#define FB_VMODE_MASK		255

void fb_videomode_to_var(struct fb_var_screeninfo *var,
                         const struct fb_videomode *mode)
{
	var->xres = mode->xres;
	var->yres = mode->yres;
	var->xres_virtual = mode->xres;
	var->yres_virtual = mode->yres;
	var->xoffset = 0;
	var->yoffset = 0;
	var->pixclock = mode->pixclock;
	var->left_margin = mode->left_margin;
	var->right_margin = mode->right_margin;
	var->upper_margin = mode->upper_margin;
	var->lower_margin = mode->lower_margin;
	var->hsync_len = mode->hsync_len;
	var->vsync_len = mode->vsync_len;
	var->sync = mode->sync;
	var->vmode = mode->vmode & FB_VMODE_MASK;
}


static int pxa168fb_init_mode(struct pxa168fb_info *fbi,
			      struct pxa168fb_mach_info *mi)
{
	struct fb_var_screeninfo *var = fbi->var;
	int ret = 0;
	u32 total_w, total_h, refresh;
	u64 div_result;
	const struct fb_videomode *m;

	/* Set default value */
	refresh = DEFAULT_REFRESH;

	/* If has bootargs, apply it first */

	/* try to find best video mode. */
	/* Init mode */
	var->xres = mi->modes->xres;
	var->yres = mi->modes->yres;
	var->xres_virtual = mi->modes->xres;
	var->yres_virtual = mi->modes->yres;
	var->xoffset = 0;
	var->yoffset = 0;
	var->pixclock = mi->modes->pixclock;
	var->left_margin = mi->modes->left_margin;
	var->right_margin = mi->modes->right_margin;
	var->upper_margin = mi->modes->upper_margin;
	var->lower_margin = mi->modes->lower_margin;
	var->hsync_len = mi->modes->hsync_len;
	var->vsync_len = mi->modes->vsync_len;
	var->sync = mi->modes->sync;

	/* Init settings. */
	var->xres_virtual = var->xres;
	var->yres_virtual = var->yres * 2;
	/* correct pixclock. */
	if (!var->pixclock) {
		total_w = var->xres + var->left_margin + var->right_margin +
		  	var->hsync_len;
		total_h = var->yres + var->upper_margin + var->lower_margin +
		  	var->vsync_len;
		div_result = 1000000000000ll;
		do_div(div_result, total_w * total_h * refresh);
		var->pixclock = (u32)div_result;
	}
	return ret;
}

static void pxa168fb_set_default(struct pxa168fb_info *fbi,
		struct pxa168fb_mach_info *mi)
{
	struct lcd_regs *regs = get_regs(fbi);
	u32 dma_ctrl1 = 0x2012ff81;
	u32 burst_length = (mi->burst_len == 16) ?
		CFG_CYC_BURST_LEN16 : CFG_CYC_BURST_LEN8;
	/* Configure default register values */
	writel(mi->io_pin_allocation_mode | burst_length,
			fbi->reg_base + SPU_IOPAD_CONTROL);
	/* enable 16 cycle burst length to get better formance */
	writel(0x00000000, &regs->blank_color);
	writel(0x00000000, &regs->g_1);
	writel(0x00000000, &regs->g_start);

	/* Configure default bits: vsync triggers DMA,
	 * power save enable, configure alpha registers to
	 * display 100% graphics, and set pixel command.
	 */
	if (fbi->id == 1) {
		if (mi->phy_type & (DSI2DPI | DSI))
			dma_ctrl1 = 0xa03eff00;
		else
			dma_ctrl1 = 0x203eff00;	/* FIXME */
	}
	dma_ctrl_write(fbi, 1, dma_ctrl1);
}


static int pxa168fb_power(struct pxa168fb_info *fbi,
                struct pxa168fb_mach_info *mi, int on)
{
	if ((mi->spi_ctrl != -1) && (mi->spi_ctrl & CFG_SPI_ENA_MASK)) {
		writel(mi->spi_ctrl, fbi->reg_base + LCD_SPU_SPI_CTRL);
	}

	if ((mi->pxa168fb_lcd_power) && ((fbi->active && !on ) || (!fbi->active && on)))
		mi->pxa168fb_lcd_power(fbi, mi->spi_gpio_cs, mi->spi_gpio_reset, on);
	return 0;
}

void dump_lcd_registers(struct pxa168fb_info *fbi)
{

}
#define FB_ACCEL_NONE		0
#define FB_VMODE_NONINTERLACED	0
#define FB_ROTATE_UR		0
static void set_mode(struct pxa168fb_info *fbi, struct pxa168fb_mach_info *mi, struct fb_videomode *mode, int pix_fmt, int ystretch)
{
	struct fb_var_screeninfo *var = fbi->var;
	set_pix_fmt(var, pix_fmt);
	var->xres = mode->xres;
	var->yres = mode->yres;
	var->xres_virtual = var->xres;   //max(var->xres, var->xres_virtual);
	if (ystretch && !fb_share)
		var->yres_virtual = var->yres *2;
	else
		var->yres_virtual = max(var->yres, var->yres_virtual);
	var->grayscale = 0;
	var->accel_flags = FB_ACCEL_NONE;
	var->pixclock = mode->pixclock;
	var->left_margin = mode->left_margin;
	var->right_margin = mode->right_margin;
	var->upper_margin = mode->upper_margin;
	var->lower_margin = mode->lower_margin;
	var->hsync_len = mode->hsync_len;
	var->vsync_len = mode->vsync_len;
	var->sync = mode->sync;
	var->vmode = FB_VMODE_NONINTERLACED;
	var->rotate = FB_ROTATE_UR;
}

static unsigned long pll2_get_clk(void)
{
	u32 mpmu_posr;
	u32 refdiv;
	u32 fbdiv;
	u32 input_clk;
	u32 output_clk;
	u32 M, N;

	mpmu_posr = __raw_readl(0xd4050010);   // (MPMU_POSR)
	refdiv      = (mpmu_posr >> 23) & 0x1f;
	fbdiv       = (mpmu_posr >> 14) & 0x1ff;
	
	switch (refdiv) {
	case 3:
		M = 5;
		input_clk = 19200000;
		break;
	case 4:
		M = 6;
		input_clk = 26000000;
		break;
	default:
		return 0;
	}

	N = fbdiv + 2;
	output_clk = input_clk/10 * N / M *10;
	return output_clk;
}

#define MPMU_PLL1_CTRL 0xd4050418
static int lcd_clk_setrate(struct clk *clk, unsigned long val)
{
	u32 tmp = __raw_readl(0xD4282800 + 0x004C);  //clk->clk_rst
	u32 pll2clk = pll2_get_clk();
	
	tmp &= ~((0x1f << 15) | (0xf << 8) | (0x3 << 6));
	tmp |= (0x1a << 15);

	switch (val) {
	case 260000000:
		/* enable PLL1/1.5 divider */
		__raw_writel(0x1, MPMU_PLL1_CTRL);
		/* DSP1clk = PLL1/1.5/2 = 533MHz */
		tmp |= (0x2 << 8) | (0x2 << 6 | 1 << 20);
		break;
	case 400000000:
		/* DSP1clk = PLL1/2 = 400MHz */
		tmp |= (0x2 << 8) | (0x0 << 6);
		break;
	case 500000000:
		/* DSP1clk = PLL2/x = 500MHz */
		if (pll2clk > 900000000)
			tmp |= (0x2 << 8) | (0x2 << 6);
		else
			tmp |= (0x1 << 8) | (0x2 << 6);
		break;
	default:
		printf("%d not supported\n", (int)val);
		return -1;
	}
	__raw_writel(tmp, 0xD4282800 + 0x004C);  //clk->clk_rst
	return 0;
}

static void lcd_pn1_clk_enable(struct clk *clk)
{
	u32 tmp;

	tmp = __raw_readl(0xD4282800 + 0x004C);    //clk->clk_rst
	/* enable Display1 AXI clock */
	tmp |= (1<<3);
	__raw_writel(tmp, 0xD4282800 + 0x004C);
	/* enable Display1 peripheral clock */
	tmp |= (1<<4);
	__raw_writel(tmp, 0xD4282800 + 0x004C);
	/* enable DSI clock */
	tmp |= (1<<12) | (1<<5);
	__raw_writel(tmp, 0xD4282800 + 0x004C);
	/* release from reset */
	tmp |= 7;
	__raw_writel(tmp, 0xD4282800 + 0x004C);

}

int pxa168fb_init(struct pxa168fb_mach_info *mi)
{
	struct pxa168fb_info *fbi = &pxa_fb_info;
	u32 tmp; int sec = 100;
	struct dsi_info *di;
	u32 pll2clk;
	u8 data[2];
	u32 reg, clk_rst;

	/* Initialize private data */
	fbi->panel_rbswap = mi->panel_rbswap;
	fbi->is_blanked = 0;
	fbi->mi = mi;

	fbi->is_blanked = 0;
	fbi->debug = 0;
	fbi->active = mi->active;

	/*
	 * Map LCD controller registers,
	 */
	fbi->reg_base = 0xD420B000; //0xe6860000;//PXA168_LCD_REGS_BASE;
	di = mi->dsi;
	di->regs = 0xD420B800;     // DSI 1

	/*
	 * Allocate framebuffer memory
	 */
	fbi->fb_size = mi->default_fbsize?mi->default_fbsize:DEFAULT_FB_SIZE;
	fbi->fb_start = (unsigned int *) mi->default_fbbase?mi->default_fbbase:DEFAULT_FB_BASE;
	fbi->fb_start_dma = mi->default_fbbase?mi->default_fbbase:DEFAULT_FB_BASE;
	memset(fbi->fb_start, 0x00, fbi->fb_size);

	/*
	 * Set video mode
	 */
	set_mode(fbi, mi, mi->modes, mi->pix_fmt, 1);

	/*
	 * init video mode data
	 */
	pxa168fb_init_mode(fbi, mi);
	/*
	 * enable controller clock
	 */
	lcd_clk_setrate(fbi->clk, mi->sclk_src);
	lcd_pn1_clk_enable(fbi->clk);	
	/*
	 * Fill in sane defaults
	 */
	pxa168fb_set_default(fbi, mi);	/* FIXME */
	pxa168fb_set_par(fbi, mi);
	
	// eable SPU_IRQ_ENA
	writel(0x0c000c00,fbi->reg_base + 0x01c0);
	// LCD_TOP_CTRL control reg
	writel(0xfff0,fbi->reg_base + 0x01dc);
	fbi->active = 0;	
	if (pxa168fb_power(fbi, mi, 1)) {
		printf("\n\npxa168fb_init! power on failed!. \n");
	}
	fbi->active = 1;

	if(mi->phy_init)
		mi->phy_init(fbi);

	return fbi;
}


