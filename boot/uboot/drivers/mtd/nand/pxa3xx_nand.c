/*
 * (C) Copyright 2010
 * Marvell Semiconductors Ltd. Shanghai, China.
 * Written-by: Lei Wen <leiwen@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <common.h>
#include <nand.h>
#include <asm/io.h>
#include <asm/errno.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/nand.h>
#ifdef CONFIG_PXA3XX_BBM
#include <linux/mtd/pxa3xx_bbm.h>
#endif

#define	CHIP_DELAY_TIMEOUT	(500)
#define PAGE_CHUNK_SIZE		(2048)
#define OOB_CHUNK_SIZE		(64)
#define BCH_THRESHOLD           (8)
#define CMD_POOL_SIZE           (5)
#define READ_ID_BYTES		(4)
#define BCH_STRENGTH		(4)
#define HAMMING_STRENGTH	(1)

/* registers and bit definitions */
#define NDCR		(0x00) /* Control register */
#define NDTR0CS0	(0x04) /* Timing Parameter 0 for CS0 */
#define NDTR1CS0	(0x0C) /* Timing Parameter 1 for CS0 */
#define NDSR		(0x14) /* Status Register */
#define NDPCR		(0x18) /* Page Count Register */
#define NDBDR0		(0x1C) /* Bad Block Register 0 */
#define NDBDR1		(0x20) /* Bad Block Register 1 */
#define NDREDEL		(0x24) /* Read Enable Return Delay Register */
#define NDECCCTRL	(0x28) /* ECC Control Register */
#define NDBZCNT		(0x2C) /* Timer for NDRnB0 and NDRnB1 */
#define NDDB		(0x40) /* Data Buffer */
#define NDCB0		(0x48) /* Command Buffer0 */
#define NDCB1		(0x4C) /* Command Buffer1 */
#define NDCB2		(0x50) /* Command Buffer2 */

#define NDCR_SPARE_EN		(0x1 << 31)
#define NDCR_ECC_EN		(0x1 << 30)
#define NDCR_DMA_EN		(0x1 << 29)
#define NDCR_ND_RUN		(0x1 << 28)
#define NDCR_DWIDTH_C		(0x1 << 27)
#define NDCR_DWIDTH_M		(0x1 << 26)
#define NDCR_PAGE_SZ_MASK	(0x3 << 24)
#define NDCR_PAGE_SZ(x)		(((x) << 24) & NDCR_PAGE_SZ_MASK)
#define NDCR_SEQ_DIS		(0x1 << 23)
#define NDCR_ND_STOP		(0x1 << 22)
#define NDCR_FORCE_CSX		(0x1 << 21)
#define NDCR_CLR_PG_CNT		(0x1 << 20)
#define NDCR_STOP_ON_UNCOR	(0x1 << 19)
#define NDCR_RD_ID_CNT_MASK	(0x7 << 16)
#define NDCR_RD_ID_CNT(x)	(((x) << 16) & NDCR_RD_ID_CNT_MASK)

#define NDCR_RA_START		(0x1 << 15)
#define NDCR_PG_PER_BLK_MASK	(0x3 << 13)
#define NDCR_PG_PER_BLK(x)	(((x) << 13) & NDCR_PG_PER_BLK_MASK)
#define NDCR_ND_ARB_EN		(0x1 << 12)
#define NDCR_INT_MASK           (0xFFF)
#define NDCR_RDYM               (0x1 << 11)
#define NDCR_CS0_PAGEDM         (0x1 << 10)
#define NDCR_CS1_PAGEDM         (0x1 << 9)
#define NDCR_CS0_CMDDM          (0x1 << 8)
#define NDCR_CS1_CMDDM          (0x1 << 7)
#define NDCR_CS0_BBDM           (0x1 << 6)
#define NDCR_CS1_BBDM           (0x1 << 5)
#define NDCR_UNCERRM            (0x1 << 4)
#define NDCR_CORERRM            (0x1 << 3)
#define NDCR_WRDREQM            (0x1 << 2)
#define NDCR_RDDREQM            (0x1 << 1)
#define NDCR_WRCMDREQM          (0x1)

#define NDSR_MASK		(0xffffffff)
#define NDSR_ERR_CNT_MASK       (0x1F << 16)
#define NDSR_ERR_CNT(x)         (((x) << 16) & NDSR_ERR_CNT_MASK)
#define NDSR_RDY                (0x1 << 12)
#define NDSR_FLASH_RDY          (0x1 << 11)
#define NDSR_CS0_PAGED		(0x1 << 10)
#define NDSR_CS1_PAGED		(0x1 << 9)
#define NDSR_CS0_CMDD		(0x1 << 8)
#define NDSR_CS1_CMDD		(0x1 << 7)
#define NDSR_CS0_BBD		(0x1 << 6)
#define NDSR_CS1_BBD		(0x1 << 5)
#define NDSR_DBERR		(0x1 << 4)
#define NDSR_SBERR		(0x1 << 3)
#define NDSR_WRDREQ		(0x1 << 2)
#define NDSR_RDDREQ		(0x1 << 1)
#define NDSR_WRCMDREQ		(0x1)

#define NDCB0_CMD_XTYPE_MASK    (0x7 << 29)
#define NDCB0_CMD_XTYPE(x)      (((x) << 29) & NDCB0_CMD_XTYPE_MASK)
#define NDCB0_LEN_OVRD		(0x1 << 28)
#define NDCB0_ST_ROW_EN         (0x1 << 26)
#define NDCB0_AUTO_RS		(0x1 << 25)
#define NDCB0_CSEL		(0x1 << 24)
#define NDCB0_CMD_TYPE_MASK	(0x7 << 21)
#define NDCB0_CMD_TYPE(x)	(((x) << 21) & NDCB0_CMD_TYPE_MASK)
#define NDCB0_NC		(0x1 << 20)
#define NDCB0_DBC		(0x1 << 19)
#define NDCB0_ADDR_CYC_MASK	(0x7 << 16)
#define NDCB0_ADDR_CYC(x)	(((x) << 16) & NDCB0_ADDR_CYC_MASK)
#define NDCB0_CMD2_MASK		(0xff << 8)
#define NDCB0_CMD1_MASK		(0xff)
#define NDCB0_ADDR_CYC_SHIFT	(16)

/* ECC Control Register */
#define NDECCCTRL_ECC_SPARE_MSK (0xFF << 7)
#define NDECCCTRL_ECC_SPARE(x)  (((x) << 7) & NDECCCTRL_ECC_SPARE_MSK)
#define NDECCCTRL_ECC_THR_MSK   (0x3F << 1)
#define NDECCCTRL_ECC_THRESH(x) (((x) << 1) & NDECCCTRL_ECC_THR_MSK)
#define NDECCCTRL_BCH_EN        (0x1)

/* error code and state */
enum {
	ERR_NONE	= 0,
	ERR_DMABUSERR	= -1,
	ERR_SENDCMD	= -2,
	ERR_DBERR	= -3,
	ERR_BBERR	= -4,
	ERR_SBERR	= -5,
};

enum {
	STATE_CMD_WAIT_DONE	= 1,
	STATE_DATA_PROCESSING	= (1 << 1),
	STATE_DATA_DONE		= (1 << 2),
	STATE_PAGE_DONE		= (1 << 3),
	STATE_CMD_DONE		= (1 << 4),
	STATE_READY		= (1 << 5),
	STATE_CMD_PREPARED	= (1 << 6),
	STATE_IS_WRITE		= (1 << 7),
};

#define STATE_MASK		(0x3f)
/* error code and state */
enum {
	ECC_NONE = 0,
	ECC_HAMMIN,
	ECC_BCH,
};

struct pxa3xx_nand_timing {
	uint32_t	tADL; /* Adress to Write Data delay */
	uint32_t	tCH;  /* Enable signal hold time */
	uint32_t	tCS;  /* Enable signal setup time */
	uint32_t	tWH;  /* ND_nWE high duration */
	uint32_t	tWP;  /* ND_nWE pulse time */
	uint32_t	tRH;  /* ND_nRE high duration */
	uint32_t	tRP;  /* ND_nRE pulse width */
	uint32_t	tR;   /* ND_nWE high to ND_nRE low for read */
	uint32_t	tRHW; /* delay for next command issue */
	uint32_t	tWHR; /* ND_nWE high to ND_nRE low for status read */
	uint32_t	tAR;  /* ND_ALE low to ND_nRE low delay */
};

struct pxa3xx_nand_cmdset {
	uint16_t	read1;
	uint16_t	read2;
	uint16_t	program;
	uint16_t	read_status;
	uint16_t	read_id;
	uint16_t	erase;
	uint16_t	reset;
	uint16_t	lock;
	uint16_t	unlock;
	uint16_t	lock_status;
};

struct pxa3xx_nand_flash {
	char		*name;
	uint16_t	chip_id;
	uint16_t	ext_id;
	uint16_t	page_per_block; /* Pages per block */
	uint16_t 	page_size;	/* Page size in bytes */
	uint8_t		flash_width;	/* Width of Flash memory (DWIDTH_M) */
	uint8_t 	dfc_width;	/* Width of flash controller(DWIDTH_C) */
	uint8_t		ecc_strength;	/* How strong a ecc should be applied */
	uint32_t	num_blocks;	/* Number of physical blocks in Flash */
	struct pxa3xx_nand_timing *timing;	/* NAND Flash timing */
};

struct pxa3xx_nand_info {
	/* page size of attached chip */
	int			page_addr;
	uint16_t		page_size;
	uint8_t			chip_select;
	uint8_t			ecc_strength;

	/* calculated from pxa3xx_nand_flash data */
	uint8_t			col_addr_cycles;
	uint8_t			row_addr_cycles;

	/* cached register value */
	uint32_t		reg_ndcr;
	uint32_t		ndtr0cs0;
	uint32_t		ndtr1cs0;
};

struct pxa3xx_nand {
	uint32_t		mmio_base;
	uint32_t		command;
	uint16_t		data_size;	/* data size in FIFO */
	uint16_t		oob_size;
	unsigned char		*data_buff;
	unsigned char		*oob_buff;
	uint8_t			chip_select;
	uint8_t			total_cmds;
	uint32_t		buf_start;
	uint32_t		buf_count;

	uint32_t		state;
	uint32_t		bad_count;
	uint16_t		data_column;
	uint16_t		oob_column;
	int 			retcode;
	uint8_t			ecc_strength;

	/* cached register value */
	uint8_t			cmd_seqs;
	uint8_t			wait_ready[CMD_POOL_SIZE];
	uint32_t		ndcb0[CMD_POOL_SIZE];
	uint32_t		ndcb1[CMD_POOL_SIZE];
	uint32_t		ndcb2;
};

static struct pxa3xx_nand nand;
static struct pxa3xx_nand_info pxa3xx_nand_info[CONFIG_SYS_MAX_NAND_DEVICE];
/* macros for registers read/write */
#define nand_writel(off, val)	\
	__raw_writel((val), nand.mmio_base + (off))

#define nand_readl(off)		\
	__raw_readl(nand.mmio_base + (off))

const static struct pxa3xx_nand_cmdset cmdset = {
	.read1		= 0x3000,
	.read2		= 0x0050,
	.program	= 0x1080,
	.read_status	= 0x0070,
	.read_id	= 0x0090,
	.erase		= 0xD060,
	.reset		= 0x00FF,
	.lock		= 0x002A,
	.unlock		= 0x2423,
	.lock_status	= 0x007A,
};

static struct pxa3xx_nand_timing timing[] = {
	/* common timing used to detect flash id */
	{   0, 40, 80, 60, 100, 80, 100, 90000,  0, 400, 40, },
	/* Samsung NAND series */
	{ 200, 10, 15, 20,  40, 30,  40, 11123, 20, 110, 10, },
	/* Micron NAND series */
	{   0, 10, 25, 15,  25, 15,  30, 25000,  0,  60, 10, },
	/* ST NAND series */
	{   0, 10, 35, 15,  25, 15,  25, 25000,  0,  60, 10, },
	/* Hynix NAND series */
	{   0, 10, 25, 15,  25, 15,  30, 25000, 0,   60, 10, },
};

static struct pxa3xx_nand_flash builtin_flash_types[] = {
{ 		0,	0,	0,   0,	   0,  0,  0, 0,    0, &timing[0], },
{ "64MiB 16-bit",  0x46ec, 0xffff,  32,  512, 16, 16, 1, 4096, &timing[1], },
{ "256MiB 8-bit",  0xdaec, 0xffff,  64, 2048,  8,  8, 1, 2048, &timing[1], },
{ "512MiB 16-bit", 0xbcec, 0xffff,  64, 4096, 16, 16, 4, 2048, &timing[1], },
{ "1GiB 8-bit",    0xd3ec, 0xffff, 128, 2048,  8,  8, 4, 4096, &timing[1], },
{ "4GiB 8-bit",    0xd7ec, 0x29d5, 128, 4096,  8,  8, 8, 8192, &timing[1], },
{ "128MiB 8-bit",  0xa12c, 0xffff,  64, 2048,  8,  8, 1, 1024, &timing[2], },
{ "128MiB 16-bit", 0xb12c, 0xffff,  64, 2048, 16, 16, 1, 1024, &timing[2], },
{ "256MiB 16-bit", 0xba2c, 0xffff,  64, 2048, 16, 16, 1, 2048, &timing[2], },
{ "512MiB 8-bit",  0xdc2c, 0xffff,  64, 2048,  8,  8, 1, 4096, &timing[2], },
{ "512MiB 16-bit", 0xcc2c, 0xffff,  64, 2048, 16, 16, 1, 4096, &timing[2], },
{ "1GiB 8-bit",    0x382c, 0xffff, 128, 4096,  8,  8, 4, 2048, &timing[2], },
{ "256MiB 16-bit", 0xba20, 0xffff,  64, 2048, 16, 16, 1, 2048, &timing[3], },
{ "512MiB 16-bit", 0xbcad, 0xffff,  64, 2048, 16, 16, 1, 4096, &timing[4], },
};

static struct nand_flash_dev pxa3xx_flash_ids[2] = {{NULL,}, {NULL,}};
#define NDTR0_tADL(c)		(min_t(uint32_t, (c), 31) << 27)
#define NDTR0_tCH(c)		(min_t(uint32_t, (c), 7) << 19)
#define NDTR0_tCS(c)		(min_t(uint32_t, (c), 7) << 16)
#define NDTR0_tWH(c)		(min_t(uint32_t, (c), 7) << 11)
#define NDTR0_tWP(c)		(min_t(uint32_t, (c), 7) << 8)
#define NDTR0_ETRP		(0x1 << 6)
#define NDTR0_tRH(c)		(min_t(uint32_t, (c), 7) << 3)
#define NDTR0_tRP(c)		(min_t(uint32_t, (c), 7) << 0)

#define NDTR1_tR(c)		(min_t(uint32_t, (c), 65535) << 16)
#define NDTR1_PRESCALE		(0x1 << 14)
#define NDTR1_tRHW(c)		(min_t(uint32_t, (c), 3) << 8)
#define NDTR1_tWHR(c)		(min_t(uint32_t, (c), 15) << 4)
#define NDTR1_tAR(c)		(min_t(uint32_t, (c), 15) << 0)

/* convert nano-seconds to nand flash controller clock cycles */
#define ns2cycle(ns, clk)	(int)((ns) * (clk / 1000000) / 1000)
static uint8_t naked_cmd_support = 0;
static uint8_t adv_time_tuning = 0;
uint8_t bch_support = 1;
static void pxa3xx_nand_set_timing(struct pxa3xx_nand_info *info,
				   const struct pxa3xx_nand_timing *t)
{
	unsigned long nand_clk = CONFIG_PXA3XX_NAND_DEF_CLOCK;
	uint32_t ndtr0, ndtr1, tRP, tR, tRHW, tADL;

	tR = ns2cycle(t->tR, nand_clk);
	tRP = ns2cycle(t->tRP, nand_clk);
	tRHW = tADL = ndtr0 = ndtr1 = 0;
	if (naked_cmd_support)
		tR = 0;
	if (adv_time_tuning) {
		if (tRP > 0x7) {
			ndtr0 |= NDTR0_ETRP;
			tRP -= 0x7;
		}
		if (tR > 0xffff) {
			ndtr1 |= NDTR1_PRESCALE;
			tR /= 16;
		}
		if (t->tRHW > 0) {
			tRHW = ns2cycle(t->tRHW, nand_clk);
			if (tRHW < 16)
				tRHW = 1;
			else {
				if (tRHW < 32)
					tRHW = 2;
				else
					tRHW = 3;
			}
		}
		tADL = ns2cycle(t->tADL, nand_clk);
	}

	ndtr0 |= NDTR0_tADL(tADL)
		| NDTR0_tCH(ns2cycle(t->tCH, nand_clk))
		| NDTR0_tCS(ns2cycle(t->tCS, nand_clk))
		| NDTR0_tWH(ns2cycle(t->tWH, nand_clk))
		| NDTR0_tWP(ns2cycle(t->tWP, nand_clk))
		| NDTR0_tRH(ns2cycle(t->tRH, nand_clk))
		| NDTR0_tRP(tRP);

	ndtr1 |= NDTR1_tR(tR)
		| NDTR1_tRHW(tRHW)
		| NDTR1_tWHR(ns2cycle(t->tWHR, nand_clk))
		| NDTR1_tAR(ns2cycle(t->tAR, nand_clk));

	info->ndtr0cs0 = ndtr0;
	info->ndtr1cs0 = ndtr1;
	nand_writel(NDTR0CS0, ndtr0);
	nand_writel(NDTR1CS0, ndtr1);
}

static void pxa3xx_set_datasize(struct pxa3xx_nand_info *info)
{
	int oob_enable = info->reg_ndcr & NDCR_SPARE_EN;

	if (info->page_size < PAGE_CHUNK_SIZE) {
		nand.data_size = 512;
		if (!oob_enable) {
			nand.oob_size = 0;
			return;
		}

		switch (nand.ecc_strength) {
		case 0:
			nand.oob_size = 16;
			break;
		case HAMMING_STRENGTH:
			nand.oob_size = 8;
			break;
		default:
			printk("Don't support BCH on small page device!!!\n");
			BUG();
		}
		return;
	}
	nand.data_size = PAGE_CHUNK_SIZE;
	if (!oob_enable) {
		nand.oob_size = 0;
		return;
	}

	if (nand.command == NAND_CMD_READOOB) {
		switch (info->ecc_strength) {
		case HAMMING_STRENGTH:
			nand.ndcb1[3] = 2 * PAGE_CHUNK_SIZE + OOB_CHUNK_SIZE;
			nand.data_size = 64;
			break;
		case BCH_STRENGTH:
			nand.ndcb1[3] = 2 * PAGE_CHUNK_SIZE + OOB_CHUNK_SIZE - 2;
			nand.data_size = 62;
			break;
		default:
			BUG();
		}
		if (info->reg_ndcr & NDCR_DWIDTH_M) {
			nand.ndcb1[1] = PAGE_CHUNK_SIZE / 2;
			nand.ndcb1[3] /= 2;
		}
		else
			nand.ndcb1[1] = PAGE_CHUNK_SIZE;
		nand.oob_size = 0;
		return;
	}
	switch (nand.ecc_strength) {
	case 0:
		nand.oob_size = 64;
		break;
	case HAMMING_STRENGTH:
		nand.oob_size = 40;
		break;
	default:
		nand.oob_size = 32;
	}
}

/**
 * NOTE: it is a must to set ND_RUN firstly, then write
 * command buffer, otherwise, it does not work.
 * We enable all the interrupt at the same time, and
 * let pxa3xx_nand_irq to handle all logic.
 */
static void pxa3xx_nand_start(struct pxa3xx_nand_info *info)
{
	uint32_t ndcr, ndeccctrl = 0;

	ndcr = info->reg_ndcr | NDCR_INT_MASK | NDCR_ND_RUN;
	switch (nand.ecc_strength) {
	default:
		ndeccctrl |= NDECCCTRL_BCH_EN;
		ndeccctrl |= NDECCCTRL_ECC_THRESH(BCH_THRESHOLD);
	case HAMMING_STRENGTH:
		ndcr |= NDCR_ECC_EN;
	case 0:
		break;
	}

	/* clear status bits and run */
	nand_writel(NDCR, 0);
	nand_writel(NDECCCTRL, ndeccctrl);
	nand_writel(NDSR, NDSR_MASK);
	nand_writel(NDCR, ndcr);
}

static void nand_error_dump(void)
{
	int i;

	printk(KERN_ERR "NAND controller state wrong!!!\n");
	printk(KERN_ERR "command %x, state %x, current seqs %d, errcode %x, bad count %d\n",
			nand.command, nand.state, nand.cmd_seqs,
			nand.retcode, nand.bad_count);
	printk(KERN_ERR "Totally %d command for sending\n",
			nand.total_cmds);
	for (i = 0; i < nand.total_cmds; i ++)
		printk(KERN_ERR "%d::NDCB0: %x, NDCB1: %x, NDCB2: %x\n",
			i, nand.ndcb0[i], nand.ndcb1[i], nand.ndcb2);

	printk(KERN_ERR "\nRegister DUMPing ##############\n");
	printk(KERN_ERR "NDCR %x\n"
			"NDSR %x\n"
			"NDCB0 %x\n"
			"NDCB1 %x\n"
			"NDCB2 %x\n"
			"NDTR0CS0 %x\n"
			"NDTR1CS0 %x\n"
			"NDBDR0 %x\n"
			"NDBDR1 %x\n"
			"NDREDEL %x\n"
			"NDECCCTRL %x\n"
			"NDBZCNT %x\n\n",
			nand_readl(NDCR),
			nand_readl(NDSR),
			nand_readl(NDCB0),
			nand_readl(NDCB1),
			nand_readl(NDCB2),
			nand_readl(NDTR0CS0),
			nand_readl(NDTR1CS0),
			nand_readl(NDBDR0),
			nand_readl(NDBDR1),
			nand_readl(NDREDEL),
			nand_readl(NDECCCTRL),
			nand_readl(NDBZCNT));
}

static void handle_data_pio(void)
{
	unsigned int data_size, oob_size;

	data_size = DIV_ROUND_UP(nand.data_size, 4);
	oob_size = DIV_ROUND_UP(nand.oob_size, 4);
	if (nand.state & STATE_IS_WRITE) {
		__raw_writesl(nand.mmio_base + NDDB,
				nand.data_buff + nand.data_column, data_size);
		if (nand.oob_size > 0)
			__raw_writesl(nand.mmio_base + NDDB,
				nand.oob_buff + nand.oob_column, oob_size);
	}
	else {
		__raw_readsl(nand.mmio_base + NDDB,
				nand.data_buff + nand.data_column, data_size);
		if (nand.oob_size > 0)
			__raw_readsl(nand.mmio_base + NDDB,
				nand.oob_buff + nand.oob_column, oob_size);
	}
	nand.data_column += (data_size << 2);
	nand.oob_column += (oob_size << 2);
}

static int pxa3xx_nand_transaction(struct pxa3xx_nand_info *info)
{
	unsigned int status, is_completed = 0, cs, cmd_seqs;
	unsigned int ready, cmd_done, page_done, badblock_detect, ndcb2;

	cs		= nand.chip_select;
	ready           = (cs) ? NDSR_RDY : NDSR_FLASH_RDY;
	cmd_done        = (cs) ? NDSR_CS1_CMDD : NDSR_CS0_CMDD;
	page_done       = (cs) ? NDSR_CS1_PAGED : NDSR_CS0_PAGED;
	badblock_detect = (cs) ? NDSR_CS1_BBD : NDSR_CS0_BBD;
	cmd_seqs	= nand.cmd_seqs;

	status = nand_readl(NDSR);
	nand.bad_count = (status & NDSR_ERR_CNT_MASK) >> 16;
	if (status & NDSR_SBERR)
		nand.retcode = ERR_SBERR;
	if (status & NDSR_DBERR)
		nand.retcode = ERR_DBERR;
	if (status & badblock_detect)
		nand.retcode = ERR_BBERR;

	if (status & (NDSR_RDDREQ | NDSR_WRDREQ)) {
		nand.state |= STATE_DATA_PROCESSING;
		handle_data_pio();
		nand.state |= STATE_DATA_DONE;
	}
	if (status & page_done)
		nand.state |= STATE_PAGE_DONE;
	if (status & ready) {
		nand.state |= STATE_READY;
		if (nand.wait_ready[cmd_seqs] && cmd_seqs == nand.total_cmds)
				is_completed = 1;
	}
	if (status & cmd_done) {
		nand.state |= STATE_CMD_DONE;
		if (nand.wait_ready[cmd_seqs] && !(nand.state & STATE_READY))
			status &= ~cmd_done;
		if (cmd_seqs == nand.total_cmds && !nand.wait_ready[cmd_seqs])
			is_completed = 1;
	}

	if (status & NDSR_WRCMDREQ) {
		status &= ~NDSR_WRCMDREQ;
		if (nand.wait_ready[cmd_seqs] && !(nand.state & STATE_READY))
			goto IRQ_FORCE_EXIT;

		nand_writel(NDSR, NDSR_WRCMDREQ);
		if (cmd_seqs < nand.total_cmds) {
			if (cmd_seqs == 0)
				ndcb2 = nand.ndcb2;
			else
				ndcb2 = 0;
			nand.cmd_seqs ++;
			nand.state &= ~STATE_MASK;
			nand.state |= STATE_CMD_WAIT_DONE;
			nand_writel(NDCB0, nand.ndcb0[cmd_seqs]);
			nand_writel(NDCB0, nand.ndcb1[cmd_seqs]);
			nand_writel(NDCB0, ndcb2);
			if (nand.ndcb0[cmd_seqs] & NDCB0_LEN_OVRD)
				nand_writel(NDCB0, nand.data_size + nand.oob_size);
		}
		else
			is_completed = 1;
	}

IRQ_FORCE_EXIT:
	/* clear NDSR to let the controller exit the IRQ */
	nand_writel(NDSR, status);
	return is_completed;
}

static int pxa3xx_nand_polling(struct pxa3xx_nand_info *info, unsigned long timeout)
{
	int i, ret = 0;

	for (i = 0; i < timeout; i++) {
		ret = pxa3xx_nand_transaction(info);
		if (ret)
			break;
		udelay(10);
	}

	return ret;
}

static inline int is_buf_blank(uint8_t *buf, size_t len)
{
	for (; len > 0; len--)
		if (*buf++ != 0xff)
			return 0;
	return 1;
}

static int prepare_command_pool(struct mtd_info *mtd, int command,
		uint16_t column, int page_addr)
{
	uint16_t cmd;
	int addr_cycle, exec_cmd, ndcb0, i, chunks = 0;
	struct nand_chip *chip = mtd->priv;
	struct pxa3xx_nand_info *info = chip->priv;

	ndcb0 = (nand.chip_select) ? NDCB0_CSEL : 0;;
	addr_cycle = 0;
	exec_cmd = 1;

	/* reset data and oob column point to handle data */
	nand.total_cmds = 1;
	nand.buf_start	= column;

	switch (command) {
	case NAND_CMD_PAGEPROG:
	case NAND_CMD_RNDOUT:
		pxa3xx_set_datasize(info);
		chunks = info->page_size / PAGE_CHUNK_SIZE;
		if (info->ecc_strength > BCH_STRENGTH) {
			i = info->ecc_strength / BCH_STRENGTH;
			nand.data_size /= i;
			ndcb0 |= NDCB0_LEN_OVRD;
			chunks *= i;
		}
		break;
	case NAND_CMD_READOOB:
		if (info->ecc_strength > BCH_STRENGTH) {
			printk(KERN_ERR "we don't support oob command if use"
				     " 8bit per 512bytes ecc feature!!\n");
			BUG();
		}
	case NAND_CMD_ERASE2:
		return 0;
	default:
		i = (uint32_t)(&nand.state) - (uint32_t)&nand;
		memset(&nand.state, 0, sizeof(struct pxa3xx_nand) - i);
		break;
	}
	/* clear the command buffer */
	for (i = 0; i < CMD_POOL_SIZE; i ++)
		nand.ndcb0[i] = ndcb0;
	addr_cycle = NDCB0_ADDR_CYC(info->row_addr_cycles
			+ info->col_addr_cycles);

	switch (command) {
	case NAND_CMD_READ0:
	case NAND_CMD_SEQIN:
		nand.ecc_strength = info->ecc_strength;
	case NAND_CMD_READOOB:
		memset(nand.data_buff, 0xff, column);
		nand.buf_count = mtd->writesize + mtd->oobsize;
		exec_cmd = 0;
		info->page_addr = page_addr;
		/* small page addr setting */
		if (unlikely(info->page_size < PAGE_CHUNK_SIZE))
			nand.ndcb1[0] = ((page_addr & 0xFFFFFF) << 8);
		else {
			nand.ndcb1[0] = ((page_addr & 0xFFFF) << 16);

			if (page_addr & 0xFF0000)
				nand.ndcb2 = (page_addr & 0xFF0000) >> 16;
		}
		break;

	case NAND_CMD_RNDOUT:
		cmd = cmdset.read1;
		if (unlikely(info->page_size < PAGE_CHUNK_SIZE) || !naked_cmd_support) {
			if (unlikely(info->page_size < PAGE_CHUNK_SIZE))
				nand.ndcb0[0] |= NDCB0_CMD_TYPE(0)
						| addr_cycle
						| (cmd & NDCB0_CMD1_MASK);
			else
				nand.ndcb0[0] |= NDCB0_CMD_TYPE(0)
						| NDCB0_DBC
						| addr_cycle
						| cmd;
			if (nand.command == NAND_CMD_READOOB) {
				nand.buf_start = mtd->writesize + column;
				nand.buf_count = mtd->oobsize;
			}
			break;
		}

		if (nand.command == NAND_CMD_READOOB) {
			nand.total_cmds = 2 * chunks + 1;
			memset(nand.data_buff + nand.data_size, 0xff,
					mtd->oobsize - nand.data_size);
		}
		else {
			nand.total_cmds = chunks + 1;
			memset(nand.oob_buff + nand.oob_size, 0xff,
					mtd->oobsize - nand.oob_size);
		}

		nand.ndcb0[0] |= NDCB0_CMD_XTYPE(0x6)
				| NDCB0_CMD_TYPE(0)
				| NDCB0_DBC
				| NDCB0_NC
				| addr_cycle
				| cmd;
		nand.ndcb0[0] &= ~NDCB0_LEN_OVRD;

		for (i = 1; i <= nand.total_cmds - 1;) {
			if (nand.command == NAND_CMD_READOOB) {
				nand.ndcb0[i ++] |= NDCB0_CMD_XTYPE(0x6)
						| NDCB0_CMD_TYPE(0)
						| NDCB0_ADDR_CYC(info->col_addr_cycles)
						| NDCB0_DBC
						| NDCB0_NC
						| (NAND_CMD_RNDOUTSTART << 8)
						| NAND_CMD_RNDOUT;
				nand.ndcb0[i] |= NDCB0_LEN_OVRD;
			}
			nand.ndcb0[i ++] |= NDCB0_CMD_XTYPE(0x5)
					| NDCB0_NC;
		}

		nand.ndcb0[nand.total_cmds - 1] &= ~NDCB0_NC;
		/* we should wait RnB go high again
		 * before read out data*/
		nand.wait_ready[1] = 1;

		break;

	case NAND_CMD_PAGEPROG:
		if (is_buf_blank(nand.data_buff, (mtd->writesize + mtd->oobsize))) {
			exec_cmd = 0;
			break;
		}

		cmd = cmdset.program;
		nand.state |= STATE_IS_WRITE;
		if (unlikely(info->page_size < PAGE_CHUNK_SIZE) || !naked_cmd_support) {
			nand.ndcb0[0] |= NDCB0_CMD_TYPE(0x1)
					| NDCB0_AUTO_RS
					| NDCB0_ST_ROW_EN
					| NDCB0_DBC
					| cmd
					| addr_cycle;
			break;
		}

		nand.total_cmds = chunks + 1;
		nand.ndcb0[0] |= NDCB0_CMD_XTYPE(0x4)
				| NDCB0_CMD_TYPE(0x1)
				| NDCB0_NC
				| NDCB0_AUTO_RS
				| (cmd & NDCB0_CMD1_MASK)
				| addr_cycle;

		for (i = 1; i < chunks; i ++)
			nand.ndcb0[i] |= NDCB0_CMD_XTYPE(0x5)
					| NDCB0_NC
					| NDCB0_AUTO_RS
					| NDCB0_CMD_TYPE(0x1);

		nand.ndcb0[chunks] |= NDCB0_CMD_XTYPE(0x3)
					| NDCB0_CMD_TYPE(0x1)
					| NDCB0_ST_ROW_EN
					| NDCB0_DBC
					| (cmd & NDCB0_CMD2_MASK)
					| NDCB0_CMD1_MASK;
		nand.ndcb0[chunks] &= ~NDCB0_LEN_OVRD;
		/* we should wait for RnB goes high which
		 * indicate the data has been written succesfully*/
		nand.wait_ready[nand.total_cmds] = 1;
		break;

	case NAND_CMD_READID:
		cmd = cmdset.read_id;
		nand.data_buff = chip->buffers->databuf;
		nand.buf_count = READ_ID_BYTES;
		nand.ndcb0[0] |= NDCB0_CMD_TYPE(3)
				| NDCB0_ADDR_CYC(1)
				| cmd;

		nand.data_size = 8;
		break;

	case NAND_CMD_STATUS:
		cmd = cmdset.read_status;
		nand.data_buff = chip->buffers->databuf;
		nand.buf_count = 1;
		nand.ndcb0[0] |= NDCB0_CMD_TYPE(4)
				| NDCB0_ADDR_CYC(1)
				| cmd;

		nand.data_size = 8;
		break;

	case NAND_CMD_ERASE1:
		cmd = cmdset.erase;
		nand.ndcb0[0] |= NDCB0_CMD_TYPE(2)
				| NDCB0_AUTO_RS
				| NDCB0_ADDR_CYC(3)
				| NDCB0_DBC
				| cmd;
		nand.ndcb1[0] = page_addr;

		break;
	case NAND_CMD_RESET:
		cmd = cmdset.reset;
		nand.ndcb0[0] |= NDCB0_CMD_TYPE(5)
				| cmd;

		break;

	default:
		exec_cmd = 0;
		printf("non-supported command %x\n", command);
		BUG();
		break;
	}

	nand.command = command;
	return exec_cmd;
}

static void pxa3xx_nand_cmdfunc(struct mtd_info *mtd, unsigned command,
				int column, int page_addr)
{
	struct pxa3xx_nand_info *info = ((struct nand_chip *)mtd->priv)->priv;
	struct nand_chip *chip = mtd->priv;
	struct pxa3xx_bbm *pxa3xx_bbm = mtd->bbm;
	int ret, exec_cmd;
	loff_t addr;

	/* reset timing */
	if (nand.chip_select != info->chip_select) {
		nand.chip_select = info->chip_select;
		nand_writel(NDTR0CS0, info->ndtr0cs0);
		nand_writel(NDTR1CS0, info->ndtr1cs0);
	}

	if (pxa3xx_bbm && (command == NAND_CMD_READOOB
				|| command == NAND_CMD_READ0
				|| command == NAND_CMD_SEQIN
				|| command == NAND_CMD_ERASE1)) {

		addr = (loff_t)page_addr << chip->page_shift;
		addr = pxa3xx_bbm->search(mtd, addr);
		page_addr = addr >> chip->page_shift;
	}
	exec_cmd = prepare_command_pool(mtd, command, column, page_addr);
	if (exec_cmd) {
		nand.state |= STATE_CMD_PREPARED;
		pxa3xx_nand_start(info);
		ret = pxa3xx_nand_polling(info, CHIP_DELAY_TIMEOUT);
		if (!ret) {
			printf("Wait time out!!!\n");
			nand_error_dump();
		}
		nand.state &= ~STATE_CMD_PREPARED;
	}
	if (command == NAND_CMD_ERASE1 && nand.retcode == ERR_BBERR)
		pxa3xx_block_markbad(mtd, addr);
}

static uint8_t pxa3xx_nand_read_byte(struct mtd_info *mtd)
{
	char retval = 0xFF;
	if (nand.buf_start < nand.buf_count)
		/* Has just send a new command? */
		retval = nand.data_buff[nand.buf_start++];

	return retval;
}

static u16 pxa3xx_nand_read_word(struct mtd_info *mtd)
{
	u16 retval = 0xFFFF;
	if (!(nand.buf_start & 0x01) && nand.buf_start < nand.buf_count) {
		retval = *((u16 *)(nand.data_buff+nand.buf_start));
		nand.buf_start += 2;
	}
	return retval;
}

static void pxa3xx_nand_read_buf(struct mtd_info *mtd, uint8_t *buf, int len)
{
	return;
}

static void pxa3xx_nand_write_buf(struct mtd_info *mtd,
		const uint8_t *buf, int len)
{
	return;
}

static int pxa3xx_nand_verify_buf(struct mtd_info *mtd,
		const uint8_t *buf, int len)
{
	return 0;
}

static void pxa3xx_nand_select_chip(struct mtd_info *mtd, int chip)
{
	return;
}

static int pxa3xx_nand_waitfunc(struct mtd_info *mtd, struct nand_chip *this)
{
	/* pxa3xx_nand_send_command has waited for command complete */
	if (this->state == FL_WRITING || this->state == FL_ERASING) {
		if (nand.retcode == ERR_NONE)
			return 0;
		else
			return NAND_STATUS_FAIL;
	}

	return 0;
}

static void pxa3xx_nand_config_flash(struct pxa3xx_nand_info *info,
				    const struct pxa3xx_nand_flash *f)
{
	uint32_t ndcr = 0;
	if (f->page_size > PAGE_CHUNK_SIZE && !naked_cmd_support) {
		printk(KERN_ERR "Your controller don't support 4k or larger "
			       "page NAND for don't support naked command\n");
		BUG();
	}
	/* calculate flash information */
	if (f->ecc_strength != 0 && f->ecc_strength != HAMMING_STRENGTH
			&& (f->ecc_strength % BCH_STRENGTH != 0)) {
		printk(KERN_ERR "ECC strength definition error, please recheck!!\n");
		BUG();
	}
	info->ecc_strength = (bch_support) ? f->ecc_strength : 1;
	info->page_size = f->page_size;

	/* calculate addressing information */
	info->col_addr_cycles = (f->page_size >= 2048) ? 2 : 1;

	if (f->num_blocks * f->page_per_block > 65536)
		info->row_addr_cycles = 3;
	else
		info->row_addr_cycles = 2;

	ndcr |= (info->col_addr_cycles == 2) ? NDCR_RA_START : 0;
	ndcr |= (f->flash_width == 16) ? NDCR_DWIDTH_M : 0;
	ndcr |= (f->dfc_width == 16) ? NDCR_DWIDTH_C : 0;
	switch (f->page_per_block) {
		case 32:
			ndcr |= NDCR_PG_PER_BLK(0x0);
			break;
		case 128:
			ndcr |= NDCR_PG_PER_BLK(0x1);
			break;
		case 256:
			ndcr |= NDCR_PG_PER_BLK(0x3);
			break;
		case 64:
		default:
			ndcr |= NDCR_PG_PER_BLK(0x2);
			break;
	}

	switch (f->page_size) {
		case 512:
			ndcr |= NDCR_PAGE_SZ(0x0);
			break;
		case 2048:
		default:
			ndcr |= NDCR_PAGE_SZ(0x1);
			ndcr |= NDCR_FORCE_CSX;
			break;

	}

	ndcr |= NDCR_RD_ID_CNT(READ_ID_BYTES);
	/* only enable spare area when ecc is lower than 8bits per 512 bytes */
	if (f->ecc_strength <= BCH_STRENGTH)
		ndcr |= NDCR_SPARE_EN;

	info->reg_ndcr = ndcr | NDCR_ND_ARB_EN;

	pxa3xx_nand_set_timing(info, f->timing);
}

/* the max buff size should be large than the largest size
 * of page of NAND flash that currently controller support
 */
#define MAX_BUFF_SIZE	((PAGE_CHUNK_SIZE + OOB_CHUNK_SIZE) * 2)
static void pxa3xx_read_page(struct mtd_info *mtd, uint8_t *buf)
{
	struct pxa3xx_nand_info *info = ((struct nand_chip *)mtd->priv)->priv;
	struct nand_chip *chip = mtd->priv;
	int buf_blank;

	nand.data_buff = buf;
	nand.oob_buff = chip->oob_poi;
	pxa3xx_nand_cmdfunc(mtd, NAND_CMD_RNDOUT, 0, info->page_addr);
	switch (nand.retcode) {
	case ERR_SBERR:
		switch (nand.ecc_strength) {
		default:
			if (nand.bad_count > BCH_THRESHOLD)
				mtd->ecc_stats.corrected +=
					(nand.bad_count - BCH_THRESHOLD);
			break;

		case HAMMING_STRENGTH:
			mtd->ecc_stats.corrected ++;
		case 0:
			break;
		}
		break;
	case ERR_DBERR:
		buf_blank = is_buf_blank(nand.data_buff, mtd->writesize);
		if (!buf_blank)
			mtd->ecc_stats.failed++;
		break;
	case ERR_NONE:
		break;
	default:
		mtd->ecc_stats.failed++;
		break;
	}
}

static int pxa3xx_nand_read_page_hwecc(struct mtd_info *mtd,
		struct nand_chip *chip, uint8_t *buf, int page)
{
	pxa3xx_read_page(mtd, buf);
	return 0;
}

static int pxa3xx_nand_read_oob(struct mtd_info *mtd, struct nand_chip *chip,
		int page, int sndcmd)
{
	if (sndcmd) {
		pxa3xx_nand_cmdfunc(mtd, NAND_CMD_READOOB, 0, page);
		pxa3xx_read_page(mtd, chip->oob_poi);
	}
	return 0;
}

static void pxa3xx_nand_write_page_hwecc(struct mtd_info *mtd,
		struct nand_chip *chip, const uint8_t *buf)
{
	nand.data_buff = (uint8_t *)buf;
	nand.oob_buff = chip->oob_poi;
}

static int pxa3xx_nand_write_oob(struct mtd_info *mtd, struct nand_chip *chip,
			      int page)
{
	int status = 0;

	chip->cmdfunc(mtd, NAND_CMD_SEQIN, mtd->writesize, page);
	nand.oob_buff = chip->oob_poi;
	/* Send command to program the OOB data */
	chip->cmdfunc(mtd, NAND_CMD_PAGEPROG, -1, -1);

	status = chip->waitfunc(mtd, chip);

	return status & NAND_STATUS_FAIL ? -EIO : 0;
}

static int pxa3xx_nand_scan(struct nand_chip *chip)
{
	struct pxa3xx_nand_info *info = chip->priv;
	const struct pxa3xx_nand_flash *f = NULL;
	uint16_t *id;
	uint64_t chipsize;
	int i;

	nand.chip_select = info->chip_select;
	pxa3xx_nand_config_flash(info, &builtin_flash_types[0]);
	chip->cmdfunc(&nand_info[nand.chip_select], NAND_CMD_RESET, 0, 0);
	if (!(nand.state & STATE_READY))
		goto ERR_OUT;

	chip->cmdfunc(&nand_info[nand.chip_select], NAND_CMD_READID, 0, 0);
	id = (uint16_t *)nand.data_buff;
	if (id[0] == 0) {
		printf("Read out ID 0, potential timing set wrong!!\n");
		goto ERR_OUT;
	}

	for (i=0; i<ARRAY_SIZE(builtin_flash_types); i++) {
		f = &builtin_flash_types[i];

		/* find the chip in default list */
		if ((f->chip_id == id[0]) && ((f->ext_id & id[1]) == id[1]))
			break;
	}

	if (i >= ARRAY_SIZE(builtin_flash_types)) {
		printf("ERROR!! flash %x not defined!!!\n", id[0]);
		goto ERR_OUT;
	}

	pxa3xx_nand_config_flash(info, f);
	chip->ecc.mode = NAND_ECC_HW;
	chip->ecc.size = f->page_size;
	chip->options |= (info->reg_ndcr & NDCR_DWIDTH_M) ? NAND_BUSWIDTH_16: 0;
	chip->options |= NAND_NO_READRDY;
	chip->options |= NAND_USE_FLASH_BBT;
	pxa3xx_flash_ids[0].name = f->name;
	pxa3xx_flash_ids[0].id = (f->chip_id >> 8) & 0xffff;
	pxa3xx_flash_ids[0].pagesize = f->page_size;
	chipsize = (uint64_t)f->num_blocks * f->page_per_block * f->page_size;
	pxa3xx_flash_ids[0].chipsize = chipsize >> 20;
	pxa3xx_flash_ids[0].erasesize = f->page_size * f->page_per_block;
	pxa3xx_flash_ids[0].options = (f->flash_width == 16) ? NAND_BUSWIDTH_16 : 0;
	predef_ids = pxa3xx_flash_ids;
	return 0;
ERR_OUT:
	predef_ids = NULL;
	return -EINVAL;
}

static int cur_int = -1;
int board_nand_init(struct nand_chip *chip)
{
	struct pxa3xx_nand_info *info;

	if (cur_int == -1) {
		nand.mmio_base = (uint32_t)chip->IO_ADDR_W;
		cur_int = 0;
	}
	else
		cur_int ++;
#ifdef PXA3XX_NAND_NAKED_CMD_SUPPORT
	naked_cmd_support = 1;
#endif
	info = &pxa3xx_nand_info[cur_int];
	chip->ecc.read_page	= pxa3xx_nand_read_page_hwecc;
	chip->ecc.read_page_raw = pxa3xx_nand_read_page_hwecc;
	chip->ecc.read_oob      = pxa3xx_nand_read_oob;
	chip->ecc.write_page	= pxa3xx_nand_write_page_hwecc;
	chip->ecc.write_page_raw= pxa3xx_nand_write_page_hwecc;
	chip->ecc.write_oob     = pxa3xx_nand_write_oob;
	chip->waitfunc		= pxa3xx_nand_waitfunc;
	chip->select_chip	= pxa3xx_nand_select_chip;
	chip->cmdfunc		= pxa3xx_nand_cmdfunc;
	chip->read_word		= pxa3xx_nand_read_word;
	chip->read_byte		= pxa3xx_nand_read_byte;
	chip->read_buf		= pxa3xx_nand_read_buf;
	chip->write_buf		= pxa3xx_nand_write_buf;
	chip->verify_buf	= pxa3xx_nand_verify_buf;
#ifdef CONFIG_PXA3XX_BBM
	chip->scan_bbt		= pxa3xx_scan_bbt;
	chip->block_markbad	= pxa3xx_block_markbad;
	chip->block_bad		= pxa3xx_block_bad;
#endif
	chip->priv = info;
	info->chip_select = cur_int;
	return pxa3xx_nand_scan(chip);
}
