LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:=\
	lsusb.c\
	usbmisc.c\
	devtree.c\
	names.c\
	lsusb-t.c

LOCAL_CFLAGS := -O2 -g
LOCAL_CFLAGS += -DHAVE_CONFIG_H -D'DATADIR="/etc"'

LOCAL_SHARED_LIBRARIES += libusb libcutils

LOCAL_C_INCLUDES := $(KERNEL_HEADERS) \
	external/libusb \
	external/libusb/libusb

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE := lsusb

include $(BUILD_EXECUTABLE)


include $(CLEAR_VARS)
LOCAL_MODULE := usb.ids
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH :=$(TARGET_OUT)/etc
LOCAL_SRC_FILES := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)


