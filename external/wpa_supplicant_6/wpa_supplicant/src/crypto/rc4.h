/*
 * RC4 stream cipher
 * Copyright (c) 2002-2005, Jouni Malinen <j@w1.fi>
 *
 */

#ifndef RC4_H
#define RC4_H

void rc4_skip(const u8 *key, size_t keylen, size_t skip,
	      u8 *data, size_t data_len);

#endif /* RC4_H */
