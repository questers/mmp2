/** @file sms4c.h
  * @brief This file contains functions for SMS4
 *
 *  Copyright (C) 2001-2008, Iwncomm Ltd.
 */

void SMS4Crypt(unsigned char *Input, unsigned char *Output, unsigned int *rk);

void SMS4KeyExt(unsigned char *Key, unsigned int *rk, unsigned int CryptFlag);
