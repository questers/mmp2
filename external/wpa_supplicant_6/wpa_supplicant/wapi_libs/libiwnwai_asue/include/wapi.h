/** @file libiwnwai_asue/include/wapi.h
 *  @brief This header file contains data structures and function declarations of wapi
 *
 *  Copyright (C) 2001-2008, Iwncomm Ltd.
 */

#ifndef _WAPI_ASUE_H_
#define _WAPI_ASUE_H_

/** Block length */
#define BK_LEN 16
/** Page length */
#define PAGE_LEN	4096
/** Block id length */
#define BKID_LEN	16
/** Challenge length */
#define CHALLENGE_LEN	32
/** Pairkey length */
#define PAIRKEY_LEN	16
/** USKSA length */
#define USKSA_LEN	4*PAIRKEY_LEN + CHALLENGE_LEN
/** Addid length */
#define ADDID_LEN	ETH_ALEN*2
/** MIC length */
#define WAI_MIC_LEN	20
/** IV length */
#define WAI_IV_LEN	16
/** Key and ID length */
#define WAI_KEY_AN_ID_LEN	16
/** Flag: is usk enabled? */
#define ISUSK		1
/** WAI header */
#define WAI_HDR	12
/** multicast text */
#define MSK_TEXT "multicast or station key expansion for station unicast and multicast and broadcast"
/** usk text */
#define USK_TEXT "pairwise key expansion for unicast and additional keys and nonce"

/** enumeration: WAPI states */
typedef enum
{
    WAPISM_N0_ASSOC = 0,        /* not association(pre association) */
    WAPISM_AL_ASSOC = 1,        /* already association */
    WAPISM_CNT_REQ = 2,         /* (already send 4) */
    WAPISM_CNT_RESP = 3,        /* (already recv 5) */
    WAPISM_UNI_RESP = 4,        /* (already send 9) */
    WAPISM_UNI_ACK = 5,         /* (already recv 10) */
    WAPISM_MUL_RESP = 6,        /* (already send 12) */
    WAPISM_FINISH = 7,          /* (finished and opened port) */
    WAPISM_UPDATE_BK,
    WAPISM_UPDATE_USK,
    WAPISM_UPDATE_MSK,
} wapi_states;

/** Structure WAI header */
struct wai_hdr
{
        /** version */
    u8 version[2];
        /** type */
    u8 type;
        /** stype */
    u8 stype;
        /** reserved*/
    u8 reserve[2];
        /** length */
    u16 length;
        /** rx sequnce */
    u8 rxseq[2];
        /** frag_sc */
    u8 frag_sc;
        /** more_frag */
    u8 more_frag;
    /* followed octets of data */
};

/** Structure: wapi_bksa_cache */
struct wapi_bksa_cache
{
        /** Block id */
    u8 bkid[BKID_LEN];
        /** Block */
    u8 bk[BK_LEN];
        /** asue auth flag */
    u8 asue_auth_flag[32];
        /** Block length */
    size_t bk_len;
        /** akmp */
    int akmp;                   /* WAPI_KEY_MGMT_* */
        /** ae_mac */
    u8 ae_mac[ETH_ALEN];
        /** pad */
    u8 pad[2];
};

/** Structure: wapi_usk */
struct wapi_usk
{
    u8 uek[PAIRKEY_LEN];     /**< Unicast Encryption Key */
    u8 uck[PAIRKEY_LEN];     /**< Unicast Integrity check Key (UCK) */
    u8 mak[PAIRKEY_LEN];     /**< Message Authentication Key (MAK)*/
    u8 kek[PAIRKEY_LEN];     /**< Key Encryption Key (KEK) */
    u8 ae_challenge[CHALLENGE_LEN];     /**< ae challenge */
    u8 asue_challenge[CHALLENGE_LEN];     /**< asue challenge */
};

/** Structure: wapi_usksa */
struct wapi_usksa
{
        /** usk ID */
    u8 uskid;
        /** usk pad */
    u8 usk_pad[3];
        /** usk */
    struct wapi_usk usk[2];
        /** unicast suite */
    int ucast_suite;
        /** ae mac*/
    u8 ae_mac[ETH_ALEN];
        /** mac pad */
    u8 mac_pad[2];
};

/** structure: wapi_msksa*/
struct wapi_msksa
{
        /** direction */
    u8 direction;
        /** msk id */
    u8 mskid;
        /** msk pad */
    u8 msk_pad[2];
        /** msk */
    u8 msk[32];
        /** msk ann id */
    u8 msk_ann_id[16];
        /** unicast suite */
    int ucast_suite;
        /** ae mac */
    u8 ae_mac[ETH_ALEN];
        /** mac pad */
    u8 mac_pad[2];
};

/** Structure: WAPI state machine */
struct wapi_state_machine
{
        /** usksa */
    struct wapi_usksa usksa;
        /** msksa */
    struct wapi_msksa msksa;
    struct wapi_bksa_cache bksa;     /**< PMKSA cache */
    int bksa_count;     /**< number of entries in PMKSA cache */

        /** own address */
    u8 own_addr[ETH_ALEN];
        /** own address pad */
    u8 own_addr_pad[2];
        /** interface name */
    const char *ifname;
        /** BSSID */
    u8 bssid[ETH_ALEN];
        /** BSSID pad */
    u8 bssid_pad[2];

    /* Selected configuration (based on Beacon/ProbeResp WAPI IE) */
        /** Key management */
    unsigned int key_mgmt;

    u8 assoc_wapi_ie[256];     /**< Own WAPI/RSN IE from (Re)AssocReq */
        /** WAPI IE */
    u8 ap_wapi_ie[256];
        /** WAPI IE length */
    u8 ap_wapi_ie_len;
        /** assoc WAPI IE length */
    u8 assoc_wapi_ie_len;
        /** Length pad */
    u8 len_pad[2];
};

/** Structure: WAPI Rx fragment */
struct wapi_rxfrag
{
        /** Data */
    const u8 *data;
        /** Data length */
    int data_len;
        /** Maximum length */
    int maxlen;
};

/** Structure: Certificate Id */
typedef struct __cert_id
{
        /** Certificate flag */
    u16 cert_flag;
        /** Length */
    u16 length;
        /** Data */
    u8 data[2048];
} cert_id;

/** Structure: Byte data */
typedef struct _byte_data
{
        /** Length */
    u8 length;
        /** Pad */
    u8 pad[3];
        /** Data */
    u8 data[256];
} byte_data;

/** Structure: wpa_certs */
struct wpa_certs
{
        /** Type */
    int type;
        /** Status */
    int status;
        /** Serial no */
    byte_data *serial_no;
        /** as name */
    byte_data *as_name;
        /** user name */
    byte_data *user_name;
};
/** Structure: wai_fixdata_id */
typedef struct _wai_fixdata_id
{
        /** Id flag */
    u16 id_flag;
        /** Id length */
    u16 id_len;
        /** Id data */
    u8 id_data[1000];
} wai_fixdata_id;
/** Structure: cert_bin_t */
struct cert_bin_t
{
        /** Length */
    unsigned short length;
        /** Pad */
    unsigned short pad;
        /** Data */
    unsigned char *data;
};
/** Structure: _wai_fixdata_cert */
typedef struct _wai_fixdata_cert
{
        /** Cert flag */
    u16 cert_flag;
        /** pad */
    u16 pad;
        /** cert bin */
    struct cert_bin_t cert_bin;
} wai_fixdata_cert;
/** Structure: _para_alg */
typedef struct _para_alg
{
        /** para flag */
    u8 para_flag;
        /** para length */
    u16 para_len;
        /** pad */
    u8 pad;
        /** para data */
    u8 para_data[256];
} para_alg, *ppara_alg;
/** Structure: _comm_data */
typedef struct _comm_data
{
        /** Length */
    u16 length;
        /** Pad value */
    u16 pad_value;
        /** Data */
    u8 data[2048];
} comm_data, *pcomm_data, tkey, *ptkey, tsign;

/** signature algorithm*/
typedef struct _wai_fixdata_alg
{
        /** Algorithm length */
    u16 alg_length;
        /** sha256 flag */
    u8 sha256_flag;
        /** Sign alg */
    u8 sign_alg;
        /** sign parameter */
    para_alg sign_para;
} wai_fixdata_alg;

/** Re-send buffer */
typedef struct _resendbuf_st
{
        /** Cur count */
    u16 cur_count;
        /** length */
    u16 len;
        /** Data */
    void *data;
} resendbuf_st;

/** Timer data maximum length */
#define TIMER_DATA_MAXLEN    8192

/** Structure: timer_dispose */
struct timer_dispose
{
    void *pt;           /**< timer */
    int t;              /**< type, 0-none, 1-wait assoc, 2-wait unicast, 3-wait finish */
    int ctm;            /**< count of into */
    int ctx;            /**< count of tx */
    int l;         /**< length */
    u8 dat[TIMER_DATA_MAXLEN];    /**< data */
};

/**
 * struct wpa_ssid - Network configuration data
 *
 * This structure includes all the configuration variables for a network. This
 * data is included in the per-interface configuration data as an element of
 * the network list, struct wpa_config::ssid. Each network block in the
 * configuration is mapped to a struct wpa_ssid instance.
 */
struct wapi_asue_st
{
        /** own address */
    u8 own_addr[ETH_ALEN];
        /** own address pad */
    u8 own_addr_pad[2];
        /** BSSID */
    u8 bssid[ETH_ALEN];
        /** SSID pad */
    u8 ssid_pad[2];
    int reassociate;     /**< reassociation requested */
        /** Disconnected flag */
    int disconnected;
        /** WAI received */
    int wai_received;
    /* Selected configuration (based on Beacon/ProbeResp WPA IE) */
        /** Pairwise cipher */
    int pairwise_cipher;
        /** Group cipher */
    int group_cipher;
        /** Key managememt */
    int key_mgmt;
        /** WAPI state machine */
    struct wapi_state_machine *wapi_sm;
        /** Rx fragment stamp */
    int rxfragstamp;
        /** Rx Fragment */
    struct wapi_rxfrag *rxfrag;
        /** Rx sequence */
    u16 rxseq;
        /** Tx sequence */
    u16 txseq;
        /** WAPI state */
    wapi_states wapi_state;
        /** usk updated flag*/
    int usk_updated;
        /** last wai source */
    u8 last_wai_src[ETH_ALEN];
        /** flag */
    u8 flag;
        /** flag pad */
    u8 flag_pad;
        /** ae auth flag */
    u8 ae_auth_flag[32];
        /** Nasue */
    u8 Nasue[32];
        /** Nae */
    u8 Nae[32];
        /** asue cert Id */
    cert_id asue_cert;
        /** ae cert Id*/
    cert_id ae_cert;
        /** ae_asu Id */
    wai_fixdata_id ae_asu_id;
        /** ae Id */
    wai_fixdata_id ae_id;
        /** asue Id */
    wai_fixdata_id asue_id;
        /** sign algorithm */
    wai_fixdata_alg sign_alg;
        /** asue key data */
    byte_data asue_key_data;
    byte_data asue_eck;                  /**< ASUE temp private key */
        /** ae key data */
    byte_data ae_key_data;
        /** ECDH */
    para_alg ecdh;

        /** ptm */
    struct timer_dispose *ptm;
        /** State machine */
    struct wapi_state_machine vpt_wapi_sm;
        /** vpt_tm */
    struct timer_dispose vpt_tm;

    int ap_type;    /**< 0-open,1-cert,2-key */
        /** PSK block */
    
    u8 psk_bk[16];
    void * ctx;
};

int iwn_wpa_ether_send(const u8 * buf, size_t len);
int iwn_wai_fixdata_id_by_ident(void *cert_st, wai_fixdata_id * fixdata_id,
                                u16 index);
struct wapi_rxfrag *iwn_wpa_defrag(struct wapi_asue_st *wpa_s,
                                   struct wapi_rxfrag *rxbuf);
void * free_rxfrag(struct wapi_rxfrag *frag);

/** BIT */
#define BIT(n) (1 << (n))
/** Enumaration: wpa_alg */
typedef enum
{ WPA_ALG_NONE, WPA_ALG_WEP, WPA_ALG_TKIP, WPA_ALG_CCMP,
        WAPI_ALG_SMS4 } wpa_alg;
/** WAI version */
#define WAI_VERSION 1
/** WAI type */
#define WAI_TYPE 1
/** WAI flag length */
#define WAI_FLAG_LEN 1

/*WAI packets type*/
enum
{
    WAI_PREAUTH_START = 0x01,   /* pre-authentication start */
    WAI_STAKEY_REQUEST = 0x02,  /* STAKey request */
    WAI_AUTHACTIVE = 0x03,      /* authentication activation */
    WAI_ACCESS_AUTH_REQUEST = 0x04,     /* access authentication request */
    WAI_ACCESS_AUTH_RESPONSE = 0x05,    /* access authentication response */
    WAI_CERT_AUTH_REQUEST = 0x06,       /* certificate authentication request */
    WAI_CERT_AUTH_RESPONSE = 0x07,      /* certificate authentication response */
    WAI_USK_NEGOTIATION_REQUEST = 0x08, /* unicast key negotiation request */
    WAI_USK_NEGOTIATION_RESPONSE = 0x09,        /* unicast key negotiation
                                                   response */
    WAI_USK_NEGOTIATION_CONFIRMATION = 0x0A,    /* unicast key negotiation
                                                   confirmation */
    WAI_MSK_ANNOUNCEMENT = 0x0B,        /* multicast key/STAKey announcement */
    WAI_MSK_ANNOUNCEMENT_RESPONSE = 0x0C,       /* multicast key/STAKey
                                                   announcement response */
    // WAI_SENDBK = 0x10, /*BK for TMC ??*/
};

#endif
