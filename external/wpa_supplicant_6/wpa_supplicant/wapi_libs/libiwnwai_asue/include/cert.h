/** @file cert.h
 *  @brief This header file contains data structures and function declarations of cert
 *
 *  Copyright (C) 2001-2008, Iwncomm Ltd.
 */

#ifndef _CERT_H_
#define _CERT_H_

#include "wapi.h"

/** V_X509_V1 */
#define V_X509_V1   0
/** V_X509_V2 */
#define V_X509_V2   1
/** V_X509_V3 */
#define V_X509_V3   2

/** V_ASN1: UNIVERSAL */
#define V_ASN1_UNIVERSAL		0x00
/** V_ASN1: APPLICATION */
#define	V_ASN1_APPLICATION		0x40
/** V_ASN1: CONTEXT SPECIFIC */
#define V_ASN1_CONTEXT_SPECIFIC		0x80
/** V_ASN1: PRIVATE */
#define V_ASN1_PRIVATE			0xc0

/** V_ASN1: CONSTRUCTED */
#define V_ASN1_CONSTRUCTED		0x20
/** V_ASN1:PRIMITIVE TAG */
#define V_ASN1_PRIMITIVE_TAG		0x1f

/** V_ASN1:UNDEF  */
#define V_ASN1_UNDEF			-1
/** V_ASN1:EOC */
#define V_ASN1_EOC			0
/** V_ASN1: BOOLEAN */
#define V_ASN1_BOOLEAN			1 /**/
/** V_ASN1: INTEGER */
#define V_ASN1_INTEGER			2
/** V_ASN1: BIT STRING */
#define V_ASN1_BIT_STRING		3
/** V_ASN1:OCTET STRING */
#define V_ASN1_OCTET_STRING		4
/** V_ASN1: NULL */
#define V_ASN1_NULL			5
/** V_ASN1:OBJECT */
#define V_ASN1_OBJECT			6
/** V_ASN1:OBJECT DESCRIPTOR */
#define V_ASN1_OBJECT_DESCRIPTOR	7
/** V_ASN1:EXTERNAL */
#define V_ASN1_EXTERNAL			8
/** V_ASN1:REAL */
#define V_ASN1_REAL			9
/** V_ASN1:ENUMERATED */
#define V_ASN1_ENUMERATED		10
/** V_ASN1: UTF8STRING */
#define V_ASN1_UTF8STRING		12
/** V_ASN1: SEQUENCE */
#define V_ASN1_SEQUENCE			16
/** V_ASN1:SET  */
#define V_ASN1_SET			17
/** V_ASN1:NUMERIC STRING */
#define V_ASN1_NUMERICSTRING		18 /**/
/** V_ASN1: PRINTABLE STRING */
#define V_ASN1_PRINTABLESTRING		19
/** V_ASN1: T61 STRING */
#define V_ASN1_T61STRING		20
/** V_ASN1:TELE TX STRING */
#define V_ASN1_TELETEXSTRING		20      /* alias */
/** V_ASN1:VIDEO TEX STRING */
#define V_ASN1_VIDEOTEXSTRING		21 /**/
/** V_ASN1:IA5 STRING  */
#define V_ASN1_IA5STRING		22
/** V_ASN1:UTC TIME */
#define V_ASN1_UTCTIME			23
/** V_ASN1:GENERALIZED TIME */
#define V_ASN1_GENERALIZEDTIME		24 /**/
/** V_ASN1:GRAPHICS STRING */
#define V_ASN1_GRAPHICSTRING		25 /**/
/** V_ASN1:ISO64 STRING */
#define V_ASN1_ISO64STRING		26 /**/
/** V_ASN1:VISIBLE STRING */
#define V_ASN1_VISIBLESTRING		26      /* alias */
/** V_ASN1:GENERAL STRING */
#define V_ASN1_GENERALSTRING		27 /**/
/** V_ASN1:UNIVERSAL STRING  */
#define V_ASN1_UNIVERSALSTRING		28 /**/
/** V_ASN1:BMPSTRING  */
#define V_ASN1_BMPSTRING		30
/** OID number */
#define WAPI_OID_NUMBER     1
/** Byte data length */
#define MAX_BYTE_DATA_LEN  256
/** Data length */
#define COMM_DATA_LEN          	2048
/** Pack error */
#define PACK_ERROR  0xffff
/** Sign length */
#define SIGN_LEN               		48
/** Public key length */
#define PUBKEY_LEN             		48
/** Public key2 length */
#define PUBKEY2_LEN             		49
/** Security key length */
#define SECKEY_LEN             		24
/** Digest length */
#define DIGEST_LEN             		32
/** HMAC length */
#define HMAC_LEN                        	20
/** Outkey length */
#define PRF_OUTKEY_LEN			48
/** x509 certificate sign length */
#define X509_CERT_SIGN_LEN        57
/** Outkey length */
#define KD_HMAC_OUTKEY_LEN	96
/** Field offset */
#define IWN_FIELD_OFFSET(type, field)   ((int)(&((type *)0)->field))
/** ECDSA192+SHA256 */
#define WAPI_ECDSA_OID          "1.2.156.11235.1.1.1"
/** CURVE OID */
#define WAPI_ECC_CURVE_OID      "1.2.156.11235.1.1.2.1"
/** ECDH OID */
#define ECDSA_ECDH_OID          "1.2.840.10045.2.1"
/** Structure: WOID */
    typedef struct _WOID
{
        /** OID name */
    const char *pszOIDName;
        /** OID length */
    unsigned short usOIDLen;
        /** usPar length */
    unsigned short usParLen;
        /** OID */
    unsigned short bOID[MAX_BYTE_DATA_LEN];
        /** Parameter */
    unsigned short bParameter[MAX_BYTE_DATA_LEN];
} WOID, *PWOID;

/** x509 time length */
#define X509_TIME_LEN       15
/** Structure: _pov_x */
typedef struct _pov_x
{
    struct
    {
        unsigned long not_before;
        unsigned long not_after;
    };
    struct
    {
        unsigned long Length;
        unsigned char Xnot_before[X509_TIME_LEN + 1];
        unsigned char Xnot_after[X509_TIME_LEN + 1];
    };
} pov_x_t;

/** Structure: Private key */
typedef struct __private_key
{
/** version type */
    unsigned char tVersion;
/** version length */
    unsigned char lVersion;
/** version value */
    unsigned char vVersion;
/** version pad */
    unsigned char verpad;

/** private key type */
    unsigned char tPrivateKey;
/** private key length */
    unsigned char lPrivateKey;
/** private key pad */
    unsigned char prikeypad[2];
/** private key value */
    unsigned char vPrivateKey[MAX_BYTE_DATA_LEN];

/** private key algorithm type */
    unsigned char tSPrivateKeyAlgorithm;
/** private key algorithm length */
    unsigned char lSPrivateKeyAlgorithm;
/** OID type */
    unsigned char tOID;
/** OID length */
    unsigned char lOID;
/** OID value */
    unsigned char vOID[MAX_BYTE_DATA_LEN];

/** SPublic key type */
    unsigned char tSPubkey;
/** SPublic key length */
    unsigned char lSPubkey;
/** Public key type */
    unsigned char tPubkey;
/** Public key length */
    unsigned char lPubkey;
/** Public key value */
    unsigned char vPubkey[MAX_BYTE_DATA_LEN];
} private_key;

short unpack_private_key(private_key * p_private_key, const void *buffer,
                         short bufflen);

void *iwn_x509_get_pubkey(void *cert_st);

void *iwn_x509_get_subject_name(void *cert_st);

void *iwn_x509_get_serial_number(void *cert_st);

void *iwn_x509_get_issuer_name(void *cert_st);

int iwn_x509_get_sign(void *cert_st, unsigned char *out, int out_len);
int iwn_x509_get_sign_inlen(void *cert_st);

int Base64Dec(unsigned char *buf, const unsigned char *text, int size);

unsigned char *get_realinfo_from_cert(unsigned char *des,
                                      const unsigned char *src_cert, int len,
                                      const char *start_flag,
                                      const char *end_flag);

/** Certificate object none */
#define CERT_OBJ_NONE 0
/** Certificate object x509 */
#define  CERT_OBJ_X509 1
/** Certificate object */
struct cert_obj_st_t
{
        /** Certificate type */
    int cert_type;
        /** Certificate name */
    char *cert_name;
        /** asu certificate struct */
    void *asu_cert_st;
        /** asu public key */
    tkey *asu_pubkey;
        /** user certificate struct */
    void *user_cert_st;         /* user certificate struct */
        /** Private key */
    tkey *private_key;
        /** user certificate file */
    struct cert_bin_t *cert_bin;        /* user certificate file */
        /** asu certificate file*/
    struct cert_bin_t *asu_cert_bin;    /* asu certificate file */
        /** Function pointer: Get public key */
    void *(*get_public_key) (void *cert_st);
        /** Function pointer: Get subject name */
    void *(*get_subject_name) (void *cert_st);
        /** Function Pointer: Get issuer name */
    void *(*get_issuer_name) (void *cert_st);
        /** Function pointer: Get serial number */
    void *(*get_serial_number) (void *cert_st);
        /** Function pointer: Verify key */
    int (*verify_key) (const unsigned char *pub_s, int pub_sl,
                       const unsigned char *priv_s, int priv_sl);
        /** */
    int (*sign) (const unsigned char *priv_s, int priv_sl,
                 const unsigned char *in, int in_len, unsigned char *out);
        /** Function pointer: Verify function */
    int (*verify) (const unsigned char *pub_s, int pub_sl, unsigned char *in,
                   int in_len, unsigned char *sig, int sign_len);
};

void cert_obj_register(struct cert_obj_st_t *cert_obj);
void cert_obj_unregister(const struct cert_obj_st_t *cert_obj);

int get_x509_cert(const void *param, struct cert_obj_st_t *cert_obj);
void x509_free_obj_data(struct cert_obj_st_t *cert_obj);

const struct cert_obj_st_t *get_cert_obj(unsigned short index);

/** PEM_STRING: Certificate begin */
#define PEM_STRING_X509_CERTS		"-----BEGIN CERTIFICATE-----"
/** PEM_STRING: Certificate end */
#define PEM_STRING2_X509_CERTE		"-----END CERTIFICATE-----"
/** PEM_STRING: Begin private key */
#define PEM_STRING_PRIKEYS			"-----BEGIN EC PRIVATE KEY-----"
/** PEM_STRING: End private key */
#define PEM_STRING_PRIKEYE			"-----END EC PRIVATE KEY-----"

void X509_exit();
int X509_init();

/** Structure eloop_data */
struct eloop_data
{
        /** Certificate info */
    struct _asue_cert_info cert_info;
        /** asue_id */
    wai_fixdata_id asue_id;
        /** has_cert */
    int has_cert;
};

#endif /* _CERT_H_ end */
