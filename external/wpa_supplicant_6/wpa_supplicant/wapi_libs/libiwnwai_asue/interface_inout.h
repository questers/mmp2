/** @file libiwnwai_asue/interface_inout.h
 *  @brief This header file contains data structures and function declarations of interface of this lib
 *
 *  Copyright (C) 2001-2008, Iwncomm Ltd.
 *  Copyright (c) 2009-2010, Marvell International Ltd.
 *
 */

#ifndef _INTERFACE_INOUT_H_
#define _INTERFACE_INOUT_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/** Enumaration: AUTH_TYPE */
    typedef enum
    {
        AUTH_TYPE_NONE_WAPI = 0,
        AUTH_TYPE_WAPI,
        AUTH_TYPE_WAPI_PSK,
    } AUTH_TYPE;

/** Enumaration: KEY_TYPE */
    typedef enum
    {
        KEY_TYPE_ASCII = 0,
        KEY_TYPE_HEX,
    } KEY_TYPE;

/** parameter of connect ap */
    typedef struct
    {
        /** Auth type */
        AUTH_TYPE authType;
	void *ctx;
        int debug_level;
        union
        {
            struct
            {                   /* psk parameter */
                KEY_TYPE kt;            /**< type */
                unsigned int kl;        /**< length */
                unsigned char kv[128];        /**< value*/
            };
            struct
            {                   /* cert parameter */
                unsigned char as[2048];         /**< as */
                unsigned char user[2048];         /**< user */
            };
        } para;
    } CNTAP_PARA;               /* parameter of connect ap */

/** Enumaration: CONN_STATUS */
    typedef enum
    {
        CONN_ASSOC = 0,
        CONN_DISASSOC,
    } CONN_STATUS;

/** Structure: MAC_ADDRESS */
    typedef struct
    {
        unsigned char v[6]; /**< Value */
        unsigned char pad[2]; /**< Pad */
    } MAC_ADDRESS;

/**
 * WAI_CNTAPPARA_SET - Set WIE to driver
 * @CNTAP_PARA: Pointer to  struct CNTAP_PARA
 * Returns: 0 on success, -1 on failure
 *
 * set WIE to driver 
 *
 */
    int WAI_CNTAPPARA_SET(const CNTAP_PARA * pPar);
    void WAI_Msg_Input(CONN_STATUS action, const MAC_ADDRESS * pBSSID,
                       const MAC_ADDRESS * pLocalMAC, unsigned char *assoc_ie,
                       unsigned char assoc_ie_len);

    unsigned long WAI_RX_packets_indication(const u8 * pbuf, int length);

    int WIFI_lib_init();

    int WIFI_lib_exit();

    int lib_get_wapi_state(void);

/** Typedef: OS_timer_expired */
    typedef void (*OS_timer_expired) (const int pdata);
    void *OS_timer_setup(int deltaTimer, int repeated,
                         OS_timer_expired pfunction, const void *pdata);
    void OS_timer_clean(void *pTimer);
    void WIFI_Action_Deauth(void * ctx);

    unsigned long WIFI_TX_packet(void * ctx,const char *pbuf, int length);

    int WIFI_unicast_key_set(void * ctx,const char *pKeyValue, int keylength, int key_idx);

    int WIFI_group_key_set(void * ctx,const unsigned char *pKeyValue, int keylength,
                           int key_idx, const unsigned char *keyIV);

    int WIFI_WAI_IE_set(void *ctx,const unsigned char *pbuf, int length);

    unsigned char WIFI_get_rand_byte();

#ifdef  __cplusplus
}
#endif

#endif                          /* _INTERFACE_INOUT_H_ */
