/** @file libiwnwai_asue/common.c
 *  @brief This file contains functions for common, include random, print, ecc
 *
 *  Copyright (C) 2001-2008, Iwncomm Ltd.
 */

#include <stdarg.h>
#include <ctype.h>

#include "include/hmac_sha256.h"
#include "include/common.h"
#include "interface_inout.h"

/** Debug level*/
int iwn_debug_level = MSG_INFO;

/** 
 *  @brief smash random
 *   
 *  @param buffer  Buffer 
 *  @param len     Length
 *  @return        None
 */
static void
smash_random(unsigned char *buffer, int len)
{
    unsigned char smash_key[32] =
        { 0x09, 0x1A, 0x09, 0x1A, 0xFF, 0x90, 0x67, 0x90,
        0x7F, 0x48, 0x1B, 0xAF, 0x89, 0x72, 0x52, 0x8B,
        0x35, 0x43, 0x10, 0x13, 0x75, 0x67, 0x95, 0x4E,
        0x77, 0x40, 0xC5, 0x28, 0x63, 0x62, 0x8F, 0x75
    };
    KD_hmac_sha256(buffer, len, smash_key, 32, buffer, len);
}

/** 
 *  @brief Get random
 *   
 *  @param buffer  Buffer 
 *  @param len     Length
 *  @return        None
 */
#ifndef NORAND
void
get_random(unsigned char *buffer, int len)
{
    int i;
    for (i = 0; i < len; i++) {
        buffer[i] = WIFI_get_rand_byte();
    }
    smash_random(buffer, len);
}
#else
void
get_random(unsigned char *buffer, int len)
{
    int i;
    for (i = 0; i < len; i++) {
        buffer[i] = 18;
    }
}
#endif

/** 
 *  @brief Conditional printf
 *   
 *  @param level   Priority level 
 *  @param fmt     printf format string, followed by optional arguments
 *  @return        None
 */
#ifdef ANDROID
#include <android/log.h>
void iwn_wpa_printf(int level, char *fmt, ...)
{
    if (level >= iwn_debug_level) {
		va_list ap;
		if (level == MSG_ERROR) {
			level = ANDROID_LOG_ERROR;
		} else if (level == MSG_WARNING) {
			level = ANDROID_LOG_WARN;
		} else if (level == MSG_INFO) {
			level = ANDROID_LOG_INFO;
		} else {
			level = ANDROID_LOG_DEBUG;
		}
		va_start(ap, fmt);
		__android_log_vprint(level, "wpa_supplicant", fmt, ap);
		va_end(ap);
	}
}
#else
void iwn_wpa_printf(int level, char *fmt, ...)
{
    va_list ap;

    va_start(ap, fmt);
    if (level >= iwn_debug_level) {
        vprintf(fmt, ap);
        printf("\n");
    }
    va_end(ap);
}

/** 
 *  @brief Hexdump
 *   
 *  @param level   Priority level 
 *  @param title   title of for the message 
 *  @param buf     data buffer to be dumped
 *  @param len     buf length
 *  @return        None
 */
void
iwn_wpa_hexdump(int level, const char *title, const u8 * buf, size_t len)
{
    if (level >= iwn_debug_level) {
        print_buf(title, buf, len);
    }
}
#endif 

/** 
 *  @brief Print buffer
 *   
 *  @param title   Title 
 *  @param buf     Buffer
 *  @param len     Buffer length
 *  @return        None
 */
void
print_buf(const char *title, const void *buf, int len)
{
#define tmpfun_print printf

    int cl = (len - 1) / 16 + 1;
    int i;
    int j;
    char txt[260] = { 0 };
    sprintf(txt, "\n%s(len=%d,0x%X)\n", title, len, len);
    tmpfun_print("%s", txt);
    for (i = 0; i < cl; i++) {
        int n = 16;
        const unsigned char *p = NULL;
        if (i == cl - 1) {      /* lastest line */
            n = len % 16;
            if (0 == n) {
                n = 16;
            }
        }
        sprintf(txt, "%.4X  ", i * 16);
        p = (const unsigned char *) buf + i * 16;
        for (j = 0; j < n; j++) {
            sprintf(txt + strlen(txt), "%.2X", p[j]);
            if (j == n - 1) {   /* lastest char */
                if (16 == n) {  /* whole line */
                    strcat(txt, "  ");
                } else {        /* fill none whole line */
                    int k;
                    for (k = strlen(txt); k < 43; k++) {
                        txt[k] = ' ';
                    }
                    txt[k] = 0;
                }
            } else if (3 == j % 4) {
                strcat(txt, " ");
            }
        }
        for (j = 0; j < n; j++) {
            int c = p[j];
            if (c < 32) {
                c = '.';
            }
            if (c > 127 && c < 160) {
                c = '.';
            }
            if (c > 127) {
/*				c = '.';*/
            }
/*			c = '.';*/

            {
                char tmp[2] = { c };
                strcat(txt, tmp);
            }
        }
        tmpfun_print("%s", txt);
        tmpfun_print("\n");
    }
    tmpfun_print("\n\n");

#undef tmpfun_print
}

/** 
 *  @brief verify the sign information with the public key
 *   
 *  @param pub_s     Public key
 *  @param pub_sl    Public key length
 *  @param in        in
 *  @param in_len    in length
 *  @param sign      Sign
 *  @param sign_len  Sign length
 *  @return          1 -- success, otherwise 0
 */
int
x509_ecc_verify(const unsigned char *pub_s, int pub_sl, unsigned char *in,
                int in_len, unsigned char *sign, int sign_len)
{
    int ret = 0;

    if (pub_s == NULL || pub_sl <= 0 || in == NULL || in_len <= 0 ||
        sign == NULL || sign_len <= 0) {
        return ret;
    } else {
        ret = ecc192_verify(pub_s, in, in_len, sign, sign_len);
    }

    if (ret <= 0)
        ret = 0;
    else
        ret = 1;

    return ret;
}

/** 
 *  @brief sign with the private key
 *   
 *  @param priv_s  Private key
 *  @param priv_sl Private key length
 *  @param in      in
 *  @param in_len  in length
 *  @param out     out
 *  @return        >0 -- success, otherwise 0
 */
int
x509_ecc_sign(const unsigned char *priv_s, int priv_sl, const unsigned char *in,
              int in_len, unsigned char *out)
{
    priv_sl = priv_sl;          /* disable warnning */
    if (priv_s == NULL || in == NULL || in_len <= 0 || out == NULL) {
        return 0;
    } else {
        return ecc192_sign(priv_s, in, in_len, out);
    }
}

/** 
 *  @brief verify the public key and the private key
 *   
 *  @param pub_s   Public key
 *  @param pub_sl  Public key length
 *  @param priv_s  Private key
 *  @param priv_sl Private key length
 *  @return        1 -- success, otherwise 0
 */
int
x509_ecc_verify_key(const unsigned char *pub_s, int pub_sl,
                    const unsigned char *priv_s, int priv_sl)
{
#define EC962_SIGN_LEN		48
    unsigned char data[] = "123456abcd";
    unsigned char sign[EC962_SIGN_LEN + 1];
    int ret = 0;

    if (priv_s == NULL || pub_sl <= 0 || priv_s == NULL || priv_sl <= 0) {
        return 0;
    }

    memset(sign, 0, sizeof(sign));
    ret = ecc192_sign(priv_s, data, strlen((char *) data), sign);
    if (ret != EC962_SIGN_LEN) {
        printf("ecc192_sign call fail \n");
        ret = 0;
        return ret;
    }

    ret =
        ecc192_verify(pub_s, data, strlen((char *) data), sign, EC962_SIGN_LEN);
    if (ret <= 0) {
        printf("ecc192_verify call fail \n");
        ret = 0;
    }

    return ret;

}
