LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= 	\
        ebml-read.c \
        ebml-write.c \
        matroska.c \
        matroska-demux.c \
        matroska-ids.c \
        matroska-mux.c \
        webm-mux.c \
        lzo.c

LOCAL_SHARED_LIBRARIES :=	\
	libz			\
        libbz                   \
	libgsttag-0.10		\
        libgstpbutils-0.10      \
	libgstriff-0.10		\
	libgstrtp-0.10		\
	libgstreamer-0.10	\
	libgstbase-0.10		\
	libglib-2.0		\
	libgthread-2.0		\
	libgmodule-2.0		\
	libgobject-2.0

LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/$(GST_PLUGINS_PATH)

LOCAL_PRELINK_MODULE := false

LOCAL_MODULE:= libgstmatroska

LOCAL_C_INCLUDES := 				\
	$(LOCAL_PATH)				\
	$(GST_PLUGINS_GOOD_TOP)			\
	$(GST_PLUGINS_GOOD_DEP_INCLUDES)

LOCAL_CFLAGS := \
	-DHAVE_CONFIG_H			

include $(BUILD_SHARED_LIBRARY)
