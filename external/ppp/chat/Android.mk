LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        chat.c

LOCAL_SHARED_LIBRARIES := \
	libcutils \
    libutils 


LOCAL_CFLAGS := -DANDROID_CHANGES
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE:= chat

include $(BUILD_EXECUTABLE)

