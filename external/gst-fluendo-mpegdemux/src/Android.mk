LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        flumpegdemux.c  \
        gstmpegdesc.c   \
        gstmpegdemux.c  \
        gstpesfilter.c  \
        gstmpegtsdemux.c \
        flutspatinfo.c \
        flutspmtinfo.c \
        flutspmtstreaminfo.c \
        gstsectionfilter.c

LOCAL_SHARED_LIBRARIES := 	\
	libgstreamer-0.10	\
	libgstbase-0.10		\
	libglib-2.0		\
	libgthread-2.0		\
	libgmodule-2.0		\
	libgobject-2.0		\

LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/$(GST_PLUGINS_PATH)

LOCAL_MODULE:= libgstflumpegdemux

LOCAL_PRELINK_MODULE := false

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)   \
	$(GST_FLUENDO_DEP_INCLUDES)

LOCAL_CFLAGS := \
	-DHAVE_CONFIG_H			

LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)
