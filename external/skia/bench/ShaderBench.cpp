#include "SkBenchmark.h"
#include "SkBitmap.h"
#include "SkPaint.h"
#include "SkCanvas.h"
#include "SkColorPriv.h"
#include "SkRandom.h"
#include "SkString.h"
#include "SkShader.h"

#ifdef _MRVL_SKIABENCH_EXT

#define TEST_SRC8888
//#define TEST_ROTATE
#define TEST_SRC565

/* from Platform/skia/ImageSkia.cpp */
enum ResamplingMode {
    // Nearest neighbor resampling. Used when we detect that the page is
    // trying to make a pattern by stretching a small bitmap very large.
    RESAMPLE_NONE,

    // Default skia resampling. Used for large growing of images where high
    // quality resampling doesn't get us very much except a slowdown.
    RESAMPLE_LINEAR,

    // High quality resampling.
    RESAMPLE_AWESOME,
};

#define SRCA 0x80   /* the per pixel alpha used in non-opaque case */

#define SK_Color_SRCA(color, srca) \
    SkColorSetARGB((srca), \
    SkAlphaMul(SkColorGetR((color)), (srca)),\
    SkAlphaMul(SkColorGetG((color)), (srca)),\
    SkAlphaMul(SkColorGetB((color)), (srca)))

static const char *gResampleModeName[] = {
    "RESAMPLE_NONE", "RESAMPLE_LINEAR", "RESAMPLE_AWESOME"
};

static const char* gTileName[] = {
    "clamp", "repeat", "mirror"
};

static const char* gConfigName[] = {
    "ERROR", "a1", "a8", "index8", "565", "4444", "8888"
};


#ifdef CHROMIUM_BUILD
/* chromium path */
static ResamplingMode computeResamplingMode(const SkBitmap& bitmap, int srcWidth, int srcHeight, float destWidth, float destHeight)
{
    int destIWidth = static_cast<int>(destWidth);
    int destIHeight = static_cast<int>(destHeight);

    // The percent change below which we will not resample. This usually means
    // an off-by-one error on the web page, and just doing nearest neighbor
    // sampling is usually good enough.
    const float kFractionalChangeThreshold = 0.025f;

    // Images smaller than this in either direction are considered "small" and
    // are not resampled ever (see below).
    const int kSmallImageSizeThreshold = 8;

    // The amount an image can be stretched in a single direction before we
    // say that it is being stretched so much that it must be a line or
    // background that doesn't need resampling.
    const float kLargeStretch = 3.0f;

    // Figure out if we should resample this image. We try to prune out some
    // common cases where resampling won't give us anything, since it is much
    // slower than drawing stretched.
    if (srcWidth == destIWidth && srcHeight == destIHeight) {
        // We don't need to resample if the source and destination are the same.
        return RESAMPLE_NONE;
    }

    //this is not chrome case, extend for android cases
    if(bitmap.getConfig() == SkBitmap::kRGB_565_Config) {
        return RESAMPLE_LINEAR;
    }

    if (srcWidth <= kSmallImageSizeThreshold
        || srcHeight <= kSmallImageSizeThreshold
        || destWidth <= kSmallImageSizeThreshold
        || destHeight <= kSmallImageSizeThreshold) {
        // Never resample small images. These are often used for borders and
        // rules (think 1x1 images used to make lines).
        return RESAMPLE_NONE;
    }

    if (srcHeight * kLargeStretch <= destHeight || srcWidth * kLargeStretch <= destWidth) {
        // Large image detected.

        // Don't resample if it is being stretched a lot in only one direction.
        // This is trying to catch cases where somebody has created a border
        // (which might be large) and then is stretching it to fill some part
        // of the page.
        if (srcWidth == destWidth || srcHeight == destHeight)
            return RESAMPLE_NONE;

        // The image is growing a lot and in more than one direction. Resampling
        // is slow and doesn't give us very much when growing a lot.
        return RESAMPLE_LINEAR;
    }

    if ((fabs(destWidth - srcWidth) / srcWidth < kFractionalChangeThreshold)
        && (fabs(destHeight - srcHeight) / srcHeight < kFractionalChangeThreshold)) {
        // It is disappointingly common on the web for image sizes to be off by
        // one or two pixels. We don't bother resampling if the size difference
        // is a small fraction of the original size.
        return RESAMPLE_NONE;
    }

    // When the image is not yet done loading, use linear. We don't cache the
    // partially resampled images, and as they come in incrementally, it causes
    // us to have to resample the whole thing every time.
    /*
    if (!bitmap.isDataComplete())
        return RESAMPLE_LINEAR;
    */
    // Everything else gets resampled.
    return RESAMPLE_AWESOME;
}

#else /* !CHROMIUM_BUILD */
/* android path */
static ResamplingMode computeResamplingMode(const SkBitmap& bitmap, int srcWidth, int srcHeight, float destWidth, float destHeight)
{
    return RESAMPLE_LINEAR;
}
#endif /* CHROMIUM_BUILD */

static void drawIntoBitmap(const SkBitmap& bm) {
    const int w = bm.width();
    const int h = bm.height();

    SkCanvas canvas(bm);
    SkPaint p;
    p.setAntiAlias(true);
    p.setXfermodeMode(SkXfermode::kSrc_Mode);
    if(bm.isOpaque()) {
        p.setColor(SK_ColorRED);
    } else {
        p.setColor(SK_Color_SRCA(SK_ColorRED, SRCA));
    }
    canvas.drawCircle(SkIntToScalar(w)/2, SkIntToScalar(h)/2,
                      SkIntToScalar(SkMin32(w, h))*3/8, p);
}

class ShaderBench : public SkBenchmark {
    bool            fIsOpaque;
    int             fSrcWidth, fSrcHeight;
    int             fTileX, fTileY; // -1 means don't use shader
    SkScalar        fScaleX, fScaleY;
    int             fRotateDegree;

    SkBitmap        fBitmap;
    SkPaint         fPaint;
    ResamplingMode  fResampleMode;
    SkScalar        fDestWidth;
    SkScalar        fDestHeight;
    SkBitmap        fResampledBitmap;
    SkBitmap        *fBitmapDrawn;
    SkIRect         fSrect;
    SkRect          fDrect;
    bool            fEnableTile;
    bool            fSupported;
    SkString        fName;
    enum { N = 1};
public:
    ShaderBench(void* param, bool isOpaque, SkBitmap::Config c, int w=128, int h=128, int tx = -1, int ty = -1, SkScalar sx=1.0, SkScalar sy=1.0, int degree = 0)
        : INHERITED(param), fIsOpaque(isOpaque), fSrcWidth(w), fSrcHeight(h), fTileX(tx), fTileY(ty), fScaleX(sx), fScaleY(sy), fRotateDegree(degree) {
            fEnableTile  = false;
            fSupported   = true;

            if (SkBitmap::kARGB_8888_Config != c && SkBitmap::kRGB_565_Config != c) {
                fSupported = false;
                return;
            }

            fBitmap.setConfig(c, w, h);
            fBitmap.allocPixels();
            if(fIsOpaque) {
                fBitmap.eraseColor(SK_ColorBLUE);
            } else {
                fBitmap.eraseARGB(SRCA, SkColorGetR(SK_ColorBLUE), SkColorGetG(SK_ColorBLUE), SkColorGetB(SK_ColorBLUE));
            }
            if (fBitmap.getColorTable()) {
                fBitmap.getColorTable()->setIsOpaque(fIsOpaque);
            }
            fBitmap.setIsOpaque(fIsOpaque);
            drawIntoBitmap(fBitmap);

            fDestWidth  = SkScalarMul(SkIntToScalar(fBitmap.width()),fScaleX);
            fDestHeight = SkScalarMul(SkIntToScalar(fBitmap.height()),fScaleY);
            fResampleMode = computeResamplingMode(fBitmap, fBitmap.width(), fBitmap.height(), fDestWidth, fDestHeight);

            if(fTileX != -1 && fTileY != -1 ) {
                SkShader* s = SkShader::CreateBitmapShader(fBitmap, (SkShader::TileMode)fTileX, (SkShader::TileMode)fTileY);
                if(fResampleMode != RESAMPLE_AWESOME) {
                    SkMatrix matrix;
                    matrix.setScale(fScaleX, fScaleY);
                    s->setLocalMatrix(matrix);
                    //matrix.dump();
                }
                fPaint.setShader(s)->unref();
                fDrect.set(0, 0, onGetSize().fX, onGetSize().fY);   /* canvas size */
                fEnableTile = true;
            } else {
                fDrect.set(0, 0, fDestWidth, fDestHeight);          /* dst bitmap size */
            }

            /* only chromium build will return RESAMPLE_AWESOME, in this case resample is performed before skia draw, so set up src rect as size after resize */
            if(fResampleMode == RESAMPLE_AWESOME) {
                fSrect.set(0, 0, (int)(fBitmap.width()*SkScalarToFloat(fScaleX)+0.5), (int)(fBitmap.height()*SkScalarToFloat(fScaleY)+0.5));
            } else {
                fSrect.set(0, 0, fBitmap.width(), fBitmap.height());
            }

            if(fResampleMode == RESAMPLE_LINEAR) {
                fPaint.setFilterBitmap(true);                       /* this is how WebCore sets filter flag*/
            } else {
                fPaint.setFilterBitmap(false);
            }

    }

protected:
    virtual const char* onGetName() {
        fName.set("shader");
        if (fTileX >= 0) {
            fName.appendf("_%s", gTileName[fTileX]);
            if (fTileY != fTileX) {
                fName.appendf("_%s", gTileName[fTileY]);
            }
        }
        fName.appendf("_%s", gConfigName[fBitmap.config()]);
        fName.appendf("_%dx%d", fSrcWidth, fSrcHeight);
        fName.appendf("_To_%dx%d", (int)fDestWidth, (int)fDestHeight);
        if(fRotateDegree) {
            fName.appendf("_rotate%d", fRotateDegree);
        }
        fName.appendf("_%s%s_GA%2x", gResampleModeName[fResampleMode], fIsOpaque ? "" : "_SA", fForceAlpha);
        return fName.c_str();
    }

    virtual void onDraw(SkCanvas* canvas) {
        if(!fSupported) return;

        fPaint.setAlpha(fForceAlpha);
        fPaint.setAntiAlias(fForceAA);

        for (int i = 0; i < N; i++) {
#ifdef CHROMIUM_BUILD
            if(fResampleMode == RESAMPLE_AWESOME) {
                fResampledBitmap = skia::ImageOperations::Resize(fBitmap, skia::ImageOperations::RESIZE_LANCZOS3, fDestWidth, fDestHeight);
                fBitmapDrawn = &fResampledBitmap;
            } else {
                fBitmapDrawn = &fBitmap;
            }
#else
            fBitmapDrawn = &fBitmap;
#endif

            //printf("\nsrcrect(0,0,%d,%d), dstrect(0,0,%.1f,%.1f)\n", fSrect.width(), fSrect.height(), fDrect.width(), fDrect.height());
            if(fRotateDegree != 0) {
                SkScalar x, y;

                x = SkIntToScalar(fBitmapDrawn->width()) / 2;
                y = SkIntToScalar(fBitmapDrawn->height()) / 2;
                canvas->translate(x, y);
                canvas->rotate(SkIntToScalar(fRotateDegree));
                canvas->translate(-x, -y);

            }
            if(fEnableTile) {
                canvas->drawRect(fDrect, fPaint);
            } else if(SkScalarToFloat(fScaleX) == 1.0 && SkScalarToFloat(fScaleY) == 1.0) {
                canvas->drawBitmap(*fBitmapDrawn, 0, 0, &fPaint);
                /* drawBitmapRect doesn't pass bitmap, it creates temp bitmap instead, the isOpaque() flag is missing */
            } else {
                canvas->drawBitmapRect(*fBitmapDrawn, &fSrect, fDrect, &fPaint); /* with resize */
            }
        }
    }

private:
    typedef SkBenchmark INHERITED;
};

#ifdef TEST_SRC8888
/* src opaque */
/* no tile mode */
static SkBenchmark* ShaderFact0(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 640, 480, -1, -1, 1.0, 1.0, 0); }
static SkBenchmark* ShaderFact1(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 640, 480, -1, -1, 2.0, 2.0, 0); }
static SkBenchmark* ShaderFact2(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 640, 480, -1, -1, 3.0, 3.0, 0); }
static SkBenchmark* ShaderFact3(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 8, 8, -1, -1, 0.5, 0.5, 0); }
static SkBenchmark* ShaderFact4(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 8, 8, -1, -1, 1.0, 1.0, 0); }
static SkBenchmark* ShaderFact5(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 1920, 1080, -1, -1, 1.0, 1.0, 0); }
static SkBenchmark* ShaderFact6(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 1920, 1080, -1, -1, 0.5, 0.5, 0); }
static SkBenchmark* ShaderFact7(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 1920, 1080, -1, -1, 0.1, 0.1, 0); }
/* tile */
static SkBenchmark* ShaderFact8(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 50, 50, 0, 0, 1.0, 1.0, 0); } /* clamp */
static SkBenchmark* ShaderFact9(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 50, 50, 1, 1, 1.0, 1.0, 0); } /* repeat */
static SkBenchmark* ShaderFact10(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 50, 50, 0, 0, 3.0, 3.0, 0); }/* clamp resize */
static SkBenchmark* ShaderFact11(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 50, 50, 1, 1, 3.0, 3.0, 0); }/* repeat resize*/

/* src has alpha */
/* no tile mode */
static SkBenchmark* ShaderFact0A(void* p) { return new ShaderBench(p, false, SkBitmap::kARGB_8888_Config, 640, 480, -1, -1, 1.0, 1.0, 0); }
static SkBenchmark* ShaderFact1A(void* p) { return new ShaderBench(p, false, SkBitmap::kARGB_8888_Config, 640, 480, -1, -1, 2.0, 2.0, 0); }
static SkBenchmark* ShaderFact2A(void* p) { return new ShaderBench(p, false, SkBitmap::kARGB_8888_Config, 640, 480, -1, -1, 3.0, 3.0, 0); }
static SkBenchmark* ShaderFact3A(void* p) { return new ShaderBench(p, false, SkBitmap::kARGB_8888_Config, 8, 8, -1, -1, 0.5, 0.5, 0); }
static SkBenchmark* ShaderFact4A(void* p) { return new ShaderBench(p, false, SkBitmap::kARGB_8888_Config, 8, 8, -1, -1, 1.0, 1.0, 0); }
static SkBenchmark* ShaderFact5A(void* p) { return new ShaderBench(p, false, SkBitmap::kARGB_8888_Config, 1920, 1080, -1, -1, 1.0, 1.0, 0); }
static SkBenchmark* ShaderFact6A(void* p) { return new ShaderBench(p, false, SkBitmap::kARGB_8888_Config, 1920, 1080, -1, -1, 0.5, 0.5, 0); }
static SkBenchmark* ShaderFact7A(void* p) { return new ShaderBench(p, false, SkBitmap::kARGB_8888_Config, 1920, 1080, -1, -1, 0.1, 0.1, 0); }
/* tile */
static SkBenchmark* ShaderFact8A(void* p) { return new ShaderBench(p, false, SkBitmap::kARGB_8888_Config, 50, 50, 0, 0, 1.0, 1.0, 0); } /* clamp */
static SkBenchmark* ShaderFact9A(void* p) { return new ShaderBench(p, false, SkBitmap::kARGB_8888_Config, 50, 50, 1, 1, 1.0, 1.0, 0); } /* repeat */
static SkBenchmark* ShaderFact10A(void* p) { return new ShaderBench(p, false, SkBitmap::kARGB_8888_Config, 50, 50, 0, 0, 3.0, 3.0, 0); }/* clamp resize */
static SkBenchmark* ShaderFact11A(void* p) { return new ShaderBench(p, false, SkBitmap::kARGB_8888_Config, 50, 50, 1, 1, 3.0, 3.0, 0); }/* repeat resize*/

static BenchRegistry gShaderReg0(ShaderFact0);
static BenchRegistry gShaderReg1(ShaderFact1);
static BenchRegistry gShaderReg2(ShaderFact2);
static BenchRegistry gShaderReg3(ShaderFact3);
static BenchRegistry gShaderReg4(ShaderFact4);
static BenchRegistry gShaderReg5(ShaderFact5);
static BenchRegistry gShaderReg6(ShaderFact6);
static BenchRegistry gShaderReg7(ShaderFact7);
static BenchRegistry gShaderReg8(ShaderFact8);
static BenchRegistry gShaderReg9(ShaderFact9);
static BenchRegistry gShaderReg10(ShaderFact10);
static BenchRegistry gShaderReg11(ShaderFact11);

static BenchRegistry gShaderReg0A(ShaderFact0A);
static BenchRegistry gShaderReg1A(ShaderFact1A);
static BenchRegistry gShaderReg2A(ShaderFact2A);
static BenchRegistry gShaderReg3A(ShaderFact3A);
static BenchRegistry gShaderReg4A(ShaderFact4A);
static BenchRegistry gShaderReg5A(ShaderFact5A);
static BenchRegistry gShaderReg6A(ShaderFact6A);
static BenchRegistry gShaderReg7A(ShaderFact7A);
static BenchRegistry gShaderReg8A(ShaderFact8A);
static BenchRegistry gShaderReg9A(ShaderFact9A);
static BenchRegistry gShaderReg10A(ShaderFact10A);
static BenchRegistry gShaderReg11A(ShaderFact11A);

#ifdef TEST_ROTATE
/* rotate 90 */
static SkBenchmark* ShaderFact0R(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 640, 480, -1, -1, 1.0, 1.0, 90); }
static SkBenchmark* ShaderFact1R(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 640, 480, -1, -1, 2.0, 2.0, 90); }
static SkBenchmark* ShaderFact2R(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 640, 480, -1, -1, 3.0, 3.0, 90); }
static SkBenchmark* ShaderFact3R(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 8, 8, -1, -1, 0.5, 0.5, 90); }
static SkBenchmark* ShaderFact4R(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 8, 8, -1, -1, 1.0, 1.0, 90); }
static SkBenchmark* ShaderFact5R(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 1920, 1080, -1, -1, 1.0, 1.0, 90); }
static SkBenchmark* ShaderFact6R(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 1920, 1080, -1, -1, 0.5, 0.5, 90); }
static SkBenchmark* ShaderFact7R(void* p) { return new ShaderBench(p, true, SkBitmap::kARGB_8888_Config, 1920, 1080, -1, -1, 0.1, 0.1, 90); }

static BenchRegistry gShaderReg0R(ShaderFact0R);
static BenchRegistry gShaderReg1R(ShaderFact1R);
static BenchRegistry gShaderReg2R(ShaderFact2R);
static BenchRegistry gShaderReg3R(ShaderFact3R);
static BenchRegistry gShaderReg4R(ShaderFact4R);
static BenchRegistry gShaderReg5R(ShaderFact5R);
static BenchRegistry gShaderReg6R(ShaderFact6R);
static BenchRegistry gShaderReg7R(ShaderFact7R);
#endif /* TEST_ROTATE */
#endif /* TEST_SRC8888 */

#ifdef TEST_SRC565
/* no tile mode */
static SkBenchmark* ShaderFact16_0(void* p) { return new ShaderBench(p, true, SkBitmap::kRGB_565_Config, 640, 480, -1, -1, 1.0, 1.0, 0); }
static SkBenchmark* ShaderFact16_1(void* p) { return new ShaderBench(p, true, SkBitmap::kRGB_565_Config, 640, 480, -1, -1, 2.0, 2.0, 0); }
static SkBenchmark* ShaderFact16_2(void* p) { return new ShaderBench(p, true, SkBitmap::kRGB_565_Config, 640, 480, -1, -1, 3.0, 3.0, 0); }
static SkBenchmark* ShaderFact16_3(void* p) { return new ShaderBench(p, true, SkBitmap::kRGB_565_Config, 8, 8, -1, -1, 0.5, 0.5, 0); }
static SkBenchmark* ShaderFact16_4(void* p) { return new ShaderBench(p, true, SkBitmap::kRGB_565_Config, 8, 8, -1, -1, 1.0, 1.0, 0); }
static SkBenchmark* ShaderFact16_5(void* p) { return new ShaderBench(p, true, SkBitmap::kRGB_565_Config, 1920, 1080, -1, -1, 1.0, 1.0, 0); }
static SkBenchmark* ShaderFact16_6(void* p) { return new ShaderBench(p, true, SkBitmap::kRGB_565_Config, 1920, 1080, -1, -1, 0.5, 0.5, 0); }
static SkBenchmark* ShaderFact16_7(void* p) { return new ShaderBench(p, true, SkBitmap::kRGB_565_Config, 1920, 1080, -1, -1, 0.1, 0.1, 0); }
/* tile */
static SkBenchmark* ShaderFact16_8(void* p) { return new ShaderBench(p, true, SkBitmap::kRGB_565_Config, 50, 50, 0, 0, 1.0, 1.0, 0); }    //clamp
static SkBenchmark* ShaderFact16_9(void* p) { return new ShaderBench(p, true, SkBitmap::kRGB_565_Config, 50, 50, 1, 1, 1.0, 1.0, 0); }    //repeat
static SkBenchmark* ShaderFact16_10(void* p) { return new ShaderBench(p, true, SkBitmap::kRGB_565_Config, 50, 50, 0, 0, 3.0, 3.0, 0); }   //clamp resize
static SkBenchmark* ShaderFact16_11(void* p) { return new ShaderBench(p, true, SkBitmap::kRGB_565_Config, 50, 50, 1, 1, 3.0, 3.0, 0); }   //repeat resize

static BenchRegistry gShaderReg16_0(ShaderFact16_0);
static BenchRegistry gShaderReg16_1(ShaderFact16_1);
static BenchRegistry gShaderReg16_2(ShaderFact16_2);
static BenchRegistry gShaderReg16_3(ShaderFact16_3);
static BenchRegistry gShaderReg16_4(ShaderFact16_4);
static BenchRegistry gShaderReg16_5(ShaderFact16_5);
static BenchRegistry gShaderReg16_6(ShaderFact16_6);
static BenchRegistry gShaderReg16_7(ShaderFact16_7);
static BenchRegistry gShaderReg16_8(ShaderFact16_8);
static BenchRegistry gShaderReg16_9(ShaderFact16_9);
static BenchRegistry gShaderReg16_10(ShaderFact16_10);
static BenchRegistry gShaderReg16_11(ShaderFact16_11);

#endif /* TEST_SRC565 */
#endif /* _MRVL_SKIABENCH_EXT */

/* EOF */




