/******************************************************************************
 *         Copyright (c) 2010 Marvell Corporation. All Rights Reserved.
 ******************************************************************************/

#ifdef _MRVL_OPT
#ifndef SkBlitter_MRVL_DEFINED
#define SkBlitter_MRVL_DEFINED

#include "SkBitmap.h"
#include "SkMatrix.h"
#include "SkPaint.h"
#include "SkRefCnt.h"
#include "SkRegion.h"
#include "SkMask.h"
#include "SkCoreBlitters.h"

#ifdef MRVL_SKIA_OPT_GCU
#include "gcu_mrvl.h"
#endif /* MRVL_SKIA_OPT_GCU */

class SkRGB16_Shader16_Blitter;
//////////////////////////////////////////////////////////////////////////////////////////////
/* ARGB8888 */

class SkARGB32_Blitter_MRVL : public SkARGB32_Blitter {
public:
    SkARGB32_Blitter_MRVL(const SkBitmap& device, const SkPaint& paint);

    virtual void blitRect(int x, int y, int width, int height);

private:
    typedef void(*BlitRectProc)(void*);
    BlitRectProc fBlitRectProc;
    int32_t fPrivateData[18];
    int32_t *fPrivateDataP;

#ifdef MRVL_SKIA_OPT_GCU
#if GCU_ENABLE_FILL
    bool           fCanGoGCU;
    int fillRectGCU(int x, int y, int width, int height);
#endif /* GCU_ENABLE_FILL */
#endif /* MRVL_SKIA_OPT_GCU */

    typedef SkARGB32_Blitter INHERITED;
};

class SkARGB32_Shader_Blitter_MRVL : public SkARGB32_Shader_Blitter {
public:
    SkARGB32_Shader_Blitter_MRVL(const SkBitmap& device, const SkPaint& paint)
                : INHERITED(device, paint) {};

    virtual ~SkARGB32_Shader_Blitter_MRVL() {};

    virtual void blitRect(int x, int y, int width, int height);
private:
    typedef SkARGB32_Shader_Blitter INHERITED;
};

//////////////////////////////////////////////////////////////////////////////////////////////
/* RGB565 */

#ifdef _MRVL_OPT_RGB565
class SkRGB16_Shader_Blitter_MRVL : public SkRGB16_Shader16_Blitter {
public:
    SkRGB16_Shader_Blitter_MRVL(const SkBitmap& device, const SkPaint& paint)
                : INHERITED(device, paint) {};

    virtual ~SkRGB16_Shader_Blitter_MRVL() {};

    virtual void blitRect(int x, int y, int width, int height);
private:
    typedef SkRGB16_Shader16_Blitter INHERITED;
};


#endif  /* _MRVL_OPT_RGB565 */
#endif   /* SkBlitter_MRVL_DEFINED */
#endif   /* _MRVL_OPT */

/* EOF */


