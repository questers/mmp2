/******************************************************************************
  *      Copyright (c) 2011 Marvell Corporation. All Rights Reserved.
  ******************************************************************************/
#ifdef MRVL_SKIA_OPT_GCU

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "SkBitmap.h"
#include "SkPixelRef.h"
#include "SkThread.h"
#include "gcu_mrvl.h"

static GCUContext g_pContext = GCU_NULL;
static SkMutex    g_GCUMutex;
static pid_t      g_pid      = -1;
#if GCU_USE_FENCE
static GCUFence   g_pFence   = GCU_NULL;
#endif

SkGCUBitmapSurface_MRVL::SkGCUBitmapSurface_MRVL(uint32_t uGenID, uint32_t uHeight, GCU_SURFACE_DATA* pData)
    : fGenerationID(uGenID), fSurface(NULL), fVA(pData->pPreAllocInfos[0].virtualAddr),
      fRowBytes(pData->pPreAllocInfos[0].stride), fWidth(pData->width), fHeight(uHeight)
{
    g_GCUMutex.acquire();
    fSurface    = gcuCreateSurface(g_pContext, pData);
    GCUenum ret = gcuGetError();
    if(ret != GCU_NO_ERROR) {
        SkDebugf(" ====  gcuCreateSurface error %d\n", ret);
    }
    g_GCUMutex.release();
}

SkGCUBitmapSurface_MRVL::~SkGCUBitmapSurface_MRVL()
{
    GCU_DEBUG_PRINT("==== SkGCUBitmapSurface_MRVL destructor %p fSurface %p\n", this, fSurface);
    if(fSurface) {
        g_GCUMutex.acquire();
        gcuDestroySurface(g_pContext, fSurface);
        GCU_DEBUG_PRINT("==== gcuDestroySurface\n");
        GCUenum ret = gcuGetError();
        if(ret != GCU_NO_ERROR) {
            SkDebugf(" ====  gcuDestroySurface error %d\n", ret);
        }
        g_GCUMutex.release();
    }
}

#if GCU_VIZIO_LAUNCHER2
#include <cutils/properties.h>
bool SkGCUBitmapSurface_MRVL::SkIsTarget()
{
#define SIZEFILENAME    128
#define LAUNCHER_NAME   "com.android.launcher"
#define SETTINGS_NAME   "com.android.settings"

    static bool  bTargetChecked     = false;
    static bool  bIsTarget          = false;

    if(!bTargetChecked)
    {
        pid_t pid = getpid();

        char buf[SIZEFILENAME];
        char procName[SIZEFILENAME];
        FILE* fp = NULL;

        char prop_value[16];
        property_get("marvell.skia.vizio_opt", prop_value, "0");
        if(strcmp(prop_value, "1") == 0)
        {
            snprintf(procName, sizeof(procName), "/proc/%i/cmdline", pid);
            fp = fopen(procName, "r");
            if(fp)
            {
                fgets(buf, SIZEFILENAME, fp);
                fclose(fp);
                fp = NULL;
                if ((strcmp(LAUNCHER_NAME,buf) == 0) ||
                    (strcmp(SETTINGS_NAME,buf) == 0))
                {
                    bIsTarget = true;
                }
            }
            else
            {
                SkDebugf("==== SkIsTarget open file %s failed \n", procName);
            }
        }
        bTargetChecked = true;
    }
    return bIsTarget;
#undef SIZEFILENAME
#undef LAUNCHER_NAME
#undef SETTINGS_NAME
}
#endif /* GCU_VIZIO_LAUNCHER2 */

#if GCU_ENABLE_CACHEFULSH
static bool flushBitmapRect(const SkBitmap *pBitmap, int left, int top, int width, int height, int dir)
{
    GCU_DEBUG_PRINT(" === flushBitmapRect\n");
    char *src = (char*)pBitmap->getAddr(left, top);
    int stride = pBitmap->rowBytes();
    int bytesPerPixel = pBitmap->bytesPerPixel();
    if(!src || stride < 0) {
        return false;
    }
#if AVOID_SCANLINE_FLUSH
    if(((width * bytesPerPixel << 1) > stride) && (dir != BMM_DMA_FROM_DEVICE ) ) {     /* if rect width > 1/2 bitmap width, do contiguous flush instead of scan line */
        GCU_DEBUG_PRINT("nearly contiguous bmm_flush_user(%p, %d, %d) %d x %d %d \n", src, height*stride, dir, width, height, stride);
        GCU_CALL_TIMING_START();
        bmm_flush_user(src, (height-1)*stride+width*bytesPerPixel, dir);
        GCU_CALL_TIMING_STOP("FlushBitmapRect");
        return true;
    }
#endif /* AVOID_SCANLINE_FLUSH */
    GCU_CALL_TIMING_START();
    if(stride == (int)(((uint32_t)_ALIGN_CEILING_MRVL(width, 32/bytesPerPixel)) * bytesPerPixel)) {
        GCU_DEBUG_PRINT("contiguous bmm_flush_user(%p, %d, %d) %d x %d %d\n", src, width*height*bytesPerPixel, dir, width, height, stride);
        bmm_flush_user(src, width*height*bytesPerPixel, dir);
    } else {
        GCU_DEBUG_PRINT("scanline %d x bmm_flush_user(%p, %d, %d) %d x %d %d\n", height, src, width*bytesPerPixel, dir, width, height, stride);
        int bytesPerLine = width*bytesPerPixel;
        for (int i = 0; i < height; i++) {
            bmm_flush_user(src, bytesPerLine, dir);
            src += stride;
        }
    }
    GCU_CALL_TIMING_STOP("FlushBitmapRect");
    return true;
}
#endif /* GCU_ENABLE_CACHEFULSH */

SkGCUBitmapSurface_MRVL* SkGCUBitmapSurface_MRVL::createSurfaceFromBitmap(const SkBitmap *pBitmap, bool bSrcSurface, bool bTruncateHeight, int nMaxY)
{
    int rowBytes = pBitmap->rowBytes();
    if(rowBytes <= 0) {                                                      /* stride positive check */
        GCU_DEBUG_PRINT("createSurfaceFromBitmap failed oops (negative stride): pBitmap = %p (config %d), rowBytes = 0x%08x\n", pBitmap, pBitmap->config(), rowBytes);
        return NULL;
    }

    if(pBitmap->config() == SkBitmap::kARGB_8888_Config) {
        if((uint32_t)(pBitmap->rowBytes()) & GCU_STRIDE_ALIGN_ARGB_MASK) {   /* stride 64 byte aligned, that covers width 16 byte aligned check */
            GCU_DEBUG_PRINT("createSurfaceFromBitmap failed oops (stride not 64 byte aligned in 8888 bitmap): pBitmap = %p (config %d), rowBytes = 0x%08x\n", pBitmap, pBitmap->config(), rowBytes);
            return NULL;
        }
    } else if(pBitmap->config() == SkBitmap::kRGB_565_Config) {
        if((uint32_t)(pBitmap->rowBytes()) & GCU_STRIDE_ALIGN_RGB565_MASK) { /* stride 32 byte aligned, that covers width 16 byte aligned check */
            GCU_DEBUG_PRINT("createSurfaceFromBitmap failed oops (stride not 32 byte aligned in 565 bitmap): pBitmap = %p (config %d), rowBytes = 0x%08x\n", pBitmap, pBitmap->config(), rowBytes);
            return NULL;
        }
    } else {
        GCU_DEBUG_PRINT("createSurfaceFromBitmap failed oops (unsupported bitmap config): pBitmap = %p (config %d)\n", pBitmap, pBitmap->config());
        return NULL;
    }

    SkGCUBitmapSurface_MRVL* pSurface   = NULL;
    GCUSurface               gcuSurface = NULL;
    SkPixelRef*              pPixelRef  = pBitmap->pixelRef();
    void*                    addr       = pBitmap->getPixels();
    uint32_t                 width      = pBitmap->rowBytesAsPixels();
    uint32_t                 height     = pBitmap->height();

    if(pBitmap->pixelRefOffset()) {
        height += pBitmap->pixelRefOffset() / rowBytes;
    }

    if(pPixelRef) {
        SkColorTable* ctable = pPixelRef->colorTable();

        if(_ISBITMAPSURFACE(ctable)) {
            pSurface = (SkGCUBitmapSurface_MRVL*)_CTABLE2BITMAPSURFACE(ctable);
            GCU_DEBUG_PRINT("createSurfaceFromBitmap bitmap %p pixelRef %p pSurface already exists %p refCnt %d gcuSurface %p\n", pBitmap, pPixelRef, pSurface, pSurface->getRefCnt(), pSurface->fSurface);

            if(pSurface->fHeight == 0) {                                     /* largest bitmap info not set yet */
                pSurface->fWidth    = width;
                pSurface->fRowBytes = rowBytes;
                pSurface->fHeight   = height;
            }

            if(height < pSurface->fHeight) {
                height = pSurface->fHeight;
            }

            addr = (void*)pSurface->fVA;
            gcuSurface = pSurface->fSurface;
        }
    }

    if((uint32_t)addr & GCU_ADDR_ALIGN_MASK) {                               /* addr 64 byte aligned */
        GCU_DEBUG_PRINT("createSurfaceFromBitmap failed oops: pBitmap = %p (config %d), rowBytes = 0x%08x, getPixels = %p, virtualAddr = %p, height = %d\n", pBitmap, pBitmap->config(), pBitmap->rowBytes(), pBitmap->getPixels(), addr, pBitmap->height());
        return NULL;
    }

    if(!bTruncateHeight) {
        if(height & GCU_HEIGHT_ALIGN_MASK) {
            if((pBitmap->pixelRefOffset() / rowBytes + nMaxY) > (height & ~GCU_HEIGHT_ALIGN_MASK)) {
                GCU_DEBUG_PRINT("createSurfaceFromBitmap failed oops (cannot use truncated surface): pBitmap = %p (config %d) maxY = %d, big height = %d\n", pBitmap, pBitmap->config(), (pBitmap->pixelRefOffset() / rowBytes + nMaxY), height);
                return NULL;
            }
        }
    }

    if(pSurface)
    {
        if(pSurface->fRowBytes != (uint32_t)rowBytes) {
            GCU_DEBUG_PRINT("createSurfaceFromBitmap failed oops (not a previous usage model): pBitmap = %p (config %d) height = %d, stored rowbytes = %d, new rowbytes = %d\n", pBitmap, pBitmap->config(), pBitmap->height(), pSurface->fRowBytes, rowBytes);
            return NULL;
        }
        if(gcuSurface) {
            pSurface->ref();
            return pSurface;
        }
    }

    GCU_SURFACE_DATA surfaceData;
    GCU_ALLOC_INFO   preAllocInfos[1];
    preAllocInfos[0].width                  = (GCUuint)width;
    preAllocInfos[0].height                 = (GCUuint)(height & ~GCU_HEIGHT_ALIGN_MASK);
    preAllocInfos[0].stride                 = (GCUuint)rowBytes;
    preAllocInfos[0].virtualAddr            = (GCUVirtualAddr)addr;
    preAllocInfos[0].physicalAddr           = NULL;
    surfaceData.location                    = GCU_SURFACE_LOCATION_VIDEO;//GCU_SURFACE_LOCATION_VIRTUAL;
    surfaceData.dimention                   = GCU_SURFACE_2D;
    surfaceData.flag.bits.preAllocVirtual   = GCU_TRUE;
    surfaceData.flag.bits.preAllocPhysical  = GCU_FALSE;
#ifdef MRVL_BGRA_HACK
    surfaceData.format                      = (pBitmap->config() == SkBitmap::kARGB_8888_Config)?GCU_FORMAT_ARGB8888:GCU_FORMAT_RGB565;
#else
    surfaceData.format                      = (pBitmap->config() == SkBitmap::kARGB_8888_Config)?GCU_FORMAT_ABGR8888:GCU_FORMAT_RGB565;
#endif
    surfaceData.width                       = (GCUuint)width;
    surfaceData.height                      = (GCUuint)(height & ~GCU_HEIGHT_ALIGN_MASK);
    surfaceData.arraySize                   = 1;
    surfaceData.pPreAllocInfos              = preAllocInfos;

    if(!pSurface) {
        GCU_CALL_TIMING_START();
        pSurface = new SkGCUBitmapSurface_MRVL(0, height, &surfaceData);
        GCU_CALL_TIMING_STOP("gcuCreateSurface");

        if(!pSurface->getSurface()) {
            delete pSurface;
            GCU_DEBUG_PRINT("createSurfaceFromBitmap failed oops: gcuCreateSurface return NULL!\n");
            return NULL;
        }

        GCU_DEBUG_PRINT("==== new SkGCUBitmapSurface_MRVL Bitmap (%p pixelRef %p) create pSurface %d x %d (%d), stride %d, VA %p, PA %p ----> %p (%p)\n",
            pBitmap, pPixelRef,
            preAllocInfos[0].width,
            preAllocInfos[0].height,    /* set height, truncated to align to 4 */
            height,                     /* real height */
            preAllocInfos[0].stride,
            preAllocInfos[0].virtualAddr,
            preAllocInfos[0].physicalAddr,
            pSurface, _CTABLE2SURFACE(_BITMAPSURFACE2CTABLE(pSurface)));
    } else {
        GCU_CALL_TIMING_START();
        g_GCUMutex.acquire();
        gcuSurface = gcuCreateSurface(g_pContext, &surfaceData);
        g_GCUMutex.release();
        GCU_CALL_TIMING_STOP("gcuCreateSurface");
        if(!gcuSurface) {
            GCU_DEBUG_PRINT("createSurfaceFromBitmap failed oops2: gcuCreateSurface return NULL!\n");
            return NULL;
        }
        GCU_DEBUG_PRINT("==== Bitmap (%p pixelRef %p) create pSurface %d x %d (%d), stride %d, VA %p, PA %p ----> %p (%p)\n",
            pBitmap, pPixelRef,
            preAllocInfos[0].width,
            preAllocInfos[0].height,    /* set height, truncated to align to 4 */
            height,                     /* real height */
            preAllocInfos[0].stride,
            preAllocInfos[0].virtualAddr,
            preAllocInfos[0].physicalAddr,
            pSurface, gcuSurface);

        pSurface->fSurface  = gcuSurface;
        pSurface->fHeight   = height;
        pSurface->fWidth    = width;
        pSurface->fRowBytes = rowBytes;
        pSurface->ref();
#if GCU_ENABLE_CACHEFULSH
        if(bSrcSurface) {
            GCU_DEBUG_PRINT("createSurfaceFromBitmap src bmm_flush_user 1st time\n");
            GCU_CALL_TIMING_START();
            //flushBitmapRect(pBitmap, 0, 0, width, height, BMM_DMA_TO_DEVICE);
            bmm_flush_user(addr, rowBytes * height, BMM_DMA_TO_DEVICE);
            GCU_CALL_TIMING_STOP("bmm_flush_user 1st time");
            pSurface->fGenerationID = pBitmap->getGenerationID();
        }
#endif
    }

    return pSurface;
}

extern "C" void __attribute__ ((destructor)) finiGCU(void)
{
    // SkDebugf("======== finiGCU =========\n");
    SkAutoMutexAcquire ac(g_GCUMutex);

    if(g_pContext) {
#if GCU_USE_FENCE
        if(g_pFence) {
            gcuDestroyFence(g_pContext, g_pFence);
            g_pFence = (GCUFence)0;
        }
#endif
        gcuDestroyContext(g_pContext);
        gcuTerminate();
        g_pContext = (GCUContext)0;
    }
}

bool SkGCUBitmapSurface_MRVL::initGCU()
{
    SkAutoMutexAcquire ac(g_GCUMutex);
    if(g_pContext && (getpid() == g_pid)) {
        return true;
    }
    g_pContext = GCU_NULL;
    g_pid      = -1;

    GCU_INIT_DATA    initData;
    GCU_CONTEXT_DATA contextData;

    memset(&initData, 0, sizeof(GCU_INIT_DATA));
    memset(&contextData, 0, sizeof(GCU_CONTEXT_DATA));

#if GCU_DRIVER_DEBUG
    initData.debug = GCU_TRUE;
#endif
    if(gcuInitialize(&initData) == GCU_FALSE) {
        SkDebugf(" ====  gcuInitialize error %d\n", gcuGetError());
        return false;
    }

    GCU_DEBUG_PRINT(" === gcuCreateContext\n");
    GCU_CALL_TIMING_START();
    g_pContext = gcuCreateContext(&contextData);
    GCU_CALL_TIMING_STOP("gcuCreateContext");

    if(!g_pContext) {
        SkDebugf("======== gcuCreateContext failed errorCode = %d\n", gcuGetError());
        gcuTerminate();
        return false;
    }

#if GCU_USE_FENCE
    g_pFence = gcuCreateFence(g_pContext);
    if(!g_pFence) {
        SkDebugf("======== gcuCreateFence failed errorCode = %d\n", gcuGetError());
        gcuDestroyContext(g_pContext);
        gcuTerminate();
        g_pContext = NULL;
        return false;
    }
#endif
    g_pid = getpid();
    atexit(&finiGCU);

    return true;
}

static bool blendAsync(GCU_BLEND_DATA *pBlendData, int nQuality)
{
    if(nQuality) {
        GCU_DEBUG_PRINT(" === gcuSet GCU_QUALITY \n");
        GCU_CALL_TIMING_START();
        if(nQuality == 2) {
            gcuSet(g_pContext, GCU_QUALITY, GCU_QUALITY_HIGH);
        } else {
            gcuSet(g_pContext, GCU_QUALITY, GCU_QUALITY_NORMAL);
        }
        GCU_CALL_TIMING_STOP("gcuSet GCU_QUALITY");
    }
#if GCU_ENABLE_GCUBLIT
    if(GCU_BLEND_SRC == pBlendData->blendMode) {
        GCU_DEBUG_PRINT(" === gcuBlit\n");
        GCU_CALL_TIMING_START();
        GCU_BLT_DATA blitData;
        memset(&blitData, 0, sizeof(GCU_BLT_DATA));
        blitData.pSrcSurface = pBlendData->pSrcSurface;
        blitData.pDstSurface = pBlendData->pDstSurface;
        blitData.pSrcRect    = pBlendData->pSrcRect;
        blitData.pDstRect    = pBlendData->pDstRect;
        blitData.rotation    = GCU_ROTATION_0;
        gcuBlit(g_pContext, &blitData);
        GCU_CALL_TIMING_STOP("gcuBlit");
    } else
#endif /* GCU_ENABLE_GCUBLIT */
    {
        GCU_DEBUG_PRINT(" === gcuBlend\n");
        GCU_CALL_TIMING_START();
        gcuBlend(g_pContext, pBlendData);
        GCU_CALL_TIMING_STOP("gcuBlend");
    }

    GCUenum ret = gcuGetError();

    if(ret != GCU_NO_ERROR) {
        SkDebugf(" ==== gcuBlend/gcuBlit error %d\n", ret);
        return false;
    }

#if GCU_USE_FENCE
    GCU_DEBUG_PRINT(" === gcuSendFence\n");
    GCU_CALL_TIMING_START();
    gcuSendFence(g_pContext, g_pFence);
    GCU_CALL_TIMING_STOP("gcuSendFence");
#endif /* GCU_USE_FENCE */

    GCU_DEBUG_PRINT(" === gcuFlush\n");
    GCU_CALL_TIMING_START();
    gcuFlush(g_pContext);
    GCU_CALL_TIMING_STOP("gcuFlush");

    return true;
}

static bool blendSync(GCU_BLEND_DATA *pBlendData, int nQuality)
{
    if(nQuality) {
        GCU_DEBUG_PRINT(" === gcuSet GCU_QUALITY \n");
        GCU_CALL_TIMING_START();
        if(nQuality == 2) {
            gcuSet(g_pContext, GCU_QUALITY, GCU_QUALITY_HIGH);
        } else {
            gcuSet(g_pContext, GCU_QUALITY, GCU_QUALITY_NORMAL);
        }
        GCU_CALL_TIMING_STOP("gcuSet GCU_QUALITY");
    }

#if GCU_ENABLE_GCUBLIT
    if(GCU_BLEND_SRC == pBlendData->blendMode) {
        GCU_DEBUG_PRINT(" === gcuBlit\n");
        GCU_CALL_TIMING_START();
        GCU_BLT_DATA blitData;
        memset(&blitData, 0, sizeof(GCU_BLT_DATA));
        blitData.pSrcSurface = pBlendData->pSrcSurface;
        blitData.pDstSurface = pBlendData->pDstSurface;
        blitData.pSrcRect    = pBlendData->pSrcRect;
        blitData.pDstRect    = pBlendData->pDstRect;
        blitData.rotation    = GCU_ROTATION_0;
        gcuBlit(g_pContext, &blitData);
        GCU_CALL_TIMING_STOP("gcuBlit");
    } else
#endif /* GCU_ENABLE_GCUBLIT */
    {
        GCU_DEBUG_PRINT(" === gcuBlend\n");
        GCU_CALL_TIMING_START();
        gcuBlend(g_pContext, pBlendData);
        GCU_CALL_TIMING_STOP("gcuBlend");
    }

    GCUenum ret = gcuGetError();

    if(ret != GCU_NO_ERROR) {
        SkDebugf(" ==== gcuBlend/gcuBlit error %d\n", ret);
        return false;
    }

    GCU_DEBUG_PRINT(" === gcuFinish\n");
    GCU_CALL_TIMING_START();
    gcuFinish(g_pContext);
    GCU_CALL_TIMING_STOP("gcuFinish");

    return true;
}

static void syncGCU()
{
#if GCU_USE_FENCE

    GCU_DEBUG_PRINT(" === gcuWaitFence\n");
    GCU_CALL_TIMING_START();
    gcuWaitFence(g_pContext, g_pFence, GCU_INFINITE);
    GCU_CALL_TIMING_STOP("gcuWaitFence");

#else /* GCU_USE_FENCE */

    GCU_DEBUG_PRINT(" === gcuFinish\n");
    GCU_CALL_TIMING_START();
    gcuFinish(g_pContext);
    GCU_CALL_TIMING_STOP("gcuFinish");

#endif /* GCU_USE_FENCE */
}

bool SkGCUBitmapSurface_MRVL::blendAndFlush(const SkBitmap *pSrcBitmap, const SkBitmap *pDstBitmap, GCU_BLEND_DATA *pBlendData, int nQuality) const
{
#if GCU_ENABLE_CACHEFULSH
    if((pSrcBitmap->getGenerationID() == 0) || (pSrcBitmap->getGenerationID() != fGenerationID)) {
        GCU_RECT *pRect = pBlendData->pSrcRect;
        GCU_DEBUG_PRINT("==== Src changed!!! _FlushBitmapRect again genration ID %d vs %d\n",
                         fGenerationID,
                         pSrcBitmap->getGenerationID());
        fGenerationID= pSrcBitmap->getGenerationID();
        if(!flushBitmapRect(pSrcBitmap, pRect->left, pRect->top, pRect->right-pRect->left, pRect->bottom-pRect->top, BMM_DMA_TO_DEVICE)) {
            return false;
        }
    }

    int dir = BMM_DMA_FROM_DEVICE;
    if(pBlendData->blendMode == GCU_BLEND_SRC_OVER) {
        dir = BMM_DMA_BIDIRECTIONAL;
    }
    GCU_RECT *pRect = pBlendData->pDstRect;
    if(!flushBitmapRect(pDstBitmap, pRect->left, pRect->top, pRect->right-pRect->left, pRect->bottom-pRect->top, dir)) {
        return false;
    }
#endif /* GCU_ENABLE_CACHEFULSH */
    g_GCUMutex.acquire();
    if(!blendSync(pBlendData, nQuality)) {
        g_GCUMutex.release();
        return false;
    }
    g_GCUMutex.release();
    return true;
}

#if GCU_ENABLE_FILL
static bool Fillsync(GCU_FILL_DATA *pFillData)
{
    GCU_DEBUG_PRINT(" === gcuFill\n");
    GCU_CALL_TIMING_START();
    gcuFill(g_pContext, pFillData);
    GCU_CALL_TIMING_STOP("gcuFill");

    GCUenum ret = gcuGetError();

    if(ret != GCU_NO_ERROR) {
        SkDebugf(" ==== gcuFill error %d\n", ret);
        return false;
    }

    GCU_DEBUG_PRINT(" === gcuFinish\n");
    GCU_CALL_TIMING_START();
    gcuFinish(g_pContext);
    GCU_CALL_TIMING_STOP("gcuFinish");

    return true;
}

bool SkGCUBitmapSurface_MRVL::FillAndFlush(const SkBitmap *pDstBitmap, GCU_FILL_DATA *pFillData) const
{
#if GCU_ENABLE_CACHEFULSH
    GCU_RECT *pRect = pFillData->pRect;
    if(!flushBitmapRect(pDstBitmap, pRect->left, pRect->top, pRect->right-pRect->left, pRect->bottom-pRect->top, BMM_DMA_FROM_DEVICE)) {
        return false;
    }
#endif /* GCU_ENABLE_CACHEFULSH */
    g_GCUMutex.acquire();
    if(!Fillsync(pFillData)) {
        g_GCUMutex.release();
        return false;
    }
    g_GCUMutex.release();
    return true;
}
#endif /* GCU_ENABLE_FILL */

#if GCU_DUMP_SURFACES
void SkGCUBitmapSurface_MRVL::dump(const char * const pFilename, int nCount) const
{
    char fullname[100];
    snprintf(fullname, 100, "%s_%d.bmp", pFilename, nCount);
    g_GCUMutex.acquire();
    _gcuDumpSurface(g_pContext, fSurface, fullname);
    g_GCUMutex.release();
    GCUenum ret      = gcuGetError();
    if(ret != GCU_NO_ERROR) {
        SkDebugf(" ==== _gcuDumpSurface error %d\n", ret);
    }
}
#endif /* GCU_DUMP_SURFACES */

#endif /* MRVL_SKIA_OPT_GCU */

/* EOF */


