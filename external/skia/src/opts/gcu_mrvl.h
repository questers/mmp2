/******************************************************************************
  *      Copyright (c) 2011 Marvell Corporation. All Rights Reserved.
  ******************************************************************************/
#ifdef MRVL_SKIA_OPT_GCU

#ifndef _gcu_mrvl_DEFINED
#define _gcu_mrvl_DEFINED

#include "gcu.h"
#include "SkBitmap.h"

/* enable fill operation */
#define GCU_ENABLE_FILL                  0

/* debug macros 0/1 */
#define GCU_CALL_DEBUG                   0
#define GCU_DRIVER_DEBUG                 0
#define GCU_DUMP_BITMAPCALLS             0
#define GCU_DUMP_PIXELREFCALLS           0
#define GCU_DUMP_SURFACES                0
/* timing for all gcu calls */
#define GCU_CALLS_TIMING                 0

/* cache flush */
#define GCU_ENABLE_CACHEFULSH            1

#if GCU_ENABLE_CACHEFULSH
#include "bmm_lib.h"
#endif

/* replace gcuBlend (SRC mode) with gcuBlit */
#define GCU_ENABLE_GCUBLIT               1
/* ignore paint dither option as GCU doesn't support */
#define GCU_IGNORE_DITHER_FLAG           1
/* use send/wait fense instead of flush */
#define GCU_USE_FENCE                    1
/* special hack for vizio launcher */
#define GCU_VIZIO_LAUNCHER2              0

/* icon size in vizio launcher is 88 x 128 */
/* but icon -> gcu is slower (20fps vs. 18-19 fps) */
//#define WIDTH_LOW_BOUND                88
//#define HEIGHT_LOW_BOUND               128
#define WIDTH_LOW_BOUND_BLEND            256
#define HEIGHT_LOW_BOUND_BLEND           256

#define WIDTH_LOW_BOUND_FILL             256
#define HEIGHT_LOW_BOUND_FILL            256

#define WIDTH_LOW_BOUND_RESIZE_NEAREST   512
#define HEIGHT_LOW_BOUND_RESIZE_NEAREST  512

#define WIDTH_LOW_BOUND_RESIZE_BILINEAR  128
#define HEIGHT_LOW_BOUND_RESIZE_BILINEAR 128

#define GCU_ADDR_ALIGN                   64
#define GCU_WIDTH_ALIGN                  16
#define GCU_HEIGHT_ALIGN                 4
#define GCU_STRIDE_ALIGN_ARGB            (GCU_WIDTH_ALIGN*4)
#define GCU_STRIDE_ALIGN_RGB565          (GCU_WIDTH_ALIGN*2)

#define GCU_ADDR_ALIGN_MASK              (GCU_ADDR_ALIGN-1)
#define GCU_STRIDE_ALIGN_ARGB_MASK       (GCU_STRIDE_ALIGN_ARGB-1)
#define GCU_STRIDE_ALIGN_RGB565_MASK     (GCU_STRIDE_ALIGN_RGB565-1)
#define GCU_HEIGHT_ALIGN_MASK            (GCU_HEIGHT_ALIGN-1)


#define kImageHasSurface_Flag            0x1

#if GCU_CALL_DEBUG
#define GCU_DEBUG_PRINT                  SkDebugf
#else
#define GCU_DEBUG_PRINT(...)
#endif

#if GCU_DUMP_BITMAPCALLS
#define GCU_BITMAP_PRINT                 SkDebugf
#else
#define GCU_BITMAP_PRINT(...)
#endif

#if GCU_DUMP_PIXELREFCALLS
#define GCU_PIXELREF_PRINT               SkDebugf
#else
#define GCU_PIXELREF_PRINT(...)
#endif

#if GCU_ENABLE_CACHEFULSH
#define AVOID_SCANLINE_FLUSH             1
#endif


class SkGCUBitmapSurface_MRVL: public SkRefCnt {
public:
    explicit SkGCUBitmapSurface_MRVL(uint32_t uGenID, uint32_t uHeight, GCU_SURFACE_DATA* pData);
    explicit SkGCUBitmapSurface_MRVL(uint32_t uGenID, GCUVirtualAddr pVA)
        : fGenerationID(uGenID), fSurface(NULL), fVA(pVA), fRowBytes(0), fWidth(0), fHeight(0)
    { }

    virtual ~SkGCUBitmapSurface_MRVL();

    GCUSurface getSurface() const { return fSurface; }
    GCUVirtualAddr getVA() const { return fVA; }
    uint32_t getRowBytes() const { return fRowBytes; }
    uint32_t getWidth() const { return fWidth; }
    uint32_t getHeight() const { return fHeight; }

    void setRowBytes(uint32_t uRowBytes) { fRowBytes = uRowBytes; }
    void setWidth(uint32_t uWidth) { fWidth = uWidth; }
    void setHeight(uint32_t uHeight) { fHeight = uHeight; }

    bool blendAndFlush(const SkBitmap *pSrcBitmap, const SkBitmap *pDstBitmap, GCU_BLEND_DATA *pBlendData, int nQuality) const;
#if GCU_ENABLE_FILL
    bool FillAndFlush(const SkBitmap *pDstBitmap, GCU_FILL_DATA *pFillData) const;
#endif /* GCU_ENABLE_FILL */
#if GCU_DUMP_SURFACES
    void dump(const char * const pFilename, int nCount) const;
#endif /* GCU_DUMP_SURFACES */

    static SkGCUBitmapSurface_MRVL* createSurfaceFromBitmap(const SkBitmap *pBitmap, bool bSrcSurface, bool bTruncateHeight, int nMaxY);
    static bool initGCU();

#if GCU_VIZIO_LAUNCHER2
    static bool SkIsTarget();
#endif /* GCU_VIZIO_LAUNCHER2 */

private:
    mutable uint32_t    fGenerationID;
    GCUSurface          fSurface;
    GCUVirtualAddr      fVA;
    uint32_t            fRowBytes;
    uint32_t            fWidth;
    uint32_t            fHeight;
};

static inline void setGCURect(int x, int y, int w, int h, const SkBitmap *pBitmap, GCU_RECT *pOutRect) {
    size_t refOffset = pBitmap->pixelRefOffset();

    if(!refOffset) {
        pOutRect->left   = x;
        pOutRect->top    = y;
        pOutRect->right  = x + w;
        pOutRect->bottom = y + h;
    } else {
        int    stride    = pBitmap->rowBytes();
        int    subsetY   = refOffset / stride;
        int    subsetX   = (refOffset % stride) >> pBitmap->shiftPerPixel();
        pOutRect->left   = x + subsetX;
        pOutRect->top    = y + subsetY;
        pOutRect->right  = pOutRect->left + w;
        pOutRect->bottom = pOutRect->top  + h;
    }
}

static inline int getTruncatedHeight(int nMaxY, int nSurfaceHeight, const SkBitmap *pBitmap) {
    size_t   refOffset = pBitmap->pixelRefOffset();
    int      maxHeight = (nSurfaceHeight & ~GCU_HEIGHT_ALIGN_MASK);

    if(refOffset) {
        nMaxY += refOffset / pBitmap->rowBytes();
    }

    return (nMaxY > maxHeight) ? (nMaxY - maxHeight) : 0;
}


#define _ALIGN_CEILING_MRVL(val, align) (((((uint32_t)val)+(((uint32_t)align)-1))&(~(((uint32_t)align)-1))))

#define _ISBITMAPSURFACE(ctable)        (((uint32_t)(ctable)) & kImageHasSurface_Flag)
#define _ISCTABLE(ctable)               (ctable && ((((uint32_t)(ctable)) & kImageHasSurface_Flag) == 0))
#define _CTABLE2BITMAPSURFACE(ctable)   ((((uint32_t)(ctable)) & ~kImageHasSurface_Flag))
#define _CTABLE2SURFACE(ctable)         (((SkGCUBitmapSurface_MRVL*)(_CTABLE2BITMAPSURFACE(ctable)))->getSurface())
#define _BITMAPSURFACE2CTABLE(surface)  (((uint32_t)(surface)) | kImageHasSurface_Flag)
#define _CTABLE2ADDR(ctable)            (((SkGCUBitmapSurface_MRVL*)(_CTABLE2BITMAPSURFACE(ctable)))->getVA())


#if GCU_CALLS_TIMING
#include "SkTime.h"

#define GCU_CALL_TIMING_START()                     \
    {\
        SkMSec t1 = SkTime::GetUSecs();

#define GCU_CALL_TIMING_STOP(name)                  \
        SkMSec t2 = SkTime::GetUSecs();             \
        SkDebugf("+++++ %s : %d us\n", name, t2-t1);\
    }

#else /* !GCU_CALLS_TIMING */

#define GCU_CALL_TIMING_START(...)
#define GCU_CALL_TIMING_STOP(...)

#endif  /* GCU_CALLS_TIMING */

#endif /* _gcu_mrvl_DEFINED */

#endif /* MRVL_SKIA_OPT_GCU */

/* EOF */

