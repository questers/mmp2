@/******************************************************************************
@ *      Copyright (c) 2010 Marvell Corporation. All Rights Reserved.
@ ******************************************************************************/
//#include "SkColorPriv.h"

    .ifndef __MRVL_OPT_INC_
    .set __MRVL_OPT_INC_, 1

@32bpp color format, this should align with platform specific SkUserConfig.h and SkColorPriv.h
@Only support ARGB and ABGR (A-MSB)

  .if (MRVL_BGRA_HACK==1)     @Android hacked, chrome

    .equ MRVL_B32_SHIFT,  0
    .equ MRVL_G32_SHIFT,  8
    .equ MRVL_R32_SHIFT,  16
    .equ MRVL_A32_SHIFT,  24

  .else                       @Android default

    .equ MRVL_R32_SHIFT,  0
    .equ MRVL_G32_SHIFT,  8
    .equ MRVL_B32_SHIFT,  16
    .equ MRVL_A32_SHIFT,  24

  .endif

@==========================================================================================
@const to shuffle alpha to wRx from source ARGB/RGBA/ABGR
@ WSHUFH wRalpha, wRsrc, #SRCALPHA_SHUFH_CONST      wRalpha = [SA SA SA SA]
    .if (MRVL_A32_SHIFT == 24)
        .equ SRCALPHA_SHUFH_CONST, 0xff
    .else
     .if(MRVL_A32_SHIFT == 0)
        .equ SRCALPHA_SHUFH_CONST, 0x0
     .endif
    .endif

@==========================================================================================
@macro to repack src ARGB/RGBA/ABGR to RGBA (to align with unpacked dst565->RGBA)
    .macro _Src32RepackToRGBA_ dst, src
 .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 16)       @ARGB
    WSHUFH   \dst, \src,  #0x93
 .endif
 .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 0)        @ABGR
    WSHUFH   \dst, \src,  #0x1B
 .endif
 .if (MRVL_A32_SHIFT == 0) && (MRVL_R32_SHIFT == 24)        @RGBA
    @nothing to do
 .endif
    .endm

@==========================================================================================
@macro to get SIMD (4-pixel, H) A, R, G, B from 4 ARGB/RGBA/ABGR pixels
@src1, src2 are polluted if any of dstR, dstG, dstB, dstA are overlapped with them.
@please make sure dstB isn't same as either src1 or src2
@msk = [00 ff 00 ff 00 ff 00 ff]

    .macro _Src32Vectorize_ src1, src2, dstR, dstG, dstB, dstA, msk

 .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 16)       @ARGB
                                                    @ 0xd8 = 0b11011000
    WSHUFH          \src1,  \src1, #0xd8            @ src1  = src[A1 R1 A0 R0 G1 B1 G0 B0]
    WSHUFH          \src2,  \src2, #0xd8            @ src2  = src[A3 R3 A2 R2 G3 B3 G2 B2]

    WUNPCKIHW       \dstB,  \src1, \src2            @ dstB  = src[A3 R3 A2 R2 A1 R1 A0 R0]
    WUNPCKILW       \dstG,  \src1, \src2            @ dstG  = src[G3 B3 G2 B2 G1 B1 G0 B0]
    WSRLH           \dstA,  \dstB,  #8              @ dstA  = src[   A3    A2    A1    A0]
    WAND            \dstR,  \dstB, \msk             @ dstR  = src[   R3    R2    R1    R0]
    WAND            \dstB,  \dstG, \msk             @ dstB  = src[   B3    B2    B1    B0]
    WSRLH           \dstG,  \dstG, #8               @ dstG  = src[   G3    G2    G1    G0]
 .endif

 .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 0)        @ABGR
                                                    @ 0xd8 = 0b11011000
    WSHUFH          \src1,  \src1, #0xd8            @ src1  = src[A1 B1 A0 B0 G1 R1 G0 R0]
    WSHUFH          \src2,  \src2, #0xd8            @ src2  = src[A3 B3 A2 B2 G3 R3 G2 R2]

    WUNPCKIHW       \dstB,  \src1, \src2            @ dstR  = src[A3 B3 A2 B2 A1 B1 A0 B0]
    WSRLH           \dstA,  \dstB,  #8              @ dstA  = src[   A3    A2    A1    A0]
    WAND            \dstB,  \dstB, \msk             @ dstB  = src[   B3    B2    B1    B0]
    WUNPCKILW       \dstG,  \src1, \src2            @ src2  = src[G3 R3 G2 R2 G1 R1 G0 R0]
    WAND            \dstR,  \dstG, \msk             @ dstB  = src[   R3    R2    R1    R0]
    WSRLH           \dstG,  \dstG, #8               @ dstG  = src[   G3    G2    G1    G0]

 .endif

 .if (MRVL_A32_SHIFT == 0) && (MRVL_R32_SHIFT == 24)        @RGBA
    @TO do
 .endif

    .endm

@==========================================================================================
@macro to pack one src ARGB/RGBA/ABGR pixel to dst RGB565
@src is polluted

    .macro _Src32PackToDst565_ src, dst, tmp, mask1, mask2, mask3

  .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 16)      @ARGB

@maskr = mask1 = [00 f8 00 00 00 f8 00 00]
@maskg = mask2 = [00 00 fc 00 00 00 fc 00]
@maskb = mask3 = [00 00 00 f8 00 00 00 f8]

    WAND    \tmp, \src, \mask1            @ tmp = r
    WAND    \dst, \src, \mask2            @ dst = g
    WAND    \src, \src, \mask3            @ src = b

    WSRLW   \tmp, \tmp, #8                @ r >> 8
    WSRLW   \src, \src, #3                @ b >> 3
    WOR     \tmp, \tmp, \src
    WSRLW   \dst, \dst, #5                @ g >> 5
    WOR     \dst, \tmp, \dst              @ dst[half0] = [RRRRRGGGGGGBBBBB]

  .endif

  .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 0)       @ABGR

@maskb = mask1 = [00 f8 00 00 00 f8 00 00]
@maskg = mask2 = [00 00 fc 00 00 00 fc 00]
@maskr = mask3 = [00 00 00 f8 00 00 00 f8]
    WAND    \tmp, \src, \mask1            @ tmp = b
    WAND    \dst, \src, \mask2            @ dst = g
    WAND    \src, \src, \mask3            @ src = r

    WSRLW   \tmp, \tmp, #19               @ b >> 19
    WSLLW   \src, \src, #8                @ r << 8
    WOR     \tmp, \tmp, \src
    WSRLW   \dst, \dst, #5                @ g >> 5
    WOR     \dst, \tmp, \dst              @ dst[half0] = [RRRRRGGGGGGBBBBB]

  .endif

    .endm

@==========================================================================================
@arm version macro to pack one src ARGB/RGBA/ABGR pixel to dst RGB565
@src is polluted

    .macro _Src32PackToDst565_arm_ src, dst, tmp1, tmp2

  .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 16)      @ARGB

    AND     \tmp2, \src,  #0xf80000                     @R5<<8
    AND     \tmp1, \src,  #0xf8                         @B5<<3
    AND     \dst,  \src,  #0xfc00                       @G6<<5
    MOV     \dst,  \dst,  lsr #5                        @G6
    ORR     \dst,  \dst,  \tmp1, lsr #3                 @G6B5
    ORR     \dst,  \dst,  \tmp2, lsr #8                 @R5G6B5

  .endif

  .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 0)       @ABGR

    MOV     \tmp2, \src, lsl #8                         @BGR0
    MOV     \tmp1, \src, lsr #5
    AND     \dst,  \tmp2, #0xf800                       @R5<<13
    AND     \tmp1, \tmp1, #0x7e0                        @G6<<5
    ORR     \dst,  \dst, \tmp1                          @R5G6<<5
    ORR     \dst,  \dst, \tmp2, lsr #27                 @ tmp2 lsr #27 = B5

  .endif
    .endm

@==========================================================================================
@macro to pack 4 src ARGB/RGBA/ABGR pixels to 4 dst RGB565
@src1 and src2 are polluted
@ maskrb= [00f8|00f8|00f8|00f8]
@ maskg = [fc00|fc00|fc00|fc00]

    .macro _Src32PackToDst565_4pixels_ src1, src2, dst, tmp1, tmp2, maskrb, maskg

  .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 16)      @ARGB
                                        @ 0xD8 = 0b11011000
    WSHUFH      \src1, \src1, #0xD8         @ src1 = [A1 R1 A0 R0 G1 B1 G0 B0]
    WSHUFH      \src2, \src2, #0xD8         @ src2 = [A3 R3 A2 R2 G3 B3 G2 B2]
    WUNPCKILW   \dst, \src1, \src2          @ dst  = [G3 B3 G2 B2 G1 B1 G0 B0]
    WUNPCKIHW   \tmp1, \src1, \src2         @ tmp1 = [A3 R3 A2 R2 A1 R1 A0 R0]
    WAND        \tmp2, \dst, \maskrb        @ b3_0 = ([G3_0 B3_0] & 0xf8)
    WSRLH       \tmp2, \tmp2, #3            @ tmp2 = b3_0 >> 3 = [b3 b2 b1 b0]
    WAND        \dst, \dst, \maskg          @ g3_0 = ([G3_0 B3_0] & 0xfc00)
    WSRLH       \dst, \dst, #5              @ dst = g3_0 >> 5 = [g3 g2 g1 g0]
    WAND        \tmp1, \tmp1, \maskrb       @ r3_0 = ([A3_0 R3_0] & 0xf8)
    WSLLH       \tmp1, \tmp1, #8            @ tmp1 = r3_0 << 8 = [r3 r2 r0 r1]

    WOR         \dst, \dst, \tmp2
    WOR         \dst, \dst, \tmp1           @ dst = [rgb3 rgb2 rgb1 rgb0]

  .endif

  .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 0)       @ABGR
                                        @ 0xD8 = 0b11011000
    WSHUFH      \src1, \src1, #0xD8         @ src1 = [A1 B1 A0 B0 G1 R1 G0 R0]
    WSHUFH      \src2, \src2, #0xD8         @ src2 = [A3 B3 A2 B2 G3 R3 G2 R2]
    WUNPCKILW   \dst, \src1, \src2          @ dst =  [G3 R3 G2 R2 G1 R1 G0 R0]
    WUNPCKIHW   \tmp1, \src1, \src2         @ tmp1 = [A3 B3 A2 B2 A1 B1 A0 B0]
    WAND        \tmp2, \dst, \maskrb        @ r3_0 = ([G3_0 R3_0] & 0xf8)
    WSLLH       \tmp2, \tmp2, #8            @ tmp2 = r3_0 << 8 = [r3 r2 r0 r1]
    WAND        \dst, \dst, \maskg          @ g3_0 = ([G3_0 3_0] & 0xfc00)
    WSRLH       \dst, \dst, #5              @ dst = g3_0 >> 5 = [g3 g2 g1 g0]
    WAND        \tmp1, \tmp1, \maskrb       @ r3_0 = ([A3_0 B3_0] & 0xf8)
    WSRLH       \tmp1, \tmp1, #3            @ tmp1 = b3_0 >> 3 = [b3 b2 b1 b0]

    WOR         \dst, \dst, \tmp2
    WOR         \dst, \dst, \tmp1           @ dst = [rgb3 rgb2 rgb1 rgb0]

  .endif
    .endm

@==========================================================================================
@macro to pack 4 src ARGB/RGBA/ABGR pixels to 4 dst RGB565, and compare if alphas are 0xff,
@wCASF is passed to CPSR, use condition mi to judge if alphas are all 0xff
@src1 and src2 are polluted
@ maskrb= [00f8|00f8|00f8|00f8]
@ maskg = [fc00|fc00|fc00|fc00]

    .macro _Src32PackToDst565_4pixels_AlphaComp_ src1, src2, dsta, tmp1, tmp2, maskrb, maskg, maskFF, flag0, flag1

  .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 16)      @ARGB
                                        @ 0xD8 = 0b11011000
    WSHUFH      \src1, \src1, #0xD8         @ src1 = [A1 R1 A0 R0 G1 B1 G0 B0]
    WSHUFH      \src2, \src2, #0xD8         @ src2 = [A3 R3 A2 R2 G3 B3 G2 B2]
    WUNPCKIHW   \tmp1, \src1, \src2         @ tmp1 = [A3 R3 A2 R2 A1 R1 A0 R0]
    TANDCW      r15                         @ eq means alpha is zero for all 4 pixels
    beq         \flag0
    WSRLH       \dsta, \tmp1, #8
    WXOR        \dsta, \dsta, \maskFF
    TORCW       r15                         @ eq means alpha is one for all 4 pixels
    bne         100f
    WUNPCKILW   \dsta, \src1, \src2         @ dsta = [G3 B3 G2 B2 G1 B1 G0 B0]
    WAND        \src1, \dsta, \maskrb       @ b3_0 = ([G3_0 B3_0] & 0xf8)
    WSRLH       \src1, \src1, #3            @ src1 = b3_0 >> 3 = [b3 b2 b1 b0]
    WAND        \dsta, \dsta, \maskg        @ g3_0 = ([G3_0 B3_0] & 0xfc00)
    WSRLH       \dsta, \dsta, #5            @ dsta = g3_0 >> 5 = [g3 g2 g1 g0]
    WAND        \tmp1, \tmp1, \maskrb       @ r3_0 = ([A3_0 R3_0] & 0xf8)
    WSLLH       \tmp1, \tmp1, #8            @ tmp1 = r3_0 << 8 = [r3 r2 r0 r1]

    WOR         \dsta, \dsta, \src1
    WOR         \dsta, \dsta, \tmp1         @ dsta = [rgb3 rgb2 rgb1 rgb0]
    b           \flag1
100:
  .endif

  .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 0)       @ABGR
                                        @ 0xD8 = 0b11011000
    WSHUFH      \src1, \src1, #0xD8         @ src1 = [A1 B1 A0 B0 G1 R1 G0 R0]
    WSHUFH      \src2, \src2, #0xD8         @ src2 = [A3 B3 A2 B2 G3 R3 G2 R2]
    WUNPCKIHW   \tmp1, \src1, \src2         @ tmp1 = [A3 B3 A2 B2 A1 B1 A0 B0]
    TANDCW      r15                         @ eq means alpha is zero for all 4 pixels
    beq         \flag0
    WSRLH       \dsta, \tmp1, #8
    WXOR        \dsta, \dsta, \maskFF
    TORCW       r15                         @ eq means alpha is one for all 4 pixels
    bne         100f
    WUNPCKILW   \dsta, \src1, \src2         @ dsta = [G3 R3 G2 R2 G1 R1 G0 R0]
    WAND        \src1, \dsta, \maskrb       @ r3_0 = ([G3_0 R3_0] & 0xf8)
    WSLLH       \src1, \src1, #8            @ src1 = r3_0 << 8 = [r3 r2 r0 r1]
    WAND        \dsta, \dsta, \maskg        @ g3_0 = ([G3_0 3_0] & 0xfc00)
    WSRLH       \dsta, \dsta, #5            @ dsta = g3_0 >> 5 = [g3 g2 g1 g0]
    WAND        \tmp1, \tmp1, \maskrb       @ r3_0 = ([A3_0 B3_0] & 0xf8)
    WSRLH       \tmp1, \tmp1, #3            @ tmp1 = b3_0 >> 3 = [b3 b2 b1 b0]

    WOR         \dsta, \dsta, \src1
    WOR         \dsta, \dsta, \tmp1         @ dsta = [rgb3 rgb2 rgb1 rgb0]
    b           \flag1
100:
  .endif
    .endm

@==========================================================================================
@macro to get SIMD (4-pixel, H) A, R, G, B from 4 ARGB/RGBA/ABGR pixels
@src1, src2 are polluted if any of dstR, dstG, dstB are overlapped with them. dsrB need to be
@the same register as tmp1 in _Src32PackToDst565_4pixels_AlphaComp_
@please make sure dstB is not same as either src1 or src2
@msk = [00 ff 00 ff 00 ff 00 ff]

    .macro _Src32Vectorize_part_ src1, src2, dstR, dstG, dstB, msk

 .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 16)       @ARGB
    WUNPCKILW       \dstG,  \src1, \src2            @ dstG  = src[G3 B3 G2 B2 G1 B1 G0 B0]
    WAND            \dstR,  \dstB, \msk             @ dstR  = src[   R3    R2    R1    R0]
    WAND            \dstB,  \dstG, \msk             @ dstB  = src[   B3    B2    B1    B0]
    WSRLH           \dstG,  \dstG, #8               @ dstG  = src[   G3    G2    G1    G0]
 .endif

 .if (MRVL_A32_SHIFT == 24) && (MRVL_R32_SHIFT == 0)        @ABGR
    WAND            \dstB,  \dstB, \msk             @ dstB  = src[   B3    B2    B1    B0]
    WUNPCKILW       \dstG,  \src1, \src2            @ src2  = src[G3 R3 G2 R2 G1 R1 G0 R0]
    WAND            \dstR,  \dstG, \msk             @ dstB  = src[   R3    R2    R1    R0]
    WSRLH           \dstG,  \dstG, #8               @ dstG  = src[   G3    G2    G1    G0]

 .endif

 .if (MRVL_A32_SHIFT == 0) && (MRVL_R32_SHIFT == 24)        @RGBA
    @TO do
 .endif

    .endm

@==========================================================================================
@macro to get SIMD (4-pixel, H) A, R, G, B from 4 ARGB/RGBA/ABGR pixels.
@wCASF is passed to CPSR, alpha 0 are tested
@maskFF = [00 ff 00 ff 00 ff 00 ff]

    .macro _Src32_4pixels_AlphaZeroTest_ src1, src2, tmp1, tmp2, flag

 .if (MRVL_A32_SHIFT == 24)                         @ ARGB or ABGR
                                                    @ 0xd8 = 0b11011000
    WSHUFH          \tmp1,  \src1, #0xd8            @ tmp1  = src[A1 R1 A0 R0 G1 B1 G0 B0]
    WSHUFH          \tmp2,  \src2, #0xd8            @ tmp2  = src[A3 R3 A2 R2 G3 B3 G2 B2]
    WUNPCKIHW       \tmp1,  \tmp1, \tmp2            @ tmp1  = src[A3 R3 A2 R2 A1 R1 A0 R0]
    TANDCW          r15                             @ eq means alpha is zero for all 4 pixels
    beq             \flag

 .endif

    .endm

@==========================================================================================
@macro to get SIMD (4-pixel, H) A, R, G, B from 4 ARGB/RGBA/ABGR pixels
@wCASF is passed to CPSR, alpha 0 and 0xff are all tested
@maskFF = [00 ff 00 ff 00 ff 00 ff]

    .macro _Src32_4pixels_AlphaZeroOneTest_ src1, src2, tmp1, tmp2, dstA, dstOneMinusA, maskFF, flagzero, flagone

 .if (MRVL_A32_SHIFT == 24)                          @ ARGB or ABGR
                                                     @ 0xd8 = 0b11011000
    WSHUFH          \tmp1,  \src1, #0xd8             @ tmp1 = src[A1 R1 A0 R0 G1 B1 G0 B0]
    WSHUFH          \tmp2,  \src2, #0xd8             @ tmp2 = src[A3 R3 A2 R2 G3 B3 G2 B2]
    WUNPCKIHW       \dstA,  \tmp1, \tmp2             @ dstA = src[A3 R3 A2 R2 A1 R1 A0 R0]
    TANDCW          r15                              @ eq means alpha is zero for all 4 pixels
    beq             \flagzero
    WSRLH           \dstA,  \dstA, #8                @ dstA = src[   A3    A2    A1    A0]
    WXOR            \dstOneMinusA, \dstA, \maskFF
    TORCW           r15                              @ eq means alpha is one for all 4 pixels
    beq             \flagone

 .endif
    .endm


@The data struct defined in blend_scale_opts_wmmx2.h

    .equ PIXELPIPE_OFFSET_COVERAGE_WIDTH, 0x00
    .equ PIXELPIPE_OFFSET_COVERAGE_HEIGHT, 0x04
    .equ PIXELPIPE_OFFSET_IMGMATRIX_SX, 0x08
    .equ PIXELPIPE_OFFSET_IMGMATRIX_SHY, 0x0c
    .equ PIXELPIPE_OFFSET_IMGMATRIX_SHX, 0x10
    .equ PIXELPIPE_OFFSET_IMGMATRIX_SY, 0x14
    .equ PIXELPIPE_OFFSET_IMGSCANLINE_PTR, 0x18
    .equ PIXELPIPE_OFFSET_IMAGE_PTR, 0x1c
    .equ PIXELPIPE_OFFSET_IMAGE_DEPTH, 0x20
    .equ PIXELPIPE_OFFSET_IMAGE_STRIDE, 0x24
    .equ PIXELPIPE_OFFSET_IMAGE_WIDTH, 0x28
    .equ PIXELPIPE_OFFSET_IMAGE_HEIGHT, 0x2c
    .equ PIXELPIPE_OFFSET_IMAGE_STARTX, 0x30
    .equ PIXELPIPE_OFFSET_IMAGE_STARTY, 0x34
    .equ PIXELPIPE_OFFSET_DST_PTR, 0x38
    .equ PIXELPIPE_OFFSET_DST_STRIDE, 0x3c
    .equ PIXELPIPE_OFFSET_BLENDCONST, 0x40



    .endif





