/******************************************************************************
  *      Copyright (c) 2010 Marvell Corporation. All Rights Reserved.
  ******************************************************************************/
#ifdef _MRVL_OPT
#ifndef _Mrvl_opt_DEFINED
#define _Mrvl_opt_DEFINED

#include "SkUserConfig.h"
#include "SkLog.h"

#ifndef _MRVL_OPT
#undef _MRVL_OPT_RGB565
#endif

#define FloatToQ16(f)    (int32_t) (((f) > 0) ? ((f) * 65536 + 0.5) : ((f) * 65536 - 0.5))
#define IntToQ16(i)                  (int32_t)((i)<<16)

#define iRTWidth   4096
#define alignBytes 32

typedef struct _PRIVATE_DATA{
    /* pipeline data */
    int32_t                       width;
    int32_t                       height;
    int32_t                       matrixImg_Q16[4];   // sx, shy, shx, sy

    /* image generation */
    uint8_t*                      pImgScanLine;       // a visible span image generation result
    uint8_t*                      pImage;
    int32_t                       iImageDepth;
    int32_t                       iImageStride;
    int32_t                       iImageWidth;
    int32_t                       iImageHeight;

    int32_t                       imgStartX;          // fx_Q16_Q20
    int32_t                       imgStartY;          // fy_Q16_Q20


    /* blending data */
    uint8_t*                      pDst;
    int32_t                       iDstStride;
    int32_t                       blendConst;
} PRIVATE_DATA_T;


static inline void _dump_privateData(void *pData)
{
    PRIVATE_DATA_T *pd = (PRIVATE_DATA_T*)pData;
    PRINT_BLIT("\n=== private data at 0x%x\n", (uint32_t)pData);
    PRINT_BLIT("\n=== width = %d, height = %d\n", pd->width, pd->height);
    PRINT_BLIT("\n=== matrixImg_Q16[0] = %d, [1] = %d, [2] = %d, [3] = %d\n", pd->matrixImg_Q16[0], pd->matrixImg_Q16[1], pd->matrixImg_Q16[2], pd->matrixImg_Q16[3]);
    PRINT_BLIT("\n=== pImgScanLine = 0x%x, pImage = 0x%x\n", (uint32_t)pd->pImgScanLine, (uint32_t)pd->pImage);
    PRINT_BLIT("\n=== iImageDepth = %d, iImageStride = %d, iImageWidth = %d, iImageHeight = %d, imgStartX = %d, imgStartY = %d\n",
            pd->iImageDepth, pd->iImageStride, pd->iImageWidth, pd->iImageHeight, pd->imgStartX, pd->imgStartY);
    PRINT_BLIT("\n=== pDst = 0x%x, iDstStride = %d, blendConst = %d\n", (uint32_t)pd->pDst, pd->iDstStride, pd->blendConst);
}

extern "C" {

////////////////////////////////////////////////////////////////////////////////////////////
/* BlitRect functions */
/* dst ARGB8888 */
/* resize  */
void _BlendSrc32ToDst32_SrcOverDst_clamp_nobilinear_resize_MRVL(void* pPrivateData);
void _BlendSrc32ToDst32_SrcOverDst_clamp_bilinear_resize_MRVL(void* pPrivateData);
void _BlendSrc32ToDst32_SrcOverDst_repeat_nobilinear_resize_MRVL(void* pPrivateData);
void _BlendSrc32ToDst32_SrcOverDst_repeat_bilinear_resize_MRVL(void* pPrivateData);
void _BlendSrc32ToDst32_Src_clamp_bilinear_resize_NoGlobalAlpha_MRVL(void* pPrivateData);
void _BlendSrc32ToDst32_Src_clamp_nobilinear_resize_NoGlobalAlpha_MRVL(void* pPrivateData);

void _BlendSrc565ToDst32_Src_clamp_nobilinear_resize_MRVL(void* pPrivateData);
void _BlendSrc565ToDst32_Src_clamp_nobilinear_resize_NoGlobalAlpha_MRVL(void* pPrivateData);

/* translate */
void _BlendSrc32ToDst32_SrcOverDst_repeat_nobilinear_translate_MRVL(void* pPrivateData);
void _BlendSrc32ToDst32_SrcOverDst_clamp_nobilinear_translate_MRVL(void* pPrivateData);

/* FastImage */
void _BlendSrc32ToDst32_SrcOverDst_FastImage_MRVL(void* pPrivateData);
void _BlendSrc32ToDst32_SrcOverDst_FastImage_NoGlobalAlpha_MRVL(void* pPrivateData);
void _BlendSrc565ToDst32_Src_FastImage_NoGlobalAlpha_MRVL(void *pPrivateData);

/* const fill */
void _BlendSrc32ToDst32_SrcOverDst_Const_NoGlobalAlpha_MRVL(void* pPrivateData);
void _BlendSrc32ToDst32_Src_Const_NoGlobalAlpha_MRVL(void* pPrivateData);

#define s32_d32_opaque_MRVL                  memcpy_MRVL

////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _MRVL_OPT_RGB565
/* dst RGB565 */
/* resize  */
void _BlendSrc32ToDst565_SrcOverDst_clamp_nobilinear_resize_MRVL(void* pPrivateData);
void _BlendSrc32ToDst565_SrcOverDst_clamp_bilinear_resize_MRVL(void* pPrivateData);
void _BlendSrc32ToDst565_SrcOverDst_repeat_nobilinear_resize_MRVL(void* pPrivateData);
void _BlendSrc32ToDst565_SrcOverDst_repeat_bilinear_resize_MRVL(void* pPrivateData);

/* FastImage */
void _BlendSrc32ToDst565_SrcOverDst_FastImage_MRVL(void* pPrivateData);
void _BlendSrc32ToDst565_SrcOverDst_FastImage_NoGlobalAlpha_MRVL(void* pPrivateData);
void _BlendSrc32ToDst565_Src_FastImage_NoGlobalAlpha_MRVL(void* pPrivateData);


void _BlendSrc565ToDst565_Src_clamp_nobilinear_resize_MRVL(void* pPrivateData);
void _BlendSrc565ToDst565_Src_clamp_nobilinear_resize_NoGlobalAlpha_MRVL(void* pPrivateData);
void _BlendSrc565ToDst565_Src_clamp_bilinear_resize_NoGlobalAlpha_MRVL(void* pPrivateData);


////////////////////////////////////////////////////////////////////////////////////////////
/* Blit Row functions */
void s32_d565_opaque_MRVL(uint16_t* dst, const uint32_t* src, size_t size);
void s32a_d565_opaque_MRVL(uint16_t* dst, const uint32_t* src, size_t size);
void S32a_opaque_MRVL(uint32_t* dst,const uint32_t* src, int count, uint8_t alpha);
void S32a_blend_MRVL(uint32_t* dst,const uint32_t* src, int count, uint8_t alpha);
#define s565_d565_opaque_MRVL       memcpy_MRVL

void s32a_d565_opaque_dither_MRVL(uint16_t* dst, const uint32_t* src, int count, uint8_t alpha, int x, int y);
void s32_d565_opaque_dither_MRVL(uint16_t* dst, const uint32_t* src, int count, uint8_t alpha, int x, int y);

void s565_d565_blend_MRVL(uint16_t* dst, uint16_t* src, uint8_t alpha, int count);
void sconst_d565_blend_MRVL(uint16_t* dst, uint32_t color, uint8_t alpha, int count);
void sblack_d565_blend_MRVL(uint16_t* dst, uint8_t alpha, int count);

////////////////////////////////////////////////////////////////////////////////////////////
/* Blit Mask functions */
void _BlitMaskA8Dst565_Opaque_MRVL(uint16_t* device, const uint8_t* alpha, int width, int height, unsigned int deviceRB, unsigned int maskRB, uint16_t color);

#endif   /* _MRVL_OPT_RGB565 */

////////////////////////////////////////////////////////////////////////////////////////////
/* Blit Row functions */
void sconst_d32_blend_MRVL(uint32_t* dst, uint32_t color, int count);

////////////////////////////////////////////////////////////////////////////////////////////
/* Blit Mask functions */
void _BlitMaskA8Dst32_Opaque_MRVL(uint32_t* device, const uint8_t* alpha, int width, int height,unsigned int deviceRB, unsigned int maskRB, uint32_t color);
void _BlitMaskA8Dst32_MRVL(uint32_t* device, const uint8_t* alpha, int width, int height,unsigned int deviceRB, unsigned int maskRB, uint32_t color);

////////////////////////////////////////////////////////////////////////////////////////////
/* Xfermode functions */
void plus_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void xor_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void dstover_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void srcin_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void dstin_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void srcout_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void dstout_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void srcatop_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void dstatop_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void multiply_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void screen_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void overlay_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void darken_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void lighten_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);
void hardlight_modeproc_xfer32_MRVL(SkPMColor* dst, const SkPMColor* src, int width);


//////////////////////////////////////////////////////////////////////////////////////////////
/* memcpy memset */
void memcpy_MRVL(void *dst, const void *src, size_t count);
}

#endif /* _Mrvl_opt_DEFINED */

#endif /* _MRVL_OPT */
/* EOF */

