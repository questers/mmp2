/******************************************************************************
 *    Copyright (c) 2010 Marvell Corporation. All Rights Reserved.
 ******************************************************************************/
#if (defined _MRVL_OPT) && !defined(_MRVL_MUTE_IPPJPEG)

#include "SkImageDecoder.h"
#include "SkImageEncoder.h"
#include "SkColorPriv.h"
#include "SkDither.h"
#include "SkScaledBitmapSampler.h"
#include "SkStream.h"
#include "SkTemplates.h"
#include "SkUtils.h"
#include "SkLog.h"
#include "SkJpegUtility.h"

extern "C" {
    #include "codecJP.h"
    #include "misc.h"
}


#define MARVELL_LOG(...)    PRINT_JPEG(__VA_ARGS__)


#define TIME_DECODE


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

class IPPJPEGImageDecoder : public SkJPEGImageDecoder {
public:
    virtual Format getFormat() const {
        return kJPEG_Format;
    }

protected:
    virtual bool onDecode(SkStream* stream, SkBitmap* bm, Mode);
private:
    int fBytesRead;
    int fBytesTotal; 
    IppCodecStatus JPEGDec_Init(void *pInStreamHandler, IppBitstream *pSrcBitStream, 
                IppPicture *pDstPicture, MiscGeneralCallbackTable  *pSrcCallbackTable, void **ppDecoderState);

    IppCodecStatus JPEG_ROIDec(void *pInStreamHandler, IppBitstream *pSrcBitStream,
                IppPicture *pDstPicture, IppJPEGDecoderParam *pDecoderPar, void *pDecoderState);
                
    typedef SkJPEGImageDecoder INHERITED;            
    
};

//////////////////////////////////////////////////////////////////////////
/* our source struct for directing jpeg to our stream object
*/

/* Automatically clean up after throwing an exception */
class JPEGAutoClean {
public:
    JPEGAutoClean(): pCBTbl(NULL), pCodec(NULL) {}
    ~JPEGAutoClean() {
    if (pCodec) {
        DecoderFree_JPEG(&pCodec);
    }
        if (pCBTbl) {
            miscFreeGeneralCallbackTable(&pCBTbl);
        }
    }
    void set(MiscGeneralCallbackTable* pT, void* pC) {
        pCBTbl = pT;
        pCodec = pC;
    }
private:
    MiscGeneralCallbackTable* pCBTbl;
    void* pCodec;
};
///////////////////////////////////////////////////////////////////////////////
#if 0
/* Note below code is not used: (1) it's not working (2) not necessary to replace 
   default allocator. But thinking this could be potentially taken as reference
   if there does have requirement to replace an allocator. Temporarily keep it here */
   
#include "SkMallocPixelRef.h"

class SkMallocPixelRef_MRVL : public SkMallocPixelRef {
public:
    SkMallocPixelRef_MRVL(void* addr, size_t size, SkColorTable* ctable) : INHERITED(addr, size, ctable)
    {   fStorage = addr;    };
    
    virtual ~SkMallocPixelRef_MRVL()
    {
        IPP_MemFree(&fStorage);
        //SkSafeUnref(fCTable);
        //sk_free(fStorage);        
    }   

private:
    void *fStorage;
    typedef SkMallocPixelRef INHERITED;
};

class SkHeapAllocator_MRVL : public SkBitmap::HeapAllocator{
public:
    SkHeapAllocator_MRVL(): fAlignment(8) {};
    ~SkHeapAllocator_MRVL() 
    {
    };
    virtual bool allocPixelRef(SkBitmap* dst, SkColorTable* ctable)
    {
        size_t size = dst->getSize();
        if (size < 0) {
            return false;
        }
        void *addr = NULL;
        IPP_MemMalloc(&addr, size, fAlignment);
        if (!addr) {
            return false;
        }

        dst->setPixelRef(new SkMallocPixelRef_MRVL(addr, size, ctable))->unref();
        dst->lockPixels();
        return true;    
    };
    
    void setAlignment(int alignment) {
        if(alignment & (alignment -1) || alignment > 32) {
            //2,4,8, 16, etc
            return;
        }
        fAlignment  = alignment;
    }
private:
    int             fAlignment;
};
#endif

///////////////////////////////////////////////////////////////////////////////

int ReadStreamFromFile(void **pDstBuf, int unit, int number, void *handler)
{
    Ipp8u *pBuffer;
    int readNum = 0;
    SkStream *phandler = (SkStream*)handler;
    pBuffer = /**((char **)(pDstBuf))*/(Ipp8u*)(*pDstBuf);
      
    readNum = phandler->read(pBuffer, unit*number);
    MARVELL_LOG("ReadSreamFromFile out readBytes=%d", readNum);
    return readNum;
}

/* This is a slow version, considering gray case is very rare */
static void gray2rgb(SkBitmap *bm)
{
    uint8_t* srcPtr = (uint8_t*)((uint8_t*)bm->getPixels() + bm->width()*bm->height() - 1);
    int byte_per_pixel = (bm->config() == SkBitmap::kARGB_8888_Config)? 4 : 2;
    uint8_t* dstPtr = (uint8_t*)((uint8_t*)bm->getPixels() + (bm->width()*bm->height() - 1)*byte_per_pixel);
    int count = bm->width()*bm->height();
    int i, j;
    uint8_t sample;

    if (byte_per_pixel == 4){
        uint32_t *dst_32 = (uint32_t*)dstPtr;
        for(i = 0; i < count; i++){
            sample = *srcPtr;
            *dst_32 = SkPackARGB32(0xFF, sample, sample, sample);
             srcPtr--;
             dst_32--;
        }
    } else if (byte_per_pixel == 2){
        uint16_t *dst_16 = (uint16_t*)dstPtr;
        for(i = 0; i < count; i++){
            sample = *srcPtr;
            *dst_16 = SkPackRGB16(sample>>3, sample>>2, sample>>3);
             srcPtr--;
             dst_16--;
        }
    } else {
        LOGE("bitmap config not supported %d", bm->config());
    }
}

/*
modify this number to match upper level input stream buffer
see frameworks/base/graphics/java/android/graphics/BitmapFactory.java decodeStream(), 
is.mark(marklimit), marklimit is the maximum value a BufferedInputStream can rewind. 
Be careful, JPEG_INP_STREAM_BUF_LEN should not exceed this value, as we need rewind
in case of failure to fall back into libJPEG. This doesn't guarantee to work as 
expected 100%, as JPEG header marker doesn't have a theorectical byte limit, in
rare case it could fail to rewind as well. 
This depends on a patch to BitmapFactory.java to modify marklimit from 1024 to 16*1024.
1024 is too small for us. 
*/
#define JPEG_INP_STREAM_BUF_LEN 1024*16
#define JPEG_STREAM_PREBUF_LEN 1024

static const char *JPEGSubSamplingStr[] = {
    "444", "422", "422I", "411", "Error"
};

//#define FILE_DUMP
#ifdef MRVL_BGRA_HACK
#define IPPJPEG_32BPP_FMT   JPEG_ABGR
#else
#define IPPJPEG_32BPP_FMT   JPEG_ARGB
#endif
    
bool IPPJPEGImageDecoder::onDecode(SkStream* stream, SkBitmap* bm, Mode mode) {
#ifdef TIME_DECODE
    AutoTimeMillis atm("IPP JPEG Decode");
    SkDebugf("JPEG Decode %s\n", mode == (SkImageDecoder::kDecodeBounds_Mode)?"kDecodeBounds_Mode":"kDecodePixels_Mode");
#endif
#ifdef FILE_DUMP
    static int tc = 0;
    SkString str("/sdcard/jpg_dump");
    str.appendf("_%d.jpg", tc++);
    SkDebugf("==================Dumping %s jpg\n", str.c_str());
    SkFILEWStream out(str.c_str());
    char *tmpbuf = new char[1024*32];
    if(tmpbuf) {
        int readcount = 0;
        do {
            readcount = stream->read(tmpbuf, 1024*32);
            if(readcount > 0)
               out.write(tmpbuf, readcount);
        } while(readcount > 0);
    }
    delete [] tmpbuf;
    stream->rewind();
#endif
    SkAutoMalloc  srcStorage;
    JPEGAutoClean autoClean;

    IppCodecStatus ret = IPP_STATUS_NOERR;
    IppBitstream pSrc;
    IppJPEGDecoderParam DecoderPar;
    IppPicture DstPic;
    MiscGeneralCallbackTable *pCallBackTable = NULL;
    void *pDecoderState = NULL;
    int nDstWidth, nDstHeight, bytesPerPixel;
    uint8_t* dstPtr;
    uint8_t* pBasePtr = NULL;
    uint8_t* ptmp = NULL;

    int picWidth, picHeight, sampleSize;

    fBytesTotal = stream->getLength();
    fBytesRead  = 0;

    pBasePtr = new Ipp8u[JPEG_STREAM_PREBUF_LEN + JPEG_INP_STREAM_BUF_LEN + IPP_EXTRA_TAIL_BYTES + 2];
    pSrc.pBsBuffer = pBasePtr + JPEG_STREAM_PREBUF_LEN;
    pSrc.pBsCurByte = pSrc.pBsBuffer;
    pSrc.bsByteLen = JPEG_INP_STREAM_BUF_LEN;
    pSrc.bsCurBitOffset = 0;

    memset(pSrc.pBsBuffer, 0, pSrc.bsByteLen);
    memset(&DecoderPar, 0, sizeof(IppJPEGDecoderParam));

    if (miscInitGeneralCallbackTable(&pCallBackTable) != 0 ) {
        LOGE("Init callback table failed");
        if (pBasePtr) {
            delete [] pBasePtr;
            pBasePtr = NULL;
        }
        if(stream->rewind()) {
            MARVELL_LOG("IPPJPEGImageDecoder::INHERITED::onDecode\n");
            return this->INHERITED::onDecode(stream, bm, mode);
        } else {
            return false;
        }
    }

    pCallBackTable->fFileRead = (MiscFileReadCallback)ReadStreamFromFile;

    ret = JPEGDec_Init((void*)stream, &pSrc, &DstPic, pCallBackTable, &pDecoderState);
    if(ret){
        LOGE("Init JPEG decoder failed %d", ret);
        if (pBasePtr) {
            delete [] pBasePtr;
            pBasePtr = NULL;
        }
        if(stream->rewind()) {
            MARVELL_LOG("IPPJPEGImageDecoder::INHERITED::onDecode\n");
            return this->INHERITED::onDecode(stream, bm, mode);
        } else {
            return false;
        }
    }

    autoClean.set(pCallBackTable, pDecoderState);

    picWidth = DstPic.picWidth;
    picHeight = DstPic.picHeight;

    /*  Try to fulfill the requested sampleSize. Since jpeg can do it (when it
        can) much faster that we, just use their num/denom api to approximate
        the size.
    */
    sampleSize = this->getSampleSize();

    SkBitmap::Config config = this->getPrefConfig(k32Bit_SrcDepth, false);

    // only these make sense for jpegs
    if (config != SkBitmap::kARGB_8888_Config &&
        config != SkBitmap::kARGB_4444_Config &&
        config != SkBitmap::kRGB_565_Config) {
        config = SkBitmap::kARGB_8888_Config;
    }

    if (!this->chooseFromOneChoice(config, DstPic.picWidth,
                                   DstPic.picHeight)) {
        return false;
    }

    switch(config) {
        case SkBitmap::kARGB_8888_Config:
            bytesPerPixel = 4;
            DecoderPar.UnionParamJPEG.roiDecParam.nDesiredColor = (DstPic.picChannelNum == 1)? JPEG_GRAY8 : IPPJPEG_32BPP_FMT;
            break;
        case SkBitmap::kRGB_565_Config:
            bytesPerPixel = 2;
            DecoderPar.UnionParamJPEG.roiDecParam.nDesiredColor = (DstPic.picChannelNum == 1)? JPEG_GRAY8 : JPEG_BGR565;
            break;
        default:
            bytesPerPixel = 4;
            config = SkBitmap::kARGB_8888_Config;
            DecoderPar.UnionParamJPEG.roiDecParam.nDesiredColor = (DstPic.picChannelNum == 1)? JPEG_GRAY8 : IPPJPEG_32BPP_FMT;
            break;
    }

    SkScaledBitmapSampler sampler(picWidth, picHeight,
                                  sampleSize);

    nDstWidth = sampler.scaledWidth();
    nDstHeight = sampler.scaledHeight();

    /* will return the aligned size */
    if (mode == SkImageDecoder::kDecodeBounds_Mode) {
        bm->setConfig(config, nDstWidth, nDstHeight);
        bm->setIsOpaque(true);
        MARVELL_LOG(" ************ IPP JPEG finish decode (mode=kDecodeBounds_Mode) ************\n");
        MARVELL_LOG(" *** config=%d, resolution=[%d %d], subsampling=%s\n", config, nDstWidth, nDstHeight, JPEGSubSamplingStr[DstPic.picFormat]);
        if (pBasePtr) {
            delete []pBasePtr;
            pBasePtr = NULL;
        }
        return true;
    }
    

    bm->setConfig(config, nDstWidth, nDstHeight);
    bm->setIsOpaque(true);

    /* Note: alignment: 
        1. default allocator is heap allocator which calls malloc.  It guarantees 8-byte alignment. 
        from bionic source(bionic/libc/bionic/dlmalloc.c)
          Alignment:                                     8 bytes (default)
               This suffices for nearly all current machines and C compilers.
               However, you can define MALLOC_ALIGNMENT to be wider than this
               if necessary (up to 128bytes), at the expense of using more space.
        
        MALLOC_ALIGNMENT                            default: (size_t)8
          Controls the minimum alignment for malloc'ed chunks.  It must be a
          power of two and at least 8, even on machines for which smaller
          alignments would suffice. It may be defined as larger than this
          though. Note however that code and data structures are optimized for
          the case of 8-byte alignment.

        2. If higher alignment is required, a new allocator needs be implemented and set up. But I 
        failed to set up below SkHeapAllocator_MRVL (which calls IPP_MemMalloc and IPP_MemFree, 
        see code at ~L.101). I guess the reason is that free is called out of ImageDecoder life cycle 
        and IPP_MemFree isn't guaranteed to be called (why).  Thinking this should be fixable,  I 
        keep the unused code there in case it'll be useful one day. 
    */
        
    //this->setAllocator(new SkHeapAllocator_MRVL());
    if (!this->allocPixelRef(bm, NULL)) {
        LOGE("Failed to allocPixelRef");
        delete [] pBasePtr;
        pBasePtr = NULL;
        return false;
    }

    SkAutoLockPixels alp(*bm);
    dstPtr = (uint8_t*)bm->getPixels();

    DecoderPar.UnionParamJPEG.roiDecParam.nDstWidth  = nDstWidth;
    DecoderPar.UnionParamJPEG.roiDecParam.nDstHeight = nDstHeight;

    DstPic.picWidth         = nDstWidth;
    DstPic.picHeight        = nDstHeight;
    DstPic.picPlaneNum      = 1;
    DstPic.picPlaneStep[0]  = ((DstPic.picChannelNum == 1) ? 1 : bytesPerPixel) * nDstWidth;
    DstPic.ppPicPlane[0]    = dstPtr;

    DecoderPar.nModeFlag = IPP_JPEG_ROIDEC_STD;
    DecoderPar.UnionParamJPEG.roiDecParam.srcROI.x = 0;
    DecoderPar.UnionParamJPEG.roiDecParam.srcROI.y = 0;
    DecoderPar.UnionParamJPEG.roiDecParam.srcROI.width = picWidth;
    DecoderPar.UnionParamJPEG.roiDecParam.srcROI.height = picHeight;
    DecoderPar.UnionParamJPEG.roiDecParam.nDstWidth  = nDstWidth;
    DecoderPar.UnionParamJPEG.roiDecParam.nDstHeight = nDstHeight;
    DecoderPar.UnionParamJPEG.roiDecParam.nAlphaValue = 255;
    DecoderPar.UnionParamJPEG.roiDecParam.iLPCutFreq = -1;

    ret = JPEG_ROIDec((void *)stream, &pSrc, &DstPic, &DecoderPar, pDecoderState);

    if(ret){
        LOGE("Failed to decode jpeg pic, ret=%d, dst picture pixel buffer = 0x%08x\n", ret, (uint32_t)DstPic.ppPicPlane[0]);
        if(pBasePtr) {
            delete [] pBasePtr;
            pBasePtr  = NULL;
        }
        if(stream->rewind()) {
            MARVELL_LOG("IPPJPEGImageDecoder::INHERITED::onDecode\n");
            return this->INHERITED::onDecode(stream, bm, mode);
        } else {
            return false;
        }
    }

    if(DstPic.picChannelNum == 1){
        MARVELL_LOG("gray2rgb, bm (%d %d), config=%d\n", bm->width(), bm->height(), bm->config());
        gray2rgb(bm);
    }

    MARVELL_LOG(" ********** IPP JPEG finish decode (mode = kDecodePixels_Mode) ***********\n");
    MARVELL_LOG(" *** config=%d, mode=%d, sampleSize=%d, subsampling=%s\n", config, mode, sampleSize, JPEGSubSamplingStr[DstPic.picFormat]);
    MARVELL_LOG(" *** src=[%d %d], dst=[%d %d]\n", picWidth, picHeight, nDstWidth, nDstHeight);
    MARVELL_LOG(" *** Desired color=%d alpha=%d Bpp=%d, dst picture pixel buffer =0x%08x\n", DecoderPar.UnionParamJPEG.roiDecParam.nDesiredColor, 
        DecoderPar.UnionParamJPEG.roiDecParam.nAlphaValue, bytesPerPixel, (uint32_t)DstPic.ppPicPlane[0]);

    if (pBasePtr) {
        delete []pBasePtr;
        pBasePtr = NULL;
    }
//  this->setAllocator(NULL);
    return true;
}

/******************************************************************************
// Name:                JPEGDec_Init
// Description:         Init the JPEG decoder, memory callback function table and
//                      input stream buffer.
//
// Input Arguments:
//              fp:     Pointer to input JPEG file
//   pSrcBitStream:     Pointer to the src bit stream
//ppSrcCallbackTable:   Double pointer to the call back function table
//
// Output Arguments:    
//      pDstPicture:    Point to the IppPicture, which returns the JPEG image
//                      properties, such as width, height, format, etc.
//   ppDecoderState:    Double pointer to the updated decoder state structure
//
// Returns:
//        [Success]     General IPP return code
//        [Failure]     General IPP return code                 
******************************************************************************/
IppCodecStatus IPPJPEGImageDecoder::JPEGDec_Init(void *pInStreamHandler, IppBitstream *pSrcBitStream, 
            IppPicture *pDstPicture, MiscGeneralCallbackTable  *pSrcCallbackTable, void **ppDecoderState)
{
    int nReadByte, done = 0;
    IppCodecStatus rtCode; 
    MiscFileReadCallback funcFileRead;

    /* Check input argument */
    if (! (pInStreamHandler && pSrcBitStream && pDstPicture && pSrcCallbackTable && ppDecoderState))  {
        return IPP_STATUS_BADARG_ERR;
    }

    funcFileRead = pSrcCallbackTable->fFileRead;
    /*The first file read do not need cpy to working buffer. This is designed to optimize the performance for some special 
    usage model. Eg: the decoded pic file size is very large(eg: 10M), the user buffer size is also large(2M)*/

    /* 1. Read the first JPEG stream block */
    nReadByte = funcFileRead((void**)&pSrcBitStream->pBsBuffer, 1, pSrcBitStream->bsByteLen, pInStreamHandler);
    if(!nReadByte) {
        return IPP_STATUS_ERR;
    }
    fBytesRead += nReadByte;
    MARVELL_LOG("=========read %d bytes, accumulated read %d bytes, total stream length %d bytes\n", nReadByte, fBytesRead, fBytesTotal);
    if(fBytesRead == fBytesTotal && fBytesTotal != 0) {  /* stream end */
        pSrcBitStream->pBsBuffer[nReadByte++] = 0xFF;
        pSrcBitStream->pBsBuffer[nReadByte++] = 0xD9;
        MARVELL_LOG("===============================append FF D9\n");
    }
    pSrcBitStream->pBsCurByte = pSrcBitStream->pBsBuffer;
    pSrcBitStream->bsByteLen = nReadByte;
    pSrcBitStream->bsCurBitOffset = 0;

    /* 2. InitAlloc the JPEG decoder state */
    while(!done) {
        rtCode = DecoderInitAlloc_JPEG (pSrcBitStream, pDstPicture, pSrcCallbackTable, ppDecoderState);
        
        switch(rtCode){
            case IPP_STATUS_NEED_INPUT:
            {
                nReadByte = funcFileRead((void**)&pSrcBitStream->pBsBuffer, 1, JPEG_INP_STREAM_BUF_LEN, pInStreamHandler);
                MARVELL_LOG("=========read %d bytes, accumulated read %d bytes, total stream length %d bytes\n", nReadByte, fBytesRead, fBytesTotal);
                if(!nReadByte) {
                    return IPP_STATUS_ERR;
                }
                fBytesRead += nReadByte;
                if(fBytesRead == fBytesTotal && fBytesTotal != 0) {  /* stream end */
                    pSrcBitStream->pBsBuffer[nReadByte++] = 0xFF;
                    pSrcBitStream->pBsBuffer[nReadByte++] = 0xD9;
                    MARVELL_LOG("===============================append FF D9\n");
                }
                pSrcBitStream->bsByteLen = nReadByte;
                pSrcBitStream->pBsCurByte = pSrcBitStream->pBsBuffer;
                pSrcBitStream->bsCurBitOffset = 0;
            //  appiCopyStream_JPEG(pSrcBitStream, (IppJPEGDecoderState*)(*ppDecoderState));
                break;
            }
            case IPP_STATUS_NOERR:
                done = 1;
                break;

            case IPP_STATUS_BADARG_ERR:
            case IPP_STATUS_NOMEM_ERR:
            case IPP_STATUS_BITSTREAM_ERR:
            case IPP_STATUS_NOTSUPPORTED_ERR:
            case IPP_STATUS_ERR:
            default:
                return rtCode;
        }
    }

    return IPP_STATUS_NOERR;
}

/******************************************************************************
// Name:                JPEGDec_ROI
// Description:         Main JPEG decoder function: ROI decoding both for 
//                      standard mode and interactive mode.
//
// Input Arguments:
//         srcFile:     Pointer to input JPEG file
//         dstFile:     Pointer to output JPEG file
//   pSrcBitStream:     Pointer to the src bit stream
//      pDecoderPar:    Pointer to the decoder configuration structure
//
// Output Arguments:    
//      pDstPicture:    Point to the IppPicture, which returns the JPEG image
//                      properties, such as width, height, format, etc.
//   ppDecoderState:    Double pointer to the updated decoder state structure
//
// Returns:
//        [Success]     General IPP return code
//        [Failure]     General IPP return code                 
******************************************************************************/
IppCodecStatus IPPJPEGImageDecoder::JPEG_ROIDec(void *pInStreamHandler, IppBitstream *pSrcBitStream,
            IppPicture *pDstPicture, IppJPEGDecoderParam *pDecoderPar, void *pDecoderState)
{
    //IppJPEGDecoderState *pState = (IppJPEGDecoderState*)pDecoderState;
//  MiscFileReadCallback funcFileRead;
    int nReadByte, done = 0;
    IppCodecStatus rtCode; 

    /* BAC check */
    if(!(pDecoderState && pInStreamHandler && pSrcBitStream && pDstPicture && pDecoderPar)) {
        return IPP_STATUS_BADARG_ERR;
    }

//  funcFileRead = pState->ijxFileRead;

    while(!done) {
        rtCode = Decode_JPEG (pSrcBitStream, NULL, pDstPicture, pDecoderPar, pDecoderState);
        
        switch(rtCode) {
        case IPP_STATUS_OUTPUT_DATA:
            /* Not supported in platform release */
            break;

        case IPP_STATUS_NEED_INPUT:
        {
            nReadByte = ReadStreamFromFile((void**)&pSrcBitStream->pBsBuffer, 1, JPEG_INP_STREAM_BUF_LEN, pInStreamHandler);
            if(!nReadByte) {
                return IPP_STATUS_ERR;
            }
            fBytesRead += nReadByte;
            MARVELL_LOG("=========read %d bytes, accumulated read %d bytes, total stream length %d bytes\n", nReadByte, fBytesRead, fBytesTotal);
            if(fBytesRead == fBytesTotal && fBytesTotal != 0) {  /* stream end */
                pSrcBitStream->pBsBuffer[nReadByte++] = 0xFF;
                pSrcBitStream->pBsBuffer[nReadByte++] = 0xD9;
                MARVELL_LOG("===============================append FF D9\n");
            }
            pSrcBitStream->bsByteLen = nReadByte;
            pSrcBitStream->pBsCurByte = pSrcBitStream->pBsBuffer;
            pSrcBitStream->bsCurBitOffset = 0;
            //appiCopyStream_JPEG(pSrcBitStream, pState);
            break;
        }

        case IPP_STATUS_JPEG_EOF:
            done = 1;
            break;

        case IPP_STATUS_BADARG_ERR:
        case IPP_STATUS_NOMEM_ERR:
        case IPP_STATUS_BITSTREAM_ERR:
        case IPP_STATUS_NOTSUPPORTED_ERR:
        case IPP_STATUS_ERR:
        default:
            return rtCode;
        }
    }

    return IPP_STATUS_NOERR;
}

///////////////////////////////////////////////////////////////////////////////

#include "SkTRegistry.h"

static SkImageDecoder* DFactory(SkStream* stream) {
    static const char gHeader[] = { 0xFF, 0xD8, 0xFF };
    static const size_t HEADER_SIZE = sizeof(gHeader);

    char buffer[HEADER_SIZE];
    size_t len = stream->read(buffer, HEADER_SIZE);

    if (len != HEADER_SIZE) {
        return NULL;   // can't read enough
    }
    if (memcmp(buffer, gHeader, HEADER_SIZE)) { 
        return NULL;
    }
    return SkNEW(IPPJPEGImageDecoder);
 }


static SkTRegistry<SkImageDecoder*, SkStream*> gDReg(DFactory);
#endif

/* EOF */
 
