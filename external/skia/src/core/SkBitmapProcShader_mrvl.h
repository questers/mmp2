/******************************************************************************
 *       Copyright (c) 2010 Marvell Corporation. All Rights Reserved.
 ******************************************************************************/
#ifdef _MRVL_OPT
#ifndef SkBitmapProcShader_MRVL_DEFINED
#define SkBitmapProcShader_MRVL_DEFINED

#include "SkShader.h"
#include "SkBitmapProcState.h"
#ifdef MRVL_SKIA_OPT_GCU
#include "gcu_mrvl.h"
#endif

class SkShader_MRVL   {
public:
    enum Flags {
        kIsAcceleratedForBitmapProc_MRVL = 0x100,
        kIsAcceleratedForGradient_MRVL   = 0x200,
        kIsAcceleratedMask_MRVL          = 0x300,
    };
};

class SkBitmapProcShader_MRVL : public SkBitmapProcShader {
public:
    typedef void (*ShadeRectOptProc)(void*);
    ShadeRectOptProc fInternalProc;
    SkBitmapProcShader_MRVL(const SkBitmap& src, TileMode tx, TileMode ty) : INHERITED(src, tx, ty)
    {
        fFreeImgScanLineP = NULL;
        fFreeImgScanLineAlignedP = NULL;
        fInternalProc = NULL;
        int startIndex = ((uint32_t)(&fPrivateData[0]) & 7) ? 1 : 0;
        fPrivateDataP = &fPrivateData[startIndex];
        fAllocatedBytes = 0;
#ifdef MRVL_SKIA_OPT_GCU
        fCanGoGCU = SkGCUBitmapSurface_MRVL::initGCU();
#endif
    };

    // overrides from SkBitmapProcShader
    virtual ~SkBitmapProcShader_MRVL()
    {
        if(fFreeImgScanLineP) {
            sk_free(fFreeImgScanLineP);
            fFreeImgScanLineP = NULL;
        }
    };

    virtual uint32_t getFlags() { return (SkShader_MRVL::kIsAcceleratedForBitmapProc_MRVL | this->INHERITED::getFlags()); };

    virtual bool setContext(const SkBitmap&, const SkPaint&, const SkMatrix&);

    virtual bool ShadeRect(int x, int y, int width, int height, const SkBitmap *pDstBitmap);

protected:

private:
    SkBitmap::Config fSconfig;
    int32_t  fPrivateData[18];
    int32_t* fPrivateDataP;
    uint8_t* fFreeImgScanLineP;
    uint8_t* fFreeImgScanLineAlignedP;
    int32_t  fAllocatedBytes;
#ifdef MRVL_SKIA_OPT_GCU
    bool     fCanGoGCU;
    int      fGCUAccel;
    /* avoid copy constructor (SkBitmapProcShader::fRawBitmap) and surface recreation,
     * but this isn't always the right src bitmap, sometimes it's bitmap's mipmap, need check before draw */
    int      ShadeRectGCU(int x, int y, int width, int height, const SkBitmap *pDstBitmap);
#endif
    typedef SkBitmapProcShader INHERITED;
};

#endif
#endif /* _MRVL_OPT */

/* EOF */

