/* libs/graphics/sgl/SkSpriteBlitter_RGB16.cpp
**
** Copyright 2006, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License"); 
** you may not use this file except in compliance with the License. 
** You may obtain a copy of the License at 
**
**     http://www.apache.org/licenses/LICENSE-2.0 
**
** Unless required by applicable law or agreed to in writing, software 
** distributed under the License is distributed on an "AS IS" BASIS, 
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
** See the License for the specific language governing permissions and 
** limitations under the License.
*/

#include "SkSpriteBlitter.h"
#include "SkBlitRow.h"
#include "SkTemplates.h"
#include "SkUtils.h"
#include "SkColorPriv.h"

#if defined(_MRVL_OPT) && defined(_MRVL_OPT_RGB565)
#include "SkLog.h"
#include "mrvl_opt.h"

#ifdef MRVL_SKIA_OPT_GCU
#include "gcu_mrvl.h"
#endif /* MRVL_SKIA_OPT_GCU */

#endif
#define D16_S32A_Opaque_Pixel(dst, sc)                                        \
do {                                                                          \
    if (sc) {                                                                 \
        *dst = SkSrcOver32To16(sc, *dst);                                     \
    }                                                                         \
} while (0)

static inline void D16_S32A_Blend_Pixel_helper(uint16_t* dst, SkPMColor sc,
                                               unsigned src_scale) {
    uint16_t dc = *dst;
    unsigned sa = SkGetPackedA32(sc);
    unsigned dr, dg, db;

    if (255 == sa) {
        dr = SkAlphaBlend(SkPacked32ToR16(sc), SkGetPackedR16(dc), src_scale);
        dg = SkAlphaBlend(SkPacked32ToG16(sc), SkGetPackedG16(dc), src_scale);
        db = SkAlphaBlend(SkPacked32ToB16(sc), SkGetPackedB16(dc), src_scale);
    } else {
        unsigned dst_scale = 255 - SkAlphaMul(sa, src_scale);
        dr = (SkPacked32ToR16(sc) * src_scale +
              SkGetPackedR16(dc) * dst_scale) >> 8;
        dg = (SkPacked32ToG16(sc) * src_scale +
              SkGetPackedG16(dc) * dst_scale) >> 8;
        db = (SkPacked32ToB16(sc) * src_scale +
              SkGetPackedB16(dc) * dst_scale) >> 8;
    }
    *dst = SkPackRGB16(dr, dg, db);
}

#define D16_S32A_Blend_Pixel(dst, sc, src_scale) \
    do { if (sc) D16_S32A_Blend_Pixel_helper(dst, sc, src_scale); } while (0)


///////////////////////////////////////////////////////////////////////////////

class Sprite_D16_S16_Opaque : public SkSpriteBlitter {
public:
    Sprite_D16_S16_Opaque(const SkBitmap& source)
        : SkSpriteBlitter(source) 
    {
#ifdef MRVL_SKIA_OPT_GCU
        fCanGoGCU = SkGCUBitmapSurface_MRVL::initGCU();
#endif /* MRVL_SKIA_OPT_GCU */
    }

    // overrides
    virtual void blitRect(int x, int y, int width, int height) {
        int leftHeight = height;
#ifdef MRVL_SKIA_OPT_GCU
        if(fCanGoGCU) {
            leftHeight = blitRectGCU(x, y, width, height);
            if(!leftHeight) {
                return;
            }
        }
#endif /* MRVL_SKIA_OPT_GCU */
        SK_RESTRICT uint16_t* dst = fDevice->getAddr16(x, y + (height - leftHeight));
        const SK_RESTRICT uint16_t* src = fSource->getAddr16(x - fLeft,
                                                             y - fTop + (height - leftHeight));
        unsigned dstRB = fDevice->rowBytes();
        unsigned srcRB = fSource->rowBytes();

        while (--leftHeight >= 0) {
#if defined(_MRVL_OPT) && defined(_MRVL_OPT_RGB565)
            s565_d565_opaque_MRVL(dst, src, width << 1);        /* s565_d565_opaque_MRVL so far defined as memcpy_MRVL */
#else
            memcpy(dst, src, width << 1);
#endif
            dst = (uint16_t*)((char*)dst + dstRB);
            src = (const uint16_t*)((const char*)src + srcRB);
        }
    }
#ifdef MRVL_SKIA_OPT_GCU
    int blitRectGCU(int x, int y, int width, int height)
    {
        if((width < WIDTH_LOW_BOUND_BLEND) || (height < HEIGHT_LOW_BOUND_BLEND)) {
            return height;
        }
        int dstMaxY = y + height;
        int srcMaxY = dstMaxY - fTop;

        SkGCUBitmapSurface_MRVL* srcBitmapSurface = SkGCUBitmapSurface_MRVL::createSurfaceFromBitmap(fSource, true, true, srcMaxY);
        if(!srcBitmapSurface) {
            return height;
        }
        SkGCUBitmapSurface_MRVL* dstBitmapSurface = SkGCUBitmapSurface_MRVL::createSurfaceFromBitmap(fDevice, false, true, dstMaxY);
        if(!dstBitmapSurface) {
            srcBitmapSurface->unref();
            return height;
        }
        GCU_DEBUG_PRINT("============= blitRectGCU RGB565 (%d, %d, %d, %d)=================\n", x, y, width, height);

        GCU_RECT srcRect, dstRect;
        int srcTruncatedHeight = getTruncatedHeight(srcMaxY, srcBitmapSurface->getHeight(), fSource);
        int dstTruncatedHeight = getTruncatedHeight(dstMaxY, dstBitmapSurface->getHeight(), fDevice);
        int heightT = height - SkMax32(srcTruncatedHeight, dstTruncatedHeight);
        setGCURect(x - fLeft, y - fTop, width, heightT, fSource, &srcRect);
        setGCURect(x, y, width, heightT, fDevice, &dstRect);

        GCU_BLEND_DATA blendData;
        memset(&blendData, 0, sizeof(GCU_BLEND_DATA));
        blendData.pSrcRect          = &srcRect;
        blendData.pDstRect          = &dstRect;
        blendData.rotation          = GCU_ROTATION_0;
        blendData.srcGlobalAlpha    = 0xFF;
        blendData.dstGlobalAlpha    = 0xFF;
        blendData.pSrcSurface       = srcBitmapSurface->getSurface();
        blendData.pDstSurface       = dstBitmapSurface->getSurface();
        blendData.blendMode         = GCU_BLEND_SRC;

        GCU_DEBUG_PRINT("==== gcuBlend %p (Bitmap %p config %d %d x %d)(%d, %d - %d, %d) -- %p (Bitmap %p config %d %d x %d)(%d, %d - %d, %d) mode = %d alpha=0x%2x \n",
                         blendData.pSrcSurface, fSource, fSource->config(), fSource->width(), fSource->height(),
                         srcRect.left, srcRect.top, srcRect.right, srcRect.bottom,
                         blendData.pDstSurface, fDevice, fDevice->config(), fDevice->width(), fDevice->height(),
                         dstRect.left, dstRect.top, dstRect.right, dstRect.bottom,
                         blendData.blendMode,
                         blendData.srcGlobalAlpha);

        if(!srcBitmapSurface->blendAndFlush(fSource, fDevice, &blendData, 0)) {
            srcBitmapSurface->unref();
            dstBitmapSurface->unref();
            return height;
        } else {
            srcBitmapSurface->unref();
            dstBitmapSurface->unref();
            return height - heightT;
        }
    }
private:
    bool           fCanGoGCU;
#endif  /* MRVL_SKIA_OPT_GCU */
};

#define D16_S16_Blend_Pixel(dst, sc, scale)     \
    do {                                        \
        uint16_t dc = *dst;                     \
        *dst = SkBlendRGB16(sc, dc, scale);     \
    } while (0)

#define SkSPRITE_CLASSNAME                  Sprite_D16_S16_Blend
#define SkSPRITE_ARGS                       , uint8_t alpha
#define SkSPRITE_FIELDS                     uint8_t  fSrcAlpha;
#define SkSPRITE_INIT                       fSrcAlpha = alpha;
#define SkSPRITE_DST_TYPE                   uint16_t
#define SkSPRITE_SRC_TYPE                   uint16_t
#define SkSPRITE_DST_GETADDR                getAddr16
#define SkSPRITE_SRC_GETADDR                getAddr16
#define SkSPRITE_PREAMBLE(srcBM, x, y)      int scale = SkAlpha255To256(fSrcAlpha);
#define SkSPRITE_BLIT_PIXEL(dst, src)       D16_S16_Blend_Pixel(dst, src, scale)
#define SkSPRITE_NEXT_ROW
#define SkSPRITE_POSTAMBLE(srcBM)
#include "SkSpriteBlitterTemplate.h"

///////////////////////////////////////////////////////////////////////////////

#define D16_S4444_Opaque(dst, sc)           \
    do {                                    \
        uint16_t dc = *dst;                 \
        *dst = SkSrcOver4444To16(sc, dc);   \
    } while (0)

#define SkSPRITE_CLASSNAME                  Sprite_D16_S4444_Opaque
#define SkSPRITE_ARGS                       
#define SkSPRITE_FIELDS                     
#define SkSPRITE_INIT                       
#define SkSPRITE_DST_TYPE                   uint16_t
#define SkSPRITE_SRC_TYPE                   SkPMColor16
#define SkSPRITE_DST_GETADDR                getAddr16
#define SkSPRITE_SRC_GETADDR                getAddr16
#define SkSPRITE_PREAMBLE(srcBM, x, y)      
#define SkSPRITE_BLIT_PIXEL(dst, src)       D16_S4444_Opaque(dst, src)
#define SkSPRITE_NEXT_ROW
#define SkSPRITE_POSTAMBLE(srcBM)
#include "SkSpriteBlitterTemplate.h"

#define D16_S4444_Blend(dst, sc, scale16)           \
    do {                                            \
        uint16_t dc = *dst;                         \
        *dst = SkBlend4444To16(sc, dc, scale16);    \
    } while (0)


#define SkSPRITE_CLASSNAME                  Sprite_D16_S4444_Blend
#define SkSPRITE_ARGS                       , uint8_t alpha
#define SkSPRITE_FIELDS                     uint8_t  fSrcAlpha;
#define SkSPRITE_INIT                       fSrcAlpha = alpha;
#define SkSPRITE_DST_TYPE                   uint16_t
#define SkSPRITE_SRC_TYPE                   uint16_t
#define SkSPRITE_DST_GETADDR                getAddr16
#define SkSPRITE_SRC_GETADDR                getAddr16
#define SkSPRITE_PREAMBLE(srcBM, x, y)      int scale = SkAlpha15To16(fSrcAlpha);
#define SkSPRITE_BLIT_PIXEL(dst, src)       D16_S4444_Blend(dst, src, scale)
#define SkSPRITE_NEXT_ROW
#define SkSPRITE_POSTAMBLE(srcBM)
#include "SkSpriteBlitterTemplate.h"

///////////////////////////////////////////////////////////////////////////////

#define SkSPRITE_CLASSNAME                  Sprite_D16_SIndex8A_Opaque
#define SkSPRITE_ARGS
#define SkSPRITE_FIELDS
#define SkSPRITE_INIT
#define SkSPRITE_DST_TYPE                   uint16_t
#define SkSPRITE_SRC_TYPE                   uint8_t
#define SkSPRITE_DST_GETADDR                getAddr16
#define SkSPRITE_SRC_GETADDR                getAddr8
#define SkSPRITE_PREAMBLE(srcBM, x, y)      const SkPMColor* ctable = srcBM.getColorTable()->lockColors()
#define SkSPRITE_BLIT_PIXEL(dst, src)       D16_S32A_Opaque_Pixel(dst, ctable[src])
#define SkSPRITE_NEXT_ROW
#define SkSPRITE_POSTAMBLE(srcBM)           srcBM.getColorTable()->unlockColors(false)
#include "SkSpriteBlitterTemplate.h"

#define SkSPRITE_CLASSNAME                  Sprite_D16_SIndex8A_Blend
#define SkSPRITE_ARGS                       , uint8_t alpha
#define SkSPRITE_FIELDS                     uint8_t fSrcAlpha;
#define SkSPRITE_INIT                       fSrcAlpha = alpha;
#define SkSPRITE_DST_TYPE                   uint16_t
#define SkSPRITE_SRC_TYPE                   uint8_t
#define SkSPRITE_DST_GETADDR                getAddr16
#define SkSPRITE_SRC_GETADDR                getAddr8
#define SkSPRITE_PREAMBLE(srcBM, x, y)      const SkPMColor* ctable = srcBM.getColorTable()->lockColors(); unsigned src_scale = SkAlpha255To256(fSrcAlpha);
#define SkSPRITE_BLIT_PIXEL(dst, src)       D16_S32A_Blend_Pixel(dst, ctable[src], src_scale)
#define SkSPRITE_NEXT_ROW
#define SkSPRITE_POSTAMBLE(srcBM)           srcBM.getColorTable()->unlockColors(false);
#include "SkSpriteBlitterTemplate.h"

///////////////////////////////////////////////////////////////////////////////

static intptr_t asint(const void* ptr) {
    return reinterpret_cast<const char*>(ptr) - (const char*)0;
}

static void blitrow_d16_si8(SK_RESTRICT uint16_t* dst,
                            SK_RESTRICT const uint8_t* src, int count,
                            SK_RESTRICT const uint16_t* ctable) {
    if (count <= 8) {
        do {
            *dst++ = ctable[*src++];
        } while (--count);
        return;
    }

    // eat src until we're on a 4byte boundary
    while (asint(src) & 3) {
        *dst++ = ctable[*src++];
        count -= 1;
    }

    int qcount = count >> 2;
    SkASSERT(qcount > 0);
    const uint32_t* qsrc = reinterpret_cast<const uint32_t*>(src);
    if (asint(dst) & 2) {
        do {
            uint32_t s4 = *qsrc++;
#ifdef SK_CPU_LENDIAN
            *dst++ = ctable[s4 & 0xFF];
            *dst++ = ctable[(s4 >> 8) & 0xFF];
            *dst++ = ctable[(s4 >> 16) & 0xFF];
            *dst++ = ctable[s4 >> 24];
#else   // BENDIAN
            *dst++ = ctable[s4 >> 24];
            *dst++ = ctable[(s4 >> 16) & 0xFF];
            *dst++ = ctable[(s4 >> 8) & 0xFF];
            *dst++ = ctable[s4 & 0xFF];
#endif
        } while (--qcount);
    } else {    // dst is on a 4byte boundary
        uint32_t* ddst = reinterpret_cast<uint32_t*>(dst);
        do {
            uint32_t s4 = *qsrc++;
#ifdef SK_CPU_LENDIAN
            *ddst++ = (ctable[(s4 >> 8) & 0xFF] << 16) | ctable[s4 & 0xFF];
            *ddst++ = (ctable[s4 >> 24] << 16) | ctable[(s4 >> 16) & 0xFF];
#else   // BENDIAN
            *ddst++ = (ctable[s4 >> 24] << 16) | ctable[(s4 >> 16) & 0xFF];
            *ddst++ = (ctable[(s4 >> 8) & 0xFF] << 16) | ctable[s4 & 0xFF];
#endif
        } while (--qcount);
        dst = reinterpret_cast<uint16_t*>(ddst);
    }
    src = reinterpret_cast<const uint8_t*>(qsrc);
    count &= 3;
    // catch any remaining (will be < 4)
    while (--count >= 0) {
        *dst++ = ctable[*src++];
    }
}

#define SkSPRITE_ROW_PROC(d, s, n, x, y)    blitrow_d16_si8(d, s, n, ctable)

#define SkSPRITE_CLASSNAME                  Sprite_D16_SIndex8_Opaque
#define SkSPRITE_ARGS
#define SkSPRITE_FIELDS
#define SkSPRITE_INIT
#define SkSPRITE_DST_TYPE                   uint16_t
#define SkSPRITE_SRC_TYPE                   uint8_t
#define SkSPRITE_DST_GETADDR                getAddr16
#define SkSPRITE_SRC_GETADDR                getAddr8
#define SkSPRITE_PREAMBLE(srcBM, x, y)      const uint16_t* ctable = srcBM.getColorTable()->lock16BitCache()
#define SkSPRITE_BLIT_PIXEL(dst, src)       *dst = ctable[src]
#define SkSPRITE_NEXT_ROW
#define SkSPRITE_POSTAMBLE(srcBM)           srcBM.getColorTable()->unlock16BitCache()
#include "SkSpriteBlitterTemplate.h"

#define SkSPRITE_CLASSNAME                  Sprite_D16_SIndex8_Blend
#define SkSPRITE_ARGS                       , uint8_t alpha
#define SkSPRITE_FIELDS                     uint8_t fSrcAlpha;
#define SkSPRITE_INIT                       fSrcAlpha = alpha;
#define SkSPRITE_DST_TYPE                   uint16_t
#define SkSPRITE_SRC_TYPE                   uint8_t
#define SkSPRITE_DST_GETADDR                getAddr16
#define SkSPRITE_SRC_GETADDR                getAddr8
#define SkSPRITE_PREAMBLE(srcBM, x, y)      const uint16_t* ctable = srcBM.getColorTable()->lock16BitCache(); unsigned src_scale = SkAlpha255To256(fSrcAlpha);
#define SkSPRITE_BLIT_PIXEL(dst, src)       D16_S16_Blend_Pixel(dst, ctable[src], src_scale)
#define SkSPRITE_NEXT_ROW
#define SkSPRITE_POSTAMBLE(srcBM)           srcBM.getColorTable()->unlock16BitCache();
#include "SkSpriteBlitterTemplate.h"

///////////////////////////////////////////////////////////////////////////////

class Sprite_D16_S32_BlitRowProc : public SkSpriteBlitter {
public:
    Sprite_D16_S32_BlitRowProc(const SkBitmap& source)
        : SkSpriteBlitter(source) {}
    
    // overrides
    
    virtual void setup(const SkBitmap& device, int left, int top,
                       const SkPaint& paint) {
        this->INHERITED::setup(device, left, top, paint);
        
        unsigned flags = 0;
        
        if (paint.getAlpha() < 0xFF) {
            flags |= SkBlitRow::kGlobalAlpha_Flag;
        }
        if (!fSource->isOpaque()) {
            flags |= SkBlitRow::kSrcPixelAlpha_Flag;
        }
        if (paint.isDither()) {
            flags |= SkBlitRow::kDither_Flag;
        }
        fProc = SkBlitRow::Factory(flags, SkBitmap::kRGB_565_Config);
    }
    
    virtual void blitRect(int x, int y, int width, int height) {
        SK_RESTRICT uint16_t* dst = fDevice->getAddr16(x, y);
        const SK_RESTRICT SkPMColor* src = fSource->getAddr32(x - fLeft,
                                                              y - fTop);
        unsigned dstRB = fDevice->rowBytes();
        unsigned srcRB = fSource->rowBytes();
        SkBlitRow::Proc proc = fProc;
        U8CPU alpha = fPaint->getAlpha();
        
        while (--height >= 0) {
            proc(dst, src, width, alpha, x, y);
            y += 1;
            dst = (SK_RESTRICT uint16_t*)((char*)dst + dstRB);
            src = (const SK_RESTRICT SkPMColor*)((const char*)src + srcRB);
        }
    }
    
private:
    SkBlitRow::Proc fProc;
    
    typedef SkSpriteBlitter INHERITED;
};

///////////////////////////////////////////////////////////////////////////////
#if defined(_MRVL_OPT) && defined(_MRVL_OPT_RGB565)
class Sprite_D16_S32_MRVL : public Sprite_D16_S32_BlitRowProc {
public:
    Sprite_D16_S32_MRVL(const SkBitmap& src)  : INHERITED(src) {
        fBlitRectProc = NULL;
    };
    virtual void setup(const SkBitmap& device, int left, int top,
                       const SkPaint& paint);
    virtual void blitRect(int x, int y, int width, int height);
    typedef void (*BlitRectProc)(void*);
private:
    BlitRectProc fBlitRectProc;
    int32_t fPrivateData[18];
    int32_t *fPrivateDataP;
    typedef Sprite_D16_S32_BlitRowProc INHERITED;
#ifdef MRVL_SKIA_OPT_GCU
    GCU_BLEND_MODE fBlendMode;
    GCUuint        fSrcGlobalAlpha;
    bool           fCanGoGCU;
    int blitRectGCU(int x, int y, int width, int height);
#endif /* MRVL_SKIA_OPT_GCU */
};
static Sprite_D16_S32_MRVL::BlitRectProc optProcs16[] = {
    NULL, //_BlendSrc32ToDst565_Src_FastImage_NoGlobalAlpha_MRVL,                 /* S32 opaque, corresponding BlitRow function is faster, comment out */
    NULL,                                                                         /* S32 blend  */
    NULL,//_BlendSrc32ToDst565_SrcOverDst_FastImage_NoGlobalAlpha_MRVL,           /* S32A opaque, corresponding BlitRow function is faster, comment out */
    _BlendSrc32ToDst565_SrcOverDst_FastImage_MRVL,                                /* S32A blend */
    NULL,
    NULL,
    NULL,
    NULL
};
void Sprite_D16_S32_MRVL::setup(const SkBitmap& device, int left, int top,
                       const SkPaint& paint) {
    this->INHERITED::setup(device, left, top, paint);
    unsigned flags = 0;
#ifdef MRVL_SKIA_OPT_GCU
    fBlendMode      = GCU_BLEND_SRC;
    fSrcGlobalAlpha = 0xFF;
#endif /* MRVL_SKIA_OPT_GCU */
    if (paint.getAlpha() < 0xFF) {
        flags |= SkBlitRow::kGlobalAlpha_Flag;
#ifdef MRVL_SKIA_OPT_GCU
        fBlendMode      = GCU_BLEND_SRC_OVER;
        fSrcGlobalAlpha = paint.getAlpha();
#endif /* MRVL_SKIA_OPT_GCU */
    }
    if (!fSource->isOpaque()) {
      flags |= SkBlitRow::kSrcPixelAlpha_Flag;
#ifdef MRVL_SKIA_OPT_GCU
      fBlendMode = GCU_BLEND_SRC_OVER;
#endif /* MRVL_SKIA_OPT_GCU */
    }
    if (paint.isDither()) {
        flags |= SkBlitRow::kDither_Flag;
#ifdef MRVL_SKIA_OPT_GCU
#if !GCU_IGNORE_DITHER_FLAG
        fCanGoGCU = false;
#endif
#endif /* MRVL_SKIA_OPT_GCU */
    }
#ifdef MRVL_SKIA_OPT_GCU
#if !GCU_IGNORE_DITHER_FLAG
    else
#endif
        fCanGoGCU = SkGCUBitmapSurface_MRVL::initGCU();
#endif /* MRVL_SKIA_OPT_GCU */
    fBlitRectProc = optProcs16[flags];
    if(fBlitRectProc) {
        int startIndex = ((uint32_t)(&fPrivateData[0]) & 7) ? 1 : 0;
        fPrivateDataP = &fPrivateData[startIndex];
        fPrivateDataP[16] = (int32_t)paint.getAlpha();
    }
}
void Sprite_D16_S32_MRVL::blitRect(int x, int y, int width, int height) {
    SkASSERT(width > 0 && height > 0);
    int leftHeight = height;
#ifdef MRVL_SKIA_OPT_GCU
    if(fCanGoGCU) {
        leftHeight = blitRectGCU(x, y, width, height);
    }
#endif /* MRVL_SKIA_OPT_GCU */
    if(leftHeight) {
        if(fBlitRectProc) {
            fPrivateDataP[0] = width;
            fPrivateDataP[1] = leftHeight;
            fPrivateDataP[6] = (int32_t)fSource->getAddr32(x - fLeft, y - fTop + (height - leftHeight));
            fPrivateDataP[9]  = (int32_t)fSource->rowBytes();
            fPrivateDataP[14] = (int32_t)fDevice->getAddr16(x, y + (height - leftHeight));
            fPrivateDataP[15] = (int32_t)fDevice->rowBytes();
            fBlitRectProc((void*)fPrivateDataP);
        } else {
            this->INHERITED::blitRect(x, y + (height - leftHeight), width, leftHeight);
        }
    }
}

#ifdef MRVL_SKIA_OPT_GCU
int Sprite_D16_S32_MRVL::blitRectGCU(int x, int y, int width, int height)
{
    if((width >= WIDTH_LOW_BOUND_BLEND) && (height >= HEIGHT_LOW_BOUND_BLEND)) {
        int dstMaxY = y + height;
        int srcMaxY = dstMaxY - fTop;

        SkGCUBitmapSurface_MRVL* srcBitmapSurface = SkGCUBitmapSurface_MRVL::createSurfaceFromBitmap(fSource, true, true, srcMaxY);
        if(!srcBitmapSurface) {
            return height;
        }
        SkGCUBitmapSurface_MRVL* dstBitmapSurface = SkGCUBitmapSurface_MRVL::createSurfaceFromBitmap(fDevice, false, true, dstMaxY);
        if(!dstBitmapSurface) {
            srcBitmapSurface->unref();
            return height;
        }
        GCU_DEBUG_PRINT("============= blitRectGCU ARGB->RGB565 (%d, %d, %d, %d)=================\n", x, y, width, height);

        GCU_RECT srcRect, dstRect;
        int srcTruncatedHeight = getTruncatedHeight(srcMaxY, srcBitmapSurface->getHeight(), fSource);
        int dstTruncatedHeight = getTruncatedHeight(dstMaxY, dstBitmapSurface->getHeight(), fDevice);
        int heightT = height - SkMax32(srcTruncatedHeight, dstTruncatedHeight);
        setGCURect(x - fLeft, y - fTop, width, heightT, fSource, &srcRect);
        setGCURect(x, y, width, heightT, fDevice, &dstRect);

        GCU_BLEND_DATA blendData;
        memset(&blendData, 0, sizeof(GCU_BLEND_DATA));
        blendData.pSrcRect         = &srcRect;
        blendData.pDstRect         = &dstRect;
        blendData.rotation         = GCU_ROTATION_0;
        blendData.blendMode        = fBlendMode;
        blendData.srcGlobalAlpha   = fSrcGlobalAlpha;
        blendData.dstGlobalAlpha   = 0xFF;
        blendData.pSrcSurface      = srcBitmapSurface->getSurface();
        blendData.pDstSurface      = dstBitmapSurface->getSurface();
#if GCU_DUMP_SURFACES
        static int countsrc = 0;
        srcBitmapSurface->dump("/data/dump/surface_blend32on16_src", countsrc++);
#endif /* GCU_DUMP_SURFACES */
        GCU_DEBUG_PRINT("==== gcuBlend %p (Bitmap %p config %d %d x %d)(%d, %d - %d, %d) -- %p (Bitmap %p config %d %d x %d)(%d, %d - %d, %d) mode = %d alpha=0x%2x \n",
                         blendData.pSrcSurface, fSource, fSource->config(), fSource->width(), fSource->height(),
                         srcRect.left, srcRect.top, srcRect.right, srcRect.bottom,
                         blendData.pDstSurface, fDevice, fDevice->config(), fDevice->width(), fDevice->height(),
                         dstRect.left, dstRect.top, dstRect.right, dstRect.bottom,
                         blendData.blendMode,
                         blendData.srcGlobalAlpha);

        if(!srcBitmapSurface->blendAndFlush(fSource, fDevice, &blendData, 0)) {
            srcBitmapSurface->unref();
            dstBitmapSurface->unref();
            return height;
        } else {
#if GCU_DUMP_SURFACES
            static int countdst = 0;
            dstBitmapSurface->dump("/data/dump/surface_blend32on16_dst", countdst++);
#endif
            srcBitmapSurface->unref();
            dstBitmapSurface->unref();
            return height - heightT;
        }
    }
    return height;
}
#endif /* MRVL_SKIA_OPT_GCU */

#endif  /* #if defined(_MRVL_OPT) && defined(_MRVL_OPT_RGB565) */

#include "SkTemplatesPriv.h"

SkSpriteBlitter* SkSpriteBlitter::ChooseD16(const SkBitmap& source,
                                            const SkPaint& paint,
                                            void* storage, size_t storageSize) {
    if (paint.getMaskFilter() != NULL) { // may add cases for this
        return NULL;
    }
    if (paint.getXfermode() != NULL) { // may add cases for this
        return NULL;
    }
    if (paint.getColorFilter() != NULL) { // may add cases for this
        return NULL;
    }

    SkSpriteBlitter* blitter = NULL;
    unsigned alpha = paint.getAlpha();

    switch (source.getConfig()) {
        case SkBitmap::kARGB_8888_Config:
#if defined(_MRVL_OPT) && defined(_MRVL_OPT_RGB565)
            SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D16_S32_MRVL,
                                  storage, storageSize, (source));
#else
            SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D16_S32_BlitRowProc,
                                  storage, storageSize, (source));
#endif
            break;
        case SkBitmap::kARGB_4444_Config:
            if (255 == alpha) {
                SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D16_S4444_Opaque,
                                      storage, storageSize, (source));
            } else {
                SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D16_S4444_Blend,
                                    storage, storageSize, (source, alpha >> 4));
            }
            break;
        case SkBitmap::kRGB_565_Config:
            if (255 == alpha) {
                SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D16_S16_Opaque,
                                      storage, storageSize, (source));
            } else {
                SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D16_S16_Blend,
                                      storage, storageSize, (source, alpha));
            }
            break;
        case SkBitmap::kIndex8_Config:
            if (paint.isDither()) {
                // we don't support dither yet in these special cases
                break;
            }
            if (source.isOpaque()) {
                if (255 == alpha) {
                    SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D16_SIndex8_Opaque,
                                          storage, storageSize, (source));
                } else {
                    SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D16_SIndex8_Blend,
                                         storage, storageSize, (source, alpha));
                }
            } else {
                if (255 == alpha) {
                    SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D16_SIndex8A_Opaque,
                                          storage, storageSize, (source));
                } else {
                    SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D16_SIndex8A_Blend,
                                         storage, storageSize, (source, alpha));
                }
            }
            break;
        default:
            break;
    }
    return blitter;
}

