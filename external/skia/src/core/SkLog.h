/******************************************************************************
 *         Copyright (c) 2010 Marvell Corporation. All Rights Reserved.
 ******************************************************************************/
#ifndef SkLog_DEFINED
#define SkLog_DEFINED
#ifdef _ANDROID_
#include <utils/Log.h>
#endif

//#define ENABLE_LOG
//#define ENABLE_DUMPMEM

#ifdef ENABLE_LOG
 #ifdef _ANDROID_
  #undef LOG_TAG
  #define LOG_TAG "SKIADUMP"
  #define PRINT_SKIA(...)        LOGD(__VA_ARGS__)
 #elif defined(CHROMIUM_BUILD)
  void SkDebugf_FileLine(const char* file, int line, bool fatal,
                       const char* format, ...);
  #define PRINT_SKIA(...)        SkDebugf_FileLine(__FILE__, __LINE__, false, __VA_ARGS__)
 #else
  #define PRINT_SKIA             printf
 #endif
#else
 #define PRINT_SKIA(...)
#endif

//#define DUMP_DRAW
//#define DUMP_BLIT
//#define DUMP_SHADER
//#define DUMP_BLITROW
//#define DUMP_JPEG
//#define DUMP_TEMP

/* draw level dump (in SkCanvas.cpp and SkDraw.cpp) */
#ifdef DUMP_DRAW
 #define PRINT_DRAW        PRINT_SKIA
#else
 #define PRINT_DRAW(...)
#endif

/* blitter level dump (SkBlitter_xxx.cpp SkBlitRow_xxx.cpp) */
#ifdef DUMP_BLIT
 #define PRINT_BLIT        PRINT_SKIA
#else
 #define PRINT_BLIT(...)
#endif

/* BitmapProcShader */
#ifdef DUMP_SHADER
 #define PRINT_SHADER      PRINT_SKIA
#else
 #define PRINT_SHADER(...)
#endif

/* blit row level dump, could be huge */
#ifdef DUMP_BLITROW
 #define PRINT_BLITROW     PRINT_SKIA
#else
 #define PRINT_BLITROW(...)
#endif

/* JPEG decoder related dump */
#ifdef DUMP_JPEG
 #define PRINT_JPEG        PRINT_SKIA
#else
 #define PRINT_JPEG(...)
#endif

#ifdef DUMP_TEMP
 #define PRINT_TEMP  PRINT_SKIA
#else
 #define PRINT_TEMP(...)
#endif

#ifdef ENABLE_DUMPMEM
#include "SkStream.h"
#include "SkColorPriv.h"

/* structures for bmp header */
#pragma pack(2)
typedef struct tagBITMAPFILEHEADER {
        unsigned short          bfType;
        unsigned long           bfSize;
        unsigned short          bfReserved1;
        unsigned short          bfReserved2;
        unsigned long           bfOffBits;
} BITMAPFILEHEADER;
#pragma pack()

typedef struct tagBITMAPINFOHEADER{
        unsigned long           biSize;
        long                    biWidth;
        long                    biHeight;
        unsigned short          biPlanes;
        unsigned short          biBitCount;
        unsigned long           biCompression;
        unsigned long           biSizeImage;
        long                    biXPelsPerMeter;
        long                    biYPelsPerMeter;
        unsigned long           biClrUsed;
        unsigned long           biClrImportant;
} BITMAPINFOHEADER;

#define BI_RGB          0L
#define BI_RLE8         1L
#define BI_RLE4         2L
#define BI_BITFIELDS    3L

#define DUMP_ROOT          "/data"

#define  CEILING(x, align)  ((((x)+((align)-1))&(~((align)-1))))

static void _dump_memory(void *memory, int width, int height, int pitch, SkBitmap::Config format, const char* filename, bool keepAlpha)
{
    SkString str(DUMP_ROOT);
    str.appendf("/%s", filename);

    if(format != SkBitmap::kRGB_565_Config && format != SkBitmap::kARGB_8888_Config) {
        /* unsupported */
        SkDebugf("_dump_memory: Unsupported dump color format.\n");
        return;
    }
    if(!memory || !width || !height || !pitch) {
        SkDebugf("_dump_memory: wrong argument.\n");
        return;
    }

    SkFILEWStream   fout(str.c_str());

    BITMAPFILEHEADER bmpFile;
    BITMAPINFOHEADER bmpInfo;

    unsigned int anMask[3];
    int pitch_align4 = 0;
    if(format != SkBitmap::kRGB_565_Config && !keepAlpha) {
        pitch_align4 = CEILING(width*3, 4);
    } else if (format != SkBitmap::kRGB_565_Config && keepAlpha) {
        pitch_align4 = CEILING(width*4, 4);
    } else {
        pitch_align4 = CEILING(pitch, 4);
    }
    bmpFile.bfType   = 0x4D42;
    bmpFile.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + pitch_align4*(height);
    if(format == SkBitmap::kRGB_565_Config) {
        bmpFile.bfSize += 12;
    }
    bmpFile.bfReserved1 = 0;
    bmpFile.bfReserved2 = 0;
    bmpFile.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
    bmpInfo.biSize   = 40;
    bmpInfo.biWidth  = width;
    bmpInfo.biHeight = height;
    bmpInfo.biPlanes = 1;
    if(format == SkBitmap::kRGB_565_Config) {
        bmpInfo.biBitCount = 16;
    } else if(format == SkBitmap::kARGB_8888_Config && keepAlpha) {
        bmpInfo.biBitCount = 32;
    } else if(format == SkBitmap::kARGB_8888_Config && !keepAlpha) {
        bmpInfo.biBitCount = 24;
    }

    if(bmpInfo.biBitCount < 24) {
        bmpInfo.biCompression = BI_BITFIELDS;
    } else {
        bmpInfo.biCompression = BI_RGB;
    }
    bmpInfo.biSizeImage = pitch_align4 * height;
    bmpInfo.biXPelsPerMeter = bmpInfo.biYPelsPerMeter = 72;
    bmpInfo.biClrUsed = 0;
    bmpInfo.biClrImportant = 0;

    if(format == SkBitmap::kRGB_565_Config) {
        bmpFile.bfOffBits += 12;
        anMask[2]=0x001F;
        anMask[1]=0x07E0;
        anMask[0]=0xF800;
    }

    fout.write(&bmpFile, sizeof(BITMAPFILEHEADER));
    fout.write(&bmpInfo, sizeof(BITMAPINFOHEADER));

    if(format == SkBitmap::kRGB_565_Config) {
        fout.write(anMask, 4*3);
    }

    uint32_t *lineBuf = (uint32_t*)sk_malloc_throw(pitch_align4);
    memset(lineBuf, 0, pitch_align4);
    uint8_t  *pixel   = (uint8_t*)memory+pitch * (height-1);
    switch(format) {
        case SkBitmap::kARGB_8888_Config:
            if(keepAlpha) {
                /* Need repack to ARGB, BMP spec is fixed ARGB */
                for(int i=0;i<height;i++) {
                    uint32_t *pixel32 = (uint32_t*)pixel;
                    for(int j=0;j<width; j++) {
                        lineBuf[j] = (SkGetPackedA32(pixel32[j]) << 24) |
                            (SkGetPackedR32(pixel32[j]) << 16) |
                            (SkGetPackedG32(pixel32[j]) << 8) |
                            (SkGetPackedB32(pixel32[j]) << 0);
                    }
                    fout.write(lineBuf, pitch_align4);
                    pixel -= pitch;
                }
            } else {
                /* repack to RGB */
                for(int i=0;i<height;i++) {
                    uint32_t *pixel32 = (uint32_t*)pixel;
                    uint8_t  *dstPixel= (uint8_t*)lineBuf;
                    for(int j=0;j<width; j++) {
                        *dstPixel++ = (uint8_t)SkGetPackedB32(pixel32[j]);
                        *dstPixel++ = (uint8_t)SkGetPackedG32(pixel32[j]);
                        *dstPixel++ = (uint8_t)SkGetPackedR32(pixel32[j]);
                    }
                    fout.write(lineBuf, pitch_align4);
                    pixel -= pitch;
                }
            }
            break;
        case SkBitmap::kRGB_565_Config:
            for(int i=0;i<height;i++) {
                for(int j=0;j<width; j++) {
                    memcpy(lineBuf, pixel, pitch);
                }
                fout.write(lineBuf, pitch_align4);
                pixel -= pitch;
            }
            break;
        default:
            break;
    }

/*
    SkDebugf("dump bmpFile and bmpInfo.\n");
    SkDebugf("=== bfType = 0x%x\n", bmpFile.bfType);
    SkDebugf("=== bfOffBits = %d\n", bmpFile.bfOffBits);
    SkDebugf("=== bmpInfo.biSize = %d\n", bmpInfo.biSize);
    SkDebugf("=== bmpInfo.biWidth = %d\n", bmpInfo.biWidth);
    SkDebugf("=== bmpInfo.biHeight= %d\n", bmpInfo.biHeight);
    SkDebugf("=== bmpInfo.biBitCount = %d\n", bmpInfo.biBitCount);
    SkDebugf("=== bmpInfo.biSizeImage = %d\n", bmpInfo.biSizeImage);
*/
    sk_free(lineBuf);

}
#endif /* ENABLE_DUMPMEM */
#endif /* SkLog_DEFINED */


/* EOF */

