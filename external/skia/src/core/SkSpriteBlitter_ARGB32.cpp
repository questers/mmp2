/* libs/graphics/sgl/SkSpriteBlitter_ARGB32.cpp
**
** Copyright 2006, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License"); 
** you may not use this file except in compliance with the License. 
** You may obtain a copy of the License at 
**
**     http://www.apache.org/licenses/LICENSE-2.0 
**
** Unless required by applicable law or agreed to in writing, software 
** distributed under the License is distributed on an "AS IS" BASIS, 
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
** See the License for the specific language governing permissions and 
** limitations under the License.
*/
/******************************************************************************
 *      Copyright (c) 2011 Marvell Corporation. All Rights Reserved.
 ******************************************************************************/

#include "SkSpriteBlitter.h"
#include "SkBlitRow.h"
#include "SkColorFilter.h"
#include "SkColorPriv.h"
#include "SkTemplates.h"
#include "SkUtils.h"
#include "SkXfermode.h"

///////////////////////////////////////////////////////////////////////////////

class Sprite_D32_S32 : public SkSpriteBlitter {
public:
    Sprite_D32_S32(const SkBitmap& src, U8CPU alpha)  : INHERITED(src) {
        SkASSERT(src.config() == SkBitmap::kARGB_8888_Config);

        unsigned flags32 = 0;
        if (255 != alpha) {
            flags32 |= SkBlitRow::kGlobalAlpha_Flag32;
        }
        if (!src.isOpaque()) {
            flags32 |= SkBlitRow::kSrcPixelAlpha_Flag32;
        }

        fProc32 = SkBlitRow::Factory32(flags32);
        fAlpha = alpha;
    }
    
    virtual void blitRect(int x, int y, int width, int height) {
        SkASSERT(width > 0 && height > 0);
        SK_RESTRICT uint32_t* dst = fDevice->getAddr32(x, y);
        const SK_RESTRICT uint32_t* src = fSource->getAddr32(x - fLeft,
                                                             y - fTop);
        size_t dstRB = fDevice->rowBytes();
        size_t srcRB = fSource->rowBytes();
        SkBlitRow::Proc32 proc = fProc32;
        U8CPU             alpha = fAlpha;

        do {
            proc(dst, src, width, alpha);
            dst = (SK_RESTRICT uint32_t*)((char*)dst + dstRB);
            src = (const SK_RESTRICT uint32_t*)((const char*)src + srcRB);
        } while (--height != 0);
    }

private:
    SkBlitRow::Proc32   fProc32;
    U8CPU               fAlpha;
    
    typedef SkSpriteBlitter INHERITED;
};

///////////////////////////////////////////////////////////////////////////////

#ifdef _MRVL_OPT
#include "mrvl_opt.h"
static void _copy_block(void *pdata)
{
    int *pData = (int*)pdata;
    int w = ((int)pData[0])<<2;
    int h = (int)pData[1];
    unsigned char* src = (unsigned char*)pData[6];
    int sstride = (int)pData[9];
    unsigned char* dst = (unsigned char*)pData[14];
    int dstride = (int)pData[15];
    for(int i=0;i<h;i++) {
        s32_d32_opaque_MRVL(dst, src, w); /* s32_d32_opaque_MRVL so far defined as memcpy_MRVL */
        src+=sstride;
        dst+=dstride;
    }
}

#ifdef MRVL_SKIA_OPT_GCU
#include "gcu_mrvl.h"
#endif /* MRVL_SKIA_OPT_GCU */

class Sprite_D32_S16_Opaque_MRVL : public SkSpriteBlitter {
public:
    Sprite_D32_S16_Opaque_MRVL(const SkBitmap& src)  : INHERITED(src) {
        int startIndex = ((uint32_t)(&fPrivateData[0]) & 7) ? 1 : 0;
        fPrivateDataP = &fPrivateData[startIndex];
#ifdef MRVL_SKIA_OPT_GCU
        fCanGoGCU = SkGCUBitmapSurface_MRVL::initGCU();   /* TODO: add check bmm */
#endif /* MRVL_SKIA_OPT_GCU */
    };
    virtual void blitRect(int x, int y, int width, int height) {
        SkASSERT(width > 0 && height > 0);
        int leftHeight = height;
#ifdef MRVL_SKIA_OPT_GCU
        if(fCanGoGCU) {
            leftHeight = blitRectGCU(x, y, width, height);
        }
#endif /* MRVL_SKIA_OPT_GCU */
        if(leftHeight) {
            //GCU_DEBUG_PRINT("============= blitRectWMMX (%d, %d, %d, %d)=================\n", x, y, width, leftHeight);
            fPrivateDataP[0]  = width;
            fPrivateDataP[1]  = leftHeight;
            fPrivateDataP[6]  = (int32_t)fSource->getAddr(x - fLeft, y - fTop + (height - leftHeight));
            fPrivateDataP[9]  = (int32_t)fSource->rowBytes();
            fPrivateDataP[14] = (int32_t)fDevice->getAddr(x, y + height - leftHeight);
            fPrivateDataP[15] = (int32_t)fDevice->rowBytes();
            _BlendSrc565ToDst32_Src_FastImage_NoGlobalAlpha_MRVL((void*)fPrivateDataP);
        }
    };
private:
#ifdef MRVL_SKIA_OPT_GCU
    bool fCanGoGCU;
#endif /* MRVL_SKIA_OPT_GCU */
    int32_t fPrivateData[18];
    int32_t *fPrivateDataP;
    typedef SkSpriteBlitter INHERITED;
#ifdef MRVL_SKIA_OPT_GCU
    int blitRectGCU(int x, int y, int width, int height)
    {
        if((width >= WIDTH_LOW_BOUND_BLEND) && (height >= HEIGHT_LOW_BOUND_BLEND)) {
            int dstMaxY = y + height;
            int srcMaxY = dstMaxY - fTop;

            SkGCUBitmapSurface_MRVL* srcBitmapSurface = SkGCUBitmapSurface_MRVL::createSurfaceFromBitmap(fSource, true, true, srcMaxY);
            if(!srcBitmapSurface) {
                return height;
            }
            SkGCUBitmapSurface_MRVL* dstBitmapSurface = SkGCUBitmapSurface_MRVL::createSurfaceFromBitmap(fDevice, false, true, dstMaxY);
            if(!dstBitmapSurface) {
                srcBitmapSurface->unref();
                return height;
            }
            GCU_DEBUG_PRINT("============= blitRectGCU RGB565 (%d, %d, %d, %d)=================\n", x, y, width, height);

            GCU_RECT srcRect, dstRect;
            int srcTruncatedHeight = getTruncatedHeight(srcMaxY, srcBitmapSurface->getHeight(), fSource);
            int dstTruncatedHeight = getTruncatedHeight(dstMaxY, dstBitmapSurface->getHeight(), fDevice);
            int heightT = height - SkMax32(srcTruncatedHeight, dstTruncatedHeight);
            setGCURect(x - fLeft, y - fTop, width, heightT, fSource, &srcRect);
            setGCURect(x, y, width, heightT, fDevice, &dstRect);

            GCU_BLEND_DATA blendData;
            memset(&blendData, 0, sizeof(GCU_BLEND_DATA));
            blendData.pSrcRect         = &srcRect;
            blendData.pDstRect         = &dstRect;
            blendData.rotation         = GCU_ROTATION_0;
            blendData.srcGlobalAlpha   = 0xFF;
            blendData.dstGlobalAlpha   = 0xFF;
            blendData.blendMode        = GCU_BLEND_SRC;
            blendData.pSrcSurface      = srcBitmapSurface->getSurface();
            blendData.pDstSurface      = dstBitmapSurface->getSurface();

            GCU_DEBUG_PRINT("==== gcuBlend %p (Bitmap %p %d x %d)(%d, %d - %d, %d) -- %p (Bitmap %p %d x %d)(%d, %d - %d, %d) mode = %d alpha=0x%2x \n",
                            blendData.pSrcSurface, fSource, fSource->width(), fSource->height(),
                            srcRect.left, srcRect.top, srcRect.right, srcRect.bottom,
                            blendData.pDstSurface, fDevice, fDevice->width(), fDevice->height(),
                            dstRect.left, dstRect.top, dstRect.right, dstRect.bottom,
                            blendData.blendMode,
                            blendData.srcGlobalAlpha);
            if(!srcBitmapSurface->blendAndFlush(fSource, fDevice, &blendData, 0)) {
                srcBitmapSurface->unref();
                dstBitmapSurface->unref();
                return height;
            } else {
                srcBitmapSurface->unref();
                dstBitmapSurface->unref();
                return height - heightT;
            }
        }
        return height;
    }
#endif /* MRVL_SKIA_OPT_GCU */
};

class Sprite_D32_S32_MRVL : public Sprite_D32_S32 {
public:
    Sprite_D32_S32_MRVL(const SkBitmap& src, U8CPU alpha)  : INHERITED(src, alpha) {
        SkASSERT(src.config() == SkBitmap::kARGB_8888_Config);
        fBlitRectProc = NULL;
        int startIndex = ((uint32_t)(&fPrivateData[0]) & 7) ? 1 : 0;
        fPrivateDataP = &fPrivateData[startIndex];
        if(alpha == 0xFF) {
            if(src.isOpaque()) {
#ifdef MRVL_SKIA_OPT_GCU
                fBlendMode            = GCU_BLEND_SRC;
                fSrcGlobalAlpha       = 0xFF;
#endif /* MRVL_SKIA_OPT_GCU */
                fBlitRectProc = _copy_block;
            } else {
#ifdef MRVL_SKIA_OPT_GCU
                fBlendMode            = GCU_BLEND_SRC_OVER;
                fSrcGlobalAlpha       = 0xFF;
#endif /* MRVL_SKIA_OPT_GCU */
                fBlitRectProc = _BlendSrc32ToDst32_SrcOverDst_FastImage_NoGlobalAlpha_MRVL;
            }
        } else {
#ifdef MRVL_SKIA_OPT_GCU
                fBlendMode            = GCU_BLEND_SRC_OVER;
                fSrcGlobalAlpha       = alpha;
#endif /* MRVL_SKIA_OPT_GCU */
            fBlitRectProc = _BlendSrc32ToDst32_SrcOverDst_FastImage_MRVL;
        }
        fPrivateDataP[16] = (int32_t)alpha;
#ifdef MRVL_SKIA_OPT_GCU
        fCanGoGCU = SkGCUBitmapSurface_MRVL::initGCU();   /* TODO: add check bmm */
#endif /* MRVL_SKIA_OPT_GCU */
    };
    virtual void blitRect(int x, int y, int width, int height) {
        SkASSERT(width > 0 && height > 0);
        int leftHeight = height;
#ifdef MRVL_SKIA_OPT_GCU
        if(fCanGoGCU) {
            leftHeight = blitRectGCU(x, y, width, height);
        }
#endif /* MRVL_SKIA_OPT_GCU */
        if(leftHeight) {
            //GCU_DEBUG_PRINT("============= blitRectWMMX (%d, %d, %d, %d)=================\n", x, y, width, leftHeight);
            if(fBlitRectProc) {
                fPrivateDataP[0] = width;
                fPrivateDataP[1]  = leftHeight;
                fPrivateDataP[6]  = (int32_t)fSource->getAddr32(x - fLeft, y - fTop + (height - leftHeight));
                fPrivateDataP[9]  = (int32_t)fSource->rowBytes();
                fPrivateDataP[14] = (int32_t)fDevice->getAddr32(x, y + height - leftHeight);
                fPrivateDataP[15] = (int32_t)fDevice->rowBytes();
                fBlitRectProc((void*)fPrivateDataP);
            } else {
                this->INHERITED::blitRect(x, y + height - leftHeight, width, leftHeight);
            }
        }
    };
private:
    typedef void (*BlitRectProc)(void*);
    BlitRectProc fBlitRectProc;
#ifdef MRVL_SKIA_OPT_GCU
    GCU_BLEND_MODE fBlendMode;
    GCUuint        fSrcGlobalAlpha;
    bool           fCanGoGCU;
#endif /* MRVL_SKIA_OPT_GCU */
    int32_t fPrivateData[18];
    int32_t *fPrivateDataP;
    typedef Sprite_D32_S32 INHERITED;
#ifdef MRVL_SKIA_OPT_GCU
    int blitRectGCU(int x, int y, int width, int height)
    {
        if((width >= WIDTH_LOW_BOUND_BLEND) && (height >= HEIGHT_LOW_BOUND_BLEND)) {
            int dstMaxY = y + height;
            int srcMaxY = dstMaxY - fTop;

            SkGCUBitmapSurface_MRVL* srcBitmapSurface = SkGCUBitmapSurface_MRVL::createSurfaceFromBitmap(fSource, true, true, srcMaxY);
            if(!srcBitmapSurface) {
                return height;
            }
            SkGCUBitmapSurface_MRVL* dstBitmapSurface = SkGCUBitmapSurface_MRVL::createSurfaceFromBitmap(fDevice, false, true, dstMaxY);
            if(!dstBitmapSurface) {
                srcBitmapSurface->unref();
                return height;
            }
            GCU_DEBUG_PRINT("============= blitRectGCU ARGB (%d, %d, %d, %d)=================\n", x, y, width, height);

            GCU_RECT srcRect, dstRect;
            int srcTruncatedHeight = getTruncatedHeight(srcMaxY, srcBitmapSurface->getHeight(), fSource);
            int dstTruncatedHeight = getTruncatedHeight(dstMaxY, dstBitmapSurface->getHeight(), fDevice);
            int heightT = height - SkMax32(srcTruncatedHeight, dstTruncatedHeight);
            setGCURect(x - fLeft, y - fTop, width, heightT, fSource, &srcRect);
            setGCURect(x, y, width, heightT, fDevice, &dstRect);

            GCU_BLEND_DATA blendData;
            memset(&blendData, 0, sizeof(GCU_BLEND_DATA));
            blendData.pSrcRect         = &srcRect;
            blendData.pDstRect         = &dstRect;
            blendData.rotation         = GCU_ROTATION_0;
            blendData.blendMode        = fBlendMode;
            blendData.srcGlobalAlpha   = fSrcGlobalAlpha;
            blendData.dstGlobalAlpha   = 0xFF;
            blendData.pSrcSurface      = srcBitmapSurface->getSurface();
            blendData.pDstSurface      = dstBitmapSurface->getSurface();
#if GCU_DUMP_SURFACES
            static int countsrc = 0;
            srcBitmapSurface->dump("/data/dump/surface_blend32on32_src", countsrc++);
#endif /* GCU_DUMP_SURFACES */
            GCU_DEBUG_PRINT("==== gcuBlend %p (Bitmap %p %d x %d)(%d, %d - %d, %d) -- %p (Bitmap %p %d x %d)(%d, %d - %d, %d) mode = %d alpha=0x%2x \n",
                            blendData.pSrcSurface, fSource, fSource->width(), fSource->height(),
                            srcRect.left, srcRect.top, srcRect.right, srcRect.bottom,
                            blendData.pDstSurface, fDevice, fDevice->width(), fDevice->height(),
                            dstRect.left, dstRect.top, dstRect.right, dstRect.bottom,
                            blendData.blendMode,
                            blendData.srcGlobalAlpha);

            if(!srcBitmapSurface->blendAndFlush(fSource, fDevice, &blendData, 0)) {
                srcBitmapSurface->unref();
                dstBitmapSurface->unref();
                return height;
            } else {
#if GCU_DUMP_SURFACES
                static int countdst = 0;
                dstBitmapSurface->dump("/data/dump/surface_blend32on32_dst", countdst++);
#endif
                srcBitmapSurface->unref();
                dstBitmapSurface->unref();
                return height - heightT;
            }
        }
        return height;
    }
#endif /* MRVL_SKIA_OPT_GCU */
};
#endif  /* _MRVL_OPT */
class Sprite_D32_XferFilter : public SkSpriteBlitter {
public:
    Sprite_D32_XferFilter(const SkBitmap& source, const SkPaint& paint)
        : SkSpriteBlitter(source) {
        fColorFilter = paint.getColorFilter();
        fColorFilter->safeRef();
        
        fXfermode = paint.getXfermode();
        fXfermode->safeRef();
        
        fBufferSize = 0;
        fBuffer = NULL;

        unsigned flags32 = 0;
        if (255 != paint.getAlpha()) {
            flags32 |= SkBlitRow::kGlobalAlpha_Flag32;
        }
        if (!source.isOpaque()) {
            flags32 |= SkBlitRow::kSrcPixelAlpha_Flag32;
        }
        
        fProc32 = SkBlitRow::Factory32(flags32);
        fAlpha = paint.getAlpha();
    }
    
    virtual ~Sprite_D32_XferFilter() {
        delete[] fBuffer;
        fXfermode->safeUnref();
        fColorFilter->safeUnref();
    }
    
    virtual void setup(const SkBitmap& device, int left, int top,
                       const SkPaint& paint) {
        this->INHERITED::setup(device, left, top, paint);
        
        int width = device.width();
        if (width > fBufferSize) {
            fBufferSize = width;
            delete[] fBuffer;
            fBuffer = new SkPMColor[width];
        }
    }

protected:
    SkColorFilter*      fColorFilter;
    SkXfermode*         fXfermode;
    int                 fBufferSize;
    SkPMColor*          fBuffer;
    SkBlitRow::Proc32   fProc32;
    U8CPU               fAlpha;

private:
    typedef SkSpriteBlitter INHERITED;
};

///////////////////////////////////////////////////////////////////////////////

class Sprite_D32_S32A_XferFilter : public Sprite_D32_XferFilter {
public:
    Sprite_D32_S32A_XferFilter(const SkBitmap& source, const SkPaint& paint)
        : Sprite_D32_XferFilter(source, paint) {}

    virtual void blitRect(int x, int y, int width, int height) {
        SkASSERT(width > 0 && height > 0);
        SK_RESTRICT uint32_t* dst = fDevice->getAddr32(x, y);
        const SK_RESTRICT uint32_t* src = fSource->getAddr32(x - fLeft,
                                                             y - fTop);
        unsigned dstRB = fDevice->rowBytes();
        unsigned srcRB = fSource->rowBytes();
        SkColorFilter* colorFilter = fColorFilter;
        SkXfermode* xfermode = fXfermode;

        do {
            const SkPMColor* tmp = src;
            
            if (NULL != colorFilter) {
                colorFilter->filterSpan(src, width, fBuffer);
                tmp = fBuffer;
            }
            
            if (NULL != xfermode) {
                xfermode->xfer32(dst, tmp, width, NULL);
            } else {
                fProc32(dst, tmp, width, fAlpha);
            }

            dst = (SK_RESTRICT uint32_t*)((char*)dst + dstRB);
            src = (const SK_RESTRICT uint32_t*)((const char*)src + srcRB);
        } while (--height != 0);
    }
    
private:
    typedef Sprite_D32_XferFilter INHERITED;
};

static void fillbuffer(SK_RESTRICT SkPMColor dst[],
                       const SK_RESTRICT SkPMColor16 src[], int count) {
    SkASSERT(count > 0);
    
    do {
        *dst++ = SkPixel4444ToPixel32(*src++);
    } while (--count != 0);
}

class Sprite_D32_S4444_XferFilter : public Sprite_D32_XferFilter {
public:
    Sprite_D32_S4444_XferFilter(const SkBitmap& source, const SkPaint& paint)
        : Sprite_D32_XferFilter(source, paint) {}
    
    virtual void blitRect(int x, int y, int width, int height) {
        SkASSERT(width > 0 && height > 0);
        SK_RESTRICT SkPMColor* dst = fDevice->getAddr32(x, y);
        const SK_RESTRICT SkPMColor16* src = fSource->getAddr16(x - fLeft,
                                                                y - fTop);
        unsigned dstRB = fDevice->rowBytes();
        unsigned srcRB = fSource->rowBytes();
        SK_RESTRICT SkPMColor* buffer = fBuffer;
        SkColorFilter* colorFilter = fColorFilter;
        SkXfermode* xfermode = fXfermode;

        do {
            fillbuffer(buffer, src, width);
            
            if (NULL != colorFilter) {
                colorFilter->filterSpan(buffer, width, buffer);
            }
            if (NULL != xfermode) {
                xfermode->xfer32(dst, buffer, width, NULL);
            } else {
                fProc32(dst, buffer, width, fAlpha);
            }
            
            dst = (SK_RESTRICT SkPMColor*)((char*)dst + dstRB);
            src = (const SK_RESTRICT SkPMColor16*)((const char*)src + srcRB);
        } while (--height != 0);
    }
    
private:
    typedef Sprite_D32_XferFilter INHERITED;
};

///////////////////////////////////////////////////////////////////////////////

static void src_row(SK_RESTRICT SkPMColor dst[],
                    const SK_RESTRICT SkPMColor16 src[], int count) {
    do {
        *dst = SkPixel4444ToPixel32(*src);
        src += 1;
        dst += 1;
    } while (--count != 0);
}

class Sprite_D32_S4444_Opaque : public SkSpriteBlitter {
public:
    Sprite_D32_S4444_Opaque(const SkBitmap& source) : SkSpriteBlitter(source) {}
    
    virtual void blitRect(int x, int y, int width, int height) {
        SkASSERT(width > 0 && height > 0);
        SK_RESTRICT SkPMColor* dst = fDevice->getAddr32(x, y);
        const SK_RESTRICT SkPMColor16* src = fSource->getAddr16(x - fLeft,
                                                                y - fTop);
        unsigned dstRB = fDevice->rowBytes();
        unsigned srcRB = fSource->rowBytes();
        
        do {
            src_row(dst, src, width);
            dst = (SK_RESTRICT SkPMColor*)((char*)dst + dstRB);
            src = (const SK_RESTRICT SkPMColor16*)((const char*)src + srcRB);
        } while (--height != 0);
    }
};

static void srcover_row(SK_RESTRICT SkPMColor dst[],
                        const SK_RESTRICT SkPMColor16 src[], int count) {
    do {
        *dst = SkPMSrcOver(SkPixel4444ToPixel32(*src), *dst);
        src += 1;
        dst += 1;
    } while (--count != 0);
}

class Sprite_D32_S4444 : public SkSpriteBlitter {
public:
    Sprite_D32_S4444(const SkBitmap& source) : SkSpriteBlitter(source) {}
    
    virtual void blitRect(int x, int y, int width, int height) {
        SkASSERT(width > 0 && height > 0);
        SK_RESTRICT SkPMColor* dst = fDevice->getAddr32(x, y);
        const SK_RESTRICT SkPMColor16* src = fSource->getAddr16(x - fLeft,
                                                                y - fTop);
        unsigned dstRB = fDevice->rowBytes();
        unsigned srcRB = fSource->rowBytes();
        
        do {
            srcover_row(dst, src, width);
            dst = (SK_RESTRICT SkPMColor*)((char*)dst + dstRB);
            src = (const SK_RESTRICT SkPMColor16*)((const char*)src + srcRB);
        } while (--height != 0);
    }
};

///////////////////////////////////////////////////////////////////////////////

#include "SkTemplatesPriv.h"

SkSpriteBlitter* SkSpriteBlitter::ChooseD32(const SkBitmap& source,
                                            const SkPaint& paint,
                                            void* storage, size_t storageSize) {
    if (paint.getMaskFilter() != NULL) {
        return NULL;
    }

    U8CPU       alpha = paint.getAlpha();
    SkXfermode* xfermode = paint.getXfermode();
    SkColorFilter* filter = paint.getColorFilter();
    SkSpriteBlitter* blitter = NULL;

    switch (source.getConfig()) {
        case SkBitmap::kARGB_4444_Config:
            if (alpha != 0xFF) {
                return NULL;    // we only have opaque sprites
            }
            if (xfermode || filter) {
                SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D32_S4444_XferFilter,
                                      storage, storageSize, (source, paint));
            } else if (source.isOpaque()) {
                SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D32_S4444_Opaque,
                                      storage, storageSize, (source));
            } else {
                SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D32_S4444,
                                      storage, storageSize, (source));
            }
            break;
        case SkBitmap::kARGB_8888_Config:
            if (xfermode || filter) {
                if (255 == alpha) {
                    // this can handle xfermode or filter, but not alpha
                    SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D32_S32A_XferFilter,
                                      storage, storageSize, (source, paint));
                }
            } else {
#ifdef _MRVL_OPT
                    SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D32_S32_MRVL,
                                      storage, storageSize, (source, alpha));
#else
                // this can handle alpha, but not xfermode or filter
                SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D32_S32,
                              storage, storageSize, (source, alpha));
#endif
            }
            break;
#ifdef _MRVL_OPT
        case SkBitmap::kRGB_565_Config:
            if (255 == alpha && !xfermode && !filter){
                SK_PLACEMENT_NEW_ARGS(blitter, Sprite_D32_S16_Opaque_MRVL,
                                      storage, storageSize, (source));
            } else {
                return NULL;
            }
            break;
#endif
        default:
            break;
    }
    return blitter;
}

