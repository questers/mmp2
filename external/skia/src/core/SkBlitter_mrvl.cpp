/******************************************************************************
 *         Copyright (c) 2010 Marvell Corporation. All Rights Reserved.
 ******************************************************************************/

#ifdef _MRVL_OPT
#include "SkColorPriv.h"
#include "SkShader.h"
#include "SkUtils.h"
#include "SkXfermode.h"
#include "SkBlitter_mrvl.h"
#include "SkBitmapProcShader.h"
#include "SkBitmapProcShader_mrvl.h"
#include "mrvl_opt.h"

/////////////////////////////////////////////////////////////////////////////////

/* class SkARGB32_Shader_Blitter_MRVL */

void SkARGB32_Shader_Blitter_MRVL::blitRect(int x, int y, int width, int height) {
    SkASSERT(x >= 0 && y >= 0 && x + width <= fDevice.width() && y + height <= fDevice.height());


    if(fShader) {
        int flag = fShader->getFlags() & SkShader_MRVL::kIsAcceleratedMask_MRVL;
        if(flag == SkShader_MRVL::kIsAcceleratedForBitmapProc_MRVL
            && ((SkBitmapProcShader_MRVL*)fShader)->ShadeRect(x, y, width, height, &fDevice)) {
            //SkDebugf("shader->ShadeRect(x=%d, y=%d, width=%d, height=%d, dst=0x%x, dstRB=%d)\n", x, y, width, height, dst, fDevice.rowBytes());
        } else {
            //SkDebugf("this->INHERITED::blitRect(x=%d, y=%d, width=%d, height=%d)\n", x, y, width, height);
            this->INHERITED::blitRect(x, y, width, height);
        }
    } else {
        //SkDebugf("this->INHERITED::blitRect(x=%d, y=%d, width=%d, height=%d)\n", x, y, width, height);
        this->INHERITED::blitRect(x, y, width, height);
    }
}


//////////////////////////////////////////////////////////////////////////////////

static void _memset32_(void *pdata)
{
    int *p = (int*)pdata;
    int w = p[0]*4;
    int h = p[1];
    unsigned char *pDst = (unsigned char*)p[14];
    unsigned int dstride = p[15];
    for(int i =0;i<h;i++) {
        sk_memset32((uint32_t*)pDst, 0, w);
        pDst+= dstride;
    }
}

/* class SkARGB32_Blitter_MRVL */
SkARGB32_Blitter_MRVL::SkARGB32_Blitter_MRVL(const SkBitmap& device, const SkPaint& paint)
        : INHERITED(device, paint)
{
    int startIndex = ((uint32_t)(&fPrivateData[0]) & 7) ? 1 : 0;
    fPrivateDataP = &fPrivateData[startIndex];
    uint32_t alpha = SkColorGetA(fPMColor);
    if (alpha == 0) {
        fBlitRectProc = NULL;
    } else if (alpha == 0xFF) {
        fBlitRectProc = _BlendSrc32ToDst32_Src_Const_NoGlobalAlpha_MRVL;
        //fBlitRectProc = _memset32_;
    } else {
        fBlitRectProc = _BlendSrc32ToDst32_SrcOverDst_Const_NoGlobalAlpha_MRVL;
    }
    //dst stride and color
    fPrivateDataP[15] = (int32_t)fDevice.rowBytes();
    fPrivateDataP[16] = (int32_t)fPMColor;
#ifdef MRVL_SKIA_OPT_GCU
#if GCU_ENABLE_FILL
    fCanGoGCU = SkGCUBitmapSurface_MRVL::initGCU();   /* TODO: add check bmm */
#endif /* GCU_ENABLE_FILL */
#endif /* MRVL_SKIA_OPT_GCU */
}

#ifdef MRVL_SKIA_OPT_GCU
#if GCU_ENABLE_FILL
int SkARGB32_Blitter_MRVL::fillRectGCU(int x, int y, int width, int height)
{
    if((width >= WIDTH_LOW_BOUND_FILL) && (height >= HEIGHT_LOW_BOUND_FILL)) {
        int dstMaxY = y + height;
        SkGCUBitmapSurface_MRVL* dstBitmapSurface = SkGCUBitmapSurface_MRVL::createSurfaceFromBitmap(&fDevice, false, true, dstMaxY);
        if(!dstBitmapSurface) {
            return height;
        }
        GCU_DEBUG_PRINT("============= Fill blitRectGCU ARGB (%d, %d, %d, %d)=================\n", x, y, width, height);

        GCU_FILL_DATA   fillData;
        GCU_RECT        dstRect;
        int dstTruncatedHeight  = getTruncatedHeight(dstMaxY, dstBitmapSurface->getHeight(), &fDevice);
        int heightT             = height - dstTruncatedHeight;
        setGCURect(x, y, width, heightT, &fDevice, &dstRect);
        memset(&fillData, 0, sizeof(GCU_FILL_DATA));
        fillData.pSurface       = dstBitmapSurface->getSurface();
        fillData.pRect          = &dstRect;
        fillData.bSolidColor    = GCU_TRUE;
#ifdef MRVL_BGRA_HACK
        fillData.color          = fPMColor ;
#else
        fillData.color          = SkColorSetARGB(SkGetPackedA32(fPMColor), SkGetPackedR32(fPMColor),
                                                 SkGetPackedG32(fPMColor), SkGetPackedB32(fPMColor));
#endif

        GCU_DEBUG_PRINT("==== gcuFill Fillcolor 0x%x To %p (Bitmap %p %d x %d)(%d, %d - %d, %d)\n", fillData.color,
                            fillData.pSurface, &fDevice, fDevice.width(), fDevice.height(),
                            dstRect.left, dstRect.top, dstRect.right, dstRect.bottom);

        if(!dstBitmapSurface->FillAndFlush((SkBitmap*)&fDevice, &fillData)) {
            dstBitmapSurface->unref();
            return height;
        } else {
#if GCU_DUMP_SURFACES
            static int countdst = 0;
            dstBitmapSurface->dump("/data/dump/surface_fill32_dst", countdst++);
#endif
            dstBitmapSurface->unref();
            return height - heightT;
        }
    }
    return height;
}
#endif /* GCU_ENABLE_FILL */
#endif /* MRVL_SKIA_OPT_GCU */

void SkARGB32_Blitter_MRVL::blitRect(int x, int y, int width, int height)
{
    SkASSERT(x >= 0 && y >= 0 && x + width <= fDevice.width() && y + height <= fDevice.height());
    int leftHeight = height;
#ifdef MRVL_SKIA_OPT_GCU
#if GCU_ENABLE_FILL
    if(fCanGoGCU&&(fBlitRectProc==_BlendSrc32ToDst32_Src_Const_NoGlobalAlpha_MRVL)) {
        leftHeight = fillRectGCU(x, y, width, height);
        if(!leftHeight) {
            return ;
        }
    }
#endif /* GCU_ENABLE_FILL */
#endif /* MRVL_SKIA_OPT_GCU */
    if (fBlitRectProc == 0) {
        return;
    }
    //rect size
    fPrivateDataP[0] = width;
    fPrivateDataP[1] = leftHeight;

    //dst addr and stride
    fPrivateDataP[14] = (int32_t)fDevice.getAddr32(x, y + (height - leftHeight));
    //SkDebugf("SkARGB32_Blitter_MRVL::blitRect(x=%d, y=%d, width=%d, height=%d, color=0x%x, dst=0x%x)\n", x, y, width, height, fPMColor, fPrivateDataP[14]);
    //_dump_privateData((void*)fPrivateDataP);
    fBlitRectProc((void*)fPrivateDataP);

}

//////////////////////////////////////////////////////////////////////////////////
#ifdef _MRVL_OPT_RGB565

/* class SkRGB16_Shader_Blitter_MRVL */

void SkRGB16_Shader_Blitter_MRVL::blitRect(int x, int y, int width, int height) {
    SkASSERT(x >= 0 && y >= 0 && x + width <= fDevice.width() && y + height <= fDevice.height());

    if(fShader) {
        int flag = fShader->getFlags() & SkShader_MRVL::kIsAcceleratedMask_MRVL;
        if(flag == SkShader_MRVL::kIsAcceleratedForBitmapProc_MRVL
            && ((SkBitmapProcShader_MRVL*)fShader)->ShadeRect(x, y, width, height, &fDevice)) {
            //SkDebugf("shader->ShadeRect(x=%d, y=%d, width=%d, height=%d, dst=0x%x, dstRB=%d)\n", x, y, width, height, dst, fDevice.rowBytes());
        } else {
            //SkDebugf("this->INHERITED::blitRect(x=%d, y=%d, width=%d, height=%d)\n", x, y, width, height);
            this->INHERITED::blitRect(x, y, width, height);
        }
    } else {
        //SkDebugf("this->INHERITED::blitRect(x=%d, y=%d, width=%d, height=%d)\n", x, y, width, height);
        this->INHERITED::blitRect(x, y, width, height);
    }
}
#endif /* _MRVL_OPT_RGB565 */
#endif /* _MRVL_OPT */
/* EOF */






