/******************************************************************************
 *       Copyright (c) 2010 Marvell Corporation. All Rights Reserved.
 ******************************************************************************/
#ifdef _MRVL_OPT
#include "SkBitmapProcShader.h"
#include "SkBitmapProcShader_mrvl.h"
#include "SkColorPriv.h"
#include "SkPixelRef.h"
#include "SkBitmap.h"
#include "mrvl_opt.h"
#include "SkLog.h"
#define only_scale_and_translate(matrix)    \
    (((matrix).getType() & ~(SkMatrix::kTranslate_Mask | SkMatrix::kScale_Mask)) == 0)

#define only_translate(matrix) \
    (((matrix).getType() & ~(SkMatrix::kTranslate_Mask)) == 0)

typedef struct _proc_node
{
    SkBitmapProcShader_MRVL::ShadeRectOptProc proc;
    const char* const procname;
#ifdef MRVL_SKIA_OPT_GCU
    int               nGCUAccel;
#endif
} PROC_NODE;

#ifdef MRVL_SKIA_OPT_GCU

#define MAKE_NODE(x,y)  {x, #x, y}
#define GET_PROC(x)     ((x).proc)
#define GET_PROCNAME(x) ((x).procname)
#define GET_GCUFLAG(x)  ((x).nGCUAccel)

#define GCU_FLAG_NOTSUPPORT    0
#define GCU_FLAG_BLIT          1
#define GCU_FLAG_BLEND         2
#define GCU_FLAG_RESIZE_NORMAL 4
#define GCU_FLAG_RESIZE_HIGH   8
#define GCU_FLAG_RESIZE        (GCU_FLAG_RESIZE_NORMAL | GCU_FLAG_RESIZE_HIGH)
#else

#define MAKE_NODE(x,y)  {x, #x}
#define GET_PROC(x)     ((x).proc)
#define GET_PROCNAME(x) ((x).procname)
#define GET_GCUFLAG(x)  (0)

#endif

/**********************************************************************
  Optimized function tables.
  32 entries, 0 for noGA, 1 for GA
**********************************************************************/
/* Source ARGB8888 */
static const PROC_NODE optProcs[2][32] =
{
    {
        /*  ============ ARGB8888 to ARGB8888 ============*/
        /*  ====== srcOver src isn't opaque ======*/
        /* resize & translate */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_clamp_nobilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_NORMAL),   /* TODO */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_clamp_bilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_HIGH),       /* TODO */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_repeat_nobilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),                    /* TODO */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_repeat_bilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),                      /* TODO */

        /* translate only */

        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_clamp_nobilinear_translate_MRVL, GCU_FLAG_BLEND),                       /* TODO */
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),                                                                           /* no such case */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_repeat_nobilinear_translate_MRVL, GCU_FLAG_NOTSUPPORT),                 /* TODO */
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),                                                                           /* no such case */

        /*  ====== srcOver src is opaque ======*/
        /* resize & translate */
        MAKE_NODE(_BlendSrc32ToDst32_Src_clamp_nobilinear_resize_NoGlobalAlpha_MRVL, GCU_FLAG_BLIT|GCU_FLAG_RESIZE_NORMAL),
        MAKE_NODE(_BlendSrc32ToDst32_Src_clamp_bilinear_resize_NoGlobalAlpha_MRVL, GCU_FLAG_BLIT|GCU_FLAG_RESIZE_HIGH),
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_repeat_nobilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),                    /* TODO */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_repeat_bilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),                      /* TODO */

        /* translate only */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_clamp_nobilinear_translate_MRVL, GCU_FLAG_BLIT),                        /* TODO */
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),                                                                           /* no such case */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_repeat_nobilinear_translate_MRVL, GCU_FLAG_NOTSUPPORT),                 /* TODO */
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),                                                                           /* no such case */

#ifdef _MRVL_OPT_RGB565
        /* ============ ARGB8888 to RGB565 ============  */
        /*  ====== srcOver src isn't opaque ======*/
        /* resize & translate */
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_clamp_nobilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_NORMAL),  /* TODO */
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_clamp_bilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_HIGH),      /* TODO */
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_repeat_nobilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),                   /* TODO */
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_repeat_bilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),                     /* TODO */

        /* translate only */
        MAKE_NODE(NULL, GCU_FLAG_BLEND),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        /*  ====== srcOver src is opaque ======*/
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_clamp_nobilinear_resize_MRVL, GCU_FLAG_BLIT|GCU_FLAG_RESIZE_NORMAL),   /* TODO */
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_clamp_bilinear_resize_MRVL, GCU_FLAG_BLIT|GCU_FLAG_RESIZE_HIGH),       /* TODO */
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_repeat_nobilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),                   /* TODO */
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_repeat_bilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),                     /* TODO */
        MAKE_NODE(NULL, GCU_FLAG_BLIT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT)
#else
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT)
#endif
    },

    {
        /*  ============ ARGB8888 to ARGB8888 ============*/
        /*  ====== srcOver src isn't opaque ======*/

        /* resize & translate */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_clamp_nobilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_NORMAL),
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_clamp_bilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_HIGH),
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_repeat_nobilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_repeat_bilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),

        /* translate only */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_clamp_nobilinear_translate_MRVL, GCU_FLAG_BLEND),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),                                                                           /* no such case */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_repeat_nobilinear_translate_MRVL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),                                                                           /* no such case */

        /*  ====== srcOver src is opaque ======*/
        /* resize & translate */
        /* don't have src opaque primitives so far, use generic one instead. will enhance later on */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_clamp_nobilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_NORMAL),
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_clamp_bilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_HIGH),
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_repeat_nobilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_repeat_bilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),

        /* translate only */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_clamp_nobilinear_translate_MRVL, GCU_FLAG_BLEND),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),                                                                           /* no such case */
        MAKE_NODE(_BlendSrc32ToDst32_SrcOverDst_repeat_nobilinear_translate_MRVL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),                                                                           /* no such case */

#ifdef _MRVL_OPT_RGB565
        /* ============ ARGB8888 to RGB565 ============  */
        /*  ====== srcOver src isn't opaque ======*/

        /* resize & translate */
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_clamp_nobilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_NORMAL),
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_clamp_bilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_HIGH),
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_repeat_nobilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_repeat_bilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),

        /* translate only */
        MAKE_NODE(NULL, GCU_FLAG_BLEND),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        /*  ====== srcOver src is opaque ======*/

        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_clamp_nobilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_NORMAL),  /* TODO */
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_clamp_bilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_HIGH),      /* TODO */
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_repeat_nobilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),                   /* TODO */
        MAKE_NODE(_BlendSrc32ToDst565_SrcOverDst_repeat_bilinear_resize_MRVL, GCU_FLAG_NOTSUPPORT),                     /* TODO */

        MAKE_NODE(NULL, GCU_FLAG_BLEND),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT)
#else
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT)

#endif
    }
};


/* Source RGB565 */
static const PROC_NODE optProcs16[2][32] =
{
    {
        /*  ============ RGB565 to ARGB8888 ============*/
        /*  ====== srcOver src isn't opaque ======*/
        /* resize & translate */
        MAKE_NODE(_BlendSrc565ToDst32_Src_clamp_nobilinear_resize_NoGlobalAlpha_MRVL, GCU_FLAG_BLIT|GCU_FLAG_RESIZE_NORMAL),  /* shouldn't be such case, in case src.isOpaque() isn't set */
        MAKE_NODE(NULL, GCU_FLAG_BLIT|GCU_FLAG_RESIZE_HIGH),                                                                  /* clamp bilinear, GCU */
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        /* translate only */
        MAKE_NODE(NULL, GCU_FLAG_BLIT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        /*  ====== srcOver src is opaque ======*/
        MAKE_NODE(_BlendSrc565ToDst32_Src_clamp_nobilinear_resize_NoGlobalAlpha_MRVL, GCU_FLAG_BLIT|GCU_FLAG_RESIZE_NORMAL),
        MAKE_NODE(NULL, GCU_FLAG_BLIT|GCU_FLAG_RESIZE_HIGH),                                                                  /* clamp bilinear, GCU */
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        /* translate only */
        MAKE_NODE(NULL, GCU_FLAG_BLIT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        /*  ============ RGB565 to RGB565 ============*/
        /*  ====== srcOver src isn't opaque ======*/
#ifdef _MRVL_OPT_RGB565
        /* resize & translate */
        MAKE_NODE(_BlendSrc565ToDst565_Src_clamp_nobilinear_resize_NoGlobalAlpha_MRVL, GCU_FLAG_BLIT|GCU_FLAG_RESIZE_NORMAL), /* shouldn't be such case, in case src.isOpaque() isn't set */
        MAKE_NODE(_BlendSrc565ToDst565_Src_clamp_bilinear_resize_NoGlobalAlpha_MRVL, GCU_FLAG_BLIT|GCU_FLAG_RESIZE_HIGH),     /* shouldn't be such case, in case src.isOpaque() isn't set */
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        /* translate only */
        MAKE_NODE(NULL, GCU_FLAG_BLIT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        /*  ====== srcOver src is opaque ======*/
        /* resize & translate */
        MAKE_NODE(_BlendSrc565ToDst565_Src_clamp_nobilinear_resize_NoGlobalAlpha_MRVL, GCU_FLAG_BLIT|GCU_FLAG_RESIZE_NORMAL),
        MAKE_NODE(_BlendSrc565ToDst565_Src_clamp_bilinear_resize_NoGlobalAlpha_MRVL, GCU_FLAG_BLIT|GCU_FLAG_RESIZE_HIGH),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        /* translate only */
        MAKE_NODE(NULL, GCU_FLAG_BLIT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
#else
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
#endif
    },
    {
        /* ============ RGB565 to ARGB8888 ============*/
        /* ====== srcOver src isn't opaque ======*/
        /* resize & translate */
        MAKE_NODE(_BlendSrc565ToDst32_Src_clamp_nobilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_NORMAL),
        MAKE_NODE(NULL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_HIGH),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),                                                                           /* TODO */
        /* translate only */
        MAKE_NODE(NULL, GCU_FLAG_BLEND),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        /* ====== srcOver src is opaque ======*/
        MAKE_NODE(_BlendSrc565ToDst32_Src_clamp_nobilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_NORMAL),
        MAKE_NODE(NULL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_HIGH),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),                                                                           /* TODO */
        /* translate only */
        MAKE_NODE(NULL, GCU_FLAG_BLEND),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        /* ============ RGB565 to RGB565 ============*/
        /* ====== srcOver src isn't opaque ======*/
#ifdef _MRVL_OPT_RGB565
        /* resize & translate */
        MAKE_NODE(NULL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_NORMAL),
        MAKE_NODE(NULL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_HIGH),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        /* translate only */
        MAKE_NODE(NULL, GCU_FLAG_BLEND), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        /* ====== srcOver src is opaque ======*/
        /* resize & translate */
        MAKE_NODE(_BlendSrc565ToDst565_Src_clamp_nobilinear_resize_MRVL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_NORMAL),
        MAKE_NODE(NULL, GCU_FLAG_BLEND|GCU_FLAG_RESIZE_HIGH),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),

        /* translate only */
        MAKE_NODE(NULL, GCU_FLAG_BLEND), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
#else
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
        MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT), MAKE_NODE(NULL, GCU_FLAG_NOTSUPPORT),
#endif
    }
};

bool SkBitmapProcShader_MRVL::setContext(const SkBitmap& device,
                                    const SkPaint& paint,
                                    const SkMatrix& matrix)
{
    fInternalProc = NULL;
#ifdef MRVL_SKIA_OPT_GCU
    fGCUAccel = 0;
#endif

    /* go through fState.chooseProcs() */
    if (!this->INHERITED::setContext(device, paint, matrix)) {
        return false;
    }

    int index = 0;
    const SkBitmap& bitmap = *fState.fBitmap;
    SkMatrix totalMatrix = this->getTotalInverse();
    fSconfig = bitmap.config();
    //totalMatrix.dump();

    if(fSconfig != SkBitmap::kARGB_8888_Config && fSconfig != SkBitmap::kRGB_565_Config) {
        /* not supported */
        return true;
    }

    /* donot support Xfermode != srcOver */
    if(paint.getXfermode()) {
        /*
        SkXfermode::Mode mode;
        SkXfermode::IsMode(paint.getXfermode(, false), &mode);
        SkDebugf("xfermode = %d\n", mode);
        */
        return true;
    }

    if(only_scale_and_translate(totalMatrix)) {
        bool clamp_clamp = SkShader::kClamp_TileMode == fState.fTileModeX &&
                       SkShader::kClamp_TileMode == fState.fTileModeY;
        bool repeat_repeat = SkShader::kRepeat_TileMode == fState.fTileModeX &&
                       SkShader::kRepeat_TileMode == fState.fTileModeY;

        if(!clamp_clamp && !repeat_repeat) {
            return true;
        }

        SkBitmap::Config dconfig = device.getConfig();
        if(!(dconfig == SkBitmap::kRGB_565_Config || dconfig == SkBitmap::kARGB_8888_Config)) {
            return true;
        }
        if(paint.isDither() && fSconfig == SkBitmap::kARGB_8888_Config && dconfig == SkBitmap::kRGB_565_Config) {
            return true;
        }

        PRINT_SHADER("paint.isFilterBitmap() = %d, repeat_repeat=%d, bitmap.isOpaque()=%d\n", paint.isFilterBitmap(), repeat_repeat, bitmap.isOpaque());

        if(paint.isFilterBitmap() && totalMatrix.getType() > SkMatrix::kTranslate_Mask) {
            index += 1;
        }
        if(repeat_repeat) {
            index += 2;
        }
        if(only_translate(totalMatrix)) {
            index += 4;
        }
        if(bitmap.isOpaque()) {
            index += 8;
        }
        if(dconfig == SkBitmap::kRGB_565_Config) {
            index += 16;
        }
        int tblIndex = (paint.getAlpha() != 255) ? 1 : 0;
        if(fSconfig == SkBitmap::kARGB_8888_Config) {
            fInternalProc = GET_PROC(optProcs[tblIndex][index]);
#ifdef MRVL_SKIA_OPT_GCU
            fGCUAccel     = GET_GCUFLAG(optProcs[tblIndex][index]);
#endif
            PRINT_SHADER("bitmap %d x %d fInternalProc = %s(0x%x, false), %s[%d][%d]\n", bitmap.width(), bitmap.height(),
                          GET_PROCNAME(optProcs[tblIndex][index]), (uint32_t)fInternalProc, "optProcs", tblIndex, index);
        } else {
            fInternalProc = GET_PROC(optProcs16[tblIndex][index]);
#ifdef MRVL_SKIA_OPT_GCU
            fGCUAccel     = GET_GCUFLAG(optProcs16[tblIndex][index]);
#endif
            PRINT_SHADER("bitmap %d x %d fInternalProc = %s(0x%x, false), %s[%d][%d]\n", bitmap.width(), bitmap.height(),
                          GET_PROCNAME(optProcs16[tblIndex][index]), (uint32_t)fInternalProc, "optProcs16", tblIndex, index);
        }
    }
    int32_t allocatedBytes = (device.width() <<2) + (alignBytes << 1);
    if(fInternalProc && fAllocatedBytes < allocatedBytes) {
        if(fFreeImgScanLineP) {
            sk_free(fFreeImgScanLineP);
        }
        fFreeImgScanLineP = (uint8_t*) sk_malloc_throw(allocatedBytes);
        if(!fFreeImgScanLineP) {
            fInternalProc = NULL;
            fAllocatedBytes =  0;
        } else {
            fFreeImgScanLineAlignedP = (uint8_t*)(((int32_t)fFreeImgScanLineP + alignBytes - 1) & (~(alignBytes - 1)));
            fAllocatedBytes          = allocatedBytes;
        }
    }
    return true;
}

bool SkBitmapProcShader_MRVL::ShadeRect(int x, int y, int width, int height, const SkBitmap *pDstBitmap)
{
    SkASSERT(width > 0 && height > 0 && dst && dstRB > 0);
    int leftHeight = height;
#ifdef MRVL_SKIA_OPT_GCU
    if(fCanGoGCU && fGCUAccel) {
        /* return either 0 or height, no intermediate height */
        leftHeight = ShadeRectGCU(x, y, width, height, pDstBitmap);
        if(!leftHeight) {
            return true;
        }
    }
#endif
    if(fInternalProc) {
        //SkDebugf("\nSkBitmapProcShader_MRVL::ShadeRect\n");

        const SkBitmap& bitmap = *fState.fBitmap;
        int32_t imageStride = (int32_t)bitmap.rowBytes();
        if((uint16_t)((int16_t)imageStride) != imageStride) {
            return false;
        }

        SkMatrix dstMatrix = this->getTotalInverse();

        fPrivateDataP[0] = width;
        fPrivateDataP[1] = leftHeight;

        fPrivateDataP[2] = (int32_t)FloatToQ16(dstMatrix.getScaleX());
        fPrivateDataP[3] = 0;//(int32_t)FloatToQ16(dstMatrix.getSkewY());
        fPrivateDataP[4] = 0;//(int32_t)FloatToQ16(dstMatrix.getSkewX());
        fPrivateDataP[5] = (int32_t)FloatToQ16(dstMatrix.getScaleY());

        fPrivateDataP[6] = (int32_t)fFreeImgScanLineAlignedP;
        fPrivateDataP[7] = (int32_t)bitmap.getAddr(0, 0);
        fPrivateDataP[8] = (fSconfig == SkBitmap::kARGB_8888_Config)?4:2;

        fPrivateDataP[9]  = (int32_t)bitmap.rowBytes();
        fPrivateDataP[10] = (int32_t)bitmap.width();
        fPrivateDataP[11] = (int32_t)bitmap.height();

        float xOffset = x + 0.5f;
        float yOffset = y + (height - leftHeight) + 0.5f;

        //the Matrix mustn't have skewX or skewY
        fPrivateDataP[12] = (int32_t)FloatToQ16(SkScalarToFloat(dstMatrix.getScaleX())* xOffset + SkScalarToFloat(dstMatrix.getTranslateX()));
        fPrivateDataP[13] = (int32_t)FloatToQ16(SkScalarToFloat(dstMatrix.getScaleY())* yOffset + SkScalarToFloat(dstMatrix.getTranslateY()));

        // pDst, iDstStride, blendConst
        fPrivateDataP[14] = (int32_t)pDstBitmap->getAddr(x, y+(height - leftHeight));
        fPrivateDataP[15] = (int32_t)pDstBitmap->rowBytes();
        fPrivateDataP[16] = (int32_t)getPaintAlpha();

        //_dump_privateData(fPrivateDataP);
        fInternalProc((void*)fPrivateDataP);
        return true;
    }
    return false;
}

#ifdef MRVL_SKIA_OPT_GCU
int SkBitmapProcShader_MRVL::ShadeRectGCU(int x, int y, int width, int height, const SkBitmap *pDstBitmap)
{
    SkASSERT(fGCUAccel);
    if((fGCUAccel & GCU_FLAG_RESIZE_HIGH) && ((width < WIDTH_LOW_BOUND_RESIZE_BILINEAR) || (height < HEIGHT_LOW_BOUND_RESIZE_BILINEAR))) {
        return height;
    }
    if((fGCUAccel & GCU_FLAG_RESIZE_NORMAL) && ((width < WIDTH_LOW_BOUND_RESIZE_NEAREST) || (height < HEIGHT_LOW_BOUND_RESIZE_NEAREST))) {
        return height;
    }
    if(!(fGCUAccel & GCU_FLAG_RESIZE) && ((width < WIDTH_LOW_BOUND_BLEND) || (height < HEIGHT_LOW_BOUND_BLEND))) {
        return height;
    }
    const SkBitmap* pSrcBitmap = fState.fBitmap;
    bool            bResize    = (fGCUAccel & GCU_FLAG_RESIZE) ? true : false;
    SkMatrix        dstMatrix  = this->getTotalInverse();

    int srcW = (int)(SkScalarToFloat(dstMatrix.getScaleX()) * width);
    int srcH = (int)(SkScalarToFloat(dstMatrix.getScaleY()) * height);
    int srcX = (int)(SkScalarToFloat(dstMatrix.getScaleX()) * (x + 0.5f) + SkScalarToFloat(dstMatrix.getTranslateX()));
    int srcY = (int)(SkScalarToFloat(dstMatrix.getScaleY()) * (y + 0.5f) + SkScalarToFloat(dstMatrix.getTranslateY()));

    if((int)(srcW | srcH | srcX | srcY) < 0) {
        /* GCU cannot handle negative width or height  */
        GCU_DEBUG_PRINT("============= negative width or height, oops cannot deal with GCU (%d %d %d %d)\n", srcX, srcY, srcW, srcH);
        return height;
    }

    int srcMaxY = srcY + srcH;
    int dstMaxY = y + height;

    if(srcX + srcW > pSrcBitmap->width()  || srcMaxY > pSrcBitmap->height()) {
        /* a clamp case, cannot do with GCU */
        GCU_DEBUG_PRINT("============= A real clamp case, oops cannot deal with GCU (%d %d %d %d) -- %d x %d\n", srcX, srcY, srcW, srcH, pSrcBitmap->width(), pSrcBitmap->height());
        return height;
    }

    SkGCUBitmapSurface_MRVL* srcBitmapSurface = SkGCUBitmapSurface_MRVL::createSurfaceFromBitmap(pSrcBitmap, true, !bResize, srcMaxY);
    if(!srcBitmapSurface) {
        return height;
    }
    SkGCUBitmapSurface_MRVL* dstBitmapSurface = SkGCUBitmapSurface_MRVL::createSurfaceFromBitmap(pDstBitmap, false, !bResize, dstMaxY);
    if(!dstBitmapSurface) {
        srcBitmapSurface->unref();
        return height;
    }

    GCU_DEBUG_PRINT("============= ShadeRectGCU (%d, %d, %d, %d) %p %p  =================\n", x, y, width, height, pSrcBitmap, pDstBitmap);
#if GCU_CALL_DEBUG
    GCU_DEBUG_PRINT("============= Dump InverseMatrix ===================\n");
    dstMatrix.dump();
#endif

    GCU_RECT srcRect, dstRect;
    int heightT = height;
    if(bResize) {
        setGCURect(srcX, srcY, srcW, srcH, pSrcBitmap, &srcRect);
        setGCURect(x, y, width, height, pDstBitmap, &dstRect);
    } else {
        SkASSERT(srcW == width);
        SkASSERT(srcH == height);

        int srcTruncatedHeight = getTruncatedHeight(srcMaxY, srcBitmapSurface->getHeight(), pSrcBitmap);
        int dstTruncatedHeight = getTruncatedHeight(dstMaxY, dstBitmapSurface->getHeight(), pDstBitmap);
        heightT -= SkMax32(srcTruncatedHeight, dstTruncatedHeight);
        setGCURect(srcX, srcY, width, heightT, pSrcBitmap, &srcRect);
        setGCURect(x, y, width, heightT, pDstBitmap, &dstRect);
    }

    GCU_BLEND_DATA blendData;
    memset(&blendData, 0, sizeof(GCU_BLEND_DATA));
    blendData.pSrcRect          = &srcRect;
    blendData.pDstRect          = &dstRect;
    blendData.rotation          = GCU_ROTATION_0;
    blendData.pSrcSurface       = srcBitmapSurface->getSurface();
    blendData.pDstSurface       = dstBitmapSurface->getSurface();
    blendData.blendMode         = (fGCUAccel & GCU_FLAG_BLIT)?GCU_BLEND_SRC:GCU_BLEND_SRC_OVER;
    if(blendData.blendMode == GCU_BLEND_SRC_OVER) {
        blendData.srcGlobalAlpha= getPaintAlpha();
        blendData.dstGlobalAlpha= 0xFF;
    }

    int nQuality = (fGCUAccel & GCU_FLAG_RESIZE_HIGH)?2:((fGCUAccel & GCU_FLAG_RESIZE_NORMAL)?1:0);

    GCU_DEBUG_PRINT("==== gcuBlend %p (Bitmap %p config %d %d x %d)(%d, %d - %d, %d) -- %p (Bitmap %p config %d %d x %d)(%d, %d - %d, %d) mode = %d alpha=0x%2x\n",
                blendData.pSrcSurface, pSrcBitmap, pSrcBitmap->config(), pSrcBitmap->width(), pSrcBitmap->height(),
                srcRect.left, srcRect.top, srcRect.right, srcRect.bottom,
                blendData.pDstSurface, pDstBitmap, pDstBitmap->config(), pDstBitmap->width(), pDstBitmap->height(),
                dstRect.left, dstRect.top, dstRect.right, dstRect.bottom,
                blendData.blendMode, blendData.srcGlobalAlpha);

    if(!srcBitmapSurface->blendAndFlush(pSrcBitmap, pDstBitmap, &blendData, nQuality)) {
        srcBitmapSurface->unref();
        dstBitmapSurface->unref();
        return height;
    } else {
        srcBitmapSurface->unref();
        dstBitmapSurface->unref();
        return height - heightT;
    }
}
#endif /* MRVL_SKIA_OPT_GCU */

#endif /* _MRVL_OPT */
/* EOF */


