#include <stdio.h>

struct ITEM_T
{
    int bit;
    char* file;
} items[] =
{
    {0x01, "zImage"},
    {0x02, "system.img"},
    {0x04, "splash.img"},
    {0x08, "custom.img"},
    {0x10, "zImage_recovery"},
	{0x20, "uImage"},
	{0x40, "uImage_recovery"},
    {-1,   NULL},
};

int main(int argc, char* argv[])
{
    int i;
    int ret;
    int sel;

    for (i=0; items[i].bit!=-1; i++)
        fprintf(stderr, "0x%02x - %s\n", items[i].bit, items[i].file);
    fprintf(stderr, "---\n");
    fprintf(stderr, "Common case:\n");
    fprintf(stderr, "03 - zImage + system.img\n");
	fprintf(stderr, "22 - uImage + system.img\n");
    fprintf(stderr, "1f - zImage + system.img + splash.img + custom.img + zImage_recovery\n");
	fprintf(stderr, "6e - uImage + system.img + splash.img + custom.img + uImage_recovery\n");
    fprintf(stderr, "---\n");
 
    do
    {
        char buf[8];
        fprintf(stderr, "Please type the combination of the images to package (hex code): ");
        fscanf(stdin, "%2s", buf);
        ret = sscanf(buf, "%x", &sel);
    } while (ret==0 || sel==0);

    for (i=0; items[i].bit!=-1; i++)
    {
        if (sel&items[i].bit)
            printf("%s ", items[i].file);
    }

    return 0;
}
