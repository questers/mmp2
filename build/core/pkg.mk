# To build the update.zip package. -guang

META_INF_PATH := $(TOPDIR)bootable/recovery/etc/META-INF
SIGN_KEY_BASE := $(TOPDIR)build/target/product/security/$(basename $(subst _,.,$(TARGET_PRODUCT)))
SIGN_TOOL_PATH := $(TOPDIR)out/host/linux-x86/framework/signapk.jar
.PHONY: updatepackage updatepackage_partial
updatepackage: menu
	@cp -rf $(META_INF_PATH) $(PRODUCT_OUT)
	@cd $(PRODUCT_OUT) &&\
	zip -MM update-not-signed.zip -r META-INF $(shell ./menu)
	@rm -rf $(PRODUCT_OUT)/META-INF
	@rm -f menu
	java -Xmx1024m -jar $(SIGN_TOOL_PATH) -w $(SIGN_KEY_BASE).x509.pem $(SIGN_KEY_BASE).pk8 $(PRODUCT_OUT)/update-not-signed.zip $(PRODUCT_OUT)/update.zip
	@rm -f $(PRODUCT_OUT)/update-not-signed.zip

updatepackage_partial:
	@cp -rf $(META_INF_PATH)-PART $(PKG_DIR)/META-INF
	@cd $(PKG_DIR) &&\
	zip update-not-signed.zip -r *
	java -Xmx1024m -jar $(SIGN_TOOL_PATH) -w $(SIGN_KEY_BASE).x509.pem $(SIGN_KEY_BASE).pk8 $(PKG_DIR)/update-not-signed.zip $(PRODUCT_OUT)/update.zip
	@rm -f $(PKG_DIR)/update-not-signed.zip
	@rm -rf $(PKG_DIR)/META-INF

# menu utility to select the images to package
menu: $(BUILD_SYSTEM)/menu.c
	@gcc $^ -o $@ 

