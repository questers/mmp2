#!/bin/sh
AUTH='/C=CN/ST=Shenzhen/L=XiXiang/O=TanXun/OU=TanXun/CN=TanXun/emailAddress=support@tanxun.com'
JAR_PATH=../../../../out/host/linux-x86/framework/dumpkey.jar

if [ "$1" = "" ]; then
    echo "Create a certificate key."
    echo "Usage: $0 NAME"
    exit
fi

if [ ! -e $1.txt ]; then
    echo -n "Please enter the password:"
    read PASSWORD
    echo $PASSWORD > $1.txt
fi

openssl genrsa -3 -out $1.pem 2048 
openssl req -new -x509 -key $1.pem -out $1.x509.pem -days 10000 -subj $AUTH 
openssl pkcs8 -in $1.pem -topk8 -outform DER -out $1.pk8 -passout file:$1.txt

# generate .key file for recovery signature verification
java -jar $JAR_PATH $1.x509.pem > $1.key

# add testkey, so we can support 3rd package signed with testkey
echo "," >> $1.key
cat testkey.key >> $1.key

mkdir $1
cd $1
ln -s ../$1.pk8 media.pk8
ln -s ../$1.pk8 platform.pk8
ln -s ../$1.pk8 testkey.pk8
ln -s ../$1.pk8 shared.pk8
ln -s ../$1.x509.pem media.x509.pem
ln -s ../$1.x509.pem platform.x509.pem
ln -s ../$1.x509.pem testkey.x509.pem
ln -s ../$1.x509.pem shared.x509.pem
cp ../pw.txt .
sed -i "s,butterfly,$1,g" pw.txt


