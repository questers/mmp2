#if 0
#ifndef GPS_IO_POWER
#define GPS_IO_POWER GPIO(74)
#endif
#ifndef GPS_IO_ONOFF
#define GPS_IO_ONOFF GPIO(75)
#endif
#ifndef GPS_IO_RESET
#define GPS_IO_RESET GPIO(76)
#endif
#ifndef GPS_IO_FLASH
#define GPS_IO_FLASH GPIO(80)
#endif


static void gps_power_on(void)
{
	int gps_rst_n;
	int gpio;

	gpio = mfp_to_gpio(MFP_PIN_GPIO49);
	if (gpio_request(gpio, "GPIO49")) {
	        pr_err("Failed to request GPIO 49\n");
	        return;
	}
	gpio_direction_input(gpio);
	gpio_free(gpio);

	gpio = mfp_to_gpio(MFP_PIN_GPIO50);
	if (gpio_request(gpio, "GPIO50")) {
	        pr_err("Failed to request GPIO 50\n");
	        return;
	}
	gpio_direction_output(gpio, 1);
	gpio_free(gpio);

	gps_rst_n = mfp_to_gpio(MFP_PIN_GPIO15);
	if (gpio_request(gps_rst_n, "gpio_gps_rst")) {
		pr_err("Failed to request gpio_gps_rst\n");
	}

	gpio_direction_output(gps_rst_n, 0);
	mdelay(1);

	gpio_free(gps_rst_n);
	printk(KERN_INFO "sirf gps chip powered on\n");
}

static void gps_power_off(void)
{
	int gps_rst_n, gps_on;

	gps_on = mfp_to_gpio(MFP_PIN_GPIO14);
	if (gpio_request(gps_on, "gpio_gps_on")) {
		pr_err("Failed to request gpio_gps_on\n");
	}

	gps_rst_n = mfp_to_gpio(MFP_PIN_GPIO15);
	if (gpio_request(gps_rst_n, "gpio_gps_rst")) {
		pr_err("Failed to request gpio_gps_rst\n");
	}

	gpio_direction_output(gps_rst_n, 0);
	gpio_direction_output(gps_on, 0);

	gpio_free(gps_on);
	gpio_free(gps_rst_n);
	printk(KERN_INFO "sirf gps chip powered off\n");
}

static void gps_reset(int flag)
{
	int gps_rst_n;

	gps_rst_n = mfp_to_gpio(MFP_PIN_GPIO15);
	if (gpio_request(gps_rst_n, "gpio_gps_rst")) {
		pr_err("Failed to request gpio_gps_rst\n");
	}

	gpio_direction_output(gps_rst_n, flag);

	gpio_free(gps_rst_n);
	printk(KERN_INFO "sirf gps chip reset\n");
}

static void gps_on_off(int flag)
{
	int gps_on;

	gps_on = mfp_to_gpio(MFP_PIN_GPIO14);
	if (gpio_request(gps_on, "gpio_gps_on")) {
		pr_err("Failed to request gpio_gps_on\n");
	}

	gpio_direction_output(gps_on, flag);

	gpio_free(gps_on);
	printk(KERN_INFO "sirf gps chip offon\n");
}

#ifdef	CONFIG_PROC_FS
#define PROC_PRINT(fmt, args...) 	do {len += sprintf(page + len, fmt, ##args); } while(0)

static char sirf_status[4] = "off";
static ssize_t sirf_read_proc(char *page, char **start, off_t off,
		int count, int *eof, void *data)
{
	int len = strlen(sirf_status);

	sprintf(page, "%s\n", sirf_status);
	return len + 1;
}

static ssize_t sirf_write_proc(struct file *filp,
		const char *buff, size_t len, loff_t *off)
{
	char messages[256];
	int flag, ret;
	char buffer[7];

	if (len > 256)
		len = 256;

	if (copy_from_user(messages, buff, len))
		return -EFAULT;

	if (strncmp(messages, "off", 3) == 0) {
		strcpy(sirf_status, "off");
		gps_power_off();
	} else if (strncmp(messages, "on", 2) == 0) {
		strcpy(sirf_status, "on");
		gps_power_on();
	} else if (strncmp(messages, "reset", 5) == 0) {
		strcpy(sirf_status, messages);
		ret = sscanf(messages, "%s %d", buffer, &flag);
		if (ret == 2)
			gps_reset(flag);
	} else if (strncmp(messages, "sirfon", 5) == 0) {
		strcpy(sirf_status, messages);
		ret = sscanf(messages, "%s %d", buffer, &flag);
		if (ret == 2)
			gps_on_off(flag);
	} else {
		printk("usage: echo {on/off} > /proc/driver/sirf\n");
	}

	return len;
}

static void create_sirf_proc_file(void)
{
	struct proc_dir_entry *sirf_proc_file =
		create_proc_entry("driver/sirf", 0644, NULL);

	if (sirf_proc_file) {
		sirf_proc_file->read_proc = sirf_read_proc;
		sirf_proc_file->write_proc = (write_proc_t  *)sirf_write_proc;
	} else
		printk(KERN_INFO "proc file create failed!\n");
}
#endif

#endif

static void __init board_gps_init(void)
{
	printk(KERN_WARNING"gps init move to userspace\n");
}

