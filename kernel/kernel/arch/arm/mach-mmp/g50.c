/*
 *  linux/arch/arm/mach-mmp/g50.c
 *
 *  Support for the Marvell G50 Development Platform.
 *
 *  Copyright (C) 2009-2010 Marvell International Ltd.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  publishhed by the Free Software Foundation.
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/io.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/regulator/machine.h>
#include <linux/regulator/fixed.h> 
#include <linux/clk.h>
#include <linux/switch.h>
#include <linux/synaptics_i2c_rmi.h>
#include <linux/android_pmem.h>
#include <linux/pwm.h>
#include <linux/pwm_backlight.h>
#include <linux/gpio_keys.h>
#include <linux/fb.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <mach/addr-map.h>
#include <mach/mfp-mmp2.h>
#include <mach/mmp2.h>
#include <mach/irqs.h>
#include <mach/regs-mpmu.h>
#include <mach/regs-icu.h>
#include <mach/mmp2_plat_ver.h>
#include <mach/mmp2_dvfm.h>
#include <mach/pxa168fb.h>
#include <mach/mrvl_hdmitx.h>
#include <plat/pxa27x_keypad.h>
#include <plat/generic.h>
#include <linux/usb.h>
#include <linux/usb/otg.h>
#include <plat/pxa_u2o.h>
#include <plat/vbus.h>
#include "common.h"
#include <mach/uio_hdmi.h>

#ifdef CONFIG_SD8XXX_RFKILL
#include <linux/sd8x_rfkill.h>
#endif

//#define LCD_BACKLIGHT_INVERTED 
#define G50_NR_IRQS		(IRQ_BOARD_START + 48)

static unsigned long g50_pin_config[] __initdata = {
	/* UART1:88W8787 (bluetooth) */
	GPIO29_UART1_RXD,
	GPIO30_UART1_TXD,
	GPIO31_UART1_CTS,
	GPIO32_UART1_RTS,

	/* UART4:GPS */
	GPIO117_UART4_RXD,
	GPIO118_UART4_TXD,
	GPIO11_GPIO11,     /* GPS_PEN */
	GPIO135_GPIO135,     /* GPS_RESETn */
	GPIO122_GPIO122,     /* GPS_ON */
	GPIO138_GPIO138,     /* GPS_RE_FLASH */

	/* UART3:debug */
	GPIO51_UART3_RXD,
	GPIO52_UART3_TXD,

	/*CCIC1 - camera */
	GPIO20_CAM_HSYNC,		
	GPIO21_CAM_VSYNC,		
	GPIO22_CAM_MCLK,	
	GPIO23_CAM_PCLK,	
	GPIO12_CCIC_IN7,	
	GPIO13_CCIC_IN6,	
	GPIO14_CCIC_IN5,	
	GPIO15_CCIC_IN4,
	GPIO16_CCIC_IN3,	
	GPIO17_CCIC_IN2,	
	GPIO18_CCIC_IN1,	
	GPIO19_CCIC_IN0,
	GPIO53_GPIO53,	      /* low sensor power enable */
	GPIO137_GPIO137,	  /* CAM_PWR:reset is done by HW */
	
	/* touch switch */
	GPIO49_GPIO49,    /* TSI_INT_CAP */
	
	/* NAS capacitive touch screen INT */
	GPIO115_GPIO115,	  /* PENIRQ */
	GPIO141_GPIO141,		/*touch reset*/
		
	/*gsensor*/
	/*GPIO46_GPIO46,	  GS_INT --> move to as modem wakeup  */
		
	/*magnetic sensor*/
	GPIO45_GPIO45,   /* MAGNETIC_DRDY */

	/*gyroscope*/
	GPIO43_GPIO43,   /* GYROSCOPE_DRDY */
	GPIO44_GPIO44,   /* GYROSCOPE_INT */

	/*RTC*/
	GPIO36_RTC_ALARM,   /* RTC_INT */

	/* TWSI1: ADS1000 */
	TWSI1_SCL,
	TWSI1_SDA,

	/* TWSI2: RT5625, 88W8787(FM), magnetic sensor, RTC */
	GPIO55_TWSI2_SCL,
	GPIO56_TWSI2_SDA,

	/* TWSI3: camera, gyroscope, gsensor */
	GPIO71_TWSI3_SCL,
	GPIO72_TWSI3_SDA,

	/* TWSI4: touch screen, touch switch, light sensor stk2201 */
	TWSI4_SCL,
	TWSI4_SDA,

	/* TWSI6: HDMI DDC */
	GPIO47_TWSI6_SCL,
	GPIO48_TWSI6_SDA,
 
	/* RT5625 */
	GPIO33_GPIO33,         /* CODEC_INT */
	GPIO50_GPIO50,		   /* AUDIO_RST */

	GPIO125_GPIO125,		   /* JACK_DET */

	/* SSPA1 (I2S, connected between MMP2 and RT5625) */
 	GPIO24_I2S_SYSCLK,	
 	GPIO25_I2S_BITCLK,		
 	GPIO26_I2S_SYNC,	
 	GPIO27_I2S_DATA_OUT,	
 	GPIO28_I2S_SDATA_IN,
 	/* PCM is connected between RT5625 and 88W8787 (bluetooth) */ 
 	/* LINE is connected between RT5625 and 88W8787 (fm) */ 

	/*  LCD */
	GPIO74_LCD_FCLK,
	GPIO75_LCD_LCLK,
	GPIO76_LCD_PCLK,
	GPIO77_LCD_DENA,
	GPIO78_LCD_DD0,
	GPIO79_LCD_DD1,
	GPIO80_LCD_DD2,
	GPIO81_LCD_DD3,
	GPIO82_LCD_DD4,
	GPIO83_LCD_DD5,
	GPIO84_LCD_DD6,
	GPIO85_LCD_DD7,
	GPIO86_LCD_DD8,
	GPIO87_LCD_DD9,
	GPIO88_LCD_DD10,
	GPIO89_LCD_DD11,
	GPIO90_LCD_DD12,
	GPIO91_LCD_DD13,
	GPIO92_LCD_DD14,
	GPIO93_LCD_DD15,
	GPIO94_LCD_DD16,
	GPIO95_LCD_DD17,
	GPIO96_LCD_DD18,
	GPIO97_LCD_DD19,
	GPIO98_LCD_DD20,
	GPIO99_LCD_DD21,
	GPIO100_LCD_DD22,
	GPIO101_LCD_DD23,
	GPIO120_LCD_PWR,	 /* LCD_PWR_ENn */
	GPIO116_LCD_PWR_BL,	 /* LCD_BL_PWR_EN */
	
	GPIO129_VCORE_MODE,   	/*   VCORE_MODE */
	

	/*ULPI*/
	GPIO157_ULPI_CLK_PWM2,  
	GPIO59_ULPI_DAT7,
	GPIO60_ULPI_DAT6,
	GPIO61_ULPI_DAT5,
	GPIO62_ULPI_DAT4,
	GPIO63_ULPI_DAT3,
	GPIO64_ULPI_DAT2,
	GPIO65_ULPI_DAT1,
	GPIO66_ULPI_DAT0,
	GPIO67_ULPI_STP,
	GPIO68_ULPI_NXT,
	GPIO69_ULPI_DIR,
	GPIO70_ULPI_CLK,
	/*hub reset*/
	GPIO124_USB_HUB_RST,
	/*ULPI reset*/
	GPIO4_USB_PHY_RST,

	/* VBUS_FAULT_N */
	GPIO7_GPIO7,
	/* VBUS_EN */
	GPIO6_GPIO6,

	/* pwm3: Backlight */
	GPIO73_LCM_BIAS_PWM,	

	/* HDMI */
	GPIO34_HDMI_DET_PULL_HIGH,
	GPIO54_HDMI_CEC,
	
	/* 88W8787 */
	GPIO105_LPM_HIGH_PULL_LOW,      /* 88W8787 power down */  
	GPIO58_LPM_LOW_PULL_LOW,      /* 88W8787 reset */
	GPIO5_GPIO5,      /* FM wake */
	GPIO0_GPIO0,      /* wifi wake */
	GPIO10_GPIO10,      /* BT wake */

	/* USIM unused, set as GPIO input, Pull-high */
	GPIO102_GPIO102,
	GPIO103_GPIO103,
	/*GPIO142_GPIO142,	used as modem wake*/

	/* VCORE control */
	VCXO_REQ_AF2,    /*not used*/
	VCXO_OUT_AF2,    /*not used*/
	GPIO2_VCORE_ADJ0,     /* V_ADJ0 */
	GPIO3_VCORE_ADJ1,	 /* V_ADJ1 */

	/*mobile modem power control*/
	GPIO121_MODEM_PWR,	/*modem power */
	GPIO126_MODEM_ONOFF,	/*modem on/off*/
	GPIO127_MODEM_RESET,	/*modem reset*/
	GPIO110_MODEM_WAKEAP,	/*modem host wakeup 3V logic*/
	GPIO46_MODEM_WAKEAP,	/*modem host wakeup 1V8 logic*/
	GPIO142_MODEM_WAKE,		/*modem wake*/
	
	/*w1*/
	G_CLK_REQ_W1,

	/*gpio key*/
	GPIO8_GPIO8,    /*volumep*/
	GPIO9_GPIO9,    /*volumen*/


	/* MMC1:SD card */
	GPIO131_MMC1_DAT3_PULL_HIGH,
	GPIO132_MMC1_DAT2_PULL_HIGH,
	GPIO133_MMC1_DAT1_PULL_HIGH,
	GPIO134_MMC1_DAT0_PULL_HIGH,
	GPIO136_MMC1_CMD_PULL_HIGH,
	GPIO139_MMC1_CLK,
	GPIO140_MMC1_CD_PULL_LOW,

	/* MMC2 */
	GPIO37_MMC2_DAT3_PULL_HIGH,
	GPIO38_MMC2_DAT2_PULL_HIGH,
	GPIO39_MMC2_DAT1_PULL_HIGH,
	GPIO40_MMC2_DAT0_PULL_HIGH,
	GPIO41_MMC2_CMD_PULL_HIGH,
	GPIO42_MMC2_CLK,
	
	/* MMC3:emmc flash */
	GPIO165_MMC3_DAT7_PULL_HIGH,
	GPIO162_MMC3_DAT6_PULL_HIGH,
	GPIO166_MMC3_DAT5_PULL_HIGH,
	GPIO163_MMC3_DAT4_PULL_HIGH,
	GPIO167_MMC3_DAT3_PULL_HIGH,
	GPIO164_MMC3_DAT2_PULL_HIGH,
	GPIO168_MMC3_DAT1_PULL_HIGH,
	GPIO111_MMC3_DAT0_PULL_HIGH,
	GPIO112_MMC3_CMD_PULL_HIGH,
	GPIO151_MMC3_CLK,

	GPIO57_GPIO57,
	/*power management*/
	GPIO130_PWR_KEY, /*power button*/	
	GPIO128_PWR_OFF, /*power off*/
	GPIO10_REBOOT, /* reboot */

	MFP_CFG_LPM_DRV_PULL(GPIO123,FLOAT,AF0,MEDIUM,FLOAT), /* DC_IN */
	//MFP_CFG_LPM_DRV_PULL(GPIO161,FLOAT,AF1,MEDIUM,FLOAT), /* charge full */
	MFP_CFG_PULL(GPIO161, AF1, FLOAT),
	
#if defined(CONFIG_BUTTERFLY)
	GPIO119_GPIO119,     /* speaker control? */
#endif

#if defined(CONFIG_DRAGONFLY)
    GPIO104_GPIO104,
    GPIO109_GPIO109,
    GPIO110_GPIO110,
    GPIO108_GPIO108, /* tp power */
#endif

	GPIO106_GPIO106, /* high(down) sensor power down */
	GPIO107_GPIO107, /* low(up) sensor power down */
};


static void vcore_mode_set(int state)
{
	int vcore_mode;
	vcore_mode = mfp_to_gpio(GPIO129_VCORE_MODE);
	if (gpio_request(vcore_mode, "vcore_mode")) {
		printk(KERN_INFO "gpio %d request failed\n", vcore_mode);
		return ;
	}

	if (state)
		gpio_direction_output(vcore_mode, 1);
	else
		gpio_direction_output(vcore_mode, 0);

	gpio_free(vcore_mode);
}

//static int lcd_brightness = 255;

static int set_lcd_power(int on)
{
	int lcd_pwr_en;
	//int lcd_bl_pwr_en;
	int ret=-1;
	/*
	static int lcdpower_enabled=0;

	if(lcdpower_enabled==on)
		return 0;
		*/

	/*
	// Ellie: don't turn on the power if requested brightness level is 0, see PNX-192
	if((lcd_brightness==255)&&on)
	{
		printk("set_lcd_power: not turning on the power as brightness level is 0.\n");
		goto out;
	}*/

	lcd_pwr_en = GPIO(120);

	if (gpio_request(lcd_pwr_en, "lcd_pwr_en")) {
		printk(KERN_INFO "gpio %d request failed\n",
						lcd_pwr_en);
		goto out;
	}
	
	/*
	lcd_bl_pwr_en = GPIO(116);

	if (gpio_request(lcd_bl_pwr_en, "lcd_bl_pwr_en")) {
		printk(KERN_INFO "gpio %d request failed\n",
						lcd_bl_pwr_en);
		goto out1;
	}
	*/
	if (on) {
		vcore_mode_set(1);
		/* re-config lcd_lr/ud pin to enable lcd */
		printk("set lcd power on...\n");
		gpio_direction_output(lcd_pwr_en, 0);/*active low*/
		//gpio_direction_output(lcd_bl_pwr_en, 0);/*active low*/
	} else {
		printk("set lcd power off...\n");

		/* config lcd_lr/ud pin to gpio for power optimization */
		gpio_direction_output(lcd_pwr_en, 1);/*active low*/
		//gpio_direction_output(lcd_bl_pwr_en, 1);/*active low*/
		vcore_mode_set(0);
	}
	ret=0;
	//lcdpower_enabled=on;

	//gpio_free(lcd_bl_pwr_en);
//out1:
	gpio_free(lcd_pwr_en);	
out:
	return ret;
}

#ifdef CONFIG_FB_PXA168   
int pwm_backlight_notify(struct device *device, int brightness)
{		
	#if 0
	lcd_brightness=brightness;
	if(255!=brightness)
	{	
		set_lcd_power(1);
	}
	else if(255==brightness)
	{
		set_lcd_power(0);
	}
	return brightness;
	#else
	int lcd_bl_pwr_en;


	lcd_bl_pwr_en = GPIO(116);
	if (gpio_request(lcd_bl_pwr_en, "lcd_bl_pwr_en")) {
	printk(KERN_INFO "gpio %d request failed\n", lcd_bl_pwr_en);
	goto out;
	}

	#ifdef LCD_BACKLIGHT_INVERTED
	if(brightness != 255)
	#else
	if(brightness != 0)	
	#endif
	{
		if(gpio_get_value(lcd_bl_pwr_en))
		{		
			//msleep(50); // Ellie: wait some time before turning on the power, see PNX-192 and PNX-266
			gpio_direction_output(lcd_bl_pwr_en, 0);
			printk(KERN_INFO "turn on g50 backlight\n");
		}
	}else
	{
		if(!gpio_get_value(lcd_bl_pwr_en))
		{
			gpio_direction_output(lcd_bl_pwr_en, 1);
			printk(KERN_INFO "turn off g50 backlight\n");
		}
	}

	gpio_free(lcd_bl_pwr_en);

out:
	return brightness;
	
	#endif
}

static struct platform_pwm_backlight_data g50_lcd_backlight_data = {
                .pwm_id         = 2,
                .max_brightness = 255,  /* duty cycle 60% at most */
                .pwm_period_ns  = 2000000/*1250000->800Hz,2000000->500Hz,4000000->250Hz*/,  
                .notify         = pwm_backlight_notify,
                #ifdef LCD_BACKLIGHT_INVERTED
				.dft_brightness = 102,
                .quirks			= PWM_BACKLIGHT_INVERTED,
                #else
				.dft_brightness = 153,				
				#endif
};

static struct platform_device g50_lcd_backlight_devices = {
                .name           = "pwm-backlight",
                .id             = 0,
                .dev            = {
                        .platform_data = &g50_lcd_backlight_data,
                },
};

#if defined(CONFIG_PHOENIX)
#define FB_XRES		800
#define FB_YRES		600
static struct fb_videomode video_modes[] = {
	[0] = {
		.refresh	= 60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 2,
		.left_margin	= 46,
		.right_margin	= 30,
		.vsync_len	= 2,
		.upper_margin	= 23,
		.lower_margin	= 5,
		.sync		= 0,
	},
};
#endif

#if defined(CONFIG_BUTTERFLY)
#define FB_XRES		1024
#define FB_YRES		600
static struct fb_videomode video_modes[] = {
	[0] = {
		.refresh	= 30,//60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 2,
		.left_margin	= 45,
		.right_margin	= 43,
		.vsync_len	= 2,
		.upper_margin	= 5,
		.lower_margin	= 3,
		.sync		= FB_SYNC_HOR_HIGH_ACT,
	},
};
#endif

#if defined(CONFIG_DRAGONFLY)
#define FB_XRES		800
#define FB_YRES		480
static struct fb_videomode video_modes[] = {
	[0] = {
		.refresh	= 60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 2,
		.left_margin	= 46,
		.right_margin	= 30,
		.vsync_len	= 2,
		.upper_margin	= 23,
		.lower_margin	= 5,
		.sync		= FB_SYNC_HOR_HIGH_ACT,
	},
};
#endif

static int g50_lcd_power(struct pxa168fb_info *fbi,
	unsigned int spi_gpio_cs, unsigned int spi_gpio_reset, int on)
{
	return set_lcd_power(on);
}

static struct pxa168fb_mach_info mmp2_parallel_lcd_info __initdata = {
	.id			= "GFX Layer",
	.sclk_src		= 400000000,	/* 400MHz for ar0922e lpddr1 + emmc*/	
	#if defined(CONFIG_BUTTERFLY)
	.sclk_div		= 0x40001113,
	#else
	.sclk_div		= 0x4000110c,
	#endif
	.num_modes		= ARRAY_SIZE(video_modes),
	.modes			= video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.dumb_mode           =6,  /*RGB888*/
	.isr_clear_mask	= LCD_ISR_CLEAR_MASK_PXA168,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the panel is hard connected with lcd panel path and dsi1 output
	 */
	.io_pad_ctrl = CFG_CYC_BURST_LEN16,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.active			= 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.mmap			= 1,
#ifdef CONFIG_TRIPLE_BUFFERS
	.max_fb_size		= FB_XRES * FB_YRES * 12 + 4096,
#else
	.max_fb_size		= FB_XRES * FB_YRES * 8 + 4096,
#endif
	.vdma_enable		= 1,
	.sram_paddr		= 0,
	.sram_size		= 30 * 1024,
	.immid			= 0,
	.phy_type		= DPI,
	.pxa168fb_lcd_power     = &g50_lcd_power,
#ifdef CONFIG_PXA688_CMU
	.cmu_cal = {{-1, 47, 2, 2},
		{44, 92, 25, 25},
		{0, 0, 0, 0}
	},
	.cmu_cal_letter_box = {{48, 47, 2, 2},
		{93, 92, 25, 25},
		{0, 0, 0, 0}
	},
	.ioctl			= pxa688_cmu_ioctl,
#endif
};

static struct pxa168fb_mach_info mmp2_parallel_lcd_ovly_info __initdata = {
	.id			= "Video Layer",
	.num_modes		= ARRAY_SIZE(video_modes),
	.modes			= video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.dumb_mode			 =6,  /*RGB888*/
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.mmap			= 0,
#ifdef CONFIG_TRIPLE_BUFFERS
	.max_fb_size            = FB_XRES * FB_YRES * 12 + 4096,
#else
	.max_fb_size            = FB_XRES * FB_YRES * 8 + 4096,
#endif
	.vdma_enable		= 0,
	.sram_paddr		= 0,
	.sram_size		= 30 * 1024,
	.immid			= 0,
};
#endif

#ifdef CONFIG_FB_SECOND_PANEL_HDMI

static struct pxa168fb_mach_info mmp2_tv_hdmi_info __initdata = {
	.id			= "GFX Layer - TV",
	.sclk_div		= 0x5 | (1<<16) | (3<<30), /* HDMI PLL */
	.num_modes		= ARRAY_SIZE(tv_video_modes),
	.modes			= tv_video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.isr_clear_mask	= LCD_ISR_CLEAR_MASK_PXA168,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the hdmi monitor is hard connected with lcd tv path and hdmi output
	 */
	.io_pad_ctrl = CFG_CYC_BURST_LEN16,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 1,
	.active			= 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.mmap			= 1,  /* Ellie changed to 1 to fix the logo blink */
	.max_fb_size		= 1920 * 1080 * 8 + 4096,
	.vdma_enable		= 1,
	.sram_paddr		= 0,
	.sram_size		= 60 * 1024,
	.immid			= 0,
#ifdef CONFIG_PXA688_CMU
	.ioctl			= pxa688_cmu_ioctl,
#endif
};

static struct pxa168fb_mach_info mmp2_tv_hdmi_ovly_info __initdata = {
	.id			= "Video Layer - TV",
	.num_modes		= ARRAY_SIZE(tv_video_modes),
	.modes			= tv_video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.mmap			= 0,
	.max_fb_size            = 1920 * 1080 * 8 + 4096,
	.vdma_enable		= 0,
	.sram_paddr		= 0,
	.sram_size		= 60 * 1024,
	.immid			= 0,
};
#endif

static struct uio_hdmi_platform_data mmp2_hdmi_info __initdata = {
	.sspa_reg_base = 0xD42A0C00,
	.gpio = GPIO(34),
};

static void __init mmp2_add_lcd(void)
{
	unsigned char __iomem *dmc_membase;
	unsigned int CSn_NO_COL;

	dmc_membase = ioremap(DDR_MEM_CTRL_BASE, 0xfff);
	CSn_NO_COL = __raw_readl(dmc_membase + SDRAM_CONFIG0_TYPE1) >> 4;
	CSn_NO_COL &= 0xf;
	if (CSn_NO_COL <= 0x2) {
		/*
		*if DDR page size < 4KB, select no crossing 1KB boundary check
		*/
		mmp2_parallel_lcd_info.io_pad_ctrl |= CFG_BOUNDARY_1KB;
		mmp2_tv_hdmi_info.io_pad_ctrl |= CFG_BOUNDARY_1KB;
	}
	iounmap(dmc_membase);

#ifdef CONFIG_FB_PXA168
	/* lcd */
	vdma_switch = 1;
	mmp2_add_fb(&mmp2_parallel_lcd_info);
	mmp2_add_fb_ovly(&mmp2_parallel_lcd_ovly_info);

#ifdef CONFIG_FB_SECOND_PANEL_HDMI
	mmp2_add_fb_tv(&mmp2_tv_hdmi_info);
	mmp2_add_fb_tv_ovly(&mmp2_tv_hdmi_ovly_info);
#endif
#endif

#ifdef CONFIG_PXA688_MISC
	/* set TV path vertical smooth, panel2 as filter channel,
	 * vertical smooth is disabled by default to avoid underrun
	 * when video playback, to enable/disable graphics/video
	 * layer vertical smooth:
	 * echo g0/g1/v0/v1 > /sys/deivces/platform/pxa168-fb.1/misc
	 */
	fb_vsmooth = 1; fb_filter = 2;
#endif
}



#if defined(CONFIG_SWITCH_HEADSET_HOST_GPIO)
static struct gpio_switch_platform_data headset_switch_device_data = {
	.name = "h2w",
	.gpio = GPIO(125),
	.name_on = NULL,
	.name_off = NULL,
	.state_on = NULL,
	.state_off = NULL,
	.debounce_ms = 200,
};

static struct platform_device headset_switch_device = {
	.name            = "headset",
	.id              = 0,
	.dev             = {
		.platform_data = &headset_switch_device_data,
	},
};

static void __init g50_init_headset(void)
{
	int gpio = headset_switch_device_data.gpio;

	if (gpio_request(gpio, "headset detect irq")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio);
		return;
	}

	gpio_direction_input(gpio);
	mdelay(1);
	gpio_free(gpio);
	platform_device_register(&headset_switch_device);
}
#endif


#ifdef CONFIG_PXA688_CAMERA

static int cam_power_set(int flag)
{
	struct clk *gate_clk = NULL;
	struct clk *dbg_clk = NULL;

	gate_clk = clk_get(NULL, "CCICGATECLK");
	if (IS_ERR(gate_clk)) {
		printk(KERN_ERR "unable to get CCICGATECLK");
		return PTR_ERR(gate_clk);
	}

	dbg_clk = clk_get(NULL, "CCICDBGCLK");
	if (IS_ERR(dbg_clk)) {
		printk(KERN_ERR "unable to get CCICDBGCLK");
		return PTR_ERR(dbg_clk);
	}

	if (flag) {
		clk_enable(gate_clk);
		clk_enable(dbg_clk);
	} else {
		clk_disable(gate_clk);
		clk_disable(dbg_clk);
	}

	clk_put(gate_clk);
	clk_put(dbg_clk);

	return 0;
}

static int cam_pmua_set(int flag)
{
	struct clk *rst_clk = NULL;

	rst_clk = clk_get(NULL, "CCICRSTCLK");
	if (IS_ERR(rst_clk)) {
		printk(KERN_ERR "unable to get CCICRSTCLK");
		return PTR_ERR(rst_clk);
	}

	if (flag)
		clk_enable(rst_clk);
	else
		clk_disable(rst_clk);

	clk_put(rst_clk);

	return 0;
}

/*
 * FLAG, 1: ON, 0: OFF
 * RES, 0, LOW RESOLUTION, 1 HIGH RESOLUTION
 * ECO, 1: CAMERA_ECO_ON, 0: CAMERA_ECO_OFF
 * SENSOR, 1: SUCH AS OVT CAMERA SENSORS, 0: SUCH AS GCT CAMERA SENSORS
 */

static int sensor_power_set(int flag, int res, int eco, int sensor)
{
	int cam_pdn_high = GPIO(106);
	int cam_pdn_low = GPIO(107);
	int cam_pwr_en = GPIO(137);

	//printk("Set %s sensor power %s\n", res?"high":"low", flag?"on":"off");
	cam_power_set(flag);

	if (res == SENSOR_LOW)
	{
		if (gpio_request(cam_pwr_en, "CAM_ENABLE_LOW_SENSOR")) {
			printk(KERN_ERR "Request GPIO failed,"
					"gpio: %d \n", cam_pwr_en);
			return -EIO;
		}

		if (gpio_request(cam_pdn_low, "CAM_ENABLE_LOW_SENSOR")) {
			printk(KERN_ERR "Request GPIO failed,"
					"gpio: %d \n", cam_pdn_low);
			return -EIO;
		}

		if (flag)
		{
			gpio_direction_output(cam_pwr_en, 1);
			mdelay(5);
			gpio_direction_output(cam_pdn_low, 0);
		}
		else
		{
			gpio_direction_output(cam_pwr_en, 0);
			gpio_direction_output(cam_pdn_low, 1);
		}

		gpio_free(cam_pdn_low);
		gpio_free(cam_pwr_en);
	}
	else
	{
		if (gpio_request(cam_pwr_en, "CAM_ENABLE_HIGH_SENSOR")) {
			printk(KERN_ERR "Request GPIO failed,"
					"gpio: %d \n", cam_pwr_en);
			return -EIO;
		}

		if (gpio_request(cam_pdn_high, "CAM_ENABLE_HIGH_SENSOR")) {
			printk(KERN_ERR "Request GPIO failed,"
					"gpio: %d \n", cam_pdn_high);
			return -EIO;
		}

		if (flag)
		{
			gpio_direction_output(cam_pwr_en, 1);
			mdelay(5);
			gpio_direction_output(cam_pdn_high, 0);
		}
		else
		{
			gpio_direction_output(cam_pwr_en, 0);
			gpio_direction_output(cam_pdn_high, 1);
		}

		gpio_free(cam_pdn_high);
		gpio_free(cam_pwr_en);
	}
    
	return 0;
}

static struct sensor_platform_data camera_sensor_data_low = {
	.id = SENSOR_LOW,
	.power_set = sensor_power_set,
};

static struct sensor_platform_data camera_sensor_data_high = {
	.id = SENSOR_HIGH,
	.power_set = sensor_power_set,
};

/* sensor init over */
static struct cam_platform_data cam_ops = {
	.power_set      = cam_power_set,
	.pmua_set	= cam_pmua_set,
};

static int __init g50_init_cam(void)
{
	cam_pmua_set(1);
	mmp2_add_cam(1, &cam_ops);
	return 0;
}

/* sensor init over */
#endif


#if defined(CONFIG_MMC_SDHCI_PXA)
/* MMC0 controller for SD-MMC */
static struct sdhci_pxa_platdata mmp2_sdh_platdata_mmc0 = {
	.clk_delay_cycles	= 0x1f,
	.soc_set_timing = mmp2_init_sdh,
	.flags		= PXA_FLAG_CONTROL_CLK_GATE,
};

/*MMC1 controller for Wifi*/


static void mmc1_sdio_switch(unsigned int on, int with_card)
{
	uint32_t icu_int_conf, mfpr;
	uint32_t addr = APB_VIRT_BASE + 0x1e000 + 0xf0;

	if (!with_card)
		return;
	if (on) {
		/* enable I/O edge detection interrupt */
		icu_int_conf = __raw_readl(ICU_INT_CONF(23));
		__raw_writel(icu_int_conf | ICU_INT_ROUTE_PJ4_IRQ, ICU_INT_CONF(23));
	} else {
		/* disable I/O edge detection interrupt */
		icu_int_conf = __raw_readl(ICU_INT_CONF(23));
		__raw_writel(icu_int_conf & (~(ICU_INT_ROUTE_PJ4_IRQ)), ICU_INT_CONF(23));
		/* sdio function pin edge clear */
		mfpr = __raw_readl(addr);
		__raw_writel(mfpr | (1 << 6), addr);
		__raw_writel(mfpr, addr);
	}
}

static struct sdhci_pxa_platdata mmp2_sdh_platdata_mmc1 = {
	.flags		= PXA_FLAG_DISABLE_CLOCK_GATING | PXA_FLAG_CARD_PERMANENT
				| PXA_FLAG_SDIO_RESUME,
	.lp_switch	= mmc1_sdio_switch,
	.soc_set_timing = mmp2_init_sdh,
};


static struct sdhci_pxa_platdata mmp2_sdh_platdata_mmc2 = {
	.clk_delay_cycles	= 0x1f,
	.flags		= PXA_FLAG_CARD_PERMANENT | PXA_FLAG_CONTROL_CLK_GATE,
	.soc_set_timing = mmp2_init_sdh,
};

static void mmc1_set_power(unsigned int on)
{
	mfp_cfg_t mfp_cfg_power_on = MFP_CFG_LPM_DRV_LOW(GPIO105, AF1);
	mfp_cfg_t mfp_cfg_reset_on = MFP_CFG_LPM_DRV_HIGH(GPIO58, AF0);
	mfp_cfg_t mfp_cfg_power_off = MFP_CFG_LPM_DRV_HIGH(GPIO105, AF1);
	mfp_cfg_t mfp_cfg_reset_off = MFP_CFG_LPM_DRV_LOW(GPIO58, AF0);
	if (on) {
		mfp_config(&mfp_cfg_power_on, 1);
		mfp_config(&mfp_cfg_reset_on, 1);
	} else {
		mfp_config(&mfp_cfg_power_off, 1);
		mfp_config(&mfp_cfg_reset_off, 1);
	}
	
}

static void __init g50_init_mmc(void)
{
#ifdef CONFIG_SD8XXX_RFKILL
	int WIB_PD;
	int WIB_RESETn;

	WIB_PD = GPIO(105);
	WIB_RESETn = GPIO(58);
	
	add_sd8x_rfkill_device(WIB_PD, WIB_RESETn,
			&mmp2_sdh_platdata_mmc1.pmmc, &mmc1_set_power);
#endif
	/*eMMC (MMC3) pins are conflict with NAND*/
	mmp2_add_sdh(2, &mmp2_sdh_platdata_mmc2); /*eMMC*/
	mmp2_add_sdh(0, &mmp2_sdh_platdata_mmc0); /*SD/MMC*/
	mmp2_add_sdh(1, &mmp2_sdh_platdata_mmc1); /*Wifi*/
}

#endif


#include "board_usb.c"
#include "board_i2c.c"
#include "board_gps.c"
#include "board_vibrator.c"
#include "board_panel.c"
#include "board_android.c"
#include "board_pm.c"
#include "board_modem.c"
#include "board_pstn.c"

static void __init board_gpio_init(void)
{
	mfp_config(ARRAY_AND_SIZE(g50_pin_config));
}

static struct platform_device skm = 
{
	.name 	= "skm",
	.id 	= -1,
};

static struct platform_device* misc_devices[] __initdata = 
{
	&skm
};

static void __init board_misc_init(void)
{
	platform_add_devices(misc_devices, ARRAY_SIZE(misc_devices));
}

//
//One-wire
//
static struct mmp2_w1_platform_data w1_data = 
{
	.bus_shift		= 2,
	.enable 		= NULL,
	.disable		= NULL,
	.gpio = MFP_PIN_GPIO98,
};
static void __init board_security_init(void)
{
	mmp2_add_w1(&w1_data);	
}

static struct vmeta_plat_data mmp2_vmeta_pdata = {
	.set_dvfm_constraint = mmp2_vmeta_set_dvfm_constraint,
	.unset_dvfm_constraint = mmp2_vmeta_unset_dvfm_constraint,
	.init_dvfm_constraint = mmp2_vmeta_init_dvfm_constraint,
	.clean_dvfm_constraint = mmp2_vmeta_clean_dvfm_constraint,
	.decrease_core_freq = mmp2_vmeta_decrease_core_freq,
	.increase_core_freq = mmp2_vmeta_increase_core_freq,
};

int g50_get_board_version(void){
#if 0
	int gpio_vers0 = mfp_to_gpio(GPIO119_GPIO119);
	int gpio_vers1 = mfp_to_gpio(GPIO120_GPIO120);

	if (gpio_request(gpio_vers0, "vers0")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio_vers0);
		return -1;
	}

	if (gpio_request(gpio_vers1, "vers1")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio_vers1);
		gpio_free(gpio_vers0);
		return -1;
	}

	gpio_direction_input(gpio_vers0);
	gpio_direction_input(gpio_vers1);

	mmp2_platform_version = ((!!gpio_get_value(gpio_vers1))<<1) |
		((!!gpio_get_value(gpio_vers0))<<0);

	gpio_free(gpio_vers0);
	gpio_free(gpio_vers1);
#endif
	//since we don't support hw version detection,hard code here
	mmp2_platform_version = 0x3;//for g50 hw rev2

	return 0;
}

static void __init g50_init(void)
{
	extern size_t reserving_size;
	board_gpio_init();
	g50_get_board_version();

	vcore_mode_set(1);

	/* on-chip devices */
	mmp2_add_uart(1);
	mmp2_add_uart(2);
	mmp2_add_uart(3);
	mmp2_add_uart(4);
	mmp2_add_rtc();
	board_i2c_init();
	mmp2_add_lcd();

	printk("board revision: %s\n",board_is_mmp2_g50_rev2()?"rev2":
		board_is_mmp2_g50_rev3()?"rev3":"unknown");
	/* backlight */
	//FIXME:for CPU A1 workaround,we also enable pwm3 to let pwm4 does work
	//So add a dummy backlight device to open pwm3 and enable .
	mmp2_add_pwm(3);
	platform_device_register(&g50_lcd_backlight_devices);

	board_security_init();

	mmp2_add_imm();
	mmp2_add_sspa(1);
	//mmp2_add_sspa(2);
	mmp2_add_audiosram();
	mmp2_add_vmeta(&mmp2_vmeta_pdata);
	mmp2_add_thermal_sensor();

	mmp2_add_fuse();
	board_panel_init();

	mmp2_add_hdmi(&mmp2_hdmi_info);
#ifdef CONFIG_PXA688_CAMERA
     g50_init_cam();
#endif

#ifdef CONFIG_SWITCH_HEADSET_HOST_GPIO
	g50_init_headset();
#endif

#ifdef CONFIG_ANDROID_PMEM
	pxa_add_pmem("pmem", reserving_size, PMEM_ALLOCATOR_BUDDY, 1, 1);
	pxa_add_pmem("pmem_adsp", 0, 0, 0, 0);
	pxa_add_pmem("pmem_wc", 0, 0, 0, 1);
#endif

#ifdef CONFIG_DVFM_MMP2
	mmp2_add_freq();
#endif

#if defined(CONFIG_MMC_SDHCI_PXA)
	g50_init_mmc();
#endif
	
	#ifdef CONFIG_PXA_VBUS
	platform_device_register(&vbus_device);
	#endif
	#ifdef CONFIG_USB_PXA_U2O
	platform_device_register(&u2o_device);
	#endif
	#ifdef CONFIG_USB_OTG
	platform_device_register(&otg_device);
	platform_device_register(&ehci_u2o_device);
	#endif
	/* register PWM[2] for ulpi phy input clock:13M */
	mmp2_add_pwm(2);
	/* init usb ulpi phy (fsic) */
	g50_ehci_u2h_fsic_init();
	board_gps_init();

	board_misc_init();

	#ifdef CONFIG_MMP_ZSP
	if (cpu_is_mmp2() && !cpu_is_mmp2_z0() && !cpu_is_mmp2_z1()) {
		pxa_register_device(&mmp2_device_zsp, NULL, 0);
	}
	#endif
	
	board_vibrator_init();

	board_pm_init();

}

MACHINE_START(G50, "G50 Development Platform")
	.phys_io	= APB_PHYS_BASE,
	.boot_params	= 0x00000100,
	.io_pg_offst	= (APB_VIRT_BASE >> 18) & 0xfffc,
	.map_io		= pxa_map_io,
	.nr_irqs	= G50_NR_IRQS,
	.init_irq	= mmp2_init_irq,
	.timer          = &mmp2_timer,
	.init_machine   = g50_init,
MACHINE_END
