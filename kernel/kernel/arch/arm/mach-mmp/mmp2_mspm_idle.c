/*
 * PXA910 MSPM IDLE
 *
 * Copyright (c) 2003 Intel Corporation.
 *
 * This software program is licensed subject to the GNU General Public License
 * (GPL).Version 2,June 1991, available at http://www.fsf.org/copyleft/gpl.html
 *
 * (C) Copyright 2008 Marvell International Ltd.
 * All Rights Reserved
 */

/*
#undef DEBUG
#define DEBUG
*/
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/spinlock.h>

#include <asm/proc-fns.h>
#include <asm/ptrace.h>
#include <asm/mach/time.h>

#include <mach/cputype.h>
#include <mach/hardware.h>
#include <mach/dvfm.h>
#include <mach/mspm_prof.h>
#include <mach/pxa168_pm.h>
#include <mach/mmp2_pm.h>
#include <mach/mmp2_dvfm.h>
#include <mach/regs-timers.h>

#define ENABLE_IDLE_STATS 0
static int core_intidle_opidx = -1, core_extidle_opidx = -1,apps_idle_opidx = -1, apps_sleep_opidx = -1, chip_sleep_opidx = -1, sys_sleep_opidx = -1;

#define LOWPOWER_CORE_INTIDLE_THRE 0
#define LOWPOWER_CORE_EXTIDLE_THRE 0
#define LOWPOWER_APPS_IDLE_THRE 1
#define LOWPOWER_APPS_SLEEP_THRE 2
#define LOWPOWER_CHIP_SLEEP_THRE 2
#define LOWPOWER_SYS_SLEEP_THRE 3
#define IDLE_STATS_NUM 1000

#if ENABLE_IDLE_STATS
struct idle_stats {
	unsigned int	index;
	unsigned int	ticks;
};

struct mspm_idle_stats {
	struct idle_stats	stats[IDLE_STATS_NUM];
	unsigned int		stats_index;
	spinlock_t		lock;
};

static struct mspm_idle_stats mspm_stats = {
	.stats_index	= 0,
	.lock		= SPIN_LOCK_UNLOCKED,
};
#endif
static void (*orig_idle)(void);

static void mspm_cpu_idle(int sys_lowpower)
{
	struct op_info *info = NULL;
	int op;

	op = dvfm_get_op(&info);
	mspm_add_event(op, CPU_STATE_RUN);
	if (sys_lowpower)
		mmp2_cpu_do_idle();
	else
		cpu_do_idle();
	mspm_add_event(op, CPU_STATE_IDLE);
}

#if ENABLE_IDLE_STATS

/* Collect statistic information before entering idle */
static void record_idle_stats(void)
{
	int i;

	spin_lock(&mspm_stats.lock);
	if (++mspm_stats.stats_index == IDLE_STATS_NUM)
		mspm_stats.stats_index = 0;
	i = mspm_stats.stats_index;
	memset(&mspm_stats.stats[i], 0, sizeof(struct idle_stats));
	mspm_stats.stats[i].index = i;
	mspm_stats.stats[i].ticks = read_timer();
	spin_unlock(&mspm_stats.lock);
}

/* Collect statistic information after exiting idle.
 */
static void update_idle_stats(void)
{
	int i;

	spin_lock(&mspm_stats.lock);
	i = mspm_stats.stats_index;
	mspm_stats.stats[i].ticks = read_timer()
		- mspm_stats.stats[i].ticks;
	spin_unlock(&mspm_stats.lock);
}

#endif

void set_idle_op(int idx, int mode)
{
	switch (mode) {
	case POWER_MODE_CORE_INTIDLE:
		core_intidle_opidx = idx;
		break;
	case POWER_MODE_CORE_EXTIDLE:
		core_extidle_opidx = idx;
		break;
	case POWER_MODE_APPS_IDLE:
		apps_idle_opidx = idx;
		break;
	case POWER_MODE_APPS_SLEEP:
		apps_sleep_opidx = idx;
		break;
	case POWER_MODE_CHIP_SLEEP:
		chip_sleep_opidx = idx;
		break;
	case POWER_MODE_SYS_SLEEP:
		sys_sleep_opidx = idx;
		break;
	}
}


static int lpidle_is_valid(int enable, unsigned int msec,
			struct dvfm_freqs *freqs, int lp_idle)
{
	struct op_info *info = NULL;
	struct mmp2_md_opt *op;
	int prev_op;
	int ret;
	int threshold;
	int new_op = 0;

	if (freqs == NULL) 
		return 0;

	if (enable & lp_idle) {
		switch (lp_idle) {
		case IDLE_CORE_INTIDLE:
			if (core_intidle_opidx == -1)
				return 0;
			threshold = LOWPOWER_CORE_INTIDLE_THRE;
			break;
		case IDLE_CORE_EXTIDLE:
			if (core_extidle_opidx == -1)
				return 0;
			threshold = LOWPOWER_CORE_EXTIDLE_THRE;
			break;
		case IDLE_APPS_IDLE:
			if (apps_idle_opidx == -1)
				return 0;
			threshold = LOWPOWER_APPS_IDLE_THRE;
			break;
		case IDLE_APPS_SLEEP:
			if (apps_sleep_opidx == -1)
				return 0;
			threshold = LOWPOWER_APPS_SLEEP_THRE;
			break;
		case IDLE_CHIP_SLEEP:
			if (chip_sleep_opidx == -1)
				return 0;
			threshold = LOWPOWER_CHIP_SLEEP_THRE;
			break;
		case IDLE_SYS_SLEEP:
			if (sys_sleep_opidx == -1)
				return 0;
			threshold = LOWPOWER_SYS_SLEEP_THRE;
			break;
		default:
			return 0;
		}

		/* Check dynamic tick flag && idle interval */
		if (msec < threshold)
			return 0;

		/* Check whether the specified low power mode is valid */
		ret = dvfm_get_op(&info);
		if (info == NULL)
			return 0;

		op = (struct mmp2_md_opt *)info->op;
		if (op == NULL)
			return 0;

		if ((ret >= 0) && (op->power_mode == POWER_MODE_ACTIVE)) {
			prev_op = ret;
			freqs->old = ret;
			new_op = lp_idle == IDLE_CORE_EXTIDLE \
				? core_extidle_opidx :
				lp_idle == IDLE_APPS_IDLE ? apps_idle_opidx :
				lp_idle == IDLE_APPS_SLEEP ? apps_sleep_opidx :
				lp_idle == IDLE_CHIP_SLEEP ? chip_sleep_opidx :
				lp_idle == IDLE_SYS_SLEEP ? sys_sleep_opidx :
				-1;
			/* freqs->new is a unsigned int
			 * in order for negative case, use
			 * a signed int first.
			 */
			if (new_op < 0)
				return 0;
			freqs->new = new_op;

			ret = dvfm_get_opinfo(freqs->new, &info);
			if ((ret >= 0) && find_first_bit(info->device, \
					DVFM_MAX_CLIENT) == DVFM_MAX_CLIENT) {
				return 1;
			}
		}
	}
	return 0;
}

/*
 * IDLE Thread
 */

unsigned int get_remain_slice(void)
{
	uint32_t val1 = 0;
	uint32_t val2 = 0;
#ifdef CONFIG_PXA_32KTIMER
	val1 = __raw_readl(TIMERS1_VIRT_BASE + TMR_TN_MM(1, 0));
#else
	val1 = __raw_readl(TIMERS1_VIRT_BASE + TMR_TN_MM(0, 0));
#endif
	val2 = read_timer();
	return (val1-val2)*1000/CLOCK_TICK_RATE;
}


void mspm_do_idle(void)
{
	struct dvfm_freqs freqs;
	int ret = -EINVAL;
	unsigned int msec;
	int sys_lowpower = 0;

	local_irq_disable();
	if (!need_resched()) {
		msec = get_remain_slice();		
		#if ENABLE_IDLE_STATS
		record_idle_stats();
		#endif
		//printk(KERN_ERR "remain slice: %d\n", msec);
		if (lpidle_is_valid(enable_deepidle, msec, &freqs, \
				IDLE_SYS_SLEEP)) {
			ret = dvfm_set_op(&freqs, freqs.new,
					RELATION_STICK);
			if (ret == 0) {
				sys_lowpower = 1;
				goto out;
			}
		}
		if (lpidle_is_valid(enable_deepidle, msec, &freqs, \
				IDLE_CHIP_SLEEP)) {
			ret = dvfm_set_op(&freqs, freqs.new,
					RELATION_STICK);
			if (ret == 0) {
				sys_lowpower = 1;
				goto out;
			}
		}
		if (lpidle_is_valid(enable_deepidle, msec, &freqs, \
				IDLE_APPS_SLEEP)) {
			ret = dvfm_set_op(&freqs, freqs.new,
					RELATION_STICK);
			if (ret == 0) {
				sys_lowpower = 1;
				goto out;
			}
		}
		if (lpidle_is_valid(enable_deepidle, msec, &freqs, \
				IDLE_APPS_IDLE)) {
			ret = dvfm_set_op(&freqs, freqs.new,
					RELATION_STICK);
			if (ret == 0) {
				sys_lowpower = 1;
				goto out;
			}
		}
		if (enable_deepidle & IDLE_CORE_EXTIDLE) {
			//printk(KERN_ERR "enter lowpower mode\n");
			mmp2_pm_enter_lowpower_mode(POWER_MODE_CORE_EXTIDLE);
		}
out:
		mspm_cpu_idle(sys_lowpower);
		
		#if ENABLE_IDLE_STATS
		update_idle_stats();
		#endif
	}
	local_irq_enable();
}

#if ENABLE_IDLE_STATS

static struct proc_dir_entry *entry_dir;
static struct proc_dir_entry *entry_stats;

static int stats_show(struct seq_file *s, void *v)
{
	struct idle_stats *p = NULL;
	int i, ui;
	unsigned long flags;

	spin_lock_irqsave(&mspm_stats.lock, flags);
	ui = mspm_stats.stats_index;
	for (i = 0; i < IDLE_STATS_NUM; i++) {
		p = &mspm_stats.stats[ui++];
		seq_printf(s, "index:%u idle_ticks:%u\n",
				p->index, p->ticks);
		if (ui == IDLE_STATS_NUM)
			ui = 0;
	}
	spin_unlock_irqrestore(&mspm_stats.lock, flags);
	return 0;
}

static int stats_seq_open(struct inode *inode, struct file *file)
{
	return single_open(file, &stats_show, NULL);
}

static struct file_operations stats_seq_ops = {
	.owner		= THIS_MODULE,
	.open		= stats_seq_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};
#endif
static int mspm_proc_init(void)
{
#if ENABLE_IDLE_STATS

	entry_dir = proc_mkdir("driver/mspm", NULL);
	if (entry_dir == NULL)
		return -ENOMEM;

	entry_stats = create_proc_entry("stats", 0, entry_dir);
	if (entry_stats)
		entry_stats->proc_fops = &stats_seq_ops;
#endif	
	return 0;
}

static void mspm_proc_cleanup(void)
{
#if ENABLE_IDLE_STATS
	remove_proc_entry("stats", entry_dir);
	remove_proc_entry("driver/mspm", NULL);
#endif	
}

void mspm_idle_load(void)
{
	if (pm_idle != mspm_do_idle) {
		orig_idle = pm_idle;
		pm_idle = mspm_do_idle;
	}
}

void mspm_idle_clean(void)
{
	if (pm_idle == mspm_do_idle)
		pm_idle = orig_idle;
}

static int __init mspm_init(void)
{

	/* Create file in procfs */
	if (mspm_proc_init())
		return -EFAULT;

	mspm_prof_init();

	pr_info("Initialize MMP2 MSPM\n");

	return 0;
}

static void __exit mspm_exit(void)
{
	/* Remove procfs */
	mspm_proc_cleanup();

	mspm_prof_exit();

	pr_info("Quit MMP2 MSPM\n");
}

module_init(mspm_init);
module_exit(mspm_exit);

MODULE_DESCRIPTION("PXA910_MSPM");
MODULE_LICENSE("GPL");
