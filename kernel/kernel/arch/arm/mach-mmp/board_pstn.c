
#undef PSTN_MODEM_ENABLED

#ifdef CONFIG_DRAGONFLY
#define PSTN_MODEM_ENABLED
#endif

#ifdef PSTN_MODEM_ENABLED
#include <linux/workqueue.h>
#include <linux/input.h>

#undef MODEM_WAKEUP_ENABLED


struct pstn_modem;

struct pstn_modem_work
{
	struct delayed_work work;
	void (*work_callback)(struct pstn_modem* modem);	
};

struct pstn_modem
{
	struct pstn_modem_work command_work;
	int command;

	int gpio_handlestate;
	int gpio_hookstate;
	int gpio_hostwake;



	struct input_dev	*idev;
	int link_state_change;
	int screen_state;
	
};

struct pstn_modem_plat_data
{
	struct pstn_modem* modem;
};



static int gpio_get_pin(int gpio)
{
	int ret=-1;
	if(gpio>=0)
	{
		#if 0
		int error=gpio_request(gpio, "pstn pin");
		if(error)
		{			
			pr_warning("request pstn pin %d failed\n",gpio);
		}
		else
		{
			ret = gpio_get_value(gpio);
			if(ret>0) ret=1;
			gpio_free(gpio);
		}
		#else
		ret = gpio_get_value(gpio);
		if(ret>0) ret=1;
		#endif
		
	}
	return ret;
}

static int gpio_set_pin(int gpio,int val)
{
	int ret=-1;
	if(gpio>=0)
	{
		#if 0
		int error;
		error=gpio_request(gpio, "pstn pin");
		if(error)
		{			
			pr_warning("request pstn pin %d failed\n",gpio);
		}
		else
		{
			gpio_direction_output(gpio,val);
			gpio_free(gpio);
			ret = 0;
		}
		#else
		gpio_direction_output(gpio,val);
		ret = 0;		
		#endif
		
	}
	return ret;
}



static ssize_t pstn_modem_show(struct device *dev,
                            struct device_attribute *attr, char *buf)
{
	struct pstn_modem* modem = platform_get_drvdata(to_platform_device(dev));
	ssize_t len=0;
	int state;
	state = gpio_get_pin(modem->gpio_handlestate);
	len += sprintf(buf+len, "handle state [%s]\t\n", (state<0)?"invalid":
		(state>0)?"offhook":"onhook");
		
	state = gpio_get_pin(modem->gpio_hookstate );
	len += sprintf(buf+len, "hook state [%s]\t\n", (state<0)?"invalid":
		(state>0)?"offhook":"onhook");
		
	state = gpio_get_pin(modem->gpio_hostwake);
	len += sprintf(buf+len, "hostwake state [%s]\t\n", (state<0)?"invalid":
		(state>0)?"high":"low");
	len += sprintf(buf+len, "\n\necho <hook> <on|off>\tset hook state\n");

	return len;	
}

static ssize_t pstn_modem_store(struct device* dev,	struct device_attribute* attr,
	const char* buf,	size_t count)
{
	struct pstn_modem* modem = platform_get_drvdata(to_platform_device(dev));
	char func[48];
	char state[48];
	int filled;
    filled = sscanf(buf,"%s %s",func,state);
	if(2==filled)
	{
		if(!strcmp(func,"hook"))
		{
			if(!strcmp(state,"on"))
			{
				gpio_set_pin(modem->gpio_hookstate,0);
			}
			else if(!strcmp(state,"off"))
			{
				gpio_set_pin(modem->gpio_hookstate,1);
			}
				
		}		

	}
	
	return count;
}



static ssize_t pstn_link_state_show(struct device *dev,
                            struct device_attribute *attr, char *buf)
{
	struct pstn_modem* modem = platform_get_drvdata(to_platform_device(dev));
	return sprintf(buf,"%d\n",modem->link_state_change);
}
static ssize_t pstn_link_state_store(struct device* dev,	struct device_attribute* attr,
	const char* buf,	size_t count)
{
	struct pstn_modem* modem = platform_get_drvdata(to_platform_device(dev));
	int new_link_state;
	if(1==sscanf(buf,"%d",&new_link_state))
	{
		modem->link_state_change = new_link_state;
	}
	return count;
	
}

static ssize_t pstn_screen_state_show(struct device *dev,
                            struct device_attribute *attr, char *buf)
{
	struct pstn_modem* modem = platform_get_drvdata(to_platform_device(dev));
	return sprintf(buf,"%d\n",modem->screen_state);
}
static ssize_t pstn_screen_state_store(struct device* dev,	struct device_attribute* attr,
	const char* buf,	size_t count)
{
	struct pstn_modem* modem = platform_get_drvdata(to_platform_device(dev));
	int new_link_state;
	if(1==sscanf(buf,"%d",&new_link_state))
	{
		modem->screen_state = new_link_state;
	}
	return count;
	
}

static ssize_t pstn_handle_state_show(struct device *dev,
                            struct device_attribute *attr, char *buf)
{
	struct pstn_modem* modem = platform_get_drvdata(to_platform_device(dev));
	return sprintf(buf,"%d\n",gpio_get_pin(modem->gpio_handlestate));
}

static ssize_t pstn_hook_state_show(struct device *dev,
                            struct device_attribute *attr, char *buf)
{
	struct pstn_modem* modem = platform_get_drvdata(to_platform_device(dev));
	return sprintf(buf,"%d\n",gpio_get_pin(modem->gpio_hookstate));
}

static ssize_t pstn_hook_state_store(struct device* dev,	struct device_attribute* attr,
	const char* buf,	size_t count)
{
	struct pstn_modem* modem = platform_get_drvdata(to_platform_device(dev));
	int new_state;
	if(1==sscanf(buf,"%d",&new_state))
	{
		gpio_set_pin(modem->gpio_hookstate,new_state);	
	}
	return count;
	
}



static DEVICE_ATTR(pstn_modem, 0666, pstn_modem_show, pstn_modem_store);
static DEVICE_ATTR(pstn_handle_state, 0666, pstn_handle_state_show, NULL);
static DEVICE_ATTR(pstn_hook_state, 0666, pstn_hook_state_show, pstn_hook_state_store);

static DEVICE_ATTR(pstn_link_state, 0666, pstn_link_state_show, pstn_link_state_store);
static DEVICE_ATTR(pstn_screen_state, 0666, pstn_screen_state_show, pstn_screen_state_store);


static void pstn_modem_command_work(struct work_struct *work)
{
	struct pstn_modem *modem = (struct pstn_modem*)container_of(work,struct pstn_modem,command_work.work.work);	

	//TODO 
	
	if(modem->command_work.work_callback)
		(*modem->command_work.work_callback)(modem);


}


#ifdef MODEM_WAKEUP_ENABLED

static irqreturn_t pstn_modem_irq_handler(int irq, void *data)
{
	struct pstn_modem* modem = data;
	int val;

	//struct pstn_modem* m = data;
	//just for wakeup AP,dummy impl?
	printk("\n\n\nmodem wakeup\n\n\n");	
	
	//val = gpio_get_value(modem->gpio_hostwake);
	/* judge onkey status for 3 seconds */
	/* press down */
	if(0==modem->screen_state)
	{
		input_report_key(modem->idev, KEY_POWER, 1);
		input_report_key(modem->idev, KEY_POWER, 0);
	}
	input_sync(modem->idev);
	
	return IRQ_HANDLED;
}

#endif

static int __devinit pstn_modem_probe(struct platform_device *pdev)
{
	struct pstn_modem_plat_data* pdata = pdev->dev.platform_data;
	struct pstn_modem* modem;
	int ret;

	if(!pdata) return -ENODEV;

	modem = pdata->modem;
	
	//request modem host wakeup irq	
	#ifdef MODEM_WAKEUP_ENABLED
	//set host wake as input
	gpio_direction_input(modem->gpio_hostwake);
	ret = request_irq(gpio_to_irq(modem->gpio_hostwake), pstn_modem_irq_handler,
		IRQF_NO_SUSPEND | IRQF_SHARED | IRQF_TRIGGER_FALLING/* | IRQF_TRIGGER_RISING*/,
		"modem hostwake", modem);
	if (ret) {
		pr_warning("Request modem hostwake irq failed %d\n", ret);				
	}
	else
	{
		disable_irq(gpio_to_irq(modem->gpio_hostwake));
	}
	modem->idev = input_allocate_device();
	if (!modem->idev) {
		dev_err(&pdev->dev, "Failed to allocate input dev\n");
		return -ENOMEM;
	}

	modem->idev->name = "pstn_modem";
	modem->idev->phys = "pstn_modem/input0";
	modem->idev->dev.parent = &pdev->dev;
	modem->idev->evbit[0] = BIT_MASK(EV_KEY);
	modem->idev->keybit[BIT_WORD(KEY_POWER)] = BIT_MASK(KEY_POWER);
	ret = input_register_device(modem->idev);
	if (ret) {
		dev_err(&pdev->dev, "Can't register input device: %d\n", ret);
		input_free_device(modem->idev);
		return -ENODEV;
	}
	
	#endif
	

	platform_set_drvdata(pdev,modem);

	//register device attributes
    ret = device_create_file(&pdev->dev,&dev_attr_pstn_modem);
    ret = device_create_file(&pdev->dev,&dev_attr_pstn_handle_state);
    ret = device_create_file(&pdev->dev,&dev_attr_pstn_hook_state);
    ret = device_create_file(&pdev->dev,&dev_attr_pstn_link_state);
    ret = device_create_file(&pdev->dev,&dev_attr_pstn_screen_state);
	
	

	return ret;
}

static int pstn_modem_suspend(struct platform_device *pdev, pm_message_t state)
{	
	struct pstn_modem* modem = platform_get_drvdata(pdev);
	enable_irq(gpio_to_irq(modem->gpio_hostwake));
	return 0;
}
static int pstn_modem_resume(struct platform_device *pdev)
{
	struct pstn_modem* modem = platform_get_drvdata(pdev);
	disable_irq_nosync(gpio_to_irq(modem->gpio_hostwake));

	modem->link_state_change = 1;
	
	return 0;	
}


static struct platform_driver pstn_modem_driver =
{
	.probe		= pstn_modem_probe,	
	.driver		= {			
		.name	= "pstn_modem",
		.owner	= THIS_MODULE,	
	},	
	.suspend	= pstn_modem_suspend,
	.resume		= pstn_modem_resume,
};

static struct platform_device pstn_modem_device = 
{
	.name	= "pstn_modem",
	.id = -1,
};

static int __init board_pstn_init(void)
{
	struct pstn_modem* modem = kzalloc(sizeof(struct pstn_modem),GFP_KERNEL);
	struct pstn_modem_plat_data* modem_pdata=kzalloc(sizeof(struct pstn_modem_plat_data),GFP_KERNEL);

	if(!modem||!modem_pdata)
	{
		pr_warning("%s out of memory\n",__func__);
		return -ENOMEM;
	}
	modem_pdata->modem = modem;
	//request gpio pins
	modem->gpio_handlestate = GPIO(138);
	modem->gpio_hookstate = GPIO(122);

#ifdef MODEM_WAKEUP_ENABLED	
	modem->gpio_hostwake  = GPIO(110);
#else
	modem->gpio_hostwake = -1;
#endif

	if (gpio_request(modem->gpio_handlestate, "pstn handle")) 
	{
		pr_warning("request pstn handle GPIO failed\n");
		kfree(modem);
		return -EBUSY;
	}
	else if (gpio_request(modem->gpio_hookstate, "pstn hook")) 
	{
		pr_warning("request pstn hook GPIO failed\n");
		kfree(modem);
		return -EBUSY;
	}

#ifdef MODEM_WAKEUP_ENABLED
	if (gpio_request(modem->gpio_hostwake, "modem hostwake")) 
	{
	  pr_warning("request modem hostwake GPIO failed\n");
	  kfree(modem);
	  return -EBUSY;
	}		
#endif

	//set hook state as default
	gpio_direction_output(modem->gpio_hookstate,0);
	gpio_direction_input(modem->gpio_handlestate);

	INIT_DELAYED_WORK(&modem->command_work.work,pstn_modem_command_work);	

	/*usb modem device for user space access*/
	pstn_modem_device.dev.platform_data = modem_pdata;	
	platform_driver_register(&pstn_modem_driver);
	platform_device_register(&pstn_modem_device);

	return 0;
	
}


/* 
 * Set modem as late init to ensure usb subsystem already initialized 
 *
 */
late_initcall(board_pstn_init);
#else
#warning "pstn modem disabled"
#endif

