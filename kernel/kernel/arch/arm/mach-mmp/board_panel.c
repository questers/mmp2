#include <mach/timebutton.h>
#include <linux/input.h> 
#ifdef CONFIG_KEYBOARD_GPIO
/* gpio_keys */
#define INIT_KEY(_code, _gpio, _active_low, _desc)	\
	{						\
		.code       = KEY_##_code,		\
		.gpio       = _gpio,			\
		.active_low = _active_low,		\
		.desc       = _desc,			\
		.type       = EV_KEY,			\
		.wakeup     = 1,			\
		.debounce_interval     = 10,			\
	}

static struct gpio_keys_button gpio_keys_buttons[] = {
	INIT_KEY(VOLUMEUP,	GPIO(8),	1,	"volume+"),
	INIT_KEY(VOLUMEDOWN,GPIO(9),	1,	"volume-"),
#if defined(CONFIG_DRAGONFLY)
	INIT_KEY(F6,GPIO(104),	1,	"home"),
	INIT_KEY(F8,GPIO(109),	1,	"back"),
	INIT_KEY(F7,GPIO(110),	1,	"menu"),
#endif
//	INIT_KEY(POWER,GPIO(130),	0,	"power"),
};

static struct gpio_keys_platform_data gpio_keys_data = {
	.buttons = gpio_keys_buttons,
	.nbuttons = ARRAY_SIZE(gpio_keys_buttons),
	.rep = 0,
};

static struct platform_device gpio_keys = {
	.name = "gpio-keys",
	.dev  = {
		.platform_data = &gpio_keys_data,
	},
	.id   = -1,
};
#endif
#if 0
static struct timedbutton time_buttons[] = 
{
	{
		.name = "power",
		.gpio = GPIO(130),
		.active_low = 0,
		.key1 = KEY_POWER,
		.key1_debounce = 10,//ms
		.key2 = KEY_F12,
		.key2_debounce = 1000,//ms
	}
};
static struct timedbutton_platform_data time_keys_data = 
{
	.buttons_number = ARRAY_SIZE(time_buttons),
	.buttons = time_buttons,	
};

static struct platform_device timed_keys = {
	.name = "time-button",
	.dev  = {
		.platform_data = &time_keys_data,
	},
	.id   = -1,
};
#endif
static void __init board_panel_init(void)
{	
	#ifdef CONFIG_KEYBOARD_GPIO
	platform_device_register(&gpio_keys);
	#endif
	//platform_device_register(&timed_keys);

}

