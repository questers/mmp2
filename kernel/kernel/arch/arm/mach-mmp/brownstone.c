/*
 *  linux/arch/arm/mach-mmp/brownstone.c
 *
 *  Support for the Marvell Brownstone Development Platform.
 *
 *  Copyright (C) 2009-2010 Marvell International Ltd.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  publishhed by the Free Software Foundation.
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/io.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/regulator/machine.h>
#include <linux/regulator/max8649.h>
#include <linux/mfd/max8925.h>
#include <linux/regulator/fixed.h> 
#include <linux/mfd/wm8994/pdata.h>
#include <linux/clk.h>
#include <linux/switch.h>
#include <linux/synaptics_i2c_rmi.h>
#include <linux/android_pmem.h>
#include <linux/pwm_backlight.h>
#include <linux/gpio_keys.h>
#include <linux/fb.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <mach/addr-map.h>
#include <mach/mfp-mmp2.h>
#include <mach/mmp2.h>
#include <mach/irqs.h>
#include <mach/regs-mpmu.h>
#include <mach/regs-icu.h>
#include <mach/mmp2_plat_ver.h>
#include <mach/mmp2_dvfm.h>
#include <mach/tc35876x.h>
#include <mach/pxa168fb.h>
#include <mach/touchscreen.h>
#include <mach/axis_sensor.h>
#include <mach/mrvl_hdmitx.h>
#include <plat/pxa27x_keypad.h>
#include <plat/generic.h>
#include <linux/usb.h>
#include <linux/usb/otg.h>
#include <plat/pxa_u2o.h>
#include <plat/vbus.h>
#include <linux/power/max8903.h>
#include "common.h"
#include <mach/uio_hdmi.h>

#include "initlogo.h"

#ifdef CONFIG_SD8XXX_RFKILL
#include <linux/sd8x_rfkill.h>
#endif

#ifdef CONFIG_USB_ANDROID
/* Platform specific android usb settings.
 * Default settings in mmp2_android_usb.h */
#define USB_VENDOR_NAME		"Marvell"
/* FIXME: Marvell(VID:0x1286) is not already in the adb vendor list yet.
 * So take Google's VID here to make usb workable with adb tool */
#define USB_VENDOR_ID		0x18d1
#define USB_PRODUCT_NAME	"Brownstone"
#define USB_SERIAL_NUMBER	"MMP2_BS_001"
#include <mach/mmp2_android_usb.h>
#endif

#define BROWNSTONE_NR_IRQS		(IRQ_BOARD_START + 48)

#define USE_KPC
static unsigned long brownstone_pin_config[] __initdata = {
	/* UART1 */
	GPIO29_UART1_RXD,
	GPIO30_UART1_TXD,

	/* UART2 */
	GPIO47_UART2_RXD,
	GPIO48_UART2_TXD,

	/* UART3 */
	GPIO51_UART3_RXD,
	GPIO52_UART3_TXD,

	/*CAM3 - OV5642 */
	GPIO67_GPIO67,		//CAM3_RST_N
//	GPIO68_GPIO68,		//CAM2_RST_N
//	GPIO73_GPIO73,		//CAM1_RST_N
	GPIO69_CAM_MCLK,	//CAM3_MCLK | CAM2_MCLK | CAM1_MCLK

	/* TWSI1 */
	TWSI1_SCL,
	TWSI1_SDA,

	/* TWSI2 */
	GPIO43_TWSI2_SCL,
	GPIO44_TWSI2_SDA,

	/* TWSI3 */		//CAM3 - OV5642
	GPIO71_TWSI3_SCL,
	GPIO72_TWSI3_SDA,

	/* TWSI4 */
	TWSI4_SCL,
	TWSI4_SDA,

	/* TWSI5 */
	GPIO99_TWSI5_SCL,
	GPIO100_TWSI5_SDA,

	/* TWSI6 */
	GPIO97_TWSI6_SCL,
	GPIO98_TWSI6_SDA,
 
	/* SSPA1 (I2S) */
	GPIO23_GPIO23,
 	GPIO24_I2S_SYSCLK,	
 	GPIO25_I2S_BITCLK,		
 	GPIO26_I2S_SYNC,	
 	GPIO27_I2S_DATA_OUT,	
 	GPIO28_I2S_SDATA_IN,
 
 	/* SSPA2 */ 
 	GPIO33_SSPA2_CLK,
 	GPIO34_SSPA2_FRM,
 	GPIO35_SSPA2_TXD,
 	GPIO36_SSPA2_RXD,

	/* DFI */
	GPIO168_DFI_D0,
	GPIO167_DFI_D1,
	GPIO166_DFI_D2,
	GPIO165_DFI_D3,
	GPIO107_DFI_D4,
	GPIO106_DFI_D5,
	GPIO105_DFI_D6,
	GPIO104_DFI_D7,
	GPIO111_DFI_D8,
	GPIO164_DFI_D9,
	GPIO163_DFI_D10,
	GPIO162_DFI_D11,
	GPIO161_DFI_D12,
	GPIO110_DFI_D13,
	GPIO109_DFI_D14,
	GPIO108_DFI_D15,
	GPIO143_ND_nCS0,
	GPIO144_ND_nCS1,
	GPIO147_ND_nWE,
	GPIO148_ND_nRE,
	GPIO150_ND_ALE,
	GPIO149_ND_CLE,
	GPIO112_ND_RDY0,
	GPIO160_ND_RDY1,
	GPIO154_SMC_IRQ,

	/* Keypad */
#ifdef USE_KPC
	GPIO16_KP_DKIN0_PULL_HIGH,
	GPIO17_KP_DKIN1_PULL_HIGH,
	GPIO18_KP_DKIN2_PULL_HIGH,
	GPIO19_KP_DKIN3_PULL_HIGH,
#else
	GPIO16_GPIO16_PULL_HIGH,
	GPIO17_GPIO17_PULL_HIGH,
	GPIO18_GPIO18_PULL_HIGH,
	GPIO19_GPIO19_PULL_HIGH,
#endif

	/* CHG_500MA */
	GPIO79_GPIO79,
	/* CHG_EN_N */
	GPIO80_GPIO80,
	/* VBUS_FAULT_N */
	GPIO81_GPIO81,
	/* VBUS_EN */
	GPIO82_GPIO82,

	/* LCD */
	GPIO83_LCD_RST,
	GPIO89_LCD_5V_BIAS,

	/* Backlight */
	GPIO53_LCM_BIAS_PWM,

	/* Touch */
	GPIO101_GPIO101,

	/* PMIC */
	PMIC_PMIC_INT | MFP_LPM_EDGE_FALL,
	GPIO45_WM8994_LDOEN,
	GPIO46_HDMI_DET_PULL_HIGH,

	/* GPIO */
	GPIO74_GPIO74,
	GPIO75_GPIO75,
	GPIO76_GPIO76,
	GPIO77_GPIO77,
	GPIO126_GPIO126,
	GPIO127_GPIO127,
	GPIO128_GPIO128,

	/* USIM unused, set as GPIO input, Pull-high */
	GPIO102_GPIO102,
	GPIO103_GPIO103,
	GPIO142_GPIO142,

	/*Cywee sensor*/
	GPIO93_GPIO93,

	/* GPS */
	GPIO14_GPIO14,
	GPIO15_GPIO15,
	GPIO49_GPIO49,
	GPIO50_GPIO50,

	/* MMC0 */
	GPIO131_MMC1_DAT3_PULL_HIGH,
	GPIO132_MMC1_DAT2_PULL_HIGH,
	GPIO133_MMC1_DAT1_PULL_HIGH,
	GPIO134_MMC1_DAT0_PULL_HIGH,
	GPIO136_MMC1_CMD_PULL_HIGH,
	GPIO139_MMC1_CLK,
	GPIO140_MMC1_CD_PULL_LOW,
	GPIO141_MMC1_WP_PULL_LOW,

	/* MMC1 */
	GPIO37_MMC2_DAT3_PULL_HIGH,
	GPIO38_MMC2_DAT2_PULL_HIGH,
	GPIO39_MMC2_DAT1_PULL_HIGH,
	GPIO40_MMC2_DAT0_PULL_HIGH,
	GPIO41_MMC2_CMD_PULL_HIGH,
	GPIO42_MMC2_CLK,

	GPIO57_WLAN_PD_N,
	GPIO58_GPIO58,

	/* MMC2 */
	GPIO165_MMC3_DAT7_PULL_HIGH,
	GPIO162_MMC3_DAT6_PULL_HIGH,
	GPIO166_MMC3_DAT5_PULL_HIGH,
	GPIO163_MMC3_DAT4_PULL_HIGH,
	GPIO167_MMC3_DAT3_PULL_HIGH,
	GPIO164_MMC3_DAT2_PULL_HIGH,
	GPIO168_MMC3_DAT1_PULL_HIGH,
	GPIO111_MMC3_DAT0_PULL_HIGH,
	GPIO112_MMC3_CMD_PULL_HIGH,
	GPIO151_MMC3_CLK,

	/* CM3634 INT */
	GPIO92_CM3623_INT_PIN,
};

static int control_ldo(char *i, int on, int voltage);
static struct mutex ldo_mutex;
struct mutex pwr_i2c_conflict_mutex;

#ifdef CONFIG_USB_EHCI_PXA_U2H
static unsigned long hsic_reset_gpio80_config[] = {
	GPIO80_HSIC_RESET,
};

static unsigned long hsic_reset_gpio120_config[] = {
	GPIO120_HSIC_RESET,
};
#endif

static int usim_init(void)
{
	int usim = mfp_to_gpio(GPIO102_GPIO102);

	if (gpio_request(usim, "USIM_CLK")) {
		printk(KERN_INFO "gpio %d request failed\n", usim);
		return -1;
	}

	gpio_direction_input(usim);
	gpio_free(usim);

	usim = mfp_to_gpio(GPIO103_GPIO103);
	if (gpio_request(usim, "USIM_CLK")) {
		printk(KERN_INFO "gpio %d request failed\n", usim);
		return -1;
	}

	gpio_direction_input(usim);
	gpio_free(usim);

	usim = mfp_to_gpio(GPIO142_GPIO142);
	if (gpio_request(usim, "USIM_CLK")) {
		printk(KERN_INFO "gpio %d request failed\n", usim);
		return -1;
	}

	gpio_direction_input(usim);
	gpio_free(usim);

	return 0;
}

static int charger_init(void)
{
	int chg_2a_n = mfp_to_gpio(GPIO80_GPIO80);
	int chg_500ma = mfp_to_gpio(GPIO79_GPIO79);
	int vbus_en = mfp_to_gpio(GPIO82_GPIO82);

	printk(KERN_DEBUG "%s\n", __func__);

	/* set VBUS_EN to low */
	if (gpio_request(vbus_en, "VBUS_EN")) {
		printk(KERN_INFO "gpio %d request failed\n", vbus_en);
		return -1;
	}

	gpio_direction_output(vbus_en, 0);
	gpio_free(vbus_en);

	/* set CHG_500MA to low */
	if (gpio_request(chg_500ma, "CHG_500MA")) {
		printk(KERN_INFO "gpio %d request failed\n", chg_500ma);
		return -1;
	}

	gpio_direction_output(chg_500ma, 0);
	gpio_free(chg_500ma);

	/* set CHG_EN_N to low */
	if (gpio_request(chg_2a_n, "CHG_2A_N")) {
		printk(KERN_INFO "gpio %d request failed\n", chg_2a_n);
		return -1;
	}

	gpio_direction_output(chg_2a_n, 0);
	gpio_free(chg_2a_n);

	return 0;
}

static int v5p_enable_set(int on)
{
	int gpio = mfp_to_gpio(GPIO89_LCD_5V_BIAS);
	static int v5p_enabled;

	/* set panel 5V power */
	if (gpio_request(gpio, "lcd bias 5v gpio")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio);
		return -1;
	}

	if (on) {
		if (v5p_enabled++ == 0) {
			printk(KERN_DEBUG "%s on\n", __func__);
			gpio_direction_output(gpio, 1);
		}
	} else {
		if (--v5p_enabled == 0) {
			printk(KERN_DEBUG "%s off\n", __func__);
			gpio_direction_output(gpio, 0);
		}
	}

	gpio_free(gpio);

	return 0;
}

#if defined(CONFIG_TC35876X)
extern int tc35876x_write32(u16 reg, u32 val);
extern int tc35876x_write16(u16 reg, u16 val);
extern int tc35876x_read16(u16 reg, u16 *pval);
extern int tc35876x_read32(u16 reg, u32 *pval);

static int tc358765_reset(struct pxa168fb_info *fbi, int hold)
{
	int gpio = mfp_to_gpio(GPIO83_LCD_RST);

	if (gpio_request(gpio, "lcd reset gpio")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio);
		return -1;
	}

	gpio_direction_output(gpio, 0);
	if (!hold) {
		mdelay(1);
		gpio_direction_output(gpio, 1);
	}
	gpio_free(gpio);

	/* add some delay to ensure stability */
	msleep(4);

	return 0;
}

int tc358765_init(void)
{
	int gpio = mfp_to_gpio(GPIO89_LCD_5V_BIAS);

	/* hold reset before turn on any power */
	tc358765_reset(NULL, 1);

	if (gpio_request(gpio, "lcd bias 5v gpio")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio);
		return -1;
	}
	gpio_direction_output(gpio, 1);
	mdelay(1);

	gpio_free(gpio);
	return 0;
}

#define TC358765_CHIPID		0x6500
#define TC358765_CHIPID_REG	0x0580
int tcchip_readid(void)
{
	int status;
	u16 chip_id;

	status = tc35876x_read16(TC358765_CHIPID_REG, &chip_id);
	if ((status < 0) || (chip_id != TC358765_CHIPID)) {
		printk(KERN_WARNING "tc35876x unavailable!\n");
		return -EAGAIN;
	} else {
		pr_debug("tc35876x(chip id:0x%02x) detected.\n", chip_id);
	}

	return status;
}

static void tc358765_dump(void)
{
#if 0
	u32 val;

	pr_info("%s\n", __func__);
	tc35876x_read32(PPI_TX_RX_TA, &val);
	pr_info(" - PPI_TX_RX_TA = 0x%x\n", val);
	tc35876x_read32(PPI_LPTXTIMECNT, &val);
	pr_info(" - PPI_LPTXTIMECNT = 0x%x\n", val);
	tc35876x_read32(PPI_D0S_CLRSIPOCOUNT, &val);
	pr_info(" - PPI_D0S_CLRSIPOCOUNT = 0x%x\n", val);
	tc35876x_read32(PPI_D1S_CLRSIPOCOUNT, &val);
	pr_info(" - PPI_D1S_CLRSIPOCOUNT = 0x%x\n", val);

	tc35876x_read32(PPI_D2S_CLRSIPOCOUNT, &val);
	pr_info(" - PPI_D2S_CLRSIPOCOUNT = 0x%x\n", val);
	tc35876x_read32(PPI_D3S_CLRSIPOCOUNT, &val);
	pr_info(" - PPI_D3S_CLRSIPOCOUNT = 0x%x\n", val);

	tc35876x_read32(PPI_LANEENABLE, &val);
	pr_info(" - PPI_LANEENABLE = 0x%x\n", val);
	tc35876x_read32(DSI_LANEENABLE, &val);
	pr_info(" - DSI_LANEENABLE = 0x%x\n", val);
	tc35876x_read32(PPI_STARTPPI, &val);
	pr_info(" - PPI_STARTPPI = 0x%x\n", val);
	tc35876x_read32(DSI_STARTDSI, &val);
	pr_info(" - DSI_STARTDSI = 0x%x\n", val);

	tc35876x_read32(VPCTRL, &val);
	pr_info(" - VPCTRL = 0x%x\n", val);
	tc35876x_read32(HTIM1, &val);
	pr_info(" - HTIM1 = 0x%x\n", val);
	tc35876x_read32(HTIM2, &val);
	pr_info(" - HTIM2 = 0x%x\n", val);
	tc35876x_read32(VTIM1, &val);
	pr_info(" - VTIM1 = 0x%x\n", val);
	tc35876x_read32(VTIM2, &val);
	pr_info(" - VTIM2 = 0x%x\n", val);
	tc35876x_read32(VFUEN, &val);
	pr_info(" - VFUEN = 0x%x\n", val);
	tc35876x_read32(LVCFG, &val);
	pr_info(" - LVCFG = 0x%x\n", val);

	tc35876x_read32(DSI_INTSTAUS, &val);
	pr_info("!! - DSI_INTSTAUS= 0x%x BEFORE\n", val);
	tc35876x_write32(DSI_INTCLR, 0xFFFFFFFF);
	tc35876x_read32(DSI_INTSTAUS, &val);
	pr_info("!! - DSI_INTSTAUS= 0x%x AFTER\n", val);

	tc35876x_read32(DSI_LANESTATUS0, &val);
	pr_info(" - DSI_LANESTATUS0= 0x%x\n", val);
	tc35876x_read32(DSIERRCNT, &val);
	pr_info(" - DSIERRCNT= 0x%x\n", val);
	tc35876x_read32(DSIERRCNT, &val);
	pr_info(" - DSIERRCNT= 0x%x AGAIN\n", val);
	tc35876x_read32(SYSSTAT, &val);
	pr_info(" - SYSSTAT= 0x%x\n", val);
#endif
}

static int dsi_set_tc358765(struct pxa168fb_info *fbi)
{
	/* struct fb_var_screeninfo *var = &(fbi->fb_info->var); */
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	struct dsi_info *di = mi->dsi;
	u16 chip_id = 0;
	int status;

	status = tc35876x_read16(TC358765_CHIPID_REG, &chip_id);
	if ((status < 0) || (chip_id != TC358765_CHIPID)) {
		pr_warning("tc35876x unavailable! chip_id %x\n", chip_id);
		return -EIO;
	} else {
		pr_debug("tc35876x(chip id:0x%02x) detected.\n", chip_id);
	}

	/* REG 0x13C,DAT 0x000C000F */
	tc35876x_write32(PPI_TX_RX_TA, 0x00040004);
	/* REG 0x114,DAT 0x0000000A */
	tc35876x_write32(PPI_LPTXTIMECNT,0x00000004);

	/* get middle value of mim-max value
	 * 0-0x13 for 2lanes-rgb888, 0-0x26 for 4lanes-rgb888
	 * 0-0x21 for 2lanes-rgb565, 0-0x25 for 4lanes-rgb565
	 */
	if (di->lanes == 4)
		status = 0x13;
	else if (di->bpp == 24)
		status = 0xa;
	else
		status = 0x11;
	/* REG 0x164,DAT 0x00000005 */
	tc35876x_write32(PPI_D0S_CLRSIPOCOUNT, status);
	/* REG 0x168,DAT 0x00000005 */
	tc35876x_write32(PPI_D1S_CLRSIPOCOUNT, status);
	if (di->lanes == 4) {
		/* REG 0x16C,DAT 0x00000005 */
		tc35876x_write32(PPI_D2S_CLRSIPOCOUNT, status);
		/* REG 0x170,DAT 0x00000005 */
		tc35876x_write32(PPI_D3S_CLRSIPOCOUNT, status);
	}

	/* REG 0x134,DAT 0x00000007 */
	tc35876x_write32(PPI_LANEENABLE, (di->lanes == 4) ? 0x1f : 0x7);
	/* REG 0x210,DAT 0x00000007 */
	tc35876x_write32(DSI_LANEENABLE, (di->lanes == 4) ? 0x1f : 0x7);

	/* REG 0x104,DAT 0x00000001 */
	tc35876x_write32(PPI_STARTPPI, 0x0000001);
	/* REG 0x204,DAT 0x00000001 */
	tc35876x_write32(DSI_STARTDSI, 0x0000001);

	/* REG 0x450,DAT 0x00012020, VSDELAY = 8 pixels */
	tc35876x_write32(VPCTRL, 0x00800020);

	/* REG 0x454,DAT 0x00200008
	tc35876x_write32(HTIM1, ((var->left_margin + var->hsync_len) << 16)
			| var->hsync_len); */
	tc35876x_write32(HTIM1, 0x00200008);
	/* REG 0x45C,DAT 0x00040004
	tc35876x_write32(VTIM1, ((var->upper_margin + var->hsync_len) << 16)
			| var->vsync_len); */
	tc35876x_write32(VTIM1, 0x00040004);

	/* REG 0x49C,DAT 0x00000201 */
	tc35876x_write32(LVCFG, 0x00000001);

	/* dump register value */
	tc358765_dump();

	return 0;
}

static struct tc35876x_platform_data tc358765_data = {
	.platform_init	= tc358765_init,
	.id		= TC358765_CHIPID,
	.id_reg		= TC358765_CHIPID_REG,
};
#endif

#ifdef CONFIG_FB_PXA168
static struct platform_pwm_backlight_data brownstone_lcd_backlight_data = {
                /* primary backlight */
                .pwm_id         = 2,
                .max_brightness = 100,
                .dft_brightness = 50,
                .pwm_period_ns  = 2000000,
};

static struct platform_device brownstone_lcd_backlight_devices = {
                .name           = "pwm-backlight",
                .id             = 0,
                .dev            = {
                        .platform_data = &brownstone_lcd_backlight_data,
                },
};

#define FB_XRES		1280
#define FB_YRES		720
static struct fb_videomode video_modes[] = {
	[0] = {
		.refresh	= 60,
		.xres		= FB_XRES,
		.yres		= FB_YRES,
		.hsync_len	= 2,
		.left_margin	= 12,	/* hbp */
		.right_margin	= 216,	/* hfp */
		.vsync_len	= 2,
		.upper_margin	= 10,	/* vbp */
		.lower_margin	= 4,	/* vfp */
		.sync		= FB_SYNC_VERT_HIGH_ACT | FB_SYNC_HOR_HIGH_ACT,
	},
};

/* supported DSI configurations:
 * lanes    bpp    RGB    FPS    DCLK
 * 2        24     888    59.8   800 (PLL1)
 * 4        24     888    59.8   400 (PLL1/2)
 * 2        16     565    57.8   500 (PLL2/2)
 * 4        16     565    56.2   266 (PLL1/1.5/2)
 */
static struct dsi_info brownstone_dsi = {
	.id			= 1,
	.lanes			= 4,
	.bpp			= 16,
	.burst_mode		= DSI_BURST_MODE_BURST,
	.hbp_en			= 1,
	.hfp_en			= 1,
};

static int dsi_power_set(int on)
{
	if (on)
		control_ldo("v_ldo3", 1, 1200000);
	else
		control_ldo("v_ldo3", 0, 1200000);
	return 0;
}

static int lcd_twsi5_set(int en)
{
	int gpio;
	mfp_cfg_t mfp_gpio99_gpio99 = MFP_CFG_LPM_PULL_HIGH(GPIO99, AF0)
								| MFP_PULL_HIGH;
	mfp_cfg_t mfp_gpio100_gpio100 = MFP_CFG_LPM_PULL_HIGH(GPIO100, AF0)
								| MFP_PULL_HIGH;
	mfp_cfg_t mfp_gpio99_twsi5 = GPIO99_TWSI5_SCL;
	mfp_cfg_t mfp_gpio100_twsi5 = GPIO100_TWSI5_SDA;

	if (en) {
		mfp_config(&mfp_gpio99_twsi5, 1);
		mfp_config(&mfp_gpio100_twsi5, 1);
	} else {
		mfp_config(&mfp_gpio99_gpio99, 1);
		mfp_config(&mfp_gpio100_gpio100, 1);

		gpio = mfp_to_gpio(GPIO99_GPIO99);
		if (gpio_request(gpio, "gpio99")) {
			printk(KERN_INFO "gpio %d request failed\n", gpio);
			return -1;
		}
		gpio_direction_output(gpio, 0);
		gpio_free(gpio);

		gpio = mfp_to_gpio(GPIO100_GPIO100);
		if (gpio_request(gpio, "gpio100")) {
			printk(KERN_INFO "gpio %d request failed\n", gpio);
			return -1;
		}
		gpio_direction_output(gpio, 0);
		gpio_free(gpio);
	}
	return 0;
}

static int brownstone_lcd_power(struct pxa168fb_info *fbi,
	unsigned int spi_gpio_cs, unsigned int spi_gpio_reset, int on)
{
	int lcd_rst_n = mfp_to_gpio(GPIO83_LCD_RST);

	/* panel reset */
	if (gpio_request(lcd_rst_n, "lcd reset gpio")) {
		printk(KERN_INFO "gpio %d request failed\n", lcd_rst_n);
		return -1;
	}

	if (on) {
		/* enable regulator LDO17 to power VDDC and VDD_LVDS*_12 */
		control_ldo("v_ldo17", 1, 1200000);

		/* enable AVDD12_DSI voltage */
		dsi_power_set(1);

		/* release reset */
		gpio_direction_output(lcd_rst_n, 1);

		/* enable 5V power supply */
		v5p_enable_set(1);

		/* config mfp GPIO99/GPIO100 as twsi5 */
		lcd_twsi5_set(1);
	} else {
		/* config mfp GPIO99/GPIO100 as normal gpio */
		lcd_twsi5_set(0);

		/* disable 5V power supply */
		v5p_enable_set(0);

		/* keep reset */
		gpio_direction_output(lcd_rst_n, 0);

		/* disable AVDD12_DSI voltage */
		dsi_power_set(0);

		/* disable regulator LDO17 */
		control_ldo("v_ldo17", 0, 1200000);
	}
	gpio_free(lcd_rst_n);

	pr_debug("%s on %d\n", __func__, on);
	return 0;
}

static int brownstone_dsi_init(struct pxa168fb_info *fbi)
{
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;

	/* reset DSI controller */
	dsi_reset(fbi, 1);
	mdelay(1);

	/* disable continuous clock */
	dsi_cclk_set(fbi, 0);

	/* dsi out of reset */
	dsi_reset(fbi, 0);

	/* set dphy */
	dsi_set_dphy(fbi);

	/* put all lanes to LP-11 state  */
	dsi_lanes_enable(fbi, 0);
	dsi_lanes_enable(fbi, 1);

	/* reset the bridge */
	if (mi->xcvr_reset)
		mi->xcvr_reset(fbi, 0);

	mdelay(10);
	/* set dsi controller */
	dsi_set_controller(fbi);

	/* turn on DSI continuous clock */
	dsi_cclk_set(fbi, 1);

	/* set dsi to dpi conversion chip */
	if (mi->phy_type == DSI2DPI)
		mi->dsi2dpi_set(fbi);

	return 0;
}

static struct pxa168fb_mach_info mmp2_mipi_lcd_info __initdata = {
	.id			= "GFX Layer",
	.sclk_src		= 260000000,	/* 266MHz */
	.sclk_div		= 0x40000108,
	.num_modes		= ARRAY_SIZE(video_modes),
	.modes			= video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.isr_clear_mask	= LCD_ISR_CLEAR_MASK_PXA168,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the panel is hard connected with lcd panel path and dsi1 output
	 */
	.io_pad_ctrl = CFG_CYC_BURST_LEN16,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.active			= 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.mmap			= 1,
	.max_fb_size		= FB_XRES * FB_YRES * 8 + 4096,
	.vdma_enable		= 1,
	.sram_paddr		= 0,
	.sram_size		= 30 * 1024,
	.immid			= 0,
	.phy_type		= DSI2DPI,
	.phy_init		= brownstone_dsi_init,
#ifdef CONFIG_TC35876X
	.dsi2dpi_set		= dsi_set_tc358765,
	.xcvr_reset		= tc358765_reset,
#else
#error Please select CONFIG_TC35876X in menuconfig to enable DSI bridge
#endif
	.dsi			= &brownstone_dsi,
	.pxa168fb_lcd_power     = &brownstone_lcd_power,
#ifdef CONFIG_PXA688_CMU
	.cmu_cal = {{-1, 47, 2, 2},
		{44, 92, 25, 25},
		{0, 0, 0, 0}
	},
	.cmu_cal_letter_box = {{48, 47, 2, 2},
		{93, 92, 25, 25},
		{0, 0, 0, 0}
	},
	.ioctl			= pxa688_cmu_ioctl,
#endif
};

static struct pxa168fb_mach_info mmp2_mipi_lcd_ovly_info __initdata = {
	.id			= "Video Layer",
	.num_modes		= ARRAY_SIZE(video_modes),
	.modes			= video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.mmap			= 0,
	.max_fb_size            = FB_XRES * FB_YRES * 8 + 4096,
	.vdma_enable		= 0,
	.sram_paddr		= 0,
	.sram_size		= 30 * 1024,
	.immid			= 0,
};
#endif

#ifdef CONFIG_FB_SECOND_PANEL_HDMI

static struct pxa168fb_mach_info mmp2_tv_hdmi_info __initdata = {
	.id			= "GFX Layer - TV",
	.sclk_div		= 0x5 | (1<<16) | (3<<30), /* HDMI PLL */
	.num_modes		= ARRAY_SIZE(tv_video_modes),
	.modes			= tv_video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.isr_clear_mask	= LCD_ISR_CLEAR_MASK_PXA168,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the hdmi monitor is hard connected with lcd tv path and hdmi output
	 */
	.io_pad_ctrl = CFG_CYC_BURST_LEN16,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 1,
	.active			= 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.mmap			= 0,
	.max_fb_size		= 1920 * 1080 * 8 + 4096,
	.vdma_enable		= 0,
	.sram_paddr		= 0,
	.sram_size		= 60 * 1024,
	.immid			= 0,
#ifdef CONFIG_PXA688_CMU
	.ioctl			= pxa688_cmu_ioctl,
#endif
};

static struct pxa168fb_mach_info mmp2_tv_hdmi_ovly_info __initdata = {
	.id			= "Video Layer - TV",
	.num_modes		= ARRAY_SIZE(tv_video_modes),
	.modes			= tv_video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.mmap			= 0,
	.max_fb_size            = 1920 * 1080 * 8 + 4096,
	.vdma_enable		= 0,
	.sram_paddr		= 0,
	.sram_size		= 60 * 1024,
	.immid			= 0,
};
#endif

static struct uio_hdmi_platform_data mmp2_hdmi_info __initdata = {
	.sspa_reg_base = 0xD42A0C00,
	.gpio = mfp_to_gpio(GPIO46_HDMI_DET),
};

static void set_logo_image(void)
{
	int node_count = sizeof(g_rle_data)/sizeof(struct InfoNode);
	int n = 0;
	int pix_fmt_ret;
	/* 16 bpp frame buffer */
	unsigned short *bits;
	unsigned short values;
	/* 32 bpp frame buffer */
	unsigned long *bitl;
	unsigned long valuel;

	mmp2_mipi_lcd_info.logo_image =
		rvmalloc(mmp2_mipi_lcd_info.max_fb_size);
	if (!(mmp2_mipi_lcd_info.logo_image)) {
		printk(KERN_ERR "logo image malloc failed!\n");
		return;
	}
	memset(mmp2_mipi_lcd_info.logo_image,
		0, mmp2_mipi_lcd_info.max_fb_size);
	/* draw init logo */
	pix_fmt_ret = get_bpp_fmt(mmp2_mipi_lcd_info.pix_fmt);
	n = 0;
	/* 16 bpp */
	if (pix_fmt_ret == 0) {
		bits = (unsigned short *)(mmp2_mipi_lcd_info.logo_image);
		while (n < node_count) {
			values = rgb16_to_dsts(g_rle_data[n].value,
				mmp2_mipi_lcd_info.pix_fmt);
			memset16(bits, values, g_rle_data[n].len << 1);
			bits += g_rle_data[n].len;
			n++;
		}
	}
	/* 32 bpp */
	else if (pix_fmt_ret == 1) {
		bitl = (unsigned long *)(mmp2_mipi_lcd_info.logo_image);
		while (n < node_count) {
			/* 32bpp */
			valuel = rgb16_to_dstl(g_rle_data[n].value,
				mmp2_mipi_lcd_info.pix_fmt);
			memset32(bitl, valuel, g_rle_data[n].len << 2);
			bitl += g_rle_data[n].len;
			n++;
		}
	} else
		printk(KERN_WARNING "logo image not compatible with pixel format!\n");
}

static void __init mmp2_add_lcd(void)
{
	struct dsi_info *dsi = &brownstone_dsi;
	unsigned char __iomem *dmc_membase;
	unsigned int CSn_NO_COL;

	if (dsi->bpp == 24) {
		mmp2_mipi_lcd_info.sclk_src = 800000000 / (dsi->lanes / 2);
		dsi->rgb_mode = DSI_LCD_INPUT_DATA_RGB_MODE_888;
	} else {
		mmp2_mipi_lcd_info.sclk_src = 520000000 / (dsi->lanes / 2);
		dsi->rgb_mode = DSI_LCD_INPUT_DATA_RGB_MODE_565;
		video_modes[0].right_margin = (dsi->lanes == 4) ? 315 : 178;
	}
	mmp2_mipi_lcd_info.sclk_div = 0x40000100 | ((dsi->lanes == 4) ? 4 : 8);

	dmc_membase = ioremap(DDR_MEM_CTRL_BASE, 0xfff);
	CSn_NO_COL = __raw_readl(dmc_membase + SDRAM_CONFIG0_TYPE1) >> 4;
	CSn_NO_COL &= 0xf;
	if (CSn_NO_COL <= 0x2) {
		/*
		*if DDR page size < 4KB, select no crossing 1KB boundary check
		*/
		mmp2_mipi_lcd_info.io_pad_ctrl |= CFG_BOUNDARY_1KB;
		mmp2_tv_hdmi_info.io_pad_ctrl |= CFG_BOUNDARY_1KB;
	}
	iounmap(dmc_membase);

#ifdef CONFIG_FB_PXA168
	/* lcd */
	/* vdma auto switch mode*/
	vdma_switch = 1;
	set_logo_image();
	mmp2_add_fb(&mmp2_mipi_lcd_info);
	mmp2_add_fb_ovly(&mmp2_mipi_lcd_ovly_info);
#ifdef CONFIG_FB_SECOND_PANEL_HDMI
	mmp2_add_fb_tv(&mmp2_tv_hdmi_info);
	mmp2_add_fb_tv_ovly(&mmp2_tv_hdmi_ovly_info);
#endif
#endif

#ifdef CONFIG_PXA688_MISC
	/* set TV path vertical smooth, panel2 as filter channel,
	 * vertical smooth is disabled by default to avoid underrun
	 * when video playback, to enable/disable graphics/video
	 * layer vertical smooth:
	 * echo g0/g1/v0/v1 > /sys/deivces/platform/pxa168-fb.1/misc
	 */
	fb_vsmooth = 1; fb_filter = 2;
#endif
}

static struct i2c_pxa_platform_data pwri2c_info __initdata = {
	.use_pio		= 1,
};

static struct i2c_pxa_platform_data sensori2c_info __initdata = {
	.fast_mode		= 1,
};

#if defined(CONFIG_REGULATOR_WM8994) && defined(CONFIG_REGULATOR_WM8994)

static struct regulator_consumer_supply wm8994_fixed_voltage0_supplies[] = {
	REGULATOR_SUPPLY("DBVDD", NULL),
	REGULATOR_SUPPLY("AVDD2", NULL),
	REGULATOR_SUPPLY("CPVDD", NULL),
};

static struct regulator_consumer_supply wm8994_fixed_voltage1_supplies[] = {
	REGULATOR_SUPPLY("SPKVDD1", NULL),
	REGULATOR_SUPPLY("SPKVDD2", NULL),
};

static struct regulator_init_data wm8994_fixed_voltage0_init_data = {
	.constraints = {
		.always_on = 1,
	},
	.num_consumer_supplies	= ARRAY_SIZE(wm8994_fixed_voltage0_supplies),
	.consumer_supplies	= wm8994_fixed_voltage0_supplies,
};

static struct regulator_init_data wm8994_fixed_voltage1_init_data = {
	.constraints = {
		.always_on = 1,
	},
	.num_consumer_supplies	= ARRAY_SIZE(wm8994_fixed_voltage1_supplies),
	.consumer_supplies	= wm8994_fixed_voltage1_supplies,
};

static struct fixed_voltage_config wm8994_fixed_voltage0_config = {
	.supply_name	= "VCC_1.8V",
	.microvolts	= 1800000,
	.gpio		= -EINVAL,
	.init_data	= &wm8994_fixed_voltage0_init_data,
};

static struct fixed_voltage_config wm8994_fixed_voltage1_config = {
	.supply_name	= "V_BAT",
	.microvolts	= 3700000,
	.gpio		= -EINVAL,
	.init_data	= &wm8994_fixed_voltage1_init_data,
};

static struct platform_device wm8994_fixed_voltage0 = {
	.name		= "reg-fixed-voltage",
	.id		= 0,
	.dev		= {
		.platform_data	= &wm8994_fixed_voltage0_config,
	},
};

static struct platform_device wm8994_fixed_voltage1 = {
	.name		= "reg-fixed-voltage",
	.id		= 1,
	.dev		= {
		.platform_data	= &wm8994_fixed_voltage1_config,
	},
};

static struct regulator_consumer_supply wm8994_avdd1_supply =
	REGULATOR_SUPPLY("AVDD1", NULL);

static struct regulator_consumer_supply wm8994_dcvdd_supply =
	REGULATOR_SUPPLY("DCVDD", NULL);

static struct regulator_init_data wm8994_ldo1_data = {
	.constraints	= {
		.name		= "AVDD1_3.0V",
		.valid_ops_mask	= REGULATOR_CHANGE_STATUS,
	},
	.num_consumer_supplies	= 1,
	.consumer_supplies	= &wm8994_avdd1_supply,
};

static struct regulator_init_data wm8994_ldo2_data = {
	.constraints	= {
		.name		= "DCVDD_1.0V",
	},
	.num_consumer_supplies	= 1,
	.consumer_supplies	= &wm8994_dcvdd_supply,
};

static struct wm8994_pdata brownstone_wm8994_pdata = {
	.gpio_defaults[0] = 0x0003,
	/* AIF2 voice */
	.gpio_defaults[2] = 0x8100,
	.gpio_defaults[3] = 0x8100,
	.gpio_defaults[4] = 0x8100,
	.gpio_defaults[5] = 0x8100,
	.gpio_defaults[6] = 0x0100,
	/* AIF3 voice */
	.gpio_defaults[7] = 0x8100,
	.gpio_defaults[8] = 0x0100,
	.gpio_defaults[9] = 0x8100,
	.gpio_defaults[10] = 0x8100,

	.ldo[0]	= { mfp_to_gpio(GPIO45_WM8994_LDOEN), NULL, &wm8994_ldo1_data },
	.ldo[1]	= { 0, NULL, &wm8994_ldo2_data },
};

static struct i2c_board_info brownstone_twsi2_info[] = {
	{
		.type		= "wm8994",
		.addr		= 0x1a,
		.platform_data	= &brownstone_wm8994_pdata,
	},
};

static struct platform_device *fixed_rdev[] __initdata = {
	&wm8994_fixed_voltage0,
	&wm8994_fixed_voltage1,
};

static void __init brownstone_fixed_regulator(void)
{
	platform_add_devices(fixed_rdev, ARRAY_SIZE(fixed_rdev));
}
#endif

static struct regulator_consumer_supply max8649_supply[] = {
	REGULATOR_SUPPLY("v_core", NULL),
};

static struct regulator_init_data max8649_init_data = {
	.constraints	= {
		.name		= "SD1",
		.min_uV		= 750000,
		.max_uV		= 1380000,
		.always_on	= 1,
		.boot_on	= 1,
		.valid_ops_mask	= REGULATOR_CHANGE_VOLTAGE,
	},
	.num_consumer_supplies	= 1,
	.consumer_supplies	= &max8649_supply[0],
};

static struct max8649_platform_data brownstone_max8649_info = {
	.mode		= 2,	/* VID1 = 1, VID0 = 0 */
	.extclk		= 0,
	.ramp_timing	= MAX8649_RAMP_32MV,
	.regulator	= &max8649_init_data,
};

static struct regulator_consumer_supply regulator_supply[] = {
	[MAX8925_ID_SD1]	= REGULATOR_SUPPLY("v_sd1", NULL),
	[MAX8925_ID_SD2]	= REGULATOR_SUPPLY("v_sd2", NULL),
	[MAX8925_ID_SD3]	= REGULATOR_SUPPLY("v_sd3", NULL),
	[MAX8925_ID_LDO1]	= REGULATOR_SUPPLY("v_ldo1", NULL),
	[MAX8925_ID_LDO2]	= REGULATOR_SUPPLY("v_ldo2", NULL),
	[MAX8925_ID_LDO3]	= REGULATOR_SUPPLY("v_ldo3", NULL),
	[MAX8925_ID_LDO4]	= REGULATOR_SUPPLY("v_ldo4", NULL),
	[MAX8925_ID_LDO5]	= REGULATOR_SUPPLY("v_ldo5", NULL),
	[MAX8925_ID_LDO6]	= REGULATOR_SUPPLY("v_ldo6", NULL),
	[MAX8925_ID_LDO7]	= REGULATOR_SUPPLY("v_ldo7", NULL),
	[MAX8925_ID_LDO8]	= REGULATOR_SUPPLY("v_ldo8", NULL),
	[MAX8925_ID_LDO9]	= REGULATOR_SUPPLY("v_ldo9", NULL),
	[MAX8925_ID_LDO10]	= REGULATOR_SUPPLY("v_ldo10", NULL),
	[MAX8925_ID_LDO11]	= REGULATOR_SUPPLY("vmmc", "sdhci-pxa.0"),
	[MAX8925_ID_LDO12]	= REGULATOR_SUPPLY("v_ldo12", NULL),
	[MAX8925_ID_LDO13]	= REGULATOR_SUPPLY("v_ldo13", NULL),
	[MAX8925_ID_LDO14]	= REGULATOR_SUPPLY("v_ldo14", NULL),
	[MAX8925_ID_LDO15]	= REGULATOR_SUPPLY("v_ldo15", NULL),
	[MAX8925_ID_LDO16]	= REGULATOR_SUPPLY("v_ldo16", NULL),
	[MAX8925_ID_LDO17]	= REGULATOR_SUPPLY("v_ldo17", NULL),
	[MAX8925_ID_LDO18]	= REGULATOR_SUPPLY("v_ldo18", NULL),
	[MAX8925_ID_LDO19]	= REGULATOR_SUPPLY("v_ldo19", NULL),
	[MAX8925_ID_LDO20]	= REGULATOR_SUPPLY("v_ldo20", NULL),
};

#define REG_INIT(_name, _min, _max, _always, _boot)		\
{								\
	.constraints = {					\
		.name		= __stringify(_name),		\
		.min_uV		= _min,				\
		.max_uV		= _max,				\
		.always_on	= _always,			\
		.boot_on	= _boot,			\
		.valid_ops_mask	= REGULATOR_CHANGE_VOLTAGE	\
				| REGULATOR_CHANGE_STATUS,	\
	},							\
	.num_consumer_supplies	= 1,				\
	.consumer_supplies	= &regulator_supply[MAX8925_ID_##_name], \
}

static struct regulator_init_data regulator_data[] = {
	[MAX8925_ID_SD1] = REG_INIT(SD1, 637500, 1425000, 1, 1),
	[MAX8925_ID_SD2] = REG_INIT(SD2, 650000, 2225000, 1, 1),
	[MAX8925_ID_SD3] = REG_INIT(SD3, 750000, 3900000, 1, 1),
	[MAX8925_ID_LDO1] = REG_INIT(LDO1, 750000, 3900000, 1, 1),
	[MAX8925_ID_LDO2] = REG_INIT(LDO2, 650000, 2250000, 1, 1),
	[MAX8925_ID_LDO3] = REG_INIT(LDO3, 650000, 2250000, 0, 0),
	[MAX8925_ID_LDO4] = REG_INIT(LDO4, 750000, 3900000, 1, 1),
	[MAX8925_ID_LDO5] = REG_INIT(LDO5, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO6] = REG_INIT(LDO6, 750000, 3900000, 1, 1),
	[MAX8925_ID_LDO7] = REG_INIT(LDO7, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO8] = REG_INIT(LDO8, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO9] = REG_INIT(LDO9, 750000, 3900000, 1, 1),
	[MAX8925_ID_LDO10] = REG_INIT(LDO10, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO11] = REG_INIT(LDO11, 2800000, 2800000, 0, 0),
	[MAX8925_ID_LDO12] = REG_INIT(LDO12, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO13] = REG_INIT(LDO13, 750000, 1500000, 0, 0),
	[MAX8925_ID_LDO14] = REG_INIT(LDO14, 750000, 3000000, 0, 0),	// close and will set in sensor_power_set for camera sensor AF_VCC (AUTOFOCUS)
	[MAX8925_ID_LDO15] = REG_INIT(LDO15, 750000, 2800000, 0, 0),	// close and will set in sensor_power_set for camera sensor AVDD (ANALOG)
	[MAX8925_ID_LDO16] = REG_INIT(LDO16, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO17] = REG_INIT(LDO17, 650000, 2250000, 1, 1),
	[MAX8925_ID_LDO18] = REG_INIT(LDO18, 650000, 2250000, 0, 0),
	[MAX8925_ID_LDO19] = REG_INIT(LDO19, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO20] = REG_INIT(LDO20, 750000, 3900000, 0, 0),
};

/*
 * ret = 1 is power is turned on or off
 * ret = 0 if we do nothing since power was on or off
 * ret < 0 if error
 */
static int control_ldo(char *i, int on, int voltage)
{
	static struct regulator *v_ldo = NULL;
	static int ldo[MAX8925_MAX_REGULATOR] = {0};
	int loop, number = 0;
	int ret = 0;

	mutex_lock(&ldo_mutex);
	for (loop = 0; loop < MAX8925_MAX_REGULATOR; loop++) {
		if (!strcmp(regulator_supply[loop].supply, i)){
			number = loop;
			break;
		}
	}

	v_ldo = regulator_get(NULL, i);
	if (IS_ERR(v_ldo)) {
		printk(KERN_ERR "%s can't open!\n", i);
		mutex_unlock(&ldo_mutex);
		return -EIO;
	}
	if (on && !ldo[number]) {
		regulator_enable(v_ldo);
		regulator_set_voltage(v_ldo, voltage, voltage);
		ret = 1;
		pr_debug("enabled %s\n", i);
	}
	if ((!on) && (ldo[number] <= 1) && regulator_is_enabled(v_ldo)) {
		regulator_force_disable(v_ldo);
		ret = 1;
		pr_debug("disabled %s\n", i);
	}
	regulator_put(v_ldo);

	if (on)
		ldo[number]++;
	else
		ldo[number]--;
	if (ldo[number] < 0)
		ldo[number] = 0;
	mutex_unlock(&ldo_mutex);
	return ret;
}

static struct max8925_backlight_pdata brownstone_key_backlight_data = {
	.dual_string	= 0,
	.brightness_off	= 1,
};

#ifdef NOT_USBED
static int brownstone_set_led(int enable)
{
	int led;

	led = mfp_to_gpio(MFP_PIN_GPIO74);
	if (gpio_request(led, "LED Orange")) {
		pr_err("Failed to request LED orange\n");
		return -EIO;
	}
	if (enable)
		gpio_direction_output(led, 0);
	else
		gpio_direction_output(led, 1);
	gpio_free(led);
	return 0;
}
#endif

static struct max8925_power_pdata brownstone_power_data = {
	/* .set_led		= brownstone_set_led, */
	.batt_detect		= 0,	/* can't detect battery by ID pin */
	.topoff_threshold	= MAX8925_TOPOFF_THR_10PER,
	.fast_charge		= MAX8925_FCHG_1000MA,
	.chg_max8903_en		= 1,	/* 1: enable; 0: disable */
};

static int max8925_set_vbus(int enable, int srp)
{
	int vbus_en = mfp_to_gpio(MFP_PIN_GPIO82);
	int ret = gpio_request(vbus_en, "vbus_en");

	if (ret) {
		pr_debug("failed to get gpio #%d\n", vbus_en);
		return -EINVAL;
	}

	gpio_direction_output(vbus_en, enable);
	mdelay(10);
	gpio_free(vbus_en);

	return 0;
}

/* max8925 vbus pdata */
static struct max8925_vbus_pdata brownstone_vbus_data = {
	.irq_rise	= MAX8925_IRQ_VCHG_DC_R,
	.irq_fall	= MAX8925_IRQ_VCHG_DC_F,
	.reg_base	= PXA168_U2O_REGBASE,
	.reg_end	= PXA168_U2O_REGBASE + USB_REG_RANGE,
	.set_vbus	= max8925_set_vbus,
};

static struct max8925_platform_data brownstone_max8925_info = {
	.backlight              = &brownstone_key_backlight_data,
	.power			= &brownstone_power_data,
	.vbus			= &brownstone_vbus_data,
	.irq_base		= IRQ_BOARD_START,

	.regulator[MAX8925_ID_SD1] = &regulator_data[MAX8925_ID_SD1],
	.regulator[MAX8925_ID_SD2] = &regulator_data[MAX8925_ID_SD2],
	.regulator[MAX8925_ID_SD3] = &regulator_data[MAX8925_ID_SD3],
	.regulator[MAX8925_ID_LDO1] = &regulator_data[MAX8925_ID_LDO1],
	.regulator[MAX8925_ID_LDO2] = &regulator_data[MAX8925_ID_LDO2],
	.regulator[MAX8925_ID_LDO3] = &regulator_data[MAX8925_ID_LDO3],
	.regulator[MAX8925_ID_LDO4] = &regulator_data[MAX8925_ID_LDO4],
	.regulator[MAX8925_ID_LDO5] = &regulator_data[MAX8925_ID_LDO5],
	.regulator[MAX8925_ID_LDO6] = &regulator_data[MAX8925_ID_LDO6],
	.regulator[MAX8925_ID_LDO7] = &regulator_data[MAX8925_ID_LDO7],
	.regulator[MAX8925_ID_LDO8] = &regulator_data[MAX8925_ID_LDO8],
	.regulator[MAX8925_ID_LDO9] = &regulator_data[MAX8925_ID_LDO9],
	.regulator[MAX8925_ID_LDO10] = &regulator_data[MAX8925_ID_LDO10],
	.regulator[MAX8925_ID_LDO11] = &regulator_data[MAX8925_ID_LDO11],
	.regulator[MAX8925_ID_LDO12] = &regulator_data[MAX8925_ID_LDO12],
	.regulator[MAX8925_ID_LDO13] = &regulator_data[MAX8925_ID_LDO13],
	.regulator[MAX8925_ID_LDO14] = &regulator_data[MAX8925_ID_LDO14],
	.regulator[MAX8925_ID_LDO15] = &regulator_data[MAX8925_ID_LDO15],
	.regulator[MAX8925_ID_LDO16] = &regulator_data[MAX8925_ID_LDO16],
	.regulator[MAX8925_ID_LDO17] = &regulator_data[MAX8925_ID_LDO17],
	.regulator[MAX8925_ID_LDO18] = &regulator_data[MAX8925_ID_LDO18],
	.regulator[MAX8925_ID_LDO19] = &regulator_data[MAX8925_ID_LDO19],
	.regulator[MAX8925_ID_LDO20] = &regulator_data[MAX8925_ID_LDO20],
};

static DEFINE_MUTEX(v_ldo8_mutex);

int v_ldo8_set_power(int on)
{
	int ret = 0;

	mutex_lock(&v_ldo8_mutex);
	ret = control_ldo("v_ldo8", on ? 1 : 0, 2800000);
	if (ret > 0)
		msleep(80);
	mutex_unlock(&v_ldo8_mutex);
	return ret;
}

static int cywee_set_power(int on)
{
	printk(KERN_DEBUG "%s on = %d\n", __func__, on);
	return v_ldo8_set_power(on);
}
static int cm3623_set_power(int on)
{
	printk(KERN_DEBUG "%s on = %d\n", __func__, on);
	return v_ldo8_set_power(on);
}
static int r800_set_power(int on)
{
	printk(KERN_DEBUG "%s on = %d\n", __func__, on);
	return v_ldo8_set_power(on);
}

static struct axis_sensor_platform_data cywee_platform_data = {
	.set_power	= cywee_set_power,
};

static struct axis_sensor_platform_data cm3623_platform_data = {
	.set_power	= cm3623_set_power,
};

static struct i2c_board_info brownstone_twsi4_info[] =
{
#if defined(CONFIG_SENSORS_CM3623)
	{
		.type		= "cm3623_ps",
		.addr		= (0xB0>>1),
		.platform_data  = &cm3623_platform_data,
	},
	{
		.type		= "cm3623_als_msb",
		.addr		= (0x20>>1),
		.platform_data  = &cm3623_platform_data,
	},
	{
		.type		= "cm3623_als_lsb",
		.addr		= (0x22>>1),
		.platform_data  = &cm3623_platform_data,
	},
	{
		.type		= "cm3623_int",
		.addr		= (0x18>>1),
		.platform_data  = &cm3623_platform_data,
	},
	{
		.type		= "cm3623_ps_threshold",
		.addr		= (0xB2>>1),
		.platform_data  = &cm3623_platform_data,
	},
#endif
#if defined(CONFIG_SENSORS_CWB01C01)
	{
		.type		= "cwb01c01",
		.addr		= (0xF0>>1),
		.platform_data  = &cywee_platform_data,
	},
#endif

#if defined(CONFIG_SENSORS_AMI602)
	{
		.type 		= "ami602",
		.addr		= (0x60>>1),
		.platform_data  = &cywee_platform_data,
	}
#endif
};

static struct i2c_board_info brownstone_twsi1_info[] = {
	[0] = {
		.type		= "max8649",
		.addr		= (0xc0>>1),
		.platform_data	= &brownstone_max8649_info,
	},
	[1] = {
		.type		= "max8925",
		.addr		= (0x78>>1),
		.irq		= IRQ_MMP2_PMIC,
		.platform_data	= &brownstone_max8925_info,
	},
};

#ifdef CONFIG_MTD_NAND_PXA3xx
static struct mtd_partition brownstone_nand_partitions[] = {
	[0] = {
		.name		= "Bootloader",
		.offset		= 0,
		.size		= 0x100000,
		.mask_flags	= MTD_WRITEABLE,
	},
	[1] = {
		.name		= "Reserve",
		.offset		= 0x100000,
		.size		= 0x080000,
	},
	[2] = {
		.name		= "Reserve",
		.offset		= 0x180000,
		.size		= 0x800000,
		.mask_flags	= MTD_WRITEABLE,
	},
	[3] = {
		.name		= "Kernel",
		.offset		= 0x980000,
		.size		= 0x300000,
		.mask_flags	= MTD_WRITEABLE,
	},
	[4] = {
		.name		= "system",
		.offset		= 0x0c80000,
		.size		= 0x7000000,
	},
	[5] = {
		.name		= "userdata",
		.offset		= 0x7c80000,
		.size		= 0x7000000,
	},
	[6] = {
		.name		= "filesystem",
		.offset		= 0x0ec80000,
		.size		= MTDPART_SIZ_FULL,
	}
};

static struct pxa3xx_nand_platform_data brownstone_nand_info;
static void __init brownstone_init_flash(void)
{
	brownstone_nand_info.parts[0] = brownstone_nand_partitions;
	brownstone_nand_info.nr_parts[0] = ARRAY_SIZE(brownstone_nand_partitions);
	brownstone_nand_info.controller_attrs = PXA3XX_ARBI_EN | PXA3XX_NAKED_CMD_EN
		| PXA3XX_DMA_EN | PXA3XX_ADV_TIME_TUNING;
	mmp2_add_nand(&brownstone_nand_info);
}
#endif

static struct touchscreen_platform_data tpk_r800_data = {
	.set_power	= r800_set_power,
};

static struct i2c_board_info brownstone_twsi5_info[] =
{
#if defined(CONFIG_TC35876X)
	{
		.type           = "tc35876x",
		.addr           = 0x0f,
		.platform_data  = &tc358765_data,
	},
#endif
#if defined(CONFIG_TOUCHSCREEN_TPK_R800)
	{
		.type           = "tpk_r800",
		.addr           = 0x10,
		.irq            = IRQ_GPIO(101),
		.platform_data  = &tpk_r800_data,
	},
#endif
};

#if defined(CONFIG_SWITCH_HEADSET_HOST_GPIO)
static struct gpio_switch_platform_data headset_switch_device_data = {
	.name = "h2w",
	.gpio = mfp_to_gpio(GPIO23_GPIO23),
	.name_on = NULL,
	.name_off = NULL,
	.state_on = NULL,
	.state_off = NULL,
};

static struct platform_device headset_switch_device = {
	.name            = "headset",
	.id              = 0,
	.dev             = {
		.platform_data = &headset_switch_device_data,
	},
};

static int wm8994_gpio_irq(void)
{
	int gpio = mfp_to_gpio(GPIO23_GPIO23);

	if (gpio_request(gpio, "wm8994 irq")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio);
		return -1;
	}

	gpio_direction_input(gpio);
	mdelay(1);
	gpio_free(gpio);
	return 0;
}

static void __init brownstone_init_headset(void)
{
	wm8994_gpio_irq();
	platform_device_register(&headset_switch_device);
}
#endif

#if defined(CONFIG_HDMI_DDC_EDID)
static struct i2c_board_info brownstone_twsi6_info[] =
{
 	{
 		.type			= "hdmi_edid",
 		.addr			= 0x50,
 	},
};
#endif

#ifdef CONFIG_PXA688_CAMERA

static int cam_power_set(int flag)
{
	struct clk *gate_clk = NULL;
	struct clk *dbg_clk = NULL;

	gate_clk = clk_get(NULL, "CCICGATECLK");
	if (IS_ERR(gate_clk)) {
		printk(KERN_ERR "unable to get CCICGATECLK");
		return PTR_ERR(gate_clk);
	}

	dbg_clk = clk_get(NULL, "CCICDBGCLK");
	if (IS_ERR(dbg_clk)) {
		printk(KERN_ERR "unable to get CCICDBGCLK");
		return PTR_ERR(dbg_clk);
	}

	if (flag) {
		clk_enable(gate_clk);
		clk_enable(dbg_clk);
	} else {
		clk_disable(gate_clk);
		clk_disable(dbg_clk);
	}

	clk_put(gate_clk);
	clk_put(dbg_clk);

	return 0;
}

static int cam_pmua_set(int flag)
{
	struct clk *rst_clk = NULL;

	rst_clk = clk_get(NULL, "CCICRSTCLK");
	if (IS_ERR(rst_clk)) {
		printk(KERN_ERR "unable to get CCICRSTCLK");
		return PTR_ERR(rst_clk);
	}

	if (flag)
		clk_enable(rst_clk);
	else
		clk_disable(rst_clk);

	clk_put(rst_clk);

	return 0;
}

static int sensor_power_set(int flag, int res, int eco, int sensor)
{
	/*
	 * FLAG, 1: ON, 0: OFF
	 * RES, 0, LOW RESOLUTION, 1 HIGH RESOLUTION
	 * ECO, 1: CAMERA_ECO_ON, 0: CAMERA_ECO_OFF
	 * SENSOR, 1: SUCH AS OVT CAMERA SENSORS, 0: SUCH AS GCT CAMERA SENSORS
	 */
	int cam_enable = mfp_to_gpio(MFP_PIN_GPIO67);

	cam_power_set(flag);

	if (res && sensor) {
		if (gpio_request(cam_enable, "CAM_ENABLE_HI_SENSOR")) {
			printk(KERN_ERR "Request GPIO failed, gpio: %d \n", cam_enable);
			return -EIO;
		}

		if (eco) {
			if (flag)
				gpio_direction_output(cam_enable, 0);    /* pull down camera pwdn pin to enable camera sensor */
			else
				gpio_direction_output(cam_enable, 1);    /* pull up camera pwdn pin to disable camera sensor */
		} else {
			if (flag)
				gpio_direction_output(cam_enable, 1);    /* pull up camera reset pin to enable camera sensor */
			else
				gpio_direction_output(cam_enable, 0);    /* pull down camera reset pin to disable camera sensor */
		}

		gpio_free(cam_enable);
		msleep(100);
	}

	if (eco) {
		if (flag)
			control_ldo("v_ldo3", 1, 1200000);
		else
			control_ldo("v_ldo3", 0, 1200000);
	} else {
		if (flag) {
			control_ldo("v_ldo3", 1, 1200000);
			control_ldo("v_ldo15", 1, 2800000);
			control_ldo("v_ldo14", 1, 3000000);
		} else {
			control_ldo("v_ldo14", 0, 3000000);
			control_ldo("v_ldo15", 0, 2800000);
			control_ldo("v_ldo3", 0, 1200000);
		}
	}

	return 0;
}

/* compatible without do camera sensor ECO */
static int sensor_eco_set(int eco)
{
	if (eco) {
		control_ldo("v_ldo15", 1, 2800000);
		control_ldo("v_ldo14", 1, 3000000);
	}

	return 0;
}

static struct sensor_platform_data ov5642_sensor_data = {
	.id = SENSOR_HIGH,
	.eco_set = sensor_eco_set,
	.power_set = sensor_power_set,
};

/* sensor init over */
static struct cam_platform_data cam_ops = {
	.power_set      = cam_power_set,
	.pmua_set	= cam_pmua_set,
};

static int __init brownstone_init_cam(void)
{
	cam_pmua_set(1);
	mmp2_add_cam(1, &cam_ops);
	return 0;
}
#endif

static struct i2c_board_info brownstone_twsi3_info[] =
{
#ifdef CONFIG_PXA688_CAMERA
	{
		.type			= "ov5642",
		.addr			= 0x3C,
		.platform_data		= &ov5642_sensor_data,
	},
#endif
};

#if defined(CONFIG_MMC_SDHCI_PXA)

/* MMC0 controller for SD-MMC */
static struct sdhci_pxa_platdata mmp2_sdh_platdata_mmc0 = {
	.clk_delay_cycles	= 0x1f,
	.soc_set_timing = mmp2_init_sdh,
	.flags		= PXA_FLAG_CONTROL_CLK_GATE,
};

/*MMC1 controller for Wifi*/

static mfp_cfg_t mmp2brownstone_rev1_vcxo_pins[] = {
	VCXO_REQ_AF2,
	VCXO_OUT_AF2,
};

static void mmc1_sdio_switch(unsigned int on, int with_card)
{
	uint32_t icu_int_conf, mfpr;
	uint32_t addr = APB_VIRT_BASE + 0x1e000 + 0xf0;

	if (!with_card)
		return;
	if (on) {
		/* enable I/O edge detection interrupt */
		icu_int_conf = __raw_readl(ICU_INT_CONF(23));
		__raw_writel(icu_int_conf | ICU_INT_ROUTE_PJ4_IRQ, ICU_INT_CONF(23));
	} else {
		/* disable I/O edge detection interrupt */
		icu_int_conf = __raw_readl(ICU_INT_CONF(23));
		__raw_writel(icu_int_conf & (~(ICU_INT_ROUTE_PJ4_IRQ)), ICU_INT_CONF(23));
		/* sdio function pin edge clear */
		mfpr = __raw_readl(addr);
		__raw_writel(mfpr | (1 << 6), addr);
		__raw_writel(mfpr, addr);
	}
}
static struct sdhci_pxa_platdata mmp2_sdh_platdata_mmc1 = {
	.flags		= PXA_FLAG_DISABLE_CLOCK_GATING | PXA_FLAG_CARD_PERMANENT
				| PXA_FLAG_SDIO_RESUME,
	.lp_switch	= mmc1_sdio_switch,
	.soc_set_timing = mmp2_init_sdh,
};

static struct sdhci_pxa_platdata mmp2_sdh_platdata_mmc2 = {
	.clk_delay_cycles	= 0x1f,
	.flags		= PXA_FLAG_CARD_PERMANENT | PXA_FLAG_CONTROL_CLK_GATE,
	.soc_set_timing = mmp2_init_sdh,
};

static void mmc1_set_power(unsigned int on)
{
	/* sd8787 reset - GPIO58, sd8787 1V8 - ldo12 */
	mfp_cfg_t mfp_cfg_on = MFP_CFG_LPM_DRV_HIGH(GPIO58, AF0) | MFP_PULL_LOW;
	mfp_cfg_t mfp_cfg_off = MFP_CFG_LPM_DRV_LOW(GPIO58, AF0) | MFP_PULL_LOW;

	if (on) {
		control_ldo("v_ldo12", 1, 1800000);
		mfp_config(&mfp_cfg_on, 1);
	} else {
		mfp_config(&mfp_cfg_off, 1);
		control_ldo("v_ldo12", 0, 1800000);
	}
}

static void __init brownstone_init_mmc(void)
{
#ifdef CONFIG_SD8XXX_RFKILL
	int WIB_PDn;
	int WIB_RESETn;

	/* WLAN_PD_N is always configured as HIGH while WLAN_RST_N will be controlled in mmc2_set_power */
	WIB_PDn = mfp_to_gpio(MFP_PIN_GPIO57);
	WIB_RESETn = mfp_to_gpio(MFP_PIN_GPIO58);

	if (board_is_mmp2_brownstone_rev1())
		mfp_config(ARRAY_AND_SIZE(mmp2brownstone_rev1_vcxo_pins));

	add_sd8x_rfkill_device(WIB_PDn, WIB_RESETn,
			&mmp2_sdh_platdata_mmc1.pmmc, &mmc1_set_power);
#endif
#ifndef CONFIG_MTD_NAND_PXA3xx
	/*eMMC (MMC3) pins are conflict with NAND*/
	mmp2_add_sdh(2, &mmp2_sdh_platdata_mmc2); /*eMMC*/
#endif
	mmp2_add_sdh(0, &mmp2_sdh_platdata_mmc0); /*SD/MMC*/
	mmp2_add_sdh(1, &mmp2_sdh_platdata_mmc1); /*Wifi*/
}

#endif

#ifdef CONFIG_USB_EHCI_PXA_U2H

/**********************************************************************/
#if 0 /* FSIC reference code */

int brownstone_hub_reset(void)
{
	int hub_rst = mfp_to_gpio(MFP_PIN_GPIO135);

	if (gpio_request(hub_rst, "hub_reset")) {
		printk(KERN_ERR "Request GPIO failed,"
				"gpio: %d\n", hub_rst);
		return -EIO;
	}

	gpio_direction_output(hub_rst, 0);    /* reset */
	mdelay(1);
	gpio_direction_output(hub_rst, 1);    /* out of reset */
	gpio_free(hub_rst);

	return 0;
}

int brownstone_ulpi_reset(void)
{
	int ulpi_rst = mfp_to_gpio(MFP_PIN_GPIO73);

	if (gpio_request(ulpi_rst, "ulpi_reset")) {
		printk(KERN_ERR "Request GPIO failed,"
				"gpio: %d\n", ulpi_rst);
		return -EIO;
	}

	gpio_direction_output(ulpi_rst, 0);    /* reset */
	mdelay(1);
	gpio_direction_output(ulpi_rst, 1);    /* out of reset */
	gpio_free(ulpi_rst);

	return 0;
}

static int brownstone_ulpi_phy_init(void)
{
	int ulpi_pwron = mfp_to_gpio(MFP_PIN_GPIO8);
	int ulpi_pwron2 = mfp_to_gpio(MFP_PIN_GPIO80);

	/* enable PHY power */
	if (gpio_request(ulpi_pwron, "ulpi power on")) {
		printk(KERN_ERR "Request GPIO failed,"
				"gpio: %d\n", ulpi_pwron);
		return -EIO;
	}
	gpio_direction_output(ulpi_pwron, 0);    /* enable */
	gpio_free(ulpi_pwron);


	if (gpio_request(ulpi_pwron2, "ulpi power on 2")) {
		printk(KERN_ERR "Request GPIO failed,"
				"gpio: %d\n", ulpi_pwron2);
		return -EIO;
	}

	gpio_direction_output(ulpi_pwron2, 0);    /* enable */
	gpio_free(ulpi_pwron2);

	/* pull PHY out of reset */
	brownstone_ulpi_reset();
	brownstone_hub_reset();

	return 0;
}

static int brownstone_fsic_enable(unsigned int base, unsigned phy)
{
	brownstone_ulpi_phy_init();

	mmp2_fsic_enable(base, phy);

	return 0;
}

static int brownstone_ulpi_init(unsigned int base, unsigned phy)
{
	mmp2_ulpi_init(base, phy);

	brownstone_ulpi_reset();

	return 0;
}

static struct pxa_usb_plat_info brownstone_fsic_info = {
	.plat_init	= brownstone_ulpi_init,
	.phy_init	= brownstone_fsic_enable,
	.clk_name	= "FSICCLK",
};

static void brownstone_ehci_u2h_fsic_init(void)
{
	struct platform_device *pdev = &ehci_sph3_device;
	int ret;

	ret = platform_device_add_data(pdev, &brownstone_fsic_info,
			sizeof(struct pxa_usb_plat_info));
	if (ret) {
		printk("platform_device_add_data fail: %d\n", ret);
		return;
	}

	platform_device_register(&ehci_sph3_device);
}

#endif /* FSIC reference code */

/**********************************************************************/

int set_hsic1_power(int status)
{
	static struct regulator *ldo5;
	int reset;

	if (!ldo5) {
		ldo5 = regulator_get(NULL, "v_ldo5");
		if (IS_ERR(ldo5)) {
			printk("ldo5 not found\n");
			return -EIO;
		}
		regulator_set_voltage(ldo5, 1200000, 1200000);
		regulator_enable(ldo5);
	}

	if (board_is_mmp2_brownstone_rev1()) {
		mfp_config(ARRAY_AND_SIZE(hsic_reset_gpio120_config));
		reset = mfp_to_gpio(MFP_PIN_GPIO120);
	} else {
		mfp_config(ARRAY_AND_SIZE(hsic_reset_gpio80_config));
		reset = mfp_to_gpio(MFP_PIN_GPIO80);
	}

	if (gpio_request(reset, "hsic reset")) {
		pr_err("Failed to request hsic reset gpio\n");
		return -EIO;
	}

	if (status)
		gpio_direction_output(reset, 1);
	else
		gpio_direction_output(reset, 0);

	gpio_free(reset);

	/* 5V power supply to external port */
	v5p_enable_set(status);

	return 0;
}

static int brownstone_hsic_phy_init(unsigned int base, unsigned int phy)
{
	int ret;
	ret = mmp2_hsic_enable(base, phy);
	return ret;
}

static int brownstone_hsic_plat_init(unsigned int base, unsigned int phy)
{
	int ret;
	ret = mmp2_hsic_init(base, phy);
	return ret;
}

static struct pxa_usb_plat_info brownstone_hsic1_info = {
	.phy_init	= brownstone_hsic_phy_init,
	.plat_init	= brownstone_hsic_plat_init,
	.set_power	= set_hsic1_power,
	.phy_type   = USB_PHY_TYPE_HSIC,
	.clk_name	= "HSIC1CLK",
};

static void brownstone_ehci_u2h_init(void)
{
	struct platform_device *pdev = &ehci_sph1_device;
	int ret;

	ret = platform_device_add_data(pdev, &brownstone_hsic1_info,
			sizeof(struct pxa_usb_plat_info));
	if (ret) {
		printk("platform_device_add_data fail: %d\n", ret);
		return;
	}

	platform_device_register(&ehci_sph1_device);
}
#else
#define brownstone_ehci_u2h_init()
#endif

#ifdef USE_KPC
static struct pxa27x_keypad_platform_data mmp2_keypad_info = {
	.direct_key_map = {
		KEY_BACK,
		KEY_MENU,
		KEY_HOME,
		KEY_SEARCH,
	},
	.direct_key_num = 4,
	.debounce_interval = 30,
	.active_low = 1,
};
#else
/* gpio_keys */
#define INIT_KEY(_code, _gpio, _active_low, _desc)	\
	{						\
		.code       = KEY_##_code,		\
		.gpio       = _gpio,			\
		.active_low = _active_low,		\
		.desc       = _desc,			\
		.type       = EV_KEY,			\
		.wakeup     = 1,			\
		.debounce_interval     = 0,			\
	}

static struct gpio_keys_button gpio_keys_buttons[] = {
	INIT_KEY(BACK,	mfp_to_gpio(GPIO16_GPIO16),	1,	"Back button"),
	INIT_KEY(MENU,	mfp_to_gpio(GPIO17_GPIO17),	1,	"Menu button"),
	INIT_KEY(HOME,	mfp_to_gpio(GPIO18_GPIO18),	1,	"Home button"),
	INIT_KEY(SEARCH,mfp_to_gpio(GPIO19_GPIO19),	1,	"Search button"),
};

static struct gpio_keys_platform_data gpio_keys_data = {
	.buttons = gpio_keys_buttons,
	.nbuttons = ARRAY_SIZE(gpio_keys_buttons),
	.rep = 1,
};

static struct platform_device gpio_keys = {
	.name = "gpio-keys",
	.dev  = {
		.platform_data = &gpio_keys_data,
	},
	.id   = -1,
};
#endif

/* usb vbus platform data */
static struct u2o_vbus_pdata vbus_pdata = {
	.vbus_en	= mfp_to_gpio(MFP_PIN_GPIO82),
	.vbus_irq	= IRQ_MMP2_USB_OTG,
	.reg_base	= PXA168_U2O_REGBASE,
	.reg_end	= PXA168_U2O_REGBASE + USB_REG_RANGE,
	.phy_base	= PXA168_U2O_PHYBASE,
	.phy_end	= PXA168_U2O_PHYBASE + USB_PHY_RANGE,
	.phy_init	= mmp2_init_usb_phy,
};

static struct platform_device vbus_device = {
	.name		= "u2o_vbus",
	.id		= -1,
	.dev		= {
		.platform_data	= &vbus_pdata,
	},
};

/* max8903 battery charger platform data */
static struct max8903_charger_pdata chg_pdata = {
	.gpio_500ma   = mfp_to_gpio(GPIO79_GPIO79),
	/* default charging current limitation comply to USB Spec. */
	.fast_chgcur   = MAX8903_FCHG_100MA,
};

static struct platform_device max8903_charger_device = {
	.name = "max8903-charger",
	.id = -1,
	.dev = {
		.platform_data = &chg_pdata,
	},
};

static int __init led_init(void)
{
	int led;
	led = mfp_to_gpio(MFP_PIN_GPIO74);
	if (gpio_request(led, "LED Orange")) {
		pr_err("Failed to request LED orange\n");
		return -EIO;
	}
	gpio_direction_output(led, 1);
	gpio_free(led);
	led = mfp_to_gpio(MFP_PIN_GPIO75);
	if (gpio_request(led, "LED Blue")) {
		pr_err("Failed to request LED orange\n");
		return -EIO;
	}
	gpio_direction_output(led, 1);
	gpio_free(led);
	led = mfp_to_gpio(MFP_PIN_GPIO76);
	if (gpio_request(led, "LED Red")) {
		pr_err("Failed to request LED orange\n");
		return -EIO;
	}
	gpio_direction_output(led, 1);
	gpio_free(led);
	led = mfp_to_gpio(MFP_PIN_GPIO77);
	if (gpio_request(led, "LED Green")) {
		pr_err("Failed to request LED orange\n");
		return -EIO;
	}
	gpio_direction_output(led, 1);
	gpio_free(led);

	return 0;
}

static void gps_power_on(void)
{
	int gps_rst_n;
	int gpio;

	gpio = mfp_to_gpio(MFP_PIN_GPIO49);
	if (gpio_request(gpio, "GPIO49")) {
	        pr_err("Failed to request GPIO 49\n");
	        return;
	}
	gpio_direction_input(gpio);
	gpio_free(gpio);

	gpio = mfp_to_gpio(MFP_PIN_GPIO50);
	if (gpio_request(gpio, "GPIO50")) {
	        pr_err("Failed to request GPIO 50\n");
	        return;
	}
	gpio_direction_output(gpio, 1);
	gpio_free(gpio);

	gps_rst_n = mfp_to_gpio(MFP_PIN_GPIO15);
	if (gpio_request(gps_rst_n, "gpio_gps_rst")) {
		pr_err("Failed to request gpio_gps_rst\n");
	}

	gpio_direction_output(gps_rst_n, 0);
	mdelay(1);

	gpio_free(gps_rst_n);
	printk(KERN_INFO "sirf gps chip powered on\n");
}

static void gps_power_off(void)
{
	int gps_rst_n, gps_on;

	gps_on = mfp_to_gpio(MFP_PIN_GPIO14);
	if (gpio_request(gps_on, "gpio_gps_on")) {
		pr_err("Failed to request gpio_gps_on\n");
	}

	gps_rst_n = mfp_to_gpio(MFP_PIN_GPIO15);
	if (gpio_request(gps_rst_n, "gpio_gps_rst")) {
		pr_err("Failed to request gpio_gps_rst\n");
	}

	gpio_direction_output(gps_rst_n, 0);
	gpio_direction_output(gps_on, 0);

	gpio_free(gps_on);
	gpio_free(gps_rst_n);
	printk(KERN_INFO "sirf gps chip powered off\n");
}

static void gps_reset(int flag)
{
	int gps_rst_n;

	gps_rst_n = mfp_to_gpio(MFP_PIN_GPIO15);
	if (gpio_request(gps_rst_n, "gpio_gps_rst")) {
		pr_err("Failed to request gpio_gps_rst\n");
	}

	gpio_direction_output(gps_rst_n, flag);

	gpio_free(gps_rst_n);
	printk(KERN_INFO "sirf gps chip reset\n");
}

static void gps_on_off(int flag)
{
	int gps_on;

	gps_on = mfp_to_gpio(MFP_PIN_GPIO14);
	if (gpio_request(gps_on, "gpio_gps_on")) {
		pr_err("Failed to request gpio_gps_on\n");
	}

	gpio_direction_output(gps_on, flag);

	gpio_free(gps_on);
	printk(KERN_INFO "sirf gps chip offon\n");
}

#ifdef	CONFIG_PROC_FS
#define PROC_PRINT(fmt, args...) 	do {len += sprintf(page + len, fmt, ##args); } while(0)

static char sirf_status[4] = "off";
static ssize_t sirf_read_proc(char *page, char **start, off_t off,
		int count, int *eof, void *data)
{
	int len = strlen(sirf_status);

	sprintf(page, "%s\n", sirf_status);
	return len + 1;
}

static ssize_t sirf_write_proc(struct file *filp,
		const char *buff, size_t len, loff_t *off)
{
	char messages[256];
	int flag, ret;
	char buffer[7];

	if (len > 256)
		len = 256;

	if (copy_from_user(messages, buff, len))
		return -EFAULT;

	if (strncmp(messages, "off", 3) == 0) {
		strcpy(sirf_status, "off");
		gps_power_off();
	} else if (strncmp(messages, "on", 2) == 0) {
		strcpy(sirf_status, "on");
		gps_power_on();
	} else if (strncmp(messages, "reset", 5) == 0) {
		strcpy(sirf_status, messages);
		ret = sscanf(messages, "%s %d", buffer, &flag);
		if (ret == 2)
			gps_reset(flag);
	} else if (strncmp(messages, "sirfon", 5) == 0) {
		strcpy(sirf_status, messages);
		ret = sscanf(messages, "%s %d", buffer, &flag);
		if (ret == 2)
			gps_on_off(flag);
	} else {
		printk("usage: echo {on/off} > /proc/driver/sirf\n");
	}

	return len;
}

static void create_sirf_proc_file(void)
{
	struct proc_dir_entry *sirf_proc_file =
		create_proc_entry("driver/sirf", 0644, NULL);

	if (sirf_proc_file) {
		sirf_proc_file->read_proc = sirf_read_proc;
		sirf_proc_file->write_proc = (write_proc_t  *)sirf_write_proc;
	} else
		printk(KERN_INFO "proc file create failed!\n");
}
#endif

static struct vmeta_plat_data mmp2_vmeta_pdata = {
	.set_dvfm_constraint = mmp2_vmeta_set_dvfm_constraint,
	.unset_dvfm_constraint = mmp2_vmeta_unset_dvfm_constraint,
	.init_dvfm_constraint = mmp2_vmeta_init_dvfm_constraint,
	.clean_dvfm_constraint = mmp2_vmeta_clean_dvfm_constraint,
	.decrease_core_freq = mmp2_vmeta_decrease_core_freq,
	.increase_core_freq = mmp2_vmeta_increase_core_freq,
};

static void __init brownstone_init(void)
{
	extern size_t reserving_size;
	mfp_config(ARRAY_AND_SIZE(brownstone_pin_config));

	mmp2_get_platform_version();

	/* disable LED lights */
	led_init();

	/* USIM unused, set as GPIO input, Pull-high */
	usim_init();

	/* set MAX8903 charger to max current limit */
	charger_init();

	/* on-chip devices */
	mutex_init(&ldo_mutex);
	mutex_init(&pwr_i2c_conflict_mutex);
	mmp2_add_uart(1);
	mmp2_add_uart(2);
	mmp2_add_uart(3);
	mmp2_add_rtc();
	mmp2_add_twsi(1, NULL, ARRAY_AND_SIZE(brownstone_twsi1_info));
	mmp2_add_twsi(2, NULL, ARRAY_AND_SIZE(brownstone_twsi2_info));
	mmp2_add_twsi(3, NULL, ARRAY_AND_SIZE(brownstone_twsi3_info));
	mmp2_add_twsi(4, &sensori2c_info, ARRAY_AND_SIZE(brownstone_twsi4_info));
	mmp2_add_twsi(5, NULL, ARRAY_AND_SIZE(brownstone_twsi5_info));
#if defined(CONFIG_HDMI_DDC_EDID)
	mmp2_add_twsi(6, NULL, ARRAY_AND_SIZE(brownstone_twsi6_info));
#endif
	brownstone_fixed_regulator();
	regulator_has_full_constraints();
	mmp2_add_lcd();

	/* backlight */
	mmp2_add_pwm(3);
	platform_device_register(&brownstone_lcd_backlight_devices);

	mmp2_add_imm();
	mmp2_add_sspa(1);
	mmp2_add_sspa(2);
	mmp2_add_audiosram();
	mmp2_add_vmeta(&mmp2_vmeta_pdata);
	mmp2_add_thermal_sensor();

#ifdef USE_KPC
	mmp2_add_keypad(&mmp2_keypad_info);
#else
	platform_device_register(&gpio_keys);
#endif

	mmp2_add_fuse();

	mmp2_add_hdmi(&mmp2_hdmi_info);
#ifdef CONFIG_PXA688_CAMERA
        brownstone_init_cam();
#endif

#ifdef CONFIG_SWITCH_HEADSET_HOST_GPIO
	brownstone_init_headset();
#endif

#ifdef CONFIG_ANDROID_PMEM
	pxa_add_pmem("pmem", reserving_size, 0, 1, 1);
	pxa_add_pmem("pmem_adsp", 0, 0, 0, 0);
#endif

#ifdef CONFIG_DVFM_MMP2
	mmp2_add_freq();
#endif

#if defined(CONFIG_MMC_SDHCI_PXA)
	brownstone_init_mmc();
#endif
	platform_device_register(&vbus_device);
	platform_device_register(&u2o_device);
	platform_device_register(&otg_device);
	platform_device_register(&ehci_u2o_device);
	brownstone_ehci_u2h_init();
#ifdef CONFIG_USB_ANDROID
	android_add_usb_devices();
#endif
	if (board_is_mmp2_brownstone_rev1()) {
		/* only brownstone v1 using GPIO80 as charger control pin */
		chg_pdata.gpio_2a_n = mfp_to_gpio(GPIO80_GPIO80);
		/* FIXME, disable chg_max8903 and set current to max here,
		 * due to brownstone v1 need to draw high current from
		 * usb otg port w/o battery */
		brownstone_power_data.chg_max8903_en = 0;
		chg_pdata.fast_chgcur = MAX8903_FCHG_2000MA;
	}
	platform_device_register(&max8903_charger_device);

	gps_power_off();
#ifdef	CONFIG_PROC_FS
	/* create proc for sirf control */
	create_sirf_proc_file();
#endif

	if (cpu_is_mmp2() && !cpu_is_mmp2_z0() && !cpu_is_mmp2_z1()) {
		pxa_register_device(&mmp2_device_zsp, NULL, 0);
	}

}

MACHINE_START(BROWNSTONE, "Brownstone Development Platform")
	.phys_io	= APB_PHYS_BASE,
	.boot_params	= 0x00000100,
	.io_pg_offst	= (APB_VIRT_BASE >> 18) & 0xfffc,
	.map_io		= pxa_map_io,
	.nr_irqs	= BROWNSTONE_NR_IRQS,
	.init_irq	= mmp2_init_irq,
	.timer          = &mmp2_timer,
	.init_machine   = brownstone_init,
MACHINE_END
