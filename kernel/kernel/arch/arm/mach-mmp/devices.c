/*
 * linux/arch/arm/mach-mmp/devices.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/delay.h>

#include <asm/irq.h>
#include <mach/devices.h>

#include <linux/usb.h>
#include <linux/usb/otg.h>
#include <plat/pxa_u2o.h>

int __init pxa_register_device(struct pxa_device_desc *desc,
				void *data, size_t size)
{
	struct platform_device *pdev;
	struct resource res[2 + MAX_RESOURCE_DMA];
	int i, ret = 0, nres = 0;

	pdev = platform_device_alloc(desc->drv_name, desc->id);
	if (pdev == NULL)
		return -ENOMEM;

	pdev->dev.coherent_dma_mask = DMA_BIT_MASK(32);

	memset(res, 0, sizeof(res));

	if (desc->start != -1ul && desc->size > 0) {
		res[nres].start	= desc->start;
		res[nres].end	= desc->start + desc->size - 1;
		res[nres].flags	= IORESOURCE_MEM;
		nres++;
	}

	if (desc->irq != NO_IRQ) {
		res[nres].start	= desc->irq;
		res[nres].end	= desc->irq;
		res[nres].flags	= IORESOURCE_IRQ;
		nres++;
	}

	for (i = 0; i < MAX_RESOURCE_DMA; i++, nres++) {
		if (desc->dma[i] == 0)
			break;

		res[nres].start	= desc->dma[i];
		res[nres].end	= desc->dma[i];
		res[nres].flags	= IORESOURCE_DMA;
	}

	ret = platform_device_add_resources(pdev, res, nres);
	if (ret) {
		platform_device_put(pdev);
		return ret;
	}

	if (data && size) {
		ret = platform_device_add_data(pdev, data, size);
		if (ret) {
			platform_device_put(pdev);
			return ret;
		}
	}

	return platform_device_add(pdev);
}

#if defined(CONFIG_USB_EHCI_PXA_U2H)||defined(CONFIG_USB_PXA_U2O)
int pxa_usb_phy_init(unsigned int base, unsigned int phy)
{
	static int init_done;
	int count;

	if (init_done)
		printk(KERN_DEBUG "re-init phy\n");

	/* enable the pull-up */
	if (phy == USB_PHY_PXA910)
		u2o_set(base, UTMI_CTRL,
			(1 << UTMI_CTRL_INPKT_DELAY_SOF_SHIFT)
			| (1 << UTMI_CTRL_PU_REF_SHIFT));

	u2o_set(base, UTMI_CTRL, 1 << UTMI_CTRL_PLL_PWR_UP_SHIFT);
	mdelay(1);

	/* Initialize the USB PHY */
	u2o_set(base, UTMI_CTRL, 1 << UTMI_CTRL_INPKT_DELAY_SOF_SHIFT);

	u2o_clear(base, UTMI_T0, 0x8000);
	mdelay(1);

	u2o_clear(base, UTMI_PLL, UTMI_PLL_FBDIV_MASK | UTMI_PLL_REFDIV_MASK
			| UTMI_PLL_ICP_MASK | UTMI_PLL_KVCO_MASK);
	u2o_set(base, UTMI_PLL, 0xee << UTMI_PLL_FBDIV_SHIFT
			| 0xb << UTMI_PLL_REFDIV_SHIFT
			| 0x3 << UTMI_PLL_PLLVDD18_SHIFT
			| 0x3 << UTMI_PLL_PLLVDD12_SHIFT
			| 0x3 << UTMI_PLL_PLLCALI12_SHIFT
			| 0x1 << UTMI_PLL_ICP_SHIFT
			| 0x3 << UTMI_PLL_KVCO_SHIFT);
	mdelay(1);

	/* UTMI Tx */
	u2o_clear(base, UTMI_TX, (UTMI_TX_TXVDD12_MASK)
			| (UTMI_TX_CK60_PHSEL_MASK));
	u2o_set(base, UTMI_TX, 3 << UTMI_TX_TXVDD12_SHIFT
			| 4 << UTMI_TX_CK60_PHSEL_SHIFT);

	/* UTMI Rx */
	u2o_clear(base, UTMI_RX, UTMI_RX_SQ_THRESH_MASK);
	u2o_set(base, UTMI_RX, 7 << UTMI_RX_SQ_THRESH_SHIFT);
	mdelay(1);

	/* calibrate */
	count = 1000;
	while (((u2o_get(base, UTMI_PLL) & PLL_READY) == 0) && count--);
	if (count <= 0)
		pr_info("%s %d: calibrate timeout, UTMI_PLL %x\n",
			__func__, __LINE__, u2o_get(base, UTMI_PLL));

	/* toggle VCOCAL_START bit of UTMI_PLL */
	udelay(200);
	u2o_set(base, UTMI_PLL, VCOCAL_START);
	udelay(40);
	u2o_clear(base, UTMI_PLL, VCOCAL_START);

	/* toggle REG_RCAL_START bit of UTMI_TX */
	udelay(200);
	u2o_set(base, UTMI_TX, REG_RCAL_START);
	udelay(40);
	u2o_clear(base, UTMI_TX, REG_RCAL_START);

	/* make sure phy is ready */
	count = 1000;
	while (((u2o_get(base, UTMI_PLL) & PLL_READY) == 0) && count--);
	if (count <= 0)
		pr_info("%s %d: calibrate timeout, UTMI_PLL %x\n",
			__func__, __LINE__, u2o_get(base, UTMI_PLL));

	if (phy == USB_PHY_PXA168) {
		u2o_set(base, UTMI_CTL1, 1 << 5);
		/* turn on UTMI PHY OTG extension */
		u2o_write(base, UTMI_T5, 1);
	}

	init_done = 1;
	return 0;
}

int pxa_usb_phy_deinit(unsigned int base, unsigned int phy)
{
	if (phy == USB_PHY_PXA168)
		u2o_clear(base, UTMI_OTG_ADDON, UTMI_OTG_ADDON_OTG_ON);

	u2o_clear(base, UTMI_CTRL, UTMI_CTRL_RXBUF_PDWN);
	u2o_clear(base, UTMI_CTRL, UTMI_CTRL_TXBUF_PDWN);
	u2o_clear(base, UTMI_CTRL, UTMI_CTRL_USB_CLK_EN);
	u2o_clear(base, UTMI_CTRL, 1 << UTMI_CTRL_PLL_PWR_UP_SHIFT);

	return 0;
}

#endif
