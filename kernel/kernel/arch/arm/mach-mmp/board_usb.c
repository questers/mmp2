#ifdef CONFIG_USB_EHCI_PXA_U2H
#if 1 /* FSIC reference code */

static int g50_hub_reset(void)
{
	int hub_rst = GPIO(124);

	if (gpio_request(hub_rst, "hub_reset")) {
		printk(KERN_ERR "Request GPIO failed,"
				"gpio: %d\n", hub_rst);
		return -EIO;
	}

	gpio_direction_output(hub_rst, 0);    /* reset */
	mdelay(1);
	gpio_direction_output(hub_rst, 1);    /* out of reset */
	gpio_free(hub_rst);
	mdelay(1);

	return 0;
}

static int g50_ulpi_reset(void)
{
	int ulpi_rst = GPIO(4);

	if (gpio_request(ulpi_rst, "ulpi_reset")) {
		printk(KERN_ERR "Request GPIO failed,"
				"gpio: %d\n", ulpi_rst);
		return -EIO;
	}

	gpio_direction_output(ulpi_rst, 0);    /* reset */
	mdelay(1);
	gpio_direction_output(ulpi_rst, 1);    /* out of reset */
	gpio_free(ulpi_rst);

	return 0;
}

#define ULPI_CLK_PWM_ID	1	/* PWM2 for ulpi input clock;PWM[0,1,2,3] */
#define ULPI_CLK_PWM_PERIOD	(80) /* 13M */
#define ULPI_CLK_PWM_DUTY	(40)
static int g50_ulpi_pwmclk_init(void)
{
	struct pwm_device *ulpi_pwm;
	int period_ns = ULPI_CLK_PWM_PERIOD;
	int duty_ns = ULPI_CLK_PWM_DUTY;
	ulpi_pwm = pwm_request(ULPI_CLK_PWM_ID, "ulpi input clock");
	if (IS_ERR(ulpi_pwm)) {
		printk(KERN_ERR "unable to request PWM[%d] for ulpi clock",
				ULPI_CLK_PWM_ID + 1);
		goto err_pwm;
	}
	printk(KERN_DEBUG "%s:enable ulpi clock\n", __func__);
	/* enable PWM2 */
	pwm_enable(ulpi_pwm);
	pwm_config(ulpi_pwm, duty_ns, period_ns);
	pwm_free(ulpi_pwm);
err_pwm:
	return 0;
}

static int g50_ulpi_phy_init(unsigned int base, unsigned int phy)
{
	int ret;
	/* init ulpi clock */
	ret = g50_ulpi_pwmclk_init();
	if (ret)
		return ret;
	ret = g50_ulpi_reset();
	if (ret)
		return ret;
	/* select ulpi in sph controller */
	ret = mmp2_fsic_enable(base, phy);
	if (ret)
		return ret;
	ret = mmp2_ulpi_init(base, phy);
	if (ret)
		return ret;
	ret = g50_ulpi_reset();
	return ret;
}

static int g50_ulpi_plat_init(unsigned int base, unsigned int phy)
{
	int ret;
	ret = g50_hub_reset();
	return ret;
}

static int g50_ulpi_set_power(int on)
{
	int ulpi_rst = GPIO(4);
	int ret = 0;

	if (gpio_request(ulpi_rst, "ulpi_reset")) {
		printk(KERN_ERR "Request GPIO failed,"
				"gpio: %d\n", ulpi_rst);
		return -EIO;
	}

	if (on)
		gpio_direction_output(ulpi_rst, 1);
	else
		gpio_direction_output(ulpi_rst, 0);

	gpio_free(ulpi_rst);

	return ret;
}

static struct pxa_usb_plat_info g50_fsic_info = {
	.plat_init	= g50_ulpi_plat_init,
	.phy_init	= g50_ulpi_phy_init,
	.set_power	= g50_ulpi_set_power,
	.phy_type	= USB_PHY_TYPE_ULPI,
	.clk_name	= "FSICCLK",
};

static void g50_ehci_u2h_fsic_init(void)
{
	struct platform_device *pdev = &ehci_sph3_device;
	int ret;

	ret = platform_device_add_data(pdev, &g50_fsic_info,
			sizeof(struct pxa_usb_plat_info));
	if (ret) {
		printk("platform_device_add_data fail: %d\n", ret);
		return;
	}

	platform_device_register(&ehci_sph3_device);
}

#endif /* FSIC reference code */

#if 0
int set_hsic1_power(int status)
{
	static struct regulator *ldo5;
	int reset;

	if (!ldo5) {
		ldo5 = regulator_get(NULL, "v_ldo5");
		if (IS_ERR(ldo5)) {
			printk("ldo5 not found\n");
			return -EIO;
		}
		regulator_set_voltage(ldo5, 1200000, 1200000);
		regulator_enable(ldo5);
	}

	reset = mfp_to_gpio(MFP_PIN_GPIO92);

	if (gpio_request(reset, "hsic reset")) {
		pr_err("Failed to request hsic reset gpio\n");
		return -EIO;
	}

	if (status)
		gpio_direction_output(reset, 1);
	else
		gpio_direction_output(reset, 0);

	gpio_free(reset);

	/* 5V power supply to external port */
	v5p_enable_set(status);

	return 0;
}

static struct pxa_usb_plat_info g50_hsic1_info = {
	.plat_init	= mmp2_hsic_init,
	.phy_init	= mmp2_hsic_enable,
	.set_power	= set_hsic1_power,
	.clk_name	= "HSIC1CLK",
};

static void g50_ehci_u2h_init(void)
{
	struct platform_device *pdev = &ehci_sph1_device;
	int ret;

	ret = platform_device_add_data(pdev, &g50_hsic1_info,
			sizeof(struct pxa_usb_plat_info));
	if (ret) {
		printk("platform_device_add_data fail: %d\n", ret);
		return;
	}

	platform_device_register(&ehci_sph1_device);


	u2h_post_init();
	
}
#endif

#else
//#define g50_ehci_u2h_init()
#define g50_ehci_u2h_fsic_init()
#endif

#define mmp2_ehci_u2h_fsic_init g50_ehci_u2h_fsic_init

#ifdef CONFIG_PXA_VBUS
/* usb vbus platform data */
static struct u2o_vbus_pdata vbus_pdata = {
	.vbus_en	= GPIO(6),
	.vbus_irq	= IRQ_MMP2_USB_OTG,
	.vbus_oc	= GPIO(7),
	.reg_base	= PXA168_U2O_REGBASE,
	.reg_end	= PXA168_U2O_REGBASE + USB_REG_RANGE,
	.phy_base	= PXA168_U2O_PHYBASE,
	.phy_end	= PXA168_U2O_PHYBASE + USB_PHY_RANGE,
	.phy_init	= mmp2_init_usb_phy,
};

static struct platform_device vbus_device = {
	.name		= "u2o_vbus",
	.id		= -1,
	.dev		= {
		.platform_data	= &vbus_pdata,
	},
};
#endif

