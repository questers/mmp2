/*
 *  linux/arch/arm/mach-mmp/jasper.c
 *
 *  Support for the Marvell Jasper Development Platform.
 *
 *  Copyright (C) 2009-2010 Marvell International Ltd.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  publishhed by the Free Software Foundation.
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/io.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/regulator/machine.h>
#include <linux/regulator/max8649.h>
#include <linux/mfd/max8925.h>
#include <linux/interrupt.h>
#include <linux/regulator/fixed.h> 
#include <linux/mfd/wm8994/pdata.h>
#include <linux/usb.h>
#include <linux/usb/otg.h>
#include <linux/clk.h>
#include <linux/switch.h>
#include <linux/synaptics_i2c_rmi.h>
#include <linux/android_pmem.h>

#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <mach/addr-map.h>
#include <mach/mfp-mmp2.h>
#include <mach/mmp2.h>
#include <mach/irqs.h>
#include <mach/regs-mpmu.h>
#include <mach/tc35876x.h>
#include <mach/pxa168fb.h>
#include <mach/mrvl_hdmitx.h>
#include <mach/regs-icu.h>
#include <mach/mmp2_plat_ver.h>
#include <plat/pxa27x_keypad.h>
#include <plat/generic.h>
#include <plat/pxa_u2o.h>
#include <plat/vbus.h>
#include <linux/power/max8903.h>
#include <mach/uio_hdmi.h>
#include "common.h"

#ifdef CONFIG_SD8XXX_RFKILL
#include <linux/sd8x_rfkill.h>
#endif

#ifdef CONFIG_USB_ANDROID
/* Platform specific android usb settings.
 * Default settings in mmp2_android_usb.h */
#define USB_VENDOR_NAME		"Marvell"
/* FIXME: Marvell(VID:0x1286) is not already in the adb vendor list yet.
 * So take Google's VID here to make usb workable with adb tool */
#define USB_VENDOR_ID		0x18d1
#define USB_PRODUCT_NAME	"Bonnell"
#define USB_SERIAL_NUMBER	"MMP2_BN_001"
#include <mach/mmp2_android_usb.h>
#endif

#define JASPER_NR_IRQS		(IRQ_BOARD_START + 48)

static unsigned long jasper_pin_config[] __initdata = {
	/* UART1 */
	GPIO29_UART1_RXD,
	GPIO30_UART1_TXD,

	/* UART3 */
	GPIO51_UART3_RXD,
	GPIO52_UART3_TXD,

	/* CCIC1/CAM1 */	//Support OV5642
	GPIO59_CCIC_IN7,
	GPIO60_CCIC_IN6,
	GPIO61_CCIC_IN5,
	GPIO62_CCIC_IN4,
	GPIO63_CCIC_IN3,
	GPIO64_CCIC_IN2,
	GPIO65_CCIC_IN1,
	GPIO66_CCIC_IN0,
	GPIO67_CAM_HSYNC,
	GPIO68_CAM_VSYNC,
	GPIO69_CAM_MCLK,
	GPIO70_CAM_PCLK,

	/* TWSI1 */
	TWSI1_SCL,
	TWSI1_SDA,

	/* TWSI2 */
	GPIO43_TWSI2_SCL,
	GPIO44_TWSI2_SDA,

	/* TWSI3 */
	GPIO71_TWSI3_SCL,
	GPIO72_TWSI3_SDA,

	/* TWSI4 */
	TWSI4_SCL,
	TWSI4_SDA,

	/* TWSI5 */
	GPIO99_TWSI5_SCL,
	GPIO100_TWSI5_SDA,

	/* TWSI6 */
	GPIO97_TWSI6_SCL,
	GPIO98_TWSI6_SDA,
 
	/* SSPA1 (I2S) */
	GPIO23_GPIO23,
 	GPIO24_I2S_SYSCLK,	
 	GPIO25_I2S_BITCLK,		
 	GPIO26_I2S_SYNC,	
 	GPIO27_I2S_DATA_OUT,	
 	GPIO28_I2S_SDATA_IN,
 
 	/* SSPA2 */ 
 	GPIO33_SSPA2_CLK,
 	GPIO34_SSPA2_FRM,
 	GPIO35_SSPA2_TXD,
 	GPIO36_SSPA2_RXD,

	/*Keypad*/
	GPIO00_KP_MKIN0,
	GPIO01_KP_MKOUT0,
	GPIO02_KP_MKIN1,
	GPIO03_KP_MKOUT1,
	GPIO04_KP_MKIN2,
	GPIO05_KP_MKOUT2,

	/* DFI */
	GPIO168_DFI_D0,
	GPIO167_DFI_D1,
	GPIO166_DFI_D2,
	GPIO165_DFI_D3,
	GPIO107_DFI_D4,
	GPIO106_DFI_D5,
	GPIO105_DFI_D6,
	GPIO104_DFI_D7,
	GPIO111_DFI_D8,
	GPIO164_DFI_D9,
	GPIO163_DFI_D10,
	GPIO162_DFI_D11,
	GPIO161_DFI_D12,
	GPIO110_DFI_D13,
	GPIO109_DFI_D14,
	GPIO108_DFI_D15,
	GPIO143_ND_nCS0,
	GPIO144_ND_nCS1,
	GPIO147_ND_nWE,
	GPIO148_ND_nRE,
	GPIO150_ND_ALE,
	GPIO149_ND_CLE,
	GPIO112_ND_RDY0,
	GPIO160_ND_RDY1,
	GPIO154_SMC_IRQ,

	/* OTG */
	GPIO82_GPIO82,

	/* LCD */
	GPIO94_SPI_DCLK,
	GPIO95_SPI_CS0,
	GPIO96_SPI_DIN,
	GPIO83_LCD_RST,
	GPIO114_MN_CLK_OUT,
	CLK_REQ,

	/* Touch */
	GPIO101_GPIO101,

	/* PMIC */
	PMIC_PMIC_INT | MFP_LPM_EDGE_FALL,
	GPIO45_WM8994_LDOEN,
	GPIO46_HDMI_DET_PULL_HIGH,

	/* GPIO */
	GPIO126_GPIO126,
	GPIO127_GPIO127,
	GPIO128_GPIO128,

	/* MMC0 */
	GPIO131_MMC1_DAT3,
	GPIO132_MMC1_DAT2,
	GPIO133_MMC1_DAT1,
	GPIO134_MMC1_DAT0,
	GPIO136_MMC1_CMD,
	GPIO139_MMC1_CLK,
	GPIO140_MMC1_CD,
	GPIO141_MMC1_WP,

	/* MMC1 */
	GPIO37_MMC2_DAT3,
	GPIO38_MMC2_DAT2,
	GPIO39_MMC2_DAT1,
	GPIO40_MMC2_DAT0,
	GPIO41_MMC2_CMD,
	GPIO42_MMC2_CLK,

	GPIO57_GPIO57,
	GPIO58_GPIO58,

	/* MMC2 */
	GPIO165_MMC3_DAT7,
	GPIO162_MMC3_DAT6,
	GPIO166_MMC3_DAT5,
	GPIO163_MMC3_DAT4,
	GPIO167_MMC3_DAT3,
	GPIO164_MMC3_DAT2,
	GPIO168_MMC3_DAT1,
	GPIO111_MMC3_DAT0,
	GPIO112_MMC3_CMD,
	GPIO151_MMC3_CLK,
};


#if defined(CONFIG_REGULATOR_WM8994) && defined(CONFIG_REGULATOR_WM8994)
static int wm8994_ldoen(void)
{
	int gpio = mfp_to_gpio(GPIO45_WM8994_LDOEN);

	if (gpio_request(gpio, "wm8994 ldoen gpio")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio);
		return -1;
	}

	gpio_direction_output(gpio, 1);
	mdelay(1);
	gpio_free(gpio);

	return 0;
}
#endif

#if defined(CONFIG_TC35876X)
extern int tc35876x_write32(u16 reg, u32 val);
extern int tc35876x_write16(u16 reg, u16 val);
extern int tc35876x_read16(u16 reg, u16 *pval);
extern int tc35876x_read32(u16 reg, u32 *pval);

static int tc358762_reset(void)
{
	int gpio = mfp_to_gpio(GPIO83_LCD_RST);

	if (gpio_request(gpio, "lcd reset gpio")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio);
		return -1;
	}

	gpio_direction_output(gpio, 0);
	mdelay(1);
	gpio_direction_output(gpio, 1);
	mdelay(1);

	gpio_free(gpio);

	return 0;
}

static int tc358762_poweron(void)
{
	tc35876x_write32(0x47c, 0x0);

	mdelay(2);

	return 1;
}

#define TC358762_CHIPID_REG	0x4A0
#define TC358762_CHIPID		0x6200
static int tc358762_init(void)
{
	u16 chip_id = 0;
	u32 *vaddr;
	u32 reg;
	int status;
	int gpio = mfp_to_gpio(GPIO89_LCD_5V_BIAS);

	if (gpio_request(gpio, "lcd bias 5v gpio")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio);
		return -1;
	}
	gpio_direction_output(gpio, 1);
	mdelay(1);
	gpio_free(gpio);

	/*Enable the M/N_CLK_OUT*/
	reg = __raw_readl(MPMU_ACGR);
	reg |= 1<<9;
	__raw_writel(reg, MPMU_ACGR);

	/* Set the M/N value and re-enable the clock gate register*/
	__raw_writel(0x00020001, MPMU_GPCR);

	vaddr = ioremap(0xd401e160, 4);
	reg = readl(vaddr);
	reg |= 0x4000;
	writel(reg, vaddr);
	iounmap(vaddr);

	tc358762_reset();

	tc358762_poweron();
	status = tc35876x_read16(TC358762_CHIPID_REG, &chip_id);
	if (status || (chip_id != TC358762_CHIPID)) {
		printk("%s fail, chip not available\n", __func__);
		return -EAGAIN;
	} else
		printk("%s chip_id 0x%x\n", __func__, chip_id);

	return 0;
}

#define LCDCTRL		0x420
#define PXLFMT		0x414
static int dsi_set_tc358762(struct pxa168fb_info *fbi)
{
	int status;
	u16 chip_id;
	struct fb_var_screeninfo *var = &(fbi->fb_info->var);
	/*
	 * tc35876x should have been powered on
	 * tc35876x_write32(0x47c, 0x0);
	 */
	status = tc35876x_read16(TC358762_CHIPID_REG, &chip_id);
	if ((status < 0) || (chip_id != TC358762_CHIPID)) {
		printk(KERN_WARNING "tc35876x unavailable!\n");
		return -1;
	} else {
		printk(KERN_INFO "tc35876x(chip id:0x%02x) detected.\n", chip_id);
	}

	tc35876x_write16(0x0210, 0x7); /* 2 Lane*/
	mdelay(10);

	/* Set PLL to 117 MHz */
	tc35876x_write32(0x0470, 0xb8230000);	/* FIXME */
	mdelay(10);

	/* Configure the Pixel Clock Divider (for 32 MHz pixel clock) */
	tc35876x_write32(0x0464, 0x400);
	mdelay(10);

	/* Start the DSI RX function */
	tc35876x_write32(0x0204, 0x1);
	mdelay(10);
	/* Analog Timer register programming (lane 0) */
	tc35876x_write32(0x0144, 0x0);/*Set to zero (per bring up procedure doc)*/
	mdelay(10);

	/* Analog Timer register programming (lane 1) */
	tc35876x_write32(0x0148, 0x0);/*Set to zero (per bring up procedure doc)*/
	mdelay(10);

	/*Set asserting period for the duration between LP-00 detect and
	  High Speed data reception for each lane. (This is for LANE 0)
	  Set between 85ns + 6*UI and 145 ns + 10*UI based on HSBCLK*/
	tc35876x_write32(0x0164, 0x0a);
	mdelay(10);

	/*Set asserting period for the duration between LP-00 detect and
	  High Speed data reception for each lane. (This is for LANE 1)
	  Set between 85ns + 6*UI and 145 ns + 10*UI based on HSBCLK*/
	tc35876x_write32(0x0168, 0x0a);		/* FIXME */
	mdelay(10);

	/*Setup DPI register for the color depth of the LCD panel being used - 24bpp output*/
	tc35876x_write32(LCDCTRL, 0x150);
	mdelay(10);

	/*Setup SPI control register
	  The top 16 bits are the SPIRCMR register
	  The bottom 16 bits are the SPICTRL register*/
	tc35876x_write32(0x0450, 0x3FF0000);/*Address of SPIRCMR/SPICTRL registers*/
	mdelay(10);

	/*Setup specific Burst or Non Burst mode
	  No code setup for now as these registers are not applicable to non-burst mode*/
	/*Start the DSI-RX PPI function*/
	tc35876x_write32(0x0104, 0x1);/*Address of STARTPPI registers*/
	mdelay(10);

	/*Set up platform specific timings for high speed mode operation*/
	/*CLW_DLYCNTRL = 20pS*/
	tc35876x_write32(0x0020, 0x2102);/*Address of CLW_DPHYCONTRX register*/
	mdelay(10);

	/*D0W_DLYCNTRL = 10pS*/
	tc35876x_write32(0x0024, 0x2002);/*Address of D0W_DPHYCONTRX register*/
	mdelay(10);

	/*D1W_DLYCNTRL = 10pS*/
	tc35876x_write32(0x0028, 0x2002);/*Address of D1W_DPHYCONTRX register*/
	mdelay(10);

	/*Start the DSI RX function (docs imply this needs to be set again)*/
	tc35876x_write32(0x0204, 0x1);/*Address of STARTDSI register*/
	mdelay(10);

	/*EXTRA TOSHIBA CODE*/

	/* FIXME - MUST write as 32bit rather than 16bit. otherwise bit[1] can't be set. the value will be 0x150 */
	tc35876x_write32(LCDCTRL, 0x152);
	mdelay(10);

	tc35876x_write16(0x0424, var->hsync_len);/*HSR and HBFR*/
	mdelay(10);
	tc35876x_write16(0x0426, var->left_margin);/*HSR and HBFR*/
	mdelay(10);
	tc35876x_write16(0x0428, var->xres);/*HDISPR*/
	mdelay(10);

	tc35876x_write16(0x042A, var->right_margin);/*HFPR*/
	mdelay(10);

	tc35876x_write16(0x042C, var->vsync_len);/*VSR and VBFR*/
	mdelay(10);

	tc35876x_write16(0x042E, var->upper_margin);/*VSR and VBFR*/
	mdelay(10);

	tc35876x_write16(0x0430, var->yres);/*VDISPR and VFPR*/
	mdelay(10);

	tc35876x_write16(0x0432, var->lower_margin);/*VDISPR and VFPR*/
	mdelay(10);

	return 0;
}

static void dsi_dump_tc358762(struct pxa168fb_info *fbi)
{
	u16 val_16;
	u32 val_32;

	tc35876x_read32(0x47c, &val_32);
	printk("%s: tc35876x - 0x47c = 0x%x\n", __func__, val_32);
	tc35876x_read32(0x0210, &val_32);
	printk("%s: tc35876x - 0x0210 = 0x%x\n", __func__, val_32);

	tc35876x_read32(0x0470, &val_32);
	printk("%s: tc35876x - 0x0470 = 0x%x\n", __func__, val_32);
	tc35876x_read32(0x0464, &val_32);
	printk("%s: tc35876x - 0x0464 = 0x%x\n", __func__, val_32);
	tc35876x_read32(0x0204, &val_32);	/* should be 16bit according to spec */
	printk("%s: tc35876x - 0x0204 = 0x%x\n", __func__, val_32);
	tc35876x_read32(0x0144, &val_32);
	printk("%s: tc35876x - 0x0144 = 0x%x\n", __func__, val_32);
	tc35876x_read32(0x0148, &val_32);
	printk("%s: tc35876x - 0x0148 = 0x%x\n", __func__, val_32);
	tc35876x_read32(0x0164, &val_32);
	printk("%s: tc35876x - 0x0164 = 0x%x\n", __func__, val_32);
	tc35876x_read32(0x0168, &val_32);
	printk("%s: tc35876x - 0x0168 = 0x%x\n", __func__, val_32);

	tc35876x_read32(LCDCTRL, &val_32);	/* should be 16bit according to spec */
	printk("%s: tc35876x - LCDCTRL(0x0420) = 0x%x\n", __func__, val_32);

	tc35876x_read32(0x0450, &val_32);
	printk("%s: tc35876x - 0x0450 = 0x%x\n", __func__, val_32);
	tc35876x_read32(0x0104, &val_32);
	printk("%s: tc35876x - 0x0104 = 0x%x\n", __func__, val_32);
	tc35876x_read32(0x0020, &val_32);
	printk("%s: tc35876x - 0x0020 = 0x%x\n", __func__, val_32);
	tc35876x_read32(0x0024, &val_32);
	printk("%s: tc35876x - 0x0024 = 0x%x\n", __func__, val_32);
	tc35876x_read32(0x0028, &val_32);
	printk("%s: tc35876x - 0x0028 = 0x%x\n", __func__, val_32);
	tc35876x_read32(0x0204, &val_32);
	printk("%s: tc35876x - 0x0204 = 0x%x\n", __func__, val_32);

	tc35876x_read32(LCDCTRL, &val_32);
	printk("%s: tc35876x - LCDCTRL(0x0420) = 0x%x\n", __func__, val_32);

	tc35876x_read32(PXLFMT, &val_32);
	printk("%s: tc35876x - PXLFMT (0x0414) = 0x%x\n", __func__, val_32);

	tc35876x_read16(0x0424, &val_16);
	printk("%s: tc35876x - 0x0424 = 0x%x\n", __func__, val_16);
	tc35876x_read16(0x0426, &val_16);
	printk("%s: tc35876x - 0x0426 = 0x%x\n", __func__, val_16);
	tc35876x_read16(0x0428, &val_16);
	printk("%s: tc35876x - 0x0428 = 0x%x\n", __func__, val_16);
	tc35876x_read16(0x042A, &val_16);
	printk("%s: tc35876x - 0x042A = 0x%x\n", __func__, val_16);
	tc35876x_read16(0x042C, &val_16);
	printk("%s: tc35876x - 0x042C = 0x%x\n", __func__, val_16);
	tc35876x_read16(0x042E, &val_16);
	printk("%s: tc35876x - 0x042E = 0x%x\n", __func__, val_16);
	tc35876x_read16(0x0430, &val_16);
	printk("%s: tc35876x - 0x0430 = 0x%x\n", __func__, val_16);
	tc35876x_read16(0x0432, &val_16);
	printk("%s: tc35876x - 0x0432 = 0x%x\n", __func__, val_16);
}

static struct tc35876x_platform_data tc358762_data = {
	.id		= TC358762_CHIPID,
	.id_reg		= TC358762_CHIPID_REG,
	.platform_init	= tc358762_init,
};
#endif

#ifdef CONFIG_FB_SECOND_PANEL_DSI
unsigned char dsi_cmd_sharp [][30] = {
	{ 9, 0,0,0,0,	0xF1,   0x5A,   0x5A,   0x00,   0x00 },
	{ 9, 0,0,0,0,	0xFC,   0x5A,   0x5A,   0x00,   0x00 },
	{ 10, 0,0,0,0,	0xB7,   0x00,   0x11,   0x11,   0x00,   0x00 },
	{ 9, 0,0,0,0,	0xB8,   0x2D,   0x21,   0x00,   0x00 },
	{ 9, 0,0,0,0,	0xB9,   0x00,   0x06,   0x00,   0x00 },
	{ 11, 0,0,0,0,	0x2A,   0x00,   0x00,   0x01,   0xDF,   0x00,   0x00 },
	{ 11, 0,0,0,0,	0x2B,   0x00,   0x00,   0x02,   0x7F,   0x00,   0x00 },
	{ 0x04, 0x05,   0x11,   0x00,   0x00 },
	{ 0x01 },

	{ 17, 0,0,0,0,  0xF4,   0x00,   0x23,   0x00,   0x64,   0x5C,   0x00,   0x64,
		0x5C,   0x00,   0x00,   0x00,   0x00 },
	{ 17, 0,0,0,0,  0xF5,   0x00,   0x00,   0x0E,   0x00,   0x04,   0x02,   0x03,
		0x03,   0x03,   0x03,   0x00,   0x00 },
	{ 10, 0,0,0,0,  0xEE,   0x32,   0x29,   0x00,   0x00,   0x00 },
	{ 16, 0,0,0,0,  0xF2,   0x00,   0x30,   0x88,   0x88,   0x57,   0x57,   0x10,
		0x00,   0x04,   0x00,   0x00 },
	{ 19, 0,0,0,0,  0xF3,   0x00,   0x10,   0x25,   0x01,   0x2D,   0x2D,   0x24,
		0x2D,   0x10,   0x12,   0x12,   0x73,   0x00,   0x00 },
	{ 13, 0,0,0,0,  0xF6,   0x21,   0xAE,   0xBF,   0x62,   0x22,   0x62,   0x00,
		0x00 },
	{ 24, 0,0,0,0,  0xF7,   0x00,   0x01,   0x00,   0xF2,   0x0A,   0x0A,   0x0A,
		0x30,   0x0A,   0x00,   0x0F,   0x00,   0x0F,   0x00,   0x4B,	0x00,
		0x8C,   0x00,   0x00 },
	{ 24, 0,0,0,0,  0xF8,   0x00,   0x01,   0x00,   0xF2,   0x0A,   0x0A,   0x0A,
		0x30,   0x0A,   0x00,   0x0F,   0x00,   0x0F,   0x00,   0x4B,   0x00,
		0x8C,   0x00,   0x00 },
	{ 29, 0,0,0,0,  0xF9,   0x11,   0x10,   0x0F,   0x00,   0x01,   0x02,   0x04,
		0x05,   0x08,   0x00,   0x0A,   0x00,   0x00,   0x00,   0x0F,   0x10,
		0x11,   0x00,   0x00,   0xC3,   0xFF,   0x7F,   0x00,   0x00 },
	{ 0x02 },

	{ 0x04, 0x05,   0x29,   0x00,   0x00 },
	{ 0xFF },
};

static void dsi_set_sharp(struct pxa168fb_info *fbi)
{
	int i;
	for (i = 0; i < 21; i++) {
		if (dsi_cmd_sharp[i][0] == 0x01) {
			msleep(5);
			continue;
		} else if (dsi_cmd_sharp[i][0] == 0x02) {
			msleep(120);
			continue;
		} else if (dsi_cmd_sharp[i][0] == 0xFF) {
			break;
		} else {
			pxa168fb_dsi_send(fbi, dsi_cmd_sharp[i]);
		}
	}
}
#endif

static u16 tpo_spi_cmdon[] = {
	0x080F,
	0x0C5F,
	0x1017,
	0x1420,
	0x1808,
	0x1c20,
	0x2020,
	0x2420,
	0x2820,
	0x2c20,
	0x3020,
	0x3420,
	0x3810,
	0x3c10,
	0x4010,
	0x4415,
	0x48aa,
	0x4cff,
	0x5086,
	0x548d,
	0x58d4,
	0x5cfb,
	0x602e,
	0x645a,
	0x6889,
	0x6cfe,
	0x705a,
	0x749b,
	0x78c5,
	0x7cff,
	0x80f0,
	0x84f0,
	0x8808,
};

static u16 tpo_spi_cmdoff[] = {
	0x0c5e,		//standby
};

static int tpo_lcd_power(struct pxa168fb_info *fbi, unsigned int spi_gpio_cs, unsigned int spi_gpio_reset, int on)
{
	int err = 0;
	if (on) {
		if (spi_gpio_reset != -1) {
			err = gpio_request(spi_gpio_reset, "TPO_LCD_SPI_RESET");
			if (err) {
				printk("failed to request GPIO for TPO LCD RESET\n");
				return -1;
			}
			gpio_direction_output(spi_gpio_reset, 0);
			msleep(100);
			gpio_set_value(spi_gpio_reset, 1);
			msleep(100);
			gpio_free(spi_gpio_reset);
		}
		pxa168fb_spi_send(fbi, tpo_spi_cmdon, ARRAY_SIZE(tpo_spi_cmdon), spi_gpio_cs);
	} else
		pxa168fb_spi_send(fbi, tpo_spi_cmdoff, ARRAY_SIZE(tpo_spi_cmdoff), spi_gpio_cs);

	return 0;
}

/* SPI Control Register. */
#define     CFG_SCLKCNT(div)                    (div<<24)  /* 0xFF~0x2 */
#define     CFG_RXBITS(rx)                      ((rx - 1)<<16)   /* 0x1F~0x1 */
#define     CFG_TXBITS(tx)                      ((tx - 1)<<8)    /* 0x1F~0x1, 0x1: 2bits ... 0x1F: 32bits */
#define     CFG_SPI_ENA(spi)                    (spi<<3)
#define     CFG_SPI_SEL(spi)                    (spi<<2)   /* 1: port1; 0: port0 */
#define     CFG_SPI_3W4WB(wire)                 (wire<<1)  /* 1: 3-wire; 0: 4-wire */

static struct fb_videomode video_modes[] = {
	[0] = {
		.refresh	= 60,
		.xres		= 800,
		.yres		= 480,
		.hsync_len	= 4,
		.left_margin	= 212,
		.right_margin	= 40,
		.vsync_len	= 4,
		.upper_margin	= 31,
		.lower_margin	= 10,
		.sync		= FB_SYNC_VERT_HIGH_ACT | FB_SYNC_HOR_HIGH_ACT,
	},
};

static struct dsi_info jasper_dsi = {
	.id			= 1,
	.lanes			= 2,
	.bpp			= 16,
	.rgb_mode		= DSI_LCD_INPUT_DATA_RGB_MODE_565,
	.burst_mode		= DSI_BURST_MODE_SYNC_EVENT,
	.hbp_en			= 1,
	.hfp_en			= 1,
};

static int jasper_dsi_init(struct pxa168fb_info *fbi)
{
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;

	/* reset DSI controller */
	dsi_reset(fbi, 0);

	/* set dsi to dpi conversion chip */
	if (mi->phy_type == DSI2DPI)
		mi->dsi2dpi_set(fbi);

	/* set dphy */
	dsi_set_dphy(fbi);

	/* set dsi controller */
	dsi_set_controller(fbi);

	return 0;
}

static struct pxa168fb_mach_info mmp2_mipi_lcd_info __initdata = {
	.id			= "GFX Layer",
	.sclk_src		= 260000000,
	.sclk_div		= 0x40001108,
	.num_modes		= ARRAY_SIZE(video_modes),
	.modes			= video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the panel is hard connected with lcd panel path and dsi1 output
	 */
	.io_pad_ctrl = CFG_CYC_BURST_LEN16,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.active			= 1,
	.enable_lcd             = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.spi_ctrl               = CFG_SCLKCNT(2) | CFG_TXBITS(16) | CFG_SPI_SEL(0) | CFG_SPI_3W4WB(1) | CFG_SPI_ENA(1),
	.max_fb_size		= 800 * 480 * 8 + 4096,
	.phy_type		= DSI2DPI,
	.phy_init		= jasper_dsi_init,
#ifdef CONFIG_TC35876X
	.dsi2dpi_set		= dsi_set_tc358762,
#else
#error Please select CONFIG_TC35876X in menuconfig to enable DSI bridge
#endif
	.pxa168fb_lcd_power     = tpo_lcd_power,
	.dsi			= &jasper_dsi,
#ifdef CONFIG_PXA688_CMU
	.ioctl			= pxa688_cmu_ioctl,
	.cmu_cal = {{101, 149, 0, 0},
		{0, 48, 21, 20},
		{0, 0, 0, 0}
	}
#endif
};

static struct pxa168fb_mach_info mmp2_mipi_lcd_ovly_info __initdata = {
	.id			= "Video Layer",
	.num_modes		= ARRAY_SIZE(video_modes),
	.modes			= video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.active			= 1,
	.enable_lcd             = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.vdma_enable		= 1,
	.max_fb_size            = 800 * 480 * 8 + 4096,
};

#ifdef CONFIG_FB_SECOND_PANEL_DSI
static struct fb_videomode video_modes_2[] = {
	[0] = {
		.pixclock	= 49622,
		.refresh	= 60,
		.xres		= 480,
		.yres		= 640,
		.hsync_len	= 8,
		.left_margin	= 8,
		.right_margin	= 16,
		.vsync_len	= 2,
		.upper_margin	= 6,
		.lower_margin	= 8,
		.sync		= FB_SYNC_VERT_HIGH_ACT | FB_SYNC_HOR_HIGH_ACT,
	},
};

static struct pxa168fb_mach_info mmp2_mipi_lcd2_info __initdata = {
	.id			= "GFX Layer - 2",
	.sclk_div		= 0x40001108,
	.num_modes		= ARRAY_SIZE(video_modes_2),
	.modes			= video_modes_2,
	.pix_fmt		= PIX_FMT_RGB565,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the panel is hard connected with lcd tv path and dsi2 output
	 */
	.io_pad_ctrl = CFG_CYC_BURST_LEN16,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.active			= 1,
	.enable_lcd             = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.max_fb_size		= 640 * 480 * 8 + 4096,
	.display_interface	= DSI,
	.dsi_set		= dsi_set_sharp,
#ifdef CONFIG_PXA688_CMU
	.ioctl			= pxa688_cmu_ioctl,
#endif
};

static struct pxa168fb_mach_info mmp2_mipi_lcd2_ovly_info __initdata = {
	.id			= "Video Layer - 2",
	.num_modes		= ARRAY_SIZE(video_modes_2),
	.modes			= video_modes_2,
	.pix_fmt		= PIX_FMT_RGB565,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 0,
	.active			= 1,
	.enable_lcd             = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.max_fb_size            = 640 * 480 * 8 + 4096,
};
#endif

#ifdef CONFIG_FB_SECOND_PANEL_HDMI

static struct pxa168fb_mach_info mmp2_tv_hdmi_info __initdata = {
	.id			= "GFX Layer - TV",
	.sclk_div		= 0x5 | (1<<16) | (3<<30), /* HDMI PLL */
	.num_modes		= ARRAY_SIZE(tv_video_modes),
	.modes			= tv_video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	/*
	 * don't care about io_pin_allocation_mode and dumb_mode
	 * since the hdmi monitor is hard connected with lcd tv path and hdmi output
	 */
	.io_pad_ctrl = CFG_CYC_BURST_LEN16,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 1,
	.active			= 1,
	.enable_lcd             = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.max_fb_size		= 1920 * 1080 * 8 + 4096,
#ifdef CONFIG_PXA688_CMU
	.ioctl			= pxa688_cmu_ioctl,
#endif
};

static struct pxa168fb_mach_info mmp2_tv_hdmi_ovly_info __initdata = {
	.id			= "Video Layer - TV",
	.num_modes		= ARRAY_SIZE(tv_video_modes),
	.modes			= tv_video_modes,
	.pix_fmt		= PIX_FMT_RGB565,
	.panel_rgb_reverse_lanes= 0,
	.invert_composite_blank = 0,
	.invert_pix_val_ena     = 0,
	.invert_pixclock        = 0,
	.panel_rbswap           = 1,
	.active			= 1,
	.enable_lcd             = 1,
	.spi_gpio_cs            = -1,
	.spi_gpio_reset         = -1,
	.max_fb_size            = 1920 * 1080 * 8 + 4096,
	.vdma_enable		= 1,
};
#endif

static struct uio_hdmi_platform_data mmp2_hdmi_info __initdata = {
	.sspa_reg_base = 0xD42A0C00,
};

static struct i2c_pxa_platform_data pwri2c_info __initdata = {
	.use_pio		= 1,
};

#if defined(CONFIG_REGULATOR_WM8994) && defined(CONFIG_REGULATOR_WM8994)
static struct regulator_consumer_supply jasper_wm8994_regulator_supply[] = {
	[0] = {
		.supply	= "AVDD1",
	},
	[1] = {
		.supply = "DCVDD",
	},
};

struct regulator_init_data jasper_wm8994_regulator_init_data[] = {
	[0] = {
		.constraints	= {
			.name		= "wm8994-ldo1",
			.min_uV		= 2400000,
			.max_uV		= 3100000,
			.always_on	= 1,
			.boot_on	= 1,
		},
		.num_consumer_supplies	= 1,
		.consumer_supplies	= &jasper_wm8994_regulator_supply[0],
	},
	[1] = {
		.constraints	= {
			.name		= "wm8994-ldo2",
			.min_uV		= 900000,
			.max_uV		= 1200000,
			.always_on	= 1,
			.boot_on	= 1,
		},
		.num_consumer_supplies	= 1,
		.consumer_supplies	= &jasper_wm8994_regulator_supply[1],
	},
};

struct wm8994_pdata jasper_wm8994_pdata = {
	.ldo[0] = {
		.enable		= 0,
		.init_data	= &jasper_wm8994_regulator_init_data[0],
		.supply		= "AVDD1",

	},
	.ldo[1] = {
		.enable		= 0,
		.init_data	= &jasper_wm8994_regulator_init_data[1],
		.supply		= "DCVDD",

	},
};

static struct regulator_consumer_supply max8925_regulator_supply[] = {
	[0] = {
		.supply	= "DBVDD",
	},
	[1] = {
		.supply	= "AVDD2",
	},
	[2] = {
		.supply = "CPVDD",
	},
};

static struct regulator_consumer_supply jasper_fixed_regulator_supply[] = {
	[0] = {
		.supply = "SPKVDD1",
	},
	[1] = {
		.supply = "SPKVDD2",
	},
};

static struct i2c_board_info jasper_twsi2_info[] = {
	{
		.type		= "wm8994",
		.addr		= 0x1a,
		.platform_data	= &jasper_wm8994_pdata,
	},
};

struct regulator_init_data jasper_fixed_regulator_init_data[] = {
	[0] = {
		.constraints	= {
			.name		= "wm8994-SPK1",
			.always_on	= 1,
			.boot_on	= 1,
		},
		.num_consumer_supplies	= 1,
		.consumer_supplies	= &jasper_fixed_regulator_supply[0],
	},
	[1] = {
		.constraints	= {
			.name		= "wm8994-SPK2",
			.always_on	= 1,
			.boot_on	= 1,
		},
		.num_consumer_supplies	= 1,
		.consumer_supplies	= &jasper_fixed_regulator_supply[1],
	},
};

struct fixed_voltage_config jasper_fixed_pdata[2] = {
	[0] = {
		.supply_name	= "SPKVDD1",
		.microvolts	= 3700000,
		.init_data	= &jasper_fixed_regulator_init_data[0],
		.gpio		= -1,
	},
	[1] = {
		.supply_name	= "SPKVDD2",
		.microvolts	= 3700000,
		.init_data	= &jasper_fixed_regulator_init_data[1],
		.gpio		= -1,
	},
};

static struct platform_device fixed_device[] = {
	[0] = {
		.name		= "reg-fixed-voltage",
		.id		= 0,
		.dev		= {
			.platform_data	= &jasper_fixed_pdata[0],
		},
		.num_resources	= 0,
	},
	[1] = {
		.name		= "reg-fixed-voltage",
		.id		= 1,
		.dev		= {
			.platform_data	= &jasper_fixed_pdata[1],
		},
		.num_resources	= 0,
	},
};

static struct platform_device *fixed_rdev[] __initdata = {
	&fixed_device[0],
	&fixed_device[1],
};

static void __init jasper_fixed_regulator(void)
{
	platform_add_devices(fixed_rdev, ARRAY_SIZE(fixed_rdev));
}
#endif

static struct regulator_consumer_supply max8649_supply[] = {
	REGULATOR_SUPPLY("v_core", NULL),
};

static struct regulator_init_data max8649_init_data = {
	.constraints	= {
		.name		= "SD1",
		.min_uV		= 750000,
		.max_uV		= 1380000,
		.always_on	= 1,
		.boot_on	= 1,
		.valid_ops_mask	= REGULATOR_CHANGE_VOLTAGE,
	},
	.num_consumer_supplies	= 1,
	.consumer_supplies	= &max8649_supply[0],
};

static struct max8649_platform_data jasper_max8649_info = {
	.mode		= 2,	/* VID1 = 1, VID0 = 0 */
	.extclk		= 0,
	.ramp_timing	= MAX8649_RAMP_32MV,
	.regulator	= &max8649_init_data,
};

static struct regulator_consumer_supply regulator_supply[] = {
	[MAX8925_ID_SD1]	= REGULATOR_SUPPLY("v_sd1", NULL),
	[MAX8925_ID_SD2]	= REGULATOR_SUPPLY("v_sd2", NULL),
	[MAX8925_ID_SD3]	= REGULATOR_SUPPLY("v_sd3", NULL),
	[MAX8925_ID_LDO1]	= REGULATOR_SUPPLY("v_ldo1", NULL),
	[MAX8925_ID_LDO2]	= REGULATOR_SUPPLY("v_ldo2", NULL),
	[MAX8925_ID_LDO3]	= REGULATOR_SUPPLY("v_ldo3", NULL),
	[MAX8925_ID_LDO4]	= REGULATOR_SUPPLY("v_ldo4", NULL),
	[MAX8925_ID_LDO5]	= REGULATOR_SUPPLY("v_ldo5", NULL),
	[MAX8925_ID_LDO6]	= REGULATOR_SUPPLY("v_ldo6", NULL),
	[MAX8925_ID_LDO7]	= REGULATOR_SUPPLY("v_ldo7", NULL),
	[MAX8925_ID_LDO8]	= REGULATOR_SUPPLY("v_ldo8", NULL),
	[MAX8925_ID_LDO9]	= REGULATOR_SUPPLY("v_ldo9", NULL),
	[MAX8925_ID_LDO10]	= REGULATOR_SUPPLY("v_ldo10", NULL),
	[MAX8925_ID_LDO11]	= REGULATOR_SUPPLY("v_ldo11", NULL),
	[MAX8925_ID_LDO12]	= REGULATOR_SUPPLY("v_ldo12", NULL),
	[MAX8925_ID_LDO13]	= REGULATOR_SUPPLY("v_ldo13", NULL),
	[MAX8925_ID_LDO14]	= REGULATOR_SUPPLY("v_ldo14", NULL),
	[MAX8925_ID_LDO15]	= REGULATOR_SUPPLY("v_ldo15", NULL),
	[MAX8925_ID_LDO16]	= REGULATOR_SUPPLY("v_ldo16", NULL),
	[MAX8925_ID_LDO17]	= REGULATOR_SUPPLY("v_ldo17", NULL),
	[MAX8925_ID_LDO18]	= REGULATOR_SUPPLY("v_ldo18", NULL),
	[MAX8925_ID_LDO19]	= REGULATOR_SUPPLY("v_ldo19", NULL),
	[MAX8925_ID_LDO20]	= REGULATOR_SUPPLY("v_ldo20", NULL),
};

#define REG_INIT(_name, _min, _max, _always, _boot)		\
{								\
	.constraints = {					\
		.name		= __stringify(_name),		\
		.min_uV		= _min,				\
		.max_uV		= _max,				\
		.always_on	= _always,			\
		.boot_on	= _boot,			\
		.valid_ops_mask	= REGULATOR_CHANGE_VOLTAGE	\
				| REGULATOR_CHANGE_STATUS,	\
	},							\
	.num_consumer_supplies	= 1,				\
	.consumer_supplies	= &regulator_supply[MAX8925_ID_##_name], \
}

static struct regulator_init_data regulator_data[] = {
	[MAX8925_ID_SD1] = REG_INIT(SD1, 637500, 1425000, 0, 0),
	[MAX8925_ID_SD2] = {
		.constraints	= {
			.name		= "SD2",
			.min_uV		= 650000,
			.max_uV		= 2225000,
			.always_on	= 1,
			.boot_on	= 1,
		},
		.num_consumer_supplies	= ARRAY_SIZE(max8925_regulator_supply),
		.consumer_supplies	= max8925_regulator_supply,
	},
	[MAX8925_ID_SD3] = REG_INIT(SD3, 750000, 3900000, 1, 1),
	[MAX8925_ID_LDO1] = REG_INIT(LDO1, 750000, 3900000, 1, 1),
	[MAX8925_ID_LDO2] = REG_INIT(LDO2, 650000, 2250000, 1, 1),
	[MAX8925_ID_LDO3] = REG_INIT(LDO3, 650000, 2250000, 1, 1),
	[MAX8925_ID_LDO4] = REG_INIT(LDO4, 750000, 3900000, 1, 1),
	[MAX8925_ID_LDO5] = REG_INIT(LDO5, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO6] = REG_INIT(LDO6, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO7] = REG_INIT(LDO7, 750000, 3900000, 1, 1),
	[MAX8925_ID_LDO8] = REG_INIT(LDO8, 750000, 3900000, 1, 1),
	[MAX8925_ID_LDO9] = REG_INIT(LDO9, 750000, 3900000, 1, 1),
	[MAX8925_ID_LDO10] = REG_INIT(LDO10, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO11] = REG_INIT(LDO11, 750000, 3900000, 1, 1),
	[MAX8925_ID_LDO12] = REG_INIT(LDO12, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO13] = REG_INIT(LDO13, 750000, 1500000, 0, 0),
	[MAX8925_ID_LDO14] = REG_INIT(LDO14, 750000, 3000000, 0, 0),
	[MAX8925_ID_LDO15] = REG_INIT(LDO15, 750000, 2800000, 0, 0),
	[MAX8925_ID_LDO16] = REG_INIT(LDO16, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO17] = REG_INIT(LDO17, 650000, 2250000, 1, 1),
	[MAX8925_ID_LDO18] = REG_INIT(LDO18, 650000, 2250000, 1, 1),
	[MAX8925_ID_LDO19] = REG_INIT(LDO19, 750000, 3900000, 0, 0),
	[MAX8925_ID_LDO20] = REG_INIT(LDO20, 750000, 3900000, 1, 1),
};

static struct max8925_backlight_pdata jasper_backlight_data = {
	.dual_string	= 0,
};

static int jasper_set_led(int enable)
{
	int led;

	led = mfp_to_gpio(MFP_PIN_GPIO74);
	if (gpio_request(led, "LED Orange")) {
		pr_err("Failed to request LED orange\n");
		return -EIO;
	}
	if (enable)
		gpio_direction_output(led, 0);
	else
		gpio_direction_output(led, 1);
	gpio_free(led);
	return 0;
}

static struct max8925_power_pdata jasper_power_data = {
	.set_led		= jasper_set_led,
	.batt_detect		= 0,	/* can't detect battery by ID pin */
	.topoff_threshold	= MAX8925_TOPOFF_THR_10PER,
	.fast_charge		= MAX8925_FCHG_1000MA,
};

static int max8925_set_vbus(int enable, int srp)
{
	int vbus_en = mfp_to_gpio(MFP_PIN_GPIO82);
	int ret = gpio_request(vbus_en, "vbus_en");

	if (ret) {
		pr_debug("failed to get gpio #%d\n", vbus_en);
		return -EINVAL;
	}

	gpio_direction_output(vbus_en, enable);
	mdelay(10);
	gpio_free(vbus_en);

	return 0;
}

/* max8925 vbus pdata */
static struct max8925_vbus_pdata jasper_vbus_data = {
	.irq_rise	= MAX8925_IRQ_VCHG_DC_R,
	.irq_fall	= MAX8925_IRQ_VCHG_DC_F,
	.reg_base	= PXA168_U2O_REGBASE,
	.reg_end	= PXA168_U2O_REGBASE + USB_REG_RANGE,
	.set_vbus	= max8925_set_vbus,
};

static struct max8925_platform_data jasper_max8925_info = {
	.backlight		= &jasper_backlight_data,
	.power			= &jasper_power_data,
	.vbus			= &jasper_vbus_data,
	.irq_base		= IRQ_BOARD_START,

	.regulator[MAX8925_ID_SD1] = &regulator_data[MAX8925_ID_SD1],
	.regulator[MAX8925_ID_SD2] = &regulator_data[MAX8925_ID_SD2],
	.regulator[MAX8925_ID_SD3] = &regulator_data[MAX8925_ID_SD3],
	.regulator[MAX8925_ID_LDO1] = &regulator_data[MAX8925_ID_LDO1],
	.regulator[MAX8925_ID_LDO2] = &regulator_data[MAX8925_ID_LDO2],
	.regulator[MAX8925_ID_LDO3] = &regulator_data[MAX8925_ID_LDO3],
	.regulator[MAX8925_ID_LDO4] = &regulator_data[MAX8925_ID_LDO4],
	.regulator[MAX8925_ID_LDO5] = &regulator_data[MAX8925_ID_LDO5],
	.regulator[MAX8925_ID_LDO6] = &regulator_data[MAX8925_ID_LDO6],
	.regulator[MAX8925_ID_LDO7] = &regulator_data[MAX8925_ID_LDO7],
	.regulator[MAX8925_ID_LDO8] = &regulator_data[MAX8925_ID_LDO8],
	.regulator[MAX8925_ID_LDO9] = &regulator_data[MAX8925_ID_LDO9],
	.regulator[MAX8925_ID_LDO10] = &regulator_data[MAX8925_ID_LDO10],
	.regulator[MAX8925_ID_LDO11] = &regulator_data[MAX8925_ID_LDO11],
	.regulator[MAX8925_ID_LDO12] = &regulator_data[MAX8925_ID_LDO12],
	.regulator[MAX8925_ID_LDO13] = &regulator_data[MAX8925_ID_LDO13],
	.regulator[MAX8925_ID_LDO14] = &regulator_data[MAX8925_ID_LDO14],
	.regulator[MAX8925_ID_LDO15] = &regulator_data[MAX8925_ID_LDO15],
	.regulator[MAX8925_ID_LDO16] = &regulator_data[MAX8925_ID_LDO16],
	.regulator[MAX8925_ID_LDO17] = &regulator_data[MAX8925_ID_LDO17],
	.regulator[MAX8925_ID_LDO18] = &regulator_data[MAX8925_ID_LDO18],
	.regulator[MAX8925_ID_LDO19] = &regulator_data[MAX8925_ID_LDO19],
	.regulator[MAX8925_ID_LDO20] = &regulator_data[MAX8925_ID_LDO20],
};
static struct i2c_board_info jasper_twsi4_info[] =
{
#if defined(CONFIG_SENSORS_BMA150)
	{
		.type		= "bma150",
		.addr		= 0x38,
	},
#endif
#if defined(CONFIG_SENSORS_CM3623)
	{
		.type		= "cm3623_ps",
		.addr		= 0x58,
	},
	{
		.type		= "cm3623_als_msb",
		.addr		= 0x10,
	},
	{
		.type		= "cm3623_als_lsb",
		.addr		= 0x11,
	},
#endif
#if defined(CONFIG_SENSORS_HMC5843)
	{
		.type           = "hmc5843",
		.addr           = 0x1E,
	},
#endif

};

static struct i2c_board_info jasper_twsi1_info[] = {
	[0] = {
		.type		= "max8649",
		.addr		= 0x60,
		.platform_data	= &jasper_max8649_info,
	},
	[1] = {
		.type		= "max8925",
		.addr		= 0x3c,
		.irq		= IRQ_MMP2_PMIC,
		.platform_data	= &jasper_max8925_info,
	},
};

#ifdef CONFIG_MTD_NAND_PXA3xx
static struct mtd_partition jasper_nand_partitions[] = {
	[0] = {
		.name		= "Bootloader",
		.offset		= 0,
		.size		= 0x100000,
		.mask_flags	= MTD_WRITEABLE,
	},
	[1] = {
		.name		= "Reserve",
		.offset		= 0x100000,
		.size		= 0x080000,
	},
	[2] = {
		.name		= "Reserve",
		.offset		= 0x180000,
		.size		= 0x800000,
		.mask_flags	= MTD_WRITEABLE,
	},
	[3] = {
		.name		= "Kernel",
		.offset		= 0x980000,
		.size		= 0x300000,
		.mask_flags	= MTD_WRITEABLE,
	},
	[4] = {
		.name		= "system",
		.offset		= 0x0c80000,
		.size		= 0x7000000,
	},
	[5] = {
		.name		= "userdata",
		.offset		= 0x7c80000,
		.size		= 0x7000000,
	},
	[6] = {
		.name		= "filesystem",
		.offset		= 0x0ec80000,
		.size		= MTDPART_SIZ_FULL,
	}
};

static struct pxa3xx_nand_platform_data jasper_nand_info;
static void __init jasper_init_flash(void)
{
	jasper_nand_info.parts[0] = jasper_nand_partitions;
	jasper_nand_info.nr_parts[0] = ARRAY_SIZE(jasper_nand_partitions);
	jasper_nand_info.controller_attrs = PXA3XX_ARBI_EN | PXA3XX_NAKED_CMD_EN
		| PXA3XX_DMA_EN | PXA3XX_ADV_TIME_TUNING;
	mmp2_add_nand(&jasper_nand_info);
}
#endif

static struct i2c_board_info jasper_twsi5_info[] =
{
#if defined(CONFIG_TOUCHSCREEN_SYNAPTICS_I2C_RMI)
{
	.type		= SYNAPTICS_I2C_RMI_NAME,
	.addr		= 0x20,
	.irq		= IRQ_GPIO(101),
},
#endif
#if defined(CONFIG_TC35876X)
	{
		.type           = "tc35876x",
		.addr           = 0x0b,
		.platform_data  = &tc358762_data,
	},
#endif
};

#if defined(CONFIG_SWITCH_HEADSET_HOST_GPIO)
static struct gpio_switch_platform_data headset_switch_device_data = {
	.name = "h2w",
	.gpio = mfp_to_gpio(GPIO23_GPIO23),
	.name_on = NULL,
	.name_off = NULL,
	.state_on = NULL,
	.state_off = NULL,
};

static struct platform_device headset_switch_device = {
	.name            = "headset",
	.id              = 0,
	.dev             = {
		.platform_data = &headset_switch_device_data,
	},
};

static int wm8994_gpio_irq(void)
{
	int gpio = mfp_to_gpio(GPIO23_GPIO23);

	if (gpio_request(gpio, "wm8994 irq")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio);
		return -1;
	}

	gpio_direction_input(gpio);
	mdelay(1);
	gpio_free(gpio);
	return 0;
}

static void __init jasper_init_headset(void)
{
	wm8994_gpio_irq();
	platform_device_register(&headset_switch_device);
}
#endif

#if defined(CONFIG_SWITCH_HDMI)
static struct gpio_switch_platform_data hdmi_switch_device_data = {
	.name = "switch-hdmi",
	.gpio = mfp_to_gpio(GPIO46_HDMI_DET),
	.name_on = NULL,
	.name_off = NULL,
	.state_on = NULL,
	.state_off = NULL,
};

static struct platform_device hdmi_switch_device = {
	.name            = "switch-hdmi",
	.id              = 0,
	.dev             = {
		.platform_data = &hdmi_switch_device_data,
	},
};

static void __init jasper_init_hdmi_switch(void)
{
	platform_device_register(&hdmi_switch_device);
}
#endif

#if defined(CONFIG_HDMI_DDC_EDID)
static struct i2c_board_info jasper_twsi6_info[] =
{
 	{
 		.type			= "hdmi_edid",
 		.addr			= 0x50,
 	},
};
#endif

static unsigned int mmp2_matrix_key_map[] = {
	/* KEY(row, col, key_code) */
	KEY(0, 1, KEY_SEARCH),
	KEY(0, 2, KEY_CAMERA),
	KEY(1, 0, KEY_BACK),
	KEY(1, 1, KEY_HOME),
	KEY(1, 2, KEY_CAMERA),
	KEY(2, 0, KEY_VOLUMEUP),
	KEY(2, 1, KEY_VOLUMEDOWN),
};

static struct pxa27x_keypad_platform_data mmp2_keypad_info = {
	.matrix_key_rows        = 3,
	.matrix_key_cols        = 3,
	.matrix_key_map         = mmp2_matrix_key_map,
	.matrix_key_map_size    = ARRAY_SIZE(mmp2_matrix_key_map),
	.debounce_interval      = 30,
};

#ifdef CONFIG_PXA688_CAMERA

static int cam_power_set(int flag)
{
	struct clk *gate_clk = NULL;
	struct clk *dbg_clk = NULL;

	gate_clk = clk_get(NULL, "CCICGATECLK");
	if (IS_ERR(gate_clk)) {
		printk(KERN_ERR "unable to get CCICGATECLK");
		return PTR_ERR(gate_clk);
	}

	dbg_clk = clk_get(NULL, "CCICDBGCLK");
	if (IS_ERR(dbg_clk)) {
		printk(KERN_ERR "unable to get CCICDBGCLK");
		return PTR_ERR(dbg_clk);
	}

	if (flag) {
		clk_enable(gate_clk);
		clk_enable(dbg_clk);
	} else {
		clk_disable(gate_clk);
		clk_disable(dbg_clk);
	}

	clk_put(gate_clk);
	clk_put(dbg_clk);

	return 0;
}

static int cam_pmua_set(int flag)
{
	struct clk *rst_clk = NULL;

	rst_clk = clk_get(NULL, "CCICRSTCLK");
	if (IS_ERR(rst_clk)) {
		printk(KERN_ERR "unable to get CCICRSTCLK");
		return PTR_ERR(rst_clk);
	}

	if (flag)
		clk_enable(rst_clk);
	else
		clk_disable(rst_clk);

	clk_put(rst_clk);

	return 0;
}

static int sensor_power_set(int flag, int res)
{
	/*
	 * RES, 0, LOW RESOLUTION, 1 HIGH RESOLUTION
	 * FLAG, 1: ON, 0: OFF
	 */
	int cam_reset = mfp_to_gpio(MFP_PIN_GPIO73);
	struct regulator *v_ldo = NULL;

	cam_power_set(flag);

	if (res) {
		if (gpio_request(cam_reset, "CAM_ENABLE_HI_SENSOR")) {
			printk(KERN_ERR "Request GPIO failed,"
					"gpio: %d \n", cam_reset);
			return -EIO;
		}
		if (flag)
			gpio_direction_output(cam_reset, 1);    /* enable */
		else
			gpio_direction_output(cam_reset, 0);    /* disable */

		gpio_free(cam_reset);
		msleep(100);
	}

	/* enable voltage for camera sensor OV5642 */
	/* v_ldo14 3.0v */
	v_ldo = regulator_get(NULL, "v_ldo14");
	if (IS_ERR(v_ldo))
		v_ldo = NULL;
	else {
		if (flag) {
			regulator_enable(v_ldo);
			regulator_set_voltage(v_ldo, 3000000, 3000000);
		} else
			regulator_disable(v_ldo);
	}
	/* v_ldo15 2.8v */
	v_ldo = regulator_get(NULL, "v_ldo15");
	if (IS_ERR(v_ldo))
		v_ldo = NULL;
	else {
		if (flag) {
			regulator_enable(v_ldo);
			regulator_set_voltage(v_ldo, 2800000, 2800000);
		} else
			regulator_disable(v_ldo);
	}

	return 0;
}

static struct sensor_platform_data ov5642_sensor_data = {
	.id = SENSOR_HIGH,
	.power_set = sensor_power_set,
};

/* sensor init over */
static struct cam_platform_data cam_ops = {
	.power_set      = cam_power_set,
	.pmua_set	= cam_pmua_set,
};

static int __init jasper_init_cam(void)
{
	struct clk *rst_clk= NULL;

	/* rst_clk can not be disabled, otherwise cam register are reset and mipi clk will stop */
	/* for example, REG_CLKCTRL & REG_CTRL1 will reset */
	/* Besides, on MMP2, cam register can not be accessed when rst_clk is disalbed*/

	rst_clk = clk_get(NULL, "CCICRSTCLK");
	if (IS_ERR(rst_clk)) {
		printk(KERN_ERR "unable to get CCICRSTCLK");
		return PTR_ERR(rst_clk);
	}
	clk_enable(rst_clk);
	clk_put(rst_clk);

	mmp2_add_cam(1, &cam_ops);
	return 0;
}

#endif

static struct i2c_board_info jasper_twsi3_info[] =
{
#ifdef CONFIG_PXA688_CAMERA
	{
		.type			= "ov5642",
		.addr			= 0x3C,
		.platform_data		= &ov5642_sensor_data,
	},
#endif
};

#if defined(CONFIG_MMC_SDHCI_PXA)

/* MMC1 controller for SD-MMC */
static unsigned int mmc0_get_ro(struct sdhci_host *host)
{
	/* work around of bonnell v2, wp is not fold */
	return 0;
}

static void mmc0_set_ops(struct sdhci_pxa *pxa)
{
	pxa->ops->get_ro = mmc0_get_ro;
}

static struct sdhci_pxa_platdata mmp2_sdh_platdata_mmc0 = {
	.soc_set_ops	= mmc0_set_ops,
	.clk_delay_cycles	= 0x1f,
	.flags		= PXA_FLAG_CONTROL_CLK_GATE,
	.soc_set_timing	= mmp2_init_sdh,
};

static struct sdhci_pxa_platdata mmp2_sdh_platdata_mmc1 = {
	.flags		= PXA_FLAG_DISABLE_CLOCK_GATING | PXA_FLAG_CARD_PERMANENT,
	.soc_set_timing= mmp2_init_sdh,
};

static struct sdhci_pxa_platdata mmp2_sdh_platdata_mmc2 = {
	.clk_delay_cycles	= 0x1f,
	.flags		= PXA_FLAG_CARD_PERMANENT | PXA_FLAG_CONTROL_CLK_GATE,
	.soc_set_timing= mmp2_init_sdh,
};

static void __init mmp2_init_mmc(void)
{
#ifdef CONFIG_SD8XXX_RFKILL
	int WIB_PDn;
	int WIB_RESETn;

	WIB_PDn = mfp_to_gpio(MFP_PIN_GPIO57);
	WIB_RESETn = mfp_to_gpio(MFP_PIN_GPIO58);

	add_sd8x_rfkill_device(WIB_PDn, WIB_RESETn,
			&mmp2_sdh_platdata_mmc1.pmmc, NULL);
#endif
#ifndef CONFIG_MTD_NAND_PXA3xx
	/*eMMC (MMC3) pins are conflict with NAND*/
	mmp2_add_sdh(2, &mmp2_sdh_platdata_mmc2); /*eMMC*/
#endif
	mmp2_add_sdh(0, &mmp2_sdh_platdata_mmc0); /*SD/MMC*/
	mmp2_add_sdh(1, &mmp2_sdh_platdata_mmc1); /*Wifi*/
}

#endif

/* usb vbus platform data */
static struct u2o_vbus_pdata vbus_pdata = {
	.vbus_en	= mfp_to_gpio(MFP_PIN_GPIO82),
	.vbus_irq	= IRQ_MMP2_USB_OTG,
	.reg_base	= PXA168_U2O_REGBASE,
	.reg_end	= PXA168_U2O_REGBASE + USB_REG_RANGE,
	.phy_base	= PXA168_U2O_PHYBASE,
	.phy_end	= PXA168_U2O_PHYBASE + USB_PHY_RANGE,
	.phy_init	= mmp2_init_usb_phy,
};

static struct platform_device vbus_device = {
	.name		= "u2o_vbus",
	.id		= -1,
	.dev		= {
		.platform_data	= &vbus_pdata,
	},
};

/* max8903 battery charger platform data */
static struct max8903_charger_pdata chg_pdata = {
	.gpio_2a_n    = mfp_to_gpio(GPIO80_GPIO80),
	.gpio_500ma   = mfp_to_gpio(GPIO79_GPIO79),
	/* default charging current limitation */
	.fast_chgcur   = MAX8903_FCHG_100MA,
};

static struct platform_device max8903_charger_device = {
	.name = "max8903-charger",
	.id = -1,
	.dev = {
		.platform_data = &chg_pdata,
	},
};

static struct vmeta_plat_data mmp2_vmeta_pdata = {
	.set_dvfm_constraint = mmp2_vmeta_set_dvfm_constraint,
	.unset_dvfm_constraint = mmp2_vmeta_unset_dvfm_constraint,
	.init_dvfm_constraint = mmp2_vmeta_init_dvfm_constraint,
	.clean_dvfm_constraint = mmp2_vmeta_clean_dvfm_constraint,
	.decrease_core_freq = mmp2_vmeta_decrease_core_freq,
	.increase_core_freq = mmp2_vmeta_increase_core_freq,
};

static void __init jasper_init(void)
{
	mfp_config(ARRAY_AND_SIZE(jasper_pin_config));

	mmp2_get_platform_version();

	/* on-chip devices */
	mmp2_add_uart(1);
	mmp2_add_uart(3);
	mmp2_add_rtc();
	mmp2_add_twsi(1, NULL, ARRAY_AND_SIZE(jasper_twsi1_info));
	mmp2_add_twsi(2, NULL, ARRAY_AND_SIZE(jasper_twsi2_info));
	mmp2_add_twsi(3, NULL, ARRAY_AND_SIZE(jasper_twsi3_info));
	mmp2_add_twsi(4, NULL, ARRAY_AND_SIZE(jasper_twsi4_info));
#if defined(CONFIG_TOUCHSCREEN_SYNAPTICS_I2C_RMI)
	if(board_is_mmp2_bonnell_rev2())
		jasper_twsi5_info[0].irq = 0;
#endif
	mmp2_add_twsi(5, &pwri2c_info, ARRAY_AND_SIZE(jasper_twsi5_info));
#if defined(CONFIG_HDMI_DDC_EDID)
	mmp2_add_twsi(6, &pwri2c_info, ARRAY_AND_SIZE(jasper_twsi6_info));
#endif
	jasper_fixed_regulator();
	regulator_has_full_constraints();
	wm8994_ldoen();
	mmp2_add_imm();
	mmp2_add_sspa(1);
	mmp2_add_sspa(2);
	mmp2_add_audiosram();
	mmp2_add_vmeta(&mmp2_vmeta_pdata);
	mmp2_add_thermal_sensor();
	mmp2_add_keypad(&mmp2_keypad_info);

#ifdef CONFIG_FB_PXA168
	mmp2_add_fb(&mmp2_mipi_lcd_info);
	mmp2_add_fb_ovly(&mmp2_mipi_lcd_ovly_info);
#ifdef CONFIG_FB_SECOND_PANEL_DSI
	mmp2_add_fb_tv(&mmp2_mipi_lcd2_info);
	mmp2_add_fb_tv_ovly(&mmp2_mipi_lcd2_ovly_info);
#endif
#ifdef CONFIG_FB_SECOND_PANEL_HDMI
	mmp2_add_fb_tv(&mmp2_tv_hdmi_info);
	mmp2_add_fb_tv_ovly(&mmp2_tv_hdmi_ovly_info);
#endif
#endif

	mmp2_add_hdmi(&mmp2_hdmi_info);
#ifdef CONFIG_PXA688_CAMERA
        jasper_init_cam();
#endif
#if defined(CONFIG_SWITCH_HDMI)
	/*hdmi switch device*/
	jasper_init_hdmi_switch();
#endif

#ifdef CONFIG_SWITCH_HEADSET_HOST_GPIO
	jasper_init_headset();
#endif

#ifdef CONFIG_ANDROID_PMEM
	pxa_add_pmem("pmem", 0x02000000UL, 1, 1, 1);
	pxa_add_pmem("pmem_adsp", 0x02000000UL, 0, 0, 0);
#endif

#if defined(CONFIG_MMC_SDHCI_PXA)
	mmp2_init_mmc();
#endif
	platform_device_register(&vbus_device);
	platform_device_register(&u2o_device);
	platform_device_register(&otg_device);
	platform_device_register(&ehci_u2o_device);
#ifdef CONFIG_USB_ANDROID
	android_add_usb_devices();
#endif
	/* only bonnell v1 using internal charger in max8925.
	 * bonnell v2... using max8903 charger */
	if (!board_is_mmp2_bonnell_rev1()) {
		jasper_power_data.chg_max8903_en = 1;
		platform_device_register(&max8903_charger_device);
	}

	if (cpu_is_mmp2() && !cpu_is_mmp2_z0() && !cpu_is_mmp2_z1()) {
		pxa_register_device(&mmp2_device_zsp, NULL, 0);
	}

}

MACHINE_START(MARVELL_JASPER, "Jasper Development Platform")
	.phys_io        = APB_PHYS_BASE,
	.boot_params    = 0x00000100,
	.io_pg_offst    = (APB_VIRT_BASE >> 18) & 0xfffc,
	.map_io		= pxa_map_io,
	.nr_irqs	= JASPER_NR_IRQS,
	.init_irq       = mmp2_init_irq,
	.timer          = &mmp2_timer,
	.init_machine   = jasper_init,
MACHINE_END
