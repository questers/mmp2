/*
 * PXA688 Power Management Routines
 *
 * This software program is licensed subject to the GNU General Public License
 * (GPL).Version 2,June 1991, available at http://www.fsf.org/copyleft/gpl.html
 *
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 */

#undef DEBUG
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/err.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <linux/vmalloc.h>
#include <linux/kobject.h>
#include <linux/suspend.h>
#include <linux/clk.h>
#include <linux/interrupt.h>
#include <linux/slab.h>
#include <asm/cacheflush.h>
#include <asm/io.h>
#include <asm/mach-types.h>
#include <mach/hardware.h>
#include <mach/cputype.h>
#include <mach/mfp.h>
#include <mach/gpio.h>
#include <mach/addr-map.h>
#include <mach/dma.h>
#include <mach/gpio.h>
#include <mach/regs-apbc.h>
#include <mach/regs-apmu.h>
#include <mach/regs-icu.h>
#include <mach/regs-mpmu.h>
#include <mach/regs-smc.h>
#include <mach/regs-timers.h>
#include <mach/pxa168_pm.h>
#include <mach/mmp2_pm.h>
#include <mach/mmp2_dvfm.h>
#include <mach/irqs.h>
#include <linux/irq.h>
#include <linux/io.h>
#include <plat/imm.h>
#include <linux/wakelock.h>

#define JTAG_VIRT_BASE	(APB_VIRT_BASE + 0x013100)
#define JTAG_REG(x)	(JTAG_VIRT_BASE + (x))
#define JTAGSW_EN	JTAG_REG(0x0000)
#define JTAGSW_CTRL		JTAG_REG(0x0004)
#define TDI         0x3
#define TRST        0x2
#define TMS         0x6
#define TCK         0xa
#define TDITCK      0xb
#define TMSTCK      0xe
#define TDITMSTCK   0xf

//int enable_deepidle = IDLE_CORE_INTIDLE | IDLE_CORE_EXTIDLE |
//	IDLE_APPS_IDLE | IDLE_APPS_SLEEP | IDLE_SYS_SLEEP;
int enable_deepidle = IDLE_CORE_EXTIDLE | IDLE_APPS_SLEEP | IDLE_CHIP_SLEEP;
static unsigned long pm_state;
static int cur_lpmode = -1;

#ifdef CONFIG_WAKELOCK
static int pmic_wakeup_detect = 0;
static int sdh_wakeup_detect = 0;
struct wake_lock wakelock_pmic_wakeup;
struct wake_lock wakelock_sdh_wakeup;
#endif

#ifdef NOT_USED
static unsigned int pm_irqs[] = {
	IRQ_MMP2_KEYPAD_MUX,
	IRQ_MMP2_PMIC_MUX,
	IRQ_MMP2_RTC_MUX,
};
#endif

extern void mmp2_cpu_disable_l2(void* param);
extern void mmp2_cpu_enable_l2(void* param);

#ifdef CONFIG_DVFM
#include <mach/dvfm.h>

static int dvfm_dev_idx;

static void set_dvfm_constraint(void)
{
	// Disable un-safe op for vmeta
	dvfm_disable(dvfm_dev_idx);
}

static void unset_dvfm_constraint(void)
{
	dvfm_enable(dvfm_dev_idx);
}

#else
static void set_dvfm_constraint(void) {}
static void unset_dvfm_constraint(void) {}
#endif

static int apm_suspend_notifier(struct notifier_block *nb,
				unsigned long event,
				void *dummy)
{
	switch (event) {
	case PM_SUSPEND_PREPARE:
		set_dvfm_constraint();
		return NOTIFY_OK;
	case PM_POST_SUSPEND:
		unset_dvfm_constraint();
		return NOTIFY_OK;
	default:
		return NOTIFY_DONE;
	}
}
static struct notifier_block mmp2pm_notif_block = {
	.notifier_call = apm_suspend_notifier,
};

/* Base address of SRAM
 * Now, 4K SRAM is available.
 * Allocation:
 * 	0x0 - 0x7ff: 	text for DVFM
 * 	0x800 - 0xbff:	stack for DVFM
 *	0xc00 - 0xdff:	text for Low Power
 *	0xe00 - 0xfff:	stack for Low Power
 */
void *vaddr_base = NULL;
static void *va_dmcu;

static void *map_sram(void)
{
	unsigned int immid = -1;
	unsigned int sram_freebytes = 0;
	u32 paddr = 0;

	immid = imm_register_kernel("mmp2 pm");

	sram_freebytes = imm_get_freespace(0, immid);

	if (sram_freebytes < 4 * 1024) {
		printk(KERN_ERR "insufficient sram\n");
		return NULL;
	}

	vaddr_base = imm_malloc(4 * 1024, /* IMM_MALLOC_HARDWARE |*/ IMM_MALLOC_SRAM, immid);
	if(vaddr_base != NULL) {
		paddr = imm_get_physical(vaddr_base, immid);
		printk(KERN_DEBUG "alloc 4K mem from sram 0x%X\n", paddr);
	} else {
		printk(KERN_ERR "%s: sram no memory!\n", __func__);
		return NULL;
	}

	return 0;
}
static uint32_t setup_wakeup_sources(int state)
{
	uint32_t apcr, awucrm;

	awucrm = 0x0;
	//awucrm |= PMUM_GSM_WAKEUPWMX;
	//awucrm |= PMUM_WCDMA_WAKEUPX;
	//awucrm |= PMUM_GSM_WAKEUPWM;
	//awucrm |= PMUM_WCDMA_WAKEUPWM;
	//awucrm |= PMUM_AP_ASYNC_INT;
	//awucrm |= PMUM_AP_FULL_IDLE;
	//awucrm |= PMUM_SDH1;
	//awucrm |= PMUM_SDH2;
	//awucrm |= PMUM_KEYPRESS;
	//awucrm |= PMUM_TRACKBALL;
	//awucrm |= PMUM_NEWROTARY;
	//awucrm |= PMUM_WDT;
	awucrm |= PMUM_RTC_ALARM;
	//awucrm |= PMUM_CP_TIMER_3;
	//awucrm |= PMUM_CP_TIMER_2;
	//awucrm |= PMUM_CP_TIMER_1;
	//awucrm |= PMUM_AP2_TIMER_3;
	//awucrm |= PMUM_AP2_TIMER_2;
	//awucrm |= PMUM_AP2_TIMER_1;
	//awucrm |= PMUM_AP1_TIMER_3;
	//awucrm |= PMUM_AP1_TIMER_2;
	//awucrm |= PMUM_AP1_TIMER_1;
	awucrm |= PMUM_WAKEUP7;
	//awucrm |= PMUM_WAKEUP6;
	//awucrm |= PMUM_WAKEUP5;
	awucrm |= PMUM_WAKEUP4;
	//awucrm |= PMUM_WAKEUP3;
	awucrm |= PMUM_WAKEUP2;
	//awucrm |= PMUM_WAKEUP1;
	//awucrm |= PMUM_WAKEUP0;
	if (state == POWER_MODE_CHIP_SLEEP) {
		awucrm |= PMUM_AP_ASYNC_INT;
		awucrm |= PMUM_AP_FULL_IDLE;
		awucrm |= PMUM_AP2_TIMER_3;
		awucrm |= PMUM_AP2_TIMER_2;
		awucrm |= PMUM_AP2_TIMER_1;
		awucrm |= PMUM_AP1_TIMER_3;
		awucrm |= PMUM_AP1_TIMER_2;
		awucrm |= PMUM_AP1_TIMER_1;
	}
	__raw_writel(awucrm, MPMU_AWUCRM);

	apcr = 0xff087fff;	/* enable all wake-up ports */

	return apcr;
}

static unsigned int mpmu_apcr_val;	/* to restore MPMU_APCR value after resume */
static unsigned int apmu_idle_cfg_val;	/* to restore APMU_IDLE_CFG value after resume */
void mmp2_pm_enter_lowpower_mode(int state)
{
	uint32_t idle_cfg, apcr;

#if 1
	switch (state) {
	case POWER_MODE_SYS_SLEEP:
	case POWER_MODE_CHIP_SLEEP:
		__raw_writel(0xfe086000, MPMU_SPCR);
		break;
	case POWER_MODE_APPS_SLEEP:
		/* SLPEN and VCXOSD could not be set for apps sleep */
		__raw_writel(0xde006000, MPMU_SPCR);
		break;
	}
	__raw_writel(0x80300020, APMU_SP_IDLE_CFG);
#endif

	cur_lpmode = state;
	idle_cfg = __raw_readl(APMU_IDLE_CFG);
	apcr = __raw_readl(MPMU_APCR);
	mpmu_apcr_val = apcr;
	apmu_idle_cfg_val = idle_cfg;

	apcr &= ~(PMUM_SLPEN | PMUM_DDRCORSD | PMUM_APBSD | PMUM_AXISD);
	apcr &= ~PMUM_VCTCXOSD;
	idle_cfg &= ~PMUA_MOH_IDLE;

	switch (state) {
	case POWER_MODE_SYS_SLEEP:
		apcr |= PMUM_SLPEN;			/* set the SLPEN bit */
		apcr |= PMUM_VCTCXOSD;			/* set VCTCXOSD */
		apcr &= setup_wakeup_sources(state);		/* set up wakeup sources */
		/* fall through */
	case POWER_MODE_CHIP_SLEEP:
		apcr |= PMUM_SLPEN;
		apcr &= setup_wakeup_sources(state);
		/* fall through */
	case POWER_MODE_APPS_SLEEP:
		apcr |= PMUM_APBSD;			/* set APBSD */
		/* fall through */
	case POWER_MODE_APPS_IDLE:
		/*apcr |= PMUM_AXISD;			 set AXISDD bit */
		/* Enbale AXISD only when sph3 AXI clock disabled,		 
		* Otherwise usb sph3 controller will be abnormal */
		if (!(__raw_readl(APMU_USBFSIC) & 0x18)) {
			apcr |= PMUM_AXISD;			/* set AXISDD bit */
		}		
		apcr |= PMUM_DDRCORSD;			 /*set DDRCORSD bit */
		/* PJ power down (core will go to deep sleep mode when core issue idle) */
		/* because of a silicon issue on z1. current workaround is just forbid PJ4 power down on z1 */
	        if(!cpu_is_mmp2_z1())
        	        idle_cfg |= PMUA_MOH_PWRDWN;
		/* fall through */
	case POWER_MODE_CORE_EXTIDLE:
		idle_cfg |= PMUA_MOH_IDLE;		/* set the IDLE bit */
		if (cpu_is_mmp2_z0() || cpu_is_mmp2_z1()) {
			idle_cfg |= 0x000f0000;
		} else {
			idle_cfg &= ~(0x3<<28);
			idle_cfg |= 0x000a0000;
		}
	/* fall through */
	case POWER_MODE_CORE_INTIDLE:
		break;
	}


	/* program the memory controller hardware sleep type and auto wakeup */
	idle_cfg |= PMUA_MOH_DIS_MC_SW_REQ;
	idle_cfg |= PMUA_MOH_MC_WAKE_EN;
	__raw_writel(0x0, APMU_MC_HW_SLP_TYPE);		/* auto refresh */
	
	/*
	 * memory controller software sleep
	 * __raw_writel(0x0, APMU_MC_SW_SLP_TYPE);
	 * __raw_writel(0x1, APMU_MC_SLP_REQ);
	 */

	/* set DSPSD, DTCMSD, BBSD, MSASLPEN */
	apcr |= PMUM_DSPSD | PMUM_DTCMSD | PMUM_BBSD;

	/* finally write the registers back */
	__raw_writel(idle_cfg, APMU_IDLE_CFG);
	__raw_writel(apcr, MPMU_APCR);	/* 0xfe086000 */
}

#define MAXTOKENS 80

static int tokenizer(char **tbuf, const char *userbuf, ssize_t n,
			char **tokptrs, int maxtoks)
{
	char *cp, *tok;
	char *whitespace = " \t\r\n";
	int ntoks = 0;
	cp = kmalloc(n + 1, GFP_KERNEL);
	if (!cp)
		return -ENOMEM;

	*tbuf = cp;
	memcpy(cp, userbuf, n);
	cp[n] = '\0';

	do {
		cp = cp + strspn(cp, whitespace);
		tok = strsep(&cp, whitespace);
		if ((*tok == '\0') || (ntoks == maxtoks))
			break;
		tokptrs[ntoks++] = tok;
	} while (cp);

	return ntoks;
}

static ssize_t deepidle_show(struct kobject *kobj, struct kobj_attribute *attr,
		char *buf)
{
	int len = 0;

	if (enable_deepidle & IDLE_CORE_INTIDLE)
		len += sprintf(buf + len, "core_intidle ");
	if (enable_deepidle & IDLE_CORE_EXTIDLE)
		len += sprintf(buf + len, "core_extidle ");
	if (enable_deepidle & IDLE_APPS_IDLE)
		len += sprintf(buf + len, "apps_idle ");
	if (enable_deepidle & IDLE_APPS_SLEEP)
		len += sprintf(buf + len, "apps_sleep ");
	if (enable_deepidle & IDLE_CHIP_SLEEP)
		len += sprintf(buf + len, "chip_sleep ");
	if (enable_deepidle & IDLE_SYS_SLEEP)
		len += sprintf(buf + len, "sys_sleep");
	len += sprintf(buf + len, "\n");
	len += sprintf(buf + len, "Usage: echo [set|unset] "
		"[core_intidle|core_extidle|apps_idle|apps_sleep|sys_sleep] "
		"> deepidle\n");
	return len;
}

static ssize_t deepidle_store(struct kobject *kobj, struct kobj_attribute *attr,
		const char *buf, size_t len)
{
	int error = 0;
	char *tbuf = NULL;
	char *token[MAXTOKENS];
	int ntoks = tokenizer(&tbuf, buf, len, (char **)&token, MAXTOKENS);

	if (ntoks <= 0) {
		error = ntoks;
		goto out;
	}

	if (strcmp(token[0], "set") == 0) {
		if (strcmp(token[1], "core_intidle") == 0)
			enable_deepidle |= IDLE_CORE_INTIDLE;
		else if (strcmp(token[1], "core_extidle") == 0)
			enable_deepidle |= IDLE_CORE_EXTIDLE;
		else if (strcmp(token[1], "apps_idle") == 0)
			enable_deepidle |= IDLE_APPS_IDLE;
		else if (strcmp(token[1], "apps_sleep") == 0)
			enable_deepidle |= IDLE_APPS_SLEEP;
		else if (strcmp(token[1], "chip_sleep") == 0)
			enable_deepidle |= IDLE_CHIP_SLEEP;
		else if (strcmp(token[1], "sys_sleep") == 0)
			enable_deepidle |= IDLE_SYS_SLEEP;
		else
			error = -EINVAL;
	} else if (strcmp(token[0], "unset") == 0) {
		if (strcmp(token[1], "core_intidle") == 0)
			enable_deepidle &= ~IDLE_CORE_INTIDLE;
		else if (strcmp(token[1], "core_extidle") == 0)
			enable_deepidle &= ~IDLE_CORE_EXTIDLE;
		else if (strcmp(token[1], "apps_idle") == 0)
			enable_deepidle &= ~IDLE_APPS_IDLE;
		else if (strcmp(token[1], "apps_sleep") == 0)
			enable_deepidle &= ~IDLE_APPS_SLEEP;
		else if (strcmp(token[1], "chip_sleep") == 0)
			enable_deepidle &= ~IDLE_CHIP_SLEEP;
		else if (strcmp(token[1], "sys_sleep") == 0)
			enable_deepidle &= ~IDLE_SYS_SLEEP;
		else
			error = -EINVAL;
	} else {
		if (strcmp(token[0], "0") == 0)
			enable_deepidle = IDLE_ACTIVE;
		else
			error = -EINVAL;
	}
out:
	kfree(tbuf);
	return error ? error : len;
}

static struct kobj_attribute deepidle_attr = {
	.attr	= {
		.name = __stringify(deepidle),
		.mode = 0644,
	},
	.show	= deepidle_show,
	.store	= deepidle_store,
};

static ssize_t regdump_show(struct kobject *kobj, struct kobj_attribute *attr,
		char *buf)
{
	int len = 0;
	unsigned int addr;
	uint32_t ccr = __raw_readl(TIMERS1_VIRT_BASE + TMR_CCR);
	uint32_t cr0 = __raw_readl(TIMERS1_VIRT_BASE + TMR_CR(0));

	for (addr = MPMU_SPCR; addr <= (MPMU_SPCR + 0x4c); addr += 4)
		len += sprintf(buf + len, "(MPMU-SP)0x%x: 0x%x\n", addr, __raw_readl(addr));
	for (addr = MPMU_APCR; addr <= (MPMU_APCR + 0x4c); addr += 4)
		len += sprintf(buf + len, "(MPMU-PJ)0x%x: 0x%x\n", addr, __raw_readl(addr));
	for (addr = (APMU_CC_PJ - 0x4); addr <= (APMU_CCIC2_GATE + 0x4); addr += 4)
		len += sprintf(buf + len, "(PMU)0x%x: 0x%x\n", addr, __raw_readl(addr));

	len += sprintf(buf + len, "ccr 0x%x, cr0: 0x%x\n", ccr, cr0);
	return len;
}

static ssize_t regdump_store(struct kobject *kobj, struct kobj_attribute *attr,
		const char *buf, size_t len)
{
	unsigned int addr, value;
	int error = 0;
	char *tbuf = NULL;
	char *token[MAXTOKENS];
	int ntoks = tokenizer(&tbuf, buf, len, (char **)&token, MAXTOKENS);

	if (ntoks <= 0) {
		error = ntoks;
		goto out;
	}

	addr = simple_strtoul(token[0], NULL, 0);
	if (addr < 0xd4000000 || addr > 0xd4400000) {
		error = -EINVAL;
		goto out;
	}
	value = simple_strtoul(token[1], NULL, 0);

	__raw_writel(value, addr+0x2a000000);
out:
	kfree(tbuf);
	return error ? error : len;
}

static struct kobj_attribute regdump_attr = {
	.attr	= {
		.name = __stringify(regdump),
		.mode = 0644,
	},
	.show	= regdump_show,
	.store	= regdump_store,
};

static struct attribute * g[] = {
	&deepidle_attr.attr,
	&regdump_attr.attr,
	NULL,
};

static struct attribute_group attr_group = {
	.attrs = g,
};

static void apbc_set_rst(unsigned int clk_addr, int flag)
{
	unsigned int clkreg;
	unsigned int rst_msk;

	rst_msk = 1 << 2;
	clkreg = __raw_readl(clk_addr);
	if (flag)
		clkreg |= rst_msk;
	else
		clkreg &= ~rst_msk;
	__raw_writel(clkreg, clk_addr);

	return ;
}

static void pm_apbc_clk_disable(void)
{
	unsigned int clkreg;
	unsigned int rst_msk;

	rst_msk = 0x03;
/*	clkreg = __raw_readl(APBC_MMP2_GPIO);
	clkreg &= ~rst_msk;
	__raw_writel(clkreg, APBC_MMP2_GPIO); */
	clkreg = __raw_readl(APBC_MMP2_MPMU);
	clkreg &= ~rst_msk;
	__raw_writel(clkreg, APBC_MMP2_MPMU);
	clkreg = __raw_readl(APBC_MMP2_IPC);
	clkreg &= ~rst_msk;
	__raw_writel(clkreg, APBC_MMP2_IPC);
	clkreg = __raw_readl(APBC_MMP2_THSENS1);
	clkreg &= ~rst_msk;
	__raw_writel(clkreg, APBC_MMP2_THSENS1);

//	apbc_set_rst(APBC_MMP2_GPIO, 1);
	apbc_set_rst(APBC_MMP2_MPMU, 1);
	apbc_set_rst(APBC_MMP2_IPC, 1);
	apbc_set_rst(APBC_MMP2_THSENS1, 1);

	return ;
}

static void pm_apbc_clk_enable(void)
{
	unsigned int clkreg;
	unsigned int rst_msk;

	rst_msk = 0x03;
 /*	clkreg = __raw_readl(APBC_MMP2_GPIO);
	clkreg |= rst_msk;
	__raw_writel(clkreg, APBC_MMP2_GPIO);*/
	clkreg = __raw_readl(APBC_MMP2_MPMU);
	clkreg |= rst_msk;
	__raw_writel(clkreg, APBC_MMP2_MPMU);
	clkreg = __raw_readl(APBC_MMP2_IPC);
	clkreg |= rst_msk;
	__raw_writel(clkreg, APBC_MMP2_IPC);
	clkreg = __raw_readl(APBC_MMP2_THSENS1);
	clkreg |= rst_msk;
	__raw_writel(clkreg, APBC_MMP2_THSENS1);

//	apbc_set_rst(APBC_MMP2_GPIO, 0);
	apbc_set_rst(APBC_MMP2_MPMU, 0);
	apbc_set_rst(APBC_MMP2_IPC, 0);
	apbc_set_rst(APBC_MMP2_THSENS1, 0);

	return ;
}

static void pm_scu_clk_disable(void)
{
	unsigned int val;

	/* close AXI fabric clock gate */
	__raw_writel(0x0, 0xfe282c64);
	__raw_writel(0x0, 0xfe282c68);

	val = __raw_readl(0xfe282c1c);	/* MCB_CONF */
	val |= 0xf0;
	__raw_writel(val, 0xfe282c1c);

	return ;
}

static void pm_scu_clk_enable(void)
{
	unsigned int val;

	/* open AXI fabric clock gate */
	if (cpu_is_mmp2_z0() || cpu_is_mmp2_z1())
		__raw_writel(0x03002001, 0xfe282c64);
	else
		__raw_writel(0x03003003, 0xfe282c64);
	__raw_writel(0x00303030, 0xfe282c68);

	val = __raw_readl(0xfe282c1c);	/* MCB_CONF */
	val &= ~(0xf0);
	__raw_writel(val, 0xfe282c1c);

	return ;
}

static void pm_apmu_clk_disable(void)
{
	unsigned int val;

	val = __raw_readl(0xfe2828d8);	/* PMUA_MSPRO_CLK_RES_CTRL */
	val &= ~(0x3f);
	__raw_writel(val, 0xfe2828d8);

	val = __raw_readl(0xfe2828dc);	/* PMUA_GLB_CLK_RES_CTRL */
	val &= ~(0x1fffd);
	__raw_writel(val, 0xfe2828dc);

	//__raw_writel(0x0, 0xfe282868);	/* PMUA_WTM_CLK_RES_CTRL */

	val = __raw_readl(0xfe28285c);		/* PMUA_USB_CLK_RES_CTRL */
	val &= ~(0x9);
	val = __raw_writel(val, 0xfe28285c);

	return ;
}

static void pm_apmu_clk_enable(void)
{
	unsigned int val;

	val = __raw_readl(0xfe2828d8);
	val |= 0x3f;
	__raw_writel(val, 0xfe2828d8);

	val = __raw_readl(0xfe2828dc);
	val |= 0x1fffd;
	__raw_writel(val, 0xfe2828dc);

	//__raw_writel(0x1b, 0xfe282868);

	val = __raw_readl(0xfe28285c);
	val |= 0x9;
	__raw_writel(val, 0xfe28285c);

	return ;
}

static void pm_mpmu_clk_disable(void)
{
	unsigned int val;

	/* disable clocks in MPMU_CGR_PJ register, except clock for APMU_PLL1, APMU_PLL1_2 and AP_26M*/
	__raw_writel(0x0000a010, MPMU_ACGR);

#if 0
	/* set PLL2 in reset status */
	val = __raw_readl(MPMU_PLL2_CTRL1);
	val &= ~(1 <<29);
	__raw_writel(val, MPMU_PLL2_CTRL1);
#endif
	/* disable I2S clock input/output to SYSCLKx */
	val = __raw_readl(MPMU_ISCCR1);
	val &= ~(1 << 31);
	__raw_writel(val, MPMU_ISCCR1);
	val = __raw_readl(MPMU_ISCCR2);
	val &= ~(1 << 31);
	__raw_writel(val, MPMU_ISCCR2);

	return ;
}

static void pm_mpmu_clk_enable(void)
{
	unsigned int val;

	__raw_writel(0xdffefffe, MPMU_ACGR);
	val = __raw_readl(MPMU_PLL2_CTRL1);
	val |= (1 << 29);
	__raw_writel(val, MPMU_PLL2_CTRL1);
	val = __raw_readl(MPMU_ISCCR1);
	val |= (1 << 31);
	__raw_writel(val, MPMU_ISCCR1);
	val = __raw_readl(MPMU_ISCCR2);
	val |= (1 << 31);
	__raw_writel(val, MPMU_ISCCR2);

	return ;
}

void mmp2_cpu_do_idle(void)
{
	if (machine_is_brownstone() || machine_is_g50()) {
		/* FIXME unmask icu irq to enable gpio wakeup */
		if (!machine_is_g50())
			__raw_writel(0x1, ICU_REG(0x110));
		jump_to_lp_sram((vaddr_base + 0xc00), (vaddr_base + 0xe00), va_dmcu);
		__raw_writel(mpmu_apcr_val, MPMU_APCR);
		__raw_writel(apmu_idle_cfg_val, APMU_IDLE_CFG);
	} else
		cpu_do_idle();
}

static int mmp2_pm_enter(suspend_state_t state)
{
	int temp;

	//__raw_writel(0x5, MPMU_SCCR);

	temp = __raw_readl(ICU_INT_CONF(IRQ_MMP2_PMIC_MUX));
	if (!(temp & ICU_INT_ROUTE_PJ4_IRQ)) {
		printk("############# %s: PMIC interrupt is handling ##############\n", __func__);
		return -EAGAIN;
	}

	/*
	 * Setting PCR_SP and SP_IDLE_CFG for dragonite.
	 * May need to be moved to Wtm.
	 */
	mmp2_pm_enter_lowpower_mode(POWER_MODE_SYS_SLEEP);

	/* 
	 * PJ SRAM power down (sram power off when core issue idle)
	 * idle_cfg |= PMUA_MOH_SRAM_PWRDWN;
	 */

	temp = __raw_readl(APMU_SRAM_PWR_DWN);
	temp |= ((1 << 19) | (1 << 18));
	__raw_writel(temp, APMU_SRAM_PWR_DWN);


	/* 
	 * TODO - need to power down sram/l2$
	 * mmp2_cpu_disable_l2(0);
	 * outer_cache.flush_range(0, -1ul);
	 */

	/* TODO - whether need to handle low power island? */

	/* set RST in APBC_TWSIx_CLK_RST registers */
	/* just disable TWSI1 clk rather than reset it since it's needed to access PMIC onkey
	 * when system is waken up from low power mode */
	__raw_writel(0x0, APBC_MMP2_TWSI1);
//	apbc_set_rst(APBC_MMP2_TWSI1, 1);
	apbc_set_rst(APBC_MMP2_TWSI2, 1);
	apbc_set_rst(APBC_MMP2_TWSI3, 1);
	apbc_set_rst(APBC_MMP2_TWSI4, 1);
	apbc_set_rst(APBC_MMP2_TWSI5, 1);
	apbc_set_rst(APBC_MMP2_TWSI6, 1);

	pm_apbc_clk_disable();		/* disable clocks of GPIO, MPMU, IPC, THSES1 in APBC */
	pm_scu_clk_disable();		/* disable clocks in SCU */
	pm_apmu_clk_disable();		/* disable clocks in APMU */
	pm_mpmu_clk_disable();		/* disable clocks in MPMU */

	printk("%s: before suspend\n", __func__);
	mmp2_cpu_do_idle();
	printk("%s: after suspend\n", __func__);

	pm_mpmu_clk_enable();		/* enable clocks in MPMU */
	pm_apmu_clk_enable();		/* enable clocks in APMU */
	pm_scu_clk_enable();		/* enable clocks in SCU */
	pm_apbc_clk_enable();		/* enable clocks of GPIO, MPMU, IPC, THSES1 in APBC */

	/* clear RST in APBC_TWSIx_CLK_RST registers */
	__raw_writel(0x3, APBC_MMP2_TWSI1);	/* enable TWSI1 clk */
//	apbc_set_rst(APBC_MMP2_TWSI1, 0);
	apbc_set_rst(APBC_MMP2_TWSI2, 0);
	apbc_set_rst(APBC_MMP2_TWSI3, 0);
	apbc_set_rst(APBC_MMP2_TWSI4, 0);
	apbc_set_rst(APBC_MMP2_TWSI5, 0);
	apbc_set_rst(APBC_MMP2_TWSI6, 0); 
	/*
	 * mmp2_cpu_enable_l2(0);
	 */

	/*
	 * idle_cfg &= (~PMUA_MOH_SRAM_PWRDWN);
	 */

#ifdef CONFIG_WAKELOCK
	if (__raw_readl(MPMU_AWUCRS) & PMUM_WAKEUP7)
		pmic_wakeup_detect = 1;
	if (__raw_readl(MPMU_AWUCRS) & PMUM_WAKEUP2)
		sdh_wakeup_detect = 1;
#endif

	return 0;
}

#ifdef NOT_USED
static void mmp2_pm_irq(unsigned int enable)
{
	struct irq_desc *desc = irq_to_desc(pm_irqs[0]);
	unsigned int i, j, flag=0;

	for (i = 0; i < 64; i++) {
		flag = 0;
		for (j = 0; j < ARRAY_SIZE(pm_irqs); j++)
			if (i == pm_irqs[j]) {
				flag = 1;
				break;
			}
		if(flag)
			continue;
		if(enable)
			desc->chip->unmask(i);
		else
			desc->chip->mask(i);
	}
}
#endif

/*
 * Called after processes are frozen, but before we shut down devices.
 */
static int mmp2_pm_prepare(void)
{
	/*
	 * system will disable all active irqs in suspend_device_irqs.
	 * so no need to disable irqs manually here.
	 * mmp2_pm_irq(0);
	 */
	return 0;
}

/*
 * Called after devices are re-setup, but before processes are thawed.
 */
static void mmp2_pm_finish(void)
{
	pm_state = PM_SUSPEND_ON;
	/*
	 * system will enable all irqs in resume_device_irqs.
	 * so no need to enable irqs manually here.
	 * mmp2_pm_irq(1);
	 */
	__raw_writel(0x1, MPMU_VRCR);
}

#ifdef CONFIG_WAKELOCK
static void mmp2_pm_wake(void)
{
	if (pmic_wakeup_detect)
		wake_lock_timeout(&wakelock_pmic_wakeup, HZ * 2);
	pmic_wakeup_detect = 0;
	if (sdh_wakeup_detect)
		wake_lock_timeout(&wakelock_sdh_wakeup, HZ * 3);
	sdh_wakeup_detect = 0;
}
#endif

static int mmp2_pm_valid(suspend_state_t state)
{
	int ret = 1;

	if (state == PM_SUSPEND_STANDBY) {
		pm_state = PM_SUSPEND_STANDBY;
	} else if (state == PM_SUSPEND_MEM) {
		pm_state = PM_SUSPEND_MEM;
	} else {
		ret = 0;
	}
	return ret;
}

/*
 * Set to PM_DISK_FIRMWARE so we can quickly veto suspend-to-disk.
 */
static struct platform_suspend_ops mmp2_pm_ops = {
	.valid		= mmp2_pm_valid,
	.prepare	= mmp2_pm_prepare,
	.enter		= mmp2_pm_enter,
#ifdef CONFIG_WAKELOCK
	.wake		= mmp2_pm_wake,
#endif
	.finish		= mmp2_pm_finish,
};

static int __init mmp2_pm_init(void)
{
	uint32_t apcr;

	if (!cpu_is_mmp2())
		return -EIO;
	if (sysfs_create_group(power_kobj, &attr_group))
		return -1;

	suspend_set_ops(&mmp2_pm_ops);

	/* Clear default low power control bit */
	apcr = __raw_readl(MPMU_APCR);
	apcr &= ~PMUM_SLPEN & ~PMUM_DDRCORSD & ~PMUM_APBSD & ~PMUM_AXISD;
	__raw_writel(apcr, MPMU_APCR);

	va_dmcu = NULL;
	if (machine_is_brownstone() || machine_is_g50()) {
		map_sram();
		copy_lp_to_sram(vaddr_base + 0xc00);
		va_dmcu = ioremap(0xd0000000, 0xfff);
		if (!va_dmcu)
			return -EINVAL;
	}

#ifdef CONFIG_DVFM
	dvfm_register("mmp2-pm", &dvfm_dev_idx);
#endif
	register_pm_notifier(&mmp2pm_notif_block);

#ifdef CONFIG_WAKELOCK
	wake_lock_init(&wakelock_pmic_wakeup, WAKE_LOCK_SUSPEND, "wakelock_pmic_wakeup");
	wake_lock_init(&wakelock_sdh_wakeup, WAKE_LOCK_SUSPEND, "wakelock_sdh_wakeup");
#endif
	return 0;
}

late_initcall(mmp2_pm_init);
