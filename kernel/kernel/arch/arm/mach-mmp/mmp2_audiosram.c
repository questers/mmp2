/*
 *  linux/arch/arm/mach-mmp/mmp2_audiosram.c
 *
 *  based on mach-davinci/sram.c - DaVinci simple SRAM allocator
 *
 *  Copyright (c) 2011 Marvell Semiconductors Inc.
 *  All Rights Reserved
 *
 *  Add for mmp2 audio sram support - Leo Yan <leoy@marvell.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/err.h>
#include <linux/genalloc.h>

#include <mach/mmp2_audiosram.h>

static struct gen_pool *sram_pool;
static u32 audio_sram_phys;
static void __iomem *audio_sram_virt;
static u32 audio_sram_size;

void *audio_sram_alloc(size_t len, dma_addr_t *dma)
{
	unsigned long vaddr;
	dma_addr_t dma_base = (dma_addr_t)audio_sram_phys;

	if (dma)
		*dma = 0;
	if (!sram_pool || (dma && !dma_base))
		return NULL;

	vaddr = gen_pool_alloc(sram_pool, len);
	if (!vaddr)
		return NULL;

	if (dma)
		*dma = dma_base + (vaddr - (unsigned long)audio_sram_virt);
	return (void *)vaddr;
}
EXPORT_SYMBOL(audio_sram_alloc);

void audio_sram_free(void *addr, size_t len)
{
	gen_pool_free(sram_pool, (unsigned long) addr, len);
}
EXPORT_SYMBOL(audio_sram_free);

static int __devinit audio_sram_probe(struct platform_device *pdev)
{
	struct resource *res;
	int ret = 0;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (res == NULL) {
		dev_err(&pdev->dev, "no memory resource defined\n");
		ret = -ENODEV;
		goto out;
	}
	audio_sram_phys = res->start;
	audio_sram_size = res->end - res->start + 1;
	audio_sram_virt = ioremap(audio_sram_phys, audio_sram_size);

	sram_pool = gen_pool_create(ilog2(AUDIO_SRAM_GRANULARITY), -1);
	if (!sram_pool) {
		dev_err(&pdev->dev, "create pool failed\n");
		ret = -ENOMEM;
		goto create_pool_err;
	}

	ret = gen_pool_add(sram_pool, (unsigned long)audio_sram_virt,
			   audio_sram_size, -1);
	if (ret < 0) {
		dev_err(&pdev->dev, "add new chunk failed\n");
		ret = -ENOMEM;
		goto add_chunk_err;
	}

	return 0;

add_chunk_err:
	gen_pool_destroy(sram_pool);
create_pool_err:
	iounmap(audio_sram_virt);
out:
	return ret;
}

static int __devexit audio_sram_remove(struct platform_device *pdev)
{
	gen_pool_destroy(sram_pool);
	iounmap(audio_sram_virt);
	return 0;
}

static struct platform_driver audio_sram_driver = {
	.probe		= audio_sram_probe,
	.remove		= audio_sram_remove,
	.driver		= {
		.name	= "mmp2-audiosram",
	},
};

static int __init audio_sram_init(void)
{
	return platform_driver_register(&audio_sram_driver);
}

static void __exit audio_sram_exit(void)
{
	platform_driver_unregister(&audio_sram_driver);
}

module_init(audio_sram_init);
module_exit(audio_sram_exit);
MODULE_LICENSE("GPL");

