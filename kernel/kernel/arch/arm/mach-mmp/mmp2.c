/*
 * linux/arch/arm/mach-mmp/mmp2.c
 *
 * code name MMP2
 *
 * Copyright (C) 2009 Marvell International Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/io.h>

#include <asm/mach/time.h>
#include <asm/hardware/cache-tauros2.h>

#include <mach/addr-map.h>
#include <mach/regs-apbc.h>
#include <mach/regs-apmu.h>
#include <mach/regs-mpmu.h>
#include <mach/regs-zsp.h>
#include <mach/cputype.h>
#include <mach/irqs.h>
#include <mach/dma.h>
#include <mach/mfp.h>
#include <mach/gpio.h>
#include <mach/mmp2_dma.h>
#include <mach/devices.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/clk.h>

#include <linux/usb.h>
#include <linux/usb/otg.h>
#include <plat/pxa_u2o.h>
#include <plat/vbus.h>
#include <linux/mmc/sdhci.h>
#include <plat/sdhci.h>

#include <mach/soc_vmeta.h>

#include "common.h"
#include "clock.h"

#define MFPR_VIRT_BASE	(APB_VIRT_BASE + 0x1e000)

#define APMASK(i)	(GPIO_REGS_VIRT + BANK_OFF(i) + 0x9c)

static struct mfp_addr_map mmp2_addr_map[] __initdata = {

	MFP_ADDR_X(GPIO0, GPIO58, 0x54),
	MFP_ADDR_X(GPIO59, GPIO73, 0x280),
	MFP_ADDR_X(GPIO74, GPIO101, 0x170),

	MFP_ADDR(GPIO102, 0x0),
	MFP_ADDR(GPIO103, 0x4),
	MFP_ADDR(GPIO104, 0x1fc),
	MFP_ADDR(GPIO105, 0x1f8),
	MFP_ADDR(GPIO106, 0x1f4),
	MFP_ADDR(GPIO107, 0x1f0),
	MFP_ADDR(GPIO108, 0x21c),
	MFP_ADDR(GPIO109, 0x218),
	MFP_ADDR(GPIO110, 0x214),
	MFP_ADDR(GPIO111, 0x200),
	MFP_ADDR(GPIO112, 0x244),
	MFP_ADDR(GPIO113, 0x25c),
	MFP_ADDR(GPIO114, 0x164),
	MFP_ADDR_X(GPIO115, GPIO122, 0x260),

	MFP_ADDR(GPIO123, 0x148),
	MFP_ADDR_X(GPIO124, GPIO141, 0xc),

	MFP_ADDR(GPIO142, 0x8),
	MFP_ADDR_X(GPIO143, GPIO151, 0x220),
	MFP_ADDR_X(GPIO152, GPIO153, 0x248),
	MFP_ADDR_X(GPIO154, GPIO155, 0x254),
	MFP_ADDR_X(GPIO156, GPIO159, 0x14c),

	MFP_ADDR(GPIO160, 0x250),
	MFP_ADDR(GPIO161, 0x210),
	MFP_ADDR(GPIO162, 0x20c),
	MFP_ADDR(GPIO163, 0x208),
	MFP_ADDR(GPIO164, 0x204),
	MFP_ADDR(GPIO165, 0x1ec),
	MFP_ADDR(GPIO166, 0x1e8),
	MFP_ADDR(GPIO167, 0x1e4),
	MFP_ADDR(GPIO168, 0x1e0),

	MFP_ADDR_X(TWSI1_SCL, TWSI1_SDA, 0x140),
	MFP_ADDR_X(TWSI4_SCL, TWSI4_SDA, 0x2bc),

	MFP_ADDR(PMIC_INT, 0x2c4),
	MFP_ADDR(CLK_REQ, 0x160),
	MFP_ADDR(VCXO_REQ, 0x168),
	MFP_ADDR(VCXO_OUT, 0x16c),

	MFP_ADDR_END,
};

void mmp2_clear_pmic_int(void)
{
	unsigned long mfpr_pmic, data;

	mfpr_pmic = APB_VIRT_BASE + 0x1e000 + 0x2c4;
	data = __raw_readl(mfpr_pmic);
	__raw_writel(data | (1 << 6), mfpr_pmic);
	__raw_writel(data, mfpr_pmic);
}

#ifdef CONFIG_PM
static int mmp2_set_wake(unsigned int irq, unsigned int on)
{
	/* FIXME - should set wakeup resource here */
	if (on)
		enable_irq(irq);
	return 0;
}
static int mmp2_gpio_set_wake(unsigned int irq, unsigned int on)
{
	return 0;
}

#else
#define mmp2_set_wake NULL
#define mmp2_gpio_set_wake NULL
#endif

static void __init mmp2_init_gpio(void)
{
	int i;

	/* enable GPIO clock */
	__raw_writel(APBC_APBCLK | APBC_FNCLK, APBC_MMP2_GPIO);

	/* unmask GPIO edge detection for all 6 banks -- APMASKx */
	for (i = 0; i < 6; i++)
		__raw_writel(0xffffffff, APMASK(i));

	pxa_init_gpio(IRQ_MMP2_GPIO, 0, 167, mmp2_gpio_set_wake);
}

void __init mmp2_init_irq(void)
{
	mmp2_init_icu(mmp2_set_wake);
	mmp2_init_gpio();
}

static struct clk clk_usb_phy;
static int usb_phy_enable = 0;
int mmp2_init_usb_phy(unsigned int base, unsigned int phy)
{
	if (!usb_phy_enable) {
		clk_enable(&clk_usb_phy);
		usb_phy_enable = 1;
	}
	return 0;
}

int mmp2_deinit_usb_phy(unsigned int base, unsigned int phy)
{
	if (usb_phy_enable) {
		clk_disable(&clk_usb_phy);
		usb_phy_enable = 0;
	}
	return 0;
}

/* usb otg phy power control */
static int u2o_phy_set_power(int on)
{
	unsigned int u2o_phy = AXI_VIRT_BASE
			+ (PXA168_U2O_PHYBASE & 0xffff);

	if (on) {
		/* usb phy power on */
		u2o_set(u2o_phy, UTMI_CTRL, 1 << UTMI_CTRL_PWR_UP_SHIFT);
	} else {
		/* usb phy power off */
		u2o_clear(u2o_phy, UTMI_CTRL, 1 << UTMI_CTRL_PWR_UP_SHIFT);
	}

	return 0;
}

#define portsc	0x184
int mmp2_ulpi_init(unsigned int base, unsigned int phy)
{
	u32 val;

	/* check SPH clock source, should be PMU 60MHz */
	val = __raw_readl(phy + 0x30);
	pr_debug(" SPH clock src 0xf0003830: 0x%x\n",
		__raw_readl(phy + 0x30));
	if ((val & 0x100) == 0)
		pr_warning("SPH clock from external ULPI???\n");

	/* select ULPI interface in USB FSIC */
	pr_debug(" select ULPI interface in USB FSIC\n");
	val = __raw_readl(phy + FSIC_MISC);
	val |= 1<<27;
	__raw_writel(val, phy + FSIC_MISC);

	/* select ULPI interface in SPH usb controller  */
	pr_debug(" select ULPI interface in SPH usb controller\n");
	val = __raw_readl(base + portsc); val &= ~(0x3<<30);
	val |= 2<<30; __raw_writel(val, base + portsc);

	/* switch back to ULPI clock (disable PMU 60MHz)
	 * after writing to PORTSC */
	val = __raw_readl(phy + FSIC_CTRL);
	val &= ~(1<<8);		/* val |= (1<<8); PMU 60MHz as input clock */
	__raw_writel(val, phy + FSIC_CTRL);

	pr_debug("%s FSIC_MISC 0x%x FSIC_CTRL 0x%x sph 0x%x portsc 0x%x\n",
		__func__, __raw_readl(phy + FSIC_MISC),
		__raw_readl(phy + FSIC_CTRL), __raw_readl(base + 0x100),
		__raw_readl(base + portsc));

	return 0;
}

int mmp2_fsic_enable(unsigned int base, unsigned int phy)
{
	pr_debug("%s 0xd4050034: 0x%x\n 0xd4050414: 0x%x\n", __func__,
			__raw_readl(MPMU_PLL2CR), __raw_readl(MPMU_PLL2_CTRL1));

	/* Enable PMU 60MHz clock to SPH controller */
	__raw_writel(0x100, phy + FSIC_CTRL);

	pr_debug("%s select PMU 60MHz clock for FSIC/SPH3 FSIC_CTRL %x\n",
		__func__, __raw_readl(phy + FSIC_CTRL));

	return 0;
}

int mmp2_hsic_init(unsigned int base, unsigned int phy)
{
	u32 hsic_int = __raw_readl(phy + HSIC_INT);
	u32 status = __raw_readl(base + portsc);
	int count;

	/*disable irq*/
	hsic_int &= ~HSIC_INT_CONNECT_EN;
	__raw_writel(hsic_int, phy + HSIC_INT);

	pr_info("%s hsic_int 0x%x portsc 0x%x\n",
		__func__, hsic_int, status);

	/* enable port power and reserved bit 25 */
	status = __raw_readl(base + portsc);
	status |= (0x00001000) | (1<<25);
	__raw_writel(status, base + portsc);

	/* test mode: force enable hs */
	status = __raw_readl(base + portsc);
	status &= ~(0xf<<16); status |= (0x5<<16);
	__raw_writel(status, base + portsc);

	/* disable test mode */
	status = __raw_readl(base + portsc);
	status &= ~(0xf<<16);
	__raw_writel(status, base + portsc);

	/* check HS ready */
	count = 0x10000;
	do {
		hsic_int = __raw_readl(phy + HSIC_INT);
		status = __raw_readl(base + portsc);
		count--;
	} while ((count >=0) && !(hsic_int & HSIC_INT_HS_READY));
	if (count <= 0) {
		printk("HSIC_INT_HS_READY not set: hsic_int 0x%x\n", hsic_int);
		return -EAGAIN;
	}

	/* issue port reset */
	status = __raw_readl(base + portsc);
	status |= (1<<8);
	__raw_writel(status, base + portsc);

	/* check reset done */
	count =0x10000;
	do {
		status = __raw_readl(base + portsc);
		count--;
	} while ((count >=0) && !(status & (1<<8)));
	if (count <= 0) {
		printk("port reset not done: portsc 0x%x\n", status);
		return -EAGAIN;
	}

	return 0;
}

int mmp2_hsic_enable(unsigned int base, unsigned int phy)
{
	u32 val = __raw_readl(phy + HSIC_CTRL);

	/* enable hsic phy */
	val |= HSIC_CTRL_HSIC_ENABLE;
	__raw_writel(val, phy + HSIC_CTRL);

	return 0;
}

static void gc800_clk_enable(struct clk *clk)
{
	u32 tmp;

	tmp = __raw_readl(clk->clk_rst);
	tmp &= ~(0xf<<4);
	if(!cpu_is_mmp2_z0() && !cpu_is_mmp2_z1()){
		/* FIXME enable OTG PHY clock to feed GC*/
		//clk_enable(&clk_usb_phy);
//		tmp |= ((2<<4) | (1<<6));
//		tmp |= ((2<<4) | (1<<6));
		tmp &=~(0x30);//use pll1/4 for GC_ACLK
		tmp &=~(0xc0);//use pll1/2 for GC_CLK

        //  tmp |= ((1<<12) | (1<<14));
            tmp &= ~(1 << 12);
            tmp &= ~(1 << 14);
		printk(KERN_DEBUG "Bus Clock: USB PLL 480MHz\n");
		printk(KERN_DEBUG "GC Controller Clock: USB PLL 480MHz\n");
	}else{
		u32 pll2_speed;
		pll2_speed = __raw_readl(MPMU_PLL2_CTRL1);
		pll2_speed = (pll2_speed >> 6) & 0x1f;
		if(pll2_speed >= 0x10){/*PLL2 >= 650MHz*/
			if(pll2_speed <= 0x13){ /*PLL2 <= 811MHz*/
				if(cpu_is_mmp2_z0()){
					tmp |= ((3<<4) | (3<<6));
					printk(KERN_DEBUG "Bus Clock: PLL2/2\n");
					printk(KERN_DEBUG "GC Controller Clock: PLL2/2\n");
				}else{
					tmp |= ((0<<4) | (0<<6));
					printk(KERN_DEBUG "Bus Clock: PLL1/4\n");
					printk(KERN_DEBUG "GC Controller Clock: PLL1/2\n");
				}
			}else{/*PLL2 >= 811MHz*/
				if(cpu_is_mmp2_z0() || cpu_is_mmp2_z1()){
					tmp |= ((3<<4) | (3<<6));
					printk(KERN_DEBUG "Bus Clock: PLL2/2\n");
					printk(KERN_DEBUG "GC Controller Clock: PLL2/2\n");
				}else{
					tmp |= ((2<<4) | (3<<6));
					printk(KERN_DEBUG "Bus Clock: PLL2/3\n");
					printk(KERN_DEBUG "GC Controller Clock: PLL2/2\n");
				}
			}

		}else if(pll2_speed >= 0xa){/*PLL2: 403MHz~650MHz*/
			if(cpu_is_mmp2_z0() || cpu_is_mmp2_z1()){
				tmp |= ((3<<4) | (2<<6));
				printk(KERN_DEBUG "Bus Clock: PLL2/2\n");
				printk(KERN_DEBUG "GC Controller Clock: PLL2\n");
			}else{
				tmp |= ((3<<4) | (2<<6));
				printk(KERN_DEBUG "Bus Clock: PLL2\n");
				printk(KERN_DEBUG "GC Controller Clock: PLL2\n");
			}
		}else{/*PLL2 <= 403MHz*/
			if(cpu_is_mmp2_z0()){
				tmp |= ((2<<4) | (2<<6));
				printk(KERN_DEBUG "Bus Clock: PLL2\n");
				printk(KERN_DEBUG "GC Controller Clock: PLL2\n");
			}else{
				tmp |= ((0<<4) | (0<<6));
				printk(KERN_DEBUG "Bus Clock: PLL1/4\n");
				printk(KERN_DEBUG "GC Controller Clock: PLL1/2\n");
			}
		}
	}
	tmp |= (3<<9);
	__raw_writel(tmp, clk->clk_rst);

	tmp |= ((1<<3) | (1<<2));
	__raw_writel(tmp, clk->clk_rst);

	udelay(150);

	tmp |= (1<<8);
	__raw_writel(tmp, clk->clk_rst);

	udelay(1);

	if(!cpu_is_mmp2_z0() && !cpu_is_mmp2_z1() && !cpu_is_mmp2_a0()) {
		if((tmp & (1<<15)) == 0){
			tmp |= (1<<15); 
			__raw_writel(tmp, clk->clk_rst);
		}
	}

	tmp |= (1<<0);
	__raw_writel(tmp, clk->clk_rst);

	udelay(100);

	tmp |= (1<<1);
	__raw_writel(tmp, clk->clk_rst);
	udelay(100);
}

static void gc800_clk_disable(struct clk *clk)
{
	u32 tmp;

	tmp = __raw_readl(clk->clk_rst);
	tmp &= ~(1<<0);
	__raw_writel(tmp, clk->clk_rst);

	tmp &= ~(1<<1);
	__raw_writel(tmp, clk->clk_rst);

	tmp &= ~(1<<8);
	__raw_writel(tmp, clk->clk_rst);

	tmp &= ~((1<<2) | (1<<3));
	__raw_writel(tmp, clk->clk_rst);

	tmp &= ~(3<<9);
	__raw_writel(tmp, clk->clk_rst);

	/* FIXME disable OTG PHY clock */
	//clk_disable(&clk_usb_phy);
}

static int gc800_clk_setrate(struct clk *clk, unsigned long target_rate)
{
	return 0;
}

static unsigned long pll_clk_calculate(u32 refdiv, u32 fbdiv)
{
	u32 input_clk;
	u32 output_clk;
	u32 M, N;

	switch (refdiv) {
	case 3:
		M = 5;
		input_clk = 19200000;
		break;
	case 4:
		M = 6;
		input_clk = 26000000;
		break;
	default:
		printk(KERN_WARNING "The PLL REFDIV should be 0x03 or 0x04\n");
		return 0;
	}

	N = fbdiv + 2;
	output_clk = input_clk/10 * N / M *10;
	return output_clk;
}

static unsigned long pll1_get_clk(void)
{
	u32 mpmu_fccr;
	u32 mpmu_posr;
	u32 refdiv;
	u32 fbdiv;
	u32 pll1_en;

	mpmu_fccr = __raw_readl(MPMU_FCCR);
	pll1_en   = (mpmu_fccr >> 14) & 0x1;
	mpmu_posr = __raw_readl(MPMU_POSR);

	if (pll1_en) {
		refdiv    = (mpmu_posr >> 9) & 0x1f;
		fbdiv     = mpmu_posr & 0x1ff;
		return pll_clk_calculate(refdiv, fbdiv);
	} else {
		return 797330000;
	}
}

static unsigned long pll2_get_clk(void)
{
	u32 mpmu_posr;
	u32 refdiv;
	u32 fbdiv;

	mpmu_posr = __raw_readl(MPMU_POSR);
	refdiv      = (mpmu_posr >> 23) & 0x1f;
	fbdiv       = (mpmu_posr >> 14) & 0x1ff;
	return pll_clk_calculate(refdiv, fbdiv);
}

static unsigned long gc800_clk_getrate(struct clk *clk)
{
	u32 apmu_gc;
	u32 tmp;
	u32 gc_clk;
	u32 pll1clk;
	u32 pll2clk;

	gc_clk = 0;
	pll1clk = pll1_get_clk();
	pll2clk = pll2_get_clk();

	/* GC800 */
	apmu_gc = readl(APMU_GC);
	if (((apmu_gc >> 9) & 0x3) == 0x3) {
		if(cpu_is_mmp2_z0() || cpu_is_mmp2_z1()){
			tmp = (apmu_gc >> 6) & 0x3;
			switch (tmp) {
			case 0: /* PLL1/4 */
				gc_clk = pll1clk/4;
				break;
			case 1: /* PLL1/6 */
				gc_clk = pll1clk/6;
				break;
			case 2: /* PLL2 */
				gc_clk = pll2clk;
				break;
			case 3: /* PLL2/2 */
				gc_clk = pll2clk/2;
				break;
			default:
				break;
			}
		}else{
			if((apmu_gc >> 12) & 0x1){
				tmp = (apmu_gc >> 6) & 0x3;
				switch (tmp) {
				case 0: /* PLL2/4 */
					gc_clk = pll2clk/4;
					break;
				case 1: /* USB PLL */
					gc_clk = 480000000;
					break;
				default:
					break;
				}
			}else{
				tmp = (apmu_gc >> 6) & 0x3;
				switch (tmp) {
				case 0: /* PLL1/2 */
					gc_clk = pll1clk/2;
					break;
				case 1: /* PLL2/3 */
					gc_clk = pll2clk/3;
					break;
				case 2: /* PLL2 */
					gc_clk = pll2clk;
					break;
				case 3: /* PLL2/2 */
					gc_clk = pll2clk/2;
					break;
				default:
					break;
				}
			}
		}
	}

	return gc_clk;
}

struct clkops gc800_clk_ops = {
	.enable		= gc800_clk_enable,
	.disable	= gc800_clk_disable,
	.setrate	= gc800_clk_setrate,
	.getrate	= gc800_clk_getrate,
};

static void disp1_axi_clk_enable(struct clk *clk)
{
	u32 tmp = __raw_readl(clk->clk_rst);

	/* enable Display1 AXI clock */
	tmp |= (1<<3);
	__raw_writel(tmp, clk->clk_rst);

	/* release from reset */
	tmp |= 1;
	__raw_writel(tmp, clk->clk_rst);
}

static void disp1_axi_clk_disable(struct clk *clk)
{
	u32 tmp = __raw_readl(clk->clk_rst);

	/* reset display1 AXI clock */
	tmp &= ~1;
	__raw_writel(tmp, clk->clk_rst);

	/* disable display1 AXI clock */
	tmp &= ~(1<<3);
	__raw_writel(tmp, clk->clk_rst);
}

struct clkops disp1_axi_clk_ops = {
	.enable		= disp1_axi_clk_enable,
	.disable	= disp1_axi_clk_disable,
};
static struct clk clk_disp1_axi;

static void lcd_pn1_clk_enable(struct clk *clk)
{
	u32 tmp = __raw_readl(clk->clk_rst);

	/* enable Display1 peripheral clock */
	tmp |= (1<<4);
	__raw_writel(tmp, clk->clk_rst);

	/* enable DSI clock */
	tmp |= (1<<12) | (1<<5);
	__raw_writel(tmp, clk->clk_rst);

	/* release from reset */
	tmp |= 7;
	__raw_writel(tmp, clk->clk_rst);

	/* enable display1 AXI clock */
	clk_enable(&clk_disp1_axi);
}
static void lcd_pn1_clk_disable(struct clk *clk)
{
	u32 tmp;

	/* reset & disable axi clk first, otherwise panel may resume error */
	clk_disable(&clk_disp1_axi);

	tmp = __raw_readl(clk->clk_rst);
	/* disable DSI clock */
	tmp &= ~((1<<12) | (1<<5));
	__raw_writel(tmp, clk->clk_rst);

	/* disable Display1 peripheral clock */
	tmp &= ~(1<<4);
	__raw_writel(tmp, clk->clk_rst);
}

static void lcd_tv_clk_enable(struct clk *clk)
{
	u32 tmp = __raw_readl(clk->clk_rst);
	tmp |= (0x3 << 13);
	__raw_writel(tmp, clk->clk_rst);
}

static void lcd_tv_clk_disable(struct clk *clk)
{
	u32 tmp = __raw_readl(clk->clk_rst);
	tmp &= ~(0x3 << 13);
	__raw_writel(tmp, clk->clk_rst);
}

static int lcd_clk_setrate(struct clk *clk, unsigned long val)
{
	u32 tmp = __raw_readl(clk->clk_rst);
	u32 pll2clk = pll2_get_clk();

	tmp &= ~((0x1f << 15) | (0xf << 8) | (0x3 << 6) | (1<<20));
	tmp |= (0x1a << 15);

	switch (val) {
	case 260000000:
		/* enable PLL1/1.5 divider */
		__raw_writel(0x1, MPMU_PLL1_CTRL);

		/* DSP1clk = PLL1/1.5/2 = 533MHz */
		tmp |= (0x2 << 8) | (0x2 << 6 | 1 << 20);
		break;
	case 400000000:
		/* DSP1clk = PLL1/2 = 400MHz */
		tmp |= (0x2 << 8) | (0x0 << 6);
		break;
	case 520000000:
		/* DSP1clk = PLL2/x = 500MHz */
		pr_debug("%s pll2clk: %d\n\n\n", __func__, pll2clk);
		if (pll2clk > 900000000)
			tmp |= (0x2 << 8) | (0x2 << 6);
		else
			tmp |= (0x1 << 8) | (0x2 << 6);
		break;
	case 800000000:
		/* DSP1clk = PLL1 = 800MHz */
		tmp |= (0x1 << 8) | (0x0 << 6);
		break;

	default:
		printk("%s %d not supported\n", __func__, (int)val);
		return -1;
	}

	__raw_writel(tmp, clk->clk_rst);
	return 0;
}
static unsigned long lcd_clk_getrate(struct clk *clk)
{
	u32 lcdclk = __raw_readl(clk->clk_rst);
	u32 div = (lcdclk >> 8) & 0xf;
	u32 src = (lcdclk >> 6) & 0x3;
	u32 sel = (lcdclk >> 20) & 1;
	u32 pll1clk;
	u32 pll2clk;

	pll1clk = pll1_get_clk();
	pll2clk = pll2_get_clk();

	pr_debug("%s pll1clk %d pll2clk %d\n", __func__, pll1clk, pll2clk);
	if (sel) {
		switch (src) {
			case 2: /* PLL1/1.5 */
				lcdclk = pll1clk*2/3;
				break;
			case 3: /* PLL2/1.5 */
				lcdclk = pll2clk*2/3;
				break;
			case 0: /* USB PLL */
			case 1:
				lcdclk = 480000000;
				break;
			default:
				break;
			}
	} else {
		switch (src) {
			case 0: /* PLL1 */
				lcdclk = pll1clk;
				break;
			case 1: /* PLL1/16 */
				lcdclk = pll1clk/16;
				break;
			case 2: /* PLL2 */
				lcdclk = pll2clk;
				break;
			case 3: /* PLL2/2 */
				lcdclk = 66000000;
				break;
			default:
				break;
			}
	}

	lcdclk /= div;

	pr_debug("lcd clk get %d (clkrst 0x%x div %d src %d sel %d)\n",
		lcdclk, __raw_readl(clk->clk_rst), div, src, sel);
	return lcdclk;
}

struct clkops lcd_pn1_clk_ops = {
	.enable		= lcd_pn1_clk_enable,
	.disable	= lcd_pn1_clk_disable,
	.setrate	= lcd_clk_setrate,
	.getrate	= lcd_clk_getrate,
};

struct clkops lcd_tv_clk_ops = {
	.enable		= lcd_tv_clk_enable,
	.disable	= lcd_tv_clk_disable,
};

struct clkops lcd_pn2_clk_ops = {
	.enable		= lcd_pn1_clk_enable,
	.disable	= lcd_pn1_clk_disable,
	.setrate	= lcd_clk_setrate,
	.getrate	= lcd_clk_getrate,
};

static void rtc_clk_enable(struct clk *clk)
{
	uint32_t clk_rst;     

	clk_rst = APBC_APBCLK | APBC_FNCLK | APBC_FNCLKSEL(clk->fnclksel);
	clk_rst |= 1 << 7;
	__raw_writel(clk_rst, clk->clk_rst);
}

static void rtc_clk_disable(struct clk *clk)
{
	__raw_writel(0, clk->clk_rst);
}

struct clkops rtc_clk_ops = {
	.enable		= rtc_clk_enable,
	.disable	= rtc_clk_disable,
};

static void sdhc_clk_enable(struct clk *clk)
{
	uint32_t clk_rst;

	clk_rst  =  __raw_readl(clk->clk_rst);
	clk_rst |= clk->enable_val;
	__raw_writel(clk_rst, clk->clk_rst);
}

static void sdhc_clk_disable(struct clk *clk)
{
	uint32_t clk_rst;

	clk_rst  =  __raw_readl(clk->clk_rst);
	clk_rst &= ~clk->enable_val;
	__raw_writel(clk_rst, clk->clk_rst);
}

struct clkops sdhc_clk_ops = {
	.enable		= sdhc_clk_enable,
	.disable	= sdhc_clk_disable,
};

/*
 * clk rate = 199.33Mhz / 2 * denom / nume
 */
#define UART_NUMERATOR		(27)
#define UART_DENOMINATOR	(16)
#define UART_CLK_RATE 		(59060000)

static void uart_clk_enable(struct clk *clk)
{
	uint32_t clk_rst;
	uint32_t div;

	/* set numerator and denominator */
	div  = __raw_readl(MPMU_SUCCR);
	div &= ~(MPMU_SUCCR_UARTDIVN_MASK | MPMU_SUCCR_UARTDIVD_MASK);
	div |= (UART_NUMERATOR   << MPMU_SUCCR_UARTDIVN_SHIFT);
	div |= (UART_DENOMINATOR << MPMU_SUCCR_UARTDIVD_SHIFT);
	__raw_writel(div, MPMU_SUCCR);
	mdelay(1);

	clk_rst  = __raw_readl(clk->clk_rst);
	clk_rst |= APBC_FNCLK;
	__raw_writel(clk_rst, clk->clk_rst);
	mdelay(1);

	clk_rst |= APBC_APBCLK;
	__raw_writel(clk_rst, clk->clk_rst);
	mdelay(1);

	clk_rst &= ~(APBC_RST);
	__raw_writel(clk_rst, clk->clk_rst);

	/* choose programmable clk */
	clk_rst &= ~(APBC_FNCLKSEL(0x7));
	__raw_writel(clk_rst, clk->clk_rst);
}

static void uart_clk_disable(struct clk *clk)
{
	__raw_writel(0, clk->clk_rst);
	mdelay(1);
}

struct clkops uart_clk_ops = {
	.enable		= uart_clk_enable,
	.disable	= uart_clk_disable,
};

static struct clk clk_pwm1;
static void pwm2_clk_enable(struct clk *clk)
{
	uint32_t clk_rst;

	/* FIXME enable pwm1 clock because of dependency */
	clk_enable(&clk_pwm1);

	clk_rst = __raw_readl(clk->clk_rst);

	clk_rst &= ~(APBC_FNCLKSEL(0x7));	
	clk_rst |= APBC_FNCLK | APBC_FNCLKSEL(clk->fnclksel);
	__raw_writel(clk_rst, clk->clk_rst);
	mdelay(1);

	clk_rst |= APBC_APBCLK;
	__raw_writel(clk_rst, clk->clk_rst);
	mdelay(1);

	clk_rst &= ~(APBC_RST);
	__raw_writel(clk_rst, clk->clk_rst);
}

static void pwm2_clk_disable(struct clk *clk)
{
	uint32_t clk_rst;

	clk_rst = __raw_readl(clk->clk_rst);

	clk_rst &= APBC_APBCLK;
	__raw_writel(clk_rst, clk->clk_rst);
	mdelay(1);

	__raw_writel(0, clk->clk_rst);
	mdelay(1);

	/* FIXME disable pwm1 clock because of dependency */
	clk_disable(&clk_pwm1);
}

struct clkops pwm2_clk_ops = {
	.enable		= pwm2_clk_enable,
	.disable	= pwm2_clk_disable,
};

static struct clk clk_pwm3;
static void pwm4_clk_enable(struct clk *clk)
{
	uint32_t clk_rst;

	/* FIXME enable pwm1 clock because of dependency */
	clk_enable(&clk_pwm3);

	clk_rst = __raw_readl(clk->clk_rst);

	clk_rst &= ~(APBC_FNCLKSEL(0x7));	
	clk_rst |= APBC_FNCLK | APBC_FNCLKSEL(clk->fnclksel);
	__raw_writel(clk_rst, clk->clk_rst);
	mdelay(1);

	clk_rst |= APBC_APBCLK;
	__raw_writel(clk_rst, clk->clk_rst);
	mdelay(1);

	clk_rst &= ~(APBC_RST);
	__raw_writel(clk_rst, clk->clk_rst);
}

static void pwm4_clk_disable(struct clk *clk)
{
	uint32_t clk_rst;

	clk_rst = __raw_readl(clk->clk_rst);

	clk_rst &= APBC_APBCLK;
	__raw_writel(clk_rst, clk->clk_rst);
	mdelay(1);

	__raw_writel(0, clk->clk_rst);
	mdelay(1);

	/* FIXME disable pwm3 clock because of dependency */
	clk_disable(&clk_pwm3);
}

struct clkops pwm4_clk_ops = {
	.enable		= pwm4_clk_enable,
	.disable	= pwm4_clk_disable,
};


/* usb: u2o clock */
static void u2o_clk_enable(struct clk *clk)
{
	__raw_writel(0x1b, clk->clk_rst);
	return ;
}

static void u2o_clk_disable(struct clk *clk)
{
	unsigned int val;

	val = __raw_readl(clk->clk_rst);
	val &= ~(0x3 << 3);
	__raw_writel(val, clk->clk_rst);

	return ;
}

struct clkops u2o_clk_ops = {
	.enable		= u2o_clk_enable,
	.disable	= u2o_clk_disable,
};

/* usb: hsic clock */
static void hsic_clk_enable(struct clk *clk)
{
	uint32_t clk_rst;

	/* FIXME enable OTG PHY clock to feed HSIC */
	clk_enable(&clk_usb_phy);

	clk_rst  =  __raw_readl(clk->clk_rst);
	clk_rst |= 0x1b;
	__raw_writel(clk_rst, clk->clk_rst);

}

static void hsic_clk_disable(struct clk *clk)
{
	uint32_t clk_rst;

	clk_rst  =  __raw_readl(clk->clk_rst);
	clk_rst &= ~0x18;
	__raw_writel(clk_rst, clk->clk_rst);

	/* FIXME disable OTG PHY clock */
	clk_disable(&clk_usb_phy);

}

struct clkops hsic_clk_ops = {
	.enable		= hsic_clk_enable,
	.disable	= hsic_clk_disable,
};

static void ccic_dbg_clk_enable(struct clk *clk)
{
	u32 tmp = __raw_readl(clk->clk_rst);
	tmp |= (1<<25) | (1 <<27);
	__raw_writel(tmp, clk->clk_rst);
}

static void ccic_dbg_clk_disable(struct clk *clk)
{
	u32 tmp = __raw_readl(clk->clk_rst);
	tmp &= ~((1<<25) | (1 <<27));
	__raw_writel(tmp, clk->clk_rst);
}

struct clkops ccic_dbg_clk_ops = {
	.enable         = ccic_dbg_clk_enable,
	.disable        = ccic_dbg_clk_disable,
};

static void ccic_rst_clk_enable(struct clk *clk)
{
	__raw_writel(0x2e838, clk->clk_rst);
	__raw_writel(0x3e909, clk->clk_rst);
	__raw_writel(0x3eb39, clk->clk_rst);
	__raw_writel(0x3eb3f, clk->clk_rst);
}

static void ccic_rst_clk_disable(struct clk *clk)
{
	__raw_writel(0x3eb3f, clk->clk_rst);
	__raw_writel(0x3eb39, clk->clk_rst);
	__raw_writel(0x3e909, clk->clk_rst);
	__raw_writel(0x2e838, clk->clk_rst);
	__raw_writel(0x0, clk->clk_rst);
}

struct clkops ccic_rst_clk_ops = {
	.enable         = ccic_rst_clk_enable,
	.disable        = ccic_rst_clk_disable,
};

static int wtm_has_init = 0;

/* wtm clock */
static void wtm_clk_enable(struct clk *clk)
{
	unsigned int val = 0;
	unsigned int enable_flag = (APMU_AXICLK_EN | APMU_FNCLK_EN);

	val = __raw_readl(clk->clk_rst);
	if ((val & enable_flag) == enable_flag) {
		wtm_has_init = 1;
		printk("wtm has been enabled by secure core!\n");
		return ;
	}

	val  = APMU_AXICLK_EN;
	__raw_writel(val, clk->clk_rst);
	val |= APMU_AXIRST_DIS;
	__raw_writel(val, clk->clk_rst);
	val |= APMU_FNCLK_EN;
	__raw_writel(val, clk->clk_rst);
	val |= APMU_FNRST_DIS;
	__raw_writel(val, clk->clk_rst);

	return ;
}

static void wtm_clk_disable(struct clk *clk)
{
	unsigned int val;

	if (wtm_has_init)
		return;

	val = __raw_readl(clk->clk_rst);
	val &= ~(APMU_AXICLK_EN);
	val &= ~(APMU_FNCLK_EN);
	__raw_writel(val, clk->clk_rst);

	return ;
}

struct clkops wtm_clk_ops = {
	.enable		= wtm_clk_enable,
	.disable	= wtm_clk_disable,
};

/* audio clock */
static void audio_clk_enable(struct clk *clk)
{
	unsigned int val = 0;

	val  = __raw_readl(clk->clk_rst);
	val |= APMU_AUDIO_CTRL_PWR_UP;
	__raw_writel(val, clk->clk_rst);
	val |= APMU_AUDIO_CLK_ENA;
	__raw_writel(val, clk->clk_rst);
	val |= APMU_AUDIO_CTRL_ISO_DIS;
	__raw_writel(val, clk->clk_rst);
	val |= APMU_AUDIO_RST_DIS;
	__raw_writel(val, clk->clk_rst);

	return ;
}

static void audio_clk_disable(struct clk *clk)
{
	unsigned int val;

	val = __raw_readl(clk->clk_rst);
	val &= ~APMU_AUDIO_RST_DIS;
	__raw_writel(val, clk->clk_rst);
	udelay(10);
	val &= ~APMU_AUDIO_CTRL_ISO_DIS;
	__raw_writel(val, clk->clk_rst);
	udelay(10);
	val &= ~APMU_AUDIO_CLK_ENA;
	__raw_writel(val, clk->clk_rst);
	udelay(10);
	val &= ~APMU_AUDIO_CTRL_PWR_UP;
	__raw_writel(val, clk->clk_rst);
	udelay(10);

	return ;
}

struct clkops audio_clk_ops = {
	.enable	 = audio_clk_enable,
	.disable = audio_clk_disable,
};

/* APB peripheral clocks */
static APBC_CLK_OPS(uart1, MMP2_UART1, 1, UART_CLK_RATE, &uart_clk_ops);
static APBC_CLK_OPS(uart2, MMP2_UART2, 1, UART_CLK_RATE, &uart_clk_ops);
static APBC_CLK_OPS(uart3, MMP2_UART3, 1, UART_CLK_RATE, &uart_clk_ops);
static APBC_CLK_OPS(uart4, MMP2_UART4, 1, UART_CLK_RATE, &uart_clk_ops);
static APBC_CLK(twsi1, MMP2_TWSI1, 0, 26000000);
static APBC_CLK(twsi2, MMP2_TWSI2, 0, 26000000);
static APBC_CLK(twsi3, MMP2_TWSI3, 0, 26000000);
static APBC_CLK(twsi4, MMP2_TWSI4, 0, 26000000);
static APBC_CLK(twsi5, MMP2_TWSI5, 0, 26000000);
static APBC_CLK(twsi6, MMP2_TWSI6, 0, 26000000);
static APBC_CLK(keypad, MMP2_KPC, 0, 32768);
static APBC_CLK(thsens, MMP2_THSENS1, 0, 0);
static APBC_CLK_OPS(rtc, MMP2_RTC, 0, 32768, &rtc_clk_ops);
static APBC_CLK(pwm1, MMP2_PWM0, 0, 26000000);
//static APBC_CLK(pwm2, MMP2_PWM1, 0, 26000000);
static APBC_CLK_OPS(pwm2, MMP2_PWM1, 0, 26000000, &pwm2_clk_ops);
static APBC_CLK(pwm3, MMP2_PWM2, 0, 26000000);
//static APBC_CLK(pwm4, MMP2_PWM3, 0, 26000000);
static APBC_CLK_OPS(pwm4, MMP2_PWM3, 0, 26000000,&pwm4_clk_ops);

static APBC_CLK(w1,MMP2_ONEWIRE,0,26000000);

static PSEUDO_CLK(iscclk, 0, 0, 0);     /* pseudo clock for imm */
static APMU_CLK(nand, NAND, 0xbf, 100000000);
static APMU_CLK_OPS(sdh0, SDH0, 0x1b, 200000000, &sdhc_clk_ops);
static APMU_CLK_OPS(sdh1, SDH1, 0x1b, 200000000, &sdhc_clk_ops);
static APMU_CLK_OPS(sdh2, SDH2, 0x1b, 200000000, &sdhc_clk_ops);
static APMU_CLK_OPS(sdh3, SDH3, 0x1b, 200000000, &sdhc_clk_ops);
static APMU_CLK_OPS(gc, GC, 0, 0, &gc800_clk_ops);
static APMU_CLK_OPS(disp1_axi, LCD_CLK_RES_CTRL, 0, 0, &disp1_axi_clk_ops);
static APMU_CLK_OPS(lcd, LCD_CLK_RES_CTRL, 0, 0, &lcd_pn1_clk_ops);
static APMU_CLK_OPS(tv, LCD_CLK_RES_CTRL, 0, 0, &lcd_tv_clk_ops);
/* APMU: usb clock */
static APMU_CLK_OPS(u2o, USBOTG, 0x1b, 480000000, &u2o_clk_ops);
/* usb hsic1 clock */
static APMU_CLK_OPS(hsic1, USBHSIC1, 0x1b, 480000000, &hsic_clk_ops);
/* usb fsic clock */
static APMU_CLK(fsic, USBFSIC, 0x1b, 480000000);
static APMU_CLK_OPS(ccic_rst, CCIC_RST, 0, 312000000, &ccic_rst_clk_ops);
static APMU_CLK_OPS(ccic_dbg, CCIC_DBG, 0, 312000000, &ccic_dbg_clk_ops);
static APMU_CLK(ccic_gate, CCIC_GATE, 0xffff, 0);
/* wtm clock */
static APMU_CLK_OPS(wtm, GEU, 0, 0, &wtm_clk_ops);
/* audio clock */
static APMU_CLK_OPS(audio, AUDIO, 0, 0, &audio_clk_ops);

static void usb_phy_clk_enable(struct clk *clk)
{
#if defined(CONFIG_USB_EHCI_PXA_U2H)||defined(CONFIG_USB_PXA_U2O)
	unsigned u2o_phy = AXI_VIRT_BASE + (PXA168_U2O_PHYBASE & 0xffff);

	clk_enable(&clk_u2o);
	pxa_usb_phy_init(u2o_phy, USB_PHY_PXA910);
	clk_disable(&clk_u2o);
#endif
}

static void usb_phy_clk_disable(struct clk *clk)
{
#if defined(CONFIG_USB_EHCI_PXA_U2H)||defined(CONFIG_USB_PXA_U2O)
	unsigned u2o_phy = AXI_VIRT_BASE + (PXA168_U2O_PHYBASE & 0xffff);

	clk_enable(&clk_u2o);
	pxa_usb_phy_deinit(u2o_phy, USB_PHY_PXA910);
	clk_disable(&clk_u2o);
	
#endif
}

struct clkops usb_phy_clk_ops = {
	.enable		= &usb_phy_clk_enable,
	.disable	= &usb_phy_clk_disable,
};

static struct clk clk_usb_phy = {
	.ops		= &usb_phy_clk_ops,
};

static struct clk_lookup mmp2_clkregs[] = {
	INIT_CLKREG(&clk_uart1, "pxa2xx-uart.0", NULL),
	INIT_CLKREG(&clk_uart2, "pxa2xx-uart.1", NULL),
	INIT_CLKREG(&clk_uart3, "pxa2xx-uart.2", NULL),
	INIT_CLKREG(&clk_uart4, "pxa2xx-uart.3", NULL),
	INIT_CLKREG(&clk_twsi1, "pxa2xx-i2c.0", NULL),
	INIT_CLKREG(&clk_twsi2, "pxa2xx-i2c.1", NULL),
	INIT_CLKREG(&clk_twsi3, "pxa2xx-i2c.2", NULL),
	INIT_CLKREG(&clk_twsi4, "pxa2xx-i2c.3", NULL),
	INIT_CLKREG(&clk_twsi5, "pxa2xx-i2c.4", NULL),
	INIT_CLKREG(&clk_twsi6, "pxa2xx-i2c.5", NULL),
	INIT_CLKREG(&clk_nand, "pxa3xx-nand", NULL),
	INIT_CLKREG(&clk_iscclk,  NULL, "ISCCLK"),
	INIT_CLKREG(&clk_keypad, "pxa27x-keypad", NULL),
	INIT_CLKREG(&clk_thsens, "mmp2-thermal", NULL),
	INIT_CLKREG(&clk_rtc, "mmp-rtc", NULL),
	INIT_CLKREG(&clk_ccic_rst, NULL, "CCICRSTCLK"),
	INIT_CLKREG(&clk_ccic_dbg, NULL, "CCICDBGCLK"),
	INIT_CLKREG(&clk_ccic_gate, NULL, "CCICGATECLK"),
	INIT_CLKREG(&clk_sdh0, "sdhci-pxa.0", "PXA-SDHCLK"),
	INIT_CLKREG(&clk_sdh1, "sdhci-pxa.1", "PXA-SDHCLK"),
	INIT_CLKREG(&clk_sdh2, "sdhci-pxa.2", "PXA-SDHCLK"),
	INIT_CLKREG(&clk_sdh3, "sdhci-pxa.3", "PXA-SDHCLK"),
	INIT_CLKREG(&clk_gc, NULL, "GCCLK"),
	INIT_CLKREG(&clk_pwm1, "mmp2-pwm.0", NULL),
	INIT_CLKREG(&clk_pwm2, "mmp2-pwm.1", NULL),
	INIT_CLKREG(&clk_pwm3, "mmp2-pwm.2", NULL),
	INIT_CLKREG(&clk_pwm4, "mmp2-pwm.3", NULL),
	INIT_CLKREG(&clk_disp1_axi, NULL, "DISP1AXICLK"),
	INIT_CLKREG(&clk_lcd, "pxa168-fb.0", NULL),
	INIT_CLKREG(&clk_tv, "pxa168-fb.1", NULL),
	INIT_CLKREG(&clk_u2o, NULL, "USBCLK"),
	INIT_CLKREG(&clk_hsic1, NULL, "HSIC1CLK"),
	INIT_CLKREG(&clk_fsic, NULL, "FSICCLK"),
	INIT_CLKREG(&clk_usb_phy, NULL, "USBPHYCLK"),
	INIT_CLKREG(&clk_wtm, NULL, "mmp2-wtm"),
	INIT_CLKREG(&clk_audio, NULL, "mmp2-audio"),
	INIT_CLKREG(&clk_w1,"mmp2-w1","W1CLK"),
};

#define MCB_SLFST_SEL		0xD0000570
#define MCB_SLFST_CTRL2		0xD00005A0
void __init mmp2_init_mcb(void)
{
	void *mcb_slfst_sel = ioremap(MCB_SLFST_SEL, 4);
	void *mcb_slfst_ctrl2 = ioremap(MCB_SLFST_CTRL2, 4);

	/* select MCB1 */
	__raw_writel(0x1, mcb_slfst_sel);
	iounmap(mcb_slfst_sel);

	/* to reduce LCD underflow, priority enable stage 2,
	 * fast has higher priority then slow */
	__raw_writel(0xc0000, mcb_slfst_ctrl2);
	iounmap(mcb_slfst_ctrl2);
}

static int __init mmp2_init(void)
{
	if (cpu_is_mmp2()) {
#ifdef CONFIG_CACHE_TAUROS2
		tauros2_init();
#endif
		mfp_init_base(MFPR_VIRT_BASE);
		mfp_init_addr(mmp2_addr_map);
		pxa_init_dma(IRQ_MMP2_DMA_RIQ, 16);
		mmp2_init_dma();
		mmp2_init_mcb();
		clkdev_add_table(ARRAY_AND_SIZE(mmp2_clkregs));
	}

	return 0;
}
postcore_initcall(mmp2_init);

static void __init mmp2_timer_init(void)
{
	uint32_t clk_rst;

	/* this is early, we have to initialize the CCU registers by
	 * ourselves instead of using clk_* API. Clock rate is defined
	 * by APBC_TIMERS_FNCLKSEL and enabled free-running
	 */
	__raw_writel(APBC_APBCLK | APBC_RST, APBC_MMP2_TIMERS);

	/* 6.5MHz, bus/functional clock enabled, release reset */
	clk_rst = APBC_APBCLK | APBC_FNCLK | APBC_FNCLKSEL(1);
	__raw_writel(clk_rst, APBC_MMP2_TIMERS);

	timer_init(IRQ_MMP2_TIMER1, IRQ_MMP2_TIMER2);
}

struct sys_timer mmp2_timer = {
	.init	= mmp2_timer_init,
};

/* on-chip devices */
MMP2_DEVICE(uart1, "pxa2xx-uart", 0, UART1, 0xd4030000, 0x30, 4, 5);
MMP2_DEVICE(uart2, "pxa2xx-uart", 1, UART2, 0xd4017000, 0x30, 20, 21);
MMP2_DEVICE(uart3, "pxa2xx-uart", 2, UART3, 0xd4018000, 0x30, 22, 23);
MMP2_DEVICE(uart4, "pxa2xx-uart", 3, UART4, 0xd4016000, 0x30, 18, 19);
MMP2_DEVICE(twsi1, "pxa2xx-i2c", 0, TWSI1, 0xd4011000, 0x70);
MMP2_DEVICE(twsi2, "pxa2xx-i2c", 1, TWSI2, 0xd4031000, 0x70);
MMP2_DEVICE(twsi3, "pxa2xx-i2c", 2, TWSI3, 0xd4032000, 0x70);
MMP2_DEVICE(twsi4, "pxa2xx-i2c", 3, TWSI4, 0xd4033000, 0x70);
MMP2_DEVICE(twsi5, "pxa2xx-i2c", 4, TWSI5, 0xd4033800, 0x70);
MMP2_DEVICE(twsi6, "pxa2xx-i2c", 5, TWSI6, 0xd4034000, 0x70);
MMP2_DEVICE(nand, "pxa3xx-nand", -1, NAND, 0xd4283000, 0x100, 28, 29);
MMP2_DEVICE(hdmi, "mmp2-hdmi", -1, HDMI, 0xd420b000, 0x1fff);
MMP2_DEVICE(keypad, "pxa27x-keypad", -1, KPC, 0xd4012000, 0x4c);
MMP2_DEVICE(camera, "pxa688-camera", 0, CI, 0xd420a000, 0x2ff);
MMP2_DEVICE(camera2, "pxa688-camera", 1, CI2, 0xd420a800, 0x2ff);
MMP2_DEVICE(sspa1, "mmp2-sspa", 0, SSPA1, 0xd42a0c00, 0xb0, ADMA1_CH_1, ADMA1_CH_0);
MMP2_DEVICE(sspa2, "mmp2-sspa", 1, SSPA2, 0xd42a0d00, 0xb0, ADMA2_CH_1, ADMA2_CH_0);
MMP2_DEVICE(thsens, "mmp2-thermal", -1, THERMAL, 0xd4013200, 0x20);
MMP2_DEVICE(vmeta, "mmp2-vmeta", -1, VMETA, 0xf0400000, 0x400000);
MMP2_DEVICE(sdh0, "sdhci-pxa", 0, MMC, 0xd4280000, 0x120);
MMP2_DEVICE(sdh1, "sdhci-pxa", 1, MMC2, 0xd4280800, 0x120);
MMP2_DEVICE(sdh2, "sdhci-pxa", 2, MMC3, 0xd4281000, 0x120);
MMP2_DEVICE(sdh3, "sdhci-pxa", 3, MMC4, 0xd4281800, 0x120);
MMP2_DEVICE(fuse, "mmp2-fuse", -1, NONE, 0xd4290000, 0x3100);
MMP2_DEVICE(pwm1, "mmp2-pwm", 0, NONE, 0xd401a000, 0x10);
MMP2_DEVICE(pwm2, "mmp2-pwm", 1, NONE, 0xd401a400, 0x10);
MMP2_DEVICE(pwm3, "mmp2-pwm", 2, NONE, 0xd401a800, 0x10);
MMP2_DEVICE(pwm4, "mmp2-pwm", 3, NONE, 0xd401ac00, 0x10);
MMP2_DEVICE(zsp, "mmp-zsp", -1, PZIPC, 0xd42a1400, 0x1f);
MMP2_DEVICE(fb, "pxa168-fb", 0, LCD, 0xd420b000, 0x500);
MMP2_DEVICE(fb_tv, "pxa168-fb", 1, LCD, 0xd420b000, 0x500);
MMP2_DEVICE(fb_pn2, "pxa168-fb", 2, LCD, 0xd420b000, 0x500);
MMP2_DEVICE(fb_ovly, "pxa168fb_ovly", 0, LCD, 0xd420b000, 0x500);
MMP2_DEVICE(fb_tv_ovly, "pxa168fb_ovly", 1, LCD, 0xd420b000, 0x500);
MMP2_DEVICE(fb_pn2_ovly, "pxa168fb_ovly", 2, LCD, 0xd420b000, 0x500);
MMP2_DEVICE(w1, "mmp2-w1", -1, ONEWIRE, 0xd4011800, 0x14);

static struct resource mmp2_resource_imm[] = {
    [0] = {
		.name   = "phy_sram",
		.start  = 0xd1020000,
		.end    = 0xd1020000 + SZ_64K + SZ_32K- 1,
		.flags  = IORESOURCE_MEM,
	},
    [1] = {
		.name   = "imm_sram",
		.start  = 0xd1020000,
		.end    = 0xd1020000 + SZ_64K + SZ_32K- 1,
		.flags  = IORESOURCE_MEM,
	},
};

struct platform_device mmp2_device_imm = {
	.name           = "pxa3xx-imm",
	.id             = -1,
	.num_resources  = ARRAY_SIZE(mmp2_resource_imm),
	.resource       = mmp2_resource_imm,
};

static struct resource mmp2_resource_rtc[] = {
	[0] = {
		.start  = 0xd4010000,
		.end    = 0xD40100ff,
		.flags  = IORESOURCE_MEM,
	},
	[1] = {
		.start  = IRQ_MMP2_RTC,
		.end    = IRQ_MMP2_RTC,
		.flags  = IORESOURCE_IRQ,
		.name   = "RTC_1HZ",
	},

	[2] = {
		.start  = IRQ_MMP2_RTC_ALARM,
		.end    = IRQ_MMP2_RTC_ALARM,
		.flags  = IORESOURCE_IRQ,
		.name   = "RTC_ALARM",
	},

};

struct platform_device mmp2_device_rtc = {
       .name           = "mmp-rtc",
       .id             = -1,
       .resource       = mmp2_resource_rtc,
       .num_resources  = ARRAY_SIZE(mmp2_resource_rtc),
};

static struct resource mmp2_resource_audiosram[] = {
    [0] = {
		.name   = "audio_sram",
		.start  = 0xe0000000,
		.end    = 0xe0000000 + SZ_16K - 1,
		.flags  = IORESOURCE_MEM,
	},
};

struct platform_device mmp2_device_audiosram = {
	.name           = "mmp2-audiosram",
	.id             = -1,
	.num_resources  = ARRAY_SIZE(mmp2_resource_audiosram),
	.resource       = mmp2_resource_audiosram,
};

static struct resource mmp2_resource_freq[] = {
	[0] = {
		.name   = "pmum_regs",
		.start	= 0xd4050000,
		.end	= 0xd4051050,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.name   = "pmua_regs",
		.start	= 0xd4282800,
		.end	= 0xd4282920,
		.flags	= IORESOURCE_MEM,
	},
};

struct platform_device mmp2_device_freq = {
	.name		= "mmp2-freq",
	.id		= 0,
	.num_resources	= ARRAY_SIZE(mmp2_resource_freq),
	.resource	= mmp2_resource_freq,
};


#if defined(CONFIG_USB_PXA_U2O)||defined(CONFIG_USB_OTG)
static struct pxa_usb_plat_info jasper_u2o_info = {
	.phy_init	= mmp2_init_usb_phy,
	.phy_deinit	= mmp2_deinit_usb_phy,
	.set_power = u2o_phy_set_power,
	.is_otg		= 1,
	// Ellie: do not set rely_on_vbus for our board,it will prevent stopping
	// gadget when the cable is unplugged
	//.rely_on_vbus	= 1,
};

static struct resource u2o_resources[] = {
	/* reg base */
	[0] = {
		.start	= PXA168_U2O_REGBASE,
		.end	= PXA168_U2O_REGBASE + USB_REG_RANGE,
		.flags	= IORESOURCE_MEM,
		.name	= "u2o",
	},
	/* phybase */
	[1] = {
		.start	= PXA168_U2O_PHYBASE,
		.end	= PXA168_U2O_PHYBASE + USB_PHY_RANGE,
		.flags	= IORESOURCE_MEM,
		.name	= "u2ophy",
	},
	[2] = {
		.start	= IRQ_MMP2_USB_OTG,
		.end	= IRQ_MMP2_USB_OTG,
		.flags	= IORESOURCE_IRQ,
	},
};

/* usb gadget platform device data */
struct platform_device u2o_device = {
	.name		= "pxa-u2o",
	.id		= -1,
	.dev		= {
		.coherent_dma_mask	= DMA_BIT_MASK(32),
		.platform_data		= &jasper_u2o_info,
	},

	.num_resources	= ARRAY_SIZE(u2o_resources),
	.resource	= u2o_resources,
};
#endif

#ifdef CONFIG_USB_OTG
struct platform_device otg_device = {
	.name		= "pxa-otg",
	.id		= -1,
	.dev		= {
		.coherent_dma_mask	= DMA_BIT_MASK(32),
		.platform_data		= &jasper_u2o_info,
	},

	.num_resources	= ARRAY_SIZE(u2o_resources),
	.resource	= u2o_resources,
};

static struct resource ehci_u2o_resources[] = {
	/* reg base */
	[0] = {
		.start	= PXA168_U2O_REGBASE,
		.end	= PXA168_U2O_REGBASE + USB_REG_RANGE,
		.flags	= IORESOURCE_MEM,
		.name	= "u2o",
	},
	/* phybase */
	[1] = {
		.start	= PXA168_U2O_PHYBASE,
		.end	= PXA168_U2O_PHYBASE + USB_PHY_RANGE,
		.flags	= IORESOURCE_MEM,
		.name	= "u2ophy",
	},
	[2] = {
		.start	= IRQ_MMP2_USB_OTG,
		.end	= IRQ_MMP2_USB_OTG,
		.flags	= IORESOURCE_IRQ,
	},
};

static u64 ehci_hcd_pxa_dmamask = DMA_BIT_MASK(32);
struct platform_device ehci_u2o_device = {
	.name		= "pxau2o-ehci",
	.id		= -1,
	.dev		= {
		.dma_mask = &ehci_hcd_pxa_dmamask,
		.coherent_dma_mask	= DMA_BIT_MASK(32),
		.platform_data		= &jasper_u2o_info,
	},

	.num_resources	= ARRAY_SIZE(u2o_resources),
	.resource	= ehci_u2o_resources,
};
#endif

#ifdef CONFIG_USB_EHCI_PXA_U2H
static struct resource ehci_sph1_resources[] = {
	/* reg base */
	[0] = {
		.start	= PXA688_SPH1_REGBASE,
		.end	= PXA688_SPH1_REGBASE + USB_REG_RANGE,
		.flags	= IORESOURCE_MEM,
		.name	= "u2h",
	},
	/* phybase */
	[1] = {
		.start	= PXA688_HSIC1_PHYBASE,
		.end	= PXA688_HSIC1_PHYBASE + USB_PHY_RANGE,
		.flags	= IORESOURCE_MEM,
		.name	= "u2hphy",
	},
	[2] = {
		.start	= IRQ_MMP2_USB_HS1,
		.end	= IRQ_MMP2_USB_HS1,
		.flags	= IORESOURCE_IRQ,
	},
};

struct platform_device ehci_sph1_device = {
	.name		= "pxau2h-ehci",
	.id		= -1,
	.dev		= {
		.dma_mask = &ehci_hcd_pxa_dmamask,
		.coherent_dma_mask	= DMA_BIT_MASK(32),
	},

	.num_resources	= ARRAY_SIZE(ehci_sph1_resources),
	.resource	= ehci_sph1_resources,
};

static struct resource ehci_sph3_resources[] = {
	/* reg base */
	[0] = {
		.start  = PXA688_SPH3_REGBASE,
		.end    = PXA688_SPH3_REGBASE + USB_REG_RANGE,
		.flags  = IORESOURCE_MEM,
		.name   = "u2h",
	},
	/* phybase */
	[1] = {
		.start  = PXA688_FSIC_PHYBASE,
		.end    = PXA688_FSIC_PHYBASE + USB_PHY_RANGE,
		.flags  = IORESOURCE_MEM,
		.name   = "u2hphy",
	},
	[2] = {
		.start  = IRQ_MMP2_USB_FS,
		.end    = IRQ_MMP2_USB_FS,
		.flags  = IORESOURCE_IRQ,
	},
};

struct platform_device ehci_sph3_device = {
	.name		= "pxau2h-ehci",
	.id		= -1,
	.dev		= {
		.dma_mask = &ehci_hcd_pxa_dmamask,
		.coherent_dma_mask	= DMA_BIT_MASK(32),
	},

	.num_resources	= ARRAY_SIZE(ehci_sph3_resources),
	.resource	= ehci_sph3_resources,
};

#endif

#define MMP2_SD_FIFO_PARAM		0x104
#define MMP2_DIS_PAD_SD_CLK_GATE	0x400

#define MMP2_SD_CLOCK_AND_BURST_SIZE_SETUP		0x10A
#define MMP2_SDCLK_SEL	0x100
#define MMP2_SDCLK_DELAY_SHIFT	9
#define MMP2_SDCLK_DELAY_MASK	0x1f

void mmp2_init_sdh(struct sdhci_host *host, struct sdhci_pxa_platdata *pdata)
{
	/*
	 * tune timing of read data/command when crc error happen
	 * no performance impact
	 */
	if (pdata && 0 != pdata->clk_delay_cycles) {
		u16 tmp;

		tmp = readw(host->ioaddr + MMP2_SD_CLOCK_AND_BURST_SIZE_SETUP);
		tmp &= ~(MMP2_SDCLK_DELAY_MASK << MMP2_SDCLK_DELAY_SHIFT);
		tmp |= (pdata->clk_delay_cycles & MMP2_SDCLK_DELAY_MASK)
					<< MMP2_SDCLK_DELAY_SHIFT;
		tmp |= MMP2_SDCLK_SEL;
		writew(tmp, host->ioaddr + MMP2_SD_CLOCK_AND_BURST_SIZE_SETUP);
	}

	/*
	 * disable clock gating per some cards requirement
	 */

	if (pdata && pdata->flags & PXA_FLAG_DISABLE_CLOCK_GATING) {
		u32 tmp = 0;

		tmp = readl(host->ioaddr + MMP2_SD_FIFO_PARAM);
		tmp |= MMP2_DIS_PAD_SD_CLK_GATE;
		writel(tmp, host->ioaddr + MMP2_SD_FIFO_PARAM);
	}
}

/* mmp2 vmeta dvfm, vop:
 * VGA:		1~7
 * 720P:	8~13
 * 1080P:	14
 */
int mmp2_vmeta_increase_core_freq(const struct vmeta_instance *vi, const int step)
{
	int vop = vi->vop + step;
	if (vop > VMETA_OP_MIN && vop < VMETA_OP_MAX)
		return vop;
	else
		return -1;
}

int mmp2_vmeta_decrease_core_freq(const struct vmeta_instance *vi, const int step)
{
	int vop = vi->vop - step;
	if (vop > VMETA_OP_MIN && vop < VMETA_OP_MAX)
		return vop;
	else
		return -1;
}

#ifdef CONFIG_DVFM
int mmp2_vmeta_init_dvfm_constraint(struct vmeta_instance *vi, int idx)
{
	return 0;
}

int mmp2_vmeta_clean_dvfm_constraint(struct vmeta_instance *vi, int idx)
{
	return 0;
}

/* Vmeta ops:
 Resolution <= VGA			-- 1~7	200M:"Low MIPS"
 VGA < Resolution <=720p		-- 8~13	400M:"Video/Graphics M MIPS"
 Resolution > 720p			-- 14	800M:"Video/Graphics H MIPS"
 */
int mmp2_vmeta_set_dvfm_constraint(struct vmeta_instance *vi, int idx)
{
	int vop = vi->vop;
	if (vop < VMETA_OP_MIN || vop > VMETA_OP_MAX) {
		printk(KERN_DEBUG "unsupport vmeta vop=%d\n", vi->vop);
		vop = VMETA_OP_1080P_MAX;
	}
	
	if (1 <= atomic_read(&vi->mult_usrs_flag))
		vop = VMETA_OP_1080P_MAX;

	if (vop == vi->vop_real)
		return 0;

	if (vi->power_constraint == 0) {
		/* disable low power mode */
		dvfm_disable_op_name("apps_idle", idx);
		dvfm_disable_op_name("apps_sleep", idx);
		dvfm_disable_op_name("chip_sleep", idx);
		dvfm_disable_op_name("sys_sleep", idx);
	}

	if (vop >= VMETA_OP_VGA && vop <= VMETA_OP_VGA_MAX) {
		printk(KERN_DEBUG "VGA!!!\n");
		dvfm_disable_op_name("Ultra Low MIPS", idx);
		dvfm_disable_op_name("Low MIPS", idx);
		dvfm_enable_op_name("Video/Graphics M MIPS", idx);
	} else if (vop >= VMETA_OP_720P && vop <= VMETA_OP_720P_MAX) {
		printk(KERN_DEBUG "720P!!!\n");
		dvfm_disable_op_name("Ultra Low MIPS", idx);
		dvfm_disable_op_name("Low MIPS", idx);
		dvfm_enable_op_name("Video/Graphics M MIPS", idx);
	} else { /* 1080p and default ops */
		printk(KERN_DEBUG "1080P!!!\n");
		dvfm_disable_op_name("Ultra Low MIPS", idx);
		dvfm_disable_op_name("Low MIPS", idx);
		dvfm_disable_op_name("Video/Graphics M MIPS", idx);
	}

	vi->power_constraint = 1;
	vi->vop_real = vop;
	printk(KERN_DEBUG "set dvfm vop_real=%d\n", vi->vop_real);
	return 0;
}

int mmp2_vmeta_unset_dvfm_constraint(struct vmeta_instance *vi, int idx)
{
	int vop = vi->vop_real;

	if (vi->power_constraint == 1) {
		/* enable low power mode */
		dvfm_enable_op_name("apps_idle", idx);
		dvfm_enable_op_name("apps_sleep", idx);
		dvfm_enable_op_name("chip_sleep", idx);
		dvfm_enable_op_name("sys_sleep", idx);
	}

	if (vop >= VMETA_OP_VGA && vop <= VMETA_OP_VGA_MAX) {
		dvfm_enable_op_name("Ultra Low MIPS", idx);
		dvfm_enable_op_name("Low MIPS", idx);
	} else if (vop >= VMETA_OP_720P && vop <= VMETA_OP_720P_MAX) {
		dvfm_enable_op_name("Ultra Low MIPS", idx);
		dvfm_enable_op_name("Low MIPS", idx);
	} else { /* 1080p and default ops */
		dvfm_enable_op_name("Ultra Low MIPS", idx);
		dvfm_enable_op_name("Low MIPS", idx);
		dvfm_enable_op_name("Video/Graphics M MIPS", idx);
	}

	vi->power_constraint = 0;
	vi->vop_real = VMETA_OP_INVALID;
	printk(KERN_DEBUG "unset dvfm vop_real=%d\n", vi->vop_real);
	return 0;
}
#else
int mmp2_vmeta_init_dvfm_constraint(struct vmeta_instance *vi, int idx) {}
int mmp2_vmeta_clean_dvfm_constraint(struct vmeta_instance *vi, int idx) {}
int mmp2_vmeta_set_dvfm_constraint(struct vmeta_instance *vi, int idx) {}
int mmp2_vmeta_unset_dvfm_constraint(struct vmeta_instance *vi, int idx) {}
#endif
