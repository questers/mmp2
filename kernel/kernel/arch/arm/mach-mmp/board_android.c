#include <linux/reboot.h>
#include <linux/etherdevice.h>
#include <linux/usb/android_composite.h>
#undef BOOTLOADER_MISC_DISK
#define BOOTLOADER_MISC_DISK
#ifdef BOOTLOADER_MISC_DISK
#include <linux/blkdev.h>
#include <linux/genhd.h>
#endif
//
//Android
//
//
/*
;Google Nexus One
%SingleAdbInterface%        = USB_Install, USB\VID_18D1&PID_0D02
%CompositeAdbInterface%     = USB_Install, USB\VID_18D1&PID_0D02&MI_01
%SingleAdbInterface%        = USB_Install, USB\VID_18D1&PID_4E11
%CompositeAdbInterface%     = USB_Install, USB\VID_18D1&PID_4E12&MI_01

;Google Nexus S
%SingleAdbInterface%        = USB_Install, USB\VID_18D1&PID_4E21
%CompositeAdbInterface%     = USB_Install, USB\VID_18D1&PID_4E22&MI_01
%SingleAdbInterface%        = USB_Install, USB\VID_18D1&PID_4E23
%CompositeAdbInterface%     = USB_Install, USB\VID_18D1&PID_4E24&MI_01

;Google Nexus 7
%SingleBootLoaderInterface% = USB_Install, USB\VID_18D1&PID_4E40
%SingleAdbInterface%        = USB_Install, USB\VID_18D1&PID_4E41
%CompositeAdbInterface%     = USB_Install, USB\VID_18D1&PID_4E42
%CompositeAdbInterface%     = USB_Install, USB\VID_18D1&PID_4E42&MI_01
%CompositeAdbInterface%     = USB_Install, USB\VID_18D1&PID_4E44&MI_01

;Google Nexus Q
%SingleBootLoaderInterface% = USB_Install, USB\VID_18D1&PID_2C10
%SingleAdbInterface%        = USB_Install, USB\VID_18D1&PID_2C11

;Google Nexus (generic)
%SingleBootLoaderInterface% = USB_Install, USB\VID_18D1&PID_4EE0
%SingleAdbInterface%        = USB_Install, USB\VID_18D1&PID_4EE1
%CompositeAdbInterface%     = USB_Install, USB\VID_18D1&PID_4EE2
%CompositeAdbInterface%     = USB_Install, USB\VID_18D1&PID_4EE2&MI_01
%SingleAdbInterface%        = USB_Install, USB\VID_18D1&PID_4EE3
%CompositeAdbInterface%     = USB_Install, USB\VID_18D1&PID_4EE4&MI_01
%SingleAdbInterface%        = USB_Install, USB\VID_18D1&PID_4EE5
%CompositeAdbInterface%     = USB_Install, USB\VID_18D1&PID_4EE6&MI_01
*/
//
//
//We just borrow the vid/pid from HTC,and the RNDIS,RNDIS_ADB products are dummy,no 
//more information from HTC to support RNDIS gadget
//
//
//!!!RNDIS can not coexist with other usb functions!!! Windows' limitation
//

#ifndef ANDROID_PRODUCT
#define ANDROID_PRODUCT "Android"
#endif

#ifndef ANDROID_MANUFACTURER
#define ANDROID_MANUFACTURER "QUESTERS"
#endif

#ifndef ANDROID_USB_VENDOR_ID
#define ANDROID_USB_VENDOR_ID 				0x18D1
#endif

#ifndef ANDROID_USB_PRODUCT_ID
#define ANDROID_USB_PRODUCT_ID 				0x4E11
#endif

#ifndef ANDROID_USB_ADB_PRODUCT_ID
#define ANDROID_USB_ADB_PRODUCT_ID 			0x4E12
#endif

#ifndef ANDROID_USB_RNDIS_PRODUCT_ID
#define ANDROID_USB_RNDIS_PRODUCT_ID 		0x4E13
#endif

#ifndef ANDROID_USB_RNDIS_ADB_PRODUCT_ID
#define ANDROID_USB_RNDIS_ADB_PRODUCT_ID 	0x4E14
#endif

char android_serialno[] = ANDROID_PRODUCT"-0123456789ABCDEF";	/* 16 chars */
#ifdef CONFIG_USB_ANDROID
static char *usb_functions_ums[] = {
	"usb_mass_storage",
};

static char *usb_functions_ums_adb[] = {
	"usb_mass_storage",
	"adb",
};
static char *usb_functions_rndis[] = {
	"rndis",
};
static char *usb_functions_rndis_adb[] = {
	"rndis",
	"adb",
};
static char *usb_functions_all[] = {
	"usb_mass_storage",
	"adb",
#ifdef CONFIG_USB_ANDROID_RNDIS
	"rndis",
#endif
};

static struct android_usb_product usb_products[] = {
	{
		.product_id	= ANDROID_USB_PRODUCT_ID,
		.num_functions	= ARRAY_SIZE(usb_functions_ums),
		.functions	= usb_functions_ums,
	},
	{
		.product_id	= ANDROID_USB_ADB_PRODUCT_ID,
		.num_functions	= ARRAY_SIZE(usb_functions_ums_adb),
		.functions	= usb_functions_ums_adb,
	},
	{
		.product_id	= ANDROID_USB_RNDIS_PRODUCT_ID,
		.num_functions	= ARRAY_SIZE(usb_functions_rndis),
		.functions	= usb_functions_rndis,
	},
	{
		.product_id	= ANDROID_USB_RNDIS_ADB_PRODUCT_ID,
		.num_functions	= ARRAY_SIZE(usb_functions_rndis_adb),
		.functions	= usb_functions_rndis_adb,
	},
};
static struct android_usb_platform_data android_usb_platform_data = {
		.product_id 	= ANDROID_USB_ADB_PRODUCT_ID,//ANDROID_USB_PRODUCT_ID,
		.vendor_id		= ANDROID_USB_VENDOR_ID,
		.product_name	= ANDROID_PRODUCT,
		.manufacturer_name = ANDROID_MANUFACTURER,
		.num_products   = ARRAY_SIZE(usb_products),
		.products		= usb_products,
		.num_functions	= ARRAY_SIZE(usb_functions_all),
		.functions		= usb_functions_all,
		.serial_number	= android_serialno,
 };

static struct platform_device android_usb = {
		.name	= "android_usb",
		.id 			= -1,
		.dev			= {
				.platform_data = &android_usb_platform_data,
	},
};

#ifdef CONFIG_USB_ANDROID_MASS_STORAGE
static struct usb_mass_storage_platform_data usb_mass_storage_data = {
	.vendor = ANDROID_MANUFACTURER,
	.product= ANDROID_PRODUCT,
	.release= 1,
	.nluns 	= 2
};

static struct platform_device android_usb_mass_storage = {
	.name = "usb_mass_storage",
	.dev  = {
		.platform_data = &usb_mass_storage_data,
	},
};
#endif
#ifdef CONFIG_USB_ANDROID_RNDIS
static struct usb_ether_platform_data rndis_pdata = {
	.vendorID	= ANDROID_USB_PRODUCT_ID,
	.vendorDescr	= ANDROID_MANUFACTURER,
};
static struct platform_device rndis_device = {
	.name	= "rndis",
	.id	= -1,
	.dev	= {
		.platform_data = &rndis_pdata,
	},
};
#endif

#endif

//hard coding for cpu backup registers
#define REBOOT_REASON_REGISTER 0xfe010024
static int reboot_call(struct notifier_block *this, unsigned long code, void *_cmd)
{
	if((code == SYS_RESTART) && _cmd) 
	{	char *cmd = _cmd;		
		volatile uint32_t restart_reason;
		//printk("reboot command %s\n",cmd);
		if (!strcmp(cmd, "bootloader")) {		restart_reason = 0x77665500;		} 
		else if (!strcmp(cmd, "recovery")) {	restart_reason = 0x77665502;		} 
		else if (!strcmp(cmd, "autoupdate")) {	restart_reason = 0x77665503;		} 		
		else if (!strcmp(cmd, "eraseflash")) {	restart_reason = 0x776655EF;		} 
		else if (!strncmp(cmd, "oem-", 4)) 
		{			
			unsigned code = simple_strtoul(cmd + 4, 0, 16) & 0xff;			
			restart_reason = 0x6f656d00 | code;		
		} 
		else if (!strcmp(cmd, "force-hard")) 
		{
			restart_reason = 0x776655AA;		
		}
		else 
		{			
			restart_reason = 0x77665501;		
		}

		__raw_writel(restart_reason,REBOOT_REASON_REGISTER);
        #ifdef BOOTLOADER_MISC_DISK
		#ifndef CONFIG_BLOCK
		#error CONFIG_BLOCK is required for building board_android.c
		#endif
		//patch for hardware reset 
		{
			struct block_device* bdev;
			bdev = lookup_bdev("/dev/block/mmcblk0p7");
			if(IS_ERR(bdev))
			{
				printk("%s block device\n","not found");				
			}
			else
			{
				int partno;
				unsigned char* buffer;
				struct gendisk *disk;
				Sector sect;
				disk = get_gendisk(bdev->bd_dev, &partno);
				blkdev_get(bdev,FMODE_READ|FMODE_WRITE);					
				buffer = read_dev_sector(bdev,0,&sect);
				if(buffer)
				{
					if (!strcmp(cmd, "bootloader"))
					{
						sprintf(buffer,"%s","boot-loader");
					}
					else if(!strcmp(cmd, "recovery"))
					{
						sprintf(buffer,"%s","boot-recovery");//keep consistent with recovery system						
					}
					else
					{
						sprintf(buffer,"%s",(char*)_cmd);
					}
					set_page_dirty(sect.v);
					wait_on_page_writeback(sect.v);			
					put_dev_sector(sect);
				}
				blkdev_put(bdev,FMODE_READ|FMODE_WRITE);
			}
			
		}
		#endif
		
	}
	return NOTIFY_DONE;
}

static struct notifier_block reboot_notifier =
{
	.notifier_call = reboot_call,
};

static int __init board_android_init(void)
{
#ifdef CONFIG_USB_ANDROID

#ifdef CONFIG_USB_ANDROID_RNDIS
	random_ether_addr(rndis_pdata.ethaddr);
	platform_device_register(&rndis_device);
#endif
#ifdef CONFIG_USB_ANDROID_MASS_STORAGE
	platform_device_register(&android_usb_mass_storage);
#endif
	platform_device_register(&android_usb);

#endif
	register_reboot_notifier(&reboot_notifier);	

	return 0;
}

/* 
 * we put it in late_initcall so at this time
 * def_serialno[] is filled by w1. -guang
 */
late_initcall(board_android_init);

