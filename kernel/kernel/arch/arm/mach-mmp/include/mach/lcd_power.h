#if !defined(_ASM_ARCH_LCD_POWER_)
#define _ASM_ARCH_LCD_POWER_

struct lcd_power_platform_data {
	int lcd_power;
	int lcd_reset;
	int spi_sdi;
	int spi_sdo;
	int spi_cs;
	int spi_clk;
};

#endif /* _ASM_ARCH_LCD_POWER_ */