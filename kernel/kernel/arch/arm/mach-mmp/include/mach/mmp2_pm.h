/*
 * PXA910 Power Management Routines
 *
 * This software program is licensed subject to the GNU General Public License
 * (GPL).Version 2,June 1991, available at http://www.fsf.org/copyleft/gpl.html
 *
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 */

#ifndef __MMP2_PM_H__
#define __MMP2_PM_H__

extern void *vaddr_base;
extern void mmp2_cpu_do_idle(void);
extern void mmp2_pm_enter_lowpower_mode(int state);
extern void *copy_lp_to_sram(void *vaddr);
extern unsigned int jump_to_lp_sram(void *addr, void *stack, void *va_dmcu);
extern void do_lp;
extern void do_lp_end;

#endif
