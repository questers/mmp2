#ifndef __ASM_MACH_MMP2_H
#define __ASM_MACH_MMP2_H

#include <linux/i2c.h>
#include <plat/i2c.h>
#include <plat/pxa3xx_nand.h>
#include <plat/pxa27x_keypad.h>
#include <mach/camera.h>
#include <plat/sdhci.h>
#include <mach/devices.h>
#include <mach/cputype.h>
#include <mach/pxa168fb.h>
#include <mach/uio_hdmi.h>
#include <mach/soc_vmeta.h>
#include <linux/mmp2_w1.h>

extern struct pxa_device_desc mmp2_device_uart1;
extern struct pxa_device_desc mmp2_device_uart2;
extern struct pxa_device_desc mmp2_device_uart3;
extern struct pxa_device_desc mmp2_device_uart4;
extern struct pxa_device_desc mmp2_device_twsi1;
extern struct pxa_device_desc mmp2_device_twsi2;
extern struct pxa_device_desc mmp2_device_twsi3;
extern struct pxa_device_desc mmp2_device_twsi4;
extern struct pxa_device_desc mmp2_device_twsi5;
extern struct pxa_device_desc mmp2_device_twsi6;
extern struct pxa_device_desc mmp2_device_nand;
extern struct pxa_device_desc mmp2_device_fb;
extern struct pxa_device_desc mmp2_device_fb_tv;
extern struct pxa_device_desc mmp2_device_fb_pn2;
extern struct pxa_device_desc mmp2_device_fb_ovly;
extern struct pxa_device_desc mmp2_device_fb_tv_ovly;
extern struct pxa_device_desc mmp2_device_fb_pn2_ovly;
extern struct pxa_device_desc mmp2_device_sspa1;
extern struct pxa_device_desc mmp2_device_sspa2;
extern struct pxa_device_desc mmp2_device_vmeta;
extern struct platform_device mmp2_device_imm;
extern struct pxa_device_desc mmp2_device_hdmi;
extern struct platform_device mmp2_device_audiosram;
extern struct pxa_device_desc mmp2_device_keypad;
extern struct platform_device mmp2_device_rtc;
extern struct pxa_device_desc mmp2_device_camera;
extern struct pxa_device_desc mmp2_device_camera2;
extern struct pxa_device_desc mmp2_device_thsens;
extern struct pxa_device_desc mmp2_device_sdh0;
extern struct pxa_device_desc mmp2_device_sdh1;
extern struct pxa_device_desc mmp2_device_sdh2;
extern struct pxa_device_desc mmp2_device_sdh3;
extern struct pxa_device_desc mmp2_device_pwm1;
extern struct pxa_device_desc mmp2_device_pwm2;
extern struct pxa_device_desc mmp2_device_pwm3;
extern struct pxa_device_desc mmp2_device_pwm4;
extern struct pxa_device_desc mmp2_device_zsp;
extern struct pxa_device_desc mmp2_device_fuse;

extern struct platform_device mmp2_device_freq;

/* usb related devices */
extern struct platform_device u2o_device;
extern struct platform_device otg_device;
extern struct platform_device ehci_u2o_device;
extern struct platform_device ehci_u2h_device;
extern struct platform_device ehci_sph1_device;
extern struct platform_device ehci_sph3_device;
/* android usb related */
extern struct platform_device usb_mass_storage_device;
extern struct platform_device usb_rndis_device;
extern struct platform_device android_device_usb;

extern struct pxa_device_desc mmp2_device_w1;

extern void mmp2_init_sdh(struct sdhci_host *host,
			struct sdhci_pxa_platdata *pdata);

static inline int mmp2_add_pwm(int id)
{
	struct pxa_device_desc *d = NULL;

	switch (id) {
		case 1: d = &mmp2_device_pwm1; break;
		case 2: d = &mmp2_device_pwm2; break;
		case 3: d = &mmp2_device_pwm3; break;
		case 4: d = &mmp2_device_pwm4; break;
		default:
			return -EINVAL;
	}

	return pxa_register_device(d, NULL, 0);
}

static inline int mmp2_add_uart(int id)
{
	struct pxa_device_desc *d = NULL;

	switch (id) {
	case 1: d = &mmp2_device_uart1; break;
	case 2: d = &mmp2_device_uart2; break;
	case 3: d = &mmp2_device_uart3; break;
	case 4: d = &mmp2_device_uart4; break;
	default:
		return -EINVAL;
	}

	return pxa_register_device(d, NULL, 0);
}

static inline int mmp2_add_twsi(int id, struct i2c_pxa_platform_data *data,
				  struct i2c_board_info *info, unsigned size)
{
	struct pxa_device_desc *d = NULL;
	int ret;

	switch (id) {
	case 1: d = &mmp2_device_twsi1; break;
	case 2: d = &mmp2_device_twsi2; break;
	case 3: d = &mmp2_device_twsi3; break;
	case 4: d = &mmp2_device_twsi4; break;
	case 5: d = &mmp2_device_twsi5; break;
	case 6: d = &mmp2_device_twsi6; break;
	default:
		return -EINVAL;
	}

	ret = i2c_register_board_info(id - 1, info, size);
	if (ret)
		return ret;

	return pxa_register_device(d, data, sizeof(*data));
}

static inline int mmp2_add_nand(struct pxa3xx_nand_platform_data *info)
{
	return pxa_register_device(&mmp2_device_nand, info, sizeof(*info));
}

static inline int mmp2_add_fb(struct pxa168fb_mach_info *mi)
{
	return pxa_register_device(&mmp2_device_fb, mi, sizeof(*mi));
}

static inline int mmp2_add_fb_tv(struct pxa168fb_mach_info *mi)
{
	return pxa_register_device(&mmp2_device_fb_tv, mi, sizeof(*mi));
}

static inline int mmp2_add_fb_pn2(struct pxa168fb_mach_info *mi)
{
	return pxa_register_device(&mmp2_device_fb_pn2, mi, sizeof(*mi));
}

static inline int mmp2_add_fb_ovly(struct pxa168fb_mach_info *mi)
{
	return pxa_register_device(&mmp2_device_fb_ovly, mi, sizeof(*mi));
}

static inline int mmp2_add_fb_tv_ovly(struct pxa168fb_mach_info *mi)
{
	return pxa_register_device(&mmp2_device_fb_tv_ovly, mi, sizeof(*mi));
}

static inline int mmp2_add_fb_pn2_ovly(struct pxa168fb_mach_info *mi)
{
	return pxa_register_device(&mmp2_device_fb_pn2_ovly, mi, sizeof(*mi));
}

static inline void mmp2_add_imm(void)
{
	int ret;

	ret = platform_device_register(&mmp2_device_imm);

	if (ret)
		dev_err(&mmp2_device_imm.dev,
				"unable to register device: %d\n", ret);
}

static inline void mmp2_add_audiosram(void)
{
	int ret;

	ret = platform_device_register(&mmp2_device_audiosram);

	if (ret)
		dev_err(&mmp2_device_audiosram.dev,
			"unable to register device: %d\n", ret);
}

static inline int mmp2_add_sspa(int id)
{
	struct pxa_device_desc *d = NULL;

	switch (id) {
	case 1: d = &mmp2_device_sspa1; break;
	case 2: d = &mmp2_device_sspa2; break;
	default:
		return -EINVAL;
	}

	return pxa_register_device(d, NULL, 0);
}

static inline int mmp2_add_hdmi(struct uio_hdmi_platform_data *data)
{
        return pxa_register_device(&mmp2_device_hdmi, data, sizeof(*data));
}

static inline int mmp2_add_thermal_sensor(void)
{
        return pxa_register_device(&mmp2_device_thsens, NULL, 0);
}

static inline int mmp2_add_keypad(struct pxa27x_keypad_platform_data *data)
{
	return pxa_register_device(&mmp2_device_keypad, data, sizeof(*data));
}

static inline int mmp2_add_vmeta(struct vmeta_plat_data *pdata)
{
	return pxa_register_device(&mmp2_device_vmeta, pdata, sizeof(*pdata));
}

static inline void mmp2_add_rtc(void)
{
	int ret;
	ret = platform_device_register(&mmp2_device_rtc);
	if (ret)
		dev_err(&mmp2_device_rtc.dev,
			"unable to register device: %d\n", ret);
}

static inline int mmp2_add_cam(int id, struct cam_platform_data *data)
{
	struct pxa_device_desc *d = NULL;

	switch (id) {
		case 1: d = &mmp2_device_camera; break;
		case 2: d = &mmp2_device_camera2; break;
		default:
			return -EINVAL;
	}
	return pxa_register_device(d, data, sizeof(*data));
}

static inline int mmp2_add_sdh(int id, struct sdhci_pxa_platdata *data)
{
	struct pxa_device_desc *d = NULL;

	switch (id) {
	case 0: d = &mmp2_device_sdh0; break;
	case 1: d = &mmp2_device_sdh1; break;
	case 2: d = &mmp2_device_sdh2; break;
	case 3: d = &mmp2_device_sdh3; break;
	default:
		return -EINVAL;
	}

	return pxa_register_device(d, data, sizeof(*data));
}

static inline void mmp2_add_freq(void)
{
        int ret;
        ret = platform_device_register(&mmp2_device_freq);
        if (ret)
                dev_err(&mmp2_device_freq.dev,
                                "unable to register device: %d\n", ret);
}

static inline int mmp2_add_fuse(void)
{
	return pxa_register_device(&mmp2_device_fuse, NULL, 0);
}

static inline int mmp2_add_w1(struct mmp2_w1_platform_data *data)
{
	return pxa_register_device(&mmp2_device_w1, data, sizeof(*data));
}


#endif /* __ASM_MACH_MMP2_H */

