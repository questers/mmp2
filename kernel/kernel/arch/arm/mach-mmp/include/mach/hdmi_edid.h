/* 
 * include/mach/hdmi_edid.h - EDID/DDC Header
 *
 * John Jiangang Jing <jgjing@marvell.com>
 *
 * DDC is a Trademark of VESA (Video Electronics Standard Association).
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive
 * for more details.
*/
#ifndef __HDMI_EDID_H__
#define __HDMI_EDID_H__
//#define EDID_DBG 1
// Debug Print
#ifdef EDID_DBG
#define EDID_DBG_PRINT(para)    printk para
#else
#define EDID_DBG_PRINT(para)
#endif

// Check RetCode
#define EDID_CHK_RET_CODE(RetCode) \
    if (RetCode != EDID_ERR_OK) \
    { \
        EDID_DBG_PRINT(("Error is Parsing: %d\n",RetCode)); \
        return RetCode; \
    }

#define SIZE_OF_EDID_BLK            128
#define EDID_BLK_TAG_ADDR           0x00
// EDID Block 0 Field Position
#define EDID_BLK0_HDR_ADDR          0x00
#define EDID_BLK0_PRD_ID_ADDR       0x08
#define EDID_BLK0_VERS_ADDR         0x12
#define EDID_BLK0_REV_ADDR          0x13
#define EDID_BLK0_DISP_PARAMS_ADDR  0x14
#define EDID_BLK0_ETD_ADDR          0x23
#define EDID_BLK0_STD_ADDR          0x26
#define EDID_BLK0_DTD_ADDR          0x36
#define EDID_BLK0_MDB_ADDR          0x5A
#define EDID_BLK0_EXTN_FLAG_ADDR    0x7E

#define EDID_DTD_LEN                0x12

// EDID CEA Block Field Position
#define EDID_CEA_EXTN_HDR_ADDR      0x00
#define EDID_CEA_EXTN_REV_ADDR      0x01
#define EDID_CEA_EXTN_DTD_OFS_ADDR  0x02
#define EDID_CEA_EXTN_FMT_ADDR      0x03
#define EDID_CEA_EXTN_DATA_BLK_ADDR 0x04

#define EDID_CEA_RESRVD_TAG         0x00
#define EDID_CEA_AUDIO_BLK_TAG      0x01
#define EDID_CEA_VIDEO_BLK_TAG      0x02
#define EDID_CEA_VNDR_BLK_TAG       0x03
#define EDID_CEA_SPKR_ALLOC_BLK_TAG 0x04
#define EDID_CEA_DTC_BLK_TAG        0x05
#define EDID_CEA_EXTNDED_BLK_TAG    0x07

// CEA Block extension tags
#define EDID_CEA_EXTNDED_VIDEOCAPS_BLK_TAG  0x00
#define EDID_CEA_EXTNDED_VIDEOCAL_BLK_TAG   0x05

// EDID Extension Tags
#define EDID_BLOCK_MAP_TAG          0xF0
#define EDID_CEA_EXTN_TAG           0x02
#define EDID_DI_EXTN_TAG            0x40
#define EDID_LS_EXTN_TAG            0x50
#define EDID_MI_EXTN_TAG            0x60
#define EDID_MNTR_MNFR_EXTN_TAG     0xFF

#define SPRT_ENABLED        1
#define SPRT_DISABLED       0
#define RES_INFO_UNDEF      0xFFFF
#define MAX_RES_INFO	64
#define IS_BIT_SET(Num,Pos) (((Num) & (1 << Pos)) == (1 << Pos))
static const char Chars[28]="-ABCDEFGHIJKLMNOPQRSTUVWXYZ";
/* definition of hdmi control timing formats supported*/
typedef enum {
    RES_INVALID   = -1,
    FIRST_RES     = 0,
    RES_NTSC_M    = 0,
    RES_NTSC_J    = 1,
    RES_PAL_M     = 2,
    RES_PAL_BGH   = 3,
    RES_525I60    = 4,
    RES_525I5994  = 5,
    RES_625I50    = 6,
    RES_525P60    = 7,
    RES_525P5994  = 8,
    RES_625P50    = 9,
    RES_720P30    = 10,
    RES_720P2997  = 11,
    RES_720P25    = 12,
    RES_720P60    = 13,
    RES_720P5994  = 14,
    RES_720P50    = 15,
    RES_1080I60   = 16,
    RES_1080I5994 = 17,
    RES_1080I50   = 18,
    RES_1080P30   = 19,
    RES_1080P2997 = 20,
    RES_1080P25   = 21,
    RES_1080P24   = 22,
    RES_1080P2398 = 23,
    RES_1080P60   = 24,
    RES_1080P5994 = 25,
    RES_1080P50   = 26,
    MAX_NUM_RESS
}ENUM_CPCB_TG_RES;

// Pixel repetition info
struct hdmi_pixel_rept_info{
    unsigned int    resMask     : 26;
    unsigned int    prSupport   : 6;
};

struct hdmi_res_info{
    int     hActive;
    int     vActive;
    // Refresh rate in Hz, -1 if refresh rate is
    // undefined in the descriptor
    int     refreshRate;
    // 0 = progressive, 1 = interlaced,  2 = undefined
    int     interlaced;
};

// Calorimetry support
struct hdmi_calorimetry_info{
    unsigned char   xvYCC601    : 1;
    unsigned char   xvYCC709    : 1;
    unsigned char   MD0         : 1;
    unsigned char   MD1         : 1;
    unsigned char   MD2         : 1;
    unsigned char   res         : 3;
};

struct hdmi_spkr_alloc{
    unsigned char   FlFr  : 1; // FrontLeft/Front Rear
    unsigned char   Lfe   : 1; // Low Frequency Effect
    unsigned char   Fc    : 1; // Front Center
    unsigned char   RlRr  : 1; // Rear Left/Rear Right
    unsigned char   Rc    : 1; // Rear Center
    unsigned char   FlcFrc: 1; // Front Left Center/Front Right Center
    unsigned char   RlcRrc: 1; // Rear Left Center /Rear Right Center
    unsigned char   Res   : 1;
};

struct hdmi_audio_freq_sprt{
    unsigned char   Res         : 1;
    unsigned char   Fs32KHz     : 1;
    unsigned char   Fs44_1KHz   : 1;
    unsigned char   Fs48KHz     : 1;
    unsigned char   Fs88_2KHz   : 1;
    unsigned char   Fs96KHz     : 1;
    unsigned char   Fs176_4KHz  : 1;
    unsigned char   Fs192KHz    : 1;
};

struct hdmi_audio_wdlen_sprt{
    unsigned char   Res1    : 1;
    unsigned char   WdLen16 : 1;
    unsigned char   WdLen20 : 1;
    unsigned char   WdLen24 : 1;
    unsigned char   Res2    : 4;
};

struct hdmi_audio_info{
    int	audioFmt; // VPP_HDMI_AUDIO_FMT
    struct hdmi_audio_freq_sprt    freqSprt;
    // Field is valid only for compressed audio formats
    u32	maxBitRate; // in KHz
    // Field is valid only for LPCM
    struct hdmi_audio_wdlen_sprt   wdLenSprt;
    unsigned char               maxNumChnls;
};

struct hdmi_edid{
	struct device *dev;
//	int (*edid_read)(void *,char* ,int);
//	int (*edid_read)(struct i2c_client *, u8, u8, u8 *);
	struct i2c_client *client;
	// EDID Valid
	bool	validEdid;
	// Product Info
	unsigned char	monitorName[14];// Monitor Name in ASCII format
	unsigned char	mnfrName[4];
	unsigned char	productCode[5];
	unsigned char	mnfrWeek;
	unsigned short	mnfrYear;
	unsigned int	serialNum;
	// Monitor Limits
	unsigned char   minVRate;       // Minimum Vertical Rate in Hz
	unsigned char   maxVRate;       // Maximum Vertical Rate in Hz
	unsigned char   minHRate;       // Minimum Horizontal Rate in KHz
	unsigned char   maxHRate;       // Maximum Horizontal Rate in KHz
	unsigned int    maxPixelClock;  // Maximum supported pixel clock rate in MHz
	// Maximum image size
	unsigned short  maxHSize;
	unsigned short  maxVSize;

	// Gamma Factor
	float   gamma;

	// Resolution information
	unsigned int      resCnt;
	struct hdmi_res_info resInfo[64];

	// Indicates if the receiver is HDMI capable
	bool	hdmiMode;

	// Video resolutions supported both by transmitter and receiver
	// (Common ones that can be set for hdmi output display)
	unsigned int       sprtedDispRes;

	// Preferred video resolution
      	struct hdmi_res_info	prefResInfo;

	// Sink feature support
	unsigned char               YCbCr444Sprt;
	unsigned char               YCbCr422Sprt;
	unsigned char               DC30bitSprt;
	unsigned char               DC36bitSprt;
	unsigned char               YCbCr444AtDC;
	unsigned char               AISprt;

	// Pixel repetition support
	struct hdmi_pixel_rept_info    prInfo[4];

	// Colorimtery information
	struct hdmi_calorimetry_info   calInfo;

	// Supported audio format
	struct hdmi_spkr_alloc         spkrAlloc;
	struct hdmi_audio_info         audioInfo[15];

	// CEC Physical address
	unsigned short              cecPhyAddr;
};

/* Edid Basic Block Structures */
struct edid_gen_info{
    // EDID Version
    unsigned char   Version;
    unsigned char   Revision;
    // Receiver Product Details
    unsigned char   MnfrName[4];
    unsigned char   MnfrWeek;
    unsigned short  MnfrYear;
    unsigned char   ProductCode[5];
    unsigned int    SerialNum;
};

enum edid_analog{
    EDID_VPP_LVL1 = 0, // 0.3 - 0.7
    EDID_VPP_LVL2 = 2, // 0.286 - 0.714
    EDID_VPP_LVL3 = 4, // 0.4 - 1.0
    EDID_VPP_LVL4 = 6  // 0.0 - 0.7
};

struct edid_digital_vid_inp
{
    unsigned char    DFP1XEnabled;
};

struct edid_analog_vid_inp{
    enum edid_analog	Level;
    unsigned char   BlankToBlackSetup;
    unsigned char   SepSyncs;
    unsigned char   CompSync;
    unsigned char   SyncOnGreen;
    unsigned char   SerrationVSync;
};

union edid_video_inp{
    struct edid_digital_vid_inp    DigVidInp;
    struct edid_analog_vid_inp    AnalogVidInp;
};

enum edid_disp_type{
    EDID_MONOCHROME_DISP = 0,
    EDID_RGB_DISP,
    EDID_NONRGB_DISP,
    EDID_UNDEF_DISP
};

struct edid_feature_sprt{
    unsigned char    GTFSprt;
    unsigned char    PrefTiming;
    unsigned char    sRGBSprt;
    unsigned char    ActiveOffSprt;
    unsigned char    SuspendSprt;
    unsigned char    StandbySprt;
    enum edid_disp_type   DispType;
};

struct edid_col_char{
    unsigned short  Rx;
    unsigned short  Ry;
    unsigned short  Gx;
    unsigned short  Gy;
    unsigned short  Bx;
    unsigned short  By;
    unsigned short  Wx;
    unsigned short  Wy;
};

struct edid_disp_params{
    unsigned char   DigVideoInput;
    union edid_video_inp  VideoInpDet;
    // In cm
    unsigned char   MaxHImageSize;
    // In cm
    unsigned char   MaxVImageSize;
    // (Actual Gamma Value * 100) - 100
    unsigned char   Gamma;

    struct edid_feature_sprt   FeatureSprt;
    struct edid_col_char       ColorChar;
};

struct edid_monitor_info{
    unsigned char   MonitorName[14];// Monitor Name in ASCII format
    unsigned char   MinVRate;       // Minimum Vertical Rate in Hz
    unsigned char   MaxVRate;       // Maximum Vertical Rate in Hz
    unsigned char   MinHRate;       // Minimum Horizontal Rate in KHz
    unsigned char   MaxHRate;       // Maximum Horizontal Rate in KHz
    unsigned char   MaxPixelClock; // Maximum supported pixel clock rate in MHz/10
};

enum cea_res_frm_fmt{
   RES_INFO_PROGRESSIVE = 0,
   RES_INFO_INTERLACED,
   RES_INFO_FRM_FMT_UNDEF
};

// Picture Aspect Ratio
enum cea_pict_asp_ratio{
    CEA_PICT_AR_NONE = 0x00,
    CEA_PICT_AR_4_3,
    CEA_PICT_AR_16_9,
    CEA_PICT_AR_MAX
};

struct cea_res_info{
    u16              VideoIDCode;
    u16              HActive;
    u16              VActive;
    u32              RefreshRate;
   enum cea_res_frm_fmt     FrmFmt;
   enum cea_pict_asp_ratio  AspRatio;
};
// To be filled after checking the vesa spec
static struct cea_res_info EtdList[16] =
{
    {0, 800, 600, 60, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 800*600   @60Hz
    {0, 800, 600, 56, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 800*600   @56Hz
    {0, 640, 480, 75, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 640*480   @75Hz
    {0, 640, 480, 72, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 640*480   @72Hz
    {0, 640, 480, 67, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 640*480   @67Hz
    {0, 640, 480, 60, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 640*480   @60Hz
    {0, 720, 400, 88, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 720*400   @88Hz
    {0, 720, 400, 70, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 720*400   @70Hz
    {0, 1280,1024,75, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 1280*1024 @75Hz
    {0, 1024,768, 75, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 1024*768  @75Hz
    {0, 1024,768, 70, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 1024*768  @70Hz
    {0, 1024,768, 60, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 1024*768  @60Hz
    {0, 1024,768, 87, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 1024*768  @87Hz
    {0, 832, 624, 75, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 832*624   @75Hz
    {0, 800, 600, 75, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 800*600   @75Hz
    {0, 800, 600, 72, RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},// 800*600   @72Hz
};

static const struct cea_res_info video_code_map[] =
{
    {0 , 0,    0,   0,  RES_INFO_FRM_FMT_UNDEF, CEA_PICT_AR_NONE},//0  - Undefined
    {1 , 640,  480, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_4_3 },//1  - 640*480p   @59.94/60Hz 4:3
    {2 , 720,  480, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_4_3 },//2  - 720*480p   @59.94/60Hz 4:3
    {3 , 720,  480, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//3  - 720*480p   @59.94/60Hz 16:9
    {4 , 1280, 720, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//4  - 1280*720p  @59.94/60Hz 16:9
    {5 , 1920, 1080,60, RES_INFO_INTERLACED   , CEA_PICT_AR_16_9},//5  - 1920*1080i @59.94/60Hz 16:9
    {6 , 1440, 480, 60, RES_INFO_INTERLACED   , CEA_PICT_AR_4_3 },//6  - 1440*480i  @59.94/60Hz 4:3
    {7 , 1440, 480, 60, RES_INFO_INTERLACED   , CEA_PICT_AR_16_9},//7  - 1440*480i  @59.94/60Hz 16:9
    {8 , 1440, 240, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_4_3 },//8  - 1440*240p  @59.94/60Hz 4:3
    {9 , 1440, 240, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//9  - 1440*240p  @59.94/60Hz 16:9
    {10, 2880, 480, 60, RES_INFO_INTERLACED   , CEA_PICT_AR_4_3 },//10 - 2880*480i  @59.94/60Hz 4:3
    {11, 2880, 480, 60, RES_INFO_INTERLACED   , CEA_PICT_AR_16_9},//11 - 2880*480i  @59.94/60Hz 16:9
    {12, 2880, 240, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_4_3 },//12 - 2880*240p  @59.94/60Hz 4:3
    {13, 2880, 240, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//13 - 2880*240p  @59.94/60Hz 16:9
    {14, 1440, 480, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_4_3 },//14 - 1440*480p  @59.94/60Hz 4:3
    {15, 1440, 480, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//15 - 1440*480p  @59.94/60Hz 16:9
    {16, 1920, 1080,60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//16 - 1920*1080p @59.94/60Hz 16:9
    {17, 720,  576, 50, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_4_3 },//17 - 720*576p   @50Hz       4:3
    {18, 720,  576, 50, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//18 - 720*576p   @50Hz       16:9
    {19, 1280, 720, 50, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//19 - 1280*720p  @50Hz       16:9
    {20, 1920, 1080,50, RES_INFO_INTERLACED   , CEA_PICT_AR_16_9},//20 - 1920*1080i @50Hz       16:9
    {21, 1440, 576, 50, RES_INFO_INTERLACED   , CEA_PICT_AR_4_3 },//21 - 1440*576i  @50Hz       4:3
    {22, 1440, 576, 50, RES_INFO_INTERLACED   , CEA_PICT_AR_16_9},//22 - 1440*576i  @50Hz       16:9
    {23, 1440, 288, 50, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_4_3 },//23 - 1440*288p  @50Hz       4:3
    {24, 1440, 288, 50, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//24 - 1440*288p  @50Hz       16:9
    {25, 2880, 576, 50, RES_INFO_INTERLACED   , CEA_PICT_AR_4_3 },//25 - 2880*576i  @50Hz       4:3
    {26, 2880, 576, 50, RES_INFO_INTERLACED   , CEA_PICT_AR_16_9},//26 - 2880*576i  @50Hz       16:9
    {27, 2880, 288, 50, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_4_3 },//27 - 2880*288p  @50Hz       4:3
    {28, 2880, 288, 50, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//28 - 2880*288p  @50Hz       16:9
    {29, 1440, 576, 50, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_4_3 },//29 - 1440*576p  @50Hz       4:3
    {30, 1440, 576, 50, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//30 - 1440*576p  @50Hz       16:9
    {31, 1920,1080, 50, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//31 - 1920*1080p @50Hz       16:9
    {32, 1920,1080, 24, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//32 - 1920*1080p @23.97/24Hz 16:9
    {33, 1920,1080, 25, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//33 - 1920*1080p @25Hz       16:9
    {34, 1920,1080, 30, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//34 - 1920*1080p @29.97/30Hz 16:9
    {35, 2880, 480, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_4_3 },//35 - 2880*480p  @59.94/60Hz 4:3
    {36, 2880, 480, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//36 - 2880*480p  @59.94/60Hz 16:9
    {37, 2880, 576, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_4_3 },//37 - 2880*576p  @59.94/60Hz 4:3
    {38, 2880, 576, 60, RES_INFO_PROGRESSIVE  , CEA_PICT_AR_16_9},//38 - 2880*576p  @59.94/60Hz 16:9
};

// Edid Data structures
struct edid_basic_blk_data{
    // Edid Product, Manufacturer Code
    struct edid_gen_info           GenInfo;
    // Basic Display Parameters
   struct edid_disp_params        DispParams;
    // Monitor Information
    struct edid_monitor_info       MonitorInfo;
    // Supported Video Resolution Information
    unsigned char           NumSpprtedVidFmts;
   struct cea_res_info            VideoResInfo[32];
    unsigned char           PrefTimingEntry;
    // Number of extension blocks
    unsigned char           NumExtns;
};

struct edid_cea_feature_sprt{
    unsigned char    UnderScanSprt      : 1;
    unsigned char    YCbCr444Sprt       : 1;
    unsigned char    YCbCr422Sprt       : 1;
    // Indicates if 30/36/48 bit deep color modes are supported
    unsigned char    DC30bit            : 1;
    unsigned char    DC36bit            : 1;
    unsigned char    DC48bit            : 1;
    // Indicates whether YCbCr444 is supported at Deep color modes
    unsigned char    YCbCr444AtDC       : 1;
    // Indicates whether sink accepts ACP,ISRC packets
    unsigned char    SupportsAI         : 1;
};

// Audio Data Structures
enum edid_audio_format{
    EDID_AUD_FMT_RESERVED = 0,
    EDID_AUD_FMT_LPCM,
    EDID_AUD_FMT_AC3,
    EDID_AUD_FMT_MPEG1,
    EDID_AUD_FMT_MP3,
    EDID_AUD_FMT_MPEG2,
    EDID_AUD_FMT_AAC,
    EDID_AUD_FMT_DTS,
    EDID_AUD_FMT_ATRAC,
    EDID_AUD_FMT_ONE_BIT_AUDIO,
    EDID_AUD_FMT_DOLBY_DIGITAL,
    EDID_AUD_FMT_DTS_HD,
    EDID_AUD_FMT_MAT,
    EDID_AUD_FMT_DST,
    EDID_AUD_FMT_WMA_PRO,
    EDID_AUD_FMT_MAX
};

struct edid_uncompr_audrate{
    unsigned char    Sprt16Bps;
    unsigned char    Sprt20Bps;
    unsigned char    Sprt24Bps;
};

// Should be considered along with the audio format
union edid_audrate{
    unsigned int            CompAudRate;
    struct edid_uncompr_audrate UnComprAudRate;
};

struct edid_audio_info{
    enum edid_audio_format   AudioFmt;
    union edid_audrate        AudioRate;
    unsigned char       MaxNumChnls;
    unsigned char       Frequency;
};

struct edid_spkr_alloc_fld{
    unsigned char   FlFr  : 1; // FrontLeft/Front Rear
    unsigned char   Lfe   : 1; // Low Frequency Effect
    unsigned char   Fc    : 1; // Front Center
    unsigned char   RlRr  : 1; // Rear Left/Rear Right
    unsigned char   Rc    : 1; // Rear Center
    unsigned char   FlcFrc: 1; // Front Left Center/Front Right Center
    unsigned char   RlcRrc: 1; // Rear Left Center /Rear Right Center
    unsigned char   Res   : 1;
};

struct edid_aud_freq{
    unsigned char   Freq32KHz   : 1;
    unsigned char   Freq44_1KHz : 1;
    unsigned char   Freq48KHz   : 1;
    unsigned char   Freq88_2KHz : 1;
    unsigned char   Freq96KHz   : 1;
    unsigned char   Freq176_4KHz: 1;
    unsigned char   Freq192KHz  : 1;
    unsigned char   Reserved    : 1;
};

union aud_freq{
    struct edid_aud_freq FreqBitFld;
    unsigned char FreqValue;
};

union edid_spkr_alloc{
    struct edid_spkr_alloc_fld  Fld;
    unsigned char        Value;
};

// Calorimetry support
struct edid_calorimetry_info{
    unsigned char   xvYCC601    : 1;
    unsigned char   xvYCC709    : 1;
    unsigned char   MD0         : 1;
    unsigned char   MD1         : 1;
    unsigned char   MD2         : 1;
    unsigned char   res         : 3;
};

struct edid_cea_timing_extn_blk{
    bool                    VndrBlkFound;
    bool                    HdmiMode;
    // CEA Block specified feature support
    struct edid_cea_feature_sprt   FeatureSprt;
    // Supported Video Resolutions
    unsigned char           NumSpprtedVidFmts;
    struct cea_res_info            VideoResInfo[38];
    unsigned char           PrefTimingEntry;
    // Audio Information
    unsigned char           BasicAudioSprt;
    unsigned char           NumSprtedAudFmts;
    struct edid_audio_info         AudioInfo[10]; //check max number
    // Speaker Allocation Table
    struct edid_spkr_alloc_fld     SpkrAlloc;
    // Calorimtery information
    struct edid_calorimetry_info   CalorimetryInfo;
    unsigned short          CecPhyAddr;
};

struct edid_block_map_data{
   // Defines the block number where CEA extension is present
    unsigned char           CurBlkNum;
    unsigned char           CeaExtnBlkNum;
   // If specific block is not present, then, it would be set at 0
};

union edid_data{
   struct edid_basic_blk_data          BasicBlk;
   struct edid_cea_timing_extn_blk     CEAExtnBlk;
   struct edid_block_map_data          BlkMap;
};

// EDID Blocks that are supported for parsing
enum edid_blk_type{
    EDID_BASIC_BLK = 0x00,
    EDID_CEA_TIMING_EXTN,
    EDID_BLOCK_MAP,
    EDID_UNDEF_BLK,
    EDID_MAX_BLK_TYPE
};

// EDID Error Codes
enum edid_returns{
    EDID_ERR_OK = 0,
    EDID_ERR_INVALID_PARAM,
    EDID_ERR_INVALID_BLK_DATA,
    EDID_ERR_CRC_FAILED,
    EDID_ERR_UNSUPPORTED_BLK,
    EDID_ERR_MAX
};
#endif /* __HDMI_EDID_H__ */
