/*
 *  linux/arch/arm/mach-mmp/include/mach/pxa688_audiosram.h
 *
 *  PXA688 Audio SRAM Memory Management
 *
 *  Copyright (c) 2011 Marvell Semiconductors Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 */

#ifndef __ARCH_MMP2_AUDIOSRAM_H
#define __ARCH_MMP2_AUDIOSRAM_H

/* ARBITRARY:  SRAM allocations are multiples of this 2^N size */
#define AUDIO_SRAM_GRANULARITY	1024

/*
 * SRAM allocations return a CPU virtual address, or NULL on error.
 * If a DMA address is requested and the SRAM supports DMA, its
 * mapped address is also returned.
 *
 * Errors include SRAM memory not being available, and requesting
 * DMA mapped SRAM on systems which don't allow that.
 */
extern void *audio_sram_alloc(size_t len, dma_addr_t *dma);
extern void audio_sram_free(void *addr, size_t len);

#endif /* __ARCH_MMP2_AUDIOSRAM_H */
