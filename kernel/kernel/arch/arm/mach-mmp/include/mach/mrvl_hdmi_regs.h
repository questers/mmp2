/******************************************************************************
 *
 * Name_	mrvl_hdmi_regs.h
 * Project_	MMP2
 * Jiangang Jing
 * 
 * Copyright (c) 2009, Marvell International Ltd (jgjing@marvell.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *****************************************************************************/

#ifndef	__INC_HDMI_H
#define	__INC_HDMI_H

/*
 *	THE BASE ADDRESSES
 */

#define	HDMI_BASE	0xD420BC00
/*
 *	THE REGISTER DEFINES
 */
#define	HDMI_PHY_CFG_0	(0x0008)
	/* 32 bit HDMI PHY Config 0 Register */
#define	HDMI_PHY_CFG_1	(0x000C)
	/* 32 bit HDMI PHY Config 1 Register */
#define	HDMI_PHY_CFG_2	(0x0010)
	/* 32 bit HDMI PHY Config 2 Register */
#define	HDMI_PHY_CFG_3	(0x0014)
	/* 32 bit HDMI PHY Config 3 Register */
#define	HDMI_AUDIO_CFG	(0x0018)
	/* 32 bit HDMI Audio Contror Register */
#define	HDMI_CLOCK_CFG	(0x001C)
	/* 32 bit HDMI Clock Control Register */
#define	HDMI_PLL_CFG_0	(0x0020)
	/* 32 bit HDMI PLL Control 0 Register */
#define	HDMI_PLL_CFG_1	(0x0024)
	/* 32 bit HDMI PLL Control 1 Register */
#define	HDMI_PLL_CFG_2	(0x0028)	/* 32 bit HDMI PLL Control 2 Register */
#define	HDMI_PLL_CFG_3	(0x002C)	/* 32 bit HDMI PLL Control 3 Register */
#define	HDTX_ACR_N0	(0x0000)	/*  8 bit ACR N Value Bits [7_0] Register */
#define	HDTX_ACR_N1	(0x0001)	/*  8 bit ACR N Value Bits [15_8]	Register */
#define	HDTX_ACR_N2	(0x0002)	/*  8 bit ACR N Value Bits [19_16] Register */
#define	HDTX_ACR_CTS0	(0x0004)	/*  8 bit Software-Calculated ACR CTS Value Bits [7_0] Register */
#define	HDTX_ACR_CTS1	(0x0005)	/*  8 bit Software-Calculated ACR CTS Value Bits [15_8] Register */
#define	HDTX_ACR_CTS2	(0x0006)	/*  8 bit Software-Calculated ACR CTS Value Bits [19_16] Register */
#define	HDTX_ACR_CTRL	(0x0007)	/*  8 bit ACR Control Register */
#define	HDTX_ACR_STS0	(0x0008)	/*  8 bit Hardware Computed ACR CTS Bits [7_0] Register*/
#define	HDTX_ACR_STS1	(0x0009)	/*  8 bit Hardware Computed ACR CTS Bits [15_8] Register */
#define	HDTX_ACR_STS2	(0x000A)	/*  8 bit Hardware Computed ACR CTS Bits [19_16] Register */
#define	HDTX_AUD_CTRL	(0x000B)	/*  8 bit Audio Control Register  */
#define	HDTX_I2S_CTRL	(0x000C)	/*  8 bit I<super 2>S Control Register */
#define	HDTX_I2S_DLEN	(0x000D)	/*  8 bit I<super 2>S Valid Data Length Register*/
#define	HDTX_I2S_DBG_LFT0	(0x0010)	/*  8 bit I<super 2>S Left Channel Debug Bits [7_0] Register */
#define	HDTX_I2S_DBG_LFT1	(0x0011)	/*  8 bit I<super 2>S Left Channel Debug Bits [15_8] Register */
#define	HDTX_I2S_DBG_LFT2	(0x0012)	/*  8 bit I<super 2>S Left Channel Debug Bits [23_16] Register */
#define	HDTX_I2S_DBG_LFT3	(0x0013)	/*  8 bit I<super 2>S Left Channel Debug Bits [31_24]Register */
#define	HDTX_I2S_DBG_RIT0	(0x0014)	/*  8 bit I<super 2>S Right Channel Debug Bits [7_0] Register */
#define	HDTX_I2S_DBG_RIT1	(0x0015)	/*  8 bit I<super 2>S Right Channel Debug Bits [15_8] Register */
#define	HDTX_I2S_DBG_RIT2	(0x0016)	/*  8 bit I<super 2>S Right Channel Debug Bits [23_16] Register */
#define	HDTX_I2S_DBG_RIT3	(0x0017)	/*  8 bit I<super 2>S Right Channel Debug Bits [31_24] Register */
#define	HDTX_CHSTS_0	(0x0018)	/*  8 bit Channel Status Bits [7_0] Register */
#define	HDTX_CHSTS_1	(0x0019)	/*  8 bit Channel Status Bits [15_8] Register*/
#define	HDTX_CHSTS_2	(0x001A)	/*  8 bit Channel Status Bits [23_16] Register */
#define	HDTX_CHSTS_3	(0x001B)	/*  8 bit Channel Status Bits [31_24] Register*/
#define	HDTX_CHSTS_4	(0x001C)	/*  8 bit Channel Status Bits [39_32] Register */
#define	HDTX_FIFO_CTRL	(0x001D)	/*  8 bit FIFO Control Register */
#define	HDTX_MEMSIZE_L	(0x001E)	/*  8 bit FIFO Memory Size Bits [7_0] Register */
#define	HDTX_MEMSIZE_H	(0x001F)	/*  8 bit FIFO Memory Size Bit [8] Register */
#define	HDTX_GCP_CFG0	(0x0020)	/*  8 bit GCP Packet Configure Register 0 */
#define	HDTX_GCP_CFG1	(0x0021)	/*  8 bit GCP Packet Configure Register 1 */
#define	HDTX_AUD_STS	(0x0022)	/*  8 bit Audio Status Register */
#define	HDTX_HTOT_L	(0x0024)	/*  8 bit HTOTAL Bits [7_0] Register*/
#define	HDTX_HTOT_H	(0x0025)	/*  8 bit HTOTAL Bits [15_8] Register */
#define	HDTX_HBLANK_L	(0x0026)	/*  8 bit HBLANK Bits [7_0] Register*/
#define	HDTX_HBLANK_H	(0x0027)	/*  8 bit HBLANK Bits [15_8] Register */
#define	HDTX_VTOT_L	(0x0028)	/*  8 bit VTOTAL Bits [7_0] Register */
#define	HDTX_VTOT_H	(0x0029)	/*  8 bit VTOTAL Bits [15_8] Register */
#define	HDTX_VRES_L	(0x002A)	/*  8 bit VRES Bits [7_0] Register */
#define	HDTX_VRES_H	(0x002B)	/*  8 bit VRES Bits [15_8] Register */
#define	HDTX_VSTART_L	(0x002C)	/*  8 bit VSTART Bits [7_0] Register */
#define	HDTX_VSTART_H	(0x002D)	/*  8 bit VSTART Bits [15_8] Register */
#define	HDTX_HTOT_STS_L	(0x002E)	/*  8 bit HTOTAL Bits [7_0] Register */
#define	HDTX_HTOT_STS_H	(0x002F)	/*  8 bit HTOTAL Bits [15_8] Register */
#define	HDTX_HBLANK_STS_L	(0x0030)	/* 8 bit HBLANK Bits [7_0] Register */
#define	HDTX_HBLANK_STS_H	(0x0031)	/* 8 bit HBLANK Bits [15_8] Register */
#define	HDTX_VTOT_STS_L	(0x0032)	/*  8 bit VTOTAL Bits [7_0] Register */
#define	HDTX_VTOT_STS_H	(0x0033)	/*  8 bit VTOTAL Bits [15_8] Register */
#define	HDTX_VRES_STS_L	(0x0034)	/*  8 bit VRES Bits [7_0] Register */
#define	HDTX_VRES_STS_H	(0x0035)	/*  8 bit VRES Bits [15_8] Register */
#define	HDTX_VSTART_STS_L	(0x0036) /*  8 bit VSTART Bits [7_0] Register */
#define	HDTX_VSTART_STS_H	(0x0037) /*  8 bit VSTART Bits [15_8] Register */
#define	HDTX_VIDEO_STS	(0x0038)	/*  8 bit Video Status Register */
#define	HDTX_VIDEO_CTRL	(0x0039)	/*  8 bit Video Control Register */
#define	HDTX_HDMI_CTRL	(0x003A)	/*  8 bit HDMI Control Register */
#define	HDTX_PP_HW	(0x0046)	/*  8 bit Hardware Computed Pixel Packing Value Register */
#define	HDTX_DC_FIFO_SFT_RST	(0x0047)	/*  8 bit Deep Color FIFO Soft Reset Register */
#define	HDTX_DC_FIFO_WR_PTR	(0x0048)	/*  8 bit FIFO Write Pointer Register */
#define	HDTX_DC_FIFO_RD_PTR	(0x0049)	/*  8 bit FIFO Read Pointer Register */
#define	HDTX_TDATA0_0	(0x004C)	/*  8 bit Test Data [7_0] for TMDS Channel 0 Register */
#define	HDTX_TDATA0_1	(0x004D)	/*  8 bit Test Data [15_8] for TMDS Channel 0 Register */
#define	HDTX_TDATA0_2	(0x004E)	/*  8 bit Test Data [19_16] for TMDS Channel 0 Register*/
#define	HDTX_TDATA1_0	(0x0050)	/*  8 bit Test Data [7_0] for TMDS Channel 1 Register */
#define	HDTX_TDATA1_1	(0x0051)	/*  8 bit Test Data [15_8] for TMDS Channel 1 Register */
#define	HDTX_TDATA1_2	(0x0052)	/*  8 bit Test Data [19_16] for TMDS Channel 1 Register */
#define	HDTX_TDATA2_0	(0x0054)	/*  8 bit Test Data [7_0] for TMDS Channel 2 Register */
#define	HDTX_TDATA2_1	(0x0055)	/*  8 bit Test Data [15_8] for TMDS Channel 2 Register */
#define	HDTX_TDATA2_2	(0x0056)	/*  8 bit Test Data [19_16] for TMDS Channel 2 Registe */
#define	HDTX_TDATA3_0	(0x0058)	/*  8 bit Test Data [7_0]	for TMDS Channel 3 Register */
#define	HDTX_TDATA3_1	(0x0059)	/*  8 bit Test Data [15_8] for TMDS Channel 3 Register */
#define	HDTX_TDATA3_2	(0x005A)	/*  8 bit Test Data [19_16] for TMDS Channel Register */
#define	HDTX_TDATA_SEL	(0x005B)	/*  8 bit Test Data Selection Control	Register */
#define	HDTX_SWAP_CTRL	(0x005C)	/*  8 bit TMDS Data Swap Control	Register */
#define	HDTX_AVMUTE_CTRL	(0x005D)	/*  8 bit AVMUTE Control Register */
#define	HDTX_HOST_PKT_CTRL0	(0x005E)	/*  8 bit Host Packet Control Register 0 */
#define	HDTX_HOST_PKT_CTRL1	(0x005F)	/*  8 bit Host Packet Control Register 1 */
#define	HDTX_PKT0_BYTE0_30	(0x0060)	/* PKT0 Byte 0 to 30 Registers Start */
#define	HDTX_PKT1_BYTE0_30	(0x0080)	/* PKT1 Byte 0 to 30 Registers Start */
#define	HDTX_PKT2_BYTE0_30	(0x00A0)	/* PKT2 Byte 0 to 30 Registers Start */
#define	HDTX_PKT3_BYTE0_30	(0x00C0)	/* PKT3 Byte 0 to 30 Registers Start */
#define	HDTX_PKT4_BYTE0_30	(0x00E0)	/* PKT4 Byte 0 to 30 Registers Start */
#define	HDTX_PKT5_BYTE0_30	(0x0100)	/* PKT5 Byte 0 to 30 Registers Start */
#define	HDTX_UBITS_0	(0x0120)	/*  8 bit User Data Bits of SPDIF Header Register 0 */
#define	HDTX_UBITS_1	(0x0121)	/*  8 bit User Data Bits of SPDIF Header Register 1 */
#define	HDTX_UBITS_2	(0x0122)	/*  8 bit User Data Bits of SPDIF Header Register 2 */
#define	HDTX_UBITS_3	(0x0123)	/*  8 bit User Data Bits of SPDIF	Header Register 3 */
#define	HDTX_UBITS_4	(0x0124)	/*  8 bit User Data Bits of SPDIF Header Register 4 */
#define	HDTX_UBITS_5	(0x0125)	/*  8 bit User Data Bits of SPDIF	Header Register 5 */
#define	HDTX_UBITS_6	(0x0126)	/*  8 bit User Data Bits of SPDIF Header Register 6 */
#define	HDTX_UBITS_7	(0x0127)	/*  8 bit User Data Bits of SPDIF Header Register 7 */
#define	HDTX_UBITS_8	(0x0128)	/*  8 bit User Data Bits of SPDIF Header Register 8 */
#define	HDTX_UBITS_9	(0x0129)	/*  8 bit User Data Bits of SPDIF Header Register 9 */
#define	HDTX_UBITS_10	(0x012A)	/*  8 bit User Data Bits of SPDIF Header Register 10 */
#define	HDTX_UBITS_11	(0x012B)	/*  8 bit User Data Bits of SPDIF Header Register 11 */
#define	HDTX_UBITS_12	(0x012C)	/*  8 bit User Data Bits of SPDIF Header Register 12 */
#define	HDTX_UBITS_13	(0x012D)	/*  8 bit User Data Bits of SPDIF Header Register 13 */
#define	HDTX_HBR_PKT	(0x012E)	/*  8 bit HBR Packet Transmission Control Register */
#define	HDTX_PHY_FIFO_SOFT_RST	(0x0130)	/*  8 bit HDMI Tx PHY FIFO Soft Reset Register */
#define	HDTX_PHY_FIFO_PTRS	(0x0131)	/*  8 bit HDMITX PHY FIFO	Read and write Pointers Register */
#define	HDTX_PRBS_CTRL0		(0x0132)	/*  8 bit PRBS Control Register */
#define	HDTX_HST_PKT_CTRL2	(0x0133)	/*  8 bit Host Packet Control Register 2 */
#define	HDTX_HST_PKT_START	(0x0134)	/*  8 bit Host Packet Transmission Control During VBI Register */
#define	HDTX_AUD_CH1_SEL	(0x0137)	/*  8 bit Audio Channel 1	Select Register */
#define	HDTX_AUD_CH2_SEL	(0x0138)	/*  8 bit Audio Channel 2	Select Register */
#define	HDTX_AUD_CH3_SEL	(0x0139)	/*  8 bit Audio Channel 3	Select Register */
#define	HDTX_AUD_CH4_SEL	(0x013A)	/*  8 bit Audio Channel 4	Select Register */
#define	HDTX_AUD_CH5_SEL	(0x013B)	/*  8 bit Audio Channel 5	Select Register */
#define	HDTX_AUD_CH6_SEL	(0x013C)	/*  8 bit Audio Channel 6	Select Register */
#define	HDTX_AUD_CH7_SEL	(0x013D)	/*  8 bit Audio Channel 7	Select Register */
#define	HDTX_AUD_CH8_SEL	(0x013E)	/*  8 bit Audio Channel 8 Select Register */
#define	TX_HDCP_AKEY0_BYTE_0	(0x1200)	/*  8 bit AKEY0 [7_0] Register */
#define	TX_HDCP_AKEY0_BYTE_1	(0x1201)	/*  8 bit AKEY0 [15_8] Register */
#define	TX_HDCP_AKEY0_BYTE_2	(0x1202)	/*  8 bit AKEY0 [23_16] Register */
#define	TX_HDCP_AKEY0_BYTE_3	(0x1203)	/*  8 bit AKEY0 [31_24] Register */
#define	TX_HDCP_AKEY0_BYTE_4	(0x1204)	/*  8 bit AKEY0 [39_32] Register */
#define	TX_HDCP_AKEY0_BYTE_5	(0x1205)	/*  8 bit AKEY0 [47_40] Register */
#define	TX_HDCP_AKEY0_BYTE_6	(0x1206)	/*  8 bit AKEY0 [55_48] Register */
#define	TX_HDCP_AKSV_BYTE_0	(0x1340)	/*  8 bit AKSV [7_0] Register */
#define	TX_HDCP_AKSV_BYTE_1	(0x1341)	/*  8 bit AKSV [15_8] Register */
#define	TX_HDCP_AKSV_BYTE_2	(0x1342)	/*  8 bit AKSV [23_16] Register */
#define	TX_HDCP_AKSV_BYTE_3	(0x1343)	/*  8 bit AKSV [31_24] Register */
#define	TX_HDCP_AKSV_BYTE_4	(0x1344)	/*  8 bit AKSV [39_32] Register */
#define	TX_HDCP_CONTROL	(0x1350)	/*  8 bit HDCP Control Register */
#define	TX_HDCP_STATUS_1	(0x1351)	/*  8 bit HDCP Status Register 1 */
#define	TX_HDCP_STATUS_2	(0x1352)	/*  8 bit HDCP Status Register 2 */
#define	TX_HDCP_INTR_0	(0x1353)	/*  8 bit HDCP Interrupt Register */
#define	TX_HDCP_TX_AKSV_0	(0x1354)	/*  8 bit TX KSV Byte 0 Register*/
#define	TX_HDCP_TX_AKSV_1	(0x1355)	/*  8 bit TX KSV Byte 1 Register */
#define	TX_HDCP_TX_AKSV_2	(0x1356)	/*  8 bit TX KSV Byte 2 Register*/
#define	TX_HDCP_TX_AKSV_3	(0x1357)	/*  8 bit TX KSV Byte 3 Register */
#define	TX_HDCP_TX_AKSV_4	(0x1358)	/*  8 bit TX KSV Byte 4 Register*/
#define	TX_HDCP_RX_BKSV_0	(0x135C)	/*  8 bit RX KSV Byte 0 Register */
#define	TX_HDCP_RX_BKSV_1	(0x135D)	/*  8 bit RX KSV Byte 1 Register */
#define	TX_HDCP_RX_BKSV_2	(0x135E)	/*  8 bit RX KSV Byte 2 Register */
#define	TX_HDCP_RX_BKSV_3	(0x135F)	/*  8 bit RX KSV Byte 3 Register*/
#define	TX_HDCP_RX_BKSV_4	(0x1360)	/*  8 bit RX KSV Byte 4 Register*/
#define	TX_HDCP_TX_AINFO	(0x1361)	/*  8 bit Tx AINFO Register */
#define	TX_HDCP_RX_BCAPS	(0x1362)	/*  8 bit BCAPS Read from	HDMI Rx Register */
#define	TX_HDCP_RX_BSTATUS_0	(0x1364)	/*  8 bit BSTATUS MSB Byte Read from HDMI Rx Register*/
#define	TX_HDCP_RX_BSTATUS_1	(0x1365)	/*  8 bit BSTATUS LSB Byte Read from HDMI Rx Register */
#define	TX_HDCP_TX_AN_0	(0x1368)	/*  8 bit HDCP Tx AN Value (Byte 0) Register */
#define	TX_HDCP_TX_AN_1	(0x1369)	/*  8 bit HDCP Tx AN Value (Byte 1) Register */
#define	TX_HDCP_TX_AN_2	(0x136A)	/*  8 bit HDCP Tx AN Value (Byte 2) Register */
#define	TX_HDCP_TX_AN_3	(0x163B)	/*  8 bit HDCP Tx AN Value (Byte 3) Register */
#define	TX_HDCP_TX_AN_4	(0x163C)	/*  8 bit HDCP Tx AN Value (Byte 4) Register*/
#define	TX_HDCP_TX_AN_5	(0x163D)	/*  8 bit HDCP Tx AN Value (Byte 5) Register */
#define	TX_HDCP_TX_AN_6	(0x163E)	/*  8 bit HDCP Tx AN Value (Byte 6) Register */
#define	TX_HDCP_TX_AN_7	(0x163F)	/*  8 bit HDCP Tx AN Value (Byte 7) Register */
#define	TX_HDCP_TX_M0_0	(0x1370)	/*  8 bit HDCP Tx M0 value (Byte 0) Register */
#define	TX_HDCP_TX_M0_1	(0x1371)	/*  8 bit HDCP Tx M0 Value (Byte 1) Register */
#define	TX_HDCP_TX_M0_2	(0x1372)	/*  8 bit HDCP Tx M0 Value (Byte 2) Register */
#define	TX_HDCP_TX_M0_3	(0x1373)	/*  8 bit HDCP Tx M0 Value (Byte 3) Register */
#define	TX_HDCP_TX_M0_4	(0x1374)	/*  8 bit HDCP Tx M0 Value (Byte 4) Register */
#define	TX_HDCP_TX_M0_5	(0x1375)	/*  8 bit HDCP Tx M0 Value (Byte 5) Register */
#define	TX_HDCP_TX_M0_6	(0x1376)	/*  8 bit HDCP Tx M0 Value (Byte 6) Register */
#define	TX_HDCP_TX_M0_7	(0x1377)	/*  8 bit HDCP Tx M0 Value (Byte 7) Register */
#define	TX_HDCP_TX_R0_0	(0x1378)	/*  8 bit Tx R0 (Byte 0) Register */
#define	TX_HDCP_TX_R0_1	(0x1379)	/*  8 bit Tx R0 (Byte 1) Register */
#define	TX_HDCP_RX_R0_0	(0x137A)	/*  8 bit Rx R0 (Byte 0) Register */
#define	TX_HDCP_RX_R0_1	(0x137B)	/*  8 bit Rx R0 (Byte 1) Register */
#define	TX_HDCP_TX_RI_0	(0x137C)	/*  8 bit Tx RI (Byte 0) Register */
#define	TX_HDCP_TX_RI_1	(0x137D)	/*  8 bit Tx RI (Byte 1) Register */
#define	TX_HDCP_RX_RI_0	(0x137E)	/*  8 bit Rx RI (Byte 0) Register */
#define	TX_HDCP_RX_RI_1	(0x137F)	/*  8 bit Rx RI (Byte 1) Register */
#define	TX_HDCP_TX_PJ	(0x1380)	/*  8 bit Tx PJ Register */
#define	TX_HDCP_RX_PJ	(0x1381)	/*  8 bit Rx PJ Register */
#define	TX_HDCP_FIX_CLR_0	(0x1384)	/*  8 bit Default Video Value (Byte 0) Register */
#define	TX_HDCP_FIX_CLR_1	(0x1385)	/*  8 bit Default Video Value (Byte 1) Register */
#define	TX_HDCP_FIX_CLR_2	(0x1386)	/*  8 bit Default Video Value (Byte 2) Register */
#define	TX_HDCP_KINIT_0	(0x1388)	/*  8 bit KINIT (Byte 0) Register */
#define	TX_HDCP_KINIT_1	(0x1389)	/*  8 bit KINIT (Byte 1) Register */
#define	TX_HDCP_KINIT_2	(0x138A)	/*  8 bit KINIT (Byte 2) Register */
#define	TX_HDCP_KINIT_3	(0x138B)	/*  8 bit KINIT (Byte 3) Register */
#define	TX_HDCP_KINIT_4	(0x138C)	/*  8 bit KINIT (Byte 4) Register */
#define	TX_HDCP_KINIT_5	(0x138D)	/*  8 bit KINIT (Byte 5) Register */
#define	TX_HDCP_KINIT_6	(0x138E)	/*  8 bit KINIT (Byte 6) Register */
#define	TX_HDCP_BINIT_0	(0x1390)	/*  8 bit BINIT (Byte 0) Register */
#define	TX_HDCP_BINIT_1	(0x1391)	/*  8 bit BINIT (Byte 1) Register */
#define	TX_HDCP_BINIT_2	(0x1392)	/*  8 bit BINIT (Byte 2) Register */
#define	TX_HDCP_BINIT_3	(0x1393)	/*  8 bit BINIT (Byte 3) Register */
#define	TX_HDCP_BINIT_4	(0x1394)	/*  8 bit BINIT (Byte 4) Register */
#define	TX_HDCP_BINIT_5	(0x1395)	/*  8 bit BINIT (Byte 5) Register */
#define	TX_HDCP_BINIT_6	(0x1396)	/*  8 bit BINIT (Byte 6) Register */
#define	TX_HDCP_BINIT_7	(0x1397)	/*  8 bit BINIT (Byte 7) Register */
#define	TX_HDCP_INTR_CLR	(0x1398)	/*  8 bit Interrupt Clear Register */
#define	TX_HDCP_INTR_MAS	(0x1399)	/*  8 bit Interrupt Masking Register */
/*
 *	THE BIT DEFINES
 */
/*	HDMI_PHY_CFG_0		0x0008	HDMI PHY Config 0 Register */
#define	HDMI_PHY_CFG_0_CP_MSK	((0xff) << 24)	/* HDMI PHY CP Config */
#define	HDMI_PHY_CFG_0_CP_BASE	24
#define	HDMI_PHY_CFG_0_EAMP_MSK	((0xfff) << 12)	/* HDMI PHY EAMP Config */
#define	HDMI_PHY_CFG_0_EAMP_BASE	12
#define	HDMI_PHY_CFG_0_DAMP_MSK		(0xfff)/* HDMI PHY DAMP Config */
#define	HDMI_PHY_CFG_0_DAMP_BASE	0
/*	HDMI_PHY_CFG_1			0x000C	HDMI PHY Config 1 Register */
#define	HDMI_PHY_CFG_1_AJ_D_MSK		((0xf) << 28)	/* HDMI PHY AJ_D Config */
#define	HDMI_PHY_CFG_1_AJ_D_BASE	28
#define	HDMI_PHY_CFG_1_SVTX_MSK		((0xfff) << 16)/* HDMI PHY SVTX Config */
#define	HDMI_PHY_CFG_1_SVTX_BASE	16
#define	HDMI_PHY_CFG_1_IDRV_MSK		(0xffff)	/* HDMI PHY IDRV Config */
#define	HDMI_PHY_CFG_1_IDRV_BASE	0
/*	HDMI_PHY_CFG_2		0x0010	HDMI PHY Config 2 Register */
/* HDMI PHY TP_EN Config */
#define	HDMI_PHY_CFG_2_TP_EN_MSK	((0xf) << 28)	
#define	HDMI_PHY_CFG_2_TP_EN_BASE	28
/* HDMI PHY RESET_TX Config */
#define	HDMI_PHY_CFG_2_RESET_TX		(1 << 27)			
/* HDMI PHY POLSWAP_TX Config */
#define	HDMI_PHY_CFG_2_POLSWAP_TX_MSK	((0xf) << 23)
#define	HDMI_PHY_CFG_2_POLSWAP_TX_BASE	23
/* HDMI PHY PD_TX Config */
#define	HDMI_PHY_CFG_2_PD_TX_MSK	((0xf) << 19)	
#define	HDMI_PHY_CFG_2_PD_TX_BASE	19
/* HDMI PHY PD_IREF Config */
#define	HDMI_PHY_CFG_2_PD_IREF		(1 << 18)			
/* HDMI PHY Loopback Config */
#define	HDMI_PHY_CFG_2_LOOPBACK_MSK	((0xf) << 14)	
#define	HDMI_PHY_CFG_2_LOOPBACK_BASE	14
/* HDMI PHY INV_CK20T Config */
#define	HDMI_PHY_CFG_2_INV_CK20T_MSK	((0xf) << 10)	
#define	HDMI_PHY_CFG_2_INV_CK20T_BASE	10
#define	HDMI_PHY_CFG_2_AUX_MSK		((0x3f)	<< 4)/* HDMI PHY AUS Config */
#define	HDMI_PHY_CFG_2_AUX_BASE		4
/* HDMI PHY AJ_EN Config */
#define	HDMI_PHY_CFG_2_AJ_EN_MSK	(0xf)		
#define	HDMI_PHY_CFG_2_AJ_EN_BASE	0

/*	HDMI_PHY_CFG_3	0x0014	HDMI PHY Config 3 Register */
/*		Bit(s) HDMI_PHY_CFG_3_RSRV_31_11 reserved */
/* HDMI Hot Plug Detection Status */
#define	HDMI_PHY_CFG_3_HPD	(1 << 10)				
/* HDMI PHY TXDRVX2 Config */
#define	HDMI_PHY_CFG_3_TXDRVX2_MSK	((0xf) << 6)			
#define	HDMI_PHY_CFG_3_TXDRVX2_BASE	6
/* HDMI PHY TPC Config */
#define	HDMI_PHY_CFG_3_TPC_MSK	((0x1f) << 1)		
#define	HDMI_PHY_CFG_3_TPC_BASE	1
/* HDMI PHY SYNC Config */
#define	HDMI_PHY_CFG_3_SYNC	1				

/*	HDMI_AUDIO_CFG			0x0018	HDMI Audio Control Register */
/*		Bit(s) HDMI_AUDIO_CFG_RSRV_31_12 reserved */
/* HDMI Audio I<super 2>S Config */
#define	HDMI_AUDIO_CFG_I2S	(1 << 11)
/* HDMI Audio wsp Config */
#define	HDMI_AUDIO_CFG_WSP	(1 << 10)
/* HDMI Audio fsp Config */
#define	HDMI_AUDIO_CFG_FSP	(1 << 9)
/* HDMI Audio clkp Config */
#define	HDMI_AUDIO_CFG_CLKP	(1 << 8)
/* HDMI Audio xdatdly Config */
#define	HDMI_AUDIO_CFG_XDATDLY_MSK	((0x3) << 6)
#define	HDMI_AUDIO_CFG_XDATDLY_BASE	6
/* HDMI Audio xwdlen Config */
#define	HDMI_AUDIO_CFG_XWDLEN_MSK	((0x7) << 3)
#define	HDMI_AUDIO_CFG_XWDLEN_BASE	3
/* HDMI Audio xfrlen Config */
#define	HDMI_AUDIO_CFG_XFRLEN_MSK	(0x7)
#define	HDMI_AUDIO_CFG_XFRLEN_BASE	0
/*	HDMI_CLOCK_CFG			0x001C	HDMI Clock Control Register */
/*		Bit(s) HDMI_CLOCK_CFG_RSRV_31_17 reserved */
/* HDMI prclk Config */
#define	HDMI_CLOCK_CFG_HDMI_PRCLK_DIV_MSK	((0xf) << 13)
#define	HDMI_CLOCK_CFG_HDMI_PRCLK_DIV_BASE	13
#define	HDMI_CLOCK_CFG_HDMI_TCLK_DIV_MSK	((0xf) << 9 )		/* HDMI tclk Config */
#define	HDMI_CLOCK_CFG_HDMI_TCLK_DIV_BASE	9
#define	HDMI_CLOCK_CFG_HDMI_MCLK_DIV_MSK	((0xf) << 5)/* HDMI mclk Config */
#define	HDMI_CLOCK_CFG_HDMI_MCLK_DIV_BASE	5
/* HDMI Enable Config */
#define	HDMI_CLOCK_CFG_HDMI_EN	(1 << 4)
#define	HDMI_CLOCK_CFG_RTC_MSK	((0x3) << 2)/* HDMI RTC Config */
#define	HDMI_CLOCK_CFG_RTC_BASE	2
#define	HDMI_CLOCK_CFG_WTC_MSK	(0x3)		/* HDMI WTC Config */
#define	HDMI_CLOCK_CFG_WTC_BASE	0

/*HDMI_PLL_CFG_0	0x0020	HDMI PLL Control 0 Register */
/*Bit(s) HDMI_PLL_CFG_0_RSRV_31 reserved */
/* HDMI PLL DCCI Config, DCCI [1_0] */
#define	HDMI_PLL_CFG_0_DCCI_MSK	((0x3) << 29)
#define	HDMI_PLL_CFG_0_DCCI_BASE	29
/* HDMI PLL INTPI Config, INTPI [1_0] */
#define	HDMI_PLL_CFG_0_INTPI_MSK	((0x3) << 27)
#define	HDMI_PLL_CFG_0_INTPI_BASE	27
/* HDMI PLL CLK_DET_EN Config */
#define	HDMI_PLL_CFG_0_CLK_DET_EN	(1 << 26)
/* HDMI PLL DCC_CTRL Config */
#define	HDMI_PLL_CFG_0_DCC_CTRL	(1 << 25)			
#define	HDMI_PLL_CFG_0_KVCO_MSK	((0x1f) << 20)	/* HDMI PLL KVCO Config */
#define	HDMI_PLL_CFG_0_KVCO_BASE			20
/* HDMI PLL EN_TST_CLK Config */
#define	HDMI_PLL_CFG_0_EN_TST_CLK	(1 << 19)			
/* HDMI PLL POSTDIV Config */
#define	HDMI_PLL_CFG_0_POSTDIV_MSK	((0x1f) << 14)
#define	HDMI_PLL_CFG_0_POSTDIV_BASE	14
#define	HDMI_PLL_CFG_0_ICP_MSK	((0xf) << 10)	/* HDMI PLL ICP Config */
#define	HDMI_PLL_CFG_0_ICP_BASE	10
/* HDMI PLL VDDR10_PLL Config */
#define	HDMI_PLL_CFG_0_VDDR10_PLL_MSK	((0x3) << 8)
#define	HDMI_PLL_CFG_0_VDDR10_PLL_BASE	8
/* HDMI PLL VDDR_RING Config */
#define	HDMI_PLL_CFG_0_VDDR_RING_MSK	((0x3) << 6)
#define	HDMI_PLL_CFG_0_VDDR_RING_BASE	6
/* HDMI PLL VDDR12_INTP Config */
#define	HDMI_PLL_CFG_0_VDDR12_INTP_MSK	((0x3) << 4)		
#define	HDMI_PLL_CFG_0_VDDR12_INTP_BASE	4
/* HDMI PLL VDDR15_VCO Config */
#define	HDMI_PLL_CFG_0_VDDR15_VCO_MSK	((0x3) << 2)
#define	HDMI_PLL_CFG_0_VDDR15_VCO_BASE	2
/* HDMI PLL_REGPUMP Config */
#define	HDMI_PLL_CFG_0_PLL_REGPUMP_MSK	(0x3)		
#define	HDMI_PLL_CFG_0_PLL_REGPUMP_BASE	0

/*HDMI_PLL_CFG_1 0x0024	HDMI PLL Control 1 Register */
/*Bit(s) HDMI_PLL_CFG_1_RSRV_31_29 reserved */
/* HDMI PLL TEST_MON Config */
#define	HDMI_PLL_CFG_1_TEST_MON_MSK	((0xf) << 25)
#define	HDMI_PLL_CFG_1_TEST_MON_BASE	25
/* HDMI PLL EN_PANNEL Config */
#define	HDMI_PLL_CFG_1_EN_PANNEL	(1 << 24)		
/* HDMI PLL EN_HDMI Config */
#define	HDMI_PLL_CFG_1_EN_HDMI	(1 << 23)			
/* HDMI PLL FREQ_OFFSET_READY_ADJ Config */
#define	HDMI_PLL_CFG_1_FREQ_OFFSET_READY_ADJ	(1 << 22)			
/* HDMI PLL FREQ_OFFSET_INNER Config */
#define	HDMI_PLL_CFG_1_FREQ_OFFSET_INNER_MSK	((0x3ffff) << 4)
#define	HDMI_PLL_CFG_1_FREQ_OFFSET_INNER_BASE	4
/* HDMI PLL Mode Config, MODE [1_0] */
#define	HDMI_PLL_CFG_1_MODE_MSK	SHIFT2(0x3)		
#define	HDMI_PLL_CFG_1_MODE_BASE	2
/* HDMI PLL EN_FINE_DCC Config */
#define	HDMI_PLL_CFG_1_EN_FINE_DCC	(1 << 1)
/* HDMI PLL EN_DCC Config */
#define	HDMI_PLL_CFG_1_EN_DCC	1	

/*HDMI_PLL_CFG_2 0x0028	HDMI PLL Control 2 Register */
/*Bit(s) HDMI_PLL_CFG_2_RSRV_31_29 reserved */
/* HDMI PLL IPM Config */
#define	HDMI_PLL_CFG_2_IPM_MSK	((0x1f) << 24)
#define	HDMI_PLL_CFG_2_IPM_BASE	24
/*Bit(s) HDMI_PLL_CFG_2_RSRV_23_18 reserved */
/* HDMI PLL FREQ_OFFSET_ADJ Config */
#define	HDMI_PLL_CFG_2_FREQ_OFFSET_ADJ_MSK	(0x3ffff)	
#define	HDMI_PLL_CFG_2_FREQ_OFFSET_ADJ_BASE	0
/*HDMI_PLL_CFG_3 0x002C	HDMI PLL Control 3 Register */
/*Bit(s) HDMI_PLL_CFG_3_RSRV_31_28 reserved */
/* HDMI DCC_CH_RSTL Config */
#define	HDMI_PLL_CFG_3_DCC_CH_RSTL_MSK	((0x1f) << 23)
#define	HDMI_PLL_CFG_3_DCC_CH_RSTL_BASE	23
/* HDMI PLL_LOCK Config */
#define	HDMI_PLL_CFG_3_PLL_LOCK	(1 << 22)	
/* HDMI DCC_PI_RSTL Config */
#define	HDMI_PLL_CFG_3_DCC_PI_RSTL_MSK	((0x1f) << 17)
#define	HDMI_PLL_CFG_3_DCC_PI_RSTL_BASE	17
/* HDMI PLL hdmi_test_mode Config */
#define	HDMI_PLL_CFG_3_HDMI_TEST_MODE (1 << 16)		
/* HDMI PLL hdmi_pll_fbdiv Config */
#define	HDMI_PLL_CFG_3_HDMI_PLL_FBDIV_MSK	((0x1ff) << 7)
#define	HDMI_PLL_CFG_3_HDMI_PLL_FBDIV_BASE	7
/* HDMI PLL hdmi_pll_refdiv Config */
#define	HDMI_PLL_CFG_3_HDMI_PLL_REFDIV_MSK	((0x1f) << 7)
#define	HDMI_PLL_CFG_3_HDMI_PLL_REFDIV_BASE	2
/* HDMI PLL hdmi_pll_rstb Config */
#define	HDMI_PLL_CFG_3_HDMI_PLL_RSTB	(1 << 1)	
/* HDMI PLL hdmi_pll_on Config */
#define	HDMI_PLL_CFG_3_HDMI_PLL_ON	1		
/*HDTX_ACR_N0	0x0000	ACR N Value Bits [7_0] Register */
#define	HDTX_ACR_N0_ACR_N_7_0_MSK (0xff)	/* ACR_N[7_0] */
#define	HDTX_ACR_N0_ACR_N_7_0_BASE 0
/*HDTX_ACR_N1	0x0001	ACR N Value Bits [15_8] Register */
#define	HDTX_ACR_N1_ACR_N_15_8_MSK (0xff)	/* ACR_N[15_8] */
#define	HDTX_ACR_N1_ACR_N_15_8_BASE 0
/*HDTX_ACR_N2	0x0002	ACR N Value Bits [19_16] Register */
/*Bit(s) HDTX_ACR_N2_RSRV_7_4 reserved */
#define	HDTX_ACR_N2_ACR_N_19_16_MSK (0xf)	/* ACR_N[19_16] */
#define	HDTX_ACR_N2_ACR_N_19_16_BASE 0
/*	
 *HDTX_ACR_CTS0 0x0004	Software-Calculated ACR CTS Value Bits [7_0] Register
 */
#define	HDTX_ACR_CTS0_ACR_CTS_7_0_MSK (0xff)	/* ACR_CTS[7_0] */
#define	HDTX_ACR_CTS0_ACR_CTS_7_0_BASE 0
/*
 * HDTX_ACR_CTS1 0x0005	Software-Calculated ACR CTS Value Bits[15_8] Register
 */
#define	HDTX_ACR_CTS1_ACR_CTS_15_8_MSK (0xff)	/* ACR_CTS[15_8] */
#define	HDTX_ACR_CTS1_ACR_CTS_15_8_BASE 0
/*	
 *HDTX_ACR_CTS2	0x0006	Software-Calculated ACR CTS Value Bits[19_16] Register
 */
/*Bit(s) HDTX_ACR_CTS2_RSRV_7_4 reserved */
#define	HDTX_ACR_CTS2_ACR_CTS_19_16_MSK (0xf)	/* ACR_CTS[19_16] */
#define	HDTX_ACR_CTS2_ACR_CTS_19_16_BASE 0
/*HDTX_ACR_CTRL	0x0007	ACR Control Register */
/*Bit(s) HDTX_ACR_CTRL_RSRV_7_4 reserved */
#define	HDTX_ACR_CTRL_MCLK_SEL_MSK	((0x3) << 2)	/* MCLK Select */
#define	HDTX_ACR_CTRL_MCLK_SEL_BASE	2
#define	HDTX_ACR_CTRL_CTS_SEL	(1 < 1)	/* CTS Select */
#define	HDTX_ACR_CTRL_ACR_EN	1		/* ACR Enable */
/*
 * HDTX_ACR_STS0 0x0008	Hardware Computed ACR CTS Bits [7_0]Register
 */
#define	HDTX_ACR_STS0_ACR_CTS_VAL_7_0_MSK	(0xff)	/* ACR_CTS_VAL[7_0] */
#define	HDTX_ACR_STS0_ACR_CTS_VAL_7_0_BASE	0

/*	HDTX_ACR_STS1			0x0009	Hardware Computed ACR CTS Bits [15_8]
 *									Register
 */
/* ACR_CTS_VAL[15_8] */
#define	HDTX_ACR_STS1_ACR_CTS_VAL_15_8_MSK	(0xff)	
#define	HDTX_ACR_STS1_ACR_CTS_VAL_15_8_BASE	0

/*	HDTX_ACR_STS2			0x000A	Hardware Computed ACR CTS Bits [19_16]
 *									Register
 */
/*		Bit(s) HDTX_ACR_STS2_RSRV_7_4 reserved */
/* ACR_CTS_VAL[19_16] */
#define	HDTX_ACR_STS2_ACR_CTS_VAL_19_16_MSK	(0xf)	
#define	HDTX_ACR_STS2_ACR_CTS_VAL_19_16_BASE	0

/*	HDTX_AUD_CTRL			0x000B	Audio Control Register */
#define	HDTX_AUD_CTRL_VALIDITY	(1 << 7)		/* Validity */
#define	HDTX_AUD_CTRL_I2S_DBG_EN	(1 << 6)		/* I<super 2>S Debug Enable */
#define	HDTX_AUD_CTRL_I2S_MODE	(1 << 5)	/* I<super 2>S Mode */
/* I<super 2>S Channel Enable */
#define	HDTX_AUD_CTRL_I2S_CH_EN_MSK	((0xf) << 1)	
#define	HDTX_AUD_CTRL_I2S_CH_EN_BASE	1
#define	HDTX_AUD_CTRL_AUD_EN	1		/* Audio Enable */

/*	HDTX_I2S_CTRL			0x000C	I<super 2>S Control Register */
/*		Bit(s) HDTX_I2S_CTRL_RSRV_7_5 reserved */
#define	HDTX_I2S_CTRL_I2S_CTL_b4	(1 << 4) /* I<super 2>S Control [4] */
#define	HDTX_I2S_CTRL_I2S_CTL_b3	(1 << 3) /* I<super 2>S Control [3] */
#define	HDTX_I2S_CTRL_I2S_CTL_b2	(1 << 7) /* I<super 2>S Control [2] */
#define	HDTX_I2S_CTRL_I2S_CTL_b1	(1 << 1)		/* I<super 2>S Control [1] */
#define	HDTX_I2S_CTRL_I2S_CTL_b0	1		/* I<super 2>S Control [0] */

/*	HDTX_I2S_DLEN			0x000D	I<super 2>S Valid Data Length Register */
/*		Bit(s) HDTX_I2S_DLEN_RSRV_7_5 reserved */
/* I<super 2>S Data Len [4_0] */
#define	HDTX_I2S_DLEN_I2S_DATALEN_4_0_MSK	(0x1f)	
#define	HDTX_I2S_DLEN_I2S_DATALEN_4_0_BASE	0

/*	HDTX_I2S_DBG_LFT0		0x0010	I<super 2>S Left Channel Debug Bits [7_0]
 *									Register
 */
/* Left Debug [7_0] */
#define	HDTX_I2S_DBG_LFT0_LEFT_DBG_7_0_MSK	(0xff)	
#define	HDTX_I2S_DBG_LFT0_LEFT_DBG_7_0_BASE	0

/*	HDTX_I2S_DBG_LFT1		0x0011	I<super 2>S Left Channel Debug Bits [15_8]
 *									Register
 */
/* Left Debug [15_8] */
#define	HDTX_I2S_DBG_LFT1_LEFT_DBG_15_8_MSK	(0xff)	
#define	HDTX_I2S_DBG_LFT1_LEFT_DBG_15_8_BASE	0

/*	HDTX_I2S_DBG_LFT2		0x0012	I<super 2>S Left Channel Debug Bits
 *									[23_16] Register
 */
/* Left Debug [23_16] */
#define	HDTX_I2S_DBG_LFT2_LEFT_DBG_23_16_MSK	(0xff)	
#define	HDTX_I2S_DBG_LFT2_LEFT_DBG_23_16_BASE	0

/*	HDTX_I2S_DBG_LFT3		0x0013	I<super 2>S Left Channel Debug Bits
 *									[31_24] Register
 */
/* Left Debug [31_24] */
#define	HDTX_I2S_DBG_LFT3_LEFT_DBG_31_24_MSK	(0xff)	
#define	HDTX_I2S_DBG_LFT3_LEFT_DBG_31_24_BASE	0

/*	HDTX_I2S_DBG_RIT0		0x0014	I<super 2>S Right Channel Debug Bits [7_0]
 *									Register
 */
/* Right Debug [7_0] */
#define	HDTX_I2S_DBG_RIT0_RIT_DBG_7_0_MSK	(0xff)	
#define	HDTX_I2S_DBG_RIT0_RIT_DBG_7_0_BASE	0

/*	HDTX_I2S_DBG_RIT1		0x0015	I<super 2>S Right Channel Debug Bits
 *									[15_8] Register
 */
/* Right Debug [15_8] */
#define	HDTX_I2S_DBG_RIT1_RIT_DBG_15_8_MSK	(0xff)	
#define	HDTX_I2S_DBG_RIT1_RIT_DBG_15_8_BASE	0

/*	HDTX_I2S_DBG_RIT2		0x0016	I<super 2>S Right Channel Debug Bits
 *									[23_16] Register
 */
/* Right Debug [23_16] */
#define	HDTX_I2S_DBG_RIT2_RIT_DBG_23_16_MSK	(0xff)	
#define	HDTX_I2S_DBG_RIT2_RIT_DBG_23_16_BASE	0

/*	HDTX_I2S_DBG_RIT3		0x0017	I<super 2>S Right Channel Debug Bits
 *									[31_24] Register
 */
/* Right Debug [31_24] */
#define	HDTX_I2S_DBG_RIT3_RIT_DBG_31_24_MSK	(0xff)	
#define	HDTX_I2S_DBG_RIT3_RIT_DBG_31_24_BASE	0

/*	HDTX_CHSTS_0			0x0018	Channel Status Bits [7_0] Register */
#define	HDTX_CHSTS_0_CHSTS_7_0_MSK	(0xff)	/* Channel Status [7_0] */
#define	HDTX_CHSTS_0_CHSTS_7_0_BASE	0

/*	HDTX_CHSTS_1			0x0019	Channel Status Bits [15_8] Register */
/* Channel Status [15_8] */
#define	HDTX_CHSTS_1_CHSTS_15_8_MSK	(0xff)	
#define	HDTX_CHSTS_1_CHSTS_15_8_BASE	0

/*	HDTX_CHSTS_2			0x001A	Channel Status Bits [23_16] Register */
/* Channel Status [23_16] */
#define	HDTX_CHSTS_2_CHSTS_23_16_MSK	(0xff)	
#define	HDTX_CHSTS_2_CHSTS_23_16_BASE	0

/*	HDTX_CHSTS_3			0x001B	Channel Status Bits [31_24] Register */
/* Channel Status [31_24] */
#define	HDTX_CHSTS_3_CHSTS_31_24_MSK	(0xff)	
#define	HDTX_CHSTS_3_CHSTS_31_24_BASE	0

/*	HDTX_CHSTS_4			0x001C	Channel Status Bits [39_32] Register */
/* Channel Status [40_32] */
#define	HDTX_CHSTS_4_CHSTS_40_32_MSK	(0xff)	
#define	HDTX_CHSTS_4_CHSTS_40_32_BASE	0

/*	HDTX_FIFO_CTRL			0x001D	FIFO Control Register */
/*		Bit(s) HDTX_FIFO_CTRL_RSRV_7_2 reserved */
#define	HDTX_FIFO_CTRL_IRST	(1 << 1)			/* Internal FIFO Reset */
#define	HDTX_FIFO_CTRL_FIFO_RST	1			/* FIFO Reset */

/*	HDTX_MEMSIZE_L			0x001E	FIFO Memory Size Bits [7_0] Register */
/* Memory Size [7_0] */
#define	HDTX_MEMSIZE_L_MEMSIZE_7_0_MSK	(0xff)	
#define	HDTX_MEMSIZE_L_MEMSIZE_7_0_BASE	0

/*	HDTX_MEMSIZE_H			0x001F	FIFO Memory Size Bit [8] Register */
/*		Bit(s) HDTX_MEMSIZE_H_RSRV_7_1 reserved */
#define	HDTX_MEMSIZE_H_MEMSIZE_b8			1			/* Memory Size [8] */

/*	HDTX_GCP_CFG0			0x0020	GCP Packet Configure Register 0 */
/*		Bit(s) HDTX_GCP_CFG0_RSRV_7_4 reserved */
#define	HDTX_GCP_CFG0_PP_SW_VAL	(1 << 3)		/* PP Software Value */
#define	HDTX_GCP_CFG0_DEF_PHASE	(1 << 2)		/* Default Phase */
#define	HDTX_GCP_CFG0_AVMUTE	(1 << 1)		/* AV Mute */
/* GCP Packet Transmission Enable */
#define	HDTX_GCP_CFG0_GCP_EN	1		

/*	HDTX_GCP_CFG1			0x0021	GCP Packet Configure Register 1 */
#define	HDTX_GCP_CFG1_COL_DEPTH_MSK	SHIFT4(0xf)	/* Color Depth */
#define	HDTX_GCP_CFG1_COL_DEPTH_BASE	4
#define	HDTX_GCP_CFG1_PP_3_0_MSK	(0xf)	/* Pixel Packing [3_0] */
#define	HDTX_GCP_CFG1_PP_3_0_BASE	0

/*HDTX_AUD_STS 0x0022	Audio Status Register */
/*Bit(s) HDTX_AUD_STS_RSRV_7_4 reserved */
#define	HDTX_AUD_STS_UNDERFLOW	(1 << 3)		/* Underflow */
#define	HDTX_AUD_STS_OVERFLOW	(1 << 2)		/* Overflow */
/*Bit(s) HDTX_AUD_STS_RSRV_1_0 reserved */
/*HDTX_HTOT_L 0x0024	HTOTAL Bits [7_0] Register */
/* Total Horizontal Pixels per Line [7_0] */
#define	HDTX_HTOT_L_HTOT_7_0_MSK	(0xff)	
#define	HDTX_HTOT_L_HTOT_7_0_BASE	0

/* HDTX_HTOT_H 0x0025	HTOTAL Bits [15_8] Register */
/* Total Horizontal Pixels per Line [15_8] */
#define	HDTX_HTOT_H_HTOT_15_8_MSK	(0xff)	
#define	HDTX_HTOT_H_HTOT_15_8_BASE	0

/* HDTX_HBLANK_L 0x0026	HBLANK Bits [7_0] Register */
/* Horizontal Blanking Period [7_0] */
#define	HDTX_HBLANK_L_HBLANK_7_0_MSK	(0xff)	
#define	HDTX_HBLANK_L_HBLANK_7_0_BASE	0

/* HDTX_HBLANK_H 0x0027	HBLANK Bits [15_8] Register */
/* Horizontal Blanking Period [15_8] */
#define	HDTX_HBLANK_H_HBLANK_15_8_MSK	(0xff)	
#define	HDTX_HBLANK_H_HBLANK_15_8_BASE		0

/* HDTX_VTOT_L 0x0028	VTOTAL Bits [7_0] Register */
/* Vertical Resolution [7_0] */
#define	HDTX_VTOT_L_VTOT_7_0_MSK	(0xff)	
#define	HDTX_VTOT_L_VTOT_7_0_BASE	0

/* HDTX_VTOT_H 0x0029	VTOTAL Bits [15_8] Register */
/* Vertical Resolution [15_8] */
#define	HDTX_VTOT_H_VTOT_15_8_MSK	(0xff)	
#define	HDTX_VTOT_H_VTOT_15_8_BASE	0

/* HDTX_VRES_L 0x002A	VRES Bits [7_0] Register */
/* Active Vertical Resolution [7_0] */
#define	HDTX_VRES_L_VRES_7_0_MSK	(0xff)	
#define	HDTX_VRES_L_VRES_7_0_BASE	0

/* HDTX_VRES_H 0x002B VRES Bits [15_8] Register */
/* Active Vertical Resolution [15_8] */
#define	HDTX_VRES_H_VRES_15_8_MSK	(0xff)	
#define	HDTX_VRES_H_VRES_15_8_BASE	0

/* HDTX_VSTART_L 0x002C	VSTART Bits [7_0] Register */
/* First Active Line of Frame [7_0] */
#define	HDTX_VSTART_L_VSTART_7_0_MSK	(0xff)	
#define	HDTX_VSTART_L_VSTART_7_0_BASE	0

/* HDTX_VSTART_H 0x002D	VSTART Bits [15_8] Register */
/* First Active Line of Frame [15_8] */
#define	HDTX_VSTART_H_VSTART15_8_MSK	(0xff)	
#define	HDTX_VSTART_H_VSTART15_8_BASE	0

/* HDTX_HTOT_STS_L 0x002E HTOTAL Bits [7_0] Register */
/* Total Horizontal Pixels per Line [7_0] */
#define	HDTX_HTOT_STS_L_HTOT_VAL_7_0_MSK	(0xff)	
#define	HDTX_HTOT_STS_L_HTOT_VAL_7_0_BASE	0

/* HDTX_HTOT_STS_H 0x002F	HTOTAL Bits [15_8] Register */
/* Total Horizontal Pixels per Line [15_8] */
#define	HDTX_HTOT_STS_H_HTOT_VAL_15_8_MSK	(0xff)	
#define	HDTX_HTOT_STS_H_HTOT_VAL_15_8_BASE	0

/* HDTX_HBLANK_STS_L 0x0030 HBLANK Bits [7_0] Register */
/* Horizontal Blanking Period [7_0] */
#define	HDTX_HBLANK_STS_L_HBLANK_VAL_7_0_MSK	(0xff)	
#define	HDTX_HBLANK_STS_L_HBLANK_VAL_7_0_BASE	0

/* HDTX_HBLANK_STS_H 0x0031 HBLANK Bits [15_8] Register */
/* Horizontal Blanking Period [15_8] */
#define	HDTX_HBLANK_STS_H_HBLANK_VAL_15_8_MSK	(0xff)	
#define	HDTX_HBLANK_STS_H_HBLANK_VAL_15_8_BASE	0

/* HDTX_VTOT_STS_L 0x0032 VTOTAL Bits [7_0] Register */
/* Vertical Resolution of Frame [7_0] */
#define	HDTX_VTOT_STS_L_VTOT_VAL_7_0_MSK	(0xff)	
#define	HDTX_VTOT_STS_L_VTOT_VAL_7_0_BASE	0

/* HDTX_VTOT_STS_H 0x0033 VTOTAL Bits [15_8] Register */
/* Vertical Resolution of Frame [15_8] */
#define	HDTX_VTOT_STS_H_VTOT_VAL_15_8_MSK	(0xff)	
#define	HDTX_VTOT_STS_H_VTOT_VAL_15_8_BASE	0

/* HDTX_VRES_STS_L 0x0034 VRES Bits [7_0] Register */
/* Active Vertical Resolution of Frame [7_0] */
#define	HDTX_VRES_STS_L_VRES_VAL_7_0_MSK	(0xff)	
#define	HDTX_VRES_STS_L_VRES_VAL_7_0_BASE	0

/* HDTX_VRES_STS_H 0x0035 VRES Bits [15_8] Register */
/* Active Vertical Resolution of Frame [15_8] */
#define	HDTX_VRES_STS_H_VRES_VAL_15_8_MSK	(0xff)	
#define	HDTX_VRES_STS_H_VRES_VAL_15_8_BASE	0

/* HDTX_VSTART_STS_L 0x0036 VSTART Bits [7_0] Register */
/* First Active Line of Frame [7_0] */
#define	HDTX_VSTART_STS_L_VSTART_VAL_7_0_MSK	(0xff)	
#define	HDTX_VSTART_STS_L_VSTART_VAL_7_0_BASE	0

/* HDTX_VSTART_STS_H 0x0037 VSTART Bits [15_8] Register */
/* First Active Line of Frame [15_8] */
#define	HDTX_VSTART_STS_H_VSTART_VAL_15_8_MSK	(0xff)	
#define	HDTX_VSTART_STS_H_VSTART_VAL_15_8_BASE	0

/* HDTX_VIDEO_STS 0x0038 Video Status Register */
/* Bit(s) HDTX_VIDEO_STS_RSRV_7_1 reserved */
#define	HDTX_VIDEO_STS_INIT_OVER	1	/* Init Over */

/* HDTX_VIDEO_CTRL 0x0039 Video Control Register */
/* Bit(s) HDTX_VIDEO_CTRL_RSRV_7 reserved */
/* Internal Logic Video Format Detect */
#define	HDTX_VIDEO_CTRL_INT_FRM_SEL	(1 << 6)		
/* Bit(s) HDTX_VIDEO_CTRL_RSRV_5_4 reserved */
#define	HDTX_VIDEO_CTRL_ACR_PRI_SEL_I	(1 << 3)		/* ACR priority selection */
#define	HDTX_VIDEO_CTRL_DEBUG_CTRL	(1 << 2)		/* Debug control */
#define	HDTX_VIDEO_CTRL_FLD_POL	(1 << 1)		/* Filed polarity */
#define	HDTX_VIDEO_CTRL_IN_YC	1		/* Input Video YC */

/* HDTX_HDMI_CTRL 0x003A HDMI Control Register */
/* Bit(s) HDTX_HDMI_CTRL_RSRV_7 reserved */
#define	HDTX_HDMI_CTRL_PIX_RPT_MSK	SHIFT4(0x7)	/* Pixel Repetition Values */
#define	HDTX_HDMI_CTRL_PIX_RPT_BASE	4
#define	HDTX_HDMI_CTRL_BCH_ROT	(1 << 2)		/* BCH Value */
#define	HDTX_HDMI_CTRL_LAYOUT	(1 << 1)		/* Layout */
#define	HDTX_HDMI_CTRL_HDMI_MODE	1		/* HDMI Mode */

/* HDTX_PP_HW 0x0046 Hardware Computed Pixel Packing Value Register */
/* Bit(s) HDTX_PP_HW_RSRV_7_4 reserved */
#define	HDTX_PP_HW_PP_REG_MSK	(0xf)	/* Pixel Packing Phase */
#define	HDTX_PP_HW_PP_REG_BASE	0

/* HDTX_DC_FIFO_SFT_RST	0x0047	Deep Color FIFO Soft Reset Register */
/* Bit(s) HDTX_DC_FIFO_SFT_RST_RSRV_7_1 reserved */
/* FIFO Soft Reset */
#define	HDTX_DC_FIFO_SFT_RST_FIFO_SFT_RST_REG	1			

/* HDTX_DC_FIFO_WR_PTR 0x0048 FIFO Write Pointer Register */
/* Bit(s) HDTX_DC_FIFO_WR_PTR_RSRV_7_6 reserved */
/* FIFO Write Pointer */
#define	HDTX_DC_FIFO_WR_PTR_FIFO_WR_PTR__MSK	(0x3f)	
#define	HDTX_DC_FIFO_WR_PTR_FIFO_WR_PTR__BASE	0

/* HDTX_DC_FIFO_RD_PTR 0x0049 FIFO Read Pointer Register */
/* Bit(s) HDTX_DC_FIFO_RD_PTR_RSRV_7_6 reserved */
/* FIFO Read Pointer */
#define	HDTX_DC_FIFO_RD_PTR_FIFO_RD_PTR_REG_MSK	(0x3f)	
#define	HDTX_DC_FIFO_RD_PTR_FIFO_RD_PTR_REG_BASE	0

/* HDTX_TDATA0_0 0x004C	Test Data [7_0] for TMDS Channel 0 Register */
#define	HDTX_TDATA0_0_TDATA0_REG_7_0_MSK	(0xff)	/* TDATA0 [7_0] */
#define	HDTX_TDATA0_0_TDATA0_REG_7_0_BASE	0

/* HDTX_TDATA0_1 0x004D	Test Data [15_8] for TMDS Channel 0 Register */
#define	HDTX_TDATA0_1_TDATA0_REG_15_8_MSK	(0xff)	/* TDATA0 [15_8] */
#define	HDTX_TDATA0_1_TDATA0_REG_15_8_BASE	0

/* HDTX_TDATA0_2 0x004E	Test Data [19_16] for TMDS Channel 0 Register */
/* Bit(s) HDTX_TDATA0_2_RSRV_7_4 reserved */
#define	HDTX_TDATA0_2_TDATA0_REG_19_16_MSK	(0xf)	/* TDATA0 [19_16] */
#define	HDTX_TDATA0_2_TDATA0_REG_19_16_BASE	0

/* HDTX_TDATA1_0 0x0050	Test Data [7_0] for TMDS Channel 1 Register */
#define	HDTX_TDATA1_0_TDATA1_REG_7_0_MSK	(0xff)	/* TDATA1 [7_0] */
#define	HDTX_TDATA1_0_TDATA1_REG_7_0_BASE	0

/* HDTX_TDATA1_1 0x0051	Test Data [15_8] for TMDS Channel 1 Register */
#define	HDTX_TDATA1_1_TDATA1_REG_15_8_MSK	(0xff)	/* TDATA1 [15_8] */
#define	HDTX_TDATA1_1_TDATA1_REG_15_8_BASE	0

/* HDTX_TDATA1_2 0x0052	Test Data [19_16] for TMDS Channel 1 Register */
/* Bit(s) HDTX_TDATA1_2_RSRV_7_4 reserved */
#define	HDTX_TDATA1_2_TDATA1_REG_19_16_MSK	(0xf)	/* TDATA1 [19_16] */
#define	HDTX_TDATA1_2_TDATA1_REG_19_16_BASE	0

/* HDTX_TDATA2_0 0x0054	Test Data [7_0] for TMDS Channel 2 Register */
#define	HDTX_TDATA2_0_TDATA2_REG_7_0_MSK	(0xff)	/* TDATA2 [7_0] */
#define	HDTX_TDATA2_0_TDATA2_REG_7_0_BASE	0

/* HDTX_TDATA2_1 0x0055 Test Data [15_8] for TMDS Channel 2 Register */
#define	HDTX_TDATA2_1_TDATA2_REG_15_8_MSK	(0xff)	/* TDATA2 [15_8] */
#define	HDTX_TDATA2_1_TDATA2_REG_15_8_BASE	0

/* HDTX_TDATA2_2 0x0056	Test Data [19_16] for TMDS Channel 2 Register */
/* Bit(s) HDTX_TDATA2_2_RSRV_7_4 reserved */
#define	HDTX_TDATA2_2_TDATA2_REG_19_16_MSK	(0xf)	/* TDATA2 [19_16] */
#define	HDTX_TDATA2_2_TDATA2_REG_19_16_BASE	0

/* HDTX_TDATA3_0 0x0058	Test Data [7_0] for TMDS Channel 3 Register */
#define	HDTX_TDATA3_0_TDATA3_REG_7_0_MSK	(0xff)	/* TDATA3 [7_0] */
#define	HDTX_TDATA3_0_TDATA3_REG_7_0_BASE	0

/* HDTX_TDATA3_1 0x0059 Test Data [15_8] for TMDS Channel 3 Register */
#define	HDTX_TDATA3_1_TDATA3_REG_15_8_MSK	(0xff)	/* TDATA3 [15_8] */
#define	HDTX_TDATA3_1_TDATA3_REG_15_8_BASE	0

/* HDTX_TDATA3_2 0x005A	Test Data [19_16] for TMDS Channel 3 Register */
/* Bit(s) HDTX_TDATA3_2_RSRV_7_4 reserved */
#define	HDTX_TDATA3_2_TDATA3_REG_19_16_MSK	(0xf)	/* TDATA3 [19_16] */
#define	HDTX_TDATA3_2_TDATA3_REG_19_16_BASE	0

/* HDTX_TDATA_SEL 0x005B Test Data Selection Control Register */
/* Bit(s) HDTX_TDATA_SEL_RSRV_7 reserved */
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_b6	(1 << 6)		/* TDATA Select [6] */
/* TDATA Select [5_4] */
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_5_4_MSK	((0x3) << 4)
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_5_4_BASE	4
/* TDATA Select [3_2] */
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_3_2_MSK	((0x3) << 2)
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_3_2_BASE	2
/* TDATA Select [1_0] */
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_1_0_MSK	(0x3)	
#define	HDTX_TDATA_SEL_TDATA_SEL_REG_1_0_BASE	0

/* HDTX_SWAP_CTRL 0x005C	TMDS Data Swap Control Register */
/* Bit(s) HDTX_SWAP_CTRL_RSRV_7_2 reserved */
#define	HDTX_SWAP_CTRL_CHANNEL_SWAP	(1 << 1)			/* Channel Swap */
#define	HDTX_SWAP_CTRL_BIT_SWAP	1			/* Bit Swap */

/* HDTX_AVMUTE_CTRL 0x005D AVMUTE Control Register */
/* Bit(s) HDTX_AVMUTE_CTRL_RSRV_7_2 reserved */
#define	HDTX_AVMUTE_CTRL_AVMUTE_CTRL_REG_MSK	(0x3)		/* Audio Mute */
#define	HDTX_AVMUTE_CTRL_AVMUTE_CTRL_REG_BASE	0

/* HDTX_HOST_PKT_CTRL0 0x005E Host Packet Control Register 0 */
/* Bit(s) HDTX_HOST_PKT_CTRL0_RSRV_7_6 reserved */
#define	HDTX_HOST_PKT_CTRL0_PKT5_EN	(1 << 5)		/* Packet Type 5 Enable */
#define	HDTX_HOST_PKT_CTRL0_PKT4_EN	(1 << 4)		/* Packet Type 4 Enable */
#define	HDTX_HOST_PKT_CTRL0_PKT3_EN	(1 << 3)		/* Packet Type 3 Enable */
#define	HDTX_HOST_PKT_CTRL0_PKT2_EN	(1 << 2)		/* Packet Type 2 Enable */
#define	HDTX_HOST_PKT_CTRL0_PKT1_EN	(1 << 1)		/* Packet Type 1 Enable */
#define	HDTX_HOST_PKT_CTRL0_PKT0_EN	1		/* Packet Type 0 Enable */

/* HDTX_HOST_PKT_CTRL1 0x005F Host Packet Control Register 1 */
/* Bit(s) HDTX_HOST_PKT_CTRL1_RSRV_7_6 reserved */
/* Packet Type 5 Tx Mode */
#define	HDTX_HOST_PKT_CTRL1_PKT5_TX_MODE	(1 << 5)		
/* Packet Type 4 Tx Mode */
#define	HDTX_HOST_PKT_CTRL1_PKT4_TX_MODE	(1 << 4)		
/* Packet Type 3 Tx Mode */
#define	HDTX_HOST_PKT_CTRL1_PKT3_TX_MODE	(1 << 3)		
/* Packet Type 2 Tx Mode */
#define	HDTX_HOST_PKT_CTRL1_PKT2_TX_MODE	(1 << 2)		
/* Packet Type 1 Tx Mode */
#define	HDTX_HOST_PKT_CTRL1_PKT1_TX_MODE	(1 << 1)		
/* Packet Type 0 Tx Mode */
#define	HDTX_HOST_PKT_CTRL1_PKT0_TX_MODE	1		

/* HDTX_PKT0_BYTE0_30 0x0060 PKT0 Byte 0 to 30 Registers */
#define	HDTX_PKT0_BYTE0_30_PKT0_255_0_MSK	(0xff)	/* PKT0 [255_0] */
#define	HDTX_PKT0_BYTE0_30_PKT0_255_0_BASE	0

/* HDTX_PKT1_BYTE0_30 0x0080	PKT1 Byte 0 to 30 Registers */
#define	HDTX_PKT1_BYTE0_30_PKT1_255_0_MSK	(0xff)	/* PKT1 [255_0] */
#define	HDTX_PKT1_BYTE0_30_PKT1_255_0_BASE	0

/* HDTX_PKT2_BYTE0_30 0x00A0	PKT2 Byte 0 to 30 Registers */
#define	HDTX_PKT2_BYTE0_30_PKT2_255_0_MSK	(0xff)	/* PKT2 [255_0] */
#define	HDTX_PKT2_BYTE0_30_PKT2_255_0_BASE	0

/* HDTX_PKT3_BYTE0_30 0x00C0 PKT3 Byte 0 to 30 Registers */
#define	HDTX_PKT3_BYTE0_30_PKT3_255_0_MSK	(0xff)	/* PKT3 [255_0] */
#define	HDTX_PKT3_BYTE0_30_PKT3_255_0_BASE	0

/* HDTX_PKT4_BYTE0_30 0x00E0 PKT4 Byte 0 to 30 Registers */
#define	HDTX_PKT4_BYTE0_30_PKT4_255_0_MSK	(0xff)	/* PKT4 [255_0] */
#define	HDTX_PKT4_BYTE0_30_PKT4_255_0_BASE	0

/* HDTX_PKT5_BYTE0_30 0x0100	PKT5 Byte 0 to 30 Registers */
#define	HDTX_PKT5_BYTE0_30_PKT5_255_0_MSK	(0xff)	/* PKT5 [255_0] */
#define	HDTX_PKT5_BYTE0_30_PKT5_255_0_BASE	0

/* HDTX_UBITS_0 0x0120 User Data Bits of SPDIF Header Register 0 */
/* User Data Bits of SPDIF Header [7_0] */
#define	HDTX_UBITS_0_UBITS_7_0_MSK	(0xff)	
#define	HDTX_UBITS_0_UBITS_7_0_BASE	0

/* HDTX_UBITS_1 0x0121 User Data Bits of SPDIF Header Register 1 */
/* User Data Bits of SPDIF Header [15_8] */
#define	HDTX_UBITS_1_UBITS_15_8_MSK	(0xff)	
#define	HDTX_UBITS_1_UBITS_15_8_BASE	0

/* HDTX_UBITS_2	0x0122 User Data Bits of SPDIF Header Register 2 */
/* User Data Bits of SPDIF Header [23_16] */
#define	HDTX_UBITS_2_UBITS_23_16_MSK	(0xff)	
#define	HDTX_UBITS_2_UBITS_23_16_BASE	0

/* HDTX_UBITS_3 0x0123 User Data Bits of SPDIF Header Register 3 */
/* User Data Bits of SPDIF Header [31_24] */
#define	HDTX_UBITS_3_UBITS_31_24_MSK	(0xff)	
#define	HDTX_UBITS_3_UBITS_31_24_BASE	0

/* HDTX_UBITS_4	0x0124 User Data Bits of SPDIF Header Register 4 */
/* User Data Bits of SPDIF Header [39_32] */
#define	HDTX_UBITS_4_U_BITS_39_32_MSK	(0xff)	
#define	HDTX_UBITS_4_U_BITS_39_32_BASE	0

/* HDTX_UBITS_5 0x0125 User Data Bits of SPDIF Header Register 5 */
/* User Data Bits of SPDIF Header [47_40] */
#define	HDTX_UBITS_5_UBITS_47_40_MSK	(0xff)	
#define	HDTX_UBITS_5_UBITS_47_40_BASE	0

/* HDTX_UBITS_6	0x0126	User Data Bits of SPDIF Header Register 6 */
/* User Data Bits of SPDIF Header [55_48] */
#define	HDTX_UBITS_6_UBITS_55_48_MSK	(0xff)	
#define	HDTX_UBITS_6_UBITS_55_48_BASE	0

/* HDTX_UBITS_7 0x0127 User Data Bits of SPDIF Header Register 7 */
/* User Data Bits of SPDIF Header [63_56] */
#define	HDTX_UBITS_7_UBITS_63_56_MSK	(0xff)	
#define	HDTX_UBITS_7_UBITS_63_56_BASE	0

/* HDTX_UBITS_8	0x0128	User Data Bits of SPDIF Header Register 8 */
/* User Data Bits of SPDIF Header [71_64] */
#define	HDTX_UBITS_8_UBITS_71_64_MSK	(0xff)	
#define	HDTX_UBITS_8_UBITS_71_64_BASE	0

/* HDTX_UBITS_9	0x0129 User Data Bits of SPDIF Header Register 9 */
/* User Data Bits of SPDIF Header [79_72] */
#define	HDTX_UBITS_9_UBITS_79_72_MSK	(0xff)	
#define	HDTX_UBITS_9_UBITS_79_72_BASE	0

/* HDTX_UBITS_10 0x012A User Data Bits of SPDIF Header Register 10 */
/* User Data Bits of SPDIF Header [87_80] */
#define	HDTX_UBITS_10_UBITS_87_80_MSK	(0xff)	
#define	HDTX_UBITS_10_UBITS_87_80_BASE	0

/* HDTX_UBITS_11 0x012B	User Data Bits of SPDIF Header Register 11 */
/* User Data Bits of SPDIF Header [95_88] */
#define	HDTX_UBITS_11_UBITS_95_88_MSK	(0xff)	
#define	HDTX_UBITS_11_UBITS_95_88_BASE	0

/* HDTX_UBITS_12 0x012C User Data Bits of SPDIF Header Register 12 */
/* User Data Bits of SPDIF Header [103_96] */
#define	HDTX_UBITS_12_UBITS_103_96_MSK	(0xff)	
#define	HDTX_UBITS_12_UBITS_103_96_BASE	0

/* HDTX_UBITS_13 0x012D	User Data Bits of SPDIF Header Register 13 */
/* MSB 4 Bits [107_104] of SPDIF Header */
#define	HDTX_UBITS_13_UBITS_107_104_MSK	(0xff)	
#define	HDTX_UBITS_13_UBITS_107_104_BASE	0

/* HDTX_HBR_PKT 0x012E HBR Packet Transmission Control Register */
/* Bit(s) HDTX_HBR_PKT_RSRV_7_1 reserved */
/* Audio Sample/HBR Packet Transmission */
#define	HDTX_HBR_PKT_HDTX_HBR_PKT	1			

/* HDTX_PHY_FIFO_SOFT_RST 0x013 HDMI Tx PHY FIFO Soft Reset Register */
/* Bit(s) HDTX_PHY_FIFO_SOFT_RST_RSRV_7_1 reserved */
/* PHY FIFO Soft Reset */
#define	HDTX_PHY_FIFO_SOFT_RST_HDTX_PHY_FIFO_SOFT_RST	1			

/* HDTX_PHY_FIFO_PTRS 0x0131 HDMITX PHY FIFO Read and Write Pointers Register */
/* LSB 4 Bits [7_4] for Putting Read Pointer */
#define	HDTX_PHY_FIFO_PTRS_HDTX_PHY_FIFO_RD_PTRS_7_4_MSK	((0xf) << 4)
#define	HDTX_PHY_FIFO_PTRS_HDTX_PHY_FIFO_RD_PTRS_7_4_BASE	4
/* MSB 4 Bits [3_0] for Putting Write Pointer */
#define	HDTX_PHY_FIFO_PTRS_HDTX_PHY_FIFO_WR_PTRS_3_0_MSK	(0xf)	
#define	HDTX_PHY_FIFO_PTRS_HDTX_PHY_FIFO_WR_PTRS_3_0_BASE	0

/* HDTX_PRBS_CTRL0 0x0132 PRBS Control Register */
/* Bit(s) HDTX_PRBS_CTRL0_RSRV_7_3 reserved */
#define	HDTX_PRBS_CTRL0_PRBS_EN	(1 << 2)	/* PRBS Enable */
#define	HDTX_PRBS_CTRL0_PRBS_TYPE_SEL_1_0_MSK	(0x3)	/* PRBS Control */
#define	HDTX_PRBS_CTRL0_PRBS_TYPE_SEL_1_0_BASE	0

/* HDTX_HST_PKT_CTRL2 0x0133 Host Packet Control Register 2 */
/* Bit(s) HDTX_HST_PKT_CTRL2_RSRV_7_1 reserved */
/* Host Packet Control 2 */
#define	HDTX_HST_PKT_CTRL2_HOST_PKT_CTRL2	1			

/* HDTX_HST_PKT_START 0x0134 Host Packet Transmission Control During VBI Register */
/* Bit(s) HDTX_HST_PKT_START_RSRV_7_1 reserved */
/* Host Packet Start */
#define	HDTX_HST_PKT_START_HOST_PKT_START	1			

/* HDTX_AUD_CH1_SEL 0x0137 Audio Channel 1 Select Register */
/* Bit(s) HDTX_AUD_CH1_SEL_RSRV_7_3 reserved */
/* Audio Channel1 select */
#define	HDTX_AUD_CH1_SEL_AUD_CH1_SEL_MSK	(0x7)		
#define	HDTX_AUD_CH1_SEL_AUD_CH1_SEL_BASE	0

/* HDTX_AUD_CH2_SEL 0x0138 Audio Channel 2 Select Register */
/* Bit(s) HDTX_AUD_CH2_SEL_RSRV_7_3 reserved */
/* Audio Channel2 select */
#define	HDTX_AUD_CH2_SEL_AUD_CH2_SEL_MSK	(0x7)		
#define	HDTX_AUD_CH2_SEL_AUD_CH2_SEL_BASE	0

/* HDTX_AUD_CH3_SEL 0x0139 Audio Channel 3 Select Register */
/* Bit(s) HDTX_AUD_CH3_SEL_RSRV_7_3 reserved */
/* Audio Channel3 select */
#define	HDTX_AUD_CH3_SEL_AUD_CH3_SEL_MSK	(0x7)		
#define	HDTX_AUD_CH3_SEL_AUD_CH3_SEL_BASE	0

/* HDTX_AUD_CH4_SEL 0x013A Audio Channel 4 Select Register */
/* Bit(s) HDTX_AUD_CH4_SEL_RSRV_7_3 reserved */
/* Audio Channel4 select */
#define	HDTX_AUD_CH4_SEL_AUD_CH4_SEL_MSK	(0x7)		
#define	HDTX_AUD_CH4_SEL_AUD_CH4_SEL_BASE	0

/* HDTX_AUD_CH5_SEL 0x013B Audio Channel 5 Select Register */
/* Bit(s) HDTX_AUD_CH5_SEL_RSRV_7_3 reserved */
/* Audio Channel5 select */
#define	HDTX_AUD_CH5_SEL_AUD_CH5_SEL_MSK	(0x7)		
#define	HDTX_AUD_CH5_SEL_AUD_CH5_SEL_BASE	0

/* HDTX_AUD_CH6_SEL 0x013C Audio Channel 6 Select Register */
/* Bit(s) HDTX_AUD_CH6_SEL_RSRV_7_3 reserved */
/* Audio Channel6 select */
#define	HDTX_AUD_CH6_SEL_AUD_CH6_SEL_MSK	(0x7)		
#define	HDTX_AUD_CH6_SEL_AUD_CH6_SEL_BASE	0

/* HDTX_AUD_CH7_SEL 0x013D Audio Channel 7 Select Register */
/* Bit(s) HDTX_AUD_CH7_SEL_RSRV_7_3 reserved */
/* Audio Channel7 select */
#define	HDTX_AUD_CH7_SEL_AUD_CH7_SEL_MSK	(0x7)		
#define	HDTX_AUD_CH7_SEL_AUD_CH7_SEL_BASE	0

/* HDTX_AUD_CH8_SEL 0x013E Audio Channel 8 Select Register */
/* Bit(s) HDTX_AUD_CH8_SEL_RSRV_7_3 reserved */
/* Audio Channel8 select */
#define	HDTX_AUD_CH8_SEL_AUD_CH8_SEL_MSK	(0x7)		
#define	HDTX_AUD_CH8_SEL_AUD_CH8_SEL_BASE	0

/* TX_HDCP_AKEY0_BYTE_0 0x1200 AKEY0 [7_0] Register */
#define	TX_HDCP_AKEY0_BYTE_0_AKEY0_7_0_MSK	(0xff)	/* AKEY0 [7_0] */
#define	TX_HDCP_AKEY0_BYTE_0_AKEY0_7_0_BASE	0

/* TX_HDCP_AKEY0_BYTE_1 0x1201 AKEY0 [15_8] Register */
#define	TX_HDCP_AKEY0_BYTE_1_AKEY0_15_8_MSK	(0xff)	/* AKEY0 [15_8] */
#define	TX_HDCP_AKEY0_BYTE_1_AKEY0_15_8_BASE	0

/* TX_HDCP_AKEY0_BYTE_2	0x1202 AKEY0 [23_16] Register */
/* AKEY0 [23_16] */
#define	TX_HDCP_AKEY0_BYTE_2_AKEY0_23_16_MSK	(0xff)	
#define	TX_HDCP_AKEY0_BYTE_2_AKEY0_23_16_BASE	0

/* TX_HDCP_AKEY0_BYTE_3	0x1203 AKEY0 [31_24] Register */
/* AKEY0 [31_24] */
#define	TX_HDCP_AKEY0_BYTE_3_AKEY0_31_24_MSK	(0xff)	
#define	TX_HDCP_AKEY0_BYTE_3_AKEY0_31_24_BASE	0

/* TX_HDCP_AKEY0_BYTE_4	0x1204 AKEY0 [39_32] Register */
/* AKEY0 [39_32] */
#define	TX_HDCP_AKEY0_BYTE_4_AKEY0_39_32_MSK	(0xff)	
#define	TX_HDCP_AKEY0_BYTE_4_AKEY0_39_32_BASE	0

/* TX_HDCP_AKEY0_BYTE_5	0x1205 AKEY0 [47_40] Register */
/* AKEY0 [47_40] */
#define	TX_HDCP_AKEY0_BYTE_5_AKEY0_47_40_MSK	(0xff)	
#define	TX_HDCP_AKEY0_BYTE_5_AKEY0_47_40_BASE	0

/* TX_HDCP_AKEY0_BYTE_6	0x1206 AKEY0 [55_48] Register */
/* AKEY0 [55_48] */
#define	TX_HDCP_AKEY0_BYTE_6_AKEY0_55_48_MSK	(0xff)	
#define	TX_HDCP_AKEY0_BYTE_6_AKEY0_55_48_BASE	0

/* TX_HDCP_AKSV_BYTE_0 0x1340 AKSV [7_0] Register */
#define	TX_HDCP_AKSV_BYTE_0_AKSV0_7_0_MSK	(0xff)	/* AKSV0 [7_0] */
#define	TX_HDCP_AKSV_BYTE_0_AKSV0_7_0_BASE	0

/* TX_HDCP_AKSV_BYTE_1 0x1341 AKSV [15_8] Register */
#define	TX_HDCP_AKSV_BYTE_1_AKSV0_15_8_MSK	(0xff)	/* AKSV0 [15_8] */
#define	TX_HDCP_AKSV_BYTE_1_AKSV0_15_8_BASE	0

/* TX_HDCP_AKSV_BYTE_2 0x1342 AKSV [23_16] Register */
/* AKSV0 [23_16] */
#define	TX_HDCP_AKSV_BYTE_2_AKSV0_23_16_MSK	(0xff)	
#define	TX_HDCP_AKSV_BYTE_2_AKSV0_23_16_BASE	0

/* TX_HDCP_AKSV_BYTE_3 0x1343 AKSV [31_24] Register */
/* AKSV0 [31_24] */
#define	TX_HDCP_AKSV_BYTE_3_AKSV0_31_24_MSK	(0xff)	
#define	TX_HDCP_AKSV_BYTE_3_AKSV0_31_24_BASE	0

/* TX_HDCP_AKSV_BYTE_4 0x1344 AKSV [39_32] Register */
/* AKSV0 [39_32] */
#define	TX_HDCP_AKSV_BYTE_4_AKSV0_39_32_MSK	(0xff)	
#define	TX_HDCP_AKSV_BYTE_4_AKSV0_39_32_BASE	0

/* TX_HDCP_CONTROL 0x1350 HDCP Control Register */
#define	TX_HDCP_CONTROL_HDMI_MODE	(1 << 7)	/* HDMI Mode */
#define	TX_HDCP_CONTROL_EESS_EN	(1 << 6)	/* EESS Enable */
#define	TX_HDCP_CONTROL_REPEATER	(1 << 5)	/* Repeater */
#define	TX_HDCP_CONTROL_ADVANCE_CIPHER	(1 << 4)	/* Advance Cipher */
/* Enhanced Link Verification */
#define	TX_HDCP_CONTROL_ENH_LINK_VERIFICATION	(1 << 3)	
#define	TX_HDCP_CONTROL_CIPHER_EN	(1 << 2)	/* Cipher Enable */
#define	TX_HDCP_CONTROL_READ_AKSV	(1 << 1)	/* Read AKSV */
/* Bit(s) TX_HDCP_CONTROL_RSRV_0 reserved */

/* TX_HDCP_STATUS_1 0x1351 HDCP Status Register 1 */
#define	TX_HDCP_STATUS_1_BKSV_READY	(1 << 7)	/* BKSV Ready */
/* R0 Authentication Passed */
#define	TX_HDCP_STATUS_1_R0_AUTH_PASS	(1 << 6)	
/* KSV List Check Passed */
#define	TX_HDCP_STATUS_1_KSV_LIST_CHECK_PASS	(1 << 5)	
#define	TX_HDCP_STATUS_1_ERR	(1 << 4)	/* Error */
#define	TX_HDCP_STATUS_1_FW_ENC_EN	(1 << 3)	/* FW Encryption Enable */
#define	TX_HDCP_STATUS_1_READY_KEYS	(1 << 2)	/* Ready Keys */
#define	TX_HDCP_STATUS_1_AKSV_SENT	(1 << 1)	/* AKSV Sent */
/* Bit(s) TX_HDCP_STATUS_1_RSRV_0 reserved */

/* TX_HDCP_STATUS_2 0x1352 HDCP Status Register 2 */
/* Video Mode [1_0] */
#define	TX_HDCP_STATUS_2_VIDEO_MODE_1_0_MSK	((0x3) << 6)		
#define	TX_HDCP_STATUS_2_VIDEO_MODE_1_0_BASE	6
/* Bit(s) TX_HDCP_STATUS_2_RSRV_5_0 reserved */

/* TX_HDCP_INTR_0 0x1353 HDCP Interrupt Register */
#define	TX_HDCP_INTR_0_AKSV_READY_STATUS	(1 << 7)		/* AKSV Ready Status */
#define	TX_HDCP_INTR_0_TX_AN_READY_STATUS	(1 << 6)		/* Tx An Ready Status */
#define	TX_HDCP_INTR_0_TX_R0_READY_STATUS	(1 << 5)		/* Tx R0 Ready Status */
#define	TX_HDCP_INTR_0_TX_RI_READY_STATUS	(1 << 4)		/* Tx Ri Ready Status */
#define	TX_HDCP_INTR_0_TX_PJ_READY_STATUS	(1 << 3)		/* Tx Pj Ready Status */
/* Bit(s) TX_HDCP_INTR_0_RSRV_2_0 reserved */

/* TX_HDCP_TX_AKSV_0 0x1354 TX KSV Byte 0 Register */
/* TX_AKSV0 [7_0] */
#define	TX_HDCP_TX_AKSV_0_TX_AKSV0_7_0_MSK	(0xff)	
#define	TX_HDCP_TX_AKSV_0_TX_AKSV0_7_0_BASE	0

/* TX_HDCP_TX_AKSV_1 0x1355 TX KSV Byte 1 Register */
/* TX_AKSV0 [15_8] */
#define	TX_HDCP_TX_AKSV_1_TX_AKSV0_15_8_MSK	(0xff)	
#define	TX_HDCP_TX_AKSV_1_TX_AKSV0_15_8_BASE	0

/* TX_HDCP_TX_AKSV_2 0x1356 TX KSV Byte 2 Register */
/* TX_AKSV0 [23_16] */
#define	TX_HDCP_TX_AKSV_2_TX_AKSV0_23_16_MSK	(0xff)	
#define	TX_HDCP_TX_AKSV_2_TX_AKSV0_23_16_BASE	0

/* TX_HDCP_TX_AKSV_3 0x1357 TX KSV Byte 3 Register */
/* TX_AKSV0 [31_24] */
#define	TX_HDCP_TX_AKSV_3_TX_AKSV0_31_24_MSK	(0xff)	
#define	TX_HDCP_TX_AKSV_3_TX_AKSV0_31_24_BASE	0

/* TX_HDCP_TX_AKSV_4 0x1358 TX KSV Byte 4 Register */
/* TX_AKSV0 [39_32] */
#define	TX_HDCP_TX_AKSV_4_TX_AKSV0_39_32_MSK	(0xff)	
#define	TX_HDCP_TX_AKSV_4_TX_AKSV0_39_32_BASE	0

/* TX_HDCP_RX_BKSV_0 0x135C RX KSV Byte 0 Register */
/* RX_BKSV0 [7_0] */
#define	TX_HDCP_RX_BKSV_0_RX_BKSV0_7_0_MSK	(0xff)	
#define	TX_HDCP_RX_BKSV_0_RX_BKSV0_7_0_BASE	0

/* TX_HDCP_RX_BKSV_1 0x135D RX KSV Byte 1 Register */
/* RX_BKSV0 [15_8] */
#define	TX_HDCP_RX_BKSV_1_RX_BKSV0_15_8_MSK	(0xff)	
#define	TX_HDCP_RX_BKSV_1_RX_BKSV0_15_8_BASE	0

/* TX_HDCP_RX_BKSV_2 0x135E RX KSV Byte 2 Register */
/* RX_BKSV0 [23_16] */
#define	TX_HDCP_RX_BKSV_2_RX_BKSV0_23_16_MSK	(0xff)	
#define	TX_HDCP_RX_BKSV_2_RX_BKSV0_23_16_BASE	0

/* TX_HDCP_RX_BKSV_3 0x135F RX KSV Byte 3 Register */
/* RX_BKSV0 [31_24] */
#define	TX_HDCP_RX_BKSV_3_RX_BKSV0_31_24_MSK	(0xff)	
#define	TX_HDCP_RX_BKSV_3_RX_BKSV0_31_24_BASE	0

/* TX_HDCP_RX_BKSV_4 0x1360 RX KSV Byte 4 Register */
/* RX_BKSV0 [39_32] */
#define	TX_HDCP_RX_BKSV_4_RX_BKSV0_39_32_MSK	(0xff)	
#define	TX_HDCP_RX_BKSV_4_RX_BKSV0_39_32_BASE	0

/* TX_HDCP_TX_AINFO 0x1361 Tx AINFO Register */
#define	TX_HDCP_TX_AINFO_AINFO_7_2_MSK	((0x3f) << 2)	/* ainfo [7_2] */
#define	TX_HDCP_TX_AINFO_AINFO_7_2_BASE	2
/* ENABLE_1.1_FEATURES */
#define	TX_HDCP_TX_AINFO_AINFO_b1	(1 << 1)		
#define	TX_HDCP_TX_AINFO_AINFO_b0	1			/* ainfo[0] */

/* TX_HDCP_RX_BCAPS 0x1362 BCAPS Read from HDMI Rx Register */
#define	TX_HDCP_RX_BCAPS_BCAPS_b7	(1 << 7)		/* bcaps[7] */
/* REPEATER, HDCP Repeater Capability */
#define	TX_HDCP_RX_BCAPS_BCAPS_b6	(1 << 6)		
/* READY, KSV FIFO Ready */
#define	TX_HDCP_RX_BCAPS_BCAPS_b5	(1 << 5)		
#define	TX_HDCP_RX_BCAPS_BCAPS_b4	(1 << 4)		/* FAST */
#define	TX_HDCP_RX_BCAPS_BCAPS_3_2_MSK	((0x3) << 2)	/* bcaps[3_2] */
#define	TX_HDCP_RX_BCAPS_BCAPS_3_2_BASE	2
#define	TX_HDCP_RX_BCAPS_BCAPS_b1	(1 << 1)		/* 1.1_FEATURES */
/* FAST_REAUTHENTICATION */
#define	TX_HDCP_RX_BCAPS_BCAPS_b0	1		

/* TX_HDCP_RX_BSTATUS_0	0x1364 BSTATUS MSB Byte Read from HDMI Rx Register */
/* bstatus[15_14] */
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_15_14_MSK	((0x3) << 6)
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_15_14_BASE	6
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_b13	(1 << 5)		/* bstatus[13] */
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_b12	(1 << 4)		/* HDMI Mode */
/* Topology Error Indicator */
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_b11	(1 << 3)		
/* Three-bit Repeater Cascade Depth */
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_10_8_MSK	(0x7)	
#define	TX_HDCP_RX_BSTATUS_0_BSTATUS_10_8_BASE	0

/* TX_HDCP_RX_BSTATUS_1	0x1365 BSTATUS LSB Byte Read from HDMI Rx Register */
/* Topology Error Indicator */
#define	TX_HDCP_RX_BSTATUS_1_BSTATUS_b7	(1 << 7)			
/* Total Number of Attached Downstream Devices */
#define	TX_HDCP_RX_BSTATUS_1_BSTATUS_6_0_MSK	(0x7f)	
#define	TX_HDCP_RX_BSTATUS_1_BSTATUS_6_0_BASE	0

/* TX_HDCP_TX_AN_0 0x1368 HDCP Tx AN Value (Byte 0) Register */
/* HDCP Tx AN Value [7_0] */
#define	TX_HDCP_TX_AN_0_AN_7_0_MSK	(0xff)	
#define	TX_HDCP_TX_AN_0_AN_7_0_BASE	0

/* TX_HDCP_TX_AN_1 0x1369 HDCP Tx AN Value (Byte 1) Register */
/* HDCP Tx AN Value [15_8] */
#define	TX_HDCP_TX_AN_1_AN_15_8_MSK	(0xff)	
#define	TX_HDCP_TX_AN_1_AN_15_8_BASE	0

/* TX_HDCP_TX_AN_2 0x136A HDCP Tx AN Value (Byte 2) Register */
/* HDCP Tx AN Value [23_16] */
#define	TX_HDCP_TX_AN_2_AN_23_16_MSK	(0xff)	
#define	TX_HDCP_TX_AN_2_AN_23_16_BASE	0

/* TX_HDCP_TX_AN_3 0x163B HDCP Tx AN Value (Byte 3) Register */
/* HDCP Tx AN Value [31_24] */
#define	TX_HDCP_TX_AN_3_AN_31_24_MSK	(0xff)	
#define	TX_HDCP_TX_AN_3_AN_31_24_BASE	0

/* TX_HDCP_TX_AN_4 0x163C HDCP Tx AN Value (Byte 4) Register */
/* HDCP Tx AN Value [39_32] */
#define	TX_HDCP_TX_AN_4_AN_39_32_MSK	(0xff)	
#define	TX_HDCP_TX_AN_4_AN_39_32_BASE	0

/* TX_HDCP_TX_AN_5 0x163D HDCP Tx AN Value (Byte 5) Register */
/* HDCP Tx AN Value [47_40] */
#define	TX_HDCP_TX_AN_5_AN_47_40_MSK	(0xff)	
#define	TX_HDCP_TX_AN_5_AN_47_40_BASE	0

/* TX_HDCP_TX_AN_6 0x163E HDCP Tx AN Value (Byte 6) Register */
/* HDCP Tx AN Value [55_48] */
#define	TX_HDCP_TX_AN_6_AN_55_48_MSK	(0xff)	
#define	TX_HDCP_TX_AN_6_AN_55_48_BASE	0

/* TX_HDCP_TX_AN_7 0x163F HDCP Tx AN Value (Byte 7) Register */
/* HDCP Tx AN Value [63_56] */
#define	TX_HDCP_TX_AN_7_AN_63_56_MSK	(0xff)	
#define	TX_HDCP_TX_AN_7_AN_63_56_BASE	0

/* TX_HDCP_TX_M0_0 0x1370 HDCP Tx M0 Value (Byte 0) Register */
/* HDCP Tx M0 Value [7_0] */
#define	TX_HDCP_TX_M0_0_M0_7_0_MSK	(0xff)	
#define	TX_HDCP_TX_M0_0_M0_7_0_BASE	0

/* TX_HDCP_TX_M0_1 0x1371 HDCP Tx M0 Value (Byte 1) Register */
/* HDCP Tx M0 Value [15_8] */
#define	TX_HDCP_TX_M0_1_M0_15_8_MSK	(0xff)	
#define	TX_HDCP_TX_M0_1_M0_15_8_BASE	0

/* TX_HDCP_TX_M0_2 0x1372 HDCP Tx M0 Value (Byte 2) Register */
/* HDCP Tx M0 Value [23_16] */
#define	TX_HDCP_TX_M0_2_M0_23_16_MSK	(0xff)	
#define	TX_HDCP_TX_M0_2_M0_23_16_BASE	0

/* TX_HDCP_TX_M0_3 0x1373 HDCP Tx M0 Value (Byte 3) Register */
/* HDCP Tx M0 Value [31_24] */
#define	TX_HDCP_TX_M0_3_M0_31_24_MSK	(0xff)	
#define	TX_HDCP_TX_M0_3_M0_31_24_BASE	0

/* TX_HDCP_TX_M0_4 0x1374 HDCP Tx M0 Value (Byte 4) Register */
/* HDCP Tx M0 Value [39_32] */
#define	TX_HDCP_TX_M0_4_M0_39_32_MSK	(0xff)	
#define	TX_HDCP_TX_M0_4_M0_39_32_BASE	0

/* TX_HDCP_TX_M0_5 0x1375 HDCP Tx M0 Value (Byte 5) Register */
/* HDCP Tx M0 Value [47_40] */
#define	TX_HDCP_TX_M0_5_M0_47_40_MSK	(0xff)	
#define	TX_HDCP_TX_M0_5_M0_47_40_BASE	0

/* TX_HDCP_TX_M0_6 0x1376 HDCP Tx M0 Value (Byte 6) Register */ 
/* HDCP Tx M0 Value [55_48] */
#define	TX_HDCP_TX_M0_6_M0_55_48_MSK	(0xff)	
#define	TX_HDCP_TX_M0_6_M0_55_48_BASE	0

/* TX_HDCP_TX_M0_7 0x1377 HDCP Tx M0 Value (Byte 7) Register */
/* HDCP Tx M0 Value [63_56] */
#define	TX_HDCP_TX_M0_7_M0_63_56_MSK	(0xff)	
#define	TX_HDCP_TX_M0_7_M0_63_56_BASE	0

/* TX_HDCP_TX_R0_0 0x1378 Tx R0 (Byte 0) Register */
#define	TX_HDCP_TX_R0_0_TX_R0_7_0_MSK	(0xff)	/* Tx R0 [7_0] */
#define	TX_HDCP_TX_R0_0_TX_R0_7_0_BASE	0

/* TX_HDCP_TX_R0_1 0x1379 Tx R0 (Byte 1) Register */
#define	TX_HDCP_TX_R0_1_TX_R0_15_8_MSK	(0xff)	/* Tx R0 [15_8] */
#define	TX_HDCP_TX_R0_1_TX_R0_15_8_BASE	0

/* TX_HDCP_RX_R0_0 0x137A Rx R0 (Byte 0) Register */
#define	TX_HDCP_RX_R0_0_RX_R0_7_0_MSK	(0xff)	/* Rx R0 [7_0] */
#define	TX_HDCP_RX_R0_0_RX_R0_7_0_BASE	0

/* TX_HDCP_RX_R0_1 0x137B Rx R0 (Byte 1) Register */
#define	TX_HDCP_RX_R0_1_RX_R0_15_8_MSK	(0xff)	/* Rx R0 [15_8] */
#define	TX_HDCP_RX_R0_1_RX_R0_15_8_BASE	0

/* TX_HDCP_TX_RI_0 0x137C Tx RI (Byte 0) Register */
#define	TX_HDCP_TX_RI_0_TX_RI_7_0_MSK	(0xff)	/* Tx RI [7_0] */
#define	TX_HDCP_TX_RI_0_TX_RI_7_0_BASE	0

/* TX_HDCP_TX_RI_1 0x137D Rx RI (Byte 1) Register */
#define	TX_HDCP_TX_RI_1_TX_RI_15_8_MSK	(0xff)	/* Tx RI [15_8] */
#define	TX_HDCP_TX_RI_1_TX_RI_15_8_BASE	0

/* TX_HDCP_RX_RI_0 0x137E Rx RI (Byte 0) Register */
#define	TX_HDCP_RX_RI_0_RX_RI_7_0_MSK	(0xff)	/* Rx RI [7_0] */
#define	TX_HDCP_RX_RI_0_RX_RI_7_0_BASE	0

/* TX_HDCP_RX_RI_1 0x137F Rx RI (Byte 1) Register */
#define	TX_HDCP_RX_RI_1_RX_RI_15_8_MSK	(0xff)	/* Rx RI [15_8] */
#define	TX_HDCP_RX_RI_1_RX_RI_15_8_BASE	0

/* TX_HDCP_TX_PJ 0x1380	Tx PJ Register */
#define	TX_HDCP_TX_PJ_TX_PJ_MSK	(0xff)	/* Tx PJ */
#define	TX_HDCP_TX_PJ_TX_PJ_BASE	0

/* TX_HDCP_RX_PJ 0x1381	Rx PJ Register */
#define	TX_HDCP_RX_PJ_RX_PJ_MSK	(0xff)	/* Rx PJ */
#define	TX_HDCP_RX_PJ_RX_PJ_BASE	0

/* TX_HDCP_FIX_CLR_0 0x1384 Default Video Value (Byte 0) Register */
/* Default Video Value [7_0] */
#define	TX_HDCP_FIX_CLR_0_FIX_CLR_7_0_MSK	(0xff)	
#define	TX_HDCP_FIX_CLR_0_FIX_CLR_7_0_BASE	0

/* TX_HDCP_FIX_CLR_1 0x1385 Default Video Value (Byte 1) Register */
/* Default Video Value [15_8] */
#define	TX_HDCP_FIX_CLR_1_FIX_CLR_15_8_MSK	(0xff)	
#define	TX_HDCP_FIX_CLR_1_FIX_CLR_15_8_BASE	0

/* TX_HDCP_FIX_CLR_2 0x1386 Default Video Value (Byte 2) Register */
/* Default Video Value [23_16] */
#define	TX_HDCP_FIX_CLR_2_FIX_CLR_23_16_MSK	(0xff)	
#define	TX_HDCP_FIX_CLR_2_FIX_CLR_23_16_BASE	0

/* TX_HDCP_KINIT_0 0x1388 KINIT (Byte 0) Register */
#define	TX_HDCP_KINIT_0_KINIT_7_0_MSK	(0xff)	/* KINIT [7_0] */
#define	TX_HDCP_KINIT_0_KINIT_7_0_BASE	0

/* TX_HDCP_KINIT_1 0x1389 KINIT (Byte 1) Register */
#define	TX_HDCP_KINIT_1_KINIT_15_8_MSK	(0xff)	/* KINIT [15_8] */
#define	TX_HDCP_KINIT_1_KINIT_15_8_BASE	0

/* TX_HDCP_KINIT_2 0x138A KINIT (Byte 2) Register */
#define	TX_HDCP_KINIT_2_KINIT_23_16_MSK	(0xff)	/* KINIT [23_16] */
#define	TX_HDCP_KINIT_2_KINIT_23_16_BASE	0

/* TX_HDCP_KINIT_3 0x138B KINIT (Byte 3) Register */
#define	TX_HDCP_KINIT_3_KINIT_31_24_MSK	(0xff)	/* KINIT [31_24] */
#define	TX_HDCP_KINIT_3_KINIT_31_24_BASE	0

/* TX_HDCP_KINIT_4 0x138C KINIT (Byte 4) Register */
#define	TX_HDCP_KINIT_4_KINIT_39_32_MSK	(0xff)	/* KINIT [39_32] */
#define	TX_HDCP_KINIT_4_KINIT_39_32_BASE	0
/* TX_HDCP_KINIT_5 0x138D KINIT (Byte 5) Register */
#define	TX_HDCP_KINIT_5_KINIT_47_40_MSK	(0xff)	/* KINIT [47_40] */
#define	TX_HDCP_KINIT_5_KINIT_47_40_BASE	0
/* TX_HDCP_KINIT_6 0x138E KINIT (Byte 6) Register */
#define	TX_HDCP_KINIT_6_KINIT_55_48_MSK	(0xff)	/* KINIT [55_48] */
#define	TX_HDCP_KINIT_6_KINIT_55_48_BASE	0

/* TX_HDCP_BINIT_0 0x1390 BINIT (Byte 0) Register */
#define	TX_HDCP_BINIT_0_BINIT_7_0_MSK		(0xff)	/* BINIT [7_0] */
#define	TX_HDCP_BINIT_0_BINIT_7_0_BASE		0

/* TX_HDCP_BINIT_1 0x1391 BINIT (Byte 1) Register */
#define	TX_HDCP_BINIT_1_BINIT_15_8_MSK	(0xff)	/* BINIT[15_8] */
#define	TX_HDCP_BINIT_1_BINIT_15_8_BASE	0

/* TX_HDCP_BINIT_2 0x1392 BINIT (Byte 2) Register */
#define	TX_HDCP_BINIT_2_BINIT_23_16_MSK	(0xff)	/* BINIT [23_16] */
#define	TX_HDCP_BINIT_2_BINIT_23_16_BASE	0

/* TX_HDCP_BINIT_3 0x1393 BINIT (Byte 3) Register */
#define	TX_HDCP_BINIT_3_BINIT_31_24_MSK	(0xff)	/* BINIT[31_24] */
#define	TX_HDCP_BINIT_3_BINIT_31_24_BASE	0

/* TX_HDCP_BINIT_4 0x1394 BINIT (Byte 4) Register */
#define	TX_HDCP_BINIT_4_BINIT_39_32_MSK	(0xff)	/* BINIT [39_32] */
#define	TX_HDCP_BINIT_4_BINIT_39_32_BASE	0

/* TX_HDCP_BINIT_5 0x1395 BINIT (Byte 5) Register */
#define	TX_HDCP_BINIT_5_BINIT_47_40_MSK	(0xff)	/* BINIT [47_40] */
#define	TX_HDCP_BINIT_5_BINIT_47_40_BASE	0

/* TX_HDCP_BINIT_6 0x1396 BINIT (Byte 6) Register */
#define	TX_HDCP_BINIT_6_BINIT_55_48_MSK	(0xff)	/* BINIT [55_48] */
#define	TX_HDCP_BINIT_6_BINIT_55_48_BASE	0

/* TX_HDCP_BINIT_7 0x1397 BINIT (Byte 7) Register */
#define	TX_HDCP_BINIT_7_BINIT_63_56_MSK	(0xff)	/* BINIT [63_56] */
#define	TX_HDCP_BINIT_7_BINIT_63_56_BASE	0

/* TX_HDCP_INTR_CLR 0x1398 Interrupt Clear Register */
#define	TX_HDCP_INTR_CLR_AKSV_READY_CLR		(1 << 7)	/* AKSV Ready Clear */
#define	TX_HDCP_INTR_CLR_TX_AN_READY_CLR	(1 << 6)	/* Tx An Ready Clear */
#define	TX_HDCP_INTR_CLR_TX_R0_READY_CLR	(1 << 5)	/* Tx R0 Ready Clear */
#define	TX_HDCP_INTR_CLR_TX_RI_READY_CLR	(1 << 4)	/* Tx Ri Ready Clear */
#define	TX_HDCP_INTR_CLR_TX_PJ_READY_CLR	(1 << 3)	/* Tx Pj Ready Clear */
/* Bit(s) TX_HDCP_INTR_CLR_RSRV_2_0 reserved */

/* TX_HDCP_INTR_MASK 0x1399 Interrupt Masking Register */
#define	TX_HDCP_INTR_MASK_AKSV_READY_MASK	(1 << 7)	/* AKSV Ready Mask */
#define	TX_HDCP_INTR_MASK_TX_AN_READY_MASK	(1 << 6)	/* Tx An Ready Mask */
#define	TX_HDCP_INTR_MASK_TX_R0_READY_MASK	(1 << 5)	/* Tx R0 Ready Mask */
#define	TX_HDCP_INTR_MASK_TX_RI_READY_MASK	(1 << 4)	/* Tx Ri Ready Mask */
#define	TX_HDCP_INTR_MASK_TX_PJ_READY_MASK	(1 << 3)	/* Tx Pj Ready Mask */
/*Bit(s) TX_HDCP_INTR_MASK_RSRV_2_0 reserved */

#endif	/* __INC_HDMI_H */
