#ifndef _TIMEBUTTON_H
#define _TIMEBUTTON_H

struct timedbutton
{
	const char* name; //for debug
	int gpio;
	int active_low;
	int key1;	//0 means no report
	int key2;	//0 means no report 
	int key1_debounce;
	int key2_debounce;
};

struct timedbutton_platform_data {
	int buttons_number;
	struct timedbutton* buttons;
};


#endif /* _TOUCHPANEL_H */
