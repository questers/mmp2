/*
 *
 * Marvell MMP2 ZSP message queue driver.
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

#ifndef _MMP2_ZSP_H_
#define _MMP2_ZSP_H_

#include <linux/types.h>
#include <linux/ioctl.h>

#ifdef __cplusplus
extern "C" {
#endif

//#define CONFIG_ZSP_AUD_PLL_GROUP_CLK_22579200	
#define CONFIG_ZSP_AUD_PLL_GROUP_CLK_24576000	

#ifdef __cplusplus
}
#endif

#endif
