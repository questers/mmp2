/*
 *
 * Copyright (C) 2006, Marvell Corporation.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef __MACH_TC35876x_H
#define __MACH_TC35876x_H

struct tc35876x_platform_data {
	int		id;
	int		id_reg;
	int		(*platform_init)(void);
};
//DSI PPI Layer Registers
#define PPI_STARTPPI		                0x0104
#define PPI_LPTXTIMECNT		                0x0114
#define PPI_LANEENABLE		                0x0134
#define PPI_TX_RX_TA		                0x013c
#define PPI_D0S_CLRSIPOCOUNT	                0x0164
#define PPI_D1S_CLRSIPOCOUNT	                0x0168
#define PPI_D2S_CLRSIPOCOUNT	                0x016c
#define PPI_D3S_CLRSIPOCOUNT	                0x0170
//DSI Protocol Layer Register
#define DSI_STARTDSI		                0x0204
#define DSI_LANEENABLE                          0x0210

//LCD control
#define LCDCTRL                 0x0420

//video timings
#define HSR                     0x0424
#define HBPR                    0x0426
#define HDISPR                  0x0428
#define HFR                     0x042A
#define VSR                     0x042C
#define VBPR                    0x042E
#define VDISPR                  0x0430
#define VFR                     0x0432

//SPI control
#define SPICTRL					0x0450

//Video Path Register
#define HTIM1					0x0454
#define HTIM2					0x0458
#define VTIM1					0x045C
#define VTIM2					0x0460

//SYSCTRL register
#define SYSCTRL                 0x0464
//SYSPLL control registers
#define SYSPLL1                 0x0468
#define SYSPLL2                 0x046c
#define SYSPLL3                 0x0470

//SYS power management register
#define SYSPMCTRL               0x047C

//LVDS Registers
#define LVMX0003				0x0480
#define LVMX0407				0x0484
#define LVMX0811				0x0488
#define LVMX1215				0x048c
#define LVMX1619				0x0490
#define LVMX2023				0x0494
#define LVMX2427				0x0498
#define LVCFG					0x049c
//DSI PPI Layer Registers
#define PPI_BUSYPPI				0x0108
//DSI Protocol Layer Regsters
#define DSI_BUSYDSI			        0x0208
#define DSI_LANESTATUS0		                0x0214
#define DSI_LANESTATUS1		                0x0218
#define DSI_INTSTAUS			        0x0220
#define DSI_INTMASK				0x0224
#define DSI_INTCLR				0x0228
//DSI General Registers
#define DSIERRCNT				0x0300
//System Registers
#define SYSSTAT					0x0500
//Chip/Rev Registers
#define IDREG					0x0580

#define CLW_DPHYCONTRX				0x0020
#define D0W_DPHYCONTRX				0x0024
#define D1W_DPHYCONTRX				0x0028
#define D2W_DPHYCONTRX				0x002C
#define D3W_DPHYCONTRX				0x0030
#define D0W_CNTRL				0x0044
#define D1W_CNTRL				0x0048
#define D2W_CNTRL				0x004c
#define D3W_CNTRL				0x0050
#define PPI_D0S_ATMR				0x0144
#define PPI_D1S_ATMR				0x0148
#define PPI_D2S_ATMR				0x014C
#define PPI_D3S_ATMR				0x0150
#define PPI_D0S_CLRSIPOCOUNT			0x0164
#define PPI_D1S_CLRSIPOCOUNT			0x0168
#define PPI_D2S_CLRSIPOCOUNT			0x016c
#define PPI_D3S_CLRSIPOCOUNT			0x0170
#define D0S_PRE					0x0184
#define D1S_PRE					0x0188
#define D2S_PRE					0x018C
#define D3S_PRE					0x0190
#define D0S_PREP				0x01A4
#define D1S_PREP				0x01A8
#define D2S_PREP				0x01AC
#define D3S_PREP				0x01B0
#define D0S_ZERO				0x01C4
#define D1S_ZERO				0x01C8
#define D2S_ZERO				0x01CC
#define D3S_ZERO				0x01D0

#endif
