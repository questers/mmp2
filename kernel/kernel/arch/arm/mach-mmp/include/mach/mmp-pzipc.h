/*
 * This software program is licensed subject to the GNU General Public License
 * (GPL).Version 2,June 1991, available at http://www.fsf.org/copyleft/gpl.html
 * jgjing@marvell.com
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 */

#ifndef _MMP2_PZIPC_H_
#define _MMP2_PZIPC_H_
#include<linux/interrupt.h>
#ifdef __cplusplus
extern "c" {
#endif
/* interrupt bit setting for IPC */
#define IPC_INTERRUPT_BIT_0_0   (0x00000001)
#define IPC_INTERRUPT_BIT_0_1   (0x00000002)
#define IPC_INTERRUPT_BIT_0_2   (0x00000004)
#define IPC_INTERRUPT_BIT_0_3   (0x00000008)
#define IPC_INTERRUPT_BIT_0_4   (0x00000010)
#define IPC_INTERRUPT_BIT_0_5   (0x00000020)
#define IPC_INTERRUPT_BIT_0_6   (0x00000040)
#define IPC_INTERRUPT_BIT_0_7   (0x00000080)
#define IPC_INTERRUPT_BIT_1_8   (0x00000100)
#define IPC_INTERRUPT_BIT_2_9   (0x00000200)
#define IPC_INTERRUPT_BIT_3_10  (0x00000400)

/* 16 bits define in ZSP for IPC event id */
enum ipc_client_id{
	IPC_HANDSHAKE_ID          = 0x0,
	IPC_MSG_TRANSFER_ID       = 0x1,
	IPC_DDR_ACCESS_ENABLE_ID  = 0x2,
	IPC_DDR_ACCESS_DISABLE_ID = 0x3,
	IPC_PORT_FLOWCONTROL_ID   = 0x4,
	IPC_TEST_ID               = 0x5,
	IPC_IPM_ID                = 0x6,
	IPC_SOFT_RESET_ID         = 0x7,
	IPC_MAX_NUM,
};

enum pzipc_return_code{
	PZIPC_RC_OK = 0,
	PZIPC_RC_FAILURE,
	PZIPC_RC_API_FAILURE,
	PZIPC_RC_WRONG_PARAM,
};

enum pzipc_return_code pzipc_add_isr(enum ipc_client_id client_id, irq_handler_t isr_handler);
enum pzipc_return_code pzipc_remove_isr(enum ipc_client_id client_id);
enum pzipc_return_code pzipc_set_interrupt(enum ipc_client_id client_id);
void zsp_local_start(atomic_t *pcounter, int timeout_ms, int retry);
void zsp_local_stop(atomic_t *pcounter);

#ifdef __cplusplus
}
#endif

#endif  /* _MMP2_PZIPC_H_ */

