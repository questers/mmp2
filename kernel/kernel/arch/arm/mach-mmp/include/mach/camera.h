/*
    camera.h - MMP2 camera driver header file

    Copyright (C) 2003, Intel Corporation
    Copyright (C) 2008, Guennadi Liakhovetski <kernel@pengutronix.de>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __ASM_ARCH_CAMERA_H_
#define __ASM_ARCH_CAMERA_H_

#define CCIC_0		0	// for 2 camera interface
#define CCIC_1		1
#define CCIC_NUM	1	// we temp enable only one ccic controller

#define SENSOR_LOW	0	/* Low resolution sensor, ov7690 ...etc.*/
#define SENSOR_HIGH	1	/* High resolution sensor, ov5642 ...etc.*/
#define SENSOR_NONE	2	/* None sensor seleted */
#define SENSOR_CLOSE	0	/* Sensor clock disable */
#define SENSOR_OPEN	1	/* Sensor clock enable */

struct cam_platform_data {
	unsigned int vsync_gpio;
	int (*init)(void);
	void (*deinit)(void);
	void (*suspend)(void);
	void (*resume)(void);
	void (*sync_to_gpio)(void);
	void (*sync_from_gpio)(void);
	int (*power_set)(int);
	int (*pmua_set)(int);
};

struct sensor_platform_data {
	int id;
	int eco_flag;			/* Workaround for Camera HW limitation in Brownstone REV3/REV4 */
	int sensor_flag;		/* Flag for different sensor enable mode, such OVT and GCT */
	int (*eco_set)(int eco);	/* Workaround for Camera HW limitation in Brownstone REV3/REV4 */
	int (*power_set)(int flag, int res, int eco, int sensor);
};

extern int ccic_sensor_attach(struct i2c_client *client);
extern int ccic_sensor_detach(struct i2c_client *client);

#endif /* __ASM_ARCH_CAMERA_H_ */

