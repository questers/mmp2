#ifndef __ASM_MACH_CPUTYPE_H
#define __ASM_MACH_CPUTYPE_H

#include <asm/io.h>
#include <asm/cputype.h>
#include <mach/addr-map.h>

/*
 *  CPU   Stepping   CPU_ID      CHIP_ID
 *
 * PXA168    S0    0x56158400   0x0000C910
 * PXA168    A0    0x56158400   0x00A0A168
 * PXA910    Y1    0x56158400   0x00F2C920
 * PXA910    A0    0x56158400   0x00F2C910
 * PXA910    A1    0x56158400   0x00A0C910
 * PXA920    Y0    0x56158400   0x00F2C920
 * PXA920    A0    0x56158400   0x00A0C920
 * PXA920    A1    0x56158400   0x00A1C920
 * MMP2	     Z0	   0x560f5811   0x00F00410
 * MMP2      Z1    0x560f5811   0x00E00410
 * MMP2      A0    0x560f5811   0x00A0A610
 */

#define MMP_CHIPID		(AXI_VIRT_BASE + 0x82c00)

#ifdef CONFIG_CPU_PXA168
#define cpu_is_pxa168()							\
	({ unsigned int _id = (read_cpuid_id() >> 8) & 0xff;		\
	   unsigned int _mmp_id = (__raw_readl(MMP_CHIPID) & 0xfff);	\
	   _id == 0x84 && _mmp_id == 0x168; })
#else
#define cpu_is_pxa168()	(0)
#endif

/* cpu_is_pxa910() is shared on both pxa910 and pxa920 */
#ifdef CONFIG_CPU_PXA910
#define cpu_is_pxa910()							\
	({ unsigned int _id = (read_cpuid_id() >> 8) & 0xff;		\
	   unsigned int _mmp_id = (__raw_readl(MMP_CHIPID) & 0xfff);	\
	   (_id == 0x84 && (_mmp_id == 0x910 || _mmp_id == 0x920)); })
#else
#define cpu_is_pxa910()	(0)
#endif

#ifdef CONFIG_CPU_MMP2
#define cpu_is_mmp2()							\
	({ unsigned int _id = (read_cpuid_id() >> 8) & 0xff; _id == 0x58; })
#else
#define cpu_is_mmp2()	(0)
#endif

#define CHIP_ID         (AXI_VIRT_BASE + 0x82c00)
#define CPU_CONF        (AXI_VIRT_BASE + 0x82c08)
#define SOC_STEPPING    (BOOTROM_VIRT_BASE + 0x30)

extern unsigned int mmp2_stepping;

static inline int cpu_is_mmp2_z0(void)
{
	unsigned int chip_id;

	if (mmp2_stepping)
		return 0;

	chip_id = __raw_readl(CHIP_ID);
	if (cpu_is_mmp2() && ((chip_id & 0x00ff0000) == 0x00f00000))
		return 1;
	else
		return 0;
}

static inline int cpu_is_mmp2_z1(void)
{
	unsigned int chip_id;

	if (mmp2_stepping)
		return 0;

	chip_id = __raw_readl(CHIP_ID);
	if (cpu_is_mmp2() && ((chip_id & 0x00ff0000) == 0x00e00000))
		return 1;
	else
		return 0;
}

static inline int cpu_is_mmp2_a0(void)
{
	unsigned int chip_id;
	unsigned int soc_stepping;

	if (mmp2_stepping) {
		if (mmp2_stepping == 0xa0)
			return 1;
		else
			return 0;
	}

	chip_id = __raw_readl(CHIP_ID);
	soc_stepping = __raw_readl(SOC_STEPPING);
	if (cpu_is_mmp2() && ((chip_id & 0x00ff0000) == 0x00a00000) && (soc_stepping == 0x4130))
		return 1;
	else
		return 0;
}

static inline int cpu_is_mmp2_a1(void)
{
	unsigned int chip_id;
	unsigned int soc_stepping;

	if (mmp2_stepping) {
		if (mmp2_stepping == 0xa1)
			return 1;
		else
			return 0;
	}

	chip_id = __raw_readl(CHIP_ID);
	soc_stepping = __raw_readl(SOC_STEPPING);
	if (cpu_is_mmp2() && ((chip_id & 0x00ff0000) == 0x00a00000) && (soc_stepping == 0x4131))
		return 1;
	else
		return 0;
}

static inline int cpu_is_mmp2_a2(void)
{
	unsigned int chip_id;
	unsigned int soc_stepping;

	if (mmp2_stepping) {
		if (mmp2_stepping == 0xa2)
			return 1;
		else
			return 0;
	}

	if (cpu_is_mmp2_z0() || cpu_is_mmp2_z1())
		return 0;

	chip_id = __raw_readl(CHIP_ID);
	soc_stepping = __raw_readl(SOC_STEPPING);
	if (cpu_is_mmp2() && ((chip_id & 0x00ff0000) == 0x00a00000) && (soc_stepping == 0x4132))
		return 1;
	else
		return 0;
}

#endif /* __ASM_MACH_CPUTYPE_H */
