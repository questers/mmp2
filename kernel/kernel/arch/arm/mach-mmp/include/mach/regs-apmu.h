/*
 * linux/arch/arm/mach-mmp/include/mach/regs-apmu.h
 *
 *   Application Subsystem Power Management Unit
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef __ASM_MACH_REGS_APMU_H
#define __ASM_MACH_REGS_APMU_H

#include <mach/addr-map.h>

#define APMU_VIRT_BASE	(AXI_VIRT_BASE + 0x82800)
#define APMU_REG(x)	(APMU_VIRT_BASE + (x))

/* Clock Reset Control */
#define APMU_CC_SP	APMU_REG(0x000)
#define APMU_CC_PJ	APMU_REG(0x004)
#define APMU_DM_CC_SP	APMU_REG(0x008)
#define APMU_DM_CC_PJ	APMU_REG(0x00c)
#define APMU_SP_IDLE_CFG	APMU_REG(0x014)
#define APMU_IDLE_CFG	APMU_REG(0x018)
#define APMU_CCIC_GATE  APMU_REG(0x028)
#define APMU_IRE	APMU_REG(0x048)
#define APMU_LCD	APMU_REG(0x04c)
#define APMU_CCIC	APMU_REG(0x050)
#define APMU_CCIC_RST   APMU_REG(0x050)
#define APMU_SDH0	APMU_REG(0x054)
#define APMU_SDH1	APMU_REG(0x058)
#define APMU_PLL_SEL_STATUS	APMU_REG(0x00c4)
#define APMU_USBOTG	APMU_REG(0x05c)
#define APMU_NAND	APMU_REG(0x060)
#define APMU_DMA	APMU_REG(0x064)
#define APMU_GEU	APMU_REG(0x068)
#define APMU_BUS	APMU_REG(0x06c)
#define APMU_CCIC_DBG   APMU_REG(0x088)
#define APMU_SRAM_PWR_DWN	APMU_REG(0x08c)
#define APMU_VMETA	APMU_REG(0xa4)

#define APMU_VMETA_CLK_RES_CTRL	APMU_VMETA
/* Vmeta Technology Bus Clock Source */
#define	APMU_VMETA_CLK_RES_CTRL_VMETA_ACLK_SRC	(1 << 15)
/* VMeta Technology Bus Clock Select */
#define	APMU_VMETA_CLK_RES_CTRL_VMETA_ACLK_SEL_MSK	(0x7 << 12)
#define	PMUA_VMETA_CLK_RES_CTRL_VMETA_ACLK_SEL_BASE	12
/* VMeta Technology Power Mode */
#define	APMU_VMETA_CLK_RES_CTRL_VMETA_PWR_CTRL	(1 << 11)
/* VMeta Technology Power Up */
#define	APMU_VMETA_CLK_RES_CTRL_VMETA_PWRUP	(1 << 10)
/* VMeta Technology Input Isolation Enable */
#define	APMU_VMETA_CLK_RES_CTRL_VMETA_INP_ISB	(1 << 9)
/* VMeta Technology Isolation Enable */
#define	APMU_VMETA_CLK_RES_CTRL_VMETA_ISB	(1 << 8)
/* VMeta Technology Clock Select */
#define	APMU_VMETA_CLK_RES_CTRL_VMETA_CLK_SEL	(7 << 5)
/* Bit(s) PMUA_VMETA_CLK_RES_CTRL_RSRV_7 reserved */
#define	APMU_VMETA_CLK_RES_CTRL_VMETA_CLK_SEL_Z	(1 << 6)
/* Bit(s) PMUA_VMETA_CLK_RES_CTRL_RSRV_5 reserved */
/* VMeta Technology Peripheral Clock Enable */
#define	APMU_VMETA_CLK_RES_CTRL_VMETA_CLK_EN	(1 << 4)
/* VMeta Technology AXI Clock Enable */
#define	APMU_VMETA_CLK_RES_CTRL_VMETA_AXICLK_EN	(1 << 3)
/* Bit(s) PMUA_VMETA_CLK_RES_CTRL_RSRV_2 reserved */
/* VMeta Technology Peripheral Reset 1 */
#define	APMU_VMETA_CLK_RES_CTRL_VMETA_RST1	(1 << 1)
/* VMeta Technology AXI Reset */
#define	APMU_VMETA_CLK_RES_CTRL_VMETA_AXI_RST	(1 << 0)
#define APMU_SDH2	APMU_REG(0x0e8)
#define APMU_SDH3	APMU_REG(0x0ec)
#define APMU_MC_HW_SLP_TYPE     APMU_REG(0x0b0)
#define APMU_MC_SLP_REQ_PJ	APMU_REG(0x0b4)
#define APMU_SMC_CLK_RES_CTRL   APMU_REG(0x0d4)
#define APMU_GC	APMU_REG(0x0cc)
#define APMU_USBHSIC1	APMU_REG(0x0f8)
#define APMU_USBHSIC2	APMU_REG(0x0fc)
#define APMU_USBFSIC	APMU_REG(0x100)
#define APMU_USBSPH	APMU_REG(0x05c)		/* in PXA910 */

#define APMU_FNCLK_EN	(1 << 4)
#define APMU_AXICLK_EN	(1 << 3)
#define APMU_FNRST_DIS	(1 << 1)
#define APMU_AXIRST_DIS	(1 << 0)

#define APMU_LCD_CLK_RES_CTRL   APMU_REG(0x004c)
#define APMU_LCD2_CLK_RES_CTRL  APMU_REG(0x110)

#define APMU_CCIC2_GATE APMU_REG(0x118)

#define APMU_GC800_PLL1_DIV4	0x0 /*gc clk: PLL1/4; gc bus clk: PLL1/2*/
#define APMU_GC800_PLL1_DIV6	0x50/*gc clk: PLL1/6; gc bus clk: PLL1/3*/
#define APMU_GC800_PLL2		0xa0/*gc clk: PLL2; gc bus clk: PLL2*/
#define APMU_GC800_PLL2_DIV2	0xf0/*gc clk: PLL2/2; gc bus clk: PLL2/2*/

#define APMU_AUDIO			APMU_REG(0x10C)
#define APMU_AUDIO_CTRL_PWR_UP		(3 << 9)
#define APMU_AUDIO_CTRL_PWR_DOWN	(0 << 9)
#define APMU_AUDIO_CTRL_ISO_DIS		(1 << 8)
#define APMU_AUDIO_CLK_SEL_PLL1_DIV_2	(0 << 6)
#define APMU_AUDIO_CLK_SEL_PLL2_DIV_2	(1 << 6)
#define APMU_AUDIO_CLK_SEL_PLL2_DIV_3	(2 << 6)
#define APMU_AUDIO_CLK_SEL_PLL1_DIV_8	(3 << 6)
#define APMU_AUDIO_CLK_ENA		(1 << 4)
#define APMU_AUDIO_RST_DIS		(1 << 1)

#endif /* __ASM_MACH_REGS_APMU_H */
