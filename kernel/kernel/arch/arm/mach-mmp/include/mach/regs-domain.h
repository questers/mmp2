#ifndef __ASM_MACH_REGS_DOMAIN_H
#define __ASM_MACH_REGS_DOMAIN_H

#include <mach/addr-map.h>
#include <mach/regs-apbc.h>

#define DOMAIN_VIRT_BASE	(APB_VIRT_BASE + 0x01E800)
#define DOMAIN_REG(x)	(DOMAIN_VIRT_BASE + (x))

/*
 * IO domain register offsets for MMP2
 */
#define DOMAIN_MMP2_HSIC3 	DOMAIN_REG(0x000)
#define DOMAIN_MMP2_HSIC2 	DOMAIN_REG(0x004)
#define DOMAIN_MMP2_CAMERA 	DOMAIN_REG(0x00C)
#define DOMAIN_MMP2_LCD 	DOMAIN_REG(0x010)
#define DOMAIN_MMP2_TWSI 	DOMAIN_REG(0x014)
#define DOMAIN_MMP2_SDMMC 	DOMAIN_REG(0x01C)
#define DOMAIN_MMP2_NAND 	DOMAIN_REG(0x020)
#define DOMAIN_MMP2_USIM 	DOMAIN_REG(0x02C)
#define DOMAIN_MMP2_BB		DOMAIN_REG(0x030)

/*
 * Follow sequece below to access IO domain registers
 * write 0xBABA to IO_DOMAIN_FIRST_ACCESS_KEY;
 * write 0xEB10 to IO_DOMAIN_SECOND_ACCESS_KEY;
 * Single read or write operation access performed on io domain registers
 */
#define IO_DOMAIN_FIRST_ACCESS_KEY APBC_MMP2_ASFAR
#define IO_DOMAIN_FIRST_ACCESS_CODE 0xBABA
#define IO_DOMAIN_SECOND_ACCESS_KEY APBC_MMP2_ASSAR
#define IO_DOMAIN_SECOND_ACCESS_CODE 0xEB10

#endif /* __ASM_MACH_REGS_DOMAIN_H */
