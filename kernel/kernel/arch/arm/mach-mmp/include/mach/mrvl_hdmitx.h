/******************************************************************************
 *
 * Name:        mrvl_hdmitx.h
 * Project:     MMP2
 * Jiangang Jing
 *
 * Copyright (c) 2009, Marvell International Ltd (jgjing@marvell.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *****************************************************************************/

#ifndef __MRVL_HDMITX_H__
#define __MRVL_HDMITX_H__

#include <asm/ioctl.h>
#include <linux/fb.h>
#define HDMITX_MAX_PKT_INDEX     6

struct hdmi_audio_fmt {
        unsigned int sr;
        unsigned int i2s;
        unsigned int wsp;
        unsigned int fsp;
        unsigned int clkp;
        unsigned int xdatdly;
        unsigned int xwdlen1;
        unsigned int xfrlen1;
};
struct hdmi_video_fmt {
        unsigned int panel_index;
        unsigned int hd_en;
};
#define HDMIIO_AUDIO_FMT                _IOW('2', 1, struct hdmi_audio_fmt *)
#define HDMIIO_VIDEO_FMT                _IOW('2', 2, struct hdmi_video_fmt *)

enum hdmi_mode{
	HDMI_480P = 0,
	HDMI_720P,
	HDMI_1080P,
	HDMI_640x480P,
	HDMI_1440x240P,
	HDMI_720x576P_50Hz,
	HDMI_1440x288P_50Hz,
	HDMI_2880x240P,
	HDMI_1440x480P,
	HDMI_2880x288P_50Hz,
	HDMI_1440x576P_50Hz,
	HDMI_2880x480P,
	HDMI_2880x576P_50Hz,
};

enum hdmitx_out_mode{
    HDMITX_OUT_UNDEF = 0x00,
    HDMITX_OUT_DVI,
    HDMITX_OUT_HDMI,
};

enum hdmitx_audio_bus{
    HDMITX_AUDIO_I2S,
    HDMITX_AUDIO_SPDIF,
    HDMITX_AUDIO_BUS_MAX
};

// Audio Input clock samlpling freq types
enum hdmitx_audio_inp_clk_sel{
    HDMITX_AUD_INP_CLK_128fs = 0,
    HDMITX_AUD_INP_CLK_256fs,
    HDMITX_AUD_INP_CLK_384fs,
    HDMITX_AUD_INP_CLK_512fs,
    HDMITX_AUD_INP_CLK_MAX
};

// Color Depth
enum hdmitx_bit_depth{
    HDMITX_BIT_DEPTH_8 = 0,
    HDMITX_BIT_DEPTH_10,
    HDMITX_BIT_DEPTH_12,
};

// Pixel repetition types
enum hdmitx_pixel_rpt_type{
    HDMITX_PIXEL_RPT_NONE = 0,
    HDMITX_PIXEL_RPT_BY_2,
    HDMITX_PIXEL_RPT_BY_3,
    HDMITX_PIXEL_RPT_BY_4,
    HDMITX_PIXEL_RPT_BY_5,
    HDMITX_PIXEL_RPT_BY_6,
    HDMITX_PIXEL_RPT_BY_7,
    HDMITX_PIXEL_RPT_BY_8,
    HDMITX_PIXEL_RPT_BY_9,
    HDMITX_PIXEL_RPT_BY_10,
    HDMITX_PIXEL_RPT_MAX
};

// HDMI Packet Types
enum hdmitx_host_pkt_type{
    HDMITX_PKT_NONE  = 0x00,
    HDMITX_NULL_PKT,
    HDMITX_ACR_PKT,
    HDMITX_AUD_SAMP_PKT,
    HDMITX_GC_PKT,
    HDMITX_ACP_PKT,
    HDMITX_ISRC1_PKT,
    HDMITX_ISRC2_PKT,
    HDMITX_1BIT_AUD_SAMP_PKT,
    HDMITX_DST_AUD_PKT,
    HDMITX_HBR_AUD_PKT,
    HDMITX_GAMUT_METADATA_PKT,
    HDMITX_VENDOR_INFOFRM_PKT = 0x81,
    HDMITX_AVI_INFOFRM_PKT,
    HDMITX_SPD_INFOFRM_PKT,
    HDMITX_AUD_INFOFRM_PKT,
    HDMITX_MPEG_INFOFRM_PKT,
    HDMITX_MAX_PKT,
};

// Host packet transfer mode
enum hdmitx_host_pkt_tx_mode{
    HDMITX_HOST_PKT_TX_ONCE = 0,
    HDMITX_HOST_PKT_TX_EVERY_VSYNC,
    HDMITX_HOST_PKT_TX_MAX_MODE
};

union hdmitx_audio_bus_params{
    struct
    {
        bool        ClkDelay;
        bool        DataShift;
        bool        DataJustification;
        bool        LeftChnWordSelEdge;
        bool        DataLatchingEdge;
        u8       PortCfg; // Flag
    }I2SParams;
};

struct hdmitx_audio_fmt_params{
    bool	Layout;
    u8	WordLength;
    enum hdmitx_audio_inp_clk_sel	InputClk;
    u32	AcrN;
    u32	AcrCts;
    u8	HbrAudio;
};

struct hdmitx_video_fmt_params{
    // RGB/YCbCr
    int  VideoFmt;
    // Display Resolution
    int  DispRes;
    // Color Depth
    int ColorDepth;
    // Pixel Repetition
    enum hdmitx_pixel_rpt_type PixelRpt;
};

struct hdmitx_audio_chn_sts{
    u8       CpBit;
    u8       PreEmphasisInfo;
    u8       CategoryCode;
    u8       SrcNum;
    u8       ChnNum;
    u8       SampFreq;
    u8       ClkAccuracy;
    u8       MaxAudSampLen;
    u8       WordLength; //Based on MaxAudSampLen
    u8       OrigSampFreq;
};

static struct fb_videomode tv_video_modes[] = {
	[0] = {
		.pixclock	= 6734,
		.refresh	= 60,
		.xres		= 1920,
		.yres		= 1080,
		.hsync_len	= 44,
		.left_margin	= 88,
		.right_margin	= 148,
		.vsync_len	= 5,
		.upper_margin	= 4,
		.lower_margin	= 36,
	},
	[1] = {
		.pixclock	= 13149,
		.refresh	= 60,
		.xres		= 1280,
		.yres		= 720,
		.hsync_len	= 40,
		.left_margin	= 110,
		.right_margin	= 220,
		.vsync_len	= 5,
		.upper_margin	= 5,
		.lower_margin	= 20,
	},
	[2] = {
		.pixclock	= 37000,
		.refresh	= 60,
		.xres		= 720,
		.yres		= 480,
		.hsync_len	= 62,
		.left_margin	= 16,
		.right_margin	= 60,
		.vsync_len	= 6,
		.upper_margin	= 9,
		.lower_margin	= 30,
		.sync           = FB_SYNC_VERT_HIGH_ACT | FB_SYNC_HOR_HIGH_ACT,
	},
	[3] = {
		.pixclock	= 39682,
		.refresh	= 60,
		.xres		= 640,
		.yres		= 480,
		.hsync_len	= 96,
		.left_margin	= 16,
		.right_margin	= 48,
		.vsync_len	= 2,
		.upper_margin	= 10,
		.lower_margin	= 33,
		.sync           = FB_SYNC_VERT_HIGH_ACT | FB_SYNC_HOR_HIGH_ACT,
	},
	[4] = {
		.pixclock	= 37070,
		.refresh	= 60,
		.xres		= 1440,
		.yres		= 240,
		.hsync_len	= 124,
		.left_margin	= 38,
		.right_margin	= 114,
		.vsync_len	= 3,
		.upper_margin	= 4,
		.lower_margin	= 15,
		.sync           = FB_SYNC_VERT_HIGH_ACT | FB_SYNC_HOR_HIGH_ACT,
	},
	[5] = {
		.pixclock	= 37036,
		.refresh	= 50,
		.xres		= 720,
		.yres		= 576,
		.hsync_len	= 64,
		.left_margin	= 12,
		.right_margin	= 68,
		.vsync_len	= 5,
		.upper_margin	= 5,
		.lower_margin	= 39,
		.sync           = FB_SYNC_VERT_HIGH_ACT | FB_SYNC_HOR_HIGH_ACT,
	},
	[6] = {
		.pixclock	= 37095,
		.refresh	= 50,
		.xres		= 1440,
		.yres		= 288,
		.hsync_len	= 126,
		.left_margin	= 24,
		.right_margin	= 138,
		.vsync_len	= 3,
		.upper_margin	= 3,
		.lower_margin	= 18,
	},
	[7] = {
		.pixclock	= 18535,
		.refresh	= 60,
		.xres		= 2880,
		.yres		= 240,
		.hsync_len	= 248,
		.left_margin	= 76,
		.right_margin	= 228,
		.vsync_len	= 3,
		.upper_margin	= 4,
		.lower_margin	= 15,
	},
	[8] = {
		.pixclock	= 18500,
		.refresh	= 60,
		.xres		= 1440,
		.yres		= 480,
		.hsync_len	= 124,
		.left_margin	= 32,
		.right_margin	= 120,
		.vsync_len	= 6,
		.upper_margin	= 9,
		.lower_margin	= 30,
		.sync           = FB_SYNC_VERT_HIGH_ACT | FB_SYNC_HOR_HIGH_ACT,
	},
	[9] = {
		.pixclock	= 1674,
		.refresh	= 50,
		.xres		= 2880,
		.yres		= 288,
		.hsync_len	= 252,
		.left_margin	= 48,
		.right_margin	= 276,
		.vsync_len	= 3,
		.upper_margin	= 3,
		.lower_margin	= 18,
	},
	[10] = {
		.pixclock	= 18518,
		.refresh	= 50,
		.xres		= 1440,
		.yres		= 576,
		.hsync_len	= 128,
		.left_margin	= 24,
		.right_margin	= 136,
		.vsync_len	= 5,
		.upper_margin	= 5,
		.lower_margin	= 39,
		.sync           = FB_SYNC_VERT_HIGH_ACT | FB_SYNC_HOR_HIGH_ACT,
	},
	[11] = {
		.pixclock	= 9250,
		.refresh	= 60,
		.xres		= 2880,
		.yres		= 480,
		.hsync_len	= 248,
		.left_margin	= 64,
		.right_margin	= 240,
		.vsync_len	= 6,
		.upper_margin	= 9,
		.lower_margin	= 30,
	},
	[12] = {
		.pixclock	= 9259,
		.refresh	= 50,
		.xres		= 2880,
		.yres		= 576,
		.hsync_len	= 256,
		.left_margin	= 48,
		.right_margin	= 272,
		.vsync_len	= 5,
		.upper_margin	= 5,
		.lower_margin	= 39,
	},

};

/*-----------------------------------------------------------------------------
 * Description  : This function enables audio packet transfer
 * Arguments    : EnAudio - Flag to enable/disable audio packet transmission
 * Returns      : MV_VPP_OK, on success
 *              : Error code, otherwise
 * Notes        : None
 *-----------------------------------------------------------------------------
 */
int HDMITX_EnableAudioPkt(void *hiobj,bool EnAudio);


#endif // __MRVL_HDMITX_H__
