#include <linux/gpio_onkey.h>
#include <linux/wakelock.h>
/* gpio onkey platform data */
static struct gpio_onkey_pdata onkey_pdata = {
	.onkey_gpio	= mfp_to_gpio(GPIO130_PWR_KEY),
	.pwroff_gpio	= mfp_to_gpio(GPIO128_PWR_OFF),
	.reboot_gpio	= mfp_to_gpio(GPIO10_REBOOT),
};

static struct platform_device onkey_device = {
	.name		= "gpio-onkey",
	.id		= -1,
	.dev		= {
		.platform_data	= &onkey_pdata,
	},
};

#if 0
static struct wake_lock wakelock_zkfp;
static struct delayed_work zkfpwork;

static void zkfp_work(struct work_struct *work){
	int	zkfp_pwr = GPIO(104);
	if (gpio_request(zkfp_pwr, "zkfp_pwr")) {
		printk(KERN_INFO "gpio %d request failed\n", zkfp_pwr);
	}else {
		if(!!!gpio_get_value(zkfp_pwr)){
			printk("powerup zkfp\n");
			wake_lock_timeout(&wakelock_zkfp, HZ * 2);
			gpio_direction_output(zkfp_pwr,1);
		}
		gpio_free(zkfp_pwr);
	}	

	
}
static int system_fb_notifier_callback(struct notifier_block *self,
		unsigned long event, void *data)
{
	if(FB_EVENT_SUSPEND==event){
		int zkfp_pwr = GPIO(104);
		cancel_delayed_work_sync(&zkfpwork);
		if (gpio_request(zkfp_pwr, "zkfp_pwr")) {
			printk(KERN_INFO "gpio %d request failed\n", zkfp_pwr);
		}else {
			if(!!gpio_get_value(zkfp_pwr)){
				printk("shutdown zkfp\n");
				wake_lock_timeout(&wakelock_zkfp, HZ * 2);
				gpio_direction_output(zkfp_pwr,0);
			}
			gpio_free(zkfp_pwr);
		}	
		
	}
	else if(FB_EVENT_RESUME==event){
		schedule_delayed_work(&zkfpwork, msecs_to_jiffies(2000));		
	}

	return 0;
}

static struct notifier_block system_fb_notifier = {
	.notifier_call = system_fb_notifier_callback,
};

#endif
static void __init board_pm_init(void)
{
	platform_device_register(&onkey_device);
	#if 0
	{
		
		wake_lock_init(&wakelock_zkfp, WAKE_LOCK_SUSPEND, "wakelock_zkfp");
		INIT_DELAYED_WORK(&zkfpwork,zkfp_work);
		fb_register_client(&system_fb_notifier);

	}
	#endif
}

