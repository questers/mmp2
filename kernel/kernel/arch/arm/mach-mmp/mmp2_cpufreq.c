/*
 * linux/arch/arm/mach-mmp/mmp2_cpufreq.c
 *
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/cpufreq.h>
#include <linux/sysdev.h>
#include <linux/cpu.h>
#if defined(CONFIG_DVFM_MMP2)
#include <mach/dvfm.h>
#include <mach/mmp2_dvfm.h>
static int dvfm_dev_idx;
#endif

#define MAX_CORE_FREQS	50
#define MHZ_TO_KHZ	1000

static int core_freqs_table[MAX_CORE_FREQS];
static int num_pp;
static unsigned int mmp2_freqs_num;
static struct cpufreq_frequency_table *mmp2_freqs_table;
static unsigned int mmp2_cpufreq_get(unsigned int cpu);

static ssize_t supported_freqs_show(struct sys_device *sys_dev,
					struct sysdev_attribute *attr,
					char *buf)
{
	int len, i;

	len = sprintf(buf, "Supported CPU frequencies:\n");
	for (i = 0; i < num_pp; i++)
		len += sprintf(&buf[len], "freq[%d] = %d MHz\n",
				i, core_freqs_table[i]);

	return len;
}
SYSDEV_ATTR(supported_freqs, 0444, supported_freqs_show, NULL);

static ssize_t current_freq_show(struct sys_device *sys_dev,
				struct sysdev_attribute *attr,
				char *buf)
{
	int freq;

	freq = mmp2_cpufreq_get(0);
	return sprintf(buf, "Current frequency = %d\n", freq);
}
SYSDEV_ATTR(current_freqs, 0444, current_freq_show, NULL);

static struct attribute *cpufreq_stats_attr[] = {
	&attr_supported_freqs.attr,
	&attr_current_freqs.attr,
};

static int stats_add(struct sys_device *sys_dev)
{
	int i, ret;
	for (i = 0; i < ARRAY_SIZE(cpufreq_stats_attr); i++) {
		ret = sysfs_create_file(&(sys_dev->kobj),
						cpufreq_stats_attr[i]);
		if (ret)
			return ret;
	}
	return 0;
}

static int stats_rm(struct sys_device *sys_dev)
{
	int i;
	for (i = 0; i < ARRAY_SIZE(cpufreq_stats_attr); i++)
		sysfs_remove_file(&(sys_dev->kobj), cpufreq_stats_attr[i]);

	return 0;
}

static int stats_suspend(struct sys_device *sysdev, pm_message_t pmsg)
{
	return 0;
}

static int stats_resume(struct sys_device *sysdev)
{
	return 0;
}

static struct sysdev_driver cpufreq_stats_driver = {
	.add		= stats_add,
	.remove		= stats_rm,
	.suspend	= stats_suspend,
	.resume		= stats_resume,
};

static int mmp2_cpufreq_verify(struct cpufreq_policy *policy)
{
	return cpufreq_frequency_table_verify(policy, mmp2_freqs_table);
}

static int mmp2_cpufreq_target(struct cpufreq_policy *policy,
				unsigned int target_freq,
				unsigned int relation)
{
	struct cpufreq_freqs freqs;
	unsigned int op_point;
	unsigned int freq_khz = 0;

	freqs.old = policy->cur;
	freqs.cpu = policy->cpu;

	pr_debug("CPU frequency from %d MHz to %d MHz%s\n",
			freqs.old / MHZ_TO_KHZ, freqs.new / MHZ_TO_KHZ,
			(freqs.old == freqs.new) ? " (skipped)" : "");

	if (policy->cpu != 0)
		return -EINVAL;

	for (op_point = 0; op_point < mmp2_freqs_num; op_point++) {
		freq_khz = mmp2_freqs_table[op_point].frequency;
		if (target_freq <= freq_khz) {
			target_freq = op_point;
			break;
		}
	}

	freqs.new = freq_khz;

	cpufreq_notify_transition(&freqs, CPUFREQ_PRECHANGE);
	dvfm_request_op(target_freq);
	freqs.new = mmp2_cpufreq_get(policy->cpu);
	cpufreq_notify_transition(&freqs, CPUFREQ_POSTCHANGE);

	return 0;
}

static int freq_limits_get(int *freq_table, int num_pp, int *max, int *min)
{
	int max_freq;
	int min_freq;
	int i;

	max_freq = 0;
	min_freq = 0x7fffffff;

	for (i = 0; i < num_pp; i++) {
		if (max_freq < freq_table[i])
			max_freq = freq_table[i];
		if (min_freq > freq_table[i])
			min_freq = freq_table[i];
	}

	*max = max_freq;
	*min = min_freq;

	return 0;
}

static int setup_freqs_table(struct cpufreq_policy *policy,
				int *freqs_table,
				int num)
{
	struct cpufreq_frequency_table *table;
	int i;

	table = kzalloc((num + 1) * sizeof(*table), GFP_KERNEL);
	if (table == NULL)
		return -ENOMEM;

	for (i = 0; i < num; i++) {
		table[i].index = i;
		table[i].frequency = freqs_table[i] * MHZ_TO_KHZ;
	}
	table[num].index = i;
	table[num].frequency = CPUFREQ_TABLE_END;

	mmp2_freqs_num = num;
	mmp2_freqs_table = table;

	return cpufreq_frequency_table_cpuinfo(policy, table);
}

static __init int mmp2_cpufreq_init(struct cpufreq_policy *policy)
{
	int current_freq_khz;
	int ret = 0;
	int max_freq = 0;
	int min_freq = 0;

#ifdef CONFIG_DVFM_MMP2
	ret = dvfm_core_freqs_table_get(core_freqs_table,
					&num_pp, MAX_CORE_FREQS);
	if (ret)
		pr_err("failed to get cpu frequency table");

	ret = freq_limits_get(core_freqs_table, num_pp, &max_freq, &min_freq);
	if (ret)
		pr_err("failed to get cpu frequency limits");

	current_freq_khz = dvfm_current_core_freq_get() * MHZ_TO_KHZ;
	pr_debug("max_freq = %d, min_freq=%d", max_freq, min_freq);

	/* set deault policy and cpuinfo */
	policy->cpuinfo.min_freq = min_freq * MHZ_TO_KHZ;
	policy->cpuinfo.max_freq = max_freq * MHZ_TO_KHZ;
	policy->cpuinfo.transition_latency = 1000;	/* 1mSec latency */

	policy->cur = policy->min = policy->max = current_freq_khz;

	/* setup cpuinfo frequency table */
	ret = setup_freqs_table(policy, core_freqs_table, num_pp);
	if (ret) {
		pr_err("failed to setup frequency table\n");
		return ret;
	}
#endif /* CONFIG_DVFM_MMP2 */
	cpufreq_frequency_table_get_attr(mmp2_freqs_table, policy->cpu);
	pr_info("CPUFREQ support for mmp2 initialized\n");
	return 0;
}

static unsigned int mmp2_cpufreq_get(unsigned int cpu)
{
	if (cpu != 0)
		return -EINVAL;
	return dvfm_current_core_freq_get() * MHZ_TO_KHZ;
}

static unsigned int mmp2_cpufreq_getavg(struct cpufreq_policy *policy,
					unsigned int cpu)
{
	int freq;
	freq = core_freqs_table[cur_op];
	return freq * MHZ_TO_KHZ;
}

static void set_constraint(unsigned int min, unsigned int max)
{
	unsigned int i;

	for (i = min; i <= max; i++)
		dvfm_disable_op(i, dvfm_dev_idx);
	return ;
}

static void unset_constraint(unsigned int min, unsigned int max)
{
	unsigned int i;

	for (i = min; i <= max; i++)
		dvfm_enable_op(i, dvfm_dev_idx);
	return ;
}

static void mmp2_cpufreq_constraint(struct cpufreq_policy *data,
					struct cpufreq_policy *policy)
{
	unsigned int old_min = 0, old_max = 0;
	unsigned int new_min = 0, new_max = 0;
	unsigned int freq_khz = 0;
	unsigned int i = 0;

	for (i = 0; i < mmp2_freqs_num; i++) {
		freq_khz = mmp2_freqs_table[i].frequency;
		if (freq_khz == data->min)
			old_min = i;
		if (freq_khz == data->max)
			old_max = i;
		if (freq_khz == policy->min)
			new_min = i;
		if (freq_khz == policy->max)
			new_max = i;
	}

	if (old_min < new_min)
		set_constraint(old_min, new_min - 1);
	if (old_min > new_min)
		unset_constraint(new_min, old_min - 1);
	if (old_max > new_max)
		set_constraint(new_max + 1, old_max);
	if (old_max < new_max)
		unset_constraint(old_max + 1, new_max);
}

static struct freq_attr *mmp2_freqs_attr[] = {
	&cpufreq_freq_attr_scaling_available_freqs,
	NULL,
};

static struct cpufreq_driver mmp2_cpufreq_driver = {
	.verify		= mmp2_cpufreq_verify,
	.target		= mmp2_cpufreq_target,
	.init		= mmp2_cpufreq_init,
	.get		= mmp2_cpufreq_get,
	.getavg		= mmp2_cpufreq_getavg,
	.constraint	= mmp2_cpufreq_constraint,
	.name		= "mmp2-cpufreq",
	.attr		= mmp2_freqs_attr,
};

static int __init cpufreq_init(void)
{
	int ret;
#ifdef CONFIG_DVFM
	dvfm_register("cpufreq", &dvfm_dev_idx);
#endif
	ret = sysdev_driver_register(&cpu_sysdev_class, &cpufreq_stats_driver);
	if (ret)
		printk(KERN_ERR "Can't register cpufreq STATS in sysfs\n");

	return cpufreq_register_driver(&mmp2_cpufreq_driver);
}
module_init(cpufreq_init);

static void __exit cpufreq_exit(void)
{
#ifdef CONFIG_DVFM
	dvfm_unregister("cpufreq", &dvfm_dev_idx);
#endif
	sysdev_driver_register(&cpu_sysdev_class, &cpufreq_stats_driver);
	cpufreq_unregister_driver(&mmp2_cpufreq_driver);
}
MODULE_DESCRIPTION("CPU frequency scaling driver for MMP2");
MODULE_LICENSE("GPL");

