#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/err.h>
#include <linux/hrtimer.h>
#include <linux/timed_output.h>

static struct work_struct vibrator_work;
static struct hrtimer vibe_timer;
static spinlock_t vibe_lock;
static int vibe_state;

static void set_vibrator(int on)
{
	int gpio = GPIO(57);
	/* set 3.3V power */
	if (gpio_request(gpio, "vibrator enable gpio")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio);
		return;
	}

	if (on) 
	{
		gpio_direction_output(gpio, 1);
	} 
	else 
	{
		gpio_direction_output(gpio, 0);
	}

	gpio_free(gpio);

}

static void update_vibrator(struct work_struct *work)
{
      set_vibrator(vibe_state);
}

static void vibrator_enable(struct timed_output_dev *dev, int value)
{
      unsigned long   flags;

      spin_lock_irqsave(&vibe_lock, flags);
      hrtimer_cancel(&vibe_timer);

      if (value == 0)
              vibe_state = 0;
      else {
              value = (value > 15000 ? 15000 : value);
			  printk("enable vibrator %d ms\n",value);
              vibe_state = 1;
              hrtimer_start(&vibe_timer,
                      ktime_set(value / 1000, (value % 1000) * 1000000),
                      HRTIMER_MODE_REL);
      }
      spin_unlock_irqrestore(&vibe_lock, flags);

      schedule_work(&vibrator_work);
}

static int vibrator_get_time(struct timed_output_dev *dev)
{
      if (hrtimer_active(&vibe_timer)) {
              ktime_t r = hrtimer_get_remaining(&vibe_timer);
              return r.tv.sec * 1000 + r.tv.nsec / 1000000;
      } else
              return 0;
}

static enum hrtimer_restart vibrator_timer_func(struct hrtimer *timer)
{
      vibe_state = 0;
      schedule_work(&vibrator_work);
      return HRTIMER_NORESTART;
}

static struct timed_output_dev vibrator = {
      .name = "vibrator",
      .get_time = vibrator_get_time,
      .enable = vibrator_enable,
};

void __init board_vibrator_init(void)
{
      INIT_WORK(&vibrator_work, update_vibrator);

      spin_lock_init(&vibe_lock);
      vibe_state = 0;
      hrtimer_init(&vibe_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
      vibe_timer.function = vibrator_timer_func;

      timed_output_dev_register(&vibrator);
}


