#include <linux/lsm303dlh.h>
#include <linux/power/ads1000.h>
#include <linux/input/questers_ts.h>
static struct lsm303dlh_platform_data lsm303dlh_pdata=
{
	.name_a="lsm303dlh_a",
	.name_m="lsm303dlh_m", 

	/*	interrupt data */
	.irq_a1=GPIO(129),
	.irq_a2=-1,
	.irq_m=GPIO(45),

	/* position of x,y and z axis */
	.axis_map_x=1, /* [0-2] */
	.axis_map_y=0, /* [0-2] */
	.axis_map_z=2, /* [0-2] */

	/* orientation of x,y and z axis */
	.negative_x=-1,
	.negative_y=0,
	.negative_z=0,
};
//for power i2c workaround
static struct mutex twsi_bus1_access_lock;
void pwr_twsi_bus_lock(int lock)
{	
	if(lock)
		mutex_lock(&twsi_bus1_access_lock);
	else
		mutex_unlock(&twsi_bus1_access_lock);
}
/*
	TWSI bus info
	Change all bus to normal speed(100kHz) because the slew rate is solow that some devices can accept.
*/
static struct i2c_pxa_platform_data twsi_bus1_info  = {
	.use_pio  = 1,
	.fast_mode =0,
	.bus_lock = pwr_twsi_bus_lock,
};

//slow down i2c speed @100kHz to keep stability
static struct i2c_pxa_platform_data twsi_bus2_info __initdata = {
	.fast_mode = 0,
	.use_pio		= 1,
};

/*
static struct i2c_pxa_platform_data twsi_bus3_info __initdata = {
	.fast_mode = 0,
};

static struct i2c_pxa_platform_data twsi_bus4_info __initdata = {
	.fast_mode = 0,
};

static struct i2c_pxa_platform_data twsi_bus6_info __initdata = {
	.fast_mode = 0,
};*/

/*TWSI bus1 board info*/
#if defined(CONFIG_BATTERY_ADS1000)
static struct ads1000_platform_data ads1000_data =
{
	.gpio_dc_in = GPIO(123),
	.gpio_bat_full = GPIO(161),
	.monitor_interval_ms = 2000,
};

#endif
static struct i2c_board_info twsi1_board_info[] = {
#if defined(CONFIG_BATTERY_ADS1000)
	{
		.type		= "ads1000",
		.addr		= 0x48,
		.platform_data = &ads1000_data,
	},
	{
		.type		= "ads1000_bd1",
		.addr		= 0x49,
		.platform_data = &ads1000_data,
	},
#endif

#if defined(CONFIG_PXA688_CAMERA)
	{
		.type			= "gc308",
		.addr			= 0x21,
		.platform_data		= &camera_sensor_data_low,
	},
	{
		.type			= "gc2015",
		.addr			= 0x30,
		.platform_data		= &camera_sensor_data_low,
	},
	{
		.type			= "hm2055",
		.addr			= 0x24,
		.platform_data		= &camera_sensor_data_low,
	},
#endif
};

/*TWSI bus2 board info*/
static int rt5625_reset(void)
{
	int reset = GPIO(50);
	if (gpio_request(reset, "audiocodec reset")) {
		printk(KERN_INFO "gpio %d request failed\n", reset);
		return -1;
	}
	gpio_direction_output(reset,0);
	msleep(50);
	gpio_direction_output(reset,1);	
	gpio_free(reset);
	return 0;
}

static struct i2c_board_info twsi2_board_info[] = {
	{
		.type                   = "rt5625",
		.addr                   = 0x1F,
	},
	
	{
		.type	= "lsm303dlh_m",
		.addr	= 0x1E,
		.platform_data=&lsm303dlh_pdata,   
	},
#if defined(CONFIG_RTC_DRV_PCF8563)
	{
		.type		= "pcf8563-rtc",
		.addr		= 0x51,
		.irq		= IRQ_GPIO(36),
	}
#endif
};

/*TWSI bus3 board info*/
static struct i2c_board_info twsi3_board_info[] =
{
#if defined(CONFIG_PXA688_CAMERA)
	{
		.type			= "gc2015",
		.addr			= 0x30,
		.platform_data		= &camera_sensor_data_high,
	},
	{
		.type			= "gc308",
		.addr			= 0x21,
		.platform_data		= &camera_sensor_data_high,
	},
	{
		.type			= "hm2055",
		.addr			= 0x24,
		.platform_data		= &camera_sensor_data_high,
	},
	{
		.type			= "ov5640",
		.addr			= 0x3c,
		.platform_data		= &camera_sensor_data_high,
	},
#endif
	{
		.type   = "lsm303dlh_a",
		.addr   = 0x18,//SA0=0
		.platform_data=&lsm303dlh_pdata,   
	},
	{ /* gsensor */
		.type	= "lis35de",    
		.addr	= 0x1C, 
		.irq	= IRQ_GPIO(91),
	},
	{
		//gravity sensor
		.type	= "lis33de",
		.addr	= 0x1D,	/*0x3a/0x3b*/
		.irq	= IRQ_GPIO(106),	
	},		
	{ /* gyroscope */
		.type	= "L3G4200D",	
		.addr	= 0x68, 
		.irq	= IRQ_GPIO(44),
	},
	
};


static int nastouch_reset(void)
{
	int reset = GPIO(141);
	if (gpio_request(reset, "TP reset")) {
		printk(KERN_INFO "gpio %d request failed\n", reset);
		return -1;
	}
	printk("reset nas touchpanel\n");
	gpio_direction_output(reset,0);
	msleep(100);
	gpio_direction_output(reset,1);	
	msleep(150);	
	gpio_free(reset);

	return 0;
}
static struct touch_platform_data nas_pdata = 
{
	.reset 		= nastouch_reset,
	.quirks		= 0,//TOUCH_QUIRKS_BREAK_SUSPEND,
};


static int ctpm_reset(void)
{
#if defined(CONFIG_P200)
    int reset = GPIO(119);
#else
	int reset = GPIO(141);
#endif
	if (gpio_request(reset, "TP reset")) {
		printk(KERN_INFO "gpio %d request failed\n", reset);
		return -1;
	}
	printk("reset ctpm touchpanel\n");
	gpio_direction_output(reset,0);
	msleep(100);
	gpio_direction_output(reset,1);	
	msleep(150);	
	gpio_free(reset);

	return 0;
}

#ifdef CONFIG_P200
static int touch_set_led(int on){
    int boardid = g50_get_board_version();
    //only support on revision 2 or above
    if(2<=boardid){
        int gpio_led = GPIO(99);
         if (!gpio_request(gpio_led, "touchkey led")){
                gpio_direction_output(gpio_led, !!!on);
                gpio_free(gpio_led);
         }else
            return -1;

    }
    return 0;
}
static int touch_plat_init(void){
    int boardid = g50_get_board_version();
    //only support on revision 2 or above
    if(2<=boardid){
        mfp_cfg_t touch_led_pin_cfg = GPIO99_GPIO99;
        mfp_config(&touch_led_pin_cfg,1);
    }

    return 0;
    
}
#endif
static struct touch_platform_data ctpm_pdata = 
{
	.reset 		= ctpm_reset,

	/*
	 * quirks:
	 * TOUCH_QUIRKS_X_INVERTED
	 * TOUCH_QUIRKS_Y_INVERTED
	 * TOUCH_QUIRKS_XY_SWAP
	 */
#if defined(CONFIG_BUTTERFLY)
	.quirks 	= TOUCH_QUIRKS_X_INVERTED | TOUCH_QUIRKS_Y_INVERTED,
#elif defined(CONFIG_P200)
	.quirks 	= 0,
    .set_led    = touch_set_led,
    .init       = touch_plat_init,
#endif
};

static int ite_set_power(int on)
{
	int vdd = 108;
	int intr = 115;

	if (gpio_request(intr, "TP intr")) {
		printk(KERN_INFO "gpio %d request failed\n", intr);
		return -1;
	}

	if (gpio_request(vdd, "TP vdd")) {
		printk(KERN_INFO "gpio %d request failed\n", vdd);
		gpio_free(intr);
		return -1;
	}

	printk("Set ITE touchpanel power %s\n", on?"on":"off");
	if (on)
	{
		gpio_direction_output(intr, 0);
		gpio_direction_output(vdd, 1); /* reverse logic */
		msleep(5);

		gpio_direction_output(vdd, 0);
		msleep(10); /* >10ms */
		gpio_direction_output(intr, 1);
		msleep(5);
		gpio_direction_input(intr);
	}
	else
	{
		gpio_direction_output(intr, 0);
		gpio_direction_output(vdd, 1);
	}

	gpio_free(vdd);
	gpio_free(intr);

	return 0;
}


static struct touch_platform_data ite_pdata = 
{
	.power		= ite_set_power,
	.quirks 	= 0,
};

/*TWSI bus4 board info*/
static struct i2c_board_info twsi4_board_info[] =
{
	{/* light sensor*/
		.type		= "stk2201",    
		.addr		= 0x10,
	},
	{
		//touch key
		.type	= "so340010",
		.addr	= 0x2c, 
		.irq	= IRQ_GPIO(49),
	},
	{
		//nas touch panel
		.type	= "ntp-ax1",
		.addr	= 0x5c,
		.irq	= IRQ_GPIO(115),
		.platform_data = &nas_pdata,
	},
	{
		//ctpm touch panel
		.type	= "ctpm",
		.addr	= 0x38,
		.irq	= IRQ_GPIO(115),
		.platform_data = &ctpm_pdata,
	},	
	{
		//ite7260
		.type	= "ite7260",
		.addr	= 0x46,
		.irq	= IRQ_GPIO(115),
		.platform_data = &ite_pdata,
	},	
};

/*TWSI bus6 board info*/
static struct i2c_board_info twsi6_board_info[] =
{
#if defined(CONFIG_HDMI_DDC_EDID)
 	{
 		.type			= "hdmi_edid",
 		.addr			= 0x50,
 	},
#endif
};

static void __init board_i2c_init(void)
{
	rt5625_reset();//FIXME,move to codec driver?
	mutex_init(&twsi_bus1_access_lock);
	mmp2_add_twsi(1, &twsi_bus1_info, ARRAY_AND_SIZE(twsi1_board_info));
	mmp2_add_twsi(2, &twsi_bus2_info, ARRAY_AND_SIZE(twsi2_board_info));
	mmp2_add_twsi(3, NULL, ARRAY_AND_SIZE(twsi3_board_info));
	mmp2_add_twsi(4, NULL, ARRAY_AND_SIZE(twsi4_board_info));
	mmp2_add_twsi(6, NULL, ARRAY_AND_SIZE(twsi6_board_info));
}


