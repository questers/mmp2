/*
 * linux/arch/arm/mach-pxa/mfp.c
 *
 * MMP2 Multi-Function Pin Support
 *
 * Copyright (C) 2007 Marvell Internation Ltd.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/io.h>
#include <linux/sysdev.h>

#include <mach/hardware.h>
#include <mach/mfp-mmp2.h>
#include <mach/cputype.h>

#ifdef CONFIG_PM
/*
 * Configure the MFPs appropriately for suspend/resume.
 * FIXME: this should probably depend on which system state we're
 * entering - for instance, we might not want to place MFP pins in
 * a pull-down mode if they're an active low chip select, and we're
 * just entering standby.
 */
static int mmp2_mfp_suspend(struct sys_device *d, pm_message_t state)
{
	mfp_config_lpm();
	return 0;
}

static int mmp2_mfp_resume(struct sys_device *d)
{
	mfp_config_run();
	return 0;
}
#else
#define mmp2_mfp_suspend	NULL
#define mmp2_mfp_resume		NULL
#endif

struct sysdev_class mmp2_mfp_sysclass = {
	.name		= "mfp",
	.suspend	= mmp2_mfp_suspend,
	.resume 	= mmp2_mfp_resume,
};

static struct sys_device mmp2_mfp_device = {
	.cls = &mmp2_mfp_sysclass,
};

static int __init mfp_init_devicefs(void)
{
	int ret;
	if (cpu_is_mmp2()) {
		ret = sysdev_class_register(&mmp2_mfp_sysclass);
		if (!ret)
			sysdev_register(&mmp2_mfp_device); 
	}
	return 0;
}
postcore_initcall(mfp_init_devicefs);
