/*
 *  linux/arch/arm/mach-mmp/common.c
 *
 *  Code common to PXA168 processor lines
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/init.h>
#include <linux/kernel.h>

#include <asm/page.h>
#include <asm/mach/map.h>
#include <mach/addr-map.h>
#include <plat/generic.h>

#include "common.h"

#include <linux/sched.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/vmalloc.h>
#include <mach/pxa168fb.h>

int	mmp2_platform_version = -1;

static struct map_desc standard_io_desc[] __initdata = {
	{
		.pfn		= __phys_to_pfn(APB_PHYS_BASE),
		.virtual	= APB_VIRT_BASE,
		.length		= APB_PHYS_SIZE,
		.type		= MT_DEVICE,
	}, {
		.pfn		= __phys_to_pfn(AXI_PHYS_BASE),
		.virtual	= AXI_VIRT_BASE,
		.length		= AXI_PHYS_SIZE,
		.type		= MT_DEVICE,
	}, {
		.pfn		= __phys_to_pfn(USB_PHYS_BASE),
		.virtual	= USB_VIRT_BASE,
		.length		= USB_PHYS_SIZE,
		.type		= MT_DEVICE,
	}, {
		.pfn		= __phys_to_pfn(BOOTROM_PHYS_BASE),
		.virtual	= BOOTROM_VIRT_BASE,
		.length		= BOOTROM_PHYS_SIZE,
		.type		= MT_DEVICE,
	},
};

void __init pxa_map_io(void)
{
	pxa_reserve_early_dram();
	iotable_init(standard_io_desc, ARRAY_SIZE(standard_io_desc));
}

static unsigned long uva_to_pa(unsigned long addr, struct page **page)
{
	unsigned long ret = 0UL;
	pgd_t *pgd;
	pud_t *pud;
	pmd_t *pmd;
	pte_t *pte;

	pgd = pgd_offset(current->mm, addr);
	if (!pgd_none(*pgd)) {
		pud = pud_offset(pgd, addr);
		if (!pud_none(*pud)) {
			pmd = pmd_offset(pud, addr);
			if (!pmd_none(*pmd)) {
				pte = pte_offset_map(pmd, addr);
				if (!pte_none(*pte) && pte_present(*pte)) {
					(*page) = pte_page(*pte);
					ret = page_to_phys(*page);
					ret |= (addr & (PAGE_SIZE-1));
				}
			}
		}
	}
	return ret;
}

struct page* va_to_page(unsigned long user_addr)
{
	struct page *page = NULL;
	unsigned int vaddr = PAGE_ALIGN(user_addr);

	if (uva_to_pa(vaddr, &page) != 0UL)
		return page;

	return 0;
}

unsigned long va_to_pa(unsigned long user_addr, unsigned int size)
{
	unsigned long  paddr, paddr_tmp;
	unsigned long  size_tmp = 0;
	struct page *page = NULL;
	int page_num = PAGE_ALIGN(size) / PAGE_SIZE;
	unsigned int vaddr = PAGE_ALIGN(user_addr);
	int i = 0;

	if(vaddr == 0)
		return 0;

	paddr = uva_to_pa(vaddr, &page);

	for (i = 0; i < page_num; i++) {
		paddr_tmp = uva_to_pa(vaddr, &page);
		if ((paddr_tmp - paddr) != size_tmp)
			return 0;
		vaddr += PAGE_SIZE;
		size_tmp += PAGE_SIZE;
	}
	return paddr;
}

void memset32(unsigned long *dst, unsigned int value, size_t size)
{
	size >>= 2;
	while (size--)
		*dst++ = value;
}

void memset16(unsigned short *dst, unsigned short value, size_t size)
{
	size >>= 1;
	while (size--)
		*dst++ = value;
}

int get_bpp_fmt(int pix_fmt)
{
	switch (pix_fmt) {
	case PIX_FMT_RGB565:
	case PIX_FMT_BGR565:
	case PIX_FMT_RGB1555:
	case PIX_FMT_BGR1555:
		return 0;
	case PIX_FMT_RGB888UNPACK:
	case PIX_FMT_BGR888UNPACK:
		return 1;
	default:
		return -1;
	}
}

short rgb16_to_dsts(unsigned short rgb16_pixel, int pix_fmt)
{
	/* from rrrr rggg gggb bbbb */
	unsigned short r_s = (rgb16_pixel & 0xf800) >> 11;
	unsigned short g_s = (rgb16_pixel & 0x07e0) >> 5;
	unsigned short b_s = (rgb16_pixel & 0x001f);

	switch (pix_fmt) {
	case PIX_FMT_RGB565:
		return rgb16_pixel;
	case PIX_FMT_BGR565:
		return (b_s << 11) | (g_s << 5) | r_s;
	case PIX_FMT_RGB1555:
		g_s = g_s * 31 / 63;
		return (r_s << 10) | (g_s << 5) | b_s;
	case PIX_FMT_BGR1555:
		g_s = g_s * 31 / 63;
		return (b_s << 10) | (g_s << 5) | r_s;
	default:
		return 0;
	}
}

long rgb16_to_dstl(unsigned short rgb16_pixel, int pix_fmt)
{
	/* from rrrr rggg gggb bbbb */
	unsigned long r_l = (rgb16_pixel & 0xf800) >> 11;
	unsigned long g_l = (rgb16_pixel & 0x07e0) >> 5;
	unsigned long b_l = (rgb16_pixel & 0x001f);

	switch (pix_fmt) {
	case PIX_FMT_RGB888UNPACK:
		r_l = r_l * 255 / 31;
		g_l = g_l * 255 / 63;
		b_l = b_l * 255 / 31;
		return (r_l << 16) | (g_l << 8) | b_l;
	case PIX_FMT_BGR888UNPACK:
		r_l = r_l * 255 / 31;
		g_l = g_l * 255 / 63;
		b_l = b_l * 255 / 31;
		return (b_l << 16) | (g_l << 8) | r_l;
	default:
		return 0;
	}
}

void *rvmalloc(unsigned long size)
{
	void *mem;
	unsigned long adr;

	size = PAGE_ALIGN(size);
	mem = vmalloc_32(size);
	if (!mem)
		return NULL;

	memset(mem, 0, size); /* Clear the ram out, no junk to the user */
	adr = (unsigned long) mem;
	while (size > 0) {
		SetPageReserved(vmalloc_to_page((void *)adr));
		adr += PAGE_SIZE;
		size -= PAGE_SIZE;
	}
	return mem;
}

void rvfree(void *mem, unsigned long size)
{
	unsigned long adr;

	if (!mem)
		return;

	adr = (unsigned long) mem;
	while ((long) size > 0) {
		ClearPageReserved(vmalloc_to_page((void *)adr));
		adr += PAGE_SIZE;
		size -= PAGE_SIZE;
	}
	vfree(mem);
}

