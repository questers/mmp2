#define ARRAY_AND_SIZE(x)	(x), ARRAY_SIZE(x)

struct sys_timer;
typedef int (*set_wake)(unsigned int, unsigned int);

extern void timer_init(int irq1, int irq2);
extern void mmp2_clear_pmic_int(void);

extern struct sys_timer pxa168_timer;
extern struct sys_timer pxa910_timer;
extern struct sys_timer mmp2_timer;
extern void __init pxa168_init_irq(void);
extern void __init pxa910_init_irq(void);
extern void __init mmp2_init_icu(set_wake fn);
extern void __init mmp2_init_irq(void);

extern void __init icu_init_irq(void);
extern void __init pxa_map_io(void);
extern struct page* va_to_page(unsigned long user_addr);
extern unsigned long va_to_pa(unsigned long user_addr, unsigned int size);

extern void memset32(unsigned long *dst, unsigned int value, size_t size);
extern void memset16(unsigned short *dst, unsigned short value, size_t size);
extern int get_bpp_fmt(int pix_fmt);
extern short rgb16_to_dsts(unsigned short rgb16_pixel, int pix_fmt);
extern long rgb16_to_dstl(unsigned short rgb16_pixel, int pix_fmt);
extern void *rvmalloc(unsigned long size);
extern void rvfree(void *mem, unsigned long size);

