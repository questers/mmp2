int is_android(void);
extern size_t reserving_size;
int pxa_reserve_early_dram(void);
#ifdef CONFIG_ANDROID_PMEM
#include <linux/android_pmem.h>
void pxa_add_pmem(char *name, size_t size,
		  enum pmem_allocator_type allocator,
		  int cached, int buffered);
#endif
