/* linux/arch/arm/plat-pxa/include/plat/sdhci.h
 *
 * Copyright 2010 Marvell
 *	Zhangfei Gao <zhangfei.gao@marvell.com>
 *
 * PXA Platform - SDHCI platform data definitions
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#ifndef __PLAT_PXA_SDHCI_H
#define __PLAT_PXA_SDHCI_H

#include <linux/mmc/sdhci.h>
#include <linux/mmc/host.h>
#include <linux/interrupt.h>

struct mmc_host;
struct device;

/* pxa specific flag */
/* Require clock free running */
#define PXA_FLAG_DISABLE_CLOCK_GATING (1<<0)
/* card alwayes wired to host, like on-chip emmc */
#define PXA_FLAG_CARD_PERMANENT	(1<<1)
/* mmc v3 controller could support clock gating */
#define PXA_FLAG_CONTROL_CLK_GATE (1<<3)
/* keep power always on and skip resume probe for Marvell 8686/8688 wifi */
#define PXA_FLAG_SDIO_RESUME (1<<5)

/**
 * struct pxa_sdhci_platdata() - Platform device data for PXA SDHCI
 * @max_speed: the maximum speed supported
 * @quirks: quirks of specific device
 * @flags: flags for platform requirement
 * @clk_delay_cycles:
 *	mmp2: each step is roughly 100ps, 5bits width
 *	pxa910 & pxa168: each step is 1ns, 4bits width
 * @clk_delay_enable: enable clk_delay or not, used on pxa910 & pxa168
 * @clk_delay_sel: select clk_delay, used on pxa910 & pxa168
 *	0: choose feedback clk
 *	1: choose feedback clk + delay value
 *	2: choose internal clk
 * @ext_cd_gpio: gpio pin used for external CD line
 * @ext_cd_gpio_invert: invert values for external CD gpio line
 * @soc_set_timing: set timing for specific soc
 * @ext_cd_init: Initialize external card detect subsystem
 * @ext_cd_cleanup: Cleanup external card detect subsystem
 * @soc_set_ops: overwrite host ops with soc specific ops
 */
struct sdhci_pxa;
struct sdhci_pxa_platdata {
	unsigned int	max_speed;
	unsigned int	quirks;
	unsigned int	flags;
	unsigned int	clk_delay_cycles;
	unsigned int	clk_delay_sel;
	unsigned int	ext_cd_gpio;
	bool		ext_cd_gpio_invert;
	bool		clk_delay_enable;

	void (*soc_set_timing)(struct sdhci_host *host,
			struct sdhci_pxa_platdata *pdata);
	int (*ext_cd_init)(void (*notify_func)(struct platform_device *dev,
						   int state), void *data);
	int (*ext_cd_cleanup)(void (*notify_func)(struct platform_device *dev,
						      int state), void *data);
	void (*soc_set_ops)(struct sdhci_pxa *pxa);

	void (*setpower)(struct device *, unsigned int);
	/* slot operations needed while going in/out low-power mode */
	void (*lp_switch)(unsigned int on, int with_card);

#ifdef CONFIG_SD8XXX_RFKILL
	/*for sd8688-rfkill device*/
	struct mmc_host **pmmc;
#endif
};

struct sdhci_pxa {
	struct sdhci_host		*host;
	struct sdhci_pxa_platdata	*pdata;
	struct clk			*clk;
	struct resource			*res;
	struct sdhci_ops		*ops;

	u8	clk_enable;
	u8	power_mode;
};

#endif /* __PLAT_PXA_SDHCI_H */
