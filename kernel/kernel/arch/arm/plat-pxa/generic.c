/*
 *  linux/arch/arm/plat-pxa/generic.c
 *
 *  Code to PXA processor lines
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/bootmem.h>
#include <asm/page.h>

#include <plat/generic.h>

static int android_project = 0;
static int __init android_setup(char *__unused)
{
	android_project = 1;
	return 1;
}
__setup("android", android_setup);

int is_android(void)
{
	return android_project;
}
EXPORT_SYMBOL(is_android);

size_t __initdata reserving_size;
static int __init reserve_dram(char *arg)
{
	reserving_size = memparse(arg, NULL);
	return 0;
}
early_param("reserve_dram", reserve_dram);

/* Note that we have to call it from map_io() that is placed
 * between bootmem_init() and mm_init().
 */
static unsigned long __initdata dram_reserve;
int __init pxa_reserve_early_dram(void)
{
	void *va_reserve;
	if (!reserving_size)
		return 0;
	va_reserve = alloc_bootmem_pages(reserving_size);
	if (WARN_ON(!va_reserve))
		return -ENOMEM;
	dram_reserve = virt_to_phys(va_reserve);
	return 0;
}

#ifdef CONFIG_ANDROID_PMEM
void __init pxa_add_pmem(char *name, size_t size,
			 enum pmem_allocator_type allocator,
			 int cached, int buffered)
{
	struct platform_device *android_pmem_device;
	struct android_pmem_platform_data *android_pmem_pdata;
	static int id;

	if (size > PAGE_SIZE && size > reserving_size)
		return;
	android_pmem_device = kzalloc(sizeof(struct platform_device), GFP_KERNEL);
	if(android_pmem_device == NULL)
		return ;

	android_pmem_pdata = kzalloc(sizeof(struct android_pmem_platform_data), GFP_KERNEL);
	if(android_pmem_pdata == NULL) {
		kfree(android_pmem_device);
		return ;
	}

	if (size > PAGE_SIZE) {
		android_pmem_pdata->allocator = allocator;
		android_pmem_pdata->start = dram_reserve;
		android_pmem_pdata->size = size;
		dram_reserve += size;
		reserving_size -= size;
	} else {
		android_pmem_pdata->start = size;
		android_pmem_pdata->size = 0;
	}
	android_pmem_pdata->name = name;
	android_pmem_pdata->cached = cached;
	android_pmem_pdata->buffered = buffered;

	android_pmem_device->name = "android_pmem";
	android_pmem_device->id = id++;
	android_pmem_device->dev.platform_data = android_pmem_pdata;

	platform_device_register(android_pmem_device);
}
#endif

