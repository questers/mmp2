/*
 * include/linux/power/usbcharger.h
 * USB Charger support headfile.
 *
 * Copyright (C) 2010 Marvell International Ltd.
 *
 * Yunfan Zhang <yfzhang@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _LINUX_POWER_USBCHARGER_H
#define _LINUX_POWER_USBCHARGER_H

/* usb enumeration status */
enum usb_enum_status_t {
	USB_ENUM_NONE,
	USB_ENUM_START,
	USB_ENUM_500MA,
	USB_ENUM_100MA,
	USB_ENUM_FAILED,
};

#endif
