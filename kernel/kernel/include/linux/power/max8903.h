/*
 * include/linux/power/max8903.h
 * Main Battery Charger Max8903
 *
 * Copyright (C) 2010 Marvell International Ltd.
 *
 * Yunfan Zhang <yfzhang@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#ifndef _LINUX_POWER_MAX8903_H
#define _LINUX_POWER_MAX8903_H

enum max8903_chgcur_t {
	/* MAX fast charging current*/
	MAX8903_FCHG_DISABLE, /* disable charging*/
	MAX8903_FCHG_100MA,   /* usb 100mA */
	MAX8903_FCHG_500MA,   /* usb 500mA */
	MAX8903_FCHG_2000MA,  /* adapter */
};

struct max8903_charger_pdata {
	/* GPIO: charging current control */
	int gpio_2a_n;
	int gpio_500ma;

	/* default fast charging current */
	enum max8903_chgcur_t fast_chgcur;
};

struct max8903_charger_info {
	/* Max Charging Current Limitation
	 * -----------------------------------------
	 * CHARGING |chg_2a_n | chg_500ma | vbus_en
	 * ---------|---------|-----------|---------
	 *   2A     |    0    |     x     |    0
	 * ---------|---------|-----------|---------
	 *  500MA   |    1    |     1     |    0
	 * ---------|---------|-----------|---------
	 *  100MA   |    1    |     0     |    0
	 * ---------|---------|-----------|---------
	 * DISABLED |    1    |     x     |    1
	 * -----------------------------------------
	 */
	/* vbus_en is controled by vbus driver.
	 * if usb otg as host(vbus_en = 1), charger should be disabled */
	int chg_2a_n;  /* GPIO: DCM  */
	int chg_500ma; /* GPIO: IUSB */

	int (*init_chgcur)(struct max8903_charger_info *info);
	int (*set_chgcur)(int chgcur, int enable);

	/* max fast charging current setting */
	enum max8903_chgcur_t fast_chgcur;

	int (*update_charger)(int status); /* update charger */
};

extern int max8903_update_charger(int status);
extern int max8903_set_chgcur(int chgcur, int status);

#endif
