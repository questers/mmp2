/*
 * Generic PWM backlight driver data - see drivers/video/backlight/pwm_bl.c
 */
#ifndef __LINUX_PWM_BACKLIGHT_H
#define __LINUX_PWM_BACKLIGHT_H

#define PWM_BACKLIGHT_INVERTED	0x01
struct platform_pwm_backlight_data {
	int pwm_id;
	int quirks;
	unsigned int max_brightness;
	unsigned int dft_brightness;
	unsigned int pwm_period_ns;
	int (*init)(struct device *dev);
	int (*notify)(struct device *dev, int brightness);
	void (*exit)(struct device *dev);
};

#endif
