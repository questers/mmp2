
#ifndef __QUESTERS_TS_H
#define __QUESTERS_TS_H

#define TOUCH_POWER_ON 	1
#define TOUCH_POWER_OFF	0

#define TOUCH_QUIRKS_BREAK_SUSPEND 0x01
#define TOUCH_QUIRKS_XY_SWAP	   0x02
#define TOUCH_QUIRKS_X_INVERTED	   0x04
#define TOUCH_QUIRKS_Y_INVERTED	   0x08

struct touch_platform_data
{
    int (*init)(void);
	int (*power)(int on);
	int (*reset)(void);
	int (*wakeup)(void);
	int (*set_led)(int on);
	
	unsigned int quirks; 

};


#endif /*  __QUESTERS_TS_H */
