/*
 *
 * Copyright (C) 2006, Marvell Corporation.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef __MACH_GPIO_ONKEY_H
#define __MACH_GPIO_ONKEY_H

struct gpio_onkey_pdata {
	int		onkey_gpio;
	int		pwroff_gpio;
	int		reboot_gpio;
};

#endif
