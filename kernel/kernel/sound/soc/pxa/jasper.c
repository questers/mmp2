/*
 * linux/sound/soc/pxa/jasper.c
 *
 * Copyright (C) 2011 Marvell International Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/timer.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/suspend.h>
#include <linux/switch.h>
#include <linux/i2c.h>
#include <linux/clk.h>
#include <linux/mfd/wm8994/registers.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/soc.h>
#include <sound/jack.h>
#include <sound/soc-dapm.h>

#include <asm/mach-types.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <plat/ssp.h>
#include <mach/addr-map.h>
#include <mach/regs-sspa.h>
#include <mach/regs-mpmu.h>
#include <mach/regs-apmu.h>

#include "../codecs/wm8994.h"
#include "mmp2-squ.h"
#include "mmp2-sspa.h"
#include "jasper.h"

static struct clk *audio_clk;

static const struct snd_soc_dapm_widget jasper_dapm_widgets[] = {
	SND_SOC_DAPM_SPK("Ext Left Spk", NULL),
	SND_SOC_DAPM_SPK("Ext Right Spk", NULL),
	SND_SOC_DAPM_HP("Headset Stereophone", NULL),
	SND_SOC_DAPM_MIC("Headset Mic", NULL),
	SND_SOC_DAPM_MIC("Main Mic", NULL),
};

static const struct snd_soc_dapm_route jasper_dapm_routes[] = {
	{"Ext Left Spk", NULL, "SPKOUTLP"},
	{"Ext Left Spk", NULL, "SPKOUTLN"},

	{"Ext Right Spk", NULL, "SPKOUTRP"},
	{"Ext Right Spk", NULL, "SPKOUTRN"},

	{"Headset Stereophone", NULL, "HPOUT1L"},
	{"Headset Stereophone", NULL, "HPOUT1R"},

	{"IN1RN", NULL, "Headset Mic"},

	{"IN2LN", NULL, "MICBIAS1"},
	{"MICBIAS1", NULL, "Main Mic"},
};

#ifdef CONFIG_SWITCH_HEADSET
static struct snd_soc_codec *jasper_wm8994_codec;

int wm8994_headset_detect(void)
{
	struct snd_soc_codec *codec;
	int status1, status2, reg;
	int ret = 0;

	codec = jasper_wm8994_codec;
	if (codec == NULL)
		return 0;

	status1 = snd_soc_read(codec, WM8994_INTERRUPT_STATUS_1);
	status2 = snd_soc_read(codec, WM8994_INTERRUPT_STATUS_2);

	reg = snd_soc_read(codec, WM8994_INTERRUPT_RAW_STATUS_2);
	switch (reg & (WM8994_MIC2_SHRT_STS | WM8994_MIC2_DET_STS)) {
	case WM8994_MIC2_DET_STS:
		ret = 1;
		break;
	case (WM8994_MIC2_SHRT_STS | WM8994_MIC2_DET_STS):
		ret = 2;
		break;
	default:
		break;
	}

	/* clear all irqs */
	snd_soc_write(codec, WM8994_INTERRUPT_RAW_STATUS_2, 0);
	snd_soc_write(codec, WM8994_INTERRUPT_STATUS_1, status1);
	snd_soc_write(codec, WM8994_INTERRUPT_STATUS_2, status2);

	/* reset clocking to make sure trigger irq */
	snd_soc_update_bits(codec, WM8994_CLOCKING_2,
			WM8994_DBCLK_DIV_MASK |
			WM8994_TOCLK_DIV_MASK,
		        (2 << WM8994_DBCLK_DIV_SHIFT) |
			(4 << WM8994_TOCLK_DIV_SHIFT));

	snd_soc_update_bits(codec, WM8994_CLOCKING_1,
			WM8994_TOCLK_ENA_MASK,
			WM8994_TOCLK_ENA);
	return ret;
}
#endif

static int codec_hdmi_init(struct snd_soc_codec *codec)
{
	return 0;
}

static int codec_wm8994_init(struct snd_soc_codec *codec)
{
	/* add jasper specific widgets */
	snd_soc_dapm_new_controls(codec, jasper_dapm_widgets,
				  ARRAY_SIZE(jasper_dapm_widgets));

	/* set up jasper specific audio routes */
	snd_soc_dapm_add_routes(codec, jasper_dapm_routes,
				ARRAY_SIZE(jasper_dapm_routes));

	snd_soc_dapm_enable_pin(codec, "Ext Left Spk");
	snd_soc_dapm_enable_pin(codec, "Ext Right Spk");
	snd_soc_dapm_enable_pin(codec, "Headset Stereophone");
	snd_soc_dapm_enable_pin(codec, "Headset Mic");
	snd_soc_dapm_enable_pin(codec, "Main Mic");

	/* set endpoints to not connected */
	snd_soc_dapm_nc_pin(codec, "HPOUT2P");
	snd_soc_dapm_nc_pin(codec, "HPOUT2N");
	snd_soc_dapm_nc_pin(codec, "LINEOUT1N");
	snd_soc_dapm_nc_pin(codec, "LINEOUT1P");
	snd_soc_dapm_nc_pin(codec, "LINEOUT2N");
	snd_soc_dapm_nc_pin(codec, "LINEOUT2P");
	snd_soc_dapm_nc_pin(codec, "IN1LN");
	snd_soc_dapm_nc_pin(codec, "IN1LP");
	snd_soc_dapm_nc_pin(codec, "IN2LP:VXRN");
	snd_soc_dapm_nc_pin(codec, "IN2RN");
	snd_soc_dapm_nc_pin(codec, "IN2RP:VXRP");

	snd_soc_dapm_sync(codec);

#ifdef CONFIG_SWITCH_HEADSET
	jasper_wm8994_codec = codec;
	headset_detect_func = wm8994_headset_detect;

	snd_soc_update_bits(codec, WM8994_CLOCKING_2,
			    WM8994_DBCLK_DIV_MASK |
			    WM8994_TOCLK_DIV_MASK,
			    (2 << WM8994_DBCLK_DIV_SHIFT) |
			    (4 << WM8994_TOCLK_DIV_SHIFT));

	snd_soc_update_bits(codec, WM8994_CLOCKING_1,
			    WM8994_TOCLK_ENA_MASK,
			    WM8994_TOCLK_ENA);

	snd_soc_update_bits(codec, WM8994_IRQ_DEBOUNCE,
			    WM8994_MIC2_DET_DB_MASK |
			    WM8994_MIC2_SHRT_DB_MASK,
			    WM8994_MIC2_DET_DB |
			    WM8994_MIC2_SHRT_DB);

	snd_soc_update_bits(codec, WM8994_MICBIAS,
			    WM8994_MICD_ENA_MASK |
			    WM8994_MICD_SCTHR_MASK,
			    WM8994_MICD_ENA |
			    (1 << WM8994_MICD_SCTHR_SHIFT));

	/* turn on micbias2 always */
	snd_soc_update_bits(codec, WM8994_POWER_MANAGEMENT_1,
			    WM8994_MICB2_ENA_MASK,
			    WM8994_MICB2_ENA);

	/* unmask mic2 shrt and det int */
	snd_soc_update_bits(codec, WM8994_INTERRUPT_STATUS_2_MASK,
			    WM8994_IM_MIC2_SHRT_EINT_MASK |
			    WM8994_IM_MIC2_DET_EINT_MASK, 0);

	/* unmask int */
	snd_soc_update_bits(codec, WM8994_INTERRUPT_CONTROL,
			    WM8994_IM_IRQ_MASK, 0);
#endif
	return 0;
}

static int jasper_probe(struct platform_device *pdev)
{
	pr_debug("%s: enter\n", __func__);

	if (audio_clk)
		return 0;

	audio_clk = clk_get(NULL, "mmp2-audio");
	if (IS_ERR(audio_clk))
		return PTR_ERR(audio_clk);

	clk_enable(audio_clk);
	return 0;
}

static int jasper_hdmi_hw_params(struct snd_pcm_substream *substream,
				struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *cpu_dai = rtd->dai->cpu_dai;
	int freq_in, freq_out, sspa_mclk, sysclk;
	int sspa_div;

	pr_debug("%s: enter\n", __func__);

	freq_in = 26000000;
	if (params_rate(params) > 11025) {
		freq_out  = params_rate(params) * 512;
		sysclk    = params_rate(params) * 256;
		sspa_mclk = params_rate(params) * 64;
	} else {
		freq_out  = params_rate(params) * 1024;
		sysclk    = params_rate(params) * 512;
		sspa_mclk = params_rate(params) * 64;
	}
	sspa_div = freq_out;
	do_div(sspa_div, sspa_mclk);

	snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
		SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
	snd_soc_dai_set_pll(cpu_dai, SSPA_AUDIO_PLL, 0, freq_in, freq_out);
	snd_soc_dai_set_clkdiv(cpu_dai, 0, sspa_div);
	snd_soc_dai_set_sysclk(cpu_dai, 0, sysclk, 0);

	snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
		SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBM_CFM);

	return 0;
}

static int jasper_wm8994_startup(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai_link *machine = rtd->dai;
	struct snd_soc_dai *cpu_dai = machine->cpu_dai;

	pr_debug("%s: enter\n", __func__);

	/* limit the capture/playback rates */
	if (cpu_dai->active == 0) {
		cpu_dai->playback.formats = SNDRV_PCM_FMTBIT_S16_LE;
		cpu_dai->playback.rates   = SNDRV_PCM_RATE_44100;
		cpu_dai->capture.formats  = SNDRV_PCM_FMTBIT_S16_LE;
		cpu_dai->capture.rates    = SNDRV_PCM_RATE_44100;
	}

	return 0;
}

static int jasper_wm8994_hw_params(struct snd_pcm_substream *substream,
				       struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->dai->codec_dai;
	struct snd_soc_dai *cpu_dai = rtd->dai->cpu_dai;
	int freq_in, freq_out, sspa_mclk, sysclk;
	int sspa_div;

	pr_debug("%s: enter, rate %d\n", __func__, params_rate(params));

	freq_in = 26000000;
	if (params_rate(params) > 11025) {
		freq_out  = params_rate(params) * 512;
		sysclk    = params_rate(params) * 256;
		sspa_mclk = params_rate(params) * 64;
	} else {
		freq_out  = params_rate(params) * 1024;
		sysclk    = params_rate(params) * 512;
		sspa_mclk = params_rate(params) * 64;
	}
	sspa_div = freq_out;
	do_div(sspa_div, sspa_mclk);

	snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_I2S |
		SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);

	snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
		SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
	snd_soc_dai_set_pll(cpu_dai, SSPA_AUDIO_PLL, 0, freq_in, freq_out);
	snd_soc_dai_set_clkdiv(cpu_dai, 0, sspa_div);
	snd_soc_dai_set_sysclk(cpu_dai, 0, sysclk, 0);

	snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
		SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBM_CFM);

	/* set wm8994 sysclk */
	snd_soc_dai_set_sysclk(codec_dai, WM8994_SYSCLK_MCLK1, sysclk, 0);
	return 0;
}

#ifdef CONFIG_PM
static int jasper_suspend_post(struct platform_device *pdev,
				pm_message_t state)
{
	pr_debug("%s: enter\n", __func__);
	clk_disable(audio_clk);
	return 0;
}

static int jasper_resume_pre(struct platform_device *pdev)
{
	pr_debug("%s: enter\n", __func__);
	clk_enable(audio_clk);

	/*
	 * FIXME: need output sysclk before codec resume,
	 * otherwise wm8994 can not work well.
	 */
	__raw_writel(0x801, SSPA_AUD_PLL_CTRL1);
	__raw_writel(0x211105, SSPA_AUD_CTRL);
	__raw_writel(0x108a1881, SSPA_AUD_PLL_CTRL0);
	while (!(__raw_readl(SSPA_AUD_PLL_CTRL1) &
		 SSPA_AUD_PLL_CTRL1_PLL_LOCK));
	return 0;
}
#endif

/* machine stream operations */
static struct snd_soc_ops jasper_machine_ops[] = {
{
	.hw_params = jasper_hdmi_hw_params,
},
{
	.startup   = jasper_wm8994_startup,
	.hw_params = jasper_wm8994_hw_params,
},
};

static struct snd_soc_dai_link jasper_hdmi_dai = {
	.name        = "hdmi",
	.stream_name = "hdmi",
	.cpu_dai     = &mmp2_sspa_dai[0],
	.codec_dai   = &hdmi_audio_dai[0],
	.ops         = &jasper_machine_ops[0],
	.init        = codec_hdmi_init,
};

static struct snd_soc_dai_link jasper_wm8994_dai[] = {
{
	.name        = "wm8994 playback (legacy mode)",
	.stream_name = "wm8994 playback (legacy mode)",
	.cpu_dai     = &mmp2_sspa_dai[0],
	.codec_dai   = &wm8994_dai[0],
	.ops         = &jasper_machine_ops[1],
	.init        = codec_wm8994_init,
},
};

/* audio machine driver */
static struct snd_soc_card snd_soc_machine_jasper[] = {
{	.name         = "hdmi",
	.platform     = &mmp2_soc_platform,
	.dai_link     = &jasper_hdmi_dai,
	.num_links    = 1,
	.probe        = jasper_probe,
},
{	.name         = "wm8994",
	.platform     = &mmp2_soc_platform,
	.dai_link     = jasper_wm8994_dai,
	.num_links    = ARRAY_SIZE(jasper_wm8994_dai),
	.probe        = jasper_probe,
#ifdef CONFIG_PM
	.suspend_post = jasper_suspend_post,
	.resume_pre   = jasper_resume_pre,
#endif
},
};

/* audio subsystem */
static struct snd_soc_device jasper_snd_devdata[] = {
{
	.card      = &snd_soc_machine_jasper[0],
	.codec_dev = &soc_codec_dev_hdmi_audio,
},
{
	.card      = &snd_soc_machine_jasper[1],
	.codec_dev = &soc_codec_dev_wm8994,
},
};

static struct platform_device *jasper_snd_device[2];

static int __init jasper_init(void)
{
	int ret;

	if (!machine_is_jasper())
		return -ENODEV;

	jasper_snd_device[1] = platform_device_alloc("soc-audio", 1);
	if (!jasper_snd_device[1]) {
		ret = -ENOMEM;
		goto err_dev1;
	}

	platform_set_drvdata(jasper_snd_device[1],
			     &jasper_snd_devdata[1]);
	jasper_snd_devdata[1].dev = &jasper_snd_device[1]->dev;
	ret = platform_device_add(jasper_snd_device[1]);

	if (ret) {
		platform_device_put(jasper_snd_device[1]);
		goto err_dev1;
	}

	jasper_snd_device[0] = platform_device_alloc("soc-audio", 0);
	if (!jasper_snd_device[0]) {
		ret = -ENOMEM;
		goto err_dev0;
	}

	platform_set_drvdata(jasper_snd_device[0],
			     &jasper_snd_devdata[0]);
	jasper_snd_devdata[0].dev = &jasper_snd_device[0]->dev;
	ret = platform_device_add(jasper_snd_device[0]);

	if (ret) {
		platform_device_put(jasper_snd_device[0]);
		goto err_dev0;
	}

	return 0;

err_dev0:
	platform_device_unregister(jasper_snd_device[1]);
err_dev1:
	return ret;
}

static void __exit jasper_exit(void)
{
	platform_device_unregister(jasper_snd_device[0]);
	platform_device_unregister(jasper_snd_device[1]);
}

module_init(jasper_init);
module_exit(jasper_exit);

/* Module information */
MODULE_DESCRIPTION("ALSA SoC Jasper");
MODULE_LICENSE("GPL");
