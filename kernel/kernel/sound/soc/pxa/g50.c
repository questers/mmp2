/*
 * g50.c  --  SoC audio for g50
 *
 * Copyright (C) 2011 Marvell International Ltd.
 * All rights reserved.
 *
 *   2011-03-04: Leo Yan <leoy@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/timer.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/i2c.h>
#include <linux/clk.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/jack.h>
#include <asm/mach-types.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <plat/ssp.h>
#include <mach/addr-map.h>
#include <mach/regs-sspa.h>
#include <mach/regs-mpmu.h>
#include <mach/regs-apmu.h>
#include <mach/mmp-zsp.h>
#include <mach/mfp-mmp2.h>

#include "../codecs/rt5625.h"
#include "mmp2-squ.h"
#include "mmp2-sspa.h"
#include "jasper.h"

static struct clk *audio_clk;

static int g50_probe(struct platform_device *pdev)
{
	pr_debug("%s: enter\n", __func__);

	if (audio_clk)
		return 0;

	audio_clk = clk_get(NULL, "mmp2-audio");
	if (IS_ERR(audio_clk))
		return PTR_ERR(audio_clk);

	clk_enable(audio_clk);
	return 0;
}


static int g50_rt5625_hifi_startup(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai_link *machine = rtd->dai;
	struct snd_soc_dai *cpu_dai = machine->cpu_dai;

	pr_debug("%s: enter\n", __func__);

	/* limit the capture/playback rates */
	if (cpu_dai->active == 0) {
		cpu_dai->playback.formats = SNDRV_PCM_FMTBIT_S16_LE;
		cpu_dai->playback.rates   = SNDRV_PCM_RATE_48000;
		cpu_dai->capture.formats  = SNDRV_PCM_FMTBIT_S16_LE;
		cpu_dai->capture.rates    = SNDRV_PCM_RATE_48000;
	}

	return 0;
}

static int g50_rt5625_hifi_hw_params(struct snd_pcm_substream *substream,
				     struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->dai->codec_dai;
	struct snd_soc_dai *cpu_dai = rtd->dai->cpu_dai;
	int freq_in, freq_out, sspa_mclk, sysclk;
	int sspa_div;

	pr_debug("%s: enter, rate %d\n", __func__, params_rate(params));

	freq_in = 26000000;
	if (params_rate(params) > 11025) {
		freq_out  = params_rate(params) * 512;
		sysclk    = params_rate(params) * 256;
		sspa_mclk = params_rate(params) * 64;
	} else {
		freq_out  = params_rate(params) * 1024;
		sysclk    = params_rate(params) * 512;
		sspa_mclk = params_rate(params) * 64;
	}
	sspa_div = freq_out;
	do_div(sspa_div, sspa_mclk);


	snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
		SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
	snd_soc_dai_set_pll(cpu_dai, SSPA_AUDIO_PLL, 0, freq_in, freq_out);
	snd_soc_dai_set_clkdiv(cpu_dai, 0, sspa_div);
	snd_soc_dai_set_sysclk(cpu_dai, 0, sysclk, 0);

	snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_I2S |
		SND_SOC_DAIFMT_IB_NF | SND_SOC_DAIFMT_CBS_CFS);

	snd_soc_dai_set_pll(codec_dai, RT5625_PLL1_FROM_MCLK, 0, 12288000, 24576000);	
	
	/* rt5625 need set the sysclk */
	snd_soc_dai_set_sysclk(codec_dai, 0, 24576000, 0);
	return 0;
}

/* g50 machine dapm widgets */
static const struct snd_soc_dapm_widget g50_dapm_widgets[] = {
	SND_SOC_DAPM_HP("Headphone Jack", NULL),
	SND_SOC_DAPM_SPK("Ext Speaker", NULL),
	/*
	SND_SOC_DAPM_MIC("Int Microphone", NULL),	
	SND_SOC_DAPM_MIC("Ext Microphone", NULL),
	*/
};

/* g50 audio map */
static const struct snd_soc_dapm_route g50_audio_map[] = {
	/* headphone connected to HPL, HPR */
	{"Headphone Jack", NULL, "HPL"},
	{"Headphone Jack", NULL, "HPR"},

	/* ext speaker connected to SPKL, SPKR */
	{"Ext Speaker", NULL, "SPKL"},
	{"Ext Speaker", NULL, "SPKR"},

	#if 0
	/* onboard mic connected to MIC1 */
	{"Mic1", NULL, "Mic1 Bias"},
	{"Mic1 Bias", NULL, "Int Microphone"},

	/* headset mic connected to MIC2 */
	{"Mic2", NULL, "Mic2 Bias"},
	{"Mic2 Bias", NULL, "Ext Microphone"},
	//#else
	/* onboard mic connected to MIC1 */
	{"Mic1", NULL, "Int Microphone"},

	/* headset mic connected to MIC2 */
	{"Mic2", NULL, "Ext Microphone"},
	
	#endif
};
static int g50_rt5625_init(struct snd_soc_codec *codec)
{
	int ret = 0;

	/* NC codec pins */
	snd_soc_dapm_nc_pin(codec, "Phone");
	snd_soc_dapm_nc_pin(codec, "AUX");

	/* use the single ended microphone*/
	//snd_soc_update_bits(codec, RT5625_MIC_VOL,
	//		    0, MIC1_DIFF_CTRL|MIC2_DIFF_CTRL);

	/* Add g50 specific widgets */
	snd_soc_dapm_new_controls(codec, g50_dapm_widgets,
				  ARRAY_SIZE(g50_dapm_widgets));

	/* Set up g50 specific audio paths */
	snd_soc_dapm_add_routes(codec, g50_audio_map, ARRAY_SIZE(g50_audio_map));

	snd_soc_dapm_enable_pin(codec, "Headphone Jack");
	snd_soc_dapm_enable_pin(codec, "Ext Speaker");
	//snd_soc_dapm_enable_pin(codec, "Int Microphone");	
	//snd_soc_dapm_enable_pin(codec, "Ext Microphone");
	snd_soc_dapm_enable_pin(codec, "PCM");
	
	
	ret = snd_soc_dapm_sync(codec);

	return ret;
}

#ifdef CONFIG_PM
static int g50_suspend_post(struct platform_device *pdev,
				pm_message_t state)
{
	pr_debug("%s: enter\n", __func__);
	clk_disable(audio_clk);
	return 0;
}

static int g50_resume_pre(struct platform_device *pdev)
{
	pr_debug("%s: enter\n", __func__);
	clk_enable(audio_clk);

	/*
	 * FIXME: need output sysclk before codec resume,
	 * otherwise wm8994 can not work well.
	 */
	__raw_writel(0x801, SSPA_AUD_PLL_CTRL1);
	__raw_writel(0x211105, SSPA_AUD_CTRL);
	__raw_writel(0x108a1881, SSPA_AUD_PLL_CTRL0);
	while (!(__raw_readl(SSPA_AUD_PLL_CTRL1) &
		 SSPA_AUD_PLL_CTRL1_PLL_LOCK));
	return 0;
}
#endif


/* machine stream operations */
static struct snd_soc_ops g50_rt5625_machine_ops = {
	.startup   = g50_rt5625_hifi_startup,
	.hw_params = g50_rt5625_hifi_hw_params,
};

static struct platform_device *g50_snd_device;

static struct snd_soc_dai_link g50_dai[] = {
{
	.name        = "RT5625 HiFi",
	.stream_name = "RT5625 HiFi",
	.cpu_dai     = &mmp2_sspa_dai[0],
	.codec_dai   = &rt5625_dai[0],
	.ops         = &g50_rt5625_machine_ops,
	.init        = g50_rt5625_init,
},
};

static struct snd_soc_card g50 = {
	.name      = "g50 RT5625",
	.platform  = &mmp2_soc_platform,
	.dai_link  = g50_dai,
	.num_links = ARRAY_SIZE(g50_dai),
	.probe     = g50_probe,
#ifdef CONFIG_PM
	.suspend_post = g50_suspend_post,
	.resume_pre = g50_resume_pre,
#endif
};

static struct snd_soc_device g50_snd_devdata = {
	.card       = &g50,
	.codec_dev  = &soc_codec_dev_rt5625,
};

static int __init g50_init(void)
{
	int ret;

	if (!machine_is_g50())
		return -ENODEV;

	/* ssp_set_clock(0); */
	g50_snd_device = platform_device_alloc("soc-audio", 1);

	if (!g50_snd_device)
		return -ENOMEM;

	platform_set_drvdata(g50_snd_device, &g50_snd_devdata);

	g50_snd_devdata.dev = &g50_snd_device->dev;

	ret = platform_device_add(g50_snd_device);
	if (ret)
		platform_device_put(g50_snd_device);

	return ret;
}

static void __exit g50_exit(void)
{
	platform_device_unregister(g50_snd_device);
}

module_init(g50_init);
module_exit(g50_exit);

MODULE_DESCRIPTION("ALSA SoC RT5625 G50");
MODULE_AUTHOR("Leo Yan <leoy@marvell.com>");
MODULE_LICENSE("GPL");
