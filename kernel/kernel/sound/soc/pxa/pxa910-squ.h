/*
 * linux/sound/soc/pxa/pxa910-squ.h
 *
 * Base on linux/sound/soc/pxa/pxa2xx-pcm.h
 *
 * Copyright (C) 2007 Marvell International Ltd.
 * Author: Bin Yang <bin.yang@marvell.com> 
 * 			 Yael Sheli Chemla<yael.s.shemla@marvell.com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _PXA910_SQU_H
#define _PXA910_SQU_H

#include "mmp2_zsp_audio.h"

#if 0
struct pxa910_squ_dma_params {
	char *name;			/* stream identifier */
	u32 dcmd;			/* DMA descriptor dcmd field */
	u32 dev_addr;		/* device physical address for DMA */
};
#endif

struct pxa688_adma_registers {
	u32 byte_counter;
	u32 src_addr;
	u32 dest_addr;
	u32 next_desc_ptr;
	u32 ctrl;
	u32 chan_pri;				/* Only used in channel 0 */
	u32 curr_desc_ptr;
	u32 intr_mask;
	u32 intr_status;
};

struct zsp_buffer {
	void *buf;
	dma_addr_t buf_phys;
	u32 zsp_offset;
	u32 app_offset;
	int buf_len;
	int zsp_period_bytes;
};

struct pxa910_runtime_data {
	int dma_ch;
	struct pxa3xx_pcm_dma_params *params;
	void *squ_desc_array;
	dma_addr_t squ_desc_array_phys;

	spinlock_t lock;

	/* sram dma area */
	unsigned int sram_virt;
	unsigned int sram_phys;
	unsigned int sram_size;
	volatile u32 sram_idx;
	volatile u32 sram_max_num;

	/* ring buf area */
	unsigned int rbuf_virt;
	unsigned int rbuf_phys;
	unsigned int rbuf_size;
	volatile u32 rbuf_idx;
	volatile u32 rbuf_max_num;

	volatile u32 avail_in;
	volatile u32 avail_out;
	u32 period_size;

	struct snd_pcm_substream *substream;

	struct pxa688_adma_registers pxa688_adma_saved[4];
	void *squ_desc_array_saved;

	void *zmq_deliver;
	int render_id;
	int stream_id;
	struct zsp_buffer zsp_buf;
	struct workqueue_struct *zmq_workqueue;
	struct work_struct zsp_queue;
	struct completion zmq_cmd_completion; /* only release will use it */
	adma_config_t zsp_adma_conf;
};

extern int zsp_mode;

#define MMP2_SRAM_DMA_BUF_SIZE		(PAGE_SIZE)
#define MMP2_RING_BUF_SIZE		(MMP2_SRAM_DMA_BUF_SIZE	<< 4)

#endif
