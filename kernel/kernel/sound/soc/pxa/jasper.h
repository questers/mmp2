#ifndef __JASPER_H
#define __JASPER_H

#include "mmp2_zsp_audio.h"

#define	MPMU_SPCGR			MPMU_REG(0x0024)
#define MPMU_ISCCR1			MPMU_REG(0x0040)
#define MPMU_ISCCR2			MPMU_REG(0x0044)
#define APMU_AUDIO_CLK_RES_CTRL		APMU_REG(0x010c)
#define SSPA1_VIRT_BASE			(AXI_VIRT_BASE + 0xa0c00)
#define SSPA2_VIRT_BASE			(AXI_VIRT_BASE + 0xa0d00)

#define JASPER_SAMPLE_RATES (SNDRV_PCM_RATE_48000)

extern struct snd_soc_dai hdmi_audio_dai[1];
extern struct snd_soc_dai wm8994_dai[1];
extern struct snd_soc_codec_device soc_codec_dev_hdmi_audio;
extern struct snd_soc_codec_device soc_codec_dev_wm8994;

extern void mmp2_save_adma_regs(struct snd_pcm_substream *substream);
extern void mmp2_restore_adma_regs(struct snd_pcm_substream *substream);
extern int mmp2_save_desc(struct snd_pcm_substream *substream);
extern void mmp2_restore_desc(struct snd_pcm_substream *substream);
extern void mmp2_save_sspa_regs(struct ssp_device *sspa);
extern void mmp2_restore_sspa_regs(struct ssp_device *sspa);

extern void hdmi_audio_cfg(u32 sr, u32 i2s, u32 wsp, u32 fsp, u32 clkp,
				u32 xdatdly, u32 xwdlen1, u32 xfrlen1);
extern struct snd_soc_platform pxa910_soc_platform;

#endif
