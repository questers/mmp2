/*
 * g50_rt5631.c  --  SoC audio for g50 (rt5631)
 *
 * Copyright (C) 2011 Marvell International Ltd.
 * All rights reserved.
 *
 *   2011-03-04: Leo Yan <leoy@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/timer.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/i2c.h>
#include <linux/clk.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/jack.h>
#include <asm/mach-types.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <plat/ssp.h>
#include <mach/addr-map.h>
#include <mach/regs-sspa.h>
#include <mach/regs-mpmu.h>
#include <mach/regs-apmu.h>
#include <mach/mmp-zsp.h>
#include <mach/mfp-mmp2.h>
#include <mach/mmp2_plat_ver.h>

#include "../codecs/rt5631.h"
#include "mmp2-squ.h"
#include "mmp2-sspa.h"
#include "jasper.h"

#define G50_CTRL_ON    0
#define G50_CTRL_OFF   1

static struct clk *audio_clk;
static int g50_hp_func;
static int g50_int_mic_func;
static int g50_spk_func;

static void g50_ext_control(struct snd_soc_codec *codec)
{
	if (g50_spk_func == G50_CTRL_ON)
		snd_soc_dapm_enable_pin(codec, "Ext Speaker");
	else
		snd_soc_dapm_disable_pin(codec, "Ext Speaker");

	if (g50_int_mic_func == G50_CTRL_ON)
		snd_soc_dapm_enable_pin(codec, "Ext Microphone");
	else
		snd_soc_dapm_disable_pin(codec, "Ext Microphone");

	if (g50_hp_func == G50_CTRL_ON)
		snd_soc_dapm_enable_pin(codec, "Headphone Jack");
	else
		snd_soc_dapm_disable_pin(codec, "Headphone Jack");

	snd_soc_dapm_sync(codec);
	return;
}

static int g50_get_hp(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = g50_hp_func;
	return 0;
}

static int g50_set_hp(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_kcontrol_chip(kcontrol);

	if (g50_hp_func == ucontrol->value.integer.value[0])
		return 0;

	g50_hp_func = ucontrol->value.integer.value[0];
	g50_ext_control(codec);
	return 1;
}

static int g50_get_int_mic(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = g50_int_mic_func;
	return 0;
}

static int g50_set_int_mic(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_kcontrol_chip(kcontrol);

	if (g50_int_mic_func == ucontrol->value.integer.value[0])
		return 0;

	g50_int_mic_func = ucontrol->value.integer.value[0];
	g50_ext_control(codec);
	return 1;
}

static int g50_get_spk(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = g50_spk_func;
	return 0;
}

static int g50_set_spk(struct snd_kcontrol *kcontrol,
	struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec =  snd_kcontrol_chip(kcontrol);

	if (g50_spk_func == ucontrol->value.integer.value[0])
		return 0;

	g50_spk_func = ucontrol->value.integer.value[0];
	g50_ext_control(codec);
	return 1;
}

static int g50_probe(struct platform_device *pdev)
{
	pr_debug("%s: enter\n", __func__);

	if (audio_clk)
		return 0;

	audio_clk = clk_get(NULL, "mmp2-audio");
	if (IS_ERR(audio_clk))
		return PTR_ERR(audio_clk);

	clk_enable(audio_clk);
	return 0;
}

static int g50_hdmi_hw_params(struct snd_pcm_substream *substream,
				struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *cpu_dai = rtd->dai->cpu_dai;
	int freq_in, freq_out, sspa_mclk, sysclk;
	int sspa_div;

	pr_debug("%s: enter\n", __func__);

	freq_in = 26000000;
	if (params_rate(params) > 11025) {
		freq_out  = params_rate(params) * 512;
		sysclk    = params_rate(params) * 256;
		sspa_mclk = params_rate(params) * 64;
	} else {
		freq_out  = params_rate(params) * 1024;
		sysclk    = params_rate(params) * 512;
		sspa_mclk = params_rate(params) * 64;
	}
	sspa_div = freq_out;
	do_div(sspa_div, sspa_mclk);

	snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
		SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
	snd_soc_dai_set_pll(cpu_dai, SSPA_AUDIO_PLL, 0, freq_in, freq_out);
	snd_soc_dai_set_clkdiv(cpu_dai, 0, sspa_div);
	snd_soc_dai_set_sysclk(cpu_dai, 0, sysclk, 0);

	snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
		SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBM_CFM);

	return 0;
}

static int g50_rt5631_hifi_startup(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai_link *machine = rtd->dai;
	struct snd_soc_dai *cpu_dai = machine->cpu_dai;

	pr_debug("%s: enter\n", __func__);

	/* limit the capture/playback rates */
	if (cpu_dai->active == 0) {
		cpu_dai->playback.formats = SNDRV_PCM_FMTBIT_S16_LE;
		cpu_dai->playback.rates   = SNDRV_PCM_RATE_48000;
		cpu_dai->capture.formats  = SNDRV_PCM_FMTBIT_S16_LE;
		cpu_dai->capture.rates    = SNDRV_PCM_RATE_48000;
	}

	return 0;
}

static int g50_rt5631_hifi_hw_params(struct snd_pcm_substream *substream,
				     struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->dai->codec_dai;
	struct snd_soc_dai *cpu_dai = rtd->dai->cpu_dai;
	int freq_in, freq_out, sspa_mclk, sysclk;
	int sspa_div;

	pr_debug("%s: enter, rate %d\n", __func__, params_rate(params));

	freq_in = 26000000;
	if (params_rate(params) > 11025) {
		freq_out  = params_rate(params) * 512;
		sysclk    = params_rate(params) * 256;
		sspa_mclk = params_rate(params) * 64;
	} else {
		freq_out  = params_rate(params) * 1024;
		sysclk    = params_rate(params) * 512;
		sspa_mclk = params_rate(params) * 64;
	}
	sspa_div = freq_out;
	do_div(sspa_div, sspa_mclk);


	snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
		SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
	snd_soc_dai_set_pll(cpu_dai, SSPA_AUDIO_PLL, 0, freq_in, freq_out);
	snd_soc_dai_set_clkdiv(cpu_dai, 0, sspa_div);
	snd_soc_dai_set_sysclk(cpu_dai, 0, sysclk, 0);

	snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_I2S |
		SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);

	/* rt5631 need set the sysclk */
	snd_soc_dai_set_sysclk(codec_dai, 0, sysclk, 0);

	return 0;
}

/* g50 machine dapm widgets */
static const struct snd_soc_dapm_widget rt5631_dapm_widgets[] = {
	SND_SOC_DAPM_HP("Headphone Jack", NULL),
	SND_SOC_DAPM_SPK("Ext Speaker", NULL),
	SND_SOC_DAPM_MIC("Ext Microphone", NULL),
};

/* g50 audio map */
static const struct snd_soc_dapm_route audio_map[] = {
	/* headphone connected to HPL, HPR */
	{"Headphone Jack", NULL, "HPOL"},
	{"Headphone Jack", NULL, "HPOR"},

	/* ext speaker connected to SPKL, SPKR */
	{"Ext Speaker", NULL, "SPOL"},
	{"Ext Speaker", NULL, "SPOR"},

	/* mic connected to MIC1 */
	{"MIC1", NULL, "Mic Bias1"},
	{"Mic Bias1", NULL, "Ext Microphone"},
};

static const char *hp_function[] = {"On", "Off"};
static const char *int_mic_function[] = {"On", "Off"};
static const char *spk_function[]  = {"On", "Off"};
static const struct soc_enum g50_enum[] = {
	SOC_ENUM_SINGLE_EXT(2, hp_function),
	SOC_ENUM_SINGLE_EXT(2, int_mic_function),
	SOC_ENUM_SINGLE_EXT(2, spk_function),
};

static const struct snd_kcontrol_new g50_rt5631_controls[] = {
	SOC_ENUM_EXT("Headphone Function", g50_enum[0],
		     g50_get_hp, g50_set_hp),
	SOC_ENUM_EXT("Internal Mic Function", g50_enum[1],
		     g50_get_int_mic, g50_set_int_mic),
	SOC_ENUM_EXT("Speaker Function", g50_enum[2],
		     g50_get_spk, g50_set_spk),
};

static int g50_rt5631_init(struct snd_soc_codec *codec)
{
	int ret = 0;

	/* NC codec pins */
	snd_soc_dapm_nc_pin(codec, "MIC2");
	snd_soc_dapm_nc_pin(codec, "AXIL");
	snd_soc_dapm_nc_pin(codec, "AXIR");
	snd_soc_dapm_nc_pin(codec, "MONOIN_RXN");
	snd_soc_dapm_nc_pin(codec, "MONOIN_RXN");

	snd_soc_dapm_nc_pin(codec, "LOUT");
	snd_soc_dapm_nc_pin(codec, "ROUT");
	snd_soc_dapm_nc_pin(codec, "MONO");

	/* Add g50 specific widgets */
	snd_soc_dapm_new_controls(codec, rt5631_dapm_widgets,
				  ARRAY_SIZE(rt5631_dapm_widgets));

	/* Set up g50 specific audio paths */
	snd_soc_dapm_add_routes(codec, audio_map, ARRAY_SIZE(audio_map));

	/* Add g50 specific controls */
	ret = snd_soc_add_controls(codec, g50_rt5631_controls,
				   ARRAY_SIZE(g50_rt5631_controls));
	if (ret < 0) {
		printk(KERN_ERR "%s: add controls failed.\n", __func__);
		return ret;
	}

	g50_hp_func = G50_CTRL_ON;
	g50_int_mic_func = G50_CTRL_ON;
	g50_spk_func = G50_CTRL_ON;

	snd_soc_dapm_enable_pin(codec, "Headphone Jack");
	snd_soc_dapm_enable_pin(codec, "Ext Speaker");
	snd_soc_dapm_enable_pin(codec, "Ext Microphone");

	ret = snd_soc_dapm_sync(codec);
	return ret;
}

#ifdef CONFIG_PM
static int g50_suspend_post(struct platform_device *pdev,
				pm_message_t state)
{
	pr_debug("%s: enter\n", __func__);
	clk_disable(audio_clk);
	return 0;
}

static int g50_resume_pre(struct platform_device *pdev)
{
	pr_debug("%s: enter\n", __func__);
	clk_enable(audio_clk);

	/*
	 * FIXME: need output sysclk before codec resume,
	 * otherwise wm8994 can not work well.
	 */
	__raw_writel(0x801, SSPA_AUD_PLL_CTRL1);
	__raw_writel(0x211105, SSPA_AUD_CTRL);
	__raw_writel(0x108a1881, SSPA_AUD_PLL_CTRL0);
	while (!(__raw_readl(SSPA_AUD_PLL_CTRL1) &
		 SSPA_AUD_PLL_CTRL1_PLL_LOCK));
	return 0;
}
#endif

static struct platform_device *g50_snd_device[2];

/* machine stream operations */
static struct snd_soc_ops g50_rt5631_machine_ops = {
	.startup   = g50_rt5631_hifi_startup,
	.hw_params = g50_rt5631_hifi_hw_params,
};

static struct snd_soc_ops g50_hdmi_machine_ops = {
	.hw_params = g50_hdmi_hw_params,
};

static struct snd_soc_dai_link g50_hdmi_dai = {
	.name        = "hdmi",
	.stream_name = "hdmi",
	.cpu_dai     = &mmp2_sspa_dai[0],
	.codec_dai   = &hdmi_audio_dai[0],
	.ops         = &g50_hdmi_machine_ops,
};

static struct snd_soc_dai_link g50_rt5631_dai[] = {
{
	.name        = "RT5631 HiFi",
	.stream_name = "RT5631 HiFi",
	.cpu_dai     = &mmp2_sspa_dai[0],
	.codec_dai   = &rt5631_dai[0],
	.ops         = &g50_rt5631_machine_ops,
	.init        = g50_rt5631_init,
},
};

static struct snd_soc_card g50[] = {
{
	.name      = "hdmi",
	.platform  = &mmp2_soc_platform,
	.dai_link  = &g50_hdmi_dai,
	.num_links = 1,
	.probe     = g50_probe,
},
{
	.name      = "g50 RT5631",
	.platform  = &mmp2_soc_platform,
	.dai_link  = g50_rt5631_dai,
	.num_links = ARRAY_SIZE(g50_rt5631_dai),
	.probe     = g50_probe,
#ifdef CONFIG_PM
	.suspend_post = g50_suspend_post,
	.resume_pre = g50_resume_pre,
#endif
},
};

static struct snd_soc_device g50_snd_devdata[] = {
{
	.card       = &g50[0],
	.codec_dev  = &soc_codec_dev_hdmi_audio,
},
{
	.card       = &g50[1],
	.codec_dev  = &soc_codec_dev_rt5631,
},
};

static int __init g50_init(void)
{
	int ret;

	if (!board_is_mmp2_g50_rev3())
		return -ENODEV;

	g50_snd_device[1] = platform_device_alloc("soc-audio", 1);
	if (!g50_snd_device[1]) {
		ret = -ENOMEM;
		goto err_dev1;
	}

	platform_set_drvdata(g50_snd_device[1],
			     &g50_snd_devdata[1]);
	g50_snd_devdata[1].dev = &g50_snd_device[1]->dev;
	ret = platform_device_add(g50_snd_device[1]);

	if (ret) {
		platform_device_put(g50_snd_device[1]);
		goto err_dev1;
	}

	g50_snd_device[0] = platform_device_alloc("soc-audio", 0);
	if (!g50_snd_device[0]) {
		ret = -ENOMEM;
		goto err_dev0;
	}

	platform_set_drvdata(g50_snd_device[0],
			     &g50_snd_devdata[0]);
	g50_snd_devdata[0].dev = &g50_snd_device[0]->dev;
	ret = platform_device_add(g50_snd_device[0]);

	if (ret) {
		platform_device_put(g50_snd_device[0]);
		goto err_dev0;
	}

	return 0;

err_dev0:
	platform_device_unregister(g50_snd_device[1]);
err_dev1:
	return ret;
}

static void __exit g50_exit(void)
{
	platform_device_unregister(g50_snd_device[0]);
	platform_device_unregister(g50_snd_device[1]);
}

module_init(g50_init);
module_exit(g50_exit);

MODULE_DESCRIPTION("ALSA SoC RT5631 G50");
MODULE_AUTHOR("Leo Yan <leoy@marvell.com>");
MODULE_LICENSE("GPL");
