/*
 * linux/sound/soc/pxa/pxa910-squ.c
 *
 * Base on linux/sound/soc/pxa/pxa2xx-pcm.c
 *
 * Copyright (C) 2007 Marvell International Ltd.
 * Author: Bin Yang <bin.yang@marvell.com> 
 * 			 Yael Sheli Chemla<yael.s.shemla@marvell.com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/dma-mapping.h>

#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>

#include <asm/dma.h>

#include <plat/regs-ssp.h>
#include <mach/regs-sspa.h>
#include <plat/imm.h>
#include <mach/pxa910-squ.h>
#include <mach/mmp2_dma.h>
#include <mach/mmp2_audiosram.h>
#include <mach/cputype.h>
#include "pxa3xx-pcm.h"
#include "pxa910-squ.h"

#include <linux/delay.h>

static const struct snd_pcm_hardware pxa910_pcm_hardware = {
	.info			= SNDRV_PCM_INFO_MMAP |
				  SNDRV_PCM_INFO_MMAP_VALID |
				  SNDRV_PCM_INFO_INTERLEAVED |
				  SNDRV_PCM_INFO_PAUSE |
				  SNDRV_PCM_INFO_RESUME,
	.formats		= SNDRV_PCM_FMTBIT_S16_LE |
					SNDRV_PCM_FMTBIT_S24_LE |
					SNDRV_PCM_FMTBIT_S32_LE,
	.period_bytes_min	= 32,
	.period_bytes_max	= 4096 - 32,
	.periods_min		= 1,
	.periods_max		= PAGE_SIZE/sizeof(pxa910_squ_desc),
	.buffer_bytes_max	= 12 * 1024,
	.fifo_size		= 32,
};

static const struct snd_pcm_hardware mmp2_a0_pcm_hardware = {
	.info			= SNDRV_PCM_INFO_MMAP |
				  SNDRV_PCM_INFO_MMAP_VALID |
				  SNDRV_PCM_INFO_INTERLEAVED |
				  SNDRV_PCM_INFO_PAUSE |
				  SNDRV_PCM_INFO_RESUME,
	.formats		= SNDRV_PCM_FMTBIT_S16_LE |
					SNDRV_PCM_FMTBIT_S24_LE |
					SNDRV_PCM_FMTBIT_S32_LE,
	.period_bytes_min	= 1024,
	.period_bytes_max	= 2048,
	.periods_min		= 2,
	.periods_max		= PAGE_SIZE/sizeof(pxa910_squ_desc),
	.buffer_bytes_max	= MMP2_RING_BUF_SIZE,
	.fifo_size		= 32,
};

static unsigned int immid = -1;
static DECLARE_WAIT_QUEUE_HEAD(dma_wq);

static void *pxa910_sram_alloc(int size, u32 *p)
{
	void *v;

	if (immid == -1)
		immid = imm_register_kernel("pxa910 audio");

	v = imm_malloc(size, IMM_MALLOC_HARDWARE | IMM_MALLOC_SRAM, immid);
	if (v != NULL)
		*p = imm_get_physical(v, immid);

	return v;
}

static void pxa910_sram_free(void *v)
{
	imm_free(v, immid);
}

static int pxa910_sram_mmap_writecombine(struct vm_area_struct *vma,
			  void *cpu_addr, dma_addr_t dma_addr, size_t size)
{
	unsigned long user_size;
	unsigned long off = vma->vm_pgoff;
	u32 ret;

	vma->vm_page_prot = pgprot_writecombine(vma->vm_page_prot);
	user_size = (vma->vm_end - vma->vm_start) >> PAGE_SHIFT;
	ret = remap_pfn_range(vma, vma->vm_start,
					__phys_to_pfn(dma_addr) + off,
					user_size << PAGE_SHIFT,
					vma->vm_page_prot);

	return ret;
}

static int mmp2_sram_mmap_noncached(struct vm_area_struct *vma,
			  void *cpu_addr, dma_addr_t dma_addr, size_t size)
{
	unsigned long user_size;
	unsigned long off = vma->vm_pgoff;
	u32 ret;

	vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
	user_size = (vma->vm_end - vma->vm_start) >> PAGE_SHIFT;
	ret = remap_pfn_range(vma, vma->vm_start,
					__phys_to_pfn(dma_addr) + off,
					user_size << PAGE_SHIFT,
					vma->vm_page_prot);

	return ret;
}

#ifdef DEBUG

static void pxa910_dump_dma_list(struct pxa910_runtime_data *prtd)
{
	pxa910_squ_desc *squ_desc;

	pr_debug("dma list description is:\n");
	squ_desc = prtd->squ_desc_array;
	do {
		pr_debug("---------------------\n");
		pr_debug("src_addr = 0x%08x\n", squ_desc->src_addr);
		pr_debug("dst_addr = 0x%08x\n", squ_desc->dst_addr);
		pr_debug("byte_cnt = 0x%08x\n", squ_desc->byte_cnt);
		pr_debug("nxt_desc = 0x%08x\n", squ_desc->nxt_desc);

		squ_desc = (pxa910_squ_desc *)(squ_desc->nxt_desc -
			(int)prtd->squ_desc_array_phys +
			(int)prtd->squ_desc_array);

	} while (squ_desc != prtd->squ_desc_array);

	return;
}

#else

#define pxa910_dump_dma_list(prtd) do { } while (0)

#endif

static void pxa910_copy_data(int *dst, int *src, int size)
{
	int i;

	BUG_ON(!IS_ALIGNED((int)dst, sizeof(int)) ||
	       !IS_ALIGNED((int)src, sizeof(int)) ||
	       !IS_ALIGNED((int)size, sizeof(int)));

	size = size / sizeof(int);
	for (i = 0; i < size; i++) {
		*dst = *src;
		dst++;
		src++;
	}
	return;
}

static int pxa910_sram_is_in_scope(u32 total, u32 start, u32 len, u32 point)
{
	u32 h1_start, h1_size;
	u32 h2_start, h2_size;

	/* split two halves */
	if (start + len > total) {
		h1_start = start;
		h1_size  = total - start;
		h2_start = 0;
		h2_size  = start + len - total;
	} else {
		h1_start = start;
		h1_size  = len;
		h2_start = 0;
		h2_size  = 0;
	}

	/* in first half */
	if ((point >= h1_start) && (point < h1_start + h1_size))
		return h1_start + h1_size - point + h2_size;

	/* in bottom half */
	if ((point >= h2_start) && (point < h2_start + h2_size))
		return h2_start + h2_size - point;

	return 0;
}

static void pxa910_sync_sram(struct snd_pcm_substream *substream)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct pxa910_runtime_data *prtd = runtime->private_data;
	char *src, *dst;
	u32 base, offset, rest;
	int num, i;

	pr_debug("--- copy begin ---\n");
	pr_debug("sram_idx = %d, rbuf_idx = %d, "
		 "avail_out = %d, avail_in = %d\n",
		 prtd->sram_idx, prtd->rbuf_idx,
		 prtd->avail_out, prtd->avail_in);

	base = mmp2_find_dma_register_base(prtd->dma_ch);

	if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
		/*
		 * adjust sram index if dma has run ahead
		 * rather than memory copy speed
		 */
		offset = MMP2_DSAR(base) - prtd->sram_phys;
		rest = pxa910_sram_is_in_scope(prtd->sram_size,
				prtd->sram_idx * prtd->period_size,
				prtd->avail_out * prtd->period_size,
				offset);
		if (rest) {
			prtd->sram_idx = offset / prtd->period_size + 1;
			if (prtd->sram_idx >= prtd->sram_max_num)
				prtd->sram_idx = 0;
		}

		num = prtd->avail_out;

		for (i = 0; i < num; i++) {
			dst = (char *)(prtd->sram_virt +
				prtd->sram_idx * prtd->period_size);
			src = (char *)(prtd->rbuf_virt +
				prtd->rbuf_idx * prtd->period_size);
			pxa910_copy_data((int *)dst, (int *)src,
				prtd->period_size);

			prtd->sram_idx++;
			if (prtd->sram_idx >= prtd->sram_max_num)
				prtd->sram_idx = 0;
			prtd->rbuf_idx++;
			if (prtd->rbuf_idx >= prtd->rbuf_max_num)
				prtd->rbuf_idx = 0;
		}
		prtd->avail_out = 0;
	} else {
		/*
		 * check if have periods has not been over-written
		 * by dma polluted, will copy these periods data
		 * as possible.
		 */
		offset = MMP2_DDAR(base) - prtd->sram_phys;
		rest = pxa910_sram_is_in_scope(prtd->sram_size,
				prtd->sram_idx * prtd->period_size,
				prtd->avail_in * prtd->period_size,
				offset);
		if (rest) {
			prtd->sram_idx = offset / prtd->period_size + 1;
			if (prtd->sram_idx >= prtd->sram_max_num)
				prtd->sram_idx = 0;
			prtd->avail_in = rest / prtd->period_size;
		}

		num = prtd->avail_in;

		for (i = 0; i < num; i++) {
			dst = (char *)(prtd->rbuf_virt +
				prtd->rbuf_idx * prtd->period_size);
			src = (char *)(prtd->sram_virt +
				prtd->sram_idx * prtd->period_size);
			pxa910_copy_data((int *)dst, (int *)src,
				prtd->period_size);

			prtd->sram_idx++;
			if (prtd->sram_idx >= prtd->sram_max_num)
				prtd->sram_idx = 0;
			prtd->rbuf_idx++;
			if (prtd->rbuf_idx >= prtd->rbuf_max_num)
				prtd->rbuf_idx = 0;
		}
		prtd->avail_in = 0;
	}

	pr_debug("sram_idx = %d, rbuf_idx = %d, "
		 "avail_out = %d, avail_in = %d\n",
		 prtd->sram_idx, prtd->rbuf_idx,
		 prtd->avail_out, prtd->avail_in);
	pr_debug("--- copy end ---\n");

	return;
}

/* there is no interrupt for host when zsp is enable */
static void pxa910_squ_dma_irq(int dma_ch, void *dev_id)
{
	struct snd_pcm_substream *substream = dev_id;
	struct pxa910_runtime_data *prtd = substream->runtime->private_data;
	u32 base_register;

	if (cpu_is_mmp2()) {
		base_register = mmp2_find_dma_register_base(dma_ch);
		if (!base_register)
			return;

		if (!(MMP2_DISR(base_register) & 0x1)) {
			printk(KERN_ERR "%s: SQU error on channel %d \n",
				prtd->params->name, dma_ch);
			return;
		}

		/* clear adma irq status */
		MMP2_DISR(base_register) = 0;

		/* sync sram with ring buf */
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK)
			prtd->avail_out = min(prtd->avail_out + 1,
					      prtd->sram_max_num);
		else
			prtd->avail_in = min(prtd->avail_in + 1,
					     prtd->sram_max_num);
		pxa910_sync_sram(substream);
		snd_pcm_period_elapsed(substream);
	} else {
		if (SDISR(dma_ch) & 0x1) {
			snd_pcm_period_elapsed(substream);
		} else {
			printk(KERN_ERR "%s: SQU error on channel %d \n",
				prtd->params->name, dma_ch);
		}
		SDISR(dma_ch) = 0;
	}

	return;
}

static int pxa910_pcm_hw_params(struct snd_pcm_substream *substream,
	struct snd_pcm_hw_params *params)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct pxa910_runtime_data *prtd = runtime->private_data;
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct pxa3xx_pcm_dma_params *dma; // = rtd->dai->cpu_dai->dma_data;
	size_t totsize = params_buffer_bytes(params);
	size_t period  = params_period_bytes(params);
	pxa910_squ_desc *squ_desc;
	dma_addr_t dma_buff_phys, next_desc_phys;
	int ret;

	dma = snd_soc_dai_get_dma_data(rtd->dai->cpu_dai, substream);

	if (zsp_mode == 1) {
		/* setup adma conf */
		switch (params_format(params)) {
		case  SNDRV_PCM_FORMAT_S8:
			prtd->zsp_adma_conf.sample_size = 0x0;
			break;
		case SNDRV_PCM_FORMAT_S16_LE:
			prtd->zsp_adma_conf.sample_size = 0x2;
			break;
		case SNDRV_PCM_FORMAT_S20_3LE:
			prtd->zsp_adma_conf.sample_size = 0x3;
			break;
		case SNDRV_PCM_FORMAT_S24_3LE:
			prtd->zsp_adma_conf.sample_size = 0x4;
			break;
		case SNDRV_PCM_FORMAT_S32_LE:
			prtd->zsp_adma_conf.sample_size = 0x5;
			break;
		default:
			return -EINVAL;
		}
		prtd->zsp_adma_conf.pack_mode = ADMA_CONFIG_PACKMOD_PACK;
		runtime->dma_bytes = totsize;
		return 0;
	}

	/* return if this is a bufferless transfer e.g.
	 * codec <--> BT codec or GSM modem -- lg FIXME */
	if (!dma)
		return 0;

	if(!cpu_is_mmp2()){	
		if(substream->stream == SNDRV_PCM_STREAM_PLAYBACK)
	 	{
			if (0 == wait_event_timeout(dma_wq, !(SDCR(1)&SDCR_CHANEN), HZ))
				return -EBUSY;
			dma->dcmd = SDCR_DST_ADDR_HOLD | SDCR_SRC_ADDR_INC |
				SDCR_SSPMOD | SDCR_DMA_BURST_32B | SDCR_FETCHND;
		} else {
			if (0 == wait_event_timeout(dma_wq, !(SDCR(0)&SDCR_CHANEN), HZ))
				return -EBUSY;
			dma->dcmd = SDCR_SRC_ADDR_HOLD | SDCR_DST_ADDR_INC |
				SDCR_SSPMOD | SDCR_DMA_BURST_32B | SDCR_FETCHND;
	 	}
	}

	/* this may get called several times by oss emulation
	 * with different params */
	if (prtd->params == NULL) {
		prtd->params = dma;
		if(cpu_is_mmp2()){
			ret = mmp2_request_dma(prtd->params->name, *(dma->drcmr),
						pxa910_squ_dma_irq, substream);
			if (ret < 0)
				return ret;
		}else{
			if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
				ret = pxa910_request_squ(prtd->params->name, SQU_PRIO_LOW,
						pxa910_squ_dma_irq, substream);

				if (ret != 0)
					return -ENODEV;
			} else {
				ret = pxa910_request_squ(prtd->params->name, SQU_PRIO_LOW,
						pxa910_squ_dma_irq, substream);

				if (ret < 0)
					return ret;
				if (ret == 0) {
					ret = pxa910_request_squ(prtd->params->name, SQU_PRIO_LOW,
						pxa910_squ_dma_irq, substream);

					pxa910_free_squ(0);
					if (ret != 1)
						return -ENODEV;
				}
			}
		}
		prtd->dma_ch = ret;
	} else if (prtd->params != dma) {
		if(cpu_is_mmp2()){
			mmp2_free_dma(prtd->dma_ch);
			prtd->params = dma;
			ret = mmp2_request_dma(prtd->params->name, *(dma->drcmr),
						pxa910_squ_dma_irq, substream);
			if (ret < 0)
				return ret;

		}else{
			pxa910_free_squ(prtd->dma_ch);
			prtd->params = dma;
			if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
				ret = pxa910_request_squ(prtd->params->name, SQU_PRIO_LOW,
						pxa910_squ_dma_irq, substream);
				if (ret != 0)
					return -ENODEV;
			} else {
				ret = pxa910_request_squ(prtd->params->name, SQU_PRIO_LOW,
						pxa910_squ_dma_irq, substream);
				if (ret < 0)
					return ret;
				if (ret == 0) {
					ret = pxa910_request_squ(prtd->params->name, SQU_PRIO_LOW,
							pxa910_squ_dma_irq, substream);
					pxa910_free_squ(0);
					if (ret != 1)
						return -ENODEV;
				}
			}
		}
		prtd->dma_ch = ret;
	}

	snd_pcm_set_runtime_buffer(substream, &substream->dma_buffer);

	/* init prtd value */
	prtd->period_size   = period;

	prtd->rbuf_virt     = (u32)runtime->dma_area;
	prtd->rbuf_phys     = (u32)runtime->dma_addr;
	prtd->rbuf_idx	    = 0;
	prtd->rbuf_max_num  = totsize / prtd->period_size;
	prtd->rbuf_size	    = prtd->rbuf_max_num * period;

	/*
	 * the sram allocate size is MMP2_SRAM_DMA_BUF_SIZE,
	 * but it may NOT be used all of them and
	 * is decided by the user space buffer size
	 * if user buffer size < MMP2_SRAM_DMA_BUF_SIZE.
	 */
	prtd->sram_idx      = 0;
	prtd->sram_max_num  = min(totsize, (size_t)MMP2_SRAM_DMA_BUF_SIZE);
	prtd->sram_max_num  = prtd->sram_max_num / period;
	prtd->sram_size     = prtd->sram_max_num * period;

	prtd->avail_in	    = 0;
	prtd->avail_out	    = 0;

	totsize        = prtd->sram_size;
	next_desc_phys = prtd->squ_desc_array_phys;
	dma_buff_phys  = prtd->sram_phys;
	squ_desc       = prtd->squ_desc_array;
	do {
		next_desc_phys += sizeof(pxa910_squ_desc);

		squ_desc->nxt_desc = next_desc_phys;
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
			squ_desc->src_addr = dma_buff_phys;
			squ_desc->dst_addr = prtd->params->dev_addr;
		} else {
			squ_desc->src_addr = prtd->params->dev_addr;
			squ_desc->dst_addr = dma_buff_phys;
		}
		if (period > totsize)
			period = totsize;
		squ_desc->byte_cnt = period;
		squ_desc++;
		dma_buff_phys += period;

	} while (totsize -= period);
	squ_desc[-1].nxt_desc = prtd->squ_desc_array_phys;

	pxa910_dump_dma_list(prtd);
	return 0;
}

static int pxa910_pcm_hw_free(struct snd_pcm_substream *substream)
{
	struct pxa910_runtime_data *prtd = substream->runtime->private_data;

	if (zsp_mode == 0) {
		if (prtd->dma_ch != -1) {
			snd_pcm_set_runtime_buffer(substream, NULL);
			if (cpu_is_mmp2())
				mmp2_free_dma(prtd->dma_ch);
			else
				pxa910_free_squ(prtd->dma_ch);
			prtd->dma_ch = -1;
		}
	}

	return 0;
}

static int pxa910_pcm_prepare(struct snd_pcm_substream *substream)
{
	int ret = 0;
	struct pxa910_runtime_data *prtd = substream->runtime->private_data;

	if (zsp_mode == 1) {
		/* 2 channels, each channel 2 bytes */
		prtd->zsp_buf.zsp_period_bytes = \
			substream->runtime->period_size * 4;
		return 0;
	}

	if(cpu_is_mmp2()){
		u32 base_register = mmp2_find_dma_register_base(prtd->dma_ch);
		if(base_register){
			MMP2_DCR(base_register)= (prtd->params->dcmd) & (~SDCR_CHANEN);
			MMP2_DIMR(base_register) = SDIMR_COMP;
		}else{
			ret = -EINVAL;
		}
	}else{
		SDCR(prtd->dma_ch) = (prtd->params->dcmd) & (~SDCR_CHANEN);
		SDIMR(prtd->dma_ch) = SDIMR_COMP ;
	}

	return ret;
}

static int pxa910_pcm_trigger(struct snd_pcm_substream *substream, int cmd)
{
	struct pxa910_runtime_data *prtd = substream->runtime->private_data;
	unsigned long flags;
	u32 base_register = 0;
	int size;
	int ret = 0;

	pr_debug("%s enter\n", __func__);

	if (zsp_mode == 1) {
		cmd_audio_stream_trigger_t cmd_strm_tri;

		switch (cmd) {
		case SNDRV_PCM_TRIGGER_START:
		case SNDRV_PCM_TRIGGER_RESUME:
		case SNDRV_PCM_TRIGGER_PAUSE_RELEASE:
			cmd_strm_tri.command_id = AUDIO_STREAM_CMDID_TRIGGER;
			cmd_strm_tri.stream_id = prtd->stream_id;
			cmd_strm_tri.trigger_type = AUDIO_STREAM_TRIGGER_START;
			cmd_strm_tri.int_type = AUDIO_STREAM_INT_TYPE_THRESHOLD;
			cmd_strm_tri.int_threshold = 3;
			kzmq_write(prtd->zmq_deliver, (void *)&cmd_strm_tri,
					   sizeof(cmd_audio_stream_trigger_t));
			break;
		case SNDRV_PCM_TRIGGER_STOP:
		case SNDRV_PCM_TRIGGER_SUSPEND:
		case SNDRV_PCM_TRIGGER_PAUSE_PUSH:
			cmd_strm_tri.command_id = AUDIO_STREAM_CMDID_TRIGGER;
			cmd_strm_tri.stream_id = prtd->stream_id;
			cmd_strm_tri.trigger_type = AUDIO_STREAM_TRIGGER_STOP;
			kzmq_write(prtd->zmq_deliver, (void *)&cmd_strm_tri,
					   sizeof(cmd_audio_stream_trigger_t));
			break;
		default:
			return -EINVAL;
		}
		return 0;
	}

	if (cpu_is_mmp2()) {
		base_register = mmp2_find_dma_register_base(prtd->dma_ch);
		if(!base_register)
			return -EINVAL;
	}

	switch (cmd) {
	case SNDRV_PCM_TRIGGER_START:
		if (cpu_is_mmp2()) {

			spin_lock_irqsave(&prtd->lock, flags);
			prtd->sram_idx	= 0;
			prtd->rbuf_idx	= 0;
			prtd->avail_in 	= 0;

			/*
			 * if the start_threshold is less than the
			 * sram size, so set zero to the sram firstly
			 * and skip the first period. After that can
			 * copy data to sram.
			 */
			size = frames_to_bytes(substream->runtime,
				substream->runtime->start_threshold);
			if (size < prtd->sram_size) {
				memset((void *)prtd->sram_virt, 0,
				       prtd->sram_size);
				prtd->avail_out = 0;
			} else
				prtd->avail_out = prtd->sram_max_num;

			pr_debug("%s: sram_max_num = %d, start_threshold = %d\n",
				__func__, prtd->sram_max_num, size);

			pxa910_sync_sram(substream);
			spin_unlock_irqrestore(&prtd->lock, flags);

			MMP2_DNDPR(base_register) = prtd->squ_desc_array_phys;
			MMP2_DCR(base_register)= prtd->params->dcmd | SDCR_CHANEN;
		} else {
			SDNDPR(prtd->dma_ch) = prtd->squ_desc_array_phys;
			if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK)
				SDSAR(prtd->dma_ch) = substream->runtime->dma_addr;
			else
				SDDAR(prtd->dma_ch) = substream->runtime->dma_addr;
			SDCR(prtd->dma_ch) |= SDCR_CHANEN;
		}

		break;

	case SNDRV_PCM_TRIGGER_STOP:
	case SNDRV_PCM_TRIGGER_SUSPEND:
	case SNDRV_PCM_TRIGGER_PAUSE_PUSH:
		if(cpu_is_mmp2())
			MMP2_DCR(base_register)= prtd->params->dcmd;
		else
			SDCR(prtd->dma_ch) &= ~SDCR_CHANEN;
		wake_up(&dma_wq);
		break;

	case SNDRV_PCM_TRIGGER_RESUME:
		if(cpu_is_mmp2())
			MMP2_DCR(base_register)= prtd->params->dcmd | SDCR_CHANEN;
		else
			SDCR(prtd->dma_ch) |= SDCR_CHANEN;
		break;

	case SNDRV_PCM_TRIGGER_PAUSE_RELEASE:
		if(cpu_is_mmp2()){
			MMP2_DNDPR(base_register) = prtd->squ_desc_array_phys;
			MMP2_DCR(base_register)= prtd->params->dcmd | SDCR_CHANEN;
		}else{
			SDNDPR(prtd->dma_ch) = prtd->squ_desc_array_phys;
			SDCR(prtd->dma_ch) |= SDCR_CHANEN;
		}
		break;

	default:
		ret = -EINVAL;
	}

	return ret;
}

static snd_pcm_uframes_t
pxa910_pcm_pointer(struct snd_pcm_substream *substream)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct pxa910_runtime_data *prtd = runtime->private_data;
	dma_addr_t ptr;
	snd_pcm_uframes_t x;

	pr_debug("%s enter\n", __func__);

	if (zsp_mode == 1) {
		x = prtd->zsp_buf.zsp_offset;
		x = bytes_to_frames(runtime, x);
		if (x == prtd->zsp_buf.buf_len/4)
			x = 0;
	} else {
		if (cpu_is_mmp2())
			x = bytes_to_frames(runtime,
				prtd->rbuf_idx * prtd->period_size);
		else {
			ptr = (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) ?
				SDSAR(prtd->dma_ch) : SDDAR(prtd->dma_ch);
			x = bytes_to_frames(runtime, ptr - runtime->dma_addr);
		}

		if (x == runtime->buffer_size)
			x = 0;
	}
	return x;
}

static void zsp_msg_handler(struct work_struct *work)
{
	struct pxa910_runtime_data *prtd;
	cmd_audio_stream_t zsp_data_received;
	cmd_audio_stream_datarxtx_t cmd_strm_tx;
	ack_audio_stream_release_t *p_zsp_release;
	struct snd_pcm_substream *substream;

	prtd = container_of(work, struct pxa910_runtime_data, zsp_queue);
	if (prtd == NULL)
		return;
	if (prtd->zmq_deliver == NULL)
		return;
	substream = prtd->substream;
	while (zsp_mode == 1) {
		kzmq_read((void *)prtd->zmq_deliver, (void *)&zsp_data_received,
				  sizeof(ack_audio_stream_t));
		switch (zsp_data_received.command_id) {
		case AUDIO_STREAM_CMDID_ACQUIRE:
			/* suppose it is OK, tbd later */
			break;
		case AUDIO_STREAM_CMDID_PREPARE:
			/* tbd */
			break;
		case AUDIO_STREAM_CMDID_TRIGGER:
			break;
		case AUDIO_STREAM_CMDID_POSITION:
			/* tbd */
			break;
		case AUDIO_STREAM_CMDID_DATARXTXREQ:
			cmd_strm_tx.command_id = AUDIO_STREAM_CMDID_DATARXTX;
			cmd_strm_tx.stream_id = prtd->stream_id;
			cmd_strm_tx.addr = prtd->zsp_buf.buf_phys +\
				prtd->zsp_buf.zsp_offset;
			cmd_strm_tx.size = prtd->zsp_buf.zsp_period_bytes;
			kzmq_write(prtd->zmq_deliver, (void *)&cmd_strm_tx,
					   sizeof(cmd_audio_stream_datarxtx_t));

			if ((prtd->zsp_buf.zsp_offset + \
				 prtd->zsp_buf.zsp_period_bytes) >= \
				(prtd->zsp_buf.buf_len))
				prtd->zsp_buf.zsp_offset = 0;
			else
				prtd->zsp_buf.zsp_offset += \
					prtd->zsp_buf.zsp_period_bytes;
			break;
		case AUDIO_STREAM_CMDID_DATARXTX:
			/* tbd */
			snd_pcm_period_elapsed(substream);
			break;
		case AUDIO_STREAM_CMDID_RELEASE:
			p_zsp_release = \
			(ack_audio_stream_release_t *)&zsp_data_received;
			if (p_zsp_release->return_code == 0) {
				complete(&prtd->zmq_cmd_completion);
				return;
			}
			break;
		default:
			/* unknown command */
			break;
		}
	}
}

static int pxa910_pcm_open(struct snd_pcm_substream *substream)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct pxa910_runtime_data *prtd;
	int ret;

	if (cpu_is_mmp2() && !cpu_is_mmp2_z0() && !cpu_is_mmp2_z1())
		snd_soc_set_runtime_hwparams(substream, &mmp2_a0_pcm_hardware);
	else
		snd_soc_set_runtime_hwparams(substream, &pxa910_pcm_hardware);

	/*
	 * For mysterious reasons (and despite what the manual says)
	 * playback samples are lost if the DMA count is not a multiple
	 * of the DMA burst size.  Let's add a rule to enforce that.
	 */
	ret = snd_pcm_hw_constraint_step(runtime, 0,
		SNDRV_PCM_HW_PARAM_PERIOD_BYTES, 32);
	if (ret)
		goto out;

	ret = snd_pcm_hw_constraint_step(runtime, 0,
		SNDRV_PCM_HW_PARAM_BUFFER_BYTES, 32);
	if (ret)
		goto out;

	ret = snd_pcm_hw_constraint_integer(runtime,
					SNDRV_PCM_HW_PARAM_PERIODS);
	if (ret < 0)
		goto out;

	prtd = kzalloc(sizeof(struct pxa910_runtime_data), GFP_KERNEL);
	if (prtd == NULL) {
		ret = -ENOMEM;
		goto out;
	}

	prtd->substream = substream;
	prtd->zsp_buf.buf = kzalloc(runtime->hw.buffer_bytes_max, GFP_KERNEL);
	prtd->zsp_buf.buf_phys = virt_to_phys(prtd->zsp_buf.buf);
	prtd->zsp_buf.zsp_offset = 0;
	prtd->zsp_buf.app_offset = 0;
	prtd->zsp_buf.buf_len = runtime->hw.buffer_bytes_max;
	INIT_WORK(&prtd->zsp_queue, zsp_msg_handler);

	runtime->private_data = prtd;
	prtd->dma_ch = -1;

	if (zsp_mode == 1)
		return 0;

	if (cpu_is_mmp2() && !cpu_is_mmp2_z0() && !cpu_is_mmp2_z1()) {
		/*
		 * avoid sram fragment, allocate dma buffer and
		 * dma desc list at the same time.
		 */
		prtd->sram_virt = (unsigned int)audio_sram_alloc(
				MMP2_SRAM_DMA_BUF_SIZE + (PAGE_SIZE >> 2),
				(dma_addr_t *)&prtd->sram_phys);
		prtd->squ_desc_array = (void *)(prtd->sram_virt +
				MMP2_SRAM_DMA_BUF_SIZE);
		prtd->squ_desc_array_phys = (dma_addr_t)(prtd->sram_phys +
				MMP2_SRAM_DMA_BUF_SIZE);

		spin_lock_init(&prtd->lock);
	} else
		prtd->squ_desc_array =
			prtd->squ_desc_array = pxa910_sram_alloc(PAGE_SIZE, &prtd->squ_desc_array_phys);

	if (!prtd->squ_desc_array || !prtd->sram_virt) {
		ret = -ENOMEM;
		goto err1;
	}

	pr_debug("%s: dma desc array (va : pa) 0x%x : 0x%x\n",
		__func__, (int)prtd->squ_desc_array,
		(int)prtd->squ_desc_array_phys);
	pr_debug("%s: sram (va : pa) 0x%x : 0x%x\n",
		__func__, (int)prtd->sram_virt, (int)prtd->sram_phys);

	return 0;

 err1:
	kfree(prtd);
 out:
	return ret;
}

static int pxa910_pcm_close(struct snd_pcm_substream *substream)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct pxa910_runtime_data *prtd = runtime->private_data;

	if (cpu_is_mmp2() && !cpu_is_mmp2_z0() && !cpu_is_mmp2_z1()) {
		audio_sram_free((void *)prtd->sram_virt,
				MMP2_SRAM_DMA_BUF_SIZE + (PAGE_SIZE >> 2));
	} else
		pxa910_sram_free(prtd->squ_desc_array);

	kfree(prtd->zsp_buf.buf);
	kfree(prtd);
	runtime->private_data = NULL;
	return 0;
}

#ifdef CONFIG_MMP_ZSP
static int pxa910_pcm_copy(struct snd_pcm_substream *substream,
				int channel, snd_pcm_uframes_t pos,
				void *buf, snd_pcm_uframes_t count)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct pxa910_runtime_data *prtd = runtime->private_data;

	if (zsp_mode == 1) {
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
			if (copy_from_user(prtd->zsp_buf.buf + \
					prtd->zsp_buf.app_offset,
					buf, frames_to_bytes(runtime, count)))
				return -EFAULT;
			if ((prtd->zsp_buf.app_offset + \
				 frames_to_bytes(runtime, count)) >= \
				prtd->zsp_buf.buf_len)
				prtd->zsp_buf.app_offset = 0;
			else
				prtd->zsp_buf.app_offset +=\
					frames_to_bytes(runtime, count);
		} else {
			if (copy_to_user(buf, prtd->zsp_buf.buf +
					prtd->zsp_buf.app_offset,
					frames_to_bytes(runtime, count)))
				return -EFAULT;
			if ((prtd->zsp_buf.app_offset + \
				 frames_to_bytes(runtime, count)) >= \
				prtd->zsp_buf.buf_len)
				prtd->zsp_buf.app_offset = 0;
			else
				prtd->zsp_buf.app_offset += \
					frames_to_bytes(runtime, count);
		}
	} else {
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
			char *hwbuf = runtime->dma_area + \
				frames_to_bytes(runtime, pos);
			if (copy_from_user(hwbuf, buf,
					frames_to_bytes(runtime, count)))
				return -EFAULT;
		} else {
			char *hwbuf = runtime->dma_area + \
				frames_to_bytes(runtime, pos);
			if (copy_to_user(buf, hwbuf,
					frames_to_bytes(runtime, count)))
				return -EFAULT;
		}
	}

	return 0;
}
#endif

static int pxa910_pcm_mmap(struct snd_pcm_substream *substream,
	struct vm_area_struct *vma)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct pxa910_runtime_data *prtd = runtime->private_data;
	unsigned long off = vma->vm_pgoff;
	int ret;

	if (zsp_mode == 1) {
		vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
		ret = remap_pfn_range(vma, vma->vm_start,
				__phys_to_pfn(prtd->zsp_buf.buf_phys) + off,
				vma->vm_end - vma->vm_start,
				vma->vm_page_prot);
		return ret;
	}

	if(cpu_is_mmp2())
		return mmp2_sram_mmap_noncached(vma,
					     runtime->dma_area,
					     runtime->dma_addr,
					     runtime->dma_bytes);
	else
		return pxa910_sram_mmap_writecombine(vma,
					     runtime->dma_area,
					     runtime->dma_addr,
					     runtime->dma_bytes);
}

struct snd_pcm_ops pxa910_pcm_ops = {
	.open		= pxa910_pcm_open,
	.close		= pxa910_pcm_close,
	.ioctl		= snd_pcm_lib_ioctl,
	.hw_params	= pxa910_pcm_hw_params,
	.hw_free	= pxa910_pcm_hw_free,
	.prepare	= pxa910_pcm_prepare,
	.trigger	= pxa910_pcm_trigger,
	.pointer	= pxa910_pcm_pointer,
#ifdef CONFIG_MMP_ZSP
	.copy		= pxa910_pcm_copy,
#endif
	.mmap		= pxa910_pcm_mmap,
};

void mmp2_save_adma_regs(struct snd_pcm_substream *substream)
{
	u32 base_register;
	int ch;

	struct snd_pcm_runtime *runtime;
	struct pxa910_runtime_data *prtd;

	if (substream == NULL)
		return;

	runtime = substream->runtime;
	if (runtime == NULL)
		return;		/* No open PCM, return directly */

	prtd = runtime->private_data;
	if (prtd == NULL)
		return;

	ch = prtd->dma_ch;
	base_register = mmp2_find_dma_register_base(ch);

	/* MMP2 only uses chain mode */
	prtd->pxa688_adma_saved[ch - 2].src_addr = MMP2_DSAR(base_register);
	prtd->pxa688_adma_saved[ch - 2].dest_addr = MMP2_DDAR(base_register);
	prtd->pxa688_adma_saved[ch - 2].next_desc_ptr = \
			MMP2_DNDPR(base_register);
	prtd->pxa688_adma_saved[ch - 2].ctrl = MMP2_DCR(base_register);
	if (ch == 2 || ch == 4) {
		prtd->pxa688_adma_saved[ch - 2].chan_pri = \
				MMP2_DCP(base_register);
	}
	prtd->pxa688_adma_saved[ch - 2].curr_desc_ptr = \
			MMP2_DCDPR(base_register);
	prtd->pxa688_adma_saved[ch - 2].intr_mask = MMP2_DIMR(base_register);
	prtd->pxa688_adma_saved[ch - 2].intr_status = \
		MMP2_DISR(base_register);

	return;
}
EXPORT_SYMBOL_GPL(mmp2_save_adma_regs);

void mmp2_restore_adma_regs(struct snd_pcm_substream *substream)
{
	u32 base_register;
	int ch;

	struct snd_pcm_runtime *runtime;
	struct pxa910_runtime_data *prtd;

	if (substream == NULL)
		return;

	runtime = substream->runtime;
	if (runtime == NULL)
		return;		/* No open PCM, return directly */

	prtd = runtime->private_data;
	if (prtd == NULL)
		return;

	ch = prtd->dma_ch;
	base_register = mmp2_find_dma_register_base(ch);

	/* MMP2 only uses chain mode */
	MMP2_DNDPR(base_register) = \
		prtd->pxa688_adma_saved[ch - 2].next_desc_ptr;
	if (ch == 2 || ch == 4) {
		MMP2_DCP(base_register) = \
			prtd->pxa688_adma_saved[ch - 2].chan_pri;
	}
	MMP2_DIMR(base_register) = prtd->pxa688_adma_saved[ch - 2].intr_mask;
	MMP2_DCR(base_register) = prtd->pxa688_adma_saved[ch - 2].ctrl;

	return;
}
EXPORT_SYMBOL_GPL(mmp2_restore_adma_regs);

int mmp2_save_desc(struct snd_pcm_substream *substream)
{
	dma_addr_t squ_desc_array_phys;
	struct snd_pcm_runtime *runtime;
	struct pxa910_runtime_data *prtd;
	pxa910_squ_desc *squ_desc_save, *squ_desc;

	if (substream == NULL)
		return 0;

	runtime = substream->runtime;
	if (runtime == NULL)
		return 0;		/* No open PCM, return directly */

	prtd = runtime->private_data;
	if (prtd == NULL)
		return 0;

	if (prtd->squ_desc_array_saved)
		/* already saved */
		return 0;

	prtd->squ_desc_array_saved =
		kmalloc(sizeof(struct pxa910_squ_desc) * 64, GFP_KERNEL);
	if (!prtd->squ_desc_array_saved)
		return -ENOMEM;

	squ_desc_array_phys = prtd->squ_desc_array_phys;
	squ_desc = prtd->squ_desc_array;
	squ_desc_save = prtd->squ_desc_array_saved;

	while (squ_desc && (squ_desc->nxt_desc != squ_desc_array_phys)) {
		squ_desc_save->byte_cnt = squ_desc->byte_cnt;
		squ_desc_save->src_addr = squ_desc->src_addr;
		squ_desc_save->dst_addr = squ_desc->dst_addr;
		squ_desc_save->nxt_desc = squ_desc->nxt_desc;
		squ_desc++;
		squ_desc_save++;
	}
	squ_desc_save->byte_cnt = squ_desc->byte_cnt;
	squ_desc_save->src_addr = squ_desc->src_addr;
	squ_desc_save->dst_addr = squ_desc->dst_addr;
	squ_desc_save->nxt_desc = squ_desc->nxt_desc;
	return 0;
}
EXPORT_SYMBOL_GPL(mmp2_save_desc);

void mmp2_restore_desc(struct snd_pcm_substream *substream)
{
	dma_addr_t squ_desc_array_phys;
	struct snd_pcm_runtime *runtime;
	struct pxa910_runtime_data *prtd;
	pxa910_squ_desc *squ_desc_save, *squ_desc;

	if (substream == NULL)
		return;

	runtime = substream->runtime;
	if (runtime == NULL)
		return;		/* No open PCM, return directly */

	prtd = runtime->private_data;
	if (prtd == NULL)
		return;

	if (!prtd->squ_desc_array_saved)
		/* not saved or already restored? */
		return;

	/* clean sram buffer */
	memset((void *)(prtd->sram_virt), 0, prtd->sram_size);

	squ_desc_array_phys = prtd->squ_desc_array_phys;
	squ_desc = prtd->squ_desc_array;
	squ_desc_save = prtd->squ_desc_array_saved;

	while (squ_desc_save && \
		(squ_desc_save->nxt_desc != squ_desc_array_phys)) {
		squ_desc->byte_cnt = squ_desc_save->byte_cnt;
		squ_desc->src_addr = squ_desc_save->src_addr;
		squ_desc->dst_addr = squ_desc_save->dst_addr;
		squ_desc->nxt_desc = squ_desc_save->nxt_desc;
		squ_desc++;
		squ_desc_save++;
	}
	squ_desc->byte_cnt = squ_desc_save->byte_cnt;
	squ_desc->src_addr = squ_desc_save->src_addr;
	squ_desc->dst_addr = squ_desc_save->dst_addr;
	squ_desc->nxt_desc = squ_desc_save->nxt_desc;
	kfree(prtd->squ_desc_array_saved);
	prtd->squ_desc_array_saved = NULL;
	return;
}
EXPORT_SYMBOL_GPL(mmp2_restore_desc);

static int pxa910_pcm_preallocate_dma_buffer(struct snd_pcm *pcm, int stream)
{
	struct snd_pcm_substream *substream = pcm->streams[stream].substream;
	struct snd_dma_buffer *buf = &substream->dma_buffer;

	size_t size = pxa910_pcm_hardware.buffer_bytes_max;

	pr_debug("%s enter\n", __func__);

	if (cpu_is_mmp2() && !cpu_is_mmp2_z0() && !cpu_is_mmp2_z1())
		size = mmp2_a0_pcm_hardware.buffer_bytes_max;

	buf->dev.type = SNDRV_DMA_TYPE_DEV;
	buf->dev.dev = pcm->card->dev;
	buf->private_data = NULL;
	buf->area = dma_alloc_coherent(pcm->card->dev, size,
			&buf->addr, GFP_KERNEL);
	if (!buf->area)
		return -ENOMEM;
	buf->bytes = size;

	printk(KERN_INFO "%s: pre-alloc dma buf (va : pa) 0x%x : 0x%x\n",
		__func__, (int)buf->area, (int)buf->addr);

	return 0;
}

static void pxa910_pcm_free_dma_buffers(struct snd_pcm *pcm)
{
	struct snd_pcm_substream *substream;
	struct snd_dma_buffer *buf;
	int stream;

	pr_debug("%s enter\n", __func__);

	for (stream = 0; stream < 2; stream++) {
 		substream = pcm->streams[stream].substream;
		if (!substream)
			continue;

		buf = &substream->dma_buffer;
		if (!buf->area)
			continue;

		dma_free_coherent(pcm->card->dev, buf->bytes,
				  buf->area, buf->addr);
		buf->area = NULL;
	}
	return;
}

static u64 pxa910_pcm_dmamask = DMA_BIT_MASK(64);

int pxa910_pcm_new(struct snd_card *card, struct snd_soc_dai *dai,
	struct snd_pcm *pcm)
{
	int ret = 0;

	if (!card->dev->dma_mask)
		card->dev->dma_mask = &pxa910_pcm_dmamask;

	if (!card->dev->coherent_dma_mask)
		card->dev->coherent_dma_mask = DMA_BIT_MASK(64);

	if (zsp_mode == 1)
		return 0;

	if (dai->playback.channels_min) {
		ret = pxa910_pcm_preallocate_dma_buffer(pcm,SNDRV_PCM_STREAM_PLAYBACK);
		if (ret)
			goto out;
	}

	if (dai->capture.channels_min) {
		ret = pxa910_pcm_preallocate_dma_buffer(pcm,SNDRV_PCM_STREAM_CAPTURE);
		if (ret)
			goto out;
	}
 out:
	return ret;
}

struct snd_soc_platform pxa910_soc_platform = {
	.name		= "pxa910-audio",
	.pcm_ops 	= &pxa910_pcm_ops,
	.pcm_new	= pxa910_pcm_new,
	.pcm_free	= pxa910_pcm_free_dma_buffers,
};
EXPORT_SYMBOL_GPL(pxa910_soc_platform);

static int __init pxa910_pcm_modinit(void)
{
	return snd_soc_register_platform(&pxa910_soc_platform);
}
module_init(pxa910_pcm_modinit);

static void __exit pxa910_pcm_modexit(void)
{
	snd_soc_unregister_platform(&pxa910_soc_platform);
}
module_exit(pxa910_pcm_modexit);

MODULE_AUTHOR("bin.yang@marvell.com");
MODULE_DESCRIPTION("PXA910 SQU DMA module");
MODULE_LICENSE("GPL");
