#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/dma-mapping.h>
#include <linux/uaccess.h>
#include "pxa168fb_common.h"

static int check_yuv_status(FBVideoMode videoMode, struct pxa168fb_info *fbi,
		int video_layer)
{
	u32 x;

	if (video_layer) {     /* now in video layer */
		x = dma_ctrl_read(fbi->id, 0);

		if (!((videoMode & 0xf00) >> 8)) {
			if ((x & 0xf0000)>>16 == 0x5) {
				/* graphic layer broadcast YUV */
				pr_warning(" vid layer: dma_ctrl0 0x%x\n", x);
				return -EFAULT;
			}
		}
	} else {
		x = dma_ctrl_read(fbi->id, 0);

		if (!((videoMode & 0xf00) >> 8)) {
			if ((x & 0xf00000)>>20 >= 0x5
					&& (x & 0xf00000)>>20 <= 0x7) {
				pr_warning(" gfx layer: dma_ctrl0 0x%x\n", x);
				return -EFAULT; /* video layer broadcast YUV */
			}
		}
	}

	return 0;
}

static void ovlysurface_clear_pitch(struct _sOvlySurface *surface)
{
	surface->viewPortInfo.yPitch = 0;
	surface->viewPortInfo.uPitch = 0;
	surface->viewPortInfo.vPitch = 0;
}

static void update_surface(struct _sOvlySurface *surface)
{
	int tmp;

	if (surface->viewPortInfo.rotation == 90 ||
		surface->viewPortInfo.rotation == 270) {
		surface->viewPortInfo.rotation = 360 -
		surface->viewPortInfo.rotation;
		tmp = surface->viewPortInfo.srcWidth;
		surface->viewPortInfo.srcWidth =
		surface->viewPortInfo.srcHeight;
		surface->viewPortInfo.srcHeight = tmp;
		switch (surface->videoMode) {
		case FB_VMODE_YUV422PACKED:
			surface->videoMode = FB_VMODE_YUV422PACKED_IRE_90_270;
			surface->viewPortInfo.yuv_format = 1;
			ovlysurface_clear_pitch(surface);
			break;
		case FB_VMODE_YUV422PACKED_SWAPUV:
			surface->videoMode = FB_VMODE_YUV422PACKED_IRE_90_270;
			surface->viewPortInfo.yuv_format = 2;
			ovlysurface_clear_pitch(surface);
			break;
		case FB_VMODE_YUV422PACKED_SWAPYUorV:
			surface->videoMode = FB_VMODE_YUV422PACKED_IRE_90_270;
			surface->viewPortInfo.yuv_format = 4;
			ovlysurface_clear_pitch(surface);
			break;
		default:
			surface->viewPortInfo.yuv_format = 0;
		}
	}
}


int unsupport_format(struct pxa168fb_info *fbi, struct _sViewPortInfo
		viewPortInfo, FBVideoMode videoMode, int video_layer)
{
	if (check_yuv_status(videoMode, fbi, video_layer) < 0)
		return 1;

	if ((viewPortInfo.rotation == 0) || (viewPortInfo.rotation == 1)) {
		if (!video_layer) {
			/* In graphic layer now */
			switch (videoMode) {
			case FB_VMODE_YUV422PLANAR:
			case FB_VMODE_YUV420PLANAR:
				printk(KERN_ERR "Planar is not supported!\n");
				return 1;
			default:
				break;
			}
		}

		return 0;
	}

	if (viewPortInfo.srcHeight == 1080) {
		printk(KERN_ERR "1080P rotation is not supported!\n");
		return 1;
	}
	if (viewPortInfo.srcHeight == 720) {
		if (viewPortInfo.rotation == 180) {
			printk(KERN_ERR
				"720p rotation 180 is not supported!\n");
			return 1;
		}
	}

	switch (videoMode) {
	case FB_VMODE_YUV422PLANAR:
	case FB_VMODE_YUV422PLANAR_SWAPUV:
	case FB_VMODE_YUV422PLANAR_SWAPYUorV:
	case FB_VMODE_YUV420PLANAR:
	case FB_VMODE_YUV420PLANAR_SWAPUV:
	case FB_VMODE_YUV420PLANAR_SWAPYUorV:
		printk(KERN_ERR "Planar is not supported!\n");
		return 1;
	default:
		break;
	}
	return 0;
}


u8 *buf_dequeue(u8 **ppBufList)
{
	int i;
	u8 *pBuf;

	if (!ppBufList) {
		printk(KERN_ALERT "%s: invalid list\n", __func__);
		return NULL;
	}

	pBuf = ppBufList[0];

	for (i = 1; i < MAX_QUEUE_NUM; i++) {
		if (!ppBufList[i]) {
			ppBufList[i-1] = 0;
			break;
		}
		ppBufList[i-1] = ppBufList[i];
	}

	if (i >= MAX_QUEUE_NUM)
		printk(KERN_ALERT "%s: buffer overflow\n",  __func__);

	return pBuf;
}

int buf_enqueue(u8 **ppBufList, u8 *pBuf)
{
	int i = 0 ;
	struct _sOvlySurface *srf0 = (struct _sOvlySurface *)(pBuf);
	struct _sOvlySurface *srf1 = 0;

	/* Error checking */
	if (!srf0)
		return -1;

	for (; i < MAX_QUEUE_NUM; i++) {
		srf1 = (struct _sOvlySurface *)ppBufList[i];
		if (!srf1) {
			ppBufList[i] = pBuf;
			return 0;
		}
	}

	if (i >= MAX_QUEUE_NUM)
		printk(KERN_INFO "buffer overflow\n");

	return -3;
}

u8 *buf_gethead(u8 **ppBufList)
{
	u8 *pBuf;

	if (!ppBufList)
		return NULL;

	pBuf = ppBufList[0];

	return pBuf;
}

void buf_clear(u8 **ppBufList, int iFlag)
{
	int i = 0;

	/* Check null pointer. */
	if (!ppBufList)
		return;

	/* free */
	if (FREE_ENTRY & iFlag) {
		for (i = 0; i < MAX_QUEUE_NUM; i++) {
			if (ppBufList && ppBufList[i])
				kfree(ppBufList[i]);
		}
	}

	if (RESET_BUF & iFlag)
		memset(ppBufList, 0, MAX_QUEUE_NUM * sizeof(u8 *));
}

void clear_buffer(struct pxa168fb_info *fbi, int video_layer)
{
	unsigned long flags;

	spin_lock_irqsave(&fbi->buf_lock, flags);
	clearFilterBuf(fbi->filterBufList, RESET_BUF);
	buf_clear(fbi->buf_freelist, RESET_BUF|FREE_ENTRY);
	buf_clear(fbi->buf_waitlist, RESET_BUF|FREE_ENTRY);
	kfree(fbi->buf_current);
	fbi->buf_current = 0;
	kfree(fbi->buf_retired);
	fbi->buf_retired = 0;
	if (video_layer)
		ovly_info.wait_peer = 0;
	else
		gfx_info.wait_peer = 0;
	spin_unlock_irqrestore(&fbi->buf_lock, flags);
}

int convert_pix_fmt(u32 vmode)
{
/*	printk(KERN_INFO "vmode=%d\n", vmode); */
	switch (vmode) {
	case FB_VMODE_YUV422PACKED:
		return PIX_FMT_YUV422PACK;
	case FB_VMODE_YUV422PACKED_SWAPUV:
		return PIX_FMT_YVU422PACK;
	case FB_VMODE_YUV422PLANAR:
		return PIX_FMT_YUV422PLANAR;
	case FB_VMODE_YUV422PLANAR_SWAPUV:
		return PIX_FMT_YVU422PLANAR;
	case FB_VMODE_YUV420PLANAR:
		return PIX_FMT_YUV420PLANAR;
	case FB_VMODE_YUV420PLANAR_SWAPUV:
		return PIX_FMT_YVU420PLANAR;
	case FB_VMODE_YUV422PACKED_SWAPYUorV:
		return PIX_FMT_YUYV422PACK;
	case FB_VMODE_YUV422PACKED_IRE_90_270:
		return PIX_FMT_YUV422PACK_IRE_90_270;
	case FB_VMODE_RGB565:
		return PIX_FMT_RGB565;
	case FB_VMODE_BGR565:
		return PIX_FMT_BGR565;
	case FB_VMODE_RGB1555:
		return PIX_FMT_RGB1555;
	case FB_VMODE_BGR1555:
		return PIX_FMT_BGR1555;
	case FB_VMODE_RGB888PACK:
		return PIX_FMT_RGB888PACK;
	case FB_VMODE_BGR888PACK:
		return PIX_FMT_BGR888PACK;
	case FB_VMODE_RGBA888:
		return PIX_FMT_RGBA888;
	case FB_VMODE_BGRA888:
		return PIX_FMT_BGRA888;
	case FB_VMODE_RGB888UNPACK:
		return PIX_FMT_RGB888UNPACK;
	case FB_VMODE_BGR888UNPACK:
		return PIX_FMT_BGR888UNPACK;
	case FB_VMODE_YUV422PLANAR_SWAPYUorV:
	case FB_VMODE_YUV420PLANAR_SWAPYUorV:
	default:
		return -1;
	}
}

int set_pix_fmt(struct fb_var_screeninfo *var, int pix_fmt)
{
	switch (pix_fmt) {
	case PIX_FMT_RGB565:
		var->bits_per_pixel = 16;
		var->red.offset = 11;    var->red.length = 5;
		var->green.offset = 5;   var->green.length = 6;
		var->blue.offset = 0;    var->blue.length = 5;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		break;
	case PIX_FMT_BGR565:
		var->bits_per_pixel = 16;
		var->red.offset = 0;     var->red.length = 5;
		var->green.offset = 5;   var->green.length = 6;
		var->blue.offset = 11;   var->blue.length = 5;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		break;
	case PIX_FMT_RGB1555:
		var->bits_per_pixel = 16;
		var->red.offset = 10;    var->red.length = 5;
		var->green.offset = 5;   var->green.length = 5;
		var->blue.offset = 0;    var->blue.length = 5;
		var->transp.offset = 15; var->transp.length = 1;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 5 << 20;
		break;
	case PIX_FMT_BGR1555:
		var->bits_per_pixel = 16;
		var->red.offset = 0;     var->red.length = 5;
		var->green.offset = 5;   var->green.length = 5;
		var->blue.offset = 10;   var->blue.length = 5;
		var->transp.offset = 15; var->transp.length = 1;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 5 << 20;
		break;
	case PIX_FMT_RGB888PACK:
		var->bits_per_pixel = 24;
		var->red.offset = 16;    var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 0;    var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 6 << 20;
		break;
	case PIX_FMT_BGR888PACK:
		var->bits_per_pixel = 24;
		var->red.offset = 0;     var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 16;   var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 6 << 20;
		break;
	case PIX_FMT_RGB888UNPACK:
		var->bits_per_pixel = 32;
		var->red.offset = 16;    var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 0;    var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 7 << 20;
		break;
	case PIX_FMT_BGR888UNPACK:
		var->bits_per_pixel = 32;
		var->red.offset = 0;     var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 16;   var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 7 << 20;
		break;
	case PIX_FMT_RGBA888:
		var->bits_per_pixel = 32;
		var->red.offset = 16;    var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 0;    var->blue.length = 8;
		var->transp.offset = 24; var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 8 << 20;
		break;
	case PIX_FMT_BGRA888:
		var->bits_per_pixel = 32;
		var->red.offset = 0;     var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 16;   var->blue.length = 8;
		var->transp.offset = 24; var->transp.length = 8;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 8 << 20;
		break;
	case PIX_FMT_YUYV422PACK:
		var->bits_per_pixel = 16;
		var->red.offset = 8;     var->red.length = 16;
		var->green.offset = 4;   var->green.length = 16;
		var->blue.offset = 0;   var->blue.length = 16;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 9 << 20;
		break;
	case PIX_FMT_YVU422PACK:
		var->bits_per_pixel = 16;
		var->red.offset = 0;     var->red.length = 16;
		var->green.offset = 8;   var->green.length = 16;
		var->blue.offset = 12;   var->blue.length = 16;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 9 << 20;
		break;
	case PIX_FMT_YUV422PACK:
		var->bits_per_pixel = 16;
		var->red.offset = 4;     var->red.length = 16;
		var->green.offset = 12;   var->green.length = 16;
		var->blue.offset = 0;    var->blue.length = 16;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 9 << 20;
		break;
	case PIX_FMT_PSEUDOCOLOR:
		var->bits_per_pixel = 8;
		var->red.offset = 0;     var->red.length = 8;
		var->green.offset = 0;   var->green.length = 8;
		var->blue.offset = 0;    var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 0;
		break;
	case PIX_FMT_YUV422PLANAR:
		var->bits_per_pixel = 16;
		var->red.offset = 8;	 var->red.length = 8;
		var->green.offset = 4;   var->green.length = 4;
		var->blue.offset = 0;   var->blue.length = 4;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 3 << 20;
		break;
	case PIX_FMT_YVU422PLANAR:
		var->bits_per_pixel = 16;
		var->red.offset = 0;	 var->red.length = 8;
		var->green.offset = 8;   var->green.length = 4;
		var->blue.offset = 12;   var->blue.length = 4;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 3 << 20;
		break;
	case PIX_FMT_YUV420PLANAR:
		var->bits_per_pixel = 12;
		var->red.offset = 4;	 var->red.length = 8;
		var->green.offset = 2;   var->green.length = 2;
		var->blue.offset = 0;   var->blue.length = 2;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 4 << 20;
		break;
	case PIX_FMT_YVU420PLANAR:
		var->bits_per_pixel = 12;
		var->red.offset = 0;	 var->red.length = 8;
		var->green.offset = 8;   var->green.length = 2;
		var->blue.offset = 10;   var->blue.length = 2;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 4 << 20;
		break;
	/* YUV422 Packed will be YUV444 Packed after IRE 90 and 270
		 * degree rotation*/
	case PIX_FMT_YUV422PACK_IRE_90_270:
		var->bits_per_pixel = 32;
		var->red.offset = 16;    var->red.length = 8;
		var->green.offset = 8;   var->green.length = 8;
		var->blue.offset = 0;    var->blue.length = 8;
		var->transp.offset = 0;  var->transp.length = 0;
		var->nonstd &= ~0xff0fffff;
		var->nonstd |= 7 << 20;
		break;
	default:
		return  -EINVAL;
	}

	return 0;
}

void pxa168fb_update_addr(struct pxa168fb_info *fbi,
	struct _sVideoBufferAddr *new_addr, struct _sDualInfo *dualInfo,
	int video_layer)
{
	fbi->new_addr[0] = (unsigned long)new_addr->startAddr[0];
	fbi->new_addr[1] = (unsigned long)new_addr->startAddr[1];
	fbi->new_addr[2] = (unsigned long)new_addr->startAddr[2];

	if (FB_MODE_DUP) {
		struct pxa168fb_info *fbi_dual;
		if (video_layer)             /* video layer */
			fbi_dual = ovly_info.fbi[fb_dual];
		else
			fbi_dual = gfx_info.fbi[fb_dual];

		if (dualInfo->rotate) {
			fbi_dual->new_addr[0] = (unsigned long)
				dualInfo->videoBufferAddr.startAddr[0];
			fbi_dual->new_addr[1] = (unsigned long)
				dualInfo->videoBufferAddr.startAddr[1];
			fbi_dual->new_addr[2] = (unsigned long)
				dualInfo->videoBufferAddr.startAddr[2];
		} else {
			fbi_dual->new_addr[0] = fbi->new_addr[0];
			fbi_dual->new_addr[1] = fbi->new_addr[1];
			fbi_dual->new_addr[2] = fbi->new_addr[2];
		}
	}
}

int check_surface(struct fb_info *fi,
			FBVideoMode new_mode,
			struct _sViewPortInfo *new_info,
			struct _sViewPortOffset *new_offset,
			struct _sVideoBufferAddr *new_addr,
			struct _sDualInfo *dualInfo,
			int video_layer)
{
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)fi->par;
	struct pxa168fb_info *fbi_dual;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	struct fb_var_screeninfo *var = &fi->var;
	int changed = 0, xzoom_old, yzoom_old, xpos_old, ypos_old;
	int change_pos = 0;
	int change_fmt = 0;

	if (video_layer)
		fbi_dual = ovly_info.fbi[fb_dual];
	else
		fbi_dual = gfx_info.fbi[fb_dual];
	dev_dbg(fi->dev, "Enter %s\n", __func__);
	change_pos = change_fmt = fbi->before_rotate = 0;
	xzoom_old = yzoom_old = xpos_old = ypos_old = 0;

	if (fbi->surface.videoMode <= 0)
		fbi->surface.videoMode = -1;
	/*
	 * check mode
	 */
	if (new_mode >= 0 && fbi->surface.videoMode != new_mode) {
		fbi->surface.videoMode = new_mode;
		fbi->pix_fmt = convert_pix_fmt(new_mode);
		set_pix_fmt(var, fbi->pix_fmt);
		if (FB_MODE_DUP) {
			if (dualInfo && dualInfo->rotate)
				fbi_dual->pix_fmt = convert_pix_fmt
					(dualInfo->videoMode);
			else
				fbi_dual->pix_fmt = fbi->pix_fmt;

			set_pix_fmt(&fbi_dual->fb_info->var,
					fbi_dual->pix_fmt);
		}
		changed = 1;
		change_fmt = 1;
	}
	/*
	 * check view port settings.
	 */
	if (new_info && memcmp(&fbi->surface.viewPortInfo, new_info,
			sizeof(struct _sViewPortInfo))) {
		xzoom_old = fbi->surface.viewPortInfo.zoomXSize;
		yzoom_old = fbi->surface.viewPortInfo.zoomYSize;
		if (xzoom_old != new_info->zoomXSize ||
		    yzoom_old != new_info->zoomYSize) {
			change_pos = 1;
			if (fbi->debug == 8)
				pr_debug("%s zoomXSize %d -> %d, zoomYSize %d"
					" -> %d\n", __func__,
					xzoom_old, new_info->zoomXSize,
					yzoom_old, new_info->zoomYSize);
		}
		if (fbi->surface.viewPortInfo.srcWidth != new_info->srcWidth ||
		  fbi->surface.viewPortInfo.srcHeight != new_info->srcHeight ||
		  fbi->surface.viewPortInfo.yPitch != new_info->yPitch ||
		  fbi->surface.viewPortInfo.uPitch != new_info->uPitch ||
		  fbi->surface.viewPortInfo.vPitch != new_info->vPitch ||
		  fbi->surface.viewPortInfo.rotation != new_info->rotation ||
		  fbi->surface.viewPortInfo.yuv_format != new_info->yuv_format)
			change_fmt = 1;
		if (!(new_addr && new_addr->startAddr[0])) {
			if (mi->mmap && (((new_info->srcWidth
					   * new_info->srcHeight
					   * var->bits_per_pixel / 8) * 2)
					> fbi->fb_size)) {
				pr_err("%s: requested memory buffer size %d"
					"exceed the max limit %d!\n", __func__,
					(new_info->srcWidth
					 * new_info->srcHeight
					 * var->bits_per_pixel / 4),
					fbi->fb_size);
				return changed;
			}
		}
		fbi->surface.viewPortInfo = *new_info;
		changed = 1;
	}

	/*
	 * Check offset
	 */
	if (new_offset && memcmp(&fbi->surface.viewPortOffset, new_offset,
		sizeof(struct _sViewPortOffset))) {
		xpos_old = fbi->surface.viewPortOffset.xOffset;
		ypos_old = fbi->surface.viewPortOffset.yOffset;
		change_pos = 1;
		if (fbi->debug == 8)
			pr_info("%s xOffset %d -> %d, yOffset %d -> %d\n",
				__func__, xpos_old, new_offset->xOffset,
				ypos_old, new_offset->yOffset);
		fbi->surface.viewPortOffset.xOffset = new_offset->xOffset;
		fbi->surface.viewPortOffset.yOffset = new_offset->yOffset;
		changed = 1;
	}
	/*
	 * Check buffer address
	 */
	if (new_addr && new_addr->startAddr[0] &&
	    fbi->new_addr[0] != (unsigned long)new_addr->startAddr[0]) {
		pxa168fb_update_addr(fbi, new_addr, dualInfo, video_layer);
		changed = 1;
	}

	if (fb_mode && dualInfo) {
		if (changed || fbi->surface.dualInfo.rotate != dualInfo->rotate
		  || fbi->surface.dualInfo.videoMode != dualInfo->videoMode) {
			fbi->surface.dualInfo = *dualInfo;
			if (fbi->debug == 8)
				pr_info("%s dual(0x%p)->rotate %d, pix_fmt:"
					" pn 0x%x tv 0x%x\n", __func__,
					dualInfo, dualInfo->rotate,
					fbi->pix_fmt, fbi_dual->pix_fmt);
			changed = 1;
		}
	}
	if (new_offset && new_info && !change_fmt && change_pos) {
		if (xpos_old == new_offset->yOffset &&
		    ypos_old == new_offset->xOffset &&
		    xzoom_old == new_info->zoomYSize &&
		    yzoom_old == new_info->zoomXSize) {
			if (ROTATE_IGNORE)
				fbi->before_rotate = 1;
		} else if (fbi->debug == 8) {
			pr_info("old: xpos %d ypos %d xzoom %d yzoom %d\n",
				xpos_old, ypos_old, xzoom_old, yzoom_old);
			pr_info("  -> xpos %d ypos %d xzoom %d yzoom %d\n",
				new_offset->xOffset, new_offset->yOffset,
				new_info->zoomXSize, new_info->zoomYSize);
		}
	}

	return changed;
}

int check_surface_addr(struct fb_info *fi,
			struct _sOvlySurface *surface, int video_layer)
{
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)fi->par;
	struct _sVideoBufferAddr *new_addr = &surface->videoBufferAddr;
	int changed = 0;

	dev_dbg(fi->dev, "Enter %s\n", __func__);

	/* Check buffer address */
	if (new_addr && new_addr->startAddr[0] &&
	    fbi->new_addr[0] != (unsigned long)new_addr->startAddr[0]) {
		pxa168fb_update_addr(fbi, new_addr, &surface->dualInfo,
					video_layer);
		changed = 1;
	}

	return changed;
}

#ifdef OVLY_TASKLET
void pxa168fb_ovly_task(unsigned long data)
{
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)data;
#else
void pxa168fb_ovly_work(struct work_struct *w)
{
	struct pxa168fb_info *fbi = container_of(w,
		struct pxa168fb_info, buf_work);
#endif
	unsigned long flags;

	if (fbi->debug == 1)
		printk(KERN_ERR "%s fbi %d buf_retired %p\n",
			__func__, fbi->id, fbi->buf_retired);

	if (fbi->buf_retired) {
		/* enqueue current to freelist */
		spin_lock_irqsave(&fbi->buf_lock, flags);
		buf_enqueue(fbi->buf_freelist, fbi->buf_retired);
		fbi->buf_retired = 0;
		ovly_info.wait_peer = 0;
		gfx_info.wait_peer = 0;
		spin_unlock_irqrestore(&fbi->buf_lock, flags);
	}
}

void collectFreeBuf(struct pxa168fb_info *fbi,
		u8 *filterList[][3], u8 **freeList)
{
	int i = 0, j = 0;
	struct _sOvlySurface *srf = 0;
	u8 *ptr;

	if (!filterList || !freeList)
		return;

	if (fbi->debug == 1)
		printk(KERN_ERR "%s\n", __func__);
	for (i = 0, j = 0; i < MAX_QUEUE_NUM; i++) {

		ptr = freeList[i];

		/* Check freeList's content. */
		if (ptr) {
			for (; j < MAX_QUEUE_NUM; j++) {
				if (!(filterList[j][0])) {
					/* get surface's ptr. */
					srf = (struct _sOvlySurface *)ptr;

					/* save ptrs which need to be freed.*/
					filterList[j][0] =
					 srf->videoBufferAddr.startAddr[0];
					filterList[j][1] =
					 srf->videoBufferAddr.startAddr[1];
					filterList[j][2] =
					 srf->videoBufferAddr.startAddr[2];
					if (fbi->debug == 1)
						printk(KERN_ERR "%s: buffer \
					%p will be returned\n", __func__,
					srf->videoBufferAddr.startAddr[0]);
					break;
				}
			}

			if (j >= MAX_QUEUE_NUM)
				break;

			kfree(freeList[i]);
			freeList[i] = 0;
		} else {
			/* till content is null. */
			break;
		}
	}
}

void clearFilterBuf(u8 *ppBufList[][3], int iFlag)
{
	/* Check null pointer. */
	if (!ppBufList)
		return;
	if (RESET_BUF & iFlag)
		memset(ppBufList, 0, 3 * MAX_QUEUE_NUM * sizeof(u8 *));
}

void buf_endframe(void *point, int video_layer)
{
	struct fb_info *fi = (struct fb_info *)point;
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)fi->par;
	struct _sOvlySurface *pOvlySurface;
	int ret;
	u8 *buf = buf_gethead(fbi->buf_waitlist);

	if (buf && fbi->buf_retired) {
		if (video_layer)
			ovly_info.retire_err++;
		else
			gfx_info.retire_err++;
	} else {
		buf = buf_dequeue(fbi->buf_waitlist);
		if (buf) {
			pOvlySurface = (struct _sOvlySurface *)buf;

			/* Update new surface settings */
			ret = fbi->update_buff(fi, pOvlySurface, 0);

			if (!ret) {
				fbi->buf_retired = fbi->buf_current;
				fbi->buf_current = buf;
				if (WAIT_PEER && FB_MODE_DUP) {
					if (video_layer)
						ovly_info.wait_peer = 1;
					else
						gfx_info.wait_peer = 1;
				} else {
#ifdef OVLY_TASKLET
					tasklet_schedule(&fbi->ovly_task);
#else
					if (fbi->system_work)
						schedule_work(&fbi->buf_work);
					else
						queue_work(fbi->work_q,
							&fbi->fb_info->queue);
#endif
				}
			} else {
				/* enqueue the repeated buffer to freelist */
				buf_enqueue(fbi->buf_freelist, buf);
				pr_info("Detect a same surface flipped in, "
					"may flicker.\n");
			}
		}
	}
}

int flip_buffer(struct fb_info *info, unsigned long arg,
		int video_layer)
{
	void __user *argp = (void __user *)arg;
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)info->par;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	unsigned long flags;
	struct _sOvlySurface *surface = 0;
	u8 *start_addr[3], *input_data;
	u32 length;

	mutex_lock(&fbi->access_ok);
	surface = kmalloc(sizeof(struct _sOvlySurface),
			GFP_KERNEL);

	/* Get user-mode data. */
	if (copy_from_user(surface, argp,
				sizeof(struct _sOvlySurface))) {
		kfree(surface);
		mutex_unlock(&fbi->access_ok);
		return -EFAULT;
	}
	if (unsupport_format(fbi, surface->viewPortInfo, surface->videoMode,
				video_layer)) {
		kfree(surface);
		mutex_unlock(&fbi->access_ok);
		return -EFAULT;
	}

	update_surface(surface);

	length = surface->videoBufferAddr.length;
	start_addr[0] = surface->videoBufferAddr.startAddr[0];
	input_data = surface->videoBufferAddr.inputData;

	if (fbi->debug == 1)
		printk("flip surface %p buf %p\n", surface, start_addr[0]);

	/*
	 * Has DMA addr?
	 */
	if (start_addr[0] && (!input_data)) {
		spin_lock_irqsave(&fbi->buf_lock, flags);
		/*if there's less than 2 frames in waitlist,
		 *enqueue this frame to waitlist*/
		if ((!fbi->buf_waitlist[0]) ||
			(!fbi->buf_waitlist[1])) {
			buf_enqueue(fbi->buf_waitlist, (u8 *)surface);
		} else {
			/*if there is two frame in waitlist, dequeue
			 *the older frame and enqueue it to freelist,
			 *then enqueue this frame to waitlist*/
			buf_enqueue(fbi->buf_freelist,
					buf_dequeue(fbi->buf_waitlist));
			buf_enqueue(fbi->buf_waitlist, (u8 *)surface);
		}

		if (fbi->update_addr) {
			/* get buffer that will be updated @ next irq */
			surface = (struct _sOvlySurface *)
				buf_gethead(fbi->buf_waitlist);
			if (fbi->update_buff(info, surface, 1))
				if (fbi->debug == 8)
					pr_info("same surface\n");
		}

		spin_unlock_irqrestore(&fbi->buf_lock, flags);
	} else {
		if (!mi->mmap) {
			pr_err("fbi %d(line %d): input err, mmap is not"
				" supported\n", fbi->id, __LINE__);
			kfree(surface);
			mutex_unlock(&fbi->access_ok);
			return -EINVAL;
		}

		/* update buffer */
		fbi->update_buff(info, surface, 1);

		/* copy buffer */
		if (input_data) {
			if (NEED_VSYNC(fbi, video_layer))
				wait_for_vsync(fbi, video_layer);
			/* if support hw DMA, replace this. */
			if (copy_from_user(fbi->fb_start,
						input_data, length)){
				kfree(surface);
				mutex_unlock(&fbi->access_ok);
				return -EFAULT;
			}
			kfree(surface);
			mutex_unlock(&fbi->access_ok);
			return 0;
		}

		/*
		 * if it has its own physical address,
		 * switch to this memory. don't support YUV planar format
		 * with split YUV buffers. but below code seems have no
		 * chancee to execute. - FIXME
		 */
		if (start_addr[0]) {
			if (fbi->mem_status)
				free_pages(
						(unsigned long)fbi->fb_start,
						get_order(fbi->fb_size));
			else
				dma_free_writecombine(fbi->dev,
						fbi->fb_size,
						fbi->fb_start,
						fbi->fb_start_dma);

			fbi->fb_start = __va(start_addr[0]);
			fbi->fb_size = length;
			fbi->fb_start_dma =
				(dma_addr_t)__pa(fbi->fb_start);
			fbi->mem_status = 1;
			info->fix.smem_start = fbi->fb_start_dma;
			info->fix.smem_len = fbi->fb_size;
			info->screen_base = fbi->fb_start;
			info->screen_size = fbi->fb_size;
		}

		kfree(surface);
	}

	mutex_unlock(&fbi->access_ok);

	return 0;

}

static void free_buf(struct pxa168fb_info *fbi)
{
	u8 *buf;

	/* put all buffers into free list */
	do {
		buf = buf_dequeue(fbi->buf_waitlist);
		buf_enqueue(fbi->buf_freelist, buf);
	} while (buf);

	if (fbi->buf_current) {
		buf_enqueue(fbi->buf_freelist, fbi->buf_current);
		fbi->buf_current = 0;
	}
	if (fbi->buf_retired) {
		buf_enqueue(fbi->buf_freelist, fbi->buf_retired);
		fbi->buf_retired = 0;
	}

	/* clear some globals */
	ovly_info.wait_peer = 0;
	memset(&fbi->surface, 0, sizeof(struct _sOvlySurface));
	fbi->new_addr[0] = 0; fbi->new_addr[1] = 0; fbi->new_addr[2] = 0;
	if (FB_MODE_DUP) {
		memset(&ovly_info.fbi[fb_dual]->surface, 0,
			sizeof(struct _sOvlySurface));
		ovly_info.fbi[fb_dual]->new_addr[0] = 0;
		ovly_info.fbi[fb_dual]->new_addr[1] = 0;
		ovly_info.fbi[fb_dual]->new_addr[2] = 0;
	}
}

int get_freelist(struct fb_info *info, unsigned long arg,
		int video_layer)
{
	void __user *argp = (void __user *)arg;
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)info->par;
	unsigned long flags;

	if (fbi->debug == 1)
		printk(KERN_ERR "get freelist\n");

	spin_lock_irqsave(&fbi->buf_lock, flags);

	/* when lcd is suspend or dma off, free all buffers */
	if (!(gfx_info.fbi[fbi->id]->active) || !fbi->dma_on)
		free_buf(fbi);

	/* Collect expired frame to list */
	collectFreeBuf(fbi, fbi->filterBufList, fbi->buf_freelist);
	if (copy_to_user(argp, fbi->filterBufList,
				3*MAX_QUEUE_NUM*sizeof(u8 *))) {
		spin_unlock_irqrestore(&fbi->buf_lock, flags);
		return -EFAULT;
	}
	clearFilterBuf(fbi->filterBufList, RESET_BUF);
	spin_unlock_irqrestore(&fbi->buf_lock, flags);

	if (fbi->debug == 1)
		printk(KERN_ERR "get freelist end\n");

	return 0;
}

/* update xpos/ypos/xzoom/yzoom for dual path in mirror mode */
void dual_pos_zoom(struct pxa168fb_info *fbi,
	struct _sOvlySurface *surface,
	int *xzoom, int *yzoom, int *xpos, int *ypos)
{
	int xzoom_t = *xzoom, yzoom_t = *yzoom;
	int xpos_t = *xpos, ypos_t = *ypos, xres_t, yres_t;

	if (atomic_read(&ovly_info.fbi[fb_base]->op_count))
		pxa168fb_rotate_dual(surface->dualInfo.rotate);

	/* get real window size */
	xres_t = gfx_info.xres_z - (gfx_info.left_z << 1);
	yres_t = gfx_info.yres_z - (gfx_info.top_z << 1);

	if (fbi->before_rotate) {
		if (fbi->debug == 8)
			pr_info("\nbefore rotate swap xpos_t/ypos_t,"
				" xzoom_t/yzoom_t.......\n");
		swap(xpos_t, ypos_t); swap(xzoom_t, yzoom_t);
	}

	if (fbi->debug == 8)
		pr_info("\n%s dual rotate %d, gfx_info:\n"
			"\tx %4d x_z %4d - left_z %4d xres_t %4d\n"
			"\ty %4d y_z %4d - top_z  %4d yres_t %4d\n"
			"     xpos %4d ypos %4d xzoom %4d yzoom %4d\n"
			"       _t %4d   _t %4d    _t %4d    _t %4d\n",
			__func__, surface->dualInfo.rotate,
			gfx_info.xres, gfx_info.xres_z,
			gfx_info.left_z, xres_t, gfx_info.yres,
			gfx_info.yres_z, gfx_info.top_z, yres_t,
			*xpos, *ypos, *xzoom, *yzoom,
			xpos_t, ypos_t, xzoom_t, yzoom_t);

        if (((gfx_info.xres != surface->viewPortInfo.zoomXSize) &&
		(gfx_info.yres != surface->viewPortInfo.zoomYSize))
		|| (surface->dualInfo.rotate == 1) ) {
		/* keep ratio */
		if (surface->dualInfo.rotate == 1) {
			*xzoom = yzoom_t * xres_t / gfx_info.yres;
			*yzoom = xzoom_t * yres_t / gfx_info.xres;
			*xpos = gfx_info.left_z + (gfx_info.yres -
				yzoom_t - surface->viewPortOffset.
				yOffset) * xres_t / gfx_info.yres;
			*ypos = gfx_info.top_z + xpos_t * yres_t
				/ gfx_info.xres;
		} else {
			*xzoom = xzoom_t * xres_t / gfx_info.xres;
			*yzoom = yzoom_t * yres_t / gfx_info.yres;
			*xpos = gfx_info.left_z + xres_t *
				xpos_t / gfx_info.xres;
			*ypos = gfx_info.top_z + yres_t * ypos_t
				/ gfx_info.yres;
		}
        } else {
		/* ok, we need scale to fit tv screen */
		if (surface->viewPortInfo.zoomXSize * gfx_info.yres_z >
		   surface->viewPortInfo.zoomYSize * gfx_info.xres_z) {
			/* scale to fit width */
			*xzoom = gfx_info.xres_z;
			*yzoom = *xzoom * surface->viewPortInfo.zoomYSize
				/ surface->viewPortInfo.zoomXSize;

		} else {
			/* scale to fix height */
			*yzoom = gfx_info.yres_z;
			*xzoom = *yzoom * surface->viewPortInfo.zoomXSize
				/ surface->viewPortInfo.zoomYSize;
		}
                /* put to center */
                *ypos = (gfx_info.yres_z - *yzoom) / 2;
                *xpos = (gfx_info.xres_z - *xzoom) / 2;
        }
	if (fbi->debug == 8)
		pr_info("\t to xpos %4d ypos %4d xzoom %4d yzoom"
			" %4d\n", *xpos, *ypos, *xzoom, *yzoom);
}

void set_dma_active(struct pxa168fb_info *fbi, int video_layer)
{
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	u32 flag = video_layer ? CFG_DMA_ENA_MASK : CFG_GRA_ENA_MASK;
	u32 enable = video_layer ? CFG_DMA_ENA(1) : CFG_GRA_ENA(1);
	u32 active = 0;

	if (fbi->new_addr[0] || mi->mmap
			|| (!video_layer && fb_mode && fbi->id == fb_dual))
		active = fbi->check_modex_active(fbi->id, fbi->active
					&& fbi->dma_on);

	dma_ctrl_set(fbi->id, 0, flag, active ? enable : 0);

	pr_debug("%s fbi %d: active %d fbi->active %d, new_addr %lu\n",
		__func__, fbi->id, active, fbi->active, fbi->new_addr[0]);
}

int dispd_dma_enabled(struct pxa168fb_info *fbi, int video_layer)
{
	if (irqs_disabled() || !fbi->active)
		return 0;

	/* check whether display done irq enabled */
	if (!(readl(fbi->reg_base + SPU_IRQ_ENA) &
		display_done_imask(fbi->id)))
		return 0;

	/* check whether path clock is disabled */
	if (readl(fbi->reg_base + clk_div(fbi->id)) & (SCLK_DISABLE))
		return 0;

	/* in modex dma may not be enabled */
	return dma_ctrl_read(fbi->id, 0) & (video_layer ?
		CFG_DMA_ENA_MASK : CFG_GRA_ENA_MASK);
}

void clear_dispd_irq(struct pxa168fb_info *fbi)
{
	int isr = readl(fbi->reg_base + SPU_IRQ_ISR);

	if ((isr & display_done_imask(fbi->id))) {
		irq_status_clear(fbi->id, display_done_imask(fbi->id));
		printk(KERN_ERR"fbi %d irq miss, clear isr %x\n", fbi->id, isr);
	}
}

void wait_for_vsync(struct pxa168fb_info *fbi, int video_layer)
{
	struct pxa168fb_info *fbi_dual =
		video_layer ? ovly_info.fbi[fb_dual] : gfx_info.fbi[fb_dual];

again:
	atomic_set(&fbi->w_intr, 0);
	pr_debug("fbi->id %d layer: %d\n", fbi->id, video_layer);

	wait_event_interruptible_timeout(fbi->w_intr_wq,
		atomic_read(&fbi->w_intr), HZ/20);

	/* handle timeout case, to w/a irq miss */
	if (atomic_read(&fbi->w_intr) == 0)
		clear_dispd_irq(fbi);

	if (FB_MODE_DUP && NEED_VSYNC(fbi_dual, video_layer)) {
		fbi = fbi_dual;
		goto again;
	}
}
