/*
 * linux/drivers/video/pxa168fb.c -- Marvell PXA168 LCD Controller
 *
 *  Copyright (C) 2008 Marvell International Ltd.
 *  All rights reserved.
 *
 *  2009-02-16  adapted from original version for PXA168
 *		Green Wan <gwan@marvell.com>
 *              Jun Nie <njun@marvell.com>
 *		Kevin Liu <kliu5@marvell.com>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/interrupt.h>
#include <linux/console.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/cpufreq.h>
#include <linux/dma-mapping.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/fb.h>
#include <linux/uaccess.h>
#include <linux/proc_fs.h>
#include <linux/platform_device.h>
#include "pxa168fb_common.h"

#include <mach/io.h>
#include <mach/irqs.h>
#include <mach/gpio.h>

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif

#define DEBUG_VSYNC_PATH(id)	(gfx_info.fbi[(id)]->debug & 3)
#define DEBUG_ERR_IRQ(id)	(gfx_info.fbi[(id)]->debug & 4)
#define DEBUG_TV_ACTIVE(id)	(gfx_info.fbi[(id)]->debug & 8)
static int gfx_udflow_count;
static int vid_udflow_count;
static int axi_err_count;
static int vsync_check;
static int irq_count;
static int vsync_count;
static int dispd_count;
static int f0_count;
static int f1_count;
static int vf0_count;
static int vf1_count;
static struct timer_list vsync_timer;

#ifdef CONFIG_DVFM
#include <mach/dvfm.h>
#include <mach/mmp2_dvfm.h>

static int dvfm_dev_idx;
static void set_dvfm_constraint(void)
{
	/* Disable Lowpower mode */
	dvfm_disable_op_name("apps_idle", dvfm_dev_idx);
	dvfm_disable_op_name("apps_sleep", dvfm_dev_idx);
	dvfm_disable_op_name("chip_sleep", dvfm_dev_idx);
	dvfm_disable_op_name("sys_sleep", dvfm_dev_idx);
}

static void unset_dvfm_constraint(void)
{
	/* Enable Lowpower mode */
	dvfm_enable_op_name("apps_idle", dvfm_dev_idx);
	dvfm_enable_op_name("apps_sleep", dvfm_dev_idx);
	dvfm_enable_op_name("chip_sleep", dvfm_dev_idx);
	dvfm_enable_op_name("sys_sleep", dvfm_dev_idx);
}

#else
static void set_dvfm_constraint(void) {}
static void unset_dvfm_constraint(void) {}
#endif

#define DEFAULT_REFRESH		60	/* Hz */

/* Compatibility mode global switch .....
 *
 * This is a secret switch for user space programs that may want to
 * select color spaces and set resolutions the same as legacy PXA display
 * drivers. The switch is set and unset by setting a specific value in the
 * var_screeninfo.nonstd variable.
 *
 * To turn on compatibility with older PXA, set the MSB of nonstd to 0xAA.
 * To turn off compatibility with older PXA, set the MSB of nonstd to 0x55.
 */

static unsigned int COMPAT_MODE;
static unsigned int frambuffer_base = 0;
static unsigned int max_fb_size = 0;
static unsigned int fb_size_from_cmd = 0;

/* LCD mode switch
 * mode0: panel + 1080p TV path, seperately
 *
 * clone mode:
 * mode1: panel path + panel path graphics/video layer data resized to TV path
 * mode2: mode1 with panel path disalbed
 * mode3: mode1 with TV path disalbed
 *
 * fb_share mode:
 * TV path graphics layer share same frame buffer with panel path
 */
int fb_mode = 0;
int fb_share = 0;

/* VDMA switch mode
 * vdma_switch = 1
 *   auto switch
 *     for mode0 panel1 gfx vdma enable, tv gfx vdma disable
 *     for clone mode panel1 gfx vdma disable,
 *        tv gfx vdma enable(except for tv interlace mode)
 * vdma_switch = 0
 *   vdma switch manually
 *     for gfx and ovly of panel & tv,
 *     except for tv interlace mode
 */
int vdma_switch = 0;
struct fbi_info gfx_info;

struct lcd_regs *get_regs(int id)
{
	struct pxa168fb_info *fbi = gfx_info.fbi[0];
	struct lcd_regs *regs;

	if (!fbi)
		return NULL;

	regs = (struct lcd_regs *)((unsigned)fbi->reg_base);

	if (id == 0)
		regs = (struct lcd_regs *)((unsigned)fbi->reg_base + 0xc0);
	if (id == 2)
		regs = (struct lcd_regs *)((unsigned)fbi->reg_base + 0x200);

	return regs;
}
u32 dma_ctrl_read(int id, int ctrl1)
{
	struct pxa168fb_info *fbi = gfx_info.fbi[0];
	u32 reg = (u32)fbi->reg_base + dma_ctrl(ctrl1, id);

	return __raw_readl(reg);
}

void dma_ctrl_write(int id, int ctrl1, u32 value)
{
	struct pxa168fb_info *fbi = gfx_info.fbi[0];
	u32 reg = (u32)fbi->reg_base + dma_ctrl(ctrl1, id);

	__raw_writel(value, reg);
}

void dma_ctrl_set(int id, int ctrl1, u32 mask, u32 value)
{
	struct pxa168fb_info *fbi = gfx_info.fbi[0];
	u32 reg = (u32)fbi->reg_base + dma_ctrl(ctrl1, id);
	u32 tmp1, tmp2;

	tmp1 = tmp2 = __raw_readl(reg); tmp2 &= ~mask; tmp2 |= value;
	if (tmp1 != tmp2) __raw_writel(tmp2, reg);
}

void irq_mask_set(int id, u32 mask, u32 val)
{
	struct pxa168fb_info *fbi = gfx_info.fbi[0];
	u32 temp = readl(fbi->reg_base + SPU_IRQ_ENA);

	temp &= ~mask; temp |= val;
	writel(temp, fbi->reg_base + SPU_IRQ_ENA);
}

void irq_status_clear(int id, u32 mask)
{
	struct pxa168fb_info *fbi = gfx_info.fbi[0];
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;

	writel(mi->isr_clear_mask & (~mask), fbi->reg_base + SPU_IRQ_ISR);
}

int sclk_div_get(int id)
{
	struct pxa168fb_info *fbi = gfx_info.fbi[0];
	u32 val = readl(fbi->reg_base + clk_div(id));

	pr_debug("%s SCLK 0x%x\n", __func__, val);
	return val & 0xff;
}

void sclk_div_set(int id, int divider)
{
	struct pxa168fb_info *fbi = gfx_info.fbi[0];
	u32 val = readl(fbi->reg_base + clk_div(id));

	val &= ~0xff; val |= divider;
	writel(val, fbi->reg_base + clk_div(fbi->id));

	pr_debug("%s divider %x SCLK 0x%x\n", __func__, divider,
		readl(fbi->reg_base + clk_div(id)));
}

void pxa168fb_spi_send(struct pxa168fb_info *fbi, void *value,
		int count, unsigned int spi_gpio_cs)
{
	u32 x, spi_byte_len;
	u8 *cmd = (u8 *)value;
	int i, err, isr, iopad;

	if (spi_gpio_cs != -1) {
		err = gpio_request(spi_gpio_cs, "LCD_SPI_CS");
		if (err) {
			pr_err("failed to request GPIO for LCD CS\n");
			return;
		}
		gpio_direction_output(spi_gpio_cs, 1);
	}
	/* get spi data size */
	spi_byte_len = readl(fbi->reg_base + LCD_SPU_SPI_CTRL);
	spi_byte_len = (spi_byte_len >> 8) & 0xff;
	/* It should be (spi_byte_len + 7) >> 3, but spi controller
	 * request set one less than bit length */
	spi_byte_len = (spi_byte_len + 8) >> 3;
	/* spi command provided by platform should be 1, 2, or 4 byte aligned */
	if(spi_byte_len == 3)
		spi_byte_len = 4;

	iopad = readl(fbi->reg_base + SPU_IOPAD_CONTROL);
	for (i = 0; i < count; i++) {
		if ((iopad & CFG_IOPADMODE_MASK) != PIN_MODE_DUMB_18_SPI)
			writel(PIN_MODE_DUMB_18_SPI | (iopad & ~CFG_IOPADMODE_MASK),
				fbi->reg_base + SPU_IOPAD_CONTROL);
		if (spi_gpio_cs != -1)
			gpio_set_value(spi_gpio_cs, 0);
		msleep(2);

		irq_status_clear(fbi->id, SPI_IRQ_MASK);

		switch (spi_byte_len){
		case 1:
			writel(*cmd, fbi->reg_base + LCD_SPU_SPI_TXDATA);
			break;
		case 2:
			writel(*(u16*)cmd, fbi->reg_base + LCD_SPU_SPI_TXDATA);
			break;
		case 4:
			writel(*(u32*)cmd, fbi->reg_base + LCD_SPU_SPI_TXDATA);
			break;
		default:
			pr_err("Wrong spi bit length\n");
		}
		cmd += spi_byte_len;
		x = readl(fbi->reg_base + LCD_SPU_SPI_CTRL);
		x |= 0x1;
		writel(x, fbi->reg_base + LCD_SPU_SPI_CTRL);
		isr = readl(fbi->reg_base + SPU_IRQ_ISR);
		while(!(isr & SPI_IRQ_ENA_MASK)) {
			udelay(1);
			isr = readl(fbi->reg_base + SPU_IRQ_ISR);
		}
		irq_status_clear(fbi->id, SPI_IRQ_MASK);
		x = readl(fbi->reg_base + LCD_SPU_SPI_CTRL);
		x &= ~0x1;
		writel(x, fbi->reg_base + LCD_SPU_SPI_CTRL);
		if (spi_gpio_cs != -1)
			gpio_set_value(spi_gpio_cs, 1);
		if ((iopad & CFG_IOPADMODE_MASK) != PIN_MODE_DUMB_18_SPI)
			writel(iopad, fbi->reg_base + SPU_IOPAD_CONTROL);
	}
	if (spi_gpio_cs != -1)
		gpio_free(spi_gpio_cs);
}

static void pxa168fb_power(struct pxa168fb_info *fbi,
		struct pxa168fb_mach_info *mi, int on)
{
	pr_debug( "fbi->active %d on %d\n", fbi->active, on);
	if ((mi->spi_ctrl != -1) && (mi->spi_ctrl & CFG_SPI_ENA_MASK))
		writel(mi->spi_ctrl, fbi->reg_base + LCD_SPU_SPI_CTRL);

	if ((mi->pxa168fb_lcd_power) && ((fbi->active && !on ) || (!fbi->active && on)))
		mi->pxa168fb_lcd_power(fbi, mi->spi_gpio_cs, mi->spi_gpio_reset, on);
}

/*************************************************************************/

static int determine_best_pix_fmt(struct fb_var_screeninfo *var)
{
	unsigned char pxa_format;

	/* compatibility switch: if var->nonstd MSB is 0xAA then skip to
	 * using the nonstd variable to select the color space.
	 */
	if(COMPAT_MODE != 0x2625) {

		/*
		 * Pseudocolor mode?
		 */
		if (var->bits_per_pixel == 8)
			return PIX_FMT_PSEUDOCOLOR;

		/*
		 * Check for YUV422PACK.
		 */
		if (var->bits_per_pixel == 16 && var->red.length == 16 &&
		    var->green.length == 16 && var->blue.length == 16) {
			if (var->red.offset >= var->blue.offset) {
			if (var->red.offset == 4)
				return PIX_FMT_YUV422PACK;
			else
				return PIX_FMT_YUYV422PACK;
			} else
				return PIX_FMT_YVU422PACK;
		}

		/*
		 * Check for 565/1555.
		 */
		if (var->bits_per_pixel == 16 && var->red.length <= 5 &&
		    var->green.length <= 6 && var->blue.length <= 5) {
			if (var->transp.length == 0) {
				if (var->red.offset >= var->blue.offset)
					return PIX_FMT_RGB565;
				else
					return PIX_FMT_BGR565;
			}

			if (var->transp.length == 1 && var->green.length <= 5) {
				if (var->red.offset >= var->blue.offset)
					return PIX_FMT_RGB1555;
				else
					return PIX_FMT_BGR1555;
			}

			/* fall through */
		}

		/*
		 * Check for 888/A888.
		 */
		if (var->bits_per_pixel <= 32 && var->red.length <= 8 &&
		    var->green.length <= 8 && var->blue.length <= 8) {
			if (var->bits_per_pixel == 24 && var->transp.length == 0) {
				if (var->red.offset >= var->blue.offset)
					return PIX_FMT_RGB888PACK;
				else
					return PIX_FMT_BGR888PACK;
			}

			if (var->bits_per_pixel == 32 && var->transp.offset == 24) {
				if (var->red.offset >= var->blue.offset)
					return PIX_FMT_RGBA888;
				else
					return PIX_FMT_BGRA888;
			} else {
				if (var->red.offset >= var->blue.offset)
					return PIX_FMT_RGB888UNPACK;
				else
					return PIX_FMT_BGR888UNPACK;
			}
			/* fall through */
		}
	} else {

		pxa_format = (var->nonstd >> 20) & 0xf;

		switch (pxa_format) {
		case 0:
			return PIX_FMT_RGB565;
			break;
		case 5:
			return PIX_FMT_RGB1555;
			break;
		case 6:
			return PIX_FMT_RGB888PACK;
			break;
		case 7:
			return PIX_FMT_RGB888UNPACK;
			break;
		case 8:
			return PIX_FMT_RGBA888;
			break;
		case 9:
			return PIX_FMT_YUV422PACK;
			break;

		default:
			return -EINVAL;
		}
	}

	return -EINVAL;
}

static void set_mode(struct pxa168fb_info *fbi, struct fb_var_screeninfo *var,
		     const struct fb_videomode *mode, int pix_fmt, int ystretch)
{
	dev_dbg(fbi->fb_info->dev, "Enter %s\n", __FUNCTION__);
	set_pix_fmt(var, pix_fmt);

	var->xres = mode->xres;
	var->yres = mode->yres;
	var->xres_virtual = max(var->xres, var->xres_virtual);
	if (ystretch && !fb_share)
		var->yres_virtual = var->yres *2;
	else
		var->yres_virtual = max(var->yres, var->yres_virtual);
	var->grayscale = 0;
	var->accel_flags = FB_ACCEL_NONE;
	var->pixclock = mode->pixclock;
	var->left_margin = mode->left_margin;
	var->right_margin = mode->right_margin;
	var->upper_margin = mode->upper_margin;
	var->lower_margin = mode->lower_margin;
	var->hsync_len = mode->hsync_len;
	var->vsync_len = mode->vsync_len;
	var->sync = mode->sync;
	var->vmode = FB_VMODE_NONINTERLACED;
	var->rotate = FB_ROTATE_UR;
}

static int pxa168fb_check_var(struct fb_var_screeninfo *var,
			      struct fb_info *info)
{
	struct pxa168fb_info *fbi = info->par;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;

	dev_dbg(info->dev, "Enter %s\n", __FUNCTION__);

	if (var->bits_per_pixel == 8) {
		pr_debug("%s var->bits_per_pixel == 8\n", __func__);
		return -EINVAL;
	}

	/* compatibility mode: if the MSB of var->nonstd is 0xAA then
	 * set xres_virtual and yres_virtual to xres and yres. */
	if((var->nonstd >> 24) == 0xAA)
		COMPAT_MODE = 0x2625;

	if((var->nonstd >> 24) == 0x55)
		COMPAT_MODE = 0x0;

	/* Basic geometry sanity checks */
	if (var->xoffset + var->xres > var->xres_virtual) {
		pr_debug("%s var->xoffset(%d) + var->xres(%d) >"
			" var->xres_virtual(%d)\n", __func__,
			var->xoffset, var->xres, var->xres_virtual);
		return -EINVAL;
	}
	if (var->yoffset + var->yres > var->yres_virtual) {
		pr_debug("%s var->yoffset(%d) + var->yres(%d) >"
			" var->yres_virtual(%d)\n", __func__,
			var->yoffset, var->yres, var->yres_virtual);
		return -EINVAL;
	}
	if (var->xres + var->right_margin +
	    var->hsync_len + var->left_margin > 3500) {
		pr_debug("%s var->xres(%d) + var->right_margin(%d) + "
			"var->hsync_len(%d) + var->left_margin(%d) "
			"> 2500\n", __func__, var->xres, var->right_margin,
			var->hsync_len, var->left_margin);
		return -EINVAL;
	}
	if (var->yres + var->lower_margin +
	    var->vsync_len + var->upper_margin > 2500) {
		pr_debug("%s var->yres(%d) + var->lower_margin(%d) + "
			"var->vsync_len(%d) + var->upper_margin(%d) "
			"> 2500\n", __func__, var->yres, var->lower_margin,
			var->vsync_len, var->upper_margin);
		return -EINVAL;
	}

	/* Check size of framebuffer */
	if (mi->mmap && (var->xres_virtual * var->yres_virtual *
	    (var->bits_per_pixel >> 3) > fbi->fb_size)) {
		pr_debug("%s var->xres_virtual(%d) * var->yres_virtual(%d) "
			"* (var->bits_per_pixel(%d) >> 3) > max_fb_size(%d)\n",
			__func__, var->xres_virtual, var->yres_virtual,
			var->bits_per_pixel, fbi->fb_size);
		return -EINVAL;
	}

	return 0;
}

/*
 * The hardware clock divider has an integer and a fractional
 * stage:
 *
 *	clk2 = clk_in / integer_divider
 *	clk_out = clk2 * (1 - (fractional_divider >> 12))
 *
 * Calculate integer and fractional divider for given clk_in
 * and clk_out.
 */
static void set_clock_divider(struct pxa168fb_info *fbi)
{
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	struct fb_var_screeninfo *var = &fbi->fb_info->var;
	int divider_int;
	int needed_pixclk;
	u64 div_result;
	u32 div_temp, x = 0;


	/* check whether divider is fixed by platform */
	if (mi->sclk_div){
		div_temp = mi->sclk_div;
		/*for 480i and 576i, pixel clock should be half of the spec value because of
		 * pixel repetition*/
		if ((var->yres == 480 || var->yres == 576) &&
			(var->vmode == FB_VMODE_INTERLACED)) {
			div_temp &= ~0xf;
			div_temp |= 0xa;
		}
		writel(div_temp, fbi->reg_base + clk_div(fbi->id));
		if (!var->pixclock) {
			divider_int = mi->sclk_div & CLK_INT_DIV_MASK;
			if (!divider_int) {
				pr_err("%s: divider_int null\n", __func__);
				return;
			}

			if (!clk_get_rate(fbi->clk)) {
				pr_err("%s: fbi->clk get rate null\n",
						__func__);
				return;
			}

			x = clk_get_rate(fbi->clk) / divider_int / 1000;
			var->pixclock = 1000000000 / x;
			pr_debug("%s pixclock %d x %d divider_int %d\n",
				__func__, var->pixclock, x, divider_int);
		}
		return;
	}

	/* Notice: The field pixclock is used by linux fb
	 * is in pixel second. E.g. struct fb_videomode &
	 * struct fb_var_screeninfo
	 */

	/* Check input values */
	dev_dbg(fbi->fb_info->dev, "Enter %s\n", __FUNCTION__);
	if (!var->pixclock) {
		pr_err("Input refresh or pixclock is wrong.\n");
		return;
	}

	/* Using PLL/AXI clock. */
	x = 0x80000000;

	/* Calc divider according to refresh rate */
	div_result = 1000000000000ll;
	do_div(div_result, var->pixclock);
	needed_pixclk = (u32)div_result;

	divider_int = clk_get_rate(fbi->clk) / needed_pixclk;

	/* check whether divisor is too small. */
	if (divider_int < 2) {
		pr_warning("Warning: clock source is too slow."
				 "Try smaller resolution\n");
		divider_int = 2;
	}

	/* Set setting to reg */
	x |= divider_int;
	if (fbi->id != 1)
		writel(x, fbi->reg_base + clk_div(fbi->id));
}

static u32 dma_ctrl0_update(int active, struct pxa168fb_mach_info *mi, u32 x, u32 pix_fmt)
{
	if (active)
		x |= 0x00000100;
	else
		x &= ~0x00000100;

	pr_debug("\t\t%s active %d value 0x%x, active bit %x\n",
		__func__, active, x, x&(0x00000100));

	/* If we are in a pseudo-color mode, we need to enable
	 * palette lookup  */
	if (pix_fmt == PIX_FMT_PSEUDOCOLOR)
		x |= 0x10000000;

	/* Configure hardware pixel format */
	x &= ~(0xF << 16);
	x |= (pix_fmt >> 1) << 16;

	/* Check YUV422PACK */
	x &= ~((1 << 9) | (1 << 11) | (1 << 10) | (1 << 12));
	if (((pix_fmt >> 1) == 5) || (pix_fmt & 0x1000)) {
		x |= 1 << 9;
		x |= (mi->panel_rbswap) << 12;
		if (pix_fmt == 11)
			x |= 1 << 11;
		if (pix_fmt & 0x1000)
			x |= 1 << 10;
	} else {
		/* Check red and blue pixel swap.
		 * 1. source data swap. BGR[M:L] rather than RGB[M:L] is stored in memeory as source format.
		 * 2. panel output data swap
		 */
		x |= (((pix_fmt & 1) ^ 1) ^ (mi->panel_rbswap)) << 12;
	}
	/* enable horizontal smooth filter for both graphic and video layers */
	x |= CFG_GRA_HSMOOTH(1) | CFG_DMA_HSMOOTH(1);

	return x;
}

static int check_modex_active(int id, int active)
{
	if (active)
		active = gfx_info.fbi[id]->dma_on;

	switch (fb_mode) {
	case 3:
		if (id == fb_dual) {
			if (gfx_info.fbi[fb_dual])
				/* for debug purpose */
				active = DEBUG_TV_ACTIVE(fb_dual);
			else
				/* before gfx_info inited */
				active = 0;
		}
		break;
	case 2:
		if (id == fb_base)
			active = 0;
		break;
	default:
		break;
	}

	return active;
}

void enable_graphic_layer(int id)
{
	int val;

	val = check_modex_active(id, gfx_info.fbi[id]->active);
	if (!(dma_ctrl_read(id, 0) & CFG_GRA_ENA_MASK)) {
		dma_ctrl_set(id, 0, CFG_GRA_ENA_MASK, CFG_GRA_ENA(val));
	}
}

static void set_dma_control0(struct pxa168fb_info *fbi)
{
	struct pxa168fb_mach_info *mi;
	u32 x = 0, active, pix_fmt = fbi->pix_fmt;

	dev_dbg(fbi->fb_info->dev,"Enter %s\n", __FUNCTION__);
again:
	/* enable multiple burst request in DMA AXI bus arbiter for faster read */
	if (fbi->id != 1)
		dma_ctrl_set(fbi->id, 0, CFG_ARBFAST_ENA(1), CFG_ARBFAST_ENA(1));

	mi = fbi->dev->platform_data;
	x = dma_ctrl_read(fbi->id, 0);

	active = check_modex_active(fbi->id, fbi->active);

	if (fb_mode && (fbi->id == fb_dual))
		pix_fmt = gfx_info.fbi[fb_base]->pix_fmt;
	x = dma_ctrl0_update(active, mi, x, pix_fmt);
	dma_ctrl_write(fbi->id, 0, x);
	pxa688fb_vsmooth_set(fbi->id, 0, gfx_vsmooth, 0);

	if (FB_MODE_DUP) {
		gfx_info.fbi[fb_dual]->pix_fmt = fbi->pix_fmt;
		fbi = gfx_info.fbi[fb_dual];
		mi = fbi->dev->platform_data;
		goto again;
	}
}

static void pxa168fb_vdma_config(struct pxa168fb_info *fbi)
{
#ifdef CONFIG_PXA688_VDMA
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;

	pr_debug("%s fbi %d vdma_enable %d active %d\n",
		__func__, fbi->id, fbi->vdma_enable, fbi->active);
	if (fbi->vdma_enable && fbi->active) {
		int active = check_modex_active(fbi->id, fbi->active);
		if (active) {
			mi->vdma_lines = pxa688fb_vdma_get_linenum(fbi, 0, 0);
			pxa688fb_vdma_set(fbi, mi->sram_paddr, mi->vdma_lines,
				0, fbi->pix_fmt, 0, fbi->pix_fmt);
		} else {
			u32 val = vdma_ctrl_read(fbi) & 1;
			if (val) {
				pr_debug("%s fbi %d val %x\n",
					__func__, fbi->id, val);
				pxa688fb_vdma_release(fbi);
			}
		}
	}
#endif
}

static void pxa168fb_misc_update(struct pxa168fb_info *fbi)
{
	pxa168fb_vdma_config(fbi);
	pxa688fb_partdisp_update(fbi->id);
	pxa688fb_vsmooth_set(fbi->id, 0, gfx_vsmooth, 1);
}

static void set_graphics_start(struct fb_info *info,
	int xoffset, int yoffset, int wait_vsync)
{
	struct pxa168fb_info *fbi = info->par;
	struct fb_var_screeninfo *var = &info->var;
	int pixel_offset;
	unsigned long addr;
	static int debugcount = 0;
	struct lcd_regs *regs;

	if (debugcount < 10)
		debugcount++;

	if (debugcount < 9)
		dev_dbg(info->dev, "Enter %s\n", __FUNCTION__);

	pixel_offset = (yoffset * var->xres_virtual) + xoffset;

	if (fbi->new_addr[0])
		addr = fbi->new_addr[0];
	else
		addr = fbi->fb_start_dma + (pixel_offset *
				(var->bits_per_pixel >> 3));

	if (fb_mode && (fbi->id == fb_dual)) {
		if (gfx_info.rotate_addr)
			addr = gfx_info.rotate_addr;
		else
			addr = readl(&get_regs(fb_base)->g_0);
	}

	regs = get_regs(fbi->id);
	writel(addr, &regs->g_0);
	set_dma_active(fbi, 0);

	if (wait_vsync)
		fbi->misc_update = 1;
	else
		pxa168fb_misc_update(fbi);

	if (FB_MODE_DUP) {
		regs = get_regs(fb_dual);
		if (gfx_info.rotate_addr)
			addr = gfx_info.rotate_addr;
		writel(addr, &regs->g_0);
		set_dma_active(gfx_info.fbi[fb_dual], 0);

		if (wait_vsync)
			gfx_info.fbi[fb_dual]->misc_update = 1;
		else
			pxa168fb_misc_update(gfx_info.fbi[fb_dual]);
	}

	/* return until the address take effect after vsync occurs */
	if (wait_vsync && NEED_VSYNC(fbi, 0))
		wait_for_vsync(fbi, 0);
}

static void set_dumb_panel_control(struct fb_info *info)
{
	struct pxa168fb_info *fbi = info->par;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	u32 x;

	dev_dbg(info->dev, "Enter %s\n", __FUNCTION__);

	/* Preserve enable flag */
	x = readl(fbi->reg_base + intf_ctrl(fbi->id)) & 0x00000001;
	x |= (fbi->is_blanked ? 0x7 : mi->dumb_mode) << 28;
	if (fbi->id == 1) {
		/* enable AXI urgent flag */
		x |= 0xff << 16;
	} else {
		x |= mi->gpio_output_data << 20;
		x |= mi->gpio_output_mask << 12;
	}
	x |= mi->panel_rgb_reverse_lanes ? 0x00000080 : 0;
	x |= mi->invert_composite_blank ? 0x00000040 : 0;
	x |= (info->var.sync & FB_SYNC_COMP_HIGH_ACT) ? 0x00000020 : 0;
	x |= mi->invert_pix_val_ena ? 0x00000010 : 0;
	x |= (info->var.sync & FB_SYNC_VERT_HIGH_ACT) ? 0x00000008 : 0;
	x |= (info->var.sync & FB_SYNC_HOR_HIGH_ACT) ? 0x00000004 : 0;
	x |= mi->invert_pixclock ? 0x00000002 : 0;
	writel(x, fbi->reg_base + intf_ctrl(fbi->id));	/* FIXME */
}

static void set_tv_interlace(void)
{
	struct pxa168fb_info *fbi = gfx_info.fbi[1];
	struct pxa168fb_info *fbi_ovly = ovly_info.fbi[1];
	struct fb_info *info = fbi->fb_info;
	struct fb_var_screeninfo *v = &info->var;
	struct lcd_regs *regs = get_regs(fbi->id);
	int x, y, yres, interlaced = 0, vsync_ctrl;
	static int gfx_vdma, ovly_vdma;
	u32 val;

	dev_dbg(info->dev, "Enter %s\n", __func__);

	if (v->vmode & FB_VMODE_INTERLACED) {
		/* enable interlaced mode */
		interlaced = CFG_TV_INTERLACE_EN | CFG_TV_NIB;

		/* interlaced mode, TV path VDMA should be disabled */
		if (fbi->vdma_enable) {
			gfx_vdma = 1;
			pxa688fb_vdma_en(fbi->id, 0, 0);
		} else if (fbi_ovly) {
			if (fbi_ovly->vdma_enable) {
				ovly_vdma = 1;
				pxa688fb_vdma_en(fbi->id, 1, 0);
			}
		}
		val = vdma_ctrl_read(fbi) & 1;
		if (val)
			pxa688fb_vdma_release(fbi);

		x = v->xres + v->right_margin + v->hsync_len + v->left_margin;

		/* interlaced mode, recalculate vertical pixels */
		yres = v->yres >> 1;
		y = yres + v->lower_margin + v->vsync_len + v->upper_margin;

		/* even field */
		writel(((y + 1) << 16) | yres,
			fbi->reg_base + LCD_TV_V_H_TOTAL_FLD);
		writel(((v->upper_margin) << 16) | (v->lower_margin),
			fbi->reg_base + LCD_TV_V_PORCH_FLD);
		vsync_ctrl = (x >> 1) - v->left_margin - v->hsync_len;
		writel(vsync_ctrl << 16 | vsync_ctrl,
			fbi->reg_base + LCD_TV_SEPXLCNT_FLD);

		/* odd field */
		writel((yres << 16) | v->xres, &regs->screen_active);
		writel((y << 16) | x, &regs->screen_size);
	} else {
		/* vdma recovery */
		if (gfx_vdma) {
			gfx_vdma = 0;
			pxa688fb_vdma_en(fbi->id, 0, 1);
		} else if (ovly_vdma) {
			ovly_vdma = 0;
			pxa688fb_vdma_en(fbi->id, 1, 1);
		}
	}

	dma_ctrl_set(fbi->id, 1, CFG_TV_INTERLACE_EN | CFG_TV_NIB, interlaced);
}

static void set_dumb_screen_dimensions(struct fb_info *info)
{
	struct pxa168fb_info *fbi = info->par;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	struct fb_var_screeninfo *v = &info->var;
	struct lcd_regs *regs = get_regs(fbi->id);
	struct dsi_info *di = mi->dsi;
	int x, y, h_porch, vec = 10, vsync_ctrl;

	dev_dbg(info->dev, "Enter %s fb %d regs->screen_active 0x%p\n",
			__func__, fbi->id, &regs->screen_active);

	/* resolution, active */
	writel((v->yres << 16) | v->xres, &regs->screen_active);

	/* h porch, left/right margin */
	if (mi->phy_type & (DSI2DPI | DSI)) {
		h_porch = (v->xres + v->right_margin) * vec / 10 - v->xres;
		h_porch = (v->left_margin * vec / 10) << 16 | h_porch;
	} else
		h_porch = (v->left_margin) << 16 | v->right_margin;
	writel(h_porch, &regs->screen_h_porch);

	/* v porch, upper/lower margin */
	writel((v->upper_margin << 16) | v->lower_margin,
			&regs->screen_v_porch);

	if (mi->phy_type & (DSI2DPI | DSI))
		vec = ((di->lanes <= 2) ? 1 : 2) * 10 * di->bpp / 8 / di->lanes;

	x = v->xres + v->right_margin + v->hsync_len + v->left_margin;
	x = x * vec / 10;
	y = v->yres + v->lower_margin + v->vsync_len + v->upper_margin;

	writel((y << 16) | x, &regs->screen_size);

	/* vsync ctrl */
	if (mi->phy_type & (DSI2DPI | DSI))
		vsync_ctrl = 0x01330133;
	else {
		if ((fbi->id == 0) || (fbi->id == 2))
			vsync_ctrl = ((v->width + v->left_margin) << 16)
				| (v->width + v->left_margin);
		else
			vsync_ctrl = ((v->xres + v->right_margin) << 16)
				| (v->xres + v->right_margin);

	}
	writel(vsync_ctrl, &regs->vsync_ctrl);	/* FIXME */
}

static void pxa168fb_clear_framebuffer(struct fb_info *info)
{
	struct pxa168fb_info *fbi = info->par;

	memset(fbi->fb_start, 0, fbi->fb_size);
}

void pxa168fb_rotate_dual(unsigned int rotate)
{
	struct fb_var_screeninfo *var = &gfx_info.fbi[fb_dual]->fb_info->var;
	struct fb_var_screeninfo *base_var = &gfx_info.fbi[fb_base]->fb_info->var;
	int xres, yres, xres_z, yres_z;

	gfx_info.xres = base_var->xres; gfx_info.yres = base_var->yres;
	gfx_info.bpp = base_var->bits_per_pixel;
	gfx_info.xres_virtual = base_var->xres_virtual;
	gfx_info.xres_z = var->xres; gfx_info.yres_z = var->yres;
	xres = gfx_info.xres; yres = gfx_info.yres;
	xres_z = gfx_info.xres_z; yres_z = gfx_info.yres_z;

	if (rotate) {
		xres = gfx_info.yres;
		yres = gfx_info.xres;
	}

	if ((xres_z < xres && yres_z < yres) ||
		((xres << 3 / yres) == (xres_z << 3 / yres_z))) {
		/* zoom down or the same ratio */
		gfx_info.left_z = gfx_info.top_z = 0;
	} else if ((xres_z < xres) && (yres_z > yres)) {
		/* xres zoom down, yres zoom up */
		gfx_info.left_z = 0;
		gfx_info.top_z = (yres_z - xres_z * yres / xres) / 2;
	} else if ((yres_z < yres) && (xres_z > xres)) {
		/* xres zoom up, yres zoom down */
		gfx_info.left_z = (xres_z - yres_z * xres / yres) / 2;
		gfx_info.top_z = 0;
	} else if ((xres << 3 / yres) < (xres_z << 3 / yres_z)) {
		/* xres & yres zoom up */
		if (rotate) {
			/* no zoom */
			gfx_info.left_z = (xres_z - xres) / 2;
			gfx_info.top_z = (yres_z - yres) / 2;
		} else {
			/* zoom, xres_real -> xres * yres_z / yres */
			gfx_info.left_z = (xres_z - xres * yres_z / yres) / 2;
			gfx_info.top_z = 0;
		}
	} else {
		/* xres & yres zoom up, yres_real = yres * xres_z / xres */
		gfx_info.left_z = 0;
		gfx_info.top_z = (yres_z - yres * xres_z / xres) / 2;
	}

	pr_debug("gfx_info: rotate_addr 0x%x xres %d yres %d\n"
		" xres_z %d yres_z %d\n" "left_z %d top_z %d\n",
		gfx_info.rotate_addr, xres, yres,
		gfx_info.xres_z, gfx_info.yres_z,
		gfx_info.left_z, gfx_info.top_z);
}

static void pxa168fb_gfx_dual(unsigned int addr)
{
	pr_debug("%s addr 0x%x gfx_info.rotate_addr 0x%x\n",
		__func__, addr, gfx_info.rotate_addr);

	pxa168fb_rotate_dual(addr);
	gfx_info.rotate_addr = addr;
}

static void set_screen(struct pxa168fb_info *fbi, struct pxa168fb_mach_info *mi)
{
	struct fb_var_screeninfo *var;
	struct lcd_regs *regs;
	struct dsi_info *di;
	u32 x, vec = 10;
	struct _sOvlySurface *surface = &fbi->surface;
	u32 xres, yres, xres_z, yres_z, xres_virtual, bits_per_pixel;
	u32 left = 0, top = 0;

again:
	var = &fbi->fb_info->var;
	regs = get_regs(fbi->id);
	di = mi->dsi;
	xres = var->xres; yres = var->yres;
	xres_z = var->xres; yres_z = var->yres;
	xres_virtual = var->xres_virtual;
	bits_per_pixel = var->bits_per_pixel;

	if (mi->phy_type & (DSI2DPI | DSI))
		vec = ((di->lanes <= 2) ? 1 : 2) * 10 * di->bpp / 8 / di->lanes;

	if ((fb_mode) && (fbi->id == fb_dual)) {

		/* calculate tv path display region */
		pxa168fb_gfx_dual(gfx_info.rotate_addr);

		if (gfx_info.rotate_addr) {
			xres = gfx_info.yres;
			yres = gfx_info.xres;
			xres_virtual = ALIGN(gfx_info.yres, 16);
		} else {
			xres = gfx_info.xres;
			yres = gfx_info.yres;
			xres_virtual = max(gfx_info.xres, gfx_info.xres_virtual);
		}
		bits_per_pixel = gfx_info.bpp;
		left = gfx_info.left_z;
		top = gfx_info.top_z;
	}

	dev_dbg(fbi->fb_info->dev, "fb_mode %d fbi[%d]: xres %d xres_z %d"
		" yres %d yres_z %d xres_virtual %d bits_per_pixel %d\n",
		fb_mode, fbi->id, xres, xres_z, yres, yres_z,
		xres_virtual, bits_per_pixel);

	/* xres_z = total - left - right */
	xres_z = xres_z - (left << 1);
	/* yres_z = yres_z - top - bottom */
	yres_z = yres_z - (top << 1);

	if (fbi->new_addr[0] || (fb_mode && fbi->id == fb_dual
				&& gfx_info.fbi[fb_base]->new_addr[0])) {
		xres = surface->viewPortInfo.srcWidth;
		yres = surface->viewPortInfo.srcHeight;
		var->xres_virtual = surface->viewPortInfo.srcWidth;
		var->yres_virtual = surface->viewPortInfo.srcHeight * 2;
		xres_virtual = surface->viewPortInfo.srcWidth;

		xres_z = surface->viewPortInfo.zoomXSize;
		yres_z = surface->viewPortInfo.zoomYSize;

		left = surface->viewPortOffset.xOffset;
		top = surface->viewPortOffset.yOffset;

		if (fb_mode && fbi->id == fb_dual)
			dual_pos_zoom(fbi, surface, &xres_z, &yres_z,
					&left, &top);

		pr_debug("surface: xres %d xres_z %d"
			" yres %d yres_z %d\n left %d top %d\n",
		xres, xres_z, yres, yres_z, left, top);
	}

	dev_dbg(fbi->fb_info->dev, "            adjust: xres %d xres_z %d"
		" yres %d yres_z %d\n left %d top %d\n",
		xres, xres_z, yres, yres_z, left, top);

	/* start address on screen */
	writel((top << 16) | left, &regs->g_start);

	/* pitch, pixels per line */
	x = readl(&regs->g_pitch);
	if (fbi->surface.viewPortInfo.yPitch)
		x = (x & ~0xFFFF) | fbi->surface.viewPortInfo.yPitch;
	else
		x = (x & ~0xFFFF) | ((xres_virtual * bits_per_pixel) >> 3);
	writel(x, &regs->g_pitch);

	/* resolution, src size */
	writel((yres << 16) | xres, &regs->g_size);
	/* resolution, dst size */
	writel((yres_z << 16) | xres_z, &regs->g_size_z);

	if (FB_MODE_DUP) {
		fbi = gfx_info.fbi[fb_dual];
		mi = fbi->dev->platform_data;
		goto again;
	}
}

static void pxa168fb_set_regs(struct fb_info *info, int wait_vsync)
{
	struct pxa168fb_info *fbi = info->par;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	u32 x;

	pr_debug("%s fbi %d\n", __func__, fbi->id);
	/* Calculate clock divisor. */
	set_clock_divider(fbi);

	/* Configure global panel parameters. */
	set_screen(fbi, mi);

	/* Configure dumb panel ctrl regs & timings */
	set_dumb_panel_control(info);

	if (gfx_info.fbi[1] && (fbi->id == 1 || fb_mode)) {
		set_tv_interlace();
	}
	x = readl(fbi->reg_base + intf_ctrl(fbi->id));
	if ((x & 1) == 0)
		writel(x | 1, fbi->reg_base + intf_ctrl(fbi->id));

	/* Close the dma in order to clear the screen when video stopped */
	dma_ctrl_set(fbi->id, 0, CFG_GRA_ENA_MASK, 0);
	if (FB_MODE_DUP) {
		x = readl(fbi->reg_base + intf_ctrl(fb_dual));
	        if ((x & 1) == 0)
	                writel(x | 1, fbi->reg_base + intf_ctrl(fb_dual));
		dma_ctrl_set(fb_dual, 0, CFG_GRA_ENA_MASK, 0);
	}

	set_dma_control0(fbi);
	set_graphics_start(info, info->var.xoffset,
		info->var.yoffset, wait_vsync);
}

static int pxa168fb_set_var(struct fb_info *info)
{
	struct pxa168fb_info *fbi = info->par;
	struct fb_var_screeninfo *var = &info->var;
	struct pxa168fb_mach_info *mi;
	struct dsi_info *di;
	int pix_fmt;

	dev_dbg(info->dev, "Enter %s\n", __FUNCTION__);

	mi = fbi->dev->platform_data;
	di = mi->dsi;

	/* Determine which pixel format we're going to use */
	pix_fmt = determine_best_pix_fmt(&info->var);
	if (pix_fmt < 0)
		return pix_fmt;

	/* convet var to video mode; del for HDMI resolutions select via app
	mode = fb_find_best_mode(var, &info->modelist);
	set_mode(fbi, var, mode, pix_fmt, 1); */
	set_pix_fmt(var, pix_fmt);
	if (!var->xres_virtual)
		var->xres_virtual = var->xres;
	if (!var->yres_virtual)
		var->yres_virtual = var->yres * 2;
	var->grayscale = 0;
	var->accel_flags = FB_ACCEL_NONE;
	var->rotate = FB_ROTATE_UR;

	dev_dbg(info->dev, "xres=%d yres=%d\n", var->xres, var->yres);

	if (NEED_VSYNC(fbi, 0)) {
		fbi->info = info;
		wait_for_vsync(fbi, 0);
	} else
		pxa168fb_set_regs(info, 1);

	return 0;
}

static int pxa168fb_set_par(struct fb_info *info)
{
	struct pxa168fb_info *fbi = info->par;
	struct fb_var_screeninfo *var = &info->var;
	int pix_fmt;

	dev_dbg(info->dev, "Enter %s, graphics layer\n", __func__);

	/* Determine which pixel format we're going to use */
	pix_fmt = determine_best_pix_fmt(&info->var);
	fbi->pix_fmt = pix_fmt;

	/* Set additional mode info */
	if (pix_fmt == PIX_FMT_PSEUDOCOLOR)
		info->fix.visual = FB_VISUAL_PSEUDOCOLOR;
	else
		info->fix.visual = FB_VISUAL_TRUECOLOR;

	info->fix.line_length = var->xres_virtual * var->bits_per_pixel / 8;

	set_dumb_screen_dimensions(info);
	pxa168fb_set_var(info);

	return 0;
}

static unsigned int chan_to_field(unsigned int chan, struct fb_bitfield *bf)
{
	return ((chan & 0xffff) >> (16 - bf->length)) << bf->offset;
}

static u32 to_rgb(u16 red, u16 green, u16 blue)
{
	red >>= 8; green >>= 8; blue >>= 8;

	return (red << 16) | (green << 8) | blue;
}

static int
pxa168fb_setcolreg(unsigned int regno, unsigned int red, unsigned int green,
		 unsigned int blue, unsigned int trans, struct fb_info *info)
{
	struct pxa168fb_info *fbi = info->par;
	u32 val;

	if (info->var.grayscale)
		red = green = blue = (19595 * red + 38470 * green +
					7471 * blue) >> 16;

	if (info->fix.visual == FB_VISUAL_TRUECOLOR && regno < 16) {
		val =  chan_to_field(red,   &info->var.red);
		val |= chan_to_field(green, &info->var.green);
		val |= chan_to_field(blue , &info->var.blue);
		fbi->pseudo_palette[regno] = val;
	}

	if (info->fix.visual == FB_VISUAL_PSEUDOCOLOR && regno < 256) {
		val = to_rgb(red, green, blue);
		writel(val, fbi->reg_base + LCD_SPU_SRAM_WRDAT);
		writel(0x8300 | regno, fbi->reg_base + LCD_SPU_SRAM_CTRL);
	}

	return 0;
}

static int pxa168fb_active(struct pxa168fb_info *fbi, int active)
{
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	int clk = 0;

	dev_dbg(fbi->fb_info->dev, "Enter %s fbi[%d] active %d\n",
			__FUNCTION__, fbi->id, active);
	if (!active && fbi->active){
		atomic_set(&fbi->w_intr, 1);
		wake_up(&fbi->w_intr_wq);

		/* disable external panel power */
		pxa168fb_power(fbi, mi, 0);
		fbi->active = 0;

		/* disable path clock */
		clk = readl(fbi->reg_base + clk_div(fbi->id)) | SCLK_DISABLE;
		writel(clk, fbi->reg_base + clk_div(fbi->id));
	}

	if (active && !fbi->active) {
		/* enable path clock */
		clk = readl(fbi->reg_base + clk_div(fbi->id)) & (~SCLK_DISABLE);
		writel(clk, fbi->reg_base + clk_div(fbi->id));

		/* enable external panel power */
		pxa168fb_power(fbi, mi, 1);

		/* initialize external phy if needed */
		if (mi->phy_init && mi->phy_init(fbi)) {
			pr_err("%s fbi %d phy error\n", __func__, fbi->id);
			return -EIO;
		}

		fbi->active = 1;
		set_dma_control0(fbi);
	}
	return 0;
}

static int pxa168fb_blank(int blank, struct fb_info *info)
{
	struct pxa168fb_info *fbi = info->par;
#ifdef CONFIG_PM
	switch (blank) {
		case FB_BLANK_POWERDOWN:
		case FB_BLANK_VSYNC_SUSPEND:
		case FB_BLANK_HSYNC_SUSPEND:
		case FB_BLANK_NORMAL:
			/* de-activate the device */
			pxa168fb_active(fbi, 0);

			/* sync dual path behavior */
			if (fb_mode)
				pxa168fb_active(gfx_info.fbi[fb_dual], 0);

			/* allow system enter low power modes */
			unset_dvfm_constraint();
			break;

		case FB_BLANK_UNBLANK:
			/* activate the device */
			pxa168fb_active(fbi, 1);

			/* sync dual path behavior */
			if (FB_MODE_DUP)
				pxa168fb_active(gfx_info.fbi[fb_dual], 1);

			/* avoid system enter low power modes */
			set_dvfm_constraint();
			break;
		default:
			break;
	}
	return 0;
#else
	fbi->is_blanked = (blank == FB_BLANK_UNBLANK) ? 0 : 1;
	set_dumb_panel_control(info);

	return 0;
#endif
}

static int pxa168fb_pan_display(struct fb_var_screeninfo *var,
				struct fb_info *info)
{
	dev_dbg(info->dev, "Enter %s\n", __FUNCTION__);
	set_graphics_start(info, var->xoffset, var->yoffset, 1);
	return 0;
}

extern irqreturn_t pxa168fb_ovly_isr(int id);
static irqreturn_t pxa168fb_handle_irq(int irq, void *dev_id)
{
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)dev_id;
	u32 isr_en = readl(fbi->reg_base + SPU_IRQ_ISR) &
		readl(fbi->reg_base + SPU_IRQ_ENA);
	u32 id, dispd, err, sts;

	do {
		irq_status_clear(0, isr_en);
		/* display done irq */
		dispd = isr_en & display_done_imasks;
		if (dispd) {
			for (id = 0; id < 2; id++) {
				sts = dispd & display_done_imask(id);
				if (sts) {
#ifdef CONFIG_DVFM
					wakeup_dvfm_seq();
#endif
					pxa168fb_ovly_isr(id);

					if (!(fbi = gfx_info.fbi[id]))
						break;

					/* update registers if needed */
					if (fbi->info) {
						pxa168fb_set_regs(fbi->info, 0);
						fbi->info = NULL;
					}

					if (fbi->misc_update) {
						pxa168fb_misc_update(fbi);
						fbi->misc_update = 0;
					}
					/* wake up queue if condition */
					if (atomic_read(&fbi->w_intr) == 0) {
						atomic_set(&fbi->w_intr, 1);
						wake_up(&fbi->w_intr_wq);
					}

					/* trigger buf update */
					buf_endframe(fbi->fb_info, 0);

					if (vsync_check &&
						id == DEBUG_VSYNC_PATH(0))
						dispd_count++;
				}
			}
		}

		/* LCD under run error detect */
		err = isr_en & err_imasks;
		if (err) {
			for (id = 0; id < 3; id++) {
				if (err & gfx_udflow_imask(id)) {
					gfx_udflow_count++;
					if (DEBUG_ERR_IRQ(0))
					  pr_err("fb%d gfx underflow\n", id);
				}
				if (err & vid_udflow_imask(id)) {
					vid_udflow_count++;
					if (DEBUG_ERR_IRQ(0))
					  pr_err("fb%d vid underflow\n", id);
				}
			}
			if (err & AXI_BUS_ERROR_IRQ_ENA_MASK) {
				axi_err_count++;
				if (DEBUG_ERR_IRQ(0))
					pr_info("axi bus err\n");
			}
			if (err & AXI_LATENCY_TOO_LONG_IRQ_ENA_MASK) {
				axi_err_count++;
				if (DEBUG_ERR_IRQ(0))
					pr_info("axi lantency too long\n");
			}
		}

		/* count interrupts numbers in 10s */
		if (vsync_check) {
			id = DEBUG_VSYNC_PATH(0);
			if (isr_en & path_imasks(id))
				irq_count++;
			if (isr_en & gf0_imask(id))
				f0_count++;
			if (isr_en & gf1_imask(id))
				f1_count++;
			if (isr_en & vf0_imask(id))
				vf0_count++;
			if (isr_en & vf1_imask(id))
				vf1_count++;
			if (isr_en & vsync_imask(id))
				vsync_count++;
		}
	} while ((isr_en = readl(gfx_info.fbi[0]->reg_base + SPU_IRQ_ISR)) &
			readl(gfx_info.fbi[0]->reg_base + SPU_IRQ_ENA) &
			(path_imasks(0) | path_imasks(1) | err_imasks));

	return IRQ_HANDLED;
}

#ifdef CONFIG_DYNAMIC_PRINTK_DEBUG
static void debug_identify_called_ioctl(struct fb_info *info, int cmd, unsigned long arg)
{
	switch (cmd) {
	case FB_IOCTL_CLEAR_FRAMEBUFFER:
		dev_dbg(fi->dev," FB_IOCTL_CLEAR_FRAMEBUFFER\n");
		break;
	case FB_IOCTL_PUT_SWAP_GRAPHIC_RED_BLUE:
		dev_dbg(info->dev," FB_IOCTL_PUT_SWAP_GRAPHIC_RED_BLUE with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_PUT_SWAP_GRAPHIC_U_V:
		dev_dbg(info->dev," FB_IOCTL_PUT_SWAP_GRAPHIC_U_V with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_PUT_SWAP_GRAPHIC_Y_UV:
		dev_dbg(info->dev," FB_IOCTL_PUT_SWAP_GRAPHIC_Y_UV with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_PUT_VIDEO_ALPHABLEND:
		dev_dbg(info->dev," FB_IOCTL_PUT_VIDEO_ALPHABLEND with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_PUT_GLOBAL_ALPHABLEND:
		dev_dbg(info->dev," FB_IOCTL_PUT_GLOBAL_ALPHABLEND with arg = %08x\n",(unsigned int) arg);
		break;
	case FB_IOCTL_PUT_GRAPHIC_ALPHABLEND:
		dev_dbg(info->dev," FB_IOCTL_PUT_GRAPHIC_ALPHABLEND with arg = %08x\n", (unsigned int)arg);
		break;

	}
}
#endif

static int pxa168fb_update_buff(struct fb_info *fi,
	struct _sOvlySurface *surface, int address)
{
	if (address) {
		/* update buffer address only if changed */
		if (check_surface_addr(fi, surface, 0))
			set_graphics_start(fi, 0, 0, 1);
		else
			return -EINVAL;
	} else if (check_surface(fi, surface->videoMode,
					&surface->viewPortInfo,
					&surface->viewPortOffset,
					&surface->videoBufferAddr,
					&surface->dualInfo,
					0))
		/* update other parameters other than buf addr */
		return pxa168fb_set_var(fi);

	return 0;
}

static int pxa168_graphic_ioctl(struct fb_info *info, unsigned int cmd, unsigned long arg)
{
	int blendval;
	int val, mask, gra_on;
	unsigned long flags;
	unsigned char param;
	struct pxa168fb_info *fbi = info->par;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	struct pxa168fb_gra_partdisp grap;
	void __user *argp = (void __user *)arg;

#ifdef CONFIG_DYNAMIC_PRINTK_DEBUG
	debug_identify_called_ioctl(info, cmd, arg);
#endif
	dev_dbg(info->dev, "%s cmd 0x%x\n", __func__, cmd);

	switch (cmd) {

	case FB_IOCTL_CLEAR_FRAMEBUFFER:
		pxa168fb_clear_framebuffer(info);
		break;
	case FB_IOCTL_WAIT_VSYNC:
		if (NEED_VSYNC(fbi, 0))
			wait_for_vsync(fbi, 0);
		break;
	case FB_IOCTL_WAIT_VSYNC_ON:
		fbi->wait_vsync = 1;
		if (FB_MODE_DUP)
			gfx_info.fbi[fb_dual]->wait_vsync = 1;
		break;
	case FB_IOCTL_WAIT_VSYNC_OFF:
		fbi->wait_vsync = 0;
		if (FB_MODE_DUP)
			gfx_info.fbi[fb_dual]->wait_vsync = 0;
		break;
	case FB_IOCTL_PUT_VIDEO_ALPHABLEND:
		/* This puts the blending control to the Video layer */
		mask = CFG_ALPHA_MODE_MASK | CFG_ALPHA_MASK;
		val = CFG_ALPHA_MODE(0) | CFG_ALPHA(0xff);
		dma_ctrl_set(fbi->id, 1, mask, val);
		if (FB_MODE_DUP)
			dma_ctrl_set(fb_dual, 1, mask, val);
		break;

	case FB_IOCTL_PUT_GLOBAL_ALPHABLEND:
		/*  The userspace application can specify a byte value for the amount of global blend
		 *  between the video layer and the graphic layer.
		 *
		 *  The alpha blending is per the formula below:
		 *  P = (V[P] * blendval/255) + (G[P] * (1 - blendval/255))
		 *
		 *     where: P = Pixel value, V = Video Layer, and G = Graphic Layer
		 */
		blendval = (arg & 0xff);
		mask = CFG_ALPHA_MODE_MASK | CFG_ALPHA_MASK;
		val = CFG_ALPHA_MODE(2) | CFG_ALPHA(blendval);
		dma_ctrl_set(fbi->id, 1, mask, val);
		if (FB_MODE_DUP)
			dma_ctrl_set(fb_dual, 1, mask, val);
		break;

	case FB_IOCTL_PUT_GRAPHIC_ALPHABLEND:
		/*  This puts the blending back to the default mode of allowing the
		 *  graphic layer to do pixel level blending.
		 */
		mask = CFG_ALPHA_MODE_MASK | CFG_ALPHA_MASK;
		val = CFG_ALPHA_MODE(1) | CFG_ALPHA(0x0);
		dma_ctrl_set(fbi->id, 1, mask, val);
		if (FB_MODE_DUP)
			dma_ctrl_set(fb_dual, 1, mask, val);
		break;

	case FB_IOCTL_SWAP_GRAPHIC_RED_BLUE:
		param = (arg & 0x1);
		mask = CFG_GRA_SWAPRB_MASK;
		val = CFG_GRA_SWAPRB(param);
		dma_ctrl_set(fbi->id, 0, mask, val);
		if (FB_MODE_DUP)
			dma_ctrl_set(fb_dual, 1, mask, val);
		break;

	case FB_IOCTL_SWAP_GRAPHIC_U_V:
		param = (arg & 0x1);
		mask = CFG_GRA_SWAPUV_MASK;
		val = CFG_GRA_SWAPUV(param);
		dma_ctrl_set(fbi->id, 0, mask, val);
		if (FB_MODE_DUP)
			dma_ctrl_set(fb_dual, 1, mask, val);
		break;

	case FB_IOCTL_SWAP_GRAPHIC_Y_UV:
		param = (arg & 0x1);
		mask = CFG_GRA_SWAPYU_MASK;
		val = CFG_GRA_SWAPYU(param);
		dma_ctrl_set(fbi->id, 0, mask, val);
		break;

    #ifdef CONFIG_FB_SECOND_PANEL_HDMI
	case FB_IOCTL_ROTATE_ADDR:
		fbi = gfx_info.fbi[fb_dual];
		pxa168fb_gfx_dual(arg);
		/* trigger pxa168_set_regs @ TV vsync */
		fbi->info = fbi->fb_info;
		break;
	#endif
	case FB_IOCTL_FLIP_VID_BUFFER:
		val = flip_buffer(info, arg, 0);
		if (NEED_VSYNC(fbi, 0))
			wait_for_vsync(fbi, 0);
		return val;
	case FB_IOCTL_GET_FREELIST:
		return get_freelist(info, arg, 0);
	case FB_IOCTL_SWITCH_GRA_OVLY:
		if (copy_from_user(&gra_on, argp, sizeof(int)))
			return -EFAULT;

		spin_lock_irqsave(&fbi->var_lock, flags);
		fbi->dma_on = gra_on ? 1 : 0;
		mask = CFG_GRA_ENA_MASK;
		val = CFG_GRA_ENA(check_modex_active(fbi->id, fbi->active));
		dma_ctrl_set(fbi->id, 0, mask, val);

		pr_info("SWITCH_GRA_OVLY fbi %d dma_on %d, val %d\n",
			fbi->id, fbi->dma_on, val);

		if (FB_MODE_DUP) {
			gfx_info.fbi[fb_dual]->dma_on = fbi->dma_on;
			val = CFG_DMA_ENA(check_modex_active(fb_dual,
					gfx_info.fbi[fb_dual]->active));
			dma_ctrl_set(fb_dual, 0, mask, val);
		}
		spin_unlock_irqrestore(&fbi->var_lock, flags);
		break;

	case FB_IOCTL_GRA_PARTDISP:
		if (copy_from_user(&grap, argp, sizeof(grap)))
			return -EFAULT;
		pxa688fb_partdisp_set(grap);
		break;

	default:
		if (mi->ioctl)
			return mi->ioctl(info, cmd, arg);
		else
			pr_warning("%s: unknown IOCTL 0x%x\n", __func__, cmd);
		break;

	}
	return 0;
}

static int pxa168fb_open(struct fb_info *info, int user)
{
	struct pxa168fb_mach_info *mi;
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)info->par;
	struct fb_var_screeninfo *var = &info->var;

	if (fbi->debug & (1<<4))
		return 0;

	if (fb_mode && (fbi->id == fb_dual)) {
		printk(KERN_ERR "fb_mode %x, operation to TV path is not \
		allowed,\n Please switch to mode0 if needed.\n", fb_mode);
		return -EAGAIN;
	}

	printk("Enter %s, fbi %d ---------------\n",
			__FUNCTION__, fbi->id);
again:
	/* Save screen info */
	fbi->var_bak = *var;

	mi = fbi->dev->platform_data;

	fbi->new_addr[0] = 0;
	fbi->new_addr[1] = 0;
	fbi->new_addr[2] = 0;
	fbi->surface.videoMode = -1;
	fbi->surface.viewPortInfo.srcWidth = var->xres;
	fbi->surface.viewPortInfo.srcHeight = var->yres;

	set_pix_fmt(var, fbi->pix_fmt);

	if (mutex_is_locked(&fbi->access_ok))
		mutex_unlock(&fbi->access_ok);

	if (FB_MODE_DUP) {
		fbi = gfx_info.fbi[fb_dual];
		info = fbi->fb_info;
		var = &info->var;
		goto again;
	}

	return 0;
}

static int pxa168fb_release(struct fb_info *info, int user)
{
	struct fb_var_screeninfo *var = &info->var;
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)info->par;
	struct pxa168fb_info *fbi_bak = fbi;
	struct fb_info *info_bak = info;

	if (fbi->debug & (1<<4))
		return 0;

	printk("Enter %s, fbi %d ---------------\n",
			__FUNCTION__, fbi->id);

again:
	/* Turn off compatibility mode */
	var->nonstd &= ~0xff000000;
	COMPAT_MODE = 0;

	memset(&fbi->surface.viewPortInfo, 0,
			sizeof(fbi->surface.viewPortInfo));

	/* clear buffer list */
	clear_buffer(fbi, 0);

	/* Recovery screen info */
	*var = fbi->var_bak;

	fbi->pix_fmt = determine_best_pix_fmt(&info->var);

	fbi->new_addr[0] = 0;
	fbi->new_addr[1] = 0;
	fbi->new_addr[2] = 0;

	if (FB_MODE_DUP) {
		fbi = gfx_info.fbi[fb_dual];
		info = fbi->fb_info;
		var = &info->var;

		goto again;
	}

	if (NEED_VSYNC(fbi_bak, 0)) {
		fbi->info = info_bak;
		wait_for_vsync(fbi_bak, 0);
	}

	return 0;
}

static struct fb_ops pxa168fb_ops = {
	.owner		= THIS_MODULE,
	.fb_check_var	= pxa168fb_check_var,
	.fb_open	= pxa168fb_open,
	.fb_release	= pxa168fb_release,
	.fb_set_par	= pxa168fb_set_par,
	.fb_setcolreg	= pxa168fb_setcolreg,
	.fb_blank	= pxa168fb_blank,
	.fb_pan_display	= pxa168fb_pan_display,
	.fb_fillrect	= cfb_fillrect,
	.fb_copyarea	= cfb_copyarea,
	.fb_imageblit	= cfb_imageblit,
	.fb_ioctl       = pxa168_graphic_ioctl,
};

static int pxa168fb_init_mode(struct fb_info *info,
			      struct pxa168fb_mach_info *mi)
{
	struct pxa168fb_info *fbi = info->par;
	struct fb_var_screeninfo *var = &info->var;
	int ret = 0;
	u32 total_w, total_h, refresh;
	u64 div_result;
	const struct fb_videomode *m;

	dev_dbg(info->dev, "Enter %s\n", __FUNCTION__);

	/* Set default value */
	refresh = DEFAULT_REFRESH;

	/* If has bootargs, apply it first */
	if (fbi->dft_vmode.xres && fbi->dft_vmode.yres &&
	    fbi->dft_vmode.refresh) {
		/* set data according bootargs */
		var->xres = fbi->dft_vmode.xres;
		var->yres = fbi->dft_vmode.yres;
		refresh = fbi->dft_vmode.refresh;
	}

	/* try to find best video mode. */
	m = fb_find_best_mode(&info->var, &info->modelist);
	if (m)
		fb_videomode_to_var(&info->var, m);

	/* Init settings. */
	var->xres_virtual = var->xres;
	var->yres_virtual = var->yres * 2;

	if (!var->pixclock) {
		/* correct pixclock. */
		total_w = var->xres + var->left_margin + var->right_margin +
			  var->hsync_len;
		total_h = var->yres + var->upper_margin + var->lower_margin +
			  var->vsync_len;

		div_result = 1000000000000ll;
		do_div(div_result, total_w * total_h * refresh);
		var->pixclock = (u32)div_result;
	}

	return ret;
}

static void pxa168fb_set_default(struct pxa168fb_info *fbi,
		struct pxa168fb_mach_info *mi)
{
	struct lcd_regs *regs = get_regs(fbi->id);
	u32 dma_ctrl1 = 0x2012ff81, tmp;

	/*
	 * LCD Global control(LCD_TOP_CTRL) should be configed before
	 * any other LCD registers read/write, or there maybe issues.
	 */
	tmp = readl(fbi->reg_base + LCD_TOP_CTRL);
	tmp |= 0xfff0;		/* FIXME */
	writel(tmp, fbi->reg_base + LCD_TOP_CTRL);

	/* Configure default register values */
	writel(mi->io_pad_ctrl, fbi->reg_base + SPU_IOPAD_CONTROL);
		/* enable 16 cycle burst length to get better formance */

	writel(0x00000000, &regs->blank_color);
	writel(0x00000000, &regs->g_1);
	writel(0x00000000, &regs->g_start);

	/* Configure default bits: vsync triggers DMA,
	 * power save enable, configure alpha registers to
	 * display 100% graphics, and set pixel command.
	 */
	if (fbi->id == 1) {
		if (mi->phy_type & (DSI2DPI | DSI))
			dma_ctrl1 = 0xa03eff00;
		else
			dma_ctrl1 = 0x203eff00;	/* FIXME */
	}

	/*
	 * vsync in LCD internal controller is always positive,
	 * we default configure dma trigger @vsync falling edge,
	 * so that DMA idle time between DMA frame done and
	 * next DMA transfer begin can be as large as possible.
	 */
	dma_ctrl1 |= CFG_VSYNC_INV_MASK;
	dma_ctrl_write(fbi->id, 1, dma_ctrl1);

	/* blank color */
	writel(0x00000000, &regs->blank_color);
}

static int __init get_fb_size(char *str)
{
	int n;
	if (!get_option(&str, &n))
		return 0;
	max_fb_size = n;
	fb_size_from_cmd = 1;
	return 1;
}
__setup("fb_size=", get_fb_size);

static int __init get_fb_share(char *str)
{
	fb_share = 1;
	return 1;
}
__setup("fb_share", get_fb_share);

static int __init get_fb_base(char *str)
{
    frambuffer_base = simple_strtoul(str,NULL,0);
    return 1;
}
__setup("fb_base=", get_fb_base);

#ifdef CONFIG_PM

static int _pxa168fb_suspend(struct pxa168fb_info *fbi)
{
	struct fb_info *info = fbi->fb_info;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;

	/* notify others */
	fb_set_suspend(info, 1);

	/* stop dma transaction */
	fbi->dma_ctrl0 = dma_ctrl_read(fbi->id, 0) & ~CFG_GRA_ENA_MASK;
	dma_ctrl_set(fbi->id, 0, CFG_GRA_ENA_MASK, 0);

	/* disable all lcd interrupts */
	fbi->irq_mask = readl(fbi->reg_base + SPU_IRQ_ENA);
	irq_mask_set(fbi->id, 0xffffffff, 0);

	/* disable external panel power */
	pxa168fb_power(fbi, mi, 0);
	fbi->active = 0;

	/* disable clock */
	clk_disable(fbi->clk);

	pr_debug("pxa168fb.%d suspended\n", fbi->id);
	return 0;
}

static int _pxa168fb_resume(struct pxa168fb_info *fbi)
{
	struct fb_info *info = fbi->fb_info;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;

	/* enable clock */
	if (mi->sclk_src)
		clk_set_rate(fbi->clk, mi->sclk_src);
	clk_enable(fbi->clk);

	/* enable external panel power */
	pxa168fb_power(fbi, mi, 1);

	/* register setting should retain so no need to set again.
	 * pxa168fb_set_par(info);
	 * pxa168fb_set_default(fbi, mi);
	 */

	/* initialize external phy if needed */
	if (mi->phy_init && mi->phy_init(fbi)) {
		pr_err("%s fbi %d phy error\n", __func__, fbi->id);
		return -EIO;
	}

	/* restore lcd interrupts */
	irq_mask_set(fbi->id, 0xffffffff, fbi->irq_mask);

	/* restore dma after resume */
	fbi->active = 1;
	if (check_modex_active(fbi->id, fbi->active))
		fbi->dma_ctrl0 |= CFG_GRA_ENA_MASK;
	dma_ctrl_write(fbi->id, 0, fbi->dma_ctrl0);

	/* notify others */
	fb_set_suspend(info, 0);

	pr_debug("pxa168fb.%d resumed.\n", fbi->id);
	return 0;
}

#ifdef CONFIG_HAS_EARLYSUSPEND

static void pxa168fb_early_suspend(struct early_suspend *h)
{
	struct pxa168fb_info *fbi = container_of(h, struct pxa168fb_info, early_suspend);

	_pxa168fb_suspend(fbi);
	// Ellie commented out temporarily, otherwise system hangs when entering early suspend
	// without USB OTG connected, see PNX-82
	unset_dvfm_constraint();

	return;
}
static void pxa168fb_late_resume(struct early_suspend *h)
{
	struct pxa168fb_info *fbi = container_of(h, struct pxa168fb_info, early_suspend);

	set_dvfm_constraint();
	_pxa168fb_resume(fbi);

	return;
}

#else

static int pxa168fb_suspend(struct platform_device *pdev, pm_message_t mesg)
{
	struct pxa168fb_info *fbi = platform_get_drvdata(pdev);

	_pxa168fb_suspend(fbi);
	pdev->dev.power.power_state = mesg;
	unset_dvfm_constraint();

	return 0;
}

static int pxa168fb_resume(struct platform_device *pdev)
{
	struct pxa168fb_info *fbi = platform_get_drvdata(pdev);

	set_dvfm_constraint();
	_pxa168fb_resume(fbi);

	return 0;
}

#endif /* CONFIG_HAS_EARLYSUSPEND */

#endif /* CONFIG_PM */

/**********************************************************************************************************************/

static ssize_t lcd_show(struct device *dev, struct device_attribute *attr,
		char *buf)
{
	struct pxa168fb_info *fbi = dev_get_drvdata(dev);
	struct fb_var_screeninfo *var = &fbi->fb_info->var;
	struct lcd_regs *regs = get_regs(fbi->id);
	int id = fbi->id, vsmooth_show = 0;

	printk("fbi %d base 0x%p\n", fbi->id, fbi->reg_base);
	printk("var\n");
	printk("\t xres              %4d yres              %4d\n", var->xres, var->yres);
	printk("\t xres_virtual      %4d yres_virtual      %4d\n", var->xres_virtual, var->yres_virtual);
	printk("\t xoffset           %4d yoffset           %4d\n", var->xoffset, var->yoffset);
	printk("\t left_margin(hbp)  %4d right_margin(hfp) %4d\n", var->left_margin, var->right_margin);
	printk("\t upper_margin(vbp) %4d lower_margin(vfp) %4d\n", var->upper_margin, var->lower_margin);
	printk("\t hsync_len         %4d vsync_len         %4d\n", var->hsync_len, var->vsync_len);
	printk("\t bits_per_pixel    %d\n", var->bits_per_pixel);
	printk("\t pixclock          %d\n", var->pixclock);
	printk("\t sync              0x%x\n", var->sync);
	printk("\t vmode             0x%x\n", var->vmode);
	printk("\t rotate            0x%x\n", var->rotate);
	printk("\n");

	printk("gfx_info: rotate_addr 0x%x xres %d yres %d\n xres_z %d yres_z %d left_z %d top_z %d\n\n",
		gfx_info.rotate_addr, gfx_info.xres, gfx_info.yres, gfx_info.xres_z, gfx_info.yres_z,
		gfx_info.left_z, gfx_info.top_z);

again:
	regs = get_regs(id);
	printk("video layer\n");
	printk("\tv_y0        ( @%3x ) 0x%x\n", (int)(&regs->v_y0      ) & 0xfff, readl(&regs->v_y0       ) );
	printk("\tv_u0        ( @%3x ) 0x%x\n", (int)(&regs->v_u0      ) & 0xfff, readl(&regs->v_u0       ) );
	printk("\tv_v0        ( @%3x ) 0x%x\n", (int)(&regs->v_v0      ) & 0xfff, readl(&regs->v_v0       ) );
	printk("\tv_c0        ( @%3x ) 0x%x\n", (int)(&regs->v_c0      ) & 0xfff, readl(&regs->v_c0       ) );
	printk("\tv_y1        ( @%3x ) 0x%x\n", (int)(&regs->v_y1      ) & 0xfff, readl(&regs->v_y1       ) );
	printk("\tv_u1        ( @%3x ) 0x%x\n", (int)(&regs->v_u1      ) & 0xfff, readl(&regs->v_u1       ) );
	printk("\tv_v1        ( @%3x ) 0x%x\n", (int)(&regs->v_v1      ) & 0xfff, readl(&regs->v_v1       ) );
	printk("\tv_c1        ( @%3x ) 0x%x\n", (int)(&regs->v_c1      ) & 0xfff, readl(&regs->v_c1       ) );
	printk("\tv_pitch_yc  ( @%3x ) 0x%x\n", (int)(&regs->v_pitch_yc) & 0xfff, readl(&regs->v_pitch_yc ) );
	printk("\tv_pitch_uv  ( @%3x ) 0x%x\n", (int)(&regs->v_pitch_uv) & 0xfff, readl(&regs->v_pitch_uv ) );
	printk("\tv_start     ( @%3x ) 0x%x\n", (int)(&regs->v_start   ) & 0xfff, readl(&regs->v_start    ) );
	printk("\tv_size      ( @%3x ) 0x%x\n", (int)(&regs->v_size    ) & 0xfff, readl(&regs->v_size     ) );
	printk("\tv_size_z    ( @%3x ) 0x%x\n", (int)(&regs->v_size_z  ) & 0xfff, readl(&regs->v_size_z   ) );
	printk("\n");

	printk("graphic layer\n");
	printk("\tg_0         ( @%3x ) 0x%x\n", (int)(&regs->g_0      ) & 0xfff, readl(&regs->g_0     ) );
	printk("\tg_1         ( @%3x ) 0x%x\n", (int)(&regs->g_1      ) & 0xfff, readl(&regs->g_1     ) );
	printk("\tg_pitch     ( @%3x ) 0x%x\n", (int)(&regs->g_pitch  ) & 0xfff, readl(&regs->g_pitch ) );
	printk("\tg_start     ( @%3x ) 0x%x\n", (int)(&regs->g_start  ) & 0xfff, readl(&regs->g_start ) );
	printk("\tg_size      ( @%3x ) 0x%x\n", (int)(&regs->g_size   ) & 0xfff, readl(&regs->g_size  ) );
	printk("\tg_size_z    ( @%3x ) 0x%x\n", (int)(&regs->g_size_z ) & 0xfff, readl(&regs->g_size_z) );
	printk("\n");

	printk("hardware cursor\n");
	printk("\thc_start    ( @%3x ) 0x%x\n", (int)(&regs->hc_start ) & 0xfff,  readl(&regs->hc_start      ) );
	printk("\thc_size     ( @%3x ) 0x%x\n", (int)(&regs->hc_size  ) & 0xfff, readl(&regs->hc_size       ) );
	printk("\n");

	printk("screen info\n");
	printk("\tscreen_size     ( @%3x ) 0x%x\n", (int)(&regs->screen_size    ) & 0xfff, readl(&regs->screen_size     ) );
	printk("\tscreen_active   ( @%3x ) 0x%x\n", (int)(&regs->screen_active  ) & 0xfff, readl(&regs->screen_active   ) );
	printk("\tscreen_h_porch  ( @%3x ) 0x%x\n", (int)(&regs->screen_h_porch ) & 0xfff, readl(&regs->screen_h_porch  ) );
	printk("\tscreen_v_porch  ( @%3x ) 0x%x\n", (int)(&regs->screen_v_porch ) & 0xfff, readl(&regs->screen_v_porch  ) );
	printk("\n");

	printk("color\n");
	printk("\tblank_color     ( @%3x ) 0x%x\n", (int)(&regs->blank_color    ) & 0xfff, readl(&regs->blank_color     )  );
	printk("\thc_Alpha_color1 ( @%3x ) 0x%x\n", (int)(&regs->hc_Alpha_color1) & 0xfff, readl(&regs->hc_Alpha_color1 )  );
	printk("\thc_Alpha_color2 ( @%3x ) 0x%x\n", (int)(&regs->hc_Alpha_color2) & 0xfff, readl(&regs->hc_Alpha_color2 )  );
	printk("\tv_colorkey_y    ( @%3x ) 0x%x\n", (int)(&regs->v_colorkey_y   ) & 0xfff, readl(&regs->v_colorkey_y    )  );
	printk("\tv_colorkey_u    ( @%3x ) 0x%x\n", (int)(&regs->v_colorkey_u   ) & 0xfff, readl(&regs->v_colorkey_u    )  );
	printk("\tv_colorkey_v    ( @%3x ) 0x%x\n", (int)(&regs->v_colorkey_v   ) & 0xfff, readl(&regs->v_colorkey_v    )  );
	printk("\n");

	printk("control\n");
	printk("\tvsync_ctrl      ( @%3x ) 0x%x\n", (int)(&regs->vsync_ctrl      ) & 0xfff, readl(&regs->vsync_ctrl     ));

	printk("\tdma_ctrl0       ( @%3x ) 0x%x\n", (int)(dma_ctrl(0, id)) & 0xfff, readl((fbi->reg_base + dma_ctrl0(id))));
	printk("\tdma_ctrl1       ( @%3x ) 0x%x\n", (int)(dma_ctrl(1, id)) & 0xfff, readl((fbi->reg_base + dma_ctrl1(id))));
	printk("\tintf_ctrl       ( @%3x ) 0x%x\n", (int)(intf_ctrl(id)) & 0xfff, readl(fbi->reg_base + intf_ctrl(id)));
	printk("\tirq_enable      ( @%3x ) 0x%8x\n", (int)(SPU_IRQ_ENA) & 0xfff, readl(fbi->reg_base + SPU_IRQ_ENA));
	printk("\tirq_status      ( @%3x ) 0x%8x\n", (int)(SPU_IRQ_ISR) & 0xfff, readl(fbi->reg_base + SPU_IRQ_ISR));
	printk("\tclk_div         ( @%3x ) 0x%x\n", (int)(clk_div(id)) & 0xfff, readl(fbi->reg_base + clk_div(id)));

	/* TV path registers */
	if (fbi->id == 1) {
		printk("\ntv path interlace related:\n");
		printk("\tv_h_total       ( @%3x ) 0x%8x\n", (int)(LCD_TV_V_H_TOTAL_FLD) & 0xfff, readl(fbi->reg_base + LCD_TV_V_H_TOTAL_FLD));
		printk("\tv_porch         ( @%3x ) 0x%8x\n", (int)(LCD_TV_V_PORCH_FLD) & 0xfff, readl(fbi->reg_base + LCD_TV_V_PORCH_FLD));
		printk("\tvsync_ctrl      ( @%3x ) 0x%8x\n", (int)(LCD_TV_SEPXLCNT_FLD) & 0xfff, readl(fbi->reg_base + LCD_TV_SEPXLCNT_FLD));
	}
	printk("\n");

#ifdef CONFIG_PXA688_MISC
	if (fbi->id == fb_vsmooth && !vsmooth_show) {
		id = fb_filter;
		vsmooth_show = 1;
		printk("========== fb_vsmooth = %d ==========\n", fb_vsmooth);
		goto again;
	}
#endif

	return sprintf(buf, "fb %d: active %d, debug 0x%x\n"
		"DEBUG_VSYNC_PATH %d DEBUG_ERR_IRQ %d DEBUG_TV_ACTIVE %d\n"
		"gfx_underflow %d vid_underflow %d axi_err %d dma_on %d\n",
		fbi->id, fbi->active, fbi->debug, DEBUG_VSYNC_PATH(fbi->id),
		DEBUG_ERR_IRQ(fbi->id), DEBUG_TV_ACTIVE(fbi->id),
		gfx_udflow_count, vid_udflow_count,
				axi_err_count, fbi->dma_on);
}

static ssize_t lcd_store(
		struct device *dev, struct device_attribute *attr,
		const char *buf, size_t size)
{
	struct pxa168fb_info *fbi = dev_get_drvdata(dev);

	/* fbi->debug usage:
	 * 	bit 0-1: indicates to count vsync number for which path, "0" "1"
	 * 	0: count vsync number for pn path
	 * 	1: count vsync number for tv path
	 * 	2: count vsync number for p2 path, not valid yet
	 * 	bit 2: show bus error interrupts on screen, "4"
	 * 	bit 3: enable TV path graphics layer dma always, "8"
	 * 	bit 4: enable do not restore var in release "16"
	 */
	sscanf(buf, "%d", &fbi->debug);

	return size;
}

static DEVICE_ATTR(lcd, S_IRUGO | S_IWUSR, lcd_show, lcd_store);

static ssize_t vsync_show(struct device *dev, struct device_attribute *attr,
		char *buf)
{
	struct pxa168fb_info *fbi = dev_get_drvdata(dev);

	return sprintf(buf, "fbi %d wait vsync: %d\n", fbi->id, fbi->wait_vsync);
}

static ssize_t vsync_store(
		struct device *dev, struct device_attribute *attr,
		const char *buf, size_t size)
{
	struct pxa168fb_info *fbi = dev_get_drvdata(dev);

	sscanf(buf, "%d", &fbi->wait_vsync);

	return size;
}

static DEVICE_ATTR(vsync, S_IRUGO | S_IWUSR, vsync_show, vsync_store);

/*************************************************************************/
static int pxa168fb_set_par(struct fb_info *info);
static int pxa168fb_mode_switch(int mode)
{
	struct pxa168fb_info *fbi_base = gfx_info.fbi[fb_base];
	struct pxa168fb_info *fbi_dual = gfx_info.fbi[fb_dual];
	struct fb_info *info_base = fbi_base->fb_info;
	struct fb_info *info_dual = fbi_dual->fb_info;
	struct pxa168fb_mach_info *mi_base = fbi_base->dev->platform_data;
	struct pxa168fb_mach_info *mi_dual = fbi_dual->dev->platform_data;

	if (fb_share) {
		printk("fb_share mode already, try clone mode w/o"
			" fb_share in cmdline\n");
		return -EAGAIN;
	}

	pr_debug("fbi_base: fb_start_dma 0x%p fb_start 0x%p fb_size %d\n",
		(int *)fbi_base->fb_start_dma, fbi_base->fb_start,
		fbi_base->fb_size);
	pr_debug("fbi_dual: fb_start_dma 0x%p fb_start 0x%p fb_size %d\n",
		(int *)fbi_dual->fb_start_dma, fbi_dual->fb_start,
		fbi_dual->fb_size);

	switch (mode) {
	case 0:
		if (fb_mode) {
			/* turn off video layer */
			pxa168fb_ovly_dual(0);
			fb_mode = mode;
			fbi_dual->fb_start_dma = fbi_dual->fb_start_dma_bak;
			fbi_dual->fb_start = fbi_dual->fb_start_bak;
			fbi_dual->dma_on = 0;
			/* disable dual path graphics DMA */
			dma_ctrl_set(fb_dual, 0, CFG_GRA_ENA_MASK, 0);
			pxa168fb_set_par(info_dual);
			pxa168fb_set_par(info_base);
			if (vdma_switch) {
				pxa688fb_vdma_en(fb_base, 0, mi_base->vdma_enable);
				pxa688fb_vdma_en(fb_dual, 0, mi_dual->vdma_enable);
			}
		}
		break;
	case 1:
	case 2:
	case 3:
		if (fb_mode != mode) {
			fb_mode = mode;
			fbi_dual->fb_start_dma = fbi_base->fb_start_dma;
			fbi_dual->fb_start = fbi_base->fb_start;
			fbi_dual->dma_on = fbi_base->dma_on;
			pxa168fb_set_par(info_base);
			/* turn on video layer */
			pxa168fb_ovly_dual(1);
			if (vdma_switch)
				pxa688fb_vdma_en(fb_base, 0, 0);
			pxa688fb_vdma_en(fb_dual, 0, 1);
		}
		break;
	default:
		break;
		;
	}

	return 0;
}
/*************************************************************************/

#define VSYNC_CHECK_TIME	(10 * HZ)
static void vsync_check_timer(unsigned long data)
{
	int id = DEBUG_VSYNC_PATH(0);

	/* disable count interrupts */
	irq_mask_set(id, vsync_imask(id) | gf0_imask(id) |
		gf1_imask(id) | vf0_imask(id) | vf1_imask(id), 0);

	vsync_check = 0;
	del_timer(&vsync_timer);
	pr_info("fbi %d: irq_count %d\n", id, irq_count);
	pr_info("\tvsync_count %d\n",  vsync_count);
	pr_info("\tdispd_count %d\n",  dispd_count);
	pr_info("\tf0_count %d\n", f0_count);
	pr_info("\tf1_count %d\n", f1_count);
	pr_info("\tvf0_count %d\n", vf0_count);
	pr_info("\tvf1_count %d\n", vf1_count);
}

static struct proc_dir_entry *pxa168fb_proc;
static unsigned int proc_reg = 0;
static int pxa168fb_proc_write (struct file *file, const char *buffer,
                      unsigned long count, void *data)
{
	struct fb_info *info = gfx_info.fbi[0]->fb_info;
	struct pxa168fb_mach_info *mi = gfx_info.fbi[0]->dev->platform_data;
	struct dsi_info *di = mi->dsi;
	char kbuf[11], vol[11];
	int index, reg_val, i;
	u32 mask;

	if (count >= 12)
		return -EINVAL;
	if (copy_from_user(kbuf, buffer, count))
		return -EFAULT;

	if ('-' == kbuf[0]) {
		memcpy(vol, kbuf+1, count-1);
		proc_reg = (int) simple_strtoul(vol, NULL, 16);
		printk("reg @ 0x%x: 0x%x\n", proc_reg,
			__raw_readl(gfx_info.fbi[0]->reg_base + proc_reg));
		return count;
	} else if ('0' == kbuf[0] && 'x' == kbuf[1]) {
		/* set the register value */
		reg_val = (int)simple_strtoul(kbuf, NULL, 16);
		__raw_writel(reg_val, gfx_info.fbi[0]->reg_base + proc_reg);
		printk("set reg @ 0x%x: 0x%x\n", proc_reg,
			__raw_readl(gfx_info.fbi[0]->reg_base + proc_reg));
		return count;
	} else if ('l' == kbuf[0]) {
		for (i = 0; i < 0x300; i+=4) {
			if (!(i % 16) && i)
				printk("\n0x%3x: ", i);
			printk(" %8x", __raw_readl(gfx_info.fbi[0]->reg_base + i));
		}
		printk("\n");
		return count;
 	} else if ('d' == kbuf[0]) {
 		unsigned addr;
 		if (!di) {
 			printk("fb0 no dsi info\n");
 			return count;
 		}
 		addr = (unsigned)di->regs;
 		for (i = 0x0; i < 0x200; i+=4) {
 			if (!(i % 16))
 				printk("\n0x%3x: ", i);
 			printk(" %8x", __raw_readl(addr + i));
 		}
 		printk("\n");
 		return count;
	}

	index = (int)simple_strtoul(kbuf, NULL, 10);

	switch (index) {
	case 0:
	case 1:
	case 2:
	case 3:
		pxa168fb_mode_switch(index);
		break;

	case 4:
		memset(info->screen_base, 2, info->screen_size/2);
		memset(info->screen_base + info->screen_size/4, 8, info->screen_size/2);
		break;
	case 5:
#ifdef CONFIG_PM
		_pxa168fb_suspend(gfx_info.fbi[1]);
		_pxa168fb_suspend(gfx_info.fbi[0]);
#endif
		break;

	case 6:
#ifdef CONFIG_PM
		_pxa168fb_resume(gfx_info.fbi[0]);
		_pxa168fb_resume(gfx_info.fbi[1]);
#endif
		break;

	case 7:
		pxa168fb_gfx_dual(1);
		pxa168fb_set_par(gfx_info.fbi[fb_base]->fb_info);
		break;
	case 8:
		pxa168fb_gfx_dual(0);
		pxa168fb_set_par(gfx_info.fbi[fb_base]->fb_info);
		break;

	case 9:
		index = DEBUG_VSYNC_PATH(0);
		/* enable count interrupts */
		mask = vsync_imask(index) | gf0_imask(index) |
			gf1_imask(index) | vf0_imask(index) | vf1_imask(index);
		irq_mask_set(index, mask, mask);
		irq_status_clear(index, mask);

		init_timer(&vsync_timer);
		vsync_timer.function = vsync_check_timer;
		vsync_check = 1;
		vsync_count = 0;
		dispd_count = 0;
		irq_count = 0;
		f0_count = f1_count = 0;
		vf0_count = vf1_count = 0;
		mod_timer(&vsync_timer, jiffies + 10*HZ);
		break;

	default:
		return -EINVAL;
	}

    return count;
}

static int pxa168fb_proc_read (char *buffer, char **buffer_location, off_t offset,
                            int buffer_length, int *zero, void *ptr)
{

    if(offset > 0)
        return 0;

    return sprintf(buffer, "fb_mode %d reg @ 0x%x: 0x%x\n", fb_mode,
	proc_reg, __raw_readl(gfx_info.fbi[0]->reg_base + proc_reg));
}

extern void rvfree(void *mem, unsigned long size);



static int __devinit pxa168fb_probe(struct platform_device *pdev)
{
	struct pxa168fb_mach_info *mi;
	struct fb_info *info = 0;
	struct pxa168fb_info *fbi = 0;
	struct resource *res;
	struct clk *clk;
	int irq, irq_enable_mask, ret = 0;
	struct dsi_info *di;
	static int proc_inited;

	mi = pdev->dev.platform_data;
	if (mi == NULL) {
		dev_err(&pdev->dev, "no platform data defined\n");
		return -EINVAL;
	}
	/* if isr_clear_mask not initizlized, set to 0xffffffff as default */
	if (!mi->isr_clear_mask) {
		pr_err("pxa168fb: %s isr_clear_mask not been initialized!\n", mi->id);
		mi->isr_clear_mask = 0xffffffff;
	}

	clk = clk_get(&pdev->dev, NULL);
	if (IS_ERR(clk)) {
		dev_err(&pdev->dev, "unable to get LCDCLK");
		return PTR_ERR(clk);
	}

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (res == NULL) {
		dev_err(&pdev->dev, "no IO memory defined\n");
		return -ENOENT;
	}

	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		dev_err(&pdev->dev, "no IRQ defined\n");
		return -ENOENT;
	}

	info = framebuffer_alloc(sizeof(struct pxa168fb_info), &pdev->dev);
	if ((info == NULL) || (!info->par))
		return -ENOMEM;

	/* Initialize private data */
	fbi = info->par;
	fbi->id = pdev->id;
	fbi->update_addr = 1;
	fbi->check_modex_active = check_modex_active;
	fbi->update_buff = pxa168fb_update_buff;

	if (!fbi->id) {
		memset(&gfx_info, 0, sizeof(gfx_info));
		fbi->dma_on = 1;
	}

	gfx_info.fbi[fbi->id] = fbi;

#ifdef OVLY_TASKLET
	tasklet_init(&fbi->ovly_task, pxa168fb_ovly_task, (unsigned long)fbi);
#else
	fbi->system_work = 1;
	if (fbi->system_work)
		INIT_WORK(&fbi->buf_work, pxa168fb_ovly_work);
	else {
		fbi->work_q = create_singlethread_workqueue("pxa168fb-ovly");
		INIT_WORK(&fi->queue, pxa168fb_ovly_work);
	}
#endif

	di = mi->dsi;
	if (di) {
		printk("fb%d dsi %d di->lanes %d\n",
			fbi->id, di->id, di->lanes);
		if (di->id & 1)
			di->regs = (unsigned)ioremap_nocache(DSI1_REGS_PHYSICAL_BASE,
					sizeof(struct dsi_regs));
		else
			di->regs = (unsigned)ioremap_nocache(DSI2_REGS_PHYSICAL_BASE,
					sizeof(struct dsi_regs));
	} else
		printk("fb%d no dsi info\n", fbi->id);

	fbi->fb_info = info;
	platform_set_drvdata(pdev, fbi);
	fbi->check_modex_active = check_modex_active;
	fbi->clk = clk;
	fbi->dev = &pdev->dev;
	fbi->fb_info->dev = &pdev->dev;
	fbi->is_blanked = 0;
	fbi->active = mi->active;

	/* Initialize boot setting */
	fbi->dft_vmode.xres = mi->modes->xres;
	fbi->dft_vmode.yres = mi->modes->yres;
	fbi->dft_vmode.refresh = mi->modes->refresh;

	init_waitqueue_head(&fbi->w_intr_wq);
	mutex_init(&fbi->access_ok);

	/* Initialise static fb parameters */
	info->flags = FBINFO_DEFAULT | FBINFO_PARTIAL_PAN_OK |
			FBINFO_HWACCEL_XPAN | FBINFO_HWACCEL_YPAN;
	info->node = -1;
	strcpy(info->fix.id, mi->id);
	info->fix.type = FB_TYPE_PACKED_PIXELS;
	info->fix.type_aux = 0;
	info->fix.xpanstep = 1;
	info->fix.ypanstep = 1;
	info->fix.ywrapstep = 0;
	info->fix.mmio_start = res->start;
	info->fix.mmio_len = res->end - res->start + 1;
	info->fix.accel = FB_ACCEL_NONE;
	info->fbops = &pxa168fb_ops;
	info->pseudo_palette = fbi->pseudo_palette;

	/* Map LCD controller registers */
	fbi->reg_base = ioremap_nocache(res->start, res->end - res->start);
	if (fbi->reg_base == NULL) {
		ret = -ENOMEM;
		goto failed;
	}

	/* Allocate framebuffer memory */
	if (!fb_size_from_cmd) {
		if (mi->max_fb_size)
			max_fb_size = mi->max_fb_size;
		else
			max_fb_size = DEFAULT_FB_SIZE;
	}
	if (fb_share) {
		/* fb_share mode, allocate more memory as frame buffer */
		max_fb_size = max(max_fb_size, 2 * 4 * (mi->modes->xres *
				(mi->modes->xres + mi->modes->yres)));
	}

	max_fb_size = PAGE_ALIGN(max_fb_size);
	if (mi->mmap)
		fbi->fb_size = max_fb_size;

	if(fb_share && (fbi->id == 1) && gfx_info.fbi[0]->fb_start) {
		fbi->fb_size = gfx_info.fbi[0]->fb_size;
		fbi->fb_start = gfx_info.fbi[0]->fb_start;
		fbi->fb_start_dma = gfx_info.fbi[0]->fb_start_dma;
		printk("--share--FB DMA buffer phy addr : %x\n",
			(unsigned int)fbi->fb_start_dma);
	} else if (mi->mmap) {
		if(fbi->id==0&&frambuffer_base)
		{
				//use reserved 6M memory to support bigger resolution like 1024x600
				fbi->fb_start = ioremap (frambuffer_base, fbi->fb_size);
				fbi->fb_start_dma = frambuffer_base;
		}
		else
		{
			fbi->fb_start = dma_alloc_writecombine(fbi->dev, max_fb_size,
					&fbi->fb_start_dma, GFP_KERNEL);

			if (!fbi->fb_start || !fbi->fb_start_dma) {
				fbi->fb_start = (void *)__get_free_pages(GFP_DMA |
					GFP_KERNEL, get_order(fbi->fb_size));
				fbi->fb_start_dma =
					(dma_addr_t)__virt_to_phys(fbi->fb_start);
			}

			if (fbi->fb_start == NULL) {
				printk("%s: no enough memory!\n", __func__);
				ret = -ENOMEM;
				goto failed;
			}
			printk("---------FB DMA buffer phy addr : %x\n",
				(unsigned int)fbi->fb_start_dma);
		/* clear frame buffer */
		memset(fbi->fb_start, 0, fbi->fb_size);
		}
	}

	/* fill backup information for mode switch */
	fbi->fb_start_dma_bak = fbi->fb_start_dma;
	fbi->fb_start_bak = fbi->fb_start;

	info->fix.smem_start = fbi->fb_start_dma;
	info->fix.smem_len = fbi->fb_size;
	info->screen_base = fbi->fb_start;
	info->screen_size = fbi->fb_size;

	/* avoid system enter low power modes */
	set_dvfm_constraint();

	/* Set video mode according to platform data */
	set_mode(fbi, &info->var, mi->modes, mi->pix_fmt, 1);

	fb_videomode_to_modelist(mi->modes, mi->num_modes, &info->modelist);

	/* init video mode data */
	pxa168fb_init_mode(info, mi);

	/* enable controller clock */
	if (mi->sclk_src)
		clk_set_rate(fbi->clk, mi->sclk_src);
	clk_enable(fbi->clk);
	pr_info("fb%d: sclk_src %d clk_get_rate = %d\n", fbi->id,
		mi->sclk_src, (int)clk_get_rate(fbi->clk));

#ifdef CONFIG_PXA688_VDMA
	mi->immid = pxa688fb_imm_register("mmp2 dma");
	pxa688_vdma_clkset(1);
	if (mi->vdma_enable) {
		if (!mi->sram_paddr)
			mi->sram_paddr = pxa688fb_vdma_squ_malloc(
				&mi->sram_size, mi->immid);
		pr_debug("vdma enabled, sram_paddr 0x%x sram_size 0x%x\n",
			mi->sram_paddr, mi->sram_size);
	}
	fbi->vdma_enable = mi->vdma_enable;
#endif

#ifdef CONFIG_PXA688_MISC
	ret = device_create_file(&pdev->dev, &dev_attr_misc);
	if (ret < 0) {
		printk("device attr vsmooth create fail: %d\n", ret);
		return ret;
	}
#endif

	/* Fill in sane defaults */
	pxa168fb_set_default(fbi, mi);	/* FIXME */
	pxa168fb_set_par(info);

	/* Allocate color map */
	if (fb_alloc_cmap(&info->cmap, 256, 0) < 0) {
		ret = -ENOMEM;
		goto failed_free_clk;
	}

	/* Register irq handler */
	if (!fbi->id) {
		/* Clear the irq status before kernel startup */
		irq_status_clear(fbi->id, 0xFFFFFFFF);

		ret = request_irq(irq, pxa168fb_handle_irq, IRQF_DISABLED, mi->id, fbi);
		if (ret < 0) {
			dev_err(&pdev->dev, "unable to request IRQ\n");
			ret = -ENXIO;
			goto failed_free_cmap;
		}
	}

	/* disable GFX interrupt */
	irq_enable_mask = gf0_imask(fbi->id) | gf1_imask(fbi->id);
	irq_mask_set(fbi->id, irq_enable_mask, 0);
	fbi->wait_vsync = 1;

	/* enable display done interrupt */
	irq_mask_set(fbi->id, display_done_imask(fbi->id),
		display_done_imask(fbi->id));
	/*enable GFX err interrupt*/
	irq_mask_set(fbi->id, err_imasks, err_imasks);

	/* enable power supply */
	fbi->active = 0;
	pxa168fb_power(fbi, mi, 1);
	fbi->active = 1;

	/* initialize external phy if needed */
	if (mi->phy_init && mi->phy_init(fbi)) {
		pr_err("%s fbi %d phy error\n", __func__, fbi->id);
		goto failed_free_irq;
	}

	/* Register framebuffer */
	ret = register_framebuffer(info);
	if (ret < 0) {
		dev_err(&pdev->dev, "Failed to register pxa168-fb: %d\n", ret);
		ret = -ENXIO;
		goto failed_free_irq;
	}
	pr_info("pxa168fb: frame buffer device was loaded"
		" to /dev/fb%d <%s>.\n", info->node, info->fix.id);

#ifdef CONFIG_HAS_EARLYSUSPEND
	/* let TV path suspend first and resume late than panel path */
	fbi->early_suspend.level = EARLY_SUSPEND_LEVEL_DISABLE_FB - fbi->id;
	fbi->early_suspend.suspend = pxa168fb_early_suspend;
	fbi->early_suspend.resume = pxa168fb_late_resume;
        register_early_suspend(&fbi->early_suspend);
#endif

#ifdef CONFIG_PXA688_DSI
	if (mi->phy_type & (DSI2DPI | DSI)) {
		ret = device_create_file(&pdev->dev, &dev_attr_dsi);
		if (ret < 0) {
			printk("device attr create fail: %d\n", ret);
			return ret;
		}
	}
#endif

#ifdef CONFIG_PXA688_VDMA
	ret = device_create_file(&pdev->dev, &dev_attr_vdma);
	if (ret < 0) {
		pr_err("device attr create fail: %d\n", ret);
		return ret;
	}
#endif

	ret = device_create_file(&pdev->dev, &dev_attr_lcd);
	if (ret < 0) {
		pr_err("device attr lcd create fail: %d\n", ret);
		return ret;
	}

	ret = device_create_file(&pdev->dev, &dev_attr_vsync);
	if (ret < 0) {
		pr_err("device attr create fail: %d\n", ret);
		return ret;
	}

	if (!proc_inited) {
		proc_inited = 1;
		pxa168fb_proc = create_proc_entry ("pxa168fb" , 0666 , NULL);
		pxa168fb_proc->read_proc = pxa168fb_proc_read;
		pxa168fb_proc->write_proc = pxa168fb_proc_write;
	}

	return 0;

failed_free_irq:
	free_irq(irq, fbi);
failed_free_cmap:
	fb_dealloc_cmap(&info->cmap);
failed_free_clk:
	clk_disable(fbi->clk);
	clk_put(fbi->clk);
failed:
	pr_err("pxa168-fb: frame buffer device init failed\n");
	platform_set_drvdata(pdev, NULL);
	fb_dealloc_cmap(&info->cmap);

	if (fbi && fbi->reg_base) {
		iounmap(fbi->reg_base);
		kfree(fbi);
	}

	unset_dvfm_constraint();
	return ret;
}

static struct platform_driver pxa168fb_driver = {
	.driver		= {
		.name	= "pxa168-fb",
		.owner	= THIS_MODULE,
	},
	.probe		= pxa168fb_probe,
#ifdef CONFIG_PM
#ifndef CONFIG_HAS_EARLYSUSPEND
	.suspend	= pxa168fb_suspend,
	.resume		= pxa168fb_resume,
#endif
#endif
};

static int __devinit pxa168fb_init(void)
{
#ifdef CONFIG_DVFM
	dvfm_register("LCD Base", &dvfm_dev_idx);
#endif
	return platform_driver_register(&pxa168fb_driver);
}
module_init(pxa168fb_init);

MODULE_AUTHOR("Lennert Buytenhek <buytenh@marvell.com>");
MODULE_DESCRIPTION("Framebuffer driver for PXA168");
MODULE_LICENSE("GPL");
