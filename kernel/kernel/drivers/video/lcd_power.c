#include <asm/gpio.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <mach/mfp-mmp2.h>

#include <mach/lcd_power.h>

static int lcd_power = -1;
static int lcd_reset = -1;
static int spi_sdi = -1;
static int spi_sdo = -1;
static int spi_sclk = -1;
static int spi_cs = -1;

#define LCD_IO_ENABLE 0
#define LCD_IO_DISABLE 1
#define ARRAY_AND_SIZE(x)    (x), ARRAY_SIZE(x)


static 
void lcd_fun_set(int fun)
{
	if(fun){}
}

static void spi_addr(unsigned short addr)
{
    unsigned int i;
    int j;

    //printk(KERN_ERR"addr - %02x\n", addr);
    gpio_direction_output(spi_cs, 0);    
    gpio_direction_output(spi_sclk, 1);    
    
    i = addr | 0x7000;
    for (j = 0; j < 9; j++) {
        gpio_direction_output(spi_sclk, 0);    
        //udelay(2);

        if (i & 0x0100)
            gpio_direction_output(spi_sdo, 1);    
        else
            gpio_direction_output(spi_sdo, 0);    

        udelay(3);
        gpio_direction_output(spi_sclk, 1);    
        udelay(3);
        i <<= 1;
    }

    gpio_direction_output(spi_cs, 1);    
}

static void spi_data(unsigned short data)
{
    unsigned int i;
    int j;

    //printk(KERN_ERR"\tdata - %02x\n", data);
    gpio_direction_output(spi_cs, 0);    
    gpio_direction_output(spi_sclk, 1);    

    i = data | 0x7100;

    for (j = 0; j < 9; j++) {
        gpio_direction_output(spi_sclk, 0);
        //udelay(2);
        
        if (i & 0x0100)
            gpio_direction_output(spi_sdo, 1);    
        else
            gpio_direction_output(spi_sdo, 0);    

        udelay(3);
        gpio_direction_output(spi_sclk, 1);    
        udelay(3);
        i <<= 1;
    }

    gpio_direction_output(spi_cs, 1);   
}

static void init_mcu(void)
{
    spi_addr(0xB9);  // SET password
    spi_data(0xFF);
    spi_data(0x83);
    spi_data(0x69);

    spi_addr(0xB1);  //Set Power
    spi_data(0x85);
    spi_data(0x00);
    spi_data(0x34);
    spi_data(0x06);
    spi_data(0x00);
    spi_data(0x10);
    spi_data(0x10);
    spi_data(0x2F);
    spi_data(0x3A);
    spi_data(0x3F);
    spi_data(0x3F);
    spi_data(0x07);
    spi_data(0x23); //23
    spi_data(0x01);
    spi_data(0xE6);
    spi_data(0xE6);
    spi_data(0xE6);
    spi_data(0xE6);
    spi_data(0xE6);

    spi_addr(0xB2);  // SET Display  480x800
    spi_data(0x00);
    spi_data(0x2B);
    spi_data(0x08);
    spi_data(0x2A);
    spi_data(0x70);
    spi_data(0x00);
    spi_data(0xFF);
    spi_data(0x00);
    spi_data(0x00);
    spi_data(0x00);
    spi_data(0x00);
    spi_data(0x03);
    spi_data(0x03);
    spi_data(0x00);
    spi_data(0x01);

    //spi_addr(0x2C);

    spi_addr(0xB3);
    spi_data(0x0F);		//DPL=1,HSPL=1,VSPL=1,EPL=1
    
    //spi_addr(0xB0);
    //spi_data(0x01);
    //spi_data(0x0b);

    spi_addr(0xB4);  	// SET Display  480x800
    spi_data(0x00);
    spi_data(0x0C);   	//0C
    spi_data(0xA0); 	//A0
    spi_data(0x0E);  	// 0E
    spi_data(0x06);   	//06

    spi_addr(0xB6);  	// SET VCOM
    spi_data(0x31);
    spi_data(0x31);

    spi_addr(0xD5);
    spi_data(0x00);
    spi_data(0x05);
    spi_data(0x03);
    spi_data(0x00);
    spi_data(0x01);
    spi_data(0x09);
    spi_data(0x10);
    spi_data(0x80);
    spi_data(0x37);
    spi_data(0x37);
    spi_data(0x20);
    spi_data(0x31);
    spi_data(0x46);
    spi_data(0x8A);
    spi_data(0x57);
    spi_data(0x9B);
    spi_data(0x20);
    spi_data(0x31);
    spi_data(0x46);
    spi_data(0x8A);
    spi_data(0x57);
    spi_data(0x9B);
    spi_data(0x07);
    spi_data(0x0F);
    spi_data(0x02);
    spi_data(0x00);

    spi_addr(0xE0);
    spi_data(0x01);
    spi_data(0x08);
    spi_data(0x0D);
    spi_data(0x2D);
    spi_data(0x34);
    spi_data(0x37);
    spi_data(0x21);
    spi_data(0x3C);
    spi_data(0x0E);
    spi_data(0x10);
    spi_data(0x0E);
    spi_data(0x12);
    spi_data(0x14);
    spi_data(0x12);
    spi_data(0x14);
    spi_data(0x13);
    spi_data(0x19);

    spi_data(0x01);
    spi_data(0x08);
    spi_data(0x0D);
    spi_data(0x2D);
    spi_data(0x34);
    spi_data(0x37);
    spi_data(0x21);
    spi_data(0x3C);
    spi_data(0x0E);
    spi_data(0x10);
    spi_data(0x0E);
    spi_data(0x12);
    spi_data(0x14);
    spi_data(0x12);
    spi_data(0x14);
    spi_data(0x13);
    spi_data(0x19);

    spi_addr(0x36);  //Set_address_mode
    spi_data(0x10); //����X�᾵��
    spi_addr(0x2C);

    spi_addr(0xCC); //Set_address_mode
    spi_data(0x02);  //SS,REV,BGR  0x0A
    spi_addr(0x2C);

    spi_addr(0x3A);   //Set_pixel_format
    spi_data(0x66);

    spi_addr(0x11);
    msleep(120);

    spi_addr(0x29);

    spi_addr(0x2C);
}

static void lcd_init(void)
{
    lcd_fun_set(LCD_IO_ENABLE);
//    mdelay(100);
    init_mcu();
    lcd_fun_set(LCD_IO_DISABLE);
    printk(KERN_ERR"lcd init done!\n");
}

void lcd_power_set(int on)
{
    int gpio;
    int ret;

    gpio = lcd_power;
    ret = gpio_request(gpio, "lcd_power");
    if (ret) printk("failed requesting %d\n", gpio);

    gpio = spi_sdo;
    ret = gpio_request(gpio, "lcd_sdo");
    if (ret) printk("failed requesting %d\n", gpio);

    gpio = spi_cs;
    ret = gpio_request(gpio, "lcd_cs");
    if (ret) printk("failed requesting %d\n", gpio);

    gpio = spi_sclk;
    ret = gpio_request(gpio, "lcd_sclk");
    if (ret) printk("failed requesting %d\n", gpio);

    gpio_direction_output(lcd_power, !on);

    if(-1!=lcd_reset){        
        gpio_request(lcd_reset, "lcd_reset");
        gpio_direction_output(lcd_reset, 0);
    }

    if (on)
    {
        
        if(-1!=lcd_reset){        
            msleep(50);
            gpio_direction_output(lcd_reset, 1);
            msleep(10);
        }
        lcd_init();
	//	lcd_init();
    }
    else
    {
    	;
//        gpio_direction_output(spi_sdo, 0);
//        gpio_direction_output(spi_sclk, 0);
//        gpio_direction_output(spi_cs, 0);
    }

    gpio_free(lcd_power);
    gpio_free(spi_sdo);
    gpio_free(spi_sclk);
    gpio_free(spi_cs);
    
    if(-1!=lcd_reset)
        gpio_free(lcd_reset);
}

EXPORT_SYMBOL(lcd_power_set);

static ssize_t pwr_set(struct device *dev,
    struct device_attribute *attr, const char *buf, size_t count)
{
    lcd_power_set(1);
    return count;
}

static struct device_attribute lcd_power_static_attrs[] = {
    __ATTR(pwr, 0666, NULL, pwr_set),
};


static int lcd_power_probe(struct platform_device *pdev)
{
    int ret;
    
    struct lcd_power_platform_data* pdata = pdev->dev.platform_data;
    if (pdata == NULL)
    {
        printk("Invalid data for spiemu. Quit.\n");
        return 0;
    }

    lcd_power = pdata->lcd_power;
    lcd_reset = pdata->lcd_reset;
    spi_sdi = pdata->spi_sdi;
    spi_sdo = pdata->spi_sdo;
    spi_sclk = pdata->spi_clk;
    spi_cs = pdata->spi_cs;
    

    ret = device_create_file(&pdev->dev, &lcd_power_static_attrs[0]);
    if (ret)
        printk("failed create attribute file for lcd_power\n");
    
    return 0;
}

static struct platform_driver this_driver = {
    .probe  = lcd_power_probe,
    .driver = {
        .name   = "lcd_power",
    },
};

static int __init lcd_power_init(void)
{
    int ret;

    ret = platform_driver_register(&this_driver);

    return ret;
}

postcore_initcall(lcd_power_init);
