/*
 * linux/drivers/video/pxa168fb_ovly.c -- Marvell PXA168 LCD Controller
 *
 * Copyright (C) Marvell Semiconductor Company.  All rights reserved.
 *
 * 2009-03-19   adapted from original version for PXA168
 *		Green Wan <gwan@marvell.com>
 *		Kevin Liu <kliu5@marvell.com>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

/*
 * 1. Adapted from:  linux/drivers/video/skeletonfb.c
 * 2. Merged from: linux/drivers/video/dovefb.c (Lennert Buytenhek)
 */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/interrupt.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/cpufreq.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/uaccess.h>
#include <linux/console.h>
#include <linux/timer.h>

#define OVLY_TASKLET
#include "pxa168fb_common.h"
#include <asm/io.h>
#include <asm/irq.h>

//#define OVLY_DVFM_CONSTRAINT
#undef OVLY_DVFM_CONSTRAINT
#ifdef OVLY_DVFM_CONSTRAINT
static int dvfm_dev_idx;
#ifdef CONFIG_DVFM
#include <mach/dvfm.h>
static void set_dvfm_constraint(void)
{
	dvfm_disable(dvfm_dev_idx);
}

static void unset_dvfm_constraint(void)
{
	dvfm_enable(dvfm_dev_idx);
}
#else
static void set_dvfm_constraint(void) {}
static void unset_dvfm_constraint(void) {}
static void dvfm_register(char *name, int *id) {}
#endif
#endif

#define RESET_BUF	0x1
#define FREE_ENTRY	0x2

static int pxa168fb_set_par(struct fb_info *fi);
static void set_video_start(struct fb_info *fi, int xoffset,
		int yoffset, int dual_wait, int dual_trigger);
static void set_dma_control0(struct pxa168fb_info *fbi);
static int pxa168fb_pan_display(struct fb_var_screeninfo *var, struct fb_info *fi);

/* buffer management:
 *    filterBufList: list return to upper layer which indicates buff is free
 *    freelist: list indicates buff is free
 *    waitlist: wait queue which indicates "using" buffer, will be writen in
 *              DMA register
 *    current: buffer on showing
 * Operation:
 *    flip: if !waitlist[0] || !waitlist[1] enqueue to waiting list;
 *          else enqueue the  waitlist[0] to freelist, new buf to waitlist
 *    get freelist: return freelist
 *    eof intr: enqueue current to freelist; dequeue waitlist[0] to current;
 *    buffers are protected spin_lock_irq disable/enable
 *    suspend: when lcd is suspend, move all buffers as "switched",
 *             but don't really set hw register.
 */

static unsigned int max_fb_size = 0;
static unsigned int fb_size_from_cmd = 0;

/* Compatibility mode global switch .....
 *
 * This is a secret switch for user space programs that may want to
 * select color spaces and set overlay position through the nonstd
 * element of fb_var_screeninfo This is the way legacy PXA display
 * drivers were used. The mode reverts back to normal mode on driver release.
 *
 * To turn on compatibility with older PXA, set the MSB of nonstd to 0xAA.
 */

static unsigned int COMPAT_MODE;

static struct _sViewPortInfo gViewPortInfo = {
	.srcWidth = 640,	/* video source size */
	.srcHeight = 480,
	.zoomXSize = 640,	/* size after zooming */
	.zoomYSize = 480,
};

static struct _sViewPortOffset gViewPortOffset = {
	.xOffset = 0,	/* position on screen */
	.yOffset = 0
};

/* mirror mode related info/flags */
struct fbi_info ovly_info;

#define xp		printk("%s, line %d\n", __func__, __LINE__)

#ifdef FB_PM_DEBUG
static unsigned int g_regs[1024];
static unsigned int g_regs1[1024];
static unsigned int pxa168fb_rw_all_regs(struct pxa168fb_info *fbi,
		unsigned int *regs, int is_read)
{
	u32 i;
	u32 reg;

	for (i = 0xC0; i <= 0x01C4; i += 4) {
		if (is_read) {
			reg = readl(fbi->reg_base + i);
			regs[i] = reg;
		} else {
			writel(regs[i], fbi->reg_base + i);
		}
	}

	return 0;
}
#endif

#if 0
static struct fb_videomode *
find_best_mode(struct pxa168fb_info *fbi, struct fb_var_screeninfo *var)
{
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	struct fb_videomode *best_mode;
	int i;

	dev_dbg(fbi->fb_info->dev, "Enter %s\n", __FUNCTION__);
	best_mode = NULL;
	for (i = 0; i < mi->num_modes; i++) {
		struct fb_videomode *m = mi->modes + i;

		/*
		 * Check whether this mode is suitable.
		 */
		if (var->xres > m->xres)
			continue;
		if (var->yres > m->yres)
			continue;

		/*
		 * Check whether this mode is more suitable than
		 * the best mode so far.
		 */
		if (best_mode != NULL &&
		    (best_mode->xres < m->xres ||
		     best_mode->yres < m->yres ||
		     best_mode->pixclock > m->pixclock))
			continue;

		best_mode = m;
	}

	return best_mode;
}
#endif

static int determine_best_pix_fmt(struct fb_var_screeninfo *var)
{
	unsigned char pxa_format;

	/* compatibility switch: if var->nonstd MSB is 0xAA then skip to
	 * using the nonstd variable to select the color space.
	 */
	if(COMPAT_MODE != 0x2625) {

		/*
		 * Pseudocolor mode?
		 */
		if (var->bits_per_pixel == 8)
			return PIX_FMT_PSEUDOCOLOR;
		/*
		 * Check for YUV422PACK.
		 */
		if (var->bits_per_pixel == 16 && var->red.length == 16 &&
		    var->green.length == 16 && var->blue.length == 16) {
			if (var->red.offset >= var->blue.offset) {
				if (var->red.offset == 4)
					return PIX_FMT_YUV422PACK;
				else
					return PIX_FMT_YUYV422PACK;
			} else
				return PIX_FMT_YVU422PACK;
		}
		/*
		 * Check for YUV422PLANAR.
		 */
		if (var->bits_per_pixel == 16 && var->red.length == 8 &&
		    var->green.length == 4 && var->blue.length == 4) {
			if (var->red.offset >= var->blue.offset)
				return PIX_FMT_YUV422PLANAR;
			else
				return PIX_FMT_YVU422PLANAR;
		}

		/*
		 * Check for YUV420PLANAR.
		 */
		if (var->bits_per_pixel == 12 && var->red.length == 8 &&
		    var->green.length == 2 && var->blue.length == 2) {
			if (var->red.offset >= var->blue.offset)
				return PIX_FMT_YUV420PLANAR;
			else
				return PIX_FMT_YVU420PLANAR;
		}
		/*
		 * Check for 565/1555.
		 */
		if (var->bits_per_pixel == 16 && var->red.length <= 5 &&
		    var->green.length <= 6 && var->blue.length <= 5) {
			if (var->transp.length == 0) {
				if (var->red.offset >= var->blue.offset)
					return PIX_FMT_RGB565;
				else
					return PIX_FMT_BGR565;
			}

			if (var->transp.length == 1 && var->green.length <= 5) {
				if (var->red.offset >= var->blue.offset)
					return PIX_FMT_RGB1555;
				else
					return PIX_FMT_BGR1555;
			}

			/* fall through */
		}

		/*
		 * Check for 888/A888.
		 */
		if (var->bits_per_pixel <= 32 && var->red.length <= 8 &&
		    var->green.length <= 8 && var->blue.length <= 8) {
			if (var->bits_per_pixel == 24 && var->transp.length == 0) {
				if (var->red.offset >= var->blue.offset)
					return PIX_FMT_RGB888PACK;
				else
					return PIX_FMT_BGR888PACK;
			}

			if (var->bits_per_pixel == 32 && var->transp.offset == 24) {
				if (var->red.offset >= var->blue.offset)
					return PIX_FMT_RGBA888;
				else
					return PIX_FMT_BGRA888;
			} else {
				if (var->transp.length == 8) {
					if (var->red.offset >= var->blue.offset)
						return PIX_FMT_RGB888UNPACK;
					else
						return PIX_FMT_BGR888UNPACK;
				} else
					return PIX_FMT_YUV422PACK_IRE_90_270;

			}
			/* fall through */
		}
	} else {

		pxa_format = (var->nonstd >> 20) & 0xf;

		switch (pxa_format) {
		case 0:
			return PIX_FMT_RGB565;
			break;
		case 3:
			return PIX_FMT_YUV422PLANAR;
			break;
		case 4:
			return PIX_FMT_YUV420PLANAR;
			break;
		case 5:
			return PIX_FMT_RGB1555;
			break;
		case 6:
			return PIX_FMT_RGB888PACK;
			break;
		case 7:
			return PIX_FMT_RGB888UNPACK;
			break;
		case 8:
			return PIX_FMT_RGBA888;
			break;
		case 9:
			return PIX_FMT_YUV422PACK;
			break;

		default:
			return -EINVAL;
		}
	}



	return -EINVAL;
}

static void pxa168_sync_colorkey_structures(struct pxa168fb_info *fbi, int direction)
{
	struct _sColorKeyNAlpha *colorkey = &fbi->ckey_alpha;
	struct pxa168_fb_chroma *chroma = &fbi->chroma;
	unsigned int temp;

	dev_dbg(fbi->fb_info->dev, "ENTER %s\n", __FUNCTION__);
	if (direction == FB_SYNC_COLORKEY_TO_CHROMA) {
		chroma->mode        = colorkey->mode;
		chroma->y_alpha     = (colorkey->Y_ColorAlpha) & 0xff;
		chroma->y           = (colorkey->Y_ColorAlpha >> 8) & 0xff;
		chroma->y1          = (colorkey->Y_ColorAlpha >> 16) & 0xff;
		chroma->y2          = (colorkey->Y_ColorAlpha >> 24) & 0xff;

		chroma->u_alpha     = (colorkey->U_ColorAlpha) & 0xff;
		chroma->u           = (colorkey->U_ColorAlpha >> 8) & 0xff;
		chroma->u1          = (colorkey->U_ColorAlpha >> 16) & 0xff;
		chroma->u2          = (colorkey->U_ColorAlpha >> 24) & 0xff;

		chroma->v_alpha     = (colorkey->V_ColorAlpha) & 0xff;
		chroma->v           = (colorkey->V_ColorAlpha >> 8) & 0xff;
		chroma->v1          = (colorkey->V_ColorAlpha >> 16) & 0xff;
		chroma->v2          = (colorkey->V_ColorAlpha >> 24) & 0xff;
	}


	if (direction == FB_SYNC_CHROMA_TO_COLORKEY) {

		colorkey->mode = chroma->mode;
		temp = chroma->y_alpha;
		temp |= chroma->y << 8;
		temp |= chroma->y1 << 16;
		temp |= chroma->y2 << 24;
		colorkey->Y_ColorAlpha = temp;

		temp = chroma->u_alpha;
		temp |= chroma->u << 8;
		temp |= chroma->u1 << 16;
		temp |= chroma->u2 << 24;
		colorkey->U_ColorAlpha = temp;

		temp = chroma->v_alpha;
		temp |= chroma->v << 8;
		temp |= chroma->v1 << 16;
		temp |= chroma->v2 << 24;
		colorkey->V_ColorAlpha = temp;

	}
}

static u32 pxa168fb_ovly_set_colorkeyalpha(struct pxa168fb_info *fbi)
{
	struct _sColorKeyNAlpha color_a = fbi->ckey_alpha;
	unsigned int rb, x, layer, dma0, shift, r, b;
	struct pxa168fb_mach_info *mi;
	struct lcd_regs *regs;

	dev_dbg(fbi->fb_info->dev, "Enter %s\n", __FUNCTION__);
again:
	mi = fbi->dev->platform_data;
	regs = get_regs(fbi->id);
	dma0 = dma_ctrl_read(fbi->id, 0);
	shift = fbi->id ? 20 : 18;
	rb = layer = 0;
	r = color_a.Y_ColorAlpha;
	b = color_a.V_ColorAlpha;

	/* reset to 0x0 to disable color key. */
	x = dma_ctrl_read(fbi->id, 1) & ~(CFG_COLOR_KEY_MASK |
			CFG_ALPHA_MODE_MASK | CFG_ALPHA_MASK);

	/* switch to color key mode */
	switch (color_a.mode) {
	case FB_DISABLE_COLORKEY_MODE:
		/* do nothing */
		break;
	case FB_ENABLE_Y_COLORKEY_MODE:
		x |= CFG_COLOR_KEY_MODE(0x1);
		break;
	case FB_ENABLE_U_COLORKEY_MODE:
		x |= CFG_COLOR_KEY_MODE(0x2);
		break;
	case FB_ENABLE_V_COLORKEY_MODE:
		x |= CFG_COLOR_KEY_MODE(0x4);
		pr_info("V colorkey not supported, Chroma key instead\n");
		break;
	case FB_ENABLE_RGB_COLORKEY_MODE:
		x |= CFG_COLOR_KEY_MODE(0x3);
		rb = 1;
		break;
	case FB_ENABLE_R_COLORKEY_MODE:
		x |= CFG_COLOR_KEY_MODE(0x1);
		rb = 1;
		break;
	case FB_ENABLE_G_COLORKEY_MODE:
		x |= CFG_COLOR_KEY_MODE(0x6);
		pr_info("G colorkey not supported, Luma key instead\n");
		break;
	case FB_ENABLE_B_COLORKEY_MODE:
		x |= CFG_COLOR_KEY_MODE(0x7);
		rb = 1;
		break;
	default:
		printk(KERN_INFO "unknown mode");
		return -1;
	}


	/* switch to alpha path selection */
	switch (color_a.alphapath) {
	case FB_VID_PATH_ALPHA:
		x |= CFG_ALPHA_MODE(0x0);
		layer = CFG_CKEY_DMA;
		if (rb)
			rb = ((dma0 & CFG_DMA_SWAPRB_MASK)>> 4) ^
				(mi->panel_rbswap);
		break;
	case FB_GRA_PATH_ALPHA:
		x |= CFG_ALPHA_MODE(0x1);
		layer = CFG_CKEY_GRA;
		if(rb)
			rb = ((dma0 & CFG_GRA_SWAPRB_MASK)>> 12) ^
				(mi->panel_rbswap);
		break;
	case FB_CONFIG_ALPHA:
		x |= CFG_ALPHA_MODE(0x2);
		rb = 0;
		break;
	default:
		printk(KERN_INFO "unknown alpha path");
		return -1;
	}

	/* check whether DMA turn on RB swap for this pixelformat. */
	if (rb) {
		if (color_a.mode == FB_ENABLE_R_COLORKEY_MODE) {
			x &= ~CFG_COLOR_KEY_MODE(0x1);
			x |= CFG_COLOR_KEY_MODE(0x7);
		}

		if (color_a.mode == FB_ENABLE_B_COLORKEY_MODE) {
			x &= ~CFG_COLOR_KEY_MODE(0x7);
			x |= CFG_COLOR_KEY_MODE(0x1);
		}

		/* exchange r b fields. */
		r = color_a.V_ColorAlpha;
		b = color_a.Y_ColorAlpha;

		/* only alpha_Y take effect, switch back from V */
		if (color_a.mode == FB_ENABLE_RGB_COLORKEY_MODE) {
			r &= 0xffffff00;
			r |= (color_a.Y_ColorAlpha & 0xff);
		}
	}

	/* configure alpha */
	x |= CFG_ALPHA((color_a.config & 0xff));
	dma_ctrl_write(fbi->id, 1, x);
	writel(r, &regs->v_colorkey_y);
	writel(color_a.U_ColorAlpha, &regs->v_colorkey_u);
	writel(b, &regs->v_colorkey_v);

	if (fbi->id != 2) {
		/* enable DMA colorkey on graphics/video layer in panel/TV path */
		x = readl(fbi->reg_base + LCD_TV_CTRL1);
		x &= ~(3<<shift); x |= layer<<shift;
		writel(x, fbi->reg_base + LCD_TV_CTRL1);
	}

	/* color key / alpha configured flag */
	fbi->ckalpha_set = 1;

	if (FB_MODE_DUP) {
		fbi = ovly_info.fbi[fb_dual];
		/* enable color key on TV path in mirror mode*/
		color_a.mode = FB_ENABLE_RGB_COLORKEY_MODE;
		goto again;

#if 0
		if (debug_ck) {
			printk("\n\n   fb1 dma1 0x%x\n\n", dma_ctrl_read(fbi, 1));
			printk(" fb1 rb 0x%x colorkey @ 0x%p y: 0x%x; u: 0x%x; v: 0x%x\n\n",
				rb, &regs->v_colorkey_y, readl(&regs->v_colorkey_y),
				readl(&regs->v_colorkey_u), readl(&regs->v_colorkey_v));
		}
#endif
	}

	return 0;
}

/* check surface info expect buffer addr
 *  When other parameters other then buffer address change in mirror mode,
 *  TV path registers are configured in panel path display done irq, while
 *  buffer address is still called in flip ioctl. This would cause screen
 *  abormal (some white lines) for one frame if TV path irq comes between
 *  flip ioctl & panel irq. Warkaround this issue by delay tv path registers
 *  set until next tv path irq happens after panel path irq.
 */
static int check_surface_info(struct fb_info *fi,
			struct _sOvlySurface *surface)
{
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)fi->par;

	if (memcmp(&fbi->surface.viewPortInfo, &surface->viewPortInfo,
				sizeof(struct _sViewPortInfo))) {
		if (fbi->debug == 8)
			printk("%s: viewport info changed\n", __func__);
		return 1;
	}

	if (memcmp(&fbi->surface.viewPortOffset, &surface->viewPortOffset,
				sizeof(struct _sViewPortOffset))) {
		if (fbi->debug == 8)
			printk("%s: viewport offset changed\n", __func__);
		return 1;
	}

	if (fbi->surface.videoMode != surface->videoMode) {
		if (fbi->debug == 8)
			printk("%s: video mode: %d -> %d\n", __func__,
				fbi->surface.videoMode, surface->videoMode);
		return 1;
	}

	if (memcmp(&fbi->surface.dualInfo, &surface->dualInfo,
				sizeof(struct _sDualInfo) -
				sizeof(struct _sVideoBufferAddr))) {
		if (fbi->debug == 8)
			printk("%s: dual info changed\n", __func__);
		return 1;
	}

	return 0;
}

static int pxa168fb_update_buff(struct fb_info *fi,
	struct _sOvlySurface *surface, int addr)
{
	int dual_wait = check_surface_info(fi, surface);

	if (addr) {
		/* update buffer address only if changed */
		if (check_surface_addr(fi, surface, 1))
			set_video_start(fi, 0, 0, dual_wait, 0);
		else
			return -EINVAL;
	} else if (check_surface(fi, surface->videoMode,
					&surface->viewPortInfo,
					&surface->viewPortOffset,
					&surface->videoBufferAddr,
					&surface->dualInfo, 1))
		/* update other parameters other than buf addr */
		return pxa168fb_set_par(fi);

	return 0;
}

static void pxa168fb_clear_framebuffer(struct fb_info *fi)
{
	struct pxa168fb_info *fbi = fi->par;

	memset(fbi->fb_start, 0, fbi->fb_size);
}

static int check_modex_active(int id, int on)
{
	int active = on & ovly_info.fbi[id]->dma_on;

	/* mode2, base off, dual on */
	if ((fb_mode == 2) && (id == fb_base))
		active = 0;

	/* mode3, dual off, base on */
	if ((fb_mode == 3) && (id == fb_dual))
		active = 0;

	pr_debug("%s fb_mode %d fbi[%d] on %d active %d dma_on %d\n",
		__func__, fb_mode, id, on, active, ovly_info.fbi[id]->dma_on);
	return active;
}

#ifdef CONFIG_DYNAMIC_PRINTK_DEBUG
static void debug_identify_called_ioctl(struct fb_info *fi, int cmd, unsigned long arg)
{
	switch (cmd) {
	case FB_IOCTL_CLEAR_FRAMEBUFFER:
		dev_dbg(fi->dev," FB_IOCTL_CLEAR_FRAMEBUFFER\n");
		break;
	case FB_IOCTL_WAIT_VSYNC:
		dev_dbg(fi->dev," FB_IOCTL_WAIT_VSYNC\n");
		break;
	case FB_IOCTL_GET_VIEWPORT_INFO:
		dev_dbg(fi->dev," FB_IOCTL_GET_VIEWPORT_INFO with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_SET_VIEWPORT_INFO:
		dev_dbg(fi->dev," FB_IOCTL_SET_VIEWPORT_INFO with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_SET_VIDEO_MODE:
		dev_dbg(fi->dev," FB_IOCTL_SET_VIDEO_MODE with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_GET_VIDEO_MODE:
		dev_dbg(fi->dev," FB_IOCTL_GET_VIDEO_MODE with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_FLIP_VID_BUFFER:
		dev_dbg(fi->dev," FB_IOCTL_FLIP_VID_BUFFER with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_GET_FREELIST:
		dev_dbg(fi->dev," FB_IOCTL_GET_FREELIST with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_GET_BUFF_ADDR:
		dev_dbg(fi->dev," FB_IOCTL_GET_BUFF_ADDR with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_SET_VID_OFFSET:
		dev_dbg(fi->dev," FB_IOCTL_SET_VID_OFFSET with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_GET_VID_OFFSET:
		dev_dbg(fi->dev," FB_IOCTL_GET_VID_OFFSET with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_SET_MEMORY_TOGGLE:
		dev_dbg(fi->dev," FB_IOCTL_SET_MEMORY_TOGGLE with arg = %08x\n",(unsigned int) arg);
		break;
	case FB_IOCTL_SET_COLORKEYnALPHA:
		dev_dbg(fi->dev," FB_IOCTL_SET_COLORKEYnALPHA with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_GET_CHROMAKEYS:
		dev_dbg(fi->dev," FB_IOCTL_GET_CHROMAKEYS with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_PUT_CHROMAKEYS:
		dev_dbg(fi->dev," FB_IOCTL_PUT_CHROMAKEYS with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_GET_COLORKEYnALPHA:
		dev_dbg(fi->dev," FB_IOCTL_GET_COLORKEYnALPHA with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_SWITCH_VID_OVLY:
		dev_dbg(fi->dev," FB_IOCTL_SWITCH_VID_OVLY with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_SWITCH_GRA_OVLY:
		dev_dbg(fi->dev," FB_IOCTL_SWITCH_GRA_OVLY with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_SWAP_VIDEO_RED_BLUE:
		dev_dbg(fi->dev," FB_IOCTL_PUT_SWAP_VIDEO_RED_BLUE with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_SWAP_VIDEO_U_V:
		dev_dbg(fi->dev," FB_IOCTL_PUT_SWAP_VIDEO_U_V with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_SWAP_VIDEO_Y_UV:
		dev_dbg(fi->dev," FB_IOCTL_PUT_SWAP_VIDEO_Y_UV with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_PUT_VIDEO_ALPHABLEND:
		dev_dbg(fi->dev," FB_IOCTL_PUT_VIDEO_ALPHABLEND with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_PUT_GLOBAL_ALPHABLEND:
		dev_dbg(fi->dev," FB_IOCTL_PUT_GLOBAL_ALPHABLEND with arg = %08x\n", (unsigned int)arg);
		break;
	case FB_IOCTL_PUT_GRAPHIC_ALPHABLEND:
		dev_dbg(fi->dev," FB_IOCTL_PUT_GRAPHIC_ALPHABLEND with arg = %08x\n", (unsigned int)arg);
		break;

	}
}
#endif

static int pxa168fb_ovly_ioctl(struct fb_info *fi, unsigned int cmd,
			unsigned long arg)
{
	void __user *argp = (void __user *)arg;
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)fi->par;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	int vmode = 0;
	int vid_on = 1;
	int val = 0, mask = 0;
	unsigned char param;
	int blendval = 0;
	int res,tmp;
	int ret = 0;
	unsigned long flags;

#ifdef CONFIG_DYNAMIC_PRINTK_DEBUG
	debug_identify_called_ioctl(fi, cmd, arg);
#endif

	switch (cmd) {
	case FB_IOCTL_CLEAR_FRAMEBUFFER:
		if (!mi->mmap)
			return -EINVAL;
		pxa168fb_clear_framebuffer(fi);
		return 0;
		break;
	case FB_IOCTL_WAIT_VSYNC:
		if (NEED_VSYNC(fbi, 1))
			wait_for_vsync(fbi, 1);
		break;
	case FB_IOCTL_GET_VIEWPORT_INFO:/*if rotate 90/270, w/h swap*/
		mutex_lock(&fbi->access_ok);
		if (fbi->surface.viewPortInfo.rotation == 90 ||
			fbi->surface.viewPortInfo.rotation == 270) {
			tmp = fbi->surface.viewPortInfo.srcWidth;
			fbi->surface.viewPortInfo.srcWidth =
			fbi->surface.viewPortInfo.srcHeight;
			fbi->surface.viewPortInfo.srcHeight = tmp;
			fbi->surface.viewPortInfo.rotation = 360 -
			fbi->surface.viewPortInfo.rotation;
		}
		res = copy_to_user(argp, &fbi->surface.viewPortInfo,
			sizeof(struct _sViewPortInfo)) ? -EFAULT : 0;
		if (fbi->surface.viewPortInfo.rotation == 90 ||
			fbi->surface.viewPortInfo.rotation == 270) {
			tmp = fbi->surface.viewPortInfo.srcWidth;
			fbi->surface.viewPortInfo.srcWidth =
			fbi->surface.viewPortInfo.srcHeight;
			fbi->surface.viewPortInfo.srcHeight = tmp;
			fbi->surface.viewPortInfo.rotation = 360 -
			fbi->surface.viewPortInfo.rotation;
		}
		mutex_unlock(&fbi->access_ok);
		return res;
	case FB_IOCTL_SET_VIEWPORT_INFO:/*if rotate 90/270, w/h swap*/
		mutex_lock(&fbi->access_ok);
		if (copy_from_user(&gViewPortInfo, argp,
				sizeof(gViewPortInfo))) {
			mutex_unlock(&fbi->access_ok);
			return -EFAULT;
		}
		if (unsupport_format(fbi, gViewPortInfo, -1, 1)) {
			mutex_unlock(&fbi->access_ok);
			return -EFAULT;
		}
		gViewPortInfo.rotation = (360 - gViewPortInfo.rotation) % 360;
		if (gViewPortInfo.rotation == 90 ||
			gViewPortInfo.rotation == 270) {
			tmp = gViewPortInfo.srcWidth;
			gViewPortInfo.srcWidth =
			gViewPortInfo.srcHeight;
			gViewPortInfo.srcHeight = tmp;
		}

		if (check_surface(fi, -1, &gViewPortInfo, 0, 0, 0, 1))
			pxa168fb_set_par(fi);

		mutex_unlock(&fbi->access_ok);
		break;
	case FB_IOCTL_SET_VIDEO_MODE:
		/*
		 * Get data from user space.
		 */
		if (copy_from_user(&vmode, argp, sizeof(vmode)))
			return -EFAULT;

		if (check_surface(fi, vmode, 0, 0, 0, 0, 1))
			pxa168fb_set_par(fi);
		break;
	case FB_IOCTL_GET_VIDEO_MODE:
		return copy_to_user(argp, &fbi->surface.videoMode,
			sizeof(u32)) ? -EFAULT : 0;
	case FB_IOCTL_FLIP_VID_BUFFER:
		return flip_buffer(fi, arg, 1);
	case FB_IOCTL_GET_FREELIST:
		return get_freelist(fi, arg, 1);
	case FB_IOCTL_GET_BUFF_ADDR:
	{
		return copy_to_user(argp, &fbi->surface.videoBufferAddr,
			sizeof(struct _sVideoBufferAddr)) ? -EFAULT : 0;
	}
	case FB_IOCTL_SET_VID_OFFSET:
		mutex_lock(&fbi->access_ok);
		if (copy_from_user(&gViewPortOffset, argp,
				sizeof(gViewPortOffset))) {
			mutex_unlock(&fbi->access_ok);
			return -EFAULT;
		}

		if (check_surface(fi, -1, 0, &gViewPortOffset, 0, 0, 1))
			pxa168fb_set_par(fi);
		mutex_unlock(&fbi->access_ok);
		break;
	case FB_IOCTL_GET_VID_OFFSET:
		return copy_to_user(argp, &fbi->surface.viewPortOffset,
			sizeof(struct _sViewPortOffset)) ? -EFAULT : 0;

	case FB_IOCTL_SET_SURFACE:
	{
		mutex_lock(&fbi->access_ok);
		/* Get user-mode data. */
		if (copy_from_user(&fbi->surface_bak, argp,
					sizeof(struct _sOvlySurface))) {
			mutex_unlock(&fbi->access_ok);
			return -EFAULT;
		}
		fbi->surface_set = 1;

		mutex_unlock(&fbi->access_ok);
		return 0;
	}
	case FB_IOCTL_GET_SURFACE:
	{
		mutex_lock(&fbi->access_ok);
		if( fbi->surface_set ) {
		    ret = copy_to_user(argp, &fbi->surface_bak,
		            sizeof(struct _sOvlySurface) );
		}else {
		    ret = copy_to_user(argp, &fbi->surface,
		            sizeof(struct _sOvlySurface));
		}

		ret = (ret ? -EFAULT : 0);
		mutex_unlock(&fbi->access_ok);
		return ret;
	}

	case FB_IOCTL_SET_MEMORY_TOGGLE:
		break;

	case FB_IOCTL_SET_COLORKEYnALPHA:
		if (copy_from_user(&fbi->ckey_alpha, argp,
		    sizeof(struct _sColorKeyNAlpha)))
			return -EFAULT;

		pxa168fb_ovly_set_colorkeyalpha(fbi);
		break;
	case FB_IOCTL_GET_CHROMAKEYS:
		pxa168_sync_colorkey_structures(fbi, FB_SYNC_COLORKEY_TO_CHROMA);
		return copy_to_user(argp, &fbi->chroma, sizeof(struct pxa168_fb_chroma)) ? -EFAULT : 0;
	case FB_IOCTL_PUT_CHROMAKEYS:
		if (copy_from_user(&fbi->chroma, argp, sizeof(struct pxa168_fb_chroma)))
			return -EFAULT;
		pxa168_sync_colorkey_structures(fbi, FB_SYNC_CHROMA_TO_COLORKEY);
		pxa168fb_ovly_set_colorkeyalpha(fbi);
		return copy_to_user(argp, &fbi->chroma, sizeof(struct pxa168_fb_chroma)) ? -EFAULT : 0;

	case FB_IOCTL_GET_COLORKEYnALPHA:
		if (copy_to_user(argp, &fbi->ckey_alpha,
		    sizeof(struct _sColorKeyNAlpha)))
			return -EFAULT;
		break;
	case FB_IOCTL_SWITCH_VID_OVLY:
		if (copy_from_user(&vid_on, argp, sizeof(int)))
			return -EFAULT;
		spin_lock_irqsave(&fbi->var_lock, flags);
		mask = CFG_DMA_ENA_MASK;
again:
		fbi->dma_on = vid_on ? 1 : 0;
		val = CFG_DMA_ENA(check_modex_active(fbi->id, vid_on & 1));
#ifdef CONFIG_PXA688_VDMA
		if (vid_on == 0 && fbi->vdma_enable == 1)
			pxa688fb_vdma_release(fbi);
#endif
		if (!val) {
			/* switch off, disable DMA */
			dma_ctrl_set(fbi->id, 0, mask, val);
			/* in case already suspended, save in sw */
			gfx_info.fbi[fbi->id]->dma_ctrl0 &= ~mask;
		} else if (!buf_gethead(fbi->buf_waitlist) && !fbi->buf_current
				&& !(fb_mode && fbi->id == fb_dual))
			/* switch on, but no buf flipped, return error */
			ret = -EAGAIN;

		pr_info("SWITCH_VID_OVLY fbi %d dma_on %d, val %d, waitlist %p"
			" buf_current %p, ret %d\n", fbi->id, fbi->dma_on, val,
			buf_gethead(fbi->buf_waitlist), fbi->buf_current, ret);

		pxa688fb_vsmooth_set(fbi->id, 1, vid_vsmooth & vid_on, 0);

		if (FB_MODE_DUP) {
			fbi = ovly_info.fbi[fb_dual];
			goto again;
		}
		if (fb_mode && (fbi->id == fb_dual))
			fbi = ovly_info.fbi[fb_base];

		spin_unlock_irqrestore(&fbi->var_lock, flags);
		return ret;
		break;

	case FB_IOCTL_SWAP_VIDEO_RED_BLUE:
		param = (arg & 0x1);
		dma_ctrl_set(fbi->id, 0, CFG_DMA_SWAPRB_MASK, CFG_DMA_SWAPRB(param));
		if (FB_MODE_DUP)
			dma_ctrl_set(fb_dual, 0, CFG_DMA_SWAPRB_MASK,
				CFG_DMA_SWAPRB(param));
		return 0;
		break;

	case FB_IOCTL_SWAP_VIDEO_U_V:
		param = (arg & 0x1);
		dma_ctrl_set(fbi->id, 0, CFG_DMA_SWAPUV_MASK, CFG_DMA_SWAPUV(param));
		if (FB_MODE_DUP)
			dma_ctrl_set(fb_dual, 0, CFG_DMA_SWAPUV_MASK,
				CFG_DMA_SWAPUV(param));
		return 0;
		break;

	case FB_IOCTL_SWAP_VIDEO_Y_UV:
		param = (arg & 0x1);
		dma_ctrl_set(fbi->id, 0, CFG_DMA_SWAPYU_MASK, CFG_DMA_SWAPYU(param));
		if (FB_MODE_DUP)
			dma_ctrl_set(fb_dual, 0, CFG_DMA_SWAPYU_MASK,
				CFG_DMA_SWAPYU(param));
	        return 0;
		break;

	case FB_IOCTL_PUT_VIDEO_ALPHABLEND:
		/*
		 *  This puts the blending control to the Video layer.
		 */
		mask = CFG_ALPHA_MODE_MASK | CFG_ALPHA_MASK;
		val = CFG_ALPHA_MODE(0) | CFG_ALPHA(0xff);
		dma_ctrl_set(fbi->id, 1, mask, val);
		if (FB_MODE_DUP)
			dma_ctrl_set(fb_dual, 1, mask, val);
		return 0;
		break;

	case FB_IOCTL_PUT_GLOBAL_ALPHABLEND:
		/*
		 *  The userspace application can specify a byte value for the amount of global blend
		 *  between the video layer and the graphic layer.
		 *
		 *  The alpha blending is per the formula below:
		 *  P = (V[P] * blendval/255) + (G[P] * (1 - blendval/255))
		 *
		 *     where: P = Pixel value, V = Video Layer, and G = Graphic Layer
		 */
		blendval = (arg & 0xff);
		mask = CFG_ALPHA_MODE_MASK | CFG_ALPHA_MASK;
		val = CFG_ALPHA_MODE(2) | CFG_ALPHA(blendval);
		dma_ctrl_set(fbi->id, 1, mask, val);
		if (FB_MODE_DUP)
			dma_ctrl_set(fb_dual, 1, mask, val);
		return 0;
		break;

	case FB_IOCTL_PUT_GRAPHIC_ALPHABLEND:
		/*
		 *  This puts the blending back to the default mode of allowing the
		 *  graphic layer to do pixel level blending.
		 */
		mask = CFG_ALPHA_MODE_MASK | CFG_ALPHA_MASK;
		val = CFG_ALPHA_MODE(1) | CFG_ALPHA(0x0);
		dma_ctrl_set(fbi->id, 1, mask, val);
		if (FB_MODE_DUP)
			dma_ctrl_set(fb_dual, 1, mask, val);
		return 0;
		break;

	default:
		break;
	}

	return 0;
}

static int pxa168fb_open(struct fb_info *fi, int user)
{
	struct pxa168fb_mach_info *mi;
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)fi->par;
	struct fb_var_screeninfo *var = &fi->var;

	printk("Enter %s, fbi %d opened %d times -----------\n",
		__func__, fbi->id, atomic_read(&fbi->op_count));
	dev_dbg(fi->dev, "Enter %s\n", __FUNCTION__);

	if (fb_mode && (fbi->id == fb_dual)) {
		printk("fb_mode %x, operation to TV path is not allowed.\n"
			"Please switch to mode0 if needed.\n", fb_mode);
		return -EAGAIN;
	}

#ifdef OVLY_DVFM_CONSTRAINT
	set_dvfm_constraint();
#endif

	mi = fbi->dev->platform_data;
	fbi->new_addr[0] = 0;
	fbi->new_addr[1] = 0;
	fbi->new_addr[2] = 0;
	fi->fix.smem_start = fbi->fb_start_dma;
	fi->screen_base = fbi->fb_start;
	fbi->surface.videoMode = -1;
	fbi->surface.viewPortInfo.srcWidth = var->xres;
	fbi->surface.viewPortInfo.srcHeight = var->yres;

	fbi->active = 1;
	if (FB_MODE_DUP)
		ovly_info.fbi[fb_dual]->active = 1;

	set_pix_fmt(var, fbi->pix_fmt);

	if (mutex_is_locked(&fbi->access_ok))
		mutex_unlock(&fbi->access_ok);
	/* clear buffer list. */
	clear_buffer(fbi, 1);

	/* increase open count */
	atomic_inc(&fbi->op_count);

	if (FB_MODE_DUP) {
		fbi = ovly_info.fbi[fb_dual];
		fi = fbi->fb_info;
		var = &fi->var;
	}

	return 0;
}

extern void enable_graphic_layer(int id);

static int pxa168fb_release(struct fb_info *fi, int user)
{
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)fi->par;
	struct fb_var_screeninfo *var = &fi->var;
	u32 val;

	printk("Enter %s, fbi %d opened %d times -----------\n",
		__func__, fbi->id, atomic_read(&fbi->op_count));

	val = dma_ctrl_read(fbi->id, 0);
	val = val & 0xFF0FFFFE;
	dma_ctrl_write(fbi->id, 0, val);
	/* Force Video DMA engine off at RELEASE.*/
	if(atomic_dec_and_test(&fbi->op_count)) {
again:
#ifdef CONFIG_PXA688_VDMA
		if (fbi->vdma_enable) {
			val = vdma_ctrl_read(fbi) & 1;
			if (val)
				pxa688fb_vdma_release(fbi);
		}
#endif
		dma_ctrl_set(fbi->id, 0, CFG_DMA_ENA_MASK, 0);
		pxa688fb_vsmooth_set(fbi->id, 1, 0, 0);

		if (FB_MODE_DUP) {
			fbi = ovly_info.fbi[fb_dual];
			goto again;
		}
		if (fb_mode && (fbi->id == fb_dual))
			fbi = ovly_info.fbi[fb_base];
	}

	/* Turn off compatibility mode */
	var->nonstd &= ~0xff000000;
	COMPAT_MODE = 0;

	/* make sure graphics layer is enabled */
	enable_graphic_layer(fbi->id);
	if (FB_MODE_DUP) {
		enable_graphic_layer(fb_dual);
		val = dma_ctrl_read(fb_dual, 0);
		val = val & 0xFF0FFFFE;
		dma_ctrl_write(fb_dual, 0, val);
	}

	/* clear buffer list. */
	clear_buffer(fbi, 1);

	/* clear some globals */
	memset(&fbi->surface, 0, sizeof(struct _sOvlySurface));
	fbi->new_addr[0] = 0; fbi->new_addr[1] = 0; fbi->new_addr[2] = 0;
	fbi->active = 0;
	fbi->dma_on = 0;
	if (FB_MODE_DUP) {
		struct pxa168fb_info *fbi_dual = ovly_info.fbi[fb_dual];
		memset(&fbi_dual->surface, 0, sizeof(struct _sOvlySurface));
		fbi_dual->new_addr[0] = 0;
		fbi_dual->new_addr[1] = 0;
		fbi_dual->new_addr[2] = 0;
		fbi_dual->active = 0;
		fbi_dual->dma_on = 0;
	}

#ifdef OVLY_DVFM_CONSTRAINT
	unset_dvfm_constraint();
#endif

	return 0;
}

void pxa168fb_ovly_dual(int enable)
{
	struct pxa168fb_info *fbi_base = ovly_info.fbi[fb_base];
	struct pxa168fb_info *fbi_dual = ovly_info.fbi[fb_dual];

	pr_debug("%s enable %d fb_base->op_count %d fb_dual->active %d\n",
		__func__, enable, atomic_read(&fbi_base->op_count),
		fbi_dual->active);

	if (!atomic_read(&fbi_base->op_count))
		return;

	if (enable) {
		fbi_dual->active = fbi_base->active;
		fbi_dual->dma_on = fbi_base->dma_on;
		if (fbi_base->ckalpha_set)
			pxa168fb_ovly_set_colorkeyalpha(fbi_base);
		pxa168fb_set_par(fbi_base->fb_info);
	} else {
		/* disable video layer DMA */
		dma_ctrl_set(fb_dual, 0, CFG_DMA_ENA_MASK, 0);
		/* in case already suspended, save in sw */
		gfx_info.fbi[fb_dual]->dma_ctrl0 &= ~CFG_DMA_ENA_MASK;
		fbi_dual->active = 0; fbi_dual->dma_on = 0;

		/* free buffer if wait_peer flag is set */
		if (ovly_info.wait_peer) {
			ovly_info.wait_peer = 0;
#ifdef OVLY_TASKLET
			tasklet_schedule(&fbi_base->ovly_task);
#else
			if (fbi_base->system_work)
					schedule_work(&fbi_base->buf_work);
			else
				queue_work(fbi_base->work_q,
					&fbi_base->fb_info->queue);
#endif
		}
	}
}

static void set_mode(struct pxa168fb_info *fbi, struct fb_var_screeninfo *var,
		     struct fb_videomode *mode, int pix_fmt, int ystretch)
{
	dev_dbg(fbi->fb_info->dev, "Enter %s\n", __FUNCTION__);
	set_pix_fmt(var, pix_fmt);

	var->xres = mode->xres;
	var->yres = mode->yres;
	var->xres_virtual = max(var->xres, var->xres_virtual);
	if (ystretch)
		var->yres_virtual = var->yres*2;
	else
		var->yres_virtual = max(var->yres, var->yres_virtual);

	var->grayscale = 0;
	var->accel_flags = FB_ACCEL_NONE;
	var->pixclock = mode->pixclock;
	var->left_margin = mode->left_margin;
	var->right_margin = mode->right_margin;
	var->upper_margin = mode->upper_margin;
	var->lower_margin = mode->lower_margin;
	var->hsync_len = mode->hsync_len;
	var->vsync_len = mode->vsync_len;
	var->sync = mode->sync;
	var->vmode = FB_VMODE_NONINTERLACED;
	var->rotate = FB_ROTATE_UR;
}

static int pxa168fb_check_var(struct fb_var_screeninfo *var, struct fb_info *fi)
{
	struct pxa168fb_info *fbi = fi->par;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	int pix_fmt;

	dev_dbg(fi->dev, "Enter %s\n", __FUNCTION__);
	if (var->bits_per_pixel == 8) {
		printk("bits per pixel too small\n");
		return -EINVAL;
	}

	/* compatibility mode: if the MSB of var->nonstd is 0xAA then
	 * set xres_virtual and yres_virtual to xres and yres.
	 */

	if((var->nonstd >> 24) == 0xAA)
		COMPAT_MODE = 0x2625;

	if((var->nonstd >> 24) == 0x55)
		COMPAT_MODE = 0x0;

	/*
	 * Basic geometry sanity checks.
	 */

	if (var->xoffset + var->xres > var->xres_virtual) {
		pr_err("ERROR: xoffset(%d) + xres(%d) is greater than "
			"xres_virtual(%d)\n", var->xoffset, var->xres,
			var->xres_virtual);
		return -EINVAL;
	}
	if (var->yoffset + var->yres > var->yres_virtual) {
		pr_err("ERROR: yoffset(%d) + yres(%d) is greater than "
			"yres_virtual(%d)\n", var->yoffset, var->yres,
			var->yres_virtual);
		return -EINVAL;
	}

	if (var->xres + var->right_margin +
	    var->hsync_len + var->left_margin > 2048) {
		pr_err("ERROR: var->xres(%d) + var->right_margin(%d) + "
			"var->hsync_len(%d) + var->left_margin(%d) > 2048",
			var->xres, var->right_margin, var->hsync_len,
			var->left_margin);
		return -EINVAL;
	}
	if (var->yres + var->lower_margin +
	    var->vsync_len + var->upper_margin > 2048) {
		pr_err("var->yres(%d) + var->lower_margin(%d) + "
			"var->vsync_len(%d) + var->upper_margin(%d) > 2048",
			var->yres, var->lower_margin, var->vsync_len,
			var->upper_margin);
		return -EINVAL;
	}

	/*
	 * Check size of framebuffer.
	 */
	if (mi->mmap && (var->xres_virtual * var->yres_virtual *
	    (var->bits_per_pixel >> 3) > max_fb_size)) {
		pr_err("xres_virtual(%d) * yres_virtual(%d) * "
			"(bits_per_pixel(%d) >> 3) > max_fb_size(%d)",
			var->xres_virtual, var->yres_virtual,
			var->bits_per_pixel, max_fb_size);
		return -EINVAL;
	}

	/*
	 * Select most suitable hardware pixel format.
	 */
	pix_fmt = determine_best_pix_fmt(var);
	dev_dbg(fi->dev, "%s determine_best_pix_fmt returned: %d\n", __FUNCTION__, pix_fmt);
	if (pix_fmt < 0)
		return pix_fmt;

	return 0;
}

static u32 dma_ctrl0_update(struct pxa168fb_mach_info *mi, u32 x, u32 pix_fmt)
{
	/*
	 * clear video layer's field
	 */
	x &= 0xef0fff01;

	/*
	 * If we are in a pseudo-color mode, we need to enable
	 * palette lookup.
	 */
	if (pix_fmt == PIX_FMT_PSEUDOCOLOR)
		x |= 0x10000000;

	x |= 1 << 6;	/* horizontal smooth filter */
	/*
	 * Configure hardware pixel format.
	 */
	x |= ((pix_fmt & ~0x1000) >> 1) << 20;

	/*
	 * color format in memory:
	 * PXA168/PXA910:
	 * PIX_FMT_YUV422PACK: UYVY(CbY0CrY1)
	 * PIX_FMT_YUV422PLANAR: YUV
	 * PIX_FMT_YUV420PLANAR: YUV
	 */
	if (((pix_fmt & ~0x1000) >> 1) == 5) {
		x |= 0x00000002;
		x |= (mi->panel_rbswap) << 4;
		if ((pix_fmt & 0x1000))
			x |= 1 << 2;	//Y and U/V is swapped
		else
			x |= (pix_fmt & 1) << 3;
	} else if (pix_fmt >= 12) {	/* PIX_FMT_YUV422PACK_IRE_90_270 is here */
		x |= 0x00000002;
		x |= (pix_fmt & 1) << 3;
		x |= (mi->panel_rbswap) << 4;
	} else {
		x |= (((pix_fmt & 1) ^ 1) ^ (mi->panel_rbswap)) << 4;
	}

	return x;
}

static void set_dma_control0(struct pxa168fb_info *fbi)
{
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	u32 x = 0, x_bk = 0;

	dev_dbg(fbi->fb_info->dev, "FB1: Enter %s\n", __FUNCTION__);

	/*
	 * Get reg's current value
	 */
	x_bk = x = dma_ctrl_read(fbi->id, 0);

	/* update dma_ctrl0 according to pix_fmt */
	x = dma_ctrl0_update(mi, x, fbi->pix_fmt);

	/* clear reserved bits if not panel path */
	if (fbi->id)
		x &= ~CFG_ARBFAST_ENA(1);

	if (x_bk != x) {
		dma_ctrl_write(fbi->id, 0, x);
		pr_debug("set fbi %d dma ctrl0(0x%x -> 0x%x): addr %lu"
			" fbi->active %d\n", fbi->id, x_bk,
			x, fbi->new_addr[0], fbi->active);
	}
}

static void set_yuv_start(struct pxa168fb_info *fbi, unsigned long addr)
{
	struct lcd_regs *regs = get_regs(fbi->id);
	struct fb_var_screeninfo *var = &fbi->fb_info->var;
	unsigned long addr_y0, addr_u0, addr_v0;

	/* caculate yuv offset */
	addr_y0 = addr;

	if ((fbi->pix_fmt >= 12) && (fbi->pix_fmt <= 15))
		addr += var->xres * var->yres;
	addr_u0 = addr;

	if ((fbi->pix_fmt >> 1) == 6)
		addr += var->xres * var->yres/2;
	else if ((fbi->pix_fmt>>1) == 7)
		addr += var->xres * var->yres/4;
	addr_v0 = addr;

	/* update registers */
	writel(addr_y0, &regs->v_y0);
	writel(addr_u0, &regs->v_u0);
	writel(addr_v0, &regs->v_v0);
}

static void pxa168fb_vdma_config(struct pxa168fb_info *fbi)
{
#ifdef CONFIG_PXA688_VDMA
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	int active;
	u32 val;

	pr_debug("%s fbi %d vdma_enable %d active %d\n",
		__func__, fbi->id, fbi->vdma_enable, fbi->active);
	if (fbi->vdma_enable && fbi->active) {
		active = check_modex_active(fbi->id, fbi->active);
		if (active) {
			mi->vdma_lines = pxa688fb_vdma_get_linenum(fbi, 1,
				fbi->surface.viewPortInfo.rotation);
			pxa688fb_vdma_set(fbi, mi->sram_paddr, mi->vdma_lines,
				1, fbi->surface.videoMode,
				fbi->surface.viewPortInfo.rotation,
				fbi->surface.viewPortInfo.yuv_format);
		} else {
			val = vdma_ctrl_read(fbi) & 1;
			if (val) {
				pr_debug("%s fbi %d val %x\n",
					__func__, fbi->id, val);
				pxa688fb_vdma_release(fbi);
			}
		}
	}
#endif
}

static void pxa168fb_misc_update(struct pxa168fb_info *fbi)
{
	pxa168fb_vdma_config(fbi);
	pxa688fb_vsmooth_set(fbi->id, 1, vid_vsmooth, 0);
}

static void set_video_start(struct fb_info *fi, int xoffset,
		int yoffset, int dual_wait, int dual_trigger)
{
	struct pxa168fb_info *fbi = fi->par;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	struct fb_var_screeninfo *var = &fi->var;
	struct lcd_regs *regs;
	int pixel_offset;
	unsigned long addr = 0;
	static int debugcount = 0;

	if(debugcount < 10)
		debugcount++;

	if (debugcount < 9)
		dev_dbg(fi->dev, "Enter %s\n", __FUNCTION__);

again:
	regs = get_regs(fbi->id);

	if (!(fbi->new_addr[0])) {
		if (!mi->mmap) {
			pr_debug("fbi %d(line %d): input err, mmap is not"
				" supported\n", fbi->id, __LINE__);
			return;
		}
		pixel_offset = (yoffset * var->xres_virtual) + xoffset;
		/* Set this at VSync interrupt time */
		addr = fbi->fb_start_dma + ((pixel_offset * var->bits_per_pixel) >> 3);
		set_yuv_start(fbi, addr);

		/* return until the address take effect after vsync occurs */
		if (NEED_VSYNC(fbi, 1))
			wait_for_vsync(fbi, 1);
	} else {
		if (fbi->debug == 1)
			printk("%s: buffer updated to %x\n",
				 __func__, (int)fbi->new_addr[0]);

		writel(fbi->new_addr[0], &regs->v_y0);
		if (fbi->pix_fmt >= 12 && fbi->pix_fmt <= 15) {
			writel(fbi->new_addr[1], &regs->v_u0);
			writel(fbi->new_addr[2], &regs->v_v0);
		}
	}

	/* enable DMA transfer */
	set_dma_active(fbi, 1);

	fbi->misc_update = 1;

	if (FB_MODE_DUP) {
		struct pxa168fb_info *fbi_dual = ovly_info.fbi[fb_dual];
		/* set TV path buffer adder */
		fbi_dual->fb_start_dma = fbi->fb_start_dma;
		if (!fbi_dual->new_addr[0] && fbi->new_addr[0]) {
			fbi_dual->new_addr[0] = fbi->new_addr[0];
			fbi_dual->new_addr[1] = fbi->new_addr[1];
			fbi_dual->new_addr[2] = fbi->new_addr[2];
		}
		if (dual_trigger) {
			/* trigger register set @ dual path interrupt */
			fbi_dual->info = fbi_dual->fb_info;
			return;
		}
		if (dual_wait)
			/* don't set dual path until set_par */
			return;
		fbi = fbi_dual;
		goto again;
	}
}

/* if video layer is full screen without alpha blending then turn off graphics dma to save bandwidth */
static void pxa168fb_graphics_off(struct pxa168fb_info *fbi)
{
	u32 dma1, gfx_bottom, v_size_dst, screen_active;
	struct lcd_regs *regs;

	dev_dbg(fbi->fb_info->dev, "%s\n", __func__);
again:
	regs = get_regs(fbi->id);

	dma1 = dma_ctrl_read(fbi->id, 1) & (CFG_ALPHA_MODE_MASK | CFG_ALPHA_MASK);
	gfx_bottom = (dma1 == (CFG_ALPHA_MODE(2) | CFG_ALPHA(0xff))) ? 1 : 0;
	v_size_dst = readl(&regs->v_size_z);
	screen_active = readl(&regs->screen_active);

	dev_dbg(fbi->fb_info->dev, "v_size_dst %d screen_active %d dma1 0x%x\n",
			v_size_dst, screen_active, dma1);

	/* graphics layer off if overlay on upper layer and full screen */
	if ((v_size_dst == screen_active) && gfx_bottom)
		dma_ctrl_set(fbi->id, 0, CFG_GRA_ENA_MASK, 0);

	if (FB_MODE_DUP) {
		fbi = ovly_info.fbi[fb_dual];
		goto again;
	}
}

static void set_twolevel_zoomdown(struct pxa168fb_info *fbi,
		int xres, int xzoom)
{
	u32 reg = (fbi->id == 2) ? LCD_PN2_LAYER_ALPHA_SEL1 :
		LCD_AFA_ALL2ONE, x;
	int shift = (fbi->id == 1) ? 22 : 20;

	x = readl(fbi->reg_base + reg);
	if (!(xres & 0x1) && ((xres >> 1) >= xzoom))
		x |= 1 << shift;
	else
		x &= ~(1 << shift);
	writel(x, fbi->reg_base + reg);
}

/* set dual path video layer registers for mirror mode */
static void pxa168fb_set_regs(struct pxa168fb_info *fbi,
		struct _sOvlySurface *surface)
{
	struct lcd_regs *regs = get_regs(fbi->id);
	int xres = surface->viewPortInfo.srcWidth;
	int yres = surface->viewPortInfo.srcHeight;
	int xzoom = surface->viewPortInfo.zoomXSize;
	int yzoom = surface->viewPortInfo.zoomYSize;
	int xpos = surface->viewPortOffset.xOffset;
	int ypos = surface->viewPortOffset.yOffset;
	int yPitch = surface->viewPortInfo.yPitch;
	int uPitch = surface->viewPortInfo.uPitch;
	int vPitch = surface->viewPortInfo.vPitch;

	writel(fbi->new_addr[0], &regs->v_y0);
	writel(fbi->new_addr[1], &regs->v_u0);
	writel(fbi->new_addr[2], &regs->v_v0);

	writel(yPitch, &regs->v_pitch_yc);
	writel(vPitch << 16 | uPitch, &regs->v_pitch_uv);

	writel((yres << 16) | xres, &regs->v_size);
	writel((ypos << 16) | xpos, &regs->v_start);
	writel((yzoom << 16) | xzoom, &regs->v_size_z);

	/* enable two-level zoom down if the ratio exceed 2 */
	if (xzoom & yPitch)
		set_twolevel_zoomdown(fbi, xres, xzoom);

	set_dma_control0(fbi);

	/* enable DMA transfer */
	set_dma_active(fbi, 1);

	fbi->misc_update = 1;

	if (fbi->debug == 8)
		pr_info("%s fbi %d new_addr[0]: 0x%lu\n",
			__func__, fbi->id, fbi->new_addr[0]);
	return;
}
static void set_yuv_pitch(struct pxa168fb_info *fbi,
	struct _sOvlySurface *surface, struct fb_var_screeninfo *var, int fake)
{
	struct lcd_regs *regs = get_regs(fbi->id);
	int xres = surface->viewPortInfo.srcWidth;
	int yres = surface->viewPortInfo.srcHeight;
	int xzoom = surface->viewPortInfo.zoomXSize;
	int yzoom = surface->viewPortInfo.zoomYSize;
	int xpos = surface->viewPortOffset.xOffset;
	int ypos = surface->viewPortOffset.yOffset;
	int yPitch = surface->viewPortInfo.yPitch;
	int uPitch = surface->viewPortInfo.uPitch;
	int vPitch = surface->viewPortInfo.vPitch;

	/* check/update all the settings */
	if (fb_mode && (fbi->id == fb_dual) && surface->dualInfo.rotate) {
		/* rotate, get video width/height from user */
		xres = surface->dualInfo.srcWidth;
		yres = surface->dualInfo.srcHeight;
		/* fb_dual, overlay will never rotate, so will no yPitch info,
		 * as android overlay-hal always set ypitch = 0 when no rotate
		 */
		yPitch = 0;
	}

	xres = xres ? xres : var->xres;
	yres = yres ? yres : var->yres;
	if (((fbi->pix_fmt & ~0x1000) >> 1) < 6) {
		yPitch = yPitch ? yPitch : xres * var->bits_per_pixel >> 3;
		uPitch = 0; vPitch = 0;
	} else {
		yPitch = yPitch ? yPitch : xres;

		if (!uPitch || !vPitch) {
			uPitch = xres >> 1; vPitch = xres >> 1;
		}
	}

	if (fb_mode && (fbi->id == fb_dual))
		dual_pos_zoom(fbi, surface, &xzoom, &yzoom, &xpos, &ypos);

	if (fake) {
		/* mirror mode, save all settings and set until dual path irq */
		fbi->surface.viewPortInfo.srcWidth = xres;
		fbi->surface.viewPortInfo.srcHeight = yres;
		fbi->surface.viewPortInfo.zoomXSize = xzoom;
		fbi->surface.viewPortInfo.zoomYSize = yzoom;
		fbi->surface.viewPortOffset.xOffset = xpos;
		fbi->surface.viewPortOffset.yOffset = ypos;
		fbi->surface.viewPortInfo.yPitch = yPitch;
		fbi->surface.viewPortInfo.uPitch = uPitch;
		fbi->surface.viewPortInfo.vPitch = vPitch;
		return;
	}

	/* pitch */
	writel(yPitch, &regs->v_pitch_yc);
	writel(vPitch << 16 | uPitch, &regs->v_pitch_uv);

	/* width, height */
	writel((yres << 16) | xres, &regs->v_size);

	/* xpos, ypos, xzoom, yzoom */
	writel((yzoom << 16) | xzoom, &regs->v_size_z);

	/* enable two-level zoom down if the ratio exceed 2 */
	if (xzoom && var->bits_per_pixel)
		set_twolevel_zoomdown(fbi, xres, xzoom);

	if(COMPAT_MODE != 0x2625) {
		/* update video position offset */
		dev_dbg(fbi->fb_info->dev, "Setting Surface offsets...\n");

		writel(CFG_DMA_OVSA_VLN(ypos)| xpos, &regs->v_start);
	}
	else {
		dev_dbg(fbi->fb_info->dev, "Executing 2625 compatibility mode\n");
		xpos = (var->nonstd & 0x3ff);
		ypos = (var->nonstd >> 10) & 0x3ff;
		writel(CFG_DMA_OVSA_VLN(ypos) | xpos, &regs->v_start);
		writel((var->height << 16) | var->width, &regs->v_size_z);
	}
}

static int pxa168fb_set_par(struct fb_info *fi)
{
	struct pxa168fb_info *fbi = fi->par;
	struct fb_var_screeninfo *var = &fi->var;
	struct _sOvlySurface *surface = &fbi->surface;
	int pix_fmt;

	dev_dbg(fi->dev,"FB1: Enter %s\n", __FUNCTION__);
	/*
	 * Determine which pixel format we're going to use.
	 */
	pix_fmt = determine_best_pix_fmt(&fi->var);
	dev_dbg(fi->dev,"determine_best_pix_fmt returned: %d\n", pix_fmt);
	if (pix_fmt < 0)
		return pix_fmt;
	fbi->pix_fmt = pix_fmt;

	dev_dbg(fi->dev, "pix_fmt=%d\n", pix_fmt);
	dev_dbg(fi->dev,"BPP = %d\n", var->bits_per_pixel);
	/*
	 * Set additional mode info.
	 */
	if (pix_fmt == PIX_FMT_PSEUDOCOLOR)
		fi->fix.visual = FB_VISUAL_PSEUDOCOLOR;
	else
		fi->fix.visual = FB_VISUAL_TRUECOLOR;
	fi->fix.line_length = var->xres_virtual * var->bits_per_pixel / 8;

	/* when lcd is suspend, read or write lcd controller's
	* register is not effective, so just return*/
	if (!(gfx_info.fbi[fbi->id]->active)) {
		printk(KERN_DEBUG"LCD is not active, don't touch hardware\n");
		return 0;
	}

	/*
	 * Configure graphics DMA parameters.
	 */
	set_yuv_pitch(fbi, surface, &fbi->fb_info->var, 0);

	if (FB_MODE_DUP) {
		struct _sDualInfo *dual = &surface->dualInfo;
		struct pxa168fb_info *fbi_dual = ovly_info.fbi[fb_dual];
		struct fb_info *fi_dual = fbi_dual->fb_info;

		if (dual->rotate == 1) {
			fi_dual->var.bits_per_pixel = 16;
			fbi_dual->pix_fmt = convert_pix_fmt(dual->videoMode);
		} else {
			fi_dual->var.bits_per_pixel = fi->var.bits_per_pixel;
			fbi_dual->pix_fmt = fbi->pix_fmt;
		}

		fi_dual->var.xres = fi->var.xres;
		fi_dual->var.yres = fi->var.yres;
		fi_dual->var.nonstd = fi->var.nonstd;

		set_yuv_pitch(fbi_dual, surface, &fbi_dual->fb_info->var, 1);

		if (fbi->debug == 8) {
			pr_info("%s dual->rotate %d videoMode %d"
				" pix_fmt %d\n", __func__, dual->rotate,
				dual->videoMode, fbi_dual->pix_fmt);
		}
	}

	/*
	 * Configure global panel parameters.
	 */
	set_dma_control0(fbi);

	/* set video start address */
	set_video_start(fi, fi->var.xoffset, fi->var.yoffset, 1, 1);

	pxa168fb_graphics_off(fbi);
	return 0;
}

static unsigned int chan_to_field(unsigned int chan, struct fb_bitfield *bf)
{
	return ((chan & 0xffff) >> (16 - bf->length)) << bf->offset;
}

static u32 to_rgb(u16 red, u16 green, u16 blue)
{
	red >>= 8;
	green >>= 8;
	blue >>= 8;

	return (red << 16) | (green << 8) | blue;
}

static int
pxa168fb_setcolreg(unsigned int regno, unsigned int red, unsigned int green,
		 unsigned int blue, unsigned int trans, struct fb_info *fi)
{
	struct pxa168fb_info *fbi = fi->par;
	u32 val;

	if (fi->fix.visual == FB_VISUAL_TRUECOLOR && regno < 16) {
		val =  chan_to_field(red,   &fi->var.red);
		val |= chan_to_field(green, &fi->var.green);
		val |= chan_to_field(blue , &fi->var.blue);
		fbi->pseudo_palette[regno] = val;
	}

	if (fi->fix.visual == FB_VISUAL_PSEUDOCOLOR && regno < 256) {
		val = to_rgb(red, green, blue);
		writel(val, fbi->reg_base + LCD_SPU_SRAM_WRDAT);	/* FIXME */
		writel(0x8300 | regno, fbi->reg_base + LCD_SPU_SRAM_CTRL);
	}

	return 0;
}

static int pxa168fb_pan_display(struct fb_var_screeninfo *var,
				struct fb_info *fi)
{
	struct pxa168fb_info *fbi = fi->par;
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;

	if (!mi->mmap)
		return -EINVAL;

	fbi->active = 1;
	set_video_start(fi, var->xoffset, var->yoffset, 1, 1);
	return 0;
}

static int pxa168fb_fb_sync(struct fb_info *info)
{
	struct pxa168fb_info *fbi = (struct pxa168fb_info *)info->par;

	if (NEED_VSYNC(fbi, 1))
		wait_for_vsync(fbi, 1);
	return 0;
}

/*
 *  pxa168fb_handle_irq(two lcd controllers)
 */
irqreturn_t pxa168fb_ovly_isr(int id)
{
	struct pxa168fb_info *fbi = ovly_info.fbi[id];
	struct fb_info *fi;

	if (!fbi)
		return IRQ_HANDLED;
	fi = fbi->fb_info;
	if (fbi->debug == 1)
		printk(KERN_DEBUG "%s fbi %d\n", __func__, fbi->id);

	if (id == fb_base && fbi->misc_update) {
		pxa168fb_misc_update(fbi);
		fbi->misc_update = 0;
	}

	if (fb_mode && (id == fb_dual)) {
		if (fbi->info) {
			pxa168fb_set_regs(fbi, &fbi->surface);
			fbi->info = NULL;
		}
		if (fbi->misc_update) {
			pxa168fb_misc_update(fbi);
			fbi->misc_update = 0;
		}
		/* mirror mode, tv path irq, check wait_peer set or not */
		if (ovly_info.wait_peer) {
			/* trigger irq to free panel path buf */
			fbi = ovly_info.fbi[fb_base];
#ifdef OVLY_TASKLET
			tasklet_schedule(&fbi->ovly_task);
#else
			if (fbi->system_work)
				schedule_work(&fbi->buf_work);
			else
				queue_work(fbi->work_q,
					&fbi->fb_info->queue);
#endif
		}
		/* no buf need to be handled for tv path, wakeup only */
		goto wakeup;
	}

	if (atomic_read(&fbi->op_count)) {
		/* do buffer switch for video flip */
		buf_endframe(fi, 1);

wakeup:
		/* wake up queue. */
		if (atomic_read(&fbi->w_intr) == 0) {
			atomic_set(&fbi->w_intr, 1);
			wake_up(&fbi->w_intr_wq);
		}
	}

	return IRQ_HANDLED;
}

static ssize_t debug_show(struct device *dev, struct device_attribute *attr,
		char *buf)
{
	struct pxa168fb_info *fbi = dev_get_drvdata(dev);

	return sprintf(buf, "fbi %d debug %d active %d\n"
		" retire_err %d surface_set %d update_addr %d\n"
		" wait_peer %d wait_free %d rotate_ignore %d\n"
		" buf_flipped %d buf_displayed %d buf_ignored %d dma_on %d\n",
		fbi->id, fbi->debug, fbi->active, ovly_info.retire_err,
		fbi->surface_set, fbi->update_addr,
		WAIT_PEER, WAIT_FREE, ROTATE_IGNORE, fbi->buf_flipped,
		fbi->buf_displayed, fbi->buf_flipped - fbi->buf_displayed,
		fbi->dma_on);
}

static ssize_t debug_store(
		struct device *dev, struct device_attribute *attr,
		const char *buf, size_t size)
{
	struct pxa168fb_info *fbi = dev_get_drvdata(dev);
	int	tmp;

	sscanf(buf, "%d", &tmp);
	if (tmp == 2 || tmp == 3)
		fbi->update_addr = tmp & 1;
	else if (tmp == 9) {
		ovly_info.flag++;
		if (ovly_info.flag > 0xffff)
			ovly_info.flag = 0;
	} else
		/* 0: disable all debug info
		 * 1: enable log for buffer management
		 * 8: enable log for mirror mode surface adjust
		 */
		fbi->debug = tmp;

	return size;
}

static DEVICE_ATTR(debug, S_IRUGO | S_IWUSR, debug_show, debug_store);

static struct fb_ops pxa168fb_ops = {
	.owner		= THIS_MODULE,
	.fb_open	= pxa168fb_open,
	.fb_release	= pxa168fb_release,

	.fb_check_var	= pxa168fb_check_var,
	.fb_set_par	= pxa168fb_set_par,
	.fb_setcolreg	= pxa168fb_setcolreg,
	.fb_pan_display	= pxa168fb_pan_display,
	.fb_fillrect	= cfb_fillrect,
	.fb_copyarea	= cfb_copyarea,
	.fb_imageblit	= cfb_imageblit,
	.fb_sync	= pxa168fb_fb_sync,
	.fb_ioctl	= pxa168fb_ovly_ioctl,
};

static int __init get_ovly_size(char *str)
{
	int n;
	if (!get_option(&str, &n))
		return 0;
	max_fb_size = n;
	fb_size_from_cmd = 1;
	return 1;
}
__setup("ovly_size=", get_ovly_size);

static int __devinit pxa168fb_probe(struct platform_device *pdev)
{
	struct pxa168fb_mach_info *mi;
	struct fb_info *fi;
	struct pxa168fb_info *fbi;
	struct lcd_regs *regs;
	struct resource *res;
	int ret;

	mi = pdev->dev.platform_data;
	if (mi == NULL)
		return -EINVAL;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (res == NULL)
		return -EINVAL;

	fi = framebuffer_alloc(sizeof(struct pxa168fb_info), &pdev->dev);
	if (fi == NULL){
		printk("%s: no enough memory!\n", __func__);
		return -ENOMEM;
	}

	fbi = fi->par;
	fbi->id = pdev->id;

	if (!fbi->id)
		memset(&ovly_info, 0, sizeof(ovly_info));
	ovly_info.fbi[fbi->id] = fbi;
	ovly_info.flag = 5;
	fbi->update_addr = 0;

	platform_set_drvdata(pdev, fbi);
	fbi->check_modex_active = check_modex_active;
	fbi->update_buff = pxa168fb_update_buff;
	fbi->fb_info = fi;
	fbi->dev = &pdev->dev;
	fbi->id = pdev->id;
	fbi->fb_info->dev = &pdev->dev;
	fbi->debug = 0;
	init_waitqueue_head(&fbi->w_intr_wq);
	mutex_init(&fbi->access_ok);
	spin_lock_init(&fbi->buf_lock);
	spin_lock_init(&fbi->var_lock);

#ifdef OVLY_TASKLET
	tasklet_init(&fbi->ovly_task, pxa168fb_ovly_task, (unsigned long)fbi);
#else
	fbi->system_work = 1;
	if (fbi->system_work)
		INIT_WORK(&fbi->buf_work, pxa168fb_ovly_work);
	else {
		fbi->work_q = create_singlethread_workqueue("pxa168fb-ovly");
		INIT_WORK(&fi->queue, pxa168fb_ovly_work);
	}
#endif

	/* FIXME - video layer has no specific clk. it depend on graphic layer clk.
	 * fbi->clk = clk_get(&pdev->dev, NULL);
	 */

	/*
	 * Initialise static fb parameters.
	 */
	fi->flags = FBINFO_DEFAULT | FBINFO_PARTIAL_PAN_OK |
		    FBINFO_HWACCEL_XPAN | FBINFO_HWACCEL_YPAN;
	fi->node = -1;
	strcpy(fi->fix.id, mi->id);
	fi->fix.type = FB_TYPE_PACKED_PIXELS;
	fi->fix.type_aux = 0;
	fi->fix.xpanstep = 1;
	fi->fix.ypanstep = 1;
	fi->fix.ywrapstep = 0;
	fi->fix.mmio_start = res->start;
	fi->fix.mmio_len = res->end - res->start + 1;
	fi->fix.accel = FB_ACCEL_NONE;
	fi->fbops = &pxa168fb_ops;
	fi->pseudo_palette = fbi->pseudo_palette;

	/*
	 * Map LCD controller registers.
	 */
	fbi->reg_base = ioremap_nocache(res->start, res->end - res->start);
	if (fbi->reg_base == NULL) {
		printk("%s: no enough memory!\n", __func__);
		ret = -ENOMEM;
		goto failed;
	}

	fbi->mem_status = -1;
	if (mi->mmap) {
		fbi->mem_status = 0;
		/*
		 * Allocate framebuffer memory.
		 */
		if (!fb_size_from_cmd) {
			if (mi->max_fb_size)
				max_fb_size = mi->max_fb_size;
			else
				max_fb_size = DEFAULT_FB_SIZE;
		}
		max_fb_size = PAGE_ALIGN(max_fb_size);
		fbi->fb_size = max_fb_size;

		/*
		 * FIXME, It may fail to alloc DMA buffer from dma_alloc_xxx
		 */
		fbi->fb_start = dma_alloc_writecombine(fbi->dev, max_fb_size,
							&fbi->fb_start_dma,
							GFP_KERNEL);
		if (!fbi->fb_start || !fbi->fb_start_dma) {
			fbi->new_addr[0] = 0;
			fbi->new_addr[1] = 0;
			fbi->new_addr[2] = 0;
			fbi->mem_status = 1;
			fbi->fb_start = (void *)__get_free_pages(GFP_DMA | GFP_KERNEL,
						get_order(fbi->fb_size));
			fbi->fb_start_dma = (dma_addr_t)__virt_to_phys(fbi->fb_start);
		}

		if (fbi->fb_start == NULL) {
			printk("%s: no enough memory!\n", __func__);
			ret = -ENOMEM;
			goto failed;
		}
		printk("---------FBoverlay DMA buffer phy addr : %x\n",(unsigned int)fbi->fb_start_dma);

		memset(fbi->fb_start, 0, fbi->fb_size);
		fi->fix.smem_start = fbi->fb_start_dma;
		fi->fix.smem_len = fbi->fb_size;
		fi->screen_base = fbi->fb_start;
		fi->screen_size = fbi->fb_size;
	}

#ifdef FB_PM_DEBUG
	pxa168fb_rw_all_regs(fbi, g_regs, 1);
#endif

	fbi->surface.viewPortInfo.rotation = 0;
#ifdef CONFIG_PXA688_VDMA
	mi->immid = pxa688fb_imm_register("mmp2 dma");
	pxa688_vdma_clkset(1);
	if (mi->vdma_enable) {
		if (!mi->sram_paddr)
			mi->sram_paddr = pxa688fb_vdma_squ_malloc(
				&mi->sram_size, mi->immid);
		pr_debug("vdma enabled, sram_paddr 0x%x sram_size 0x%x\n",
			mi->sram_paddr, mi->sram_size);
	}
	fbi->vdma_enable = mi->vdma_enable;
#endif

	/*
	 * Fill in sane defaults.
	 */
	set_mode(fbi, &fi->var, mi->modes, mi->pix_fmt, 1);
	pxa168fb_set_par(fi);

	/*
	 * Configure default register values.
	 */
	regs = get_regs(fbi->id);
	writel(0, &regs->v_y1);
	writel(0, &regs->v_u1);
	writel(0, &regs->v_v1);
	writel(0, &regs->v_start);

	/* Set this frame off by default */
	dma_ctrl_set(fbi->id, 0, CFG_DMA_ENA_MASK, 0);

	/*
	 * Allocate color map.
	 */
	if (fb_alloc_cmap(&fi->cmap, 256, 0) < 0) {
		ret = -ENOMEM;
		goto failed;
	}

	/*
	 * Get IRQ number.
	 */
	res = platform_get_resource(pdev, IORESOURCE_IRQ, 0);
	if (res == NULL)
		return -EINVAL;

#if 0
	/*
	 * Register irq handler.
	 */
	ret = request_irq(res->start, pxa168fb_handle_irq, IRQF_SHARED,
				mi->id, fi);

	if (ret < 0)
	{
		/*
		 *
		 */
		dev_err(&pdev->dev, "Failed to register pxa168fb: %d\n", ret);
		ret = -ENXIO;
		goto failed;
	}
#endif

	/*
	 * Register framebuffer.
	 */
	ret = register_framebuffer(fi);
	if (ret < 0) {
		dev_err(&pdev->dev, "Failed to register pxa168fb: %d\n", ret);
		ret = -ENXIO;
		goto failed;
	}

	printk(KERN_INFO "pxa168fb_ovly: frame buffer device was loaded"
		" to /dev/fb%d <%s>.\n", fi->node, fi->fix.id);

#ifdef OVLY_DVFM_CONSTRAINT
	dvfm_register("overlay1", &dvfm_dev_idx);
#endif

#ifdef CONFIG_PXA688_VDMA
	ret = device_create_file(&pdev->dev, &dev_attr_vdma);
	if (ret < 0) {
		pr_err("device attr create fail: %d\n", ret);
		return ret;
	}
#endif

	ret = device_create_file(&pdev->dev, &dev_attr_debug);
	if (ret < 0) {
		pr_err("device attr create fail: %d\n", ret);
		goto failed;
	}

	return 0;

failed:
	platform_set_drvdata(pdev, NULL);
	fb_dealloc_cmap(&fi->cmap);
	if (fbi->fb_start != NULL) {
		if (fbi->mem_status)
			free_pages((unsigned long)fbi->fb_start,
				get_order(max_fb_size));
		else
			dma_free_writecombine(fbi->dev, max_fb_size,
				fbi->fb_start, fbi->fb_start_dma);
	}
	if (fbi->reg_base != NULL)
		iounmap(fbi->reg_base);
	kfree(fbi);
	return ret;
}

#ifdef CONFIG_PM
#if 0
static int pxa168fb_vid_suspend(struct platform_device *pdev, pm_message_t mesg)
{
	struct pxa168fb_info *fbi = platform_get_drvdata(pdev);
	struct fb_info *fi = fbi->fb_info;

#ifdef FB_PM_DEBUG
	pxa168fb_rw_all_regs(fbi, g_regs1, 1);
#endif

	if (mesg.event & PM_EVENT_SLEEP)
		fb_set_suspend(fi, 1);
	pdev->dev.power.power_state = mesg;

#ifdef FB_PM_DEBUG
	pxa168fb_rw_all_regs(fbi, g_regs, 0);
#endif
	printk(KERN_INFO "pxa168fb_ovly.%d suspended, state = %d.\n", fbi->id, mesg.event);

	return 0;
}

static int pxa168fb_vid_resume(struct platform_device *pdev)
{
	struct pxa168fb_info *fbi = platform_get_drvdata(pdev);
	struct fb_info *fi = fbi->fb_info;

	fb_set_suspend(fi, 0);

#ifdef FB_PM_DEBUG
	{
		u32 i;
		u32 reg;

		for (i = 0xC0; i <= 0x01C4; i += 4) {
			reg = readl(fbi->reg_base + i);
			if (reg != g_regs1[i])
				printk("Register 0x%08x: 0x%08x - 0x%08x.\n",
						i, g_regs1[i], reg);
		}
	}
#endif

	printk(KERN_INFO "pxa168fb_ovly.%d resumed.\n", fbi->id);

	return 0;
}
#endif
#endif


static struct platform_driver pxa168fb_driver = {
	.probe		= pxa168fb_probe,
/*	.remove		= pxa168fb_remove,		*/
#ifdef CONFIG_PM
//	.suspend	= pxa168fb_vid_suspend,
//	.resume		= pxa168fb_vid_resume,
#endif
	.driver		= {
		.name	= "pxa168fb_ovly",
		.owner	= THIS_MODULE,
	},
};

static int __devinit pxa168fb_init(void)
{
	return platform_driver_register(&pxa168fb_driver);
}

/*module_init(pxa168fb_init);*/
late_initcall(pxa168fb_init);

MODULE_DESCRIPTION("Framebuffer driver for PXA168");
MODULE_LICENSE("GPL");
