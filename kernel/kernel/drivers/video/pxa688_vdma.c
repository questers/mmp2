/*
 * linux/drivers/video/pxa168fb.c -- Marvell PXA168 LCD Controller
 *
 *  Copyright (C) 2008 Marvell International Ltd.
 *  All rights reserved.
 *
 *  2009-02-16  adapted from original version for PXA168
 *		Kevin Liu <kliu5@marvell.com>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License. See the file COPYING in the main directory of this archive for
 * more details.
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/interrupt.h>
#include <linux/console.h>
#include <linux/slab.h>
#include <linux/fb.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/cpufreq.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/uaccess.h>
#include <linux/proc_fs.h>

#ifdef CONFIG_PXA688_VDMA

#include "pxa168fb.h"
#include <mach/io.h>
#include <mach/irqs.h>
#include <mach/pxa168fb.h>
#include <mach/hardware.h>
#include <mach/cputype.h>
#include <mach/gpio.h>

#include <asm/mach-types.h>
#include <mach/regs-apmu.h>
#include <mach/mfp-mmp2.h>
#include <mach/regs-mpmu.h>
#include <asm/mach-types.h>

#include <plat/imm.h>

#define vdma_ctrl(id)		(id ? VDMA_CTRL_2 : VDMA_CTRL_1)

u32 lcd_pitch_read(struct pxa168fb_info *fbi, int video)
{
	struct lcd_regs *regs = get_regs(fbi->id);
	u32 reg = (u32)&regs->g_pitch;

	if (video)
		reg = (u32)&regs->v_pitch_yc;

	return (__raw_readl(reg) & 0xffff);
}

u32 lcd_height_read(struct pxa168fb_info *fbi, int video)
{
	struct lcd_regs *regs = get_regs(fbi->id);
	u32 reg = (u32)&regs->g_size;

	if (video)
		reg = (u32)&regs->v_size;

	return (__raw_readl(reg) & 0xfff0000) >> 16;
}

u32 lcd_width_read(struct pxa168fb_info *fbi, int video)
{
	struct lcd_regs *regs = get_regs(fbi->id);
	u32 reg = (u32)&regs->g_size;

	if (video)
		reg = (u32)&regs->v_size;

	return (__raw_readl(reg) & 0xfff);
}

u32 vdma_ctrl_read(struct pxa168fb_info *fbi)
{
	u32 reg = (u32)fbi->reg_base + vdma_ctrl(fbi->id);

	return __raw_readl(reg);
}

void vdma_ctrl_write(struct pxa168fb_info *fbi, int value)
{
	u32 reg = (u32)fbi->reg_base + vdma_ctrl(fbi->id);

	__raw_writel(value, reg);
}

void pxa688_vdma_clkset(int en)
{
	if (en)
		writel(readl(APMU_LCD2_CLK_RES_CTRL) | 0x11b,
				APMU_LCD2_CLK_RES_CTRL);
}

int pxa688fb_vdma_squ_free(u32 paddr, u32 immid)
{
	void *vsqu;
	int ret = 0;

	vsqu = (void *)imm_get_virtual_immid(paddr, immid);
	ret = imm_free(vsqu, immid);
	if (ret)
		pr_err("%s: sram free failed!\n", __func__);
	return ret;
}

u32 pxa688fb_imm_register(char *name)
{
	return imm_register_kernel(name);
}

/* malloc sram buffer for squ - 64 lines by default */
u32 pxa688fb_vdma_squ_malloc(unsigned int *psize, u32 immid)
{
	unsigned int sram_freebytes;
	void *vsqu;
	u32 psqu = 0;

	sram_freebytes = imm_get_freespace(0, immid);
	if (*psize) {
		if (sram_freebytes < *psize) {
			pr_err("%s err: sram_freebyter 0x%x, adjust request"
				" size 0x%x to 0x%x\n", __func__,
				sram_freebytes, *psize, sram_freebytes);
			*psize = sram_freebytes;
		} else
			sram_freebytes = *psize;
	} else
		*psize = sram_freebytes;
	pr_debug("%s: vdma sram_size = %d\n", __func__, sram_freebytes);

	vsqu = imm_malloc(sram_freebytes, IMM_MALLOC_HARDWARE |
			IMM_MALLOC_SRAM, immid);
	if(vsqu != NULL) {
		psqu = imm_get_physical(vsqu, immid);
		return psqu;
	} else {
		printk("%s: sram malloc failed!\n", __func__);
		return 0;
	}
}

void pxa688fb_vdma_release(struct pxa168fb_info *fbi)
{
	struct vdma_regs *vdma = (struct vdma_regs *)((u32)fbi->reg_base
			+ VDMA_ARBR_CTRL);
	struct vdma_ch_regs *vdma_ch = fbi->id ? &vdma->ch2 : &vdma->ch1;
	unsigned reg; //, isr, current_time, irq_mask;

#if 0
	isr = readl(fbi->reg_base + SPU_IRQ_ISR);
	if (fbi->id == 0)
		irq_mask = DMA_FRAME_IRQ0_MASK | DMA_FRAME_IRQ1_MASK;
	else if (fbi->id == 1)
		irq_mask = TV_DMA_FRAME_IRQ0_MASK | TV_DMA_FRAME_IRQ1_MASK;
	else
		irq_mask = PN2_DMA_FRAME_IRQ0_MASK | PN2_DMA_FRAME_IRQ1_MASK;
	irq_status_clear(fbi, irq_mask);
	current_time = jiffies;
	while((readl(fbi->reg_base + SPU_IRQ_ISR) &	irq_mask) == 0) {
		if (jiffies_to_msecs(jiffies - current_time) > EOF_TIMEOUT) {
			printk(KERN_ERR"EOF not detected !");
			break;
		}
	}
#endif

	reg = readl(&vdma_ch->ctrl);
	reg &= ~0xF;
	writel(reg, &vdma_ch->ctrl);

	/* disable squ access */
	reg = readl(fbi->reg_base + squln_ctrl(fbi->id));
	reg &= (~0x1);
	writel(reg, fbi->reg_base + squln_ctrl(fbi->id));
#ifdef CONFIG_PXA688_MISC
	/* FIXME */
	if (fbi->id == fb_vsmooth && (gfx_vsmooth || vid_vsmooth))
		writel(reg, fbi->reg_base + squln_ctrl(fb_filter));
#endif
	printk(KERN_DEBUG "%s fbi %d squln_ctrl %x\n", __func__,
		fbi->id, readl(fbi->reg_base + squln_ctrl(fbi->id)));
}

/* convert graphics layer pix fmt to vmode */
static int pixfmt_to_vmode(int video, int vmode)
{
	if (!video) {
		switch(vmode) {
		case PIX_FMT_RGB565:
			vmode = FB_VMODE_RGB565;
			break;
		case PIX_FMT_BGR565:
			vmode = FB_VMODE_BGR565;
			break;
		case PIX_FMT_RGB1555:
			vmode = FB_VMODE_RGB1555;
			break;
		case PIX_FMT_BGR1555:
			vmode = FB_VMODE_BGR1555;
			break;
		case PIX_FMT_RGB888PACK:
			vmode = FB_VMODE_RGB888PACK;
			break;
		case PIX_FMT_BGR888PACK:
			vmode = FB_VMODE_BGR888PACK;
			break;
		case PIX_FMT_RGB888UNPACK:
			vmode = FB_VMODE_RGB888UNPACK;
			break;
		case PIX_FMT_BGR888UNPACK:
			vmode = FB_VMODE_BGR888UNPACK;
			break;
		case PIX_FMT_RGBA888:
			vmode = FB_VMODE_RGBA888;
			break;
		case PIX_FMT_BGRA888:
			vmode = FB_VMODE_BGRA888;
			break;

		case PIX_FMT_YUV422PACK:
			vmode = FB_VMODE_YUV422PACKED;
			break;
		case PIX_FMT_YVU422PACK:
			vmode = FB_VMODE_YUV422PACKED_SWAPUV;
			break;
		case PIX_FMT_YUYV422PACK:
			vmode = FB_VMODE_YUV422PACKED_SWAPYUorV;
			break;
		case PIX_FMT_YUV422PACK_IRE_90_270:
			vmode = FB_VMODE_YUV422PACKED_IRE_90_270;
			break;
		default:
			break;
		};
	}

	return vmode;
}

static int __get_vdma_rot_ctrl(int vmode, int angle, int yuv_format)
{
	int rotation, flag = 0, reg = 0;

	if (!angle || (angle == 1))
		return 0;

	switch (angle) {
		case 90:
			rotation = 1;
			break;
		case 270:
			rotation = 0;
			break;
		case 180:
			rotation = 2;
			break;
		default:
			rotation = 0;
			break;
			/* return rotation; */
	}

	switch(vmode)
	{
		case FB_VMODE_RGB565:
		case FB_VMODE_BGR565:
		case FB_VMODE_RGB1555:
		case FB_VMODE_BGR1555:
			reg = 4 << 2;
			reg |= rotation << 11;
			break;
		case FB_VMODE_YUV422PACKED:
		case FB_VMODE_YUV422PACKED_SWAPUV:
		case FB_VMODE_YUV422PACKED_SWAPYUorV:
			flag = 1;
		case FB_VMODE_YUV422PACKED_IRE_90_270:
			reg = 1 << 5;
			reg |= 1 << 7;
			reg |= 3 << 2;
			reg |= rotation << 11;
			break;
		default:
			reg = 6 << 2;
			reg |= rotation << 11;
			reg |= 0 << 7;
			break;

	}

	if ( vmode == FB_VMODE_YUV422PACKED_IRE_90_270 || flag == 1) {
		if (rotation == 2)
			reg |= 2 << 21;
		else
			reg |= 1 << 21;

		if (vmode == FB_VMODE_YUV422PACKED)
			yuv_format = 1;
		if (vmode == FB_VMODE_YUV422PACKED_SWAPUV)
			yuv_format = 2;
		if (vmode == FB_VMODE_YUV422PACKED_SWAPYUorV)
			yuv_format = 4;

		switch (yuv_format)
		{
			case 1:/*YUV_FORMAT_UYVY*/
				reg |= 1 << 9;
				break;
			case 2:/*YUV_FORMAT_VYUY*/
				reg |= 0 << 9;
				break;
			case 3:/*YUV_FORAMT_YVYU*/
				reg |= 3 << 9;
				break;
			case 4:/*YUV_FORMAT_YUYV*/
				reg |= 2 << 9;
				break;
		}
	}

	return reg;
}

static int __get_vdma_src_sz(struct pxa168fb_info *fbi, int video,
		int vmode, int width, int height)
{
	int res = lcd_pitch_read(fbi, video) * height;

	if (vmode == FB_VMODE_YUV422PACKED_IRE_90_270)
		return res >> 1;
	else
		return res;
}

static int __get_vdma_sa(struct pxa168fb_info *fbi, int video, int vmode,
		int width, int height, int bpp, int rotation)
{
	struct lcd_regs *regs = get_regs(fbi->id);
	int addr = 0;

	if (vmode == FB_VMODE_YUV422PACKED_IRE_90_270)
		bpp = 2;

	if (video)
		addr = readl(&regs->v_y0);
	else
		addr = readl(&regs->g_0);

	switch (rotation)
	{
		case 90:
		break;
		case 180:
		addr += width * height * bpp - 1;
		break;
		case 270:
		addr += height * (width - 1) * bpp;
		break;
	}
	return addr;

}

static int __get_vdma_sz(struct pxa168fb_info *fbi, int video, int vmode,
		int rotation, int width, int height, int bpp)
{
	int src_pitch = lcd_pitch_read(fbi, video);

	if (vmode == FB_VMODE_YUV422PACKED_IRE_90_270)
		bpp = 2;
	switch (rotation)
	{
		case 0:
		case 1:
			return height << 16 | src_pitch;
		case 90:
			return width << 16 | height;
		case 180:
			return width | height << 16;
		case 270:
			return width << 16 | height;
	}
	return 0;

}

static int __get_vdma_pitch(struct pxa168fb_info *fbi, int video, int vmode,
		int rotation, int width, int height, int bpp)
{
	int src_bpp = bpp, src_pitch = lcd_pitch_read(fbi, video);

	if (vmode == FB_VMODE_YUV422PACKED_IRE_90_270)
		src_bpp = 2;
	switch (rotation)
	{
		case 0:
		case 1:
			return src_pitch | (width * bpp << 16);
		case 90:
			return (width * bpp) << 16 | (height * src_bpp);
		case 180:
			return width * src_bpp | (width * bpp << 16);
		case 270:
			return (width * bpp) << 16 | (height * src_bpp);
	}
	return 0;

}

static int __get_vdma_ctrl(struct pxa168fb_info *fbi, int rotation, int line)
{
	if (!rotation || (rotation == 1))
		return (line << 8) | 0xa1;
	else
		return (line << 8) | 0xa5;
}

int pxa688fb_vdma_get_linenum(struct pxa168fb_info *fbi, int video, int angle)
{
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	int mulfactor, lines, lines_exp;
	int pitch = lcd_pitch_read(fbi, video);
	int height = lcd_height_read(fbi, video);

	if (!pitch)
		return 0;
	if (!angle || (angle == 1))
		/* no rotation */
		mulfactor = 2;
	else
		mulfactor = 16;
	lines = (mi->sram_size / pitch)&(~(mulfactor-1));

	if (lines < 2)  return 0; /* at least 2 lines*/
	if (lines > 64) lines = 64;

	for (lines_exp = 0; lines_exp < lines; lines_exp += mulfactor)
	{
		if (height%(lines-lines_exp) == 0)
		{
			break;
		}
	}
	if (lines_exp >= lines)
	{
		return 32;
	}
	lines -= lines_exp;
	pr_debug("lines %d lines_exp %d angle %d mulfactor %d height %d"
		" pitch %d sram_size %d\n", lines, lines_exp, angle,
		mulfactor, height, pitch, mi->sram_size);
	return lines;
}

void pxa688fb_vdma_set(struct pxa168fb_info *fbi, u32 psqu, unsigned int lines,
		unsigned int video, int vmode, int rotation, unsigned format)
{
	struct vdma_regs *vdma = (struct vdma_regs *)((u32)fbi->reg_base
			+ VDMA_ARBR_CTRL);
	struct vdma_ch_regs *vdma_ch = fbi->id ? &vdma->ch2 : &vdma->ch1;
	unsigned int reg, width, height, bpp;

	if (lines < 2) {
		pr_warn("%s lines = %d < 2???\n", __func__, lines);
		return;
	}

	width = lcd_width_read(fbi, video);
	height = lcd_height_read(fbi, video);
	bpp = lcd_pitch_read(fbi, video) / width;
	vmode = pixfmt_to_vmode(video, vmode);

	if (!psqu) {
		pxa688fb_vdma_release(fbi);
		return;
	} else {
		/* select video layer or graphics layer */
		reg = readl(fbi->reg_base + LCD_PN2_SQULN2_CTRL);
		if (video)
			reg |= 1 << (24 + fbi->id);
		else
			reg &= ~(1 << (24 + fbi->id));
		writel(reg, fbi->reg_base + LCD_PN2_SQULN2_CTRL);
	}
	reg = (u32)psqu | ((lines/2-1)<<1 | 0x1);
	writel(reg, fbi->reg_base + squln_ctrl(fbi->id));

	pr_debug("%s psqu %x reg %x, width %d height %d bpp %d lines %d\n",
		__func__, psqu, reg, width, height, bpp, lines);

	/* src/dst addr */
	reg = __get_vdma_sa(fbi, video, vmode, width, height, bpp, rotation);
	writel(reg, &vdma_ch->src_addr);
	writel((u32)psqu, &vdma_ch->dst_addr);

	/* source size */
	reg = __get_vdma_src_sz(fbi, video, vmode, width, height);
	writel(reg, &vdma_ch->src_size);

	/* size */
	reg = __get_vdma_sz(fbi, video, vmode, rotation, width, height, bpp);
	writel(reg, &vdma_ch->dst_size);

	/* pitch */
	reg = __get_vdma_pitch(fbi, video, vmode, rotation, width, height, bpp);
	writel(reg, &vdma_ch->pitch);

	/* rotation ctrl */
	reg = __get_vdma_rot_ctrl(vmode, rotation, format);
	writel(reg, &vdma_ch->rot_ctrl);

	if (vmode == FB_VMODE_YUV422PACKED_SWAPYUorV && rotation == 180) {
		reg = dma_ctrl_read(fbi->id, 0);
		reg &= ~(0x1 << 2);
		dma_ctrl_write(fbi->id, 0, reg);
	}

	/* control */
	reg = __get_vdma_ctrl(fbi, rotation, lines);
	writel(reg, &vdma_ch->ctrl);
}

void pxa688fb_vdma_en(int id, int vid, int enable)
{
	struct pxa168fb_info *fbi = gfx_info.fbi[id];
	struct pxa168fb_info *fbi_sec;
	struct pxa168fb_mach_info *mi;
	struct fb_info *info = fbi->fb_info;
	struct fb_var_screeninfo *var = &info->var;
	int ret;
	u32 reg, val;

	if ((id == 1) && (var->vmode & FB_VMODE_INTERLACED) && enable)
		return;

	fbi = vid ? ovly_info.fbi[id] : gfx_info.fbi[id];
	mi = fbi->dev->platform_data;
	if (fbi->vdma_enable == enable)
		return;

	fbi_sec = vid ? gfx_info.fbi[id] : ovly_info.fbi[id];

	if (enable) {
		reg = readl(fbi->reg_base + squln_ctrl(id));
		if ((reg & 0x1) || fbi_sec->vdma_enable) {
			pr_warn("%s vdma has been enabled for"
				" another layer\n", __func__);
			return;
		}
		if (!mi->sram_paddr) {
			mi->sram_paddr = pxa688fb_vdma_squ_malloc(
				&mi->sram_size, mi->immid);
		}
		if (mi->sram_paddr) {
			fbi->vdma_enable = 1;
			pr_info("%s fbi%s %d vdma enabled\n", __func__,
				vid ? "_ovly" : " ", id);
		}
	} else {
		fbi->vdma_enable = 0;
		val = vdma_ctrl_read(fbi) & 1;
		if (val) {
			pr_debug("%s fbi %d val %x\n",
				__func__, id, val);
			pxa688fb_vdma_release(fbi);
		}
		if (mi->sram_paddr) {
			ret = pxa688fb_vdma_squ_free(mi->sram_paddr, mi->immid);
			if (!ret) {
				mi->sram_paddr = 0;
				pr_info("%s fbi%s %d vdma disabled\n", __func__,
					vid ? "_ovly" : " ", id);
			}
		}
	}
}

ssize_t vdma_show(struct device *dev, struct device_attribute *attr,
		char *buf)
{
	struct pxa168fb_info *fbi = dev_get_drvdata(dev);
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;
	struct vdma_regs *vdma = (struct vdma_regs *)((u32)fbi->reg_base
			+ VDMA_ARBR_CTRL);

	printk("\tvdma switch mode: %d\n", vdma_switch);
	printk("\tmi->vdma_enable:  %d\n", mi->vdma_enable);
	printk("\tfbi->vdma_enable: %d\n", fbi->vdma_enable);

	if (!fbi->vdma_enable) {
		printk("vdma not enabled\n");
		goto out;
	}

	printk("\nvdma regs base 0x%p\n", vdma);
	printk("\tarbr_ctr       (@%3x):\t0x%x\n", (int)(&vdma->arbr_ctr      )&0xfff, readl(&vdma->arbr_ctr       ) );
	printk("\tirq_raw        (@%3x):\t0x%x\n", (int)(&vdma->irq_raw       )&0xfff, readl(&vdma->irq_raw        ) );
	printk("\tirq_mask       (@%3x):\t0x%x\n", (int)(&vdma->irq_mask      )&0xfff, readl(&vdma->irq_mask       ) );
	printk("\tirq_status     (@%3x):\t0x%x\n", (int)(&vdma->irq_status    )&0xfff, readl(&vdma->irq_status     ) );
	printk("\tmdma_arbr_ctrl (@%3x):\t0x%x\n", (int)(&vdma->mdma_arbr_ctrl)&0xfff, readl(&vdma->mdma_arbr_ctrl ) );

	printk("\nch1 regs\n");
	printk("\tdc_saddr       (@%3x):\t0x%x\n", (int)(&vdma->ch1.dc_saddr )&0xfff, readl(&vdma->ch1.dc_saddr ) );
	printk("\tdc_size        (@%3x):\t0x%x\n", (int)(&vdma->ch1.dc_size  )&0xfff, readl(&vdma->ch1.dc_size  ) );
	printk("\tctrl           (@%3x):\t0x%x\n", (int)(&vdma->ch1.ctrl     )&0xfff, readl(&vdma->ch1.ctrl     ) );
	printk("\tsrc_size       (@%3x):\t0x%x\n", (int)(&vdma->ch1.src_size )&0xfff, readl(&vdma->ch1.src_size ) );
	printk("\tsrc_addr       (@%3x):\t0x%x\n", (int)(&vdma->ch1.src_addr )&0xfff, readl(&vdma->ch1.src_addr ) );
	printk("\tdst_addr       (@%3x):\t0x%x\n", (int)(&vdma->ch1.dst_addr )&0xfff, readl(&vdma->ch1.dst_addr ) );
	printk("\tdst_size       (@%3x):\t0x%x\n", (int)(&vdma->ch1.dst_size )&0xfff, readl(&vdma->ch1.dst_size ) );
	printk("\tpitch          (@%3x):\t0x%x\n", (int)(&vdma->ch1.pitch    )&0xfff, readl(&vdma->ch1.pitch    ) );
	printk("\trot_ctrl       (@%3x):\t0x%x\n", (int)(&vdma->ch1.rot_ctrl )&0xfff, readl(&vdma->ch1.rot_ctrl ) );
	printk("\tram_ctrl0      (@%3x):\t0x%x\n", (int)(&vdma->ch1.ram_ctrl0)&0xfff, readl(&vdma->ch1.ram_ctrl0) );
	printk("\tram_ctrl1      (@%3x):\t0x%x\n", (int)(&vdma->ch1.ram_ctrl1)&0xfff, readl(&vdma->ch1.ram_ctrl1) );

	printk("\nch2 regs\n");
	printk("\tdc_saddr       (@%3x):\t0x%x\n", (int)(&vdma->ch2.dc_saddr )&0xfff, readl(&vdma->ch2.dc_saddr ) );
	printk("\tdc_size        (@%3x):\t0x%x\n", (int)(&vdma->ch2.dc_size  )&0xfff, readl(&vdma->ch2.dc_size  ) );
	printk("\tctrl           (@%3x):\t0x%x\n", (int)(&vdma->ch2.ctrl     )&0xfff, readl(&vdma->ch2.ctrl     ) );
	printk("\tsrc_size       (@%3x):\t0x%x\n", (int)(&vdma->ch2.src_size )&0xfff, readl(&vdma->ch2.src_size ) );
	printk("\tsrc_addr       (@%3x):\t0x%x\n", (int)(&vdma->ch2.src_addr )&0xfff, readl(&vdma->ch2.src_addr ) );
	printk("\tdst_addr       (@%3x):\t0x%x\n", (int)(&vdma->ch2.dst_addr )&0xfff, readl(&vdma->ch2.dst_addr ) );
	printk("\tdst_size       (@%3x):\t0x%x\n", (int)(&vdma->ch2.dst_size )&0xfff, readl(&vdma->ch2.dst_size ) );
	printk("\tpitch          (@%3x):\t0x%x\n", (int)(&vdma->ch2.pitch    )&0xfff, readl(&vdma->ch2.pitch    ) );
	printk("\trot_ctrl       (@%3x):\t0x%x\n", (int)(&vdma->ch2.rot_ctrl )&0xfff, readl(&vdma->ch2.rot_ctrl ) );
	printk("\tram_ctrl0      (@%3x):\t0x%x\n", (int)(&vdma->ch2.ram_ctrl0)&0xfff, readl(&vdma->ch2.ram_ctrl0) );
	printk("\tram_ctrl1      (@%3x):\t0x%x\n", (int)(&vdma->ch2.ram_ctrl1)&0xfff, readl(&vdma->ch2.ram_ctrl1) );

	printk("\nlcd related regs\n");
	printk("\tsquln_ctrl     (@%3x):\t0x%x\n", (int)(fbi->reg_base + squln_ctrl(fbi->id))&0xfff, readl(fbi->reg_base + squln_ctrl(fbi->id)));
	printk("\tpn2_squln2_ctrl(@%3x):\t0x%x\n", (int)(fbi->reg_base + LCD_PN2_SQULN2_CTRL)&0xfff, readl(fbi->reg_base + LCD_PN2_SQULN2_CTRL));

	printk("\nbasic info\n");
	printk("\tvdma_lines     %d\n", mi->vdma_lines);
	printk("\tsram_paddr     0x%x\n", mi->sram_paddr);
	printk("\tsram_size      0x%x\n", mi->sram_size);

out:
	return sprintf(buf, "%d\n", fbi->id);
}
ssize_t vdma_store(
		struct device *dev, struct device_attribute *attr,
		const char *buf, size_t size)
{
	int enable = 0;
	char vol[10];
	struct pxa168fb_info *fbi = dev_get_drvdata(dev);
	struct pxa168fb_mach_info *mi = fbi->dev->platform_data;

	if (!mi) {
		pr_err("fbi %d not available\n", fbi->id);
		return size;
	}
	if ('g' == buf[0]) {
		memcpy(vol, (void *)((u32)buf + 1), size - 1);
		enable = (int)simple_strtoul(vol, NULL, 10);
		pxa688fb_vdma_en(fbi->id, 0, enable);
	} else if ('v' == buf[0]) {
		memcpy(vol, (void *)((u32)buf + 1), size - 1);
		enable = (int) simple_strtoul(vol, NULL, 10);
		pxa688fb_vdma_en(fbi->id, 1, enable);
	} else if ('s' == buf[0]) {
		memcpy(vol, (void *)((u32)buf + 1), size - 1);
		vdma_switch = (int) simple_strtoul(vol, NULL, 10);
	}

	return size;
}
DEVICE_ATTR(vdma, S_IRUGO | S_IWUSR, vdma_show, vdma_store);

#endif
