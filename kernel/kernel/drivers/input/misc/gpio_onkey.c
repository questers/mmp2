/**
 * gpio_onkey.c - GPIO ONKEY driver
 *
 * Copyright (C) 2011 Marvell International Ltd.
 *      Yipeng Yao <ypyao@marvell.com>
 *
 * This file is subject to the terms and conditions of the GNU General
 * Public License. See the file "COPYING" in the main directory of this
 * archive for more details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/wakelock.h>
#include <linux/irq.h>
#include <linux/slab.h>
#include <linux/gpio.h>
#include <linux/gpio_onkey.h>
#include <linux/delay.h>

#include <mach/mfp-mmp2.h>
#include <asm/system.h>
#include <asm/io.h>
#include <asm/mach-types.h>


static struct gpio_onkey_info {
	struct input_dev	*idev;
	struct device		*dev;
	int			onkey_gpio;
	int			pwroff_gpio;
	int			reboot_gpio;
} *onkey_pinfo;

void gpio_system_restart(char mode, const char *cmd)
{
	if (gpio_request(onkey_pinfo->reboot_gpio, "reboot")) {
		printk(KERN_ERR "gpio %d request failed\n", onkey_pinfo->reboot_gpio);
		return ;
	}
	/* pull gpio10 up. it can trigger reboot action on g50 */
	gpio_direction_output(onkey_pinfo->reboot_gpio, 1);
	gpio_free(onkey_pinfo->reboot_gpio);
}

void gpio_system_poweroff(void)
{
	if (gpio_request(onkey_pinfo->pwroff_gpio, "pwr_down")) {
		printk(KERN_ERR "gpio %d request failed\n", onkey_pinfo->pwroff_gpio);
		return ;
	}
	printk("prepare to power off board\n");
	gpio_direction_output(onkey_pinfo->pwroff_gpio, 0);
	mdelay(100);
	gpio_free(onkey_pinfo->pwroff_gpio);
	mdelay(5000);
	panic("%s failed\n",__func__);
}


static irqreturn_t gpio_onkey_irq_handler(int irq, void *data)
{
	int gpio = onkey_pinfo->onkey_gpio;
	int val;

	if (gpio_request(gpio, "onkey")) {
		printk(KERN_INFO "gpio %d request failed\n", gpio);
		return IRQ_HANDLED;
	}

	val = gpio_get_value(gpio);
	/* press down */
	if (val)
		input_report_key(onkey_pinfo->idev, KEY_POWER, 1);
	/* release up */
	else
		input_report_key(onkey_pinfo->idev, KEY_POWER, 0);
	input_sync(onkey_pinfo->idev);

	gpio_free(gpio);

	return IRQ_HANDLED;
}

static int __devinit gpio_onkey_probe(struct platform_device *pdev)
{
	struct gpio_onkey_info *info;
	struct gpio_onkey_pdata *pdata = pdev->dev.platform_data;
	int ret = 0;

	info = kzalloc(sizeof(struct gpio_onkey_info), GFP_KERNEL);
	if (!info)
		return -ENOMEM;

	info->onkey_gpio = pdata->onkey_gpio;
	info->pwroff_gpio = pdata->pwroff_gpio;
	info->reboot_gpio = pdata->reboot_gpio;

	if (gpio_request(info->onkey_gpio, "onkey")) {
		printk(KERN_INFO "gpio %d request failed\n", info->onkey_gpio);
		goto out;
	}

	ret = request_irq(gpio_to_irq(info->onkey_gpio), gpio_onkey_irq_handler,
		IRQF_NO_SUSPEND | IRQF_TRIGGER_FALLING | IRQF_TRIGGER_RISING,
		"onkey irq", NULL);
	if (ret) {
		printk(KERN_ERR "Request Onkey irq failed %d\n", ret);
		goto out;
	}

	info->dev = &pdev->dev;
	info->idev = input_allocate_device();
	if (!info->idev) {
		dev_err(info->dev, "Failed to allocate input dev\n");
		ret = -ENOMEM;
		goto out_irq;
	}

	info->idev->name = "gpio_onkey";
	info->idev->phys = "gpio_onkey/input0";
	info->idev->dev.parent = &pdev->dev;
	info->idev->evbit[0] = BIT_MASK(EV_KEY);
	info->idev->keybit[BIT_WORD(KEY_POWER)] = BIT_MASK(KEY_POWER);
	ret = input_register_device(info->idev);
	if (ret) {
		dev_err(info->dev, "Can't register input device: %d\n", ret);
		goto out_idev;
	}

	gpio_free(info->onkey_gpio);
	platform_set_drvdata(pdev, info);

	arm_pm_restart = gpio_system_restart;
	pm_power_off = gpio_system_poweroff;

	device_init_wakeup(&pdev->dev, 1);
	onkey_pinfo = info;

	return 0;

out_idev:
	input_free_device(info->idev);
out_irq:
	free_irq(gpio_to_irq(info->onkey_gpio), info);
	gpio_free(info->onkey_gpio);
out:
	kfree(info);
	return ret;
}

static int __devexit gpio_onkey_remove(struct platform_device *pdev)
{
	struct gpio_onkey_info *info = platform_get_drvdata(pdev);

	if (info) {
		free_irq(gpio_to_irq(info->onkey_gpio), info);
		input_unregister_device(info->idev);
		kfree(info);
	}
	return 0;
}

static struct platform_driver gpio_onkey_driver = {
	.driver		= {
		.name	= "gpio-onkey",
		.owner	= THIS_MODULE,
	},
	.probe		= gpio_onkey_probe,
	.remove		= __devexit_p(gpio_onkey_remove),
};

static int __init gpio_onkey_init(void)
{
	return platform_driver_register(&gpio_onkey_driver);
}
module_init(gpio_onkey_init);

static void __exit gpio_onkey_exit(void)
{
	platform_driver_unregister(&gpio_onkey_driver);
}
module_exit(gpio_onkey_exit);

MODULE_DESCRIPTION("GPIO ONKEY driver");
MODULE_AUTHOR("Yipeng Yao <ypyao@marvell.com>");
MODULE_LICENSE("GPL");
