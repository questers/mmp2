/* 
 * Copyright (C) 2011 Quester Tech, Inc.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/platform_device.h>
#include <linux/workqueue.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/reboot.h>
#include <linux/pm.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <mach/gpio.h>
#include <mach/mfp.h>
#include <mach/mfp-pxa168.h>
#include <plat/mfp.h>
#include <linux/input.h>
#include <mach/timebutton.h>

#include <asm/mach-types.h>
#include <linux/wakelock.h>

enum button_phase
{
	kInactive=0,
	kPressed,
	kDebounced,
	kReleased,//a temp state,it will then ->inactive
};

struct button_data
{
	struct input_dev *input;
	struct timedbutton* button;
	struct delayed_work button_work;

	int phase,old_phase;
};
struct timedbutton_data
{
	struct input_dev *input;
	unsigned int n_buttons;
	struct timedbutton_platform_data* pdata;	
	struct button_data data[0];
};

static void time_button_report_event(struct button_data *bdata,int code)
{
	struct timedbutton* button=bdata->button;
	struct input_dev *input = bdata->input;
	input_event(input, EV_KEY, code, 1);
	input_sync(input);
	input_event(input, EV_KEY, code, 0);	
	input_sync(input);
}

static void timed_button_irq_work(struct work_struct *work)
{
	struct button_data *bdata = container_of(work, struct button_data, button_work.work);
	struct timedbutton *button = bdata->button;
	
	switch(bdata->old_phase)
	{
		case kInactive:
			if(kPressed == bdata->phase)
			{
				bdata->old_phase = bdata->phase;
				bdata->phase = kDebounced;				
				schedule_delayed_work(&bdata->button_work,msecs_to_jiffies(button->key2_debounce)); 		
			}
			break;
		case kPressed:
			if(kDebounced == bdata->phase)
			{
				if(button->key2)
					time_button_report_event(bdata,button->key2);		
				bdata->phase = kReleased;
			}
			else if(kReleased == bdata->phase)
			{
				if(button->key1)
					time_button_report_event(bdata,button->key1);		
			}
			break;
		default:
			bdata->phase = kReleased;			
			break;		
			
	}
	
	if(kReleased==bdata->phase) bdata->old_phase = bdata->phase = kInactive;
}



static irqreturn_t timed_button_isr(int irq, void *dev_id)
{
	struct button_data *bdata = dev_id;
	struct timedbutton *button = bdata->button;
	int state = (gpio_get_value(button->gpio) ? 1 : 0) ^ button->active_low;
	BUG_ON(irq != gpio_to_irq(button->gpio));

	cancel_delayed_work(&bdata->button_work);
	
	if(!!state)
	{
		bdata->phase = kPressed;
		schedule_delayed_work(&bdata->button_work,msecs_to_jiffies(button->key1_debounce));
	}
	else
	{
		bdata->phase = kReleased;
		schedule_delayed_work(&bdata->button_work,msecs_to_jiffies(0));		
	}

	return IRQ_HANDLED;
}



static int __devinit timed_button_setup_key(struct platform_device *pdev,
					 struct timedbutton *button,struct button_data* bdata)
{
	const char *desc = button->name ? button->name : "timebutton";
	struct device *dev = &pdev->dev;
	unsigned long irqflags;
	int irq, error;

	bdata->phase = bdata->old_phase = kInactive;
	
	INIT_DELAYED_WORK(&bdata->button_work,timed_button_irq_work);

	error = gpio_request(button->gpio, desc);
	if (error < 0) {
		dev_err(dev, "failed to request GPIO %d, error %d\n",
			button->gpio, error);
		goto fail2;
	}

	error = gpio_direction_input(button->gpio);
	if (error < 0) {
		dev_err(dev, "failed to configure"
			" direction for GPIO %d, error %d\n",
			button->gpio, error);
		goto fail3;
	}

	irq = gpio_to_irq(button->gpio);
	if (irq < 0) {
		error = irq;
		dev_err(dev, "Unable to get irq number for GPIO %d, error %d\n",
			button->gpio, error);
		goto fail3;
	}

	irqflags = IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING;
	/*
	 * If platform has specified that the button can be disabled,
	 * we don't want it to share the interrupt line.
	 */
	//if (!button->can_disable)
		irqflags |= IRQF_SHARED;

	error = request_irq(irq, timed_button_isr, irqflags, desc, bdata);
	if (error) {
		dev_err(dev, "Unable to claim irq %d; error %d\n",
			irq, error);
		goto fail3;
	}

	return 0;

fail3:
	gpio_free(button->gpio);
fail2:
	return error;


}

static int timed_button_remove(struct platform_device *pdev)
{
	return 0;
}
static int timed_button_probe(struct platform_device *pdev)
{
	struct timedbutton_data* ddata;
	struct timedbutton_platform_data* pdata = pdev->dev.platform_data;
	struct input_dev *input;
	struct device *dev = &pdev->dev;
	int i;
	int error;
	if(!pdata)
	{
		printk(KERN_ERR "%s: no power button device detected\n",
				__FUNCTION__);
		return -ENODEV;		
	}
	ddata = kzalloc(sizeof(struct timedbutton_data)+
		pdata->buttons_number* sizeof(struct button_data),GFP_KERNEL);
	input = input_allocate_device();

	if (!ddata || !input) {
		dev_err(dev, "failed to allocate state\n");
		error = -ENOMEM;
		goto fail1;
	}


	input->name = "timed_button";
	input->phys = "timed_button/input0";
	input->dev.parent = &pdev->dev;

	input->id.bustype = BUS_HOST;
	input->id.vendor = 0x0001;
	input->id.product = 0x0001;
	input->id.version = 0x0100;

	ddata->n_buttons = pdata->buttons_number;
	ddata->input = input;
	ddata->pdata = pdata;
	
	platform_set_drvdata(pdev, ddata);
	
	for(i=0;i<pdata->buttons_number;i++)
	{
		if(pdata->buttons[i].key1||pdata->buttons[i].key1)
		{		
			struct button_data *bdata = &ddata->data[i];
			bdata->input = input;
			bdata->button = &pdata->buttons[i];
			error = timed_button_setup_key(pdev,&pdata->buttons[i],bdata);
			
			if (error < 0) {
				dev_err(&pdev->dev,"setup key %s failed\n",pdata->buttons[i].name?pdata->buttons[i].name:"unknown");
				continue;
			}

			if(pdata->buttons[i].key1)
				input_set_capability(input, EV_KEY, pdata->buttons[i].key1);
			if(pdata->buttons[i].key2)
				input_set_capability(input, EV_KEY, pdata->buttons[i].key2);
				
		}
	}


	error = input_register_device(input);
	if (error) {
		dev_err(dev, "Unable to register input device, error: %d\n",
			error);
		goto fail2;
	}

	return 0;
fail2:	
	while (--i >= 0) {
		free_irq(gpio_to_irq(pdata->buttons[i].gpio), &ddata->data[i]);
		cancel_delayed_work(&ddata->data[i].button_work);
		gpio_free(pdata->buttons[i].gpio);
	}
	platform_set_drvdata(pdev, NULL);
fail1:
	   input_free_device(input);
	   kfree(ddata);
	
	   return error;
}

static int timed_button_suspend(struct platform_device *pdev,pm_message_t state)
{
	return 0;
}
static int timed_button_resume(struct platform_device *pdev)
{
	return 0;
}
static struct platform_driver timed_button_driver = {
	.probe		= timed_button_probe,
	.remove 	= __devexit_p(timed_button_remove),		
	.driver 	= {
		.name	= "time-button",
		.owner	= THIS_MODULE,
	},
	.suspend	= timed_button_suspend,
	.resume		= timed_button_resume,
};

static int __init timed_button_init(void)
{
	int ret;


	ret = platform_driver_register(&timed_button_driver);
	if (ret) {
		printk(KERN_ERR "timed_button_driver register failure\n");
		return ret;
	}
	return 0;
}

static void __exit timed_button_exit(void)
{
	platform_driver_unregister(&timed_button_driver);
}


module_init(timed_button_init);
module_exit(timed_button_exit);

MODULE_LICENSE("GPL");



