/*
 * Marvell HDMI UIO driver
 *
 * Yifan Zhang <zhangyf@marvell.com>
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 * (c) 2010
 *
 */

#include <linux/uio_driver.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/clk.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <mach/uio_hdmi.h>
#include <linux/earlysuspend.h>
#include <mach/addr-map.h>

static atomic_t hdmi_state = ATOMIC_INIT(0);
static int early_suspend_flag;
static int connect_lock;
static int late_disable_flag;

struct hdmi_instance {
	struct clk *clk;
	void *reg_base;
	void *sspa1_reg_base;
	unsigned int gpio;
	unsigned int edid_bus_num;
	struct delayed_work work;
	struct delayed_work hpd_work;
	struct uio_info uio_info;
	struct early_suspend    early_suspend;
};
static unsigned gsspa1_reg_base;

int hdmi_open(struct uio_info *info, struct inode *inode, void *file_priv)
{
	return 0;
}

int hdmi_release(struct uio_info *info, struct inode *indoe, void *file_priv)
{
	return 0;
}

static int hdmi_ioctl(struct uio_info *info, unsigned cmd, unsigned long arg,
		void *file_priv)
{
	unsigned offset, tmp;
	void *argp = (void *)arg;
	struct hdmi_instance *hi =
		container_of(info, struct hdmi_instance, uio_info);
	int hpd = -1;

	switch(cmd) {
	case SSPA1_GET_VALUE:
		if (copy_from_user(&offset, argp, sizeof(offset)))
			return -EFAULT;
		tmp = readl(gsspa1_reg_base + offset);
		if (copy_to_user(argp, &tmp, sizeof(tmp)))
			return -EFAULT;
		break;
	case HPD_PIN_READ:
		hpd = gpio_get_value(hi->gpio);
		/*if disconnected HDMI , late_disable_flag is 1, 300 ms is
		 * the time wait for HDMI is disabled, then disp1_axi_bus
		 * can be disabled. disp1_axi_bus clear will cause HDMI
		 * clock disbaled and any operation not takes effects*/
		if (late_disable_flag && hpd)
			schedule_delayed_work(&hi->hpd_work,
					msecs_to_jiffies(300));
		/* when resume, force disconnect/connect HDMI */
		if (connect_lock == 1) {
			hpd = -1;
			connect_lock = 2;
		} else if (connect_lock == 2) {
			hpd = 0;
			connect_lock = 0;
		}
		pr_debug("early_suspend_flag %d Kernel space: hpd is %d\n",
			early_suspend_flag, hpd);
		if (copy_to_user(argp, &hpd, sizeof(int))) {
			printk("copy_to_user error !~!\n");
			return -EFAULT;
		}
		break;
	case EDID_NUM:
		if (copy_to_user(argp, &hi->edid_bus_num, sizeof(unsigned int))) {
			printk("copy to user error !\n");
			return -EFAULT;
		}
	}
	return 0;
}

static int hdmi_remove(struct platform_device *pdev)
{
	return 0;
}

#ifdef CONFIG_DVFM
#include <mach/dvfm.h>
static int dvfm_dev_idx;
static int dvfm_set = 0;
static void set_dvfm_constraint(void)
{
	printk(KERN_INFO"%s: HDMI ! dvfm_set %d\n", __func__, dvfm_set);
	if (!dvfm_set) {
		dvfm_disable(dvfm_dev_idx);
		dvfm_set = 1;
	}
}

static void unset_dvfm_constraint(void)
{
	printk(KERN_INFO"%s: HDMI ! dvfm_set %d\n", __func__, dvfm_set);
	if (dvfm_set) {
		dvfm_enable(dvfm_dev_idx);
		dvfm_set = 0;
	}
}
#else
static void set_dvfm_constraint(void) {}
static void unset_dvfm_constraint(void) {}
#endif

#ifdef CONFIG_HAS_EARLYSUSPEND
static void hdmi_early_suspend(struct early_suspend *h)
{
	early_suspend_flag = 1;
	if (atomic_read(&hdmi_state) == 1)
		unset_dvfm_constraint();
	return;
}
static void hdmi_late_resume(struct early_suspend *h)
{
	early_suspend_flag = 0;
	if (atomic_read(&hdmi_state) == 1)
		set_dvfm_constraint();
	return;
}
#endif
static int hdmi_suspend(struct platform_device *pdev, pm_message_t mesg)
{
	struct hdmi_instance *hi = platform_get_drvdata(pdev);

	if (atomic_read(&hdmi_state) == 1)
		clk_disable(hi->clk);
	pdev->dev.power.power_state = mesg;

	return 0;
}

static int hdmi_resume(struct platform_device *pdev)
{
	struct hdmi_instance *hi = platform_get_drvdata(pdev);

	if (gpio_get_value(hi->gpio) == 0) {
		/*if connected, reset HDMI*/
		atomic_set(&hdmi_state, 1);
		clk_enable(hi->clk);
		connect_lock = 1;
		/* send disconnect event to upper layer */
		uio_event_notify(&hi->uio_info);
		/*if uio_event_notify both directly, 1 event will be
		 * missed, so delayed_work*/
		schedule_delayed_work(&hi->hpd_work, msecs_to_jiffies(1500));
	} else if (atomic_read(&hdmi_state) == 1) {
		atomic_set(&hdmi_state, 0);
		uio_event_notify(&hi->uio_info);
	}

	return 0;
}

static void hdmi_delayed_func(struct work_struct *work)
{
	struct hdmi_instance *hi = container_of((struct delayed_work *)work,
			struct hdmi_instance, hpd_work);
	if (late_disable_flag) {		
		if (atomic_read(&hdmi_state) == 1)
			clk_disable(hi->clk);
		//clk_disable(hi->clk);
		late_disable_flag = 0;
	} else {
		/* send connect event to upper layer */
		uio_event_notify(&hi->uio_info);
	}

}
static void hdmi_switch_work(struct work_struct *work)
{
	struct hdmi_instance *hi =
		container_of((struct delayed_work *)work, struct hdmi_instance, work);
	int state = gpio_get_value(hi->gpio);

	if(state!=0) {
		state = 0;
		late_disable_flag = 1; /*wait for hdmi disbaled*/
		atomic_set(&hdmi_state, 0);
		if (early_suspend_flag == 0)
			unset_dvfm_constraint();
	} else {
		clk_enable(hi->clk);
		state = 1;
		if (early_suspend_flag == 0)
			set_dvfm_constraint();
		atomic_set(&hdmi_state, 1);
	}
	pr_debug("++++++++++++ %s state %x hdmi_state %d\n", __func__,
		state, atomic_read(&hdmi_state));
}

static irqreturn_t hpd_handler(int irq, struct uio_info *dev_info)
{
	struct hdmi_instance *hi =
		container_of(dev_info, struct hdmi_instance, uio_info);

	pr_debug("----- %s\n", __func__);
	schedule_delayed_work(&hi->work, msecs_to_jiffies(1500));
	return IRQ_HANDLED;
}

static int hdmi_probe(struct platform_device *pdev)
{
	struct resource *res;
	struct hdmi_instance *hi;
	int ret;
	struct uio_hdmi_platform_data *pdata;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	pdata = pdev->dev.platform_data;
	if (res == NULL) {
		printk(KERN_ERR "hdmi_probe: no memory resources given");
		return -ENODEV;
	}

	hi = kzalloc(sizeof(*hi), GFP_KERNEL);
	if (hi == NULL) {
		printk(KERN_ERR "%s: out of memory\n", __func__);
		return -ENOMEM;
	}

	hi->clk = clk_get(NULL, "DISP1AXICLK");
	if (hi->clk == NULL) {
		pr_err("%s: can't get DISP1AXICLK\n", __func__);
		return -EIO;
	}
	hi->reg_base = ioremap(res->start, res->end - res->start + 1);
	if (hi->reg_base == NULL) {
		printk(KERN_ERR "%s: can't remap resgister area", __func__);
		return -ENOMEM;
	}
	hi->sspa1_reg_base = ioremap_nocache(pdata->sspa_reg_base, 0xff);
	if (hi->sspa1_reg_base == NULL) {
		printk(KERN_WARNING "failed to request register memory\n");
		ret = -EBUSY;
		goto out_free;
	}
	gsspa1_reg_base = (unsigned)hi->sspa1_reg_base;

	platform_set_drvdata(pdev, hi);

	hi->uio_info.name = "mmp2-hdmi";
	hi->uio_info.version = "build1";
	hi->uio_info.irq_flags = IRQF_TRIGGER_FALLING | IRQF_TRIGGER_RISING;
	hi->uio_info.handler = hpd_handler;
	hi->gpio = pdata->gpio;
	hi->edid_bus_num = pdata->edid_bus_num;
	if (hi->edid_bus_num == 0)
		hi->edid_bus_num = 6;

	ret = gpio_request(pdata->gpio, pdev->name);
	if (ret < 0)
		goto out_free;

	ret = gpio_direction_input(pdata->gpio);
	if (ret < 0)
		goto out_free;

	hi->uio_info.irq = gpio_to_irq(pdata->gpio);
	if (hi->uio_info.irq < 0) {
		ret = hi->uio_info.irq;
		goto out_free;
	}

	hi->uio_info.mem[0].internal_addr = hi->reg_base;
	hi->uio_info.mem[0].addr = res->start;
	hi->uio_info.mem[0].memtype = UIO_MEM_PHYS;
	hi->uio_info.mem[0].size = res->end - res->start + 1;
	hi->uio_info.mem[0].name = "hdmi-iomap";
	hi->uio_info.priv = hi;

	hi->uio_info.open = hdmi_open;
	hi->uio_info.release = hdmi_release;
	hi->uio_info.ioctl = hdmi_ioctl;
	ret = uio_register_device(&pdev->dev, &hi->uio_info);
	if (ret) {
		printk(KERN_ERR"%s: register device fails !!!\n", __func__);
		goto out_free;
	}

	/* Check HDMI cable when boot up */
	ret = gpio_get_value(hi->gpio);
	printk("%s hpd 0x%x------------\n", __func__, ret);
	if (ret == 0) {
		clk_enable(hi->clk);
		atomic_set(&hdmi_state, 1);
		set_dvfm_constraint();
	}

	INIT_DELAYED_WORK(&hi->work, hdmi_switch_work);
	INIT_DELAYED_WORK(&hi->hpd_work, hdmi_delayed_func);

#ifdef CONFIG_HAS_EARLYSUSPEND
        hi->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN;
        hi->early_suspend.suspend = hdmi_early_suspend;
        hi->early_suspend.resume = hdmi_late_resume;
        register_early_suspend(&hi->early_suspend);
#endif

	return 0;

out_free:
	kfree(hi);

	return ret;
}

static struct platform_driver hdmi_driver = {
	.probe	= hdmi_probe,
	.remove	= hdmi_remove,
	.driver = {
		.name	= "mmp2-hdmi",
		.owner	= THIS_MODULE,
	},
#ifdef CONFIG_PM
	.suspend = hdmi_suspend,
	.resume  = hdmi_resume,
#endif
};

static void __init hdmi_exit(void)
{
	platform_driver_unregister(&hdmi_driver);
}

static int __init hdmi_init(void)
{
#ifdef CONFIG_DVFM
	dvfm_register("HDMI", &dvfm_dev_idx);
#endif
	return platform_driver_register(&hdmi_driver);
}

module_init(hdmi_init);
module_exit(hdmi_exit);

MODULE_DESCRIPTION("UIO driver for Marvell hdmi");
MODULE_LICENSE("GPL");
