/*
 * drivers/uio/uio_vmeta.c
 *
 * Marvell multi-format video decoder engine UIO driver.
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 *
 * Based on an earlier version by Peter Liao.
 */

#include <linux/uio_driver.h>
#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/io.h>
#include <linux/dma-mapping.h>
#include <linux/uio_vmeta.h>
#include <linux/semaphore.h>
#include <linux/slab.h>
#include <linux/clk.h>
#include <mach/regs-apmu.h>
#include <mach/regs-mpmu.h>
#include <mach/cputype.h>

#include <mach/soc_vmeta.h>

#define CONFIG_MEM_FOR_MULTIPROCESS
#define VMETA_HW_CONTEXT_SIZE		SZ_512K
#define VMETA_OBJ_SIZE			SZ_64K
#define KERNEL_SHARE_SIZE		SZ_4K

/* public */
#define UIO_VMETA_NAME		"mmp2-vmeta"
#define UIO_VMETA_VERSION	"build-006"

#define VMETA_DEBUG	1

#if VMETA_DEBUG
#define vmeta_print printk
#else
#define vmeta_print(x, ...)
#endif

#ifdef CONFIG_DVFM
static struct dvfm_lock dvfm_lock = {
	.lock		= __SPIN_LOCK_UNLOCKED(dvfm_lock.lock),
	.dev_idx	= -1,
	.count		= 0,
};

DEFINE_MUTEX(dvfm_mutex);

static void set_dvfm_work_func(struct work_struct *work)
{
#if 0
	struct vmeta_instance *vi;
	int ret = 0;
	vi = container_of(work, struct vmeta_instance, set_dvfm_work);
	if (!vi) {
		printk(KERN_ERR "%s: vi is not available!\n", __func__);
		return;
	}
	if (vi->plat_data && vi->plat_data->set_dvfm_constraint) {
		ret = vi->plat_data->set_dvfm_constraint(vi, dvfm_lock.dev_idx);
		if (ret)
			printk(KERN_ERR "vmeta set_dvfm_constraint error with %d\n", ret);
	}
	vi->power_constraint = 1;
#endif
}

static void unset_dvfm_work_func(struct work_struct *work)
{
	struct vmeta_instance *vi;
	int ret = 0;
	vi = container_of(work, struct vmeta_instance, unset_dvfm_work);
	if (!vi) {
		printk(KERN_ERR "%s: vi is not available!\n", __func__);
		return;
	}
	if (vi->plat_data && vi->plat_data->unset_dvfm_constraint) {
		ret = vi->plat_data->unset_dvfm_constraint(vi, dvfm_lock.dev_idx);
		if (ret)
			printk(KERN_ERR "vmeta unset_dvfm_constraint error with %d\n", ret);
	}
}

static void set_dvfm_constraint(struct vmeta_instance *vi)
{
	int ret = 0;
	mutex_lock(&dvfm_mutex);

	if (timer_pending(&vi->power_timer))
		del_timer(&vi->power_timer);
	if (vi->plat_data && vi->plat_data->set_dvfm_constraint) {
		ret = vi->plat_data->set_dvfm_constraint(vi, dvfm_lock.dev_idx);
		if (ret)
			printk(KERN_ERR "vmeta set_dvfm_constraint error with %d\n", ret);
	}

	mutex_unlock(&dvfm_mutex);
}

static void unset_dvfm_constraint(struct vmeta_instance *vi)
{
	mutex_lock(&dvfm_mutex);
	mod_timer(&vi->power_timer, jiffies + msecs_to_jiffies(vi->power_down_ms));
	mutex_unlock(&dvfm_mutex);
}

static void vmeta_power_timer_handler(unsigned long data)
{
	struct vmeta_instance* vi = (struct vmeta_instance *)data;
	schedule_work(&vi->unset_dvfm_work);
}

#else
static void set_dvfm_work_func(struct work_struct *work) {}
static void unset_dvfm_work_func(struct work_struct *work) {}
static void set_dvfm_constraint(struct vmeta_instance *vi) {}
static void unset_dvfm_constraint(struct vmeta_instance *vi) {}
static void vmeta_power_timer_handler(unsigned long data) {}
#endif

#ifndef VMETA_USE_PLL2
static int vmeta_usb_phy_clk_enable;
#endif

void vmeta_lock_init(struct vmeta_instance *vi)
{
	sema_init(vi->sema, 1);
	sema_init(vi->priv_sema, 1);
}

int vmeta_lock(unsigned long ms, struct vmeta_instance *vi)
{
	int ret;

	ret = down_timeout(vi->sema,msecs_to_jiffies(ms));

	return ret;
}

int vmeta_unlock(struct vmeta_instance *vi)
{
	if (vi->sema->count == 0) {
		up(vi->sema);
		return 0;
	} else if (vi->sema->count == 1) {
		return 0;
	} else {
		return -1;
	}
}

int vmeta_priv_lock(unsigned long ms, struct vmeta_instance *vi)
{
	int ret;

	ret = down_timeout(vi->priv_sema, msecs_to_jiffies(ms));

	return ret;
}

int vmeta_priv_unlock(struct vmeta_instance *vi)
{
	if (vi->priv_sema->count == 0) {
		up(vi->priv_sema);
		return 0;
	} else if (vi->priv_sema->count == 1) {
		return 0;
	} else {
		return -1;
	}
}

int vmeta_power_on(struct vmeta_instance *vi)
{
	int reg;

	mutex_lock(&vi->vmeta_mutex);
	if (vi->power_status == 1) {
		mutex_unlock(&vi->vmeta_mutex);
		return 0;
	}

	reg = readl(APMU_VMETA_CLK_RES_CTRL);
	/* enable Hardware power mode */
	reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_PWR_CTRL;
	reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_PWRUP;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	if (cpu_is_mmp2_z0() || cpu_is_mmp2_z1())
		reg &= ~APMU_VMETA_CLK_RES_CTRL_VMETA_INP_ISB;
	else
		reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_INP_ISB;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_ISB;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	vi->power_status = 1;
	mutex_unlock(&vi->vmeta_mutex);

	return 0;
}

int vmeta_clk_on(struct vmeta_instance *vi)
{
	int reg;

	mutex_lock(&vi->vmeta_mutex);
	if (vi->clk_status != 0) {
		mutex_unlock(&vi->vmeta_mutex);
		return 0;
	}

	set_dvfm_constraint(vi);

#ifndef VMETA_USE_PLL2
	if (!vmeta_usb_phy_clk_enable) {
		/* Use 480MHz USB PHY CLK if video is 1080P */
		if (vi->vop > VMETA_OP_720P_MAX) {
			clk_enable(clk_get(NULL, "USBPHYCLK"));
			vmeta_usb_phy_clk_enable = 1;
		}
	}
#endif

	reg = readl(APMU_VMETA_CLK_RES_CTRL);
	if (cpu_is_mmp2_z0() || cpu_is_mmp2_z1())
		reg &= ~ APMU_VMETA_CLK_RES_CTRL_VMETA_CLK_SEL_Z;
	else{
#ifdef VMETA_USE_PLL2
		u32 pll2_speed;

		pll2_speed = __raw_readl(MPMU_PLL2_CTRL1);
		pll2_speed = (pll2_speed >> 6) & 0x1f;

		reg &= ~ APMU_VMETA_CLK_RES_CTRL_VMETA_CLK_SEL;
		reg &= ~ APMU_VMETA_CLK_RES_CTRL_VMETA_ACLK_SEL_MSK;
		if((pll2_speed <= 0x18) && (pll2_speed >= 0x14)) /*PLL2: 811MHz~1011MHz */
			reg |= (3<<5) | (4<<12); /*Both PLL2/2*/
		else if((pll2_speed <= 0xc) && (pll2_speed >= 0xa)) /*PLL2: 403MHz~528MHz */
			reg |= (2<<5) | (3<<12); /*Both PLL2*/
		else
			reg |= (0<<5) | (5<<12); /*Both PLL1/2*/
#else
		reg &= ~ APMU_VMETA_CLK_RES_CTRL_VMETA_CLK_SEL;
		reg &= ~ APMU_VMETA_CLK_RES_CTRL_VMETA_ACLK_SEL_MSK;
		if (vi->vop > VMETA_OP_720P_MAX)
			reg |= (5<<5) | (6<<12); /*USB PLL 480MHz*/
		else
			reg |= (0 << 5) | (5 << 12); /* 400MHz */
#endif
	}
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_CLK_EN;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_AXICLK_EN;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	reg &= ~ APMU_VMETA_CLK_RES_CTRL_VMETA_RST1;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);
	reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_RST1;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_AXI_RST;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	vi->clk_status = 1;
	mutex_unlock(&vi->vmeta_mutex);

	return 0;
}

int vmeta_clk_enable(struct vmeta_instance *vi)
{
	int reg;

	mutex_lock(&vi->vmeta_mutex);
	if (vi->clk_status != 0) {
		mutex_unlock(&vi->vmeta_mutex);
		return 0;
	}

	set_dvfm_constraint(vi);

	/* FIXME enable OTG PHY clock to feed vmeta */
#ifndef VMETA_USE_PLL2
	if (!vmeta_usb_phy_clk_enable) {
		/* Use 480MHz USB PHY CLK if video is 1080P */
		if (vi->vop > VMETA_OP_720P_MAX) {
			clk_enable(clk_get(NULL, "USBPHYCLK"));
			vmeta_usb_phy_clk_enable = 1;
		}
	}
#endif

	reg = readl(APMU_VMETA_CLK_RES_CTRL);

	if (cpu_is_mmp2_z0() || cpu_is_mmp2_z1())
		reg &= ~ APMU_VMETA_CLK_RES_CTRL_VMETA_CLK_SEL_Z;
	else{
#ifdef VMETA_USE_PLL2
		u32 pll2_speed;

		pll2_speed = __raw_readl(MPMU_PLL2_CTRL1);
		pll2_speed = (pll2_speed >> 6) & 0x1f;

		reg &= ~ APMU_VMETA_CLK_RES_CTRL_VMETA_CLK_SEL;
		reg &= ~ APMU_VMETA_CLK_RES_CTRL_VMETA_ACLK_SEL_MSK;
		if((pll2_speed <= 0x18) && (pll2_speed >= 0x14)) /*PLL2: 811MHz~1011MHz */
			reg |= (3<<5) | (4<<12); /*Both PLL2/2*/
		else if((pll2_speed <= 0xc) && (pll2_speed >= 0xa)) /*PLL2: 403MHz~528MHz */
			reg |= (2<<5) | (3<<12); /*Both PLL2*/
		else
			reg |= (0<<5) | (5<<12); /*Both PLL1/2*/
#else
		reg &= ~ APMU_VMETA_CLK_RES_CTRL_VMETA_CLK_SEL;
		reg &= ~ APMU_VMETA_CLK_RES_CTRL_VMETA_ACLK_SEL_MSK;
		if (vi->vop > VMETA_OP_720P_MAX)
			reg |= (5<<5) | (6<<12); /*USB PLL 480MHz*/
		else
			reg |= (0 << 5) | (5 << 12); /* 400MHz */
#endif
	}

	reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_CLK_EN;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_AXICLK_EN;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_RST1;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_AXI_RST;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);


	vi->clk_status = 1;
	mutex_unlock(&vi->vmeta_mutex);

	return 0;
}

int vmeta_turn_on(struct vmeta_instance *vi)
{
	int ret;

	ret = vmeta_power_on(vi);
	if(ret) {
		return -1;
	}

	ret = vmeta_clk_on(vi);
	if(ret) {
		return -1;
	}

	return 0;
}

int vmeta_open(struct uio_info *info, struct inode *inode,  void *file_priv)
{
	struct vmeta_instance *vi;

	struct uio_listener *priv = file_priv;
	int *p, i;

	priv->extend = kmalloc(sizeof(int)*MAX_VMETA_INSTANCE, GFP_KERNEL);
	if (!priv->extend) {
		printk(KERN_ERR "vmeta open error\n");
		return -1;
	}

	p = (int *)priv->extend;

	for (i = 0; i < MAX_VMETA_INSTANCE; i++)
		p[i] = MAX_VMETA_INSTANCE;

	vi = (struct vmeta_instance *)info->priv;

	printk(KERN_INFO "power on vmeta\n");
	vmeta_turn_on(vi);

	atomic_inc(&vi->mult_usrs_flag);

	return 0;
}

int vmeta_power_off(struct vmeta_instance *vi)
{
	int reg;

	mutex_lock(&vi->vmeta_mutex);
	if (vi->power_status == 0) {
		mutex_unlock(&vi->vmeta_mutex);
		return 0;
	}

	reg = readl(APMU_VMETA_CLK_RES_CTRL);

	reg &= ~APMU_VMETA_CLK_RES_CTRL_VMETA_RST1;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	if (cpu_is_mmp2_z0() || cpu_is_mmp2_z1()) {
		reg &= ~APMU_VMETA_CLK_RES_CTRL_VMETA_AXI_RST;
		writel(reg, APMU_VMETA_CLK_RES_CTRL);
	}

	reg = readl(APMU_VMETA_CLK_RES_CTRL);

	reg &= ~APMU_VMETA_CLK_RES_CTRL_VMETA_ISB;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	if (cpu_is_mmp2_z0() || cpu_is_mmp2_z1())
		reg |= APMU_VMETA_CLK_RES_CTRL_VMETA_INP_ISB;
	else
		reg &= ~APMU_VMETA_CLK_RES_CTRL_VMETA_INP_ISB;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	reg &= ~APMU_VMETA_CLK_RES_CTRL_VMETA_PWRUP;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	unset_dvfm_constraint(vi);

	vi->power_status = 0;
	mutex_unlock(&vi->vmeta_mutex);

	return 0;
}

int vmeta_clk_disable(struct vmeta_instance *vi)
{
	int reg;

	mutex_lock(&vi->vmeta_mutex);
	if (vi->clk_status == 0) {
		mutex_unlock(&vi->vmeta_mutex);
		return 0;
	}

	reg = readl(APMU_VMETA_CLK_RES_CTRL);
	reg &= ~APMU_VMETA_CLK_RES_CTRL_VMETA_CLK_EN;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	reg &= ~APMU_VMETA_CLK_RES_CTRL_VMETA_AXICLK_EN;
	writel(reg, APMU_VMETA_CLK_RES_CTRL);

	unset_dvfm_constraint(vi);

	vi->clk_status = 0;


	/* FIXME disable OTG PHY clock */
#ifndef VMETA_USE_PLL2
	if (vmeta_usb_phy_clk_enable) {
		clk_disable(clk_get(NULL, "USBPHYCLK"));
		vmeta_usb_phy_clk_enable = 0;
	}
#endif

	mutex_unlock(&vi->vmeta_mutex);

	return 0;
}

int vmeta_turn_off(struct vmeta_instance *vi)
{
	int ret;

	ret = vmeta_clk_disable(vi);
	if(ret) {
		return -1;
	}

	ret = vmeta_power_off(vi);
	if(ret) {
		return -1;
	}

	return 0;
}

/*
vco bitmap
31~24		23~16	15~8	0~7
reserved	vop		step	flags
vop:	vmeta op table defined in kernel space
step:	delta vop level;
flags:	0-set vop for vmeta;
		1-increase/decrease vop by the value in step;
		2-max vop;
		3-min vop
e.g.
a) set vmeta as OP3 vco = 0x00030000
b) set vmeta higher vop by 1 step vco = 0x00000101

VOP Definition, see detail uio_vmeta.h
resolution <= VGA          -- VOP=[1~7]
VGA < resolution <=720p    -- VOP=[8~13]
resolution > 720p          -- VOP=[14]
VOP 0/15 are reserved to notify upper layer.
*/
int vmeta_clk_switch(struct vmeta_instance *vi, unsigned long clk_flag)
{
	int ret = -1;
	int flags;
	int vop = -1;

	mutex_lock(&vi->vmeta_mutex);
	vmeta_print(KERN_DEBUG "[vmeta] clk switch 0x%lx\n", clk_flag);
	flags = clk_flag & 0xff;
	if (flags == 0) {
		vop = (clk_flag & (0xff << 16)) >> 16;
		vmeta_print(KERN_DEBUG "[vmeta] set vop as %d\n", vop);
	} else if (flags == 1) {
		signed char step;
		step = (signed char)((clk_flag & (0xff<<8)) >> 8);
		vmeta_print(KERN_DEBUG "[vmeta] +/- step= %d\n", step);
		if (step > 0 && vi->plat_data && vi->plat_data->increase_core_freq)
			ret = vi->plat_data->increase_core_freq(vi, step);
		else if (step < 0 && vi->plat_data && vi->plat_data->decrease_core_freq)
			ret = vi->plat_data->decrease_core_freq(vi, 0 - step);

		if (ret < 0) {
			mutex_unlock(&vi->vmeta_mutex);
			return ret;
		}


		vop = ret;
		vmeta_print(KERN_DEBUG "[vmeta] set vop as %d\n", vop);
	} else if (flags == 2) {
		vop = VMETA_OP_1080P_MAX;
		vmeta_print(KERN_DEBUG "[vmeta] set vop as %d\n", vop);
	} else if (flags == 3) {
		vop = VMETA_OP_VGA;
		vmeta_print(KERN_DEBUG "[vmeta] set vop as %d\n", vop);
	}

	vmeta_print(KERN_DEBUG "[vmeta] original vop =%d\n", vi->vop);
	if (vi->vop != vop)
		vi->vop = vop;
	/* if out of range, set as highest op */
	if (vi->vop <= VMETA_OP_MIN || vi->vop >= VMETA_OP_MAX)
		vi->vop = VMETA_OP_1080P_MAX;
	vmeta_print(KERN_DEBUG "[vmeta] set vop to %d\n", vi->vop);

	ret = vi->vop;
	mutex_unlock(&vi->vmeta_mutex);
	return ret;
}

int vmeta_release(struct uio_info *info, struct inode *inode, void *file_priv)
{
	struct vmeta_instance *vi;
	kernel_share *ks;
	int current_id = 0;
	struct uio_listener *priv = file_priv;
	int *p, i;

	if (!priv)
		return -1;
	p = (int *)priv->extend;

	vi = (struct vmeta_instance *)info->priv;
	ks = (kernel_share *)vi->uio_info.mem[3].internal_addr;

	for (i = 0; i < MAX_VMETA_INSTANCE; i++) {
		if (p[i] >= 0 && p[i] < MAX_VMETA_INSTANCE) {
			current_id = p[i];
			printk(KERN_INFO "vmeta release current tgid(%d) pid(%d), id to be closed=%d, active id=%d\n", \
				current->tgid, current->pid, current_id, ks->active_user_id);

			mutex_lock(&vi->vmeta_mutex);

			/* in case, current instance have been locked */
			if (ks->active_user_id == current_id) {
				ks->active_user_id = MAX_VMETA_INSTANCE;
				if (ks->lock_flag == VMETA_LOCK_ON) {
					printk(KERN_ERR "vmeta error , instance id(%d) holds the lock and exit abnormally\n", current_id);
					ks->lock_flag = VMETA_LOCK_FORCE_INIT;
					vmeta_unlock(vi);
				}
			}

			/* in case, it's an abnormal exit, we should clear the instance */
			if (ks->user_id_list[current_id].status != 0) {
				printk(KERN_ERR "vmeta error, clear instance[%d], previous status=0x%x\n", \
					current_id, ks->user_id_list[current_id].status);
				ks->ref_count--;
				memset(&(ks->user_id_list[current_id]), 0x0, sizeof(id_instance));
				printk("ref_count=%d, lock flag=%d, active_user_id=%d\n", ks->ref_count, ks->lock_flag, current_id);
			}

			mutex_unlock(&vi->vmeta_mutex);
			p[i] = MAX_VMETA_INSTANCE;
		}
	}

	mutex_lock(&vi->vmeta_mutex);
	if (ks->ref_count == 0) {
		mutex_unlock(&vi->vmeta_mutex);
		vmeta_turn_off(vi);
		mutex_lock(&vi->vmeta_mutex);
#ifdef CONFIG_DVFM
		if (vi->plat_data && vi->plat_data->clean_dvfm_constraint) {
			vi->plat_data->clean_dvfm_constraint(vi, dvfm_lock.dev_idx);
			printk(KERN_DEBUG "vmeta op clean up\n");
		}
#endif
		vi->vop = VMETA_OP_INVALID;
		vi->vop_real = VMETA_OP_INVALID;
	}

	atomic_dec(&vi->mult_usrs_flag);

	mutex_unlock(&vi->vmeta_mutex);
	kfree(priv->extend);

	return 0;
}

/* Either register and unregister use the same function */
int vmeta_reg_unreg(struct uio_listener *file_priv, int uid)
{
	int *p = (int *)file_priv->extend;
	int i;

	if (!p) {
		printk(KERN_ERR "vmeta register error, point NULL\n");
		return -1;
	}

	for (i = 0; i < MAX_VMETA_INSTANCE; i++) {
		if (p[i] == uid) {
			p[i] = MAX_VMETA_INSTANCE;
			printk(KERN_DEBUG "vmeta kern unregister p[%d]= %d \n", i, uid);
			return 0;
		}
	}

	for (i = 0; i < MAX_VMETA_INSTANCE; i++) {
		if (p[i] < 0 || p[i] >= MAX_VMETA_INSTANCE)
			break;
	}

	if (i >= MAX_VMETA_INSTANCE) {
		printk(KERN_ERR "vmeta kern register full \n");
		return -1;
	}

	p[i] = uid;
	printk(KERN_DEBUG "vmeta kern register p[%d]= %d \n", i, p[i]);
	return 0;
}

#ifdef CONFIG_UIO_VMETA_POLLING_MODE
static void vmeta_irq_poll_timer_handler(unsigned long data)
{
	struct vmeta_instance *vi = (struct vmeta_instance *)data;

	uio_event_notify(&vi->uio_info);
	mod_timer(&vi->irq_poll_timer, jiffies + HZ/100);/*10ms timer*/
}
#endif

static irqreturn_t
vmeta_irq_handler(int irq, struct uio_info *dev_info)
{
	struct vmeta_instance *priv = dev_info->priv;

	/* Just disable the interrupt in the interrupt controller, and
	 * remember the state so we can allow user space to enable it later.
	 */

	if (!test_and_set_bit(0, &priv->flags))
		disable_irq_nosync(irq);

	return IRQ_HANDLED;
}

static int vmeta_irqcontrol(struct uio_info *dev_info, s32 irq_on)
{
	struct vmeta_instance *priv = dev_info->priv;
	unsigned long flags;

	/* Allow user space to enable and disable the interrupt
	 * in the interrupt controller, but keep track of the
	 * state to prevent per-irq depth damage.
	 *
	 * Serialize this operation to support multiple tasks.
	 */

	spin_lock_irqsave(&priv->lock, flags);
	if (irq_on) {
		if (test_and_clear_bit(0, &priv->flags))
			enable_irq(dev_info->irq);
	} else {
		if (!test_and_set_bit(0, &priv->flags))
			disable_irq(dev_info->irq);
	}
	spin_unlock_irqrestore(&priv->lock, flags);

	return 0;
}

static int vmeta_ioctl(struct uio_info *info, unsigned int cmd, unsigned long arg, void *file_priv)
{
	int ret = 0;
	struct vmeta_instance *priv = info->priv;

	switch(cmd) {/*Fix ME: Add real control here*/
		case VMETA_CMD_POWER_ON:
			printk(KERN_DEBUG "VMETA CMD POWER ON");
			ret = vmeta_power_on(priv);
			break;
		case VMETA_CMD_POWER_OFF:
			printk(KERN_DEBUG "VMETA CMD POWER OFF");
			ret = vmeta_power_off(priv);
			break;
		case VMETA_CMD_CLK_ON:
			ret = vmeta_clk_enable(priv);
			break;
		case VMETA_CMD_CLK_OFF:
			ret = vmeta_clk_disable(priv);
			break;
		case VMETA_CMD_CLK_SWITCH:
			printk(KERN_DEBUG "VMETA CMD CLK SWITCH\n");
			ret = vmeta_clk_switch(priv, arg);
			break;
		case VMETA_CMD_LOCK:
			ret = vmeta_lock(arg, priv);
			break;
		case VMETA_CMD_UNLOCK:
			ret = vmeta_unlock(priv);
			break;
		case VMETA_CMD_PRIV_LOCK:
			ret = vmeta_priv_lock(arg, priv);
			break;
		case VMETA_CMD_PRIV_UNLOCK:
			ret = vmeta_priv_unlock(priv);
			break;
		case VMETA_CMD_REG_UNREG:
			ret = vmeta_reg_unreg((struct uio_listener *)file_priv, (int)arg);
		default:
			break;
	}

	return ret;
}

static int vmeta_probe(struct platform_device *pdev)
{
	struct resource *res;
	struct vmeta_instance *vi;
	int ret;
	int irq;
#ifdef CONFIG_MEM_FOR_MULTIPROCESS
	dma_addr_t mem_dma_addr;
	void *mem_vir_addr;
	kernel_share* p_ks;
#endif

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (res == NULL) {
		printk(KERN_ERR "vmeta_probe: no memory resources given\n");
		return -ENODEV;
	}
	
#ifndef CONFIG_UIO_VMETA_POLLING_MODE
	irq = platform_get_irq(pdev,0);
	if(irq < 0){
		printk(KERN_ERR "vmeta_probe: no irq resources given in interrupt mode\n");
		return -ENODEV;
	}
#endif

	vi = kzalloc(sizeof(*vi), GFP_KERNEL);
	if (vi == NULL) {
		printk(KERN_ERR "vmeta_probe: out of memory\n");
		return -ENOMEM;
	}

	vi->reg_base = ioremap(res->start, res->end - res->start + 1);
	if (vi->reg_base == NULL) {
		printk(KERN_ERR "vmeta_probe: can't remap register area\n");
		ret = -ENOMEM;
		goto out_free;
	}

	vi->plat_data = (struct vmeta_plat_data *)pdev->dev.platform_data;

	platform_set_drvdata(pdev, vi);

	spin_lock_init(&vi->lock);
	vi->flags = 0; /* interrupt is enabled to begin with */

	vi->uio_info.name = UIO_VMETA_NAME;
	vi->uio_info.version = UIO_VMETA_VERSION;
	vi->uio_info.mem[0].internal_addr = vi->reg_base;
	vi->uio_info.mem[0].addr = res->start;
	vi->uio_info.mem[0].memtype = UIO_MEM_PHYS;
	vi->uio_info.mem[0].size = res->end - res->start + 1;

#ifdef CONFIG_MEM_FOR_MULTIPROCESS
	mem_vir_addr = dma_alloc_coherent(&pdev->dev, VMETA_HW_CONTEXT_SIZE, 
			&mem_dma_addr, GFP_KERNEL);
	if (!mem_vir_addr) {
		ret = -ENOMEM;
		goto out_free;
	}
	vi->uio_info.mem[1].internal_addr = mem_vir_addr;
	vi->uio_info.mem[1].addr = mem_dma_addr;
	vi->uio_info.mem[1].memtype = UIO_MEM_PHYS;
	vi->uio_info.mem[1].size = VMETA_HW_CONTEXT_SIZE;

	mem_vir_addr = dma_alloc_coherent(&pdev->dev, VMETA_OBJ_SIZE,
			&mem_dma_addr, GFP_KERNEL);
	if (!mem_vir_addr) {
		ret = -ENOMEM;
		goto out_free;
	}
	vi->uio_info.mem[2].internal_addr = mem_vir_addr;
	vi->uio_info.mem[2].addr = mem_dma_addr;
	vi->uio_info.mem[2].memtype = UIO_MEM_PHYS;
	vi->uio_info.mem[2].size = VMETA_OBJ_SIZE;

	/* for vmeta driver internally and shared between user space and kernel space*/
	mem_vir_addr = dma_alloc_coherent(&pdev->dev, KERNEL_SHARE_SIZE,
			&mem_dma_addr, GFP_KERNEL);
	if (!mem_vir_addr) {
		ret = -ENOMEM;
		goto out_free;
	}

	memset(mem_vir_addr, 0, KERNEL_SHARE_SIZE);

	vi->uio_info.mem[3].internal_addr = mem_vir_addr;
	vi->uio_info.mem[3].addr = mem_dma_addr;
	vi->uio_info.mem[3].memtype = UIO_MEM_PHYS;
	vi->uio_info.mem[3].size = KERNEL_SHARE_SIZE;

	p_ks = (kernel_share*) mem_vir_addr;
	p_ks->active_user_id = MAX_VMETA_INSTANCE;
#endif

#ifdef CONFIG_UIO_VMETA_POLLING_MODE
	vi->uio_info.irq = UIO_IRQ_CUSTOM;
	init_timer(&vi->irq_poll_timer);
	vi->irq_poll_timer.data = (unsigned long)vi;
	vi->irq_poll_timer.function = vmeta_irq_poll_timer_handler;
#else
	vi->uio_info.irq_flags = IRQF_DISABLED;
  	vi->uio_info.irq = irq;	
	vi->uio_info.handler = vmeta_irq_handler; 
	vi->uio_info.irqcontrol = vmeta_irqcontrol;
#endif
	vi->uio_info.priv = vi;

	vi->uio_info.open = vmeta_open;
	vi->uio_info.release = vmeta_release;
	vi->uio_info.ioctl = vmeta_ioctl;
	ret = uio_register_device(&pdev->dev, &vi->uio_info);
	if (ret)
		goto out_free;

	vi->sema = kzalloc(sizeof(struct semaphore), GFP_KERNEL);
	vi->priv_sema = kzalloc(sizeof(struct semaphore), GFP_KERNEL);

	if (vi->sema == NULL || vi->priv_sema == NULL) {
		printk(KERN_ERR "vmeta->sema: out of memory\n");
		return -ENOMEM;
	}

	mutex_init(&vi->vmeta_mutex);
	vmeta_lock_init(vi);

#ifdef CONFIG_UIO_VMETA_POLLING_MODE
	mod_timer(&vi->irq_poll_timer, jiffies + HZ/100);
#endif
	init_timer(&vi->power_timer);
	vi->power_timer.data = (unsigned long)vi;
	vi->power_timer.function = vmeta_power_timer_handler;
	vi->power_down_ms = 500;

	vi->vop = VMETA_OP_INVALID;
	vi->vop_real = VMETA_OP_INVALID;

	atomic_set(&vi->mult_usrs_flag, -1);

	INIT_WORK(&vi->set_dvfm_work, set_dvfm_work_func);
	INIT_WORK(&vi->unset_dvfm_work, unset_dvfm_work_func);

#ifdef CONFIG_DVFM
	if (vi->plat_data && vi->plat_data->init_dvfm_constraint)
		vi->plat_data->init_dvfm_constraint(vi, dvfm_lock.dev_idx);
#endif

	return 0;

out_free:
	kfree(vi->sema);
	kfree(vi->priv_sema);
	kfree(vi);

	return ret;
}

static int vmeta_remove(struct platform_device *pdev)
{
	struct vmeta_instance *vi = platform_get_drvdata(pdev);
#ifdef CONFIG_UIO_VMETA_POLLING_MODE
	del_timer_sync(&vi->irq_poll_timer);
#endif
	del_timer_sync(&vi->power_timer);
#ifdef CONFIG_MEM_FOR_MULTIPROCESS
	dma_free_coherent(&pdev->dev, VMETA_HW_CONTEXT_SIZE, 
		vi->uio_info.mem[1].internal_addr, vi->uio_info.mem[1].addr);
	dma_free_coherent(&pdev->dev, VMETA_OBJ_SIZE, 
		vi->uio_info.mem[2].internal_addr, vi->uio_info.mem[2].addr);
	dma_free_coherent(&pdev->dev, KERNEL_SHARE_SIZE, 
		vi->uio_info.mem[3].internal_addr, vi->uio_info.mem[3].addr);
#endif
	uio_unregister_device(&vi->uio_info);

	iounmap(vi->reg_base);

	kfree(vi->sema);
	kfree(vi->priv_sema);
	kfree(vi);

	return 0;
}

static void vmeta_shutdown(struct platform_device *dev)
{
}

#ifdef CONFIG_PM
static int vmeta_suspend(struct platform_device *dev, pm_message_t state)
{
#ifndef VMETA_USE_PLL2
	if (vmeta_usb_phy_clk_enable)
		clk_disable(clk_get(NULL, "USBPHYCLK"));
#endif
	return 0;
}

static int vmeta_resume(struct platform_device *dev)
{
#ifndef VMETA_USE_PLL2
	if (vmeta_usb_phy_clk_enable)
		clk_enable(clk_get(NULL, "USBPHYCLK"));
#endif
	return 0;
}
#endif

static struct platform_driver vmeta_driver = {
	.probe		= vmeta_probe,
	.remove		= vmeta_remove,
	.shutdown	= vmeta_shutdown,
#ifdef CONFIG_PM
	.suspend	= vmeta_suspend,
	.resume		= vmeta_resume,
#endif
	.driver = {
		.name	= "mmp2-vmeta",
		.owner	= THIS_MODULE,
	},
};

static int __init vmeta_init(void)
{
#ifdef CONFIG_DVFM
	dvfm_register("mmp2-vmeta", &dvfm_lock.dev_idx);
#endif

	return platform_driver_register(&vmeta_driver);
}

static void __exit vmeta_exit(void)
{
#ifdef CONFIG_DVFM
	dvfm_unregister("mmp2-vmeta", &dvfm_lock.dev_idx);
#endif
	platform_driver_unregister(&vmeta_driver);
}

module_init(vmeta_init);
module_exit(vmeta_exit);

MODULE_DESCRIPTION("UIO driver for Marvell multi-format video codec engine");
MODULE_LICENSE("GPL");
