/* ami602.h - header file for Aichi Steel 6-axis motion sensor
 *
 * Copyright (C) 2010 CyWee Group Ltd.
 * Author: Joe Wei <joewei@cywee.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#ifndef __CWB01C01_H__
#define __CWB01C01_H__

#define DEV_NAME				"ami602"
#define PRECISION				10000
#define ACC_MAX					((2048 + 800 * 2)) * PRECISION
#define MAG_MAX					((2048 + 600 * 6)) * PRECISION
#define MIN_INTERVAL_MS				50
#define DEFAULT_SAMPLE_INTERVAL		200


#define AMI602IO						 	 0x82
#define AMI602_IOCTL_INIT                  _IO(AMI602IO, 0x01)
#define AMI602_IOCTL_READ_CHIPINFO         _IOR(AMI602IO, 0x02, int)
#define AMI602_IOCTL_READ_SENSORDATA       _IOR(AMI602IO, 0x03, int)
#define AMI602_CMD_GET_FIRMWARE				0x17
#define AMI602_CMD_GET_MES					0x14
#define AMI602_CMD_REQ_MES					0x55
#define AMI602_CMD_SET_PWR_DOWN				0x57
#define AMI602_CMD_SET_AEN					0x74
#define AMI602_CMD_SET_SUSPEND				0x75
#define AMI602_CMD_GET_MES_SUSPEND			0x28
#define AMI602_CMD_SET_INDEX_DAT			0x63
#define AMI602_CMD_GET_DAT					0x21
#define AMI602_BUFSIZE						256


#endif /* __CWB01C01_H__ */
