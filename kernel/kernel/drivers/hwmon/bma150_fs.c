/*
*  bma150_fs.c - ST BMA150 accelerometer driver
*
*  Copyright (C) 2007-2008 Yan Burman
*  Copyright (C) 2008 Eric Piel
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/dmi.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/input.h>
#include <linux/kthread.h>
#include <linux/semaphore.h>
#include <linux/delay.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include <linux/freezer.h>
#include <linux/uaccess.h>
#include <linux/proc_fs.h>
#include <linux/i2c.h>
#include <asm/atomic.h>
#include <linux/fs.h>
#include <asm/system.h>
#include <linux/miscdevice.h>
#include <linux/earlysuspend.h>
#include "bma150_fs.h"

static struct i2c_bma150 bma_dev;
static struct i2c_client *g_client;
struct work_struct bma150_work;

static int bma150_get_xyz_axis_adjusted(int *x, int *y, int *z);
static void bma150_increase_use(void);
static void bma150_decrease_use(void);
int bma150_misc_on;

#define DEBUG 0

static int bma150_misc_open(struct inode *inode, struct file *file)
{
#if DEBUG
	printk(KERN_INFO "%s", __func__);
#endif
	return nonseekable_open(inode, file);
}

static int bma150_misc_release(struct inode *inode, struct file *file)
{
#if DEBUG
	printk(KERN_INFO "%s", __func__);
#endif
	return 0;
}

static int bma150_misc_ioctl(struct inode *inode, struct file *file,
			     unsigned int cmd, unsigned long arg)
{
	void __user *pa = (void __user *)arg;
	int vec[3] = { 0 };
	int x, y, z;
	int ret;
	static int x_last, y_last, z_last;
	unsigned char flag;

	switch (cmd) {
	case BMA150_IOC_READ_A_XYZ:
		if (bma_dev.is_on == 0 || bma150_misc_on == 0)
			return -EIO;
		ret = bma150_get_xyz_axis_adjusted(&x, &y, &z);
		if (ret) {
			printk(KERN_INFO "read xyz axis failed\n");
			return -EFAULT;
		}

		x = (x * 1 + x_last * 3) / 4;
		y = (y * 1 + y_last * 3) / 4;
		z = (z * 1 + z_last * 3) / 4;
		x_last = x;
		y_last = y;
		z_last = z;

		vec[0] = x - bma_dev.xcalib;
		vec[1] = y - bma_dev.ycalib;
		vec[2] = z - bma_dev.zcalib;

		if (copy_to_user(pa, vec, sizeof(vec)))
			return -EFAULT;
		break;
	case BMA150_IOC_SET_A_YPR:
		break;
	case BMA150_IOC_GET_A_ACTIVE:
		if (bma_dev.is_on == 0)
			flag = 0;
		else
			flag = 1;
		if (copy_to_user(pa, &(flag), sizeof(flag)))
			return -EFAULT;
		break;
	case BMA150_IOC_SET_A_ACTIVE:
		if (copy_from_user(&flag, pa, sizeof(flag)))
			return -EFAULT;

		if (flag != 1 && flag != 0)
			return -EINVAL;
		if (flag == 1)
			bma150_increase_use();
		else
			bma150_decrease_use();
		break;
	default:
		break;
	}

	return 0;
}

static const struct file_operations bma150_misc_fops = {
	.owner = THIS_MODULE,
	.open = bma150_misc_open,
	.release = bma150_misc_release,
	.ioctl = bma150_misc_ioctl,
};

static struct miscdevice bma150_misc_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = BMA150_MISC_NAME,
	.fops = &bma150_misc_fops,
};

//**********************i2c bus function*****************************//

static int i2c_bma150_read_buf(u8 reg_addr, u8 * buf, u8 cnt)
{				/*ret 0 for success */
	int ret = 0;

	if (NULL == g_client)	/* No global client pointer? */
		return -EFAULT;

	ret = i2c_master_send(g_client, &reg_addr, 1);	/*i2c_smbus_write_byte(g_client,reg_addr); */
	if (ret != 1)
		return -EIO;

	ret = i2c_master_recv(g_client, buf, cnt);
	if (ret != cnt)
		return -EIO;

	return 0;
}

static int i2c_bma150_write_buf(u8 reg_addr, u8 * buf, u8 cnt)
{				/*ret 0 for success */
	int ret = 0;

	if (NULL == g_client)	/* No global client pointer? */
		return -EFAULT;

	ret = i2c_smbus_write_byte_data(g_client, reg_addr, *buf);	/*0 for success */

	if (0 == ret)
		return 0;
	else
		return -EIO;
}

//*************************** low level operation **************************************//
static int bma150_detect(void)
{
	int comres = 0;
	unsigned char data;

#if DEBUG
	printk(KERN_INFO "%s", __func__);
#endif

	/*get chip info. */
	bma_dev.dev_addr = BMA150_I2C_ADDR;	/* preset SM380 I2C_addr */
	comres = i2c_bma150_read_buf(CHIP_ID__REG, &data, 1);	/* read Chip Id */
	if (comres < 0) {
#if DEBUG
		printk(KERN_ERR "%s, i2c_bma150_read_buf error, comres = %d\n",
		       __func__, comres);
#endif
		return comres;
	}

	bma_dev.chip_id = BMA150_GET_BITSLICE(data, CHIP_ID);	/* get bitslice */
	comres = i2c_bma150_read_buf(ML_VERSION__REG, &data, 1);	/* read Version reg */
	if (comres < 0)
		return comres;
	bma_dev.ml_version = BMA150_GET_BITSLICE(data, ML_VERSION);	/* get ML Version */
	bma_dev.al_version = BMA150_GET_BITSLICE(data, AL_VERSION);	/* get AL Version */

	return comres;
}

/** Perform soft reset of BMA150 via bus command
*/
int bma150_soft_reset(void)
{
	int comres;
	unsigned char data = 0;
#if DEBUG
	printk(KERN_INFO "%s", __func__);
#endif
	data = BMA150_SET_BITSLICE(data, SOFT_RESET, 1);
	comres = i2c_bma150_write_buf(SOFT_RESET__REG, &data, 1);
	return comres;
}

/**	start BMA150s integrated selftest function
\param st 1 = selftest0, 3 = selftest1 (see also)
\return result of bus communication function
\see BMA150_SELF_TEST0_ON
\see BMA150_SELF_TEST1_ON
*/
int bma150_selftest(unsigned char st)
{
	int comres;
	unsigned char data;
	comres = i2c_bma150_read_buf(SELF_TEST__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, SELF_TEST, st);
	comres |= i2c_bma150_write_buf(SELF_TEST__REG, &data, 1);
	return comres;
}

/**	set bma150s range
\param range
\return  result of bus communication function
\see BMA150_RANGE_2G
\see BMA150_RANGE_4G
\see BMA150_RANGE_8G
*/
int bma150_set_range(char range)
{
	int comres = 0;
	unsigned char data;

	if (range < 3) {
		comres = i2c_bma150_read_buf(RANGE__REG, &data, 1);
		if (comres < 0)
			return comres;
		data = BMA150_SET_BITSLICE(data, RANGE, range);
		comres = i2c_bma150_write_buf(RANGE__REG, &data, 1);
		if (comres < 0)
			return comres;
	}
	return comres;
}

/* readout select range from BMA150
\param *range pointer to range setting
\return result of bus communication function
\see BMA150_RANGE_2G, BMA150_RANGE_4G, BMA150_RANGE_8G
\see bma150_set_range()
*/
int bma150_get_range(unsigned char *range)
{
	int comres = 0;

	comres = i2c_bma150_read_buf(RANGE__REG, range, 1);
	*range = BMA150_GET_BITSLICE(*range, RANGE);
	return comres;
}

/** set BMA150s operation mode
\param mode 0 = normal, 2 = sleep, 3 = auto wake up
\return result of bus communication function
\note Available constants see below
\see BMA150_MODE_NORMAL, BMA150_MODE_SLEEP, BMA150_MODE_WAKE_UP
\see bma150_get_mode()
*/
int bma150_set_mode(unsigned char mode)
{
	int comres = 0;
	unsigned char data1, data2;
#if DEBUG
	printk(KERN_INFO "%s", __func__);
#endif
	if (mode < 4 || mode != 1) {
		comres = i2c_bma150_read_buf(WAKE_UP__REG, &data1, 1);
		if (comres < 0)
			return comres;
		data1 = BMA150_SET_BITSLICE(data1, WAKE_UP, mode);
		comres = i2c_bma150_read_buf(SLEEP__REG, &data2, 1);
		if (comres < 0)
			return comres;
		data2 = BMA150_SET_BITSLICE(data2, SLEEP, (mode >> 1));

		comres = i2c_bma150_write_buf(WAKE_UP__REG, &data1, 1);
		if (comres < 0)
			return comres;
		comres = i2c_bma150_write_buf(SLEEP__REG, &data2, 1);
		if (comres < 0)
			return comres;
		bma_dev.mode = mode;
	}
	return comres;
}

/** get selected mode
\return used mode
\note this function returns the mode stored in \ref bma150_t structure
\see BMA150_MODE_NORMAL, BMA150_MODE_SLEEP, BMA150_MODE_WAKE_UP
\see bma150_set_mode()
*/
unsigned char bma150_get_mode(void)
{
#if DEBUG
	printk(KERN_INFO "%s", __func__);
#endif
	return bma_dev.mode;
}

/** set BMA150 internal filter bandwidth
\param bw bandwidth (see bandwidth constants)
\return result of bus communication function
\see #define BMA150_BW_25HZ, BMA150_BW_50HZ, BMA150_BW_100HZ,
\BMA150_BW_190HZ, BMA150_BW_375HZ,
\BMA150_BW_750HZ, BMA150_BW_1500HZ
\see bma150_get_bandwidth()
*/
int bma150_set_bandwidth(char bw)
{
	int comres = 0;
	unsigned char data;
	if (bw < 8) {
		comres = i2c_bma150_read_buf(BANDWIDTH__REG, &data, 1);
		if (comres < 0)
			return comres;
		data = BMA150_SET_BITSLICE(data, BANDWIDTH, bw);
		comres = i2c_bma150_write_buf(BANDWIDTH__REG, &data, 1);
		if (comres < 0)
			return comres;
	}
	return comres;
}

/** read selected bandwidth from BMA150
\param *bw pointer to bandwidth return value
\return result of bus communication function
\see #define BMA150_BW_25HZ, BMA150_BW_50HZ, BMA150_BW_100HZ,
\BMA150_BW_190HZ, BMA150_BW_375HZ,BMA150_BW_750HZ, BMA150_BW_1500HZ
\see bma150_set_bandwidth()
*/
int bma150_get_bandwidth(unsigned char *bw)
{
	int comres = 1;

	comres = i2c_bma150_read_buf(BANDWIDTH__REG, bw, 1);
	*bw = BMA150_GET_BITSLICE(*bw, BANDWIDTH);
	return comres;
}

/** set BMA150 auto wake up pause
\param wup wake_up_pause parameters
\return result of bus communication function
\see BMA150_WAKE_UP_PAUSE_20MS, BMA150_WAKE_UP_PAUSE_80MS,
\BMA150_WAKE_UP_PAUSE_320MS,BMA150_WAKE_UP_PAUSE_2560MS
\see bma150_get_wake_up_pause()
*/

int bma150_set_wake_up_pause(unsigned char wup)
{
	int comres = 0;
	unsigned char data;

	comres = i2c_bma150_read_buf(WAKE_UP_PAUSE__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, WAKE_UP_PAUSE, wup);
	comres |= i2c_bma150_write_buf(WAKE_UP_PAUSE__REG, &data, 1);
	return comres;
}

/** read BMA150 auto wake up pause from image
\param *wup wake up pause read back pointer
\see BMA150_WAKE_UP_PAUSE_20MS, BMA150_WAKE_UP_PAUSE_80MS,
\BMA150_WAKE_UP_PAUSE_320MS,BMA150_WAKE_UP_PAUSE_2560MS
\see bma150_set_wake_up_pause()
*/
int bma150_get_wake_up_pause(unsigned char *wup)
{
	int comres = 1;
	unsigned char data;
	comres = i2c_bma150_read_buf(WAKE_UP_PAUSE__REG, &data, 1);
	*wup = BMA150_GET_BITSLICE(data, WAKE_UP_PAUSE);
	return comres;
}

/* Thresholds and Interrupt Configuration */
/** set low-g interrupt threshold
\param th set the threshold
\note the threshold depends on configured range. A macro
\ref BMA150_LG_THRES_IN_G() for range to
\register value conversion is available.
\see BMA150_LG_THRES_IN_G()
\see bma150_get_low_g_threshold()
*/
int bma150_set_low_g_threshold(unsigned char th)
{
	int comres;
	comres = i2c_bma150_write_buf(LG_THRES__REG, &th, 1);
	return comres;
}

/** get low-g interrupt threshold
\param *th get the threshold  value from sensor image
\see bma150_set_low_g_threshold()
*/
int bma150_get_low_g_threshold(unsigned char *th)
{
	int comres = 1;
	comres = i2c_bma150_read_buf(LG_THRES__REG, th, 1);
	return comres;
}

/** set low-g interrupt countdown
\param cnt get the countdown value from sensor image
\see bma150_get_low_g_countdown()
*/
int bma150_set_low_g_countdown(unsigned char cnt)
{
	int comres = 0;
	unsigned char data;
	comres = i2c_bma150_read_buf(COUNTER_LG__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, COUNTER_LG, cnt);
	comres |= i2c_bma150_write_buf(COUNTER_LG__REG, &data, 1);
	return comres;
}

/** get low-g interrupt countdown
\param cnt get the countdown  value from sensor image
\see bma150_set_low_g_countdown()
*/
int bma150_get_low_g_countdown(unsigned char *cnt)
{
	int comres = 1;
	unsigned char data;
	comres = i2c_bma150_read_buf(COUNTER_LG__REG, &data, 1);
	*cnt = BMA150_GET_BITSLICE(data, COUNTER_LG);
	return comres;
}

/** set high-g interrupt countdown
\param cnt get the countdown value from sensor image
\see bma150_get_high_g_countdown()
*/
int bma150_set_high_g_countdown(unsigned char cnt)
{
	int comres = 1;
	unsigned char data;
	comres = i2c_bma150_read_buf(COUNTER_HG__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, COUNTER_HG, cnt);
	comres |= i2c_bma150_write_buf(COUNTER_HG__REG, &data, 1);
	return comres;
}

/** get high-g interrupt countdown
\param cnt get the countdown  value from sensor image
\see bma150_set_high_g_countdown()
*/
int bma150_get_high_g_countdown(unsigned char *cnt)
{
	int comres = 0;
	unsigned char data;
	comres = i2c_bma150_read_buf(COUNTER_HG__REG, &data, 1);
	*cnt = BMA150_GET_BITSLICE(data, COUNTER_HG);
	return comres;
}

/** configure low-g duration value
\param dur low-g duration in miliseconds
\see bma150_get_low_g_duration(), bma150_get_high_g_duration(),
\bma150_set_high_g_duration()
*/
int bma150_set_low_g_duration(unsigned char dur)
{
	int comres = 0;
	comres = i2c_bma150_write_buf(LG_DUR__REG, &dur, 1);
	return comres;
}

int bma150_set_low_g_hyst(unsigned char hyst)
{
	int comres = 0;
	unsigned char data;

	comres = i2c_bma150_read_buf(LG_HYST__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, LG_HYST, hyst);
	comres |= i2c_bma150_write_buf(LG_HYST__REG, &data, 1);
	return comres;
}

int bma150_set_high_g_hyst(unsigned char hyst)
{
	int comres = 0;
	unsigned char data;

	comres = i2c_bma150_read_buf(HG_HYST__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, HG_HYST, hyst);
	comres |= i2c_bma150_write_buf(HG_HYST__REG, &data, 1);
	return comres;
}

/** read out low-g duration value from sensor image
\param dur low-g duration in miliseconds
\see bma150_set_low_g_duration(), bma150_get_high_g_duration(),
\bma150_set_high_g_duration()
*/
int bma150_get_low_g_duration(unsigned char *dur)
{
	int comres = 0;
	comres = i2c_bma150_read_buf(LG_DUR__REG, dur, 1);
	return comres;
}

/** set low-g interrupt threshold
\param th set the threshold
\note the threshold depends on configured range. A macro
\ref BMA150_HG_THRES_IN_G() for range to
\register value conversion is available.
\see BMA150_HG_THRES_IN_G()
\see bma150_get_high_g_threshold()
*/
int bma150_set_high_g_threshold(unsigned char th)
{
	int comres = 0;
	comres = i2c_bma150_write_buf(HG_THRES__REG, &th, 1);
	return comres;
}

/** get high-g interrupt threshold
\param *th get the threshold  value from sensor image
\see bma150_set_high_g_threshold()
*/
int bma150_get_high_g_threshold(unsigned char *th)
{
	int comres = 0;
	comres = i2c_bma150_read_buf(HG_THRES__REG, th, 1);
	return comres;
}

/** configure high-g duration value
\param dur high-g duration in miliseconds
\see  bma150_get_high_g_duration(), bma150_set_low_g_duration(),
\bma150_get_low_g_duration()
*/
int bma150_set_high_g_duration(unsigned char dur)
{
	int comres = 0;
	comres = i2c_bma150_write_buf(HG_DUR__REG, &dur, 1);
	return comres;
}

/** read out high-g duration value from sensor image
\param dur high-g duration in miliseconds
\see  bma150_set_high_g_duration(), bma150_get_low_g_duration(), bma150_set_low_g_duration(),
*/
int bma150_get_high_g_duration(unsigned char *dur)
{
	int comres = 0;
	comres = i2c_bma150_read_buf(HG_DUR__REG, dur, 1);
	return comres;
}

/**  set threshold value for any_motion feature
\param th set the threshold a macro \ref BMA150_ANY_MOTION_THRES_IN_G()  is available for that
\see BMA150_ANY_MOTION_THRES_IN_G()
*/
int bma150_set_any_motion_threshold(unsigned char th)
{
	int comres = 0;
	comres = i2c_bma150_write_buf(ANY_MOTION_THRES__REG, &th, 1);
	return comres;
}

/**  get threshold value for any_motion feature
\param *th read back any_motion threshold from image register
\see BMA150_ANY_MOTION_THRES_IN_G()
*/
int bma150_get_any_motion_threshold(unsigned char *th)
{
	int comres = 0;
	comres = i2c_bma150_read_buf(ANY_MOTION_THRES__REG, th, 1);
	return comres;
}

/**  set counter value for any_motion feature
\param amc set the counter value, constants are available for that
\see BMA150_ANY_MOTION_DUR_1, BMA150_ANY_MOTION_DUR_3, BMA150_ANY_MOTION_DUR_5,
\BMA150_ANY_MOTION_DUR_7
*/
int bma150_set_any_motion_count(unsigned char amc)
{
	int comres = 0;
	unsigned char data;
	comres = i2c_bma150_read_buf(ANY_MOTION_DUR__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, ANY_MOTION_DUR, amc);
	comres = i2c_bma150_write_buf(ANY_MOTION_DUR__REG, &data, 1);
	return comres;
}

/**  get counter value for any_motion feature from image register
\param *amc readback pointer for counter value
\see BMA150_ANY_MOTION_DUR_1, BMA150_ANY_MOTION_DUR_3, BMA150_ANY_MOTION_DUR_5,
\BMA150_ANY_MOTION_DUR_7
*/
int bma150_get_any_motion_count(unsigned char *amc)
{
	int comres = 0;
	unsigned char data;
	comres = i2c_bma150_read_buf(ANY_MOTION_DUR__REG, &data, 1);
	*amc = BMA150_GET_BITSLICE(data, ANY_MOTION_DUR);
	return comres;
}

/** set the interrupt mask for BMA150's interrupt features in one mask
\param mask input for interrupt mask
\see BMA150_INT_ALERT, BMA150_INT_ANY_MOTION, BMA150_INT_EN_ADV_INT, BMA150_INT_NEW_DATA,
\BMA150_INT_LATCH, BMA150_INT_HG, BMA150_INT_LG
*/
int bma150_set_interrupt_mask(unsigned char mask)
{
	int comres = 0;
	unsigned char data[4];
	data[0] = mask & BMA150_CONF1_INT_MSK;
	data[2] = ((mask << 1) & BMA150_CONF2_INT_MSK);
	comres = i2c_bma150_read_buf(BMA150_CONF1_REG, &data[1], 1);
	comres |= i2c_bma150_read_buf(BMA150_CONF2_REG, &data[3], 1);
	data[1] &= (~BMA150_CONF1_INT_MSK);
	data[1] |= data[0];
	data[3] &= (~(BMA150_CONF2_INT_MSK));
	data[3] |= data[2];
	comres |= i2c_bma150_write_buf(BMA150_CONF1_REG, &data[1], 1);
	comres |= i2c_bma150_write_buf(BMA150_CONF2_REG, &data[3], 1);
	return comres;
}

/** set the shadow_dis for BMA150's shadow_dis bit
\param ssd input for on/off control
\see BMA150_SHADOW_DIS_OFF, BMA150_SHADOW_DIS_ON
*/
int bma150_set_shadow_dis(unsigned char ssd)
{
	int comres = 0;
	unsigned char data;
	if ((ssd == 1) || (ssd == 0)) {
		comres = i2c_bma150_read_buf(BMA150_CONF2_REG, &data, 1);
		data = BMA150_SET_BITSLICE(data, SHADOW_DIS, ssd);
		comres |= i2c_bma150_write_buf(BMA150_CONF2_REG, &data, 1);
	}
	return comres;
}

/** get the current interrupt mask settings from BMA150 image registers
\param *mask return variable pointer for interrupt mask
\see BMA150_INT_ALERT, BMA150_INT_ANY_MOTION, BMA150_INT_EN_ADV_INT, BMA150_INT_NEW_DATA,
\BMA150_INT_LATCH, BMA150_INT_HG, BMA150_INT_LG
*/
int bma150_get_interrupt_mask(unsigned char *mask)
{
	int comres = 0;
	unsigned char data;
	comres = i2c_bma150_read_buf(BMA150_CONF1_REG, &data, 1);
	*mask = data & BMA150_CONF1_INT_MSK;
	comres |= i2c_bma150_read_buf(BMA150_CONF2_REG, &data, 1);
	*mask = *mask | ((data & BMA150_CONF2_INT_MSK) >> 1);
	return comres;
}

/** resets the BMA150 interrupt status
\note this feature can be used to reset a latched interrupt
*/
int bma150_reset_interrupt(void)
{
	int comres = 0;
	unsigned char data = (1 << RESET_INT__POS);
	comres = i2c_bma150_write_buf(RESET_INT__REG, &data, 1);
	return comres;
}

/* Data Readout */
/** X-axis acceleration data readout
\param *a_x pointer for 16 bit 2's complement data output (LSB aligned)
*/
int bma150_read_accel_x(short *a_x)
{
	int comres;
	unsigned char data[2];
	comres = i2c_bma150_read_buf(ACC_X_LSB__REG, data, 2);
	if (comres)
		return -EIO;
	*a_x = BMA150_GET_BITSLICE(data[0], ACC_X_LSB) |
	    BMA150_GET_BITSLICE(data[1], ACC_X_MSB) << ACC_X_LSB__LEN;
	*a_x = *a_x << (sizeof(short) * 8 - (ACC_X_LSB__LEN + ACC_X_MSB__LEN));
	*a_x = *a_x >> (sizeof(short) * 8 - (ACC_X_LSB__LEN + ACC_X_MSB__LEN));
	return comres;
}

/** Y-axis acceleration data readout
\param *a_y pointer for 16 bit 2's complement data output (LSB aligned)
*/
int bma150_read_accel_y(short *a_y)
{
	int comres;
	unsigned char data[2];

	comres = i2c_bma150_read_buf(ACC_Y_LSB__REG, data, 2);
	if (comres)
		return -EIO;
	*a_y = BMA150_GET_BITSLICE(data[0], ACC_Y_LSB) |
	    BMA150_GET_BITSLICE(data[1], ACC_Y_MSB) << ACC_Y_LSB__LEN;
	*a_y = *a_y << (sizeof(short) * 8 - (ACC_Y_LSB__LEN + ACC_Y_MSB__LEN));
	*a_y = *a_y >> (sizeof(short) * 8 - (ACC_Y_LSB__LEN + ACC_Y_MSB__LEN));
	return comres;
}

/** Z-axis acceleration data readout
\param *a_z pointer for 16 bit 2's complement data output (LSB aligned)
*/
int bma150_read_accel_z(short *a_z)
{
	int comres;
	unsigned char data[2];

	comres = i2c_bma150_read_buf(ACC_Z_LSB__REG, data, 2);
	if (comres)
		return -EIO;
	*a_z = BMA150_GET_BITSLICE(data[0], ACC_Z_LSB) |
	    BMA150_GET_BITSLICE(data[1], ACC_Z_MSB) << ACC_Z_LSB__LEN;
	*a_z = *a_z << (sizeof(short) * 8 - (ACC_Z_LSB__LEN + ACC_Z_MSB__LEN));
	*a_z = *a_z >> (sizeof(short) * 8 - (ACC_Z_LSB__LEN + ACC_Z_MSB__LEN));
	return comres;
}

/** X,Y and Z-axis acceleration data readout
\param *acc pointer to \ref bma150acc_t structure for x,y,z data readout
\note data will be read by multi-byte protocol into a 6 byte structure
*/
int bma150_read_accel_xyz(bma150acc_t * acc)
{
	int comres;
	unsigned char data[6];
	comres = i2c_bma150_read_buf(ACC_X_LSB__REG, &data[0], 6);
	if (comres) {
#if DEBUG
		printk(KERN_INFO
		       "%s, i2c_bma150_read_buf error, comres = %d \n",
		       __func__, comres);
#endif
		return -EIO;
	}
	acc->x = BMA150_GET_BITSLICE(data[0], ACC_X_LSB) |
	    (BMA150_GET_BITSLICE(data[1], ACC_X_MSB) << ACC_X_LSB__LEN);
	acc->x =
	    acc->x << (sizeof(short) * 8 - (ACC_X_LSB__LEN + ACC_X_MSB__LEN));
	acc->x =
	    acc->x >> (sizeof(short) * 8 - (ACC_X_LSB__LEN + ACC_X_MSB__LEN));
	acc->y =
	    BMA150_GET_BITSLICE(data[2],
				ACC_Y_LSB) | (BMA150_GET_BITSLICE(data[3],
								  ACC_Y_MSB) <<
					      ACC_Y_LSB__LEN);
	acc->y =
	    acc->y << (sizeof(short) * 8 - (ACC_Y_LSB__LEN + ACC_Y_MSB__LEN));
	acc->y =
	    acc->y >> (sizeof(short) * 8 - (ACC_Y_LSB__LEN + ACC_Y_MSB__LEN));
	acc->z =
	    BMA150_GET_BITSLICE(data[4],
				ACC_Z_LSB) | (BMA150_GET_BITSLICE(data[5],
								  ACC_Z_MSB) <<
					      ACC_Z_LSB__LEN);
	acc->z =
	    acc->z << (sizeof(short) * 8 - (ACC_Z_LSB__LEN + ACC_Z_MSB__LEN));
	acc->z =
	    acc->z >> (sizeof(short) * 8 - (ACC_Z_LSB__LEN + ACC_Z_MSB__LEN));
#if DEBUG
	printk(KERN_INFO "%s, [x %d], [y %d], [z %d] \n", __func__, acc->x,
	       acc->y, acc->z);
#endif
	return comres;
}

/** check current interrupt status from interrupt status register in BMA150 image register
\param *ist pointer to interrupt status byte
\see BMA150_INT_STATUS_HG, BMA150_INT_STATUS_LG, BMA150_INT_STATUS_HG_LATCHED,
\BMA150_INT_STATUS_LG_LATCHED, BMA150_INT_STATUS_ALERT, BMA150_INT_STATUS_ST_RESULT
*/
int bma150_get_interrupt_status(unsigned char *ist)
{
	int comres = 0;
	comres = i2c_bma150_read_buf(BMA150_STATUS_REG, ist, 1);
	return comres;
}

/** enable/ disable low-g interrupt feature
\param onoff enable=1, disable=0
*/
int bma150_set_low_g_int(unsigned char onoff)
{
	int comres;
	unsigned char data;

	comres = i2c_bma150_read_buf(ENABLE_LG__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, ENABLE_LG, onoff);
	comres |= i2c_bma150_write_buf(ENABLE_LG__REG, &data, 1);
	return comres;
}

/** enable/ disable high-g interrupt feature
\param onoff enable=1, disable=0
*/
int bma150_set_high_g_int(unsigned char onoff)
{
	int comres;
	unsigned char data;

	comres = i2c_bma150_read_buf(ENABLE_HG__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, ENABLE_HG, onoff);
	comres |= i2c_bma150_write_buf(ENABLE_HG__REG, &data, 1);
	return comres;
}

/** enable/ disable any_motion interrupt feature
\param onoff enable=1, disable=0
\note for any_motion interrupt feature usage see also \ref bma150_set_advanced_int()
*/
int bma150_set_any_motion_int(unsigned char onoff)
{
	int comres;
	unsigned char data;
	comres = i2c_bma150_read_buf(EN_ANY_MOTION__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, EN_ANY_MOTION, onoff);
	comres |= i2c_bma150_write_buf(EN_ANY_MOTION__REG, &data, 1);
	return comres;
}

/** enable/ disable alert-int interrupt feature
\param onoff enable=1, disable=0
\note for any_motion interrupt feature usage see also \ref bma150_set_advanced_int()
*/
int bma150_set_alert_int(unsigned char onoff)
{
	int comres;
	unsigned char data;

	comres = i2c_bma150_read_buf(ALERT__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, ALERT, onoff);
	comres |= i2c_bma150_write_buf(ALERT__REG, &data, 1);
	return comres;
}

/** enable/ disable advanced interrupt feature
\param onoff enable=1, disable=0
\see bma150_set_any_motion_int()
\see bma150_set_alert_int()
*/
int bma150_set_advanced_int(unsigned char onoff)
{
	int comres;
	unsigned char data;
	comres = i2c_bma150_read_buf(ENABLE_ADV_INT__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, EN_ANY_MOTION, onoff);
	comres |= i2c_bma150_write_buf(ENABLE_ADV_INT__REG, &data, 1);
	return comres;
}

/** enable/disable latched interrupt for all interrupt feature (global option)
\param latched (=1 for latched interrupts), (=0 for unlatched interrupts)
*/

int bma150_latch_int(unsigned char latched)
{
	int comres;
	unsigned char data;

	comres = i2c_bma150_read_buf(LATCH_INT__REG, &data, 1);
	comres |= i2c_bma150_write_buf(LATCH_INT__REG, &data, 1);
	return comres;
}

int bma150_set_new_data_int(unsigned char onoff)
{
	/*printk("bma150_set_new_data_int onoff:%d \n",onoff); */
	int comres;
	unsigned char data;
	comres = i2c_bma150_read_buf(NEW_DATA_INT__REG, &data, 1);
	data = BMA150_SET_BITSLICE(data, NEW_DATA_INT, onoff);
	comres |= i2c_bma150_write_buf(NEW_DATA_INT__REG, &data, 1);
	return comres;
}

/** read function for raw register access
\param addr register address
\param *data pointer to data array for register read back
\param len number of bytes to be read starting from addr
*/
int bma150_read_reg(unsigned char addr, unsigned char *data, unsigned char len)
{
	int comres;

	comres = i2c_bma150_read_buf(addr, data, len);
	return comres;
}

/** write function for raw register access
\param addr register address
\param *data pointer to data array for register write
\param len number of bytes to be written starting from addr
*/
int bma150_write_reg(unsigned char addr, unsigned char *data, unsigned char len)
{
	int comres;
	comres = i2c_bma150_write_buf(addr, data, len);
	return comres;
}

/* Maximum value our axis may get for the input device (signed 10 bits) */
#define ACC_MAX_VAL 512

static int bma150_remove_fs(void);
static int bma150_add_fs(struct device *device);

/**
* bma150_get_axis - For the given axis, give the value converted
* @axis:      1,2,3 - can also be negative
* @hw_values: raw values returned by the hardware
*
* Returns the converted value.
*/
static inline int bma150_get_axis(s8 axis, int hw_values[3])
{
	if (axis > 0)
		return hw_values[axis - 1];
	else
		return -hw_values[-axis - 1];
}

/**
* bma150_get_xyz_axis_adjusted - Get X, Y and Z axis values from the accelerometer
* @handle: the handle to the device
* @x:      where to store the X axis value
* @y:      where to store the Y axis value
* @z:      where to store the Z axis value
*
* Note that 40Hz input device can eat up about 10% CPU at 800MHZ
*/
static int bma150_get_xyz_axis_adjusted(int *x, int *y, int *z)
{				/* get the data from register and put them in x, y, z */
	int position[3];
	int ret;

	bma150acc_t acc;
	ret = bma150_read_accel_xyz(&acc);	/*Here we get the data from BMA150 register */
	if (ret)
		return -EIO;
	position[0] = acc.x;	/*w data x */
	position[1] = acc.y;
	position[2] = acc.z;

	*x = bma150_get_axis(bma_dev.ac.x, position);	/*transfer the hw data to user space data */
	*y = bma150_get_axis(bma_dev.ac.y, position);
	*z = bma150_get_axis(bma_dev.ac.z, position);
	return 0;
}

static void bma150_timer_func(unsigned long _data)
{
#if DEBUG
	printk(KERN_INFO "%s \n", __func__);
#endif
	schedule_work(&bma150_work);
	mod_timer(&bma_dev.timer, jiffies + msecs_to_jiffies(bma_dev.sample_interval));	/*reset the timer's interval, that's why timer knows the interval */
}

static int bma150_poweroff(void)
{
#if DEBUG
	printk(KERN_INFO "%s \n", __func__);
#endif
	del_timer_sync(&bma_dev.timer);
	bma150_set_mode(BMA150_MODE_SLEEP);	/*set to sleep mode instead of power off */
	return 0;
}

static int bma150_poweron(void)
{
	unsigned char data1, data2;
	int ret = 0;
#if DEBUG
	printk(KERN_INFO "%s \n", __func__);
#endif
	setup_timer(&bma_dev.timer, bma150_timer_func,
		    (unsigned long)(&(bma_dev)));

	ret = bma150_set_mode(BMA150_MODE_NORMAL);	/*set to normal mode */
	if (ret < 0)
		return ret;
	msleep(2);		/*delay for 1ms */
	ret = bma150_soft_reset();
	if (ret < 0)
		return ret;
	msleep(5);		/*delay for >=1.3ms; */
	ret = bma150_set_range(bma_dev.range);
	if (ret < 0)
		return ret;
	ret = bma150_set_bandwidth(bma_dev.bandwidth);
	if (ret < 0)
		return ret;
	/*bma150_set_shadow_dis(BMA150_SHADOW_DIS_ON);  free read sequence */

	/*bma150_selftest(0); */
	/*test customer reserved reg at addr CUSTOMER1_REG, CUSTOMER2_REG */
	data1 = 0x55;
	data2 = 0xAA;
	ret = i2c_bma150_write_buf(CUSTOMER_RESERVED1__REG, &data1, 1);
	if (ret < 0)
		return ret;
	ret = i2c_bma150_write_buf(CUSTOMER_RESERVED2__REG, &data2, 1);
	if (ret < 0)
		return ret;
	ret = i2c_bma150_read_buf(CUSTOMER_RESERVED1__REG, &data1, 1);
	if (ret < 0)
		return ret;
	ret = i2c_bma150_read_buf(CUSTOMER_RESERVED2__REG, &data2, 1);
	if (ret < 0)
		return ret;
	if (ret)
		return ret;

	if ((0x55 != data1) || (0xAA != data2)) {
		printk(KERN_ERR "bma150 poweron err.\n");
		bma150_set_mode(BMA150_MODE_SLEEP);	/*set to normal mode */
		return -1;
	}

	ret = bma150_set_mode(BMA150_MODE_NORMAL);	/*set to normal mode */
	if (ret < 0) {
		printk(KERN_ERR "bma150_set_mode err.\n");
		return ret;
	}
	msleep(1);		/*delay for 1ms */
	mod_timer(&bma_dev.timer,
		  jiffies + msecs_to_jiffies(bma_dev.sample_interval));
	return ret;
	/*bma150_soft_reset(); */
	/*msleep(5);            delay for >=1.3ms; */
}

/*
* To be called before starting to use the device. It makes sure that the
* device will always be on until a call to bma150_decrease_use(). Not to be
* used from interrupt context.
*/
static void bma150_increase_use(void)
{
#if DEBUG
	printk(KERN_INFO "Before %s, usage = %d, is_on = %d\n", __FUNCTION__,
	       bma_dev.usage, bma_dev.is_on);
#endif
	mutex_lock(&bma_dev.lock);
	bma_dev.usage++;
	if (bma_dev.usage == 1) {
		if (!bma_dev.is_on) {
			if (!bma150_poweron()) {	/*0 for success power on */
				bma_dev.is_on = 1;
			} else {
				bma_dev.is_on = 0;
				bma_dev.usage = 0;
			}
		}
	}
#if DEBUG
	printk(KERN_INFO "After %s, usage = %d, is_on = %d\n", __FUNCTION__,
	       bma_dev.usage, bma_dev.is_on);
#endif
	mutex_unlock(&bma_dev.lock);
}

/*
* To be called whenever a usage of the device is stopped.
* It will make sure to turn off the device when there is not usage.
*/
static void bma150_decrease_use(void)
{
#if DEBUG
	printk(KERN_INFO "Before %s, usage = %d, is_on = %d\n", __FUNCTION__,
	       bma_dev.usage, bma_dev.is_on);
#endif
	mutex_lock(&bma_dev.lock);
	if (bma_dev.usage == 0) {
		mutex_unlock(&bma_dev.lock);
		return;
	}
	bma_dev.usage--;
	if (bma_dev.usage == 0) {
		bma150_poweroff();
		bma_dev.is_on = 0;
	}
#if DEBUG
	printk(KERN_INFO "After %s, usage = %d, is_on = %d\n", __FUNCTION__,
	       bma_dev.usage, bma_dev.is_on);
#endif
	mutex_unlock(&bma_dev.lock);
}

/*
static inline void bma150_calibrate_joystick(void)
{
	bma150_get_xyz_axis_adjusted(&bma_dev.xcalib, &bma_dev.ycalib, &bma_dev.zcalib);
}
*/

static int bma150_joystick_enable(void)
{
	int err;

	if (bma_dev.idev)
		return -EINVAL;

	bma_dev.idev = input_allocate_device();
	if (!bma_dev.idev)
		return -ENOMEM;

	bma_dev.idev->name = "BOSCH BMA150 Accelerometer";
	bma_dev.idev->name = "bma150_input";
	bma_dev.idev->phys = "bma150/input0";
	bma_dev.idev->id.bustype = BUS_I2C;
	bma_dev.idev->id.vendor = 0;
	bma_dev.idev->dev.parent = bma_dev.device;
	/*
	   bma_dev.idev->open       = bma150_joystick_open;
	   bma_dev.idev->close      = bma150_joystick_close;
	 */
#ifdef REL_MODE
	__set_bit(EV_REL, bma_dev.idev->evbit);
	__set_bit(REL_X, bma_dev.idev->relbit);
	__set_bit(REL_Y, bma_dev.idev->relbit);
	__set_bit(REL_Z, bma_dev.idev->relbit);

	__set_bit(EV_SYN, bma_dev.idev->evbit);
	__set_bit(EV_KEY, bma_dev.idev->evbit);
#else
	__set_bit(EV_ABS, bma_dev.idev->evbit);
	__set_bit(ABS_X, bma_dev.idev->absbit);
	__set_bit(ABS_Y, bma_dev.idev->absbit);
	__set_bit(ABS_Z, bma_dev.idev->absbit);
	__set_bit(ABS_MISC, bma_dev.idev->absbit);

	__set_bit(EV_SYN, bma_dev.idev->evbit);
	__set_bit(EV_KEY, bma_dev.idev->evbit);

#endif
	input_set_abs_params(bma_dev.idev, ABS_X,
			     -ACC_MAX_VAL, ACC_MAX_VAL, 0, 0);
	input_set_abs_params(bma_dev.idev, ABS_Y,
			     -ACC_MAX_VAL, ACC_MAX_VAL, 0, 0);
	input_set_abs_params(bma_dev.idev, ABS_Z,
			     -ACC_MAX_VAL, ACC_MAX_VAL, 0, 0);
	input_set_abs_params(bma_dev.idev, ABS_MISC, -100, 100, 0, 0);

	err = input_register_device(bma_dev.idev);
	if (err) {
		printk(KERN_ERR "regist input driver error\n");
		input_free_device(bma_dev.idev);
		bma_dev.idev = NULL;
	}

	return err;
}

static void bma150_joystick_disable(void)
{
	if (!bma_dev.idev)
		return;

	input_unregister_device(bma_dev.idev);
	bma_dev.idev = NULL;
}

/*
* Initialise the accelerometer and the various subsystems.
* Should be rather independant of the bus system.
*/
static int bma150_init_device(struct i2c_bma150 *dev)
{
#if DEBUG
	printk(KERN_INFO "%s \n", __func__);
#endif
	if (bma150_joystick_enable())
		printk(KERN_ERR "bma150: joystick initialization failed\n");
	bma150_add_fs(&bma_dev.idev->dev);

	return 0;
}

static int bma150_dmi_matched(const struct dmi_system_id *dmi)
{
	bma_dev.ac = *((struct axis_conversion *)dmi->driver_data);
#if DEBUG
	printk(KERN_INFO "bma150: hardware type %s found.\n", dmi->ident);
#endif
	return 1;
}

/* Represents, for each axis seen by userspace, the corresponding hw axis (+1).
* If the value is negative, the opposite of the hw value is used. */
//static struct axis_conversion bma150_axis_normal = {1, 2, 3};
static struct axis_conversion bma150_axis_y_inverted = { 1, -2, 3 };
static struct axis_conversion bma150_axis_x_inverted = { -1, 2, 3 };
static struct axis_conversion bma150_axis_z_inverted = { 1, 2, -3 };
static struct axis_conversion bma150_axis_xy_rotated_left = { -2, 1, 3 };
static struct axis_conversion bma150_axis_xy_swap_inverted = { -2, -1, 3 };

#define AXIS_DMI_MATCH(_ident, _name, _axis) {		\
	.ident = _ident,				\
	.callback = bma150_dmi_matched,		\
	.matches = {					\
		DMI_MATCH(DMI_PRODUCT_NAME, _name)	\
	},						\
	.driver_data = &bma150_axis_##_axis		\
}
static struct dmi_system_id bma150_dmi_ids[] = {
	/* product names are truncated to match all kinds of a same model */
	AXIS_DMI_MATCH("NC64x0", "HP Compaq nc64", x_inverted),
	AXIS_DMI_MATCH("NC84x0", "HP Compaq nc84", z_inverted),
	AXIS_DMI_MATCH("NX9420", "HP Compaq nx9420", x_inverted),
	AXIS_DMI_MATCH("NW9440", "HP Compaq nw9440", x_inverted),
	AXIS_DMI_MATCH("NC2510", "HP Compaq 2510", y_inverted),
	AXIS_DMI_MATCH("NC8510", "HP Compaq 8510", xy_swap_inverted),
	AXIS_DMI_MATCH("HP2133", "HP 2133", xy_rotated_left),
	{NULL,}
	/* Laptop models without axis info (yet):
	 * "NC651xx" "HP Compaq 651"
	 * "NC671xx" "HP Compaq 671"
	 * "NC6910" "HP Compaq 6910"
	 * HP Compaq 8710x Notebook PC / Mobile Workstation
	 * "NC2400" "HP Compaq nc2400"
	 * "NX74x0" "HP Compaq nx74"
	 * "NX6325" "HP Compaq nx6325"
	 * "NC4400" "HP Compaq nc4400"
	 */
};

static int bma150_add(struct device *device)
{
	if (!device)
		return -EINVAL;
	bma_dev.device = device;

	//##!!## axis direction should be checked
	/* If possible use a "standard" axes order */
	if (dmi_check_system(bma150_dmi_ids) == 0) {
		printk(KERN_INFO "bma150" ": laptop model unknown, "
		       "using default axes configuration\n");
		bma_dev.ac = bma150_axis_x_inverted;
	}
	return bma150_init_device(&bma_dev);
}

/*********************** Sysfs stuff ***********************/
static ssize_t bma150_acceleration_show(struct device *dev,
					struct device_attribute *attr,
					char *buf)
{
	int x, y, z;
	if (bma_dev.is_on != 0) {
		bma150_get_xyz_axis_adjusted(&x, &y, &z);
	} else {
		x = 0;
		y = 0;
		z = 0;
	}

	if (x < ACC_MAX_VAL && x > -ACC_MAX_VAL &&
	    y < ACC_MAX_VAL && y > -ACC_MAX_VAL &&
	    z < ACC_MAX_VAL && z > -ACC_MAX_VAL) {
		return sprintf(buf, "(%d,%d,%d)\n", x, y, z);
	} else
		return 0;
}

static ssize_t bma150_calibrate_show(struct device *dev,
				     struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "(%d,%d,%d)\n", bma_dev.xcalib, bma_dev.ycalib,
		       bma_dev.zcalib);
}

static ssize_t bma150_calibrate_store(struct device *dev,
				      struct device_attribute *attr,
				      const char *buf, size_t count)
{
	if (bma_dev.is_on != 0) {
		//bma150_calibrate_joystick();
		int calibrate_data[3];
		const char *current_position = buf;
		char *tail_position = NULL;
		int i;

		if (count > 50)
			count = 50;

		for (i = 0; i < 3; i++) {
			while ((*current_position > '9'
				|| *current_position < '0')
			       && *current_position != '-')
				current_position++;

			calibrate_data[i] =
			    simple_strtol(current_position, &tail_position, 10);

			if (calibrate_data[i] > 256)
				calibrate_data[i] = 256;
			if (calibrate_data[i] < -256)
				calibrate_data[i] = -256;
			current_position = tail_position;
		}
		bma_dev.xcalib = calibrate_data[0];
		bma_dev.ycalib = calibrate_data[1];
		bma_dev.zcalib = calibrate_data[2];
	}
	return count;
}

static ssize_t bma150_status_show(struct device *dev,
				  struct device_attribute *attr, char *buf)
{
	if (bma_dev.is_on == 0)
		return sprintf(buf, "0\n");
	else
		return sprintf(buf, "1\n");
}

static ssize_t bma150_status_set(struct device *dev,
				 struct device_attribute *attr,
				 const char *buf, size_t count)
{
	if (strcmp(buf, "1\n") == 0) {
		bma150_increase_use();
	} else if (strcmp(buf, "0\n") == 0) {
		bma150_decrease_use();
	} else
		printk(KERN_INFO "unrecognized, using on/off\n");
	return count;
}

static ssize_t bma150_interval_set(struct device *dev,	/*DO NOTING to BMA150 phisically, nothing written to registers, just modify bma_dev.sanple_interval */
				   struct device_attribute *attr,
				   const char *buf, size_t count)
{
	unsigned int val = 0;
	char msg[256] = "\0";
	if (count > 256)
		count = 256;
	memcpy(msg, buf, count - 1);	/*skip last "\n" */
	msg[count - 1] = '\0';
	val = (unsigned int)simple_strtoul(msg, NULL, 10);
	if (val < 5)		/*TODO:avoid i2c too busy */
		val = 5;
	bma_dev.sample_interval = val;	/*double increase sample rate */
	/*reset the timer's interval, that's why timer knows the interval */
	mod_timer(&bma_dev.timer,
		  jiffies + msecs_to_jiffies(bma_dev.sample_interval));
	pr_debug("bma150 sample interval %d ms\n", bma_dev.sample_interval);
	return count;
}

static ssize_t bma150_interval_show(struct device *dev,
				    struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", bma_dev.sample_interval);
}

static ssize_t bma150_range_set(struct device *dev,
				struct device_attribute *attr,
				const char *buf, size_t count)
{
	unsigned int val = 0;
	char msg[10] = "\0";
	if (count > 10)
		count = 10;
	memcpy(msg, buf, count - 1);	/*skip last "\n" */
	msg[count - 1] = '\0';
	val = (unsigned int)simple_strtoul(msg, NULL, 10);
	if (val >= 2)
		bma_dev.range = BMA150_RANGE_8G;
	else
		bma_dev.range = val;	/*0 for BMA150_RANGE_2G, 1 for BMA150_RANGE_4G */
	if (bma_dev.is_on)	/*activate immediately? */
		bma150_set_range(bma_dev.range);

	printk(KERN_INFO "bma150 range %d\n", bma_dev.range);
	return count;
}

static ssize_t bma150_range_show(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", bma_dev.range);
}

static ssize_t bma150_bandwidth_set(struct device *dev,
				    struct device_attribute *attr,
				    const char *buf, size_t count)
{
	unsigned int val = 0;
	char msg[10] = "\0";
	if (count > 10)
		count = 10;
	memcpy(msg, buf, count - 1);	/*skip last "\n" */
	msg[count - 1] = '\0';
	val = (unsigned int)simple_strtoul(msg, NULL, 10);
	printk("val=%d\n", val);
	if (val >= 6)
		bma_dev.bandwidth = BMA150_BW_1500HZ;
	else
		bma_dev.bandwidth = val;
	if (bma_dev.is_on)	/*activate immediately? */
		bma150_set_bandwidth(bma_dev.bandwidth);

	printk(KERN_INFO "bma150 bandwidth %d\n", bma_dev.bandwidth);
	return count;
}

static ssize_t bma150_bandwidth_show(struct device *dev,
				     struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", bma_dev.bandwidth);
}

static ssize_t bma150_wake_set(struct device *dev,
			       struct device_attribute *attr,
			       const char *buf, size_t count)
{
	static int i = 1;
#if DEBUG
	printk(KERN_INFO "%s \n", __func__);
#endif
	if (i == 10)
		i = 1;
	input_report_abs(bma_dev.idev, ABS_MISC, i++);
	return count;
}
static int status_show(struct device *dev,
		       struct device_attribute *attr, char *buf)
{
	int status;
	status = SENSOR_STATUS_ACCURACY_HIGH;
	return sprintf(buf, "%d\n", status);
}

static DEVICE_ATTR(active, S_IRUGO | S_IWUGO, bma150_status_show,
		   bma150_status_set);
static DEVICE_ATTR(data, S_IRUGO, bma150_acceleration_show, NULL);
static DEVICE_ATTR(calibrate, S_IRUGO | S_IWUGO, bma150_calibrate_show,
		   bma150_calibrate_store);
static DEVICE_ATTR(interval, S_IRUGO | S_IWUGO, bma150_interval_show,
		   bma150_interval_set);
static DEVICE_ATTR(range, S_IRUGO | S_IWUGO, bma150_range_show,
		   bma150_range_set);
static DEVICE_ATTR(bandwidth, S_IRUGO | S_IWUGO, bma150_bandwidth_show,
		   bma150_bandwidth_set);
static DEVICE_ATTR(wake, S_IRUGO | S_IWUGO, NULL, bma150_wake_set);
static DEVICE_ATTR(status, S_IRUGO | S_IWUGO, status_show, NULL);
/*static DEVICE_ATTR(rate, S_IRUGO, bma150_rate_show, NULL);*/

static struct attribute *bma150_attributes[] = {
	&dev_attr_active.attr,
	&dev_attr_data.attr,
	&dev_attr_calibrate.attr,
	&dev_attr_interval.attr,
	&dev_attr_range.attr,
	&dev_attr_bandwidth.attr,
	&dev_attr_wake.attr,
	&dev_attr_status.attr,
	NULL
};

static struct attribute_group bma150_attribute_group = {
	.attrs = bma150_attributes
};

static int bma150_add_fs(struct device *device)
{
	return sysfs_create_group(&device->kobj, &bma150_attribute_group);
}

static int bma150_remove_fs(void)
{
	sysfs_remove_group(&bma_dev.device->kobj, &bma150_attribute_group);
	return 0;
}

#ifdef	CONFIG_PM
static int bma150_suspend(struct i2c_client *client, pm_message_t state)
{
	return 0;
}
static int bma150_resume(struct i2c_client *client)
{
	return 0;
}
#else
#define	bma150_suspend		NULL
#define	bma150_resume		NULL
#endif

#ifdef CONFIG_HAS_EARLYSUSPEND
static void bma150_early_suspend(struct early_suspend *h)
 {
#if DEBUG
	printk(KERN_INFO "Before %s, usage = %d, is_on = %d\n", __FUNCTION__,
	       bma_dev.usage, bma_dev.is_on);
#endif
	mutex_lock(&bma_dev.lock);
	if (bma_dev.usage == 0) {
		mutex_unlock(&bma_dev.lock);
		return;
	} else {
		bma150_poweroff();
		bma_dev.is_on = 0;
	}
	mutex_unlock(&bma_dev.lock);
#if DEBUG
	printk(KERN_INFO "After %s, usage = %d, is_on = %d\n", __FUNCTION__,
	       bma_dev.usage, bma_dev.is_on);
#endif
	return;
}

static void bma150_late_resume(struct early_suspend *h)
{
	int retry = 0;
#if DEBUG
	printk(KERN_INFO "Before %s, usage = %d, is_on = %d\n", __FUNCTION__,
	       bma_dev.usage, bma_dev.is_on);
#endif
	mutex_lock(&bma_dev.lock);
	if (bma_dev.usage > 0) {
		bma_dev.is_on = 1;
		for (retry = 0; retry < 2; retry++) {
			if (bma150_poweron() >= 0)
				break;
		}
	}
	mutex_unlock(&bma_dev.lock);
#if DEBUG
	printk(KERN_INFO "After %s, usage = %d, is_on = %d\n", __FUNCTION__,
	       bma_dev.usage, bma_dev.is_on);
#endif
	return;
}
#endif
static void bma150_report_value(struct work_struct *work)
{
	int x, y, z;
	int ret;
	static int x_last = 0, y_last = 0, z_last = 0;
#if DEBUG
	printk(KERN_INFO "%s", __FUNCTION__);
#endif
	if (bma_dev.is_on == 0)
		return;
	ret = bma150_get_xyz_axis_adjusted(&x, &y, &z);
	if (ret) {		/*retry if this time failed */
#if DEBUG
		printk(KERN_ERR "%s, usage = %d, is_on = %d\n", __FUNCTION__,
		       bma_dev.usage, bma_dev.is_on);
#endif
		return;
	}

	x = (x * 1 + x_last * 3) / 4;
	y = (y * 1 + y_last * 3) / 4;
	z = (z * 1 + z_last * 3) / 4;
	x_last = x;
	y_last = y;
	z_last = z;
#if DEBUG
	printk(KERN_INFO "%s, [X: %d], [Y: %d], [Z: %d]\n", __FUNCTION__, x, y,
	       z);
#endif
#ifdef REL_MODE			/*where we report our data to upper layer input_deice, here is the precise part */
	input_report_rel(bma_dev.idev, REL_X, x - bma_dev.xcalib);	/*the 3rd argumnet is x, for xcalib = 0 */
	input_report_rel(bma_dev.idev, REL_Y, y - bma_dev.ycalib);	/*the 3rd argumnet is y, for ycalib = 0 */
	input_report_rel(bma_dev.idev, REL_Z, z - bma_dev.zcalib);	/*the 3rd argumnet is z, for zcalib = 0 */
#else
	input_report_abs(bma_dev.idev, ABS_X, x - bma_dev.xcalib);	/*the 3rd argumnet is x, for xcalib = 0 */
	input_report_abs(bma_dev.idev, ABS_Y, y - bma_dev.ycalib);	/*the 3rd argumnet is y, for ycalib = 0 */
	input_report_abs(bma_dev.idev, ABS_Z, z - bma_dev.zcalib);	/*the 3rd argumnet is z, for zcalib = 0 */
#endif
	input_sync(bma_dev.idev);	/*where the EV_SYN happen */

}

static int __devinit bma150_probe(struct i2c_client *client,
				  const struct i2c_device_id *id)
{
	int retry;
	int res;

	/*initialize global ptr */
	g_client = client;

	bma_dev.usage = 0;
	bma_dev.is_on = 0;
	bma_dev.range = BMA150_RANGE_2G;
	bma_dev.bandwidth = BMA150_BW_25HZ;
	bma_dev.sample_interval = 200;	/*ms */
	mutex_init(&bma_dev.lock);	/*be called before bma150_increase_use() */

	/*make sure it's a bma150 sensor */
	for (retry = 0; retry < 2; retry++) {
		if (bma150_poweron() < 0)
			continue;
		if (bma150_detect() < 0)
			continue;
		if (bma150_poweroff() < 0)
			continue;
		if (bma_dev.chip_id == 0x02)
			break;
	}
	if (bma_dev.chip_id != 0x02) {
		printk(KERN_ERR "bma150 chip id error\n");
		del_timer_sync(&bma_dev.timer);
		return -1;
	}

	printk(KERN_INFO
	       "bma150:attatch to i2c bus, addr=0x%x, chip id=0x%x \n",
	       bma_dev.dev_addr, bma_dev.chip_id);
	INIT_WORK(&bma150_work, bma150_report_value);
	bma150_add(&client->dev);

	/* register misc device for bma150 */
	res = misc_register(&bma150_misc_device);
	if (res) {
		pr_err("%s: bma150 misc device register failed\n", __func__);
		bma150_misc_on = 0;
	} else {
		bma150_misc_on = 1;
	}
#ifdef CONFIG_HAS_EARLYSUSPEND
	bma_dev.bma150_early_suspend.level = EARLY_SUSPEND_LEVEL_STOP_DRAWING;
	bma_dev.bma150_early_suspend.suspend = bma150_early_suspend;
	bma_dev.bma150_early_suspend.resume = bma150_late_resume;
	register_early_suspend(&bma_dev.bma150_early_suspend);
#endif
	return 0;
}

static int bma150_remove(struct i2c_client *client)
{
	if (bma150_misc_on)
		misc_deregister(&bma150_misc_device);
	bma150_joystick_disable();
	bma150_remove_fs();
	bma150_poweroff();
	bma_dev.usage = 0;
	bma_dev.is_on = 0;

	return 0;
}

static const struct i2c_device_id bma150_id[] = {
	{"bma150", 0},
	{}
};

static struct i2c_driver bma150_driver = {
	.driver = {
		   .name = "bma150",
		   },
	.id_table = bma150_id,
	.probe = bma150_probe,
	.remove = bma150_remove,
	.suspend = bma150_suspend,
	.resume = bma150_resume,
};

static int __init bma150_i2c_init(void)
{
	return i2c_add_driver(&bma150_driver);
}

static void __exit bma150_i2c_exit(void)
{
	i2c_del_driver(&bma150_driver);
}

/*====================================================================*/

MODULE_DESCRIPTION
    ("BOSCH BMA150 three-axis digital accelerometer (I2C) driver");
MODULE_AUTHOR("Yifan Zhang <zhangyf@marvell.com>");
MODULE_LICENSE("GPL");

module_init(bma150_i2c_init);
module_exit(bma150_i2c_exit);

/*====================================================================*/
