/*
*  cywee.c - Cywee 9-axis motion sensor driver
*
*  Copyright (C) Yifan Zhang <zhangyf@marvell.com>
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/sysfs.h>
#include <linux/timer.h>
#include <linux/input.h>
#include <linux/delay.h>
#include <linux/miscdevice.h>
#include <linux/uaccess.h>
#include <linux/gpio.h>
#include <linux/earlysuspend.h>
#include <mach/mfp-mmp2.h>
#include <mach/axis_sensor.h>
#include "cywee.h"
int cy_gp;
static int ami602_init(void);
static struct i2c_client *g_client;
static const struct i2c_device_id cywee_id[] = {
	{"cywee", 0},
	{}
};

#ifdef CONFIG_HAS_EARLYSUSPEND
static struct early_suspend axis_sensor_early_suspend;
#endif

static int ami602_open(struct inode *inode, struct file *file)
{
		return nonseekable_open(inode, file);
}

static int ami602_release(struct inode *inode, struct file *file)
{
		return 0;
}

static int ami602_sendtrigger(void)
{
	gpio_direction_output(cy_gp, 0);
	udelay(200);
	gpio_direction_output(cy_gp, 1);
	udelay(100);
	return 0;
}
static int ami602_read_chip_info(char *buf, int bufsize)
{
	u8 databuf[10];
	char cmd;
	int res = 0;

	cmd = AMI602_CMD_GET_FIRMWARE;
	memset(databuf, 0, sizeof(u8)*10);

	if ((!buf)||(bufsize<=30))
		return -1;
	if (!g_client)
	{
		*buf = 0;
		return -2;
	}
	ami602_sendtrigger();
	res = i2c_master_send(g_client, &cmd, 0x1);
	if (res<=0)
		goto exit_AMI602_ReadChipInfo;
	udelay(200);
	res = i2c_master_recv(g_client, databuf, 8);
	if (res<=0)
		goto exit_AMI602_ReadChipInfo;

exit_AMI602_ReadChipInfo:
	if (res<=0) {
		sprintf(buf, "I2C error: ret value=%d", res);
		return -3;
	}
	else if (databuf[0] == 0) {
		sprintf(buf, "AMI602 Firmware Version:%x.%x, Date:%02x%02x/%02x/%02x"
				, databuf[1], databuf[2], databuf[3], databuf[4], databuf[5], databuf[6]);
	}
	else {
		sprintf(buf, "Failed to execute I2C command");
		return -4;
	}
	return 0;
}
static int ami602_read_sensor_data(char *buf, int bufsize)
{
	char cmd;
	u8 databuf[20];
	int ch1,ch2,ch3,ch4,ch5,ch6;
	int res = 0;
	memset(databuf, 0, sizeof(u8)*10);

	if ((!buf)||(bufsize<=80))
		return -1;
	if (!g_client)
	{
		*buf = 0;
		return -2;
	}

	ami602_sendtrigger();
	cmd = AMI602_CMD_REQ_MES;

	res = i2c_master_send(g_client, &cmd, 0x1);
	if (res<=0)
		goto exit_AMI602_ReadSensorData;
	udelay(350);

	res = i2c_master_recv(g_client, databuf, 1);
	if (res<=0)
		goto exit_AMI602_ReadSensorData;

	msleep(4);
	ami602_sendtrigger();
	cmd = 0x28;
	res = i2c_master_send(g_client, &cmd, 0x1);
	if (res<=0)
		goto exit_AMI602_ReadSensorData;
	udelay(350);
	res = i2c_master_recv(g_client, databuf, 10);
	if (res<=0)
		goto exit_AMI602_ReadSensorData;

	ch1 = ((int) databuf[1]) << 4 | ((int) databuf[2]) >> 4;
	ch2 = ((int) databuf[2] & 0x0f)   << 8  | ((int) databuf[3]);
	ch3 = ((int) databuf[4]) << 4 | ((int) databuf[5]) >> 4;
	ch4 = ((int) databuf[5] & 0x0f)   << 8  | ((int) databuf[6]);
	ch5 = ((int) databuf[7]) << 4 | ((int) databuf[8]) >> 4;
	ch6 = ((int) databuf[8] & 0x0f)   << 8  | ((int) databuf[9]);

exit_AMI602_ReadSensorData:
	if (res<=0) {
		sprintf(buf, "I2C error: ret value=%d", res);
		return -3;
	}
	else if (databuf[0] == 0) {
		memcpy(buf, databuf+1, 9);
		return 9;
	}
	else {
		printk("Failed to execute I2C command");
		return -4;
	}
	return 0;
}
static int ami602_ioctl(struct inode *inode, struct file *file, unsigned int cmd,
		unsigned long arg)
{
	char strbuf[AMI602_BUFSIZE];
	void __user *data;
	int retval=0;
	int res;

	switch (cmd) {
		case AMI602_IOCTL_INIT:
			ami602_init();
			break;

		case AMI602_IOCTL_READ_CHIPINFO:
			data = (void __user *) arg;
			if (data == NULL)
				break;
			ami602_read_chip_info(strbuf, AMI602_BUFSIZE);
			if (copy_to_user(data, strbuf, strlen(strbuf)+1)) {
				retval = -EFAULT;
				goto err_out;
			}
			break;

		case AMI602_IOCTL_READ_SENSORDATA:
			data = (void __user *) arg;
			if (data == NULL)
				break;
			res = ami602_read_sensor_data(strbuf, AMI602_BUFSIZE);
			if (res <= 0) {
				retval = -EFAULT;
				goto err_out;
			} else {
				if (copy_to_user(data, strbuf, res)) {
					retval = -EFAULT;
					goto err_out;
				}
			}
			break;

		default:
			printk(KERN_ERR "%s not supported = 0x%04x", __FUNCTION__, cmd);
			retval = -ENOIOCTLCMD;
			break;
	}

err_out:
	return retval;
}
static struct file_operations ami602_fops = {
	.owner = THIS_MODULE,
	.open = ami602_open,
	.release = ami602_release,
	.ioctl = ami602_ioctl,
};
static struct miscdevice ami602_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "ami602",
	.fops = &ami602_fops,
};
static int cywee_register_device(struct i2c_client *client)
{
	int err = misc_register(&ami602_device);
	if (err) {
		printk(KERN_ERR
				"ami602_device register failed\n");
		goto exit_misc_device_register_failed;
	}
	return 0;

exit_misc_device_register_failed:
	return err;
}
static ssize_t show_chipinfo_value(struct device *dev, struct device_attribute *attr, char *buf)
{
	char strbuf[AMI602_BUFSIZE];
	ami602_read_chip_info(strbuf, AMI602_BUFSIZE);
	return sprintf(buf, "%s\n", strbuf);
}

static ssize_t show_sensordata_value(struct device *dev, struct device_attribute *attr, char *buf)
{
	char strbuf[AMI602_BUFSIZE];
	ami602_read_sensor_data(strbuf, AMI602_BUFSIZE);
	memcpy(buf, strbuf, 9);
	return 9;
}

static DEVICE_ATTR(chipinfo, S_IRUGO, show_chipinfo_value, NULL);
static DEVICE_ATTR(sensordata, S_IRUGO, show_sensordata_value, NULL);

static struct attribute *cywee_attributes[] = {
		&dev_attr_chipinfo.attr,
		&dev_attr_sensordata.attr,
		NULL
};

static struct attribute_group cywee_attribute_group = {
	.attrs = cywee_attributes
};

static int cywee_add_fs(struct device *device)
{
	return sysfs_create_group(&device->kobj, &cywee_attribute_group);
}

static int ami602_init(void)
{
	u8 databuf[10];
	int res = 0;
	memset(databuf, 0, sizeof(u8)*10);
	/* Enable AEN*/
	databuf[0] = AMI602_CMD_SET_AEN;/*AMI602_CMD_SET_AEN*/
	databuf[1] = 0x01;
	ami602_sendtrigger();
	res = i2c_master_send(g_client, databuf, 0x02);
	if (res<=0) {
		printk("i2c_master_send fails !!! \n");
		goto exit_AMI602_Init;
	}
	udelay(350);
	res = i2c_master_recv(g_client, databuf, 0x01);
	if (res<=0)
		goto exit_AMI602_Init;

exit_AMI602_Init:
	if (res<=0) {
		return -1;
	}
	else if (databuf[0] != 0) {
		return -2;
	}
	return 0;
}

#ifdef CONFIG_HAS_EARLYSUSPEND
static void cywee_early_suspend(struct early_suspend *h)
{
	if (((struct axis_sensor_platform_data *)(g_client->dev.platform_data))->set_power)
		((struct axis_sensor_platform_data *)(g_client->dev.platform_data))->set_power(0);
	return;
}
static void cywee_late_resume(struct early_suspend *h)
{
	if (((struct axis_sensor_platform_data *)(g_client->dev.platform_data))->set_power)
		((struct axis_sensor_platform_data *)(g_client->dev.platform_data))->set_power(1);
	msleep(70);
	if (ami602_init() < 0)
		printk("ami602_init fails \n");
	return;
}
#endif

static int cywee_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int ret;

	if (((struct axis_sensor_platform_data *)(client->dev.platform_data))->set_power)
		((struct axis_sensor_platform_data *)(client->dev.platform_data))->set_power(1);

	ret = cywee_register_device(client);
	if (ret)
		return ret;
	g_client = client;
	cy_gp = mfp_to_gpio(GPIO93_GPIO93);
	if (gpio_request(cy_gp, "CYWEE SET")) {
		printk(KERN_INFO "gpio %d request failed \n", cy_gp);
		return -1;
	}
	if (ami602_init() < 0) {
		printk("ami602_init fails \n");
		return -1;
	}
	cywee_add_fs(&client->dev);
#ifdef CONFIG_HAS_EARLYSUSPEND
	axis_sensor_early_suspend.level = EARLY_SUSPEND_LEVEL_DISABLE_FB + 500;	/* workaround for cm3623 i2c error */
	axis_sensor_early_suspend.suspend = cywee_early_suspend;
	axis_sensor_early_suspend.resume = cywee_late_resume;
	register_early_suspend(&axis_sensor_early_suspend);
#endif
	return 0;
}

static struct i2c_driver cywee_driver = {
	.driver = {
		.name	= "cywee_sensor_driver",
	},

	.id_table	= cywee_id,
	.probe		= cywee_probe,
};

static int __init cywee_init(void)
{
	return i2c_add_driver(&cywee_driver);
}

static void  __exit cywee_exit(void)
{
	i2c_del_driver(&cywee_driver);
}
module_init(cywee_init);
module_exit(cywee_exit);

MODULE_DESCRIPTION("cywee 9-axises driver");
MODULE_AUTHOR("yifan zhang<zhangyf@marvell.com>");
MODULE_LICENSE("GPL");
