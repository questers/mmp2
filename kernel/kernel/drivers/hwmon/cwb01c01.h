/* cwb01c01.h - header file for Cywee B01C01 2-axis gyroscope 
 *
 * Copyright (C) 2010 CyWee Group Ltd.
 * Author: Joe Wei <joewei@cywee.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#ifndef __CWB01C01_H__
#define __CWB01C01_H__

#define DEV_NAME					"cwb01c01"
#define DPS_MAX						2000
#define DEFAULT_SAMPLE_INTERVAL		200
#define MIN_INTERVAL_MS				50
#define ZERO_OFFSET					2048

#define COMMAND_CODE		0x01
#define DATA_CODE			0x03
#define VERSION_CODE		0x04

/* Commands */
#define CMD_SEND_RAW_DATA	0xF4
#define CMD_VERSION			0xF5


#endif /* __CWB01C01_H__ */
