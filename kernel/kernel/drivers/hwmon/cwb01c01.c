/* cwb01c01.c - Cywee B01C01 2-axis gyroscope
 *
 * Copyright (C) 2010 CyWee Group Ltd.
 * Author: Joe Wei <joewei@cywee.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/sysfs.h>
#include <linux/string.h>
#include <linux/input.h>
#include <linux/timer.h>
#include <linux/mutex.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/earlysuspend.h>
#include <mach/axis_sensor.h>
#include "cwb01c01.h"

struct i2c_gyro_sensor {
	struct i2c_client *client;
	struct input_dev *input;
	struct delayed_work input_work;
	unsigned int sample_interval;
	atomic_t enabled;

	struct mutex i2c_lock;
	struct mutex set_lock;

#ifdef CONFIG_HAS_EARLYSUSPEND
	struct early_suspend early_suspend;
#endif

	/* Data */
	int angular_velocity[3];
	int temperature;
};


/**
 * Returns the checksum of the given data
 *
 * The checksum is calculated by summing up all the data in the buffer
 * and ignores any overflow
 *
 * @buffer data used to calculate the checksum
 * @size the size of buffer
 * @return the checksum
 */
static int cwb01c01_checksum(const char *buffer, size_t size)
{
	unsigned char checksum = 0;
	unsigned char i = 0;

	for (i = 0; i < size; i++)
		checksum += buffer[i];

	return checksum;
}


static int cwb01c01_send_command(struct i2c_gyro_sensor *gyro, unsigned char cmd)
{
    unsigned char buf[4];

    buf[0] = sizeof(buf);
    buf[1] = COMMAND_CODE;
    buf[2] = cmd;
    buf[3] = cwb01c01_checksum(buf, 3);

    return i2c_master_send(gyro->client, buf, buf[0]);
}


static int cwb01c01_read_raw(struct i2c_gyro_sensor *gyro)
{
	int err = 0;
	char buf[13];

	mutex_lock(&gyro->i2c_lock);
	if ((err = cwb01c01_send_command(gyro, CMD_SEND_RAW_DATA)) < 0) {
		dev_err(&gyro->client->dev, "Failed to send command\n");
		goto free_lock;
	}

	mdelay(1);

	if ((err = i2c_master_recv(gyro->client, (char *)buf, sizeof(buf))) < 0) {
		dev_err(&gyro->client->dev, "Failed to read raw data\n");
		goto free_lock;
	}

	/* check the correctness of the received data */
	if (buf[0] != sizeof(buf) || buf[1] != DATA_CODE) {
		dev_err(&gyro->client->dev, "Something wrong in the received data: buf[0] = 0x%02X, buf[1] = 0x%02X", buf[0], buf[1]);
		goto free_lock;
	}

	gyro->temperature = buf[2] << 8 | buf[3];
	gyro->angular_velocity[0] = (buf[4] << 8 | buf[5]) - ZERO_OFFSET;
	gyro->angular_velocity[1] = 0;
	gyro->angular_velocity[2] = (buf[6] << 8 | buf[7]) - ZERO_OFFSET;
	pr_debug("wx=%05d, wy=%05d, wz=%05d\n",
			gyro->angular_velocity[0],
			gyro->angular_velocity[1],
			gyro->angular_velocity[2]
	);

	err = 0;

free_lock:
	mutex_unlock(&gyro->i2c_lock);
	return err;
}


static int cwb01c01_power_on(struct i2c_gyro_sensor *gyro)
{
	return 0;
}


static int cwb01c01_power_off(struct i2c_gyro_sensor *gyro)
{
	return 0;
}


static int cwb01c01_enable(struct i2c_gyro_sensor *gyro, int enabled)
{
	mutex_lock(&gyro->set_lock);
	if (enabled) {
		if (cwb01c01_power_on(gyro) < 0) {
			dev_err(&gyro->client->dev, "Failed to power on %s\n", DEV_NAME);
			atomic_set(&gyro->enabled, 0);
			mutex_unlock(&gyro->set_lock);
			return -1;
		}

		schedule_delayed_work(&gyro->input_work, msecs_to_jiffies(
					gyro->sample_interval));
		pr_debug("cwb01c01 enabled\n");

	} else {
		cancel_delayed_work_sync(&gyro->input_work);
		cwb01c01_power_off(gyro);
		pr_debug("cwb01c01 disabled\n");
	}
	mutex_unlock(&gyro->set_lock);
	return 0;
}

static int active_set(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev->parent);
	struct i2c_gyro_sensor *gyro = (struct i2c_gyro_sensor *)i2c_get_clientdata(client);

	int enabled = (strcmp(buf,"1\n") == 0) ? 1 : 0;
	if (enabled) {
		if (!atomic_cmpxchg(&gyro->enabled, 0, 1)) {
			if (((struct axis_sensor_platform_data *)(gyro->client->dev.platform_data))->set_power) {
				((struct axis_sensor_platform_data *)(gyro->client->dev.platform_data))->set_power(1);
			}
			cwb01c01_enable(gyro, enabled);
		}
	} else {
		if (atomic_cmpxchg(&gyro->enabled, 1, 0)) {
			cwb01c01_enable(gyro, enabled);
			if (((struct axis_sensor_platform_data *)(gyro->client->dev.platform_data))->set_power) {
				((struct axis_sensor_platform_data *)(gyro->client->dev.platform_data))->set_power(0);
			}
		}
	}
	return count;
}

static int active_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
    struct i2c_client *client = to_i2c_client(dev->parent);
    struct i2c_gyro_sensor *gyro = (struct i2c_gyro_sensor *)i2c_get_clientdata(client);

	if (atomic_read(&gyro->enabled))
		return sprintf(buf, "1\n");
	else
		return sprintf(buf, "0\n");
}


static int interval_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
    struct i2c_client *client = to_i2c_client(dev->parent);
    struct i2c_gyro_sensor *gyro = (struct i2c_gyro_sensor *)i2c_get_clientdata(client);

    return sprintf(buf, "%d\n", gyro->sample_interval);
}

static int interval_set(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
    struct i2c_client *client = to_i2c_client(dev->parent);
    struct i2c_gyro_sensor *gyro = (struct i2c_gyro_sensor *)i2c_get_clientdata(client);
	unsigned int val = 0;
	char msg[256];

	if (count > 256)
		count = 256;
	memcpy(msg, buf, count-1);
	msg[count-1] = '\0';
	val = (unsigned int)simple_strtoul(msg, NULL, 10);
	if (val < MIN_INTERVAL_MS)
		val = MIN_INTERVAL_MS;
	gyro->sample_interval = val;
	dev_info(dev, "CWB01C01 2-axis Gyroscope sample interval is %d ms\n", gyro->sample_interval);
	return count;
}

static ssize_t wake_set(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
	return count;
}

static int data_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
    struct i2c_client *client = to_i2c_client(dev->parent);
    struct i2c_gyro_sensor *gyro = (struct i2c_gyro_sensor *)i2c_get_clientdata(client);

	if (atomic_read(&gyro->enabled)) {
		cwb01c01_read_raw(gyro);
	}
	return sprintf(buf, "%d %d %d\n",
			gyro->angular_velocity[0],
			gyro->angular_velocity[1],
			gyro->angular_velocity[2]);
}

static int status_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	return 0;
}

static DEVICE_ATTR(active, S_IRUGO|S_IWUGO, active_show, active_set);
static DEVICE_ATTR(interval, S_IRUGO|S_IWUGO, interval_show, interval_set);
static DEVICE_ATTR(data, S_IRUGO, data_show, NULL);
static DEVICE_ATTR(wake, S_IRUGO|S_IWUGO, NULL, wake_set);
static DEVICE_ATTR(status, S_IRUGO|S_IWUGO, status_show, NULL);

static struct attribute *sysfs_attributes[] = {
		&dev_attr_status.attr,
		&dev_attr_interval.attr,
		&dev_attr_data.attr,
		&dev_attr_active.attr,
		&dev_attr_wake.attr,
		NULL
};

static struct attribute_group sysfs_attribute_group = {
	.attrs = sysfs_attributes
};


static void cwb01c01_report_values(struct i2c_gyro_sensor *gyro)
{
	input_report_abs(gyro->input, ABS_X, gyro->angular_velocity[0]);
	input_report_abs(gyro->input, ABS_Y, gyro->angular_velocity[1]);
	input_report_abs(gyro->input, ABS_Z, gyro->angular_velocity[2]);
	input_sync(gyro->input);
}


static void cwb01c01_work_func(struct work_struct *work)
{
	struct i2c_gyro_sensor *gyro;

	gyro = container_of((struct delayed_work *)work,
			 	 	 	 struct i2c_gyro_sensor,
			 	 	 	 input_work);

	if (!cwb01c01_read_raw(gyro))
		cwb01c01_report_values(gyro);

	schedule_delayed_work(&gyro->input_work, msecs_to_jiffies(
			gyro->sample_interval));
}


static int cwb01c01_input_init(struct i2c_gyro_sensor *gyro)
{
	int err = 0;

	gyro->input = input_allocate_device();
	if (!gyro->input) {
		err = -ENOMEM;
		dev_err(&gyro->client->dev, "Failed to allocate input device\n");
		goto failed;
	}

	gyro->input->name = "CWB01C01 2-axis gyroscope";
	gyro->input->id.bustype = BUS_I2C;
	gyro->input->dev.parent = &gyro->client->dev;

	set_bit(EV_ABS, gyro->input->evbit);
	input_set_abs_params(gyro->input, ABS_X, -DPS_MAX, DPS_MAX, 0, 0);
	input_set_abs_params(gyro->input, ABS_Y, -DPS_MAX, DPS_MAX, 0, 0);
	input_set_abs_params(gyro->input, ABS_Z, -DPS_MAX, DPS_MAX, 0, 0);

	err = input_register_device(gyro->input);
	if (err) {
		dev_err(&gyro->input->dev, "Failed to register input device\n");
		input_free_device(gyro->input);
		gyro->input = NULL;
		goto failed;
	}

	INIT_DELAYED_WORK(&gyro->input_work, cwb01c01_work_func);

	return 0;

failed:
	return err;
}

#ifdef CONFIG_HAS_EARLYSUSPEND
static void cwb01c01_early_suspend(struct early_suspend *h)
{
	struct i2c_gyro_sensor *gyro = container_of(h, struct i2c_gyro_sensor, early_suspend);

	if (((struct axis_sensor_platform_data *)(gyro->client->dev.platform_data))->set_power) {
		((struct axis_sensor_platform_data *)(gyro->client->dev.platform_data))->set_power(0);
	}
	return;
}

static void cwb01c01_late_resume(struct early_suspend *h)
{
	struct i2c_gyro_sensor *gyro = container_of(h, struct i2c_gyro_sensor, early_suspend);

	if (((struct axis_sensor_platform_data *)(gyro->client->dev.platform_data))->set_power) {
		((struct axis_sensor_platform_data *)(gyro->client->dev.platform_data))->set_power(1);
	}
	return;
}
#endif

static int cwb01c01_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct i2c_gyro_sensor *gyro;
	int err = 0;

	if (((struct axis_sensor_platform_data *)(client->dev.platform_data))->set_power)
		((struct axis_sensor_platform_data *)(client->dev.platform_data))->set_power(1);
	gyro = kzalloc(sizeof(struct i2c_gyro_sensor), GFP_KERNEL);
	if (gyro == NULL) {
		dev_err(&client->dev, "Failed to allocate memory\n");
		err = -ENOMEM;
		goto failed_alloc;
	}

	i2c_set_clientdata(client, gyro);
	gyro->client = client;
	gyro->sample_interval = DEFAULT_SAMPLE_INTERVAL;
	atomic_set(&gyro->enabled, 0);
	mutex_init(&gyro->i2c_lock);
	mutex_init(&gyro->set_lock);

	err = cwb01c01_input_init(gyro);
	if (err)
		goto exit_free;

	/* Creates attributes */
	err = sysfs_create_group(&gyro->input->dev.kobj, &sysfs_attribute_group);
	if (err)
		goto exit_free_input;

#ifdef CONFIG_HAS_EARLYSUSPEND
	gyro->early_suspend.level = EARLY_SUSPEND_LEVEL_DISABLE_FB;
	gyro->early_suspend.suspend = cwb01c01_early_suspend;
	gyro->early_suspend.resume = cwb01c01_late_resume;
	register_early_suspend(&gyro->early_suspend);
#endif
	return 0;

exit_free_input:
	input_free_device(gyro->input);
exit_free:
	kfree(gyro);
failed_alloc:
	pr_err("%s: Initialized driver failed\n", DEV_NAME);
	return err;
}

static int cwb01c01_remove(struct i2c_client *client)
{
	return 0;
}

static int cwb01c01_suspend(struct i2c_client *client, pm_message_t mesg)
{
	return 0;
}

static int cwb01c01_resume(struct i2c_client *client)
{
	return 0;
}

static const struct i2c_device_id cwb01c01_id[] = {
	{ DEV_NAME , 0 },
	{},
};

MODULE_DEVICE_TABLE(i2c, cwb01c01_id);

static struct i2c_driver cwb01c01_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = DEV_NAME,
	},
	.class = I2C_CLASS_HWMON,
	.probe = cwb01c01_probe,
	.remove = __devexit_p(cwb01c01_remove),
	.id_table = cwb01c01_id,
#ifdef CONFIG_PM
	.suspend = cwb01c01_suspend,
	.resume = cwb01c01_resume,
#endif

};

static int __init cwb01c01_init(void)
{
	printk("%s: gyroscope driver init\n", DEV_NAME);

	return i2c_add_driver(&cwb01c01_driver);
}

static void __exit cwb01c01_exit(void)
{
	printk("CWB01C01 exit\n");

	i2c_del_driver(&cwb01c01_driver);
	return;
}


module_init(cwb01c01_init);
module_exit(cwb01c01_exit);

MODULE_DESCRIPTION("CWB01C01 digital 2-axis gyroscope driver");
MODULE_AUTHOR("Joe Wei <joewei@cywee.com>");
MODULE_LICENSE("GPL");
