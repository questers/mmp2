/*
*  cywee.c - Cywee 9-axis motion sensor driver
*
*  Copyright (C) Yifan Zhang <zhangyf@marvell.com>
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef __AMI602_H__
#define __AMI602_H__

#define AMI602_IOCTL_INIT                  _IO(AMI602IO, 0x01)
#define AMI602_IOCTL_READ_CHIPINFO         _IOR(AMI602IO, 0x02, int)
#define AMI602_IOCTL_READ_SENSORDATA       _IOR(AMI602IO, 0x03, int)
#define AMI602_CMD_GET_FIRMWARE		0x17
#define AMI602_CMD_GET_MES			0x14
#define AMI602_CMD_REQ_MES			0x55
#define AMI602_CMD_SET_AEN			0x74
#define AMI602_BUFSIZE						256
#define AMI602IO						   0x82

#endif
