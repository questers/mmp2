/*
*  hmc5843.c - HMC5843 compass sensor driver
*
*  Copyright (C) Yifan Zhang <zhangyf@marvell.com>
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/sysfs.h>
#include <linux/timer.h>
#include <linux/input.h>
#include "hmc5843.h"

static struct i2c_client *g_client;
static int hmc5843_status;
static struct work_struct hmc5843_work;
static struct timer_list hmc5843_timer;
static int hmc5843_sample_interval;
static struct mutex hmc5843_lock;
static struct input_dev *hmc5843_input_dev;
static int hmc5843_user_count;

static int hmc5843_read_reg(u8 reg, u8 *buf, int count)
{
	int ret;
	ret = i2c_master_send(g_client, &reg, 1);
	if (ret < 0)
		return ret;
	ret = i2c_master_recv(g_client, buf, count);
	if (ret < 0)
		return ret;
	return ret;
}

static int hmc5843_write_reg(u8 reg, u8 val)
{
	int ret;

	ret = i2c_smbus_write_byte_data(g_client, reg, val);
	if (ret == 0)
		return 0;
	else
		return -EIO;
}
static int interval_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", hmc5843_sample_interval);
}
static int interval_set(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
	unsigned int val = 0;
	char msg[MAX_ECHO_LENGTH];
	if (count > MAX_ECHO_LENGTH)
		count = MAX_ECHO_LENGTH;
	memcpy(msg, buf, count-1);
	msg[count-1] = '\0';
	val = (unsigned int)simple_strtoul(msg, NULL, 10);
	if (val < MIN_MSECS)
		val = MIN_MSECS;
	hmc5843_sample_interval = val;
	dev_info(dev, "interval is %d ms\n", hmc5843_sample_interval);
	return count;
}

static int active_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	if (hmc5843_status == ON)
		return sprintf(buf, "1\n");
	else
		return sprintf(buf, "0\n");
}

/*Poll the light sensor*/
static void hmc5843_timer_func(unsigned long data)
{
	schedule_work(&hmc5843_work);
	mod_timer(&hmc5843_timer, jiffies +
			msecs_to_jiffies(hmc5843_sample_interval));
}

/*show the status of proximity sensor*/
static int active_set(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
	int ret;
	if (strcmp(buf, "1\n") == 0) {
		mutex_lock(&hmc5843_lock);
		hmc5843_user_count++;
		if (hmc5843_user_count == 1) {
			ret = hmc5843_write_reg(CONFIG_REGISTER_A, CONFIG_A_DATA);
			if (ret < 0)
				return ret;
			ret = hmc5843_write_reg(CONFIG_REGISTER_B, CONFIG_B_DATA);
			if (ret < 0)
				return ret;
			ret = hmc5843_write_reg(MODE_REGISTER, NORMAL_SET_DATA);
			if (ret < 0)
				return ret;
			hmc5843_status = ON;
			setup_timer(&hmc5843_timer, hmc5843_timer_func, 0);
			mod_timer(&hmc5843_timer,
				jiffies + msecs_to_jiffies(hmc5843_sample_interval));
			dev_info(dev, "The status is set to on\n");
		}
		mutex_unlock(&hmc5843_lock);
	} else if (strcmp(buf, "0\n") == 0) {
		mutex_lock(&hmc5843_lock);
		if (hmc5843_user_count > 0) {
			hmc5843_user_count--;
			if (hmc5843_user_count == 0) {
				ret = hmc5843_write_reg(MODE_REGISTER, SLEEP_SET_DATA);
				if (ret < 0)
					return ret;
				hmc5843_status = OFF;
				del_timer_sync(&hmc5843_timer);
				dev_info(dev, "The status is set to off\n");
			}
		}
		mutex_unlock(&hmc5843_lock);
	} else
		dev_info(dev, "unrecognized, using on/off\n");
	return count;
}

static int wake_set(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
	static int i = 1;
	if (i == 10)
		i = 1;
	input_report_abs(hmc5843_input_dev, ABS_MISC, i ++);

	return count;
}

static int data_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	char tmp[6];
	short result[3];
	int ret, i;

	char reg = DATA_X_MSB_ADDRESS;
	ret = i2c_master_send(g_client, &reg, 1);
	if (ret < 0) {
		printk("chip error ! \n");
		return -1;
	}
	for (i = 0; i < 6; i++) {
		ret = i2c_master_recv(g_client, tmp+i, 1);
		if (ret < 0) {
			printk("chip error ! \n");
			return -1;
		}
	}
	result[0] = tmp[0] << 8 | tmp[1];
	result[1] = tmp[2] << 8 | tmp[3];
	result[2] = tmp[4] << 8 | tmp[5];
	return sprintf(buf, "(%d,%d,%d)\n", result[0],result[1],result[2]);
}

static int status_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	return 0;
}



static DEVICE_ATTR(active, S_IRUGO|S_IWUGO, active_show,
		active_set);
static DEVICE_ATTR(interval, S_IRUGO|S_IWUGO, interval_show,
		interval_set);
static DEVICE_ATTR(data, S_IRUGO, data_show, NULL);
static DEVICE_ATTR(wake, S_IRUGO|S_IWUGO, NULL,
		wake_set);
static DEVICE_ATTR(status, S_IRUGO|S_IWUGO, status_show, NULL);


static struct attribute *hmc5843_attributes[] = {
		&dev_attr_active.attr,
		&dev_attr_interval.attr,
		&dev_attr_data.attr,
		&dev_attr_wake.attr,
		&dev_attr_status.attr,
		NULL
};

static struct attribute_group hmc5843_attribute_group = {
	.attrs = hmc5843_attributes
};

static int hmc5843_add_fs(struct device *device)
{
	return sysfs_create_group(&device->kobj,
			&hmc5843_attribute_group);
}

static int hmc5843_remove_fs(void)
{
	sysfs_remove_group(&g_client->dev.kobj,
						&hmc5843_attribute_group);
	return 0;
}

static int is_hmc5843(struct i2c_client *client)
{
	unsigned char buf1, buf2, buf3;
	int ret;
	ret = hmc5843_read_reg(ID_A_ADDRESS, &buf1, 1);
	if (ret < 0)
		return ret;
	ret = hmc5843_read_reg(ID_B_ADDRESS, &buf2, 1);
	if (ret < 0)
		return ret;
	ret = hmc5843_read_reg(ID_C_ADDRESS, &buf3, 1);
	if (ret < 0)
		return ret;
	if (buf1 == ID_REGISTER_A &&
		buf2 == ID_REGISTER_B &&
		buf3 == ID_REGISTER_C &&
		ret >= 0)
		return 0;
	else
		return -1;
}
static int hmc5843_register_device(struct i2c_client *client)
{
	int err;

	hmc5843_input_dev = input_allocate_device();
	hmc5843_input_dev->name       = "hmc5843 compass";
	hmc5843_input_dev->phys       = "compass/input0";
	hmc5843_input_dev->id.bustype = BUS_I2C;
	hmc5843_input_dev->id.vendor  = 0;

	__set_bit(EV_ABS, hmc5843_input_dev->evbit);
	__set_bit(ABS_X, hmc5843_input_dev->absbit);
	__set_bit(ABS_Y, hmc5843_input_dev->absbit);
	__set_bit(ABS_Z, hmc5843_input_dev->absbit);
	__set_bit(ABS_MISC, hmc5843_input_dev->absbit);
	__set_bit(ABS_BRAKE, hmc5843_input_dev->absbit);
	__set_bit(EV_SYN, hmc5843_input_dev->evbit);
	input_set_abs_params(hmc5843_input_dev, ABS_X, MIN_VALUE, MAX_VALUE, 0, 0);
	input_set_abs_params(hmc5843_input_dev, ABS_Y, MIN_VALUE, MAX_VALUE, 0, 0);
	input_set_abs_params(hmc5843_input_dev, ABS_Z, MIN_VALUE, MAX_VALUE, 0, 0);
	input_set_abs_params(hmc5843_input_dev, ABS_MISC, -10, 10, 0, 0);
	input_set_abs_params(hmc5843_input_dev, ABS_BRAKE, 0, 3, 0, 0);

	err = input_register_device(hmc5843_input_dev);
	if (err) {
		dev_err(&client->dev, "register input driver error\n");
		input_free_device(hmc5843_input_dev);
		hmc5843_input_dev = NULL;
		return err;
	}
	return 0;
}

static void hmc5843_report_value(struct work_struct *work)
{
	char tmp[6];
	short result[3];
	int ret, i;

	char reg = DATA_X_MSB_ADDRESS;
	ret = i2c_master_send(g_client, &reg, 1);
	if (ret < 0)
		return;
	for (i = 0; i < 6; i++) {
		ret = i2c_master_recv(g_client, tmp+i, 1);
		if (ret < 0)
			return;
	}
	if (ret < 0) {
		mod_timer(&hmc5843_timer,
			jiffies + msecs_to_jiffies(hmc5843_sample_interval));
		return;
	}
	result[0] = tmp[0] << 8 | tmp[1];
	result[1] = tmp[2] << 8 | tmp[3];
	result[2] = tmp[4] << 8 | tmp[5];
	input_report_abs(hmc5843_input_dev, ABS_X, result[0]);
	input_report_abs(hmc5843_input_dev, ABS_Y, result[1]);
	input_report_abs(hmc5843_input_dev, ABS_Z, result[2]);
	input_sync(hmc5843_input_dev);
}

static int __devinit hmc5843_probe(struct i2c_client *client,
		const struct i2c_device_id *id)
{
	int ret;

	dev_info(&client->dev, "This is %s\n", __func__);
	g_client = client;
	ret = is_hmc5843(client);
	if (ret < 0) {
		dev_err(&client->dev, "hmc5843 unavailable!\n");
		return -ENXIO;
	} else {
		dev_info(&client->dev, "hmc5843 is detected\n");
	}
	hmc5843_status = OFF;
	hmc5843_sample_interval = DEFAULT_SAMPLE_INTERVAL;
	hmc5843_user_count = 0;
	mutex_init(&hmc5843_lock);

	ret = hmc5843_register_device(client);
	hmc5843_add_fs(&hmc5843_input_dev->dev);
	INIT_WORK(&hmc5843_work, hmc5843_report_value);
	if (ret)
		dev_err(&client->dev, "%s is "
		"unable to register input device\n", __func__);
	return 0;
}
static int hmc5843_suspend(struct i2c_client *client, pm_message_t state)
{
	int ret;
	if (hmc5843_status == ON) {
		ret = hmc5843_write_reg(MODE_REGISTER, SLEEP_SET_DATA);
		del_timer_sync(&hmc5843_timer);
		if (ret < 0)
			printk("hmc5843 not disabled \n");
	}
	return 0;
}
static int hmc5843_resume(struct i2c_client *client)
{
	int ret;
	if (hmc5843_status == ON) {
		ret = hmc5843_write_reg(MODE_REGISTER, NORMAL_SET_DATA);
		if (ret < 0)
			printk("hmc5843 not enabled ! \n");
		setup_timer(&hmc5843_timer, hmc5843_timer_func, 0);
		mod_timer(&hmc5843_timer,
				jiffies + msecs_to_jiffies(hmc5843_sample_interval));
	}
	return 0;
}
static int hmc5843_remove(struct i2c_client *client)
{
	hmc5843_remove_fs();
	del_timer_sync(&hmc5843_timer);
	return 0;
}
static const struct i2c_device_id hmc5843_id[] = {
	{"hmc5843", 0},
	{}
};


static struct i2c_driver hmc5843_driver = {
	.driver = {
		.name	= "hmc5843",
	},
	.id_table	= hmc5843_id,
	.probe		= hmc5843_probe,
	.remove		= hmc5843_remove,
	.suspend	= hmc5843_suspend,
	.resume		= hmc5843_resume,
};

static int __init hmc5843_init(void)
{
	return i2c_add_driver(&hmc5843_driver);
}

static void  __exit hmc5843_exit(void)
{
	i2c_del_driver(&hmc5843_driver);
}
module_init(hmc5843_init);
module_exit(hmc5843_exit);

MODULE_DESCRIPTION("hmc5843 compass driver");
MODULE_AUTHOR("Yifan Zhang<zhangyf@marvell.com>");
MODULE_LICENSE("GPL");
