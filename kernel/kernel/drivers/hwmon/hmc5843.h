/*
*  hmc5843.c - HMC5843 compass sensor driver
*
*  Copyright (C) Yifan Zhang <zhangyf@marvell.com>
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef __HMC5843_H__
#define __HMC5843_H__

#define ID_REGISTER_A 0x48
#define ID_REGISTER_B 0x34
#define ID_REGISTER_C 0x33
#define DATA_X_MSB_ADDRESS 3
#define ID_A_ADDRESS 10
#define ID_B_ADDRESS 11
#define ID_C_ADDRESS 12
#define CONFIG_REGISTER_A 0x00
#define CONFIG_REGISTER_B 0x01
#define MODE_REGISTER 0x02
#define DEFAULT_SAMPLE_INTERVAL 200
#define ON 1
#define OFF 0

#define SLEEP_SET_DATA  0x03
#define NORMAL_SET_DATA 0x00
#define CONFIG_A_DATA 0x10
#define CONFIG_B_DATA 0x20
#define MAX_ECHO_LENGTH 256
#define MIN_MSECS 100
#define MAX_VALUE 65536
#define MIN_VALUE -65536

#endif
