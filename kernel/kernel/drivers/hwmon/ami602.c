/* ami602.c - Aichi Steel 6-axis motion sensor
 *
 * Copyright (C) 2010 CyWee Group Ltd.
 * Author: Joe Wei <joewei@cywee.com>
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/sysfs.h>
#include <linux/string.h>
#include <linux/input.h>
#include <linux/timer.h>
#include <linux/mutex.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/gpio.h>
#include <linux/mutex.h>
#include <linux/earlysuspend.h>
#include <mach/mfp-mmp2.h>
#include <mach/axis_sensor.h>
#include "ami602.h"

#define ACC_NAME	"Aichi Steel 3-axis accelerometer"
#define MAG_NAME	"Aichi Steel 3-axis compass"
#define ORI_NAME	"orientation"

struct i2c_ami602_sensor {
	struct i2c_client *client;
	struct input_dev *acc_input;
	struct input_dev *mag_input;
	struct input_dev *ori_input;
	struct delayed_work input_work;

#ifdef CONFIG_HAS_EARLYSUSPEND
	struct early_suspend early_suspend;
#endif

	unsigned int sample_interval;

	atomic_t acc_enabled;
	atomic_t mag_enabled;
	atomic_t ori_enabled;
	atomic_t i2c_sync;
	struct mutex i2c_lock;
	struct mutex set_lock;
	struct mutex show_lock;
	int gpio;
	int acc_zero[3];
	int mag_zero[3];
	int acc_sen[3];
	int mag_sen[3];

	/* Data */
	int acc[3];
	int mag[3];
	int ori[3];
	int temperature;
};


static int ami602_send_trigger(struct i2c_ami602_sensor *sensor)
{
	gpio_direction_output(sensor->gpio, 0);
	udelay(200);
	gpio_direction_output(sensor->gpio, 1);
	udelay(100);
	return 0;
}


static int ami602_write(struct i2c_ami602_sensor *sensor, char cmd, const char *buf, size_t size)
{
	char local_buf[16];
	int res;

	local_buf[0] = cmd;
	if (buf != NULL)
		memcpy(local_buf + 1, buf, size);

	ami602_send_trigger(sensor);
	res = i2c_master_send(sensor->client, local_buf, size + 1);
	if (res <= 0) {
		pr_err("Failed to send command 0x%02X\n", cmd);
		return -1;
	}

	udelay(350);

	res = i2c_master_recv(sensor->client, local_buf, 1);
	if (res <= 0) {
		pr_err("Failed to receive response for command 0x%02X\n", cmd);
		return -1;
	}

	return local_buf[0];
}


static int ami602_read(struct i2c_ami602_sensor *sensor, char cmd, char *buf, size_t size)
{
	char local_buf[16];
	int res;

	ami602_send_trigger(sensor);
	res = i2c_master_send(sensor->client, &cmd, 0x1);
	if (res <= 0) {
		pr_err("Failed to send command %d\n", cmd);
		return -1;
	}

	udelay(350);

	res = i2c_master_recv(sensor->client, local_buf, size + 1);
	if (res <= 0 || local_buf[0]) {
		pr_err("Failed to receive response for command %d\n", cmd);
		return -1;
	}

	/* Skip error code */
	memcpy(buf, local_buf + 1, size);

	return 0;
}


static int ami602_read_stored_data(struct i2c_ami602_sensor *sensor, char index, char *buf, size_t size)
{
	int res = 0;

	res = ami602_write(sensor, AMI602_CMD_SET_INDEX_DAT, &index, 1);
	if (res) {
		pr_err("Failed to send command AMI602_CMD_SET_INDEX_DAT: 0x%02X\n", res);
		return -1;
	}

	res = ami602_read(sensor, AMI602_CMD_GET_DAT, buf, size);
	if (res) {
		pr_err("Failed to send command AMI602_CMD_GET_DAT: 0x%02X\n", res);
		return -1;
	}

	pr_debug("index: %d, (%d, %d, %d)\n", index,
			buf[0] << 8 | buf[1],
			buf[2] << 8 | buf[3],
			buf[4] << 8 | buf[5]
		);

	return 0;
}


static int ami602_read_calib_data(struct i2c_ami602_sensor *sensor)
{
	char buf[6];

	/* Read zero offsets of accelerometer and magnetic field sensors */
	if (ami602_read_stored_data(sensor, 0, buf, sizeof(buf))) {
		pr_err("Failed to read data in index 0\n");
		return -1;
	}
	sensor->mag_zero[2] = buf[0] << 8 | buf[1];
	sensor->mag_zero[0] = buf[2] << 8 | buf[3];
	sensor->acc_zero[2] = buf[4] << 8 | buf[5];

	if (ami602_read_stored_data(sensor, 1, buf, sizeof(buf))) {
		pr_err("Failed to read data in index 1\n");
		return -1;
	}
	sensor->acc_zero[0] = buf[0] << 8 | buf[1];
	sensor->acc_zero[1] = buf[2] << 8 | buf[3];
	sensor->mag_zero[1] = buf[4] << 8 | buf[5];

	/* Read sensitivity of accelerometer & magnetic field sensors */
	if (ami602_read_stored_data(sensor, 2, buf, sizeof(buf))) {
		pr_err("Failed to read data in index 2\n");
		return -1;
	}
	sensor->mag_sen[2] = buf[0] << 8 | buf[1];
	sensor->mag_sen[0] = buf[2] << 8 | buf[3];

	if (ami602_read_stored_data(sensor, 3, buf, sizeof(buf))) {
		pr_err("Failed to read data in index 3\n");
		return -1;
	}
	sensor->mag_sen[1] = buf[4] << 8 | buf[5];

	if (ami602_read_stored_data(sensor, 4, buf, sizeof(buf))) {
		pr_err("Failed to read data in index 4\n");
		return -1;
	}
	sensor->acc_sen[2] = buf[0] << 8 | buf[1];
	sensor->acc_sen[0] = buf[2] << 8 | buf[3];
	sensor->acc_sen[1] = buf[4] << 8 | buf[5];

	printk("zero:\nax=%d, ay=%d, az=%d\nmx=%d, my=%d, mz=%d\n"
		 "sensitivity\nax=%d, ay=%d, az=%d\nmx=%d, my=%d, mz=%d\n",
		 sensor->acc_zero[0], sensor->acc_zero[1], sensor->acc_zero[2],
		 sensor->mag_zero[0], sensor->mag_zero[1], sensor->mag_zero[2],
		 sensor->acc_sen[0], sensor->acc_sen[1], sensor->acc_sen[2],
		 sensor->mag_sen[0], sensor->mag_sen[1], sensor->mag_sen[2]
	);

	return 0;
}


static int ami602_read_raw(struct i2c_ami602_sensor *sensor)
{
	char cmd;
	u8 databuf[20];
	int ch1,ch2,ch3,ch4,ch5,ch6;
	int res = 0;

	mutex_lock(&sensor->i2c_lock);
	memset(databuf, 0, sizeof(databuf));

	ami602_send_trigger(sensor);
	cmd = AMI602_CMD_REQ_MES;
	res = i2c_master_send(sensor->client, &cmd, 0x1);
	if (res <= 0) {
		pr_err("Failed to send command 0x%02X\n", cmd);
		goto free_lock;
	}

	udelay(350);

	res = i2c_master_recv(sensor->client, databuf, 1);
	if (res <= 0) {
		pr_err("Failed to receive response for command 0x%02X\n", cmd);
		goto free_lock;
	}

	/* Wait for measuring (2.28 ms for 6-axis measurement)*/
	msleep(4);

	ami602_send_trigger(sensor);
	cmd = AMI602_CMD_GET_MES_SUSPEND;
	res = i2c_master_send(sensor->client, &cmd, 0x1);
	if (res <= 0) {
		pr_err("Failed to send command 0x%02X\n", cmd);
		goto free_lock;
	}

	udelay(350);

	res = i2c_master_recv(sensor->client, databuf, 10);
	if (res <= 0) {
		pr_err("Failed to receive response for command 0x%02X\n", cmd);
		goto free_lock;
	}

	ch1 = ((int) databuf[1]) << 4 | ((int) databuf[2]) >> 4;
	ch2 = ((int) databuf[2] & 0x0f)   << 8  | ((int) databuf[3]);
	ch3 = ((int) databuf[4]) << 4 | ((int) databuf[5]) >> 4;
	ch4 = ((int) databuf[5] & 0x0f)   << 8  | ((int) databuf[6]);
	ch5 = ((int) databuf[7]) << 4 | ((int) databuf[8]) >> 4;
	ch6 = ((int) databuf[8] & 0x0f)   << 8  | ((int) databuf[9]);

	sensor->mag[0] = ch2 - 2048;
	sensor->mag[1] = ch6 - 2048;
	sensor->mag[2] = ch1 - 2048;
	sensor->acc[0] = -(ch4 - sensor->acc_zero[0]) * PRECISION * 2 / sensor->acc_sen[0];
	sensor->acc[1] = (ch5 - sensor->acc_zero[1]) * PRECISION * 2 / sensor->acc_sen[1];
	sensor->acc[2] = -(ch3 - sensor->acc_zero[2]) * PRECISION * 2 / sensor->acc_sen[2];

	res = 0;

free_lock:
	mutex_unlock(&sensor->i2c_lock);
	return res;
}


static int ami602_enable_acc(struct i2c_ami602_sensor *sensor, char enabled)
{
	char databuf[1];
	int res = 0;

	mutex_lock(&sensor->i2c_lock);
	databuf[0] = enabled;
	res = ami602_write(sensor, AMI602_CMD_SET_AEN, databuf, sizeof(databuf));
	if (res) {
		pr_err("Failed to %s ami602 accelerometer: 0x%02X\n", enabled ? "enabled" : "disabled", res);
		goto free_lock;
	}

	udelay(30);

	pr_debug("ami602 accelerometer %s\n", enabled ? "enabled" : "disabled");

free_lock:
	mutex_unlock(&sensor->i2c_lock);
	return res;
}


static int ami602_power_on(struct i2c_ami602_sensor *sensor)
{
	return 0;
}


static int ami602_power_off(struct i2c_ami602_sensor *sensor)
{
	return 0;
}


static int ami602_ori_report_control(struct i2c_ami602_sensor *sensor)
{
	int value = (atomic_read(&sensor->ori_enabled) << 16) |
				(atomic_read(&sensor->mag_enabled) << 17) |
			     sensor->sample_interval;

	pr_debug("%s(): value = 0x%08X\n", __func__, value);

	input_report_abs(sensor->ori_input, ABS_THROTTLE, value);
	input_sync(sensor->ori_input);

	return 0;
}


static int ami602_enable(struct i2c_ami602_sensor *sensor, int enabled, const char *sensor_name)
{
	atomic_t *enabled_atomic = NULL;
	int is_acc = strcmp(sensor_name, ACC_NAME) ? 0 : 1;

	if (is_acc)
		enabled_atomic = &sensor->acc_enabled;
	else
		enabled_atomic = &sensor->mag_enabled;

	if (enabled) {
		if (ami602_power_on(sensor) < 0) {
			dev_err(&sensor->client->dev, "Failed to power on %s\n", DEV_NAME);
			atomic_set(enabled_atomic, 0);
			return -1;
		}

		if (is_acc) {
			if (ami602_enable_acc(sensor, 1)) {
				atomic_cmpxchg(enabled_atomic, 1, 0);
			}
		} else {
			ami602_ori_report_control(sensor);
		}
		schedule_delayed_work(&sensor->input_work, msecs_to_jiffies(
					sensor->sample_interval));
		atomic_set(&sensor->i2c_sync, 1);

		pr_debug("ami602 %s enabled\n", sensor_name);
	} else {
		if (!atomic_read(&sensor->acc_enabled) &&
				!atomic_read(&sensor->mag_enabled)) {
			pr_debug("cancel work queue\n");
			cancel_delayed_work_sync(&sensor->input_work);
			atomic_set(&sensor->i2c_sync, 0);
		}

		if (is_acc)
			ami602_enable_acc(sensor, 0);
		else {
			ami602_power_off(sensor);
			ami602_ori_report_control(sensor);
		}
		pr_debug("ami602 %s disabled\n", sensor_name);
	}

	return 0;
}

static void sensor_enable(struct i2c_ami602_sensor *sensor, struct input_dev *input, int enabled)
{
	mutex_lock(&sensor->set_lock);
	if (strcmp(input->name, ORI_NAME) == 0) {
		if (enabled) {
			pr_debug("ami602 %s enabled\n", ORI_NAME);
			ami602_ori_report_control(sensor);
		} else {
			pr_debug("ami602 %s disabled\n", ORI_NAME);
			ami602_ori_report_control(sensor);
		}
	} else if (strcmp(input->name, ACC_NAME) == 0) {
		ami602_enable(sensor, enabled, ACC_NAME);
	} else {
		ami602_enable(sensor, enabled, MAG_NAME);
	}	
	mutex_unlock(&sensor->set_lock);
}

static int active_set(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
	struct input_dev *input = to_input_dev(dev);
	struct i2c_client *client = to_i2c_client(dev->parent);
	struct i2c_ami602_sensor *sensor =
		(struct i2c_ami602_sensor *)i2c_get_clientdata(client);
	int enabled = (strcmp(buf,"1\n") == 0) ? 1 : 0;

	pr_debug("%s(): %s\n", __func__, input->name);

	if (enabled) {
		if (((strcmp(input->name, ORI_NAME) == 0) && (!atomic_cmpxchg(&sensor->ori_enabled, 0, 1)))
				|| ((strcmp(input->name, ACC_NAME) == 0) && (!atomic_cmpxchg(&sensor->acc_enabled, 0, 1)))
				|| ((strcmp(input->name, MAG_NAME) == 0) && (!atomic_cmpxchg(&sensor->mag_enabled, 0, 1)))) {
			if (((struct axis_sensor_platform_data *)(sensor->client->dev.platform_data))->set_power) {
				((struct axis_sensor_platform_data *)(sensor->client->dev.platform_data))->set_power(1);
			}
			sensor_enable(sensor, input, enabled);
		}
	} else {
		if (((strcmp(input->name, ORI_NAME) == 0) && (atomic_cmpxchg(&sensor->ori_enabled, 1, 0)))
				|| ((strcmp(input->name, ACC_NAME) == 0) && (atomic_cmpxchg(&sensor->acc_enabled, 1, 0)))
				|| ((strcmp(input->name, MAG_NAME) == 0) && (atomic_cmpxchg(&sensor->mag_enabled, 1, 0)))) {
			sensor_enable(sensor, input, enabled);
			if ((strcmp(input->name, ORI_NAME) == 0) && (atomic_read(&sensor->i2c_sync))) {
				msleep(sensor->sample_interval);
			}
			if (((struct axis_sensor_platform_data *)(sensor->client->dev.platform_data))->set_power) {
				((struct axis_sensor_platform_data *)(sensor->client->dev.platform_data))->set_power(0);
			}
		}
	}
	return count;
}

static int active_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct input_dev *input = to_input_dev(dev);
	struct i2c_client *client = to_i2c_client(dev->parent);
	struct i2c_ami602_sensor *sensor = (struct i2c_ami602_sensor *)i2c_get_clientdata(client);
	atomic_t *enabled_atomic = NULL;

	if (strcmp(input->name, ORI_NAME) == 0)
		enabled_atomic = &sensor->ori_enabled;
	else if (strcmp(input->name, ACC_NAME) == 0)
		enabled_atomic = &sensor->acc_enabled;
	else
		enabled_atomic = &sensor->mag_enabled;


	if (atomic_read(enabled_atomic))
		return sprintf(buf, "1\n");
	else
		return sprintf(buf, "0\n");
}


static int interval_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
    struct i2c_client *client = to_i2c_client(dev->parent);
    struct i2c_ami602_sensor *sensor = (struct i2c_ami602_sensor *)i2c_get_clientdata(client);

    return sprintf(buf, "%d\n", sensor->sample_interval);
}

static int interval_set(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
    struct i2c_client *client = to_i2c_client(dev->parent);
    struct i2c_ami602_sensor *sensor = (struct i2c_ami602_sensor *)i2c_get_clientdata(client);
	unsigned int val = 0;
	char msg[256];

	if (count > 256)
		count = 256;
	memcpy(msg, buf, count-1);
	msg[count-1] = '\0';
	val = (unsigned int)simple_strtoul(msg, NULL, 10);
	if (val < MIN_INTERVAL_MS)
		val = MIN_INTERVAL_MS;
	sensor->sample_interval = val;

	return count;
}

static ssize_t wake_set(struct device *dev,
		struct device_attribute *attr,
		const char *buf, size_t count)
{
	return count;
}

static int data_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	struct input_dev *input = to_input_dev(dev);
	struct i2c_client *client = to_i2c_client(dev->parent);
	struct i2c_ami602_sensor *sensor =
		(struct i2c_ami602_sensor *)i2c_get_clientdata(client);
	int vec[3];

	mutex_lock(&sensor->show_lock);
	if ((atomic_read(&sensor->acc_enabled)) ||
		(atomic_read(&sensor->mag_enabled))) {
		if (!ami602_read_raw(sensor)) {
			if (strcmp(input->name, ACC_NAME) == 0)
				memcpy(vec, sensor->acc, sizeof(vec));
			else if (strcmp(input->name, ORI_NAME) == 0)
				memcpy(vec, sensor->ori, sizeof(vec));
			else
				memcpy(vec, sensor->mag, sizeof(vec));
		}
	} else {
		memset(vec, 0x00, sizeof(vec));
	}
	mutex_unlock(&sensor->show_lock);
	return sprintf(buf, "%d %d %d\n", vec[0], vec[1], vec[2]);
}

static int status_show(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	return 0;
}

static DEVICE_ATTR(active, S_IRUGO|S_IWUGO, active_show, active_set);
static DEVICE_ATTR(interval, S_IRUGO|S_IWUGO, interval_show, interval_set);
static DEVICE_ATTR(data, S_IRUGO, data_show, NULL);
static DEVICE_ATTR(wake, S_IRUGO|S_IWUGO, NULL, wake_set);
static DEVICE_ATTR(status, S_IRUGO|S_IWUGO, status_show, NULL);

static struct attribute *sysfs_attributes[] = {
		&dev_attr_status.attr,
		&dev_attr_interval.attr,
		&dev_attr_data.attr,
		&dev_attr_active.attr,
		&dev_attr_wake.attr,
		NULL
};

static struct attribute_group sysfs_attribute_group = {
	.attrs = sysfs_attributes
};


static void ami602_mag_report_values(struct i2c_ami602_sensor *sensor)
{
	input_report_abs(sensor->mag_input, ABS_HAT0X, sensor->mag[0]);
	input_report_abs(sensor->mag_input, ABS_HAT0Y, sensor->mag[1]);
	input_report_abs(sensor->mag_input, ABS_HAT1X, sensor->mag[2]);
	input_sync(sensor->mag_input);

	pr_debug("mx=%04d, my=%04d, mz=%04d\n",
			sensor->mag[0], sensor->mag[1], sensor->mag[2]);
}

static void ami602_acc_report_values(struct i2c_ami602_sensor *sensor)
{
	input_report_abs(sensor->acc_input, ABS_X, sensor->acc[0]);
	input_report_abs(sensor->acc_input, ABS_Y, sensor->acc[1]);
	input_report_abs(sensor->acc_input, ABS_Z, sensor->acc[2]);
	input_sync(sensor->acc_input);

	pr_debug("ax=%04d, ay=%04d, az=%04d\n",
		sensor->acc[0], sensor->acc[1], sensor->acc[2]);
}


static void ami602_work_func(struct work_struct *work)
{
	struct i2c_ami602_sensor *sensor;

	sensor = container_of((struct delayed_work *)work,
			 	 	 	 struct i2c_ami602_sensor,
			 	 	 	 input_work);
	if (ami602_read_raw(sensor)) {
		pr_err("Read ami602 data failed.\n");
		if (atomic_read(&sensor->acc_enabled)) {
			pr_err("Re-enable accelerometer...\n");
			ami602_enable(sensor, 0, ACC_NAME);
			msleep(4);
			ami602_enable(sensor,1, ACC_NAME);
		}
	} else {
		if (atomic_read(&sensor->acc_enabled))
			ami602_acc_report_values(sensor);
		if (atomic_read(&sensor->mag_enabled))
			ami602_mag_report_values(sensor);
	}

	schedule_delayed_work(&sensor->input_work, msecs_to_jiffies(
			sensor->sample_interval));
}


static int ami602_acc_input_init(struct i2c_ami602_sensor *sensor)
{
	int err = 0;

	sensor->acc_input = input_allocate_device();
	if (!sensor->acc_input) {
		err = -ENOMEM;
		dev_err(&sensor->client->dev, "Failed to allocate input device\n");
		goto failed;
	}

	sensor->acc_input->name = ACC_NAME;
	sensor->acc_input->id.bustype = BUS_I2C;
	sensor->acc_input->dev.parent = &sensor->client->dev;

	set_bit(EV_ABS, sensor->acc_input->evbit);
	input_set_abs_params(sensor->acc_input, ABS_X, -ACC_MAX, ACC_MAX, 0, 0);
	input_set_abs_params(sensor->acc_input, ABS_Y, -ACC_MAX, ACC_MAX, 0, 0);
	input_set_abs_params(sensor->acc_input, ABS_Z, -ACC_MAX, ACC_MAX, 0, 0);

	err = input_register_device(sensor->acc_input);
	if (err) {
		dev_err(&sensor->acc_input->dev, "Failed to register input device\n");
		input_free_device(sensor->acc_input);
		sensor->acc_input = NULL;
		goto failed;
	}

	return 0;

failed:
	return err;
}


static int ami602_mag_input_init(struct i2c_ami602_sensor *sensor)
{
	int err = 0;

	sensor->mag_input = input_allocate_device();
	if (!sensor->mag_input) {
		err = -ENOMEM;
		dev_err(&sensor->client->dev, "Failed to allocate input device\n");
		goto failed;
	}

	sensor->mag_input->name = MAG_NAME;
	sensor->mag_input->id.bustype = BUS_I2C;
	sensor->mag_input->dev.parent = &sensor->client->dev;

	set_bit(EV_ABS, sensor->mag_input->evbit);

	input_set_abs_params(sensor->mag_input, ABS_HAT0X, -MAG_MAX, MAG_MAX, 0, 0);
	input_set_abs_params(sensor->mag_input, ABS_HAT0Y, -MAG_MAX, MAG_MAX, 0, 0);
	input_set_abs_params(sensor->mag_input, ABS_HAT1X, -MAG_MAX, MAG_MAX, 0, 0);
	input_set_abs_params(sensor->mag_input, ABS_X, -MAG_MAX, MAG_MAX, 0, 0);
	input_set_abs_params(sensor->mag_input, ABS_Y, -MAG_MAX, MAG_MAX, 0, 0);
	input_set_abs_params(sensor->mag_input, ABS_Z, -MAG_MAX, MAG_MAX, 0, 0);
	input_set_abs_params(sensor->mag_input, ABS_BRAKE, 0, 5, 0, 0);
	input_set_abs_params(sensor->mag_input, ABS_MISC, 0, 5, 0, 0);

	err = input_register_device(sensor->mag_input);
	if (err) {
		dev_err(&sensor->mag_input->dev, "Failed to register input device\n");
		input_free_device(sensor->mag_input);
		sensor->mag_input = NULL;
		goto failed;
	}

	return 0;

failed:
	return err;
}


static int ami602_ori_input_init(struct i2c_ami602_sensor *sensor)
{
	int err = 0;

	sensor->ori_input = input_allocate_device();
	if (!sensor->ori_input) {
		err = -ENOMEM;
		dev_err(&sensor->client->dev, "Failed to allocate input device\n");
		goto failed;
	}

	sensor->ori_input->name = ORI_NAME;
	sensor->ori_input->id.bustype = BUS_VIRTUAL;
	sensor->ori_input->dev.parent = &sensor->client->dev;

	set_bit(EV_ABS, sensor->ori_input->evbit);
	input_set_abs_params(sensor->ori_input, ABS_X, -MAG_MAX, MAG_MAX, 0, 0);
	input_set_abs_params(sensor->ori_input, ABS_Y, -MAG_MAX, MAG_MAX, 0, 0);
	input_set_abs_params(sensor->ori_input, ABS_Z, -MAG_MAX, MAG_MAX, 0, 0);
	input_set_abs_params(sensor->ori_input, ABS_THROTTLE, 0, 0x1ffff, 0, 0);

	err = input_register_device(sensor->ori_input);
	if (err) {
		dev_err(&sensor->ori_input->dev, "Failed to register input device for orientation sensor\n");
		input_free_device(sensor->ori_input);
		sensor->ori_input = NULL;
		goto failed;
	}

	return 0;

failed:
	return err;
}

#ifdef CONFIG_HAS_EARLYSUSPEND
static void ami602_early_suspend(struct early_suspend *h)
{
	struct i2c_ami602_sensor *sensor = container_of(h, struct i2c_ami602_sensor, early_suspend);

	pr_debug("%s\n", __func__);
	if (((struct axis_sensor_platform_data *)(sensor->client->dev.platform_data))->set_power) {
		((struct axis_sensor_platform_data *)(sensor->client->dev.platform_data))->set_power(0);
	}
	return;
}

static void ami602_late_resume(struct early_suspend *h)
{
	struct i2c_ami602_sensor *sensor = container_of(h, struct i2c_ami602_sensor, early_suspend);

	pr_debug("%s\n", __func__);
	if (((struct axis_sensor_platform_data *)(sensor->client->dev.platform_data))->set_power) {
		((struct axis_sensor_platform_data *)(sensor->client->dev.platform_data))->set_power(1);
	}
	return;
}
#endif

static int ami602_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct i2c_ami602_sensor *sensor;
	int err = 0;

	pr_debug("%s\n", __func__);

	if (((struct axis_sensor_platform_data *)(client->dev.platform_data))->set_power)
		((struct axis_sensor_platform_data *)(client->dev.platform_data))->set_power(1);

	sensor = kzalloc(sizeof(struct i2c_ami602_sensor), GFP_KERNEL);
	if (sensor == NULL) {
		dev_err(&client->dev, "Failed to allocate memory\n");
		err = -ENOMEM;
		goto failed_alloc;
	}

	i2c_set_clientdata(client, sensor);
	sensor->client = client;
	sensor->sample_interval = DEFAULT_SAMPLE_INTERVAL;
	atomic_set(&sensor->acc_enabled, 0);
	atomic_set(&sensor->mag_enabled, 0);
	atomic_set(&sensor->ori_enabled, 0);
	atomic_set(&sensor->i2c_sync, 0);
	mutex_init(&sensor->i2c_lock);
	mutex_init(&sensor->set_lock);
	mutex_init(&sensor->show_lock);
	sensor->gpio = mfp_to_gpio(GPIO93_GPIO93);
	if (gpio_request(sensor->gpio, "ami602")) {
		pr_err("gpio %d request failed \n", sensor->gpio);
		err = -EBUSY;
		goto exit_free;
	}

	err = ami602_acc_input_init(sensor);
	if (err)
		goto exit_free;

	err = ami602_mag_input_init(sensor);
	if (err)
		goto exit_free_acc_input;

	err = ami602_ori_input_init(sensor);
	if (err)
		goto exit_free_mag_input;

	INIT_DELAYED_WORK(&sensor->input_work, ami602_work_func);

	err = ami602_read_calib_data(sensor);
	if (err) {
		pr_err("Read calibration data failed.\n");
		goto exit_free_ori_input;
	}

	/* Creates attributes */
	err = sysfs_create_group(&sensor->acc_input->dev.kobj, &sysfs_attribute_group);
	if (err)
		goto exit_free_ori_input;

	err = sysfs_create_group(&sensor->mag_input->dev.kobj, &sysfs_attribute_group);
	if (err)
		goto exit_free_ori_input;

	err = sysfs_create_group(&sensor->ori_input->dev.kobj, &sysfs_attribute_group);
	if (err)
		goto exit_free_ori_input;

#ifdef CONFIG_HAS_EARLYSUSPEND
	sensor->early_suspend.level = EARLY_SUSPEND_LEVEL_DISABLE_FB;
	sensor->early_suspend.suspend = ami602_early_suspend;
	sensor->early_suspend.resume = ami602_late_resume;
	register_early_suspend(&sensor->early_suspend);
#endif

	return 0;

exit_free_ori_input:
	input_free_device(sensor->ori_input);
exit_free_mag_input:
	input_free_device(sensor->mag_input);
exit_free_acc_input:
	input_free_device(sensor->acc_input);
exit_free:
	kfree(sensor);
failed_alloc:
	pr_err("%s: Initialized driver failed\n", DEV_NAME);
	return err;
}

static int ami602_remove(struct i2c_client *client)
{
	return 0;
}

static int ami602_resume(struct i2c_client *client)
{
	return 0;
}

static int ami602_suspend(struct i2c_client *client, pm_message_t mesg)
{
#if 0
	struct i2c_ami602_sensor *sensor = (struct i2c_ami602_sensor *)i2c_get_clientdata(client);
	char databuf[2];
	int res = 0;

	databuf[0] = 's';
	databuf[1] = 'p';
	res = ami602_write(sensor, AMI602_CMD_SET_SUSPEND, databuf, sizeof(databuf));
	if (res) {
		pr_debug("Failed to put ami602 to sleep: 0x%02X\n", res);
		return -1;
	}
#endif

	return 0;
}

static const struct i2c_device_id ami602_id[] = {
	{ DEV_NAME , 0 },
	{},
};

MODULE_DEVICE_TABLE(i2c, ami602_id);

static struct i2c_driver ami602_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = DEV_NAME,
	},
	.class = I2C_CLASS_HWMON,
	.probe = ami602_probe,
	.remove = __devexit_p(ami602_remove),
	.id_table = ami602_id,
#ifdef CONFIG_PM
	.suspend = ami602_suspend,
	.resume = ami602_resume,
#endif

};

static int __init ami602_init(void)
{
	return i2c_add_driver(&ami602_driver);
}

static void __exit ami602_exit(void)
{
	i2c_del_driver(&ami602_driver);
	return;
}


module_init(ami602_init);
module_exit(ami602_exit);

MODULE_DESCRIPTION("Aichi Steel 6-axis motion sensor");
MODULE_AUTHOR("Joe Wei <joewei@cywee.com>");
MODULE_LICENSE("GPL");

