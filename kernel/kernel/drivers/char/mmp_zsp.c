/*
 *
 * Marvell MMP ZSP Host driver.
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */


#include <linux/types.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/sysdev.h>
#include <linux/spinlock.h>
#include <linux/notifier.h>
#include <linux/string.h>
#include <linux/kobject.h>
#include <linux/list.h>
#include <linux/notifier.h>
#include <linux/relay.h>
#include <linux/debugfs.h>
#include <linux/device.h>
#include <linux/miscdevice.h>
#include <linux/delay.h>
#include <linux/mtd/super.h>
#include <linux/version.h>
#include <linux/mmp2_mdma.h>
#include <linux/proc_fs.h>
#include <linux/firmware.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <mach/hardware.h>
#include <mach/regs-zsp.h>
#include <mach/regs-apmu.h>
#include <mach/regs-icu.h>
#include <mach/mmp-pzipc.h>
#include <mach/mmp-zmq.h>
#include <mach/mmp-zsp.h>
#include <mach/cputype.h>
#include <asm/atomic.h>
#include <asm/io.h>

/*mmp PZIPC registers*/
#define IPC_WDR				0x0004
#define IPC_ISRW			0x0008
#define IPC_ICR				0x000C
#define IPC_IIR				0x0010
#define IPC_RDR				0x0014
#define pzipc_readl(pszp, off)		__raw_readl(pzsp->_ipc_regs_base + (off))
#define pzipc_writel(pzsp, off, v)	__raw_writel((v), pzsp->_ipc_regs_base + (off))
/* read IIR*/
#define PZIPC_IIR_READ(pzsp, IIR_val)			\
{					      		\
	/* dummy write to latch the IIR value*/		\
	pzipc_writel(pzsp, IPC_IIR, 0x0);		\
	barrier();					\
	(IIR_val) = pzipc_readl(pzsp, IPC_IIR);		\
}

#define MPMU_CGR_PJ			MPMU_REG(0x1024)
#define MPMU_CGR_SP			MPMU_REG(0x0024)
#define APBC_GPIO_CLK_RST		APBC_REG(0x0038)
#define APBC_AIB_CLK_RST		APBC_REG(0x0064)
#define APMU_AUDIO_CLK_RES_CTRL		APMU_REG(0x010c)


#define SSPA1_VIRT_BASE			(AXI_VIRT_BASE + 0xa0c00)
#define SSPA2_VIRT_BASE			(AXI_VIRT_BASE + 0xa0d00)
#define SSPA_AUD_PLL_CTRL0		(0x38)
#define SSPA_AUD_PLL_CTRL1		(0x3c)


static void zsp_halt(void) {
	/* reset the zsp core */
	writel(0x0, APMU_AUDIO_CLK_RES_CTRL);
	mdelay(50);
}
static void zsp_release_zsp(void)
{
	int value;

	//printk("%s:release zsp...", __func__);
	/* reset the zsp core */
	writel(0x0, APMU_AUDIO_CLK_RES_CTRL);
	mdelay(50);
	/* enalbe zsp clock */
	value = readl(APMU_AUDIO_CLK_RES_CTRL) | 0x600;
	writel(value, APMU_AUDIO_CLK_RES_CTRL);
	mdelay(5);
	value = readl(APMU_AUDIO_CLK_RES_CTRL) | 0x100;
	writel(value, APMU_AUDIO_CLK_RES_CTRL);
	mdelay(5);
	value = readl(APMU_AUDIO_CLK_RES_CTRL) | 0x10;
	writel(value, APMU_AUDIO_CLK_RES_CTRL);
	mdelay(5);
	value = readl(APMU_AUDIO_CLK_RES_CTRL) | 0x02;
	writel(value, APMU_AUDIO_CLK_RES_CTRL);
	mdelay(5);
	value = readl(APMU_AUDIO_CLK_RES_CTRL) | 0x10000;
	writel(value, APMU_AUDIO_CLK_RES_CTRL);
	mdelay(5);

	value = readl(ZSP_CONFIG_REG0) & 0xFFFFBFFF;
	writel(value, ZSP_CONFIG_REG0);
	mdelay(5);
	//value = readl(ZSP_CONFIG_REG0) | 0x1C;
	//writel(value, ZSP_CONFIG_REG0);
	/*PJ4 ADMA mask*/

	if (cpu_is_mmp2_a2()) {
#ifdef CONFIG_ZSP_AUD_PLL_GROUP_CLK_22579200
		/* 11289600 mclk config,  which is more precision for 44.1k pcm */
		__raw_writel(0x108A1881, SSPA1_VIRT_BASE + SSPA_AUD_PLL_CTRL0);
		__raw_writel(0x30801, SSPA1_VIRT_BASE + SSPA_AUD_PLL_CTRL1);
#endif

#ifdef CONFIG_ZSP_AUD_PLL_GROUP_CLK_24576000
		/* 12288000 mclk config,  which is more precision for 48k pcm */
		__raw_writel(0x100DA189, SSPA1_VIRT_BASE + SSPA_AUD_PLL_CTRL0);
		__raw_writel(0x30801, SSPA1_VIRT_BASE + SSPA_AUD_PLL_CTRL1);
#endif

#if defined(CONFIG_ZSP_AUD_PLL_GROUP_CLK_22579200) || defined(CONFIG_ZSP_AUD_PLL_GROUP_CLK_24576000)
		/* the divider for zclk, which is from audio pll.
		 *  ZSP_CONFIG_CLK_CONFIG0[4:2] */
		value = (readl(ZSP_CONFIG_REG0) & 0xffffffe3) | 0x1c;
		writel(value, ZSP_CONFIG_REG0);
		mdelay(5);
#endif

#if defined(CONFIG_ZSP_AUD_PLL_GROUP_CLK_22579200) || defined(CONFIG_ZSP_AUD_PLL_GROUP_CLK_24576000)
		/* zclk clock select. ZSP_CONFIG_CLK_CONFIG0[0].
		 * 1. audio pll. 2. pll1 or pll2 */
		value = readl(ZSP_CONFIG_REG0) | 0x1;
		writel(value, ZSP_CONFIG_REG0);
		mdelay(5);
#endif
	} else {
#ifdef CONFIG_ZSP_AUD_PLL_GROUP_CLK_22579200
		/* 11289600 mclk config,  which is more precision for 44.1k pcm */
		__raw_writel(0x108A1881, SSPA1_VIRT_BASE + SSPA_AUD_PLL_CTRL0);
		__raw_writel(0x10801, SSPA1_VIRT_BASE + SSPA_AUD_PLL_CTRL1);
#endif

#ifdef CONFIG_ZSP_AUD_PLL_GROUP_CLK_24576000
		/* 12288000 mclk config,  which is more precision for 48k pcm */
		__raw_writel(0x100DA189, SSPA1_VIRT_BASE + SSPA_AUD_PLL_CTRL0);
		__raw_writel(0x10801, SSPA1_VIRT_BASE + SSPA_AUD_PLL_CTRL1);
#endif


	
	
	
	}


	writel(0x003c0000, ICU_DMAIRQ_MASK);

	writel(0x60, ZSP_INT_MASK0);

	//printk("done!\n");

}

static void zsp_start_core(void)
{
	int value;
	//printk("%s:start ZSP core...\n", __func__);
	writel(0x0000, ZSP_CONFIG_SVTADDR);
	mdelay(5);
	value = readl(APMU_AUDIO_CLK_RES_CTRL) | 0x40000000;
	writel(value, APMU_AUDIO_CLK_RES_CTRL);

}

typedef struct {
    enum ipc_client_id  logic_id;     /* ACIPC Client Logical Id */
    u32            ipc_irq_bit;  /* The interrupt type bit assigned to the client */
    irq_handler_t  isr_handler;  /* The ISR handler for this client */
} zsp_ipc_cft_t;

typedef struct {
	struct platform_device *	_dev;
	void *				_ipc_regs_base;
	unsigned int			_irq;
	atomic_t			_ccnt;
	struct mutex			_floadlock;
	volatile bool			_ready;
	volatile bool			_zspup;
	zsp_ipc_cft_t			_ipcmap[IPC_MAX_NUM];
} zsp_device_t;
static zsp_device_t gs_zsp = {
	NULL, NULL, -1, {0}, {{1},}, false, false,
	{
		/*client id */   /* interrupt bit*/  /*interrupt handler */
		{IPC_HANDSHAKE_ID,         IPC_INTERRUPT_BIT_0_0,   NULL},
		{IPC_MSG_TRANSFER_ID,      IPC_INTERRUPT_BIT_0_1,   NULL},
		{IPC_DDR_ACCESS_ENABLE_ID, IPC_INTERRUPT_BIT_0_2,   NULL},
		{IPC_DDR_ACCESS_DISABLE_ID,IPC_INTERRUPT_BIT_0_3,   NULL},
		{IPC_PORT_FLOWCONTROL_ID,  IPC_INTERRUPT_BIT_0_4,   NULL},
		{IPC_TEST_ID,              IPC_INTERRUPT_BIT_0_5,   NULL},
		{IPC_IPM_ID,               IPC_INTERRUPT_BIT_2_9,   NULL},
		/* TBD */
	}
};

static irqreturn_t pzipc_interrupt_handler(int irq, void *dev_id)
{
	zsp_device_t * pzsp = (zsp_device_t *)dev_id;
	u32 i, iir_value = 0;

	/* read the IIR*/
	PZIPC_IIR_READ(pzsp, iir_value);
	/* clear status */
	pzipc_writel(pzsp, IPC_ICR, iir_value);

	/* call ISR for detail interrupt  */
	for (i = 0; i < IPC_MAX_NUM; i++) {
		if (		((iir_value & pzsp->_ipcmap[i].ipc_irq_bit) != 0)
			&&	(pzsp->_ipcmap[i].isr_handler != NULL)) {
			pzsp->_ipcmap[i].isr_handler(irq,dev_id);
		}
	}
	return IRQ_HANDLED;
}

enum pzipc_return_code pzipc_set_interrupt(enum ipc_client_id client_id)
{
	zsp_device_t * pzsp = &gs_zsp;
	
	if (client_id >= IPC_MAX_NUM)
		return PZIPC_RC_WRONG_PARAM;

	//printk("pzipc_set_interrupt %d\n", client_id);
	pzipc_writel(pzsp, IPC_ISRW, pzsp->_ipcmap[client_id].ipc_irq_bit);
	return PZIPC_RC_OK;
}
EXPORT_SYMBOL(pzipc_set_interrupt);

enum pzipc_return_code pzipc_add_isr(enum ipc_client_id client_id, irq_handler_t isr_handler)
{
	if (client_id >= IPC_MAX_NUM)
		return PZIPC_RC_WRONG_PARAM;
	gs_zsp._ipcmap[client_id].isr_handler = isr_handler;
	//printk("pzipc_add_isr %d, %p\n", client_id, isr_handler);
	return PZIPC_RC_OK;
}
EXPORT_SYMBOL(pzipc_add_isr);

enum pzipc_return_code pzipc_remove_isr(enum ipc_client_id client_id)
{
	if (client_id >= IPC_MAX_NUM)
		return PZIPC_RC_WRONG_PARAM;
	gs_zsp._ipcmap[client_id].isr_handler = NULL;
	//printk("pzipc_remove_isr %d\n", client_id);
	return PZIPC_RC_OK;
}
EXPORT_SYMBOL(pzipc_remove_isr);


#define _ZALIGN(x, a, t) ((t)(((u32)(x)+((u32)(a)-1))&(~((u32)(a)-1))))
#define ZALIGN256(x, t) _ZALIGN(x,256,t)

static irqreturn_t zsp_ap_handshake_isr(int irq, void *dev_id)
{
	zsp_device_t * pzsp = (zsp_device_t *)dev_id;
	
	//printk("%s:ZSP hand shake signal received\n", __func__);
	pzsp->_zspup = true;
	
	return IRQ_HANDLED;
}
static irqreturn_t zsp_ddr_access_enable_isr(int irq, void *dev_id)
{
	zsp_device_t * pzsp;
	pzsp = (zsp_device_t *)dev_id;
	/**************  add pm constraint here later  ************/

	//printk("%s:ZSP ddr access  requirement received\n", __func__);
	pzipc_set_interrupt(IPC_DDR_ACCESS_ENABLE_ID);
	
	return IRQ_HANDLED;
}
static irqreturn_t zsp_ddr_access_disable_isr(int irq, void *dev_id)
{
	zsp_device_t * pzsp;
	pzsp = (zsp_device_t *)dev_id;
	/**************  remove pm constraint here later  ************/
	
	//printk("%s:ZSP ddr access finish notification\n", __func__);
	pzipc_set_interrupt(IPC_DDR_ACCESS_DISABLE_ID);
	
	return IRQ_HANDLED;
}

static int zsp_loadbin(const char * pname, u32 targetphyaddr, struct platform_device *pdev) {

	int ret = -1;
	const struct firmware *fw;
	char *fw_ptr;
	u32 fw_size, adjusted_size;
	void * pbuffer, * pbuffer_aligned;
	void * pbuffer_rb, * pbuffer_rb_aligned;
	u32 sourcephyaddr, rbaddr;
	dma_addr_t dma_handle, dma_handle_rb;

	ret = request_firmware(&fw, pname, &pdev->dev);
	if (ret){
		printk("%s:request firmware %s fail\n", __func__, pname);
		return ret;
	}
	
	fw_ptr		=(char *)fw->data;
	fw_size		= (u32)fw->size;
	adjusted_size	= ZALIGN256(fw_size,u32) + 256;
	dma_handle	= 0;
	dma_handle_rb	= 0;

	pbuffer		= dmam_alloc_coherent(&pdev->dev, adjusted_size, &dma_handle, GFP_KERNEL);
	pbuffer_rb	= dmam_alloc_coherent(&pdev->dev, adjusted_size, &dma_handle_rb, GFP_KERNEL);
	if ((pbuffer == NULL) || pbuffer_rb == NULL) {
		printk("%s:allocate internal buffer fail\n", __func__);
		goto cleanup;
	}

	pbuffer_aligned		= ZALIGN256(pbuffer,void*);
	pbuffer_rb_aligned	= ZALIGN256(pbuffer_rb,void*);
	sourcephyaddr		= ZALIGN256(dma_handle, u32);
	rbaddr			= ZALIGN256(dma_handle_rb, u32);

	memcpy(pbuffer_aligned, fw_ptr, fw_size);

	//printk("%s:load %s, from va:%p(pa:0x%08x/dma:0x%08x) to pa:0x%08x, size:0x%x/0x%x\n",
	//	__func__, pname, pbuffer, sourcephyaddr, dma_handle, targetphyaddr,
	//	fw_size, adjusted_size);

	ret = mdma_pmemcpy(targetphyaddr, sourcephyaddr, adjusted_size);
	ret = mdma_pmemcpy(rbaddr, targetphyaddr, adjusted_size);

	if (memcmp(fw_ptr, pbuffer_rb_aligned, fw_size) != 0) {
		printk("%s: memory content doesnot match\n", __func__);
		ret = -1;
		goto cleanup;
	}

	ret = 0;

cleanup:

	if (pbuffer != NULL) dmam_free_coherent(&pdev->dev, adjusted_size, pbuffer, dma_handle);
	if (pbuffer_rb != NULL) dmam_free_coherent(&pdev->dev, adjusted_size, pbuffer_rb, dma_handle_rb);

	release_firmware(fw);
	return ret;
}

static int zsp_doreset(struct platform_device *pdev, int timeout_ms) {

	zsp_device_t * pzsp;
	int loop;
	int intervalms;
	int ret = -EINVAL;

	zsp_release_zsp();

	ret = zsp_loadbin("zsp_ints.bin", ZSP_ITCM_START, pdev);
	if (ret != 0) {
		printk("%s: load ints.bin failed\n", __func__);
		goto cleanup;
	}
	ret = zsp_loadbin("zsp_text.bin", ZSP_ITCM_START+0x400, pdev);
	if (ret != 0) {
		printk("%s: load text.bin failed\n", __func__);
		goto cleanup;
	}
	ret = zsp_loadbin("zsp_data.bin", ZSP_DTCM_START, pdev);
	if (ret != 0) {
		printk("%s: load data.bin failed\n", __func__);
		goto cleanup;
	}

	ret = pzipc_add_isr(IPC_HANDSHAKE_ID, zsp_ap_handshake_isr);
	ret = pzipc_add_isr(IPC_DDR_ACCESS_ENABLE_ID, zsp_ddr_access_enable_isr);
	ret = pzipc_add_isr(IPC_DDR_ACCESS_DISABLE_ID, zsp_ddr_access_disable_isr);
	if (PZIPC_RC_OK != ret) {
		printk("%s: cannot send hand shake signal to ZSP\n", __func__);
		return -EINVAL;
	}

	pzsp = (zsp_device_t *)platform_get_drvdata(pdev);

	pzsp->_zspup = false;

	//printk("%s: firmware downloaded, waiting for ZSP response\n", __func__);
	zsp_start_core();

	intervalms = 50;
	loop = timeout_ms/intervalms + 1;

	while (loop-- > 0) {
		mdelay(intervalms);
		if(pzsp->_zspup) {
			pzipc_set_interrupt(IPC_HANDSHAKE_ID);
			//printk("%s: ZSP initialize successfully\n", __func__);
			ret = 0;

			goto cleanup;
		}
	}
	
	printk("%s: ZSP initialize timeout\n", __func__);
	ret = -EINVAL;

cleanup:
	pzipc_remove_isr(IPC_HANDSHAKE_ID);
	return ret;
}

#define ZSP_HALT_WHEN_NO_REFERENCE
static void zsp_start(int timeout_ms, int retry) {
	if (gs_zsp._dev != NULL) {
		if (atomic_add_return(1, &gs_zsp._ccnt) == 1) {
#ifdef ZSP_HALT_WHEN_NO_REFERENCE
			while (retry -- > 0) {
				if (zsp_doreset(gs_zsp._dev, timeout_ms) == 0) {
					printk("%s: ZSP firmware loaded\n", __func__);
					break;
				}
			}
#else
			static bool s_zsp_loaded = false;
			if (!s_zsp_loaded) {
				while (retry -- > 0) {
					if (zsp_doreset(gs_zsp._dev, timeout_ms) == 0) {
						printk("%s: ZSP firmware loaded\n", __func__);
						s_zsp_loaded = true;
						break;
					}
				}
			}
#endif
		}
	}
}
static void zsp_stop(void) {
	if (gs_zsp._dev != NULL) {
		if (atomic_sub_return(1, &gs_zsp._ccnt) == 0) {
#ifdef ZSP_HALT_WHEN_NO_REFERENCE
			printk("%s: ZSP halt\n", __func__);
			zsp_halt();
#endif
		}
	}
}

void zsp_local_start(atomic_t *pcounter, int timeout_ms, int retry) {
	mutex_lock(&gs_zsp._floadlock);
	if (atomic_add_return(1, pcounter) == 1) {
		zsp_start(timeout_ms,retry);
	}
	mutex_unlock(&gs_zsp._floadlock);
}
EXPORT_SYMBOL(zsp_local_start);

void zsp_local_stop(atomic_t *pcounter) {
	mutex_lock(&gs_zsp._floadlock);
	if (atomic_sub_return(1, pcounter) == 0) {
		zsp_stop();
	}
	mutex_unlock(&gs_zsp._floadlock);
}
EXPORT_SYMBOL(zsp_local_stop);

static int __init zsp_probe(struct platform_device *pdev)
{
	zsp_device_t *		pzsp;
	struct resource *	resource;
	int			ret;
	int			ret_hook;
	
	pzsp		= &gs_zsp;
	ret		= -EBUSY;
	ret_hook	= -EBUSY;

	if ( pzsp->_ready ) {
		printk(KERN_WARNING "ZSP already running\n");
		return -EBUSY;
	}

	pzsp->_ready	= false;

	mutex_init(&gs_zsp._floadlock);

	resource	= platform_get_resource(pdev, IORESOURCE_MEM, 0);

	if (resource == NULL) {
		printk(KERN_WARNING "resource for %s does not exist\n", pdev->name);
		ret = -ENXIO;
		goto cleanup;
	}
	
	if ((pzsp->_ipc_regs_base = ioremap_nocache(resource->start, resource->end - resource->start + 1)) == NULL) {
		printk(KERN_WARNING "failed to map IO resource\n");
		ret = -EBUSY;
		goto cleanup;
	}

	if ((pzsp->_irq = platform_get_irq(pdev, 0)) < 0 ) {
		printk(KERN_WARNING "failed to get irq\n");
		ret = -EBUSY;
		goto cleanup;
	}

	ret_hook = request_irq(pzsp->_irq, pzipc_interrupt_handler, IRQF_DISABLED, "pzipc", pzsp);
	if (ret_hook < 0) {
		printk(KERN_WARNING "failed to request irq\n");
		ret = -EBUSY;
		goto cleanup;
	}

	platform_set_drvdata(pdev, pzsp);

	pdev->dev.coherent_dma_mask = 0xffffffff;
	pzsp->_ready	= true;
	pzsp->_dev	= pdev;
	pzsp->_ccnt.counter = 0;

	zsp_halt(); // now we do late firmware load

	printk("ZSP driver started\n");
        return 0;

cleanup:
	if (ret_hook >= 0) {
		free_irq(pzsp->_irq, pzsp);
	}

	if (pzsp->_ipc_regs_base != NULL) {
		iounmap(pzsp->_ipc_regs_base);
	}

	pzsp->_irq = -1;
	pzsp->_ipc_regs_base = NULL;
	pzsp->_dev	= NULL;
	pzsp->_ready	= false;
	pzsp->_zspup	= false;

	return ret;
}

static int __devexit zsp_remove(struct platform_device *pdev)
{
	zsp_device_t *pzsp = (zsp_device_t *)platform_get_drvdata(pdev);

	if (pzsp->_ready) {
		disable_irq(pzsp->_irq);
		zsp_halt();
		free_irq(pzsp->_irq, pzsp);
		iounmap(pzsp->_ipc_regs_base);

		pzsp->_irq = -1;
		pzsp->_ipc_regs_base = NULL;
		pzsp->_dev	= NULL;
		pzsp->_ready	= false;
		pzsp->_zspup	= false;
		pzsp->_ccnt.counter = 0;
		mutex_destroy(&gs_zsp._floadlock);
		printk("ZSP driver stopped\n");
	}

	return 0;
}

static struct platform_driver gs_zsp_driver = {
	.driver		= {
		.name	= "mmp-zsp",
	},
	.probe		= zsp_probe,
	.remove		= __devexit_p(zsp_remove)
};


static int __init zsp_init(void)
{
	return platform_driver_register(&gs_zsp_driver);
}

static void __exit zsp_exit(void)
{
	platform_driver_unregister(&gs_zsp_driver);
}


module_init(zsp_init);
module_exit(zsp_exit);

MODULE_AUTHOR("Marvell Technology");
MODULE_DESCRIPTION("MMP ZSP Firmware loader");
MODULE_LICENSE("GPL");

