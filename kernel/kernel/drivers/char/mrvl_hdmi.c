/*
 * Marvell HDMI driver
 * Jiangang Jing
 * Copyright (c) 2009, Marvell International Ltd (jgjing@marvell.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
*/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/types.h>
#include <linux/miscdevice.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/spinlock.h>
#include <linux/interrupt.h>
#include <linux/kthread.h>
#include <linux/jiffies.h>

#include <asm-generic/errno.h>
#include <asm/uaccess.h>
#include <asm/irq.h>

#include <mach/pxa168fb.h>
#include <mach/mrvl_hdmi_regs.h> /* marvell hdmi register definition */
#include <mach/mrvl_hdmitx.h> /* marvell hdmi trasmitter head file */
#include <mach/regs-sspa.h>
#include <mach/hdmi_edid.h>
#define HDMI_PLL_FREQ_25MHz	25
#define HDMI_PLL_FREQ_27MHz	27
#define HDMI_PLL_FREQ_54MHz	54
#define HDMI_PLL_FREQ_74MHz	74
#define HDMI_PLL_FREQ_108MHz	108
#define HDMI_PLL_FREQ_148MHz	148

#define HDMI_DATA   (0x00)
#define HDMI_ADDR   (0x04)
#define PMUA_BASE       0xD4282800
#define PMUA_DISPLAY1_CLK_RES_CTRL	(0x004C)
#define PMUA_DISPLAY1_CLK_RES_CTRL_HDMI_I2SCLK_EN	1 << 14
#define PMUA_DISPLAY1_CLK_RES_CTRL_HDMI_REFCLK_EN	1 << 13
#define PMUA_AUDIO_CLK_RES_CTRL	(0x010C)
#define PMUA_AUDIO_CLK_RES_CTRL_AUDIO_ISB	1 << 8


#define	I2S_PORT_0	1<<HDTX_AUD_CTRL_I2S_CH_EN_BASE
#define	I2S_PORT_1	2<<HDTX_AUD_CTRL_I2S_CH_EN_BASE
#define	I2S_PORT_2	4<<HDTX_AUD_CTRL_I2S_CH_EN_BASE
#define	I2S_PORT_3	8<<HDTX_AUD_CTRL_I2S_CH_EN_BASE

#define	DVI_MODE	0
#define	HDMI_MODE	HDTX_HDMI_CTRL_HDMI_MODE
#define	SELECT_HDMI_CLOCK	3<<LCD_TCLK_DIV_TCLK_SOURCE_SELECT_BASE
#define	HDMI_TCLK_BYPASS	1<<LCD_TCLK_DIV_CFG_TCLKNX_DIV_BASE

#define	HDMI_CONNECTED		1
#define	HDMI_TIMER_TIMEOUT	(2 * HZ)

#ifdef HDMI_DEBUG
#undef HDMI_DEBUG
#define hdmi_dbg(fmt, args...) \
        printk(KERN_INFO "%s:%d> " fmt, __func__, __LINE__ , ## arg)
#else
#define hdmi_dbg(fmt, args...) \
        do { } while (0)
#endif /* HDMI_DEBUG */

#define MRVL_HDMI_NAME "marvell_hdmitx"
struct hdmi_device {
	struct device	*dev;
	struct clk	*clk;

	void	*hdmi_regs_base;
	void	*pmua_regs_base;
	void	*sspa1_regs_base;
	void	*lcd_regs_base;
	unsigned long           total_gmem_size;
	struct hdmi_audio_fmt audio_fmt;
	struct hdmi_video_fmt video_fmt;
	unsigned int            irq;
	int	state;
	int	state_old;
	struct	timer_list timer;
};

static int g_res_support[13] = {0}; 
static struct hdmi_device g_hdmi_dev;
struct hdmi_edid g_hdmi_edid;

#ifdef CONFIG_DVFM
#include <mach/dvfm.h>
/* AXI bus speed too low would cause TV screen flick @ high resulotions */
static int dvfm_dev_idx;
static int low_ops_disabled = 0;
static void set_dvfm_constraint(void)
{
	if (low_ops_disabled == 0) {
		pr_debug("%s\n", __func__);
		dvfm_disable_op_name("Ultra Low MIPS", dvfm_dev_idx);
		low_ops_disabled = 1;
	}
}
static void unset_dvfm_constraint(void)
{

	if (low_ops_disabled == 1) {
		pr_debug("%s\n", __func__);
		dvfm_enable_op_name("Ultra Low MIPS", dvfm_dev_idx);
		low_ops_disabled = 0;
	}
}
#else
static void set_dvfm_constraint(void) {}
static void unset_dvfm_constraint(void) {}
#endif

static int mrvl_hdmi_fops_open(struct inode * inode, struct file * file)
{
	file->private_data = &g_hdmi_dev;
	return 0 ;
}

void hdmi_regWrite(struct hdmi_device *hi, unsigned int addr,
			unsigned int data)
{
       writel((data & 0xff), hi->hdmi_regs_base + HDMI_DATA);
       writel((addr | 1 << 31), hi->hdmi_regs_base + HDMI_ADDR);
}


unsigned int hdmi_regRead(struct hdmi_device *hi,unsigned int addr)
{
	writel((addr | 1 << 30), hi->hdmi_regs_base + HDMI_ADDR);
	return (readl(hi->hdmi_regs_base + HDMI_DATA) & 0xff);
}

static ssize_t mrvl_hdmi_fops_read(struct file *filp, char __user *buff,
				size_t count, loff_t *ppos)
{
        return 0 ;
}

int hdmi_pll_cfg(struct hdmi_device *hi, unsigned int freq)
{
	/* Fix the pll2refdiv to 1(+2), to get 8.66MHz ref clk
	 * Stable val recomended between 8-12MHz. To get the reqd
	 * freq val, just program the fbdiv
	 * freq takes effect during a fc req
	 */ 
	volatile unsigned int temp;
	unsigned int postdiv;
	unsigned int freq_offset_inner;
	unsigned int hdmi_pll_fbdiv;
	unsigned int hdmi_pll_refdiv;
	unsigned int kvco;
	
	unsigned int PLL_REGPUMP=1;
	unsigned int VDDR15_VCO=2;
	unsigned int VDDR12_INTP=3;
	unsigned int VDDR_RING=1;
	unsigned int VDDR10_PLL=1;
	unsigned int ICP=0xf;
	unsigned int EN_TST_CLK=0;
	unsigned int DCC_CTRL=0;
	unsigned int CLK_DET_EN=1;
	unsigned int INTPI=1;
	unsigned int DCCI=1;
	unsigned int IPM = 2;
	int count = 0x10000;

	switch ( freq ){
		case HDMI_PLL_FREQ_25MHz:
			postdiv=0x4;
			freq_offset_inner=0x4ec5;
			hdmi_pll_fbdiv   =38;
			hdmi_pll_refdiv  =16;
			kvco = 0xc;				
			break;

		case HDMI_PLL_FREQ_27MHz:
			postdiv=0x4;
			freq_offset_inner=0x35cb;
			hdmi_pll_fbdiv   =41;
			hdmi_pll_refdiv  =16;
			kvco = 0xb;		
			break;

		case HDMI_PLL_FREQ_54MHz:
			postdiv=0x2;
			freq_offset_inner=0x35cb;
			hdmi_pll_fbdiv   =41;
			hdmi_pll_refdiv  =16;
			kvco = 0xb;		
			break;

		case HDMI_PLL_FREQ_74MHz:
			postdiv=0x2;
			freq_offset_inner=0x84B;
			hdmi_pll_fbdiv   =57;
			hdmi_pll_refdiv  =16;
			kvco = 0x3;		
			break;

		case HDMI_PLL_FREQ_108MHz:
			postdiv=0x1;
			freq_offset_inner=0x35cb;
			hdmi_pll_fbdiv   =41;
			hdmi_pll_refdiv  =16;
			kvco = 0xb;		
			break;

		case HDMI_PLL_FREQ_148MHz:
		default:
			postdiv=0x1;
			freq_offset_inner=0x84B;
			hdmi_pll_fbdiv   =57;
			hdmi_pll_refdiv  =16;
			kvco = 0x3;		
			break;
	}
	
	        
	temp = (DCCI  << HDMI_PLL_CFG_0_DCCI_BASE)	|
			(INTPI << HDMI_PLL_CFG_0_INTPI_BASE)	|
			(CLK_DET_EN  << 26)	|
			(DCC_CTRL  << 25)	|
			(kvco << HDMI_PLL_CFG_0_KVCO_BASE)	|
			(EN_TST_CLK  << 19)	|
			(postdiv << HDMI_PLL_CFG_0_POSTDIV_BASE)	|
			(ICP  << HDMI_PLL_CFG_0_ICP_BASE)	|
			(VDDR10_PLL << HDMI_PLL_CFG_0_VDDR10_PLL_BASE)	|
			(VDDR_RING << HDMI_PLL_CFG_0_VDDR_RING_BASE)	|
			(VDDR12_INTP << HDMI_PLL_CFG_0_VDDR12_INTP_BASE)|
			(VDDR15_VCO<<HDMI_PLL_CFG_0_VDDR15_VCO_BASE)	|
			(PLL_REGPUMP);
	writel(temp, hi->hdmi_regs_base + HDMI_PLL_CFG_0);
	
	temp =((HDMI_PLL_CFG_1_EN_PANNEL)|
	        (HDMI_PLL_CFG_1_EN_HDMI)|
	        (HDMI_PLL_CFG_1_FREQ_OFFSET_READY_ADJ)|
	        (freq_offset_inner << HDMI_PLL_CFG_1_FREQ_OFFSET_INNER_BASE)|
	        (0x1  << HDMI_PLL_CFG_1_MODE_BASE )|
	        (HDMI_PLL_CFG_1_EN_FINE_DCC)|
	        (HDMI_PLL_CFG_1_EN_DCC)); 
	writel(temp, hi->hdmi_regs_base + HDMI_PLL_CFG_1);

	/* pll_cfg_2 config */
	temp = 	IPM << HDMI_PLL_CFG_2_IPM_BASE; 
	writel(temp, hi->hdmi_regs_base + HDMI_PLL_CFG_2);
	
	/* Power Down the pll and set the divider ratios*/
	temp = (hdmi_pll_fbdiv << HDMI_PLL_CFG_3_HDMI_PLL_FBDIV_BASE)|
	       (hdmi_pll_refdiv << HDMI_PLL_CFG_3_HDMI_PLL_REFDIV_BASE);
	writel(temp, hi->hdmi_regs_base + HDMI_PLL_CFG_3);

	/* power up the pll */
	temp |= HDMI_PLL_CFG_3_HDMI_PLL_ON;	
	writel(temp, hi->hdmi_regs_base + HDMI_PLL_CFG_3);

	/*wait 10us : just an estimate*/
	udelay(100);

	/* release the pll out of reset*/
	temp |= HDMI_PLL_CFG_3_HDMI_PLL_RSTB; 
	writel(temp, hi->hdmi_regs_base + HDMI_PLL_CFG_3);
	
	/* PLL_LOCK should be 1, */
	temp = readl(hi->hdmi_regs_base +HDMI_PLL_CFG_3);
	temp &= 0x400000;
	while ( !temp )	{
		temp = readl(hi->hdmi_regs_base +HDMI_PLL_CFG_3);
		temp &= 0x400000;
		udelay(10);
		count--;
		if (count <= 0) {
			printk("%s PLL lock timeout error, HDMI_PLL_CFG_3 %x\n",
				__func__, temp);
			return -EIO;
		}
	}
	 
	mdelay(1);
	
	/* MODE=3;*/
	temp = readl(hi->hdmi_regs_base + HDMI_PLL_CFG_1) |
			(0x3<<HDMI_PLL_CFG_1_MODE_BASE);
	writel(temp, hi->hdmi_regs_base + HDMI_PLL_CFG_1);

	/*finish HDMI_PLL setup */
	return 0;
}

u8 check_sum(u8 *pDataBuf, u8 length)
{
	u8 chkSum = 0;
	u8 count;
	u8 ret;
	if (pDataBuf == NULL || length == 0)
		return 0;

	for (count = 0; count < length; count++)
		chkSum += pDataBuf[count];
	ret = 0x100-chkSum;
	return ret;
}

void hdmi_send_packet(struct hdmi_device *hi, u32 pkt_idx,
			u8 *pDataBuf, u8 length)
{
	u32 pkt_byte_base, i;

	pkt_byte_base = HDTX_PKT0_BYTE0_30 + 0x20*pkt_idx;

	for (i = 0; i < length; i++)
		hdmi_regWrite(hi, pkt_byte_base+i,  pDataBuf[i]);

	hdmi_regWrite(hi, pkt_byte_base+31, 1);

	/* transmit pkt_idx*/
	hdmi_regWrite(hi, HDTX_HOST_PKT_CTRL1,
		(0x1<<pkt_idx) | hdmi_regRead(hi, HDTX_HOST_PKT_CTRL1));
	hdmi_regWrite(hi, HDTX_HOST_PKT_CTRL0,
		(0x1<<pkt_idx) | hdmi_regRead(hi, HDTX_HOST_PKT_CTRL0));
}

void hdmi_video_cfg(struct hdmi_device *hi, char panel_index, u32 hd_en)
{
	int mclk_div;

	if (hd_en && (panel_index == HDMI_1080P))
		set_dvfm_constraint();
	else
		unset_dvfm_constraint();

 	switch ( panel_index )
 	{
	case HDMI_640x480P:
		hdmi_pll_cfg(hi, HDMI_PLL_FREQ_25MHz);
		break;

	case HDMI_1440x240P:
	case HDMI_2880x240P:
	case HDMI_1440x480P:
	case HDMI_2880x288P_50Hz:
	case HDMI_1440x576P_50Hz:
		hdmi_pll_cfg(hi, HDMI_PLL_FREQ_54MHz);
		break;

	case HDMI_720P:
		hdmi_pll_cfg(hi, HDMI_PLL_FREQ_74MHz);
		break;

	case HDMI_2880x480P:
	case HDMI_2880x576P_50Hz:
		hdmi_pll_cfg(hi, HDMI_PLL_FREQ_108MHz);
		break;

	case HDMI_480P:
	case HDMI_720x576P_50Hz:
	case HDMI_1440x288P_50Hz:
		hdmi_pll_cfg(hi, HDMI_PLL_FREQ_27MHz);
		break;

	case HDMI_1080P:
		hdmi_pll_cfg(hi, HDMI_PLL_FREQ_148MHz);
		break;

	default:
		printk("not an HDMI mode, configuration aborted!\n  ");
		return;	
 	}

	mclk_div = 6;
	printk("mclk_div : 0x%x\n", mclk_div);
	printk("hd_en : %d\n", hd_en);
	writel(1|1<<2|hd_en<<4|mclk_div<<5|5<<9|5<<13,
		hi->hdmi_regs_base + HDMI_CLOCK_CFG);
	writel(0, hi->hdmi_regs_base + HDMI_PHY_CFG_2);
	writel(1<<27, hi->hdmi_regs_base + HDMI_PHY_CFG_2);
	writel(1<<4, hi->hdmi_regs_base + HDMI_PHY_CFG_2);
	/*packet header for AVI Packet in pkt1*/
	{
		u8 buf[14];
		buf[0] = 0x82;
		buf[1] = 0x2;
		buf[2] = 0xd;
		buf[3] = 0x0;	/* checksum : 0xb3*/
		buf[4] = 0x10;	/* RGB, Active Format, no bar, no scan*/
		buf[5] = 0xa8;	/* ITU-R 709, 16:9*/
		buf[6] = 0x0;	/* No IT content, xvYCC601, no scaling*/
		switch (panel_index) {
		case 0:
			buf[7] = 0x3;
			break;
		case 1:
			buf[7] = 0x4;
			break;
		case 2:
			buf[7] = 0x10;
			break;
		case 3:
			buf[7] = 0x1;
			break;
		case 4:
			buf[7] = 0x9;
			break;
		case 5:
			buf[7] = 0x12;
			break;
		case 6:
			buf[7] = 0x18;
			break;
		case 7:
			buf[7] = 0xd;
			break;
		case 8:
			buf[7] = 0xf;
			break;
		case 9:
			buf[7] = 0x1c;
			break;
		case 10:
			buf[7] = 0x16;
			break;
		case 11:
			buf[7] = 0x24;
			break;
		case 12:
			buf[7] = 0x26;
			break;
		}
		buf[8] = 0x0;	/* limited YCC range, graphics, no repeat*/
		buf[9] = 0x0;
		buf[10] = 0x0;
		buf[11] = 0x0;
		buf[12] = 0x0;
		buf[13] = 0x0;

		buf[3] = check_sum(buf, sizeof(buf));
		hdmi_send_packet(hi, 1, buf, sizeof(buf));
	}
	hdmi_regWrite(hi, HDTX_TDATA3_0, 0xe0);
	hdmi_regWrite(hi, HDTX_TDATA3_1, 0x83);
	hdmi_regWrite(hi, HDTX_TDATA3_2, 0xf);
	
	writel(0xb6d, hi->hdmi_regs_base +HDMI_PHY_CFG_0);
	/* auto detection enabled  Internal Logic Video Format Detect bit 6*/
  	/* HDMI_CTRL, enable HDMI mode*/
  	hdmi_regWrite(hi, HDTX_HDMI_CTRL, HDMI_MODE);
   
	hdmi_regWrite(hi, HDTX_DC_FIFO_WR_PTR,  0x1);
	hdmi_regWrite(hi, HDTX_DC_FIFO_RD_PTR,  0x1A); 

	hdmi_regWrite(hi, HDTX_DC_FIFO_SFT_RST, 0x1);
	hdmi_regWrite(hi, HDTX_DC_FIFO_SFT_RST, 0x0);

	hdmi_regWrite(hi, HDTX_PHY_FIFO_PTRS, 0x08);

	hdmi_regWrite(hi, HDTX_PHY_FIFO_SOFT_RST, 0x1);
	hdmi_regWrite(hi, HDTX_PHY_FIFO_SOFT_RST, 0x0);

	hdmi_regWrite(hi, HDTX_VIDEO_CTRL, 0x0 );
  	hdmi_regWrite(hi, HDTX_VIDEO_CTRL, HDTX_VIDEO_CTRL_INT_FRM_SEL );

}

void hdmi_audio_cfg(u32 sr, u32 i2s, u32 wsp, u32 fsp, u32 clkp,
			u32 xdatdly, u32 xwdlen1,
			u32 xfrlen1)
{
	u32	aud_cfg, reg_val;
	u32	N;
	struct hdmi_device *hi = &g_hdmi_dev;
	int mclk_div;

	reg_val = readl(hi->hdmi_regs_base + HDMI_CLOCK_CFG);
	if ((reg_val & (1<<4)) == 0) {
		hdmi_dbg("HDMI: Please enable video before playing audio\n");
		return;
	}

	aud_cfg = ( i2s  << 11)	|
			( wsp  << 10)	|
			( fsp  << 9)|
			( clkp << 8)|
			( xdatdly << 6 )|
			( xwdlen1 <<3 )|
			( xfrlen1 );
				 
	writel(aud_cfg, hi->hdmi_regs_base +HDMI_AUDIO_CFG);

	hdmi_regWrite(hi, HDTX_I2S_DBG_LFT0,  0xcc);
	hdmi_regWrite(hi, HDTX_I2S_DBG_LFT1,  0xcc);
	hdmi_regWrite(hi, HDTX_I2S_DBG_LFT2,  0xcc);
	hdmi_regWrite(hi, HDTX_I2S_DBG_LFT3,  0xcc);

	hdmi_regWrite(hi, HDTX_I2S_DBG_RIT0,  0xcc);
	hdmi_regWrite(hi, HDTX_I2S_DBG_RIT1,  0xcc);
	hdmi_regWrite(hi, HDTX_I2S_DBG_RIT2,  0xcc);
	hdmi_regWrite(hi, HDTX_I2S_DBG_RIT3,  0xcc);

	hdmi_regWrite(hi, HDTX_I2S_DLEN,  0x10);
	hdmi_regWrite(hi, HDTX_CHSTS_4, 2 );
	
	/* set HDMI ACR control; fs  = 48000
	 *  N = 128 * fs / 1000;
	 */
	N = 6144;
 	hdmi_regWrite(hi, HDTX_ACR_N0, N&0xff );
 	hdmi_regWrite(hi, HDTX_ACR_N1, (N>>8)&0xff );
 	hdmi_regWrite(hi, HDTX_ACR_N2, (N>>16)&0xff );
	
  	hdmi_regWrite(hi, HDTX_ACR_CTRL, 0x1 );

 	/* Audio Enable I2S port 0 = 3*/
  	hdmi_regWrite(hi, HDTX_AUD_CTRL, HDTX_AUD_CTRL_VALIDITY | 
			HDTX_AUD_CTRL_AUD_EN | I2S_PORT_0 );
	mclk_div = readl(hi->sspa1_regs_base + SSPA_AUD_CTRL);
	mclk_div >>=9;
	mclk_div &= 0xf;	/* make sure it will not impact on tclk */
	mclk_div /=2;
	if (((reg_val & (0xf<<5))>>5) != mclk_div) {
		reg_val &= ~(0xf<<5);
		reg_val |= (mclk_div<<5);
		writel(reg_val, hi->hdmi_regs_base + HDMI_CLOCK_CFG);
	}

	/* packet header for audio InfoFrame Packet*/
	{
		u8 buf[14];
	
		buf[0] = 0x84;
		buf[1] = 0x1;
		buf[2] = 0xa;

		buf[3] = 0x0;	/* checksum : 0x57*/

		buf[4] = 0x11;	/* PCM, 2 channels*/
		buf[5] = 0x9;	/*44.1K, 16 bit*/
		buf[4] = 0x0;
		buf[5] = 0x0;
	
		buf[6] = 0x0;
		buf[7] = 0x0;	/* speaker alloc*/
		buf[8] = 0x0;
		buf[9] = 0x0;
		buf[10] = 0x0;
		buf[11] = 0x0;
		buf[12] = 0x0;
		buf[13] = 0x0;
		buf[3] = check_sum(buf, sizeof(buf));
		hdmi_send_packet(hi,1,buf, sizeof(buf));
	}
}
EXPORT_SYMBOL(hdmi_audio_cfg);

static int mrvl_hdmi_fops_ioctl(struct inode *inode, struct file *file,
                     unsigned int cmd, unsigned long arg)
{	
	
	struct hdmi_device *hi;
	hi = file->private_data;
        switch (cmd)
        {
                case HDMIIO_VIDEO_FMT:   
		{
			void __user *argp = (void *)arg;
			if (copy_from_user(&hi->video_fmt, argp, sizeof(struct hdmi_video_fmt)))
				return -EFAULT;
			hdmi_video_cfg(hi,hi->video_fmt.panel_index, hi->video_fmt.hd_en);
                        break ;
		}
                case HDMIIO_AUDIO_FMT:
                { 
			void __user *argp = (void *)arg;
			if (copy_from_user(&hi->audio_fmt, argp, sizeof(struct hdmi_audio_fmt)))
				return -EFAULT;
                        hdmi_audio_cfg(hi->audio_fmt.sr,hi->audio_fmt.i2s,hi->audio_fmt.wsp,hi->audio_fmt.fsp,
				hi->audio_fmt.clkp,hi->audio_fmt.xdatdly,hi->audio_fmt.xwdlen1,hi->audio_fmt.xfrlen1);
			break ;
		}
                default:
                        return -EINVAL;
        }

        return 0 ;
}

#ifdef NOT_YET 
static enum edid_returns check_crc(unsigned char *data_buf)
{
    unsigned char       count;
    unsigned char       check_sum;
    enum edid_returns        ret;

    check_sum = 0;
    for (count = 0; count < SIZE_OF_EDID_BLK; count++)
    {
        // One byte checksum
        check_sum += data_buf[count];
    }

    ret = (check_sum == 0)?EDID_ERR_OK:EDID_ERR_CRC_FAILED;
    return ret;
}
#endif

static bool is_videoID_defined (u16 videoID)
{
    u16 max_videoID;
    max_videoID = sizeof(video_code_map)/sizeof(struct cea_res_info);

    // Check given VideoID
    if (videoID == 0 || videoID >= max_videoID)
    {
        return false;
    }

    return true;
}
// Short Video Descriptor
static enum edid_returns edid_parseSVD(unsigned char *data_buf,unsigned char svd_len)
{
    unsigned char   dataOfs = 0;
    unsigned char   videoID;

    // Skip the header
    dataOfs++;
    while (dataOfs <= svd_len)
    {
        // Parse SVD
        videoID = (data_buf[dataOfs] & 0x7F);
        dataOfs++;

        if (!is_videoID_defined(videoID))
        {
            // Don't add it
            continue;
        }
	switch (videoID){
	case 1:
		g_res_support[3] = 1;
		break;
	case 2:
	case 3:
		g_res_support[0] = 1;
		break;
	case 4:
		g_res_support[1] = 1;
		break;
	case 8:
	case 9:
		g_res_support[4] = 1;
		break;
	case 12:
	case 13:
		g_res_support[7] = 1;
		break;
	case 14:
	case 15:
		g_res_support[8] = 1;
		break;
	case 16:
		g_res_support[2] = 1;
		break;
	case 17:
	case 18:
		g_res_support[5] = 1;
		break;
	case 21:
	case 22:
		g_res_support[10] = 1;
		break;
	case 23:
	case 24:
		g_res_support[6] = 1;
		break;
	case 27:
	case 28:
		g_res_support[9] = 1;
		break;
	case 35:
	case 36:
		g_res_support[11] = 1;
		break;
	case 37:
	case 38:
		g_res_support[12] = 1;
		break;
	default:
		break; // unsupport currently by hdmi controler
	}
    }
    return EDID_ERR_OK;
}

static int hdmi_get_edid_info(void)
{
	u8           data_buf[256];
	int             ret;
	unsigned char sv_len;

	ret = i2c_master_recv(g_hdmi_edid.client, data_buf, 256);
	sv_len = data_buf[0x84]&0x1f;
	edid_parseSVD(&data_buf[0x84],sv_len);

	if (ret != 256){
		printk("read error\n");
		return -EIO;
	}

	return (EDID_ERR_OK);
}

static ssize_t hdmi_show_res(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	int i, ret = 1;
	for (i = 0; i < 13; i++) {
		ret += sprintf(&buf[ret], "%u ", g_res_support[i]);
	}
	printk("\n");
	return ret;
}

static DEVICE_ATTR(ResSupport, 0644, hdmi_show_res, NULL);
static ssize_t hdmi_show_current_res(struct device *dev,
				   struct device_attribute *attr, char *buf)
{
	u32 reg_val, reg_val2;
	int ret = 1;
	/* get LCD_TV_V_H_ACTIVE */
	reg_val = readl(g_hdmi_dev.lcd_regs_base + 0x58);
	reg_val2 = readl(g_hdmi_dev.hdmi_regs_base + HDMI_CLOCK_CFG);
	if (reg_val2&(1<<4))
		ret = sprintf(&buf[ret], "%uX%u\n", reg_val&0xfff,
				(reg_val>>16)&0xfff);
	else
		ret = sprintf(&buf[ret], "0 \n");
	return ret;
}

static DEVICE_ATTR(currentRes, 0644, hdmi_show_current_res, NULL);

/* distinguish disconnect event & HDCP repeater */
static void hdmi_conn_change(int state)
{
	pr_debug("----- %s low_ops_disabled %d state %s\n", __func__,
		low_ops_disabled, state ? "connected":"disconnected");

	if (!low_ops_disabled)
		return;

	if (state == HDMI_CONNECTED)
		dvfm_disable_op_name("Ultra Low MIPS", dvfm_dev_idx);
	else
		dvfm_enable_op_name("Ultra Low MIPS", dvfm_dev_idx);

}

static void hdmi_timer_work(unsigned long data)
{
	if (g_hdmi_dev.state != HDMI_CONNECTED) {
		g_hdmi_dev.state_old = g_hdmi_dev.state;
		hdmi_conn_change(!HDMI_CONNECTED);
	}
}

static void hdmi_conn_det(int state)
{
	g_hdmi_dev.state = state ? HDMI_CONNECTED : !HDMI_CONNECTED;
	pr_debug("%s state %d state_old %d\n", __func__,
		g_hdmi_dev.state, g_hdmi_dev.state_old);
	if (g_hdmi_dev.state != g_hdmi_dev.state_old) {
		if (g_hdmi_dev.state == HDMI_CONNECTED) {
			/* connected event */
			g_hdmi_dev.state_old = g_hdmi_dev.state;
			hdmi_conn_change(HDMI_CONNECTED);
		}
		/* handle disconnected event in timer */
		mod_timer(&g_hdmi_dev.timer, jiffies + HDMI_TIMER_TIMEOUT);
	}
}

void hdmi_hpd_det(int state)
{
	u8 reg_value ;

	reg_value = readl(g_hdmi_dev.hdmi_regs_base + HDMI_PHY_CFG_3);
	if (state == 0)
		writel(reg_value & ~(1 << 10), g_hdmi_dev.hdmi_regs_base + HDMI_PHY_CFG_3);
	else{
		writel(reg_value | (1 << 10), g_hdmi_dev.hdmi_regs_base + HDMI_PHY_CFG_3);
		//hdmi_getEdidInfo(&g_hdmi_edid);
		hdmi_get_edid_info();
	}

	hdmi_conn_det(state);
}
#ifdef NOT_YET
static int hdmi_checkHdcpDevice(struct hdmi_device *hi)
{
	return 0;
}

static int hdmi_regWriteAnHdcpRx(struct hdmi_device *hi)
{
        return 0 ;
}
static int hdmi_regWriteBksvHdcpTx(struct hdmi_device *hi)
{
        return 0 ;
}
static int hdmi_regReadBksvHdcpRx(struct hdmi_device *hi)
{
        return 0 ;
}

static int hdmi_regWriteAksvHdcpRx(struct hdmi_device *hi)
{
        return 0 ;
}

static int hdmi_regReadAksvHdcpTx(struct hdmi_device *hi)
{
        return 0 ;
}

static int hdmi_regReadAnHdcpTx(struct hdmi_device *hi)
{
        return 0 ;
}

static int hdmi_generateAn(struct hdmi_device *hi)
{
        return 0 ;
}

static int hdmi_compareR0(struct hdmi_device *hi)
{
        return false ;
}
#endif

int hdmi_dumpSystemStatus(struct hdmi_device *hi)
{
        return 0 ;
}


int hdmi_hdcpAuthentication(struct hdmi_device *hi)
{
        return 0 ;
}


#ifdef CONFIG_HDMI_DDC_EDID
static int hdmiedid_probe(struct i2c_client *client,
		const struct i2c_device_id *id)
{
	int ret;
	struct hdmi_edid *edidinfo;
	edidinfo = kzalloc(sizeof(*edidinfo), GFP_KERNEL);
	if (edidinfo == NULL)
		return -ENOMEM;

//      edidinfo->edid_read = i2c_master_recv;
        i2c_set_clientdata(client, edidinfo);

	ret = device_create_file(&client->dev, &dev_attr_ResSupport);
	if (ret < 0)
		return -EBADFD;
	ret = device_create_file(&client->dev, &dev_attr_currentRes);
	if (ret < 0)
		return -EBADFD;

        edidinfo->dev = &client->dev;
        edidinfo->client = client;
	g_hdmi_edid = *edidinfo;

	/* timer function */
	init_timer(&g_hdmi_dev.timer);
	g_hdmi_dev.timer.function = hdmi_timer_work;

	/* get connect event if connected already */
	g_hdmi_dev.state_old = !HDMI_CONNECTED;

	return 0;
}

static int hdmiedid_remove(struct i2c_client *client)
{
	device_remove_file(&client->dev, &dev_attr_ResSupport);
	device_remove_file(&client->dev, &dev_attr_currentRes);
	return 0;
}

static const struct i2c_device_id hdmiedid_id[] = {
	{ "hdmi_edid", 0 },
	{ }
};

static struct i2c_driver hdmiedid_driver = {
	.driver = {
		.name	= "hdmi_edid",
	},
	.id_table 	= hdmiedid_id,
	.probe		= hdmiedid_probe,
	.remove		= hdmiedid_remove,
};
#endif

static irqreturn_t hdmi_irq_handler(int irq, void *dev_id)
{
       /* struct hdmi_device *dev = dev_id;*/
        return IRQ_HANDLED;
}

static struct file_operations mrvl_hdmi_fops = {
        .owner          = THIS_MODULE,
	.open    = mrvl_hdmi_fops_open,
	.ioctl   = mrvl_hdmi_fops_ioctl,
	.read    = mrvl_hdmi_fops_read,
};

static struct miscdevice pxa688_hdmi_miscdev = {
        .minor          = MISC_DYNAMIC_MINOR,
        .name           = "mmp2-hdmi",
        .fops           = &mrvl_hdmi_fops,
};

static int __devinit mrvl_hdmi_probe(struct platform_device *pdev)
{
        struct resource *res;
        struct hdmi_device *hi;
        int ret, size, irq;

        ret = misc_register(&pxa688_hdmi_miscdev);
        if (ret) {
                printk(KERN_WARNING "%s: failed to register misc device\n",
                        __func__);
                goto exit;
        }

        res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
        size = res->end - res->start + 1;
	hi = kzalloc(sizeof(*hi), GFP_KERNEL);
        if (hi == NULL) {
                printk(KERN_ERR "hdmi_probe: out of memory\n");
                return -ENOMEM;
        }

        hi->hdmi_regs_base = ioremap_nocache(res->start, size);
        if (hi->hdmi_regs_base == NULL) {
                printk(KERN_WARNING "failed to request register memory\n");
                ret = -EBUSY;
                goto failed_deregmisc;
        }
	g_hdmi_dev.hdmi_regs_base = hi->hdmi_regs_base;
	hi->pmua_regs_base = ioremap_nocache(0xD4282800, 0x1ff);
        if (hi->pmua_regs_base == NULL) {
                printk(KERN_WARNING "failed to request register memory\n");
                ret = -EBUSY;
                goto failed_deregmisc;
        }
	g_hdmi_dev.pmua_regs_base = hi->pmua_regs_base;

	hi->sspa1_regs_base = ioremap_nocache(0xD42A0C00, 0xff);
        if (hi->sspa1_regs_base == NULL) {
                printk(KERN_WARNING "failed to request register memory\n");
                ret = -EBUSY;
                goto failed_deregmisc;
        }
	g_hdmi_dev.sspa1_regs_base = hi->sspa1_regs_base;

	hi->lcd_regs_base = ioremap_nocache(0xD420B000, 0x1ef);
	if (hi->lcd_regs_base == NULL) {
		printk(KERN_WARNING "failed to request register memory\n");
		ret = -EBUSY;
		goto failed_deregmisc;
	}
	g_hdmi_dev.lcd_regs_base = hi->lcd_regs_base;

        irq = platform_get_irq(pdev, 0);
        ret = request_irq(irq, hdmi_irq_handler,
                                        IRQF_DISABLED, "pxa688-hdmi", hi);
        hi->irq = irq;

        if (ret)  {
                printk(KERN_WARNING "failed to request IRQ\n");
                goto failed_freemem;
        }

        platform_set_drvdata(pdev, hi);

        return 0;

failed_freemem:
        iounmap(hi->hdmi_regs_base);
        iounmap(hi->pmua_regs_base);
failed_deregmisc:
        misc_deregister(&pxa688_hdmi_miscdev);
exit:
        return ret;
}

static int __devexit mrvl_hdmi_remove(struct platform_device *pdev)
{
	struct hdmi_device *hi = platform_get_drvdata(pdev);

	iounmap(hi->hdmi_regs_base);
	iounmap(hi->pmua_regs_base);

        misc_deregister(&pxa688_hdmi_miscdev);
	return 0;
}

static struct platform_driver mrvl_hdmi_driver = {
        .driver     = {
                .owner          = THIS_MODULE,
                .name           = "mmp2-hdmi",
                },
		.probe          = mrvl_hdmi_probe,
		.remove         = __devexit_p(mrvl_hdmi_remove)
};

static int __init mrvl_hdmi_init(void)
{

#ifdef CONFIG_HDMI_DDC_EDID
	int ret;
	ret = i2c_add_driver(&hdmiedid_driver);
	if (ret != 0)
		pr_err("Failed to register DDC edid I2C driver: %d\n", ret);
#endif

#ifdef CONFIG_DVFM
	dvfm_register("HDMI", &dvfm_dev_idx);
#endif

	return platform_driver_register(&mrvl_hdmi_driver);
}

static void __exit mrvl_hdmi_exit(void)
{
        hdmi_dbg("----------%s----------\n",__FUNCTION__) ;
	platform_driver_unregister(&mrvl_hdmi_driver);
}

MODULE_AUTHOR("Mrvl");
MODULE_LICENSE("Marvell International Ltd");

module_init(mrvl_hdmi_init);
module_exit(mrvl_hdmi_exit);
