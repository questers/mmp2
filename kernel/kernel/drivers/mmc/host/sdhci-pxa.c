/* linux/drivers/mmc/host/sdhci-pxa.c
 *
 * Copyright (C) 2010 Marvell International Ltd.
 *		Zhangfei Gao <zhangfei.gao@marvell.com>
 *		Kevin Wang <dwang4@marvell.com>
 *		Mingwei Wang <mwwang@marvell.com>
 *		Philip Rakity <prakity@marvell.com>
 *		Mark Brown <markb@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

/* Supports:
 * SDHCI support for MMP2/PXA910/PXA168
 *
 * Refer to sdhci-s3c.c.
 */
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/mmc/host.h>
#include <linux/clk.h>
#include <linux/io.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <plat/sdhci.h>
#include "sdhci.h"

#define DRIVER_NAME	"sdhci-pxa"

/*****************************************************************************\
 *                                                                           *
 * SDHCI core callbacks                                                      *
 *                                                                           *
\*****************************************************************************/

static void set_clock(struct sdhci_host *host, unsigned int clock)
{
	struct sdhci_pxa *pxa = sdhci_priv(host);
	struct sdhci_pxa_platdata *pdata = pxa->pdata;

	if (clock) {
		if (pdata && pdata->soc_set_timing)
			pdata->soc_set_timing(host, pdata);
	}

}

static void sdhci_pxa_notify_change(struct platform_device *dev, int state)
{
	struct sdhci_host *host = platform_get_drvdata(dev);
	unsigned long flags;

	if (host) {
		spin_lock_irqsave(&host->lock, flags);
		if (state) {
			dev_dbg(&dev->dev, "card inserted.\n");
			host->flags &= ~SDHCI_DEVICE_DEAD;
		} else {
			dev_dbg(&dev->dev, "card removed.\n");
			host->flags |= SDHCI_DEVICE_DEAD;
		}
		tasklet_schedule(&host->card_tasklet);
		spin_unlock_irqrestore(&host->lock, flags);
	}
}

/*****************************************************************************\
 *                                                                           *
 * Device probing/removal                                                    *
 *                                                                           *
\*****************************************************************************/

static int __devinit sdhci_pxa_probe(struct platform_device *pdev)
{
	struct sdhci_pxa_platdata *pdata = pdev->dev.platform_data;
	struct device *dev = &pdev->dev;
	struct sdhci_host *host = NULL;
	struct resource *iomem = NULL;
	struct sdhci_pxa *pxa = NULL;
	int ret, irq;

	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		dev_err(dev, "no irq specified\n");
		return irq;
	}

	iomem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!iomem) {
		dev_err(dev, "no memory specified\n");
		return -ENOENT;
	}

	host = sdhci_alloc_host(&pdev->dev, sizeof(struct sdhci_pxa));
	if (IS_ERR(host)) {
		dev_err(dev, "failed to alloc host\n");
		return PTR_ERR(host);
	}

	pxa = sdhci_priv(host);
	pxa->host = host;
	pxa->pdata = pdata;
	pxa->clk_enable = 0;

	pxa->ops = kzalloc(sizeof(struct sdhci_ops), GFP_KERNEL);
	if (!pxa->ops) {
		ret = -ENOMEM;
		goto out;
	}

	pxa->clk = clk_get(dev, "PXA-SDHCLK");
	if (IS_ERR(pxa->clk)) {
		dev_err(dev, "failed to get io clock\n");
		ret = PTR_ERR(pxa->clk);
		goto out;
	}
	clk_enable(pxa->clk);

	pxa->res = request_mem_region(iomem->start, resource_size(iomem),
				      mmc_hostname(host->mmc));
	if (!pxa->res) {
		dev_err(&pdev->dev, "cannot request region\n");
		ret = -EBUSY;
		goto out;
	}

	host->ioaddr = ioremap(iomem->start, resource_size(iomem));
	if (!host->ioaddr) {
		dev_err(&pdev->dev, "failed to remap registers\n");
		ret = -ENOMEM;
		goto out;
	}

	host->hw_name = "MMC";
	host->irq = irq;
	host->quirks = SDHCI_QUIRK_BROKEN_ADMA | SDHCI_QUIRK_BROKEN_TIMEOUT_VAL;

	if (pdata && pdata->flags & PXA_FLAG_CARD_PERMANENT) {
		/* on-chip device */
		host->quirks |= SDHCI_QUIRK_BROKEN_CARD_DETECTION;
		host->mmc->caps |= MMC_CAP_NONREMOVABLE;
	}

	if (pdata && pdata->flags & PXA_FLAG_CONTROL_CLK_GATE) {
		/* v3 controller could support clock gating */
		host->mmc->caps |= MMC_CAP_CLOCK_GATE;
	}

	if (pdata && pdata->quirks)
		host->quirks |= pdata->quirks;

	if (pxa->pdata->setpower)
		pxa->pdata->setpower(host->mmc->parent, 1);

	if (pdata && pdata->soc_set_ops)
		pdata->soc_set_ops(pxa);

	pxa->ops->set_clock = set_clock;
	host->ops = pxa->ops;

	if (pdata && pdata->flags & PXA_FLAG_SDIO_RESUME) {
		host->mmc->pm_caps |= MMC_PM_KEEP_POWER |
					MMC_PM_SKIP_RESUME_PROBE;
	}

	ret = sdhci_add_host(host);
	if (ret) {
		dev_err(&pdev->dev, "failed to add host\n");
		goto out;
	}
	
	platform_set_drvdata(pdev, host);

#ifdef CONFIG_SD8XXX_RFKILL
    if (pxa->pdata->pmmc)
        *pxa->pdata->pmmc = host->mmc;
#endif

	if (pdata && pdata->max_speed)
		host->mmc->f_max = pdata->max_speed;

	if (pdata && pdata->ext_cd_init)
		pdata->ext_cd_init(&sdhci_pxa_notify_change, pdata);

	return 0;
out:
	if (host) {
		clk_disable(pxa->clk);
		clk_put(pxa->clk);
		kfree(pxa->ops);
		if (host->ioaddr)
			iounmap(host->ioaddr);
		if (pxa->res)
			release_mem_region(pxa->res->start,
					   resource_size(pxa->res));
		sdhci_free_host(host);
	}

	return ret;
}

static int __devexit sdhci_pxa_remove(struct platform_device *pdev)
{
	struct sdhci_pxa_platdata *pdata = pdev->dev.platform_data;
	struct sdhci_host *host = platform_get_drvdata(pdev);
	struct sdhci_pxa *pxa = sdhci_priv(host);
	int dead = 0;
	u32 scratch;

	if (host) {
		if (pdata && pdata->ext_cd_cleanup)
			pdata->ext_cd_cleanup(&sdhci_pxa_notify_change, pdata);

		scratch = readl(host->ioaddr + SDHCI_INT_STATUS);
		if (scratch == (u32)-1)
			dead = 1;

		sdhci_remove_host(host, dead);

		kfree(pxa->ops);
		if (host->ioaddr)
			iounmap(host->ioaddr);
		if (pxa->res)
			release_mem_region(pxa->res->start,
					   resource_size(pxa->res));
		clk_disable(pxa->clk);
		clk_put(pxa->clk);

		sdhci_free_host(host);
		platform_set_drvdata(pdev, NULL);
	}

	return 0;
}

#ifdef CONFIG_PM
static int sdhci_pxa_suspend(struct platform_device *dev, pm_message_t state)
{
	struct sdhci_host *host = platform_get_drvdata(dev);
	struct sdhci_pxa *pxa = sdhci_priv(host);

	int ret = 0;

	ret = sdhci_suspend_host(host, state);
	if (pxa->pdata->lp_switch)
		pxa->pdata->lp_switch(1, (int)host->mmc->card);
	if (pxa->pdata->setpower)
		pxa->pdata->setpower(host->mmc->parent, 0);
	return ret;
}

static int sdhci_pxa_resume(struct platform_device *dev)
{
	struct sdhci_host *host = platform_get_drvdata(dev);
	struct sdhci_pxa *pxa = sdhci_priv(host);

	int ret = 0;

	if (pxa->pdata->lp_switch)
		pxa->pdata->lp_switch(0, (int)host->mmc->card);
	if (pxa->pdata->setpower)
		pxa->pdata->setpower(host->mmc->parent, 1);

	ret = sdhci_resume_host(host);

	return ret;
}
#else
#define sdhci_pxa_suspend	NULL
#define sdhci_pxa_resume	NULL
#endif

static struct platform_driver sdhci_pxa_driver = {
	.probe		= sdhci_pxa_probe,
	.remove		= __devexit_p(sdhci_pxa_remove),
	.suspend	= sdhci_pxa_suspend,
	.resume		= sdhci_pxa_resume,
	.driver		= {
		.name	= DRIVER_NAME,
		.owner	= THIS_MODULE,
	},
};

/*****************************************************************************\
 *                                                                           *
 * Driver init/exit                                                          *
 *                                                                           *
\*****************************************************************************/

static int __init sdhci_pxa_init(void)
{
	return platform_driver_register(&sdhci_pxa_driver);
}

static void __exit sdhci_pxa_exit(void)
{
	platform_driver_unregister(&sdhci_pxa_driver);
}

module_init(sdhci_pxa_init);
module_exit(sdhci_pxa_exit);

MODULE_DESCRIPTION("SDH controller driver for PXA168/PXA910/MMP2");
MODULE_AUTHOR("Zhangfei Gao <zhangfei.gao@marvell.com>");
MODULE_LICENSE("GPL v2");
