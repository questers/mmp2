/**
 * An I2C driver for the Philips PCF8563 RTC
 * Copyright 2005-06 Tower Technologies
 *
 * Author: Alessandro Zummo <a.zummo@towertech.it>
 * Maintainers: http://www.nslu2-linux.org/
 *
 * based on the other drivers in this same directory.
 *
 * http://www.semiconductors.philips.com/acrobat/datasheets/PCF8563-04.pdf
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/bcd.h>
#include <linux/rtc.h>
#include <linux/slab.h>

#define DRV_VERSION "0.4.3"

#define PCF8563_REG_ST1		0x00 /** status */
#define PCF8563_REG_ST2		0x01

#define PCF8563_REG_SC		0x02 /** datetime */
#define PCF8563_REG_MN		0x03
#define PCF8563_REG_HR		0x04
#define PCF8563_REG_DM		0x05
#define PCF8563_REG_DW		0x06
#define PCF8563_REG_MO		0x07
#define PCF8563_REG_YR		0x08

#define PCF8563_REG_AMN		0x09 /** alarm */
#define PCF8563_REG_AHR		0x0A
#define PCF8563_REG_ADM		0x0B
#define PCF8563_REG_ADW		0x0C

#define PCF8563_REG_CLKO	0x0D /** clock out */
#define PCF8563_REG_TMRC	0x0E /** timer control */
#define PCF8563_REG_TMR		0x0F /** timer */

#define PCF8563_SC_LV		0x80 /** low voltage */
#define PCF8563_MO_C		0x80 /** century */


enum {
	RTC_SEC = 0,
	RTC_MIN,
	RTC_HOUR,
	RTC_DATE,
	RTC_WEEKDAY,
	RTC_MONTH,
	RTC_YEAR,
};

#define PCF8563_RTC_CNTL1		0x00
#define PCF8563_RTC_CNTL2		0x01
#define PCF8563_RTC_SEC			0x02
#define PCF8563_RTC_MIN			0x03
#define PCF8563_RTC_HOUR		0x04
#define PCF8563_RTC_DATE		0x05
#define PCF8563_RTC_WEEKDAY		0x06
#define PCF8563_RTC_MONTH		0x07
#define PCF8563_RTC_YEAR		0x08
#define PCF8563_ALARM_MIN		0x09
#define PCF8563_ALARM_HOUR		0x0a
#define PCF8563_ALARM_DATE		0x0b
#define PCF8563_ALARM_WEEKDAY		0x0c

#define TIME_NUM			7
#define ALARM_NUM			4
#define CENTURY_BIT			(1 << 7)
#define ALARM_AF			(1 << 3)
#define TIMER_TF			(1 << 2)
#define ALARM_AIE			(1 << 1)
struct pcf8563 {
	struct rtc_device *rtc;
	/**
	 * The meaning of MO_C bit varies by the chip type.
	 * From PCF8563 datasheet: this bit is toggled when the years
	 * register overflows from 99 to 00
	 *   0 indicates the century is 20xx
	 *   1 indicates the century is 19xx
	 * From RTC8564 datasheet: this bit indicates change of
	 * century. When the year digit data overflows from 99 to 00,
	 * this bit is set. By presetting it to 0 while still in the
	 * 20th century, it will be set in year 2000, ...
	 * There seems no reliable way to know how the system use this
	 * bit.  So let's do it heuristically, assuming we are live in
	 * 1970...2069.
	 */
	int c_polarity;	/** 0: MO_C=1 means 19xx, otherwise MO_C=1 means 20xx */
	struct i2c_client	*client;
};
static struct i2c_driver pcf8563_driver;


static inline void dump_tm(const char* desc,struct rtc_time* tm){
    printk("%s [%d.%d.%d %d:%d:%d %d]\n",desc,
    tm->tm_year,tm->tm_mon,tm->tm_mday,
    tm->tm_hour,tm->tm_min,tm->tm_sec,
    tm->tm_wday);
}

/**
 * In the routines that deal directly with the pcf8563 hardware, we use
 * rtc_time -- month 0-11, hour 0-23, yr = calendar year-epoch.
 */
static int pcf8563_set_datetime(struct i2c_client *client, struct rtc_time *tm)
{
	struct pcf8563 *pcf8563 = i2c_get_clientdata(client);
	int i, err;
	unsigned char buf[9];

    dump_tm(__func__,tm);

	/** hours, minutes and seconds */
	buf[PCF8563_REG_SC] = bin2bcd(tm->tm_sec);
	buf[PCF8563_REG_MN] = bin2bcd(tm->tm_min);
	buf[PCF8563_REG_HR] = bin2bcd(tm->tm_hour);

	buf[PCF8563_REG_DM] = bin2bcd(tm->tm_mday);

	/** month, 1 - 12 */
	buf[PCF8563_REG_MO] = bin2bcd(tm->tm_mon + 1);

	/** year and century */
	buf[PCF8563_REG_YR] = bin2bcd(tm->tm_year % 100);
	if (pcf8563->c_polarity ? (tm->tm_year >= 100) : (tm->tm_year < 100))
		buf[PCF8563_REG_MO] |= PCF8563_MO_C;

	buf[PCF8563_REG_DW] = tm->tm_wday & 0x07;

	/** write register's data */
	for (i = 0; i < 7; i++) {
		unsigned char data[2] = { PCF8563_REG_SC + i,
						buf[PCF8563_REG_SC + i] };

		err = i2c_master_send(client, data, sizeof(data));
		if (err != sizeof(data)) {
			dev_err(&client->dev,
				"%s: err=%d addr=%02x, data=%02x\n",
				__func__, err, data[0], data[1]);
			return -EIO;
		}
	};

	return 0;
} 
static int pcf8563_get_datetime(struct i2c_client *client, struct rtc_time *tm)
{
	struct pcf8563 *pcf8563 = i2c_get_clientdata(client);
	unsigned char buf[13] = { PCF8563_REG_ST1 };

	struct i2c_msg msgs[] = {
		{ client->addr, 0, 1, buf },	/** setup read ptr */
		{ client->addr, I2C_M_RD, 13, buf },	/** read status + date */
	};

	/** read registers */
	if ((i2c_transfer(client->adapter, msgs, 2)) != 2) {
		dev_err(&client->dev, "%s: read error\n", __func__);
		return -EIO;
	}

	if (buf[PCF8563_REG_SC] & PCF8563_SC_LV)
		dev_info(&client->dev,
			"low voltage detected, date/time is not reliable.\n");

	tm->tm_sec = bcd2bin(buf[PCF8563_REG_SC] & 0x7F);
	tm->tm_min = bcd2bin(buf[PCF8563_REG_MN] & 0x7F);
	tm->tm_hour = bcd2bin(buf[PCF8563_REG_HR] & 0x3F); /** rtc hr 0-23 */
	tm->tm_mday = bcd2bin(buf[PCF8563_REG_DM] & 0x3F);
	tm->tm_wday = buf[PCF8563_REG_DW] & 0x07;
	tm->tm_mon = bcd2bin(buf[PCF8563_REG_MO] & 0x1F) - 1; /** rtc mn 1-12 */
	tm->tm_year = bcd2bin(buf[PCF8563_REG_YR]);
	if (tm->tm_year < 70)
		tm->tm_year += 100;	/** assume we are in 1970...2069 */
	/** detect the polarity heuristically. see note above. */
	pcf8563->c_polarity = (buf[PCF8563_REG_MO] & PCF8563_MO_C) ?
		(tm->tm_year >= 100) : (tm->tm_year < 100);

	/*
	Ellie fixes the android crash: 
	
	"Attempt to launch receivers of broadcast intent Intent { act=android.intent.action.TIME_SET flg=0x20000000 }
	before boot completion"
	
	if System.currentTimeMillis() returns negative value, system server will call setCurrentTimeMillis and do 
	ANDROID_ALARM_SET_RTC, then when AlarmThread runs, it calls waitForAlarm and do ANDROID_ALARM_WAIT,
	ANDROID_ALARM_TIME_CHANGE_MASK will be returned and broadcast to the system before boot completion.
	
	struct timeval {
		__kernel_time_t 	tv_sec; 
		__kernel_suseconds_t	tv_usec;
	};
	typedef long			__kernel_time_t;
	typedef long			__kernel_suseconds_t;
	
	maximum positive seconds:0x7fffffff=2147483647
	maximum days:24855
	maximum year:68
	1970+68=2038, make it 2037 for safety
	*/
    if(tm->tm_year >= 137 || tm->tm_year < 100)
    {
	    printk("%s:invalid year, forcing to 2000/1/1 wday[2]\n",__func__);
		memset(tm, 0, sizeof(tm));
		tm->tm_year = 100;
		tm->tm_mon  = 1;
		tm->tm_mday = 1;
		tm->tm_wday = 2;
		pcf8563_set_datetime(client,tm);
    }
    dump_tm(__func__,tm);

	/** the clock can give out invalid datetime, but we cannot return
	 * -EINVAL otherwise hwclock will refuse to set the time on bootup.
	 */
	if (rtc_valid_tm(tm) < 0)
		dev_err(&client->dev, "retrieved date/time is not valid.\n");

	return 0;
}




static irqreturn_t rtc_update_handler(int irq, void *data)
{
	struct pcf8563 *pcf8563 = (struct pcf8563 *)(data);
	int ret;
	ret = i2c_smbus_read_byte_data(pcf8563->client, PCF8563_REG_ST2);
	printk("pcf8563 rtc interrupt event=%d\n",ret);
	if (ret & ALARM_AF) {
		rtc_update_irq(pcf8563->rtc, 1, RTC_IRQF | RTC_AF);
	}
	if (ret & TIMER_TF)
	{
		rtc_update_irq(pcf8563->rtc, 1, RTC_IRQF | RTC_UF);
	}
	//anyway clear alarm flags
	ret = 0;
	i2c_smbus_write_byte_data(pcf8563->client, PCF8563_REG_ST2, ret);

	return IRQ_HANDLED;
}

#if 0
static int tm_calc(struct rtc_time *tm, unsigned char *buf, int len)
{
	if (len < TIME_NUM)
		return -EINVAL;
	tm->tm_year = (buf[RTC_YEAR] >> 4) * 10
			+ (buf[RTC_YEAR] & 0xf);
	tm->tm_year += (buf[RTC_MONTH] >> 7) ? 1900 : 2000;
	tm->tm_year -= 1900;
	tm->tm_mon = ((buf[RTC_MONTH] >> 4) & 0x01) * 10
			+ (buf[RTC_MONTH] & 0x0f) - 1;
	tm->tm_mday = ((buf[RTC_DATE] >> 4) & 0x03) * 10
			+ (buf[RTC_DATE] & 0x0f);
	tm->tm_wday = buf[RTC_WEEKDAY] & 0x07;
	tm->tm_hour = ((buf[RTC_HOUR] >> 4) & 0x03) * 10
			+ (buf[RTC_HOUR] & 0x0f);
	tm->tm_min = ((buf[RTC_MIN] >> 4) & 0x7) * 10
			+ (buf[RTC_MIN] & 0x0f);
	tm->tm_sec = ((buf[RTC_SEC] >> 4) & 0x7) * 10
			+ (buf[RTC_SEC] & 0x0f);

	return 0;
}

static int data_calc(unsigned char *buf, struct rtc_time *tm, int len)
{
	unsigned char high, low;

	if (len < TIME_NUM)
		return -EINVAL;

	high = (tm->tm_year + 1900) / 10;
	low = tm->tm_year + 1900;
	low = low - high * 10;
	high = high - (high / 10) * 10;
	buf[RTC_YEAR] = (high << 4) + low;
	high = (tm->tm_mon + 1) / 10;
	low = (tm->tm_mon + 1);
	low = low - high * 10;
	if (tm->tm_year < 100)
		high |= (1 << 3);
	buf[RTC_MONTH] = (high << 4) + low;
	high = tm->tm_mday / 10;
	low = tm->tm_mday;
	low = low - high * 10;
	buf[RTC_DATE] = (high << 4) + low;
	buf[RTC_WEEKDAY] = tm->tm_wday;
	high = tm->tm_hour / 10;
	low = tm->tm_hour;
	low = low - high * 10;
	buf[RTC_HOUR] = (high << 4) + low;
	high = tm->tm_min / 10;
	low = tm->tm_min;
	low = low - high * 10;
	buf[RTC_MIN] = (high << 4) + low;
	high = tm->tm_sec / 10;
	low = tm->tm_sec;
	low = low - high * 10;
	buf[RTC_SEC] = (high << 4) + low;
	return 0;
}
#endif

static int pcf8563_rtc_read_time(struct device *dev, struct rtc_time *tm)
{
	#if 0
	struct i2c_client *client = to_i2c_client(dev);
	unsigned char buf[TIME_NUM];
	int ret;

	ret = i2c_smbus_read_i2c_block_data(client, PCF8563_RTC_SEC, TIME_NUM, buf);
	if (ret < 0)
		goto out;


	ret = tm_calc(tm, buf, TIME_NUM);
	/*
	Ellie fixes the android crash: 
	
	"Attempt to launch receivers of broadcast intent Intent { act=android.intent.action.TIME_SET flg=0x20000000 }
	before boot completion"
	
	if System.currentTimeMillis() returns negative value, system server will call setCurrentTimeMillis and do 
	ANDROID_ALARM_SET_RTC, then when AlarmThread runs, it calls waitForAlarm and do ANDROID_ALARM_WAIT,
	ANDROID_ALARM_TIME_CHANGE_MASK will be returned and broadcast to the system before boot completion.
	
	struct timeval {
		__kernel_time_t 	tv_sec; 
		__kernel_suseconds_t	tv_usec;
	};
	typedef long			__kernel_time_t;
	typedef long			__kernel_suseconds_t;
	
	maximum positive seconds:0x7fffffff=2147483647
	maximum days:24855
	maximum year:68
	1970+68=2038, make it 2037 for safety
	*/
    if(tm->tm_year >= 137 || tm->tm_year < 100)
    {
	    printk("%s:invalid year, forcing to 2000\n",__func__);
		memset(tm, 0, sizeof(tm));
		tm->tm_year = 100;
		tm->tm_mday = 1;
    }
    dump_tm(__func__,tm);
out:
	return ret;
	#else
	return pcf8563_get_datetime(to_i2c_client(dev), tm);
	#endif
}

static int pcf8563_rtc_set_time(struct device *dev, struct rtc_time *tm)
{
	#if 0
	struct i2c_client *client = to_i2c_client(dev);
	unsigned char buf[TIME_NUM];
	int ret;

	ret = data_calc(buf, tm, TIME_NUM);
	if (ret < 0)
		goto out;

    dump_tm(__func__,tm);
	ret = i2c_smbus_write_i2c_block_data(client, PCF8563_RTC_SEC, TIME_NUM, buf);
out:
	return ret;
	#else
	return pcf8563_set_datetime(to_i2c_client(dev), tm);
	#endif
}

static int pcf8563_rtc_read_alarm(struct device *dev, struct rtc_wkalrm *alrm)
{
	#if 0
	struct i2c_client *client = to_i2c_client(dev);
	unsigned char buf[TIME_NUM];
	int ret;

	ret = i2c_smbus_read_i2c_block_data(client, PCF8563_RTC_SEC, TIME_NUM, buf);
	if (ret < 0)
		goto out;
	buf[RTC_SEC] = 0;
	ret = i2c_smbus_read_i2c_block_data(client, PCF8563_ALARM_MIN, ALARM_NUM, &buf[RTC_MIN]);
	if (ret < 0)
		goto out;
	ret = tm_calc(&alrm->time, buf, TIME_NUM);
	if (ret < 0)
		goto out;
	ret = i2c_smbus_read_byte_data(client, PCF8563_RTC_CNTL2);
	if (ret < 0)
		goto out;
	if (ret & ALARM_AIE)
		alrm->enabled = 1;
	else
		alrm->enabled = 0;
	if (ret & ALARM_AF)
		alrm->pending = 1;
	else
		alrm->pending = 0;
	#else	
	struct i2c_client *client = to_i2c_client(dev);
	unsigned char buf[13] = { PCF8563_REG_ST1 };
	struct rtc_time *tm = &alrm->time;

	struct i2c_msg msgs[] = {
		{ client->addr, 0, 1, buf },	/** setup read ptr */
		{ client->addr, I2C_M_RD, 13, buf },	/** read status + date */
	};

	/** read registers */
	if ((i2c_transfer(client->adapter, msgs, 2)) != 2) {
		dev_err(&client->dev, "%s: read error\n", __func__);
		return -EIO;
	}

	if (buf[PCF8563_REG_SC] & PCF8563_SC_LV)
		dev_info(&client->dev,
			"low voltage detected, date/time is not reliable.\n");

	tm->tm_sec = -1;//bcd2bin(buf[PCF8563_REG_SC] & 0x7F);//no alarm second,adopt current second
	tm->tm_min = bcd2bin(buf[PCF8563_REG_AMN] & 0x7F);
	tm->tm_hour = bcd2bin(buf[PCF8563_REG_AHR] & 0x3F); /** rtc hr 0-23 */
	tm->tm_mday = bcd2bin(buf[PCF8563_REG_ADM] & 0x3F);
	tm->tm_wday = -1;//buf[PCF8563_REG_ADW] & 0x07;
	tm->tm_mon = -1;//bcd2bin(buf[PCF8563_REG_MO] & 0x1F) - 1; /** rtc mn 1-12 */
	tm->tm_year = -1;//bcd2bin(buf[PCF8563_REG_YR]);
	#if 0
	if (tm->tm_year < 70)
		tm->tm_year += 100;	/** assume we are in 1970...2069 */
	/** detect the polarity heuristically. see note above. */
	pcf8563->c_polarity = (buf[PCF8563_REG_MO] & PCF8563_MO_C) ?
		(tm->tm_year >= 100) : (tm->tm_year < 100);
	#endif
	alrm->enabled=(buf[PCF8563_REG_ST2]&ALARM_AIE)?1:0;	
	alrm->pending=(buf[PCF8563_REG_ST2]&ALARM_AF)?1:0;
	#endif

    dump_tm(__func__,&alrm->time);

	return 0;
}

static int pcf8563_rtc_set_alarm(struct device *dev, struct rtc_wkalrm *alrm)
{
	struct i2c_client *client = to_i2c_client(dev);
	unsigned char buf[13];
	int ret;
	unsigned long time_tmp;
	struct rtc_time* tm = &alrm->time;

	if (alrm->time.tm_sec >= 1) {
		rtc_tm_to_time(&(alrm->time), &time_tmp);
		time_tmp += 59;
		rtc_time_to_tm(time_tmp, &(alrm->time));
	}

	#if 0
	ret = data_calc(buf, &alrm->time, TIME_NUM);
	i2c_smbus_read_i2c_block_data(client, PCF8563_RTC_WEEKDAY, 1, &wday);

    //fix weekday alarm issue since rtc time time never update weekday
    buf[RTC_WEEKDAY] = wday;
	printk("%s alarm %s\n",__func__,alrm->enabled?"enabled":"disabled");
    dump_tm(__func__,&alrm->time);
	
	if (ret < 0)
		goto out;
	ret = i2c_smbus_write_i2c_block_data(client, PCF8563_ALARM_MIN, ALARM_NUM, &buf[RTC_MIN]);
	if (ret < 0)
		goto out;
	ret = i2c_smbus_read_byte_data(client, PCF8563_RTC_CNTL2);
	if (ret < 0)
		goto out;
	#else
	/** alarm  minute hour mday*/
	buf[PCF8563_REG_AMN] = bin2bcd(tm->tm_min);
	buf[PCF8563_REG_AHR] = bin2bcd(tm->tm_hour);
	buf[PCF8563_REG_ADM] = bin2bcd(tm->tm_mday);
	buf[PCF8563_REG_ADW] = tm->tm_wday|0x80;	
	printk("%s alarm %s\n",__func__,alrm->enabled?"enabled":"disabled");
    dump_tm(__func__,&alrm->time);
	
	ret = i2c_smbus_write_i2c_block_data(client, PCF8563_REG_AMN, 4, &buf[PCF8563_REG_AMN]);
	if (ret < 0)
		goto out;
	#endif
	ret = alrm->enabled?ALARM_AIE:0;	
	ret = i2c_smbus_write_byte_data(client, PCF8563_RTC_CNTL2, ret);
	if (ret < 0)
		goto out;
	return 0;
out:
	return ret;
}
static int pcf8563_rtc_alarm_irq_enable(struct device *dev, unsigned int enabled)
{
	struct i2c_client *client = to_i2c_client(dev);
	int ret;

	ret = i2c_smbus_read_byte_data(client, PCF8563_RTC_CNTL2);
	if (ret < 0)
		goto out;
	if (enabled)
		ret = ALARM_AIE;
	else
		ret = 0;
	i2c_smbus_write_byte_data(client, PCF8563_RTC_CNTL2, ret);

	return 0;
out:
	return ret;
}


static const struct rtc_class_ops pcf8563_rtc_ops = {
	.read_time	= pcf8563_rtc_read_time,
	.set_time	= pcf8563_rtc_set_time,
	.read_alarm	= pcf8563_rtc_read_alarm,
	.set_alarm	= pcf8563_rtc_set_alarm,
	.alarm_irq_enable = pcf8563_rtc_alarm_irq_enable,	
};

static ssize_t regs_show(struct device *dev,
                          struct device_attribute *attr, char *buf)
{
	struct i2c_client* client = to_i2c_client(dev);
	unsigned char regbuf[13] = { PCF8563_REG_ST1 };
	int i;
	int ret=0;

	struct i2c_msg msgs[] = {
		{ client->addr, 0, 1, regbuf },	/** setup read ptr */
		{ client->addr, I2C_M_RD, 13, regbuf },	/** read status + date */
	};

	/** read registers */
	if ((i2c_transfer(client->adapter, msgs, 2)) != 2) {
		dev_err(&client->dev, "%s: read error\n", __func__);
		return 0;
	}

	
	for(i=0;i<13;i++)
		ret+= sprintf(buf+ret,"%#x \n",regbuf[i]);
	ret+=sprintf(buf+ret,"%s\n"," ==end==");

	return ret;
}


static DEVICE_ATTR(regs, S_IRUGO|S_IWUGO,regs_show, NULL);

static int __devinit pcf8563_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct pcf8563 *pcf8563;

	int ret;

	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
		return -ENODEV;

    // probe the device    
	ret = i2c_smbus_write_byte_data(client, PCF8563_RTC_CNTL1, 0x0);
	if (ret < 0) return -ENODEV;
	ret = i2c_smbus_write_byte_data(client, PCF8563_RTC_CNTL2, 0x2);
	if (ret < 0) return -ENODEV;

	pcf8563 = kzalloc(sizeof(struct pcf8563), GFP_KERNEL);
	if(!pcf8563)
	{
		dev_err(&client->dev,"no mem for driver\n");
		return -ENOMEM;
	}
	dev_info(&client->dev, "chip found, driver version " DRV_VERSION "\n");
	
	pcf8563->client = client;
	i2c_set_clientdata(client, pcf8563);

	ret = request_threaded_irq(client->irq,NULL, rtc_update_handler,
				   IRQF_NO_SUSPEND | IRQF_ONESHOT | IRQF_TRIGGER_FALLING | IRQF_TRIGGER_RISING,
				   "pcf8563-rtc", pcf8563);
	if (ret < 0) {
		dev_err(&client->dev, "Failed to request IRQ: #%d: %d\n",
			client->irq, ret);
		goto out_irq;
	}


	pcf8563->rtc = rtc_device_register(pcf8563_driver.driver.name,
				&client->dev, &pcf8563_rtc_ops, THIS_MODULE);

	if (IS_ERR(pcf8563->rtc)) {
		ret = PTR_ERR(pcf8563->rtc);
		goto out_rtc;
	}
	
    ret = device_create_file(&client->dev,&dev_attr_regs);

	return 0;
out_rtc:
	free_irq(client->irq, pcf8563);
out_irq:
	kfree(pcf8563);
	return ret;
}

static int pcf8563_remove(struct i2c_client *client)
{
	struct pcf8563* pcf8563 = i2c_get_clientdata(client);

    free_irq(client->irq, pcf8563);
	if (pcf8563->rtc)
		rtc_device_unregister(pcf8563->rtc);

	device_remove_file(&client->dev,&dev_attr_regs);
	
	kfree(pcf8563);
	return 0;
}

static const struct i2c_device_id pcf8563_id[] = {
	{ "pcf8563-rtc", 0 },
	{ "pcf8564-rtc", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, pcf8563_id);

static struct i2c_driver pcf8563_driver = {
	.driver		= {
		.name	= "pcf8563-rtc",
		.owner	= THIS_MODULE,
	},
	.probe		= pcf8563_probe,
	.remove		= pcf8563_remove,
	.id_table	= pcf8563_id,
};

static int __init pcf8563_init(void)
{
	return i2c_add_driver(&pcf8563_driver);
}

static void __exit pcf8563_exit(void)
{
	i2c_del_driver(&pcf8563_driver);
}

MODULE_AUTHOR("Alessandro Zummo <a.zummo@towertech.it>");
MODULE_DESCRIPTION("Philips PCF8563/Epson RTC8564 RTC driver");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRV_VERSION);

module_init(pcf8563_init);
module_exit(pcf8563_exit);
