/*
 * BQ27410 system-side battery fuel-gauge driver.
 *
 * Copyright (c) 2011 Marvell Technology Ltd.
 * Yunfan Zhang <yfzhang@marvell.com>
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */
#include <linux/module.h>
#include <linux/param.h>
#include <linux/jiffies.h>
#include <linux/workqueue.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/power_supply.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>

enum bq27410_cmds {
	/* Standard Commands */
	BQ27410_ST_CNTL,
	BQ27410_ST_TEMP,
	BQ27410_ST_VOLT,
	BQ27410_ST_FLAGS,
	BQ27410_ST_NAC,
	BQ27410_ST_FAC,
	BQ27410_ST_RM,
	BQ27410_ST_FCC,
	BQ27410_ST_AI,
	BQ27410_ST_SI,
	BQ27410_ST_MLI,
	BQ27410_ST_AE,
	BQ27410_ST_AP,
	BQ27410_ST_SOC,
	BQ27410_ST_ITEMP,
	BQ27410_ST_SOH,
	/* Extended Commands */
	BQ27410_EXT_OPCFG,
	BQ27410_EXT_DCAP,
	BQ27410_EXT_DFCLS,
	BQ27410_EXT_DFBLK,
	BQ27410_EXT_DFD,
	BQ27410_EXT_DFDCKS,
	BQ27410_EXT_DFDCNTL,
	BQ27410_EXT_DNAMELEN,
	BQ27410_EXT_DNAME,
};

/* Control() Subcommands: control data */
#define BQ27410_CNTL_STATUS				0x0000
#define BQ27410_CNTL_DEVICE_TYPE		0x0001
#define BQ27410_CNTL_FW_VERSION			0x0002
#define BQ27410_CNTL_HW_VERSION			0x0003
#define BQ27410_CNTL_PREV_MACWRITE		0x0007
#define BQ27410_CNTL_BAT_INSERT			0x000c
#define BQ27410_CNTL_BAT_REMOVE			0x000d
#define BQ27410_CNTL_SET_FULLSLEEP		0x0010
#define BQ27410_CNTL_SET_HIBERNATE		0x0011
#define BQ27410_CNTL_CLEAR_HIBERNATE	0x0012
#define BQ27410_CNTL_FACTORY_RESTORE	0x0015
#define BQ27410_CNTL_SEALED				0x0020
#define BQ27410_CNTL_RESET				0x0041
/* Unsealed Key0/1 */
#define BQ27410_CNTL_UNSEALED_KEY_0		0x0414
#define BQ27410_CNTL_UNSEALED_KEY_1		0x3672
/* FullAccess Key0/1 */
#define BQ27410_CNTL_FULLACCESS_KEY_0	0xffff
#define BQ27410_CNTL_FULLACCESS_KEY_1	0xffff

enum bq27410_data_flash {
	BQ27410_DF_DCAP,
	BQ27410_DF_DE,
	BQ27410_DF_OPCFG,
	BQ27410_DF_TVCFG,
};

#define BQ27410_DF_INFO(_subclass, _offset, _len) { \
	.subclass = _subclass, \
	.offset = _offset, \
	.len = _len, \
}

struct bq27410_data_flash_info {
	u8 subclass;
	u8 offset;
	u8 len;
};

static const struct bq27410_data_flash_info data_flash_info[] = {
	[BQ27410_DF_DCAP] = BQ27410_DF_INFO(48, 19, 2),
	[BQ27410_DF_DE] = BQ27410_DF_INFO(48, 21, 2),
	[BQ27410_DF_OPCFG] = BQ27410_DF_INFO(64, 0, 1),
	[BQ27410_DF_TVCFG] = BQ27410_DF_INFO(80, 45, 2),
};

/* Flash data settings */
#define BQ27410_BAT_DCAP	0xfa0	/* 4000mAh */
#define BQ27410_BAT_DE	0x39d0	/* 4000*3.7=14800mWh */
#define BQ27410_BAT_OPCFG	0x38	/* BATLOWEN:1, SLEEP:1, RMFCC:1 */

#define BQ27410_CMD_INFO(_cmd, _len) { \
	.cmd = _cmd, \
	.len = _len, \
}

struct bq27410_cmd_info {
	u8 cmd;
	u8 len;
};

static const struct bq27410_cmd_info cmds_info[] = {
	/* Standard Commands */
	[BQ27410_ST_CNTL] = BQ27410_CMD_INFO(0x00, 2),
	[BQ27410_ST_TEMP] = BQ27410_CMD_INFO(0x02, 2),
	[BQ27410_ST_VOLT] = BQ27410_CMD_INFO(0x04, 2),
	[BQ27410_ST_FLAGS] = BQ27410_CMD_INFO(0x06, 2),
	[BQ27410_ST_NAC] = BQ27410_CMD_INFO(0x08, 2),
	[BQ27410_ST_FAC] = BQ27410_CMD_INFO(0x0a, 2),
	[BQ27410_ST_RM] = BQ27410_CMD_INFO(0x0c, 2),
	[BQ27410_ST_FCC] = BQ27410_CMD_INFO(0x0e, 2),
	[BQ27410_ST_AI] = BQ27410_CMD_INFO(0x10, 2),
	[BQ27410_ST_SI] = BQ27410_CMD_INFO(0x12, 2),
	[BQ27410_ST_MLI] = BQ27410_CMD_INFO(0x14, 2),
	[BQ27410_ST_AE] = BQ27410_CMD_INFO(0x16, 2),
	[BQ27410_ST_AP] = BQ27410_CMD_INFO(0x18, 2),
	[BQ27410_ST_SOC] = BQ27410_CMD_INFO(0x1c, 2),
	[BQ27410_ST_ITEMP] = BQ27410_CMD_INFO(0x1e, 2),
	[BQ27410_ST_SOH] = BQ27410_CMD_INFO(0x20, 2),
	/* Extended Commands */
	[BQ27410_EXT_OPCFG] = BQ27410_CMD_INFO(0x3a, 1),
	[BQ27410_EXT_DCAP] = BQ27410_CMD_INFO(0x3c, 2),
	[BQ27410_EXT_DFCLS] = BQ27410_CMD_INFO(0x3e, 1),
	[BQ27410_EXT_DFBLK] = BQ27410_CMD_INFO(0x3f, 1),
	[BQ27410_EXT_DFD] = BQ27410_CMD_INFO(0x40, 32),
	[BQ27410_EXT_DFDCKS] = BQ27410_CMD_INFO(0x60, 1),
	[BQ27410_EXT_DFDCNTL] = BQ27410_CMD_INFO(0x61, 1),
	[BQ27410_EXT_DNAMELEN] = BQ27410_CMD_INFO(0x62, 1),
	[BQ27410_EXT_DNAME] = BQ27410_CMD_INFO(0x63, 7),
};

struct bq27410_battery_params {
	/* current battery states */
	int status;
	int present;
	int volt;
	int cur;
	int cap;
	int temp;
	int health;
	int tech;
	/* last battery states */
	int last_status;
	int last_present;
	int last_volt;
	int last_cur;
	int last_cap;
	int last_temp;
};

struct bq27410_device_info {
	struct device *dev;
	struct power_supply bat;
	struct i2c_client *client;
	struct bq27410_battery_params bat_params;
	struct delayed_work monitor_work;
	unsigned int interval;
};

static enum power_supply_property bq27410_battery_props[] = {
	POWER_SUPPLY_PROP_STATUS,
	POWER_SUPPLY_PROP_PRESENT,
	POWER_SUPPLY_PROP_VOLTAGE_NOW,
	POWER_SUPPLY_PROP_CURRENT_NOW,
	POWER_SUPPLY_PROP_CAPACITY,
	POWER_SUPPLY_PROP_TEMP,
	POWER_SUPPLY_PROP_HEALTH,
	POWER_SUPPLY_PROP_TECHNOLOGY,
};

/* Basic functions for register reading and writing:
 * 1.Read register:
 *  bq27410_read_reg(u8 cmd, u8 len, u8 *data, struct i2c_client *client)
 * 2.Write register:
 *  bq27410_write_reg(u8 cmd, u8 len, u8 *data, struct i2c_client *client)
 * */
/* Return the number of read bytes */
static int bq27410_read_reg(u8 cmd, u8 len, u8 *data,
				struct i2c_client *client)
{
	int ret = 0;

	if (!client || !data || len <= 0)
		return -EINVAL;

	ret = i2c_smbus_read_i2c_block_data(client, cmd, len, data);

	return ret;
}

/* Return a negative errno code else zero on success. */
static int bq27410_write_reg(u8 cmd, u8 len, u8 *data,
				struct i2c_client *client)
{
	int ret = 0;

	if (!client || !data || len <= 0)
		return -EINVAL;

	ret = i2c_smbus_write_i2c_block_data(client, cmd, len, data);

	return ret;
}

/* Basic APIs for control and configuration */
static int bq27410_control_get_status(u8 *data, struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_CNTL];
	u8 buf[2] = { 0 };
	int ret = 0;

	/* instructs fuel gauge to return status information */
	buf[0] = BQ27410_CNTL_STATUS & 0xff;
	buf[1] = (BQ27410_CNTL_STATUS >> 8) & 0xff;
	ret = bq27410_write_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret < 0) {
		pr_debug("%s:failed!\n", __func__);
		return ret;
	}
	/* read control status */
	ret = bq27410_read_reg(cmd_info->cmd, cmd_info->len, data, client);
	if (ret != cmd_info->len) {
		pr_debug("%s:failed!\n", __func__);
		return -1;
	}

	return 0;
}

/* Force BAT_DET bit in Flags to 1.
 * Return negative code on failed, zero on success */
static int bq27410_control_bat_insert(struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_CNTL];
	u8 buf[2] = { 0 };
	int ret = 0;

	buf[0] = BQ27410_CNTL_BAT_INSERT & 0xff;
	buf[1] = (BQ27410_CNTL_BAT_INSERT >> 8) & 0xff;
	ret = bq27410_write_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret < 0)
		pr_debug("%s:failed!\n", __func__);

	return ret;
}

/* Force BAT_DET bit in Flags to 0.
 * Return negative code on failed, zero on success */
static int bq27410_control_bat_remove(struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_CNTL];
	u8 buf[2] = { 0 };
	int ret = 0;

	buf[0] = BQ27410_CNTL_BAT_REMOVE & 0xff;
	buf[1] = (BQ27410_CNTL_BAT_REMOVE >> 8) & 0xff;
	ret = bq27410_write_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret < 0)
		pr_debug("%s:failed!\n", __func__);

	return ret;
}

/* Place chip in SEALED access mode.
 * Return negative code on failed, zero on success */
static int bq27410_control_sealed(struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_CNTL];
	u8 buf[2] = { 0 };
	int ret = 0;

	buf[0] = BQ27410_CNTL_SEALED & 0xff;
	buf[1] = (BQ27410_CNTL_SEALED >> 8) & 0xff;
	ret = bq27410_write_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret < 0)
		pr_debug("%s:failed!\n", __func__);

	return ret;
}

/* Place chip in UNSEALED access mode.
 * Return negative code on failed, zero on success */
static int bq27410_control_unsealed(struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_CNTL];
	u8 buf[4] = { 0 };
	int ret = 0;

	/* Sealed to Unsealed */
	buf[0] = BQ27410_CNTL_UNSEALED_KEY_0 & 0xff;
	buf[1] = (BQ27410_CNTL_UNSEALED_KEY_0 >> 8) & 0xff;
	buf[2] = BQ27410_CNTL_UNSEALED_KEY_1 & 0xff;
	buf[3] = (BQ27410_CNTL_UNSEALED_KEY_1 >> 8) & 0xff;
	ret = bq27410_write_reg(cmd_info->cmd, 4, buf, client);
	if (ret < 0)
		pr_debug("%s:failed!\n", __func__);

	return ret;
}

/* Force a full reset of the bq27410.
 * Return negative code on failed, zero on success */
static int bq27410_control_reset(struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_CNTL];
	u8 buf[2] = { 0 };
	int ret = 0;

	buf[0] = BQ27410_CNTL_RESET & 0xff;
	buf[1] = (BQ27410_CNTL_RESET >> 8) & 0xff;
	ret = bq27410_write_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret < 0)
		pr_debug("%s:failed!\n", __func__);

	return ret;
}

/* APIs for extended commands */
/* Get Operation Configuration */
static int bq27410_ext_get_opconfig(u8 *data, struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_EXT_OPCFG];
	int ret = 0;

	ret = bq27410_read_reg(cmd_info->cmd, cmd_info->len, data, client);
	if (ret != cmd_info->len) {
		pr_debug("%s:failed!\n", __func__);
		return -1;
	}

	return 0;
}

/* Get Design Capacity */
static int bq27410_ext_get_design_capacity(u8 *data,
				struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_EXT_DCAP];
	int ret = 0;

	ret = bq27410_read_reg(cmd_info->cmd, cmd_info->len, data, client);
	if (ret != cmd_info->len) {
		pr_debug("%s:failed!\n", __func__);
		return -1;
	}

	return 0;
}

/* Set the data flash class to be accessed.
 * Return negative code on failed, zero on success */
static int bq27410_ext_set_df_class(u8 subclass, struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_EXT_DFCLS];
	u8 buf[1] = { 0 };
	int ret = 0;

	buf[0] = subclass;
	ret = bq27410_write_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret < 0)
		pr_debug("%s:failed!\n", __func__);

	return ret;
}

/* Set the data flash block to be accessed.
 * Return negative code on failed, zero on success */
static int bq27410_ext_set_df_block(u8 block, struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_EXT_DFBLK];
	u8 buf[1] = { 0 };
	int ret = 0;

	buf[0] = block;
	ret = bq27410_write_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret < 0)
		pr_debug("%s:failed!\n", __func__);

	return ret;
}

#ifdef NOT_USED
/* Get checksum of 32 bytes block data */
static u8 bq27410_ext_get_checksum(u8 *data, struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info =
	    &cmds_info[BQ27410_EXT_DFDCKS];
	int ret = 0;

	ret = bq27410_read_reg(cmd_info->cmd, cmd_info->len, data, client);
	if (ret != cmd_info->len) {
		pr_debug("%s:failed!\n", __func__);
		return 0;
	}

	return ret;
}
#endif
/* Set checksum of 32 bytes block data */
static int bq27410_ext_set_checksum(u8 checksum, struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info =
	    &cmds_info[BQ27410_EXT_DFDCKS];
	u8 buf[1] = { 0 };
	int ret = 0;

	buf[0] = checksum;
	ret = bq27410_write_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret < 0)
		pr_debug("%s:failed!\n", __func__);

	return ret;
}

/* Set data flash access mode */
static int bq27410_ext_df_control(u8 mode, struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info =
	    &cmds_info[BQ27410_EXT_DFDCNTL];
	u8 buf[1] = { 0 };
	int ret = 0;

	buf[0] = mode;
	ret = bq27410_write_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret < 0)
		pr_debug("%s:failed!\n", __func__);

	return ret;
}

/* Get 32 bytes block data */
static int bq27410_ext_get_blockdata(u8 *data, struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_EXT_DFD];
	int ret = 0;

	ret = bq27410_read_reg(cmd_info->cmd, cmd_info->len, data, client);
	if (ret != cmd_info->len) {
		pr_debug("%s:failed!\n", __func__);
		return -1;
	}

	return 0;
}

/* Set data flash to new value */
static int bq27410_ext_set_df(u8 *data,
				const struct bq27410_data_flash_info *df_info,
				struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_EXT_DFD];
	int ret = 0;

	ret = bq27410_write_reg(cmd_info->cmd + df_info->offset,
				df_info->len, data, client);
	if (ret < 0)
		pr_debug("%s:failed!\n", __func__);

	return ret;
}

/**********************************************************************/
/* Set battery DesignCapacity */
static int bq27410_battery_set_design_capacity(u16 data,
				struct bq27410_device_info *di)
{
	const struct bq27410_data_flash_info *df_info =
	    &data_flash_info[BQ27410_DF_DCAP];
	u8 subclass, offset, block, check_sum;
	u8 cap[2] = { 0 }, buf[32] = { 0 };
	int sum, i, ret = 0;

	bq27410_ext_get_design_capacity(buf, di);
	ret = (buf[1] << 8) | buf[0];
	if (ret == data)
		return 0;

	/* NOTE: data in flash should be reversed */
	cap[0] = (data >> 8) & 0xff;	/* MSB */
	cap[1] = data & 0xff;	/* LSB */

	subclass = df_info->subclass;
	offset = df_info->offset;
	block = offset / 32;
	/* Step 1: Set data flash class */
	ret = bq27410_ext_set_df_class(subclass, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(20);
	/* Step 2: Set data flash block */
	ret = bq27410_ext_set_df_block(block, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(20);
	/* Step 3: Set BlockData() to access general data flash */
	ret = bq27410_ext_df_control(0x00, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(20);
	/* Step 4: Read 32 bytes block data */
	ret = bq27410_ext_get_blockdata(buf, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(20);
	/* Step 5: Calculate sum of block data */
	sum = i = 0;
	while (i < 32) {
		sum += buf[i];
		i++;
	}
	/* Step 6: Modify flash data */
	ret = bq27410_ext_set_df(cap, df_info, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(100);
	/* Step 7: Set new checksum to take effect */
	check_sum = 0xff - ((sum - buf[offset] - buf[offset + 1]
			     + cap[0] + cap[1]) & 0xff);
	ret = bq27410_ext_set_checksum(check_sum, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(100);
	/* Step 8: validate whether success or not */
	bq27410_ext_get_design_capacity(buf, di);
	ret = (buf[1] << 8) | buf[0];
	if (ret != data) {
		pr_debug("%s:line%d: set design capacity failed!\n",
			__func__, __LINE__);
		return -1;
	}

	return 0;
}

/* Set battery DesignEnergy */
static int bq27410_battery_set_design_energy(u16 data,
				struct bq27410_device_info *di)
{
	const struct bq27410_data_flash_info *df_info =
	    &data_flash_info[BQ27410_DF_DE];
	u8 subclass, offset, block, check_sum;
	u8 eg[2] = { 0 }, buf[32] = { 0 };
	int sum, i, ret = 0;

	/* NOTE: data in flash should be reversed */
	eg[0] = (data >> 8) & 0xff;	/* MSB */
	eg[1] = data & 0xff;	/* LSB */

	subclass = df_info->subclass;
	offset = df_info->offset;
	block = offset / 32;
	/* Step 1: Set data flash class */
	ret = bq27410_ext_set_df_class(subclass, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(20);
	/* Step 2: Set data flash block */
	ret = bq27410_ext_set_df_block(block, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(20);
	/* Step 3: Set BlockData() to access general data flash */
	ret = bq27410_ext_df_control(0x00, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(20);
	/* Step 4: Read 32 bytes block data */
	ret = bq27410_ext_get_blockdata(buf, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}

	/* if they're equal, just return */
	if (buf[offset] == eg[0] && buf[offset + 1] == eg[1])
		return 0;

	msleep(20);
	/* Step 5: Calculate sum of block */
	sum = i = 0;
	while (i < 32) {
		sum += buf[i];
		i++;
	}
	/* Step 6: Modify flash data */
	ret = bq27410_ext_set_df(eg, df_info, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(100);
	/* Step 7: Set new checksum to take effect */
	check_sum = 0xff - ((sum - buf[offset] - buf[offset + 1]
			     + eg[0] + eg[1]) & 0xff);
	ret = bq27410_ext_set_checksum(check_sum, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(100);
	/* Step 8: validate whether success or not */
	ret = bq27410_ext_get_blockdata(buf, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	if (((buf[offset] << 8) | (buf[offset + 1])) != data) {
		pr_debug("%s:line%d: set design energy failed!\n",
			__func__, __LINE__);
		return -1;
	}

	return 0;
}

/* Set battery OpConfiguration */
static int bq27410_battery_set_opconfig(u8 data,
				struct bq27410_device_info *di)
{
	const struct bq27410_data_flash_info *df_info =
	    &data_flash_info[BQ27410_DF_OPCFG];
	u8 subclass, offset, block, check_sum;
	u8 op[1] = { 0 }, buf[32] = { 0 };
	int sum, i, ret = 0;

	bq27410_ext_get_opconfig(buf, di);
	if (buf[0] == data)
		return 0;

	op[0] = data;

	subclass = df_info->subclass;
	offset = df_info->offset;
	block = offset / 32;
	/* Step 1: Set data flash class */
	ret = bq27410_ext_set_df_class(subclass, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(20);
	/* Step 2: Set data flash block */
	ret = bq27410_ext_set_df_block(block, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(20);
	/* Step 3: Set BlockData() to access general data flash */
	ret = bq27410_ext_df_control(0x00, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(20);
	/* Step 4: Read 32 bytes block data */
	ret = bq27410_ext_get_blockdata(buf, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(20);
	/* Step 5: Calculate sum of block data */
	sum = i = 0;
	while (i < 32) {
		sum += buf[i];
		i++;
	}
	/* Step 6: Modify flash data */
	ret = bq27410_ext_set_df(op, df_info, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(100);
	/* Step 7: Set new checksum to take effect */
	check_sum = 0xff - ((sum - buf[offset] + op[0]) & 0xff);
	ret = bq27410_ext_set_checksum(check_sum, di);
	if (ret < 0) {
		pr_debug("%s:line%d:failed!\n", __func__, __LINE__);
		return ret;
	}
	msleep(100);
	/* Step 8: validate whether success or not */
	bq27410_ext_get_opconfig(buf, di);
	if (buf[0] != data) {
		pr_debug("%s:line%d: set OpConfig failed!\n",
			__func__, __LINE__);
		return -1;
	}

	return 0;
}

/* APIs for reading battery information */

/* Return Flags */
static int bq27410_battery_flags(struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_FLAGS];
	u8 buf[2] = { 0 };
	int ret = 0;

	ret = bq27410_read_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret != cmd_info->len) {
		pr_debug("%s:failed!\n", __func__);
		return 0;
	}
	ret = (buf[1] << 8) | buf[0];

	return ret;
}

/* Return battery temperature in Centidegree{C} */
static int bq27410_battery_temperature(struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_ITEMP];
	u8 buf[2] = { 0 };
	int ret = 0;

	ret = bq27410_read_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret != cmd_info->len) {
		pr_debug("%s:failed!\n", __func__);
		return 0;
	}
	/* Unit: 0.1K */
	ret = (buf[1] << 8) | buf[0];
	/* Convert from Kelvin(K) to Centidegree(C) */
	ret = (ret / 10) - 273;

	return ret;
}

/* Return battery voltage in millivolt(mV) */
static int bq27410_battery_voltage(struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_VOLT];
	u8 buf[2] = { 0 };
	int ret = 0;

	ret = bq27410_read_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret != cmd_info->len) {
		pr_debug("%s:failed!\n", __func__);
		return 0;
	}
	ret = (buf[1] << 8) | buf[0];

	return ret;
}

/* Return battery average current in milliamp(mA) */
static int bq27410_battery_current(struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_AI];
	u8 buf[2] = { 0 };
	int ret = 0;

	ret = bq27410_read_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret != cmd_info->len) {
		pr_debug("%s:failed!\n", __func__);
		return 0;
	}

	if (buf[1] & 0x80) {
		/* negative value when discharging */
		ret = 0xFFFF - ((buf[1] << 8) | buf[0]);
		ret = 0 - ret;
	} else {
		/* positive value when charging */
		ret = (buf[1] << 8) | buf[0];
	}

	return ret;
}

/* Return battery capacity in percent(%) */
static int bq27410_battery_capacity(struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_SOC];
	u8 buf[2] = { 0 };
	int ret = 0;

	ret = bq27410_read_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret != cmd_info->len) {
		pr_debug("%s:failed!\n", __func__);
		return 0;
	}
	ret = (buf[1] << 8) | buf[0];

	return ret;
}

/* Return battery Remaining Capacity in mAh */
static int bq27410_battery_remaining_capacity(struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_RM];
	u8 buf[2] = { 0 };
	int ret = 0;

	ret = bq27410_read_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret != cmd_info->len) {
		pr_debug("%s:failed!\n", __func__);
		return 0;
	}
	ret = (buf[1] << 8) | buf[0];

	return ret;
}

/* Return battery FullCharge Capacity in mAh */
static int bq27410_battery_fullcharge_capacity(struct bq27410_device_info *di)
{
	struct i2c_client *client = di->client;
	const struct bq27410_cmd_info *cmd_info = &cmds_info[BQ27410_ST_FCC];
	u8 buf[2] = { 0 };
	int ret = 0;

	ret = bq27410_read_reg(cmd_info->cmd, cmd_info->len, buf, client);
	if (ret != cmd_info->len) {
		pr_debug("%s:failed!\n", __func__);
		return 0;
	}
	ret = (buf[1] << 8) | buf[0];

	return ret;
}

static int bq27410_battery_update_state(struct bq27410_device_info *di)
{
	int bat_changed = 0;
	/* update battery voltage */
	di->bat_params.volt = bq27410_battery_voltage(di);
	/* update current through battery */
	di->bat_params.cur = bq27410_battery_current(di);
	/* update battery capacity */
	di->bat_params.cap = bq27410_battery_capacity(di);
	/* update battery temperature */
	di->bat_params.temp = bq27410_battery_temperature(di);

	if (di->bat_params.volt > 100)
		di->bat_params.present = 1;
	else
		di->bat_params.present = 0;

	if (di->bat_params.cur > 0)
		di->bat_params.status = POWER_SUPPLY_STATUS_CHARGING;
	else if (di->bat_params.cur < 0)
		di->bat_params.status = POWER_SUPPLY_STATUS_DISCHARGING;
	else
		di->bat_params.status = POWER_SUPPLY_STATUS_NOT_CHARGING;

	di->bat_params.health = POWER_SUPPLY_HEALTH_GOOD;
	di->bat_params.tech = POWER_SUPPLY_TECHNOLOGY_LION;

	if ((di->bat_params.volt != di->bat_params.last_volt)
	    || (di->bat_params.cur != di->bat_params.last_cur)
	    || (di->bat_params.cap != di->bat_params.last_cap)
	    || (di->bat_params.temp != di->bat_params.last_temp)
	    || (di->bat_params.status != di->bat_params.last_status)
	    || (di->bat_params.present != di->bat_params.last_present)) {
		/* battery states changed */
		bat_changed = 1;

	}
	di->bat_params.last_volt = di->bat_params.volt;
	di->bat_params.last_cur = di->bat_params.cur;
	di->bat_params.last_cap = di->bat_params.cap;
	di->bat_params.last_temp = di->bat_params.temp;
	di->bat_params.last_status = di->bat_params.status;
	di->bat_params.last_present = di->bat_params.present;

	return bat_changed;
}

static void bq27410_battery_monitor(struct work_struct *work)
{
	struct bq27410_device_info *di;
	int bat_changed = 0;

	di = container_of((struct delayed_work *)work,
			  struct bq27410_device_info, monitor_work);

	bat_changed = bq27410_battery_update_state(di);
	/* if battery states changed, trigger uevent */
	if (bat_changed)
		power_supply_changed(&di->bat);

	/* reschedule for the next time */
	schedule_delayed_work(&di->monitor_work, di->interval);
}

static void bq27410_battery_state_init(struct bq27410_device_info *di)
{
	/* update battery voltage */
	di->bat_params.volt = bq27410_battery_voltage(di);
	/* update current through battery */
	di->bat_params.cur = bq27410_battery_current(di);
	/* update battery capacity */
	di->bat_params.cap = bq27410_battery_capacity(di);
	/* update battery temperature */
	di->bat_params.temp = bq27410_battery_temperature(di);

	if (di->bat_params.volt > 100)
		di->bat_params.present = 1;
	else
		di->bat_params.present = 0;

	if (di->bat_params.cur > 0)
		di->bat_params.status = POWER_SUPPLY_STATUS_CHARGING;
	else if (di->bat_params.cur < 0)
		di->bat_params.status = POWER_SUPPLY_STATUS_DISCHARGING;
	else
		di->bat_params.status = POWER_SUPPLY_STATUS_NOT_CHARGING;

	di->bat_params.health = POWER_SUPPLY_HEALTH_GOOD;
	di->bat_params.tech = POWER_SUPPLY_TECHNOLOGY_LION;
	/* init last states parameters */
	di->bat_params.last_volt = di->bat_params.volt;
	di->bat_params.last_cur = di->bat_params.cur;
	di->bat_params.last_cap = di->bat_params.cap;
	di->bat_params.last_temp = di->bat_params.temp;
	di->bat_params.last_status = di->bat_params.status;
	di->bat_params.last_present = di->bat_params.present;
}

#define to_bq27410_device_info(x) container_of((x), \
				struct bq27410_device_info, bat);

static int bq27410_battery_get_property(struct power_supply *psy,
					enum power_supply_property psp,
					union power_supply_propval *val)
{
	struct bq27410_device_info *di = to_bq27410_device_info(psy);

	switch (psp) {
	case POWER_SUPPLY_PROP_PRESENT:
		val->intval = di->bat_params.present;
		break;
	case POWER_SUPPLY_PROP_STATUS:
		val->intval = di->bat_params.status;
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
		val->intval = di->bat_params.volt;
		break;
	case POWER_SUPPLY_PROP_CURRENT_NOW:
		val->intval = di->bat_params.cur;
		break;
	case POWER_SUPPLY_PROP_CAPACITY:
		val->intval = di->bat_params.cap;
		break;
	case POWER_SUPPLY_PROP_TEMP:
		val->intval = di->bat_params.temp;
		break;
	case POWER_SUPPLY_PROP_HEALTH:
		val->intval = di->bat_params.health;
		break;
	case POWER_SUPPLY_PROP_TECHNOLOGY:
		val->intval = di->bat_params.tech;
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

static int bq27410_powersupply_init(struct bq27410_device_info *di)
{
	int ret = 0;

	di->bat.type = POWER_SUPPLY_TYPE_BATTERY;
	di->bat.properties = bq27410_battery_props;
	di->bat.num_properties = ARRAY_SIZE(bq27410_battery_props);
	di->bat.get_property = bq27410_battery_get_property;
	di->bat.external_power_changed = NULL;

	ret = power_supply_register(di->dev, &di->bat);

	return ret;
}

static struct proc_dir_entry *bq27410_dump;
static int bq27410_proc_write(struct file *file, const char *buffer,
				unsigned long count, void *data)
{
	u8 kbuf[8], buf[2] = { 0 };
	struct bq27410_device_info *di = (struct bq27410_device_info *)data;
	unsigned long index = 0;
	int ret = 0;

	if (count >= 8)
		return -EINVAL;
	if (copy_from_user(kbuf, buffer, count))
		return -EFAULT;
	ret = strict_strtoul(kbuf, 10, &index);
	if (ret < 0)
		return -EINVAL;

	switch (index) {
	case 1:
		ret = bq27410_battery_capacity(di);
		pr_info("bq27410: Capacity: %d%%\n", ret);
		ret = bq27410_battery_voltage(di);
		pr_info("bq27410: Voltage: %dmV\n", ret);
		ret = bq27410_battery_current(di);
		pr_info("bq27410: Current: %dmA\n", ret);
		ret = bq27410_battery_temperature(di);
		pr_info("bq27410: Temperature: %dC\n", ret);
		ret = bq27410_battery_flags(di);
		pr_info("bq27410: Flags: 0x%x\n", ret);

		ret = bq27410_battery_remaining_capacity(di);
		pr_info("bq27410: Remaining Capacity: %dmAh\n", ret);
		ret = bq27410_battery_fullcharge_capacity(di);
		pr_info("bq27410: FullCharge Capacity: %dmAh\n", ret);
		/* Fall Through */
	case 2:
		bq27410_ext_get_opconfig(buf, di);
		pr_info("bq27410: OpCfg: 0x%x\n", buf[0]);
		bq27410_ext_get_design_capacity(buf, di);
		ret = (buf[1] << 8) | buf[0];
		pr_info("bq27410: DesignCapacity: 0x%x(%dmAh)\n", ret, ret);
		/* Fall Through */
	case 3:
		bq27410_control_get_status(buf, di);
		ret = (buf[1] << 8) | buf[0];
		pr_info("bq27410: ControlStatus: 0x%x\n", ret);
		break;
	case 4:
		bq27410_control_bat_insert(di);
		pr_info("bq27410: Force BAT_DET to 1.\n");
		break;
	case 5:
		bq27410_control_bat_remove(di);
		pr_info("bq27410: Force BAT_DET to 0.\n");
		break;
	case 6:
		bq27410_control_sealed(di);
		pr_info("bq27410: Place chip in SEALED access mode.\n");
		break;
	case 7:
		bq27410_control_unsealed(di);
		pr_info("bq27410: Place chip in UNSEALED access mode.\n");
		break;
	case 8:
		bq27410_control_reset(di);
		pr_info("bq27410: Force a full reset.\n");
		break;
	default:
		break;
	}

	return count;
}

static int bq27410_proc_read(char *buffer, char **buffer_location,
				off_t offset, int buffer_length,
				int *zero, void *ptr)
{
	if (offset > 0)
		return 0;
	return sprintf(buffer, "%s\n", __func__);
}

static void bq27410_battery_setup(struct bq27410_device_info *di)
{
	u8 buf[2] = { 0 };

	bq27410_control_get_status(buf, di);
	if (buf[1] & (1 << 5)) {
		bq27410_control_unsealed(di);
		msleep(20);
	}

	bq27410_ext_get_opconfig(buf, di);
	if (buf[0] != BQ27410_BAT_OPCFG)
		bq27410_battery_set_opconfig(BQ27410_BAT_OPCFG, di);

	/* Set Design Capacity */
	bq27410_battery_set_design_capacity(BQ27410_BAT_DCAP, di);
	/* Set Design Energy */
	bq27410_battery_set_design_energy(BQ27410_BAT_DE, di);

	/* Force BAT_DET bit in Flags to 1, enable battery monitor */
	bq27410_control_bat_insert(di);
}

static int bq27410_battery_probe(struct i2c_client *client,
				 const struct i2c_device_id *id)
{
	struct bq27410_device_info *di;
	int ret = 0;

	di = kzalloc(sizeof(*di), GFP_KERNEL);
	if (!di) {
		dev_err(&client->dev, "failed to allocate device info data\n");
		return -ENOMEM;
	}

	i2c_set_clientdata(client, di);
	di->client = client;
	di->dev = &client->dev;
	di->bat.name = "bq27410-battery";

	bq27410_battery_setup(di);
	msleep(10);
	bq27410_battery_state_init(di);

	ret = bq27410_powersupply_init(di);
	if (ret) {
		dev_err(&client->dev, "failed to register battery\n");
		goto err_power_supply_reg;
	}

	/* Init battery monitor periodic work queue */
	di->interval = msecs_to_jiffies(500);
	INIT_DELAYED_WORK(&di->monitor_work, bq27410_battery_monitor);
	/* Start monitor work */
	schedule_delayed_work(&di->monitor_work, msecs_to_jiffies(1000));

	bq27410_dump = create_proc_entry("bq27410_battery", 0666, NULL);
	if (bq27410_dump) {
		bq27410_dump->read_proc = bq27410_proc_read;
		bq27410_dump->write_proc = bq27410_proc_write;
		bq27410_dump->data = di;
	} else
		dev_err(&client->dev, "failed to create proc entry\n");

	return 0;

err_power_supply_reg:
	kfree(di);

	return ret;
}

static int bq27410_battery_remove(struct i2c_client *client)
{
	struct bq27410_device_info *di = i2c_get_clientdata(client);

	power_supply_unregister(&di->bat);
	cancel_delayed_work_sync(&di->monitor_work);

	kfree(di);

	return 0;
}

static const struct i2c_device_id bq27410_id[] = {
	{"bq27410", -1},
	{},
};

static struct i2c_driver bq27410_battery_driver = {
	.driver = {
		.name = "bq27410-battery",
	},
	.probe = bq27410_battery_probe,
	.remove = bq27410_battery_remove,
	.id_table = bq27410_id,
};

static int __init bq27410_battery_init(void)
{
	int ret;

	ret = i2c_add_driver(&bq27410_battery_driver);
	if (ret)
		pr_err("Unable to register BQ27410 driver\n");

	return ret;
}

module_init(bq27410_battery_init);

static void __exit bq27410_battery_exit(void)
{
	i2c_del_driver(&bq27410_battery_driver);
}

module_exit(bq27410_battery_exit);

MODULE_AUTHOR("Yunfan Zhang <yfzhang@marvell.com>");
MODULE_DESCRIPTION("BQ27410 battery driver");
MODULE_LICENSE("GPL");
