/*
 * ADS1000 driver
 *
 * Copyright (C) 2008 Rodolfo Giometti <giometti@linux.it>
 * Copyright (C) 2008 Eurotech S.p.A. <info@eurotech.it>
 *
 * Based on a previous work by Copyright (C) 2008 Texas Instruments, Inc.
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * THIS PACKAGE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
#define DEBUG

#include <linux/module.h>
#include <linux/param.h>
#include <linux/jiffies.h>
#include <linux/workqueue.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/power_supply.h>
#include <linux/idr.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <asm/unaligned.h>
#include <linux/irq.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/power/ads1000.h>

#define DRIVER_VERSION			"1.1.0"
/* revisit */
#define FULL_BATTERY_VOLTAGE		4200
#define ZERO_BATTERY_VOLTAGE		3700
#define VDD_VOLTAGE 3000
#define MAX_FULL_COUNT 6
#define BATTERY_MONITOR_INTERVAL 1 /* monitor interval in seconds */

static DEFINE_IDR(battery_id);
static DEFINE_MUTEX(battery_mutex);

struct ads1000_device_info;
struct ads1000_access_methods {
	int (*read)(uint8_t *buf, int len, struct ads1000_device_info *di);
	int (*write)(uint8_t *buf, int len, struct ads1000_device_info *di);
};

struct ads1000_device_info {
	struct device		*dev;
	int			id;
	struct ads1000_access_methods	*bus;
	struct power_supply	bat;
	struct i2c_client	*client;
	struct work_struct	dc_in_work;
	struct delayed_work	monitor_work;
	unsigned int		interval;
	int			cur_volt;
	int			batt_status;
	int			full_count;
	int			irq;
	int			dc_in;
	int			vdd;
	int 		gpio_dc_in;
};

static enum power_supply_property ads1000_battery_props[] = {
	POWER_SUPPLY_PROP_STATUS,
	POWER_SUPPLY_PROP_PRESENT,
	POWER_SUPPLY_PROP_VOLTAGE_NOW,
	POWER_SUPPLY_PROP_CAPACITY,
};

/* Return the voltage in milivolts */
static int calculate_voltage(int out_code, int vdd)
{
	int val;
	/* output code = 2048(PGA)((VIN+ - VIN-)/VDD)
	 * For G50 platform, VIN- is zero, PGA is set to 1
	 */
	val = out_code * vdd / 2048;

	val *= 3;
	val /= 2;

	return val;;
}

/*
 * Return the battery Voltage in milivolts
 * Or < 0 if something fails.
 */
static int ads1000_battery_voltage(struct ads1000_device_info *di)
{
	int ret;
	int volt = 0;
	uint8_t buf[3];
	int out_code;

	ret = di->bus->read(buf, 3, di);
	if (ret) {
		dev_err(di->dev, "error reading voltage\n");
		return ret;
	}

	out_code = (buf[0] << 8) | buf[1];
	volt = calculate_voltage(out_code, di->vdd);

	return volt;
}

#define to_ads1000_device_info(x) container_of((x), \
				struct ads1000_device_info, bat);

static int ads1000_battery_get_property(struct power_supply *psy,
					enum power_supply_property psp,
					union power_supply_propval *val)
{
	int ret = 0;
	struct ads1000_device_info *di = to_ads1000_device_info(psy);

	switch (psp) {
	case POWER_SUPPLY_PROP_STATUS:
		/* the status of battery: charging, discharging, full
		 * the di->batt_status is update every di->interval seconds
		 */
		val->intval = di->batt_status;
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
	case POWER_SUPPLY_PROP_PRESENT:
		val->intval = di->cur_volt;
		if (psp == POWER_SUPPLY_PROP_PRESENT)
			val->intval = val->intval <= 0 ? 0 : 1;
		break;
	case POWER_SUPPLY_PROP_CAPACITY:
		if (di->cur_volt >= FULL_BATTERY_VOLTAGE)
			val->intval = 100;
        else if (di->cur_volt <= ZERO_BATTERY_VOLTAGE)
            val->intval = 0;
		else
			val->intval = (di->cur_volt-ZERO_BATTERY_VOLTAGE) * 100 / (FULL_BATTERY_VOLTAGE-ZERO_BATTERY_VOLTAGE);
		break;
	default:
		return -EINVAL;
	}

	return ret;
}

static void ads1000_powersupply_init(struct ads1000_device_info *di)
{
	di->bat.type = POWER_SUPPLY_TYPE_BATTERY;
	di->bat.properties = ads1000_battery_props;
	di->bat.num_properties = ARRAY_SIZE(ads1000_battery_props);
	di->bat.get_property = ads1000_battery_get_property;
	di->bat.external_power_changed = NULL;
}

/*
 * i2c specific code
 */

static int ads1000_read_i2c(uint8_t *buf, int len,
		struct ads1000_device_info *di)
{
	int status;
	int ret;
	struct i2c_client *client = di->client;

	if (!client->adapter)
		return -ENODEV;

	if (!buf)
		return -EINVAL;

	ret = i2c_master_recv(client, buf, len);
	if (ret >= 0)
		status = 0;
	else
		status = -EIO;

	return status;
}

static int ads1000_write_i2c(uint8_t *buf, int len,
		struct ads1000_device_info *di)
{
	int status;
	int ret;
	struct i2c_client *client = di->client;

	if (!client->adapter)
		return -ENODEV;

	if (len != 1) {
		printk(KERN_ALERT "the write length(%d) is invalid\n", len);
		return -EINVAL;
	}

	ret = i2c_master_send(client, buf, 1);
	if (ret >= 0)
		status = 0;
	else
		status = -EIO;

	return status;
}

static void ads1000_battery_check_state(struct ads1000_device_info *di)
{
	int volt = ads1000_battery_voltage(di);

	if (di->dc_in) {
		if (volt >= FULL_BATTERY_VOLTAGE) {
			if (di->full_count <= MAX_FULL_COUNT)
				di->full_count++;
		} else {
			di->full_count = 0;
			di->batt_status = POWER_SUPPLY_STATUS_CHARGING;
		}

		/* when charger is on, the voltage increases or not. But it is
		 * impossible to decrease, should remove the burr
		 */
		if (volt > FULL_BATTERY_VOLTAGE)
			volt = FULL_BATTERY_VOLTAGE;
		if (volt > di->cur_volt)
			di->cur_volt = volt;

		/* when charger is on, if the voltage of battery is alway equal
		 * or more than full voltage during MAX_FULL_COUNT*di->interval,
		 * conside it as the battery is full. else charging
		 */
		if (di->full_count > MAX_FULL_COUNT)
			di->batt_status = POWER_SUPPLY_STATUS_FULL;
		else
			di->batt_status = POWER_SUPPLY_STATUS_CHARGING;
	} else {
		di->full_count = 0; /* reset the full count */
		/* when no charging, the voltage decreases, remove the burr */
		if (volt < di->cur_volt)
			di->cur_volt = volt;
		if (di->cur_volt >= FULL_BATTERY_VOLTAGE)
			di->batt_status = POWER_SUPPLY_STATUS_FULL;
		else
			di->batt_status = POWER_SUPPLY_STATUS_NOT_CHARGING;
	}

	power_supply_changed(&di->bat);
	return;
}

static void ads1000_battery_monitor(struct work_struct *work)
{
	struct ads1000_device_info *di;

	di = container_of((struct delayed_work *)work,
			struct ads1000_device_info,
			monitor_work);

	ads1000_battery_check_state(di);

	/* reschedule for the next time */
	schedule_delayed_work(&di->monitor_work, di->interval);
}

static void dc_in_check_worker(struct work_struct *work)
{
	struct ads1000_device_info *di;

	di = container_of(work, struct ads1000_device_info, dc_in_work);
	ads1000_battery_check_state(di);
}

static irqreturn_t charger_DC_IN_detect(int irq, void *dev_id)
{
	struct ads1000_device_info *di = (struct ads1000_device_info *)dev_id;

	di->dc_in = !!gpio_get_value(di->gpio_dc_in);

	printk(KERN_INFO "dc_in: %d\n", di->dc_in);

	schedule_work(&di->dc_in_work);

	return IRQ_HANDLED;
}

static ssize_t dc_in_show(struct device *dev, struct device_attribute *attr,
		char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct ads1000_device_info *di = i2c_get_clientdata(client);
	ssize_t ret = 0;

	sprintf(buf, "dc in: %d\n",
			!!gpio_get_value(di->gpio_dc_in));
	ret = strlen(buf) + 1;


	return ret;
}

static DEVICE_ATTR(dc_in, S_IRUGO, dc_in_show, NULL);

static int ads1000_battery_probe(struct i2c_client *client,
				 const struct i2c_device_id *id)
{
	struct ads1000_platform_data* pdata = client->dev.platform_data;
	char *name;
	struct ads1000_device_info *di;
	struct ads1000_access_methods *bus;
	int num,gpio;
	int retval = 0;
	uint8_t buf[3] = {0};

	if(!pdata) return -ENODEV;
	gpio = pdata->gpio_dc_in;

	/* Get new ID for the new battery device */
	retval = idr_pre_get(&battery_id, GFP_KERNEL);
	if (retval == 0)
		return -ENOMEM;
	mutex_lock(&battery_mutex);
	retval = idr_get_new(&battery_id, client, &num);
	mutex_unlock(&battery_mutex);
	if (retval < 0)
		return retval;

	name = kasprintf(GFP_KERNEL, "%s-%d", id->name, num);
	if (!name) {
		dev_err(&client->dev, "failed to allocate device name\n");
		retval = -ENOMEM;
		goto batt_failed_1;
	}

	di = kzalloc(sizeof(*di), GFP_KERNEL);
	if (!di) {
		dev_err(&client->dev, "failed to allocate device info data\n");
		retval = -ENOMEM;
		goto batt_failed_2;
	}
	di->id = num;

	bus = kzalloc(sizeof(*bus), GFP_KERNEL);
	if (!bus) {
		dev_err(&client->dev, "failed to allocate access method "
					"data\n");
		retval = -ENOMEM;
		goto batt_failed_3;
	}

	i2c_set_clientdata(client, di);
	di->dev = &client->dev;
	di->bat.name = name;
	bus->read = &ads1000_read_i2c;
	bus->write = &ads1000_write_i2c;
	di->bus = bus;
	di->client = client;
	di->dc_in = !!gpio_get_value(gpio);
	di->gpio_dc_in = gpio;

	/* set ads1000 in contiuous convertion mode
	 * set the PGA to zero
	 */
	retval = ads1000_read_i2c(buf, 3, di);
	if (retval < 0) {
		dev_err(&client->dev, "failed to read ads1000\n");
		retval = -EIO;
		goto batt_failed_3;
	}

	buf[2] &= ~(1 << 4);
	buf[2] &= ~0x3;
	buf[0] = buf[2];
	retval = ads1000_write_i2c(buf, 1, di);
	if (retval < 0) {
		dev_err(&client->dev, "failed to configure ads1000\n");
		retval = -EIO;
		goto batt_failed_3;
	}
	msleep(100);

	ads1000_powersupply_init(di);

	di->batt_status = POWER_SUPPLY_STATUS_FULL;
	di->vdd = VDD_VOLTAGE;
	di->cur_volt = ads1000_battery_voltage(di);
	di->full_count = 0;

	/* 10 seconds between monitor runs */
	if(pdata->monitor_interval_ms)
		di->interval = msecs_to_jiffies(pdata->monitor_interval_ms);
	else
		di->interval = msecs_to_jiffies(BATTERY_MONITOR_INTERVAL * 1000);
	INIT_DELAYED_WORK(&di->monitor_work, ads1000_battery_monitor);
	/* update the battery status after 1 second for the first time */
	schedule_delayed_work(&di->monitor_work, msecs_to_jiffies(1));

	retval = power_supply_register(&client->dev, &di->bat);
	if (retval) {
		dev_err(&client->dev, "failed to register battery\n");
		goto batt_failed_4;
	}

	INIT_WORK(&di->dc_in_work, dc_in_check_worker);

	di->irq = gpio_to_irq(gpio);
	retval = request_irq(di->irq, charger_DC_IN_detect,
			IRQ_DISABLED |
			IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING,
			"g50 DC IN", di);
	if (retval < 0) {
		dev_err(&client->dev, "unable to request IRQ\n");
		retval = -ENXIO;
		goto batt_failed_request_irq;
	}

	retval = device_create_file(&client->dev, &dev_attr_dc_in);
	if (retval) {
		dev_err(&client->dev, "device_create_file failed\n");
		goto batt_failed_sysfs;
	}

	dev_info(&client->dev, "support ver. %s enabled\n", DRIVER_VERSION);

	return 0;

batt_failed_sysfs:
	free_irq(di->irq, di);
batt_failed_request_irq:
	power_supply_unregister(&di->bat);
batt_failed_4:
	cancel_delayed_work_sync(&di->monitor_work);
	kfree(bus);
batt_failed_3:
	kfree(di);
batt_failed_2:
	kfree(name);
batt_failed_1:
	mutex_lock(&battery_mutex);
	idr_remove(&battery_id, num);
	mutex_unlock(&battery_mutex);

	return retval;
}

static int ads1000_battery_remove(struct i2c_client *client)
{
	struct ads1000_device_info *di = i2c_get_clientdata(client);
	device_remove_file(&client->dev, &dev_attr_dc_in);

	free_irq(di->irq, di);
	cancel_delayed_work_sync(&di->monitor_work);
	power_supply_unregister(&di->bat);

	kfree(di->bat.name);

	mutex_lock(&battery_mutex);
	idr_remove(&battery_id, di->id);
	mutex_unlock(&battery_mutex);

	kfree(di->bus);

	kfree(di);

	return 0;
}

static const struct i2c_device_id ads1000_id[] = {
	{ "ads1000", -1},
	{ "ads1000_bd1",-1},
	{},
};

static struct i2c_driver ads1000_driver = {
	.driver = {
		.name = "ads1000",
	},
	.probe = ads1000_battery_probe,
	.remove = ads1000_battery_remove,
	.id_table = ads1000_id,
};

static int __init ads1000_init(void)
{
	int ret;

	ret = i2c_add_driver(&ads1000_driver);
	if (ret)
		printk(KERN_ERR "Unable to register ADS1000 driver\n");

	return ret;
}
module_init(ads1000_init);

static void __exit ads1000_exit(void)
{
	i2c_del_driver(&ads1000_driver);
}
module_exit(ads1000_exit);

MODULE_DESCRIPTION("ADS1000 driver");
MODULE_LICENSE("GPL");
