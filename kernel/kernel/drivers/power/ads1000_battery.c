/*
 * Ads1000 Battery Driver
 * Battery with A/D convert chip ads1000(I2C)
 *
 * Copyright (c) 2011 Marvell Technology Ltd.
 * Yunfan Zhang <yfzhang@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <linux/module.h>
#include <linux/param.h>
#include <linux/jiffies.h>
#include <linux/workqueue.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/power_supply.h>
#include <linux/idr.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <asm/unaligned.h>
#include <linux/irq.h>
#include <linux/gpio.h>
#include <plat/mfp.h>
#include <mach/mfp-mmp2.h>
#include <linux/interrupt.h>
#include <linux/power/ads1000.h>

#include <linux/timer.h>
#include <linux/timex.h>
#include <linux/rtc.h>
#include <linux/fs.h>
#include <asm/unistd.h>

#define ads_dbg(fmt, arg...) do { if (debug) printk(fmt, ##arg); } while(0)

struct ads1000_device_info;
struct ads1000_access_methods {
	int (*read)(uint8_t *buf, int len, struct ads1000_device_info *di);
	int (*write)(uint8_t *buf, int len, struct ads1000_device_info *di);
};

struct ads1000_device_info {
	struct device *dev;
	struct ads1000_access_methods *bus;
	struct power_supply bat;
	struct i2c_client *client;
	struct delayed_work dc_in_work;
	struct delayed_work monitor_work;
	unsigned int interval;
	/* battery parameters */
	int bat_volt;
	int bat_cap;
	int bat_status;
	int bat_present;
	int conv_mode;		/* conversion mode: 1: single, 0: continuous */
	int dc_in_st;		/* DC_IN status: DC online: 1, offline: 0 */
	int vdd;
	int irq;
	/* GPIOs: DC_IN, STAT1 and STAT2 */
	int dc_in;
	int bat_full;

	/* 
	 * The jiffies at last battery capacity, that's to say, it will reset when capacity increases.
	 * If the time since last jiffies exceeds a max value (CHARGE_THRESHOLD), capacity is set to 100%
	 */
	int last_jiffies;
	int sample_last;
};

/* Charging status indicated by GPIO STAT1&STAT2 */
enum charge_states {
	CHARGE_STAT_CHARGING,	/* Precharge or fast charge in progress */
	CHARGE_STAT_CHG_COMP,	/* Charge Complete */
	CHARGE_STAT_FAULT,	/* Fault */
	CHARGE_STAT_SUSPEND,	/* Suspend */
};

static enum power_supply_property ads1000_battery_props[] = {
	POWER_SUPPLY_PROP_STATUS,
	POWER_SUPPLY_PROP_PRESENT,
	POWER_SUPPLY_PROP_ONLINE,
	POWER_SUPPLY_PROP_VOLTAGE_NOW,
	POWER_SUPPLY_PROP_CAPACITY,
	POWER_SUPPLY_PROP_HEALTH,
	POWER_SUPPLY_PROP_TECHNOLOGY,
};

#define VDD_VOLTAGE	(2940)
#define VALID_VOLT(v) ((v)>=3000&&(v)<=5000)
static int debug = 0;
static char debug_real[128]="";

struct capacity_percent {
	int voltage;
	int percent;
};
/* Battery Capacity Table: Voltage vs Capacity */
static struct capacity_percent charge_table[] = {
#if defined(CONFIG_P200)
	{3632, 0},
	{3700, 9},
	{3746, 13},
	{3853, 20},
	{3892, 27},
	{3907, 36},		
	{3917, 44},		
	{3949, 55},		
	{3976, 61},		
	{4006, 70},		
	{4051, 79},		
	{4097, 88},
	{4139, 93},
	{4190, 97},
/*	
	{3700, 1},
	{3760, 15},
	{3808, 24},
	{3850, 34},
	{3874, 40},
	{3958, 50},             
	{4051, 79},             
	{4097, 82},
	{4124, 93},
	{4150, 97},
*/
#else
#error Battery charge capacity table not defined!
#endif
};
static struct capacity_percent discharge_table[] = {
#if defined(CONFIG_P200)
	{3400, 0},
	{3500, 5},
	{3600, 15},
	{3650, 35},
	{3740, 60},		
	{3870, 80},		
	{4060, 99},
#else
#error Battery discharge capacity table not defined!
#endif
};

//for calibration the capacity display
int bat_cap[30]={0};
int store_count=0;
int full=0;
int resume_count=0;
int resume_flag = 0;
int charge_base=0;
int using_count=0;
int suspend_cap=0;
int bootup_flag=1;
int first_base=1;
int record_suspend_time=0;
unsigned long system_time=0;
unsigned long suspend_time=0;
/* global device info for ads1000 */
static struct ads1000_device_info *g_di;
static int get_bat_capacity(int volt, struct ads1000_device_info *di);


/* Return the voltage in milivolts */
static int calculate_voltage(int out_code, struct ads1000_device_info *di)
{
	int val = 0;
	int vdd = di->vdd;
	int valid = 0;

	/* Output Code = 2048(PGA)((VIN+ - VIN-)/VDD)
	 * For G50 platform:
	 * VIN- = 0, PGA = 1, voltage divider: 2/3
	 */
	val = out_code * vdd / 2048;
	val *= 3;
	val /= 2;

	if (!VALID_VOLT(val))
		return 0;

	/*
	 * possible sample sequence:
	 * (a) 4069, 3882, dc_in(0), 3882, 3892, 3898, 3903, dc_in(1), 3993, 4069, 4069, 4069, ...
	 * (b) 3610, 3610, dc_in(0), 3318, 3357, 3352, 3348, ..., 3330, dc_in(1), 3759, 3592, 3592, ...
	 */
	valid = 0;
	/*detect by ads val should be add 55mV and will be equle to detct by Multimeter*/
	val += 55;
	
	if (di->sample_last == -1) valid = 1;
	if (abs(val - di->sample_last) <= 5) valid = 1;
	di->sample_last = val;
	di->bat_volt = val;

	//ads_dbg("sample: %d, correction: %d\n", val, offset);
	sprintf(debug_real,"sample:%4.4d\tcapacity:%2.2d%%\t",val,get_bat_capacity(val,di));
	return val;
}

/* Return capacity of battery, in percent */
static int get_bat_capacity(int volt, struct ads1000_device_info *di)
{
	int i, size;
	int p1,p2,v1,v2;
	int ret = 0;
	if(di->dc_in_st == 1)//charge
	{
		size = ARRAY_SIZE(charge_table);
		if (volt >= charge_table[size-1].voltage) {
			ret = 99;
		return ret;
		} else if (volt <= charge_table[0].voltage) {
			ret = 0;
		return ret;
		}
		for (i = size - 2; i >= 0; i--) {
			p1 = charge_table[i+1].percent;
			p2 = charge_table[i+0].percent;
			v1 = charge_table[i+1].voltage;
			v2 = charge_table[i+0].voltage;
			if (volt < charge_table[i].voltage)
				continue;
			
			ret = (p1-p2)*(volt-v2)/(v1-v2)+p2;
			if(ret<25&&first_base)
			{
				charge_base = ret;
				first_base=0;
			}
			
			break;
		}
	}
	else//discharge
	{
		charge_base = 0;
		first_base=1;
		size = ARRAY_SIZE(discharge_table);
		if (volt >= discharge_table[size-1].voltage) {
			ret = 99;
		return ret;
		} else if (volt <= discharge_table[0].voltage) {
			ret = 0;
		return ret;
		}
		for (i = size - 2; i >= 0; i--) {
			p1 = discharge_table[i+1].percent;
			p2 = discharge_table[i+0].percent;
			v1 = discharge_table[i+1].voltage;
			v2 = discharge_table[i+0].voltage;
			if (volt < discharge_table[i].voltage)
				continue;
			
			ret = (p1-p2)*(volt-v2)/(v1-v2)+p2;
				break;
		}
	}	
	if(ret<0)
		ret = 0;
	else if(ret>=100)
		ret = 100;
   	return ret;
}

/*
 * Return the battery Voltage in milivolts
 * Or < 0 if something fails.
 */
static int get_bat_voltage(struct ads1000_device_info *di)
{
	int ret = 0, volt = 0;
	uint8_t cfg_reg, buf[3];
	int time_out = 100;
	int out_code;

	if (di->conv_mode) {	/* single mode */
		/* Using single conversion mode to save power */
		ret = di->bus->read(buf, 3, di);
		if (ret)
			goto err;
		cfg_reg = buf[2];
		cfg_reg |= (1 << 7);
		/* Start a conversion */
		ret = di->bus->write(&cfg_reg, 1, di);
		if (ret)
			goto err;
		/* Wait conversion complete */
		while (time_out >= 0) {
			ret = di->bus->read(buf, 3, di);
			if (ret)
				goto err;
			/* check ST/BSY bit */
			if (!(buf[2] & (1 << 7)))
				break;
			time_out--;
			msleep(1);
		}
		if (time_out < 0) {
			pr_debug("ADC conversion timeout\n");
			goto err;
		}
	} else {		/* continuous mode */
		ret = di->bus->read(buf, 2, di);
		if (ret)
			goto err;
	}
	out_code = (buf[0] << 8) | buf[1];

	volt = calculate_voltage(out_code, di);

	return volt;
err:	/* error: return voltage zero */
	pr_debug("get battery voltage failed\n");
	return 0;
}

#define to_ads1000_device_info(x) container_of((x), \
				struct ads1000_device_info, bat);

static int ads1000_battery_get_property(struct power_supply *psy,
					enum power_supply_property psp,
					union power_supply_propval *val)
{
	int ret = 0;
	struct ads1000_device_info *di = to_ads1000_device_info(psy);

	switch (psp) {
	case POWER_SUPPLY_PROP_STATUS:
		val->intval = di->bat_status;
		ret = 0;
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
		val->intval = di->bat_volt;
		ret = 0;
		break;
	case POWER_SUPPLY_PROP_PRESENT:
		val->intval = di->bat_present;
		ret = 0;
		break;
	case POWER_SUPPLY_PROP_ONLINE:
		val->intval = di->dc_in_st;
		ret = 0; 
		break;
	case POWER_SUPPLY_PROP_CAPACITY:
		val->intval = di->bat_cap;
		ret = 0;
		break;
	case POWER_SUPPLY_PROP_HEALTH:
		val->intval = POWER_SUPPLY_HEALTH_GOOD;
		ret = 0;
		break;
	case POWER_SUPPLY_PROP_TECHNOLOGY:
		val->intval = POWER_SUPPLY_TECHNOLOGY_LION;
		ret = 0;
		break;
	default:
		return -EINVAL;
	}

	return ret;
}

static int ads1000_power_supply_init(struct ads1000_device_info *di)
{
	int ret = 0;

	di->bat.type = POWER_SUPPLY_TYPE_BATTERY;
	di->bat.properties = ads1000_battery_props;
	di->bat.num_properties = ARRAY_SIZE(ads1000_battery_props);
	di->bat.get_property = ads1000_battery_get_property;
	di->bat.external_power_changed = NULL;
	di->bat_volt = -1;

	di->sample_last = -1;

	ret = power_supply_register(di->dev, &di->bat);

	return ret;
}

/*
 * i2c specific code
 */

static int ads1000_read_i2c(uint8_t *buf, int len,
			    struct ads1000_device_info *di)
{
	int status;
	int ret;
	struct i2c_client *client = di->client;

	/* Reading from ads1000:
	 * First two bytes: output register contents;
	 * Third byte: configuration register contents.
	 */
	if (!client->adapter)
		return -ENODEV;

	if (!buf)
		return -EINVAL;

	ret = i2c_master_recv(client, buf, len);
	if (ret >= 0)
		status = 0;
	else
		status = -EIO;

	return status;
}

static int ads1000_write_i2c(uint8_t *buf, int len,
			     struct ads1000_device_info *di)
{
	int status;
	int ret;
	struct i2c_client *client = di->client;

	if (!client->adapter)
		return -ENODEV;

	if (len != 1) {
		printk(KERN_ALERT "the write length(%d) is invalid\n", len);
		return -EINVAL;
	}

	ret = i2c_master_send(client, buf, 1);
	if (ret >= 0)
		status = 0;
	else
		status = -EIO;

	return status;
}
#ifdef CONFIG_P200

#define ADS1000_INFO_PATH "/data/data/ads1000_battery_info"
enum ADS1000_INFO_STORE{
	READ_SHUT_CAP,
	WRITE_SHUT_CAP,
	READ_SUSPEND_TIME,
	GET_SYSTEM_TIME,
	READ_SUSPEND_CAP,
	WRITE_SUSPEND_CAP
};
static int ads1000_battery_info_store(struct ads1000_device_info *di,
											enum ADS1000_INFO_STORE action)
{
	static char buf[128];
	ssize_t result;
	ssize_t ret;
	int tmp=0;
	struct timex  txc;
	//struct rtc_time tm;	
	struct file *file=NULL;
	/*For store battery info*/
	file=filp_open(ADS1000_INFO_PATH,O_CREAT | O_RDWR,0);
	if(IS_ERR(file))
	{
		ads_dbg("load %s action %d failed\n",ADS1000_INFO_PATH,action);
		return -1;
	}

	switch(action){
		case READ_SHUT_CAP:
			result=file->f_op->read(file,buf,sizeof(buf),&file->f_pos);
			tmp = simple_strtol(buf, NULL, 0);
			if(tmp < di->bat_cap)
			{
				if(di->bat_cap<20)
				{
					if((di->bat_cap - tmp)<15)
						di->bat_cap=tmp;
					else
					{
						sprintf(buf,"%d",(di->bat_cap-4));
						ret=file->f_op->write(file,buf,sizeof(buf),&file->f_pos);
						ads_dbg("READ_SHUT_CAP write buf:%s\n",buf);	
					}
				}
				else if((di->bat_cap>=20)&&(di->bat_cap<45))
				{
					if((di->bat_cap - tmp)<=20)
						di->bat_cap=tmp;				
				}
				else if((di->bat_cap>=45)&&(di->bat_cap<55))
				{
					if((di->bat_cap - tmp)<=10)
						di->bat_cap=tmp + 2;			
				}
				else if((di->bat_cap>=55)&&(di->bat_cap<85))
				{
					if((di->bat_cap - tmp)<10)
						di->bat_cap=tmp;
				}		
				else if((di->bat_cap>=85)&&(di->bat_cap<99))
				{
					if((di->bat_cap - tmp)<5)
						di->bat_cap=tmp;
				}
			}
			else if(tmp > di->bat_cap)
			{
				if((tmp - di->bat_cap)<3)
					di->bat_cap=tmp;
			}
			ads_dbg("READ_SHUT_CAP:%s\ttmp:%d\tdi->bat_cap:%d\n",buf,tmp,di->bat_cap);
			break;
		case WRITE_SHUT_CAP:
			sprintf(buf,"%d",di->bat_cap);
			ret=file->f_op->write(file,buf,sizeof(buf),&file->f_pos);
			ads_dbg("write buf:%s\n",buf);			
			break;
		case READ_SUSPEND_TIME:
			ads_dbg("********* READ_SUSPEND_TIME *********\n" );
			ads_dbg("system_time - suspend_time:%ld	\n",(system_time - suspend_time));
			ads_dbg("system_time:%ld,suspend_time:%ld\n",system_time,suspend_time );
			ads_dbg("***************************************\n" );
			return (system_time - suspend_time);
		case GET_SYSTEM_TIME:
			do_gettimeofday(&(txc.time));
			if(di->dc_in)
				suspend_cap = di->bat_cap;
			system_time = txc.time.tv_sec;
			ads_dbg("GET_SYSTEM_TIME system_time:%ld\n",system_time);
			//rtc_time_to_tm(txc.time.tv_sec,&tm);
			//ads_dbg("WRITE UTC time :%d-%d-%d %d:%d:%d \n",
			//	tm.tm_year+1900,tm.tm_mon, tm.tm_mday,tm.tm_hour,tm.tm_min,tm.tm_sec);
			break;
			
		case READ_SUSPEND_CAP:
			break;
			
		case WRITE_SUSPEND_CAP:		
			break;
	}
	filp_close(file,NULL);
	ads_dbg("load %s action %d success\n",ADS1000_INFO_PATH,action);
	return 0;
}

static int ads1000_battery_analyser(int *val)
{
    int cmp = *val;
	int array_size=30;
	int analyze[30][2]={{0}};
	int value=0;
	int new_array[30]={0};
	int new_array_size=0;
	int cmp_count=0;
	int max_value=0;
	int index_max=0;
	int most_fit_value=0;
	int i=0;
	if(val == NULL){
	    ads_dbg("Error:Ads1000 no value to be analyzed!!!\n\n\n");
		return 0;
	}
    while(array_size > 0)
    {
        for(i=0;i<array_size;i++)
        {
            if(cmp == *(val+i))
            {
                cmp_count += 1;
            }
            else
            {
                new_array[new_array_size]=*(val+i);
                new_array_size++;
            }
        }
        analyze[value][1] = cmp_count;
        analyze[value][0] = cmp;
        value++;
        cmp = new_array[0];
        cmp_count = 0;
        val = new_array;
        array_size = new_array_size;
        new_array_size = 0;
    }
	
    for(i=0;i<10;i++)
        ads_dbg("analyze[%d][0]=%d analyze[%d][1]=%d\n",i,analyze[i][0],i,analyze[i][1]);

	max_value = analyze[0][1];
	for(i=0;i<10;i++)
	{
		if(max_value < analyze[i][1])
		{
			max_value = analyze[i][1];
			index_max = i;
		}
	}
	most_fit_value=analyze[index_max][0];
	
	ads_dbg("************Max value:%d  most_fit_value:%d***********\n",max_value,most_fit_value);
	return most_fit_value;	
}
static int ads1000_prevent_voltage_drop(int cap,struct ads1000_device_info *di)
{
	/*Prevend the voltage drop cause by big current*/
	ads_dbg("cap:%d\tdi->bat_cap:%d\tusing_count:%d\n",cap,di->bat_cap,using_count);
	if((di->bat_cap - cap) == 1)
		cap = di->bat_cap - 1;
	else if(((di->bat_cap)-cap) >= 2)
	{
		using_count++;
		if(di->bat_cap<15)
		{
			cap = ((di->bat_cap) - 1);
			using_count = 0;
		}
		else if(di->bat_cap<25 && di->bat_cap>=15)
		{
			
			if(using_count == 2)
			{
				cap = ((di->bat_cap) - 1); 
				using_count = 0;
			}
			else
				cap = (di->bat_cap);
		}
		else
		{
			if(using_count == 3)
			{
				cap = ((di->bat_cap) - 1);
				using_count = 0;
			}
			else
				cap = (di->bat_cap);
		}
	}

	return cap;	
}
static void ads1000_battery_info_filter(struct ads1000_device_info *di)
{
	int volt = -1;
	int full_detect = -1;
	int analyze_cap=0;
	int tmp=-1;
	/*report capacity*/
	volt = get_bat_voltage(di);
	if (VALID_VOLT(volt))
	{
		bat_cap[store_count] = get_bat_capacity(volt,di);
		store_count++;
		//ads_dbg("volt:%d bat_cap[%d]:%d\n",volt,store_count-1,bat_cap[store_count-1]);
		/*while booting up will use this resolution*/
		if(bootup_flag)
		{
			if(bat_cap[29]==0)
			{
				if((di->dc_in_st == 1)&&(full == 10))
				{
					(di->bat_cap) = 100;
					di->bat_status = POWER_SUPPLY_STATUS_FULL;
				}
				else 
				{
					if(tmp == -1)
					{
						if(di->dc_in_st == 1)
						{
							(di->bat_cap) = bat_cap[1] - 10;
							if((di->bat_cap)<0)
								(di->bat_cap) = 0;
						}
						else
							(di->bat_cap) = bat_cap[1];
						tmp = ads1000_battery_info_store(di,READ_SHUT_CAP);
						if(tmp!=-1)
						{
							bootup_flag=0;
							ads1000_battery_info_store(di,WRITE_SHUT_CAP);
						}
					}
				}
			}
		}
		ads1000_battery_info_store(di,GET_SYSTEM_TIME);
		/*after boot up will use this resolution*/
		if(store_count==30){
			int j,k=0;
			int mean=0,sum=0;
			for(j=0;j<store_count;j++)
			{
#if 1
				if(k<4)
				{
					ads_dbg("bat_cap[%2.2d]:%d ",j,bat_cap[j]);
					k++;
				}
				else
				{
					ads_dbg("bat_cap[%2.2d]:%d\n",j,bat_cap[j]);
					k=0;
				}
#endif
				sum+=bat_cap[j];
			}
			mean=sum/30;
			ads_dbg("\n");
			ads_dbg("***************Mean:%d******************\n",mean);
			ads1000_battery_info_store(di,WRITE_SHUT_CAP);
			analyze_cap	= ads1000_battery_analyser(bat_cap);
			store_count = 0;
			if(di->dc_in_st == 1)
			{
				if((di->bat_cap)<analyze_cap)
				{
					if(charge_base==0)
					{
						charge_base = mean;
					}
					else
					{
						if((mean - charge_base)>0)
						{
							int tmp=0;
							/*don`t let the capacity rise too fast*/
							if((mean - charge_base)>=3&&(di->bat_cap>=50))
								charge_base = mean-2;
							else if((mean - charge_base)>4&&(di->bat_cap>20)&&(di->bat_cap<50))
								charge_base = mean-2;
							else if((mean - charge_base)>4 && (di->bat_cap<=20))//To control the charge speed,while low power.
								charge_base = mean-2;
							/*report the capacity*/
							tmp	= di->bat_cap + (mean - charge_base);
							if(tmp >=100)
								di->bat_cap = 99;
							else
								di->bat_cap = tmp;
							charge_base = mean;
						}
					}
				}	
			}
			else
			{
				if((di->bat_cap)>analyze_cap)
				{
					(di->bat_cap) = ads1000_prevent_voltage_drop(analyze_cap,di);
				}
			}
		}
	}
	else
	{
		ads_dbg("%s:%d Invalid voltage\n",__func__,__LINE__);			
	}
	/*report status*/
	if (di->dc_in_st == 0)
	{
		di->bat_status = POWER_SUPPLY_STATUS_DISCHARGING;
		full=0;
	}
	else
	{				
		/*report battery full or not,full_detect=1:full*/
		full_detect = !!gpio_get_value(di->bat_full);
		ads_dbg("full:%d full_detect:%d\n",full,full_detect);
		/*if the pin be detected for 10 times hight.will tell the system battery is full*/
		if(full_detect)
		{
			full+=full_detect;
			if(full == 10)
			{
				di->bat_status = POWER_SUPPLY_STATUS_FULL;
				(di->bat_cap) = 100;
				full=0;
			}
		}
		else
		{
			di->bat_status = POWER_SUPPLY_STATUS_CHARGING;
			full=0;
		}

	}
	ads_dbg("%s\n",debug_real);
	ads_dbg("volt  :%4.4d\tcapacity:%2.2d%%\tfull:%d\tcharge_base=%d\n",volt,di->bat_cap,full,charge_base);
}
#endif
static void ads1000_battery_resume_policy(struct ads1000_device_info *di)
{
	int volt = -1;
	int full = -1;
	int resume_cap=0;
	int i = 0;
	
	ads_dbg("Resume now, battery update battery Info.\n");
	/*Add to handle the suspend and resume action
		after resume,will report these values */
	/* update battery voltage and capacity */
	volt = get_bat_voltage(di);
	if (VALID_VOLT(volt))
	{
		int tmp,resume_tmp;
		ads1000_battery_info_store(di,GET_SYSTEM_TIME);
		tmp = ads1000_battery_info_store(di,READ_SUSPEND_TIME);
		resume_tmp = tmp;
		resume_cap = get_bat_capacity(volt,di);
		ads_dbg("tmp:%d di->bat_cap:%d resume_count:%d resume_cap:%d record_suspend_time:%d\n",
							tmp,di->bat_cap,resume_count,resume_cap,record_suspend_time);
		if (di->dc_in_st == 0)
		{
			di->bat_status = POWER_SUPPLY_STATUS_DISCHARGING;
			if(resume_cap <= di->bat_cap)
			{
				/*
					suspend power:60MAh;total power:3700MAh 60/3700=0.0162
					the battery capacity will decline about 1.6% each hour
				*/
				if((record_suspend_time)<1800)
				{
					record_suspend_time += tmp;
					ads_dbg("Suspend time is too short,will be skip!!!record_suspend_time:%d\n",
								record_suspend_time);
					if((di->bat_cap)>resume_cap)
					{
						if((di->bat_cap - resume_cap)>15 && di->bat_cap > 0 && di->bat_cap <= 40)
							di->bat_cap = (resume_cap+7);
						else if((di->bat_cap - resume_cap)>10)
							di->bat_cap = (resume_cap+5);
					}
					else
						ads_dbg("Nothing to be done\n");
				}
				else
				{
					ads_dbg("Suspend time will be handle!!!record_suspend_time:%d\n",
								record_suspend_time);
					tmp = di->bat_cap - record_suspend_time/1800;
					if(tmp>0)
					{ 
						if(tmp > resume_cap)
							di->bat_cap = tmp;
						else
						{
							di->bat_cap = resume_cap+3;
							if(di->bat_cap > 100)
								di->bat_cap = 100;
						}
						if((di->bat_cap)>resume_cap)
						{
							if((di->bat_cap - resume_cap)>10)
								di->bat_cap = (resume_cap+4);
							else if((di->bat_cap - resume_cap)<10 && (di->bat_cap - resume_cap)> 5)
								di->bat_cap = (resume_cap+2);
						}
						else
							ads_dbg("Nothing to be done\n");

						
						ads_dbg("After suspend di->bat_cap:%d!!! \n",di->bat_cap);
					}
					else
					{
						ads_dbg("Error battery capacity di->bat_cap:%d\n",di->bat_cap);
						if(resume_cap<=0)
							di->bat_cap = 2;
						else//??
						{
							di->bat_cap = resume_cap+4;
							if(di->bat_cap > 100)
								di->bat_cap = 100;
						}
					}
					record_suspend_time -= ((record_suspend_time/1800)*1800);
				}
				if(resume_count == 100)
				{
					resume_count = 0;
					di->bat_cap -= 1;
				}
			}
			else
			{
				//Handle:unplug the charger after charging the device for over 10 minutes.
				if(resume_tmp >= 300 || record_suspend_time >= 600)
				{
					if(resume_cap >= 0&&resume_cap <= 30)
					{
						if((resume_cap - di->bat_cap)>10 && resume_cap >= 20 && resume_cap <= 50)
							di->bat_cap = resume_cap - 10;
						else
							di->bat_cap = resume_cap;
					}
					else
					{
						if((resume_cap - di->bat_cap)> 30)
							di->bat_cap = resume_cap-10;
					}
					record_suspend_time=0;
				}
				else if((resume_cap - di->bat_cap)>30)
					di->bat_cap = resume_cap-5;
			}
		}
		else
		{
			di->bat_status = POWER_SUPPLY_STATUS_CHARGING;
			
				//Handle:charging the device for over 10 minutes.
			if(resume_tmp >= 300 || record_suspend_time >=600)
			{
				if(resume_cap > di->bat_cap && resume_cap < 100 )
				{
					if((resume_cap - di->bat_cap)>10 && resume_cap >= 20 && resume_cap <= 50)
						di->bat_cap = resume_cap - 10;
					else
						di->bat_cap = resume_cap;
				}
				else if (resume_cap>= 100)
					di->bat_cap = 99;
				resume_tmp = 0;
				record_suspend_time=0;
			}					
			else if((resume_cap - di->bat_cap)>30)
				di->bat_cap = resume_cap-10;
		}
		/*reset analyze value*/
		for(i=0;i<30;i++)
			bat_cap[i]=(di->bat_cap);
		
		resume_flag=0;
		ads_dbg("resume volt:%4.4d\tcapacity:%2.2d%%\tfull:%d\tresume_tmp:%d\n",volt,di->bat_cap,full,resume_tmp);
	}
}
static void ads1000_battery_update_state(struct ads1000_device_info *di)
{
#ifdef CONFIG_P200
	if(resume_flag)
	{
		ads1000_battery_resume_policy(di);
	}
	else
	{
		ads1000_battery_info_filter(di);
	}
	di->bat_present = 1;
#endif

}

static void ads1000_battery_monitor(struct work_struct *work)
{
	struct ads1000_device_info *di;

	di = container_of((struct delayed_work *)work,
			  struct ads1000_device_info, monitor_work);

	ads1000_battery_update_state(di);
	power_supply_changed(&di->bat);

	/* reschedule for the next time */
	schedule_delayed_work(&di->monitor_work, di->interval);
}

static void dc_in_check_worker(struct work_struct *work)
{
	struct ads1000_device_info *di = container_of((struct delayed_work *)work, 
		struct ads1000_device_info, dc_in_work);

	di->dc_in_st = !!gpio_get_value(di->dc_in);
	printk("dc_in: %d\n", di->dc_in_st);
	
	ads1000_battery_update_state(di);
	power_supply_changed(&di->bat);

	enable_irq(di->irq);

}

static irqreturn_t charger_dc_in_detect(int irq, void *dev_id)
{
	struct ads1000_device_info *di = (struct ads1000_device_info *)dev_id;

	disable_irq_nosync(di->irq);
	/* delay 100ms to wait charge_full stable */
	
	schedule_delayed_work(&di->dc_in_work, msecs_to_jiffies(100));

	return IRQ_HANDLED;
}

static ssize_t dc_in_show(struct device *dev, struct device_attribute *attr,
			  char *buf)
{
	ssize_t ret = 0;

	sprintf(buf, "dc in: %d\n", !!gpio_get_value(g_di->dc_in));
	ret = strlen(buf) + 1;

	return ret;
}

static ssize_t debug_show(struct device *dev, struct device_attribute *attr,
			  char *buf)
{
	return sprintf(buf,"debug:%s\n", debug?"on":"off");
}

static ssize_t debug_store(struct device *dev, struct device_attribute *attr,
			  const char *buf, size_t count)
{
    sscanf(buf, "%d", &debug);
    if (debug) printk("debug on\n");
    else printk("debug off");
	return count;
}

static struct device_attribute ads1000_static_attrs[] = {
	__ATTR(dc_in, 0666, dc_in_show, NULL),
	__ATTR(debug, 0666, debug_show, debug_store),
};


static int dc_in_setup(struct ads1000_device_info *di)
{
	int gpio, ret = 0;
	gpio = di->dc_in;
	/* set DC_IN as input */
	if (gpio_request(gpio, "DC_IN")) {
		dev_err(di->dev, "Failed to request DC_IN(GPIO%d)\n", gpio);
		ret = -EIO;
		goto err_gpio;
	}
	gpio_direction_input(gpio);

	/* request dc_in interrupt */
	ret = request_irq(di->irq, charger_dc_in_detect,
			  IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING,
			  "DC_IN", di);
	if (ret < 0) {
		dev_err(di->dev, "unable to request IRQ\n");
		ret = -EIO;
		goto err_irq;
	}
	return 0;
err_irq:
	gpio_free(di->dc_in);
err_gpio:
	return ret;
}

/* AD converter setup */
static int adc_setup(struct ads1000_device_info *di)
{
	uint8_t cfg_reg, buf[3] = { 0 };
	int ret = 0;
	/* Set vdd voltage */
	di->vdd = VDD_VOLTAGE;
	/* Set as single conversion mode to save power */
	di->conv_mode = 1;
	/* Init ADC chip */
	ret = ads1000_read_i2c(buf, 3, di);
	if (ret < 0) {
		dev_err(di->dev, "failed to read ads1000\n");
		return ret;
	}
	/* Third byte is the configuration register contents */
	cfg_reg = buf[2];

	/* Configuration register:
	 * bit7: ST/BSY
	 * bit4: conversion mode(1: single, 0: continuous)
	 * bit[1,0]: PGA(Gain) setting */
	if (di->conv_mode)
		cfg_reg |= (1 << 4);	/* single */
	else
		cfg_reg &= ~(1 << 4);	/* continuous */
	/* set gain to 1 */
	cfg_reg &= (~0x3);
	/* configure adc */
	ret = ads1000_write_i2c(&cfg_reg, 1, di);
	if (ret < 0) {
		dev_err(di->dev, "failed to configure ads1000\n");
		return ret;
	}
	return 0;
}

static int ads1000_battery_probe(struct i2c_client *client,
				 const struct i2c_device_id *id)
{
	struct ads1000_device_info *di;
	struct ads1000_access_methods *bus;
	struct ads1000_platform_data *pdata;
	int ret = 0;
	int i;

	/* Allocate device info data */
	di = kzalloc(sizeof(*di), GFP_KERNEL);
	if (!di) {
		dev_err(&client->dev, "failed to allocate device info data\n");
		ret = -ENOMEM;
		goto err_di;
	}
	bus = kzalloc(sizeof(*bus), GFP_KERNEL);
	if (!bus) {
		dev_err(&client->dev, "failed to allocate access method "
			"data\n");
		ret = -ENOMEM;
		goto err_bus;
	}
	/* Init client data */
	i2c_set_clientdata(client, di);
	di->dev = &client->dev;
	di->bat.name = "ads1000-battery";
	bus->read = &ads1000_read_i2c;
	bus->write = &ads1000_write_i2c;
	di->bus = bus;
	di->client = client;
	printk("read ads1000 addr:0x%x\n",client->addr);

	/* Init platform data */
	pdata = (struct ads1000_platform_data *)client->dev.platform_data;
	di->dc_in = pdata->gpio_dc_in;
	di->bat_full = pdata->gpio_bat_full;
	/* DC_IN irq */
	di->irq = gpio_to_irq(di->dc_in);

	/* Init battery monitor periodic work queue */
	di->interval = msecs_to_jiffies(pdata->monitor_interval_ms);
	INIT_DELAYED_WORK(&di->monitor_work, ads1000_battery_monitor);
	/* Init DC_IN work queue */
	INIT_DELAYED_WORK(&di->dc_in_work, dc_in_check_worker);

	/* 
	 * I moved adc_setup() to the place before dc_in_setup(), 
	 * since adc_setup() reads i2c to check if the real 
	 * device exist. If not, dc_in_setup() will be skipped 
	 * and won't occupy the gpio. -guang
	 */

	/* AD converter init */
	ret = adc_setup(di);
	if (ret) {
		dev_err(&client->dev, "failed to setup ADC\n");
		goto err_adc_setup;
	}
	
	/* DC_IN gpio and irq init */
	ret = dc_in_setup(di);
	if (ret) {
		dev_err(&client->dev, "failed to setup DC_IN\n");
		goto err_dc_in_setup;
	}

	if (gpio_request(di->bat_full, "ads1000-battery") == 0)
	{
		gpio_direction_output(di->bat_full, 0);
		gpio_direction_input(di->bat_full);
	}
	else
	{
		printk("request bat_full gpio failed.\n");
	}
	
	/* Register power supply device for battery */
	ret = ads1000_power_supply_init(di);
	if (ret) {
		dev_err(&client->dev, "failed to register battery\n");
		goto err_power_supply_reg;
	}

	/* Init battery ststus */
	ads1000_battery_update_state(di);
	di->dc_in_st = !!gpio_get_value(di->dc_in);
	resume_flag=0;
	
	/* Start monitor work */
	schedule_delayed_work(&di->monitor_work, msecs_to_jiffies(1000));
	/* Init global device info */
	g_di = di;

	for (i=0; i<ARRAY_SIZE(ads1000_static_attrs); i++)
	{
		ret = device_create_file(&client->dev, &ads1000_static_attrs[i]);
		if (ret) {
			dev_err(&client->dev, "device_create_file failed\n");
		}
	}

	return 0;

err_power_supply_reg:
	free_irq(di->irq, di);
	gpio_free(di->dc_in);
err_adc_setup:
err_dc_in_setup:
	kfree(bus);
err_bus:
	kfree(di);
err_di:
	return ret;
}

static int ads1000_battery_remove(struct i2c_client *client)
{
	int i;
	struct ads1000_device_info *di = i2c_get_clientdata(client);

	for (i=0; i<ARRAY_SIZE(ads1000_static_attrs); i++)
		device_remove_file(&client->dev, &ads1000_static_attrs[i]);
	
	cancel_delayed_work_sync(&di->monitor_work);
	cancel_delayed_work_sync(&di->dc_in_work);
	power_supply_unregister(&di->bat);

	free_irq(di->irq, di);
	gpio_free(di->dc_in);

	gpio_free(di->bat_full);

	kfree(di->bus);
	kfree(di);

	return 0;
}

int ads1000_suspend(struct i2c_client* c, pm_message_t mesg)
{
	struct timex txc;
	do_gettimeofday(&(txc.time));
	if(resume_flag)
	{
		record_suspend_time += (txc.time.tv_sec - suspend_time);
	}
	suspend_time = txc.time.tv_sec;
	ads_dbg("********   WRITE_SUSPEND_TIME  ********\n");
	ads_dbg("suspend_time:%ld record_suspend_time:%d resume_flag:%d\n",
			suspend_time,record_suspend_time,resume_flag);
	ads_dbg("***************************************\n");
	ads_dbg("ads1000_suspend dc_in:%d\n",!!gpio_get_value(g_di->dc_in));
	cancel_delayed_work_sync(&g_di->monitor_work);
	return 0;
}

int ads1000_resume(struct i2c_client* c)
{
	if (g_di->dc_in_st != !!gpio_get_value(g_di->dc_in))
	{
		g_di->dc_in_st = !!gpio_get_value(g_di->dc_in);
		printk("dc_in: %d\n", g_di->dc_in_st);
	}
	resume_flag=1;
	resume_count++;
	schedule_delayed_work(&g_di->monitor_work, 125);
	ads_dbg("ads1000_resume dc_in:%d  resume_flag:%d\n",!!gpio_get_value(g_di->dc_in),resume_flag);
	
	return 0;
}

static const struct i2c_device_id ads1000_id[] = {
	{ "ads1000", -1},
	{ "ads1000_bd1",-1},
	{},
};

static struct i2c_driver ads1000_driver = {
	.driver = {
		.name = "ads1000",
	},
	.probe = ads1000_battery_probe,
	.remove = ads1000_battery_remove,
	.id_table = ads1000_id,
	.suspend = ads1000_suspend,
	.resume = ads1000_resume,
};

static int __init ads1000_init(void)
{
	int ret;

	ret = i2c_add_driver(&ads1000_driver);
	if (ret)
		printk(KERN_ERR "Unable to register ADS1000 driver\n");

#if defined(BATTERY_3000MA)
    printk("3000mA battery driver registered!\n");
#elif defined(BATTERY_3200MA)
    printk("3200mA battery driver registered!\n");
#elif defined(BATTERY_3500MA)
    printk("3500mA battery driver registered!\n");
#elif defined(CONFIG_P200)
    printk("4200mA battery driver registered!\n");
#endif

return ret;
}

module_init(ads1000_init);

static void __exit ads1000_exit(void)
{
	i2c_del_driver(&ads1000_driver);
}

module_exit(ads1000_exit);

MODULE_AUTHOR("Yunfan Zhang <yfzhang@marvell.com>");
MODULE_DESCRIPTION("ADS1000 Battery Driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform: ads1000-battery");
