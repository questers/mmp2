/*
 * Battery driver for Maxim MAX8925
 *
 * Copyright (c) 2009-2010 Marvell International Ltd.
 *	Haojian Zhuang <haojian.zhuang@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/power_supply.h>
#include <linux/workqueue.h>
#include <linux/mfd/max8925.h>
#include <plat/vbus.h>
#include <asm/mach-types.h>
#include <linux/power/max8903.h>
#include <linux/power/usbcharger.h>
#include <mach/mfp-mmp2.h>
#include <mach/mmp2_plat_ver.h>
#ifdef CONFIG_DVFM
#include <mach/dvfm.h>
#endif

/* registers in GPM */
#define MAX8925_OUT5VEN			0x54
#define MAX8925_OUT3VEN			0x58
#define MAX8925_CHG_CNTL1		0x7c

/* bits definition */
#define MAX8925_CHG_STAT_VSYSLOW	(1 << 0)
#define MAX8925_CHG_STAT_MODE_MASK	(3 << 2)
#define MAX8925_CHG_STAT_EN_MASK	(1 << 4)
#define MAX8925_CHG_MBDET		(1 << 1)
#define MAX8925_CHG_AC_RANGE_MASK	(3 << 6)

/* registers in ADC */
#define MAX8925_ADC_RES_CNFG1		0x06
#define MAX8925_ADC_AVG_CNFG1		0x07
#define MAX8925_ADC_ACQ_CNFG1		0x08
#define MAX8925_ADC_ACQ_CNFG2		0x09
/* 2 bytes registers in below. MSB is 1st, LSB is 2nd. */
#define MAX8925_ADC_AUX2		0x62
#define MAX8925_ADC_VCHG		0x64
#define MAX8925_ADC_VBBATT		0x66
#define MAX8925_ADC_VMBATT		0x68
#define MAX8925_ADC_ISNS		0x6a
#define MAX8925_ADC_THM			0x6c
#define MAX8925_ADC_TDIE		0x6e
#define MAX8925_CMD_AUX2		0xc8
#define MAX8925_CMD_VCHG		0xd0
#define MAX8925_CMD_VBBATT		0xd8
#define MAX8925_CMD_VMBATT		0xe0
#define MAX8925_CMD_ISNS		0xe8
#define MAX8925_CMD_THM			0xf0
#define MAX8925_CMD_TDIE		0xf8

enum {
	MEASURE_AUX2,
	MEASURE_VCHG,
	MEASURE_VBBATT,
	MEASURE_VMBATT,
	MEASURE_ISNS,
	MEASURE_THM,
	MEASURE_TDIE,
	MEASURE_MAX,
};

struct max8925_power_info {
	struct max8925_chip	*chip;
	struct i2c_client	*gpm;
	struct i2c_client	*adc;
	struct work_struct	wq;
	struct work_struct	dvfm_wq;

	struct power_supply	ac;
	struct power_supply	usb;
	struct power_supply	battery;
	int			irq_base;
	unsigned		ac_online:1;
	unsigned		usb_online:1;
	unsigned		bat_online:1;
	unsigned		chg_mode:2;
	unsigned		batt_detect:1;	/* detecing MB by ID pin */
	unsigned		topoff_threshold:2;
	unsigned		fast_charge:3;  /* fast charging current */
	/* max8903 charger en/disable flag */
	unsigned		chg_max8903_en:1; /* 1:enable; 0:disable */

	int (*set_charger) (int);
	int (*set_led) (int);

	int (*update_charger)(int status);

	struct timer_list chg_poll_timer;

	/* VBus disturbance rejection timer */
	int enable_detect;
	struct timer_list detect_delay_timer;
};

struct capacity_percent {
	int	voltage;
	int	percent;
};


static struct max8925_power_info *power_info;
/* usb enumeration status
 * extern from driver/usb/gadget/mv_gadget.c */
extern enum usb_enum_status_t usb_enum_st;
/* charger polling delay time */
#define CHARGER_POLLING_DELAY msecs_to_jiffies(500)
/* battery resistance offset */
#define BATTERY_R_CHARGING		330		/* charging offset */
#define BATTERY_R_DISCHARGING	60		/* discharging offset */

static struct capacity_percent cap_table[] = {
	{3200, 0}, {3320, 10}, {3420, 20}, {3500, 30}, {3550, 40},
	{3600, 50}, {3650, 60}, {3750, 70}, {3850, 80}, {3950, 90},
	{4100, 100},
};

#ifdef CONFIG_DVFM
static struct dvfm_lock dvfm_lock = {
	.dev_idx	= -1,
	.count		= 0,
};

static void dvfm_work_func(struct work_struct *work)
{
	struct max8925_power_info *info;
	info = container_of(work, struct max8925_power_info, dvfm_wq);
	/* if usb online, set dvfm constraint */
	if (info->usb_online) {
		if (dvfm_lock.count == 0) {
			dvfm_lock.count++;
			dvfm_disable_op_name("apps_idle", dvfm_lock.dev_idx);
			dvfm_disable_op_name("apps_sleep", dvfm_lock.dev_idx);
			dvfm_disable_op_name("chip_sleep", dvfm_lock.dev_idx);
			dvfm_disable_op_name("sys_sleep", dvfm_lock.dev_idx);
			dvfm_disable_op_name("Ultra Low MIPS", dvfm_lock.dev_idx);
			dvfm_disable_op_name("Low MIPS", dvfm_lock.dev_idx);
		}
	} else {
		if (dvfm_lock.count == 1) {
			dvfm_lock.count--;
			dvfm_enable_op_name("apps_idle", dvfm_lock.dev_idx);
			dvfm_enable_op_name("apps_sleep", dvfm_lock.dev_idx);
			dvfm_enable_op_name("chip_sleep", dvfm_lock.dev_idx);
			dvfm_enable_op_name("sys_sleep", dvfm_lock.dev_idx);
			dvfm_enable_op_name("Ultra Low MIPS", dvfm_lock.dev_idx);
			dvfm_enable_op_name("Low MIPS", dvfm_lock.dev_idx);
		}
	}
}
#endif

static int max8925_set_charger(int chgcur, int enable)
{
	struct max8925_power_info *info = power_info;
	struct max8925_chip *chip = info->chip;

	if (enable) { /* enable charger */
		max8925_set_bits(info->gpm, MAX8925_CHG_CNTL1, 1 << 7 | 0x07, chgcur);
		info->fast_charge = chgcur;
	} else          /* disable charge */
		max8925_set_bits(info->gpm, MAX8925_CHG_CNTL1, 1 << 7, 1 << 7);

	dev_dbg(chip->dev, "%s\n", (enable) ? "Enable charger"
		: "Disable charger");
	return 0;
}

static int __set_charger(struct max8925_power_info *info, int enable)
{
	if (!info) {
		pr_info("max8925 power info is not available !\n");
		return -EINVAL;
	}
	/* use max8903 charger */
	if (info->chg_max8903_en) {
#ifdef CONFIG_CHARGER_MAX8903
		max8903_set_chgcur(info->fast_charge, enable);
#else
		pr_debug("foo: max8903: set charger current!\n");
#endif
	} else {
		/* charger on PMIC max8925 */
		max8925_set_charger(info->fast_charge, enable);
	}

	if (enable) {
		/* enable indicator led */
		if (info->set_led)
			info->set_led(1);
	} else {
		/* disbale indicator led */
		if (info->set_led)
			info->set_led(0);
		/* delete polling-timer if disable charger */
		if (timer_pending(&info->chg_poll_timer) == 1)
			del_timer_sync(&info->chg_poll_timer);
	}

	return 0;
}

/* update charging current */
static int max8925_update_charger(int status)
{
	int ret = 0;
	/* update fast charging current */
	switch (status) {
	case USB_ENUM_NONE:
		ret = MAX8925_FCHG_1000MA;
		break;
	case USB_ENUM_START:
	case USB_ENUM_100MA:
	case USB_ENUM_FAILED:
		ret = MAX8925_FCHG_85MA;
		break;
	case USB_ENUM_500MA:
		ret = MAX8925_FCHG_460MA;
		break;
	default:
		break;
	}

	return ret;
}

/* start a timer to detect the type of charger */
static void start_charger_detect(struct max8925_power_info *info, unsigned int delay)
{
	if (!info) {
		pr_info("max8925 power info is not available !\n");
		return;
	}
	mod_timer(&info->chg_poll_timer, jiffies + delay);
}


static void chg_poll_timer_event(unsigned long data)
{
	struct max8925_power_info *pw_info  = (struct max8925_power_info *)data;
	int status;

	if (!pw_info) {
		pr_info("max8925 power info is not available !\n");
		return;
	}

	status = usb_enum_st;
	/* update charging current */
	if (pw_info->chg_max8903_en) {
		/* max8903 charger */
#ifdef CONFIG_CHARGER_MAX8903
		pw_info->fast_charge = max8903_update_charger(status);
#else
		pr_debug("foo: max8903 update charger!\n");
#endif
	} else {
		/* max8925 charger */
		pw_info->fast_charge = max8925_update_charger(status);
	}

	/* charger changed() */
	if (1)/*status  == USB_ENUM_NONE)*/ {
		/* ac charger connected */
		pr_debug("---ac charger detected---\n");
		pw_info->ac_online = 1;
		pw_info->usb_online = 0;
		power_supply_changed(&pw_info->ac);
	} else {
		pr_debug("---usb charger detected---\n");
		pw_info->usb_online = 1;
		pw_info->ac_online = 0;
		/* usb charger connected */
		power_supply_changed(&pw_info->usb);
	}

	/* set charger current */
	schedule_work(&pw_info->wq);
	/* set usb dvfm constranit */
#ifdef CONFIG_DVFM
	schedule_work(&pw_info->dvfm_wq);
#endif
}

/* VBus disturbance rejection timer function */
static void detect_delay_timer_event(unsigned long data)
{
	struct max8925_power_info *info  = (struct max8925_power_info *)data;
	info->enable_detect = 1;
}

irqreturn_t max8925_charger_handler(int irq, void *data)
{
	struct max8925_power_info *info = power_info;
	struct max8925_chip *chip;

	if (!info) {
		pr_debug("max8925 power info is not available!\n");
		return IRQ_HANDLED;
	}

	chip = info->chip;

	if (pxa_query_usbdev() != USB_B_DEVICE) {
		/* disable charger */
		__set_charger(info, 0);
		return IRQ_HANDLED;
	}

	switch (irq - chip->irq_base) {
	case MAX8925_IRQ_VCHG_DC_R:
		/* charger attached/connected */
		info->usb_online = 0;
		info->ac_online = 0;
		if (info->enable_detect) {
			/* usb/ac cable attached, start charger detecting */
			usb_enum_st = USB_ENUM_NONE;
			start_charger_detect(info, CHARGER_POLLING_DELAY);
		}
		dev_dbg(chip->dev, "Charger inserted\n");
		break;
	case MAX8925_IRQ_VCHG_DC_F:
		/* charger detached */
		if (info->chg_max8903_en) {
#ifdef CONFIG_CHARGER_MAX8903
			/* In fact, vbus_en should be '1' to disable charger.
			 * vbus_en is controled by vbus driver.
			 * Actually, charging current limitation is set to 100mA here */
			max8903_set_chgcur(MAX8903_FCHG_DISABLE, 0);
#else
			pr_debug("foo: max8903 set charger current!\n");
#endif
			/* Disturbance Rejection: disable detect for 500ms to avoid vbus disturbance */
			info->enable_detect = 0;
			mod_timer(&info->detect_delay_timer, jiffies + msecs_to_jiffies(50));

			/* disbale indicator led */
			if (info->set_led)
				info->set_led(0);
			/* delete polling-timer */
			if (timer_pending(&info->chg_poll_timer) == 1)
				del_timer_sync(&info->chg_poll_timer);
		} else {
			/* disable charger */
			__set_charger(info, 0);
		}

		usb_enum_st = USB_ENUM_NONE;
		/* charger detached/changed */
		if (info->ac_online)
			power_supply_changed(&info->ac);
		if (info->usb_online)
			power_supply_changed(&info->usb);

		info->ac_online = 0;
		info->usb_online = 0;
		/* unset usb dvfm constraint */
#ifdef CONFIG_DVFM
		schedule_work(&info->dvfm_wq);
#endif
		dev_dbg(chip->dev, "Charger is removal\n");
		break;
	case MAX8925_IRQ_VCHG_USB_R:
		info->usb_online = 1;
		dev_dbg(chip->dev, "USB inserted\n");
		break;
	case MAX8925_IRQ_VCHG_USB_F:
		info->usb_online = 0;
		dev_dbg(chip->dev, "USB is removal\n");
		break;
	case MAX8925_IRQ_VCHG_THM_OK_F:
		/* Battery is not ready yet */
		dev_dbg(chip->dev, "Battery temperature is out of range\n");
		break;
	case MAX8925_IRQ_VCHG_DC_OVP:
		dev_dbg(chip->dev, "Error detection\n");
		break;
	case MAX8925_IRQ_VCHG_THM_OK_R:
		/* Battery is ready now */
		dev_dbg(chip->dev, "Battery temperature is in range\n");
		break;
	case MAX8925_IRQ_VCHG_SYSLOW_R:
		/* VSYS is low */
		dev_info(chip->dev, "Sys power is too low\n");
		break;
	case MAX8925_IRQ_VCHG_SYSLOW_F:
		dev_dbg(chip->dev, "Sys power is above low threshold\n");
		break;
	case MAX8925_IRQ_VCHG_DONE:
		/* disable charger */
		__set_charger(info, 0);
		dev_dbg(chip->dev, "Charging is done\n");
		break;
	case MAX8925_IRQ_VCHG_TOPOFF:
		dev_dbg(chip->dev, "Charging in top-off mode\n");
		break;
	case MAX8925_IRQ_VCHG_TMR_FAULT:
		/* disable charger */
		__set_charger(info, 0);
		dev_dbg(chip->dev, "Safe timer is expired\n");
		break;
	case MAX8925_IRQ_VCHG_RST:
		dev_dbg(chip->dev, "Charger is reset\n");
		break;
	}
	return IRQ_HANDLED;
}

static int start_measure(struct max8925_power_info *info, int type)
{
	unsigned char buf[2] = {0, 0};
	int meas_reg = 0, ret;

	switch (type) {
	case MEASURE_VCHG:
		meas_reg = MAX8925_ADC_VCHG;
		break;
	case MEASURE_VBBATT:
		meas_reg = MAX8925_ADC_VBBATT;
		break;
	case MEASURE_VMBATT:
		meas_reg = MAX8925_ADC_VMBATT;
		break;
	case MEASURE_ISNS:
		meas_reg = MAX8925_ADC_ISNS;
		break;
	default:
		return -EINVAL;
	}

	max8925_bulk_read(info->adc, meas_reg, 2, buf);
	ret = (buf[0] << 4) | (buf[1] >> 4);

	return ret;
}

static int get_capacity(int data)
{
	int i, size;
	int ret = -EINVAL;

	size = ARRAY_SIZE(cap_table);
	if (data >= cap_table[size-1].voltage) {
		ret = 100;
		return ret;
	} else if (data <= cap_table[0].voltage) {
		ret = 0;
		return ret;
	}
	for (i = size - 2; i >= 0; i--) {
		if (data < cap_table[i].voltage)
			continue;
		ret = (data - cap_table[i].voltage) * 10
			/ (cap_table[i + 1].voltage - cap_table[i].voltage);
		ret += cap_table[i].percent;
		break;
	}
	return ret;
}

static int max8925_get_bat_current(struct max8925_power_info *info)
{
	int ret = 0;
	long long int tmp = 0;

	ret = start_measure(info, MEASURE_ISNS);
	if (ret < 0) {
		pr_debug("fail to measure battery current!\n");
		return 0;
	}
	/* convert to current(mA)*/
	tmp = (long long int)ret * 6250 / 4096 - 3125;
	ret = (int)tmp;
	/* FIXME, reverse the charging current if board is BONNELL Rev1,
	 * since the current direction is inverted in Bonnell Hardware */
	if (machine_is_marvell_jasper() && board_is_mmp2_bonnell_rev1())
		ret = -ret;

	return ret;
}

static int max8925_get_bat_voltage(struct max8925_power_info *info)
{
	int cur = 0, vol = 0, offset = 0;
	int ret = 0;
	/* get battery current */
	ret = start_measure(info, MEASURE_VMBATT);
	if (ret < 0) {
		pr_debug("fail to measure battery voltage!\n");
		return 0;
	}

	vol = ret << 1;/* unit is mV */

	/* count in offset voltage caused by battery resistance */
	cur = max8925_get_bat_current(info);
	if (cur >= 0)
		offset = cur * BATTERY_R_CHARGING / 1000;
	else
		offset = cur * BATTERY_R_DISCHARGING / 1000;
	/* calculate actual battery voltage */
	vol -= offset;

	if (vol < 0) {
		pr_debug("battery voltage is abnormal!\n");
		vol = 0;
	}

	return vol;
}


static int max8925_ac_get_prop(struct power_supply *psy,
			       enum power_supply_property psp,
			       union power_supply_propval *val)
{
	struct max8925_power_info *info = dev_get_drvdata(psy->dev->parent);
	int ret = 0;

	if (!info) {
		pr_debug("max8925 power info is not availible!");
		return -EINVAL;
	}

	switch (psp) {
	case POWER_SUPPLY_PROP_ONLINE:
		val->intval = info->ac_online;
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
		if (info->ac_online) {
			ret = start_measure(info, MEASURE_VCHG);
			if (ret >= 0)
				val->intval = ret << 1;	/* unit is mV */
		} else
			val->intval = 0;
		break;
	default:
		ret = -ENODEV;
		break;
	}

	return ret;
}

static enum power_supply_property max8925_ac_props[] = {
	POWER_SUPPLY_PROP_ONLINE,
	POWER_SUPPLY_PROP_VOLTAGE_NOW,
};

static int max8925_usb_get_prop(struct power_supply *psy,
				enum power_supply_property psp,
				union power_supply_propval *val)
{
	struct max8925_power_info *info = dev_get_drvdata(psy->dev->parent);
	int ret = 0;

	if (!info) {
		pr_debug("max8925 power info is not availible!");
		return -EINVAL;
	}

	switch (psp) {
	case POWER_SUPPLY_PROP_ONLINE:
		val->intval = info->usb_online;
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
		if (info->usb_online) {
			ret = start_measure(info, MEASURE_VCHG);
			if (ret >= 0)
				val->intval = ret << 1;	/* unit is mV */
		} else
			val->intval = 0;
		break;
	default:
		ret = -ENODEV;
		break;
	}

	return ret;
}

static enum power_supply_property max8925_usb_props[] = {
	POWER_SUPPLY_PROP_ONLINE,
	POWER_SUPPLY_PROP_VOLTAGE_NOW,
};

static int max8925_bat_get_prop(struct power_supply *psy,
				enum power_supply_property psp,
				union power_supply_propval *val)
{
	struct max8925_power_info *info = dev_get_drvdata(psy->dev->parent);
	int ret = 0;

	if (!info) {
		pr_debug("max8925 power info is not availible!");
		return -EINVAL;
	}

	switch (psp) {
	case POWER_SUPPLY_PROP_ONLINE:
		val->intval = info->bat_online;
		break;
	case POWER_SUPPLY_PROP_VOLTAGE_NOW:
		/* 
		  * PNX-59,fix issue that get voltage property may failed while initializing kernel
		*/
		//if (info->bat_online) {
			val->intval = max8925_get_bat_voltage(info);
			ret = 0;
			break;
		//}
		//ret = -ENODATA;
		break;
	case POWER_SUPPLY_PROP_CURRENT_NOW:
		if (info->bat_online) {
			val->intval = max8925_get_bat_current(info);
			ret = 0;
			break;
		}
		ret = -ENODATA;
		break;
	case POWER_SUPPLY_PROP_CHARGE_TYPE:
		if (!info->bat_online) {
			ret = -ENODATA;
			break;
		}
		ret = max8925_get_bat_current(info);
		if (ret <= 0)
			val->intval = POWER_SUPPLY_CHARGE_TYPE_NONE;
		else if (ret <= 50)
			val->intval = POWER_SUPPLY_CHARGE_TYPE_TRICKLE;
		else
			val->intval = POWER_SUPPLY_CHARGE_TYPE_FAST;
		ret = 0;
		break;
	case POWER_SUPPLY_PROP_STATUS:
		if (!info->bat_online) {
			ret = -ENODATA;
			break;
		}
		ret = max8925_get_bat_current(info);
		if (ret < -5)
			val->intval = POWER_SUPPLY_STATUS_DISCHARGING;
		else if (ret > 5)
			val->intval = POWER_SUPPLY_STATUS_CHARGING;
		else
			val->intval = POWER_SUPPLY_STATUS_NOT_CHARGING;
		ret = 0;
		break;
	case POWER_SUPPLY_PROP_CAPACITY:
		if (!info->bat_online) {
			ret = -ENODATA;
			break;
		}
		ret = max8925_get_bat_voltage(info);
		val->intval = get_capacity(ret);
		ret = 0;
		break;
	default:
		ret = -ENODEV;
		break;
	}
	return ret;
}

static enum power_supply_property max8925_battery_props[] = {
	POWER_SUPPLY_PROP_ONLINE,
	POWER_SUPPLY_PROP_VOLTAGE_NOW,
	POWER_SUPPLY_PROP_CURRENT_NOW,
	POWER_SUPPLY_PROP_CHARGE_TYPE,
	POWER_SUPPLY_PROP_STATUS,
	POWER_SUPPLY_PROP_CAPACITY,
};

static void set_charger_work(struct work_struct *work)
{
	struct max8925_power_info *info;
	info = container_of(work, struct max8925_power_info, wq);
	if (pxa_query_usbdev() == USB_B_DEVICE)
		__set_charger(info, 1);
}

#define REQUEST_IRQ(_irq, _name)					\
do {									\
	ret = request_threaded_irq(chip->irq_base + _irq, NULL,		\
				    max8925_charger_handler,		\
				    IRQF_ONESHOT, _name, info);		\
	if (ret)							\
		dev_err(chip->dev, "Failed to request IRQ #%d: %d\n",	\
			_irq, ret);					\
} while (0)

static __devinit int max8925_init_charger(struct max8925_chip *chip,
					  struct max8925_power_info *info)
{
	int ret;

	REQUEST_IRQ(MAX8925_IRQ_VCHG_DC_OVP, "ac-ovp");
	/* if charger in/out are handled by max8925_vbus, the IRQ_VCHG_DC_F/R
	 * should be requested and handled in max8925_vbus */
#ifndef CONFIG_USB_VBUS_MAX8925
	REQUEST_IRQ(MAX8925_IRQ_VCHG_DC_F, "ac-remove");
	REQUEST_IRQ(MAX8925_IRQ_VCHG_DC_R, "ac-insert");
#endif
	REQUEST_IRQ(MAX8925_IRQ_VCHG_USB_OVP, "usb-ovp");
	REQUEST_IRQ(MAX8925_IRQ_VCHG_USB_F, "usb-remove");
	REQUEST_IRQ(MAX8925_IRQ_VCHG_USB_R, "usb-insert");
	REQUEST_IRQ(MAX8925_IRQ_VCHG_THM_OK_R, "batt-temp-in-range");
	REQUEST_IRQ(MAX8925_IRQ_VCHG_THM_OK_F, "batt-temp-out-range");
	REQUEST_IRQ(MAX8925_IRQ_VCHG_SYSLOW_F, "vsys-high");
	REQUEST_IRQ(MAX8925_IRQ_VCHG_SYSLOW_R, "vsys-low");
	REQUEST_IRQ(MAX8925_IRQ_VCHG_RST, "charger-reset");
	REQUEST_IRQ(MAX8925_IRQ_VCHG_DONE, "charger-done");
	REQUEST_IRQ(MAX8925_IRQ_VCHG_TOPOFF, "charger-topoff");
	REQUEST_IRQ(MAX8925_IRQ_VCHG_TMR_FAULT, "charger-timer-expire");

	info->ac_online = 0;
	info->usb_online = 0;
	info->bat_online = 0;
	/*
	 * If battery detection is enabled, ID pin of battery is
	 * connected to MBDET pin of MAX8925. It could be used to
	 * detect battery presence.
	 * Otherwise, we have to assume that battery is always on.
	 */
	if (info->batt_detect) {
		ret = max8925_reg_read(info->gpm, MAX8925_CHG_STATUS);
		info->bat_online = (ret & MAX8925_CHG_MBDET) ? 0 : 1;
	}
	else
		info->bat_online = 1;

	/* disable max8925 charge */
	max8925_set_bits(info->gpm, MAX8925_CHG_CNTL1, 1 << 7, 1 << 7);

	/* use charger on PMIC max8925 */
	if (!info->chg_max8903_en) {
		/* set charging current in charge topoff mode */
		max8925_set_bits(info->gpm, MAX8925_CHG_CNTL1, 3 << 5,
						info->topoff_threshold << 5);
		/* set charing current in fast charge mode */
		max8925_set_bits(info->gpm, MAX8925_CHG_CNTL1, 7, info->fast_charge);
	}

	INIT_WORK(&info->wq, set_charger_work);
	if (pxa_query_usbdev() == USB_B_DEVICE )
	{
		printk("%s, usb b device \n",__func__);
		if(pxa_query_vbus() & VBUS_B_SESSION_VALID)
		{
			printk("%s,b session valid\n",__func__);
			/* trigger usb/ac charger detection */
			start_charger_detect(info, msecs_to_jiffies(3000));
		}
	}
	return 0;
}

static __devexit int max8925_deinit_charger(struct max8925_power_info *info)
{
	struct max8925_chip *chip = info->chip;
	int irq;

	/* disable charger */
	if (pxa_query_usbdev() == USB_B_DEVICE)
		__set_charger(info, 0);

	irq = chip->irq_base + MAX8925_IRQ_VCHG_DC_OVP;
	for (; irq <= chip->irq_base + MAX8925_IRQ_VCHG_TMR_FAULT; irq++)
		free_irq(irq, info);

	return 0;
}

static __devinit int max8925_power_probe(struct platform_device *pdev)
{
	struct max8925_chip *chip = dev_get_drvdata(pdev->dev.parent);
	struct max8925_platform_data *max8925_pdata;
	struct max8925_power_pdata *pdata = NULL;
	struct max8925_power_info *info;
	int ret;

	if (pdev->dev.parent->platform_data) {
		max8925_pdata = pdev->dev.parent->platform_data;
		pdata = max8925_pdata->power;
	}

	if (!pdata) {
		dev_err(&pdev->dev, "platform data isn't assigned to "
			"power supply\n");
		return -EINVAL;
	}

	info = kzalloc(sizeof(struct max8925_power_info), GFP_KERNEL);
	if (!info)
		return -ENOMEM;
	info->chip = chip;
	info->gpm = chip->i2c;
	info->adc = chip->adc;

	dev_set_drvdata(&pdev->dev, info);
	platform_set_drvdata(pdev, info);

	info->ac.name = "max8925-ac";
	info->ac.type = POWER_SUPPLY_TYPE_MAINS;
	info->ac.properties = max8925_ac_props;
	info->ac.num_properties = ARRAY_SIZE(max8925_ac_props);
	info->ac.get_property = max8925_ac_get_prop;
	ret = power_supply_register(&pdev->dev, &info->ac);
	if (ret)
		goto out;
	info->ac.dev->parent = &pdev->dev;

	info->usb.name = "max8925-usb";
	info->usb.type = POWER_SUPPLY_TYPE_USB;
	info->usb.properties = max8925_usb_props;
	info->usb.num_properties = ARRAY_SIZE(max8925_usb_props);
	info->usb.get_property = max8925_usb_get_prop;
	ret = power_supply_register(&pdev->dev, &info->usb);
	if (ret)
		goto out_usb;
	info->usb.dev->parent = &pdev->dev;

	info->battery.name = "max8925-battery";
	info->battery.type = POWER_SUPPLY_TYPE_BATTERY;
	info->battery.properties = max8925_battery_props;
	info->battery.num_properties = ARRAY_SIZE(max8925_battery_props);
	info->battery.get_property = max8925_bat_get_prop;
	ret = power_supply_register(&pdev->dev, &info->battery);
	if (ret)
		goto out_battery;
	info->battery.dev->parent = &pdev->dev;

	info->batt_detect = pdata->batt_detect;
	info->topoff_threshold = pdata->topoff_threshold;
	info->fast_charge = pdata->fast_charge;
	info->set_led = pdata->set_led;
	info->chg_max8903_en = pdata->chg_max8903_en;

	init_timer(&info->chg_poll_timer);
	info->chg_poll_timer.function = chg_poll_timer_event;
	info->chg_poll_timer.data = (unsigned long)info;

	init_timer(&info->detect_delay_timer);
	info->detect_delay_timer.function = detect_delay_timer_event;
	info->detect_delay_timer.data = (unsigned long)info;

	info->enable_detect = 1;

	power_info = info;

#ifdef CONFIG_DVFM
	dvfm_register("U2O", &dvfm_lock.dev_idx);
	INIT_WORK(&info->dvfm_wq, dvfm_work_func);
#endif

	max8925_init_charger(chip, info);
	return 0;
out_battery:
	power_supply_unregister(&info->battery);
out_usb:
	power_supply_unregister(&info->ac);
out:
	max8925_deinit_charger(info);
	kfree(info);
	return ret;
}

static __devexit int max8925_power_remove(struct platform_device *pdev)
{
	struct max8925_power_info *info = platform_get_drvdata(pdev);

#ifdef CONFIG_DVFM
    dvfm_unregister("U2O", &dvfm_lock.dev_idx);
#endif
	if (info) {
		del_timer_sync(&info->chg_poll_timer);
		del_timer_sync(&info->detect_delay_timer);
		power_supply_unregister(&info->ac);
		power_supply_unregister(&info->usb);
		power_supply_unregister(&info->battery);
		max8925_deinit_charger(info);
		kfree(info);
	}
	return 0;
}

static struct platform_driver max8925_power_driver = {
	.probe	= max8925_power_probe,
	.remove	= __devexit_p(max8925_power_remove),
	.driver	= {
		.name	= "max8925-power",
	},
};

static int __init max8925_power_init(void)
{
	return platform_driver_register(&max8925_power_driver);
}
module_init(max8925_power_init);

static void __exit max8925_power_exit(void)
{
	platform_driver_unregister(&max8925_power_driver);
}
module_exit(max8925_power_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Power supply driver for MAX8925");
MODULE_ALIAS("platform:max8925-power");
