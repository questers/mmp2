/*
 * Main Battery Charger Driver for Max8903
 * With external AC(Adapter Charger)/USB power supplies.
 *
 * Copyright (c) 2010 Marvell Technology Ltd.
 * Yunfan Zhang <yfzhang@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/timer.h>
#include <linux/platform_device.h>
#include <linux/power_supply.h>
#include <linux/workqueue.h>
#include <linux/gpio.h>
#include <mach/mfp-mmp2.h>
#include <linux/power/max8903.h>
#include <linux/power/usbcharger.h>
#include <mach/mmp2_plat_ver.h>

static struct max8903_charger_info *chg_info;

/* update charging current */
int max8903_update_charger(int status)
{
	struct max8903_charger_info *info = chg_info;
	int ret = 0;
	if (!info) {
		pr_info("max8903 charger info is not available!\n");
		return -EINVAL;
	}
	/* update fast charging current */
	switch (status) {
	case USB_ENUM_NONE:
		ret = MAX8903_FCHG_2000MA;
		break;
	case USB_ENUM_500MA:
		ret = MAX8903_FCHG_500MA;
		break;
	case USB_ENUM_START:
	case USB_ENUM_100MA:
	case USB_ENUM_FAILED:
		ret = MAX8903_FCHG_100MA;
		break;
	default:
		break;
	}
	info->fast_chgcur = ret;

	return ret;
}
EXPORT_SYMBOL(max8903_update_charger);

/* set charging current */
int max8903_set_chgcur(int chgcur, int enable)
{
	struct max8903_charger_info *info = chg_info;

	if (!info) {
		pr_info("max8903 charger info is unavailable!\n");
		return -EINVAL;
	}

	if (enable) {
		info->fast_chgcur = chgcur;
		/* set max fast charging current */
		switch (chgcur) {
		case MAX8903_FCHG_2000MA:
			if (info->chg_500ma)
				gpio_direction_output(info->chg_500ma,   1);
			if (info->chg_2a_n)
				gpio_direction_output(info->chg_2a_n,    0);
			break;
		case MAX8903_FCHG_500MA:
			if (info->chg_500ma)
				gpio_direction_output(info->chg_500ma,   1);
			if (info->chg_2a_n)
				gpio_direction_output(info->chg_2a_n,    1);
			break;
		case MAX8903_FCHG_100MA:
			if (info->chg_500ma)
				gpio_direction_output(info->chg_500ma,   0);
			if (info->chg_2a_n)
				gpio_direction_output(info->chg_2a_n,    1);
			break;
		case MAX8903_FCHG_DISABLE: /* disbale charger */
		default: /* undefined parameter: disable charger */
			/* In fact, vbus_en should be '1' to disable charger.
			 * vbus_en is controled by vbus driver */
			if (info->chg_500ma)
				gpio_direction_output(info->chg_500ma,   0);
			if (info->chg_2a_n)
				gpio_direction_output(info->chg_2a_n,    1);
		break;
		}
	} else {
		/* disable */
		if (info->chg_500ma)
			gpio_direction_output(info->chg_500ma,   0);
		if (info->chg_2a_n)
			gpio_direction_output(info->chg_2a_n,    1);
	}

	return 0;
}
EXPORT_SYMBOL(max8903_set_chgcur);

/* initialize max8903 charger */
static int max8903_init_chgcur(struct max8903_charger_info *info)
{
	if (!info) {
		pr_info("max8903 charger info is unavailable!\n");
		return -EINVAL;
	}
	if (info->set_chgcur)
		info->set_chgcur(info->fast_chgcur, 1);
	return 0;
}

static __devinit int max8903_charger_probe(struct platform_device *pdev)
{
	struct max8903_charger_pdata *pdata = pdev->dev.platform_data;
	struct max8903_charger_info *info = NULL;
	int ret = 0;

	if (!pdata) {
		dev_err(&pdev->dev, "platform data is missing !\n");
		ret = -EINVAL;
		goto out;
	}

	info = kzalloc(sizeof(struct max8903_charger_info), GFP_KERNEL);
	if (!info) {
		ret = -ENOMEM;
		goto out;
	}

	/* NOTE: driver don't reject 'gpio_2a_n == 0' for compatibility
	 * gpio_2a_n(GPIO80) not used on brownstone v2 */
	if (pdata->gpio_2a_n) {
		info->chg_2a_n = pdata->gpio_2a_n;
		ret = gpio_request(info->chg_2a_n, "CHG_2A_N");
		if (ret) {
			pr_info("fail to request GPIO:%d !\n", info->chg_2a_n);
			goto out1;
		}
	}

	if (!pdata->gpio_500ma) {
		pr_info("max8903:gpio_500ma is not available !\n");
		ret = -EINVAL;
		goto out2;
	}

	info->chg_500ma = pdata->gpio_500ma;
	ret = gpio_request(info->chg_500ma, "CHG_500MA");
	if (ret) {
		pr_info("fail to request GPIO:%d !\n", info->chg_500ma);
		goto out2;
	}

	info->fast_chgcur = pdata->fast_chgcur;

	info->init_chgcur = max8903_init_chgcur;
	info->set_chgcur = max8903_set_chgcur;
	info->update_charger = max8903_update_charger;

	chg_info = info;
	platform_set_drvdata(pdev, info);

	/* initialize max charging current limitation */
	info->init_chgcur(info);

	return ret;

out2:
	if (info->chg_2a_n)
		gpio_free(info->chg_2a_n);
out1:
	kfree(info);
out:
	return ret;
}

static __devexit int max8903_charger_remove(struct platform_device *pdev)
{
	struct max8903_charger_info *info = platform_get_drvdata(pdev);
	/* disable charging */
	info->set_chgcur(info->fast_chgcur, 0);

	gpio_free(info->chg_2a_n);
	gpio_free(info->chg_500ma);

	platform_set_drvdata(pdev, NULL);
	chg_info = NULL;
	kfree(info);

	return 0;
}

static int max8903_charger_suspend(struct platform_device *pdev, pm_message_t state)
{
	return 0;
}

static int max8903_charger_resume(struct platform_device *pdev)
{
	return 0;
}

static struct platform_driver max8903_charger_driver = {
	.probe   = max8903_charger_probe,
	.remove  = __devexit_p(max8903_charger_remove),
	.suspend = max8903_charger_suspend,
	.resume  = max8903_charger_resume,
	.driver = {
		.name = "max8903-charger",
		.owner = THIS_MODULE,
	},
};

static int __init max8903_charger_init(void)
{
	return platform_driver_register(&max8903_charger_driver);
}
module_init(max8903_charger_init);

static void __exit max8903_charger_exit(void)
{
	platform_driver_unregister(&max8903_charger_driver);
}
module_exit(max8903_charger_exit);


MODULE_AUTHOR("Yunfan Zhang");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Main Battery Charger Driver for Max8903");
MODULE_ALIAS("platform: max8903-charger");
