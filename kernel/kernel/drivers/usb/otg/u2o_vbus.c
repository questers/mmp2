/*
 * VBus driver for Marvell USB
 *
 * Copyright (C) 2010 Marvell International Ltd.
 * 	Haojian Zhuang <haojian.zhuang@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/usb.h>
#include <linux/usb/otg.h>

#include <linux/slab.h>
#include <asm/io.h>
#include <mach/regs-apmu.h>

#include <plat/vbus.h>
#include <plat/pxa_u2o.h>

#ifdef CONFIG_USB_OTG
#include <plat/pxa3xx_otg.h>
#endif

struct u2o_vbus_info {
	struct resource		res;
	struct resource		phy;
	struct device		*dev;
	int			irq;
	int			gpio_en;
	int			irq_oc;	   /* Ellie added for over current detection */
	int			oc;
};

static struct u2o_vbus_info *vbus_data = NULL;

static irqreturn_t u2o_vbus_handler(int irq, void *data)
{
	int status;

	pxa_clear_vbus_status();
	status = pxa_query_vbus();

	/* notify usb driver if vbus event */
	if (status & VBUS_EVENT) {
#ifdef CONFIG_USB_OTG
		struct otg_transceiver *otg = NULL;
		otg = otg_get_transceiver();
		if (otg) {
			otg_interrupt(otg);
		}
#endif
		pxa_vbus_handler(status);
	}
	return IRQ_HANDLED;
}

static int u2o_set_vbus(int enable, int srp)
{
	int ret = 0;

	ret = gpio_request(vbus_data->gpio_en, "VBus En GPIO");
	if (ret) {
		dev_err(vbus_data->dev, "failed to get gpio pin\n");
		goto out;
	}
	// Ellie: hardware changes, vbus will be turned on when gpio is low
	gpio_direction_output(vbus_data->gpio_en, !enable);
	mdelay(5);
	gpio_free(vbus_data->gpio_en);
	/* Ellie added for over current detection */
	if(vbus_data->oc&&enable&&!srp&&vbus_data->irq_oc)
	{
		vbus_data->oc=0;
		enable_irq(vbus_data->irq_oc);
	}

out:
	return ret;
}

/* Ellie added for over current detection */
static irqreturn_t vbus_oc_irq_handler(int irq, void *data)
{
	/* disable irq */
	disable_irq_nosync(irq);
	//printk("oc detected\n");
	vbus_data->oc=1;
	u2o_set_vbus(0,0);
	return IRQ_HANDLED;
}

static int __devinit u2o_vbus_probe(struct platform_device *pdev)
{
	struct u2o_vbus_pdata *pdata = pdev->dev.platform_data;
	struct u2o_vbus_info *vbus;
	struct pxa_vbus_info info;
	unsigned int data;
	int ret = 0;

	if (!pdata) {
		dev_err(&pdev->dev, "platform data is missing\n");
		ret = -EINVAL;
		goto out;
	}

	vbus = kzalloc(sizeof(struct u2o_vbus_info), GFP_KERNEL);
	if (!vbus) {
		ret = -ENOMEM;
		goto out;
	}
	vbus_data = vbus;

	if (!pdata->reg_base || !pdata->reg_end) {
		dev_err(&pdev->dev, "USB register address is missed\n");
		ret = -EINVAL;
		goto out_mem;
	}
	if (!pdata->phy_base || !pdata->phy_end) {
		dev_err(&pdev->dev, "USB PHY register address is missed\n");
		ret = -EINVAL;
		goto out_mem;
	}
	vbus->res.start = pdata->reg_base;
	vbus->res.end = pdata->reg_end;
	vbus->phy.start = pdata->phy_base;
	vbus->phy.end = pdata->phy_end;
	vbus->dev = &pdev->dev;

	memset(&info, 0, sizeof(struct pxa_vbus_info));
	info.set_vbus = u2o_set_vbus;
	info.dev = &pdev->dev;
	info.res = &vbus->res;
	info.phy = &vbus->phy;
	info.phy_init = pdata->phy_init;
	pxa_vbus_init(&info);

	/* VBus is controller by GPIO */
	if (pdata->vbus_en) {
		vbus->gpio_en = pdata->vbus_en;
		u2o_set_vbus(VBUS_LOW, 0);
	}

	if (!pdata->vbus_irq) {
		dev_err(&pdev->dev, "IRQ is missed\n");
		goto out_mount;
	}
	vbus->irq = pdata->vbus_irq;
//	data = VBUS_1MS | VBUS_B_SESSION_VALID
//		| VBUS_B_SESSION_END;
	data = VBUS_1MS;
	pxa_mask_vbus(data);

	ret = request_irq(vbus->irq, u2o_vbus_handler, IRQF_SHARED,
			  "u2o_vbus", vbus);
	if (ret < 0) {
		dev_err(&pdev->dev, "failed to request IRQ on vbus\n");
		ret = -EINVAL;
		goto out_mount;
	}
	
	/* request irq for over current detection GPIO, added by Ellie */
	if (pdata->vbus_oc)
	{
		vbus->irq_oc=gpio_to_irq(pdata->vbus_oc);
		ret = request_irq(vbus->irq_oc, vbus_oc_irq_handler,IRQF_TRIGGER_FALLING,"u2o_vbus_oc", vbus);
		if (ret < 0) {
			  dev_err(&pdev->dev, "failed to request IRQ on vbus over current pin\n");
			  free_irq(vbus->irq, vbus);
			  ret = -EINVAL;
			  goto out_mount;
		}
	}
	else
		vbus->irq_oc=0;
	vbus->oc=0;
	
	platform_set_drvdata(pdev, vbus);

	return ret;

out_mount:
	pxa_vbus_deinit();
out_mem:
	kfree(vbus);
out:
	vbus_data = NULL;
	return ret;
}

static int __devexit u2o_vbus_remove(struct platform_device *pdev)
{
	struct u2o_vbus_info *vbus = platform_get_drvdata(pdev);
	unsigned int data;

	if (vbus) {
		data = VBUS_A_VALID | VBUS_A_SESSION_VALID
			| VBUS_B_SESSION_VALID
			| VBUS_B_SESSION_END;
		pxa_mask_vbus(data);
		if (vbus->gpio_en) {
			gpio_direction_input(vbus->gpio_en);
			gpio_free(vbus->gpio_en);
		}
		if(vbus->irq_oc)
			free_irq(vbus->irq_oc, vbus);
		free_irq(vbus->irq, vbus);
		pxa_vbus_deinit();
		platform_set_drvdata(pdev, NULL);
		kfree(vbus);
		vbus_data = NULL;
	}
	return 0;
}

static struct platform_driver u2o_vbus_driver = {
	.driver		= {
		.name	= "u2o_vbus",
		.owner	= THIS_MODULE,
	},
	.probe		= u2o_vbus_probe,
	.remove		= __devexit_p(u2o_vbus_remove),
};

static int __init u2o_vbus_init(void)
{
	return platform_driver_register(&u2o_vbus_driver);
}
module_init(u2o_vbus_init);

static void __exit u2o_vbus_exit(void)
{
	platform_driver_unregister(&u2o_vbus_driver);
}
module_exit(u2o_vbus_exit);

MODULE_DESCRIPTION("Marvell VBUS driver");
MODULE_AUTHOR("Haojian Zhuang <haojian.zhuang@marvell.com>");
MODULE_LICENSE("GPL");
