/*
 * max8925 PMIC vbus driver for Marvell USB.
 *
 * Copyright (C) 2010 Marvell International Ltd.
 * Yunfan Zhang <yfzhang@marvell.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/mfd/max8925.h>
#include <plat/vbus.h>

struct max8925_vbus_info {
	struct max8925_chip	*chip;
	struct resource res;
	int	irq_r;
	int	irq_f;
	int status;
};

static struct max8925_vbus_info *vbus_data;

#ifdef CONFIG_MAX8925_POWER
extern irqreturn_t max8925_charger_handler(int irq, void *data);
#endif

static irqreturn_t max8925_vbus_handler(int irq, void *data)
{
	struct max8925_vbus_info *vbus = vbus_data;
	struct max8925_chip *chip;

	if (!vbus) {
		pr_debug("vbus data is not available!");
		return IRQ_HANDLED;
	}

	chip = vbus->chip;

	switch (irq - chip->irq_base) {
	case MAX8925_IRQ_VCHG_DC_R:
		vbus->status = VBUS_HIGH;
		break;
	case MAX8925_IRQ_VCHG_DC_F:
	default:
		vbus->status = VBUS_LOW;
		break;
	}

	/* handle vbus event */
	pxa_vbus_handler(vbus->status);

#ifdef CONFIG_MAX8925_POWER
	/* notify max8925 power driver to handle charger in/out events */
	max8925_charger_handler(irq, data);
#endif

	return IRQ_HANDLED;
}

static int __devinit max8925_vbus_probe(struct platform_device *pdev)
{
	struct max8925_chip *chip = dev_get_drvdata(pdev->dev.parent);
	struct max8925_platform_data *max8925_pdata;
	struct max8925_vbus_pdata *pdata;
	struct max8925_vbus_info *vbus = NULL;
	struct pxa_vbus_info info;
	int ret = 0;

	max8925_pdata = pdev->dev.parent->platform_data;
	if (!max8925_pdata) {
		dev_err(&pdev->dev, "max8925 pdata is not available!\n");
		return -EINVAL;
	}

	pdata = max8925_pdata->vbus;
	if (!pdata) {
		dev_err(&pdev->dev, "max8925 vbus pdata is not available!\n");
		return -EINVAL;
	}

	vbus = kzalloc(sizeof(struct max8925_vbus_info), GFP_KERNEL);
	if (!vbus)
		return -ENOMEM;

	vbus->chip = chip;
	vbus->res.start = pdata->reg_base;
	vbus->res.end = pdata->reg_end;

	vbus->irq_r = chip->irq_base + pdata->irq_rise;
	vbus->irq_f = chip->irq_base + pdata->irq_fall;

	vbus_data = vbus;
	platform_set_drvdata(pdev, vbus);

	ret = request_threaded_irq(vbus->irq_r, NULL, \
					max8925_vbus_handler, \
					IRQF_ONESHOT, "max8925-vbus-rise", vbus);
	if (ret) {
		dev_err(chip->dev, "Failed to request IRQ #%d: %d\n", \
					vbus->irq_r, ret);
		ret = -EINVAL;
		goto out_irq1;
	}

	ret = request_threaded_irq(vbus->irq_f, NULL, \
					max8925_vbus_handler, \
					IRQF_ONESHOT, "max8925-vbus-fall", vbus);
	if (ret) {
		dev_err(chip->dev, "Failed to request IRQ #%d: %d\n", \
						vbus->irq_f, ret);
		ret = -EINVAL;
		goto out_irq2;
	}

	memset(&info, 0, sizeof(struct pxa_vbus_info));

	info.set_vbus = pdata->set_vbus;
	info.dev = chip->dev;
	info.res = &vbus->res;
	pxa_vbus_init(&info);

	/* set vbus low by default */
	pxa_set_vbus(VBUS_LOW, 0);

	return 0;

out_irq2:
	free_irq(vbus->irq_r, vbus);
out_irq1:
	platform_set_drvdata(pdev, NULL);
	vbus_data = NULL;
	kfree(vbus);
	return ret;
}

static int __devexit max8925_vbus_remove(struct platform_device *pdev)
{
	struct max8925_vbus_info *vbus = platform_get_drvdata(pdev);

	if (!vbus) {
		dev_err(&pdev->dev, "max8925 vbus info is not available!\n");
		return -EINVAL;
	}

	free_irq(vbus->irq_r, vbus);
	free_irq(vbus->irq_f, vbus);
	pxa_vbus_deinit();
	platform_set_drvdata(pdev, NULL);
	vbus_data = NULL;
	kfree(vbus);

	return 0;
}

static struct platform_driver max8925_vbus_driver = {
	.driver		= {
		.name	= "max8925-vbus",
		.owner	= THIS_MODULE,
	},
	.probe		= max8925_vbus_probe,
	.remove		= __devexit_p(max8925_vbus_remove),
};

static int __init max8925_vbus_init(void)
{
	return platform_driver_register(&max8925_vbus_driver);
}
module_init(max8925_vbus_init);

static void __exit max8925_vbus_exit(void)
{
	platform_driver_unregister(&max8925_vbus_driver);
}
module_exit(max8925_vbus_exit);

MODULE_DESCRIPTION("Marvell max8925 PMIC vbus driver");
MODULE_AUTHOR("Yunfan Zhang <yfzhang@marvell.com>");
MODULE_LICENSE("GPL");
