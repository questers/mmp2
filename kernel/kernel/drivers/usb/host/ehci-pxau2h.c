/*
 * (C) Marvell Inc. 2008 (kvedere@marvell.com)
 * Code Based on ehci-fsl.c & ehci-pxa9xx.c
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <plat/pxa_u2o.h>
#include <mach/regs-apmu.h>
#include <linux/usb/ulpi.h>

struct pxau2h_ehci_hcd {
	struct ehci_hcd *ehci;
	struct clk *clk;
	struct pxa_usb_plat_info *info;
	enum usb_port_speed port_speed;
#ifdef CONFIG_PROC_FS
	struct proc_dir_entry *u2h_dump;
#endif
	unsigned int ulpi_viewport;
};

/* ulpi viewport register operation */
#define ULPI_VIEW_WAKEUP	(1 << 31)
#define ULPI_VIEW_RUN		(1 << 30)
#define ULPI_VIEW_WRITE		(1 << 29)
#define ULPI_VIEW_READ		(0 << 29)
#define ULPI_VIEW_ADDR(x)	(((x) & 0xff) << 16)
#define ULPI_VIEW_DATA_READ(x)	(((x) >> 8) & 0xff)
#define ULPI_VIEW_DATA_WRITE(x)	((x) & 0xff)

static int ulpi_viewport_wait(u32 *view, u32 mask)
{
	unsigned long usec = 5000;

	while (usec--) {
		if (!(readl(view) & mask))
			return 0;

		udelay(1);
	};
	pr_err("%s:ulpi operation timeout\n", __func__);
	return -ETIMEDOUT;
}

static int ulpi_viewport_read(u32 *view, u32 reg)
{
	int ret;

	writel(ULPI_VIEW_WAKEUP | ULPI_VIEW_WRITE, view);
	ret = ulpi_viewport_wait(view, ULPI_VIEW_WAKEUP);
	if (ret)
		return ret;

	writel(ULPI_VIEW_RUN | ULPI_VIEW_READ | ULPI_VIEW_ADDR(reg), view);
	ret = ulpi_viewport_wait(view, ULPI_VIEW_RUN);
	if (ret)
		return ret;

	return ULPI_VIEW_DATA_READ(readl(view));
}

static int ulpi_viewport_write(u32 *view, u32 val, u32 reg)
{
	int ret;

	writel(ULPI_VIEW_WAKEUP | ULPI_VIEW_WRITE, view);
	ret = ulpi_viewport_wait(view, ULPI_VIEW_WAKEUP);
	if (ret)
		return ret;

	writel(ULPI_VIEW_RUN | ULPI_VIEW_WRITE | ULPI_VIEW_DATA_WRITE(val) |
			ULPI_VIEW_ADDR(reg), view);

	return ulpi_viewport_wait(view, ULPI_VIEW_RUN);
}

/* called during probe() after chip reset completes */
static int pxau2h_ehci_setup(struct usb_hcd *hcd)
{
	struct ehci_hcd *ehci = hcd_to_ehci(hcd);
	int retval;

	/* EHCI registers start at offset 0x100 */
	ehci->caps = hcd->regs + U2x_CAPREGS_OFFSET;
	ehci->regs = hcd->regs + U2x_CAPREGS_OFFSET +
	    HC_LENGTH(ehci_readl(ehci, &ehci->caps->hc_capbase));
	dbg_hcs_params(ehci, "reset");
	dbg_hcc_params(ehci, "reset");

	/* cache this readonly data; minimize chip reads */
	ehci->hcs_params = ehci_readl(ehci, &ehci->caps->hcs_params);

	retval = ehci_halt(ehci);
	if (retval)
		return retval;

	/* data structure init */
	retval = ehci_init(hcd);
	if (retval)
		return retval;

	hcd->has_tt = 1;
	ehci->sbrn = 0x20;
	ehci_reset(ehci);
	ehci_port_power(ehci, 1);

	return retval;
}

static const struct hc_driver pxau2h_ehci_hc_driver = {
	.description = hcd_name,
	.product_desc = "Marvell PXA SOC EHCI Host Controller",
	.hcd_priv_size = sizeof(struct ehci_hcd),

	/*
	 * generic hardware linkage
	 */
	.irq = ehci_irq,
	.flags = HCD_MEMORY | HCD_USB2,

	/*
	 * basic lifecycle operations
	 */
	.reset = pxau2h_ehci_setup,
	.start = ehci_run,
#ifdef CONFIG_PM
	.bus_suspend = ehci_bus_suspend,
	.bus_resume = ehci_bus_resume,
#endif
	.stop = ehci_stop,
	.shutdown = ehci_shutdown,

	/*
	 * managing i/o requests and associated device resources
	 */
	.urb_enqueue = ehci_urb_enqueue,
	.urb_dequeue = ehci_urb_dequeue,
	.endpoint_disable = ehci_endpoint_disable,
	.endpoint_reset = ehci_endpoint_reset,
	.clear_tt_buffer_complete = ehci_clear_tt_buffer_complete,

	/*
	 * scheduling support
	 */
	.get_frame_number = ehci_get_frame,

	/*
	 * root hub support
	 */
	.hub_status_data = ehci_hub_status_data,
	.hub_control = ehci_hub_control,

	.relinquish_port = ehci_relinquish_port,
	.port_handed_over = ehci_port_handed_over,
};

#ifdef CONFIG_PROC_FS
static void u2h_regs_dump(struct pxau2h_ehci_hcd *pxau2h)
{
	u32 offset, base;

	pr_info("U2H PHY select:\n");
	pr_info("\t %2X: 0x%8X\n", pxau2h->info->phybase,
		__raw_readl(pxau2h->info->phybase + 0x4));

	pr_info("U2H regs:\n");
	base = pxau2h->info->regbase;
	for (offset = 0; offset < 0x210; offset += 4) {
		pr_info("\t %2X: 0x%8X\n", base + offset,
			u2o_get(base, offset));
	}
}

static int u2h_proc_write(struct file *file, const char *buffer,
			unsigned long count, void *data)
{
	char kbuf[8];
	unsigned int index, i;
	struct pxau2h_ehci_hcd *pxau2h = data;

	if (count >= 8)
		return -EINVAL;
	if (copy_from_user(kbuf, buffer, count))
		return -EFAULT;
	index = (int)simple_strtoul(kbuf, NULL, 10);

	switch (index) {
	case 1:
		/* ulpi viewport register */
		if (pxau2h->info->phy_type == USB_PHY_TYPE_ULPI) {
			for (i = 0; i < 0x30; i++)
				pr_info("addr 0x%x: 0x%x\n",
					i, ulpi_viewport_read(
					(u32 *)pxau2h->ulpi_viewport, i));
		}
		break;
	case 3:
		/* u2h controller register */
		u2h_regs_dump(pxau2h);
		break;
	default:
		return -EINVAL;
	}

	return count;
}

static int u2h_proc_read(char *buffer, char **buffer_location, off_t offset,
			int buffer_length, int *zero, void *ptr)
{
	if (offset > 0)
		return 0;

	return sprintf(buffer, "%s\n", __func__);
}
#endif

static int pxau2h_ehci_probe(struct platform_device *pdev)
{
	struct resource *res;
	struct clk *clk = NULL;
	struct usb_hcd *hcd;
	struct pxau2h_ehci_hcd *pxau2h;
	struct pxa_usb_plat_info *plat_info;
	int irq, retval = 0, tmp;

	dev_dbg(&pdev->dev, "Initializing PXA EHCI-SOC USB Controller(U2H)\n");

	plat_info = pdev->dev.platform_data;
	if (!plat_info) {
		dev_err(&pdev->dev, "Platform data missing\n");
		return -EINVAL;
	}

	pxau2h = kzalloc(sizeof(struct pxau2h_ehci_hcd), GFP_KERNEL);
	if (!pxau2h) {
		dev_err(&pdev->dev, "Failed to allocate pxau2h\n");
		return -ENOMEM;
	}

	pxau2h->info = plat_info;

	/* Create and initialize an HCD structure */
	hcd = usb_create_hcd(&pxau2h_ehci_hc_driver, &pdev->dev, "pxa-u2h");
	if (!hcd) {
		dev_err(&pdev->dev, "Failed to create HCD(usb_create_hcd)\n");
		retval = -ENOMEM;
		goto err_usb_create_hcd;
	}

	/* Get u2h controller register address */
	res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "u2h");
	if (!res) {
		dev_err(&pdev->dev, "Failed to get u2h memory\n");
		retval = -ENXIO;
		goto err_get_res_u2h;
	}
	pxau2h->info->regbase =
		(unsigned)ioremap_nocache(res->start, res_size(res));
	if (!pxau2h->info->regbase) {
		dev_err(&pdev->dev, "Failed to remap u2h register\n");
		retval = -ENOMEM;
		goto err_ioremap_u2h;
	}

	/* ulpi viewport register address */
	pxau2h->ulpi_viewport =
		pxau2h->info->regbase + U2x_ULPI_VIEWPORT_OFFSET;

	hcd->rsrc_start = res->start;
	hcd->rsrc_len = resource_size(res);
	hcd->regs = (void __iomem *)pxau2h->info->regbase;

	/* Get u2h phy register address */
	res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "u2hphy");
	if (!res) {
		dev_err(&pdev->dev, "Failed to get u2hphy memory\n");
		retval = -ENXIO;
		goto err_get_res_u2hphy;
	}
	pxau2h->info->phybase =
		(unsigned)ioremap_nocache(res->start, res_size(res));
	if (!pxau2h->info->phybase) {
		dev_err(&pdev->dev, "Failed to remap u2hphy register\n");
		retval = -ENODEV;
		goto err_ioremap_u2hphy;
	}

	/* Get u2h irq */
	irq = platform_get_irq(pdev, 0);
	if (!irq) {
		dev_err(&pdev->dev, "Cannot get irq %x\n", irq);
		retval = -ENODEV;
		goto err_get_irq;
	}
	hcd->irq = (unsigned int)irq;

	/* Get u2h clock */
	if (!pxau2h->info->clk_name) {
		dev_err(&pdev->dev, "Failed to get clk name\n");
		retval = -ENODEV;
		goto err_get_clkname;
	}
	clk = clk_get(NULL, pxau2h->info->clk_name);
	if (IS_ERR(clk)) {
		dev_err(&pdev->dev, "Failed to get USB clk\n");
		retval = PTR_ERR(clk);
		goto err_clk_get;
	}
	pxau2h->clk = clk;

	dev_info(&pdev->dev,
		"u2h regbase 0x%x phybase 0x%x irq %d, clk: %s\n",
		pxau2h->info->regbase, pxau2h->info->phybase,
		irq, pxau2h->info->clk_name);

	if (pxau2h->info->set_power && pxau2h->info->set_power(1)) {
		dev_err(&pdev->dev, "set_power failed\n");
		retval = -EBUSY;
		goto err_set_power;
	}

	/* Enable u2h clock */
	clk_enable(pxau2h->clk);

	/* Init u2h PHY */
	if (pxau2h->info->phy_init
		&& pxau2h->info->phy_init(pxau2h->info->regbase,
					pxau2h->info->phybase)) {
		dev_err(&pdev->dev, "Failed to init usb 2.0 host controller\n");
		retval = -EBUSY;
		goto err_phy_init;
	}

	pxau2h->ehci = hcd_to_ehci(hcd);
	platform_set_drvdata(pdev, pxau2h);

	/* @USBCMD, Reset USB core */
	tmp = readl(hcd->regs + U2xUSBCMD);
	tmp |= U2xUSBCMD_RST;
	writel(tmp, hcd->regs + U2xUSBCMD);
	msleep(100);

	/* Finish generic HCD structure initialization and register */
	retval = usb_add_hcd(hcd, hcd->irq, IRQF_DISABLED | IRQF_SHARED);
	if (retval) {
		dev_err(&pdev->dev, "Failed to register HCD(usb_add_hcd)\n");
		goto err_usb_add_hcd;
	}

	/* Enable platform vbus, etc */
	if (pxau2h->info->plat_init
		&& pxau2h->info->plat_init(pxau2h->info->regbase,
					pxau2h->info->phybase)) {
		dev_err(&pdev->dev, "Failed to power USB Host Controller\n");
		retval = -EBUSY;
		goto err_plat_init;
	}

#ifdef CONFIG_PROC_FS
	pxau2h->u2h_dump = create_proc_entry("u2h", 0666, NULL);
	if (pxau2h->u2h_dump) {
		pxau2h->u2h_dump->read_proc = u2h_proc_read;
		pxau2h->u2h_dump->write_proc = u2h_proc_write;
		pxau2h->u2h_dump->data = pxau2h;
	} else
		dev_err(&pdev->dev, "Failed to create proc entry\n");
#endif

	return 0;

err_plat_init:
	usb_remove_hcd(hcd);
err_usb_add_hcd:
err_phy_init:
	clk_disable(pxau2h->clk);
err_set_power:
err_clk_get:
err_get_clkname:
err_get_irq:
	iounmap((void *)pxau2h->info->phybase);
err_ioremap_u2hphy:
err_get_res_u2hphy:
	iounmap((void *)pxau2h->info->regbase);
err_ioremap_u2h:
err_get_res_u2h:
	usb_put_hcd(hcd);
err_usb_create_hcd:
	kfree(pxau2h);

	dev_err(&pdev->dev, "Init %s failed, error no:%d\n", "pxa u2h", retval);
	return retval;
}

static int pxau2h_ehci_remove(struct platform_device *pdev)
{
	struct pxau2h_ehci_hcd *pxau2h = platform_get_drvdata(pdev);
	struct usb_hcd *hcd = ehci_to_hcd(pxau2h->ehci);

	if (pxau2h->u2h_dump)
		remove_proc_entry("u2h", NULL);

	if (HC_IS_RUNNING(hcd->state))
		hcd->state = HC_STATE_QUIESCING;
	usb_disconnect(&hcd->self.root_hub);
	hcd->driver->stop(hcd);

	usb_remove_hcd(hcd);
	clk_disable(pxau2h->clk);
	iounmap(hcd->regs);
	iounmap((void *)pxau2h->info->phybase);
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
	usb_put_hcd(hcd);
	kfree(pxau2h);

	return 0;
}

#ifdef CONFIG_PM
static int pxau2h_driver_suspend(struct platform_device *pdev,
				pm_message_t message)
{
	struct pxau2h_ehci_hcd *pxau2h = platform_get_drvdata(pdev);
	struct ehci_hcd *ehci = pxau2h->ehci;
	struct usb_hcd *hcd = ehci_to_hcd(ehci);
	struct ehci_regs __iomem *hc_reg = ehci->regs;
	unsigned long flags;
	int rc = 0;

	if (time_before(jiffies, ehci->next_statechange))
		msleep(10);

	/* Root hub was already suspended. Disable irq emission and
	 * mark HW unaccessible, bail out if RH has been resumed. Use
	 * the spinlock to properly synchronize with possible pending
	 * RH suspend or resume activity.
	 *
	 * This is still racy as hcd->state is manipulated outside of
	 * any locks =P But that will be a different fix.
	 */
	spin_lock_irqsave(&ehci->lock, flags);
	if (hcd->state != HC_STATE_SUSPENDED) {
		rc = -EINVAL;
		goto bail;
	}
	/* Store port speed before suspend */
	pxau2h->port_speed =
			(ehci_readl(ehci, &hc_reg->port_status[0]) >> 26) & 0x3;

	ehci_writel(ehci, 0, &ehci->regs->intr_enable);
	(void)ehci_readl(ehci, &ehci->regs->intr_enable);

	clear_bit(HCD_FLAG_HW_ACCESSIBLE, &hcd->flags);

bail:
	spin_unlock_irqrestore(&ehci->lock, flags);

	/* Place ulpi in low power mode.
	 * Clear SuspendM bit in function control register */
	if (pxau2h->info->phy_type == USB_PHY_TYPE_ULPI) {
		ulpi_viewport_write((void *)pxau2h->ulpi_viewport,
			ULPI_FUNC_CTRL_SUSPENDM, ULPI_FUNC_CTRL + 0x2);
	}

	if (pxau2h->info->set_power && pxau2h->info->set_power(0)) {
		pr_err("%s set_power failed\n", __func__);
		return -EIO;
	}

	clk_disable(pxau2h->clk);

	return rc;
}

static int pxau2h_driver_resume(struct platform_device *pdev)
{
	struct pxau2h_ehci_hcd *pxau2h = platform_get_drvdata(pdev);
	struct ehci_hcd *ehci = pxau2h->ehci;
	struct usb_hcd *hcd = ehci_to_hcd(ehci);
	struct ehci_regs __iomem *hc_reg = ehci->regs;
	int val = 0;

	if (pxau2h->info->set_power && pxau2h->info->set_power(1)) {
		pr_err("%s set_power failed\n", __func__);
		return -EIO;
	}

	/* This delay is necessary for stability */
	msleep(5);

	clk_enable(pxau2h->clk);

	if (pxau2h->info->phy_init
		&& pxau2h->info->phy_init(pxau2h->info->regbase,
					pxau2h->info->phybase)) {
		pr_err("%s phy_init failed\n", __func__);
		return -EIO;
	}

	if (pxau2h->info->phy_type == USB_PHY_TYPE_HSIC){
		/* hsic need plat_init after suspend */
		if (pxau2h->info->plat_init
			&& pxau2h->info->plat_init(pxau2h->info->regbase,
						pxau2h->info->phybase)) {
			pr_err("Unable to power USB Host Controller.\n");
			return -EBUSY;
		}
	}

	if (time_before(jiffies, ehci->next_statechange))
		udelay(100);

	/* Set HCD accessible */
	set_bit(HCD_FLAG_HW_ACCESSIBLE, &hcd->flags);
	/* Enable USB Port Power */
	val = ehci_readl(ehci, &hc_reg->port_status[0]);
	val |= PORT_POWER;
	ehci_writel(ehci, val, &hc_reg->port_status[0]);
	udelay(20);

	if (pxau2h->port_speed >= USB_PORT_SPEED_UNKNOWN)
		goto done;
	/* Force USB contorller to port_speed before suspend */
	if (!ehci_readl(ehci, &hc_reg->async_next)) {
		val = ehci_readl(ehci, &hc_reg->port_status[0]);
		val &= ~(0xf << 16);
		if (pxau2h->port_speed == USB_PORT_SPEED_HIGH) {
			/* High Speed Mode */
			val |= 0x5 << 16;
		} else if (pxau2h->port_speed == USB_PORT_SPEED_FULL) {
			/* Full Speed Mode */
			val |= 0x6 << 16;
		} else if (pxau2h->port_speed == USB_PORT_SPEED_LOW) {
			/* Low Speed Mode */
			val |= 0x7 << 16;
		}
		ehci_writel(ehci, val, &hc_reg->port_status[0]);
		udelay(20);
		/* Disable Test Mode */
		val = ehci_readl(ehci, &hc_reg->port_status[0]);
		val &= ~(0xf << 16);
		ehci_writel(ehci, val, &hc_reg->port_status[0]);
		udelay(20);
	}

	/* Wait for PORT_CONNECT assert */
	if (handshake(ehci, &hc_reg->port_status[0],
				PORT_CONNECT, PORT_CONNECT, 5000)) {
		pr_err("%s:waiting for PORT_CONNECT timeout!\n", __func__);
		goto done;
	}
	/* Wait for PORT_PE assert */
	if (handshake(ehci, &hc_reg->port_status[0],
				PORT_PE, PORT_PE, 5000)) {
		pr_err("%s:waiting for PORT_PE timeout!\n", __func__);
		goto done;
	}

	/* Port change detect */
	val = ehci_readl(ehci, &hc_reg->status);
	val |= STS_PCD;
	ehci_writel(ehci, val, &hc_reg->status);

	/* Place USB Controller in Suspend Mode,
	 * will resume later */
	val = ehci_readl(ehci, &hc_reg->port_status[0]);
	if ((val & PORT_POWER) && (val & PORT_PE)) {
		val |= PORT_SUSPEND;
		ehci_writel(ehci, val, &hc_reg->port_status[0]);
		/* Wait for PORT_SUSPEND assert */
		if (handshake(ehci, &hc_reg->port_status[0],
					PORT_SUSPEND, PORT_SUSPEND, 5000)) {
			pr_err("%s:waiting for PORT_SUSPEND timeout!\n",
					__func__);
			goto done;
		}
	}
done:
	return 0;
}
#endif

static void pxau2h_ehci_hcd_shutdown(struct platform_device *pdev)
{
	struct pxau2h_ehci_hcd *pxau2h = platform_get_drvdata(pdev);
	struct usb_hcd *hcd = ehci_to_hcd(pxau2h->ehci);

	if (hcd->driver->shutdown)
		hcd->driver->shutdown(hcd);
}

MODULE_ALIAS("pxau2h-ehci");

static struct platform_driver pxau2h_ehci_driver = {
	.probe = pxau2h_ehci_probe,
	.remove = pxau2h_ehci_remove,
	.shutdown = pxau2h_ehci_hcd_shutdown,
	.driver = {
		.name = "pxau2h-ehci",
		.bus = &platform_bus_type,
	},
#ifdef CONFIG_PM
	.suspend = pxau2h_driver_suspend,
	.resume = pxau2h_driver_resume,
#endif
};
