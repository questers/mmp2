/*
 * Copyright (c) 2008 Marvell Technology Ltd.
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/uaccess.h>
#include <linux/proc_fs.h>
#include <plat/pxa_u2o.h>
#ifdef CONFIG_USB_OTG
#include <linux/usb/otg.h>
#endif
#include <plat/vbus.h>
#ifdef CONFIG_DVFM
#include <mach/dvfm.h>
#endif

struct pxau2o_ehci_hcd {
	struct ehci_hcd *ehci;
	struct clk *clk;
	unsigned int regbase;
	struct workqueue_struct *switch_wq;
	struct work_struct rm_hcd_work;
	struct work_struct add_hcd_work;
#ifdef CONFIG_DVFM
	struct dvfm_lock dvfm_lock;
#endif
#ifdef CONFIG_PROC_FS
	struct proc_dir_entry *u2o_dump;
#endif
};

#ifdef CONFIG_PROC_FS
static void u2o_regs_dump(struct pxau2o_ehci_hcd *pxau2o)
{
	u32 offset, base;

	clk_enable(pxau2o->clk);
	pr_info("U2O regs:\n");
	base = pxau2o->regbase;
	for (offset = 0; offset < 0x210; offset += 4) {
		pr_info("\t %2X: 0x%8X\n", base + offset,
			u2o_get(base, offset));
	}
	clk_disable(pxau2o->clk);
}

static int u2o_proc_write(struct file *file, const char *buffer,
			unsigned long count, void *data)
{
	u8 kbuf[8];
	unsigned int index;
	struct pxau2o_ehci_hcd *pxau2o = data;

	if (count >= 8)
		return -EINVAL;
	if (copy_from_user(kbuf, buffer, count))
		return -EFAULT;
	index = (int)simple_strtoul(kbuf, NULL, 10);

	switch (index) {
	case 3:
		/* Dump u2o controller registers */
		u2o_regs_dump(pxau2o);
		break;
	default:
		return -EINVAL;
	}

	return count;
}

static int u2o_proc_read(char *buffer, char **buffer_location, off_t offset,
			int buffer_length, int *zero, void *ptr)
{
	if (offset > 0)
		return 0;
	return sprintf(buffer, "%s\n", __func__);
}
#endif

#ifdef CONFIG_DVFM
static void set_dvfm_constraint(struct pxau2o_ehci_hcd *pxau2o)
{
	struct dvfm_lock *dvfm_lock = &pxau2o->dvfm_lock;

	if (dvfm_lock->count == 0) {
		/* disable low power mode */
		dvfm_lock->count++;
		dvfm_disable_op_name("apps_idle", dvfm_lock->dev_idx);
		dvfm_disable_op_name("apps_sleep", dvfm_lock->dev_idx);
		dvfm_disable_op_name("chip_sleep", dvfm_lock->dev_idx);
		dvfm_disable_op_name("sys_sleep", dvfm_lock->dev_idx);
	}
}

static void unset_dvfm_constraint(struct pxau2o_ehci_hcd *pxau2o)
{
	struct dvfm_lock *dvfm_lock = &pxau2o->dvfm_lock;

	if (dvfm_lock->count == 1) {
		/* enable low power mode */
		dvfm_lock->count--;
		dvfm_enable_op_name("apps_idle", dvfm_lock->dev_idx);
		dvfm_enable_op_name("apps_sleep", dvfm_lock->dev_idx);
		dvfm_enable_op_name("chip_sleep", dvfm_lock->dev_idx);
		dvfm_enable_op_name("sys_sleep", dvfm_lock->dev_idx);
	}
}
#endif

static void mv_remove_hcd_work(struct work_struct *work)
{
	struct pxau2o_ehci_hcd *pxau2o =
		container_of(work, struct pxau2o_ehci_hcd, rm_hcd_work);
	struct usb_hcd *hcd = ehci_to_hcd(pxau2o->ehci);

	if (hcd->rh_registered) {
		usb_remove_hcd(hcd);
		pxa_set_usbdev(USB_B_DEVICE);
#ifdef CONFIG_DVFM
		unset_dvfm_constraint(pxau2o);
#endif
	}

}

static void mv_add_hcd_work(struct work_struct *work)
{
	struct pxau2o_ehci_hcd *pxau2o =
		container_of(work, struct pxau2o_ehci_hcd, add_hcd_work);
	struct usb_hcd *hcd = ehci_to_hcd(pxau2o->ehci);
	int ret = 0;

#ifdef CONFIG_DVFM
	set_dvfm_constraint(pxau2o);
#endif
	if (hcd->rh_registered) {
		pr_debug("%s not removed???\n", __func__);
		usb_remove_hcd(hcd);
	}

	ret = usb_add_hcd(hcd, hcd->irq, IRQF_DISABLED | IRQF_SHARED);
	if (ret < 0) {
		pr_err("%s:usb_add_hcd failed\n", __func__);
#ifdef CONFIG_DVFM
		unset_dvfm_constraint(pxau2o);
#endif
	}
}

static struct pxau2o_ehci_hcd *g_pxau2o;
/* Invoked in pxa_u2o.c */
void pxa9xx_ehci_set(int enable)
{
	static int enabled;

	if (!g_pxau2o) {
		pr_err("%s:g_pxau2o is not available\n", __func__);
		return;
	}

	pr_debug("%s %s enabled %d\n", __func__,
		 enable ? "enable" : "disable", enabled);
	if ((!!enable) != enabled) {
		if (enable) {
			queue_work(g_pxau2o->switch_wq,
					&g_pxau2o->add_hcd_work);
			enabled = 1;
		} else {
			queue_work(g_pxau2o->switch_wq,
					&g_pxau2o->rm_hcd_work);
			enabled = 0;
		}
	}
}

static void pxau2o_host_enable(struct usb_hcd *hcd)
{
	u32 base = (u32)hcd->regs;
#ifndef CONFIG_USB_OTG
	/* Turn off IDPU bit of U2xOTGSC - we are not in OTG mode */
	u2o_set(base, U2xOTGSC, U2xOTGSC_IDPU);
#endif
	/* Set hardware to Host Mode: 0x3 */
	u2o_set(base, U2xUSBMODE, U2xUSBMODE_CM_MASK);
}

static int pxau2o_ehci_setup(struct usb_hcd *hcd)
{
	struct ehci_hcd *ehci = hcd_to_ehci(hcd);
	int retval;

	/* Enable Host Mode */
	pxau2o_host_enable(hcd);
	/* EHCI registers start at offset 0x100 */
	ehci->caps = hcd->regs + U2x_CAPREGS_OFFSET;
	ehci->regs = hcd->regs + U2x_CAPREGS_OFFSET +
	    HC_LENGTH(ehci_readl(ehci, &ehci->caps->hc_capbase));
	dbg_hcs_params(ehci, "reset");
	dbg_hcc_params(ehci, "reset");

	/* Cache this readonly data; minimize chip reads */
	ehci->hcs_params = ehci_readl(ehci, &ehci->caps->hcs_params);

	retval = ehci_halt(ehci);
	if (retval)
		return retval;

	/* Data structure init */
	retval = ehci_init(hcd);
	if (retval)
		return retval;

	hcd->has_tt = 1;
	ehci->sbrn = 0x20;
	ehci_reset(ehci);
	ehci_port_power(ehci, 1);

	return retval;
}

static irqreturn_t pxau2o_ehci_irq(struct usb_hcd *hcd)
{
#ifdef CONFIG_USB_OTG
	if (!otg_is_host()) {
		int start = hcd->state;
		pr_debug("\n%s start %x\n", __func__, start);
		return IRQ_NONE;
	}
#endif

	return ehci_irq(hcd);
}

#ifdef CONFIG_USB_OTG
static int pxau2o_ehci_connect(struct usb_hcd *hcd, struct usb_device *udev)
{
	return otg_connect((hcd_to_ehci(hcd))->transceiver, udev);
}

static int pxau2o_ehci_disconnect(struct usb_hcd *hcd)
{
	return otg_disconnect((hcd_to_ehci(hcd))->transceiver);
}
#endif

static const struct hc_driver pxau2o_ehci_hc_driver = {
	.description = hcd_name,
	.product_desc = "Marvell PXA OTG EHCI Host Controller",
	.hcd_priv_size = sizeof(struct ehci_hcd),

	/* generic hardware linkage */
	.irq = pxau2o_ehci_irq,
	.flags = HCD_MEMORY | HCD_USB2,

	/* basic lifecycle operations */
	.reset = pxau2o_ehci_setup,
	.start = ehci_run,
	.stop = ehci_stop,
	.shutdown = ehci_shutdown,

	/* managing i/o requests and associated device resources */
	.urb_enqueue = ehci_urb_enqueue,
	.urb_dequeue = ehci_urb_dequeue,
	.endpoint_disable = ehci_endpoint_disable,
	.endpoint_reset = ehci_endpoint_reset,
	.clear_tt_buffer_complete = ehci_clear_tt_buffer_complete,

	/* scheduling support */
	.get_frame_number = ehci_get_frame,

	/* root hub support */
	.hub_status_data = ehci_hub_status_data,
	.hub_control = ehci_hub_control,
#ifdef CONFIG_PM
	.bus_suspend = ehci_bus_suspend,
	.bus_resume = ehci_bus_resume,
#endif

#ifdef CONFIG_USB_OTG
	.disconnect = pxau2o_ehci_disconnect,
	.connect = pxau2o_ehci_connect,
#endif
};

static int pxau2o_ehci_probe(struct platform_device *pdev)
{
	struct usb_hcd *hcd;
	struct ehci_hcd *ehci;
	struct pxau2o_ehci_hcd *pxau2o;
	struct resource *res;
	struct clk *clk = NULL;
	int irq, retval = 0;

	if (usb_disabled())
		return -ENODEV;

	pxau2o = kzalloc(sizeof(struct pxau2o_ehci_hcd), GFP_KERNEL);
	if (!pxau2o) {
		dev_err(&pdev->dev, "Failed to allocate pxau2o\n");
		return -ENOMEM;
	}

	/* Create and initialize an HCD structure */
	hcd = usb_create_hcd(&pxau2o_ehci_hc_driver, &pdev->dev, "pxau2o-ehci");
	if (!hcd) {
		dev_err(&pdev->dev, "Failed to create HCD(usb_create_hcd)\n");
		retval = -ENOMEM;
		goto err_usb_create_hcd;
	}

	/* Get u2o controller register address */
	res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "u2o");
	if (!res) {
		dev_err(&pdev->dev, "Failed to get u2o memory\n");
		retval = -ENXIO;
		goto err_get_res_u2o;
	}
	pxau2o->regbase =
		(unsigned)ioremap_nocache(res->start, resource_size(res));
	if (!pxau2o->regbase) {
		dev_err(&pdev->dev, "Failed to remap u2o register\n");
		retval = -ENOMEM;
		goto err_ioremap_u2o;
	}

	hcd->rsrc_start = res->start;
	hcd->rsrc_len = resource_size(res);
	hcd->regs = (void __iomem *)pxau2o->regbase;

	/* Get u2o irq */
	irq = platform_get_irq(pdev, 0);
	if (!irq) {
		dev_err(&pdev->dev, "Cannot get irq %x\n", irq);
		retval = -ENODEV;
		goto err_get_irq;
	}
	hcd->irq = (unsigned int)irq;

	/* Get u2o clock */
	clk = clk_get(NULL, "USBCLK");
	if (IS_ERR(clk)) {
		dev_err(&pdev->dev, "Failed to get USB clk\n");
		retval = PTR_ERR(clk);
		goto err_clk_get;
	}
	pxau2o->clk = clk;

	/* Enable u2o clock */
	clk_enable(pxau2o->clk);

	ehci = hcd_to_ehci(hcd);
	pxau2o->ehci = ehci;
	platform_set_drvdata(pdev, pxau2o);
	g_pxau2o = pxau2o;

	/* EHCI registers start at offset 0x100 */
	ehci->caps = hcd->regs + U2x_CAPREGS_OFFSET;
	ehci->regs = hcd->regs + U2x_CAPREGS_OFFSET +
	    HC_LENGTH(ehci_readl(ehci, &ehci->caps->hc_capbase));


#ifdef CONFIG_DVFM
	/* DVFM lock init */
	pxau2o->dvfm_lock.dev_idx = -1;
	pxau2o->dvfm_lock.count = 0;
	retval = dvfm_register("ehci-pxau2o", &pxau2o->dvfm_lock.dev_idx);
	if (retval) {
		dev_err(&pdev->dev, "Failed to register dvfm lock\n");
		goto err_dvfm_register;
	}
#endif
	INIT_WORK(&pxau2o->rm_hcd_work, mv_remove_hcd_work);
	INIT_WORK(&pxau2o->add_hcd_work, mv_add_hcd_work);
	pxau2o->switch_wq =
			create_singlethread_workqueue("u2o-switch-queue");
	if (!pxau2o->switch_wq) {
		dev_err(&pdev->dev,
			"Failed to create workqueue: u2o-switch-queue\n");
		retval = -ENOMEM;
		goto err_create_switch_workqueue;
	}

	dev_info(&pdev->dev, "u2oehci regbase 0x%x irq %d\n",
			pxau2o->regbase, hcd->irq);

	ehci->transceiver = otg_get_transceiver();
	if (ehci->transceiver) {
		otg_set_host(ehci->transceiver, &hcd->self);
	}

#ifdef CONFIG_PROC_FS
	pxau2o->u2o_dump = create_proc_entry("u2o", 0666, NULL);
	if (pxau2o->u2o_dump) {
		pxau2o->u2o_dump->read_proc = u2o_proc_read;
		pxau2o->u2o_dump->write_proc = u2o_proc_write;
		pxau2o->u2o_dump->data = pxau2o;
	} else
		dev_err(&pdev->dev, "Failed to create proc entry\n");
#endif
	/* Disable u2o clock */
	clk_disable(pxau2o->clk);

	return 0;

err_create_switch_workqueue:
#ifdef CONFIG_DVFM
	dvfm_unregister("ehci-pxau2o", &pxau2o->dvfm_lock.dev_idx);
#endif
err_dvfm_register:
	clk_disable(pxau2o->clk);
err_clk_get:
err_get_irq:
	iounmap((void *)pxau2o->regbase);
err_ioremap_u2o:
err_get_res_u2o:
	usb_put_hcd(hcd);
err_usb_create_hcd:
	kfree(pxau2o);

	dev_err(&pdev->dev, "Init %s failed, error no:%d\n", "pxa u2o", retval);
	return retval;
}

static int pxau2o_ehci_remove(struct platform_device *pdev)
{
	struct pxau2o_ehci_hcd *pxau2o = platform_get_drvdata(pdev);
	struct usb_hcd *hcd = ehci_to_hcd(pxau2o->ehci);

#ifdef CONFIG_PROC_FS
	if (pxau2o->u2o_dump)
		remove_proc_entry("u2o", NULL);
#endif
	if (HC_IS_RUNNING(hcd->state))
		hcd->state = HC_STATE_QUIESCING;
	usb_disconnect(&hcd->self.root_hub);
	hcd->driver->stop(hcd);

	flush_workqueue(pxau2o->switch_wq);
	destroy_workqueue(pxau2o->switch_wq);
#ifdef CONFIG_DVFM
	dvfm_unregister("ehci-pxau2o", &pxau2o->dvfm_lock.dev_idx);
#endif
	clk_disable(pxau2o->clk);
	iounmap((void *)pxau2o->regbase);
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
	usb_put_hcd(hcd);
	kfree(pxau2o);

	return 0;
}

MODULE_ALIAS("pxau2o-ehci");

static void pxau2o_ehci_hcd_shutdown(struct platform_device *pdev)
{
	struct pxau2o_ehci_hcd *pxau2o = platform_get_drvdata(pdev);
	struct usb_hcd *hcd = ehci_to_hcd(pxau2o->ehci);

	if (hcd->driver->shutdown)
		hcd->driver->shutdown(hcd);
}

static struct platform_driver pxau2o_ehci_driver = {
	.probe = pxau2o_ehci_probe,
	.remove = pxau2o_ehci_remove,
	//.shutdown = usb_hcd_platform_shutdown,
	.driver = {
		.name = "pxau2o-ehci",
		.bus = &platform_bus_type,
	},
};
