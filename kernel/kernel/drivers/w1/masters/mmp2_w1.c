/*
 * 1-wire busmaster driver for marvell MMP2 
 *
 * Copyright (c) 2010-2011, Ellie Cao 
 *
 * Use consistent with the GNU GPL is permitted,
 * provided that this copyright notice is
 * preserved in its entirety in all copies and derived works.
 */
#define DEBUG
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/pm.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/mmp2_w1.h>

#include <linux/clk.h>
#include <plat/mfp.h>
#include <asm/gpio.h>

#include <asm/io.h>

#include "../w1.h"
#include "../w1_int.h"


#define MMP2_W1_CMD	0x00	/* R/W 4 bits command */
#define MMP2_W1_DATA	0x01	/* R/W 8 bits, transmit/receive buffer */
#define MMP2_W1_INT	0x02	/* R/W interrupt status */
#define MMP2_W1_INT_EN	0x03	/* R/W interrupt enable */
#define MMP2_W1_CLKDIV	0x04	/* R/W 5 bits of divisor and pre-scale */

#define MMP2_W1_CMD_1W_RESET  (1 << 0)	/* force reset on 1-wire bus */
#define MMP2_W1_CMD_SRA	    (1 << 1)	/* enable Search ROM accelerator mode */
#define MMP2_W1_CMD_DQ_OUTPUT (1 << 2)	/* write only - forces bus low */
#define MMP2_W1_CMD_DQ_INPUT  (1 << 3)	/* read only - reflects state of bus */

#define MMP2_W1_INT_PD	    (1 << 0)	/* presence detect */
#define MMP2_W1_INT_PDR	    (1 << 1)	/* presence detect result */
#define MMP2_W1_INT_TBE	    (1 << 2)	/* tx buffer empty */
#define MMP2_W1_INT_TSRE	    (1 << 3)	/* tx shift register empty */
#define MMP2_W1_INT_RBF	    (1 << 4)	/* rx buffer full */

#define MMP2_W1_INTEN_EPD	    (1 << 0)	/* enable presence detect int */
#define MMP2_W1_INTEN_ETBE    (1 << 2)	/* enable tx buffer empty int */
#define MMP2_W1_INTEN_ETMT    (1 << 3)	/* enable tx shift register empty int */
#define MMP2_W1_INTEN_ERBF    (1 << 4)	/* enable rx buffer full int */
#define MMP2_W1_INTEN_DQO	    (1 << 7)	/* enable direct bus driving ops */


#define MMP2_W1_TIMEOUT (HZ * 5)

static struct {
	unsigned long freq;
	unsigned long divisor;
} freq[] = {
	{ 4000000, 0x8 },
	{ 5000000, 0x2 },
	{ 6000000, 0x5 },
	{ 7000000, 0x3 },
	{ 8000000, 0xc },
	{ 10000000, 0x6 },
	{ 12000000, 0x9 },
	{ 14000000, 0x7 },
	{ 16000000, 0x10 },
	{ 20000000, 0xa },
	{ 24000000, 0xd },
	{ 28000000, 0xb },
	{ 32000000, 0x14 },
	{ 40000000, 0xe },
	{ 48000000, 0x11 },
	{ 56000000, 0xf },
	{ 64000000, 0x18 },
	{ 80000000, 0x12 },
	{ 96000000, 0x15 },
	{ 112000000, 0x13 },
	{ 128000000, 0x1c },
};

struct mmp2_w1_data {
	void		__iomem *map;
	int		bus_shift; /* # of shifts to calc register offsets */
	struct platform_device *pdev;
	struct mmp2_w1_platform_data *pdata;
	struct clk	*clk;
	int		slave_present;
	u8		read_byte; /* last byte received */
};

static inline void mmp2_w1_write_register(struct mmp2_w1_data *mmp2_w1_data, u32 reg,
					u8 val)
{
	__raw_writel(val, mmp2_w1_data->map + (reg << mmp2_w1_data->bus_shift));
}

static inline u8 mmp2_w1_read_register(struct mmp2_w1_data *mmp2_w1_data, u32 reg)
{
	return  __raw_readb(mmp2_w1_data->map + (reg << mmp2_w1_data->bus_shift));
}

static int mmp2_w1_reset(struct mmp2_w1_data *mmp2_w1_data)
{
	int cnt=0;
	
	mmp2_w1_write_register(mmp2_w1_data, MMP2_W1_CMD, MMP2_W1_CMD_1W_RESET);

    while(mmp2_w1_read_register(mmp2_w1_data, MMP2_W1_CMD)&MMP2_W1_CMD_1W_RESET)
    {
	    cnt++;
	    udelay(20);
    }

	mmp2_w1_data->slave_present=!(mmp2_w1_read_register(mmp2_w1_data, MMP2_W1_INT)&MMP2_W1_INT_PDR);

	/* Wait for the end of the reset. According to the specs, the time
	 * from when the interrupt is asserted to the end of the reset is:
	 *     tRSTH  - tPDH  - tPDL - tPDI
	 *     625 us - 60 us - 240 us - 100 ns = 324.9 us
	 *
	 * We'll wait a bit longer just to be sure.
	 * Was udelay(500), but if it is going to busywait the cpu that long,
	 * might as well come back later.
	 */
	msleep(1);
	
	if (!mmp2_w1_data->slave_present) {
		dev_err(&mmp2_w1_data->pdev->dev, "reset: no devices found\n");
		return 1;
	}

	return 0;
}

static inline u8 mmp2_w1_touch_bit(struct mmp2_w1_data *data, u8 bit)
{
	u8 result;
	unsigned gpio=data->pdata->gpio;

	if (bit)
	{
		gpio_direction_output(gpio,0);
		udelay(6);
		gpio_direction_input(gpio);
		udelay(9);
		
		result = gpio_get_value(gpio)?1:0;
		udelay(55);
	}
	else 
	{
		gpio_direction_output(gpio,0);
		udelay(60);
		gpio_direction_input(gpio);
		udelay(10);
		result=0;
	}
	return result;
	
}

static int mmp2_w1_write(struct mmp2_w1_data *mmp2_w1_data, u8 data)
{
	int cnt=0;
	mmp2_w1_write_register(mmp2_w1_data, MMP2_W1_DATA, data);
    while((mmp2_w1_read_register(mmp2_w1_data, MMP2_W1_INT)&(MMP2_W1_INT_TSRE|MMP2_W1_INT_TBE))
		!=(MMP2_W1_INT_TSRE|MMP2_W1_INT_TBE))
    {
	    cnt++;
	    udelay(20);
    }

	return 0;
}

#if 0
static int mmp2_w1_write1(struct mmp2_w1_data *mmp2_w1_data, u8 data)
{
	u8 i,bit;

	// gain direct control over the wire
	mfp_clr(MFP_PIN_EXT_WAKEUP,0x7);
	mfp_set(MFP_PIN_EXT_WAKEUP,0xa006);
	
	if (gpio_request(MFP_PIN_GPIO86, "one wire")) {
		mfp_clr(MFP_PIN_EXT_WAKEUP,0x7);
		mfp_set(MFP_PIN_EXT_WAKEUP,0x1);
		dev_err(&mmp2_w1_data->pdev->dev,"failed to request gpio for w1\n");
		return (0x03);
	}

	for(i=0;i<8;i++)
	{
		bit=data&1;
		data>>=1;
	//	printk("writing bit 0x%d\n",bit);
		mmp2_w1_touch_bit(mmp2_w1_data, bit);
	}

	gpio_free(MFP_PIN_GPIO86);

	// release the wire and restore to one-wire signal line
	mfp_clr(MFP_PIN_EXT_WAKEUP,0xa007);
	mfp_set(MFP_PIN_EXT_WAKEUP,0x1);
	return 0;

}
#endif

static int mmp2_w1_read(struct mmp2_w1_data *mmp2_w1_data, unsigned char write_data)
{
	int cnt=0;
	mmp2_w1_write(mmp2_w1_data, write_data);
	
    while(!(mmp2_w1_read_register(mmp2_w1_data, MMP2_W1_INT)&MMP2_W1_INT_RBF))
    {
	    cnt++;
	    udelay(20);
    }

	mmp2_w1_data->read_byte = mmp2_w1_read_register(mmp2_w1_data,
							    MMP2_W1_DATA);

	return mmp2_w1_data->read_byte;
}

static int mmp2_w1_find_divisor(int gclk)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(freq); i++)
		if (gclk <= freq[i].freq)
			return freq[i].divisor;

	return 0;
}

static void mmp2_w1_up(struct mmp2_w1_data *mmp2_w1_data)
{
	int gclk, divisor;

	if (mmp2_w1_data->pdata->enable)
		mmp2_w1_data->pdata->enable(mmp2_w1_data->pdev);

	gclk = clk_get_rate(mmp2_w1_data->clk);
	clk_enable(mmp2_w1_data->clk);
	divisor = mmp2_w1_find_divisor(gclk);
	if (divisor == 0) {
		dev_err(&mmp2_w1_data->pdev->dev,
			"no suitable divisor for %dHz clock\n", gclk);
		return;
	}
	mmp2_w1_write_register(mmp2_w1_data, MMP2_W1_CLKDIV, divisor);

	/* Let the w1 clock stabilize. */
	mdelay(1);

	//mmp2_w1_reset(mmp2_w1_data);
}

static void mmp2_w1_down(struct mmp2_w1_data *mmp2_w1_data)
{
	//mmp2_w1_reset(mmp2_w1_data);

	if (mmp2_w1_data->pdata->disable)
		mmp2_w1_data->pdata->disable(mmp2_w1_data->pdev);

	clk_disable(mmp2_w1_data->clk);
}

/* --------------------------------------------------------------------- */
/* w1 methods */

static u8 mmp2_w1_read_byte(void *data)
{
	struct mmp2_w1_data *mmp2_w1_data = data;

	return mmp2_w1_read(mmp2_w1_data, 0xff);
}

static void mmp2_w1_write_byte(void *data, u8 byte)
{
	struct mmp2_w1_data *mmp2_w1_data = data;

	mmp2_w1_write(mmp2_w1_data, byte);
}

static u8 mmp2_w1_reset_bus(void *data)
{
	struct mmp2_w1_data *mmp2_w1_data = data;

	mmp2_w1_reset(mmp2_w1_data);

	return 0;
}


static u8 mmp2_w1_triplet(void *data, u8 bdir)
{
	struct mmp2_w1_data *mmp2_w1_data = data;
	u8 id_bit;
	u8 comp_bit;
	u8 retval;
	unsigned long tmp,regsave;
	unsigned gpio=mmp2_w1_data->pdata->gpio;

	// gain direct control over the wire
	regsave = tmp = mfp_read(gpio);
	tmp &= ~0x7;
	tmp|=0xa000;
	mfp_write(gpio, tmp);

	if (gpio_request(gpio, "one wire")) {
		mfp_write(gpio, regsave);
		dev_err(&mmp2_w1_data->pdev->dev,"failed to request gpio for w1\n");
		return (0x03);
	}

	id_bit	= mmp2_w1_touch_bit(mmp2_w1_data, 1);
	comp_bit = mmp2_w1_touch_bit(mmp2_w1_data, 1);

	if ( id_bit && comp_bit )
	{
		gpio_free(gpio);
		
		mfp_write(gpio, regsave);
		dev_dbg(&mmp2_w1_data->pdev->dev,"triplet no device\n");
		return(0x03);  /* error */
	}
	
	if ( !id_bit && !comp_bit ) {
		/* Both bits are valid, take the direction given */
		retval = bdir ? 0x04 : 0;
	} else {
		/* Only one bit is valid, take that direction */
		bdir = id_bit;
		retval = id_bit ? 0x05 : 0x02;
	}
	
	mmp2_w1_touch_bit(mmp2_w1_data, bdir);
	gpio_free(gpio);

	// release the wire and restore to one-wire signal line
	mfp_write(gpio, regsave);
	return(retval);

}

static void mmp2_w1_search(void *data, struct w1_master *master_dev,
			u8 search_type, w1_slave_found_callback slave_found)
{
	struct mmp2_w1_data *mmp2_w1_data = data;
	int i;
	unsigned long long rom_id;

	/* we are indeed "read rom" */
	if (mmp2_w1_reset(mmp2_w1_data))
		return;

	mmp2_w1_write(mmp2_w1_data, W1_READ_ROM);

	for (rom_id = 0, i = 0; i < 8; i++) {

		unsigned char resp;

		resp = mmp2_w1_read(mmp2_w1_data, 0xff);

		rom_id |= (unsigned long long) resp << (i * 8);

	}
	//dev_dbg(&mmp2_w1_data->pdev->dev, "found 0x%08llX\n", rom_id);

	mmp2_w1_reset(mmp2_w1_data);

	slave_found(master_dev, rom_id);
}


/* --------------------------------------------------------------------- */

static struct w1_bus_master mmp2_w1_master = {
	.read_byte  = mmp2_w1_read_byte,
	.write_byte = mmp2_w1_write_byte,
	.reset_bus  = mmp2_w1_reset_bus,
	//.triplet	= mmp2_w1_triplet,
	.search 	= mmp2_w1_search,
};

static int mmp2_w1_probe(struct platform_device *pdev)
{
	struct mmp2_w1_data *mmp2_w1_data;
	struct mmp2_w1_platform_data *plat;
	struct resource *res;
	int ret;

	if (!pdev)
		return -ENODEV;

	mmp2_w1_data = kzalloc(sizeof(*mmp2_w1_data), GFP_KERNEL);
	if (!mmp2_w1_data)
		return -ENOMEM;

	platform_set_drvdata(pdev, mmp2_w1_data);

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		ret = -ENXIO;
		goto err0;
	}
	mmp2_w1_data->map = ioremap_nocache(res->start, res->end - res->start + 1);
	if (!mmp2_w1_data->map) {
		ret = -ENOMEM;
		goto err0;
	}
	plat = pdev->dev.platform_data;
	mmp2_w1_data->bus_shift = plat->bus_shift;
	mmp2_w1_data->pdev = pdev;
	mmp2_w1_data->pdata = plat;

	mmp2_w1_data->clk = clk_get(&pdev->dev, "W1CLK");
	if (IS_ERR(mmp2_w1_data->clk)) {
		ret = PTR_ERR(mmp2_w1_data->clk);
		goto err1;
	}

	mmp2_w1_up(mmp2_w1_data);

	if(plat->gpio>=0){
		mmp2_w1_master.triplet = mmp2_w1_triplet;
	}
		

	mmp2_w1_master.data = (void *)mmp2_w1_data;

	ret = w1_add_master_device(&mmp2_w1_master);
	if (ret)
		goto err2;

	return 0;

err2:
	mmp2_w1_down(mmp2_w1_data);
	clk_put(mmp2_w1_data->clk);
err1:
	iounmap(mmp2_w1_data->map);
err0:
	kfree(mmp2_w1_data);

	return ret;
}

#ifdef CONFIG_PM
static int mmp2_w1_suspend(struct platform_device *pdev, pm_message_t state)
{
	struct mmp2_w1_data *mmp2_w1_data = platform_get_drvdata(pdev);

	mmp2_w1_down(mmp2_w1_data);

	return 0;
}

static int mmp2_w1_resume(struct platform_device *pdev)
{
	struct mmp2_w1_data *mmp2_w1_data = platform_get_drvdata(pdev);

	mmp2_w1_up(mmp2_w1_data);

	return 0;
}
#else
#define mmp2_w1_suspend NULL
#define mmp2_w1_resume NULL
#endif

static int mmp2_w1_remove(struct platform_device *pdev)
{
	struct mmp2_w1_data *mmp2_w1_data = platform_get_drvdata(pdev);

	w1_remove_master_device(&mmp2_w1_master);
	mmp2_w1_down(mmp2_w1_data);
	clk_put(mmp2_w1_data->clk);
	iounmap(mmp2_w1_data->map);
	kfree(mmp2_w1_data);

	return 0;
}

static struct platform_driver mmp2_w1_driver = {
	.driver   = {
		.name = "mmp2-w1",
	},
	.probe    = mmp2_w1_probe,
	.remove   = mmp2_w1_remove,
	.suspend  = mmp2_w1_suspend,
	.resume   = mmp2_w1_resume
};

static int __init mmp2_w1_init(void)
{
	return platform_driver_register(&mmp2_w1_driver);
}

static void __exit mmp2_w1_exit(void)
{
	platform_driver_unregister(&mmp2_w1_driver);
}

module_init(mmp2_w1_init);
module_exit(mmp2_w1_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ellie Cao");
MODULE_DESCRIPTION("mmp2 w1 busmaster driver");
