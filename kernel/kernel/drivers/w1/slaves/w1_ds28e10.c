/*
 * w1_ds2431.c - w1 family DS28E10 driver
 *
 * Copyright (c) 2010 Ellie Cao
 *
 * This source code is licensed under the GNU General Public License,
 * Version 2. See the file COPYING for more details.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/workqueue.h>
#include <linux/random.h>
#include <linux/slab.h>
#include <linux/crypto.h>
#include <linux/scatterlist.h>
#include <linux/fb.h>
#include <linux/linux_logo.h>
#include "../w1.h"
#include "../w1_int.h"
#include "../w1_family.h"


#define UniqueSecretOption 1

#define AUTHENTICATE_IN_APPLICATION 0

static int force_enable=0;

//define basic 64-bit secret for DS28E10 (public key)
//static unsigned char	DeviceSecret[8] = {0x12,0x29,0xfd,0x23,0x43,0x22,0x44,0x52};
//static unsigned char	DeviceSecret[8] = {0x77,0x95,0x69,0x8C,0xA8,0x85,0xA5,0xC1};
static unsigned char	DeviceSecret[8] = {0x7C,0xE2,0x93,0x73,0x87,0x66,0x94,0x2C};
#if !AUTHENTICATE_IN_APPLICATION
static unsigned char	DeviceSecretReal[16];
#endif

//hold data read from DS28E10
static unsigned char pbuf[40];


/**************************
       SHA-1 variables
***************************/
static unsigned char SHAVM_Message[64];

// Hash buffer, in wrong order for Dallas MAC
static unsigned long  SHAVM_Hash[5];
// MAC buffer, in right order for Dallas MAC
static unsigned char SHAVM_MAC[20];

static unsigned long SHAVM_MTword[80];
static long SHAVM_Temp;
static int SHAVM_cnt;

static long SHAVM_KTN[4];


/****************************************
       DS28E10 memory function commands
****************************************/
#define DS28E10_FUNC_WRITE_CHALLENGE    0x0F
#define DS28E10_FUNC_READ_AUTH_PAGE     0xA5

#if AUTHENTICATE_IN_APPLICATION

#define W1_DS28E10_ATTR_RW(_name, _mode)				\
	struct device_attribute w1_ds28e10_attribute_##_name =	\
		__ATTR(w1_ds28e10_##_name, _mode,		\
		       w1_ds28e10_attribute_show_##_name,	\
		       w1_ds28e10_attribute_store_##_name)

#define W1_DS28E10_ATTR_RO(_name, _mode)				\
	struct device_attribute w1_ds28e10_attribute_##_name =	\
		__ATTR(w1_ds28e10_##_name, _mode,		\
		       w1_ds28e10_attribute_show_##_name, NULL)

#define W1_DS28E10_ATTR_WO(_name, _mode)				\
	struct device_attribute w1_ds28e10_attribute_##_name =	\
		__ATTR(w1_ds28e10_##_name, _mode,		\
		       NULL, w1_ds28e10_attribute_store_##_name)
		       
#endif

#define W1_DS28E10_CHALLENGE_LENGTH 12

static int write_challenge(struct w1_slave *sl,const char *challenge)
{
	int i;
	if (w1_reset_select_slave(sl))
		return -1;
	
	w1_write_8(sl->master,DS28E10_FUNC_WRITE_CHALLENGE); 
	w1_write_block(sl->master,challenge,W1_DS28E10_CHALLENGE_LENGTH);

	for(i=0;i<W1_DS28E10_CHALLENGE_LENGTH;i++) 
	{
		if( w1_read_8(sl->master)!=challenge[i]) 
			break;
	}
	if(i!=W1_DS28E10_CHALLENGE_LENGTH) 
	{ 
		dev_err(&sl->dev, "write challenge failed\n"); 
		return -1; 
	} 

	return 0;
}

/*--------------------------------------------------------------------------
 * Calculate a new CRC16 from the input data shorteger.  Return the current
 * CRC16 and also update the global variable CRC16.
 */
static short oddparity[16] = { 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0 };

static unsigned int CRC16;

static unsigned short docrc16(unsigned short data)
{
   data = (data ^ (CRC16 & 0xff)) & 0xff;
   CRC16 >>= 8;

   if (oddparity[data & 0xf] ^ oddparity[data >> 4])
     CRC16 ^= 0xc001;

   data <<= 6;
   CRC16   ^= data;
   data <<= 1;
   CRC16   ^= data;

   return CRC16;
}


//----------------------------------------------------------------------
// computes a SHA given the 64 byte MT digest buffer.  The resulting 5
// long values are stored in the long array, hash.
//
// 'SHAVM_Message' - buffer containing the message digest
// 'SHAVM_Hash'    - result buffer
// 'SHAVM_MAC'     - result buffer, in order for Dallas part
//
static void SHAVM_Compute(void)
{
   SHAVM_KTN[0]=(long)0x5a827999;
   SHAVM_KTN[1]=(long)0x6ed9eba1;
   SHAVM_KTN[2]=(long)0x8f1bbcdc;
   SHAVM_KTN[3]=(long)0xca62c1d6;   
   for(SHAVM_cnt=0; SHAVM_cnt<16; SHAVM_cnt++)
   {
      SHAVM_MTword[SHAVM_cnt]
         = (((long)SHAVM_Message[SHAVM_cnt*4]&0x00FF) << 24L)
         | (((long)SHAVM_Message[SHAVM_cnt*4+1]&0x00FF) << 16L)
         | (((long)SHAVM_Message[SHAVM_cnt*4+2]&0x00FF) << 8L)
         |  ((long)SHAVM_Message[SHAVM_cnt*4+3]&0x00FF);
   }

   for(; SHAVM_cnt<80; SHAVM_cnt++)
   {
      SHAVM_Temp
         = SHAVM_MTword[SHAVM_cnt-3]  ^ SHAVM_MTword[SHAVM_cnt-8]
         ^ SHAVM_MTword[SHAVM_cnt-14] ^ SHAVM_MTword[SHAVM_cnt-16];
      SHAVM_MTword[SHAVM_cnt]
         = ((SHAVM_Temp << 1) & 0xFFFFFFFE)
         | ((SHAVM_Temp >> 31) & 0x00000001);
   }

   SHAVM_Hash[0] = 0x67452301;
   SHAVM_Hash[1] = 0xEFCDAB89;
   SHAVM_Hash[2] = 0x98BADCFE;
   SHAVM_Hash[3] = 0x10325476;
   SHAVM_Hash[4] = 0xC3D2E1F0;

   for(SHAVM_cnt=0; SHAVM_cnt<80; SHAVM_cnt++)
   {
      SHAVM_Temp
         = ((SHAVM_Hash[0] << 5) & 0xFFFFFFE0)
         | ((SHAVM_Hash[0] >> 27) & 0x0000001F);
      if(SHAVM_cnt<20)
         SHAVM_Temp += ((SHAVM_Hash[1]&SHAVM_Hash[2])|((~SHAVM_Hash[1])&SHAVM_Hash[3]));
      else if(SHAVM_cnt<40)
         SHAVM_Temp += (SHAVM_Hash[1]^SHAVM_Hash[2]^SHAVM_Hash[3]);
      else if(SHAVM_cnt<60)
         SHAVM_Temp += ((SHAVM_Hash[1]&SHAVM_Hash[2])
                       |(SHAVM_Hash[1]&SHAVM_Hash[3])
                       |(SHAVM_Hash[2]&SHAVM_Hash[3]));
      else
         SHAVM_Temp += (SHAVM_Hash[1]^SHAVM_Hash[2]^SHAVM_Hash[3]);
      SHAVM_Temp += SHAVM_Hash[4] + SHAVM_KTN[SHAVM_cnt/20]
                  + SHAVM_MTword[SHAVM_cnt];
      SHAVM_Hash[4] = SHAVM_Hash[3];
      SHAVM_Hash[3] = SHAVM_Hash[2];
      SHAVM_Hash[2]
         = ((SHAVM_Hash[1] << 30) & 0xC0000000)
         | ((SHAVM_Hash[1] >> 2) & 0x3FFFFFFF);
      SHAVM_Hash[1] = SHAVM_Hash[0];
      SHAVM_Hash[0] = SHAVM_Temp;
   }

   //iButtons use LSB first, so we have to turn
   //the result around a little bit.  Instead of
   //result A-B-C-D-E, our result is E-D-C-B-A,
   //where each letter represents four bytes of
   //the result.
   for(SHAVM_cnt=0; SHAVM_cnt<5; SHAVM_cnt++)
   {
      SHAVM_Temp = SHAVM_Hash[4-SHAVM_cnt];
      SHAVM_MAC[((SHAVM_cnt)*4)+0] = (unsigned char)SHAVM_Temp;
      SHAVM_Temp >>= 8;
      SHAVM_MAC[((SHAVM_cnt)*4)+1] = (unsigned char)SHAVM_Temp;
      SHAVM_Temp >>= 8;
      SHAVM_MAC[((SHAVM_cnt)*4)+2] = (unsigned char)SHAVM_Temp;
      SHAVM_Temp >>= 8;
      SHAVM_MAC[((SHAVM_cnt)*4)+3] = (unsigned char)SHAVM_Temp;
   }
   
}

//Compute SHA-1 result
//input parameter: 1. 64-bit unique ROM ID
//                         2. 28-byte user data
//                         3. 12-byte challenge
//                         4. 64-bit secret
static void ComputeSHA1(unsigned char *secret, unsigned char *RomID,
                             unsigned char *data, unsigned char *challenge)
{
	// check mandatory input parameters
	if( (secret == NULL) || (RomID == NULL) )
		return;

	// set up message block
	memcpy(SHAVM_Message, secret, 4);
	if(data)
	{
		memcpy(&SHAVM_Message[4], data, 28);
	}
	else
	{
		memset(&SHAVM_Message[4], 0x00, 28);
	}
	if(challenge)
	{
		memcpy(&SHAVM_Message[32], &challenge[8], 4);
		memcpy(&SHAVM_Message[36], challenge, 4);
		SHAVM_Message[40] =challenge[7];
		memcpy(&SHAVM_Message[41], RomID, 7);
		memcpy(&SHAVM_Message[52], &challenge[4], 3);
	}
	else
	{
		memset(&SHAVM_Message[32], 0x00, 4);
		memset(&SHAVM_Message[36], 0xff, 4);
		SHAVM_Message[40]=RomID[0]&0x3f;
		memcpy(&SHAVM_Message[41], &RomID[1], 7);
		memset(&SHAVM_Message[52],0xff, 3);
	}
	memcpy(&SHAVM_Message[48], &secret[4], 4);
	SHAVM_Message[55] = 0x80;
	memset(&SHAVM_Message[56], 0x00, 6);
	SHAVM_Message[62] = 0x01;
	SHAVM_Message[63] = 0xB8;
	
	// perform SHA-1 algorithm, result will be in SHAVM_MAC
	SHAVM_Compute();
	
}

static inline unsigned safe_shift(unsigned d, int n)
{
	return n < 0 ? d >> -n : d << n;
}

static void  fb_set_logo_truepalette(struct fb_info *info,
					    const struct linux_logo *logo,
					    u32 *palette)
{
	static const unsigned char mask[] = { 0,0x80,0xc0,0xe0,0xf0,0xf8,0xfc,0xfe,0xff };
	unsigned char redmask, greenmask, bluemask;
	int redshift, greenshift, blueshift;
	int i;
	const unsigned char *clut = logo->clut;
	/*
	 * We have to create a temporary palette since console palette is only
	 * 16 colors long.
	 */
	/* Bug: Doesn't obey msb_right ... (who needs that?) */
	redmask   = mask[info->var.red.length   < 8 ? info->var.red.length   : 8];
	greenmask = mask[info->var.green.length < 8 ? info->var.green.length : 8];
	bluemask  = mask[info->var.blue.length  < 8 ? info->var.blue.length  : 8];
	redshift   = info->var.red.offset   - (8 - info->var.red.length);
	greenshift = info->var.green.offset - (8 - info->var.green.length);
	blueshift  = info->var.blue.offset  - (8 - info->var.blue.length);
		
	for ( i = 0; i < logo->clutsize; i++) {
		// Ellie changed to support 256 colors
		palette[i/*+32*/] = (safe_shift((clut[0] & redmask), redshift) |
				 safe_shift((clut[1] & greenmask), greenshift) |
				 safe_shift((clut[2] & bluemask), blueshift));
		clut += 3;
	}
}

extern const struct linux_logo logo_skull_clut224;

static void do_panic(const char *message)
{
	struct fb_info *info=registered_fb[0];
	struct fb_image image;
	struct fb_fillrect rect;
	u32 *palette=NULL;
	const struct linux_logo *logo=&logo_skull_clut224;

	if(info == NULL)
		goto kernel_panic;

	palette = kmalloc(256 * 4, GFP_KERNEL);
	if (palette == NULL)
		goto kernel_panic;

	/* fill screen with black */
	memset(&rect, 0, sizeof(rect));
	rect.width = info->var.xres;
	rect.height = info->var.yres;
	info->fbops->fb_fillrect(info, &rect);

	/* blit the skull in the center */
	fb_set_logo_truepalette(info, logo, palette);
	info->pseudo_palette = palette;
	image.depth = 8;
	image.data = logo->data;
	image.width = logo->width;
	image.height = logo->height;
	image.dx = (info->var.xres-logo->width)/2;
	image.dy = (info->var.yres-logo->height)/2;
	info->fbops->fb_imageblit(info, &image);

	info->state=FBINFO_STATE_SUSPENDED;
kernel_panic:
	panic(message);
}

static int read_authenticated_page(struct w1_slave *sl, unsigned char *challenge, 
				struct hash_desc *desc, struct scatterlist *sg)
{
	int cnt,i;
	u64 RomID;
	u8 *secret;

	memcpy(&RomID,&sl->reg_num,8);
	RomID=cpu_to_le64(RomID);

	// TODO: use unique id as data

#if AUTHENTICATE_IN_APPLICATION
	secret=DeviceSecret;
	//data=NULL;
#else
	// calculate DeviceSecretReal
	crypto_hash_init(desc);
	crypto_hash_update(desc, sg, sizeof(DeviceSecret));
	crypto_hash_final(desc, DeviceSecretReal);
	secret=DeviceSecretReal;
	//data= &DeviceSecretReal[8];
#endif

	// compute secret to SHAVM_MAC
	if( UniqueSecretOption )
	{
		// compute secret from public key
		ComputeSHA1(secret,(unsigned char *)&RomID,NULL,NULL);
	}
	else
	{
		// use public key as secret
		memcpy(SHAVM_MAC, secret, 8); 
	}
			
#if !AUTHENTICATE_IN_APPLICATION
	memset(DeviceSecretReal,0,sizeof(DeviceSecretReal));
#endif

	if (w1_reset_select_slave(sl))
	{
		memset(SHAVM_MAC,0,sizeof(SHAVM_MAC));
		return -1;
	}

	// write "read authenticated page" command with target address 0
	cnt=0;
	pbuf[cnt++]=DS28E10_FUNC_READ_AUTH_PAGE;
	pbuf[cnt++]=0;
	pbuf[cnt++]=0;
	for(i=0;i<cnt;i++) 
	{
		w1_write_8(sl->master,pbuf[i]);
	}

	// read 28 bytes data + "FF" + 2 bytes CRC
	for(i = 0;i < 31;i++)
	{
		pbuf[cnt++] = w1_read_8(sl->master);
	}

	// run the CRC over this part
	CRC16 = 0;
	for (i = 0; i < cnt; i++)
	{
		docrc16(pbuf[i]);
	}
	if( CRC16 != 0xB001) //not 0 because that the calculating result is CRC16 and the reading result is inverted CRC16
	{
		memset(SHAVM_MAC,0,sizeof(SHAVM_MAC));
		dev_err(&sl->dev, "read page data failed\n"); 
		return -1; 
	} 
	
	//calculate the corresponding MAC by the host, device secret reserved in SHAVM_MAC[]
	ComputeSHA1(SHAVM_MAC,(unsigned char *)&RomID,&pbuf[3],challenge);

	// wait for 2 ms
	msleep(2);

	// read 20 bytes MAC and 2 bytes CRC
	cnt=0;
	for(i = 0;i < 22;i++)
	{
		pbuf[cnt++] = w1_read_8(sl->master);
	}
	
	// run the CRC over this part MAC
	CRC16 = 0;
	for (i = 0; i < cnt; i++)
	{
		docrc16(pbuf[i]);
	}
	if( CRC16 != 0xB001) //not 0 because that the calculating result is CRC16 and the reading result is inverted CRC16
	{
		dev_err(&sl->dev, "read MAC failed\n"); 
		return -1; 
	} 

/*	printk("host MAC=");
	for(i=0;i<20;i++)
	{
		printk("%02x ",SHAVM_MAC[i]);
	}
	printk("\n");
*/
	
	//Compare calculated MAC with the MAC from the DS28E10
	for(i=0;i<20;i++)
	{
		if( SHAVM_MAC[i]!=pbuf[i] )
			break;
	}
	if( i<20 )
	{
		dev_err(&sl->dev, "MAC unmatch\n"); 
		return -1; 
	} 

	return 0;
}

#if AUTHENTICATE_IN_APPLICATION
static ssize_t w1_ds28e10_attribute_store_challenge(struct device *dev,
						struct device_attribute *attr,
						const char *buf, size_t count)
{
	struct w1_slave *sl = dev_to_w1_slave(dev);
	ssize_t result;

	if ((buf == NULL)||(count != W1_DS28E10_CHALLENGE_LENGTH))
		return -EINVAL;

	result=count;

	mutex_lock(&sl->master->mutex);
	
	if (write_challenge(sl,buf)<0)
		result=-EIO;
	else if (read_authenticated_page(sl,buf,NULL,NULL)<0)
		result=-EKEYREJECTED;
	
	mutex_unlock(&sl->master->mutex);

	return result;
}

static W1_DS28E10_ATTR_WO(challenge, S_IWUSR);

static struct attribute *w1_ds28e10_attrs[] = {
	&w1_ds28e10_attribute_challenge.attr,
	NULL
};

static struct attribute_group w1_ds28e10_attr_group = {
	.attrs = w1_ds28e10_attrs,
};

#else

static unsigned long Challenge[3];
// {0x12,0x24,0x4d,0x2a,0x73,0x12,0x24,0x4d,0x2a,0x73,0x24,0x56};

struct w1_ds28e10_authenticate_work
{
	struct w1_slave * sldev;
	struct delayed_work delayed_work;
	struct workqueue_struct *auth_wq;
	int fail_cnt;
	struct hash_desc desc;
	struct scatterlist sg;
};

struct delayed_work *w1_ds28e10_shutdown_work=NULL;

// TODO: use different seed for each device 
static void authenticate_delayed_work(struct work_struct* work)
{
	struct w1_ds28e10_authenticate_work *auth_work; 
	struct w1_slave *sl;
	int result=0;

	auth_work= (struct w1_ds28e10_authenticate_work*)container_of(work,struct w1_ds28e10_authenticate_work,delayed_work.work);	
	sl=auth_work->sldev;

	mutex_lock(&sl->master->mutex);

	// generate random challenge
	Challenge[0]=random32();
	Challenge[1]=random32();
	Challenge[2]=random32();
//printk("challenge: 0x%08x%08x%08x\n",Challenge[2],Challenge[1],Challenge[0]);
	if (write_challenge(sl,(unsigned char *)Challenge)<0)
		result=-1;
	else if (read_authenticated_page(sl,(unsigned char *)Challenge,
		           &auth_work->desc,&auth_work->sg)<0)
		result=-1;
	
	mutex_unlock(&sl->master->mutex);

	if(result<0)
	{
		dev_err(&sl->dev, "Authentication failed!!!\n"); 
		auth_work->fail_cnt++;
		if(auth_work->fail_cnt>5)
			do_panic("Authentication failed!!\n");
	}
	else
	{
		dev_dbg(&sl->dev, "Authentication ok.\n"); 
		if(w1_ds28e10_shutdown_work)
		{
			cancel_delayed_work_sync(w1_ds28e10_shutdown_work);
			kfree(w1_ds28e10_shutdown_work);
			w1_ds28e10_shutdown_work=NULL;
		}
		auth_work->fail_cnt=0;
	}
	queue_delayed_work(auth_work->auth_wq,&auth_work->delayed_work,msecs_to_jiffies(10000));
}

#endif

extern char android_serialno[];
static int w1_ds28e10_add_slave(struct w1_slave *sl)
{
#if AUTHENTICATE_IN_APPLICATION
	return sysfs_create_group(&sl->dev.kobj, &w1_ds28e10_attr_group);
#else
	struct w1_ds28e10_authenticate_work *auth_work;
	struct crypto_hash *tfm;
	struct hash_desc desc;

	/* save rom id in def_serialno, which is used to initialize gadget name */
	char* dst = strchr(android_serialno, '-');
	char* src = strchr(sl->name, '-');
	if (src && dst) strcpy(dst, src);

	// use MD5 to calculate the real device secret 
	tfm = crypto_alloc_hash("md5", 0, CRYPTO_ALG_ASYNC);

	if (!tfm) 
	{
		printk("failed to load transform\n");
		return -ENOMEM;
	}

	desc.tfm = tfm;
	desc.flags = 0;

	if (crypto_hash_digestsize(tfm) > sizeof(DeviceSecretReal)) 
	{
		printk("digestsize(%u) > outputbuffer(%zu)\n",
		       crypto_hash_digestsize(tfm), sizeof(DeviceSecretReal));
		crypto_free_hash(tfm);
		return -ENOMEM;
	}
	
	auth_work = kzalloc(sizeof(struct w1_ds28e10_authenticate_work), GFP_KERNEL);
	if (!auth_work) 
	{
		dev_err(&sl->dev,
			 "%s: failed to allocate authenticate work struct.\n",
			 __func__);
		memset(DeviceSecretReal,0,sizeof(DeviceSecretReal));
		crypto_free_hash(tfm);
		return -ENOMEM;
	}
	auth_work->sldev=sl;
	auth_work->fail_cnt=0;
	memcpy(&auth_work->desc,&desc,sizeof(desc));
	sg_init_table(&auth_work->sg, 1);
	sg_set_buf(&auth_work->sg, DeviceSecret, sizeof(DeviceSecret));
	auth_work->auth_wq = create_singlethread_workqueue("w1");
	sl->family_data=(void *)auth_work;
	INIT_DELAYED_WORK(&auth_work->delayed_work,authenticate_delayed_work);
	queue_delayed_work(auth_work->auth_wq,&auth_work->delayed_work,0);
printk("DS28E10 adding slave ok\n");

	return 0;
#endif
}

static void w1_ds28e10_remove_slave(struct w1_slave *sl)
{
#if AUTHENTICATE_IN_APPLICATION
	sysfs_remove_group(&sl->dev.kobj, &w1_ds28e10_attr_group);
#else
	struct w1_ds28e10_authenticate_work *auth_work=(struct w1_ds28e10_authenticate_work *)sl->family_data;
	cancel_delayed_work_sync(&auth_work->delayed_work);
	destroy_workqueue(auth_work->auth_wq);
	memset(DeviceSecretReal,0,sizeof(DeviceSecretReal));
	crypto_free_hash(auth_work->desc.tfm);
	
	sl->family_data=NULL;
	kfree(auth_work);
	
#endif
	do_panic("Authenticator is removed!!\n");

}

static struct w1_family_ops w1_ds28e10_fops = {
	.add_slave      = w1_ds28e10_add_slave,
	.remove_slave   = w1_ds28e10_remove_slave,
};

static struct w1_family w1_ds28e10_family = {
	.fid = W1_FAMILY_DS28E10,
	.fops = &w1_ds28e10_fops,
};

static void shutdown_delayed_work(struct work_struct* work)
{
	do_panic("authentication error!!\n");
}

extern int g50_get_board_version(void);

static int __init w1_ds28e10_init(void)
{
	int boardid = g50_get_board_version();
	if(force_enable||(boardid>1)){
		printk("authenticator enabled\n");
		w1_ds28e10_shutdown_work = kzalloc(sizeof(struct delayed_work), GFP_KERNEL);
		if (!w1_ds28e10_shutdown_work) 
		{
			printk("%s: failed to init DS28E10 driver.\n",
				 __func__);
			return -ENOMEM;
		}
		INIT_DELAYED_WORK(w1_ds28e10_shutdown_work,shutdown_delayed_work);
		schedule_delayed_work(w1_ds28e10_shutdown_work,msecs_to_jiffies(20000));
		printk("Driver for DS28E10\n");
		return w1_register_family(&w1_ds28e10_family);
	}else{
	
		printk("authenticator disabled\n");
	}

	return 0;
}

static void __exit w1_ds28e10_fini(void)
{
	int boardid = g50_get_board_version();
	if(force_enable||(boardid>1)){

		if(w1_ds28e10_shutdown_work)
		{
			cancel_delayed_work_sync(w1_ds28e10_shutdown_work);
			kfree(w1_ds28e10_shutdown_work);
			w1_ds28e10_shutdown_work=NULL;
		}
		w1_unregister_family(&w1_ds28e10_family);
	}

}

module_init(w1_ds28e10_init);
module_exit(w1_ds28e10_fini);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ellie Cao");
MODULE_DESCRIPTION("w1 family DS28E10 driver");

