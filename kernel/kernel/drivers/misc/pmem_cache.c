/* drivers/misc/pmem_cache.c
 *
 * Copyright (C) 2007 Google, Inc.
 * Copyright (C) 2010-2011 Marvell, Inc.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/fs.h>
#include <asm/cacheflush.h>

#include "pmem.h"

#ifdef CONFIG_ARM

void arch_sync_pmem(void *vaddr, unsigned long addr, unsigned long len,
		    unsigned int cmd, enum dma_data_direction dir)
{
	if (((unsigned long) vaddr | addr | len) & (ARCH_KMALLOC_MINALIGN-1)) {
		DLOG("Invalid pmem sync parameter!\n");
		return;
	}

	switch (cmd) {
	case PMEM_CACHE_FLUSH:
		dmac_flush_range(vaddr, vaddr + len);
		outer_flush_range(addr, addr + len);
		break;
	case PMEM_MAP_REGION:
		dmac_map_area(vaddr, len, dir);
		if (dir == DMA_FROM_DEVICE)
			outer_inv_range(addr, addr + len);
		else
			outer_clean_range(addr, addr + len);
		break;
	case PMEM_UNMAP_REGION:
		if (dir != DMA_TO_DEVICE)
			outer_inv_range(addr, addr + len);
		dmac_unmap_area(vaddr, len, dir);
	}
}

#else

void arch_sync_pmem(void *vaddr, unsigned long addr, unsigned long len,
		    unsigned int cmd, enum dma_data_direction dir)
{
	WARN(1, "Invalid pmem sync operation!\n");
}

#endif
