/* drivers/misc/pmem.h
 *
 * Copyright (C) 2007 Google, Inc.
 * Copyright (C) 2010-2011 Marvell, Inc.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
#ifndef _PMEM_H_
#define _PMEM_H_

#include <linux/miscdevice.h>
#include <linux/android_pmem.h>

#define PMEM_DEBUG 1

struct pmem_bits {
	unsigned char allocated:1;	/* 1 if allocated, 0 if free */
	unsigned char compound:1;	/* 1 if the next index belongs to
					     a compound allocation,
					   0 if no */
	unsigned char order:6;		/* size of the region in pmem space */
};

struct pmem_allocator {
	const char *name;
	int (*setup) (int);
	void (*remove) (int);
	int (*allocate) (int, struct pmem_region *);
	int (*free) (int, struct pmem_region *);
	unsigned long (*start_addr) (int, struct pmem_region *);
	unsigned long (*len) (int, struct pmem_region *);
	void (*dump) (int);
};
extern struct pmem_allocator allocators[];

struct pmem_bitmap {
	/* number of entries in the pmem space */
	unsigned long num_entries;
	/* the bitmap for the region indicating which entries are allocated
	 * and which are free */
	union {
		struct pmem_bits *buddy;
		unsigned long *bits;
	} map;
	/* pmem_sem protects the bitmap array
	 * a write lock should be held when modifying entries in bitmap
	 * a read lock should be held when reading data from bits or
	 * dereferencing a pointer into bitmap
	 *
	 * pmem_data->sem protects the pmem data of a particular file
	 * Many of the function that require the pmem_data->sem have a non-
	 * locking version for when the caller is already holding that sem.
	 *
	 * IF YOU TAKE BOTH LOCKS TAKE THEM IN THIS ORDER:
	 * down(pmem_data->sem) => down(bitmap->sem)
	 */
	struct rw_semaphore sem;
	struct pmem_allocator allocator;
};

#define PMEM_DEBUG_MSGS 0
#if PMEM_DEBUG_MSGS
#define DLOG(fmt,args...) \
	do { printk(KERN_INFO "[%s:%s:%d] "fmt, __FILE__, __func__, __LINE__, \
		    ##args); } \
	while (0)
#else
#define DLOG(x...) do {} while (0)
#endif

struct pmem_info {
	struct miscdevice dev;
	/* physical start address of the pmem space */
	unsigned long base;
	/* total size of the pmem space */
	unsigned long size;
	/* pfn of the garbage page in memory */
	unsigned long garbage_pfn;
	/* index of the garbage page in the pmem space */
	int garbage_index;
	/* the bitmap for the region indicating which entries are allocated
	 * and which are free */
	struct pmem_bitmap *bitmap;
	/* indicates maps of this region should be cached, if a mix of
	 * cached and uncached is desired, set this and open the device with
	 * O_SYNC to get an uncached region */
	unsigned cached;
	unsigned buffered;
	/* in no_allocator mode the first mapper gets the whole space and sets
	 * this flag */
	unsigned allocated;
	/* for debugging, creates a list of pmem file structs, the
	 * data_list_sem should be taken before pmem_data->sem if both are
	 * needed */
	struct semaphore data_list_sem;
	struct list_head data_list;

	long (*ioctl)(struct file *, unsigned int, unsigned long);
	int (*release)(struct inode *, struct file *);
};
extern struct pmem_info pmem[];

#ifdef CONFIG_ARM
#ifdef CONFIG_CPU_V6
#include <asm/shmparam.h>
#define PMEM_MIN_ALLOC pmem_min_alloc()
static inline unsigned long __attribute__((pure)) pmem_min_alloc(void)
{
	return cache_is_vipt_aliasing() ? SHMLBA : PAGE_SIZE;
}
#define PMEM_MIN_SHIFT pmem_min_shift()
static inline unsigned long __attribute__((pure)) pmem_min_shift(void)
{
	return cache_is_vipt_aliasing() ? get_order(SHMLBA) + PAGE_SHIFT
					: PAGE_SHIFT;
}
#else
#define PMEM_MIN_ALLOC PAGE_SIZE
#define PMEM_MIN_SHIFT PAGE_SHIFT
#endif
#else
#define PMEM_MIN_ALLOC PAGE_SIZE
#define PMEM_MIN_SHIFT PAGE_SHIFT
#endif

#endif
