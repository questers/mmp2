/* drivers/misc/pmem_allocator.c
 *
 * Copyright (C) 2007 Google, Inc.
 * Copyright (C) 2010-2011 Marvell, Inc.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
#include <linux/mempolicy.h>

#include "pmem.h"

#define PMEM_MAX_ORDER (1<<6)

#define PMEM_IS_FREE(id, index) !(pmem[id].bitmap->map.buddy[index].allocated)
#define PMEM_ORDER(id, index) pmem[id].bitmap->map.buddy[index].order
#define PMEM_BUDDY_INDEX(id, index) (index ^ (1 << PMEM_ORDER(id, index)))
#define PMEM_NEXT_INDEX(id, index) (index + (1 << PMEM_ORDER(id, index)))
#define PMEM_OFFSET(index) (index << PMEM_MIN_SHIFT)
#define PMEM_START_ADDR(id, index) (PMEM_OFFSET(index) + pmem[id].base)
#define PMEM_LEN_ORDER(id, index) ((1 << PMEM_ORDER(id, index)) << PMEM_MIN_SHIFT)

/* PMEM_ALLOCATOR_NONE,
 * the region should not be managed with an allocator. */

static int __pmem_setup_none(int id)
{
	pmem[id].allocated = 0;
	return 0;
}

static void __pmem_remove_none(int id)
{
}

static int __pmem_allocate_none(int id, struct pmem_region *index)
{
	if ((index->len > pmem[id].size) || pmem[id].allocated)
		return -1;
	pmem[id].allocated = 1;
	return 0;
}

static int __pmem_free_none(int id, struct pmem_region *index)
{
	pmem[id].allocated = 0;
	return 0;
}

static unsigned long __pmem_start_addr_none(int id, struct pmem_region *index)
{
	return PMEM_START_ADDR(id, 0);
}

static unsigned long __pmem_len_none(int id, struct pmem_region *index)
{
	return index ? index->len : 0;
}

static void __pmem_dump_none(int id)
{
	if (pmem[id].allocated)
		printk("The whole zone is allocated!\n");
	else
		printk("There is no allocation!\n");
}

/* PMEM_ALLOCATOR_BUDDY, the buddy allocator */

static unsigned long __pmem_order_buddy(unsigned long len)
{
	int i;

	len--;
	for (i = 0; i < sizeof(len)*8; i++)
		if (len >> i == 0)
			break;
	return i;
}

static int __pmem_setup_buddy(int id)
{
	struct pmem_bitmap *bitmap = pmem[id].bitmap;
	int i, index = 0;
	bitmap->map.buddy = kzalloc(bitmap->num_entries *
				    sizeof(struct pmem_bits),
				    GFP_KERNEL);
	if (!bitmap->map.buddy)
		return -ENOMEM;

	for (i = sizeof(bitmap->num_entries) * 8 - 1;
	     i >= 0; i--) {
		if ((bitmap->num_entries) & 1<<i) {
			PMEM_ORDER(id, index) = i;
			index = PMEM_NEXT_INDEX(id, index);
		}
	}
	return 0;
}

static void __pmem_remove_buddy(int id)
{
	kfree(pmem[id].bitmap->map.buddy);
}

static int __pmem_allocate_buddy(int id, struct pmem_region *index)
{
	struct pmem_bitmap *bitmap = pmem[id].bitmap;
	int curr = 0;
	int end = bitmap->num_entries;
	int best_fit = -1, compound_fit = -1;
	unsigned long len = (index->len + PMEM_MIN_ALLOC - 1) >> PMEM_MIN_SHIFT;
	unsigned long order = __pmem_order_buddy(len), mask, remains;

	index->len = 0;
	if (order > PMEM_MAX_ORDER)
		return -1;
	DLOG("order %lx, len %lx\n", order, len);

	/* look through the bitmap:
	 * 	if you find a free slot of the correct order use it
	 * 	otherwise,
	 *      1. find continuous free slots whose summation of order/len
	 *         meets the requirement.
	 * 	2. use the best fit (smallest with size > order) slot
	 */
	while (curr < end) {
		if (PMEM_IS_FREE(id, curr)) {
			unsigned long curr_order = PMEM_ORDER(id, curr);
			if (curr_order == (unsigned char)order) {
				/* set the not free bit and clear others */
				best_fit = curr;
				break;
			}
			if (curr_order > (unsigned char)order) {
				if (best_fit < 0 ||
			            curr_order < PMEM_ORDER(id, best_fit))
					best_fit = curr;
				compound_fit = -1;
			} else {
				if (compound_fit < 0) {
					remains = len;
					compound_fit = curr;
				}
				mask = 1 << curr_order;
				if (remains & mask)
					remains &= ~mask;
				else
					remains &= ~(mask - 1);
				if (!remains) {
					best_fit = compound_fit;
					break;
				}
			}
		} else
			compound_fit = -1;
		curr = PMEM_NEXT_INDEX(id, curr);
	}

	/* if best_fit < 0, there are no suitable slots,
	 * return an error
	 */
	if (best_fit < 0) {
		printk("pmem: no space left to allocate!\n");
		return -1;
	}

	index->offset = best_fit;
	index->len = len << PMEM_MIN_SHIFT;
	/* now partition the best fit:
	 * 1. order > length
	 * 	split the slot into 2 buddies of order - 1
	 * 2. order & length
	 * 	mark allocated
	 * 	mark compound if there remains un-allocation
	 * 	go ahead with the next index
	 */
	while (len) {
		while (!(len >> PMEM_ORDER(id, best_fit))) {
			int buddy;
			PMEM_ORDER(id, best_fit) -= 1;
			buddy = PMEM_BUDDY_INDEX(id, best_fit);
			PMEM_ORDER(id, buddy) = PMEM_ORDER(id, best_fit);
		}
		mask = 1<<PMEM_ORDER(id, best_fit);
		DLOG("allocated %d mask %lx order %d\n",
		     best_fit, mask, PMEM_ORDER(id, best_fit));
		if (len & mask)
			len &= ~mask;
		else
			len &= ~(mask - 1);
		bitmap->map.buddy[best_fit].allocated = 1;
		bitmap->map.buddy[best_fit].compound = (len != 0);
		best_fit = PMEM_NEXT_INDEX(id, best_fit);
	}
	return 0;
}

static int __pmem_free_buddy(int id, struct pmem_region *index)
{
	struct pmem_bitmap *bitmap = pmem[id].bitmap;
	int buddy = index->offset, curr;
	int end = bitmap->num_entries;

	/* clean up the bitmap, merging any buddies */
	do {
		curr = buddy;
		bitmap->map.buddy[curr].allocated = 0;
		buddy = PMEM_NEXT_INDEX(id, curr);
		DLOG("free %d %d\n", curr, bitmap->map.buddy[curr].compound);
	} while (bitmap->map.buddy[curr].compound);
	/* find a slots buddy Buddy# = Slot# ^ (1 << order)
	 * if the buddy is also free merge them
	 * repeat until the buddy is not free or end of the bitmap is reached
	 */
	do {
		buddy = PMEM_BUDDY_INDEX(id, curr);
		if (PMEM_IS_FREE(id, buddy) &&
				PMEM_ORDER(id, buddy) == PMEM_ORDER(id, curr)) {
			PMEM_ORDER(id, buddy)++;
			PMEM_ORDER(id, curr)++;
			curr = min(buddy, curr);
		} else {
			break;
		}
	} while (curr < end);

	return 0;
}

static unsigned long __pmem_start_addr(int id, struct pmem_region *index)
{
	return PMEM_START_ADDR(id, index->offset);
}

static unsigned long __pmem_len_buddy(int id, struct pmem_region *index)
{
	struct pmem_bitmap *bitmap = pmem[id].bitmap;
	unsigned long len = 0;
	int next = index->offset, curr;
	do {
		curr = next;
		next = PMEM_NEXT_INDEX(id, curr);
		len += PMEM_LEN_ORDER(id, curr);
	} while (bitmap->map.buddy[curr].compound);
	return len;
}

static void __pmem_dump_buddy(int id)
{
	struct pmem_bitmap *bitmap = pmem[id].bitmap;
	int curr = 0;
	int end = bitmap->num_entries;
	char buf[64];
	char *p, *q;

	p = buf;
	q = p + sizeof(buf);
	while (curr < end) {
		int n = snprintf(p, q - p, "[ %03d:%01d ]",
				 PMEM_ORDER(id, curr),
				 !PMEM_IS_FREE(id, curr));
		p += n;
		if (p + n > q) {
			printk("%s\n", buf);
			p = buf;
		}
		curr = PMEM_NEXT_INDEX(id, curr);
	}
	if (p != buf)
		printk("%s\n", buf);
}

/* The allocators based on simple bitmap */

static int __pmem_setup_bitsfit(int id)
{
	struct pmem_bitmap *bitmap = pmem[id].bitmap;
	bitmap->map.bits = kzalloc(
		(bitmap->num_entries + BITS_PER_BYTE - 1)
			/ BITS_PER_BYTE,
		GFP_KERNEL);
	if (!bitmap->map.bits)
		return -ENOMEM;
	return 0;
}

static void __pmem_remove_bitsfit(int id)
{
	kfree(pmem[id].bitmap->map.bits);
}

/* PMEM_ALLOCATOR_FIRSTFIT, the first-fit allocator */

static int __pmem_allocate_firstfit(int id, struct pmem_region *index)
{
	struct pmem_bitmap *bitmap = pmem[id].bitmap;
	int start, end;
	unsigned long size = (index->len + PMEM_MIN_ALLOC - 1) >> PMEM_MIN_SHIFT;

	index->len = 0;
	end = bitmap->num_entries;
	start = bitmap_find_next_zero_area(bitmap->map.bits, end, 0, size, 0);
	if (start >= end)
		return -ENOMEM;
	bitmap_set(bitmap->map.bits, start, size);
	index->offset = start;
	index->len = size;
	return 0;
}

static int __pmem_free_bitsfit(int id, struct pmem_region *index)
{
	struct pmem_bitmap *bitmap = pmem[id].bitmap;
	if ((index->offset + index->len) < bitmap->num_entries)
		bitmap_clear(bitmap->map.bits, index->offset, index->len);
	return 0;
}

static unsigned long __pmem_len_bitsfit(int id, struct pmem_region *index)
{
	return PMEM_OFFSET(index->len);
}

static void __pmem_dump_bitsfit(int id)
{
	struct pmem_bitmap *bitmap = pmem[id].bitmap;
	char buf[64];
	const unsigned long *curr;
	unsigned long remains = bitmap->num_entries, n;

	for (curr = bitmap->map.bits; remains;
	     curr += n / sizeof(unsigned long)) {
		n = bitmap_scnprintf(buf, sizeof(buf), curr, remains);
		remains -= n;
		printk("%s\n", buf);
	}
}

/* The array of allocators */

struct pmem_allocator allocators[PMEM_ALLOCATOR_NUM] = {
	/* PMEM_ALLOCATOR_NONE */
	{
		.name = "none",
		.setup = __pmem_setup_none,
		.remove = __pmem_remove_none,
		.allocate = __pmem_allocate_none,
		.free = __pmem_free_none,
		.start_addr = __pmem_start_addr_none,
		.len = __pmem_len_none,
		.dump = __pmem_dump_none,
	},
	/* PMEM_ALLOCATOR_BUDDY */
	{
		.name = "buddy",
		.setup = __pmem_setup_buddy,
		.remove = __pmem_remove_buddy,
		.allocate = __pmem_allocate_buddy,
		.free = __pmem_free_buddy,
		.start_addr = __pmem_start_addr,
		.len = __pmem_len_buddy,
		.dump = __pmem_dump_buddy,
	},
	/* PMEM_ALLOCATOR_FIRSTFIT */
	{
		.name = "first-fit",
		.setup = __pmem_setup_bitsfit,
		.remove = __pmem_remove_bitsfit,
		.allocate = __pmem_allocate_firstfit,
		.free = __pmem_free_bitsfit,
		.start_addr = __pmem_start_addr,
		.len = __pmem_len_bitsfit,
		.dump = __pmem_dump_bitsfit,
	},
};
