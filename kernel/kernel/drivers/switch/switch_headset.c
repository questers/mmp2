/*
 *	drivers/switch/switch_headset.c
 *
 *	headset detect driver for Android
 *
 * 	Copyright (C) 2010, Marvell Corporation
 *	Author: Jiangang Jing <jgjing@marvell.com>
 *		Libin Yang <lbyang@marvell.com>
 *	Author: Raul Xiong <xjian@marvell.com>
 *		Mike Lockwood <lockwood@android.com>
 *
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License version 2 as
 *	published by the Free Software Foundation.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/switch.h>
#include <linux/workqueue.h>
#include <linux/gpio.h>
#include <linux/errno.h>
#include <mach/hardware.h>
#include <mach/cputype.h>
#include <linux/delay.h>
#include <linux/slab.h>

#include "switch_headset.h"

static void headset_switch_work(struct work_struct *work)
{
	struct headset_switch_data	*data =
		container_of(work, struct headset_switch_data, delay_work.work);
	int state;


	state = gpio_get_value(data->gpio);
	if (state)
		state = 1;
	if(state)
	{
		#ifdef CONFIG_SND_SOC_RT5625
		if(rt5625_headset_detect())
			state = 2;
		#endif
	}
	printk("headset switch state =%d\n",state);
	/*
	 *  state meaning
	 *  0: no headset plug in
	 *  1: headset with microphone plugged
	 *  2: headset without microphone plugged
	*/
	switch_set_state(&data->sdev, state);
}


static irqreturn_t headset_irq_handler(int irq, void *dev_id)
{
	struct headset_switch_data *switch_data =
	    (struct headset_switch_data *)dev_id;
	cancel_delayed_work(&switch_data->delay_work);
	queue_delayed_work(switch_data->wq,&switch_data->delay_work,msecs_to_jiffies(switch_data->debounce_ms));
	return IRQ_HANDLED;
}

static ssize_t switch_headset_print_state(struct switch_dev *sdev, char *buf)
{
	struct headset_switch_data	*switch_data =
		container_of(sdev, struct headset_switch_data, sdev);
	const char *state;
	if (switch_get_state(sdev))
		state = switch_data->state_on;
	else
		state = switch_data->state_off;

	if (state)
		return sprintf(buf, "%s\n", state);
	return -1;
}

static int headset_switch_probe(struct platform_device *pdev)
{
	struct gpio_switch_platform_data *pdata = pdev->dev.platform_data;
	struct headset_switch_data *switch_data;
	int ret = 0;
	if (!pdata)
		return -EBUSY;

	switch_data = kzalloc(sizeof(struct headset_switch_data), GFP_KERNEL);
	if (!switch_data)
		return -ENOMEM;

	switch_data->sdev.name = pdata->name;
	switch_data->gpio = pdata->gpio;
	switch_data->name_on = pdata->name_on;
	switch_data->name_off = pdata->name_off;
	switch_data->state_on = pdata->state_on;
	switch_data->state_off = pdata->state_off;
	switch_data->debounce_ms = pdata->debounce_ms;
	switch_data->sdev.print_state = switch_headset_print_state;
	ret = switch_dev_register(&switch_data->sdev);
	if (ret < 0)
		goto err_switch_dev_register;

	switch_data->wq = create_singlethread_workqueue("h2w");

	INIT_DELAYED_WORK(&switch_data->delay_work, headset_switch_work);
	switch_data->irq = gpio_to_irq(switch_data->gpio);
	if (switch_data->irq < 0) {
		ret = switch_data->irq;
		goto err_detect_irq_num_failed;

	}

	ret = request_irq(switch_data->irq, headset_irq_handler,
			  IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING,
			  pdev->name, switch_data);

	if (ret < 0)
		goto err_request_irq;

	//initial check
	queue_delayed_work(switch_data->wq,&switch_data->delay_work,0);

	return 0;

err_request_irq:
err_detect_irq_num_failed:
	switch_dev_unregister(&switch_data->sdev);

err_switch_dev_register:
	kfree(switch_data);

	return ret;
}

static int __devexit headset_switch_remove(struct platform_device *pdev)
{
	struct headset_switch_data *switch_data = platform_get_drvdata(pdev);

	cancel_delayed_work(&switch_data->delay_work);

	gpio_free(switch_data->gpio);
	switch_dev_unregister(&switch_data->sdev);
	kfree(switch_data);

	return 0;
}

static struct platform_driver headset_switch_driver = {
	.probe		= headset_switch_probe,
	.remove		= __devexit_p(headset_switch_remove),
	.driver		= {
		.name	= "headset",
		.owner	= THIS_MODULE,
	},
};

static int __init headset_switch_init(void)
{
	return platform_driver_register(&headset_switch_driver);
}

static void __exit headset_switch_exit(void)
{
	platform_driver_unregister(&headset_switch_driver);
}

late_initcall(headset_switch_init);
module_exit(headset_switch_exit);

MODULE_AUTHOR("Mike Lockwood <lockwood@android.com>");
MODULE_DESCRIPTION("headset Switch driver");
MODULE_LICENSE("GPL");
