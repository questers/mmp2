/*
 * Copyright (C) 2010 MEMSIC, Inc.
 *
 * Initial Code:
 *	Robbie Cao
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 */

#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/miscdevice.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/workqueue.h>
#include <linux/freezer.h>
#include <asm/uaccess.h>

#include "mecs.h"

#define DEBUG			0

/* #define ECS_DATA_DEV_NAME	"ecompass_data" */
#define ECS_CTRL_DEV_NAME	"ecompass_ctrl"

static int ecs_ctrl_open(struct inode *inode, struct file *file);
static int ecs_ctrl_release(struct inode *inode, struct file *file);
static int ecs_ctrl_ioctl(struct inode *inode, struct file *file,
			  unsigned int cmd, unsigned long arg);

static DECLARE_WAIT_QUEUE_HEAD(open_wq);

static atomic_t open_count;
static atomic_t open_flag;
static atomic_t reserve_open_flag;

static atomic_t a_flag;
static atomic_t m_flag;
static atomic_t o_flag;

static short ecompass_delay = 0;

enum _ecs_data_devs {
	ECS_DATA_ACC_DEV = 0,
	ECS_DATA_MAG_DEV,
	ECS_DATA_ORI_DEV,
	ECS_DATA_MAX_DEV,
};

static char ecs_data_devs_name[ECS_DATA_MAX_DEV][20] = {
	[ECS_DATA_ACC_DEV] = "ecompass_acc_data",
	[ECS_DATA_MAG_DEV] = "ecompass_mag_data",
	[ECS_DATA_ORI_DEV] = "ecompass_ori_data",
};

static char ecs_input_devs_name[ECS_DATA_MAX_DEV][10];

/* static struct input_dev *ecs_data_device; */
static struct input_dev *ecs_data_devs[ECS_DATA_MAX_DEV];

static struct file_operations ecs_ctrl_fops = {
	.owner = THIS_MODULE,
	.open = ecs_ctrl_open,
	.release = ecs_ctrl_release,
	.ioctl = ecs_ctrl_ioctl,
};

static struct miscdevice ecs_ctrl_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = ECS_CTRL_DEV_NAME,
	.fops = &ecs_ctrl_fops,
};

static int ecs_ctrl_open(struct inode *inode, struct file *file)
{
#if 1
	atomic_set(&reserve_open_flag, 1);
	atomic_set(&open_flag, 1);
	atomic_set(&open_count, 1);
	wake_up(&open_wq);

	return 0;
#else
	int ret = -1;

	if (atomic_cmpxchg(&open_count, 0, 1) == 0) {
		if (atomic_cmpxchg(&open_flag, 0, 1) == 0) {
			atomic_set(&reserve_open_flag, 1);
			wake_up(&open_wq);
			ret = 0;
		}
	}

	return ret;
#endif
}

static int ecs_ctrl_release(struct inode *inode, struct file *file)
{
	atomic_set(&reserve_open_flag, 0);
	atomic_set(&open_flag, 0);
	atomic_set(&open_count, 0);
	wake_up(&open_wq);

	return 0;
}

static int ecs_ctrl_ioctl(struct inode *inode, struct file *file,
			  unsigned int cmd, unsigned long arg)
{
	void __user *pa = (void __user *)arg;
	short flag;
	short delay;
	int parms[4];
	int ypr[12];
	struct input_dev *ecs_data_device = NULL;

	switch (cmd) {
	case ECOMPASS_IOC_SET_MODE:
		break;
	case ECOMPASS_IOC_SET_DELAY:
		if (copy_from_user(&delay, pa, sizeof(delay)))
			return -EFAULT;
		ecompass_delay = delay;
		break;
	case ECOMPASS_IOC_GET_DELAY:
		delay = ecompass_delay;
		if (copy_to_user(pa, &delay, sizeof(delay)))
			return -EFAULT;
		break;

	case ECOMPASS_IOC_SET_AFLAG:
		if (copy_from_user(&flag, pa, sizeof(flag)))
			return -EFAULT;
		if (flag < 0 || flag > 1)
			return -EINVAL;
		atomic_set(&a_flag, flag);
		break;
	case ECOMPASS_IOC_GET_AFLAG:
		flag = atomic_read(&a_flag);
		if (copy_to_user(pa, &flag, sizeof(flag)))
			return -EFAULT;
		break;
	case ECOMPASS_IOC_SET_MFLAG:
		if (copy_from_user(&flag, pa, sizeof(flag)))
			return -EFAULT;
		if (flag < 0 || flag > 1)
			return -EINVAL;
		atomic_set(&m_flag, flag);
		break;
	case ECOMPASS_IOC_GET_MFLAG:
		flag = atomic_read(&m_flag);
		if (copy_to_user(pa, &flag, sizeof(flag)))
			return -EFAULT;
		break;
	case ECOMPASS_IOC_SET_OFLAG:
		if (copy_from_user(&flag, pa, sizeof(flag)))
			return -EFAULT;
		if (flag < 0 || flag > 1)
			return -EINVAL;
		atomic_set(&o_flag, flag);
		break;
	case ECOMPASS_IOC_GET_OFLAG:
		flag = atomic_read(&o_flag);
		if (copy_to_user(pa, &flag, sizeof(flag)))
			return -EFAULT;
		break;

	case ECOMPASS_IOC_SET_APARMS:
		if (copy_from_user(parms, pa, sizeof(parms)))
			return -EFAULT;
		ecs_data_device = ecs_data_devs[ECS_DATA_ACC_DEV];
		/* acceleration x-axis */
		input_set_abs_params(ecs_data_device, ABS_X,
				     parms[0], parms[1], parms[2], parms[3]);
		/* acceleration y-axis */
		input_set_abs_params(ecs_data_device, ABS_Y,
				     parms[0], parms[1], parms[2], parms[3]);
		/* acceleration z-axis */
		input_set_abs_params(ecs_data_device, ABS_Z,
				     parms[0], parms[1], parms[2], parms[3]);
		break;
	case ECOMPASS_IOC_GET_APARMS:
		break;
	case ECOMPASS_IOC_SET_MPARMS:
		if (copy_from_user(parms, pa, sizeof(parms)))
			return -EFAULT;
		ecs_data_device = ecs_data_devs[ECS_DATA_MAG_DEV];
		/* magnetic raw x-axis */
		input_set_abs_params(ecs_data_device, ABS_HAT0X,
				     parms[0], parms[1], parms[2], parms[3]);
		/* magnetic raw y-axis */
		input_set_abs_params(ecs_data_device, ABS_HAT0Y,
				     parms[0], parms[1], parms[2], parms[3]);
		/* magnetic raw z-axis */
		input_set_abs_params(ecs_data_device, ABS_BRAKE,
				     parms[0], parms[1], parms[2], parms[3]);
		break;
	case ECOMPASS_IOC_GET_MPARMS:
		break;
	case ECOMPASS_IOC_SET_OPARMS_YAW:
		if (copy_from_user(parms, pa, sizeof(parms)))
			return -EFAULT;
		ecs_data_device = ecs_data_devs[ECS_DATA_ORI_DEV];
		/* orientation yaw */
		input_set_abs_params(ecs_data_device, ABS_RX,
				     parms[0], parms[1], parms[2], parms[3]);
		break;
	case ECOMPASS_IOC_GET_OPARMS_YAW:
		break;
	case ECOMPASS_IOC_SET_OPARMS_PITCH:
		if (copy_from_user(parms, pa, sizeof(parms)))
			return -EFAULT;
		ecs_data_device = ecs_data_devs[ECS_DATA_ORI_DEV];
		/* orientation pitch */
		input_set_abs_params(ecs_data_device, ABS_RY,
				     parms[0], parms[1], parms[2], parms[3]);
		break;
	case ECOMPASS_IOC_GET_OPARMS_PITCH:
		break;
	case ECOMPASS_IOC_SET_OPARMS_ROLL:
		if (copy_from_user(parms, pa, sizeof(parms)))
			return -EFAULT;
		ecs_data_device = ecs_data_devs[ECS_DATA_ORI_DEV];
		/* orientation roll */
		input_set_abs_params(ecs_data_device, ABS_RZ,
				     parms[0], parms[1], parms[2], parms[3]);
		break;
	case ECOMPASS_IOC_GET_OPARMS_ROLL:
		break;

	case ECOMPASS_IOC_SET_YPR:
		if (copy_from_user(ypr, pa, sizeof(ypr)))
			return -EFAULT;
		/* Report acceleration sensor information */
		if (atomic_read(&a_flag)) {
			/* bma150 G-sensor x, y, z axis data will be reported
			 * from bma150 driver
			 */
			/* ecs_data_device = ecs_data_devs[ECS_DATA_ACC_DEV];
			   input_report_abs(ecs_data_device, ABS_X, ypr[0]);
			   input_report_abs(ecs_data_device, ABS_Y, ypr[1]);
			   input_report_abs(ecs_data_device, ABS_Z, ypr[2]);
			   input_report_abs(ecs_data_device, ABS_WHEEL, ypr[3]);
			   input_sync(ecs_data_device); */
		}

		/* Report magnetic sensor information */
		if (atomic_read(&m_flag)) {
			ecs_data_device = ecs_data_devs[ECS_DATA_MAG_DEV];
			input_report_abs(ecs_data_device, ABS_X, ypr[4]);
			input_report_abs(ecs_data_device, ABS_Y, ypr[5]);
			input_report_abs(ecs_data_device, ABS_Z, ypr[6]);
			input_report_abs(ecs_data_device, ABS_BRAKE, ypr[7]);
			input_sync(ecs_data_device);
		}

		/* Report orientation information */
		if (atomic_read(&o_flag)) {
			ecs_data_device = ecs_data_devs[ECS_DATA_ORI_DEV];
			input_report_abs(ecs_data_device, ABS_RX, ypr[8]);
			input_report_abs(ecs_data_device, ABS_RY, ypr[9]);
			input_report_abs(ecs_data_device, ABS_RZ, ypr[10]);
			input_report_abs(ecs_data_device, ABS_BRAKE, ypr[11]);
			input_sync(ecs_data_device);
		}

		/* input_sync(ecs_data_device); */
		break;

	default:
		break;
	}

	return 0;
}

static ssize_t ecs_ctrl_show(struct device *dev, struct device_attribute *attr,
			     char *buf)
{
	ssize_t ret = 0;

	sprintf(buf, "ecompass_ctrl");
	ret = strlen(buf) + 1;

	return ret;
}

/* active sys fs for Marvell sensor HAL */
static ssize_t ecs_active_show(struct device *dev,
			       struct device_attribute *attr, char *buf)
{
	int i;
	int status = 0;
	int count = 0;

	for (i = 0; i < ECS_DATA_MAX_DEV; i++)
		if (!strcmp(ecs_input_devs_name[i], dev->kobj.name))
			break;

	switch (i) {
	case ECS_DATA_ACC_DEV:
		if (atomic_read(&a_flag) == 0)
			status = 0;
		else
			status = 1;
		break;
	case ECS_DATA_MAG_DEV:
		if (atomic_read(&m_flag) == 0)
			status = 0;
		else
			status = 1;
		break;
	case ECS_DATA_ORI_DEV:
		if (atomic_read(&o_flag) == 0)
			status = 0;
		else
			status = 1;
		break;
	default:
		printk(KERN_WARNING "%s: active wrong sensor\n", __func__);
		break;
	}

	if (i < ECS_DATA_MAX_DEV)
		count = sprintf(buf, "%s status: %d\n", ecs_input_devs_name[i],
				status);

	return count;
}

static ssize_t ecs_active_set(struct device *dev,
			      struct device_attribute *attr,
			      const char *buf, size_t count)
{
	int ctrl = 0;
	int i;

	/* from open function */
	atomic_set(&reserve_open_flag, 1);
	atomic_set(&open_flag, 1);
	atomic_set(&open_count, 1);
	wake_up(&open_wq);

	for (i = 0; i < ECS_DATA_MAX_DEV; i++)
		if (!strcmp(ecs_input_devs_name[i], dev->kobj.name))
			break;

	if (i >= ECS_DATA_MAX_DEV) {
		printk(KERN_WARNING "%s: unrecognized ecs dev\n", __func__);
		return count;

	}

	if (strcmp(buf, "1\n") == 0)
		ctrl = 1;
	else
		ctrl = 0;

	switch (i) {
	case ECS_DATA_ACC_DEV:
		atomic_set(&a_flag, ctrl);
		break;
	case ECS_DATA_MAG_DEV:
		atomic_set(&m_flag, ctrl);
		break;
	case ECS_DATA_ORI_DEV:
		atomic_set(&o_flag, ctrl);
		break;
	default:
		printk(KERN_WARNING "%s: unrecognized, using on/off\n",
		       __func__);
		break;
	}

	printk(KERN_INFO "%s: dev [%s] active %s\n", __func__,
	       ecs_input_devs_name[i], buf);

	return count;
}

static ssize_t ecs_interval_set(struct device *dev,
				struct device_attribute *attr,
				const char *buf, size_t count)
{
	unsigned int val = 0;
	char msg[256] = "\0";
	if (count > 256)
		count = 256;
	memcpy(msg, buf, count - 1);	/*skip last "\n" */
	msg[count - 1] = '\0';
	val = (unsigned int)simple_strtoul(msg, NULL, 10);
	if (val < 5)		/*TODO:avoid i2c too busy */
		val = 5;
	ecompass_delay = val;	/*double increase sample rate */
	printk(KERN_INFO "bma150 sample interval %d ms\n", ecompass_delay);
	return count;
}

static ssize_t ecs_interval_show(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", ecompass_delay);
}

static DEVICE_ATTR(ecs_ctrl, S_IRUGO, ecs_ctrl_show, NULL);
static DEVICE_ATTR(active, S_IRUGO | S_IWUGO, ecs_active_show, ecs_active_set);
static DEVICE_ATTR(interval, S_IRUGO | S_IWUGO, ecs_interval_show,
		   ecs_interval_set);

static struct attribute *ecs_attributes[] = {
	&dev_attr_active.attr,
	&dev_attr_interval.attr,
	NULL
};

static struct attribute_group ecs_attribute_group = {
	.attrs = ecs_attributes
};

static int ecs_add_fs(struct device *device)
{
	return sysfs_create_group(&device->kobj, &ecs_attribute_group);
}

static int ecs_remove_fs(struct device *device)
{
	sysfs_remove_group(&device->kobj, &ecs_attribute_group);
	return 0;
}

static int __init ecompass_init(void)
{
	int res = 0;
	int i = 0;
	int j = 0;
	int k = 0;

	pr_info("ecompass driver: init\n");

	/* ecs_data_device = input_allocate_device();
	   if (!ecs_data_device) {
	   res = -ENOMEM;
	   pr_err("%s: failed to allocate input device\n", __FUNCTION__);
	   goto out;
	   } */

	for (i = 0; i < ECS_DATA_MAX_DEV; i++) {
		ecs_data_devs[i] = input_allocate_device();
		if (!ecs_data_devs[i]) {
			res = -ENOMEM;
			pr_err("%s: failed to allocate input device %d\n",
			       __func__, i);
			goto failed_alloc_input_devs;
		}
		set_bit(EV_ABS, ecs_data_devs[i]->evbit);
	}

	/* set_bit(EV_ABS, ecs_data_device->evbit); */

	/* 32768 == 1g, range -4g ~ +4g */
	/* acceleration x-axis */
	/* input_set_abs_params(ecs_data_device, ABS_X,
	   -32768*4, 32768*4, 0, 0); */
	/* acceleration y-axis */
	/* input_set_abs_params(ecs_data_device, ABS_Y,
	   -32768*4, 32768*4, 0, 0); */
	/* acceleration z-axis */
	/* input_set_abs_params(ecs_data_device, ABS_Z,
	   -32768*4, 32768*4, 0, 0); */

	/* acceleration x, y, z axis */
	input_set_abs_params(ecs_data_devs[ECS_DATA_ACC_DEV], ABS_X,
			     -32768 * 4, 32768 * 4, 0, 0);
	input_set_abs_params(ecs_data_devs[ECS_DATA_ACC_DEV], ABS_Y,
			     -32768 * 4, 32768 * 4, 0, 0);
	input_set_abs_params(ecs_data_devs[ECS_DATA_ACC_DEV], ABS_Z,
			     -32768 * 4, 32768 * 4, 0, 0);

	/* 32768 == 1gauss, range -4gauss ~ +4gauss */
	/* magnetic raw x-axis */
	/* input_set_abs_params(ecs_data_device, ABS_HAT0X,
	   -32768*4, 32768*4, 0, 0); */
	/* magnetic raw y-axis */
	/* input_set_abs_params(ecs_data_device, ABS_HAT0Y,
	   -32768*4, 32768*4, 0, 0); */
	/* magnetic raw z-axis */
	/* input_set_abs_params(ecs_data_device, ABS_BRAKE,
	   -32768*4, 32768*4, 0, 0); */

	/* magnetic raw x, y, z axis */
	input_set_abs_params(ecs_data_devs[ECS_DATA_MAG_DEV], ABS_HAT0X,
			     -32768 * 4, 32768 * 4, 0, 0);
	input_set_abs_params(ecs_data_devs[ECS_DATA_MAG_DEV], ABS_HAT0Y,
			     -32768 * 4, 32768 * 4, 0, 0);
	input_set_abs_params(ecs_data_devs[ECS_DATA_MAG_DEV], ABS_BRAKE,
			     -32768 * 4, 32768 * 4, 0, 0);

	/* 65536 == 360degree */
	/* orientation yaw, 0 ~ 360 */
	/* input_set_abs_params(ecs_data_device, ABS_RX,
	   0, 65536, 0, 0); */
	/* orientation pitch, -180 ~ 180 */
	/* input_set_abs_params(ecs_data_device, ABS_RY,
	   -65536/2, 65536/2, 0, 0); */
	/* orientation roll, -90 ~ 90 */
	/* input_set_abs_params(ecs_data_device, ABS_RZ,
	   -65536/4, 65536/4, 0, 0); */

	/* orientation yaw, pitch, roll */
	input_set_abs_params(ecs_data_devs[ECS_DATA_ORI_DEV], ABS_RX,
			     0, 65536, 0, 0);
	input_set_abs_params(ecs_data_devs[ECS_DATA_ORI_DEV], ABS_RY,
			     -65536 / 2, 65536 / 2, 0, 0);
	input_set_abs_params(ecs_data_devs[ECS_DATA_ORI_DEV], ABS_RZ,
			     -65536 / 4, 65536 / 4, 0, 0);

	/* ecs_data_device->name = ECS_DATA_DEV_NAME;
	   res = input_register_device(ecs_data_device);
	   if (res) {
	   pr_err("%s: unable to register input device: %s\n",
	   __FUNCTION__, ecs_data_device->name);
	   goto out_free_input;
	   } */

	for (j = 0; j < ECS_DATA_MAX_DEV; j++) {
		ecs_data_devs[j]->name = ecs_data_devs_name[j];
		res = input_register_device(ecs_data_devs[j]);
		if (res) {
			pr_err("%s: unable to register input device: %d\n",
			       __FUNCTION__, j);
			goto failed_register_input_devs;
		}
		sprintf(ecs_input_devs_name[j],
			ecs_data_devs[j]->dev.kobj.name);
		printk(KERN_INFO "%s: input_devs %d name: %s\n", __func__, j,
		       ecs_input_devs_name[j]);
	}

	res = misc_register(&ecs_ctrl_device);
	if (res) {
		pr_err("%s: ecs_ctrl_device register failed\n", __FUNCTION__);
		goto failed_register_input_devs;
	}
	res =
	    device_create_file(ecs_ctrl_device.this_device, &dev_attr_ecs_ctrl);
	if (res) {
		pr_err("%s: device_create_file failed\n", __FUNCTION__);
		goto out_deregister_misc;
	}

	/* create sys fs for ecs data devices */
	for (k = 0; k < ECS_DATA_MAX_DEV; k++) {
		res = ecs_add_fs(&ecs_data_devs[k]->dev);
		if (res) {
			pr_err("%s: ecs_add_fs %d failed\n", __func__, k);
			goto failed_add_data_devs_sysfs;
		}
	}

	return 0;

failed_add_data_devs_sysfs:
	while (k--)
		ecs_remove_fs(&ecs_data_devs[k]->dev);
	device_remove_file(ecs_ctrl_device.this_device, &dev_attr_ecs_ctrl);
out_deregister_misc:
	misc_deregister(&ecs_ctrl_device);
/* out_free_input:
	input_free_device(ecs_data_device); */
failed_register_input_devs:
	while (j--)
		input_unregister_device(ecs_data_devs[j]);
/* out: */
failed_alloc_input_devs:
	while (i--)
		input_free_device(ecs_data_devs[i]);
	return res;
}

static void __exit ecompass_exit(void)
{
	int i = 0;
	pr_info("ecompass driver: exit\n");
	for (i = 0; i < ECS_DATA_MAX_DEV; i++) {
		ecs_remove_fs(&ecs_data_devs[i]->dev);
		input_unregister_device(ecs_data_devs[i]);
		input_free_device(ecs_data_devs[i]);
	}
	device_remove_file(ecs_ctrl_device.this_device, &dev_attr_ecs_ctrl);
	misc_deregister(&ecs_ctrl_device);
	/* input_free_device(ecs_data_device); */
}

module_init(ecompass_init);
module_exit(ecompass_exit);

MODULE_AUTHOR("Sarah Zhang <xiazh@marvell.com>");
MODULE_DESCRIPTION("MEMSIC eCompass Driver");
MODULE_LICENSE("GPL");
