/*
 *  linux/drivers/media/video/pxa688_camera.c - MMP2 MMCI driver
 *
 *  Based on linux/drivers/media/video/cafe_ccic.c
 *
 *  Copyright:	(C) Copyright 2008 Marvell International Ltd.
 *              Mingwei Wang <mwwang@marvell.com>
 *
 * A driver for the CMOS camera controller in the Marvell 88ALP01 "cafe"
 * multifunction chip.  Currently works with the Omnivision OV7670
 * sensor.
 *
 * The data sheet for this device can be found at:
 *    http://www.marvell.com/products/pcconn/88ALP01.jsp
 *
 * Copyright 2006 One Laptop Per Child Association, Inc.
 * Copyright 2006-7 Jonathan Corbet <corbet@lwn.net>
 *
 * Written by Jonathan Corbet, corbet@lwn.net.
 *
 * This file may be distributed under the terms of the GNU General
 * Public License, version 2.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/spinlock.h>
#include <linux/videodev2.h>
#include <linux/videodev.h>
#include <linux/wait.h>
#include <linux/list.h>
#include <linux/dma-mapping.h>
#include <linux/delay.h>
#include <linux/jiffies.h>
#include <linux/vmalloc.h>
#include <linux/highmem.h>
#include <linux/clk.h>
#include <linux/platform_device.h>
/* for kzalloc()/kfree() in kernel 2.6.35 */
#include <linux/slab.h>

#include <media/v4l2-ioctl.h>
#include <media/v4l2-common.h>
#include <media/v4l2-chip-ident.h>
#include <media/v4l2-device.h>

#include <asm/uaccess.h>
#include <asm/io.h>
#include <asm/cacheflush.h>
#include <asm/mach-types.h>

#include <mach/camera.h>
#include <mach/mfp.h>
#include <mach/mfp-mmp2.h>
#include <mach/mmp2_plat_ver.h>

#include "pxa688_camera.h"

#ifdef CONFIG_DVFM
#include <mach/dvfm.h>

static int dvfm_dev_idx;
static void set_dvfm_constraint(void)
{
	//dvfm_disable(dvfm_dev_idx);
	dvfm_disable_op_name("Ultra Low MIPS", dvfm_dev_idx);
	dvfm_disable_op_name("Low MIPS", dvfm_dev_idx);
	dvfm_disable_op_name("Video/Graphics M MIPS", dvfm_dev_idx);
	dvfm_disable_op_name("Ultra High MIPS", dvfm_dev_idx);

	dvfm_disable_op_name("apps_idle", dvfm_dev_idx);
	dvfm_disable_op_name("apps_sleep", dvfm_dev_idx);
	dvfm_disable_op_name("chip_sleep", dvfm_dev_idx);
	dvfm_disable_op_name("sys_sleep", dvfm_dev_idx);
}

static void unset_dvfm_constraint(void)
{
	//dvfm_enable(dvfm_dev_idx);
	dvfm_enable_op_name("Ultra Low MIPS", dvfm_dev_idx);
	dvfm_enable_op_name("Low MIPS", dvfm_dev_idx);
	dvfm_enable_op_name("Video/Graphics M MIPS", dvfm_dev_idx);
	dvfm_enable_op_name("Ultra High MIPS", dvfm_dev_idx);

	dvfm_enable_op_name("apps_idle", dvfm_dev_idx);
	dvfm_enable_op_name("apps_sleep", dvfm_dev_idx);
	dvfm_enable_op_name("chip_sleep", dvfm_dev_idx);
	dvfm_enable_op_name("sys_sleep", dvfm_dev_idx);
}
#else
static void set_dvfm_constraint(void) {}
static void unset_dvfm_constraint(void) {}
#endif

#define ENABLE_TWO_LANES
//#define DUMP_REG_DEBUG
//#define SOF_EOF_DEBUG
//#define DEV_OP_DEBUG

/* Define device operation debug by IOCTL */
#ifdef DEV_OP_DEBUG
	#define ioctl_debug(filp); \
	do { \
		printk("%s:%d, filp->f_count = %d\n", __FUNCTION__, __LINE__, filp->f_count.counter); \
	} while (0);
#endif

#define CCIC_VERSION 0x000001
/*
 * Parameters.
 */
MODULE_AUTHOR("Jonathan Corbet <corbet@lwn.net>");
MODULE_DESCRIPTION("Marvell 88ALP01 CMOS Camera Controller driver");
MODULE_LICENSE("GPL");
MODULE_SUPPORTED_DEVICE("Video");

/*
 * Internal DMA buffer management.  Since the controller cannot do S/G I/O,
 * we must have physically contiguous buffers to bring frames into.
 * These parameters control how many buffers we use, whether we
 * allocate them at load time (better chance of success, but nails down
 * memory) or when somebody tries to use the camera (riskier), and,
 * for load-time allocation, how big they should be.
 *
 * The controller can cycle through three buffers.  We could use
 * more by flipping pointers around, but it probably makes little
 * sense.
 */

//#define DMA_POOL
#define MAX_DMA_BUFS 3
#define MIN_DMA_BUFS 2
#define MAX_DMA_SIZE (2592*1944*2)	/* YUV422Packed 1080P */

static int alloc_bufs_at_read = 0;
static int n_dma_bufs = MAX_DMA_BUFS;	/* frame buffer number */
static int dma_buf_size = MAX_DMA_SIZE;	/* Worst case */
static int min_buffers = 1;
static int max_buffers = 10;
static int flip = 0;
static int detected_high = 0;
static int detected_low = 0;
static int sensor_selected = SENSOR_NONE;
volatile unsigned int ccic_irqs;	/* current ccic irq status */

module_param(alloc_bufs_at_read, bool, 0444);
MODULE_PARM_DESC(alloc_bufs_at_read,
		"Non-zero value causes DMA buffers to be allocated when the "
		"video capture device is read, rather than at module load "
		"time. This saves memory, but decreases the chances of "
		"successfully getting those buffers.");

module_param(n_dma_bufs, uint, 0644);
MODULE_PARM_DESC(n_dma_bufs,
		"The number of DMA buffers to allocate. Can be either two "
		"(saves memory, makes timing tighter) or three.");

module_param(dma_buf_size, uint, 0444);
MODULE_PARM_DESC(dma_buf_size,
		"The size of the allocated DMA buffers. If actual operating "
		"parameters require larger buffers, an attempt to reallocate "
		"will be made.");

module_param(min_buffers, uint, 0644);
MODULE_PARM_DESC(min_buffers,
		"The minimum number of streaming I/O buffers we are willing "
		"to work with.");

module_param(max_buffers, uint, 0644);
MODULE_PARM_DESC(max_buffers,
		"The maximum number of streaming I/O buffers an application "
		"will be allowed to allocate. These buffers are big and live "
		"in vmalloc space.");

module_param(flip, bool, 0444);
MODULE_PARM_DESC(flip,
		"If set, the sensor will be instructed to flip the image "
		"vertically.");

enum ccic_state {
	S_NOTREADY,	/* Not yet initialized */
	S_IDLE,		/* Just hanging around */
	S_FLAKED,	/* Some sort of problem */
	S_SINGLEREAD,	/* In read() */
	S_SPECREAD,   	/* Speculative read (for future read()) */
	S_STREAMING	/* Streaming data */
};

struct yuv_pointer_t {
	dma_addr_t y;
	dma_addr_t u;
	dma_addr_t v;
};

/*
 * Tracking of streaming I/O buffers.
 */
struct ccic_sio_buffer {
	struct list_head list;
	struct v4l2_buffer v4lbuf;
	char *buffer;   /* Where it lives in kernel space */
	int mapcount;
	struct ccic_camera *cam;
	struct vm_area_struct *svma;
	dma_addr_t dma_handles;
	struct page *page;
	struct yuv_pointer_t yuv_p;
};

/*
 * A description of one of our devices.
 * Locking: controlled by s_mutex.  Certain fields, however, require
 * 	    the dev_lock spinlock; they are marked as such by comments.
 *	    dev_lock is also required for access to device registers.
 */
struct ccic_camera
{
	enum ccic_state state;
	unsigned long flags;   		/* Buffer status, mainly (dev_lock) */
	int users;			/* How many open FDs */
	struct file *owner;		/* Who has data access (v4l2) */

	/*
	 * Subsystem structures.
	 */
	int irq;
	struct platform_device *pdev;
	struct video_device v4ldev;
	struct i2c_adapter i2c_adapter;
	struct i2c_client *sensor;
	struct i2c_client *sensors[SENSOR_MAX];
	unsigned int bus_type[SENSOR_MAX];	/* parrallel or MIPI */
	unsigned int ident[SENSOR_MAX];

	unsigned char __iomem *regs;
	struct list_head dev_list;		/* link to other devices */

	/* DMA buffers */
	unsigned int nbufs;			/* How many buffer are allocated */
	unsigned int n_mapbufs;			/* How many buffer from user point */
	int next_buf;				/* Next to consume (dev_lock) */
	unsigned int dma_buf_size;		/* allocated size */
	int order[MAX_DMA_BUFS];		/* Internal buffer addresses */
	void *dma_bufs[MAX_DMA_BUFS];		/* Internal buffer addresses */
	dma_addr_t dma_handles[MAX_DMA_BUFS];	/* Buffer bus addresses */
	struct page *pages[MAX_DMA_BUFS];
	struct page *page;
	void *rubbish_buf_virt;
	dma_addr_t rubbish_buf_phy;

	/* Streaming buffers */
	unsigned int n_sbufs;			/* How many we have */
	struct ccic_sio_buffer *sb_bufs;	/* The array of housekeeping structs */
	struct list_head sb_avail;		/* Available for data (we own) (dev_lock) */
	struct list_head sb_full;		/* With data (user space owns) (dev_lock) */
	struct list_head sb_dma;		/* dma list (dev_lock) */
	struct tasklet_struct s_tasklet;

	/* Current operating parameters */
	u32 sensor_type;			/* Currently sensor */
	struct v4l2_pix_format pix_format;

	/* Locks */
	struct mutex s_mutex;			/* Access to this structure */

	/*  Access to DMA BASE address registers and LIST
	 *  sb_avail, sb_full, sb_dma will be protected.
	 */
	spinlock_t dev_lock;			/* Access to device */

	/* Misc */
	wait_queue_head_t iowait;		/* Waiting on frame data */
	unsigned long io_type;

	/* platform data */
	struct cam_platform_data *platform_ops;
};

#define BUS_PARALLEL 	0x01
#define BUS_MIPI 	0x02
#define BUS_IS_PARALLEL(b) (b & BUS_PARALLEL)
#define BUS_IS_MIPI(b) (b & BUS_MIPI)

/*
 * Debugging and related.
 */
#define cam_err(cam, fmt, arg...) \
	dev_err(&(cam)->pdev->dev, fmt, ##arg);
#define cam_warn(cam, fmt, arg...) \
	dev_warn(&(cam)->pdev->dev, fmt, ##arg);
#define cam_dbg(cam, fmt, arg...) \
	dev_dbg(&(cam)->pdev->dev, fmt, ##arg);

/* ---------------------------------------------------------------------*/
/*
 * We keep a simple list of known devices to search at open time.
 */
static LIST_HEAD(ccic_dev_list);
static DEFINE_MUTEX(ccic_dev_list_lock);
static int __ccic_cam_cmd(struct ccic_camera *cam, int cmd, void *arg);

static void ccic_add_dev(struct ccic_camera *cam)
{
	mutex_lock(&ccic_dev_list_lock);
	list_add_tail(&cam->dev_list, &ccic_dev_list);
	mutex_unlock(&ccic_dev_list_lock);
}

static void ccic_remove_dev(struct ccic_camera *cam)
{
	mutex_lock(&ccic_dev_list_lock);
	list_del(&cam->dev_list);
	mutex_unlock(&ccic_dev_list_lock);
}

static struct ccic_camera *ccic_find_dev(int minor)
{
	struct ccic_camera *cam;

	mutex_lock(&ccic_dev_list_lock);
	list_for_each_entry(cam, &ccic_dev_list, dev_list) {
		if (cam->v4ldev.minor == minor)
			goto done;
	}
	cam = NULL;
done:
	mutex_unlock(&ccic_dev_list_lock);
	return cam;
}

static struct ccic_camera *ccic_find_by_pdev(struct platform_device *pdev)
{
	struct ccic_camera *cam;

	mutex_lock(&ccic_dev_list_lock);
	list_for_each_entry(cam, &ccic_dev_list, dev_list) {
		if (cam->pdev == pdev)
			goto done;
	}
	cam = NULL;
done:
	mutex_unlock(&ccic_dev_list_lock);
	return cam;
}

/* ------------------------------------------------------------------------ */
/*
 * Device register I/O
 */
static inline void ccic_reg_write(struct ccic_camera *cam, unsigned int reg, unsigned int val)
{
	__raw_writel(val, cam->regs + reg);
}

static inline unsigned int ccic_reg_read(struct ccic_camera *cam, unsigned int reg)
{
	return __raw_readl(cam->regs + reg);
}

static inline void ccic_reg_write_mask(struct ccic_camera *cam, unsigned int reg, unsigned int val, unsigned int mask)
{
	unsigned int v = ccic_reg_read(cam, reg);

	v = (v & ~mask) | (val & mask);
	ccic_reg_write(cam, reg, v);
}

static inline void ccic_reg_clear_bit(struct ccic_camera *cam, unsigned int reg, unsigned int val)
{
	ccic_reg_write_mask(cam, reg, 0, val);
}

static inline void ccic_reg_set_bit(struct ccic_camera *cam, unsigned int reg, unsigned int val)
{
	ccic_reg_write_mask(cam, reg, val, val);
}

/* only for debug */
#ifdef DUMP_REG_DEBUG
static int dump_register(struct ccic_camera *cam)
{
	unsigned int irqs, reg, i;
	unsigned long flags = 0;

	spin_lock_irqsave(&cam->dev_lock, flags);
	irqs = ccic_reg_read(cam, REG_IRQSTAT);
	printk("CCIC: REG_IRQSTAT is 0x%08X\n", irqs);
	irqs = ccic_reg_read(cam, REG_IRQSTATRAW);
	printk("CCIC: REG_IRQSTATRAW is 0x%08X\n", irqs);
	irqs = ccic_reg_read(cam, REG_IRQMASK);
	printk("CCIC: REG_IRQMASK is 0x%08X\n\n", irqs);
	irqs = ccic_reg_read(cam, REG_IMGPITCH);
	printk("CCIC: REG_IMGPITCH is 0x%08X\n", irqs);
	irqs = ccic_reg_read(cam, REG_IMGSIZE);
	printk("CCIC: REG_IMGSIZE is 0x%08X\n", irqs);
	irqs = ccic_reg_read(cam, REG_IMGOFFSET);
	printk("CCIC: REG_IMGOFFSET is 0x%08X\n\n", irqs);
	irqs = ccic_reg_read(cam, REG_CTRL0);
	printk("CCIC: REG_CTRL0 is 0x%08X\n", irqs);
	irqs = ccic_reg_read(cam, REG_CTRL1);
	printk("CCIC: REG_CTRL1 is 0x%08X\n", irqs);
	irqs = ccic_reg_read(cam, REG_CLKCTRL);
	printk("CCIC: REG_CLKCTRL is 0x%08X\n\n", irqs);
	irqs = ccic_reg_read(cam, REG_CSI2_DPHY3);
	printk("CCIC: REG_CSI2_DPHY3 is 0x%08X\n", irqs);
	irqs = ccic_reg_read(cam, REG_CSI2_DPHY6);
	printk("CCIC: REG_CSI2_DPHY6 is 0x%08X\n", irqs);
	irqs = ccic_reg_read(cam, REG_CSI2_DPHY5);
	printk("CCIC: REG_CSI2_DPHY5 is 0x%08X\n", irqs);
	irqs = ccic_reg_read(cam, REG_CSI2_CTRL0);
	printk("CCIC: REG_CSI2_CTRL0 is 0x%08X\n\n", irqs);
	irqs = ccic_reg_read(cam, REG_Y0BAR );
	printk("CCIC: REG_Y0BAR is 0x%08X\n", irqs);
	irqs = ccic_reg_read(cam, REG_Y0BAR + 4);
	printk("CCIC: REG_Y0BAR is 0x%08X\n", irqs);
	irqs = ccic_reg_read(cam, REG_Y0BAR + 8);
	printk("CCIC: REG_Y0BAR is 0x%08X\n", irqs);

	printk("\n######## Dump CCIC Register for Debug ########\n");
	/* 0xd4200000 map to 0xfe200000 */
	for (reg = 0xfe20a000, i = 0;reg < 0xfe20a200;reg += 0x04, i++) {
		if (i % 4 == 0)
			printk("%08X: %08X ", reg - 0x2a000000, __raw_readl(reg));
		else if (i % 4 == 3)
			printk("%08X\n", __raw_readl(reg));
		else
			printk("%08X ", __raw_readl(reg));
	}
	printk("##############################################\n");
	for (reg = 0xfe282800, i = 0;reg < 0xfe282938;reg += 0x04, i++) {
		if (i % 4 == 0)
			printk("%08X: %08X ", reg - 0x2a000000, __raw_readl(reg));
		else if (i % 4 == 3)
			printk("%08X\n", __raw_readl(reg));
		else
			printk("%08X ", __raw_readl(reg));
	}
	printk("\n######## Dump CCIC Register for Debug ########\n");

	spin_unlock_irqrestore(&cam->dev_lock, flags);
	return 0;
}
#endif

/* provided for sensor calling to enable clock at begining of probe */
void ccic_set_clock(unsigned int reg, unsigned int val)
{
	struct ccic_camera *cam;
	cam = ccic_find_dev(0);
	ccic_reg_write(cam, reg, val);
}

unsigned int ccic_get_clock(unsigned int reg)
{
        struct ccic_camera *cam;
        cam = ccic_find_dev(0);
	return ccic_reg_read(cam, reg);
}

void ccic_disable_mclk(void)
{
	ccic_set_clock(REG_CLKCTRL, 0x0);
}
EXPORT_SYMBOL(ccic_disable_mclk);

void ccic_enable_vclk(void)
{
	ccic_set_clock(REG_CLKCTRL, 0x40000004);
}
EXPORT_SYMBOL(ccic_enable_vclk);

void ccic_enable_mclk(struct ccic_camera *cam)
{
	/*
	 * register CCIC_CLOCK_CTRL
	 * core clock = 396?(33*0xc)
	 * div = floor(396/mclk)
	 * cft = ceil(((396/div-mclk)/(396/div))*0xfff)
	 */
	if (BUS_IS_MIPI(cam->bus_type[sensor_selected]))
	{
		printk("set mipi clock...\n");
		/* 0x3 use Core clock, mclk=25M */
		ccic_reg_write(cam, REG_CLKCTRL, 0x3<<29 | 0x10);
		/* 0x3 use Core clock, mclk=26.6M */
//		ccic_reg_write(cam, REG_CLKCTRL, 0x3<<29 | 0xf);
		/* 0x2 use AXI BUS clock = 266M, mclk=22M */
//		ccic_reg_write(cam, REG_CLKCTRL, 0x2<<29 | 0xc);
	}
	else
	{
		if (cam->ident[sensor_selected] == V4L2_IDENT_GC2015)
		{
			//printk("set 48m mclk...\n");
			ccic_reg_write(cam, REG_CLKCTRL, 0x3<<29 | 0x7d<<16 | 0x08); /* 48M */
		}
		else
		{
			//printk("set 24m mclk...\n");
			ccic_reg_write(cam, REG_CLKCTRL, 0x3<<29 | 0x7d<<16 | 0x10); /* 24M? */
		}
	}
}

static void ccic_stop_at_buf(struct ccic_camera *cam);
static void ccic_ctlr_stop_dma(struct ccic_camera *cam);
static void ccic_ctlr_power_down(struct ccic_camera *cam);

int ccic_sensor_attach(struct i2c_client *client)
{
	struct ccic_camera *cam;
	struct v4l2_dbg_chip_ident chip = { {}, 0, 0 };
	int ret = 0;

	chip.match.type = V4L2_CHIP_MATCH_I2C_ADDR;
	chip.match.addr = 0;

	cam = ccic_find_dev(0);		//TODO - only support one camera controller
	if (cam == NULL){
		printk("camera: didn't find camera device!\n");
		return -ENODEV;
	}

	/*
	 * Don't talk to chips we don't recognize.
	 */
	mutex_lock(&cam->s_mutex);
	cam->sensor = client;
	chip.match.type = V4L2_CHIP_MATCH_I2C_ADDR;
	chip.match.addr = cam->sensor->addr;
	chip.ident = V4L2_IDENT_NONE;
	ret = __ccic_cam_cmd(cam, VIDIOC_DBG_G_CHIP_IDENT, &chip);
	if (ret)
		goto out;

	cam->sensor_type = chip.ident;
	if ((cam->sensor_type == V4L2_IDENT_OV7660)
		|| (cam->sensor_type == V4L2_IDENT_OV7670)) {
		cam->sensors[SENSOR_LOW] = client;
		cam->bus_type[SENSOR_LOW] = BUS_PARALLEL;
		detected_low = 1;
		sensor_selected = SENSOR_LOW; /* Set default sensor */
		printk(KERN_NOTICE "camera: ov7660 attached\n");
	} else if (cam->sensor_type == V4L2_IDENT_OV5642) {
		cam->sensors[SENSOR_HIGH] = client;
		cam->bus_type[SENSOR_HIGH] = BUS_MIPI;
		detected_high = 1;
		sensor_selected = SENSOR_HIGH;	/* Set default sensor */
		printk(KERN_NOTICE "camera: ov5642 attached\n");
	} else if (cam->sensor_type == V4L2_IDENT_OV265X) {
		cam->sensors[SENSOR_LOW] = client;
		detected_low = 1;
		sensor_selected = SENSOR_LOW;	/* Set default sensor */
		cam->bus_type[SENSOR_LOW] = BUS_PARALLEL;
		printk(KERN_NOTICE "camera: ov265x attached\n");
	} else if (cam->sensor_type == V4L2_IDENT_GC308) {
		struct sensor_platform_data *pdata = client->dev.platform_data;
		cam->sensors[pdata->id] = client;
		sensor_selected = pdata->id;	/* Set default sensor */
		cam->bus_type[pdata->id] = BUS_PARALLEL;
		if (pdata->id == SENSOR_LOW)
			detected_low = 1;
		else
			detected_high = 1;
		printk(KERN_ERR "gc0308[%s] attached as %s sensor.\n",
			dev_name(&client->dev), pdata->id==SENSOR_LOW?"low":"high");		
	} else if (cam->sensor_type == V4L2_IDENT_GC2015) {
		struct sensor_platform_data *pdata = client->dev.platform_data;
		cam->sensors[pdata->id] = client;
		sensor_selected = pdata->id;	/* Set default sensor */
		cam->bus_type[pdata->id] = BUS_PARALLEL;
		if (pdata->id == SENSOR_LOW)
			detected_low = 1;
		else
			detected_high = 1;
		printk(KERN_ERR "gc2015[%s] attached as %s sensor.\n",
			dev_name(&client->dev), pdata->id==SENSOR_LOW?"low":"high");		
	} else if (cam->sensor_type == V4L2_IDENT_HM2055) {
		struct sensor_platform_data *pdata = client->dev.platform_data;
		cam->sensors[pdata->id] = client;
		sensor_selected = pdata->id;	/* Set default sensor */
		cam->bus_type[pdata->id] = BUS_PARALLEL;
		if (pdata->id == SENSOR_LOW)
			detected_low = 1;
		else
			detected_high = 1;
		printk(KERN_ERR "hm2055[%s] attached as %s sensor.\n",
			dev_name(&client->dev), pdata->id==SENSOR_LOW?"low":"high");		
	} else if (cam->sensor_type == V4L2_IDENT_HM2057) {
		struct sensor_platform_data *pdata = client->dev.platform_data;
		cam->sensors[pdata->id] = client;
		sensor_selected = pdata->id;	/* Set default sensor */
		cam->bus_type[pdata->id] = BUS_PARALLEL;
		if (pdata->id == SENSOR_LOW)
			detected_low = 1;
		else
			detected_high = 1;
		printk(KERN_ERR "hm2057[%s] attached as %s sensor.\n",
			dev_name(&client->dev), pdata->id==SENSOR_LOW?"low":"high");		
	} else if (cam->sensor_type == V4L2_IDENT_OV5640) {
		struct sensor_platform_data *pdata = client->dev.platform_data;
		cam->sensors[pdata->id] = client;
		sensor_selected = pdata->id;	/* Set default sensor */
		cam->bus_type[pdata->id] = BUS_PARALLEL;
		if (pdata->id == SENSOR_LOW)
			detected_low = 1;
		else
			detected_high = 1;
		printk(KERN_ERR "ov5640[%s] attached as %s sensor.\n",
			dev_name(&client->dev), pdata->id==SENSOR_LOW?"low":"high");		
	} else {
		cam_err(cam, "camera: unsupported sensor type %d at addr 0x%x", cam->sensor_type, cam->sensor->addr);
		ret = -EINVAL;
		goto out;
	}

	cam->ident[sensor_selected] = cam->sensor_type;

	/* Get/set parameters? */
	ret = 0;
	cam->state = S_IDLE;
out:
	//ccic_ctlr_power_down(cam); /* deleted to support dual camera. -guang */
	cam->sensor = NULL;
	mutex_unlock(&cam->s_mutex);
	return ret;
}
EXPORT_SYMBOL (ccic_sensor_attach);

int ccic_sensor_detach(struct i2c_client *client)
{
	struct ccic_camera *cam;

	cam = ccic_find_dev(0);
	if (cam == NULL)
		return -ENODEV;

	if (cam->sensor == client) {
		ccic_ctlr_stop_dma(cam);
		ccic_ctlr_power_down(cam);
		cam_err(cam, "camera: lost the sensor!\n");
		cam->sensor = NULL;  /* Bummer, no camera */
		cam->state = S_NOTREADY;
	}
	return 0;
}
EXPORT_SYMBOL (ccic_sensor_detach);

/* ------------------------------------------------------------------- */
/*
 * Deal with the controller.
 */

/*
 * Only handle in irq context
 */
static inline void ccic_switch_dma(struct ccic_camera *cam, unsigned int frame)
{
	struct ccic_sio_buffer *sbuf = NULL, *newsbuf = NULL;
	unsigned long flags = 0;
	dma_addr_t phy = 0;

	/*
	 * if the buffer is dequeued, try to fetch one buffer from avail list
	 * if the buffer is in avail list, move form avail list to full list,
	 * dequeue the buffer at the same time to protect
	 */
	spin_lock_irqsave(&cam->dev_lock, flags);
	phy = ccic_reg_read(cam, REG_Y0BAR + (frame<<2));
	/* rubbish got data */
	if (cam->rubbish_buf_phy == phy){
		/* the buffer is dequeued */
		if (list_empty(&cam->sb_avail))
			goto out_unlock;

		sbuf = list_entry(cam->sb_avail.next, struct ccic_sio_buffer, list);
		ccic_reg_write(cam, REG_Y0BAR + (frame<<2), sbuf->yuv_p.y);
		ccic_reg_write(cam, REG_U0BAR + (frame<<2), sbuf->yuv_p.u);
		ccic_reg_write(cam, REG_V0BAR + (frame<<2), sbuf->yuv_p.v);
		list_move_tail(&sbuf->list, &cam->sb_dma);
	} else {  // got the real data
		list_for_each_entry(sbuf, &cam->sb_dma, list) {
			if (phy == sbuf->dma_handles) { // find the match sbuf
				/* drop current JPEG frame if get wrong header */
				if (cam->pix_format.pixelformat == V4L2_PIX_FMT_JPEG) {
					dma_map_page(&cam->pdev->dev, sbuf->page, 0, sbuf->v4lbuf.length, DMA_FROM_DEVICE);
					if ((sbuf->buffer[0] != 0xff) || (sbuf->buffer[1] != 0xd8)) {
						printk("camera: JPEG Header ERROR !!! will dropped the frame %d!!!\n", frame);
						goto out_unlock;
					}
				}

				if (list_empty(&cam->sb_avail)) {  // no more dma buffer
					printk(KERN_DEBUG "camera: CCIC link to rubbish buffer\n");
					ccic_reg_write(cam, REG_Y0BAR + (frame<<2), cam->rubbish_buf_phy);
					ccic_reg_write(cam, REG_U0BAR + (frame<<2), cam->rubbish_buf_phy);
					ccic_reg_write(cam, REG_V0BAR + (frame<<2), cam->rubbish_buf_phy);
				} else {
					newsbuf = list_entry(cam->sb_avail.next, struct ccic_sio_buffer, list);
					ccic_reg_write(cam, REG_Y0BAR + (frame<<2), newsbuf->yuv_p.y);
					ccic_reg_write(cam, REG_U0BAR + (frame<<2), newsbuf->yuv_p.u);
					ccic_reg_write(cam, REG_V0BAR + (frame<<2), newsbuf->yuv_p.v);
					list_move_tail(&newsbuf->list, &cam->sb_dma);
				}

				sbuf->v4lbuf.bytesused = cam->pix_format.sizeimage;
				sbuf->v4lbuf.flags &= ~V4L2_BUF_FLAG_QUEUED;
				sbuf->v4lbuf.flags |= V4L2_BUF_FLAG_DONE;

				cam->next_buf = frame;
				list_move_tail(&sbuf->list, &cam->sb_full);
				if (list_empty(&cam->sb_full))
					printk("camera: user space owns buffer with data is empty!!!\n");
				else
					wake_up(&cam->iowait);
				goto out_unlock;
			}
		}
	}
out_unlock:
	spin_unlock_irqrestore(&cam->dev_lock, flags);
	return;
}

/*
 * Do everything we think we need to have the interface operating
 * according to the desired format.
 */
static int ccic_ctlr_dma(struct ccic_camera *cam)
{
	int frame = 0;
	struct ccic_sio_buffer *sbuf = NULL;

	if (cam->nbufs < 2) {
		printk(KERN_ERR "camera: ccic at least 2 dma buffers\n");
    		return -ENOMEM;
        }

	for (frame = 0; frame < cam->nbufs; frame++) {
		/* suppose only 3 buffers queued */
		if(frame == MAX_DMA_BUFS)
			break;

		sbuf = cam->sb_bufs + frame;
		ccic_reg_write(cam, REG_Y0BAR + (frame<<2), sbuf->yuv_p.y);
		ccic_reg_write(cam, REG_U0BAR + (frame<<2), sbuf->yuv_p.u);
		ccic_reg_write(cam, REG_V0BAR + (frame<<2), sbuf->yuv_p.v);
		list_move_tail(&sbuf->list, &cam->sb_dma);
	}

	/* use fixed 2 frames for stability */
	ccic_reg_set_bit(cam, REG_CTRL1, C1_TWOBUFS);

	return 0;
}

static void ccic_ctlr_image(struct ccic_camera *cam)
{
	int imgsz;
	struct v4l2_pix_format *fmt = &cam->pix_format;
	int widthy = 0, widthuv = 0;

	if (fmt->pixelformat == V4L2_PIX_FMT_YUV420)
		imgsz = ((fmt->height << IMGSZ_V_SHIFT) & IMGSZ_V_MASK) | (((fmt->bytesperline)*4/3) & IMGSZ_H_MASK);
	else if (fmt->pixelformat == V4L2_PIX_FMT_JPEG)
		imgsz = (((fmt->sizeimage/fmt->bytesperline)<< IMGSZ_V_SHIFT) & IMGSZ_V_MASK) | (fmt->bytesperline & IMGSZ_H_MASK);
	else
		imgsz = ((fmt->height << IMGSZ_V_SHIFT) & IMGSZ_V_MASK) | (fmt->bytesperline & IMGSZ_H_MASK);

	/* YPITCH just drops the last two bits */
	switch (fmt->pixelformat) {
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUYV:
			widthy = fmt->width*2;
			widthuv = fmt->width*2;
			break;
		case V4L2_PIX_FMT_RGB565:
			widthy = fmt->width*2;
			widthuv = 0;
			break;
		case V4L2_PIX_FMT_JPEG:
			widthy = fmt->bytesperline;
			widthuv = fmt->bytesperline;
			break;
		case V4L2_PIX_FMT_YUV422P:
			widthy = fmt->width;
			widthuv = fmt->width/2;
			break;
		case V4L2_PIX_FMT_YUV420:
			widthy = fmt->width;
			widthuv = fmt->width/2;
			break;
		default:
			break;
	}

	ccic_reg_write(cam, REG_IMGPITCH, widthuv << 16 | widthy);
	ccic_reg_write(cam, REG_IMGSIZE, imgsz);
	ccic_reg_write(cam, REG_IMGOFFSET, 0x0);

	/*
	 * Tell the controller about the image format we are using.
	 */
	switch (cam->pix_format.pixelformat) {
		case V4L2_PIX_FMT_YUV422P:
			ccic_reg_write_mask(cam, REG_CTRL0,
				C0_DF_YUV|C0_YUV_PLANAR|C0_YUVE_YVYU,	/*the endianness of sensor output is UYVY(Y1CrY0Cb)*/
				C0_DF_MASK);
			break;
		case V4L2_PIX_FMT_YUV420:
			ccic_reg_write_mask(cam, REG_CTRL0,
				C0_DF_YUV|C0_YUV_420PL|C0_YUVE_YVYU,	/*the endianness of sensor output is UYVY(Y1CrY0Cb)*/
				C0_DF_MASK);
			break;
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUYV:
			ccic_reg_write_mask(cam, REG_CTRL0,
				C0_DF_YUV|C0_YUV_PACKED|C0_YUVE_YUYV,	/*the endianness of sensor output is UYVY(Y1CrY0Cb)*/
				C0_DF_MASK);
			break;
		case V4L2_PIX_FMT_JPEG:
			if (BUS_IS_MIPI(cam->bus_type[sensor_selected])) {
				ccic_reg_write_mask(cam, REG_CTRL0,
					C0_DF_RGB|C0_RGB_BGR|C0_RGB4_BGRX,	/* Set CTRL0 as 0x10a8 for JPEG */
					C0_DF_MASK);
			} else {
				ccic_reg_write_mask(cam, REG_CTRL0,
					C0_DF_YUV|C0_YUV_PACKED|C0_YUVE_YUYV,
					C0_DF_MASK);
			}
			break;
		case V4L2_PIX_FMT_RGB444:
			ccic_reg_write_mask(cam, REG_CTRL0,
				C0_DF_RGB|C0_RGBF_444|C0_RGB4_XRGB,
				C0_DF_MASK);
			/* Alpha value? */
			break;
		case V4L2_PIX_FMT_RGB565:
			ccic_reg_write_mask(cam, REG_CTRL0,
				C0_DF_RGB|C0_RGBF_565|C0_RGB5_BGGR,
				C0_DF_MASK);
			break;
		default:
			cam_err(cam, "camera: unknown format %x\n", cam->pix_format.pixelformat);
			break;
	}
	/*
	 * Make sure it knows we want to use hsync/vsync.
	 */
	ccic_reg_write_mask(cam, REG_CTRL0, C0_SIF_HVSYNC, C0_SIFM_MASK);
}

/*
 * Configure the controller for operation; caller holds the
 * device mutex.
 */
static int ccic_ctlr_configure(struct ccic_camera *cam)
{
	unsigned long flags = 0;
	int ret = 0;

	spin_lock_irqsave(&cam->dev_lock, flags);
	ret = ccic_ctlr_dma(cam);
	ccic_ctlr_image(cam);
	spin_unlock_irqrestore(&cam->dev_lock, flags);
	return ret;
}

static void ccic_ctlr_irq_enable(struct ccic_camera *cam)
{
	/*
	 * Clear any pending interrupts, since we do not
	 * expect to have I/O active prior to enabling.
	 */
	ccic_reg_write(cam, REG_IRQSTAT, FRAMEIRQS);
	ccic_reg_set_bit(cam, REG_IRQMASK, FRAMEIRQS);
}

static void ccic_ctlr_irq_disable(struct ccic_camera *cam)
{
	ccic_reg_clear_bit(cam, REG_IRQMASK, FRAMEIRQS);
}

/*
 * Make the controller start grabbing images.  Everything must
 * be set up before doing this.
 */
static void ccic_ctlr_start(struct ccic_camera *cam)
{
	ccic_reg_set_bit(cam, REG_CTRL0, C0_ENABLE);
}

static void ccic_ctlr_stop(struct ccic_camera *cam)
{
	ccic_reg_clear_bit(cam, REG_CTRL0, C0_ENABLE);
}

void ccic_ctlr_init(struct ccic_camera *cam)
{
	/*
	 * Make sure it's not powered down.
	 */
	ccic_reg_clear_bit(cam, REG_CTRL1, C1_PWRDWN);
	/*
	 * Turn off the enable bit.  It sure should be off anyway,
	 * but it's good to be sure.
	 */
	ccic_reg_clear_bit(cam, REG_CTRL0, C0_ENABLE);
	/*
	 * Mask all interrupts.
	 */
	ccic_reg_write(cam, REG_IRQMASK, 0);
}

/*
 * Stop the controller, and don't return until we're really sure that no
 * further DMA is going on.
 */
static void ccic_ctlr_stop_dma(struct ccic_camera *cam)
{
	/* CSI2/DPHY need be cleared, or no EOF will be received */
	ccic_reg_write(cam, REG_CSI2_DPHY3, 0x0);
	ccic_reg_write(cam, REG_CSI2_DPHY6, 0x0);
	ccic_reg_write(cam, REG_CSI2_DPHY5, 0x0);
	ccic_reg_write(cam, REG_CSI2_CTRL0, 0x0);

	cam->state = S_IDLE;
	ccic_ctlr_irq_disable(cam);
}

/*
 * Power up and down.
 */
void ccic_ctlr_power_up(struct ccic_camera *cam)
{
	/*
	 * Part one of the sensor dance: turn the global
	 * GPIO signal on.
	 */
	ccic_reg_clear_bit(cam, REG_CTRL1, C1_PWRDWN);
}

static void ccic_ctlr_power_down(struct ccic_camera *cam)
{
	ccic_reg_set_bit(cam, REG_CTRL1, C1_PWRDWN);
}

void ccic_mclk_set(int on)
{
	struct ccic_camera* cam = NULL;

	cam = ccic_find_dev(0);
	if (!cam) return;

	if (on)
	{
		ccic_ctlr_power_up(cam);
		ccic_enable_mclk(cam); 
	}
	else
	{
		ccic_ctlr_power_down(cam);
		ccic_disable_mclk(); 
	}
}
EXPORT_SYMBOL(ccic_mclk_set);

/* -------------------------------------------------------------------- */
/*
 * Communications with the sensor.
 */
static int __ccic_cam_cmd(struct ccic_camera *cam, int cmd, void *arg)
{
	struct i2c_client *sc = cam->sensor;
	int ret;

	if (sc == NULL || sc->driver == NULL || sc->driver->command == NULL)
		return -EINVAL;
	ret = sc->driver->command(sc, cmd, arg);
	if (ret == -EPERM) /* Unsupported command */
		return 0;
	return ret;
}

static int __ccic_cam_reset(struct ccic_camera *cam)
{
	return __ccic_cam_cmd(cam, VIDIOC_INT_RESET, NULL);
}

static int ccic_cam_configure(struct ccic_camera *cam)
{
	struct v4l2_format fmt;
	int ret = 0;

	if (cam->state != S_IDLE)
		return -EINVAL;
	fmt.fmt.pix = cam->pix_format;
	ret = __ccic_cam_cmd(cam, VIDIOC_S_FMT, &fmt);

	return ret;
}

/* -------------------------------------------------------------------- */
/*
 * DMA buffer management.  These functions need s_mutex held.
 */

/* FIXME: this is inefficient as hell, since dma_alloc_coherent just
 * does a get_free_pages() call, and we waste a good chunk of an orderN
 * allocation.  Should try to allocate the whole set in one chunk.
 */
static int ccic_alloc_dma_bufs(struct ccic_camera *cam, int loadtime)
{
	int i;

	if (loadtime)
		cam->dma_buf_size = dma_buf_size;
	else
		cam->dma_buf_size = cam->pix_format.sizeimage;
	cam->dma_buf_size = PAGE_ALIGN(cam->dma_buf_size);

	if (n_dma_bufs > MAX_DMA_BUFS)
		n_dma_bufs = MAX_DMA_BUFS;

	cam->n_mapbufs = 0;
	for (i = 0; i < n_dma_bufs; i++) {
#ifdef DMA_POOL
		cam->dma_bufs[i] = dma_alloc_coherent(&cam->pdev->dev,
				cam->dma_buf_size, cam->dma_handles + i,
				GFP_KERNEL);
#else
		cam->order[i] = get_order(cam->dma_buf_size);
		cam->pages[i] = alloc_pages(GFP_KERNEL, cam->order[i]);
		if (cam->pages[i] == NULL){
			printk(KERN_ERR "camera: allocate dma_bufs failed\n");
			return -ENOMEM;
		}

		cam->dma_bufs[i] = (void *)page_address(cam->pages[i]);
		cam->dma_handles[i] = __pa(cam->dma_bufs[i]);
#endif
		if (cam->dma_bufs[i] == NULL) {
			cam_warn(cam, "camera: failed to allocate DMA buffer\n");
			break;
		}
		/* For debug, remove eventually */
		memset(cam->dma_bufs[i], 0xcc, cam->dma_buf_size);
		cam->n_mapbufs++;
	}

	switch (cam->n_mapbufs) {
		case 1:
#ifdef DMA_POOL
			dma_free_coherent(&cam->pdev->dev, cam->dma_buf_size, cam->dma_bufs[0], cam->dma_handles[0]);
#else
			free_pages((unsigned long)cam->dma_bufs[0], cam->order[0]);
#endif
			cam->n_mapbufs = 0;
		case 0:
			cam_err(cam, "camera: insufficient DMA buffers, cannot operate\n");
			return -ENOMEM;
		case 2:
			if (n_dma_bufs > 2)
				cam_warn(cam, "camera: will limp along with only 2 buffers\n");
			break;
		default:
			break;
	}

	return 0;
}

static void ccic_free_dma_bufs(struct ccic_camera *cam)
{
	int i;
	for (i = 0; i < cam->n_mapbufs; i++) {
#ifdef DMA_POOL
		dma_free_coherent(&cam->pdev->dev, cam->dma_buf_size, cam->dma_bufs[i], cam->dma_handles[i]);
		cam->dma_bufs[i] = NULL;
#else
		if(cam->dma_bufs[i]){
			free_pages((unsigned long)cam->dma_bufs[i], cam->order[i]);
			cam->dma_bufs[i] = NULL;
		}
#endif
	}
	cam->n_mapbufs = 0;
}

/*
 * Get everything ready, and start grabbing frames.
 */
static int ccic_read_setup(struct ccic_camera *cam, enum ccic_state state)
{
	int ret;

	/*
	 * Configuration.  If we still don't have DMA buffers,
	 * make one last, desperate attempt.
	 */
	if(cam->io_type == V4L2_MEMORY_MMAP)
		cam->nbufs = cam->n_mapbufs;

	if (cam->nbufs == 0) {
		if (ccic_alloc_dma_bufs(cam, 0))
			return -ENOMEM;
		cam->nbufs = cam->n_mapbufs;
	}

	ret = ccic_cam_configure(cam);
	if (ret)
		return ret;

	ret = ccic_ctlr_configure(cam);
	if (ret)
		return ret;

	/*
	 * Turn it loose.
	 */
	cam->next_buf = -1;
	ccic_ctlr_irq_enable(cam);
	cam->state = state;

	if (BUS_IS_MIPI(cam->bus_type[sensor_selected])) {
		if (cam->sensor_type == V4L2_IDENT_OV9640)
			ccic_reg_write(cam, REG_CSI2_DPHY3, 0x0a06);	/* upgrade from .32, still need confirm it */
		else if (cam->sensor_type == V4L2_IDENT_OV5642)
			ccic_reg_write(cam, REG_CSI2_DPHY3, 0x1b0b);
		ccic_reg_write(cam, REG_CSI2_DPHY6, 0x1a03);
#ifdef ENABLE_TWO_LANES
		ccic_reg_write(cam, REG_CSI2_DPHY5, 0x33);	/* 0x33 for 2 lanes. 0x11 for 1 lane */
		ccic_reg_write(cam, REG_CSI2_CTRL0, 0x43);	/* 0x43 for 2 lanes, 0x41 for 1 lane. */
#else
		ccic_reg_write(cam, REG_CSI2_DPHY5, 0x11);	/* 0x33 for 2 lanes. 0x11 for 1 lane */
		ccic_reg_write(cam, REG_CSI2_CTRL0, 0x41);	/* 0x43 for 2 lanes, 0x41 for 1 lane. */
#endif
	} else {
		ccic_reg_write(cam, REG_CSI2_DPHY3, 0x0);
		ccic_reg_write(cam, REG_CSI2_DPHY6, 0x0);
		ccic_reg_write(cam, REG_CSI2_DPHY5, 0x0);
		ccic_reg_write(cam, REG_CSI2_CTRL0, 0x0);
	}
#ifdef DUMP_REG_DEBUG
	dump_register(cam);
#endif
	ccic_ctlr_start(cam);
#ifdef DUMP_REG_DEBUG
	dump_register(cam);
#endif
	ret = __ccic_cam_cmd(cam, VIDIOC_STREAMON, NULL);

	return ret;
}

/*
 * Streaming I/O support.
 */
static int ccic_vidioc_streamon(struct file *filp, void *priv, enum v4l2_buf_type type)
{
	struct ccic_camera *cam = filp->private_data;
	int ret = -EINVAL;

	mutex_lock(&cam->s_mutex);
	if (type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
		goto out_unlock;

	if (cam->state != S_IDLE || cam->n_sbufs == 0)
		goto out_unlock;

	ret = ccic_read_setup(cam, S_STREAMING);
out_unlock:
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_streamoff(struct file *filp, void *priv, enum v4l2_buf_type type)
{
	struct ccic_camera *cam = filp->private_data;
	int ret = -EINVAL;

	mutex_lock(&cam->s_mutex);
	if (type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
		goto out_unlock;

	if (cam->state != S_STREAMING)
		goto out_unlock;

	ccic_stop_at_buf(cam);
	ccic_ctlr_stop_dma(cam);
	__ccic_cam_cmd(cam, VIDIOC_STREAMOFF, NULL);
	ret = 0;
out_unlock:
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_setup_siobuf(struct ccic_camera *cam, int index)
{
	struct ccic_sio_buffer *buf = cam->sb_bufs + index;
	struct v4l2_pix_format *fmt = &cam->pix_format;

	INIT_LIST_HEAD(&buf->list);
	buf->v4lbuf.length = PAGE_ALIGN(cam->pix_format.sizeimage);
	buf->dma_handles = cam->dma_handles[index];
	buf->page = cam->pages[index];
	buf->buffer = cam->dma_bufs[index];
	if (buf->buffer == NULL)
		return -ENOMEM;

	buf->mapcount = 0;
	buf->cam = cam;

	buf->v4lbuf.index = index;
	buf->v4lbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf->v4lbuf.field = V4L2_FIELD_NONE;
	buf->v4lbuf.memory = V4L2_MEMORY_MMAP;

	/*
	 * Offset: must be 32-bit even on a 64-bit system.  videobuf-dma-sg
	 * just uses the length times the index, but the spec warns
	 * against doing just that - vma merging problems.  So we
	 * leave a gap between each pair of buffers.
	 */
	buf->v4lbuf.m.offset = 2*index*buf->v4lbuf.length;

	if (fmt->pixelformat == V4L2_PIX_FMT_YUV422P) {
		buf->yuv_p.y = buf->dma_handles;
		buf->yuv_p.u = buf->yuv_p.y + fmt->width*fmt->height;
		buf->yuv_p.v = buf->yuv_p.u + fmt->width*fmt->height/2;
	} else if (fmt->pixelformat == V4L2_PIX_FMT_YUV420){
		buf->yuv_p.y = buf->dma_handles;
		buf->yuv_p.u = buf->yuv_p.y + fmt->width*fmt->height;
		buf->yuv_p.v = buf->yuv_p.u + fmt->width*fmt->height/4;
	} else {
		buf->yuv_p.y = buf->dma_handles;
		buf->yuv_p.u = cam->rubbish_buf_phy;
		buf->yuv_p.v = cam->rubbish_buf_phy;
	}

	return 0;
}

extern struct page* va_to_page(unsigned long user_addr);
extern unsigned long va_to_pa(unsigned long user_addr, unsigned int size);

static int ccic_prepare_buffer_node(struct ccic_camera *cam,
	struct ccic_sio_buffer *buf, unsigned long userptr,
                unsigned int size, unsigned int index)
{
	unsigned int vaddr = PAGE_ALIGN(userptr);
	struct v4l2_pix_format *fmt = &cam->pix_format;
	unsigned int high_mem = 0;

	if (vaddr != userptr) {
		printk(KERN_ERR "camera: the memory is not page align\n");
		BUG_ON(1);
		return -EPERM;
	}
	if (0 != size % 32) {
		printk(KERN_ERR "camera: the memory size 0x%08x is not 32 bytes align\n", size);
		BUG_ON(1);
		return -EPERM;
	}

	buf->dma_handles = va_to_pa(vaddr, size);
	if (!buf->dma_handles) {
		printk(KERN_ERR "camera: memory is not contiguous\n");
		BUG_ON(1);
		return -ENOMEM;
	}

	buf->page = va_to_page(vaddr);
	if (!buf->page) {
		printk(KERN_ERR "camera: fail to get page info\n");
		BUG_ON(1);
		return -EFAULT;
	}

	if (PageHighMem(buf->page)) {
		high_mem = 1;
		printk(KERN_NOTICE "camera: used HIGHMEM in this platform\n");
	}

	if (0 == high_mem)
		buf->buffer = page_address(buf->page);
	else
		buf->buffer = kmap(buf->page);

	if (!buf->buffer) {
		printk(KERN_ERR "camera: fail to get buffer info\n");
		BUG_ON(1);
		return -EFAULT;
	}

	INIT_LIST_HEAD(&buf->list);
	buf->v4lbuf.length = size;
	buf->mapcount = 0;

	buf->v4lbuf.index = index;
	buf->v4lbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf->v4lbuf.field = V4L2_FIELD_NONE;
	buf->v4lbuf.memory = V4L2_MEMORY_USERPTR;
	buf->v4lbuf.m.offset = 2*index*buf->v4lbuf.length;

	if (fmt->pixelformat == V4L2_PIX_FMT_YUV422P) {
		buf->yuv_p.y = buf->dma_handles;
		buf->yuv_p.u = buf->yuv_p.y + fmt->width*fmt->height;
		buf->yuv_p.v = buf->yuv_p.u + fmt->width*fmt->height/2;
	} else if (fmt->pixelformat == V4L2_PIX_FMT_YUV420){
		buf->yuv_p.y = buf->dma_handles;
		buf->yuv_p.u = buf->yuv_p.y + fmt->width*fmt->height;
		buf->yuv_p.v = buf->yuv_p.u + fmt->width*fmt->height/4;
	} else {
		buf->yuv_p.y = buf->dma_handles;
		buf->yuv_p.u = cam->rubbish_buf_phy;
		buf->yuv_p.v = cam->rubbish_buf_phy;
	}

	return 0;
}

static void ccic_free_buffer_node(struct ccic_sio_buffer *sbuf)
{
	/*
	 * vunmap will do TLB flush for us.
	 * We map uncachable memory, so needn't cache invalid operation here.
	 */
	if(V4L2_MEMORY_USERPTR != sbuf->v4lbuf.memory)
		return;
}

static int ccic_free_sio_buffers(struct ccic_camera *cam)
{
	int i;

	/*
	 * If any buffers are mapped, we cannot free them at all.
	 */
	for (i = 0; i < cam->n_sbufs; i++){
		if (cam->sb_bufs[i].mapcount > 0)
			return -EBUSY;
		ccic_free_buffer_node(&cam->sb_bufs[i]);
	}

	/*
	 * OK, let's do it.
	 */
	cam->n_sbufs = 0;
	cam->nbufs = 0;
	if (cam->sb_bufs) {
		kfree(cam->sb_bufs);
		cam->sb_bufs = NULL;
	}
	INIT_LIST_HEAD(&cam->sb_avail);
	INIT_LIST_HEAD(&cam->sb_full);
	INIT_LIST_HEAD(&cam->sb_dma);
	return 0;
}

static int ccic_vidioc_reqbufs(struct file *filp, void *priv, struct v4l2_requestbuffers *req)
{
	struct ccic_camera *cam = filp->private_data;
	int ret = 0;  /* Silence warning */

	/*
	 * Make sure it's something we can do.  User pointers could be
	 * implemented without great pain, but that's not been done yet.
	 */
	mutex_lock(&cam->s_mutex);
	if (req->type != V4L2_BUF_TYPE_VIDEO_CAPTURE) {
		printk(KERN_ERR "camera: invalid type, only support VIDEO_CAPTURE!\n");
		ret = -EINVAL;
		goto out_unlock;
	}

	if (req->memory == V4L2_MEMORY_USERPTR) {
		if (cam->state != S_IDLE || (cam->owner && cam->owner != filp)) {
			ret = -EBUSY;
			goto out_unlock;
		}
		cam->io_type = V4L2_MEMORY_USERPTR;
		cam->owner = filp;
		/* we do not need kernel to alloc the DAM buffer */
		ccic_free_dma_bufs(cam);
		ret = ccic_free_sio_buffers (cam);
		goto out_unlock;
	}

	if (req->memory != V4L2_MEMORY_MMAP) {
		printk(KERN_ERR "camera: invalid memory type, only support MMAP or USERPTR!\n");
		ret = -EINVAL;
		goto out_unlock;
	}

	/*
	 * If they ask for zero buffers, they really want us to stop streaming
	 * (if it's happening) and free everything.  Should we check owner?
	 */
	if (req->count == 0) {
		if (cam->state == S_STREAMING)
			ccic_ctlr_stop_dma(cam);
		ret = ccic_free_sio_buffers (cam);
		goto out_unlock;
	}

	/*
	 * Device needs to be idle and working.  We *could* try to do the
	 * right thing in S_SPECREAD by shutting things down, but it
	 * probably doesn't matter.
	 */
	if (cam->state != S_IDLE || (cam->owner && cam->owner != filp)) {
		ret = -EBUSY;
		goto out_unlock;
	}
	cam->owner = filp;
	cam->io_type = V4L2_MEMORY_MMAP;

	if (req->count < MIN_DMA_BUFS)
		req->count = MIN_DMA_BUFS;
	else if (req->count > MAX_DMA_BUFS)
		req->count = MAX_DMA_BUFS;

	if (cam->n_sbufs > 0) {
		ret = ccic_free_sio_buffers(cam);
		if (ret)
			goto out_unlock;
	}

	cam->sb_bufs = kzalloc(req->count*sizeof(struct ccic_sio_buffer), GFP_KERNEL);
	if (cam->sb_bufs == NULL) {
		ret = -ENOMEM;
		goto out_unlock;
	}

	for (cam->n_sbufs = 0; cam->n_sbufs < req->count; (cam->n_sbufs)++) {
		ret = ccic_setup_siobuf(cam, cam->n_sbufs);
		if (ret)
			break;
	}

	if (cam->n_sbufs == 0)	/* no luck at all - ret already set */
		kfree(cam->sb_bufs);

	req->count = cam->n_sbufs;  /* In case of partial success */

out_unlock:
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_querybuf(struct file *filp, void *priv, struct v4l2_buffer *buf)
{
	struct ccic_camera *cam = filp->private_data;
	int ret = -EINVAL;

	mutex_lock(&cam->s_mutex);
	if (buf->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
		goto out_unlock;

	if (buf->index < 0 || buf->index >= cam->n_sbufs)
		goto out_unlock;

	*buf = cam->sb_bufs[buf->index].v4lbuf;
	ret = 0;
out_unlock:
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_qbuf(struct file *filp, void *priv, struct v4l2_buffer *buf)
{
	struct ccic_camera *cam = filp->private_data;
	struct ccic_sio_buffer *sbuf = NULL;
	unsigned long flags = 0;
	int ret = -EINVAL;

	mutex_lock(&cam->s_mutex);
	if (buf->type != V4L2_BUF_TYPE_VIDEO_CAPTURE) {
		printk(KERN_ERR "camera: buf type != V4L2_BUF_TYPE_VIDEO_CAPTURE\n");
		ret = -EINVAL;
		goto out_unlock;
	}

	if(NULL == cam->sb_bufs) {
		cam->sb_bufs = kzalloc(max_buffers*sizeof(struct ccic_sio_buffer), GFP_KERNEL);
		if (cam->sb_bufs == NULL) {
			printk(KERN_ERR "camera: request buf fail\n");
			ret = -ENOMEM;
			goto out_unlock;
		}
	}
	sbuf = cam->sb_bufs + buf->index;

	if (sbuf->v4lbuf.flags & V4L2_BUF_FLAG_QUEUED) {
		/* Already queued?? */
		printk(KERN_ERR "camera: already queued buf\n");
		ret = 0;
		goto out_unlock;
	}

	if (sbuf->v4lbuf.flags & V4L2_BUF_FLAG_DONE) {
		/* Spec doesn't say anything */
		printk(KERN_ERR "camera: queued buf already done\n");
		ret = -EBUSY;
		goto out_unlock;
	}

	if ((buf->memory == V4L2_MEMORY_USERPTR) && (buf->index == cam->n_sbufs)) {
		if (buf->length < cam->pix_format.sizeimage) {
			printk(KERN_ERR "camera: prepare buffer, size is not enough; buf->length = %d, cam->pix_format.sizeimage = %d\n",
				buf->length, cam->pix_format.sizeimage);
			goto out_unlock;
		}

		if (buf->index > max_buffers) {
			printk(KERN_ERR "camera: only %d buffers are supported\n", max_buffers);
			goto out_unlock;
		}

		if (ccic_prepare_buffer_node(cam, sbuf, buf->m.userptr, buf->length, buf->index)) {
			printk(KERN_ERR "camera: ccic prepare buffer node failed\n");
			ret = -EINVAL;
			goto out_unlock;
		}
		cam->nbufs++;
		cam->n_sbufs++;
	} else {
		if (buf->index < 0 || buf->index >= cam->n_sbufs)
			goto out_unlock;
	}

	sbuf->v4lbuf.flags |= V4L2_BUF_FLAG_QUEUED;
	spin_lock_irqsave(&cam->dev_lock, flags);
	list_add_tail(&sbuf->list, &cam->sb_avail);
	spin_unlock_irqrestore(&cam->dev_lock, flags);
	ret = 0;
out_unlock:
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_dqbuf(struct file *filp, void *priv, struct v4l2_buffer *buf)
{
	struct ccic_camera *cam = filp->private_data;
	struct ccic_sio_buffer *sbuf = NULL;
	unsigned long flags = 0;
	int ret = -EINVAL;

	mutex_lock(&cam->s_mutex);
	if (buf->type != V4L2_BUF_TYPE_VIDEO_CAPTURE) {
		printk(KERN_ERR "camera: type != V4L2_BUF_TYPE_VIDEO_CAPTURE\n");
		goto out_unlock;
	}

	if (cam->state != S_STREAMING) {
		printk(KERN_ERR "camera: state != S_STREAMING\n");
		goto out_unlock;
	}

	if (list_empty(&cam->sb_full) && (filp->f_flags & O_NONBLOCK)) {
		printk(KERN_ERR "camera: sio buf list empty in NONBLOCK mode\n");
		ret = -EAGAIN;
		goto out_unlock;
	}

	while (list_empty(&cam->sb_full) && cam->state == S_STREAMING) {
		mutex_unlock(&cam->s_mutex);
		if (wait_event_interruptible(cam->iowait, !list_empty(&cam->sb_full))) {
			printk(KERN_ERR "camera: sio buf list empty when cam->state = STREAMING\n");
			ret = -ERESTARTSYS;
			goto out;
		}
		mutex_lock(&cam->s_mutex);
	}

	if (cam->state != S_STREAMING) {
		printk(KERN_ERR "camera: cam->state is not STREAMING\n");
		ret = -EINTR;
	} else {
		spin_lock_irqsave(&cam->dev_lock, flags);
		/* Should probably recheck !list_empty() here */
		sbuf = list_entry(cam->sb_full.next, struct ccic_sio_buffer, list);
		list_del_init(&sbuf->list);
		dma_map_page(&cam->pdev->dev, sbuf->page, 0, sbuf->v4lbuf.length, DMA_FROM_DEVICE);
		spin_unlock_irqrestore(&cam->dev_lock, flags);
		sbuf->v4lbuf.flags &= ~V4L2_BUF_FLAG_DONE;
		*buf = sbuf->v4lbuf;
		ret = 0;
	}

out_unlock:
	mutex_unlock(&cam->s_mutex);
out:
	return ret;
}

static void ccic_v4l_vm_open(struct vm_area_struct *vma)
{
	struct ccic_sio_buffer *sbuf = vma->vm_private_data;

	/*
	 * Locking: done under mmap_sem, so we don't need to
	 * go back to the camera lock here.
	 */
	sbuf->mapcount++;

	/* FIXME:
	 * Workaround only. The svma now could only be set
	 * by the first process opens the driver.
	 */
	if (!sbuf->svma)
		sbuf->svma = vma;
}

static void ccic_v4l_vm_close(struct vm_area_struct *vma)
{
	struct ccic_sio_buffer *sbuf = vma->vm_private_data;

	mutex_lock(&sbuf->cam->s_mutex);
	sbuf->mapcount--;
	/* Docs say we should stop I/O too... */
	if (sbuf->mapcount == 0) {
		sbuf->v4lbuf.flags &= ~V4L2_BUF_FLAG_MAPPED;
		sbuf->svma = 0;
	}
	mutex_unlock(&sbuf->cam->s_mutex);
}

static struct vm_operations_struct ccic_v4l_vm_ops = {
	.open = ccic_v4l_vm_open,
	.close = ccic_v4l_vm_close
};

static int ccic_v4l_mmap(struct file *filp, struct vm_area_struct *vma)
{
	struct ccic_camera *cam = filp->private_data;
	unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;
	int ret = -EINVAL;
	int i;
	struct ccic_sio_buffer *sbuf = NULL;

	mutex_lock(&cam->s_mutex);
	for (i = 0; i < cam->n_sbufs; i++) {
		if (cam->sb_bufs[i].v4lbuf.m.offset == offset) {
			sbuf = cam->sb_bufs + i;
			break;
		}
	}

	if (sbuf == NULL)
		goto out_unlock;

	ret =  remap_pfn_range(vma, vma->vm_start,
		sbuf->dma_handles >> PAGE_SHIFT,
		(vma->vm_end - vma->vm_start),
		vma->vm_page_prot);
	if (ret)
		goto out_unlock;

	vma->vm_flags |= VM_RESERVED;   /* Don't swap */
	vma->vm_flags |= VM_DONTEXPAND; /* Don't remap */

	vma->vm_private_data = sbuf;
	vma->vm_ops = &ccic_v4l_vm_ops;
	sbuf->v4lbuf.flags |= V4L2_BUF_FLAG_MAPPED;

	ccic_v4l_vm_open(vma);
	ret = 0;
out_unlock:
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_v4l_open(struct file *filp)
{
	struct video_device *vd = video_devdata(filp);
	struct ccic_camera *cam = container_of(vd, struct ccic_camera, v4ldev);
	int ret = 0;

	if (cam == NULL)
		return -ENODEV;

	mutex_lock(&cam->s_mutex);
	/* sensor_selected should be 0 or 1 */
	if (SENSOR_NONE == sensor_selected){
		printk(KERN_NOTICE "camera: no sensor detected!\n");
		ret = -ENODEV;
		goto out_unlock;
	}

	set_dvfm_constraint();
	filp->private_data = cam;
	if (cam->platform_ops->pmua_set)
		cam->platform_ops->pmua_set(1);

	if (cam->users == 0)
		ccic_ctlr_power_up(cam);

	cam->sensor = cam->sensors[sensor_selected];

	if (cam->users == 0) {
		if (cam->sensor) {
			ccic_enable_mclk(cam);
			((struct sensor_platform_data *)cam->sensor->dev.platform_data)->power_set(1, sensor_selected, ((struct sensor_platform_data *)cam->sensor->dev.platform_data)->eco_flag, ((struct sensor_platform_data *)cam->sensor->dev.platform_data)->sensor_flag);
			mdelay(30);
		} else {
			printk(KERN_ERR "camera: weird, could not find default sensor\n");
			ret = -ENODEV;
			goto out_unlock;
		}
	}
	cam->users++;
out_unlock:
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_v4l_release(struct file *filp)
{
	struct ccic_camera *cam = filp->private_data;
	struct sensor_platform_data *pdata;
	pdata = cam->sensor->dev.platform_data;

	mutex_lock(&cam->s_mutex);
	cam->users--;
	if (filp == cam->owner) {
		if (cam->state != S_IDLE) {
			ccic_stop_at_buf(cam);
			ccic_ctlr_stop_dma(cam);
			__ccic_cam_cmd(cam, VIDIOC_STREAMOFF, NULL);
		}
		ccic_free_sio_buffers(cam);
		cam->owner = NULL;
	}

	if (cam->users == 0) {
		ccic_disable_mclk();
		pdata->power_set(0, sensor_selected, pdata->eco_flag, pdata->sensor_flag);
		ccic_ctlr_power_down(cam);
		if (alloc_bufs_at_read) /* add such cond. -guang */
			ccic_free_dma_bufs(cam);
	}
	mutex_unlock(&cam->s_mutex);

	wake_up_all(&cam->iowait);
	if (cam->platform_ops->pmua_set)
		cam->platform_ops->pmua_set(0);
	unset_dvfm_constraint();
	return 0;
}

static unsigned int ccic_v4l_poll(struct file *filp, struct poll_table_struct *pt)
{
	struct ccic_camera *cam = filp->private_data;
	int ret  = 0;

	mutex_lock(&cam->s_mutex);
	poll_wait(filp, &cam->iowait, pt);
	if (!list_empty(&cam->sb_full))
		ret = POLLIN | POLLRDNORM;

	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_queryctrl(struct file *filp, void *priv, struct v4l2_queryctrl *qc)
{
	struct ccic_camera *cam = filp->private_data;
	int ret = 0;

	mutex_lock(&cam->s_mutex);
	ret = __ccic_cam_cmd(cam, VIDIOC_QUERYCTRL, qc);
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_g_ctrl(struct file *filp, void *priv, struct v4l2_control *ctrl)
{
	struct ccic_camera *cam = filp->private_data;
	int ret = 0;

	mutex_lock(&cam->s_mutex);
	ret = __ccic_cam_cmd(cam, VIDIOC_G_CTRL, ctrl);
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_s_ctrl(struct file *filp, void *priv, struct v4l2_control *ctrl)
{
	struct ccic_camera *cam = filp->private_data;
	int ret = 0;

	mutex_lock(&cam->s_mutex);
	ret = __ccic_cam_cmd(cam, VIDIOC_S_CTRL, ctrl);
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_querycap(struct file *filp, void *priv, struct v4l2_capability *cap)
{
	struct ccic_camera *cam = filp->private_data;
	int ret = 0;

	mutex_lock(&cam->s_mutex);
	ret = __ccic_cam_cmd(cam, VIDIOC_QUERYCAP, cap);
	mutex_unlock(&cam->s_mutex);

	cap->version = CCIC_VERSION;
	cap->capabilities = V4L2_CAP_VIDEO_CAPTURE | V4L2_CAP_STREAMING;

	return ret;
}

/*
 * The default format we use until somebody says otherwise.
 */
static struct v4l2_pix_format ccic_def_pix_format = {
	.width		= CIF_WIDTH,
	.height		= CIF_HEIGHT,
	.pixelformat	= V4L2_PIX_FMT_UYVY,
	.field		= V4L2_FIELD_NONE,
	.bytesperline	= CIF_WIDTH*2,
	.sizeimage	= CIF_WIDTH*VGA_HEIGHT*2,
};

static int ccic_vidioc_enum_fmt_cap(struct file *filp, void *priv, struct v4l2_fmtdesc *fmt)
{
	struct ccic_camera *cam = priv;
	int ret = 0;

	if (fmt->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
		return -EINVAL;
	mutex_lock(&cam->s_mutex);
	ret = __ccic_cam_cmd(cam, VIDIOC_ENUM_FMT, fmt);
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_try_fmt_cap (struct file *filp, void *priv, struct v4l2_format *fmt)
{
	struct ccic_camera *cam = priv;
	int ret = 0;

	mutex_lock(&cam->s_mutex);
	ret = __ccic_cam_cmd(cam, VIDIOC_TRY_FMT, fmt);
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_s_fmt_cap(struct file *filp, void *priv,	struct v4l2_format *fmt)
{
	struct ccic_camera *cam = priv;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	int ret = 0;

	/*
	 * Can't do anything if the device is not idle
	 * Also can't if there are streaming buffers in place.
	 */
	mutex_lock(&cam->s_mutex);
	if (cam->state != S_IDLE) {
		ret = -EBUSY;
		goto out_unlock;
	}

	/*
	 * See if the formatting works in principle.
	 */
	if(cam->pix_format.bytesperline <= 0) {
		printk("camera: get wrong bytesperline = %d\n", cam->pix_format.bytesperline);
		ret = -EINVAL;
		goto out_unlock;
	}
	mutex_unlock(&cam->s_mutex);

	ret = ccic_vidioc_try_fmt_cap(filp, priv, fmt);
	if (ret < 0) {
		printk(KERN_ERR "camera: try format failed with pix->width = %d : pix->height = %d\n", pix->width, pix->height);
		goto out;
	}

	/*
	 * Now we start to change things for real, so let's do it
	 * under lock.
	 */
	mutex_lock(&cam->s_mutex);
	cam->pix_format = fmt->fmt.pix;

	/*
	 * Make sure we have appropriate DMA buffers.
	 */
	if (cam->nbufs > 0 && cam->dma_buf_size < cam->pix_format.sizeimage)
		ccic_free_dma_bufs(cam);

out_unlock:
	mutex_unlock(&cam->s_mutex);
out:
	return ret;
}

/*
 * Return our stored notion of how the camera is/should be configured.
 * The V4l2 spec wants us to be smarter, and actually get this from
 * the camera (and not mess with it at open time).  Someday.
 */
static int ccic_vidioc_g_fmt_cap(struct file *filp, void *priv,	struct v4l2_format *f)
{
	struct ccic_camera *cam = priv;

	mutex_lock(&cam->s_mutex);
	f->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	f->fmt.pix = cam->pix_format;
	mutex_unlock(&cam->s_mutex);
	return 0;
}

/*
 * We only have one input - the sensor - so minimize the nonsense here.
 */
static int ccic_vidioc_enum_input(struct file *filp, void *priv, struct v4l2_input *input)
{
	struct ccic_camera *cam = priv;
	int ret = 0;

	mutex_lock(&cam->s_mutex);
	if ((input->index == 0 && detected_low == 0) ||
		(input->index == 1 && detected_high == 0) ||
		(input->index != 0 && input->index != 1)) {
		ret = -EINVAL;
		goto out_unlock;
	}

	input->type = V4L2_INPUT_TYPE_CAMERA;
	/* input->std = V4L2_STD_ALL; */ /* Not sure what should go here */
	strcpy(input->name, "Camera");

out_unlock:
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_s_input(struct file *filp, void *priv, unsigned int i)
{
	struct ccic_camera *cam = filp->private_data;
        int ret = 0;

	/* If the required sensor is the same as the current
	 * active one, return immediately.
	 */
	mutex_lock(&cam->s_mutex);
	if (i == sensor_selected)
		goto init_sensor;

	if (((i == SENSOR_LOW) && (detected_low == 1)) || ((i == SENSOR_HIGH) && (detected_high == 1))) {
		/* switch off the previous sensor power */
		((struct sensor_platform_data *)cam->sensor->dev.platform_data)->power_set(0, sensor_selected, ((struct sensor_platform_data *)cam->sensor->dev.platform_data)->eco_flag, ((struct sensor_platform_data *)cam->sensor->dev.platform_data)->sensor_flag);
		cam->sensor = cam->sensors[i];
		sensor_selected = i;
	} else {
		ret = -EINVAL;
		goto out_unlock;
	}

	ccic_enable_mclk(cam);
	((struct sensor_platform_data *)cam->sensor->dev.platform_data)->power_set(1, sensor_selected, ((struct sensor_platform_data *)cam->sensor->dev.platform_data)->eco_flag, ((struct sensor_platform_data *)cam->sensor->dev.platform_data)->sensor_flag);
	mdelay(30);

init_sensor:
	__ccic_cam_reset(cam);
	ret = __ccic_cam_cmd(cam, VIDIOC_S_INPUT, &sensor_selected);

out_unlock:
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_g_input(struct file *filp, void *priv, unsigned int *i)
{
	struct ccic_camera *cam = priv;

	mutex_lock(&cam->s_mutex);
	*i = sensor_selected;
	mutex_unlock(&cam->s_mutex);

	return 0;
}

/* from vivi.c */
static int ccic_vidioc_s_std(struct file *filp, void *priv, v4l2_std_id *a)
{
	return 0;
}

/*
 * G/S_PARM.  Most of this is done by the sensor, but we are
 * the level which controls the number of read buffers.
 */
static int ccic_vidioc_g_parm(struct file *filp, void *priv, struct v4l2_streamparm *parms)
{
	struct ccic_camera *cam = priv;
	int ret = 0;

	mutex_lock(&cam->s_mutex);
	ret = __ccic_cam_cmd(cam, VIDIOC_G_PARM, parms);
	mutex_unlock(&cam->s_mutex);
	parms->parm.capture.readbuffers = n_dma_bufs;
	return ret;
}

static int ccic_vidioc_s_parm(struct file *filp, void *priv, struct v4l2_streamparm *parms)
{
	struct ccic_camera *cam = priv;
	int ret = 0;

	mutex_lock(&cam->s_mutex);
	ret = __ccic_cam_cmd(cam, VIDIOC_S_PARM, parms);
	mutex_unlock(&cam->s_mutex);
	parms->parm.capture.readbuffers = n_dma_bufs;
	return ret;
}

static int ccic_vidioc_cropcap(struct file *filp, void *priv, struct v4l2_cropcap *a)
{
	struct ccic_camera *cam = priv;
	struct v4l2_cropcap *ccap = a;
	int ret = 0;

	mutex_lock(&cam->s_mutex);
	ret = __ccic_cam_cmd(cam, VIDIOC_CROPCAP, ccap);
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_enum_framesizes(struct file *filp, void *priv, struct v4l2_frmsizeenum *fsize)
{
	struct ccic_camera *cam = priv;
	int ret = 0;

	mutex_lock(&cam->s_mutex);
	ret = __ccic_cam_cmd(cam, VIDIOC_ENUM_FRAMESIZES, fsize);
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_enum_frameintervals(struct file *filp, void *priv, struct v4l2_frmivalenum *finterval)
{
	struct ccic_camera *cam = priv;
	int ret = 0;

	mutex_lock(&cam->s_mutex);
	ret = __ccic_cam_cmd(cam, VIDIOC_ENUM_FRAMEINTERVALS, finterval);
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static void ccic_v4l_dev_release(struct video_device *vd)
{
	struct ccic_camera *cam = container_of(vd, struct ccic_camera, v4ldev);
	kfree(cam);
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int ccic_vidioc_g_register(struct file *filp, void *priv, struct v4l2_dbg_register *reg)
{
	struct ccic_camera *cam = priv;
        int ret = 0;

        mutex_lock(&cam->s_mutex);
	ret =  __ccic_cam_cmd(cam, VIDIOC_DBG_G_REGISTER, reg);
	mutex_unlock(&cam->s_mutex);
	return ret;
}

static int ccic_vidioc_s_register(struct file *filp, void *priv, struct v4l2_dbg_register *reg)
{
        struct ccic_camera *cam = priv;
        int ret = 0;

        mutex_lock(&cam->s_mutex);
        ret =  __ccic_cam_cmd(cam, VIDIOC_DBG_S_REGISTER, reg);
        mutex_unlock(&cam->s_mutex);
        return ret;

}
#endif

static long ccic_v4l_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	struct video_device *vdev = video_devdata(filp);
	struct ccic_camera *cam = container_of(vdev, struct ccic_camera, v4ldev);
	int ret = 0;

	/* Handle some specific cmds */
	switch (cmd) {
		case VIDIOC_QUERYCAP:
			ret = ccic_vidioc_querycap(filp, (void *)cam, (struct v4l2_capability *)arg);
			return ret;
		case VIDIOC_ENUM_FMT:
			ret = ccic_vidioc_enum_fmt_cap(filp, (void *)cam, (struct v4l2_fmtdesc *)arg);
			return ret;
		case VIDIOC_TRY_FMT:
			ret = ccic_vidioc_try_fmt_cap(filp, (void *)cam, (struct v4l2_format *)arg);
			return ret;
		case VIDIOC_ENUM_FRAMESIZES:
			ret = ccic_vidioc_enum_framesizes(filp, (void *)cam, (struct v4l2_frmsizeenum *)arg);
			return ret;
		case VIDIOC_ENUM_FRAMEINTERVALS:
			ret = ccic_vidioc_enum_frameintervals(filp, (void *)cam, (struct v4l2_frmivalenum *)arg);
			return ret;
#ifdef CONFIG_VIDEO_ADV_DEBUG
		case VIDIOC_DBG_S_REGISTER:
			ret = ccic_vidioc_s_register(filp, (void *)cam,	(struct v4l2_dbg_register *)arg);
			return ret;
		case VIDIOC_DBG_G_REGISTER:
			ret = ccic_vidioc_g_register(filp, (void *)cam, (struct v4l2_dbg_register *)arg);
			return ret;
#endif
		default:
			break;
	}

	/* Handle other ioctl cmds with standard interface */
	ret = video_ioctl2(filp, cmd, arg);
	return ret;
}

/*
 * This template device holds all of those v4l2 methods; we
 * clone it for specific real devices.
 */
static const struct v4l2_file_operations ccic_v4l_fops = {
	.owner = THIS_MODULE,
	.open = ccic_v4l_open,
	.release = ccic_v4l_release,
	.poll = ccic_v4l_poll,
	.mmap = ccic_v4l_mmap,
	.ioctl = ccic_v4l_ioctl,
};

/* upgrade changes from .25 to .28 */
struct v4l2_ioctl_ops ccic_ioctl_ops = {
	.vidioc_querycap		= ccic_vidioc_querycap,
	.vidioc_enum_fmt_vid_cap	= ccic_vidioc_enum_fmt_cap,
	.vidioc_try_fmt_vid_cap		= ccic_vidioc_try_fmt_cap,
	.vidioc_s_fmt_vid_cap		= ccic_vidioc_s_fmt_cap,
	.vidioc_g_fmt_vid_cap		= ccic_vidioc_g_fmt_cap,
	.vidioc_enum_framesizes		= ccic_vidioc_enum_framesizes,
	.vidioc_enum_frameintervals	= ccic_vidioc_enum_frameintervals,
	.vidioc_enum_input		= ccic_vidioc_enum_input,
	.vidioc_s_input			= ccic_vidioc_s_input,
	.vidioc_g_input			= ccic_vidioc_g_input,
	.vidioc_s_std			= ccic_vidioc_s_std,
	.vidioc_reqbufs			= ccic_vidioc_reqbufs,
	.vidioc_querybuf		= ccic_vidioc_querybuf,
	.vidioc_qbuf			= ccic_vidioc_qbuf,
	.vidioc_dqbuf			= ccic_vidioc_dqbuf,
	.vidioc_streamon		= ccic_vidioc_streamon,
	.vidioc_streamoff		= ccic_vidioc_streamoff,
	.vidioc_queryctrl		= ccic_vidioc_queryctrl,
	.vidioc_g_ctrl			= ccic_vidioc_g_ctrl,
	.vidioc_s_ctrl			= ccic_vidioc_s_ctrl,
	.vidioc_g_parm			= ccic_vidioc_g_parm,
	.vidioc_s_parm			= ccic_vidioc_s_parm,
	.vidioc_cropcap			= ccic_vidioc_cropcap,
#ifdef CONFIG_VIDEO_ADV_DEBUG
	.vidioc_g_register		= ccic_vidioc_g_register,
	.vidioc_s_register		= ccic_vidioc_s_register,
#endif
};

static struct video_device ccic_v4l_template = {
	.name = "pxa688-camera",
	.vfl_type = VID_TYPE_CAPTURE,
	.minor = -1, /* Get one dynamically */
	.tvnorms = V4L2_STD_NTSC_M,
	.current_norm = V4L2_STD_NTSC_M, /* Qiang: changed to solve camerasrc negotiate failure */
	.fops = &ccic_v4l_fops,
	.release = ccic_v4l_dev_release,
	.ioctl_ops = &ccic_ioctl_ops,
};

/*
 * Disable ccic when receive SOFx for stop DMA
 */
static void ccic_stop_at_buf(struct ccic_camera *cam)
{
	unsigned int irqs, frame = 0xff;
	unsigned long current_time;

	/* Receive SOFx of frame x */
	current_time = jiffies;
	do {
		irqs = ccic_irqs;
		if (irqs & IRQ_SOF0) {
			frame = 0;
			break;
		} else if (irqs & IRQ_SOF1) {
			frame = 1;
			break;
		} else if (irqs & IRQ_SOF2) {
			frame = 2;
			break;
		}

		/* 100ms timeout. sometimes there's no SOF, which make kernel halts. -guang */
		if (time_before(current_time + HZ / 10, jiffies))
			break;
	} while (!(irqs & IRQ_SOF));

	/* Disable CCIC after receive SOFx, it will stop DMA automatically */
	ccic_ctlr_stop(cam);

	/* Get current time stamp */
	current_time = jiffies;

	/* Receive EOFx of frame x then can stop controller */
	do {
		irqs = ccic_irqs;
		if (irqs & (IRQ_EOF0 << frame))
			break;
		/*
		 * Workaround for stopping DMA controller
		 * Exit the wait loop if can't receive EOFx in one frame transfer time slot
		 */
		if (time_before(current_time + HZ / 10, jiffies))
			break;
	} while (!(irqs & (IRQ_EOF0 << frame)));
}

static irqreturn_t ccic_irq(int irq, void *data)
{
	struct ccic_camera *cam = data;
	unsigned int irqs, frame = 0xff;
#ifdef SOF_EOF_DEBUG
	unsigned int irqs_raw, line_num;
	static unsigned int first = 0, second = 0;
	static int counter = 0;
#endif

#ifdef SOF_EOF_DEBUG
	irqs_raw = ccic_reg_read(cam, REG_IRQSTATRAW);
#endif
	irqs = ccic_reg_read(cam, REG_IRQSTAT);
	ccic_reg_write(cam, REG_IRQSTAT, irqs);	//clear irqs here
#ifdef SOF_EOF_DEBUG
	printk("%s: REG_IRQRAWSTAT = %x\n", __func__, irqs_raw);
	if (counter++ % 2)
		second = irqs;
	else 
		first = irqs;
	if (first == second)
		printk("\n########## %s: TWO CONTINUOUS SOF/EOF OCCURED!!! ##########\n\n", __func__);
#endif

	/*
	 * Handle any frame completions
	 * There really should not be more than one of these, or we have fallen far behind
	 */
	if (irqs & IRQ_EOF0)
		frame = 0;
	else if (irqs & IRQ_EOF1)
		frame = 1;
	else if (irqs & IRQ_EOF2)
		frame = 2;
	else
		frame = 0xff;

	if (0xff != frame) {
#ifdef SOF_EOF_DEBUG
		line_num = ccic_reg_read(cam, REG_LNNUM);
		printk("%s: REG_LINENUM = %d\n", __func__, line_num);
#endif
		if (cam->state == S_STREAMING)
			ccic_switch_dma(cam, frame);
	}

	/*
	 * Get current ccic irq status
	 * Stop DMA by SOF/EOF in ccic_stop_at_buf()
	 */
	if (irqs & FRAMEIRQS)
		ccic_irqs = irqs;

	return IRQ_HANDLED;
}

static int pxa910_camera_probe(struct platform_device *pdev)
{
	struct resource *res;
	struct ccic_camera *cam;
	int ret = 0;

	/*
	 * Start putting together one of our big camera structures.
	 */
	ret = -ENOMEM;
	cam = kzalloc(sizeof(struct ccic_camera), GFP_KERNEL);
	if (cam == NULL)
		goto out;
	memset(cam, 0x00, sizeof(struct ccic_camera));
	platform_set_drvdata(pdev, cam);
	mutex_init(&cam->s_mutex);
	mutex_lock(&cam->s_mutex);
	spin_lock_init(&cam->dev_lock);
	cam->state = S_NOTREADY;
	init_waitqueue_head(&cam->iowait);
	cam->pdev = pdev;
	cam->pix_format = ccic_def_pix_format;
	INIT_LIST_HEAD(&cam->dev_list);
	INIT_LIST_HEAD(&cam->sb_avail);
	INIT_LIST_HEAD(&cam->sb_full);
	INIT_LIST_HEAD(&cam->sb_dma);

	cam->platform_ops = pdev->dev.platform_data;
	if (cam->platform_ops == NULL) {
		printk(KERN_ERR "camera: no platform data defined\n");
		ret = -ENODEV;
		goto out_free;
	}

	cam->irq = platform_get_irq(pdev, 0);
	if (cam->irq < 0) {
		ret = -ENXIO;
		goto out_free;
	}

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (res == NULL) {
		printk("camera: no IO memory resource defined\n");
		ret = -ENODEV;
		goto out_free;
	}

	ret = -EIO;
	cam->regs = ioremap(res->start, SZ_4K);
	if (! cam->regs) {
		printk(KERN_ERR "camera: unable to ioremap pxa688-camera regs\n");
		goto out_free;
	}
	ret = request_irq(cam->irq, ccic_irq, IRQF_SHARED, "pxa688-camera", cam);
	if (ret)
		goto out_iounmap;

	/*
	 * Initialize the controller and leave it powered up.  It will
	 * stay that way until the sensor driver shows up.
	 */
	if (cam->platform_ops->power_set) {
		cam->platform_ops->power_set(1);
		ccic_ctlr_init(cam);
		cam->platform_ops->power_set(0);
	}

	/* CCIC_CLOCK_CTRL */
	/* 0x3 use Core clock, mclk=25M */
	ccic_reg_write(cam, REG_CLKCTRL, 0x3<<29 | 0x10);
	/* 0x3 use Core clock, mclk=26.6M */
//	ccic_reg_write(cam, REG_CLKCTRL, 0x3<<29 | 0xf);
	/* 0x2 use AXI BUS clock = 266M, mclk=22M */
//	ccic_reg_write(cam, REG_CLKCTRL, 0x2<<29 | 0xc);

	/* CCIC_CTRL_1 */
	/* Select 2 Frames and 128-bytes DMA burst */
//	ccic_reg_write(cam, REG_CTRL1, 0x0a00003c);
	/* Select 3 Frames and 256-bytes DMA burst */
//	ccic_reg_write(cam, REG_CTRL1, 0x0400003c);
	/* Select 3 Frames, 256-bytes DMA burst, DMA posted write */
//	ccic_reg_write(cam, REG_CTRL1, 0x4400003c);
	/* Select 2 Frames, 256-bytes DMA burst, DMA posted write */
//	ccic_reg_write(cam, REG_CTRL1, 0x4c00003c);
	/* Select 2 Frames, 128-bytes DMA burst, DMA posted write */
	ccic_reg_write(cam, REG_CTRL1, 0x4a00003c);	// for YUV Planar Format Output, silicon issue�?
	/*
	 * Get the v4l2 setup done.
	 */
	cam->v4ldev = ccic_v4l_template;
	cam->v4ldev.debug = 0;
	cam->v4ldev.dev = pdev->dev;	//upgrade changes from .25 to .28
	cam->v4ldev.index = pdev->id;
	printk(KERN_NOTICE "camera: probe id = %d\n", cam->v4ldev.index);

	cam->v4ldev.parent = &pdev->dev;
	ret = video_register_device(&cam->v4ldev, VFL_TYPE_GRABBER, -1);
	if (ret)
		goto out_freeirq;

	/*
	 * If so requested, try to get our DMA buffers now.
	 */
	if (!alloc_bufs_at_read) {
		if (ccic_alloc_dma_bufs(cam, 1))
			cam_warn(cam, "camera: unable to alloc DMA buffers at load will try again later.");
	}

	/* allocate rubbish buffer */
	cam->page = alloc_pages(GFP_KERNEL, get_order(dma_buf_size));
	if (cam->page == NULL){
		printk(KERN_ERR "camera: allocate rubbish buffer failed\n");
		ret = -ENOMEM;
		goto out_free;
	}

	cam->rubbish_buf_virt = (void *)page_address(cam->page);
	cam->rubbish_buf_phy = __pa(cam->rubbish_buf_virt);
	mutex_unlock(&cam->s_mutex);
	ccic_add_dev(cam);
	return 0;
out_freeirq:
	ccic_ctlr_power_down(cam);
	free_irq(cam->irq, cam);
out_iounmap:
	iounmap(cam->regs);
out_free:
	kfree(cam);
out:
	return ret;
}

/*
 * Shut down an initialized device
 */
static void ccic_shutdown(struct ccic_camera *cam)
{
	/* FIXME: Make sure we take care of everything here */
	if (cam->n_sbufs > 0)
		/* What if they are still mapped?  Shouldn't be, but... */
		ccic_free_sio_buffers(cam);
	ccic_remove_dev(cam);
	ccic_ctlr_stop_dma(cam);
	ccic_ctlr_power_down(cam);
	ccic_free_dma_bufs(cam);
	free_irq(cam->irq, cam);
	iounmap(cam->regs);
	video_unregister_device(&cam->v4ldev);
//	kfree(cam); /* done in ccic_v4l_dev_release() */
}

static int pxa910_camera_remove(struct platform_device *pdev)
{
	struct ccic_camera *cam = ccic_find_by_pdev(pdev);

	if (cam == NULL) {
		printk(KERN_WARNING "camera: remove on unknown pdev %p\n", pdev);
		return -ENODEV;
	}
	mutex_lock(&cam->s_mutex);
	if (cam->users > 0)
		cam_warn(cam, "camera: removing a device with users!\n");
	ccic_shutdown(cam);

	/* free rubbish buffer */
	if (cam->rubbish_buf_virt)
		free_pages((unsigned long)cam->rubbish_buf_virt, get_order(dma_buf_size));

	/* free ccic_camera struct */
	if (cam)
		kfree(cam);

	return 0;
}

#ifdef CONFIG_PM
/*
 * Basic power management.
 */
static int ccic_suspend(struct platform_device *dev, pm_message_t state)
{
	struct ccic_camera *cam = platform_get_drvdata(dev);
	struct sensor_platform_data *pdata;
	enum ccic_state cstate;

	cstate = cam->state;	/* HACK - ccic_ctlr_stop_dma() set state to S_IDLE */

	if (cam->state != S_IDLE && cam->state != S_NOTREADY) {
		ccic_stop_at_buf(cam);
		ccic_ctlr_stop_dma(cam);
		__ccic_cam_cmd(cam, VIDIOC_STREAMOFF, NULL);
	}

	if (cam->users > 0) {
		/* we just keep sensor in idle state in first state. reset sensor need more configuration */
		pdata = cam->sensor->dev.platform_data;
		pdata->power_set(0, sensor_selected, pdata->eco_flag, pdata->sensor_flag);
		ccic_ctlr_power_down(cam);
		ccic_disable_mclk();
		unset_dvfm_constraint();
	}

	cam->state = cstate;

	return 0;
}

static int ccic_resume(struct platform_device *dev)
{
	struct ccic_camera *cam = platform_get_drvdata(dev);
	struct sensor_platform_data *pdata;
	int ret = 0;

	ccic_ctlr_init(cam);
	ccic_ctlr_power_down(cam);

	mutex_lock(&cam->s_mutex);
	if (cam->users > 0) {
		pdata = cam->sensor->dev.platform_data;
		set_dvfm_constraint();
		ccic_enable_mclk(cam);
		ccic_ctlr_power_up(cam);
		pdata->power_set(1, sensor_selected, pdata->eco_flag, pdata->sensor_flag);
	}
	mutex_unlock(&cam->s_mutex);

	if (cam->state == S_SPECREAD)
		cam->state = S_IDLE;  /* Don't bother restarting */
	else if (cam->state == S_SINGLEREAD || cam->state == S_STREAMING)
		ret = ccic_read_setup(cam, cam->state);
	return ret;
}
#endif  /* CONFIG_PM */

static struct platform_driver pxa910_camera_driver = {
	.driver = {
		.name = "pxa688-camera"
	},
	.probe 		= pxa910_camera_probe,
	.remove 	= pxa910_camera_remove,
#ifdef CONFIG_PM
	.suspend 	= ccic_suspend,
	.resume 	= ccic_resume,
#endif
};

static int __devinit pxa910_camera_init(void)
{
#ifdef CONFIG_DVFM
	dvfm_register("Camera", &dvfm_dev_idx);
#endif
	return platform_driver_register(&pxa910_camera_driver);
}

static void __exit pxa910_camera_exit(void)
{
	platform_driver_unregister(&pxa910_camera_driver);
#ifdef CONFIG_DVFM
	dvfm_unregister("Camera", &dvfm_dev_idx);
#endif
}

module_init(pxa910_camera_init);
module_exit(pxa910_camera_exit);

