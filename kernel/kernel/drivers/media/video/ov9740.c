/*
 * OmniVision ov9740 sensor driver
 *
 * Copyright (C) 2011 Marvell International Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Created:  2011 Albert Wang <twang13@marvell.com>
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/videodev.h>
#include <linux/i2c.h>
#include <linux/clk.h>
#include <media/v4l2-common.h>
#include <media/v4l2-chip-ident.h>
#include <mach/hardware.h>
#include <mach/camera.h>

#include "pxa688_camera.h"
#include "ov9740.h"

MODULE_AUTHOR("Albert Wang <twang13@marvell.com>");
MODULE_DESCRIPTION("OmniVision ov9740 sensor driver");
MODULE_LICENSE("GPL");

/* The maximum allowed times for repeating OV9740 detection */
#define MAX_DETECT_NUM	3

/* Supported Resolutions */
enum {
	OV9740_VGA,
	OV9740_WVGA,
	OV9740_720P,
};

struct ov9740_resolution {
	unsigned int width;
	unsigned int height;
	int fps;
};

static struct ov9740_resolution ov9740_resolutions[] = {
	[OV9740_VGA] = {
		.width	= 640,
		.height	= 480,
		.fps    = 30,
	},
	[OV9740_WVGA] = {
		.width	= 800,
		.height	= 480,
		.fps    = 30,
	},
	[OV9740_720P] = {
		.width	= 1280,
		.height	= 720,
		.fps    = 30,
	},
};

#define N_OV9740_RES ARRAY_SIZE(ov9740_resolutions)

static struct ov9740_format_struct {
	__u8 *desc;
	__u32 pixelformat;
	struct ov9740_reg *regs;
	int bpp;   /* bits per pixel */
} ov9740_formats[] = {
	{
		.desc		= "YUV420 planar",
		.pixelformat	= V4L2_PIX_FMT_YUV420,
		.regs		= ov9740_fmt_yuv422,
		.bpp		= 12,
	},
	{
		.desc		= "YUYV422 packet",
		.pixelformat	= V4L2_PIX_FMT_YUYV,
		.regs		= ov9740_fmt_yuv422,
		.bpp		= 16,
	},
	{
		.desc		= "UYVY422 packet",
		.pixelformat	= V4L2_PIX_FMT_UYVY,
		.regs		= ov9740_fmt_yuv422,
		.bpp		= 16,
	},
};

#define N_OV9740_FMTS ARRAY_SIZE(ov9740_formats)

/* Read a register */
static int ov9740_reg_read(struct i2c_client *client, u16 reg, u8 *val)
{
	int ret;
	struct i2c_msg msg[] = {
		{
			.addr	= client->addr,
			.flags	= 0,
			.len	= 2,
			.buf	= (u8 *)&reg,
		},
		{
			.addr	= client->addr,
			.flags	= I2C_M_RD,
			.len	= 1,
			.buf	= val,
		},
	};

	reg = swab16(reg);

	ret = i2c_transfer(client->adapter, msg, 2);
	if (ret < 0) {
		dev_err(&client->dev, "camera: ov9740 failed to read register 0x%04x!\n", reg);
		return ret;
	}

	return 0;
}

/* Write a register */
static int ov9740_reg_write(struct i2c_client *client, u16 reg, u8 val)
{
	struct i2c_msg msg;
	struct {
		u16 reg;
		u8 val;
	} __packed buf;
	int ret;

	reg = swab16(reg);

	buf.reg = reg;
	buf.val = val;

	msg.addr = client->addr;
	msg.flags = 0;
	msg.len	= 3;
	msg.buf	= (u8 *)&buf;

	ret = i2c_transfer(client->adapter, &msg, 1);
	if (ret < 0) {
		dev_err(&client->dev, "camera: ov9740 failed to write register 0x%04x!\n", reg);
		return ret;
	}

	if (reg == SOFTWARE_RESET && (val & 0x01)) {
		printk("camera: a s/w reset triggered, may result in register value lost!\n");
		msleep(2); /* Wait for reset to run, OV spec suggest 2 ~ 5 ms */
	}

	return 0;
}

/* Read a register, alter its bits, write it back */
static int ov9740_reg_rmw(struct i2c_client *client, u16 reg, u8 set, u8 unset)
{
	u8 val;
	int ret;

	ret = ov9740_reg_read(client, reg, &val);
	if (ret < 0) {
		dev_err(&client->dev, "camera: ov9740 [Read]-Modify-Write of register %04x failed!\n", reg);
		return ret;
	}

	val |= set;
	val &= ~unset;

	ret = ov9740_reg_write(client, reg, val);
	if (ret < 0) {
		dev_err(&client->dev, "camera: ov9740 Read-Modify-[Write] of register %04x failed!\n", reg);
		return ret;
	}

	return 0;
}

static int ov9740_reg_write_array(struct i2c_client *client, const struct ov9740_reg *regarray, int regarraylen)
{
	int i;
	int ret;

	for (i = 0; i < regarraylen; i++) {
		ret = ov9740_reg_write(client, regarray[i].reg, regarray[i].val);
		if (ret < 0)
			return ret;
	}

	return 0;
}

static int ov9740_t_vflip(struct i2c_client *client, int value)
{
	int ret = 0;

	if (value)
		ret = ov9740_reg_rmw(client, IMAGE_ORT, 0x2, 0);
	else
		ret = ov9740_reg_rmw(client, IMAGE_ORT, 0, 0x2);

	return ret;
}

static int ov9740_q_vflip(struct i2c_client *client, __s32 *value)
{
	int ret = 0;
	u8 v;

	ret = ov9740_reg_read(client, IMAGE_ORT, &v);
	*value = (v & 0x2) == 0x2;

	return ret;
}

static int ov9740_t_hflip(struct i2c_client *client, int value)
{
	int ret = 0;

	if (value)
		ret = ov9740_reg_rmw(client, IMAGE_ORT, 0x1, 0);
	else
		ret = ov9740_reg_rmw(client, IMAGE_ORT, 0, 0x1);

	return ret;
}

static int ov9740_q_hflip(struct i2c_client *client, __s32 *value)
{
	int ret = 0;
	u8 v;
	ret = ov9740_reg_read(client, IMAGE_ORT, &v);
	*value = (v & 0x1) == 0x1;

	return ret;
}

static struct ov9740_control {
	struct v4l2_queryctrl qc;
	int (*query)(struct i2c_client *client, __s32 *value);
	int (*tweak)(struct i2c_client *client, int value);
} ov9740_controls[] = {
	{
		.qc = {
			.id		= V4L2_CID_VFLIP,
			.type		= V4L2_CTRL_TYPE_BOOLEAN,
			.name		= "Flip Vertically",
			.minimum	= 0,
			.maximum	= 1,
			.step		= 1,
			.default_value	= 0,
		},
		.tweak = ov9740_t_vflip,
		.query = ov9740_q_vflip,
	},
	{
		.qc = {
			.id		= V4L2_CID_HFLIP,
			.type		= V4L2_CTRL_TYPE_BOOLEAN,
			.name		= "Flip Horizontally",
			.minimum	= 0,
			.maximum	= 1,
			.step		= 1,
			.default_value	= 0,
		},
		.tweak = ov9740_t_hflip,
		.query = ov9740_q_hflip,
	},
};

#define N_CONTROLS (ARRAY_SIZE(ov9740_controls))

/* Start/Stop streaming from the device */
static int ov9740_streamon(struct i2c_client *client)
{
	int ret;

	dev_dbg(&client->dev, "camera: ov9740 Stream On!\n");
	/* Start Streaming */
	ret = ov9740_reg_write(client, MODE_SELECT, 0x01);

	return ret;
}

static int ov9740_streamoff(struct i2c_client *client)
{
	int ret;

	dev_dbg(&client->dev, "camera: ov9740 Stream Off!\n");
	/* Set Streaming to Standby */
	ret = ov9740_reg_write(client, MODE_SELECT, 0x00);

	return ret;
}

static struct ov9740_control *ov9740_find_control(__u32 id)
{
	int i;

	for (i = 0; i < N_CONTROLS; i++)
		if (ov9740_controls[i].qc.id == id)
			return ov9740_controls + i;

	return NULL;
}

static int ov9740_g_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct ov9740_control *octrl = ov9740_find_control(ctrl->id);
	int ret = 0;

	if (octrl == NULL)
		return -EINVAL;

	ret = octrl->query(client, &ctrl->value);
	if (ret >= 0)
		return 0;

	return ret;
}

static int ov9740_s_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct ov9740_control *octrl = ov9740_find_control(ctrl->id);
	int ret = 0;

	if (octrl == NULL)
		return -EINVAL;

	ret =  octrl->tweak(client, ctrl->value);
	if (ret >= 0)
		return 0;

	return ret;
}

static int ov9740_g_parm(struct i2c_client *client, struct v4l2_streamparm *parms)
{
	return 0;
}

static int ov9740_s_parm(struct i2c_client *client, struct v4l2_streamparm *parms)
{
	return 0;
}

static int ov9740_s_input(struct i2c_client *client, int *id)
{
	return 0;
}

static int ov9740_querycap(struct i2c_client *client, struct v4l2_capability *argp)
{
	if (!argp) {
		printk(KERN_ERR " argp is NULL %s %d \n", __FUNCTION__, __LINE__);
		return -EINVAL;
	}
	strcpy(argp->driver, "ov9740");
	strcpy(argp->card, "MMP2");
	return 0;
}

static int ov9740_enum_fmt(struct i2c_client *client, struct v4l2_fmtdesc *fmt)
{
	struct ov9740_format_struct *ofmt;

	if (fmt->index >= N_OV9740_FMTS)
		return -EINVAL;

	ofmt = ov9740_formats + fmt->index;
	fmt->flags = 0;
	strcpy(fmt->description, ofmt->desc);
	fmt->pixelformat = ofmt->pixelformat;
	return 0;
}

static int ov9740_int_reset(struct i2c_client *client)
{
	int ret = 0;

	/* Software Reset */
	ret = ov9740_reg_write(client, SOFTWARE_RESET, 0x01);
	if (!ret)
		/* Set Streaming to Standby */
		ret = ov9740_reg_write(client, MODE_SELECT, 0x00);
	msleep(2);

	/* Initialize settings */
	ret = ov9740_reg_write_array(client, ov9740_defaults, ARRAY_SIZE(ov9740_defaults));
	if (ret < 0)
		return ret;
	return 0;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int ov9740_get_register(struct i2c_client *client, struct v4l2_dbg_register *reg)
{
	int ret;
	u8 val;

	if (reg->reg & ~0xffff)
		return -EINVAL;

	reg->size = 2;

	ret = ov9740_reg_read(client, reg->reg, &val);
	if (ret)
		return ret;

	reg->val = (__u64)val;

	return ret;
}

static int ov9740_set_register(struct i2c_client *client, struct v4l2_dbg_register *reg)
{
	if (reg->reg & ~0xffff || reg->val & ~0xff)
		return -EINVAL;

	return ov9740_reg_write(client, reg->reg, reg->val);
}
#endif

static int ov9740_try_fmt(struct i2c_client *client, struct v4l2_format *fmt,
		struct ov9740_format_struct **ret_fmt,
		struct ov9740_resolution **ret_wsize)
{
	int index_fmt, index_wsize;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	for (index_fmt = 0; index_fmt < N_OV9740_FMTS; index_fmt++)
		if (ov9740_formats[index_fmt].pixelformat == pix->pixelformat)
			break;

	if (index_fmt >= N_OV9740_FMTS) {
		printk(KERN_ERR "camera: ov9740 try unsupported format!\n");
		return -EINVAL;
	}

	if (ret_fmt != NULL)
		*ret_fmt = &ov9740_formats[index_fmt];

	if (pix->field == V4L2_FIELD_ANY)
		pix->field = V4L2_FIELD_NONE;
	else if (pix->field != V4L2_FIELD_NONE) {
		printk(KERN_ERR "camera: ov9740 pixel filed should be V4l2_FIELD_NONE!\n");
		return -EINVAL;
	}

	for (index_wsize = 0; index_wsize < N_OV9740_RES; index_wsize++)
		if (pix->width == ov9740_resolutions[index_wsize].width && pix->height == ov9740_resolutions[index_wsize].height)
			break;

	if (index_wsize >= N_OV9740_RES) {
		printk(KERN_ERR "camera: ov9740 try unsupported preview format size!\n");
		return -EINVAL;
	}

	if (ret_wsize != NULL)
		*ret_wsize = &ov9740_resolutions[index_wsize];
	pix->bytesperline = pix->width*ov9740_formats[index_fmt].bpp/8;
	pix->sizeimage = pix->height*pix->bytesperline;

	return 0;
}

static int ov9740_enum_fmsize(struct i2c_client *client, struct v4l2_frmsizeenum *argp)
{
	struct v4l2_frmsizeenum frmsize;

	if (copy_from_user(&frmsize, argp, sizeof(frmsize)))
		return -EFAULT;

	switch (frmsize.pixel_format) {
	case V4L2_PIX_FMT_YUYV:
		if (frmsize.index == N_OV9740_RES)
			return -EINVAL;
		else if (frmsize.index > N_OV9740_RES) {
			printk(KERN_ERR "camera: ov9740 enum unsupported preview format size!\n");
			return -EINVAL;
		} else {
			frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
			frmsize.discrete.height = ov9740_resolutions[frmsize.index].height;
			frmsize.discrete.width = ov9740_resolutions[frmsize.index].width;
			break;
		}
	case V4L2_PIX_FMT_UYVY:
		if (frmsize.index == N_OV9740_RES)
			return -EINVAL;
		else if (frmsize.index > N_OV9740_RES) {
			printk(KERN_ERR "camera: ov9740 enum unsupported preview format size!\n");
			return -EINVAL;
		} else {
			frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
			frmsize.discrete.height = ov9740_resolutions[frmsize.index].height;
			frmsize.discrete.width = ov9740_resolutions[frmsize.index].width;
			break;
		}
	case V4L2_PIX_FMT_YUV420:
		if (frmsize.index == N_OV9740_RES)
			return -EINVAL;
		else if (frmsize.index > N_OV9740_RES) {
			printk(KERN_ERR "camera: ov9740 enum unsupported preview format size!\n");
			return -EINVAL;
		} else {
			frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
			frmsize.discrete.height = ov9740_resolutions[frmsize.index].height;
			frmsize.discrete.width = ov9740_resolutions[frmsize.index].width;
			break;
		}

	default:
		printk(KERN_ERR "camera: ov9740 enum format size with unsupported format!\n");
		return -EINVAL;
	}

	if (copy_to_user(argp, &frmsize, sizeof(frmsize)))
		return -EFAULT;
	return 0;
}

static int ov9740_enum_fminterval(struct i2c_client *client, struct v4l2_frmivalenum *argp)
{
	struct v4l2_frmivalenum frminterval;

	if (copy_from_user(&frminterval, argp, sizeof(frminterval)))
		return -EFAULT;

	switch (frminterval.pixel_format) {
	case V4L2_PIX_FMT_YUYV:
		if (frminterval.index == N_OV9740_RES)
			return -EINVAL;
		else if (frminterval.index > N_OV9740_RES) {
			printk(KERN_ERR "camera: ov9740 enum unsupported preview format frame rate!\n");
			return -EINVAL;
		} else {
			frminterval.type = V4L2_FRMIVAL_TYPE_DISCRETE;
			frminterval.discrete.numerator = 1;
			frminterval.discrete.denominator = ov9740_resolutions[frminterval.index].fps;
			break;
		}
	case V4L2_PIX_FMT_UYVY:
		if (frminterval.index == N_OV9740_RES)
			return -EINVAL;
		else if (frminterval.index > N_OV9740_RES) {
			printk(KERN_ERR "camera: ov9740 enum unsupported preview format frame rate!\n");
			return -EINVAL;
		} else {
			frminterval.type = V4L2_FRMIVAL_TYPE_DISCRETE;
			frminterval.discrete.numerator = 1;
			frminterval.discrete.denominator = ov9740_resolutions[frminterval.index].fps;
			break;
		}
	case V4L2_PIX_FMT_YUV420:
		if (frminterval.index == N_OV9740_RES)
			return -EINVAL;
		else if (frminterval.index > N_OV9740_RES) {
			printk(KERN_ERR "camera: ov9740 enum unsupported preview format frame rate!\n");
			return -EINVAL;
		} else {
			frminterval.type = V4L2_FRMIVAL_TYPE_DISCRETE;
			frminterval.discrete.numerator = 1;
			frminterval.discrete.denominator = ov9740_resolutions[frminterval.index].fps;
			break;
		}
	default:
		printk(KERN_ERR "camera: ov9740 enum format frame rate with unsupported format!\n");
		return -EINVAL;
	}

	if (copy_to_user(argp, &frminterval, sizeof(frminterval)))
		return -EFAULT;
	return 0;
}

static int ov9740_s_fmt(struct i2c_client *client, struct v4l2_format *fmt)
{
	int ret = 0;
	struct ov9740_format_struct *ovfmt = NULL;
	struct ov9740_resolution *wsize = NULL;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;

	ret = ov9740_try_fmt(client, fmt, &ovfmt, &wsize);
	if (ret)
		return ret;

	/* select register configuration for given resolution */
	if (pix->width == ov9740_resolutions[OV9740_VGA].width) {
		dev_dbg(&client->dev, "camera: ov9740 set image size to VGA!\n");
		ret = ov9740_reg_write_array(client, ov9740_regs_vga, ARRAY_SIZE(ov9740_regs_vga));
	} else if (pix->width == ov9740_resolutions[OV9740_WVGA].width) {
		dev_dbg(&client->dev, "camera: ov9740 set image size to WVGA!\n");
		ret = ov9740_reg_write_array(client, ov9740_regs_wvga, ARRAY_SIZE(ov9740_regs_wvga));
	} else if (pix->width == ov9740_resolutions[OV9740_720P].width) {
		dev_dbg(&client->dev, "camera: ov9740 set image size to 720P!\n");
		ret = ov9740_reg_write_array(client, ov9740_regs_720p, ARRAY_SIZE(ov9740_regs_720p));
	} else {
		dev_err(&client->dev, "camera: ov9740 failed to select resolution!\n");
		ret = -EINVAL;
	}

	return ret;
}

static int ov9740_q_ctrl(struct i2c_client *client, struct v4l2_queryctrl *qc)
{
	struct ov9740_control *ctrl = ov9740_find_control(qc->id);

	if (ctrl == NULL)
		return -EINVAL;
	*qc = ctrl->qc;
	return 0;
}

static int ov9740_detect(struct i2c_client *client)
{
	struct ov9740_priv *priv = i2c_get_clientdata(client);
	u8 modelhi, modello;
	int ret;

	/*
	 * check and show product ID and manufacturer ID
	 */
	ret = ov9740_reg_read(client, MODEL_ID_HI, &modelhi);
	if (ret < 0)
		goto err;

	ret = ov9740_reg_read(client, MODEL_ID_LO, &modello);
	if (ret < 0)
		goto err;

	priv->model = (modelhi << 8) | modello;

	ret = ov9740_reg_read(client, REVISION_NUMBER, &priv->revision);
	if (ret < 0)
		goto err;

	ret = ov9740_reg_read(client, MANUFACTURER_ID, &priv->manid);
	if (ret < 0)
		goto err;

	ret = ov9740_reg_read(client, SMIA_VERSION, &priv->smiaver);
	if (ret < 0)
		goto err;

	if (priv->model != 0x9740) {
		ret = -ENODEV;
		goto err;
	}

	priv->ident = V4L2_IDENT_OV9740;
	printk("camera: ov9740 Model_ID = 0x%04x, Revision_NUM = 0x%02x, Manufacturer_ID = 0x%02x, SMIA_Version = 0x%02x\n", priv->model, priv->revision, priv->manid, priv->smiaver);

err:
	return ret;
}

/*
 * i2c_driver function
 */
static int ov9740_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int i, ret, eco = CAMERA_ECO_OFF, sensor = SENSOR_OVT;
	struct ov9740_priv *priv;
	struct sensor_platform_data *pdata;

	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_OPEN, pdata->id, eco, sensor);
	pdata->eco_set(eco);

	priv = kzalloc(sizeof(struct ov9740_priv), GFP_KERNEL);
	if (!priv) {
		dev_err(&client->dev, "camera: ov9740 failed to allocate private data!\n");
		return -ENOMEM;
	}

	i2c_set_clientdata(client, priv);
	msleep(10);

	for (i = MAX_DETECT_NUM; i > 0; --i) {
		ret = ov9740_detect(client);
		if (!ret) {
			printk(KERN_NOTICE "camera: OmniVision 9740 sensor detected!\n");
			pdata->eco_flag = eco;
			pdata->sensor_flag = sensor;
			goto sensor_detected;
		}

		if (ret == -ENXIO)
			goto out_free_priv;
		else
			printk(KERN_ERR "camera: OmniVision 9740 sensor detect failure, will retry %d times!\n", i);
	}
	/* NO OV9740 detected within limited loops */
	printk(KERN_NOTICE "camera: Abort retry, failed to detect OmniVision 9740 sensor!\n");
	ret = -ENODEV;
	goto out_free_priv;

sensor_detected:
	ret = ccic_sensor_attach(client);
	goto out_free;

out_free_priv:
	kfree(priv);

out_free:
	pdata->power_set(SENSOR_CLOSE, pdata->id, eco, sensor);

	return ret;
}

static int ov9740_remove(struct i2c_client *client)
{
	struct ov9740_priv *priv;
	struct sensor_platform_data *pdata;

	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_CLOSE, pdata->id, pdata->eco_flag, pdata->sensor_flag);
	ccic_sensor_detach(client);
	priv = i2c_get_clientdata(client);
	i2c_set_clientdata(client, NULL);
	kfree(priv);

	return 0;
}

static int ov9740_command(struct i2c_client *client, unsigned int cmd, void *arg)
{
	switch (cmd) {
	case VIDIOC_DBG_G_CHIP_IDENT:
		return v4l2_chip_ident_i2c_client(client, arg, V4L2_IDENT_OV9740, 0);
	case VIDIOC_INT_RESET:
		return ov9740_int_reset(client);
	case VIDIOC_QUERYCAP:
		return ov9740_querycap(client, (struct v4l2_capability *) arg);
	case VIDIOC_ENUM_FMT:
		return ov9740_enum_fmt(client, (struct v4l2_fmtdesc *) arg);
	case VIDIOC_TRY_FMT:
		return ov9740_try_fmt(client, (struct v4l2_format *) arg, NULL, NULL);
	case VIDIOC_ENUM_FRAMESIZES:
		return ov9740_enum_fmsize(client, (struct v4l2_frmsizeenum *) arg);
	case VIDIOC_ENUM_FRAMEINTERVALS:
		return ov9740_enum_fminterval(client, (struct v4l2_frmivalenum *) arg);
	case VIDIOC_S_FMT:
		return ov9740_s_fmt(client, (struct v4l2_format *) arg);
	case VIDIOC_QUERYCTRL:
		return ov9740_q_ctrl(client, (struct v4l2_queryctrl *) arg);
	case VIDIOC_S_CTRL:
		return ov9740_s_ctrl(client, (struct v4l2_control *) arg);
	case VIDIOC_G_CTRL:
		return ov9740_g_ctrl(client, (struct v4l2_control *) arg);
	case VIDIOC_S_PARM:
		return ov9740_s_parm(client, (struct v4l2_streamparm *) arg);
	case VIDIOC_G_PARM:
		return ov9740_g_parm(client, (struct v4l2_streamparm *) arg);
	case VIDIOC_S_INPUT:
		return ov9740_s_input(client, (int *) arg);
	case VIDIOC_STREAMON:
		return ov9740_streamon(client);
	case VIDIOC_STREAMOFF:
		return ov9740_streamoff(client);
#ifdef CONFIG_VIDEO_ADV_DEBUG
	case VIDIOC_DBG_G_REGISTER:
		return ov9740_get_register(client, (struct v4l2_dbg_register *) arg);
	case VIDIOC_DBG_S_REGISTER:
		return ov9740_set_register(client, (struct v4l2_dbg_register *) arg);
	default:
		break;
#endif
	}

	return -EINVAL;
}

static const struct i2c_device_id ov9740_idtable[] = {
	{ "ov9740", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, ov9740_idtable);

static struct i2c_driver ov9740_driver = {
	.driver = {
		.name = "ov9740",
	},
	.id_table = ov9740_idtable,
	.command  = ov9740_command,
	.probe    = ov9740_probe,
	.remove   = ov9740_remove,
};

static int __init ov9740_mod_init(void)
{
	return i2c_add_driver(&ov9740_driver);
}

static void __exit ov9740_mod_exit(void)
{
	i2c_del_driver(&ov9740_driver);
}

late_initcall(ov9740_mod_init);
module_exit(ov9740_mod_exit);

