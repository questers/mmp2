/*
 * OmniVision ov5642 sensor driver
 *
 * Copyright (C) 2009-2010 Marvell International Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Created:  2009 Zhangfei Gao <zgao6@marvell.com>
 * Modified: 2010 Albert Wang <twang13@marvell.com>
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/videodev.h>
#include <linux/i2c.h>
#include <linux/clk.h>
#include <media/v4l2-common.h>
#include <media/v4l2-chip-ident.h>
#include <mach/hardware.h>
#include <mach/camera.h>
#include <mach/mfp.h>
#include <mach/mfp-mmp2.h>
#include <mach/mmp2_plat_ver.h>

#include "pxa688_camera.h"
#include "ov5642.h"

MODULE_AUTHOR("Zhangfei Gao <zgao6@marvell.com>");
MODULE_DESCRIPTION("OmniVision ov5642 sensor driver");
MODULE_LICENSE("GPL");

/* The maximum allowed times for repeating OV5642 detection */
#define MAX_DETECT_NUM	3

/* Non-CameraEngine Usage */
//#define NON_CAMERA_ENGINE

/* Debug OV5642 register Read/Write */
//#define OV5642_DEBUG

#define setbit(reg, mask) \
do{ \
	unsigned char val; \
	ov5642_read(client, reg, &val); \
	/* printk(KERN_NOTICE"Reg0x%4X: 0x%2X -> 0x%2X\n", reg, val, (val)|(mask)); */ \
	val |= (mask); \
	ov5642_write(client, reg, val); \
}while (0)

#define clrbit(reg, mask) \
do{ \
	unsigned char val; \
	ov5642_read(client, reg, &val);	\
	/* printk(KERN_NOTICE"Reg0x%4X: 0x%2X -> 0x%2X\n", reg, val, (val)&(~(mask))); */ \
	val &= (~(mask)); \
	ov5642_write(client, reg, val);	\
}while (0)

static int awb_flag = 0;	/* Capture AWB Gain Restore flag */

/*
 * Information we maintain about a known sensor.
 */
enum flash_arg_format {
	FLASH_ARG_TIME_OFF	= 0,
	FLASH_ARG_MODE_0	= 0,
	FLASH_ARG_MODE_1,
	FLASH_ARG_MODE_2,
	FLASH_ARG_MODE_3,
	FLASH_ARG_MODE_EVER	= FLASH_ARG_MODE_0, /* Always on till send off  */
	FLASH_ARG_MODE_TIMED	= FLASH_ARG_MODE_1, /* On for some time */
	FLASH_ARG_MODE_REPEAT	= FLASH_ARG_MODE_2, /* Repeatedlly on */
	FLASH_ARG_MODE_BLINK	= FLASH_ARG_MODE_3, /* On for some jeffies defined by H/W */
	FLASH_ARG_MODE_MAX,	/* Equal or greater mode considered invalid */
	FLASH_ARG_LUMI_OFF	= 0, /* Flash off */
	FLASH_ARG_LUMI_FULL,
	FLASH_ARG_LUMI_DIM1,	/* One fifth luminance*/
	FLASH_ARG_LUMI_DIM2,	/* One third luminance, not supported in H/W configuration */
	FLASH_ARG_LUMI_MAX,	/* Equal or greater luminance considered invalid */
};

struct cam_flash_struct {
	unsigned char	mode;
/*	flash mode
 *	00 Continous flash, flash on when on req flash off when off req.
 *	01 One flash, the flash strobe is opened for a period of time specified
 *	   by high 3 bytes.
 *	10 periodic flash, the flash is on periodiclly the period is specified
 *	   by high 3 bytes.
 *	11 pulse flash, flash one time for a very short pulse, width of the
 *	   pulse is specified by bit
 */
	unsigned char	brightness;
	unsigned int 	duration;
};

struct ov5642_format_struct;  /* coming later */

struct ov5642_info {
	struct ov5642_format_struct *fmt;  /* Current format */
	unsigned char sat;		/* Saturation value */
	int hue;			/* Hue value */
	struct cam_flash_struct flash;	/* Flash strobe related info */
};

/*
 * Low-level register I/O.
 */
/* issue that OV sensor must write then read. 5642 register is 16bit!!! */
static int ov5642_read(struct i2c_client *client, u16 reg, unsigned char *value)
{
	int ret = 0;
	u8 data;
	u8 address[2];
	address[0] = reg>>8;
	address[1] = reg;
	ret = i2c_smbus_write_byte_data(client, address[0], address[1]);
	if (ret)
		return ret;
	data = i2c_smbus_read_byte(client);
	*value = data;
	return 0;
}

static int ov5642_write(struct i2c_client *client, u16 reg, unsigned char value)
{
#ifdef OV5642_DEBUG
	unsigned char verify = 0;
#endif
	int ret = 0;
	u8 data[3];
	data[0] = reg>>8;
	data[1] = reg;
	data[2]=  value;
	ret = i2c_master_send(client, data, 3);
	if (ret < 0)
		return ret;
	if (reg == REG_SYSCTRL && (value & SW_RESET)) {
		printk("camera: a s/w reset triggered, may result in register value lost!\n");
		msleep(2); /* Wait for reset to run, OV spec suggest 2 ~ 5 ms */
	}
#ifdef OV5642_DEBUG
	/* verify write regs for debugging */
	ret = ov5642_read(client, reg, &verify);
	if (ret)
		return ret;
	if (verify != value && reg != 0x3008 && reg != 0x5316)
		printk(KERN_ERR "ov5642 register 0x%x: write = 0x%x, but read = 0x%x, return value = %d\n", reg, value, verify, ret);
#endif
	return 0;
}

/*
 * Write a list of register settings; ff/ff stops the process.
 */
static int ov5642_write_array(struct i2c_client *client, struct regval_list *vals)
{
	while (vals->reg_num != 0xffff || vals->value != 0xff) {
		int ret = ov5642_write(client, vals->reg_num, vals->value);
		if (ret < 0)
			return ret;
		vals++;
	}
	return 0;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int ov5642_g_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return ov5642_read(client, (u16)reg->reg, (unsigned char *)&(reg->val));
}

static int ov5642_s_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return ov5642_write(client, (u16)reg->reg, (unsigned char)reg->val);
}
#endif

static int ov5642_detect(struct i2c_client *client)
{
	unsigned char v = 0;
	int ret = 0;

	/*
	 * no MID register found. OK, we know we have an OmniVision chip...but which one?
	 */
	ret = ov5642_read(client, REG_PIDH, &v);
	if (ret < 0)
		return -ENXIO;
	if (v != 0x56)
		return -ENODEV;
	else
		printk(KERN_NOTICE "camera: ov5642 detect 0x%x\n", v);

	ret = ov5642_read(client, REG_PIDL, &v);
	if (ret < 0)
		return -ENXIO;
	if (v != 0x42)
		return -ENODEV;
	else
		printk(KERN_NOTICE "camera: ov5642 detect 0x%x\n", v);

	return 0;
}

/*
 * Store information about the video data format.  The color matrix
 * is deeply tied into the format, so keep the relevant values here.
 * The magic matrix nubmers come from OmniVision.
 */
static struct ov5642_format_struct {
	__u8 *desc;
	__u32 pixelformat;
	struct regval_list *regs;
	int bpp;   /* bits per pixel */
} ov5642_formats[] = {
	{
		.desc		= "YUYV422 packet",
		.pixelformat	= V4L2_PIX_FMT_UYVY,
		.regs		= ov5642_fmt_yuv422,
		.bpp		= 16,
	},
	{
		.desc		= "YUYV422 planar",
		.pixelformat	= V4L2_PIX_FMT_YUV422P,
		.regs		= ov5642_fmt_yuv422,
		.bpp		= 16,
	},
	{
		.desc		= "YUYV 4:2:0",
		.pixelformat	= V4L2_PIX_FMT_YUV420,
		.regs		= ov5642_fmt_yuv422,
		.bpp		= 12,
	},
	{
		.desc		= "JFIF JPEG",
		.pixelformat	= V4L2_PIX_FMT_JPEG,
		.regs		= ov5642_fmt_jpg,
		.bpp		= 16,
	},
#ifdef NON_CAMERA_ENGINE
	{
		.desc		= "RGB 565",
		.pixelformat	= V4L2_PIX_FMT_RGB565,
		.regs		= ov5642_fmt_rgb565,
		.bpp		= 16,
	},
#endif
};

#define N_OV5642_FMTS ARRAY_SIZE(ov5642_formats)

/*
 * Then there is the issue of window sizes.  Try to capture the info here.
 */
struct ov5642_win_size {
	int	width;
	int	height;
	int	fps;
	struct regval_list *regs;
};

/* Preview Size */
static struct ov5642_win_size ov5642_win_sizes[] = {
#ifdef NON_CAMERA_ENGINE
	/* QVGA v */
	{
		.width		= 240,
		.height		= 320,
		.fps		= 30,
		.regs		= ov5642_res_qvga_v,
	},
	/* HVGA v */
	{
		.width		= 320,
		.height		= 480,
		.fps		= 30,
		.regs		= ov5642_res_hvga_v,
	},
	/* VGA v */
	{
		.width		= 480,
		.height		= 640,
		.fps		= 30,
		.regs		= ov5642_res_vga_v,
	},
#endif
	/* QCIF */
	{
		.width		= 176,
		.height		= 144,
		.fps		= 30,
		.regs		= ov5642_res_qcif,
	},
	/* CIF */
	{
		.width		= 352,
		.height		= 288,
		.fps		= 30,
		.regs		= ov5642_res_cif,
	},
	/* QVGA */
	{
		.width		= 320,
		.height		= 240,
		.fps		= 30,
		.regs		= ov5642_res_qvga,
	},
	/* HVGA */
	{
		.width		= 480,
		.height		= 320,
		.fps		= 30,
		.regs		= ov5642_res_hvga,
	},
	/* VGA */
	{
		.width		= 640,
		.height		= 480,
		.fps		= 30,
		.regs		= ov5642_res_vga,
	},
	/* WVGA */
	{
		.width		= 800,
		.height		= 480,
		.fps		= 30,
		.regs		= ov5642_res_wvga,
	},
	/* 720P */
	{
		.width		= 1280,
		.height		= 720,
		.fps		= 30,
		.regs		= ov5642_res_720P,
	},
	/* 1080P */
	{
		.width		= 1920,
		.height		= 1080,
		.fps		= 30,
		.regs		= ov5642_res_1080P,
	},
};

#define N_WIN_SIZES (ARRAY_SIZE(ov5642_win_sizes))

/* Capture JPEG Size */
static struct ov5642_win_size ov5642_win_jpg_sizes[] = {
	/* QVGA 320x240 */
	{
		.width		= 320,
		.height		= 240,
		.fps		= 10,
		.regs		= ov5642_res_qvga,
	},
	/* HVGA 480x320 */
	{
		.width		= 480,
		.height		= 320,
		.fps		= 30,
		.regs		= ov5642_res_hvga,
	},
	/* VGA 640x480 */
	{
		.width		= 640,
		.height 	= 480,
		.fps		= 10,
		.regs		= ov5642_res_vga,
	},
	/* 5M 2592x1944 */
	{
		.width		= 2592,
		.height 	= 1944,
		.fps		= 10,
		.regs		= ov5642_res_5M,
	},
};

#define N_WIN_JPEG_SIZES (ARRAY_SIZE(ov5642_win_jpg_sizes))

/*
 * Save Camera Sensor AWB RGB GAIN Values
 */
struct ov5642_awb_gain {
	__u8 *desc;
	unsigned char high;
	unsigned char low;
} ov5642_awb_gains[] = {
	{
		.desc		= "AWB Red Gain",
		.high		= 0,
		.low		= 0,
	},
	{
		.desc		= "AWB Green Gain",
		.high		= 0,
		.low		= 0,
	},
	{
		.desc		= "AWB Blue Gain",
		.high		= 0,
		.low		= 0,
	},
};

static int ov5642_q_awb(struct i2c_client *client, __s32 *value)
{
	int ret;
	unsigned char v = 0;

	ret = ov5642_read(client, REG_AWB_EN, &v);
	*value = ((v & AWB_DIS) != AWB_DIS);
	return ret;
}

static int ov5642_t_awb(struct i2c_client *client, int value)
{
	unsigned char v = 0;
	int ret;

	ret = ov5642_read(client, REG_AWB_EN, &v);
	if (!value)
		v = AWB_DIS;
	else
		v = 0x00;
	ret = ov5642_write(client, REG_AWB_EN, v);
	return ret;
}

static int ov5642_q_ae(struct i2c_client *client, __s32 *value)
{
	int ret;
	unsigned char v = 0;

	ret = ov5642_read(client, REG_AE_EN, &v);
	*value = ((v & AE_EN) == AE_EN);
	return ret;
}

static int ov5642_t_ae(struct i2c_client *client, int value)
{
	unsigned char v = 0;
	int ret;

	ret = ov5642_read(client, REG_AE_EN, &v);
	if (value)
		v |= AE_EN;
	else
		v &= ~AE_EN;
	ret += ov5642_write(client, REG_AE_EN, v);
	return ret;
}

static int ov5642_q_af(struct i2c_client *client, __s32 *value)
{
	int ret;
	unsigned char v = 0;

	ret = ov5642_read(client, REG_AF_EN, &v);
	*value = (v & AF_EN) == AF_EN;
	return ret;
}

static int ov5642_t_af(struct i2c_client *client, int value)
{
	unsigned char v = 0;
	int ret;

	ret = ov5642_read(client, REG_AF_EN, &v);
	if (value)
		v |= AF_EN;
	else
		v &= ~AF_EN;
	ret += ov5642_write(client, REG_AF_EN, v);
	return ret;
}

/* Flash related functions */
static int ov5642_q_flash_time(struct i2c_client *client, __s32 *time);
static int ov5642_t_flash_time(struct i2c_client *client, int time);
static int ov5642_q_flash_mode(struct i2c_client *client, __s32 *mode);
static int ov5642_t_flash_mode(struct i2c_client *client, int mode);
static int ov5642_q_flash_lum(struct i2c_client *client, __s32 *lum);
static int ov5642_t_flash_lum(struct i2c_client *client, int lum);

static int ov5642_q_flash_time(struct i2c_client *client, __s32 *time)
{
	struct ov5642_info *info = i2c_get_clientdata(client);
	*time = info->flash.duration;
	return *time;
}

static int ov5642_t_flash_time(struct i2c_client *client, int time)
{
	int ret = 0;
	struct ov5642_info *info = i2c_get_clientdata(client);
	if (time < 0) {
		printk(KERN_NOTICE "camera: ov5642 invalid flash duration: %d\n", time);
		return -EINVAL;
	}
	info->flash.duration = time;
	if (time == 0)
		info->flash.mode = FLASH_ARG_MODE_EVER;
	else
		info->flash.mode = FLASH_ARG_MODE_TIMED;
	/* If the flash is on, the duration change will affect the
	 * behavoir of flash immediatelly */
	if (info->flash.brightness != FLASH_ARG_LUMI_OFF)
		ret = ov5642_t_flash_lum(client, info->flash.brightness);
	return ret;
}

static int ov5642_q_flash_mode(struct i2c_client *client, __s32 *mode)
{
	struct ov5642_info *info = i2c_get_clientdata(client);
	*mode = info->flash.mode;
	return *mode;
}

static int ov5642_t_flash_mode(struct i2c_client *client, int mode)
{
	struct ov5642_info *info = i2c_get_clientdata(client);

	if (mode >= FLASH_ARG_MODE_MAX)
		return -EINVAL;
	info->flash.mode = mode;
	return 0;
}

static int ov5642_q_flash_lum(struct i2c_client *client, __s32 *lum)
{
	struct ov5642_info *info = i2c_get_clientdata(client);
	*lum = info->flash.brightness;
	return *lum;
}

static int ov5642_t_flash_lum(struct i2c_client *client, int lum)
{
	struct ov5642_info *info = i2c_get_clientdata(client);
	int ret = 0;

	if ((lum < 0) || (lum > FLASH_ARG_LUMI_MAX))
		return -EINVAL;

	if (lum == 0) {
		printk(KERN_DEBUG "Turn OFF flash\n");
		info->flash.brightness = FLASH_ARG_LUMI_OFF;
		/* Request turn OFF the flash, so ignore flash mode */
		clrbit(REG_SYS_RESET, 0x08);	// clear reset
		setbit(REG_CLK_ENABLE, 0x08);	// enable clock
		setbit(REG_PAD_OUTPUT, 0x20);	// pad output enable
		setbit(REG_FREX_MODE, 0x02);	// FREX mode
		/* Strobe control */
		ret = ov5642_write(client, REG_STRB_CTRL, 0x03);
		/* Write again to make sure strobe request end is received
		 * after other bits are saved in the reg, use LED3 mode off */
		ret |= ov5642_write(client, REG_STRB_CTRL, 0x03);
		return ret;
	}

	info->flash.brightness = lum;
	clrbit(REG_SYS_RESET, 0x08);	// clear reset
	setbit(REG_CLK_ENABLE, 0x08);	// enable clock
	setbit(REG_PAD_OUTPUT, 0x02);	// pad output enable
	setbit(REG_FREX_MODE, 0x02);	// FREX mode

	switch (info->flash.mode)
	{
		case FLASH_ARG_MODE_TIMED:
			if (info->flash.duration) {
				/* Strobe control */
				ret = ov5642_write(client, REG_STRB_CTRL, 0x03);
				/* Send strobe request */
				ret |= ov5642_write(client, REG_STRB_CTRL, 0x83);
				msleep(info->flash.duration);
				ret |= ov5642_write(client, REG_STRB_CTRL, 0x03);
				info->flash.brightness = FLASH_ARG_LUMI_OFF;
				return ret;
			}
			/* NO break here, if duration is 0, leave it ON */
		case FLASH_ARG_MODE_EVER:
			/* Strobe control */
			ret = ov5642_write(client, REG_STRB_CTRL, 0x03);
			/* Send strobe request */
			ret |= ov5642_write(client, REG_STRB_CTRL, 0x83);
			return ret;
		case FLASH_ARG_MODE_REPEAT:
			/* Strobe control */
			ret = ov5642_write(client, REG_STRB_CTRL, 0x02);
			/* Send strobe request */
			ret |= ov5642_write(client, REG_STRB_CTRL, 0x82);
			return ret;
		case FLASH_ARG_MODE_BLINK:
			/* Use ov5642 xenon flash mode */
			return ret;
		default:
			break;
	}
	return 0;
}

static int ov5642_firmware_download(struct i2c_client *client, int value)
{
	int ret, i, j;
	char data[258];

	printk("camera: ov5642 AF firmware will be downloaded!\n");
	for (i = 0; i < N_FIRMWARE_SIZES; i++) {
		data[0] = ov5642_firmware_regs[i].reg >> 8;
		data[1] = ov5642_firmware_regs[i].reg;
		for (j = 0; j < ov5642_firmware_regs[i].len; j++)
			data[j + 2] = ov5642_firmware_regs[i].value[j];

		ret = i2c_master_send(client, data, ov5642_firmware_regs[i].len + 2);
		if (ret < 0) {
			printk("camera: ov5642 download AF firmware failed with reg[%d]\n", i);
			break;
		}
	}
	return (ret > 0) ? 0 : ret;
}

static struct ov5642_control {
	struct v4l2_queryctrl qc;
	int (*query)(struct i2c_client *client, __s32 *value);
	int (*tweak)(struct i2c_client *client, int value);
} ov5642_controls[] =
{
	{
		.qc = {
			.id = V4L2_CID_FOCUS_AUTO,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "auto focus",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = ov5642_t_af,
		.query = ov5642_q_af,
	},
	{
		.qc = {
			.id = V4L2_CID_AUTO_WHITE_BALANCE,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "auto white balance",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = ov5642_t_awb,
		.query = ov5642_q_awb,
	},
	{
		.qc = {
			.id = V4L2_CID_EXPOSURE_AUTO,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "auto exposure",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = ov5642_t_ae,
		.query = ov5642_q_ae,
	},
	{
		.qc = {
			.id = V4L2_CID_FLASH_DURATION,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "flash duration",
			.minimum = 0,
			.maximum = 60000,
			.step = 1,
			.default_value = 1000,
		},
		.tweak = ov5642_t_flash_time,
		.query = ov5642_q_flash_time,
	},
		{
		.qc = {
			.id = V4L2_CID_FLASH_MODE,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "flash mode",
			.minimum = FLASH_ARG_MODE_0,
			.maximum = FLASH_ARG_MODE_MAX - 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = ov5642_t_flash_mode,
		.query = ov5642_q_flash_mode,
	},
	{
		.qc = {
			.id = V4L2_CID_FLASH_LUMINANCE,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "flash brightness",
			.minimum = FLASH_ARG_LUMI_OFF,
			.maximum = FLASH_ARG_LUMI_MAX - 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = ov5642_t_flash_lum,
		.query = ov5642_q_flash_lum,
	},
	{
		.qc = {
			.id = V4L2_CID_AF_FIRMWARE_DOWNLOAD,
		},
		.tweak = ov5642_firmware_download,
	},
};

#define N_CONTROLS (ARRAY_SIZE(ov5642_controls))

static int ov5642_querycap(struct i2c_client *client, struct v4l2_capability *argp)
{
	if (!argp) {
		printk(KERN_ERR " argp is NULL %s %d \n", __FUNCTION__, __LINE__);
		return -EINVAL;
	}
	strcpy(argp->driver, "ov5642");
	strcpy(argp->card, "MMP2");
	return 0;
}

static int ov5642_enum_fmt(struct i2c_client *client, struct v4l2_fmtdesc *fmt)
{
	struct ov5642_format_struct *ofmt;

	if (fmt->index >= N_OV5642_FMTS)
		return -EINVAL;

	ofmt = ov5642_formats + fmt->index;
	fmt->flags = 0;
	strcpy(fmt->description, ofmt->desc);
	fmt->pixelformat = ofmt->pixelformat;
	return 0;
}

static int ov5642_try_fmt(struct i2c_client *client, struct v4l2_format *fmt,
		struct ov5642_format_struct **ret_fmt,
		struct ov5642_win_size **ret_wsize)
{
	int index_fmt, index_wsize;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	for (index_fmt = 0; index_fmt < N_OV5642_FMTS; index_fmt++)
		if (ov5642_formats[index_fmt].pixelformat == pix->pixelformat)
			break;
	if (index_fmt >= N_OV5642_FMTS) {
		printk(KERN_ERR "camera: ov5642 try unsupported format!\n");
		return -EINVAL;
	}
	if (ret_fmt != NULL)
		*ret_fmt = &ov5642_formats[index_fmt];

	if (pix->field == V4L2_FIELD_ANY)
		pix->field = V4L2_FIELD_NONE;
	else if (pix->field != V4L2_FIELD_NONE) {
		printk(KERN_ERR "camera: ov5642 pixel filed should be V4l2_FIELD_NONE!\n");
		return -EINVAL;
	}

	/*
	 * Round requested image size down to the nearest
	 * we support, but not below the smallest.
	 */
	if (V4L2_PIX_FMT_JPEG == pix->pixelformat) {
		for (index_wsize = 0; index_wsize < N_WIN_JPEG_SIZES; index_wsize++)
			if (pix->width == ov5642_win_jpg_sizes[index_wsize].width
				&& pix->height == ov5642_win_jpg_sizes[index_wsize].height)
				break;
		if (index_wsize >= N_WIN_JPEG_SIZES) {
			printk(KERN_ERR"camera: ov5642 try unsupported jpeg format size!\n");
			return -EINVAL;
		}
		if (ret_wsize != NULL)
			*ret_wsize = &ov5642_win_jpg_sizes[index_wsize];
		/* for OV5642, HSYNC contains 2048bytes */
		pix->bytesperline = 2048;
	} else {
		for (index_wsize = 0; index_wsize < N_WIN_SIZES; index_wsize++)
			if (pix->width == ov5642_win_sizes[index_wsize].width
				&& pix->height == ov5642_win_sizes[index_wsize].height)
				break;
		if (index_wsize >= N_WIN_SIZES) {
			printk(KERN_ERR "camera: ov5642 try unsupported preview format size!\n");
			return -EINVAL;
		}
		if (ret_wsize != NULL)
			*ret_wsize = &ov5642_win_sizes[index_wsize];
		pix->bytesperline = pix->width*ov5642_formats[index_fmt].bpp/8;
		pix->sizeimage = pix->height*pix->bytesperline;
	}
	return 0;
}

static int ov5642_enum_fmsize(struct i2c_client *client, struct v4l2_frmsizeenum *argp)
{
	struct v4l2_frmsizeenum frmsize;

	if (copy_from_user(&frmsize, argp, sizeof(frmsize)))
		return -EFAULT;

	switch (frmsize.pixel_format) {
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420:
			if (frmsize.index == N_WIN_SIZES)
				return -EINVAL;
			else if (frmsize.index > N_WIN_SIZES) {
				printk(KERN_ERR "camera: ov5642 enum unsupported preview format size!\n");
				return -EINVAL;
			} else {
				frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
				frmsize.discrete.height = ov5642_win_sizes[frmsize.index].height;
				frmsize.discrete.width = ov5642_win_sizes[frmsize.index].width;
				break;
			}
		case V4L2_PIX_FMT_JPEG:
			if (frmsize.index == N_WIN_JPEG_SIZES)
				return -EINVAL;
			else if (frmsize.index > N_WIN_JPEG_SIZES) {
				printk(KERN_ERR "camera: ov5642 enum unsupported jpeg format size!\n");
				return -EINVAL;
			} else {
				frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
				frmsize.discrete.height = ov5642_win_jpg_sizes[frmsize.index].height;
				frmsize.discrete.width = ov5642_win_jpg_sizes[frmsize.index].width;
				break;
			}
		default:
			printk(KERN_ERR "camera: ov5642 enum format size with unsupported format!\n");
			return -EINVAL;
	}

	if (copy_to_user(argp, &frmsize, sizeof(frmsize)))
		return -EFAULT;
	return 0;
}

static int ov5642_enum_fminterval(struct i2c_client *client, struct v4l2_frmivalenum *argp)
{
	struct v4l2_frmivalenum frminterval;

	if (copy_from_user(&frminterval, argp, sizeof(frminterval)))
		return -EFAULT;

	switch (frminterval.pixel_format) {
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420:
			if (frminterval.index == N_WIN_SIZES)
				return -EINVAL;
			else if (frminterval.index > N_WIN_SIZES) {
				printk(KERN_ERR "camera: ov5642 enum unsupported preview format frame rate!\n");
				return -EINVAL;
			} else {
				frminterval.type = V4L2_FRMIVAL_TYPE_DISCRETE;
				frminterval.discrete.numerator = 1;
				frminterval.discrete.denominator = ov5642_win_sizes[frminterval.index].fps;
				break;
			}
		case V4L2_PIX_FMT_JPEG:
			if (frminterval.index == N_WIN_JPEG_SIZES)
				return -EINVAL;
			else if (frminterval.index > N_WIN_JPEG_SIZES) {
				printk(KERN_ERR "camera: ov5642 enum unsupported jpeg format frame rate!\n");
				return -EINVAL;
			} else {
				frminterval.type = V4L2_FRMIVAL_TYPE_DISCRETE;
				frminterval.discrete.numerator = 1;
				frminterval.discrete.denominator = ov5642_win_jpg_sizes[frminterval.index].fps;
				break;
			}
		default:
			printk(KERN_ERR "camera: ov5642 enum format frame rate with unsupported format!\n");
			return -EINVAL;
	}

	if (copy_to_user(argp, &frminterval, sizeof(frminterval)))
		return -EFAULT;
	return 0;
}

/*
 * Reset sensor for initializing settings
 * this function is supposed to be called only at beginning
 * call this function will result in registers value lost
 */
static int ov5642_int_reset(struct i2c_client *client)
{
	unsigned char value = 0;
	int ret = 0;

	/* S/W reset */
	ret = ov5642_read(client, REG_SYSCTRL, &value);
	if (ret)
		return ret;

	ret = ov5642_write(client, REG_SYSCTRL, value | SW_RESET);
	if (ret < 0) {
		printk(KERN_ERR "camera: ov5642 sensor s/w reset failed\n");
		return ret;
	}

	/* Wait till reset complete */
	msleep(2);
	ret = ov5642_write(client, REG_SYSCTRL, value);
	if (ret < 0)
		return ret;

	/* Initialize settings */
	ov5642_write_array(client, ov5642_yuv_default);
	ov5642_write_array(client, ov5642_fmt_yuv422);
	ov5642_write_array(client, ov5642_mipi);
	return 0;
}

static struct ov5642_control *ov5642_find_control(__u32 id)
{
	int i;

	for (i = 0; i < N_CONTROLS; i++)
		if (ov5642_controls[i].qc.id == id)
			return ov5642_controls + i;
	return NULL;
}

static int ov5642_s_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct ov5642_control *octrl = ov5642_find_control(ctrl->id);
	int ret = 0;

	if (octrl == NULL)
		return -EINVAL;
	ret =  octrl->tweak(client, ctrl->value);
	if (ret >= 0)
		return 0;
	return ret;
}

static int ov5642_q_ctrl(struct i2c_client *client, struct v4l2_queryctrl *qc)
{
	struct ov5642_control *ctrl = ov5642_find_control(qc->id);

	if (ctrl == NULL)
		return -EINVAL;
	*qc = ctrl->qc;
	return 0;
}

static int ov5642_g_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct ov5642_control *octrl = ov5642_find_control(ctrl->id);
	int ret = 0;

	if (octrl == NULL)
		return -EINVAL;
	ret = octrl->query(client, &ctrl->value);
	if (ret >= 0)
		return 0;
	return ret;
}

/*
 * Set a format.
 */
static int ov5642_s_fmt(struct i2c_client *client, struct v4l2_format *fmt)
{
	int ret = 0;
	unsigned char val;
	struct ov5642_format_struct *ovfmt = NULL;
	struct ov5642_win_size *wsize = NULL;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;

	ret = ov5642_try_fmt(client, fmt, &ovfmt, &wsize);
	if (ret)
		return ret;

	/* sensor resume from software power down mode */
	ov5642_read(client, REG_SYSCTRL, &val);
	val &= ~0x40;
	ov5642_write(client, REG_SYSCTRL, val);

	switch (pix->pixelformat) {
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420:
			/* High resolution, 1920x1080(1080p) */
			if (pix->width == 1920)
				ov5642_write_array(client, ov5642_yuv_1080p);
			/* High resolution, 1280x720(720p) */
			else if (pix->width == 1280)
				ov5642_write_array(client, ov5642_yuv_720p);
			/* Low resolution, 640x480(480p) or smaller size */
			else
				ov5642_write_array(client, ov5642_yuv_480p);
			ov5642_write_array(client, wsize->regs);
			awb_flag = 0;
			break;
		case V4L2_PIX_FMT_JPEG:
			/* 2592x1944(5M), 640x480(VGA), 480x320(HVGA), 320x240(QVGA) */
			ov5642_write_array(client, ov5642_jpg_default);
			ov5642_write_array(client, wsize->regs);
			awb_flag = 1;
			break;
		default:
			printk("camera: ov5642 set unsupported pixel format!\n");
			ret = -EINVAL;
			break;
	}

	/* frontal camera sensor on brownstone board */
	if (board_is_mmp2_brownstone_rev1() || board_is_mmp2_brownstone_rev2() || board_is_mmp2_brownstone_rev4()) {
		/* need clear sensor mirror and flip */
		ov5642_read(client, REG_TIMINGCTRL, &val);
		val &= ~0x60;
		ov5642_write(client, REG_TIMINGCTRL, val);

		ov5642_read(client, REG_ARRAYCTRL, &val);
		val |= 0x20;
		ov5642_write(client, REG_ARRAYCTRL, val);
	}

	/* sensor enter software power down mode */
	ov5642_read(client, REG_SYSCTRL, &val);
	val |= 0x40;
	ov5642_write(client, REG_SYSCTRL, val);

	return ret;
}

/*
 * Implement G/S_PARM.  There is a "high quality" mode we could try
 * to do someday; for now, we just do the frame rate tweak.
 */
static int ov5642_g_parm(struct i2c_client *client, struct v4l2_streamparm *parms)
{
	return 0;
}

static int ov5642_s_parm(struct i2c_client *client, struct v4l2_streamparm *parms)
{
	return 0;
}

static int ov5642_s_input(struct i2c_client *client, int *id)
{
	return 0;
}

int ov5642_sensor_on(struct i2c_client *client)
{
	struct sensor_platform_data *pdata;
	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_OPEN, 1, pdata->eco_flag, pdata->sensor_flag);
	printk(KERN_NOTICE "camera: ov5642 sensor power on!\n");
	return 0;
}

int ov5642_sensor_off(struct i2c_client *client)
{

	struct sensor_platform_data *pdata;
	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_CLOSE, 1, pdata->eco_flag, pdata->sensor_flag);
	printk(KERN_NOTICE "camera: ov5642 sensor power off!\n");
	return 0;
}

int ov5642_bridge(struct i2c_client *client, struct v4l2_format *fmt)
{
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	ov5642_write_array(client, ov5642_bridge_mode_start);
	if ((pix->width == 176) && (pix->height == 144))
		ov5642_write_array(client, ov5642_bridge_res_qcif);
	else if ((pix->width == 320) && (pix->height == 240))
		ov5642_write_array(client, ov5642_bridge_res_qvga);
	else if ((pix->width == 640) && (pix->height == 480))
		ov5642_write_array(client, ov5642_bridge_res_vga);
#ifdef NON_CAMERA_ENGINE
	else if ((pix->width == 144) && (pix->height == 176))
		ov5642_write_array(client, ov5642_bridge_res_qcif_v);
	else if ((pix->width == 240) && (pix->height == 320))
		ov5642_write_array(client, ov5642_bridge_res_qvga_v);
	else if ((pix->width == 480) && (pix->height == 640))
		ov5642_write_array(client, ov5642_bridge_res_vga_v);
#endif
	else
		printk(KERN_NOTICE "camera: ov5642 set unsupported bridge format!\n");
	ov5642_write_array(client, ov5642_bridge_mode_end);
	msleep(1);
	return 0;
}

/*
 * Basic i2c stuff.
 */
static int __devinit ov5642_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int ret, i, eco = CAMERA_ECO_ON, sensor = SENSOR_OVT;
	struct ov5642_info *info;
	struct sensor_platform_data *pdata;

	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_OPEN, pdata->id, eco, sensor);
	pdata->eco_set(eco);

	/*
	 * Set up our info structure.
	 */
	info = kzalloc(sizeof (struct ov5642_info), GFP_KERNEL);
	if (!info) {
		ret = -ENOMEM;
		goto out_free;
	}
	info->fmt = &ov5642_formats[1];
	info->sat = 128; /* Review this */
	info->flash.brightness = FLASH_ARG_LUMI_OFF;
	info->flash.duration = FLASH_ARG_TIME_OFF;
	info->flash.mode = FLASH_ARG_MODE_EVER;
	i2c_set_clientdata(client, info);
	msleep(10);

//	printk(KERN_NOTICE "APBC_TWSI3_CLK_RST Value=0x%x\n", *(unsigned long *)0xFE01500C);	// TWSI3_CLK value

	/*
	 * Make sure it's an ov5642
	 * Need try Camera HW ECO firstly on Brownstone REV3/REV4
	 */
	if (board_is_mmp2_brownstone_rev2() || board_is_mmp2_brownstone_rev4()) {
		for (i = MAX_DETECT_NUM; i > 0; --i) {
			ret = ov5642_detect(client);
			if (!ret) {
				printk(KERN_NOTICE "camera: OmniVision 5642 sensor detected!\n");
				pdata->eco_flag = eco;
				pdata->sensor_flag = sensor;
				goto label_sensor_detected;
			}

			if (ret == -ENXIO)
				goto label_eco_detected;
			else
				printk(KERN_ERR "camera: OmniVision 5642 sensor detect failure with Camera ECO, will retry %d times!\n", i);
		}
		/* NO OV5642 detected within limited loops */
		printk(KERN_NOTICE "camera: Abort retry, failed to detect OmniVision 5642 sensor!\n");
		ret = -ENODEV;
		goto out_free_info;
	}

label_eco_detected:
	eco = CAMERA_ECO_OFF;
	pdata->power_set(SENSOR_CLOSE, pdata->id, eco, sensor);
	pdata->power_set(SENSOR_OPEN, pdata->id, eco, sensor);
	for (i = MAX_DETECT_NUM; i > 0; --i) {
		ret = ov5642_detect(client);
		if (!ret) {
			printk(KERN_NOTICE "camera: OmniVision 5642 sensor detected!\n");
			pdata->eco_flag = eco;
			pdata->sensor_flag = sensor;
			goto label_sensor_detected;
		}
		if (ret == -ENXIO)
			goto out_free_info;
		else
			printk(KERN_ERR "camera: OmniVision 5642 sensor detect failure, will retry %d times!\n", i);
	}
	/* NO OV5642 detected within limited loops */
	printk(KERN_NOTICE "camera: Abort retry, failed to detect OmniVision 5642 sensor!\n");
	ret = -ENODEV;
	goto out_free_info;

label_sensor_detected:
	ccic_sensor_attach(client);
	ret = 0;
	goto out_free;

out_free_info:
	kfree(info);

out_free:
	pdata->power_set(SENSOR_CLOSE, pdata->id, eco, sensor);
	return ret;
}

static int ov5642_remove(struct i2c_client *client)
{
	struct ov5642_info *info;
	struct sensor_platform_data *pdata;

	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_CLOSE, pdata->id, pdata->eco_flag, pdata->sensor_flag);
	ccic_sensor_detach(client);
	info = i2c_get_clientdata(client);
	i2c_set_clientdata(client, NULL);
	kfree(info);
	return 0;
}

static int ov5642_streamon(struct i2c_client *client)
{
	unsigned char val;

	/* sensor resume from software power down mode */
	ov5642_read(client, REG_SYSCTRL, &val);
	val &= ~0x40;
	ov5642_write(client, REG_SYSCTRL, val);

	/*
	 * Take over AWB jobs from Camera Engine for Capture mode
	 */
	if (awb_flag) {
		/* Set AWB Current Value to AWB GAIN REGs */
		ov5642_write(client, REG_AWBCTRL, 0x83);	/* Set AWB GAIN to manual mode */
		ov5642_write(client, REG_AWB_GAIN_RH, ov5642_awb_gains[0].high);
		ov5642_write(client, REG_AWB_GAIN_RL, ov5642_awb_gains[0].low);
		ov5642_write(client, REG_AWB_GAIN_GH, ov5642_awb_gains[1].high);
		ov5642_write(client, REG_AWB_GAIN_GL, ov5642_awb_gains[1].low);
		ov5642_write(client, REG_AWB_GAIN_BH, ov5642_awb_gains[2].high);
		ov5642_write(client, REG_AWB_GAIN_BL, ov5642_awb_gains[2].low);
	} else
		ov5642_write(client, REG_AWBCTRL, 0x03);	/* Set AWB GAIN to auto mode */

	return 0;
}

static int ov5642_streamoff(struct i2c_client *client)
{
	unsigned char val;

	if (!awb_flag) {
		/* Get AWB Gain Values from Preview for Capture */
		ov5642_read(client, REG_AWB_CUR_RH, &val);
		ov5642_awb_gains[0].high = val & 0x0f;
		ov5642_read(client, REG_AWB_CUR_RL, &val);
		ov5642_awb_gains[0].low = val;
		ov5642_read(client, REG_AWB_CUR_GH, &val);
		ov5642_awb_gains[1].high = val & 0x0f;
		ov5642_read(client, REG_AWB_CUR_GL, &val);
		ov5642_awb_gains[1].low = val;
		ov5642_read(client, REG_AWB_CUR_BH, &val);
		ov5642_awb_gains[2].high = val & 0x0f;
		ov5642_read(client, REG_AWB_CUR_BL, &val);
		ov5642_awb_gains[2].low = val;
	}

	/* sensor enter software power down mode */
	ov5642_read(client, REG_SYSCTRL, &val);
	val |= 0x40;
	ov5642_write(client, REG_SYSCTRL, val);

#ifdef NON_CAMERA_ENGINE
	/* sensor stop output after one frame finished */
	ov5642_read(client, REG_FRAMECTRL, &val);
	val |= 0x01;
	ov5642_write(client, REG_FRAMECTRL, val);

	/* sensor enter suspend mode */
	ov5642_read(client, REG_MIPICTRL, &val);
	val |= 0x02;
	ov5642_write(client, REG_MIPICTRL, val);

	msleep(200);
#endif
	return 0;
}

static int ov5642_command(struct i2c_client *client, unsigned int cmd, void *arg)
{
	switch (cmd) {
		case VIDIOC_DBG_G_CHIP_IDENT:
			return v4l2_chip_ident_i2c_client(client, arg, V4L2_IDENT_OV5642, 0);
		case VIDIOC_INT_RESET:
			return ov5642_int_reset(client);
		case VIDIOC_QUERYCAP:
			return ov5642_querycap(client, (struct v4l2_capability *) arg);
		case VIDIOC_ENUM_FMT:
			return ov5642_enum_fmt(client, (struct v4l2_fmtdesc *) arg);
		case VIDIOC_TRY_FMT:
			return ov5642_try_fmt(client, (struct v4l2_format *) arg, NULL, NULL);
		case VIDIOC_ENUM_FRAMESIZES:
			return ov5642_enum_fmsize(client, (struct v4l2_frmsizeenum *) arg);
		case VIDIOC_ENUM_FRAMEINTERVALS:
			return ov5642_enum_fminterval(client, (struct v4l2_frmivalenum *) arg);
		case VIDIOC_S_FMT:
			return ov5642_s_fmt(client, (struct v4l2_format *) arg);
		case VIDIOC_QUERYCTRL:
			return ov5642_q_ctrl(client, (struct v4l2_queryctrl *) arg);
		case VIDIOC_S_CTRL:
			return ov5642_s_ctrl(client, (struct v4l2_control *) arg);
		case VIDIOC_G_CTRL:
			return ov5642_g_ctrl(client, (struct v4l2_control *) arg);
		case VIDIOC_S_PARM:
			return ov5642_s_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_G_PARM:
			return ov5642_g_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_S_INPUT:
			return ov5642_s_input(client, (int *) arg);
		case VIDIOC_STREAMON:
			return ov5642_streamon(client);
		case VIDIOC_STREAMOFF:
			return ov5642_streamoff(client);
		case VIDIOC_SET_BRIDGE:
			ov5642_sensor_on(client);
			msleep(1);
			return	ov5642_bridge(client, arg);
#ifdef CONFIG_VIDEO_ADV_DEBUG
		case VIDIOC_DBG_G_REGISTER:
			return ov5642_g_register(client, (struct v4l2_dbg_register *) arg);
		case VIDIOC_DBG_S_REGISTER:
			return ov5642_s_register(client, (struct v4l2_dbg_register *) arg);
		default:
			break;
#endif
	}

	return -EINVAL;
}

static struct i2c_device_id ov5642_idtable[] = {
	{ "ov5642", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, ov5642_idtable);

static struct i2c_driver ov5642_driver = {
	.driver = {
		.name	= "ov5642",
	},
	.id_table       = ov5642_idtable,
	.command	= ov5642_command,
	.probe		= ov5642_probe,
	.remove		= ov5642_remove,
};

/*
 * Module initialization
 */
static int __init ov5642_mod_init(void)
{
	int ret = 0;
	ret = i2c_add_driver(&ov5642_driver);
	return ret;
}

static void __exit ov5642_mod_exit(void)
{
	i2c_del_driver(&ov5642_driver);
}

late_initcall(ov5642_mod_init);
module_exit(ov5642_mod_exit);

