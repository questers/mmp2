/*
 * Galaxycore gc2015 sensor driver
 *
 * Copyright (C) 2011 Marvell International Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Created:  2011 Albert Wang <twang13@marvell.com>
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/videodev.h>
#include <linux/i2c.h>
#include <linux/clk.h>
#include <media/v4l2-common.h>
#include <media/v4l2-chip-ident.h>
#include <mach/hardware.h>
#include <mach/camera.h>

#include "pxa688_camera.h"
#include "gc2015.h"

MODULE_AUTHOR("Albert Wang <twang13@marvell.com>");
MODULE_DESCRIPTION("Galaxycore gc2015 sensor driver");
MODULE_LICENSE("GPL");

/* The maximum allowed times for repeating GC2015 detection */
#define MAX_DETECT_NUM	3

struct gc2015_resolution {
	unsigned int width;
	unsigned int height;
	int fps;
	struct gc2015_reg *regs;
};

static struct gc2015_resolution gc2015_resolutions[] = {
	/* QCIF */
	{
		.width	= 176,
		.height	= 144,
		.fps	= 20,
		.regs	= gc2015_res_qcif,
	},
	/* CIF */
	{
		.width	= 352,
		.height	= 288,
		.fps	= 20,
		.regs	= gc2015_res_cif,
	},
	/* QHVGA */
	{
		.width	= 240,
		.height	= 160,
		.fps	= 20,
		.regs	= gc2015_res_qhvga,
	},
	/* QVGA */
	{
		.width	= 320,
		.height	= 240,
		.fps	= 20,
		.regs	= gc2015_res_qvga,
	},
	/* VGA */
	{
		.width	= 640,
		.height	= 480,
		.fps	= 20,
		.regs	= gc2015_res_vga,
	},
	/* WVGA */
	{
		.width	= 800,
		.height	= 480,
		.fps	= 20,
		.regs	= gc2015_res_wvga,
	},
	/* 720P */
	{
		.width	= 1280,
		.height	= 720,
		.fps	= 10,
		.regs	= gc2015_res_720p,
	},
	/* 2M */
	{
		.width	= 1600,
		.height	= 1200,
		.fps	= 10,
		.regs	= gc2015_res_2m,
	},
};

#define N_GC2015_RES ARRAY_SIZE(gc2015_resolutions)

static struct gc2015_format_struct {
	__u8 *desc;
	__u32 pixelformat;
	struct gc2015_reg *regs;
	int bpp;   /* bits per pixel */
} gc2015_formats[] = {
	{
		.desc		= "YUYV422 planar",
		.pixelformat	= V4L2_PIX_FMT_YUV422P,
		.regs		= gc2015_fmt_yuv422,
		.bpp		= 16,
	},
	{
		.desc		= "YUV420 planar",
		.pixelformat	= V4L2_PIX_FMT_YUV420,
		.regs		= gc2015_fmt_yuv422,
		.bpp		= 12,
	},
	{
		.desc		= "UYVY422 packet",
		.pixelformat	= V4L2_PIX_FMT_UYVY,
		.regs		= gc2015_fmt_yuv422,
		.bpp		= 16,
	},
};

#define N_GC2015_FMTS ARRAY_SIZE(gc2015_formats)

/* Read a register */
static int gc2015_reg_read(struct i2c_client *client, u8 reg, u8 *val)
{
	int ret = 0;
	struct i2c_msg msg[] = {
		{
			.addr	= client->addr,
			.flags	= 0,
			.len	= 1,
			.buf	= (u8 *)&reg,
		},
		{
			.addr	= client->addr,
			.flags	= I2C_M_RD,
			.len	= 1,
			.buf	= val,
		},
	};

	ret = i2c_transfer(client->adapter, msg, 2);
	if (ret < 0) {
		dev_err(&client->dev, "camera: gc2015 failed to read register 0x%02x!\n", reg);
		return ret;
	}

	return 0;
}

/* Write a register */
static int gc2015_reg_write(struct i2c_client *client, u8 reg, u8 val)
{
	int ret = 0;
	struct {
		u8 reg;
		u8 val;
	} __packed buf = {
		.reg	= reg,
		.val	= val,
	};

	struct i2c_msg msg = {
		.addr	= client->addr,
		.flags	= 0,
		.len	= 2,
		.buf	= (u8 *)&buf,
	};

	ret = i2c_transfer(client->adapter, &msg, 1);
	if (ret < 0) {
		dev_err(&client->dev, "camera: gc2015 failed to write register 0x%02x!\n", reg);
		return ret;
	}

	if (reg == SOFTWARE_RESET && (val & 0x80)) {
		printk("camera: a s/w reset triggered, may result in register value lost!\n");
		msleep(2); /* Wait for reset to run */
	}

	return 0;
}

/* Read a register, alter its bits, write it back */
static int gc2015_reg_rmw(struct i2c_client *client, u8 reg, u8 set, u8 unset)
{
	u8 val;
	int ret = 0;

	ret = gc2015_reg_read(client, reg, &val);
	if (ret < 0) {
		dev_err(&client->dev, "camera: gc2015 [Read]-Modify-Write of register %02x failed!\n", reg);
		return ret;
	}

	val |= set;
	val &= ~unset;

	ret = gc2015_reg_write(client, reg, val);
	if (ret < 0) {
		dev_err(&client->dev, "camera: gc2015 Read-Modify-[Write] of register %02x failed!\n", reg);
		return ret;
	}

	return 0;
}

static int gc2015_reg_write_array(struct i2c_client *client, struct gc2015_reg *regarray)
{
	while (regarray->reg != 0xff || regarray->val != 0xff) {
		int ret = gc2015_reg_write(client, regarray->reg, regarray->val);
		if (ret < 0)
			return ret;
		regarray++;
	}

	return 0;
}

static int gc2015_t_vflip(struct i2c_client *client, int value)
{
	int ret = 0;

	if (value)
		ret = gc2015_reg_rmw(client, IMAGE_ORT, 0x2, 0);
	else
		ret = gc2015_reg_rmw(client, IMAGE_ORT, 0, 0x2);

	return ret;
}

static int gc2015_q_vflip(struct i2c_client *client, __s32 *value)
{
	int ret = 0;
	u8 v;

	ret = gc2015_reg_read(client, IMAGE_ORT, &v);
	*value = (v & 0x2) == 0x2;

	return ret;
}

static int gc2015_t_hflip(struct i2c_client *client, int value)
{
	int ret = 0;

	if (value)
		ret = gc2015_reg_rmw(client, IMAGE_ORT, 0x1, 0);
	else
		ret = gc2015_reg_rmw(client, IMAGE_ORT, 0, 0x1);

	return ret;
}

static int gc2015_q_hflip(struct i2c_client *client, __s32 *value)
{
	int ret = 0;
	u8 v;
	ret = gc2015_reg_read(client, IMAGE_ORT, &v);
	*value = (v & 0x1) == 0x1;

	return ret;
}

static struct gc2015_control {
	struct v4l2_queryctrl qc;
	int (*query)(struct i2c_client *client, __s32 *value);
	int (*tweak)(struct i2c_client *client, int value);
} gc2015_controls[] = {
	{
		.qc = {
			.id		= V4L2_CID_VFLIP,
			.type		= V4L2_CTRL_TYPE_BOOLEAN,
			.name		= "Flip Vertically",
			.minimum	= 0,
			.maximum	= 1,
			.step		= 1,
			.default_value	= 0,
		},
		.tweak = gc2015_t_vflip,
		.query = gc2015_q_vflip,
	},
	{
		.qc = {
			.id		= V4L2_CID_HFLIP,
			.type		= V4L2_CTRL_TYPE_BOOLEAN,
			.name		= "Flip Horizontally",
			.minimum	= 0,
			.maximum	= 1,
			.step		= 1,
			.default_value	= 0,
		},
		.tweak = gc2015_t_hflip,
		.query = gc2015_q_hflip,
	},
};

#define N_CONTROLS (ARRAY_SIZE(gc2015_controls))

/* Start/Stop streaming from the device */
static int gc2015_streamon(struct i2c_client *client)
{
	int ret;

	dev_dbg(&client->dev, "camera: gc2015 Stream On!\n");
	/* Select Page 0 Registers */
	gc2015_reg_write(client, SOFTWARE_RESET, 0x00);
	/* Start Output Data */
	ret = gc2015_reg_write(client, OUTPUT_SELECT, 0x0f);

	return ret;
}

static int gc2015_streamoff(struct i2c_client *client)
{
	int ret;

	dev_dbg(&client->dev, "camera: gc2015 Stream Off!\n");
	/* Select Page 0 Registers */
	gc2015_reg_write(client, SOFTWARE_RESET, 0x00);
	/* Stop Output Data */
	ret = gc2015_reg_write(client, OUTPUT_SELECT, 0x00);

	return ret;
}

static struct gc2015_control *gc2015_find_control(__u32 id)
{
	int i;

	for (i = 0; i < N_CONTROLS; i++)
		if (gc2015_controls[i].qc.id == id)
			return gc2015_controls + i;

	return NULL;
}

static int gc2015_g_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct gc2015_control *octrl = gc2015_find_control(ctrl->id);
	int ret = 0;

	if (octrl == NULL)
		return -EINVAL;

	ret = octrl->query(client, &ctrl->value);
	if (ret >= 0)
		return 0;

	return ret;
}

static int gc2015_s_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct gc2015_control *octrl = gc2015_find_control(ctrl->id);
	int ret = 0;

	if (octrl == NULL)
		return -EINVAL;

	ret =  octrl->tweak(client, ctrl->value);
	if (ret >= 0)
		return 0;

	return ret;
}

static int gc2015_g_parm(struct i2c_client *client, struct v4l2_streamparm *parms)
{
	return 0;
}

static int gc2015_s_parm(struct i2c_client *client, struct v4l2_streamparm *parms)
{
	return 0;
}

static int gc2015_s_input(struct i2c_client *client, int *id)
{
	return 0;
}

static int gc2015_querycap(struct i2c_client *client, struct v4l2_capability *argp)
{
	if (!argp) {
		printk(KERN_ERR " argp is NULL %s %d \n", __FUNCTION__, __LINE__);
		return -EINVAL;
	}
	strcpy(argp->driver, "gc2015");
	strcpy(argp->card, "MMP2");
	return 0;
}

static int gc2015_enum_fmt(struct i2c_client *client, struct v4l2_fmtdesc *fmt)
{
	struct gc2015_format_struct *ofmt;

	if (fmt->index >= N_GC2015_FMTS)
		return -EINVAL;

	ofmt = gc2015_formats + fmt->index;
	fmt->flags = 0;
	strcpy(fmt->description, ofmt->desc);
	fmt->pixelformat = ofmt->pixelformat;
	return 0;
}

static int gc2015_int_reset(struct i2c_client *client)
{
	int ret = 0;

	/* Software Reset */
	ret = gc2015_reg_write(client, SOFTWARE_RESET, 0x80);
	msleep(2);

	/* Initialize settings */
	ret = gc2015_reg_write_array(client, gc2015_regs_defaults);
	if (ret < 0)
		return ret;
	return 0;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int gc2015_get_register(struct i2c_client *client, struct v4l2_dbg_register *reg)
{
	int ret;
	u8 val;

	if (reg->reg & ~0xff)
		return -EINVAL;

	reg->size = 2;

	ret = gc2015_reg_read(client, reg->reg, &val);
	if (ret)
		return ret;

	reg->val = (__u64)val;

	return ret;
}

static int gc2015_set_register(struct i2c_client *client, struct v4l2_dbg_register *reg)
{
	if (reg->reg & ~0xff || reg->val & ~0xff)
		return -EINVAL;

	return gc2015_reg_write(client, reg->reg, reg->val);
}
#endif

static int gc2015_try_fmt(struct i2c_client *client, struct v4l2_format *fmt,
		struct gc2015_format_struct **ret_fmt,
		struct gc2015_resolution **ret_wsize)
{
	int index_fmt, index_wsize;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	for (index_fmt = 0; index_fmt < N_GC2015_FMTS; index_fmt++)
		if (gc2015_formats[index_fmt].pixelformat == pix->pixelformat)
			break;

	if (index_fmt >= N_GC2015_FMTS) {
		printk(KERN_ERR "camera: gc2015 try unsupported format!\n");
		return -EINVAL;
	}

	if (ret_fmt != NULL)
		*ret_fmt = &gc2015_formats[index_fmt];

	if (pix->field == V4L2_FIELD_ANY)
		pix->field = V4L2_FIELD_NONE;
	else if (pix->field != V4L2_FIELD_NONE) {
		printk(KERN_ERR "camera: gc2015 pixel filed should be V4l2_FIELD_NONE!\n");
		return -EINVAL;
	}

	for (index_wsize = 0; index_wsize < N_GC2015_RES; index_wsize++)
		if (pix->width == gc2015_resolutions[index_wsize].width && pix->height == gc2015_resolutions[index_wsize].height)
			break;

	if (index_wsize >= N_GC2015_RES) {
		printk(KERN_ERR "camera: gc2015 try unsupported preview format size!\n");
		return -EINVAL;
	}

	if (ret_wsize != NULL)
		*ret_wsize = &gc2015_resolutions[index_wsize];
	pix->bytesperline = pix->width*gc2015_formats[index_fmt].bpp/8;
	pix->sizeimage = pix->height*pix->bytesperline;

	return 0;
}

static int gc2015_enum_fmsize(struct i2c_client *client, struct v4l2_frmsizeenum *argp)
{
	struct v4l2_frmsizeenum frmsize;

	if (copy_from_user(&frmsize, argp, sizeof(frmsize)))
		return -EFAULT;

	switch (frmsize.pixel_format) {
	case V4L2_PIX_FMT_UYVY:
	case V4L2_PIX_FMT_YUV422P:
	case V4L2_PIX_FMT_YUV420:
		if (frmsize.index == N_GC2015_RES)
			return -EINVAL;
		else if (frmsize.index > N_GC2015_RES) {
			printk(KERN_ERR "camera: gc2015 enum unsupported preview format size!\n");
			return -EINVAL;
		} else {
			frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
			frmsize.discrete.height = gc2015_resolutions[frmsize.index].height;
			frmsize.discrete.width = gc2015_resolutions[frmsize.index].width;
			break;
		}
	default:
		printk(KERN_ERR "camera: gc2015 enum format size with unsupported format!\n");
		return -EINVAL;
	}

	if (copy_to_user(argp, &frmsize, sizeof(frmsize)))
		return -EFAULT;
	return 0;
}

static int gc2015_enum_fminterval(struct i2c_client *client, struct v4l2_frmivalenum *argp)
{
	struct v4l2_frmivalenum frminterval;

	if (copy_from_user(&frminterval, argp, sizeof(frminterval)))
		return -EFAULT;

	switch (frminterval.pixel_format) {
	case V4L2_PIX_FMT_UYVY:
	case V4L2_PIX_FMT_YUV422P:
	case V4L2_PIX_FMT_YUV420:
		if (frminterval.index == N_GC2015_RES)
			return -EINVAL;
		else if (frminterval.index > N_GC2015_RES) {
			printk(KERN_ERR "camera: gc2015 enum unsupported preview format frame rate!\n");
			return -EINVAL;
		} else {
			frminterval.type = V4L2_FRMIVAL_TYPE_DISCRETE;
			frminterval.discrete.numerator = 1;
			frminterval.discrete.denominator = gc2015_resolutions[frminterval.index].fps;
			break;
		}
	default:
		printk(KERN_ERR "camera: gc2015 enum format frame rate with unsupported format!\n");
		return -EINVAL;
	}

	if (copy_to_user(argp, &frminterval, sizeof(frminterval)))
		return -EFAULT;
	return 0;
}

static int gc2015_s_fmt(struct i2c_client *client, struct v4l2_format *fmt)
{
	int ret = 0;
	struct gc2015_format_struct *ovfmt = NULL;
	struct gc2015_resolution *wsize = NULL;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;

	ret = gc2015_try_fmt(client, fmt, &ovfmt, &wsize);
	if (ret)
		return ret;

	/* select register configuration for given resolution */
	switch (pix->pixelformat) {
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420:
			ret = gc2015_reg_write_array(client, wsize->regs);
			if (pix->width > 800)
				ret = gc2015_reg_write_array(client, gc2015_regs_high);
			else
				ret = gc2015_reg_write_array(client, gc2015_regs_low);
			break;
		default:
			printk("camera: gc2015 set unsupported pixel format!\n");
			ret = -EINVAL;
			break;
	}

	return ret;
}

static int gc2015_q_ctrl(struct i2c_client *client, struct v4l2_queryctrl *qc)
{
	struct gc2015_control *ctrl = gc2015_find_control(qc->id);

	if (ctrl == NULL)
		return -EINVAL;
	*qc = ctrl->qc;
	return 0;
}

static int gc2015_detect(struct i2c_client *client)
{
	struct gc2015_priv *priv = i2c_get_clientdata(client);
	u8 modelhi, modello;
	int ret;

	/*
	 * check and show product ID and manufacturer ID
	 */
	ret = gc2015_reg_read(client, MODEL_ID_HI, &modelhi);
	if (ret < 0)
		goto err;

	ret = gc2015_reg_read(client, MODEL_ID_LO, &modello);
	if (ret < 0)
		goto err;

	priv->model = (modelhi << 8) | modello;

	if (priv->model != 0x2005) {
		ret = -ENODEV;
		goto err;
	}

	priv->ident = V4L2_IDENT_GC2015;
	printk("camera: gc2015 Model_ID = 0x%04x\n", priv->model);

err:
	return ret;
}

/*
 * i2c_driver function
 */
static int gc2015_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int i, ret, eco = CAMERA_ECO_ON, sensor = SENSOR_OVT;	/* GalaxyCore GC2015 is special case, sensor enable mode is same with OminiVision sensor */
	struct gc2015_priv *priv;
	struct sensor_platform_data *pdata;

	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_OPEN, pdata->id, eco, sensor);
	pdata->eco_set(eco);

	priv = kzalloc(sizeof(struct gc2015_priv), GFP_KERNEL);
	if (!priv) {
		dev_err(&client->dev, "camera: gc2015 failed to allocate private data!\n");
		return -ENOMEM;
	}

	i2c_set_clientdata(client, priv);
	msleep(10);

	for (i = MAX_DETECT_NUM; i > 0; --i) {
		ret = gc2015_detect(client);
		if (!ret) {
			printk(KERN_NOTICE "camera: GalaxyCore 2015 sensor detected!\n");
			pdata->eco_flag = eco;
			pdata->sensor_flag = sensor;
			goto sensor_detected;
		}

		if (ret == -ENXIO)
			goto out_free_priv;
		else
			printk(KERN_ERR "camera: GalaxyCore 2015 sensor detect failure, will retry %d times!\n", i);
	}
	/* NO GC2015 detected within limited loops */
	printk(KERN_NOTICE "camera: Abort retry, failed to detect GalaxyCore 2015 sensor!\n");
	ret = -ENODEV;
	goto out_free_priv;

sensor_detected:
	ret = ccic_sensor_attach(client);
	goto out_free;

out_free_priv:
	kfree(priv);

out_free:
	pdata->power_set(SENSOR_CLOSE, pdata->id, eco, sensor);

	return ret;
}

static int gc2015_remove(struct i2c_client *client)
{
	struct gc2015_priv *priv;
	struct sensor_platform_data *pdata;

	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_CLOSE, pdata->id, pdata->eco_flag, pdata->sensor_flag);
	ccic_sensor_detach(client);
	priv = i2c_get_clientdata(client);
	i2c_set_clientdata(client, NULL);
	kfree(priv);

	return 0;
}

static int gc2015_command(struct i2c_client *client, unsigned int cmd, void *arg)
{
	switch (cmd) {
	case VIDIOC_DBG_G_CHIP_IDENT:
		return v4l2_chip_ident_i2c_client(client, arg, V4L2_IDENT_GC2015, 0);
	case VIDIOC_INT_RESET:
		return gc2015_int_reset(client);
	case VIDIOC_QUERYCAP:
		return gc2015_querycap(client, (struct v4l2_capability *) arg);
	case VIDIOC_ENUM_FMT:
		return gc2015_enum_fmt(client, (struct v4l2_fmtdesc *) arg);
	case VIDIOC_TRY_FMT:
		return gc2015_try_fmt(client, (struct v4l2_format *) arg, NULL, NULL);
	case VIDIOC_ENUM_FRAMESIZES:
		return gc2015_enum_fmsize(client, (struct v4l2_frmsizeenum *) arg);
	case VIDIOC_ENUM_FRAMEINTERVALS:
		return gc2015_enum_fminterval(client, (struct v4l2_frmivalenum *) arg);
	case VIDIOC_S_FMT:
		return gc2015_s_fmt(client, (struct v4l2_format *) arg);
	case VIDIOC_QUERYCTRL:
		return gc2015_q_ctrl(client, (struct v4l2_queryctrl *) arg);
	case VIDIOC_S_CTRL:
		return gc2015_s_ctrl(client, (struct v4l2_control *) arg);
	case VIDIOC_G_CTRL:
		return gc2015_g_ctrl(client, (struct v4l2_control *) arg);
	case VIDIOC_S_PARM:
		return gc2015_s_parm(client, (struct v4l2_streamparm *) arg);
	case VIDIOC_G_PARM:
		return gc2015_g_parm(client, (struct v4l2_streamparm *) arg);
	case VIDIOC_S_INPUT:
		return gc2015_s_input(client, (int *) arg);
	case VIDIOC_STREAMON:
		return gc2015_streamon(client);
	case VIDIOC_STREAMOFF:
		return gc2015_streamoff(client);
#ifdef CONFIG_VIDEO_ADV_DEBUG
	case VIDIOC_DBG_G_REGISTER:
		return gc2015_get_register(client, (struct v4l2_dbg_register *) arg);
	case VIDIOC_DBG_S_REGISTER:
		return gc2015_set_register(client, (struct v4l2_dbg_register *) arg);
	default:
		break;
#endif
	}

	return -EINVAL;
}

static const struct i2c_device_id gc2015_idtable[] = {
	{ "gc2015", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, gc2015_idtable);

static struct i2c_driver gc2015_driver = {
	.driver = {
		.name = "gc2015",
	},
	.id_table = gc2015_idtable,
	.command  = gc2015_command,
	.probe    = gc2015_probe,
	.remove   = gc2015_remove,
};

static int __init gc2015_mod_init(void)
{
	return i2c_add_driver(&gc2015_driver);
}

static void __exit gc2015_mod_exit(void)
{
	i2c_del_driver(&gc2015_driver);
}

late_initcall(gc2015_mod_init);
module_exit(gc2015_mod_exit);

