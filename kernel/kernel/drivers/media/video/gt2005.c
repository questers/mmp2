/*
 * Galaxycore gt2005 sensor driver
 *
 * Copyright (C) 2011 Marvell International Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Created:  2011 Albert Wang <twang13@marvell.com>
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/videodev.h>
#include <linux/i2c.h>
#include <linux/clk.h>
#include <media/v4l2-common.h>
#include <media/v4l2-chip-ident.h>
#include <mach/hardware.h>
#include <mach/camera.h>

#include "pxa688_camera.h"
#include "gt2005.h"

MODULE_AUTHOR("Albert Wang <twang13@marvell.com>");
MODULE_DESCRIPTION("Galaxycore gt2005 sensor driver");
MODULE_LICENSE("GPL");

/* The maximum allowed times for repeating GT2005 detection */
#define MAX_DETECT_NUM	3

/* Supported Resolutions */
enum {
	GT2005_VGA,
	GT2005_WVGA,
	GT2005_720P,
	GT2005_2M,
};

struct gt2005_resolution {
	unsigned int width;
	unsigned int height;
	int fps;
};

static struct gt2005_resolution gt2005_resolutions[] = {
	[GT2005_VGA] = {
		.width	= 640,
		.height	= 480,
		.fps    = 30,
	},
	[GT2005_WVGA] = {
		.width	= 800,
		.height	= 480,
		.fps    = 30,
	},
	[GT2005_720P] = {
		.width	= 1280,
		.height	= 720,
		.fps    = 20,
	},
	[GT2005_2M] = {
		.width	= 1600,
		.height	= 1200,
		.fps    = 20,
	},
};

#define N_GT2005_RES ARRAY_SIZE(gt2005_resolutions)

static struct gt2005_format_struct {
	__u8 *desc;
	__u32 pixelformat;
	struct gt2005_reg *regs;
	int bpp;   /* bits per pixel */
} gt2005_formats[] = {
	{
		.desc		= "YUYV422 planar",
		.pixelformat	= V4L2_PIX_FMT_YUV422P,
		.regs		= gt2005_fmt_yuv422,
		.bpp		= 16,
	},
	{
		.desc		= "YUV420 planar",
		.pixelformat	= V4L2_PIX_FMT_YUV420,
		.regs		= gt2005_fmt_yuv422,
		.bpp		= 12,
	},
	{
		.desc		= "UYVY422 packet",
		.pixelformat	= V4L2_PIX_FMT_UYVY,
		.regs		= gt2005_fmt_yuv422,
		.bpp		= 16,
	},
};

#define N_GT2005_FMTS ARRAY_SIZE(gt2005_formats)

/* Read a register */
static int gt2005_reg_read(struct i2c_client *client, u16 reg, u8 *val)
{
	int ret;
	struct i2c_msg msg[] = {
		{
			.addr	= client->addr,
			.flags	= 0,
			.len	= 2,
			.buf	= (u8 *)&reg,
		},
		{
			.addr	= client->addr,
			.flags	= I2C_M_RD,
			.len	= 1,
			.buf	= val,
		},
	};

	reg = swab16(reg);

	ret = i2c_transfer(client->adapter, msg, 2);
	if (ret < 0) {
		dev_err(&client->dev, "camera: gt2005 failed to read register 0x%04x!\n", reg);
		return ret;
	}

	return 0;
}

/* Write a register */
static int gt2005_reg_write(struct i2c_client *client, u16 reg, u8 val)
{
	struct i2c_msg msg;
	struct {
		u16 reg;
		u8 val;
	} __packed buf;
	int ret;

	reg = swab16(reg);

	buf.reg = reg;
	buf.val = val;

	msg.addr = client->addr;
	msg.flags = 0;
	msg.len	= 3;
	msg.buf	= (u8 *)&buf;

	ret = i2c_transfer(client->adapter, &msg, 1);
	if (ret < 0) {
		dev_err(&client->dev, "camera: gt2005 failed to write register 0x%04x!\n", reg);
		return ret;
	}

	if (reg == SOFTWARE_RESET && (val & 0x01)) {
		printk("camera: a s/w reset triggered, may result in register value lost!\n");
		msleep(2); /* Wait for reset to run */
	}

	return 0;
}

/* Read a register, alter its bits, write it back */
static int gt2005_reg_rmw(struct i2c_client *client, u16 reg, u8 set, u8 unset)
{
	u8 val;
	int ret;

	ret = gt2005_reg_read(client, reg, &val);
	if (ret < 0) {
		dev_err(&client->dev, "camera: gt2005 [Read]-Modify-Write of register %04x failed!\n", reg);
		return ret;
	}

	val |= set;
	val &= ~unset;

	ret = gt2005_reg_write(client, reg, val);
	if (ret < 0) {
		dev_err(&client->dev, "camera: gt2005 Read-Modify-[Write] of register %04x failed!\n", reg);
		return ret;
	}

	return 0;
}

static int gt2005_reg_write_array(struct i2c_client *client, const struct gt2005_reg *regarray, int regarraylen)
{
	int i;
	int ret;

	for (i = 0; i < regarraylen; i++) {
		ret = gt2005_reg_write(client, regarray[i].reg, regarray[i].val);
		if (ret < 0)
			return ret;
	}

	return 0;
}

static int gt2005_t_vflip(struct i2c_client *client, int value)
{
	int ret = 0;

	if (value)
		ret = gt2005_reg_rmw(client, IMAGE_ORT, 0x2, 0);
	else
		ret = gt2005_reg_rmw(client, IMAGE_ORT, 0, 0x2);

	return ret;
}

static int gt2005_q_vflip(struct i2c_client *client, __s32 *value)
{
	int ret = 0;
	u8 v;

	ret = gt2005_reg_read(client, IMAGE_ORT, &v);
	*value = (v & 0x2) == 0x2;

	return ret;
}

static int gt2005_t_hflip(struct i2c_client *client, int value)
{
	int ret = 0;

	if (value)
		ret = gt2005_reg_rmw(client, IMAGE_ORT, 0x1, 0);
	else
		ret = gt2005_reg_rmw(client, IMAGE_ORT, 0, 0x1);

	return ret;
}

static int gt2005_q_hflip(struct i2c_client *client, __s32 *value)
{
	int ret = 0;
	u8 v;
	ret = gt2005_reg_read(client, IMAGE_ORT, &v);
	*value = (v & 0x1) == 0x1;

	return ret;
}

static struct gt2005_control {
	struct v4l2_queryctrl qc;
	int (*query)(struct i2c_client *client, __s32 *value);
	int (*tweak)(struct i2c_client *client, int value);
} gt2005_controls[] = {
	{
		.qc = {
			.id		= V4L2_CID_VFLIP,
			.type		= V4L2_CTRL_TYPE_BOOLEAN,
			.name		= "Flip Vertically",
			.minimum	= 0,
			.maximum	= 1,
			.step		= 1,
			.default_value	= 0,
		},
		.tweak = gt2005_t_vflip,
		.query = gt2005_q_vflip,
	},
	{
		.qc = {
			.id		= V4L2_CID_HFLIP,
			.type		= V4L2_CTRL_TYPE_BOOLEAN,
			.name		= "Flip Horizontally",
			.minimum	= 0,
			.maximum	= 1,
			.step		= 1,
			.default_value	= 0,
		},
		.tweak = gt2005_t_hflip,
		.query = gt2005_q_hflip,
	},
};

#define N_CONTROLS (ARRAY_SIZE(gt2005_controls))

/* Start/Stop streaming from the device */
static int gt2005_streamon(struct i2c_client *client)
{
	int ret;

	dev_dbg(&client->dev, "camera: gt2005 Stream On!\n");
	/* Start Streaming */
	ret = gt2005_reg_write(client, MODE_SELECT, 0x01);
	ret = gt2005_reg_write(client, HOLD_SELECT, 0x02);
	ret = gt2005_reg_write(client, OUTPUT_SELECT, 0x03);

	return ret;
}

static int gt2005_streamoff(struct i2c_client *client)
{
	int ret;

	dev_dbg(&client->dev, "camera: gt2005 Stream Off!\n");
	/* Set Streaming to Standby */
	ret = gt2005_reg_write(client, MODE_SELECT, 0x00);
	ret = gt2005_reg_write(client, HOLD_SELECT, 0x01);
	ret = gt2005_reg_write(client, OUTPUT_SELECT, 0x00);

	return ret;
}

static struct gt2005_control *gt2005_find_control(__u32 id)
{
	int i;

	for (i = 0; i < N_CONTROLS; i++)
		if (gt2005_controls[i].qc.id == id)
			return gt2005_controls + i;

	return NULL;
}

static int gt2005_g_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct gt2005_control *octrl = gt2005_find_control(ctrl->id);
	int ret = 0;

	if (octrl == NULL)
		return -EINVAL;

	ret = octrl->query(client, &ctrl->value);
	if (ret >= 0)
		return 0;

	return ret;
}

static int gt2005_s_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct gt2005_control *octrl = gt2005_find_control(ctrl->id);
	int ret = 0;

	if (octrl == NULL)
		return -EINVAL;

	ret =  octrl->tweak(client, ctrl->value);
	if (ret >= 0)
		return 0;

	return ret;
}

static int gt2005_g_parm(struct i2c_client *client, struct v4l2_streamparm *parms)
{
	return 0;
}

static int gt2005_s_parm(struct i2c_client *client, struct v4l2_streamparm *parms)
{
	return 0;
}

static int gt2005_s_input(struct i2c_client *client, int *id)
{
	return 0;
}

static int gt2005_querycap(struct i2c_client *client, struct v4l2_capability *argp)
{
	if (!argp) {
		printk(KERN_ERR " argp is NULL %s %d \n", __FUNCTION__, __LINE__);
		return -EINVAL;
	}
	strcpy(argp->driver, "gt2005");
	strcpy(argp->card, "MMP2");
	return 0;
}

static int gt2005_enum_fmt(struct i2c_client *client, struct v4l2_fmtdesc *fmt)
{
	struct gt2005_format_struct *ofmt;

	if (fmt->index >= N_GT2005_FMTS)
		return -EINVAL;

	ofmt = gt2005_formats + fmt->index;
	fmt->flags = 0;
	strcpy(fmt->description, ofmt->desc);
	fmt->pixelformat = ofmt->pixelformat;
	return 0;
}

static int gt2005_int_reset(struct i2c_client *client)
{
	int ret = 0;

	/* Software Reset */
	ret = gt2005_reg_write(client, SOFTWARE_RESET, 0x01);
	msleep(2);

	/* Initialize settings */
	ret = gt2005_reg_write_array(client, gt2005_defaults, ARRAY_SIZE(gt2005_defaults));
	if (ret < 0)
		return ret;
	return 0;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int gt2005_get_register(struct i2c_client *client, struct v4l2_dbg_register *reg)
{
	int ret;
	u8 val;

	if (reg->reg & ~0xffff)
		return -EINVAL;

	reg->size = 2;

	ret = gt2005_reg_read(client, reg->reg, &val);
	if (ret)
		return ret;

	reg->val = (__u64)val;

	return ret;
}

static int gt2005_set_register(struct i2c_client *client, struct v4l2_dbg_register *reg)
{
	if (reg->reg & ~0xffff || reg->val & ~0xff)
		return -EINVAL;

	return gt2005_reg_write(client, reg->reg, reg->val);
}
#endif

static int gt2005_try_fmt(struct i2c_client *client, struct v4l2_format *fmt,
		struct gt2005_format_struct **ret_fmt,
		struct gt2005_resolution **ret_wsize)
{
	int index_fmt, index_wsize;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	for (index_fmt = 0; index_fmt < N_GT2005_FMTS; index_fmt++)
		if (gt2005_formats[index_fmt].pixelformat == pix->pixelformat)
			break;

	if (index_fmt >= N_GT2005_FMTS) {
		printk(KERN_ERR "camera: gt2005 try unsupported format!\n");
		return -EINVAL;
	}

	if (ret_fmt != NULL)
		*ret_fmt = &gt2005_formats[index_fmt];

	if (pix->field == V4L2_FIELD_ANY)
		pix->field = V4L2_FIELD_NONE;
	else if (pix->field != V4L2_FIELD_NONE) {
		printk(KERN_ERR "camera: gt2005 pixel filed should be V4l2_FIELD_NONE!\n");
		return -EINVAL;
	}

	for (index_wsize = 0; index_wsize < N_GT2005_RES; index_wsize++)
		if (pix->width == gt2005_resolutions[index_wsize].width && pix->height == gt2005_resolutions[index_wsize].height)
			break;

	if (index_wsize >= N_GT2005_RES) {
		printk(KERN_ERR "camera: gt2005 try unsupported preview format size!\n");
		return -EINVAL;
	}

	if (ret_wsize != NULL)
		*ret_wsize = &gt2005_resolutions[index_wsize];
	pix->bytesperline = pix->width*gt2005_formats[index_fmt].bpp/8;
	pix->sizeimage = pix->height*pix->bytesperline;

	return 0;
}

static int gt2005_enum_fmsize(struct i2c_client *client, struct v4l2_frmsizeenum *argp)
{
	struct v4l2_frmsizeenum frmsize;

	if (copy_from_user(&frmsize, argp, sizeof(frmsize)))
		return -EFAULT;

	switch (frmsize.pixel_format) {
	case V4L2_PIX_FMT_YUYV:
		if (frmsize.index == N_GT2005_RES)
			return -EINVAL;
		else if (frmsize.index > N_GT2005_RES) {
			printk(KERN_ERR "camera: gt2005 enum unsupported preview format size!\n");
			return -EINVAL;
		} else {
			frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
			frmsize.discrete.height = gt2005_resolutions[frmsize.index].height;
			frmsize.discrete.width = gt2005_resolutions[frmsize.index].width;
			break;
		}
	case V4L2_PIX_FMT_UYVY:
		if (frmsize.index == N_GT2005_RES)
			return -EINVAL;
		else if (frmsize.index > N_GT2005_RES) {
			printk(KERN_ERR "camera: gt2005 enum unsupported preview format size!\n");
			return -EINVAL;
		} else {
			frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
			frmsize.discrete.height = gt2005_resolutions[frmsize.index].height;
			frmsize.discrete.width = gt2005_resolutions[frmsize.index].width;
			break;
		}
	case V4L2_PIX_FMT_YUV420:
		if (frmsize.index == N_GT2005_RES)
			return -EINVAL;
		else if (frmsize.index > N_GT2005_RES) {
			printk(KERN_ERR "camera: gt2005 enum unsupported preview format size!\n");
			return -EINVAL;
		} else {
			frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
			frmsize.discrete.height = gt2005_resolutions[frmsize.index].height;
			frmsize.discrete.width = gt2005_resolutions[frmsize.index].width;
			break;
		}

	default:
		printk(KERN_ERR "camera: gt2005 enum format size with unsupported format!\n");
		return -EINVAL;
	}

	if (copy_to_user(argp, &frmsize, sizeof(frmsize)))
		return -EFAULT;
	return 0;
}

static int gt2005_enum_fminterval(struct i2c_client *client, struct v4l2_frmivalenum *argp)
{
	struct v4l2_frmivalenum frminterval;

	if (copy_from_user(&frminterval, argp, sizeof(frminterval)))
		return -EFAULT;

	switch (frminterval.pixel_format) {
	case V4L2_PIX_FMT_YUYV:
		if (frminterval.index == N_GT2005_RES)
			return -EINVAL;
		else if (frminterval.index > N_GT2005_RES) {
			printk(KERN_ERR "camera: gt2005 enum unsupported preview format frame rate!\n");
			return -EINVAL;
		} else {
			frminterval.type = V4L2_FRMIVAL_TYPE_DISCRETE;
			frminterval.discrete.numerator = 1;
			frminterval.discrete.denominator = gt2005_resolutions[frminterval.index].fps;
			break;
		}
	case V4L2_PIX_FMT_UYVY:
		if (frminterval.index == N_GT2005_RES)
			return -EINVAL;
		else if (frminterval.index > N_GT2005_RES) {
			printk(KERN_ERR "camera: gt2005 enum unsupported preview format frame rate!\n");
			return -EINVAL;
		} else {
			frminterval.type = V4L2_FRMIVAL_TYPE_DISCRETE;
			frminterval.discrete.numerator = 1;
			frminterval.discrete.denominator = gt2005_resolutions[frminterval.index].fps;
			break;
		}
	case V4L2_PIX_FMT_YUV420:
		if (frminterval.index == N_GT2005_RES)
			return -EINVAL;
		else if (frminterval.index > N_GT2005_RES) {
			printk(KERN_ERR "camera: gt2005 enum unsupported preview format frame rate!\n");
			return -EINVAL;
		} else {
			frminterval.type = V4L2_FRMIVAL_TYPE_DISCRETE;
			frminterval.discrete.numerator = 1;
			frminterval.discrete.denominator = gt2005_resolutions[frminterval.index].fps;
			break;
		}
	default:
		printk(KERN_ERR "camera: gt2005 enum format frame rate with unsupported format!\n");
		return -EINVAL;
	}

	if (copy_to_user(argp, &frminterval, sizeof(frminterval)))
		return -EFAULT;
	return 0;
}

static int gt2005_s_fmt(struct i2c_client *client, struct v4l2_format *fmt)
{
	int ret = 0;
	struct gt2005_format_struct *ovfmt = NULL;
	struct gt2005_resolution *wsize = NULL;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;

	ret = gt2005_try_fmt(client, fmt, &ovfmt, &wsize);
	if (ret)
		return ret;

	/* select register configuration for given resolution */
	if (pix->width == gt2005_resolutions[GT2005_VGA].width) {
		dev_dbg(&client->dev, "camera: gt2005 set image size to VGA!\n");
		ret = gt2005_reg_write_array(client, gt2005_regs_vga, ARRAY_SIZE(gt2005_regs_vga));
	} else if (pix->width == gt2005_resolutions[GT2005_WVGA].width) {
		dev_dbg(&client->dev, "camera: gt2005 set image size to WVGA!\n");
		ret = gt2005_reg_write_array(client, gt2005_regs_wvga, ARRAY_SIZE(gt2005_regs_wvga));
	} else if (pix->width == gt2005_resolutions[GT2005_720P].width) {
		dev_dbg(&client->dev, "camera: gt2005 set image size to 720P!\n");
		ret = gt2005_reg_write_array(client, gt2005_regs_720p, ARRAY_SIZE(gt2005_regs_720p));
	} else if (pix->width == gt2005_resolutions[GT2005_2M].width) {
		dev_dbg(&client->dev, "camera: gt2005 set image size to 2M full size!\n");
		ret = gt2005_reg_write_array(client, gt2005_regs_2m, ARRAY_SIZE(gt2005_regs_2m));
	} else {
		dev_err(&client->dev, "camera: gt2005 failed to select resolution!\n");
		ret = -EINVAL;
	}

	return ret;
}

static int gt2005_q_ctrl(struct i2c_client *client, struct v4l2_queryctrl *qc)
{
	struct gt2005_control *ctrl = gt2005_find_control(qc->id);

	if (ctrl == NULL)
		return -EINVAL;
	*qc = ctrl->qc;
	return 0;
}

static int gt2005_detect(struct i2c_client *client)
{
	struct gt2005_priv *priv = i2c_get_clientdata(client);
	u8 modelhi, modello;
	int ret;

	/*
	 * check and show product ID and manufacturer ID
	 */
	ret = gt2005_reg_read(client, MODEL_ID_HI, &modelhi);
	if (ret < 0)
		goto err;

	ret = gt2005_reg_read(client, MODEL_ID_LO, &modello);
	if (ret < 0)
		goto err;

	priv->model = (modelhi << 8) | modello;

	if (priv->model != 0x5138) {
		ret = -ENODEV;
		goto err;
	}

	priv->ident = V4L2_IDENT_GT2005;
	printk("camera: gt2005 Model_ID = 0x%04x\n", priv->model);

err:
	return ret;
}

/*
 * i2c_driver function
 */
static int gt2005_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int i, ret, eco = CAMERA_ECO_ON, sensor = SENSOR_GCT;
	struct gt2005_priv *priv;
	struct sensor_platform_data *pdata;

	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_OPEN, pdata->id, eco, sensor);
	pdata->eco_set(eco);

	priv = kzalloc(sizeof(struct gt2005_priv), GFP_KERNEL);
	if (!priv) {
		dev_err(&client->dev, "camera: gt2005 failed to allocate private data!\n");
		return -ENOMEM;
	}

	i2c_set_clientdata(client, priv);
	msleep(10);

	for (i = MAX_DETECT_NUM; i > 0; --i) {
		ret = gt2005_detect(client);
		if (!ret) {
			printk(KERN_NOTICE "camera: GalaxyCore 2005 sensor detected!\n");
			pdata->eco_flag = eco;
			pdata->sensor_flag = sensor;
			goto sensor_detected;
		}

		if (ret == -ENXIO)
			goto out_free_priv;
		else
			printk(KERN_ERR "camera: GalaxyCore 2005 sensor detect failure, will retry %d times!\n", i);
	}
	/* NO GT2005 detected within limited loops */
	printk(KERN_NOTICE "camera: Abort retry, failed to detect GalaxyCore 2005 sensor!\n");
	ret = -ENODEV;
	goto out_free_priv;

sensor_detected:
	ret = ccic_sensor_attach(client);
	goto out_free;

out_free_priv:
	kfree(priv);

out_free:
	pdata->power_set(SENSOR_CLOSE, pdata->id, eco, sensor);

	return ret;
}

static int gt2005_remove(struct i2c_client *client)
{
	struct gt2005_priv *priv;
	struct sensor_platform_data *pdata;

	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_CLOSE, pdata->id, pdata->eco_flag, pdata->sensor_flag);
	ccic_sensor_detach(client);
	priv = i2c_get_clientdata(client);
	i2c_set_clientdata(client, NULL);
	kfree(priv);

	return 0;
}

static int gt2005_command(struct i2c_client *client, unsigned int cmd, void *arg)
{
	switch (cmd) {
	case VIDIOC_DBG_G_CHIP_IDENT:
		return v4l2_chip_ident_i2c_client(client, arg, V4L2_IDENT_GT2005, 0);
	case VIDIOC_INT_RESET:
		return gt2005_int_reset(client);
	case VIDIOC_QUERYCAP:
		return gt2005_querycap(client, (struct v4l2_capability *) arg);
	case VIDIOC_ENUM_FMT:
		return gt2005_enum_fmt(client, (struct v4l2_fmtdesc *) arg);
	case VIDIOC_TRY_FMT:
		return gt2005_try_fmt(client, (struct v4l2_format *) arg, NULL, NULL);
	case VIDIOC_ENUM_FRAMESIZES:
		return gt2005_enum_fmsize(client, (struct v4l2_frmsizeenum *) arg);
	case VIDIOC_ENUM_FRAMEINTERVALS:
		return gt2005_enum_fminterval(client, (struct v4l2_frmivalenum *) arg);
	case VIDIOC_S_FMT:
		return gt2005_s_fmt(client, (struct v4l2_format *) arg);
	case VIDIOC_QUERYCTRL:
		return gt2005_q_ctrl(client, (struct v4l2_queryctrl *) arg);
	case VIDIOC_S_CTRL:
		return gt2005_s_ctrl(client, (struct v4l2_control *) arg);
	case VIDIOC_G_CTRL:
		return gt2005_g_ctrl(client, (struct v4l2_control *) arg);
	case VIDIOC_S_PARM:
		return gt2005_s_parm(client, (struct v4l2_streamparm *) arg);
	case VIDIOC_G_PARM:
		return gt2005_g_parm(client, (struct v4l2_streamparm *) arg);
	case VIDIOC_S_INPUT:
		return gt2005_s_input(client, (int *) arg);
	case VIDIOC_STREAMON:
		return gt2005_streamon(client);
	case VIDIOC_STREAMOFF:
		return gt2005_streamoff(client);
#ifdef CONFIG_VIDEO_ADV_DEBUG
	case VIDIOC_DBG_G_REGISTER:
		return gt2005_get_register(client, (struct v4l2_dbg_register *) arg);
	case VIDIOC_DBG_S_REGISTER:
		return gt2005_set_register(client, (struct v4l2_dbg_register *) arg);
	default:
		break;
#endif
	}

	return -EINVAL;
}

static const struct i2c_device_id gt2005_idtable[] = {
	{ "gt2005", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, gt2005_idtable);

static struct i2c_driver gt2005_driver = {
	.driver = {
		.name = "gt2005",
	},
	.id_table = gt2005_idtable,
	.command  = gt2005_command,
	.probe    = gt2005_probe,
	.remove   = gt2005_remove,
};

static int __init gt2005_mod_init(void)
{
	return i2c_add_driver(&gt2005_driver);
}

static void __exit gt2005_mod_exit(void)
{
	i2c_del_driver(&gt2005_driver);
}

late_initcall(gt2005_mod_init);
module_exit(gt2005_mod_exit);

