/*
 *  ITE7260 multi-touch driver.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 */
 
//Comment below to disable device debug output
#define DEBUG
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/i2c.h>
#include <linux/hrtimer.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <plat/mfp.h>
#include <linux/input/questers_ts.h>

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif

#define HRTIMER_POLL		0 /* use hrtimer to poll touch events */

#define IT_QUERY_BUFF_REG	0x80
#define IT_QUERY_BUFF_LEN	1
#define IT_POINT_DATA_REG	0xe0
#define IT_MAX_DATA_LEN1	16
#define IT_MAX_DATA_LEN2	144
#define IT_MAX_POINTS		2

#define MAX_BUFFER_SIZE		144
#define IOC_MAGIC		'd'
#define IOCTL_SET 		_IOW(IOC_MAGIC, 1, struct ioctl_cmd168)
#define IOCTL_GET 		_IOR(IOC_MAGIC, 2, struct ioctl_cmd168)
#define DEVICE_NAME		"IT7260"


#define ITE_TS_WIDTH 480
#define ITE_TS_HEIGHT 800

static int ite7260_major = 0; // dynamic major by default
static int ite7260_minor = 0;
static struct cdev ite7260_cdev;
static struct class *ite7260_class = NULL;
static dev_t ite7260_dev;

struct ioctl_cmd168 {
	unsigned short bufferIndex;
	unsigned short length;
	unsigned short buffer[MAX_BUFFER_SIZE];
};

struct ite7260_data {
	rwlock_t lock;
	unsigned short bufferIndex;
	unsigned short length;
	unsigned short buffer[MAX_BUFFER_SIZE];
};

struct raw_sample
{
	bool pressed; /* pressed and data available */
	
	u16 x;
	u16 y;
	u16 p;	/* pressure */
};

struct touch_ite_state{
	struct i2c_client* client;
	struct device* dev;
	struct input_dev *idev;
	struct touch_platform_data *pdata;

	struct workqueue_struct* wq;
	struct delayed_work delayed_work;

	//last samples
	struct raw_sample raw[IT_MAX_POINTS];
	u8 read_data[IT_MAX_DATA_LEN2];
	
#if HRTIMER_POLL
	struct hrtimer timer;
#endif
	int irq;

	//gpio handle for touch pad interrupt
	int gpio;

	//reference counter touch pad device
	int  ref_count;

	int quirks;
	
	int x_max;
	int y_max;

#if defined(CONFIG_HAS_EARLYSUSPEND)
	struct early_suspend early_suspend;
#endif
};

#ifdef CONFIG_HAS_EARLYSUSPEND
static volatile int touch_suspend = 0 ;
#endif

static int interval = 4; /* query touch panel at 4ms interval */
module_param(interval, int, 0);

struct touch_ite_state* g_ts_state = NULL;

#if defined(CONFIG_HAS_EARLYSUSPEND)
static void ite7260_earlysuspend(struct early_suspend* h)
{
	struct touch_ite_state *ts_state = container_of(h, struct touch_ite_state, early_suspend);

	touch_suspend = 1;

	cancel_delayed_work_sync(&ts_state->delayed_work);
#if !HRTIMER_POLL
	disable_irq(ts_state->irq);
#else
	hrtimer_cancel(&ts_state->timer);
#endif

	if (ts_state->pdata->power)
	{
		ts_state->pdata->power(0);
	}
}

static void ite7260_lateresume(struct early_suspend* h)
{
	struct touch_ite_state *ts_state = container_of(h, struct touch_ite_state, early_suspend);

	if (ts_state->pdata->power)
	{
		ts_state->pdata->power(1);
	}
	
#if !HRTIMER_POLL
	enable_irq(ts_state->irq);
#else
	hrtimer_start(&ts_state->timer, ktime_set(0, 0), HRTIMER_MODE_REL);
#endif
	
	touch_suspend = 0;
}
#endif

/*
 * Sample the touchscreen
 */
int read_touch(struct touch_ite_state *ts)
{
	int i;
	s32 ret;
	
	/* check if data is available */
	ret = i2c_smbus_read_i2c_block_data(ts->client, IT_QUERY_BUFF_REG, IT_QUERY_BUFF_LEN, ts->read_data);
	if(ret != IT_QUERY_BUFF_LEN)
	{
		dev_err(ts->dev,"i2c read error\n");
		return 0;
	}

	/* no point info */
	if (!(ts->read_data[0] & 0x80))
	{
		//dev_err(ts->dev, "no point info\n");
		return 0;
	}

	/* read point data */
	ret = i2c_smbus_read_i2c_block_data(ts->client,IT_POINT_DATA_REG,IT_MAX_DATA_LEN1,ts->read_data);
	if(ret == 0)
	{
		dev_err(ts->dev,"i2c read error\n");
		return 0;
	}

	dev_dbg(ts->dev, "data[0]: %02x\n", ts->read_data[0]);
	if (ts->read_data[0]&0xf0)
	{
		/* not point data */
		return 0;
	}

	if (ts->read_data[1]&0x01)
	{
		/* palm info, ignore */
		return 0;
	}
	
	for (i=0; i<IT_MAX_POINTS; i++)
	{
		int pressed = (ts->read_data[0]>>i) & 0x01;
		if (ts->raw[i].pressed && !pressed)
		{
			dev_dbg(ts->dev, "%d up\n", i);
			input_report_abs(ts->idev, ABS_MT_TOUCH_MAJOR, 0);
			input_report_abs(ts->idev, ABS_MT_WIDTH_MAJOR, 0);
			input_mt_sync(ts->idev);
		}

		if (!ts->raw[i].pressed && pressed)
			dev_dbg(ts->dev, "%d down\n", i);

		ts->raw[i].pressed = pressed;
		if (ts->raw[i].pressed)
		{
			
			ts->raw[i].x = (ts->read_data[2+i*4+1]&0xf0)<<4 | (ts->read_data[2+i*4+2]);
			ts->raw[i].y = (ts->read_data[2+i*4+1]&0x0f)<<8 | (ts->read_data[2+i*4+0]);
			ts->raw[i].p = (ts->read_data[2+i*4+3]&0x0f);

			if(ts->quirks&TOUCH_QUIRKS_XY_SWAP)
			{
				u16 temp;
				temp = ts->raw[i].x;
				ts->raw[i].x = ts->raw[i].y;
				ts->raw[i].y = temp;
			}
			if(ts->quirks&TOUCH_QUIRKS_X_INVERTED)
				ts->raw[i].x = ts->x_max -ts->raw[i].x;
			if(ts->quirks&TOUCH_QUIRKS_Y_INVERTED)
				ts->raw[i].y = ts->y_max -ts->raw[i].y;
		
			dev_dbg(ts->dev, "%d(%d,%d)\n", i, ts->raw[i].x, ts->raw[i].y);

			input_report_abs(ts->idev, ABS_MT_POSITION_X, ts->raw[i].x);
			input_report_abs(ts->idev, ABS_MT_POSITION_Y, ts->raw[i].y);
			input_report_abs(ts->idev, ABS_MT_TOUCH_MAJOR, ts->raw[i].p);
			input_report_abs(ts->idev, ABS_MT_WIDTH_MAJOR, 0xf);
			input_mt_sync(ts->idev);
		}
	}
	input_sync(ts->idev);

	return 0;
}

static void touch_delayed_work(struct work_struct* work)
{
	struct touch_ite_state *ts = (struct touch_ite_state*)container_of(work,struct touch_ite_state,delayed_work.work);	

#if !HRTIMER_POLL
	while (!gpio_get_value(ts->gpio)&&!touch_suspend) read_touch(ts);

	//enable_irq(ts->irq);
#else
	read_touch(ts);
	hrtimer_start(&ts->timer, ktime_set(0, interval*1000000), HRTIMER_MODE_REL);
#endif
}

/*
 * Start the touchscreen thread and
 * the touch digitiser.
 */
static int touch_ite_input_open(struct input_dev *idev)
{
	struct touch_ite_state *ts_state = (struct touch_ite_state*)input_get_drvdata(idev);

#ifdef CONFIG_HAS_EARLYSUSPEND
	if(touch_suspend){
		dev_dbg(ts_state->dev,"touch is suspended!\n");
		return -1;
	}
#endif

	if (ts_state->ref_count++ > 0)
	{
		printk("Touchpanel has already been opened. Abort.\n");
		return 0;
	}

	dev_dbg(ts_state->dev,"Touch is opened. Use count: %d\n", ts_state->ref_count);

	/* 
	 * Read the pending data. This is necessary, 
	 * otherwise interrupt won't be triggered.
	 */
	read_touch(ts_state);

#if HRTIMER_POLL
	hrtimer_start(&ts_state->timer, ktime_set(0, 1000000L*interval), HRTIMER_MODE_REL);
#endif

	return 0;
}

static void touch_ite_input_close(struct input_dev *idev)
{
	struct touch_ite_state *ts_state = (struct touch_ite_state*)input_get_drvdata(idev);

	if (ts_state->ref_count-- != 1)
	{
		printk("Touchpanel has not been opened. Abort.\n");
	}

#ifdef CONFIG_HAS_EARLYSUSPEND
	if(touch_suspend){
		pr_info("touch is suspended!\n");
		return;
	}
#endif

#if 0
#if HRTIMER_POLL
	disable_irq(ts_state->irq);
#else
	hrtimer_cancel(&ts_state->timer);
#endif
#endif
}

#if !HRTIMER_POLL
static irqreturn_t ite7260_touch_irq(int irq, void *irq_data)
{
	struct touch_ite_state *ts = (struct touch_ite_state*)irq_data;

	if (ts && ts->ref_count > 0)
	{
		//disable_irq_nosync(ts->irq);
		if(!delayed_work_pending(&ts->delayed_work))
			queue_delayed_work(ts->wq, &ts->delayed_work, msecs_to_jiffies(0));
	}
	
	return IRQ_HANDLED;
}
#else
static enum hrtimer_restart ite7260_timer_func(struct hrtimer *timer)
{
	struct touch_ite_state *ts = container_of(timer, struct touch_ite_state, timer);

	if (gpio_get_value(ts->gpio)==0) 
	{
		queue_delayed_work(ts->wq, &ts->delayed_work, msecs_to_jiffies(0));
	}
	else
	{
		hrtimer_start(timer, ktime_set(0, 1000000L*interval), HRTIMER_MODE_REL);
	}
	
	return HRTIMER_NORESTART; /* one time */
}
#endif

int ite7260_show_info(struct touch_ite_state* ts)
{
	int ret;
	int tries;
	unsigned char query;
	unsigned char cmd[80];

	cmd[0] = 0x00;
	ret = i2c_smbus_write_i2c_block_data(ts->client, 0x20, 1, cmd);
	if (ret<0)
	{
		printk("Error sending command.\n");
		return -1;
	}

	/* waiting for command OK */
	tries = 0x100;
	do
	{
		query = i2c_smbus_read_byte_data(ts->client, 0x80);
		if (query<0) query = 0xff;
	} while ((query&0x1)&&(tries--));

	memset(cmd, 0, 0x0a);
	ret = i2c_smbus_read_i2c_block_data(ts->client, 0xa0, 0x0a, cmd);
	if (ret<0)
	{
		printk("Error reading device info.\n");
		return -1;
	}

	printk("Device ID: %c%c%c%c%c%c%c\n", cmd[1], cmd[2], cmd[3], cmd[4], cmd[5], cmd[6], cmd[7]);
	printk("Device Version: %c%c(%s)\n", cmd[8], cmd[9], cmd[8]=='5'?"BX3":"BX4");

	cmd[0] = 0x01;
	cmd[1] = 0x00;
	ret = i2c_smbus_write_i2c_block_data(ts->client, 0x20, 2, cmd);
	if (ret<0)
	{
		printk("Error sending command.\n");
		return -1;
	}

	/* waiting for command OK */
	tries = 0x100;
	do
	{
		query = i2c_smbus_read_byte_data(ts->client, 0x80);
		if (query<0) query = 0xff;
	} while ((query&0x1)&&(tries--));

	memset(cmd, 0, 0x0a);
	ret = i2c_smbus_read_i2c_block_data(ts->client, 0xa0, 0x09, cmd);
	if (ret<0)
	{
		printk("Error reading firmware version.\n");
		return -1;
	}

	printk("ROM firmware version: %d.%d.%d.%d\n", cmd[1], cmd[2], cmd[3], cmd[4]);
	printk("Flash firmware version: %d.%d.%d.%d\n", cmd[5], cmd[6], cmd[7], cmd[8]);

	return 0;
}

static ssize_t ite7260_cal_store(
		struct device* dev,
		struct device_attribute* attr,
		const char* buf,
		size_t count)
{
	int ret;
	int tries;
	unsigned char query;
	unsigned char cmd[80];
	//struct touch_ite_state* ts_state = container_of(&dev, struct touch_ite_state, dev);
	printk("Calibration in progress...\n");

	tries = 0x80000;
	do
	{
		query = i2c_smbus_read_byte_data(g_ts_state->client, 0x80);
		if (query<0) query = 0xff;
	} while ((query&0x1)&&(tries--));

	if (tries<=0)
	{
		printk("Calibration failed: target is busy before writting command.\n");
		goto failed;
	}

	memset(cmd, 0, 20);
	cmd[0] = 0x13;
	cmd[1] = 0x00;
	ret = i2c_smbus_write_i2c_block_data(g_ts_state->client, 0x20, 12, cmd);
	if (ret<0)
	{
		printk("Calibration failed: failed writting command.\n");
		goto failed;
	}

	tries = 0x80000;
	do
	{
		query = i2c_smbus_read_byte_data(g_ts_state->client, 0x80);
		if (query<0) query = 0xff;
	} while ((query&0x1)&&(tries--));

	if (tries<=0)
	{
		printk("Calibration failed: target is busy after writting command.\n");
		goto failed;
	}

	memset(cmd, 0, 20);
	ret = i2c_smbus_read_i2c_block_data(g_ts_state->client, 0xa0, 1, cmd);
	printk("Calibration result: %x.\n", cmd[0]);

failed:
	return count;
}

static DEVICE_ATTR(cal, 0666, NULL, ite7260_cal_store);

static ssize_t ite7260_info_show(
	struct device* dev,
	struct device_attribute* attr,
	char* buf)
{
	int ret;

	ret = ite7260_show_info(g_ts_state);
	
	return 0;
}

static DEVICE_ATTR(info, 0666, ite7260_info_show, NULL);

int ite7260_read
(
	struct i2c_client *client, 
	unsigned char bufferIndex,
	unsigned char dataBuffer[], 
	unsigned short dataLength
) 
{
	int ret;
	struct i2c_msg msgs[2] = { 
		{ 
			.addr = client->addr, 
			.flags = I2C_M_NOSTART,
			.len = 1, 
			.buf = &bufferIndex 
		}, 
		{ 
			.addr = client->addr, 
			.flags = I2C_M_RD, 
			.len = dataLength, 
			.buf = dataBuffer 
		} 
	};

	memset(dataBuffer, 0xFF, dataLength);
	ret = i2c_transfer(client->adapter, msgs, 2);
	return ret;
}

int ite7260_write
(
	struct i2c_client *client, 
	unsigned char bufferIndex,
	unsigned char const dataBuffer[], 
	unsigned short dataLength
) 
{
	unsigned char buffer4Write[256];
	struct i2c_msg msgs[1] = { 
		{ 
			.addr = client->addr, 
			.flags = 0, 
			.len = dataLength + 1, 
			.buf = buffer4Write 
		} 
	};

	buffer4Write[0] = bufferIndex;
	memcpy(&(buffer4Write[1]), dataBuffer, dataLength);
	return i2c_transfer(client->adapter, msgs, 1);
}

int ite7260_ioctl
(
	struct inode *inode, 
	struct file *filp, 
	unsigned int cmd,
	unsigned long arg
) 
{
	int retval = 0;
	int i;
	unsigned char buffer[MAX_BUFFER_SIZE];
	struct ioctl_cmd168 data;
	unsigned char datalen;
	unsigned char ent[] = {0x60, 0x00, 0x49, 0x54, 0x37, 0x32};
	unsigned char ext[] = {0x60, 0x80, 0x49, 0x54, 0x37, 0x32};

	//pr_info("=ite7260_ioctl=\n");
	memset(&data, 0, sizeof(struct ioctl_cmd168));

	switch (cmd) {
	case IOCTL_SET:
		//pr_info("=IOCTL_SET=\n");
		if (!access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd))) {
			retval = -EFAULT;
			goto done;
		}

		if ( copy_from_user(&data, (int __user *)arg, sizeof(struct ioctl_cmd168)) ) {
			retval = -EFAULT;
			goto done;
		}
		buffer[0] = (unsigned char) data.bufferIndex;
		//pr_info("%.2X ", buffer[0]);
		for (i = 1; i < data.length + 1; i++) {
			buffer[i] = (unsigned char) data.buffer[i - 1];
			//pr_info("%.2X ", buffer[i]);
		}

		if (!memcmp(&(buffer[1]), ent, sizeof(ent))) {
			pr_info("Disabling IRQ.\n");
			disable_irq(g_ts_state->client->irq);
		}

		if (!memcmp(&(buffer[1]), ext, sizeof(ext))) {
	        	pr_info("Enabling IRQ.\n");
	        	enable_irq(g_ts_state->client->irq);
		}

		//pr_info("=================================================\n");
		//pr_info("name[%s]---addr[%x]-flags[%d]=\n",g_ts_state->client->name,g_ts_state->client->addr,g_ts_state->client->flags);
		datalen = (unsigned char) (data.length + 1);
		//pr_info("datalen=%d\n", datalen);
		//write_lock(&dev->lock);
		retval = ite7260_write(g_ts_state->client,
				(unsigned char) data.bufferIndex, &(buffer[1]), datalen - 1);
		//write_unlock(&dev->lock);
		//pr_info("SET:retval=%x\n", retval);
		retval = 0;
		break;

	case IOCTL_GET:
		//pr_info("=IOCTL_GET=\n");
		if (!access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd))) {
			retval = -EFAULT;
			goto done;
		}

		if (!access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd))) {
			retval = -EFAULT;
			goto done;
		}

		//pr_info("sizeof(struct ioctl_cmd168)=%d\n", sizeof(struct ioctl_cmd168));
		if ( copy_from_user(&data, (int __user *)arg, sizeof(struct ioctl_cmd168)) ) {
			retval = -EFAULT;
			goto done;
		}

		//pr_info("=================================================\n");
		//pr_info("name[%s]---addr[%x]-flags[%d]=\n",g_ts_state->client->name,g_ts_state->client->addr,g_ts_state->client->flags);
		//read_lock(&dev->lock);
		retval = ite7260_read(g_ts_state->client,
				(unsigned char) data.bufferIndex, (unsigned char*) buffer,
				(unsigned char) data.length);
		//read_unlock(&dev->lock);
		//pr_info("GET:retval=%x\n", retval);
		retval = 0;
		for (i = 0; i < data.length; i++) {
			data.buffer[i] = (unsigned short) buffer[i];
		}

		if ( copy_to_user((int __user *)arg, &data, sizeof(struct ioctl_cmd168)) ) {
			retval = -EFAULT;
			goto done;
		}
		break;

	default:
		retval = -ENOTTY;
		break;
	}

	done:
	//pr_info("DONE! retval=%d\n", retval);
	return (retval);
}

int ite7260_open(struct inode *inode, struct file *filp) {
	int i;
	struct ite7260_data *dev;

	pr_info("=ite7260_open=\n");
	dev = kmalloc(sizeof(struct ite7260_data), GFP_KERNEL);
	if (dev == NULL) {
		return -ENOMEM;
	}

	/* initialize members */
	rwlock_init(&dev->lock);
	for (i = 0; i < MAX_BUFFER_SIZE; i++) {
		dev->buffer[i] = 0xFF;
	}

	filp->private_data = dev;

	return 0; /* success */
}

int ite7260_close(struct inode *inode, struct file *filp) {
	struct ite7260_data *dev = filp->private_data;

	if (dev) {
		kfree(dev);
	}

	return 0;
}

struct file_operations ite7260_fops = { 
	.owner = THIS_MODULE, 
	.open = ite7260_open, 
	.release = ite7260_close, 
	.ioctl = ite7260_ioctl, 
};


static int __devinit ite7260_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct touch_ite_state* ts_state;
	struct device *dev = &client->dev;
	struct input_dev* input_device;
	int ret = 0;
	struct touch_platform_data* pdata = (struct touch_platform_data*)client->dev.platform_data;

	if (pdata)
	{
		if (pdata->power) pdata->power(TOUCH_POWER_ON);
		if (pdata->reset) pdata->reset();
	}

	ts_state = kzalloc(sizeof(struct touch_ite_state),GFP_KERNEL);
	if(!ts_state)
	{
		dev_err(dev,"failed to create touch nas state\n");
		return -ENOMEM;
	}
	
	g_ts_state = ts_state;
	ts_state->client=client;
	ts_state->dev = dev;
	ts_state->pdata = pdata;

	if (ite7260_show_info(ts_state))
	{
		printk("Failed reading ite touch info, abort.\n");
		kfree(ts_state);
		return -1;
	}

	ts_state->wq = create_singlethread_workqueue("touchpanel");
	INIT_DELAYED_WORK(&ts_state->delayed_work, touch_delayed_work);
	
	//set gpio and request irq	
	ts_state->irq = client->irq;	
	ts_state->gpio = irq_to_gpio(ts_state->irq);
	//gpio_direction_input(ts_state->gpio);
#if !HRTIMER_POLL
	/* falling edge interrupt */
	ret = request_irq(ts_state->irq, 
			ite7260_touch_irq,
			IRQF_TRIGGER_FALLING, 
			"ITE7260",
			ts_state);
	if (ret) {
		dev_dbg(dev,"Request IRQ for touch failed (%d).\n", ret);
		goto err1;
	}
	//disable_irq_nosync(ts_state->irq);
#else	
	hrtimer_init(&ts_state->timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
	ts_state->timer.function = ite7260_timer_func;
	//hrtimer_start(&ts_state->timer, ktime_set(0, 1000000L*interval), HRTIMER_MODE_REL);
#endif

	/* tell input system what we events we accept and register */
	input_device= input_allocate_device();
	if (NULL==input_device  ) {
		dev_err(dev,"%s: failed to allocate input dev\n", __FUNCTION__);
		ret = -ENOMEM;
		goto err1;
	}

	input_device->name = "touch_ite";
	input_device->phys = "touch_ite/input1";
	input_device->dev.parent  = dev;

	input_device->open = touch_ite_input_open;
	input_device->close = touch_ite_input_close;
	input_device->id.bustype = BUS_HOST;
	input_device->id.vendor  = 0xdead;
	input_device->id.product = 0xdead;
	input_device->evbit[0]= BIT_MASK(EV_SYN)|BIT_MASK(EV_KEY)|BIT_MASK(EV_ABS);
	input_device->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);

	
	ts_state->x_max= ITE_TS_WIDTH;
	ts_state->y_max = ITE_TS_HEIGHT;	
	ts_state->quirks = TOUCH_QUIRKS_X_INVERTED;

	input_set_abs_params(input_device, ABS_MT_POSITION_X, 0, ITE_TS_WIDTH, 0, 0);
	input_set_abs_params(input_device, ABS_MT_POSITION_Y, 0, ITE_TS_HEIGHT, 0, 0);
	input_set_abs_params(input_device, ABS_MT_TOUCH_MAJOR, 0, 0xf, 0, 0);
	input_set_abs_params(input_device, ABS_MT_WIDTH_MAJOR, 0, 0xf, 0, 0);

	ts_state->idev = input_device;
	input_set_drvdata(input_device,ts_state);	
	ret = input_register_device(input_device);
	if (ret) {
		dev_err(dev,"%s: unabled to register input device, ret = %d\n",
				__FUNCTION__, ret);
		goto err2;
	}
	
#if defined(CONFIG_HAS_EARLYSUSPEND)
	ts_state->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN+1;
	ts_state->early_suspend.suspend = ite7260_earlysuspend;
	ts_state->early_suspend.resume = ite7260_lateresume;
	if (0==(ts_state->pdata->quirks&TOUCH_QUIRKS_BREAK_SUSPEND))
	{
		register_early_suspend(&ts_state->early_suspend);
	}
#endif

	i2c_set_clientdata(client, ts_state);

	ret = device_create_file(dev, &dev_attr_cal);
	if (ret)
		printk("Error attaching calibration attribute file.\n");

	ret = device_create_file(dev, &dev_attr_info);
	if (ret)
		printk("Error attaching information attribute file.\n");
	
#if HRTIMER_POLL
	printk("ITE7260 multi-touch is OK. Working in poll mode, poll interval: %d\n", interval);
#else
	//check if touch event already triggered
	if (gpio_get_value(ts_state->gpio)==0) 
	{
		queue_delayed_work(ts_state->wq, &ts_state->delayed_work, msecs_to_jiffies(20));
	}

	printk("ITE7260 multi-touch is OK. Working in interrupt mode\n");
#endif

	return 0;

err2:
	input_free_device(input_device);	
err1:	
	kfree(ts_state);
	return ret;
}

static int ite7260_remove(struct i2c_client *client)
{
	struct touch_ite_state *ts_state = i2c_get_clientdata(client);

#if defined(CONFIG_HAS_EARLYSUSPEND)
	if (0==(ts_state->pdata->quirks&TOUCH_QUIRKS_BREAK_SUSPEND))
	{
		unregister_early_suspend(&ts_state->early_suspend);
	}
#endif
	
	if(ts_state->idev)
	{
#if HRTIMER_POLL
		hrtimer_cancel(&ts_state->timer);
#else
		free_irq(ts_state->irq, ts_state);
#endif
		cancel_delayed_work_sync(&ts_state->delayed_work);
		destroy_workqueue(ts_state->wq);
		input_unregister_device(ts_state->idev);
	}

	device_remove_file(ts_state->dev, &dev_attr_cal);
	device_remove_file(ts_state->dev, &dev_attr_info);
	
	//free my data
	kfree(ts_state);
	
	return 0;
}
 
static struct i2c_device_id ite7260_idtable[] = { 
	{ "ite7260", 0 }, 
	{ } 
}; 

static struct i2c_driver ite7260_driver = {
	.driver = {
		.name 	= "ite7260",
	},
	.id_table	= ite7260_idtable,
	.probe		= ite7260_probe,
	.remove		= __devexit_p(ite7260_remove),
};

static int __init touch_ite_init(void)
{
	int ret;
	dev_t dev = MKDEV(ite7260_major, 0);
	struct device *class_dev = NULL;

	printk("ITE7260 Multi-touch driver\n");

	ret = alloc_chrdev_region(&dev, 0, 1, DEVICE_NAME);
	if (ret) {
		printk("IT7260 cdev can't get major number\n");
		return -1;
	}
	ite7260_major = MAJOR(dev);

	// allocate the character device
	cdev_init(&ite7260_cdev, &ite7260_fops);
	ite7260_cdev.owner = THIS_MODULE;
	ite7260_cdev.ops = &ite7260_fops;
	ret = cdev_add(&ite7260_cdev, MKDEV(ite7260_major, ite7260_minor), 1);
	if(ret) {
		return -1;
	}

	// register class
	ite7260_class = class_create(THIS_MODULE, DEVICE_NAME);
	if(IS_ERR(ite7260_class)) {
		printk("Err: failed in creating class.\n");
		return -1;
	}

	ite7260_dev = MKDEV(ite7260_major, ite7260_minor);
	class_dev = device_create(ite7260_class, NULL, ite7260_dev, NULL, DEVICE_NAME);
	if(class_dev == NULL)
	{
		printk("Err: failed in creating device.\n");
		return -1;
	}

	return i2c_add_driver(&ite7260_driver);
}

static void __exit touch_ite_exit(void)
{
	dev_t dev = MKDEV(ite7260_major, ite7260_minor);

	device_destroy(ite7260_class, ite7260_dev);
	class_destroy(ite7260_class);

	cdev_del(&ite7260_cdev);
	unregister_chrdev_region(dev, 1);

	i2c_del_driver(&ite7260_driver);
}

module_init(touch_ite_init);
module_exit(touch_ite_exit);

MODULE_DESCRIPTION("ITE7260 Multi-touch driver");
MODULE_AUTHOR("Guang");
MODULE_LICENSE("GPL");

