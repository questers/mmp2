/*
 * touch_nas.c
 * NAS capacitive touch panel driver
 *
 * Copyright (C) 2011 Questers Tech,Ltd.ALL RIGHTS RESERVED.
 *  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/i2c.h>
#include <linux/input/questers_ts.h>
#include <plat/mfp.h>
#include "tslib.h"

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif

/*
 *  There is 3 modes to cover most of touch panel parameters
 *  MODE_TSLIB_CALIBRATING   ,only report single point data ,use for ts lib calibration.
 *  MODE_TSLIB_CALIBRATED         ,report multi point data with ts linear translation,    If panel resolution different from screen resolution,use calibrated data get from calibration mode.
 *  MODE_RAW   ,report multi point data without ts linear translation,If panel can report screen resolution data.use this mode
 *  MODE_SELF_CALIBRATION ,capacitive self calibration for capacitance drift .It is one time required if touch panel position changed.
*/ 
#define MODE_TSLIB_CALIBRATING  	0
#define MODE_TSLIB_CALIBRATED     	1
#define MODE_RAW  					2
#define MODE_SELF_CALIBRATION		4

/*
39531 -997 -721256 183 -53454 39942936 65536 800 600
39547 0 -1468909 0 -52012 39165562 65536 800 600
*/
#define TSLIB_PARAM_A 39547 
#define TSLIB_PARAM_B 0
#define TSLIB_PARAM_C -1468909
#define TSLIB_PARAM_D 0
#define TSLIB_PARAM_E -52012
#define TSLIB_PARAM_F 39165562
#define TSLIB_PARAM_S 65536


#define NAS_MODE_OFFSET			0x14

#define NAS_MODE_ACTIVE			0
#define NAS_MODE_SLEEP			1
#define NAS_MODE_DEEP_SLEEP 	2
#define NAS_MODE_FREEZE			3
#define NAS_MODE_MASK			0x3
#define NAS_MODE_ALLOW_SLEEP	0x4

#define SCREEN_WIDTH      800
#define SCREEN_HEIGHT     600 

#define NAS_CMD_SELFCAL 	1
#define NAS_CMD_TPENABLE	2
#define NAS_CMD_INIT		3
#define NAS_CMD_PROBEINIT	4
#define NAS_CMD_SUSPEND		5

#define NAS_CMD_STATUS_IDLE	0
#define NAS_CMD_STATUS_BUSY	1


#define NAS_USER_OFFSET		330
#define NAS_USER_BIT_CAL	0x01
#define NAS_USER_BIT_RES	0x02
#define NAS_USER_BIT_MODE	0x04


#define NAS_IDLE_TIME_MS	60000		
static int mode=MODE_RAW;//MODE_TSLIB_CALIBRATED;

module_param(mode, int, S_IRUGO|S_IWUSR);
MODULE_PARM_DESC(mode, "0: calibration mode; 1: multitouch with tslib linear; 2: multitouch without tslib linear");

enum {
	DEBUG_RAW_REPORT = 1U << 0,
	DEBUG_TSLIB_REPORT = 1U << 1,
	DEBUG_TSLIBCAL_REPORT = 1U << 2,
};
static int debug_mask = 0;//DEBUG_RAW_REPORT;
module_param_named(debug_mask, debug_mask, int, S_IRUGO | S_IWUSR | S_IWGRP);


struct nas_touch_device{
	struct i2c_client* client;
	struct device* my_dev;
	struct input_dev *idev;

	struct touch_platform_data *pdata;
	struct workqueue_struct* wq;
	struct delayed_work delayed_work;

	struct delayed_work command_work;
	u8	   nas_command;
	u8	   nas_command_status;

	u8 fingers;//old fingers

	struct timer_list idle_timer;

	//irq number
	int irq;

	//reference counter touch pad device
	int  ref_count;

	//tslib integration
	struct tsdev* tsdev;

	
	struct early_suspend    early_suspend;
	
};

static int nastouch_write(struct i2c_client *client, u8* msg, u8 uCnt)
{	
	return i2c_master_send(client, msg, uCnt);
}
static int nastouch_read(struct i2c_client *client,u8 reg,u8* buf, u8 uCnt)
{
	int ret;	
	ret = nastouch_write(client, &reg, 1);
	if(ret<0) return ret;
	return i2c_master_recv(client, buf, uCnt);
}



//for tslib integration
static int nastouch_callback(struct ts_sample* samp,int nr,void* userdata)
{
	struct nas_touch_device *nas = (struct nas_touch_device*)userdata;
	struct i2c_client* client = nas->client;
	u8 fingers;
	u8 buf[10];
	int ret;
	int x0,y0,x1,y1;
	x0 = y0 = x1 = y1 = 0;

	if(nr<2) {printk(KERN_ERR"nastouch_callback ,at least 2 samples space required.\n");return 0;}
	ret = nastouch_read(client, 0x0,buf,10);
	if(ret<0)
	{		
		printk("nastouch_raw_report: Finger Read Error ret=%d \n",ret);
		return 0;
	}
	fingers = buf[0];
	if(fingers>2) fingers=0;
	switch(fingers)
	{
		default:
		case 0:
			samp[0].p = 0;
			samp[1].p = 0;
			break;
		case 1:
			x0 = (buf[3]<<8)+buf[2];
			y0 = (buf[5]<<8)+buf[4];
			samp[0].x = x0;
			samp[0].y = y0;
			samp[0].p = 0xfff;
			samp[1].p = 0;
			if(DEBUG_RAW_REPORT&debug_mask)
			{
				printk("nastouch_callback:x0,y0[%d,%d]\n",x0,y0);
			}
			break;
		case 2:
			x0 = (buf[3]<<8)+buf[2];
			y0 = (buf[5]<<8)+buf[4];			
			x1 = (buf[7]<<8)+buf[6];
			y1 = (buf[9]<<8)+buf[8];	
			samp[0].x = x0;
			samp[0].y = y0;
			samp[0].p = 0xfff;
			samp[1].x = x1;
			samp[1].y = y1;
			samp[1].p = 0xfff;
			if(DEBUG_RAW_REPORT&debug_mask)
			{
				printk("nastouch_callback:x0,y0[%d,%d] x1,y1[%d,%d]\n",x0,y0,x1,y1);
			}
			break;
	}
	return nr;
	
}

//in this scenario,we just process first point data
static void nastouch_tslibcal_report(struct nas_touch_device *nas)
{
	struct i2c_client* client = nas->client;
	u16 x0,y0;
 	int ret;
	u8 fingers;
	u8 buf[10];

	x0 = y0 = 0;

	ret = nastouch_read(client, 0x0,buf,10);
	if(ret<0)
	{		
		printk("nastouch_tslibcal_report: Finger Read Error ret=%d \n",ret);
		return;
	}
	fingers = buf[0];
	if(fingers>2) fingers=0;
	if(fingers>0)
	{
		x0 = (buf[3]<<8)+buf[2];
		y0 = (buf[5]<<8)+buf[4];
		if(DEBUG_TSLIBCAL_REPORT&debug_mask)
		{
			printk("nastouch_tslibcal_report: X = %d , Y = %d\n",x0,y0);
		}
		
		input_report_abs(nas->idev, ABS_X, x0 );
		input_report_abs(nas->idev, ABS_Y, y0 );	
		input_report_abs(nas->idev, ABS_PRESSURE, 0xfff);
		input_report_key(nas->idev, BTN_TOUCH,1);			
	}
	else 
	{
		input_report_abs(nas->idev, ABS_PRESSURE, 0);
		input_report_key(nas->idev, BTN_TOUCH,0);			
	}
	//sync
	input_sync(nas->idev);		

}
static void nastouch_tslib_report(struct nas_touch_device *nas)
{
	struct ts_sample samples[2];
	int fingers=0;
	ts_read(nas->tsdev,samples,2);
	if(samples[0].p) fingers++;
	if(samples[1].p) fingers++;
	if(fingers>0)
	{
		int x0,y0,x1,y1;
		
		x0 = samples[0].x;
		y0 = samples[0].y;
		if(DEBUG_TSLIB_REPORT&debug_mask)
		{
			printk("nastouch_tslib_report: fingers = %d\n",fingers);
			printk("nastouch_tslib_report: X0 = %d , Y0 = %d\n",x0,y0);
		}
		input_report_abs(nas->idev, ABS_MT_POSITION_X, x0);
		input_report_abs(nas->idev, ABS_MT_POSITION_Y, y0);
		input_report_abs(nas->idev, ABS_MT_TOUCH_MAJOR,0xff);
		input_report_abs(nas->idev, ABS_MT_WIDTH_MAJOR,0xf);
		input_mt_sync(nas->idev);
		
		if(fingers>1)
		{
			x1 = samples[1].x;
			y1 = samples[1].y;	
			
			if(DEBUG_TSLIB_REPORT&debug_mask)
			{
				printk("nastouch_tslib_report: X1 = %d , Y1 = %d\n",x1,y1);
			}
			input_report_abs(nas->idev, ABS_MT_POSITION_X, x1);
			input_report_abs(nas->idev, ABS_MT_POSITION_Y, y1);
			input_report_abs(nas->idev, ABS_MT_TOUCH_MAJOR,0xff);
			input_report_abs(nas->idev, ABS_MT_WIDTH_MAJOR,0xf);
			input_mt_sync(nas->idev);			
		}
		/*
		else if(nas->fingers>fingers)
		{
			input_report_abs(nas->idev, ABS_MT_TOUCH_MAJOR,0);
			input_report_abs(nas->idev, ABS_MT_WIDTH_MAJOR,0);
			input_mt_sync(nas->idev);				
		}
		*/
	}
	else if(nas->fingers) //all fingers up
	{
	
		if(DEBUG_TSLIB_REPORT&debug_mask)
		{
			printk("nastouch_tslib_report: all fingers up\n");
		}
		input_report_abs(nas->idev, ABS_MT_TOUCH_MAJOR,0);
		input_report_abs(nas->idev, ABS_MT_WIDTH_MAJOR,0);
		input_mt_sync(nas->idev);				
	}
	
	
	input_sync(nas->idev);
	
	nas->fingers = fingers;	
	
	
}

static inline void nastouch_raw_report(struct nas_touch_device *nas)
{
	struct i2c_client* client = nas->client;
	u16 x0,y0,x1,y1,s0,s1;
 	int ret;
	u8 fingers;
	u8 buf[14];

	x0 = y0 = x1 = y1 = 0;

	ret = nastouch_read(client, 0x0,buf,14);
	if(ret<0)
	{		
		printk("nastouch_raw_report: Finger Read Error ret=%d \n",ret);
		return;
	}
	fingers = buf[0];
	if(fingers>2) fingers=0;
	if(fingers>0)
	{
		//swap x,y for android coordernate portrait mode
		y0 = (buf[3]<<8)+buf[2];
		x0 = (buf[5]<<8)+buf[4];
		s0 = buf[10]*buf[11];
		
		if(DEBUG_RAW_REPORT&debug_mask)
		{
			printk("nastouch_raw_report: fingers = %d\n",fingers);
			printk("nastouch_raw_report: X0 = %d , Y0 = %d ,S0 = %d \n",x0,y0,s0);
		}		
		
		input_report_abs(nas->idev, ABS_MT_POSITION_X, x0);
		input_report_abs(nas->idev, ABS_MT_POSITION_Y, y0);
		input_report_abs(nas->idev, ABS_MT_TOUCH_MAJOR,0xff);
		input_report_abs(nas->idev, ABS_MT_WIDTH_MAJOR,s0);
		input_mt_sync(nas->idev);
		
		if(fingers>1)
		{		
			//swap x,y for android coordernate portrait mode
			y1 = (buf[7]<<8)+buf[6];
			x1 = (buf[9]<<8)+buf[8];	
			s1 = buf[12]*buf[13];
			if(DEBUG_RAW_REPORT&debug_mask)
			{
				printk("nastouch_raw_report: X1 = %d , Y1 = %d, S1 = %d\n",x1,y1,s1);
			}
			input_report_abs(nas->idev, ABS_MT_POSITION_X, x1);
			input_report_abs(nas->idev, ABS_MT_POSITION_Y, y1);
			input_report_abs(nas->idev, ABS_MT_TOUCH_MAJOR,0xff);
			input_report_abs(nas->idev, ABS_MT_WIDTH_MAJOR,s1);
			input_mt_sync(nas->idev);			
		}
	}
	else if(nas->fingers) //all fingers up
	{
		
		if(DEBUG_RAW_REPORT&debug_mask)
		{
			printk("all fingers up\n");
		}
		input_report_abs(nas->idev, ABS_MT_TOUCH_MAJOR,0);
		input_report_abs(nas->idev, ABS_MT_WIDTH_MAJOR,0);
		input_mt_sync(nas->idev);				
	}
	
	
	input_sync(nas->idev);
	
	nas->fingers = fingers;

	
}

static int nastouch_eeprom_read(struct i2c_client *client,u16 offset,u8* buf, u8 uCnt)
{
	u8 msg[3];

	#if 0
	//write to special op register 0x00 to switch to normal mode
	msg[0] = 0x37;
	msg[1] = 0x00;
	i2c_master_send(client,msg,2);
	#endif

	//write offset to eeprom read address
	msg[0] = 0x38;
	msg[1] = offset&0xff;
	msg[2] = (offset>>8)&0xff;	
	i2c_master_send(client,msg,3);

	msleep(100);
	//write to special op register 0x01
	msg[0] = 0x37;
	msg[1] = 0x01;
	i2c_master_send(client,msg,2);

	msleep(100);
	
	return i2c_master_recv(client, buf, uCnt);
	
}
static int nastouch_eeprom_write(struct i2c_client *client, u16 offset,u8* value, u8 uCnt)
{
	int retry=50,write_retry=10;
	int ret, i;
	int verify_failed=1;
	u8  write_bytes[16],verify_bytes[16];
	u8  write_cnt;
	u8 msg[3];
	if(uCnt>16)
	{
		printk("maximum eeprom write bytes is 16\n");
		return -1;
	}
	write_cnt=uCnt;
	memcpy(write_bytes,value,uCnt);
	do{
		printk("write eeprom byte %d times\n",11-write_retry);
		//write to special op register 0x02
		msg[0] = 0x37;
		msg[1] = 0x02;
		do{
			ret = i2c_master_send(client,msg,2);
			if(ret!=2)
			{
				printk("failed to switch eeprom write mode,[%d]\n",ret);		
			}
			msleep(100);
		}while(2!=ret&&retry--);
		if(ret!=2)
		{
			printk("failed to switch eeprom write mode,[%d]\n",ret);	
			return -1;
		}

		

		msleep(1000);
		
		printk("eeprom write mode\n");
		//special op to eeprom write mode
		//write eeprom byte
		for(i=0;i<uCnt;i++)
		{
			msg[0] = (offset+i)&0xff;//address low  byte
			msg[1] = ((offset+i)>>8)&0xff;//address high byte
			msg[2] = write_bytes[i];
			printk("write eeprom in offset %d,value=%d\n",offset+i,msg[2]);
			if(3!=i2c_master_send(client,msg,3))
			{
				printk("failed to write eeprom data in offset %d\n",offset+i);
			}
			//waiting for erase cycle finished
			//160us for 1 byte
			//200ms for 1 block erase,one block is 512 bytes
			//200ms+512*160us=200ms+81.920ms=300ms
			msleep(2000);		
		}

		//dummy read
		retry=50;
		do{
			ret = i2c_master_recv(client,msg,2);
			if(ret!=2)
			{
				printk("dummy read failed,[%d]\n",ret);		
			}
			msleep(1000);
		}while(2!=ret&&retry--);

		//verify write result
		memset(verify_bytes,0,write_cnt);
		nastouch_eeprom_read(client,offset,verify_bytes,write_cnt);
		for(i=0;i<write_cnt;i++)
		{
			if(verify_bytes[i]!=write_bytes[i])
			{
				verify_failed=1;
				break;
			}
		}
		if(i>=write_cnt) verify_failed = 0;

	}while(verify_failed&&write_retry--);
	

	msleep(5000);			

	return uCnt;
	
}


static int nastouch_set_resolution(struct i2c_client *client,short w,short h)
{	
	int ret;
	short x,y;
	u8	msg[4];
	short x_request,y_request;
	x_request = w;
	y_request = h;
	ret = nastouch_eeprom_read(client,301,msg,4);
	if(ret<0) return ret;
	x = msg[0]|(msg[1]<<8);
	y = msg[2]|(msg[3]<<8);

	if((x_request!=x)||
		(y_request!=y))
	{
		printk("request x[0x%x,%d],y[0x%x,%d]\n",
			x_request,x_request,
			y_request,y_request);
		msg[0] = x_request&0xff;
		msg[1] = (x_request>>8)&0xff;
		msg[2] = y_request&0xff;
		msg[3] = (y_request>>8)&0xff;	
		
		ret = nastouch_eeprom_write(client,301,msg,4);	
		
		ret = nastouch_eeprom_read(client,301,msg,4);
		x = msg[0]|(msg[1]<<8);
		y = msg[2]|(msg[3]<<8);
		printk("screen resolution change to %dx%d\n",x,y);
	}

	return ret;
	
}
static int nastouch_self_cal(struct i2c_client *client)
{
	u8	msg[4];
	int error;

	//one time calibration
	msg[0]=0x37;
	msg[1]=0x03;		
	error=nastouch_write(client,msg,2);
	if(error<0)
	{
			printk("|	Calibration : Command Error\n");
			printk("|	Calibration : error = 0x%x\n",error);
	}
	else
	{
			printk("|	Calibration : Command OK\n");
			printk("|	Calibration : error = 0x%x\n",error);
	}
	return error;
}

static int nastouch_enable_autosleep(struct i2c_client *client)
{
	int ret;
	u8 buf[2];
	//to save power ,enable auto sleep mode
	buf[0] = NAS_MODE_OFFSET;
	buf[1] = 0;//NAS_MODE_ALLOW_SLEEP|0x10;//period 10 ,allow sleep , |0xA0,0x00;//
	ret = nastouch_write(client,buf,2);
	printk("nas autosleep %s\n",(ret>0)?"okay":"failed");
	return ret;
}
static void nastouch_command(struct nas_touch_device* nas,u8 cmd,int delay_ms)
{
	nas->nas_command_status = NAS_CMD_STATUS_BUSY;
	nas->nas_command = cmd;			
	queue_delayed_work(nas->wq, &nas->command_work,msecs_to_jiffies(delay_ms));	
}
static void nastouch_command_handler(struct work_struct* work)
{
	struct nas_touch_device *nas = (struct nas_touch_device*)container_of(work,struct nas_touch_device,command_work.work);	
	int ret;
	switch(nas->nas_command)
	{	
		case NAS_CMD_SELFCAL:
			{
				u8 buf=0;
				//screen resolution 
				ret = nastouch_set_resolution(nas->client,SCREEN_WIDTH,SCREEN_HEIGHT);
				if(ret>0)
					buf|=NAS_USER_BIT_RES;
				ret = nastouch_self_cal(nas->client);		
				if(ret>0)
					buf|=NAS_USER_BIT_CAL;	
				ret = nastouch_enable_autosleep(nas->client);				
				if(ret>0)
					buf|=NAS_USER_BIT_MODE;	
				nastouch_eeprom_write(nas->client,NAS_USER_OFFSET,&buf,1);
				
				nastouch_command(nas,NAS_CMD_TPENABLE,0);
			}
			break;
		case NAS_CMD_TPENABLE:
			{
				printk("touch panel enable now\n");
				enable_irq(nas->irq);
			}
			break;
		case NAS_CMD_INIT:			
			{
				//reset TP?
				if(nas->pdata&&nas->pdata->reset)
					nas->pdata->reset();
				nastouch_command(nas,NAS_CMD_TPENABLE,0);
			}
			break;
		case NAS_CMD_PROBEINIT:
			{
				/*
				 * In user custom area ,check if this is a panel calibrated,if it is not calibrated,do it.
				*/
				u8 buf[1];
				ret = nastouch_eeprom_read(nas->client,NAS_USER_OFFSET,buf,1);
				if(ret>0)
				{	
					u8 old_buf=buf[0];
					printk("touchpanel custom=%d\n",buf[0]);
					if(0==(buf[0]&NAS_USER_BIT_RES))
					{
						ret = nastouch_set_resolution(nas->client,SCREEN_WIDTH,-SCREEN_HEIGHT);
						if(ret>0)
							buf[0]|=NAS_USER_BIT_RES;
					}
					if(0==(buf[0]&NAS_USER_BIT_CAL))
					{
						ret = nastouch_self_cal(nas->client);
						if(ret>0)
							buf[0]|=NAS_USER_BIT_CAL;							
					}
					if(0==(buf[0]&NAS_USER_BIT_MODE))
					{
						ret = nastouch_enable_autosleep(nas->client);
						if(ret>0)
							buf[0]|=NAS_USER_BIT_MODE;							
					}


					//write to eeprom if not matched
					if(old_buf!=buf[0])
					{
						nastouch_eeprom_write(nas->client,NAS_USER_OFFSET,buf,1);
					}
						
				}
				nastouch_command(nas,NAS_CMD_TPENABLE,0);
			}
			break;
		case NAS_CMD_SUSPEND:
			{
				u8 buf[2];
				int ret;
				//enter freeze mode 
				disable_irq(nas->irq);
				//flush_delayed_work(&nas->command_work);
				//flush_delayed_work(&nas->delayed_work);
				buf[0]=NAS_MODE_OFFSET;
				buf[1]=NAS_MODE_FREEZE;
				ret=nastouch_write(nas->client,buf,2);
				printk("nastouch suspended %s\n",(ret>0)?"okay":"failed");					
				
			}
			break;
		default:break;
	}

	nas->nas_command_status = NAS_CMD_STATUS_IDLE;
	
}

static void nastouch_work(struct work_struct* work)
{
	struct nas_touch_device *nas = (struct nas_touch_device*)container_of(work,struct nas_touch_device,delayed_work.work);	
	if(MODE_TSLIB_CALIBRATED==mode)
	{
		nastouch_tslib_report(nas);
	}
	else if(MODE_TSLIB_CALIBRATING==mode)//calibration mode
	{
		nastouch_tslibcal_report(nas);
	}
	else
	{
		nastouch_raw_report(nas);
	}

	//enable idle timer
	mod_timer(&nas->idle_timer,jiffies+msecs_to_jiffies(NAS_IDLE_TIME_MS));

	//enable_irq(nas->irq);
}

static void nastouch_idletimer_callback(unsigned long data)
{
	#if 0
	struct nas_touch_device *nas = (struct nas_touch_device*)data;
	//reset tp
	disable_irq(nas->irq);
	nastouch_command(nas,NAS_CMD_INIT,0);	
	#endif
}

static ssize_t nastouch_show(struct device *dev,
                          struct device_attribute *attr, char *buf)
{
	struct i2c_client* client = to_i2c_client(dev);
	u8 buffer[24];
	int ret,result;
	result = nastouch_read(client, 0x0,buffer,24);
	if(result>0)
	{
		int i;
		ret=0;
		for(i=0;i<result;i++)
			ret+= sprintf(buf+ret,"%d,%d\n",i,buffer[i]);
		ret+=sprintf(buf+ret,"%s\n"," ==end==");
	}
	else
	{
		ret = sprintf(buf,"%s\n","can not access nas touchpanel");
	}

	return ret;
}

static ssize_t nastouch_eeprom_show(struct device *dev,
                          struct device_attribute *attr, char *buf)
{
	struct i2c_client* client = to_i2c_client(dev);
	u8 buffer[64];
	int ret,result;
	result = nastouch_eeprom_read(client, 308,buffer,14);
	if(result>0)
	{
		int i;
		ret=0;
		for(i=0;i<result;i++)
			ret+= sprintf(buf+ret,"%d %d \n",308+i,buffer[i]);
		result = nastouch_eeprom_read(client, 330,buffer,10);
		if(result>0)
		{
			for(i=0;i<result;i++)
				ret+= sprintf(buf+ret,"%d %d \n",330+i,buffer[i]);
			
		}
		ret+=sprintf(buf+ret,"%s\n"," ==end==");
	}
	else
	{
		ret = sprintf(buf,"%s\n","can not access nas touchpanel eeprom");
	}

	return ret;
	

}
static ssize_t nastouch_eeprom_store(struct device *dev,
                           struct device_attribute *attr, const char *buf, size_t count)
{
	struct i2c_client* client = to_i2c_client(dev);
	int offset,value;
	int elements;
    elements = sscanf(buf,"%d %d",&offset,&value);
	if(elements>=2)
	{
		nastouch_eeprom_write(client,offset,(u8*)&value,1);		
	}
	return count;
	
}

static DEVICE_ATTR(touch, S_IRUGO|S_IWUGO,nastouch_show, NULL);
static DEVICE_ATTR(eeprom, S_IRUGO|S_IWUGO,nastouch_eeprom_show, nastouch_eeprom_store);

/*
 * Start the touchscreen thread and
 * the touch digitiser.
 */
static int nastouch_open(struct input_dev *idev)
{

	struct nas_touch_device *nas = (struct nas_touch_device*)input_get_drvdata(idev);


	if (nas->ref_count++ > 0)
		return 0;
 
	dev_dbg(nas->my_dev,"Touch is opened. Use count: %d\n", nas->ref_count);

 	return 0;
}

/*
 * Kill the touchscreen thread and stop
 * the touch digitiser.
 */
static void nastouch_close(struct input_dev *idev)
{
	struct nas_touch_device *nas = (struct nas_touch_device *) input_get_drvdata(idev);

	dev_dbg(nas->my_dev,"close ts input!\n");
}



static irqreturn_t nastouch_isr(int irq, void *irq_data)
{
	struct nas_touch_device *nas = (struct nas_touch_device*)irq_data;

	//disable_irq_nosync(nas->irq);
	cancel_delayed_work(&nas->delayed_work);
	queue_delayed_work(nas->wq, &nas->delayed_work,msecs_to_jiffies(0));
 
	return IRQ_HANDLED;
}


static int nastouch_suspend(struct i2c_client* client,pm_message_t state)
{
	struct nas_touch_device *nas = i2c_get_clientdata(client);	
	nastouch_command(nas,NAS_CMD_SUSPEND,0);	
	return 0;
}

static int nastouch_resume(struct i2c_client* client)
{
	struct nas_touch_device *nas = i2c_get_clientdata(client);	
	nastouch_command(nas,NAS_CMD_INIT,0);	
	return 0;
}

#ifdef CONFIG_HAS_EARLYSUSPEND
//TODO early suspend/resume support
static void nastouch_early_suspend(struct early_suspend *h)
{
	struct nas_touch_device *nas = container_of(h, struct nas_touch_device, early_suspend);
	nastouch_suspend(nas->client, PMSG_SUSPEND);

}
static void nastouch_late_resume(struct early_suspend *h)
{
	struct nas_touch_device *nas = container_of(h, struct nas_touch_device, early_suspend);
	nastouch_resume(nas->client);

}
#endif

static int __devinit nastouch_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct nas_touch_device* nas;
	struct device *dev = &client->dev;
	struct input_dev* input_device;
	int ret = 0;		
	struct touch_platform_data *nas_pdata = (struct touch_platform_data *)client->dev.platform_data;

	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
	{
		printk(KERN_ERR"nastouch_probe: need I2C_FUNC_I2C\n");
		return -ENODEV;
	}

	if(nas_pdata)
	{
		if(nas_pdata->power)
			nas_pdata->power(TOUCH_POWER_ON);
		//reset now
		if(nas_pdata->reset)
			nas_pdata->reset();
			
	}
	
	nas = kzalloc(sizeof(struct nas_touch_device),GFP_KERNEL);
	if(!nas)
	{
		dev_err(dev,"failed to create touch nas state\n");
		return -ENOMEM;
	}
	nas->client=client;
	nas->my_dev = dev;
	nas->pdata = nas_pdata;
	nas->fingers = 0;

	
	nas->wq = create_singlethread_workqueue("touchpanel");
	
	INIT_DELAYED_WORK(&nas->delayed_work,nastouch_work);

	/* tell input system what we events we accept and register */
	input_device= input_allocate_device();
	if (NULL==input_device  ) {
		dev_err(dev,"%s: failed to allocate input dev\n", __FUNCTION__);
		ret = -ENOMEM;
		goto err1;
	}

	input_device->name = "touch_nas";
	input_device->phys = "touch_nas/input1";
	input_device->dev.parent  = dev;

	input_device->open = nastouch_open;
	input_device->close = nastouch_close;
	input_device->id.bustype = BUS_I2C;
	input_device->id.vendor  = 0xdead;
	input_device->id.product = 0xdead;
	
	if (mode == MODE_TSLIB_CALIBRATING)
	{
		input_device->evbit[0]= BIT_MASK(EV_SYN)|BIT_MASK(EV_KEY)|BIT_MASK(EV_ABS);
		input_device->keybit[BIT_WORD(BTN_TOUCH)] = BIT_MASK(BTN_TOUCH);
		input_set_abs_params(input_device,ABS_X,0,0xffff,0,0);
		input_set_abs_params(input_device,ABS_Y,0,0xffff,0,0);
		input_set_abs_params(input_device,ABS_PRESSURE,0,0xffff,0,0);
	}
	else
	{
		input_device->evbit[0]= BIT_MASK(EV_SYN)|BIT_MASK(EV_KEY)|BIT_MASK(EV_ABS);
		input_device->absbit[BIT_WORD(ABS_MT_POSITION_X)] |= BIT_MASK(ABS_MT_POSITION_X);
		input_device->absbit[BIT_WORD(ABS_MT_POSITION_Y)] |= BIT_MASK(ABS_MT_POSITION_Y);
		input_device->absbit[BIT_WORD(ABS_MT_TOUCH_MAJOR)] |= BIT_MASK(ABS_MT_TOUCH_MAJOR);
		input_device->absbit[BIT_WORD(ABS_MT_WIDTH_MAJOR)] |= BIT_MASK(ABS_MT_WIDTH_MAJOR);
		
		input_set_abs_params(input_device, ABS_MT_POSITION_X, 0, SCREEN_HEIGHT, 0, 0);
		input_set_abs_params(input_device, ABS_MT_POSITION_Y, 0,SCREEN_WIDTH , 0, 0);
		input_set_abs_params(input_device, ABS_MT_TOUCH_MAJOR, 0, 0xff, 0, 0);
		input_set_abs_params(input_device, ABS_MT_WIDTH_MAJOR, 0, 0xffff, 0, 0);
	}

	input_set_drvdata(input_device,nas);	

	nas->idev = input_device;

	//set gpio and request irq	
	nas->irq = client->irq;	
	i2c_set_clientdata(client, nas);


	if (MODE_TSLIB_CALIBRATED==mode)
	{
		nas->tsdev = ts_open("tsdev"); 	
		ts_load_module(nas->tsdev,MODULE_INPUT_RAW,NULL);	
		{
			char params[160];		
			sprintf(params,"a=%d b=%d c=%d d=%d e=%d f=%d s=%d w=%d h=%d",
				TSLIB_PARAM_A,TSLIB_PARAM_B,TSLIB_PARAM_C,TSLIB_PARAM_D,
				TSLIB_PARAM_E,TSLIB_PARAM_F,TSLIB_PARAM_S,SCREEN_WIDTH,
				SCREEN_HEIGHT);
			printk("calibrated params=[%s]\n",params);		
			ts_load_module(nas->tsdev,MODULE_LINEAR,params);	
		}
		//now set callback function
		ts_set_inputraw_callback(nas->tsdev,nastouch_callback,nas);
	}
	else
		nas->tsdev = 0;

#ifdef CONFIG_HAS_EARLYSUSPEND
		nas->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN+1;
		nas->early_suspend.suspend = nastouch_early_suspend;
		nas->early_suspend.resume = nastouch_late_resume;
		if(0==(nas->pdata->quirks&TOUCH_QUIRKS_BREAK_SUSPEND))
		{
			register_early_suspend(&nas->early_suspend);
		}
#endif

	ret = input_register_device(input_device);
	if (ret) {
		dev_err(dev,"%s: unabled to register input device, ret = %d\n",
				__FUNCTION__, ret);
		goto err2;
	}
	

	//falling edge interrupt
	ret = request_irq(nas->irq, nastouch_isr,
			IRQF_SHARED|IRQF_SAMPLE_RANDOM | IRQF_TRIGGER_FALLING, 
			"touch interrupt",
			nas);
	if (ret) {
		dev_dbg(dev,"Request IRQ for touch failed (%d).\n", ret);
		goto err3;
	}

	disable_irq_nosync(nas->irq);	
	INIT_DELAYED_WORK(&nas->command_work,nastouch_command_handler);
	
	if(MODE_SELF_CALIBRATION == mode)
	{
		nastouch_command(nas,NAS_CMD_SELFCAL,2000);
	}
	else
	{
		nastouch_command(nas,NAS_CMD_PROBEINIT,2000);
	}

    ret = device_create_file(&client->dev,&dev_attr_touch);
    ret = device_create_file(&client->dev,&dev_attr_eeprom);

	setup_timer(&nas->idle_timer,nastouch_idletimer_callback,(unsigned long)nas);
	//enable idle timer
	mod_timer(&nas->idle_timer,jiffies+msecs_to_jiffies(NAS_IDLE_TIME_MS));


	return 0;
err3:
	input_unregister_device(input_device);
	input_device = NULL;	
err2:
	input_free_device(input_device);	
	if(nas->tsdev) ts_close(nas->tsdev);
err1:		
	destroy_workqueue(nas->wq);
	kfree(nas);
	return ret;
}

static int nastouch_remove(struct i2c_client *client)
{
	struct nas_touch_device *nas = i2c_get_clientdata(client);

#ifdef CONFIG_HAS_EARLYSUSPEND
	 if(0==(nas->pdata->quirks&TOUCH_QUIRKS_BREAK_SUSPEND))
	 {
		 unregister_early_suspend(&nas->early_suspend);
	 }
#endif

	del_timer(&nas->idle_timer);


	device_remove_file(&client->dev,&dev_attr_touch);
	device_remove_file(&client->dev,&dev_attr_eeprom);

	cancel_delayed_work_sync(&nas->delayed_work);
	destroy_workqueue(nas->wq);
	
 	free_irq(nas->irq, nas);
	if(nas->tsdev)
		ts_close(nas->tsdev);
	
	input_unregister_device(nas->idev);	
	kfree(nas);

	dev_set_drvdata(&client->dev, NULL);
	return 0;
}
 



static struct i2c_device_id nastouch_idtable[] = { 
	{ "ntp-ax1", 0 }, 
	{ } 
}; 

static struct i2c_driver nastouch_driver = {
	.driver = {
		.name 	= "ntp-ax1",
	},
	.id_table       = nastouch_idtable,
	.probe		= nastouch_probe,
	.remove		= __devexit_p(nastouch_remove),
	
	#ifdef CONFIG_PM
	#ifndef CONFIG_HAS_EARLYSUSPEND
	.suspend  = nastouch_suspend,
	.resume   = nastouch_resume,
	#endif
	#endif
};

static int __init nastouch_init(void)
{
	if (mode == MODE_TSLIB_CALIBRATING)
		printk("ntp-ax1 driver, in calibration mode\n");
	else if (mode == MODE_TSLIB_CALIBRATED)
		printk("ntp-ax1 driver, multi touch, with tslib\n");
	else if (mode == MODE_RAW)
		printk("ntp-ax1 driver, multi touch, without tslib\n");
	
	return i2c_add_driver(&nastouch_driver);
}

static void __exit nastouch_exit(void)
{	
	i2c_del_driver(&nastouch_driver);
}

module_init(nastouch_init);
module_exit(nastouch_exit);

MODULE_DESCRIPTION("NTP-070CM-AX1 Touch Screen driver");
MODULE_AUTHOR("Ellie Cao");
MODULE_LICENSE("GPL");

