#ifndef _TSLIB_PRIVATE_H_
#define _TSLIB_PRIVATE_H_
/*
 *  tslib/src/tslib-private.h
 *
 *  Copyright (C) 2001 Russell King.
 *
 * This file is placed under the LGPL.
 *
 * $Id: tslib-private.h,v 1.3 2004/07/21 19:12:59 dlowder Exp $
 *
 * Internal touch screen library definitions.
 */
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

struct tsdev;
struct tslib_module_info;

struct tslib_vars {
	const char *name;
	void *data;
	int (*fn)(struct tslib_module_info *inf, char *str, void *data);
};

struct tslib_ops {
	int (*read)(struct tslib_module_info *inf, struct ts_sample *samp, int nr);
	int (*fini)(struct tslib_module_info *inf);
	int (*reset)(struct tslib_module_info *inf);
};

struct tslib_module_info {
	struct tsdev *dev;
	struct tslib_module_info *next;	/* next module in chain	*/
	const struct tslib_ops *ops;
};

struct tsdev {
	struct tslib_module_info *list;
	struct tslib_module_info *list_raw; /* points to position in 'list' where raw reads
					       come from.  default is the position of the
					       ts_read_raw module. */
	unsigned int res_x;
	unsigned int res_y;
	int rotation;
};


int __ts_attach(struct tsdev *ts, struct tslib_module_info *info);
int __ts_attach_raw(struct tsdev *ts, struct tslib_module_info *info);

int tslib_parse_vars(struct tslib_module_info *,
			    const struct tslib_vars *, int,
			    const char *);


#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* _TSLIB_PRIVATE_H_ */
