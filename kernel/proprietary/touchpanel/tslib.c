#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include "tslib.h"
#include "tslib-private.h"

int __ts_attach(struct tsdev *ts, struct tslib_module_info *info)
{
	info->dev = ts;
	info->next = ts->list;
	ts->list = info;

	return 0;
}

int __ts_attach_raw(struct tsdev *ts, struct tslib_module_info *info)
{
	struct tslib_module_info *next, *prev, *prev_list = ts->list_raw;
	info->dev = ts;
	info->next = prev_list;
	ts->list_raw = info;

	/* 
	 * ensure the last item in the normal list now points to the
	 * top of the raw list.
	 */

	if (ts->list == NULL || ts->list == prev_list) { /* main list is empty, ensure it points here */
		ts->list = info;
		return 0;
	}

	for(next = ts->list, prev=next;
	    next != NULL && next != prev_list;
	    next = prev->next, prev = next);

	prev->next = info;
	return 0;
}


int ts_read(struct tsdev *ts, struct ts_sample *samp, int nr)
{
	int result;
	result = ts->list->ops->read(ts->list, samp, nr);
#ifdef DEBUG
	if (result)
		printk("TS_READ----> x = %d, y = %d, p = %d\n", samp->x, samp->y, samp->p);
#endif
	return result;

}


int ts_read_raw(struct tsdev *ts, struct ts_sample *samp, int nr)
{
	int result = ts->list_raw->ops->read(ts->list_raw, samp, nr);
#ifdef DEBUG
	printk("TS_READ_RAW----> x = %d, y = %d, p = %d\n", samp->x, samp->y, samp->p);
#endif
	return result;
}

void ts_set_inputraw_callback(struct tsdev *ts,input_raw_read read,void* userdata)
{
	extern void inputraw_set_callback(struct tslib_module_info *inf,input_raw_read read,void* userdata);

	inputraw_set_callback(ts->list_raw,read,userdata);
}


extern	struct tslib_module_info *inputraw_mod_init(struct tsdev *dev, const char *params);
extern	struct tslib_module_info *pthres_mod_init(struct tsdev *dev, const char *params);
extern struct tslib_module_info *variance_mod_init(struct tsdev *dev, const char *params);
extern struct tslib_module_info *dejitter_mod_init(struct tsdev *dev, const char *params) 	;
extern struct tslib_module_info *linear_mod_init(struct tsdev *dev, const char *params);
extern struct tslib_module_info *lpf_mod_init(struct tsdev *dev, const char *params);

int __ts_load_module(struct tsdev *ts, const char *module, const char *params)
{
	
	struct tslib_module_info * (*init)(struct tsdev *, const char *);
	struct tslib_module_info *info;
	int raw=0;
	int ret;

	if(!strcmp(module,MODULE_INPUT_RAW))
	{
		init = inputraw_mod_init;
		raw=1;
	}
	else if(!strcmp(module,MODULE_PTHRES))
	{
		init = pthres_mod_init;
	}
	else if(!strcmp(module,MODULE_VARIANCE))
	{
		init = variance_mod_init;
	}
	else if(!strcmp(module,MODULE_DEJITTER))
	{
		init = dejitter_mod_init;
	}
	else if(!strcmp(module,MODULE_LINEAR))
	{
		init = linear_mod_init;
	}
	else if(!strcmp(module,MODULE_LPF))
	{
		init = lpf_mod_init;
	}
	
	else
	{
		printk("unknown module name %s\n",module);
		return -1;
	}


	info = init(ts, params);
	if (!info) {
#ifdef DEBUG
		printk ("Can't init %s\n", module);
#endif
		return -1;
	}

	if (raw) {
		ret = __ts_attach_raw(ts, info);
	} else {
		ret = __ts_attach(ts, info);
	}
	if (ret) {
#ifdef DEBUG
		printk("Can't attach %s\n", module);
#endif
		info->ops->fini(info);
	}

	return ret;
}


int ts_load_module(struct tsdev *ts, const char *module, const char *params)
{
	return __ts_load_module(ts, module, params);
}


int tslib_parse_vars(struct tslib_module_info *mod,
		     const struct tslib_vars *vars, int nr,
		     const char *str)
{
	static char s_holder[1024];
	char *s, *p;
	int ret = 0;

	if (!str)
		return 0;

	memset(s_holder, 0, 1024);
	strncpy(s_holder,str,strlen(str));
	s = s_holder;
	while ((p = strsep(&s, " \t")) != NULL && ret == 0) {
		const struct tslib_vars *v;
		char *eq;

		eq = strchr(p, '=');
		if (eq)
			*eq++ = '\0';

		for (v = vars; v < vars + nr; v++)
			if (strcasecmp(v->name, p) == 0) {
				ret = v->fn(mod, eq, v->data);
				break;
			}
	}

	return ret;
}


struct tsdev *ts_open(const char *name)
{
	struct tsdev *ts;

	ts = kzalloc(sizeof(struct tsdev),GFP_KERNEL);
	if (!ts) {
		return NULL;
	}

	return ts;

}

int ts_close(struct tsdev *ts)
{
	struct tslib_module_info *module,*next_module;
	module=ts->list;
	while(module)
	{
		next_module = module->next;
		if(module->ops->fini)
			module->ops->fini(module);
		module = next_module;
	}

	kfree(ts);

	return 0;
}

int ts_reset(struct tsdev *ts)
{
	struct tslib_module_info *module=ts->list;

	while(module)
	{
		if(module->ops->reset)
			module->ops->reset(module);
		module = module->next;
	}

	return 0;
}



