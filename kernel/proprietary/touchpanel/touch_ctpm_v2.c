/*
 * touch_ctpm_v2.c
 * focaltech capacitive touch panel driver but utilizing ctpm standard protocol
 *
 * Copyright (C) 2011 Questers Tech,Ltd.ALL RIGHTS RESERVED.
 *  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <plat/mfp.h>
#include <linux/input/questers_ts.h>

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif

//#define POLLING_MODE_SUPPORT 

#define CTPM_MAX_POINTS		0x5

#define CTPM_REG_DEVICEMODE	0x00
#define CTPM_REG_GESTUREID	0x01
#define CTPM_REG_TDSTATUS	0x02
#define CTPM_REG_TP_DATA	0x03
#define CTPM_REG_POWER_MODE 0xA5
#define CTPM_REG_ERRCODE	0xA9

//data array offset,plus CTPM_REG_TP_DATA
#define CTPM_POINT_XH		0x00
#define CTPM_POINT_XL		0x01
#define CTPM_POINT_YH		0x02
#define CTPM_POINT_YL		0x03
#define CTPM_POINT_WEIGHT	0x04
#define CTPM_POINT_CONTACT	0x05
#define CTPM_POINT_BYTES	6

#define DEV_MODE(v) ((v>>4)&0x07)
#define FINGERS(v) (v&0xf)

#if defined(CONFIG_BUTTERFLY)
#define SCREEN_WIDTH      600
#define SCREEN_HEIGHT     1024
#elif defined(CONFIG_P200)
#define SCREEN_WIDTH      480
#define SCREEN_HEIGHT     800
#else
#define SCREEN_WIDTH      480
#define SCREEN_HEIGHT     800
#endif


#define CTPM_EVENT_PULL_DOWN 0x00
#define CTPM_EVENT_PULL_UP	 0x01
#define CTPM_EVENT_CONTACT	 0x02
#define CTPM_EVENT_RESERVED	 0x03

#define LED_ON_TIME_IN_MS 10000

enum {
	DEBUG_RAW_REPORT = 1U << 0,
	DEBUG_XY_SWAP	 = 1U << 1,
	DEBUG_X_INVERTED = 1U << 2,
	DEBUG_Y_INVERTED = 1U << 3,
		
};

//local change has high priority ,just for simplicy
static int debug_mask = 0;//DEBUG_RAW_REPORT;
module_param_named(debug_mask, debug_mask, int, S_IRUGO | S_IWUSR | S_IWGRP);

#define MAX_KEY_NUM 4
#define KEY_RADIUS 50
#define KEY_RELEASED 0
#define KEY_PRESSED 1
int key_status[MAX_KEY_NUM] = {0};
struct touchkey_t
{
    int code;
    int x;
    int y;
    const char* name;
} touchkey[MAX_KEY_NUM] = {
    {
        .code = KEY_MENU,
        .x = 70,
        .y = 850,
        .name = "menu",
    },
    {
        .code = KEY_HOME,
        .x = 170,
        .y = 850,
        .name = "home",
    },
    {
        .code = KEY_BACK,
        .x = 300,
        .y = 850,
        .name = "back",
    },
    {
        .code = KEY_SEARCH,
        .x = 400,
        .y = 850,
        .name = "search",
    },
};

enum ctpm_mode
{
	kModeOperating	=	0x00,
	kModeSystemInfo	=	0x01,
	kModeRaw		=	0x04,
};
enum ctpm_gesture
{
	kGestureNone	=	0x00,
	kGestureKey0 	= 	0x01,
	kGestureKey1	=	0x02,	
	kGestureKey2	=	0x04,
	kGestureKey3	=	0x08,
	kGestureZoomIn	=	0x48,
	kGestureZoomOut	=	0x49,	
};

struct touch_point
{
	int x;
	int y;
	int t;//touch
	int w;//width
};

struct ctpm_touch_data
{
	u8 fingers;
	struct touch_point points[CTPM_MAX_POINTS];	
};

struct ctpm_touch_device{
	struct i2c_client* client;
	struct input_dev *idev;

	struct workqueue_struct* wq;
	struct delayed_work delayed_work;
	struct delayed_work delayed_resume_work;

	struct ctpm_touch_data touch_data;

	int x_max;
	int y_max;

	//irq number
	int irq;

	struct touch_platform_data* pdata;
	
	struct early_suspend    early_suspend;
	
	struct delayed_work touchkey_led_work;
};

static void ctpmtouch_touchkey_led_work_func(struct work_struct* work)
{
    struct ctpm_touch_device* ctpm = (struct ctpm_touch_device*) container_of(work, struct ctpm_touch_device, touchkey_led_work.work);
	if(ctpm->pdata->set_led)
	    ctpm->pdata->set_led(0);
}

static int ctpmtouch_write(struct i2c_client *client, u8* msg, u8 uCnt)
{	
	return i2c_master_send(client, msg, uCnt);
}
static int ctpmtouch_read(struct i2c_client *client,u8 reg,u8* buf, u8 uCnt)
{
	int ret;	
	ret = i2c_master_send(client, &reg, 1);
	if(ret<0) return ret;
	return i2c_master_recv(client, buf, uCnt);
}


static int ctpmtouch_regwrite(struct i2c_client *client,u8 reg,u8 value)
{
	u8 buf[4];
	buf[0]=reg;
	buf[1]=value;
	return ctpmtouch_write(client,buf,2);

}

static int ctpmtouch_regread(struct i2c_client *client,u8 reg,u8* value)
{
	u8 buf[4];
	int ret;
	buf[0]=reg;
	ret =  ctpmtouch_write(client,buf,1);
	if(ret<0) return ret;

	ret = i2c_master_recv(client,&buf[2],1);
	*value = buf[2];
	return ret;

}

//
//return :<0 error,otherwise return touch point number
static inline int ctpmtouch_head_parser(u8* h,int l)
{	
	int fingers;
	if(DEV_MODE(h[0]) != kModeOperating)
	{
		return -1;
	}
	fingers = FINGERS(h[2]);
	if(!fingers||fingers>CTPM_MAX_POINTS)
	{
		return 0;
	}
	return fingers;
}


static inline void ctpmtouch_data_parser(u8* d,struct ctpm_touch_data* t)
{
	int tracker,fingers_contact;
	struct touch_point* p;
	unsigned char event;
	//reset touch data
	memset(&t->points[0],0,sizeof(struct touch_point)*CTPM_MAX_POINTS);
	fingers_contact = 0;

	//start to parse finger data
	for(tracker=0;tracker<t->fingers;tracker++)
	{
		//touch_id=(d[tracker*CTPM_POINT_BYTES+CTPM_POINT_YH]&0xF0)>>4;
		//if(touch_id>CTPM_MAX_POINTS) continue;		
		p = &t->points[tracker];
		p->x = ((d[tracker*CTPM_POINT_BYTES+CTPM_POINT_XH]<<8)+d[tracker*CTPM_POINT_BYTES+CTPM_POINT_XL])&0xFFF;
		p->y = ((d[tracker*CTPM_POINT_BYTES+CTPM_POINT_YH]<<8)+d[tracker*CTPM_POINT_BYTES+CTPM_POINT_YL])&0xFFF;
		event = (d[tracker*CTPM_POINT_BYTES+CTPM_POINT_XH]&0xC0)>>6;
		if((CTPM_EVENT_PULL_UP==event)||
			(CTPM_EVENT_RESERVED==event))
		{
			p->t = 0;
			p->w = 0;
		}
		else
		{
			p->t = 0xff;//d[tracker*CTPM_POINT_BYTES+CTPM_POINT_CONTACT];
			p->w = 0xff;//d[tracker*CTPM_POINT_BYTES+CTPM_POINT_WEIGHT];
			fingers_contact++;
		}				
		
	}


}


static inline void ctpmtouch_report(struct ctpm_touch_device *ctpm)
{
	struct i2c_client* client = ctpm->client;
	struct ctpm_touch_data* t = &ctpm->touch_data;
	int ret;
	u8 buf[33];

	ret = ctpmtouch_read(client,CTPM_REG_DEVICEMODE,buf,3);
	if(ret!=3)
	{
		dev_warn(&client->dev,"reading touch header failed [%d]\n",ret);
		return;
	}
	
	t->fingers = ctpmtouch_head_parser(buf,3);

	if(t->fingers<0)
	{
		dev_warn(&client->dev,"touch point header error [%d]\n",t->fingers);
		return;	
	}

	if(t->fingers>0)
	{
		int fingerid;
		int val;
		/*
		 * Can we make sure that all fingers data are located at right place?
		*/
		ret = ctpmtouch_read(client,CTPM_REG_TP_DATA,buf,t->fingers*CTPM_POINT_BYTES);
		if(ret<0)
		{			
			dev_warn(&client->dev,"touch point data error [%d]\n",ret);
			return;
		}
		
		ctpmtouch_data_parser(buf,t);

		
		for(fingerid=0;fingerid<t->fingers;fingerid++)
		{
			if(!t->points[fingerid].t)
			{
				continue;			
			}
			if (t->points[fingerid].y < ctpm->y_max)
            {
                if(ctpm->pdata&&(ctpm->pdata->quirks&TOUCH_QUIRKS_XY_SWAP))
                {
                    val = t->points[fingerid].x;
                    t->points[fingerid].x = t->points[fingerid].y;
                    t->points[fingerid].y = val;
                }
                if(ctpm->pdata&&(ctpm->pdata->quirks&TOUCH_QUIRKS_X_INVERTED))
                {
                    t->points[fingerid].x = ctpm->x_max-t->points[fingerid].x;
                }
                if(ctpm->pdata&&(ctpm->pdata->quirks&TOUCH_QUIRKS_Y_INVERTED))
                {
                    t->points[fingerid].y = ctpm->y_max-t->points[fingerid].y;
                }

                if(DEBUG_RAW_REPORT&debug_mask)
                {
                    printk("ctpmtouch_raw_report: finger ID[%d]x=%d,y=%d,w=%d,t=%d\n",fingerid,
                            t->points[fingerid].x,
                            t->points[fingerid].y,
                            t->points[fingerid].w,
                            t->points[fingerid].t);
                }		

                input_report_abs(ctpm->idev, ABS_MT_POSITION_X, t->points[fingerid].x);
                input_report_abs(ctpm->idev, ABS_MT_POSITION_Y, t->points[fingerid].y);
                input_report_abs(ctpm->idev, ABS_MT_WIDTH_MAJOR,t->points[fingerid].w);
                input_report_abs(ctpm->idev, ABS_MT_TOUCH_MAJOR,t->points[fingerid].t);
                input_mt_sync(ctpm->idev);
            }
            else
            {
                int i;

                for (i=0; i<MAX_KEY_NUM; i++)
                {
                
					if(DEBUG_RAW_REPORT&debug_mask)
						printk(KERN_INFO"screen out [%d,%d]\n", 
							t->points[fingerid].x,
							t->points[fingerid].y);
                    if (abs(touchkey[i].x-t->points[fingerid].x) < KEY_RADIUS &&
                        abs(touchkey[i].y-t->points[fingerid].y) < KEY_RADIUS)
                    {
                        if (key_status[i] == KEY_RELEASED)
                        {
                            /* report key down */
                            if(DEBUG_RAW_REPORT&debug_mask)
                                printk(KERN_INFO"%s down\n", touchkey[i].name);
                            input_report_key(ctpm->idev, touchkey[i].code, 1);
                            key_status[i] = KEY_PRESSED;


                            /* turn on led */                            
                            if(ctpm->pdata->set_led){
                                ctpm->pdata->set_led(1);
                                schedule_delayed_work(&ctpm->touchkey_led_work, msecs_to_jiffies(LED_ON_TIME_IN_MS));
                            }
                            
                        }
                        else
                        {
                            if (t->points[fingerid].t == 0)
                            {
                                /* report key release */
                                if(DEBUG_RAW_REPORT&debug_mask)
                                    printk(KERN_ERR"%s up\n", touchkey[i].name);
                                input_report_key(ctpm->idev, touchkey[i].code, 0);
                                key_status[i] = KEY_RELEASED;
                            }
                        }
                    }
                }
            }
		}
	}
	else  //all fingers up
	{
		int i;		
		if(DEBUG_RAW_REPORT&debug_mask)
		{
			printk("all fingers up\n");
		}
		input_report_abs(ctpm->idev, ABS_MT_TOUCH_MAJOR,0);
		input_report_abs(ctpm->idev, ABS_MT_WIDTH_MAJOR,0);
		input_mt_sync(ctpm->idev);				

		for (i=0; i<MAX_KEY_NUM; i++)
        {
            if (key_status[i] == KEY_PRESSED)
            {
                /* report key release */
                if(DEBUG_RAW_REPORT&debug_mask)
                    printk(KERN_ERR"%s up\n", touchkey[i].name);
                input_report_key(ctpm->idev, touchkey[i].code, 0);
                key_status[i] = KEY_RELEASED;
            }
        }
	}
	
	input_sync(ctpm->idev);
}


static void ctpmtouch_work(struct work_struct* work)
{
	struct ctpm_touch_device *ctpm = (struct ctpm_touch_device*)container_of(work,struct ctpm_touch_device,delayed_work.work);	
	ctpmtouch_report(ctpm);
	#ifdef POLLING_MODE_SUPPORT
	if(gpio_get_value(irq_to_gpio(ctpm->irq)))
	{
		enable_irq(ctpm->irq);
	}
	else 
	{
		queue_delayed_work(ctpm->wq,&ctpm->delayed_work, msecs_to_jiffies(5));
	}
	#endif	
}

static void ctpmtouch_resume_work(struct work_struct* work)
{
	struct ctpm_touch_device *ctpm = (struct ctpm_touch_device*)container_of(work,struct ctpm_touch_device,delayed_resume_work.work);	
	if(ctpm->pdata&&ctpm->pdata->reset)
	{
		ctpm->pdata->reset();
	}
	enable_irq(ctpm->irq);
}


/*
 * Start the touchscreen thread and
 * the touch digitiser.
 */
static int ctpmtouch_open(struct input_dev *idev)
{
	/*
	struct ctpm_touch_device *ctpm = container_of(idev, struct ctpm_touch_device, idev);
	queue_delayed_work(ctpm->wq, &ctpm->delayed_work,0); */
 	return 0;
}

/*
 * Kill the touchscreen thread and stop
 * the touch digitiser.
 */
static void ctpmtouch_close(struct input_dev *idev)
{
}



static irqreturn_t ctpmtouch_isr(int irq, void *irq_data)
{
	struct ctpm_touch_device *ctpm = (struct ctpm_touch_device*)irq_data;

	#ifdef POLLING_MODE_SUPPORT
	disable_irq_nosync(ctpm->irq);
	#endif
	//cancel_delayed_work(&ctpm->delayed_work);
	queue_delayed_work(ctpm->wq, &ctpm->delayed_work,0);
 
	return IRQ_HANDLED;
}


static int ctpmtouch_suspend(struct i2c_client* client,pm_message_t state)
{
	struct ctpm_touch_device *ctpm = i2c_get_clientdata(client);	

	disable_irq(ctpm->irq);

	//led handler	
	cancel_delayed_work_sync(&ctpm->delayed_work);
	if(ctpm->pdata->set_led)
	    ctpm->pdata->set_led(0);
	//set mode to suspend
	ctpmtouch_regwrite(ctpm->client,CTPM_REG_POWER_MODE,0x03);
	
	return 0;
}

static int ctpmtouch_resume(struct i2c_client* client)
{
	struct ctpm_touch_device *ctpm = i2c_get_clientdata(client);	
	queue_delayed_work(ctpm->wq, &ctpm->delayed_resume_work,0);
	return 0;
}

#ifdef CONFIG_HAS_EARLYSUSPEND
//TODO early suspend/resume support
static void ctpmtouch_early_suspend(struct early_suspend *h)
{
	struct ctpm_touch_device *ctpm = container_of(h, struct ctpm_touch_device, early_suspend);
	ctpmtouch_suspend(ctpm->client, PMSG_SUSPEND);

}
static void ctpmtouch_late_resume(struct early_suspend *h)
{
	struct ctpm_touch_device *ctpm = container_of(h, struct ctpm_touch_device, early_suspend);
	ctpmtouch_resume(ctpm->client);

}
#endif

static int __devinit ctpmtouch_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct ctpm_touch_device* ctpm;
	struct touch_platform_data* pdata = (struct touch_platform_data*)client->dev.platform_data;
	struct device *dev = &client->dev;
	struct input_dev* input_device;
	u8 val;
	int ret = 0;		
    int i;

	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
	{
		printk(KERN_ERR"ctpmtouch_probe: need I2C_FUNC_I2C\n");
		return -ENODEV;
	}
	if(pdata)
	{
		if(pdata->power)
			pdata->power(TOUCH_POWER_ON);
		//reset now
		if(pdata->reset)
			pdata->reset();			
	}

	//check if remote exist
	val = 0;
	ret = ctpmtouch_regread(client,CTPM_REG_DEVICEMODE,&val);
	if(ret<0||(0!=val))
	{
		dev_err(dev,"ctpm touchpanel not exist or state error val=%d\n",val);
		return -ENODEV;
	}
	
	ctpm = kzalloc(sizeof(struct ctpm_touch_device),GFP_KERNEL);
	if(!ctpm)
	{
		dev_err(dev,"failed to create touch nas state\n");
		return -ENOMEM;
	}
	ctpm->client=client;
	ctpm->pdata = pdata;
	ctpm->x_max= SCREEN_WIDTH;
	ctpm->y_max = SCREEN_HEIGHT;	

	//trying to overlay XY setting from platform data
	if(0!=(debug_mask&(DEBUG_XY_SWAP|DEBUG_X_INVERTED|DEBUG_Y_INVERTED)))
	{
		pdata->quirks&=~(TOUCH_QUIRKS_XY_SWAP|TOUCH_QUIRKS_X_INVERTED|TOUCH_QUIRKS_Y_INVERTED);
		if(debug_mask&DEBUG_XY_SWAP)
		{
			pdata->quirks|=TOUCH_QUIRKS_XY_SWAP;		
			ctpm->x_max = SCREEN_HEIGHT;
			ctpm->y_max = SCREEN_WIDTH;
		}
		if(debug_mask&DEBUG_X_INVERTED)
			pdata->quirks|=TOUCH_QUIRKS_X_INVERTED;
		if(debug_mask&DEBUG_Y_INVERTED)
			pdata->quirks|=TOUCH_QUIRKS_Y_INVERTED;
	}
	
	ctpm->wq = create_singlethread_workqueue("touchpanel");
	if(ctpm->pdata->init)
	    ctpm->pdata->init();
	if(ctpm->pdata->set_led)
	    ctpm->pdata->set_led(0);
	    
	INIT_DELAYED_WORK(&ctpm->touchkey_led_work, ctpmtouch_touchkey_led_work_func);
	
	INIT_DELAYED_WORK(&ctpm->delayed_work,ctpmtouch_work);
	INIT_DELAYED_WORK(&ctpm->delayed_resume_work,ctpmtouch_resume_work);

	/* tell input system what we events we accept and register */
	input_device= input_allocate_device();
	if (NULL==input_device  ) {
		dev_err(dev,"%s: failed to allocate input dev\n", __FUNCTION__);
		ret = -ENOMEM;
		goto err1;
	}

	input_device->name = "touch_ctpm";
	input_device->phys = "touch_ctpm/input1";
	input_device->dev.parent  = dev;

	input_device->open = ctpmtouch_open;
	input_device->close = ctpmtouch_close;
	input_device->id.bustype = BUS_I2C;
	input_device->id.vendor  = 0xdead;
	input_device->id.product = 0xdead;
	
	input_device->evbit[0]= BIT_MASK(EV_SYN)|BIT_MASK(EV_KEY)|BIT_MASK(EV_ABS);
	input_device->absbit[BIT_WORD(ABS_MT_POSITION_X)] |= BIT_MASK(ABS_MT_POSITION_X);
	input_device->absbit[BIT_WORD(ABS_MT_POSITION_Y)] |= BIT_MASK(ABS_MT_POSITION_Y);
	input_device->absbit[BIT_WORD(ABS_MT_TOUCH_MAJOR)] |= BIT_MASK(ABS_MT_TOUCH_MAJOR);
	input_device->absbit[BIT_WORD(ABS_MT_WIDTH_MAJOR)] |= BIT_MASK(ABS_MT_WIDTH_MAJOR);
	
	printk(KERN_ERR "max_x: %d, max_y: %d\n", ctpm->x_max, ctpm->y_max);
	input_set_abs_params(input_device, ABS_MT_POSITION_X, 0, ctpm->x_max, 0, 0);
	input_set_abs_params(input_device, ABS_MT_POSITION_Y, 0, ctpm->y_max, 0, 0);
	input_set_abs_params(input_device, ABS_MT_TOUCH_MAJOR, 0, 0xff, 0, 0);
	input_set_abs_params(input_device, ABS_MT_WIDTH_MAJOR, 0, 0xffff, 0, 0);

	input_set_drvdata(input_device,ctpm);	

	ctpm->idev = input_device;

	//set gpio and request irq	
	ctpm->irq = client->irq;	
	i2c_set_clientdata(client, ctpm);

	for (i=0; i<MAX_KEY_NUM; i++)
    {
        key_status[i] = KEY_RELEASED;
	    input_set_capability(input_device, EV_KEY, touchkey[i].code);
    }

#ifdef CONFIG_HAS_EARLYSUSPEND
		ctpm->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN+1;
		ctpm->early_suspend.suspend = ctpmtouch_early_suspend;
		ctpm->early_suspend.resume = ctpmtouch_late_resume;
		register_early_suspend(&ctpm->early_suspend);
#endif

	ret = input_register_device(input_device);
	if (ret) {
		dev_err(dev,"%s: unabled to register input device, ret = %d\n",
				__FUNCTION__, ret);
		goto err2;
	}
	

	//falling edge interrupt
	ret = request_irq(ctpm->irq, ctpmtouch_isr,
			IRQF_DISABLED|IRQF_TRIGGER_FALLING, 
			"touch interrupt",
			ctpm);
	if (ret) {
		dev_dbg(dev,"Request IRQ for touch failed (%d).\n", ret);
		goto err3;
	}



	return 0;
err3:
	input_unregister_device(input_device);
	input_device = NULL;	
err2:
	input_free_device(input_device);	
err1:		
	destroy_workqueue(ctpm->wq);
	kfree(ctpm);
	return ret;
}

static int ctpmtouch_remove(struct i2c_client *client)
{
	struct ctpm_touch_device *ctpm = i2c_get_clientdata(client);

#ifdef CONFIG_HAS_EARLYSUSPEND
		unregister_early_suspend(&ctpm->early_suspend);
#endif


	cancel_delayed_work_sync(&ctpm->delayed_work);
	destroy_workqueue(ctpm->wq);
	
 	free_irq(ctpm->irq, ctpm);
	
	input_unregister_device(ctpm->idev);	
	kfree(ctpm);

	dev_set_drvdata(&client->dev, NULL);
	return 0;
}
 



static struct i2c_device_id ctpmtouch_idtable[] = { 
	{ "ctpm", 0 }, 
	{ } 
}; 

static struct i2c_driver ctpmtouch_driver = {
	.driver = {
		.name 	= "ctpm",
	},
	.id_table       = ctpmtouch_idtable,
	.probe		= ctpmtouch_probe,
	.remove		= __devexit_p(ctpmtouch_remove),
	
	#ifdef CONFIG_PM
	#ifndef CONFIG_HAS_EARLYSUSPEND
	.suspend  = ctpmtouch_suspend,
	.resume   = ctpmtouch_resume,
	#endif
	#endif
};

static int __init ctpmtouch_init(void)
{	
	return i2c_add_driver(&ctpmtouch_driver);
}

static void __exit ctpmtouch_exit(void)
{	
	i2c_del_driver(&ctpmtouch_driver);
}

module_init(ctpmtouch_init);
module_exit(ctpmtouch_exit);

MODULE_DESCRIPTION("FocalTech CTPM V2<standard protocol> Touch Screen driver");
MODULE_AUTHOR("Raymond Wang <raymond1860@gmail.com>");
MODULE_LICENSE("GPL");

