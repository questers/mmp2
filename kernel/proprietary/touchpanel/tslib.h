#ifndef _TS_LIB_H_
#define _TS_LIB_H_

#include <linux/slab.h>
#undef DEBUG
//#define DEBUG
struct tsdev;

struct ts_sample {
	int		x;
	int		y;
	unsigned int	p;
};

//input module is must load module
#define MODULE_INPUT_RAW  	"inputraw"

typedef int (*input_raw_read)(struct ts_sample* samp,int nr,void* userdata);

//pthres 
//pressure threshold
#define MODULE_PTHRES 		"pthres"
/*
module:	variance
----------------

Description:
  Variance filter. Tries to do it's best in order to filter out random noise
  coming from touchscreen ADC's. This is achieved by limiting the sample
  movement speed to some value (e.g. the pen is not supposed to move quicker
  than some threshold).

  This is a 'greedy' filter, e.g. it gives less samples on output than
  receives on input.

Parameters:
  delta
	Set the squared distance in touchscreen units between previous and
	current pen position (e.g. (X2-X1)^2 + (Y2-Y1)^2). This defines the
	criteria for determining whenever two samples are 'near' or 'far'
	to each other.

	Now if the distance between previous and current sample is 'far',
	the sample is marked as 'potential noise'. This doesn't mean yet
	that it will be discarded; if the next reading will be close to it,
	this will be considered just a regular 'quick motion' event, and it
	will sneak to the next layer. Also, if the sample after the
	'potential noise' is 'far' from both previously discussed samples,
	this is also considered a 'quick motion' event and the sample sneaks
	into the output stream.
*/
#define MODULE_VARIANCE		"variance"

/*
module: dejitter
----------------

Description:
  Removes jitter on the X and Y co-ordinates. This is achieved by applying a
  weighted smoothing filter. The latest samples have most weight; earlier
  samples have less weight. This allows to achieve 1:1 input->output rate.

Parameters:
  delta
	Squared distance between two samples ((X2-X1)^2 + (Y2-Y1)^2) that
	defines the 'quick motion' threshold. If the pen moves quick, it
	is not feasible to smooth pen motion, besides quick motion is not
	precise anyway; so if quick motion is detected the module just
	discards the backlog and simply copies input to output.

*/
#define MODULE_DEJITTER 	"dejitter"

/*
module: linear
--------------

Description:
  Linear scaling module, primerily used for conversion of touch screen
  co-ordinates to screen co-ordinates.

Parameters:
  xyswap
	interchange the X and Y co-ordinates -- no longer used or needed
	if the new linear calibration utility ts_calibrate is used.
  a
  b
  c
  d
  e
  f
  s
      The famous 7 params to linear scale touchscreen coordinates values to screen values.
  Formula source:
  This implementation is a linear transformation using 7 parameters
	(a, b, c, d, e, f and s) to transform the device coordinates (Xd, Yd)
	into screen coordinates (Xs, Ys) using the following equations:
	s*Xs = a*Xd + b*Yd + c
	s*Ys = d*Xd + e*Yd + f

  width
  height
      calibrate params performed on screen resolution (width x height)
   

*/
#define MODULE_LINEAR		"linear"

/*
 * low pass filter plugin
*/
#define MODULE_LPF			"lpf"

struct tsdev *ts_open(const char *name);

void ts_set_inputraw_callback(struct tsdev *ts,input_raw_read read,void* userdata);

int ts_close(struct tsdev *ts);

/*
 * Load a filter/scaling module
 */
int ts_load_module(struct tsdev *, const char *module, const char *params);


/*
 * Return a scaled touchscreen sample.
 */
int ts_read(struct tsdev *, struct ts_sample *, int);

/*
 * Returns a raw, unscaled sample from the touchscreen.
 */
int ts_read_raw(struct tsdev *, struct ts_sample *, int);

int ts_reset(struct tsdev *ts);


#endif
