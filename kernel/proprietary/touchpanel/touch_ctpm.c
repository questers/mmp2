/*
 * touch_ctpm.c
 * focaltech capacitive touch panel driver
 *
 * Copyright (C) 2011 Questers Tech,Ltd.ALL RIGHTS RESERVED.
 *  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <plat/mfp.h>
#include <linux/input/questers_ts.h>

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif

//#define FOCALTECH_PATCH
//#define POLLING_MODE_SUPPORT 

#define CTPM_MAX_POINTS		0x5
#define CTPM_TAG_TOUCH		0xF9
#define CTPM_TAG_REGISTER	0xFC


#define CTPM_REG_STATE		0x3E
#define CTPM_REG_TPMID		0x3D
#define CTPM_REG_POWERMODE	0x3A

#define SCREEN_WIDTH      600
//1024
#define SCREEN_HEIGHT     1024
//600

#define TRACKER_DATA_OFFSET 7

#define CTPM_EVENT_PULL_DOWN 0x00
#define CTPM_EVENT_PULL_UP	 0x01
#define CTPM_EVENT_CONTACT	 0x02
#define CTPM_EVENT_RESERVED	 0x03
enum {
	DEBUG_RAW_REPORT = 1U << 0,

	DEBUG_XY_SWAP	 = 1U << 1,
	DEBUG_X_INVERTED = 1U << 2,
	DEBUG_Y_INVERTED = 1U << 3,
	DEBUG_FORCE_OVERLAY = 1U << 4,
};

static int debug_mask =  0;//DEBUG_RAW_REPORT;
module_param_named(debug_mask, debug_mask, int, S_IRUGO | S_IWUSR | S_IWGRP);

struct touch_point
{
	int x;
	int y;
	int t;//touch
	int w;//width
};

struct ctpm_touch_data
{
	u8 fingers;
	struct touch_point points[CTPM_MAX_POINTS];	
};

struct ctpm_touch_device{
	struct i2c_client* client;
	struct input_dev *idev;

	struct workqueue_struct* wq;
	struct delayed_work delayed_work;


	struct ctpm_touch_data touch_data;

	int x_max;
	int y_max;
	//irq number
	int irq;

	struct touch_platform_data* pdata;
	
	struct early_suspend    early_suspend;
	
};

#ifdef FOCALTECH_PATCH
//porting from focaltech?
static unsigned short gamma600[]={
0 ,   0 ,   0 ,   0 ,
 0 ,   0 ,   0 ,   0 ,   0 ,   0 ,   1 ,   1 ,
 1 ,   1 ,   2 ,   2 ,   2 ,   2 ,   3 ,   3 ,
 3 ,   3 ,   4 ,   4 ,   4 ,   5 ,   5 ,   6 ,
 6 ,   6 ,   7 ,   7 ,   8 ,   8 ,   8 ,   9 ,
 9 ,   10 ,   10 ,   11 ,   11 ,   12 ,   12 ,   13 ,
 13 ,   14 ,   14 ,   15 ,   15 ,   16 ,   16 ,   17 ,
 18 ,   18 ,   19 ,   19 ,   20 ,   20 ,   21 ,   22 ,
 22 ,   23 ,   24 ,   24 ,   25 ,   26 ,   26 ,   27 ,
 28 ,   28 ,   29 ,   30 ,   30 ,   31 ,   32 ,   33 ,
 33 ,   34 ,   35 ,   36 ,   36 ,   37 ,   38 ,   39 ,
 40 ,   40 ,   41 ,   42 ,   43 ,   44 ,   44 ,   45 ,
 46 ,   47 ,   48 ,   49 ,   50 ,   50 ,   51 ,   52 ,
 53 ,   54 ,   55 ,   56 ,   57 ,   58 ,   59 ,   59 ,
 60 ,   61 ,   62 ,   63 ,   64 ,   65 ,   66 ,   67 ,
 68 ,   69 ,   70 ,   71 ,   72 ,   73 ,   74 ,   75 ,
 76 ,   77 ,   78 ,   79 ,   80 ,   81 ,   82 ,   84 ,
 85 ,   86 ,   87 ,   88 ,   89 ,   90 ,   91 ,   92 ,
 93 ,   94 ,   96 ,   97 ,   98 ,   99 ,   100 ,   101 ,
 102 ,   104 ,   105 ,   106 ,   107 ,   108 ,   110 ,   111 ,
 112 ,   113 ,   114 ,   116 ,   117 ,   118 ,   119 ,   120 ,
 122 ,   123 ,   124 ,   125 ,   127 ,   128 ,   129 ,   131 ,
 132 ,   133 ,   134 ,   136 ,   137 ,   138 ,   140 ,   141 ,
 142 ,   144 ,   145 ,   146 ,   148 ,   149 ,   150 ,   152 ,
 153 ,   154 ,   156 ,   157 ,   158 ,   160 ,   161 ,   163 ,
 164 ,   165 ,   167 ,   168 ,   170 ,   171 ,   172 ,   174 ,
 175 ,   177 ,   178 ,   180 ,   181 ,   183 ,   184 ,   185 ,
 187 ,   188 ,   190 ,   191 ,   193 ,   194 ,   196 ,   197 ,
 199 ,   200 ,   202 ,   203 ,   205 ,   206 ,   208 ,   210 ,
 211 ,   213 ,   214 ,   216 ,   217 ,   219 ,   220 ,   222 ,
 224 ,   225 ,   227 ,   228 ,   230 ,   232 ,   233 ,   235 ,
 236 ,   238 ,   240 ,   241 ,   243 ,   245 ,   246 ,   248 ,
 250 ,   251 ,   253 ,   255 ,   256 ,   258 ,   260 ,   261
};
#endif
static int ctpmtouch_write(struct i2c_client *client, u8* msg, u8 uCnt)
{	
	return i2c_master_send(client, msg, uCnt);
}
static int ctpmtouch_read(struct i2c_client *client,u8 reg,u8* buf, u8 uCnt)
{
	int ret;	
	ret = i2c_master_send(client, &reg, 1);
	if(ret<0) return ret;
	return i2c_master_recv(client, buf, uCnt);
}


static int ctpmtouch_regwrite(struct i2c_client *client,u8 reg,u8 value)
{
	u8 buf[4];
	buf[0]=CTPM_TAG_REGISTER;
	buf[1]=reg;
	buf[2]=value;
	buf[3]=buf[0]^buf[1]^buf[2];//ecc
	return ctpmtouch_write(client,buf,4);

}

static int ctpmtouch_regread(struct i2c_client *client,u8 reg,u8* value)
{
	u8 buf[4];
	int ret;
	buf[0]=CTPM_TAG_REGISTER;
	buf[1]=reg+0x40;
	ret =  ctpmtouch_write(client,buf,2);
	if(ret<0) return ret;

	ret = i2c_master_recv(client,&buf[2],2);
	*value = buf[2];
	//should I verify ECC?

	return ret;

}


static inline void ctpmtouch_touchparser(u8* d,struct ctpm_touch_data* t)
{
	int tracker,finger,fingers_contact;
	struct touch_point* p;
	unsigned char event;
/*	u16 magic=*((u16*)(&d[0]));
	if((magic!=0xAAAA)||(0x1A!=d[2]))
		return;
	t->fingers = 0;*/
	t->fingers = d[3]&0xf;
	if(t->fingers==0xf) t->fingers = 0;
	if(t->fingers==0)
		return;

	//reset touch data
	memset(&t->points[0],0,sizeof(struct touch_point)*CTPM_MAX_POINTS);
	fingers_contact = 0;

	//start to parse finger data
	for(tracker=0;tracker<CTPM_MAX_POINTS;tracker++)
	{
		finger=(d[tracker*4+TRACKER_DATA_OFFSET]&0xF0)>>4;
		if(finger>CTPM_MAX_POINTS) continue;		
		p = &t->points[finger];
		p->x = ((d[tracker*4+5]<<8)+d[tracker*4+6])&0xFFF;
		p->y = ((d[tracker*4+7]<<8)+d[tracker*4+8])&0xFFF;
		event = (d[tracker*4+5]&0xC0)>>6;
		if((CTPM_EVENT_PULL_UP==event)||
			(CTPM_EVENT_RESERVED==event))
		{
			p->t = 0;
			p->w = 0;
		}
		else
		{
			fingers_contact++;
			p->t = 0xff;
			p->w = 0xff;
		}				
		
	}

	t->fingers=fingers_contact;

		

}


static inline void ctpmtouch_report(struct ctpm_touch_device *ctpm)
{
	struct i2c_client* client = ctpm->client;
	struct ctpm_touch_data* t = &ctpm->touch_data;
	int fingers;
	int ret;
	u8 buf[26];

	ret = ctpmtouch_read(client,CTPM_TAG_TOUCH,buf,26);
	if(ret<0)
		return;
	ctpmtouch_touchparser(buf,t);
	fingers = t->fingers;
	//if(fingers>2) fingers=0;
	if(fingers>0)
	{
		int fingerid;
		int val;
		for(fingerid=0;fingerid<CTPM_MAX_POINTS;fingerid++)
		{
			if(!t->points[fingerid].t)
			{
				continue;			
			}
			if(ctpm->pdata&&(ctpm->pdata->quirks&TOUCH_QUIRKS_XY_SWAP))
			{
				val = t->points[fingerid].x;
				t->points[fingerid].x = t->points[fingerid].y;
				t->points[fingerid].y = val;
			}
			if(ctpm->pdata&&(ctpm->pdata->quirks&TOUCH_QUIRKS_X_INVERTED))
			{
				t->points[fingerid].x = ctpm->x_max-t->points[fingerid].x;
			}
			if(ctpm->pdata&&(ctpm->pdata->quirks&TOUCH_QUIRKS_Y_INVERTED))
			{
				t->points[fingerid].y = ctpm->y_max-t->points[fingerid].y;
			}
			#ifdef FOCALTECH_PATCH
			if(t->points[fingerid].y<260)
				t->points[fingerid].y = gamma600[t->points[fingerid].y];
			#endif
			
			if(DEBUG_RAW_REPORT&debug_mask)
			{
				printk("ctpmtouch_raw_report: finger ID[%d]x=%d,y=%d,s=%d\n",fingerid,
					t->points[fingerid].x,
					t->points[fingerid].y,t->points[fingerid].w);
			}		
			
			input_report_abs(ctpm->idev, ABS_MT_POSITION_X, t->points[fingerid].x);
			input_report_abs(ctpm->idev, ABS_MT_POSITION_Y, t->points[fingerid].y);
			input_report_abs(ctpm->idev, ABS_MT_TOUCH_MAJOR,t->points[fingerid].t);
			input_report_abs(ctpm->idev, ABS_MT_WIDTH_MAJOR,t->points[fingerid].w);
			input_mt_sync(ctpm->idev);
			
		}

	}
	else  //all fingers up
	{
		
		if(DEBUG_RAW_REPORT&debug_mask)
		{
			printk("all fingers up\n");
		}
		input_report_abs(ctpm->idev, ABS_MT_TOUCH_MAJOR,0);
		input_report_abs(ctpm->idev, ABS_MT_WIDTH_MAJOR,0);
		input_mt_sync(ctpm->idev);				
	}
	
	
	input_sync(ctpm->idev);
	
}


static void ctpmtouch_work(struct work_struct* work)
{
	struct ctpm_touch_device *ctpm = (struct ctpm_touch_device*)container_of(work,struct ctpm_touch_device,delayed_work.work);	
	ctpmtouch_report(ctpm);
	#ifdef POLLING_MODE_SUPPORT
	if(gpio_get_value(irq_to_gpio(ctpm->irq)))
	{
		enable_irq(ctpm->irq);
	}
	else 
	{
		queue_delayed_work(ctpm->wq,&ctpm->delayed_work, msecs_to_jiffies(5));
	}
	#endif	
}


/*
 * Start the touchscreen thread and
 * the touch digitiser.
 */
static int ctpmtouch_open(struct input_dev *idev)
{
 	return 0;
}

/*
 * Kill the touchscreen thread and stop
 * the touch digitiser.
 */
static void ctpmtouch_close(struct input_dev *idev)
{
}



static irqreturn_t ctpmtouch_isr(int irq, void *irq_data)
{
	struct ctpm_touch_device *ctpm = (struct ctpm_touch_device*)irq_data;

	#ifdef POLLING_MODE_SUPPORT
	disable_irq_nosync(ctpm->irq);
	#endif
	//cancel_delayed_work(&ctpm->delayed_work);
	queue_delayed_work(ctpm->wq, &ctpm->delayed_work,0);
 
	return IRQ_HANDLED;
}


static int ctpmtouch_suspend(struct i2c_client* client,pm_message_t state)
{
	struct ctpm_touch_device *ctpm = i2c_get_clientdata(client);	

	//set mode to suspend
	ctpmtouch_regwrite(ctpm->client,CTPM_REG_POWERMODE,0x03);
	
	return 0;
}

static int ctpmtouch_resume(struct i2c_client* client)
{
	struct ctpm_touch_device *ctpm = i2c_get_clientdata(client);	
	if(ctpm->pdata)
	{
		if(ctpm->pdata->reset)
			ctpm->pdata->reset();
	}
	return 0;
}

#ifdef CONFIG_HAS_EARLYSUSPEND
//TODO early suspend/resume support
static void ctpmtouch_early_suspend(struct early_suspend *h)
{
	struct ctpm_touch_device *ctpm = container_of(h, struct ctpm_touch_device, early_suspend);
	ctpmtouch_suspend(ctpm->client, PMSG_SUSPEND);

}
static void ctpmtouch_late_resume(struct early_suspend *h)
{
	struct ctpm_touch_device *ctpm = container_of(h, struct ctpm_touch_device, early_suspend);
	ctpmtouch_resume(ctpm->client);

}
#endif

static int __devinit ctpmtouch_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct ctpm_touch_device* ctpm;
	struct touch_platform_data* pdata = (struct touch_platform_data*)client->dev.platform_data;
	struct device *dev = &client->dev;
	struct input_dev* input_device;
	u8 val;
	int ret = 0;		

	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
	{
		printk(KERN_ERR"ctpmtouch_probe: need I2C_FUNC_I2C\n");
		return -ENODEV;
	}
	if(pdata)
	{
		if(pdata->power)
			pdata->power(TOUCH_POWER_ON);
		//reset now
		if(pdata->reset)
			pdata->reset();			
	}

	//check if remote exist
	ret = ctpmtouch_regread(client,CTPM_REG_STATE,&val);
	if(ret<0||(0!=val))
	{
		dev_err(dev,"ctpm touchpanel not exist or state error\n");
		return -ENODEV;
	}
	
	ctpm = kzalloc(sizeof(struct ctpm_touch_device),GFP_KERNEL);
	if(!ctpm)
	{
		dev_err(dev,"failed to create touch nas state\n");
		return -ENOMEM;
	}
	ctpm->client=client;
	ctpm->pdata = pdata;

	//trying to overlay XY setting from platform data
	ctpm->x_max= SCREEN_WIDTH;
	ctpm->y_max = SCREEN_HEIGHT;
	if(0!=(debug_mask&(DEBUG_XY_SWAP|DEBUG_X_INVERTED|DEBUG_Y_INVERTED)))
	{
		pdata->quirks&=~(TOUCH_QUIRKS_XY_SWAP|TOUCH_QUIRKS_X_INVERTED|TOUCH_QUIRKS_Y_INVERTED);
	}
	else if (0!=(debug_mask&(DEBUG_FORCE_OVERLAY)))
	{
		pdata->quirks &=~(TOUCH_QUIRKS_XY_SWAP|TOUCH_QUIRKS_X_INVERTED|TOUCH_QUIRKS_Y_INVERTED);
	}
	
	if(debug_mask&DEBUG_XY_SWAP)
	{
		pdata->quirks|=TOUCH_QUIRKS_XY_SWAP;
		ctpm->x_max = SCREEN_HEIGHT;
		ctpm->y_max = SCREEN_WIDTH;
	}
	if(debug_mask&DEBUG_X_INVERTED)
		pdata->quirks|=TOUCH_QUIRKS_X_INVERTED;
	if(debug_mask&DEBUG_Y_INVERTED)
		pdata->quirks|=TOUCH_QUIRKS_Y_INVERTED;
	
	
	ctpm->wq = create_singlethread_workqueue("touchpanel");
	
	INIT_DELAYED_WORK(&ctpm->delayed_work,ctpmtouch_work);

	/* tell input system what we events we accept and register */
	input_device= input_allocate_device();
	if (NULL==input_device  ) {
		dev_err(dev,"%s: failed to allocate input dev\n", __FUNCTION__);
		ret = -ENOMEM;
		goto err1;
	}

	input_device->name = "touch_ctpm";
	input_device->phys = "touch_ctpm/input1";
	input_device->dev.parent  = dev;

	input_device->open = ctpmtouch_open;
	input_device->close = ctpmtouch_close;
	input_device->id.bustype = BUS_I2C;
	input_device->id.vendor  = 0xdead;
	input_device->id.product = 0xdead;
	
	input_device->evbit[0]= BIT_MASK(EV_SYN)|BIT_MASK(EV_KEY)|BIT_MASK(EV_ABS);
	input_device->absbit[BIT_WORD(ABS_MT_POSITION_X)] |= BIT_MASK(ABS_MT_POSITION_X);
	input_device->absbit[BIT_WORD(ABS_MT_POSITION_Y)] |= BIT_MASK(ABS_MT_POSITION_Y);
	input_device->absbit[BIT_WORD(ABS_MT_TOUCH_MAJOR)] |= BIT_MASK(ABS_MT_TOUCH_MAJOR);
	input_device->absbit[BIT_WORD(ABS_MT_WIDTH_MAJOR)] |= BIT_MASK(ABS_MT_WIDTH_MAJOR);
	
	input_set_abs_params(input_device, ABS_MT_POSITION_X, 0, ctpm->x_max, 0, 0);
	input_set_abs_params(input_device, ABS_MT_POSITION_Y, 0, ctpm->y_max, 0, 0);
	input_set_abs_params(input_device, ABS_MT_TOUCH_MAJOR, 0, 0xff, 0, 0);
	input_set_abs_params(input_device, ABS_MT_WIDTH_MAJOR, 0, 0xffff, 0, 0);

	input_set_drvdata(input_device,ctpm);	

	ctpm->idev = input_device;

	//set gpio and request irq	
	ctpm->irq = client->irq;	
	i2c_set_clientdata(client, ctpm);


#ifdef CONFIG_HAS_EARLYSUSPEND
		ctpm->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN+1;
		ctpm->early_suspend.suspend = ctpmtouch_early_suspend;
		ctpm->early_suspend.resume = ctpmtouch_late_resume;
		register_early_suspend(&ctpm->early_suspend);
#endif

	ret = input_register_device(input_device);
	if (ret) {
		dev_err(dev,"%s: unabled to register input device, ret = %d\n",
				__FUNCTION__, ret);
		goto err2;
	}
	

	//falling edge interrupt
	ret = request_irq(ctpm->irq, ctpmtouch_isr,
			IRQF_DISABLED|IRQF_TRIGGER_FALLING, 
			"touch interrupt",
			ctpm);
	if (ret) {
		dev_dbg(dev,"Request IRQ for touch failed (%d).\n", ret);
		goto err3;
	}



	return 0;
err3:
	input_unregister_device(input_device);
	input_device = NULL;	
err2:
	input_free_device(input_device);	
err1:		
	destroy_workqueue(ctpm->wq);
	kfree(ctpm);
	return ret;
}

static int ctpmtouch_remove(struct i2c_client *client)
{
	struct ctpm_touch_device *ctpm = i2c_get_clientdata(client);

#ifdef CONFIG_HAS_EARLYSUSPEND
		unregister_early_suspend(&ctpm->early_suspend);
#endif


	cancel_delayed_work_sync(&ctpm->delayed_work);
	destroy_workqueue(ctpm->wq);
	
 	free_irq(ctpm->irq, ctpm);
	
	input_unregister_device(ctpm->idev);	
	kfree(ctpm);

	dev_set_drvdata(&client->dev, NULL);
	return 0;
}
 



static struct i2c_device_id ctpmtouch_idtable[] = { 
	{ "ctpm", 0 }, 
	{ } 
}; 

static struct i2c_driver ctpmtouch_driver = {
	.driver = {
		.name 	= "ctpm",
	},
	.id_table       = ctpmtouch_idtable,
	.probe		= ctpmtouch_probe,
	.remove		= __devexit_p(ctpmtouch_remove),
	
	#ifdef CONFIG_PM
	#ifndef CONFIG_HAS_EARLYSUSPEND
	.suspend  = ctpmtouch_suspend,
	.resume   = ctpmtouch_resume,
	#endif
	#endif
};

static int __init ctpmtouch_init(void)
{	
	return i2c_add_driver(&ctpmtouch_driver);
}

static void __exit ctpmtouch_exit(void)
{	
	i2c_del_driver(&ctpmtouch_driver);
}

module_init(ctpmtouch_init);
module_exit(ctpmtouch_exit);

MODULE_DESCRIPTION("FocalTech CTPM Touch Screen driver");
MODULE_AUTHOR("Raymond Wang");
MODULE_LICENSE("GPL");

