/*
 *  tslib/plugins/input-raw.c
 *
 *  Original version:
 *  Copyright (C) 2001 Russell King.
 *
 *  Rewritten for the Linux input device API:
 *  Copyright (C) 2002 Nicolas Pitre
 *
 * This file is placed under the LGPL.  Please see the file
 * COPYING for more details.
 *
 * $Id: input-raw.c,v 1.5 2005/02/26 01:47:23 kergoth Exp $
 *
 * Read raw pressure, x, y, and timestamp from a touchscreen device.
 */
#include <linux/slab.h>
#include "tslib.h"
#include "tslib-private.h"

#define GRAB_EVENTS_WANTED	1
#define GRAB_EVENTS_ACTIVE	2

struct tslib_input {
	struct tslib_module_info module;
	input_raw_read read; 
	void* userdata;
	int debug;
};


static int ts_input_read(struct tslib_module_info *inf,
			 struct ts_sample *samp, int nr)
{
	int cnt;
	struct tslib_input *input = (struct tslib_input *)inf;
	if(!input->read)
	{
		printk("please set input raw read function\n");
		return 0;
	}
	cnt = input->read(samp,nr,input->userdata);
	if(input->debug&&cnt)
	{
		int idx=0;
		do{
			printk("INPUTRAW------------------>[%d,%d,%d]\n",
				samp[idx].x,
				samp[idx].y,
				samp[idx].p);
		}while(++idx<cnt);		
	}
	return cnt;
}

static int ts_input_fini(struct tslib_module_info *inf)
{
	kfree(inf);
	return 0;
}

static int input_limit(struct tslib_module_info *inf, char *str, void *data)
{
	struct tslib_input *i = (struct tslib_input *)inf;
	unsigned long v;

	v = simple_strtoul(str, NULL, 0);

	if (v == ULONG_MAX )
		return -1;

	switch ((int)data) {
	case 1:
		i->debug= v;
		break;
	default:
		return -1;
	}
	return 0;
}


static const struct tslib_ops __ts_input_ops = {
	.read	= ts_input_read,
	.fini	= ts_input_fini,
};

static const struct tslib_vars input_vars[] =
{
	{ "debug",	(void *)1, input_limit },
};


#define NR_VARS (sizeof(input_vars) / sizeof(input_vars[0]))

struct tslib_module_info *inputraw_mod_init(struct tsdev *dev, const char *params)
{
	struct tslib_input *i;

	i = kzalloc(sizeof(struct tslib_input),GFP_KERNEL);
	if (i == NULL)
		return NULL;

	i->module.ops = &__ts_input_ops;
	i->module.dev = dev;

	/*
	 * Parse the parameters.
	 */
	if (tslib_parse_vars(&i->module, input_vars, NR_VARS, params)) {
		kfree(i);
		return NULL;
	}


	return &(i->module);
}

void inputraw_set_callback(struct tslib_module_info *inf,input_raw_read read,void* userdata)
{
	struct tslib_input *input = (struct tslib_input *)inf;
	if(input)
	{
		input->read = read;
		input->userdata = userdata;
	}
	
}


