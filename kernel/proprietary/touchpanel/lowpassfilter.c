/*
 * This file is placed under the LGPL.  Please see the file
 * COPYING for more details.
 * 
 * Very stupid lowpass dejittering filter
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/slab.h>
#include "tslib.h"
#include "tslib-private.h"

struct tslib_lowpass {
	struct tslib_module_info module;
        struct ts_sample last;
        struct ts_sample ideal;
	//float factor; kernel does not support float point
	unsigned long factor;
	unsigned int flags;
	unsigned char threshold;
#define VAR_PENUP		0x00000001
};



static int lowpass_read(struct tslib_module_info *info, struct ts_sample *samp, int nr)
{
	struct tslib_lowpass *var = (struct tslib_lowpass *)info;
	struct ts_sample cur;
	int count = 0;
	int delta;

	while (count < nr) {
		if (info->next->ops->read(info->next, &cur, 1) < 1)
			return count;

		if (cur.p == 0) {
			var->flags |= VAR_PENUP;
			samp [count++] = cur;
		} else if (var->flags & VAR_PENUP) {
			var->flags &= ~VAR_PENUP;
			var->last = cur;
			samp [count++] = cur;
		} else {
			var->ideal = cur;

			var->ideal.x = var->last.x;
			delta = cur.x - var->last.x;
			if (delta <= var->threshold && 
					delta >= -var->threshold)
				delta = 0;
			delta = (delta*var->factor)/10;
			var->ideal.x += delta;
			
			var->ideal.y = var->last.y;
			delta = cur.y - var->last.y;
			if (delta <= var->threshold && 
					delta >= -var->threshold)
				delta = 0;
			delta = (delta*var->factor)/10;
			var->ideal.y += delta;

			var->last = var->ideal;
			samp [count++] = var->ideal;
		}
	}
	return count;
}

static int lowpass_fini(struct tslib_module_info *info)
{
	kfree(info);
    return 0;
}

static const struct tslib_ops lowpass_ops =
{
	.read	= lowpass_read,
	.fini	= lowpass_fini,
};

static int lowpass_limit(struct tslib_module_info *inf, char *str, void *data)
{
	struct tslib_lowpass *var = (struct tslib_lowpass *)inf;
	unsigned long v;
	v = simple_strtoul(str, NULL, 0);
	
	if (v == ULONG_MAX)
		return -1;

	switch ((int)data) {
	case 1:
		var->factor = v;
		break;
	case 2:		
		var->threshold = v;
	default:
		return -1;
	}
	return 0;
}

static const struct tslib_vars lowpass_vars[] =
{
	{ "factor",		(void *)1, lowpass_limit },
	{ "threshold",  (void *)2, lowpass_limit },
};

#define NR_VARS (sizeof(lowpass_vars) / sizeof(lowpass_vars[0]))

struct tslib_module_info *lpf_mod_init(struct tsdev *dev, const char *params)
{
	struct tslib_lowpass *var;

	var = kzalloc(sizeof(struct tslib_lowpass),GFP_KERNEL);
	if (var == NULL)
		return NULL;

	var->module.ops = &lowpass_ops;

	var->factor = 4;
	var->threshold = 2;
	var->flags = VAR_PENUP;

	if (tslib_parse_vars(&var->module, lowpass_vars, NR_VARS, params)) {
		kfree(var);
		return NULL;
	}


	return &var->module;
}
