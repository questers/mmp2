/*
 *  tslib/plugins/linear.c
 *
 *  Copyright (C) 2001 Russell King.
 *  Copyright (C) 2005 Alberto Mardegan <mardy@sourceforge.net>
 *
 * This file is placed under the LGPL.  Please see the file
 * COPYING for more details.
 *
 * $Id: linear.c,v 1.10 2005/02/26 01:47:23 kergoth Exp $
 *
 * Linearly scale touchscreen values
 */
	 
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/slab.h>
#include "tslib.h"
#include "tslib-private.h"

struct tslib_linear {
	struct tslib_module_info module;
	int	swap_xy;

// Linear scaling and offset parameters for pressure
	int	p_offset;
	int	p_mult;
	int	p_div;

// Linear scaling and offset parameters for x,y (can include rotation)
	int	a[7];

// Screen resolution at the time when calibration was performed
	unsigned int cal_res_x;
	unsigned int cal_res_y;
};

static int
linear_read(struct tslib_module_info *info, struct ts_sample *samp, int nr)
{
	struct tslib_linear *lin = (struct tslib_linear *)info;
	int ret;
	int xtemp,ytemp;

	ret = info->next->ops->read(info->next, samp, nr);
	if (ret >= 0) {
		int nr;

		for (nr = 0; nr < ret; nr++, samp++) {
#ifdef DEBUG
			printk("BEFORE CALIB--------------------> %d %d %d\n",samp->x, samp->y, samp->p);
#endif /*DEBUG*/
			xtemp = samp->x; ytemp = samp->y;
			samp->x = 	( lin->a[2] +
					lin->a[0]*xtemp + 
					lin->a[1]*ytemp ) / lin->a[6];
			samp->y =	( lin->a[5] +
					lin->a[3]*xtemp +
					lin->a[4]*ytemp ) / lin->a[6];
			if (info->dev->res_x && lin->cal_res_x)
				samp->x = samp->x * info->dev->res_x / lin->cal_res_x;
			if (info->dev->res_y && lin->cal_res_y)
				samp->y = samp->y * info->dev->res_y / lin->cal_res_y;

			samp->p = ((samp->p + lin->p_offset)
						 * lin->p_mult) / lin->p_div;
			if (lin->swap_xy) {
				int tmp = samp->x;
				samp->x = samp->y;
				samp->y = tmp;
			}
		}
	}

	return ret;
}

static int linear_fini(struct tslib_module_info *info)
{
	kfree(info);
	return 0;
}

static const struct tslib_ops linear_ops =
{
	.read	= linear_read,
	.fini	= linear_fini,
};

static int linear_xyswap(struct tslib_module_info *inf, char *str, void *data)
{
	struct tslib_linear *lin = (struct tslib_linear *)inf;

	lin->swap_xy = 1;
	return 0;
}

static int linear_params(struct tslib_module_info *inf, char *str, void *data)
{
	
	struct tslib_linear *lin = (struct tslib_linear *)inf;
	long v;

	v = simple_strtol(str, NULL, 0);

	if (v == ULONG_MAX)
		return -1;

	switch ((int)data) {
	case 1:
		lin->a[0] = v;
		break;
	case 2:
		lin->a[1] = v;
		break;
	case 3:
		lin->a[2] = v;
		break;
	case 4:
		lin->a[3] = v;
		break;
	case 5:
		lin->a[4] = v;
		break;
	case 6:
		lin->a[5] = v;
		break;
	case 7:
		lin->a[6] = v;
		break;	
	case 8:
		lin->cal_res_x = v;
		break;
	case 9:
		lin->cal_res_y = v;
		break;	

	default:
		return -1;
	}
	return 0;
}


static const struct tslib_vars linear_vars[] =
{
	{ "xyswap",	(void *)1, linear_xyswap },
	{ "a"	,(void*)1,linear_params},
	{ "b"	,(void*)2,linear_params},
	{ "c"	,(void*)3,linear_params},
	{ "d"	,(void*)4,linear_params},
	{ "e"	,(void*)5,linear_params},
	{ "f"	,(void*)6,linear_params},
	{ "s"	,(void*)7,linear_params},
	{ "width"	,(void*)8,linear_params},
	{ "height"	,(void*)9,linear_params},
	
	
};

#define NR_VARS (sizeof(linear_vars) / sizeof(linear_vars[0]))

struct tslib_module_info *linear_mod_init(struct tsdev *dev, const char *params)
{

	struct tslib_linear *lin;

	lin = kzalloc(sizeof(struct tslib_linear),GFP_KERNEL);
	if (lin == NULL)
		return NULL;

	lin->module.ops = &linear_ops;

	// Use default values that leave ts numbers unchanged after transform
	lin->a[0] = 1;
	lin->a[1] = 0;
	lin->a[2] = 0;
	lin->a[3] = 0;
	lin->a[4] = 1;
	lin->a[5] = 0;
	lin->a[6] = 1;
	lin->p_offset = 0;
	lin->p_mult   = 1;
	lin->p_div    = 1;
	lin->swap_xy  = 0;
		
	/*
	 * Parse the parameters.
	 */
	if (tslib_parse_vars(&lin->module, linear_vars, NR_VARS, params)) {
		kfree(lin);
		return NULL;
	}

#ifdef DEBUG
	printk("a=%d,b=%d,c=%d,d=%d,e=%d,f=%d,s=%d,w=%d,h=%d\n",
		lin->a[0],lin->a[1],lin->a[2],lin->a[3],lin->a[4],
		lin->a[5],lin->a[6],lin->cal_res_x,lin->cal_res_y);
#endif

	return &lin->module;
}


