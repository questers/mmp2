/*
 * touch_virtual.c
 * virtual touch panel driver for android ,only for android debug
 *
 * Copyright (C) 2011 Questers Tech,Ltd.ALL RIGHTS RESERVED.
 *  
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <plat/mfp.h>

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif






struct touch_device{
	struct input_dev *idev;
	
	struct early_suspend    early_suspend;
	
};

static struct touch_device* thiz;

/*
 * Start the touchscreen thread and
 * the touch digitiser.
 */
static int virtouch_open(struct input_dev *idev)
{
 	return 0;
}

/*
 * Kill the touchscreen thread and stop
 * the touch digitiser.
 */
static void virtouch_close(struct input_dev *idev)
{
}







#ifdef CONFIG_HAS_EARLYSUSPEND
static void virtouch_early_suspend(struct early_suspend *h)
{
}
static void virtouch_late_resume(struct early_suspend *h)
{

}
#endif

static int __init virtouch_init(void)
{
	struct touch_device* touch;
	struct input_dev* input_device;
	u8 val;
	int ret = 0;		

	
	
	touch = kzalloc(sizeof(struct touch_device),GFP_KERNEL);
	if(!touch)
	{
		return -ENOMEM;
	}	

	/* tell input system what we events we accept and register */
	input_device= input_allocate_device();
	if (NULL==input_device  ) {
		ret = -ENOMEM;
		goto err1;
	}

	input_device->name = "touch_virtual";
	input_device->phys = "touch_virtual/input1";

	input_device->open = virtouch_open;
	input_device->close = virtouch_close;
	input_device->id.bustype = BUS_I2C;
	input_device->id.vendor  = 0xdead;
	input_device->id.product = 0xdead;
	
	input_device->evbit[0]= BIT_MASK(EV_SYN)|BIT_MASK(EV_KEY)|BIT_MASK(EV_ABS);
	input_device->absbit[BIT_WORD(ABS_MT_POSITION_X)] |= BIT_MASK(ABS_MT_POSITION_X);
	input_device->absbit[BIT_WORD(ABS_MT_POSITION_Y)] |= BIT_MASK(ABS_MT_POSITION_Y);
	input_device->absbit[BIT_WORD(ABS_MT_TOUCH_MAJOR)] |= BIT_MASK(ABS_MT_TOUCH_MAJOR);
	input_device->absbit[BIT_WORD(ABS_MT_WIDTH_MAJOR)] |= BIT_MASK(ABS_MT_WIDTH_MAJOR);
	
	input_set_abs_params(input_device, ABS_MT_POSITION_X, 0, 1024, 0, 0);
	input_set_abs_params(input_device, ABS_MT_POSITION_Y, 0, 600, 0, 0);
	input_set_abs_params(input_device, ABS_MT_TOUCH_MAJOR, 0, 0xff, 0, 0);
	input_set_abs_params(input_device, ABS_MT_WIDTH_MAJOR, 0, 0xffff, 0, 0);


	touch->idev = input_device;


#ifdef CONFIG_HAS_EARLYSUSPEND
		touch->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN+1;
		touch->early_suspend.suspend = virtouch_early_suspend;
		touch->early_suspend.resume = virtouch_late_resume;
		register_early_suspend(&touch->early_suspend);
#endif

	ret = input_register_device(input_device);
	if (ret) {
		goto err2;
	}

  thiz = touch;
	return 0;

err2:
	input_free_device(input_device);	
err1:		
	kfree(touch);
	return ret;
}

static void __exit virtouch_exit(void)
{
	if(thiz)
	{
		input_unregister_device(thiz->idev);	
		kfree(thiz);
	}
}


module_init(virtouch_init);
module_exit(virtouch_exit);

MODULE_DESCRIPTION("Quester Virtual Touch Screen driver");
MODULE_AUTHOR("Raymond Wang");
MODULE_LICENSE("GPL");

