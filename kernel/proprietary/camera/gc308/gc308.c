/*
 * A V4L2 driver for GalaxyCore gc308 cameras.
 *
 * Copyright 2006 One Laptop Per Child Association, Inc.  Written
 * by Jonathan Corbet with substantial inspiration from Mark
 * McClelland's ovcamchip code.
 *
 * Copyright 2006-7 Jonathan Corbet <corbet@lwn.net>
 *
 * This file may be distributed under the terms of the GNU General
 * Public License, version 2.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/videodev.h>
#include <media/v4l2-common.h>
#include <media/v4l2-chip-ident.h>
#include <linux/i2c.h>
#include <mach/hardware.h>
#include <mach/camera.h>
#include <mach/mfp.h>

#include <linux/clk.h>
#include <mach/pxa910.h>
#include "pxa688_camera.h"
#include "gc308.h"

#define gc308_FRAME_RATE 30


/*
 * Basic window sizes.  These probably belong somewhere more globally
 * useful.
 */

#define QXGA_WIDTH	2048
#define QXGA_HEIGHT	1536

#define UXGA_WIDTH	1600
#define UXGA_HEIGHT 1200

#define SXGA_WIDTH	1280
#define SXGA_HEIGHT 1024

#define XGA_WIDTH	1024
#define XGA_HEIGHT  768

#define VGA_WIDTH	640
#define VGA_HEIGHT	480

#define QVGA_WIDTH	320
#define QVGA_HEIGHT	240

#define CIF_WIDTH	352
#define CIF_HEIGHT	288

#define QCIF_WIDTH	176
#define	QCIF_HEIGHT	144

#define COM7_FMT_VGA 0
#define COM7_FMT_QVGA 0
#define COM7_FMT_VGA 0

#define REG_CLKRC 0x3011
#define CLK_EXT 0x40
#define CLK_SCALE 0x3f

/*
 * Information we maintain about a known sensor.
 */
struct gc308_format_struct;  /* coming later */
struct gc308_info {
	struct gc308_format_struct *fmt;  /* Current format */
	unsigned char sat;		/* Saturation value */
	int hue;			/* Hue value */
};

/*
 * The default register settings, as obtained from GalaxyCore.  There
 * is really no making sense of most of these - lots of "reserved" values
 * and such.
 *
 * These settings give VGA YUYV.
 */

struct regval_list {
	unsigned char reg_num;
	unsigned char value;
};


/*
 * Here we'll try to encapsulate the changes for just the output
 * video format.
 *
 * RGB656 and YUV422 come from OV; RGB444 is homebrewed.
 *
 * IMPORTANT RULE: the first entry must be for COM7, see gc308_s_fmt for why.
 */

/* yuv422 640x480 */
static struct regval_list gc308_fmt_yuv422_vga[] =
{
	{0xfe, 0x01},
	{0x54, 0x11},
	{0x55, 0x03},
	{0x56, 0x00},
	{0x57, 0x00},
	{0x58, 0x00},
	{0x59, 0x00},
	{0xfe, 0x00},
	{0x46, 0x00},

	{0x01, 0x6a},
	{0x02, 0x89},
	{0x0f, 0x00},


	// 20M 20fps
	{0xe2, 0x00},	//anti-flicker step [11:8]
	{0xe3, 0x7d},	//anti-flicker step [7:0]

	{0xe4, 0x02},	//exp level 1  20fps 
	{0xe5, 0x71},
	{0xe6, 0x02},	//exp level 2  20fps
	{0xe7, 0x71},
	{0xe8, 0x02},	//exp level 3  20fps
	{0xe9, 0x71},
	{0xea, 0x0c},	//exp level 4  4fps
	{0xeb, 0x35},

	{0xff, 0xff},
};

/* yuv422 352x288 */
static struct regval_list gc308_fmt_yuv422_cif[] =
{
	{0xfe, 0x01},
	{0x54, 0x55},
	{0x55, 0x03},
	{0x56, 0x02},
	{0x57, 0x04},
	{0x58, 0x02},
	{0x59, 0x04},
	{0xfe, 0x00},
	{0x46, 0x80},
	{0x47, 0x00},
	{0x48, 0x00},
	{0x49, 0x01},
	{0x4a, 0x20},
	{0x4b, 0x01},
	{0x4c, 0x60},
	{0xff, 0xff},
};

/* yuv422 176x144 */
static struct regval_list gc308_fmt_yuv422_qcif[] =
{
	{0xfe, 0x01},
	{0x54, 0x33},
	{0x55, 0x03},
	{0x56, 0x00},
	{0x57, 0x00},
	{0x58, 0x00},
	{0x59, 0x00},
	{0xfe, 0x00},
	{0x46, 0x80},
	{0x47, 0x00},
	{0x48, 0x00},
	{0x49, 0x00},
	{0x4a, 0x90},
	{0x4b, 0x00},
	{0x4c, 0xb0},
	{0xff, 0xff},
};

/* default setting */
static struct regval_list gc308_default_regs[] =
{
	{0xfe, 0x80},

	{0xfe, 0x00},	// set page0

	{0xd2, 0x10},	// close AEC
	{0x22, 0x55},	// close AWB

	{0x03, 0x01},
	{0x04, 0x2c},
	{0x5a, 0x56},
	{0x5b, 0x40},
	{0x5c, 0x4a},

	{0x22, 0x57},	// Open AWB

	///////////////banding setting/////////
#if 0 /* 16fps */
	{0x01, 0xfa},
	{0x02, 0x70},
	{0x0f, 0x01},


	{0xe2, 0x00},	//anti-flicker step [11:8]
	{0xe3, 0x64},	//anti-flicker step [7:0]

	{0xe4, 0x02},	//exp level 1  16.67fps 
	{0xe5, 0x58},
	{0xe6, 0x03},	//exp level 2 16 fps
	{0xe7, 0x20},
	{0xe8, 0x04},	//exp level 3  16 fps
	{0xe9, 0xb0},
	{0xea, 0x09},	//exp level 4 6.00fps
	{0xeb, 0xc4},

#else /* 20fps */

	{0x01, 0x32},
	{0x02, 0x70},
	{0x0f, 0x01},


	{0xe2, 0x00},	//anti-flicker step [11:8]
	{0xe3, 0x64},	//anti-flicker step [7:0]

	{0xe4, 0x02},	//exp level 1  20fps 
	{0xe5, 0x58},
	{0xe6, 0x02},	//exp level 2  20fps
	{0xe7, 0x58},
	{0xe8, 0x02},	//exp level 3  20fps
	{0xe9, 0x58},
	{0xea, 0x06},	//exp level 4  6.25fps
	{0xeb, 0x40},

	/**************************
	//////20M mclk   fixed 16 fps//////
	{0x01, 0x6a},
	{0x02, 0x89},
	{0x0f, 0x00},


	{0xe2, 0x00},	//anti-flicker step [11:8]
	{0xe3, 0x7D},	//anti-flicker step [7:0]

	{0xe4, 0x02},	//exp level 1  20fps 
	{0xe5, 0x71},
	{0xe6, 0x02},	//exp level 2  20fps
	{0xe7, 0x71},
	{0xe8, 0x02},	//exp level 3  20fps
	{0xe9, 0x71},
	{0xea, 0x07},	//exp level 4  6.25fps
	{0xeb, 0xD0},
	
	**********************/
#endif

	{0x05, 0x00},
	{0x06, 0x00},
	{0x07, 0x00},
	{0x08, 0x00},
	{0x09, 0x01},
	{0x0a, 0xe8},
	{0x0b, 0x02},
	{0x0c, 0x88},
	{0x0d, 0x02},
	{0x0e, 0x02},
	{0x10, 0x22},
	{0x11, 0xfd},
	{0x12, 0x2a},
	{0x13, 0x00},

	{0x15, 0x0a},
	{0x16, 0x05},
	{0x17, 0x01},
	{0x18, 0x44},
	{0x19, 0x44},
	{0x1a, 0x1e},
	{0x1b, 0x00},
	{0x1c, 0xc1},
	{0x1d, 0x08},
	{0x1e, 0x60},
	{0x1f, 0x16},


	{0x20, 0xff},
	{0x21, 0xf8},
	{0x22, 0x57},
	{0x24, 0xa0}, /* format */
	{0x25, 0x0f}, /* hsync/vsync enable */


	{0x26, 0x03},
	{0x2f, 0x01},
	{0x30, 0xf7},
	{0x31, 0x50},
	{0x32, 0x00},
	{0x39, 0x04},
	{0x3a, 0x18},
	{0x3b, 0x20},
	{0x3c, 0x00},
	{0x3d, 0x00},
	{0x3e, 0x00},
	{0x3f, 0x00},
	{0x50, 0x10},
	{0x53, 0x82},
	{0x54, 0x80},
	{0x55, 0x80},
	{0x56, 0x82},
	{0x8b, 0x40},
	{0x8c, 0x40},
	{0x8d, 0x40},
	{0x8e, 0x2e},
	{0x8f, 0x2e},
	{0x90, 0x2e},
	{0x91, 0x3c},
	{0x92, 0x50},
	{0x5d, 0x12},
	{0x5e, 0x1a},
	{0x5f, 0x24},
	{0x60, 0x07},
	{0x61, 0x15},
	{0x62, 0x08},
	{0x64, 0x03},
	{0x66, 0xe8},
	{0x67, 0x86},
	{0x68, 0xa2},
	{0x69, 0x18},
	{0x6a, 0x0f},
	{0x6b, 0x00},
	{0x6c, 0x5f},
	{0x6d, 0x8f},
	{0x6e, 0x55},
	{0x6f, 0x38},
	{0x70, 0x15},
	{0x71, 0x33},
	{0x72, 0xdc},
	{0x73, 0x80},
	{0x74, 0x02},
	{0x75, 0x3f},
	{0x76, 0x02},
	{0x77, 0x36},
	{0x78, 0x88},
	{0x79, 0x81},
	{0x7a, 0x81},
	{0x7b, 0x22},
	{0x7c, 0xff},
	{0x93, 0x48},
	{0x94, 0x00},
	{0x95, 0x05},
	{0x96, 0xe8},
	{0x97, 0x40},
	{0x98, 0xf0},
	{0xb1, 0x38},
	{0xb2, 0x38},
	{0xbd, 0x38},
	{0xbe, 0x36},
	{0xd0, 0xc9},
	{0xd1, 0x10},

	{0xd3, 0x80},
	{0xd5, 0xf2},
	{0xd6, 0x16},
	{0xdb, 0x92},
	{0xdc, 0xa5},
	{0xdf, 0x23},
	{0xd9, 0x00},
	{0xda, 0x00},
	{0xe0, 0x09},

	{0xed, 0x04},
	{0xee, 0xa0},
	{0xef, 0x40},
	{0x80, 0x03},
	{0x80, 0x03},
	{0x9F, 0x10},
	{0xA0, 0x20},
	{0xA1, 0x38},
	{0xA2, 0x4E},
	{0xA3, 0x63},
	{0xA4, 0x76},
	{0xA5, 0x87},
	{0xA6, 0xA2},
	{0xA7, 0xB8},
	{0xA8, 0xCA},
	{0xA9, 0xD8},
	{0xAA, 0xE3},
	{0xAB, 0xEB},
	{0xAC, 0xF0},
	{0xAD, 0xF8},
	{0xAE, 0xFD},
	{0xAF, 0xFF},
	{0xc0, 0x00},
	{0xc1, 0x10},
	{0xc2, 0x1C},
	{0xc3, 0x30},
	{0xc4, 0x43},
	{0xc5, 0x54},
	{0xc6, 0x65},
	{0xc7, 0x75},
	{0xc8, 0x93},
	{0xc9, 0xB0},
	{0xca, 0xCB},
	{0xcb, 0xE6},
	{0xcc, 0xFF},
	{0xf0, 0x02},
	{0xf1, 0x01},
	{0xf2, 0x01},
	{0xf3, 0x30},
	{0xf9, 0x9f},
	{0xfa, 0x78},

	//---------------------------------------------------------------
	{0xfe, 0x01},// set page1

	{0x00, 0xf5},
	{0x02, 0x1a},
	{0x0a, 0xa0},
	{0x0b, 0x60},
	{0x0c, 0x08},
	{0x0e, 0x4c},
	{0x0f, 0x39},
	{0x11, 0x3f},
	{0x12, 0x72},
	{0x13, 0x13},
	{0x14, 0x42},
	{0x15, 0x43},
	{0x16, 0xc2},
	{0x17, 0xa8},
	{0x18, 0x18},
	{0x19, 0x40},
	{0x1a, 0xd0},
	{0x1b, 0xf5},
	{0x70, 0x40},
	{0x71, 0x58},
	{0x72, 0x30},
	{0x73, 0x48},
	{0x74, 0x20},
	{0x75, 0x60},
	{0x77, 0x20},
	{0x78, 0x32},
	{0x30, 0x03},
	{0x31, 0x40},
	{0x32, 0xe0},
	{0x33, 0xe0},
	{0x34, 0xe0},
	{0x35, 0xb0},
	{0x36, 0xc0},
	{0x37, 0xc0},
	{0x38, 0x04},
	{0x39, 0x09},
	{0x3a, 0x12},
	{0x3b, 0x1C},
	{0x3c, 0x28},
	{0x3d, 0x31},
	{0x3e, 0x44},
	{0x3f, 0x57},
	{0x40, 0x6C},
	{0x41, 0x81},
	{0x42, 0x94},
	{0x43, 0xA7},
	{0x44, 0xB8},
	{0x45, 0xD6},
	{0x46, 0xEE},
	{0x47, 0x0d},
	{0xfe, 0x00}, // set page0

	//-----------Update the registers 2010/07/06-------------//
	//Registers of Page0
	{0xfe, 0x00}, // set page0
	{0x10, 0x26},
	{0x11, 0x0d},  // fd,modified by mormo 2010/07/06
	{0x1a, 0x2a},  // 1e,modified by mormo 2010/07/06

	{0x1c, 0x49}, // c1,modified by mormo 2010/07/06
	{0x1d, 0x9a}, // 08,modified by mormo 2010/07/06
	{0x1e, 0x61}, // 60,modified by mormo 2010/07/06

	{0x3a, 0x20},

	{0x50, 0x14},  // 10,modified by mormo 2010/07/06
	{0x53, 0x80},
	{0x56, 0x80},

	{0x8b, 0x20}, //LSC
	{0x8c, 0x20},
	{0x8d, 0x20},
	{0x8e, 0x14},
	{0x8f, 0x10},
	{0x90, 0x14},

	{0x94, 0x02},
	{0x95, 0x07},
	{0x96, 0xe0},

	{0xb1, 0x40}, // YCPT
	{0xb2, 0x40},
	{0xb3, 0x40},
	{0xb6, 0xe0},

	{0xd0, 0xcb}, // AECT  c9,modifed by mormo 2010/07/06
	{0xd3, 0x48}, // 80,modified by mormor 2010/07/06

	{0xf2, 0x02},
	{0xf7, 0x12},
	{0xf8, 0x0a},

	//Registers of Page1
	{0xfe, 0x01},// set page1
	{0x02, 0x20},
	{0x04, 0x10},
	{0x05, 0x08},
	{0x06, 0x20},
	{0x08, 0x0a},

	{0x0e, 0x44},
	{0x0f, 0x32},
	{0x10, 0x41},
	{0x11, 0x37},
	{0x12, 0x22},
	{0x13, 0x19},
	{0x14, 0x44},
	{0x15, 0x44},

	{0x19, 0x50},
	{0x1a, 0xd8},

	{0x32, 0x10},

	{0x35, 0x00},
	{0x36, 0x80},
	{0x37, 0x00},
	//-----------Update the registers end---------//

	{0xfe, 0x00}, // set page0
	{0xd2, 0x90},

	//-----------GAMMA Select(3)---------------//
	{0x9F, 0x10},
	{0xA0, 0x20},
	{0xA1, 0x38},
	{0xA2, 0x4E},
	{0xA3, 0x63},
	{0xA4, 0x76},
	{0xA5, 0x87},
	{0xA6, 0xA2},
	{0xA7, 0xB8},
	{0xA8, 0xCA},
	{0xA9, 0xD8},
	{0xAA, 0xE3},
	{0xAB, 0xEB},
	{0xAC, 0xF0},
	{0xAD, 0xF8},
	{0xAE, 0xFD},
	{0xAF, 0xFF},

	// GC0308_GAMMA_Select
	//smallest gamma curve
	{0x9F, 0x0B},
	{0xA0, 0x16},
	{0xA1, 0x29},
	{0xA2, 0x3C},
	{0xA3, 0x4F},
	{0xA4, 0x5F},
	{0xA5, 0x6F},
	{0xA6, 0x8A},
	{0xA7, 0x9F},
	{0xA8, 0xB4},
	{0xA9, 0xC6},
	{0xAA, 0xD3},
	{0xAB, 0xDD},
	{0xAC, 0xE5},
	{0xAD, 0xF1},
	{0xAE, 0xFA},
	{0xAF, 0xFF},

	{0x9F, 0x0E},
	{0xA0, 0x1C},
	{0xA1, 0x34},
	{0xA2, 0x48},
	{0xA3, 0x5A},
	{0xA4, 0x6B},
	{0xA5, 0x7B},
	{0xA6, 0x95},
	{0xA7, 0xAB},
	{0xA8, 0xBF},
	{0xA9, 0xCE},
	{0xAA, 0xD9},
	{0xAB, 0xE4},
	{0xAC, 0xEC},
	{0xAD, 0xF7},
	{0xAE, 0xFD},
	{0xAF, 0xFF},

	{0x9F, 0x10},
	{0xA0, 0x20},
	{0xA1, 0x38},
	{0xA2, 0x4E},
	{0xA3, 0x63},
	{0xA4, 0x76},
	{0xA5, 0x87},
	{0xA6, 0xA2},
	{0xA7, 0xB8},
	{0xA8, 0xCA},
	{0xA9, 0xD8},
	{0xAA, 0xE3},
	{0xAB, 0xEB},
	{0xAC, 0xF0},
	{0xAD, 0xF8},
	{0xAE, 0xFD},
	{0xAF, 0xFF},

	{0x9F, 0x14},
	{0xA0, 0x28},
	{0xA1, 0x44},
	{0xA2, 0x5D},
	{0xA3, 0x72},
	{0xA4, 0x86},
	{0xA5, 0x95},
	{0xA6, 0xB1},
	{0xA7, 0xC6},
	{0xA8, 0xD5},
	{0xA9, 0xE1},
	{0xAA, 0xEA},
	{0xAB, 0xF1},
	{0xAC, 0xF5},
	{0xAD, 0xFB},
	{0xAE, 0xFE},
	{0xAF, 0xFF},

	//largest gamma curve
	{0x9F, 0x15},
	{0xA0, 0x2A},
	{0xA1, 0x4A},
	{0xA2, 0x67},
	{0xA3, 0x79},
	{0xA4, 0x8C},
	{0xA5, 0x9A},
	{0xA6, 0xB3},
	{0xA7, 0xC5},
	{0xA8, 0xD5},
	{0xA9, 0xDF},
	{0xAA, 0xE8},
	{0xAB, 0xEE},
	{0xAC, 0xF3},
	{0xAD, 0xFA},
	{0xAE, 0xFD},
	{0xAF, 0xFF},

	//-----------GAMMA Select End--------------//
	{0x14, 0x10},
	{0xff, 0xff},
};

/*
 * Store information about the video data format.  The color matrix
 * is deeply tied into the format, so keep the relevant values here.
 * The magic matrix nubmers come from GalaxyCore.
 */
static struct gc308_format_struct {
	__u8 *desc;
	__u32 pixelformat;
	struct regval_list *regs;
	int cmatrix[CMATRIX_LEN];
	int bpp;   /* bits per pixel */
} gc308_formats[] =
{
	{
		.desc		= "YUYV422 planar",
		.pixelformat	= V4L2_PIX_FMT_YUV422P,
		.cmatrix	= { 128, -128, 0, -34, -94, 128 },
		.bpp		= 16,
	},
	{
		.desc		= "UYVY422",
		.pixelformat	= V4L2_PIX_FMT_UYVY,
		.cmatrix	= { 128, -128, 0, -34, -94, 128 },
		.bpp		= 16,
	},
};
#define N_GC308_FMTS ARRAY_SIZE(gc308_formats)


/*TODO - also can use ccic size register 0x34 to do same thing for cropping...anyway, sensor doing it is better?
  0x3020~0x3027*/
static struct gc308_win_size {
	int	width;
	int	height;
	unsigned char com7_bit;
	int	hstart;		/* Start/stop values for the camera.  Note */
	int	hstop;		/* that they do not always make complete */
	int	vstart;		/* sense to humans, but evidently the sensor */
	int	vstop;		/* will do the right thing... */
	struct regval_list *regs; /* Regs to tweak */
	/* h/vref stuff */
} gc308_win_sizes[] = {
	/* QCIF */
	{
		.width		= QCIF_WIDTH,
		.height		= QCIF_HEIGHT,
		.com7_bit	= COM7_FMT_VGA, /* see comment above */
		.hstart		= 456,		/* Empirically determined */
		.hstop		=  24,
		.vstart		=  14,
		.vstop		= 494,
		.regs 		= NULL,
		//		.regs 		= gc308_qcif_regs,
	},
	/* CIF */
	{
		.width		= CIF_WIDTH,
		.height		= CIF_HEIGHT,
		//.com7_bit	= COM7_FMT_CIF,
		.hstart		= 170,		/* Empirically determined */
		.hstop		=  90,
		.vstart		=  14,
		.vstop		= 494,
		.regs 		= NULL,
	},
	/* VGA */
	{
		.width		= VGA_WIDTH,
		.height		= VGA_HEIGHT,
		.com7_bit	= COM7_FMT_VGA,
		.hstart		= 158,		/* These values from */
		.hstop		=  14,		/* GalaxyCore */
		.vstart		=  10,
		.vstop		= 490,
		.regs 		= NULL,
	},
};

#define N_GC308_WIN_SIZES (ARRAY_SIZE(gc308_win_sizes))

/*
 * flip and mirror control
 * 0: normal
 * 1: mirror on
 * 2: flip on
 * 3: flip and mirror on
 */
static int fm_low = 0;
module_param(fm_low, int, 0);
static int fm_high = 0;
module_param(fm_high, int, 0);

/*
 * Low-level register I/O.
 */

static int gc308_read(struct i2c_client *c, u8 reg, u8* value)
{
	int ret = i2c_smbus_read_byte_data(c, reg);
	if (ret < 0)
	{
		return ret;
	}
	else
	{
		*value = (u8) ret;
		return 0;
	}
}

static int gc308_write(struct i2c_client *c, u8 reg, u8 value)
{
	return i2c_smbus_write_byte_data(c, reg, value);
}

/*
 * Write a list of register settings; ff/ff stops the process.
 */
static int gc308_write_array(struct i2c_client *c, struct regval_list *vals)
{
	while (vals->reg_num != 0xff || vals->value != 0xff) {
		int ret = gc308_write(c, vals->reg_num, vals->value);
		if (ret < 0)
			return ret;
		vals++;
	}

	return 0;
}


/*
 * Stuff that knows about the sensor.
 */
static void gc308_reset(struct i2c_client *client)
{
	//soft reset
	gc308_write(client, GC308_SYS, 0x80);
	msleep(1);
}

#if 0
static int gc308_init(struct i2c_client *client)	//TODO - currently not needed on 3640
{
	return gc308_write_array(client, gc308_default_regs);
}
#endif

static int gc308_detect(struct i2c_client *client)
{
	unsigned char v=0;
	int ret;

	/*
	 * no MID register found. OK, we know we have an GalaxyCore chip...but which one?
	 */
	ret = gc308_read(client, GC308_PID, &v);
	printk("gc308_read returned %d\n", ret);
	if (ret < 0)
		return ret;

	if (v == DEFAULT_PID)
	{
		printk(KERN_NOTICE "GalaxyCore gc308 sensor detected\n");
		return 0;
	}
	else
	{
		printk(KERN_ERR "Detected unknown sensor with PID: 0x%2x\n", v);
		return -ENODEV;
	}
}


#if 0
/*
 * Store a set of start/stop values into the camera.
 */
static int gc308_set_hw(struct i2c_client *client, int hstart, int hstop,
		int vstart, int vstop)
{
	int ret;
	unsigned char v;
	/*
	 * Horizontal: 11 bits, top 8 live in hstart and hstop.  Bottom 3 of
	 * hstart are in href[2:0], bottom 3 of hstop in href[5:3].  There is
	 * a mystery "edge offset" value in the top two bits of href.
	 */
	ret =  gc308_write(client, REG_HSTART, (hstart >> 3) & 0xff);
	ret += gc308_write(client, REG_HSTOP, (hstop >> 3) & 0xff);
	ret += gc308_read(client, REG_HREF, &v);
	v = (v & 0xc0) | ((hstop & 0x7) << 3) | (hstart & 0x7);
	msleep(10);
	ret += gc308_write(client, REG_HREF, v);
	/*
	 * Vertical: similar arrangement, but only 10 bits.
	 */
	ret += gc308_write(client, REG_VSTART, (vstart >> 2) & 0xff);
	ret += gc308_write(client, REG_VSTOP, (vstop >> 2) & 0xff);
	ret += gc308_read(client, REG_VREF, &v);
	v = (v & 0xf0) | ((vstop & 0x3) << 2) | (vstart & 0x3);
	msleep(10);
	ret += gc308_write(client, REG_VREF, v);
	return ret;
	return 0;
}
#endif

static int gc308_enum_fmsize(struct i2c_client *client, struct v4l2_frmsizeenum *argp)
{
	struct v4l2_frmsizeenum frmsize;

	if (copy_from_user(&frmsize, argp, sizeof(frmsize)))
		return -EFAULT;

	switch (frmsize.pixel_format) {
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420:
			if (frmsize.index == ARRAY_SIZE(gc308_win_sizes))
				return -EINVAL;
			else if (frmsize.index > ARRAY_SIZE(gc308_win_sizes)) {
				printk(KERN_ERR "camera: gc308 enum unsupported preview format size!\n");
				return -EINVAL;
			} else {
				frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
				frmsize.discrete.height = gc308_win_sizes[frmsize.index].height;
				frmsize.discrete.width = gc308_win_sizes[frmsize.index].width;
				break;
			}
		default:
			printk(KERN_ERR "camera: gc308 enum format size with unsupported format!\n");
			return -EINVAL;
	}

	if (copy_to_user(argp, &frmsize, sizeof(frmsize)))
		return -EFAULT;
	
	return 0;
}

static int gc308_enum_fmt(struct i2c_client *c, struct v4l2_fmtdesc *fmt)
{
	struct gc308_format_struct *ofmt;

	if (fmt->index >= N_GC308_FMTS)
		return -EINVAL;

	ofmt = gc308_formats + fmt->index;
	fmt->flags = 0;
	strcpy(fmt->description, ofmt->desc);
	fmt->pixelformat = ofmt->pixelformat;
	return 0;
}


static int gc308_try_fmt(struct i2c_client *c, struct v4l2_format *fmt,
		struct gc308_format_struct **ret_fmt,
		struct gc308_win_size **ret_wsize)
{
	int index;
	struct gc308_win_size *wsize;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	for (index = 0; index < N_GC308_FMTS; index++)
		if (gc308_formats[index].pixelformat == pix->pixelformat)
			break;
	if (index >= N_GC308_FMTS){
		printk("unsupported format[%d]!\n", pix->pixelformat);
		return -EINVAL;
	}
	if (ret_fmt != NULL)
		*ret_fmt = gc308_formats + index;
	/*
	 * Fields: the OV devices claim to be progressive.
	 */
	if (pix->field == V4L2_FIELD_ANY)
		pix->field = V4L2_FIELD_NONE;
	else if (pix->field != V4L2_FIELD_NONE)
	{
		printk("invalid field\n");
		//return -EINVAL; /* don't return, for mpt. -guang */
	}
	/*
	 * Round requested image size down to the nearest
	 * we support, but not below the smallest.
	 */
	for (wsize = gc308_win_sizes; wsize < gc308_win_sizes + N_GC308_WIN_SIZES;
			wsize++)
		if (pix->width <= wsize->width && pix->height <= wsize->height)
			break;
	if (wsize >= gc308_win_sizes + N_GC308_WIN_SIZES){
		printk("size exceed and set as QXGA!\n");
		wsize--;   /* Take the smallest one */
	}
	if (ret_wsize != NULL)
		*ret_wsize = wsize;
	/*
	 * Note the size we'll actually handle.
	 */
#if 0
	pix->width = wsize->width;
	pix->height = wsize->height;
#endif
	pix->bytesperline = pix->width*gc308_formats[index].bpp/8;
	pix->sizeimage = pix->height*pix->bytesperline;

	if (ret_fmt == NULL)
		return 0;
	
	switch (pix->pixelformat)
	{
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420:
			switch (wsize->width)
			{
				case VGA_WIDTH:
					(*ret_fmt)->regs = gc308_fmt_yuv422_vga;
					break;
				case CIF_WIDTH:
					(*ret_fmt)->regs = gc308_fmt_yuv422_cif;
					break;
				case QCIF_WIDTH:
					(*ret_fmt)->regs = gc308_fmt_yuv422_qcif;
					break;
				default:
					printk("unsupported size!\n");
					return -EINVAL;
			}
			break;
		default:
			printk("unsupported format - %d!\n", pix->pixelformat);
			return -EINVAL;
	}
	return 0;
}

/*
 * Set a format.
 */
static int gc308_s_fmt(struct i2c_client *c, struct v4l2_format *fmt)
{
	int ret;
	u8 fm;
	struct gc308_format_struct *ovfmt;
	struct gc308_win_size *wsize;
	int fm_ctrl;
	struct sensor_platform_data *pdata = c->dev.platform_data;

	ret = gc308_try_fmt(c, fmt, &ovfmt, &wsize);
	if (ret)
		return ret;

	gc308_write_array(c, gc308_default_regs); /* first set default. -guang */
	gc308_write_array(c, ovfmt->regs);

	/* flip and mirror */
	if (pdata->id == SENSOR_LOW)
		fm_ctrl = fm_low;
	else
		fm_ctrl = fm_high;
	fm_ctrl &= 0x3;
	gc308_write(c, 0xfe, 0x00); /* set page 0 */
	gc308_read(c, 0x29, &fm);
	fm &= 0xfc;
	fm |= fm_ctrl;
	gc308_write(c, 0xfe, 0x00); /* set page 0 */
	gc308_write(c, 0x14, fm);

	return ret;
}

/*
 * Get a format.
 */
static int gc308_g_fmt(struct i2c_client *c, struct v4l2_format *fmt)
{
	/*
	unsigned char w_msb;
	unsigned char w_lsb;
	unsigned char h_msb;
	unsigned char h_lsb;

	gc308_read(c, 0x3088, &w_msb);
	gc308_read(c, 0x3089, &w_lsb);
	gc308_read(c, 0x308a, &h_msb);
	gc308_read(c, 0x308b, &h_lsb);

	fmt->type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	fmt->fmt.pix.width = (w_msb<<8)|w_lsb;
	fmt->fmt.pix.height = (h_msb<<8)|h_lsb;
	fmt->fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
	fmt->fmt.pix.field = V4L2_FIELD_NONE;*/

	return 0;
}

/*
 * Implement G/S_PARM.  There is a "high quality" mode we could try
 * to do someday; for now, we just do the frame rate tweak.
 */
static int gc308_g_parm(struct i2c_client *c, struct v4l2_streamparm *parms)
{
#if 0        
	struct v4l2_captureparm *cp = &parms->parm.capture;
	unsigned char clkrc;
	int ret;

	if (parms->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
		return -EINVAL;
	ret = gc308_read(c, REG_CLKRC, &clkrc);
	if (ret < 0)
		return ret;
	memset(cp, 0, sizeof(struct v4l2_captureparm));
	cp->capability = V4L2_CAP_TIMEPERFRAME;
	cp->timeperframe.numerator = 1;
	cp->timeperframe.denominator = gc308_FRAME_RATE;
	if ((clkrc & CLK_EXT) == 0 && (clkrc & CLK_SCALE) > 1)
		cp->timeperframe.denominator /= (clkrc & CLK_SCALE);
#endif    
	return 0;
}

static int gc308_s_parm(struct i2c_client *c, struct v4l2_streamparm *parms)
{
#if 0
	struct v4l2_captureparm *cp = &parms->parm.capture;
	struct v4l2_fract *tpf = &cp->timeperframe;
	unsigned char clkrc;
	int ret, div;

	if (parms->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
		return -EINVAL;
	if (cp->extendedmode != 0)
		return -EINVAL;
	/*
	 * CLKRC has a reserved bit, so let's preserve it.
	 */
	ret = gc308_read(c, REG_CLKRC, &clkrc);
	if (ret < 0)
		return ret;
	if (tpf->numerator == 0 || tpf->denominator == 0)
		div = 1;  /* Reset to full rate */
	else
		div = (tpf->numerator*gc308_FRAME_RATE)/tpf->denominator;
	if (div == 0)
		div = 1;
	else if (div > CLK_SCALE)
		div = CLK_SCALE;
	clkrc = (clkrc & 0xc0) | div ;
	tpf->numerator = 1;
	tpf->denominator = gc308_FRAME_RATE/div;
	return gc308_write(c, REG_CLKRC, clkrc);
#endif
	return 0;
}

static int gc308_s_input(struct i2c_client *c, int *id)
{
	return 0;
}

/*
 * Code for dealing with controls.
 */


/*TODO - need to port below register codes for 3640...maybe not used*/


static int gc308_store_cmatrix(struct i2c_client *client,
		int matrix[CMATRIX_LEN])
{
#if 0
	int i, ret;
	unsigned char signbits;

	/*
	 * Weird crap seems to exist in the upper part of
	 * the sign bits register, so let's preserve it.
	 */
	ret = gc308_read(client, REG_CMATRIX_SIGN, &signbits);
	signbits &= 0xc0;

	for (i = 0; i < CMATRIX_LEN; i++) {
		unsigned char raw;

		if (matrix[i] < 0) {
			signbits |= (1 << i);
			if (matrix[i] < -255)
				raw = 0xff;
			else
				raw = (-1 * matrix[i]) & 0xff;
		}
		else {
			if (matrix[i] > 255)
				raw = 0xff;
			else
				raw = matrix[i] & 0xff;
		}
		ret += gc308_write(client, REG_CMATRIX_BASE + i, raw);
	}
	ret += gc308_write(client, REG_CMATRIX_SIGN, signbits);
	return ret;
#endif
	return 0;
}


/*
 * Hue also requires messing with the color matrix.  It also requires
 * trig functions, which tend not to be well supported in the kernel.
 * So here is a simple table of sine values, 0-90 degrees, in steps
 * of five degrees.  Values are multiplied by 1000.
 *
 * The following naive approximate trig functions require an argument
 * carefully limited to -180 <= theta <= 180.
 */
#define SIN_STEP 5
static const int gc308_sin_table[] = {
	0,	 87,   173,   258,   342,   422,
	499,	573,   642,   707,   766,   819,
	866,	906,   939,   965,   984,   996,
	1000
};

static int gc308_sine(int theta)
{
	int chs = 1;
	int sine;

	if (theta < 0) {
		theta = -theta;
		chs = -1;
	}
	if (theta <= 90)
		sine = gc308_sin_table[theta/SIN_STEP];
	else {
		theta -= 90;
		sine = 1000 - gc308_sin_table[theta/SIN_STEP];
	}
	return sine*chs;
}

static int gc308_cosine(int theta)
{
	theta = 90 - theta;
	if (theta > 180)
		theta -= 360;
	else if (theta < -180)
		theta += 360;
	return gc308_sine(theta);
}




static void gc308_calc_cmatrix(struct gc308_info *info,
		int matrix[CMATRIX_LEN])
{
	int i;
	/*
	 * Apply the current saturation setting first.
	 */
	for (i = 0; i < CMATRIX_LEN; i++)
		matrix[i] = (info->fmt->cmatrix[i]*info->sat) >> 7;
	/*
	 * Then, if need be, rotate the hue value.
	 */
	if (info->hue != 0) {
		int sinth, costh, tmpmatrix[CMATRIX_LEN];

		memcpy(tmpmatrix, matrix, CMATRIX_LEN*sizeof(int));
		sinth = gc308_sine(info->hue);
		costh = gc308_cosine(info->hue);

		matrix[0] = (matrix[3]*sinth + matrix[0]*costh)/1000;
		matrix[1] = (matrix[4]*sinth + matrix[1]*costh)/1000;
		matrix[2] = (matrix[5]*sinth + matrix[2]*costh)/1000;
		matrix[3] = (matrix[3]*costh - matrix[0]*sinth)/1000;
		matrix[4] = (matrix[4]*costh - matrix[1]*sinth)/1000;
		matrix[5] = (matrix[5]*costh - matrix[2]*sinth)/1000;
	}
}



static int gc308_t_sat(struct i2c_client *client, int value)
{
	struct gc308_info *info = i2c_get_clientdata(client);
	int matrix[CMATRIX_LEN];
	int ret;

	info->sat = value;
	gc308_calc_cmatrix(info, matrix);
	ret = gc308_store_cmatrix(client, matrix);
	return ret;
}

static int gc308_q_sat(struct i2c_client *client, __s32 *value)
{
	struct gc308_info *info = i2c_get_clientdata(client);

	*value = info->sat;
	return 0;
}

static int gc308_t_hue(struct i2c_client *client, int value)
{
	struct gc308_info *info = i2c_get_clientdata(client);
	int matrix[CMATRIX_LEN];
	int ret;

	if (value < -180 || value > 180)
		return -EINVAL;
	info->hue = value;
	gc308_calc_cmatrix(info, matrix);
	ret = gc308_store_cmatrix(client, matrix);
	return ret;
}


static int gc308_q_hue(struct i2c_client *client, __s32 *value)
{
	struct gc308_info *info = i2c_get_clientdata(client);

	*value = info->hue;
	return 0;
}

#if 0
/*
 * Some weird registers seem to store values in a sign/magnitude format!
 */
static unsigned char gc308_sm_to_abs(unsigned char v)
{
	if ((v & 0x80) == 0)
		return v + 128;
	else
		return 128 - (v & 0x7f);
}


static unsigned char gc308_abs_to_sm(unsigned char v)
{
	if (v > 127)
		return v & 0x7f;
	else
		return (128 - v) | 0x80;
}
#endif

static int gc308_t_brightness(struct i2c_client *client, int value)
{
#if 0
	unsigned char com8, v;
	int ret;

	gc308_read(client, REG_COM8, &com8);
	com8 &= ~COM8_AEC;
	gc308_write(client, REG_COM8, com8);
	v = gc308_abs_to_sm(value);
	ret = gc308_write(client, REG_BRIGHT, v);
	return ret;
#endif
	return 0;
}

static int gc308_q_brightness(struct i2c_client *client, __s32 *value)
{
#if 0
	unsigned char v;
	int ret = gc308_read(client, REG_BRIGHT, &v);

	*value = gc308_sm_to_abs(v);
	return ret;
#endif
	return 0;
}

static int gc308_t_contrast(struct i2c_client *client, int value)
{
#if 0
	return gc308_write(client, REG_CONTRAS, (unsigned char) value);
#endif
	return 0;
}

static int gc308_q_contrast(struct i2c_client *client, __s32 *value)
{
#if 0
	unsigned char v;
	int ret = gc308_read(client, REG_CONTRAS, &v);

	*value = v;
	return ret;
#endif
		return 0;
}

static int gc308_q_hflip(struct i2c_client *client, __s32 *value)
{
#if 0
	int ret;
	unsigned char v;

	ret = gc308_read(client, REG_MVFP, &v);
	*value = (v & MVFP_MIRROR) == MVFP_MIRROR;
	return ret;
#endif
			return 0;
}


static int gc308_t_hflip(struct i2c_client *client, int value)
{
#if 0
	unsigned char v;
	int ret;

	ret = gc308_read(client, REG_MVFP, &v);
	if (value)
		v |= MVFP_MIRROR;
	else
		v &= ~MVFP_MIRROR;
	msleep(10);  /* FIXME */
	ret += gc308_write(client, REG_MVFP, v);
	return ret;
#endif
	return 0;
}



static int gc308_q_vflip(struct i2c_client *client, __s32 *value)
{
#if 0
	int ret;
	unsigned char v;

	ret = gc308_read(client, REG_MVFP, &v);
	*value = (v & MVFP_FLIP) == MVFP_FLIP;
	return ret;
#endif
		return 0;
}


static int gc308_t_vflip(struct i2c_client *client, int value)
{
#if 0
	unsigned char v;
	int ret;

	ret = gc308_read(client, REG_MVFP, &v);
	if (value)
		v |= MVFP_FLIP;
	else
		v &= ~MVFP_FLIP;
	msleep(10);  /* FIXME */
	ret += gc308_write(client, REG_MVFP, v);
	return ret;
#endif
	return 0;
}


static struct gc308_control {
	struct v4l2_queryctrl qc;
	int (*query)(struct i2c_client *c, __s32 *value);
	int (*tweak)(struct i2c_client *c, int value);
} gc308_controls[] =
{
	{
		.qc = {
			.id = V4L2_CID_BRIGHTNESS,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "Brightness",
			.minimum = 0,
			.maximum = 255,
			.step = 1,
			.default_value = 0x80,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = gc308_t_brightness,
		.query = gc308_q_brightness,
	},
	{
		.qc = {
			.id = V4L2_CID_CONTRAST,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "Contrast",
			.minimum = 0,
			.maximum = 127,
			.step = 1,
			.default_value = 0x40,   /* XXX gc308 spec */
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = gc308_t_contrast,
		.query = gc308_q_contrast,
	},
	{
		.qc = {
			.id = V4L2_CID_SATURATION,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "Saturation",
			.minimum = 0,
			.maximum = 256,
			.step = 1,
			.default_value = 0x80,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = gc308_t_sat,
		.query = gc308_q_sat,
	},
	{
		.qc = {
			.id = V4L2_CID_HUE,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "HUE",
			.minimum = -180,
			.maximum = 180,
			.step = 5,
			.default_value = 0,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = gc308_t_hue,
		.query = gc308_q_hue,
	},
	{
		.qc = {
			.id = V4L2_CID_VFLIP,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "Vertical flip",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = gc308_t_vflip,
		.query = gc308_q_vflip,
	},
	{
		.qc = {
			.id = V4L2_CID_HFLIP,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "Horizontal mirror",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = gc308_t_hflip,
		.query = gc308_q_hflip,
	},
};
#define N_CONTROLS (ARRAY_SIZE(gc308_controls))

static struct gc308_control *gc308_find_control(__u32 id)
{
	int i;

	for (i = 0; i < N_CONTROLS; i++)
		if (gc308_controls[i].qc.id == id)
			return gc308_controls + i;
	return NULL;
}

static int gc308_querycap(struct i2c_client *client,
		struct v4l2_capability *cap)
{
	strcpy(cap->driver, "gc0308");

	return 0;
}


static int gc308_queryctrl(struct i2c_client *client,
		struct v4l2_queryctrl *qc)
{
	struct gc308_control *ctrl = gc308_find_control(qc->id);

	if (ctrl == NULL)
		return -EINVAL;
	*qc = ctrl->qc;
	return 0;
}

static int gc308_g_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct gc308_control *octrl = gc308_find_control(ctrl->id);
	int ret;

	if (octrl == NULL)
		return -EINVAL;
	ret = octrl->query(client, &ctrl->value);
	if (ret >= 0)
		return 0;
	return ret;
}

static int gc308_s_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct gc308_control *octrl = gc308_find_control(ctrl->id);
	int ret;

	if (octrl == NULL)
		return -EINVAL;
	ret =  octrl->tweak(client, ctrl->value);
	if (ret >= 0)
		return 0;
	return ret;
}

int ccic_sensor_attach(struct i2c_client *client);

u16 last_reg = 0;
static ssize_t sensor_get_reg(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u8 val = 0;
	struct i2c_client* c = container_of(dev, struct i2c_client, dev);

	if (last_reg)
	{
		gc308_read(c, last_reg, &val);
		return sprintf(buf, "%x:%x\n", last_reg, val);
	}
	else
	{
		int len=0;
		int index;

		for (index=0; gc308_default_regs[index].reg_num!=0xff; index++)
		{
			gc308_read(c, gc308_default_regs[index].reg_num, &val);
			len += sprintf(&buf[len], "%02x:%02x%s\n", gc308_default_regs[index].reg_num, val, (val!=gc308_default_regs[index].value)?"*":"");
		}
		return len;
	}
}

static ssize_t sensor_set_reg(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned int reg ,val;
	int ret;
	struct i2c_client* c = container_of(dev, struct i2c_client, dev);

	ret = sscanf(buf, "%x:%x", &reg, &val);
	if (ret == 1)
	{
		last_reg = reg;
		return count;
	}
	else if (ret == 2)
	{
		gc308_write(c, reg, val);
		last_reg = reg;
		return count;
	}
	else
	{
		last_reg = 0;
		return count;
	}
}

static ssize_t sensor_get_id(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	int ret;

	ret = sprintf(buf, "gc308");
	
	return ret;
}

static struct device_attribute sensor_static_attrs[] = {
	__ATTR(reg, 0666, sensor_get_reg, sensor_set_reg),
	__ATTR(id, 0666, sensor_get_id, NULL),
};

/*
 * Basic i2c stuff.
 */
extern struct clk *pxa168_ccic_gate_clk;
extern void ccic_mclk_set(int on);
static int __devinit gc308_probe(struct i2c_client *client, const struct i2c_device_id * id)
{
	int i;
	int ret;
	struct gc308_info *info;
	struct sensor_platform_data *pdata;
	int fm_ctrl;

	pdata = client->dev.platform_data;

	/*
	 * Set up our info structure.
	 */
	info = kzalloc(sizeof (struct gc308_info), GFP_KERNEL);
	if (! info) {
		ret = -ENOMEM;
		goto out_free;
	}

	//set default format
	info->fmt = &gc308_formats[0];
	info->sat = 128;	/* Review this */
	i2c_set_clientdata(client, info);
	/*
	 * Make sure it's an gc308
	 */
	ccic_mclk_set(1);
	pdata->power_set(SENSOR_OPEN, pdata->id, pdata->eco_flag, pdata->sensor_flag);
	ret = gc308_detect(client);
	pdata->power_set(SENSOR_CLOSE, pdata->id, pdata->eco_flag,pdata->sensor_flag);
	ccic_mclk_set(0);
	if (ret)
		goto out_free_info;

	ret = ccic_sensor_attach(client);
	if (ret)
		goto out_free_info;

	if (pdata->id == SENSOR_LOW)
		fm_ctrl = fm_low;
	else
		fm_ctrl = fm_high;

	printk("gc308[%s] flip [%s] mirror [%s]\n", 
		dev_name(&client->dev), 
		(fm_ctrl&0x1)?"on":"off", 
		(fm_ctrl&0x2)?"on":"off");

	/* create sys entries */
	for (i=0; i<ARRAY_SIZE(sensor_static_attrs); i++)
	{
		ret = device_create_file(&client->dev, &sensor_static_attrs[i]);
		if(ret)
			goto out_free_info;
	}

	return 0;

out_free_info:
	kfree(info);
out_free:
	return ret;
}


static int gc308_remove(struct i2c_client *client)
{
	int i;
	
	for (i=0; i<ARRAY_SIZE(sensor_static_attrs); i++)
		device_remove_file(&client->dev, &sensor_static_attrs[i]);

	return 0;
}


static int gc308_streamon(struct i2c_client *client)
{
#if 0
	unsigned char val;
	gc308_read(client, 0x3086, &val);
	val &= ~0x03;
	gc308_write(client, 0x3086, val);
#endif
	return 0;
}

static int gc308_streamoff(struct i2c_client *client)
{
#if 0
	unsigned char val;
	gc308_read(client, 0x3086, &val);
	val |= 0x03;
	gc308_write(client, 0x3086, val);
#endif
	return 0;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int gc308_g_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return gc308_read(client, (u16)reg->reg, (unsigned char *)&(reg->val));
}

static int gc308_s_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return gc308_write(client, (u16)reg->reg, (unsigned char)reg->val);
}
#endif
static int gc308_command(struct i2c_client *client, unsigned int cmd,
		void *arg)
{
	//printk("%s, cmd: %d\n", __func__, _IOC_NR(cmd));

	switch (cmd) {
		case VIDIOC_DBG_G_CHIP_IDENT:
			return v4l2_chip_ident_i2c_client(client, arg, V4L2_IDENT_GC308, 0);

		case VIDIOC_INT_RESET:
			gc308_reset(client);
			return 0;

		/*
		case VIDIOC_INT_INIT:
			gc308_init(client);
			return 0;
		*/
		case VIDIOC_ENUM_FRAMESIZES:
			return gc308_enum_fmsize(client, (struct v4l2_frmsizeenum *) arg);
		case VIDIOC_ENUM_FMT:
			return gc308_enum_fmt(client, (struct v4l2_fmtdesc *) arg);
		case VIDIOC_TRY_FMT:
			return gc308_try_fmt(client, (struct v4l2_format *) arg, NULL, NULL);
		case VIDIOC_S_FMT:
			return gc308_s_fmt(client, (struct v4l2_format *) arg);
		case VIDIOC_G_FMT:
			return gc308_g_fmt(client, (struct v4l2_format *) arg);
		case VIDIOC_QUERYCAP:
			return gc308_querycap(client, (struct v4l2_capability *) arg);
		case VIDIOC_QUERYCTRL:
			return gc308_queryctrl(client, (struct v4l2_queryctrl *) arg);
		case VIDIOC_S_CTRL:
			return gc308_s_ctrl(client, (struct v4l2_control *) arg);
		case VIDIOC_G_CTRL:
			return gc308_g_ctrl(client, (struct v4l2_control *) arg);
		case VIDIOC_S_PARM:
			//return 0;
			return gc308_s_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_G_PARM:
			return gc308_g_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_S_INPUT:
			return gc308_s_input(client, (int *) arg);
		case VIDIOC_STREAMON:
			return gc308_streamon(client);
		case VIDIOC_STREAMOFF:
			return gc308_streamoff(client);
#ifdef CONFIG_VIDEO_ADV_DEBUG
		case VIDIOC_DBG_G_REGISTER:
			return gc308_g_register(client, (struct v4l2_dbg_register *) arg);
		case VIDIOC_DBG_S_REGISTER:
			return gc308_s_register(client, (struct v4l2_dbg_register *) arg);
#endif
	}
	return -EINVAL;
}

static struct i2c_device_id gc308_idtable[] = {
	{ "gc308", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, gc308_idtable);

static struct i2c_driver gc308_driver = {
	.driver = {
		.name	= "gc308",
	},
	.id_table   = gc308_idtable,
	.command	= gc308_command,
	.probe		= gc308_probe,
	.remove		= __devexit_p(gc308_remove),
};


/*
 * Module initialization
 */
static int __init gc308_mod_init(void)
{
	printk(KERN_NOTICE "GalaxyCore GC308 sensor driver, at your service.\n");
	return i2c_add_driver(&gc308_driver);
}

static void __exit gc308_mod_exit(void)
{
	i2c_del_driver(&gc308_driver);
}

module_init(gc308_mod_init);
module_exit(gc308_mod_exit);

MODULE_AUTHOR("PENG GUANG <pengguang001@gmail.com>");
MODULE_DESCRIPTION("Aspen168 based driver for GalaxyCore GC308 sensor");
MODULE_LICENSE("GPL");

