#ifndef _GC308_H_
#define _GC308_H_
                                                    
/* General definition for ov2650 */                  
#define OV2650_OUTWND_MAX_H            UXGA_SIZE_H   
#define OV2650_OUTWND_MAX_V            UXGA_SIZE_V  

#define GC308_SYS              0xfe
#define SYS_RESET              0x80 
#define CMATRIX_LEN            6 
#define GC308_PID              0x00
#define DEFAULT_PID            0x9b

#endif
