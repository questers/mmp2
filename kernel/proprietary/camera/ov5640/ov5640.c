/*
 * OmniVision ov5640 sensor driver
 *
 * Copyright (C) 20010-2011 Questers Co. Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/videodev.h>
#include <linux/i2c.h>
#include <linux/clk.h>
#include <media/v4l2-common.h>
#include <media/v4l2-chip-ident.h>
#include <mach/hardware.h>
#include <mach/camera.h>
#include <mach/mfp.h>
#include <mach/mfp-mmp2.h>
#include <mach/mmp2_plat_ver.h>

#include "pxa688_camera.h"
#include "ov5640.h"

MODULE_AUTHOR("Zhangfei Gao <zgao6@marvell.com>");
MODULE_DESCRIPTION("OmniVision ov5640 sensor driver");
MODULE_LICENSE("GPL");

/* The maximum allowed times for repeating OV5640 detection */
#define MAX_DETECT_NUM	3

/* Non-CameraEngine Usage */
//#define NON_CAMERA_ENGINE

/* Debug OV5640 register Read/Write */
//#define OV5640_DEBUG

/*
 * flip and mirror control
 * 0: normal
 * 1: mirror on
 * 2: flip on
 * 3: flip and mirror on
 */
static int fm_low = 0;
module_param(fm_low, int, 0);
static int fm_high = 0;
module_param(fm_high, int, 0);

static int div_expo = 6;
static int div_gain = 1;

#define setbit(reg, mask) \
do{ \
	unsigned char val; \
	ov5640_read(client, reg, &val); \
	/* printk(KERN_NOTICE"Reg0x%4X: 0x%2X -> 0x%2X\n", reg, val, (val)|(mask)); */ \
	val |= (mask); \
	ov5640_write(client, reg, val); \
}while (0)

#define clrbit(reg, mask) \
do{ \
	unsigned char val; \
	ov5640_read(client, reg, &val);	\
	/* printk(KERN_NOTICE"Reg0x%4X: 0x%2X -> 0x%2X\n", reg, val, (val)&(~(mask))); */ \
	val &= (~(mask)); \
	ov5640_write(client, reg, val);	\
}while (0)

/*
 * Information we maintain about a known sensor.
 */
enum flash_arg_format {
	FLASH_ARG_TIME_OFF	= 0,
	FLASH_ARG_MODE_0	= 0,
	FLASH_ARG_MODE_1,
	FLASH_ARG_MODE_2,
	FLASH_ARG_MODE_3,
	FLASH_ARG_MODE_EVER	= FLASH_ARG_MODE_0, /* Always on till send off  */
	FLASH_ARG_MODE_TIMED	= FLASH_ARG_MODE_1, /* On for some time */
	FLASH_ARG_MODE_REPEAT	= FLASH_ARG_MODE_2, /* Repeatedlly on */
	FLASH_ARG_MODE_BLINK	= FLASH_ARG_MODE_3, /* On for some jeffies defined by H/W */
	FLASH_ARG_MODE_MAX,	/* Equal or greater mode considered invalid */
	FLASH_ARG_LUMI_OFF	= 0, /* Flash off */
	FLASH_ARG_LUMI_FULL,
	FLASH_ARG_LUMI_DIM1,	/* One fifth luminance*/
	FLASH_ARG_LUMI_DIM2,	/* One third luminance, not supported in H/W configuration */
	FLASH_ARG_LUMI_MAX,	/* Equal or greater luminance considered invalid */
};

struct cam_flash_struct {
	unsigned char	mode;
/*	flash mode
 *	00 Continous flash, flash on when on req flash off when off req.
 *	01 One flash, the flash strobe is opened for a period of time specified
 *	   by high 3 bytes.
 *	10 periodic flash, the flash is on periodiclly the period is specified
 *	   by high 3 bytes.
 *	11 pulse flash, flash one time for a very short pulse, width of the
 *	   pulse is specified by bit
 */
	unsigned char	brightness;
	unsigned int 	duration;
};

struct ov5640_format_struct;  /* coming later */

struct ov5640_info {
	struct ov5640_format_struct *fmt;  /* Current format */
	unsigned char sat;		/* Saturation value */
	int hue;			/* Hue value */
	struct cam_flash_struct flash;	/* Flash strobe related info */
};

/*
 * Low-level register I/O.
 */
/* issue that OV sensor must write then read. 5640 register is 16bit!!! */
static int ov5640_read(struct i2c_client *client, u16 reg, unsigned char *value)
{
	int ret = 0;
	u8 data;
	u8 address[2];
	address[0] = reg>>8;
	address[1] = reg;
	ret = i2c_smbus_write_byte_data(client, address[0], address[1]);
	if (ret)
		return ret;
	data = i2c_smbus_read_byte(client);
	*value = data;
	return 0;
}

static int ov5640_write(struct i2c_client *client, u16 reg, unsigned char value)
{
#ifdef OV5640_DEBUG
	unsigned char verify = 0;
#endif
	int ret = 0;
	u8 data[3];
	data[0] = reg>>8;
	data[1] = reg;
	data[2]=  value;
	ret = i2c_master_send(client, data, 3);
	if (ret < 0)
		return ret;
	if (reg == REG_SYSCTRL && (value & SW_RESET)) {
		printk("camera: a s/w reset triggered, may result in register value lost!\n");
		msleep(2); /* Wait for reset to run, OV spec suggest 2 ~ 5 ms */
	}
#ifdef OV5640_DEBUG
	/* verify write regs for debugging */
	ret = ov5640_read(client, reg, &verify);
	if (ret)
		return ret;
	if (verify != value && reg != 0x3008 && reg != 0x5316)
		printk(KERN_ERR "ov5640 register 0x%x: write = 0x%x, but read = 0x%x, return value = %d\n", reg, value, verify, ret);
#endif
	return 0;
}

static int sensor_sequential_write_reg(struct i2c_client *client, unsigned char *data, u16 datasize)
{
	int err;
	struct i2c_msg msg;
	int retry = 3;

	if (datasize==0)
		return 0;
	if (!client->adapter)
		return -ENODEV;

	msg.addr = client->addr;
	msg.flags = 0;
	msg.len = datasize;
	msg.buf = data;
	do {
		err = i2c_transfer(client->adapter, &msg, 1);
		if (err == 1)
			return 0;
		retry++;
		//pr_err("yuv_sensor : i2c transfer failed, retrying %x %x\n",
		      // addr, val);
		pr_err("yuv_sensor : i2c transfer failed, count %x \n",
		       msg.addr);
//		msleep(3);
	} while (retry--);

	return err;
}

static int build_sequential_buffer(unsigned char *pBuf, u16 width, u16 value) {
	u32 count = 0;

	switch (width)
	{
	  case 0:
	  // possibly no address, some focusers use this
	  break;

	  // cascading switch
	  case 32:
	    pBuf[count++] = (u8)((value>>24) & 0xFF);
	  case 24:
	    pBuf[count++] = (u8)((value>>16) & 0xFF);
	  case 16:
	    pBuf[count++] = (u8)((value>>8) & 0xFF);
	  case 8:
	    pBuf[count++] = (u8)(value & 0xFF);
	    break;

	  default:
	    printk("Unsupported Bit Width %d\n", width);
	    break;
	}
	return count;

}

/*
 * Write a list of register settings; ff/ff stops the process.
 */
static int ov5640_write_array(struct i2c_client *client, struct regval_list *vals)
{
	int datasize = 0;
	u8 data[128];

	while (vals->reg_num != 0xffff || vals->value != 0xff) {
		if (vals->reg_num == 0xdddd)
		{
			mdelay(vals->value);
		}
		else if (vals->reg_num == 0xfffd) {	/* seq write start */
			vals += 1;
			while (vals->reg_num !=0xfffe && vals->reg_num !=0xffff) {	/* seq write end */
				if (datasize==0) {//
					datasize += build_sequential_buffer(&data[datasize], 16, vals->reg_num);
					datasize += build_sequential_buffer(&data[datasize], 8, vals->value);
				}
				else
					datasize += build_sequential_buffer(&data[datasize], 8, vals->value);
				if (datasize==128) {
					sensor_sequential_write_reg(client, data, datasize);
					printk("seq write: %d bytes written.\n", datasize);
					datasize = 0;
				}
				vals += 1;
			}
			sensor_sequential_write_reg(client, data, datasize); //flush out the remaining buffer.
			printk("seq write: %d bytes written.\n", datasize);
			datasize = 0;
		}
		else
		{
			int ret = ov5640_write(client, vals->reg_num, vals->value);
			if (ret < 0)
				return ret;
		}
		vals++;
	}

	return 0;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int ov5640_g_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return ov5640_read(client, (u16)reg->reg, (unsigned char *)&(reg->val));
}

static int ov5640_s_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return ov5640_write(client, (u16)reg->reg, (unsigned char)reg->val);
}
#endif

static int ov5640_detect(struct i2c_client *client)
{
	u8 h = 0, l = 0;
	int ret = 0;

	/*
	 * no MID register found. OK, we know we have an OmniVision chip...but which one?
	 */
	ret = ov5640_read(client, REG_PIDH, &h);
	if (ret < 0 || h != 0x56)
	{
		return -ENXIO;
	}

	ret = ov5640_read(client, REG_PIDL, &l);
	if (ret < 0 || l != 0x40)
	{
		return -ENXIO;
	}

	return 0;
}

/*
 * Store information about the video data format.  The color matrix
 * is deeply tied into the format, so keep the relevant values here.
 * The magic matrix nubmers come from OmniVision.
 */
static struct ov5640_format_struct {
	__u8 *desc;
	__u32 pixelformat;
	struct regval_list *regs;
	int bpp;   /* bits per pixel */
} ov5640_formats[] = {
	{
		.desc		= "YUYV422 packet",
		.pixelformat	= V4L2_PIX_FMT_UYVY,
		.bpp		= 16,
	},
	{
		.desc		= "YUYV422 planar",
		.pixelformat	= V4L2_PIX_FMT_YUV422P,
		.bpp		= 16,
	},
	{
		.desc		= "YUYV 4:2:0",
		.pixelformat	= V4L2_PIX_FMT_YUV420,
		.bpp		= 12,
	},
	/*
	{
		.desc		= "JFIF JPEG",
		.pixelformat	= V4L2_PIX_FMT_JPEG,
		.regs		= ov5640_fmt_jpg,
		.bpp		= 16,
	},*/
#ifdef NON_CAMERA_ENGINE
	{
		.desc		= "RGB 565",
		.pixelformat	= V4L2_PIX_FMT_RGB565,
		.regs		= ov5640_fmt_rgb565,
		.bpp		= 16,
	},
#endif
};

#define N_OV5640_FMTS ARRAY_SIZE(ov5640_formats)

/*
 * Then there is the issue of window sizes.  Try to capture the info here.
 */
struct ov5640_win_size {
	int	width;
	int	height;
	int	fps;
	struct regval_list *regs;
};

/* Preview Size */
static struct ov5640_win_size ov5640_win_sizes[] = {
#if 0
	/* 176x144 */
	{
		.width		= 176,
		.height		= 144,
		.fps		= 30,
		.regs		= reg_176x144,
	},
	/* 320x240 */
	{
		.width		= 320,
		.height		= 240,
		.fps		= 30,
		.regs		= reg_320x240,
	},
#endif
	/* 352x288 */
	{
		.width		= 352,
		.height		= 288,
		.fps		= 30,
		.regs		= reg_352x288,
	},
	/* 640x480 */
	{
		.width		= 640,
		.height		= 480,
		.fps		= 30,
		.regs		= reg_640x480,
	},
#if 0
	/* 1024x768 */
	{
		.width		= 1024,
		.height 	= 768,
		.fps		= 30,
		.regs		= reg_1024x768,
	},
	/* 1600x1200 */
	{
		.width		= 1600,
		.height 	= 1200,
		.fps		= 30,
		.regs		= reg_1600x1200,
	},
	/* 3M */
	{
		.width		= 2048,
		.height		= 1536,
		.fps		= 30,
		.regs		= reg_2048x1536,
	},
#endif
	/* 5M */
	{
		.width		= 2592,
		.height 	= 1944,
		.fps		= 30,
		.regs		= reg_2592x1944,
	},
#if 0
	/* 720x480 */
	{
		.width		= 720,
		.height 	= 480,
		.fps		= 30,
		.regs		= reg_720x480,
	},
	/* 1280x720 */
	{
		.width		= 1280,
		.height 	= 720,
		.fps		= 30,
		.regs		= reg_1280x720,
	},
	/* 1920x1080 */
	{
		.width		= 1920,
		.height 	= 1080,
		.fps		= 30,
		.regs		= reg_1920x1080,
	},
#endif
};

#define N_WIN_SIZES (ARRAY_SIZE(ov5640_win_sizes))

/* Capture JPEG Size */
static struct ov5640_win_size ov5640_win_jpg_sizes[] = {
};

#define N_WIN_JPEG_SIZES (ARRAY_SIZE(ov5640_win_jpg_sizes))

/*
 * Save Camera Sensor AWB RGB GAIN Values
 */
struct ov5640_awb_gain {
	__u8 *desc;
	unsigned char high;
	unsigned char low;
} ov5640_awb_gains[] = {
	{
		.desc		= "AWB Red Gain",
		.high		= 0,
		.low		= 0,
	},
	{
		.desc		= "AWB Green Gain",
		.high		= 0,
		.low		= 0,
	},
	{
		.desc		= "AWB Blue Gain",
		.high		= 0,
		.low		= 0,
	},
};

static int ov5640_q_awb(struct i2c_client *client, __s32 *value)
{
	int ret;
	unsigned char v = 0;

	ret = ov5640_read(client, REG_AWB_EN, &v);
	*value = ((v & AWB_DIS) != AWB_DIS);
	return ret;
}

static int ov5640_t_awb(struct i2c_client *client, int value)
{
	unsigned char v = 0;
	int ret;

	ret = ov5640_read(client, REG_AWB_EN, &v);
	if (!value)
		v = AWB_DIS;
	else
		v = 0x00;
	ret = ov5640_write(client, REG_AWB_EN, v);
	return ret;
}

static int ov5640_q_ae(struct i2c_client *client, __s32 *value)
{
	int ret;
	unsigned char v = 0;

	ret = ov5640_read(client, REG_AE_EN, &v);
	*value = ((v & AE_EN) == AE_EN);
	return ret;
}

static int ov5640_t_ae(struct i2c_client *client, int value)
{
	unsigned char v = 0;
	int ret;

	ret = ov5640_read(client, REG_AE_EN, &v);
	if (value)
		v |= AE_EN;
	else
		v &= ~AE_EN;
	ret += ov5640_write(client, REG_AE_EN, v);
	return ret;
}

static int ov5640_q_af(struct i2c_client *client, __s32 *value)
{
	int ret;
	unsigned char v = 0;

	ret = ov5640_read(client, REG_AF_EN, &v);
	*value = (v & AF_EN) == AF_EN;
	return ret;
}

static int ov5640_t_af(struct i2c_client *client, int value)
{
	unsigned char v = 0;
	int ret;

	ret = ov5640_read(client, REG_AF_EN, &v);
	if (value)
		v |= AF_EN;
	else
		v &= ~AF_EN;
	ret += ov5640_write(client, REG_AF_EN, v);
	return ret;
}

/* Flash related functions */
static int ov5640_q_flash_time(struct i2c_client *client, __s32 *time);
static int ov5640_t_flash_time(struct i2c_client *client, int time);
static int ov5640_q_flash_mode(struct i2c_client *client, __s32 *mode);
static int ov5640_t_flash_mode(struct i2c_client *client, int mode);
static int ov5640_q_flash_lum(struct i2c_client *client, __s32 *lum);
static int ov5640_t_flash_lum(struct i2c_client *client, int lum);

static int ov5640_q_flash_time(struct i2c_client *client, __s32 *time)
{
	struct ov5640_info *info = i2c_get_clientdata(client);
	*time = info->flash.duration;
	return *time;
}

static int ov5640_t_flash_time(struct i2c_client *client, int time)
{
	int ret = 0;
	struct ov5640_info *info = i2c_get_clientdata(client);
	if (time < 0) {
		printk(KERN_NOTICE "camera: ov5640 invalid flash duration: %d\n", time);
		return -EINVAL;
	}
	info->flash.duration = time;
	if (time == 0)
		info->flash.mode = FLASH_ARG_MODE_EVER;
	else
		info->flash.mode = FLASH_ARG_MODE_TIMED;
	/* If the flash is on, the duration change will affect the
	 * behavoir of flash immediatelly */
	if (info->flash.brightness != FLASH_ARG_LUMI_OFF)
		ret = ov5640_t_flash_lum(client, info->flash.brightness);
	return ret;
}

static int ov5640_q_flash_mode(struct i2c_client *client, __s32 *mode)
{
	struct ov5640_info *info = i2c_get_clientdata(client);
	*mode = info->flash.mode;
	return *mode;
}

static int ov5640_t_flash_mode(struct i2c_client *client, int mode)
{
	struct ov5640_info *info = i2c_get_clientdata(client);

	if (mode >= FLASH_ARG_MODE_MAX)
		return -EINVAL;
	info->flash.mode = mode;
	return 0;
}

static int ov5640_q_flash_lum(struct i2c_client *client, __s32 *lum)
{
	struct ov5640_info *info = i2c_get_clientdata(client);
	*lum = info->flash.brightness;
	return *lum;
}

static int ov5640_t_flash_lum(struct i2c_client *client, int lum)
{
	struct ov5640_info *info = i2c_get_clientdata(client);
	int ret = 0;

	if ((lum < 0) || (lum > FLASH_ARG_LUMI_MAX))
		return -EINVAL;

	if (lum == 0) {
		printk(KERN_DEBUG "Turn OFF flash\n");
		info->flash.brightness = FLASH_ARG_LUMI_OFF;
		/* Request turn OFF the flash, so ignore flash mode */
		clrbit(REG_SYS_RESET, 0x08);	// clear reset
		setbit(REG_CLK_ENABLE, 0x08);	// enable clock
		setbit(REG_PAD_OUTPUT, 0x20);	// pad output enable
		setbit(REG_FREX_MODE, 0x02);	// FREX mode
		/* Strobe control */
		ret = ov5640_write(client, REG_STRB_CTRL, 0x03);
		/* Write again to make sure strobe request end is received
		 * after other bits are saved in the reg, use LED3 mode off */
		ret |= ov5640_write(client, REG_STRB_CTRL, 0x03);
		return ret;
	}

	info->flash.brightness = lum;
	clrbit(REG_SYS_RESET, 0x08);	// clear reset
	setbit(REG_CLK_ENABLE, 0x08);	// enable clock
	setbit(REG_PAD_OUTPUT, 0x02);	// pad output enable
	setbit(REG_FREX_MODE, 0x02);	// FREX mode

	switch (info->flash.mode)
	{
		case FLASH_ARG_MODE_TIMED:
			if (info->flash.duration) {
				/* Strobe control */
				ret = ov5640_write(client, REG_STRB_CTRL, 0x03);
				/* Send strobe request */
				ret |= ov5640_write(client, REG_STRB_CTRL, 0x83);
				msleep(info->flash.duration);
				ret |= ov5640_write(client, REG_STRB_CTRL, 0x03);
				info->flash.brightness = FLASH_ARG_LUMI_OFF;
				return ret;
			}
			/* NO break here, if duration is 0, leave it ON */
		case FLASH_ARG_MODE_EVER:
			/* Strobe control */
			ret = ov5640_write(client, REG_STRB_CTRL, 0x03);
			/* Send strobe request */
			ret |= ov5640_write(client, REG_STRB_CTRL, 0x83);
			return ret;
		case FLASH_ARG_MODE_REPEAT:
			/* Strobe control */
			ret = ov5640_write(client, REG_STRB_CTRL, 0x02);
			/* Send strobe request */
			ret |= ov5640_write(client, REG_STRB_CTRL, 0x82);
			return ret;
		case FLASH_ARG_MODE_BLINK:
			/* Use ov5640 xenon flash mode */
			return ret;
		default:
			break;
	}
	return 0;
}

static int ov5640_firmware_download(struct i2c_client *client, int value)
{
	/* skip */
	return 0;
	
	printk(KERN_ERR "Write af fw start...\n");
	ov5640_write_array(client, reg_af_fw);
	printk(KERN_ERR "Write af fw end\n");
	return 0;
}

static int ov5640_fw_down(struct i2c_client *client)
{
	printk(KERN_ERR "Write af fw start...\n");
	ov5640_write_array(client, reg_af_fw);
	printk(KERN_ERR "Write af fw end\n");
	return 0;
}


static struct ov5640_control {
	struct v4l2_queryctrl qc;
	int (*query)(struct i2c_client *client, __s32 *value);
	int (*tweak)(struct i2c_client *client, int value);
} ov5640_controls[] =
{
	{
		.qc = {
			.id = V4L2_CID_FOCUS_AUTO,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "auto focus",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = ov5640_t_af,
		.query = ov5640_q_af,
	},
	{
		.qc = {
			.id = V4L2_CID_AUTO_WHITE_BALANCE,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "auto white balance",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = ov5640_t_awb,
		.query = ov5640_q_awb,
	},
	{
		.qc = {
			.id = V4L2_CID_EXPOSURE_AUTO,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "auto exposure",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = ov5640_t_ae,
		.query = ov5640_q_ae,
	},
	{
		.qc = {
			.id = V4L2_CID_FLASH_DURATION,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "flash duration",
			.minimum = 0,
			.maximum = 60000,
			.step = 1,
			.default_value = 1000,
		},
		.tweak = ov5640_t_flash_time,
		.query = ov5640_q_flash_time,
	},
		{
		.qc = {
			.id = V4L2_CID_FLASH_MODE,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "flash mode",
			.minimum = FLASH_ARG_MODE_0,
			.maximum = FLASH_ARG_MODE_MAX - 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = ov5640_t_flash_mode,
		.query = ov5640_q_flash_mode,
	},
	{
		.qc = {
			.id = V4L2_CID_FLASH_LUMINANCE,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "flash brightness",
			.minimum = FLASH_ARG_LUMI_OFF,
			.maximum = FLASH_ARG_LUMI_MAX - 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = ov5640_t_flash_lum,
		.query = ov5640_q_flash_lum,
	},
	{
		.qc = {
			.id = V4L2_CID_AF_FIRMWARE_DOWNLOAD,
		},
		.tweak = ov5640_firmware_download,
	},
};

#define N_CONTROLS (ARRAY_SIZE(ov5640_controls))

static int ov5640_querycap(struct i2c_client *client, struct v4l2_capability *argp)
{
	if (!argp) {
		printk(KERN_ERR " argp is NULL %s %d \n", __FUNCTION__, __LINE__);
		return -EINVAL;
	}
	strcpy(argp->driver, "ov5640");
	strcpy(argp->card, "MMP2");
	return 0;
}

static int ov5640_enum_fmt(struct i2c_client *client, struct v4l2_fmtdesc *fmt)
{
	struct ov5640_format_struct *ofmt;

	if (fmt->index >= N_OV5640_FMTS)
		return -EINVAL;

	ofmt = ov5640_formats + fmt->index;
	fmt->flags = 0;
	strcpy(fmt->description, ofmt->desc);
	fmt->pixelformat = ofmt->pixelformat;
	return 0;
}

static int ov5640_try_fmt(struct i2c_client *client, struct v4l2_format *fmt,
		struct ov5640_format_struct **ret_fmt,
		struct ov5640_win_size **ret_wsize)
{
	int index_fmt, index_wsize;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	for (index_fmt = 0; index_fmt < N_OV5640_FMTS; index_fmt++)
		if (ov5640_formats[index_fmt].pixelformat == pix->pixelformat)
			break;
	if (index_fmt >= N_OV5640_FMTS) {
		printk(KERN_ERR "camera: ov5640 try unsupported format!\n");
		return -EINVAL;
	}
	if (ret_fmt != NULL)
		*ret_fmt = &ov5640_formats[index_fmt];

	if (pix->field == V4L2_FIELD_ANY)
		pix->field = V4L2_FIELD_NONE;
	else if (pix->field != V4L2_FIELD_NONE) {
		printk(KERN_ERR "camera: ov5640 pixel filed should be V4l2_FIELD_NONE!\n");
		return -EINVAL;
	}

	/*
	 * Round requested image size down to the nearest
	 * we support, but not below the smallest.
	 */
	if (V4L2_PIX_FMT_JPEG == pix->pixelformat) {
		for (index_wsize = 0; index_wsize < N_WIN_JPEG_SIZES; index_wsize++)
			if (pix->width == ov5640_win_jpg_sizes[index_wsize].width
				&& pix->height == ov5640_win_jpg_sizes[index_wsize].height)
				break;
		if (index_wsize >= N_WIN_JPEG_SIZES) {
			printk(KERN_ERR"camera: ov5640 try unsupported jpeg format size!\n");
			return -EINVAL;
		}
		if (ret_wsize != NULL)
			*ret_wsize = &ov5640_win_jpg_sizes[index_wsize];
		/* for OV5640, HSYNC contains 2048bytes */
		pix->bytesperline = 2048;
	} else {
		for (index_wsize = 0; index_wsize < N_WIN_SIZES; index_wsize++)
			if (pix->width == ov5640_win_sizes[index_wsize].width
				&& pix->height == ov5640_win_sizes[index_wsize].height)
				break;
		if (index_wsize >= N_WIN_SIZES) {
			printk(KERN_ERR "camera: ov5640 try unsupported preview format size!\n");
			return -EINVAL;
		}
		if (ret_wsize != NULL)
			*ret_wsize = &ov5640_win_sizes[index_wsize];
		pix->bytesperline = pix->width*ov5640_formats[index_fmt].bpp/8;
		pix->sizeimage = pix->height*pix->bytesperline;
	}
	return 0;
}

static int ov5640_enum_fmsize(struct i2c_client *client, struct v4l2_frmsizeenum *argp)
{
	struct v4l2_frmsizeenum frmsize;

	if (copy_from_user(&frmsize, argp, sizeof(frmsize)))
		return -EFAULT;

	switch (frmsize.pixel_format) {
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420:
			if (frmsize.index == N_WIN_SIZES)
				return -EINVAL;
			else if (frmsize.index > N_WIN_SIZES) {
				printk(KERN_ERR "camera: ov5640 enum unsupported preview format size!\n");
				return -EINVAL;
			} else {
				frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
				frmsize.discrete.height = ov5640_win_sizes[frmsize.index].height;
				frmsize.discrete.width = ov5640_win_sizes[frmsize.index].width;
				break;
			}
		case V4L2_PIX_FMT_JPEG:
			if (frmsize.index == N_WIN_JPEG_SIZES)
				return -EINVAL;
			else if (frmsize.index > N_WIN_JPEG_SIZES) {
				printk(KERN_ERR "camera: ov5640 enum unsupported jpeg format size!\n");
				return -EINVAL;
			} else {
				frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
				frmsize.discrete.height = ov5640_win_jpg_sizes[frmsize.index].height;
				frmsize.discrete.width = ov5640_win_jpg_sizes[frmsize.index].width;
				break;
			}
		default:
			printk(KERN_ERR "camera: ov5640 enum format size with unsupported format!\n");
			return -EINVAL;
	}

	if (copy_to_user(argp, &frmsize, sizeof(frmsize)))
		return -EFAULT;
	return 0;
}

static int ov5640_enum_fminterval(struct i2c_client *client, struct v4l2_frmivalenum *argp)
{
	struct v4l2_frmivalenum frminterval;

	if (copy_from_user(&frminterval, argp, sizeof(frminterval)))
		return -EFAULT;

	switch (frminterval.pixel_format) {
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420:
			if (frminterval.index == N_WIN_SIZES)
				return -EINVAL;
			else if (frminterval.index > N_WIN_SIZES) {
				printk(KERN_ERR "camera: ov5640 enum unsupported preview format frame rate!\n");
				return -EINVAL;
			} else {
				frminterval.type = V4L2_FRMIVAL_TYPE_DISCRETE;
				frminterval.discrete.numerator = 1;
				frminterval.discrete.denominator = ov5640_win_sizes[frminterval.index].fps;
				break;
			}
		case V4L2_PIX_FMT_JPEG:
			if (frminterval.index == N_WIN_JPEG_SIZES)
				return -EINVAL;
			else if (frminterval.index > N_WIN_JPEG_SIZES) {
				printk(KERN_ERR "camera: ov5640 enum unsupported jpeg format frame rate!\n");
				return -EINVAL;
			} else {
				frminterval.type = V4L2_FRMIVAL_TYPE_DISCRETE;
				frminterval.discrete.numerator = 1;
				frminterval.discrete.denominator = ov5640_win_jpg_sizes[frminterval.index].fps;
				break;
			}
		default:
			printk(KERN_ERR "camera: ov5640 enum format frame rate with unsupported format!\n");
			return -EINVAL;
	}

	if (copy_to_user(argp, &frminterval, sizeof(frminterval)))
		return -EFAULT;
	return 0;
}

/*
 * Reset sensor for initializing settings
 * this function is supposed to be called only at beginning
 * call this function will result in registers value lost
 */
static int ov5640_int_reset(struct i2c_client *client)
{
	/* Initialize settings */
	ov5640_write_array(client, reg_default);
	printk(KERN_ERR "Writting initial setting done.\n");
	
	return 0;
}

static struct ov5640_control *ov5640_find_control(__u32 id)
{
	int i;

	for (i = 0; i < N_CONTROLS; i++)
		if (ov5640_controls[i].qc.id == id)
			return ov5640_controls + i;
	return NULL;
}

static int ov5640_s_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct ov5640_control *octrl = ov5640_find_control(ctrl->id);
	int ret = 0;

	if (octrl == NULL)
		return -EINVAL;
	ret =  octrl->tweak(client, ctrl->value);
	if (ret >= 0)
		return 0;
	return ret;
}

static int ov5640_q_ctrl(struct i2c_client *client, struct v4l2_queryctrl *qc)
{
	struct ov5640_control *ctrl = ov5640_find_control(qc->id);

	if (ctrl == NULL)
		return -EINVAL;
	*qc = ctrl->qc;
	return 0;
}

static int ov5640_g_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct ov5640_control *octrl = ov5640_find_control(ctrl->id);
	int ret = 0;

	if (octrl == NULL)
		return -EINVAL;
	ret = octrl->query(client, &ctrl->value);
	if (ret >= 0)
		return 0;
	return ret;
}

/*
 * Set a format.
 */
static int ov5640_s_fmt(struct i2c_client *client, struct v4l2_format *fmt)
{
	u8 val;
	int ret = 0;
	struct ov5640_format_struct *ovfmt = NULL;
	struct ov5640_win_size *wsize = NULL;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	struct sensor_platform_data *pdata = client->dev.platform_data;
	int fm_ctrl;

	ret = ov5640_try_fmt(client, fmt, &ovfmt, &wsize);
	if (ret)
		return ret;

    switch (pix->pixelformat) {
        case V4L2_PIX_FMT_UYVY:
        case V4L2_PIX_FMT_YUV422P:
        case V4L2_PIX_FMT_YUV420:
            if (wsize->width < 1600 && wsize->height <1200)
            {
                ov5640_write_array(client, wsize->regs);
            }
            else
            {
#if 0
                int Lines_10ms;
                int Capture_MaxLines;
                int Preview_Maxlines;
                long ulCapture_Exposure;
                long ulCapture_Exposure_Gain;
                long ulPreviewExposure;
                long iCapture_Gain;
                u8 Gain ;
                u8 ExposureLow;
                u8 ExposureMid ;
                u8 ExposureHigh ;
                u8 reg_519f,reg_51a0,reg_51a1,reg_51a2,reg_51a3,reg_51a4;
                u32 Capture_Framerate = 300;
                u32 Preview_Framerate = 3000;

                ov5640_write(client, 0x3503, 0x07);
                ov5640_read(client, 0x350b, &Gain);
                ov5640_read(client, 0x3502, &ExposureLow);
                ov5640_read(client, 0x3501, &ExposureMid);
                ov5640_read(client, 0x3500, &ExposureHigh);
                Preview_Maxlines = 980;
                Capture_MaxLines = 1964;
                Lines_10ms = Capture_Framerate * Capture_MaxLines/10000*13/12;
                ulPreviewExposure = ((u32)(ExposureHigh))<<12 ;
                ulPreviewExposure += ((u32)ExposureMid)<<4 ;
                ulPreviewExposure += (ExposureLow >>4);
                if(0 == Preview_Maxlines ||0 ==Preview_Framerate ||0== Lines_10ms)
                {
                    printk("panic!\n");
                }

                ov5640_write_array(client, wsize->regs);

                ulCapture_Exposure =
                    ((ulPreviewExposure*(Capture_Framerate)*(Capture_MaxLines))/
                     (((Preview_Maxlines)*(Preview_Framerate))))*6/5; //6/5,控制预览和拍照亮度的一致性
                iCapture_Gain = Gain ;

                ulCapture_Exposure_Gain = ulCapture_Exposure * iCapture_Gain;//夜景模式*2

                if(ulCapture_Exposure_Gain < ((u32)(Capture_MaxLines)*16))
                {
                    ulCapture_Exposure = ulCapture_Exposure_Gain/16;
                    if (ulCapture_Exposure > Lines_10ms){
                        ulCapture_Exposure /= Lines_10ms;
                        ulCapture_Exposure *= Lines_10ms;
                    }
                }
                else
                {
                    ulCapture_Exposure = Capture_MaxLines;
                }
                if(ulCapture_Exposure == 0)
                {
                    ulCapture_Exposure = 1;
                }
                iCapture_Gain = (ulCapture_Exposure_Gain*2/ulCapture_Exposure + 1)/2;
                ExposureLow = ((u8)ulCapture_Exposure)<<4;
                ExposureMid = (u8)(ulCapture_Exposure >> 4) & 0xff;
                ExposureHigh = (u8)(ulCapture_Exposure >> 12);
                Gain =(u8) iCapture_Gain;
                ov5640_write(client, 0x350b, Gain);
                ov5640_write(client, 0x3502, ExposureLow);
                ov5640_write(client, 0x3501, ExposureMid);
                ov5640_write(client, 0x3500, ExposureHigh);
#else
                u16 exposure_00=0;
                u16 exposure_01=0;
                u16 exposure_02=0;
                u16 gain=0;
                u16 maxlines_0c=0;
                u16 maxlines_0d=0;
                u32 exposure=0;
                ov5640_write(client, 0x3503, 0x03);
                ov5640_write(client, 0x3406, 0x01);
                ov5640_read(client, 0x3500, &exposure_00);
                ov5640_read(client, 0x3501, &exposure_01);
                ov5640_read(client, 0x3502, &exposure_02);
                ov5640_read(client, 0x350b, &gain);
                ov5640_read(client, 0x350c, &maxlines_0c);
                ov5640_read(client, 0x350d, &maxlines_0d);
                exposure |= (exposure_00<<16) | (exposure_01 << 8) | exposure_02;
                printk("in preview, exposure: %08x, gain: %02x\n", exposure, gain);
                exposure /= div_expo;
                gain /= div_gain;
                ov5640_write_array(client, wsize->regs);;

                ov5640_write(client, 0x350b, gain&0xff);
                ov5640_write(client, 0x3500, (exposure >> 16)&0xFF);
                ov5640_write(client, 0x3501, (exposure >> 8)&0xFF);
                ov5640_write(client, 0x3502, exposure & 0xFF);
                //msleep(200);
                //msleep(350);
                msleep(100);
#endif
            }
            break;
        default:
            printk("camera: ov5640 set unsupported pixel format!\n");
            ret = -EINVAL;
            break;
    }

	/* flip and mirror */
	if (pdata->id == SENSOR_LOW)
		fm_ctrl = fm_low;
	else
		fm_ctrl = fm_high;
	
	/* flip */
	ov5640_read(client, 0x3820, &val);
	if (fm_ctrl & 0x2) 
		val |= 0x6;
	else 
		val &= ~0x6;
	ov5640_write(client, 0x3820, val);

	/* mirror */
	ov5640_read(client, 0x3821, &val);
	if (fm_ctrl & 0x1) 
		val |= 0x6;
	else 
		val &= ~0x6;
	ov5640_write(client, 0x3821, val);

	printk(KERN_ERR "Writting resolution [%dx%d] done\n", wsize->width, wsize->height);
	if (wsize->width == 640) ov5640_fw_down(client);
	
	return ret;
}

/*
 * Implement G/S_PARM.  There is a "high quality" mode we could try
 * to do someday; for now, we just do the frame rate tweak.
 */
static int ov5640_g_parm(struct i2c_client *client, struct v4l2_streamparm *parms)
{
	return 0;
}

static int ov5640_s_parm(struct i2c_client *client, struct v4l2_streamparm *parms)
{
	return 0;
}

static int ov5640_s_input(struct i2c_client *client, int *id)
{
	return 0;
}

int ov5640_sensor_on(struct i2c_client *client)
{
	struct sensor_platform_data *pdata;
	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_OPEN, 1, pdata->eco_flag, pdata->sensor_flag);
	printk(KERN_NOTICE "camera: ov5640 sensor power on!\n");
	return 0;
}

int ov5640_sensor_off(struct i2c_client *client)
{

	struct sensor_platform_data *pdata;
	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_CLOSE, 1, pdata->eco_flag, pdata->sensor_flag);
	printk(KERN_NOTICE "camera: ov5640 sensor power off!\n");
	return 0;
}

u16 last_reg = 0;
static ssize_t ov5640_get_reg(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	u8 val = 0;
	struct i2c_client* c = container_of(dev, struct i2c_client, dev);

	if (last_reg)
	{
		ov5640_read(c, last_reg, &val);
		return sprintf(buf, "%04x:%02x\n", last_reg, val);
	}
	else
	{
		return sprintf(buf, "Please first set the reg to read.\n");
	}

	return 0;
}

static ssize_t ov5640_set_reg(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	u32 reg ,val;
	int ret;
	struct i2c_client* c = container_of(dev, struct i2c_client, dev);

	ret = sscanf(buf, "%x:%x", &reg, &val);
	if (ret == 1)
	{
		last_reg = reg;
	}
	else if (ret == 2)
	{
		ov5640_write(c, reg, val);
		last_reg = reg;
	}

	return count;
}

static ssize_t ov5640_set_div(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d:%d", &div_expo, &div_gain);
	printk("div_expo: %d, div_gain: %d\n", div_expo, div_gain);
	return count;
}


static struct device_attribute static_attrs[] = {
	__ATTR(reg, 0666, ov5640_get_reg, ov5640_set_reg),
	__ATTR(div, 0666, NULL, ov5640_set_div),
};


/*
 * Basic i2c stuff.
 */
extern void ccic_mclk_set(int on);
static int __devinit ov5640_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int fm_ctrl;
	int ret, i;
	struct ov5640_info *info;
	struct sensor_platform_data *pdata;
	int count = MAX_DETECT_NUM;

	ccic_mclk_set(1);
	pdata = client->dev.platform_data;

	do
	{
		pdata->power_set(SENSOR_OPEN, pdata->id, pdata->eco_flag, pdata->sensor_flag);
		ret = ov5640_detect(client);
		pdata->power_set(SENSOR_CLOSE, pdata->id, pdata->eco_flag, pdata->sensor_flag);
	} while (ret && count--);
	
	ccic_mclk_set(0);
	
	if (ret)
	{
		printk("camera: failed detecting ov5640\n");
		return -1;
	}

	/*
	 * Set up our info structure.
	 */
	info = kzalloc(sizeof (struct ov5640_info), GFP_KERNEL);
	if (!info) {
		return -ENOMEM;
	}
	
	info->fmt = &ov5640_formats[1];
	info->sat = 128; /* Review this */
	info->flash.brightness = FLASH_ARG_LUMI_OFF;
	info->flash.duration = FLASH_ARG_TIME_OFF;
	info->flash.mode = FLASH_ARG_MODE_EVER;
	i2c_set_clientdata(client, info);

	ccic_sensor_attach(client);

	if (pdata->id == SENSOR_LOW)
		fm_ctrl = fm_low;
	else
		fm_ctrl = fm_high;

	printk("ov5640[%s] mirror [%s] flip [%s]\n", 
		dev_name(&client->dev), 
		(fm_ctrl&0x1)?"on":"off", 
		(fm_ctrl&0x2)?"on":"off");

	for (i=0; i<ARRAY_SIZE(static_attrs); i++)
	{
		ret = device_create_file(&client->dev, &static_attrs[i]);
		if (ret) printk("camera: failed creating device file\n");
	}
	
	return 0;
}

static int ov5640_remove(struct i2c_client *client)
{
	int i;
	struct ov5640_info *info;

	for (i=0; i<ARRAY_SIZE(static_attrs); i++)
	{
		device_remove_file(&client->dev, &static_attrs[i]);
	}

	ccic_sensor_detach(client);
	info = i2c_get_clientdata(client);
	i2c_set_clientdata(client, NULL);
	kfree(info);
	
	return 0;
}

static int ov5640_streamon(struct i2c_client *client)
{
	return 0;
}

static int ov5640_streamoff(struct i2c_client *client)
{
	return 0;
}

static int ov5640_command(struct i2c_client *client, unsigned int cmd, void *arg)
{
	switch (cmd) {
		case VIDIOC_DBG_G_CHIP_IDENT:
			return v4l2_chip_ident_i2c_client(client, arg, V4L2_IDENT_OV5640, 0);
		case VIDIOC_INT_RESET:
			return ov5640_int_reset(client);
		case VIDIOC_QUERYCAP:
			return ov5640_querycap(client, (struct v4l2_capability *) arg);
		case VIDIOC_ENUM_FMT:
			return ov5640_enum_fmt(client, (struct v4l2_fmtdesc *) arg);
		case VIDIOC_TRY_FMT:
			return ov5640_try_fmt(client, (struct v4l2_format *) arg, NULL, NULL);
		case VIDIOC_ENUM_FRAMESIZES:
			return ov5640_enum_fmsize(client, (struct v4l2_frmsizeenum *) arg);
		case VIDIOC_ENUM_FRAMEINTERVALS:
			return ov5640_enum_fminterval(client, (struct v4l2_frmivalenum *) arg);
		case VIDIOC_S_FMT:
			return ov5640_s_fmt(client, (struct v4l2_format *) arg);
		case VIDIOC_QUERYCTRL:
			return ov5640_q_ctrl(client, (struct v4l2_queryctrl *) arg);
		case VIDIOC_S_CTRL:
			return ov5640_s_ctrl(client, (struct v4l2_control *) arg);
		case VIDIOC_G_CTRL:
			return ov5640_g_ctrl(client, (struct v4l2_control *) arg);
		case VIDIOC_S_PARM:
			return ov5640_s_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_G_PARM:
			return ov5640_g_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_S_INPUT:
			return ov5640_s_input(client, (int *) arg);
		case VIDIOC_STREAMON:
			return ov5640_streamon(client);
		case VIDIOC_STREAMOFF:
			return ov5640_streamoff(client);
#ifdef CONFIG_VIDEO_ADV_DEBUG
		case VIDIOC_DBG_G_REGISTER:
			return ov5640_g_register(client, (struct v4l2_dbg_register *) arg);
		case VIDIOC_DBG_S_REGISTER:
			return ov5640_s_register(client, (struct v4l2_dbg_register *) arg);
		default:
			break;
#endif
	}

	return -EINVAL;
}

static struct i2c_device_id ov5640_idtable[] = {
	{ "ov5640", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, ov5640_idtable);

static struct i2c_driver ov5640_driver = {
	.driver = {
		.name	= "ov5640",
	},
	.id_table       = ov5640_idtable,
	.command	= ov5640_command,
	.probe		= ov5640_probe,
	.remove		= ov5640_remove,
};

/*
 * Module initialization
 */
static int __init ov5640_mod_init(void)
{
	printk(KERN_NOTICE "OmiVision ov5640 sensor driver, at your service\n");
	return i2c_add_driver(&ov5640_driver);
}

static void __exit ov5640_mod_exit(void)
{
	i2c_del_driver(&ov5640_driver);
}

module_init(ov5640_mod_init);
module_exit(ov5640_mod_exit);

