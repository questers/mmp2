/*
 * A V4L2 driver for Himax HM2055 cameras.
 *
 *
 * Copyright 2006-7 
 *
 * This file may be distributed under the terms of the GNU General
 * Public License, version 2.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/videodev.h>
#include <media/v4l2-common.h>
#include <media/v4l2-chip-ident.h>
#include <linux/i2c.h>
#include <mach/hardware.h>
#include <mach/camera.h>
#include <linux/clk.h>

#include <mach/mfp-pxa910.h>


#include "pxa688_camera.h"
#include "hm2055.h"

MODULE_AUTHOR("Kindle.jiang@szime.com>");
MODULE_DESCRIPTION("A low-level driver for hm2055 sensors");
MODULE_LICENSE("GPL");

/*
 * flip and mirror control
 * 0: normal
 * 1: mirror on
 * 2: flip on
 * 3: flip and mirror on
 */
static int fm_low = 0;
module_param(fm_low, int, 0);
static int fm_high = 0;
module_param(fm_high, int, 0);

/*
 * Low-level register I/O.
 */
static int hm2055_read(struct i2c_client *c, u16 reg,
		unsigned char *value)
{

	u8 retVal = 0;
	u8 buffer[2];
	int status;

	if( c == NULL )	
		return -1;
	buffer[0] = (reg >> 8) & 0xff;
	buffer[1] = reg & 0xff;
	
	
	status = i2c_master_send(c,buffer,2);
	if (status >0) {	
		
		status = i2c_master_recv(c,buffer,1);
		retVal = buffer[0];
	}
	*value=retVal;
	return 0;
}


static int hm2055_write(struct i2c_client *c, u16 reg,
		unsigned char value)
{
	u8 data[3];
	data[0] = reg>>8;
	data[1] = reg;
	data[2]=  value;
	i2c_master_send(c, data, 3);
	//if (reg == REG_SYS && (value & SYS_RESET))
	//	msleep(2);  /* Wait for reset to run */
	return 0;
}


/*
 * Write a list of register settings; ff/ff stops the process.
 */
static int hm2055_write_array(struct i2c_client *c, struct regval_list *vals)
{
	int i = 0;
	while (vals->reg_num != 0xffff || vals->value != 0xff) {
		if (vals->reg_num == 0xdddd)
		{
			mdelay(vals->value);
			vals++;
		}
		else
		{
			int ret = hm2055_write(c, vals->reg_num, vals->value);
			if (ret < 0)
				return ret;
			vals++;
			//if (i == 0)
			//	mdelay(20);
			if(vals->reg_num==0x3012&&vals->value==0x80)
				mdelay(20);
			i++;
		}
	}
	
	return 0;
}


/*
 * Stuff that knows about the sensor.
 */
static void hm2055_reset(struct i2c_client *client)
{
	hm2055_write_array(client, hm2055_initial_reg);
	msleep(10);
}

#define HM2055_SENSOR_ID (0x2055)

static int hm2055_detect(struct i2c_client *client)
{
	unsigned char v1,v2;
	u16 id;
	int ret;

	/*
	 * no MID register found. OK, we know we have an OmniVision chip...but which one?
	 */


	ret = hm2055_read(client, REG_PIDH, &v1);
	printk("hm2055_detect REG_PIDH=%d \r\n",v1);
	
	if (ret < 0)
		return ret;
	

	ret = hm2055_read(client, REG_PIDL, &v2);
	printk("hm2055_detect REG_PIDL=%d \r\n",v2);
	if (ret < 0)
		return ret;

	id = (v1<<8)|v2;
	if(id != HM2055_SENSOR_ID) {
		printk("hm2055_detect failed:%x\r\n", id);
		return -ENODEV;
	}
	printk("hm2055_detect -----\r\n");

	
	return 0;
}


/*
 * Store information about the video data format.  The color matrix
 * is deeply tied into the format, so keep the relevant values here.
 * The magic matrix nubmers come from OmniVision.
 */
static struct hm2055_format_struct {
	__u8 *desc;
	__u32 pixelformat;
	int bpp;   /* bits per pixel */
} hm2055_formats[] = {
	{
		.desc			= "UYVY422",
		.pixelformat	= V4L2_PIX_FMT_UYVY,
		.bpp			= 16,
	},
	{
		.desc			= "YUYV422 planar",
		.pixelformat	= V4L2_PIX_FMT_YUV422P,
		.bpp			= 16,
	},
	{
		.desc			= "YUYV 4:2:0",
		.pixelformat	= V4L2_PIX_FMT_YUV420,
		.bpp			= 12,
	},
};
#define N_HM2055_FMTS ARRAY_SIZE(hm2055_formats)

/*TODO - also can use ccic size register 0x34 to do same thing for cropping...anyway, sensor doing it is better?
  0x3020~0x3027*/
static struct hm2055_win_size {
	int	width;
	int	height;
	/* h/vref stuff */
} hm2055_win_sizes[] = {
	/* QCIF */
	{
		.width		= QCIF_WIDTH,
		.height		= QCIF_HEIGHT,
	},
	/* QVGA */
	{
		.width		= QVGA_WIDTH,
		.height		= QVGA_HEIGHT,
	},
	/* CIF */
	{
		.width		= CIF_WIDTH,
		.height		= CIF_HEIGHT,
	},
	/* VGA */
	{
		.width		= VGA_WIDTH,
		.height		= VGA_HEIGHT,
	},
	/* 480p */
	{
		.width		= Q480P_WIDTH,
		.height 	= Q480P_HEIGHT,
	}, 
	/* SVGA */
	{
		.width		= SVGA_WIDTH,
		.height 	= SVGA_HEIGHT,
	}, 
	/* 720p */
	{
		.width		= Q720P_WIDTH,
		.height		= Q720P_HEIGHT,
	}, 
	/* XGA */
	{
		.width		= XGA_WIDTH,
		.height 	= XGA_HEIGHT,
	}, 
	/* UXGA */
	{
		.width		= UXGA_WIDTH,
		.height 	= UXGA_HEIGHT,
	}, 
};

/* capture jpeg size */
static struct hm2055_win_size hm2055_win_sizes_jpeg[] = {
	/* full */
	{
		.width = UXGA_WIDTH,
		.height = UXGA_HEIGHT,
	},
	{
		.width = 640,
		.height = 480,
	},
};

static int hm2055_querycap(struct i2c_client *c, struct v4l2_capability *argp)
{
	if(!argp){
		printk(KERN_ERR" argp is NULL %s %d \n", __FUNCTION__, __LINE__);
		return -EINVAL;
	}
	//printk("************ hm2055_querycap ************\r\n");
	strcpy(argp->driver, "hm2055");
	//strcpy(argp->card, "TD/TTC");
	return 0;
}

static int hm2055_enum_fmt(struct i2c_client *c, struct v4l2_fmtdesc *fmt)
{
	struct hm2055_format_struct *ofmt;

	if (fmt->index >= ARRAY_SIZE(hm2055_formats))
        {
		printk("NO such fmt->index\n");
		return -EINVAL;
	} 
	ofmt = hm2055_formats + fmt->index; ;
	fmt->flags = 0;
	strncpy(fmt->description, ofmt->desc, 32);
	fmt->pixelformat = ofmt->pixelformat;
	return 0;

}


static int hm2055_enum_fmsize(struct i2c_client *c, struct v4l2_frmsizeenum *argp)
{
	struct v4l2_frmsizeenum frmsize;

	if (copy_from_user(&frmsize, argp, sizeof(frmsize)))
		   return -EFAULT;

	if ((frmsize.pixel_format == V4L2_PIX_FMT_YUV420)|| (frmsize.pixel_format == V4L2_PIX_FMT_YUV422P)||(frmsize.pixel_format == V4L2_PIX_FMT_UYVY)){
		if (frmsize.index >= (ARRAY_SIZE(hm2055_win_sizes))){
		    return -EINVAL;
		}
		frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
		frmsize.discrete.height = hm2055_win_sizes[frmsize.index].height;
		frmsize.discrete.width = hm2055_win_sizes[frmsize.index].width;
	}else if(frmsize.pixel_format == V4L2_PIX_FMT_JPEG){
		if (frmsize.index >= ARRAY_SIZE(hm2055_win_sizes_jpeg)){
			   return -EINVAL;
		}
		frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
		frmsize.discrete.height = hm2055_win_sizes_jpeg[frmsize.index].height;
		frmsize.discrete.width = hm2055_win_sizes_jpeg[frmsize.index].width;

	}else
	   return -EINVAL;

	if (copy_to_user(argp, &frmsize, sizeof(frmsize)))
		   return -EFAULT;
	return 0;
}

static int hm2055_try_fmt(struct i2c_client *c, struct v4l2_format *fmt)
{
	int index = 0;
	int i = 0;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	
	for (index = 0; index < ARRAY_SIZE(hm2055_formats); index++)
		if (hm2055_formats[index].pixelformat == pix->pixelformat)
			break;
	if (index >= ARRAY_SIZE(hm2055_formats))
	{
		printk(KERN_ERR"unsupported format!\n");
		return -EINVAL;
	}
	printk("hm2055_try_fmt index=%d\r\n",index);
	if (pix->field == V4L2_FIELD_ANY)
		pix->field = V4L2_FIELD_NONE;
	else if (pix->field != V4L2_FIELD_NONE)
	{
		printk(KERN_ERR"pix->filed != V4l2_FIELD_NONE\n");
		return -EINVAL;
	}
	if(pix->pixelformat == V4L2_PIX_FMT_JPEG){
		for (i = 0; i < ARRAY_SIZE(hm2055_win_sizes_jpeg); i++)
			if (pix->width == hm2055_win_sizes_jpeg[i].width && pix->height == hm2055_win_sizes_jpeg[i].height)
				break;

		if (i >= ARRAY_SIZE(hm2055_win_sizes_jpeg)){
				printk(KERN_ERR"invalid size request for jpeg! %d %d  \n",
					pix->width, pix->height);
				return -EINVAL;
			}
		/* for OV5642, HSYNC contains 2048bytes */
		pix->bytesperline = 2048;
		printk(KERN_ERR"change jpeg width as w %d h %d \n",pix->width,pix->height );
	}else{
		for (i = 0; i < ARRAY_SIZE(hm2055_win_sizes);i ++)
			if (pix->width == hm2055_win_sizes[i].width && pix->height == hm2055_win_sizes[i].height)
				break;

		if (i>= ARRAY_SIZE(hm2055_win_sizes)){
				printk(KERN_ERR"invalid size request for preview! %d %d  \n",
					pix->width, pix->height);
				return -EINVAL;
			}
		printk("i=%d hm2055_win_sizes[i].width=%d\r\n",i,hm2055_win_sizes[i].width);
		pix->bytesperline = pix->width*hm2055_formats[index].bpp/8;
		pix->sizeimage = pix->height*pix->bytesperline;
	}
	return 0;
}

/*
 * Set a format.
 */


static int hm2055_s_fmt(struct i2c_client *c, struct v4l2_format *fmt)
{
	int ret;
	u8 fm;
	u8 val;
	u16 fm_reg;
	int fm_ctrl;
	struct sensor_platform_data *pdata = c->dev.platform_data;
	
	struct regval_list    *pregs;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	ret = hm2055_try_fmt(c, fmt);
	if (ret < 0)
	{
		printk("try fmt error\n");
		return ret;
	}

	
	switch(pix->pixelformat)
	{
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420:
			switch (pix->width )
			{
				case QCIF_WIDTH:
					pregs = hm2055_fmt_yuv422_qcif;
					printk("choose qcif setting \n");
					break;	
				case QVGA_WIDTH:
					pregs = hm2055_fmt_yuv422_qvga;
					printk("choose qvga setting \n");
					break;
				case CIF_WIDTH:
					pregs = hm2055_fmt_yuv422_cif;
					printk("choose cif setting \n");
					break;	
				case VGA_WIDTH:
					pregs = hm2055_fmt_yuv422_vga;
					printk("choose vga setting \n");
					break;
				case SVGA_WIDTH:
					pregs = hm2055_fmt_yuv422_svga;
					printk("choose svga setting \n");
					break;
				case 720:
					pregs = hm2055_fmt_yuv422_480p;
					printk("choose 480p setting \n");
					break;
				case XGA_WIDTH:
					pregs = hm2055_fmt_yuv422_xga;
					printk("choose xga setting \n");
					break;
				case 1280:
					pregs = hm2055_fmt_yuv422_720p;
					printk("choose 720p setting \n");
					break;
				case UXGA_WIDTH:
					pregs = hm2055_fmt_yuv422_uxga;
					printk("choose uxga setting \n");
					break;
				default:
					printk("unsupported YUV format !\n");
					ret = -EINVAL;
					goto out;
					break;
			}
			break;
		default:
			printk("unsupported format!\n");
			ret = -EINVAL;
			goto out;
			break;
	}	

	if (pregs) 
		hm2055_write_array(c, pregs);

	/* flip and mirror */
	hm2055_read(c, 0x0005, &val);
	if (val&0x02)
		fm_reg = REG_RDCFG_CAPTURE;
	else
		fm_reg = REG_RDCFG_PREVIEW;
	printk("%s\n", fm_reg == REG_RDCFG_CAPTURE?"capture mode":"preview mode");
	if (pdata->id == SENSOR_LOW)
		fm_ctrl = fm_low;
	else
		fm_ctrl = fm_high;
	fm_ctrl &= 0x3;
	hm2055_read(c, fm_reg, &fm);
	fm &= 0xfc;
	fm |= ((fm_ctrl>>1)&0x1);
	fm |= ((fm_ctrl<<1)&0x2);
	hm2055_write(c, fm_reg, fm);

out:
	return ret;
}

/*
 * Implement G/S_PARM.  There is a "high quality" mode we could try
 * to do someday; for now, we just do the frame rate tweak.
 */
static int hm2055_g_parm(struct i2c_client *c, struct v4l2_streamparm *parms)
{
	return 0;
}

static int hm2055_s_parm(struct i2c_client *c, struct v4l2_streamparm *parms)
{
	return 0;
}

static int hm2055_s_input(struct i2c_client *c, int *id)
{
	return 0;
}

/*
 * Hue also requires messing with the color matrix.  It also requires
 * trig functions, which tend not to be well supported in the kernel.
 * So here is a simple table of sine values, 0-90 degrees, in steps
 * of five degrees.  Values are multiplied by 1000.
 *
 * The following naive approximate trig functions require an argument
 * carefully limited to -180 <= theta <= 180.
 */

static int g_brightness;
static int g_hue;
static int g_saturation;
static int g_contrast;
static int g_vflip;
static int g_hflip;
static int g_whitebalance;
static int g_exposure;

static int hm2055_t_sat(struct i2c_client *client, int value)
{
	return 0;
}

static int hm2055_q_sat(struct i2c_client *client, __s32 *value)
{
	*value = g_saturation;
	return 0;
}

static int hm2055_t_hue(struct i2c_client *client, int value)
{
	return 0;
}


static int hm2055_q_hue(struct i2c_client *client, __s32 *value)
{
	*value = g_hue;
	return 0;
}

static int hm2055_t_brightness(struct i2c_client *client, int value)
{
	return 0;
}

static int hm2055_q_brightness(struct i2c_client *client, __s32 *value)
{
	*value = g_brightness;
	return 0;
}

static int hm2055_t_contrast(struct i2c_client *client, int value)
{
	return 0;
}

static int hm2055_q_contrast(struct i2c_client *client, __s32 *value)
{
	*value = g_contrast;
	return 0;
}

static int hm2055_q_hflip(struct i2c_client *client, __s32 *value)
{
	*value = g_hflip;
	return 0;
}


static int hm2055_t_hflip(struct i2c_client *client, int value)
{
	return 0;
}

static int hm2055_q_vflip(struct i2c_client *client, __s32 *value)
{
	*value = g_vflip;
	return 0;
}

static int hm2055_t_vflip(struct i2c_client *client, int value)
{
	return 0;
}

static int hm2055_t_whitebalance(struct i2c_client *client, int value)
{
	return 0;
}

static int hm2055_q_whitebalance(struct i2c_client *client, __s32 *value)
{
	*value = g_whitebalance;
	return 0;
}

static int hm2055_t_exposure(struct i2c_client *client, int value)
{
	return 0;
}

static int hm2055_q_exposure(struct i2c_client *client, __s32 *value)
{
	*value = g_exposure;
	return 0;
}


static struct hm2055_control {
	struct v4l2_queryctrl qc;
	int (*query)(struct i2c_client *c, __s32 *value);
	int (*tweak)(struct i2c_client *c, int value);
} hm2055_controls[] =
{
	{
		.qc = {
			.id = V4L2_CID_BRIGHTNESS,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "Brightness",
			.minimum = 0,
			.maximum = 4,
			.step = 1,
			.default_value = 2,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = hm2055_t_brightness,
		.query = hm2055_q_brightness,
	},
	{
		.qc = {
			.id = V4L2_CID_CONTRAST,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "Contrast",
			.minimum = 0,
			.maximum = 4,
			.step = 1,
			.default_value = 2,   /* XXX hm2055 spec */
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = hm2055_t_contrast,
		.query = hm2055_q_contrast,
	},
	{
		.qc = {
			.id = V4L2_CID_SATURATION,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "Saturation",
			.minimum = 0,
			.maximum = 4,
			.step = 1,
			.default_value = 2,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = hm2055_t_sat,
		.query = hm2055_q_sat,
	},
	{
		.qc = {
			.id = V4L2_CID_HUE,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "HUE",
			.minimum = 0,
			.maximum = 3,
			.step = 1,
			.default_value = 0,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = hm2055_t_hue,
		.query = hm2055_q_hue,
	},
	{
		.qc = {
			.id = V4L2_CID_VFLIP,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "Vertical flip",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = hm2055_t_vflip,
		.query = hm2055_q_vflip,
	},
	{
		.qc = {
			.id = V4L2_CID_HFLIP,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "Horizontal mirror",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = hm2055_t_hflip,
		.query = hm2055_q_hflip,
	},
	{
		.qc = {
			.id = V4L2_CID_DO_WHITE_BALANCE,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "white balance",
			.minimum = 0,
			.maximum = 4,
			.step = 1,
			.default_value = 2,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = hm2055_t_whitebalance,
		.query = hm2055_q_whitebalance,
	},
	{
		.qc = {
			.id = V4L2_CID_EXPOSURE,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "exposure",
			.minimum = 0,
			.maximum = 4,
			.step = 1,
			.default_value = 2,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = hm2055_t_exposure,
		.query = hm2055_q_exposure,
	},
};
#define N_CONTROLS (ARRAY_SIZE(hm2055_controls))

static struct hm2055_control *hm2055_find_control(__u32 id)
{
	int i;
	printk("************ hm2055_find_control ************\r\n");
	for (i = 0; i < N_CONTROLS; i++)
		if (hm2055_controls[i].qc.id == id)
			return hm2055_controls + i;
	return NULL;
}


static int hm2055_queryctrl(struct i2c_client *client,
		struct v4l2_queryctrl *qc)
{
	struct hm2055_control *ctrl = hm2055_find_control(qc->id);
	printk("************ hm2055_queryctrl ************\r\n");
	if (ctrl == NULL)
		return -EINVAL;
	*qc = ctrl->qc;
	return 0;
}

static int hm2055_g_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct hm2055_control *octrl = hm2055_find_control(ctrl->id);
	int ret;

	if (octrl == NULL)
		return -EINVAL;
	ret = octrl->query(client, &ctrl->value);
	if (ret >= 0)
		return 0;
	return ret;
}

static int hm2055_s_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct hm2055_control *octrl = hm2055_find_control(ctrl->id);
	int ret;

	if (octrl == NULL)
		return -EINVAL;
	ret =  octrl->tweak(client, ctrl->value);
	if (ret >= 0)
		return 0;
	return ret;
}

u16 last_reg = 0;
static ssize_t sensor_get_reg(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u8 val = 0;
	struct i2c_client* c = container_of(dev, struct i2c_client, dev);

	if (last_reg)
	{
		hm2055_read(c, last_reg, &val);
		return sprintf(buf, "%04x:%02x\n", last_reg, val);
	}
	else
	{
		int len=0;
		int index;

		for (index=0; hm2055_initial_reg[index].reg_num!=0xffff; index++)
		{
			hm2055_read(c, hm2055_initial_reg[index].reg_num, &val);
			len += sprintf(&buf[len], "%04x:%02x%s\n", hm2055_initial_reg[index].reg_num, val, (val!=hm2055_initial_reg[index].value)?"*":"");
		}
		return len;
	}
}

static ssize_t sensor_set_reg(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned int reg ,val;
	int ret;
	struct i2c_client* c = container_of(dev, struct i2c_client, dev);

	ret = sscanf(buf, "%x:%x", &reg, &val);
	if (ret == 1)
	{
		last_reg = reg;
		return count;
	}
	else if (ret == 2)
	{
		hm2055_write(c, reg, val);
		last_reg = reg;
		return count;
	}
	else
	{
		last_reg = 0;
		return count;
	}
}

static struct device_attribute sensor_static_attrs[] = {
	__ATTR(reg, 0666, sensor_get_reg, sensor_set_reg),
};



int ccic_sensor_attach(struct i2c_client *client);
extern struct clk *pxa168_ccic_gate_clk;
extern void ccic_mclk_set(int on);

/*
 * Basic i2c stuff.
 */
static int __devinit hm2055_probe(struct i2c_client *client, const struct i2c_device_id * i2c_id)
{
	int fm_ctrl;
	int ret = 0;
	int i=0;
	struct hm2055_info *info;
	struct sensor_platform_data *pdata;
	pdata = client->dev.platform_data;

	ccic_mclk_set(1);
	pdata->power_set(SENSOR_OPEN, pdata->id, pdata->eco_flag, pdata->sensor_flag);
	/*
	 * Set up our info structure.
	 */
	info = kzalloc(sizeof (struct hm2055_info), GFP_KERNEL);
	if (! info) {
		ret = -ENOMEM;
		goto out_free;
	}
	info->fmt = &hm2055_formats[1];
	info->sat = 128;	/* Review this */
	i2c_set_clientdata(client, info);
	printk("hm2055_probe name=%s addr=0x%x\r\n",client->name,client->addr);
	/*
	 * Make sure it's an hm2055
	 */

	ret = hm2055_detect(client);

	if (ret)
		goto out_free;
	printk(KERN_NOTICE "HiMax hm2055 sensor detected\n");
	ret = ccic_sensor_attach(client);
	if (ret)
		goto out_free;

	if (pdata->id == SENSOR_LOW)
		fm_ctrl = fm_low;
	else
		fm_ctrl = fm_high;

	printk("hm2055[%s] mirror [%s] flip [%s]\n", 
		dev_name(&client->dev), 
		(fm_ctrl&0x1)?"on":"off", 
		(fm_ctrl&0x2)?"on":"off");

	/* create sys entries */
	for (i=0; i<ARRAY_SIZE(sensor_static_attrs); i++)
	{
		ret = device_create_file(&client->dev, &sensor_static_attrs[i]);
		if(ret)
			goto out_free;
	}

out_free:
	pdata->power_set(SENSOR_CLOSE, pdata->id, pdata->eco_flag, pdata->sensor_flag);
	ccic_mclk_set(0);

	if(ret)
		kfree(info);
	return ret;
}


static int hm2055_remove(struct i2c_client *client)
{
	int i;
	
	for (i=0; i<ARRAY_SIZE(sensor_static_attrs); i++)
		device_remove_file(&client->dev, &sensor_static_attrs[i]);

	return 0;	//TODO
}


static int hm2055_streamon(struct i2c_client *client)
{
	return 0;
}

static int hm2055_streamoff(struct i2c_client *client)
{
	return 0;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int hm2055_g_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return hm2055_read(client, (u16)reg->reg, (unsigned char *)&(reg->val));
}

static int hm2055_s_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return hm2055_write(client, (u16)reg->reg, (unsigned char)reg->val);
}
#endif
static int hm2055_command(struct i2c_client *client, unsigned int cmd,
		void *arg)
{
//	printk("\n ************ hm2055_command cmd= ");
	switch (cmd) {
		case VIDIOC_DBG_G_CHIP_IDENT:
			return v4l2_chip_ident_i2c_client(client, arg, V4L2_IDENT_HM2055, 0);
			
		case VIDIOC_INT_RESET:
			hm2055_reset(client);
			return 0;
#if 0
		case VIDIOC_INT_INIT:
			printk("VIDIOC_INT_INIT \r\n");
			hm2055_init(client);
			return 0;//hm2055_init(client);		//TODO - should get 2650 default register values
#endif
		case VIDIOC_QUERYCAP:
			return hm2055_querycap(client, (struct v4l2_capability *) arg);
		case VIDIOC_ENUM_FMT:
			return hm2055_enum_fmt(client, (struct v4l2_fmtdesc *) arg);
		case VIDIOC_ENUM_FRAMESIZES:
			return hm2055_enum_fmsize(client, (struct v4l2_frmsizeenum *) arg);
		case VIDIOC_TRY_FMT:
			return hm2055_try_fmt(client, (struct v4l2_format *) arg);
		case VIDIOC_S_FMT:
			return hm2055_s_fmt(client, (struct v4l2_format *) arg);
		case VIDIOC_QUERYCTRL:
			return hm2055_queryctrl(client, (struct v4l2_queryctrl *) arg);
		case VIDIOC_S_CTRL:
			return hm2055_s_ctrl(client, (struct v4l2_control *) arg);
		case VIDIOC_G_CTRL:
			return hm2055_g_ctrl(client, (struct v4l2_control *) arg);
		case VIDIOC_S_PARM:
			return hm2055_s_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_G_PARM:
			return hm2055_g_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_S_INPUT:
			return hm2055_s_input(client, (int *) arg);
		case VIDIOC_STREAMON:
			return hm2055_streamon(client);
		case VIDIOC_STREAMOFF:
			return hm2055_streamoff(client);
#ifdef CONFIG_VIDEO_ADV_DEBUG
		case VIDIOC_DBG_G_REGISTER:
			return hm2055_g_register(client, (struct v4l2_dbg_register *) arg);
		case VIDIOC_DBG_S_REGISTER:
			return hm2055_s_register(client, (struct v4l2_dbg_register *) arg);
#endif
	}
	return -EINVAL;
}

static struct i2c_device_id hm2055_idtable[] = {
	{ "hm2055", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, hm2055_idtable);

static struct i2c_driver hm2055_driver = {
	.driver = {
		.name	= "hm2055",
	},
	.id_table       = hm2055_idtable,
	.command	= hm2055_command,
	.probe		= hm2055_probe,
	.remove		= hm2055_remove,
};


/*
 * Module initialization
 */
static int __init hm2055_mod_init(void)
{
	printk(KERN_NOTICE "HiMax hm2055 sensor driver, at your service\n");
	return i2c_add_driver(&hm2055_driver);
}

static void __exit hm2055_mod_exit(void)
{
	i2c_del_driver(&hm2055_driver);
}

module_init(hm2055_mod_init);
module_exit(hm2055_mod_exit);

