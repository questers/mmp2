/*
 * A V4L2 driver for GalaxyCore gc2015 cameras.
 *
 * Copyright 2006 One Laptop Per Child Association, Inc.  Written
 * by Jonathan Corbet with substantial inspiration from Mark
 * McClelland's ovcamchip code.
 *
 * Copyright 2006-7 Jonathan Corbet <corbet@lwn.net>
 *
 * This file may be distributed under the terms of the GNU General
 * Public License, version 2.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/videodev.h>
#include <media/v4l2-common.h>
#include <media/v4l2-chip-ident.h>
#include <linux/i2c.h>
#include <mach/hardware.h>
#include <mach/camera.h>
#include <mach/mfp.h>

#include <linux/clk.h>
#include <mach/pxa910.h>
#include "pxa688_camera.h"
#include "gc2015.h"

/*
 * flip and mirror control
 * 0: normal
 * 1: mirror on
 * 2: flip on
 * 3: flip and mirror on
 */
static int fm_low = 0;
module_param(fm_low, int, 0);
static int fm_high = 0;
module_param(fm_high, int, 0);

/*
 * Low-level register I/O.
 */
static int gc2015_read(struct i2c_client *c, u8 reg, u8* value)
{
	int ret = i2c_smbus_read_byte_data(c, reg);
	if (ret < 0)
	{
		return ret;
	}
	else
	{
		*value = (u8) ret;
		return 0;
	}
}

static int gc2015_write(struct i2c_client *c, u8 reg, u8 value)
{
	return i2c_smbus_write_byte_data(c, reg, value);
}

/*
 * Write a list of register settings; ff/ff stops the process.
 */
static int gc2015_write_array(struct i2c_client *c, struct regval_list *vals)
{
	while (vals->reg_num != 0xff || vals->value != 0xff) {
		int ret = gc2015_write(c, vals->reg_num, vals->value);
		if (ret < 0)
			return ret;
		vals++;
	}

	return 0;
}


/*
 * Stuff that knows about the sensor.
 */
static int gc2015_reset(struct i2c_client *client)
{
	int ret;
	
	//soft reset
	gc2015_write(client, SOFTWARE_RESET, 0x80);
	msleep(2);

	/* Initialize settings */
	ret = gc2015_write_array(client, gc2015_regs_defaults);
	if (ret < 0)
		return ret;

	return 0;
}

////////////////james added  20110919/////////////////
static int first_time = 1; /* do we need to set the default parameter? */
static int div = 17;
static int gc2015_read_shutter(struct i2c_client* client)   //james
{
	int ret;
	u8 page;
	u16 reg1, reg2;
	u8 temp_reg1, temp_reg2;
	u16 gc2015_sensor_pv_shutter=0;
	u16 gc2015_sensor_pv_extra_shutter=0;
	
	/* Backup the preview mode last shutter & sensor gain. */
	ret = gc2015_read(client,0xfe,&page);
	gc2015_write(client, 0xfe, 0);
	reg1 = gc2015_read(client,0x03,&temp_reg1);
	reg2 = gc2015_read(client,0x04,&temp_reg2);
	gc2015_sensor_pv_shutter = (temp_reg1 << 8) | (temp_reg2 & 0xFF);
	
	/* Backup the preview mode last shutter & sensor gain. */
	gc2015_sensor_pv_extra_shutter = 0;
	
	return gc2015_sensor_pv_shutter + gc2015_sensor_pv_extra_shutter;
}    /* gc2015_read_shutter */

static void gc2015_set_hb_shutter(struct i2c_client* client, u16 hb_add,  u16 shutter)  //james
{
	u16 hb_ori, hb_total;
	u16 temp_reg;

	/*Set HB start*/
	
	/*The HB must < 0xFFF*/
	//hb_ori = (gc2015_read_cmos_sensor(0x12)<<8 )|( gc2015_read_cmos_sensor(0x13));
	hb_ori = 298;
	hb_total = hb_ori + hb_add;


	if(hb_total > 0xfff)
	{
		gc2015_write(client, 0x12 , 0x0f);
		gc2015_write(client, 0x13 , 0xff); 
		temp_reg = shutter* (1702 + hb_ori ) *10  / ( div * 4095);
		
	}
	else
	{
		gc2015_write(client, 0x12 , (hb_total>>8)&0xff);
		gc2015_write(client, 0x13 , hb_total&0xff); 
		temp_reg = shutter * (1702 + hb_ori ) * 10  / ( div * (1702 + hb_ori + hb_add));
	}
	//temp_reg = shutter * 10 / 11; 

	/*Set HB end*/

	/*Set Shutter start*/
	if(temp_reg < 1) temp_reg = 1;
	gc2015_write(client, 0x03 , (temp_reg>>8)&0xff);           
	gc2015_write(client, 0x04 , temp_reg&0xff); 
	/*Set Shutter end*/
}    /* gc2015_set_dummy */



static void gc2015_cap_setting(struct i2c_client* client)   //james
{
	u16 shutter;
	int gc2015_sensor_cap_dummy_pixels = 0;
	u8 AE_reg,AWB_reg;
	char temp_AE_reg,temp_AWB_reg;
	   		// turn off AEC/AGC

	AE_reg=gc2015_read(client,0x4f,&temp_AE_reg);
	gc2015_write(client,0x4f , temp_AE_reg&~0x01); /* Turn OFF AEC/AGC*/
	
	AWB_reg=gc2015_read(client,0x42,&temp_AWB_reg);//turn off awb
	gc2015_write(client,0x42 , temp_AWB_reg&~0x02);

	shutter = gc2015_read_shutter(client);
	
	gc2015_set_hb_shutter(client, gc2015_sensor_cap_dummy_pixels, shutter);

	gc2015_write(client, 0x6e , 0x19); //BLK offset submode,offset R
	gc2015_write(client, 0x6f , 0x10);
	gc2015_write(client, 0x70 , 0x19);
	gc2015_write(client, 0x71 , 0x10);	


	gc2015_write(client,0x4f , temp_AE_reg|0x01);/* Turn ON AEC/AGC*/
	gc2015_write(client,0x42 , temp_AWB_reg|0x02);

	msleep(50);  // james
}

static int gc2015_detect(struct i2c_client *client)
{
	int ret;
	u8 v;

	ret = gc2015_read(client, GC2015_PID_H, &v);
	if (ret < 0)
	{
		printk("Error reading PID high.\n");
		return ret;
	}

	if (v != DEFAULT_PID_H)
	{
		printk(KERN_ERR "Detected unknown sensor with PID H: 0x%2x\n", v);
		return -ENODEV;
	}

	ret = gc2015_read(client, GC2015_PID_L, &v);
	if (ret < 0)
	{
		printk("Error reading PID low.\n");
		return ret;
	}

	if (v != DEFAULT_PID_L)
	{
		printk(KERN_ERR "Detected unknown sensor with PID L: 0x%2x\n", v);
		return -ENODEV;
	}

	return 0;
}


static int gc2015_enum_fmsize(struct i2c_client *client, struct v4l2_frmsizeenum *argp)
{
	struct v4l2_frmsizeenum frmsize;

	if (copy_from_user(&frmsize, argp, sizeof(frmsize)))
		return -EFAULT;

	switch (frmsize.pixel_format) {
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420:
			if (frmsize.index == ARRAY_SIZE(gc2015_win_sizes))
				return -EINVAL;
			else if (frmsize.index > ARRAY_SIZE(gc2015_win_sizes)) {
				printk(KERN_ERR "camera: gc2015 enum unsupported preview format size!\n");
				return -EINVAL;
			} else {
				frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
				frmsize.discrete.height = gc2015_win_sizes[frmsize.index].height;
				frmsize.discrete.width = gc2015_win_sizes[frmsize.index].width;
				break;
			}
		default:
			printk(KERN_ERR "camera: gc2015 enum format size with unsupported format!\n");
			return -EINVAL;
	}

	if (copy_to_user(argp, &frmsize, sizeof(frmsize)))
		return -EFAULT;
	
	return 0;
}

static int gc2015_enum_fmt(struct i2c_client *c, struct v4l2_fmtdesc *fmt)
{
	struct gc2015_format_struct *ofmt;

	if (fmt->index >= N_GC2015_FMTS)
		return -EINVAL;

	ofmt = gc2015_formats + fmt->index;
	fmt->flags = 0;
	strcpy(fmt->description, ofmt->desc);
	fmt->pixelformat = ofmt->pixelformat;
	return 0;
}


static int gc2015_try_fmt(struct i2c_client *c, struct v4l2_format *fmt,
		struct gc2015_format_struct **ret_fmt,
		struct gc2015_win_size **ret_wsize)
{
	int index;
	struct gc2015_win_size *wsize;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	for (index = 0; index < N_GC2015_FMTS; index++)
		if (gc2015_formats[index].pixelformat == pix->pixelformat)
			break;
	if (index >= N_GC2015_FMTS){
		printk("unsupported format[%d]!\n", pix->pixelformat);
		return -EINVAL;
	}
	if (ret_fmt != NULL)
		*ret_fmt = gc2015_formats + index;
	/*
	 * Fields: the OV devices claim to be progressive.
	 */
	if (pix->field == V4L2_FIELD_ANY)
		pix->field = V4L2_FIELD_NONE;
	else if (pix->field != V4L2_FIELD_NONE)
	{
		printk("invalid field\n");
		//return -EINVAL; /* don't return, for mpt. -guang */
	}
	/*
	 * Round requested image size down to the nearest
	 * we support, but not below the smallest.
	 */
	for (wsize = gc2015_win_sizes; wsize < gc2015_win_sizes + N_GC2015_WIN_SIZES;
			wsize++)
		if (pix->width <= wsize->width && pix->height <= wsize->height)
			break;
	if (wsize >= gc2015_win_sizes + N_GC2015_WIN_SIZES){
		printk("size exceed and set as QXGA!\n");
		wsize--;   /* Take the smallest one */
	}
	if (ret_wsize != NULL)
		*ret_wsize = wsize;
	/*
	 * Note the size we'll actually handle.
	 */
#if 0
	pix->width = wsize->width;
	pix->height = wsize->height;
#endif
	pix->bytesperline = pix->width*gc2015_formats[index].bpp/8;
	pix->sizeimage = pix->height*pix->bytesperline;

	if (ret_fmt == NULL)
		return 0;
	
	switch (pix->pixelformat)
	{
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420:
			break;
		default:
			printk("unsupported format - %d!\n", pix->pixelformat);
			return -EINVAL;
	}
	return 0;
}

/*
 * Set a format.
 */
static int gc2015_s_fmt(struct i2c_client *c, struct v4l2_format *fmt)
{
	int ret;
	u8 fm;
	struct gc2015_format_struct *ovfmt;
	struct gc2015_win_size *wsize;
	int fm_ctrl;
	struct sensor_platform_data *pdata = c->dev.platform_data;

	ret = gc2015_try_fmt(c, fmt, &ovfmt, &wsize);
	if (ret)
		return ret;

	gc2015_write_array(c, wsize->regs);

	/* 
	 * we also set gc2015_regs_high for low resolution
	 * otherwise low resolution preview would mess
	 */
	if (wsize->width > 800)
		gc2015_write_array(c, gc2015_regs_high);
	else
		gc2015_write_array(c, gc2015_regs_high);

	/* if size is large than 1024x768, set special gain */
	if (wsize->width > 800)
	{
		printk("set special gain for size[%dx%d]\n", wsize->width, wsize->height);
		gc2015_cap_setting(c);
	}

	/* flip and mirror */
	if (pdata->id == SENSOR_LOW)
		fm_ctrl = fm_low;
	else
		fm_ctrl = fm_high;
	fm_ctrl &= 0x3;
	gc2015_write(c, 0xfe, 0x00); /* set page 0 */
	gc2015_read(c, 0x29, &fm);
	fm &= 0xfc;
	fm |= fm_ctrl;
	gc2015_write(c, 0xfe, 0x00); /* set page 0 */
	gc2015_write(c, 0x29, fm);

	return ret;
}

/*
 * Get a format.
 */
static int gc2015_g_fmt(struct i2c_client *c, struct v4l2_format *fmt)
{
	/*
	unsigned char w_msb;
	unsigned char w_lsb;
	unsigned char h_msb;
	unsigned char h_lsb;

	gc2015_read(c, 0x3088, &w_msb);
	gc2015_read(c, 0x3089, &w_lsb);
	gc2015_read(c, 0x308a, &h_msb);
	gc2015_read(c, 0x308b, &h_lsb);

	fmt->type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	fmt->fmt.pix.width = (w_msb<<8)|w_lsb;
	fmt->fmt.pix.height = (h_msb<<8)|h_lsb;
	fmt->fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
	fmt->fmt.pix.field = V4L2_FIELD_NONE;*/

	return 0;
}

/*
 * Implement G/S_PARM.  There is a "high quality" mode we could try
 * to do someday; for now, we just do the frame rate tweak.
 */
static int gc2015_g_parm(struct i2c_client *c, struct v4l2_streamparm *parms)
{
#if 0        
	struct v4l2_captureparm *cp = &parms->parm.capture;
	unsigned char clkrc;
	int ret;

	if (parms->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
		return -EINVAL;
	ret = gc2015_read(c, REG_CLKRC, &clkrc);
	if (ret < 0)
		return ret;
	memset(cp, 0, sizeof(struct v4l2_captureparm));
	cp->capability = V4L2_CAP_TIMEPERFRAME;
	cp->timeperframe.numerator = 1;
	cp->timeperframe.denominator = gc2015_FRAME_RATE;
	if ((clkrc & CLK_EXT) == 0 && (clkrc & CLK_SCALE) > 1)
		cp->timeperframe.denominator /= (clkrc & CLK_SCALE);
#endif    
	return 0;
}

static int gc2015_s_parm(struct i2c_client *c, struct v4l2_streamparm *parms)
{
#if 0
	struct v4l2_captureparm *cp = &parms->parm.capture;
	struct v4l2_fract *tpf = &cp->timeperframe;
	unsigned char clkrc;
	int ret, div;

	if (parms->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
		return -EINVAL;
	if (cp->extendedmode != 0)
		return -EINVAL;
	/*
	 * CLKRC has a reserved bit, so let's preserve it.
	 */
	ret = gc2015_read(c, REG_CLKRC, &clkrc);
	if (ret < 0)
		return ret;
	if (tpf->numerator == 0 || tpf->denominator == 0)
		div = 1;  /* Reset to full rate */
	else
		div = (tpf->numerator*gc2015_FRAME_RATE)/tpf->denominator;
	if (div == 0)
		div = 1;
	else if (div > CLK_SCALE)
		div = CLK_SCALE;
	clkrc = (clkrc & 0xc0) | div ;
	tpf->numerator = 1;
	tpf->denominator = gc2015_FRAME_RATE/div;
	return gc2015_write(c, REG_CLKRC, clkrc);
#endif
	return 0;
}

static int gc2015_s_input(struct i2c_client *c, int *id)
{
	first_time = 1;
	return 0;
}

/*
 * Code for dealing with controls.
 */


/*TODO - need to port below register codes for 3640...maybe not used*/


static int gc2015_store_cmatrix(struct i2c_client *client,
		int matrix[CMATRIX_LEN])
{
#if 0
	int i, ret;
	unsigned char signbits;

	/*
	 * Weird crap seems to exist in the upper part of
	 * the sign bits register, so let's preserve it.
	 */
	ret = gc2015_read(client, REG_CMATRIX_SIGN, &signbits);
	signbits &= 0xc0;

	for (i = 0; i < CMATRIX_LEN; i++) {
		unsigned char raw;

		if (matrix[i] < 0) {
			signbits |= (1 << i);
			if (matrix[i] < -255)
				raw = 0xff;
			else
				raw = (-1 * matrix[i]) & 0xff;
		}
		else {
			if (matrix[i] > 255)
				raw = 0xff;
			else
				raw = matrix[i] & 0xff;
		}
		ret += gc2015_write(client, REG_CMATRIX_BASE + i, raw);
	}
	ret += gc2015_write(client, REG_CMATRIX_SIGN, signbits);
	return ret;
#endif
	return 0;
}


/*
 * Hue also requires messing with the color matrix.  It also requires
 * trig functions, which tend not to be well supported in the kernel.
 * So here is a simple table of sine values, 0-90 degrees, in steps
 * of five degrees.  Values are multiplied by 1000.
 *
 * The following naive approximate trig functions require an argument
 * carefully limited to -180 <= theta <= 180.
 */
#define SIN_STEP 5
static const int gc2015_sin_table[] = {
	0,	 87,   173,   258,   342,   422,
	499,	573,   642,   707,   766,   819,
	866,	906,   939,   965,   984,   996,
	1000
};

static int gc2015_sine(int theta)
{
	int chs = 1;
	int sine;

	if (theta < 0) {
		theta = -theta;
		chs = -1;
	}
	if (theta <= 90)
		sine = gc2015_sin_table[theta/SIN_STEP];
	else {
		theta -= 90;
		sine = 1000 - gc2015_sin_table[theta/SIN_STEP];
	}
	return sine*chs;
}

static int gc2015_cosine(int theta)
{
	theta = 90 - theta;
	if (theta > 180)
		theta -= 360;
	else if (theta < -180)
		theta += 360;
	return gc2015_sine(theta);
}




static void gc2015_calc_cmatrix(struct gc2015_info *info,
		int matrix[CMATRIX_LEN])
{
	int i;
	/*
	 * Apply the current saturation setting first.
	 */
	for (i = 0; i < CMATRIX_LEN; i++)
		matrix[i] = (info->fmt->cmatrix[i]*info->sat) >> 7;
	/*
	 * Then, if need be, rotate the hue value.
	 */
	if (info->hue != 0) {
		int sinth, costh, tmpmatrix[CMATRIX_LEN];

		memcpy(tmpmatrix, matrix, CMATRIX_LEN*sizeof(int));
		sinth = gc2015_sine(info->hue);
		costh = gc2015_cosine(info->hue);

		matrix[0] = (matrix[3]*sinth + matrix[0]*costh)/1000;
		matrix[1] = (matrix[4]*sinth + matrix[1]*costh)/1000;
		matrix[2] = (matrix[5]*sinth + matrix[2]*costh)/1000;
		matrix[3] = (matrix[3]*costh - matrix[0]*sinth)/1000;
		matrix[4] = (matrix[4]*costh - matrix[1]*sinth)/1000;
		matrix[5] = (matrix[5]*costh - matrix[2]*sinth)/1000;
	}
}



static int gc2015_t_sat(struct i2c_client *client, int value)
{
	struct gc2015_info *info = i2c_get_clientdata(client);
	int matrix[CMATRIX_LEN];
	int ret;

	info->sat = value;
	gc2015_calc_cmatrix(info, matrix);
	ret = gc2015_store_cmatrix(client, matrix);
	return ret;
}

static int gc2015_q_sat(struct i2c_client *client, __s32 *value)
{
	struct gc2015_info *info = i2c_get_clientdata(client);

	*value = info->sat;
	return 0;
}

static int gc2015_t_hue(struct i2c_client *client, int value)
{
	struct gc2015_info *info = i2c_get_clientdata(client);
	int matrix[CMATRIX_LEN];
	int ret;

	if (value < -180 || value > 180)
		return -EINVAL;
	info->hue = value;
	gc2015_calc_cmatrix(info, matrix);
	ret = gc2015_store_cmatrix(client, matrix);
	return ret;
}


static int gc2015_q_hue(struct i2c_client *client, __s32 *value)
{
	struct gc2015_info *info = i2c_get_clientdata(client);

	*value = info->hue;
	return 0;
}

#if 0
/*
 * Some weird registers seem to store values in a sign/magnitude format!
 */
static unsigned char gc2015_sm_to_abs(unsigned char v)
{
	if ((v & 0x80) == 0)
		return v + 128;
	else
		return 128 - (v & 0x7f);
}


static unsigned char gc2015_abs_to_sm(unsigned char v)
{
	if (v > 127)
		return v & 0x7f;
	else
		return (128 - v) | 0x80;
}
#endif

static int gc2015_t_brightness(struct i2c_client *client, int value)
{
#if 0
	unsigned char com8, v;
	int ret;

	gc2015_read(client, REG_COM8, &com8);
	com8 &= ~COM8_AEC;
	gc2015_write(client, REG_COM8, com8);
	v = gc2015_abs_to_sm(value);
	ret = gc2015_write(client, REG_BRIGHT, v);
	return ret;
#endif
	return 0;
}

static int gc2015_q_brightness(struct i2c_client *client, __s32 *value)
{
#if 0
	unsigned char v;
	int ret = gc2015_read(client, REG_BRIGHT, &v);

	*value = gc2015_sm_to_abs(v);
	return ret;
#endif
	return 0;
}

static int gc2015_t_contrast(struct i2c_client *client, int value)
{
#if 0
	return gc2015_write(client, REG_CONTRAS, (unsigned char) value);
#endif
	return 0;
}

static int gc2015_q_contrast(struct i2c_client *client, __s32 *value)
{
#if 0
	unsigned char v;
	int ret = gc2015_read(client, REG_CONTRAS, &v);

	*value = v;
	return ret;
#endif
		return 0;
}

static int gc2015_q_hflip(struct i2c_client *client, __s32 *value)
{
#if 0
	int ret;
	unsigned char v;

	ret = gc2015_read(client, REG_MVFP, &v);
	*value = (v & MVFP_MIRROR) == MVFP_MIRROR;
	return ret;
#endif
			return 0;
}


static int gc2015_t_hflip(struct i2c_client *client, int value)
{
#if 0
	unsigned char v;
	int ret;

	ret = gc2015_read(client, REG_MVFP, &v);
	if (value)
		v |= MVFP_MIRROR;
	else
		v &= ~MVFP_MIRROR;
	msleep(10);  /* FIXME */
	ret += gc2015_write(client, REG_MVFP, v);
	return ret;
#endif
	return 0;
}



static int gc2015_q_vflip(struct i2c_client *client, __s32 *value)
{
#if 0
	int ret;
	unsigned char v;

	ret = gc2015_read(client, REG_MVFP, &v);
	*value = (v & MVFP_FLIP) == MVFP_FLIP;
	return ret;
#endif
		return 0;
}


static int gc2015_t_vflip(struct i2c_client *client, int value)
{
#if 0
	unsigned char v;
	int ret;

	ret = gc2015_read(client, REG_MVFP, &v);
	if (value)
		v |= MVFP_FLIP;
	else
		v &= ~MVFP_FLIP;
	msleep(10);  /* FIXME */
	ret += gc2015_write(client, REG_MVFP, v);
	return ret;
#endif
	return 0;
}


static struct gc2015_control {
	struct v4l2_queryctrl qc;
	int (*query)(struct i2c_client *c, __s32 *value);
	int (*tweak)(struct i2c_client *c, int value);
} gc2015_controls[] =
{
	{
		.qc = {
			.id = V4L2_CID_BRIGHTNESS,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "Brightness",
			.minimum = 0,
			.maximum = 255,
			.step = 1,
			.default_value = 0x80,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = gc2015_t_brightness,
		.query = gc2015_q_brightness,
	},
	{
		.qc = {
			.id = V4L2_CID_CONTRAST,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "Contrast",
			.minimum = 0,
			.maximum = 127,
			.step = 1,
			.default_value = 0x40,   /* XXX gc2015 spec */
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = gc2015_t_contrast,
		.query = gc2015_q_contrast,
	},
	{
		.qc = {
			.id = V4L2_CID_SATURATION,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "Saturation",
			.minimum = 0,
			.maximum = 256,
			.step = 1,
			.default_value = 0x80,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = gc2015_t_sat,
		.query = gc2015_q_sat,
	},
	{
		.qc = {
			.id = V4L2_CID_HUE,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "HUE",
			.minimum = -180,
			.maximum = 180,
			.step = 5,
			.default_value = 0,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = gc2015_t_hue,
		.query = gc2015_q_hue,
	},
	{
		.qc = {
			.id = V4L2_CID_VFLIP,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "Vertical flip",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = gc2015_t_vflip,
		.query = gc2015_q_vflip,
	},
	{
		.qc = {
			.id = V4L2_CID_HFLIP,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "Horizontal mirror",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = gc2015_t_hflip,
		.query = gc2015_q_hflip,
	},
};
#define N_CONTROLS (ARRAY_SIZE(gc2015_controls))

static struct gc2015_control *gc2015_find_control(__u32 id)
{
	int i;

	for (i = 0; i < N_CONTROLS; i++)
		if (gc2015_controls[i].qc.id == id)
			return gc2015_controls + i;
	return NULL;
}

static int gc2015_querycap(struct i2c_client *client,
		struct v4l2_capability *cap)
{
	strcpy(cap->driver, "gc2015");

	return 0;
}


static int gc2015_queryctrl(struct i2c_client *client,
		struct v4l2_queryctrl *qc)
{
	struct gc2015_control *ctrl = gc2015_find_control(qc->id);

	if (ctrl == NULL)
		return -EINVAL;
	*qc = ctrl->qc;
	return 0;
}

static int gc2015_g_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct gc2015_control *octrl = gc2015_find_control(ctrl->id);
	int ret;

	if (octrl == NULL)
		return -EINVAL;
	ret = octrl->query(client, &ctrl->value);
	if (ret >= 0)
		return 0;
	return ret;
}

static int gc2015_s_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct gc2015_control *octrl = gc2015_find_control(ctrl->id);
	int ret;

	if (octrl == NULL)
		return -EINVAL;
	ret =  octrl->tweak(client, ctrl->value);
	if (ret >= 0)
		return 0;
	return ret;
}

int ccic_sensor_attach(struct i2c_client *client);

u16 last_reg = 0;
static ssize_t sensor_get_reg(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u8 val = 0;
	struct i2c_client* c = container_of(dev, struct i2c_client, dev);

	if (last_reg)
	{
		gc2015_read(c, last_reg, &val);
		return sprintf(buf, "%x:%x\n", last_reg, val);
	}
	else
	{
		int len=0;
		int index;

		for (index=0; gc2015_regs_defaults[index].reg_num!=0xff; index++)
		{
			gc2015_read(c, gc2015_regs_defaults[index].reg_num, &val);
			len += sprintf(&buf[len], "%02x:%02x%s\n", gc2015_regs_defaults[index].reg_num, val, (val!=gc2015_regs_defaults[index].value)?"*":"");
		}
		return len;
	}
}

static ssize_t sensor_set_reg(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned int reg ,val;
	int ret;
	struct i2c_client* c = container_of(dev, struct i2c_client, dev);

	ret = sscanf(buf, "%x:%x", &reg, &val);
	if (ret == 1)
	{
		last_reg = reg;
		return count;
	}
	else if (ret == 2)
	{
		gc2015_write(c, reg, val);
		last_reg = reg;
		return count;
	}
	else
	{
		last_reg = 0;
		return count;
	}
}

static ssize_t sensor_get_id(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	int ret;

	ret = sprintf(buf, "gc2015");
	
	return ret;
}

static ssize_t sensor_set_div(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	sscanf(buf, "%d", &div);
	printk("div set to %d\n", div);
	return count;
}

static struct device_attribute sensor_static_attrs[] = {
	__ATTR(reg, 0666, sensor_get_reg, sensor_set_reg),
	__ATTR(id, 0666, sensor_get_id, NULL),
	__ATTR(div, 0666, NULL, sensor_set_div),
};

/*
 * Basic i2c stuff.
 */
extern struct clk *pxa168_ccic_gate_clk;
extern void ccic_mclk_set(int on);
static int __devinit gc2015_probe(struct i2c_client *client, const struct i2c_device_id * id)
{
	int i;
	int ret;
	struct gc2015_info *info;
	struct sensor_platform_data *pdata;
	int fm_ctrl;

	pdata = client->dev.platform_data;

	/*
	 * Set up our info structure.
	 */
	info = kzalloc(sizeof (struct gc2015_info), GFP_KERNEL);
	if (! info) {
		ret = -ENOMEM;
		goto out_free;
	}

	//set default format
	info->fmt = &gc2015_formats[0];
	info->sat = 128;	/* Review this */
	i2c_set_clientdata(client, info);
	/*
	 * Make sure it's an gc2015
	 */
	ccic_mclk_set(1);
	pdata->power_set(SENSOR_OPEN, pdata->id, pdata->eco_flag, pdata->sensor_flag);
	ret = gc2015_detect(client);
	pdata->power_set(SENSOR_CLOSE, pdata->id, pdata->eco_flag, pdata->sensor_flag);
	ccic_mclk_set(0);
	if (ret)
	{
		printk("Detecting %s sensor failed.\n", pdata->id==SENSOR_LOW?"low":"high");
		goto out_free_info;
	}

	ret = ccic_sensor_attach(client);
	if (ret)
		goto out_free_info;

	if (pdata->id == SENSOR_LOW)
		fm_ctrl = fm_low;
	else
		fm_ctrl = fm_high;

	printk("gc2015[%s] mirror [%s] flip [%s]\n", 
		dev_name(&client->dev), 
		(fm_ctrl&0x1)?"on":"off", 
		(fm_ctrl&0x2)?"on":"off");

	/* create sys entries */
	for (i=0; i<ARRAY_SIZE(sensor_static_attrs); i++)
	{
		ret = device_create_file(&client->dev, &sensor_static_attrs[i]);
		if(ret)
			goto out_free_info;
	}

	return 0;

out_free_info:
	kfree(info);
out_free:
	return ret;
}


static int gc2015_remove(struct i2c_client *client)
{
	int i;
	
	for (i=0; i<ARRAY_SIZE(sensor_static_attrs); i++)
		device_remove_file(&client->dev, &sensor_static_attrs[i]);

	return 0;
}


static int gc2015_streamon(struct i2c_client *client)
{
	printk("gc2015: stream on\n");
	gc2015_write(client, SOFTWARE_RESET, 0x00);
	gc2015_write(client, OUTPUT_SELECT, 0x0f);
	
	return 0;
}

static int gc2015_streamoff(struct i2c_client *client)
{
	printk("gc2015: stream off\n");
	gc2015_write(client, SOFTWARE_RESET, 0x00);
	gc2015_write(client, OUTPUT_SELECT, 0x00);
	
	return 0;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int gc2015_g_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return gc2015_read(client, (u8)(reg->reg), (u8*)&(reg->val));
}

static int gc2015_s_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return gc2015_write(client, (u8)reg->reg, (u8)reg->val);
}
#endif
static int gc2015_command(struct i2c_client *client, unsigned int cmd,
		void *arg)
{
	//printk("%s, cmd: %d\n", __func__, _IOC_NR(cmd));

	switch (cmd) {
		case VIDIOC_DBG_G_CHIP_IDENT:
			return v4l2_chip_ident_i2c_client(client, arg, V4L2_IDENT_GC2015, 0);

		case VIDIOC_INT_RESET:
			return gc2015_reset(client);
		case VIDIOC_ENUM_FRAMESIZES:
			return gc2015_enum_fmsize(client, (struct v4l2_frmsizeenum *) arg);
		case VIDIOC_ENUM_FMT:
			return gc2015_enum_fmt(client, (struct v4l2_fmtdesc *) arg);
		case VIDIOC_TRY_FMT:
			return gc2015_try_fmt(client, (struct v4l2_format *) arg, NULL, NULL);
		case VIDIOC_S_FMT:
			return gc2015_s_fmt(client, (struct v4l2_format *) arg);
		case VIDIOC_G_FMT:
			return gc2015_g_fmt(client, (struct v4l2_format *) arg);
		case VIDIOC_QUERYCAP:
			return gc2015_querycap(client, (struct v4l2_capability *) arg);
		case VIDIOC_QUERYCTRL:
			return gc2015_queryctrl(client, (struct v4l2_queryctrl *) arg);
		case VIDIOC_S_CTRL:
			return gc2015_s_ctrl(client, (struct v4l2_control *) arg);
		case VIDIOC_G_CTRL:
			return gc2015_g_ctrl(client, (struct v4l2_control *) arg);
		case VIDIOC_S_PARM:
			//return 0;
			return gc2015_s_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_G_PARM:
			return gc2015_g_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_S_INPUT:
			return gc2015_s_input(client, (int *) arg);
		case VIDIOC_STREAMON:
			return gc2015_streamon(client);
		case VIDIOC_STREAMOFF:
			return gc2015_streamoff(client);
#ifdef CONFIG_VIDEO_ADV_DEBUG
		case VIDIOC_DBG_G_REGISTER:
			return gc2015_g_register(client, (struct v4l2_dbg_register *) arg);
		case VIDIOC_DBG_S_REGISTER:
			return gc2015_s_register(client, (struct v4l2_dbg_register *) arg);
#endif
	}
	return -EINVAL;
}

static struct i2c_device_id gc2015_idtable[] = {
	{ "gc2015", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, gc2015_idtable);

static struct i2c_driver gc2015_driver = {
	.driver = {
		.name	= "gc2015",
	},
	.id_table   = gc2015_idtable,
	.command	= gc2015_command,
	.probe		= gc2015_probe,
	.remove		= __devexit_p(gc2015_remove),
};


/*
 * Module initialization
 */
static int __init gc2015_mod_init(void)
{
	printk(KERN_NOTICE "GalaxyCore GC2015 sensor driver, at your service.\n");
	return i2c_add_driver(&gc2015_driver);
}

static void __exit gc2015_mod_exit(void)
{
	i2c_del_driver(&gc2015_driver);
}

module_init(gc2015_mod_init);
module_exit(gc2015_mod_exit);

MODULE_AUTHOR("PENG GUANG <pengguang001@gmail.com>");
MODULE_DESCRIPTION("Aspen168 based driver for GalaxyCore GC2015 sensor");
MODULE_LICENSE("GPL");

