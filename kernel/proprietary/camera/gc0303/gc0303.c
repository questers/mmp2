/*
 * A V4L2 driver for GalaxyCore gc0303 cameras.
 *
 * Copyright 2006 One Laptop Per Child Association, Inc.  Written
 * by Jonathan Corbet with substantial inspiration from Mark
 * McClelland's ovcamchip code.
 *
 * Copyright 2006-7 Jonathan Corbet <corbet@lwn.net>
 *
 * This file may be distributed under the terms of the GNU General
 * Public License, version 2.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/videodev.h>
#include <media/v4l2-common.h>
#include <media/v4l2-chip-ident.h>
#include <linux/i2c.h>
#include <mach/hardware.h>
#include <mach/camera.h>
#include <mach/mfp.h>

#include <linux/clk.h>
#include <mach/pxa910.h>
#include "pxa688_camera.h"

#define REG_FRAME_CONTROL 0x9f
#define GC0303_PID 0x80
#define DEFAULT_PID 0x11

/*
 * Basic window sizes.  These probably belong somewhere more globally
 * useful.
 */

#define QXGA_WIDTH	2048
#define QXGA_HEIGHT	1536

#define UXGA_WIDTH	1600
#define UXGA_HEIGHT 1200

#define SXGA_WIDTH	1280
#define SXGA_HEIGHT 1024

#define XGA_WIDTH	1024
#define XGA_HEIGHT  768

#define VGA_WIDTH	640
#define VGA_HEIGHT	480

#define QVGA_WIDTH	320
#define QVGA_HEIGHT	240

#define CIF_WIDTH	352
#define CIF_HEIGHT	288

#define QCIF_WIDTH	176
#define	QCIF_HEIGHT	144

#define COM7_FMT_VGA 0
#define COM7_FMT_QVGA 0
#define COM7_FMT_VGA 0

#define REG_CLKRC 0x3011
#define CLK_EXT 0x40
#define CLK_SCALE 0x3f

/*
 * Information we maintain about a known sensor.
 */
struct gc0303_format_struct;  /* coming later */
struct gc0303_info {
	struct gc0303_format_struct *fmt;  /* Current format */
	unsigned char sat;		/* Saturation value */
	int hue;			/* Hue value */
};

/*
 * The default register settings, as obtained from GalaxyCore.  There
 * is really no making sense of most of these - lots of "reserved" values
 * and such.
 *
 * These settings give VGA YUYV.
 */

struct regval_list {
	unsigned char reg_num;
	unsigned char value;
};

static struct regval_list gc0303_fmt_bayer_sgbrg_vga[] = 
{
	/* empty */
	{0xff, 0xff},
};

/* default setting */
static struct regval_list gc0303_default_regs[] =
{
	{0x9c, 0x17},
	{0x9d, 0x80},
	{0x9f, 0x89},
	{0xa0, 0x00},
	
	{0x9e, 0x08}, /* pclk = mclk */
	
	{0x85, 0x00},
	{0x9a, 0x02},
	
	{0x86, 0x00},
	{0x87, 0x00},
	{0x88, 0x00},
	{0x89, 0x00},
	
	{0x8a, 0x01},
	{0x8b, 0x80},
	{0x8c, 0x01},
	{0x8d, 0x80},
	{0x8e, 0x01},
	{0x8f, 0x80},
	{0x90, 0x01},
	{0x91, 0x30},
	
	{0x92, 0x00}, /* win size: 640x480 */
	{0x93, 0x00},
	{0x94, 0x00},
	{0x95, 0x00},
	{0x96, 0x01},
	{0x97, 0xe0},
	{0x98, 0x02},
	{0x99, 0x80},
	
	{0x83, 0x01}, /* exposure */
	{0x84, 0x06},
	
	{0x9b, 0x20}, /* power on */

	{0xff, 0xff},
};

/*
 * Store information about the video data format.  The color matrix
 * is deeply tied into the format, so keep the relevant values here.
 * The magic matrix nubmers come from GalaxyCore.
 */
static struct gc0303_format_struct {
	__u8 *desc;
	__u32 pixelformat;
	struct regval_list *regs;
	int bpp;   /* bits per pixel */
} gc0303_formats[] =
{
	{
		.desc		= "Bayer GBRG",
		.pixelformat	= V4L2_PIX_FMT_SGBRG8,
		.regs = NULL,
		.bpp		= 8,
	},
};
#define N_GC0303_FMTS ARRAY_SIZE(gc0303_formats)


static struct gc0303_win_size {
	int	width;
	int	height;
} gc0303_win_sizes[] = {
	/* VGA */
	{
		.width		= VGA_WIDTH,
		.height		= VGA_HEIGHT,
	},
};

#define N_GC0303_WIN_SIZES (ARRAY_SIZE(gc0303_win_sizes))

/*
 * flip and mirror control
 * 0: normal
 * 1: mirror on
 * 2: flip on
 * 3: flip and mirror on
 */
static int fm_low = 0;
module_param(fm_low, int, 0);
static int fm_high = 0;
module_param(fm_high, int, 0);

/*
 * Low-level register I/O.
 */

static int gc0303_read(struct i2c_client *c, u8 reg, u8* value)
{
	int ret = i2c_smbus_read_byte_data(c, reg);
	if (ret < 0)
	{
		return ret;
	}
	else
	{
		*value = (u8) ret;
		return 0;
	}
}

static int gc0303_write(struct i2c_client *c, u8 reg, u8 value)
{
	return i2c_smbus_write_byte_data(c, reg, value);
}

/*
 * Write a list of register settings; ff/ff stops the process.
 */
static int gc0303_write_array(struct i2c_client *c, struct regval_list *vals)
{
	while (vals->reg_num != 0xff || vals->value != 0xff) {
		int ret = gc0303_write(c, vals->reg_num, vals->value);
		if (ret < 0)
			return ret;
		vals++;
	}

	return 0;
}


/*
 * Stuff that knows about the sensor.
 */
static void gc0303_reset(struct i2c_client *client)
{
	/* soft reset, nothing done */
}

static int gc0303_detect(struct i2c_client *client)
{
	u8 v=0;
	int ret;

	ret = gc0303_read(client, GC0303_PID, &v);
	printk("gc0303_read returned %d\n", ret);
	if (ret < 0)
		return ret;

	if (v == DEFAULT_PID)
	{
		printk(KERN_NOTICE "GalaxyCore gc0303 sensor detected\n");
		return 0;
	}
	else
	{
		printk(KERN_ERR "Detected unknown sensor with PID: 0x%2x\n", v);
		return -ENODEV;
	}
}

static int gc0303_enum_fmsize(struct i2c_client *client, struct v4l2_frmsizeenum *argp)
{
	struct v4l2_frmsizeenum frmsize;

	if (copy_from_user(&frmsize, argp, sizeof(frmsize)))
		return -EFAULT;

	switch (frmsize.pixel_format) {
		case V4L2_PIX_FMT_SGBRG8:
			if (frmsize.index == ARRAY_SIZE(gc0303_win_sizes))
				return -EINVAL;
			else if (frmsize.index > ARRAY_SIZE(gc0303_win_sizes)) {
				printk(KERN_ERR "camera: gc0303 enum unsupported preview format size!\n");
				return -EINVAL;
			} else {
				frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
				frmsize.discrete.height = gc0303_win_sizes[frmsize.index].height;
				frmsize.discrete.width = gc0303_win_sizes[frmsize.index].width;
				break;
			}
		default:
			printk(KERN_ERR "camera: gc0303 enum format size with unsupported format!\n");
			return -EINVAL;
	}

	if (copy_to_user(argp, &frmsize, sizeof(frmsize)))
		return -EFAULT;
	
	return 0;
}

static int gc0303_enum_fmt(struct i2c_client *c, struct v4l2_fmtdesc *fmt)
{
	struct gc0303_format_struct *ofmt;

	if (fmt->index >= N_GC0303_FMTS)
		return -EINVAL;

	ofmt = gc0303_formats + fmt->index;
	fmt->flags = 0;
	strcpy(fmt->description, ofmt->desc);
	fmt->pixelformat = ofmt->pixelformat;
	return 0;
}


static int gc0303_try_fmt(struct i2c_client *c, struct v4l2_format *fmt,
		struct gc0303_format_struct **ret_fmt,
		struct gc0303_win_size **ret_wsize)
{
	int index;
	struct gc0303_win_size *wsize;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	for (index = 0; index < N_GC0303_FMTS; index++)
		if (gc0303_formats[index].pixelformat == pix->pixelformat)
			break;
	if (index >= N_GC0303_FMTS){
		printk("unsupported format[%d]!\n", pix->pixelformat);
		return -EINVAL;
	}
	if (ret_fmt != NULL)
		*ret_fmt = gc0303_formats + index;
	/*
	 * Fields: the OV devices claim to be progressive.
	 */
	if (pix->field == V4L2_FIELD_ANY)
		pix->field = V4L2_FIELD_NONE;
	else if (pix->field != V4L2_FIELD_NONE)
	{
		printk("invalid field\n");
		//return -EINVAL; /* don't return, for mpt. -guang */
	}
	/*
	 * Round requested image size down to the nearest
	 * we support, but not below the smallest.
	 */
	for (wsize = gc0303_win_sizes; wsize < gc0303_win_sizes + N_GC0303_WIN_SIZES;
			wsize++)
		if (pix->width <= wsize->width && pix->height <= wsize->height)
			break;
	if (wsize >= gc0303_win_sizes + N_GC0303_WIN_SIZES){
		printk("size exceed and set as QXGA!\n");
		wsize--;   /* Take the smallest one */
	}
	if (ret_wsize != NULL)
		*ret_wsize = wsize;
	/*
	 * Note the size we'll actually handle.
	 */
	pix->bytesperline = pix->width*gc0303_formats[index].bpp/8;
	pix->sizeimage = pix->height*pix->bytesperline;

	if (ret_fmt == NULL)
		return 0;
	
	switch (pix->pixelformat)
	{
		case V4L2_PIX_FMT_SGBRG8:
			switch (wsize->width)
			{
				case VGA_WIDTH:
					(*ret_fmt)->regs = gc0303_fmt_bayer_sgbrg_vga;
					break;
			}
			break;
		default:
			printk("unsupported format - %d!\n", pix->pixelformat);
			return -EINVAL;
	}
	return 0;
}

/*
 * Set a format.
 */
static int gc0303_s_fmt(struct i2c_client *c, struct v4l2_format *fmt)
{
	int ret;
	u8 fm;
	struct gc0303_format_struct *ovfmt;
	struct gc0303_win_size *wsize;
	int fm_ctrl;
	struct sensor_platform_data *pdata = c->dev.platform_data;

	ret = gc0303_try_fmt(c, fmt, &ovfmt, &wsize);
	if (ret)
		return ret;

	gc0303_write_array(c, gc0303_default_regs);
	gc0303_write_array(c, ovfmt->regs);

	/* flip and mirror */
	if (pdata->id == SENSOR_LOW)
		fm_ctrl = fm_low;
	else
		fm_ctrl = fm_high;
	fm_ctrl &= 0x3;
	
	gc0303_read(c, REG_FRAME_CONTROL, &fm);
	fm &= 0x3f;
	if (fm_ctrl & 0x1)
		fm_ctrl |= 0x80;
	else
		fm_ctrl &= ~0x80;
	if (fm_ctrl & 0x2)
		fm_ctrl |= 0x40;
	else
		fm_ctrl &= ~0x40;
	gc0303_write(c, REG_FRAME_CONTROL, fm);

	return ret;
}

/*
 * Get a format.
 */
static int gc0303_g_fmt(struct i2c_client *c, struct v4l2_format *fmt)
{
	return 0;
}

static int gc0303_g_parm(struct i2c_client *c, struct v4l2_streamparm *parms)
{
	return 0;
}

static int gc0303_s_parm(struct i2c_client *c, struct v4l2_streamparm *parms)
{
	return 0;
}

static int gc0303_s_input(struct i2c_client *c, int *id)
{
	return 0;
}

static int gc0303_querycap(struct i2c_client *client,
		struct v4l2_capability *cap)
{
	strcpy(cap->driver, "gc0308");

	return 0;
}

int ccic_sensor_attach(struct i2c_client *client);

u16 last_reg = 0;
static ssize_t sensor_get_reg(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u8 val = 0;
	struct i2c_client* c = container_of(dev, struct i2c_client, dev);

	if (last_reg)
	{
		gc0303_read(c, last_reg, &val);
		return sprintf(buf, "%x:%x\n", last_reg, val);
	}
	else
	{
		int len=0;
		int index;

		for (index=0; gc0303_default_regs[index].reg_num!=0xff; index++)
		{
			gc0303_read(c, gc0303_default_regs[index].reg_num, &val);
			len += sprintf(&buf[len], "%02x:%02x%s\n", gc0303_default_regs[index].reg_num, val, (val!=gc0303_default_regs[index].value)?"*":"");
		}
		return len;
	}
}

static ssize_t sensor_set_reg(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned int reg ,val;
	int ret;
	struct i2c_client* c = container_of(dev, struct i2c_client, dev);

	ret = sscanf(buf, "%x:%x", &reg, &val);
	if (ret == 1)
	{
		last_reg = reg;
		return count;
	}
	else if (ret == 2)
	{
		gc0303_write(c, reg, val);
		last_reg = reg;
		return count;
	}
	else
	{
		last_reg = 0;
		return count;
	}
}

static ssize_t sensor_get_id(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	int ret;

	ret = sprintf(buf, "gc0303");
	
	return ret;
}

static struct device_attribute sensor_static_attrs[] = {
	__ATTR(reg, 0666, sensor_get_reg, sensor_set_reg),
	__ATTR(id, 0666, sensor_get_id, NULL),
};

/*
 * Basic i2c stuff.
 */
extern struct clk *pxa168_ccic_gate_clk;
extern void ccic_mclk_set(int on);
static int __devinit gc0303_probe(struct i2c_client *client, const struct i2c_device_id * id)
{
	int i;
	int ret;
	struct gc0303_info *info;
	struct sensor_platform_data *pdata;
	int fm_ctrl;

	pdata = client->dev.platform_data;

	/*
	 * Set up our info structure.
	 */
	info = kzalloc(sizeof (struct gc0303_info), GFP_KERNEL);
	if (! info) {
		ret = -ENOMEM;
		goto out_free;
	}

	//set default format
	info->fmt = &gc0303_formats[0];
	info->sat = 128;	/* Review this */
	i2c_set_clientdata(client, info);
	/*
	 * Make sure it's an gc0303
	 */
	ccic_mclk_set(1);
	pdata->power_set(SENSOR_OPEN, pdata->id, pdata->eco_flag, pdata->sensor_flag);
	ret = gc0303_detect(client);
	pdata->power_set(SENSOR_CLOSE, pdata->id, pdata->eco_flag,pdata->sensor_flag);
	ccic_mclk_set(0);
	if (ret)
		goto out_free_info;

	ret = ccic_sensor_attach(client);
	if (ret)
		goto out_free_info;

	if (pdata->id == SENSOR_LOW)
		fm_ctrl = fm_low;
	else
		fm_ctrl = fm_high;

	printk("gc0303[%s] mirror [%s] flip [%s]\n", 
		dev_name(&client->dev), 
		(fm_ctrl&0x1)?"on":"off", 
		(fm_ctrl&0x2)?"on":"off");

	/* create sys entries */
	for (i=0; i<ARRAY_SIZE(sensor_static_attrs); i++)
	{
		ret = device_create_file(&client->dev, &sensor_static_attrs[i]);
		if(ret)
			goto out_free_info;
	}

	return 0;

out_free_info:
	kfree(info);
out_free:
	return ret;
}


static int gc0303_remove(struct i2c_client *client)
{
	int i;
	
	for (i=0; i<ARRAY_SIZE(sensor_static_attrs); i++)
		device_remove_file(&client->dev, &sensor_static_attrs[i]);

	return 0;
}


static int gc0303_streamon(struct i2c_client *client)
{
	return 0;
}

static int gc0303_streamoff(struct i2c_client *client)
{
	return 0;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int gc0303_g_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return gc0303_read(client, (u16)reg->reg, (unsigned char *)&(reg->val));
}

static int gc0303_s_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return gc0303_write(client, (u16)reg->reg, (unsigned char)reg->val);
}
#endif
static int gc0303_command(struct i2c_client *client, unsigned int cmd,
		void *arg)
{
	switch (cmd) {
		case VIDIOC_DBG_G_CHIP_IDENT:
			return v4l2_chip_ident_i2c_client(client, arg, V4L2_IDENT_GC0303, 0);

		case VIDIOC_INT_RESET:
			gc0303_reset(client);
			return 0;

		/*
		case VIDIOC_INT_INIT:
			gc0303_init(client);
			return 0;
		*/
		case VIDIOC_ENUM_FRAMESIZES:
			return gc0303_enum_fmsize(client, (struct v4l2_frmsizeenum *) arg);
		case VIDIOC_ENUM_FMT:
			return gc0303_enum_fmt(client, (struct v4l2_fmtdesc *) arg);
		case VIDIOC_TRY_FMT:
			return gc0303_try_fmt(client, (struct v4l2_format *) arg, NULL, NULL);
		case VIDIOC_S_FMT:
			return gc0303_s_fmt(client, (struct v4l2_format *) arg);
		case VIDIOC_G_FMT:
			return gc0303_g_fmt(client, (struct v4l2_format *) arg);
		case VIDIOC_QUERYCAP:
			return gc0303_querycap(client, (struct v4l2_capability *) arg);
		case VIDIOC_S_PARM:
			//return 0;
			return gc0303_s_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_G_PARM:
			return gc0303_g_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_S_INPUT:
			return gc0303_s_input(client, (int *) arg);
		case VIDIOC_STREAMON:
			return gc0303_streamon(client);
		case VIDIOC_STREAMOFF:
			return gc0303_streamoff(client);
#ifdef CONFIG_VIDEO_ADV_DEBUG
		case VIDIOC_DBG_G_REGISTER:
			return gc0303_g_register(client, (struct v4l2_dbg_register *) arg);
		case VIDIOC_DBG_S_REGISTER:
			return gc0303_s_register(client, (struct v4l2_dbg_register *) arg);
#endif
	}
	return -EINVAL;
}

static struct i2c_device_id gc0303_idtable[] = {
	{ "gc0303", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, gc0303_idtable);

static struct i2c_driver gc0303_driver = {
	.driver = {
		.name	= "gc0303",
	},
	.id_table   = gc0303_idtable,
	.command	= gc0303_command,
	.probe		= gc0303_probe,
	.remove		= __devexit_p(gc0303_remove),
};


/*
 * Module initialization
 */
static int __init gc0303_mod_init(void)
{
	printk(KERN_NOTICE "GalaxyCore GC0303 sensor driver registered.\n");
	return i2c_add_driver(&gc0303_driver);
}

static void __exit gc0303_mod_exit(void)
{
	i2c_del_driver(&gc0303_driver);
}

module_init(gc0303_mod_init);
module_exit(gc0303_mod_exit);

MODULE_AUTHOR("PENG GUANG <pengguang001@gmail.com>");
MODULE_DESCRIPTION("Aspen168 based driver for GalaxyCore GC0303 sensor");
MODULE_LICENSE("GPL");

