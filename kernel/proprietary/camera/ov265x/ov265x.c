/*
 * A V4L2 driver for OmniVision ov2650/ov2655 cameras.
 *
 * Copyright 2006 One Laptop Per Child Association, Inc.  Written
 * by Jonathan Corbet with substantial inspiration from Mark
 * McClelland's ovcamchip code.
 *
 * Copyright 2006-7 Jonathan Corbet <corbet@lwn.net>
 *
 * This file may be distributed under the terms of the GNU General
 * Public License, version 2.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/videodev.h>
#include <media/v4l2-common.h>
#include <media/v4l2-chip-ident.h>
#include <linux/i2c.h>
#include <mach/hardware.h>
#include <mach/camera.h>
#include <mach/mfp.h>

#include <linux/clk.h>
#include <mach/pxa910.h>
#include "pxa688_camera.h"
#include "ov265x.h"

#define OV2650_FRAME_RATE 30


/*
 * Basic window sizes.  These probably belong somewhere more globally
 * useful.
 */

#define QXGA_WIDTH	2048
#define QXGA_HEIGHT	1536

#define UXGA_WIDTH	1600
#define UXGA_HEIGHT 1200

#define SXGA_WIDTH	1280
#define SXGA_HEIGHT 1024

#define XGA_WIDTH	1024
#define XGA_HEIGHT  768

#define VGA_WIDTH	640
#define VGA_HEIGHT	480

#define QVGA_WIDTH	320
#define QVGA_HEIGHT	240

#define CIF_WIDTH	352
#define CIF_HEIGHT	288

#define QCIF_WIDTH	176
#define	QCIF_HEIGHT	144

#define COM7_FMT_VGA 0
#define COM7_FMT_QVGA 0
#define COM7_FMT_VGA 0

#define REG_CLKRC 0x3011
#define CLK_EXT 0x40
#define CLK_SCALE 0x3f

#define FLIP_MIRROR     0x307c
#define ARRAY_MIRROR    0x3090

/*
 * Information we maintain about a known sensor.
 */
struct ov2650_format_struct;  /* coming later */
struct ov2650_info {
	struct ov2650_format_struct *fmt;  /* Current format */
	unsigned char sat;		/* Saturation value */
	int hue;			/* Hue value */
};

/*
 * The default register settings, as obtained from OmniVision.  There
 * is really no making sense of most of these - lots of "reserved" values
 * and such.
 *
 * These settings give VGA YUYV.
 */

struct regval_list {
	u16 reg_num;
	unsigned char value;
};


/*
 * Here we'll try to encapsulate the changes for just the output
 * video format.
 *
 * RGB656 and YUV422 come from OV; RGB444 is homebrewed.
 *
 * IMPORTANT RULE: the first entry must be for COM7, see ov2650_s_fmt for why.
 */

/* yuv422 1600*1200 */
static struct regval_list ov2650_fmt_yuv422_uxga[] = 
{
	// @@ 1600x1200 YUV 5FPS
	// ;2655 Rev1A reference setting 06232008
	// ;24MHz 		;5FPS
	// ;1280x1024	;YUV	output 
	// ;HREF		positive
	// ;Vsync		positive
	// ;AEC;		Auto;	0x3013[0]=1
	// ;AGC; 		Auto;	0x3013[2]=1	8x ceiling; 	0x3015[2:0]=011
	// ;Banding Filter;Auto;	0x3013[5]=1	
	// ;50/60 Auto detection	ON	0x3014[6]=1
	// ;Night mode 	off;	0x3014[3]=0
	// ;AWB; 		ON;	0x3300[5:4]=11
	// ;LC 		ON;	0x3300[3:2]=11	
	// ;UVadjust 	ON;	0x3301[6]=1
	// ;WBC 		ON;	0x3301[1:0]=11
	// ;UV average	ON;	0x3302[1:0]=01
	// ;Scaling	ON;	0x3302[4]=0
	// ;DNS 		Auto;	0x3306[2]=0  
	// ;Sharpness 	Auto; 	0x3306[3]=0

	// ;Soft Reset
	{0x3012, 0x80}, 
	// ;Add some dealy or wait a few miliseconds after register reset

	// ;IO & Clock & Analog Setup
	{0x308c, 0x80},
	{0x308d, 0x0e},
	{0x360b, 0x00},
	{0x30b0, 0xff},
	{0x30b1, 0xff},
	{0x30b2, 0x04},

	{0x300e, 0x34},
	{0x300f, 0xa6},
	{0x3010, 0x81},
	{0x3082, 0x01},
	{0x30f4, 0x01},
	{0x3090, 0x43},
	{0x3091, 0xc0},
	{0x30ac, 0x42},

	{0x30d1, 0x08},
	{0x30a8, 0x55},
	{0x3015, 0x02},
	{0x3093, 0x00},
	{0x307e, 0xe5},
	{0x3079, 0x00},
	{0x30aa, 0x42},
	{0x3017, 0x40},
	{0x30f3, 0x83},
	{0x306a, 0x0c},
	{0x306d, 0x00},
	{0x336a, 0x3c},
	{0x3076, 0x6a},
	{0x30d9, 0x95},
	{0x3016, 0x82},
	{0x3601, 0x30},
	{0x304e, 0x88},
	{0x30f1, 0x82},

	{0x3011, 0x00},	/* PCLK=XCLK/(n+1) */

	// ;AEC/AGC
	{0x3013, 0xf7},
	{0x3018, 0x70},
	{0x3019, 0x60},
	{0x301a, 0xd4},
	{0x301c, 0x13},
	{0x301d, 0x17},
	{0x3070, 0x3e},
	{0x3072, 0x34},

	// ;D5060
	{0x30af, 0x00},
	{0x3048, 0x1f}, 
	{0x3049, 0x4e},  
	{0x304a, 0x20},  
	{0x304f, 0x20},  
	{0x304b, 0x02}, 
	{0x304c, 0x00},  
	{0x304d, 0x02},  
	{0x304f, 0x20},  
	{0x30a3, 0x10},  
	{0x3013, 0xf7}, 
	{0x3014, 0x44},  
	{0x3071, 0x00},
	{0x3070, 0x3e},
	{0x3073, 0x00},
	{0x3072, 0x34},
	{0x301c, 0x12},
	{0x301d, 0x16}, 
	{0x304d, 0x42},     
	{0x304a, 0x40},  
	{0x304f, 0x40},  
	{0x3095, 0x07},  
	{0x3096, 0x16}, 
	{0x3097, 0x1d}, 

	// ;Window Setup
	{0x3020, 0x01},
	{0x3021, 0x18},
	{0x3022, 0x00},
	{0x3023, 0x0a},
	{0x3024, 0x06},
	{0x3025, 0x58},
	{0x3026, 0x04},
	{0x3027, 0xbc},
	{0x3088, 0x06},
	{0x3089, 0x40},
	{0x308a, 0x04},
	{0x308b, 0xb0},
	{0x3316, 0x64},
	{0x3317, 0x4b},
	{0x3318, 0x00},
	{0x331a, 0x64},
	{0x331b, 0x4b},
	{0x331c, 0x00},
	{0x3100, 0x00},

	// ;AWB
	{0x3320, 0xfa},
	{0x3321, 0x11},  
	{0x3322, 0x92},  
	{0x3323, 0x01},   
	{0x3324, 0x97},  
	{0x3325, 0x02},   
	{0x3326, 0xff},  
	{0x3327, 0x10},   
	{0x3328, 0x10},  
	{0x3329, 0x1f},  
	{0x332a, 0x58},  
	{0x332b, 0x50},  
	{0x332c, 0xbe},  
	{0x332d, 0xce},  
	{0x332e, 0x2e},  
	{0x332f, 0x36},  
	{0x3330, 0x4d},  
	{0x3331, 0x44},  
	{0x3332, 0xf0},  
	{0x3333, 0x0a},   
	{0x3334, 0xf0},  
	{0x3335, 0xf0},  
	{0x3336, 0xf0},  
	{0x3337, 0x40},  
	{0x3338, 0x40},  
	{0x3339, 0x40},  
	{0x333a, 0x00},   
	{0x333b, 0x00}, 

	// ;Color Matrix
	{0x3380, 0x20},  
	{0x3381, 0x5b},   
	{0x3382, 0x05},  
	{0x3383, 0x22}, 
	{0x3384, 0x9d},
	{0x3385, 0xc0},   
	{0x3386, 0xb6},  
	{0x3387, 0xb5},  
	{0x3388, 0x02},  
	{0x3389, 0x98}, 
	{0x338a, 0x00},

	// ;Gamma
	{0x3340, 0x09},
	{0x3341, 0x19},
	{0x3342, 0x2f},
	{0x3343, 0x45},
	{0x3344, 0x5a},
	{0x3345, 0x69},
	{0x3346, 0x75},
	{0x3347, 0x7e},
	{0x3348, 0x88},
	{0x3349, 0x96},
	{0x334a, 0xa3},
	{0x334b, 0xaf},
	{0x334c, 0xc4},
	{0x334d, 0xd7},
	{0x334e, 0xe8},
	{0x334f, 0x20},

	// ;Lens correction
	{0x3350, 0x32},
	{0x3351, 0x25},  
	{0x3352, 0x80},  
	{0x3353, 0x1e},  
	{0x3354, 0x00},  
	{0x3355, 0x84},  
	{0x3356, 0x32},   
	{0x3357, 0x25},   
	{0x3358, 0x80},   
	{0x3359, 0x1b},   
	{0x335a, 0x00},  
	{0x335b, 0x84},   
	{0x335c, 0x32},   
	{0x335d, 0x25},   
	{0x335e, 0x80},   
	{0x335f, 0x1b},   
	{0x3360, 0x00},  
	{0x3361, 0x84},   
	{0x3363, 0x70},   
	{0x3364, 0x7f},   
	{0x3365, 0x00},  
	{0x3366, 0x00},  

	// ;UVadjust
	{0x3301, 0xff},
	{0x338B, 0x1b},
	{0x338c, 0x1f},
	{0x338d, 0x40},

	// ;Sharpness/De-noise
	{0x3370, 0xd0},
	{0x3371, 0x00},
	{0x3372, 0x00},
	{0x3373, 0x40},
	{0x3374, 0x10},
	{0x3375, 0x10},
	{0x3376, 0x04},
	{0x3377, 0x00},
	{0x3378, 0x04},
	{0x3379, 0x80},

	// ;BLC
	{0x3069, 0x86},
	{0x307c, 0x10},
	{0x3087, 0x02},

	// ;black sun 
	// ;Avdd 2.55~3.0V
	{0x3090, 0x03},
	{0x30aa, 0x32},
	{0x30a3, 0x80},
	{0x30a1, 0x41},
	// ;Avdd 2.45~2.7V
	// ;60 3090 03
	// ;60 30aa 22
	// ;60 30a3 80
	// ;60 30a1 41

	// ;Other functions
	{0x3300, 0xfc},
	{0x3302, 0x01},
	{0x3400, 0x02},
	{0x3606, 0x20},
	{0x3601, 0x30},
	{0x300e, 0x34},
	{0x30f3, 0x83},
	{0x304e, 0x88},
	{0x363b, 0x01},
	{0x363c, 0xf2},

	{0x3023, 0x0c},
	{0x3319, 0x4c},

	{0x3086, 0x0f},
	{0x3086, 0x00},

	{0xffff, 0xff},	/* end mark */
}; 

/* yuv422 1280x1024 */
static struct regval_list ov2650_fmt_yuv422_sxga[] = 
{
	/* 2650 parameters from ov */
	{0x3012,0x00},
	{0x302a,0x04},
	{0x302b,0xd4},
	{0x306f,0x54},
	{0x3362,0x80},
	
	{0x3070,0x9a},
	{0x3072,0x9a},
	{0x301c,0x07},
	{0x301d,0x07},
	
	{0x3020,0x01},
	{0x3021,0x18},
	{0x3022,0x00},
	{0x3023,0x0A},
	{0x3024,0x06},
	{0x3025,0x58},
	{0x3026,0x04},
	{0x3027,0xbc},
	{0x3088,0x05},
	{0x3089,0x00},
	{0x308A,0x04},
	{0x308B,0x00},
	{0x3316,0x64},
	{0x3317,0x4B},
	{0x3318,0x00},
	{0x3319,0x6C},
	{0x331A,0x50},
	{0x331B,0x40},
	{0x331C,0x00},
	{0x331D,0x6C},
	{0x3302,0x11},

	{0xffff,0xff},	/* end mark */
}; 

/* yuv422 640x480 */
static struct regval_list ov2650_fmt_yuv422_vga[] = 
{
	/* 2650 parameters from ov, yuv422, 640*480 */
	
	// Soft Reset
	{0x3012,0x80},
	// Add some dealy or wait a few miliseconds after register reset
	
	// IO & Clock & Analog Setup
	{0x308c,0x80},
	{0x308d,0x0e},
	{0x360b,0x00},
	{0x30b0,0xff},
	{0x30b1,0xff},
	{0x30b2,0x24},
	
	{0x300e,0x34},
	{0x300f,0xa6},
	{0x3010,0x81},
	{0x3082,0x01},
	{0x30f4,0x01},
	{0x3090,0x33},
	{0x3091,0xc0},
	{0x30ac,0x42},
	
	{0x30d1,0x08},
	{0x30a8,0x56},
	{0x3015,0x03},
	{0x3093,0x00},
	{0x307e,0xe5},
	{0x3079,0x00},
	{0x30aa,0x42},
	{0x3017,0x40},
	{0x30f3,0x82},
	{0x306a,0x0c},
	{0x306d,0x00},
	{0x336a,0x3c},
	{0x3076,0x6a},
	{0x30d9,0x8c},
	{0x3016,0x82},
	{0x3601,0x30},
	{0x304e,0x88},
	{0x30f1,0x82},
	{0x306f,0x14},
	
	{0x3012,0x10},
	{0x3011,0x00},	/* PCLK=XCLK/(n+1) */
	{0x302A,0x02},
	{0x302B,0x6a},
	{0x3028,0x07},
	{0x3029,0x93},
	
	// saturation
	{0x3391,0x06},
	{0x3394,0x38},
	{0x3395,0x38},
	
	// auto frame
	{0x3015,0x22},
	{0x302d,0x00},
	{0x302e,0x00},
	
	// AEC/AGC
	{0x3013,0xf7},
	{0x3018,0x78},
	{0x3019,0x68},
	{0x301a,0xd4},
	
	// D5060
	{0x30af,0x00},
	{0x3048,0x1f},
	{0x3049,0x4e},
	{0x304a,0x20},
	{0x304f,0x20},
	{0x304b,0x02},
	{0x304c,0x00},
	{0x304d,0x02},
	{0x304f,0x20},
	{0x30a3,0x10},
	{0x3013,0xf7},
	{0x3014,0x8c},
	{0x3071,0x00},
	{0x3070,0x9a},
	{0x3073,0x00},
	{0x3072,0x9a},
	{0x301c,0x03},
	{0x301d,0x03},
	{0x304d,0x42},
	{0x304a,0x40},
	{0x304f,0x40},
	{0x3095,0x07},
	{0x3096,0x16},
	{0x3097,0x1d},
	
	// Window Setup
	{0x3020,0x01},
	{0x3021,0x18},
	{0x3022,0x00},
	{0x3023,0x06},
	{0x3024,0x06},
	{0x3025,0x58},
	{0x3026,0x02},
	{0x3027,0x61},
	{0x3088,0x02},
	{0x3089,0x80},
	{0x308a,0x01},
	{0x308b,0xe0},
	{0x3316,0x64},
	{0x3317,0x25},
	{0x3318,0x80},
	{0x3319,0x08},
	{0x331a,0x28},
	{0x331b,0x1e},
	{0x331c,0x00},
	{0x331d,0x38},
	{0x3100,0x00},
	
	// AWB
	{0x3320,0xfa},
	{0x3321,0x11},
	{0x3322,0x92},
	{0x3323,0x01},
	{0x3324,0x97},
	{0x3325,0x02},
	{0x3326,0xff},
	{0x3327,0x0c},
	{0x3328,0x10},
	{0x3329,0x10},
	{0x332a,0x58},
	{0x332b,0x56},
	{0x332c,0xbe},
	{0x332d,0xe1},
	{0x332e,0x3a},
	{0x332f,0x38},
	{0x3330,0x4d},
	{0x3331,0x44},
	{0x3332,0xf8},
	{0x3333,0x0a},
	{0x3334,0xf0},
	{0x3335,0xf0},
	{0x3336,0xf0},
	{0x3337,0x40},
	{0x3338,0x40},
	{0x3339,0x40},
	{0x333a,0x00},
	{0x333b,0x00},
	
	// Color Matrix
	{0x3380,0x28},
	{0x3381,0x48},
	{0x3382,0x10},
	{0x3383,0x22},
	{0x3384,0xc0},
	{0x3385,0xe2},
	{0x3386,0xe2},
	{0x3387,0xf2},
	{0x3388,0x10},
	{0x3389,0x98},
	{0x338a,0x00},
	
	// Gamma
	{0x3340,0x04},
	{0x3341,0x07},
	{0x3342,0x19},
	{0x3343,0x34},
	{0x3344,0x4a},
	{0x3345,0x5a},
	{0x3346,0x67},
	{0x3347,0x71},
	{0x3348,0x7c},
	{0x3349,0x8c},
	{0x334a,0x9b},
	{0x334b,0xa9},
	{0x334c,0xc0},
	{0x334d,0xd5},
	{0x334e,0xe8},
	{0x334f,0x20},
	
	// Lens correction
	{0x3350,0x33},
	{0x3351,0x28},
	{0x3352,0x00},
	{0x3353,0x14},
	{0x3354,0x00},
	{0x3355,0x85},
	{0x3356,0x35},
	{0x3357,0x28},
	{0x3358,0x00},
	{0x3359,0x13},
	{0x335a,0x00},
	{0x335b,0x85},
	{0x335c,0x34},
	{0x335d,0x28},
	{0x335e,0x00},
	{0x335f,0x13},
	{0x3360,0x00},
	{0x3361,0x85},
	{0x3363,0x70},
	{0x3364,0x7f},
	{0x3365,0x00},
	{0x3366,0x00},
	
	{0x3362,0x90},
	
	// UVadjust
	{0x3301,0xff},
	{0x338B,0x14},
	{0x338c,0x10},
	{0x338d,0x40},
	
	// Sharpness/De-noise
	{0x3370,0xd0},
	{0x3371,0x00},
	{0x3372,0x00},
	{0x3373,0x30},
	{0x3374,0x10},
	{0x3375,0x10},
	{0x3376,0x07},
	{0x3377,0x00},
	{0x3378,0x04},
	{0x3379,0x80},
	
	// BLC
	{0x3069,0x84},
	{0x307c,0x10},
	{0x3087,0x02},
	
	// Other functions
	{0x3300,0xfc},
	{0x3302,0x11},
//	{0x3308,0x01},  /* enable color bar. -guang */
	{0x3400,0x02},	/* a must: YUV422 format */
	{0x3606,0x20},
	{0x3601,0x30},
	{0x30f3,0x83},
	{0x304e,0x88},
	
	{0x30aa,0x72},
	{0x30a3,0x80},
	{0x30a1,0x41},
	{0x3086,0x0f},
	{0x3086,0x00},

	{0xffff,0xff},	/* end mark */
}; 

/* 352x288 */
static struct regval_list ov2650_fmt_yuv422_cif[] = 
{
	// 352*288, yuv422
	
	// ;Soft Reset
	{0x3012, 0x80}, 
	// ;Add some dealy or wait a few miliseconds after register reset
	
	// ;IO & Clock & Analog Setup
	{0x308c, 0x80},
	{0x308d, 0x0e},
	{0x360b, 0x00},
	{0x30b0, 0xff},
	{0x30b1, 0xff},
	{0x30b2, 0x24},
	
	{0x3085, 0x20},
	
	{0x300e, 0x34},
	{0x300f, 0xa6},
	{0x3010, 0x81},
	{0x3082, 0x01},
	{0x30f4, 0x01},
	{0x3090, 0x33},
	{0x3091, 0xc0},
	{0x30ac, 0x42},
	
	{0x30d1, 0x08},
	{0x30a8, 0x56},
	{0x3015, 0x03},
	{0x3093, 0x00},
	{0x307e, 0xe5},
	{0x3079, 0x00},
	{0x30aa, 0x42},
	{0x3017, 0x40},
	{0x30f3, 0x82},
	{0x306a, 0x0c},
	{0x306d, 0x00},
	{0x336a, 0x3c},
	{0x3076, 0x6a},
	{0x30d9, 0x8c},
	{0x3016, 0x82},
	{0x3601, 0x30},
	{0x304e, 0x88},
	{0x30f1, 0x82},
	{0x306f, 0x14},
	
	{0x3012, 0x10},
	{0x3011, 0x00},
	{0x302A, 0x02},
	{0x302B, 0x6a},
	{0x3028, 0x07},
	{0x3029, 0x93},
	
	// ;saturation
	{0x3391, 0x06},
	{0x3394, 0x38},
	{0x3395, 0x38},
	
	{0x3015, 0x02},
	{0x302d, 0x00},
	{0x302e, 0x00},
	
	// ;AEC/AGC
	{0x3013, 0xf7},
	{0x3018, 0x78},
	{0x3019, 0x68},
	{0x301a, 0xc4},
	
	// ;D5060
	{0x30af, 0x00},
	{0x3048, 0x1f}, 
	{0x3049, 0x4e},  
	{0x304a, 0x20},  
	{0x304f, 0x20},  
	{0x304b, 0x02}, 
	{0x304c, 0x00},  
	{0x304d, 0x02},  
	{0x304f, 0x20},  
	{0x30a3, 0x10},  
	{0x3013, 0xf7}, 
	{0x3014, 0x84},  
	{0x3071, 0x00},
	{0x3070, 0xb9},
	{0x3073, 0x00},
	{0x3072, 0x9a},
	{0x301c, 0x02},
	{0x301d, 0x03},
	{0x304d, 0x42},     
	{0x304a, 0x40},  
	{0x304f, 0x40},  
	{0x3095, 0x07},  
	{0x3096, 0x16}, 
	{0x3097, 0x1d}, 
	
	// ;Window Setup
	{0x3020, 0x01},
	{0x3021, 0x18},
	{0x3022, 0x00},
	{0x3023, 0x06},
	{0x3024, 0x06},
	{0x3025, 0x58},
	{0x3026, 0x02},
	{0x3027, 0x61},
	{0x3088, 0x01},
	{0x3089, 0x60},
	{0x308a, 0x01},
	{0x308b, 0x20},
	{0x3316, 0x64},
	{0x3317, 0x25},
	{0x3318, 0x80},
	{0x3319, 0x08},
	{0x331a, 0x16},
	{0x331b, 0x12},
	{0x331c, 0x00},
	{0x331d, 0x38},
	{0x3100, 0x00},
	
	// ;AWB
	{0x3320, 0xfa},
	{0x3321, 0x11},
	{0x3322, 0x92},
	{0x3323, 0x01},
	{0x3324, 0x97},
	{0x3325, 0x02},
	{0x3326, 0xff},
	{0x3327, 0x0c},
	{0x3328, 0x10}, 
	{0x3329, 0x10},
	{0x332a, 0x56},
	{0x332b, 0x54},
	{0x332c, 0xbe},
	{0x332d, 0xe1},
	{0x332e, 0x3a},
	{0x332f, 0x36},
	{0x3330, 0x4d},
	{0x3331, 0x44},
	{0x3332, 0xf8},
	{0x3333, 0x0a},
	{0x3334, 0xf0},
	{0x3335, 0xf0},
	{0x3336, 0xf0},
	{0x3337, 0x40},
	{0x3338, 0x40},
	{0x3339, 0x40},
	{0x333a, 0x00},
	{0x333b, 0x00}, 
	
	// ;Color Matrix
	{0x3380, 0x28}, 
	{0x3381, 0x48}, 
	{0x3382, 0x10}, 
	{0x3383, 0x2d}, 
	{0x3384, 0xb8}, 
	{0x3385, 0xe5}, 
	{0x3386, 0xb7}, 
	{0x3387, 0xaf}, 
	{0x3388, 0x08}, 
	{0x3389, 0x98}, 
	{0x338a, 0x01},
	
	// ;Gamma
	{0x3340, 0x04},
	{0x3341, 0x07},
	{0x3342, 0x19},
	{0x3343, 0x34},
	{0x3344, 0x4a},
	{0x3345, 0x5a},
	{0x3346, 0x67},
	{0x3347, 0x71},
	{0x3348, 0x7c},
	{0x3349, 0x8c},
	{0x334a, 0x9b},
	{0x334b, 0xa9},
	{0x334c, 0xc0},
	{0x334d, 0xd5},
	{0x334e, 0xe8},
	{0x334f, 0x20},
	
	// ;Lens correction
	{0x3350, 0x33},  
	{0x3351, 0x28},  
	{0x3352, 0x00}, 
	{0x3353, 0x13},	 
	{0x3354, 0x00}, 
	{0x3355, 0x85},  
	{0x3356, 0x35},  
	{0x3357, 0x28},  
	{0x3358, 0x00}, 
	{0x3359, 0x13},  
	{0x335a, 0x00}, 
	{0x335b, 0x85},  
	{0x335c, 0x34},  
	{0x335d, 0x28},  
	{0x335e, 0x00}, 
	{0x335f, 0x13},  
	{0x3360, 0x00}, 
	{0x3361, 0x85},  
	{0x3363, 0x70}, 
	{0x3364, 0x7f}, 
	{0x3365, 0x00}, 
	{0x3366, 0x00},  
	
	{0x3362, 0x90},
	
	// ;UVadjust
	{0x3301, 0xff},
	{0x338B, 0x14},
	{0x338c, 0x10},
	{0x338d, 0x40},
	
	// ;Sharpness/De-noise
	{0x3370, 0xd0},
	{0x3371, 0x00},
	{0x3372, 0x00},
	{0x3373, 0x40},
	{0x3374, 0x10},
	{0x3375, 0x10},
	{0x3376, 0x05},
	{0x3377, 0x00},
	{0x3378, 0x04},
	{0x3379, 0x80},
	
	// ;BLC
	{0x3069, 0x84},
	{0x307c, 0x10},
	{0x3087, 0x02},
	
	// ;Other functions
	{0x3300, 0xfc},
	{0x3302, 0x11},
	{0x3400, 0x02},
	{0x3600, 0x00},
	{0x3606, 0x20},
	{0x3601, 0x30},
	{0x30f3, 0x83},
	{0x304e, 0x88},
	
	{0x30a8, 0x54},
	{0x30aa, 0x82},
	{0x30a3, 0x91},
	{0x30a1, 0x41},
	
	{0x3086, 0x0f},
	{0x3086, 0x00},

	{0x3308, 0x00},

	{0xffff, 0xff},	/* end mark */
};

/* 320x240 */
static struct regval_list ov2650_fmt_yuv422_qvga[] = 
{
	{0x3012,0x10},
	{0x302a,0x02},
	{0x302b,0x6a},
	{0x306f,0x14},
	{0x3362,0x90},
	
	{0x3070,0x9a},
	{0x3072,0x9a},
	{0x301c,0x03},
	{0x301d,0x03},
	
	{0x3020,0x01},
	{0x3021,0x18},
	{0x3022,0x00},
	{0x3023,0x06},
	{0x3024,0x06},
	{0x3025,0x58},
	{0x3026,0x02},
	{0x3027,0x61},
	{0x3088,0x01},
	{0x3089,0x40},
	{0x308A,0x00},
	{0x308B,0xf0},
	{0x3316,0x64},
	{0x3317,0x25},
	{0x3318,0x80},
	{0x3319,0x08},
	{0x331A,0x14},
	{0x331B,0x0f},
	{0x331C,0x00},
	{0x331D,0x38},
	{0x3302,0x11},

	{0xffff, 0xff},
};

static struct regval_list ov2650_fmt_jpeg_vga[] = 
{
	//VGA JPG for test purpose!
	{0x3012,0x80}, //
	{0x304d,0x45}, // ;Rev2A
	{0x3087,0x16}, // ;Rev2A
	{0x30aa,0x45}, // ;Rev2A
	{0x30b0,0xff}, // ;Rev2A
	{0x30b1,0xff}, //
	{0x30b2,0x10}, //
	{0x30d7,0x10}, // ;Rev2A

	{0x309e,0x00}, // ;terry
	{0x3602,0x26}, //0x22, //0x26, // ;2a ;SOL/EOL on//turn off short packet 
	{0x3603,0x4D}, // ;ecc 
	{0x364c,0x04}, // ;ecc 
	{0x360c,0x12}, // ;virtual channel 0
	{0x361e,0x00}, //
	{0x361f,0x11}, // ;pclk_period, terry
	{0x3633,0x32}, // ;terry, increase hs_prepare
	{0x3629,0x3c}, // ;terry, increase clk_prepare
	{0x300e,0x39}, // ;15fps terry
	{0x300f,0xa1}, // ;terry
	{0x3010,0xa4}, //0xa4, // ;high mipi spd, 81 ;terry //use 2 data lanes
	{0x3011,0x00}, //
	{0x304c,0x88}, // ;Rev2A


	{0x3018,0x38}, // ;aec
	{0x3019,0x30}, // ;06142007
	{0x301a,0x61}, // ;06142007
	{0x307d,0x00}, // ;aec isp 06142007
	{0x3087,0x02}, // ;06142007
	{0x3082,0x20}, // ;06142007

	{0x303c,0x08}, // ;aec weight
	{0x303d,0x18}, //
	{0x303e,0x06}, //
	{0x303F,0x0c}, //
	{0x3030,0x62}, //   
	{0x3031,0x26}, //
	{0x3032,0xe6}, //
	{0x3033,0x6e}, //
	{0x3034,0xea}, //
	{0x3035,0xae}, //
	{0x3036,0xa6}, //
	{0x3037,0x6a}, //

	{0x3015,0x12}, // ;07182007 8x gain, auto 1/2
	{0x3014,0x04}, // ;06142007 auto frame off
	{0x3013,0xe7}, // ;07182007

	{0x3104,0x02}, //
	{0x3105,0xfd}, //
	{0x3106,0x00}, //
	{0x3107,0xff}, //
	{0x3308,0xa5}, //
	{0x3316,0xff}, //
	{0x3317,0x00}, //
	{0x3087,0x02}, //
	{0x3082,0x20}, //
	{0x3300,0x13}, //
	{0x3301,0xd6}, //
	{0x3302,0xef}, //

	{0x30b8,0x20}, // ;10
	{0x30b9,0x17}, // ;18
	{0x30ba,0x04}, // ;00
	{0x30bb,0x08}, // ;1f

	{0x3100,0x32}, // ;JPG //format
	{0x3304,0xfc}, //
	{0x3400,0x02}, //
	{0x3404,0x22}, //
	{0x3500,0x00}, // ;
	{0x3610,0x0c}, //

	{0x3020,0x01}, // ;JPG //size
	{0x3021,0x1d}, //
	{0x3022,0x00}, //
	{0x3023,0x0a}, //
	{0x3024,0x08}, //
	{0x3025,0x18}, //
	{0x3026,0x06}, //
	{0x3027,0x0c}, //
	{0x335f,0x68}, //
	{0x3360,0x18}, //
	{0x3361,0x0c}, //
	{0x3362,0x12}, //VGA
	{0x3363,0x88}, //VGA
	{0x3364,0xe4}, //VGA
	{0x3403,0x42}, //VGA
	{0x3088,0x02}, //VGA
	{0x3089,0x80}, //VGA
	{0x308a,0x01}, //VGA
	{0x308b,0xe0}, //VGA

	{0x3507,0x06}, //
	{0x350a,0x4f}, //
	{0x3600,0xc4}, //
	{0x3086,0x03},

	//    {0x306c,0x00},//test mode
	//    {0x307b,0x42},//test mode
	//    {0x307d,0x80},//test mode
	{0xffff,0xff},
};


/*TODO - ov2650_fmt_jpeg_qxga can't work*/
static struct regval_list ov2650_fmt_jpeg_qxga[] = {
	//2048x1536 JPG@15fps

	{0x3012,0x80},
	{0x304d,0x45}, //Rev2A
	{0x30a7,0x5e}, //Rev2C mi
	{0x3087,0x16}, //Rev2A
	{0x309C,0x1a}, //Rev2C 
	{0x30a2,0xe4}, //Rev2C 
	{0x30aa,0x42}, //Rev2C 
	{0x30b0,0xff}, //Rev2A
	{0x30b1,0xff},
	{0x30b2,0x10},
	{0x30d7,0x10},//Rev2A

	{0x309e,0x00}, //terry
	{0x3602,0x26}, //2a //SOL/EOL on
	{0x3603,0x4D}, //ecc 
	{0x364c,0x04}, //ecc 
	{0x360c,0x12}, //virtual channel 0
	{0x361e,0x00},
	{0x361f,0x11}, //pclk_period, terry
	{0x3633,0x32}, //terry, increase hs_prepare
	{0x3629,0x3c}, //terry, increase clk_prepare
	{0x300e,0x39}, //15fps terry
	{0x300f,0xa1}, //terry
	{0x3010,0xa2}, //high mipi spd, 2lane
	{0x3011,0x00},
	{0x304c,0x84}, //Rev2A

	{0x30d9,0x0d}, //Rev2C
	{0x30db,0x08}, //Rev2C
	{0x3016,0x82}, //Rev2C

	{0x3018,0x38}, //aec
	{0x3019,0x30}, //06142007
	{0x301a,0x61}, //06142007
	{0x307d,0x00}, //aec isp 06142007
	{0x3087,0x02}, //06142007
	{0x3082,0x20}, //06142007

	{0x303c,0x08}, //aec weight
	{0x303d,0x18},
	{0x303e,0x06},
	{0x303F,0x0c},
	{0x3030,0x62},	
	{0x3031,0x26},
	{0x3032,0xe6},
	{0x3033,0x6e},
	{0x3034,0xea},
	{0x3035,0xae},
	{0x3036,0xa6},
	{0x3037,0x6a},

	{0x3015,0x12}, //07182007 8x gain, auto 1/2
	{0x3014,0x04}, //06142007 auto frame off
	{0x3013,0xf7}, //07182007

	{0x3104,0x02},
	{0x3105,0xfd},
	{0x3106,0x00},
	{0x3107,0xff},

	{0x3300,0x13},
	{0x3301,0xde},
	{0x3302,0xef},

	{0x3308,0xa5},
	{0x3316,0xff},
	{0x3317,0x00},
	{0x3312,0x26},
	{0x3314,0x42},
	{0x3313,0x2b},
	{0x3315,0x42},
	{0x3310,0xd0},
	{0x3311,0xbd},
	{0x330c,0x18},
	{0x330d,0x18},
	{0x330e,0x56},
	{0x330f,0x5c},
	{0x330b,0x1c},
	{0x3306,0x5c},
	{0x3307,0x11},

	{0x336a,0x52}, //052207
	{0x3370,0x46},
	{0x3376,0x38},

	{0x30b8,0x20}, //10
	{0x30b9,0x17}, //18
	{0x30ba,0x04}, //00
	{0x30bb,0x08}, //1f

	//Format
	{0x3100,0x32}, //JPG
	{0x3304,0x00},
	{0x3400,0x02},
	{0x3404,0x22},
	{0x3500,0x00}, //
	{0x3610,0x80}, //0c

	//Size QXGA
	{0x3302,0xcf},
	{0x3088,0x08},
	{0x3089,0x00},
	{0x308a,0x06},
	{0x308b,0x00},

	{0x3507,0x06},
	{0x350a,0x4f},
	{0x3600,0xc4},

	{0xffff,0xff},
};

#define ov2650_default_regs ov2650_fmt_yuv422_vga
#define ov2650_fmt_rgb444 ov2650_fmt_yuv422_vga
#define ov2650_fmt_rgb565 ov2650_fmt_yuv422_vga
#define ov2650_fmt_raw ov2650_fmt_yuv422_vga

/*
 * Store information about the video data format.  The color matrix
 * is deeply tied into the format, so keep the relevant values here.
 * The magic matrix nubmers come from OmniVision.
 */
static struct ov2650_format_struct {
	__u8 *desc;
	__u32 pixelformat;
	struct regval_list *regs;
	int cmatrix[CMATRIX_LEN];
	int bpp;   /* bits per pixel */
} ov2650_formats[] = {

	{
		.desc		= "YUYV422 planar",
		.pixelformat	= V4L2_PIX_FMT_YUV422P,
		.regs 		= ov2650_fmt_yuv422_vga,
		.cmatrix	= { 128, -128, 0, -34, -94, 128 },
		.bpp		= 16,
	},
	{
		.desc		= "YUYV422",
		.pixelformat	= V4L2_PIX_FMT_YUYV,
		.regs 		= ov2650_fmt_yuv422_vga,
		.cmatrix	= { 128, -128, 0, -34, -94, 128 },
		.bpp		= 16,
	},
	{
		.desc		= "UYVY422",
		.pixelformat	= V4L2_PIX_FMT_UYVY,
		.regs 		= ov2650_fmt_yuv422_vga,
		.cmatrix	= { 128, -128, 0, -34, -94, 128 },
		.bpp		= 16,
	},

	/* guang: ov2650 doesn't support yuv420 */
	{
		.desc           = "YUYV 4:2:0",
		.pixelformat    = V4L2_PIX_FMT_YUV420,
		.regs           = NULL,
		.cmatrix        = { 128, -128, 0, -34, -94, 128 },
		.bpp            = 12,
	},
	{
		.desc           = "JFIF JPEG",
		.pixelformat    = V4L2_PIX_FMT_JPEG,
		.regs           = ov2650_fmt_jpeg_qxga,
		.cmatrix        = { 128, -128, 0, -34, -94, 128 },
		.bpp            = 16,
	},
	{
		.desc		= "RGB 444",
		.pixelformat	= V4L2_PIX_FMT_RGB444,
		.regs		= ov2650_fmt_rgb444,
		.cmatrix	= { 179, -179, 0, -61, -176, 228 },
		.bpp		= 16,
	},
	{
		.desc		= "RGB 565",
		.pixelformat	= V4L2_PIX_FMT_RGB565,
		.regs		= ov2650_fmt_rgb565,
		.cmatrix	= { 179, -179, 0, -61, -176, 228 },
		.bpp		= 16,
	},
	{
		.desc		= "Raw RGB Bayer",
		.pixelformat	= V4L2_PIX_FMT_SBGGR8,
		.regs 		= ov2650_fmt_raw,
		.cmatrix	= { 0, 0, 0, 0, 0, 0 },
		.bpp		= 8,
	},
};
#define N_OV2650_FMTS ARRAY_SIZE(ov2650_formats)


/*TODO - also can use ccic size register 0x34 to do same thing for cropping...anyway, sensor doing it is better?
  0x3020~0x3027*/
static struct ov2650_win_size {
	int	width;
	int	height;
	unsigned char com7_bit;
	int	hstart;		/* Start/stop values for the camera.  Note */
	int	hstop;		/* that they do not always make complete */
	int	vstart;		/* sense to humans, but evidently the sensor */
	int	vstop;		/* will do the right thing... */
	struct regval_list *regs; /* Regs to tweak */
	/* h/vref stuff */
} ov2650_win_sizes[] = {
	/* QVGA */
	{
		.width		= QVGA_WIDTH,
		.height		= QVGA_HEIGHT,
		.com7_bit	= COM7_FMT_QVGA,
		.hstart		= 164,		/* Empirically determined */
		.hstop		=  20,
		.vstart		=  14,
		.vstop		= 494,
		.regs 		= NULL,
	},
#if 1
	/* CIF */
	{
		.width		= CIF_WIDTH,
		.height		= CIF_HEIGHT,
		//.com7_bit	= COM7_FMT_CIF,
		.hstart		= 170,		/* Empirically determined */
		.hstop		=  90,
		.vstart		=  14,
		.vstop		= 494,
		.regs 		= NULL,
	},
#endif
	/* VGA */
	{
		.width		= VGA_WIDTH,
		.height		= VGA_HEIGHT,
		.com7_bit	= COM7_FMT_VGA,
		.hstart		= 158,		/* These values from */
		.hstop		=  14,		/* Omnivision */
		.vstart		=  10,
		.vstop		= 490,
		.regs 		= NULL,
	},
	/* UXGA */
	{
		.width			= UXGA_WIDTH,
		.height 		= UXGA_HEIGHT,
		.com7_bit		= 0,
		.hstart 		= 158,			/* These values from */
		.hstop			=  14,			/* Omnivision */
		.vstart 		=  10,
		.vstop			= 490,
		.regs			= NULL,
	},
};

#define N_OV2650_WIN_SIZES (ARRAY_SIZE(ov2650_win_sizes))

/*
 * flip and mirror control
 * 0: normal
 * 1: flip on
 * 2: mirror on
 * 3: flip and mirror on
 */
static int fm_ctrl = 0;
module_param(fm_ctrl, int, 0);

/*
 * Low-level register I/O.
 */
/*issue that OV sensor must write then read. 3640 register is 16bit!!!*/
static int ov2650_read(struct i2c_client *c, u16 reg,
		unsigned char *value)
{
	u8 data;
	u8 address[2];
	address[0] = reg>>8;
	address[1] = reg;         
	i2c_smbus_write_byte_data(c,address[0],address[1]);
	data = i2c_smbus_read_byte(c);
	*value = data;
	return 0;
}


static int ov2650_write(struct i2c_client *c, u16 reg,
		unsigned char value)
{
	u8 data[3];
	data[0] = reg>>8;
	data[1] = reg;
	data[2]=  value;
	i2c_master_send(c, data, 3);
	if (reg == OV2650_SYS && (value & SYS_RESET))
		msleep(2);  /* Wait for reset to run */
	return 0;
}


/*
 * Write a list of register settings; ff/ff stops the process.
 */
static int ov2650_write_array(struct i2c_client *c, struct regval_list *vals)
{
	int i = 0;
	
	while (vals->reg_num != 0xffff || vals->value != 0xff) {
		int ret = ov2650_write(c, vals->reg_num, vals->value);
		if (ret < 0)
			return ret;
		vals++;
		if (i == 0)
			mdelay(5);
		i++;
	}
	return 0;
}


/*
 * Stuff that knows about the sensor.
 */
static void ov2650_reset(struct i2c_client *client)
{
	//soft reset
	ov2650_write(client, OV2650_SYS, 0x80);
	msleep(1);
}


static int ov2650_init(struct i2c_client *client)	//TODO - currently not needed on 3640
{
	return ov2650_write_array(client, ov2650_default_regs);
}



static int ov2650_detect(struct i2c_client *client)
{
	unsigned char v=0;
	int ret;

	/*
	 * no MID register found. OK, we know we have an OmniVision chip...but which one?
	 */
	ret = ov2650_read(client, OV2650_PID_H, &v);
	if (ret < 0)
		return ret;
	if (v != 0x26)
		return -ENODEV;
	ret = ov2650_read(client, OV2650_PID_L, &v);
	if (ret < 0)
		return ret;

	if (v == 0x52) 
	{
		printk(KERN_NOTICE "OmniVision ov2650 sensor detected\n");
		return 0;
	}
	else if (v == 0x56) 
	{
		printk(KERN_NOTICE "OmniVision ov2655 sensor detected\n");
		return 0;
	}
	else
	{
		printk(KERN_ERR "No sensor detected. ID: 0x%2x%2x\n", 0x26, v);
		return -ENODEV;
	}
}


#if 0
/*
 * Store a set of start/stop values into the camera.	
 */
static int ov2650_set_hw(struct i2c_client *client, int hstart, int hstop,
		int vstart, int vstop)
{
	int ret;
	unsigned char v;
	/*
	 * Horizontal: 11 bits, top 8 live in hstart and hstop.  Bottom 3 of
	 * hstart are in href[2:0], bottom 3 of hstop in href[5:3].  There is
	 * a mystery "edge offset" value in the top two bits of href.
	 */
	ret =  ov2650_write(client, REG_HSTART, (hstart >> 3) & 0xff);
	ret += ov2650_write(client, REG_HSTOP, (hstop >> 3) & 0xff);
	ret += ov2650_read(client, REG_HREF, &v);
	v = (v & 0xc0) | ((hstop & 0x7) << 3) | (hstart & 0x7);
	msleep(10);
	ret += ov2650_write(client, REG_HREF, v);
	/*
	 * Vertical: similar arrangement, but only 10 bits.
	 */
	ret += ov2650_write(client, REG_VSTART, (vstart >> 2) & 0xff);
	ret += ov2650_write(client, REG_VSTOP, (vstop >> 2) & 0xff);
	ret += ov2650_read(client, REG_VREF, &v);
	v = (v & 0xf0) | ((vstop & 0x3) << 2) | (vstart & 0x3);
	msleep(10);
	ret += ov2650_write(client, REG_VREF, v);
	return ret;
	return 0;
}
#endif

static int ov2650_enum_fmsize(struct i2c_client *client, struct v4l2_frmsizeenum *argp)
{
	struct v4l2_frmsizeenum frmsize;

	if (copy_from_user(&frmsize, argp, sizeof(frmsize)))
		return -EFAULT;

	switch (frmsize.pixel_format) {
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420:
			if (frmsize.index == ARRAY_SIZE(ov2650_win_sizes))
				return -EINVAL;
			else if (frmsize.index > ARRAY_SIZE(ov2650_win_sizes)) {
				printk(KERN_ERR "camera: ov2650 enum unsupported preview format size!\n");
				return -EINVAL;
			} else {
				frmsize.type = V4L2_FRMSIZE_TYPE_DISCRETE;
				frmsize.discrete.height = ov2650_win_sizes[frmsize.index].height;
				frmsize.discrete.width = ov2650_win_sizes[frmsize.index].width;
				break;
			}
		default:
			printk(KERN_ERR "camera: ov2650 enum format size with unsupported format!\n");
			return -EINVAL;
	}

	if (copy_to_user(argp, &frmsize, sizeof(frmsize)))
		return -EFAULT;
	
	return 0;
}

static int ov2650_enum_fmt(struct i2c_client *c, struct v4l2_fmtdesc *fmt)
{
	struct ov2650_format_struct *ofmt;

	if (fmt->index >= N_OV2650_FMTS)
		return -EINVAL;

	ofmt = ov2650_formats + fmt->index;
	fmt->flags = 0;
	strcpy(fmt->description, ofmt->desc);
	fmt->pixelformat = ofmt->pixelformat;
	return 0;
}


static int ov2650_try_fmt(struct i2c_client *c, struct v4l2_format *fmt,
		struct ov2650_format_struct **ret_fmt,
		struct ov2650_win_size **ret_wsize)
{
	int index;
	struct ov2650_win_size *wsize;
	struct v4l2_pix_format *pix = &fmt->fmt.pix;
	for (index = 0; index < N_OV2650_FMTS; index++)
		if (ov2650_formats[index].pixelformat == pix->pixelformat)
			break;
	if (index >= N_OV2650_FMTS){
		printk("unsupported format!\n");
		return -EINVAL;
	}
	if (ret_fmt != NULL)
		*ret_fmt = ov2650_formats + index;
	/*
	 * Fields: the OV devices claim to be progressive.
	 */
	if (pix->field == V4L2_FIELD_ANY)
		pix->field = V4L2_FIELD_NONE;
	else if (pix->field != V4L2_FIELD_NONE)
	{
		printk("invalid field\n");
		//return -EINVAL; /* don't return, for mpt. -guang */
	}
	/*
	 * Round requested image size down to the nearest
	 * we support, but not below the smallest.
	 */
	for (wsize = ov2650_win_sizes; wsize < ov2650_win_sizes + N_OV2650_WIN_SIZES;
			wsize++)
		if (pix->width <= wsize->width && pix->height <= wsize->height)
			break;
	if (wsize >= ov2650_win_sizes + N_OV2650_WIN_SIZES){
		printk("size exceed and set as QXGA!\n");
		wsize--;   /* Take the smallest one */
	}
	if (ret_wsize != NULL)
		*ret_wsize = wsize;
	/*
	 * Note the size we'll actually handle.
	 */
#if 0
	pix->width = wsize->width;
	pix->height = wsize->height;
#endif
	pix->bytesperline = pix->width*ov2650_formats[index].bpp/8;
	pix->sizeimage = pix->height*pix->bytesperline;

	if (ret_fmt == NULL)
		return 0;
	switch (pix->pixelformat)
	{
		case V4L2_PIX_FMT_YUYV: 
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUV420: 
			switch (wsize->width)
			{
				case UXGA_WIDTH:
					(*ret_fmt)->regs = ov2650_fmt_yuv422_uxga;
					break;
				case VGA_WIDTH:
					(*ret_fmt)->regs = ov2650_fmt_yuv422_vga;
					break;
				case CIF_WIDTH:
					(*ret_fmt)->regs = ov2650_fmt_yuv422_cif;
					break;
				case QVGA_WIDTH:
					(*ret_fmt)->regs = ov2650_fmt_yuv422_qvga;
					break;
				default:
					printk("unsupported size!\n");
					break;
			}
			break;
		case V4L2_PIX_FMT_JPEG:
			switch (wsize->width)
			{
				case QXGA_WIDTH:
					(*ret_fmt)->regs = ov2650_fmt_jpeg_qxga;
					break;
				case VGA_WIDTH:
					(*ret_fmt)->regs = ov2650_fmt_jpeg_vga;
					break;
				default:
					printk("unsupported size!\n");
					break;
			}
			break;
		default:
			printk("unsupported format!\n");
			break;
	}	
	return 0;
}

/*
 * Set a format.
 */
static int ov2650_s_fmt(struct i2c_client *c, struct v4l2_format *fmt)
{
	int ret;
	struct ov2650_format_struct *ovfmt;
	struct ov2650_win_size *wsize;

	unsigned char flip_mirror = 0;
	unsigned char array_mirror = 0;
	int flip = fm_ctrl & 0x1;
	int mirror = fm_ctrl & 0x2;

	ret = ov2650_try_fmt(c, fmt, &ovfmt, &wsize);
	if (ret)
		return ret;

	ov2650_write_array(c, ovfmt->regs);
	
	ov2650_read(c, FLIP_MIRROR, &flip_mirror);
	ov2650_read(c, ARRAY_MIRROR, &array_mirror);

	if (flip)
	{
		flip_mirror |= 0x1;
	}
	else
	{
		flip_mirror &= ~0x1;
	}

	if (mirror)
	{
		flip_mirror |= 0x2;
		array_mirror |= 0x8;
	}
	else
	{
		flip_mirror &= ~0x2;
		array_mirror &= ~0x8;
	}

	ov2650_write(c, FLIP_MIRROR, flip_mirror);
	ov2650_write(c, ARRAY_MIRROR, array_mirror);
	
	return ret;
}

/*
 * Get a format.
 */
static int ov2650_g_fmt(struct i2c_client *c, struct v4l2_format *fmt)
{
	unsigned char w_msb;
	unsigned char w_lsb;
	unsigned char h_msb;
	unsigned char h_lsb;

	ov2650_read(c, 0x3088, &w_msb);
	ov2650_read(c, 0x3089, &w_lsb);
	ov2650_read(c, 0x308a, &h_msb);
	ov2650_read(c, 0x308b, &h_lsb);

	fmt->type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	fmt->fmt.pix.width = (w_msb<<8)|w_lsb;
	fmt->fmt.pix.height = (h_msb<<8)|h_lsb;
	fmt->fmt.pix.pixelformat = V4L2_PIX_FMT_YUV422P;
	fmt->fmt.pix.field = V4L2_FIELD_NONE;
	
	return 0;
}

/*
 * Implement G/S_PARM.  There is a "high quality" mode we could try
 * to do someday; for now, we just do the frame rate tweak.
 */
static int ov2650_g_parm(struct i2c_client *c, struct v4l2_streamparm *parms)
{
	struct v4l2_captureparm *cp = &parms->parm.capture;
	unsigned char clkrc;
	int ret;

	if (parms->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
		return -EINVAL;
	ret = ov2650_read(c, REG_CLKRC, &clkrc);
	if (ret < 0)
		return ret;
	memset(cp, 0, sizeof(struct v4l2_captureparm));
	cp->capability = V4L2_CAP_TIMEPERFRAME;
	cp->timeperframe.numerator = 1;
	cp->timeperframe.denominator = OV2650_FRAME_RATE;
	if ((clkrc & CLK_EXT) == 0 && (clkrc & CLK_SCALE) > 1)
		cp->timeperframe.denominator /= (clkrc & CLK_SCALE);
	return 0;
}

static int ov2650_s_parm(struct i2c_client *c, struct v4l2_streamparm *parms)
{
	struct v4l2_captureparm *cp = &parms->parm.capture;
	struct v4l2_fract *tpf = &cp->timeperframe;
	unsigned char clkrc;
	int ret, div;

	if (parms->type != V4L2_BUF_TYPE_VIDEO_CAPTURE)
		return -EINVAL;
	if (cp->extendedmode != 0)
		return -EINVAL;
	/*
	 * CLKRC has a reserved bit, so let's preserve it.
	 */
	ret = ov2650_read(c, REG_CLKRC, &clkrc);
	if (ret < 0)
		return ret;
	if (tpf->numerator == 0 || tpf->denominator == 0)
		div = 1;  /* Reset to full rate */
	else
		div = (tpf->numerator*OV2650_FRAME_RATE)/tpf->denominator;
	if (div == 0)
		div = 1;
	else if (div > CLK_SCALE)
		div = CLK_SCALE;
	clkrc = (clkrc & 0xc0) | div ;
	tpf->numerator = 1;
	tpf->denominator = OV2650_FRAME_RATE/div;
	return ov2650_write(c, REG_CLKRC, clkrc);
}

static int ov2650_s_input(struct i2c_client *c, int *id)
{
	return 0;
}

/*
 * Code for dealing with controls.
 */


/*TODO - need to port below register codes for 3640...maybe not used*/


static int ov2650_store_cmatrix(struct i2c_client *client,
		int matrix[CMATRIX_LEN])
{
#if 0
	int i, ret;
	unsigned char signbits;

	/*
	 * Weird crap seems to exist in the upper part of
	 * the sign bits register, so let's preserve it.
	 */
	ret = ov2650_read(client, REG_CMATRIX_SIGN, &signbits);
	signbits &= 0xc0;

	for (i = 0; i < CMATRIX_LEN; i++) {
		unsigned char raw;

		if (matrix[i] < 0) {
			signbits |= (1 << i);
			if (matrix[i] < -255)
				raw = 0xff;
			else
				raw = (-1 * matrix[i]) & 0xff;
		}
		else {
			if (matrix[i] > 255)
				raw = 0xff;
			else
				raw = matrix[i] & 0xff;
		}
		ret += ov2650_write(client, REG_CMATRIX_BASE + i, raw);
	}
	ret += ov2650_write(client, REG_CMATRIX_SIGN, signbits);
	return ret;
#endif	
	return 0;
}


/*
 * Hue also requires messing with the color matrix.  It also requires
 * trig functions, which tend not to be well supported in the kernel.
 * So here is a simple table of sine values, 0-90 degrees, in steps
 * of five degrees.  Values are multiplied by 1000.
 *
 * The following naive approximate trig functions require an argument
 * carefully limited to -180 <= theta <= 180.
 */
#define SIN_STEP 5
static const int ov2650_sin_table[] = {
	0,	 87,   173,   258,   342,   422,
	499,	573,   642,   707,   766,   819,
	866,	906,   939,   965,   984,   996,
	1000
};

static int ov2650_sine(int theta)
{
	int chs = 1;
	int sine;

	if (theta < 0) {
		theta = -theta;
		chs = -1;
	}
	if (theta <= 90)
		sine = ov2650_sin_table[theta/SIN_STEP];
	else {
		theta -= 90;
		sine = 1000 - ov2650_sin_table[theta/SIN_STEP];
	}
	return sine*chs;
}

static int ov2650_cosine(int theta)
{
	theta = 90 - theta;
	if (theta > 180)
		theta -= 360;
	else if (theta < -180)
		theta += 360;
	return ov2650_sine(theta);
}




static void ov2650_calc_cmatrix(struct ov2650_info *info,
		int matrix[CMATRIX_LEN])
{
	int i;
	/*
	 * Apply the current saturation setting first.
	 */
	for (i = 0; i < CMATRIX_LEN; i++)
		matrix[i] = (info->fmt->cmatrix[i]*info->sat) >> 7;
	/*
	 * Then, if need be, rotate the hue value.
	 */
	if (info->hue != 0) {
		int sinth, costh, tmpmatrix[CMATRIX_LEN];

		memcpy(tmpmatrix, matrix, CMATRIX_LEN*sizeof(int));
		sinth = ov2650_sine(info->hue);
		costh = ov2650_cosine(info->hue);

		matrix[0] = (matrix[3]*sinth + matrix[0]*costh)/1000;
		matrix[1] = (matrix[4]*sinth + matrix[1]*costh)/1000;
		matrix[2] = (matrix[5]*sinth + matrix[2]*costh)/1000;
		matrix[3] = (matrix[3]*costh - matrix[0]*sinth)/1000;
		matrix[4] = (matrix[4]*costh - matrix[1]*sinth)/1000;
		matrix[5] = (matrix[5]*costh - matrix[2]*sinth)/1000;
	}
}



static int ov2650_t_sat(struct i2c_client *client, int value)
{
	struct ov2650_info *info = i2c_get_clientdata(client);
	int matrix[CMATRIX_LEN];
	int ret;

	info->sat = value;
	ov2650_calc_cmatrix(info, matrix);
	ret = ov2650_store_cmatrix(client, matrix);
	return ret;
}

static int ov2650_q_sat(struct i2c_client *client, __s32 *value)
{
	struct ov2650_info *info = i2c_get_clientdata(client);

	*value = info->sat;
	return 0;
}

static int ov2650_t_hue(struct i2c_client *client, int value)
{
	struct ov2650_info *info = i2c_get_clientdata(client);
	int matrix[CMATRIX_LEN];
	int ret;

	if (value < -180 || value > 180)
		return -EINVAL;
	info->hue = value;
	ov2650_calc_cmatrix(info, matrix);
	ret = ov2650_store_cmatrix(client, matrix);
	return ret;
}


static int ov2650_q_hue(struct i2c_client *client, __s32 *value)
{
	struct ov2650_info *info = i2c_get_clientdata(client);

	*value = info->hue;
	return 0;
}

#if 0
/*
 * Some weird registers seem to store values in a sign/magnitude format!
 */
static unsigned char ov2650_sm_to_abs(unsigned char v)
{
	if ((v & 0x80) == 0)
		return v + 128;
	else
		return 128 - (v & 0x7f);
}


static unsigned char ov2650_abs_to_sm(unsigned char v)
{
	if (v > 127)
		return v & 0x7f;
	else
		return (128 - v) | 0x80;
}
#endif

static int ov2650_t_brightness(struct i2c_client *client, int value)
{
#if 0
	unsigned char com8, v;
	int ret;

	ov2650_read(client, REG_COM8, &com8);
	com8 &= ~COM8_AEC;
	ov2650_write(client, REG_COM8, com8);
	v = ov2650_abs_to_sm(value);
	ret = ov2650_write(client, REG_BRIGHT, v);
	return ret;
#endif	
	return 0;
}

static int ov2650_q_brightness(struct i2c_client *client, __s32 *value)
{
#if 0	
	unsigned char v;
	int ret = ov2650_read(client, REG_BRIGHT, &v);

	*value = ov2650_sm_to_abs(v);
	return ret;
#endif	
	return 0;
}

static int ov2650_t_contrast(struct i2c_client *client, int value)
{
#if 0	
	return ov2650_write(client, REG_CONTRAS, (unsigned char) value);
#endif	
	return 0;
}

static int ov2650_q_contrast(struct i2c_client *client, __s32 *value)
{
#if 0	
	unsigned char v;
	int ret = ov2650_read(client, REG_CONTRAS, &v);

	*value = v;
	return ret;
#endif	
		return 0;
}

static int ov2650_q_hflip(struct i2c_client *client, __s32 *value)
{
#if 0	
	int ret;
	unsigned char v;

	ret = ov2650_read(client, REG_MVFP, &v);
	*value = (v & MVFP_MIRROR) == MVFP_MIRROR;
	return ret;
#endif	
			return 0;
}


static int ov2650_t_hflip(struct i2c_client *client, int value)
{
#if 0	
	unsigned char v;
	int ret;

	ret = ov2650_read(client, REG_MVFP, &v);
	if (value)
		v |= MVFP_MIRROR;
	else
		v &= ~MVFP_MIRROR;
	msleep(10);  /* FIXME */
	ret += ov2650_write(client, REG_MVFP, v);
	return ret;
#endif	
	return 0;
}



static int ov2650_q_vflip(struct i2c_client *client, __s32 *value)
{
#if 0	
	int ret;
	unsigned char v;

	ret = ov2650_read(client, REG_MVFP, &v);
	*value = (v & MVFP_FLIP) == MVFP_FLIP;
	return ret;
#endif	
		return 0;
}


static int ov2650_t_vflip(struct i2c_client *client, int value)
{
#if 0	
	unsigned char v;
	int ret;

	ret = ov2650_read(client, REG_MVFP, &v);
	if (value)
		v |= MVFP_FLIP;
	else
		v &= ~MVFP_FLIP;
	msleep(10);  /* FIXME */
	ret += ov2650_write(client, REG_MVFP, v);
	return ret;
#endif	
	return 0;
}


static struct ov2650_control {
	struct v4l2_queryctrl qc;
	int (*query)(struct i2c_client *c, __s32 *value);
	int (*tweak)(struct i2c_client *c, int value);
} ov2650_controls[] =
{
	{
		.qc = {
			.id = V4L2_CID_BRIGHTNESS,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "Brightness",
			.minimum = 0,
			.maximum = 255,
			.step = 1,
			.default_value = 0x80,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = ov2650_t_brightness,
		.query = ov2650_q_brightness,
	},
	{
		.qc = {
			.id = V4L2_CID_CONTRAST,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "Contrast",
			.minimum = 0,
			.maximum = 127,
			.step = 1,
			.default_value = 0x40,   /* XXX ov2650 spec */
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = ov2650_t_contrast,
		.query = ov2650_q_contrast,
	},
	{
		.qc = {
			.id = V4L2_CID_SATURATION,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "Saturation",
			.minimum = 0,
			.maximum = 256,
			.step = 1,
			.default_value = 0x80,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = ov2650_t_sat,
		.query = ov2650_q_sat,
	},
	{
		.qc = {
			.id = V4L2_CID_HUE,
			.type = V4L2_CTRL_TYPE_INTEGER,
			.name = "HUE",
			.minimum = -180,
			.maximum = 180,
			.step = 5,
			.default_value = 0,
			.flags = V4L2_CTRL_FLAG_SLIDER
		},
		.tweak = ov2650_t_hue,
		.query = ov2650_q_hue,
	},
	{
		.qc = {
			.id = V4L2_CID_VFLIP,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "Vertical flip",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = ov2650_t_vflip,
		.query = ov2650_q_vflip,
	},
	{
		.qc = {
			.id = V4L2_CID_HFLIP,
			.type = V4L2_CTRL_TYPE_BOOLEAN,
			.name = "Horizontal mirror",
			.minimum = 0,
			.maximum = 1,
			.step = 1,
			.default_value = 0,
		},
		.tweak = ov2650_t_hflip,
		.query = ov2650_q_hflip,
	},
};
#define N_CONTROLS (ARRAY_SIZE(ov2650_controls))

static struct ov2650_control *ov2650_find_control(__u32 id)
{
	int i;

	for (i = 0; i < N_CONTROLS; i++)
		if (ov2650_controls[i].qc.id == id)
			return ov2650_controls + i;
	return NULL;
}


static int ov2650_queryctrl(struct i2c_client *client,
		struct v4l2_queryctrl *qc)
{
	struct ov2650_control *ctrl = ov2650_find_control(qc->id);

	if (ctrl == NULL)
		return -EINVAL;
	*qc = ctrl->qc;
	return 0;
}

static int ov2650_g_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct ov2650_control *octrl = ov2650_find_control(ctrl->id);
	int ret;

	if (octrl == NULL)
		return -EINVAL;
	ret = octrl->query(client, &ctrl->value);
	if (ret >= 0)
		return 0;
	return ret;
}

static int ov2650_s_ctrl(struct i2c_client *client, struct v4l2_control *ctrl)
{
	struct ov2650_control *octrl = ov2650_find_control(ctrl->id);
	int ret;

	if (octrl == NULL)
		return -EINVAL;
	ret =  octrl->tweak(client, ctrl->value);
	if (ret >= 0)
		return 0;
	return ret;
}




int ccic_sensor_attach(struct i2c_client *client);

struct i2c_client * sensor_i2c_client = NULL;
u16 last_reg = 0;
static ssize_t sensor_get_reg(struct device *dev,
		struct device_attribute *attr, char *buf) 
{
	u8 val = 0;

	if (!sensor_i2c_client)
		return sprintf(buf, "Invalid sensor handle!\n");
	
	if (last_reg)
	{
		ov2650_read(sensor_i2c_client, last_reg, &val);
		return sprintf(buf, "%x:%x\n", last_reg, val);
	}
	else
	{
		int len=0;
		int index;

		for (index=0; ov2650_default_regs[index].reg_num!=0xffff; index++)
		{
			ov2650_read(sensor_i2c_client, ov2650_default_regs[index].reg_num, &val);
			len += sprintf(&buf[len], "%04x:%02x%s\n", ov2650_default_regs[index].reg_num, val, (val!=ov2650_default_regs[index].value)?"*":"");
		}
		return len;
	}
}

static ssize_t sensor_set_reg(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count) 
{
	unsigned int reg ,val;
	int ret;

	ret = sscanf(buf, "%x:%x", &reg, &val);
	if (ret == 1)
	{
		last_reg = reg;
		return count;
	}
	else if (ret == 2)
	{
		if (!sensor_i2c_client)
			printk("Invalid sensor client handle!\n");
		
		ov2650_write(sensor_i2c_client, reg, val);
		last_reg = reg;
		return count;
	}
	else
	{
		last_reg = 0;
		return count;
	}
}

static struct device_attribute sensor_static_attrs[] = {
	__ATTR(reg, 0666, sensor_get_reg, sensor_set_reg),
};

/*
 * Basic i2c stuff.
 */
extern struct clk *pxa168_ccic_gate_clk;
static int __devinit ov2650_probe(struct i2c_client *client, const struct i2c_device_id * id)
{
	int i;
	int ret;
	struct ov2650_info *info;
	struct sensor_platform_data *pdata;
	pdata = client->dev.platform_data;

	pdata = client->dev.platform_data;
	pdata->power_set(SENSOR_OPEN, pdata->id, pdata->eco_flag, pdata->sensor_flag);

	/*
	 * Set up our info structure.
	 */
	info = kzalloc(sizeof (struct ov2650_info), GFP_KERNEL);
	if (! info) {
		ret = -ENOMEM;
		goto out_free;
	}

	//set default format
	info->fmt = &ov2650_formats[0];
	info->sat = 128;	/* Review this */
	i2c_set_clientdata(client, info);
	/*
	 * Make sure it's an ov2650
	 */
	ret = ov2650_detect(client);
	if (ret)
		goto out_free_info;

	ret = ccic_sensor_attach(client);
	if (ret)
		goto out_free_info;

	if (fm_ctrl & 0x1) printk("Vertical flip on.\n");
	if (fm_ctrl & 0x2) printk("Horizonal mirror on.\n");

	pdata->power_set(SENSOR_CLOSE, pdata->id, pdata->eco_flag, pdata->sensor_flag);

	/* create sys entries */
	sensor_i2c_client = client;

	/* create sys entries */
	for (i=0; i<ARRAY_SIZE(sensor_static_attrs); i++)
	{
		ret = device_create_file(&client->dev, &sensor_static_attrs[i]);
		if(ret)
			goto out_free_info;
	}
	
	return 0;

out_free_info:
	kfree(info);
out_free:
	return ret;
}


static int ov2650_remove(struct i2c_client *client)
{
	int i;
	
	for (i=0; i<ARRAY_SIZE(sensor_static_attrs); i++)
	{
		device_remove_file(&client->dev, &sensor_static_attrs[i]);
	}
	
	return 0;
}


static int ov2650_streamon(struct i2c_client *client)
{
#if 0	
	unsigned char val;
	ov2650_read(client, 0x3086, &val);
	val &= ~0x03;
	ov2650_write(client, 0x3086, val);
#endif	
	return 0;
}

static int ov2650_streamoff(struct i2c_client *client)
{
#if 0	
	unsigned char val;
	ov2650_read(client, 0x3086, &val);
	val |= 0x03;
	ov2650_write(client, 0x3086, val);
#endif	
	return 0;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int ov2650_g_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return ov2650_read(client, (u16)reg->reg, (unsigned char *)&(reg->val));
}

static int ov2650_s_register(struct i2c_client *client, struct v4l2_dbg_register * reg)
{
	return ov2650_write(client, (u16)reg->reg, (unsigned char)reg->val);
}
#endif
static int ov2650_command(struct i2c_client *client, unsigned int cmd,
		void *arg)
{
	switch (cmd) {
		case VIDIOC_DBG_G_CHIP_IDENT:
			return v4l2_chip_ident_i2c_client(client, arg, V4L2_IDENT_OV265X, 0);
		case VIDIOC_INT_RESET:
			ov2650_reset(client);
			return 0;

		/*
		case VIDIOC_INT_INIT:
			ov2650_init(client);
			return 0;
		*/
		case VIDIOC_ENUM_FRAMESIZES:
			return ov2650_enum_fmsize(client, (struct v4l2_frmsizeenum *) arg);
		case VIDIOC_ENUM_FMT:
			return ov2650_enum_fmt(client, (struct v4l2_fmtdesc *) arg);
		case VIDIOC_TRY_FMT:
			return ov2650_try_fmt(client, (struct v4l2_format *) arg, NULL, NULL);
		case VIDIOC_S_FMT:
			return ov2650_s_fmt(client, (struct v4l2_format *) arg);
		case VIDIOC_G_FMT:
			return ov2650_g_fmt(client, (struct v4l2_format *) arg);
		case VIDIOC_QUERYCTRL:
			return ov2650_queryctrl(client, (struct v4l2_queryctrl *) arg);
		case VIDIOC_S_CTRL:
			return ov2650_s_ctrl(client, (struct v4l2_control *) arg);
		case VIDIOC_G_CTRL:
			return ov2650_g_ctrl(client, (struct v4l2_control *) arg);
		case VIDIOC_S_PARM:
			//return 0;
			return ov2650_s_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_G_PARM:
			return ov2650_g_parm(client, (struct v4l2_streamparm *) arg);
		case VIDIOC_S_INPUT:
			return ov2650_s_input(client, (int *) arg);
		case VIDIOC_STREAMON:
			return ov2650_streamon(client);
		case VIDIOC_STREAMOFF:
			return ov2650_streamoff(client);
#ifdef CONFIG_VIDEO_ADV_DEBUG
		case VIDIOC_DBG_G_REGISTER:
			return ov2650_g_register(client, (struct v4l2_dbg_register *) arg);
		case VIDIOC_DBG_S_REGISTER:
			return ov2650_s_register(client, (struct v4l2_dbg_register *) arg);
#endif
	}
	return -EINVAL;
}

static struct i2c_device_id ov2650_idtable[] = {
	{ "ov265x", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, ov2650_idtable);

static struct i2c_driver ov2650_driver = {
	.driver = {
		.name	= "ov265x",
	},
	.id_table       = ov2650_idtable,
	.command	= ov2650_command,
	.probe		= ov2650_probe,
	.remove		= __devexit_p(ov2650_remove),
};


/*
 * Module initialization
 */
static int __init ov2650_mod_init(void)
{
	printk(KERN_NOTICE "OmniVision ov265x sensor driver, at your service.\n");
	return i2c_add_driver(&ov2650_driver);
}

static void __exit ov2650_mod_exit(void)
{
	i2c_del_driver(&ov2650_driver);
}

module_init(ov2650_mod_init);
module_exit(ov2650_mod_exit);

MODULE_AUTHOR("Raymond Wang <raymond1860@gmail.com>");
MODULE_DESCRIPTION("Aspen168 based driver for OmniVision ov2650/ov2655 sensor");
MODULE_LICENSE("GPL");

