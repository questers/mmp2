#ifndef _OV2650_H_
#define _OV2650_H_
                                                     
/* System control register */                        
#define OV2650_AGC     0x3000                
#define OV2650_AGCS    0x3001                        
#define OV2650_AEC_H   0x3002                        
#define OV2650_AEC_L   0x3003                        
#define OV2650_AECL    0x3004                        
#define OV2650_AECS_H  0x3008                        
#define OV2650_AECS_L  0x3009                        
#define OV2650_PID_H   0x300A                        
#define OV2650_PID_L   0x300B                        
#define OV2650_SCCB    0x300C                
#define OV2650_PCLK    0x300D                
#define OV2650_PLL_1   0x300E                        
#define OV2650_PLL_2   0x300F                        
#define OV2650_PLL_3   0x3010                        
#define OV2650_CLK     0x3011                
#define OV2650_SYS     0x3012                
#define OV2650_AUTO_1  0x3013                        
#define OV2650_AUTO_2  0x3014                        
#define OV2650_AUTO_3  0x3015                        
#define OV2650_AUTO_4  0x3016                        
#define OV2650_AUTO_5  0x3017                        
#define OV2650_WPT     0x3018                
#define OV2650_BPT     0x3019                
#define OV2650_VPT     0x301A                
#define OV2650_YAVG    0x301B                
#define OV2650_AECG_50 0x301C                        
#define OV2650_AECG_60 0x301D                        
#define OV2650_RZM_H   0x301E                        
#define OV2650_RZM_L   0x301F                        
#define OV2650_HS_H    0x3020                
#define OV2650_HS_L    0x3021                
#define OV2650_VS_H    0x3022                
#define OV2650_VS_L    0x3023                
#define OV2650_HW_H    0x3024                
#define OV2650_HW_L    0x3025                
#define OV2650_VH_H    0x3026                
#define OV2650_VH_L    0x3027                
#define OV2650_HTS_H   0x3028                        
#define OV2650_HTS_L   0x3029                        
#define OV2650_VTS_H   0x302A                        
#define OV2650_VTS_L   0x302B                        
#define OV2650_EXHTS   0x302C                        
#define OV2650_EXVTS_H 0x302D                        
#define OV2650_EXVTS_L 0x302E                        
#define OV2650_WET_0   0x3030                        
#define OV2650_WET_1   0x3031                        
#define OV2650_WET_2   0x3032                        
#define OV2650_WET_3   0x3033                        
#define OV2650_AHS_H   0x3038                        
#define OV2650_AHS_L   0x3039                        
#define OV2650_AVS_H   0x303A                        
#define OV2650_AVS_L   0x303B                        
#define OV2650_AHW_H   0x303C                        
#define OV2650_AHW_L   0x303D                        
#define OV2650_AVH_H   0x303E                        
#define OV2650_AVH_L   0x303F                        
#define OV2650_HISTO_0 0x3040                        
#define OV2650_HISTO_1 0x3041                        
#define OV2650_HISTO_2 0x3042                        
#define OV2650_HISTO_3 0x3043                        
#define OV2650_HISTO_4 0x3044                        
#define OV2650_BLC9A   0x3069                        
#define OV2650_BLCC    0x306C                
#define OV2650_BLCD    0x306D                
#define OV2650_BLCF    0x306F                
#define OV2650_BD50_L  0x3070                        
#define OV2650_BD50_H  0x3071                        
#define OV2650_BD60_L  0x3072                        
#define OV2650_BD60_H  0x3073                        
#define OV2650_TMC_0   0x3076                        
#define OV2650_TMC_1   0x3077                        
#define OV2650_TMC_2   0x3078                        
#define OV2650_TMC_4   0x307A                        
#define OV2650_TMC_6   0x307C                        
#define OV2650_TMC_8   0x307E                        
#define OV2650_TMC_I2C 0x3084                        
#define OV2650_TMC_10  0x3086                        
#define OV2650_TMC_11  0x3087                        
#define OV2650_ISP_XO_H        0x3088                
#define OV2650_ISP_XO_L        0x3089                
#define OV2650_ISP_YO_H        0x308A                
#define OV2650_ISP_YO_L        0x308B                
#define OV2650_TMC_12  0x308C                        
#define OV2650_TMC_13  0x308D                        
#define OV2650_EFUSE   0x308F                        
#define OV2650_IO_CTL_0        0x30B0                
#define OV2650_IO_CRL_1 0x30B1                       
#define OV2650_IO_CTL_2 0x30B2                       
#define OV2650_LAEC            0x30F0                
#define OV2650_GRP_EOP 0x30FF                        
                                                     
/* SC registers */                                   
#define OV2650_SC_CTL_0        0x3100                
#define OV2650_SC_SYN_CTL_0 0x3104                   
#define OV2650_SC_SYN_CTL_1 0x3105                   
#define OV2650_SC_SYN_CTL_3 0x3107                   
#define OV2650_SC_SYN_CTL_4 0x3108                   
                                                     
/* DSP control register */                           
#define OV2650_ISP_CTL_0       0x3300                
#define OV2650_ISP_CTL_1       0x3301                
#define OV2650_ISP_CTL_2       0x3302                
#define OV2650_ISP_CTL_3       0x3303                
#define OV2650_ISP_CTL_4       0x3304                
#define OV2650_ISP_CTL_5       0x3305                
#define OV2650_ISP_CTL_6       0x3306                
#define OV2650_ISP_CTL_7       0x3307                
#define OV2650_ISP_CTL_8       0x3308                
#define OV2650_ISP_CTL_9       0x3309                
#define OV2650_ISP_CTL_A       0x330A                
#define OV2650_ISP_CTL_B       0x330B                
#define OV2650_ISP_CTL_10      0x3310                
#define OV2650_ISP_CTL_11      0x3311                
#define OV2650_ISP_CTL_12      0x3312                
#define OV2650_ISP_CTL_13      0x3313                
#define OV2650_ISP_CTL_14      0x3314                
#define OV2650_ISP_CTL_15      0x3315                
#define OV2650_ISP_CTL_16      0x3316                
#define OV2650_ISP_CTL_17      0x3317                
#define OV2650_ISP_CTL_18      0x3318                
#define OV2650_ISP_CTL_19      0x3319                
#define OV2650_ISP_CTL_1A      0x331A                
#define OV2650_ISP_CTL_1B      0x331B                
#define OV2650_ISP_CTL_1C      0x331C                
#define OV2650_ISP_CTL_1D      0x331D                
#define OV2650_ISP_CTL_1E      0x331E                
#define OV2650_ISP_CTL_20      0x3320                
#define OV2650_ISP_CTL_21      0x3321                
#define OV2650_ISP_CTL_22      0x3322                
#define OV2650_ISP_CTL_23      0x3323                
#define OV2650_ISP_CTL_24      0x3324                
#define OV2650_ISP_CTL_27      0x3327                
#define OV2650_ISP_CTL_28      0x3328                
#define OV2650_ISP_CTL_29      0x3329                
#define OV2650_ISP_CTL_2A      0x332A                
#define OV2650_ISP_CTL_2B      0x332B                
#define OV2650_ISP_CTL_2C      0x332C                
#define OV2650_ISP_CTL_2D      0x332D                
#define OV2650_ISP_CTL_2E      0x332E                
#define OV2650_ISP_CTL_2F      0x332F                
#define OV2650_ISP_CTL_30      0x3330                
#define OV2650_ISP_CTL_31      0x3331                
#define OV2650_ISP_CTL_32      0x3332                
#define OV2650_ISP_CTL_33      0x3333                
#define OV2650_ISP_CTL_34      0x3334                
#define OV2650_ISP_CTL_35      0x3335                
#define OV2650_ISP_CTL_36      0x3336                
#define OV2650_ISP_CTL_40      0x3340                
#define OV2650_ISP_CTL_41      0x3341                
#define OV2650_ISP_CTL_42      0x3342                
#define OV2650_ISP_CTL_43      0x3343                
#define OV2650_ISP_CTL_44      0x3344                
#define OV2650_ISP_CTL_45      0x3345                
#define OV2650_ISP_CTL_46      0x3346                
#define OV2650_ISP_CTL_47      0x3347                
#define OV2650_ISP_CTL_48      0x3348                
#define OV2650_ISP_CTL_49      0x3349                
#define OV2650_ISP_CTL_4A      0x334A                
#define OV2650_ISP_CTL_4B      0x334B                
#define OV2650_ISP_CTL_4C      0x334C                
#define OV2650_ISP_CTL_4D      0x334D                
#define OV2650_ISP_CTL_4E      0x334E                
#define OV2650_ISP_CTL_4F      0x334F                
#define OV2650_ISP_CTL_50      0x3350                
#define OV2650_ISP_CTL_51      0x3351                
#define OV2650_ISP_CTL_52      0x3352                
#define OV2650_ISP_CTL_53      0x3353                
#define OV2650_ISP_CTL_54      0x3354                
#define OV2650_ISP_CTL_55      0x3355                
#define OV2650_ISP_CTL_56      0x3356                
#define OV2650_ISP_CTL_57      0x3357                
#define OV2650_ISP_CTL_58      0x3358                
#define OV2650_ISP_CTL_59      0x3359                
#define OV2650_ISP_CTL_5A      0x335A                
#define OV2650_ISP_CTL_5B      0x335B                
#define OV2650_ISP_CTL_5C      0x335C                
#define OV2650_ISP_CTL_5D      0x335D                
#define OV2650_ISP_CTL_5E      0x335E                
#define OV2650_ISP_CTL_5F      0x335F                
#define OV2650_ISP_CTL_60      0x3360                
#define OV2650_ISP_CTL_61      0x3361                
#define OV2650_ISP_CTL_62      0x3362                
#define OV2650_ISP_CTL_63      0x3363                
#define OV2650_ISP_CTL_64      0x3364                
#define OV2650_ISP_CTL_65      0x3365                
#define OV2650_ISP_CTL_6A      0x336A                
#define OV2650_ISP_CTL_6B      0x336B                
#define OV2650_ISP_CTL_6C      0x336C                
#define OV2650_ISP_CTL_6E      0x336E                
#define OV2650_ISP_CTL_71      0x3371                
#define OV2650_ISP_CTL_72      0x3372                
#define OV2650_ISP_CTL_73      0x3373                
#define OV2650_ISP_CTL_74      0x3374                
#define OV2650_ISP_CTL_75      0x3375                
#define OV2650_ISP_CTL_76      0x3376                
#define OV2650_ISP_CTL_77      0x3377                
#define OV2650_ISP_CTL_78      0x3378                
#define OV2650_ISP_CTL_79      0x3379                
#define OV2650_ISP_CTL_7A      0x337A                
#define OV2650_ISP_CTL_7B      0x337B                
#define OV2650_ISP_CTL_7C      0x337C                
#define OV2650_ISP_CTL_80      0x3380                
#define OV2650_ISP_CTL_81      0x3381                
#define OV2650_ISP_CTL_82      0x3382                
#define OV2650_ISP_CTL_83      0x3383                
#define OV2650_ISP_CTL_84      0x3384                
#define OV2650_ISP_CTL_85      0x3385                
#define OV2650_ISP_CTL_86      0x3386                
#define OV2650_ISP_CTL_87      0x3387                
#define OV2650_ISP_CTL_88      0x3388                
#define OV2650_ISP_CTL_89      0x3389                
#define OV2650_ISP_CTL_8A      0x338A                
#define OV2650_ISP_CTL_8B      0x338B                
#define OV2650_ISP_CTL_8C      0x338C                
#define OV2650_ISP_CTL_8D      0x338D                
#define OV2650_ISP_CTL_8E      0x338E                
#define OV2650_ISP_CTL_90      0x3390                
#define OV2650_ISP_CTL_91      0x3391                
#define OV2650_ISP_CTL_92      0x3392                
#define OV2650_ISP_CTL_93      0x3393                
#define OV2650_ISP_CTL_94      0x3394                
#define OV2650_ISP_CTL_95      0x3395                
#define OV2650_ISP_CTL_96      0x3396                
#define OV2650_ISP_CTL_97      0x3397                
#define OV2650_ISP_CTL_98      0x3398                
#define OV2650_ISP_CTL_99      0x3399                
#define OV2650_ISP_CTL_9A      0x339A                
#define OV2650_ISP_CTL_A0      0x33A0                
#define OV2650_ISP_CTL_A1      0x33A1                
#define OV2650_ISP_CTL_A2      0x33A2                
#define OV2650_ISP_CTL_A3      0x33A3                
#define OV2650_ISP_CTL_A4      0x33A4                
#define OV2650_ISP_CTL_A5      0x33A5                
#define OV2650_ISP_CTL_A6      0x33A6                
#define OV2650_ISP_CTL_A7      0x33A7                
#define OV2650_ISP_CTL_A8      0x33A8                
#define OV2650_ISP_CTL_AA      0x33AA                
#define OV2650_ISP_CTL_AB      0x33AB                
#define OV2650_ISP_CTL_AC      0x33AC                
#define OV2650_ISP_CTL_AD      0x33AD                
#define OV2650_ISP_CTL_AE      0x33AE                
#define OV2650_ISP_CTL_AF      0x33AF                
#define OV2650_ISP_CTL_B0      0x33B0                
#define OV2650_ISP_CTL_B1      0x33B1                
#define OV2650_ISP_CTL_B2      0x33B2                
#define OV2650_ISP_CTL_B3      0x33B3                
#define OV2650_ISP_CTL_B4      0x33B4                
#define OV2650_ISP_CTL_B5      0x33B5                
#define OV2650_ISP_CTL_B6      0x33B6                
#define OV2650_ISP_CTL_B7      0x33B7                
#define OV2650_ISP_CTL_B8      0x33B8                
#define OV2650_ISP_CTL_B9      0x33B9                
                                                     
/* Format register */                                
#define OV2650_FMT_CTL_0       0x3400                
#define OV2650_FMT_CTL_1       0x3401                
#define OV2650_FMT_CTL_2       0x3402                
#define OV2650_FMT_CTL_3       0x3403                
#define OV2650_FMT_CTL_4       0x3404                
#define OV2650_FMT_CTL_5       0x3405                
#define OV2650_FMT_CTL_6       0x3406                
#define OV2650_FMT_CTL_7       0x3407                
#define OV2650_FMT_CTL_8       0x3408                
#define OV2650_DITHER_CTL      0x3409                
#define OV2650_DVP_CTL_0       0x3600                
#define OV2650_DVP_CTL_1       0x3601                
#define OV2650_DVP_CTL_6       0x3606                
#define OV2650_DVP_CTL_7       0x3607                
#define OV2650_DVP_CTL_9       0x3609                
#define OV2650_DVP_CTL_B       0x360B                
                                                     
/* General definition for ov2650 */                  
#define OV2650_OUTWND_MAX_H            UXGA_SIZE_H   
#define OV2650_OUTWND_MAX_V            UXGA_SIZE_V  

#define SYS_RESET              0x80 
#define CMATRIX_LEN            6 

#endif
