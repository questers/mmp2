#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/input-polldev.h>

#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/leds.h>
#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif


#undef TOUCHKEY_DEBUG

//comment next line to disable verbose debug messages
//#define TOUCHKEY_DEBUG 
#define RELY_ON_IRQ
 
/*Configuration Registers*/
#define REG_CONFIG_INTERFACE 	0x0000
#define REG_CONFIG_GENERAL   	0x0001
#define REG_CONFIG_BUTTON    	0x0004
#define REG_CONFIG_GPIO			0x000E
#define REG_CONFIG_SENSITIVITY1	0x0010
#define REG_CONFIG_SENSITIVITY2	0x0011
#define REG_CONFIG_MAPPING		0x001E
#define REG_CONFIG_TIMER		0x001F
#define REG_CONFIG_LED_ENABLE	0x0022
#define REG_CONFIG_LED_EFFECT	0x0023
#define REG_CONFIG_LED_CONTROL1	0x0024
#define REG_CONFIG_LED_CONTROL2	0x0025
/*Data Registers*/
#define REG_DATA_GPIO			0x0108
#define REG_DATA_BUTTON			0x0109
#define REG_DATA_TIMER			0x010B
#define REG_DATA_PRESSURE1		0x010C
#define REG_DATA_PRESSURE2		0x010D
/*Reset Registers*/
#define REG_RESET				0x0300


#define LED_MODE_COMBO			0x1

#define LED_MODE (LED_MODE_COMBO)

#define LED_MAX_BRIGHTNESS		20
#define MAX_TOUCH_BUTTONS		4

#define LED_IDLE_TIME  2000
#define LED_ONOFF_TIME 100 

#define LED_COMMAND_NONE 0
#define LED_COMMAND_SET_BRIGHTNESS 1

#define LED_BRIGHTNESS_OFFSET      3

/*driver private object*/
struct so340010_priv
{
	struct i2c_client* client;
	struct device* dev;
	struct input_dev *idev;
	
	//work queue  
	struct workqueue_struct* wq;
	struct delayed_work delayed_work;

	//user space access
	struct led_classdev led;
	struct work_struct command_work;
	u32 command;
	u32 command_data;

	int ref_count;
#ifdef RELY_ON_IRQ
	int irq;
#endif
	u8 suspended;
	//led control
	u8 led_mode;
	u8 max_brightness;
	u8 led_on;

	//idle work
	struct delayed_work idle_work;

	u32 touch_state,old_touch_state;
	
	struct early_suspend    early_suspend;
};



static int so340010_slave_read(struct so340010_priv* priv, u16 addr, u16* data)
{
	struct i2c_client* client = priv->client;
	struct device* dev = &client->dev;
	int ret;
	u8 buf[4];	
	struct i2c_msg msgs[2];

	//fill register 
	buf[0] = (addr>>8)&0xff;
	buf[1] = addr&0xff;
	
	msgs[0].addr = client->addr;
	msgs[0].flags = 0;
	msgs[0].buf = buf;
	msgs[0].len = 2;

	msgs[1].addr = client->addr;
	msgs[1].flags = I2C_M_RD;
	msgs[1].buf = buf+2;//skip register offset
	msgs[1].len = 2;

	ret = i2c_transfer(client->adapter, msgs, 2);
	if (ret < 0 )
	{
		dev_err(dev, "Error reading so340010 slave\n");
		return -1;
	}


	if(data)
		*data = (buf[2]<<8)+buf[3];
	
	return 0;
}

static int so340010_slave_write(struct so340010_priv* priv, u16 addr, u16 data)
{
	struct i2c_msg msg;
	struct i2c_client* client = priv->client;
	struct device* dev = &client->dev;
	int ret;
	u8 buf[4];

	
	msg.addr = client->addr;
	msg.flags = 0;
	msg.buf = buf;
	buf[0] = (addr>>8)&0xff;
	buf[1] = addr&0xff;
	buf[2] = (data>>8)&0xff;
	buf[3] =  data&0xff;
	msg.len = 4;
	ret = i2c_transfer(client->adapter, &msg, 1);
	if (ret != 1)
	{
		dev_err(dev, "Error writting so340010 slave\n");
		return -1;
	}
	
	return 0;
}

static int so340010_state_process(struct so340010_priv* priv)
{
	int i;
	int ret;
	u16 data;
	int code[4] = {KEY_F7, KEY_F8, KEY_F6, KEY_F9}; /* MENU, HOME, BACK */
	#ifdef TOUCHKEY_DEBUG
	static const char* scan_code[]={"F7","F8","F6","F9"};
	#endif
	ret = so340010_slave_read(priv, REG_DATA_BUTTON, &data); /* button state register */
	if (ret)
	{
		printk("read touchkey failed\n");
		return -1;
	}

	priv->touch_state = data;

	
	if(priv->touch_state&&priv->led_mode&LED_MODE_COMBO)
	{
		cancel_delayed_work_sync(&priv->idle_work);
		if(!priv->led_on)
		{	
			#ifdef TOUCHKEY_DEBUG
			printk("light on touchkey led\n");
			#endif
			so340010_slave_write(priv,REG_CONFIG_GPIO,0x0f0f);		
			priv->led_on = 1;  
		}
	}
	else if(priv->old_touch_state&&priv->led_on&&priv->led_mode&LED_MODE_COMBO)
	{
		//run ilde work		
		queue_delayed_work(priv->wq,&priv->idle_work, msecs_to_jiffies(LED_IDLE_TIME));
	}

	#ifdef TOUCHKEY_DEBUG
	printk("so340010_state_process state=%08x,old=%08x\n",priv->touch_state,priv->old_touch_state);
	#endif
	for (i=0; i<MAX_TOUCH_BUTTONS; i++)
	{

		if ((priv->touch_state&(1<<i))&&!(priv->old_touch_state&(1<<i)))
		{
			#ifdef TOUCHKEY_DEBUG
			printk("Button %s pressed.\n", scan_code[i]);
			#endif
			input_report_key(priv->idev, code[i], 1);
		}

		if (!(priv->touch_state&(1<<i))&&(priv->old_touch_state&(1<<i)))
		{
			#ifdef TOUCHKEY_DEBUG
			printk("Button %s released.\n", scan_code[i]);
			#endif
			input_report_key(priv->idev, code[i], 0);
		}
		input_sync(priv->idev);
	}

	priv->old_touch_state = priv->touch_state;
	
	return 0;
}


static void so340010_idle_work(struct work_struct* work)
{
	struct so340010_priv *priv = (struct so340010_priv*)container_of(work,
		struct so340010_priv, idle_work.work);
	//ramp dwon to 0	
	#ifdef TOUCHKEY_DEBUG
	printk("light off touchkey led\n");
	#endif
	
	so340010_slave_write(priv,REG_CONFIG_GPIO,0x0f00);
	priv->led_on = 0;

}

	
static int so340010_chip_init(struct so340010_priv* so340010,int timeout_ms);
static void so340010_delayed_work(struct work_struct* work)
{

	struct so340010_priv *priv = (struct so340010_priv*)container_of(work,
		struct so340010_priv, delayed_work.work);
	int ret;
	
	ret = so340010_state_process(priv);
#ifdef RELY_ON_IRQ
	if(ret) //read error,then trying to resume device
	{
		so340010_chip_init(priv,50);
	}
	if(gpio_get_value(irq_to_gpio(priv->irq)))
	{
		enable_irq(priv->irq);
	}
	else
		queue_delayed_work(priv->wq,&priv->delayed_work, msecs_to_jiffies(100));
#else
	queue_delayed_work(priv->wq,&priv->delayed_work, msecs_to_jiffies(100));
#endif	
}

#ifdef RELY_ON_IRQ
static irqreturn_t so340010_irq(int irq, void *irq_data)
{
	struct so340010_priv* priv = (struct so340010_priv*)irq_data;
	//printk("so340010_irq\n");
	disable_irq_nosync(priv->irq);
	// Ellie commented out because unnecessary--irq has been disabled
	//cancel_delayed_work(&priv->delayed_work);
	queue_delayed_work(priv->wq,&priv->delayed_work, msecs_to_jiffies(0));
	
	return IRQ_HANDLED;
}
#endif

static int so340010_input_open(struct input_dev *idev)
{
	struct so340010_priv *so340010 = (struct so340010_priv*)input_get_drvdata(idev);
	struct device* dev = so340010->dev;

#ifdef CONFIG_PM
	if (so340010->suspended){
		dev_dbg(dev, "touch is suspended!\n");
		return -1;
	}
#endif

	if (so340010->ref_count++ > 0)
	{
		dev_dbg(dev, "Touch is opened. Use count: %d\n", so340010->ref_count);
		return 0;
	}	

	
	so340010->touch_state = so340010->old_touch_state = 0;
	
	return 0;
}

static void so340010_input_close(struct input_dev *idev)
{
	struct so340010_priv* so340010 = (struct so340010_priv *) input_get_drvdata(idev);
#ifdef CONFIG_PM
	if (so340010->suspended){
		pr_info("touch is suspended!\n");
		return;
	}
#endif

}

static void so340010_command_process(struct work_struct* work)
{
	struct so340010_priv *priv = (struct so340010_priv*)container_of(work,struct so340010_priv, command_work);

	switch(priv->command)
	{
		case LED_COMMAND_SET_BRIGHTNESS:
			{
				u16 brightness;
				priv->max_brightness = (u8)priv->command_data;			
				/* effect/brightness */ 				
				brightness = (priv->max_brightness<<8)+priv->max_brightness;
				so340010_slave_write(priv,REG_CONFIG_LED_CONTROL1,brightness);
				so340010_slave_write(priv,REG_CONFIG_LED_CONTROL2,brightness);
			}
			break;
		default:			
			break;
	}
	
}

static void so340010_led_set(struct led_classdev *led_cdev,
			 enum led_brightness value)
{
	struct so340010_priv *priv = (struct so340010_priv*)container_of(led_cdev,
		struct so340010_priv, led);
	if(priv)
	{
		priv->command = LED_COMMAND_SET_BRIGHTNESS;
		priv->command_data = value>>LED_BRIGHTNESS_OFFSET;
		//disable it to prevent brightness can be set to zero by android framework
		//queue_work(priv->wq,&priv->command_work);
	}

}
static enum led_brightness so340010_led_get(struct led_classdev *led_cdev)
{
	struct so340010_priv *priv = (struct so340010_priv*)container_of(led_cdev,
		struct so340010_priv, led);
	if(priv)
	{
		return priv->max_brightness<<LED_BRIGHTNESS_OFFSET;		
	}
	return LED_OFF;

}

#ifdef RELY_ON_IRQ
static int so340010_poll_attention(struct so340010_priv* priv,int timeout_ms)
{
	int state;
	do
	{
		state = gpio_get_value(irq_to_gpio(priv->irq));
		if(!state)
			return 0;
		if(!timeout_ms) break;
		mdelay(1);
		
	}while(--timeout_ms);

	printk(KERN_WARNING"polling so340010 attention failed\n");
	return 0;

}
#endif

static int so340010_chip_init(struct so340010_priv* so340010,int timeout_ms)
{
	u16 data,brightness;
#ifdef RELY_ON_IRQ
	if(so340010_poll_attention(so340010,timeout_ms))
		goto init_fail;
#endif
	/* configure cap button */
	if(so340010_slave_write(so340010, REG_CONFIG_INTERFACE, 0x0007)) /* data enable and attention mode */
		goto init_fail;
	/*normal mode,strongest button only mode*/
	if(so340010_slave_write(so340010, REG_CONFIG_GENERAL, 0x0010))
		goto init_fail;

	/* enable s0,s1,s2 buttons */
	if(so340010_slave_write(so340010, REG_CONFIG_BUTTON, 0x0007))
		goto init_fail;

	/*
	 * For unused pin,use low driving output;
	 *	gpio output to driver led with on effect
	 */
	if(so340010_slave_write(so340010, REG_CONFIG_GPIO, 0x0f00))
		goto init_fail;

	/*with maximum sensitivity for all sensor pins,ignored by disabled sensor pins*/
	if(so340010_slave_write(so340010, REG_CONFIG_SENSITIVITY1, 0x8888))
		goto init_fail;;
	if(so340010_slave_write(so340010, REG_CONFIG_SENSITIVITY2, 0x8888))
		goto init_fail;

	/*in combo mode,led is controlled by driver,otherwise it's controlled by chip itself*/
	if(LED_MODE_COMBO&so340010->led_mode)
	{
		if(so340010_slave_write(so340010, REG_CONFIG_MAPPING, 0x0000))
			goto init_fail;
	}
	else
	{
		/*toggle on touch mode,map to gpio data bit,s0,s1,s2 mapped gpio0,gpio1,gpio2*/
		if(so340010_slave_write(so340010, REG_CONFIG_MAPPING, 0x400f))
			goto init_fail;
	}

	/*disable timer function*/
	if(so340010_slave_write(so340010, REG_CONFIG_TIMER, 0x0000))
		goto init_fail;

	/*enable GPO0,GPO1,GPO2 led driving*/	
	if(so340010_slave_write(so340010, REG_CONFIG_LED_ENABLE, 0x000f))
		goto init_fail;

	/* period A and B, 80*12.5ms = 1000ms */
	if(so340010_slave_write(so340010, REG_CONFIG_LED_EFFECT, 0x5050))
		goto init_fail;

	/* effect/brightness */	
	brightness = (so340010->max_brightness<<8)+so340010->max_brightness;
	if(so340010_slave_write(so340010,REG_CONFIG_LED_CONTROL1,brightness))
		goto init_fail;
	if(so340010_slave_write(so340010,REG_CONFIG_LED_CONTROL2,brightness))
		goto init_fail;

	/* dummy read to deassert ATTN pin*/
	if(so340010_slave_read(so340010, REG_DATA_GPIO, &data))
		goto init_fail;


	return 0;
init_fail:
	return -1;
	
}


static int so340010_suspend(struct i2c_client *client, pm_message_t mesg)
{
    struct so340010_priv *priv = i2c_get_clientdata(client);

	flush_delayed_work(&priv->delayed_work);
	flush_work(&priv->command_work);
	if(LED_MODE_COMBO&priv->led_mode)
	{		
		flush_delayed_work(&priv->idle_work);		
	}

	#ifdef RELY_ON_IRQ
	disable_irq(priv->irq);
	#endif
	
	/* turn off led abruptly */
	so340010_slave_write(priv,REG_CONFIG_LED_ENABLE,0x0);
	so340010_slave_write(priv,REG_CONFIG_GPIO,0x0);
	
	//set sleep mode
	so340010_slave_write(priv,REG_CONFIG_GENERAL,0x0010|0x80);
	priv->suspended = 1;

	
	return 0;
}

static int so340010_resume(struct i2c_client *client)
{
    struct so340010_priv *priv = i2c_get_clientdata(client);
	
	//clear sleep mode
	so340010_slave_write(priv,REG_CONFIG_GENERAL,0x0010);
	priv->suspended = 0;
	
	/* enable led */
	so340010_slave_write(priv,REG_CONFIG_LED_ENABLE,0xf);

#ifdef RELY_ON_IRQ
	if(gpio_get_value(irq_to_gpio(priv->irq)))
	{
		enable_irq(priv->irq);
	}
	else
#endif
	if(priv->wq)
	{
		queue_delayed_work(priv->wq,&priv->delayed_work, msecs_to_jiffies(100));
	}

	
	return 0;
}

#ifdef CONFIG_HAS_EARLYSUSPEND
static void so340010_early_suspend(struct early_suspend *h)
{
	struct so340010_priv *priv = container_of(h, struct so340010_priv, early_suspend);
	so340010_suspend(priv->client, PMSG_SUSPEND);

}
static void so340010_late_resume(struct early_suspend *h)
{
	struct so340010_priv *priv = container_of(h, struct so340010_priv, early_suspend);
	so340010_resume(priv->client);
}
#endif

static int __devinit so340010_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct device *dev = &client->dev;
	struct led_classdev *led_cdev;
	struct input_dev* input_device;
	int ret = 0;

	struct so340010_priv* priv = NULL;

	priv = kzalloc(sizeof(struct so340010_priv), GFP_KERNEL);
	if (!priv)
	{
		dev_err(dev, "Can't allocate private block\n");
		return -ENOMEM;
	}

	led_cdev = &priv->led;
	priv->client = client;
	priv->dev = dev;

	priv->led_mode = LED_MODE;
	priv->max_brightness = LED_MAX_BRIGHTNESS;
	priv->led_on = 0;

	
    i2c_set_clientdata(client, priv);

	priv->wq = create_singlethread_workqueue("touchkey");
	
	INIT_DELAYED_WORK(&priv->delayed_work,so340010_delayed_work);
	

	/* tell input system what we events we accept and register */
	input_device = input_allocate_device();
	if (NULL == input_device) {
		dev_err(dev, "Failed allocating input device\n");
		ret = -ENOMEM;
		goto err1;
	}

	input_device->name = "touchkey";
	input_device->phys = "touchkey/input1";
	input_device->dev.parent = dev;
	input_device->open = so340010_input_open;
	input_device->close = so340010_input_close;
	input_device->id.bustype = BUS_HOST;
	input_device->id.vendor  = 0xdead;
	input_device->id.product = 0xdead;
	__set_bit(EV_KEY,input_device->evbit);
	__set_bit(EV_SYN,input_device->evbit);
	__set_bit(KEY_F6,input_device->keybit);
	__set_bit(KEY_F7,input_device->keybit);
	__set_bit(KEY_F8,input_device->keybit);
	__set_bit(KEY_F9,input_device->keybit);
	priv->idev = input_device;
	input_set_drvdata(input_device, priv);
	ret = input_register_device(input_device);
	if (ret) {
		dev_err(dev, "Unabled to register input device, ret = %d\n", ret);
		ret = -ENOMEM;
		goto err2;
	}
	
#ifdef RELY_ON_IRQ	
	priv->irq = priv->client->irq;	
#endif
	//init onetouch now
	ret = so340010_chip_init(priv,50);
	if(ret)
	{
		dev_err(dev, "chip init failed, ret = %d\n", ret);	
		goto err3;
	}
#ifdef RELY_ON_IRQ
	//gpio_direction_input(irq_to_gpio(priv->irq));
	
	/* falling edge interrupt */
	ret = request_irq(priv->irq, 
		so340010_irq,
		IRQF_DISABLED|IRQF_TRIGGER_FALLING, 
		"touchkey",
		priv);
	if (ret) {
		dev_dbg(dev, "Request IRQ for capacitive button failed (%d).\n", ret);
		goto err3;
	}
	// Ellie added to fix the "unbalanced irq" warning
	disable_irq_nosync(priv->irq);

#endif

	if(LED_MODE_COMBO&priv->led_mode)
	{		
		INIT_DELAYED_WORK(&priv->idle_work,so340010_idle_work);
	}

	INIT_WORK(&priv->command_work,so340010_command_process);
	priv->command = LED_COMMAND_NONE;
	
	led_cdev = &priv->led;
	led_cdev->name = "button-backlight";
	led_cdev->brightness_set = so340010_led_set;
	led_cdev->brightness_get = so340010_led_get;

	ret = led_classdev_register(dev, led_cdev);
	if (ret) {
		dev_err(dev,"failed to register %s led\n",led_cdev->name);
		goto err3;
	}
	
	if(priv->wq)
		queue_delayed_work(priv->wq,&priv->delayed_work, msecs_to_jiffies(200));

	#ifdef CONFIG_HAS_EARLYSUSPEND
	priv->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN+1;
	priv->early_suspend.suspend = so340010_early_suspend;
	priv->early_suspend.resume = so340010_late_resume;
	register_early_suspend(&priv->early_suspend);
	#endif

	
	return 0;
	
err3:
	input_unregister_device(input_device);
err2:
	input_free_device(input_device);
err1:
	destroy_workqueue(priv->wq);
	kfree(priv);	
    i2c_set_clientdata(client, NULL);
	return ret;
}

static int so340010_remove(struct i2c_client *client)
{
    struct so340010_priv *so340010 = i2c_get_clientdata(client);

#ifdef CONFIG_HAS_EARLYSUSPEND
	unregister_early_suspend(&so340010->early_suspend);
#endif

	if(LED_MODE_COMBO&so340010->led_mode)
	{		
		cancel_delayed_work(&so340010->idle_work);
	}
	cancel_delayed_work(&so340010->delayed_work);
	destroy_workqueue(so340010->wq);

	led_classdev_unregister(&so340010->led);
	

	if(so340010->idev)
	{
#ifdef RELY_ON_IRQ
		free_irq(so340010->irq,so340010);
#endif
		input_unregister_device(so340010->idev);
		input_free_device(so340010->idev);			
	}

	kfree(so340010);

	return 0;
}


static struct i2c_device_id so340010_idtable[] = { 
	{ "so340010", 0 }, 
	{ } 
}; 

static struct i2c_driver so340010_driver = {
	.driver = {
		.name 	= "so340010",
	},
	.id_table	= so340010_idtable,
	.probe		= so340010_probe,
	.remove		= __devexit_p(so340010_remove),
	
#ifdef CONFIG_PM
#ifndef CONFIG_HAS_EARLYSUSPEND
		.suspend  = so340010_suspend,
		.resume   = so340010_resume,
#endif
#endif
};

static int __init so340010_init(void)
{
	printk("SO340010 Capacitive Sensing Button Driver\n");
	return i2c_add_driver(&so340010_driver);
}

static void __exit so340010_exit(void)
{
	i2c_del_driver(&so340010_driver);
}

module_init(so340010_init);
module_exit(so340010_exit);

MODULE_DESCRIPTION("SO340010 Capacitive Sensing Button Driver");
MODULE_AUTHOR("Guang <pengguang001@gmail.com");
MODULE_LICENSE("GPL");

