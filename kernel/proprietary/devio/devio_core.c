#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/debugfs.h>
#include <linux/io.h>

#ifndef CONFIG_DEBUG_FS
#error devio driver need CONFIG_DEBUG_FS support
#endif
#ifdef CONFIG_DEBUG_FS
static struct dentry *debugfs_root;
#endif


#define MAX_DEVIO_BUFFER_SIZE (4*PAGE_SIZE)
struct devio_space_t
{
	const char* 	ioname;
	unsigned long	base;
	unsigned long   offset;
	unsigned long 	step;
	unsigned long	size;
	u32  (*hook_read)(struct devio_space_t* iospace,u32 addr);
	void (*hook_write)(struct devio_space_t* iospace,u32 val,u32 addr);
	
};


struct devio_dev
{	
	struct devio_space_t* dev_space;
	struct dentry *debugfs_reg;
	struct list_head list;
};

//include hook api here
#include "hdmi_hook.c"

static struct devio_space_t builtin_devio[] = 
{
	//name,  	base,    		offset,	step,      size,  	read hook,  write hook
	{"mfp",		0xfe01e000,	0,		4,		0x2c4,	0,			0},
	{"pwm1",	0xfe01a000,	0,		4,		0xc,	0,			0},
	{"pwm2",	0xfe01a400,	0,		4,		0xc,	0,			0},
	{"pwm3",	0xfe01a800, 0,		4,		0xc,	0,			0},	
	{"pwm4",	0xfe01ac00, 0,		4,		0xc,	0,			0},
	{"soc",		0xfe282c00, 0,		4,		0x6c,	0,			0},	
	{"gpio0", 	0xfe019000, 0,		12,		0xa8,	0,			0}, 
	{"gpio1", 	0xfe019004, 0,		12,		0xa8,	0,			0}, 
	{"gpio2", 	0xfe019008, 0,		12,		0xa8,	0,			0}, 
	{"gpio3", 	0xfe019100, 0,		12,		0xa8,	0,			0}, 
	{"gpio4",	0xfe019104, 0,		12,		0xa8,	0,			0}, 
	{"gpio5",	0xfe019108, 0,		12,		0xa8,	0,			0}, 
	
	{"hdmi_phy",0xfe20bc00, 8,		4,		0x30,	0,			0},		
	{"hdmi_tx",	0xfe20bc00, 1,		1,		0x13e,	hdmi_tx_read,hdmi_tx_write},	
	{"apbclock",0xfe015000,	0,		4,		0xa4,	0,			0},		
	
};


static LIST_HEAD(devio_list);

static ssize_t devio_show(struct devio_dev *iodev, char *buf)
{
	int i,count=0;
	struct devio_space_t* iospace=iodev->dev_space;
	count += sprintf(buf, "%s registers,base=%#x,offset=%#x,step=%#x\n", iodev->dev_space->ioname,iospace->base,iospace->offset,iospace->step);
	for(i=iospace->offset;i<=iospace->size;i+=iospace->step)
	{
		if(iospace->hook_read)
		{
			count += sprintf(buf + count,"%#x: %#x \n",i,iospace->hook_read(iospace,i));			
		}
		else
		{
			count += sprintf(buf + count,"%#x: %#x \n",i,__raw_readl(iospace->base+i));
		}
	}
	return count;
}

static int devio_open_file(struct inode *inode, struct file *file)
{
	file->private_data = inode->i_private;
	return 0;
}

static ssize_t  devio_read_file(struct file *file, char __user *user_buf,
			       size_t count, loff_t *ppos)
{
	ssize_t ret;
	struct devio_dev *iodev = file->private_data;
	char *buf = kmalloc(MAX_DEVIO_BUFFER_SIZE, GFP_KERNEL);	
	ret = devio_show(iodev, buf);
	if (ret >= 0)
		ret = simple_read_from_buffer(user_buf, count, ppos, buf, ret);
	kfree(buf);
	return ret;
}

static ssize_t  devio_write_file(struct file *file,
		const char __user *user_buf, size_t count, loff_t *ppos)
{
	char buf[32];
	int buf_size;
	char *start = buf;
	unsigned long reg, value;
	struct devio_dev *iodev = file->private_data;
	struct devio_space_t* iospace=iodev->dev_space;
	

	buf_size = min(count, (sizeof(buf)-1));
	if (copy_from_user(buf, user_buf, buf_size))
		return -EFAULT;
	buf[buf_size] = 0;

	while (*start == ' ')
		start++;
	reg = simple_strtoul(start, &start, 16);
	if (reg >= (iospace->base+iospace->size) )
	{
		printk("address out of dev io space\n");
		return -EINVAL;
	}
	while (*start == ' ')
		start++;
	if (strict_strtoul(start, 16, &value))
		return -EINVAL;
	//permission granted?
	printk("write %#x to %#x\n",value,reg);
	if(iospace->hook_write)
		iospace->hook_write(iospace,value,reg);
	else
	__raw_writel(value,(void __iomem *)reg);

	return buf_size;
}


static const struct file_operations devio_reg_fops = {
	.open =  devio_open_file,
	.read =  devio_read_file,
	.write =  devio_write_file,
};

static int register_devio(struct devio_space_t* iospace)
{
	struct devio_dev* iodev = kzalloc(sizeof(struct devio_dev),GFP_KERNEL);
	if(!iodev)
		return -ENOMEM;
	iodev->dev_space = iospace;
	iodev->debugfs_reg = debugfs_create_file(iodev->dev_space->ioname, 0644,
						 debugfs_root, iodev,
						 &devio_reg_fops);
	list_add(&iodev->list,&devio_list);
	return 0;
}

static int register_devio_group(struct devio_space_t* iospace,int space_num)
{
	int ret,i;
	ret=0;
	for(i=0;i<space_num;i++)
	{
		ret = register_devio(iospace++);
		if(ret)
			return ret;
	}

	return 0;
}
int __init devio_init(void)
{
	debugfs_root = debugfs_create_dir("devio", NULL);
	if (IS_ERR(debugfs_root) || !debugfs_root) {
		printk(KERN_WARNING
		       "devio: Failed to create debugfs directory\n");
		debugfs_root = NULL;
	}

	return register_devio_group(builtin_devio,ARRAY_SIZE(builtin_devio));

}

void __exit devio_exit(void)
{
	//TODO ,list destroy
	debugfs_remove_recursive(debugfs_root);
}


MODULE_DESCRIPTION("device io access driver");
MODULE_AUTHOR("Raymond Wang<raymond1860@gmail.com");
MODULE_LICENSE("GPL");


module_init(devio_init);
module_exit(devio_exit);


