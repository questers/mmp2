
#define HDMI_DATA_PORT   (0x00)
#define HDMI_ADDR_PORT   (0x04)

u32  hdmi_tx_read(struct devio_space_t* iospace,u32 addr)
{
	int result;
	__raw_writel((addr | 1 << 30), iospace->base + HDMI_ADDR_PORT);
	result = __raw_readl(iospace->base + HDMI_DATA_PORT) & 0xff;
//	printk("hdmi_tx_read addr[%#x]=%#x\n",addr,result);
	return result;

}
void hdmi_tx_write(struct devio_space_t* iospace,u32 val,u32 addr)
{	
//	printk("hdmi_tx_write addr[%#x]=%#x\n",addr,val);
	__raw_writel((val & 0xff), iospace->base + HDMI_DATA_PORT);
	__raw_writel((addr | 1 << 31), iospace->base + HDMI_ADDR_PORT);

}

