/*
 *  sensorsmanager.c - sensor manager under linux kernel layer
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/input.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/workqueue.h>
#include <linux/delay.h>
#include <linux/mutex.h>
#include <linux/wait.h>
#include <linux/freezer.h>
#include <linux/uaccess.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include "sensormanager.h"
#include <asm/div64.h>

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif


/* #define PEERFORMANCE */
struct sensormanager
{

    struct mutex lock;
    //
    u32 sensor_enabled_mask;
    u32 sensor_change_mask;

    //some queued work needs to be done
    struct work_struct queuedwork;

    //work queue for all sensors work
    struct workqueue_struct* wq;

    //sensor list
    struct list_head sensor_list;

    //latch all sensor on/off
    int latch;

#ifdef CONFIG_HAS_EARLYSUSPEND
	struct early_suspend  early_suspend;
#endif
};

static struct sensormanager *sm_p;
#if 0
static int sensors_input_open(struct input_dev *input)
{
    struct sensormanager *sm = input_get_drvdata(input);

    struct sensor_dev *sensor;
    struct sensor_dev *tmp;

    mutex_lock(&sm->lock);

    if(sm)
    {
        if (sm->refcount== 0)
        {
            list_for_each_entry_safe(sensor, tmp, &sm->sensor_list, list)
            {
                if((sensor->enabled)&&(!sensor->on))
                {
                    sensor->poweron();
                }
                sensor->on = 1;
            }
        }
        //only add the reference counter if already open
        sm->refcount++;

    }

    mutex_unlock(&sm->lock);
    return 0;
}

static void sensors_input_close(struct input_dev *input)
{
    struct sensormanager *sm = input_get_drvdata(input);
    struct sensor_dev *sensor;
    struct sensor_dev *tmp;

    mutex_lock(&sm->lock);

    if (sm)
    {
        sm->refcount--;
        if (sm->refcount== 0)
        {
            list_for_each_entry_safe(sensor, tmp, &sm->sensor_list, list)
            {
                if(sensor->on)
                {
                    if(sensor->poweroff)
                        sensor->poweroff();
                    sensor->count = 0;
                    sensor->on = 0;
                }
            }
        }
    }
    mutex_unlock(&sm->lock);
}
#endif
/*
struct sensor_dev* sensor_add(u32 type, char *name,
                              void (*poweron)(struct sensor_dev* sensor),
                              void (*poweroff)(struct sensor_dev* sensor))
{
    struct sensormanager* sm=sm_p;
    struct sensor_dev *sensor;

    if(!sm) return NULL;

    sensor =kzalloc(sizeof(struct sensor_dev), GFP_KERNEL);

    if(!sensor)
        return NULL;

    sensor->type = type;
    sensor->name = name;
    sensor->poweron = poweron;
    sensor->poweroff = poweroff;
    sensor->delay = DEFAULT_POLLING_DELAY_MS;
    printk("sensor_add\n");
    mutex_lock(&sm->lock);
    list_add(&sensor->list, &sm->sensor_list);

    //set sensor device default attribute
    sensor->enabled = 1;
    sensor->on = 0;
    mutex_unlock(&sm->lock);
    return sensor;
}*/

int sensor_add(struct sensor_dev* sensor)
{
    struct sensormanager* sm=sm_p;
	if(!sensor||!sm) return -EINVAL;
	printk("sensor[%s] was added\n", sensor->name);
	mutex_lock(&sm->lock);
	list_add(&sensor->list, &sm->sensor_list);
	
	sensor->on = 0;
	mutex_unlock(&sm->lock);
	return 0;	
}

EXPORT_SYMBOL(sensor_add);

void sensor_remove(char *name)
{
    struct sensormanager* sm=sm_p;

    struct sensor_dev *sensor;
    struct sensor_dev *tmp;
    if(!sm) return;
    mutex_lock(&sm->lock);
    list_for_each_entry_safe(sensor, tmp, &sm->sensor_list, list)
    {
        if(!strcmp(name, sensor->name))
        {
            printk(KERN_WARNING "sensor[%s] was removed\n",name);
            list_del(&sensor->list);
            kfree(sensor);
            mutex_unlock(&sm->lock);
            return;
        }
    }
    mutex_unlock(&sm->lock);
    printk(KERN_ERR "Try to remove a unknow sensor: %s\n", name);
}

EXPORT_SYMBOL(sensor_remove);

int sensor_queue_work(struct work_struct *work)
{
    if(sm_p)
        return queue_work(sm_p->wq,work);
    return -1;
}
EXPORT_SYMBOL(sensor_queue_work);

int sensor_queue_delayed_work(struct delayed_work *work,unsigned long delay)
{
    if(sm_p)
        return queue_delayed_work(sm_p->wq,work,delay);
    return -1;

}
EXPORT_SYMBOL(sensor_queue_delayed_work);

static char* sensor_name(int type)
{
	int i;
	static char* sensor_names[] = {
		"accelerometer",//ID_ACCELEROMETER,
		"magneticfiled",//ID_MAGNETIC_FIELD,
		"orientation",//ID_ORIENTATION,
		"gyroscope",//ID_GYROSCOPE,
		"light",//ID_LIGHT,
		"pressure",//ID_PRESSURE,
		"temperature",//ID_TEMPERATURE,
		"proximity",//ID_PROXIMITY,
		"gravity",//ID_GRAVITY,
		"linear acceleration",//ID_LINEAR_ACCELERATION,
		"rotation vector",//ID_ROTATION_VECTOR,
		"unknown",//ID_MAX
	};

	for (i=0; i<ID_MAX; i++)
	{
		if (type & (1<<i))
			return sensor_names[i];
	}

	return sensor_names[i];
}

#ifdef CONFIG_HAS_EARLYSUSPEND
static void sensor_manager_early_suspend(struct early_suspend *h)
{
	struct sensor_dev *sensor;
	struct sensormanager* sm = container_of(h, struct sensormanager, early_suspend);

	if (!list_empty(&sm->sensor_list))
	{
		mutex_lock(&sm->lock);
		
		list_for_each_entry(sensor,&sm->sensor_list,list)
		{
			sensor->on_before_suspend = sensor->on;
			if (sensor->on)
			{
				printk("on early suspend, [%s]->0\n", sensor_name(sensor->type));
				sensor->poweroff(sensor);
				sensor->on = 0;
			}
		}
		
		mutex_unlock(&sm->lock);
	}
}

static void sensor_manager_late_resume(struct early_suspend *h)
{
	struct sensor_dev *sensor;
	struct sensormanager* sm = container_of(h, struct sensormanager, early_suspend);

	if (!list_empty(&sm->sensor_list))
	{
		mutex_lock(&sm->lock);
		
		list_for_each_entry(sensor,&sm->sensor_list,list)
		{
			if (sensor->on_before_suspend)
			{
				printk("on late resume, [%s]->1\n", sensor_name(sensor->type));
				sensor->poweron(sensor);
				sensor->on = 1;
			}
		}
		
		mutex_unlock(&sm->lock);
	}
}
#endif


static void sensor_manager_refine(struct work_struct* work)
{
    struct sensormanager* sm = container_of(work,struct sensormanager,queuedwork);
    struct sensor_dev *sensor;
    int sensor_enable;
    mutex_lock(&sm->lock);
    if(sm->latch)
    {
        printk("sensor latch on,ignore...");
        mutex_unlock(&sm->lock);
        return;
    }
    if(sm->sensor_change_mask)
    {
        if(sm->sensor_change_mask&SENSOR_ACCELEROMETER)
        {
            sensor_enable = sm->sensor_enabled_mask&SENSOR_ACCELEROMETER;
            list_for_each_entry(sensor,&sm->sensor_list,list)
            {
                if(sensor->type&SENSOR_ACCELEROMETER)
                {
                    if(sensor_enable)
                    {
                        sensor->poweron(sensor);
                        sensor->on = 1;
                    }
                    else
                    {
                        sensor->poweroff(sensor);
                        sensor->on = 0;
                    }
                    printk("accleration enabled->%d\n",sensor->on);
                }
            }
        }

        if(sm->sensor_change_mask&SENSOR_MAGNETIC_FIELD)
        {
            sensor_enable = sm->sensor_enabled_mask&SENSOR_MAGNETIC_FIELD;
            list_for_each_entry(sensor,&sm->sensor_list,list)
            {
                if(sensor->type&SENSOR_MAGNETIC_FIELD)
                {
                    if(sensor_enable)
                    {
                        sensor->poweron(sensor);
                        sensor->on = 1;
                    }
                    else
                    {
                        sensor->poweroff(sensor);
                        sensor->on = 0;
                    }
                    printk("magneticfield enabled->%d\n",sensor->on);

                }
            }
        }

        if(sm->sensor_change_mask&SENSOR_ORIENTATION)
        {
            sensor_enable = sm->sensor_enabled_mask&SENSOR_ORIENTATION;
            list_for_each_entry(sensor,&sm->sensor_list,list)
            {
                if(sensor->type&SENSOR_ORIENTATION)
                {
                    if(sensor_enable)
                    {
                        sensor->poweron(sensor);
                        sensor->on = 1;
                    }
                    else
                    {
                        sensor->poweroff(sensor);
                        sensor->on = 0;
                    }
                    printk("orientation enabled->%d\n",sensor->on);
                }
            }
        }

        if(sm->sensor_change_mask&SENSOR_TEMPERATURE)
        {
            sensor_enable = sm->sensor_enabled_mask&SENSOR_TEMPERATURE;
            list_for_each_entry(sensor,&sm->sensor_list,list)
            {
                if(sensor->type&SENSOR_TEMPERATURE)
                {
                    if(sensor_enable)
                    {
                        sensor->poweron(sensor);
                        sensor->on = 1;
                    }
                    else
                    {
                        sensor->poweroff(sensor);
                        sensor->on = 0;
                    }
                    printk("magneticfield enabled->%d\n",sensor->on);
                }
            }
        }
        if(sm->sensor_change_mask&SENSOR_LIGHT)
        {
            sensor_enable = sm->sensor_enabled_mask&SENSOR_LIGHT;
            list_for_each_entry(sensor,&sm->sensor_list,list)
            {
                if(sensor->type&SENSOR_LIGHT)
                {
                    if(sensor_enable)
                    {
                        sensor->poweron(sensor);
                        sensor->on = 1;
                    }
                    else
                    {
                        sensor->poweroff(sensor);
                        sensor->on = 0;
                    }
                    printk("light enabled->%d\n",sensor->on);
                }
            }
        }

        if(sm->sensor_change_mask&SENSOR_GYROSCOPE)
        {
            sensor_enable = sm->sensor_enabled_mask&SENSOR_GYROSCOPE;
            list_for_each_entry(sensor,&sm->sensor_list,list)
            {
                if(sensor->type&SENSOR_GYROSCOPE)
                {
                    if(sensor_enable)
                    {
                        sensor->poweron(sensor);
                        sensor->on = 1;
                    }
                    else
                    {
                        sensor->poweroff(sensor);
                        sensor->on = 0;
                    }
                    printk("gyroscope enabled->%d\n",sensor->on);
                }
            }
        }

        sm->sensor_change_mask = 0;
    }
    mutex_unlock(&sm->lock);

}

#if 0 //def	CONFIG_PM
static int sensor_manager_suspend(struct platform_device *pdev, pm_message_t state)
{
    struct sensormanager *sm;
    struct sensor_dev *sensor;
    struct sensor_dev *tmp;

    sm = platform_get_drvdata(pdev);
    if(sm)
    {
        if(!sm->refcount)
            return 0;
        //try to lock sensor manager
        if(!mutex_trylock(&sm->lock))
            return -EBUSY;

        //power off each managed sensor device
        list_for_each_entry_safe(sensor, tmp, &sm->sensor_list, list)
        {
            if(sensor->poweroff)
                sensor->poweroff();
            sensor->on = 0;
        }
    }
}

static int sensor_manager_resume(struct platform_device *pdev)
{
    struct sensormanager *sm;
    struct sensor_dev *sensor;
    struct sensor_dev *tmp;

    sm = platform_get_drvdata(pdev);
    if(sm)
    {
        if(!sm->refcount)
            return 0;

        list_for_each_entry_safe(sensor, tmp, &sm->sensor_list, list)
        {
            if(sensor->enabled)
            {
                if(sensor->poweron)
                    sensor->poweron();
                sensor->on = 1;
            }
        }
    }

    mutex_unlock(&sm->lock);
    return 0;
}
#else
#define	sensor_manager_suspend		NULL
#define	sensor_manager_resume		NULL
#endif


/* Sysfs stuff */
static ssize_t sensors_show(struct device *dev,
                            struct device_attribute *attr, char *buf)
{
    //this is also a platform device,but we should use linux low level device
    //directly
    struct sensormanager *sm = dev_get_drvdata(dev);
    int len = 0;
    struct sensor_dev *sensor;
    struct sensor_dev *tmp;

    if(sm)
    {
        mutex_lock(&sm->lock);
        list_for_each_entry_safe(sensor, tmp, &sm->sensor_list, list)
        len += sprintf(buf+len, "%s\t\n", sensor->name);
        mutex_unlock(&sm->lock);
    }

    return len;

}

static ssize_t acceleration_show(struct device *dev,
                                 struct device_attribute *attr, char *buf)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int len = 0;
    if(sm)
    {
        len = sprintf(buf, "%d\n", (sm->sensor_enabled_mask&SENSOR_ACCELEROMETER)?1:0);
    }
    return len;
}
static ssize_t acceleration_store(struct device *dev,
                                  struct device_attribute *attr, const char *buf, size_t count)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int sensor_enabled;
    sscanf(buf,"%d",&sensor_enabled);
    if(sm)
    {
        if(sensor_enabled!=(sm->sensor_enabled_mask&SENSOR_ACCELEROMETER))
        {
            if(sensor_enabled>0)
                sm->sensor_enabled_mask|=SENSOR_ACCELEROMETER;
            else
                sm->sensor_enabled_mask&=~SENSOR_ACCELEROMETER;
            sm->sensor_change_mask|=SENSOR_ACCELEROMETER;
            //queue_work(sm->wq,&sm->queuedwork);            
			sensor_manager_refine(&sm->queuedwork);
        }
    }
    return count;
}
static ssize_t magneticfield_show(struct device *dev,
                                  struct device_attribute *attr, char *buf)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int len = 0;
    if(sm)
    {
        len = sprintf(buf, "%d\n", (sm->sensor_enabled_mask&SENSOR_MAGNETIC_FIELD)?1:0);
    }
    return len;
}
static ssize_t magneticfield_store(struct device *dev,
                                   struct device_attribute *attr, const char *buf, size_t count)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int sensor_enabled;
    sscanf(buf,"%d",&sensor_enabled);
    if(sm)
    {
        if(sensor_enabled!=(sm->sensor_enabled_mask&SENSOR_MAGNETIC_FIELD))
        {
            if(sensor_enabled>0)
                sm->sensor_enabled_mask|=SENSOR_MAGNETIC_FIELD;
            else
                sm->sensor_enabled_mask&=~SENSOR_MAGNETIC_FIELD;
            sm->sensor_change_mask|=SENSOR_MAGNETIC_FIELD;

			sensor_manager_refine(&sm->queuedwork);
            //queue_work(sm->wq,&sm->queuedwork);
        }
    }
    return count;
}

static ssize_t orientation_show(struct device *dev,
                                struct device_attribute *attr, char *buf)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int len = 0;
    if(sm)
    {
        len = sprintf(buf, "%d\n", (sm->sensor_enabled_mask&SENSOR_ORIENTATION)?1:0);
    }
    return len;
}
static ssize_t orientation_store(struct device *dev,
                                 struct device_attribute *attr, const char *buf, size_t count)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int sensor_enabled;
    sscanf(buf,"%d",&sensor_enabled);
    if(sm)
    {
        if(sensor_enabled!=(sm->sensor_enabled_mask&SENSOR_ORIENTATION))
        {
            if(sensor_enabled>0)
                sm->sensor_enabled_mask|=SENSOR_ORIENTATION;
            else
                sm->sensor_enabled_mask&=~SENSOR_ORIENTATION;
            sm->sensor_change_mask|=SENSOR_ORIENTATION;
			
			sensor_manager_refine(&sm->queuedwork);
            //queue_work(sm->wq,&sm->queuedwork);
        }
    }
    return count;
}

static ssize_t temperature_show(struct device *dev,
                                struct device_attribute *attr, char *buf)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int len = 0;
    if(sm)
    {
        len = sprintf(buf, "%d\n", (sm->sensor_enabled_mask&SENSOR_TEMPERATURE)?1:0);
    }
    return len;
}
static ssize_t temperature_store(struct device *dev,
                                 struct device_attribute *attr, const char *buf, size_t count)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int sensor_enabled;
    sscanf(buf,"%d",&sensor_enabled);
    if(sm)
    {
        if(sensor_enabled!=(sm->sensor_enabled_mask&SENSOR_TEMPERATURE))
        {
            if(sensor_enabled>0)
                sm->sensor_enabled_mask|=SENSOR_TEMPERATURE;
            else
                sm->sensor_enabled_mask&=~SENSOR_TEMPERATURE;
            sm->sensor_change_mask|=SENSOR_TEMPERATURE;
			sensor_manager_refine(&sm->queuedwork);
            //queue_work(sm->wq,&sm->queuedwork);
        }
    }
    return count;
}

static ssize_t delayms_show(struct device *dev,
                            struct device_attribute *attr, char *buf)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    struct sensor_dev  *sensor,*tmp;
    int len = 0;
    if(sm)
    {
        mutex_lock(&sm->lock);
        list_for_each_entry_safe(sensor, tmp, &sm->sensor_list, list)
        {
            len += sprintf(buf+len, "%s:%dms\n", sensor->name,sensor->delay);
        }
        mutex_unlock(&sm->lock);
    }
    return len;
}
static ssize_t delayms_store(struct device *dev,
                             struct device_attribute *attr, const char *buf, size_t count)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    struct sensor_dev  *sensor,*tmp;
    int handle;
    int64_t delayms;
    sscanf(buf,"%d %lld",&handle,&delayms);
    //do_div(delayms,1000000);   /* change ns to ms */
    //printk("sensors report delay: %d -> %lldms\n", handle, delayms);
    if(sm)
    {
        mutex_lock(&sm->lock);
        list_for_each_entry_safe(sensor, tmp, &sm->sensor_list, list)
        {
            if(sensor->type==(1<<handle))
            {
                sensor->delay = delayms;
                if(sensor->setdelay)
                {
                    printk("delay: %s - %lldms\n", sensor->name, delayms);
                    sensor->setdelay(sensor,delayms);
                }
            }
        }
        mutex_unlock(&sm->lock);

    }
    return count;
}

static ssize_t latch_show(struct device *dev,
                          struct device_attribute *attr, char *buf)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int len = 0;
    if(sm)
    {
        len = sprintf(buf, "%d\n", sm->latch);
    }
    return len;
}
static ssize_t latch_store(struct device *dev,
                           struct device_attribute *attr, const char *buf, size_t count)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    struct sensor_dev  *sensor,*tmp;
    int latch;
    sscanf(buf,"%d",&latch);
    printk("set skm latch ->%d\n",latch);
    if(sm)
    {
        sm->latch= latch;
        //latch on ,then power off all managed sensors
        if(latch)
        {
            mutex_lock(&sm->lock);
            list_for_each_entry_safe(sensor, tmp, &sm->sensor_list, list)
            {
                if(sensor->poweroff)
                    sensor->poweroff(sensor);
            }
            mutex_unlock(&sm->lock);
        }
        else if(sm->sensor_change_mask)
        {
            queue_work(sm->wq,&sm->queuedwork);
        }

    }
    return count;
}

static ssize_t light_show(struct device *dev,
                          struct device_attribute *attr, char *buf)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int len = 0;
    if(sm)
    {
        len = sprintf(buf, "%d\n", (sm->sensor_enabled_mask&SENSOR_LIGHT)?1:0);
    }
    return len;
}
static ssize_t light_store(struct device *dev,
                           struct device_attribute *attr, const char *buf, size_t count)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int sensor_enabled;
    sscanf(buf,"%d",&sensor_enabled);
    if(sm)
    {
        if(sensor_enabled!=(sm->sensor_enabled_mask&SENSOR_LIGHT))
        {
            if(sensor_enabled>0)
                sm->sensor_enabled_mask|=SENSOR_LIGHT;
            else
                sm->sensor_enabled_mask&=~SENSOR_LIGHT;
            sm->sensor_change_mask|=SENSOR_LIGHT;

			sensor_manager_refine(&sm->queuedwork);
            //queue_work(sm->wq,&sm->queuedwork);
        }
    }
    return count;
}

static ssize_t gyroscope_show(struct device *dev,
                              struct device_attribute *attr, char *buf)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int len = 0;
    if(sm)
    {
        len = sprintf(buf, "%d\n", (sm->sensor_enabled_mask&SENSOR_GYROSCOPE)?1:0);
    }
    return len;
}
static ssize_t gyroscope_store(struct device *dev,
                               struct device_attribute *attr, const char *buf, size_t count)
{
    struct sensormanager *sm = dev_get_drvdata(dev);
    int sensor_enabled;
    sscanf(buf,"%d",&sensor_enabled);
    if(sm)
    {
        if(sensor_enabled!=(sm->sensor_enabled_mask&SENSOR_GYROSCOPE))
        {
            if(sensor_enabled>0)
                sm->sensor_enabled_mask|=SENSOR_GYROSCOPE;
            else
                sm->sensor_enabled_mask&=~SENSOR_GYROSCOPE;
            sm->sensor_change_mask|=SENSOR_GYROSCOPE;

            //queue_work(sm->wq,&sm->queuedwork);            
			sensor_manager_refine(&sm->queuedwork);
        }
    }
    return count;
}


static DEVICE_ATTR(sensorslist, S_IRUGO,sensors_show, NULL);
static DEVICE_ATTR(accelerameter, S_IRUGO|S_IWUGO,acceleration_show, acceleration_store);
static DEVICE_ATTR(magnetometer, S_IRUGO|S_IWUGO,magneticfield_show, magneticfield_store);
static DEVICE_ATTR(orientation, S_IRUGO|S_IWUGO,orientation_show, orientation_store);
static DEVICE_ATTR(temperature, S_IRUGO|S_IWUGO,temperature_show, temperature_store);
static DEVICE_ATTR(delayms, S_IRUGO|S_IWUGO,delayms_show, delayms_store);
static DEVICE_ATTR(latch, S_IRUGO|S_IWUGO,latch_show, latch_store);
static DEVICE_ATTR(lightsensor, S_IRUGO|S_IWUGO,light_show, light_store);
static DEVICE_ATTR(gyroscope, S_IRUGO|S_IWUGO,gyroscope_show, gyroscope_store);

static int __devinit sensor_manager_probe(struct platform_device *pdev)
{
    struct sensormanager *sm;
    int err;

    sm = kzalloc(sizeof(struct sensormanager), GFP_KERNEL);
    if(!sm)
        return -ENOMEM;

    //init sensor manger
    mutex_init(&sm->lock);
    sm->latch = 0;
    INIT_LIST_HEAD(&sm->sensor_list);

    platform_set_drvdata(pdev,sm);

    //create device attribute
    err = device_create_file(&pdev->dev,&dev_attr_sensorslist);
    err = device_create_file(&pdev->dev,&dev_attr_accelerameter);
    err = device_create_file(&pdev->dev,&dev_attr_magnetometer);
    err = device_create_file(&pdev->dev,&dev_attr_orientation);
    err = device_create_file(&pdev->dev,&dev_attr_temperature);
    err = device_create_file(&pdev->dev,&dev_attr_delayms);
    err = device_create_file(&pdev->dev,&dev_attr_latch);
    err = device_create_file(&pdev->dev,&dev_attr_lightsensor);
    err = device_create_file(&pdev->dev,&dev_attr_gyroscope);

    //init work queue
    INIT_WORK(&sm->queuedwork,sensor_manager_refine);
    sm->wq = create_singlethread_workqueue("skm");

    //store the sensor manager pointer as module variable.
    sm_p = sm;

#ifdef CONFIG_HAS_EARLYSUSPEND
	sm->early_suspend.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN+1;
	sm->early_suspend.suspend = sensor_manager_early_suspend;
	sm->early_suspend.resume = sensor_manager_late_resume;
	register_early_suspend(&sm->early_suspend);
#endif

    return 0;
}

static int sensor_manager_remove(struct platform_device *pdev)
{
    struct sensormanager* sm = platform_get_drvdata(pdev);
    if(sm)
    {
#ifdef CONFIG_HAS_EARLYSUSPEND
	unregister_early_suspend(&sm->early_suspend);
#endif
        if(!list_empty(&sm->sensor_list))
            return -EBUSY;
        device_remove_file(&pdev->dev,&dev_attr_sensorslist);
        device_remove_file(&pdev->dev,&dev_attr_accelerameter);
        device_remove_file(&pdev->dev,&dev_attr_magnetometer);
        device_remove_file(&pdev->dev,&dev_attr_orientation);
        device_remove_file(&pdev->dev,&dev_attr_temperature);
        device_remove_file(&pdev->dev,&dev_attr_delayms);
        device_remove_file(&pdev->dev,&dev_attr_lightsensor);
        device_remove_file(&pdev->dev,&dev_attr_gyroscope);

        //work queue destory
        flush_workqueue(sm->wq);
        destroy_workqueue(sm->wq);
        kfree(sm);
    }

    return 0;
}


static struct platform_driver sensor_manager_driver =
{
    .probe          = sensor_manager_probe,
    .remove         = __devexit_p(sensor_manager_remove),
    .suspend	= sensor_manager_suspend,
    .resume		= sensor_manager_resume,
    .driver		= {
        .name	= "skm",
        .owner	= THIS_MODULE,
    },
};

int __init sensor_manager_init(void)
{
    return platform_driver_register(&sensor_manager_driver);
}

void __exit sensor_manager_exit(void)
{
    platform_driver_unregister(&sensor_manager_driver);
}


MODULE_DESCRIPTION("Sensor Manager driver");
MODULE_AUTHOR("Raymond Wang<raymond1860@gmail.com");
MODULE_LICENSE("GPL");


module_init(sensor_manager_init);
module_exit(sensor_manager_exit);


