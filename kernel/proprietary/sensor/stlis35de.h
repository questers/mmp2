#ifndef _LIS35DE_H_
#define _LIS35DE_H_

#define LIS35DE_REG_AUTO_INCREMENT	0x80
// Register Value
#define LIS35DE_REG_CTRL1		0x20
#define LIS35DE_REG_CTRL2		0x21
#define LIS35DE_REG_CTRL3		0x22
#define LIS35DE_REG_HP_RESET		0x23
#define LIS35DE_REG_STATUS		0x27
#define LIS35DE_REG_OUT_X		0x29
#define LIS35DE_REG_OUT_Y		0x2b
#define LIS35DE_REG_OUT_Z		0x2d
#define LIS35DE_REG_FF_WU_CFG_1		0x30
#define LIS35DE_REG_FF_WU_SRC_1		0x31
#define LIS35DE_REG_FF_WU_THS_1		0x32
#define LIS35DE_REG_FF_WU_DURATION_1	0x33
#define LIS35DE_REG_FF_WU_CFG_2		0x34
#define LIS35DE_REG_FF_WU_SRC_2		0x35
#define LIS35DE_REG_FF_WU_THS_2		0x36
#define LIS35DE_REG_FF_WU_DURATION_2	0x37
#define LIS35DE_REG_CLICK_CFG		0x38
#define LIS35DE_REG_CLICK_SRC		0x39
#define LIS35DE_REG_CLICK_THSY_X	0x3B
#define LIS35DE_REG_CLICK_THSZ		0x3C
#define LIS35DE_REG_CLICK_TIMELIMIT	0x3D
#define LIS35DE_REG_CLICK_LATENCY	0x3E
#define LIS35DE_REG_CLICK_WINDOW	0x3F

enum LIS35DE_CTRL1 {
  CTRL1_Xen = 0x01,
  CTRL1_Yen = 0x02,
  CTRL1_Zen = 0x04,
  CTRL1_FS  = 0x20, /*Full scale selection*/
  CTRL1_PD  = 0x40,/*Power down control ,0:power down mode,1:active mode*/
  CTRL1_DR  = 0x80,/*Data Rate Selection,0:100Hz,1:400Hz*/
};

enum LIS35DE_CTRL2 {
  CTRL2_HP_COEFF1 = 0x01,
  CTRL2_HP_COEFF2 = 0x02,
  CTRL2_HP_FF_WU1 = 0x04,
  CTRL2_HP_FF_WU2 = 0x08,
  CTRL2_FDS	  = 0x10,/*filtered data selection,0:internal filter bypassed*/
  CTRL2_BOOT	  = 0x40,/*reboot memory content,0:normal mode,1:reboot memory content*/
  CTRL2_SIM	  = 0x80,/*SPI serial interface mode selection,0:4-wire,1:3-wire*/
};

enum LIS35DE_CTRL3 {
  CTRL3_INT1_CFG0 = 0x01,
  CTRL3_INT1_CFG1 = 0x02,
  CTRL3_INT1_CFG2 = 0x04,
  CTRL3_INT2_CFG0 = 0x08,
  CTRL3_INT2_CFG1 = 0x10,
  CTRL3_INT2_CFG2 = 0x20,
  CTRL3_PP_OD 	  = 0x40,/*push-pull/open drain selection on interrupt pad,0:push-pull,1:open drain*/
  CTRL3_IHL	  = 0x80,/*interrupt active high,0:active high,1:active low*/
};

enum LIS35DE_STATUS {
  STATUS_XDA	  = 0x01,/*X axis ,new data available*/
  STATUS_YDA	  = 0x02,
  STATUS_ZDA	  = 0x04,
  STATUS_XYZDA    = 0x08,/*X and Y and Z axis ,new data available*/
  STATUS_XOR	  = 0x10,/*X axis data overrun*/
  STATUS_YOR	  = 0x20,
  STATUS_ZOR	  = 0x40,
  STATUS_XYZOR	  = 0x80,/*X and Y and Z axis ,data overrun*/
};

enum LIS35DE_FF_WU_CFG1 {
  FF_WU_CFG1_XLIE = 0x01,
  FF_WU_CFG1_XHIE = 0x02,
  FF_WU_CFG1_YLIE = 0x04,
  FF_WU_CFG1_YHIE = 0x08,
  FF_WU_CFG1_ZLIE = 0x10,
  FF_WU_CFG1_ZHIE = 0x20,
  FF_WU_CFG1_LIR  = 0x40,
  FF_WU_CFG1_AOI  = 0x80,
};

enum LIS35DE_FF_WU_SRC1 {
  FF_WU_SRC1_XL   = 0x01,/*X low ,0: no interrupt ,1:XL event generated*/
  FF_WU_SRC1_XH	  = 0x02,
  FF_WU_SRC1_YL   = 0x04,
  FF_WU_SRC1_YH   = 0x08,
  FF_WU_SRC1_ZL   = 0x10,
  FF_WU_SRC1_ZH   = 0x20,
  FF_WU_SRC1_IA   = 0x40,/*Interrupt Active,0:no interrupt generated,1:one or more interrupt generated*/
};

enum LIS35DE_FF_WU_CFG2 {
  FF_WU_CFG2_XLIE = 0x01,
  FF_WU_CFG2_XHIE = 0x02,
  FF_WU_CFG2_YLIE = 0x04,
  FF_WU_CFG2_YHIE = 0x08,
  FF_WU_CFG2_ZLIE = 0x10,
  FF_WU_CFG2_ZHIE = 0x20,
  FF_WU_CFG2_LIR  = 0x40,
  FF_WU_CFG2_AOI  = 0x80,
};

enum LIS35DE_FF_WU_SRC2 {
  FF_WU_SRC2_XL   = 0x01,/*X low ,0: no interrupt ,1:XL event generated*/
  FF_WU_SRC2_XH	  = 0x02,
  FF_WU_SRC2_YL   = 0x04,
  FF_WU_SRC2_YH   = 0x08,
  FF_WU_SRC2_ZL   = 0x10,
  FF_WU_SRC2_ZH   = 0x20,
  FF_WU_SRC2_IA   = 0x40,/*Interrupt Active,0:no interrupt generated,1:one or more interrupt generated*/
};

enum LIS35DE_CLICK_CFG {
  CLICK_CFG_SINGLE_X = 0x01,
  CLICK_CFG_DOUBLE_X = 0x02,
  CLICK_CFG_SINGLE_Y = 0x04,
  CLICK_CFG_DOUBLE_Y = 0x08,
  CLICK_CFG_SINGLE_Z = 0x10,
  CLICK_CFG_DOUBLE_Z = 0x20,
  CLICK_CFG_LIR      = 0x40,
};

enum LIS35DE_CLICK_SRC {
  CLICK_SRC_SINGLE_X = 0x01,
  CLICK_SRC_DOUBLE_X = 0x02,
  CLICK_SRC_SINGLE_Y = 0x04,
  CLICK_SRC_DOUBLE_Y = 0x08,
  CLICK_SRC_SINGLE_Z = 0x10,
  CLICK_SRC_DOUBLE_Z = 0x20,
  CLICK_SRC_IA       = 0x40,
};

#define CLICK_THS_X	(1<<0)
#define CLICK_THS_Y	(1<<4)
#define CLICK_THS_Z	(1<<0)



// Acceleration data threshold
#define ACCELERATION_THRESHOLD_ROTATE_P		0x20	// +576mg
#define ACCELERATION_THRESHOLD_ROTATE_M		0xe0	// -576mg
#define ACCELERATION_THRESHOLD_NAVIGATE_P	0x0f	// +270mg
#define ACCELERATION_THRESHOLD_NAVIGATE_M	0xf1	// -270mg
#define ACCELERATION_THRESHOLD_DYNAMIC_P	0x3d	// +1.1g
#define ACCELERATION_THRESHOLD_DYNAMIC_M	0xc3	// -1.1g
#define ACCELERATION_THRESHOLD_MAX_P		0x3d	// +2g/+8g
#define ACCELERATION_THRESHOLD_MAX_M		0xc3	// -2g/-8g
#define ACCELERATION_THRESHOLD_1G_P		0x38	// 
#define ACCELERATION_THRESHOLD_1G_M		0xc8	// 

#define ACCELERATION_DURATION_STATIC		0x64	// 250ms
#define ACCELERATION_DURATION_DYNAMIC		0x02	// 5ms

#endif //_LIS35DE_H_

