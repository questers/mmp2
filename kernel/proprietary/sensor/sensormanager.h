#ifndef _SENSOR_MANAGER_H
#define _SENSOR_MANAGER_H


#define DEFAULT_SENSOR_POLLING_MS 10


struct sensor_dev
{
    //must be the first member since we use list to manager sensor devices
    struct list_head	list;
    u32 	type;
    char	*name;
    int		delay;
    int		count;//sensor event count
    int		on;
	int     on_before_suspend;
    struct input_dev* dev;//this member is the unified input device belongs all sensors,and it will filled by sensormanager
    void (*poweron)(struct sensor_dev* sensor);
    void (*poweroff)(struct sensor_dev* sensor);
    void (*setdelay)(struct sensor_dev* sensor,int ms);
};

/*extern struct sensor_dev* sensor_add(u32 type, char *name,
                                     void (*poweron)(struct sensor_dev* sensor),
                                     void (*poweroff)(struct sensor_dev* sensor));*/
extern int sensor_add(struct sensor_dev* sensor);

extern void sensor_remove(char *name);

extern int sensor_queue_work(struct work_struct *work);

extern int sensor_queue_delayed_work(struct delayed_work *work,unsigned long delay);

/**
 * Sensor types
 */

enum
{
    ID_ACCELEROMETER,
    ID_MAGNETIC_FIELD,
    ID_ORIENTATION,
    ID_GYROSCOPE,
    ID_LIGHT,
    ID_PRESSURE,
    ID_TEMPERATURE,
    ID_PROXIMITY,
    ID_GRAVITY,
    ID_LINEAR_ACCELERATION,
    ID_ROTATION_VECTOR,
    ID_MAX
};

#define SENSOR_ACCELEROMETER       (1<<ID_ACCELEROMETER)
#define SENSOR_MAGNETIC_FIELD      (1<<ID_MAGNETIC_FIELD)
#define SENSOR_ORIENTATION         (1<<ID_ORIENTATION)
#define SENSOR_GYROSCOPE           (1<<ID_GYROSCOPE)
#define SENSOR_LIGHT               (1<<ID_LIGHT)
#define SENSOR_PRESSURE            (1<<ID_PRESSURE)
#define SENSOR_TEMPERATURE         (1<<ID_TEMPERATURE)
#define SENSOR_PROXIMITY           (1<<ID_PROXIMITY)
#define SENSOR_GRAVITY             (1<<ID_GRAVITY)
#define SENSOR_LINEAR_ACCELERATION (1<<ID_LINEAR_ACCELERATION)
#define SENSOR_ROTATION_VECTOR     (1<<ID_ROTATION_VECTOR)

#define SENSOR_NAME_LIGHTSENSOR "lightsensor"
#define SENSOR_NAME_ACCELEROMETER "accelerameter"
#define SENSOR_NAME_MAGNETOMETER "magnetometer"
#define SENSOR_NAME_GYROSCOPE "gyroscope"

#endif /*_SENSOR_MANAGER_H*/


