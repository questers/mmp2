/*
 * lsm303dlh_a.c
 * ST 3-Axis Accelerometer Driver
 *
 * Copyright (C) 2011 Questers Tech,Ltd.
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
//#define DEBUG
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <linux/semaphore.h>
#include <linux/input.h>
#include <linux/fs.h>
#include <linux/i2c.h>
#include <linux/completion.h>
#include <linux/interrupt.h>
#include <linux/freezer.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <plat/mfp.h>
#include <mach/gpio.h>
#include <linux/lsm303dlh.h>
#include "sensormanager.h"

/* lsm303dlh accelerometer registers */
#define WHO_AM_I    0x0F

/* ctrl 1: pm2 pm1 pm0 dr1 dr0 zenable yenable zenable */
#define CTRL_REG1       0x20    /* power control reg */
#define CTRL_REG2       0x21    /* power control reg */
#define CTRL_REG3       0x22    /* power control reg */
#define CTRL_REG4       0x23    /* interrupt control reg */
#define CTRL_REG5       0x24    /* interrupt control reg */

#define STATUS_REG      0x27    /* status register */

#define AXISDATA_REG    0x28    /* axis data */

#define INT1_CFG  0x30    /* interrupt 1 configuration */
#define INT1_SRC  0x31    /* interrupt 1 source reg    */
#define INT1_THS  0x32    /* interrupt 1 threshold */
#define INT1_DURATION  0x33    /* interrupt 1 threshold */

#define INT2_CFG  0x34    /* interrupt 2 configuration */
#define INT2_SRC  0x35    /* interrupt 2 source reg    */
#define INT2_THS  0x36    /* interrupt 2 threshold */
#define INT2_DURATION  0x37    /* interrupt 2 threshold */


/* Sensitivity adjustment */
#define FDS_2G	0
#define FDS_4G	1
#define FDS_8G	3
#define SENSITIVITY_2G 1 /*mg/LSB*/
#define SENSITIVITY_4G 2 /*mg/LSB*/
#define SENSITIVITY_8G 4 /*mg/LSB*/


#define STATUS_DATA_AVAILABLE 0x08


/* Multiple byte transfer enable */
#define MULTIPLE_I2C_TR 0x80
/* Range -2048 to 2047 */
struct lsm303dlh_a_t
{
    short	x;
    short	y;
    short	z;
};
/*
 * accelerometer local data
 */
struct lsm303dlh_a_data
{
    struct sensor_dev sensor;

	
    struct i2c_client* client;
    struct lsm303dlh_a_t data;
	struct lsm303dlh_a_t last;

    struct delayed_work delayed_work;

	int fds;//0: 2g mode,1:4g mode,3:8gmode
	int sensitivity;

    struct lsm303dlh_platform_data pdata;


    //add more client parameters below
};

/*LOCAL DATA*/
static int odr=100;
module_param(odr, int, S_IRUGO);
MODULE_PARM_DESC(odr, "output data rate [50|100|400|1000]Hz");

static int lsm303dlh_a_read(struct i2c_client *client,u8 reg,u8* pval)
{
    int ret;
    ret = i2c_smbus_read_byte_data(client,reg);
    if(ret>=0)
    {
        *pval = ret;
        return 0;
    }
    return ret;
}
static int lsm303dlh_a_write(struct i2c_client *client,u8 reg,u8 val)
{
    int ret;
    ret = i2c_smbus_write_byte_data(client,reg,val);
    if(0==ret)
        return 0;
    return ret;
}

/******************************************
 *
 * device dependent operation functions
 *
 *
 *******************************************/
static int lsm303dlh_a_readdata(struct lsm303dlh_a_data *ddata)
{
    unsigned char acc_data[6];
    short data[3];

    int ret = i2c_smbus_read_i2c_block_data(ddata->client,
                                            AXISDATA_REG | MULTIPLE_I2C_TR, 6, acc_data);
    if (ret < 0)
    {
        dev_err(&ddata->client->dev,
                "i2c_smbus_read_byte_data failed error %d\
				Register AXISDATA_REG \n", ret);
        return ret;
    }

	//12bit representation
    data[0] = ((short) ((acc_data[1] << 8) | acc_data[0]))>>4;
    data[1] = ((short) ((acc_data[3] << 8) | acc_data[2]))>>4;
    data[2] = ((short) ((acc_data[5] << 8) | acc_data[4]))>>4;

    data[0] *= ddata->sensitivity;
    data[1] *= ddata->sensitivity;
    data[2] *= ddata->sensitivity;

    /* taking position and orientation of x,y,z axis into account*/

    data[ddata->pdata.axis_map_x] = ddata->pdata.negative_x ?
                                    -data[ddata->pdata.axis_map_x] : data[ddata->pdata.axis_map_x];
    data[ddata->pdata.axis_map_y] = ddata->pdata.negative_y ?
                                    -data[ddata->pdata.axis_map_y] : data[ddata->pdata.axis_map_y];
    data[ddata->pdata.axis_map_z] = ddata->pdata.negative_z ?
                                    -data[ddata->pdata.axis_map_z] : data[ddata->pdata.axis_map_z];

    ddata->data.x = data[ddata->pdata.axis_map_x];
    ddata->data.y = data[ddata->pdata.axis_map_y];
    ddata->data.z = data[ddata->pdata.axis_map_z];
	
    return 0;

}

static void lsm303dlh_a_report(struct input_dev *idev,struct lsm303dlh_a_data* ddata)
{
    u8 val=0;
    if(!lsm303dlh_a_read(ddata->client,STATUS_REG, &val)&&
            ((val & STATUS_DATA_AVAILABLE) == STATUS_DATA_AVAILABLE)&&
            (!lsm303dlh_a_readdata(ddata)))
    {
    		//prevent linux input framework fuzz blocked
    		if(ddata->data.x==ddata->last.x)
				ddata->data.x++;
    		if(ddata->data.y==ddata->last.y)
				ddata->data.y++;			
    		if(ddata->data.z==ddata->last.z)
				ddata->data.z++;
    	

            input_report_abs(idev, ABS_X, ddata->data.x);
            input_report_abs(idev, ABS_Y, ddata->data.y);
            input_report_abs(idev, ABS_Z, ddata->data.z);
            input_sync(idev);

			ddata->last = ddata->data;
    }
}

static void lsm303dlh_a_delayed_work(struct work_struct* work)
{
    struct lsm303dlh_a_data* ddata = (struct lsm303dlh_a_data*)container_of(work,struct lsm303dlh_a_data,delayed_work.work);

    lsm303dlh_a_report(ddata->sensor.dev,ddata);
    sensor_queue_delayed_work(&ddata->delayed_work,msecs_to_jiffies(ddata->sensor.delay));
}


static void lsm303dlh_a_poweroff(struct sensor_dev* device)
{
	struct lsm303dlh_a_data* ddata=(struct lsm303dlh_a_data*)container_of(device,struct lsm303dlh_a_data,sensor);

    cancel_delayed_work_sync(&ddata->delayed_work);
    //disable x/y/z axis and power down
    lsm303dlh_a_write(ddata->client,CTRL_REG1,0x00);

}

static void lsm303dlh_a_poweron(struct sensor_dev* device)
{
	struct lsm303dlh_a_data* ddata=(struct lsm303dlh_a_data*)container_of(device,struct lsm303dlh_a_data,sensor);
    u8 value;

	//Control reg1
	value=0;
	value = 0x7; //xyz enable
	value |=1<<5;//normal mode
	switch(odr)
	{
		case 100:value |= 1<<3;break;
		case 400:value |= 2<<3;break;
		case 1000:value |= 3<<3;break;		
		case 50:
		default:break;
	}
    lsm303dlh_a_write(ddata->client,CTRL_REG1,value);



    // control reg2
    value=0;
    lsm303dlh_a_write(ddata->client,CTRL_REG2,value);

	//control reg3
	value=0;
    lsm303dlh_a_write(ddata->client,CTRL_REG3,value);

	//control reg4
	value=0;
	value|=0x80;//BDU enable
	switch(ddata->fds)
	{
		case FDS_4G: //4g mode	
		{	
			ddata->sensitivity= SENSITIVITY_4G;
			value|=FDS_4G<<4;
		}
		break;
		case FDS_8G://8g mode
		{	
			ddata->sensitivity= SENSITIVITY_8G;
			value|=FDS_8G<<4;
		}
		break;		
		default://2g mode
		{	
			value|=FDS_2G<<4;
			ddata->sensitivity= SENSITIVITY_2G;
		}
		break;		
		
	}
    lsm303dlh_a_write(ddata->client,CTRL_REG4,value);

	//control reg5
    value=0x00;
    lsm303dlh_a_write(ddata->client,CTRL_REG5,value);
	
	
    sensor_queue_delayed_work(&ddata->delayed_work,msecs_to_jiffies(ddata->sensor.delay));

	

}


static void lsm303dlh_a_setdelay(struct sensor_dev* device,int ms)
{
	//not implemented
}

static int __devinit lsm303dlh_a_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    struct lsm303dlh_a_data* state;
    struct device *dev = &client->dev;
    struct input_dev *idev;
    int err;

    if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
    {
        dev_err(dev,"need I2C_FUNC_I2C\n");
        return -ENODEV;
    }	

    // probe the device    
    if(i2c_smbus_read_byte_data(client,WHO_AM_I)<0)
    {
        dev_err(dev, "failed to access %s in addr %02x\n",client->name,client->addr);
        return -ENODEV;
    }

    

    state = kzalloc(sizeof(struct lsm303dlh_a_data), GFP_KERNEL);
    if (state == NULL)
    {
        dev_err(dev, "failed to create client data\n");
        return -ENOMEM;
    }
    /* copy platform specific data */
    memcpy(&state->pdata, client->dev.platform_data, sizeof(state->pdata));

    state->client = client;

    printk(KERN_INFO "%s:attach to i2c bus, addr = %#x\n",id->name,client->addr);

	state->sensitivity = SENSITIVITY_2G;//2g mode
	state->fds = FDS_2G;
	
    state->sensor.setdelay = lsm303dlh_a_setdelay;
	state->sensor.poweron = lsm303dlh_a_poweron;
	state->sensor.poweroff = lsm303dlh_a_poweroff;
	state->sensor.type = SENSOR_ACCELEROMETER;
	state->sensor.name = SENSOR_NAME_ACCELEROMETER;
	state->sensor.delay = DEFAULT_SENSOR_POLLING_MS;
	
    sensor_add(&state->sensor);
	
    idev = input_allocate_device();
    if (!idev)
    {
        err = -ENOMEM;
        goto fail1;
    }

//store input device to sensor manager
    state->sensor.dev = idev;

    idev->id.bustype = BUS_I2C;
    idev->id.vendor = 0x0001;
    idev->id.product = 0x0001;
    idev->id.version = 0x0100;

    idev->name		 = SENSOR_NAME_ACCELEROMETER;
    idev->phys		 =	"sensor-input/accelerometer";

    __set_bit(EV_ABS, idev->evbit);
    __set_bit(ABS_X, idev->absbit);
    __set_bit(ABS_Y, idev->absbit);
    __set_bit(ABS_Z, idev->absbit);
    __set_bit(EV_SYN, idev->evbit);

    input_set_abs_params(idev,ABS_X,-8000,8000,0,0);
    input_set_abs_params(idev,ABS_Y,-8000,8000,0,0);
    input_set_abs_params(idev,ABS_Z,-8000,8000,0,0);
    input_set_drvdata(idev, state);

    if(input_register_device(idev))
    {
        printk(KERN_ERR "register accelerometer input device error\n");
        err=-1;
        goto fail2;
    }
    i2c_set_clientdata(client, state);


    INIT_DELAYED_WORK(&state->delayed_work,lsm303dlh_a_delayed_work);


    /* rest of the initialisation goes here. */
    dev_info(dev, "lsm303dlh_a client created\n");

    return 0;

fail2:
    input_free_device(idev);
fail1:
    sensor_remove(SENSOR_NAME_ACCELEROMETER);
    kfree(state);
    return err;
}

static int lsm303dlh_a_remove(struct i2c_client *client)
{
    struct lsm303dlh_a_data *state = i2c_get_clientdata(client);

    cancel_delayed_work_sync(&state->delayed_work);

    lsm303dlh_a_poweroff(&state->sensor);
    input_unregister_device(state->sensor.dev);
    input_free_device(state->sensor.dev);
    sensor_remove(SENSOR_NAME_ACCELEROMETER);
    kfree(state);
    return 0;
}

static const struct i2c_device_id lsm303dlh_a_idtable[] =
{
    { "lsm303dlh_a", 0 },
    { },
};

static struct i2c_driver lsm303dlh_a_driver =
{
    .driver = {
        .name = "lsm303dlh_a",
    },
    .id_table	= lsm303dlh_a_idtable,
    .probe	= lsm303dlh_a_probe,
    .remove	= __devexit_p(lsm303dlh_a_remove),
};
static int __init lsm303dlh_a_module_init(void)
{
    if(odr>400) odr=400;
    else odr=100;
    return i2c_add_driver(&lsm303dlh_a_driver);
}

static void __exit lsm303dlh_a_module_exit(void)
{
    i2c_del_driver(&lsm303dlh_a_driver);
}

MODULE_AUTHOR("Raymond Wang <raymond1860@gmail.com>");
MODULE_DESCRIPTION("lsm303dlh 3-axis accelerometer driver");
MODULE_LICENSE("GPL");

module_init(lsm303dlh_a_module_init);
module_exit(lsm303dlh_a_module_exit);

