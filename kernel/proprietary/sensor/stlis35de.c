/*
 *  stlis35de.c - ST LIS35DE accelerometer driver
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
//#define DEBUG
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/semaphore.h>
#include <linux/input.h>
#include <linux/fs.h>
#include <linux/i2c.h>
#include <linux/completion.h>
#include <linux/interrupt.h>
#include <linux/freezer.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <plat/mfp.h>
#include <mach/gpio.h>
#include "sensormanager.h"
#include "stlis35de.h"

#undef dprint
#define dprint(level, x...)			\
	do {						\
		if (debug_level >= (level))	\
			printk(x);			\
	} while (0)

//#define ENABLE_GSENSOR_INT

struct axis_gravity
{
    int x;
    int y;
    int z;
};
struct lis35de_state
{
    struct sensor_dev sensor;

	
    struct i2c_client* client;

    struct delayed_work delayed_work;


#ifdef ENABLE_GSENSOR_INT
    //ira handle
    int irq;
#endif

    struct axis_gravity last_g;

    int poweron_skip;

    bool is_lis35de;
    //add more client parameters below
};

struct axis_conversion
{
    s8 x;
    s8 y;
    s8 z;
};

/*LOCAL DATA*/
static struct lis35de_state* this_state;

static int debug_level=0;

module_param(debug_level, int, S_IRUGO);
MODULE_PARM_DESC(debug_level, "gravity sensor debug level");

static int odr=100;

module_param(odr, int, S_IRUGO);
MODULE_PARM_DESC(odr, "output data rate 100Hz or 400Hz");

static int lis35de_read(u8 reg,u8* pval)
{
    int ret;
    if(NULL == this_state->client) return -1;
    ret = i2c_smbus_read_byte_data(this_state->client,reg);
    if(ret>=0)
    {
        *pval = ret;
        return 0;
    }
    return -EIO;
}
static int lis35de_write(u8 reg,u8 val)
{
    int ret;
    if(NULL == this_state->client) return -1;
    ret = i2c_smbus_write_byte_data(this_state->client,reg,val);
    if(0==ret)
        return 0;
    return -EIO;
}

/******************************************
 *
 * device dependent operation functions
 *
 *
 *******************************************/
#define FULL_SCALE_SELECTION 18 /*mg/digit*/

//ACCELERATION_THRESHOLD_NAVIGATE_P*FULL_SCALE_SELECTION = 15*18 = 270
#define GRAVITY_SENSIBILITY_THRESHOLD  270
#define LIS35DE_ACCURACY 1024
static int lis35de_get_xyz(int *x,int *y,int *z)
{
    s8 val;

    if(lis35de_read(LIS35DE_REG_OUT_X,(u8*)&val)<0) return -1;
    *x = val*FULL_SCALE_SELECTION;
    if(lis35de_read(LIS35DE_REG_OUT_Y,(u8*)&val)<0) return -1;
    *y = val*FULL_SCALE_SELECTION;
    if(lis35de_read(LIS35DE_REG_OUT_Z,(u8*)&val)<0) return -1;
    *z = val*FULL_SCALE_SELECTION;
    return 0;

}

static void lis35de_report(struct input_dev *idev,struct lis35de_state* this)
{
    struct axis_gravity g,g_report;
    u8 val=0;

#ifdef ENABLE_GSENSOR_INT
    //read interrupt source
    lis35de_read(LIS35DE_REG_FF_WU_SRC_1, &val);
    if(0==(val&FF_WU_SRC1_IA))
    {
        return;
    }
#endif

    if(!lis35de_read(LIS35DE_REG_STATUS, &val)&&
            ((val & STATUS_XYZDA) == STATUS_XYZDA)&&
            (!lis35de_get_xyz(&g.x,&g.y,&g.z)))
    {
        if(this_state->is_lis35de==0)
		{
	    	/* Fit list33de on board
	    	         *x,y swap and y is negative
	    		*/
	    	g_report.x = g.y;
			g_report.y = g.x;
			g_report.z = g.z;

			g_report.y = -g_report.y;
        }
		else
		{
	    	g_report.x = g.x;
			g_report.y = g.y;
			g_report.z = g.z;
			
		}
        //printk("report gsensor eventx[%d]y[%d]z[%d] \n",g_report.x,g_report.y,g_report.z);

        input_report_abs(idev, ABS_X, g_report.x);
        input_report_abs(idev, ABS_Y, g_report.y);
        input_report_abs(idev, ABS_Z, g_report.z);
        input_sync(idev);
    }
}
static void lis35de_delayed_work(struct work_struct* work)
{
    struct lis35de_state* state = (struct lis35de_state*)container_of(work,struct lis35de_state,delayed_work.work);

    if(state->poweron_skip) state->poweron_skip--;

    if(!state->poweron_skip)
        lis35de_report(state->sensor.dev,state);

#ifndef ENABLE_GSENSOR_INT
    //if not interrupt driven,we need queue self work
    sensor_queue_delayed_work(&state->delayed_work,msecs_to_jiffies(state->sensor.delay));
#endif

}


static void lis35de_poweroff(struct sensor_dev* device)
{
	struct lis35de_state* thiz=(struct lis35de_state*)container_of(device,struct lis35de_state,sensor);

    cancel_delayed_work_sync(&thiz->delayed_work);
    //disable x/y/z axis and power down
    lis35de_write(LIS35DE_REG_CTRL1,0x00);


}

static void lis35de_poweron(struct sensor_dev* device)
{
	struct lis35de_state* thiz=(struct lis35de_state*)container_of(device,struct lis35de_state,sensor);
    u8 value;

    // lis35de_read(LIS35DE_REG_CTRL1,&value);
    //xyz enable,power up,400HZ,
    //full scale :0
    // value &=~(CTRL1_FS);
    value = CTRL1_PD|CTRL1_Xen|CTRL1_Yen|CTRL1_Zen;//0x47
    if(odr==400)
        value |= CTRL1_DR;
    lis35de_write(LIS35DE_REG_CTRL1,value);



    // lis35de_read(LIS35DE_REG_CTRL2,&value);
    //reboot memory content,HPF enable for FF-WU1,fcutoff=8Hz
    /*
    * HP_coeff2,1 	ft(HZ) DR=100Hz	ft(Hz)DR=400Hz
    * 00			2				8
    *	01			1				4
    * 10			0.5				2
    * 11			0.25				1
    */
    value =thiz->is_lis35de?(CTRL2_SIM|CTRL2_HP_FF_WU1|CTRL2_HP_COEFF2):CTRL2_SIM;
    lis35de_write(LIS35DE_REG_CTRL2,value);

    /*
    //change to normal mode again
    value&=~(CTRL2_BOOT);
    lis35de_write(LIS35DE_REG_CTRL2,value);
    */

    //interrupt active high,push-pull,disable interrupt pin,for now we use polling method
    //FF_WU_1->INT1,int2 use FF_WU_2->INT2
    /*
    CFG2		CFG1		CFG0 		INT PAD
    0			0			0			GND
    0			0			1			FF_WU_1
    0			1			0			FF_WU_2
    0			1			1			FF_WU_1 or FF_WU_2
    1			0			0			Data ready
    1			1			1			Click Interrupt
    */
#ifdef ENABLE_GSENSOR_INT
    //enable FF_WU_1 interrupt on INT1
    lis35de_write(LIS35DE_REG_CTRL3,CTRL3_INT1_CFG0);
#else
    lis35de_write(LIS35DE_REG_CTRL3,0);//CTRL3_INT1_CFG0|CTRL3_INT2_CFG1);
    sensor_queue_delayed_work(&thiz->delayed_work,msecs_to_jiffies(thiz->sensor.delay));;
#endif

#ifdef ENABLE_GSENSOR_INT
    //free fall,wakeup
    lis35de_write(LIS35DE_REG_FF_WU_CFG_1,FF_WU_CFG1_LIR|
                  FF_WU_CFG1_XHIE|FF_WU_CFG1_XLIE|
                  FF_WU_CFG1_YHIE|FF_WU_CFG1_YLIE|
                  FF_WU_CFG1_ZHIE|FF_WU_CFG1_ZLIE);
    //threshold
    lis35de_write(LIS35DE_REG_FF_WU_THS_1,ACCELERATION_THRESHOLD_ROTATE_P);

    //duration,for ODR 100Hz, 100ms = 0x0A
    if(odr==400)
    {
        //each LSB is 1000/400 = 2.5ms
        value = (thiz->sensor.delay*10)/25;
    }
    else
    {
        //each LSB is 1000/100 = 10ms
        value = thiz->sensor.delay/10;

    }
    lis35de_write(LIS35DE_REG_FF_WU_DURATION_1,value);


#endif
    thiz->poweron_skip = 5;

}


static void lis35de_setdelay(struct sensor_dev* device,int ms)
{
#ifdef ENABLE_GSENSOR_INT
	struct lis35de_state* thiz=(struct lis35de_state*)container_of(device,struct lis35de_state,sensor);

    u8 value;
    //duration,for ODR 100Hz, 100ms = 0x0A
    if(odr==400)
    {
        //each LSB is 1000/400 = 2.5ms
        value = (thiz->sensor->delay*10)/25;
    }
    else
    {
        //each LSB is 1000/100 = 10ms
        value = thiz->sensor->delay/10;

    }
    lis35de_write(LIS35DE_REG_FF_WU_DURATION_1,value);

#else
#endif
}

#ifdef ENABLE_GSENSOR_INT
static irqreturn_t lis35de_interrupt(int irq, void *irq_data)
{
//  u8 val;
    struct lis35de_state* state = (struct lis35de_state*)irq_data;

    dprint(2,SENSOR_NAME_ACCELEROMETER" interrupt event\n");
    sensor_queue_delayed_work(&state->delayed_work,0);


    return IRQ_HANDLED;
}
#endif

static int __devinit lis35de_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    struct lis35de_state* state;
    struct device *dev = &client->dev;
    struct input_dev *idev;
    int err;

    if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
    {
        dev_err(dev,"need I2C_FUNC_I2C\n");
        return -ENODEV;
    }	

    // probe the device
    if(i2c_smbus_read_byte_data(client,LIS35DE_REG_STATUS)<0)
    {
        dev_err(dev, "failed to access %s in addr %02x\n",client->name,client->addr);
        return -ENODEV;
    }

    

    state = kzalloc(sizeof(struct lis35de_state), GFP_KERNEL);
    if (state == NULL)
    {
        dev_err(dev, "failed to create our state\n");
        return -ENOMEM;
    }

    state->client = client;

    printk(KERN_INFO "%s:attach to i2c bus, addr = %#x\n",id->name,client->addr);

#ifdef ENABLE_GSENSOR_INT
    //interrupt
    state->irq = client->irq;
    if (request_irq(state->irq, lis35de_interrupt,
                    IRQF_TRIGGER_RISING,
                    "gsensor interrupt",
                    state))
    {
        dev_err(dev,"Request IRQ for gsensor failed \n");
        kfree(state);
        return -EINTR;
    }
#endif
    state->sensor.setdelay = lis35de_setdelay;
	state->sensor.poweron = lis35de_poweron;
	state->sensor.poweroff = lis35de_poweroff;
	state->sensor.type = SENSOR_ACCELEROMETER;
	state->sensor.name = SENSOR_NAME_ACCELEROMETER;
	state->sensor.delay = DEFAULT_SENSOR_POLLING_MS;
	
    sensor_add(&state->sensor);
	
    idev = input_allocate_device();
    if (!idev)
    {
        err = -ENOMEM;
        goto fail1;
    }

//store input device to sensor manager
    state->sensor.dev = idev;

    idev->id.bustype = BUS_I2C;
    idev->id.vendor = 0x0001;
    idev->id.product = 0x0001;
    idev->id.version = 0x0100;

    idev->name		 = SENSOR_NAME_ACCELEROMETER;
    idev->phys		 =	"sensor-input/accelerometer";

    __set_bit(EV_ABS, idev->evbit);
    __set_bit(ABS_X, idev->absbit);
    __set_bit(ABS_Y, idev->absbit);
    __set_bit(ABS_Z, idev->absbit);
    __set_bit(EV_SYN, idev->evbit);

    input_set_abs_params(idev,ABS_X,0,0xffff,0,0);
    input_set_abs_params(idev,ABS_Y,0,0xffff,0,0);
    input_set_abs_params(idev,ABS_Z,0,0xffff,0,0);
    input_set_drvdata(idev, state);

    if(input_register_device(idev))
    {
        printk(KERN_ERR "register accelerometer input device error\n");
        err=-1;
        goto fail2;
    }
    state->poweron_skip = 5;
    state->is_lis35de = (strcmp(id->name,"lis35de")==0);
    i2c_set_clientdata(client, state);


    INIT_DELAYED_WORK(&state->delayed_work,lis35de_delayed_work);

    this_state = state;

    /* rest of the initialisation goes here. */
    dev_info(dev, "lis35de client created\n");

    return 0;

fail2:
    input_free_device(idev);
fail1:
    sensor_remove(SENSOR_NAME_ACCELEROMETER);
#ifdef ENABLE_GSENSOR_INT
    free_irq(state->irq,state);
#endif
    kfree(state);
    return err;
}

static int lis35de_remove(struct i2c_client *client)
{
    struct lis35de_state *state = i2c_get_clientdata(client);

    printk("rmmod lis35de\n");

    cancel_delayed_work_sync(&state->delayed_work);

    lis35de_poweroff(&state->sensor);
    input_unregister_device(state->sensor.dev);
    input_free_device(state->sensor.dev);
    sensor_remove(SENSOR_NAME_ACCELEROMETER);
#ifdef ENABLE_GSENSOR_INT
    free_irq(state->irq, state);
#endif
    kfree(state);
    this_state = NULL;
    return 0;
}

static const struct i2c_device_id lis35de_idtable[] =
{
    {"lis35de",0},
    {"lis33de",0},
    {}
};

static struct i2c_driver lis35de_driver =
{
    .driver = {
        .name = "lis35de",
    },
    .id_table	= lis35de_idtable,
    .probe	= lis35de_probe,
    .remove	= __devexit_p(lis35de_remove),
};
static int __init lis35de_module_init(void)
{
    if(odr>400) odr=400;
    else odr=100;
    return i2c_add_driver(&lis35de_driver);
}

static void __exit lis35de_module_exit(void)
{
    i2c_del_driver(&lis35de_driver);
}

MODULE_AUTHOR("Raymond Wang <raymond1860@gmail.com>");
MODULE_DESCRIPTION("lis35de driver");
MODULE_LICENSE("GPL");

module_init(lis35de_module_init);
module_exit(lis35de_module_exit);

