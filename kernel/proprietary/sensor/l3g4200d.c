/*
 *  l3g4200d.c - ST L3G4200D gyroscope driver
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/semaphore.h>
#include <linux/input.h>
#include <linux/fs.h>
#include <linux/i2c.h>
#include <linux/completion.h>
#include <linux/interrupt.h>
#include <linux/freezer.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <plat/mfp.h>
#include <mach/gpio.h>
#include "sensormanager.h"
#include "l3g4200d.h"

struct l3g4200d_state
{
    struct sensor_dev sensor;
	
    struct i2c_client* client;

    struct delayed_work delayed_work;

    u8 odr;

    //add more client parameters below
};

/*LOCAL DATA*/
static struct l3g4200d_state* this_state;

struct output_rate {
	int poll_rate_ms;
	u8 mask;
};

static const struct output_rate odr_table[] = {

	{	2,	ODR800|BW10},
	{	3,	ODR400|BW01},
	{	5,	ODR200|BW00},
	{	10,	ODR100|BW00},
};

static int l3g4200d_read(u8 reg,u8* pval)
{
    int ret;
    if(NULL == this_state->client) return -1;
    ret = i2c_smbus_read_byte_data(this_state->client,reg);
    if(ret>=0)
    {
        *pval = ret;
        return 0;
    }
    return -EIO;
}
static int l3g4200d_write(u8 reg,u8 val)
{
    int ret;
    if(NULL == this_state->client) return -1;
    ret = i2c_smbus_write_byte_data(this_state->client,reg,val);
    if(0==ret)
        return 0;
    return -EIO;
}

/*******************************************
 *
 * device dependent operation functions
 *
 *
 *******************************************/
 
static int l3g4200d_get_short(u8 reg_h,u8 reg_l,int16_t *val)
{
    u8 byte=0;
    u16 value;

    if(l3g4200d_read(reg_h, &byte))
    {
        printk("l3g4200d_get_short:l3g4200d_read error!\n");
        return -1;
    }
    value=byte;
    if(l3g4200d_read(reg_l, &byte))
    {
        printk("l3g4200d_get_short:l3g4200d_read error!\n");
        return -1;
    }
    value=(value<<8)+byte;
	*val=(int16_t)value;
	return 0;
}

static void l3g4200d_report(struct input_dev *idev,struct l3g4200d_state* this)
{
    int16_t x,y,z;
//	x=y=z=0;

    if(l3g4200d_get_short(L3G4200D_REG_OUT_XH, L3G4200D_REG_OUT_XL, &x))
    {
        printk("l3g4200d_report:l3g4200d_get_short error!\n");
        return;
    }
    if(l3g4200d_get_short(L3G4200D_REG_OUT_YH, L3G4200D_REG_OUT_YL, &y))
    {
        printk("l3g4200d_report:l3g4200d_get_short error!\n");
        return;
    }
    if(l3g4200d_get_short(L3G4200D_REG_OUT_ZH, L3G4200D_REG_OUT_ZL, &z))
    {
        printk("l3g4200d_report:l3g4200d_get_short error!\n");
        return;
    }

    input_report_abs(idev, ABS_X, -y);
    input_report_abs(idev, ABS_Y, x);
    input_report_abs(idev, ABS_Z, z);
    input_sync(idev);
}

static void l3g4200d_delayed_work(struct work_struct* work)
{
    struct l3g4200d_state* state = (struct l3g4200d_state*)container_of(work,struct l3g4200d_state,delayed_work.work);
    l3g4200d_report(state->sensor.dev,state);
    sensor_queue_delayed_work(&state->delayed_work,msecs_to_jiffies(state->sensor.delay));
}


static void l3g4200d_poweroff(struct sensor_dev* device)
{
	struct l3g4200d_state* thiz=(struct l3g4200d_state*)container_of(device,struct l3g4200d_state,sensor);

    cancel_delayed_work_sync(&thiz->delayed_work);
    l3g4200d_write(L3G4200D_REG_CTRL1,L3G4200D_VALUE_SLEEP_MODE);
}

static void l3g4200d_poweron(struct sensor_dev* device)
{
	struct l3g4200d_state* thiz=(struct l3g4200d_state*)container_of(device,struct l3g4200d_state,sensor);

    l3g4200d_write(L3G4200D_REG_CTRL1,thiz->odr|L3G4200D_VALUE_NORMAL_MODE);
    sensor_queue_delayed_work(&thiz->delayed_work,msecs_to_jiffies(thiz->sensor.delay));
}


static void l3g4200d_setdelay(struct sensor_dev* device,int ms)
{
	int i;
	struct l3g4200d_state* thiz=(struct l3g4200d_state*)container_of(device,struct l3g4200d_state,sensor);

	for (i = ARRAY_SIZE(odr_table) - 1; i >= 0; i--) {
#if 0
		/* fix odr to 100hz. -guang */
		if ((odr_table[i].mask & 0xc0) == ODR100)
			break;
#endif
		
		if (odr_table[i].poll_rate_ms <= ms)
			break;
	}
	if (i < 0) i = 0;
	
	thiz->odr = odr_table[i].mask;

	if(thiz->sensor.on)
		l3g4200d_write(L3G4200D_REG_CTRL1,thiz->odr|L3G4200D_VALUE_NORMAL_MODE);
}

u8 last_reg = 0;
static ssize_t sensor_get_reg(struct device *dev,
		struct device_attribute *attr, char *buf)
{
	u8 val = 0;

	if (last_reg)
	{
		l3g4200d_read(last_reg, &val);
		return sprintf(buf, "%x:%x\n", last_reg, val);
	}
	else
	{
		printk("Please first set the reg to read.\n");;
	}

	return 0;
}

static ssize_t sensor_set_reg(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	u32 reg ,val;
	int ret;

	ret = sscanf(buf, "%x:%x", &reg, &val);
	if (ret == 1)
	{
		last_reg = reg;
	}
	else if (ret == 2)
	{
		l3g4200d_write(reg, val);
		last_reg = reg;
	}

	return count;
}


static struct device_attribute static_attrs[] = {
	__ATTR(reg, 0666, sensor_get_reg, sensor_set_reg),
};

static int __devinit l3g4200d_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int i;
	int ret;
    struct l3g4200d_state* state;
    struct device *dev = &client->dev;
    struct input_dev *idev;
    int err;
    u8 byte=0;

    if((byte=i2c_smbus_read_byte_data(client,L3G4200D_REG_WHO_AM_I))<0)
    {
        printk("l3g4200d_probe:failed to read WHOAMI register!\n");
        return -1;
    }

	if(byte!=0xD3)
	{
		printk("l3g4200d_probe:the content of WHOAMI register is wrong!\n");
		return -1;
	}

    state = kzalloc(sizeof(struct l3g4200d_state), GFP_KERNEL);
    if (state == NULL)
    {
        dev_err(dev, "failed to create our state\n");
        return -ENOMEM;
    }

    state->client = client;

    printk(KERN_INFO "%s:attach to i2c bus, addr = %#x\n",id->name,client->addr);

	state->sensor.poweron = l3g4200d_poweron;
	state->sensor.poweroff = l3g4200d_poweroff;
    state->sensor.setdelay = l3g4200d_setdelay;
	state->sensor.type = SENSOR_GYROSCOPE;
	state->sensor.name = SENSOR_NAME_GYROSCOPE;
	state->sensor.delay = 50;

    sensor_add(&state->sensor);
    idev = input_allocate_device();
    if (!idev)
    {
        err = -ENOMEM;
        goto fail1;
    }

//store input device to sensor manager
    state->sensor.dev = idev;

    idev->id.bustype = BUS_I2C;
    idev->id.vendor = 0x0001;
    idev->id.product = 0x0001;
    idev->id.version = 0x0100;

    idev->name		 = SENSOR_NAME_GYROSCOPE;
    idev->phys		 =	"sensor-input/gyroscope";

    __set_bit(EV_ABS, idev->evbit);
    __set_bit(ABS_X, idev->absbit);
    __set_bit(ABS_Y, idev->absbit);
    __set_bit(ABS_Z, idev->absbit);
    __set_bit(EV_SYN, idev->evbit);

    input_set_abs_params(idev,ABS_X,-0x8000,0x7fff,0,0);
    input_set_abs_params(idev,ABS_Y,-0x8000,0x7fff,0,0);
    input_set_abs_params(idev,ABS_Z,-0x8000,0x7fff,0,0);

    input_set_drvdata(idev, state);

    if(input_register_device(idev))
    {
        printk(KERN_ERR "register gyroscope input device error\n");
        err=-1;
        goto fail2;
    }

    i2c_set_clientdata(client, state);

    this_state = state;
	state->odr = L3G4200D_VALUE_ODR_100;
    l3g4200d_write(L3G4200D_REG_CTRL1,0);
    l3g4200d_write(L3G4200D_REG_CTRL4,L3G4200D_FS_2000DPS);
    INIT_DELAYED_WORK(&state->delayed_work,l3g4200d_delayed_work);

    for (i=0; i<ARRAY_SIZE(static_attrs); i++)
    {
        ret = device_create_file(&client->dev, &static_attrs[i]);
        if(ret)
            goto fail2;
    }

    /* rest of the initialisation goes here. */
    dev_info(dev, "l3g4200d client created\n");
    return 0;

fail2:
    input_free_device(idev);
fail1:
    sensor_remove(SENSOR_NAME_GYROSCOPE);
    kfree(state);
    return err;
}

static int l3g4200d_remove(struct i2c_client *client)
{
    int i;
    struct l3g4200d_state *state = i2c_get_clientdata(client);

    printk("rmmod l3g4200d\n");

    for (i=0; i<ARRAY_SIZE(static_attrs); i++)
        device_remove_file(&client->dev, &static_attrs[i]);

    cancel_delayed_work_sync(&state->delayed_work);

    l3g4200d_poweroff(&state->sensor);
    input_unregister_device(state->sensor.dev);
    input_free_device(state->sensor.dev);

    sensor_remove(SENSOR_NAME_GYROSCOPE);
    kfree(state);
    this_state = NULL;
    return 0;
}

static const struct i2c_device_id l3g4200d_idtable[] =
{
    {"L3G4200D",0},
    {}
};

static struct i2c_driver l3g4200d_driver =
{
    .driver = {
        .name = "L3G4200D",
    },
    .id_table	= l3g4200d_idtable,
    .probe	= l3g4200d_probe,
    .remove	= __devexit_p(l3g4200d_remove),
};
static int __init l3g4200d_module_init(void)
{
    return i2c_add_driver(&l3g4200d_driver);
}

static void __exit l3g4200d_module_exit(void)
{
    i2c_del_driver(&l3g4200d_driver);
}

MODULE_AUTHOR("Ellie Cao <elliejcao@gmail.com>");
MODULE_DESCRIPTION("l3g4200d driver");
MODULE_LICENSE("GPL");

module_init(l3g4200d_module_init);
module_exit(l3g4200d_module_exit);

