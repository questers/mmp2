#ifndef _L3G4200D_H_
#define _L3G4200D_H_

#define L3G4200D_REG_WHO_AM_I	0x0F

#define L3G4200D_REG_CTRL1	    0x20
#define L3G4200D_REG_CTRL2	    0x21
#define L3G4200D_REG_CTRL3	    0x22
#define L3G4200D_REG_CTRL4	    0x23
#define L3G4200D_REG_CTRL5	    0x24

#define L3G4200D_REG_REFERENCE  0x25

#define L3G4200D_REG_STATUS	    0x27

#define L3G4200D_REG_OUT_XL	    0x28
#define L3G4200D_REG_OUT_XH	    0x29
#define L3G4200D_REG_OUT_YL	    0x2A
#define L3G4200D_REG_OUT_YH	    0x2B
#define L3G4200D_REG_OUT_ZL	    0x2C
#define L3G4200D_REG_OUT_ZH	    0x2D

#define L3G4200D_REG_INT_CFG    0x30
#define L3G4200D_REG_INT_SRC    0x31

#define L3G4200D_REG_INT_TSH_XH	    0x32
#define L3G4200D_REG_INT_TSH_XL	    0x33
#define L3G4200D_REG_INT_TSH_YH	    0x34
#define L3G4200D_REG_INT_TSH_YL	    0x35
#define L3G4200D_REG_INT_TSH_ZH	    0x36
#define L3G4200D_REG_INT_TSH_ZL	    0x37

#define L3G4200D_REG_INT_DURATION   0x38

#define L3G4200D_VALUE_WHO_AM_I     0xD3

/* TODO: how to set BW[1:0]? */
#define L3G4200D_VALUE_ODR_100      0x00
#define L3G4200D_VALUE_ODR_200      0x40
#define L3G4200D_VALUE_ODR_400      0x80
#define L3G4200D_VALUE_ODR_800      0xC0

#define L3G4200D_VALUE_NORMAL_MODE     0x0F
#define L3G4200D_VALUE_SLEEP_MODE      0x08

#define L3G4200D_INT_ENABLE         0x80

#define L3G4200D_FS_250DPS         0x00
#define L3G4200D_FS_500DPS         0x10
#define L3G4200D_FS_1000DPS        0x20
#define L3G4200D_FS_2000DPS        0x30

/* CTRL_REG1 */
#define PM_OFF		0x00
#define PM_NORMAL	0x08
#define ENABLE_ALL_AXES	0x07
#define BW00		0x00
#define BW01		0x10
#define BW10		0x20
#define BW11		0x30
#define ODR100		0x00  /* ODR = 100Hz */
#define ODR200		0x40  /* ODR = 200Hz */
#define ODR400		0x80  /* ODR = 400Hz */
#define ODR800		0xC0  /* ODR = 800Hz */

#endif //_L3G4200D_H_

