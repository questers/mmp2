/*
 *  stk2201.c - STK2201 light sensor driver
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/semaphore.h>
#include <linux/input.h>
#include <linux/fs.h>
#include <linux/i2c.h>
#include <linux/completion.h>
#include <linux/interrupt.h>
#include <linux/freezer.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <plat/mfp.h>
#include <mach/gpio.h>
#include "sensormanager.h"
#include "stk2201.h"

struct stk2201_state
{
    struct sensor_dev sensor;
	
    struct i2c_client* client;

    struct delayed_work delayed_work;

    u8 commandval;

    //add more client parameters below
};

/*LOCAL DATA*/
static struct stk2201_state* this_state;

/*
 * GAIN   50k		100k	200k
 * 0		2		1		0.5
 * 1		1		0.5		0.25
 * 2		0.5		0.25		0.125
 * 3		0.25		0.125	0.0625		>Current adopted
 *
 * IT		50k		100k	200k
 * 0		*		50ms	100ms
 * 1		*		100ms	200ms
 * 2		*		200ms	400ms		>Current adopted
 * 3		*		400ms	800ms
 *
 * Rset = 100k
 * MaxRange = 0~512Lux
*/
#define STK2201_GAIN 	3
#define STK2201_IT		2

//For different gain 0/1/2/3,scale is 1/2/4/8
#define STK2201_SCALE 1   /* check the values in sensors.c  1Lux/LSB*/

static int it_ms_100k[] = 
{
	50,100,200,400
};
static int stk2201_read(u8 reg,u8* pval)
{
    int ret;
    if(NULL == this_state->client) return -1;
    ret = i2c_smbus_read_byte_data(this_state->client,reg);
    if(ret>=0)
    {
        *pval = ret;
        return 0;
    }
    return -EIO;
}
static int stk2201_write(u8 reg,u8 val)
{
    int ret;
    if(NULL == this_state->client) return -1;
    ret = i2c_smbus_write_byte_data(this_state->client,reg,val);
    if(0==ret)
        return 0;
    return -EIO;
}

/******************************************
 *
 * device dependent operation functions
 *
 *
 *******************************************/
static void stk2201_report(struct input_dev *idev,struct stk2201_state* this)
{
    u8 high,low;
    int value;

	value=0;

    if(stk2201_read(STK2201_REG_DATAH, &high))
    {
        //printk("stk2201_report:stk2201_read error!\n");
        return;
    }
    if(stk2201_read(STK2201_REG_DATAL, &low))
    {
        //printk("stk2201_report:stk2201_read error!\n");
        return;
    }
    value=(high<<(8-STK2201_REG_DATA_SHIFT))+(low>>STK2201_REG_DATA_SHIFT);

	//value/=STK2201_SCALE;
    //printk("report light sensor event[%d]\n",value);

    input_report_abs(idev, ABS_VOLUME, value);
    input_sync(idev);
}

static void stk2201_delayed_work(struct work_struct* work)
{
    struct stk2201_state* state = (struct stk2201_state*)container_of(work,struct stk2201_state,delayed_work.work);
    stk2201_report(state->sensor.dev,state);
    sensor_queue_delayed_work(&state->delayed_work,msecs_to_jiffies(state->sensor.delay));
}


static void stk2201_poweroff(struct sensor_dev* device)
{
	struct stk2201_state* thiz=(struct stk2201_state*)container_of(device,struct stk2201_state,sensor);

    cancel_delayed_work_sync(&thiz->delayed_work);
    stk2201_write(STK2201_REG_COMMAND,thiz->commandval|STK2201_FUNCTION_SD);

}

static void stk2201_poweron(struct sensor_dev* device)
{
	struct stk2201_state* thiz=(struct stk2201_state*)container_of(device,struct stk2201_state,sensor);

    stk2201_write(STK2201_REG_COMMAND,thiz->commandval);
    sensor_queue_delayed_work(&thiz->delayed_work,msecs_to_jiffies(thiz->sensor.delay));
}


static void stk2201_setdelay(struct sensor_dev* device,int ms)
{
	struct stk2201_state* thiz=(struct stk2201_state*)container_of(device,struct stk2201_state,sensor);
    thiz->commandval=(STK2201_GAIN<<STK2201_FUNCTION_GAIN_SHIFT)|(STK2201_IT<<STK2201_FUNCTION_IT_SHIFT);
    if(thiz->sensor.on)
        stk2201_write(STK2201_REG_COMMAND,thiz->commandval);
    else
        stk2201_write(STK2201_REG_COMMAND,thiz->commandval|STK2201_FUNCTION_SD);
}

static int __devinit stk2201_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    struct stk2201_state* state;
    struct device *dev = &client->dev;
    struct input_dev *idev;
    int err;

    // probe the device
    if(i2c_smbus_read_byte_data(client,STK2201_REG_DATAH)<0)
    {
        dev_err(dev, "device %s not found\n",client->name);
        return -ENODEV;
    }

    state = kzalloc(sizeof(struct stk2201_state), GFP_KERNEL);
    if (state == NULL)
    {
        dev_err(dev, "failed to create our state\n");
        return -ENOMEM;
    }

    state->client = client;

    printk(KERN_INFO "%s:attach to i2c bus, addr = %#x\n",id->name,client->addr);

	state->sensor.poweron = stk2201_poweron;
	state->sensor.poweroff = stk2201_poweroff;
    state->sensor.setdelay = stk2201_setdelay;
	state->sensor.type = SENSOR_LIGHT;
	state->sensor.name = SENSOR_NAME_LIGHTSENSOR;
	state->sensor.delay = it_ms_100k[STK2201_IT];

    sensor_add(&state->sensor);
    idev = input_allocate_device();
    if (!idev)
    {
        err = -ENOMEM;
        goto fail1;
    }

//store input device to sensor manager
    state->sensor.dev = idev;

    idev->id.bustype = BUS_I2C;
    idev->id.vendor = 0x0001;
    idev->id.product = 0x0001;
    idev->id.version = 0x0100;

    idev->name		 = SENSOR_NAME_LIGHTSENSOR;
    idev->phys		 =	"sensor-input/light";

    __set_bit(EV_ABS, idev->evbit);
    __set_bit(ABS_VOLUME, idev->absbit);
    __set_bit(EV_SYN, idev->evbit);

    input_set_abs_params(idev,ABS_VOLUME,0,0xffff,0,0);

    input_set_drvdata(idev, state);

    if(input_register_device(idev))
    {
        printk(KERN_ERR "register light sensor input device error\n");
        err=-1;
        goto fail2;
    }

    state->commandval=(STK2201_GAIN<<STK2201_FUNCTION_GAIN_SHIFT)|(STK2201_IT<<STK2201_FUNCTION_IT_SHIFT);
    i2c_set_clientdata(client, state);

    this_state = state;
    stk2201_write(STK2201_REG_COMMAND,state->commandval|STK2201_FUNCTION_SD);
    INIT_DELAYED_WORK(&state->delayed_work,stk2201_delayed_work);

    /* rest of the initialisation goes here. */
    dev_info(dev, "stk2201 client created\n");
    return 0;

fail2:
    input_free_device(idev);
fail1:
    sensor_remove(SENSOR_NAME_LIGHTSENSOR);
    kfree(state);
    return err;
}

static int stk2201_remove(struct i2c_client *client)
{
    struct stk2201_state *state = i2c_get_clientdata(client);

    printk("rmmod stk2201\n");

    cancel_delayed_work_sync(&state->delayed_work);

    stk2201_poweroff(&state->sensor);
    input_unregister_device(state->sensor.dev);
    input_free_device(state->sensor.dev);

    sensor_remove(SENSOR_NAME_LIGHTSENSOR);
    kfree(state);
    this_state = NULL;
    return 0;
}

static const struct i2c_device_id stk2201_idtable[] =
{
    {"stk2201",0},
    {}
};

static struct i2c_driver stk2201_driver =
{
    .driver = {
        .name = "stk2201",
    },
    .id_table	= stk2201_idtable,
    .probe	= stk2201_probe,
    .remove	= __devexit_p(stk2201_remove),
};
static int __init stk2201_module_init(void)
{
    return i2c_add_driver(&stk2201_driver);
}

static void __exit stk2201_module_exit(void)
{
    i2c_del_driver(&stk2201_driver);
}

MODULE_AUTHOR("Ellie Cao <elliejcao@gmail.com>");
MODULE_DESCRIPTION("stk2201 driver");
MODULE_LICENSE("GPL");

module_init(stk2201_module_init);
module_exit(stk2201_module_exit);

