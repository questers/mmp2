/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The shared header file of WTM function layer definition.
 *
 */

#ifndef _MV_WTM_FUNC_H_
#define _MV_WTM_FUNC_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_types.h"
#include "mv_wtm_err.h"
#include "mv_wtm_func_cmn.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

/* hash */
extern int32_t mv_wtm_func_hash_init_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t key_phys, uint32_t key_len);
#ifdef CONFIG_EN_FIPS
extern int32_t mv_wtm_func_hash_init_fips(
        mv_wtm_ss ss, uint32_t sch,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof_phys, uint32_t usrk_wof_phys);
#endif
extern int32_t mv_wtm_func_hash_update(
        mv_wtm_ss ss, uint32_t src_phys, uint32_t src_dma_phys, uint32_t len);
extern int32_t mv_wtm_func_hash_final(
        mv_wtm_ss ss, uint32_t src_phys, uint32_t src_dma_phys, uint32_t len,
        uint32_t dst_phys);
extern int32_t mv_wtm_func_hash_kill_seq(mv_wtm_ss ss);
extern int32_t mv_wtm_func_hash_dgst_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t key_phys, uint32_t key_len,
        uint32_t src_phys, uint32_t src_dma_phys, uint32_t len,
        uint32_t dst_phys);
#ifdef CONFIG_EN_FIPS
extern int32_t mv_wtm_func_hash_dgst_fips(
        mv_wtm_ss ss, uint32_t sch,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof_phys, uint32_t usrk_wof_phys,
        uint32_t src_phys, uint32_t src_dma_phys, uint32_t len,
        uint32_t dst_phys);
#endif

/* sym */
extern int32_t mv_wtm_func_sym_init_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t key0, uint32_t key1, uint32_t key2,
        uint32_t key_len, uint32_t iv);
#ifdef CONFIG_EN_FIPS
extern int32_t mv_wtm_func_sym_init_fips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof, uint32_t usrk_wof, uint32_t iv);
#endif
extern int32_t mv_wtm_func_sym_proc(
        mv_wtm_ss ss, uint32_t src_phys, uint32_t src_dma_phys,
        uint32_t dst_phys, uint32_t dst_dma_phys, uint32_t len);
extern int32_t mv_wtm_func_sym_final(
        mv_wtm_ss ss, uint32_t src_phys, uint32_t src_dma_phys,
        uint32_t dst_phys, uint32_t dst_dma_phys, uint32_t len);
extern int32_t mv_wtm_func_sym_kill_seq(mv_wtm_ss ss);
extern int32_t mv_wtm_func_sym_op_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t key0_phys, uint32_t key1_phys, uint32_t key2_phys,
        uint32_t key_len, uint32_t iv_phys,
        uint32_t src_phys, uint32_t src_dma_phys,
        uint32_t dst_phys, uint32_t dst_dma_phys, uint32_t len);
extern int32_t mv_wtm_func_sym_crypto_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t key0, uint32_t key1, uint32_t key2, uint32_t iv,
        uint32_t src_phys, uint32_t src_dma_phys,
        uint32_t dst_phys, uint32_t dst_dma_phys, uint32_t len);
#ifdef CONFIG_EN_FIPS
extern int32_t mv_wtm_func_sym_crypto_fips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof, uint32_t usrk_wof, uint32_t iv,
        uint32_t src_phys, uint32_t dst_phys,
        uint32_t src_dma_phys, uint32_t dst_dma_phys, uint32_t len);
#endif

/* pkcs */
extern int32_t mv_wtm_func_pkcs_init_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t key_mod_phys, uint32_t key_exp_phys);
#ifdef CONFIG_EN_FIPS
extern int32_t mv_wtm_func_pkcs_init_fips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof_phys, uint32_t usrk_wof_phys);
#endif
extern int32_t mv_wtm_func_pkcs_update(
        mv_wtm_ss ss, uint32_t src_phys, uint32_t src_dma_phys, uint32_t len);
extern int32_t mv_wtm_func_pkcs_final(
        mv_wtm_ss ss, uint32_t src_phys, uint32_t src_dma_phys, uint32_t len,
        uint32_t sig_phys, bool *verf);
extern int32_t mv_wtm_func_pkcs_kill_seq(mv_wtm_ss ss);
extern int32_t mv_wtm_func_pkcs_op_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t key_mod_phys, uint32_t key_exp_phys,
        uint32_t src_phys, uint32_t src_dma_phys, uint32_t len,
        uint32_t sig_phys, bool *verf);
#ifdef CONFIG_EN_FIPS
extern int32_t mv_wtm_func_pkcs_op_fips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof_phys, uint32_t usrk_wof_phys,
        uint32_t src_phys, uint32_t src_dma_phys, uint32_t len,
        uint32_t sig_phys, bool *verf);
#endif

/* ec-dsa */
extern int32_t mv_wtm_func_ec_dsa_init_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t key_phys, uint32_t domain_phys, uint32_t mode);
#ifdef CONFIG_EN_FIPS
extern int32_t mv_wtm_func_ec_dsa_init_fips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof_phys, uint32_t usrk_wof_phys,
        uint32_t domain_phys, uint32_t mode);
#endif
extern int32_t mv_wtm_func_ec_dsa_update(
        mv_wtm_ss ss, uint32_t src_phys, uint32_t src_dma_phys, uint32_t len);
extern int32_t mv_wtm_func_ec_dsa_final(
        mv_wtm_ss ss, uint32_t src_phys, uint32_t src_dma_phys, uint32_t len,
        uint32_t sig_r_phys, uint32_t sig_s_phys, bool *verf);
extern int32_t mv_wtm_func_ec_dsa_kill_seq(mv_wtm_ss ss);
extern int32_t mv_wtm_func_ec_dsa_op_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t key_phys, uint32_t domain_phys, uint32_t mode,
        uint32_t src_phys, uint32_t src_dma_phys, uint32_t len,
        uint32_t sig_r_phys, uint32_t sig_s_phys, bool *verf);
#ifdef CONFIG_EN_FIPS
extern int32_t mv_wtm_func_ec_dsa_op_fips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof_phys, uint32_t usrk_wof_phys,
        uint32_t domain_phys, uint32_t mode,
        uint32_t src_phys, uint32_t src_dma_phys, uint32_t len,
        uint32_t sig_r_phys, uint32_t sig_s_phys, bool *verf);
#endif

/* km */
extern int32_t mv_wtm_func_create_ec_key_pair_nonfips(mv_wtm_ss ss,
        uint32_t ksch, uint32_t domain_phys, uint32_t mode,
        uint32_t prv_key_phys, uint32_t pub_key_phys);
extern int32_t mv_wtm_func_derivate_ec_pub_key_nonfips(mv_wtm_ss ss,
        uint32_t ksch, uint32_t domain_phys, uint32_t mode,
        uint32_t prv_key_phys, uint32_t pub_key_phys);
extern int32_t mv_wtm_func_create_ec_dh_shared_key_nonfips(mv_wtm_ss ss,
        uint32_t ksch,
        uint32_t domain_phys, uint32_t mode,
        uint32_t prv_key_phys, uint32_t peer_pub_key_phys,
        uint32_t shrd_key_phys);
extern int32_t mv_wtm_func_gen_dh_sys_param(mv_wtm_ss ss,
        uint32_t ksch, uint32_t p_phys, uint32_t g_phys);
extern int32_t mv_wtm_func_create_dh_key_pair_nonfips(mv_wtm_ss ss,
        uint32_t ksch, uint32_t p_phys, uint32_t g_phys,
        uint32_t prv_key_phys, uint32_t pub_key_phys);
extern int32_t mv_wtm_func_derivate_dh_pub_key_nonfips(mv_wtm_ss ss,
        uint32_t ksch, uint32_t p_phys, uint32_t g_phys,
        uint32_t prv_key_phys, uint32_t pub_key_phys);
extern int32_t mv_wtm_func_create_dh_shared_key_nonfips(mv_wtm_ss ss,
        uint32_t ksch,
        uint32_t p_phys, uint32_t g_phys,
        uint32_t peer_pub_key_phys, uint32_t prv_key_phys,
        uint32_t shrd_key_phys);
extern int32_t mv_wtm_func_create_rsa_key_pair_nonfips(mv_wtm_ss ss,
        uint32_t ksch,
        uint32_t prv_key_exp_phys, uint32_t pub_key_exp_phys,
        uint32_t key_mod_phys,
        uint32_t p_phys, uint32_t q_phys,
        uint32_t trials, uint32_t limit);

#ifdef CONFIG_EN_FIPS
typedef struct _mv_wtm_key_info
{
    uint32_t kr_kad_virt;
    uint32_t uk_kad_virt;
    uint32_t subk_wof_phys;
    uint32_t subk_wof_sz;
    uint32_t usrk_wof_phys;
    uint32_t usrk_wof_sz;
} mv_wtm_key_info;

extern int32_t mv_wtm_func_create_subkey(mv_wtm_ss ss,
        mv_wtm_key_info *endk_info_virt,
        uint32_t key_id,
        uint32_t kr_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz);
extern int32_t mv_wtm_func_enroll_sym_usrkey(mv_wtm_ss ss,
        uint32_t ksch, uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_phys, uint32_t usrk_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz);
extern int32_t mv_wtm_func_enroll_ec_usrkey(mv_wtm_ss ss,
        uint32_t ksch, uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_phys, uint32_t usrk_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz);
extern int32_t mv_wtm_func_enroll_rsa_usrkey(mv_wtm_ss ss,
        uint32_t ksch, uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_phys, uint32_t usrk_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz);
extern int32_t mv_wtm_func_create_sym_usrkey(mv_wtm_ss ss,
        mv_wtm_key_info *endk_info_virt,
        uint32_t ksch, uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz);
extern int32_t mv_wtm_func_create_ec_usrkey(mv_wtm_ss ss,
        mv_wtm_key_info *endk_info_virt,
        uint32_t ksch,
        uint32_t domain_phys, uint32_t mode,
        uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t pub_key_phys);
extern int32_t mv_wtm_func_derivate_ec_pub_key(mv_wtm_ss ss,
        uint32_t ksch,
        uint32_t domain_phys, uint32_t mode,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t pub_key_phys);
extern int32_t mv_wtm_func_create_dh_usrkey(mv_wtm_ss ss,
        mv_wtm_key_info *endk_info_virt,
        uint32_t ksch,
        uint32_t p_phys, uint32_t g_phys,
        uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t pub_key_phys);
extern int32_t mv_wtm_func_derivate_dh_pub_key(mv_wtm_ss ss,
        uint32_t ksch,
        uint32_t p_phys, uint32_t g_phys,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t pub_key_phys);
extern int32_t mv_wtm_func_create_rsa_usrkey(mv_wtm_ss ss,
        mv_wtm_key_info *endk_info_virt,
        uint32_t ksch,
        uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t pub_key_exp_phys, uint32_t key_mod_phys,
        uint32_t trials, uint32_t limit);
extern int32_t mv_wtm_func_create_ec_dh_shared_usrkey(mv_wtm_ss ss,
        uint32_t ec_dh_sch, mv_wtm_key_info *ec_dh_key_info_virt,
        uint32_t peer_pub_key_phys,
        uint32_t ksch, uint32_t key_id,
        uint32_t kr_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz);
extern int32_t mv_wtm_func_create_dh_shared_usrkey(mv_wtm_ss ss,
        uint32_t dh_sch, mv_wtm_key_info *dh_key_info_virt,
        uint32_t ksch, uint32_t key_id,
        uint32_t p_phys, uint32_t g_phys,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t peer_pub_key_phys);
extern int32_t mv_wtm_func_pr_ec256_dec(mv_wtm_ss ss,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t ec_point0_phys, uint32_t ec_point1_phys,
        uint32_t ksch_lsb, uint32_t usrk_id_lsb,
        uint32_t kr_kad_virt_lsb,
        uint32_t subk_wof_phys_lsb, uint32_t subk_wof_sz_lsb,
        uint32_t usrk_wof_phys_lsb, uint32_t usrk_wof_sz_lsb,
        uint32_t ksch_msb, uint32_t usrk_id_msb,
        uint32_t kr_kad_virt_msb,
        uint32_t subk_wof_phys_msb, uint32_t subk_wof_sz_msb,
        uint32_t usrk_wof_phys_msb, uint32_t usrk_wof_sz_msb);
extern int32_t mv_wtm_func_check_usrkey(mv_wtm_ss ss,
        uint32_t ksch,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        bool *is_valid);
extern int32_t mv_wtm_func_bind_pr_key(mv_wtm_ss ss,
        uint32_t pr_ksch, uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_phys, uint32_t usrk_sz,
        uint32_t aes_iv, uint32_t aes_iv_sz,
        uint32_t trans_ksch, uint32_t byte_swap,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz);
#endif /* CONFIG_EN_FIPS */

/* hdcp */
extern int32_t mv_wtm_func_hdcp_load_key(mv_wtm_ss ss, uint32_t wof_phys);

/* drbg */
extern int32_t mv_wtm_func_drbg_init(mv_wtm_ss ss);
extern int32_t mv_wtm_func_drbg_reseed(mv_wtm_ss ss,
        uint32_t seed_phys, uint32_t seed_bit_sz);
extern int32_t mv_wtm_func_drbg_gen(mv_wtm_ss ss,
        uint32_t ran_phys, uint32_t ran_bit_sz);

/* prov */
extern int32_t mv_wtm_func_prov_rkek_prov(mv_wtm_ss ss,
        uint32_t mode, uint32_t rkek_phys, uint32_t rkek_sz, uint32_t seed);
extern int32_t mv_wtm_func_prov_ec521_dk_prov(mv_wtm_ss ss,
        uint32_t mode,
        uint32_t key_msb_phys, uint32_t key_lsb_phys,
        uint32_t key_sz, uint32_t seed, uint32_t seed_sz);
extern int32_t mv_wtm_func_prov_plat_bind(mv_wtm_ss ss,
        uint32_t sch,
        uint32_t key0_phys, uint32_t key1_phys, uint32_t key_sz);
extern int32_t mv_wtm_func_prov_plat_verf(mv_wtm_ss ss,
        uint32_t sch,
        uint32_t key0_phys, uint32_t key1_phys, uint32_t key_sz,
        bool *is_valid);
extern int32_t mv_wtm_func_prov_bind_jtag_key(mv_wtm_ss ss,
        uint32_t sch,
        uint32_t key0, uint32_t key1, uint32_t key_sz);
extern int32_t mv_wtm_func_prov_verf_jtag_key(mv_wtm_ss ss,
        uint32_t sch,
        uint32_t key0, uint32_t key1, uint32_t key_sz,
        bool *is_valid);
extern int32_t mv_wtm_func_prov_dis_jtag(mv_wtm_ss ss);
extern int32_t mv_wtm_func_prov_tmp_dis_fa(mv_wtm_ss ss);
extern int32_t mv_wtm_func_prov_rd_uniq_id(mv_wtm_ss ss,
        uint32_t id_phys, uint32_t id_sz);
extern int32_t mv_wtm_func_prov_wr_plat_conf(mv_wtm_ss ss,
        uint32_t conf_phys, uint32_t conf_sz);
extern int32_t mv_wtm_func_prov_rd_plat_conf(mv_wtm_ss ss,
        uint32_t conf_phys, uint32_t conf_sz);
extern int32_t mv_wtm_func_prov_wr_usb_id(mv_wtm_ss ss,
        uint32_t id_phys, uint32_t id_sz);
extern int32_t mv_wtm_func_prov_rd_usb_id(mv_wtm_ss ss,
        uint32_t id_phys, uint32_t id_sz);
extern int32_t mv_wtm_func_prov_hdcp_wrap_key(mv_wtm_ss ss,
        uint32_t key_set_phys, uint32_t ksv_phys, uint32_t wof_phys);
extern int32_t mv_wtm_func_prov_set_fips_perm(mv_wtm_ss ss);
extern int32_t mv_wtm_func_prov_set_nonfips_perm(mv_wtm_ss ss);
extern bool mv_wtm_func_prov_is_proved(mv_wtm_ss ss);
extern int32_t mv_wtm_func_prov_pre_proc(mv_wtm_ss ss);
extern int32_t mv_wtm_func_prov_post_proc(mv_wtm_ss ss, uint32_t stage,
        uint8_t *wrap_subk_kad, uint8_t *wrap_subk_wof);
extern int32_t mv_wtm_func_prov_transit_key(mv_wtm_ss ss,
        uint32_t key_phys, uint32_t ksch, uint32_t type);

/* stat mgmt */
extern int32_t mv_wtm_func_get_tsr(mv_wtm_ss ss, uint32_t *tsr);
extern int32_t mv_wtm_func_life_cycle_adv(mv_wtm_ss ss);
extern int32_t mv_wtm_func_life_cycle_read(mv_wtm_ss ss, uint32_t *life_cycle);
extern int32_t mv_wtm_func_soft_ver_adv(mv_wtm_ss ss);
extern int32_t mv_wtm_func_soft_ver_read(mv_wtm_ss, uint32_t *ver);
extern int32_t mv_wtm_func_kernel_ver_read(mv_wtm_ss ss, uint32_t *ver);

/* jtag */
extern int32_t mv_wtm_func_get_jtag_nonce(mv_wtm_ss ss, uint32_t nonce_phys);
extern int32_t mv_wtm_func_set_jtag(
        mv_wtm_ss ss, uint32_t sch,
        uint32_t key0_phys, uint32_t key1_phys, uint32_t ctrl, uint32_t sig);

#ifdef __cplusplus
}
#endif

#endif /* _MV_WTM_FUNC_H_ */
