/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_xllp.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of WTM function layer
 *                Firmware version: WTMv3_Kernel_1_0_11
 *
 */

#ifndef _MV_WTM_XLLP_H_
#define _MV_WTM_XLLP_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_types.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/* the alignment requirement of the physical addressing arguments */
#define WTM_PHYS_ADDR_ALGN          (32) // TEMP WORK-AROUND (8)
#define WTM_LINK_LIST_ALGN          (8)
#define WTM_ADDR_IN_LIST_ALGN       (8)

/*
 * due to (4 * N) limit of memory length,
 * the address should be 4-byte-aligned.
 */
#define WTM_ADDR_ALGN_FOR_LINK_LIST (4)
#define WTM_LEN_ALGN_FOR_LINK_LIST  (4)

#define WTM_KAD_SZ                  (32) /* sizeof(SHA256) */

#define WTM_STAGE_VG                (0)
#define WTM_STAGE_CM                (1)
#define WTM_STAGE_DM                (2)
#define WTM_STAGE_DD                (3)
#define WTM_STAGE_FA                (4)

#define WTM_SYM_WOF_SZ              (416)
#define WTM_ASYM_WOF_SZ             (736)

/*
 ******************************
 *          TYPES
 ******************************
 */

typedef struct _dma_node
{
    uint32_t trans_phys;
    uint32_t trans_sz;
    uint32_t next_phys;
    uint32_t reserved;
} dma_node;

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#endif /* _MV_WTM_XLLP_H_ */
