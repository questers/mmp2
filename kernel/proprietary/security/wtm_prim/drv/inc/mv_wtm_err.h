/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_err.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of WTM errors.
 *
 */

#ifndef _MV_WTM_ERR_H_
#define _MV_WTM_ERR_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

/*
 ******************************
 *          MACROS
 ******************************
 */

#define MV_WTM_SUCCESS          (0x00000000)
#define MV_WTM_ERR_HARDWARE     (0x00010000)
#define MV_WTM_ERR_AUTH         (0x00020000)
#define MV_WTM_ERR_LIFE_CYCLE   (0x00020001)
#define MV_WTM_ERR_INVALID_WOF  (0x00020002)
#define MV_WTM_ERR_OUT_OF_MEM   (0x00020003)
#define MV_WTM_ERR_GEN_KEY      (0x00020004)
#define MV_WTM_ERR_INVALID_KEY  (0x00020005)
#define MV_WTM_ERR_PERM_DENIED  (0x00020006)

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#endif /* _MV_WTM_ERR_H_ */
