/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_ec_dsa.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of ec-dsa logic in WTM function layer
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func_gen.h"
#include "mv_wtm_func_logic.h"
#include "mv_wtm_func_km.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/* NEED TO CHANGE IF WTM_CRYPTO_SCHEME CHANGES/EXTENDS */
/* ec-dsa sch to pi */
#define EC_DSA_FUNC_TO_INIT(_func)              \
        ((_func) ? (WTM_ECCP_DSA_SIGN_INIT) :   \
        (WTM_ECCP_DSA_VERIFY_INIT))
#define EC_DSA_FUNC_TO_ZERO(_func)              \
        (WTM_ECCP_DSA_ZEROIZE)
#define EC_DSA_FUNC_TO_PROC(_func)              \
        ((_func) ? (WTM_ECCP_DSA_SIGN_UPDATE) : \
        (WTM_ECCP_DSA_VERIFY_UPDATE))
#define EC_DSA_FUNC_TO_FINI(_func)              \
        ((_func) ? (WTM_ECCP_DSA_SIGN_FINAL) :  \
        (WTM_ECCP_DSA_VERIFY_FINAL))
#define EC_DSA_FUNC_TO_ALL(_func)               \
        ((_func) ? (WTM_ECCP_DSA_SIGN) :        \
        (WTM_ECCP_DSA_VERIFY))

#define EC_DSA_PS_INIT(_s)                                  \
    do {                                                    \
        (_s)->ps.init = EC_DSA_FUNC_TO_INIT((_s)->func);    \
        (_s)->ps.zero = EC_DSA_FUNC_TO_ZERO((_s)->func);    \
        (_s)->ps.proc = EC_DSA_FUNC_TO_PROC((_s)->func);    \
        (_s)->ps.fini = EC_DSA_FUNC_TO_FINI((_s)->func);    \
        (_s)->ps.all  = EC_DSA_FUNC_TO_ALL((_s)->func);     \
    } while (0)

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t mv_wtm_func_ec_dsa_init_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t key_phys, uint32_t domain_phys, uint32_t mode)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    s->sch = sch;
    s->func = func;
    s->mode = WTM_NONFIPS_MODE;
    s->rsvd.slot_id = NULL;

    EC_DSA_PS_INIT(s);

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    s->cntx = MV_WTM_FUNC_CREATE_CNTX(s);
    MV_WTM_FUNC_ASSERT(s->cntx);

    MV_WTM_FUNC_SET_4_PARAMS(sch, key_phys, domain_phys, mode);
    MV_WTM_FUNC_SET_CMD(s->ps.init);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_STORE_CNTX(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_ec_dsa_init_nonfips);

#ifdef CONFIG_EN_FIPS

int32_t mv_wtm_func_ec_dsa_init_fips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof_phys, uint32_t usrk_wof_phys,
        uint32_t domain_phys, uint32_t mode)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t subk_cache_slot_id = WTM_INVALID_CACHE_SLOT_ID;
    uint32_t usrk_cache_slot_id = WTM_INVALID_CACHE_SLOT_ID;
    int32_t ret;

    s->sch = sch;
    s->func = func;
    s->mode = WTM_FIPS_MODE;

    EC_DSA_PS_INIT(s);

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    s->cntx = MV_WTM_FUNC_CREATE_CNTX(s);
    MV_WTM_FUNC_ASSERT(s->cntx);

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* two wrappings for subk and usrk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys, kr_kad_virt,
            &subk_cache_slot_id);
    if (ret) {
        goto _out;
    }

    ret = mv_wtm_func_load_key_to_eng(
            sch, usrk_wof_phys, usrk_kad_virt,
            &usrk_cache_slot_id);
    if (ret) {
        goto _out;
    }

    mv_wtm_func_save_usrk_info(
            usrk_kad_virt, usrk_cache_slot_id, s);

    /* subkey is useless from now on */

    /* lets batch 3 cmds */
    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_AES_ZEROIZE);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot_id);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_4_PARAMS(sch, 0, domain_phys, mode);
    MV_WTM_FUNC_SET_CMD(s->ps.init);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_STORE_CNTX(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;

_out:

    if (WTM_INVALID_CACHE_SLOT_ID != subk_cache_slot_id) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot_id);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
    }

    if (s->cntx) {
        MV_WTM_FUNC_DESTROY_CNTX(s);
    }

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_ERR_AUTH;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_ec_dsa_init_fips);

#endif /* CONFIG_EN_FIPS */

int32_t mv_wtm_func_ec_dsa_update(
        mv_wtm_ss ss, uint32_t src_phys, uint32_t src_dma_phys, uint32_t len)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_LOAD_CNTX(s);

    MV_WTM_FUNC_SET_PARAMS(
            src_phys, len, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, src_dma_phys, 0);
    MV_WTM_FUNC_SET_CMD(s->ps.proc);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_STORE_CNTX(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_ec_dsa_update);

int32_t mv_wtm_func_ec_dsa_final(
        mv_wtm_ss ss, uint32_t src_phys, uint32_t src_dma_phys, uint32_t len,
        uint32_t sig_r_phys, uint32_t sig_s_phys, bool *verf)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t slot_id;

    MV_WTM_FUNC_LOCK;
    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_LOAD_CNTX(s);

    slot_id = s->rsvd.slot_id ?
        *(s->rsvd.slot_id) : WTM_INVALID_CACHE_SLOT_ID;

    if (0 == s->func) {
        MV_WTM_FUNC_SET_PARAMS(
                src_phys, len, slot_id, sig_r_phys,
                sig_s_phys, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, src_dma_phys, 0);
        MV_WTM_FUNC_SET_CMD(s->ps.fini);
        MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;

        if (g_wtm_func_drv.irq_stat) {
            *verf = false;
        } else {
            *verf = true;
        }

#ifdef CONFIG_EN_FIPS

        if (WTM_FIPS_MODE == s->mode) {
            mv_wtm_func_save_usrk_info(0, WTM_INVALID_CACHE_SLOT_ID, s);
        }

#endif
    } else {
        /* batch 2 cmds when signing */
        MV_WTM_FUNC_SET_1_PARAM(2);
        MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
        MV_WTM_FUNC_CHK_BAT_IRQ;

        MV_WTM_FUNC_SET_PARAMS(
                src_phys, len, slot_id, sig_r_phys,
                sig_s_phys, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, src_dma_phys, 0);
        MV_WTM_FUNC_SET_CMD(s->ps.fini);
        MV_WTM_FUNC_CHK_BAT_IRQ;

#ifdef CONFIG_EN_FIPS

        if (WTM_FIPS_MODE == s->mode) {
            mv_wtm_func_save_usrk_info(0, WTM_INVALID_CACHE_SLOT_ID, s);
        }

#endif
    }

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(s->ps.zero);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_ec_dsa_final);

int32_t mv_wtm_func_ec_dsa_kill_seq(mv_wtm_ss ss)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_LOAD_CNTX(s);

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(s->ps.zero);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_ec_dsa_kill_seq);

int32_t mv_wtm_func_ec_dsa_op_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t key_phys, uint32_t domain_phys, uint32_t mode,
        uint32_t src_phys, uint32_t src_dma_phys, uint32_t len,
        uint32_t sig_r_phys, uint32_t sig_s_phys, bool *verf)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    s->sch = sch;
    s->func = func;
    s->mode = WTM_NONFIPS_MODE;

    EC_DSA_PS_INIT(s);

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    if (0 == s->func) {
        MV_WTM_FUNC_SET_PARAMS(
                sch, src_phys, len, key_phys,
                domain_phys, mode, sig_r_phys, sig_s_phys,
                0, 0, 0, 0,
                0, 0, src_dma_phys, 0);
        MV_WTM_FUNC_SET_CMD(s->ps.all);
        MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;

        if (g_wtm_func_drv.irq_stat) {
            *verf = false;
        } else {
            *verf = true;
        }
    } else {
        MV_WTM_FUNC_SET_1_PARAM(2);
        MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
        MV_WTM_FUNC_CHK_BAT_IRQ;

        MV_WTM_FUNC_SET_PARAMS(
                sch, src_phys, len, WTM_INVALID_CACHE_SLOT_ID,
                key_phys, domain_phys, mode, sig_r_phys,
                sig_s_phys, 0, 0, 0,
                0, 0, src_dma_phys, 0);
        MV_WTM_FUNC_SET_CMD(s->ps.all);
        MV_WTM_FUNC_CHK_BAT_IRQ;
    }

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(s->ps.zero);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_ec_dsa_op_nonfips);

#ifdef CONFIG_EN_FIPS

int32_t mv_wtm_func_ec_dsa_op_fips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof_phys, uint32_t usrk_wof_phys,
        uint32_t domain_phys, uint32_t mode,
        uint32_t src_phys, uint32_t src_dma_phys, uint32_t len,
        uint32_t sig_r_phys, uint32_t sig_s_phys, bool *verf)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t subk_cache_slot_id = WTM_INVALID_CACHE_SLOT_ID;
    uint32_t usrk_cache_slot_id = WTM_INVALID_CACHE_SLOT_ID;
    int32_t ret;

    s->sch = sch;
    s->func = func;
    s->mode = WTM_FIPS_MODE;

    EC_DSA_PS_INIT(s);

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* two wrappings for subk and usrk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys, kr_kad_virt,
            &subk_cache_slot_id);
    if (ret) {
        goto _out;
    }

    ret = mv_wtm_func_load_key_to_eng(
            sch, usrk_wof_phys, usrk_kad_virt,
            &usrk_cache_slot_id);
    if (ret) {
        goto _out;
    }

    /* subkey is useless from now on */

    /* lets batch 2 cmds */
    MV_WTM_FUNC_SET_1_PARAM(2);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_AES_ZEROIZE);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot_id);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_SET_PARAMS(
            sch, src_phys, len, usrk_cache_slot_id,
            0, domain_phys, mode, sig_r_phys,
            sig_s_phys, 0, 0, 0,
            0, 0, src_dma_phys, 0);
    MV_WTM_FUNC_SET_CMD(s->ps.all);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;

    if ((0 == func) && g_wtm_func_drv.irq_stat) {
        *verf = false;
    } else {
        *verf = true;
    }

    mv_wtm_func_save_usrk_info(
            0, WTM_INVALID_CACHE_SLOT_ID, s);

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(s->ps.zero);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;

_out:

    if (WTM_INVALID_CACHE_SLOT_ID != subk_cache_slot_id) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot_id);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
    }

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_ERR_AUTH;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_ec_dsa_op_fips);

#endif /* CONFIG_EN_FIPS */
