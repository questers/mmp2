/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_func_cntx.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of context mangement
 *                for WTM function layer
 *
 */

#ifndef _MV_WTM_FUNC_CNTX_H_
#define _MV_WTM_FUNC_CNTX_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func.h"

#include "mv_wtm_func_gen.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

typedef void * mv_wtm_func_cntx;

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */
#ifdef CONFIG_EN_FIPS
extern int32_t mv_wtm_func_init_fips_res_list(void);
extern void mv_wtm_func_cleanup_fips_res_list(void);
#endif

extern mv_wtm_func_cntx mv_wtm_func_create_cntx(mv_wtm_func_ss_priv *s);
extern void mv_wtm_func_destroy_cntx(
        mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx);
#ifdef CONFIG_EN_LAZY_CNTX
extern mv_wtm_func_cntx mv_wtm_func_create_lazy_cntx(mv_wtm_func_ss_priv *s);
extern void mv_wtm_func_destroy_lazy_cntx(
        mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx);
#endif
#ifdef CONFIG_EN_FIPS
extern void mv_wtm_func_save_usrk_info(
        uint32_t kad_virt, uint32_t cache_slot_id, mv_wtm_func_ss_priv *s);
#endif
extern uint32_t mv_wtm_func_get_mode_by_cntx(mv_wtm_func_cntx cntx);
extern void mv_wtm_func_switch_mode(mv_wtm_func_ss_priv *s);
extern void mv_wtm_func_load_cntx(
        mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx);
extern void mv_wtm_func_store_cntx(
        mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx);
#ifdef CONFIG_EN_LAZY_CNTX
extern void mv_wtm_func_load_lazy_cntx(
        mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx);
extern void mv_wtm_func_store_lazy_cntx(
        mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx);
extern mv_wtm_func_cntx mv_wtm_func_create_lazy_cntx_oneshot(
        mv_wtm_func_ss_priv *s);
extern void mv_wtm_func_destroy_lazy_cntx_oneshot(
        mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx);
#endif
extern uint32_t mv_wtm_func_get_freed_slt_num(void);
extern bool mv_wtm_func_need_wrap_slt_out(void);
extern int32_t mv_wtm_func_wrap_slt_out(void);
#ifdef CONFIG_EN_DBG
extern uint32_t mv_wtm_func_get_cntx_num_in_list(void);
#endif

#endif /* _MV_WTM_FUNC_CNTX_H_ */
