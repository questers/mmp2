/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_xllp.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of XLLP in WTM function layer
 *                Firmware version: WTMv3_Kernel_1_0_11
 *
 */

#ifndef _MV_WTM_FUNC_XLLP_H_
#define _MV_WTM_FUNC_XLLP_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_xllp.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#define WTM_KEY_AUTH_DATA_SZ        (32)
#define WTM_THRD_ID_SZ              (16)
#define WTM_THRD_ID_DGST_SZ         (32)
#define WTM_OVERALL_NONCE_SZ        (48)
#define WTM_OIAP_TAG_SZ             (32)
#define WTM_JTAG_NONCE_SZ           (8)

/* wtm mode */
#define WTM_NONFIPS_MODE            (0)
#define WTM_FIPS_MODE               (1)

/* wtm key type */
#define WTM_KEY_TYPE_SYM            (1)
#define WTM_KEY_TYPE_ASYM           (2)

/* wtm key output type */
#define WTM_KEY_INTERNAL            (0)
#define WTM_KEY_EXTERNAL            (1)

/* wtm fifo cap */
#define WTM_CMD_FIFO_MAX_NUM        (4)
#define WTM_CMD_FIFO_NUM_MASK       (0x00000007)

#define WTM_INVALID_CACHE_SLOT_ID   (0xFFFFFFFF)

#define WTM_LIFE_CYCLE_CM           (0)
#define WTM_LIFE_CYCLE_DM           (1)
#define WTM_LIFE_CYCLE_DD           (2)
#define WTM_LIFE_CYCLE_FA           (3)

#define WTM_TSR_RKEK_PROV_MASK      (0x00000001)
#define WTM_TSR_FIPS_ONLY_MASK      (0x00002000)
#define WTM_TSR_NONFIPS_ONLY_MASK   (0x00004000)
#define WTM_TSR_INIT_MASK           (0x00010000)
#define WTM_TSR_OP_STATUS_MASK      (0x00038000)

#define WTM_TSR_RKEK_PROVED         (0x00000001)
#define WTM_TSR_FIPS_ONLY           (0x00002000)
#define WTM_TSR_NONFIPS_ONLY        (0x00004000)
#define WTM_TSR_IS_INITED           (0x00010000)
#define WTM_TSR_OP_STATE_FIPS       (0x00010000)
#define WTM_TSR_OP_STATE_ACC        (0x00018000)

#define STATUS_HASH_TIMEOUT         (316)
#define STATUS_AES_TIMEOUT          (317)
#define STATUS_DES_TIMEOUT          (320)
/* wtm dma bus width & burst count */
#define WTM_DMA_BUS_WIDTH_32_BITS   (32)
#define WTM_DMA_BUS_WIDTH_64_BITS   (64)
#define WTM_DMA_BURST_COUNT         (1)

/* wtm dma endian mode */
#define WTM_DMA_LITTLE_ENDIAN       (0)
#define WTM_DMA_BIG_ENDIAN          (1)

/* bit definition for HostIntrStatus register */
#define WTM_IRQ_STAT_HST_QUEUE_FULL     (1 << 18)
#define WTM_IRQ_STAT_HST_QUEUE_BAD_ACC  (1 << 17)
#define WTM_IRQ_STAT_HST_ADDR_EXCPTN    (1 << 16)
#define WTM_IRQ_STAT_SP_CMD_COMPLT      (1 << 0)

/* NEED TO CHANGE IF WTM_CRYPTO_SCHEME CHANGES/EXTENDS */
/* wtm sch to key type: see "wtm key type" above */
#define WTM_SCH_TO_KEY_TYPE(_sch)               \
    (((0x0000A000 == ((_sch) & 0x0000F000)) ||  \
     (0x0000B000 == ((_sch) & 0x0000F000))) ?   \
     WTM_KEY_TYPE_ASYM : WTM_KEY_TYPE_SYM)
#define WTM_SCH_TO_WOF_SZ(_sch)                 \
    (((0x0000A000 == ((_sch) & 0x0000F000)) ||  \
     (0x0000B000 == ((_sch) & 0x0000F000))) ?   \
     WTM_ASYM_WOF_SZ : WTM_SYM_WOF_SZ)

/* NEED TO CHANGE IF WTM_CRYPTO_SCHEME CHANGES/EXTENDS */
#define WTM_IS_SCH_HASH(_sch)   \
        ((0x9000 == ((_sch) & 0xF000)) && (((_sch) & 0xF0) < 0x40))
#define WTM_IS_SCH_PKCS(_sch)   \
        (0xA000 == ((_sch) & 0xF000))

/* NEED TO CHANGE IF WTM_CRYPTO_SCHEME CHANGES/EXTENDS */
#define WTM_ECC_MULT_RANDOM_MODE    (0)
#define WTM_ECC_MULT_FIXED_MODE     (1)

/*
 ******************************
 *          TYPES
 ******************************
 */
 
typedef enum _WTM_CRYPTO_SCHEME {
    WTM_INVALID_SCHEME              = 0x00000000,
    WTM_AES_ECB128                  = 0x00008000,
    WTM_AES_ECB192                  = 0x00008002,
    WTM_AES_ECB256                  = 0x00008001,
    WTM_AES_CBC128                  = 0x00008004,
    WTM_AES_CBC192                  = 0x00008006,
    WTM_AES_CBC256                  = 0x00008005,
    WTM_AES_CTR128                  = 0x00008008,
    WTM_AES_CTR192                  = 0x0000800A,
    WTM_AES_CTR256                  = 0x00008009,
    WTM_AES_XTS256                  = 0x0000800C,
    WTM_AES_XTS512                  = 0x0000800D,
    WTM_SHA1                        = 0x00009000,
    WTM_SHA224                      = 0x00009020,
    WTM_SHA256                      = 0x00009010,
    WTM_SHA384                      = 0x00009050,
    WTM_SHA512                      = 0x00009040,
    WTM_MD5                         = 0x00009030,
    WTM_HMAC_SHA1                   = 0x00009080,
    WTM_HMAC_SHA224                 = 0x000090A0,
    WTM_HMAC_SHA256                 = 0x00009090,
    WTM_HMAC_SHA384                 = 0x000090D0,
    WTM_HMAC_SHA512                 = 0x000090C0,
    WTM_HMAC_MD5                    = 0x000090B0,
    WTM_RSA_DH_1024                 = 0x0000A001,
    WTM_RSA_DH_2048                 = 0x0000A002,
    WTM_PKCSV1_SHA1_1024RSA         = 0x0000A100,
    WTM_PKCSV1_SHA224_1024RSA       = 0x0000A120,
    WTM_PKCSV1_SHA256_1024RSA       = 0x0000A110,
    WTM_PKCSV1_SHA1_2048RSA         = 0x0000A200,
    WTM_PKCSV1_SHA224_2048RSA       = 0x0000A220,
    WTM_PKCSV1_SHA256_2048RSA       = 0x0000A210,
    WTM_ECCP224_DH                  = 0x0000B000,
    WTM_ECCP256_DH                  = 0x0000B100,
    WTM_ECCP384_DH                  = 0x0000B200,
    WTM_ECCP521_DH                  = 0x0000B300,
    WTM_ECCP224_DSA_SHA1            = 0x0000B001,
    WTM_ECCP224_DSA_SHA224          = 0x0000B021,
    WTM_ECCP224_DSA_SHA256          = 0x0000B011,
    WTM_ECCP224_DSA_SHA384          = 0x0000B051,
    WTM_ECCP224_DSA_SHA512          = 0x0000B041,
    WTM_ECCP256_DSA_SHA1            = 0x0000B101,
    WTM_ECCP256_DSA_SHA224          = 0x0000B121,
    WTM_ECCP256_DSA_SHA256          = 0x0000B111,
    WTM_ECCP256_DSA_SHA384          = 0x0000B151,
    WTM_ECCP256_DSA_SHA512          = 0x0000B141,
    WTM_ECCP384_DSA_SHA1            = 0x0000B201,
    WTM_ECCP384_DSA_SHA224          = 0x0000B221,
    WTM_ECCP384_DSA_SHA256          = 0x0000B211,
    WTM_ECCP384_DSA_SHA384          = 0x0000B251,
    WTM_ECCP384_DSA_SHA512          = 0x0000B241,
    WTM_ECCP521_DSA_SHA1            = 0x0000B301,
    WTM_ECCP521_DSA_SHA224          = 0x0000B321,
    WTM_ECCP521_DSA_SHA256          = 0x0000B311,
    WTM_ECCP521_DSA_SHA384          = 0x0000B351,
    WTM_ECCP521_DSA_SHA512          = 0x0000B341,
    WTM_DES_ECB                     = 0x0000C000,
    WTM_DES_CBC                     = 0x0000C001,
    WTM_TDES_ECB                    = 0x0000C002,
    WTM_TDES_CBC                    = 0x0000C003,
    WTM_RC4                         = 0x0000D000,
} WTM_CRYPTO_SCHEME;

typedef enum _WTM_CMD {
    //System Setup: 10 PIs
    WTM_RESET                          = 0x0000,
    WTM_INIT                           = 0x0001,
    WTM_SELF_TEST                      = 0x0002,
    WTM_SLEEP                          = 0x0003,
    WTM_CONFIGURE_DMA                  = 0x0004,
    WTM_SET_BATCH_COUNT                = 0x0005,
    WTM_ACK_BATCH_ERROR                = 0x0006,
    WTM_RESUME                         = 0x0007,
    WTM_JTAG_START                     = 0x0008,
    WTM_JTAG_AUTHORIZATION             = 0x0009,

    //State Management: 6 PIs
    WTM_GET_TRUST_STATUS_REGISTER      = 0x1000,
    WTM_LIFECYCLE_ADVANCE              = 0x1001,
    WTM_LIFECYCLE_READ                 = 0x1002,
    WTM_SOFTWARE_VERSION_ADVANCE       = 0x1003,
    WTM_SOFTWARE_VERSION_READ          = 0x1004,
    WTM_KERNEL_VERSION_READ            = 0x1005,

    //Platform Provision: 15 PIs
    WTM_RKEK_PROVISION                 = 0x2000,
    WTM_EC521_DK_PROVISION             = 0x2001,
    WTM_OEM_PLATFORM_BIND              = 0x2002,
    WTM_OEM_PLATFORM_VERIFY            = 0x2003,
    WTM_OEM_JTAG_KEY_BIND              = 0x2004,
    WTM_OEM_JTAG_KEY_VERIFY            = 0x2005,
    WTM_SET_JTAG_PERMANENT_DISABLE     = 0x2006,
    WTM_SET_TEMPORARY_FA_DISABLE       = 0x2007,
    WTM_SOC_UNIQUE_ID_READ             = 0x2008,
    WTM_OTP_WRITE_PLATFORM_CONFIG      = 0x2009,
    WTM_OTP_READ_PLATFORM_CONFIG       = 0x200A,
    WTM_OEM_USBID_PROVISION            = 0x200B,
    WTM_OEM_USBID_READ                 = 0x200C,
    WTM_HDCP_WRAP_KEY                  = 0x200D,
    WTM_HDCP_LOAD_KEY                  = 0x200E,

    //Sliding Window: 4 PIs
    WTM_SET_TIM_R                      = 0x3000,
    WTM_LAUNCH_IMG_SECURITY_CHECK      = 0x3001,
    WTM_HALT_IMG_SECURITY_CHECK        = 0x3002,
    WTM_RESUME_IMG_SECURITY_CHECK      = 0x3003,

    //FIPS Mode Management: 3 PIs
    WTM_SWITCH_FIPS_MODE               = 0x4000,
    WTM_SET_FIPS_MODE_PERMANENT        = 0x4001,
    WTM_SET_NONFIPS_MODE_PERMANENT     = 0x4002,

    //Key Management: 5 PIs
    WTM_ENROLL_KEY                     = 0x5000,
    WTM_CREATE_KEY                     = 0x5001,
    WTM_LOAD_KEY                       = 0x5002,
    WTM_OIAP_START                     = 0x5003,
    WTM_CREATE_RSA_KEYPAIR             = 0x5004,

    //Context Cache Management: 8 PIs
    WTM_GET_CONTEXT_INFO               = 0x6000,
    WTM_LOAD_ENGINE_CONTEXT            = 0x6001,
    WTM_STORE_ENGINE_CONTEXT           = 0x6002,
    WTM_WRAP_KEYCONTEXT                = 0x6003,
    WTM_UNWRAP_KEYCONTEXT              = 0x6004,
    WTM_LOAD_ENGINE_CONTEXT_EXTERNAL   = 0x6005,
    WTM_STORE_ENGINE_CONTEXT_EXTERNAL  = 0x6006,
    WTM_PURGE_CONTEXT                  = 0x6007,

    //HW-RNG: 3 PIs
    WTM_DRBG_INIT                      = 0x7000,
    WTM_DRBG_RESEED                    = 0x7001,
    WTM_DRBG_GEN_RAN_BITS              = 0x7002,

    //Symmetric Cipher: 8 PIs
    WTM_AES_INIT                       = 0x8000,
    WTM_AES_ZEROIZE                    = 0x8001,
    WTM_AES_PROCESS                    = 0x8002,
    WTM_AES_FINISH                     = 0x8003,
    WTM_DES_INIT                       = 0x8004,
    WTM_DES_ZEROIZE                    = 0x8005,
    WTM_DES_PROCESS                    = 0x8006,
    WTM_DES_FINISH                     = 0x8007,
    WTM_RC4_INIT                       = 0x8008,
    WTM_RC4_ZEROIZE                    = 0x8009,
    WTM_RC4_PROCESS                    = 0x800A,
    WTM_RC4_FINISH                     = 0x800B,

    //HASH/HMAC: 8 PIs
    WTM_HASH_INIT                      = 0x9000,
    WTM_HASH_ZEROIZE                   = 0x9001,
    WTM_HASH_UPDATE                    = 0x9002,
    WTM_HASH_FINAL                     = 0x9003,
    WTM_HMAC_INIT                      = 0x9004,
    WTM_HMAC_ZEROIZE                   = 0x9005,
    WTM_HMAC_UPDATE                    = 0x9006,
    WTM_HMAC_FINAL                     = 0x9007,

    //RDSA: 9 PIs
    WTM_EMSA_PKCS1_V15_VERIFY          = 0xA000,
    WTM_EMSA_PKCS1_V15_VERIFY_INIT     = 0xA001,
    WTM_EMSA_PKCS1_V15_VERIFY_UPDATE   = 0xA002,
    WTM_EMSA_PKCS1_V15_VERIFY_FINAL    = 0xA003,
    WTM_EMSA_PKCS1_V15_ZEROIZE         = 0xA004,
    WTM_EMSA_PKCS1_V15_SIGN            = 0xA005,
    WTM_EMSA_PKCS1_V15_SIGN_INIT       = 0xA006,
    WTM_EMSA_PKCS1_V15_SIGN_UPDATE     = 0xA007,
    WTM_EMSA_PKCS1_V15_SIGN_FINAL      = 0xA008,
    WTM_RSA_KEY_GEN                    = 0xA009,
    WTM_DH_SYSPARAM_GEN                = 0xA00A,
    WTM_DH_PUBLIC_KEY_DERIVE           = 0xA00B,
    WTM_DH_SHARED_KEY_GEN              = 0xA00C,
    WTM_DH_SAVE_SHARED_KEY             = 0xA00D,
    WTM_DH_DK_PUBLIC_KEY_DERIVE        = 0xA00E,
    WTM_DH_DK_SHARED_KEY_GEN           = 0xA00F,

    // ECC new PIs, total 16 PIs
    WTM_ECCP_DSA_ZEROIZE               = 0xB000,
    WTM_ECCP_KEYPAIR_GEN               = 0xB001,
    WTM_ECCP_PUBLIC_KEY_DERIVATION     = 0xB002,
    WTM_ECCP_DH_SHARED_KEY             = 0xB003,
    WTM_ECCP_DSA_SIGN_INIT             = 0xB004,
    WTM_ECCP_DSA_SIGN_UPDATE           = 0xB005,
    WTM_ECCP_DSA_SIGN_FINAL            = 0xB006,
    WTM_ECCP_DSA_VERIFY_INIT           = 0xB007,
    WTM_ECCP_DSA_VERIFY_UPDATE         = 0xB008,
    WTM_ECCP_DSA_VERIFY_FINAL          = 0xB009,
    WTM_ECCP_DSA_SIGN                  = 0xB00A,
    WTM_ECCP_DSA_VERIFY                = 0xB00B,
    WTM_ECCP_POINT_MAC                 = 0xB00C,
    WTM_ECCP_SAVE_SHARED_KEY           = 0xB00D,
    WTM_ECCP_CUSTOMIZED_KEA            = 0xB00E,
    WTM_MODULAR_MAC                    = 0xB00F,

    // one addition PI for DK and reserved test
    WTM_OTP_DK_LOAD                    = 0xC000,
    WTM_OTP_DK_DISABLE                 = 0xC001,
    WTM_OTP_DSK_LOAD                   = 0xC002,
    WTM_OTP_DSK_REMOVE                 = 0xC003,

    // EC-ELGAMAL
    WTM_ECCP_ELGAMAL_ENCRYPT           = 0XC004,
    WTM_ECCP_ELGAMAL_DECRYPT           = 0XC005,
    WTM_ECCP_SAVE_ELGAMAL_KEY          = 0XC006,

    // PR-specific
    WTM_ECCP_PR256_DECRYPT             = 0XC007,
    WTM_AES_PR_ECB128_DECRYPT          = 0XC008,    

    // RESERVED
    WTM_PR_KEY_BIND                    = 0xC009,
    WTM_OEM_TRANSIT_KEY_PROVISION      = 0xC00A,
    WTM_RESERVED_TEST                  = 0xC00B,
    
} WTM_CMD;

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#ifdef CONFIG_EN_FIPS

extern int32_t mv_wtm_func_xllp_set_wrap_subk(
        uint8_t *wrap_subk_kad, uint8_t *wrap_subk_wof);

#endif

extern int32_t mv_wtm_func_xllp_init(void);
extern void mv_wtm_func_xllp_cleanup(void);

#endif /* _MV_WTM_FUNC_XLLP_H_ */
