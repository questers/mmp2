/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_os_srv.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The internal header file of WTM function layer definition for Linux kernel
 *                This file is os-specific
 *
 */

#ifndef _MV_WTM_FUNC_OS_SRV_H_
#define _MV_WTM_FUNC_OS_SRV_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/dma-mapping.h>
#include <linux/interrupt.h>
#include <linux/semaphore.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/uaccess.h>
#include <linux/proc_fs.h>
#include <asm/cacheflush.h>
#include <asm/io.h>

#include "mv_wtm_func_gcc.h"
#include "mv_wtm_func_types.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#ifdef CONFIG_EN_DBG

#define MV_WTM_FUNC_ASSERT(x)                   \
    if (!(x)) {                                 \
        mv_wtm_func_pr("ASSERT FAILURE:\n");    \
        mv_wtm_func_pr("%s (%d): %s\n",         \
            __FILE__, __LINE__, __FUNCTION__);  \
        dump_stack();                           \
        while (1) msleep(1000);                 \
    }

#else

#define MV_WTM_FUNC_ASSERT(x)

#endif /* CONFIG_EN_DBG */

#ifdef CONFIG_EN_EXPORT

#define MV_WTM_FUNC_EXPORT  EXPORT_SYMBOL

#else

#define MV_WTM_FUNC_EXPORT

#endif /* CONFIG_EN_EXPORT */

#ifndef INFINITE
#define INFINITE    (-1UL)
#endif

#define mv_wtm_func_list_iter(head, entry)          \
    for ((entry) = (head)->next; (entry) != (head); (entry) = (entry)->next)

#define mv_wtm_func_list_iter_safe(head, entry, local)      \
    for ((entry) = (head)->next, (local) = (entry)->next;   \
            (entry) != (head); (entry) = (local), (local) = (entry)->next)

#define mv_wtm_func_list_get_entry(addr, type, mem) \
    container_of(addr, type, mem)

#define MV_WTM_FUNC_INVALID_PHYS_ADDR   (0xFFFFFFFF)

#define PRIM_LAYER_VERSION (0x03010B)
#define MAJOR_VERSION(x) ((x & 0xFF0000) >> 16)
#define MINOR_VERSION(x) ((x & 0xFF00) >> 8)
#define LAST_VERSION(x) (x & 0xFF)

/*
 ******************************
 *          TYPES
 ******************************
 */

typedef void * mv_wtm_func_irq;
typedef struct __event
{
    struct completion comp;
} _event;
typedef void * mv_wtm_func_event;
#ifdef CONFIG_EN_MULTI_THRD
typedef struct __mutex
{
    struct semaphore sem;
} _mutex;
typedef void * mv_wtm_func_mutex;
#endif

typedef struct _mv_wtm_func_list
{
    struct _mv_wtm_func_list *next;
    struct _mv_wtm_func_list *prev;
} mv_wtm_func_list;

#ifdef CONFIG_EN_WTM_PROC
typedef struct _mv_wtm_func_proc
{
    struct proc_dir_entry *wtm_entry;
    struct proc_dir_entry *info_entry;
    struct proc_dir_entry *regs_entry;
    struct proc_dir_entry *send_cmd_entry;
    struct proc_dir_entry *param_entry[16];
    struct proc_dir_entry *cmd_entry;
} mv_wtm_func_proc;
#endif

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#ifdef CONFIG_EN_FIPS
uint32_t mv_wtm_func_virt_to_phys(uint32_t virt);

/* cache */

__INLINE static void mv_wtm_func_flush_dcache(uint32_t virt, uint32_t sz)
{
    dmac_flush_range((void *)virt, (void *)(virt + sz));
}

__INLINE static void mv_wtm_func_flush_l2dcache(uint32_t virt, uint32_t sz)
{
    ulong_t flags;
    vuint32_t phys = mv_wtm_func_virt_to_phys(virt);
    local_irq_save(flags);
    outer_flush_range(phys, phys + sz);
    local_irq_restore(flags);
}

#endif /* CONFIG_EN_FIPS */

/* dbg */
extern uint32_t mv_wtm_func_pr(const void *format, ...);

/* irq */
extern mv_wtm_func_irq mv_wtm_func_request_irq(
                        int32_t irq, uint32_t flags,
                        int32_t (*isr)(void *), void *arg);
extern void mv_wtm_func_free_irq(mv_wtm_func_irq irq);

#ifdef CONFIG_EN_FIPS

/* list */
__INLINE static void mv_wtm_func_list_init(mv_wtm_func_list *head)
{
    MV_WTM_FUNC_ASSERT(head);

    head->next = head;
    head->prev = head;
}

__INLINE static void mv_wtm_func_list_add(
        mv_wtm_func_list *entry, mv_wtm_func_list *head)
{
    MV_WTM_FUNC_ASSERT(entry);
    MV_WTM_FUNC_ASSERT(head);

    entry->next = head->next;
    entry->prev = head;
    head->next->prev = entry;
    head->next = entry;
}

__INLINE static void mv_wtm_func_list_add_tail(
        mv_wtm_func_list *entry, mv_wtm_func_list *head)
{
    MV_WTM_FUNC_ASSERT(entry);
    MV_WTM_FUNC_ASSERT(head);

    entry->next = head;
    entry->prev = head->prev;
    head->prev->next = entry;
    head->prev = entry;
}

__INLINE static void mv_wtm_func_list_del(mv_wtm_func_list *entry)
{
    MV_WTM_FUNC_ASSERT(entry);

    entry->prev->next = entry->next;
    entry->next->prev = entry->prev;
}

__INLINE static bool mv_wtm_func_list_is_empty(mv_wtm_func_list *head)
{
    MV_WTM_FUNC_ASSERT(head);

    return ((head->next == head) && (head->prev == head));
}

#endif /* CONFIG_EN_FIPS */

/* mem */
__INLINE static void *mv_wtm_func_alloc_mem(uint32_t size)
{
    return kmalloc(size, GFP_KERNEL);
}

__INLINE static void mv_wtm_func_free_mem(void *mem)
{
    kfree(mem);
}

extern uint32_t mv_wtm_func_virt_to_phys(uint32_t virt);

__INLINE static void *mv_wtm_func_memset(void *s, int32_t c, uint32_t n)
{
    return memset(s, c, n);
}

__INLINE static void *mv_wtm_func_memcpy(void *d, void *s, uint32_t n)
{
    return memcpy(d, s, n);
}

__INLINE static void *mv_wtm_func_ioremap(void __user *phys, uint32_t size)
{
    return ioremap((ulong_t)phys, size);
}

__INLINE static void mv_wtm_func_iounmap(void *virt, uint32_t size)
{
    __iounmap((void __iomem *)virt);
}

extern void *mv_wtm_func_alloc_dma_mem(
                        uint32_t size, uint32_t algn, void **phys);
extern void mv_wtm_func_free_dma_mem(void *virt);

/* sync */
__INLINE static mv_wtm_func_event mv_wtm_func_create_event(void)
{
    _event *e = kmalloc(sizeof(_event), GFP_KERNEL);
    if (e) {
        init_completion(&e->comp);
    }
    return (mv_wtm_func_event)e;
}

__INLINE static void mv_wtm_func_destroy_event(mv_wtm_func_event event)
{
    kfree(event);
}

__INLINE static int32_t mv_wtm_func_wait_for_event(
        mv_wtm_func_event event, int32_t timeout)
{
    _event *e = (_event *)event;
    int32_t ret;
    if (timeout) {
        ret = wait_for_completion_timeout(&e->comp,
                (timeout < 0) ? MAX_SCHEDULE_TIMEOUT : timeout);
    } else {
        ret = !try_wait_for_completion(&e->comp);
    }
    init_completion(&e->comp);
    return ret;
}

__INLINE static void mv_wtm_func_set_event(mv_wtm_func_event event)
{
    _event *e = (_event *)event;
    complete(&e->comp);
}

#ifdef CONFIG_EN_MULTI_THRD

__INLINE static mv_wtm_func_mutex mv_wtm_func_create_mutex(void)
{
    _mutex *m = kmalloc(sizeof(_mutex), GFP_KERNEL);
    if (m) {
        sema_init(&m->sem, 1);
    }
    return (mv_wtm_func_mutex)m;
}

__INLINE static void mv_wtm_func_destroy_mutex(mv_wtm_func_mutex mutex)
{
    kfree(mutex);
}

__INLINE static int32_t mv_wtm_func_wait_for_mutex(
        mv_wtm_func_mutex mutex, int32_t timeout)
{
    _mutex *m = (_mutex *)mutex;
    if (INFINITE == timeout) {
        down(&m->sem);
        return 0;
    } else {
        return down_timeout(&m->sem, timeout);
    }
}

__INLINE static void mv_wtm_func_release_mutex(mv_wtm_func_mutex mutex)
{
    _mutex *m = (_mutex *)mutex;
    return up(&m->sem);
}

__INLINE static uint32_t mv_wtm_func_get_os_time(void)
{
    return jiffies_to_msecs(jiffies);
}

#endif /* CONFIG_EN_MULTI_THRD */

#endif /* _MV_WTM_FUNC_OS_SRV_H_ */
