/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_gen.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of WTM function layer
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func.h"

#include "mv_wtm_func_gen.h"
#include "mv_wtm_func_os_srv.h"
#include "mv_wtm_func_xllp.h"
#include "mv_wtm_func_platform.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#ifndef WINCE

MODULE_LICENSE("GPL");

#endif

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

mv_wtm_func_drv g_wtm_func_drv;

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

/* general functions */

static void _mv_wtm_func_free_res(void)
{
#ifdef CONFIG_EN_MULTI_THRD

    if (g_wtm_func_drv.lock) {
        mv_wtm_func_destroy_mutex(g_wtm_func_drv.lock);
    }

#endif

    if (g_wtm_func_drv.irq) {
        mv_wtm_func_free_irq(g_wtm_func_drv.irq);
    }

    if (g_wtm_func_drv.irq_done) {
        mv_wtm_func_destroy_event(g_wtm_func_drv.irq_done);
    }

    if (g_wtm_func_drv.regs) {
        mv_wtm_func_iounmap(g_wtm_func_drv.regs, sizeof(mv_wtm_func_regs));
    }
}

static int32_t _isr(void *arg)
{
    vuint32_t host_irq_stat;

    host_irq_stat = g_wtm_func_drv.regs->host_irq_stat;

    if (host_irq_stat & (~WTM_IRQ_STAT_SP_CMD_COMPLT)) {
        mv_wtm_func_pr("ERROR - Invalid intr stat 0x%08x\n", host_irq_stat);
    }

    if (host_irq_stat & WTM_IRQ_STAT_HST_QUEUE_BAD_ACC) {
        g_wtm_func_drv.irq_stat = MV_WTM_ERR_HARDWARE;
        mv_wtm_func_pr("ERROR - WTM fault queue full\n");
    } else if (host_irq_stat & WTM_IRQ_STAT_HST_ADDR_EXCPTN) {
        g_wtm_func_drv.irq_stat = MV_WTM_ERR_HARDWARE;
        mv_wtm_func_pr("ERROR - WTM fault data abort\n");
    } else {
        g_wtm_func_drv.irq_stat = g_wtm_func_drv.regs->cmd_ret_stat;
    }

    g_wtm_func_drv.regs->host_irq_stat = 0xFFFFFFFF;

    mv_wtm_func_set_event(g_wtm_func_drv.irq_done);

    return 0;
}

int32_t mv_wtm_func_init(void)
{
    if (g_wtm_func_drv.used_num) {
        g_wtm_func_drv.used_num++;
        return 0;
    }

    /* the first user comes here */

    mv_wtm_func_memset(&g_wtm_func_drv, 0, sizeof(mv_wtm_func_drv));

    g_wtm_func_drv.used_num = 1;

    g_wtm_func_drv.regs = mv_wtm_func_platform_ioremap();
    if (!g_wtm_func_drv.regs) {
        goto _out;
    }

    mv_wtm_func_pr("INFO - WTM reg addr = 0x%08x\n", g_wtm_func_drv.regs);

    g_wtm_func_drv.irq_done = mv_wtm_func_create_event();
    if (!g_wtm_func_drv.irq_done) {
        goto _out;
    }

    g_wtm_func_drv.irq = mv_wtm_func_platform_request_irq(_isr, NULL);
    if (!g_wtm_func_drv.irq) {
        goto _out;
    }
    /* enable all dragonite interrupts */
    g_wtm_func_drv.regs->host_irq_mask = 0x00000000;

    mv_wtm_func_pr("INFO - WTM irq hooked\n");

#ifdef CONFIG_EN_MULTI_THRD

    g_wtm_func_drv.lock = mv_wtm_func_create_mutex();
    if (!g_wtm_func_drv.lock) {
        goto _out;
    }

#endif

#ifdef CONFIG_EN_LAZY_CNTX

    g_wtm_func_drv.last_s = 0;

#endif

    if (mv_wtm_func_xllp_init()) {
        goto _out;
    }

    return 0;

_out:

    _mv_wtm_func_free_res();

    g_wtm_func_drv.used_num--;

    return -1;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_init);

void mv_wtm_func_cleanup(void)
{
    MV_WTM_FUNC_ASSERT(g_wtm_func_drv.used_num);

    if (--g_wtm_func_drv.used_num) {
        return;
    }

    /* the last user comes here */

    mv_wtm_func_xllp_cleanup();
    _mv_wtm_func_free_res();

    mv_wtm_func_memset(&g_wtm_func_drv, 0, sizeof(mv_wtm_func_drv));
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_cleanup);

/*
 * even if no CONFIG_EN_MULTI_THRD, ss is still a must
 * thinking of multi sessions in one thread
 */

mv_wtm_ss mv_wtm_func_create_ss(void)
{
    return (mv_wtm_ss)mv_wtm_func_alloc_mem(sizeof(mv_wtm_func_ss_priv));
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_ss);

void mv_wtm_func_destroy_ss(mv_wtm_ss ss)
{
    mv_wtm_func_free_mem(ss);
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_destroy_ss);
