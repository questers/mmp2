/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_cntx.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of context management
 *                for WTM function layer
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func_cntx.h"
#include "mv_wtm_func_km.h"
#include "mv_wtm_func_os_srv.h"
#include "mv_wtm_func_sw.h"
#include "mv_wtm_func_xllp.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#define INVALID_THREAD_ID_BYTE      (0x00)
#define THREAD_ID_BYTES_SIZE        (16)
#define THREAD_ID_DIGEST_BYTES_SIZE (32)
#define IS_THREAD_ID_VALID(ti, b)                       \
    do {                                                \
        uint8_t *_p = (uint8_t *)ti;                    \
        uint8_t _i;                                     \
        for (_i = 0; _i < THREAD_ID_BYTES_SIZE; _i++) { \
            if (INVALID_THREAD_ID_BYTE != _p[_i]) {     \
                break;                                  \
            }                                           \
        }                                               \
        if (_i < THREAD_ID_BYTES_SIZE) {                \
            b = true;                                   \
        } else {                                        \
            b = false;                                  \
        }                                               \
    } while (0)

#define CNTX_IN_NONFIPS(_ci) \
    (((mv_wtm_func_cntx_info *)(_ci))->cntx_info_nonfips)
#define CNTX_IN_FIPS(_ci) \
    (((mv_wtm_func_cntx_info *)(_ci))->cntx_info_fips)
#define CACHE_IN_FIPS(_ci) \
    (((mv_wtm_func_cntx_info *)(_ci))->cntx_info_fips.cache_info)

#if 0
#define NEED_SCH_SWITCH_MODE(_s)                        \
        (!(WTM_IS_SCH_HASH((_s)->sch) ||                \
        (WTM_IS_SCH_PKCS((_s)->sch) && (0 == _s->func))))
#else
#define NEED_SCH_SWITCH_MODE(_s)    (1)
#endif

#define MIN_FREED_SLOT_NUM          (2)
#define MAX_UINT32                  (0xFFFFFFFF)

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 * Currently we just support in-place wrap.
 */
typedef enum _mv_wtm_func_cache_stat {
    INVALID_CACHE_STAT,
    PLAIN_IN_CACHE,
    WRAPPED_IN_PLACE,
    WRAPPED_OUTSIDE,
} mv_wtm_func_cache_stat;

typedef struct _mv_wtm_func_cache_info {
    /* Wrap/unwraping cache needs scheme as a param */
    uint32_t                cache_sch;
    mv_wtm_func_cache_stat  cache_stat;
    /* cache activity for wrap-out */
    uint32_t                cache_act;
    /* Valid when PLAIN_IN_CACHE and WRAPPED_IN_PLACE */
    uint32_t                cache_slot_id;
    /* Valid if WRAPPED_OUTSIDE, point to a memory block */
    uint32_t                wrapper_mem_virt;
    uint32_t                wrapper_mem_phys;
} mv_wtm_func_cache_info;

/*
 * Context management.
 * It will be treated as a handle to user space.
 */
typedef union _mv_wtm_func_cntx_info {
    /* Valid when CONTEXT_NONFIPS */
    struct _cntx_info_nonfips {
        uint32_t cntx_virt;
        uint32_t cntx_phys;
    } cntx_info_nonfips;
    /* Valid when CONTEXT_FIPS */
    struct _cntx_info_fips {
        mv_wtm_func_list        fips_cntx_node;
        /* required by WTM_LOAD_ENGINE_CONTEXT. */
        uint8_t                 kad[WTM_KEY_AUTH_DATA_SZ];
        mv_wtm_func_cache_info  cache_info;
        uint32_t                thread_id_virt;
        uint32_t                thread_id_phys;
        uint32_t                dgst_virt;
        uint32_t                dgst_phys;
    } cntx_info_fips;
} mv_wtm_func_cntx_info;

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

static void _mv_wtm_func_free_res(
        mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx)
{
    mv_wtm_func_cntx_info *cntx_info = (mv_wtm_func_cntx_info *)cntx;

    if (cntx_info) {
        if (WTM_NONFIPS_MODE == s->mode) {
            if (CNTX_IN_NONFIPS(cntx_info).cntx_virt) {
                mv_wtm_func_free_dma_mem(
                        (void *)CNTX_IN_NONFIPS(cntx_info).cntx_virt);
            }
        } else {
#ifdef CONFIG_EN_FIPS

            if (WTM_INVALID_CACHE_SLOT_ID !=
                    CACHE_IN_FIPS(cntx_info).cache_slot_id) {
                MV_WTM_FUNC_SET_2_PARAMS(
                        WTM_SCH_TO_KEY_TYPE(s->sch),
                        CACHE_IN_FIPS(cntx_info).cache_slot_id);
                MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
                MV_WTM_FUNC_WAIT_FOR_IRQ;
            }
            if (CACHE_IN_FIPS(cntx_info).wrapper_mem_virt) {
                mv_wtm_func_free_dma_mem(
                        (void *)CACHE_IN_FIPS(cntx_info).wrapper_mem_virt);
            }
            if (CNTX_IN_FIPS(cntx_info).thread_id_virt) {
                mv_wtm_func_free_dma_mem(
                        (void *)CNTX_IN_FIPS(cntx_info).thread_id_virt);
            }
            if (CNTX_IN_FIPS(cntx_info).dgst_virt) {
                mv_wtm_func_free_dma_mem(
                        (void *)CNTX_IN_FIPS(cntx_info).dgst_virt);
            }
            mv_wtm_func_list_del(&(CNTX_IN_FIPS(cntx_info).fips_cntx_node));

#else

            MV_WTM_FUNC_ASSERT(0);

#endif
        }
        mv_wtm_func_free_mem(cntx_info);
    }
}

#ifdef CONFIG_EN_FIPS

int32_t mv_wtm_func_init_fips_res_list(void)
{
    mv_wtm_func_list_init(&g_wtm_func_drv.fips_cntx_list);
    return 0;
}

void mv_wtm_func_cleanup_fips_res_list(void)
{
    mv_wtm_func_list *node, *tmp;
    struct _cntx_info_fips *ci_fips;
    mv_wtm_func_list_iter_safe(&g_wtm_func_drv.fips_cntx_list, node, tmp) {
        ci_fips = mv_wtm_func_list_get_entry(node,
                struct _cntx_info_fips, fips_cntx_node);
        if (WTM_INVALID_CACHE_SLOT_ID != ci_fips->cache_info.cache_slot_id) {
            MV_WTM_FUNC_SET_2_PARAMS(
                    WTM_SCH_TO_KEY_TYPE(ci_fips->cache_info.cache_sch),
                    ci_fips->cache_info.cache_slot_id);
            MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
            MV_WTM_FUNC_WAIT_FOR_IRQ;
        }
        if (ci_fips->thread_id_virt) {
            mv_wtm_func_free_dma_mem(
                    (void *)ci_fips->thread_id_virt);
        }
        if (ci_fips->dgst_virt) {
            mv_wtm_func_free_dma_mem(
                    (void *)ci_fips->dgst_virt);
        }
        mv_wtm_func_list_del(&(ci_fips->fips_cntx_node));
    }
}

#endif /* CONFIG_EN_FIPS */

mv_wtm_func_cntx mv_wtm_func_create_cntx(mv_wtm_func_ss_priv *s)
{
    mv_wtm_func_cntx_info *cntx_info = NULL;

    MV_WTM_FUNC_ASSERT(s);

    cntx_info = mv_wtm_func_alloc_mem(sizeof(mv_wtm_func_cntx_info));
    if (NULL == cntx_info) {
        goto _out;
    }
    mv_wtm_func_memset(cntx_info, 0, sizeof(mv_wtm_func_cntx_info));

    if (WTM_NONFIPS_MODE == s->mode) {
        CNTX_IN_NONFIPS(cntx_info).cntx_virt =
                (uint32_t)mv_wtm_func_alloc_dma_mem(
                WTM_SCH_TO_WOF_SZ(s->sch), WTM_PHYS_ADDR_ALGN,
                (void **)&(CNTX_IN_NONFIPS(cntx_info).cntx_phys));
        if (0 == CNTX_IN_NONFIPS(cntx_info).cntx_virt) {
            goto _out;
        }
    } else {
#ifdef CONFIG_EN_FIPS

        CACHE_IN_FIPS(cntx_info).cache_sch = s->sch;
        CACHE_IN_FIPS(cntx_info).cache_stat = INVALID_CACHE_STAT;
        CACHE_IN_FIPS(cntx_info).cache_act = MAX_UINT32;
        CACHE_IN_FIPS(cntx_info).cache_slot_id = WTM_INVALID_CACHE_SLOT_ID;
        CACHE_IN_FIPS(cntx_info).wrapper_mem_virt =
                (uint32_t)mv_wtm_func_alloc_dma_mem(
                        ((WTM_KEY_TYPE_SYM == WTM_SCH_TO_KEY_TYPE(s->sch)) ?
                        WTM_SYM_WOF_SZ : WTM_ASYM_WOF_SZ), WTM_PHYS_ADDR_ALGN,
                        (void **)&(CACHE_IN_FIPS(cntx_info).wrapper_mem_phys));
        if (0 == CACHE_IN_FIPS(cntx_info).wrapper_mem_virt) {
            goto _out;
        }
        CNTX_IN_FIPS(cntx_info).thread_id_virt =
                (uint32_t)mv_wtm_func_alloc_dma_mem(
                THREAD_ID_BYTES_SIZE, WTM_PHYS_ADDR_ALGN,
                (void **)&(CNTX_IN_FIPS(cntx_info).thread_id_phys));
        if (0 == CNTX_IN_FIPS(cntx_info).thread_id_virt) {
            goto _out;
        }
        mv_wtm_func_memset(
                (void *)CNTX_IN_FIPS(cntx_info).thread_id_virt,
                0, THREAD_ID_BYTES_SIZE);
        CNTX_IN_FIPS(cntx_info).dgst_virt = (uint32_t)mv_wtm_func_alloc_dma_mem(
                WTM_THRD_ID_DGST_SZ, WTM_PHYS_ADDR_ALGN,
                (void **)&(CNTX_IN_FIPS(cntx_info).dgst_phys));
        if (0 == CNTX_IN_FIPS(cntx_info).dgst_virt) {
            goto _out;
        }
        mv_wtm_func_memset(
                (void *)CNTX_IN_FIPS(cntx_info).dgst_virt,
                0, WTM_THRD_ID_DGST_SZ);

        mv_wtm_func_list_add_tail(&(CNTX_IN_FIPS(cntx_info).fips_cntx_node),
                &(g_wtm_func_drv.fips_cntx_list));

#else

        MV_WTM_FUNC_ASSERT(0);

#endif
    }

    return (mv_wtm_func_cntx)cntx_info;

_out:

    _mv_wtm_func_free_res(s, cntx_info);

    return NULL;
}

void mv_wtm_func_destroy_cntx(mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx)
{
    MV_WTM_FUNC_ASSERT(s && cntx);
    _mv_wtm_func_free_res(s, cntx);
}


#ifdef CONFIG_EN_LAZY_CNTX

mv_wtm_func_cntx mv_wtm_func_create_lazy_cntx(mv_wtm_func_ss_priv *s)
{
    MV_WTM_FUNC_ASSERT((uint32_t)s != g_wtm_func_drv.last_s);

    if (g_wtm_func_drv.last_s) {
        /* it means the modes of last_s and s are the same */
        mv_wtm_func_store_cntx(
                (mv_wtm_func_ss_priv *)g_wtm_func_drv.last_s,
                ((mv_wtm_func_ss_priv *)g_wtm_func_drv.last_s)->cntx);
        g_wtm_func_drv.last_s = 0;
    }

    return mv_wtm_func_create_cntx(s);
}

void mv_wtm_func_destroy_lazy_cntx(
        mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx)
{
    mv_wtm_func_destroy_cntx(s, cntx);
    g_wtm_func_drv.last_s = 0;
}

#endif /* CONFIG_EN_LAZY_CNTX */

#ifdef CONFIG_EN_FIPS

void mv_wtm_func_save_usrk_info(
        uint32_t kad_virt, uint32_t cache_slot_id, mv_wtm_func_ss_priv *s)
{
    mv_wtm_func_cntx_info *cntx_info = (mv_wtm_func_cntx_info *)(s->cntx);

    MV_WTM_FUNC_ASSERT(s);

    CACHE_IN_FIPS(cntx_info).cache_stat = PLAIN_IN_CACHE;
    CACHE_IN_FIPS(cntx_info).cache_act = mv_wtm_func_get_os_time();
    CACHE_IN_FIPS(cntx_info).cache_slot_id = cache_slot_id;
    if (kad_virt) {
        mv_wtm_func_memcpy(
                CNTX_IN_FIPS(cntx_info).kad,
                (void *)kad_virt,
                WTM_KEY_AUTH_DATA_SZ);
    }

    s->rsvd.slot_id = &(CACHE_IN_FIPS(cntx_info).cache_slot_id);
}

#endif /* CONFIG_EN_FIPS */

#ifdef CONFIG_EN_FIPS

static void _mv_wtm_func_switch_to_nonfips(void)
{
    /*
     * FIPS --> non-FIPS:
     *
     * 1. Load wrap-subkey to AES engine by thread-id.
     * 2. WTM_WRAP_KEYCONTEXT in-place all the cache slot(s)
     *    owned by the driver.
     * 3. Purge the cache slot of wrap-subkey.
     * 4. WTM_SWITCH_FIPS_MODE to non-FIPS mode.
     */

    mv_wtm_func_list *node;
    uint32_t wrap_subk_cache_slot_id;
    struct _cntx_info_fips *ci_fips;
    uint32_t ret;

    if (!mv_wtm_func_list_is_empty(&g_wtm_func_drv.fips_cntx_list)) {
        /* wrap-in-place all the fips cache slot(s) */
        mv_wtm_func_list_iter(&g_wtm_func_drv.fips_cntx_list, node) {
            ci_fips = mv_wtm_func_list_get_entry(node,
                    struct _cntx_info_fips, fips_cntx_node);
            MV_WTM_FUNC_ASSERT(ci_fips);
            MV_WTM_FUNC_ASSERT(
                    (PLAIN_IN_CACHE == ci_fips->cache_info.cache_stat) ||
                    (WRAPPED_OUTSIDE == ci_fips->cache_info.cache_stat));

            if (PLAIN_IN_CACHE == ci_fips->cache_info.cache_stat) {
                /*
                 * do NOT need to check cache vacancy
                 * for wrap-subk's slot is reserved.
                 */
                ret = mv_wtm_func_load_key_to_eng(
                        WTM_AES_CBC256,
                        g_wtm_func_drv.wrap_subk_wof_phys,
                        (uint32_t)g_wtm_func_drv.wrap_subk_kad_virt,
                        &wrap_subk_cache_slot_id);
                MV_WTM_FUNC_ASSERT(0 == ret);

                MV_WTM_FUNC_SET_1_PARAM(2);
                MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
                MV_WTM_FUNC_CHK_BAT_IRQ;

                MV_WTM_FUNC_SET_2_PARAMS(
                        WTM_SCH_TO_KEY_TYPE(WTM_AES_CBC256),
                        wrap_subk_cache_slot_id);
                MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
                MV_WTM_FUNC_CHK_BAT_IRQ;

                MV_WTM_FUNC_SET_3_PARAMS(
                        ci_fips->cache_info.cache_sch,
                        WTM_KEY_INTERNAL,
                        ci_fips->cache_info.cache_slot_id);
                MV_WTM_FUNC_SET_CMD(WTM_WRAP_KEYCONTEXT);
                MV_WTM_FUNC_WAIT_FOR_IRQ;

                ci_fips->cache_info.cache_stat = WRAPPED_IN_PLACE;
                /* cache_act should NOT be updated since it is passive */
            }
        }
    }

    MV_WTM_FUNC_SET_1_PARAM(WTM_NONFIPS_MODE);
    MV_WTM_FUNC_SET_CMD(WTM_SWITCH_FIPS_MODE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    g_wtm_func_drv.mode = WTM_NONFIPS_MODE;
}

static void _mv_wtm_func_switch_to_fips(void)
{
    /*
     * non-FIPS --> FIPS:
     *
     * 1. WTM_SWITCH_FIPS_MODE to FIPS mode.
     * 2. call OIAP to load wrap-subkey.
     * 3. WTM_UNWRAP_KEYCONTEXT in-place all the cache slot(s)
     *    owned by the driver.
     */

    mv_wtm_func_list *node;
    uint32_t wrap_subk_cache_slot_id;
    struct _cntx_info_fips *ci_fips;
    int32_t ret;

    MV_WTM_FUNC_SET_1_PARAM(WTM_FIPS_MODE);
    MV_WTM_FUNC_SET_CMD(WTM_SWITCH_FIPS_MODE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    g_wtm_func_drv.mode = WTM_FIPS_MODE;

    mv_wtm_func_list_iter(&g_wtm_func_drv.fips_cntx_list, node) {
        ci_fips = mv_wtm_func_list_get_entry(node,
                struct _cntx_info_fips, fips_cntx_node);
        MV_WTM_FUNC_ASSERT(ci_fips);
        MV_WTM_FUNC_ASSERT(
                (WRAPPED_IN_PLACE == ci_fips->cache_info.cache_stat) ||
                (WRAPPED_OUTSIDE == ci_fips->cache_info.cache_stat));

        if (WRAPPED_IN_PLACE == ci_fips->cache_info.cache_stat) {
            /*
             * do NOT need to check cache vacancy
             * for wrap-subk's slot is reserved.
             */
            ret = mv_wtm_func_load_key_to_eng(
                    WTM_AES_CBC256,
                    g_wtm_func_drv.wrap_subk_wof_phys,
                    (uint32_t)g_wtm_func_drv.wrap_subk_kad_virt,
                    &wrap_subk_cache_slot_id);
            MV_WTM_FUNC_ASSERT(0 == ret);

            MV_WTM_FUNC_SET_1_PARAM(2);
            MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
            MV_WTM_FUNC_CHK_BAT_IRQ;

            MV_WTM_FUNC_SET_2_PARAMS(
                    WTM_SCH_TO_KEY_TYPE(WTM_AES_CBC256),
                    wrap_subk_cache_slot_id);
            MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
            MV_WTM_FUNC_CHK_BAT_IRQ;

            MV_WTM_FUNC_SET_3_PARAMS(
                    ci_fips->cache_info.cache_sch, WTM_KEY_INTERNAL,
                    ci_fips->cache_info.cache_slot_id);
            MV_WTM_FUNC_SET_CMD(WTM_UNWRAP_KEYCONTEXT);
            MV_WTM_FUNC_WAIT_FOR_IRQ;

            ci_fips->cache_info.cache_stat = PLAIN_IN_CACHE;
            /* cache_act should NOT be updated since it is passive */
        }
    }
}

void mv_wtm_func_switch_mode(mv_wtm_func_ss_priv *s)
{
    MV_WTM_FUNC_ASSERT(s);

    if (g_wtm_func_drv.mode == s->mode) {
        return;
    }

    if (!NEED_SCH_SWITCH_MODE(s)) {
        return;
    }

#ifdef CONFIG_EN_LAZY_CNTX

    /* if mode switch has to happen, store lazy cntx then */
    if (g_wtm_func_drv.last_s) {
        mv_wtm_func_store_cntx(
                (mv_wtm_func_ss_priv *)g_wtm_func_drv.last_s,
                ((mv_wtm_func_ss_priv *)g_wtm_func_drv.last_s)->cntx);
        g_wtm_func_drv.last_s = 0;
    }

#endif /* CONFIG_EN_LAZY_CNTX */

    if (WTM_NONFIPS_MODE == s->mode) {
        _mv_wtm_func_switch_to_nonfips();
    } else {
        _mv_wtm_func_switch_to_fips();
    }
}

#else /* !CONFIG_EN_FIPS */

void mv_wtm_func_switch_mode(mv_wtm_func_ss_priv *s)
{
    /* nothing */;
}

#endif /* CONFIG_EN_FIPS */

void mv_wtm_func_load_cntx(mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx)
{
    mv_wtm_func_cntx_info *cntx_info = (mv_wtm_func_cntx_info *)cntx;

    MV_WTM_FUNC_ASSERT(cntx);

    if (WTM_NONFIPS_MODE == s->mode) {
        MV_WTM_FUNC_SET_2_PARAMS(
                s->sch, CNTX_IN_NONFIPS(cntx_info).cntx_phys);
        MV_WTM_FUNC_SET_CMD(WTM_LOAD_ENGINE_CONTEXT_EXTERNAL);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
    } else {
#ifndef CONFIG_EN_FIPS

        MV_WTM_FUNC_ASSERT(0);

#else /* !CONFIG_EN_FIPS */

        uint32_t wrap_subk_cache_slot_id;
        int32_t ret;

        MV_WTM_FUNC_ASSERT(
                (PLAIN_IN_CACHE == CACHE_IN_FIPS(cntx_info).cache_stat) ||
                (WRAPPED_OUTSIDE == CACHE_IN_FIPS(cntx_info).cache_stat));

        if (WRAPPED_OUTSIDE == CACHE_IN_FIPS(cntx_info).cache_stat) {
            if (mv_wtm_func_need_wrap_slt_out()) {
                ret = mv_wtm_func_wrap_slt_out();
                MV_WTM_FUNC_ASSERT(0 == ret);
            }

            /*
             * do NOT need to check cache vacancy
             * for wrap-subk's slot is reserved.
             */
            ret = mv_wtm_func_load_key_to_eng(
                    WTM_AES_CBC256,
                    g_wtm_func_drv.wrap_subk_wof_phys,
                    (uint32_t)g_wtm_func_drv.wrap_subk_kad_virt,
                    &wrap_subk_cache_slot_id);
            MV_WTM_FUNC_ASSERT(0 == ret);

            MV_WTM_FUNC_SET_1_PARAM(2);
            MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
            MV_WTM_FUNC_CHK_BAT_IRQ;

            MV_WTM_FUNC_SET_2_PARAMS(
                    WTM_SCH_TO_KEY_TYPE(WTM_AES_CBC256),
                    wrap_subk_cache_slot_id);
            MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
            MV_WTM_FUNC_CHK_BAT_IRQ;

            MV_WTM_FUNC_SET_4_PARAMS(s->sch, WTM_KEY_EXTERNAL,
                    0, CACHE_IN_FIPS(cntx_info).wrapper_mem_phys);
            MV_WTM_FUNC_SET_CMD(WTM_UNWRAP_KEYCONTEXT);
            MV_WTM_FUNC_WAIT_FOR_IRQ;
            MV_WTM_FUNC_READ_1_RES(&CACHE_IN_FIPS(cntx_info).cache_slot_id);

            CACHE_IN_FIPS(cntx_info).cache_stat = PLAIN_IN_CACHE;
            /* cache_act will be updated below */
        }

        ret = mv_wtm_func_hmac_sha256_dgst(
                CNTX_IN_FIPS(cntx_info).thread_id_virt,
                THREAD_ID_BYTES_SIZE,
                (uint32_t)CNTX_IN_FIPS(cntx_info).kad,
                WTM_KEY_AUTH_DATA_SZ,
                CNTX_IN_FIPS(cntx_info).dgst_virt);
        MV_WTM_FUNC_ASSERT(0 == ret);

        /* Load "key + cntx" from a cache slot to WTM engine. */
        MV_WTM_FUNC_SET_3_PARAMS(
                s->sch, CACHE_IN_FIPS(cntx_info).cache_slot_id,
                CNTX_IN_FIPS(cntx_info).dgst_phys);
        MV_WTM_FUNC_SET_CMD(WTM_LOAD_ENGINE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;

        CACHE_IN_FIPS(cntx_info).cache_act = mv_wtm_func_get_os_time();

#endif /* CONFIG_EN_FIPS */
    }
}

void mv_wtm_func_store_cntx(mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx)
{
    mv_wtm_func_cntx_info *cntx_info = (mv_wtm_func_cntx_info *)cntx;

    MV_WTM_FUNC_ASSERT(cntx);

    if (WTM_NONFIPS_MODE == s->mode) {
        /* batch the following two cmds */
        MV_WTM_FUNC_SET_1_PARAM(2);
        MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
        MV_WTM_FUNC_CHK_BAT_IRQ;

        MV_WTM_FUNC_SET_2_PARAMS(
                s->sch, CNTX_IN_NONFIPS(cntx_info).cntx_phys);
        MV_WTM_FUNC_SET_CMD(WTM_STORE_ENGINE_CONTEXT_EXTERNAL);
        MV_WTM_FUNC_CHK_BAT_IRQ;

        MV_WTM_FUNC_SET_0_PARAM;
        MV_WTM_FUNC_SET_CMD(s->ps.zero);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
    } else {
#ifndef CONFIG_EN_FIPS

        MV_WTM_FUNC_ASSERT(0);

#else /* CONFIG_EN_FIPS */

        MV_WTM_FUNC_ASSERT(PLAIN_IN_CACHE ==
                CACHE_IN_FIPS(cntx_info).cache_stat);

        /* batch the following two cmds */
        MV_WTM_FUNC_SET_1_PARAM(2);
        MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
        MV_WTM_FUNC_CHK_BAT_IRQ;

        /* Store the content of WTM engine to cache. */
        MV_WTM_FUNC_SET_3_PARAMS(
                s->sch,
                CACHE_IN_FIPS(cntx_info).cache_slot_id,
                CNTX_IN_FIPS(cntx_info).thread_id_phys);
        MV_WTM_FUNC_SET_CMD(WTM_STORE_ENGINE_CONTEXT);
        MV_WTM_FUNC_CHK_BAT_IRQ;

        MV_WTM_FUNC_SET_0_PARAM;
        MV_WTM_FUNC_SET_CMD(s->ps.zero);
        MV_WTM_FUNC_WAIT_FOR_IRQ;

#endif /* CONFIG_EN_FIPS */
    }
}

#ifdef CONFIG_EN_LAZY_CNTX

void mv_wtm_func_load_lazy_cntx(mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx)
{
    mv_wtm_func_cntx_info *cntx_info = (mv_wtm_func_cntx_info *)cntx;

    if ((uint32_t)s == g_wtm_func_drv.last_s) {
        if (WTM_FIPS_MODE == s->mode) {
            MV_WTM_FUNC_ASSERT(
                    PLAIN_IN_CACHE == CACHE_IN_FIPS(cntx_info).cache_stat);
            /* update the cache activity */
            CACHE_IN_FIPS(cntx_info).cache_act = mv_wtm_func_get_os_time();
        }
    } else {
        if (g_wtm_func_drv.last_s) {
            /* it means the modes of last_s and s are the same */
            mv_wtm_func_store_cntx(
                    (mv_wtm_func_ss_priv *)g_wtm_func_drv.last_s,
                    ((mv_wtm_func_ss_priv *)g_wtm_func_drv.last_s)->cntx);
            g_wtm_func_drv.last_s = 0;
        }

        mv_wtm_func_load_cntx(s, cntx);
    }
}

void mv_wtm_func_store_lazy_cntx(mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx)
{
    MV_WTM_FUNC_ASSERT(!g_wtm_func_drv.last_s ||
            ((uint32_t)s == g_wtm_func_drv.last_s));
    g_wtm_func_drv.last_s = (uint32_t)s;
}

mv_wtm_func_cntx mv_wtm_func_create_lazy_cntx_oneshot(mv_wtm_func_ss_priv *s)
{
    MV_WTM_FUNC_ASSERT((uint32_t)s != g_wtm_func_drv.last_s);

    if (g_wtm_func_drv.last_s) {
        /* it means the modes of last_s and s are the same */
        mv_wtm_func_store_cntx(
                (mv_wtm_func_ss_priv *)g_wtm_func_drv.last_s,
                ((mv_wtm_func_ss_priv *)g_wtm_func_drv.last_s)->cntx);
        g_wtm_func_drv.last_s = 0;
    }

    /* fake cntx handle */
    return (mv_wtm_func_cntx)1;
}

void mv_wtm_func_destroy_lazy_cntx_oneshot(
        mv_wtm_func_ss_priv *s, mv_wtm_func_cntx cntx)
{
    MV_WTM_FUNC_ASSERT(!g_wtm_func_drv.last_s);
}

#endif /* CONFIG_EN_LAZY_CNTX */

uint32_t mv_wtm_func_get_freed_slt_num(void)
{
    uint32_t total_slt_num, freed_slt_num, cntx_sz;

    MV_WTM_FUNC_SET_1_PARAM(WTM_AES_CBC256); /* fake scheme */
    MV_WTM_FUNC_SET_CMD(WTM_GET_CONTEXT_INFO);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_3_RES(&total_slt_num, &freed_slt_num, &cntx_sz);

    return freed_slt_num;
}

bool mv_wtm_func_need_wrap_slt_out(void)
{
    uint32_t freed_slots = mv_wtm_func_get_freed_slt_num();
    MV_WTM_FUNC_ASSERT(freed_slots);
    return (freed_slots <= MIN_FREED_SLOT_NUM);
}

int32_t mv_wtm_func_wrap_slt_out(void)
{
    mv_wtm_func_list *node;
    uint32_t min_act = MAX_UINT32;
    struct _cntx_info_fips *ptr, *ci_to_wrap = NULL;
    uint32_t wrap_subk_cache_slot_id;
    int32_t ret;

    /* find the victim */
    mv_wtm_func_list_iter(&g_wtm_func_drv.fips_cntx_list, node) {
        ptr = mv_wtm_func_list_get_entry(node,
                struct _cntx_info_fips, fips_cntx_node);
        MV_WTM_FUNC_ASSERT(ptr);

        /*
         * cache_stat may be INVALID_CACHE_STAT
         * since the cur stat is invalid before save_usrk_info
        */
        MV_WTM_FUNC_ASSERT(WRAPPED_IN_PLACE != ptr->cache_info.cache_stat);

        if (PLAIN_IN_CACHE == ptr->cache_info.cache_stat) {
            if (min_act > CACHE_IN_FIPS(ptr).cache_act) {
                ci_to_wrap = ptr;
                min_act = CACHE_IN_FIPS(ptr).cache_act;
            }
        }
    }

    /* we assume the caller makes sure there are still some plain slot(s) */
    MV_WTM_FUNC_ASSERT(ci_to_wrap);

    /*
     * do NOT need to check cache vacancy
     * for wrap-subk's slot is reserved.
     */
    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256,
            g_wtm_func_drv.wrap_subk_wof_phys,
            (uint32_t)g_wtm_func_drv.wrap_subk_kad_virt,
            &wrap_subk_cache_slot_id);
    MV_WTM_FUNC_ASSERT(0 == ret);

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_2_PARAMS(
            WTM_SCH_TO_KEY_TYPE(WTM_AES_CBC256),
            wrap_subk_cache_slot_id);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_4_PARAMS(
            ci_to_wrap->cache_info.cache_sch,
            WTM_KEY_EXTERNAL,
            ci_to_wrap->cache_info.cache_slot_id,
            ci_to_wrap->cache_info.wrapper_mem_phys);
    MV_WTM_FUNC_SET_CMD(WTM_WRAP_KEYCONTEXT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_2_PARAMS(
            WTM_SCH_TO_KEY_TYPE(ci_to_wrap->cache_info.cache_sch),
            ci_to_wrap->cache_info.cache_slot_id);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    ci_to_wrap->cache_info.cache_stat = WRAPPED_OUTSIDE;
    ci_to_wrap->cache_info.cache_act = MAX_UINT32;
    ci_to_wrap->cache_info.cache_slot_id = WTM_INVALID_CACHE_SLOT_ID;

    return 0;
}

#ifdef CONFIG_EN_DBG

uint32_t mv_wtm_func_get_cntx_num_in_list(void)
{
    mv_wtm_func_list *node;
    uint32_t ret = 0;

    mv_wtm_func_list_iter(&g_wtm_func_drv.fips_cntx_list, node) {
        ret++;
    }

    return ret;
}

#endif /* CONFIG_EN_DBG */
