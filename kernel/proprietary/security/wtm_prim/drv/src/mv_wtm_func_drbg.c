/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_hash.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of hash logic in WTM function layer
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func_gen.h"
#include "mv_wtm_func_logic.h"
#include "mv_wtm_func_xllp.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t mv_wtm_func_drbg_init(mv_wtm_ss ss)
{
    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_DRBG_INIT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_drbg_init);

int32_t mv_wtm_func_drbg_reseed(mv_wtm_ss ss,
    uint32_t seed_phys, uint32_t seed_bit_sz)
{
    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SET_2_PARAMS(seed_phys, seed_bit_sz);
    MV_WTM_FUNC_SET_CMD(WTM_DRBG_RESEED);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_drbg_reseed);

int32_t mv_wtm_func_drbg_gen(mv_wtm_ss ss,
        uint32_t ran_phys, uint32_t ran_bit_sz)
{
    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SET_2_PARAMS(ran_bit_sz, ran_phys);
    MV_WTM_FUNC_SET_CMD(WTM_DRBG_GEN_RAN_BITS);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_drbg_gen);

