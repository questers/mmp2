/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_gen.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of generic framework in WTM function layer
 *
 */

#ifndef _MV_WTM_FUNC_GEN_H_
#define _MV_WTM_FUNC_GEN_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func_types.h"
#include "mv_wtm_func.h"

#include "mv_wtm_func_os_srv.h"
#include "mv_wtm_func_xllp.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#undef MV_WTM_DBG_DUMP_CMD

#if defined(CONFIG_EN_DBG) && defined(MV_WTM_DBG_DUMP_CMD)

#define MV_WTM_FUNC_SET_PARAMS(_p0, _p1, _p2, _p3,          \
                                _p4, _p5, _p6, _p7,         \
                                _p8, _p9, _p10, _p11,       \
                                _p12, _p13, _p14, _p15)     \
    do {                                                    \
        vuint32_t stat;                                     \
        do {                                                \
            stat = g_wtm_func_drv.regs->cmd_fifo_stat;      \
        } while ((stat &                                    \
                WTM_CMD_FIFO_NUM_MASK) >=                   \
                WTM_CMD_FIFO_MAX_NUM);                      \
        g_wtm_func_drv.regs->params[0] = (uint32_t)(_p0);   \
        g_wtm_func_drv.regs->params[1] = (uint32_t)(_p1);   \
        g_wtm_func_drv.regs->params[2] = (uint32_t)(_p2);   \
        g_wtm_func_drv.regs->params[3] = (uint32_t)(_p3);   \
        g_wtm_func_drv.regs->params[4] = (uint32_t)(_p4);   \
        g_wtm_func_drv.regs->params[5] = (uint32_t)(_p5);   \
        g_wtm_func_drv.regs->params[6] = (uint32_t)(_p6);   \
        g_wtm_func_drv.regs->params[7] = (uint32_t)(_p7);   \
        g_wtm_func_drv.regs->params[8] = (uint32_t)(_p8);   \
        g_wtm_func_drv.regs->params[9] = (uint32_t)(_p9);   \
        g_wtm_func_drv.regs->params[10] = (uint32_t)(_p10); \
        g_wtm_func_drv.regs->params[11] = (uint32_t)(_p11); \
        g_wtm_func_drv.regs->params[12] = (uint32_t)(_p12); \
        g_wtm_func_drv.regs->params[13] = (uint32_t)(_p13); \
        g_wtm_func_drv.regs->params[14] = (uint32_t)(_p14); \
        g_wtm_func_drv.regs->params[15] = (uint32_t)(_p15); \
        mv_wtm_func_pr("PARAM: 0x%08x 0x%08x 0x%08x 0x%08x\n"       \
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n"       \
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n"       \
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n",      \
                            (uint32_t)(_p0), (uint32_t)(_p1),       \
                            (uint32_t)(_p2), (uint32_t)(_p3),       \
                            (uint32_t)(_p4), (uint32_t)(_p5),       \
                            (uint32_t)(_p6), (uint32_t)(_p7),       \
                            (uint32_t)(_p8), (uint32_t)(_p9),       \
                            (uint32_t)(_p10), (uint32_t)(_p11),     \
                            (uint32_t)(_p12), (uint32_t)(_p13),     \
                            (uint32_t)(_p14), (uint32_t)(_p15));    \
    } while (0)
#else /* !CONFIG_EN_DBG || !MV_WTM_DBG_DUMP_CMD */

#define MV_WTM_FUNC_SET_PARAMS(_p0, _p1, _p2, _p3,          \
                                _p4, _p5, _p6, _p7,         \
                                _p8, _p9, _p10, _p11,       \
                                _p12, _p13, _p14, _p15)     \
    do {                                                    \
        vuint32_t stat;                                     \
        do {                                                \
            stat = g_wtm_func_drv.regs->cmd_fifo_stat;      \
        } while ((stat &                                    \
                WTM_CMD_FIFO_NUM_MASK) >=                   \
                WTM_CMD_FIFO_MAX_NUM);                      \
        g_wtm_func_drv.regs->params[0] = (uint32_t)(_p0);   \
        g_wtm_func_drv.regs->params[1] = (uint32_t)(_p1);   \
        g_wtm_func_drv.regs->params[2] = (uint32_t)(_p2);   \
        g_wtm_func_drv.regs->params[3] = (uint32_t)(_p3);   \
        g_wtm_func_drv.regs->params[4] = (uint32_t)(_p4);   \
        g_wtm_func_drv.regs->params[5] = (uint32_t)(_p5);   \
        g_wtm_func_drv.regs->params[6] = (uint32_t)(_p6);   \
        g_wtm_func_drv.regs->params[7] = (uint32_t)(_p7);   \
        g_wtm_func_drv.regs->params[8] = (uint32_t)(_p8);   \
        g_wtm_func_drv.regs->params[9] = (uint32_t)(_p9);   \
        g_wtm_func_drv.regs->params[10] = (uint32_t)(_p10); \
        g_wtm_func_drv.regs->params[11] = (uint32_t)(_p11); \
        g_wtm_func_drv.regs->params[12] = (uint32_t)(_p12); \
        g_wtm_func_drv.regs->params[13] = (uint32_t)(_p13); \
        g_wtm_func_drv.regs->params[14] = (uint32_t)(_p14); \
        g_wtm_func_drv.regs->params[15] = (uint32_t)(_p15); \
    } while (0)

#endif /* CONFIG_EN_DBG && MV_WTM_DBG_DUMP_CMD */

#define MV_WTM_FUNC_SET_0_PARAM \
        MV_WTM_FUNC_SET_PARAMS( \
                0, 0, 0, 0,     \
                0, 0, 0, 0,     \
                0, 0, 0, 0,     \
                0, 0, 0, 0)
#define MV_WTM_FUNC_SET_1_PARAM(    \
                _p0)                \
        MV_WTM_FUNC_SET_PARAMS(     \
                _p0, 0, 0, 0,       \
                0, 0, 0, 0,         \
                0, 0, 0, 0,         \
                0, 0, 0, 0)
#define MV_WTM_FUNC_SET_2_PARAMS(   \
                _p0, _p1)           \
        MV_WTM_FUNC_SET_PARAMS(     \
                _p0, _p1, 0, 0,     \
                0, 0, 0, 0,         \
                0, 0, 0, 0,         \
                0, 0, 0, 0)
#define MV_WTM_FUNC_SET_3_PARAMS(   \
                 _p0, _p1, _p2)     \
        MV_WTM_FUNC_SET_PARAMS(     \
                _p0, _p1, _p2, 0,   \
                0, 0, 0, 0,         \
                0, 0, 0, 0,         \
                0, 0, 0, 0)
#define MV_WTM_FUNC_SET_4_PARAMS(   \
                _p0, _p1, _p2, _p3) \
        MV_WTM_FUNC_SET_PARAMS(     \
                _p0, _p1, _p2, _p3, \
                0, 0, 0, 0,         \
                0, 0, 0, 0,         \
                0, 0, 0, 0)
#define MV_WTM_FUNC_SET_5_PARAMS(   \
                _p0, _p1, _p2, _p3, \
                _p4)                \
        MV_WTM_FUNC_SET_PARAMS(     \
                _p0, _p1, _p2, _p3, \
                _p4, 0, 0, 0,       \
                0, 0, 0, 0,         \
                0, 0, 0, 0)
#define MV_WTM_FUNC_SET_6_PARAMS(   \
                _p0, _p1, _p2, _p3, \
                _p4, _p5)           \
        MV_WTM_FUNC_SET_PARAMS(     \
                _p0, _p1, _p2, _p3, \
                _p4, _p5, 0, 0,     \
                0, 0, 0, 0,         \
                0, 0, 0, 0)
#define MV_WTM_FUNC_SET_7_PARAMS(   \
                _p0, _p1, _p2, _p3, \
                _p4, _p5, _p6)      \
        MV_WTM_FUNC_SET_PARAMS(     \
                _p0, _p1, _p2, _p3, \
                _p4, _p5, _p6, 0,   \
                0, 0, 0, 0,         \
                0, 0, 0, 0)
#define MV_WTM_FUNC_SET_8_PARAMS(   \
                _p0, _p1, _p2, _p3, \
                _p4, _p5, _p6, _p7) \
        MV_WTM_FUNC_SET_PARAMS(     \
                _p0, _p1, _p2, _p3, \
                _p4, _p5, _p6, _p7, \
                0, 0, 0, 0,         \
                0, 0, 0, 0)

#if defined(CONFIG_EN_DBG) && defined(MV_WTM_DBG_DUMP_CMD)

#define MV_WTM_FUNC_SET_CMD(_cmd)                               \
    do {                                                        \
        g_wtm_func_drv.irq_stat = 0;                            \
        g_wtm_func_drv.regs->cmd = (_cmd);                      \
        mv_wtm_func_pr("CMD  : 0x%08x\n", (uint32_t)(_cmd));    \
    } while (0)

#else /* !CONFIG_EN_DBG || !MV_WTM_DBG_DUMP_CMD */

#define MV_WTM_FUNC_SET_CMD(_cmd)           \
    do {                                    \
        g_wtm_func_drv.irq_stat = 0;        \
        g_wtm_func_drv.regs->cmd = (_cmd);  \
    } while (0)

#endif /* CONFIG_EN_DBG && MV_WTM_DBG_DUMP_CMD */

#ifndef CONFIG_EN_DBG

#define MV_WTM_FUNC_WAIT_FOR_IRQ                            \
    do {                                                    \
        (void)mv_wtm_func_wait_for_event(                   \
                g_wtm_func_drv.irq_done, INFINITE);         \
    } while (0)

#else

#ifndef MV_WTM_DBG_DUMP_CMD

#define MV_WTM_FUNC_WAIT_FOR_IRQ                            \
    do {                                                    \
        (void)mv_wtm_func_wait_for_event(                   \
                g_wtm_func_drv.irq_done, INFINITE);         \
        if (0 != g_wtm_func_drv.irq_stat) {                 \
            mv_wtm_func_pr("\n");                           \
            mv_wtm_func_pr("ERROR - err = %d [0x%08x]\n",   \
                    g_wtm_func_drv.irq_stat,                \
                    g_wtm_func_drv.irq_stat);               \
            MV_WTM_FUNC_ASSERT(0);                          \
        }                                                   \
    } while (0)

#else /* MV_WTM_DBG_DUMP_CMD */

#define MV_WTM_FUNC_WAIT_FOR_IRQ                                \
    do {                                                        \
        (void)mv_wtm_func_wait_for_event(                       \
                g_wtm_func_drv.irq_done, INFINITE);             \
        mv_wtm_func_pr("STAT : 0x%08x 0x%08x 0x%08x 0x%08x\n"   \
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n"   \
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n"   \
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n",  \
                            g_wtm_func_drv.regs->cmd_stat[ 0],  \
                            g_wtm_func_drv.regs->cmd_stat[ 1],  \
                            g_wtm_func_drv.regs->cmd_stat[ 2],  \
                            g_wtm_func_drv.regs->cmd_stat[ 3],  \
                            g_wtm_func_drv.regs->cmd_stat[ 4],  \
                            g_wtm_func_drv.regs->cmd_stat[ 5],  \
                            g_wtm_func_drv.regs->cmd_stat[ 6],  \
                            g_wtm_func_drv.regs->cmd_stat[ 7],  \
                            g_wtm_func_drv.regs->cmd_stat[ 8],  \
                            g_wtm_func_drv.regs->cmd_stat[ 9],  \
                            g_wtm_func_drv.regs->cmd_stat[10],  \
                            g_wtm_func_drv.regs->cmd_stat[11],  \
                            g_wtm_func_drv.regs->cmd_stat[12],  \
                            g_wtm_func_drv.regs->cmd_stat[13],  \
                            g_wtm_func_drv.regs->cmd_stat[14],  \
                            g_wtm_func_drv.regs->cmd_stat[15]); \
        if (0 != g_wtm_func_drv.irq_stat) {                     \
            mv_wtm_func_pr("\n");                               \
            mv_wtm_func_pr("ERROR - err = %d [0x%08x]\n",       \
                    g_wtm_func_drv.irq_stat,                    \
                    g_wtm_func_drv.irq_stat);                   \
        }                                                       \
        mv_wtm_func_pr("--------------------------"             \
                       "------------------------\n");           \
        if (0 != g_wtm_func_drv.irq_stat) {                     \
            MV_WTM_FUNC_ASSERT(0);                              \
        }                                                       \
    } while (0)

#endif /* MV_WTM_DBG_DUMP_CMD */

#endif /* CONFIG_EN_DBG */

#if !defined(CONFIG_EN_DBG) || !defined(MV_WTM_DBG_DUMP_CMD)

#define MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK                     \
    do {                                                    \
        (void)mv_wtm_func_wait_for_event(                   \
                g_wtm_func_drv.irq_done, INFINITE);         \
    } while (0)

#else

#define MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK                         \
    do {                                                        \
        (void)mv_wtm_func_wait_for_event(                       \
                g_wtm_func_drv.irq_done, INFINITE);             \
        mv_wtm_func_pr("STAT : 0x%08x 0x%08x 0x%08x 0x%08x\n"   \
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n"   \
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n"   \
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n",  \
                            g_wtm_func_drv.regs->cmd_stat[ 0],  \
                            g_wtm_func_drv.regs->cmd_stat[ 1],  \
                            g_wtm_func_drv.regs->cmd_stat[ 2],  \
                            g_wtm_func_drv.regs->cmd_stat[ 3],  \
                            g_wtm_func_drv.regs->cmd_stat[ 4],  \
                            g_wtm_func_drv.regs->cmd_stat[ 5],  \
                            g_wtm_func_drv.regs->cmd_stat[ 6],  \
                            g_wtm_func_drv.regs->cmd_stat[ 7],  \
                            g_wtm_func_drv.regs->cmd_stat[ 8],  \
                            g_wtm_func_drv.regs->cmd_stat[ 9],  \
                            g_wtm_func_drv.regs->cmd_stat[10],  \
                            g_wtm_func_drv.regs->cmd_stat[11],  \
                            g_wtm_func_drv.regs->cmd_stat[12],  \
                            g_wtm_func_drv.regs->cmd_stat[13],  \
                            g_wtm_func_drv.regs->cmd_stat[14],  \
                            g_wtm_func_drv.regs->cmd_stat[15]); \
        mv_wtm_func_pr("--------------------------"             \
                       "------------------------\n");           \
    } while (0)

#endif

#define MV_WTM_FUNC_GET_CMD_STAT    (g_wtm_func_drv.irq_stat)

#ifndef CONFIG_EN_DBG

#define MV_WTM_FUNC_CHK_BAT_IRQ

#else

#define MV_WTM_FUNC_CHK_BAT_IRQ                                         \
    do {                                                                \
        int32_t ret;                                                    \
        ret = mv_wtm_func_wait_for_event(g_wtm_func_drv.irq_done, 0);   \
        if (0 == ret) {                                                 \
            mv_wtm_func_pr("\n");                                       \
            mv_wtm_func_pr("ERROR - err = %d [0x%08x]\n",               \
                    g_wtm_func_drv.regs->cmd_ret_stat,                  \
                    g_wtm_func_drv.regs->cmd_ret_stat);                 \
            MV_WTM_FUNC_ASSERT(0);                                      \
        }                                                               \
    } while (0)

#endif /* CONFIG_EN_DBG */

#define MV_WTM_FUNC_READ_1_RES(_p0)                                 \
    do {                                                            \
        *((uint32_t *)(_p0)) = g_wtm_func_drv.regs->cmd_stat[0];    \
    } while (0)
#define MV_WTM_FUNC_READ_2_RES(_p0, _p1)                            \
    do {                                                            \
        *((uint32_t *)(_p0)) = g_wtm_func_drv.regs->cmd_stat[0];    \
        *((uint32_t *)(_p1)) = g_wtm_func_drv.regs->cmd_stat[1];    \
    } while (0)
#define MV_WTM_FUNC_READ_3_RES(_p0, _p1, _p2)                       \
    do {                                                            \
        *((uint32_t *)(_p0)) = g_wtm_func_drv.regs->cmd_stat[0];    \
        *((uint32_t *)(_p1)) = g_wtm_func_drv.regs->cmd_stat[1];    \
        *((uint32_t *)(_p2)) = g_wtm_func_drv.regs->cmd_stat[2];    \
    } while (0)
#define MV_WTM_FUNC_READ_4_RES(_p0, _p1, _p2, _p3)                  \
    do {                                                            \
        *((uint32_t *)(_p0)) = g_wtm_func_drv.regs->cmd_stat[0];    \
        *((uint32_t *)(_p1)) = g_wtm_func_drv.regs->cmd_stat[1];    \
        *((uint32_t *)(_p2)) = g_wtm_func_drv.regs->cmd_stat[2];    \
        *((uint32_t *)(_p3)) = g_wtm_func_drv.regs->cmd_stat[3];    \
    } while (0)

/*
 ******************************
 *          TYPES
 ******************************
 */

typedef struct _mv_wtm_func_regs
{
    vuint32_t params[16];
    vuint32_t cmd;
    vuint32_t rsvd[15];
    vuint32_t cmd_ret_stat;
    vuint32_t cmd_stat[16];
    vuint32_t cmd_fifo_stat;
    vuint32_t host_irq_stat;
    vuint32_t host_irq_mask;
    vuint32_t host_excp_addr;
    vuint32_t sp_trust;
    vuint32_t wtm_id;
    vuint32_t wtm_ver;
    vuint32_t cntx_stat;
} mv_wtm_func_regs;

typedef struct _mv_wtm_func_drv
{
    uint32_t            used_num;
    mv_wtm_func_regs    *regs;
    uint32_t            mode;
    mv_wtm_func_list    fips_cntx_list;
    mv_wtm_func_irq     irq;
    mv_wtm_func_event   irq_done;
    int32_t             irq_stat;
#ifdef CONFIG_EN_MULTI_THRD
    mv_wtm_func_mutex   lock;
#endif
#ifdef CONFIG_EN_FIPS
    uint32_t            wrap_subk_wof_virt;
    uint32_t            wrap_subk_wof_phys;
    uint32_t            wrap_subk_wof_sz;
    uint8_t             wrap_subk_kad_virt[WTM_KEY_AUTH_DATA_SZ];
    bool                is_wrap_subk_set;
#endif
#ifdef CONFIG_EN_DBG
    uint32_t            freed_cache_slots;
#endif
#ifdef CONFIG_EN_LAZY_CNTX
    uint32_t            last_s;
#endif
} mv_wtm_func_drv;

typedef struct _mv_wtm_rsvd
{
    bool        resumed; /* valid in aes */
    uint32_t    *slot_id;
} mv_wtm_rsvd;

typedef struct _mv_wtm_pi_set
{
    uint32_t init;
    uint32_t zero;
    uint32_t proc;
    uint32_t fini;
    uint32_t all;
} mv_wtm_pi_set;

typedef struct _mv_wtm_func_ss_priv
{
    uint32_t        sch;
    uint32_t        func; /* valid in sym and pkcs */
    uint32_t        mode;
    void            *cntx;
    mv_wtm_rsvd     rsvd;
    mv_wtm_pi_set   ps;
} mv_wtm_func_ss_priv;

/*
 ******************************
 *          VARIABLES
 ******************************
 */

extern mv_wtm_func_drv g_wtm_func_drv;

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#endif /* _MV_WTM_FUNC_GEN_H_ */
