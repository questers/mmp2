/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_os_srv.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The internal source file of WTM function layer definition
 *                for Linux kernel
 *                This file is os-specific
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */
#include "mv_wtm_func_os_srv.h"
#include "mv_wtm_func_logic.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#ifdef CONFIG_EN_WTM_PROC
#define WTM_FUNC_PROC_WTM  "wtm"
#define WTM_FUNC_PROC_INFO "info"
#define WTM_FUNC_PROC_REGS "regs"

#define WTM_FUNC_PROC_SEND_CMD "send_cmd"
#define WTM_FUNC_PROC_PARAM "p%d"
#define WTM_FUNC_PROC_CMD "cmd"
#endif

/*
 ******************************
 *          TYPES
 ******************************
 */

typedef struct __phys_mem_info
{
    void        *alloc_virt_addr;
    void        *alloc_phys_addr;
    uint32_t    alloc_size;
} _phys_mem_info;

typedef struct __irq_info
{
    uint32_t    irq;
    int32_t     (*isr)(void *);
    void        *arg;
} _irq_info;

/*
 ******************************
 *          VARIABLES
 ******************************
 */

#ifdef CONFIG_EN_WTM_PROC
static mv_wtm_func_proc _g_wtm_func_proc;
static uint32_t _g_wtm_func_proc_params[16] = {0};
#endif

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

uint32_t mv_wtm_func_pr(const void *format, ...)
{
    uint32_t ret = 0;

#ifdef CONFIG_EN_DBG

    va_list args;

    va_start(args, format);
    ret = vprintk(format, args);
    va_end(args);

#endif

    return ret;
}

static irqreturn_t _isr_shell(int32_t irq, void *arg)
{
    _irq_info *_irq = (_irq_info *)arg;

    if (0 == _irq->isr(_irq->arg)) {
        return IRQ_HANDLED;
    } else {
        return IRQ_NONE;
    }
}

mv_wtm_func_irq mv_wtm_func_request_irq(
                    int32_t irq, uint32_t flags,
                    int32_t (*isr)(void *), void *arg)
{
    _irq_info *_irq;
    int32_t ret;

    MV_WTM_FUNC_ASSERT(isr);

    _irq = kzalloc(sizeof(_irq_info), GFP_KERNEL);
    if (NULL == _irq) {
        return NULL;
    }

    _irq->irq = irq;
    _irq->isr = isr;
    _irq->arg = arg;

    ret = request_irq(irq, _isr_shell, flags, "mv_wtm_func", (void *)_irq);
    if (ret) {
        kfree(_irq);
        return NULL;
    }

    return (mv_wtm_func_irq)_irq;
}

void mv_wtm_func_free_irq(mv_wtm_func_irq irq)
{
    _irq_info *_irq = (_irq_info *)irq;

    free_irq(_irq->irq, (void *)_irq);
    kfree(_irq);
}

static uint32_t _mv_wtm_func_pg_tab_walk(struct mm_struct *mm, uint32_t virt)
{
    pgd_t *pgd;
    pmd_t *pmd;
    pte_t *pte;

    ulong_t pfn;
#ifdef pte_offset_map_lock
    spinlock_t *ptl;
#endif
    uint32_t ret = MV_WTM_FUNC_INVALID_PHYS_ADDR;

    down_read(&mm->mmap_sem);

    pgd = pgd_offset(mm, (ulong_t)virt);
    if (!pgd_present(*pgd)) {
        goto _no_pgd;
    }

    pmd = pmd_offset(pgd, (ulong_t)virt);
    if (!pmd_present(*pmd)) {
        goto _no_pmd;
    }

#ifdef pte_offset_map_lock
    pte = pte_offset_map_lock(mm, pmd, (ulong_t)virt, &ptl);
#else
    pte = pte_offset_map(pmd, (ulong_t)virt);
#endif
    if (!pte_present(*pte)) {
        goto _no_pte;
    }

    pfn = pte_pfn(*pte);

    ret = (pfn << PAGE_SHIFT)
            + ((ulong_t)virt & (~PAGE_MASK));
_no_pte:
#ifdef pte_offset_map_lock
    pte_unmap_unlock(pte, ptl);
#endif

_no_pmd:
_no_pgd:
    up_read(&mm->mmap_sem);

    return ret;
}

uint32_t mv_wtm_func_virt_to_phys(uint32_t virt)
{
    if (virt < PAGE_OFFSET) {
        return _mv_wtm_func_pg_tab_walk(current->mm, virt);
    } else if (virt < (uint32_t)high_memory) {
        return virt_to_phys((void *)virt);
    } else {
        MV_WTM_FUNC_ASSERT(0);
        return _mv_wtm_func_pg_tab_walk(init_task.active_mm, virt);
    }
}

void *mv_wtm_func_alloc_dma_mem(
                    uint32_t size, uint32_t algn, void **phys)
{
    uint32_t mem_blk_size;
    void *alloc_virt_addr;
    void *alloc_phys_addr;
    void *ret_virt_addr;
    _phys_mem_info *rec_virt_addr;

    if (!size || !phys) {
        return NULL;
    }

    mem_blk_size = size + sizeof(_phys_mem_info) + algn;

    alloc_virt_addr = dma_alloc_coherent(
                NULL, mem_blk_size, (dma_addr_t *)&alloc_phys_addr, GFP_KERNEL);
    if (NULL == alloc_virt_addr) {
        *phys = NULL;
        return NULL;
    }

    /* calculate the virtual address returned */
    ret_virt_addr = (void *)(((uint32_t)
                alloc_virt_addr + algn - 1) & (~(algn - 1)));
    do {
        if (ret_virt_addr - alloc_virt_addr
             >= sizeof(_phys_mem_info)) {
            rec_virt_addr =
                (_phys_mem_info *)((uint32_t)
                        ret_virt_addr - sizeof(_phys_mem_info));
            break;
        } else {
            ret_virt_addr = (void *)(
                    (uint32_t)ret_virt_addr + algn);
        }
    } while (1);

    MV_WTM_FUNC_ASSERT((uint32_t)rec_virt_addr >= (uint32_t)alloc_virt_addr);
    MV_WTM_FUNC_ASSERT((uint32_t)ret_virt_addr + size
                            <= (uint32_t)alloc_virt_addr + mem_blk_size);

    rec_virt_addr->alloc_virt_addr = alloc_virt_addr;
    rec_virt_addr->alloc_phys_addr = alloc_phys_addr;
    rec_virt_addr->alloc_size = mem_blk_size;

    *phys = (void *)((uint32_t)alloc_phys_addr +
            ((uint32_t)ret_virt_addr - (uint32_t)alloc_virt_addr));

    return ret_virt_addr;
}

void mv_wtm_func_free_dma_mem(void *virt)
{
    _phys_mem_info *rec_virt_addr;

    MV_WTM_FUNC_ASSERT(virt);

    rec_virt_addr = (_phys_mem_info *)(
            (uint32_t)virt - sizeof(_phys_mem_info));

    dma_free_coherent(
                NULL, rec_virt_addr->alloc_size,
                rec_virt_addr->alloc_virt_addr,
                (dma_addr_t)rec_virt_addr->alloc_phys_addr);
}

#ifdef CONFIG_EN_WTM_PROC
static int32_t _mv_wtm_func_proc_info_read(int8_t *page, int8_t **start, off_t off,
                          int32_t count, int32_t *eof, void *data)
{
    uint32_t length = 0;
    uint32_t tsr;
    uint32_t lc;
    uint32_t ver;
    char *lc_name[4] = {"CM", "DM", "DD", "FA"};

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_GET_TRUST_STATUS_REGISTER);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(&tsr);

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_LIFECYCLE_READ);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(&lc);

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_KERNEL_VERSION_READ);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(&ver);

    MV_WTM_FUNC_UNLOCK;

    length = sprintf(page, "Board Information\n");
    length += sprintf(page + length, "Life-cycle : %s\n", (lc <= 3) ? lc_name[lc] : "ERROR");
    length += sprintf(page + length, "TSR        : 0x%08x\n", tsr);
    length += sprintf(page + length, "WTM version: 0x%08x\n", ver);
    return length;
}

static ssize_t _mv_wtm_func_proc_regs_read(int8_t *page, int8_t **start, off_t off,
                          int32_t count, int32_t *eof, void *data)
{
    uint32_t offset = 0;
    uint32_t length = 0;

    mv_wtm_func_drv *drv = &g_wtm_func_drv;

    length = sprintf(page, "wtm registers:\n");
    length += sprintf(page + length, "cmd_ret_stat : 0x%08x\n", drv->regs->cmd_ret_stat);

    offset = 0;
    length += sprintf(page + length, "cmd_stat     : 0x%08x, 0x%08x, 0x%08x, 0x%08x,\n",
            drv->regs->cmd_stat[0], drv->regs->cmd_stat[1],
            drv->regs->cmd_stat[2], drv->regs->cmd_stat[3]);
    length += sprintf(page + length, "               0x%08x, 0x%08x, 0x%08x, 0x%08x,\n",
            drv->regs->cmd_stat[4], drv->regs->cmd_stat[5],
            drv->regs->cmd_stat[6], drv->regs->cmd_stat[7]);
    length += sprintf(page + length, "               0x%08x, 0x%08x, 0x%08x, 0x%08x,\n",
            drv->regs->cmd_stat[8], drv->regs->cmd_stat[9],
            drv->regs->cmd_stat[10], drv->regs->cmd_stat[11]);
    length += sprintf(page + length, "               0x%08x, 0x%08x, 0x%08x, 0x%08x,\n",
            drv->regs->cmd_stat[12], drv->regs->cmd_stat[13],
            drv->regs->cmd_stat[14], drv->regs->cmd_stat[15]);

    length += sprintf(page + length, "cmd_fifo_stat: 0x%08x\n", drv->regs->cmd_fifo_stat);
    length += sprintf(page + length, "host_irq_stat: 0x%08x\n", drv->regs->host_irq_stat);
    length += sprintf(page + length, "host_irq_mask: 0x%08x\n", drv->regs->host_irq_mask);
    length += sprintf(page + length, "host_exc_addr: 0x%08x\n", drv->regs->host_excp_addr);
    length += sprintf(page + length, "sp_trust     : 0x%08x\n", drv->regs->sp_trust);
    length += sprintf(page + length, "wtm_id       : 0x%08x\n", drv->regs->wtm_id);
    length += sprintf(page + length, "wtm_ver      : 0x%08x\n", drv->regs->wtm_ver);
    length += sprintf(page + length, "cntx_stat    : 0x%08x\n", drv->regs->cntx_stat);
    return length;
}

static uint32_t _mv_wtm_func_proc_write (struct file *file, const char *buffer,
                              unsigned long count, void *data)
{
    int8_t kbuf[12];
    uint32_t val;

    if (count > 12)
        return -EINVAL;
    if (copy_from_user(kbuf, buffer, count))
        return -EFAULT;
    val = (uint32_t) simple_strtoul(kbuf, NULL, 0);

    if ((uint32_t)data < 16) {

        _g_wtm_func_proc_params[(uint32_t)data] = val;

    } else if ((uint32_t)data == 16){

        mv_wtm_func_pr("PARAM: 0x%08x 0x%08x 0x%08x 0x%08x\n" 
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n" 
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n" 
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n",
                            _g_wtm_func_proc_params[0],
                            _g_wtm_func_proc_params[1],
                            _g_wtm_func_proc_params[2],
                            _g_wtm_func_proc_params[3],
                            _g_wtm_func_proc_params[4],
                            _g_wtm_func_proc_params[5],
                            _g_wtm_func_proc_params[6],
                            _g_wtm_func_proc_params[7],
                            _g_wtm_func_proc_params[8],
                            _g_wtm_func_proc_params[9],
                            _g_wtm_func_proc_params[10],
                            _g_wtm_func_proc_params[11],
                            _g_wtm_func_proc_params[12],
                            _g_wtm_func_proc_params[13],
                            _g_wtm_func_proc_params[14],
                            _g_wtm_func_proc_params[15]);

        mv_wtm_func_pr("CMD  : 0x%08x\n", val);

        MV_WTM_FUNC_LOCK;

        MV_WTM_FUNC_SET_PARAMS(_g_wtm_func_proc_params[0],
                            _g_wtm_func_proc_params[1],
                            _g_wtm_func_proc_params[2],
                            _g_wtm_func_proc_params[3],
                            _g_wtm_func_proc_params[4],
                            _g_wtm_func_proc_params[5],
                            _g_wtm_func_proc_params[6],
                            _g_wtm_func_proc_params[7],
                            _g_wtm_func_proc_params[8],
                            _g_wtm_func_proc_params[9],
                            _g_wtm_func_proc_params[10],
                            _g_wtm_func_proc_params[11],
                            _g_wtm_func_proc_params[12],
                            _g_wtm_func_proc_params[13],
                            _g_wtm_func_proc_params[14],
                            _g_wtm_func_proc_params[15]);
        MV_WTM_FUNC_SET_CMD(val);
        MV_WTM_FUNC_WAIT_FOR_IRQ;

        MV_WTM_FUNC_UNLOCK;

        mv_wtm_func_pr("STAT : 0x%08x 0x%08x 0x%08x 0x%08x\n" 
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n" 
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n" 
                       "       0x%08x 0x%08x 0x%08x 0x%08x\n",
                            g_wtm_func_drv.regs->cmd_stat[ 0],
                            g_wtm_func_drv.regs->cmd_stat[ 1],
                            g_wtm_func_drv.regs->cmd_stat[ 2],
                            g_wtm_func_drv.regs->cmd_stat[ 3],
                            g_wtm_func_drv.regs->cmd_stat[ 4],
                            g_wtm_func_drv.regs->cmd_stat[ 5],
                            g_wtm_func_drv.regs->cmd_stat[ 6],
                            g_wtm_func_drv.regs->cmd_stat[ 7],
                            g_wtm_func_drv.regs->cmd_stat[ 8],
                            g_wtm_func_drv.regs->cmd_stat[ 9],
                            g_wtm_func_drv.regs->cmd_stat[10],
                            g_wtm_func_drv.regs->cmd_stat[11],
                            g_wtm_func_drv.regs->cmd_stat[12],
                            g_wtm_func_drv.regs->cmd_stat[13],
                            g_wtm_func_drv.regs->cmd_stat[14],
                            g_wtm_func_drv.regs->cmd_stat[15]);
        memset((void *)_g_wtm_func_proc_params, 0, 64);
    }
    return count;
}
#endif

static int32_t __init _wtm_func_init(void)
{
#ifdef CONFIG_EN_WTM_PROC
    uint32_t i;
#endif

    mv_wtm_func_pr("INFO - WTPsp prim version is %d.%d.%d\n", MAJOR_VERSION(PRIM_LAYER_VERSION),
            MINOR_VERSION(PRIM_LAYER_VERSION), LAST_VERSION(PRIM_LAYER_VERSION));

#ifdef CONFIG_EN_WTM_PROC
    _g_wtm_func_proc.wtm_entry = proc_mkdir(WTM_FUNC_PROC_WTM, NULL);
    if (!_g_wtm_func_proc.wtm_entry) {
        mv_wtm_func_pr("ERROR - Fail to create procfs\n");
        return -ENOMEM;
    }
    _g_wtm_func_proc.info_entry =
        create_proc_entry(WTM_FUNC_PROC_INFO, 0444, _g_wtm_func_proc.wtm_entry);
    if (_g_wtm_func_proc.info_entry) {
        _g_wtm_func_proc.info_entry->read_proc = (read_proc_t *)_mv_wtm_func_proc_info_read;
    }

    _g_wtm_func_proc.regs_entry =
        create_proc_entry(WTM_FUNC_PROC_REGS, 0444, _g_wtm_func_proc.wtm_entry);
    if (_g_wtm_func_proc.regs_entry) {
        _g_wtm_func_proc.regs_entry->read_proc = (read_proc_t *)_mv_wtm_func_proc_regs_read;
    }

    _g_wtm_func_proc.send_cmd_entry =
        proc_mkdir(WTM_FUNC_PROC_SEND_CMD, _g_wtm_func_proc.wtm_entry);

    if (!_g_wtm_func_proc.send_cmd_entry) {
        mv_wtm_func_pr("ERROR - Fail to create procfs\n");
        return -ENOMEM;
    }
    for (i = 0; i < 16; i++) {
        int8_t param_name[3];
        sprintf(param_name, WTM_FUNC_PROC_PARAM, i);
        _g_wtm_func_proc.param_entry[i] =
            create_proc_entry(param_name, 0222, _g_wtm_func_proc.send_cmd_entry);
        _g_wtm_func_proc.param_entry[i]->data = (void *)i;
        _g_wtm_func_proc.param_entry[i]->write_proc = (write_proc_t *)_mv_wtm_func_proc_write;
    }
    _g_wtm_func_proc.cmd_entry =
        create_proc_entry(WTM_FUNC_PROC_CMD, 0222, _g_wtm_func_proc.send_cmd_entry);
        _g_wtm_func_proc.cmd_entry->data = (void *)i;
        _g_wtm_func_proc.cmd_entry->write_proc = (write_proc_t *)_mv_wtm_func_proc_write;
#endif

    return 0;
}

static void __exit _wtm_func_cleanup(void)
{
    mv_wtm_func_pr("INFO - deinit wtm prim\n");

#ifdef CONFIG_EN_WTM_PROC
    if (_g_wtm_func_proc.wtm_entry) {
        remove_proc_entry(WTM_FUNC_PROC_INFO, _g_wtm_func_proc.wtm_entry);
        remove_proc_entry(WTM_FUNC_PROC_REGS, _g_wtm_func_proc.wtm_entry);
        if (_g_wtm_func_proc.send_cmd_entry) {
            uint32_t i;
            remove_proc_entry(WTM_FUNC_PROC_CMD, _g_wtm_func_proc.send_cmd_entry);
            for (i = 0; i < 16; i++) {
                int8_t param_name[3];
                sprintf(param_name, WTM_FUNC_PROC_PARAM, i);
                remove_proc_entry(param_name, _g_wtm_func_proc.send_cmd_entry);
            }
            remove_proc_entry(WTM_FUNC_PROC_SEND_CMD, _g_wtm_func_proc.wtm_entry);
        }
        remove_proc_entry(WTM_FUNC_PROC_WTM, NULL);

    }
#endif
}

module_init(_wtm_func_init);
module_exit(_wtm_func_cleanup);
