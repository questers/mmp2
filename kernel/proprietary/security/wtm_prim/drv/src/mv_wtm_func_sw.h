/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_sw.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of software crypto in WTM function layer
 *                This file is os-specific
 *
 */

#ifndef _MV_WTM_FUNC_SW_H_
#define _MV_WTM_FUNC_SW_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func.h"

#include "mv_wtm_func_gcc.h"
#include "mv_wtm_func_os_srv.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#ifdef CONFIG_EN_FIPS

extern int32_t mv_wtm_func_sha256_dgst(
        uint32_t src_virt, uint32_t src_len, uint32_t res);
extern int32_t mv_wtm_func_hmac_sha256_dgst(
        uint32_t src_virt, uint32_t src_len,
        uint32_t key_virt, uint32_t key_len,
        uint32_t res);
__INLINE static uint32_t mv_wtm_func_rand(void)
{
    /* FIXME */
    return (0x5A5A5A5A);
}

#endif /* CONFIG_EN_FIPS */

#endif /* _MV_WTM_FUNC_SW_H_ */
