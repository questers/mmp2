/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : .h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of state management
 *                in WTM function layer.
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func_gen.h"
#include "mv_wtm_func_logic.h"
#include "mv_wtm_func_xllp.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t mv_wtm_func_get_tsr(mv_wtm_ss ss, uint32_t *tsr)
{
    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_GET_TRUST_STATUS_REGISTER);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(tsr);

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_get_tsr);

int32_t mv_wtm_func_life_cycle_adv(mv_wtm_ss ss)
{
    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_LIFECYCLE_ADVANCE);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_life_cycle_adv);

int32_t mv_wtm_func_life_cycle_read(mv_wtm_ss ss, uint32_t *life_cycle)
{
    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_LIFECYCLE_READ);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(life_cycle);

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_life_cycle_read);

int32_t mv_wtm_func_soft_ver_adv(mv_wtm_ss ss)
{
    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_SOFTWARE_VERSION_ADVANCE);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_soft_ver_adv);

int32_t mv_wtm_func_soft_ver_read(mv_wtm_ss ss, uint32_t *ver)
{
    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_SOFTWARE_VERSION_READ);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(ver);

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_soft_ver_read);

int32_t mv_wtm_func_kernel_ver_read(mv_wtm_ss ss, uint32_t *ver)
{
    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_KERNEL_VERSION_READ);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(ver);

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_kernel_ver_read);

