/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_types.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of type definitions in WTM function layer
 *                This file is os-specific
 *
 */

#ifndef _MV_WTM_FUNC_TYPES_H_
#define _MV_WTM_FUNC_TYPES_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_types.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#ifndef true
#define true    (1)
#endif

#ifndef false
#define false   (0)
#endif

/*
 ******************************
 *          TYPES
 ******************************
 */

typedef signed long         long_t;
typedef unsigned long       ulong_t;

typedef volatile int8_t     vint8_t;
typedef volatile uint8_t    vuint8_t;
typedef volatile int16_t    vint16_t;
typedef volatile uint16_t   vuint16_t;
typedef volatile int32_t    vint32_t;
typedef volatile uint32_t   vuint32_t;
typedef volatile long_t     vlong_t;
typedef volatile ulong_t    vulong_t;

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#endif /* _MV_WTM_FUNC_TYPES_H_ */
