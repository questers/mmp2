/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_km.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of key management in WTM function layer
 *
 */

/* OPT: THIS FILE NEEDS FURTHER OPTIMIZATION */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func_gen.h"
#include "mv_wtm_func_logic.h"
#include "mv_wtm_func_km.h"
#include "mv_wtm_func_sw.h"
#include "mv_wtm_func_xllp.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/* WARNING: search __ALIGNED to fix all the values of this macro */
#define _OVERALL_NONCE_SZ_ALGN  (64)
/* WARNING: search __ALIGNED to fix all the values of this macro */
#define _OIAP_TAG_SZ_ALGN       (32)
/* WARNING: search __ALIGNED to fix all the values of this macro */
#define _KAD_SZ_ALGN            (32)
/* WARNING: search __ALIGNED to fix all the values of this macro */
#define _THRD_ID_ALGN           (16)
/* WARNING: search __ALIGNED to fix all the values of this macro */
#define _THRD_ID_DGST_ALGN      (32)

#define _SYM_OP_ENC             (0)
#define _SYM_OP_DEC             (1)

/*
 * this is a temporary workaround for updateloigc
 * three functions' lifecycle checkings are removed temporarily:
 * mv_wtm_func_create_subkey        - for creating keyring
 * mv_wtm_func_enroll_sym_usrkey    - for enrolling endorsement key
 * mv_wtm_func_create_sym_usrkey    - for creating sym user key
 */
#define __WORK_AROUND_FOR_UPDATE_LOGIC__

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t mv_wtm_func_create_ec_key_pair_nonfips(mv_wtm_ss ss,
        uint32_t ksch, uint32_t domain_phys, uint32_t mode,
        uint32_t prv_key_phys, uint32_t pub_key_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    s->sch = ksch;
    s->mode = WTM_NONFIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    MV_WTM_FUNC_SET_5_PARAMS(ksch, domain_phys, mode,
            prv_key_phys, pub_key_phys);
    MV_WTM_FUNC_SET_CMD(WTM_ECCP_KEYPAIR_GEN);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_ec_key_pair_nonfips);

int32_t mv_wtm_func_derivate_ec_pub_key_nonfips(mv_wtm_ss ss,
        uint32_t ksch, uint32_t domain_phys, uint32_t mode,
        uint32_t prv_key_phys, uint32_t pub_key_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    s->sch = ksch;
    s->mode = WTM_NONFIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    MV_WTM_FUNC_SET_5_PARAMS(ksch, domain_phys, mode,
            prv_key_phys, pub_key_phys);
    MV_WTM_FUNC_SET_CMD(WTM_ECCP_PUBLIC_KEY_DERIVATION);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_derivate_ec_pub_key_nonfips);

int32_t mv_wtm_func_create_ec_dh_shared_key_nonfips(mv_wtm_ss ss,
        uint32_t ksch,
        uint32_t domain_phys, uint32_t mode,
        uint32_t prv_key_phys, uint32_t peer_pub_key_phys,
        uint32_t shrd_key_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    s->sch = ksch;
    s->mode = WTM_NONFIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    MV_WTM_FUNC_SET_8_PARAMS(
            ksch,
            domain_phys,
            mode,
            prv_key_phys,
            peer_pub_key_phys,
            shrd_key_phys,
            NULL,
            NULL);
    MV_WTM_FUNC_SET_CMD(WTM_ECCP_DH_SHARED_KEY);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_ec_dh_shared_key_nonfips);

int32_t mv_wtm_func_gen_dh_sys_param(mv_wtm_ss ss,
        uint32_t ksch, uint32_t p_phys, uint32_t g_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    s->sch = ksch;
    s->mode = WTM_NONFIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    MV_WTM_FUNC_SET_3_PARAMS(
            ksch, p_phys, g_phys);
    MV_WTM_FUNC_SET_CMD(WTM_DH_SYSPARAM_GEN);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_gen_dh_sys_param);

int32_t mv_wtm_func_create_dh_key_pair_nonfips(mv_wtm_ss ss,
        uint32_t ksch, uint32_t p_phys, uint32_t g_phys,
        uint32_t prv_key_phys, uint32_t pub_key_phys)
{
    /* no such PI */
    return -1;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_dh_key_pair_nonfips);

int32_t mv_wtm_func_derivate_dh_pub_key_nonfips(mv_wtm_ss ss,
        uint32_t ksch, uint32_t p_phys, uint32_t g_phys,
        uint32_t prv_key_phys, uint32_t pub_key_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    s->sch = ksch;
    s->mode = WTM_NONFIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    MV_WTM_FUNC_SET_6_PARAMS(
        ksch, p_phys, g_phys,
        prv_key_phys, 0, pub_key_phys);
    MV_WTM_FUNC_SET_CMD(WTM_DH_PUBLIC_KEY_DERIVE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_derivate_dh_pub_key_nonfips);

int32_t mv_wtm_func_create_dh_shared_key_nonfips(mv_wtm_ss ss,
        uint32_t ksch,
        uint32_t p_phys, uint32_t g_phys,
        uint32_t peer_pub_key_phys, uint32_t prv_key_phys,
        uint32_t shrd_key_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    s->sch = ksch;
    s->mode = WTM_NONFIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    MV_WTM_FUNC_SET_8_PARAMS(
            ksch, p_phys, g_phys,
            peer_pub_key_phys, prv_key_phys, 0, NULL, shrd_key_phys);
    MV_WTM_FUNC_SET_CMD(WTM_DH_SHARED_KEY_GEN);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_dh_shared_key_nonfips);

int32_t mv_wtm_func_create_rsa_key_pair_nonfips(mv_wtm_ss ss,
        uint32_t ksch,
        uint32_t prv_key_exp_phys, uint32_t pub_key_exp_phys,
        uint32_t key_mod_phys,
        uint32_t p_phys, uint32_t q_phys,
        uint32_t trials, uint32_t limit)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    s->sch = ksch;
    s->mode = WTM_NONFIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    MV_WTM_FUNC_SET_8_PARAMS(ksch,
            pub_key_exp_phys, key_mod_phys,
            p_phys, q_phys,
            prv_key_exp_phys,
            trials, limit);
    MV_WTM_FUNC_SET_CMD(WTM_RSA_KEY_GEN);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return MV_WTM_ERR_GEN_KEY;
    }

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_rsa_key_pair_nonfips);

#ifdef CONFIG_EN_FIPS

/*
 * it is safe to use global variables since we have a big lock
 * when CONFIG_EN_MULTI_THRD is active
 * --- ---
 * for win* compiler, there may be a bug of macros
 */
__ALIGNED(static uint8_t _g_wtm_nonce_virt[WTM_OVERALL_NONCE_SZ],
        64 /* _OVERALL_NONCE_SZ_ALGN */);

__ALIGNED(static uint8_t _g_oiap_tag_virt[WTM_OIAP_TAG_SZ],
        32 /* _OIAP_TAG_SZ_ALGN */);

__ALIGNED(static uint8_t _g_wtm_func_kad[2][WTM_KEY_AUTH_DATA_SZ],
        32 /* _KAD_SZ_ALGN */);

__ALIGNED(static uint8_t _g_wtm_thrd_id[WTM_THRD_ID_SZ],
        16 /* _THRD_ID_ALGN */);

__ALIGNED(static uint8_t _g_thrd_id_dgst[WTM_THRD_ID_DGST_SZ],
        32 /* _THRD_ID_DGST_ALGN */);

static int32_t _mv_wtm_func_load_cache_to_eng(
        uint32_t ksch, uint32_t kad_virt, uint32_t cache_slot)
{
    uint32_t wtm_nonce_phys;
    uint32_t oiap_tag_phys;
    uint32_t *nonce;
    int32_t ret;

    wtm_nonce_phys = mv_wtm_func_virt_to_phys((uint32_t)_g_wtm_nonce_virt);
    oiap_tag_phys = mv_wtm_func_virt_to_phys((uint32_t)_g_oiap_tag_virt);

    mv_wtm_func_flush_dcache(
            (uint32_t)_g_wtm_nonce_virt, WTM_OVERALL_NONCE_SZ);
    mv_wtm_func_flush_l2dcache(
            (uint32_t)_g_wtm_nonce_virt, WTM_OVERALL_NONCE_SZ);

    MV_WTM_FUNC_SET_1_PARAM(wtm_nonce_phys);
    MV_WTM_FUNC_SET_CMD(WTM_OIAP_START);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    nonce = (uint32_t *)_g_wtm_nonce_virt;

    /* user nonce need NOT to be WTM_PHYS_ADDR_ALGN aligned */
    nonce[6] = mv_wtm_func_rand();
    nonce[7] = mv_wtm_func_rand();
    nonce[8] = mv_wtm_func_rand();
    nonce[9] = mv_wtm_func_rand();
    nonce[10] = mv_wtm_func_rand();
    nonce[11] = mv_wtm_func_rand();

    ret = mv_wtm_func_hmac_sha256_dgst(
            (uint32_t)_g_wtm_nonce_virt, WTM_OVERALL_NONCE_SZ,
            kad_virt, WTM_KEY_AUTH_DATA_SZ, (uint32_t)_g_oiap_tag_virt);
    MV_WTM_FUNC_ASSERT(0 == ret);

    /* replace WTM nonce by user nonce for addr-algn */
    nonce[0] = nonce[6];
    nonce[1] = nonce[7];
    nonce[2] = nonce[8];
    nonce[3] = nonce[9];
    nonce[4] = nonce[10];
    nonce[5] = nonce[11];

    mv_wtm_func_flush_dcache(
            (uint32_t)_g_wtm_nonce_virt, WTM_OVERALL_NONCE_SZ);
    mv_wtm_func_flush_l2dcache(
            (uint32_t)_g_wtm_nonce_virt, WTM_OVERALL_NONCE_SZ);
    mv_wtm_func_flush_dcache(
            (uint32_t)_g_oiap_tag_virt, WTM_OIAP_TAG_SZ);
    mv_wtm_func_flush_l2dcache(
            (uint32_t)_g_oiap_tag_virt, WTM_OIAP_TAG_SZ);

    MV_WTM_FUNC_SET_4_PARAMS(
            ksch, cache_slot, wtm_nonce_phys, oiap_tag_phys);
    MV_WTM_FUNC_SET_CMD(WTM_LOAD_KEY);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        return MV_WTM_ERR_AUTH;
    }
    MV_WTM_FUNC_READ_1_RES(&ret);
    /* NOTE: here cmd_stat[0] is 1 for loading key successfully! */
    if (1 != ret) {
        return MV_WTM_ERR_AUTH;
    } else {
        return 0;
    }
}

__INLINE static int32_t _mv_wtm_func_load_key_to_cache(
        uint32_t sch, uint32_t wof_phys, uint32_t kad_virt,
        uint32_t *cache_slot)
{
    int32_t ret = 0;

    MV_WTM_FUNC_ASSERT(wof_phys && kad_virt && cache_slot);
    MV_WTM_FUNC_ASSERT(WTM_OVERALL_NONCE_SZ <= _OVERALL_NONCE_SZ_ALGN);
    MV_WTM_FUNC_ASSERT(WTM_OIAP_TAG_SZ <= _OIAP_TAG_SZ_ALGN);

    MV_WTM_FUNC_SET_4_PARAMS(sch, WTM_KEY_EXTERNAL, 0, wof_phys);
    MV_WTM_FUNC_SET_CMD(WTM_UNWRAP_KEYCONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        *cache_slot = WTM_INVALID_CACHE_SLOT_ID;
        ret = MV_WTM_ERR_INVALID_WOF;
        goto _out;
    }
    MV_WTM_FUNC_READ_1_RES(cache_slot);

_out:

    return ret;
}

/* this function does NOT check slot vacancy */
int32_t mv_wtm_func_load_key_to_eng(
        uint32_t sch, uint32_t wof_phys, uint32_t kad_virt,
        uint32_t *cache_slot)
{
    int32_t ret;

    ret = _mv_wtm_func_load_key_to_cache(sch, wof_phys, kad_virt, cache_slot);
    if (ret) {
        goto _out;
    }

    ret = _mv_wtm_func_load_cache_to_eng(sch, kad_virt, *cache_slot);
    if (ret) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_SCH_TO_KEY_TYPE(sch), *cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
        *cache_slot = WTM_INVALID_CACHE_SLOT_ID;
    }

_out:

    return ret;
}

/* if (NULL == slot_id) { purge_cache(slot_id); } */
/* this function does NOT check slot vacancy */
static int32_t _mv_wtm_func_load_key_seq(
        uint32_t ksch, mv_wtm_key_info *key_info_virt,
        uint32_t *subk_slot_id, uint32_t *usrk_slot_id)
{
    uint32_t _subk_slot_id, _usrk_slot_id;
    int32_t ret;

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256,
            key_info_virt->subk_wof_phys,
            key_info_virt->kr_kad_virt,
            &_subk_slot_id);
    if (ret) {
        return ret;
    }

    ret = mv_wtm_func_load_key_to_eng(
            ksch,
            key_info_virt->usrk_wof_phys,
            key_info_virt->uk_kad_virt,
            &_usrk_slot_id);
    if (ret) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, _subk_slot_id);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;

        return ret;
    }

    if (!subk_slot_id && !usrk_slot_id) {
        MV_WTM_FUNC_SET_1_PARAM(2);
        MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
        MV_WTM_FUNC_CHK_BAT_IRQ;

        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, _subk_slot_id);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_CHK_BAT_IRQ;

        MV_WTM_FUNC_SET_2_PARAMS(WTM_SCH_TO_KEY_TYPE(ksch), _usrk_slot_id);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
    } else {
        if (subk_slot_id) {
            *subk_slot_id = _subk_slot_id;
        } else {
            MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, _subk_slot_id);
            MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
            MV_WTM_FUNC_WAIT_FOR_IRQ;
        }

        if (usrk_slot_id) {
            *usrk_slot_id = _usrk_slot_id;
        } else {
            MV_WTM_FUNC_SET_2_PARAMS(WTM_SCH_TO_KEY_TYPE(ksch), _usrk_slot_id);
            MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
            MV_WTM_FUNC_WAIT_FOR_IRQ;
        }
    }

    return 0;
}

static int32_t _mv_wtm_func_encrypt_kad(
        mv_wtm_key_info *endk_info_virt,
        uint32_t kad_src_phys, uint32_t kad_dst_phys)
{
    uint32_t endk_subk_slot_id, endk_usrk_slot_id;
    int32_t ret;

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* two wrappings for end_subk and end_usrk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = _mv_wtm_func_load_key_seq(
            WTM_AES_ECB256, endk_info_virt,
            &endk_subk_slot_id, &endk_usrk_slot_id);
    if (ret) {
        return ret;
    }

    /* purge cache slots together for batch cmds */
    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, endk_subk_slot_id);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    /* endk_usrk_slot_id is purged in WTM_AES_FINISH */

    MV_WTM_FUNC_SET_6_PARAMS(_SYM_OP_ENC, WTM_AES_ECB256, 0, 0, 0, 0);
    MV_WTM_FUNC_SET_CMD(WTM_AES_INIT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_5_PARAMS(
            kad_src_phys, kad_dst_phys, WTM_KEY_AUTH_DATA_SZ, 0,
            endk_usrk_slot_id);
    MV_WTM_FUNC_SET_CMD(WTM_AES_FINISH);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    return 0;
}

/* this function makes sure the slot vacancy */
static int32_t _mv_wtm_func_create_key(
        mv_wtm_key_info *endk_info_virt, uint32_t ksch, uint32_t key_id,
        uint32_t kad_virt, uint32_t *cache_slot)
{
    uint8_t *p_kad_virt;
    int32_t ret;

    mv_wtm_func_memcpy(
            _g_wtm_func_kad[0], (void *)kad_virt, WTM_KEY_AUTH_DATA_SZ);
    if (endk_info_virt) {
        /* assert for cache_flushing_size = WTM_KEY_AUTH_DATA_SZ << 1 */
        MV_WTM_FUNC_ASSERT(WTM_KEY_AUTH_DATA_SZ == _KAD_SZ_ALGN);
        mv_wtm_func_flush_dcache(
                (uint32_t)_g_wtm_func_kad, WTM_KEY_AUTH_DATA_SZ << 1);
        mv_wtm_func_flush_l2dcache(
                (uint32_t)_g_wtm_func_kad, WTM_KEY_AUTH_DATA_SZ << 1);
        ret = _mv_wtm_func_encrypt_kad(endk_info_virt,
                mv_wtm_func_virt_to_phys((uint32_t)_g_wtm_func_kad[0]),
                mv_wtm_func_virt_to_phys((uint32_t)_g_wtm_func_kad[1]));
        if (ret) {
            return ret;
        }
        /* encrypted kad is in _g_wtm_func_kad[1] */
        /* and this memory is cache-coherent now */
        p_kad_virt = _g_wtm_func_kad[1];
    } else {
        mv_wtm_func_flush_dcache(
                (uint32_t)_g_wtm_func_kad[0], WTM_KEY_AUTH_DATA_SZ);
        mv_wtm_func_flush_l2dcache(
                (uint32_t)_g_wtm_func_kad[0], WTM_KEY_AUTH_DATA_SZ);
        p_kad_virt = _g_wtm_func_kad[0];
    }

    if (endk_info_virt) {
        if (mv_wtm_func_need_wrap_slt_out()) {
            /* two wrappings for end_subk and end_usrk */
            ret = mv_wtm_func_wrap_slt_out();
            MV_WTM_FUNC_ASSERT(0 == ret);
            ret = mv_wtm_func_wrap_slt_out();
            MV_WTM_FUNC_ASSERT(0 == ret);
        }

        ret = _mv_wtm_func_load_key_seq(
                WTM_AES_ECB256, endk_info_virt, NULL, NULL);
        if (ret) {
            return ret;
        }
    }

    if (mv_wtm_func_need_wrap_slt_out()) {
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

#ifdef __WORK_AROUND_FOR_UPDATE_LOGIC__

    if ((NULL == endk_info_virt) && _mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_SET_PARAMS(mv_wtm_func_virt_to_phys(
                (uint32_t)p_kad_virt), 0, 0, 0,
                0, ksch, key_id, 0,
                0, 0, 0, 0,
                0, 0, 0, 0);
        MV_WTM_FUNC_SET_CMD(WTM_RESERVED_TEST);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
        MV_WTM_FUNC_READ_1_RES(cache_slot);
    } else

#endif

    {
        MV_WTM_FUNC_SET_3_PARAMS(
                ksch,
                mv_wtm_func_virt_to_phys((uint32_t)p_kad_virt),
                key_id);
        MV_WTM_FUNC_SET_CMD(WTM_CREATE_KEY);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
        MV_WTM_FUNC_READ_1_RES(cache_slot);
    }

    return 0;
}

/* this function makes sure slot vacancy */
static int32_t _mv_wtm_func_create_rsa_key_pair(
        mv_wtm_key_info *endk_info_virt, uint32_t ksch, uint32_t key_id,
        uint32_t kad_virt, uint32_t *cache_slot,
        uint32_t pub_key_exp_phys, uint32_t key_mod_phys,
        uint32_t trials, uint32_t limit)
{
    uint8_t *p_kad_virt;
    int32_t ret;

    mv_wtm_func_memcpy(
            _g_wtm_func_kad[0], (void *)kad_virt, WTM_KEY_AUTH_DATA_SZ);
    if (endk_info_virt) {
        /* assert for cache_flushing_size = WTM_KEY_AUTH_DATA_SZ << 1 */
        MV_WTM_FUNC_ASSERT(WTM_KEY_AUTH_DATA_SZ == _KAD_SZ_ALGN);
        mv_wtm_func_flush_dcache(
                (uint32_t)_g_wtm_func_kad, WTM_KEY_AUTH_DATA_SZ << 1);
        mv_wtm_func_flush_l2dcache(
                (uint32_t)_g_wtm_func_kad, WTM_KEY_AUTH_DATA_SZ << 1);
        ret = _mv_wtm_func_encrypt_kad(endk_info_virt,
                mv_wtm_func_virt_to_phys((uint32_t)_g_wtm_func_kad[0]),
                mv_wtm_func_virt_to_phys((uint32_t)_g_wtm_func_kad[1]));
        if (ret) {
            return ret;
        }
        /* encrypted kad is in _g_wtm_func_kad[1] */
        /* and this memory is cache-coherent now */
        p_kad_virt = _g_wtm_func_kad[1];
    } else {
        mv_wtm_func_flush_dcache(
                (uint32_t)_g_wtm_func_kad[0], WTM_KEY_AUTH_DATA_SZ);
        mv_wtm_func_flush_l2dcache(
                (uint32_t)_g_wtm_func_kad[0], WTM_KEY_AUTH_DATA_SZ);
        p_kad_virt = _g_wtm_func_kad[0];
    }

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* three wrappings for end_subk, end_usrk and rsa_prv_key */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    if (endk_info_virt) {
        if (mv_wtm_func_need_wrap_slt_out()) {
            /* two wrappings for end_subk and end_usrk */
            ret = mv_wtm_func_wrap_slt_out();
            MV_WTM_FUNC_ASSERT(0 == ret);
            ret = mv_wtm_func_wrap_slt_out();
            MV_WTM_FUNC_ASSERT(0 == ret);
        }

        ret = _mv_wtm_func_load_key_seq(
                WTM_AES_ECB256, endk_info_virt, NULL, NULL);
        if (ret) {
            return ret;
        }
    }

    MV_WTM_FUNC_SET_7_PARAMS(
            ksch, pub_key_exp_phys, key_mod_phys,
            mv_wtm_func_virt_to_phys((uint32_t)p_kad_virt),
            key_id, trials, limit);
    MV_WTM_FUNC_SET_CMD(WTM_CREATE_RSA_KEYPAIR);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        return MV_WTM_ERR_GEN_KEY;
    } else {
        MV_WTM_FUNC_READ_1_RES(cache_slot);
        return 0;
    }
}

__INLINE static int32_t _mv_wtm_func_wrap_key(uint32_t ksch,
        uint32_t cache_slot, uint32_t wof_phys, bool is_bated_outside)
{
    if (is_bated_outside) {
        MV_WTM_FUNC_SET_4_PARAMS(
               ksch, WTM_KEY_EXTERNAL, cache_slot, wof_phys);
        MV_WTM_FUNC_SET_CMD(WTM_WRAP_KEYCONTEXT);
        MV_WTM_FUNC_CHK_BAT_IRQ;

        MV_WTM_FUNC_SET_2_PARAMS(WTM_SCH_TO_KEY_TYPE(ksch), cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_CHK_BAT_IRQ;
    } else {
        MV_WTM_FUNC_SET_1_PARAM(2);
        MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
        MV_WTM_FUNC_CHK_BAT_IRQ;

        MV_WTM_FUNC_SET_4_PARAMS(
               ksch, WTM_KEY_EXTERNAL, cache_slot, wof_phys);
        MV_WTM_FUNC_SET_CMD(WTM_WRAP_KEYCONTEXT);
        MV_WTM_FUNC_CHK_BAT_IRQ;

        MV_WTM_FUNC_SET_2_PARAMS(WTM_SCH_TO_KEY_TYPE(ksch), cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
    }

    return 0;
}

int32_t mv_wtm_func_create_subkey(mv_wtm_ss ss,
        mv_wtm_key_info *endk_info_virt,
        uint32_t key_id,
        uint32_t kr_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t cache_slot;
    int32_t ret;

    s->sch = WTM_AES_CBC256;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

    if (!endk_info_virt) {
#ifndef __WORK_AROUND_FOR_UPDATE_LOGIC__
        if (_mv_wtm_func_is_life_cycle_dd()) {
            MV_WTM_FUNC_UNLOCK;
            return MV_WTM_ERR_LIFE_CYCLE;
        }
#endif
    }

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    /* create subkey */
    ret = _mv_wtm_func_create_key(
            endk_info_virt, WTM_AES_CBC256, key_id, kr_kad_virt, &cache_slot);
    if (ret) {
        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    /* wrap out subkey by RKEK */
    ret = _mv_wtm_func_wrap_key(
            WTM_AES_CBC256, cache_slot, subk_wof_phys, false);

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return ret;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_subkey);

int32_t mv_wtm_func_enroll_sym_usrkey(mv_wtm_ss ss,
        uint32_t ksch, uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_phys, uint32_t usrk_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t subk_cache_slot;
    int32_t ret;

    s->sch = ksch;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

#ifndef __WORK_AROUND_FOR_UPDATE_LOGIC__
    if (_mv_wtm_func_is_life_cycle_dd()) {
    
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }
#endif

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* one wrapping for subk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }
 
    /* load subkey to engine */
    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys,
            kr_kad_virt, &subk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    /* for addr-alignement */
    mv_wtm_func_memcpy(
            _g_wtm_func_kad[0], (void *)uk_kad_virt, WTM_KEY_AUTH_DATA_SZ);
    mv_wtm_func_flush_dcache(
            (uint32_t)_g_wtm_func_kad[0], WTM_KEY_AUTH_DATA_SZ);
    mv_wtm_func_flush_l2dcache(
            (uint32_t)_g_wtm_func_kad[0], WTM_KEY_AUTH_DATA_SZ);

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_5_PARAMS(
            ksch, usrk_phys,
            mv_wtm_func_virt_to_phys((uint32_t)_g_wtm_func_kad[0]),
            key_id, usrk_wof_phys);
    MV_WTM_FUNC_SET_CMD(WTM_ENROLL_KEY);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_AES_ZEROIZE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_enroll_sym_usrkey);

int32_t mv_wtm_func_enroll_ec_usrkey(mv_wtm_ss ss,
        uint32_t ksch, uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_phys, uint32_t usrk_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz)
{
    /* same as enroll_sym_usrkey */
    return mv_wtm_func_enroll_sym_usrkey(ss,
            ksch, key_id,
            kr_kad_virt, uk_kad_virt,
            subk_wof_phys, subk_wof_sz,
            usrk_phys, usrk_sz,
            usrk_wof_phys, usrk_wof_sz);
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_enroll_ec_usrkey);

int32_t mv_wtm_func_enroll_rsa_usrkey(mv_wtm_ss ss,
        uint32_t ksch, uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_phys, uint32_t usrk_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz)
{
    return mv_wtm_func_enroll_sym_usrkey(ss,
            ksch, key_id,
            kr_kad_virt, uk_kad_virt,
            subk_wof_phys, subk_wof_sz,
            usrk_phys, usrk_sz,
            usrk_wof_phys, usrk_wof_sz);
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_enroll_rsa_usrkey);

int32_t mv_wtm_func_create_sym_usrkey(mv_wtm_ss ss,
        mv_wtm_key_info *endk_info_virt,
        uint32_t ksch, uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t subk_cache_slot, usrk_cache_slot;
    int32_t ret;

    s->sch = ksch;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

    if (!endk_info_virt) {
#ifndef __WORK_AROUND_FOR_UPDATE_LOGIC__
        if (_mv_wtm_func_is_life_cycle_dd()) {
            MV_WTM_FUNC_UNLOCK;
            return MV_WTM_ERR_LIFE_CYCLE;
        }
#endif
    }

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    /* create usrkey */
    ret = _mv_wtm_func_create_key(
            endk_info_virt, ksch, key_id, uk_kad_virt, &usrk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* one wrapping for subk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys,
            kr_kad_virt, &subk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, usrk_cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;

        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    /* wrap usrkey by subkey, 2 bat cmd */
    _mv_wtm_func_wrap_key(ksch, usrk_cache_slot, usrk_wof_phys, true);

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_sym_usrkey);

int32_t mv_wtm_func_create_ec_usrkey(mv_wtm_ss ss,
        mv_wtm_key_info *endk_info_virt,
        uint32_t ksch,
        uint32_t domain_phys, uint32_t mode,
        uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t pub_key_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t subk_cache_slot, usrk_cache_slot;
    int32_t ret;

    s->sch = ksch;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

    if (!endk_info_virt) {
        if (_mv_wtm_func_is_life_cycle_dd()) {
            MV_WTM_FUNC_UNLOCK;
            return MV_WTM_ERR_LIFE_CYCLE;
        }
    }

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    /* create usrkey */
    ret = _mv_wtm_func_create_key(
            endk_info_virt, ksch, key_id, uk_kad_virt, &usrk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    ret = _mv_wtm_func_load_cache_to_eng(
            ksch, uk_kad_virt, usrk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_SCH_TO_KEY_TYPE(ksch), usrk_cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;

        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    MV_WTM_FUNC_SET_5_PARAMS(
            ksch, domain_phys, mode, 0,
            pub_key_phys);
    MV_WTM_FUNC_SET_CMD(WTM_ECCP_PUBLIC_KEY_DERIVATION);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    /* here let's suppose no usrkey reloading required */

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* one wrapping for subk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys,
            kr_kad_virt, &subk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_ASYM, usrk_cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;

        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    /* wrap usrkey by subkey, 2 bat cmd */
    _mv_wtm_func_wrap_key(ksch, usrk_cache_slot, usrk_wof_phys, true);

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_ec_usrkey);

int32_t mv_wtm_func_derivate_ec_pub_key(mv_wtm_ss ss,
        uint32_t ksch,
        uint32_t domain_phys, uint32_t mode,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t pub_key_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t subk_cache_slot, usrk_cache_slot;
    int32_t ret;

    s->sch = ksch;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* two wrappings for subk and usrk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys, kr_kad_virt,
            &subk_cache_slot);
    if (ret) {
        goto _out0;
    }

    ret = mv_wtm_func_load_key_to_eng(
            ksch, usrk_wof_phys, uk_kad_virt,
            &usrk_cache_slot);
    if (ret) {
        goto _out1;
    }

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_5_PARAMS(
            ksch, domain_phys, mode, 0,
            pub_key_phys);
    MV_WTM_FUNC_SET_CMD(WTM_ECCP_PUBLIC_KEY_DERIVATION);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, usrk_cache_slot);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

_out1:

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

_out0:

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return ret;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_derivate_ec_pub_key);

int32_t mv_wtm_func_create_dh_usrkey(mv_wtm_ss ss,
        mv_wtm_key_info *endk_info_virt,
        uint32_t ksch,
        uint32_t p_phys, uint32_t g_phys,
        uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t pub_key_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t subk_cache_slot, usrk_cache_slot;
    int32_t ret;

    s->sch = ksch;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

    if (!endk_info_virt) {
        if (_mv_wtm_func_is_life_cycle_dd()) {
            MV_WTM_FUNC_UNLOCK;
            return MV_WTM_ERR_LIFE_CYCLE;
        }
    }

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    /* create usrkey */
    ret = _mv_wtm_func_create_key(
            endk_info_virt, ksch, key_id, uk_kad_virt, &usrk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    ret = _mv_wtm_func_load_cache_to_eng(
            ksch, uk_kad_virt, usrk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_SCH_TO_KEY_TYPE(ksch), usrk_cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;

        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    MV_WTM_FUNC_SET_6_PARAMS(
            ksch, p_phys, g_phys,
            NULL, usrk_cache_slot, pub_key_phys);
    MV_WTM_FUNC_SET_CMD(WTM_DH_PUBLIC_KEY_DERIVE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    /* here let's suppose no usrkey reloading required */

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* one wrapping for subk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys,
            kr_kad_virt, &subk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_ASYM, usrk_cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;

        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    /* wrap usrkey by subkey, 2 bat cmd */
    _mv_wtm_func_wrap_key(ksch, usrk_cache_slot, usrk_wof_phys, true);

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_dh_usrkey);

int32_t mv_wtm_func_derivate_dh_pub_key(mv_wtm_ss ss,
        uint32_t ksch,
        uint32_t p_phys, uint32_t g_phys,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t pub_key_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t subk_cache_slot, usrk_cache_slot;
    int32_t ret;

    s->sch = ksch;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* two wrappings for subk and usrk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys, kr_kad_virt,
            &subk_cache_slot);
    if (ret) {
        goto _out0;
    }

    ret = mv_wtm_func_load_key_to_eng(
            ksch, usrk_wof_phys, uk_kad_virt,
            &usrk_cache_slot);
    if (ret) {
        goto _out1;
    }

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_6_PARAMS(
            ksch, p_phys, g_phys,
            NULL, usrk_cache_slot, pub_key_phys);
    MV_WTM_FUNC_SET_CMD(WTM_DH_PUBLIC_KEY_DERIVE);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, usrk_cache_slot);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

_out1:

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

_out0:

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return ret;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_derivate_dh_pub_key);

int32_t mv_wtm_func_create_rsa_usrkey(mv_wtm_ss ss,
        mv_wtm_key_info *endk_info_virt,
        uint32_t ksch,
        uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t pub_key_exp_phys, uint32_t key_mod_phys,
        uint32_t trials, uint32_t limit)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t subk_cache_slot, usrk_cache_slot;
    int32_t ret;

    s->sch = ksch;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

    if (!endk_info_virt) {
        if (_mv_wtm_func_is_life_cycle_dd()) {
            MV_WTM_FUNC_UNLOCK;
            return MV_WTM_ERR_LIFE_CYCLE;
        }
    }

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    /* create usrkey */
    ret = _mv_wtm_func_create_rsa_key_pair(
            endk_info_virt, ksch, key_id, uk_kad_virt, &usrk_cache_slot,
            pub_key_exp_phys, key_mod_phys, trials, limit);
    if (ret) {
        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* one wrapping for subk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys,
            kr_kad_virt, &subk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_ASYM, usrk_cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;

        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    /* wrap usrkey by subkey, 2 bat cmd */
    _mv_wtm_func_wrap_key(ksch, usrk_cache_slot, usrk_wof_phys, true);

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_rsa_usrkey);

/* uk_passwd should be the same as ec_dh_key_info_virt->uk_passwd */
int32_t mv_wtm_func_create_ec_dh_shared_usrkey(mv_wtm_ss ss,
        uint32_t ec_dh_sch, mv_wtm_key_info *ec_dh_key_info_virt,
        uint32_t peer_pub_key_phys,
        uint32_t ksch, uint32_t key_id,
        uint32_t kr_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t shared_cache_slot, subk_cache_slot, usrk_cache_slot;
    int32_t ret;

    s->sch = ksch;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* two wrappings for ec_dh_subk and ec_dh_usrk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = _mv_wtm_func_load_key_seq(
            ec_dh_sch, ec_dh_key_info_virt, NULL, &shared_cache_slot);
    if (ret) {
        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    /* ec_dh prv_key in shared_cache_slot now */

    mv_wtm_func_flush_dcache((uint32_t)_g_wtm_thrd_id, WTM_THRD_ID_SZ);
    mv_wtm_func_flush_l2dcache((uint32_t)_g_wtm_thrd_id, WTM_THRD_ID_SZ);

    MV_WTM_FUNC_SET_8_PARAMS(
            ec_dh_sch,
            NULL,
            WTM_ECC_MULT_RANDOM_MODE,
            NULL,
            peer_pub_key_phys,
            NULL,
            shared_cache_slot,
            mv_wtm_func_virt_to_phys((uint32_t)_g_wtm_thrd_id));
    MV_WTM_FUNC_SET_CMD(WTM_ECCP_DH_SHARED_KEY);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    /* shared key in shared_cache_slot now */

    /* make sure there is enough freed cache slots */
    if (mv_wtm_func_need_wrap_slt_out()) {
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_hmac_sha256_dgst(
            (uint32_t)_g_wtm_thrd_id, WTM_THRD_ID_SZ,
            (uint32_t)ec_dh_key_info_virt->uk_kad_virt, WTM_KEY_AUTH_DATA_SZ,
            (uint32_t)_g_thrd_id_dgst);
    MV_WTM_FUNC_ASSERT(0 == ret);

    mv_wtm_func_flush_dcache((uint32_t)_g_thrd_id_dgst, WTM_THRD_ID_DGST_SZ);
    mv_wtm_func_flush_l2dcache((uint32_t)_g_thrd_id_dgst, WTM_THRD_ID_DGST_SZ);

    MV_WTM_FUNC_SET_4_PARAMS(
            ksch,
            key_id,
            shared_cache_slot,
            mv_wtm_func_virt_to_phys((uint32_t)_g_thrd_id_dgst));
    MV_WTM_FUNC_SET_CMD(WTM_ECCP_SAVE_SHARED_KEY);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(&usrk_cache_slot);

    /* sym key in cache slot */

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* one wrapping for subk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    /* load subkey to engine */
    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys,
            kr_kad_virt, &subk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, usrk_cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;

        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    /* wrap usrkey by subkey, 2 bat cmds */
    _mv_wtm_func_wrap_key(ksch, usrk_cache_slot, usrk_wof_phys, true);

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_ec_dh_shared_usrkey);

int32_t mv_wtm_func_create_dh_shared_usrkey(mv_wtm_ss ss,
        uint32_t dh_sch, mv_wtm_key_info *dh_key_info_virt,
        uint32_t ksch, uint32_t key_id,
        uint32_t p_phys, uint32_t g_phys,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t peer_pub_key_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t shared_cache_slot, subk_cache_slot, usrk_cache_slot;
    int32_t ret;

    s->sch = ksch;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* two wrappings for ec_dh_subk and ec_dh_usrk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = _mv_wtm_func_load_key_seq(
            dh_sch, dh_key_info_virt, NULL, &shared_cache_slot);
    if (ret) {
        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    /* dh prv_key in shared_cache_slot now */

    mv_wtm_func_flush_dcache((uint32_t)_g_wtm_thrd_id, WTM_THRD_ID_SZ);
    mv_wtm_func_flush_l2dcache((uint32_t)_g_wtm_thrd_id, WTM_THRD_ID_SZ);

    MV_WTM_FUNC_SET_8_PARAMS(
            dh_sch,
            p_phys,
            g_phys,
            peer_pub_key_phys,
            NULL,
            shared_cache_slot,
            mv_wtm_func_virt_to_phys((uint32_t)_g_wtm_thrd_id),
            NULL);
    MV_WTM_FUNC_SET_CMD(WTM_DH_SHARED_KEY_GEN);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    /* shared key in shared_cache_slot now */

    /* make sure there is enough freed cache slots */
    if (mv_wtm_func_need_wrap_slt_out()) {
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_hmac_sha256_dgst(
            (uint32_t)_g_wtm_thrd_id, WTM_THRD_ID_SZ,
            (uint32_t)dh_key_info_virt->uk_kad_virt, WTM_KEY_AUTH_DATA_SZ,
            (uint32_t)_g_thrd_id_dgst);
    MV_WTM_FUNC_ASSERT(0 == ret);

    mv_wtm_func_flush_dcache((uint32_t)_g_thrd_id_dgst, WTM_THRD_ID_DGST_SZ);
    mv_wtm_func_flush_l2dcache((uint32_t)_g_thrd_id_dgst, WTM_THRD_ID_DGST_SZ);

    MV_WTM_FUNC_SET_4_PARAMS(
            ksch,
            key_id,
            shared_cache_slot,
            mv_wtm_func_virt_to_phys((uint32_t)_g_thrd_id_dgst));
    MV_WTM_FUNC_SET_CMD(WTM_ECCP_SAVE_SHARED_KEY);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(&usrk_cache_slot);

    /* sym key in cache slot */

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* one wrapping for subk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    /* load subkey to engine */
    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys,
            kr_kad_virt, &subk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, usrk_cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;

        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    /* wrap usrkey by subkey, 2 bat cmds */
    _mv_wtm_func_wrap_key(ksch, usrk_cache_slot, usrk_wof_phys, true);

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_create_dh_shared_usrkey);

int32_t mv_wtm_func_pr_ec256_dec(mv_wtm_ss ss,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        uint32_t ec_point0_phys, uint32_t ec_point1_phys,
        uint32_t ksch_lsb, uint32_t usrk_id_lsb,
        uint32_t kr_kad_virt_lsb,
        uint32_t subk_wof_phys_lsb, uint32_t subk_wof_sz_lsb,
        uint32_t usrk_wof_phys_lsb, uint32_t usrk_wof_sz_lsb,
        uint32_t ksch_msb, uint32_t usrk_id_msb,
        uint32_t kr_kad_virt_msb,
        uint32_t subk_wof_phys_msb, uint32_t subk_wof_sz_msb,
        uint32_t usrk_wof_phys_msb, uint32_t usrk_wof_sz_msb)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t ec256_subk_slot_id, ec256_usrk_slot_id;
    uint32_t subk_slot_id_lsb, usrk_slot_id_lsb;
    uint32_t subk_slot_id_msb, usrk_slot_id_msb;
    int32_t ret;

    s->sch = WTM_ECCP256_DSA_SHA256;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* three wrappings for ec_p256_subk and ec_p256_usrk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys, kr_kad_virt, &ec256_subk_slot_id);
    if (ret) {
        goto _out;
    }

    ret = mv_wtm_func_load_key_to_eng(
            WTM_ECCP256_DSA_SHA256, usrk_wof_phys,
            uk_kad_virt, &ec256_usrk_slot_id);
    if (ret) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, ec256_subk_slot_id);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
        goto _out;
    }

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, ec256_subk_slot_id);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_SET_8_PARAMS(
            ec256_usrk_slot_id,
            ec_point0_phys,
            ec_point1_phys,
            1, /* wrap-flag == 1 */
            ksch_msb,
            ksch_lsb,
            usrk_id_msb,
            usrk_id_lsb);
    MV_WTM_FUNC_SET_CMD(WTM_ECCP_PR256_DECRYPT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_2_RES(&usrk_slot_id_msb, &usrk_slot_id_lsb);


    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_ASYM, ec256_usrk_slot_id);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    /* wrap lsb sym keys */

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys_lsb,
            kr_kad_virt_lsb, &subk_slot_id_lsb);
    if (ret) {
        MV_WTM_FUNC_SET_1_PARAM(2);
        MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
        MV_WTM_FUNC_CHK_BAT_IRQ;
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, usrk_slot_id_lsb);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_CHK_BAT_IRQ;
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, usrk_slot_id_msb);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
        goto _out;
    }

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    /* wrap usrkey by subkey, 2 bat cmd */
    _mv_wtm_func_wrap_key(ksch_lsb, usrk_slot_id_lsb, usrk_wof_phys_lsb, true);

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_slot_id_lsb);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    /* wrap msb sym keys */

    ret = mv_wtm_func_load_key_to_eng(
                WTM_AES_CBC256, subk_wof_phys_msb,
                kr_kad_virt_msb, &subk_slot_id_msb);
    if (ret) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, usrk_slot_id_msb);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
        goto _out;
    }

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    /* wrap usrkey by subkey, 2 bat cmd */
    _mv_wtm_func_wrap_key(ksch_msb, usrk_slot_id_msb, usrk_wof_phys_msb, true);

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_slot_id_msb);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

_out:

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return ret;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_pr_ec256_dec);

static void _mv_wtm_func_zeroize_all_engs(void)
{
    MV_WTM_FUNC_SET_1_PARAM(2);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_AES_ZEROIZE);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    // MV_WTM_FUNC_SET_0_PARAM;
    // MV_WTM_FUNC_SET_CMD(WTM_DES_ZEROIZE);
    // MV_WTM_FUNC_CHK_BAT_IRQ;
    
    // MV_WTM_FUNC_SET_0_PARAM;
    // MV_WTM_FUNC_SET_CMD(WTM_RC4_ZEROIZE);
    // MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_HASH_ZEROIZE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_HMAC_ZEROIZE);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_EMSA_PKCS1_V15_ZEROIZE);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_ECCP_DSA_ZEROIZE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
}

int32_t mv_wtm_func_check_usrkey(mv_wtm_ss ss,
        uint32_t ksch,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz,
        bool *is_valid)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t subk_cache_slot = WTM_INVALID_CACHE_SLOT_ID,
             usrk_cache_slot = WTM_INVALID_CACHE_SLOT_ID;
    int32_t ret = 0;

    s->sch = ksch;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* two wrappings for subk and usrk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys,
            kr_kad_virt, &subk_cache_slot);
    if (ret) {
        *is_valid = false;
        ret = -1;
        goto _out;
    }

    ret = mv_wtm_func_load_key_to_eng(
            ksch, usrk_wof_phys,
            usrk_kad_virt, &usrk_cache_slot);
    if (ret) {
        *is_valid = false;
        ret = -1;
        goto _out;
    }

    *is_valid = true;

_out:

    if (WTM_INVALID_CACHE_SLOT_ID != usrk_cache_slot) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_SCH_TO_KEY_TYPE(ksch), usrk_cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
    }

    if (WTM_INVALID_CACHE_SLOT_ID != subk_cache_slot) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
    }

    _mv_wtm_func_zeroize_all_engs();

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return ret;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_check_usrkey);

int32_t mv_wtm_func_bind_pr_key(mv_wtm_ss ss,
        uint32_t pr_ksch, uint32_t key_id,
        uint32_t kr_kad_virt, uint32_t uk_kad_virt,
        uint32_t subk_wof_phys, uint32_t subk_wof_sz,
        uint32_t usrk_phys, uint32_t usrk_sz,
        uint32_t aes_iv_phys, uint32_t aes_iv_sz,
        uint32_t trans_ksch, uint32_t byte_swap,
        uint32_t usrk_wof_phys, uint32_t usrk_wof_sz)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t subk_cache_slot;
    int32_t ret;

    s->sch = pr_ksch;
    s->mode = WTM_FIPS_MODE;

    MV_WTM_FUNC_LOCK;

#ifndef __WORK_AROUND_FOR_UPDATE_LOGIC__
    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }
#endif

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* one wrapping for subk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }
 
    /* load subkey to engine */
    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys,
            kr_kad_virt, &subk_cache_slot);
    if (ret) {
        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return ret;
    }

    /* for addr-alignement */
    mv_wtm_func_memcpy(
            _g_wtm_func_kad[0], (void *)uk_kad_virt, WTM_KEY_AUTH_DATA_SZ);
    mv_wtm_func_flush_dcache(
            (uint32_t)_g_wtm_func_kad[0], WTM_KEY_AUTH_DATA_SZ);
    mv_wtm_func_flush_l2dcache(
            (uint32_t)_g_wtm_func_kad[0], WTM_KEY_AUTH_DATA_SZ);


    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_8_PARAMS(
            pr_ksch, trans_ksch, key_id, byte_swap, usrk_phys,
            aes_iv_phys, mv_wtm_func_virt_to_phys((uint32_t)_g_wtm_func_kad[0]),
            usrk_wof_phys);
    MV_WTM_FUNC_SET_CMD(WTM_PR_KEY_BIND);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_AES_ZEROIZE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_bind_pr_key);

#endif /* CONFIG_EN_FIPS */
