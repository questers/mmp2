/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_otp.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of OTP in WTM function layer
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func_gen.h"
#include "mv_wtm_func_logic.h"
#include "mv_wtm_func_km.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t mv_wtm_func_prov_rkek_prov(mv_wtm_ss ss,
        uint32_t mode, uint32_t rkek_phys, uint32_t rkek_sz, uint32_t seed)
{
    int32_t ret = MV_WTM_SUCCESS;

    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_3_PARAMS(mode, rkek_phys, seed);
    MV_WTM_FUNC_SET_CMD(WTM_RKEK_PROVISION);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        ret = MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_UNLOCK;

    return ret;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_rkek_prov);

int32_t mv_wtm_func_prov_ec521_dk_prov(mv_wtm_ss ss,
        uint32_t mode,
        uint32_t key_msb_phys, uint32_t key_lsb_phys,
        uint32_t key_sz, uint32_t seed, uint32_t seed_sz)
{
    int32_t ret = MV_WTM_SUCCESS;

    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_4_PARAMS(mode,
            key_msb_phys, key_lsb_phys,
            seed);
    MV_WTM_FUNC_SET_CMD(WTM_EC521_DK_PROVISION);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        ret = MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_UNLOCK;

    return ret;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_ec521_dk_prov);

int32_t mv_wtm_func_prov_plat_bind(mv_wtm_ss ss,
        uint32_t sch,
        uint32_t key0_phys, uint32_t key1_phys, uint32_t key_sz)
{
    int32_t ret = MV_WTM_SUCCESS;

    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_3_PARAMS(sch, key0_phys, key1_phys);
    MV_WTM_FUNC_SET_CMD(WTM_OEM_PLATFORM_BIND);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        ret = MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_UNLOCK;

    return ret;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_plat_bind);

int32_t mv_wtm_func_prov_plat_verf(mv_wtm_ss ss,
        uint32_t sch,
        uint32_t key0_phys, uint32_t key1_phys, uint32_t key_sz,
        bool *is_valid)
{
    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_3_PARAMS(sch, key0_phys, key1_phys);
    MV_WTM_FUNC_SET_CMD(WTM_OEM_PLATFORM_VERIFY);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;

    if (MV_WTM_FUNC_GET_CMD_STAT) {
        *is_valid = false;
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_READ_1_RES(is_valid);

    /* 0 - OK, 1 - FAIL in PI spec */
    *is_valid = !(*is_valid);
    if (!*is_valid) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_INVALID_KEY;
    }

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_plat_verf);

int32_t mv_wtm_func_prov_bind_jtag_key(mv_wtm_ss ss,
        uint32_t sch,
        uint32_t key0, uint32_t key1, uint32_t key_sz)
{
    int32_t ret = MV_WTM_SUCCESS;

    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_3_PARAMS(sch, key0, key1);
    MV_WTM_FUNC_SET_CMD(WTM_OEM_JTAG_KEY_BIND);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        ret = MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_UNLOCK;

    return ret;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_bind_jtag_key);

int32_t mv_wtm_func_prov_verf_jtag_key(mv_wtm_ss ss,
        uint32_t sch,
        uint32_t key0, uint32_t key1, uint32_t key_sz,
        bool *is_valid)
{
    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_3_PARAMS(sch, key0, key1);
    MV_WTM_FUNC_SET_CMD(WTM_OEM_JTAG_KEY_VERIFY);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;

    if (MV_WTM_FUNC_GET_CMD_STAT) {
        *is_valid = false;
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_READ_1_RES(is_valid);

    /* 0 - OK, 1 - FAIL in PI spec */
    *is_valid = !(*is_valid);
    if (!*is_valid) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_INVALID_KEY;
    }

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_verf_jtag_key);

int32_t mv_wtm_func_prov_dis_jtag(mv_wtm_ss ss)
{
    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_1_PARAM(0);
    MV_WTM_FUNC_SET_CMD(WTM_SET_JTAG_PERMANENT_DISABLE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_dis_jtag);

int32_t mv_wtm_func_prov_tmp_dis_fa(mv_wtm_ss ss)
{
    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_1_PARAM(0);
    MV_WTM_FUNC_SET_CMD(WTM_SET_TEMPORARY_FA_DISABLE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_tmp_dis_fa);

int32_t mv_wtm_func_prov_rd_uniq_id(mv_wtm_ss ss,
        uint32_t id_phys, uint32_t id_sz)
{
    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_1_PARAM(id_phys);
    MV_WTM_FUNC_SET_CMD(WTM_SOC_UNIQUE_ID_READ);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_rd_uniq_id);

int32_t mv_wtm_func_prov_wr_plat_conf(mv_wtm_ss ss,
        uint32_t conf_phys, uint32_t conf_sz)
{
    int32_t ret = MV_WTM_SUCCESS;

    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_1_PARAM(conf_phys);
    MV_WTM_FUNC_SET_CMD(WTM_OTP_WRITE_PLATFORM_CONFIG);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        ret = MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_UNLOCK;

    return ret;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_wr_plat_conf);

int32_t mv_wtm_func_prov_rd_plat_conf(mv_wtm_ss ss,
        uint32_t conf_phys, uint32_t conf_sz)
{
    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_1_PARAM(conf_phys);
    MV_WTM_FUNC_SET_CMD(WTM_OTP_READ_PLATFORM_CONFIG);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_rd_plat_conf);

int32_t mv_wtm_func_prov_wr_usb_id(mv_wtm_ss ss,
        uint32_t id_phys, uint32_t id_sz)
{
    int32_t ret = MV_WTM_SUCCESS;

    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_1_PARAM(id_phys);
    MV_WTM_FUNC_SET_CMD(WTM_OEM_USBID_PROVISION);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        ret = MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_UNLOCK;

    return ret;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_wr_usb_id);

int32_t mv_wtm_func_prov_rd_usb_id(mv_wtm_ss ss,
        uint32_t id_phys, uint32_t id_sz)
{
    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_1_PARAM(id_phys);
    MV_WTM_FUNC_SET_CMD(WTM_OEM_USBID_READ);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_rd_usb_id);

int32_t mv_wtm_func_prov_hdcp_wrap_key(mv_wtm_ss ss,
        uint32_t key_set_phys, uint32_t ksv_phys, uint32_t wof_phys)
{
    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_4_PARAMS(0, NULL, key_set_phys, wof_phys);
    MV_WTM_FUNC_SET_CMD(WTM_HDCP_WRAP_KEY);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_hdcp_wrap_key);

int32_t mv_wtm_func_prov_set_fips_perm(mv_wtm_ss ss)
{
    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_1_PARAM(1);
    MV_WTM_FUNC_SET_CMD(WTM_SWITCH_FIPS_MODE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    g_wtm_func_drv.mode = 1;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_SET_FIPS_MODE_PERMANENT);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_set_fips_perm);

int32_t mv_wtm_func_prov_set_nonfips_perm(mv_wtm_ss ss)
{
    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_1_PARAM(0);
    MV_WTM_FUNC_SET_CMD(WTM_SWITCH_FIPS_MODE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    g_wtm_func_drv.mode = 0;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_SET_NONFIPS_MODE_PERMANENT);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_PERM_DENIED;
    }

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_set_nonfips_perm);

bool mv_wtm_func_prov_is_proved(mv_wtm_ss ss)
{
    uint32_t tsr;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_GET_TRUST_STATUS_REGISTER);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(&tsr);

    MV_WTM_FUNC_UNLOCK;

    return (WTM_TSR_RKEK_PROVED == (tsr & WTM_TSR_RKEK_PROV_MASK));
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_is_proved);

int32_t mv_wtm_func_prov_pre_proc(mv_wtm_ss ss)
{
    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_pre_proc);

int32_t mv_wtm_func_prov_post_proc(mv_wtm_ss ss, uint32_t stage,
        uint8_t *wrap_subk_kad, uint8_t *wrap_subk_wof)
{
    uint32_t lc;
    uint32_t tsr;
    int32_t ret = 0;

    if ((0 != stage) && (1 != stage)) {
        mv_wtm_func_pr("ERROR - invalid wtm stage\n");
        return -1;
    }

    MV_WTM_FUNC_LOCK;

#ifdef CONFIG_EN_FIPS

    /*
     * if WTM_INIT is called before prov,
     * this branch will check if wrap_subk is set.
     */
    if (wrap_subk_kad && wrap_subk_wof) {
        ret = mv_wtm_func_xllp_set_wrap_subk(wrap_subk_kad, wrap_subk_wof);
        MV_WTM_FUNC_ASSERT(MV_WTM_SUCCESS == ret);
    }

#endif

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_LIFECYCLE_READ);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(&lc);

    if (WTM_LIFE_CYCLE_DM == lc) {
        if (1 == stage) {
            MV_WTM_FUNC_SET_0_PARAM;
            MV_WTM_FUNC_SET_CMD(WTM_LIFECYCLE_ADVANCE);
            MV_WTM_FUNC_WAIT_FOR_IRQ;
        }
    } else /* DD */ {
        if (0 == stage) {
            mv_wtm_func_pr("ERROR - wtm in DD mode, reboot required\n");
            return -1;
        }
    }

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_GET_TRUST_STATUS_REGISTER);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(&tsr);

    if (WTM_TSR_FIPS_ONLY == (tsr & WTM_TSR_FIPS_ONLY_MASK)) {
        /* launch an init mode: fips */
        MV_WTM_FUNC_SET_1_PARAM(1);
        MV_WTM_FUNC_SET_CMD(WTM_SWITCH_FIPS_MODE);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
        g_wtm_func_drv.mode = 1;
    } else /* non-fips only or nonfips/fips */ {
        /* launch an init mode: non-fips */
        MV_WTM_FUNC_SET_1_PARAM(0);
        MV_WTM_FUNC_SET_CMD(WTM_SWITCH_FIPS_MODE);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
        g_wtm_func_drv.mode = 0;
    }

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_post_proc);

int32_t mv_wtm_func_prov_transit_key(mv_wtm_ss ss,
        uint32_t key_phys, uint32_t ksch, uint32_t type)
{
    MV_WTM_FUNC_LOCK;

    if (_mv_wtm_func_is_life_cycle_dd()) {
        MV_WTM_FUNC_UNLOCK;
        return MV_WTM_ERR_LIFE_CYCLE;
    }

    MV_WTM_FUNC_SET_1_PARAM(1);
    MV_WTM_FUNC_SET_CMD(WTM_SWITCH_FIPS_MODE);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    g_wtm_func_drv.mode = 1;

    MV_WTM_FUNC_SET_3_PARAMS(type, ksch, key_phys);
    MV_WTM_FUNC_SET_CMD(WTM_OEM_TRANSIT_KEY_PROVISION);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_SUCCESS;
}
MV_WTM_FUNC_EXPORT(mv_wtm_func_prov_transit_key);
