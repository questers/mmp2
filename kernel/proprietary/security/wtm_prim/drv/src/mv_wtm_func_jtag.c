/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_jtag.c
 * Author       : Dafu Lv
 * Date Created : 06/09/2010
 * Description  : The implementation file of JTAG enabling in WTM function layer.
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func_gen.h"
#include "mv_wtm_func_logic.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t mv_wtm_func_get_jtag_nonce(mv_wtm_ss ss, uint32_t nonce_phys)
{
    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SET_1_PARAM(nonce_phys);
    MV_WTM_FUNC_SET_CMD(WTM_JTAG_START);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_get_jtag_nonce);

int32_t mv_wtm_func_set_jtag(mv_wtm_ss ss, uint32_t sch,
        uint32_t key0_phys, uint32_t key1_phys,
        uint32_t ctrl_phys, uint32_t sig_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    int32_t ret = 0;

    s->sch = sch;
    s->mode = WTM_NONFIPS_MODE;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    MV_WTM_FUNC_SET_5_PARAMS(
            sch, ctrl_phys, key0_phys, key1_phys,
            sig_phys);
    MV_WTM_FUNC_SET_CMD(WTM_JTAG_AUTHORIZATION);
    MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    if (MV_WTM_FUNC_GET_CMD_STAT) {
        MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

        MV_WTM_FUNC_UNLOCK;

        return MV_WTM_ERR_AUTH;
    }

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return ret;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_set_jtag);
