/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_sym.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of sym logic in WTM function layer
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func_logic.h"
#include "mv_wtm_func_km.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/* NEED TO CHANGE IF WTM_CRYPTO_SCHEME CHANGES/EXTENDS */

#define IS_SYM_SCH_AES(_sch)        (0x8000 == ((_sch) & 0xF000))
#define IS_SYM_SCH_RC4(_sch)        (0xD000 == ((_sch) & 0xF000))

/* sym sch to pi */
#define SYM_SCH_TO_INIT(_sch)   \
        (IS_SYM_SCH_AES(_sch)) ? (WTM_AES_INIT) : \
        ((IS_SYM_SCH_RC4(_sch)) ? (WTM_RC4_INIT) : (WTM_DES_INIT))
#define SYM_SCH_TO_ZERO(_sch)   \
        (IS_SYM_SCH_AES(_sch)) ? (WTM_AES_ZEROIZE) : \
        ((IS_SYM_SCH_RC4(_sch)) ? (WTM_RC4_ZEROIZE) : (WTM_DES_ZEROIZE))
#define SYM_SCH_TO_PROC(_sch)   \
        (IS_SYM_SCH_AES(_sch)) ? (WTM_AES_PROCESS) : \
        ((IS_SYM_SCH_RC4(_sch)) ? (WTM_RC4_PROCESS) : (WTM_DES_PROCESS))
#define SYM_SCH_TO_FINI(_sch)   \
        (IS_SYM_SCH_AES(_sch)) ? (WTM_AES_FINISH) : \
        ((IS_SYM_SCH_RC4(_sch)) ? (WTM_RC4_FINISH) : (WTM_DES_FINISH))
#define SYM_SCH_TO_ALL(_sch)    (0xFFFFFFFF)

#define SYM_PS_INIT(_s)                             \
    do {                                            \
        (_s)->ps.init = SYM_SCH_TO_INIT((_s)->sch); \
        (_s)->ps.zero = SYM_SCH_TO_ZERO((_s)->sch); \
        (_s)->ps.proc = SYM_SCH_TO_PROC((_s)->sch); \
        (_s)->ps.fini = SYM_SCH_TO_FINI((_s)->sch); \
        (_s)->ps.all  = SYM_SCH_TO_ALL((_s)->sch);  \
    } while (0)

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t mv_wtm_func_sym_init_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t key0_phys, uint32_t key1_phys,
        uint32_t key2_phys, uint32_t key_len,
        uint32_t iv_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    s->sch = sch;
    s->func = func;
    s->mode = WTM_NONFIPS_MODE;
    s->rsvd.resumed = false;
    s->rsvd.slot_id = NULL;

    SYM_PS_INIT(s);

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    s->cntx = MV_WTM_FUNC_CREATE_CNTX(s);
    MV_WTM_FUNC_ASSERT(s->cntx);

    /* OPT: func, sch, iv, key0, key1, key2 */
    if (IS_SYM_SCH_AES(sch)) {
        MV_WTM_FUNC_SET_5_PARAMS(
                func, sch, key0_phys, key1_phys, iv_phys);
    } else if (IS_SYM_SCH_RC4(sch)) {
        MV_WTM_FUNC_SET_4_PARAMS(
                func, sch, key0_phys, key_len);
    } else {
        MV_WTM_FUNC_SET_6_PARAMS(
                func, sch, key0_phys, key1_phys, key2_phys, iv_phys);
    }
    MV_WTM_FUNC_SET_CMD(s->ps.init);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_STORE_CNTX(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_sym_init_nonfips);

#ifdef CONFIG_EN_FIPS

int32_t mv_wtm_func_sym_init_fips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof_phys, uint32_t usrk_wof_phys, uint32_t iv_phys)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t subk_cache_slot_id = WTM_INVALID_CACHE_SLOT_ID;
    uint32_t usrk_cache_slot_id = WTM_INVALID_CACHE_SLOT_ID;
    int32_t ret;

    s->sch = sch;
    s->func = func;
    s->mode = WTM_FIPS_MODE;
    s->rsvd.resumed = false;

    SYM_PS_INIT(s);

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    s->cntx = MV_WTM_FUNC_CREATE_CNTX(s);
    MV_WTM_FUNC_ASSERT(s->cntx);

    if (mv_wtm_func_need_wrap_slt_out()) {
        /* two wrappings for subk and usrk */
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
        ret = mv_wtm_func_wrap_slt_out();
        MV_WTM_FUNC_ASSERT(0 == ret);
    }

    ret = mv_wtm_func_load_key_to_eng(
            WTM_AES_CBC256, subk_wof_phys, kr_kad_virt,
            &subk_cache_slot_id);
    if (ret) {
        goto _out;
    }

    ret = mv_wtm_func_load_key_to_eng(
            sch, usrk_wof_phys, usrk_kad_virt,
            &usrk_cache_slot_id);
    if (ret) {
        goto _out;
    }

    mv_wtm_func_save_usrk_info(
            usrk_kad_virt, usrk_cache_slot_id, s);

    /* subkey is over-write */

    /* lets batch 2 cmds */
    MV_WTM_FUNC_SET_1_PARAM(2);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot_id);
    MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    /* aes: 5 params, des: 6 params */
    MV_WTM_FUNC_SET_6_PARAMS(func, sch, 0, 0,
            IS_SYM_SCH_AES(sch) ? iv_phys : 0,
            IS_SYM_SCH_AES(sch) ? 0 : iv_phys);
    MV_WTM_FUNC_SET_CMD(s->ps.init);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_STORE_CNTX(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;

_out:

    if (WTM_INVALID_CACHE_SLOT_ID != usrk_cache_slot_id) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, usrk_cache_slot_id);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
    }

    if (WTM_INVALID_CACHE_SLOT_ID != subk_cache_slot_id) {
        MV_WTM_FUNC_SET_2_PARAMS(WTM_KEY_TYPE_SYM, subk_cache_slot_id);
        MV_WTM_FUNC_SET_CMD(WTM_PURGE_CONTEXT);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
    }

    if (s->cntx) {
        MV_WTM_FUNC_DESTROY_CNTX(s);
    }

    MV_WTM_FUNC_UNLOCK;

    return MV_WTM_ERR_AUTH;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_sym_init_fips);

#endif /* CONFIG_EN_FIPS */

int32_t mv_wtm_func_sym_proc(
        mv_wtm_ss ss, uint32_t src_phys, uint32_t src_dma_phys,
        uint32_t dst_phys, uint32_t dst_dma_phys, uint32_t len)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_LOAD_CNTX(s);

    do{
        MV_WTM_FUNC_SET_PARAMS(
                src_phys, dst_phys, len, s->rsvd.resumed,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, src_dma_phys, dst_dma_phys);
        MV_WTM_FUNC_SET_CMD(s->ps.proc);
        MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    } while(g_wtm_func_drv.irq_stat == STATUS_AES_TIMEOUT || \
            g_wtm_func_drv.irq_stat == STATUS_DES_TIMEOUT);
    MV_WTM_FUNC_ASSERT(0 == g_wtm_func_drv.irq_stat);

    s->rsvd.resumed = true;

    MV_WTM_FUNC_STORE_CNTX(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_sym_proc);

int32_t mv_wtm_func_sym_final(
        mv_wtm_ss ss, uint32_t src_phys, uint32_t src_dma_phys,
        uint32_t dst_phys, uint32_t dst_dma_phys, uint32_t len)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;
    uint32_t slot_id;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_LOAD_CNTX(s);

    slot_id = s->rsvd.slot_id ?
        *(s->rsvd.slot_id) : WTM_INVALID_CACHE_SLOT_ID;

    do {
        /* NOTE: AES final has one more param: resume flag */
        MV_WTM_FUNC_SET_PARAMS(
                src_phys, dst_phys, len,
                IS_SYM_SCH_AES(s->sch) ? s->rsvd.resumed : slot_id,
                IS_SYM_SCH_AES(s->sch) ? slot_id : 0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, src_dma_phys, dst_dma_phys);
        MV_WTM_FUNC_SET_CMD(s->ps.fini);
        MV_WTM_FUNC_WAIT_FOR_IRQ_NO_CHK;
    } while (g_wtm_func_drv.irq_stat == STATUS_AES_TIMEOUT || \
            g_wtm_func_drv.irq_stat == STATUS_DES_TIMEOUT);
    MV_WTM_FUNC_ASSERT(0 == g_wtm_func_drv.irq_stat);

#ifdef CONFIG_EN_FIPS

    if (WTM_FIPS_MODE == s->mode) {
        mv_wtm_func_save_usrk_info(
                0, WTM_INVALID_CACHE_SLOT_ID, s);
    }

#endif

    s->rsvd.resumed = false;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(s->ps.zero);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_sym_final);

int32_t mv_wtm_func_sym_kill_seq(mv_wtm_ss ss)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_LOAD_CNTX(s);

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(s->ps.zero);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_sym_kill_seq);

int32_t mv_wtm_func_sym_op_nonfips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t key0_phys, uint32_t key1_phys, uint32_t key2_phys,
        uint32_t key_len, uint32_t iv_phys,
        uint32_t src_phys, uint32_t src_dma_phys,
        uint32_t dst_phys, uint32_t dst_dma_phys, uint32_t len)
{
    mv_wtm_func_ss_priv *s = (mv_wtm_func_ss_priv *)ss;

    s->sch = sch;
    s->func = func;
    s->mode = WTM_NONFIPS_MODE;

    SYM_PS_INIT(s);

    MV_WTM_FUNC_LOCK;

    MV_WTM_FUNC_SWITCH_MODE(s);

    MV_WTM_FUNC_CREATE_CNTX_ONESHOT(s);

    MV_WTM_FUNC_SET_1_PARAM(3);
    MV_WTM_FUNC_SET_CMD(WTM_SET_BATCH_COUNT);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    /* OPT: func, sch, iv, key0, key1, key2 */
    if (IS_SYM_SCH_AES(sch)) {
        MV_WTM_FUNC_SET_5_PARAMS(
                func, sch, key0_phys, key1_phys, iv_phys);
    } else if (IS_SYM_SCH_RC4(sch)) {
        MV_WTM_FUNC_SET_4_PARAMS(
                func, sch, key0_phys, key_len);
    } else {
        MV_WTM_FUNC_SET_6_PARAMS(
                func, sch, key0_phys, key1_phys, key2_phys, iv_phys);
    }
    MV_WTM_FUNC_SET_CMD(s->ps.init);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_PARAMS(
            src_phys, dst_phys, len, 0, 
            WTM_INVALID_CACHE_SLOT_ID, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, src_dma_phys, dst_dma_phys);
    MV_WTM_FUNC_SET_CMD(s->ps.fini);
    MV_WTM_FUNC_CHK_BAT_IRQ;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(s->ps.zero);
    MV_WTM_FUNC_WAIT_FOR_IRQ;

    MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(s);

    MV_WTM_FUNC_UNLOCK;

    return 0;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_sym_op_nonfips);

#ifdef CONFIG_EN_FIPS

int32_t mv_wtm_func_sym_op_fips(
        mv_wtm_ss ss, uint32_t sch, uint32_t func,
        uint32_t kr_kad_virt, uint32_t usrk_kad_virt,
        uint32_t subk_wof_phys, uint32_t usrk_wof_phys, uint32_t iv_phys,
        uint32_t src_phys, uint32_t dst_phys,
        uint32_t src_dma_phys, uint32_t dst_dma_phys, uint32_t len)
{
    /* not supported yet */
    return -1;
}

MV_WTM_FUNC_EXPORT(mv_wtm_func_sym_op_fips);

#endif /* CONFIG_EN_FIPS */
