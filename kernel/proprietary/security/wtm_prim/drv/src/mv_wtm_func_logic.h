/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_logic.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file for scheme-spec logic in WTM function layer
 *
 */

#ifndef _MV_WTM_FUNC_LOGIC_H_
#define _MV_WTM_FUNC_LOGIC_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func_cntx.h"
#include "mv_wtm_func_gen.h"
#include "mv_wtm_func_xllp.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#define MV_WTM_FUNC_SWITCH_MODE(_s)    mv_wtm_func_switch_mode(_s)

#ifndef CONFIG_EN_LAZY_CNTX

#define MV_WTM_FUNC_CREATE_CNTX(_s)     \
    mv_wtm_func_create_cntx(_s)
#define MV_WTM_FUNC_DESTROY_CNTX(_s)    \
    mv_wtm_func_destroy_cntx(_s, (_s)->cntx)

#define MV_WTM_FUNC_STORE_CNTX(_s)  \
    mv_wtm_func_store_cntx(_s, (_s)->cntx)
#define MV_WTM_FUNC_LOAD_CNTX(_s)   \
    mv_wtm_func_load_cntx(_s, (_s)->cntx)

#define MV_WTM_FUNC_CREATE_CNTX_ONESHOT(_s)
#define MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(_s)

#else /* CONFIG_EN_LAZY_CNTX */

#define MV_WTM_FUNC_CREATE_CNTX(_s)     \
    mv_wtm_func_create_lazy_cntx(_s)
#define MV_WTM_FUNC_DESTROY_CNTX(_s)    \
    mv_wtm_func_destroy_lazy_cntx(_s, (_s)->cntx)

#define MV_WTM_FUNC_STORE_CNTX(_s)  \
    mv_wtm_func_store_lazy_cntx(_s, (_s)->cntx)
#define MV_WTM_FUNC_LOAD_CNTX(_s)   \
    mv_wtm_func_load_lazy_cntx(_s, (_s)->cntx)

#define MV_WTM_FUNC_CREATE_CNTX_ONESHOT(_s)     \
    mv_wtm_func_create_lazy_cntx_oneshot(_s)
#define MV_WTM_FUNC_DESTROY_CNTX_ONESHOT(_s)    \
    mv_wtm_func_destroy_lazy_cntx_oneshot(_s, (_s)->cntx)

#endif /* CONFIG_EN_LAZY_CNTX */

#ifdef CONFIG_EN_MULTI_THRD

#define MV_WTM_FUNC_LOCK    \
    mv_wtm_func_wait_for_mutex(g_wtm_func_drv.lock, INFINITE)
#define MV_WTM_FUNC_UNLOCK  \
    mv_wtm_func_release_mutex(g_wtm_func_drv.lock)

#else

#define MV_WTM_FUNC_LOCK
#define MV_WTM_FUNC_UNLOCK

#endif

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

extern mv_wtm_func_drv g_wtm_func_drv;

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#endif /* _MV_WTM_FUNC_LOGIC_H_ */
