/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_km.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of key management in WTM function layer
 *
 */

#ifndef _MV_WTM_FUNC_KM_H_
#define _MV_WTM_FUNC_KM_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func.h"

#include "mv_wtm_func_gen.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

__INLINE static bool _mv_wtm_func_is_life_cycle_dd(void)
{
    uint32_t lc;

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_LIFECYCLE_READ);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(&lc);
    if (WTM_LIFE_CYCLE_DD == lc) {
        return true;
    } else {
        return false;
    }
}

#ifdef CONFIG_EN_FIPS
extern int32_t mv_wtm_func_load_key_to_eng(
        uint32_t sch, uint32_t wof_phys, uint32_t kad_virt,
        uint32_t *cache_slot);
/* other interfaces are exported in mv_wtm_func.h */
#endif /* CONFIG_EN_FIPS */

#endif /* _MV_WTM_FUNC_KM_ */
