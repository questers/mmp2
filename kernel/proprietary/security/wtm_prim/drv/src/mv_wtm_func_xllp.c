/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_func_xllp.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of XLLP in WTM function layer.
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func_km.h"
#include "mv_wtm_func_logic.h"
#include "mv_wtm_func_os_srv.h"
#include "mv_wtm_func_sw.h"
#include "mv_wtm_func_xllp.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

static int32_t _mv_wtm_func_chk_fw(void)
{
    mv_wtm_func_drv *drv = &g_wtm_func_drv;

    /* if wtm firmware is ready, the 8th bit of cmd_fifo_stat is 1 */
    if (drv->regs->cmd_fifo_stat & 0x100) {
        return 0;
    } else {
        mv_wtm_func_pr("WARNING - wtm firmware not available.\n");
        return -1;
    }
}

#ifdef CONFIG_EN_FIPS

static int32_t _mv_wtm_func_xllp_fips_init(void)
{
    mv_wtm_func_drv *drv = &g_wtm_func_drv;
    int32_t ret;

#ifdef CONFIG_EN_DBG

    g_wtm_func_drv.freed_cache_slots = mv_wtm_func_get_freed_slt_num();

#endif

    drv->wrap_subk_wof_sz = WTM_SYM_WOF_SZ; /* sizeof(WOF_of_AES) */
    if (!drv->wrap_subk_wof_virt) {
        drv->wrap_subk_wof_virt = (uint32_t)mv_wtm_func_alloc_dma_mem(
                drv->wrap_subk_wof_sz, WTM_PHYS_ADDR_ALGN,
                (void **)&drv->wrap_subk_wof_phys);
        MV_WTM_FUNC_ASSERT(drv->wrap_subk_wof_virt);
    }

    ret = mv_wtm_func_init_fips_res_list();
    MV_WTM_FUNC_ASSERT(0 == ret);

    return 0;
}

static void _mv_wtm_func_xllp_fips_cleanup(void)
{
    mv_wtm_func_drv *drv = &g_wtm_func_drv;

    mv_wtm_func_cleanup_fips_res_list();

    if (drv->wrap_subk_wof_virt) {
        mv_wtm_func_free_dma_mem((void *)drv->wrap_subk_wof_virt);
        drv->wrap_subk_wof_virt = 0;
    }

#ifdef CONFIG_EN_DBG

    if (g_wtm_func_drv.freed_cache_slots)
    {
        uint32_t freed_slt_num = mv_wtm_func_get_freed_slt_num();

        MV_WTM_FUNC_ASSERT(freed_slt_num <= g_wtm_func_drv.freed_cache_slots);

        if (freed_slt_num < g_wtm_func_drv.freed_cache_slots) {
            mv_wtm_func_pr("WARNING - cache slot leak from %d to %d\n",
                    g_wtm_func_drv.freed_cache_slots, freed_slt_num);
            mv_wtm_func_pr("dead-looping, waiting for hardware reset\n");
            while (1);
        } else {
            mv_wtm_func_pr("INFO - all dirty cache slot(s) freed [%d]\n",
                    freed_slt_num);
        }
    }

#endif
}

int32_t mv_wtm_func_xllp_create_wrap_subk(
        uint8_t *wrap_subk_kad)
{
    mv_wtm_ss ss;
    int32_t ret;
    mv_wtm_func_drv *drv = &g_wtm_func_drv;

    MV_WTM_FUNC_ASSERT(drv->wrap_subk_wof_virt);

    if (!drv->is_wrap_subk_set) {
        if (wrap_subk_kad) {
            mv_wtm_func_memcpy(
                    (void *)drv->wrap_subk_kad_virt,
                    wrap_subk_kad,
                    WTM_KEY_AUTH_DATA_SZ);
        }
        drv->is_wrap_subk_set = true;
    }
	
    ss = mv_wtm_func_create_ss();
    MV_WTM_FUNC_ASSERT(ss);
	
    ret = mv_wtm_func_create_subkey(
            ss, NULL, 0x7FFFFFFF,
            (uint32_t)drv->wrap_subk_kad_virt, (uint32_t)drv->wrap_subk_wof_phys,
            drv->wrap_subk_wof_sz);
    MV_WTM_FUNC_ASSERT(0 == ret);
    mv_wtm_func_destroy_ss(ss);
    return 0;
}

int32_t mv_wtm_func_xllp_set_wrap_subk(
        uint8_t *wrap_subk_kad, uint8_t *wrap_subk_wof)
{
    mv_wtm_func_drv *drv = &g_wtm_func_drv;

    MV_WTM_FUNC_ASSERT(drv->wrap_subk_wof_virt);

    if (!drv->is_wrap_subk_set) {
        if (wrap_subk_wof) {
            mv_wtm_func_memcpy(
                    (void *)drv->wrap_subk_wof_virt,
                    wrap_subk_wof,
                    drv->wrap_subk_wof_sz);
        }
        if (wrap_subk_kad) {
            mv_wtm_func_memcpy(
                    (void *)drv->wrap_subk_kad_virt,
                    wrap_subk_kad,
                    WTM_KEY_AUTH_DATA_SZ);
        }

        drv->is_wrap_subk_set = true;
    }

    return 0;
}

#endif /* CONFIG_EN_FIPS */

#include <linux/kthread.h>

static struct timer_list _g_wtm_timer;
static struct semaphore  _g_wtm_sem;
static bool _g_wtm_need_reset = false;

static int _wtm_monitor(void *param)
{
    down(&_g_wtm_sem);

    if (_g_wtm_need_reset) {
        mv_wtm_func_pr("ERROR - wtm init timeout & reboot\n");
        arm_pm_restart('\0', NULL);
    }

    return 0;
}

static void _wtm_init_timeout (unsigned long arg)
{
    _g_wtm_need_reset = true;
    up(&_g_wtm_sem);
}

static uint8_t _g_wrap_subk_kad[WTM_KEY_AUTH_DATA_SZ] = 
{
    0xaa, 0xaa, 0xaa, 0xaa,
    0x55, 0x55, 0x55, 0x55,
    0xaa, 0xaa, 0xaa, 0xaa,
    0x55, 0x55, 0x55, 0x55,
};

int32_t mv_wtm_func_xllp_init(void)
{
    uint32_t tsr;

    sema_init(&_g_wtm_sem, 0);

    kthread_run(_wtm_monitor, NULL, "_wtm_monitor");

    init_timer(&_g_wtm_timer);
    _g_wtm_timer.expires = jiffies + 2 * HZ;
    _g_wtm_timer.function = &_wtm_init_timeout;
    add_timer(&_g_wtm_timer);

    if (_mv_wtm_func_chk_fw()) {
        mv_wtm_func_pr("WARNING - wtm firmware not ready\n");
        return -1;
    }

#ifdef CONFIG_EN_FIPS

    (void)_mv_wtm_func_xllp_fips_init();

#endif

    MV_WTM_FUNC_SET_0_PARAM;
    MV_WTM_FUNC_SET_CMD(WTM_GET_TRUST_STATUS_REGISTER);
    MV_WTM_FUNC_WAIT_FOR_IRQ;
    MV_WTM_FUNC_READ_1_RES(&tsr);

    del_timer_sync(&_g_wtm_timer);
    /* stop the monitor thread */
    up(&_g_wtm_sem);

    if ((WTM_TSR_RKEK_PROVED == (tsr & WTM_TSR_RKEK_PROV_MASK)) ||
            (WTM_TSR_NONFIPS_ONLY == (tsr & WTM_TSR_NONFIPS_ONLY_MASK))) {
        if ((WTM_TSR_OP_STATE_FIPS != (tsr & WTM_TSR_OP_STATUS_MASK)) &&
            (WTM_TSR_OP_STATE_ACC != (tsr & WTM_TSR_OP_STATUS_MASK))){
            MV_WTM_FUNC_SET_0_PARAM;
            MV_WTM_FUNC_SET_CMD(WTM_RESET);
            MV_WTM_FUNC_WAIT_FOR_IRQ;

            MV_WTM_FUNC_SET_0_PARAM;
            MV_WTM_FUNC_SET_CMD(WTM_INIT);
            MV_WTM_FUNC_WAIT_FOR_IRQ;
        }

        /* launch an init mode: non-fips */
        MV_WTM_FUNC_SET_1_PARAM(0);
        MV_WTM_FUNC_SET_CMD(WTM_SWITCH_FIPS_MODE);
        MV_WTM_FUNC_WAIT_FOR_IRQ;
        g_wtm_func_drv.mode = 0;
    } else {
        if ((WTM_TSR_OP_STATE_FIPS != (tsr & WTM_TSR_OP_STATUS_MASK)) &&
            (WTM_TSR_OP_STATE_ACC != (tsr & WTM_TSR_OP_STATUS_MASK))){
            MV_WTM_FUNC_SET_0_PARAM;
            MV_WTM_FUNC_SET_CMD(WTM_RESET);
            MV_WTM_FUNC_WAIT_FOR_IRQ;

            MV_WTM_FUNC_SET_0_PARAM;
            MV_WTM_FUNC_SET_CMD(WTM_INIT);
            MV_WTM_FUNC_WAIT_FOR_IRQ;

            /* VIRGIN --> DM, ready for prov */
            MV_WTM_FUNC_SET_0_PARAM;
            MV_WTM_FUNC_SET_CMD(WTM_LIFECYCLE_ADVANCE);
            MV_WTM_FUNC_WAIT_FOR_IRQ;
        }
    }

    if (WTM_TSR_RKEK_PROVED == (tsr & WTM_TSR_RKEK_PROV_MASK)) {
        mv_wtm_func_xllp_create_wrap_subk((uint8_t *)_g_wrap_subk_kad);
    }

    return 0;
}

void mv_wtm_func_xllp_cleanup(void)
{
#ifdef CONFIG_EN_FIPS

    _mv_wtm_func_xllp_fips_cleanup();

#endif
}

