/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_ioctl.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The shared header file of IOCTL argument structure definition
 *                between driver and middleware.
 *
 */

#ifndef _MV_WTM_IOCTL_H_
#define _MV_WTM_IOCTL_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_types.h"
#include "mv_wtm_err.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************
 *          MACROS
 ******************************
 */

#define _WTM_MAGIC                              'w'

#define MV_WTM_IOCTL_HASH_INIT_NONFIPS          _WTM_IO(_WTM_MAGIC, 0x10)
#define MV_WTM_IOCTL_HASH_INIT_FIPS             _WTM_IO(_WTM_MAGIC, 0x11)
#define MV_WTM_IOCTL_HASH_UPDATE                _WTM_IO(_WTM_MAGIC, 0x12)
#define MV_WTM_IOCTL_HASH_FINAL                 _WTM_IO(_WTM_MAGIC, 0x13)
#define MV_WTM_IOCTL_HASH_DGST_NONFIPS          _WTM_IO(_WTM_MAGIC, 0x14)
#define MV_WTM_IOCTL_HASH_DGST_FIPS             _WTM_IO(_WTM_MAGIC, 0x15)

#define MV_WTM_IOCTL_SYM_INIT_NOFIPS            _WTM_IO(_WTM_MAGIC, 0x20)
#define MV_WTM_IOCTL_SYM_INIT_FIPS              _WTM_IO(_WTM_MAGIC, 0x21)
#define MV_WTM_IOCTL_SYM_PROC                   _WTM_IO(_WTM_MAGIC, 0x22)
#define MV_WTM_IOCTL_SYM_FINAL                  _WTM_IO(_WTM_MAGIC, 0x23)

#define MV_WTM_IOCTL_PKCS_INIT_NONFIPS          _WTM_IO(_WTM_MAGIC, 0x30)
#define MV_WTM_IOCTL_PKCS_INIT_FIPS             _WTM_IO(_WTM_MAGIC, 0x31)
#define MV_WTM_IOCTL_PKCS_UPDATE                _WTM_IO(_WTM_MAGIC, 0x32)
#define MV_WTM_IOCTL_PKCS_FINAL                 _WTM_IO(_WTM_MAGIC, 0x33)
#define MV_WTM_IOCTL_PKCS_OP_NONFIPS            _WTM_IO(_WTM_MAGIC, 0x34)
#define MV_WTM_IOCTL_PKCS_OP_FIPS               _WTM_IO(_WTM_MAGIC, 0x35)

#define MV_WTM_IOCTL_EC_DSA_INIT_NONFIPS        _WTM_IO(_WTM_MAGIC, 0x40)
#define MV_WTM_IOCTL_EC_DSA_INIT_FIPS           _WTM_IO(_WTM_MAGIC, 0x41)
#define MV_WTM_IOCTL_EC_DSA_UPDATE              _WTM_IO(_WTM_MAGIC, 0x42)
#define MV_WTM_IOCTL_EC_DSA_FINAL               _WTM_IO(_WTM_MAGIC, 0x43)
#define MV_WTM_IOCTL_EC_DSA_OP_NONFIPS          _WTM_IO(_WTM_MAGIC, 0x44)
#define MV_WTM_IOCTL_EC_DSA_OP_FIPS             _WTM_IO(_WTM_MAGIC, 0x45)

#define MV_WTM_IOCTL_CREATE_EC_KEY_PAIR         _WTM_IO(_WTM_MAGIC, 0x50)
#define MV_WTM_IOCTL_DERIVATE_EC_PUB_KEY        _WTM_IO(_WTM_MAGIC, 0x51)
#define MV_WTM_IOCTL_GEN_DH_SYS_ARGS            _WTM_IO(_WTM_MAGIC, 0x52)
#define MV_WTM_IOCTL_CREATE_DH_KEY_PAIR         _WTM_IO(_WTM_MAGIC, 0x53)
#define MV_WTM_IOCTL_DERIVATE_DH_PUB_KEY        _WTM_IO(_WTM_MAGIC, 0x54)
#define MV_WTM_IOCTL_CREATE_RSA_KEY_PAIR        _WTM_IO(_WTM_MAGIC, 0x55)
#define MV_WTM_IOCTL_CREATE_SUBKEY              _WTM_IO(_WTM_MAGIC, 0x56)
#define MV_WTM_IOCTL_ENROLL_SYM_USRKEY          _WTM_IO(_WTM_MAGIC, 0x57)
#define MV_WTM_IOCTL_ENROLL_EC_USRKEY           _WTM_IO(_WTM_MAGIC, 0x58)
#define MV_WTM_IOCTL_ENROLL_DH_USRKEY           _WTM_IO(_WTM_MAGIC, 0x59)
#define MV_WTM_IOCTL_ENROLL_RSA_USRKEY          _WTM_IO(_WTM_MAGIC, 0x5A)
#define MV_WTM_IOCTL_CREATE_SYM_USRKEY          _WTM_IO(_WTM_MAGIC, 0x5B)
#define MV_WTM_IOCTL_CREATE_EC_DH_SHRD_USRKEY   _WTM_IO(_WTM_MAGIC, 0x5C)
#define MV_WTM_IOCTL_CREATE_DH_SHRD_USRKEY      _WTM_IO(_WTM_MAGIC, 0x5D)
#define MV_WTM_IOCTL_PR_EC256_DEC               _WTM_IO(_WTM_MAGIC, 0x5E)
#define MV_WTM_IOCTL_CHECK_USRKEY               _WTM_IO(_WTM_MAGIC, 0x5F)

#define MV_WTM_IOCTL_HDCP_LOAD_KEY              _WTM_IO(_WTM_MAGIC, 0x60)

#define MV_WTM_IOCTL_DRBG_INIT                  _WTM_IO(_WTM_MAGIC, 0x70)
#define MV_WTM_IOCTL_DRBG_RESEED                _WTM_IO(_WTM_MAGIC, 0x71)
#define MV_WTM_IOCTL_DRBG_GEN                   _WTM_IO(_WTM_MAGIC, 0x72)

#define MV_WTM_IOCTL_RKEK_PROV                  _WTM_IO(_WTM_MAGIC, 0x90)
#define MV_WTM_IOCTL_EC521_DK                   _WTM_IO(_WTM_MAGIC, 0x91)
#define MV_WTM_IOCTL_PLAT_BIND                  _WTM_IO(_WTM_MAGIC, 0x92)
#define MV_WTM_IOCTL_PLAT_VERF                  _WTM_IO(_WTM_MAGIC, 0x93)
#define MV_WTM_IOCTL_BIND_JTAG_KEY              _WTM_IO(_WTM_MAGIC, 0x94)
#define MV_WTM_IOCTL_VERF_JTAG_KEY              _WTM_IO(_WTM_MAGIC, 0x95)
#define MV_WTM_IOCTL_DIS_JTAG                   _WTM_IO(_WTM_MAGIC, 0x96)
#define MV_WTM_IOCTL_TMP_DIS_FA                 _WTM_IO(_WTM_MAGIC, 0x97)
#define MV_WTM_IOCTL_RD_UNIQ_ID                 _WTM_IO(_WTM_MAGIC, 0x98)
#define MV_WTM_IOCTL_WR_PLAT_CONF               _WTM_IO(_WTM_MAGIC, 0x99)
#define MV_WTM_IOCTL_RD_PLAT_CONF               _WTM_IO(_WTM_MAGIC, 0x9A)
#define MV_WTM_IOCTL_WR_USB_ID                  _WTM_IO(_WTM_MAGIC, 0x9B)
#define MV_WTM_IOCTL_RD_USB_ID                  _WTM_IO(_WTM_MAGIC, 0x9C)
#define MV_WTM_IOCTL_HDCP_WRAP_KEY              _WTM_IO(_WTM_MAGIC, 0x9D)
#define MV_WTM_IOCTL_SET_FIPS_PERM              _WTM_IO(_WTM_MAGIC, 0x9E)
#define MV_WTM_IOCTL_SET_NONFIPS_PERM           _WTM_IO(_WTM_MAGIC, 0x9F)
#define MV_WTM_IOCTL_PROV_PRE_PROC              _WTM_IO(_WTM_MAGIC, 0xA0)
#define MV_WTM_IOCTL_PROV_POST_PROC             _WTM_IO(_WTM_MAGIC, 0xA1)
#define MV_WTM_IOCTL_PROV_OEM_TRANS_KEY         _WTM_IO(_WTM_MAGIC, 0xA2)
#define MV_WTM_IOCTL_BIND_PLAYREADY_KEY         _WTM_IO(_WTM_MAGIC, 0xA3)

#define MV_WTM_IOCTL_GET_TSR                    _WTM_IO(_WTM_MAGIC, 0xB0)
#define MV_WTM_IOCTL_LIFE_CYCLE_ADV             _WTM_IO(_WTM_MAGIC, 0xB1)
#define MV_WTM_IOCTL_LIFE_CYCLE_READ            _WTM_IO(_WTM_MAGIC, 0xB2)
#define MV_WTM_IOCTL_SOFT_VER_ADV               _WTM_IO(_WTM_MAGIC, 0xB3)
#define MV_WTM_IOCTL_SOFT_VER_READ              _WTM_IO(_WTM_MAGIC, 0xB4)
#define MV_WTM_IOCTL_KERNEL_VER_READ            _WTM_IO(_WTM_MAGIC, 0xB5)

#define MV_WTM_IOCTL_JTAG_GET_NONCE             _WTM_IO(_WTM_MAGIC, 0xC0)
#define MV_WTM_IOCTL_JTAG_CONF                  _WTM_IO(_WTM_MAGIC, 0xC1)

#define MV_WTM_MAGIC_SZ         (4)

/* overall max part-blk-sz */
/* WARNING: search __ALIGNED to fix all the values of this macro */
#define MV_WTM_MAX_PART_BLK_SZ  (128)

/* eccp max sz */
#define MV_WTM_MAX_ECCP_COEF_SZ (68)

/*
 ******************************
 *          TYPES
 ******************************
 */

typedef enum _mv_wtm_ioctl_sch
{
    /* same as wtm firmware definitions */
    MV_WTM_IOCTL_INVALID_SCHEME         = 0x00000000,
    MV_WTM_IOCTL_AES_ECB128             = 0x00008000,
    MV_WTM_IOCTL_AES_ECB192             = 0x00008002,
    MV_WTM_IOCTL_AES_ECB256             = 0x00008001,
    MV_WTM_IOCTL_AES_CBC128             = 0x00008004,
    MV_WTM_IOCTL_AES_CBC192             = 0x00008006,
    MV_WTM_IOCTL_AES_CBC256             = 0x00008005,
    MV_WTM_IOCTL_AES_CTR128             = 0x00008008,
    MV_WTM_IOCTL_AES_CTR192             = 0x0000800A,
    MV_WTM_IOCTL_AES_CTR256             = 0x00008009,
    MV_WTM_IOCTL_AES_XTS256             = 0x0000800C,
    MV_WTM_IOCTL_AES_XTS512             = 0x0000800D,
    MV_WTM_IOCTL_SHA1                   = 0x00009000,
    MV_WTM_IOCTL_SHA224                 = 0x00009020,
    MV_WTM_IOCTL_SHA256                 = 0x00009010,
    MV_WTM_IOCTL_SHA384                 = 0x00009050,
    MV_WTM_IOCTL_SHA512                 = 0x00009040,
    MV_WTM_IOCTL_MD5                    = 0x00009030,
    MV_WTM_IOCTL_HMAC_SHA1              = 0x00009080,
    MV_WTM_IOCTL_HMAC_SHA224            = 0x000090A0,
    MV_WTM_IOCTL_HMAC_SHA256            = 0x00009090,
    MV_WTM_IOCTL_HMAC_SHA384            = 0x000090D0,
    MV_WTM_IOCTL_HMAC_SHA512            = 0x000090C0,
    MV_WTM_IOCTL_HMAC_MD5               = 0x000090B0,
    MV_WTM_IOCTL_RSA_DH_1024            = 0x0000A001,
    MV_WTM_IOCTL_RSA_DH_2048            = 0x0000A002,
    MV_WTM_IOCTL_PKCSV1_SHA1_1024RSA    = 0x0000A100,
    MV_WTM_IOCTL_PKCSV1_SHA224_1024RSA  = 0x0000A120,
    MV_WTM_IOCTL_PKCSV1_SHA256_1024RSA  = 0x0000A110,
    MV_WTM_IOCTL_PKCSV1_SHA1_2048RSA    = 0x0000A200,
    MV_WTM_IOCTL_PKCSV1_SHA224_2048RSA  = 0x0000A220,
    MV_WTM_IOCTL_PKCSV1_SHA256_2048RSA  = 0x0000A210,
    MV_WTM_IOCTL_ECCP224_DH             = 0x0000B000,
    MV_WTM_IOCTL_ECCP256_DH             = 0x0000B100,
    MV_WTM_IOCTL_ECCP384_DH             = 0x0000B200,
    MV_WTM_IOCTL_ECCP521_DH             = 0x0000B300,
    MV_WTM_IOCTL_EC224_DSA_SHA1         = 0x0000B001,
    MV_WTM_IOCTL_EC224_DSA_SHA224       = 0x0000B021,
    MV_WTM_IOCTL_EC224_DSA_SHA256       = 0x0000B011,
    MV_WTM_IOCTL_EC224_DSA_SHA384       = 0x0000B051,
    MV_WTM_IOCTL_EC224_DSA_SHA512       = 0x0000B041,
    MV_WTM_IOCTL_EC256_DSA_SHA1         = 0x0000B101,
    MV_WTM_IOCTL_EC256_DSA_SHA224       = 0x0000B121,
    MV_WTM_IOCTL_EC256_DSA_SHA256       = 0x0000B111,
    MV_WTM_IOCTL_EC256_DSA_SHA384       = 0x0000B151,
    MV_WTM_IOCTL_EC256_DSA_SHA512       = 0x0000B141,
    MV_WTM_IOCTL_EC384_DSA_SHA1         = 0x0000B201,
    MV_WTM_IOCTL_EC384_DSA_SHA224       = 0x0000B221,
    MV_WTM_IOCTL_EC384_DSA_SHA256       = 0x0000B211,
    MV_WTM_IOCTL_EC384_DSA_SHA384       = 0x0000B251,
    MV_WTM_IOCTL_EC384_DSA_SHA512       = 0x0000B241,
    MV_WTM_IOCTL_EC521_DSA_SHA1         = 0x0000B301,
    MV_WTM_IOCTL_EC521_DSA_SHA224       = 0x0000B321,
    MV_WTM_IOCTL_EC521_DSA_SHA256       = 0x0000B311,
    MV_WTM_IOCTL_EC521_DSA_SHA384       = 0x0000B351,
    MV_WTM_IOCTL_EC521_DSA_SHA512       = 0x0000B341,
    MV_WTM_IOCTL_DES_ECB                = 0x0000C000,
    MV_WTM_IOCTL_DES_CBC                = 0x0000C001,
    MV_WTM_IOCTL_TDES_ECB               = 0x0000C002,
    MV_WTM_IOCTL_TDES_CBC               = 0x0000C003,
    MV_WTM_IOCTL_RC4                    = 0x0000D000,
} mv_wtm_ioctl_sch;

typedef struct _mv_wtm_ioctl_fips_key_info
{
    uint8_t     *kr_passwd;
    uint32_t    kr_passwd_sz;
    uint8_t     *usrk_passwd;
    uint32_t    usrk_passwd_sz;
    uint8_t     *subk_wof;
    uint32_t    subk_wof_sz;
    uint8_t     *usrk_wof;
    uint32_t    usrk_wof_sz;
} mv_wtm_ioctl_fips_key_info;

typedef struct _mv_wtm_ioctl_pb_info
{
    uint8_t     pb_buf[MV_WTM_MAX_PART_BLK_SZ << 1];
    uint8_t     *part_blk;
    uint32_t    part_blk_sz;
} mv_wtm_ioctl_pb_info;

/* +++ hash/HMAC +++ */

typedef struct _mv_wtm_ioctl_hash_cntx
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];
    uint32_t                    state;
    uint32_t                    seq_err_flag;

    mv_wtm_ioctl_sch            sch;

    /* key info */
    uint8_t                     *key;
    uint32_t                    key_sz;
    mv_wtm_ioctl_fips_key_info  fips_ki;

    /* input */
    mv_wtm_ioctl_pb_info        pb_info;
    uint8_t                     *src_blks;
    uint32_t                    src_blks_sz;

    /* output */
    uint8_t                     *hash_val;
    uint32_t                    hash_val_sz;

    /* ret val */
    uint32_t                    wtm_fault;

    /* cntx */
    uint32_t                    wtm_cntx;
} mv_wtm_ioctl_hash_cntx;

/* --- hash/HMAC --- */

/* +++ sym +++ */

/* same as hardware definition */
#define MV_WTM_IOCTL_SYM_OP_ENC         (0)
#define MV_WTM_IOCTL_SYM_OP_DEC         (1)
/* if res of enc and dec are the same */
#define MV_WTM_IOCTL_SYM_OP_DONT_CARE   (0)
typedef unsigned int mv_wtm_ioctl_sym_op;

typedef struct _mv_wtm_ioctl_sym_cntx
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];
    uint32_t                    state;
    uint32_t                    seq_err_flag;

    mv_wtm_ioctl_sch            sch;
    mv_wtm_ioctl_sym_op         op;

    /* key info */
    uint8_t                     *sym_key0;
    uint8_t                     *sym_key1;
    uint8_t                     *sym_key2;
    uint32_t                    sym_key_sz;
    mv_wtm_ioctl_fips_key_info  fips_ki;

    /* input */
    uint8_t                     *iv;
    uint32_t                    iv_sz;
    mv_wtm_ioctl_pb_info        pb_info;    /* pb valid only in final */
                                            /* appendix of src msg */
    uint8_t                     *src_blks;
    uint32_t                    src_blks_sz;

    /* output */
    uint8_t                     *dst_blks;
    uint32_t                    dst_blks_sz;    /* src_sz in proc */
                                                /* src_sz + part_sz in final */

    /* ret val */
    uint32_t                    wtm_fault;

    /* cntx */
    uint32_t                    wtm_cntx;
} mv_wtm_ioctl_sym_cntx;

/* --- sym --- */

/* +++ PKCS +++ */

/* same as hardware definition */
typedef enum _mv_wtm_ioctl_pkcs_op
{
    MV_WTM_PKCS_OP_VERF,
    MV_WTM_PKCS_OP_SIGN,
} mv_wtm_ioctl_pkcs_op;

typedef struct _mv_wtm_ioctl_pkcs_cntx
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];
    uint32_t                    state;
    uint32_t                    seq_err_flag;

    mv_wtm_ioctl_sch            sch;
    mv_wtm_ioctl_pkcs_op        op;

    /* key info */
    uint8_t                     *key_mod;
    uint8_t                     *key_exp;
    uint32_t                    key_sz;
    mv_wtm_ioctl_fips_key_info  fips_ki;

    /* input */
    mv_wtm_ioctl_pb_info        pb_info;
    uint8_t                     *src_blks;
    uint32_t                    src_blks_sz;

    /* in/output */
    uint8_t                     *sig;       /* in if op = 0, out if op = 1 */
    uint32_t                    sig_sz;

    /* output */
    bool                        is_valid;   /* valid when op = 0 */

    /* ret val */
    uint32_t                    wtm_fault;

    /* cntx */
    uint32_t                    wtm_cntx;
} mv_wtm_ioctl_pkcs_cntx;

/* --- PKCS --- */

/* +++ EC_DSA +++ */

/* same as hardware definition */
typedef enum _mv_wtm_ioctl_ec_dsa_op
{
    MV_WTM_EC_DSA_OP_VERF,
    MV_WTM_EC_DSA_OP_SIGN,
} mv_wtm_ioctl_ec_dsa_op;

typedef struct _mv_wtm_eccp_pub_key_info {
    uint32_t    sz_in_bits;
    uint8_t     x[MV_WTM_MAX_ECCP_COEF_SZ];
    uint8_t     y[MV_WTM_MAX_ECCP_COEF_SZ];
} mv_wtm_eccp_pub_key_info;

typedef struct _mv_wtm_eccp_domain_param {
    uint32_t    sz_in_bits;
    uint32_t    cofactor;
    uint8_t     ecc[MV_WTM_MAX_ECCP_COEF_SZ];
    uint8_t     n[MV_WTM_MAX_ECCP_COEF_SZ];
    uint8_t     a[MV_WTM_MAX_ECCP_COEF_SZ];
    uint8_t     b[MV_WTM_MAX_ECCP_COEF_SZ];
    uint8_t     g_x[MV_WTM_MAX_ECCP_COEF_SZ];
    uint8_t     g_y[MV_WTM_MAX_ECCP_COEF_SZ];
} mv_wtm_eccp_domain_param;

typedef enum _mv_wtm_ioctl_ec_dsa_mode
{
    MV_WTM_EC_DSA_MODE_RANDOM,
    MV_WTM_EC_DSA_MODE_FIX,
} mv_wtm_ioctl_ec_dsa_mode;

typedef struct _mv_wtm_ioctl_ec_dsa_cntx
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];
    uint32_t                    state;
    uint32_t                    seq_err_flag;

    mv_wtm_ioctl_sch            sch;
    mv_wtm_ioctl_ec_dsa_op      op;

    /* key info */
    mv_wtm_eccp_pub_key_info    *pub_key;
    uint8_t                     *prv_key;
    uint32_t                    prv_key_sz;
    mv_wtm_ioctl_fips_key_info  fips_ki;

    /* input */
    mv_wtm_eccp_domain_param    *domain_param;
    mv_wtm_ioctl_ec_dsa_mode    mode;
    mv_wtm_ioctl_pb_info        pb_info;
    uint8_t                     *src_blks;
    uint32_t                    src_blks_sz;

    /* in/output */
    uint8_t                     *sig_r;     /* in if op = 0, out if op = 1 */
    uint8_t                     *sig_s;     /* in if op = 0, out if op = 1 */
    uint32_t                    sig_sz;

    /* output */
    bool                        is_valid;   /* valid when op = 0 */

    /* ret val */
    uint32_t                    wtm_fault;

    /* cntx */
    uint32_t                    wtm_cntx;
} mv_wtm_ioctl_ec_dsa_cntx;

/* --- EC_DSA --- */

/* +++ KM +++ */

/* create normal key */
typedef struct _mv_wtm_ioctl_create_key_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    bool                        is_stage_dd;
    mv_wtm_ioctl_fips_key_info  endk_info;  /* valid in dd */
    mv_wtm_ioctl_sch            ksch;
    uint32_t                    key_id;     /* input */

    /* fips_ki->usrk_passwd(_sz) is valid when creating usrkey */
    /* fips_ki->subk_wof(_sz) is valid when creating usrkey */
    mv_wtm_ioctl_fips_key_info  fips_ki;    /* in/output */

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_create_key_param;

typedef struct _mv_wtm_ioctl_create_ec_key_pair_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    bool                        is_stage_dd;
    mv_wtm_ioctl_fips_key_info  endk_info;  /* valid in fips, dd */
    mv_wtm_ioctl_sch            ksch;
    uint32_t                    key_id;

    bool                        is_nonfips;
    uint8_t                     *prv_key;   /* valid in non-FIPS */
    uint32_t                    prv_key_sz; /* valid in non-FIPS */
    mv_wtm_eccp_pub_key_info    *pub_key;   /* valid in non-FIPS/FIPS */
    mv_wtm_ioctl_fips_key_info  fips_ki;    /* valid in FIPS */

    mv_wtm_eccp_domain_param    *domain_param;
    mv_wtm_ioctl_ec_dsa_mode    mode;

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_create_ec_key_pair_param;

typedef struct _mv_wtm_ioctl_derivate_ec_pub_key_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    mv_wtm_ioctl_sch            ksch;

    bool                        is_nonfips;
    uint8_t                     *prv_key;   /* valid in non-FIPS */
    uint32_t                    prv_key_sz; /* valid in non-FIPS */
    mv_wtm_eccp_pub_key_info    *pub_key;   /* valid in non-FIPS/FIPS */
    mv_wtm_ioctl_fips_key_info  fips_ki;    /* valid in FIPS */

    mv_wtm_eccp_domain_param    *domain_param;
    mv_wtm_ioctl_ec_dsa_mode    mode;

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_derivate_ec_pub_key_param;

typedef struct _mv_wtm_ioctl_gen_dh_sys_args_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    mv_wtm_ioctl_sch            ksch;

    uint8_t                     *p;
    uint32_t                    p_sz;
    uint8_t                     *g;
    uint32_t                    g_sz;

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_gen_dh_sys_args_param;

typedef struct _mv_wtm_ioctl_create_dh_key_pair_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    bool                        is_stage_dd;
    mv_wtm_ioctl_fips_key_info  endk_info;  /* valid in fips, dd */
    mv_wtm_ioctl_sch            ksch;
    uint32_t                    key_id;

    bool                        is_nonfips;
    uint8_t                     *prv_key;   /* valid in non-FIPS */
    uint32_t                    prv_key_sz; /* valid in non-FIPS */
    uint8_t                     *pub_key;   /* valid in non-FIPS/FIPS */
    uint32_t                    pub_key_sz; /* valid in non-FIPS/FIPS */
    mv_wtm_ioctl_fips_key_info  fips_ki;    /* valid in FIPS */

    uint8_t                     *p;
    uint32_t                    p_sz;
    uint8_t                     *g;
    uint32_t                    g_sz;

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_create_dh_key_pair_param;

typedef struct _mv_wtm_ioctl_derivate_dh_pub_key_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    mv_wtm_ioctl_sch            ksch;

    bool                        is_nonfips;
    uint8_t                     *prv_key;   /* valid in non-FIPS */
    uint32_t                    prv_key_sz; /* valid in non-FIPS */
    uint8_t                     *pub_key;   /* valid in non-FIPS/FIPS */
    uint32_t                    pub_key_sz;
    mv_wtm_ioctl_fips_key_info  fips_ki;    /* valid in FIPS */

    uint8_t                     *p;
    uint32_t                    p_sz;
    uint8_t                     *g;
    uint32_t                    g_sz;

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_derivate_dh_pub_key_param;

typedef struct _mv_wtm_ioctl_create_rsa_key_pair_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    bool                        is_stage_dd;
    mv_wtm_ioctl_fips_key_info  endk_info;  /* valid in fips, dd */
    mv_wtm_ioctl_sch            ksch;
    uint32_t                    key_id;

    bool                        is_nonfips;
    uint8_t                     *prv_key;   /* valid in non-FIPS */
    uint32_t                    prv_key_sz; /* valid in non-FIPS */
    uint8_t                     *pub_key;   /* valid in non-FIPS/FIPS */
    uint32_t                    pub_key_sz; /* valid in non-FIPS/FIPS */
    uint8_t                     *key_mod;   /* valid in non-FIPS/FIPS */
    uint32_t                    key_mod_sz; /* valid in non-FIPS/FIPS */
    mv_wtm_ioctl_fips_key_info  fips_ki;    /* valid in FIPS */

    uint32_t                    trials;
    uint32_t                    limit;

    uint8_t                     *p;         /* output */
    uint32_t                    p_sz;       /* output */
    uint8_t                     *q;         /* output */
    uint32_t                    q_sz;       /* output */

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_create_rsa_key_pair_param;

typedef struct _mv_wtm_ioctl_enroll_key_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    mv_wtm_ioctl_sch            ksch;
    uint32_t                    key_id;

    uint8_t                     *usrk;
    uint32_t                    usrk_sz;
    /* output: fips_ki->usrk_wof/_sz, others are input */
    mv_wtm_ioctl_fips_key_info  fips_ki;

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_enroll_key_param;

typedef mv_wtm_ioctl_enroll_key_param   mv_wtm_ioctl_enroll_ec_key_pair_param;

typedef mv_wtm_ioctl_enroll_key_param   mv_wtm_ioctl_enroll_dh_key_pair_param;

typedef mv_wtm_ioctl_enroll_key_param   mv_wtm_ioctl_enroll_rsa_key_pair_param;

typedef struct _mv_wtm_ioctl_create_ec_dh_shared_key_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    bool                        is_nonfips;

    /* non-FIPS and FIPS */
    uint32_t                    ec_dh_ksch;     /* ec_dh_key_sch */
    mv_wtm_eccp_pub_key_info    *peer_pub_key;  /* peer's pub_key */

    /* non-FIPS */
    uint8_t                     *prv_key;
    uint32_t                    prv_key_sz;
    uint8_t                     *shrd_key;      /* in/output */
    uint32_t                    shrd_key_sz;

    /* FIPS */
    mv_wtm_ioctl_fips_key_info  ec_dh_fips_ki;  /* input */
    mv_wtm_ioctl_sch            ksch;           /* sym_key_sch */
    uint32_t                    key_id;         /* input */
    mv_wtm_ioctl_fips_key_info  fips_ki;        /* in/output */

    /* non-FIPS */
    mv_wtm_eccp_domain_param    *domain_param;  /* NULL in FIPS */
    mv_wtm_ioctl_ec_dsa_mode    mode;           /* random mode in FIPS */

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_create_ec_dh_shared_key_param;

typedef struct _mv_wtm_ioctl_create_dh_shared_key_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    bool                        is_nonfips;

    /* non-FIPS and FIPS */
    uint32_t                    dh_ksch;
    uint8_t                     *peer_pub_key;      /* input */
    uint32_t                    peer_pub_key_sz;

    /* non-FIPS */
    uint8_t                     *prv_key;
    uint32_t                    prv_key_sz;
    uint8_t                     *shrd_key;          /* in/output */
    uint32_t                    shrd_key_sz;

    /* FIPS */
    mv_wtm_ioctl_fips_key_info  dh_fips_ki;         /* input */
    mv_wtm_ioctl_sch            ksch;               /* sym_key_sch */
    uint32_t                    key_id;             /* input */
    mv_wtm_ioctl_fips_key_info  fips_ki;            /* in/output */

    /* non-FIPS and FIPS */
    /* g is not required in creating dh shared key */
    uint8_t                     *p;
    uint32_t                    p_sz;

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_create_dh_shared_key_param;

typedef struct _mv_wtm_ioctl_pr_ec256_decrypt_keys_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    mv_wtm_ioctl_fips_key_info  ec256_ki;       /* input ec256 prv key */

    mv_wtm_eccp_pub_key_info    *ec_point0;     /* input cipher key 0 */
    mv_wtm_eccp_pub_key_info    *ec_point1;     /* input cipher key 1 */

    uint32_t                    sym_ksch_lsb;   /* input LSB ksch */
    uint32_t                    sym_key_id_lsb; /* input LSB sym key id */
    mv_wtm_ioctl_fips_key_info  sym_ki_lsb;     /* in/output LSB sym key info */
    uint32_t                    sym_ksch_msb;   /* input MSB ksch */
    uint32_t                    sym_key_id_msb; /* input MSB sym key id */
    mv_wtm_ioctl_fips_key_info  sym_ki_msb;     /* in/output MSB sym key info */

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_pr_ec256_decrypt_keys_param;

typedef struct _mv_wtm_ioctl_check_key_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    bool                        is_stage_dd;
    mv_wtm_ioctl_fips_key_info  endk_info;  /* valid in dd */
    mv_wtm_ioctl_sch            ksch;

    /* fips_ki->usrk_passwd(_sz) is valid when checking usrkey */
    /* fips_ki->subk_wof(_sz) is valid when checking usrkey */
    mv_wtm_ioctl_fips_key_info  fips_ki;    /* input */

    bool                        is_valid;

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_check_key_param;

/* --- KM --- */

/* +++ HDCP +++ */

typedef struct _mv_wtm_ioctl_hdcp_load_key_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint8_t     *wrapped_key;
    uint32_t    wrapped_key_sz;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_hdcp_load_key_param;

/* --- HDCP --- */

/* +++ RAN NUM +++ */

typedef struct _mv_wtm_ioctl_drbg_init_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    wtm_fault;
} mv_wtm_ioctl_drbg_init_param;

typedef struct _mv_wtm_ioctl_drbg_reseed_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint8_t     *seed;
    uint32_t    seed_sz;    /* 16, 24 or 32 */

    uint32_t    wtm_fault;
} mv_wtm_ioctl_drbg_reseed_param;

typedef struct _mv_wtm_ioctl_drbg_gen_ran_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint8_t     *ran;
    uint32_t    ran_sz;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_drbg_gen_ran_param;

/* --- RAN NUM --- */

/* +++ PROV +++ */

typedef enum _mv_wtm_ioctl_rkek_prov_mode
{
    MV_WTM_IOCTL_RKEK_PROV_CLIENT,
    MV_WTM_IOCTL_RKEK_PROV_GEN,
    MV_WTM_IOCTL_RKEK_PROV_GEN_RESEED,
} mv_wtm_ioctl_rkek_prov_mode;

typedef struct _mv_wtm_ioctl_rkek_prov_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    mv_wtm_ioctl_rkek_prov_mode mode;
    uint8_t                     *rkek;
    uint32_t                    rkek_sz;
    uint8_t                     *seed;
    uint32_t                    seed_sz;

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_rkek_prov_param;

typedef enum _mv_wtm_ioctl_ec521_dk_mode
{
    MV_WTM_IOCTL_EC521_DK_CLIENT,
    MV_WTM_IOCTL_EC521_DK_GEN,
    MV_WTM_IOCTL_EC521_DK_GEN_RESEED,
} mv_wtm_ioctl_ec521_dk_mode;

typedef struct _mv_wtm_ioctl_ec521_dk_prov_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    mv_wtm_ioctl_ec521_dk_mode  mode;
    uint8_t                     *key_msb;
    uint8_t                     *key_lsb;
    uint32_t                    key_sz;
    uint8_t                     *seed;
    uint32_t                    seed_sz;

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_ec521_dk_prov_param;

typedef struct _mv_wtm_ioctl_plat_bind_param
{
    uint8_t             magic[MV_WTM_MAGIC_SZ];

    mv_wtm_ioctl_sch    sch;
    uint8_t             *key0;
    uint8_t             *key1;
    uint32_t            key_sz;

    uint32_t            wtm_fault;
} mv_wtm_ioctl_plat_bind_param;

typedef struct _mv_wtm_ioctl_plat_verf_param
{
    uint8_t             magic[MV_WTM_MAGIC_SZ];

    mv_wtm_ioctl_sch    sch;
    uint8_t             *key0;
    uint8_t             *key1;
    uint32_t            key_sz;

    bool                is_valid;

    uint32_t            wtm_fault;
} mv_wtm_ioctl_plat_verf_param;

typedef struct _mv_wtm_ioctl_bind_jtag_key_param
{
    uint8_t             magic[MV_WTM_MAGIC_SZ];

    mv_wtm_ioctl_sch    sch;
    uint8_t             *key0;
    uint8_t             *key1;
    uint32_t            key_sz;

    uint32_t            wtm_fault;
} mv_wtm_ioctl_bind_jtag_key_param;

typedef struct _mv_wtm_ioctl_verf_jtag_key_param
{
    uint8_t             magic[MV_WTM_MAGIC_SZ];

    mv_wtm_ioctl_sch    sch;
    uint8_t             *key0;
    uint8_t             *key1;
    uint32_t            key_sz;

    bool                is_valid;

    uint32_t            wtm_fault;
} mv_wtm_ioctl_verf_jtag_key_param;

typedef struct _mv_wtm_ioctl_dis_jtag_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    wtm_fault;
} mv_wtm_ioctl_dis_jtag_param;

typedef struct _mv_wtm_ioctl_tmp_dis_fa_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    wtm_fault;
} mv_wtm_ioctl_tmp_dis_fa_param;

typedef struct _mv_wtm_ioctl_rd_uniq_id_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint8_t     *id;
    uint32_t    id_sz;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_rd_uniq_id_param;

typedef struct _mv_wtm_ioctl_wr_plat_conf_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint8_t     *conf;
    uint32_t    conf_sz;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_wr_plat_conf_param;

typedef struct _mv_wtm_ioctl_rd_plat_conf_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint8_t     *conf;
    uint32_t    conf_sz;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_rd_plat_conf_param;

typedef struct _mv_wtm_ioctl_wr_usb_id_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint8_t     *id;
    uint32_t    id_sz;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_wr_usb_id_param;

typedef struct _mv_wtm_ioctl_rd_usb_id_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint8_t     *id;
    uint32_t    id_sz;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_rd_usb_id_param;

typedef struct _mv_wtm_ioctl_hdcp_wrap_key_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint8_t     *key_set;
    uint32_t    key_set_sz;     /* 280B */
    uint8_t     *ksv;
    uint32_t    ksv_sz;         /* 5B */

    uint8_t     *wrapped_key;
    uint32_t    wrapped_key_sz; /* 85B */

    uint32_t    wtm_fault;
} mv_wtm_ioctl_hdcp_wrap_key_param;

typedef struct _mv_wtm_ioctl_prov_set_fips_perm_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    wtm_fault;
} mv_wtm_ioctl_prov_set_fips_perm_param;

typedef struct _mv_wtm_ioctl_prov_set_nonfips_perm_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    wtm_fault;
} mv_wtm_ioctl_prov_set_nonfips_perm_param;

typedef struct _mv_wtm_ioctl_prov_pre_proc_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    wtm_fault;
} mv_wtm_ioctl_prov_pre_proc_param;

typedef struct _mv_wtm_ioctl_prov_post_proc_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];

    uint32_t                    stage; /* 0 - DM 1 - DD */
    mv_wtm_ioctl_fips_key_info  wrap_subk_fips_ki;

    uint32_t                    wtm_fault;
} mv_wtm_ioctl_prov_post_proc_param;

typedef enum _mv_wtm_ioctl_prov_trans_key
{
    MV_WTM_IOCTL_TRANSKEY_PLAINTEXT,
    MV_WTM_IOCTL_TEANSKEY_AES128,
    MV_WTM_IOCTL_TEANSKEY_AES256,
} mv_wtm_ioctl_prov_trans_key;

typedef struct _mv_wtm_ioctl_trans_key_prov_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];
    mv_wtm_ioctl_sch            ksch;
    mv_wtm_ioctl_prov_trans_key type;
    uint8_t                     *trans_key;
    uint32_t                    trans_key_sz;
    uint32_t                    wtm_fault;
} mv_wtm_ioctl_trans_key_prov_param;

typedef struct _mv_wtm_ioctl_bind_PlayReady_key_param
{
    uint8_t                     magic[MV_WTM_MAGIC_SZ];
    mv_wtm_ioctl_sch            trans_ksch;
    mv_wtm_ioctl_sch            pr_ksch;
    uint32_t                    key_id;
    uint32_t                    byte_swap;
    uint8_t                     *aes_iv;
    uint32_t                    aes_iv_sz;
    uint8_t                     *usrk;
    uint32_t                    usrk_sz;
    mv_wtm_ioctl_fips_key_info  fips_ki;
    uint32_t                    wtm_fault;
} mv_wtm_ioctl_bind_PlayReady_key_param;

/* --- PROV --- */

/* +++ STAT MGMT +++ */

typedef struct _mv_wtm_ioctl_get_tsr_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    tsr;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_get_tsr_param;

typedef struct _mv_wtm_ioctl_life_cycle_adv_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    wtm_fault;
} mv_wtm_ioctl_life_cycle_adv_param;

typedef struct _mv_wtm_ioctl_life_cycle_read_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    life_cycle;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_life_cycle_read_param;

typedef struct _mv_wtm_ioctl_soft_ver_adv_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    wtm_fault;
} mv_wtm_ioctl_soft_ver_adv_param;

typedef struct _mv_wtm_ioctl_soft_ver_read_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    ver;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_soft_ver_read_param;

typedef struct _mv_wtm_ioctl_kernel_ver_read_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    ver;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_kernel_ver_read_param;

/* --- STAT MGMT --- */

/* +++ JTAG +++ */

typedef struct _mv_wtm_ioctl_jtag_get_nonce_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint8_t     *nonce;
    uint32_t    nonce_sz;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_jtag_get_nonce_param;

typedef struct _mv_wtm_ioctl_jtag_conf_param
{
    uint8_t     magic[MV_WTM_MAGIC_SZ];

    uint32_t    sch;

    uint8_t     *pubk0;
    uint8_t     *pubk1;
    uint32_t    pubk_sz;

    uint32_t    ctrl;
    uint8_t     *sig;
    uint32_t    sig_sz;

    uint32_t    is_valid;

    uint32_t    wtm_fault;
} mv_wtm_ioctl_jtag_conf_param;

/* --- JTAG --- */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#ifdef __cplusplus
}
#endif

#endif /* _MV_WTM_IOCTL_H_ */

