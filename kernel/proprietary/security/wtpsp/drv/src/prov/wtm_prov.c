/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_prov.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of WTM provision functions
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_ioctl.h"
#include "mv_wtm_func.h"

#include "wtm_main.h"
#include "wtm_os_util.h"

#include "_wtm_prov.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t wtm_prov_rkek_prov(void *arg)
{
    mv_wtm_ioctl_rkek_prov_param *p;
    wtm_vm p_vm = NULL;
    uint32_t delta = WTM_PAGE_SIZE >> 1;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_rkek_prov_param, arg,
            sizeof(mv_wtm_ioctl_rkek_prov_param),
            p, p_vm);

    if (MV_WTM_IOCTL_RKEK_PROV_CLIENT == p->mode) {
        wtm_copy_from_user(
                g_wtm_drv.key_buf_virt, (uint32_t)p->rkek, p->rkek_sz);
    }

    if (MV_WTM_IOCTL_RKEK_PROV_GEN_RESEED == p->mode) {
        wtm_copy_from_user(
                g_wtm_drv.key_buf_virt + delta, (uint32_t)p->seed, p->seed_sz);
    }

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_rkek_prov(ss, p->mode,
            g_wtm_drv.key_buf_phys, p->rkek_sz,
            g_wtm_drv.key_buf_phys + delta);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_ec521_dk_prov(void *arg)
{
    mv_wtm_ioctl_ec521_dk_prov_param *p;
    wtm_vm p_vm = NULL;
    uint32_t delta = WTM_PAGE_SIZE >> 2;
    uint32_t half_key_sz;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_ec521_dk_prov_param, arg,
            sizeof(mv_wtm_ioctl_ec521_dk_prov_param),
            p, p_vm);

    if (MV_WTM_IOCTL_EC521_DK_CLIENT == p->mode) {
        half_key_sz = p->key_sz >> 1;

        wtm_copy_from_user(g_wtm_drv.key_buf_virt,
                (uint32_t)p->key_msb, half_key_sz);
        wtm_copy_from_user(g_wtm_drv.key_buf_virt + delta,
                (uint32_t)p->key_lsb, half_key_sz);
    }

    if (MV_WTM_IOCTL_EC521_DK_GEN_RESEED == p->mode) {
        wtm_copy_from_user(g_wtm_drv.key_buf_virt + (delta << 1),
                (uint32_t)p->seed, p->seed_sz);
    }

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_ec521_dk_prov(ss, p->mode,
            g_wtm_drv.key_buf_phys, g_wtm_drv.key_buf_phys + delta,
            p->key_sz, g_wtm_drv.key_buf_phys + (delta << 1), p->seed_sz);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_plat_bind(void *arg)
{
    mv_wtm_ioctl_plat_bind_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_plat_bind_param, arg,
            sizeof(mv_wtm_ioctl_plat_bind_param),
            p, p_vm);

    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt,
            (uint32_t)p->key0, p->key_sz);
    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt + (WTM_PAGE_SIZE >> 1),
            (uint32_t)p->key1, p->key_sz);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_plat_bind(ss, p->sch,
            g_wtm_drv.key_buf_phys,
            g_wtm_drv.key_buf_phys + (WTM_PAGE_SIZE >> 1),
            p->key_sz);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_plat_verf(void *arg)
{
    mv_wtm_ioctl_plat_verf_param *p;
    wtm_vm p_vm = NULL;

    bool is_valid;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_plat_verf_param, arg,
            sizeof(mv_wtm_ioctl_plat_verf_param),
            p, p_vm);

    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt,
            (uint32_t)p->key0, p->key_sz);
    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt + (WTM_PAGE_SIZE >> 1),
            (uint32_t)p->key1, p->key_sz);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_plat_verf(ss, p->sch,
            g_wtm_drv.key_buf_phys,
            g_wtm_drv.key_buf_phys + (WTM_PAGE_SIZE >> 1),
            p->key_sz,
            &is_valid);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    if (!ret) {
        p->is_valid = is_valid;
    }

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_bind_jtag_key(void *arg)
{
    mv_wtm_ioctl_bind_jtag_key_param *p;
    wtm_vm p_vm = NULL;
    uint32_t delta = WTM_PAGE_SIZE >> 1;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_bind_jtag_key_param, arg,
            sizeof(mv_wtm_ioctl_bind_jtag_key_param),
            p, p_vm);

    wtm_copy_from_user(g_wtm_drv.key_buf_virt,
            (uint32_t)p->key0, p->key_sz);
    wtm_copy_from_user(g_wtm_drv.key_buf_virt + delta,
            (uint32_t)p->key1, p->key_sz);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_bind_jtag_key(ss,
            p->sch, g_wtm_drv.key_buf_phys, g_wtm_drv.key_buf_phys + delta,
            p->key_sz);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_verf_jtag_key(void *arg)
{
    mv_wtm_ioctl_verf_jtag_key_param *p;
    wtm_vm p_vm = NULL;
    uint32_t delta = WTM_PAGE_SIZE >> 1;

    bool is_valid;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_verf_jtag_key_param, arg,
            sizeof(mv_wtm_ioctl_verf_jtag_key_param),
            p, p_vm);

    wtm_copy_from_user(g_wtm_drv.key_buf_virt,
            (uint32_t)p->key0, p->key_sz);
    wtm_copy_from_user(g_wtm_drv.key_buf_virt + delta,
            (uint32_t)p->key1, p->key_sz);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_verf_jtag_key(ss,
            p->sch,
            g_wtm_drv.key_buf_phys, g_wtm_drv.key_buf_phys + delta, p->key_sz,
            &is_valid);

    if (!ret) {
        p->is_valid = is_valid;
    }

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_dis_jtag(void *arg)
{
    mv_wtm_ioctl_dis_jtag_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_dis_jtag_param, arg,
            sizeof(mv_wtm_ioctl_dis_jtag_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_dis_jtag(ss);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_tmp_dis_fa(void *arg)
{
    mv_wtm_ioctl_tmp_dis_fa_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_tmp_dis_fa_param, arg,
            sizeof(mv_wtm_ioctl_tmp_dis_fa_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_tmp_dis_fa(ss);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_rd_uniq_id(void *arg)
{
    mv_wtm_ioctl_rd_uniq_id_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_rd_uniq_id_param, arg,
            sizeof(mv_wtm_ioctl_rd_uniq_id_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_rd_uniq_id(
            ss, g_wtm_drv.key_buf_phys, p->id_sz);

    mv_wtm_func_destroy_ss(ss);

    wtm_copy_to_user((uint32_t)p->id, g_wtm_drv.key_buf_virt, p->id_sz);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_wr_plat_conf(void *arg)
{
    mv_wtm_ioctl_wr_plat_conf_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_wr_plat_conf_param, arg,
            sizeof(mv_wtm_ioctl_wr_plat_conf_param),
            p, p_vm);

    wtm_copy_from_user(g_wtm_drv.key_buf_virt, (uint32_t)p->conf, p->conf_sz);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_wr_plat_conf(
            ss, g_wtm_drv.key_buf_phys, p->conf_sz);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_rd_plat_conf(void *arg)
{
    mv_wtm_ioctl_rd_plat_conf_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_rd_plat_conf_param, arg,
            sizeof(mv_wtm_ioctl_rd_plat_conf_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_rd_usb_id(
            ss, g_wtm_drv.key_buf_phys, p->conf_sz);

    mv_wtm_func_destroy_ss(ss);

    wtm_copy_to_user((uint32_t)p->conf, g_wtm_drv.key_buf_virt, p->conf_sz);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_wr_usb_id(void *arg)
{
    mv_wtm_ioctl_wr_usb_id_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_wr_usb_id_param, arg,
            sizeof(mv_wtm_ioctl_wr_usb_id_param),
            p, p_vm);

    wtm_copy_from_user(g_wtm_drv.key_buf_virt, (uint32_t)p->id, p->id_sz);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_wr_usb_id(
            ss, g_wtm_drv.key_buf_phys, p->id_sz);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_rd_usb_id(void *arg)
{
    mv_wtm_ioctl_rd_usb_id_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_rd_usb_id_param, arg,
            sizeof(mv_wtm_ioctl_rd_usb_id_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_rd_usb_id(
            ss, g_wtm_drv.key_buf_phys, p->id_sz);

    mv_wtm_func_destroy_ss(ss);

    wtm_copy_to_user((uint32_t)p->id, g_wtm_drv.key_buf_virt, p->id_sz);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_hdcp_wrap_key(void *arg)
{
    mv_wtm_ioctl_hdcp_wrap_key_param *p;
    wtm_vm p_vm = NULL;
    
    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_hdcp_wrap_key_param, arg,
            sizeof(mv_wtm_ioctl_hdcp_wrap_key_param),
            p, p_vm);

    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt + 280, (uint32_t)p->ksv, p->ksv_sz);
    /*3 bytes between ksv and key*/
    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt, (uint32_t)p->key_set, p->key_set_sz);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_hdcp_wrap_key(ss,
            g_wtm_drv.key_buf_phys,
            g_wtm_drv.key_buf_phys + 280,
            g_wtm_drv.out_buf_phys);

    mv_wtm_func_destroy_ss(ss);

    wtm_copy_to_user((uint32_t)p->wrapped_key,
            g_wtm_drv.out_buf_virt, p->wrapped_key_sz);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_set_fips_perm(void *arg)
{
    mv_wtm_ioctl_prov_set_fips_perm_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_prov_set_fips_perm_param, arg,
            sizeof(mv_wtm_ioctl_prov_set_fips_perm_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_set_fips_perm(ss);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_set_nonfips_perm(void *arg)
{
    mv_wtm_ioctl_prov_set_nonfips_perm_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_prov_set_nonfips_perm_param, arg,
            sizeof(mv_wtm_ioctl_prov_set_nonfips_perm_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_set_nonfips_perm(ss);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_pre_proc(void *arg)
{
    mv_wtm_ioctl_prov_pre_proc_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_prov_pre_proc_param, arg,
            sizeof(mv_wtm_ioctl_prov_pre_proc_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_pre_proc(ss);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_post_proc(void *arg)
{
    mv_wtm_ioctl_prov_post_proc_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_prov_post_proc_param, arg,
            sizeof(mv_wtm_ioctl_prov_post_proc_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_prov_post_proc(ss, p->stage, NULL, NULL);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_prov_oem_trans_key(void *arg)
{
    mv_wtm_ioctl_trans_key_prov_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_trans_key_prov_param, arg,
            sizeof(mv_wtm_ioctl_trans_key_prov_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt, (uint32_t)p->trans_key, p->trans_key_sz);

    ret = mv_wtm_func_prov_transit_key(ss, g_wtm_drv.key_buf_phys, p->ksch, p->type);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
        return 0;
}
