/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_main.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of WTM driver framework
 *                This file is os-specific
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/platform_device.h>

#include "mv_wtm_xllp.h"
#include "mv_wtm_func.h"

#include "wtm_types.h"
#include "wtm_main.h"
#include "wtm_crypto.h"
#include "wtm_prov.h"
#include "wtm_stat_mgmt.h"
#include "wtm_jtag.h"
#include "wtm_os_util.h"
#include "wtm_crypto_util.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

MODULE_LICENSE("GPL");

#ifdef CONFIG_DVFM

#include <mach/dvfm.h>

static int32_t dvfm_dev_idx;

#define SET_DSVL_CONSTRAINT     do {                                    \
                                    dvfm_disable_op_name("apps_idle",   \
                                            dvfm_dev_idx);              \
                                    dvfm_disable_op_name("apps_sleep",  \
                                            dvfm_dev_idx);              \
                                    dvfm_disable_op_name("sys_sleep",   \
                                            dvfm_dev_idx);              \
                                } while (0)
#define RESET_DSVL_CONSTRAINT   do {                                    \
                                    dvfm_enable_op_name("apps_idle",    \
                                            dvfm_dev_idx);              \
                                    dvfm_enable_op_name("apps_sleep",   \
                                            dvfm_dev_idx);              \
                                    dvfm_enable_op_name("sys_sleep",    \
                                            dvfm_dev_idx);              \
                                } while (0)

#else

#define SET_DSVL_CONSTRAINT
#define RESET_DSVL_CONSTRAINT

#endif

#define PIN_LOWER_LIMIT (64)
#define PIN_UPPER_LIMIT (0xFFFFFFFF)

#define CMD_MAJOR(_c)   ((_IOC_NR(_c) & 0xF0) >> 4)
#define CMD_MINOR(_c)   (_IOC_NR(_c) & 0xF)

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

wtm_drv g_wtm_drv;
static struct cdev _g_wtm_dev;
static struct class *_g_wtm_class;

static int32_t wtm_major = 0;
module_param(wtm_major, int, 0644);
MODULE_PARM_DESC(wtm_major, "the major number of WTM driver");
static int32_t wtm_pin_min = 64;
module_param(wtm_pin_min, int, 0644);
MODULE_PARM_DESC(wtm_pin_min, "min value of pinning pages to kernel");
static int32_t wtm_pin_max = WTM_PAGE_SIZE << 8;
module_param(wtm_pin_max, int, 0644);
MODULE_PARM_DESC(wtm_pin_min, "max value of pinning pages to kernel");

static int32_t _wtm_open(struct inode *inode, struct file *filp);
static int32_t _wtm_release(struct inode *inode, struct file *filp);
static int32_t _wtm_ioctl(struct inode *inode,
        struct file *filp, uint32_t cmd, ulong_t arg);

static struct file_operations _g_wtm_fops = {
    .owner      = THIS_MODULE,
    .open       = _wtm_open,
    .release    = _wtm_release,
    .ioctl      = _wtm_ioctl,
};

static int _wtm_suspend(struct platform_device *dev, pm_message_t state);
static int _wtm_resume(struct platform_device *dev);

static struct platform_driver _g_wtm_drv = {
    .suspend    = _wtm_suspend,
    .resume     = _wtm_resume,
    .driver     = {
        .name   = "wtm",
        .owner  = THIS_MODULE,
    },
};

static struct semaphore _g_ioctl_lock;
static struct semaphore _g_pm_lock; /* for power management */

static void *_g_wtm_ioctl_arr[][16] = {
    /* 0 */
    {
        NULL,
    },

    /* 1 */
    {
        wtm_hash_init_nonfips,
#ifdef CONFIG_EN_FIPS
        wtm_hash_init_fips,
#else
        NULL,
#endif
        wtm_hash_update,
        wtm_hash_final,
        wtm_hash_dgst_nonfips,
#ifdef CONFIG_EN_FIPS
        wtm_hash_dgst_fips,
#else
        NULL,
#endif
    },

    /* 2 */
    {
        wtm_sym_init_nonfips,
#ifdef CONFIG_EN_FIPS
        wtm_sym_init_fips,
#else
        NULL,
#endif
        wtm_sym_proc,
        wtm_sym_final,
    },

    /* 3 */
    {
        wtm_pkcs_init_nonfips,
#ifdef CONFIG_EN_FIPS
        wtm_pkcs_init_fips,
#else
        NULL,
#endif
        wtm_pkcs_update,
        wtm_pkcs_final,
        wtm_pkcs_op_nonfips,
#ifdef CONFIG_EN_FIPS
        wtm_pkcs_op_fips,
#else
        NULL,
#endif
    },

    /* 4 */
    {
        wtm_ec_dsa_init_nonfips,
#ifdef CONFIG_EN_FIPS
        wtm_ec_dsa_init_fips,
#else
        NULL,
#endif
        wtm_ec_dsa_update,
        wtm_ec_dsa_final,
        wtm_ec_dsa_op_nonfips,
#ifdef CONFIG_EN_FIPS
        wtm_ec_dsa_op_fips,
#else
        NULL,
#endif
    },

    /* 5 */
    {
        wtm_create_ec_key_pair,
        wtm_derivate_ec_pub_key,
        wtm_create_dh_sys_args,
        wtm_create_dh_key_pair,
        wtm_derivate_dh_pub_key,
        wtm_create_rsa_key_pair,
#ifdef CONFIG_EN_FIPS
        wtm_create_subkey,
#else
        NULL,
#endif
#ifdef CONFIG_EN_FIPS
        wtm_enroll_sym_usrkey,
#else
        NULL,
#endif
#ifdef CONFIG_EN_FIPS
        wtm_enroll_ec_usrkey,
#else
        NULL,
#endif
#ifdef CONFIG_EN_FIPS
        wtm_enroll_dh_usrkey,
#else
        NULL,
#endif
#ifdef CONFIG_EN_FIPS
        wtm_enroll_rsa_usrkey,
#else
        NULL,
#endif
#ifdef CONFIG_EN_FIPS
        wtm_create_sym_usrkey,
#else
        NULL,
#endif
        wtm_create_ec_dh_shared_key,
#ifdef CONFIG_EN_FIPS
        wtm_create_dh_shared_key,
#else
        NULL,
#endif
#ifdef CONFIG_EN_FIPS
        wtm_pr_ec256_dec,
#else
        NULL,
#endif
#ifdef CONFIG_EN_FIPS
        wtm_check_usrkey,
#else
        NULL,
#endif
    },

    /* 6 */
    {
        wtm_hdcp_load_key,
    },

    /* 7 */
    {
        wtm_drbg_init,
        wtm_drbg_reseed,
        wtm_drbg_gen_ran,
    },

    /* 8 */
    {
        NULL,
    },

    /* 9 */
    {
        wtm_prov_rkek_prov,
        wtm_prov_ec521_dk_prov,
        wtm_prov_plat_bind,
        wtm_prov_plat_verf,
        wtm_prov_bind_jtag_key,
        wtm_prov_verf_jtag_key,
        wtm_prov_dis_jtag,
        wtm_prov_tmp_dis_fa,
        wtm_prov_rd_uniq_id,
        wtm_prov_wr_plat_conf,
        wtm_prov_rd_plat_conf,
        wtm_prov_wr_usb_id,
        wtm_prov_rd_usb_id,
        wtm_prov_hdcp_wrap_key,
        wtm_prov_set_fips_perm,
        wtm_prov_set_nonfips_perm,
    },

    /* A */
    {
        wtm_prov_pre_proc,
        wtm_prov_post_proc,
        wtm_prov_oem_trans_key,
        wtm_bind_pr_key,
    },

    /* B */
    {
        wtm_get_tsr,
        wtm_life_cycle_adv,
        wtm_life_cycle_read,
        wtm_soft_ver_adv,
        wtm_soft_ver_read,
        wtm_kernel_ver_read,
    },

    /* C */
    {
        wtm_jtag_get_nonce,
        wtm_jtag_conf,
    },
};

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

static int32_t _wtm_open(struct inode *inode, struct file *filp)
{
    return 0;
}

static int32_t _wtm_release(struct inode *inode, struct file *filp)
{
    return 0;
}

static int32_t _wtm_ioctl(struct inode *inode,
        struct file *filp, uint32_t cmd, ulong_t arg)
{
    int32_t (*func)(ulong_t);

    func = (int32_t (*)(ulong_t))
        _g_wtm_ioctl_arr[CMD_MAJOR(cmd)][CMD_MINOR(cmd)];

    if (func) {
        int32_t ret;
        /*
         * in $/fs/ioctl.c
         * 49     } else if (filp->f_op->ioctl) {
         * 50         lock_kernel();
         * 51         error = filp->f_op->ioctl(filp->f_path.dentry->d_inode,
         * 52                       filp, cmd, arg);
         * 53         unlock_kernel();
         * 54     }
         * lock_kernel/unlock_kernel seems invisible now
         * is it a bug of removing big lock?
         * lock/unlock in our own manner
         */
        down(&_g_ioctl_lock);
        down(&_g_pm_lock);
        SET_DSVL_CONSTRAINT;
        ret = func(arg);
        RESET_DSVL_CONSTRAINT;
        up(&_g_pm_lock);
        up(&_g_ioctl_lock);
        return ret;
    } else {
        _PR("ERROR - no such func called [cmd = 0x%08x].\n", cmd);
        return -1;
    }
}

static int32_t _wtm_suspend(struct platform_device *dev, pm_message_t state)
{
    return down_trylock(&_g_pm_lock);
}

static int32_t _wtm_resume(struct platform_device *dev)
{
    up(&_g_pm_lock);
    return 0;
}

static int32_t __init _wtm_init(void)
{
    int32_t ret = 0;
    dev_t dev_nr = MKDEV(wtm_major, 0);

    _PR("INFO - WTPsp drv version is %d.%d.%d\n", MAJOR_VERSION(DRV_LAYER_VERSION),
            MINOR_VERSION(DRV_LAYER_VERSION), LAST_VERSION(DRV_LAYER_VERSION));

    /* register/allocate the major number of the device */
    if (wtm_major) {
        ret = register_chrdev_region(dev_nr, 1, "wtm");
    } else {
        ret = alloc_chrdev_region(&dev_nr, 0, 1, "wtm");
        wtm_major = MAJOR(dev_nr);
    }

    if (ret < 0) {
        _PR("ERROR - failed to get major %d in %s\n",
                wtm_major, __FUNCTION__);
        return ret;
    }

    cdev_init(&_g_wtm_dev, &_g_wtm_fops);
    _g_wtm_dev.owner = THIS_MODULE;
    _g_wtm_dev.ops = &_g_wtm_fops;

    ret = cdev_add(&_g_wtm_dev, dev_nr, 1);
    if (ret) {
        _PR("ERROR - failed to add wtm in %s\n", __FUNCTION__);
        goto _failed_in_adding_cdev;
    }

    ret = platform_driver_register(&_g_wtm_drv);
    if (ret) {
        _PR("ERROR - failed to reg drv in %s\n", __FUNCTION__);
        goto _failed_in_reg_drv;
    }

    _g_wtm_class = class_create(THIS_MODULE, "wtm");
    if (IS_ERR(_g_wtm_class)) {
        _PR("ERROR - failed to create wtm class in %s\n", __FUNCTION__);
        goto _failed_in_crt_class;
    }
    device_create(_g_wtm_class, NULL, MKDEV(wtm_major, 0), NULL, "wtm");

    sema_init(&_g_ioctl_lock, 1);
    sema_init(&_g_pm_lock, 1);

    g_wtm_drv.key_buf_virt = wtm_alloc_dma_mem(
            WTM_PAGE_SIZE, WTM_PHYS_ADDR_ALGN, &g_wtm_drv.key_buf_phys);
    _ASSERT(g_wtm_drv.key_buf_virt);
    g_wtm_drv.in_buf_virt = wtm_alloc_dma_mem(
            WTM_PAGE_SIZE, WTM_PHYS_ADDR_ALGN, &g_wtm_drv.in_buf_phys);
    _ASSERT(g_wtm_drv.in_buf_virt);
    g_wtm_drv.out_buf_virt = wtm_alloc_dma_mem(
            WTM_PAGE_SIZE, WTM_PHYS_ADDR_ALGN, &g_wtm_drv.out_buf_phys);
    _ASSERT(g_wtm_drv.out_buf_virt);
    _ASSERT(wtm_is_mem_direct_ok(
                (uint32_t)g_wtm_drv.part_blk_virt, MV_WTM_MAX_PART_BLK_SZ));
    if (wtm_pin_min >= PIN_LOWER_LIMIT) {
        g_wtm_drv.pin_min = wtm_pin_min;
    } else {
        g_wtm_drv.pin_min = PIN_LOWER_LIMIT;
        _PR("WARNING - pin_min must NOT be less than %d\n", PIN_LOWER_LIMIT);
    }
    if (wtm_pin_max <= PIN_UPPER_LIMIT) {
        g_wtm_drv.pin_max = wtm_pin_max;
    } else {
        g_wtm_drv.pin_max = PIN_UPPER_LIMIT;
        _PR("WARNING - pin_max must NOT be more than %d\n", PIN_UPPER_LIMIT);
    }

    ret = mv_wtm_func_init();
    if (ret) {
        _PR("ERROR - failed to init wtm core in %s\n", __FUNCTION__);
        goto _failed_in_init_core;
    }

    _PR("INFO - init wtm driver (major_nr = %d)\n", wtm_major);

    return ret;

_failed_in_init_core:

    device_destroy(_g_wtm_class, MKDEV(wtm_major, 0));
    class_destroy(_g_wtm_class);

_failed_in_crt_class:

    platform_driver_unregister(&_g_wtm_drv);

_failed_in_reg_drv:

    cdev_del(&_g_wtm_dev);

_failed_in_adding_cdev:

    unregister_chrdev_region(MKDEV(wtm_major, 0), 1);

    return ret;
}

static void __exit _wtm_cleanup(void)
{
    mv_wtm_func_cleanup();

    device_destroy(_g_wtm_class, MKDEV(wtm_major, 0));
    class_destroy(_g_wtm_class);

    wtm_free_dma_mem(g_wtm_drv.key_buf_virt);
    wtm_free_dma_mem(g_wtm_drv.in_buf_virt);
    wtm_free_dma_mem(g_wtm_drv.out_buf_virt);

    platform_driver_unregister(&_g_wtm_drv);
    cdev_del(&_g_wtm_dev);
    unregister_chrdev_region(MKDEV(wtm_major, 0), 1);

    _PR("INFO - deinit wtm driver\n");
}

module_init(_wtm_init);
module_exit(_wtm_cleanup);
