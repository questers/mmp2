/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_os_util.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of os-spec WTM utilities
 *                This file is os-specific
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "wtm_os_util.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

typedef struct __phys_mem_info
{
    void        *alloc_virt_addr;
    void        *alloc_phys_addr;
    uint32_t    alloc_size;
} _phys_mem_info;

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t wtm_pr(uint8_t *fmt, ...)
{
    va_list args;
    uint32_t ret = 0;

    va_start(args, fmt);
    ret = vprintk(fmt, args);
    va_end(args);

    return ret;
}

wtm_vm wtm_get_user_mem(void __user *virt, uint32_t size,
        void __kernel **kvirt)
{
    ulong_t start = (ulong_t)virt;
    ulong_t end = (start + size + WTM_PAGE_SIZE - 1) & PAGE_MASK;

    struct vm_area_struct *vma;
    ulong_t cur = start & PAGE_MASK, cur_end;
    int32_t err, write;

    _ASSERT(virt && size);

    down_read(&current->mm->mmap_sem);

    do { 
        vma = find_vma(current->mm, cur);
        if (!vma) {
            up_read(&current->mm->mmap_sem);
            return NULL;
        }    
        cur_end = ((end <= vma->vm_end) ? end : vma->vm_end);

        /* this vma is NOT mmap-ed and pfn-direct mapped */
        if (!(vma->vm_flags & (VM_IO | VM_PFNMAP))) {
            write = ((vma->vm_flags & VM_WRITE) != 0);
            /* NOTE: no get_page since overall sync-flow */
            err = get_user_pages(current, current->mm,
                    (ulong_t)cur, (cur_end - cur) >> PAGE_SHIFT,
                    write, 0, NULL, NULL);
            if (err < 0) { 
                up_read(&current->mm->mmap_sem);
                return NULL;
            }    
        }

        cur = cur_end;
    } while (cur_end < end);

    up_read(&current->mm->mmap_sem);

    *kvirt = virt;

    /* fake wtm_vm handle here */
    return (wtm_vm)(1);
}

void wtm_put_user_mem(wtm_vm vm)
{
    _ASSERT((wtm_vm)(1) == vm);

    /* nothing */;
}

static uint32_t _wtm_pg_tab_walk(struct mm_struct *mm, uint32_t virt)
{
    pgd_t *pgd;               
    pmd_t *pmd;
    pte_t *pte;

    ulong_t pfn;      
#ifdef pte_offset_map_lock
    spinlock_t *ptl;
#endif
    uint32_t ret = WTM_INVALID_PHYS_ADDR;

    down_read(&mm->mmap_sem);

    pgd = pgd_offset(mm, (ulong_t)virt);
    if (!pgd_present(*pgd)) {
        goto _no_pgd;
    }

    pmd = pmd_offset(pgd, (ulong_t)virt);
    if (!pmd_present(*pmd)) {
        goto _no_pmd;
    }

#ifdef pte_offset_map_lock
    pte = pte_offset_map_lock(mm, pmd, (ulong_t)virt, &ptl);
#else
    pte = pte_offset_map(pmd, (ulong_t)virt);
#endif
    if (!pte_present(*pte)) {
        goto _no_pte;
    }

    pfn = pte_pfn(*pte);

    ret = (pfn << PAGE_SHIFT)
            + ((ulong_t)virt & (~PAGE_MASK));
_no_pte:
#ifdef pte_offset_map_lock
    pte_unmap_unlock(pte, ptl);
#endif

_no_pmd:
_no_pgd:
    up_read(&mm->mmap_sem);

    return ret;
}

bool wtm_is_mem_contig(uint32_t msg, uint32_t sz)
{
    uint32_t virt;
    uint32_t phys;
    uint32_t _sz;

    _ASSERT(msg && sz);

    /* the memory block is in one page in most of the cases */
    if (((msg % WTM_PAGE_SIZE) + sz) <= WTM_PAGE_SIZE) {
        return true;
    }

    /* round down by page */
    virt = (uint32_t)msg & ~(WTM_PAGE_SIZE - 1);
    phys = (uint32_t)wtm_virt_to_phys(virt);
    _sz = (uint32_t)msg - virt + sz;

    do { 
        virt += WTM_PAGE_SIZE;
        phys += WTM_PAGE_SIZE;

        if (phys != wtm_virt_to_phys(virt)) {
            return false;
        }

        _sz -= WTM_PAGE_SIZE;
    } while (_sz > WTM_PAGE_SIZE);

    return true;
}

uint32_t wtm_alloc_dma_mem(
                    uint32_t size, uint32_t algn, uint32_t *phys)
{
    uint32_t mem_blk_size;
    void *alloc_virt_addr;
    void *alloc_phys_addr;
    void *ret_virt_addr;
    _phys_mem_info *rec_virt_addr;

    if (!size || !phys) {
        return 0;
    }

    mem_blk_size = size + sizeof(_phys_mem_info) + algn;

    alloc_virt_addr = dma_alloc_coherent(
                NULL, mem_blk_size, (dma_addr_t *)&alloc_phys_addr, GFP_KERNEL);
    if (NULL == alloc_virt_addr) {
        *phys = 0;
        return 0;
    }

    /* calculate the virtual address returned */
    ret_virt_addr = (void *)(((uint32_t)
                alloc_virt_addr + algn - 1) & (~(algn - 1)));
    do {
        if (ret_virt_addr - alloc_virt_addr
             >= sizeof(_phys_mem_info)) {
            rec_virt_addr =
                (_phys_mem_info *)((uint32_t)
                        ret_virt_addr - sizeof(_phys_mem_info));
            break;
        } else {
            ret_virt_addr = (void *)(
                    (uint32_t)ret_virt_addr + algn);
        }
    } while (1);

    _ASSERT((uint32_t)rec_virt_addr >= (uint32_t)alloc_virt_addr);
    _ASSERT((uint32_t)ret_virt_addr + size
                            <= (uint32_t)alloc_virt_addr + mem_blk_size);

    rec_virt_addr->alloc_virt_addr = alloc_virt_addr;
    rec_virt_addr->alloc_phys_addr = alloc_phys_addr;
    rec_virt_addr->alloc_size = mem_blk_size;

    *phys = (uint32_t)alloc_phys_addr +
            ((uint32_t)ret_virt_addr - (uint32_t)alloc_virt_addr);

    return (uint32_t)ret_virt_addr;
}

void wtm_free_dma_mem(uint32_t virt)
{
    _phys_mem_info *rec_virt_addr;

    _ASSERT(virt);

    rec_virt_addr = (_phys_mem_info *)(
            virt - sizeof(_phys_mem_info));

    dma_free_coherent(
                NULL, rec_virt_addr->alloc_size,
                rec_virt_addr->alloc_virt_addr,
                (dma_addr_t)rec_virt_addr->alloc_phys_addr);
}

uint32_t wtm_virt_to_phys(uint32_t virt)
{
    if (virt < PAGE_OFFSET) {
        return _wtm_pg_tab_walk(current->mm, virt);
    } else if (virt < (uint32_t)high_memory) {
        return virt_to_phys((void *)virt);
    } else {
        _ASSERT(0);
        return _wtm_pg_tab_walk(init_task.active_mm, virt);
    }
}
