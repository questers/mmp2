/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : mv_wtm_drv_crypto_util.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of crypto-related utilities in WTM driver
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_xllp.h"
#include "wtm_platform.h"
#include "wtm_crypto_util.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#undef WTM_DBG_DUMP_LIST

#define DIR_TO_WTM          (0)
#define DIR_FROM_WTM        (1)

#define BYTE_SZ_TO_WORD(_byte_sz)   (((_byte_sz) + 3) >> 2)

#define SET_REC_INFO(_rec, _node_virt, _mem_to_free, _mem_to_free_sz)   \
    do {                                                                \
        _rec->node_virt = (dma_node *)_node_virt;                       \
        _rec->is_rec_static = false;                                    \
        _rec->mem_to_free = _mem_to_free;                               \
        _rec->mem_to_free_sz = _mem_to_free_sz;                         \
        _rec->next = NULL;                                              \
    } while (0)

#define LINK_TO_REC_LIST(_last, _cur)                                   \
    do {                                                                \
        if (_last) {                                                    \
            _last->next = _cur;                                         \
        }                                                               \
        _last = _cur;                                                   \
    } while (0)

#define SET_DMA_NODE(_node, _trans_phys, _trans_sz)                     \
    do {                                                                \
        _node->trans_phys = _trans_phys;                                \
        _node->trans_sz = BYTE_SZ_TO_WORD(_trans_sz);                   \
        _node->next_phys = 0;                                           \
        _node->reserved = 0;                                            \
    } while (0)

#define LINK_TO_DMA_LIST(_last, _cur, _cur_entry_phys)                  \
    do {                                                                \
        if (_last) {                                                    \
            _last->next_phys = _cur_entry_phys;                         \
        }                                                               \
        _last = _cur;                                                   \
    } while (0)

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

__INLINE static bool _wtm_is_msg_list_ok(uint32_t msg, uint32_t msg_sz)
{
    if (_IS_ALIGNED(msg, WTM_ADDR_ALGN_FOR_LINK_LIST) &&
            _IS_ALIGNED(msg_sz, WTM_LEN_ALGN_FOR_LINK_LIST)) {
        return true;
    } else {
        return false;
    }
}

__INLINE static bool _wtm_is_msg_cache_line_aligned(
        uint32_t msg, uint32_t msg_sz)
{
    if (_IS_ALIGNED(msg, CACHE_LINE_SZ) &&
            _IS_ALIGNED(msg_sz, CACHE_LINE_SZ)) {
        return true;
    } else {
        return false;
    }
}

static void _wtm_destroy_dma_list(dma_node_rec *rec)
{
    dma_node_rec *cur = rec, *next;
    
    _ASSERT(rec);

    while (cur) {
        if (cur->mem_to_free) {
            wtm_free_dma_mem(cur->mem_to_free);
        }
        if (cur->is_rec_static) {
            cur = cur->next;
            continue;
        }
        wtm_free_dma_mem((uint32_t)cur->node_virt);
        next = cur->next;
        wtm_free_mem((uint32_t)cur);
        cur = next;
    }
}

static int32_t _wtm_create_dma_list(
        uint32_t msg, uint32_t msg_sz, uint32_t dir,
        uint32_t *list_phys, dma_node_rec **rec)
{
    uint32_t addr = msg;
    uint32_t len  = msg_sz, len_bak;
    uint32_t range;
    uint32_t entry_virt, entry_phys;
    uint32_t first_virt, first_phys;
    dma_node_rec *rec_virt, *last_rec_virt= NULL;
    uint32_t trans_phys, trans_sz;
    bool first = true;
    dma_node *node = NULL, *last_node = NULL;
    int32_t ret = 0;

    /*
     * virt_addr_start - the virtual starting address of the physical-continuous memory
     * phys_addr_start - the physical starting address of the physical-continuous memory
     * phys_contig_sz  - the size of the physical-continuous memory
     */
    uint32_t virt_addr_start = 0, phys_addr_start = 0, phys_contig_sz = 0;

    _ASSERT(msg && msg_sz && rec);

    *list_phys = 0;
    *rec = NULL;

    do {
        range = WTM_PAGE_SIZE - (addr % WTM_PAGE_SIZE);

        trans_phys = wtm_virt_to_phys(addr);
        _ASSERT(trans_phys);
        trans_sz = (range < len) ? range : len;

        if (first) {
            /*
             * if the start address is not addr-aligned, we just allocate
             * an entry in the link list for the first memory segment
             */
            if (!_IS_ALIGNED(trans_phys, WTM_ADDR_IN_LIST_ALGN)) {
                /*
                 * the first list is to record the info
                 * of the entries of DMA link list
                 */
                rec_virt = (dma_node_rec *)wtm_alloc_mem(sizeof(dma_node_rec));
                if (NULL == rec_virt) {
                    ret = -1;
                    break;
                }

                /*
                 * the second list is the DMA link list
                 * allocate a memory block for a physical-contiguous DMA entry
                 */
                entry_virt = wtm_alloc_dma_mem(sizeof(dma_node),
                                            WTM_LINK_LIST_ALGN,
                                            &entry_phys);
                if (0 == entry_virt) {
                    wtm_free_mem((uint32_t)rec_virt);
                    ret = -1;
                    break;
                }

                /* allocate a memory block for the contents of the first block */
                first_virt = wtm_alloc_dma_mem(trans_sz,
                                            WTM_ADDR_IN_LIST_ALGN,
                                            &first_phys);
                if (0 == first_virt) {
                    wtm_free_dma_mem(entry_virt);
                    wtm_free_mem((uint32_t)rec_virt);
                    ret = -1;
                    break;
                }

                /* set the entry of the record list */
                SET_REC_INFO(rec_virt, entry_virt, first_virt, trans_sz);
                /* and create rec link list */
                LINK_TO_REC_LIST(last_rec_virt, rec_virt);

                /* set the entry of DMA link list */
                node = (dma_node *)entry_virt;
                SET_DMA_NODE(node, first_phys, trans_sz);
                LINK_TO_DMA_LIST(last_node, node, entry_phys);

                /* copy the content of the first block if DMA_LINK_LIST_IN */
                if (DIR_TO_WTM == dir) {
                    wtm_memcpy(first_virt, addr, trans_sz);
                }

                /* record the output arguments */
                *list_phys = entry_phys;
                *rec = rec_virt;

                /* update the local records */
                virt_addr_start = addr + trans_sz;
                phys_addr_start = wtm_virt_to_phys(virt_addr_start);
                phys_contig_sz = 0;
            } else {
                /* the address is addr-aligned, record it and go ahead */
                virt_addr_start = addr;
                phys_addr_start = wtm_virt_to_phys(addr);
                phys_contig_sz = trans_sz;
            }

            first = false;
        } else {
            if (phys_addr_start + phys_contig_sz == wtm_virt_to_phys(addr)) {
                /* if continuous, record it and go further */
                phys_contig_sz += trans_sz;
            } else /* not continuous */ {
                /*
                 * the first list is to record the info
                 * of the entries of DMA link list
                 */
                rec_virt = (dma_node_rec *)wtm_alloc_mem(sizeof(dma_node_rec));
                if (NULL == rec_virt) {
                    ret = -1;
                    break;
                }

                /*
                 * the second list is the DMA link list
                 * allocate a memory block for a physical-contiguous DMA entry
                 */
                entry_virt = wtm_alloc_dma_mem(sizeof(dma_node),
                                            WTM_LINK_LIST_ALGN,
                                            &entry_phys);
                if (0 == entry_virt) {
                    wtm_free_mem((uint32_t)rec_virt);
                    ret = -1;
                    break;
                }

                /* set the entry of the record list */
                SET_REC_INFO(rec_virt, entry_virt, 0, 0);
                /* and create rec link list */
                LINK_TO_REC_LIST(last_rec_virt, rec_virt);

                /* set the entry of DMA link list */
                node = (dma_node *)entry_virt;
                SET_DMA_NODE(node, phys_addr_start, phys_contig_sz);
                LINK_TO_DMA_LIST(last_node, node, entry_phys);

                /* record the output arguments if not set */
                if (0 == *list_phys) {
                    *list_phys = entry_phys;
                }
                if (0 == *rec) {
                    *rec = rec_virt;
                }

                /* update the local records */
                virt_addr_start = addr;
                phys_addr_start = wtm_virt_to_phys(addr);
                phys_contig_sz = trans_sz;
            }
        }

        /* update the local variables for the loop */
        addr += range;
        len_bak = len;
        len -= range;
    } while (len_bak > range);

    /* handle the last piece of physical-continuous memory */
    if ((0 == ret) &&
                (virt_addr_start < msg + msg_sz)) {
        /*
         * the first list is to record the info
         * of the entries of DMA link list
         */
        rec_virt = (dma_node_rec *)wtm_alloc_mem(sizeof(dma_node_rec));
        if (NULL == rec_virt) {
            ret = -1;
            goto _out;
        }

        /*
         * the second list is the DMA link list
         * allocate a memory block for a physical-contiguous DMA entry
         */
        entry_virt = wtm_alloc_dma_mem(sizeof(dma_node),
                                    WTM_LINK_LIST_ALGN,
                                    &entry_phys);
        if (0 == entry_virt) {
            wtm_free_mem((uint32_t)rec_virt);
            ret = -1;
            goto _out;
        }

        /* set the entry of the record list */
        SET_REC_INFO(rec_virt, entry_virt, 0, 0);
        LINK_TO_REC_LIST(last_rec_virt, rec_virt);

        /* set the entry of DMA link list */
        node = (dma_node *)entry_virt;
        SET_DMA_NODE(node, phys_addr_start, phys_contig_sz);
        LINK_TO_DMA_LIST(last_node, node, entry_phys);

        /* record the output arguments if not set */
        if (0 == *list_phys) {
            *list_phys = entry_phys;
        }
        if (0 == *rec) {
            *rec = rec_virt;
        }
    }

_out:

    /* if failed and some resource allocated, free the resource now */
    if (ret && *rec) {
        /* release link list if any error occurs in loop. */
        (void)_wtm_destroy_dma_list(*rec);
        *list_phys = 0;
        *rec = NULL;
    }

    return ret;
}

static void _wtm_append_dma_list(
        msg_struct *ms, dma_node_rec *rec, bool is_src)
{
    dma_node_rec *cur = NULL, *next;

    _ASSERT(ms && rec);

    if (is_src) {
        next = ms->src_dma_rec;
    } else {
        next = ms->dst_dma_rec;
    }

    /* find the last one */
    while (next) {
        cur = next;
        next = next->next;
    }

    cur->next = rec;
    cur->node_virt->next_phys = wtm_virt_to_phys((uint32_t)rec->node_virt);
}

static int32_t _wtm_msg_struct_init_input(
        uint32_t part_blk, uint32_t part_blk_sz,
        uint32_t src_msg, uint32_t src_msg_sz,
        bool is_part_first, msg_struct *ms)
{

    int32_t ret;

    if (!part_blk_sz && src_msg_sz) {
        if (wtm_is_mem_direct_ok(src_msg, src_msg_sz)) {
            ms->src_stat = MSG_PHYS_DIRECT;
            ms->src_handle = (msg_handle)wtm_virt_to_phys(src_msg);
            wtm_flush_dcache(src_msg, src_msg_sz);
            wtm_flush_l2dcache(src_msg, src_msg_sz);
            return 0;
        } else if (_wtm_is_msg_list_ok(src_msg, src_msg_sz)) {
            ms->src_stat = MSG_DMA_LNK_LST;
            ret = _wtm_create_dma_list(
                    src_msg, src_msg_sz, DIR_TO_WTM,
                    (uint32_t *)&ms->src_handle,
                    (dma_node_rec **)&ms->src_dma_rec);
            if (ret) {
                return ret;
            }
            wtm_flush_dcache(src_msg, src_msg_sz);
            wtm_flush_l2dcache(src_msg, src_msg_sz);
            return 0;
        } else {
            /* can not be handled by one PI */
            return -1;
        }
    } else if (part_blk_sz && src_msg_sz) {
        /* setup static dma_node and dma_node_rec */
        ms->static_src_node = (dma_node *)
            _ROUND_UP(ms->src_node_buf, sizeof(dma_node));
        memset(&ms->static_src_rec, 0, sizeof(dma_node_rec) << 1);
        ms->static_src_rec[0].is_rec_static = true;
        ms->static_src_rec[1].is_rec_static = true;
        ms->static_src_rec[0].node_virt = &ms->static_src_node[0];
        ms->static_src_rec[1].node_virt = &ms->static_src_node[1];

        if (wtm_is_mem_direct_ok(src_msg, src_msg_sz)) {
            ms->static_src_rec[0].next = &ms->static_src_rec[1];
            /* unnecessary: ms->static_src_rec[1].next = NULL; */
            if (is_part_first) {
                /* 0 for part_blk 1 for src_msg */
                ms->static_src_node[0].trans_phys = wtm_virt_to_phys(part_blk);
                ms->static_src_node[0].trans_sz = BYTE_SZ_TO_WORD(part_blk_sz);
                ms->static_src_node[0].next_phys =
                    wtm_virt_to_phys((uint32_t)&ms->static_src_node[1]);
                ms->static_src_node[1].trans_phys = wtm_virt_to_phys(src_msg);
                ms->static_src_node[1].trans_sz = BYTE_SZ_TO_WORD(src_msg_sz);
                ms->static_src_node[1].next_phys = 0;
            } else {
                /* 0 for src_msg 1 for part_blk */
                ms->static_src_node[0].trans_phys = wtm_virt_to_phys(src_msg);
                ms->static_src_node[0].trans_sz = BYTE_SZ_TO_WORD(src_msg_sz);
                ms->static_src_node[0].next_phys =
                    wtm_virt_to_phys((uint32_t)&ms->static_src_node[1]);
                ms->static_src_node[1].trans_phys = wtm_virt_to_phys(part_blk);
                ms->static_src_node[1].trans_sz = BYTE_SZ_TO_WORD(part_blk_sz);
                ms->static_src_node[1].next_phys = 0;
            }
            wtm_flush_dcache(
                    (uint32_t)ms->static_src_node, sizeof(dma_node) << 1);
            wtm_flush_l2dcache(
                    (uint32_t)ms->static_src_node, sizeof(dma_node) << 1);
            wtm_flush_dcache(part_blk, part_blk_sz);
            wtm_flush_l2dcache(part_blk, part_blk_sz);
            wtm_flush_dcache(src_msg, src_msg_sz);
            wtm_flush_l2dcache(src_msg, src_msg_sz);
            ms->src_stat = MSG_DMA_LNK_LST;
            ms->src_handle = (msg_handle)
                wtm_virt_to_phys((uint32_t)ms->static_src_node);
            ms->src_dma_rec = ms->static_src_rec;
            return 0;
        } else if (_wtm_is_msg_list_ok(src_msg, src_msg_sz)) {
            ms->src_stat = MSG_DMA_LNK_LST;
            ret = _wtm_create_dma_list(
                    src_msg, src_msg_sz, DIR_TO_WTM,
                    (uint32_t *)&ms->src_handle,
                    (dma_node_rec **)&ms->src_dma_rec);
            if (ret) {
                return ret;
            }
            /* link part-blk's dma node to dma link list */
            if (is_part_first) {
                ms->static_src_rec[0].is_rec_static = true;
                ms->static_src_rec[0].next = ms->src_dma_rec;
                ms->src_dma_rec = &ms->static_src_rec[0];
                ms->static_src_node[0].trans_phys = wtm_virt_to_phys(part_blk);
                ms->static_src_node[0].trans_sz = BYTE_SZ_TO_WORD(part_blk_sz);
                ms->static_src_node[0].next_phys = (uint32_t)ms->src_handle;
                ms->src_handle = (msg_handle)
                    wtm_virt_to_phys((uint32_t)&ms->static_src_node[0]);
            } else {
                ms->static_src_rec[0].is_rec_static = true;
                _wtm_append_dma_list(ms, &ms->static_src_rec[0], true);
                ms->static_src_node[0].trans_phys = wtm_virt_to_phys(part_blk);
                ms->static_src_node[0].trans_sz = BYTE_SZ_TO_WORD(part_blk_sz);
                ms->static_src_node[0].next_phys = 0;
            }
            wtm_flush_dcache((uint32_t)ms->static_src_node, sizeof(dma_node));
            wtm_flush_l2dcache((uint32_t)ms->static_src_node, sizeof(dma_node));
            wtm_flush_dcache(part_blk, part_blk_sz);
            wtm_flush_l2dcache(part_blk, part_blk_sz);
            wtm_flush_dcache(src_msg, src_msg_sz);
            wtm_flush_l2dcache(src_msg, src_msg_sz);
            return 0;
        } else {
            /* can not be handled by one PI */
            return -1;
        }
    } else if (part_blk_sz && !src_msg_sz) {
        ms->src_stat = MSG_PHYS_DIRECT;
        ms->src_handle = (msg_handle)wtm_virt_to_phys(part_blk);
        wtm_flush_dcache(part_blk, part_blk_sz);
        wtm_flush_l2dcache(part_blk, part_blk_sz);
        return 0;
    } else {
        _ASSERT(0);
        return -1;
    }
}

static int32_t _wtm_msg_struct_init_dual(
        uint32_t part_blk, uint32_t part_blk_sz,
        uint32_t src_msg, uint32_t src_msg_sz,
        uint32_t dst_msg, uint32_t dst_msg_sz,
        bool is_part_first, msg_struct *ms)
{
    bool is_src_direct_ok, is_src_ok;
    bool is_dst_direct_ok, is_dst_ok;
    int32_t ret;

    _ASSERT(CACHE_LINE_SZ >= WTM_ADDR_IN_LIST_ALGN);
    _ASSERT(CACHE_LINE_SZ >= WTM_ADDR_ALGN_FOR_LINK_LIST);

    if (src_msg_sz) {
        is_src_direct_ok = wtm_is_mem_direct_ok(src_msg, src_msg_sz);
    } else {
        /* part blk only */
        is_src_direct_ok = true;
    }
    if (is_src_direct_ok) {
        is_src_ok = true;
    } else {
        is_src_ok = _wtm_is_msg_list_ok(src_msg, src_msg_sz);
    }

    is_dst_direct_ok = wtm_is_mem_direct_ok(dst_msg, dst_msg_sz);
    if (is_dst_direct_ok) {
        is_dst_ok = true;
    } else {
        is_dst_ok = _wtm_is_msg_list_ok(dst_msg, dst_msg_sz);
    }

    /*
     * src and dst should be handled by one PI,
     * or return -1
     */
    if (!is_src_ok || !is_dst_ok) {
        return -1;
    }

    ret = _wtm_msg_struct_init_input(part_blk, part_blk_sz,
            src_msg, src_msg_sz, is_part_first, ms);
    _ASSERT(0 == ret);

    ms->is_dst_cache_ok = _wtm_is_msg_cache_line_aligned(dst_msg, dst_msg_sz);

    if (ms->is_dst_cache_ok) {
        if (is_dst_direct_ok) {
            /* direct call */
            ms->dst_stat = MSG_PHYS_DIRECT;
            ms->dst_handle = (msg_handle)wtm_virt_to_phys(dst_msg);
        } else {
            /* link list */
            ms->dst_stat = MSG_DMA_LNK_LST;
            ret = _wtm_create_dma_list(
                    dst_msg, dst_msg_sz, DIR_FROM_WTM,
                    (uint32_t *)&ms->dst_handle,
                    (dma_node_rec **)&ms->dst_dma_rec);
            _ASSERT(0 == ret);
        }

        wtm_flush_dcache(dst_msg, dst_msg_sz);
        wtm_flush_l2dcache(dst_msg, dst_msg_sz);
    } else {
        /* need follow-up in "after_pi" */

        /* if dst_msg is short enough, copy it and create one-node link list */
        if (dst_msg_sz <= (CACHE_LINE_SZ << 1)) {
            ms->dst_msg_dup = wtm_alloc_dma_mem(dst_msg_sz, WTM_PHYS_ADDR_ALGN,
                    (uint32_t *)&(ms->dst_handle));
            _ASSERT(ms->dst_msg_dup);
            ms->dst_stat = MSG_PHYS_DIRECT;

            wtm_memcpy(ms->dst_msg_dup, dst_msg, dst_msg_sz);

            /* no cache flush on dst_msg needed */
        } else {
            uint32_t i;

            /* setup static dma_node and dma_node_rec */
            ms->static_dst_node = (dma_node *)
                _ROUND_UP(ms->dst_node_buf, sizeof(dma_node));

            ms->unalgn_len[0] = CACHE_LINE_SZ - (dst_msg % CACHE_LINE_SZ);
            ms->unalgn_len[1] = (dst_msg + dst_msg_sz) % CACHE_LINE_SZ;

            for (i = 0; i < 2; i++) {
                if (ms->unalgn_len[i]) {
                    memset(&ms->static_dst_rec[i], 0, sizeof(dma_node_rec));
                    memset(&ms->static_dst_node[i], 0, sizeof(dma_node));

                    /* set static node rec */
                    ms->static_dst_rec[i].is_rec_static = true;
                    ms->static_dst_rec[i].node_virt = &ms->static_dst_node[i];
                    ms->static_dst_rec[i].mem_to_free =
                        wtm_alloc_dma_mem(ms->unalgn_len[i], WTM_ADDR_IN_LIST_ALGN,
                                &(ms->static_dst_node[i].trans_phys));
                    ms->static_dst_rec[i].mem_to_free_sz = ms->unalgn_len[i];

                    /* set static node */
                    ms->static_dst_node[i].trans_sz =
                        BYTE_SZ_TO_WORD(ms->unalgn_len[i]);
                }
            }

            /* copy head and tail if any */
            if (ms->unalgn_len[0]) {
                wtm_memcpy(ms->static_dst_rec[0].mem_to_free,
                        dst_msg, ms->unalgn_len[0]);
            }
            if (ms->unalgn_len[1]) {
                wtm_memcpy(ms->static_dst_rec[1].mem_to_free,
                        dst_msg + dst_msg_sz - ms->unalgn_len[1],
                        ms->unalgn_len[1]);
            }
            /* no cache flush for head and tail needed */

            ms->dst_stat = MSG_DMA_LNK_LST;
            ret = _wtm_create_dma_list(
                    dst_msg + ms->unalgn_len[0],
                    dst_msg_sz - ms->unalgn_len[0] - ms->unalgn_len[1],
                    DIR_FROM_WTM,
                    (uint32_t *)&ms->dst_handle,
                    (dma_node_rec **)&ms->dst_dma_rec);
            _ASSERT(0 == ret);

            /* assembly the link list */
            if (ms->unalgn_len[0]) {
                /* link node rec */
                ms->static_dst_rec[0].next = ms->dst_dma_rec;
                ms->dst_dma_rec = &ms->static_dst_rec[0];

                /* link node */
                ms->static_dst_node[0].next_phys = (uint32_t)ms->dst_handle;
                ms->dst_handle = (msg_handle)wtm_virt_to_phys(
                        (uint32_t)&ms->static_dst_node[0]);

                /* flush node */
                wtm_flush_dcache(
                        (uint32_t)&ms->static_dst_node[0], sizeof(dma_node));
                wtm_flush_l2dcache(
                        (uint32_t)&ms->static_dst_node[0], sizeof(dma_node));
            }
            if (ms->unalgn_len[1]) {
                dma_node_rec *last_rec = ms->dst_dma_rec;

                _ASSERT(last_rec);

                /* find the last rec */
                while (last_rec->next) {
                    last_rec = last_rec->next;
                }

                /* link node rec */
                last_rec->next = &ms->static_dst_rec[1];

                /* link node */
                last_rec->node_virt->next_phys = wtm_virt_to_phys(
                        (uint32_t)&ms->static_dst_node[1]);

#if 0
                /* flush the last two nodes */
                wtm_flush_dcache(
                        (uint32_t)last_rec->node_virt, sizeof(dma_node));
                wtm_flush_l2dcache(
                        (uint32_t)last_rec->node_virt, sizeof(dma_node));
                wtm_flush_dcache(
                        (uint32_t)&ms->static_dst_node[1], sizeof(dma_node));
                wtm_flush_l2dcache(
                        (uint32_t)&ms->static_dst_node[1], sizeof(dma_node));
#endif
            }

            wtm_flush_dcache(dst_msg, dst_msg_sz);
            wtm_flush_l2dcache(dst_msg, dst_msg_sz);
        }
    }

    return 0;
}

int32_t wtm_msg_struct_init(
        uint32_t part_blk, uint32_t part_blk_sz,
        uint32_t src_msg, uint32_t src_msg_sz,
        uint32_t dst_msg, uint32_t dst_msg_sz,
        bool is_part_first, msg_struct *ms)
{
    int32_t ret;

    _ASSERT(part_blk_sz || src_msg_sz);
    _ASSERT(ms);

    wtm_memset((uint32_t)ms, 0, sizeof(msg_struct));
    if (!dst_msg_sz) {
        ms->dst_msg = 0; /* valid flag of dst msg */
        ms->dst_msg_sz = 0;
        ret = _wtm_msg_struct_init_input(
                part_blk, part_blk_sz, src_msg, src_msg_sz, is_part_first, ms);
    } else {
        _ASSERT(part_blk_sz + src_msg_sz <= dst_msg_sz);
        ms->dst_msg = dst_msg; /* valid flag of dst msg */
        ms->dst_msg_sz = dst_msg_sz;
        ret = _wtm_msg_struct_init_dual(
                part_blk, part_blk_sz, src_msg, src_msg_sz,
                dst_msg, dst_msg_sz, is_part_first, ms);
    }

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_LIST)

    if (!ret) {
        dma_node_rec *cur;

        cur = ms->src_dma_rec;

        wtm_pr("in list:\n");
        wtm_pr("static\ttrans_phys\ttrans_sz\tnext_phys\n");
        if (!cur) {
            wtm_pr("NULL\n");
        } else {
            while (cur) {
                wtm_pr("%d\t0x%08x\t0x%08x\t0x%08x\n",
                        cur->is_rec_static,
                        cur->node_virt->trans_phys,
                        cur->node_virt->trans_sz,
                        cur->node_virt->next_phys);
                cur = cur->next;
            }
        }

        cur = ms->dst_dma_rec;

        if (!cur) {
            wtm_pr("out list: cache_ok = ? unalgn[0] = ? unalgn[1] = ?\n");
            wtm_pr("static\ttrans_phys\ttrans_sz\tnext_phys\n");
            wtm_pr("NULL\n");
        } else {
            wtm_pr("out list: cache_ok = %d unalgn[0] = %d unalgn[1] = %d\n",
                    ms->is_dst_cache_ok, ms->unalgn_len[0], ms->unalgn_len[1]);
            wtm_pr("static\ttrans_phys\ttrans_sz\tnext_phys\n");
            while (cur) {
                wtm_pr("%d\t0x%08x\t0x%08x\t0x%08x\n",
                        cur->is_rec_static,
                        cur->node_virt->trans_phys,
                        cur->node_virt->trans_sz,
                        cur->node_virt->next_phys);
                cur = cur->next;
            }
        }

        wtm_pr("\n");
    }

#endif

    return ret;
}

void wtm_msg_struct_cleanup(msg_struct *ms)
{
    _ASSERT(ms);

    if (MSG_DMA_LNK_LST == ms->src_stat) {
        _wtm_destroy_dma_list(ms->src_dma_rec);
    }

    if (ms->dst_msg) {
        if (MSG_PHYS_DIRECT == ms->dst_stat) {
            if (ms->dst_msg_dup) {
                wtm_free_dma_mem(ms->dst_msg_dup);
            }
        } else if (MSG_DMA_LNK_LST == ms->dst_stat) {
            _wtm_destroy_dma_list(ms->dst_dma_rec);
        } else {
            _ASSERT(0);
        }
    }
}

void wtm_msg_struct_proc_output_after_pi(msg_struct *ms)
{
    _ASSERT(ms && ms->dst_msg && ms->dst_msg_sz);

    if (!ms->is_dst_cache_ok) {
        if (ms->dst_msg_sz <= (CACHE_LINE_SZ << 1)) {
            wtm_memcpy(ms->dst_msg,
                    ms->dst_msg_dup, ms->dst_msg_sz);
        } else {
            if (ms->unalgn_len[0]) {
                _ASSERT(ms->unalgn_len[0] ==
                        ms->static_dst_rec[0].mem_to_free_sz);
                wtm_memcpy(ms->dst_msg,
                        ms->static_dst_rec[0].mem_to_free, ms->unalgn_len[0]);
            }
            if (ms->unalgn_len[1]) {
                _ASSERT(ms->unalgn_len[1] ==
                        ms->static_dst_rec[1].mem_to_free_sz);
                wtm_memcpy(ms->dst_msg + ms->dst_msg_sz - ms->unalgn_len[1],
                        ms->static_dst_rec[1].mem_to_free, ms->unalgn_len[1]);
            }
        }
    }
}
