/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_drbg.c
 * Author       : Dafu Lv
 * Date Created : 21/07/2010
 * Description  : The implementation file of drbg functions in WTM driver
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_func.h"

#include "wtm_os_util.h"

#include "_wtm_crypto.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t wtm_drbg_init(void *arg)
{
    mv_wtm_ioctl_drbg_init_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_drbg_init_param, arg,
            sizeof(mv_wtm_ioctl_drbg_init_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_drbg_init(ss);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_drbg_reseed(void *arg)
{
    mv_wtm_ioctl_drbg_reseed_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_drbg_reseed_param, arg,
            sizeof(mv_wtm_ioctl_drbg_reseed_param),
            p, p_vm);

    if (p->seed) {
        wtm_copy_from_user(g_wtm_drv.key_buf_virt,
                (uint32_t)p->seed, p->seed_sz);
    }

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_drbg_reseed(ss,
            g_wtm_drv.key_buf_phys, (p->seed_sz - 16) << 3);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_drbg_gen_ran(void *arg)
{
    mv_wtm_ioctl_drbg_gen_ran_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_drbg_gen_ran_param, arg,
            sizeof(mv_wtm_ioctl_drbg_gen_ran_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_drbg_gen(ss, g_wtm_drv.out_buf_phys, p->ran_sz << 3);

    mv_wtm_func_destroy_ss(ss);

    wtm_copy_to_user((uint32_t)p->ran, g_wtm_drv.out_buf_virt, p->ran_sz);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

