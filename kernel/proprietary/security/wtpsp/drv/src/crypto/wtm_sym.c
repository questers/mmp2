/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_sym.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of sym scheme in WTM driver
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_ioctl.h"
#include "mv_wtm_func.h"
#include "mv_wtm_xllp.h"

#include "wtm_main.h"
#include "wtm_os_util.h"
#include "wtm_crypto_util.h"

#include "_wtm_crypto.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t wtm_sym_init_nonfips(void *arg)
{
    mv_wtm_ioctl_sym_cntx *cntx;
    wtm_vm cntx_vm = NULL;

    uint32_t delta;

    uint32_t key0_virt = 0, key1_virt = 0, key2_virt = 0, iv_virt = 0;
    uint32_t key0_phys = 0, key1_phys = 0, key2_phys = 0, iv_phys = 0;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_CNTX(
            mv_wtm_ioctl_sym_cntx, arg, sizeof(mv_wtm_ioctl_sym_cntx),
            cntx, cntx_vm);

    /* max size = 64B */
    _ASSERT(cntx->sym_key0);
    {
        key0_virt = g_wtm_drv.key_buf_virt;
        key0_phys = g_wtm_drv.key_buf_phys;
        wtm_copy_from_user(
                key0_virt, (uint32_t)cntx->sym_key0, cntx->sym_key_sz);
    }

    delta = WTM_PAGE_SIZE >> 2;

    if (cntx->sym_key1) {
        key1_virt = g_wtm_drv.key_buf_virt + delta;
        key1_phys = g_wtm_drv.key_buf_phys + delta;
        wtm_copy_from_user(
                key1_virt, (uint32_t)cntx->sym_key1, cntx->sym_key_sz);
    }

    if (cntx->sym_key2) {
        key2_virt = g_wtm_drv.key_buf_virt + (delta << 1);
        key2_phys = g_wtm_drv.key_buf_phys + (delta << 1);
        wtm_copy_from_user(
                key2_virt, (uint32_t)cntx->sym_key2, cntx->sym_key_sz);
    }

    if (cntx->iv) {
        iv_virt = g_wtm_drv.key_buf_virt + (delta << 1) + delta;
        iv_phys = g_wtm_drv.key_buf_phys + (delta << 1) + delta;
        wtm_copy_from_user(iv_virt, (uint32_t)cntx->iv, cntx->iv_sz);
    }

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_sym_init_nonfips(
            ss, cntx->sch, cntx->op,
            key0_phys, key1_phys, key2_phys,
            cntx->sym_key_sz, iv_phys);

    if (ret) {
        mv_wtm_func_destroy_ss(ss);
    } else {
        cntx->wtm_cntx = (uint32_t)ss;
    }

    cntx->wtm_fault = ret;

    _UNREF_CNTX(cntx_vm);

    return RET_VAL(ret);
}

#ifdef CONFIG_EN_FIPS

int32_t wtm_sym_init_fips(void *arg)
{
    mv_wtm_ioctl_sym_cntx *cntx;
    wtm_vm cntx_vm = NULL;

    uint8_t kr_kad[WTM_KAD_SZ], usrk_kad[WTM_KAD_SZ];

    uint32_t delta;

    uint32_t subk_wof_phys_local, usrk_wof_phys_local;
    wtm_vm subk_wof_vm = NULL, usrk_wof_vm = NULL;

    uint32_t iv_virt, iv_phys = 0;

    mv_wtm_ss ss;

    int32_t ret;

    _REF_CNTX(
            mv_wtm_ioctl_sym_cntx, arg, sizeof(mv_wtm_ioctl_sym_cntx),
            cntx, cntx_vm);

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _CALC_KAD(
            cntx->fips_ki.kr_passwd, cntx->fips_ki.kr_passwd_sz,
            cntx->fips_ki.usrk_passwd, cntx->fips_ki.usrk_passwd_sz,
            (uint32_t)kr_kad, (uint32_t)usrk_kad, _out);

    delta = (WTM_PAGE_SIZE >> 1);

    _ASSERT(cntx->fips_ki.subk_wof_sz &&
            cntx->fips_ki.subk_wof_sz <= delta);
    _ASSERT(cntx->fips_ki.usrk_wof_sz &&
            cntx->fips_ki.usrk_wof_sz <= delta);

    _REF_WOF(
            cntx->fips_ki.subk_wof, cntx->fips_ki.subk_wof_sz,
            g_wtm_drv.key_buf_virt, g_wtm_drv.key_buf_phys,
            subk_wof_phys_local, subk_wof_vm);
    _REF_WOF(
            cntx->fips_ki.usrk_wof, cntx->fips_ki.usrk_wof_sz,
            g_wtm_drv.key_buf_virt + delta, g_wtm_drv.key_buf_phys + delta,
            usrk_wof_phys_local, usrk_wof_vm);

    if (cntx->iv_sz) {
        iv_virt = g_wtm_drv.in_buf_virt;
        iv_phys = g_wtm_drv.in_buf_phys;
        wtm_copy_from_user(iv_virt, (uint32_t)cntx->iv, cntx->iv_sz);
    } else {
        iv_phys = 0;
    }

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_sym_init_fips(
            ss, cntx->sch, cntx->op,
            (uint32_t)kr_kad, (uint32_t)usrk_kad,
            subk_wof_phys_local, usrk_wof_phys_local, iv_phys);

    if (ret) {
        mv_wtm_func_destroy_ss(ss);
    } else {
        cntx->wtm_cntx = (uint32_t)ss;
    }

_out:

    cntx->wtm_fault = ret;

    _UNREF_WOF(subk_wof_vm, cntx->fips_ki.subk_wof_sz);
    _UNREF_WOF(usrk_wof_vm, cntx->fips_ki.usrk_wof_sz);

    _UNREF_CNTX(cntx_vm);

    return RET_VAL(ret);
}

#endif /* CONFIG_EN_FIPS */

int32_t wtm_sym_proc(void *arg)
{
    mv_wtm_ioctl_sym_cntx *cntx;
    wtm_vm cntx_vm = NULL;

    uint32_t src_blks_local = 0;
    wtm_vm src_blks_vm = NULL;

    uint32_t dst_blks_local = 0;
    wtm_vm dst_blks_vm = NULL;

    msg_struct ms;
    msg_seq mseq;

    int32_t ret;

    _ASSERT(arg);

    _REF_CNTX(
            mv_wtm_ioctl_sym_cntx, arg, sizeof(mv_wtm_ioctl_sym_cntx),
            cntx, cntx_vm);

    _ASSERT(cntx->src_blks_sz == cntx->dst_blks_sz);

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_MSG(
            cntx->src_blks, cntx->src_blks_sz, MSG_DIR_IN,
            src_blks_local, src_blks_vm, _out);

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_MSG(
            cntx->dst_blks, cntx->dst_blks_sz, MSG_DIR_OUT,
            dst_blks_local, dst_blks_vm, _out);

    /* try msg_struct first */
    ret = wtm_msg_struct_init(
            0, 0,
            src_blks_local, cntx->src_blks_sz,
            dst_blks_local, cntx->dst_blks_sz,
            false, &ms);
    if (0 == ret) {
        ret = mv_wtm_func_sym_proc(
                (mv_wtm_ss)cntx->wtm_cntx,
                (uint32_t)(ms.src_stat ? 0 : ms.src_handle),
                (uint32_t)(ms.src_stat ? ms.src_handle : 0),
                (uint32_t)(ms.dst_stat ? 0 : ms.dst_handle),
                (uint32_t)(ms.dst_stat ? ms.dst_handle : 0),
                cntx->src_blks_sz);
        wtm_msg_struct_proc_output_after_pi(&ms);
        wtm_msg_struct_cleanup(&ms);
    } else {
        /* if failed, turn to msg_seq then */
        ret = wtm_msg_seq_init(
                0, 0,
                src_blks_local, cntx->src_blks_sz,
                dst_blks_local, cntx->dst_blks_sz,
                g_wtm_drv.in_buf_virt, g_wtm_drv.out_buf_virt,
                &mseq);
        _ASSERT(0 == ret);
        do {
            wtm_msg_seq_pre_proc(&mseq);
            ret = mv_wtm_func_sym_proc(
                    (mv_wtm_ss)cntx->wtm_cntx,
                    g_wtm_drv.in_buf_phys, 0,
                    g_wtm_drv.out_buf_phys, 0,
                    mseq.cur_sz);
            if (ret) {
                wtm_msg_seq_cleanup(&mseq);
                goto _out;
            }
            wtm_msg_seq_post_proc(&mseq);
        } while (!wtm_msg_seq_is_done(&mseq));
        wtm_msg_seq_cleanup(&mseq);
    }

_out:

    cntx->wtm_fault = ret;

    _UNREF_MSG(cntx->dst_blks, cntx->dst_blks_sz, MSG_DIR_OUT,
            dst_blks_local, dst_blks_vm);

    _UNREF_MSG(cntx->src_blks, cntx->src_blks_sz, MSG_DIR_IN,
            src_blks_local, src_blks_vm);

    _UNREF_CNTX(cntx_vm);

    return RET_VAL(ret);
}

int32_t wtm_sym_final(void *arg)
{
    mv_wtm_ioctl_sym_cntx *cntx;
    wtm_vm cntx_vm = NULL;

    uint32_t part_blk_local = 0;
    wtm_vm part_blk_vm = NULL;

    uint32_t src_blks_local = 0;
    wtm_vm src_blks_vm = NULL;

    uint32_t dst_blks_local = 0;
    wtm_vm dst_blks_vm = NULL;

    msg_struct ms;
    msg_seq mseq;

    int32_t ret;

    _ASSERT(arg);

    _REF_CNTX(
            mv_wtm_ioctl_sym_cntx, arg, sizeof(mv_wtm_ioctl_sym_cntx),
            cntx, cntx_vm);

    if (cntx->seq_err_flag) {
        ret = mv_wtm_func_hash_kill_seq((mv_wtm_ss)cntx->wtm_cntx);
        _ASSERT(0 == ret);
        goto _seq_err;
    }

    _ASSERT(cntx->src_blks_sz +
            cntx->pb_info.part_blk_sz == cntx->dst_blks_sz);

    _REF_PART_BLK(
            cntx->pb_info.part_blk, cntx->pb_info.part_blk_sz,
            g_wtm_drv.part_blk_virt,
            part_blk_local, part_blk_vm);

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_MSG(
            cntx->src_blks, cntx->src_blks_sz, MSG_DIR_IN,
            src_blks_local, src_blks_vm, _out);

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_MSG(
            cntx->dst_blks, cntx->dst_blks_sz, MSG_DIR_OUT,
            dst_blks_local, dst_blks_vm, _out);

    if (cntx->pb_info.part_blk_sz + cntx->src_blks_sz) {
        /* try msg_struct first */
        ret = wtm_msg_struct_init(
                part_blk_local, cntx->pb_info.part_blk_sz,
                src_blks_local, cntx->src_blks_sz,
                dst_blks_local, cntx->dst_blks_sz,
                false, &ms);
        if (0 == ret) {
            ret = mv_wtm_func_sym_final(
                    (mv_wtm_ss)cntx->wtm_cntx,
                    (uint32_t)(ms.src_stat ? 0 : ms.src_handle),
                    (uint32_t)(ms.src_stat ? ms.src_handle : 0),
                    (uint32_t)(ms.dst_stat ? 0 : ms.dst_handle),
                    (uint32_t)(ms.dst_stat ? ms.dst_handle : 0),
                    cntx->pb_info.part_blk_sz + cntx->src_blks_sz);
            wtm_msg_struct_proc_output_after_pi(&ms);
            wtm_msg_struct_cleanup(&ms);
        } else {
            /* if failed, turn to msg_seq then */
            ret = wtm_msg_seq_init(
                    0, 0,
                    src_blks_local, cntx->src_blks_sz,
                    /* last blk for final */
                    dst_blks_local, cntx->src_blks_sz,
                    g_wtm_drv.in_buf_virt, g_wtm_drv.out_buf_virt,
                    &mseq);
            _ASSERT(0 == ret);
            do {
                wtm_msg_seq_pre_proc(&mseq);
                ret = mv_wtm_func_sym_proc(
                        (mv_wtm_ss)cntx->wtm_cntx,
                        g_wtm_drv.in_buf_phys, 0,
                        g_wtm_drv.out_buf_phys, 0,
                        mseq.cur_sz);
                if (ret) {
                    wtm_msg_seq_cleanup(&mseq);
                    goto _out;
                }
                wtm_msg_seq_post_proc(&mseq);
            } while (!wtm_msg_seq_is_done(&mseq));
            wtm_msg_seq_cleanup(&mseq);

            ret = mv_wtm_func_sym_final(
                    (mv_wtm_ss)cntx->wtm_cntx,
                    wtm_virt_to_phys(part_blk_local), 0,
                    g_wtm_drv.out_buf_phys, 0,
                    cntx->pb_info.part_blk_sz);

            if (cntx->pb_info.part_blk_sz) {
                wtm_memcpy(dst_blks_local + cntx->src_blks_sz,
                        g_wtm_drv.out_buf_virt, cntx->pb_info.part_blk_sz);
            }
        }
    } else {
        /* zero-length msg: fake in/out-buf */
        ret = mv_wtm_func_sym_final(
                (mv_wtm_ss)cntx->wtm_cntx,
                g_wtm_drv.in_buf_phys, 0,
                g_wtm_drv.out_buf_phys, 0,
                0);
    }

_out:

    _UNREF_MSG(cntx->dst_blks, cntx->dst_blks_sz, MSG_DIR_OUT,
            dst_blks_local, dst_blks_vm);

    _UNREF_MSG(cntx->src_blks, cntx->src_blks_sz, MSG_DIR_IN,
            src_blks_local, src_blks_vm);

    _UNREF_PART_BLK(part_blk_vm, cntx->pb_info.part_blk_sz);

_seq_err:

    mv_wtm_func_destroy_ss((mv_wtm_ss)cntx->wtm_cntx);

    cntx->wtm_fault = ret;

    _UNREF_CNTX(cntx_vm);

    return RET_VAL(ret);
}
