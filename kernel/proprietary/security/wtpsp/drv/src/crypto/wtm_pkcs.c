/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_pkcs.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of PKCS in WTM driver
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_ioctl.h"
#include "mv_wtm_func.h"
#include "mv_wtm_xllp.h"

#include "wtm_main.h"
#include "wtm_os_util.h"
#include "wtm_crypto_util.h"

#include "_wtm_crypto.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/* PKCS key is too large, so treat it as a special case */

#define _REF_PKCS_KEY(_key, _key_sz, _key_buf_virt, _key_buf_phys,          \
        _key_virt_local, _key_phys_local, _key_vm)                          \
    do {                                                                    \
        _ASSERT(!_key_vm);                                                  \
        if (wtm_is_mem_direct_ok((uint32_t)_key, _key_sz)) {                \
            _key_vm = wtm_get_user_mem(                                     \
                    _key, _key_sz, (void **)&_key_virt_local);              \
            _ASSERT(_key_vm);                                               \
            _key_phys_local = wtm_virt_to_phys((uint32_t)_key_virt_local);  \
            wtm_flush_dcache(_key_virt_local, _key_sz);                     \
            wtm_flush_l2dcache(_key_virt_local, _key_sz);                   \
        } else {                                                            \
            _key_virt_local = (uint32_t)_key_buf_virt;                      \
            _key_phys_local = (uint32_t)_key_buf_phys;                      \
            wtm_copy_from_user(_key_virt_local, (uint32_t)_key, _key_sz);   \
        }                                                                   \
    } while (0)

#define _UNREF_PKCS_KEY(_key_vm)        \
    do {                                \
        if (_key_vm) {                  \
            wtm_put_user_mem(_key_vm);  \
        }                               \
    } while (0)

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t wtm_pkcs_init_nonfips(void *arg)
{
    mv_wtm_ioctl_pkcs_cntx *cntx;
    wtm_vm cntx_vm = NULL;

    wtm_vm key_mod_vm = NULL;
    uint32_t key_mod_virt, key_mod_phys;

    wtm_vm key_exp_vm = NULL;
    uint32_t key_exp_virt, key_exp_phys;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_CNTX(
            mv_wtm_ioctl_pkcs_cntx, arg, sizeof(mv_wtm_ioctl_pkcs_cntx),
            cntx, cntx_vm);

    _REF_PKCS_KEY(
            cntx->key_mod, cntx->key_sz,
            g_wtm_drv.key_buf_virt,
            g_wtm_drv.key_buf_phys,
            key_mod_virt, key_mod_phys, key_mod_vm);
    _REF_PKCS_KEY(
            cntx->key_exp, cntx->key_sz,
            g_wtm_drv.key_buf_virt + (WTM_PAGE_SIZE >> 1),
            g_wtm_drv.key_buf_phys + (WTM_PAGE_SIZE >> 1),
            key_exp_virt, key_exp_phys, key_exp_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_pkcs_init_nonfips(
            ss, cntx->sch, cntx->op, key_mod_phys, key_exp_phys);

    if (ret) {
        mv_wtm_func_destroy_ss(ss);
    } else {
        cntx->wtm_cntx = (uint32_t)ss;
    }

    cntx->wtm_fault = ret;

    _UNREF_PKCS_KEY(key_mod_vm);
    _UNREF_PKCS_KEY(key_exp_vm);

    _UNREF_CNTX(cntx_vm);

    return RET_VAL(ret);
}

#ifdef CONFIG_EN_FIPS

int32_t wtm_pkcs_init_fips(void *arg)
{
    mv_wtm_ioctl_pkcs_cntx *cntx;
    wtm_vm cntx_vm = NULL;

    uint8_t kr_kad[WTM_KAD_SZ], usrk_kad[WTM_KAD_SZ];

    uint32_t delta;

    uint32_t subk_wof_phys_local, usrk_wof_phys_local;
    wtm_vm subk_wof_vm = NULL, usrk_wof_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_CNTX(
            mv_wtm_ioctl_pkcs_cntx, arg, sizeof(mv_wtm_ioctl_pkcs_cntx),
            cntx, cntx_vm);

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _CALC_KAD(
            cntx->fips_ki.kr_passwd, cntx->fips_ki.kr_passwd_sz,
            cntx->fips_ki.usrk_passwd, cntx->fips_ki.usrk_passwd_sz,
            (uint32_t)kr_kad, (uint32_t)usrk_kad, _out);

    delta = (WTM_PAGE_SIZE >> 1);

    _ASSERT(cntx->fips_ki.subk_wof_sz &&
            cntx->fips_ki.subk_wof_sz <= delta);
    _ASSERT(cntx->fips_ki.usrk_wof_sz &&
            cntx->fips_ki.usrk_wof_sz <= delta);

    _REF_WOF(
            cntx->fips_ki.subk_wof, cntx->fips_ki.subk_wof_sz,
            g_wtm_drv.key_buf_virt, g_wtm_drv.key_buf_phys,
            subk_wof_phys_local, subk_wof_vm);
    _REF_WOF(
            cntx->fips_ki.usrk_wof, cntx->fips_ki.usrk_wof_sz,
            g_wtm_drv.key_buf_virt + delta, g_wtm_drv.key_buf_phys + delta,
            usrk_wof_phys_local, usrk_wof_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_pkcs_init_fips(
            ss, cntx->sch, cntx->op,
            (uint32_t)kr_kad, (uint32_t)usrk_kad,
            subk_wof_phys_local, usrk_wof_phys_local);

    if (ret) {
        mv_wtm_func_destroy_ss(ss);
    } else {
        cntx->wtm_cntx = (uint32_t)ss;
    }

_out:

    cntx->wtm_fault = ret;

    _UNREF_WOF(subk_wof_vm, cntx->fips_ki.subk_wof_sz);
    _UNREF_WOF(usrk_wof_vm, cntx->fips_ki.usrk_wof_sz);

    _UNREF_CNTX(cntx_vm);

    return RET_VAL(ret);
}

#endif /* CONFIG_EN_FIPS */

int32_t wtm_pkcs_update(void *arg)
{
    mv_wtm_ioctl_pkcs_cntx *cntx;
    wtm_vm cntx_vm = NULL;

    uint32_t part_blk_local = 0;
    wtm_vm part_blk_vm = NULL;

    uint32_t src_blks_local = 0;
    wtm_vm src_blks_vm = NULL;

    msg_struct ms;
    msg_seq mseq;

    int32_t ret;

    _ASSERT(arg);

    _REF_CNTX(
            mv_wtm_ioctl_pkcs_cntx, arg, sizeof(mv_wtm_ioctl_pkcs_cntx),
            cntx, cntx_vm);

    _REF_PART_BLK(
            cntx->pb_info.part_blk, cntx->pb_info.part_blk_sz,
            g_wtm_drv.part_blk_virt,
            part_blk_local, part_blk_vm);

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_MSG(
            cntx->src_blks, cntx->src_blks_sz, MSG_DIR_IN,
            src_blks_local, src_blks_vm, _out);

    /* try msg_struct first */
    ret = wtm_msg_struct_init(
            part_blk_local, cntx->pb_info.part_blk_sz,
            src_blks_local, cntx->src_blks_sz,
            0, 0, true, &ms);
    if (0 == ret) {
        ret = mv_wtm_func_pkcs_update(
                (mv_wtm_ss)cntx->wtm_cntx,
                (uint32_t)(ms.src_stat ? 0 : ms.src_handle),
                (uint32_t)(ms.src_stat ? ms.src_handle : 0),
                cntx->pb_info.part_blk_sz + cntx->src_blks_sz);
        wtm_msg_struct_cleanup(&ms);
    } else {
        /* if failed, turn to msg_seq then */
        ret = wtm_msg_seq_init(
                part_blk_local, cntx->pb_info.part_blk_sz,
                src_blks_local, cntx->src_blks_sz,
                0, 0,
                g_wtm_drv.in_buf_virt, 0,
                &mseq);
        _ASSERT(0 == ret);
        do {
            wtm_msg_seq_pre_proc(&mseq);
            ret = mv_wtm_func_pkcs_update(
                    (mv_wtm_ss)cntx->wtm_cntx,
                    g_wtm_drv.in_buf_phys,
                    0,
                    mseq.cur_sz);
            if (ret) {
                wtm_msg_seq_cleanup(&mseq);
                goto _out;
            }
            wtm_msg_seq_post_proc(&mseq);
        } while (!wtm_msg_seq_is_done(&mseq));
        wtm_msg_seq_cleanup(&mseq);
    }

_out:

    cntx->wtm_fault = ret;

    _UNREF_MSG(cntx->src_blks, cntx->src_blks_sz, MSG_DIR_IN,
            src_blks_local, src_blks_vm);

    _UNREF_PART_BLK(part_blk_vm, cntx->pb_info.part_blk_sz);

    _UNREF_CNTX(cntx_vm);

    return RET_VAL(ret);
}

int32_t wtm_pkcs_final(void *arg)
{
    mv_wtm_ioctl_pkcs_cntx *cntx;
    wtm_vm cntx_vm = NULL;

    bool is_valid;

    int32_t ret;

    _ASSERT(arg);

    _REF_CNTX(
            mv_wtm_ioctl_pkcs_cntx, arg, sizeof(mv_wtm_ioctl_pkcs_cntx),
            cntx, cntx_vm);

    if (cntx->seq_err_flag) {
        ret = mv_wtm_func_pkcs_kill_seq((mv_wtm_ss)cntx->wtm_cntx);
        _ASSERT(0 == ret);
        goto _seq_err;
    }

    if (cntx->pb_info.part_blk_sz) {
        /* copy from user directly for better performance */
        wtm_copy_from_user(
                g_wtm_drv.in_buf_virt,
                (uint32_t)cntx->pb_info.part_blk,
                cntx->pb_info.part_blk_sz);
    }

    /* if pkcs verify */
    if (MV_WTM_PKCS_OP_VERF == cntx->op) {
        wtm_copy_from_user(
                g_wtm_drv.out_buf_virt,
                (uint32_t)cntx->sig,
                cntx->sig_sz);
    }

    ret = mv_wtm_func_pkcs_final(
            (mv_wtm_ss)cntx->wtm_cntx,
            g_wtm_drv.in_buf_phys,
            0,
            cntx->pb_info.part_blk_sz,
            g_wtm_drv.out_buf_phys,
            &is_valid);

    if (0 == ret) {
        if (MV_WTM_PKCS_OP_SIGN == cntx->op) {
            /* copy results to the buffer in user space */
            wtm_copy_to_user(
                    (uint32_t)cntx->sig, g_wtm_drv.out_buf_virt, cntx->sig_sz);
        } else {
            cntx->is_valid = is_valid;
        }
    }

_seq_err:

    mv_wtm_func_destroy_ss((mv_wtm_ss)cntx->wtm_cntx);

    cntx->wtm_fault = ret;

    _UNREF_CNTX(cntx_vm);

    return RET_VAL(ret);
}

int32_t wtm_pkcs_op_nonfips(void *arg)
{
    mv_wtm_ioctl_pkcs_cntx *cntx;
    wtm_vm cntx_vm = NULL;

    wtm_vm key_mod_vm = NULL;
    uint32_t key_mod_virt, key_mod_phys;

    wtm_vm key_exp_vm = NULL;
    uint32_t key_exp_virt, key_exp_phys;

    uint32_t src_blks_local = 0;
    wtm_vm src_blks_vm = NULL;

    msg_struct ms;
    msg_seq mseq;

    bool is_valid;

    mv_wtm_ss ss = NULL;

    int32_t ret;

    _ASSERT(arg);

    _REF_CNTX(
            mv_wtm_ioctl_pkcs_cntx, arg, sizeof(mv_wtm_ioctl_pkcs_cntx),
            cntx, cntx_vm);

    _REF_PKCS_KEY(
            cntx->key_mod, cntx->key_sz,
            g_wtm_drv.key_buf_virt,
            g_wtm_drv.key_buf_phys,
            key_mod_virt, key_mod_phys, key_mod_vm);
    _REF_PKCS_KEY(
            cntx->key_exp, cntx->key_sz,
            g_wtm_drv.key_buf_virt + (WTM_PAGE_SIZE >> 1),
            g_wtm_drv.key_buf_phys + (WTM_PAGE_SIZE >> 1),
            key_exp_virt, key_exp_phys, key_exp_vm);

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_MSG(
            cntx->src_blks, cntx->src_blks_sz, MSG_DIR_IN,
            src_blks_local, src_blks_vm, _out);

    /* if pkcs verify */
    if (MV_WTM_PKCS_OP_VERF == cntx->op) {
        wtm_copy_from_user(
                g_wtm_drv.out_buf_virt,
                (uint32_t)cntx->sig,
                cntx->sig_sz);
    }

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    if (cntx->src_blks_sz) {
        /* try msg_struct first */
        ret = wtm_msg_struct_init(
                0, 0,
                src_blks_local, cntx->src_blks_sz,
                0, 0, true, &ms);
        if (0 == ret) {
            ret = mv_wtm_func_pkcs_op_nonfips(
                    ss, cntx->sch, cntx->op, key_mod_phys, key_exp_phys,
                    (uint32_t)(ms.src_stat ? 0 : ms.src_handle),
                    (uint32_t)(ms.src_stat ? ms.src_handle : 0),
                    cntx->src_blks_sz,
                    g_wtm_drv.out_buf_phys, &is_valid);
            wtm_msg_struct_cleanup(&ms);
        } else {
            ret = mv_wtm_func_pkcs_init_nonfips(
                    ss, cntx->sch, cntx->op, key_mod_phys, key_exp_phys);
            if (ret) {
                goto _out;
            }

            /* if failed, turn to msg_seq then */
            ret = wtm_msg_seq_init(
                    0, 0,
                    src_blks_local, cntx->src_blks_sz,
                    0, 0,
                    g_wtm_drv.in_buf_virt, 0,
                    &mseq);
            _ASSERT(0 == ret);

            while (!wtm_msg_seq_is_last_blk_left(&mseq)) {
                wtm_msg_seq_pre_proc(&mseq);
                ret = mv_wtm_func_pkcs_update(
                        ss,
                        g_wtm_drv.in_buf_phys,
                        0,
                        mseq.cur_sz);
                if (ret) {
                    wtm_msg_seq_cleanup(&mseq);
                    goto _out;
                }
                wtm_msg_seq_post_proc(&mseq);
            }

            wtm_msg_seq_pre_proc(&mseq);
            ret = mv_wtm_func_pkcs_final(
                    ss,
                    g_wtm_drv.in_buf_phys,
                    0,
                    mseq.cur_sz,
                    g_wtm_drv.out_buf_phys,
                    &is_valid);
            wtm_msg_seq_post_proc(&mseq);

            wtm_msg_seq_cleanup(&mseq);
        }
    } else {
        /* zero-length msg: fake in-buf */
        ret = mv_wtm_func_pkcs_op_nonfips(
                ss, cntx->sch, cntx->op, key_mod_phys, key_exp_phys,
                g_wtm_drv.in_buf_phys, 0, 0,
                g_wtm_drv.out_buf_phys, &is_valid);
    }

    if (0 == ret) {
        if (MV_WTM_PKCS_OP_SIGN == cntx->op) {
            /* copy results to the buffer in user space */
            wtm_copy_to_user(
                    (uint32_t)cntx->sig, g_wtm_drv.out_buf_virt, cntx->sig_sz);
        } else {
            cntx->is_valid = is_valid;
        }
    }

_out:

    if (ss) {
        mv_wtm_func_destroy_ss(ss);
    }

    cntx->wtm_fault = ret;

    _UNREF_MSG(cntx->src_blks, cntx->src_blks_sz, MSG_DIR_IN,
            src_blks_local, src_blks_vm);

    _UNREF_PKCS_KEY(key_mod_vm);
    _UNREF_PKCS_KEY(key_exp_vm);

    _UNREF_CNTX(cntx_vm);

    return RET_VAL(ret);
}

#ifdef CONFIG_EN_FIPS

int32_t wtm_pkcs_op_fips(void *arg)
{
    mv_wtm_ioctl_pkcs_cntx *cntx;
    wtm_vm cntx_vm = NULL;

    uint8_t kr_kad[WTM_KAD_SZ], usrk_kad[WTM_KAD_SZ];

    uint32_t delta;

    uint32_t subk_wof_phys_local, usrk_wof_phys_local;
    wtm_vm subk_wof_vm = NULL, usrk_wof_vm = NULL;

    uint32_t src_blks_local = 0;
    wtm_vm src_blks_vm = NULL;

    msg_struct ms;
    msg_seq mseq;

    bool is_valid;

    mv_wtm_ss ss = NULL;

    int32_t ret;

    _ASSERT(arg);

    _REF_CNTX(
            mv_wtm_ioctl_pkcs_cntx, arg, sizeof(mv_wtm_ioctl_pkcs_cntx),
            cntx, cntx_vm);

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _CALC_KAD(
            cntx->fips_ki.kr_passwd, cntx->fips_ki.kr_passwd_sz,
            cntx->fips_ki.usrk_passwd, cntx->fips_ki.usrk_passwd_sz,
            (uint32_t)kr_kad, (uint32_t)usrk_kad, _out);

    delta = (WTM_PAGE_SIZE >> 1);

    _ASSERT(cntx->fips_ki.subk_wof_sz &&
            cntx->fips_ki.subk_wof_sz <= delta);
    _ASSERT(cntx->fips_ki.usrk_wof_sz &&
            cntx->fips_ki.usrk_wof_sz <= delta);

    _REF_WOF(
            cntx->fips_ki.subk_wof, cntx->fips_ki.subk_wof_sz,
            g_wtm_drv.key_buf_virt, g_wtm_drv.key_buf_phys,
            subk_wof_phys_local, subk_wof_vm);
    _REF_WOF(
            cntx->fips_ki.usrk_wof, cntx->fips_ki.usrk_wof_sz,
            g_wtm_drv.key_buf_virt + delta, g_wtm_drv.key_buf_phys + delta,
            usrk_wof_phys_local, usrk_wof_vm);

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_MSG(
            cntx->src_blks, cntx->src_blks_sz, MSG_DIR_IN,
            src_blks_local, src_blks_vm, _out);

    /* if pkcs verify */
    if (!cntx->op) {
        wtm_copy_from_user(
                g_wtm_drv.out_buf_virt,
                (uint32_t)cntx->sig,
                cntx->sig_sz);
    }

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    if (cntx->src_blks_sz) {
        /* try msg_struct first */
        ret = wtm_msg_struct_init(
                0, 0,
                src_blks_local, cntx->src_blks_sz,
                0, 0, true, &ms);
        if (0 == ret) {
            ret = mv_wtm_func_pkcs_op_fips(
                    ss, cntx->sch, cntx->op,
                    (uint32_t)kr_kad, (uint32_t)usrk_kad,
                    subk_wof_phys_local, usrk_wof_phys_local,
                    (uint32_t)(ms.src_stat ? 0 : ms.src_handle),
                    (uint32_t)(ms.src_stat ? ms.src_handle : 0),
                    cntx->src_blks_sz,
                    g_wtm_drv.out_buf_phys, &is_valid);
            wtm_msg_struct_cleanup(&ms);
        } else {
            ret = mv_wtm_func_pkcs_init_fips(
                    ss, cntx->sch, cntx->op,
                    (uint32_t)kr_kad, (uint32_t)usrk_kad,
                    subk_wof_phys_local, usrk_wof_phys_local);
            if (ret) {
                goto _out;
            }

            /* if failed ,turn to msg_seq then */
            ret = wtm_msg_seq_init(
                    0, 0,
                    src_blks_local, cntx->src_blks_sz,
                    0, 0,
                    g_wtm_drv.in_buf_virt, 0,
                    &mseq);
            _ASSERT(0 == ret);

            while (!wtm_msg_seq_is_last_blk_left(&mseq)) {
                wtm_msg_seq_pre_proc(&mseq);
                ret = mv_wtm_func_pkcs_update(
                        ss,
                        g_wtm_drv.in_buf_phys,
                        0,
                        mseq.cur_sz);
                if (ret) {
                    wtm_msg_seq_cleanup(&mseq);
                    goto _out;
                }
                wtm_msg_seq_post_proc(&mseq);
            }

            wtm_msg_seq_pre_proc(&mseq);
            ret = mv_wtm_func_pkcs_final(
                    ss,
                    g_wtm_drv.in_buf_phys,
                    0,
                    mseq.cur_sz,
                    g_wtm_drv.out_buf_phys,
                    &is_valid);
            wtm_msg_seq_post_proc(&mseq);

            wtm_msg_seq_cleanup(&mseq);
        }
    } else {
        /* zero-length msg: fake in-buf */
        ret = mv_wtm_func_pkcs_op_fips(
                ss, cntx->sch, cntx->op,
                (uint32_t)kr_kad, (uint32_t)usrk_kad,
                subk_wof_phys_local, usrk_wof_phys_local,
                g_wtm_drv.in_buf_phys, 0, 0,
                g_wtm_drv.out_buf_phys, &is_valid);
    }

    if (0 == ret) {
        if (cntx->op) {
            /* copy results to the buffer in user space */
            wtm_copy_to_user(
                    (uint32_t)cntx->sig, g_wtm_drv.out_buf_virt, cntx->sig_sz);
        } else {
            cntx->is_valid = is_valid;
        }
    }

_out:

    if (ss) {
        mv_wtm_func_destroy_ss(ss);
    }

    cntx->wtm_fault = ret;

    _UNREF_MSG(cntx->src_blks, cntx->src_blks_sz, MSG_DIR_IN,
            src_blks_local, src_blks_vm);

    _UNREF_WOF(subk_wof_vm, cntx->fips_ki.subk_wof_sz);
    _UNREF_WOF(usrk_wof_vm, cntx->fips_ki.usrk_wof_sz);

    _UNREF_CNTX(cntx_vm);

    return RET_VAL(ret);
}

#endif /* CONFIG_EN_FIPS */
