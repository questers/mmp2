/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : _wtm_crypto.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of WTM crypto functions
 *
 */

#ifndef __WTM_CRYPTO_H_
#define __WTM_CRYPTO_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "wtm_main.h"

#include "wtm_crypto_alg.h"
#include "wtm_os_util.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#define _MAX(_x, _y)        (((_x) > (_y)) ? (_x) : (_y))

#define IS_SZ_FOR_CPY(sz)   ((sz) <= g_wtm_drv.pin_min ||    \
                            (sz) > g_wtm_drv.pin_max)

#define MSG_DIR_IN          (0)
#define MSG_DIR_OUT         (1)

/* user memory processing */

/*
 * input:
 * _type - the data type of _arg
 * _arg - the input pointer to cntx
 * _sz - the size of cntx
 *
 * output:
 * _cntx - the output pointer to cntx
 * _cntx_vm - the vm handle of cntx
 */
#define _REF_CNTX(_type, _arg, _sz, _cntx, _cntx_vm)            \
    do {                                                        \
        _cntx_vm = wtm_get_user_mem(arg, _sz, (void **)&_cntx); \
        _ASSERT(_cntx_vm);                                      \
    } while (0)

/*
 * input:
 * _vm - the vm handle of cntx
 */
#define _UNREF_CNTX(_cntx_vm)   wtm_put_user_mem(_cntx_vm)

/* for km only */

#define _REF_PARAM(_type, _arg, _sz, _p, _p_vm)   \
    _REF_CNTX(_type, _arg, _sz, _p, _p_vm)

#define _UNREF_PARAM(_p_vm)     _UNREF_CNTX(_p_vm)

/*
 * input:
 * _wof - the wof
 * _wof_sz - the size of the wof
 * _key_buf_virt - the virtual address of key buffer
 * _key_buf_phys - the physical address of key buffer
 *
 * output:
 * _wof_phys_local - the local pointer to wof for kernel reference
 * _wof_vm - the wof vm handle
 */
#define _REF_WOF(_wof, _wof_sz, _key_buf_virt, _key_buf_phys,               \
        _wof_phys_local, _wof_vm)                                           \
    do {                                                                    \
        if (IS_SZ_FOR_CPY(_wof_sz) ||                                       \
                !wtm_is_mem_direct_ok((uint32_t)_wof, _wof_sz)) {           \
            wtm_copy_from_user(_key_buf_virt, (uint32_t)_wof, _wof_sz);     \
            _wof_phys_local = _key_buf_phys;                                \
            _wof_vm = 0;                                                    \
        } else {                                                            \
            void *_wof_virt_local;                                          \
            _wof_vm = wtm_get_user_mem(                                     \
                    _wof, _wof_sz, (void **)&_wof_virt_local);              \
            _ASSERT(_wof_vm);                                               \
            _wof_phys_local = wtm_virt_to_phys((uint32_t)_wof_virt_local);  \
            wtm_flush_dcache((uint32_t)_wof_virt_local, _wof_sz);           \
            wtm_flush_l2dcache((uint32_t)_wof_virt_local, _wof_sz);         \
        }                                                                   \
    } while (0)

/*
 * input:
 * _wof_vm - the wof vm handle
 * _wof_sz - the size of the wof
 */
#define _UNREF_WOF(_wof_vm, _wof_sz)    \
    do {                                \
        if (_wof_vm) {                  \
            wtm_put_user_mem(_wof_vm);  \
        }                               \
    } while (0)

/*
 * input:
 * _pb - the part-blk
 * _pb_sz - the size of part-blk
 * _pb_static - the static memory for copy_from_user-ing part-blk
 *
 * output:
 * _pb_local - the pointer to part-blk for kernel reference
 * _pb_vm - the vm handle of part-blk
 */
#define _REF_PART_BLK(_pb, _pb_sz, _pb_static, _pb_local, _pb_vm)           \
    do {                                                                    \
        if (_pb_sz) {                                                       \
            if (IS_SZ_FOR_CPY(_pb_sz)) {                                    \
                wtm_copy_from_user(                                         \
                        (uint32_t)_pb_static, (uint32_t)_pb, _pb_sz);       \
                _pb_local = (uint32_t)_pb_static;                           \
                _pb_vm = NULL;                                              \
            } else {                                                        \
                if (wtm_is_mem_direct_ok((uint32_t)_pb, _pb_sz)) {          \
                    _pb_vm = wtm_get_user_mem(                              \
                            _pb, _pb_sz, (void **)&_pb_local);              \
                    _ASSERT(_pb_vm && _pb_local);                           \
                } else { /* no choice but copy */                           \
                    wtm_copy_from_user(                                     \
                            (uint32_t)_pb_static, (uint32_t)_pb, _pb_sz);   \
                    _pb_local = (uint32_t)_pb_static;                       \
                    _pb_vm = NULL;                                          \
                }                                                           \
            }                                                               \
        }                                                                   \
    } while (0)

/*
 * input:
 * _pb_vm - the part-blk vm handle
 * _pb_sz - the size of part-blk
 */
#define _UNREF_PART_BLK(_pb_vm, _pb_sz)             \
    do {                                            \
        if (_pb_sz) {                               \
            if (!IS_SZ_FOR_CPY(_pb_sz) && _pb_vm) { \
                wtm_put_user_mem(_pb_vm);           \
            }                                       \
        }                                           \
    } while (0)

/*
 * input:
 * _msg - the msg to validate
 * _msg_sz - the size of msg
 *
 * output:
 * _msg_local - the pointer to msg for kernel reference
 * _msg_vm - the vm handle of msg
 *
 * _fail - the failure point to jump
 */
#define _REF_MSG(_msg, _msg_sz, _msg_dir, _msg_local, _msg_vm, _fail)   \
    do {                                                                \
        if (_msg_sz) {                                                  \
            if (IS_SZ_FOR_CPY(_msg_sz)) {                               \
                _msg_local = wtm_alloc_mem(_msg_sz);                    \
                if (0 == _msg_local) {                                  \
                    goto _fail;                                         \
                }                                                       \
                if (MSG_DIR_IN == _msg_dir) {                           \
                    wtm_copy_from_user(                                 \
                            _msg_local, (uint32_t)_msg, _msg_sz);       \
                }                                                       \
            } else {                                                    \
                _msg_vm = wtm_get_user_mem(                             \
                        _msg, _msg_sz, (void **)&_msg_local);           \
                _ASSERT(_msg_vm);                                       \
            }                                                           \
        }                                                               \
    } while (0)

/*
 * input:
 * _msg - the msg to validate
 * _msg_sz - the size of msg
 * _msg_local - the pointer to msg for kernel reference
 * _msg_vm - the vm handle of msg
 */
#define _UNREF_MSG(_msg, _msg_sz, _msg_dir, _msg_local, _msg_vm)    \
    do {                                                            \
        if (_msg_sz) {                                              \
            if (IS_SZ_FOR_CPY(_msg_sz)) {                           \
                _ASSERT(_msg_local);                                \
                if (MSG_DIR_OUT == _msg_dir) {                      \
                    wtm_copy_to_user(                               \
                            (uint32_t)_msg, _msg_local, _msg_sz);   \
                }                                                   \
                wtm_free_mem(_msg_local);                           \
            } else {                                                \
                wtm_put_user_mem(_msg_vm);                          \
            }                                                       \
        }                                                           \
    } while (0)

/* calculate kad for fips init */

/*
 * input:
 * _kr_pass - the keyring password
 * _kr_pass_sz - the size of the keyring password
 * _usrk_pass - the usrkey password
 * _usrk_pass_sz - the size of the usrkey password
 * 
 * output:
 * _kr_kad - the key_auth_data of the keyring
 * _usrk_kad - the key_auth_data of the usrkey
 *
 * _fail - the failure point to jump
 */
#define _CALC_KAD(                                                      \
        _kr_pass, _kr_pass_sz, _usrk_pass, _usrk_pass_sz,               \
        _kr_kad, _usrk_kad, _fail)                                      \
    do {                                                                \
        uint32_t pass_local;                                            \
        int32_t rv;                                                     \
        pass_local = wtm_alloc_mem(_MAX(_kr_pass_sz, _usrk_pass_sz));   \
        if (0 == pass_local) {                                          \
            goto _fail;                                                 \
        }                                                               \
        if (0 != _kr_kad) {                                             \
            wtm_copy_from_user(                                         \
                    pass_local, (uint32_t)_kr_pass, _kr_pass_sz);       \
            rv = wtm_sw_sha256_dgst(                                    \
                    pass_local, _kr_pass_sz, _kr_kad);                  \
            _ASSERT(0 == rv);                                           \
        }                                                               \
        if (0 != _usrk_kad) {                                           \
            wtm_copy_from_user(                                         \
                    pass_local, (uint32_t)_usrk_pass, _usrk_pass_sz);   \
            rv = wtm_sw_sha256_dgst(                                    \
                    pass_local, _usrk_pass_sz, _usrk_kad);              \
            _ASSERT(0 == rv);                                           \
        }                                                               \
        wtm_free_mem(pass_local);                                       \
    } while (0)

/*
 * input:
 * _fips_ki_usr - fips key info from user space
 * _fips_ki_func - fips key info for func layer
 * _subk_wof_virt - virt addr of subkey wof
 * _usrk_wof_virt - virt addr of usrkey wof
 *
 * _fail - the failure point to jump
 */
#define _REF_FIPS_KI(_fips_ki_usr, _fips_ki_func,                       \
        _subk_wof_virt, _subk_wof_phys,                                 \
        _usrk_wof_virt, _usrk_wof_phys, _fail)                          \
    do {                                                                \
        _CALC_KAD((_fips_ki_usr)->kr_passwd,                            \
                (_fips_ki_usr)->kr_passwd_sz,                           \
                (_fips_ki_usr)->usrk_passwd,                            \
                (_fips_ki_usr)->usrk_passwd_sz,                         \
                (_fips_ki_func)->kr_kad_virt,                           \
                (_fips_ki_func)->uk_kad_virt,                           \
                _fail);                                                 \
        if (_subk_wof_virt) {                                           \
            wtm_copy_from_user(_subk_wof_virt,                          \
                    (uint32_t)(_fips_ki_usr)->subk_wof,                 \
                    (long_t)(_fips_ki_usr)->subk_wof_sz);               \
            (_fips_ki_func)->subk_wof_phys = _subk_wof_phys;            \
            (_fips_ki_func)->subk_wof_sz = (_fips_ki_usr)->subk_wof_sz; \
        }                                                               \
        if (_usrk_wof_virt) {                                           \
            wtm_copy_from_user(_usrk_wof_virt,                          \
                    (uint32_t)(_fips_ki_usr)->usrk_wof,                 \
                    (long_t)(_fips_ki_usr)->usrk_wof_sz);               \
            (_fips_ki_func)->usrk_wof_phys = _usrk_wof_phys;            \
            (_fips_ki_func)->usrk_wof_sz = (_fips_ki_usr)->usrk_wof_sz; \
        }                                                               \
    } while (0)

/*
 * input:
 * _fips_ki_func - fips key info for func layer
 */
#define _UNREF_FIPS_KI(_fips_ki_func)   /* nothing */

/* EC_DSA domain parameter is 200B, too large to copy */
/*
 * input:
 * _dmn - domain param from user space
 * _dmn_buf_virt - virt addr of global buf
 * _dmn_buf_phys - phys addr of global buf
 *
 * output:
 * _dmn_virt_local - virt ptr to local domain info
 * _dmn_phys_local - phys ptr to local domain info
 * _dmn_vm - the vm handle of domain info
 */
#define _REF_DMN(_dmn, _dmn_buf_virt, _dmn_buf_phys,                \
        _dmn_virt_local, _dmn_phys_local, _dmn_vm)                  \
    do {                                                            \
        _ASSERT(!_dmn_vm);                                          \
        if (_dmn) {                                                 \
            if (wtm_is_mem_direct_ok((uint32_t)_dmn,                \
                        sizeof(mv_wtm_eccp_domain_param))) {        \
                _dmn_vm = wtm_get_user_mem(_dmn,                    \
                        sizeof(mv_wtm_eccp_domain_param),           \
                        (void **)&_dmn_virt_local);                 \
                _ASSERT(_dmn_vm);                                   \
                _dmn_phys_local = wtm_virt_to_phys(                 \
                        (uint32_t)_dmn_virt_local);                 \
                wtm_flush_dcache(_dmn_virt_local,                   \
                        sizeof(mv_wtm_eccp_domain_param));          \
                wtm_flush_l2dcache(_dmn_virt_local,                 \
                        sizeof(mv_wtm_eccp_domain_param));          \
            } else {                                                \
                _dmn_virt_local = (uint32_t)_dmn_buf_virt;          \
                _dmn_phys_local = (uint32_t)_dmn_buf_phys;          \
                wtm_copy_from_user(_dmn_virt_local, (uint32_t)_dmn, \
                        sizeof(mv_wtm_eccp_domain_param));          \
            }                                                       \
        }                                                           \
    } while (0)

#define _UNREF_DMN(_dmn_vm)             \
    do {                                \
        if (_dmn_vm) {                  \
            wtm_put_user_mem(_dmn_vm);  \
        }                               \
    } while (0)

#define RET_VAL(_ret)   ((_ret) ? -1 : 0)

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#endif /* __WTM_CRYPTO_H_ */
