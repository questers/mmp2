/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_km.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of key management in WTM driver
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_ioctl.h"
#include "mv_wtm_func.h"
#include "mv_wtm_xllp.h"

#include "wtm_main.h"
#include "wtm_os_util.h"
#include "wtm_crypto_util.h"

#include "_wtm_crypto.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#define WTM_IS_KSCH_EC_DSA(_ksch)   (0x0000B000 == ((_ksch) & 0x0000F000))
#define WTM_IS_KSCH_RSA(_ksch)      (0x0000A000 == ((_ksch) & 0x0000F000))
#define WTM_IS_KSCH_SYM(_ksch)      \
    (!WTM_IS_KSCH_EC_DSA(_ksch)) && !(WTM_IS_KSCH_RSA(_ksch))

#undef WTM_DBG_DUMP_WOF

#define _INPUT  (0)
#define _OUTPUT (1)

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

static void _wtm_pr_wof(
        uint8_t *type, const uint8_t *func, bool dir,
        uint8_t *wof, uint32_t wof_sz)
{
    uint32_t *ptr = (uint32_t *)wof;
    uint32_t wof_dw_sz = wof_sz >> 2;
    uint32_t i, j;

    _ASSERT(!(wof_sz % 4));

    wtm_pr("\n");
    dir ? wtm_pr("WOF - %s O - %s\n", type, func) :
        wtm_pr("WOF - %s I - %s \n", type, func);
    for (i = 0; i < (wof_dw_sz >> 2); i++) {
        wtm_pr("0x%08x 0x%08x 0x%08x 0x%08x\n",
                ptr[i << 2], ptr[(i << 2) + 1],
                ptr[(i << 2) + 2], ptr[(i << 2) + 3]);
    }
    for (j = 0; j < wof_dw_sz - (i << 2); j++) {
        j ? wtm_pr(" ") : j;
        wtm_pr("0x%08x", ptr[(i << 2) + j]);
    }
    j ? wtm_pr("\n") : j;
    wtm_pr("\n");
}

#endif

/* available in both non-fips and fips */

static int32_t _wtm_create_ec_key_pair_nonfips(
        mv_wtm_ss ss, mv_wtm_ioctl_create_ec_key_pair_param *p)
{
    uint32_t delta = WTM_PAGE_SIZE >> 2;

    wtm_vm dmn_vm = NULL;
    uint32_t dmn_virt = 0, dmn_phys = 0;

    int32_t ret;

    _REF_DMN(
            p->domain_param,
            g_wtm_drv.key_buf_virt,
            g_wtm_drv.key_buf_phys,
            dmn_virt, dmn_phys, dmn_vm);

    wtm_copy_from_user(g_wtm_drv.key_buf_virt + delta,
            (uint32_t)&(p->pub_key->sz_in_bits), sizeof(uint32_t));

    ret = mv_wtm_func_create_ec_key_pair_nonfips(ss,
            p->ksch, dmn_phys, p->mode,
            g_wtm_drv.key_buf_phys + (delta << 1),
            g_wtm_drv.key_buf_phys + delta);

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->prv_key,
                g_wtm_drv.key_buf_virt + (delta << 1),
                p->prv_key_sz);
        wtm_copy_to_user((uint32_t)p->pub_key,
                g_wtm_drv.key_buf_virt + delta,
                sizeof(mv_wtm_eccp_pub_key_info));
    }

    _UNREF_DMN(dmn_vm);

    return ret;
}

static int32_t _wtm_derivate_ec_pub_key_nonfips(
        mv_wtm_ss ss, mv_wtm_ioctl_derivate_ec_pub_key_param *p)
{
    uint32_t delta = WTM_PAGE_SIZE >> 2;

    wtm_vm dmn_vm = NULL;
    uint32_t dmn_virt = 0, dmn_phys = 0;

    int32_t ret;

    _REF_DMN(
            p->domain_param,
            g_wtm_drv.key_buf_virt,
            g_wtm_drv.key_buf_phys,
            dmn_virt, dmn_phys, dmn_vm);

    wtm_copy_from_user(g_wtm_drv.key_buf_virt + delta,
            (uint32_t)p->prv_key, p->prv_key_sz);

    ret = mv_wtm_func_derivate_ec_pub_key_nonfips(ss,
            p->ksch, dmn_phys, p->mode,
            g_wtm_drv.key_buf_phys + delta,
            g_wtm_drv.key_buf_phys + (delta << 1));

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->pub_key,
                g_wtm_drv.key_buf_virt + (delta << 1),
                sizeof(mv_wtm_eccp_pub_key_info));
    }

    _UNREF_DMN(dmn_vm);

    return ret;
}

#ifdef CONFIG_EN_FIPS

static int32_t _wtm_create_ec_key_pair_fips(
        mv_wtm_ss ss, mv_wtm_ioctl_create_ec_key_pair_param *p)
{
    uint32_t delta0 = WTM_PAGE_SIZE >> 2;
    uint32_t delta1 = WTM_PAGE_SIZE >> 1;

    uint8_t endk_kr_kad[WTM_KAD_SZ], endk_uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info endk_info_func, *p_endk_info_func;

    wtm_vm dmn_vm = NULL;
    uint32_t dmn_virt = 0, dmn_phys = 0;

    uint8_t kr_kad[WTM_KAD_SZ], uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info fips_ki_func;

    int32_t ret;

    if (p->is_stage_dd) {
        _ASSERT(WTM_SYM_WOF_SZ <= p->endk_info.subk_wof_sz);
        _ASSERT(WTM_SYM_WOF_SZ <= p->endk_info.usrk_wof_sz);

        endk_info_func.kr_kad_virt = (uint32_t)endk_kr_kad;
        endk_info_func.uk_kad_virt = (uint32_t)endk_uk_kad;

        ret = MV_WTM_ERR_OUT_OF_MEM;
        _REF_FIPS_KI(&p->endk_info, &endk_info_func,
                g_wtm_drv.key_buf_virt,
                g_wtm_drv.key_buf_phys,
                g_wtm_drv.key_buf_virt + delta0,
                g_wtm_drv.key_buf_phys + delta0,
                _out0);

        endk_info_func.subk_wof_sz = WTM_SYM_WOF_SZ;
        endk_info_func.usrk_wof_sz = WTM_SYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

        _wtm_pr_wof("ES", __FUNCTION__, _INPUT,
                p->endk_info.subk_wof, p->endk_info.subk_wof_sz);
        _wtm_pr_wof("EU", __FUNCTION__, _INPUT,
                p->endk_info.usrk_wof, p->endk_info.usrk_wof_sz);

#endif

        p_endk_info_func = &endk_info_func;
    } else {
        p_endk_info_func = NULL;
    }

    _REF_DMN(
            p->domain_param,
            g_wtm_drv.key_buf_virt + (delta0 << 1),
            g_wtm_drv.key_buf_phys + (delta0 << 1),
            dmn_virt, dmn_phys, dmn_vm);

    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.subk_wof_sz);
    _ASSERT(WTM_ASYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);

    fips_ki_func.kr_kad_virt = (uint32_t)kr_kad;
    fips_ki_func.uk_kad_virt = (uint32_t)uk_kad;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->fips_ki, &fips_ki_func,
            g_wtm_drv.key_buf_virt + (delta0 << 1) + delta0,
            g_wtm_drv.key_buf_phys + (delta0 << 1) + delta0,
            0, 0, _out1);

    fips_ki_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    fips_ki_func.usrk_wof_sz = WTM_ASYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("S", __FUNCTION__, _INPUT,
            p->fips_ki.subk_wof, p->fips_ki.subk_wof_sz);

#endif

    ret = mv_wtm_func_create_ec_usrkey(ss,
            p_endk_info_func, p->ksch, dmn_phys, p->mode, p->key_id,
            fips_ki_func.kr_kad_virt, fips_ki_func.uk_kad_virt,
            g_wtm_drv.key_buf_phys + (delta0 << 1) + delta0,    /* subk_wof */
            fips_ki_func.subk_wof_sz,
            g_wtm_drv.out_buf_phys,                             /* usrk_wof */
            fips_ki_func.usrk_wof_sz,
            g_wtm_drv.out_buf_phys + delta1                 /* pub key */
            );

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->fips_ki.usrk_wof,
                g_wtm_drv.out_buf_virt,
                p->fips_ki.usrk_wof_sz);
        wtm_copy_to_user((uint32_t)p->pub_key,
                g_wtm_drv.out_buf_virt + delta1,
                sizeof(mv_wtm_eccp_pub_key_info));

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

        _wtm_pr_wof("U", __FUNCTION__, _OUTPUT,
                p->fips_ki.usrk_wof, p->fips_ki.usrk_wof_sz);

#endif
    }

    _UNREF_FIPS_KI(&fips_ki_func);

_out1:

    _UNREF_DMN(dmn_vm);

    if (p->is_stage_dd) {
        _UNREF_FIPS_KI(&endk_info_func);
    }

_out0:

    return ret;
}

static int32_t _wtm_derivate_ec_pub_key_fips(
        mv_wtm_ss ss, mv_wtm_ioctl_derivate_ec_pub_key_param *p)
{
    uint32_t delta = WTM_PAGE_SIZE >> 2;

    wtm_vm dmn_vm = NULL;
    uint32_t dmn_virt = 0, dmn_phys = 0;

    uint8_t kr_kad[WTM_KAD_SZ], uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info fips_ki_func;

    int32_t ret;

    _REF_DMN(
            p->domain_param,
            g_wtm_drv.key_buf_virt,
            g_wtm_drv.key_buf_phys,
            dmn_virt, dmn_phys, dmn_vm);

    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.subk_wof_sz);
    _ASSERT(WTM_ASYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);

    fips_ki_func.kr_kad_virt = (uint32_t)kr_kad;
    fips_ki_func.uk_kad_virt = (uint32_t)uk_kad;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->fips_ki, &fips_ki_func,
            g_wtm_drv.key_buf_virt + delta,
            g_wtm_drv.key_buf_phys + delta,
            g_wtm_drv.key_buf_virt + (delta << 1),
            g_wtm_drv.key_buf_phys + (delta << 1),
            _out);

    fips_ki_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    fips_ki_func.usrk_wof_sz = WTM_ASYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

   _wtm_pr_wof("S", __FUNCTION__, _INPUT,
           p->fips_ki.subk_wof, p->fips_ki.subk_wof_sz);
   _wtm_pr_wof("U", __FUNCTION__, _INPUT,
           p->fips_ki.usrk_wof, p->fips_ki.usrk_wof_sz);

#endif

    ret = mv_wtm_func_derivate_ec_pub_key(ss,
            p->ksch, g_wtm_drv.key_buf_phys, p->mode,
            fips_ki_func.kr_kad_virt, fips_ki_func.uk_kad_virt,
            g_wtm_drv.key_buf_phys + delta,
            fips_ki_func.subk_wof_sz,
            g_wtm_drv.key_buf_phys + (delta << 1),
            fips_ki_func.usrk_wof_sz,
            g_wtm_drv.key_buf_phys + (delta << 1) + delta);

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->pub_key,
                g_wtm_drv.key_buf_virt + (delta << 1) + delta,
                sizeof(mv_wtm_eccp_pub_key_info));
    }

    _UNREF_FIPS_KI(&fips_ki_func);

_out:

    _UNREF_DMN(dmn_vm);

    return ret;
}

#endif /* CONFIG_EN_FIPS */

int32_t wtm_create_ec_key_pair(void *arg)
{
    mv_wtm_ioctl_create_ec_key_pair_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_create_ec_key_pair_param, arg,
            sizeof(mv_wtm_ioctl_create_ec_key_pair_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    if (p->is_nonfips) {
        ret = _wtm_create_ec_key_pair_nonfips(ss, p);
    } else {
#ifdef CONFIG_EN_FIPS
        ret = _wtm_create_ec_key_pair_fips(ss, p);
#else
        ret = -1; /* to depress warnings */
        _ASSERT(0);
#endif
    }

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_derivate_ec_pub_key(void *arg)
{
    mv_wtm_ioctl_derivate_ec_pub_key_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_derivate_ec_pub_key_param, arg,
            sizeof(mv_wtm_ioctl_derivate_ec_pub_key_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    if (p->is_nonfips) {
        ret = _wtm_derivate_ec_pub_key_nonfips(ss, p);
    } else {
#ifdef CONFIG_EN_FIPS
        ret = _wtm_derivate_ec_pub_key_fips(ss, p);
#else
        ret = -1; /* to depress warnings */
        _ASSERT(0);
#endif
    }

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_create_dh_sys_args(void *arg)
{
    mv_wtm_ioctl_gen_dh_sys_args_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    uint32_t delta = WTM_PAGE_SIZE >> 1;

    int32_t ret;

    _REF_PARAM(
            mv_wtm_ioctl_gen_dh_sys_args_param, arg,
            sizeof(mv_wtm_ioctl_gen_dh_sys_args_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_gen_dh_sys_param(ss,
            p->ksch, g_wtm_drv.out_buf_phys, g_wtm_drv.out_buf_phys + delta);

    mv_wtm_func_destroy_ss(ss);

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->p,
                g_wtm_drv.out_buf_virt, p->p_sz);
        wtm_copy_to_user((uint32_t)p->g,
                g_wtm_drv.out_buf_virt + delta, p->g_sz);
    }

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

static int32_t _wtm_create_dh_key_pair_nonfips(
        mv_wtm_ss ss, mv_wtm_ioctl_create_dh_key_pair_param *p)
{
    uint32_t delta = WTM_PAGE_SIZE >> 2;
    int32_t ret;

    wtm_copy_from_user(g_wtm_drv.key_buf_virt,
            (uint32_t)p->p, p->p_sz);
    wtm_copy_from_user(g_wtm_drv.key_buf_virt + delta,
            (uint32_t)p->g, p->g_sz);

    ret = mv_wtm_func_drbg_gen(
            ss, g_wtm_drv.key_buf_phys + (delta << 1) + delta, p->p_sz << 3);

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->prv_key,
                g_wtm_drv.key_buf_virt + (delta << 1) + delta,
                p->prv_key_sz);
    } else {
        goto _out;
    }

    ret = mv_wtm_func_derivate_dh_pub_key_nonfips(
            ss, p->ksch,
            g_wtm_drv.key_buf_phys, g_wtm_drv.key_buf_phys + delta,
            g_wtm_drv.key_buf_phys + (delta << 1) + delta,
            g_wtm_drv.key_buf_phys + (delta << 1));

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->pub_key,
                g_wtm_drv.key_buf_virt + (delta << 1),
                p->pub_key_sz);
    }

_out:

    return ret;
}

static int32_t _wtm_derivate_dh_pub_key_nonfips(
        mv_wtm_ss ss, mv_wtm_ioctl_derivate_dh_pub_key_param *p)
{
    uint32_t delta = WTM_PAGE_SIZE >> 2;
    int32_t ret;

    wtm_copy_from_user(g_wtm_drv.key_buf_virt,
            (uint32_t)p->p, p->p_sz);
    wtm_copy_from_user(g_wtm_drv.key_buf_virt + delta,
            (uint32_t)p->g, p->g_sz);

    ret = mv_wtm_func_derivate_dh_pub_key_nonfips(
            ss, p->ksch,
            g_wtm_drv.key_buf_phys, g_wtm_drv.key_buf_phys + delta,
            g_wtm_drv.key_buf_phys + (delta << 1) + delta,
            g_wtm_drv.key_buf_phys + (delta << 1));

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->pub_key,
                g_wtm_drv.key_buf_virt + (delta << 1),
                p->pub_key_sz);
    }

    return ret;
}

#ifdef CONFIG_EN_FIPS

static int32_t _wtm_create_dh_key_pair_fips(
        mv_wtm_ss ss, mv_wtm_ioctl_create_dh_key_pair_param *p)
{
    uint32_t delta0 = WTM_PAGE_SIZE >> 2;
    uint32_t delta1 = WTM_PAGE_SIZE >> 1;
    uint32_t delta2 = WTM_PAGE_SIZE >> 1;

    uint8_t endk_kr_kad[WTM_KAD_SZ], endk_uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info endk_info_func, *p_endk_info_func;

    uint8_t kr_kad[WTM_KAD_SZ], uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info fips_ki_func;

    int32_t ret;

    if (p->is_stage_dd) {
        _ASSERT(WTM_SYM_WOF_SZ <= p->endk_info.subk_wof_sz);
        _ASSERT(WTM_SYM_WOF_SZ <= p->endk_info.usrk_wof_sz);

        endk_info_func.kr_kad_virt = (uint32_t)endk_kr_kad;
        endk_info_func.uk_kad_virt = (uint32_t)endk_uk_kad;

        ret = MV_WTM_ERR_OUT_OF_MEM;
        _REF_FIPS_KI(&p->endk_info, &endk_info_func,
                g_wtm_drv.key_buf_virt,
                g_wtm_drv.key_buf_phys,
                g_wtm_drv.key_buf_virt + delta0,
                g_wtm_drv.key_buf_phys + delta0,
                _out0);

        endk_info_func.subk_wof_sz = WTM_SYM_WOF_SZ;
        endk_info_func.usrk_wof_sz = WTM_SYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

        _wtm_pr_wof("ES", __FUNCTION__, _INPUT,
                p->endk_info.subk_wof, p->endk_info.subk_wof_sz);
        _wtm_pr_wof("EU", __FUNCTION__, _INPUT,
                p->endk_info.usrk_wof, p->endk_info.usrk_wof_sz);

#endif

        p_endk_info_func = &endk_info_func;
    } else {
        p_endk_info_func = NULL;
    }

    wtm_copy_from_user(
            g_wtm_drv.in_buf_virt, (uint32_t)p->p, p->p_sz);
    wtm_copy_from_user(
            g_wtm_drv.in_buf_virt + delta1, (uint32_t)p->g, p->g_sz);

    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.subk_wof_sz);
    _ASSERT(WTM_ASYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);

    fips_ki_func.kr_kad_virt = (uint32_t)kr_kad;
    fips_ki_func.uk_kad_virt = (uint32_t)uk_kad;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->fips_ki, &fips_ki_func,
            g_wtm_drv.key_buf_virt + (delta0 << 1) + delta0,
            g_wtm_drv.key_buf_phys + (delta0 << 1) + delta0,
            0, 0, _out1);

    fips_ki_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    fips_ki_func.usrk_wof_sz = WTM_ASYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("S", __FUNCTION__, _INPUT,
            p->fips_ki.subk_wof, p->fips_ki.subk_wof_sz);

#endif

    ret = mv_wtm_func_create_dh_usrkey(ss,
            p_endk_info_func, p->ksch,
            g_wtm_drv.in_buf_virt,
            g_wtm_drv.in_buf_virt + delta1,
            p->key_id,
            fips_ki_func.kr_kad_virt, fips_ki_func.uk_kad_virt,
            g_wtm_drv.key_buf_phys + (delta0 << 1) + delta0,
            fips_ki_func.subk_wof_sz,
            g_wtm_drv.out_buf_phys,
            fips_ki_func.usrk_wof_sz,
            g_wtm_drv.out_buf_phys + delta2
            );

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->fips_ki.usrk_wof,
                g_wtm_drv.out_buf_virt,
                p->fips_ki.usrk_wof_sz);
        wtm_copy_to_user((uint32_t)p->pub_key,
                g_wtm_drv.out_buf_virt + delta2,
                p->pub_key_sz);
    }

    _UNREF_FIPS_KI(&fips_ki_func);

_out1:

    if (p->is_stage_dd) {
        _UNREF_FIPS_KI(&endk_info_func);
    }

_out0:

    return ret;
}

static int32_t _wtm_derivate_dh_pub_key_fips(
        mv_wtm_ss ss, mv_wtm_ioctl_derivate_dh_pub_key_param *p)
{
    uint32_t delta0 = WTM_PAGE_SIZE >> 1;
    uint32_t delta1 = WTM_PAGE_SIZE >> 2;

    uint8_t kr_kad[WTM_KAD_SZ], uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info fips_ki_func;

    int32_t ret;

    wtm_copy_from_user(
            g_wtm_drv.in_buf_virt, (uint32_t)p->p, p->p_sz);
    wtm_copy_from_user(
            g_wtm_drv.in_buf_virt + delta0, (uint32_t)p->g, p->g_sz);

    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.subk_wof_sz);
    _ASSERT(WTM_ASYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);

    fips_ki_func.kr_kad_virt = (uint32_t)kr_kad;
    fips_ki_func.uk_kad_virt = (uint32_t)uk_kad;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->fips_ki, &fips_ki_func,
            g_wtm_drv.key_buf_virt + delta1,
            g_wtm_drv.key_buf_phys + delta1,
            g_wtm_drv.key_buf_virt + (delta1 << 1),
            g_wtm_drv.key_buf_phys + (delta1 << 1),
            _out);

    fips_ki_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    fips_ki_func.usrk_wof_sz = WTM_ASYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

   _wtm_pr_wof("S", __FUNCTION__, _INPUT,
           p->fips_ki.subk_wof, p->fips_ki.subk_wof_sz);
   _wtm_pr_wof("U", __FUNCTION__, _INPUT,
           p->fips_ki.usrk_wof, p->fips_ki.usrk_wof_sz);

#endif

   ret = mv_wtm_func_derivate_dh_pub_key(ss,
           p->ksch, g_wtm_drv.in_buf_virt, g_wtm_drv.in_buf_virt + delta0,
           fips_ki_func.kr_kad_virt, fips_ki_func.uk_kad_virt,
           g_wtm_drv.key_buf_phys + delta1, fips_ki_func.subk_wof_sz,
           g_wtm_drv.key_buf_phys + (delta1 << 1), fips_ki_func.usrk_wof_sz,
           g_wtm_drv.key_buf_phys + (delta1 << 1) + delta1);

   if (!ret) {
       wtm_copy_to_user((uint32_t)p->pub_key,
               g_wtm_drv.key_buf_virt + (delta1 << 1) + delta1,
               p->pub_key_sz);
   }

   _UNREF_FIPS_KI(&fips_ki_func);

_out:

   return ret;
}

#endif /* CONFIG_EN_FIPS */

int32_t wtm_create_dh_key_pair(void *arg)
{
    mv_wtm_ioctl_create_dh_key_pair_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_create_dh_key_pair_param, arg,
            sizeof(mv_wtm_ioctl_create_dh_key_pair_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    if (p->is_nonfips) {
        ret = _wtm_create_dh_key_pair_nonfips(ss, p);
    } else {
#ifdef CONFIG_EN_FIPS
        ret = _wtm_create_dh_key_pair_fips(ss, p);
#else
        ret = -1; /* to depress warnings */
        _ASSERT(0);
#endif
    }

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_derivate_dh_pub_key(void *arg)
{
    mv_wtm_ioctl_derivate_dh_pub_key_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_derivate_dh_pub_key_param, arg,
            sizeof(mv_wtm_ioctl_derivate_dh_pub_key_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    if (p->is_nonfips) {
        ret = _wtm_derivate_dh_pub_key_nonfips(ss, p);
    } else {
#ifdef CONFIG_EN_FIPS
        ret = _wtm_derivate_dh_pub_key_fips(ss, p);
#else
        ret = -1; /* to depress warnings */
        _ASSERT(0);
#endif
    }

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

static int32_t _wtm_create_rsa_key_pair_nonfips(
        mv_wtm_ss ss, mv_wtm_ioctl_create_rsa_key_pair_param *p)
{
    uint32_t delta = WTM_PAGE_SIZE >> 3; /* buf req <= 512B */

    int32_t ret;

    wtm_copy_from_user(g_wtm_drv.key_buf_virt + delta,
            (uint32_t)p->pub_key, p->pub_key_sz);

    ret = mv_wtm_func_create_rsa_key_pair_nonfips(ss,
            p->ksch,
            g_wtm_drv.key_buf_phys,                         /* prv_key_exp */
            g_wtm_drv.key_buf_phys + delta,                 /* pub_key_exp */
            g_wtm_drv.key_buf_phys + (delta << 1),          /* key_mod */
            g_wtm_drv.key_buf_phys + (delta << 1) + delta,  /* p */
            g_wtm_drv.key_buf_phys + (delta << 2),          /* q */
            p->trials, p->limit);

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->prv_key,
                g_wtm_drv.key_buf_virt,
                p->prv_key_sz);
        wtm_copy_to_user((uint32_t)p->pub_key,
                g_wtm_drv.key_buf_virt + delta,
                p->pub_key_sz);
        wtm_copy_to_user((uint32_t)p->key_mod,
                g_wtm_drv.key_buf_virt + (delta << 1),
                p->key_mod_sz);
        wtm_copy_to_user((uint32_t)p->p,
                g_wtm_drv.key_buf_virt + (delta << 1) + delta,
                p->p_sz);
        wtm_copy_to_user((uint32_t)p->q,
                g_wtm_drv.key_buf_virt + (delta << 2),
                p->q_sz);
    }

    return ret;
}

#ifdef CONFIG_EN_FIPS

static int32_t _wtm_create_rsa_key_pair_fips(
        mv_wtm_ss ss, mv_wtm_ioctl_create_rsa_key_pair_param *p)
{
    uint32_t delta0 = WTM_PAGE_SIZE >> 2;
    uint32_t delta1 = WTM_PAGE_SIZE >> 1;

    uint8_t endk_kr_kad[WTM_KAD_SZ], endk_uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info endk_info_func, *p_endk_info_func;

    uint8_t kr_kad[WTM_KAD_SZ], uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info fips_ki_func;

    int32_t ret;

    if (p->is_stage_dd) {
        _ASSERT(WTM_SYM_WOF_SZ <= p->endk_info.subk_wof_sz);
        _ASSERT(WTM_SYM_WOF_SZ <= p->endk_info.usrk_wof_sz);

        endk_info_func.kr_kad_virt = (uint32_t)endk_kr_kad;
        endk_info_func.uk_kad_virt = (uint32_t)endk_uk_kad;

        ret = MV_WTM_ERR_OUT_OF_MEM;
        _REF_FIPS_KI(&p->endk_info, &endk_info_func,
                g_wtm_drv.key_buf_virt,
                g_wtm_drv.key_buf_phys,
                g_wtm_drv.key_buf_virt + delta0,
                g_wtm_drv.key_buf_phys + delta0,
                _out0);

        endk_info_func.subk_wof_sz = WTM_SYM_WOF_SZ;
        endk_info_func.usrk_wof_sz = WTM_SYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

        _wtm_pr_wof("ES", __FUNCTION__, _INPUT,
                p->endk_info.subk_wof, p->endk_info.subk_wof_sz);
        _wtm_pr_wof("EU", __FUNCTION__, _INPUT,
                p->endk_info.usrk_wof, p->endk_info.usrk_wof_sz);

#endif

        p_endk_info_func = &endk_info_func;
    } else {
        p_endk_info_func = NULL;
    }

    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.subk_wof_sz);
    _ASSERT(WTM_ASYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);

    fips_ki_func.kr_kad_virt = (uint32_t)kr_kad;
    fips_ki_func.uk_kad_virt = (uint32_t)uk_kad;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->fips_ki, &fips_ki_func,
            g_wtm_drv.key_buf_virt + (delta0 << 1),
            g_wtm_drv.key_buf_phys + (delta0 << 1),
            0, 0, _out1);

    fips_ki_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    fips_ki_func.usrk_wof_sz = WTM_ASYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("S", __FUNCTION__, _INPUT,
            p->fips_ki.subk_wof, p->fips_ki.subk_wof_sz);

#endif

    /* pub key exp is input */
    wtm_copy_from_user(g_wtm_drv.key_buf_virt + (delta0 << 1) + delta0,
            (uint32_t)p->pub_key, p->pub_key_sz);

    ret = mv_wtm_func_create_rsa_usrkey(ss,
            p_endk_info_func, p->ksch, p->key_id,
            fips_ki_func.kr_kad_virt, fips_ki_func.uk_kad_virt,
            g_wtm_drv.key_buf_phys + (delta0 << 1),         /* subk_wof */
            fips_ki_func.subk_wof_sz,
            g_wtm_drv.out_buf_phys,                         /* usrk_wof */
            fips_ki_func.usrk_wof_sz,
            g_wtm_drv.key_buf_phys + (delta0 << 1) + delta0,    /* pubk_exp */
            g_wtm_drv.out_buf_phys + delta1,                /* key mod */
            p->trials, p->limit);

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->fips_ki.usrk_wof,
                g_wtm_drv.out_buf_virt,
                p->fips_ki.usrk_wof_sz);
        wtm_copy_to_user((uint32_t)p->key_mod,
                g_wtm_drv.out_buf_virt + delta1,
                p->key_mod_sz);

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

        _wtm_pr_wof("U", __FUNCTION__, _OUTPUT,
                p->fips_ki.usrk_wof, p->fips_ki.usrk_wof_sz);

#endif
    }

    _UNREF_FIPS_KI(&fips_ki_func);

_out1:

    if (p->is_stage_dd) {
        _UNREF_FIPS_KI(&endk_info_func);
    }

_out0:

    return ret;
}

#endif /* CONFIG_EN_FIPS */

int32_t wtm_create_rsa_key_pair(void *arg)
{
    mv_wtm_ioctl_create_rsa_key_pair_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_create_rsa_key_pair_param, arg,
            sizeof(mv_wtm_ioctl_create_rsa_key_pair_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    if (p->is_nonfips) {
        ret = _wtm_create_rsa_key_pair_nonfips(ss, p);
    } else {
#ifdef CONFIG_EN_FIPS
        ret = _wtm_create_rsa_key_pair_fips(ss, p);
#else
        ret = -1; /* to depress warnings */
        _ASSERT(0);
#endif
    }

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

#ifdef CONFIG_EN_FIPS

int32_t wtm_create_subkey(void *arg)
{
    mv_wtm_ioctl_create_key_param *p;
    wtm_vm p_vm = NULL;

    uint32_t delta = WTM_PAGE_SIZE >> 2;

    uint8_t endk_kr_kad[WTM_KAD_SZ], endk_uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info endk_info_func, *p_endk_info_func;

    uint8_t kr_kad[WTM_KAD_SZ];
    mv_wtm_key_info fips_ki_func;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_create_key_param, arg,
            sizeof(mv_wtm_ioctl_create_key_param),
            p, p_vm);

    if (p->is_stage_dd) {
        _ASSERT(WTM_SYM_WOF_SZ <= p->endk_info.subk_wof_sz);
        _ASSERT(WTM_SYM_WOF_SZ <= p->endk_info.usrk_wof_sz);

        endk_info_func.kr_kad_virt = (uint32_t)endk_kr_kad;
        endk_info_func.uk_kad_virt = (uint32_t)endk_uk_kad;

        ret = MV_WTM_ERR_OUT_OF_MEM;
        _REF_FIPS_KI(&p->endk_info, &endk_info_func,
                g_wtm_drv.key_buf_virt, g_wtm_drv.key_buf_phys,
                g_wtm_drv.key_buf_virt + delta, g_wtm_drv.key_buf_phys + delta,
                _out0);

        endk_info_func.subk_wof_sz = WTM_SYM_WOF_SZ;
        endk_info_func.usrk_wof_sz = WTM_SYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

        _wtm_pr_wof("ES", __FUNCTION__, _INPUT,
                p->endk_info.subk_wof, p->endk_info.subk_wof_sz);
        _wtm_pr_wof("EU", __FUNCTION__, _INPUT,
                p->endk_info.usrk_wof, p->endk_info.usrk_wof_sz);

#endif

        p_endk_info_func = &endk_info_func;
    } else {
        p_endk_info_func = NULL;
    }

    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.subk_wof_sz);

    fips_ki_func.kr_kad_virt = (uint32_t)kr_kad;
    fips_ki_func.uk_kad_virt = 0;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->fips_ki, &fips_ki_func, 0, 0, 0, 0, _out1);

    fips_ki_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    fips_ki_func.usrk_wof_sz = 0;

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_create_subkey(
            ss, p_endk_info_func, p->key_id,
            fips_ki_func.kr_kad_virt, g_wtm_drv.key_buf_phys + (delta << 1),
            fips_ki_func.subk_wof_sz);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->fips_ki.subk_wof,
                g_wtm_drv.key_buf_virt + (delta << 1),
                fips_ki_func.subk_wof_sz);

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

        _wtm_pr_wof("S", __FUNCTION__, _OUTPUT,
                p->fips_ki.subk_wof, p->fips_ki.subk_wof_sz);

#endif
    }

_out1:

    _UNREF_FIPS_KI(&fips_ki_func);

_out0:

    if (p->is_stage_dd) {
        _UNREF_FIPS_KI(p_endk_info_func);
    }

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

static int32_t _wtm_enroll_usrkey(void *arg)
{
    mv_wtm_ioctl_enroll_key_param *p;
    wtm_vm p_vm = NULL;

    uint8_t kr_kad[WTM_KAD_SZ], uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info fips_ki_func;

    uint32_t delta = WTM_PAGE_SIZE >> 2;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_enroll_key_param, arg,
            sizeof(mv_wtm_ioctl_enroll_key_param),
            p, p_vm);

    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.subk_wof_sz);
    if (WTM_IS_KSCH_SYM(p->ksch)) {
        _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);
    } else {
        _ASSERT(WTM_ASYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);
    }

    fips_ki_func.kr_kad_virt = (uint32_t)kr_kad;
    fips_ki_func.uk_kad_virt = (uint32_t)uk_kad;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->fips_ki, &fips_ki_func,
            g_wtm_drv.key_buf_virt, g_wtm_drv.key_buf_phys,
            0, 0, _out);

    fips_ki_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    fips_ki_func.usrk_wof_sz = (WTM_IS_KSCH_SYM(p->ksch) ?
        WTM_SYM_WOF_SZ : WTM_ASYM_WOF_SZ);

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("S", __FUNCTION__, _INPUT,
            p->fips_ki.subk_wof, p->fips_ki.subk_wof_sz);

#endif

    /* max size of "usrk_passwd" is (WTM_PAGE_SIZE >> 2) */
    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt + (delta << 1),
            (uint32_t)p->usrk, p->usrk_sz);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    if (WTM_IS_KSCH_SYM(p->ksch)) {
        ret = mv_wtm_func_enroll_sym_usrkey(
                ss, p->ksch, p->key_id,
                fips_ki_func.kr_kad_virt, fips_ki_func.uk_kad_virt,
                g_wtm_drv.key_buf_phys, fips_ki_func.subk_wof_sz,
                g_wtm_drv.key_buf_phys + (delta << 1), p->usrk_sz,
                g_wtm_drv.key_buf_phys + (delta << 1) + delta,
                fips_ki_func.usrk_wof_sz);
    } else if (WTM_IS_KSCH_EC_DSA(p->ksch)) {
        ret = mv_wtm_func_enroll_ec_usrkey(
                ss, p->ksch, p->key_id,
                fips_ki_func.kr_kad_virt, fips_ki_func.uk_kad_virt,
                g_wtm_drv.key_buf_phys, fips_ki_func.subk_wof_sz,
                g_wtm_drv.key_buf_phys + (delta << 1), p->usrk_sz,
                g_wtm_drv.key_buf_phys + (delta << 1) + delta,
                fips_ki_func.usrk_wof_sz);
    } else if (WTM_IS_KSCH_RSA(p->ksch)) {
        ret = mv_wtm_func_enroll_rsa_usrkey(
                ss, p->ksch, p->key_id,
                fips_ki_func.kr_kad_virt, fips_ki_func.uk_kad_virt,
                g_wtm_drv.key_buf_phys, fips_ki_func.subk_wof_sz,
                g_wtm_drv.key_buf_phys + (delta << 1), p->usrk_sz,
                g_wtm_drv.key_buf_phys + (delta << 1) + delta,
                fips_ki_func.usrk_wof_sz);
    } else {
        _ASSERT(0);
    }

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->fips_ki.usrk_wof,
                g_wtm_drv.key_buf_virt + (delta << 1) + delta,
                fips_ki_func.usrk_wof_sz);

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

        _wtm_pr_wof("U", __FUNCTION__, _OUTPUT,
                p->fips_ki.usrk_wof, p->fips_ki.usrk_wof_sz);

#endif
    }

    _UNREF_FIPS_KI(&fips_ki_func);

_out:

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_enroll_sym_usrkey(void *arg)
{
    return _wtm_enroll_usrkey(arg);
}

int32_t wtm_enroll_ec_usrkey(void *arg)
{
    return _wtm_enroll_usrkey(arg);
}

int32_t wtm_enroll_dh_usrkey(void *arg)
{
    return _wtm_enroll_usrkey(arg);
}

int32_t wtm_enroll_rsa_usrkey(void *arg)
{
    return _wtm_enroll_usrkey(arg);
}

int32_t wtm_create_sym_usrkey(void *arg)
{
    mv_wtm_ioctl_create_key_param *p;
    wtm_vm p_vm = NULL;

    uint32_t delta = WTM_PAGE_SIZE >> 2;

    uint8_t endk_kr_kad[WTM_KAD_SZ], endk_uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info endk_info_func, *p_endk_info_func;

    uint8_t kr_kad[WTM_KAD_SZ], uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info fips_ki_func;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_create_key_param, arg,
            sizeof(mv_wtm_ioctl_create_key_param),
            p, p_vm);

    if (p->is_stage_dd) {
        _ASSERT(WTM_SYM_WOF_SZ <= p->endk_info.subk_wof_sz);
        _ASSERT(WTM_SYM_WOF_SZ <= p->endk_info.usrk_wof_sz);

        endk_info_func.kr_kad_virt = (uint32_t)endk_kr_kad;
        endk_info_func.uk_kad_virt = (uint32_t)endk_uk_kad;

        ret = MV_WTM_ERR_OUT_OF_MEM;
        _REF_FIPS_KI(&p->endk_info, &endk_info_func,
                g_wtm_drv.key_buf_virt, g_wtm_drv.key_buf_phys,
                g_wtm_drv.key_buf_virt + delta, g_wtm_drv.key_buf_phys + delta,
                _out0);

        endk_info_func.subk_wof_sz = WTM_SYM_WOF_SZ;
        endk_info_func.usrk_wof_sz = WTM_SYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

        _wtm_pr_wof("ES", __FUNCTION__, _INPUT,
                p->endk_info.subk_wof, p->endk_info.subk_wof_sz);
        _wtm_pr_wof("EU", __FUNCTION__, _INPUT,
                p->endk_info.usrk_wof, p->endk_info.usrk_wof_sz);

#endif

        p_endk_info_func = &endk_info_func;
    } else {
        p_endk_info_func = NULL;
    }

    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.subk_wof_sz);
    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);

    fips_ki_func.kr_kad_virt = (uint32_t)kr_kad;
    fips_ki_func.uk_kad_virt = (uint32_t)uk_kad;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->fips_ki, &fips_ki_func,
            g_wtm_drv.key_buf_virt + (delta << 1),
            g_wtm_drv.key_buf_phys + (delta << 1),
            0, 0, _out1);

    fips_ki_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    fips_ki_func.usrk_wof_sz = WTM_SYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("S", __FUNCTION__, _INPUT,
            p->fips_ki.subk_wof, p->fips_ki.subk_wof_sz);

#endif

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_create_sym_usrkey(
            ss, p_endk_info_func, p->ksch, p->key_id,
            fips_ki_func.kr_kad_virt, fips_ki_func.uk_kad_virt,
            g_wtm_drv.key_buf_phys + (delta << 1),
            fips_ki_func.subk_wof_sz,
            g_wtm_drv.key_buf_phys + (delta << 1) + delta,
            fips_ki_func.usrk_wof_sz);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->fips_ki.usrk_wof,
                g_wtm_drv.key_buf_virt + (delta << 1) + delta,
                fips_ki_func.usrk_wof_sz);

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

        _wtm_pr_wof("U", __FUNCTION__, _OUTPUT,
                p->fips_ki.usrk_wof, p->fips_ki.usrk_wof_sz);

#endif
    }

_out1:

    _UNREF_FIPS_KI(&fips_ki_func);

_out0:

    if (p->is_stage_dd) {
        _UNREF_FIPS_KI(p_endk_info_func);
    }

    return RET_VAL(ret);
}

static int32_t _wtm_create_ec_dh_shared_key_nonfips(
        mv_wtm_ss ss, mv_wtm_ioctl_create_ec_dh_shared_key_param *p)
{
    uint32_t delta = WTM_PAGE_SIZE >> 2;

    wtm_vm dmn_vm = NULL;
    uint32_t dmn_virt = 0, dmn_phys = 0;

    int32_t ret;

    _REF_DMN(
            p->domain_param,
            g_wtm_drv.key_buf_virt,
            g_wtm_drv.key_buf_phys,
            dmn_virt, dmn_phys, dmn_vm);

    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt + delta,
            (uint32_t)p->prv_key,
            p->prv_key_sz);

    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt + (delta << 1),
            (uint32_t)p->peer_pub_key,
            sizeof(mv_wtm_eccp_pub_key_info));

    ret = mv_wtm_func_create_ec_dh_shared_key_nonfips(ss,
            p->ksch,
            g_wtm_drv.key_buf_phys, p->mode,
            g_wtm_drv.key_buf_phys + delta,
            g_wtm_drv.key_buf_phys + (delta << 1),
            g_wtm_drv.key_buf_phys + (delta << 1) + delta);

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->shrd_key,
                g_wtm_drv.key_buf_virt + (delta << 1) + delta,
                p->shrd_key_sz);
    }

    _UNREF_DMN(dmn_vm);

    return ret;
}

static int32_t _wtm_create_ec_dh_shared_key_fips(
        mv_wtm_ss ss, mv_wtm_ioctl_create_ec_dh_shared_key_param *p)
{
    uint32_t delta = WTM_PAGE_SIZE >> 2;

    uint8_t ec_dh_kr_kad[WTM_KAD_SZ], ec_dh_uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info ec_dh_info_func;

    uint8_t kr_kad[WTM_KAD_SZ];
    mv_wtm_key_info fips_ki_func;

    int32_t ret;

    _ASSERT(WTM_SYM_WOF_SZ <= p->ec_dh_fips_ki.subk_wof_sz);
    _ASSERT(WTM_ASYM_WOF_SZ <= p->ec_dh_fips_ki.usrk_wof_sz);

    ec_dh_info_func.kr_kad_virt = (uint32_t)ec_dh_kr_kad;
    ec_dh_info_func.uk_kad_virt = (uint32_t)ec_dh_uk_kad;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->ec_dh_fips_ki, &ec_dh_info_func,
            g_wtm_drv.key_buf_virt,
            g_wtm_drv.key_buf_phys,
            g_wtm_drv.key_buf_virt + delta,
            g_wtm_drv.key_buf_phys + delta,
            _out0);

    ec_dh_info_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    ec_dh_info_func.usrk_wof_sz = WTM_ASYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("EDS", __FUNCTION__, _INPUT,
            p->ec_dh_fips_ki.subk_wof, p->ec_dh_fips_ki.subk_wof_sz);
    _wtm_pr_wof("EDU", __FUNCTION__, _INPUT,
            p->ec_dh_fips_ki.usrk_wof, p->ec_dh_fips_ki.usrk_wof_sz);

#endif

    wtm_copy_from_user(
        g_wtm_drv.key_buf_virt + (delta << 1),
        (uint32_t)p->peer_pub_key,
        sizeof(mv_wtm_eccp_pub_key_info));

    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.subk_wof_sz);
    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);

    fips_ki_func.kr_kad_virt = (uint32_t)kr_kad;
    fips_ki_func.uk_kad_virt = 0;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->fips_ki, &fips_ki_func,
            g_wtm_drv.key_buf_virt + (delta << 1) + delta,
            g_wtm_drv.key_buf_phys + (delta << 1) + delta,
            0, 0, _out1);

    fips_ki_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    fips_ki_func.usrk_wof_sz = WTM_SYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("S", __FUNCTION__, _INPUT,
            p->fips_ki.subk_wof, p->fips_ki.subk_wof_sz);

#endif

    ret = mv_wtm_func_create_ec_dh_shared_usrkey(ss,
            p->ec_dh_ksch, &ec_dh_info_func,
            g_wtm_drv.key_buf_phys + (delta << 1),          /* peer pub key */
            p->ksch, p->key_id,
            fips_ki_func.kr_kad_virt,
            g_wtm_drv.key_buf_phys + (delta << 1) + delta,  /* sym subk wof */
            fips_ki_func.subk_wof_sz,
            g_wtm_drv.out_buf_phys,                         /* sym usrk wof */
            fips_ki_func.usrk_wof_sz);

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->fips_ki.usrk_wof,
                g_wtm_drv.out_buf_virt,
                fips_ki_func.usrk_wof_sz);

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("U", __FUNCTION__, _OUTPUT,
            p->fips_ki.usrk_wof, p->fips_ki.usrk_wof_sz);

#endif
    }

_out1:

    _UNREF_FIPS_KI(&fips_ki_func);

_out0:

    _UNREF_FIPS_KI(&ec_dh_info_func);

    return ret;
}

int32_t wtm_create_ec_dh_shared_key(void *arg)
{
    mv_wtm_ioctl_create_ec_dh_shared_key_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_create_ec_dh_shared_key_param, arg,
            sizeof(mv_wtm_ioctl_create_ec_dh_shared_key_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    if (p->is_nonfips) {
        ret = _wtm_create_ec_dh_shared_key_nonfips(ss, p);
    } else {
#ifdef CONFIG_EN_FIPS
        ret = _wtm_create_ec_dh_shared_key_fips(ss, p);
#else
        ret = -1; /* to depress warnings */
        _ASSERT(0);
#endif
    }

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t _wtm_create_dh_shared_key_nonfips(
        mv_wtm_ss ss, mv_wtm_ioctl_create_dh_shared_key_param *p)
{
    uint32_t delta0 = WTM_PAGE_SIZE >> 1;
    uint32_t delta1 = WTM_PAGE_SIZE >> 2;

    int32_t ret;

    wtm_copy_from_user(
            g_wtm_drv.in_buf_virt,
            (uint32_t)p->p,
            p->p_sz);
    /* g is ignored */
    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt,
            (uint32_t)p->prv_key,
            p->prv_key_sz);
    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt + delta1,
            (uint32_t)p->peer_pub_key,
            p->peer_pub_key_sz);

    ret = mv_wtm_func_create_dh_shared_key_nonfips(ss,
            p->ksch,
            g_wtm_drv.in_buf_phys,
            g_wtm_drv.in_buf_phys + delta0, /* fake input g */
            g_wtm_drv.key_buf_phys,
            g_wtm_drv.key_buf_phys + delta1,
            g_wtm_drv.key_buf_phys + (delta1 << 1));

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->shrd_key,
                g_wtm_drv.key_buf_virt + (delta1 << 1),
                p->shrd_key_sz);
    }

    return ret;
}

int32_t _wtm_create_dh_shared_key_fips(
        mv_wtm_ss ss, mv_wtm_ioctl_create_dh_shared_key_param *p)
{
    uint32_t delta0 = WTM_PAGE_SIZE >> 1;
    uint32_t delta1 = WTM_PAGE_SIZE >> 2;

    uint8_t dh_kr_kad[WTM_KAD_SZ], dh_uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info dh_info_func;

    uint8_t kr_kad[WTM_KAD_SZ];
    mv_wtm_key_info fips_ki_func;

    int32_t ret;

    wtm_copy_from_user(g_wtm_drv.in_buf_virt, (uint32_t)p->p, p->p_sz);

    _ASSERT(WTM_SYM_WOF_SZ <= p->dh_fips_ki.subk_wof_sz);
    _ASSERT(WTM_ASYM_WOF_SZ <= p->dh_fips_ki.usrk_wof_sz);

    dh_info_func.kr_kad_virt = (uint32_t)dh_kr_kad;
    dh_info_func.uk_kad_virt = (uint32_t)dh_uk_kad;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->dh_fips_ki, &dh_info_func,
            g_wtm_drv.key_buf_virt,
            g_wtm_drv.key_buf_phys,
            g_wtm_drv.key_buf_virt + delta1,
            g_wtm_drv.key_buf_phys + delta1,
            _out0);

    dh_info_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    dh_info_func.usrk_wof_sz = WTM_ASYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("EDS", __FUNCTION__, _INPUT,
            p->dh_fips_ki.subk_wof, p->dh_fips_ki.subk_wof_sz);
    _wtm_pr_wof("EDU", __FUNCTION__, _INPUT,
            p->dh_fips_ki.usrk_wof, p->dh_fips_ki.usrk_wof_sz);

#endif

    wtm_copy_from_user(
        g_wtm_drv.key_buf_virt + (delta1 << 1),
        (uint32_t)p->peer_pub_key,
        p->peer_pub_key_sz);

    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.subk_wof_sz);
    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);

    fips_ki_func.kr_kad_virt = (uint32_t)kr_kad;
    fips_ki_func.uk_kad_virt = 0;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->fips_ki, &fips_ki_func,
            g_wtm_drv.key_buf_virt + (delta1 << 1) + delta1,
            g_wtm_drv.key_buf_phys + (delta1 << 1) + delta1,
            0, 0, _out1);

    fips_ki_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    fips_ki_func.usrk_wof_sz = WTM_SYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("S", __FUNCTION__, _INPUT,
            p->fips_ki.subk_wof, p->fips_ki.subk_wof_sz);

#endif

    ret = mv_wtm_func_create_dh_shared_usrkey(ss,
            p->dh_ksch, &dh_info_func,
            g_wtm_drv.key_buf_phys + (delta1 << 1),          /* peer pub key */
            p->ksch, p->key_id,
            g_wtm_drv.in_buf_phys,
            g_wtm_drv.in_buf_phys + delta0,                 /* fake input g */
            fips_ki_func.kr_kad_virt,
            fips_ki_func.uk_kad_virt,
            g_wtm_drv.key_buf_phys + (delta1 << 1) + delta1,    /* sym subk wof */
            fips_ki_func.subk_wof_sz,
            g_wtm_drv.out_buf_phys,                         /* sym usrk wof */
            fips_ki_func.usrk_wof_sz);

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->fips_ki.usrk_wof,
                g_wtm_drv.out_buf_virt,
                fips_ki_func.usrk_wof_sz);

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("U", __FUNCTION__, _OUTPUT,
            p->fips_ki.usrk_wof, p->fips_ki.usrk_wof_sz);

#endif
    }

_out1:

    _UNREF_FIPS_KI(&fips_ki_func);

_out0:

    _UNREF_FIPS_KI(&dh_info_func);

    return ret;
}

int32_t wtm_create_dh_shared_key(void *arg)
{
    mv_wtm_ioctl_create_dh_shared_key_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_create_dh_shared_key_param, arg,
            sizeof(mv_wtm_ioctl_create_dh_shared_key_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    if (p->is_nonfips) {
        ret = _wtm_create_dh_shared_key_nonfips(ss, p);
    } else {
#ifdef CONFIG_EN_FIPS
        ret = _wtm_create_dh_shared_key_fips(ss, p);
#else
        ret = -1; /* to depress warnings */
        _ASSERT(0);
#endif
    }

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_pr_ec256_dec(void *arg)
{
    mv_wtm_ioctl_pr_ec256_decrypt_keys_param *p;
    wtm_vm p_vm = NULL;

    uint32_t delta0 = (WTM_PAGE_SIZE >> 2);
    uint32_t delta1 = (WTM_PAGE_SIZE >> 1);
    uint32_t delta2 = (WTM_PAGE_SIZE >> 1);

    uint8_t ec256_kr_kad[WTM_KAD_SZ], ec256_uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info ec256_ki;

    uint8_t sym_kr_kad_lsb[WTM_KAD_SZ];
    mv_wtm_key_info sym_ki_lsb;

    uint8_t sym_kr_kad_msb[WTM_KAD_SZ];
    mv_wtm_key_info sym_ki_msb;

    uint32_t ec_point0_virt, ec_point0_phys;
    uint32_t ec_point1_virt, ec_point1_phys;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_prov_post_proc_param, arg,
            sizeof(mv_wtm_ioctl_prov_post_proc_param),
            p, p_vm);

    _ASSERT(p->ec256_ki.subk_wof);
    _ASSERT(WTM_SYM_WOF_SZ <= p->ec256_ki.subk_wof_sz);
    _ASSERT(p->ec256_ki.usrk_wof);
    _ASSERT(WTM_ASYM_WOF_SZ <= p->ec256_ki.usrk_wof_sz);

    ec256_ki.kr_kad_virt = (uint32_t)ec256_kr_kad;
    ec256_ki.uk_kad_virt = (uint32_t)ec256_uk_kad;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->ec256_ki, &ec256_ki,
            g_wtm_drv.key_buf_virt, g_wtm_drv.key_buf_phys,
            g_wtm_drv.key_buf_virt + delta0, g_wtm_drv.key_buf_phys + delta0,
            _out0);
    ec256_ki.subk_wof_sz = WTM_SYM_WOF_SZ;
    ec256_ki.usrk_wof_sz = WTM_ASYM_WOF_SZ;

    _ASSERT(p->sym_ki_lsb.subk_wof);
    _ASSERT(WTM_SYM_WOF_SZ <= p->sym_ki_lsb.subk_wof_sz);
    _ASSERT(p->sym_ki_lsb.usrk_wof);
    _ASSERT(WTM_SYM_WOF_SZ <= p->sym_ki_lsb.usrk_wof_sz);

    sym_ki_lsb.kr_kad_virt = (uint32_t)sym_kr_kad_lsb;
    sym_ki_lsb.uk_kad_virt = 0;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->sym_ki_lsb, &sym_ki_lsb,
            g_wtm_drv.key_buf_virt + (delta0 << 1),
            g_wtm_drv.key_buf_phys + (delta0 << 1),
            0, 0, _out1);
    sym_ki_lsb.subk_wof_sz = WTM_SYM_WOF_SZ;
    sym_ki_lsb.usrk_wof_sz = WTM_SYM_WOF_SZ;

    _ASSERT(p->sym_ki_msb.subk_wof);
    _ASSERT(WTM_SYM_WOF_SZ <= p->sym_ki_msb.subk_wof_sz);
    _ASSERT(p->sym_ki_msb.usrk_wof);
    _ASSERT(WTM_SYM_WOF_SZ <= p->sym_ki_msb.usrk_wof_sz);

    sym_ki_msb.kr_kad_virt = (uint32_t)sym_kr_kad_msb;
    sym_ki_msb.uk_kad_virt = 0;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->sym_ki_msb, &sym_ki_msb,
            g_wtm_drv.key_buf_virt + (delta0 << 1) + delta0,
            g_wtm_drv.key_buf_phys + (delta0 << 1) + delta0,
            0, 0, _out2);
    sym_ki_msb.subk_wof_sz = WTM_SYM_WOF_SZ;
    sym_ki_msb.usrk_wof_sz = WTM_SYM_WOF_SZ;

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ec_point0_phys = g_wtm_drv.in_buf_phys;
    ec_point0_virt = g_wtm_drv.in_buf_virt;

    wtm_copy_from_user(
            ec_point0_virt,
            (uint32_t)p->ec_point0,
            sizeof(mv_wtm_eccp_pub_key_info));

    ec_point1_phys = g_wtm_drv.in_buf_phys + delta2;
    ec_point1_virt = g_wtm_drv.in_buf_virt + delta2;

    wtm_copy_from_user(
            ec_point1_virt,
            (uint32_t)p->ec_point1,
            sizeof(mv_wtm_eccp_pub_key_info));

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("S", __FUNCTION__, _INPUT,
            p->ec256_ki.subk_wof, p->ec256_ki.subk_wof_sz);
    _wtm_pr_wof("U", __FUNCTION__, _INPUT,
            p->ec256_ki.usrk_wof, p->ec256_ki.usrk_wof_sz);

    _wtm_pr_wof("SS0", __FUNCTION__, _INPUT,
            p->sym_ki_lsb.subk_wof, p->sym_ki_lsb.subk_wof_sz);
    _wtm_pr_wof("SS1", __FUNCTION__, _INPUT,
            p->sym_ki_msb.subk_wof, p->sym_ki_msb.subk_wof_sz);

#endif

    ret = mv_wtm_func_pr_ec256_dec(ss,
            ec256_ki.kr_kad_virt, ec256_ki.uk_kad_virt,
            g_wtm_drv.key_buf_phys, ec256_ki.subk_wof_sz,
            g_wtm_drv.key_buf_phys + delta0, ec256_ki.usrk_wof_sz,
            ec_point0_phys, ec_point1_phys,
            p->sym_ksch_lsb, p->sym_key_id_lsb,
            sym_ki_lsb.kr_kad_virt,
            g_wtm_drv.key_buf_phys + (delta0 << 1), sym_ki_lsb.subk_wof_sz,
            g_wtm_drv.out_buf_phys, p->sym_ki_lsb.usrk_wof_sz,
            p->sym_ksch_msb, p->sym_key_id_msb,
            sym_ki_msb.kr_kad_virt,
            g_wtm_drv.key_buf_phys + (delta0 << 1) + delta0, sym_ki_msb.subk_wof_sz,
            g_wtm_drv.out_buf_phys + delta1, p->sym_ki_msb.usrk_wof_sz);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->sym_ki_lsb.usrk_wof,
                        g_wtm_drv.out_buf_virt,
                        p->sym_ki_lsb.usrk_wof_sz);
        wtm_copy_to_user((uint32_t)p->sym_ki_msb.usrk_wof,
                        g_wtm_drv.out_buf_virt + delta1,
                        p->sym_ki_msb.usrk_wof_sz);

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)
        _wtm_pr_wof("SU0", __FUNCTION__, _OUTPUT,
                p->sym_ki_lsb.usrk_wof, p->sym_ki_lsb.usrk_wof_sz);
        _wtm_pr_wof("SU1", __FUNCTION__, _OUTPUT,
                p->sym_ki_msb.usrk_wof, p->sym_ki_msb.usrk_wof_sz);
#endif
    }

    _UNREF_FIPS_KI(&sym_ki_msb);

_out2:

    _UNREF_FIPS_KI(&sym_ki_lsb);

_out1:

    _UNREF_FIPS_KI(&ec256_ki);

_out0:

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_check_usrkey(void *arg)
{
    mv_wtm_ioctl_check_key_param *p;
    wtm_vm p_vm = NULL;

    uint32_t delta = WTM_PAGE_SIZE >> 1;

    uint8_t kr_kad[WTM_KAD_SZ], uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info fips_ki_func;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_check_key_param, arg,
            sizeof(mv_wtm_ioctl_check_key_param),
            p, p_vm);

    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.subk_wof_sz);
    if (WTM_IS_KSCH_SYM(p->ksch)) {
        _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);
    } else {
        _ASSERT(WTM_ASYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);
    }

    fips_ki_func.kr_kad_virt = (uint32_t)kr_kad;
    fips_ki_func.uk_kad_virt = (uint32_t)uk_kad;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->fips_ki, &fips_ki_func,
            g_wtm_drv.key_buf_virt, g_wtm_drv.key_buf_phys,
            g_wtm_drv.key_buf_virt + delta, g_wtm_drv.key_buf_phys + delta,
            _out);

    fips_ki_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    fips_ki_func.usrk_wof_sz = (WTM_IS_KSCH_SYM(p->ksch) ?
            WTM_SYM_WOF_SZ : WTM_ASYM_WOF_SZ);

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("S", __FUNCTION__, _INPUT,
            p->fips_ki.subk_wof, p->fips_ki.subk_wof_sz);
    _wtm_pr_wof("U", __FUNCTION__, _INPUT,
            p->fips_ki.usrk_wof, p->fips_ki.usrk_wof_sz);

#endif

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_check_usrkey(
            ss, p->ksch,
            fips_ki_func.kr_kad_virt, fips_ki_func.uk_kad_virt,
            g_wtm_drv.key_buf_phys, fips_ki_func.subk_wof_sz,
            g_wtm_drv.key_buf_phys + delta, fips_ki_func.usrk_wof_sz,
            &p->is_valid);

    mv_wtm_func_destroy_ss(ss);

    if (p->is_valid) {
        p->wtm_fault = MV_WTM_SUCCESS;
    } else {
        p->wtm_fault = MV_WTM_ERR_AUTH;
    }

    _UNREF_FIPS_KI(&fips_ki_func);

_out:

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_bind_pr_key(void *arg)
{
    mv_wtm_ioctl_bind_PlayReady_key_param *p;
    wtm_vm p_vm = NULL;
    uint8_t kr_kad[WTM_KAD_SZ], uk_kad[WTM_KAD_SZ];
    mv_wtm_key_info fips_ki_func;

    uint32_t delta = WTM_PAGE_SIZE >> 2;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_bind_PlayReady_key_param, arg,
            sizeof(mv_wtm_ioctl_bind_PlayReady_key_param),
            p, p_vm);

    _ASSERT(WTM_SYM_WOF_SZ <= p->fips_ki.subk_wof_sz);
    _ASSERT(WTM_ASYM_WOF_SZ <= p->fips_ki.usrk_wof_sz);

    fips_ki_func.kr_kad_virt = (uint32_t)kr_kad;
    fips_ki_func.uk_kad_virt = (uint32_t)uk_kad;

    ret = MV_WTM_ERR_OUT_OF_MEM;
    _REF_FIPS_KI(&p->fips_ki, &fips_ki_func,
            g_wtm_drv.key_buf_virt, g_wtm_drv.key_buf_phys,
            0, 0, _out);

    fips_ki_func.subk_wof_sz = WTM_SYM_WOF_SZ;
    fips_ki_func.usrk_wof_sz = WTM_ASYM_WOF_SZ;

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

    _wtm_pr_wof("S", __FUNCTION__, _INPUT,
            p->fips_ki.subk_wof, p->fips_ki.subk_wof_sz);

#endif

    /* max size of "usrk_passwd" is (WTM_PAGE_SIZE >> 2) */
    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt + delta,
            (uint32_t)p->usrk, p->usrk_sz);
    if(p->aes_iv_sz != 0)
        wtm_copy_from_user(
            g_wtm_drv.key_buf_virt + (delta << 1),
            (uint32_t)p->aes_iv, p->aes_iv_sz);
    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

        ret = mv_wtm_func_bind_pr_key(
            ss, p->pr_ksch, p->key_id,
            fips_ki_func.kr_kad_virt, fips_ki_func.uk_kad_virt,
            g_wtm_drv.key_buf_phys, fips_ki_func.subk_wof_sz,
            g_wtm_drv.key_buf_phys + delta, p->usrk_sz,
            (p->aes_iv_sz != 0) ? (g_wtm_drv.key_buf_phys + (delta << 1)) : 0,
            p->aes_iv_sz, p->trans_ksch, p->byte_swap,
            g_wtm_drv.key_buf_phys + (delta << 1) + delta,
            fips_ki_func.usrk_wof_sz);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    if (!ret) {
        wtm_copy_to_user((uint32_t)p->fips_ki.usrk_wof,
                g_wtm_drv.key_buf_virt + (delta << 1) + delta,
                fips_ki_func.usrk_wof_sz);

#if defined(CONFIG_EN_DBG) && defined(WTM_DBG_DUMP_WOF)

        _wtm_pr_wof("U", __FUNCTION__, _OUTPUT,
                p->fips_ki.usrk_wof, p->fips_ki.usrk_wof_sz);

#endif
    }

    _UNREF_FIPS_KI(&fips_ki_func);

_out:

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
 
}
#endif /* CONFIG_EN_FIPS */
