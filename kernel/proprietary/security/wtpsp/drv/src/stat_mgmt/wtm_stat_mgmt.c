/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_prov.c
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of WTM stat mgmt functions
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_ioctl.h"
#include "mv_wtm_func.h"

#include "wtm_main.h"
#include "wtm_os_util.h"

#include "_wtm_stat_mgmt.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t wtm_get_tsr(void *arg)
{
    mv_wtm_ioctl_get_tsr_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_get_tsr_param, arg,
            sizeof(mv_wtm_ioctl_get_tsr_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_get_tsr(ss, &p->tsr);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_life_cycle_adv(void *arg)
{
    mv_wtm_ioctl_life_cycle_adv_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_life_cycle_adv_param, arg,
            sizeof(mv_wtm_ioctl_life_cycle_adv_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_life_cycle_adv(ss);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_life_cycle_read(void *arg)
{
    mv_wtm_ioctl_life_cycle_read_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_life_cycle_read_param, arg,
            sizeof(mv_wtm_ioctl_life_cycle_read_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_life_cycle_read(ss, &p->life_cycle);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_soft_ver_adv(void *arg)
{
    mv_wtm_ioctl_soft_ver_adv_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_soft_ver_adv_param, arg,
            sizeof(mv_wtm_ioctl_soft_ver_adv_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_soft_ver_adv(ss);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_soft_ver_read(void *arg)
{
    mv_wtm_ioctl_soft_ver_read_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_soft_ver_read_param, arg,
            sizeof(mv_wtm_ioctl_soft_ver_read_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_soft_ver_read(ss, &p->ver);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_kernel_ver_read(void *arg)
{
    mv_wtm_ioctl_kernel_ver_read_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_kernel_ver_read_param, arg,
            sizeof(mv_wtm_ioctl_kernel_ver_read_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_kernel_ver_read(ss, &p->ver);

    mv_wtm_func_destroy_ss(ss);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

