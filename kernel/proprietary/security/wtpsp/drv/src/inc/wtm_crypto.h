/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_crypto.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of WTM crypto functions
 *
 */

#ifndef _WTM_CRYPTO_H_
#define _WTM_CRYPTO_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "wtm_types.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

extern int32_t wtm_hash_init_nonfips(void *arg);
#ifdef CONFIG_EN_FIPS
extern int32_t wtm_hash_init_fips(void *arg);
#endif
extern int32_t wtm_hash_update(void *arg);
extern int32_t wtm_hash_final(void *arg);
extern int32_t wtm_hash_dgst_nonfips(void *arg);
#ifdef CONFIG_EN_FIPS
extern int32_t wtm_hash_dgst_fips(void *arg);
#endif

extern int32_t wtm_sym_init_nonfips(void *arg);
#ifdef CONFIG_EN_FIPS
extern int32_t wtm_sym_init_fips(void *arg);
#endif
extern int32_t wtm_sym_proc(void *arg);
extern int32_t wtm_sym_final(void *arg);

extern int32_t wtm_pkcs_init_nonfips(void *arg);
#ifdef CONFIG_EN_FIPS
extern int32_t wtm_pkcs_init_fips(void *arg);
#endif
extern int32_t wtm_pkcs_update(void *arg);
extern int32_t wtm_pkcs_final(void *arg);
extern int32_t wtm_pkcs_op_nonfips(void *arg);
#ifdef CONFIG_EN_FIPS
extern int32_t wtm_pkcs_op_fips(void *arg);
#endif

extern int32_t wtm_ec_dsa_init_nonfips(void *arg);
#ifdef CONFIG_EN_FIPS
extern int32_t wtm_ec_dsa_init_fips(void *arg);
#endif
extern int32_t wtm_ec_dsa_update(void *arg);
extern int32_t wtm_ec_dsa_final(void *arg);
extern int32_t wtm_ec_dsa_op_nonfips(void *arg);
#ifdef CONFIG_EN_FIPS
extern int32_t wtm_ec_dsa_op_fips(void *arg);
#endif

extern int32_t wtm_create_ec_key_pair(void *arg);
extern int32_t wtm_derivate_ec_pub_key(void *arg);
extern int32_t wtm_create_dh_sys_args(void *arg);
extern int32_t wtm_create_dh_key_pair(void *arg);
extern int32_t wtm_derivate_dh_pub_key(void *arg);
extern int32_t wtm_create_rsa_key_pair(void *arg);
extern int32_t wtm_create_ec_dh_shared_key(void *arg);
extern int32_t wtm_create_dh_shared_key(void *arg);
#ifdef CONFIG_EN_FIPS
extern int32_t wtm_create_subkey(void *arg);
extern int32_t wtm_enroll_sym_usrkey(void *arg);
extern int32_t wtm_enroll_ec_usrkey(void *arg);
extern int32_t wtm_enroll_dh_usrkey(void *arg);
extern int32_t wtm_enroll_rsa_usrkey(void *arg);
extern int32_t wtm_create_sym_usrkey(void *arg);
extern int32_t wtm_pr_ec256_dec(void *arg);
extern int32_t wtm_check_usrkey(void *arg);
extern int32_t wtm_bind_pr_key(void *arg);
#endif

extern int32_t wtm_hdcp_load_key(void *arg);

extern int32_t wtm_drbg_init(void *arg);
extern int32_t wtm_drbg_reseed(void *arg);
extern int32_t wtm_drbg_gen_ran(void *arg);

#endif /* _WTM_CRYPTO_H_ */
