/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_main.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of WTM driver framework
 *
 */

#ifndef _WTM_MAIN_H_
#define _WTM_MAIN_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_types.h"
#include "mv_wtm_ioctl.h"
#include "wtm_os_util.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#define DRV_LAYER_VERSION (0x03010B)
#define MAJOR_VERSION(x) ((x & 0xFF0000) >> 16)
#define MINOR_VERSION(x) ((x & 0xFF00) >> 8)
#define LAST_VERSION(x) (x & 0xFF)

/*
 ******************************
 *          TYPES
 ******************************
 */

typedef struct _wtm_drv
{
    uint32_t key_buf_virt;
    uint32_t key_buf_phys;
    uint32_t in_buf_virt;
    uint32_t in_buf_phys;
    uint32_t out_buf_virt;
    uint32_t out_buf_phys;

    /* for win* compiler, there may be a bug of macros */
    __ALIGNED(uint8_t part_blk_virt[MV_WTM_MAX_PART_BLK_SZ],
            128 /* MV_WTM_MAX_PART_BLK_SZ */);

    uint32_t pin_min;
    uint32_t pin_max;
} wtm_drv;

/*
 ******************************
 *          VARIABLES
 ******************************
 */

extern wtm_drv g_wtm_drv;

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#endif /* _WTM_MAIN_H_ */
