/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_otp.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of WTM stat mgmt
 *
 */

#ifndef _WTM_STAT_MGMT_H_
#define _WTM_STAT_MGMT_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "wtm_types.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

extern int32_t wtm_get_tsr(void *arg);
extern int32_t wtm_life_cycle_adv(void *arg);
extern int32_t wtm_life_cycle_read(void *arg);
extern int32_t wtm_soft_ver_adv(void *arg);
extern int32_t wtm_soft_ver_read(void *arg);
extern int32_t wtm_kernel_ver_read(void *arg);

#endif /* _WTM_STAT_MGMT_H_ */

