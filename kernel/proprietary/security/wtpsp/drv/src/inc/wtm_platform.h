/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_platform.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of platform parameters
 *
 */

#ifndef _WTM_PLATFORM_H_
#define _WTM_PLATFORM_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

/*
 ******************************
 *          MACROS
 ******************************
 */

#ifndef _MAX
#define _MAX(_x, _y)  ((_x) > (_y) ? (_x) : (_y))
#endif

/* MMP2 */

#define _L1_CACHE_LINE_SZ   (32)
#define _L2_CACHE_LINE_SZ   (32)
#define _L3_CACHE_LINE_SZ   (0)     /* zero if no such cache */

#define CACHE_LINE_SZ       (_MAX(_MAX(_L1_CACHE_LINE_SZ,   \
                            _L2_CACHE_LINE_SZ), _L3_CACHE_LINE_SZ))
#define CACHE_LINE_MASK     (~(CACHE_LINE_SZ - 1))

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#endif /* _WTM_PLATFORM_H_ */
