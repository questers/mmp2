/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_prov.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of WTM provision
 *
 */

#ifndef _WTM_PROV_H_
#define _WTM_PROV_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "wtm_types.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

extern int32_t wtm_prov_rkek_prov(void *arg);
extern int32_t wtm_prov_ec521_dk_prov(void *arg);
extern int32_t wtm_prov_plat_bind(void *arg);
extern int32_t wtm_prov_plat_verf(void *arg);
extern int32_t wtm_prov_bind_jtag_key(void *arg);
extern int32_t wtm_prov_verf_jtag_key(void *arg);
extern int32_t wtm_prov_dis_jtag(void *arg);
extern int32_t wtm_prov_tmp_dis_fa(void *arg);
extern int32_t wtm_prov_rd_uniq_id(void *arg);
extern int32_t wtm_prov_wr_plat_conf(void *arg);
extern int32_t wtm_prov_rd_plat_conf(void *arg);
extern int32_t wtm_prov_wr_usb_id(void *arg);
extern int32_t wtm_prov_rd_usb_id(void *arg);
extern int32_t wtm_prov_hdcp_wrap_key(void *arg);
extern int32_t wtm_prov_set_fips_perm(void *arg);
extern int32_t wtm_prov_set_nonfips_perm(void *arg);
extern int32_t wtm_prov_pre_proc(void *arg);
extern int32_t wtm_prov_post_proc(void *arg);
extern int32_t wtm_prov_oem_trans_key(void *arg);

#endif /* _WTM_PROV_H_ */
