/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_crypto_util.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of WTM crypto utility
 *
 */

#ifndef _WTM_CRYPTO_UTIL_H_
#define _WTM_CRYPTO_UTIL_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "wtm_types.h"

#include "wtm_os_util.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#define _IS_ALIGNED(_addr, _algn)   (!((_addr) & ((_algn) - 1)))
#define _ROUND_UP(_x, _a)           (((uint32_t)(_x) + (_a) - 1) & ~((_a) - 1))

/*
 ******************************
 *          TYPES
 ******************************
 */

typedef enum _msg_stat
{
    MSG_PHYS_DIRECT = 0,
    MSG_DMA_LNK_LST = 1,
} msg_stat;

typedef void * msg_handle;

typedef struct _dma_node_rec
{
    dma_node                *node_virt;
    bool                    is_rec_static;
    uint32_t                mem_to_free;
    uint32_t                mem_to_free_sz;
    struct _dma_node_rec    *next;
} dma_node_rec;

typedef struct _msg_struct
{
    /* output */
    msg_stat src_stat;
    msg_handle src_handle;
    msg_stat dst_stat;
    msg_handle dst_handle;

    /* local */
    dma_node_rec *src_dma_rec;
    dma_node_rec *dst_dma_rec;
    /* two "dma_node" required, one more for alignment */
    /* sizeof(dma_node) > WTM_LINK_LIST_ALGN */
    uint8_t src_node_buf[(sizeof(dma_node) << 1) + sizeof(dma_node)];
    dma_node *static_src_node;
    dma_node_rec static_src_rec[2];
    /* two "dma_node" required, one more for alignment */
    /* sizeof(dma_node) > WTM_LINK_LIST_ALGN */
    uint8_t dst_node_buf[(sizeof(dma_node) << 1) + sizeof(dma_node)];
    dma_node *static_dst_node;
    dma_node_rec static_dst_rec[2];

    /* for cache line aignment */
    bool is_dst_cache_ok;
    /* valid if is_dst_cache_ok == false */
    uint32_t dst_msg_dup;   /* valid if dst_msg_sz <= (CACHE_LINE_SZ << 1) */
    uint32_t unalgn_len[2]; /* valid if dst_msg_sz > (CACHE_LINE_SZ << 1) */

    /* for post handling */
    uint32_t dst_msg;
    uint32_t dst_msg_sz;
} msg_struct;

typedef struct _msg_seq
{
    /* input */
    uint32_t part_blk;
    uint32_t part_blk_sz;
    uint32_t src_msg;
    uint32_t src_msg_sz;
    uint32_t dst_msg;
    uint32_t dst_msg_sz;

    /* input/output */
    uint32_t src_page;
    uint32_t dst_page;

    /* output */
    uint32_t cur_sz;

    /* local */
    uint32_t src_cur;
    uint32_t dst_cur;
    uint32_t left;
} msg_seq;

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

__INLINE static bool wtm_is_mem_direct_ok(uint32_t mem, uint32_t mem_sz)
{
    if (_IS_ALIGNED(mem, WTM_PHYS_ADDR_ALGN)) {
        if (wtm_is_mem_contig(mem, mem_sz)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

extern int32_t wtm_msg_struct_init(
        uint32_t part_blk, uint32_t part_blk_sz,
        uint32_t src_msg, uint32_t src_msg_sz,
        uint32_t dst_msg, uint32_t dst_msg_sz,
        bool is_part_first, msg_struct *ms);
extern void wtm_msg_struct_cleanup(msg_struct *ms);
extern void wtm_msg_struct_proc_output_after_pi(msg_struct *ms);

__INLINE static int32_t wtm_msg_seq_init(
        uint32_t part_blk, uint32_t part_blk_sz,
        uint32_t src_msg, uint32_t src_msg_sz,
        uint32_t dst_msg, uint32_t dst_msg_sz,
        uint32_t src_page, uint32_t dst_page,
        msg_seq *mseq)
{
    _ASSERT(part_blk || src_msg);
    _ASSERT(part_blk_sz <= WTM_PAGE_SIZE);
    if (dst_msg) {
        _ASSERT(part_blk_sz + src_msg_sz <= dst_msg_sz);
    }

    mseq->part_blk = part_blk;
    mseq->part_blk_sz = part_blk_sz;
    mseq->src_msg = src_msg;
    mseq->src_msg_sz = src_msg_sz;
    mseq->dst_msg = dst_msg;
    mseq->dst_msg_sz = dst_msg_sz;
    mseq->src_page = src_page;
    mseq->dst_page = dst_page;

    mseq->src_cur = (mseq->part_blk_sz ? mseq->part_blk : mseq->src_msg);
    mseq->dst_cur = mseq->dst_msg;
    mseq->left = mseq->part_blk_sz + mseq->src_msg_sz;

    return 0;
}

__INLINE static void wtm_msg_seq_cleanup(msg_seq *mseq)
{
    /* nothing */;
}

__INLINE static void wtm_msg_seq_pre_proc(msg_seq *mseq)
{
    _ASSERT(mseq && mseq->left);

    if (mseq->src_cur != mseq->part_blk) {
        mseq->cur_sz = ((WTM_PAGE_SIZE <= mseq->left) ?
                WTM_PAGE_SIZE : mseq->left);
        wtm_memcpy(mseq->src_page, mseq->src_cur, mseq->cur_sz);
        mseq->src_cur = mseq->src_cur + mseq->cur_sz;
        mseq->left -= mseq->cur_sz;
    } else {
        wtm_memcpy(mseq->src_page, mseq->src_cur, mseq->part_blk_sz);
        mseq->cur_sz = WTM_PAGE_SIZE - mseq->part_blk_sz;
        mseq->cur_sz = ((mseq->cur_sz <= mseq->src_msg_sz) ?
                mseq->cur_sz : mseq->src_msg_sz);
        wtm_memcpy(mseq->src_page + mseq->part_blk_sz,
                mseq->src_msg, mseq->cur_sz);
        mseq->src_cur = mseq->src_msg + mseq->cur_sz;
        mseq->cur_sz += mseq->part_blk_sz;
        mseq->left -= mseq->cur_sz;
    }
}

__INLINE static void wtm_msg_seq_post_proc(msg_seq *mseq)
{
    _ASSERT(mseq);

    if (mseq->dst_msg_sz) {
        wtm_memcpy(mseq->dst_cur, mseq->dst_page, mseq->cur_sz);
        mseq->dst_cur += mseq->cur_sz;
    }
}

__INLINE static bool wtm_msg_seq_is_done(msg_seq *mseq)
{
    _ASSERT(mseq);

    return !mseq->left;
}

__INLINE static bool wtm_msg_seq_is_last_blk_left(msg_seq *mseq)
{
    _ASSERT(mseq);

    return (mseq->left <= WTM_PAGE_SIZE);
}

#endif /* _WTM_CRYPTO_UTIL_H_ */
