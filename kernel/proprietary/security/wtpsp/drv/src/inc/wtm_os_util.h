/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_os_util.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The header file of WTM utility definition for WTM driver
 *                This file is os-specific
 *
 */

#ifndef _WTM_OS_UTIL_H_
#define _WTM_OS_UTIL_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

/* os headers */

#include <stdarg.h>

#include <linux/dma-mapping.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/slab.h>

#include <asm/cacheflush.h>
#include <asm/io.h>
#include <asm/uaccess.h>

/* global header(s) */

/* local header(s) */

#include "wtm_gcc.h"
#include "wtm_types.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

#define WTM_PAGE_SIZE           (0x1000)

#ifdef CONFIG_EN_DBG

#define _ASSERT(x)   if (!(x)) {            \
    wtm_pr("ASSERT FAILURE:\r\n");          \
    wtm_pr("%s (%d): %s\r\n",               \
        __FILE__, __LINE__, __FUNCTION__);  \
    dump_stack();                           \
    while (1);                              \
}

#define _PR(fmt, arg...)    wtm_pr(fmt, ##arg)

#else /* !CONFIG_EN_DBG */

#define _ASSERT(x)

#define _PR(fmt, arg...)

#endif /* CONFIG_EN_DBG */

#define WTM_INVALID_PHYS_ADDR   (-1UL)

#define _L1_CACHE_LINE_SZ       (32)
#define _L1_CACHE_LINE_MASK     (~(_L1_CACHE_LINE_SZ - 1))
#define _L2_CACHE_LINE_SZ       (32)
#define _L2_CACHE_LINE_MASK     (~(_L2_CACHE_LINE_SZ - 1))

/*
 ******************************
 *          TYPES
 ******************************
 */

typedef void * wtm_vm;

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

/* dbg */
extern int32_t wtm_pr(uint8_t *fmt, ...);

/* os-spec */

__INLINE static long_t wtm_copy_from_user(
        uint32_t to, uint32_t from, long_t n)
{
    return copy_from_user((void *)to, (void __user *)from, n);
}

__INLINE static long_t wtm_copy_to_user(
        uint32_t to, uint32_t from, long_t n)
{
    return copy_to_user((void __user *)to, (void *)from, n);
}

__INLINE static uint32_t wtm_memcpy(uint32_t dst, uint32_t src, uint32_t n)
{
    return (uint32_t)memcpy((void *)dst, (void *)src, n);
}

__INLINE static uint32_t wtm_memset(uint32_t s, uint32_t c, uint32_t n)
{
    return (uint32_t)memset((void *)s, c, n);
}

extern wtm_vm wtm_get_user_mem(void __user *virt, uint32_t size,
        void __kernel **kvirt);
extern void wtm_put_user_mem(wtm_vm vm);

extern bool wtm_is_mem_contig(uint32_t msg, uint32_t sz);

__INLINE static uint32_t wtm_alloc_mem(uint32_t size)
{
    return (uint32_t)kmalloc(size, GFP_KERNEL);
}

__INLINE static void wtm_free_mem(uint32_t virt)
{
    kfree((void *)virt);
}

extern uint32_t wtm_alloc_dma_mem(
                        uint32_t size, uint32_t algn, uint32_t *phys);
extern void wtm_free_dma_mem(uint32_t virt);

extern uint32_t wtm_virt_to_phys(uint32_t virt);

__INLINE static void wtm_flush_dcache(uint32_t virt, uint32_t sz)
{
    dmac_flush_range((void *)virt, (void *)(virt + sz));
}

__INLINE static void wtm_flush_l2dcache(uint32_t virt, uint32_t sz)
{
    ulong_t flags;
    vuint32_t phys = wtm_virt_to_phys(virt);
    local_irq_save(flags);
    outer_flush_range(phys, phys + sz);
    local_irq_restore(flags);
}

#if 0

__INLINE static void wtm_invalidate_dcache(uint32_t virt, uint32_t sz)
{
    uint32_t start, end;
    uint32_t len0, len1;
    uint8_t buf[2][32];
    uint8_t *ptr0, *ptr1;
    uint32_t i;
    ulong_t flags = 0;

    start = virt & _L1_CACHE_LINE_MASK;
    len0 = virt - start;

    end = (virt + sz + _L1_CACHE_LINE_SZ - 1) & _L1_CACHE_LINE_MASK;
    len1 = end - (virt + sz);

    if (len0 || len1) {
        local_irq_save(flags);
    }

    /* save unaligned parts */
    ptr0 = (uint8_t *)start;
    for (i = 0; i < len0; i++) {
        buf[0][i] = ptr0[i];
    }
    ptr1 = (uint8_t *)(virt + sz);
    for (i = 0; i < len1; i++) {
        buf[1][i] = ptr1[i];
    }

    dmac_inv_range((void *)start, (void *)end);

    /* restore unaligned parts into cache */
    for (i = 0; i < len0; i++) {
        ptr0[i] = buf[0][i];
    }
    for (i = 0; i < len1; i++) {
        ptr1[i] = buf[1][i];
    }

    if (len0 || len1) {
        local_irq_restore(flags);
    }
}

__INLINE static void wtm_invalidate_l2dcache(uint32_t virt, uint32_t sz)
{
#if 0
    uint32_t start, end;
    uint32_t len0, len1;
    uint8_t buf[2][32];
    uint8_t *ptr0, *ptr1;
    uint32_t i;
    ulong_t flags = 0;

    start = virt & _L1_CACHE_LINE_MASK;
    len0 = virt - start;

    end = (virt + sz + _L1_CACHE_LINE_SZ - 1) & _L1_CACHE_LINE_MASK;
    len1 = end - (virt + sz);

    if (len0 || len1) {
        local_irq_save(flags);
    }

    /* save unaligned parts */
    ptr0 = (uint8_t *)start;
    for (i = 0; i < len0; i++) {
        buf[0][i] = ptr0[i];
    }
    ptr1 = (uint8_t *)(virt + sz);
    for (i = 0; i < len1; i++) {
        buf[1][i] = ptr1[i];
    }

    outer_flush_range((void *)start, (void *)end);

    /* restore unaligned parts into cache */
    for (i = 0; i < len0; i++) {
        ptr0[i] = buf[0][i];
    }
    for (i = 0; i < len1; i++) {
        ptr1[i] = buf[1][i];
    }

    if (len0 || len1) {
        local_irq_restore(flags);
    }
#endif
}

#endif

#endif /* _WTM_OS_UTIL_H_ */
