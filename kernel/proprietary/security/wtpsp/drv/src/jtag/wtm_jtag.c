/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : wtm_jtag.c
 * Author       : Dafu Lv
 * Date Created : 06/09/2010
 * Description  : The implementation file of WTM jtag settings.
 *
 */

/*
 ******************************
 *          HEADERS
 ******************************
 */

#include "mv_wtm_ioctl.h"
#include "mv_wtm_func.h"

#include "wtm_main.h"
#include "wtm_os_util.h"

#include "_wtm_jtag.h"

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

int32_t wtm_jtag_get_nonce(void *arg)
{
    mv_wtm_ioctl_jtag_get_nonce_param *p;
    wtm_vm p_vm = NULL;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_jtag_get_nonce_param, arg,
            sizeof(mv_wtm_ioctl_jtag_get_nonce_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    ret = mv_wtm_func_get_jtag_nonce(ss, g_wtm_drv.out_buf_phys);

    mv_wtm_func_destroy_ss(ss);

    wtm_memcpy((uint32_t)p->nonce, g_wtm_drv.out_buf_virt, p->nonce_sz);

    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

int32_t wtm_jtag_conf(void *arg)
{
    mv_wtm_ioctl_jtag_conf_param *p;
    wtm_vm p_vm = NULL;
    uint32_t delta = WTM_PAGE_SIZE >> 1;

    mv_wtm_ss ss;

    int32_t ret;

    _ASSERT(arg);

    _REF_PARAM(
            mv_wtm_ioctl_jtag_conf_param, arg,
            sizeof(mv_wtm_ioctl_jtag_conf_param),
            p, p_vm);

    ss = mv_wtm_func_create_ss();
    _ASSERT(ss);

    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt, (uint32_t)p->pubk0, p->pubk_sz);
    wtm_copy_from_user(
            g_wtm_drv.key_buf_virt + delta, (uint32_t)p->pubk1, p->pubk_sz);
    wtm_copy_from_user(
            g_wtm_drv.in_buf_virt, (uint32_t)p->sig, p->sig_sz);

    ret = mv_wtm_func_set_jtag(ss, p->sch, p->ctrl,
            g_wtm_drv.key_buf_phys, g_wtm_drv.key_buf_phys + delta,
            g_wtm_drv.in_buf_phys);

    mv_wtm_func_destroy_ss(ss);

    p->is_valid = !ret;
    p->wtm_fault = ret;

    _UNREF_PARAM(p_vm);

    return RET_VAL(ret);
}

