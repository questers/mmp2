/*
 * (C) Copyright 2009 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * Filename     : _wtm_jtag.h
 * Author       : Dafu Lv
 * Date Created : 18/01/2010
 * Description  : The implementation file of WTM JTAG functions.
 *
 */

#ifndef __WTM_JTAG_H_
#define __WTM_JTAG_H_

/*
 ******************************
 *          HEADERS
 ******************************
 */

/*
 ******************************
 *          MACROS
 ******************************
 */

/*
 * input:
 * _type - the data type of _arg
 * _arg - the input pointer to param
 * _sz - the size of param
 *
 * output:
 * _p - the output pointer to param
 * _p_vm - the vm handle of param
 */
#define _REF_PARAM(_type, _arg, _sz, _p, _p_vm)             \
    do {                                                    \
        _p_vm = wtm_get_user_mem(arg, _sz, (void **)&_p);   \
        _ASSERT(_p_vm);                                     \
    } while (0)

/*
 * input:
 * _vm - the vm handle of param
 */
#define _UNREF_PARAM(_p_vm)   wtm_put_user_mem(_p_vm)

#define RET_VAL(_ret)   ((_ret) ? -1 : 0)

/*
 ******************************
 *          TYPES
 ******************************
 */

/*
 ******************************
 *          VARIABLES
 ******************************
 */

/*
 ******************************
 *          FUNCTIONS
 ******************************
 */

#endif /* __WTM_JTAG_H_ */
