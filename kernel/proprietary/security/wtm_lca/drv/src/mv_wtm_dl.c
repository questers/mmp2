/*
 * Marvell Wireless Trusted Module (WTM) DMA List Module
 *
 * Copyright (c) 2009 Marvell International Ltd.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/crypto.h>
#include <linux/cryptohash.h>
#include <linux/dma-mapping.h>
#include <linux/dmapool.h>
#include <linux/io.h>
#include <linux/spinlock.h>
#include <linux/kthread.h>
#include <linux/platform_device.h>
#include <linux/scatterlist.h>
#include <asm/cacheflush.h>

#include "mv_wtm.h"

static int mv_wtm_count_sg_num(struct scatterlist *sl, unsigned int total_bytes, 
                               unsigned int * use_dma, unsigned int addr_mask, 
							   unsigned int blk_mask)
{
	int i = 0;
	int nbytes;
	unsigned int flag = 1;

	if ((!sl) || (!total_bytes))
		return 0;	
		
	if (total_bytes <= PAGE_SIZE) {
	    flag = 0;
	}	
		
	nbytes = (int)total_bytes;	

	do {
	    if (flag == 1) {
		    if (((unsigned int)(sl[i].offset) & addr_mask) ||
		        ((unsigned int)(sl[i].length) & blk_mask)) {
			    flag = 0;
		    }
	    }
		
		nbytes -= sl[i].length;
		i++;		

	} while (nbytes > 0);
	
	*use_dma = flag;

	return i;
}

/*static unsigned int mv_wtm_dl_can_use_dma(struct scatterlist *sl, unsigned int nents,
		unsigned int addr_mask, unsigned int blk_mask)
{
	struct sg_mapping_iter miter;

	unsigned int ret = 1;

	// iterate the scatter list 
	sg_miter_start(&miter, sl, nents, SG_MITER_FROM_SG);
	while (sg_miter_next(&miter)) {
		if (((unsigned int)miter.addr & addr_mask) ||
		    ((unsigned int)miter.length & blk_mask)) {
			ret = 0;
			break;
		}
	}
	sg_miter_stop(&miter);

	return ret;
} */

static void mv_wtm_dl_dump_chain(struct mv_wtm_dn_rec *head)
{
	struct mv_wtm_dn_rec *rec_next;
	struct mv_wtm_dn *dn;
	dma_addr_t dma_addr;

	if (!head) {
		mv_wtm_debug("dump dma list chain: it is empty.\n");
		return;
	}

	mv_wtm_debug("dump dma list chain:\n");
	rec_next = head; 	

	/* iterate the nodes with their dma addr */
	while (rec_next) {
		dma_addr = rec_next->dn_phys;
		dn = rec_next->dn_virt;

		rec_next = rec_next->next;

		mv_wtm_debug("{ node = 0x%08x, addr = 0x%08x, len = 0x%08x, next = 0x%08x }\n",
			(u32)dn, dn->addr, dn->word * 4, (u32)(dn->next));
	}

	return;
}

static unsigned int *mv_wtm_dl_alloc_chain(struct scatterlist *sl,
		unsigned int sl_len, unsigned int nents)
{
	struct mv_wtm_dn *head = NULL;
	struct mv_wtm_dn *tail = NULL;	

	struct mv_wtm_dn *new;
	dma_addr_t dma_addr;
	
	struct mv_wtm_dn_rec *rec_head = NULL;
	struct mv_wtm_dn_rec *rec_tail = NULL;

	struct mv_wtm_dn_rec *rec_new;
	
	struct sg_mapping_iter miter;
	unsigned int offset;
	unsigned int len;
	u32 miter_dma_addr;

	if (!sl_len)
		return NULL;

	offset = 0;

	/* iterate the scatter list */
	sg_miter_start(&miter, sl, nents, SG_MITER_FROM_SG);
	while (sg_miter_next(&miter) && offset < sl_len) {

		len = min(miter.length, sl_len - offset);

		offset += len;
		
		new = dma_pool_alloc(mv_wtm_dn_pool, GFP_KERNEL, &dma_addr);

		miter_dma_addr = (u32)page_to_phys(miter.page) + ((u32)(miter.addr)	& (PAGE_SIZE - 1));	
				
		new->addr = miter_dma_addr;
		new->word = ((len + 3) >> 2);
		new->next = NULL;
		
		rec_new = (struct mv_wtm_dn_rec *)kmalloc(sizeof(struct mv_wtm_dn_rec), GFP_KERNEL);
		
		rec_new->dn_virt = new;
		rec_new->dn_phys = dma_addr;
		rec_new->next = NULL;

		if (!head) {
			head = new;
			tail = new;
			
			rec_head = rec_new;
			rec_tail = rec_new;			
		} else {
			tail->next = (struct mv_wtm_dn *)dma_addr;
			tail = new;
			
			rec_tail->next = rec_new;
			rec_tail = rec_new;
		}		
	}
	sg_miter_stop(&miter);	

	mv_wtm_dl_dump_chain(rec_head);

	return (unsigned int *)rec_head;
}

static void mv_wtm_dl_free_chain(unsigned int *head)
{
	struct mv_wtm_dn_rec *rec_next, *rec_prev;
	struct mv_wtm_dn *dn;
	dma_addr_t dma_addr;

	if (!head)
		return;		
	
	rec_next = (struct mv_wtm_dn_rec *)head;
	
	/* iterate the nodes with their dma addr */
	while (rec_next) {
		dma_addr = rec_next->dn_phys;
		dn = rec_next->dn_virt;

		rec_prev = rec_next;
		rec_next = rec_next->next;
		
		dma_pool_free(mv_wtm_dn_pool, dn, dma_addr);		
		
        kfree(rec_prev);
	}

	return;
}

unsigned int mv_wtm_dl_get_src_block(struct mv_wtm_dl *dl, unsigned int **data, unsigned int left_bytes)
{
	unsigned int src_copy;
	unsigned int left;
	void *buf;
	int ret;

	if (dl->src_len != 0)
		memset(dl->src_virt, 0, dl->src_len);

	while ((dl->src_len < PAGE_SIZE) && (dl->src_len < left_bytes)) {

		if (!dl->src_sg_left) {
			ret = sg_miter_next(&(dl->src_miter));
			if (!ret)
				break;

			dl->src_sg_left = dl->src_miter.length;
			dl->src_sg_offset = 0;
		}

		mv_wtm_debug("block mode: miter addr = 0x%x, length = 0x%x\n",
			(u32)dl->src_miter.addr, (u32)dl->src_miter.length);

		left = PAGE_SIZE - dl->src_len;
		src_copy = min(dl->src_sg_left, left);
		src_copy = min(src_copy, left_bytes);

		buf = dl->src_miter.addr;
		buf += dl->src_sg_offset;

		memcpy((void *)((u32)dl->src_virt + dl->src_len), buf, src_copy);

		mv_wtm_debug("block mode: copy data, len = 0x%x, buf = 0x%x, offset = 0x%x, left = 0x%x\n",
			dl->src_len, (u32)buf, dl->src_sg_offset, dl->src_sg_left);
		mv_wtm_dump(KERN_CONT, "mid data: ", DUMP_PREFIX_OFFSET,
			16, 1, buf, src_copy, false);

		dl->src_sg_left   -= src_copy;
		dl->src_sg_offset += src_copy;
		dl->src_len       += src_copy;
	}

	mv_wtm_debug("block mode: vaddr = 0x%08x, paddr = 0x%08x, len = 0x%x\n",
		(u32)dl->src_virt, (u32)dl->src_phys, dl->src_len);
	mv_wtm_dump(KERN_CONT, "final data: ", DUMP_PREFIX_OFFSET,
		16, 1, dl->src_virt, dl->src_len, false);

	*data = (unsigned int *)dl->src_phys;
	if ((dl->src_len == PAGE_SIZE) && (dl->src_len < left_bytes)) {
		dl->src_len = 0;
		return PAGE_SIZE;
	}

	return 0;
}

unsigned int mv_wtm_dl_get_src_tail(struct mv_wtm_dl *dl, unsigned int **data)
{
	unsigned int len;

	*data = (unsigned int *)dl->src_phys;
	len = dl->src_len;
	dl->src_len = 0;

	return len;
}

unsigned int mv_wtm_dl_get_dst_buf(struct mv_wtm_dl *dl, unsigned int **data)
{
	*data = (unsigned int *)dl->dst_phys;
	return dl->dst_len;
}

unsigned int mv_wtm_dl_put_dst_block(struct mv_wtm_dl *dl, unsigned int *data,
				     unsigned int len)
{
	unsigned int dst_copy;
	unsigned int offset = 0;
	char *buf;
	int ret;
	int nbytes;	

	mv_wtm_dump(KERN_CONT, "result data: ", DUMP_PREFIX_OFFSET,
		16, 1, dl->dst_virt, len, false);
		
	nbytes = (int)len;	

	do {
		if (!dl->dst_sg_left) {
			ret = sg_miter_next(&dl->dst_miter);
			if (!ret)
				break;

			dl->dst_sg_left   = dl->dst_miter.length;
			dl->dst_sg_offset = 0;
		}

		buf = dl->dst_miter.addr;
		buf += dl->dst_sg_offset;

		dst_copy = min((unsigned int)nbytes, dl->dst_sg_left);

		//memcpy(buf, (void *)((unsigned int)data + offset), dst_copy);
		memcpy(buf, (void *)((unsigned int)(dl->dst_virt)+ offset), dst_copy);

		dl->dst_sg_left   -= dst_copy;
		dl->dst_sg_offset += dst_copy;

		nbytes -= dst_copy;
		offset += dst_copy;
	} while (nbytes > 0);

	return offset;
}

/*
 * create the dma chain
 */
unsigned int mv_wtm_dl_alloc_src_chain(struct mv_wtm_dl *dl, unsigned int **head)
{
	if ((!dl->use_dma) || (!dl->src_sl)) {
		*head = NULL;
		return 0;
	}

	*head = mv_wtm_dl_alloc_chain(dl->src_sl, dl->src_sl_len, dl->src_sl_nents);

	return dl->src_sl_len;
}

void mv_wtm_dl_free_src_chain(struct mv_wtm_dl *dl, unsigned int *head)
{
	if ((!dl->use_dma) || (!dl->src_sl))
		return;

	mv_wtm_dl_free_chain(head);

	return;
}

unsigned int mv_wtm_dl_alloc_dst_chain(struct mv_wtm_dl *dl, unsigned int **head)
{
	if ((!dl->use_dma) || (!dl->dst_sl)) {
		*head = NULL;
		return 0;
	}

	*head = mv_wtm_dl_alloc_chain(dl->dst_sl, dl->dst_sl_len, dl->dst_sl_nents);

	return dl->dst_sl_len;
}

void mv_wtm_dl_free_dst_chain(struct mv_wtm_dl *dl, unsigned int *head)
{
	if ((!dl->use_dma) || (!dl->dst_sl))
		return;

	mv_wtm_dl_free_chain(head);

	return;
}

void mv_wtm_dl_sync_src(struct mv_wtm_dl *dl)
{
	dma_map_sg(mv_wtm_get_dev(), dl->src_sl, dl->src_sl_nents, DMA_TO_DEVICE);
	dma_sync_sg_for_device(mv_wtm_get_dev(), dl->src_sl,
			       dl->src_sl_nents, DMA_TO_DEVICE);
	dma_unmap_sg(mv_wtm_get_dev(), dl->src_sl, dl->src_sl_nents, DMA_TO_DEVICE);

	return;
}

void mv_wtm_dl_sync_dst(struct mv_wtm_dl *dl)
{
	dma_map_sg(mv_wtm_get_dev(), dl->dst_sl, dl->dst_sl_nents, DMA_FROM_DEVICE);
	dma_sync_sg_for_device(mv_wtm_get_dev(), dl->dst_sl,
			       dl->dst_sl_nents, DMA_FROM_DEVICE);
	dma_unmap_sg(mv_wtm_get_dev(), dl->dst_sl, dl->dst_sl_nents, DMA_FROM_DEVICE);

	return;
}

/* 
 * Init the scatterlist, and it will check this scatterlist
 * can use dma or not. block mode = 0, dma mode = 1.
 */
int mv_wtm_dl_setup_sl(struct mv_wtm_dl *dl,
		       struct scatterlist *src, unsigned int src_sl_len,
		       struct scatterlist *dst, unsigned int dst_sl_len)
{	
    unsigned int src_use_dma = 0;
	unsigned int dst_use_dma = 0;
	
	/* init src data */
	dl->src_sl = src;
	dl->src_sl_len = src_sl_len;
	dl->src_sl_nents = mv_wtm_count_sg_num(src, src_sl_len, &src_use_dma, 
	                                       dl->addr_mask, dl->blk_mask);
	dl->src_sg_left = 0;
	dl->src_sg_offset = 0;	

	/* init dst data */
	dl->dst_sl = dst;
	dl->dst_sl_len = dst_sl_len;
	dl->dst_sl_nents = mv_wtm_count_sg_num(dst, dst_sl_len, &dst_use_dma,
	                                       dl->addr_mask, dl->blk_mask);
	dl->dst_sg_left = 0;
	dl->dst_sg_offset = 0;
	
	if (src_use_dma && dst_use_dma) {
	    /*
	         * sometimes the previous opertion (such as update) have left data,
	         * which have not been handled; so at this moment in the 
	         * next opertion (such as final), will setup another scatterlist,
	         * so we need handle this situation
	         */
	    if (dl->src_len) {
	        sg_miter_start(&(dl->src_miter), src, dl->src_sl_nents, SG_MITER_FROM_SG);
	        sg_miter_start(&(dl->dst_miter), dst, dl->dst_sl_nents, SG_MITER_TO_SG);
		    dl->use_dma = 0;
		} else {
		    sg_miter_start(&(dl->src_miter), src, dl->src_sl_nents, SG_MITER_FROM_SG);
	        sg_miter_start(&(dl->dst_miter), dst, dl->dst_sl_nents, SG_MITER_FROM_SG);
		    dl->use_dma = 1;
		}
	} else {	
	    sg_miter_start(&(dl->src_miter), src, dl->src_sl_nents, SG_MITER_FROM_SG);
	    sg_miter_start(&(dl->dst_miter), dst, dl->dst_sl_nents, SG_MITER_TO_SG);
		dl->use_dma = 0;
	}			
	
	return dl->use_dma;
}

void mv_wtm_dl_cleanup_sl(struct mv_wtm_dl *dl)
{
	dl->src_sl = NULL;
	dl->src_sl_len = 0;
	dl->src_sl_nents = 0;
	dl->src_sg_left = 0;
	dl->src_sg_offset = 0;

	dl->dst_sl = NULL;
	dl->dst_sl_len = 0;
	dl->dst_sl_nents = 0; 
	dl->dst_sg_left = 0;
	dl->dst_sg_offset = 0;

	sg_miter_stop(&(dl->src_miter));
	sg_miter_stop(&(dl->dst_miter));

	return;
}

void mv_wtm_dl_init(struct mv_wtm_dl *dl, unsigned int blk_align, unsigned int addr_align)
{
	dl->src_virt = dma_alloc_coherent(mv_wtm_get_dev(), PAGE_SIZE,
				(dma_addr_t *)&(dl->src_phys), GFP_KERNEL);
	dl->src_len = 0;

	dl->dst_virt = dma_alloc_coherent(mv_wtm_get_dev(), PAGE_SIZE,
				(dma_addr_t *)&(dl->dst_phys), GFP_KERNEL);
	dl->dst_len = 0;

	dl->addr_align = addr_align;
	dl->addr_mask  = addr_align - 1;
	dl->blk_align  = blk_align;
	dl->blk_mask   = blk_align - 1;

	return;
}

void mv_wtm_dl_exit(struct mv_wtm_dl *dl)
{
	dma_free_coherent(mv_wtm_get_dev(), PAGE_SIZE,
			  dl->src_virt, (dma_addr_t)dl->src_phys);
	dma_free_coherent(mv_wtm_get_dev(), PAGE_SIZE,
			  dl->dst_virt, (dma_addr_t)dl->dst_phys);

	return;
}

