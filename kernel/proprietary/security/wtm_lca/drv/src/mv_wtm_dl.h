/*
 * Marvell Wireless Trusted Module (WTM) DMA List Header
 *
 * Copyright (c) 2009 Marvell International Ltd.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _MV_WTM_DL_H_

/*
 * dma node struct
 */
struct mv_wtm_dn {
	u32 addr;
	u32 word;
	struct mv_wtm_dn *next;
	u32 reserved;
};

/*
 * dma node record struct
 */
struct mv_wtm_dn_rec {
	struct mv_wtm_dn *dn_virt;
	dma_addr_t dn_phys;	
	struct mv_wtm_dn_rec *next;	
	u32 reserved;
};

/* 
 * dma list struct
 */
struct mv_wtm_dl {
	u32 blk_align;
	u32 blk_mask;

	u32 addr_align;
	u32 addr_mask;

	u32 *src_phys, *dst_phys;
	u32 *src_virt, *dst_virt;
	u32 src_len, dst_len;

	struct scatterlist *src_sl;
	struct scatterlist *dst_sl;
	u32 src_sl_len;
	u32 dst_sl_len;
	u32 src_sl_nents;
	u32 dst_sl_nents;
	u32 src_sg_left;
	u32 dst_sg_left;
	u32 src_sg_offset;
	u32 dst_sg_offset;
	struct sg_mapping_iter src_miter;
	struct sg_mapping_iter dst_miter;

	u32 use_dma;
};

/* init and exit functions */
extern void mv_wtm_dl_init(struct mv_wtm_dl *dl,
	unsigned int blk_align, unsigned int addr_align);
extern void mv_wtm_dl_exit(struct mv_wtm_dl *dl);
extern int mv_wtm_dl_setup_sl(struct mv_wtm_dl *dl,
	struct scatterlist *src, unsigned int src_sl_len,
	struct scatterlist *dst, unsigned int dst_sl_len);
extern void mv_wtm_dl_cleanup_sl(struct mv_wtm_dl *dl);

/* block mode related functions */
extern unsigned int mv_wtm_dl_get_src_block(struct mv_wtm_dl *dl, unsigned int **data, unsigned int left_bytes);
extern unsigned int mv_wtm_dl_get_src_tail(struct mv_wtm_dl *dl, unsigned int **data);
extern unsigned int mv_wtm_dl_get_dst_buf(struct mv_wtm_dl *dl, unsigned int **data);
extern unsigned int mv_wtm_dl_put_dst_block(struct mv_wtm_dl *dl,
		unsigned int *data, unsigned int len);

/* dma mode related functions */
extern unsigned int mv_wtm_dl_alloc_src_chain(struct mv_wtm_dl *dl, unsigned int **head);
extern void mv_wtm_dl_free_src_chain(struct mv_wtm_dl *dl, unsigned int *head);
extern unsigned int mv_wtm_dl_alloc_dst_chain(struct mv_wtm_dl *dl, unsigned int **head);
extern void mv_wtm_dl_free_dst_chain(struct mv_wtm_dl *dl, unsigned int *head);
extern void mv_wtm_dl_sync_src(struct mv_wtm_dl *dl);
extern void mv_wtm_dl_sync_dst(struct mv_wtm_dl *dl);

#endif /* _MV_WTM_DL_H_ */

