/*
 * Marvell Wireless Trusted Module (WTM) HMAC Algorithms
 *
 * Copyright (c) 2009 Marvell International Ltd.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/crypto.h>
#include <linux/cryptohash.h>
#include <linux/dma-mapping.h>
#include <linux/dmapool.h>
#include <linux/io.h>
#include <linux/spinlock.h>
#include <linux/kthread.h>
#include <linux/platform_device.h>
#include <linux/scatterlist.h>

#include <crypto/algapi.h>
#include <crypto/crypto_wq.h>
#include <crypto/sha.h>
#include <crypto/internal/hash.h>

#include "mv_wtm.h"

/* functions for the wtm hash hardware engine */
int mv_wtm_engine_hmac_init(struct ahash_request *req, int err)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);
	struct mv_wtm_alg_request_ctx *rctx;

   	rctx = &(ctx->rctx);
	
	if (unlikely(err == -EINPROGRESS))
		goto out;
		
	req->base.flags |= CRYPTO_TFM_REQ_MAY_SLEEP;	

	mv_wtm_func_hash_init_nonfips(ctx->ss, ctx->scheme,
				      (uint32_t)ctx->key_phys,
				      ctx->key_len);
					  
	req->base.complete = rctx->complete;
	
out:
    local_bh_disable();
    rctx->complete(&req->base, err);
    local_bh_enable();							  

    return 0;
}

int mv_wtm_engine_hmac_update(struct ahash_request *req, int err)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);
	struct mv_wtm_alg_request_ctx *rctx;
	struct mv_wtm_dl *dl;
	unsigned int use_dma;
	unsigned int *head;
	unsigned int *data, len;
	unsigned int left_bytes;
	
	rctx = &(ctx->rctx);	
	
	if (unlikely(err == -EINPROGRESS))
		goto out;		

	dl = &(ctx->dl);

	left_bytes = req->nbytes;
	use_dma = mv_wtm_dl_setup_sl(dl, req->src, req->nbytes, NULL, 0);
	if (use_dma) {
		mv_wtm_dl_sync_src(dl);
		len = mv_wtm_dl_alloc_src_chain(dl, &head);
		mv_wtm_func_hash_update(ctx->ss, (u32)NULL, ((struct mv_wtm_dn_rec *)head)->dn_phys, len);
		mv_wtm_dl_free_src_chain(dl, head);
	} else {
		len = mv_wtm_dl_get_src_block(dl, &data, left_bytes);
		while(len) {
			mv_wtm_func_hash_update(ctx->ss, (u32)data, (u32)NULL, len);
			left_bytes -= len;
			len = mv_wtm_dl_get_src_block(dl, &data, left_bytes);
		}
	}
	mv_wtm_dl_cleanup_sl(dl);
	
    req->base.complete = rctx->complete;
        
out:
    local_bh_disable();
    rctx->complete(&req->base, err);
    local_bh_enable();   		

    return 0;
}

int mv_wtm_engine_hmac_final(struct ahash_request *req, int err)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);
	struct mv_wtm_alg_request_ctx *rctx;
	struct mv_wtm_dl *dl;
	unsigned int *tail;
	unsigned int len;
	unsigned int i;
	
	rctx = &(ctx->rctx);
	
	if (unlikely(err == -EINPROGRESS))
		goto out;	

	dl = &(ctx->dl);

	/* get the tail data */
	len = mv_wtm_dl_get_src_tail(dl, &tail);
	mv_wtm_func_hash_final(ctx->ss, (u32)tail, (u32)NULL, len, (u32)ctx->out_phys);

	for (i = 0; i < ctx->out_size; i++)
		req->result[i] = ctx->out_virt[i];

	mv_wtm_dump(KERN_CONT, "hmac result:", DUMP_PREFIX_OFFSET,
		16, 1, ctx->out_virt, ctx->out_size, false);
		
    req->base.complete = rctx->complete;
        
out:
    local_bh_disable();
    rctx->complete(&req->base, err);
    local_bh_enable();		

	return 0;
}

int mv_wtm_engine_hmac_digest(struct ahash_request *req, int err)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);
	struct mv_wtm_alg_request_ctx *rctx;
	struct mv_wtm_dl *dl;
	unsigned int use_dma;
	unsigned int *head;
	unsigned int *tail;
	unsigned int *data, len;
	unsigned int left_bytes;
	unsigned int i;
	
   	rctx = &(ctx->rctx);
	
	if (unlikely(err == -EINPROGRESS))
		goto out;
		
	req->base.flags |= CRYPTO_TFM_REQ_MAY_SLEEP;	

	mv_wtm_func_hash_init_nonfips(ctx->ss, ctx->scheme,
				      (uint32_t)ctx->key_phys,
				      ctx->key_len);

	dl = &(ctx->dl);
	
	left_bytes = req->nbytes;
	use_dma = mv_wtm_dl_setup_sl(dl, req->src, req->nbytes, NULL, 0);
	if (use_dma) {
		mv_wtm_dl_sync_src(dl);
		len = mv_wtm_dl_alloc_src_chain(dl, &head);
		mv_wtm_func_hash_update(ctx->ss, (u32)NULL, ((struct mv_wtm_dn_rec *)head)->dn_phys, len);
		mv_wtm_dl_free_src_chain(dl, head);
	} else {
		len = mv_wtm_dl_get_src_block(dl, &data, left_bytes);
		while(len) {
			mv_wtm_func_hash_update(ctx->ss, (u32)data, (u32)NULL, len);
			left_bytes -= len;
			len = mv_wtm_dl_get_src_block(dl, &data, left_bytes);
		}
	}
	mv_wtm_dl_cleanup_sl(dl);

	/* get the tail data */
	len = mv_wtm_dl_get_src_tail(dl, &tail);
	mv_wtm_func_hash_final(ctx->ss, (u32)tail, (u32)NULL, len, (u32)ctx->out_phys);

	for (i = 0; i < ctx->out_size; i++)
		req->result[i] = ctx->out_virt[i];

	mv_wtm_dump(KERN_CONT, "hmac result:", DUMP_PREFIX_OFFSET,
		16, 1, ctx->out_virt, ctx->out_size, false);
		
    req->base.complete = rctx->complete;
        
out:
    local_bh_disable();
    rctx->complete(&req->base, err);
    local_bh_enable();		

	return 0;
}

static void mv_wtm_hmac_init(struct crypto_async_request *req, int err)
{
    mv_wtm_engine_hmac_init(ahash_request_cast(req), err);
}

static void mv_wtm_hmac_update(struct crypto_async_request *req, int err)
{
    mv_wtm_engine_hmac_update(ahash_request_cast(req), err);
}

static void mv_wtm_hmac_final(struct crypto_async_request *req, int err)
{
    mv_wtm_engine_hmac_final(ahash_request_cast(req), err);
}

static void mv_wtm_hmac_digest(struct crypto_async_request *req, int err)
{
    mv_wtm_engine_hmac_digest(ahash_request_cast(req), err);
}

static int mv_wtm_hmac_enqueue(struct ahash_request *req,
				crypto_completion_t complete)
{
    struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);
	struct mv_wtm_alg_request_ctx *rctx;	

	rctx = &(ctx->rctx);
	
	rctx->complete = req->base.complete;
	req->base.complete = complete;

	return mv_wtm_enqueue_request(&req->base);
}

/* functions for the algorithms */
static int mv_wtm_alg_hmac_init(struct ahash_request *req)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);

	ctx->op = MV_WTM_OP_HMAC_INIT;	

    return mv_wtm_hmac_enqueue(req, mv_wtm_hmac_init);
}

static int mv_wtm_alg_hmac_update(struct ahash_request *req)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);

	ctx->op = MV_WTM_OP_HMAC_UPDATE;

    return mv_wtm_hmac_enqueue(req, mv_wtm_hmac_update);
}

static int mv_wtm_alg_hmac_final(struct ahash_request *req)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);

	ctx->op = MV_WTM_OP_HMAC_FINAL;

    return mv_wtm_hmac_enqueue(req, mv_wtm_hmac_final);
}

static int mv_wtm_alg_hmac_digest(struct ahash_request *req)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);

	ctx->op = MV_WTM_OP_HMAC_DIGEST;

	return mv_wtm_hmac_enqueue(req, mv_wtm_hmac_digest);
}

static int mv_wtm_alg_hmac_setkey(struct crypto_ahash *tfm, const u8 *key,
		unsigned int len)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(&(tfm->base));
	mv_wtm_ss ss;
	int *phys, *virt;
	int size;
	int ret = 0;

	mv_wtm_dump(KERN_CONT, "hmac key:", DUMP_PREFIX_OFFSET,
		16, 1, key, len, false);

	if (len <= 64) {
		memcpy(ctx->key_virt, key, len);
		ctx->key_len = len;
	} else {
		ss = mv_wtm_func_create_ss();

		size = (len + (PAGE_SIZE - 1)) & ~(PAGE_SIZE - 1);
		virt = dma_alloc_coherent(mv_wtm_get_dev(), size,
					  (dma_addr_t *)&phys, GFP_KERNEL);
		memcpy((void *)virt, key, len);
		mv_wtm_func_hash_dgst_nonfips(ss, ctx->scheme - WTM_HMAC_SHA1 + WTM_SHA1,
					      0, 0,
					      (uint32_t)phys, (uint32_t)NULL, len,
					      (uint32_t)ctx->key_phys);

		dma_free_coherent(mv_wtm_get_dev(), size, virt, (dma_addr_t)phys);

		mv_wtm_func_destroy_ss(ss);

		ctx->key_len = ctx->out_size;
	}

	return ret;
}

static int mv_wtm_hmac_sha1_cra_init(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	ctx->scheme = WTM_HMAC_SHA1;
	ctx->out_size = (5 << 2);
	ctx->out_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->out_phys));	
	ctx->key_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->key_phys));	
	ctx->ss = mv_wtm_func_create_ss();

	mv_wtm_dl_init(&(ctx->dl), 64, 8);

	mv_wtm_debug("in hmac sha1 cra_init\n");

	return 0;
}

static int mv_wtm_hmac_sha224_cra_init(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	ctx->scheme = WTM_HMAC_SHA224;
	ctx->out_size = (7 << 2);
	ctx->out_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->out_phys));	
	ctx->key_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->key_phys));	
	ctx->ss = mv_wtm_func_create_ss();
	mv_wtm_dl_init(&(ctx->dl), 64, 8);

	mv_wtm_debug("in hmac sha224 cra_init\n");

	return 0;
}

static int mv_wtm_hmac_sha256_cra_init(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	ctx->scheme = WTM_HMAC_SHA256;
	ctx->out_size = (8 << 2);
	ctx->out_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->out_phys));	
	ctx->key_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->key_phys));	
	ctx->ss = mv_wtm_func_create_ss();
	mv_wtm_dl_init(&(ctx->dl), 64, 8);

	mv_wtm_debug("in hmac sha256 cra_init\n");

	return 0;
}

static int mv_wtm_hmac_md5_cra_init(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	ctx->scheme = WTM_HMAC_MD5;
	ctx->out_size = (4 << 2);
	ctx->out_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->out_phys));	
	ctx->key_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->key_phys));	
	ctx->ss = mv_wtm_func_create_ss();
	mv_wtm_dl_init(&(ctx->dl), 64, 8);

	mv_wtm_debug("in hmac md5 cra_init\n");

	return 0;
}

static void mv_wtm_hmac_sha_cra_exit(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);
	
	dma_pool_free(mv_wtm_block_pool, ctx->key_virt, (dma_addr_t)(ctx->key_phys));	
	
	dma_pool_free(mv_wtm_block_pool, ctx->out_virt, (dma_addr_t)(ctx->out_phys));
	
	mv_wtm_func_destroy_ss(ctx->ss);
	mv_wtm_dl_exit(&(ctx->dl));

	return;
}


struct ahash_alg mv_wtm_hmac_sha1_alg = {
	.setkey  = mv_wtm_alg_hmac_setkey,
	.init    = mv_wtm_alg_hmac_init,
	.update  = mv_wtm_alg_hmac_update,
	.final   = mv_wtm_alg_hmac_final,
	.digest  = mv_wtm_alg_hmac_digest,
	.halg = {
		.digestsize = SHA1_DIGEST_SIZE,
		.base = {
			.cra_name		= "hmac(sha1)",
			.cra_driver_name	= "hmac-sha1-mv",
			.cra_priority		= MV_WTM_CRA_PRIORITY,
			.cra_flags		= CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
			.cra_blocksize		= SHA1_BLOCK_SIZE,
			.cra_module		= THIS_MODULE,
			.cra_init		= mv_wtm_hmac_sha1_cra_init,
			.cra_exit		= mv_wtm_hmac_sha_cra_exit,
			.cra_ctxsize		= sizeof(struct mv_wtm_ctx),
			.cra_list		= LIST_HEAD_INIT(mv_wtm_hmac_sha1_alg.halg.base.cra_list), 
		}
	}
};

struct ahash_alg mv_wtm_hmac_sha224_alg = {
	.setkey  = mv_wtm_alg_hmac_setkey,
	.init    = mv_wtm_alg_hmac_init,
	.update  = mv_wtm_alg_hmac_update,
	.final   = mv_wtm_alg_hmac_final,
	.digest  = mv_wtm_alg_hmac_digest,
	.halg = {
		.digestsize = SHA224_DIGEST_SIZE,
		.base = {
			.cra_name		= "hmac(sha224)",
			.cra_driver_name	= "hmac-sha224-mv",
			.cra_priority		= MV_WTM_CRA_PRIORITY,
			.cra_flags		= CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
			.cra_blocksize		= SHA224_BLOCK_SIZE,
			.cra_module		= THIS_MODULE,
			.cra_init		= mv_wtm_hmac_sha224_cra_init,
			.cra_exit		= mv_wtm_hmac_sha_cra_exit,
			.cra_ctxsize		= sizeof(struct mv_wtm_ctx),
			.cra_list		= LIST_HEAD_INIT(mv_wtm_hmac_sha224_alg.halg.base.cra_list), 
        }
    }
};

struct ahash_alg mv_wtm_hmac_sha256_alg = {
	.setkey  = mv_wtm_alg_hmac_setkey,
	.init    = mv_wtm_alg_hmac_init,
	.update  = mv_wtm_alg_hmac_update,
	.final   = mv_wtm_alg_hmac_final,
	.digest  = mv_wtm_alg_hmac_digest,
	.halg = {
		.digestsize = SHA256_DIGEST_SIZE,
		.base = {
			.cra_name		= "hmac(sha256)",
			.cra_driver_name	= "hmac-sha256-mv",
			.cra_priority		= MV_WTM_CRA_PRIORITY,
			.cra_flags		= CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
			.cra_blocksize		= SHA256_BLOCK_SIZE,
			.cra_module		= THIS_MODULE,
			.cra_init		= mv_wtm_hmac_sha256_cra_init,
			.cra_exit		= mv_wtm_hmac_sha_cra_exit,
			.cra_ctxsize		= sizeof(struct mv_wtm_ctx),
			.cra_list		= LIST_HEAD_INIT(mv_wtm_hmac_sha256_alg.halg.base.cra_list), 
		}
	}
};

struct ahash_alg mv_wtm_hmac_md5_alg = {
	.setkey  = mv_wtm_alg_hmac_setkey,
	.init    = mv_wtm_alg_hmac_init,
	.update  = mv_wtm_alg_hmac_update,
	.final   = mv_wtm_alg_hmac_final,
	.digest  = mv_wtm_alg_hmac_digest,
	.halg = {
		.digestsize = MD5_DIGEST_SIZE,
		.base = {
			.cra_name		= "hmac(md5)",
			.cra_driver_name	= "hmac-md5-mv",
			.cra_priority		= MV_WTM_CRA_PRIORITY,
			.cra_flags		= CRYPTO_ALG_TYPE_AHASH | CRYPTO_ALG_ASYNC,
			.cra_blocksize		= MD5_HMAC_BLOCK_SIZE,
			.cra_module		= THIS_MODULE,
			.cra_init		= mv_wtm_hmac_md5_cra_init,
			.cra_exit		= mv_wtm_hmac_sha_cra_exit,
			.cra_ctxsize		= sizeof(struct mv_wtm_ctx),
			.cra_list		= LIST_HEAD_INIT(mv_wtm_hmac_md5_alg.halg.base.cra_list), 
		}
	}
};


