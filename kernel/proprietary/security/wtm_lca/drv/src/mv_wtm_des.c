/*
 * Marvell Wireless Trusted Module (WTM) DES Algorithms
 *
 * Copyright (c) 2009 Marvell International Ltd.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/crypto.h>
#include <linux/cryptohash.h>
#include <linux/dma-mapping.h>
#include <linux/dmapool.h>
#include <linux/io.h>
#include <linux/spinlock.h>
#include <linux/kthread.h>
#include <linux/platform_device.h>
#include <linux/scatterlist.h>

#include <crypto/algapi.h>
#include <crypto/crypto_wq.h>
#include <crypto/des.h>

#include "mv_wtm.h"

/* functions for the wtm hash hardware engine */
int mv_wtm_engine_des_enc(struct ablkcipher_request *req, int err)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);
	struct mv_wtm_alg_request_ctx *rctx;
	struct mv_wtm_dl *dl;
	unsigned int use_dma;
	unsigned int *src_head, *dst_head;
	unsigned int src_len, dst_len;
	unsigned int *data, len;
	unsigned int align_len;
	unsigned int *buf;
	unsigned int *tail;
//	unsigned int resume = 0;
    unsigned int left_bytes;
	
	rctx = &(ctx->rctx);
	
	if (unlikely(err == -EINPROGRESS))
		goto out;
		
	req->base.flags |= CRYPTO_TFM_REQ_MAY_SLEEP;	

	dl = &(ctx->dl);

	if (ctx->scheme == WTM_DES_CBC) {
		memcpy(ctx->iv_virt, req->info, ctx->iv_len);
	}

	mv_wtm_dump(KERN_CONT, "iv : ", DUMP_PREFIX_OFFSET,
		    16, 1, ctx->iv_virt, DES_BLOCK_SIZE, false);

	mv_wtm_func_sym_init_nonfips(ctx->ss, ctx->scheme,
				     WTM_DES_ENCRYPTION,
				     (u32)ctx->key_phys,
				     (u32)NULL,
				     (u32)NULL,
				     ctx->key_len, 
				     (u32)ctx->iv_phys);

	mv_wtm_dl_get_dst_buf(dl, &buf);

	left_bytes = req->nbytes;
	use_dma = mv_wtm_dl_setup_sl(dl, req->src, req->nbytes, req->dst, req->nbytes);
	if (use_dma) {
		mv_wtm_dl_sync_src(dl);
		src_len = mv_wtm_dl_alloc_src_chain(dl, &src_head);
		dst_len = mv_wtm_dl_alloc_src_chain(dl, &dst_head);
		mv_wtm_func_sym_proc(ctx->ss, (u32)NULL, (u32)src_head, (u32)NULL, (u32)dst_head, src_len);
		mv_wtm_dl_free_src_chain(dl, src_head);
		mv_wtm_dl_free_src_chain(dl, dst_head);
		mv_wtm_dl_sync_dst(dl);
	} else {
		len = mv_wtm_dl_get_src_block(dl, &data, left_bytes);
		while(len) {
			mv_wtm_func_sym_proc(ctx->ss, (u32)data, (u32)NULL, (u32)buf, (u32)NULL, len);
			mv_wtm_dl_put_dst_block(dl, buf, len);
			left_bytes -= len;
			len = mv_wtm_dl_get_src_block(dl, &data, left_bytes);
//			resume = 1;
		}
	}

	len = mv_wtm_dl_get_src_tail(dl, &tail);
	align_len = (len + (16 - 1)) & ~(16 - 1);
	mv_wtm_func_sym_final(ctx->ss, (u32)tail, (u32)NULL, (u32)buf, (u32)NULL, align_len);
	mv_wtm_dl_put_dst_block(dl, buf, len);

	mv_wtm_dl_cleanup_sl(dl);
	
	req->base.complete = rctx->complete;
	
out:
    local_bh_disable();
    rctx->complete(&req->base, err);
    local_bh_enable();		

    return 0;
}

int mv_wtm_engine_des_dec(struct ablkcipher_request *req, int err)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);
	struct mv_wtm_alg_request_ctx *rctx;
	struct mv_wtm_dl *dl;
	unsigned int use_dma;
	unsigned int *src_head, *dst_head;
	unsigned int src_len, dst_len;
	unsigned int *data, len;
	unsigned int *buf;
	unsigned int *tail;
	unsigned int align_len;
//	unsigned int resume = 0;
    unsigned int left_bytes;
	
	rctx = &(ctx->rctx);
	
	if (unlikely(err == -EINPROGRESS))
		goto out;
	
	req->base.flags |= CRYPTO_TFM_REQ_MAY_SLEEP;

	dl = &(ctx->dl);

	if (ctx->scheme == WTM_DES_CBC) {
		memcpy(ctx->iv_virt, req->info, ctx->iv_len);
	}

	mv_wtm_func_sym_init_nonfips(ctx->ss, ctx->scheme,
				     WTM_DES_DECRYPTION,
				     (u32)ctx->key_phys,
				     (u32)NULL,
				     (u32)NULL,
				     ctx->key_len, 
				     (u32)ctx->iv_phys);

	mv_wtm_dl_get_dst_buf(dl, &buf);

	left_bytes = req->nbytes;
	use_dma = mv_wtm_dl_setup_sl(dl, req->src, req->nbytes,
				     req->dst, req->nbytes);
	if (use_dma) {
		mv_wtm_dl_sync_src(dl);
		src_len = mv_wtm_dl_alloc_src_chain(dl, &src_head);
		dst_len = mv_wtm_dl_alloc_src_chain(dl, &dst_head);
		mv_wtm_func_sym_proc(ctx->ss, (u32)NULL, (u32)src_head, (u32)NULL, (u32)dst_head, src_len);
		mv_wtm_dl_free_src_chain(dl, src_head);
		mv_wtm_dl_free_src_chain(dl, dst_head);
		mv_wtm_dl_sync_dst(dl);
	} else {
		len = mv_wtm_dl_get_src_block(dl, &data, left_bytes);
		while(len) {
			mv_wtm_func_sym_proc(ctx->ss, (u32)data, (u32)NULL, (u32)buf, (u32)NULL, len);
			mv_wtm_dl_put_dst_block(dl, buf, len);
			left_bytes -= len;
			len = mv_wtm_dl_get_src_block(dl, &data, left_bytes);
//			resume = 1;
		}
	}

	len = mv_wtm_dl_get_src_tail(dl, &tail);
	align_len = (len + (16 - 1)) & ~(16 - 1);
	mv_wtm_func_sym_final(ctx->ss, (u32)tail, (u32)NULL, (u32)buf, (u32)NULL, align_len);
	mv_wtm_dl_put_dst_block(dl, buf, len);

	mv_wtm_dl_cleanup_sl(dl);
	
    req->base.complete = rctx->complete;
        
out:
    local_bh_disable();
    rctx->complete(&req->base, err);
    local_bh_enable();   		

	return 0;
}

static int mv_wtm_des_setkey(struct crypto_ablkcipher *cipher, const u8 *key,
			     unsigned int len)
{
	struct crypto_tfm *tfm = crypto_ablkcipher_tfm(cipher);
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	u32 *flags = &tfm->crt_flags;
	u32 tmp[DES_EXPKEY_WORDS];
	int ret;

	/* Expand to tmp */
	ret = des_ekey(tmp, key);
	if (unlikely(ret == 0) && (*flags & CRYPTO_TFM_REQ_WEAK_KEY)) {
		*flags |= CRYPTO_TFM_RES_WEAK_KEY;
		return -EINVAL;
	}

	ctx->scheme = ctx->base;
	ctx->key_len = len;
	memcpy(ctx->key_virt, key, len);

	mv_wtm_debug("ken len    = 0x%x\n", len);
	mv_wtm_debug("ctx base   = 0x%x\n", ctx->base);
	mv_wtm_debug("ctx scheme = 0x%x\n", ctx->scheme);

	mv_wtm_dump(KERN_CONT, "key val: ", DUMP_PREFIX_OFFSET,
		    16, 1, ctx->key_virt, DES_KEY_SIZE, false);

	return 0;
}

static void mv_wtm_des_encrypt(struct crypto_async_request *req, int err)
{
    mv_wtm_engine_des_enc(ablkcipher_request_cast(req), err);
}

static void mv_wtm_des_decrypt(struct crypto_async_request *req, int err)
{
	mv_wtm_engine_des_dec(ablkcipher_request_cast(req), err);
}

static int mv_wtm_des_enqueue(struct ablkcipher_request *req,
				    crypto_completion_t complete)
{
    struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);
	struct mv_wtm_alg_request_ctx *rctx;	
	
	rctx = &(ctx->rctx);
	
	rctx->complete = req->base.complete;
	req->base.complete = complete;

	return mv_wtm_enqueue_request(&req->base);
}

static int mv_wtm_des_enc(struct ablkcipher_request *req)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);

	ctx->op = MV_WTM_OP_DES_ENC;

	return mv_wtm_des_enqueue(req, mv_wtm_des_encrypt);
}

static int mv_wtm_des_dec(struct ablkcipher_request *req)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);

	ctx->op = MV_WTM_OP_DES_DEC;

	return mv_wtm_des_enqueue(req, mv_wtm_des_decrypt);
}

static int mv_wtm_des_ecb_cra_init(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	ctx->base     = WTM_DES_ECB;
	ctx->key_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->key_phys));
	ctx->iv_virt  = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->iv_phys));
	ctx->iv_len   = DES_BLOCK_SIZE;
	ctx->ss       = mv_wtm_func_create_ss();

	mv_wtm_dl_init(&(ctx->dl), DES_BLOCK_SIZE, 8);

	return 0;
}

static int mv_wtm_des_cbc_cra_init(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	ctx->base     = WTM_DES_CBC;
	ctx->key_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->key_phys));
	ctx->iv_virt  = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->iv_phys));
	ctx->iv_len   = DES_BLOCK_SIZE;
	ctx->ss       = mv_wtm_func_create_ss();

	mv_wtm_dl_init(&(ctx->dl), DES_BLOCK_SIZE, 8);

	return 0;
}

static void mv_wtm_des_cra_exit(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);
	
	dma_pool_free(mv_wtm_block_pool, ctx->key_virt, (dma_addr_t)(ctx->key_phys));	
	
	dma_pool_free(mv_wtm_block_pool, ctx->iv_virt, (dma_addr_t)(ctx->iv_phys));

	mv_wtm_func_destroy_ss(ctx->ss);

	mv_wtm_dl_exit(&(ctx->dl));

	return;
}

struct crypto_alg mv_wtm_des_ecb_alg = {
	.cra_name		=	"ecb(des)",
	.cra_driver_name	=	"ecb-des-mv",
	.cra_priority		=	MV_WTM_CRA_PRIORITY,
	.cra_flags		=	CRYPTO_ALG_TYPE_ABLKCIPHER | CRYPTO_ALG_ASYNC,
	.cra_blocksize		=	DES_BLOCK_SIZE,
	.cra_ctxsize		=	sizeof(struct mv_wtm_ctx),
	.cra_alignmask		=	3,
	.cra_type		=	&crypto_ablkcipher_type,
	.cra_module		=	THIS_MODULE,
	.cra_init		=	mv_wtm_des_ecb_cra_init,
	.cra_exit		=	mv_wtm_des_cra_exit,
	.cra_list		=	LIST_HEAD_INIT(mv_wtm_des_ecb_alg.cra_list),
	.cra_u			= {
		.ablkcipher = {
			.min_keysize	=	DES_KEY_SIZE,
			.max_keysize	=	DES_KEY_SIZE,
			.setkey		=	mv_wtm_des_setkey,
			.encrypt	=	mv_wtm_des_enc,
			.decrypt	=	mv_wtm_des_dec,
		},
	},
};

struct crypto_alg mv_wtm_des_cbc_alg = {
	.cra_name		=	"cbc(des)",
	.cra_driver_name	=	"cbc-des-mv",
	.cra_priority		=	MV_WTM_CRA_PRIORITY,
	.cra_flags		=	CRYPTO_ALG_TYPE_ABLKCIPHER | CRYPTO_ALG_ASYNC,
	.cra_blocksize		=	DES_BLOCK_SIZE,
	.cra_ctxsize		=	sizeof(struct mv_wtm_ctx),
	.cra_alignmask		=	3,
	.cra_type		=	&crypto_ablkcipher_type,
	.cra_module		=	THIS_MODULE,
	.cra_init		=	mv_wtm_des_cbc_cra_init,
	.cra_exit		=	mv_wtm_des_cra_exit,
	.cra_list		=	LIST_HEAD_INIT(mv_wtm_des_cbc_alg.cra_list),
	.cra_u			= {
		.ablkcipher = {
		    .ivsize         = DES_BLOCK_SIZE,
			.min_keysize	=	DES_KEY_SIZE,
			.max_keysize	=	DES_KEY_SIZE,
			.setkey		=	mv_wtm_des_setkey,
			.encrypt	=	mv_wtm_des_enc,
			.decrypt	=	mv_wtm_des_dec,
		},
	},
};

