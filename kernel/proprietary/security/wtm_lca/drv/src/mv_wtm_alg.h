/*
 * Marvell Wireless Trusted Module (WTM) Algorithms Header
 *
 * Copyright (c) 2009 Marvell International Ltd.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _MV_WTM_ALG_H_

#include <crypto/algapi.h>
#include <crypto/crypto_wq.h>
#include <crypto/aes.h>
#include <crypto/hash.h>
#include <crypto/internal/hash.h>

#define MD5_DIGEST_SIZE     16
#define MD5_HMAC_BLOCK_SIZE 64
#define MD5_BLOCK_WORDS     16
#define MD5_HASH_WORDS      4

struct mv_wtm_alg_request_ctx {
	crypto_completion_t complete;
};

struct mv_wtm_ctx {
	u32 op;
	u32 scheme;
	u32 base;
	struct mv_wtm_dl dl;

	u8 *out_phys;
	u8 *out_virt;
	u32 out_size;

	u8 *key_phys;
	u8 *key_virt;
	u32 key_len;

	u8 *iv_phys;
	u8 *iv_virt;
	u32 iv_len;

	mv_wtm_ss ss;
	
	struct mv_wtm_alg_request_ctx rctx;
};

extern struct ahash_alg mv_wtm_sha1_alg;
extern struct ahash_alg mv_wtm_sha224_alg;
extern struct ahash_alg mv_wtm_sha256_alg;
extern struct ahash_alg mv_wtm_md5_alg;
extern struct ahash_alg mv_wtm_hmac_sha1_alg;
extern struct ahash_alg mv_wtm_hmac_sha224_alg;
extern struct ahash_alg mv_wtm_hmac_sha256_alg;
extern struct ahash_alg mv_wtm_hmac_md5_alg;
extern struct crypto_alg mv_wtm_aes_ecb_alg;
extern struct crypto_alg mv_wtm_aes_cbc_alg;
extern struct crypto_alg mv_wtm_aes_ctr_alg;
extern struct crypto_alg mv_wtm_aes_xts_alg;
extern struct crypto_alg mv_wtm_des_ecb_alg;
extern struct crypto_alg mv_wtm_des_cbc_alg;
extern struct crypto_alg mv_wtm_tdes_ecb_alg;
extern struct crypto_alg mv_wtm_tdes_cbc_alg;
extern struct crypto_alg mv_wtm_rc4_alg;

extern int mv_wtm_engine_hash_init(struct ahash_request *req, int err);
extern int mv_wtm_engine_hash_update(struct ahash_request *req, int err);
extern int mv_wtm_engine_hash_final(struct ahash_request *req, int err);
extern int mv_wtm_engine_hash_digest(struct ahash_request *req, int err);
extern int mv_wtm_engine_hmac_init(struct ahash_request *req, int err);
extern int mv_wtm_engine_hmac_update(struct ahash_request *req, int err);
extern int mv_wtm_engine_hmac_final(struct ahash_request *req, int err);
extern int mv_wtm_engine_hmac_digest(struct ahash_request *req, int err);
extern int mv_wtm_engine_aes_enc(struct ablkcipher_request *req, int err);
extern int mv_wtm_engine_aes_dec(struct ablkcipher_request *req, int err);
extern int mv_wtm_engine_des_enc(struct ablkcipher_request *req, int err);
extern int mv_wtm_engine_des_dec(struct ablkcipher_request *req, int err);
extern int mv_wtm_engine_tdes_enc(struct ablkcipher_request *req, int err);
extern int mv_wtm_engine_tdes_dec(struct ablkcipher_request *req, int err);
extern int mv_wtm_engine_rc4_crypt(struct ablkcipher_request *req, int err);

#endif /* _MV_WTM_ALG_H_ */

