/*
 * Marvell Wireless Trusted Module (WTM) AES Algorithms
 *
 * Copyright (c) 2009 Marvell International Ltd.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/crypto.h>
#include <linux/cryptohash.h>
#include <linux/dma-mapping.h>
#include <linux/dmapool.h>
#include <linux/io.h>
#include <linux/spinlock.h>
#include <linux/kthread.h>
#include <linux/platform_device.h>
#include <linux/scatterlist.h>

#include <crypto/algapi.h>
#include <crypto/crypto_wq.h>
#include <crypto/aes.h>

#include "mv_wtm.h"

/* functions for the wtm aes hardware engine */
int mv_wtm_engine_aes_enc(struct ablkcipher_request *req, int err)
{
    struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);
	struct mv_wtm_alg_request_ctx *rctx;
	struct mv_wtm_dl *dl;
	unsigned int use_dma;
	unsigned int *src_head, *dst_head;
	unsigned int src_len, dst_len;
	unsigned int *data, len;
	unsigned int align_len;
	unsigned int *buf;
	unsigned int *tail;
/*	unsigned int resume = 0; */
	unsigned int left_bytes;
	
	rctx = &(ctx->rctx);
	
	if (unlikely(err == -EINPROGRESS))
		goto out;
		
	req->base.flags |= CRYPTO_TFM_REQ_MAY_SLEEP;	

	dl = &(ctx->dl);

	if (((ctx->scheme >= WTM_AES_CBC128) && (ctx->scheme <= WTM_AES_CBC192)) ||
	    ((ctx->scheme >= WTM_AES_XTS256) && (ctx->scheme <= WTM_AES_XTS512))) {
		memcpy(ctx->iv_virt, req->info, ctx->iv_len);
	} else if ((ctx->scheme >= WTM_AES_CTR128) && (ctx->scheme <= WTM_AES_CTR192)) {

		unsigned char ctr_blk[4];

		ctr_blk[0] = 0;
		ctr_blk[1] = 0;
		ctr_blk[2] = 0;
		ctr_blk[3] = 1;

		memcpy(ctx->iv_virt+4, req->info, 8);
		memcpy(ctx->iv_virt+4+8, ctr_blk, 4);
	}

	mv_wtm_dump(KERN_CONT, "iv : ", DUMP_PREFIX_OFFSET,
		       16, 1, ctx->iv_virt, ctx->iv_len, false);

	/*mv_wtm_debug("iv virt = 0x%x, phys = 0x%x, size = 0x%x\n",
		ctx->iv_virt, ctx->iv_phys, ctx->iv_len); */
	/*mv_wtm_debug("key virt = 0x%x, phys = 0x%x, size = 0x%x\n",
		ctx->key_virt, ctx->key_phys, ctx->key_len); */

	/*mv_wtm_prim_aes_zerorize(); */
	/*mv_wtm_prim_aes_init(WTM_AES_ENCRYPTION, ctx->scheme,
			     (u32)ctx->key_phys,
			     (u32)(ctx->key_phys + (ctx->key_len / 2)),
			     (u32)ctx->iv_phys); */
	
	left_bytes = req->nbytes;
	use_dma = mv_wtm_dl_setup_sl(dl, req->src, req->nbytes, req->dst, req->nbytes);
	if (use_dma) {
		mv_wtm_dl_sync_src(dl);
		src_len = mv_wtm_dl_alloc_src_chain(dl, &src_head);
		dst_len = mv_wtm_dl_alloc_dst_chain(dl, &dst_head);
		/*mv_wtm_prim_aes_process((u32)NULL, (u32)NULL, src_len, 
				0, (u32)src_head, (u32)dst_head); */
		
		mv_wtm_func_sym_op_nonfips(ctx->ss, ctx->scheme, WTM_AES_ENCRYPTION, (u32)ctx->key_phys, 
			    (u32)(ctx->key_phys + (ctx->key_len / 2)), (u32)NULL, (ctx->key_len / 2), 
				(u32)ctx->iv_phys, (u32)NULL, ((struct mv_wtm_dn_rec *)src_head)->dn_phys, (u32)NULL, 
				((struct mv_wtm_dn_rec *)dst_head)->dn_phys, src_len);	        
		
        /*mv_wtm_func_sym_proc(ctx->ss, (u32)NULL, ((struct mv_wtm_dn_rec *)src_head)->dn_phys, (u32)NULL, 
		                     ((struct mv_wtm_dn_rec *)dst_head)->dn_phys, src_len); */
		
		mv_wtm_dl_free_src_chain(dl, src_head);
		mv_wtm_dl_free_dst_chain(dl, dst_head);
		
		mv_wtm_dl_sync_dst(dl);
	} else {
	    mv_wtm_dl_get_dst_buf(dl, &buf);
		len = mv_wtm_dl_get_src_block(dl, &data, left_bytes);

		if (len == 0) {
		    len = mv_wtm_dl_get_src_tail(dl, &tail);	
	        align_len = (len + (16 - 1)) & ~(16 - 1);
			
			mv_wtm_func_sym_op_nonfips(ctx->ss, ctx->scheme, WTM_AES_ENCRYPTION, (u32)ctx->key_phys, 
			    (u32)(ctx->key_phys + (ctx->key_len / 2)), (u32)NULL, (ctx->key_len / 2), 
				(u32)ctx->iv_phys, (u32)tail, (u32)NULL, (u32)buf, (u32)NULL, align_len);
	        
	        mv_wtm_dl_put_dst_block(dl, buf, len);
		} else {
		    mv_wtm_func_sym_init_nonfips(ctx->ss, ctx->scheme, WTM_AES_ENCRYPTION,
				    (u32)ctx->key_phys,
				    (u32)(ctx->key_phys + (ctx->key_len / 2)),
				    (u32)NULL,
				    (ctx->key_len / 2),
				    (u32)ctx->iv_phys);
					
			while(len) {
                mv_wtm_func_sym_proc(ctx->ss, (u32)data, (u32)NULL, (u32)buf, (u32)NULL, len);			
			    /*mv_wtm_prim_aes_process((u32)data, (u32)buf, len, 
			   			0, (u32)NULL, (u32)NULL); */
			    mv_wtm_dl_put_dst_block(dl, buf, len);
			    left_bytes -= len;
			    len = mv_wtm_dl_get_src_block(dl, &data, left_bytes);
    			/*resume = 1; */
		    }
			
			len = mv_wtm_dl_get_src_tail(dl, &tail);	
	        align_len = (len + (16 - 1)) & ~(16 - 1);
	        /*mv_wtm_prim_aes_finish((u32)tail, (u32)buf, align_len,
	        		        resume, -1, (u32)NULL, (u32)NULL); */
            mv_wtm_func_sym_final(ctx->ss, (u32)tail, (u32)NULL, (u32)buf, (u32)NULL, align_len);
	        mv_wtm_dl_put_dst_block(dl, buf, len);
		}		
	}	

	mv_wtm_dl_cleanup_sl(dl);
	/*mv_wtm_prim_aes_zerorize(); */
	
	req->base.complete = rctx->complete;
	
out:
    local_bh_disable();
    rctx->complete(&req->base, err);
    local_bh_enable();	

    return 0;
}

int mv_wtm_engine_aes_dec(struct ablkcipher_request *req, int err)
{
    struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);
	struct mv_wtm_alg_request_ctx *rctx;
	struct mv_wtm_dl *dl;
	unsigned int use_dma;
	unsigned int *src_head, *dst_head;
	unsigned int src_len, dst_len;
	unsigned int *data, len;
	unsigned int *buf;
	unsigned int *tail;
	unsigned int align_len;
/*	unsigned int resume = 0; */
    unsigned int left_bytes;
	
	rctx = &(ctx->rctx);
	
	if (unlikely(err == -EINPROGRESS))
		goto out;
	
	req->base.flags |= CRYPTO_TFM_REQ_MAY_SLEEP;

	dl = &(ctx->dl);

	if (((ctx->scheme >= WTM_AES_CBC128) && (ctx->scheme <= WTM_AES_CBC192)) ||
	    ((ctx->scheme >= WTM_AES_XTS256) && (ctx->scheme <= WTM_AES_XTS512))) {
		memcpy(ctx->iv_virt, req->info, ctx->iv_len);
	} else if ((ctx->scheme >= WTM_AES_CTR128) && (ctx->scheme <= WTM_AES_CTR192)) {

		unsigned char ctr_blk[4];

		ctr_blk[0] = 0;
		ctr_blk[1] = 0;
		ctr_blk[2] = 0;
		ctr_blk[3] = 1;

		memcpy(ctx->iv_virt+4, req->info, 8);
		memcpy(ctx->iv_virt+4+8, ctr_blk, 4);
	}

	/*mv_wtm_prim_aes_zerorize(); */
	/*mv_wtm_prim_aes_init(WTM_AES_DECRYPTION, ctx->scheme,
			     (u32)ctx->key_phys,
			     (u32)(ctx->key_phys + (ctx->key_len / 2)),
			     (u32)ctx->iv_phys); */

	left_bytes = req->nbytes;
	use_dma = mv_wtm_dl_setup_sl(dl, req->src, req->nbytes, req->dst, req->nbytes);
	if (use_dma) {
		mv_wtm_dl_sync_src(dl);
		src_len = mv_wtm_dl_alloc_src_chain(dl, &src_head);
		dst_len = mv_wtm_dl_alloc_dst_chain(dl, &dst_head);
		
		mv_wtm_func_sym_op_nonfips(ctx->ss, ctx->scheme, WTM_AES_DECRYPTION, (u32)ctx->key_phys, 
			    (u32)(ctx->key_phys + (ctx->key_len / 2)), (u32)NULL, (ctx->key_len / 2), 
				(u32)ctx->iv_phys, (u32)NULL, ((struct mv_wtm_dn_rec *)src_head)->dn_phys, (u32)NULL, 
				((struct mv_wtm_dn_rec *)dst_head)->dn_phys, src_len);
		
        /*mv_wtm_func_sym_proc(ctx->ss, (u32)NULL, ((struct mv_wtm_dn_rec *)src_head)->dn_phys, (u32)NULL, 
		                     ((struct mv_wtm_dn_rec *)dst_head)->dn_phys, src_len); */
		/*mv_wtm_prim_aes_process((u32)NULL, (u32)NULL, src_len, 
				0, (u32)src_head, (u32)dst_head); */
		mv_wtm_dl_free_src_chain(dl, src_head);
		mv_wtm_dl_free_dst_chain(dl, dst_head);
		mv_wtm_dl_sync_dst(dl);
	} else {
	    mv_wtm_dl_get_dst_buf(dl, &buf);
		len = mv_wtm_dl_get_src_block(dl, &data, left_bytes);
		if (len == 0) {
		    len = mv_wtm_dl_get_src_tail(dl, &tail);
	        align_len = (len + (16 - 1)) & ~(16 - 1);
			
			mv_wtm_func_sym_op_nonfips(ctx->ss, ctx->scheme, WTM_AES_DECRYPTION, (u32)ctx->key_phys, 
			    (u32)(ctx->key_phys + (ctx->key_len / 2)), (u32)NULL, (ctx->key_len / 2), 
				(u32)ctx->iv_phys, (u32)tail, (u32)NULL, (u32)buf, (u32)NULL, align_len);
	        
	        mv_wtm_dl_put_dst_block(dl, buf, len);		
		} else {
		    mv_wtm_func_sym_init_nonfips(ctx->ss, ctx->scheme, WTM_AES_DECRYPTION,
				    (u32)ctx->key_phys,
				    (u32)(ctx->key_phys + (ctx->key_len / 2)),
				    (u32)NULL,
				    (ctx->key_len / 2),
				    (u32)ctx->iv_phys);
					
		    while(len) {
                mv_wtm_func_sym_proc(ctx->ss, (u32)data, (u32)NULL, (u32)buf, (u32)NULL, len);
			    /*mv_wtm_prim_aes_process((u32)data, (u32)buf, len, 
			    			0, (u32)NULL, (u32)NULL); */
			    mv_wtm_dl_put_dst_block(dl, buf, len);
			    left_bytes -= len;
			    len = mv_wtm_dl_get_src_block(dl, &data, left_bytes);
			    /*resume = 1; */
		    }
			
	        len = mv_wtm_dl_get_src_tail(dl, &tail);
	        align_len = (len + (16 - 1)) & ~(16 - 1);
            mv_wtm_func_sym_final(ctx->ss, (u32)tail, (u32)NULL, (u32)buf, (u32)NULL, align_len);
	        /*mv_wtm_prim_aes_finish((u32)tail, (u32)buf, align_len,
	        		        resume, -1, (u32)NULL, (u32)NULL); */
	        mv_wtm_dl_put_dst_block(dl, buf, len);		
		}		
	}

	mv_wtm_dl_cleanup_sl(dl);
	/*mv_wtm_prim_aes_zerorize(); */
	
    req->base.complete = rctx->complete;
        
out:
    local_bh_disable();
    rctx->complete(&req->base, err);
    local_bh_enable();   	

	return 0;
}

static int mv_wtm_aes_setkey(struct crypto_ablkcipher *cipher, const u8 *key,
		unsigned int len)
{
	struct crypto_tfm *tfm = crypto_ablkcipher_tfm(cipher);
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	if (ctx->base == WTM_AES_CTR128)
		len = len - 4;

	switch (len) {
	case AES_KEYSIZE_128:
		ctx->scheme = ctx->base + 0;
		break;
	case AES_KEYSIZE_192:
		ctx->scheme = ctx->base + 2;
		break;
	case AES_KEYSIZE_256:
		if (ctx->base == WTM_AES_XTS256) {
			ctx->scheme = ctx->base;
		} else {
			ctx->scheme = ctx->base + 1;
		}
		break;
	default:
		crypto_ablkcipher_set_flags(cipher, CRYPTO_TFM_RES_BAD_KEY_LEN);
		return -EINVAL;
	}
	ctx->key_len = len;
	memcpy(ctx->key_virt, key, len);

	mv_wtm_debug("ken len    = 0x%x\n", len);
	mv_wtm_debug("ctx base   = 0x%x\n", ctx->base);
	mv_wtm_debug("ctx scheme = 0x%x\n", ctx->scheme);

	mv_wtm_dump(KERN_CONT, "key val: ", DUMP_PREFIX_OFFSET,
		       16, 1, ctx->key_virt, len, false);

	if ((ctx->scheme >= WTM_AES_CTR128) &&
	    (ctx->scheme <= WTM_AES_CTR192)) {
		memcpy(ctx->iv_virt, key+len, 4);
	}

	return 0;
}

static void mv_wtm_aes_encrypt(struct crypto_async_request *req, int err)
{
    mv_wtm_engine_aes_enc(ablkcipher_request_cast(req), err);
}

static void mv_wtm_aes_decrypt(struct crypto_async_request *req, int err)
{
	mv_wtm_engine_aes_dec(ablkcipher_request_cast(req), err);
}

static int mv_wtm_aes_enqueue(struct ablkcipher_request *req,
				    crypto_completion_t complete)
{
    struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);
	struct mv_wtm_alg_request_ctx *rctx;	
	
	rctx = &(ctx->rctx);
	
	rctx->complete = req->base.complete;
	req->base.complete = complete;

	return mv_wtm_enqueue_request(&req->base);
}

static int mv_wtm_aes_enc(struct ablkcipher_request *req)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);

	ctx->op = MV_WTM_OP_AES_ENC;	

	return mv_wtm_aes_enqueue(req, mv_wtm_aes_encrypt);
}

static int mv_wtm_aes_dec(struct ablkcipher_request *req)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(req->base.tfm);

	ctx->op = MV_WTM_OP_AES_DEC;	

	return mv_wtm_aes_enqueue(req, mv_wtm_aes_decrypt);;
}

static int mv_wtm_aes_ecb_cra_init(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	ctx->base = WTM_AES_ECB128;
	ctx->key_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->key_phys));
	ctx->iv_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->iv_phys));	
	ctx->iv_len = 16;
	ctx->ss = mv_wtm_func_create_ss();

	mv_wtm_dl_init(&(ctx->dl), 16, 8);

	return 0;
}

static int mv_wtm_aes_cbc_cra_init(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	ctx->base = WTM_AES_CBC128;
	ctx->key_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->key_phys));	
	ctx->iv_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->iv_phys));	
	ctx->iv_len = 16;
	ctx->ss = mv_wtm_func_create_ss();

	mv_wtm_dl_init(&(ctx->dl), 16, 8);

	return 0;
}

static int mv_wtm_aes_ctr_cra_init(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	ctx->base = WTM_AES_CTR128;
	ctx->key_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->key_phys));	
	ctx->iv_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->iv_phys));
	ctx->iv_len = 16;
	ctx->ss = mv_wtm_func_create_ss();

	mv_wtm_dl_init(&(ctx->dl), 16, 8);

	return 0;
}

static int mv_wtm_aes_xts_cra_init(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	ctx->base = WTM_AES_XTS256;
	ctx->key_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->key_phys));	
	ctx->iv_virt = dma_pool_alloc(mv_wtm_block_pool, GFP_KERNEL, (dma_addr_t *)&(ctx->iv_phys));	
	ctx->iv_len = 16;
	ctx->ss = mv_wtm_func_create_ss();

	mv_wtm_dl_init(&(ctx->dl), 16, 8);

	return 0;
}

static void mv_wtm_aes_cra_exit(struct crypto_tfm *tfm)
{
	struct mv_wtm_ctx *ctx = crypto_tfm_ctx(tfm);

	dma_pool_free(mv_wtm_block_pool, ctx->key_virt, (dma_addr_t)(ctx->key_phys));	

	dma_pool_free(mv_wtm_block_pool, ctx->iv_virt, (dma_addr_t)(ctx->iv_phys));	

	mv_wtm_func_destroy_ss(ctx->ss);

	mv_wtm_dl_exit(&(ctx->dl));

	return;
}

struct crypto_alg mv_wtm_aes_ecb_alg = {
	.cra_name		=	"ecb(aes)",
	.cra_driver_name	=	"ecb-aes-mv",
	.cra_priority		=	MV_WTM_CRA_PRIORITY,
	.cra_flags		=	CRYPTO_ALG_TYPE_ABLKCIPHER | CRYPTO_ALG_ASYNC,
	.cra_blocksize		=	16,
	.cra_ctxsize		=	sizeof(struct mv_wtm_ctx),
	.cra_alignmask		=	3,
	.cra_type		=	&crypto_ablkcipher_type,
	.cra_module		=	THIS_MODULE,
	.cra_init		=	mv_wtm_aes_ecb_cra_init,
	.cra_exit		=	mv_wtm_aes_cra_exit,
	.cra_list		=	LIST_HEAD_INIT(mv_wtm_aes_ecb_alg.cra_list),
	.cra_u			= {
		.ablkcipher = {
			.min_keysize	=	AES_MIN_KEY_SIZE,
			.max_keysize	=	AES_MAX_KEY_SIZE,
			.setkey		=	mv_wtm_aes_setkey,
			.encrypt	=	mv_wtm_aes_enc,
			.decrypt	=	mv_wtm_aes_dec,
		},
	},
};

struct crypto_alg mv_wtm_aes_cbc_alg = {
	.cra_name		=	"cbc(aes)",
	.cra_driver_name	=	"cbc-aes-mv",
	.cra_priority		=	MV_WTM_CRA_PRIORITY,
	.cra_flags		=	CRYPTO_ALG_TYPE_ABLKCIPHER | CRYPTO_ALG_ASYNC,
	.cra_blocksize		=	16,
	.cra_ctxsize		=	sizeof(struct mv_wtm_ctx),
	.cra_alignmask		=	3,
	.cra_type		=	&crypto_ablkcipher_type,
	.cra_module		=	THIS_MODULE,
	.cra_init		=	mv_wtm_aes_cbc_cra_init,
	.cra_exit		=	mv_wtm_aes_cra_exit,
	.cra_list		=	LIST_HEAD_INIT(mv_wtm_aes_cbc_alg.cra_list),
	.cra_u			= {
		.ablkcipher = {
		    .ivsize         = 16,
			.min_keysize	=	AES_MIN_KEY_SIZE,
			.max_keysize	=	AES_MAX_KEY_SIZE,
			.setkey		=	mv_wtm_aes_setkey,
			.encrypt	=	mv_wtm_aes_enc,
			.decrypt	=	mv_wtm_aes_dec,
		},
	},
};

struct crypto_alg mv_wtm_aes_ctr_alg = {
	.cra_name		=	"rfc3686(ctr(aes))",
	.cra_driver_name	=	"ctr-aes-mv",
	.cra_priority		=	MV_WTM_CRA_PRIORITY,
	.cra_flags		=	CRYPTO_ALG_TYPE_ABLKCIPHER | CRYPTO_ALG_ASYNC,
	.cra_blocksize		=	16,
	.cra_ctxsize		=	sizeof(struct mv_wtm_ctx),
	.cra_alignmask		=	3,
	.cra_type		=	&crypto_ablkcipher_type,
	.cra_module		=	THIS_MODULE,
	.cra_init		=	mv_wtm_aes_ctr_cra_init,
	.cra_exit		=	mv_wtm_aes_cra_exit,
	.cra_list		=	LIST_HEAD_INIT(mv_wtm_aes_ctr_alg.cra_list),
	.cra_u			= {
		.ablkcipher = {
		    .ivsize         = 8,
			.min_keysize	=	AES_MIN_KEY_SIZE + 4,
			.max_keysize	=	AES_MAX_KEY_SIZE + 4,
			.setkey		=	mv_wtm_aes_setkey,
			.encrypt	=	mv_wtm_aes_enc,
			.decrypt	=	mv_wtm_aes_dec,
		},
	},
};

struct crypto_alg mv_wtm_aes_xts_alg = {
	.cra_name		=	"xts(aes)",
	.cra_driver_name	=	"xts-aes-mv",
	.cra_priority		=	MV_WTM_CRA_PRIORITY,
	.cra_flags		=	CRYPTO_ALG_TYPE_ABLKCIPHER | CRYPTO_ALG_ASYNC,
	.cra_blocksize		=	16,
	.cra_ctxsize		=	sizeof(struct mv_wtm_ctx),
	.cra_alignmask		=	3,
	.cra_type		=	&crypto_ablkcipher_type,
	.cra_module		=	THIS_MODULE,
	.cra_init		=	mv_wtm_aes_xts_cra_init,
	.cra_exit		=	mv_wtm_aes_cra_exit,
	.cra_list		=	LIST_HEAD_INIT(mv_wtm_aes_xts_alg.cra_list),
	.cra_u			= {
		.ablkcipher = {
		    .ivsize         = 16,
			.min_keysize	=	AES_MIN_KEY_SIZE,
			.max_keysize	=	AES_MAX_KEY_SIZE,
			.setkey		=	mv_wtm_aes_setkey,
			.encrypt	=	mv_wtm_aes_enc,
			.decrypt	=	mv_wtm_aes_dec,
		},
	},
};

