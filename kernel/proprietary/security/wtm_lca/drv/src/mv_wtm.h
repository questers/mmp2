/*
 * Marvell Wireless Trusted Module (WTM) device driver
 *
 * Copyright (c) 2009 Marvell International Ltd.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _MV_WTM_H_

#define MV_WTM_CRA_PRIORITY			300
#define MV_WTM_BLOCK_SIZE			64

extern struct mv_wtm_driver *wtm_drv;

extern struct dma_pool *mv_wtm_block_pool;
extern struct dma_pool *mv_wtm_dn_pool;

extern struct device *mv_wtm_get_dev(void);
extern void mv_wtm_wait_irq(void);
extern void mv_wtm_otp(void);
extern int mv_wtm_enqueue_request(struct crypto_async_request *request);

#include "mv_wtm_func.h"
#include "mv_wtm_dl.h"
#include "mv_wtm_alg.h"
#include "mv_wtm_reg.h"

#define MV_WTM_OP_HASH_INIT			0
#define MV_WTM_OP_HASH_UPDATE			1
#define MV_WTM_OP_HASH_FINAL			2
#define MV_WTM_OP_HASH_DIGEST			3
#define MV_WTM_OP_AES_ENC			4
#define MV_WTM_OP_AES_DEC			5
#define MV_WTM_OP_DES_ENC			6
#define MV_WTM_OP_DES_DEC			7
#define MV_WTM_OP_TDES_ENC			8
#define MV_WTM_OP_TDES_DEC			9
#define MV_WTM_OP_HMAC_INIT			10
#define MV_WTM_OP_HMAC_UPDATE			11
#define MV_WTM_OP_HMAC_FINAL			12
#define MV_WTM_OP_HMAC_DIGEST			13
#define MV_WTM_OP_RC4_CRYPT			14

#undef MV_WTM_LCA_DEBUG_EN
//#define MV_WTM_LCA_DEBUG_EN

#ifdef MV_WTM_LCA_DEBUG_EN

#define mv_wtm_debug(fmt, args...)                          \
    do {                                                    \
        printk("%s:%d:%s ", __FILE__, __LINE__, __func__ ); \
        printk(fmt, ##args );                               \
    } while (0)

#define mv_wtm_dump print_hex_dump

#else

#define mv_wtm_debug(fmt, args...)
#define mv_wtm_dump(x...)

#endif

#endif /* _MV_WTM_H_ */

