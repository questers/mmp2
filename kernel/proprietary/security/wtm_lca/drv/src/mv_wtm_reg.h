/*
 * Marvell Wireless Trusted Module (WTM) Register Header file
 *
 * Copyright (c) 2009 Marvell International Ltd.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _MV_WTM_REG_H_

/*
 * wtm state
 */
#define WTM_STATE_RESET				0
#define WTM_STATE_INIT				1
#define WTM_STATE_RUN				2
#define WTM_STATE_EXIT				3

#define WTM_INT_OK				0
#define WTM_INT_DATA_ABORT			1
#define WTM_INT_ERR_CMD				2
#define WTM_INT_QUEUE_FULL			3

/*
 * wtm engine id
 */
#define WTM_ENGINE_ID_NULL			(0x0)
#define WTM_ENGINE_ID_HASH			(0x1)
#define WTM_ENGINE_ID_AES			(0x2)
#define WTM_ENGINE_ID_ZMODP			(0x3)
#define WTM_ENGINE_ID_RC4			(0x4)
#define WTM_ENGINE_ID_ENTROPY			(0x5)
#define WTM_ENGINE_ID_PRNG			(0x6)
#define WTM_ENGINE_ID_ECC			(0x7)
#define WTM_ENGINE_ID_PRIMALITY			(0x8)
#define WTM_ENGINE_ID_DMA			(0x9)
#define WTM_ENGINE_ID_MTC			(0xa)
#define WTM_ENGINE_ID_SCRATCH_PAD		(0xb)

/* 
 * wtm primitve instruction
 */
#define WTM_RESET				0x0000
#define WTM_INIT				0x0001
#define WTM_SELF_TEST				0x0002
#define WTM_SLEEP				0x0003
#define WTM_CONFIGURE_DMA			0x0004
#define WTM_SET_BATCH_COUNT			0x0005
#define WTM_ACK_BATCH_ERROR			0x0006

/* provision command */
#define WTM_GET_TSR				0x1000
#define WTM_LIFECYCLE_ADVANCE			0x1001
#define WTM_LIFECYCLE_READ			0x1002
#define WTM_SOFTWARE_VERSION_ADVANCE		0x1003
#define WTM_SOFTWARE_VERSION_READ		0x1004

#define WTM_RKEK_PROVISION			0x2000
#define WTM_OEM_PLATFORM_BIND			0x2001
#define WTM_OEM_PLATFORM_VERIFY			0x2002
#define WTM_OEM_JTAG_UNLOCKING_KEY_WRITE	0x2003
#define WTM_OEM_JTAG_UNLOCKING_KEY_READ		0x2003
#define WTM_OEM_UNIQUE_ID_WRITE			0x2004
#define WTM_OEM_UNIQUE_ID_READ			0x2005
#define WTM_OTP_WRITE_PLATFORM_CONFIG		0x2006
#define WTM_OTP_READ_PLATFORM_CONFIG		0x2007
#define WTM_OEM_USBID_PROVISION			0x2008
#define WTM_OEM_USBID_READ			0x2009

#define WTM_SET_TIM_R				0x3000
#define WTM_LAUNCH_IMG_SECURITY_CHECK		0x3001

/* fips mode switch command */
#define WTM_SWITCH_FIPS_MODE			0x4000
#define WTM_SET_FIPS_MODE_PERMANENT		0x4001
#define WTM_SET_NONFIPS_PERMANENT		0x4002

/* key management command */
#define WTM_ENROLL_KEY				0x5000
#define WTM_CREATE_KEY				0x5001
#define WTM_LOAD_KEY				0x5002
#define WTM_OIAP_START				0x5003

#define WTM_GET_CONTEXT_INFO			0x6000
#define WTM_LOAD_ENGINE_CONTEXT			0x6001
#define WTM_STORE_ENGINE_CONTEXT		0x6002
#define WTM_WRAP_KEYCONTEXT			0x6003
#define WTM_UNWRAP_KEYCONTEXT			0x6004
#define WTM_LOAD_ENGINE_CONTEXT_EXTERNAL	0x6005
#define WTM_STORE_ENGINE_CONTEXT_EXTERNAL	0x6006
#define WTM_PURGE_CONTEXT_CACHE			0x6007

#define WTM_DRBG_INIT				0x7000
#define WTM_DRBG_RESEED				0x7001
#define WTM_DRBG_GEN_RAN_BITS			0x7002

/* symmetric crypto command */
#define WTM_AES_INIT				0x8000
#define WTM_AES_ZERORIZE			0x8001
#define WTM_AES_PROCESS				0x8002
#define WTM_AES_FINISH				0x8003
#define WTM_DES_INIT				0x8004
#define WTM_DES_ZERORIZE			0x8005
#define WTM_DES_PROCESS				0x8006
#define WTM_DES_FINISH				0x8007

/* hash crypto command */
#define WTM_HASH_INIT				0x9000
#define WTM_HASH_ZERORIZE			0x9001
#define WTM_HASH_UPDATE				0x9002
#define WTM_HASH_FINAL				0x9003
#define WTM_HMAC_INIT				0x9004
#define WTM_HMAC_ZERORIZE			0x9005
#define WTM_HMAC_UPDATE				0x9006
#define WTM_HMAC_FINAL				0x9007

/* asymmetric crypto command */
#define WTM_EMSA_PKCS1_V15_VERIFY		0xA000
#define WTM_EMSA_PKCS1_V15_VERIFY_INIT		0xA001
#define WTM_EMSA_PKCS1_V15_VERIFY_UPDATE	0xA002
#define WTM_EMSA_PKCS1_V15_VERIFY_FINAL		0xA003
#define WTM_EMSA_PKCS1_V15_ZEROIZE		0xA004
#define WTM_EMSA_PKCS1_V15_SIGN			0xA005
#define WTM_EMSA_PKCS1_V15_SIGN_INIT		0xA006
#define WTM_EMSA_PKCS1_V15_SIGN_UPDATE		0xA007
#define WTM_EMSA_PKCS1_V15_SIGN_FINAL		0xA008

/* 
 * wtm supported crypto sheme
 */
#define WTM_INVALID_SCHEME			0x00000000
#define WTM_AES_ECB128				0x00008000
#define WTM_AES_ECB192				0x00008002
#define WTM_AES_ECB256				0x00008001
#define WTM_AES_CBC128				0x00008004
#define WTM_AES_CBC192				0x00008006
#define WTM_AES_CBC256				0x00008005
#define WTM_AES_CTR128				0x00008008
#define WTM_AES_CTR192				0x0000800A
#define WTM_AES_CTR256				0x00008009
#define WTM_AES_XTS256				0x0000800C
#define WTM_AES_XTS512				0x0000800D
#define WTM_SHA1				0x00009000
#define WTM_SHA224				0x00009020
#define WTM_SHA256				0x00009010
#define WTM_MD5					0x00009030
#define WTM_HMAC_SHA1				0x00009080
#define WTM_HMAC_SHA224				0x000090A0
#define WTM_HMAC_SHA256				0x00009090
#define WTM_HMAC_MD5				0x000090B0
#define WTM_PKCSV1_SHA1_1024RSA			0x0000A100
#define WTM_PKCSV1_SHA224_1024RSA		0x0000A120
#define WTM_PKCSV1_SHA256_1024RSA		0x0000A110
#define WTM_PKCSV1_SHA1_2048RSA			0x0000A200
#define WTM_PKCSV1_SHA224_2048RSA		0x0000A220
#define WTM_PKCSV1_SHA256_2048RSA		0x0000A210
#define WTM_DES_ECB				0x0000C000
#define WTM_DES_CBC				0x0000C001
#define WTM_TDES_ECB				0x0000C002
#define WTM_TDES_CBC				0x0000C003
#define WTM_RC4	    				0x0000D000

/* WTM AES function */
#define WTM_AES_ENCRYPTION			0x0
#define WTM_AES_DECRYPTION			0x1

/* WTM DES function */
#define WTM_DES_ENCRYPTION			0x0
#define WTM_DES_DECRYPTION			0x1

/* WTM TDES function */
#define WTM_TDES_ENCRYPTION			0x0
#define WTM_TDES_DECRYPTION			0x1

/* WTM RC4 function */
#define WTM_RC4_ENCRYPTION			0x0

/* WTM key output type */
#define WTM_KEY_INTERNAL			0x0
#define WTM_KEY_EXTERNEL			0x1

/* WTM key type */
#define WTM_KEY_TYPE_SYM			0x1
#define WTM_KEY_TYPE_ASYM			0x2

/* WTM work mode */
#define WTM_NONFIPS_MODE			0x0
#define WTM_FIPS_MODE				0x1

/* WTM DMA endian mode */
#define WTM_DMA_LITTLE_ENDIAN			0x0
#define WTM_DMA_BIG_ENDIAN			0x1

/* bits definition for HostIntrStatus register */
#define WTM_HOST_QUEUE_FULL			(1<<18)
#define WTM_HOST_QUEUE_BAD_ACC			(1<<17)
#define WTM_HOST_ADDR_EXCPTN			(1<<16)
#define WTM_SP_CMD_COMPLT			(1<<0)

#define WTM_CMD_FIFO_MAX_NUM			(4)
#define WTM_CMD_FIFO_NUM_MASK			(0x00000007)

/* command parameter index */
#define WTM_SRC_ARG_INDEX			(0)
#define WTM_DST_ARG_INDEX			(1)
#define WTM_MAX_IN_ARG_INDEX			(13)
#define WTM_IN_DMA_ARG_INDEX			(14)
#define WTM_OUT_DMA_ARG_INDEX			(15)

/* WTM DMA bus width & burst count */
#define WTM_DMA_BUS_WIDTH_32_BITS		(32)
#define WTM_DMA_BUS_WIDTH_64_BITS		(64)
#define WTM_DMA_BURST_COUNT			(1)

/* Max. number of the WTM batched commands */
#define WTM_MAX_BATCH_COMMANDS_NUM		(4)

/* The alignment requirement of the physical addressing arguments */
#define WTM_PHYS_ADDR_ALGN			(8)
#define WTM_LINK_LIST_ALGN			(8)
#define WTM_ADDR_IN_LIST_ALGN			(8)

/* WTM WOF size */
#define WTM_SYM_WOF_SIZE			(292)
#define WTM_ASYM_WOF_SIZE			(1600)

/* WTM TSR macros */
#define WTM_TSR_LIFE_CYCLE_MASK			(3<<4)
#define WTM_TSR_LIFE_CYCLE_CM			(0)
#define WTM_TSR_LIFE_CYCLE_DM			(1<<4)
#define WTM_TSR_LIFE_CYCLE_DD			(2<<4)
#define WTM_TSR_LIFE_CYCLE_FA			(3<<4)

#define WTM_LIFE_CYCLE_CM			(0)
#define WTM_LIFE_CYCLE_DM			(1)
#define WTM_LIFE_CYCLE_DD			(2)
#define WTM_LIFE_CYCLE_FA			(3)

#endif /* _MV_WTM_REG_H_ */

