/*
 * Marvell Wireless Trusted Module (WTM) device driver
 *
 * Copyright (c) 2009 Marvell International Ltd.
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/crypto.h>
#include <linux/dma-mapping.h>
#include <linux/dmapool.h>
#include <linux/io.h>
#include <linux/spinlock.h>
#include <linux/kthread.h>
#include <linux/platform_device.h>
#include <linux/scatterlist.h>
#include <asm/cacheflush.h>

#include <crypto/algapi.h>
#include <crypto/crypto_wq.h>
#include <crypto/aes.h>
#include <crypto/hash.h>
#include <crypto/internal/hash.h>

#include "mv_wtm.h"

#define MV_WTM_MAX_CPU_QLEN 100

struct mv_wtm_cpu_queue {
	struct crypto_queue queue;
	struct work_struct work;
};

struct mv_wtm_queue {
	struct mv_wtm_cpu_queue __percpu *cpu_queue;
};

struct mv_wtm_driver {
	struct device *dev;
	struct task_struct *th;

	/* lock protects queue */
	spinlock_t lock;
	
	struct mv_wtm_queue queue;
};

struct mv_wtm_driver *wtm_drv;
volatile struct mv_wtm_reg *wtm_reg;
struct completion wtm_drv_completion;
struct dma_pool *mv_wtm_block_pool;
struct dma_pool *mv_wtm_dn_pool;

struct device *mv_wtm_get_dev(void)
{
	return wtm_drv->dev;
}

int mv_wtm_enqueue_request(struct crypto_async_request *request)
{
	int cpu, err;
	struct mv_wtm_cpu_queue *cpu_queue;
	struct mv_wtm_queue *queue;
	
	queue = &(wtm_drv->queue);

	cpu = get_cpu();
	cpu_queue = this_cpu_ptr(queue->cpu_queue);
	err = crypto_enqueue_request(&cpu_queue->queue, request);
	queue_work_on(cpu, kcrypto_wq, &cpu_queue->work);
	put_cpu();

	return err;	
}

/*
 * All the requests will be handled by this thread
 */

/* Called in workqueue context, do one real cryption work (via
 * req->complete) and reschedule itself if there are more work to
 * do. */
static void mv_wtm_queue_worker(struct work_struct *work)
{
	struct mv_wtm_cpu_queue *cpu_queue;
	struct crypto_async_request *req, *backlog;

	cpu_queue = container_of(work, struct mv_wtm_cpu_queue, work);
	/* Only handle one request at a time to avoid hogging crypto
	 * workqueue. preempt_disable/enable is used to prevent
	 * being preempted by mv_wtm_enqueue_request() */
	preempt_disable();
	backlog = crypto_get_backlog(&cpu_queue->queue);
	req = crypto_dequeue_request(&cpu_queue->queue);
	preempt_enable();

	if (!req)
		return;

	if (backlog)
		backlog->complete(backlog, -EINPROGRESS);
		
	req->complete(req, 0);

	if (cpu_queue->queue.qlen)
		queue_work(kcrypto_wq, &cpu_queue->work);
}

static int mv_wtm_init_queue(struct mv_wtm_queue *queue,
			     unsigned int max_cpu_qlen)
{
	int cpu;
	struct mv_wtm_cpu_queue *cpu_queue;

	queue->cpu_queue = alloc_percpu(struct mv_wtm_cpu_queue);
	if (!queue->cpu_queue)
		return -ENOMEM;
	for_each_possible_cpu(cpu) {
		cpu_queue = per_cpu_ptr(queue->cpu_queue, cpu);
		crypto_init_queue(&cpu_queue->queue, max_cpu_qlen);
		INIT_WORK(&cpu_queue->work, mv_wtm_queue_worker);
	}
	return 0;
}

static void mv_wtm_fini_queue(struct mv_wtm_queue *queue)
{
	int cpu;
	struct mv_wtm_cpu_queue *cpu_queue;

	for_each_possible_cpu(cpu) {
		cpu_queue = per_cpu_ptr(queue->cpu_queue, cpu);
		BUG_ON(cpu_queue->queue.qlen);
	}
	free_percpu(queue->cpu_queue);
}

static int mv_wtm_probe(void)
{
	int ret;

	wtm_drv = kzalloc(sizeof(struct mv_wtm_driver), GFP_KERNEL);
	if (!wtm_drv)
		return -ENOMEM;

//	spin_lock_init(&wtm_drv->lock);
    ret = mv_wtm_init_queue(&(wtm_drv->queue), MV_WTM_MAX_CPU_QLEN);
	if (ret)
		goto err;

	wtm_drv->th = NULL;

	mv_wtm_block_pool = dma_pool_create("mv_wtm_block", mv_wtm_get_dev(),
                                        MV_WTM_BLOCK_SIZE, 8, 0);
	
	mv_wtm_dn_pool = dma_pool_create("mv_wtm_dn", mv_wtm_get_dev(), 
	                                 sizeof(struct mv_wtm_dn), 8, 0);
				
	crypto_register_ahash(&mv_wtm_sha1_alg);
	crypto_register_ahash(&mv_wtm_sha224_alg);
	crypto_register_ahash(&mv_wtm_sha256_alg);
//	crypto_register_ahash(&mv_wtm_md5_alg);
	crypto_register_ahash(&mv_wtm_hmac_sha1_alg);
	crypto_register_ahash(&mv_wtm_hmac_sha224_alg);
	crypto_register_ahash(&mv_wtm_hmac_sha256_alg);
//	crypto_register_ahash(&mv_wtm_hmac_md5_alg);

	crypto_register_alg(&mv_wtm_aes_ecb_alg);
	crypto_register_alg(&mv_wtm_aes_cbc_alg);
	crypto_register_alg(&mv_wtm_aes_ctr_alg);
	crypto_register_alg(&mv_wtm_aes_xts_alg);
/*	crypto_register_alg(&mv_wtm_des_ecb_alg);
	crypto_register_alg(&mv_wtm_des_cbc_alg);
	crypto_register_alg(&mv_wtm_tdes_ecb_alg);
	crypto_register_alg(&mv_wtm_tdes_cbc_alg);

	crypto_register_alg(&mv_wtm_rc4_alg); */

	return 0;

err:
	kfree(wtm_drv);
	return ret;
}

static int mv_wtm_remove(void)
{
	crypto_unregister_ahash(&mv_wtm_sha1_alg);
	crypto_unregister_ahash(&mv_wtm_sha224_alg);
	crypto_unregister_ahash(&mv_wtm_sha256_alg);
//	crypto_unregister_ahash(&mv_wtm_md5_alg);
	crypto_unregister_ahash(&mv_wtm_hmac_sha1_alg);
	crypto_unregister_ahash(&mv_wtm_hmac_sha224_alg);
	crypto_unregister_ahash(&mv_wtm_hmac_sha256_alg);
//	crypto_unregister_ahash(&mv_wtm_hmac_md5_alg);

	crypto_unregister_alg(&mv_wtm_aes_ecb_alg);
	crypto_unregister_alg(&mv_wtm_aes_cbc_alg);
	crypto_unregister_alg(&mv_wtm_aes_ctr_alg);
	crypto_unregister_alg(&mv_wtm_aes_xts_alg);
/*	crypto_unregister_alg(&mv_wtm_des_ecb_alg);
	crypto_unregister_alg(&mv_wtm_des_cbc_alg);
	crypto_unregister_alg(&mv_wtm_tdes_ecb_alg);
	crypto_unregister_alg(&mv_wtm_tdes_cbc_alg);

	crypto_unregister_alg(&mv_wtm_rc4_alg); */

	dma_pool_destroy(mv_wtm_block_pool);
	dma_pool_destroy(mv_wtm_dn_pool);

	mv_wtm_fini_queue(&(wtm_drv->queue));
	kfree(wtm_drv);

	return 0;
}

static int __init mv_wtm_init(void)
{
	int32_t ret;

	ret = mv_wtm_func_init();
	if (ret) {
		printk("ERROR - failed to init wtm core in %s\n", __FUNCTION__);
		return ret;
	}

	return mv_wtm_probe();
}

static void __exit mv_wtm_exit(void)
{
	mv_wtm_remove();

	mv_wtm_func_cleanup();

	return;
}

module_init(mv_wtm_init);
module_exit(mv_wtm_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Leo Yan <leoy@marvell.com>");
MODULE_DESCRIPTION("Marvell Wireless Trusted Module (WTM) driver");

