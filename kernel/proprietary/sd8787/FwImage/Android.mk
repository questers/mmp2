LOCAL_PATH := $(call my-dir)

wifi_firmware1 := $(TARGET_OUT_ETC)/firmware/mrvl/sd8787_uapsta.bin
$(wifi_firmware1) : $(LOCAL_PATH)/sd8787_uapsta.bin | $(ACP)
	$(transform-prebuilt-to-target)

ALL_PREBUILT += $(wifi_firmware1)

