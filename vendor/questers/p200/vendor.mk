# Copyright (C) 2010 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)



MODULE_TARGET:=$(TARGET_OUT)/lib/modules
MODULE_SOURCE:=$(LOCAL_PATH)/modules
ROOT_MODULE_TARGET:=$(TARGET_ROOT_OUT)/lib/modules

module_list := \
	sd8787.ko \
	bt8787.ko \
	galcore.ko \
	bmm.ko \
	mlan.ko \
	mv_wtm_prim.ko \
	mv_wtm_drv.ko \
	mv_wtm_lca.ko \
	mpdc_cm.ko \
	mpdc_css.ko \
	mpdc_hs.ko \
	touchkey.ko \
	touchpanel.ko \
	touchpanel_ctpm.ko \
	gsensor.ko \
	sm.ko \
	camera_ov265x.ko \
	camera_gc0308.ko \
	camera_gc2015.ko \
	camera_hm2055.ko \
	camera_ov5640.ko \
	lsensor.ko \
	devio.ko \
	gsensor_lsm303.ko \
	msensor.ko \
	gyro.ko
		
module_out := $(addprefix $(MODULE_TARGET)/,$(module_list))
$(module_out) : $(MODULE_TARGET)/% : $(MODULE_SOURCE)/% | $(ACP)
	$(transform-prebuilt-to-target)

ALL_PREBUILT += $(module_out)

root_module_list := \
	mv_wtm_drv.ko \
	mv_wtm_prim.ko
		
root_module_out := $(addprefix $(ROOT_MODULE_TARGET)/,$(root_module_list))
$(root_module_out) : $(ROOT_MODULE_TARGET)/% : $(MODULE_SOURCE)/% | $(ACP)
	$(transform-prebuilt-to-target)

ALL_PREBUILT += $(root_module_out)

file := $(PRODUCT_OUT)/zImage
$(file) : $(LOCAL_PATH)/zImage | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)

file := $(PRODUCT_OUT)/zImage_recovery
$(file) : $(LOCAL_PATH)/zImage_recovery | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)

file := $(PRODUCT_OUT)/uImage
$(file) : $(LOCAL_PATH)/uImage | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)

file := $(PRODUCT_OUT)/uImage_recovery
$(file) : $(LOCAL_PATH)/uImage_recovery | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)

file := $(PRODUCT_OUT)/splash.img
$(file) : $(LOCAL_PATH)/splash.img | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)


