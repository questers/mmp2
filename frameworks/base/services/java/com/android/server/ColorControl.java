
package com.android.server;
class ColorControl {
    public static final String PROP_COLOR_CONTROL_STATUS = "persist.sys.cc.enabled";

    public static final String COLOR_CONTROL_SETTINGS_CONTRAST = "color.control.contrast";
    public static final String COLOR_CONTROL_SETTINGS_BRIGHTNESS = "color.control.brightness";
    public static final String COLOR_CONTROL_SETTINGS_SATURATION = "color.control.saturation";
    public static final String COLOR_CONTROL_SETTINGS_HUE        = "color.control.hue";
    public static final String COLOR_CONTROL_SETTINGS_TONE       = "color.control.tone";
    public static final String COLOR_CONTROL_SETTINGS_BELEVEL    = "color.control.belevel";
    public static final String COLOR_CONTROL_SETTINGS_ACEMODE    = "color.control.acemode";
    public static final String COLOR_CONTROL_SETTINGS_FLESHTONE  = "color.control.fleshtone";
    public static final String COLOR_CONTROL_SETTINGS_GAMUT      = "color.control.gamut";
    public static final String COLOR_CONTROL_SETTINGS_VM_ID      = "color.control.vm.id";

    public static final int COLOR_CONTROL_CONTRAST_MAX           = 128;
    public static final int COLOR_CONTROL_CONTRAST_MIN           = -127;
    public static final int COLOR_CONTROL_CONTRAST_DEFAULT       = 0;

    public static final int COLOR_CONTROL_BRIGHTNESS_MAX         = 128;
    public static final int COLOR_CONTROL_BRIGHTNESS_MIN         = -127;
    public static final int COLOR_CONTROL_BRIGHTNESS_DEFAULT     = 0;

    public static final int COLOR_CONTROL_SATURATION_MAX         = 128;
    public static final int COLOR_CONTROL_SATURATION_MIN         = -127;
    public static final int COLOR_CONTROL_SATURATION_DEFAULT     = 0;

    public static final int COLOR_CONTROL_HUE_MAX                = 360;
    public static final int COLOR_CONTROL_HUE_MIN                = 0;
    public static final int COLOR_CONTROL_HUE_DEFAULT            = 0;

    public static final int COLOR_CONTROL_TONE_MAX               = 4000;
    public static final int COLOR_CONTROL_TONE_MIN               = 10000;
    public static final int COLOR_CONTROL_TONE_DEFAULT           = 6500;

    public static final int COLOR_CONTROL_VM_ID_DEFAULT          = -1;

    public static final int COLOR_CONTROL_ACE_OFF = 0;
    public static final int COLOR_CONTROL_ACE_LOW = 1;
    public static final int COLOR_CONTROL_ACE_MEDIUM = 2;
    public static final int COLOR_CONTROL_ACE_HIGH = 3;

    public static final int COLOR_CONTROL_BELEVEL_DEFAULT        = 0;
    public static final int COLOR_CONTROL_ACEMODE_DEFAULT        = COLOR_CONTROL_ACE_OFF;
    public static final int COLOR_CONTROL_FLESHTONE_DEFAULT      = 0;
    public static final int COLOR_CONTROL_GAMUT_DEFAULT          = 0;

    public static final int COLOR_CONTROL_PARAM_CONTRAST         = 0;
    public static final int COLOR_CONTROL_PARAM_BRIGHTNESS       = 1;
    public static final int COLOR_CONTROL_PARAM_SATURATION       = 2;
    public static final int COLOR_CONTROL_PARAM_HUE              = 3;
    public static final int COLOR_CONTROL_PARAM_TONE             = 4;
    public static final int COLOR_CONTROL_PARAM_BELEVEL          = 5;
    public static final int COLOR_CONTROL_PARAM_ACEMODE          = 6;
    public static final int COLOR_CONTROL_PARAM_FLESHTONE        = 7;
    public static final int COLOR_CONTROL_PARAM_GAMUT            = 8;

    public static final int COLOR_CONTROL_ROUTINE_TV = 0;
    public static final int COLOR_CONTROL_ROUTINE_LCD1 = 1;
    public static final int COLOR_CONTROL_ROUTINE_LCD2 = 2;

    public static final int COLOR_CONTROL_CHANNEL_MAIN = 0;
    public static final int COLOR_CONTROL_CHANNEL_PIP = 1;


}
