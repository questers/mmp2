/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server;

import android.content.Context;
import android.content.Intent;
import android.os.UEventObserver;
import android.util.Slog;

import java.io.FileReader;
import java.io.FileNotFoundException;

/**
 * <p>FlashlightObserver monitors for keyboard.
 */
class FlashlightObserver extends UEventObserver {
    private static final String TAG = FlashlightObserver.class.getSimpleName();
    private static final boolean LOG = true;

    private static final String FLASHLIGHT_UEVENT_MATCH = "DEVPATH=/devices/virtual/switch/flashlight";
    private static final String FLASHLIGHT_STATE_PATH = "/sys/class/switch/flashlight/state";
    private static final String FLASHLIGHT_NAME_PATH = "/sys/class/switch/flashlight/name";
	
	private final Context mContext;

    public FlashlightObserver(Context context) {
        mContext = context;
        startObserving(FLASHLIGHT_UEVENT_MATCH);

        init();  // set initial status
    }

    @Override
    public void onUEvent(UEventObserver.UEvent event) {
        if (LOG) Slog.v(TAG, "Flashlight UEVENT: " + event.toString());

        try {
            update(event.get("SWITCH_NAME"), Integer.parseInt(event.get("SWITCH_STATE")));
        } catch (NumberFormatException e) {
            Slog.e(TAG, "Could not parse switch state from event " + event);
        }
    }

    private synchronized final void init() {        
        char[] buffer = new char[1024];

        String newName = "";
        int newState = -1;

        try {
            FileReader file = new FileReader(FLASHLIGHT_STATE_PATH);
            int len = file.read(buffer, 0, 1024);
            newState = Integer.valueOf((new String(buffer, 0, len)).trim());

            file = new FileReader(FLASHLIGHT_NAME_PATH);
            len = file.read(buffer, 0, 1024);
            newName = new String(buffer, 0, len).trim();

        } catch (FileNotFoundException e) {
            Slog.w(TAG, "This kernel does not have flashlight keyboard support");
        } catch (Exception e) {
            Slog.e(TAG, "" , e);
        }

        update(newName, newState);
    }

    private synchronized final void update(String newName, int newState) {
        if (newName.equals("flashlight") && newState == 0) {
            Intent intent = new Intent("quester.intent.action.FLASHLIGHT");
            mContext.sendBroadcast(intent);
        }
    }

}
