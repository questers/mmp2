/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.ContentResolver;
import android.net.Uri;
import android.os.IColorControlService;
import android.os.Environment;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.UEventObserver;
import android.os.Handler;
import android.os.Message;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.provider.Settings;

/**
 * ColorControlService implements an to the color control service
 * @hide
 */
class ColorControlService extends IColorControlService.Stub {
    
    private static final String TAG = "ColorControlService";


    public static final int SYSTEM_MODE = 0;
    public static final int APP_MODE = 1;

    private int mNativeData = 0;
    private int mCurrentMode = SYSTEM_MODE;
    private boolean mEnableCMU = false;
    private IcrViewModeParam mSystemParam = null;
    private IcrViewModeParam mAppParam = null;
    private boolean mIdle = true;
    private int mCurrentRoutine = -1;

    /**
     * Binder context for this service
     */
    private Context mContext;

    static {
        classInitNative();
    }
    
    /**
     * Constructs a new ColorControlService instance
     * 
     * @param context  Binder context for this service
     */
    public ColorControlService(Context context) {
        mContext = context;
        mEnableCMU = SystemProperties.getBoolean( ColorControl.PROP_COLOR_CONTROL_STATUS, false );

        if (!mEnableCMU)
        {
            Log.i(TAG, "Enable CMU");
            SystemProperties.set( ColorControl.PROP_COLOR_CONTROL_STATUS, "true");
            mEnableCMU = SystemProperties.getBoolean( ColorControl.PROP_COLOR_CONTROL_STATUS, false );
        }

        initDataNative();
        mSystemParam = new IcrViewModeParam(context);
        if( mEnableCMU ) {
            // apply settings
            int result = openNative();
            if( result != 0 ) {
                // openNative failed
                Log.e(TAG, "openNative failed with results : " + result);
                return;
            }
            result = setRoutineNative(ColorControl.COLOR_CONTROL_ROUTINE_LCD1);
            if( result != 0 ) {
                // setRoutineNative failed
                Log.e(TAG, "setRoutineNative failed with results : " + result);
                return;
            }
            mCurrentRoutine = ColorControl.COLOR_CONTROL_ROUTINE_LCD1;
            // ok, now we begin to apply system settings
            result = applySystemSettings();
            if( result != 0 ) {
                // applySystemSettings failed
                Log.e(TAG, "applySystemSettings failed with results : " + result);
                return;
            }
        }
    }

    private int applySystemSettings() {
        int result = 1;
        result &= setColorParamNative(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, ColorControl.COLOR_CONTROL_PARAM_CONTRAST, mSystemParam.mContrast);
        result &= setColorParamNative(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, ColorControl.COLOR_CONTROL_PARAM_BRIGHTNESS, mSystemParam.mBrightness); 
        result &= setColorParamNative(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, ColorControl.COLOR_CONTROL_PARAM_SATURATION, mSystemParam.mSaturation);
        result &= setColorParamNative(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, ColorControl.COLOR_CONTROL_PARAM_HUE, mSystemParam.mHue); 
        result &= setColorParamNative(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, ColorControl.COLOR_CONTROL_PARAM_TONE, mSystemParam.mTone); 
/*
        result &= setColorParamNative(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, ColorControl.COLOR_CONTROL_PARAM_BELEVEL, mSystemParam.mBeLevel); 
        result &= setColorParamNative(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, ColorControl.COLOR_CONTROL_PARAM_ACEMODE, mSystemParam.mAceMode); 
        result &= setColorParamNative(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, ColorControl.COLOR_CONTROL_PARAM_FLESHTONE, mSystemParam.mFleshToneEnabled);
        result &= setColorParamNative(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, ColorControl.COLOR_CONTROL_PARAM_GAMUT, mSystemParam.mGamutCompressEnabled);
*/
        if( mSystemParam.mViewModeId != -1 ) {
            result &= setViewModeNative(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, mSystemParam.mViewModeId);
            Log.e(TAG, "applySystemSettings with id =" + mSystemParam.mViewModeId);

            int mode = getViewModeNative(ColorControl.COLOR_CONTROL_CHANNEL_MAIN);
            Log.e(TAG, "check view mode: "+mode);
        }
        return result;
    }

    public class IcrViewModeParam {
        public int mContrast;
        public int mBrightness;
        public int mSaturation;
        public int mHue;
        public int mTone;
        public int mBeLevel;
        public int mAceMode;
        public int mFleshToneEnabled;
        public int mGamutCompressEnabled;
        public int mViewModeId;
        public int mLocalAxis[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        public int mLocalHue[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        public int mLocalSat[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        public int mOutCsc[][] = {{0 ,0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
        public IcrViewModeParam(Context context) {
            reset2System(context);
        }
        public IcrViewModeParam(IcrViewModeParam param) {
            reset(param);
        }
        public void reset2System(Context context) {
            // get system global cmu setting params
            final ContentResolver resolver = context.getContentResolver();
            mContrast = Settings.System.getInt(resolver, ColorControl.COLOR_CONTROL_SETTINGS_CONTRAST, ColorControl.COLOR_CONTROL_CONTRAST_DEFAULT );
            mBrightness = Settings.System.getInt(resolver, ColorControl.COLOR_CONTROL_SETTINGS_BRIGHTNESS, ColorControl.COLOR_CONTROL_BRIGHTNESS_DEFAULT );
            mSaturation = Settings.System.getInt(resolver, ColorControl.COLOR_CONTROL_SETTINGS_SATURATION, ColorControl.COLOR_CONTROL_SATURATION_DEFAULT );
            mHue = Settings.System.getInt(resolver, ColorControl.COLOR_CONTROL_SETTINGS_HUE, ColorControl.COLOR_CONTROL_HUE_DEFAULT );
            mTone = Settings.System.getInt(resolver, ColorControl.COLOR_CONTROL_SETTINGS_TONE, ColorControl.COLOR_CONTROL_TONE_DEFAULT );
            mBeLevel = Settings.System.getInt(resolver, ColorControl.COLOR_CONTROL_SETTINGS_BELEVEL, ColorControl.COLOR_CONTROL_BELEVEL_DEFAULT );
            mAceMode = Settings.System.getInt(resolver, ColorControl.COLOR_CONTROL_SETTINGS_ACEMODE, ColorControl.COLOR_CONTROL_ACEMODE_DEFAULT );
            mFleshToneEnabled = Settings.System.getInt(resolver, ColorControl.COLOR_CONTROL_SETTINGS_FLESHTONE, ColorControl.COLOR_CONTROL_FLESHTONE_DEFAULT );
            mGamutCompressEnabled = Settings.System.getInt(resolver, ColorControl.COLOR_CONTROL_SETTINGS_GAMUT, ColorControl.COLOR_CONTROL_GAMUT_DEFAULT );            
            mViewModeId = Settings.System.getInt(resolver, ColorControl.COLOR_CONTROL_SETTINGS_VM_ID, ColorControl.COLOR_CONTROL_VM_ID_DEFAULT);

            String s = String.format("contrast: %d, saturation: %d, hue: %d, mode: %d",
                    mContrast, mSaturation, mHue, mViewModeId);
            Log.i(TAG, s);
        }
        public void reset( IcrViewModeParam param ) {
            mContrast = param.mContrast;
            mBrightness = param.mBrightness;
            mSaturation = param.mSaturation;
            mHue = param.mHue;
            mTone = param.mTone;
            mBeLevel = param.mBeLevel;
            mAceMode = param.mAceMode;
            mFleshToneEnabled = param.mFleshToneEnabled;
            mGamutCompressEnabled = param.mGamutCompressEnabled;
            mViewModeId = param.mViewModeId;          
        }
    };

    /**
     * Acquire ColorControlService, 1 acquired 0 failed
     */
    public int acquireService(int type) {
        int ret = 0;
        synchronized(mContext) {
            // TODO: get a better way to let system access ColorControlService when CMU is disabled
            if( !mEnableCMU && type == APP_MODE ) {
                return 0;
            }
            if( mIdle ) {
                mIdle = false;
                ret = 1;
                // acquire it
                mCurrentMode = type;
                if(mCurrentMode == APP_MODE)  {
                    if( mAppParam == null ) {
                        mAppParam = new IcrViewModeParam(mSystemParam);                        
                    } else {
                        mAppParam.reset(mSystemParam);
                    }
                }
                
            }
        }
        return ret;
    }

    /**
     * Release ColorControlService
     */
    public void releaseService() {
        synchronized(mContext) {
            if( !mIdle ) {                
                mIdle = true;
                if( mCurrentMode == APP_MODE && mEnableCMU ) {
                    mCurrentMode = SYSTEM_MODE;
                    // apply global system setting
                    int ret = setCMURoutine(ColorControl.COLOR_CONTROL_ROUTINE_LCD1);
                    if( ret != 0 ) {
                        Log.e(TAG, "releaseService-->setCMURoutine faild with result : " + ret );
                    }
                    mCurrentRoutine = ColorControl.COLOR_CONTROL_ROUTINE_LCD1;
                    ret = applySystemSettings();
                    if( ret != 0 ) {
                        Log.e(TAG, "releaseService-->applySystemSettings faild with result : " + ret );
                    }
                }
            }
        }
    }

    public int enableCMU(IBinder id, boolean enable) {
        // TODO: check permission first
        int ret = 0;
        if( mEnableCMU == enable ) {
            return ret;
        }
        if( enable ) {
            ret = openNative();
        }
        ret = enableCMUNative(enable); 
        if (ret == 0 ) mEnableCMU = enable;        
        if( mEnableCMU ) {
            //setRoutineNative(ColorControl.COLOR_CONTROL_ROUTINE_LCD1);
            mCurrentRoutine = ColorControl.COLOR_CONTROL_ROUTINE_LCD1;
            ret = applySystemSettings();
        }
        SystemProperties.set(ColorControl.PROP_COLOR_CONTROL_STATUS, enable? "1" : "0");
        if( !enable ) {
            Log.e(TAG, "closeNative");
            ret = closeNative();
        }
        return ret;
    }
    /**
     * Set CMU Routine
     */
    public int setCMURoutine( int routine ) {
        int ret = -1;
        if( routine < ColorControl.COLOR_CONTROL_ROUTINE_TV && routine > ColorControl.COLOR_CONTROL_ROUTINE_LCD2 ) {
            return ret;
        }
        if( mEnableCMU ) {
            ret = setRoutineNative(routine);
        }
        if( ret == 0 ) {
            mCurrentRoutine = routine;
        }
        return ret;
    }

    /**
     * Get CMU Routine
     */
    public int getCMURoutine() {
        if( !mEnableCMU ) {
            return -1;
        } else {
            return getRoutineNative();
        }
    }

    public String[] getViewModeList() {
        return getViewModeListNative();
    }
    
    public int[] getViewModeIdList() {
        return getViewModeIdListNative();
    }

    public int setViewMode(int channel, int id) {
        if( !mEnableCMU ) {
            return -1;
        } else {
            int ret = setViewModeNative(channel, id);
            if( ret == 0 && channel == ColorControl.COLOR_CONTROL_CHANNEL_MAIN && mCurrentMode == SYSTEM_MODE ) {
                // save setting information
                final ContentResolver resolver = mContext.getContentResolver();
                if( resolver == null ) {
                    Log.e(TAG, "mContext.getContentResolver return null");
                    return 0;
                }
                Settings.System.putInt(resolver, ColorControl.COLOR_CONTROL_SETTINGS_VM_ID, id);
                Log.e(TAG, "putInt : " + ColorControl.COLOR_CONTROL_SETTINGS_VM_ID + " = " + id);
            } else {
                Log.e(TAG, "ret = " + ret + "; channel = " + channel + "; mode = " + mCurrentMode);
            }
            IcrViewModeParam vmParam = ( mCurrentMode == SYSTEM_MODE ) ? mSystemParam : mAppParam;
            vmParam.mViewModeId = id;            
            return ret;
        }
    }
    public int getViewMode(int channel) {
        if( !mEnableCMU ) {
            Log.i(TAG, "CMU not enabled");
            return -1;
        } else {
            Log.i(TAG, "getViewMode: "+getViewModeNative(channel));
            return getViewModeNative(channel);
        }
       
    }
    /**
     * Set PIP rectangle
     */
    //virtual int setPIPRect(Rect pip) =0;

    /**
     * Get PIP rectangle
     */
    //virtual Rect getPIPRect() = 0;

    /**
     * Set cmu parameter
     */
    public int setColorParameter(int channel, int param, int value) {
        int ret = -1;
        if( mEnableCMU ) {
            if( channel != ColorControl.COLOR_CONTROL_CHANNEL_MAIN && channel != ColorControl.COLOR_CONTROL_CHANNEL_PIP ) {
                Log.e(TAG, "invalid channel : " + channel);
                return ret;
            }            
            ret = setColorParamNative(channel, param, value);
            IcrViewModeParam vmParam = ( mCurrentMode == SYSTEM_MODE ) ? mSystemParam : mAppParam;
 
            final ContentResolver resolver = mContext.getContentResolver();
            if( resolver == null ) {
                Log.e(TAG, "mContext.getContentResolver return null");
                return 0;
            }
            String name = null;
            if( ret == 0 ) {
                switch( param ) {
                case ColorControl.COLOR_CONTROL_PARAM_CONTRAST:
                    vmParam.mContrast = value;
                    name = ColorControl.COLOR_CONTROL_SETTINGS_CONTRAST;
                    break;
                case ColorControl.COLOR_CONTROL_PARAM_BRIGHTNESS:
                    vmParam.mBrightness = value;
                    name = ColorControl.COLOR_CONTROL_SETTINGS_BRIGHTNESS;
                    break;
                case ColorControl.COLOR_CONTROL_PARAM_SATURATION:
                    vmParam.mSaturation = value;
                    name = ColorControl.COLOR_CONTROL_SETTINGS_SATURATION;
                    break;
                case ColorControl.COLOR_CONTROL_PARAM_HUE:
                    vmParam.mHue = value;
                    name = ColorControl.COLOR_CONTROL_SETTINGS_HUE;
                    break;
                case ColorControl.COLOR_CONTROL_PARAM_TONE:
                    vmParam.mTone = value;
                    name = ColorControl.COLOR_CONTROL_SETTINGS_TONE;
                    break;
                case ColorControl.COLOR_CONTROL_PARAM_BELEVEL:
                    vmParam.mBeLevel = value;
                    name = ColorControl.COLOR_CONTROL_SETTINGS_BELEVEL;
                    break;
                case ColorControl.COLOR_CONTROL_PARAM_ACEMODE:
                    vmParam.mAceMode = value;
                    name = ColorControl.COLOR_CONTROL_SETTINGS_ACEMODE;
                    break;
                case ColorControl.COLOR_CONTROL_PARAM_FLESHTONE:
                    vmParam.mFleshToneEnabled = value;
                    name = ColorControl.COLOR_CONTROL_SETTINGS_FLESHTONE;
                    break;
                case ColorControl.COLOR_CONTROL_PARAM_GAMUT:
                    vmParam.mGamutCompressEnabled = value;
                    name = ColorControl.COLOR_CONTROL_SETTINGS_GAMUT;
                    break;
                default:
                    break;
                }
                if( mCurrentMode == SYSTEM_MODE && name != null && channel == ColorControl.COLOR_CONTROL_CHANNEL_MAIN){
                    Settings.System.putInt(resolver, name, value);
                }
            }

        }       
        return ret;
    }

    /**
     * Get cmu parameter
     */
    public int getColorParameter(int channel, int param) {
        int ret = -1;
        if( mEnableCMU ) {
            ret = getColorParamNative( channel, param );
        }
        return ret;
    }

    @Override
    protected void finalize() throws Throwable {
        try{
            cleanupDataNative();
        } finally {
            super.finalize();
        }
    }


    // open IcrSession, the native function will call IcrOpen
    private native int openNative();

    // close IcrSession, the native function will call IcrClose
    private native int closeNative();

    // enable/disable CMU
    private native int enableCMUNative(boolean enable);

    // set CMU routine
    private native int setRoutineNative(int value);

    // get current CMU routine
    private native int getRoutineNative();
    
    // set CMU params
    private native int setColorParamNative(int channel, int param, int value);

    // get CMU params
    private native int getColorParamNative(int channel, int param);

    private native static void classInitNative();
    
    private native boolean initDataNative();

    private native void cleanupDataNative();

    private native String[] getViewModeListNative();
    private native int[] getViewModeIdListNative();
    private native int setViewModeNative(int channel, int id);
    private native int getViewModeNative(int channel);
}

