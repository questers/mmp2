/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server;

import java.io.File;
import java.io.IOException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.RecoverySystem;
import android.util.Log;
import android.util.Slog;

public class OtaUpdateReceiver extends BroadcastReceiver {

    private static final String TAG = "OtaUpdateReceiver";

    @Override
    public void onReceive(final Context context, final Intent intent) {

	Slog.w(TAG, "!!! RECOVERY MODE UPDATE PACKAGE !!!");
	// The reboot call is blocking, so we need to do it on another thread.
	Thread thr = new Thread("Reboot") {
	    @Override
	    public void run() {
		try {
		    String path = intent.getStringExtra("ota_package_path");
		    File packageFile = new File(path);
		    RecoverySystem.installPackage(context, packageFile);
		    Log.wtf(TAG, "Still running recovery update!");
		} catch (IOException e) {
		    Slog.e(TAG, "Can't perform update package", e);
		}
	    }
	};
	thr.start();
    }
}
