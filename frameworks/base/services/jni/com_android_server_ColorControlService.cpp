#define LOG_TAG "ColorControlService_JNI"

#include "utils/Log.h"
#include "jni.h"
#include "JNIHelp.h"
#ifdef HAVE_CMU
#include "ctrlsvr.h"
static const int CMU_PARAM_CONTRAST         = 0;
static const int CMU_PARAM_BRIGHTNESS       = 1;
static const int CMU_PARAM_SATURATION       = 2;
static const int CMU_PARAM_HUE              = 3;
static const int CMU_PARAM_TONE             = 4;
static const int CMU_PARAM_BELEVEL          = 5;
static const int CMU_PARAM_ACEMODE          = 6;
static const int CMU_PARAM_FLESHTONE        = 7;
static const int CMU_PARAM_GAMUT            = 8;

static const int SYSTEM_MODE = 0;
static const int APP_MODE = 0;

static const int CMU_ROUTINE_TV = 0;
static const int CMU_ROUTINE_LCD1 = 1;
static const int CMU_ROUTINE_LCD2 = 2;

static const int CMU_CHANNEL_MAIN = 0;
static const int CMU_CHANNEL_PIP = 1;

static const int CMU_ACE_OFF = 0;
static const int CMU_ACE_LOW = 1;
static const int CMU_ACE_MEDIUM = 2;
static const int CMU_ACE_HIGH = 3;

static const int CMU_VIEWMODE_VIVID	    = 2;
static const int CMU_VIEWMODE_SOFT 		= 3;
static const int CMU_VIEWMODE_LANDSCAPE	= 4;
static const int CMU_VIEWMODE_PORTRAIT	= 5;
static const int CMU_VIEWMODE_SEPIA		= 6;
static const int CMU_VIEWMODE_PLATINUM	= 7;
static const int CMU_VIEWMODE_SKY		= 8;
static const int CMU_VIEWMODE_SKYGRASS	= 9;

const IcrViewModeParam icr_vm_vivid = {
	CONTRAST_NORMAL, BRIGHTNESS_NORMAL, SATURATION_NORMAL, HUE_NORMAL, TONE_NORMAL,
	0, ICR_ACE_OFF,
	1, 1,
	{0,   40,  80,  120, 160, 200, 240, 280, 320, 360, 0, 0, 0, 0},
	{0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f, 0, 0, 0, 0},
	{120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 0, 0, 0, 0},
	{
		256,   0,   0,   0,
		  0, 256,   0,   0,
		  0,   0, 256,   0,
	},
};

const IcrViewModeParam icr_vm_soft = {
	CONTRAST_NORMAL, BRIGHTNESS_NORMAL, SATURATION_NORMAL, HUE_NORMAL, TONE_NORMAL,
	0, ICR_ACE_OFF,
	1, 1,
	{0,   40,  80,  120, 160, 200, 240, 280, 320, 360, 0, 0, 0, 0},
	{0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f, 0, 0, 0, 0},
	{30,  30,  30,  30,  30,  30,  30,  30,  30,  30, 0, 0, 0, 0},
	{
		256,   0,   0,   0,
		  0, 256,   0,   0,
		  0,   0, 256,   0,
	},
};

const IcrViewModeParam icr_vm_landscape = {
	CONTRAST_NORMAL, BRIGHTNESS_NORMAL, SATURATION_NORMAL, HUE_NORMAL, TONE_NORMAL,
	0, ICR_ACE_OFF,
	0, 1,
	{0,  160,  170, 250, 280, 300, 310, 330, 340, 360, 0, 0, 0, 0},
	{15,  15,   15,   0,  15,  15,  25,   5,  15,  15, 0, 0, 0, 0},
	{64 , 64,  100, 100,  64,  64,  80,  80,  64,  64, 0, 0, 0, 0},
	{
		256,   0,   0,   0,
		  0, 256,   0,   0,
		  0,   0, 256,   0,
	},
};

const IcrViewModeParam icr_vm_portrait = {
	CONTRAST_NORMAL, BRIGHTNESS_NORMAL, SATURATION_NORMAL, HUE_NORMAL, TONE_NORMAL,
	0, ICR_ACE_OFF,
	1, 1,
	{0,  160,  170, 250, 280, 300, 310, 330, 340, 360, 0, 0, 0, 0},
	{15,  15,   15,   0,  15,  15,  25,   5,  15,  15, 0, 0, 0, 0},
	{64 , 64,  100, 100,  64,  64,  80,  80,  64,  64, 0, 0, 0, 0},
	{
		256,   0,   0,   0,
		  0, 256,   0,   0,
		  0,   0, 256,   0,
	},
};

#define TONE_MATRIX(r, g, b, percent)\
{\
	(r*256/(r+2*g+b) * percent + 256 * (100-percent)) / 100, r*512/(r+2*g+b), r*256/(r+2*g+b), 0,\
	g*256/(r+2*g+b), (g*512/(r+2*g+b) * percent + 256 * (100-percent)) / 100, g*256/(r+2*g+b), 0,\
	b*256/(r+2*g+b), b*512/(r+2*g+b), (b*256/(r+2*g+b) * percent + 256 * (100-percent)) / 100, 0,\
}

const IcrViewModeParam icr_vm_sepia = {
	CONTRAST_NORMAL+50, BRIGHTNESS_NORMAL+20, SATURATION_NORMAL, HUE_NORMAL, TONE_NORMAL,
	0, ICR_ACE_OFF,
	0, 1,
	{   0,  40,  80, 120, 160, 200, 240, 280, 320, 360, 0, 0, 0, 0},
	{0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f, 0, 0, 0, 0},
	{  64,  64,  64,  64,  64,  64,  64,  64,  64,  64, 0, 0, 0, 0},
	TONE_MATRIX( 162, 138, 101, 100 ),
};

const IcrViewModeParam icr_vm_platinum = {
	CONTRAST_NORMAL, BRIGHTNESS_NORMAL, SATURATION_NORMAL, HUE_NORMAL, TONE_NORMAL,
	0, ICR_ACE_OFF,
	0, 1,
	{   0,  40,  80, 120, 160, 200, 240, 280, 320, 360, 0, 0, 0, 0},
	{0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f, 0, 0, 0, 0},
	{  64,  64,  64,  64,  64,  64,  64,  64,  64,  64, 0, 0, 0, 0},
	TONE_MATRIX( 100, 100, 111, 100 ),
};

IcrError setupIcrModes(IcrSession* pSession)
{
	IcrError err;

	if ( !pSession )
		return ICR_ERR_INVALID_PARAM;

	err = IcrViewModeRegister(pSession, CMU_VIEWMODE_VIVID, &icr_vm_vivid);
	if (err != ICR_ERR_OK)
		return err;

	err = IcrViewModeRegister(pSession, CMU_VIEWMODE_SOFT, &icr_vm_soft);
	if (err != ICR_ERR_OK)
		return err;

	err = IcrViewModeRegister(pSession, CMU_VIEWMODE_LANDSCAPE, &icr_vm_landscape);
	if (err != ICR_ERR_OK)
		return err;

	err = IcrViewModeRegister(pSession, CMU_VIEWMODE_PORTRAIT, &icr_vm_portrait);
	if (err != ICR_ERR_OK)
		return err;

	err = IcrViewModeRegister(pSession, CMU_VIEWMODE_SEPIA, &icr_vm_sepia);
	if (err != ICR_ERR_OK)
		return err;

	err = IcrViewModeRegister(pSession, CMU_VIEWMODE_PLATINUM, &icr_vm_platinum);
	if (err != ICR_ERR_OK)
		return err;

	return err;
}

static const char* ViewModeName[] = {"vivid", "soft", "landscape", "portrait", "sepia", "platinum"};
static const int   VideoModeId[] = {CMU_VIEWMODE_VIVID, CMU_VIEWMODE_SOFT, CMU_VIEWMODE_LANDSCAPE, 
            CMU_VIEWMODE_PORTRAIT, CMU_VIEWMODE_SEPIA, CMU_VIEWMODE_PLATINUM};

#endif

namespace android {
#ifdef HAVE_CMU
static jfieldID field_mNativeData;

typedef struct {
    IcrSession* session;
    int icrModeIndex;
} native_data_t;

jfieldID get_field(JNIEnv *env, jclass clazz, const char *member,
                   const char *mtype) {
    jfieldID field = env->GetFieldID(clazz, member, mtype);
    if (field == NULL) {
        LOGE("Can't find member %s", member);
    }
    return field;
}

/** Get native data stored in the opaque (Java code maintained) pointer mNativeData
 *  Perform quick sanity check, if there are any problems return NULL
 */
static inline native_data_t * get_native_data(JNIEnv *env, jobject object) {
    native_data_t *nat =
            (native_data_t *)(env->GetIntField(object, field_mNativeData));
    if (nat == NULL) {
        LOGE("Uninitialized native data\n");
        return NULL;
    }
    return nat;
}

#endif

static void classInitNative(JNIEnv* env, jclass clazz) {
    LOGV(__FUNCTION__);
#ifdef HAVE_CMU
    field_mNativeData = get_field(env, clazz, "mNativeData", "I");
#endif
}

static bool initDataNative(JNIEnv* env, jobject object) {
    LOGV(__FUNCTION__);
#ifdef HAVE_CMU
    native_data_t *nat = (native_data_t *)calloc(1, sizeof(native_data_t));
    if (NULL == nat) {
        LOGE("%s: out of memory!", __FUNCTION__);
        return false;
    }
    
    env->SetIntField(object, field_mNativeData, (jint)nat);

    nat->session = NULL;
    nat->icrModeIndex = -1;
#endif  /*HAVE_CMU*/
    return true;
}

static void cleanupDataNative(JNIEnv* env, jobject object) {
    LOGV(__FUNCTION__);
#ifdef HAVE_CMU
    native_data_t *nat =
        (native_data_t *)env->GetIntField(object, field_mNativeData);
    if (nat) {
        if( nat->session ) {
            IcrClose(nat->session);
        }
        free(nat);
        nat = NULL;
        
    }
#endif
}

static int openNative(JNIEnv* env, jobject object) {
    LOGV(__FUNCTION__);
    int ret = 0;
#ifdef HAVE_CMU
    native_data_t *nat =
        (native_data_t *)env->GetIntField(object, field_mNativeData);
    if (nat) {
        if( nat->session == NULL ) {
            ret = IcrOpen( &(nat->session) );
            if( ret == ICR_ERR_OK ) {
                ret = setupIcrModes( nat->session);
            }
        }
    }
#endif
    return ret;
}

static int closeNative(JNIEnv* env, jobject object) {
    LOGV(__FUNCTION__);
    int ret = 0;
#ifdef HAVE_CMU
    native_data_t *nat =
        (native_data_t *)env->GetIntField(object, field_mNativeData);
    if (nat && nat->session) {
        ret = IcrClose( nat->session );
        nat->session = NULL;
    }    
#endif
    return ret;
}

// by default, when enable cmu, set routine to CMU_ROUTINE_LCD1
static int enableCMUNative(JNIEnv* env, jobject object, jboolean enable) {
    LOGV(__FUNCTION__);
    int ret = 0;
#ifdef HAVE_CMU
    native_data_t *nat =
        (native_data_t *)env->GetIntField(object, field_mNativeData);
    if (nat && nat->session) {
        IcrRoute route = enable ? ICR_ROUTE_LCD1 : ICR_ROUTE_OFF;
        ret = IcrRouteSet( nat->session, route );
    }
#endif
    return ret;
}

static int setRoutineNative(JNIEnv* env, jobject object, jint value) {
    LOGV(__FUNCTION__);
    int ret = 0;
#ifdef HAVE_CMU
    native_data_t *nat =
        (native_data_t *)env->GetIntField(object, field_mNativeData);
    if (nat && nat->session) {
        IcrRoute route = ICR_ROUTE_OFF;
        switch( value ) {
        case CMU_ROUTINE_TV:
            route = ICR_ROUTE_TV;
            break;
        case CMU_ROUTINE_LCD1:
            route = ICR_ROUTE_LCD1;
            break;
        case CMU_ROUTINE_LCD2:
            route = ICR_ROUTE_LCD2;
            break;
        default:
            break;
        }
        if( route != ICR_ROUTE_OFF ) {
            ret = IcrRouteSet( nat->session, route);
        }
    }    
#endif
    return ret;
}

static int getRoutineNative(JNIEnv* env, jobject object) {
    LOGV(__FUNCTION__);
    int ret = 0;
#ifdef HAVE_CMU
    native_data_t *nat =
        (native_data_t *)env->GetIntField(object, field_mNativeData);
    if (nat && nat->session) {
        IcrRoute route = ICR_ROUTE_OFF;
        IcrRouteGet( nat->session, &route);
        switch( route ) {
        case ICR_ROUTE_TV:
            ret = CMU_ROUTINE_TV;
            break;
        case ICR_ROUTE_LCD1:
            ret = CMU_ROUTINE_LCD1;
            break;
        case ICR_ROUTE_LCD2:
            ret = CMU_ROUTINE_LCD2;
            break;
        default:
            break;
        }
    }    
#endif
    return ret;
}


static int setColorParamNative(JNIEnv* env, jobject object, jint arg1, jint arg2, jint arg3) {
    LOGV(__FUNCTION__);
    int ret = 0;
#ifdef HAVE_CMU
    native_data_t *nat =
        (native_data_t *)env->GetIntField(object, field_mNativeData);
    IcrChannel channel = (arg1 == CMU_CHANNEL_MAIN) ? ICR_MAIN_CHANNEL : ICR_PIP_CHANNEL;    
    if (nat && nat->session) {
        switch( arg2 ) {
        case CMU_PARAM_CONTRAST:
            ret = IcrContrastSet( nat->session, channel, arg3 );
            break;
        case CMU_PARAM_BRIGHTNESS:
            ret = IcrBrightnessSet( nat->session, channel, arg3 );

            break;
        case CMU_PARAM_SATURATION:
            ret = IcrSaturationSet( nat->session, channel, arg3 );
            break;
        case CMU_PARAM_HUE:
            ret = IcrHueSet( nat->session, channel, arg3 );
            break;
        case CMU_PARAM_TONE:
            ret = IcrToneSet( nat->session, channel, arg3 );
            break;
        case CMU_PARAM_BELEVEL:
            ret = IcrBeLevelSet( nat->session, channel, arg3 );
            break;
        case CMU_PARAM_ACEMODE:
            {
            IcrAceMode mode = ICR_ACE_MEDIUM;
            switch( arg3 ) {
            case CMU_ACE_OFF:
                mode = ICR_ACE_OFF;
                break;
            case CMU_ACE_LOW:
                mode = ICR_ACE_LOW;
                break;
            case CMU_ACE_MEDIUM:
                mode = ICR_ACE_MEDIUM;
                break;
            case CMU_ACE_HIGH:
                mode = ICR_ACE_HIGH;
                break;
            default:
                break;
            }
            ret = IcrAceModeSet( nat->session, channel, mode);
            }
            break;
        case CMU_PARAM_FLESHTONE:
            ret = IcrFleshToneEnable( nat->session, channel, arg3 );
            break;
        case CMU_PARAM_GAMUT:
            ret = IcrGamutCompressEnable( nat->session, channel, arg3 );
            break;
        default:
            break;
        }
    }    
#endif
    return ret;
}


static int getColorParamNative(JNIEnv* env, jobject object, jint arg1, jint arg2) {
    LOGV(__FUNCTION__);
    int ret = 0;
#ifdef HAVE_CMU
    native_data_t *nat =
        (native_data_t *)env->GetIntField(object, field_mNativeData);
    IcrChannel channel = (arg1 == CMU_CHANNEL_MAIN) ? ICR_MAIN_CHANNEL : ICR_PIP_CHANNEL;    
    if (nat && nat->session) {
        switch( arg2 ) {
        case CMU_PARAM_CONTRAST:
            IcrContrastGet( nat->session, channel, &ret);
            break;
        case CMU_PARAM_BRIGHTNESS:
            IcrBrightnessGet( nat->session, channel, &ret );
            break;
        case CMU_PARAM_SATURATION:
            IcrSaturationGet( nat->session, channel, &ret );
            break;
        case CMU_PARAM_HUE:
            IcrHueGet( nat->session, channel, &ret );
            break;
        case CMU_PARAM_TONE:
            IcrToneGet( nat->session, channel, &ret );
            break;
        case CMU_PARAM_BELEVEL:
            IcrBeLevelGet( nat->session, channel, &ret );
            break;
        case CMU_PARAM_ACEMODE:
            {
            IcrAceMode mode = ICR_ACE_MEDIUM;
            IcrAceModeGet( nat->session, channel, &mode);
            switch( mode ) {
            case ICR_ACE_OFF:
                ret = CMU_ACE_OFF;
                break;
            case ICR_ACE_LOW:
                ret = CMU_ACE_LOW;
                break;
            case CMU_ACE_MEDIUM:
                ret = CMU_ACE_MEDIUM;
                break;
            case ICR_ACE_HIGH:
                ret = CMU_ACE_HIGH;
                break;
            default:
                break;
            }
            }
            break;
        case CMU_PARAM_FLESHTONE:
            IcrFleshToneGet( nat->session, channel, &ret );
            break;
        case CMU_PARAM_GAMUT:
            ret = IcrGamutCompressGet( nat->session, channel, &ret );
            break;
        default:
            break;
        }
    }    
    
#endif
    return ret;

}

static jobjectArray getViewModeListNative(JNIEnv *env, jobject jobj) {
    jobjectArray result = NULL;
#ifdef HAVE_CMU
    jclass strCls = env->FindClass("java/lang/String");
    if (strCls == NULL) {
        return NULL;
    }
    int count = sizeof( ViewModeName ) / sizeof(char*);
    result = env->NewObjectArray(count, strCls, NULL);
    for (unsigned int i = 0; i < count; i++) {
        jstring viewmode = env->NewStringUTF(ViewModeName[i]);
        env->SetObjectArrayElement(result, i, viewmode);
        env->DeleteLocalRef(viewmode);
    }
#endif
    return result;
}

static jintArray getViewModeIdListNative(JNIEnv *env, jobject jobj) {
#ifdef HAVE_CMU
    jintArray result;
    result = env->NewIntArray(sizeof(VideoModeId)/sizeof(int));
    if (result == NULL) {
        return NULL; /* out of memory error thrown */
    }
    env->SetIntArrayRegion(result, 0, sizeof(VideoModeId)/sizeof(int), VideoModeId);
    return result;
#else
    return NULL;
#endif
}

static int setViewModeNative(JNIEnv* env, jobject object, jint arg1, jint arg2) {
    LOGV(__FUNCTION__);
    int ret = 0;
#ifdef HAVE_CMU
    IcrChannel channel = (arg1 == CMU_CHANNEL_MAIN) ? ICR_MAIN_CHANNEL : ICR_PIP_CHANNEL;    
    native_data_t *nat =
        (native_data_t *)env->GetIntField(object, field_mNativeData);
    if (nat && nat->session) {
        ret = IcrViewModeSet( nat->session, channel, arg2 );
    }    
#endif
    return ret;
}

static int getViewModeNative(JNIEnv* env, jobject object, jint arg1) {
    LOGV(__FUNCTION__);
    int ret = 0;
#ifdef HAVE_CMU
    IcrChannel channel = (arg1 == CMU_CHANNEL_MAIN) ? ICR_MAIN_CHANNEL : ICR_PIP_CHANNEL;    
    native_data_t *nat =
        (native_data_t *)env->GetIntField(object, field_mNativeData);
    if (nat && nat->session) {
        IcrViewModeGet( nat->session, channel, &ret );
    }    
#endif
    return ret;
}

static JNINativeMethod method_table[] = {
     /* name, signature, funcPtr */
    {"classInitNative", "()V", (void*)classInitNative},
    {"initDataNative", "()Z", (void *)initDataNative},
    {"cleanupDataNative", "()V", (void *)cleanupDataNative},
    {"openNative", "()I", (void*)openNative},
    {"closeNative", "()I", (void*)closeNative},
    {"enableCMUNative", "(Z)I", (void*)enableCMUNative},
    {"setRoutineNative", "(I)I", (void*)setRoutineNative},
    {"getRoutineNative", "()I", (void*)getRoutineNative},
    {"setColorParamNative", "(III)I", (void*)setColorParamNative},
    {"getColorParamNative", "(II)I", (void*)getColorParamNative},
    {"getViewModeListNative", "()[Ljava/lang/String;", (void*)getViewModeListNative},
    {"getViewModeIdListNative", "()[I", (void*)getViewModeIdListNative},
    {"setViewModeNative", "(II)I", (void*)setViewModeNative},
    {"getViewModeNative", "(I)I", (void*)getViewModeNative},        
};

int register_android_server_ColorControlService(JNIEnv *env)
{
    return jniRegisterNativeMethods(env, "com/android/server/ColorControlService",
            method_table, NELEM(method_table));
}

};
