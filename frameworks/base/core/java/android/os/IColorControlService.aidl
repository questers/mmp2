
package android.os;

/** WARNING! Update IColorControlService.h and IColorControlService.cpp if you change this file.
 * In particular, the ordering of the methods below must match the 
 * _TRANSACTION enum in IColorControlService.cpp
 * @hide
 */

interface IColorControlService
{
    /**
     * Acquire ColorControlService
     */
    int acquireService(int type);

    /**
     * Release ColorControlService
     */
    void releaseService();

    /**
     * Release ColorControlService
     */
    int enableCMU(IBinder id, boolean enable);
  
    /**
     * Set CMU Routine
     */
    int setCMURoutine( int routine );

    /**
     * Get CMU Routine
     */
    int getCMURoutine();

    /**
     * Set PIP rectangle
     */
    //int setPIPRect(Rect pip);

    /**
     * Get PIP rectangle
     */
    //Rect getPIPRect();
   
    /**
     * Set cmu parameter
     */
    int setColorParameter(int channel, int param, int value);

    /**
     * Get cmu parameter
     */
    int getColorParameter(int channel, int param);

    /**
     * Get predefine view mode list
     */
    String[] getViewModeList();
    
    /**
     * Get predefine view mode id list
     */ 
    int[] getViewModeIdList();

    /**
     * Set predefine view mode
     */
    int setViewMode(int channel, int id); 

    /**
     * Get view mode
     */
    int getViewMode(int channel); 

}
