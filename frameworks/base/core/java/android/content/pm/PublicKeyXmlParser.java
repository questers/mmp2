package android.content.pm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import android.util.Log;
import android.util.Xml;

public class PublicKeyXmlParser {
	
	private static final String TAG = "PublicKeyXmlParser";
	// names of the XML tags
	private static final String PUB_KEY = "publicKey";
	private static final String ITEM = "item";
	
	private final File publicKeyFile;
	
	public PublicKeyXmlParser(String XmlPath) {
		publicKeyFile = new File(XmlPath);
		if (!publicKeyFile.exists()) {
			try{
				publicKeyFile.createNewFile();
				String command = "chmod 666 " + publicKeyFile.getAbsolutePath();
				Runtime runtime = Runtime.getRuntime();
				runtime.exec(command);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
		}
	}

	public File getFile() {
		return this.publicKeyFile;
	}
	
	private InputStream getInputStream() {
		InputStream ism = null;
		try {
			ism = new FileInputStream(publicKeyFile);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		return ism;
	}
	
	public List<String> parse() {
		List<String> messages = null;
		XmlPullParser parser = Xml.newPullParser();
		InputStream ism = this.getInputStream();
		try {
			// auto-detect the encoding from the stream
			parser.setInput(ism, null);
			int eventType = parser.getEventType();
			String publicKey = null;
			boolean done =false;
			while (eventType != XmlPullParser.END_DOCUMENT && !done) {
				String name = null;
				switch (eventType) {
					case XmlPullParser.START_DOCUMENT:
						messages = new ArrayList<String>();
						break;
					case XmlPullParser.START_TAG:
						name = parser.getName();
						if (name.equalsIgnoreCase(PUB_KEY)) {
							Log.d(TAG, "Public key began to parse..");
						}
						else if (name.equalsIgnoreCase(ITEM)) {
							publicKey = parser.nextText();
						}
						break;
					case XmlPullParser.END_TAG:
						name = parser.getName();
						if (name.equalsIgnoreCase(ITEM) && publicKey != null) {
							messages.add(publicKey);
						}
						else if (name.equalsIgnoreCase(PUB_KEY)) {
							Log.d(TAG, "Public key parses has been done..");
							done = true;
						}
						break;
				}
				eventType = parser.next();
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (ism != null) {
				try {
					ism.close();
				} catch (Exception e) {
					Log.e(TAG, e.getMessage());
				}
			}
		}
		return messages;
	}
	
	public boolean writePKXml(List<String> messages) {
		XmlSerializer serializer = Xml.newSerializer();
		OutputStreamWriter writer = null;
		try {
			writer = new OutputStreamWriter(
					new FileOutputStream(publicKeyFile));
			serializer.setOutput(writer);
			serializer.startDocument("UTF-8", true);
			serializer.startTag("", PUB_KEY);
			for (String publicKey : messages) {
				serializer.startTag("", ITEM);
				serializer.text(publicKey);
				serializer.endTag("", ITEM);
			}
			serializer.endTag("", PUB_KEY);
			serializer.endDocument();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return false;
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch(Exception e) {
					Log.e(TAG, e.getMessage());
				}
			}
		}
		return true;
	}
	
	public boolean insertPK2Xml(List<String> pkmsgs) {
		List<String> messages = this.parse();
		int initial = messages.size();
		boolean isEquals = false;
		for (String str1 : pkmsgs) {
			for (String str2 : messages) {
				if (str1.equals(str2)) {
					isEquals = true;
				}
			}
			if (!isEquals) {
				messages.add(str1);
			}
			isEquals = false;
		}
		if (messages.size() == initial) {
			return false;
		}
		return this.writePKXml(messages);
	}

}
