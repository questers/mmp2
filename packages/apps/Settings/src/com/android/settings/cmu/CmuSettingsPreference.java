package com.android.settings.cmu;

import android.content.Context;
import java.util.ArrayList;
import android.os.Bundle;
import android.os.IColorControlService;
import android.os.ServiceManager;
import android.os.RemoteException;
//import android.preference.SeekBarPreference;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.util.AttributeSet;
import android.util.Log;
import com.android.settings.cmu.ColorControl;
import com.android.settings.R;
import android.preference.PreferenceActivity;
import android.preference.Preference;
import android.preference.ListPreference;
import android.preference.PreferenceScreen;
import com.android.settings.SeekBarPreference;


public class CmuSettingsPreference extends PreferenceActivity implements
Preference.OnPreferenceChangeListener {

    private ListPreference mViewMode;

    private static final String TAG = CmuSettingsPreference.class.getSimpleName();
    private static final int DIALOG_CHOOSE_BG = 0;
    private static final int DIALOG_CHOOSE_MODE = 1;
    private static final int DIALOG_CHANGE_FLESH_TONE = 2;
    private static final int DIALOG_CHANGE_GAMUT_COMPRESS = 3;
    private static final int DIALOG_CHANGE_ACE_MODE = 4;

    private static final int HUE_CONVERT = 2;
    private static final int TONE_CONVERT = 50;
    private int mViewModeIndex = 0;
    private int mEnableFleshTone = 0;
    private int mEnableGamutCompress = 0;
    private int mAceMode = 0;
    private String mAceModeList[] = {"off", "low", "medium", "high"};
    private int mAceModeIdList[] = {ColorControl.COLOR_CONTROL_ACE_OFF, ColorControl.COLOR_CONTROL_ACE_LOW,
        ColorControl.COLOR_CONTROL_ACE_MEDIUM, ColorControl.COLOR_CONTROL_ACE_HIGH}; 

    private IColorControlService mService;


    ArrayList<String> mViewModeList = new ArrayList<String>();
    ArrayList<Integer> mViewModeIdList = new ArrayList<Integer>();

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.cmu_settings);
        mService = IColorControlService.Stub.asInterface(ServiceManager.getService("colorcontrol"));

        mViewMode= (ListPreference) findPreference("viewmode");
        mViewMode.setOnPreferenceChangeListener(this);

        try{
            int ret = mService.acquireService(0);
            if( ret == 0 ) {
                return;
            }

            String vm[] = mService.getViewModeList();
            int vmid[] = mService.getViewModeIdList();
            if (vm.length != 0 && vmid.length != 0) {
                CharSequence[] entries = new CharSequence[vm.length+1];
                CharSequence[] values = new CharSequence[vmid.length+1];

                entries[0] = "standard";
                values[0] = "0";
                
                if( vm != null ) {
                    int i=1;
                    for(String s : vm) {
                        Log.i(TAG, "i = "+i+", s = "+s);
                        entries[i++] = s;
                    }
                }

                if( vmid != null ) {
                    int i=1;
                    for(int id : vmid ) {
                        Log.i(TAG, "i = "+i+", id = "+id);
                        values[i++] = String.format("%d", id);
                    }
                }
                mViewMode.setEntries(entries);
                mViewMode.setEntryValues(values);
            }
            else
            {
                String s = String.format("View mode list number: %d/%d", vm.length, vmid.length);
                Log.e(TAG, s);
            }
        } catch (Exception e ) {
            Log.e(TAG, "Catch "+e);
            e.printStackTrace();
        }

        int value = -1;
        try {
            mService.acquireService(0);
            mService.enableCMU(null, true);
            value = mService.getViewMode(ColorControl.COLOR_CONTROL_CHANNEL_MAIN);
            mService.releaseService();
        } catch (Exception e) {
            Log.e(TAG, "Caught "+e);
        }

        if (value != -1)
        {
            mViewMode.setValue(""+value);
            mViewMode.setSummary(mViewMode.getEntry()!=null?mViewMode.getEntry():"unknown");
        }
        Log.i(TAG, "view mode set to "+value);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        return true;
    }

    public boolean onPreferenceChange(Preference preference, Object objValue) {
        if (preference.getKey().equals("viewmode"))
        {
            int value = Integer.parseInt((String)objValue);
            try {
                mService.acquireService(0);
                mService.setViewMode(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, value);
                mService.releaseService();
            } catch (Exception e) {
                Log.i(TAG, "Caught "+e);
            }

            ListPreference pref = (ListPreference)preference;
            int index = pref.findIndexOfValue((String)objValue);
            if (index != -1) {
                pref.setSummary(pref.getEntries()[index]);
            }
            else {
                pref.setSummary("unknown");
            }
            //preference.setSummary(pref.getEntry()!=null?pref.getEntry():"unknown");

            ((SeekBarPreference)findPreference("color_contrast")).update();
            ((SeekBarPreference)findPreference("color_saturation")).update();
            ((SeekBarPreference)findPreference("color_hue")).update();

            Log.i(TAG, "view mode set to: "+value);
        }

        return true;
    }

    private void updateState(boolean force) {
        try{
            int ret = mService.acquireService(0);
            if( ret == 0 ) {
                return;
            }
            /*
               String vm[] = mService.getViewModeList();
               if( vm != null ) {
               for(String s : vm) {
               mViewModeList.add(s);
               }
               }
               int vmid[] = mService.getViewModeIdList();
               if( vmid != null ) {
               for(int id : vmid ) {
               mViewModeIdList.add(id);
               }
               }*/
            int contrast = mService.getColorParameter(ColorControl.COLOR_CONTROL_CHANNEL_MAIN,
                    ColorControl.COLOR_CONTROL_PARAM_CONTRAST) + 127;
            int brightness = mService.getColorParameter(ColorControl.COLOR_CONTROL_CHANNEL_MAIN,
                    ColorControl.COLOR_CONTROL_PARAM_BRIGHTNESS) + 127;
            int saturation = mService.getColorParameter(ColorControl.COLOR_CONTROL_CHANNEL_MAIN,
                    ColorControl.COLOR_CONTROL_PARAM_SATURATION) + 127;
            int hue = mService.getColorParameter(ColorControl.COLOR_CONTROL_CHANNEL_MAIN,
                    ColorControl.COLOR_CONTROL_PARAM_HUE) / HUE_CONVERT;
            int tone = (mService.getColorParameter(ColorControl.COLOR_CONTROL_CHANNEL_MAIN,
                        ColorControl.COLOR_CONTROL_PARAM_TONE) -ColorControl.COLOR_CONTROL_TONE_MIN) / TONE_CONVERT;
            /*mSeekBarBrightness.setProgress(brightness);
              mSeekBarContrast.setProgress(contrast);
              mSeekBarSaturation.setProgress(saturation);
              mSeekBarHue.setProgress(hue);
              mSeekBarTone.setProgress(tone);
              mService.releaseService();*/
        } catch (RemoteException e ) {
            Log.e(TAG, "Catch remote exception");
        }	

    }	

}
