package com.android.settings;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.os.IColorControlService;
import android.os.ServiceManager;
import com.android.settings.cmu.ColorControl;

public class SeekBarPreference extends Preference {
    private static final String TAG = SeekBarPreference.class.getSimpleName();
    public SeekBar mSeekBar;
    private int mMin;
    private int mMax;

    private int mParam;

    private IColorControlService mService;
    
    private OnSeekBarChangeListener mChangeListener = new OnSeekBarChangeListener() {
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            try {
                int ret = mService.acquireService(0);
                ret = mService.setColorParameter(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, mParam, progress+mMin);
                mService.releaseService();

                String s = String.format("%s, %d", getKey(), progress+mMin);
                Log.i(TAG, s);
            } catch (Exception e) {
                Log.i(TAG, "Caught "+e);
            }
        }

        public void onStartTrackingTouch(SeekBar seekBar) {}
        public void onStopTrackingTouch(SeekBar seekBar) {}
    };
    
    public SeekBarPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SeekBarPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setLayoutResource(R.layout.preference_seekbar);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.SeekbarPreference, defStyle, 0);
        mMin = a.getInt(R.styleable.SeekbarPreference_min, 0);
        mMax = a.getInt(R.styleable.SeekbarPreference_max, 0);
        String s = String.format("key: %s, min: %d, max: %d", getKey(), mMin, mMax);
        Log.i(TAG, s);

        if (getKey().equals("color_contrast"))
            mParam = ColorControl.COLOR_CONTROL_PARAM_CONTRAST;
        else if (getKey().equals("color_saturation"))
            mParam = ColorControl.COLOR_CONTROL_PARAM_SATURATION;
        else if (getKey().equals("color_hue"))
            mParam = ColorControl.COLOR_CONTROL_PARAM_HUE;
        else
            mParam = -1;

        mService = IColorControlService.Stub.asInterface(ServiceManager.getService("colorcontrol"));

        try {
            int ret = mService.acquireService(0);
            mService.enableCMU(null, true);
            mService.releaseService();
        } catch (Exception e) {
            Log.i(TAG, "Caught "+e);
        }
    }

    @Override
    public void onBindView(View view) {
        super.onBindView(view);
        Log.d(TAG, "onBindView");
        mSeekBar = (SeekBar) view.findViewById(R.id.bar);
        if (mSeekBar != null)
        {
            mSeekBar.setMax(mMax-mMin);
            mSeekBar.setOnSeekBarChangeListener(mChangeListener);

            update();
        }
    }

    public void update() {
        int value;
        try {
            int ret = mService.acquireService(0);
            value = mService.getColorParameter(ColorControl.COLOR_CONTROL_CHANNEL_MAIN, mParam);
            mService.releaseService();
        } catch (Exception e) {
            Log.e(TAG, "Caught "+e);
            return;
        }
        
        value -= mMin;
        mSeekBar.setProgress(value);
        String s = String.format("%s: %d", getKey(), value);
        Log.i(TAG, s);
    }
}
