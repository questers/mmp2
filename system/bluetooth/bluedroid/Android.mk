#
# libbluedroid
#

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/include \
	system/bluetooth/bluez-clean-headers

LOCAL_SHARED_LIBRARIES := \
	libcutils

ifeq ($(BOARD_ENABLE_MARVELL_BLUETOOTH),true)
LOCAL_SRC_FILES += marvell_bluetooth.c
LOCAL_SHARED_LIBRARIES += libMarvellWireless
LOCAL_C_INCLUDES += device/generic/components/libMarvellWireless
else
LOCAL_SRC_FILES += bluetooth.c
endif

LOCAL_MODULE := libbluedroid

include $(BUILD_SHARED_LIBRARY)
