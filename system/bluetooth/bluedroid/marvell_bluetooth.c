/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "Marvell BT"
#include <cutils/log.h>
#include "marvell_wireless.h"

static int bt_on = 0;
int bt_enable() {
    LOGV(__FUNCTION__);

    int ret = -1;
    ret = bluetooth_enable();
out:
    if (ret){
        bluetooth_disable();
	    bt_on = 0;
    }else{
        bt_on = 1;
    }
    return ret;
}

int bt_disable() {
    LOGV(__FUNCTION__);

    int ret = -1;
    ret = bluetooth_disable();
    if(ret == 0)bt_on = 0;
out:
    return ret;
}

int bt_is_enabled() {
    return bt_on;
}
