#!/system/bin/sh
# This script can be used to quickly switch on usb suspend
# power saving mode on the Option modem in sysfs
#
# checking for root
#USERID=`id -u`
#MODE="auto"
#TIME="10"
#if [ "$USERID" != "0" ]
#then
#    /system/bin/log -t radio "Need root permissions to run this script"
#    exit
#fi

/system/bin/log -t radio "set radio suspend enable" 
echo "auto" >`find /sys -name idVendor | xargs -n 1 grep -H 19d2 | sed  "s/idVendor:19d2/power\/control/"`
echo "5" >`find /sys -name idVendor | xargs -n 1 grep -H 19d2 | sed  "s/idVendor:19d2/power\/autosuspend/"`


