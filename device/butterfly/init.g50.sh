#!/system/bin/sh

#echo disable_pm_suspend > /sys/power/wake_lock

/system/xbin/insmod /system/lib/modules/camera_gc2015.ko fm_low=3 fm_high=0
/system/xbin/insmod /system/lib/modules/camera_hm2055.ko fm_low=2 fm_high=2

#
#install all prebuilt apk WARNING :this must be the last step
#
while [ "`getprop dev.bootcomplete`" != 1 ]
do
    sleep 2
done
log -t provision "boot completed"

echo 1 > /sys/power/mspm/mspm 
echo 1 > /sys/power/mspm/prof 

#if [ "`getprop persist.prebuilt.install`" != '1' ] 
#then
#find /vendor/prebuilt -name '*.apk' -type f |
#	while read file   
#    do
#        log -t provision "install package $file"
#        pm install $file 
#    done
#setprop persist.prebuilt.install 1
#log -t provision "prebuilt packages process done"
#else
#log -t provision "prebuilt packages installed"
#fi


