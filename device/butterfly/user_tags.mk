
include device/$(TARGET_PRODUCT)/gst_modules.mk

# graphics modules
GRANDFATHERED_USER_MODULES += \
	libGAL \
	libGLESv1_CM_MRVL \
	libGLESv2_MRVL \
	libEGL_MRVL \
	libgcu \
	librs_jni \
	libRS

# multimedia modules except gst, include IPP, bmm/pmem, opencore...
GRANDFATHERED_USER_MODULES += \
	libippexif \
	libavutil \
	libavcodec \
	libavformat \
	libstagefright_hardware_renderer \
	libphycontmem \
	libpmemhelper \
	libbmm \
	libvmeta \

#Ipp codec library only for internal use
GRANDFATHERED_USER_MODULES += \
	libvpx_ipp \
	libcodecaacdec \
	libcodecaacenc \
	libcodecmp3dec \
	libcodech263dec \
	libcodech264dec \
	libcodecmpeg4dec \
	libcodecmpeg2dec \
	libcodecwmvdec \
	libcodecamrnbdec \
	libcodecamrnbenc \
	libcodecamrwbdec \
	libcodecamrwbenc \
	libcodecg711 \
	libcodecjpegdec \
	libcodecjpegenc \
	libcodecmpeg4enc \
	libcodech264enc \
	libcodech263enc \
	libvmetahal \
	libcodecwmadec \
	libcodecvmetaenc \
	libcodecvmetadec \
	libippcam \
	libippsp \
	libippvp \
	lib_il_basecore_wmmx2lnx \
	lib_il_ippomxmem_wmmx2lnx \
	lib_il_aacdec_wmmx2lnx \
	lib_il_aacenc_wmmx2lnx \
	lib_il_mp3dec_wmmx2lnx \
	lib_il_wmadec_wmmx2lnx \
	lib_il_vmetadec_wmmx2lnx \
	lib_il_vmetaenc_wmmx2lnx \
	lib_il_h264dec_wmmx2lnx \
	lib_il_h264enc_wmmx2lnx \
	lib_il_h263dec_wmmx2lnx \
	lib_il_h263enc_wmmx2lnx \
	lib_il_mpeg4aspdec_wmmx2lnx \
	lib_il_mpeg4enc_wmmx2lnx \
	lib_il_wmvdec_wmmx2lnx \
	lib_il_amrnbdec_wmmx2lnx \
	lib_il_amrnbenc_wmmx2lnx \
	lib_il_amrwbdec_wmmx2lnx \
	lib_il_amrwbenc_wmmx2lnx \
	lib_il_drmplayer_wmmx2lnx 
	
#Ipp codec library only for internal use
GRANDFATHERED_USER_MODULES += \
	agdb.py

