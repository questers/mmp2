DEVICE_PACKAGE_OVERLAYS := device/phoenix/overlay

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)

# stuff common to all questers devices
#$(call inherit-product, device/questers/common/common.mk)



PRODUCT_PROPERTY_OVERRIDES :=

$(call inherit-product, $(SRC_TARGET_DIR)/product/generic.mk)

#	TestServer1\\

PRODUCT_PACKAGES += \
	SoundRecorder \
	LiveWallpapersPicker \
	LiveWallpapers \
	MagicSmokeWallpapers \
	VisualizationWallpapers \
	MarvellWirelessDaemon \
	rfkill \
	zsp_data.bin \
	zsp_ints.bin \
	zsp_text.bin \
	librecovery_ui_brownstone \
	librecovery_updater_mrvl \
	libext4_utils \
	make_ext4fs \
	setup_fs \
	emmcexpose

#CmdLine command files disappeared?
#	CmdLine 
#	FMRadio 
#	FileManager //replaced with es_file_explorer  
PRODUCT_PACKAGES+= \
	ClockSync \
	OtaUpdater \
	OpenTheDeviceStorage \
	OpenTheSdCard \
	OpenTheUsbMassStorage

PRODUCT_COPY_FILES+= \
    device/phoenix/init.g50.sh:system/etc/init.g50.sh \
    device/phoenix/initlogo.rle:custom/initlogo.rle \
    device/phoenix/touchkey.kl:system/usr/keylayout/touchkey.kl
include device/$(TARGET_PRODUCT)/optional_modules.mk


#
#hardware permissions
#
#multitouch distinct and jazzhand?
PRODUCT_COPY_FILES+= \
    frameworks/base/data/etc/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
    frameworks/base/data/etc/android.hardware.camera.flash.xml:system/etc/permissions/android.hardware.camera.flash.xml \
    frameworks/base/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
    frameworks/base/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml \
    frameworks/base/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
    frameworks/base/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    frameworks/base/data/etc/android.hardware.touchscreen.multitouch.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.xml	
PRODUCT_COPY_FILES+= \
    frameworks/base/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml	\
    frameworks/base/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml	\
    frameworks/base/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
    frameworks/base/data/etc/android.hardware.audio.low_latency.xml:system/etc/permissions/android.hardware.audio.low_latency.xml
    
#software permissions
PRODUCT_COPY_FILES+= \
    frameworks/base/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
    packages/wallpapers/LivePicker/android.software.live_wallpaper.xml:/system/etc/permissions/android.software.live_wallpaper.xml

# Get a full list of languages.
$(call inherit-product, build/target/product/languages_full.mk)

# Get the TTS language packs
$(call inherit-product-if-exists, external/svox/pico/lang/all_pico_languages.mk)

$(call inherit-product, device/generic/google/google_apps.mk)
$(call inherit-product, device/generic/thirdparty/thirdparty.mk)



#include the ringtones.
include frameworks/base/data/sounds/AudioPackage2.mk

# Overrides
PRODUCT_MANUFACTURER := Questers
PRODUCT_BRAND := Questers
PRODUCT_NAME := phoenix
PRODUCT_DEVICE := phoenix
PRODUCT_MODEL := Phoenix

#
#
#
BUILD_NUMBER:=builder.$(shell date +%Y%m%d.%H%M%S)


