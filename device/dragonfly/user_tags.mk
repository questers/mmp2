
include device/$(TARGET_PRODUCT)/gst_modules.mk

# graphics modules
GRANDFATHERED_USER_MODULES += \
	libGAL \
	libGLESv1_CM_MRVL \
	libGLESv2_MRVL \
	libEGL_MRVL \
	libgcu \
	librs_jni \
	libRS

# multimedia modules except gst, include IPP, bmm/pmem, opencore...
GRANDFATHERED_USER_MODULES += \
	libippexif \
	libavutil \
	libavcodec \
	libavformat \
	libstagefright_hardware_renderer \
	libphycontmem \
	libpmemhelper \
	libbmm \
	libvmeta \


