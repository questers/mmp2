#FM
PRODUCT_PACKAGES +=  \
	com.marvell.fmmanager \
	FMRadioServer

#VoiceCallService
PRODUCT_PACKAGES +=  \
	libvoicecall \
	VoiceCallServer 

#network
PRODUCT_PACKAGES +=  \
	libqst_ril \
	ip-up-vpn \
	ip-up-ppp0 \
	ip-down-ppp0 \
	pppd \
	chat 

#misc	
PRODUCT_PACKAGES +=  \
	libusb \
	gps.marvell \
	lights.marvell \
	overlay.marvell \
	sensors.marvell \
	amixer \
	librs_jni \
	envtool

#plugin
#PRODUCT_PACKAGES +=  \
#	com.adobe.flashplayer
