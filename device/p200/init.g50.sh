#!/system/bin/sh

#echo disable_pm_suspend > /sys/power/wake_lock

/system/xbin/insmod /system/lib/modules/camera_ov5640.ko fm_low=0 fm_high=1

#
#install all prebuilt apk WARNING :this must be the last step
#
while [ "`getprop dev.bootcomplete`" != 1 ]
do
    sleep 2
done
log -t provision "boot completed"

echo 1 > /sys/power/mspm/mspm 
echo 1 > /sys/power/mspm/prof 

if [ ! -e /data/sdcard/iengine.lic ]; then
	/system/xbin/mkdir -p /data/sdcard
	/system/xbin/chown system.system /data/sdcard
	/system/xbin/cp -f /custom/prebuilt/iengine.lic /data/sdcard/iengine.lic
	/system/xbin/chown system.system /data/sdcard/iengine.lic
fi

#if [ "`getprop persist.prebuilt.install`" != '1' ] 
#then
#find /vendor/prebuilt -name '*.apk' -type f |
#	while read file   
#    do
#        log -t provision "install package $file"
#        pm install $file 
#    done
#setprop persist.prebuilt.install 1
#log -t provision "prebuilt packages process done"
#else
#log -t provision "prebuilt packages installed"
#fi

w1_master=/sys/bus/w1/devices/w1_bus_master1
slave_count=0

cat $w1_master/w1_master_slave_count | while read slave_count
do
    echo "w1 slaves:$slave_count"
        if [ "$slave_count" -gt "0" ]
        then
            echo "disable auto search"
            echo 1 > $w1_master/w1_master_search
        fi                
done       

