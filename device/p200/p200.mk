DEVICE_PACKAGE_OVERLAYS := device/p200/overlay

#
#Basic Packages
#
#$(call inherit-product, $(SRC_TARGET_DIR)/product/generic.mk)
PRODUCT_PACKAGES := \
    AccountAndSyncSettings \
    DeskClock \
    AlarmProvider \
    Bluetooth \
    Calendar \
    Camera \
    CertInstaller \
    DrmProvider \
    Gallery3D \
    LatinIME \
    Launcher2 \
    Mms \
    Music \
    Provision \
    QuickSearchBox \
    Settings \
    Sync \
    SystemUI \
    Updater \
    CalendarProvider \
    Email \
    SyncProvider

$(call inherit-product, $(SRC_TARGET_DIR)/product/core.mk)

#
#Language packages
# Inherit from those products. Most specific first.
PRODUCT_LOCALES := zh_CN en_US



PRODUCT_PROPERTY_OVERRIDES :=


ifeq ($(strip $(BOARD_HAVE_HDMI_SERVICE)),true)
	PRODUCT_PROPERTY_OVERRIDES += board.service.hdmi=true 
	PRODUCT_PACKAGES += com.marvell.hdmimanager
endif

ifeq ($(strip $(BOARD_HAVE_VOICECALL_SERVICE)),true)
	PRODUCT_PROPERTY_OVERRIDES += board.service.voicecall=true 
endif

ifeq ($(strip $(BOARD_HAVE_FM_SERVICE)),true)
	PRODUCT_PROPERTY_OVERRIDES += board.service.fm=true 
	PRODUCT_PACKAGES += com.marvell.fmmanager
endif

ifeq ($(strip $(BOARD_HAVE_CMU)),true)
	PRODUCT_PROPERTY_OVERRIDES += board.service.cmu=true 
endif


#
#Extra Packages
#
PRODUCT_PACKAGES += \
	SoundRecorder \
	LiveWallpapersPicker \
	LiveWallpapers \
	MagicSmokeWallpapers \
	VisualizationWallpapers \
	MarvellWirelessDaemon \
	rfkill \
	zsp_data.bin \
	zsp_ints.bin \
	zsp_text.bin \
	librecovery_ui_brownstone \
	librecovery_updater_mrvl \
	libext4_utils \
	make_ext4fs \
	setup_fs \
	wpa_supplicant \
	emmcexpose \
	libusbcam \
	librfid \
	libmifare \
	Flashlight \
	ClockSync \
	OtaUpdater

PRODUCT_PACKAGES+=\
	su

include device/$(TARGET_PRODUCT)/optional_modules.mk


#
#Product Specified Files
#
PRODUCT_COPY_FILES+= \
    device/p200/vold.fstab:system/etc/vold.fstab \
    device/p200/init.g50.sh:system/etc/init.g50.sh \
    device/p200/init.modem.sh:system/etc/init.modem.sh \
    device/p200/touchkey.kl:system/usr/keylayout/touchkey.kl \
    device/p200/gpio-keys.kl:system/usr/keylayout/gpio-keys.kl 

#
#hardware permissions
#
#multitouch distinct and jazzhand?
PRODUCT_COPY_FILES+= \
    frameworks/base/data/etc/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
    frameworks/base/data/etc/android.hardware.camera.flash.xml:system/etc/permissions/android.hardware.camera.flash.xml \
    frameworks/base/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
    frameworks/base/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml \
    frameworks/base/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
    frameworks/base/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    frameworks/base/data/etc/android.hardware.touchscreen.multitouch.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.xml	
PRODUCT_COPY_FILES+= \
    frameworks/base/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml	\
    frameworks/base/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
    frameworks/base/data/etc/android.hardware.audio.low_latency.xml:system/etc/permissions/android.hardware.audio.low_latency.xml
    
#software permissions
PRODUCT_COPY_FILES+= \
    frameworks/base/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml  \
    packages/wallpapers/LivePicker/android.software.live_wallpaper.xml:/system/etc/permissions/android.software.live_wallpaper.xml


#
#Extra Binary Packages
#
PRODUCT_COPY_FILES += \
	device/generic/thirdparty/es_file_explorer_v1_6_2_3.apk:custom/prebuilt/es_file_explorer_v1_6_2_3.apk \
	device/generic/thirdparty/FingerprintService.apk:custom/prebuilt/FingerprintService.apk \
	device/generic/thirdparty/P200Demo.apk:custom/prebuilt/P200Demo.apk \
	device/generic/thirdparty/iengine.lic:custom/prebuilt/iengine.lic \
	device/generic/google/app/GooglePartnerSetup.apk:system/app/GooglePartnerSetup.apk \
	device/generic/google/app/GoogleServicesFramework.apk:system/app/GoogleServicesFramework.apk \
	device/generic/google/app/GoogleBackupTransport.apk:system/app/GoogleBackupTransport.apk \
	device/generic/google/app/GoogleCalendarSyncAdapter.apk:system/app/GoogleCalendarSyncAdapter.apk \
	device/generic/google/app/GoogleContactsSyncAdapter.apk:system/app/GoogleContactsSyncAdapter.apk \
	device/generic/google/app/OneTimeInitializer.apk:system/app/OneTimeInitializer.apk

PRODUCT_PROPERTY_OVERRIDES += \
	ro.url.legal=http://www.google.com/intl/%s/mobile/android/basic/phone-legal.html \
	ro.url.legal.android_privacy=http://www.google.com/intl/%s/mobile/android/basic/privacy.html \
	ro.com.google.clientidbase=android-google \
	ro.setupwizard.mode=DISABLED \


#include the ringtones.
include frameworks/base/data/sounds/AudioPackage2.mk

# Overrides
PRODUCT_MANUFACTURER := ZKTECO
PRODUCT_BRAND := ZKTECO
PRODUCT_NAME := p200
PRODUCT_DEVICE := p200
PRODUCT_MODEL := P200

#
#
#
BUILD_NUMBER:=builder.$(shell date +%Y%m%d.%H%M%S)


