#include <linux/input.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>

#include "recovery_ui.h"
#include "common.h"

char* MENU_HEADERS[] = { "V+/V- to move highlight;",
                         "Home to select.",
                         "",
                         NULL };

char* MENU_ITEMS[] = { "Reboot system now",
                       "Apply update package in SD card",
                       "Wipe data/factory reset",
                       "Wipe cache partition",
                       NULL };

int device_recovery_start() {
    return 0;
}

int device_toggle_display(volatile char* key_pressed, int key_code) {
    return key_code == KEY_HOME || key_code == KEY_F7;
}

int device_reboot_now(volatile char* key_pressed, int key_code) {
	return 0;
}

int device_handle_key(int key_code, int visible) {
    if (visible) {
        switch (key_code) {
            case KEY_DOWN:
            case KEY_VOLUMEDOWN:
            case KEY_BACK:
                return HIGHLIGHT_DOWN;

            case KEY_UP:
            case KEY_VOLUMEUP:
            case KEY_MENU:
                return HIGHLIGHT_UP;

            case KEY_ENTER:
            case KEY_SEARCH:
            case KEY_F6:
                return SELECT_ITEM;

            case KEY_F8:
                return ITEM_REBOOT;
        }
    }

    return NO_ACTION;
}

int device_perform_action(int which) {
    return which;
}

int device_wipe_data() {
    return 0;
}
