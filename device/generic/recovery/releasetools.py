"""Emit commands needed for Marvell devices during OTA installation
(installing uboot, obm and kernel image)."""

import common
import sha

def FullOTA_InstallEnd(info):
  info.script.UnmountAll()
  fstab = info.info_dict.get("fstab", None);

  try:
    kernel_img = info.input_zip.read("KERNEL/zImage")
  except KeyError:
    print "no zImage in update_files; skipping install"
  else:
    common.ZipWriteStr(info.output_zip, "zImage", kernel_img)
    info.script.Print("Writing kernel image...")
    info.script.ShowProgress(0.3, 2)
    info.script.AppendExtra('package_extract_file("zImage", "/cache/zImage");')
    info.script.AppendExtra('write_raw_image_emmc("%s", "/cache/zImage", 0);' %(fstab["/kernel"].device))
  try:
    if info.trusted_boot is True:
      uboot_img = info.input_zip.read("UBOOT/trusted/u-boot.bin")
    else:
      uboot_img = info.input_zip.read("UBOOT/nontrusted/u-boot.bin")
  except KeyError:
    print "no u-boot.bin in update_files; skipping install"
  else:
    common.ZipWriteStr(info.output_zip, "u-boot.bin", uboot_img)
    info.script.Print("Copying u-boot image...")
    info.script.ShowProgress(0.1, 1)
    info.script.AppendExtra('package_extract_file("u-boot.bin", "/cache/u-boot.bin");')
    info.script.AppendExtra('write_raw_image_emmc("%s", "/cache/u-boot.bin", 1536);' %(fstab["/misc"].device))

  info.script.Print("Writing bootloader message...")
  info.script.ShowProgress(0.1, 1)
  info.script.AppendExtra('mrvl_update_firmware("%s", "update-firmware");' %(fstab["/misc"].device))
  info.script.Print("Rebooting...")
  info.script.AppendExtra('reboot();')
