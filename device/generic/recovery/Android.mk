LOCAL_PATH := $(call my-dir)
RECOVERY_PATH := $(LOCAL_PATH)

include $(RECOVERY_PATH)/ui/Mdroid.mk
include $(CLEAR_VARS)

include $(RECOVERY_PATH)/updater/Mdroid.mk
include $(CLEAR_VARS)

# -----------------------------------------------------------------
# recovery ramdisk
MRVL_RECOVERY_IMAGE := $(PRODUCT_OUT)/ramdisk_recovery.img
recovery_initrc := $(call include-path-for, recovery)/etc/init.rc
recovery_build_prop := $(INSTALLED_BUILD_PROP_TARGET)
recovery_binary := $(call intermediates-dir-for,EXECUTABLES,recovery)/recovery
recovery_resources_common := $(call include-path-for, recovery)/res
recovery_resources_private := $(strip $(wildcard $(TARGET_DEVICE_DIR)/recovery/res))
recovery_command_private := $(strip $(wildcard $(TARGET_DEVICE_DIR)/recovery/sbin))
recovery_lib_private := $(strip $(wildcard $(TARGET_DEVICE_DIR)/recovery/lib))
recovery_resource_deps := $(shell find $(recovery_resources_common) \
  $(recovery_resources_private) -type f)
recovery_fstab := $(strip $(wildcard $(TARGET_DEVICE_DIR)/recovery/recovery.fstab))

ifeq ($(recovery_resources_private),)
  $(info No private recovery resources for TARGET_DEVICE $(TARGET_DEVICE))
endif

OTA_PUBLIC_KEYS := $(SRC_TARGET_DIR)/product/security/testkey.x509.pem

RECOVERY_INSTALL_OTA_KEYS := \
	$(call intermediates-dir-for,PACKAGING,ota_keys)/keys
DUMPKEY_JAR := $(HOST_OUT_JAVA_LIBRARIES)/dumpkey.jar
$(RECOVERY_INSTALL_OTA_KEYS): PRIVATE_OTA_PUBLIC_KEYS := $(OTA_PUBLIC_KEYS)
$(RECOVERY_INSTALL_OTA_KEYS): $(OTA_PUBLIC_KEYS) $(DUMPKEY_JAR)
	@echo "DumpPublicKey: $@ <= $(PRIVATE_OTA_PUBLIC_KEYS)"
	@rm -rf $@
	@mkdir -p $(dir $@)
	java -jar $(DUMPKEY_JAR) $(PRIVATE_OTA_PUBLIC_KEYS) > $@

$(MRVL_RECOVERY_IMAGE): $(MKBOOTFS) $(MINIGZIP) \
		$(INSTALLED_RAMDISK_TARGET) \
		$(recovery_binary) \
		$(recovery_initrc) \
		$(recovery_build_prop) $(recovery_resource_deps) \
		$(recovery_fstab) \
		$(RECOVERY_INSTALL_OTA_KEYS) \
		systemimage \
		$(INSTALLED_USERDATAIMAGE_TARGET) \
		$(INSTALLED_FILES_FILE)
	@echo ----- Making recovery ramdisk image ------
	rm -rf $(TARGET_RECOVERY_OUT)
	mkdir -p $(TARGET_RECOVERY_OUT)
	mkdir -p $(TARGET_RECOVERY_ROOT_OUT)
	mkdir -p $(TARGET_RECOVERY_ROOT_OUT)/etc
	mkdir -p $(TARGET_RECOVERY_ROOT_OUT)/tmp
	echo Copying baseline ramdisk...
	cp -R $(TARGET_ROOT_OUT) $(TARGET_RECOVERY_OUT)
	cp -rf $(TARGET_DEVICE_DIR)/recovery/init* $(TARGET_RECOVERY_ROOT_OUT)/
	echo Modifying ramdisk contents...
	cp -f $(recovery_initrc) $(TARGET_RECOVERY_ROOT_OUT)/
	cp -f $(recovery_binary) $(TARGET_RECOVERY_ROOT_OUT)/sbin/
	cp -rf $(recovery_resources_common) $(TARGET_RECOVERY_ROOT_OUT)/
	$(foreach item,$(recovery_lib_private), \
	  cp -rf $(item) $(TARGET_RECOVERY_ROOT_OUT)/)
	$(foreach item,$(recovery_resources_private), \
	  cp -rf $(item) $(TARGET_RECOVERY_ROOT_OUT)/)
	$(foreach item,$(recovery_command_private), \
	  cp -rf $(item) $(TARGET_RECOVERY_ROOT_OUT)/)
	$(foreach item,$(recovery_fstab), \
	  cp -f $(item) $(TARGET_RECOVERY_ROOT_OUT)/etc/recovery.fstab)
	cp $(RECOVERY_INSTALL_OTA_KEYS) $(TARGET_RECOVERY_ROOT_OUT)/res/keys
	cat $(INSTALLED_DEFAULT_PROP_TARGET) $(recovery_build_prop) \
	        > $(TARGET_RECOVERY_ROOT_OUT)/default.prop
	$(MKBOOTFS) $(TARGET_RECOVERY_ROOT_OUT) | $(MINIGZIP) > $(MRVL_RECOVERY_IMAGE)
	@echo ----- Made recovery ramdisk image -------- $@

.PHONY: mrvlrecoveryimg
mrvlrecoveryimg: $(MRVL_RECOVERY_IMAGE)

# removed. -guang
#droidcore: $(MRVL_RECOVERY_IMAGE)

# -----------------------------------------------------------------
# a zip of the directories including update files
name := $(TARGET_PRODUCT)
name := $(name)-update_files

intermediates := $(call intermediates-dir-for,PACKAGING,update_files)
BUILT_UPDATE_FILES_PACKAGE := $(intermediates)/$(name).zip
$(BUILT_UPDATE_FILES_PACKAGE): intermediates := $(intermediates)
$(BUILT_UPDATE_FILES_PACKAGE): \
		zip_root := $(intermediates)/$(name)

built_ota_tools := \
	$(call intermediates-dir-for,EXECUTABLES,applypatch)/applypatch \
	$(call intermediates-dir-for,EXECUTABLES,applypatch_static)/applypatch_static \
	$(call intermediates-dir-for,EXECUTABLES,check_prereq)/check_prereq \
	$(call intermediates-dir-for,EXECUTABLES,updater)/updater
$(BUILT_UPDATE_FILES_PACKAGE): PRIVATE_OTA_TOOLS := $(built_ota_tools)
$(BUILT_UPDATE_FILES_PACKAGE): tool_extensions := $(TARGET_DEVICE_DIR)/recovery

MRVL_UBOOT_TRUST_IMAGE := $(TOPDIR)boot/out/trusted/u-boot.bin
MRVL_UBOOT_NONTRUST_IMAGE := $(TOPDIR)boot/out/nontrusted/u-boot.bin

PRODUCT_PLATFORM := $(basename $(subst _,.,$(TARGET_PRODUCT)))


#MRVL_KERNEL_IMAGE := $(TOPDIR)kernel/out/zImage
MRVL_KERNEL_IMAGE := $(TOPDIR)vendor/questers/$(PRODUCT_PLATFORM)/zImage
MRVL_RAMDISK_IMAGE := $(PRODUCT_OUT)/ramdisk.img
MRVL_RECOVERY_API_VERSION := 3
$(BUILT_UPDATE_FILES_PACKAGE): \
		$(INSTALLED_SYSTEMIMAGE) \
		$(INSTALLED_USERDATAIMAGE_TARGET) \
		$(built_ota_tools) \
		$(HOST_OUT_EXECUTABLES)/fs_config \
		$(MRVL_RAMDISK_IMAGE)\
		$(MRVL_KERNEL_IMAGE)\
		$(MRVL_UBOOT_TRUST_IMAGE)\
		$(MRVL_UBOOT_NONTRUST_IMAGE)\
		$(MRVL_RECOVERY_IMAGE)\
		| $(ACP)
	@echo "Package target files: $@"
	$(hide) rm -rf $@ $(zip_root)
	$(hide) mkdir -p $(dir $@) $(zip_root)
	@# Contents of the uboot
	$(hide) mkdir -p $(zip_root)/UBOOT/trusted
	$(hide) mkdir -p $(zip_root)/UBOOT/nontrusted
	$(hide) $(ACP) $(MRVL_UBOOT_TRUST_IMAGE) $(zip_root)/UBOOT/trusted
	$(hide) $(ACP) $(MRVL_UBOOT_NONTRUST_IMAGE) $(zip_root)/UBOOT/nontrusted
	@# Contents of the kernel
	$(hide) mkdir -p $(zip_root)/KERNEL
	$(hide) $(ACP) $(MRVL_KERNEL_IMAGE) $(zip_root)/KERNEL
	@# Contents of the ramdisk
	$(hide) mkdir -p $(zip_root)/RAMDISK
	$(hide) $(ACP) $(MRVL_RAMDISK_IMAGE) $(zip_root)/RAMDISK
	@# Components of the recovery image
	$(hide) mkdir -p $(zip_root)/RECOVERY
	$(hide) $(call package_files-copy-root, \
	        $(TARGET_RECOVERY_ROOT_OUT),$(zip_root)/RECOVERY/RAMDISK)
	@# Contents of the system image
	$(hide) $(call package_files-copy-root, \
		$(SYSTEMIMAGE_SOURCE_DIR),$(zip_root)/SYSTEM)
	@# Contents of the data image
	$(hide) $(call package_files-copy-root, \
		$(TARGET_OUT_DATA),$(zip_root)/DATA)
	@# Extra contents of the OTA package
	$(hide) mkdir -p $(zip_root)/OTA/bin
	$(hide) $(ACP) $(PRIVATE_OTA_TOOLS) $(zip_root)/OTA/bin/
	@# Files that do not end up in any images, but are necessary to build them
	$(hide) mkdir -p $(zip_root)/META
	$(hide) echo "recovery_api_version=$(MRVL_RECOVERY_API_VERSION)" > $(zip_root)/META/misc_info.txt
	$(hide) echo "tool_extensions=$(tool_extensions)" >> $(zip_root)/META/misc_info.txt

	@# Zip everything up, preserving symlinks
	$(hide) (cd $(zip_root) && zip -qry ../$(notdir $@) .)
	@# Run fs_config on all the system files in the zip, and save the output
	$(hide) zipinfo -1 $@ | awk -F/ 'BEGIN { OFS="/" } /^SYSTEM\// {$$1 = "system"; print}' | $(HOST_OUT_EXECUTABLES)/fs_config > $(zip_root)/META/filesystem_config.txt
	$(hide) (cd $(zip_root) && zip -q ../$(notdir $@) META/filesystem_config.txt)

# -----------------------------------------------------------------
# OTA update package
DEFAULT_KEY_CERT_PAIR := $(SRC_TARGET_DIR)/product/security/$(PRODUCT_PLATFORM)

TRUSTED_UPDATE := $(PRODUCT_OUT)/update_trusted.zip
NONTRUSTED_UPDATE := $(PRODUCT_OUT)/update_nontrusted.zip
MRVL_DROID_UPDATE := $(TRUSTED_UPDATE) $(NONTRUSTED_UPDATE)
$(MRVL_DROID_UPDATE): DROID_KEY_CERT_PAIR := $(DEFAULT_KEY_CERT_PAIR)
$(MRVL_DROID_UPDATE): $(BUILT_UPDATE_FILES_PACKAGE) $(OTATOOLS)
	@echo "Marvell droid update package: $@"
	$(hide) ./build/tools/releasetools/ota_from_target_files -t -v -p $(HOST_OUT) -k $(DROID_KEY_CERT_PAIR) $(BUILT_UPDATE_FILES_PACKAGE) $(TRUSTED_UPDATE)
	$(hide) ./build/tools/releasetools/ota_from_target_files -v -p $(HOST_OUT) -k $(DROID_KEY_CERT_PAIR) $(BUILT_UPDATE_FILES_PACKAGE) $(NONTRUSTED_UPDATE)

.PHONY: droidupdate
droidupdate: $(MRVL_DROID_UPDATE)

# removed, -guang
#droidcore: $(MRVL_DROID_UPDATE)
