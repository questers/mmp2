LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

#Targeting Root Directory
ifeq ($(TARGET_PROVIDES_ROOTDIR),true)
DIRS := $(addprefix $(TARGET_ROOT_OUT)/, \
		sbin \
		dev \
		proc \
		sys \
		system \
		data \
		custom \
		cache \
	) \
	$(TARGET_OUT_DATA)

$(DIRS):
	@mkdir -p $@

$(TARGET_OUT)/dummy: $(DIRS)
	@echo "making root directories ok" 
	
ALL_PREBUILT += $(TARGET_OUT)/dummy
endif


#Targeting system configuration
include $(CLEAR_VARS)
copy_list := \
	etc/vold.fstab \
	etc/apns-conf.xml \
	etc/spn-conf.xml \
	etc/plmn-conf.xml \
	etc/ts.conf \
	etc/ts.env \
	etc/ntp.conf \
	etc/wifi/wpa_supplicant.conf \
	etc/media_profiles.xml \
	etc/dhcpcd/dhcpcd.conf \
	etc/firmware/zsp_data.bin \
	etc/firmware/zsp_ints.bin \
	etc/firmware/zsp_text.bin \
	etc/ppp/ip-up-ppp0 \
	etc/ppp/ip-down-ppp0 \
	etc/ppp/options \
	usr/keylayout/max8925_on.kl \
	usr/keylayout/pxa27x-keypad.kl \
	usr/keylayout/gpio-keys.kl \




#     move to product makefile like phoenix.mk	
#	etc/permissions/sw-features.xml \
#	etc/permissions/hw-features.xml \

#tutorial
copy_list += \
	etc/tutorial/tutorial.html \
	etc/tutorial/tutorial_zh_CN.html \
	etc/tutorial/tutorial_zh_TW.html \
	etc/tutorial/tutorial_files/2011-03-30-17-46-47.png \
	etc/tutorial/tutorial_files/2011-03-30-20-54-33.png \
	etc/tutorial/tutorial_files/2011-03-30-14-38-01.png \
	etc/tutorial/tutorial_files/image026.png \
	etc/tutorial/tutorial_files/2011-03-30-20-25-13.png \
	etc/tutorial/tutorial_files/2011-03-31-16-24-09.png \
	etc/tutorial/tutorial_files/2011-03-30-20-49-24.png \
	etc/tutorial/tutorial_files/image020.png \
	etc/tutorial/tutorial_files/2011-03-30-14-32-47.png \
	etc/tutorial/tutorial_files/2011-03-30-17-15-31.png \
	etc/tutorial/tutorial_files/2011-03-30-17-34-53.png \
	etc/tutorial/tutorial_files/2011-03-30-16-19-01.png \
	etc/tutorial/tutorial_files/2011-03-30-14-39-41.png \
	etc/tutorial/tutorial_files/2011-03-30-17-02-12.png \
	etc/tutorial/tutorial_files/2011-03-30-14-29-16.png \
	etc/tutorial/tutorial_files/2011-03-30-18-25-10.png

#install prebuit files with executable permission
copy_to := $(addprefix $(TARGET_OUT)/,$(copy_list))
$(copy_to) : $(TARGET_OUT)/% : $(LOCAL_PATH)/% | $(ACP)
	$(transform-prebuilt-to-target)

ALL_PREBUILT += $(copy_to)


file := $(TARGET_OUT_EXECUTABLES)/init.pppd.sh
$(file) : $(LOCAL_PATH)/init.pppd.sh | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)


file := $(TARGET_ROOT_OUT)/init.rc
$(file) : $(LOCAL_PATH)/init.rc | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)


