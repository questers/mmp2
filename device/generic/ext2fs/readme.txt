1.Please use new pgpt_xx.bin and agpt_xx.bin .

For 4GB (0x760000 sectors)
 partition            start block    start address   used blocks/total partition blocks     end address
1 kernel                   0x4c00          9961472                0x2000/0x4000                  18350079
2 android_system           0x8c00         18350080               0x4b000/0x4b000                175636479
3 android_data            0x53c00        175636480              0x2e7000/0x2e7000              1733820415
4 splash                 0x33ac00       1733820416                0x1000/0x1000                1735917567
5 custom                 0x33bc00       1735917568               0x18c00/0x18c00               1787822079   
6 kernel_recovery        0x354800       1787822080                0x4000/0x5000                1798307839
7 misc                   0x359800       1798307840                 0x400/0x400                 1798832127
8 cache                  0x359c00       1798832128               0x4b000/0x4b000               1956118527
9 storage                0x3a4c00       1956118528              0x3ba000/0x3ba000              3956801535

2.GPT background knowledge
http://en.wikipedia.org/wiki/GUID_Partition_Table
  As a summary,
  block 0 ,legacy MBR 
  block 1~0x33 pgpt
  block 0x34~0x75ffde user partition
  block 0x75ffdf~0x75ffff agpt
  