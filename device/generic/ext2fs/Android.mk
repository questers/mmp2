LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := setup_fs.c
LOCAL_MODULE := setup_fs
LOCAL_MODULE_TAGS := debug eng
LOCAL_SHARED_LIBRARIES += libcutils libutils
include $(BUILD_EXECUTABLE)

file := $(PRODUCT_OUT)/primary_gpt_2g
$(file) : $(LOCAL_PATH)/primary_gpt_pnx2g | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)

file := $(PRODUCT_OUT)/secondary_gpt_2g
$(file) : $(LOCAL_PATH)/secondary_gpt_pnx2g | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)

file := $(PRODUCT_OUT)/primary_gpt_4g
$(file) : $(LOCAL_PATH)/primary_gpt_pnx4g | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)

file := $(PRODUCT_OUT)/secondary_gpt_4g
$(file) : $(LOCAL_PATH)/secondary_gpt_pnx4g | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)

file := $(PRODUCT_OUT)/primary_gpt_8g
$(file) : $(LOCAL_PATH)/primary_gpt_pnx8g | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)

file := $(PRODUCT_OUT)/secondary_gpt_8g
$(file) : $(LOCAL_PATH)/secondary_gpt_pnx8g | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)

file := $(PRODUCT_OUT)/primary_gpt_16g
$(file) : $(LOCAL_PATH)/primary_gpt_pnx16g | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)

file := $(PRODUCT_OUT)/secondary_gpt_16g
$(file) : $(LOCAL_PATH)/secondary_gpt_pnx16g | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)

