GPT partition information

partition			start block address		end block address	size in block	size in byte	device name
-------------------------------------------------------------------------------------------------------------
kernel				0x4c00					0x8c00				0x4000			8M				mmcblk0p8
ramdisk				0x8c00					0xcc00				0x4000			8M				mmcblk0p1
android system		0xcc00					0x57c00				0x4b000			150M			mmcblk0p2
android data		0x57c00					0xa2c00				0x4b000			150M			mmcblk0p3
kernel recovery		0xa2c00					0xa6c00				0x4000			8M				-
ramdisk recovery	0xa6c00					0xaac00				0x4000			8M				mmcblk0p5
misc				0xaac00					0xab000				0x400			512K			mmcblk0p6
cache				0xab000					0xf6000				0x4b000			150M			mmcblk0p7
mass storage		0xf6000					0xe48c00			0xd52c00		6821M			mmcblk0p4
