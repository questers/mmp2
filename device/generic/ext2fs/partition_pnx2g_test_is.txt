    partition            start block    start address   used blocks/total partition blocks     end address
1 kernel                   0x4c00          9961472                0x2000/0x4000                  18350079
2 android_system           0x8c00         18350080               0x4b000/0x4b000                175636479
3 android_data            0x53c00        175636480               0x4b000/0x4b000                332922879
4 splash                  0x9ec00        332922880                0x1000/0x1000                 335020031
5 custom                  0x9fc00        335020032               0x18c00/0x18c00                386924543   
6 kernel_recovery         0xb8800        386924544                0x4000/0x5000                 397410303
7 misc                    0xbd800        397410304                 0x400/0x400                  397934591
8 cache                   0xbdc00        397934592               0x4b000/0x4b000                555220991
9 storage                0x108c00        555220992              0x29c000/0x29c000              1956118527

                                        
