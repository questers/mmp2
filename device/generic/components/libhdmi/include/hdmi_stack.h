/*******************************************************************************
*                Copyright 2007, MARVELL SEMICONDUCTOR, LTD.                   *
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL.                      *
* NO RIGHTS ARE GRANTED HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT  *
* OF MARVELL OR ANY THIRD PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE        *
* DISCRETION TO REQUEST THAT THIS CODE BE IMMEDIATELY RETURNED TO MARVELL.     *
* THIS CODE IS PROVIDED "AS IS". MARVELL MAKES NO WARRANTIES, EXPRESSED,       *
* IMPLIED OR OTHERWISE, REGARDING ITS ACCURACY, COMPLETENESS OR PERFORMANCE.   *
*                                                                              *
* MARVELL COMPRISES MARVELL TECHNOLOGY GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, *
* MARVELL INTERNATIONAL LTD. (MIL), MARVELL TECHNOLOGY, INC. (MTI), MARVELL    *
* SEMICONDUCTOR, INC. (MSI), MARVELL ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K.  *
* (MJKK), MARVELL ISRAEL LTD. (MSIL).                                          *
*******************************************************************************/

#ifndef _HDMI_STACK_H_
#define _HDMI_STACK_H_

typedef unsigned char       UCHAR;
typedef char                CHAR;
typedef UCHAR               BOOLEAN;
#ifndef BOOL
typedef UCHAR               BOOL;
#endif
typedef short               SHORT;
typedef unsigned short      USHORT;
typedef int                 INT;
typedef unsigned int        UINT; 
typedef long                LONG;
typedef unsigned long       ULONG;
typedef long long			LONGLONG;
typedef unsigned long long	ULONGLONG;
typedef void                VOID;
typedef void*               PTR;
typedef void**              PHANDLE;
typedef void*               HANDLE;
typedef void*               PVOID;

typedef UCHAR               BYTE;

typedef CHAR                INT8;
typedef UCHAR               UINT8;
typedef short               INT16;
typedef unsigned short      UINT16;
typedef int                 INT32;
typedef unsigned int        UINT32; 
typedef long long           INT64;
typedef unsigned long long  UINT64; 
typedef unsigned int        SIZE_T;

typedef signed int			HRESULT;

typedef HRESULT (*MV_VPP_EVENT_CALLBACK)(UINT32 EventCode, void *EventInfo, void *Context);
/*-----------------------------------------------------------------------------
 * Macros and Constants
 *-----------------------------------------------------------------------------
 */

/* definition of all the output color supported by VPP*/
typedef enum {
    OUTPUT_COLOR_FMT_INVALID     = -1,
    FIRST_OUTPUT_COLOR_FMT     = 0,
    OUTPUT_COLOR_FMT_RGB888      = 0,
    OUTPUT_COLOR_FMT_YCBCR444    = 1,
    OUTPUT_COLOR_FMT_YCBCR422    = 2,
    MAX_NUM_OUTPUT_COLOR_FMTS
} ENUM_OUTPUT_COLOR_FMT;

/* definition of all the bit depths (output) supported by VPP*/
typedef enum {
    OUTPUT_BIT_DEPTH_INVALID = -1,
    FIRST_OUTPUT_BIT_DEPTH   = 0,
    OUTPUT_BIT_DEPTH_12BIT   = 0,
    OUTPUT_BIT_DEPTH_10BIT   = 1,
    OUTPUT_BIT_DEPTH_8BIT    = 2,
    MAX_NUM_OUTPUT_BIT_DEPTHS
} ENUM_OUTPUT_BIT_DEPTH;

/*-----------------------------------------------------------------------------
 * HDMI Notification Events
 *-----------------------------------------------------------------------------
 */
#define MV_VPP_EVENT_HDMI_SINK_CONNECTED        0
#define MV_VPP_EVENT_HDMI_SINK_DISCONNECTED     1
#define MV_VPP_EVENT_HDMI_VIDEO_CFG_ERR         2
#define MV_VPP_EVENT_HDMI_AUDIO_CFG_ERR         3
#define MV_VPP_EVENT_HDMI_HDCP_ERR              4

/*-----------------------------------------------------------------------------
 * HDMI CEC Notification Events
 *-----------------------------------------------------------------------------
 */
#define MV_VPP_EVENT_CEC_LOG_ADDR_STS           0
#define MV_VPP_EVENT_CEC_MSG_TX_STS             1
#define MV_VPP_EVENT_CEC_MSG_RX_STS             2

/*-----------------------------------------------------------------------------
 * HDMI CEC Feature Support Configuration
 *-----------------------------------------------------------------------------
 */
#define     VPP_CEC_FEATURE_ONE_TOUCH_PLAY              0x00000001
#define     VPP_CEC_FEATURE_ROUTING_CONTROL             0x00000002
#define     VPP_CEC_FEATURE_SYSTEM_STANDBY              0x00000004
#define     VPP_CEC_FEATURE_ONE_TOUCH_RECORD            0x00000008
#define     VPP_CEC_FEATURE_SYSTEM_INFO                 0x00000010
#define     VPP_CEC_FEATURE_DECK_CONTROL                0x00000020
#define     VPP_CEC_FEATURE_TUNER_CONTROL               0x00000040
#define     VPP_CEC_FEATURE_VENDOR_SPEC_CMDS            0x00000080
#define     VPP_CEC_FEATURE_OSD_STATUS_DISPLAY          0x00000100
#define     VPP_CEC_FEATURE_DEVICE_OSD_NAME_TX          0x00000200
#define     VPP_CEC_FEATURE_DEVICE_MENU_CONTROL         0x00000400
#define     VPP_CEC_FEATURE_REMOTE_CONTROL_PASS_THRU    0x00000800
#define     VPP_CEC_FEATURE_DEVICE_POWER_STATUS         0x00001000

#define     VPP_CEC_FEATURE_SYSTEM_AUDIO_CONTROL        0x00002000
#define     VPP_CEC_FEATURE_AUDIO_RATE_CONTROL          0x00004000
#define     VPP_CEC_FEATURE_TIMER_PROGRAMMING           0x00008000

// Maximum message length
#define     VPP_CEC_MAX_MSG_LEN                         16

// Invalid physical address F.F.F.F
#define     VPP_CEC_INVALID_PHY_ADDR                    0xFFFF

/*-----------------------------------------------------------------------------
 * HDMI CEC Message Opcodes
 *-----------------------------------------------------------------------------
 */
#define VPP_CEC_MSG_OPCODE_UNDEFINED                    0xFFFF

// General Protocol messages
#define VPP_CEC_MSG_OPCODE_FEATURE_ABORT                0x00
#define VPP_CEC_MSG_OPCODE_ABORT                        0xFF

// One Touch Play
#define VPP_CEC_MSG_OPCODE_ACTIVE_SOURCE                0x82
#define VPP_CEC_MSG_OPCODE_IMAGE_VIEW_ON                0x04
#define VPP_CEC_MSG_OPCODE_TEXT_VIEW_ON                 0x0D

// Routing control
#define VPP_CEC_MSG_OPCODE_REQUEST_ACTIVE_SOURCE        0x85
#define VPP_CEC_MSG_OPCODE_SET_STREAM_PATH              0x86
#define VPP_CEC_MSG_OPCODE_ROUTING_CHANGE               0x80
#define VPP_CEC_MSG_OPCODE_ROUTING_INFO                 0x81
#define VPP_CEC_MSG_OPCODE_INACTIVE_SOURCE              0x9D

// Standby
#define VPP_CEC_MSG_OPCODE_STANDBY                      0x36

// One touch record
#define VPP_CEC_MSG_OPCODE_RECORD_OFF                   0x0B
#define VPP_CEC_MSG_OPCODE_RECORD_ON                    0x09
#define VPP_CEC_MSG_OPCODE_RECORD_STATUS                0x0A
#define VPP_CEC_MSG_OPCODE_RECORD_TV_SCREEN             0x0F

// System information
#define VPP_CEC_MSG_OPCODE_GET_MENU_LANG                0x91
#define VPP_CEC_MSG_OPCODE_GIVE_PHY_ADDR                0x83
#define VPP_CEC_MSG_OPCODE_REPORT_PHY_ADDR              0x84
#define VPP_CEC_MSG_OPCODE_SET_MENU_LANG                0x32
#define VPP_CEC_MSG_OPCODE_CEC_VERSION                  0x9E
#define VPP_CEC_MSG_OPCODE_GET_CEC_VERSION              0x9F

// Deck control
#define VPP_CEC_MSG_OPCODE_DECK_STATUS                  0x1B
#define VPP_CEC_MSG_OPCODE_GIVE_DECK_STATUS             0x1A
#define VPP_CEC_MSG_OPCODE_DECK_CONTROL                 0x42
#define VPP_CEC_MSG_OPCODE_PLAY                         0x41

// Tuner control
#define VPP_CEC_MSG_OPCODE_GIVE_TUNER_DEVICE_STATUS     0x08
#define VPP_CEC_MSG_OPCODE_SELECT_ANALOGUE_SERVICE      0x92
#define VPP_CEC_MSG_OPCODE_SELECT_DIGITAL_SERVICE       0x93
#define VPP_CEC_MSG_OPCODE_TUNER_STEP_DECREMENT         0x06
#define VPP_CEC_MSG_OPCODE_TUNER_STEP_INCREMENT         0x05
#define VPP_CEC_MSG_OPCODE_TUNER_DEVICE_STATUS          0x07

// Vendor specific commands
#define VPP_CEC_MSG_OPCODE_DEVICE_VENDOR_ID             0x87
#define VPP_CEC_MSG_OPCODE_GIVE_DEVICE_VENDOR_ID        0x8C
#define VPP_CEC_MSG_OPCODE_VENDOR_COMMAND               0x89
#define VPP_CEC_MSG_OPCODE_VENDOR_COMMAND_WITH_ID       0xA0
#define VPP_CEC_MSG_OPCODE_VENDOR_REMOTE_BTN_DOWN       0x8A
#define VPP_CEC_MSG_OPCODE_VENDOR_REMOTE_BTN_UP         0x8B

// OSD status display
#define VPP_CEC_MSG_OPCODE_SET_OSD_STRING               0x64

// Device OSD name transfer
#define VPP_CEC_MSG_OPCODE_GIVE_OSD_NAME                0x46
#define VPP_CEC_MSG_OPCODE_SET_OSD_NAME                 0x47

// Device menu control, Remote control pass-through
#define VPP_CEC_MSG_OPCODE_USER_CONTROL_PRESSED         0x44
#define VPP_CEC_MSG_OPCODE_USER_CONTROL_RELEASED        0x45
#define VPP_CEC_MSG_OPCODE_MENU_REQUEST                 0x8D
#define VPP_CEC_MSG_OPCODE_MENU_STATUS                  0x8E

// Device power status
#define VPP_CEC_MSG_OPCODE_GIVE_DEVICE_POWER_STATUS     0x8F
#define VPP_CEC_MSG_OPCODE_REPORT_POWER_STATUS          0x90

// System Audio Control
#define VPP_CEC_MSG_OPCODE_GIVE_AUDIO_STATUS            0x71
#define VPP_CEC_MSG_OPCODE_GIVE_SYS_AUDIO_MODE_STATUS   0x7D
#define VPP_CEC_MSG_OPCODE_REPORT_AUDIO_STATUS          0x7A
#define VPP_CEC_MSG_OPCODE_SET_SYS_AUDIO_MODE           0x72
#define VPP_CEC_MSG_OPCODE_SYS_AUDIO_MODE_REQUEST       0x70
#define VPP_CEC_MSG_OPCODE_SYS_AUDIO_MODE_STATUS        0x7E

// Audio Rate Control
#define VPP_CEC_MSG_OPCODE_SET_AUDIO_RATE               0x9A

// Timer Programming
#define VPP_CEC_MSG_OPCODE_CLR_ANALOGUE_TIMER           0x33
#define VPP_CEC_MSG_OPCODE_CLR_DIGITAL_TIMER            0x99
#define VPP_CEC_MSG_OPCODE_CLR_EXTERNAL_TIMER           0xA1
#define VPP_CEC_MSG_OPCODE_SET_ANALOGUE_TIMER           0x34
#define VPP_CEC_MSG_OPCODE_SET_DIGITAL_TIMER            0x97
#define VPP_CEC_MSG_OPCODE_SET_EXTERNAL_TIMER           0xA2
#define VPP_CEC_MSG_OPCODE_SET_TIMER_PGM_TITLE          0x67
#define VPP_CEC_MSG_OPCODE_TIMER_CLEARED_STATUS         0x43
#define VPP_CEC_MSG_OPCODE_TIMER_STATUS                 0x35

/* definition of VPP TG timing formats */
typedef enum {
    RES_INVALID   = -1,
    FIRST_RES     = 0,
    RES_NTSC_M    = 0,
    RES_NTSC_J    = 1,
    RES_PAL_M     = 2,
    RES_PAL_BGH   = 3,
    RES_525I60    = 4,
    RES_525I5994  = 5,
    RES_625I50    = 6,
    RES_525P60    = 7,//yes display on SONY 480p 60Hz
    RES_525P5994  = 8,// yes on SONY
    RES_625P50    = 9,//yes display on SONY 576p 50Hz
    RES_720P30    = 10,
    RES_720P2997  = 11,
    RES_720P25    = 12,
    RES_720P60    = 13,//yes on SONY
    RES_720P5994  = 14,//yes on SONY
    RES_720P50    = 15,//yes on SONY
    RES_1080I60   = 16,
    RES_1080I5994 = 17,
    RES_1080I50   = 18,
    RES_1080P30   = 19,
    RES_1080P2997 = 20,
    RES_1080P25   = 21,
    RES_1080P24   = 22,//yes
    RES_1080P2398 = 23,//yes
    RES_1080P60   = 24,//yes
    RES_1080P5994 = 25,//yes
    RES_1080P50   = 26,//yes
    RES_VGA_480P60 = 27,
    RES_VGA_480P5994 = 28,
    RES_RESET,
    MAX_NUM_RESS
}ENUM_CPCB_TG_RES;

typedef struct RESOLUTION_INFO_T {
    int active_width;
    int active_height;   /* active height of channel in pixel */
    int width;  /* width of channel in pixel */
    int height; /* height of channel in pixel */
    int hfrontporch; /* front porch of hsync */
    int hsyncwidth; /* hsync width */
    int hbackporch; /* back porch of hsync */
    int vfrontporch; /* front porch of vsync */
    int vsyncwidth; /* vsync width */
    int vbackporch; /* back porch of vsync */
    int type;   /* resolution type: HD or SD */
    int scan;   /* scan mode: progressive or interlaced */
    int frame_rate;   /* frame rate */
    int flag_3d;   /* 1 for 3D, 0 for 2D */
    int freq;   /* pixel frequency */
    int pts_per_cnt_4;   /* time interval in term of PTS for every 4 frames */
}RESOLUTION_INFO;

/* definition of video resolution type */
enum {
    TYPE_SD = 0,
    TYPE_HD = 1,
};

/* definition of video scan mode */
enum {
    SCAN_PROGRESSIVE = 0,
    SCAN_INTERLACED  = 1,
};

/* definition of video frame-rate */
enum {
    FRAME_RATE_59P94 = 0,
    FRAME_RATE_60    = 1,
    FRAME_RATE_50    = 2,
    FRAME_RATE_29P97 = 3,
    FRAME_RATE_30    = 4,
    FRAME_RATE_25    = 5,
    FRAME_RATE_23P98 = 6,
    FRAME_RATE_24    = 7,
};

int MV_VPPOBJ_PhyReset(int vpp_handle);

/*-----------------------------------------------------------------------------
 * HDMI Sink Capabilities structures
 *-----------------------------------------------------------------------------
 */
typedef enum VPP_HDMI_AUDIO_FMT_T
{
    VPP_HDMI_AUDIO_FMT_UNDEF = 0x00,
    VPP_HDMI_AUDIO_FMT_PCM   = 0x01,
    VPP_HDMI_AUDIO_FMT_AC3,
    VPP_HDMI_AUDIO_FMT_MPEG1,
    VPP_HDMI_AUDIO_FMT_MP3,
    VPP_HDMI_AUDIO_FMT_MPEG2,
    VPP_HDMI_AUDIO_FMT_AAC,
    VPP_HDMI_AUDIO_FMT_DTS,
    VPP_HDMI_AUDIO_FMT_ATRAC,
    VPP_HDMI_AUDIO_FMT_ONE_BIT_AUDIO,
    VPP_HDMI_AUDIO_FMT_DOLBY_DIGITAL_PLUS,
    VPP_HDMI_AUDIO_FMT_DTS_HD,
    VPP_HDMI_AUDIO_FMT_MAT,
    VPP_HDMI_AUDIO_FMT_DST,
    VPP_HDMI_AUDIO_FMT_WMA_PRO,
}VPP_HDMI_AUDIO_FMT;

typedef struct VPP_HDMI_RES_INFO_T {
    int     hActive;
    int     vActive;
    // Refresh rate in Hz, -1 if refresh rate is
    // undefined in the descriptor
    int     refreshRate;
    // 0 = progressive, 1 = interlaced,  2 = undefined
    int     interlaced;
} VPP_HDMI_RES_INFO;

typedef struct VPP_HDMI_AUDIO_FREQ_SPRT_T {
    unsigned char   Res         : 1;
    unsigned char   Fs32KHz     : 1;
    unsigned char   Fs44_1KHz   : 1;
    unsigned char   Fs48KHz     : 1;
    unsigned char   Fs88_2KHz   : 1;
    unsigned char   Fs96KHz     : 1;
    unsigned char   Fs176_4KHz  : 1;
    unsigned char   Fs192KHz    : 1;
} VPP_HDMI_AUDIO_FREQ_SPRT;

typedef struct VPP_HDMI_AUDIO_WDLEN_SPRT_T {
    unsigned char   Res1    : 1;
    unsigned char   WdLen16 : 1;
    unsigned char   WdLen20 : 1;
    unsigned char   WdLen24 : 1;
    unsigned char   Res2    : 4;
} VPP_HDMI_AUDIO_WDLEN_SPRT;

typedef struct VPP_HDMI_AUDIO_INFO_T {
    INT                         audioFmt; // VPP_HDMI_AUDIO_FMT
    VPP_HDMI_AUDIO_FREQ_SPRT    freqSprt;
    // Field is valid only for compressed audio formats
    UINT32                      maxBitRate; // in KHz
    // Field is valid only for LPCM
    VPP_HDMI_AUDIO_WDLEN_SPRT   wdLenSprt;
    unsigned char               maxNumChnls;
} VPP_HDMI_AUDIO_INFO;

typedef struct VPP_HDMI_SPKR_ALLOC_T {
    unsigned char   FlFr  : 1; // FrontLeft/Front Rear
    unsigned char   Lfe   : 1; // Low Frequency Effect
    unsigned char   Fc    : 1; // Front Center
    unsigned char   RlRr  : 1; // Rear Left/Rear Right
    unsigned char   Rc    : 1; // Rear Center
    unsigned char   FlcFrc: 1; // Front Left Center/Front Right Center
    unsigned char   RlcRrc: 1; // Rear Left Center /Rear Right Center
    unsigned char   Res   : 1;
} VPP_HDMI_SPKR_ALLOC;

// Calorimetry support
typedef struct tagVPP_HDMI_CALORIMETRY_INFO
{
    unsigned char   xvYCC601    : 1;
    unsigned char   xvYCC709    : 1;
    unsigned char   MD0         : 1;
    unsigned char   MD1         : 1;
    unsigned char   MD2         : 1;
    unsigned char   res         : 3;
}VPP_HDMI_CALORIMETRY_INFO;

// Pixel repetition info
typedef struct tagVPP_HDMI_PIXEL_REPT_INFO
{
    unsigned int    resMask     : 26;
    unsigned int    prSupport   : 6;
}VPP_HDMI_PIXEL_REPT_INFO;


typedef struct VPP_HDMI_SINK_CAPS_T {
    // EDID Valid
    BOOL    validEdid;

    // Product Info
    unsigned char   monitorName[14];// Monitor Name in ASCII format
    unsigned char   mnfrName[4];
    unsigned char   productCode[5];
    unsigned char   mnfrWeek;
    unsigned short  mnfrYear;
    unsigned int    serialNum;

    // Monitor Limits
    unsigned char   minVRate;       // Minimum Vertical Rate in Hz
    unsigned char   maxVRate;       // Maximum Vertical Rate in Hz
    unsigned char   minHRate;       // Minimum Horizontal Rate in KHz
    unsigned char   maxHRate;       // Maximum Horizontal Rate in KHz
    unsigned int    maxPixelClock;  // Maximum supported pixel clock rate in MHz

    // Maximum image size
    unsigned short  maxHSize;
    unsigned short  maxVSize;

    // Gamma Factor
    float   gamma;

    // Resolution information
    unsigned int      resCnt;
    VPP_HDMI_RES_INFO resInfo[64];

    // Indicates if the receiver is HDMI capable
    BOOL              hdmiMode;

    // Video resolutions supported both by transmitter and receiver
    // (Common ones that can be set for hdmi output display)
    unsigned int       sprtedDispRes;

#if /*(BERLIN_CHIP_VERSION >= BERLIN_C_2) && */defined(PE_3D_BD)
    BOOL              support_3D;    // indicates if the receiver supports 3D formats
    unsigned int      sprted3DDispRes; // bit[x]: 1 - support, 0 - not support. x means for 3D resolution 3D_FIRST_3D+x
    unsigned int      sprted3DStructures[/*MAX_NUM_RES_3D-FIRST_RES_3D+1*/32]; // sprted3DStructures[x]: x means for 3D resolution 3D_FIRST_3D+x
                                                                         // bit[15~0]: bit[x] set to 1 means 3D structure x is supported, otherwise is not supported.
                                                                         // bit[31~16]: extra parameter for structure 8 or above.
#endif

    // Preferred video resolution
    VPP_HDMI_RES_INFO           prefResInfo;

    // Sink feature support
    unsigned char               YCbCr444Sprt;
    unsigned char               YCbCr422Sprt;
    unsigned char               DC30bitSprt;
    unsigned char               DC36bitSprt;
    unsigned char               YCbCr444AtDC;
    unsigned char               AISprt;

    // Pixel repetition support
    VPP_HDMI_PIXEL_REPT_INFO    prInfo[4];

    // Colorimtery information
    VPP_HDMI_CALORIMETRY_INFO   calInfo;

    // Supported audio format
    VPP_HDMI_SPKR_ALLOC         spkrAlloc;
    VPP_HDMI_AUDIO_INFO         audioInfo[15];

    // CEC Physical address
    unsigned short              cecPhyAddr;
} VPP_HDMI_SINK_CAPS;

//#include "ErrorCode.h"
//#include "vpp_api_types.h"
// error code base list
#define ERRORCODE_BASE		(0)
#define E_GENERIC_BASE		( 0x0000 << 16 )
#define E_SYSTEM_BASE		( 0x0001 << 16 )
#define E_DEBUG_BASE		( 0x0002 << 16 )


#define E_WMDRM_BASE		( 0x0004 << 16 )
#define E_AUDIO_BASE		( 0x0010 << 16 )
#define E_VIDEO_BASE		( 0x0020 << 16 )
#define E_SUB_BASE			( 0x0030 << 16 )	// Generic subtitle
#define E_DEMUX_BASE		( 0x0040 << 16 )
#define E_BD_BASE			( 0x0050 << 16 )
#define E_HDDVD_BASE		( 0x0060 << 16 )	// HDDVD: Standard Content
#define E_HDI_BASE			( 0x0070 << 16 )	// HDDVD: Advanced Content
#define E_GFX_BASE			( 0x0080 << 16 )	// GFXDS in PE
#define E_DC_BASE			( 0x0090 << 16)		// DC Base
#define E_DOM_BASE			( 0x00A0 << 16)		// MV DOM
#define E_BDG_BASE          ( 0x00B0 << 16 )    // BD Graphics (IG/PG/TS)
#define E_PE_BASE			( 0x00C0 << 16)		// PE Base
#define E_DVDSPU_BASE		( 0x00D0 << 16)		// GraphDec  HDDVD&DVD SPU
#define E_VFD_BASE			( 0x00E0 << 16)		// GraphDec  HDDVD&DVD SPU
#define E_NET_BASE			( 0x00F0 << 16)		//Network
#define E_OU_BASE			( 0x0100 << 16)		//online upgrade base
#define E_VPP_BASE                 ( 0x0200 << 16)   //VPP base
#define E_DIVX_BASE			( 0x0400 << 16)		// DivX base
#define E_BDRESUB_BASE          ( 0x0800 << 16 )    // BD-RE SUBs (DVB/ISDB/ATSC)

#define E_SUC				( 0x00000000 )
#define E_ERR				( 0x80000000 )
#define S_VPP( code ) ( E_SUC | E_VPP_BASE | ( (code) & 0xFFFF ) )
#define E_VPP( code ) ( E_ERR | E_VPP_BASE | ( (code) & 0xFFFF ) )

#define S_VPP_OK 						(S_OK)

/* error code */
#define VPP_E_NODEV         E_VPP(1)
#define VPP_E_BADPARAM      E_VPP(2)
#define VPP_E_BADCALL       E_VPP(3)
#define VPP_E_UNSUPPORT     E_VPP(4)
#define VPP_E_IOFAIL        E_VPP(5)
#define VPP_E_UNCONFIG      E_VPP(6)
#define VPP_E_CMDQFULL      E_VPP(7)
#define VPP_E_FRAMEQFULL    E_VPP(8)
#define VPP_E_BCMBUFFULL    E_VPP(9)
#define VPP_E_NOMEM         E_VPP(10)
#define VPP_EVBIBUFFULL  E_VPP(11)
#define VPP_EHARDWAREBUSY  E_VPP(12)

// error code definitions
#define MV_VPP_OK          0
#define MV_VPP_ENODEV      VPP_E_NODEV
#define MV_VPP_EBADPARAM   VPP_E_BADPARAM
#define MV_VPP_EBADCALL    VPP_E_BADCALL
#define MV_VPP_EUNSUPPORT  VPP_E_UNSUPPORT
#define MV_VPP_EIOFAIL     VPP_E_IOFAIL
#define MV_VPP_EUNCONFIG   VPP_E_UNCONFIG
#define MV_VPP_ECMDQFULL   VPP_E_CMDQFULL
#define MV_VPP_EFRAMEQFULL VPP_E_FRAMEQFULL
#define MV_VPP_EBCMBUFFULL VPP_E_BCMBUFFULL
#define MV_VPP_ENOMEM      VPP_E_NOMEM
#define MV_VPP_EVBIBUFFULL VPP_EVBIBUFFULL
#define MV_VPP_EHARDWAREBUSY VPP_EHARDWAREBUSY

#define VPP_IOCTL_VBI_DMA_CFGQ 0xbeef0001
#define VPP_IOCTL_VBI_BCM_CFGQ 0xbeef0002
#define VPP_IOCTL_VDE_BCM_CFGQ 0xbeef0003
#define VPP_IOCTL_TIMING       0xbeef0004
#define VPP_IOCTL_START_BCM_TRANSACTION 0xbeef0005

/* definition of VPP status */
typedef enum {
    STATUS_INACTIVE         = 0, /* for plane and channel */
    STATUS_INACTIVE_PENDING = 1, /* for plane and channel */
    STATUS_ACTIVE           = 2, /* for plane and channel */
    STATUS_ACTIVE_PENDING   = 3, /* for plane and channel */
    STATUS_DISP             = 4, /* for channel only */
    STATUS_DISP_LOGO        = 5, /* for plane only */
    STATUS_DISP_VIDEO       = 6, /* for plane only */
    STATUS_DISP_PATGEN      = 7, /* for main plane */
}ENUM_PLANE_STATUS;

/************* VPP module external APIs *****************/

/*********************************************************
 * FUNCTION: initialize VPP module
 * PARAMS: fb - the dev path of framebuffer
 * RETURN: MV_VPP_OK - succeed
 *         MV_VPP_EBADCALL - function called previously
 ********************************************************/
int MV_VPP_Init(const char *fb);

/***********************************************
 * FUNCTION: create a VPP object
 * PARAMS: base_addr - VPP object base address
 *         *handle - pointer to object handle
 * RETURN: MV_VPP_OK - succeed
 *         MV_VPP_EUNCONFIG - not initialized
 *         MV_VPP_ENODEV - no device
 *         MV_VPP_ENOMEM - no memory
 ***********************************************/
int MV_VPPOBJ_Create(int *handle);

/***********************************************
 * FUNCTION: destroy a VPP object
 * PARAMS: handle - VPP object handle
 * RETURN: MV_VPP_OK - succeed
 *         MV_VPP_EUNCONFIG - not initialized
 *         MV_VPP_ENODEV - no device
 *         MV_VPP_ENOMEM - no memory
 ***********************************************/
int MV_VPPOBJ_Destroy(int handle);

/***************************************
 * FUNCTION: VPP reset
 * INPUT: NONE
 * RETURN: NONE
 **************************************/
int MV_VPPOBJ_Reset(int handle);

/********************************************************************************
 * FUNCTION: Enable/Disable HDCP
 * INPUT: Enable - Control to turn HDCP on/off
 * RETURN: MV_VPP_OK - SUCCEED
 *         MV_EBADPARAM - invalid parameters
 *         MV_EUNCONFIG - VPP not configured
 *         MV_VPP_ENODEV - no device
 *         MV_EUNSUPPORT - channel not connected in configuration
 *         MV_VPP_EBADCALL - channel not connected to DV1
 *         MV_ECMDQFULL - command queue is full
 ********************************************************************************/
int MV_VPPOBJ_EnableHDCP(int handle, int enable);

int MV_VPPOBJ_ConfigOutputRes(int handle, int resID);
/********************************************************************************
 * FUNCTION: Configure SRM data
 * INPUT: pSrmBuf - Pointer to SRM data buffer (Input)
 *      : pSignValid - Pointer to return if the signature is valid on the given
 *      :            - SRM data (Output)
 *      : pVernNum - Pointer to return version number of SRM data (output)
 * RETURN: MV_VPP_OK - SUCCEED
 *         MV_EBADPARAM - invalid parameters
 *         MV_EUNCONFIG - VPP not configured
 *         MV_VPP_ENODEV - no device
 *         MV_EUNSUPPORT - channel not connected in configuration
 *         MV_VPP_EBADCALL - channel not connected to DV1
 *         MV_ECMDQFULL - command queue is full
 ********************************************************************************/
int MV_VPPOBJ_ConfigureSRM(int handle, UINT8 *pSrmBuf, int *pSignValid, int *pVernNum);

/********************************************************************************
 * FUNCTION: Load HDCP Keys to the registers
 * INPUT: handle    - VPP object handle
 *      : pHdcpKeys - Pointer to HDCP keys to be loaded
 * RETURN: MV_VPP_OK - SUCCEED
 *         MV_EBADPARAM - invalid parameters
 *         MV_EUNCONFIG - VPP not configured
 *         MV_VPP_ENODEV - no device
 *         MV_EUNSUPPORT - channel not connected in configuration
 *         MV_VPP_EBADCALL - HDCP is enabled currently(Keys cannot be loaded
 *                         - with HDCP on)
 ********************************************************************************/
int MV_VPPOBJ_LoadHDCPKeys(int handle);

/***************************************
 * FUNCTION: VPP profile configuration
 * INPUT: NONE
 * RETURN: NONE
 **************************************/
int MV_VPPOBJ_Config(int handle);

/********************************************************************************
 * FUNCTION: Get Hdmi Sink Capabilities
 * INPUT: pSinkCaps
 * RETURN: MV_VPP_OK - SUCCEED
 *         MV_EBADPARAM - invalid parameters
 *         MV_EUNCONFIG - VPP not configured
 *         MV_VPP_ENODEV - no device
 *         MV_EUNSUPPORT - channel not connected in configuration
 *         MV_VPP_EBADCALL - channel not connected to DV1
 *         MV_ECMDQFULL - command queue is full
 ********************************************************************************/
int MV_VPPOBJ_GetHDMISinkCaps(int handle, VPP_HDMI_SINK_CAPS *pSinkCaps);

/********************************************************************************
 * FUNCTION: print edid info for vpp_handle 
 ********************************************************************************/
void MV_VPPOBJ_PrintEdidInfo (int vpp_handle);

/********************************************************************************
 * FUNCTION: Register for HDMI event notifications
 * INPUT: eventCode - Event code for which notifications has to be raised
 *      : cbFnAddr - Address of callback function
 *      : contextParam - Context parameter for the registering application
 * RETURN: MV_VPP_OK - SUCCEED
 *         MV_EBADPARAM - invalid parameters
 *         MV_EUNCONFIG - VPP not configured
 *         MV_VPP_ENODEV - no device
 *         MV_EUNSUPPORT - channel not connected in configuration
 *         MV_VPP_EBADCALL - channel not connected to DV1
 *         MV_ECMDQFULL - command queue is full
 ********************************************************************************/
int MV_VPPOBJ_RegisterHdmiCallback (int handle, UINT32 eventCode,
                                    MV_VPP_EVENT_CALLBACK callbackFn,
                                    void *contextParam);

/********************************************************************************
 * FUNCTION: Get HDMI connect status
 */
int MV_VPPOBJ_GetHDMIConnectStatus(int handle);

/******************************************************************************
 * FUNCTION: Config Audio register for HDMI
 * INPUT: numChannels - number of channels
 *      : sampFreq - sample rate, for example, 48000 means 48KHz sample rate
 *      : mClkFactor - use which mclk factor in HDMI, you can set 128, 256, 384
 *      : spkrCfg - the location of speaker, set -1 if location is unknown
 *      : timeInSecs - not used currently
 *****************************************************************************/
void MV_VPPOBJ_ConfigAudio(int handle, int numChannels, int sampFreq, int mClkFactor,
				 int spkrCfg, int timeInSecs);

/******************************************************************************
 * FUNCTION: mute/un-mute hdmi audio output
 * INPUT: mute - 1:mute, 0:un-mute
 * RETURN: MV_VPP_OK - SUCCEED
 *         MV_EBADPARAM - invalid parameters
 *         MV_EUNCONFIG - VPP not configured
 *         MV_VPP_ENODEV - no device
 *         MV_EUNSUPPORT - channel not connected in configuration
 *         MV_VPP_EBADCALL - channel not connected to DV1
 *         MV_ECMDQFULL - command queue is full
 ******************************************************************************/
int MV_VPPOBJ_SetHdmiAudioMute(int handle, int mute);

/********************************************************************************
 * FUNCTION: Set Hdmi Video format
 * INPUT: color_fmt - color format (RGB, YCbCr 444, 422)
 *      : bit_depth - 8/10/12 bit color
 *      : pixel_rept - 1/2/4 repetitions of pixel
 * RETURN: MV_VPP_OK - SUCCEED
 *         MV_EBADPARAM - invalid parameters
 *         MV_EUNCONFIG - VPP not configured
 *         MV_VPP_ENODEV - no device
 *         MV_EUNSUPPORT - channel not connected in configuration
 *         MV_VPP_EBADCALL - channel not connected to DV1
 *         MV_ECMDQFULL - command queue is full
 ********************************************************************************/
int MV_VPPOBJ_SetHdmiVideoFmt(int handle, int color_fmt, int bit_depth, int pixel_rept);

/******************************************************************************
 * FUNCTION:set hdmi mute
 * mute - 1 mute; 0 unmute
 *  ******************************************************************************/
int MV_VPPOBJ_SetHDMIMute(int handle, int mute);

/******************************************************************************
 * FUNCTION:disable hdmi
 *  ******************************************************************************/
int MV_VPPOBJ_DisableHdmi(int handle);

#endif
