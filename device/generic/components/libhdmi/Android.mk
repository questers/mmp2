LOCAL_PATH:= $(call my-dir)

# libhdmi_mrvl
# =====================================================
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := lib/libvpp.bionic.a lib/libosal.bionic.a lib/libshm.bionic.a
LOCAL_MODULE_TAGS := optional
include $(BUILD_MULTI_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libhdmi_mrvl
LOCAL_WHOLE_STATIC_LIBRARIES := \
	libvpp.bionic \
	libosal.bionic \
	libshm.bionic

LOCAL_SHARED_LIBRARIES += libwtpsp

LOCAL_PRELINK_MODULE := false
LOCAL_LDLIBS := -ldl

LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)

# The sample code
# =====================================================
include $(CLEAR_VARS)

LOCAL_MODULE := hdmi_demo0

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/include
LOCAL_SRC_FILES := \
	sample/main_hdmi.c
LOCAL_SHARED_LIBRARIES := libhdmi_mrvl libwtpsp

LOCAL_MODULE_TAGS := optional
include $(BUILD_EXECUTABLE)
