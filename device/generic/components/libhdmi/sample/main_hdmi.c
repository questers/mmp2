#include <unistd.h>
#include "hdmi_stack.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#define MMP2_DEBUG

int m_HdmiRxConnected = 0;
int vpp_handle;
extern unsigned *hdmi_base;
VPP_HDMI_SINK_CAPS m_SinkCaps;

//
// get HDMI receiver preferred resolution which is supported by Berlin player
//
static void getPreferredResolution(int *phdmiRes)
{
    *phdmiRes = RES_INVALID;

    if ((m_SinkCaps.prefResInfo.hActive == 720)
        &&(m_SinkCaps.prefResInfo.vActive == 480)
        &&(m_SinkCaps.prefResInfo.refreshRate == 60)) {

        if (m_SinkCaps.prefResInfo.interlaced == 1)
            *phdmiRes = RES_525I5994;
        else
            *phdmiRes = RES_525P5994;

    } else if ((m_SinkCaps.prefResInfo.hActive == 720)
               &&(m_SinkCaps.prefResInfo.vActive == 576)
               &&(m_SinkCaps.prefResInfo.refreshRate == 50)) {

        if (m_SinkCaps.prefResInfo.interlaced == 1)
            *phdmiRes = RES_625I50;
        else
            *phdmiRes = RES_625P50;

    } else if ((m_SinkCaps.prefResInfo.hActive == 1280)
               &&(m_SinkCaps.prefResInfo.vActive == 720)) {

        if (m_SinkCaps.prefResInfo.refreshRate == 30)
            *phdmiRes = RES_720P2997;
        else if (m_SinkCaps.prefResInfo.refreshRate == 25)
            *phdmiRes = RES_720P25;
        else if (m_SinkCaps.prefResInfo.refreshRate == 60)
            *phdmiRes = RES_720P5994;
        else if (m_SinkCaps.prefResInfo.refreshRate == 50)
            *phdmiRes = RES_720P50;

    } else if ((m_SinkCaps.prefResInfo.hActive == 1920)
               &&(m_SinkCaps.prefResInfo.vActive == 1080)) {

        if (m_SinkCaps.prefResInfo.refreshRate == 30)
            *phdmiRes = RES_1080P2997;
        else if (m_SinkCaps.prefResInfo.refreshRate == 25)
            *phdmiRes = RES_1080P25;
        else if (m_SinkCaps.prefResInfo.refreshRate == 24)
            *phdmiRes = RES_1080P24;
        else if (m_SinkCaps.prefResInfo.refreshRate == 60) {
             if (m_SinkCaps.prefResInfo.interlaced == 1)
                 *phdmiRes = RES_1080I5994;
             else
                 *phdmiRes = RES_1080P5994;
        }
        else if (m_SinkCaps.prefResInfo.refreshRate == 50) {
             if (m_SinkCaps.prefResInfo.interlaced == 1)
                 *phdmiRes = RES_1080I50;
             else
                 *phdmiRes = RES_1080P50;
        }
    }
}

int HdmiEvtCB (unsigned int eventCode, void *pEventInfo, void *pContext)
{
	int Res;
    unsigned int evt = *((unsigned int*)(pEventInfo));
    switch (evt)
    {
        case MV_VPP_EVENT_HDMI_SINK_CONNECTED:
        {
            printf("connected event handled !\n"); 
			m_HdmiRxConnected = 1;
            // read HDMI sink capability
            MV_VPPOBJ_GetHDMISinkCaps(vpp_handle, &m_SinkCaps);
            if (m_SinkCaps.validEdid)
            {
		getPreferredResolution(&Res);//get edid device preferred resolution
		MV_VPPOBJ_PhyReset(vpp_handle);
		MV_VPPOBJ_ConfigOutputRes(vpp_handle, Res);
		MV_VPPOBJ_SetHdmiVideoFmt(vpp_handle, OUTPUT_COLOR_FMT_RGB888, OUTPUT_BIT_DEPTH_8BIT, 1);
		MV_VPPOBJ_ConfigAudio(vpp_handle, 2, 48000, 128, -1, 100);

		MV_VPPOBJ_PrintEdidInfo(vpp_handle);//print edid info
            }
        }
        break;
       case MV_VPP_EVENT_HDMI_SINK_DISCONNECTED:
        {
		printf("disconnected event handled !\n");
		MV_VPPOBJ_DisableHdmi(vpp_handle);
		printf("hdmi disabled !\n");
            m_HdmiRxConnected = 0;
        }
        break;
        case MV_VPP_EVENT_HDMI_VIDEO_CFG_ERR:
        {
            printf ("HDMI - Video config error\n");
        }
        break;
        case MV_VPP_EVENT_HDMI_AUDIO_CFG_ERR:
        {
            printf ("HDMI - Audio config error\n");
        }
        break;
        case MV_VPP_EVENT_HDMI_HDCP_ERR:
        {
            printf ("HDCP error - private keys not stored\n");
        }
        break;
        default:
            break;
    }
    return (0);
}

typedef void (*pFunc)(void);
typedef void (*pFunc1)(unsigned int);
typedef void (*pFunc2)(unsigned int,unsigned int);
typedef void (*pFunc3)(unsigned int,unsigned int,unsigned int);
typedef void (*pFunc4)(unsigned int,unsigned int,unsigned int,unsigned int);

typedef struct _hdmi_cmd_desc
{
    pFunc func;
    int para_count;
    char *name;
    char *help_info;
}hdmi_cmd_desc;

#define HDMI_P0_CMD(name) int hdmi_cmd_##name(void)
#define HDMI_P1_CMD(name, p1) int hdmi_cmd_##name(int p1)
#define HDMI_P2_CMD(name, p1, p2) int hdmi_cmd_##name(int p1, int p2)
#define HDMI_P3_CMD(name, p1, p2, p3) int hdmi_cmd_##name(int p1, int p2, int p3)
#define HDMI_P4_CMD(name, p1, p2, p3, p4) int hdmi_cmd_##name(int p1, int p2, int p3, int p4)
#define SHOW_REG_ITEM(name) \
    { \
        .addr = name, \
        .info = #name, \
    }


#define HDMI_CMD(na,nu,he) \
{ \
    .name = #na, \
    .func = (pFunc)hdmi_cmd_##na, \
    .para_count = nu, \
    .help_info = he, \
}
#define HDMI_CMD_END \
{ \
    .name = NULL, \
    .func = NULL, \
}

HDMI_P1_CMD(sv, i)
{
	printf("start set video format\n");
	MV_VPPOBJ_PhyReset(vpp_handle);
	MV_VPPOBJ_ConfigOutputRes(vpp_handle, i);
	MV_VPPOBJ_SetHdmiVideoFmt(vpp_handle, OUTPUT_COLOR_FMT_RGB888, OUTPUT_BIT_DEPTH_8BIT, 1);
	MV_VPPOBJ_ConfigAudio(vpp_handle, 2, 48000, 128, -1, 100);
	printf("end set video format\n");
	return 0;
}
//
HDMI_P1_CMD(mv, i)
{
	MV_VPPOBJ_SetHDMIMute(vpp_handle, i);
	printf("finish mute hdmi video\n");
	return 0;
}
HDMI_P0_CMD(dish)
{
	MV_VPPOBJ_DisableHdmi(vpp_handle);
	return 0;
}

HDMI_P1_CMD(test1, i)
{
	printf("test 1 %d \n", i);
	return 0;
}

#if defined(MMP2_DEBUG)
HDMI_P1_CMD(d1h, i)
{
	printf("offset 0x%x is 0x%x\n", i, hdmi_read(hdmi_base, i));
}

HDMI_P2_CMD(w1h, i, value)
{
	printf("before write, offset 0x%x is 0x%x\n", i, hdmi_read(hdmi_base, i));
	printf("offset 0x%x is set to 0x%x\n", i, value);
	hdmi_write(hdmi_base, i, value);
	printf("after write, offset 0x%x is 0x%x\n", i, hdmi_read(hdmi_base, i));
}

HDMI_P0_CMD(dh)
{
	int i;

	printf("************direct register*******************\n");
	for (i = 0x8; i <= 0x2c; i += 4)
		printf("direct offset 0x%x is 0x%x\n", i, hdmi_direct_read(hdmi_base, i));
	printf("************indirect register*******************\n");
	for (i = 0; i < 0x13e; i++)
		printf("offset 0x%x is 0x%x\n", i, hdmi_read(hdmi_base, i));
}
#endif

hdmi_cmd_desc hdmi_cmd[] = 
{
    //HDMI_CMD(test, 0, "test"),
    HDMI_CMD(mv, 1, "mute video"),
    HDMI_CMD(sv, 1, "set video format"),
    HDMI_CMD(dish, 0, "disable hdmi register"),
    HDMI_CMD(test1, 1, "test"),
#if defined(MMP2_DEBUG)
    HDMI_CMD(d1h, 1, "dump 1 hdmi register"),
    HDMI_CMD(dh, 0, "dump hdmi register"),
    HDMI_CMD(w1h, 2, "write 1 hdmi register"),
#endif
    HDMI_CMD_END
};


void console_task(void)
{
    char buf[100];
    char *pBuf;
    int length;
    int i;
    int para_count;
    int para[4];
    hdmi_cmd_desc *pcmd;
    printf("console\n");
    while(1)
    {
        printf(">>");
       usleep(100);
        gets(buf);
        if(buf[0] == 0)
            continue;
        if(buf[0] == 'q' && (buf[1] < 'a' || buf[1] > 'z'))
            return;
        pBuf = NULL;
        for(i = 0; i < 100 && buf[i] != 0; i++)
        {
            if(pBuf == NULL)
                if(buf[i] >= 'a' && buf[i] <= 'z')
                    pBuf = &buf[i];
                else
                    continue;
            else
                if(!((buf[i] >= 'a' && buf[i] <= 'z') || (buf[i] >= '0' && buf[i] <= '9')))
                {
                    break;
                }
                else
                {
                    continue;       
                }
        }
        
        if(pBuf == NULL)
            continue;
        length = (int)(&buf[i] - pBuf);
        pcmd = hdmi_cmd;
        while(pcmd->func)
        {
            if(strncmp(pcmd->name, pBuf, length) == 0)
            {
                break;
            }
            else
                pcmd++;
        }
        if(NULL == pcmd->func)
            continue;
        pBuf = &buf[i];
        for(para_count = 0; para_count < pcmd->para_count; para_count++)
        {
            if(pBuf >= &buf[99] || *pBuf == 0)
            {
                printf("error param\n");
                pBuf = NULL;
                break;
            }
            para[para_count] = strtoul(pBuf, &pBuf, 0);   
        }
        if (pBuf == NULL)
            continue;
        if(para_count != pcmd->para_count)
        {
            printf("parameter number should be %d", pcmd->para_count);
            continue;
        }
        switch(pcmd->para_count)
        {
            case 0:
                pcmd->func();
                break;
            case 1:
                ((pFunc1)(pcmd->func))(para[0]);
                break;
            case 2:
                ((pFunc2)(pcmd->func))(para[0], para[1]);
                break;
            case 3:
                ((pFunc3)(pcmd->func))(para[0], para[1], para[2]);
                break;
            case 4:
                ((pFunc4)(pcmd->func))(para[0], para[1], para[2], para[3]);
                break;
        }

    }
}

int main()
{
	int Res;

	if (MV_VPP_Init("/dev/fb1") < 0)
		MV_VPP_Init("/dev/graphics/fb1");
	MV_VPPOBJ_Create(&vpp_handle);
	MV_VPPOBJ_Reset(vpp_handle);// do a hotplghandle
	MV_VPPOBJ_Config(vpp_handle);// init 2 hpd thread and hdmi thread
	if (MV_VPPOBJ_GetHDMIConnectStatus(vpp_handle) == 1) {
		printf("hdmi already connected !\n");
		m_HdmiRxConnected = 1;
	// read HDMI sink capability
            MV_VPPOBJ_GetHDMISinkCaps(vpp_handle, &m_SinkCaps);
            if (m_SinkCaps.validEdid)
            {
		getPreferredResolution(&Res);//get edid device preferred resolution
		MV_VPPOBJ_ConfigOutputRes(vpp_handle, Res);
		MV_VPPOBJ_SetHdmiVideoFmt(vpp_handle, OUTPUT_COLOR_FMT_RGB888, OUTPUT_BIT_DEPTH_8BIT, 1);
		MV_VPPOBJ_ConfigAudio(vpp_handle, 2, 48000, 128, -1, 100);

		MV_VPPOBJ_PrintEdidInfo(vpp_handle);//print edid info
            }

	}

	MV_VPPOBJ_RegisterHdmiCallback (vpp_handle, 0, HdmiEvtCB, NULL);

	console_task();
	return 0;
}
