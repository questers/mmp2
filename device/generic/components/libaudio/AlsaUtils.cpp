#include <utils/String8.h>
#include <AudioLog.h>
#include "AlsaUtils.h"

namespace android {


int ALSA_UTILS::MixerCSet(const char* sentence)
{
    return ALSA_UTILS::MixerCSet("default",sentence);
}

int ALSA_UTILS::MixerCSet(const char* card,const char* sentence)
{
#define AMIXER_SET_START_STRING "amixer cset <"
#define MAX_AMIXER_CSET_SENTENCE_LENGTH 60
    snd_ctl_t *handle;
    int err;
    snd_ctl_elem_id_t *id;
    snd_ctl_elem_value_t *control;
    unsigned int arg1,arg2;
    unsigned int argnum;
    const char *pstr;
    char *ptmp;
    char name[MAX_AMIXER_CSET_SENTENCE_LENGTH];
    DBG("alsa command <%s>\n",sentence);
    if(strlen(sentence)>=(MAX_AMIXER_CSET_SENTENCE_LENGTH+strlen(AMIXER_SET_START_STRING)))
    {
        ERROR("AmixerCSet:sentence exceeds the length limit\n");
        return -1;
    }
    
    if(strncmp(AMIXER_SET_START_STRING,sentence,strlen(AMIXER_SET_START_STRING)))
    {
        ERROR("alsa command[%s] not recognized\n",sentence);
        return -1;
    }
    
    pstr=sentence+strlen(AMIXER_SET_START_STRING);
    if(strchr(pstr,'>')==NULL)
    {
        ERROR("AmixerCSet: invalid control name\n");
        return -2;
    }
    ptmp=name;
    while(*pstr!='>')
        *ptmp++=*pstr++;
    *ptmp=0;
    if(strchr(pstr,','))
    {
        sscanf(pstr,"> %d,%d",&arg1,&arg2);
        argnum=2;
    }
    else
    {
        sscanf(pstr,"> %d",&arg1);
        argnum=1;
    }
    
    snd_ctl_elem_id_alloca(&id);
    snd_ctl_elem_value_alloca(&control);    
    snd_ctl_elem_id_set_interface(id, SND_CTL_ELEM_IFACE_MIXER);
    err = snd_ctl_open(&handle, card, 0);
    
    if (err < 0) {
        DBG("Control default open error: %s\n", snd_strerror(err));
        return -3;
    }
    
    snd_ctl_elem_id_set_name(id,name);
    snd_ctl_elem_value_set_integer(control, 0, arg1);
    if(argnum==2)
        snd_ctl_elem_value_set_integer(control, 1, arg2);   
    snd_ctl_elem_value_set_id(control, id); 
    err = snd_ctl_elem_write(handle, control);
    if (err < 0) {
        DBG("element write error: name=%s, %s", name,snd_strerror(err));
        return -4;
    }
    
    snd_ctl_close(handle);

    return 0;
    


}

int ALSA_UTILS::XRun_Recovery(snd_pcm_t * handle, int err)
{
    if (err == -EPIPE) {          
		snd_pcm_status_t* status;		
		snd_pcm_status_alloca(&status);
		snd_pcm_status(handle,status);
		if (snd_pcm_status_get_state(status) == SND_PCM_STATE_XRUN) 
		{
			struct timeval now, diff, tstamp;
			snd_pcm_stream_t stream = snd_pcm_stream(handle);		
			gettimeofday(&now, 0);
			snd_pcm_status_get_trigger_tstamp(status, &tstamp);
			timersub(&now, &tstamp, &diff);
			WARN("%s!!! (at least %.3f ms long)\n",
				SND_PCM_STREAM_PLAYBACK == stream? "underrun": "overrun",
				diff.tv_sec * 1000 + diff.tv_usec / 1000.0);
		} 
        err = snd_pcm_prepare (handle);
        if (err < 0) {
            return err;
        }
        return 0;
    } else if (err == -ESTRPIPE) {
        while ((err = snd_pcm_resume (handle)) == -EAGAIN)
            usleep (10);           /* wait until the suspend flag is released */
        if (err < 0) {
            err = snd_pcm_prepare (handle);
            if (err < 0) {
                return err;
            }
        }
        return 0;
    }
    return err;
}


int ALSA_UTILS::SoundCardCheck(String8& cardname)
{
    int status = ~NO_ERROR;    
	snd_ctl_t *handle;
	int card, err, dev, idx;
	snd_ctl_card_info_t *info;
	snd_pcm_info_t *pcminfo;
	snd_ctl_card_info_alloca(&info);

	card = -1;
	if (snd_card_next(&card) < 0 || card < 0) {
		ERROR("no soundcards found...");
		return status;
	}
	while (card >= 0) {
		char name[32];
		sprintf(name, "hw:%d", card);
		if ((err = snd_ctl_open(&handle, name, 0)) < 0) {
			ERROR("control open (%i): %s", card, snd_strerror(err));
			goto next_card;
		}
		if ((err = snd_ctl_card_info(handle, info)) < 0) {
			ERROR("control hardware info (%i): %s", card, snd_strerror(err));
			snd_ctl_close(handle);
			goto next_card;
		}        

        
        ALWAYS("sound card %i: %s [%s] founded\n",
            card, snd_ctl_card_info_get_id(info), snd_ctl_card_info_get_name(info));
        snd_ctl_close(handle);

        cardname.setTo(snd_ctl_card_info_get_name(info));
        // 1 sound card is enough
        status = NO_ERROR;
        break;
        
        next_card:
            if (snd_card_next(&card) < 0) {
                ERROR("snd_card_next");
                break;
            }
    }


    return status;
    
}

};

