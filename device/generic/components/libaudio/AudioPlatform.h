#ifndef _AUDIO_PLATFORM_H
#define _AUDIO_PLATFORM_H

#include <utils/Errors.h>
#include <media/AudioSystem.h>

namespace android
{

class AudioPlatform
{
    public:
        enum audio_aec_mode{
            AEC_MODE_INVALID=-1,
            AEC_MODE_PCM_IN_PCM_OUT=0,
            AEC_MODE_ANALOG_IN_ANALOG_OUT,
            AEC_MODE_DAC_IN_DAC_OUT,
            AEC_MODE_DISABLED,
        };
	public:
    AudioPlatform(const char* platform);
	virtual ~AudioPlatform();


	virtual status_t setModeImpl(int mode)=0;
	virtual status_t setAecImpl(int mode)=0;
    virtual status_t Init()=0;
	virtual status_t PreDevEnable(uint32_t devices,bool enable);
	virtual status_t PostDevEnable(uint32_t devices,bool enable);
    //simple API to call  AEC function
    void setAecEnable(bool enable=true){setAecImpl((true==enable)?AEC_MODE_PCM_IN_PCM_OUT:AEC_MODE_DISABLED);}
	
	const char* Name(){return mPlatform;}	
	bool IsVirtual(){return mbVirtual;}
	void SetVirtual(bool bVirtual){mbVirtual = bVirtual;}

    /*
	int AecMode(int aec=AudioPlatform::AEC_MODE_INVALID){
	    int old=mAecMode;  
	    if(AudioPlatform::AEC_MODE_INVALID!=aec) 
	        mAecMode = aec;
        return old;
    }
	int SceneMode(int scene=AudioSystem::MODE_INVALID){
	    int old = mSceneMode;
	    if(AudioSystem::MODE_INVALID!=scene) 
	        mSceneMode = scene; 
	    return old;
	}*/
	protected:
		const char* mPlatform;
		bool mbVirtual;
		int mAecMode;
		int mSceneMode;
};


};

#endif


