/*
 *  Copyright 2012 Quester Technology,Inc.
 *  ALL RIGHTS RESERVED
 *
 */

#ifndef _AUDIO_PLATFORM_STUB_H
#define _AUDIO_PLATFORM_STUB_H


namespace android
{

class AudioPlatform;

class AudioPlatformStub:public AudioPlatform
{
	public:
		AudioPlatformStub();
	virtual ~AudioPlatformStub();


	virtual status_t setModeImpl(int mode);
	virtual status_t setAecImpl(int mode);
    virtual status_t Init();	

};


};

#endif



