/*
 *  Copyright 2012 Quester Technology,Inc.
 *  ALL RIGHTS RESERVED
 *
 */

#ifndef _AUDIO_PLATFORM_RT5625_H
#define _AUDIO_PLATFORM_RT5625_H

#include <utils/KeyedVector.h>

namespace android
{


class String8;
class AudioParameter;
class AudioPlatform;
class AudioPlatformRT5625:public AudioPlatform
{
	public:
		AudioPlatformRT5625();
	virtual ~AudioPlatformRT5625();


    virtual status_t Init();
	virtual status_t setModeImpl(int mode);
	virtual status_t setAecImpl(int mode);
	
	virtual status_t PreDevEnable(uint32_t devices,bool enable);
	virtual status_t PostDevEnable(uint32_t devices,bool enable);

    private:
        bool mbEarpieceShouldEnable;

    class OnBoardSpeaker:public AudioDevice{
        public:
            OnBoardSpeaker();
            virtual ~OnBoardSpeaker(){}
            virtual int SetVolume(float l,float r);
            virtual int Enable(bool enable);
            virtual int Suspend(bool suspend);
            virtual void Init();
			
	};
    
    class OnBoardMicrophone:public AudioDevice{
        public:
            OnBoardMicrophone();
            virtual ~OnBoardMicrophone(){}
            virtual int SetVolume(float l,float r);
            virtual int Enable(bool enable);
            virtual int Suspend(bool suspend);
            virtual void Init();                
    };
    
    
    class HeadSetSpeaker:public AudioDevice{
        public:
            HeadSetSpeaker(const char*,const char*);
            HeadSetSpeaker();
            virtual ~HeadSetSpeaker(){}
            virtual int SetVolume(float l,float r);
            virtual int Enable(bool enable);
            virtual int Suspend(bool suspend);
            virtual void Init();
    };
    
    class HeadSetMicrophone:public AudioDevice{
        public:
            HeadSetMicrophone();
            virtual ~HeadSetMicrophone(){}
            virtual int SetVolume(float l,float r);
            virtual int Enable(bool enable);
            virtual int Suspend(bool suspend);
            virtual void Init();
    };
    
    class HeadPhoneSpeaker:public HeadSetSpeaker{
        public:
            HeadPhoneSpeaker();
            virtual ~HeadPhoneSpeaker(){}
            virtual int SetVolume(float l,float r);
            virtual int Enable(bool enable);
            virtual int Suspend(bool suspend);
    };
    
    
    class BluetoothSpeaker:public AudioDevice{
        public:
            BluetoothSpeaker();
            virtual ~BluetoothSpeaker(){}
            virtual int SetVolume(float l,float r);
            virtual int Enable(bool enable);
            virtual int Suspend(bool suspend);
    };
    
    class BluetoothMicrophone:public AudioDevice{
        public:
            BluetoothMicrophone();
            virtual ~BluetoothMicrophone(){}
            virtual int SetVolume(float l,float r);
            virtual int Enable(bool enable);
            virtual int Suspend(bool suspend);
            virtual void Init();
    };      
    
    class FMSpeaker:public AudioDevice{
        public:
            FMSpeaker();
            virtual ~FMSpeaker(){}
            virtual int SetVolume(float l,float r);
            virtual int Enable(bool enable);
            virtual int Suspend(bool suspend);
    };
    
    class FMHeadPhone:public AudioDevice{
        public:
            FMHeadPhone();
            virtual ~FMHeadPhone(){}
            virtual int SetVolume(float l,float r);
            virtual int Enable(bool enable);
            virtual int Suspend(bool suspend);
    };
	
};


};

#endif



