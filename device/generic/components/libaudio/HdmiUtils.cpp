#include <binder/IServiceManager.h>
#include <binder/IPCThreadState.h>
#include "IHdmiService.h"

#include "HdmiUtils.h"

namespace android {
int HDMI_UTILS::HdmiDisplayMode()
{
  int displaymode=DISPLAY_MODE_INVALID;
  sp<IServiceManager> sm = defaultServiceManager();
  sp<IBinder> binder = sm->getService(String16("HdmiService"));   
  sp<IHdmiService> sHdmiService = 0; 
  if( binder != 0 ) {
      sHdmiService = interface_cast<IHdmiService>(binder);
  }
  if( sHdmiService.get() ) {
      displaymode =  sHdmiService->get_display_mode();
  }
  return displaymode;        
  
}

int HDMI_UTILS::HdmiSetAudioMute(bool mute)
{
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder = sm->getService(String16("HdmiService"));   
    sp<IHdmiService> sHdmiService = 0; 
    if( binder != 0 ) {
        sHdmiService = interface_cast<IHdmiService>(binder);
    }
    if( sHdmiService.get() ) {
        return sHdmiService->set_audio_mute(mute);
    }

    return -1;
}

bool HDMI_UTILS::HdmiServerRunning()
{
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder = sm->checkService(String16("HdmiService"));   
    return (binder!=0)?true:false;
}



};

