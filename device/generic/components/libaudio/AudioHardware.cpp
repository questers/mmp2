/*
 * (C) Copyright 2012 Quester Technology, Inc.
 * All Rights Reserved
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 */
#include <arch/linux-arm/AndroidConfig.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <utils/String8.h>
#include <media/AudioSystem.h>
#include <hardware_legacy/power.h>

#include "AudioLog.h"
#include "AudioSetting.h"
#include "AudioHardware.h"
#include "AlsaUtils.h"
#include "AudioStreamDump.h"
#include "AudioDevice.h"
#include "AudioDeviceManager.h"

namespace android {


// ----------------------------------------------------------------------------

AudioHardwareMarvell::AudioHardwareMarvell() : 
mMicMute(false),
mOutput(0), 
mInput(0)
{
    mMode = AudioSystem::MODE_NORMAL;
    mVoiceVolume = 0.9f;
}

AudioHardwareMarvell::~AudioHardwareMarvell()
{
    VOICECALL_END();
    if(mOutput)
        delete mOutput;
    if(mInput)
        delete mInput;
}

status_t AudioHardwareMarvell::initCheck()
{
    DBG("AudioHardwareMarvell::initCheck\n");
    return DEV_CHECK(1);
}

AudioStreamOut* AudioHardwareMarvell::openOutputStream(
        uint32_t devices, int *format, uint32_t *channels, uint32_t *sampleRate, status_t *status)
{
	AutoMutex lock(&mLock);
    // only one output stream allowed
    if (mOutput) {
        if(status) {
            *status = INVALID_OPERATION;
        }
        return NULL;
    }

    INFO("AudioHardwareMarvell::openOutputStream(%d)\n", devices);
    // create new output stream
    AudioStreamOutMarvell* out = new AudioStreamOutMarvell();
    if (out->set(this, devices, format, channels, sampleRate) != NO_ERROR) {
        *status = UNKNOWN_ERROR;
        delete out;
        return NULL;
    }

    mOutput = out;
    if(status) {
        *status = OK;
    }
    return mOutput;
}

void AudioHardwareMarvell::closeOutputStream(AudioStreamOut* out) 
{	
    if (mOutput && out == mOutput) {
        delete mOutput;
        mOutput = 0;
    }
}

AudioStreamIn* AudioHardwareMarvell::openInputStream(
        uint32_t devices, int *format, uint32_t *channels, uint32_t *sampleRate,
        status_t *status, AudioSystem::audio_in_acoustics acoustics)
{
    // check for valid input source
    if (!AudioSystem::isInputDevice((AudioSystem::audio_devices)devices)) {
        return 0;
    }

	AutoMutex lock(&mLock);

    // only one input stream allowed
    if (mInput) {
		ERROR("only 1 input stream allowed");
        if(status) {
            *status = INVALID_OPERATION;
        }
        return NULL;
    }

    INFO("AudioHardwareMarvell::openInputStream\n");
    // create new input stream
    AudioStreamInMarvell* in = new AudioStreamInMarvell();
    if (in->set(this, devices, format, channels, sampleRate, acoustics) != NO_ERROR) {
        delete in;
        return NULL;
    }

    mInput = in;
    if(status) {
        *status = OK;
    }
    return mInput;	
}

void AudioHardwareMarvell::closeInputStream(AudioStreamIn* in) 
{
    if (mInput && in == mInput) {
        delete mInput;
        mInput = 0;
    }
}

status_t AudioHardwareMarvell::setParameters(const String8& keyValuePairs)
{
    DBG("audio->setparameter %s", keyValuePairs.string());
	
    AudioParameter param = AudioParameter(keyValuePairs);
    String8 value;
    int intvalue;


    if (keyValuePairs.length() == 0) return BAD_VALUE;

	//routing    
    if (param.getInt(String8(AudioParameter::keyRouting),intvalue) == NO_ERROR) 
	{
		DEV_ENABLE(intvalue,true);		
    }

    
    return NO_ERROR;
}

status_t AudioHardwareMarvell::setVoiceVolume(float volume)
{
	INFO("audio->setvoicevolume %f",volume);
    mVoiceVolume = volume;
    
	if(mMode==AudioSystem::MODE_IN_CALL)
	{
		//DEV_VOLUME(AudioSystem::DEVICE_OUT_ALL,volume,volume);

		//Kylin-397 ,voice volume adjustment should not affect remote earpiece
		DEV_VOLUME(AudioSystem::DEVICE_IN_ALL,volume,volume);
	}
	
	VOICECALL_SETVOL(mVoiceVolume);
    return NO_ERROR;
}

status_t AudioHardwareMarvell::setMasterVolume(float volume)
{
	INFO("audio->setmastervolume %f",volume);
	mMasterVolume=volume;
	
//	if(mMode!=AudioSystem::MODE_IN_CALL)
//	{
//		DEV_VOLUME(AudioSystem::DEVICE_OUT_ALL,volume,volume);
//		DEV_VOLUME(AudioSystem::DEVICE_IN_ALL,volume,volume);
//	}

    // return error - software mixer will handle it
    return INVALID_OPERATION;
}


status_t AudioHardwareMarvell::setMicMute(bool state) 
{
    DBG("AudioHardwareMarvell::setMicMute(%s)\n",state?"true":"false"); 
    mMicMute = state;  
	
	DEV_MUTE(state);
    return  NO_ERROR; 
}

status_t AudioHardwareMarvell::getMicMute(bool* state) 
{ 
    *state = mMicMute ; 
    return NO_ERROR; 
}

status_t AudioHardwareMarvell::dumpInternals(int fd, const Vector<String16>& args)
{
    const size_t SIZE = 256;
    char buffer[SIZE];
    String8 result;
    result.append("AudioHardwareMarvell::dumpInternals\n");
    snprintf(buffer, SIZE, "\tmMicMute: %s\n", mMicMute? "true": "false");
    result.append(buffer);
    ::write(fd, result.string(), result.size());
    return NO_ERROR;
}

status_t AudioHardwareMarvell::dump(int fd, const Vector<String16>& args)
{
    dumpInternals(fd, args);
	if(mOutput)
		mOutput->dump(fd,args);
	if(mInput)
		mInput->dump(fd,args);
    return NO_ERROR;
}

// default implementation
size_t AudioHardwareMarvell::getInputBufferSize(uint32_t sampleRate, int format, int channelCount)
{
    if (format != AudioSystem::PCM_16_BIT) {
        WARN("getInputBufferSize bad format: %d", format);
        return 0;
    }

    //return 160;//10ms*8*2 = 160

    return ((AudioSettings::DEFAULT_INPUT_PERIOD_TIME * sampleRate) / 1000000) * sizeof(uint16_t)*channelCount;
}

status_t AudioHardwareMarvell::setMode(int mode)
{
	status_t ret;
    int old_mode = mMode;

	ret=AudioHardwareBase::setMode(mode);

	if(ret!=NO_ERROR)
		return ret;

		
    DBG("setMode(%d)->(%d)", old_mode,mode);
    DEV_MODE(mode);
	//if(((mode==AudioSystem::MODE_IN_CALL)&&(old_mode!=AudioSystem::MODE_RINGTONE))||(mode==AudioSystem::MODE_RINGTONE))
	if(mode==AudioSystem::MODE_IN_CALL)
	{
        if(mOutput){
            mOutput->resume();
        }
		DEV_VOLUME(AudioSystem::DEVICE_OUT_ALL,mVoiceVolume,mVoiceVolume);
		DBG("enter call mode");
        VOICECALL_START();
	}
	else if(mode==AudioSystem::MODE_NORMAL)
	{
		DBG("leave call mode");	
        VOICECALL_END();

		DEV_VOLUME(AudioSystem::DEVICE_OUT_ALL,mMasterVolume,mMasterVolume);
		DEV_VOLUME(AudioSystem::DEVICE_IN_ALL,mMasterVolume,mMasterVolume);
	}
	return NO_ERROR;
}
int AudioHardwareMarvell::getMode(void)
{

    return mMode;
}



// ----------------------------------------------------------------------------

AudioStreamOutMarvell::AudioStreamOutMarvell():
mAudioHardware(0),
mHandle(0),
mUserFlag(0),
mPowerLock(false)
{
    mFormat = AudioSystem::PCM_16_BIT;
    mSampleRate = AudioSettings::DEFAULT_OUTPUT_SAMPLE_RATE;
    mChannel = AudioSystem::CHANNEL_OUT_STEREO;
    mChannelCount = AudioSystem::popCount(mChannel);
    mBufferTime = AudioSettings::DEFAULT_OUTPUT_BUFFER_TIME;
    mPeriodTime = AudioSettings::DEFAULT_OUTPUT_PERIOD_TIME;
	mBufferSize = (mBufferTime * mSampleRate) / 1000000;
    mPeriodSize = (mPeriodTime * mSampleRate) / 1000000;
}

AudioStreamOutMarvell::~AudioStreamOutMarvell()
{
    if(mHandle) {
		DEV_ENABLE(mDevice,false);
        snd_pcm_close(mHandle);
        mHandle = NULL;
    }
    if (mPowerLock) 
    {
        release_wake_lock ("AudioOutLock");        
        mPowerLock = false;    
    }

}
status_t AudioStreamOutMarvell::connect2hardware(void)
{
     int err;
     err = snd_pcm_open(&mHandle, DEV_NAME(mDevice), SND_PCM_STREAM_PLAYBACK, 0);
     if (err < 0) {
    	 ERROR("snd_pcm_open failed [%s",snd_strerror(err));
    	 return BAD_VALUE;
     }


     unsigned int requestedBufferTime = mBufferTime;
     unsigned int requestedPeriodTime = mPeriodTime;
     snd_pcm_hw_params_t *hw_params;	 
     snd_pcm_sw_params_t *sw_params;

     //allocate params
     snd_pcm_hw_params_alloca(&hw_params);
     snd_pcm_sw_params_alloca(&sw_params);

     //choose all params
     err = snd_pcm_hw_params_any(mHandle, hw_params);
     if(err<0)
     {
    	 ERROR("set hw_params: choose config failed[%s]",snd_strerror(err));
    	 return BAD_VALUE;
     }

     /* Set the desired hardware parameters. */  
     err = snd_pcm_hw_params_set_access(mHandle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
     if(err<0)
     {
    	 ERROR("set hw_params: set access failed[%s]",snd_strerror(err));
    	 return BAD_VALUE;
     }
     
     err = snd_pcm_hw_params_set_format(mHandle, hw_params, SND_PCM_FORMAT_S16_LE);
     if(err<0)
     {
    	 ERROR("set hw_params: set format failed[%s]",snd_strerror(err));
    	 return BAD_VALUE;
     }
     
     err = snd_pcm_hw_params_set_channels(mHandle, hw_params, mChannelCount);
     if(err<0)
     {
    	 ERROR("set hw_params: set channels failed[%s]",snd_strerror(err));
    	 return BAD_VALUE;
     }

     err = snd_pcm_hw_params_set_rate_near(mHandle, hw_params, &mSampleRate, NULL);
     if(err<0)
     {
    	 ERROR("set hw_params: set rate near failed[%s]",snd_strerror(err));
    	 return BAD_VALUE;
     }

     INFO("rate eventually set as %d", mSampleRate);

     err = snd_pcm_hw_params_set_rate_resample(mHandle, hw_params, 0);
     if( err<0 ){
    	 WARN("set hw_params: unable to disable hw resampling");
     }

     err = snd_pcm_hw_params_set_buffer_time_near(mHandle, hw_params, &mBufferTime, 0);
     if( err<0 ){
    	 WARN("set hw_params: Unable to set buffer time to %d usec", requestedBufferTime);
     }	 

     err = snd_pcm_hw_params_get_buffer_size(hw_params, &mBufferSize);												   
     if (err < 0) { 																						   
    		 ERROR("Unable to get buffer size for playback: %s\n", snd_strerror(err));						   
    		 return BAD_VALUE;																					   
     }																										   

     INFO("buffer time eventually set as %d usec", mBufferTime);

     
     err = snd_pcm_hw_params_set_period_time_near(mHandle,hw_params, &mPeriodTime,0);
     if( err<0 ){
    	 ERROR("set hw_params: Unable to set period time to %u usec", requestedPeriodTime);
     }

     err = snd_pcm_hw_params_get_period_size(hw_params, &mPeriodSize, 0);										   
     if (err < 0) { 																						   
    		 ERROR("get period size failed[%s]\n", snd_strerror(err));						   
    		 return BAD_VALUE;																					   
     }												 
     
     INFO("period time eventually set as %d usec, period size %d frames", mPeriodTime, (int)mPeriodSize);

     /* Write the parameters to the driver */
     err = snd_pcm_hw_params(mHandle, hw_params);
     if (err<0) {
    	 ERROR("set hw_params: unable to set hw parameters[%s]", snd_strerror(err));
    	 return BAD_VALUE;
     }

     /* soft params */
     err = snd_pcm_sw_params_current(mHandle, sw_params);
     if(err<0){
    	 ERROR("failed to fetch alsa current sw params[%s]",snd_strerror(err));
    	 return BAD_VALUE;
     }
     
     err = snd_pcm_sw_params_set_start_threshold(mHandle, sw_params, mBufferSize-mPeriodSize);
     if( err<0 ){
    	 ERROR("set sw_params:set start threshold to %lu frames failed[%s]", mBufferSize-mPeriodSize,snd_strerror(err));
    	 return BAD_VALUE;
    	 
     }
    // Stop the transfer when the buffer is almost full.
    err = snd_pcm_sw_params_set_stop_threshold(mHandle, sw_params, mBufferSize);
    if(err<0 ){
    	 WARN("set sw_params:set stop threshold to %lu frames failed[%s]", mBufferSize,snd_strerror(err));
    	 return BAD_VALUE;
    }
    // Allow the transfer to start when at least periodSize samples can be
    // processed.
    err = snd_pcm_sw_params_set_avail_min(mHandle, sw_params, mPeriodSize);
    if(err<0 ){
    	 ERROR("set sw_params: set available minimum to %lu failed[%s]", mPeriodSize,snd_strerror(err));
    	 return BAD_VALUE;
    }

     err = snd_pcm_sw_params(mHandle, sw_params);
     if(err<0){
    	 ERROR("set sw_params:set software params failed[%s]",snd_strerror(err));
    	 return BAD_VALUE;
    }

    //DEV_ENABLE(mDevice,true);
    return NO_ERROR;
    
}
status_t AudioStreamOutMarvell::set(
        AudioHardwareMarvell *hw,
        uint32_t devices,
        int *pFormat,
        uint32_t *pChannels,
        uint32_t *pRate)
{
	AutoMutex lock(&mLock);
     int err;    
	 int Format = pFormat ? *pFormat : 0;
	 uint32_t Channels = pChannels ? *pChannels : 0;
	 uint32_t Rate = pRate ? *pRate : 0;

	 // fix up defaults
	 if (Format == 0) Format = mFormat;
	 if (Channels == 0) Channels = mChannel;
	 if (Rate ==0) Rate = mSampleRate;

	 // check values
	 if ((Format != mFormat) ||
	 	 (Channels!= mChannel)||
		 (Rate != mSampleRate)) {
		 if(pFormat) *pFormat = mFormat;
		 if(pChannels) *pChannels = mChannel;
		 if(pRate) *pRate = mSampleRate;
		 DBG("AudioStreamOutMarvell set error params");	 
		 
		 return BAD_VALUE;
	 }
	 
	 mChannelCount = AudioSystem::popCount(Channels);
     mAudioHardware = hw;
     mDevice = devices;

	 INFO("audiohardware output set format[%d] channels[%d],rate[%d],device[0x%x]",
        Format,
        mChannelCount,
        Rate,
        mDevice);  
    err = connect2hardware();
    if (pFormat) *pFormat = Format;
    if (pChannels) *pChannels = Channels;
    if (pRate) *pRate = Rate;

	return err;

}

status_t AudioStreamOutMarvell::standby()
{
	AutoMutex lock(&mLock);

	INFO("audioout->standby  modify");
	//DEV_STANDBY(AudioSystem::DEVICE_OUT_ALL,true);

    mUserFlag++;

    if(mHandle)
    {    
        if(mAudioHardware&&(AudioSystem::MODE_IN_CALL!=mAudioHardware->getMode())){
    		DEV_ENABLE(mDevice,false);
            snd_pcm_close(mHandle);
            mHandle = NULL;
        }
    }
    if (mPowerLock) 
    {
    
	INFO("audioout->standby mPowerLock ");
        release_wake_lock ("AudioOutLock");        
        mPowerLock = false;    
    }

    return NO_ERROR;
}



status_t AudioStreamOutMarvell::resume(void)
{

    if(mUserFlag>0&&!mHandle)
    {
        DBG("AudioStreamOutMarvell::resume");

        //pause
        connect2hardware();

        mUserFlag--;
        usleep(200000);
        
        DEV_ENABLE(mDevice,true);
        
    }
	
	return NO_ERROR;
}

status_t AudioStreamOutMarvell::setVolume(float left, float right)
{

	INFO("audioout->setVolume %f,%f",left,right);
	
	//DEV_VOLUME(AudioSystem::DEVICE_OUT_ALL,left,right);
    return NO_ERROR;
}

status_t AudioStreamOutMarvell::setParameters(const String8& keyValuePairs)
{
    DBG("audioout->setparameter %s", keyValuePairs.string());
    AudioParameter param = AudioParameter(keyValuePairs);
    String8 key = String8(AudioParameter::keyRouting);
    status_t status = NO_ERROR;
    int device=0;
    int mode = mAudioHardware->getMode();

	// routing key supported	
    if (param.getInt(key, device) == NO_ERROR) 
	{            
        if (!device) {
            DEV_ENABLE(mDevice,false);                
            
        }else {
            if(mDevice!=device)   
           {
                int disabledevices = mDevice&(~device);
                int enabledevices = device&(~mDevice);
                DEV_ENABLE(disabledevices,false);                 
                DEV_ENABLE(enabledevices,true);            
                mDevice = device; 
           }
        }
		param.remove(key);
    }

    if (param.size()) {
        status = BAD_VALUE;
    }
    return status;
}

String8 AudioStreamOutMarvell::getParameters(const String8& keys)
{

    AudioParameter param = AudioParameter(keys);
    String8 value;
    String8 key = String8(AudioParameter::keyRouting);

    if (param.get(key, value) == NO_ERROR) {
        param.addInt(key, (int)mDevice);
    }

    return param.toString();
}

ssize_t AudioStreamOutMarvell::write(const void* buffer, size_t bytes)
{
	AutoMutex lock(&mLock);

	//INFO("AudioStreamOutMarvell::write %u bytes",bytes);

    DownStreamDumper::getInstance().StreamDump(buffer,bytes);

	resume();
    
    if(!mHandle)
    {
        // fake timing for audio output
        usleep(bytes * 1000000 / sizeof(int16_t) / AudioSystem::popCount(channels()) / sampleRate());
        return (ssize_t)bytes;
    }

    if(!mPowerLock)
    {
        acquire_wake_lock (PARTIAL_WAKE_LOCK, "AudioOutLock");
        mPowerLock = true;        
    }

    
    int frames = snd_pcm_bytes_to_frames(mHandle,bytes);
    int ret;
    int8_t *ptr = (int8_t*)buffer;


    while(frames > 0)
    {
        ret = snd_pcm_writei(mHandle, ptr, frames);  
		if(ret == -EAGAIN) continue;
		if(ret < 0) 	
		{
            if (ALSA_UTILS::XRun_Recovery(mHandle, ret) < 0) 
			{
				ERROR("alsa xrun write failed"); 
				break;
			}
			continue;
		}		
    
        ptr += snd_pcm_frames_to_bytes(mHandle,ret);;
        frames -= ret;
    }
    return (ssize_t)bytes;
}

status_t AudioStreamOutMarvell::dump(int fd, const Vector<String16>& args)
{

    const size_t SIZE = 256;
    char buffer[SIZE];
    String8 result;
    snprintf(buffer, SIZE, "AudioStreamOutMarvell::dump\n");
    snprintf(buffer, SIZE, "\tsample rate: %d\n", sampleRate());
    snprintf(buffer, SIZE, "\tbuffer size: %d\n", bufferSize());
    snprintf(buffer, SIZE, "\tchannel count: %d\n", channels());
    snprintf(buffer, SIZE, "\tformat: %d\n", format());
    result.append(buffer);
    ::write(fd, result.string(), result.size());
    return NO_ERROR; 
}

// ----------------------------------------------------------------------------

AudioStreamInMarvell::AudioStreamInMarvell():
mAudioHardware(0),
mHandle(0)
{

    mFormat = AudioSystem::PCM_16_BIT;
    mSampleRate = AudioSettings::DEFAULT_INPUT_SAMPLE_RATE;
    mChannel = AudioSystem::CHANNEL_IN_MONO;
    mChannelCount = AudioSystem::popCount(mChannel);
    mBufferTime = AudioSettings::DEFAULT_INPUT_BUFFER_TIME;
    mPeriodTime = AudioSettings::DEFAULT_INPUT_PERIOD_TIME;
    mBufferSize = (mBufferTime * mSampleRate) / 1000000;
    mPeriodSize = (mPeriodTime * mSampleRate) / 1000000;
}

AudioStreamInMarvell::~AudioStreamInMarvell()
{
    if(mHandle) {
        snd_pcm_close(mHandle);
        mHandle = NULL;
		DEV_ENABLE(mDevice,false);
    }
}

status_t AudioStreamInMarvell::set(
        AudioHardwareMarvell *hw,
        uint32_t devices,
        int *pFormat,
        uint32_t *pChannels,
        uint32_t *pRate,
        AudioSystem::audio_in_acoustics acoustics)
{
	AutoMutex lock(&mLock);
	
    if (!pFormat||!pChannels||!pRate) return BAD_VALUE;

	int err;
	
	int Format = *pFormat;
	uint32_t Channels = *pChannels;
	uint32_t Rate = *pRate;
	
	// sanity check 
	if(Format != format())
	{
        *pFormat = format();
		ERROR("%s bad format",__func__);
		return BAD_VALUE;
	}
    
    mChannel = Channels;
    mChannelCount = AudioSystem::popCount(mChannel);
	err = snd_pcm_open(&mHandle, DEV_NAME(devices), SND_PCM_STREAM_CAPTURE, 0);
	if (err < 0) {
		ERROR("snd_pcm_open failed [%s",snd_strerror(err));
		return BAD_VALUE;
	}


	INFO("AudioStreamInMarvell set format[%d] channels[%d:%d] rate[%d] devices[%s] \n",Format,Channels,mChannelCount,Rate,AudioDeviceManager::getInstance().dumpDevices(devices));
    

	snd_pcm_hw_params_t *hw_params;	
	snd_pcm_sw_params_t *sw_params;
	snd_pcm_hw_params_alloca(&hw_params);
	snd_pcm_sw_params_alloca(&sw_params);
	snd_pcm_hw_params_any(mHandle, hw_params);

    unsigned int requestedBufferTime = mBufferTime;
    unsigned int requestedPeriodTime = mPeriodTime;

	/* Set the desired hardware parameters. */
	err = snd_pcm_hw_params_set_access(mHandle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);	
	if(err<0)
	{
		ERROR("set hw_params: set access failed[%s]",snd_strerror(err));
		return BAD_VALUE;
	}
	err = snd_pcm_hw_params_set_format(mHandle, hw_params, SND_PCM_FORMAT_S16_LE);
	if(err<0)
	{
		ERROR("set hw_params: set format failed[%s]",snd_strerror(err));
		return BAD_VALUE;
	}

	//err = snd_pcm_hw_params_set_channels(mHandle, hw_params, channels);
	err = snd_pcm_hw_params_set_channels_near(mHandle, hw_params, (unsigned int*)&mChannelCount);
	if(err<0)
	{
		ERROR("set hw_params: set channels failed[%s]",snd_strerror(err));
		return BAD_VALUE;
	}
    INFO("channels count[%d]\n",mChannelCount);
	err = snd_pcm_hw_params_set_rate_near(mHandle, hw_params, &Rate, NULL);
	if(err<0)
	{
		ERROR("set hw_params: set rate failed[%s]",snd_strerror(err));
		return BAD_VALUE;
	}
	mSampleRate=Rate;
    INFO("sample rate=%d",mSampleRate);
	err = snd_pcm_hw_params_set_buffer_time_near(mHandle, hw_params, &mBufferTime, 0);
	if( err<0 ){
		WARN("set hw_params: Unable to set buffer time to %d usec", requestedBufferTime);
	}	
	
	err = snd_pcm_hw_params_get_buffer_size(hw_params, &mBufferSize);												  
	if (err < 0) {																							  
			ERROR("Unable to get buffer size for playback: %s\n", snd_strerror(err));						  
			return BAD_VALUE;																					  
	}																										  
	
	INFO("buffer time eventually set as %d usec,%lu frames", mBufferTime,mBufferSize);

	err = snd_pcm_hw_params_set_period_time_near(mHandle,hw_params, &mPeriodTime,0);
	if( err<0 ){
		ERROR("set hw_params: Unable to set period time to %u usec", requestedPeriodTime);
	}	
	/*
	snd_pcm_hw_params_get_period_time(hw_params, &mPeriodTime,0);*/
	err = snd_pcm_hw_params_get_period_size(hw_params, &mPeriodSize, 0); 										  
	if (err < 0) {																							  
			ERROR("get period size failed[%s]\n", snd_strerror(err));						  
			return BAD_VALUE;																					  
	}												
	
	INFO("period time eventually set as %d usec,%lu frames", mPeriodTime,mPeriodSize);

	/* Write the parameters to the driver */
	int ret = snd_pcm_hw_params(mHandle, hw_params);
	if (ret < 0) {
		ERROR("AudioStreamInMarvell unable to set hw parameters: %s", snd_strerror(ret));
		return BAD_VALUE;
	}


	/* soft params */	
	// Get the current software parameters
	err = snd_pcm_sw_params_current(mHandle, sw_params);
	if(err<0){
		ERROR("failed to fetch alsa current sw params[%s]",snd_strerror(err));
		return BAD_VALUE;
	}

    snd_pcm_uframes_t start_threshold = (double)Rate/ 1000000;    
	if (start_threshold < 1)
		start_threshold = 1;
	if (start_threshold > mBufferSize)
		start_threshold = mBufferSize;
    if( snd_pcm_sw_params_set_start_threshold(mHandle, sw_params, start_threshold)<0 ){
        WARN("Unable to set start threshold to %lu frames", start_threshold);
    }

    // Stop the transfer when the buffer is full.
    if( snd_pcm_sw_params_set_stop_threshold(mHandle, sw_params, mBufferSize)<0 ){
        WARN("Unable to set stop threshold to %lu frames", mBufferSize);
    }

    // Allow the transfer to start when at least periodSize samples can be
    // processed.
    if( snd_pcm_sw_params_set_avail_min(mHandle, sw_params, mPeriodSize)<0 ){
        WARN("Unable to configure available minimum to %lu", mPeriodSize);
    }

    if( snd_pcm_sw_params(mHandle, sw_params)<0 ){;
        ERROR("Unable to configure software parameters");
        return BAD_VALUE;
    }

    mAudioHardware = hw;
    mDevice = devices;	

	//DEV_ENABLE(mDevice,true);
	
	return NO_ERROR;
}

status_t AudioStreamInMarvell::setGain(float gain)
{
	DEV_VOLUME(AudioSystem::DEVICE_IN_ALL,gain,gain);
    return NO_ERROR; 
}

status_t AudioStreamInMarvell::standby()
{
    // TODO: audio hardware to standby mode
    return NO_ERROR;
}

status_t AudioStreamInMarvell::setParameters(const String8& keyValuePairs)
{
    DBG("audioin->setparameter %s", keyValuePairs.string());

    AudioParameter param = AudioParameter(keyValuePairs);
    String8 key = String8(AudioParameter::keyRouting);
    status_t status = NO_ERROR;
    int device=0;
    int source;


    #if PLATFORM_SDK_VERSION>=9

    // read source before device so that it is upto date when doRouting() is called 
    if (param.getInt(String8(AudioParameter::keyInputSource), source) == NO_ERROR) {
        param.remove(String8(AudioParameter::keyInputSource));    
    }

    #endif
    
    if (param.getInt(key, device) == NO_ERROR) {
        if (!device) {
            DEV_ENABLE(mDevice,false);    
            
        }else {
            if(mDevice!=device)   DEV_ENABLE(mDevice,false);                 
            DEV_ENABLE(device,true);            
            mDevice = device; 
        }
        param.remove(key);
    }

    if (param.size()) {
        status = BAD_VALUE;
    }
    return status;
}

String8 AudioStreamInMarvell::getParameters(const String8& keys)
{

    AudioParameter param = AudioParameter(keys);
    String8 value;
    String8 key = String8(AudioParameter::keyRouting);

    if (param.get(key, value) == NO_ERROR) {
        param.addInt(key, (int)mDevice);
    }

    INFO("getParameters() %s", param.toString().string());
    return param.toString();
}

ssize_t AudioStreamInMarvell::read(void* buffer, ssize_t bytes)
{

    if(!mHandle)
    {
        ERROR("AudioStreamInMarvell::read handle is null");
		// fake timing for audio input
		memset(buffer, 0, bytes);
        usleep(bytes * 1000000 / sizeof(int16_t) / AudioSystem::popCount(channels()) / sampleRate());
		return bytes;
		
    }

    int frames = snd_pcm_bytes_to_frames(mHandle,bytes);
    int ret;
    int8_t *ptr = (int8_t*)buffer;
    while(frames > 0)
    {
        ret = snd_pcm_readi(mHandle, ptr, frames);    
        if(ret < 0) {   
            if (ret == -EAGAIN) {
                continue;
            } 
            else if (ALSA_UTILS::XRun_Recovery (mHandle, ret) < 0) 
			{			
				ERROR("alsa xrun read failed");				
	           	break;
            }
            continue;
        }
        else if(ret<frames)
        {
            snd_pcm_wait(mHandle, 1000);
        }
        ptr += snd_pcm_frames_to_bytes(mHandle,ret);
        frames -= ret;
    }

    UpStreamDumper::getInstance().StreamDump(buffer,bytes);
    return (ssize_t)bytes;
}

status_t AudioStreamInMarvell::dump(int fd, const Vector<String16>& args)
{
    const size_t SIZE = 256;
    char buffer[SIZE];
    String8 result;
    snprintf(buffer, SIZE, "AudioStreamInMarvell::dump\n");
    result.append(buffer);
    snprintf(buffer, SIZE, "\tsample rate: %d\n", sampleRate());
    result.append(buffer);
    snprintf(buffer, SIZE, "\tbuffer size: %d\n", bufferSize());
    result.append(buffer);
    snprintf(buffer, SIZE, "\tchannel count: %d\n", channels());
    result.append(buffer);
    snprintf(buffer, SIZE, "\tformat: %d\n", format());
    result.append(buffer);
    ::write(fd, result.string(), result.size());
    return NO_ERROR;
}


// ----------------------------------------------------------------------------

extern "C" AudioHardwareInterface* createAudioHardware(void)
{
    return new AudioHardwareMarvell();
}

}; // namespace android
