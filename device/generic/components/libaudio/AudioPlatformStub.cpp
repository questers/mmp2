/*
 *  Copyright 2012 Quester Technology,Inc.
 *  ALL RIGHTS RESERVED
 *
 */

#include <utils/String8.h>
#include <media/AudioSystem.h>
#include <AudioDevice.h>
#include <AudioPlatform.h>
#include <AudioPlatformStub.h>

namespace android {

AudioPlatformStub::AudioPlatformStub():
AudioPlatform("stub")
{

}

AudioPlatformStub::~AudioPlatformStub()
{}


status_t AudioPlatformStub::setModeImpl(int mode)
{
    return NO_ERROR;
}
status_t AudioPlatformStub::setAecImpl(int mode){
    
    return NO_ERROR;
}
status_t AudioPlatformStub::Init(){
    
    return NO_ERROR;
}

};
