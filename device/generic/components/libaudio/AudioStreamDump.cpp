#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h> 
#include <stdio.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <unistd.h> 


#include <cutils/properties.h>

#include "AudioSetting.h"
#include "AudioStreamDump.h"

namespace android
{
StreamDumper::StreamDumper(const char* name):
stream(name),
dumpFD(0),
started(false)
{
}
int StreamDumper::StreamDump(const void* buffer,size_t bytes)
{
    int ret=-1;
    if(dumpFD)
    {
        ret = ::fwrite(buffer,1,bytes,dumpFD);

        timer.stop();
        if((0!=AudioSettings::STREAM_DUMP_FLUSH_INTERVAL)&&
           ( (timer.durationUsecs()/1000)>=AudioSettings::STREAM_DUMP_FLUSH_INTERVAL))
            ::fflush(dumpFD);
    }
    return ret;
}

int StreamDumper::StreamClose()
{
    if(dumpFD)
    {
        ::fclose(dumpFD);
        dumpFD = 0;
        started=false;
    }
    return 0;
}

int StreamDumper::StreamStart()
{
    if(!dumpFD)
    {
        char buffer[256];
        sprintf(buffer,"%s/%s.bin",AudioSettings::STREAM_DUMP_PATH,stream?stream:"StreamDumper");
        dumpFD = ::fopen(buffer,"wb");
        if(dumpFD)
        {
            timer.start();
            started=true;
        }
    }

    return 0;

}


StreamDumper::~StreamDumper()
{
    if(dumpFD)
    {
        ::fclose(dumpFD);
        dumpFD = 0;
    }

}

ANDROID_SINGLETON_STATIC_INSTANCE(DownStreamDumper)      

int DownStreamDumper::StreamDump(const void * buffer,size_t bytes)
{    
	char value[PROPERTY_VALUE_MAX];
    if(property_get("audio.debug.ddump", value, "")>0)
    {
        if(atoi(value))
        {
            if(false==StreamStarted())
                StreamStart();
            return StreamDumper::StreamDump(buffer,bytes);
        }
        else
        {
            if(StreamStarted())
                StreamClose();        
        }
    }
    return 0;
}

ANDROID_SINGLETON_STATIC_INSTANCE(UpStreamDumper)      
    
int UpStreamDumper::StreamDump(const void * buffer,size_t bytes)
{
	char value[PROPERTY_VALUE_MAX];
    
    if(property_get("audio.debug.udump", value, "")>0)
    {
        if(atoi(value))
        {
            if(false==StreamStarted())
                StreamStart();
        return StreamDumper::StreamDump(buffer,bytes);
        }
        else
        {
            if(StreamStarted())
                StreamClose();        
        }
    }
    return 0;
    
}

};
