/*
 *  Copyright 2012 Quester Technology,Inc.
 *  ALL RIGHTS RESERVED
 *
 */

#ifndef _AUDIO_PLATFORM_RT5631_H
#define _AUDIO_PLATFORM_RT5631_H


namespace android
{


class AudioPlatform;

class AudioPlatformRT5631:public AudioPlatform
{
	public:
		AudioPlatformRT5631();
	virtual ~AudioPlatformRT5631();


	virtual status_t setModeImpl(int mode);
	virtual status_t setAecImpl(int mode);	
    virtual status_t Init();

    class OnBoardSpeaker:public AudioDevice{
        public:
            OnBoardSpeaker();
            virtual ~OnBoardSpeaker(){}
            virtual int SetVolume(float l,float r);
            virtual int Enable(bool enable);
            virtual int Suspend(bool suspend);
            virtual void Init();
    };

    class OnBoardMicrophone:public AudioDevice{
        public:
            OnBoardMicrophone();
            virtual ~OnBoardMicrophone(){}
            virtual int SetVolume(float l,float r);
            virtual int Enable(bool enable);
            virtual int Suspend(bool suspend);
            virtual void Init();                
    };
            

};


};

#endif



