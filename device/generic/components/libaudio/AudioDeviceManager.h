#ifndef _AUDIO_DEVICE_MANAGER_H
#define _AUDIO_DEVICE_MANAGER_H


#include <utils/Singleton.h>
#include <utils/KeyedVector.h>
namespace android
{

class String8;
class AudioParameter;
class AudioDevice;
class AudioPlatform;


class AudioDeviceManager :public Singleton<AudioDeviceManager>
{
	friend class Singleton<AudioDeviceManager>;
	class AdmCommandThread;

	AudioDeviceManager(const AudioDeviceManager&);
	AudioDeviceManager& operator = (const AudioDeviceManager&);

	 public:
	 status_t initCheck(void);
	 char*  dumpDevices(uint32_t devices);

	 //single device operation
	 const char * getDevName(uint32_t device);
	 bool IsDevSuspended(uint32_t device);
	 bool IsDevEnabled(uint32_t device);
	 bool muteMic(bool mute);

     public:
     static const char* DEFAULT_ALSA_DEVICENAME;

	 
	public:
	 /*non-block operations*/
	 void setDevEnable(uint32_t devices,bool enable=true);
	 void setDevRoute(uint32_t old_route,uint32_t new_route);
	 void setDevVolume(uint32_t devices,float left_vol=0.75,float right_vol=0.75);
	 void setDevStandby(uint32_t devices,bool standby=true);
	 void setMode(int mode);
	 int  getMode(void){return mMode;}
	 void setCall(bool start);
	 void setCallVolume(float vol);	 
	 void setCallPath(int path);//IVoiceCall Path
	 void setAecPermission(bool permitted);
	 void setAec(int mode); 
    void registerDevice(uint32_t type, AudioDevice* dev);
	 
	protected:
		bool init(void);
		void Dump(void);
		/*block operations,not open */
		 //combined devices operations
		bool setDevEnableImpl(uint32_t devices,bool enable=true);
		bool setDevVolumeImpl(uint32_t devices,float left_vol=0.75,float right_vol=0.75);//volume is 0~1.0
		bool setDevStandbyImpl(uint32_t devices,bool standby=true);
		bool setDevRouteImpl(uint32_t old_devices,uint32_t new_devices);	 
		status_t setModeImpl(int mode);
		void setAecImpl(int mode);
		
	protected:
		AudioDeviceManager();
		virtual ~AudioDeviceManager();
	private:
		uint32_t enabledDevices;
		bool mInDeviceSuspended;
		bool mOutDeviceSuspended;
		KeyedVector <uint32_t, AudioDevice*> mManagedDevices;
		int 			mMode;
		bool initChecked;		
		sp <AdmCommandThread> mAdmCommandThread;	// adm commands thread

		status_t hardware_status;

		//AEC mode status
		int aec_mode;
		bool aec_permitted;
		int mVoicePath;

		AudioPlatform* m_AudioPlatform;

	private:
		class AdmCommandThread : public Thread
		{
			class AdmCommand;
			public:
				//commands for ADM
				enum
				{
					COMMAND_DEVICE_ENABLE=0,
					COMMAND_DEVICE_ROUTE,
					COMMAND_DEVICE_VOLUME,
					COMMAND_DEVICE_STANDBY,
					COMMAND_AUDIO_MODE,
					COMMAND_VOICECALL,
					COMMAND_VOICECALL_VOLUME,
					COMMAND_VOICECALL_PATH,
					COMMAND_AEC,
					COMMAND_MAX,
				};
				AdmCommandThread (String8 name);
				virtual 			~AdmCommandThread();
				
			
				
				status_t	DevEnableCommand(uint32_t devices,bool enable);
				status_t	DevRouteCommand(uint32_t old_route,uint32_t new_route);
				status_t	DevVolumeCommand(uint32_t devices,float left_vol,float right_vol);
				status_t	DevStandbyCommand(uint32_t devices,bool standby);
				status_t	AudioModeCommand(int mode);					
				status_t	VoiceCallCommand(bool start);	
				status_t	VoiceCallVolumeCommand(float vol);	
				status_t	VoiceCallPathCommand(int path);	
				status_t    AecCommand(int mode);
				
				void		exit();

			protected:	
				inline status_t insertCommand(AdmCommand *command, bool wait_finish=false);
				inline void insertCommand_l(AdmCommand *command);

			private:
				// Thread virtuals
				virtual 	void		onFirstRef();
				virtual 	bool		threadLoop();

				void dumpCommand(AdmCommand* cmd);
				
				class AdmCommand
				{
					public:
						AdmCommand():mCommand(-1){}
						
						int mCommand;	//
						nsecs_t mTime;	// time stamp
						Condition mCond; // condition for status return
						status_t mStatus; // command status
						bool mWaitStatus; // true if caller is waiting for status
						void *mParam;	  // command parameter ()
						
				};
				class EnableData
				{
					public:
						uint32_t devices;
						bool	 enable;
						
				};
				class RouteData
				{
					public:
						uint32_t old_route;
						uint32_t new_route;
				};
				class VolumeData
				{
					public:
						uint32_t devices;
						float l,r;
				};
				class StandbyData
				{
					public:
						uint32_t devices;
						bool standby;
				};
				class ModeData
				{
					public:
						int mode;//AudioSystem::MODE_IN_CALL ...
				};

				class CallData
				{
					public:
						bool start;
						float vol;
						int path;
				};
				class AecData
				{
					public:
						int mode;
				};
				
		        Mutex   mLock;
		        Condition mWaitWorkCV;
		        Vector <AdmCommand *> mAdmCommands; // list of pending commands
		        AdmCommand mLastCommand;          // last processed command (used by dump)
		        String8 mName;                      // string used by wake lock fo delayed commands
			
		};

		
};

#define DEV_NAME(dev) \
		AudioDeviceManager::getInstance().getDevName(dev)

#define DEV_ENABLE(dev,enable) \
		AudioDeviceManager::getInstance().setDevEnable(dev,enable)

#define DEV_STANDBY(dev,standby) \
		AudioDeviceManager::getInstance().setDevStandby(dev,standby)

#define DEV_VOLUME(dev,left,right) \
		AudioDeviceManager::getInstance().setDevVolume(dev,left,right)

#define DEV_ROUTING(old,new) \
		AudioDeviceManager::getInstance().setDevRoute(old,new)

#define DEV_CHECK(dev) \
		AudioDeviceManager::getInstance().initCheck()

#define DEV_MODE(mode) \
		AudioDeviceManager::getInstance().setMode(mode)	

#define DEV_MUTE(mute) \
		AudioDeviceManager::getInstance().muteMic(mute)

#define DEV_ISENABLED(dev) \
		AudioDeviceManager::getInstance().IsDevEnabled(dev)
		
#define VOICECALL_START() \
	AudioDeviceManager::getInstance().setCall(true)

#define VOICECALL_END() \
	AudioDeviceManager::getInstance().setCall(false)

#define VOICECALL_SETVOL(v) \
	AudioDeviceManager::getInstance().setCallVolume(v)

#define VOICECALL_PATH(p) \
	AudioDeviceManager::getInstance().setCallPath(p)
	
};
	
#endif
