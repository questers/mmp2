/*
 *  Copyright 2012 Quester Technology,Inc.
 *  ALL RIGHTS RESERVED
 *
 */
#include <utils/String8.h>
#include <media/AudioSystem.h>
#include <AudioDevice.h>
#include <AudioPlatform.h>
#include <AudioLog.h>
#include "AlsaUtils.h"
#include "AudioDeviceManager.h"
#include "AudioPlatformRt5631.h"

namespace android{

AudioPlatformRT5631::AudioPlatformRT5631():
AudioPlatform("rt5631")
{}
AudioPlatformRT5631::~AudioPlatformRT5631(){

}


status_t AudioPlatformRT5631::setModeImpl(int mode){
    return NO_ERROR;
}
status_t AudioPlatformRT5631::setAecImpl(int mode){
    return NO_ERROR;
}
status_t AudioPlatformRT5631::Init(){
    AudioDeviceManager& adm= AudioDeviceManager::getInstance();
	//on board speaker,register the same audio output for speaker and earpiece
	AudioDevice* spk=new OnBoardSpeaker();
	adm.registerDevice(AudioSystem::DEVICE_OUT_SPEAKER,spk);
	adm.registerDevice(AudioSystem::DEVICE_OUT_EARPIECE,spk);  
	adm.registerDevice(AudioSystem::DEVICE_IN_BUILTIN_MIC,new OnBoardMicrophone());    
    return NO_ERROR;
}


//
//Onboard 
//
AudioPlatformRT5631::OnBoardSpeaker::OnBoardSpeaker():
AudioDevice(AudioDeviceManager::DEFAULT_ALSA_DEVICENAME,"onboardspeaker")
{
}
int AudioPlatformRT5631::OnBoardSpeaker::SetVolume(float l,float r)
{
    /*
    char volumebuf[256];
    int left,right;
    left = l*100;
    right=r*100;
    
    left=(left*64)/100;
    right=(right*64)/100;
    DBG("set speaker volume=%d,%d\n",left,right);
    sprintf(volumebuf,"amixer cset <Speaker Playback Volume> %d,%d",left,right);
    ALSA_UTILS::MixerCSet(volumebuf);
    left_vol = l;
    right_vol = r;*/
    return 0;
}

int AudioPlatformRT5631::OnBoardSpeaker::Enable(bool enable)
{
    
    if(true==enable)
    {
        #ifdef ENABLE_HDMI_PORT
        if(AudioSettings::HDMI_AUDIO_ENABLE&&HDMI_UTILS::HdmiServerRunning())
        {
            int display_mode = HDMI_UTILS::HdmiDisplayMode();
            if( display_mode == DISPLAY_MODE_MIRROR 
                || display_mode == DISPLAY_MODE_HDMI_ONLY ) {
                // MIRROR and HDMI ONLY mode, don't enable speaker
                
                // we should not mute HDMI audio
                HDMI_UTILS::HdmiSetAudioMute(false); 
                
                return 0;
            }
        }
        #endif
        ALSA_UTILS::MixerCSet("amixer cset <Speaker Function> 0");
        ALSA_UTILS::MixerCSet("amixer cset <Speaker Playback Switch> 1,1");
        
    }
    else
    {        
        ALSA_UTILS::MixerCSet("amixer cset <Speaker Function> 1");
        ALSA_UTILS::MixerCSet("amixer cset <Speaker Playback Switch> 0,0");
    }

    enabled = enable;
    
    return 0;

}

int AudioPlatformRT5631::OnBoardSpeaker::Suspend(bool suspend)
{
    Enable(!suspend);
    return 0;
}
void AudioPlatformRT5631::OnBoardSpeaker::Init()
{
    ALSA_UTILS::MixerCSet("amixer cset <Speaker Playback Volume> 58,58");
}

AudioPlatformRT5631::OnBoardMicrophone::OnBoardMicrophone():
AudioDevice(AudioDeviceManager::DEFAULT_ALSA_DEVICENAME,"onboardmicrophone_mic1")
{
}
int AudioPlatformRT5631::OnBoardMicrophone::SetVolume(float l,float r)
{
    /*
    char volumebuf[256];
    int left,right;
    left = l*100;
    right=r*100;
    left=(left*64)/100;
    right=(right*64)/100;
    
    sprintf(volumebuf,"amixer cset <PCM Capture Volume> %d,%d",left,right);
    ALSA_UTILS::MixerCSet(volumebuf);
    left_vol = l;
    right_vol = r;
    */
    return 0;
}

int AudioPlatformRT5631::OnBoardMicrophone::Enable(bool enable)
{
    if(true==enable)
    {
        ALSA_UTILS::MixerCSet("amixer cset <Internal Mic Function> 0");  
    }
    else
    {        
        ALSA_UTILS::MixerCSet("amixer cset <Internal Mic Function> 1");
    }

    enabled = enable;
    return 0;

}

int AudioPlatformRT5631::OnBoardMicrophone::Suspend(bool suspend)
{
    Enable(!suspend);
    return 0;
}

void AudioPlatformRT5631::OnBoardMicrophone::Init()
{
    
}

};

