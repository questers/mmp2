#ifndef _AUDIO_DEVICE_H
#define _AUDIO_DEVICE_H


namespace android
{


class AudioDevice
{
	public:
		AudioDevice(const char* dev_name,const char* alias);
	virtual ~AudioDevice(){}

	virtual int SetVolume(float l,float r)=0;
	virtual int Enable(bool enable)=0;
	virtual int Suspend(bool suspend)=0;
	virtual int Mute(bool mute);
	virtual void Init();

	const char* Name(){return mDeviceName;}	
	const char* Alias(){return mAliasName;}
	
	bool IsEnabled(){return enabled;}
	bool IsSuspended(){return suspended;}
	bool IsMuted(){return muted;}
	protected:
		const char* mDeviceName;
		const char* mAliasName;
		bool  enabled;
		bool  suspended;
		bool  muted;
		float left_vol,right_vol;
};


};

#endif

