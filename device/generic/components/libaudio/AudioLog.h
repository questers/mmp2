/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _AUDIO_LOG_H
#define _AUDIO_LOG_H

#include <utils/Singleton.h>
#include <cutils/compiler.h>

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#ifdef LOG_NDEBUG
#undef LOG_NDEBUG
#endif

#define LOG_TAG "audiohal"
#define LOG_NDEBUG 0

#include <utils/Log.h>


namespace android {

class AudioLogSetting : public Singleton<AudioLogSetting>
{
	uint32_t mLogLevel;
    uint32_t mEnabled;

public:
	enum
	{
		AUDIOLOG_WARN			=0x1,
		AUDIOLOG_INFO			=0x2,
		AUDIOLOG_ERROR			=0x4,
		AUDIOLOG_DEBUG			=0x800,
		AUDIOLOG_DEFAULT        =AUDIOLOG_WARN|AUDIOLOG_ERROR,
		AUDIOLOG_ALL            =AUDIOLOG_WARN|AUDIOLOG_INFO|AUDIOLOG_ERROR|AUDIOLOG_DEBUG,
	};
	
    void setEnabled(bool enable);
	bool getEnabled(){return mEnabled;}
    void setLevel(uint32_t level);
	uint32_t getLevel(){return mLogLevel;}

	
	
   	AudioLogSetting();

};

#define WARN(...) \
	LOGI_IF(AudioLogSetting::getInstance().getEnabled()&&AudioLogSetting::getInstance().getLevel()&AudioLogSetting::AUDIOLOG_WARN,__VA_ARGS__)

#define INFO(...) \
	LOGI_IF(AudioLogSetting::getInstance().getEnabled()&&AudioLogSetting::getInstance().getLevel()&AudioLogSetting::AUDIOLOG_INFO,__VA_ARGS__)

#define ERROR(...) \
	LOGI_IF(AudioLogSetting::getInstance().getEnabled()&&AudioLogSetting::getInstance().getLevel()&AudioLogSetting::AUDIOLOG_ERROR,__VA_ARGS__)

#define DBG(...) \
	LOGI_IF(AudioLogSetting::getInstance().getEnabled()&&AudioLogSetting::getInstance().getLevel()&AudioLogSetting::AUDIOLOG_DEBUG,__VA_ARGS__)

#define ALWAYS(...) \
	LOGI_IF(1,__VA_ARGS__);
}

#endif // _UI_GRAPHIC_LOG_H

