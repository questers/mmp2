/*
 *  Copyright 2011 Quester Technology,Inc.
 *  ALL RIGHTS RESERVED
 *
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 */
#ifndef AUDIO_SETTING_H
#define AUDIO_SETTING_H

#include <sys/types.h>
#include "AudioLog.h"

namespace android
{

class AudioSettings
{
	public:

	//
	//Stream output
	//
	static const int DEFAULT_OUTPUT_SAMPLE_RATE		=	48000;
	static const int DEFAULT_OUTPUT_PERIOD_TIME		=	25000;
	static const int DEFAULT_OUTPUT_BUFFER_TIME 	=	100000;//4 // 4 times period is better

	//
	//Stream input
	//
	static const int DEFAULT_INPUT_SAMPLE_RATE		=	8000;
	static const int DEFAULT_INPUT_PERIOD_TIME		=	10000;//5250;
	static const int DEFAULT_INPUT_BUFFER_TIME 		=	40000;//21250;//4

	//
	//Stream Dumper
	//
	static const char* STREAM_DUMP_PATH;
	static const int   STREAM_DUMP_FLUSH_INTERVAL	=	500;//ms


	//
	//HDMI connection
	//
	static const bool HDMI_AUDIO_ENABLE=true;

	//
	//AudioHAL log
	//
	static const uint32_t AUDIOLOG_DEFAULT_LEVEL=AudioLogSetting::AUDIOLOG_DEFAULT;
	static const uint32_t AUDIOLOG_DEFAULT_ENABLE=1;//we may add more components flags here,default is enabled



	//
	//Key shared by audio hardware and audio policy manager
	//	
    static const char *KEY_FM_STATE;


	//
	//Misc settings
	//
	static const bool  ADM_COMMAND_DUMP=false;
};

};

#endif
