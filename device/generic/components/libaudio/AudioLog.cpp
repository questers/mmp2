/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <stdlib.h>
#include <unistd.h>
#include <cutils/properties.h>
#include <utils/Endian.h>
#include <utils/Timers.h>

#include "AudioLog.h"
#include "AudioSetting.h"

namespace android {

ANDROID_SINGLETON_STATIC_INSTANCE(AudioLogSetting)


AudioLogSetting::AudioLogSetting():
mLogLevel(AudioSettings::AUDIOLOG_DEFAULT_LEVEL),
mEnabled(AudioSettings::AUDIOLOG_DEFAULT_ENABLE)
{
    char property[PROPERTY_VALUE_MAX];
    if (property_get("audio.debug.enable", property, NULL) > 0) {
        mEnabled = atoi(property);
    }
    if (property_get("audio.debug.level", property, NULL) > 0) {
        mLogLevel = atoi(property);
    }
    
}

void AudioLogSetting::setEnabled(bool enable)
{
    mEnabled = enable;
}


void AudioLogSetting::setLevel(uint32_t level)
{
    mLogLevel = level;
}

}
