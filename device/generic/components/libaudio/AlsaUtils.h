#ifndef _ALSA_UTILS_H
#define _ALSA_UTILS_H

#include <alsa/asoundlib.h>

namespace android{

class String8;

class ALSA_UTILS
{
	public:
    static int MixerCSet(const char* sentence);
	static int MixerCSet(const char* card,const char* sentence);
	static int XRun_Recovery(snd_pcm_t * handle, int err);
	static int SoundCardCheck(String8& cardname);
	
};



};
#endif

