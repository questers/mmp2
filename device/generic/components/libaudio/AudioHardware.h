/*
 * (C) Copyright 2012 Quester Technology,Inc.
 * All Rights Reserved
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved 
 */

#ifndef ANDROID_AUDIO_HARDWARE_Marvell_H
#define ANDROID_AUDIO_HARDWARE_Marvell_H

#include <stdint.h>
#include <sys/types.h>
#include <utils/threads.h>
#include <alsa/asoundlib.h>

#include <hardware_legacy/AudioHardwareBase.h>


namespace android {

class AudioHardwareMarvell;

// ----------------------------------------------------------------------------

class AudioStreamOutMarvell : public AudioStreamOut {
public:
                        AudioStreamOutMarvell();
    virtual             ~AudioStreamOutMarvell();

    status_t    set(
            AudioHardwareMarvell *hw,
            uint32_t devices,
            int *pFormat,
            uint32_t *pChannels,
            uint32_t *pRate);

    virtual uint32_t    sampleRate() const { return mSampleRate; }
	/* Buffer size seems has a limits that it must be power of 2,4096 is better number for most scenario
	  * For Aspen platform the period size is limited to 8192-32 bytes (due to DMA limits)
	  * And the softwar dmix plugin also has a period size limits(2040 frames,equals 8160 bytes). Ugly?
	*/
    virtual size_t      bufferSize() const { return mPeriodSize * mChannelCount * sizeof(uint16_t); }
    virtual uint32_t    channels() const { return mChannel; }
    virtual int         format() const { return mFormat; }
    virtual uint32_t    latency() const { return 0;/* mBufferTime/1000;*/ }
    virtual status_t    setVolume(float left, float right);
    virtual ssize_t     write(const void* buffer, size_t bytes);
    virtual status_t    standby();
    virtual status_t    dump(int fd, const Vector<String16>& args);
    virtual status_t    setParameters(const String8& keyValuePairs);
    virtual String8     getParameters(const String8& keys);
#if PLATFORM_SDK_VERSION >= 8
    // return the number of audio frames written by the audio dsp to DAC since
    // the output has exited standby
    virtual status_t    getRenderPosition(uint32_t *dspFrames) {*dspFrames = 0; return NO_ERROR;}
#endif
	virtual status_t    resume();
	status_t 			connect2hardware(void);

private:
    AudioHardwareMarvell  *mAudioHardware;
    int                    mFormat;
    int                    mChannelCount;
    uint32_t               mChannel;
    uint32_t               mSampleRate;
    Mutex		   mLock;
    snd_pcm_t             *mHandle;
    unsigned int           mBufferTime; /* macro second */
    unsigned int           mPeriodTime; /* macro second */
    snd_pcm_uframes_t      mBufferSize; /* sample number */
    snd_pcm_uframes_t      mPeriodSize; /* sample number */
    uint32_t               mDevice;
	int                    mUserFlag;     
	bool 				   mPowerLock;
};

class AudioStreamInMarvell : public AudioStreamIn {
public:
                        AudioStreamInMarvell();
    virtual             ~AudioStreamInMarvell();

    status_t    set(
            AudioHardwareMarvell *hw,
            uint32_t devices,
            int *pFormat,
            uint32_t *pChannels,
            uint32_t *pRate,
            AudioSystem::audio_in_acoustics acoustics);

    virtual uint32_t    sampleRate() const { return mSampleRate; }
    virtual size_t      bufferSize() const { return mPeriodSize * mChannelCount * sizeof(uint16_t); }
    virtual uint32_t    channels() const { return mChannel; }
    virtual int         format() const { return mFormat; }
    virtual status_t    setGain(float gain);
    virtual ssize_t     read(void* buffer, ssize_t bytes);
    virtual status_t    dump(int fd, const Vector<String16>& args);
    virtual status_t    standby();
    virtual status_t    setParameters(const String8& keyValuePairs);
    virtual String8     getParameters(const String8& keys);
#if PLATFORM_SDK_VERSION >= 8
    // Return the amount of input frames lost in the audio driver since the last call of this function.
    // Audio driver is expected to reset the value to 0 and restart counting upon returning the current value by this function call.
    // Such loss typically occurs when the user space process is blocked longer than the capacity of audio driver buffers.
    // Unit: the number of input audio frames
    virtual unsigned int  getInputFramesLost() const {return 0;}
#endif

private:
    AudioHardwareMarvell  *mAudioHardware;
    int                    mFormat;
    int                    mChannelCount;
    uint32_t               mChannel;
    uint32_t               mSampleRate;
    Mutex              	   mLock;
    snd_pcm_t             *mHandle;
    uint32_t               mDevice;
    unsigned int           mBufferTime; /* macro second */
    unsigned int           mPeriodTime; /* macro second */
    snd_pcm_uframes_t      mBufferSize; /* sample number */
    snd_pcm_uframes_t      mPeriodSize; /* sample number */
};

class AudioHardwareMarvell : public  AudioHardwareBase
{
public:
                        AudioHardwareMarvell();
    virtual             ~AudioHardwareMarvell();
    virtual status_t    initCheck();
    virtual status_t    setVoiceVolume(float volume);
    virtual status_t    setMasterVolume(float volume);

	
    // mic mute
    virtual status_t    setMicMute(bool state);
    virtual status_t    getMicMute(bool* state);

    virtual status_t    setParameters(const String8& keyValuePairs);

    virtual size_t 		getInputBufferSize(uint32_t sampleRate, int format, int channelCount);

    // create I/O streams
    virtual AudioStreamOut* openOutputStream(
				uint32_t devices,
                                int *format=0,
                                uint32_t *channels=0,
                                uint32_t *sampleRate=0,
                                status_t *status=0);
    virtual    void        closeOutputStream(AudioStreamOut* out);

    virtual AudioStreamIn* openInputStream(
				uint32_t devices,
                                int *format,
                                uint32_t *channels,
                                uint32_t *sampleRate,
                                status_t *status,
				AudioSystem::audio_in_acoustics);
    virtual    void        closeInputStream(AudioStreamIn* in);

    void                setVolume(int direction, float volume);
	status_t            setMode(int mode);
	int 				getMode(void);

protected:
    virtual status_t    dump(int fd, const Vector<String16>& args);

    bool                mMicMute;
private:
    status_t            dumpInternals(int fd, const Vector<String16>& args);

    AudioStreamOutMarvell   *mOutput;
    AudioStreamInMarvell    *mInput;
    Mutex					mLock;
    float                    mVoiceVolume;	
	float                    mMasterVolume;
};

}; // namespace android

#endif // ANDROID_AUDIO_HARDWARE_Marvell_H
