/*
 * Copyright 2011 Quester Technology,Inc.
 * ALL RIGHTS RESERVED
 *  
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 */
/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "AudioPolicyManager"
//#define LOG_NDEBUG 0
#include <utils/Log.h>
#include "AudioPolicyManager.h"
#include <media/mediarecorder.h>
#include "AudioSetting.h"

namespace android {


// ---  class factory


extern "C" AudioPolicyInterface* createAudioPolicyManager(AudioPolicyClientInterface *clientInterface)
{
    return new AudioPolicyManager(clientInterface);
}

extern "C" void destroyAudioPolicyManager(AudioPolicyInterface *interface)
{
    delete interface;
}

status_t AudioPolicyManager::setDeviceConnectionState(AudioSystem::audio_devices device,AudioSystem::device_connection_state state,const char *device_address)
{
   
    // connect/disconnect only 1 device at a time
    if (AudioSystem::popCount(device) != 1) return BAD_VALUE;


    switch (state)
    {
        // handle output device connection
        case AudioSystem::DEVICE_STATE_AVAILABLE:    
            if((device == AudioSystem::DEVICE_OUT_FM_HEADPHONE) ||
            (device == AudioSystem::DEVICE_OUT_FM_SPEAKER))
            {
                int fm_status = (device == AudioSystem::DEVICE_OUT_FM_HEADPHONE) ? 1 : 2;
                LOGD("setDeviceConnectionState(); FM ENABLE");
                AudioParameter param = AudioParameter();
                param.addInt(String8(AudioParameter::keyRouting), (int)device);
                param.addInt(String8(AudioSettings::KEY_FM_STATE), fm_status /* Disable */);
                //0 means global parameters
                mpClientInterface->setParameters(mHardwareOutput, param.toString());
            }
            break;
        case AudioSystem::DEVICE_STATE_UNAVAILABLE:
            if((device == AudioSystem::DEVICE_OUT_FM_HEADPHONE) ||
            (device == AudioSystem::DEVICE_OUT_FM_SPEAKER))
            {                
                LOGD("setDeviceConnectionState(); FM DISABLE");
                AudioParameter param = AudioParameter();
                param.addInt(String8(AudioParameter::keyRouting), (int)device);
                param.addInt(String8(AudioSettings::KEY_FM_STATE), 0 /* Disable */);
                //0 means global parameters
                mpClientInterface->setParameters(mHardwareOutput, param.toString());
            }
            break;
            
    }

    return AudioPolicyManagerBase::setDeviceConnectionState(device,state,device_address);

}

}; // namespace android
