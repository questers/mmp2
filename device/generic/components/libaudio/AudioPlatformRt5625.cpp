/*
 *  Copyright 2012 Quester Technology,Inc.
 *  ALL RIGHTS RESERVED
 *
 */
 
 //1. Microphone depop 
 //2. Add bluetooth support
 //3. Add FM support
 //4. Add Bluetooth path support
 
 /* Audio Codec Path Map
 
 
 Onboard playback/record:
 
                                     |------>DAC-----Spk mixer--->Onboard speaker
 Main I2S Interface<---->
                                     |<-----Record Mixer<-----ADC<-----MIC1
 
 
                                     |------>DAC-----HP mixer--->Onboard speaker
 Main I2S Interface<---->
                                     |<-----Record Mixer<-----ADC<-----MIC1
 
                                    
 
 Headset/HeadPhone:
 
                                                      |------>HP mixer 
                                                      |                  |
                                                      |                 \/
                                     |------>DAC-----HP PGA------Headset Speaker
 Main I2S Interface<---->
                                     |<-----Record Mixer<-----ADC<-----MIC2(Headset Microphone)
 
 
 
     
 Bluetooth1: Currently adopted
                                                                                             R
                                     |------>DAC-----HP mixer--->Record Mixer---->Voice ADC---->Voice PCM upstream
 Main I2S Interface<---->
                                     |<-----Record Mixer<-----Mono Mixer<-----Voice DAC<-----Voice PCM Downstream
                                                                         L 
 
 Bluetooth2: (only support while rt5625 stereo and voice i2s/PCM in master mode)
 
                                                                                             R
                                     |------>DAC-----HP mixer--->Record Mixer---->Voice ADC---->Voice PCM upstream
 Main I2S Interface<---->
                                     |<-----Voice to Stereo Digital Path<------- PCM Downstream
                                                                         
 FM speaker/FM headphone
 
 FM receiver------>LineIn(L/R)-------- ->-HP mixer------->HP
 FM receiver------>LineIn(L/R)-------- ->-HP mixer------->SPK
 
                                                                                              
 */
 /*
   * SCHEME 2
   * 
Music/Ringtone/Sonification
 
                                     |------>DAC-----Spk mixer--->Onboard speaker
 Main I2S Interface<---->
                                     |<-----Record Mixer<-----ADC<-----MIC1

With AEC AnalogIn/AnalogOut 

-------------
|                   |<------------------AuxOut<----------------------------|
|Voice Modem|                                                        -------                           |                            
|                   |------>PhoneIn---->ADCL------->|          |--->DAC---->Mono Mixer
-----------                 MIC1------->ADCR------>|VoDSP|----VxDAC--->SPK Mixer->On board speaker                                                        
                                                                                -------         |_______>HP Mixer-->Earpiece

With AEC Disabled
 -------------
 |                   |<------AuxOut<-----Mono Mixer<------Mic1
 |Voice Modem|                                  |------>HP Mixer-->Earpiece                         
 |                   |------>PhoneIn---->|          
 -----------                                     |------->SPK Mixer->On board speaker                                                        

 */
 /*
   * 2013-3-7
   * 
 Optimize call quality:

|-----------|
|                   |<------------------AuxOut<------------ ---------------|
|Voice Modem|                                                      -------                            |                            
|                   |------> MIC1---->ADCR------->|          |				     |
|-------  ---|-----> PhoneIn------->ADCL-- >|VoDSP |--->DAC---->Mono Mixer                                                     
                                       |                                    |----- |         
				           |	----->SPK Mixer->On board speaker   
					    | ----->HP Mixer-->Earpiece


   
 */
#include <utils/String8.h>
#include <media/AudioSystem.h>
#include <AudioDevice.h>
#include <AudioPlatform.h>
#ifdef ENABLE_HDMI_PORT
#include "IHdmiService.h"
#endif
#include "AlsaUtils.h"
#include "AudioDeviceManager.h"
#include "AudioPlatformRt5625.h"
#include "AudioLog.h"
namespace android {


AudioPlatformRT5625::AudioPlatformRT5625():
AudioPlatform("rt5625"),
mbEarpieceShouldEnable(false)
{

}

AudioPlatformRT5625::~AudioPlatformRT5625(){
    
}

status_t AudioPlatformRT5625::setModeImpl(int mode){ 
    DBG("%s mode=%d\n",__func__,mode);    
    AudioDeviceManager& adm= AudioDeviceManager::getInstance();
    mbEarpieceShouldEnable = false;
    if(AudioSystem::MODE_RINGTONE == mode || AudioSystem::MODE_IN_CALL == mode)
    {
        if(AudioSystem::MODE_IN_CALL == mode){     

            //common set
            ALSA_UTILS::MixerCSet("amixer cset <Voice DAC Mute> 0");                       
            ALSA_UTILS::MixerCSet("amixer cset <Mic1 Amp Boost Type> 0");     

            //ADCL for PhoneIn
            ALSA_UTILS::MixerCSet("amixer cset <Left Rec Mixer Phone Capture Switch> 1"); 
            //ADCR for MIC1
            ALSA_UTILS::MixerCSet("amixer cset <Right Rec Mixer Mic1 Capture Switch> 1"); 
            
            //DAC->Mono Mixer                
            ALSA_UTILS::MixerCSet("amixer cset <MoNo Mixer DAC Mixer Playback Switch> 1");      //
            //Mono Mixer->AuxOut                
            ALSA_UTILS::MixerCSet("amixer cset <AUXOUT Mux> 3");
            ALSA_UTILS::MixerCSet("amixer cset <AUXOUT Playback Switch> 1,1");                  //auxout playback switch        
            ALSA_UTILS::MixerCSet("amixer cset <AUXOUT Playback Volume> 31,31");                //auxout playback volume
            
            //VoiceDAC->Speaker
            ALSA_UTILS::MixerCSet("amixer cset <Voice DAC Playback Switch> 1");
            ALSA_UTILS::MixerCSet("amixer cset <Voice Volume> 47");

            //earpiece playback switch
            ALSA_UTILS::MixerCSet("amixer cset <Left HP Mixer HIFI DAC Playback Switch> 0");        //
            ALSA_UTILS::MixerCSet("amixer cset <Right HP Mixer HIFI DAC Playback Switch> 0"); 

            //spk playback switch
            ALSA_UTILS::MixerCSet("amixer cset <SPKOUT Playback Switch> 0,0");  
            ALSA_UTILS::MixerCSet("amixer cset <SPK Mixer DAC Mixer Playback Switch> 0");
            
            if(adm.IsDevEnabled(AudioSystem::DEVICE_OUT_SPEAKER)){
                DBG("set path for speaker");

                //enable speaker out
                ALSA_UTILS::MixerCSet("amixer cset <SPK Mixer Voice DAC Playback Switch> 1"); 
                ALSA_UTILS::MixerCSet("amixer cset <SPKOUT Playback Switch> 1,1");                    //spk playback switch
            }

            
            if(adm.IsDevEnabled(AudioSystem::DEVICE_OUT_EARPIECE)){
                DBG("set path for earpiece");
                ALSA_UTILS::MixerCSet("amixer cset <HPROUT Mux> 1");
                ALSA_UTILS::MixerCSet("amixer cset <HPLOUT Mux> 1");                
                ALSA_UTILS::MixerCSet("amixer cset <Left HP Mixer Voice DAC Playback Switch> 1");       //

                //enable speaker out
                ALSA_UTILS::MixerCSet("amixer cset <HPOUT Playback Switch> 1,1");       
                mbEarpieceShouldEnable = true;
            }

            
            //AEC enable
            setAecImpl(AEC_MODE_ANALOG_IN_ANALOG_OUT);


        }
		ALSA_UTILS::MixerCSet("amixer cset <PCM Capture Volume> 0,11");
		ALSA_UTILS::MixerCSet("amixer cset <Mic1 Playback Volume> 25");
    }
	else
	{
        ALSA_UTILS::MixerCSet("amixer cset <Left Rec Mixer Phone Capture Switch> 0"); 
        //ADCR for MIC1
        ALSA_UTILS::MixerCSet("amixer cset <Right Rec Mixer Mic1 Capture Switch> 0"); 
        
        //DAC->Mono Mixer                
        ALSA_UTILS::MixerCSet("amixer cset <MoNo Mixer DAC Mixer Playback Switch> 0");      //
        //Mono Mixer->AuxOut                
        ALSA_UTILS::MixerCSet("amixer cset <AUXOUT Playback Switch> 0,0");                  //auxout playback switch        
        
        //VoiceDAC->Speaker
       // ALSA_UTILS::MixerCSet("amixer cset <Voice DAC Playback Switch> 0");
       // ALSA_UTILS::MixerCSet("amixer cset <SPK Mixer Voice DAC Playback Switch> 0");         
       // ALSA_UTILS::MixerCSet("amixer cset <Left HP Mixer Voice DAC Playback Switch> 0");       //
       // ALSA_UTILS::MixerCSet("amixer cset <Right HP Mixer Voice DAC Playback Switch> 0");

        
        setAecImpl(AEC_MODE_DISABLED);//disable aec
	}
    mSceneMode = mode;
    return NO_ERROR;
}
status_t AudioPlatformRT5625::setAecImpl(int mode){
    if(mode!=mAecMode)
    {
        char buf[64];
        sprintf(buf,"amixer cset <AEC Mode> %d",mode);
        ALSA_UTILS::MixerCSet(buf);
        DBG("AEC mode ->%s",(3==mode)?"disabled":"enabled");
        mAecMode = mode;
    }
    
    return NO_ERROR;    
}
status_t AudioPlatformRT5625::PreDevEnable(uint32_t devices,bool enable)
{

    return AudioPlatform::PreDevEnable(devices,enable);
}

status_t AudioPlatformRT5625::PostDevEnable(uint32_t devices,bool enable)
{    
    AudioDeviceManager& adm= AudioDeviceManager::getInstance();
    if(AudioSystem::MODE_IN_CALL == mSceneMode){    
        //always disable record mixer left channel 
        ALSA_UTILS::MixerCSet("amixer cset <Left Rec Mixer Mic1 Capture Switch> 0");        
        ALSA_UTILS::MixerCSet("amixer cset <Right Rec Mixer Mic1 Capture Switch> 1");  
        
        //DAC for spk mixer as a hack for AEC mode
        ALSA_UTILS::MixerCSet("amixer cset <SPK Mixer DAC Mixer Playback Switch> 0");     


        if(true==enable){   
            if(devices&AudioSystem::DEVICE_OUT_SPEAKER){
                DBG("speaker enable in postdevenable");
                ALSA_UTILS::MixerCSet("amixer cset <DACR Func> 2");                    
                ALSA_UTILS::MixerCSet("amixer cset <MoNo Mixer Mic1 Playback Switch> 0"); 
                ALSA_UTILS::MixerCSet("amixer cset <SPK Mixer Voice DAC Playback Switch> 1"); 
                ALSA_UTILS::MixerCSet("amixer cset <SPKOUT Playback Switch> 1,1");                    //spk playback switch
                
            }
            if(devices&AudioSystem::DEVICE_OUT_EARPIECE){     
                
                DBG("earpiece enable in postdevenable");
                ALSA_UTILS::MixerCSet("amixer cset <DACR Func> 0"); 
                ALSA_UTILS::MixerCSet("amixer cset <MoNo Mixer Mic1 Playback Switch> 1"); 
                ALSA_UTILS::MixerCSet("amixer cset <Left HP Mixer Voice DAC Playback Switch> 1");       //
                ALSA_UTILS::MixerCSet("amixer cset <HPOUT Playback Switch> 1,1");       
            }
			
        }else {            
            if(devices&AudioSystem::DEVICE_OUT_SPEAKER){
                
                DBG("speaker disable in postdevenable");
                ALSA_UTILS::MixerCSet("amixer cset <SPK Mixer Voice DAC Playback Switch> 0"); 
                ALSA_UTILS::MixerCSet("amixer cset <SPKOUT Playback Switch> 0,0");                    //spk playback switch
            }
            if(devices&AudioSystem::DEVICE_OUT_EARPIECE){  
                
                DBG("earpiece disable in postdevenable");
                ALSA_UTILS::MixerCSet("amixer cset <Left HP Mixer Voice DAC Playback Switch> 0");       //
                ALSA_UTILS::MixerCSet("amixer cset <HPOUT Playback Switch> 0,0");       
            }
        }
        //enable DAC filter power
        ALSA_UTILS::MixerCSet("amixer cset <DAC CLK Power> 1,1");       
        
        
    }
    
    if(AudioSystem::MODE_NORMAL == mSceneMode||AudioSystem::MODE_INVALID == mSceneMode){    
		ALSA_UTILS::MixerCSet("amixer cset <PCM Capture Volume> 17,17");
    }
    return AudioPlatform::PostDevEnable(devices,enable);
}


status_t AudioPlatformRT5625::Init(){ 
	
    AudioDeviceManager& adm= AudioDeviceManager::getInstance();
	//on board speaker,register the same audio output for speaker and earpiece
	AudioDevice* spk=new OnBoardSpeaker();
	AudioDevice* earpiece=new HeadSetSpeaker();
	adm.registerDevice(AudioSystem::DEVICE_OUT_SPEAKER,spk);
	adm.registerDevice(AudioSystem::DEVICE_OUT_EARPIECE,earpiece);        
	adm.registerDevice(AudioSystem::DEVICE_IN_BUILTIN_MIC,new OnBoardMicrophone());
//	adm.registerDevice(AudioSystem::DEVICE_OUT_WIRED_HEADSET,new HeadSetSpeaker());
//	adm.registerDevice(AudioSystem::DEVICE_IN_WIRED_HEADSET,new HeadSetMicrophone());    
//	adm.registerDevice(AudioSystem::DEVICE_OUT_WIRED_HEADPHONE,new HeadPhoneSpeaker());    
//	adm.registerDevice(AudioSystem::DEVICE_OUT_BLUETOOTH_SCO_HEADSET,new BluetoothSpeaker());
//	adm.registerDevice(AudioSystem::DEVICE_IN_BLUETOOTH_SCO_HEADSET,new BluetoothMicrophone());
//	adm.registerDevice(AudioSystem::DEVICE_OUT_FM_SPEAKER,new FMSpeaker());
//	adm.registerDevice(AudioSystem::DEVICE_OUT_FM_HEADPHONE,new FMHeadPhone());

    //platform init
    ALSA_UTILS::MixerCSet("amixer cset <Left Rec Mixer Phone Capture Switch> 0");                 
    ALSA_UTILS::MixerCSet("amixer cset <SPK Mixer Phone Playback Switch> 0");             
    ALSA_UTILS::MixerCSet("amixer cset <Right HP Mixer Voice DAC Playback Switch> 0");             
    ALSA_UTILS::MixerCSet("amixer cset <Left HP Mixer Voice DAC Playback Switch> 0");    
    ALSA_UTILS::MixerCSet("amixer cset <Voice DAC Enable> 0");    
    
    return NO_ERROR;
}


//
//Onboard 
//
AudioPlatformRT5625::OnBoardSpeaker::OnBoardSpeaker():
AudioDevice(AudioDeviceManager::DEFAULT_ALSA_DEVICENAME,"onboardspeaker")
{
}
int AudioPlatformRT5625::OnBoardSpeaker::SetVolume(float l,float r)
{

    char volumebuf[256];
    int left,right;
    left = l*100;
    right=r*100;
    
    left=(left*31)/100;
    right=(right*31)/100;

	
    sprintf(volumebuf,"amixer cset <SPKOUT Playback Volume> %d,%d",left,right);
    ALSA_UTILS::MixerCSet(volumebuf);
    left_vol = l;
    right_vol = r;
    return 0;
}

int AudioPlatformRT5625::OnBoardSpeaker::Enable(bool enable)
{
	
    AudioDeviceManager& adm= AudioDeviceManager::getInstance();
    DBG("%s onboard speaker!!!",(true==enable)?"enable":"disable");
    if(true==enable)
    { 
        #ifdef ENABLE_HDMI_PORT
        if(AudioSettings::HDMI_AUDIO_ENABLE&&HDMI_UTILS::HdmiServerRunning())
        {
            int display_mode = HDMI_UTILS::HdmiDisplayMode();
            if( display_mode == DISPLAY_MODE_MIRROR 
                || display_mode == DISPLAY_MODE_HDMI_ONLY ) {
                // MIRROR and HDMI ONLY mode, don't enable speaker
                
                // we should not mute HDMI audio
                HDMI_UTILS::HdmiSetAudioMute(false); 
                
                return 0;
            }
        }
        #endif
			ALSA_UTILS::MixerCSet("amixer cset <SPK Mixer DAC Mixer Playback Switch> 1");
			//02H
			ALSA_UTILS::MixerCSet("amixer cset <SPKOUT Playback Switch> 1,1");
            //mux from speaker mixer
			ALSA_UTILS::MixerCSet("amixer cset <SPKOUT Mux> 2");
            
    }
    else
    {   
        ALSA_UTILS::MixerCSet("amixer cset <SPK Mixer DAC Mixer Playback Switch> 0");    
        ALSA_UTILS::MixerCSet("amixer cset <SPKOUT Playback Switch> 0,0");
        ALSA_UTILS::MixerCSet("amixer cset <SPKOUT Playback Volume> 31,31");
    }
    enabled = enable;
    
    return 0;

}

int AudioPlatformRT5625::OnBoardSpeaker::Suspend(bool suspend)
{
    Enable(!suspend);
    return 0;
}
void AudioPlatformRT5625::OnBoardSpeaker::Init()
{
 /*
    numid=4,iface=MIXER,name='SPK Amp Ratio'
  ; type=ENUMERATED,access=rw------,values=1,items=6
  ; Item #0 '2.25 Vdd'
  ; Item #1 '2.00 Vdd'
  ; Item #2 '1.75 Vdd'
  ; Item #3 '1.50 Vdd'
  ; Item #4 '1.25 Vdd'
  ; Item #5 '1.00 Vdd'
  : values=3
  */
    
    ALSA_UTILS::MixerCSet("amixer cset <SPK Amp Ratio> 3");
}

AudioPlatformRT5625::OnBoardMicrophone::OnBoardMicrophone():
AudioDevice(AudioDeviceManager::DEFAULT_ALSA_DEVICENAME,"onboardmicrophone_mic1")
{
}
int AudioPlatformRT5625::OnBoardMicrophone::SetVolume(float l,float r)
{
    char volumebuf[256];
    int left,right;
    left = l*100;
    right=r*100;
    left=(left*31)/100;
    right=(right*31)/100;
    
    sprintf(volumebuf,"amixer cset <PCM Capture Volume> %d,%d",left,right);
    ALSA_UTILS::MixerCSet(volumebuf);
    left_vol = l;
    right_vol = r;

    return 0;
}

int AudioPlatformRT5625::OnBoardMicrophone::Enable(bool enable)
{
    DBG("%s onboard microphone!!!",(true==enable)?"enable":"disable");

    if(true==enable)
    {
        ALSA_UTILS::MixerCSet("amixer cset <Mic1 Amp Boost Type> 1");  
        ALSA_UTILS::MixerCSet("amixer cset <Left Rec Mixer Mic1 Capture Switch> 1");
        ALSA_UTILS::MixerCSet("amixer cset <Right Rec Mixer Mic1 Capture Switch> 1");     
    }
    else
    {        
        ALSA_UTILS::MixerCSet("amixer cset <Left Rec Mixer Mic1 Capture Switch> 0");
        ALSA_UTILS::MixerCSet("amixer cset <Right Rec Mixer Mic1 Capture Switch> 0");     
    }

    enabled = enable;
    return 0;

}

int AudioPlatformRT5625::OnBoardMicrophone::Suspend(bool suspend)
{
    Enable(!suspend);
    return 0;
}

void AudioPlatformRT5625::OnBoardMicrophone::Init()
{
    
}


//
//HeadPhone (speaker only) 
//
AudioPlatformRT5625::HeadPhoneSpeaker::HeadPhoneSpeaker():
HeadSetSpeaker(AudioDeviceManager::DEFAULT_ALSA_DEVICENAME,"hpspeaker")
{

}


int AudioPlatformRT5625::HeadPhoneSpeaker::SetVolume(float l,float r)
{
    return HeadSetSpeaker::SetVolume(l,r);
}

int AudioPlatformRT5625::HeadPhoneSpeaker::Enable(bool enable)
{
    return HeadSetSpeaker::Enable(enable);
}

int AudioPlatformRT5625::HeadPhoneSpeaker::Suspend(bool suspend)
{
    return HeadSetSpeaker::Suspend(suspend);
}

//
//Headset 
//
AudioPlatformRT5625::HeadSetSpeaker::HeadSetSpeaker():
AudioDevice(AudioDeviceManager::DEFAULT_ALSA_DEVICENAME,"hsspeaker")
{
}
AudioPlatformRT5625::HeadSetSpeaker::HeadSetSpeaker(const char* dev,const char* alias):
AudioDevice::AudioDevice(dev,alias)
{
}

int AudioPlatformRT5625::HeadSetSpeaker::SetVolume(float l,float r)
{
    char volumebuf[256];
    int left,right;
    left = l*100;
    right=r*100;
    left=(left*31)/100;
    right=(right*31)/100;
    
    sprintf(volumebuf,"amixer cset <HPOUT Playback Volume> %d,%d",left,right);
    ALSA_UTILS::MixerCSet(volumebuf);
    left_vol = l;
    right_vol = r;

    return 0;
}

int AudioPlatformRT5625::HeadSetSpeaker::Enable(bool enable)
{
    DBG("%s headset speaker!!!",(true==enable)?"enable":"disable");
    AudioDeviceManager& adm= AudioDeviceManager::getInstance();

    if(true==enable)
    {
        #ifdef ENABLE_HDMI_PORT
        if(AudioSettings::HDMI_AUDIO_ENABLE&&HDMI_UTILS::HdmiServerRunning())
        {
            if(DISPLAY_MODE_VIDEO  != HDMI_UTILS::HdmiDisplayMode())
            {
                // if not video mode, we should mute HDMI audio
                HDMI_UTILS::HdmiSetAudioMute(true); 
            }
        }        
        #endif
		
		{
			//04H HPOUT Playback Switch
			ALSA_UTILS::MixerCSet("amixer cset <HPOUT Playback Switch> 1,1");		
			//02H
			//ALSA_UTILS::MixerCSet("amixer cset <SPKOUT Playback Switch> 1,1");		

            //HP out mux from HP mixer
			//1CH
			ALSA_UTILS::MixerCSet("amixer cset <HPROUT Mux> 1");
			ALSA_UTILS::MixerCSet("amixer cset <HPLOUT Mux> 1");

			
		}
        
    }
    else
    {           
        
        #if 0 //HDMI server will handle this
        if(AudioSettings::HDMI_AUDIO_ENABLE&&HDMI_UTILS::HdmiServerRunning())
            HDMI_UTILS::HdmiSetAudioMute(false); 
        #endif
        ALSA_UTILS::MixerCSet("amixer cset <HPOUT Playback Switch> 0,0");      
    }

    enabled = enable;

    return 0;

}

int AudioPlatformRT5625::HeadSetSpeaker::Suspend(bool suspend)
{
    Enable(!suspend);
    return 0;

}

void AudioPlatformRT5625::HeadSetSpeaker::Init()
{
    //don't higher the volume to maximum,otherwise it may cause headset switch enter a dead loop

}


AudioPlatformRT5625::HeadSetMicrophone::HeadSetMicrophone():
AudioDevice(AudioDeviceManager::DEFAULT_ALSA_DEVICENAME,"hsmicrophone_mic2")
{
}
int AudioPlatformRT5625::HeadSetMicrophone::SetVolume(float l,float r)
{
    char volumebuf[256];
    int left,right;
    left = l*100;
    right=r*100;
    left=(left*31)/100;
    right=(right*31)/100;
    
    sprintf(volumebuf,"amixer cset <PCM Capture Volume> %d,%d",left,right);
    ALSA_UTILS::MixerCSet(volumebuf);
    left_vol = l;
    right_vol = r;

    return 0;
}

int AudioPlatformRT5625::HeadSetMicrophone::Enable(bool enable)
{
    DBG("%s headset microphone!!!",(true==enable)?"enable":"disable");

    if(true==enable)
    {
        ALSA_UTILS::MixerCSet("amixer cset <Right Rec Mixer Mic2 Capture Switch> 1");     
        ALSA_UTILS::MixerCSet("amixer cset <Left Rec Mixer Mic2 Capture Switch> 1");
    }
    else
    {        
        ALSA_UTILS::MixerCSet("amixer cset <Left Rec Mixer Mic2 Capture Switch> 0");
        ALSA_UTILS::MixerCSet("amixer cset <Right Rec Mixer Mic2 Capture Switch> 0");     
    }

    enabled = enable;

    return 0;

}

int AudioPlatformRT5625::HeadSetMicrophone::Suspend(bool suspend)
{
    return 0;

}

void AudioPlatformRT5625::HeadSetMicrophone::Init()
{
    ALSA_UTILS::MixerCSet("amixer cset <Mic2 Amp Boost Type> 0");  
    ALSA_UTILS::MixerCSet("amixer cset <AEC Mode> 3");    
}

//
// Bluetooth 
//
AudioPlatformRT5625::BluetoothSpeaker::BluetoothSpeaker():
AudioDevice(AudioDeviceManager::DEFAULT_ALSA_DEVICENAME,"btspeaker")
{
}
int AudioPlatformRT5625::BluetoothSpeaker::SetVolume(float l,float r)
{
    return 0;
}

int AudioPlatformRT5625::BluetoothSpeaker::Enable(bool enable)
{
    if(enable)
    {
        #ifdef ENABLE_HDMI_PORT
        if(AudioSettings::HDMI_AUDIO_ENABLE&&HDMI_UTILS::HdmiServerRunning())
        {
            if(DISPLAY_MODE_VIDEO  != HDMI_UTILS::HdmiDisplayMode())
            {
                // if not video mode, we should mute HDMI audio
                HDMI_UTILS::HdmiSetAudioMute(true); 
            }
        }        
        #endif
        ALSA_UTILS::MixerCSet("amixer cset <Right HP Mixer HIFI DAC Playback Switch> 1");
        ALSA_UTILS::MixerCSet("amixer cset <Right Rec Mixer HP Mixer Capture Switch> 1");        
        ALSA_UTILS::MixerCSet("amixer cset <Voice DAC Mute> 0");     
        ALSA_UTILS::MixerCSet("amixer cset <Voice DAC Enable> 1");     
        ALSA_UTILS::MixerCSet("amixer cset <Voice DAC Power> 1");     
        ALSA_UTILS::MixerCSet("amixer cset <Voice CLK Power> 1");     
        ALSA_UTILS::MixerCSet("amixer cset <ADCR Func> 1");     //right channel to voice adc
        
        
    }
    else
    {   
        #if 0
        if(AudioSettings::HDMI_AUDIO_ENABLE&&HDMI_UTILS::HdmiServerRunning())
            HDMI_UTILS::HdmiSetAudioMute(false); 
        #endif
        ALSA_UTILS::MixerCSet("amixer cset <Voice CLK Power> 0");     
        ALSA_UTILS::MixerCSet("amixer cset <Voice DAC Mute> 1");     
        ALSA_UTILS::MixerCSet("amixer cset <Voice DAC Enable> 0");     
        ALSA_UTILS::MixerCSet("amixer cset <Voice DAC Power> 0");    
        
        //don't disable these switches since HDMI server need this
        //ALSA_UTILS::MixerCSet("amixer cset <Right HP Mixer HIFI DAC Playback Switch> 0");
        ALSA_UTILS::MixerCSet("amixer cset <Right Rec Mixer HP Mixer Capture Switch> 0");        
    }

    enabled = enable;

    return 0;

}

int AudioPlatformRT5625::BluetoothSpeaker::Suspend(bool suspend)
{
    return 0;

}

AudioPlatformRT5625::BluetoothMicrophone::BluetoothMicrophone():
AudioDevice(AudioDeviceManager::DEFAULT_ALSA_DEVICENAME,"btmicrophone")
{
}
int AudioPlatformRT5625::BluetoothMicrophone::SetVolume(float l,float r)
{
    return 0;
}

int AudioPlatformRT5625::BluetoothMicrophone::Enable(bool enable)
{
    if(enable)
    {
        ALSA_UTILS::MixerCSet("amixer cset <MoNo Mixer Voice DAC Playback Switch> 1");
        ALSA_UTILS::MixerCSet("amixer cset <Left Rec Mixer MoNo Mixer Capture Switch> 1");
        //enable ADCR function for bt
        ALSA_UTILS::MixerCSet("amixer cset <ADCR Func> 1");
       
    }
    else
    {            
        ALSA_UTILS::MixerCSet("amixer cset <Left Rec Mixer MoNo Mixer Capture Switch> 0");
        ALSA_UTILS::MixerCSet("amixer cset <MoNo Mixer Voice DAC Playback Switch> 0");
        //disable ADCR function for bt
        ALSA_UTILS::MixerCSet("amixer cset <ADCR Func> 0");
    }

    enabled = enable;

    return 0;

}

int AudioPlatformRT5625::BluetoothMicrophone::Suspend(bool suspend)
{
    return 0;
}
void AudioPlatformRT5625::BluetoothMicrophone::Init()
{
    ALSA_UTILS::MixerCSet("amixer cset <Voice Volume> 15");    
}

//
// FM Radio 
//
AudioPlatformRT5625::FMSpeaker::FMSpeaker():
AudioDevice(AudioDeviceManager::DEFAULT_ALSA_DEVICENAME,"fmspeaker")
{
}
int AudioPlatformRT5625::FMSpeaker::SetVolume(float l,float r)
{
    char volumebuf[256];
    int left,right;
    left = l*100;
    right=r*100;
    left=(left*31)/100;
    right=(right*31)/100;
    
    sprintf(volumebuf,"amixer cset <LineIn Playback Volume> %d,%d",left,right);
    ALSA_UTILS::MixerCSet(volumebuf);
    left_vol = l;
    right_vol = r;
    return 0;
}

int AudioPlatformRT5625::FMSpeaker::Enable(bool enable)
{
    if(enable)
    {
        ALSA_UTILS::MixerCSet("amixer cset <Left HP Mixer LineIn Playback Switch> 1");       
        ALSA_UTILS::MixerCSet("amixer cset <Right HP Mixer LineIn Playback Switch> 1");       
        
    }
    else
    {            
        ALSA_UTILS::MixerCSet("amixer cset <Left HP Mixer LineIn Playback Switch> 0");       
        ALSA_UTILS::MixerCSet("amixer cset <Right HP Mixer LineIn Playback Switch> 0");       
    }

    enabled = enable;

    return 0;

}

int AudioPlatformRT5625::FMSpeaker::Suspend(bool suspend)
{
    return 0;
}

AudioPlatformRT5625::FMHeadPhone::FMHeadPhone():
AudioDevice(AudioDeviceManager::DEFAULT_ALSA_DEVICENAME,"fmheadphone")
{
}
int AudioPlatformRT5625::FMHeadPhone::SetVolume(float l,float r)
{
    char volumebuf[256];
    int left,right;
    left = l*100;
    right=r*100;
    left=(left*31)/100;
    right=(right*31)/100;
    
    sprintf(volumebuf,"amixer cset <LineIn Playback Volume> %d,%d",left,right);
    ALSA_UTILS::MixerCSet(volumebuf);
    left_vol = l;
    right_vol = r;

    return 0;
}

int AudioPlatformRT5625::FMHeadPhone::Enable(bool enable)
{
    if(enable)
    {
        ALSA_UTILS::MixerCSet("amixer cset <Right HP Mixer LineIn Playback Switch> 1");
        ALSA_UTILS::MixerCSet("amixer cset <Left HP Mixer LineIn Playback Switch> 1");
    }
    else
    {            
        ALSA_UTILS::MixerCSet("amixer cset <Right HP Mixer LineIn Playback Switch> 0");
        ALSA_UTILS::MixerCSet("amixer cset <Left HP Mixer LineIn Playback Switch> 0");
    }

    enabled = enable;

    return 0;

}

int AudioPlatformRT5625::FMHeadPhone::Suspend(bool suspend)
{
    return 0;

}


};
