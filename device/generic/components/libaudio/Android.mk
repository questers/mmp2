LOCAL_PATH:= $(call my-dir)

ifneq ($(BOARD_USES_GENERIC_AUDIO),true)

#
#build libaudio.so
#
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	AudioLog.cpp \
	AlsaUtils.cpp \
	AudioSetting.cpp \
	AudioHardware.cpp \
	CallManager.cpp \
	AudioStreamDump.cpp \
	AudioDevice.cpp \
	AudioPlatform.cpp \
	AudioPlatformStub.cpp \
	AudioPlatformRt5625.cpp \
	AudioPlatformRt5631.cpp \
	AudioDeviceManager.cpp

LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libutils \
    libmedia \
    libasound \
    libhardware_legacy \
    libbinder \
    libhdmiservice

LOCAL_STATIC_LIBRARIES += libaudiointerface
	
LOCAL_C_INCLUDES += \
	external/alsa/alsa-lib/include 

ifeq ($(BOARD_HAVE_HDMI_SERVICE),true)	
LOCAL_SRC_FILES+= \
	HdmiUtils.cpp 
LOCAL_C_INCLUDES += \
	device/generic/components/HdmiService/Service 
LOCAL_SHARED_LIBRARIES += \
	libhdmiservice
LOCAL_CFLAGS += -DENABLE_HDMI_PORT	
endif	
    
ifeq ($(BOARD_HAVE_VOICECALL_SERVICE),true) 
LOCAL_C_INCLUDES += \
	device/generic/components/CallService	
LOCAL_SHARED_LIBRARIES += \
	libvoicecall
LOCAL_CFLAGS += -DENABLE_VOICE_MODEM	
endif


LOCAL_MODULE:= libaudio

ifeq ($(BOARD_HAVE_BLUETOOTH),true)
  LOCAL_SHARED_LIBRARIES += liba2dp
endif

ifeq ($(BOARD_ENABLE_ECPATH),true)
  LOCAL_CFLAGS += -DWITH_ECPATH
endif

ifeq ($(BOARD_ENABLE_SRSWITCH),true)
  LOCAL_CFLAGS += -DWITH_SRSWITCH
endif

ifeq ($(BOARD_SUPPORT_MEDIA_SET_PROPERTY),true)
  LOCAL_CFLAGS += -DUSE_PROPERTY
endif

ifeq ($(BOARD_USES_AUDIO_PATH_STUB),true)
  LOCAL_CFLAGS += -DAUDIO_PATH_STUB
endif


LOCAL_CFLAGS += -DPLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION) -D_POSIX_SOURCE

include $(BUILD_SHARED_LIBRARY)

#
#Build libaudiopolicy.so
#
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
		AudioSetting.cpp \
        AudioPolicyManager.cpp 

LOCAL_C_INCLUDES += \
        external/alsa/alsa-lib/include 

ifeq ($(BOARD_HAVE_HDMI_SERVICE),true)	
LOCAL_C_INCLUDES += \
	device/generic/components/HdmiService/Service 
LOCAL_SHARED_LIBRARIES += \
	libhdmiservice
LOCAL_CFLAGS += -DENABLE_HDMI_PORT	
endif	
        


LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libutils \
    libmedia \
    libhardware_legacy \
    libbinder \

ifneq ($(PLATFORM_SDK_VERSION), 7)
LOCAL_WHOLE_STATIC_LIBRARIES += libaudiopolicybase
endif

LOCAL_MODULE:= libaudiopolicy

ifeq ($(BOARD_HAVE_BLUETOOTH),true)
  LOCAL_CFLAGS += -DWITH_A2DP
endif

ifeq ($(BOARD_USES_AUDIO_PATH_STUB),true)
  LOCAL_CFLAGS += -DAUDIO_PATH_STUB
endif

LOCAL_CFLAGS += -DPLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION) -D_POSIX_SOURCE

include $(BUILD_SHARED_LIBRARY)
endif


