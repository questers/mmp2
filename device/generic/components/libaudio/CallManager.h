#ifndef _CALL_MANAGER_H
#define _CALL_MANAGER_H


#include <utils/Singleton.h>

namespace android
{

class CallManager :public Singleton<CallManager>
{
	friend class Singleton<CallManager>;

	CallManager(const CallManager&);
	CallManager& operator = (const CallManager&);

	protected:
		CallManager();
		virtual ~CallManager();
		bool CallServerAlive(void);
	 
	 public:
	 	int startCall();
		int endCall();
		int setVolume(float vol);
		int setPath(int path);
		
};



}
	
#endif
