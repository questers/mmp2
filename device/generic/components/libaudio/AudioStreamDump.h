#ifndef _AUDIO_STREAM_DUMP_H
#define _AUDIO_STREAM_DUMP_H

#include <stdio.h>
#include <utils/Timers.h>
#include <utils/Singleton.h>

namespace android
{


class StreamDumper
{
	public:
		StreamDumper(const char* name);	

		bool StreamStarted(){return started;}
		
		virtual int StreamClose();
		virtual int StreamStart();
		virtual int StreamDump(const void* buffer,size_t bytes);
		
	protected:
		StreamDumper(){};
		virtual ~StreamDumper();
	private:
		const char* stream;
		FILE* dumpFD;
		DurationTimer timer;
		bool started;
};


class DownStreamDumper :public Singleton<DownStreamDumper>,public StreamDumper
{
	friend class Singleton<DownStreamDumper>;

	DownStreamDumper(const DownStreamDumper&);
	DownStreamDumper& operator = (const DownStreamDumper&);	 

	protected:
	 	DownStreamDumper():StreamDumper("downstream"){}

	public:
		virtual int StreamDump(const void* buffer,size_t bytes);
		
		
};

class UpStreamDumper :public Singleton<UpStreamDumper>,public StreamDumper
{
	friend class Singleton<UpStreamDumper>;

	UpStreamDumper(const UpStreamDumper&);
	UpStreamDumper& operator = (const UpStreamDumper&);
	 
	protected:
		 UpStreamDumper():StreamDumper("upstream"){}
	public:
		virtual int StreamDump(const void* buffer,size_t bytes);
		
		
};



};
	
#endif
