#ifndef _HDMI_UTILS_H
#define _HDMI_UTILS_H
namespace android{

class HDMI_UTILS
{
	public:
    static int HdmiDisplayMode();
	static int HdmiSetAudioMute(bool mute=true);
	static bool HdmiServerRunning();
};
};
#endif

