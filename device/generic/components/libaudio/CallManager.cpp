#include <binder/IServiceManager.h>
#include <binder/IPCThreadState.h>
#ifdef ENABLE_VOICE_MODEM
#include <IVoiceCall.h>
#endif
#include "CallManager.h"


namespace android
{

ANDROID_SINGLETON_STATIC_INSTANCE( CallManager )
    
CallManager::CallManager()
{

}
CallManager::~CallManager()
{

}


bool CallManager::CallServerAlive()
{
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder = sm->checkService(String16("media.voicecallserver"));   
    return (binder!=0)?true:false;

}

int CallManager::startCall()
{
    #ifdef ENABLE_VOICE_MODEM
    if(!CallServerAlive()) return -1;
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder = sm->getService(String16("media.voicecallserver"));   
    if( binder != 0 ) {
        sp<IVoiceCall> mVoiceCall = interface_cast<IVoiceCall>(binder);
        return mVoiceCall->startCall();
    }
    
    return -1;
    #else
    //always return success
    return 0;
    #endif
}

int CallManager::endCall()
{    
    #ifdef ENABLE_VOICE_MODEM
    if(!CallServerAlive()) return -1;

    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder = sm->getService(String16("media.voicecallserver"));   
    if( binder != 0 ) {
        sp<IVoiceCall> mVoiceCall = interface_cast<IVoiceCall>(binder);
        return mVoiceCall->endCall();
    }
    return -1;
    #else
    //always return success
    return 0;
    #endif

}

int CallManager::setVolume(float vol)
{
    #ifdef ENABLE_VOICE_MODEM

    if(!CallServerAlive()) return -1;

    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder = sm->getService(String16("media.voicecallserver"));   
    if( binder != 0 ) {
        sp<IVoiceCall> mVoiceCall = interface_cast<IVoiceCall>(binder);
        return mVoiceCall->setVolume(vol);
    }
    return -1;
    #else
    //always return success
    return 0;
    #endif

}

int CallManager::setPath(int path)
{
    
    #ifdef ENABLE_VOICE_MODEM
    if(!CallServerAlive()) return -1;

    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder = sm->getService(String16("media.voicecallserver"));   
    if( binder != 0 ) {
        sp<IVoiceCall> mVoiceCall = interface_cast<IVoiceCall>(binder);
        return mVoiceCall->setVoicePath(path);
    }
    return -1;
    #else
    //always return success
    return 0;    
    #endif
}


}

