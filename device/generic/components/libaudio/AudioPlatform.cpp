#include <AudioPlatform.h>

namespace android {

AudioPlatform::AudioPlatform(const char* platform):
mPlatform(platform),
mbVirtual(false),
mAecMode(AEC_MODE_INVALID),
mSceneMode(AudioSystem::MODE_INVALID)
{
}
AudioPlatform::~AudioPlatform(){
}

status_t AudioPlatform::PreDevEnable(uint32_t devices,bool enable)
{
    return NO_ERROR;
}

status_t AudioPlatform::PostDevEnable(uint32_t devices,bool enable)
{
    return NO_ERROR;
}

};
