#undef __STRICT_ANSI__
#define __STDINT_LIMITS
#define __STDC_LIMIT_MACROS
#include <stdint.h>
#include <sys/time.h>
#include <stdio.h>
#include <sys/types.h>

#include <stdlib.h>
#include <unistd.h>

#include <cutils/properties.h>

#include <media/AudioSystem.h>

#ifdef ENABLE_HDMI_PORT
#include "IHdmiService.h"
#endif
#include "AudioSetting.h"
#include "AudioLog.h"
#include "AlsaUtils.h"
#include "AudioDevice.h"
#include "AudioDeviceManager.h"
#include "CallManager.h"

#ifdef ENABLE_VOICE_MODEM
#include <IVoiceCall.h>
#endif

#include <AudioPlatform.h>
#include <AudioPlatformRt5625.h>
#include <AudioPlatformRt5631.h>
#include <AudioPlatformStub.h>


namespace android {


const char* AudioDeviceManager::DEFAULT_ALSA_DEVICENAME ="plughw:0,0" ;

//
//
//AudioPath Manager
//
//

ANDROID_SINGLETON_STATIC_INSTANCE( AudioDeviceManager )


AudioDeviceManager::AudioDeviceManager():
initChecked(false),
aec_permitted(true)
{
}
AudioDeviceManager::~AudioDeviceManager()
{
    mAdmCommandThread->exit();
    mAdmCommandThread.clear();

	if(mManagedDevices.size())
	{
		AudioDevice* audioDev;
		for(uint32_t i=0;i<mManagedDevices.size();i++)
		{
			audioDev = mManagedDevices.valueAt(i);
			delete audioDev;
		}
	}
	mManagedDevices.clear();
	enabledDevices=0;
	mInDeviceSuspended=false;
	mOutDeviceSuspended=false;
	mMode=AudioSystem::MODE_INVALID;
}

status_t AudioDeviceManager::initCheck(void)
{
    
    if(false==initChecked)
	{

        //init checking code
        init();
        initChecked = true;
    }
	
	return hardware_status;
}

char* AudioDeviceManager::dumpDevices(uint32_t devices)
{
    static char buf[1024];
    if(!devices){
        sprintf(buf,"null");
    }else{
    sprintf(buf,"[%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s]",
    (devices&AudioSystem::DEVICE_OUT_EARPIECE)?"o_earpiece|":"",
    (devices&AudioSystem::DEVICE_OUT_SPEAKER)?"o_speaker|":"",    
    (devices&AudioSystem::DEVICE_OUT_WIRED_HEADSET)?"o_wired_headset|":"",    
    (devices&AudioSystem::DEVICE_OUT_WIRED_HEADPHONE)?"o_wired_headphone|":"",    
    (devices&AudioSystem::DEVICE_OUT_BLUETOOTH_SCO)?"o_bluetooth_sco|":"",    
    (devices&AudioSystem::DEVICE_OUT_BLUETOOTH_SCO_HEADSET)?"o_bluetooth_sco_headset|":"",    
    (devices&AudioSystem::DEVICE_OUT_BLUETOOTH_SCO_CARKIT)?"o_bluetooth_sco_carkit|":"",    
    (devices&AudioSystem::DEVICE_OUT_BLUETOOTH_A2DP)?"o_bluetooth_a2dp|":"",    
    (devices&AudioSystem::DEVICE_OUT_BLUETOOTH_A2DP_HEADPHONES)?"o_bluetooth_a2dp_headphones|":"",    
    (devices&AudioSystem::DEVICE_OUT_BLUETOOTH_A2DP_SPEAKER)?"o_bluetooth_a2dp_speaker|":"",    
    (devices&AudioSystem::DEVICE_OUT_AUX_DIGITAL)?"o_aux_digital|":"",    
    (devices&AudioSystem::DEVICE_OUT_FM_HEADPHONE)?"o_fm_headphone|":"",    
    (devices&AudioSystem::DEVICE_OUT_FM_SPEAKER)?"o_fm_speaker|":"",        
    #ifdef ENABLE_HDMI_PORT
    (devices&AudioSystem::DEVICE_OUT_HDMI)?"o_hdmi|":"",    
    #else
    "",
    #endif
    (devices&AudioSystem::DEVICE_IN_COMMUNICATION)?"i_communication|":"",    
    (devices&AudioSystem::DEVICE_IN_AMBIENT)?"i_ambient|":"",    
    (devices&AudioSystem::DEVICE_IN_BUILTIN_MIC)?"i_builtin_mic|":"",    
    (devices&AudioSystem::DEVICE_IN_BLUETOOTH_SCO_HEADSET)?"i_bluetooth_sco_headset|":"",    
    (devices&AudioSystem::DEVICE_IN_WIRED_HEADSET)?"i_wired_headset|":"",    
    (devices&AudioSystem::DEVICE_IN_AUX_DIGITAL)?"i_aux_digital|":"",    
    (devices&AudioSystem::DEVICE_IN_VOICE_CALL)?"i_voicecall|":"",    
    (devices&AudioSystem::DEVICE_IN_BACK_MIC)?"i_back_mic|":"",    
    (devices&AudioSystem::DEVICE_IN_VT_MIC)?"i_vt_mic|":"",    
    (devices&AudioSystem::DEVICE_IN_FMRADIO)?"i_fmradio":""
    );
    }
    return buf;
}

//
//single device operations
//
const char * AudioDeviceManager::getDevName(uint32_t device)
{	
    static const char* def_devname="plughw:0,0";
    if(1!=AudioSystem::popCount(device))
        return def_devname;
	if(mManagedDevices.size())
	{
	    int index = mManagedDevices.indexOfKey(device);
        if(index>=0)
		{
		    AudioDevice* audioDev=mManagedDevices.valueFor(device);
            return audioDev->Name();        
        }
	}
    DBG("getDevName return default name");
	return def_devname;
}

bool AudioDeviceManager::IsDevSuspended(uint32_t device)
{
	if(mManagedDevices.size())
	{
		AudioDevice* audioDev=mManagedDevices.valueFor(device);
		if(audioDev)
		{			
			return audioDev->IsSuspended();
		}
		
	}
	return false;
	
}
bool AudioDeviceManager::IsDevEnabled(uint32_t device)
{
	if(mManagedDevices.size())
	{
		AudioDevice* audioDev=mManagedDevices.valueFor(device);
		if(audioDev)
		{			
			return audioDev->IsEnabled();
		}
		
	}
	return false;	
}


//
//combined devices operations
//
bool AudioDeviceManager::setDevEnableImpl(uint32_t devices,bool enable)
{
	bool ret=false;
    uint32_t enabled=enabledDevices;
    
    m_AudioPlatform->PreDevEnable(devices,enable);

	if(enable)
		enabledDevices|=devices;
	else
		enabledDevices&=(~devices);
    char log_buf[256];
    sprintf(log_buf,"%s %s  ",enable?"enable":"disable",dumpDevices(devices));
    sprintf(log_buf+strlen(log_buf),"%s->",dumpDevices(enabled));
    sprintf(log_buf+strlen(log_buf),"%s",dumpDevices(enabledDevices));
    INFO("%s ",log_buf);
	
	for(unsigned i=0;i<mManagedDevices.size();i++)
	{
		uint32_t device=mManagedDevices.keyAt(i);
		if(devices&device)
		{
			AudioDevice* audioDev = mManagedDevices.valueFor(device);
            ret = audioDev->Enable(enable);         
		}
		
	}

    m_AudioPlatform->PostDevEnable(devices,enable);


	
	return ret;
	
}

bool AudioDeviceManager::muteMic(bool mute)
{
	bool ret=false;

	for(unsigned i=0;i<mManagedDevices.size();i++)
	{
		uint32_t device=mManagedDevices.keyAt(i);
		if(AudioSystem::DEVICE_IN_ALL&device)
		{
			AudioDevice* audioDev = mManagedDevices.valueFor(device);
			ret = audioDev->Mute(mute);			
		}
	}
	
	return ret;
}

bool AudioDeviceManager::setDevRouteImpl(uint32_t old_devices,uint32_t new_devices)
{
	uint32_t old_devs,new_devs,device,i;
	AudioDevice* audioDev;
	bool ret=true;

	old_devs=old_devices-(old_devices&new_devices);
	new_devs=new_devices-(old_devices&new_devices);
	
	DBG("RoutingRequest,old_devs=0x%x[0x%x],new_devs=0x%x[0x%x]",old_devs,old_devices,new_devs,new_devices);
	
	//disable old routing
	if(old_devs)
	{
		ret=setDevEnableImpl(old_devs,false);
	}

	//we have to enable each device should be enabled again after disabling system requested
	//device
    ret = setDevEnableImpl(new_devs,true);
	
	return ret;
}

bool AudioDeviceManager::setDevVolumeImpl(uint32_t devices,float left_vol,float right_vol)
{
	bool ret=false;
	for(unsigned i=0;i<mManagedDevices.size();i++)
	{
		uint32_t device=mManagedDevices.keyAt(i);
		if(device==AudioSystem::DEVICE_OUT_EARPIECE||device==AudioSystem::DEVICE_OUT_SPEAKER)
		{
			AudioDevice* audioDev = mManagedDevices.valueFor(device);
			ret = audioDev->SetVolume(left_vol,right_vol);					
		}		
		if(devices&device)
		{

			if(device==AudioSystem::DEVICE_OUT_BLUETOOTH_SCO_HEADSET)
			{
				AudioDevice* audioDev = mManagedDevices.valueFor(device);
				ret = audioDev->SetVolume(left_vol,right_vol);	
			}
			// all input devices share the same volume control command
		}
		
	}
	
	return ret;	
}


bool AudioDeviceManager::setDevStandbyImpl(uint32_t devices,bool standby)
{
	bool ret=false;
	for(unsigned i=0;i<mManagedDevices.size();i++)
	{
		uint32_t device=mManagedDevices.keyAt(i);
		if(devices&device&&enabledDevices&device)
		{
			AudioDevice* audioDev = mManagedDevices.valueFor(device);
			ret = audioDev->Suspend(standby);	
		}
	}
	if(devices&AudioSystem::DEVICE_IN_ALL)
		mInDeviceSuspended=standby;
	if(devices&AudioSystem::DEVICE_OUT_ALL)
		mOutDeviceSuspended=standby;
	return ret;	
	
}

status_t AudioDeviceManager::setModeImpl(int mode)
{
	mMode=mode;
    
    if(m_AudioPlatform)
        m_AudioPlatform->setModeImpl(mode);
    
	return NO_ERROR;
}

void AudioDeviceManager::setAecImpl(int mode)
{
    if(!aec_permitted) return;

    if(m_AudioPlatform)
        m_AudioPlatform->setAecImpl(mode);
}



void AudioDeviceManager::Dump(void)
{
	DBG("======Dump Audio Path======");
	for(unsigned i=0;i<mManagedDevices.size();i++)
	{
		AudioDevice* dev = mManagedDevices.valueAt(i);
		if(dev)
		{
			DBG("@%d,dev[%s],alias[%s]",
				i,
				(dev->Name())?dev->Name():"null",
				(dev->Alias())?dev->Alias():"null");
		}
		
	}
	DBG("==========================");
}

//add devices to be managed here
bool AudioDeviceManager::init(void)
{
	//
	//Audio Path Manager Init code
	//
	
	m_AudioPlatform = NULL;
	mInDeviceSuspended=false;
	mOutDeviceSuspended=false;
	mMode=AudioSystem::MODE_NORMAL;
	enabledDevices=0;	

    String8 card;
    hardware_status = ALSA_UTILS::SoundCardCheck(card); 
    if((card.find("rt5625")>=0)||(card.find("RT5625")>=0)){
        m_AudioPlatform = new AudioPlatformRT5625();
    }else if((card.find("rt5631")>=0)||(card.find("RT5631")>=0)){
        m_AudioPlatform = new AudioPlatformRT5631();
    }else {
        m_AudioPlatform = new AudioPlatformStub();
    }

    ALWAYS("Audio Platform->%s\n",m_AudioPlatform->Name());
    m_AudioPlatform->Init();
    
    Dump();
    
    
    #ifdef ENABLE_VOICE_MODEM
	
    ALWAYS("[%s]%d:",__func__,__LINE__);
    mVoicePath = IVoiceCall::kVoicePathDefault;
    #endif
	char value[PROPERTY_VALUE_MAX];    
    property_get("persist.audio.aec", value, "on");
    if(strcmp(value,"on"))
    {
      aec_permitted = false;
    }
    ALWAYS("[%s]%d:",__func__,__LINE__);

	
    // start audio commands thread
    mAdmCommandThread = new AdmCommandThread(String8("AdmCommandThread"));
    
    ALWAYS("[%s]%d:",__func__,__LINE__);

    //init devices
    AudioDevice* audioDev;
    int size = mManagedDevices.size();
	for(int i=0;i<size;i++)
	{
		audioDev = mManagedDevices.valueAt(i);
		audioDev->Enable(false);		
		audioDev->Mute(true);					
        audioDev->Init();
	}

    if(m_AudioPlatform->IsVirtual())
        hardware_status = ~NO_ERROR;
    else
        hardware_status = NO_ERROR;

    property_get("persist.audio.stub", value, "off");
    if(strcmp(value,"off"))
    {
        hardware_status = ~NO_ERROR;
    }
    
        
    DBG("AudioDeviceManager init over hardware_status=%d\n",hardware_status);
	return true;
	
}

void AudioDeviceManager::registerDevice(uint32_t type, AudioDevice* dev)
{
    mManagedDevices.add(type,dev);      
    
}

void AudioDeviceManager::setDevEnable(uint32_t devices,bool enable)
{
    mAdmCommandThread->DevEnableCommand(devices,enable);
    
    #ifdef ENABLE_VOICE_MODEM
    int voicepath=IVoiceCall::kVoicePathHandfree;
    if(enabledDevices&(AudioSystem::DEVICE_OUT_WIRED_HEADSET
        |AudioSystem::DEVICE_OUT_WIRED_HEADPHONE
        ))
        voicepath = IVoiceCall::kVoicePathHeadset;
    if(voicepath!=mVoicePath)
    {
        DBG("set vc path=%s",(IVoiceCall::kVoicePathHandfree==voicepath)?"handsfree":"headset");
        CallManager::getInstance().setPath(voicepath);
        mVoicePath = voicepath;
    }
    #endif
    
                    
}
void AudioDeviceManager::setDevRoute(uint32_t old_route,uint32_t new_route)
{
    mAdmCommandThread->DevRouteCommand(old_route,new_route );    
}
void AudioDeviceManager::setDevVolume(uint32_t devices,float left_vol,float right_vol)
{
    mAdmCommandThread->DevVolumeCommand(devices,left_vol,right_vol );    
}
void AudioDeviceManager::setDevStandby(uint32_t devices,bool standby)
{
    mAdmCommandThread->DevStandbyCommand(devices,standby );    
}
void AudioDeviceManager::setMode(int mode)
{
    mAdmCommandThread->AudioModeCommand(mode );     
}

void AudioDeviceManager::setCall(bool start)
{
    mAdmCommandThread->VoiceCallCommand(start );    
}

void AudioDeviceManager::setCallVolume(float vol)
{
    mAdmCommandThread->VoiceCallVolumeCommand(vol);    
}
void AudioDeviceManager::setCallPath(int path)
{
    mAdmCommandThread->VoiceCallPathCommand(path);    
}

void AudioDeviceManager::setAecPermission(bool permitted)
{
    aec_permitted = permitted;
}
void AudioDeviceManager::setAec(int mode)
{
    mAdmCommandThread->AecCommand(mode);        
}

// -----------  AudioDeviceManager::AdmCommandThread implementation ----------

AudioDeviceManager::AdmCommandThread::AdmCommandThread(String8 name)
    : Thread(false), mName(name)
{
}


AudioDeviceManager::AdmCommandThread::~AdmCommandThread()
{
    mAdmCommands.clear();
}

void AudioDeviceManager::AdmCommandThread::onFirstRef()
{
    if (mName != "") {
        run(mName.string(), ANDROID_PRIORITY_NORMAL);
    } else {
        run("AdmCommandThread", ANDROID_PRIORITY_NORMAL);
    }
}

bool AudioDeviceManager::AdmCommandThread::threadLoop()
{
    mLock.lock();
    while (!exitPending())
    {
        while(!mAdmCommands.isEmpty()) 
        {
                AdmCommand *command = mAdmCommands[0];
                if(AudioSettings::ADM_COMMAND_DUMP)
                    dumpCommand(command);
                mAdmCommands.removeAt(0);
                mLastCommand = *command;
				DBG("command->mCommand=%d\n",command->mCommand);

                switch (command->mCommand) {
                case COMMAND_DEVICE_ENABLE: 
                    {
                        EnableData *data = (EnableData *)command->mParam;
                        AudioDeviceManager::getInstance().setDevEnableImpl(data->devices,data->enable);
                        command->mStatus = NO_ERROR;
                        if (command->mWaitStatus) {
                            command->mCond.signal();
                            mWaitWorkCV.wait(mLock);
                        }
                        delete data;
                    }
                    break;
                case COMMAND_DEVICE_ROUTE: 
                    {
                        RouteData *data = (RouteData *)command->mParam;
                        AudioDeviceManager::getInstance().setDevRouteImpl(data->old_route,data->new_route);
                        command->mStatus = NO_ERROR;
                        if (command->mWaitStatus) {
                        command->mCond.signal();
                        mWaitWorkCV.wait(mLock);
                        }
                        delete data;
                    }
                    break;
                case COMMAND_DEVICE_VOLUME: 
                    {
                        VolumeData *data = (VolumeData *)command->mParam;
                        AudioDeviceManager::getInstance().setDevVolumeImpl(data->devices,data->l,data->r);
                        command->mStatus = NO_ERROR;
                        if (command->mWaitStatus) {
                        command->mCond.signal();
                        mWaitWorkCV.wait(mLock);
                        }
                        delete data;
                    }
                    break;
                case COMMAND_DEVICE_STANDBY: 
                    {
                        StandbyData *data = (StandbyData *)command->mParam;
                        AudioDeviceManager::getInstance().setDevStandbyImpl(data->devices,data->standby);
                        command->mStatus = NO_ERROR;
                        if (command->mWaitStatus) {
                        command->mCond.signal();
                        mWaitWorkCV.wait(mLock);
                        }
                        delete data;
                    }
                    break;
                case COMMAND_AUDIO_MODE: 
                    {
                        ModeData *data = (ModeData *)command->mParam;
                        AudioDeviceManager::getInstance().setModeImpl(data->mode);
                        command->mStatus = NO_ERROR;
                        if (command->mWaitStatus) {
                        command->mCond.signal();
                        mWaitWorkCV.wait(mLock);
                        }
                        delete data;
                    }
                    break;
                case COMMAND_VOICECALL: 
                    {
                        CallData *data = (CallData *)command->mParam;
                        if(data->start)
                            CallManager::getInstance().startCall();
                        else
                            CallManager::getInstance().endCall();
                        command->mStatus = NO_ERROR;
                        if (command->mWaitStatus) {
                        command->mCond.signal();
                        mWaitWorkCV.wait(mLock);
                        }
                        delete data;
                    }
                    break;
                case COMMAND_VOICECALL_VOLUME: 
                    {
                        CallData *data = (CallData *)command->mParam;                        
                        CallManager::getInstance().setVolume(data->vol);
                        command->mStatus = NO_ERROR;
                        if (command->mWaitStatus) {
                        command->mCond.signal();
                        mWaitWorkCV.wait(mLock);
                        }
                        delete data;
                    }
                    break;
                case COMMAND_VOICECALL_PATH: 
                    {
                        CallData *data = (CallData *)command->mParam;                        
                        CallManager::getInstance().setPath(data->path);
                        command->mStatus = NO_ERROR;
                        if (command->mWaitStatus) {
                        command->mCond.signal();
                        mWaitWorkCV.wait(mLock);
                        }
                        delete data;
                    }
                    break;             
                case COMMAND_AEC: 
                    {
                        AecData*data = (AecData *)command->mParam;                        
                        AudioDeviceManager::getInstance().setAecImpl(data->mode);
                        command->mStatus = NO_ERROR;
                        if (command->mWaitStatus) {
                        command->mCond.signal();
                        mWaitWorkCV.wait(mLock);
                        }
                        delete data;
                    }
                    break;             
               
                default:
                    WARN("AdmCommandThread() unknown command %d", command->mCommand);
                    break;
                }
                delete command;
        }
        INFO("AdmCommandThread() going to sleep");
        mWaitWorkCV.wait(mLock );
        INFO("AdmCommandThread() waking up");
    }
    mLock.unlock();
    return false;
}


void AudioDeviceManager::AdmCommandThread::dumpCommand(AdmCommand* cmd)
{
    if(cmd)
    {
        static const char* cmd_name[]=
        {
            "Enable",
            "Route",
            "Volume",
            "Standby",
            "Mode",
            "VoiceCall",
            "VoiceCallVolume",
            "Unknown"
        };
        if(cmd->mCommand<COMMAND_MAX)
        {
            ALWAYS("AdmCommand \"%s\"   %06d.%03d  %01u    %p\n",
                cmd_name[cmd->mCommand],
                (int)ns2s(cmd->mTime),
                (int)ns2ms(cmd->mTime)%1000,
                cmd->mWaitStatus,
                cmd->mParam);
        }
        
               
    }
}


status_t AudioDeviceManager::AdmCommandThread::DevEnableCommand(uint32_t devices,bool enable )
{
    AdmCommand *command = new AdmCommand();
    command->mCommand = COMMAND_DEVICE_ENABLE;
    EnableData *data = new EnableData();
    data->devices = devices;
    data->enable = enable;
    command->mParam = data;
    return  insertCommand(command,true);
}

status_t   AudioDeviceManager::AdmCommandThread::DevRouteCommand(uint32_t old_route,uint32_t new_route )
{
    AdmCommand *command = new AdmCommand();
    command->mCommand = COMMAND_DEVICE_ROUTE;
    RouteData *data = new RouteData();
    data->old_route = old_route;
    data->new_route = new_route;
    command->mParam = data;
    return  insertCommand(command );
    
}
status_t   AudioDeviceManager::AdmCommandThread::DevVolumeCommand(uint32_t devices,float left_vol,float right_vol )
{
    AdmCommand *command = new AdmCommand();
    command->mCommand = COMMAND_DEVICE_VOLUME;
    VolumeData *data = new VolumeData();
    data->devices = devices;
    data->l = left_vol;
    data->r = right_vol;
    
    command->mParam = data;
    return  insertCommand(command );    
}

status_t AudioDeviceManager::AdmCommandThread::DevStandbyCommand(uint32_t devices,bool standby )
{
    AdmCommand *command = new AdmCommand();
    command->mCommand = COMMAND_DEVICE_STANDBY;
    StandbyData *data = new StandbyData();
    data->devices = devices;
    data->standby= standby;
    command->mParam = data;
    return  insertCommand(command );
    
}

status_t AudioDeviceManager::AdmCommandThread::AudioModeCommand(int mode )
{
    AdmCommand *command = new AdmCommand();
    command->mCommand = COMMAND_AUDIO_MODE;
    ModeData *data = new ModeData();
    data->mode = mode;
    command->mParam = data;
    return  insertCommand(command);
}

status_t AudioDeviceManager::AdmCommandThread::VoiceCallCommand(bool start )
{
    AdmCommand *command = new AdmCommand();
    command->mCommand = COMMAND_VOICECALL;
    CallData *data = new CallData();
    data->start = start;
    command->mParam = data;
    return  insertCommand(command);
}

status_t AudioDeviceManager::AdmCommandThread::VoiceCallVolumeCommand(float vol)
{
    AdmCommand *command = new AdmCommand();
    command->mCommand = COMMAND_VOICECALL_VOLUME;
    CallData *data = new CallData();
    data->vol = vol ;
    command->mParam = data;
    return  insertCommand(command);
}

status_t AudioDeviceManager::AdmCommandThread::VoiceCallPathCommand(int path)
{
    AdmCommand *command = new AdmCommand();
    command->mCommand = COMMAND_VOICECALL_PATH;
    CallData *data = new CallData();
    data->path = path ;
    command->mParam = data;
    return  insertCommand(command);
}
status_t AudioDeviceManager::AdmCommandThread::AecCommand(int mode)
{
    AdmCommand *command = new AdmCommand();
    command->mCommand = COMMAND_AEC;
    AecData *data = new AecData();
    data->mode = mode ;
    command->mParam = data;
    return  insertCommand(command);
    
}

status_t AudioDeviceManager::AdmCommandThread::insertCommand(AdmCommand *command, bool wait_finish)
{
    status_t status = NO_ERROR;
    command->mWaitStatus = wait_finish;
    Mutex::Autolock _l(mLock);
    insertCommand_l(command);
    mWaitWorkCV.signal();
    if (command->mWaitStatus) {
        command->mCond.wait(mLock);
        status =  command->mStatus;
        mWaitWorkCV.signal();
    }
    return status;
}



// insertCommand_l() must be called with mLock held
void AudioDeviceManager::AdmCommandThread::insertCommand_l(AdmCommand *command)
{
    mAdmCommands.add(command);
}

void AudioDeviceManager::AdmCommandThread::exit()
{
    DBG("AdmCommandThread::exit");
    {
        AutoMutex _l(mLock);
        requestExit();
        mWaitWorkCV.signal();
    }
    requestExitAndWait();
}

};
