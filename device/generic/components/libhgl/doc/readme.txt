    
    GC500/GC600/GC800 Unified Graphics Driver for Android
    Ver0.8.0.4040

1.  Introduction
    This package contains GC500/GC600/GC800 unified graphics driver for 
    android and some documents. 
    This document is used to describe the package contents, how to build 
    kernel mode driver and how to install the driver binaries to device.
 
2.  Revision History
    Ver0.8.0.4040
    1) Fix G50 livewall paper flash issue

    Ver0.8.0.3799
    1) Fix unlock screen waitforcondition issue
    2) Add full screen game bypass mode,default off
    3) Fix glTexSubImage failed issue
    4) Temp fix for Neocore "waitforcondition" on exit
    5) Add err check for some nativeWindow->functions

    Ver0.8.0.3478p1
    1) Add external video memory heap to workaround kernel can't allocate none power of 2 memory issue

    Ver0.8.0.3478
    1) Add bgra_hack check incase hack is disabled.

    Ver0.8.0.3465
    1) Fix zte and huawei gallery3D hang issue
    2) Fix NOVA and Sandstorm texture rendering error
    3) Let quadrant go single thread
    4) Add temp fix to let DEQP pass

    Ver0.8.0.3428
    1) Recover bug fix for narutom.com mapUserMemory
    2) Remove error log of map user signal fail

    Ver0.8.0.3412p1
    1) Revert beta1 check-in 3342,3382,3388,3412

    Ver0.8.0.3412
    1) Let quadrant to go single thread, increase about 20 points
    2) Add game patch for quadrant, increase default VB size
    3) Disable the 30 fps limitation for rovio and popcap
    4) Force Quadrant level texture format to RGB565

    Ver0.8.0.3391
    1) Fix oes20_conform mipmaps case failed issue
    2) Fix narutom.com mapUserMemory bug
    3) Fix error roll back code for texture with no mipmap
    4) Fix game issue of 3D Bio Ball, cut the rope, Riptide and DarkArea
    5) Fix PES2011 black screen bug
    6) Fix neocore performace drop issue
    7) Move pmem cache flush from driver to gralloc
    8) Supports DXT3, DXT5 in OES 1.0 path, default disable
    9) Fix Asphalt6 low memory issue
    10) Enable fbo y invert, and remove memory allocate fail log
    11) Enable Wclip patch for CrazySnowBoard game 
    
    Ver0.8.0.3287p1
    1) Integrated texture map fix for MP build

    Ver0.8.0.3287
    1) Fix game cordy_1.apk rendering error
    2) Add stream valid check for ES20's build stream path
    
    Ver0.8.0.3282
    1) Fix game Dungoen Defender can not run issue
    2) Fix game Ashalt6 auto-exit issue after the end of video player
    
    Ver0.8.0.3247
    1) Fix npow2 texture issue
    2) Refine application fps limitation logic to avoid usleep(0) case

    Ver0.8.0.3220
    1) Refine depth related code
    2) Enable GC silent reset
    3) Add application fps limitation to save power, default off
    4) Fix SKIA-GCU pre-multiplied color blend error
    5) Fix Tank Hero game missing floor tile texture issue

    Ver0.8.0.3179
    1) Fix blur/dim layer render to black window issue
    2) Fix logic error in _FindUsage
    3) Fix Samsung Camera baselay issue and SPBShell3D rendering error
    4) Add temp game patch com.tactel.electopia, limit uniform count
    5) simplify property control for fps_throttling

    Ver0.8.0.3116
    1) Fix Game AirAttackHDLite and com.glu.android.skob2_free crash issue
    2) Fix com.nix.game.mahjong-1.apk and labyrinth draw error
    3) Dont power off GC before entering early suspend to workround white screen issue
    4) Fix "Jet car" 3D game can not play issue
    5) Disable user mode heap allocation
    6) Fix switch between suspend and resume in video playing make live wallpaper hangs issue

    Ver0.8.0.2987
    1) fix master (2.3.3) performance drop issue
    
    Ver0.8.0.2983
    1) GC860 2D blending final fix
    2) Refine glFinish, avoid commit when no draw
    3) Support screen capture in GingerBread
    4) Add delay clear in 2D path for UI performance
    5) Fix small video blur or dark when rotation, force uploading filter every time
    6) Recover GC side pmem cache flush on mmp2 gingerbread beta1 branch
    7) use BUILD_MULTI_PREBUILT instread of add-prebuilt-files
    8) Fix the garbage issue when exit from camera, refine the skip 2 frame logic
    9) Fix gal2D multi-thread test cause GUI hang issue

    Ver0.8.0.2847
    1) Remove pmem cache flush operation from GC

    Ver0.8.0.2835
    1) Fix gc860 alpha blending issue in 2D path
    2) Fix software blur path render error
    3) Refine reserve VB memory and unmap textennal texture logic
    4) Fix UnlockScreen waitforcondition issue on vizio tablet
    
    Ver0.8.0.2795
    1) Enable 64 alignment unpack for GCU acceleration
    2) Fix Angrybird seasons version corruption issue
    3) Refine non-paged memory allocation and unmap logic to avoid memory leak 

    Ver0.8.0.2775
    1) Refine depth mask setting for depth0 situation

    Ver0.8.0.2770
    1) Fix Vizio OES 2.0 UI NP2 texture does not drawn correctly issue
    2) Add gco2D_Begin and gco2D_End into GCU functions
    3) Fix UnlockScreen hangs issue on vizio tablet
    4) Default enable skip 2D context for GCU, while still disable it for general 2D path

    Ver0.8.0.2727
    1) Temporarily remove error log message in gckHARDWARE_SetPowerManagementState as it prints too many
    2) The prebuilt binaries are from 2719 as Rander and Haipang are OOO

    Ver0.8.0.2719
    1) Refine GC power mutex logic, and enable depth0 config
    2) Add the new patch for fast clear include depth surface patch
    3) Fix a kernel panic issue when android is booting up
    4) Merge gc860 new feature into 2d path
    5) Fix Benchmark11 Z-buffer test precision lower issue
    6) Fix com.caza.apoolv2.apk render error
    7) Remove unnecessary resovle, depth and image texture buffer to save video memory

    Ver0.8.0.2665
    1) Enable GC 2D path on gingerbread
    2) Fix Dungeon Defenders crash on MMP2 issue
    3) Fix extra line issue in gles20 apps or when window height is not aligned 
    4) Enable GC profiling on Brownstone to fix livewallpaper and mm06 suspend&resume issue
    5) Disable op0 and op4 before GC clock_on, and enable them when GC is idle or off
    6) Fix game deadlychambers force close issue
    7) Fix memory leak when delete mapped signal failed
    8) Fix com.akjava.android.openglsamples render error issue 
     
    Ver0.8.0.1155
    1) First release package of MMP2 graphics driver for android eclair.    
    
    Ver0.8.0.1108
    1) Pre-alpha1 release for ARMADA 610 (MMP2) generic linux.
    2) Based on GC 2009.9.patch release, including recent bug fixes and 
       optimizations.

    Ver0.8.0.1006
    1) First release package of TavorPV2-A0 graphics driver for generic linux.

3.  Package Contents
    --/                   -- Package root directory
       doc                -- Documentation
       galcroe_src        -- GC source code needed to build kernel mode driver
       prebuilt           -- Prebuilt driver binaries, just for reference, you need to build them by yourself.
       galcore            -- Dir to placed galcore.ko you build
       hgl                -- Dir placed user mode static link libraries to build
       Mdroid.mk          -- Android make file to build user mode driver and copy gc driver to android output dir

4.  Build and Installation
4.1 System Requirement 
    Host PC :
       Operating system   -- Ubuntu 8.04 or newer
       Toolchain          -- arm-marvell-linux-gnueabi-vfp
    Target Device:
        Platform          -- PXA950 processor EVB platform
                             PXA968 processor EVB platform
                             ARMADA 610 processor EVB platform
        Operation system  -- Linux kernel 2.6.29 or newer
   
4.2  Build Kernel Mode Driver
     Enter ~/galcore_src, modify the "Makefile" according to your system:
        1)  Modify "CROSS_COMPILE" to your toolchain directory with prefix
           (path_to_your_marvelltoolchain_dir/bin/arm-marvell-linux-gnueabi-);
        2)  Modify "KERNEL_DIR" to point to your linux kernel tree.
     Build kernel mode driver by commands:
        #make clean
        #make
     If no error, the build result galcore.ko will be placed on current directory.
	
4.3 Build User Mode Driver
    1) Copy galcore.ko built by you to ~/galcore
    2) #cd android_home
       #. ./build/envsetup.sh
       #choosecombo
       #cp ~/galcore_src/galcore.ko ~/galcore
       #cd ~
       #mv Mdroid.mk Android.mk
       #mm
    3) Output:
       android_output_dir/system/lib/modules/galcore.ko
       android_output_dir/system/lib/libGAL.so
       android_output_dir/system/lib/egl/egl.cfg
       android_output_dir/system/lib/egl/libEGL.so
       android_output_dir/system/lib/egl/libGLESv1_CM_MRVL.so
       android_output_dir/system/lib/egl/libGLESv2_MRVL.so
       android_output_dir/system/lib/egl/libOpenVG.so
       android_output_dir/system/lib/egl/libEGL_FB.so

4.4 Installation
    For ARMADA610(MMP2)
       mknod	/dev/galcore c 199 0
       insmod	/lib/modules/galcore.ko registerMemBase=0xd420d000 irqLine=8 contiguousSize=0x2000000
    
    For PXA950(TavorPV2)
       mknod	/dev/galcore c 199 0
       insmod	/lib/modules/galcore.ko registerMemBase=0x54000000 irqLine=70 contiguousSize=0x1000000
    Reboot the device and run sample applications on device.

5.  FAQ
    1. "We can't find the arm-marvell-linux-gnueabi-vfp toolchain".
       Contact your Marvell representative to get arm-marvell-linux-gnueabi-vfp toolchain.

6.  Known Issues
    N/A.




