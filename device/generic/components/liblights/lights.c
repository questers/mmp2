
#include <hardware/lights.h>
#include <hardware/hardware.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <pthread.h>

#define LOG_TAG "lights"
#include <utils/Log.h>

#define LIGHTS_DEBUG 

enum eLightType
{
	eLightTypeUnknown=0,
	eLightTypeBacklight,
	eLightTypeKeyboard,
	eLightTypeButtons,
	eLightTypeBattery,
	eLightTypeNotifications,
	eLightTypeAttention,
	eLightTypeMax
};

struct light_module_t
{
    struct hw_module_t common;
};

struct my_light_device_t
{
    struct light_device_t dev;
	const char* brightness_path;
	int light_type;
    int max_brightness;
};

static int lights_device_open(const struct hw_module_t *module,
				const char *name, struct hw_device_t **device);

const char * const LCD_BACKLIGHT = "/sys/class/backlight/pwm-backlight.0/brightness";
const char * const LCD_MAX_BACKLIGHT = "/sys/class/backlight/pwm-backlight.0/max_brightness";
const char * const KEYBOARD_BACKLIGHT = "/sys/class/backlight/max8925-backlight/brightness";
const char * const KEYBOARD_MAX_BACKLIGHT = "/sys/class/backlight/max8925-backlight/max_brightness";
const char * const BUTTON_BACKLIGHT = "/sys/class/leds/button-backlight/brightness";
const char * const BUTTON_MAX_BACKLIGHT = "/sys/class/leds/button-backlight/max_brightness";

const char * const BATTERY_BACKLIGHT = "";
const char * const NOTIFICATIONS_BACKLIGHT = "";
const char * const ATTENTION_BACKLIGHT = "";

static pthread_once_t g_light_init = PTHREAD_ONCE_INIT;
static pthread_mutex_t g_light_lock = PTHREAD_MUTEX_INITIALIZER;
#define LIGHT_LOCK() pthread_mutex_lock(&g_light_lock)
#define LIGHT_UNLOCK() pthread_mutex_unlock(&g_light_lock)

static struct hw_module_methods_t lights_module_methods = {
	.open      = lights_device_open
};


const struct light_module_t HAL_MODULE_INFO_SYM = {
	common: {
		tag : HARDWARE_MODULE_TAG,
		version_major : 1,
		version_minor : 0,
		id : LIGHTS_HARDWARE_MODULE_ID,
		name : "lights module",
		author : "",
		methods : &lights_module_methods,
	}
};



static int rgb_to_brightness(struct light_state_t const* state)
{
    int color = state->color & 0x00ffffff;
    return ((77*((color>>16)&0x00ff))
            + (150*((color>>8)&0x00ff)) + (29*(color&0x00ff))) >> 8;
}

static int readIntFromFile( const char* file, int* pResult )
{
    int fd = -1;
    fd = open( file, O_RDONLY );
    if( fd >= 0 ){
        char buf[20];
        int rlt = read( fd, buf, sizeof(buf) );
        if( rlt > 0 ){
            buf[rlt] = '\0';
            *pResult = atoi(buf);
            return 0;
        }
    }
    return -1;
}

static int lights_set_state_locked(struct light_device_t* dev, struct light_state_t const* state) 
{

#ifdef LIGHTS_DEBUG
	static const char* light_names[]=
	{
		"unknown",
		"backlight",
		"keyboard",
		"buttons",
		"battery",
		"notifications",
		"attention"
	};
	const char* light_name;
#endif

    struct light_state_t const *light_state = (struct light_state_t const*)state;
    struct my_light_device_t *mydev = (struct my_light_device_t*)dev;
    unsigned char brightness = 0;
    int fd = -1;
	const char* brightness_node;


    if( state ) {

		//FIXME:lcd backlight hack
		if(eLightTypeBacklight==mydev->light_type)
		{
			//invert
			brightness = rgb_to_brightness(state);
			//brightness = mydev->max_brightness-brightness;			
			brightness = ( (brightness & 0xff) * mydev->max_brightness) / 0xff;		

#if 0
			//hack for dim
			if(brightness<=55&&brightness!=0){
				brightness = 55;
			}
#endif
		}
		else
		{
			unsigned int color = state->color;
			brightness = (color & 0xff) * mydev->max_brightness / 0xff;		
		}
    } else {
        LOGE("lights_set_brightness failed: light state is null\n");
        return -EINVAL;
    }

#ifdef LIGHTS_DEBUG
	if(mydev->light_type<eLightTypeMax)
		light_name = light_names[mydev->light_type];
	else
		light_name = light_names[0];
	LOGD("lights_set: light devices[%s],brightness=%d,max_brightness=%d\n", 
		light_name,brightness,mydev->max_brightness);
#endif

	brightness_node = mydev->brightness_path;
	if(!strlen(brightness_node))
	{
		#ifdef LIGHTS_DEBUG
		LOGE("light device %s not support",light_name);
		#endif		
		return -1;
	}
    fd = open(brightness_node, O_RDWR);
    if( fd >= 0 ) {
        char buffer[20];
        int bytes = sprintf(buffer, "%d", brightness);
        int rlt = write(fd, buffer, bytes);
        if( rlt < 0 ){
            LOGE("lights_set_brightness %s, fail to set brightness to:%s, errno:%d\n", brightness_node, buffer, errno); 
            close(fd);
            return -1;
        }
        close(fd);
    } 
    return 0;
}

static int lights_set_state(struct light_device_t* dev, struct light_state_t const* state)
{	
	LIGHT_LOCK();
	lights_set_state_locked(dev,state);
	LIGHT_UNLOCK();
	return 0;
}

static void init_globals(void)
{
    // init the mutex
    pthread_mutex_init(&g_light_lock, NULL);
}

static int lights_device_close(struct hw_device_t *dev)
{
	struct my_light_device_t *light_dev = (struct my_light_device_t *)dev;

	if (light_dev)
		free(light_dev);
	
	return 0;
}

static int lights_device_open(const struct hw_module_t *module,
				const char *name, struct hw_device_t **device)
{
    int	status = -EINVAL;
    int ret;
#ifdef LIGHTS_DEBUG
	LOGD("lights_device_open: device name: %s\n", name);
#endif

    struct light_device_t *dev = NULL;
    struct my_light_device_t *mydev = NULL;
    
    mydev = (struct my_light_device_t *)calloc(sizeof(struct my_light_device_t), 1);
    dev = &(mydev->dev);
    dev->common.tag = HARDWARE_DEVICE_TAG;
    dev->common.version = 0;
    dev->common.module = (struct hw_module_t *)(module);
    dev->common.close = lights_device_close;

    mydev->max_brightness = 0xff;//default max brightness is 255
        
    *device = &dev->common;

    pthread_once(&g_light_init, init_globals);
	
    if (!strcmp(name, LIGHT_ID_BACKLIGHT)) {
        readIntFromFile( LCD_MAX_BACKLIGHT, &mydev->max_brightness);
		mydev->max_brightness=mydev->max_brightness;
		mydev->brightness_path = LCD_BACKLIGHT;
		mydev->light_type = eLightTypeBacklight;
    } else if (!strcmp(name, LIGHT_ID_KEYBOARD)) {
        readIntFromFile( KEYBOARD_MAX_BACKLIGHT, &mydev->max_brightness);
		mydev->brightness_path = KEYBOARD_BACKLIGHT;		
		mydev->light_type = eLightTypeKeyboard;
    } else if (!strcmp(name, LIGHT_ID_BUTTONS)) {
        readIntFromFile( BUTTON_MAX_BACKLIGHT, &mydev->max_brightness);
		mydev->brightness_path = BUTTON_BACKLIGHT;
		mydev->light_type = eLightTypeButtons;		
    } else if (!strcmp(name, LIGHT_ID_BATTERY)) {
		mydev->brightness_path = BATTERY_BACKLIGHT;
		mydev->light_type = eLightTypeBattery;		
    } else if (!strcmp(name, LIGHT_ID_NOTIFICATIONS)) {
		mydev->brightness_path = NOTIFICATIONS_BACKLIGHT;		
		mydev->light_type = eLightTypeNotifications;		
    } else if (!strcmp(name, LIGHT_ID_ATTENTION)) {
		mydev->brightness_path = ATTENTION_BACKLIGHT;		
		mydev->light_type = eLightTypeAttention;		
    } else
        return status;

	dev->set_light = lights_set_state;
	
	return 0;
}
