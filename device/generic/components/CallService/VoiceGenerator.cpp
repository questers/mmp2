/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <VoiceGenerator.h>
#include <VoiceLog.h>
#include <alsa/asoundlib.h>
#include <math.h>

namespace android {


static void generate_sine(const snd_pcm_channel_area_t *areas, 
                           snd_pcm_uframes_t offset,
                           int count, double *_phase,unsigned int channels,double freq)
{
     static double max_phase = 2. * M_PI;
     snd_pcm_format_t format = SND_PCM_FORMAT_S16;//only support SND_PCM_FORMAT_S16
     unsigned int rate=8000;//fix 8kHz sample rate for Voice
     double phase = *_phase;
     double step = max_phase*freq/(double)rate;
     unsigned char *samples[channels];
     int steps[channels];
     unsigned int chn;
     int format_bits = snd_pcm_format_width(format);
     unsigned int maxval = (1 << (format_bits - 1)) - 1;
     int bps = format_bits / 8;  /* bytes per sample */
     int phys_bps = snd_pcm_format_physical_width(format) / 8;
     int big_endian = snd_pcm_format_big_endian(format) == 1;
     int to_unsigned = snd_pcm_format_unsigned(format) == 1;
     int is_float = (format == SND_PCM_FORMAT_FLOAT_LE ||
                     format == SND_PCM_FORMAT_FLOAT_BE);

     /* verify and prepare the contents of areas */
     for (chn = 0; chn < channels; chn++) {
             if ((areas[chn].first % 8) != 0) {
                     ERROR("areas[%i].first == %i, aborting...\n", chn, areas[chn].first);
             }
             samples[chn] = /*(signed short *)*/(((unsigned char *)areas[chn].addr) + (areas[chn].first / 8));
             if ((areas[chn].step % 16) != 0) {
                     ERROR("areas[%i].step == %i, aborting...\n", chn, areas[chn].step);
                     exit(EXIT_FAILURE);
             }
             steps[chn] = areas[chn].step / 8;
             samples[chn] += offset * steps[chn];
     }
     /* fill the channel areas */
     while (count-- > 0) {
             union {
                     float f;
                     int i;
             } fval;
             int res, i;
             if (is_float) {
                     fval.f = sin(phase) * maxval;
                     res = fval.i;
             } else
                     res = sin(phase) * maxval;
             if (to_unsigned)
                     res ^= 1U << (format_bits - 1);
             for (chn = 0; chn < channels; chn++) {
                     /* Generate data in native endian format */
                     if (big_endian) {
                             for (i = 0; i < bps; i++)
                                     *(samples[chn] + phys_bps - 1 - i) = (res >> i * 8) & 0xff;
                     } else {
                             for (i = 0; i < bps; i++)
                                     *(samples[chn] + i) = (res >>  i * 8) & 0xff;
                     }
                     samples[chn] += steps[chn];
             }
             phase += step;
             if (phase >= max_phase)
                     phase -= max_phase;
     }
     *_phase = phase;
}
 

VoiceGenerator::VoiceGenerator():
mFrameSize(0),
mFrameCount(0),
mFrameBuffer(NULL),
voicetype(0),
phase(0.0)
{}


VoiceGenerator::~VoiceGenerator()
{
    if(mFrameBuffer)
        delete [] mFrameBuffer;
}
bool VoiceGenerator::GenerateVoice(int type)
{
    if(!mFrameSize||!mFrameCount)
       return false;

    //other constraints?
    if(type>=kGeneratorTypeNum)
        return false;

    voicetype = type;
    
    
    //read to generate
    if(mFrameBuffer)
        delete [] mFrameBuffer;
    mFrameBuffer = new unsigned char[mFrameSize*mFrameCount];
    if(!mFrameBuffer)
        return false;

    int buffersize = mFrameSize*mFrameCount;
    //
    switch(type)
    {
        default:
        case kGeneratorTypeMute:
            GenerateMute(mFrameBuffer,buffersize);
            break;
        case kGeneratorTypeSine1K:
            GenerateSine(mFrameBuffer,buffersize,1000);
            break;
    }

    return true;

}
int VoiceGenerator::Read(unsigned char* buffer,int size)
{
    int buffersize = mFrameSize*mFrameCount;
    //regenerator samples
    GenerateVoice(voicetype);    
    
    if(size>buffersize)
        size = buffersize;
    if(mFrameBuffer)
        memcpy(buffer,mFrameBuffer,size);
    else
        size = 0;
    return size;
}

void VoiceGenerator::FrameCount(int framecount)
{
    if(mFrameCount!=framecount)
    {
        mFrameCount = framecount;
    }
}

void VoiceGenerator::FrameSize(int framesize)
{
    if(mFrameSize!=framesize)
    {
        mFrameSize= framesize;
    }

}
void VoiceGenerator::GenerateMute(unsigned char* buffer,int size)
{
    INFO("Generate Mute Voice of %d bytes\n",size);
    //very simple mute,
    memset(buffer,0,size);

}

void VoiceGenerator::GenerateSine(unsigned char* buffer,int size,int frequency)
{
    const snd_pcm_channel_area_t area=
    {
        addr:buffer,
        first:0,
        step:16,
    };
    generate_sine(&area,0,size,&phase,1,1000.0);

}


//mute generator
ANDROID_SINGLETON_STATIC_INSTANCE( MuteVoiceGenerator )

MuteVoiceGenerator::MuteVoiceGenerator()
{
    FrameSize(MuteVoiceGenerator::frame_size);
    FrameCount(MuteVoiceGenerator::frame_count);

    GenerateVoice(VoiceGenerator::kGeneratorTypeMute);
}

//sine generator
ANDROID_SINGLETON_STATIC_INSTANCE( SineVoiceGenerator )

SineVoiceGenerator::SineVoiceGenerator()
{
    FrameSize(SineVoiceGenerator::frame_size);
    FrameCount(SineVoiceGenerator::frame_count);

    GenerateVoice(VoiceGenerator::kGeneratorTypeSine1K);
}



};

