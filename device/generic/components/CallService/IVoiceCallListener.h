#ifndef _IVOICECALL_LISTENER_H_
#define _IVOICECALL_LISTENER_H_

#include <binder/IInterface.h>
#include <binder/Parcel.h>


namespace android {

class IVoiceCallListener:public IInterface
{
public:
	DECLARE_META_INTERFACE(VoiceCallListener);
	
	enum {
		kVoiceCallStateUnknown,
		kVoiceCallStateStarted,
		kVoiceCallStateStopped		
	};

	virtual void onVoiceCallStateChanged(int newState)=0;
	

};

class BnVoiceCallListener: public BnInterface<IVoiceCallListener> {
public:
    virtual status_t onTransact(uint32_t code, const Parcel& data,
            Parcel* reply, uint32_t flags = 0);
};

};
#endif

