#include <stdlib.h>
#include <unistd.h>
#include <cutils/properties.h>
#include <VoiceLog.h>

namespace android
{

ANDROID_SINGLETON_STATIC_INSTANCE(VoiceLogSetting)

VoiceLogSetting::VoiceLogSetting():
mLogLevel(LOG_WARN|LOG_ERROR|LOG_DEBUG)
{
    char property[PROPERTY_VALUE_MAX];
    if (property_get("voice.debug.level", property, NULL) > 0) {
        mLogLevel = atoi(property);
    }

}

};

