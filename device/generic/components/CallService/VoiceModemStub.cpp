//provides simple voice modem interface
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h> 
#include <stdio.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <utils/Timers.h>
#include <cutils/properties.h>
#include <media/AudioSystem.h>
#include <media/AudioTrack.h>
#include <media/AudioRecord.h>
#include <media/mediarecorder.h>
#include <VoiceLog.h>
#include "VoiceCodecPCM.h"
#include "VoiceModemStub.h"
#include "VoiceGenerator.h"

namespace android
{


VoiceModemStub::VoiceModemStub():
mpVoiceCallTrack(NULL),
mpVoiceCallRecord(NULL),
codec(NULL),
rw_period_ms(20),
framesize(2)
{
    cached_trackbuffer = cached_recordbuffer = NULL;
    max_cached_trackframes = cached_trackframes = min_trackframes  = 0;
    max_cached_recordframes = cached_recordframes = min_recordframes  = 0;

	memset(&status,0,sizeof(modem_status));
}

VoiceModemStub::~VoiceModemStub()
{
    end_call();
    if(cached_trackbuffer)
        delete [] cached_trackbuffer;    
    if(cached_recordbuffer)
        delete [] cached_recordbuffer;
    
}



void VoiceModemStub::vc_track_callback(int event, void* user, void *info)
{

	if(event!=AudioTrack::EVENT_MORE_DATA) return;
	VoiceModemStub* vm=static_cast<VoiceModemStub *>(user);
    AudioTrack::Buffer *buffer = static_cast<AudioTrack::Buffer *>(info);
    int requestFrames = buffer->frameCount;

	if (buffer->size == 0) return;

    #if 1
	VERBOSE("vc track event, request frames[%d] ",requestFrames);
    int read_size = MUTE_READ(vm->cached_trackbuffer,(buffer->frameCount*vm->framesize));
    memcpy(buffer->raw,vm->cached_trackbuffer,(read_size<buffer->size)?read_size:buffer->size);
	#else
	if(vm->cached_trackframes<requestFrames)
	{
		vm->cached_trackframes = read_size/vm->framesize;
        //dummp sleep
        //usleep(vm->rw_period_ms*1000);
	}
	VERBOSE("vc track event, request frames[%d] cached frames[%d]",requestFrames,vm->cached_trackframes);
	unsigned char* frameptr;
	frameptr = vm->cached_trackbuffer+(vm->max_cached_trackframes-vm->cached_trackframes)*vm->framesize;

	if(requestFrames<=vm->cached_trackframes)
	{
		memcpy(buffer->raw,frameptr,buffer->size);
		vm->cached_trackframes-=requestFrames;
	}
	else
	{
	    WARN("bogus");
	}
    #endif
	vm->mTrackLock.lock();
	if(VoiceModemStub::sSTARTING==vm->status.trackstatus)
	{
		vm->status.trackstatus = VoiceModemStub::sSTARTED;
		vm->mWaitTrackCond.signal();
	}
	else if(VoiceModemStub::sSTOPPING==vm->status.trackstatus)
	{
		vm->mpVoiceCallTrack->stop();
		vm->status.trackstatus = VoiceModemStub::sSTOPPED;
		vm->mWaitTrackCond.signal();
	}
	vm->mTrackLock.unlock();
	
}

void VoiceModemStub::vc_record_callback(int event,void* user,void * info)
{

	if(event!=AudioTrack::EVENT_MORE_DATA) return;
	VoiceModemStub* vm=static_cast<VoiceModemStub *>(user);
    AudioTrack::Buffer *buffer = static_cast<AudioTrack::Buffer *>(info);	
    unsigned int receivedFrames = buffer->frameCount;

	VERBOSE("vc record event %d,frames[%d]",event,receivedFrames);	
	if(receivedFrames)
	{
        //just drop received frames for stub modem        
        if(vm->cached_recordframes<vm->max_cached_recordframes)
            memcpy(vm->cached_recordbuffer+vm->cached_recordframes*vm->framesize,buffer->raw,receivedFrames*vm->framesize);
	}
    
    //drop frames
    vm->cached_recordframes=0;
    buffer->size=0;

	vm->mRecordLock.lock();
	if(VoiceModemStub::sSTARTING==vm->status.recordstatus)
	{
		vm->status.recordstatus = VoiceModemStub::sSTARTED;
		vm->mWaitRecordCond.signal();
	}
	else if(VoiceModemStub::sSTOPPING==vm->status.recordstatus)
	{
		vm->mpVoiceCallRecord->stop();
		vm->status.recordstatus = VoiceModemStub::sSTOPPED;
		vm->mWaitRecordCond.signal();
	}
	
	vm->mRecordLock.unlock();
}

int VoiceModemStub::start_call(void)
{	
    int new_max_cached_trackframes,new_max_cached_recordframes;

	AutoMutex lock(&mLock);
	

	VERBOSE("start stub modem call");

	if(status.incall==1)
		return 0;

    //use pcm codec for stub modem   
	codec = new VoiceCodecPCM();//unused now
	if(codec->initCheck()) goto start_failure;
    AudioTrack::getMinFrameCount(&min_trackframes,AudioSystem::VOICE_CALL,8000);
    new_max_cached_trackframes = min_trackframes;
    cached_trackframes = 0;
    if(new_max_cached_trackframes!=max_cached_trackframes)
    {
        max_cached_trackframes = new_max_cached_trackframes;
        delete [] cached_trackbuffer;
        cached_trackbuffer = new unsigned char[max_cached_trackframes*framesize];
    }
    INFO("min_trackframes=%d",min_trackframes);
    INFO("max_cached_trackframes=%d",max_cached_trackframes);    
    INFO("cached_trackframes=%d",cached_trackframes);

    AudioRecord::getMinFrameCount(&min_recordframes,8000,AudioSystem::PCM_16_BIT,1);
    new_max_cached_recordframes = min_recordframes;
    cached_recordframes = 0;
    if(new_max_cached_recordframes!=max_cached_recordframes)
    {
        max_cached_recordframes = new_max_cached_recordframes;
        delete [] cached_recordbuffer;
        cached_recordbuffer = new unsigned char[max_cached_recordframes*framesize];
    }
    INFO("min_recordframes=%d",min_recordframes);
    INFO("max_cached_recordframes=%d",max_cached_recordframes);        
    INFO("cached_recordframes=%d",cached_recordframes);
    
	if(!cached_trackbuffer) goto start_failure;

	//create voicecall track 	
	mpVoiceCallTrack = new AudioTrack(AudioSystem::VOICE_CALL,
			  8000,
			  AudioSystem::PCM_16_BIT,
			  AudioSystem::CHANNEL_OUT_MONO,
			  min_trackframes*4,//ensure is times of vc packet, 8 times of cached frames is enough
			  0,
			  VoiceModemStub::vc_track_callback,
			  this,
			  min_trackframes//20ms voicecall data
			  );
	if(!mpVoiceCallTrack) goto start_failure;
		
	if (mpVoiceCallTrack->initCheck() != NO_ERROR) goto start_failure;

	mpVoiceCallTrack->setVolume(1.0, 1.0);	
		
	VERBOSE("create vc track object ok");

	mpVoiceCallRecord = new AudioRecord(AUDIO_SOURCE_DEFAULT,//AUDIO_SOURCE_VOICE_CALL,
			8000,
			AudioSystem::PCM_16_BIT,
			AudioSystem::CHANNEL_IN_MONO,
			min_recordframes*4,//ensure is times of vc packet, 8 times of cached frames is enough
			0,
			VoiceModemStub::vc_record_callback,
			this,
			min_recordframes//20ms voicecall data			
			);
	if(!mpVoiceCallRecord) goto start_failure;
	if (mpVoiceCallRecord->initCheck() != NO_ERROR) goto start_failure;

	VERBOSE("create vc record object ok");

	//reset track/record status
	//start now
	{
		status_t lStatus;
		mTrackLock.lock();
		status.trackstatus = VoiceModemStub::sSTARTING;
		mTrackLock.unlock();
		mpVoiceCallTrack->start();
		mTrackLock.lock();
		
		if (VoiceModemStub::sSTARTING == status.trackstatus) {
			VERBOSE("Wait for start track callback");
			lStatus = mWaitTrackCond.waitRelative(mTrackLock, seconds(3));
			if (lStatus != NO_ERROR) {
				ERROR("--- Immediate start timed out, status %d", lStatus);
			}
		}
		mTrackLock.unlock();
		
	}
	{
		status_t lStatus;
		mRecordLock.lock();
		status.recordstatus= VoiceModemStub::sSTARTING;
		mRecordLock.unlock();		
		mpVoiceCallRecord->start();
		mRecordLock.lock();
		if (VoiceModemStub::sSTARTING == status.recordstatus) {
			VERBOSE("Wait for start record callback");
			lStatus = mWaitRecordCond.waitRelative(mRecordLock, seconds(3));
			if (lStatus != NO_ERROR) {
				ERROR("--- Immediate start timed out, status %d", lStatus);
			}
		}
		mRecordLock.unlock();
		
	}
	status.incall=1;
	VERBOSE("voice call running ");
	return 0;
start_failure:
	ERROR("start voice call failed");
	if(mpVoiceCallTrack)
	{
		mpVoiceCallTrack->stop();
		delete mpVoiceCallTrack;
		mpVoiceCallTrack = NULL;
	}
	if(mpVoiceCallRecord)
	{
		mpVoiceCallRecord->stop();
		delete mpVoiceCallRecord;
		mpVoiceCallRecord = NULL;
	}
	
	return -1;
}
int VoiceModemStub::end_call(void)
{
	AutoMutex lock(&mLock);

	if(status.incall==0)
		return 0;
	VERBOSE("end voice modem call");


	{
		status_t lStatus;
		mTrackLock.lock();
		status.trackstatus = VoiceModemStub::sSTOPPING;
		mTrackLock.unlock();
		VERBOSE("stopping vc track");
		mTrackLock.lock();		
		if (VoiceModemStub::sSTOPPING == status.trackstatus) {
			VERBOSE("Wait for stop track callback");
			lStatus = mWaitTrackCond.waitRelative(mTrackLock, seconds(3));
			if (lStatus != NO_ERROR) {
				ERROR("--- Immediate stop timed out, status %d", lStatus);
			}
			else
			{
				mpVoiceCallTrack->stop();
				mpVoiceCallTrack->flush();				
			}
		}
		mTrackLock.unlock();		
		delete mpVoiceCallTrack;
		mpVoiceCallTrack = NULL;
		cached_trackframes = 0;
	}
	VERBOSE("stop voice call track ok");
		
	{
		status_t lStatus;
		mRecordLock.lock();
		status.recordstatus= VoiceModemStub::sSTOPPING;
		mRecordLock.unlock();		
		VERBOSE("stopping vc record");
		mRecordLock.lock();		
		if (VoiceModemStub::sSTOPPING == status.recordstatus) {
			VERBOSE("Wait for stop record callback");
			lStatus = mWaitRecordCond.waitRelative(mRecordLock, seconds(3));
			if (lStatus != NO_ERROR) {
				ERROR("--- Immediate stop timed out, status %d", lStatus);
			}
			else
			{
				mpVoiceCallRecord->stop();				
			}
		}
		mRecordLock.unlock();		
		delete mpVoiceCallRecord;
		mpVoiceCallRecord = NULL;
	}
	VERBOSE("stop voice call record ok");

	//destroy codec
	delete codec;
	codec = NULL;
	
	VERBOSE("voice call stopped");

	status.incall=0;
	
	return 0;

}


void VoiceModemStub::set_volume(float vol)
{
	AutoMutex lock(&mLock);
	

	if(!status.incall)
		return;	

	if(mpVoiceCallTrack)
		mpVoiceCallTrack->setVolume(vol,vol);
}

void VoiceModemStub::set_path(int path)
{
    WARN("%s not implemented\n",__func__);
}




}


