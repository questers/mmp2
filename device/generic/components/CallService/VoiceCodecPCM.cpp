
//
//TODO
//
#include "VoiceCodecPCM.h"

namespace android{

VoiceCodecPCM::VoiceCodecPCM()
{
}

VoiceCodecPCM::~VoiceCodecPCM()
{
}

int VoiceCodecPCM::initCheck(void)
{
	return 0;
}
int VoiceCodecPCM::setParameters(const String8& keyValuePairs)
{
	return 0;
}

int VoiceCodecPCM::framesize(){return 2;}

int VoiceCodecPCM::decode(const unsigned char* src,const int samples,unsigned char* dst)
{
	memcpy(dst,src,samples*2);
	return samples*2;
}

int VoiceCodecPCM::encode(const unsigned char *src,const int samples,unsigned char* dst)
{
	memcpy(dst,src,samples*2);
	return samples*2;

}

}

