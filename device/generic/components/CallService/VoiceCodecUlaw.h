#ifndef _ULAW_H_
#define _ULAW_H_

#include "VoiceCodec.h"


namespace android
{

	class UlawCodec
	{
		public:
			static int decode(const unsigned char* src,const int samples,unsigned char* dst);
			static int encode(const unsigned char *src,const int samples,unsigned char* dst);
	};

	//new ulawcodec interface
	class VoiceCodecUlaw:public VoiceCodec
	{
		public:
			VoiceCodecUlaw();
			virtual ~VoiceCodecUlaw();
			int initCheck(void);
			int framesize();
			int   setParameters(const String8& keyValuePairs);
			int decode(const unsigned char* src,const int samples,unsigned char* dst);
			int encode(const unsigned char *src,const int samples,unsigned char* dst);
		private:
			inline unsigned char lin2ulaw(int sample);
			inline int ulaw2lin(unsigned char ulawbyte);
			
	};

	
#define ULAWENCODE(src,sample,dst) \
		UlawCodec::encode(src,sample,dst)
#define ULAWDECODE(src,sample,dst) \
		UlawCodec::decode(src,sample,dst)
}

#endif
