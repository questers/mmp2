#include "VoiceCodecUlaw.h"

#define ULAW_ALGO 2
#if (ULAW_ALGO==1)
/*
 * very simple mu-law conversion algorithm
 * based on ALSA project 
*/
static inline int val_seg(int val)
{
	int r = 0;
	val >>= 7;
	if (val & 0xf0) {
		val >>= 4;
		r += 4;
	}
	if (val & 0x0c) {
		val >>= 2;
		r += 2;
	}
	if (val & 0x02)
		r += 1;
	return r;
}

/*
 * s16_to_ulaw() - Convert a linear PCM value to u-law
 *
 * In order to simplify the encoding process, the original linear magnitude
 * is biased by adding 33 which shifts the encoding range from (0 - 8158) to
 * (33 - 8191). The result can be seen in the following encoding table:
 *
 *	Biased Linear Input Code	Compressed Code
 *	------------------------	---------------
 *	00000001wxyza			000wxyz
 *	0000001wxyzab			001wxyz
 *	000001wxyzabc			010wxyz
 *	00001wxyzabcd			011wxyz
 *	0001wxyzabcde			100wxyz
 *	001wxyzabcdef			101wxyz
 *	01wxyzabcdefg			110wxyz
 *	1wxyzabcdefgh			111wxyz
 *
 * Each biased linear code has a leading 1 which identifies the segment
 * number. The value of the segment number is equal to 7 minus the number
 * of leading 0's. The quantization interval is directly available as the
 * four bits wxyz.	* The trailing bits (a - h) are ignored.
 *
 * Ordinarily the complement of the resulting code word is used for
 * transmission, and so the code word is complemented before it is returned.
 *
 * For further information see John C. Bellamy's Digital Telephony, 1982,
 * John Wiley & Sons, pps 98-111 and 472-476.
 */

static inline unsigned char lin2ulaw(int pcm_val)	/* 2's complement (16-bit range) */
{
	int mask;
	int seg;
	unsigned char uval;

	if (pcm_val < 0) {
		pcm_val = 0x84 - pcm_val;
		mask = 0x7f;
	} else {
		pcm_val += 0x84;
		mask = 0xff;
	}
	if (pcm_val > 0x7fff)
		pcm_val = 0x7fff;

	/* Convert the scaled magnitude to segment number. */
	seg = val_seg(pcm_val);

	/*
	 * Combine the sign, segment, quantization bits;
	 * and complement the code word.
	 */
	uval = (seg << 4) | ((pcm_val >> (seg + 3)) & 0x0f);
	return uval ^ mask;
}

/*
 * ulaw_to_s16() - Convert a u-law value to 16-bit linear PCM
 *
 * First, a biased linear code is derived from the code word. An unbiased
 * output can then be obtained by subtracting 33 from the biased code.
 *
 * Note that this function expects to be passed the complement of the
 * original code word. This is in keeping with ISDN conventions.
 */
static inline int ulaw2lin(unsigned char u_val)
{
	int t;

	/* Complement to obtain normal u-law value. */
	u_val = ~u_val;

	/*
	 * Extract and bias the quantization bits. Then
	 * shift up by the segment number and subtract out the bias.
	 */
	t = ((u_val & 0x0f) << 3) + 0x84;
	t <<= (u_val & 0x70) >> 4;

	return ((u_val & 0x80) ? (0x84 - t) : (t - 0x84));
}

#elif (ULAW_ALGO==2)
//another routine from http://www.speech.cs.cmu.edu/comp.speech/Section2/Q2.7.html
/*
** This routine converts from linear to ulaw
**
** Craig Reese: IDA/Supercomputing Research Center
** Joe Campbell: Department of Defense
** 29 September 1989
**
** References:
** 1) CCITT Recommendation G.711  (very difficult to follow)
** 2) "A New Digital Technique for Implementation of Any
**     Continuous PCM Companding Law," Villeret, Michel,
**     et al. 1973 IEEE Int. Conf. on Communications, Vol 1,
**     1973, pg. 11.12-11.17
** 3) MIL-STD-188-113,"Interoperability and Performance Standards
**     for Analog-to_Digital Conversion Techniques,"
**     17 February 1987
**
** Input: Signed 16 bit linear sample
** Output: 8 bit ulaw sample
*/

#define ZEROTRAP    /* turn on the trap as per the MIL-STD */
#define BIAS 0x84   /* define the add-in bias for 16 bit samples */
#define CLIP 32635

static inline unsigned char
lin2ulaw(int sample)
{
  static int exp_lut[256] = {0,0,1,1,2,2,2,2,3,3,3,3,3,3,3,3,
                             4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
                             5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
                             5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
                             6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
                             6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
                             6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
                             6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
                             7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
                             7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
                             7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
                             7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
                             7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
                             7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
                             7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
                             7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7};
  int sign, exponent, mantissa;
  unsigned char ulawbyte;

  /* Get the sample into sign-magnitude. */
  sign = (sample >> 8) & 0x80;		/* set aside the sign */
  if (sign != 0) sample = -sample;		/* get magnitude */
  if (sample > CLIP) sample = CLIP;		/* clip the magnitude */

  /* Convert from 16 bit linear to ulaw. */
  sample = sample + BIAS;
  exponent = exp_lut[(sample >> 7) & 0xFF];
  mantissa = (sample >> (exponent + 3)) & 0x0F;
  ulawbyte = ~(sign | (exponent << 4) | mantissa);
#ifdef ZEROTRAP
  if (ulawbyte == 0) ulawbyte = 0x02;	/* optional CCITT trap */
#endif

  return(ulawbyte);
}

/*
** This routine converts from ulaw to 16 bit linear.
**
** Craig Reese: IDA/Supercomputing Research Center
** 29 September 1989
**
** References:
** 1) CCITT Recommendation G.711  (very difficult to follow)
** 2) MIL-STD-188-113,"Interoperability and Performance Standards
**     for Analog-to_Digital Conversion Techniques,"
**     17 February 1987
**
** Input: 8 bit ulaw sample
** Output: signed 16 bit linear sample
*/

static inline int
ulaw2lin(unsigned char ulawbyte)
{
  static int exp_lut[8] = {0,132,396,924,1980,4092,8316,16764};
  int sign, exponent, mantissa, sample;

  ulawbyte = ~ulawbyte;
  sign = (ulawbyte & 0x80);
  exponent = (ulawbyte >> 4) & 0x07;
  mantissa = ulawbyte & 0x0F;
  sample = exp_lut[exponent] + (mantissa << (exponent + 3));
  if (sign != 0) sample = -sample;

  return(sample);
}

#else
#error please select the right ulaw algorithm in ulaw.c
#endif



namespace android
{

int UlawCodec::decode(const unsigned char* src,const int samples,unsigned char* dst)
{
	unsigned short* target=(unsigned short*)dst;
	int length=samples;
	while(length--)
	{
		*target++ = ulaw2lin(*src++);
	}
	// 2 bytes per sample
	return samples*2;
	
}

int UlawCodec::encode(const unsigned char *src,const int samples,unsigned char* dst)
{
	unsigned short* source=(unsigned short*)src;
	int length=samples;
	while(length--)
	{
		*dst++ = lin2ulaw(*source++);
	}

	// 1 byte per sample
	return samples;

}


VoiceCodecUlaw::VoiceCodecUlaw()
{
}

VoiceCodecUlaw::~VoiceCodecUlaw()
{
}

int VoiceCodecUlaw::initCheck(void)
{
	return 0;
}
int VoiceCodecUlaw::setParameters(const String8& keyValuePairs)
{
	return 0;
}

int VoiceCodecUlaw::framesize(){return 1;}

int VoiceCodecUlaw::decode(const unsigned char* src,const int samples,unsigned char* dst)
{
	unsigned short* target=(unsigned short*)dst;
	int length=samples;
	while(length--)
	{
		*target++ = ulaw2lin(*src++);
	}
	// 2 bytes per sample
	return samples*2;
}

int VoiceCodecUlaw::encode(const unsigned char *src,const int samples,unsigned char* dst)
{
	unsigned short* source=(unsigned short*)src;
	int length=samples;
	while(length--)
	{
		*dst++ = lin2ulaw(*source++);
	}

	// 1 byte per sample
	return samples;

}

/*
** This routine converts from linear to ulaw
**
** Craig Reese: IDA/Supercomputing Research Center
** Joe Campbell: Department of Defense
** 29 September 1989
**
** References:
** 1) CCITT Recommendation G.711  (very difficult to follow)
** 2) "A New Digital Technique for Implementation of Any
**     Continuous PCM Companding Law," Villeret, Michel,
**     et al. 1973 IEEE Int. Conf. on Communications, Vol 1,
**     1973, pg. 11.12-11.17
** 3) MIL-STD-188-113,"Interoperability and Performance Standards
**     for Analog-to_Digital Conversion Techniques,"
**     17 February 1987
**
** Input: Signed 16 bit linear sample
** Output: 8 bit ulaw sample
*/


unsigned char VoiceCodecUlaw::lin2ulaw(int sample)
{
#define ZEROTRAP    /* turn on the trap as per the MIL-STD */
#define BIAS 0x84   /* define the add-in bias for 16 bit samples */
#define CLIP 32635
	  static int exp_lut[256] = {0,0,1,1,2,2,2,2,3,3,3,3,3,3,3,3,
								 4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
								 5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
								 5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
								 6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
								 6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
								 6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
								 6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
								 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
								 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
								 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
								 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
								 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
								 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
								 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
								 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7};
	  int sign, exponent, mantissa;
	  unsigned char ulawbyte;
	
	  /* Get the sample into sign-magnitude. */
	  sign = (sample >> 8) & 0x80;		/* set aside the sign */
	  if (sign != 0) sample = -sample;		/* get magnitude */
	  if (sample > CLIP) sample = CLIP; 	/* clip the magnitude */
	
	  /* Convert from 16 bit linear to ulaw. */
	  sample = sample + BIAS;
	  exponent = exp_lut[(sample >> 7) & 0xFF];
	  mantissa = (sample >> (exponent + 3)) & 0x0F;
	  ulawbyte = ~(sign | (exponent << 4) | mantissa);
#ifdef ZEROTRAP
	  if (ulawbyte == 0) ulawbyte = 0x02;	/* optional CCITT trap */
#endif
	
	  return(ulawbyte);

}
int VoiceCodecUlaw::ulaw2lin(unsigned char ulawbyte)
{
	static int exp_lut[8] = {0,132,396,924,1980,4092,8316,16764};
	int sign, exponent, mantissa, sample;

	ulawbyte = ~ulawbyte;
	sign = (ulawbyte & 0x80);
	exponent = (ulawbyte >> 4) & 0x07;
	mantissa = ulawbyte & 0x0F;
	sample = exp_lut[exponent] + (mantissa << (exponent + 3));
	if (sign != 0) sample = -sample;

	return(sample);

}

}

