/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef _VOICE_GENERATOR_H
#define _VOICE_GENERATOR_H

#include <utils/Singleton.h>
#include <cutils/compiler.h>


namespace android {


//only support 8kHz sample rate ,SND_PCM_FORMAT_S16 ,mono channel

class VoiceGenerator 
{
	public:
        enum
        {
            kGeneratorTypeMute=0,
            kGeneratorTypeSine1K,
            kGeneratorTypeNum
        };

  
	   	VoiceGenerator();
		~VoiceGenerator();
		bool GenerateVoice(int type);
		int Read(unsigned char* buffer,int size);
		int FrameCount(){return mFrameCount;}
		void FrameCount(int framecount);
		
		int FrameSize(){return mFrameSize;}
		void FrameSize(int framesize);
		
	private:
		void GenerateMute(unsigned char* buffer,int size);
		void GenerateSine(unsigned char* buffer,int size,int frequency);

	private:
		int mFrameSize;
		int mFrameCount;
		unsigned char* mFrameBuffer;
		int voicetype;

		double phase;//for sin generator
};


//facility of often used voice generators
class MuteVoiceGenerator:public Singleton<MuteVoiceGenerator>,public VoiceGenerator
{
	public:
		static const int frame_size = 2;
		static const int frame_count=320;	
		MuteVoiceGenerator();
};


//facility of often used voice generators
class SineVoiceGenerator:public Singleton<SineVoiceGenerator>,public VoiceGenerator
{
	public:
		static const int frame_size = 2;
		static const int frame_count=160;	
		SineVoiceGenerator();
};



#define MUTE_READ(p,s) \
	MuteVoiceGenerator::getInstance().Read(p,s)

#define SINE_READ(p,s) \
	SineVoiceGenerator::getInstance().Read(p,s)


};
#endif 


