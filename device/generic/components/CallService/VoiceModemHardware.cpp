/*
*  This is a simple hardware voice modem common layer.
*  There is min frame count requirement for android media API. So we setup a cache buffer to handle this.
*  Also there is min frame count requirement for hardware voice modem,normally it's so called min period.
*  And it's assumed that all hardware voice modem can operate with 8000,PCM_16bit,MONO.
*  So things becomes complicated.
*  Schema:
*
*  voice modem downstream-->audio track buffer-->android mixer-->android audio HAL-->audio hardware
*
*  voice modem upstream<--audio record buffer <--audio hardware HAL<--audio hardware
*  
*  In general,for audio record case,audio hardware HAL should handle resample.I think I think android audio system
*  Also can charge for this,but I did not test with it.
*  
*  track buffer cache size should equal the bigger between modem minFrame count and audio track minFrame count
*  record buffer cache size should be equal the bigger between modem minFrame count and audio record minFrame count.
*      
*  
*/
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h> 
#include <stdio.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <utils/Timers.h>
#include <cutils/properties.h>
#include <media/mediarecorder.h>

#include <VoiceLog.h>
#include "IVoiceCall.h"
#include "VoiceCodecPCM.h"
#include "VoiceCodecUlaw.h"
#include "VoiceModemHardware.h"
#include "VoiceGenerator.h"
#include "VoiceSoftVol.h"

#define DEFAULT_PASSIVE_MODE true

#define VMODEM_DEF_DEV_NAME "/dev/ttyUSB2"
#define PROP_VMODEM_TARGET 		 "ril.vmodem.target"
#define PROP_VMODEM_PCM_FMT   	 "ril.vmodem.pcm"
#define PROP_VMODEM_PERIOD  	 "ril.vmodem.period"
#define PROP_VMODEM_DLINK        "ril.vmodem.dlink"
#define PROP_VMODEM_ULINK        "ril.vmodem.ulink"
#define PROP_VMODEM_DLINK_DUMP   "ril.vmodem.dlink.dump"
#define PROP_VMODEM_ULINK_DUMP   "ril.vmodem.ulink.dump"
#define PROP_VMODEM_HP_SCALE     "persist.vmodem.hpscale"
#define PROP_VMODEM_HF_SCALE     "persist.vmodem.hfscale"
#define PROP_VMODEM_PASSIVE      "persist.vmodem.passive"

//for sample rate 8000
#define FRAME_COUNT_PER_MS          8

namespace android
{


static const char* readable_pcm_format(int format)
{
	//keep consistent with PCM_FMT_DEFINE
	static const char* readable[] = 
	{
		"unknown",
		"raw",
		"ulaw",
		"alaw",
	};
	if(format>VoiceModemHardware::PCM_FMT_MAX)	return readable[0];
	return readable[format];
}
VoiceModemHardware::VoiceModemHardware():
latency_us(5000),
fd(-1),
mpVoiceCallTrack(NULL),
mpVoiceCallRecord(NULL),
codec(NULL),
voice_fmt(PCM_FMT_DEFAULT),
rw_period_ms(20),
framesize(2),
downlinkDump(NULL),
uplinkDump(NULL),
volume(1.0),
passive_mode(DEFAULT_PASSIVE_MODE)
{
    cached_trackbuffer = cached_recordbuffer = NULL;
    max_cached_trackframes = cached_trackframes = min_trackframes = min_modemdownstreamframes = 0;
    max_cached_recordframes = cached_recordframes = min_recordframes = min_modemupstreamframes = 0;
    dlink_disabled = ulink_disabled = false;
    set_path(IVoiceCall::kVoicePathDefault);
	memset(&status,0,sizeof(modem_status));
	attach();
}

VoiceModemHardware::~VoiceModemHardware()
{
	end_call();
	detach();
}

int VoiceModemHardware::init_hw_modem(int _baudrate)
{
	VoiceModemHardware* modem = this;
	int baudrate;
		
	char target[PROPERTY_VALUE_MAX];

    property_get(PROP_VMODEM_TARGET, target, VMODEM_DEF_DEV_NAME);

    VERBOSE("connect hw voice modem %s",target);
	//try to open VoiceModemHardware
	fd = ::open(target,O_RDWR|O_NOCTTY|O_NONBLOCK);//
	if(fd<0)
	{
		ERROR("open %s failed[%d]",target,fd);
		return -1;
	}
	else
	{
		VERBOSE("init voice modem in baudrate %d",_baudrate);
	}
	// Get the current options for the port...
	tcgetattr(modem->fd, &modem->oldtio);
	tcgetattr(modem->fd, &modem->newtio);
	
	bzero (&modem->newtio, sizeof (struct termios));
	// Set the baud rates to 9600...
	switch(_baudrate)
	{
		case 9600:baudrate = B9600; break;
		case 38400:baudrate = B38400; break;		
		case 115200:
		default:	
			baudrate = B115200; break;
	}
	
	modem->newtio.c_cflag = baudrate | CS8 | CLOCAL | CREAD;//databit8 ,receive enable
	modem->newtio.c_iflag = IGNPAR|IGNBRK;
	modem->newtio.c_iflag &=~(ICRNL);
	modem->newtio.c_oflag = 0;
	modem->newtio.c_lflag = 0;
	modem->newtio.c_cc[VTIME] = 1;   /* return after 0.1s */
	modem->newtio.c_cc[VMIN] = 0;   	
	tcflush (modem->fd, TCIOFLUSH);
	tcsetattr (modem->fd, TCSANOW, &modem->newtio);

	return 0;
}

int VoiceModemHardware::deinit_hw_modem(void)
{
	VoiceModemHardware* modem = this;
	
	if(fd>=0)
	{
		tcflush (modem->fd, TCIOFLUSH);
		tcsetattr (modem->fd, TCSANOW, &modem->oldtio);
		::close(fd);
		fd=-1;
	}

	return 0;
	
}


int VoiceModemHardware::attach(void)
{
	AutoMutex lock(&mLock);
	
	if(status.attached)
	{
		ERROR("modem device already attached");
		return -1;
	}
	if(!cached_trackbuffer)
		cached_trackbuffer = new unsigned char[max_cached_trackframes*framesize];

    //if(!passive_mode)
    {
        int priority_callin = PRIORITY_AUDIO;
        int priority_callout = PRIORITY_AUDIO;
        // Start voicecall thread
        mReaderThread = new ModemReaderThread(this);
        if (mReaderThread!=0){
            mReaderThread->run("callin", priority_callin);            
        } else{
            WARN("failed to create callin thread");
        }

         mDispatchThread = new ModemDispatchThread(this);
         if (mDispatchThread!=0){
              mDispatchThread->run("callout", priority_callout);            
         }else{
              WARN("failed to create callin thread");
         }
          
        
    }
    
	status.attached=1;
	return 0;

}

int VoiceModemHardware::detach(void)
{
	AutoMutex lock(&mLock);
	
	if (status.attached==1)
	{	    
        if (mReaderThread != 0) {
            mReaderThread->exit();
            mReaderThread.clear();        
        }
	
        if (mDispatchThread != 0) {
              mDispatchThread->exit();
              mDispatchThread.clear();        
        }
	
		if(cached_trackbuffer)
		{
			delete [] cached_trackbuffer;
			cached_trackbuffer = NULL;
		}
        if(softvol_trackbuffer)
        {
			delete [] softvol_trackbuffer;
			softvol_trackbuffer = NULL;            
        }
        if(cached_recordbuffer)
        {
			delete [] cached_recordbuffer;
            cached_recordbuffer = NULL;
        }
		status.attached=0;
	}
	return 0;

}

/*
 * optimized algorithm:
 * If cache is empty,we try reading from modem in max cache frames;
 * If request frame is smaller than cached buffer,copy cache to request buffer,otherwise ignore this request.
*/
int VoiceModemHardware::read_from_modem(char * buffer,int frames)
{
    int requestFrames = frames;
	if(!cached_trackframes)
	{
		int read_size;
        int convert_frames;
		if(PCM_FMT_ULAW==voice_fmt)
			read_size = ULAWDECODE(read_buf,::read(fd,read_buf,max_cached_trackframes),softvol_trackbuffer);
		else
			read_size = ::read(fd,softvol_trackbuffer,max_cached_trackframes*framesize);
		if(read_size<=0)
		{
		    INFO("modem is busy\n");
			read_size = MUTE_READ(softvol_trackbuffer,max_cached_trackframes*framesize);
            status.hw_ready=0;
		}
        else if(!status.hw_ready)
        {
            status.hw_ready = 1;
        }
        
        cached_trackframes = convert_frames= read_size/framesize;
        cached_trackframes = VOICECALL_DATA((char*)cached_trackbuffer,(char*)softvol_trackbuffer,(snd_pcm_uframes_t)cached_trackframes,(snd_pcm_uframes_t*)&convert_frames);            
            
        

	}
	INFO("vc track event, request frames[%d] cached:consumed frames[%d:%d]",requestFrames,cached_trackframes,consumed_trackframes);
	unsigned char* frameptr;
	frameptr = cached_trackbuffer+consumed_trackframes*framesize;

	
	if(requestFrames<=(cached_trackframes-consumed_trackframes))
	{
		memcpy(buffer,frameptr,requestFrames*framesize);
        if(downlinkDump)
            fwrite(frameptr,1,requestFrames*framesize,downlinkDump);
		consumed_trackframes+=requestFrames;
        if(consumed_trackframes==cached_trackframes)
            consumed_trackframes = cached_trackframes = 0;
        if((cached_trackframes-consumed_trackframes)>(2*min_modemdownstreamframes))
        {
            WARN("%d track frames will be dropped\n",cached_trackframes-consumed_trackframes);
            cached_trackframes=0;
            consumed_trackframes=0;
        }
	}
    else
    {
        WARN("bogus request[%d],cached[%d]\n",requestFrames,cached_trackframes);
        if(cached_trackframes)
        {
            memcpy(buffer,frameptr,cached_trackframes*framesize);
            requestFrames = cached_trackframes;
        }
        cached_trackframes = consumed_trackframes = 0;
        
    }

    return requestFrames;
    
}
int VoiceModemHardware::write_to_modem(char* buffer,int frames)
{
    int receivedFrames = frames;
    int writtensize=0;
    int space_frames;
    if(!frames) return 0;
    if(uplinkDump)
        fwrite(buffer,1,receivedFrames*framesize,uplinkDump);
    space_frames = max_cached_recordframes-cached_recordframes;
    if(space_frames>=receivedFrames)
    {
        memcpy(cached_recordbuffer+cached_recordframes*framesize,buffer,receivedFrames*framesize);
        cached_recordframes+=receivedFrames;
    }        
    else
    {
        WARN("bogus record[%d],cached[%d]\n",receivedFrames,cached_recordframes);
    }

    if(cached_recordframes>=min_modemupstreamframes)
    {
        
        if(0)//!status.hw_ready) 
        {
            WARN("modem not ready for uplink\n");
            writtensize = min_modemupstreamframes*framesize;
        }
        else
        {
            
            if(init_modemupstream_dropframes)
            {
                MUTE_READ(cached_recordbuffer,min_modemupstreamframes*framesize);
                if(init_modemupstream_dropframes>=min_modemupstreamframes)
                    init_modemupstream_dropframes-=receivedFrames;
                else
                    init_modemupstream_dropframes=0;
            }

    		if(PCM_FMT_ULAW==voice_fmt)
    		{
    		    writtensize = ::write(fd,write_buf,ULAWENCODE((unsigned char*)cached_recordbuffer,min_modemupstreamframes/*samples*/,write_buf));
                writtensize*=2; //ulaw 1 sample 1bytes ,but source is 2bytes
            }
    		else
    		{			
    			writtensize = ::write(fd,cached_recordbuffer,min_modemupstreamframes*framesize);
    		}
    		
        }

        if(writtensize!=min_modemupstreamframes*framesize)
        {
            INFO("written[%d]expect[%d]\n",writtensize,min_modemupstreamframes*framesize);                    
        }
        //anyway move remain frames to buffer start
        int remain_frames = cached_recordframes-writtensize/framesize;//min_modemupstreamframes;
        if(remain_frames)
        {
            memmove(cached_recordbuffer,cached_recordbuffer+writtensize,remain_frames*framesize);                
        }
        VERBOSE("write record frames[%d],cached[%d]",writtensize/framesize,cached_recordframes);  
        cached_recordframes-=writtensize/framesize;
    }    

    return writtensize/framesize;
}

void VoiceModemHardware::process_record_event(int event,void* info)
{
    AudioRecord::Buffer* buffer = static_cast<AudioRecord::Buffer *>(info);
    int receivedFrames = buffer->frameCount;    

	VERBOSE("vc record event ,got frames[%d]",buffer->frameCount);	

	mRecordLock.lock();
	if(sSTARTING==status.recordstatus)
	{
		status.recordstatus = sSTARTED;
		mWaitRecordCond.signal();
	}
	else if(sSTOPPING==status.recordstatus)
	{
		mpVoiceCallRecord->stop();
		status.recordstatus = sSTOPPED;
		mWaitRecordCond.signal();
	}	
	mRecordLock.unlock();

    write_to_modem((char*)buffer->raw,receivedFrames);



}
void VoiceModemHardware::process_track_event(int event,void* info)
{
    AudioTrack::Buffer* buffer = static_cast<AudioTrack::Buffer *>(info);
    int requestFrames = (int)buffer->frameCount;
	VERBOSE("vc track event ,request frames[%d]",requestFrames);	

	mTrackLock.lock();
	if(sSTARTING==status.trackstatus)
	{
		status.trackstatus = sSTARTED;
		mWaitTrackCond.signal();
	}
	else if(sSTOPPING==status.trackstatus)
	{
		mpVoiceCallTrack->stop();
		status.trackstatus = sSTOPPED;
		mWaitTrackCond.signal();
	}
	mTrackLock.unlock();
    if (buffer->size == 0) return;

    requestFrames = read_from_modem((char*)buffer->raw,requestFrames);        
    if(requestFrames!=(int)buffer->frameCount)
        buffer->frameCount = (ssize_t)requestFrames;

}


void VoiceModemHardware::vc_track_callback(int event, void* user, void *info)
{

	if(event!=AudioTrack::EVENT_MORE_DATA) return;
	VoiceModemHardware* vm=static_cast<VoiceModemHardware *>(user);
    AudioTrack::Buffer *buffer = static_cast<AudioTrack::Buffer *>(info);
    vm->process_track_event(event,info);	
}

void VoiceModemHardware::vc_record_callback(int event,void* user,void * info)
{

	if(event!=AudioRecord::EVENT_MORE_DATA) return;
	VoiceModemHardware* vm=static_cast<VoiceModemHardware *>(user);
    AudioRecord::Buffer *buffer = static_cast<AudioRecord::Buffer *>(info);	
    vm->process_record_event(event,info);
}


//read from modem and write to audio track
void VoiceModemHardware::reader_loop(void)
{
    char buffer[320];
    int frames =  read_from_modem(buffer,320/framesize);
    mpVoiceCallTrack->write(buffer,frames*framesize);
    
}
void VoiceModemHardware::dispatch_loop(void)
{
    char buffer[320];
    int size =  mpVoiceCallRecord->read(buffer,320);
    if(size>0)
        write_to_modem(buffer,size/framesize);
}

int VoiceModemHardware::start_call(void)
{	
    int new_max_cached_trackframes,new_max_cached_recordframes;
	char value[PROPERTY_VALUE_MAX];
	AutoMutex lock(&mLock);
	

	VERBOSE("start voice modem call");

	if(status.incall==1)
		return 0;


	//start modem hardware 
	if(init_hw_modem(115200)<0)
	{
		ERROR("open voice modem hardware failed");
		return -1;
	}
    //even init ,but modem may not be ready
    status.hw_ready = 0;

    property_get(PROP_VMODEM_PCM_FMT, value, "");
	if(!strcmp(value,"ulaw"))
	{
		voice_fmt = PCM_FMT_ULAW;
	}
	else if(!strcmp(value,"raw"))
	{
		voice_fmt = PCM_FMT_RAW;
	}
	else if(!strcmp(value,"alaw"))
	{
		voice_fmt = PCM_FMT_ALAW;
	}

	VERBOSE("modem pcm format == %s\n",readable_pcm_format(voice_fmt));
	/*
	   TODO: use new pattern of voice codec design
	*/
	switch(voice_fmt)
	{
		case PCM_FMT_ULAW:
			codec = new VoiceCodecUlaw();
			break;
		case PCM_FMT_RAW:
		default:
			codec = new VoiceCodecPCM();
			break;
	}
	if(codec->initCheck()) goto start_failure;

    property_get(PROP_VMODEM_DLINK, value, "");
    if(!strcmp(value,"disable"))
	{
	    dlink_disabled = true;
	}
    else if(!strcmp(value,"enable"))
    {
        dlink_disabled = false;
    }
    property_get(PROP_VMODEM_ULINK, value, "");
    if(!strcmp(value,"disable"))
	{
	    ulink_disabled = true;
	}
    else if(!strcmp(value,"enable"))
    {
        ulink_disabled = false;
    }
    
    property_get(PROP_VMODEM_DLINK_DUMP, value, 0);
	if(strlen(value))
	{
		if(downlinkDump)
			fclose(downlinkDump);
		downlinkDump = fopen(value, "wb");
		if(!downlinkDump)
		{
			LOGW("failed to access downlink dump file %s ",value);
		}		
	}
    property_get(PROP_VMODEM_ULINK_DUMP, value, 0);
	if(strlen(value))
	{
		if(uplinkDump)
			fclose(uplinkDump);
		uplinkDump = fopen(value, "wb");
		if(!uplinkDump)
		{
			LOGW("failed to access uplink dump file %s ",value);
		}		
	}
    property_get(PROP_VMODEM_PASSIVE, value, "");
    if(!strcmp(value,"true"))
	{
	    INFO("passive mode");
	    passive_mode = true;
	}
    else if(!strcmp(value,"false"))
    {
        passive_mode = false;
    }

    //decide track cache buffer size
	{
		int rw_period_new;
		property_get(PROP_VMODEM_PERIOD, value, "20");
		if(strlen(value))
		{
			rw_period_new = atoi(value);
			if(rw_period_new!=rw_period_ms)
			{
				rw_period_ms = rw_period_new;
			}
		}
	}
    DBG("voice call %s mode",passive_mode?"passive":"active");
    min_modemdownstreamframes = rw_period_ms*FRAME_COUNT_PER_MS;
    AudioTrack::getMinFrameCount(&min_trackframes,AudioSystem::VOICE_CALL,8000);
    //increase cache buffer to solve delay issue
    new_max_cached_trackframes = min_modemdownstreamframes*4;//min_trackframes>min_modemdownstreamframes?min_trackframes:min_modemdownstreamframes;
    cached_trackframes = consumed_trackframes = 0;
    if(new_max_cached_trackframes!=max_cached_trackframes)
    {
        max_cached_trackframes = new_max_cached_trackframes;
        delete [] cached_trackbuffer;
        cached_trackbuffer = new unsigned char[max_cached_trackframes*framesize];

        delete [] softvol_trackbuffer;
        softvol_trackbuffer = new unsigned char[max_cached_trackframes*framesize];
    }
    DBG("min_modemdownstreamframes=%d",min_modemdownstreamframes);
    DBG("min_trackframes=%d",min_trackframes);
    DBG("max_cached_trackframes=%d",max_cached_trackframes);    
    DBG("cached_trackframes=%d",cached_trackframes);

    min_modemupstreamframes = rw_period_ms*FRAME_COUNT_PER_MS;
    AudioRecord::getMinFrameCount(&min_recordframes,8000,AudioSystem::PCM_16_BIT,1);
    new_max_cached_recordframes = min_recordframes>min_modemupstreamframes?min_recordframes:min_modemupstreamframes;
    cached_recordframes = 0;
    if(new_max_cached_recordframes!=max_cached_recordframes)
    {
        max_cached_recordframes = new_max_cached_recordframes;
        delete [] cached_recordbuffer;
        cached_recordbuffer = new unsigned char[max_cached_recordframes*framesize];
    }
    init_modemupstream_dropframes = min_modemupstreamframes*10;//20*10 = 0.2s
    
    DBG("min_modemupstreamframes=%d",min_modemupstreamframes);
    DBG("min_recordframes=%d",min_recordframes);
    DBG("max_cached_recordframes=%d",max_cached_recordframes);        
    DBG("cached_recordframes=%d",cached_recordframes);
    DBG("init_modemupstream_dropframes=%d",init_modemupstream_dropframes);
    
	if(!cached_trackbuffer||!cached_recordbuffer) goto start_failure;

    if(true!=dlink_disabled)
	{
    	//create voicecall track 	
    	mpVoiceCallTrack = new AudioTrack(AudioSystem::VOICE_CALL,
    			  8000,
    			  AudioSystem::PCM_16_BIT,
    			  AudioSystem::CHANNEL_OUT_MONO,
    			  min_trackframes*4,//ensure is times of vc packet, 8 times of cached frames is enough
    			  0,
    			  (true==passive_mode)?VoiceModemHardware::vc_track_callback:0,
    			  this,
    			  min_trackframes//20ms voicecall data
    			  );
    	if(!mpVoiceCallTrack) goto start_failure;
    		
    	if (mpVoiceCallTrack->initCheck() != NO_ERROR) goto start_failure;

    	mpVoiceCallTrack->setVolume(1.0, 1.0);	
    		
    	VERBOSE("create vc track object ok");
    }
    else
    {
	    WARN("vc track disabled");
    }

    if(true!=ulink_disabled)
	{
    	mpVoiceCallRecord = new AudioRecord(AUDIO_SOURCE_DEFAULT,//AUDIO_SOURCE_VOICE_CALL,
			8000,
			AudioSystem::PCM_16_BIT,
			AudioSystem::CHANNEL_IN_MONO,
			min_recordframes*4,//ensure is times of vc packet, 8 times of cached frames is enough
			0,
			(true==passive_mode)?VoiceModemHardware::vc_record_callback:0,
			this,
			min_recordframes//20ms voicecall data			
			);
    	if(!mpVoiceCallRecord) goto start_failure;
    	if (mpVoiceCallRecord->initCheck() != NO_ERROR) goto start_failure;

    	VERBOSE("create vc record object ok");
    }
    else
    {
        WARN("vc record disabled");
    }

	//reset track/record status
	//start now
    if(true!=ulink_disabled)
	{
	    if(true==passive_mode){
    		status_t lStatus;
    		mRecordLock.lock();
    		status.recordstatus= VoiceModemHardware::sSTARTING;
    		mRecordLock.unlock();		
    		mpVoiceCallRecord->start();
    		mRecordLock.lock();
    		if (VoiceModemHardware::sSTARTING == status.recordstatus) {
    			VERBOSE("Wait for start record callback");
    			lStatus = mWaitRecordCond.waitRelative(mRecordLock, seconds(3));
    			if (lStatus != NO_ERROR) {
    				ERROR("--- Immediate start timed out, status %d", lStatus);
    			}
    		}
    		mRecordLock.unlock();
        }else{
            mpVoiceCallRecord->start();
            if(mDispatchThread!=0)
                mDispatchThread->start();
        }
		
	}
    
    if(true!=dlink_disabled)
	{
	    if(true==passive_mode){
    		status_t lStatus;
    		mTrackLock.lock();
    		status.trackstatus = VoiceModemHardware::sSTARTING;
    		mTrackLock.unlock();
    		mpVoiceCallTrack->start();
    		mTrackLock.lock();
    		
    		if (VoiceModemHardware::sSTARTING == status.trackstatus) {
    			VERBOSE("Wait for start track callback");
    			lStatus = mWaitTrackCond.waitRelative(mTrackLock, seconds(3));
    			if (lStatus != NO_ERROR) {
    				ERROR("--- Immediate start timed out, status %d", lStatus);
    			}
    		}
    		mTrackLock.unlock();
       }else{
            mpVoiceCallTrack->start();
            if(mReaderThread!=0)
                mReaderThread->start();
       }
		
	}



	status.incall=1;
	INFO("voice call running ");
	return 0;
start_failure:
	ERROR("start voice call failed");
	if(mpVoiceCallTrack)
	{
		mpVoiceCallTrack->stop();
		delete mpVoiceCallTrack;
		mpVoiceCallTrack = NULL;
	}
	if(mpVoiceCallRecord)
	{
		mpVoiceCallRecord->stop();
		delete mpVoiceCallRecord;
		mpVoiceCallRecord = NULL;
	}
	deinit_hw_modem();
	
	return -1;
}
int VoiceModemHardware::end_call(void)
{
	AutoMutex lock(&mLock);

	if(status.incall==0)
		return 0;
	VERBOSE("end voice modem call");

    int loop_count=0;
    while(status.hw_ready)
    {
        usleep(1000);loop_count++;
        if(loop_count>1500) break;//wait 1.5s at most.
    }
    if(true!=ulink_disabled)
	{
	    if(passive_mode){   
    		status_t lStatus;
    		mRecordLock.lock();
    		status.recordstatus= VoiceModemHardware::sSTOPPING;
    		mRecordLock.unlock();		
    		VERBOSE("stopping vc record");
    		mRecordLock.lock();		
    		if (VoiceModemHardware::sSTOPPING == status.recordstatus) {
    			VERBOSE("Wait for stop record callback");
    			lStatus = mWaitRecordCond.waitRelative(mRecordLock, seconds(3));
    			if (lStatus != NO_ERROR) {
    				ERROR("--- Immediate stop timed out, status %d", lStatus);
    			}
    			//else //stop any way
    			{
    				mpVoiceCallRecord->stop();				
    			}
    		}
    		mRecordLock.unlock();		
    		delete mpVoiceCallRecord;
    		mpVoiceCallRecord = NULL;
       }else{
            if(mDispatchThread!=0)
                  mDispatchThread->stop();                      
            mpVoiceCallRecord->stop();	
    		delete mpVoiceCallRecord;
    		mpVoiceCallRecord = NULL;            
                  
        }
        VERBOSE("stop voice call record ok");
       
    }


    if(true!=dlink_disabled)
	{
	    if(passive_mode){   
    		status_t lStatus;
    		mTrackLock.lock();
    		status.trackstatus = VoiceModemHardware::sSTOPPING;
    		mTrackLock.unlock();
    		VERBOSE("stopping vc track");
    		mTrackLock.lock();		
    		if (VoiceModemHardware::sSTOPPING == status.trackstatus) {
    			VERBOSE("Wait for stop track callback");
    			lStatus = mWaitTrackCond.waitRelative(mTrackLock, seconds(3));
    			if (lStatus != NO_ERROR) {
    				ERROR("--- Immediate stop timed out, status %d", lStatus);
    			}
    			//else //stop any way
    			{
    				mpVoiceCallTrack->stop();
    			}
    		}
    		mTrackLock.unlock();		
    		delete mpVoiceCallTrack;
    		mpVoiceCallTrack = NULL;
            }else {
                if (mReaderThread!=0) {
                    mReaderThread->stop();
                }
                mpVoiceCallTrack->stop();
               delete mpVoiceCallTrack;
               mpVoiceCallTrack=NULL;
          }

        VERBOSE("stop voice call track ok");
	}


	
	
	if(uplinkDump)
	{
		fclose(uplinkDump);
		uplinkDump = 0;
	}
	if(downlinkDump)
	{
		fclose(downlinkDump);
		downlinkDump = 0;
	}
	

	//destroy codec
	if(codec)
	{
	    delete codec;
	    codec = NULL;
    }
	//deinit modem hardware
	deinit_hw_modem();

	INFO("voice call stopped");

	status.incall=0;
	
	return 0;

}


void VoiceModemHardware::set_volume(float vol)
{

	char value[PROPERTY_VALUE_MAX];
	volume = vol;
	int min,scale,softvol;
    switch(voicePath)
    {
        default:
        case IVoiceCall::kVoicePathHeadset:
        case IVoiceCall::kVoicePathEarPiece:
        {
           int hp_scale;
           min = VOICECALL_VOL0DB();           
           property_get(PROP_VMODEM_HP_SCALE, value, "30");
           hp_scale=atoi(value);
           if(hp_scale<0)
            hp_scale==0;
           if(hp_scale>100)
            hp_scale=100;
           scale = ((VOICECALL_VOLMAX()-VOICECALL_VOL0DB())*hp_scale)/100;
        }
        break;
        
        case IVoiceCall::kVoicePathHandfree:
        {
            int hf_scale;
            property_get(PROP_VMODEM_HF_SCALE, value, "50");
            hf_scale=atoi(value);
            if(hf_scale<0)
             hf_scale==0;
            if(hf_scale>100)
             hf_scale=100;
            min = VOICECALL_VOL0DB();
            scale = ((VOICECALL_VOLMAX()-VOICECALL_VOL0DB())*hf_scale)/100;
        }
        break;        
    }

    softvol = min+((((int)(volume*100))*scale)/100);
    VERBOSE("%s vol=%f,softvol=%d,min=%d,max=%d,scale=%d\n",__func__,vol,softvol,min,VOICECALL_VOLMAX(),scale);

    VOICECALL_VOL(softvol);

    
}

void VoiceModemHardware::set_path(int path)
{
    if(path<IVoiceCall::kVoicePathMax)
    {
        voicePath = path;
        set_volume(volume);
        
    }
}


void VoiceModemHardware::ModemReaderThread::onFirstRef() 
{
      INFO("ModemReaderThread onFirstRef");
}

status_t VoiceModemHardware::ModemReaderThread::readyToRun()
{
      return NO_ERROR;
}

bool VoiceModemHardware::ModemReaderThread::threadLoop()
{
      INFO("VoiceCallListenThread: start voicecall loop");
      mActive = 0;
    while(!exitPending()) 
    {
       if(!mActive) 
        {
            mLock.lock();
            if (!mActive && !exitPending()) 
            {
            INFO("VoiceCallListenThread: loop stopping");
            mStopped.signal();
            mWaitWorkCV.wait(mLock);

                  INFO("VoiceCallListenThread: loop starting");
                  //put some voicecall init action here                           
                  mStartStatus = NO_ERROR;
                  mWaitWorkCV.signal();
            }
            mLock.unlock();
        }
       else 
       {
          modem->reader_loop();
          //release cpu some time
          usleep(modem->latency_us);
       }
    }
      return false;
      
}

int  VoiceModemHardware::ModemReaderThread::start(void)
{
    AutoMutex lock(&mLock);

    mActive = true;

    // signal thread to start
    mWaitWorkCV.signal();
    mWaitWorkCV.wait(mLock);

    return 0;

}
void VoiceModemHardware::ModemReaderThread::stop(void)
{
    AutoMutex lock(&mLock);

    if (mActive) {
    mActive = false;
    mStopped.wait(mLock);
    }


}

void VoiceModemHardware::ModemReaderThread::exit(void)
{
  {
      AutoMutex lock(&mLock);
      requestExit();
      mWaitWorkCV.signal();
  }
  requestExitAndWait();

}

void VoiceModemHardware::ModemDispatchThread::onFirstRef() 
{
      INFO("VoiceCallSpeakThread onFirstRef");
}

status_t VoiceModemHardware::ModemDispatchThread::readyToRun()
{
      return NO_ERROR;
}

bool VoiceModemHardware::ModemDispatchThread::threadLoop()
{
    INFO("ModemDispatchThread: start voicecall loop");
    mActive = 0;
    while(!exitPending()) 
    {
        if(!mActive) 
        {
            mLock.lock();
            if (!mActive && !exitPending()) 
            {
            INFO("ModemDispatchThread: loop stopping");
            mStopped.signal();
            mWaitWorkCV.wait(mLock);

                  INFO("ModemDispatchThread: loop starting");
                  //put some voicecall init action here
                  mStartStatus = NO_ERROR;
                  mWaitWorkCV.signal();
            }
            mLock.unlock();
        }
        else 
        {
          modem->dispatch_loop();
          //release cpu some time
          usleep(modem->latency_us);
        }
    }
    return false;
      
}

int  VoiceModemHardware::ModemDispatchThread::start(void)
{
    AutoMutex lock(&mLock);

    mActive = true;

    // signal thread to start
    mWaitWorkCV.signal();
    mWaitWorkCV.wait(mLock);

    return 0;

}
void VoiceModemHardware::ModemDispatchThread::stop(void)
{
    AutoMutex lock(&mLock);

    if (mActive) {
    mActive = false;
    mStopped.wait(mLock);
    }


}

void VoiceModemHardware::ModemDispatchThread::exit(void)
{
    {
      AutoMutex lock(&mLock);
      requestExit();
      mWaitWorkCV.signal();
    }
    requestExitAndWait();

}


}

