#ifndef _CODEC_H_
#define _CODEC_H_

#include <utils/String8.h>

namespace android
{
	/*	VoiceCodec is a the abstraction interface for voice format translation 
	    between voice modem and android platform.We assume all linear pcm
	    sample size is 16bit.The coded sample size is set by each type voice codec itself.
	*/
	class VoiceCodec
	{
		public:
			VoiceCodec(){}
			virtual ~VoiceCodec()=0;

			//init check ,return 0 if success
			virtual int initCheck(void)=0;

			//encoded frame size of each sample
			virtual int framesize()=0;

			virtual int  setParameters(const String8& keyValuePairs) = 0;
			
			virtual int decode(const unsigned char* src,const int samples,unsigned char* dst)=0;
			virtual int encode(const unsigned char *src,const int samples,unsigned char* dst)=0;
	};
}
#endif
