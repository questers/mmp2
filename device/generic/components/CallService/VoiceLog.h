/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _VOICE_LOG_H
#define _VOICE_LOG_H

#include <utils/Singleton.h>
#include <cutils/compiler.h>

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#ifdef LOG_NDEBUG
#undef LOG_NDEBUG
#endif

#define LOG_TAG "voicecall"
#define LOG_NDEBUG 0

#include <utils/Log.h>


namespace android {

class VoiceLogSetting : public Singleton<VoiceLogSetting>
{
	uint32_t mLogLevel;
public:
	enum
	{
		LOG_INFO			=0x1,
		LOG_WARN			=0x2,
		LOG_ERROR			=0x4,
		LOG_VERBOSE			=0x8,
		LOG_DEBUG			=0x10,
	};

    void setLevel(uint32_t level){mLogLevel = level;};
	uint32_t getLevel(){return mLogLevel;}
	
	
   	VoiceLogSetting();

};

#define VERBOSE(...) \
	LOGI_IF(VoiceLogSetting::getInstance().getLevel()&VoiceLogSetting::LOG_VERBOSE,__VA_ARGS__)
	
#define INFO(...) \
	LOGI_IF(VoiceLogSetting::getInstance().getLevel()&VoiceLogSetting::LOG_INFO,__VA_ARGS__)
	
#define WARN(...) \
	LOGI_IF(VoiceLogSetting::getInstance().getLevel()&VoiceLogSetting::LOG_WARN,__VA_ARGS__)

#define ERROR(...) \
	LOGI_IF(VoiceLogSetting::getInstance().getLevel()&VoiceLogSetting::LOG_ERROR,__VA_ARGS__)

#define DBG(...) \
	LOGI_IF(VoiceLogSetting::getInstance().getLevel()&VoiceLogSetting::LOG_DEBUG,__VA_ARGS__)



}

#endif 

