#include "IVoiceCallListener.h"

namespace android {

enum {
    TRANSACTION_onVoiceCallStateChanged = IBinder::FIRST_CALL_TRANSACTION,
};

class BpVoiceCallListener : public BpInterface<IVoiceCallListener>
{
public:
    BpVoiceCallListener(const sp<IBinder>& impl)
        : BpInterface<IVoiceCallListener>(impl)
    {
    }

    virtual void onVoiceCallStateChanged(int newState)
    {
        
    }
    
    
};

IMPLEMENT_META_INTERFACE(VoiceCallListener, "IVoiceCallListener");


status_t BnVoiceCallListener::onTransact(
    uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags)
{
    switch(code) {
        case TRANSACTION_onVoiceCallStateChanged: {
            CHECK_INTERFACE(IVoiceCallListener, data, reply);
            onVoiceCallStateChanged(data.readInt32());
            reply->writeNoException();
            return NO_ERROR;
        } break;
        default:
            return BBinder::onTransact(code, data, reply, flags);
    }
}
// ----------------------------------------------------------------------

};

