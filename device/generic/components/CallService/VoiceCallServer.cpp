#include <cutils/properties.h>
#include <binder/IServiceManager.h>
#include <VoiceModem.h>
#include <VoiceModemStub.h>
#include <VoiceModemHardware.h>
#include <VoiceCallServer.h>
#include <VoiceLog.h>
namespace android {



VoiceCallServer::VoiceCallServer()
    : BnVoiceCall()
{
    
	char value[PROPERTY_VALUE_MAX];
    property_get("ril.vc.impl", value, "");
    if(!strcmp(value,"hw"))
    {
        DBG("hardware voicemodem");
        vm = new VoiceModemHardware();
    }
    else
    {
        DBG("stub voicemodem");
        vm = new VoiceModemStub();
    }
    
    // start audio commands thread
    mServerCommandThread = new ServerCommandThread(String8("VoiceServerCommandThread"),vm);

    
}

VoiceCallServer::~VoiceCallServer()
{
    mServerCommandThread->exit();
    mServerCommandThread.clear();
    if(vm)
    {    
        vm->end_call();
        delete vm;
    }
}

int VoiceCallServer::startCall(void)
{
    DBG("VoiceCallServer::startCall");
    //if(vm)
    //    return vm->start_call();
    //return kERROR_NOTREADY;
    return mServerCommandThread->VoiceCallCommand(true);
}

int VoiceCallServer::endCall(void)
{
    DBG("VoiceCallServer::endCall");
    //if(vm)
    //    return vm->end_call();
    //return kERROR_NOTREADY;    
    return mServerCommandThread->VoiceCallCommand(false);

}
int VoiceCallServer::restartCall(void)
{
    DBG("VoiceCallServer::restartCall [deprecated]");
//    if(vm)
//    {
//        vm->end_call();
//        return vm->start_call();        
//    }
//    return kERROR_NOTREADY;
    return kERROR_OK;

}

/*
 * For software only API,we can call voicemodem member function directly
*/
int VoiceCallServer::setVolume(float volume)
{
    DBG("VoiceCallServer::setVolume");
    #if 1
    if(vm)
    {
        vm->set_volume(volume);
        return kERROR_OK;
    }
    return kERROR_NOTREADY;
    #else
    mServerCommandThread->VoiceCallVolumeCommand(volume);
   return kERROR_OK;
   #endif
    
}

int VoiceCallServer::setVoicePath(int path)
{
     DBG("VoiceCallServer::setVoicePath");
 #if 1
      if(vm)
      {
          vm->set_path(path);
          return kERROR_OK;
      }
      return kERROR_NOTREADY;
 #else
     mServerCommandThread->VoiceCallPathCommand(path);
     return kERROR_OK;
 #endif
    
}

void VoiceCallServer::binderDied(const wp<IBinder>& who) {

    VERBOSE("binderDied() %p, tid %d, calling tid %d", who.unsafe_get(), gettid(), IPCThreadState::self()->getCallingPid());
   // Mutex::Autolock _l(mLock);

    IBinder *binder = who.unsafe_get();

    if (binder != NULL) {
      //  int index = mNotificationClients.indexOf(binder);
        if (index >= 0) {
            VERBOSE("Removing notification client %p", binder);
          //  mNotificationClients.removeAt(index);
        }
    }
}


status_t VoiceCallServer::onTransact(
        uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags)
{
    return BnVoiceCall::onTransact(code, data, reply, flags);
}


// -----------  VoiceCallServer::ServerCommandThread implementation ----------

VoiceCallServer::ServerCommandThread::ServerCommandThread(String8 name,VoiceModem* _vm)
    : Thread(false), vm(_vm),mName(name)
{
}


VoiceCallServer::ServerCommandThread::~ServerCommandThread()
{
    mCommands.clear();
}

void VoiceCallServer::ServerCommandThread::onFirstRef()
{
    if (mName != "") {
        run(mName.string(), ANDROID_PRIORITY_NORMAL);
    } else {
        run("ServerCommandThread", ANDROID_PRIORITY_NORMAL);
    }
}

bool VoiceCallServer::ServerCommandThread::threadLoop()
{
    mLock.lock();
    while (!exitPending())
    {
        while(!mCommands.isEmpty()) 
        {
                ServerCommand *command = mCommands[0];
                mCommands.removeAt(0);
                mLastCommand = *command;

                switch (command->mCommand) {
                case COMMAND_VOICECALL: 
                    {
                        CallData *data = (CallData *)command->mParam;
                        if(data->start)
                            vm->start_call();                            
                        else
                            vm->end_call();        
                        command->mStatus = NO_ERROR;
                        if (command->mWaitStatus) {
                        command->mCond.signal();
                        mWaitWorkCV.wait(mLock);
                        }
                        delete data;
                    }
                    break;
                case COMMAND_VOICECALL_VOLUME: 
                    {
                        CallData *data = (CallData *)command->mParam; 
                        vm->set_volume(data->vol);
                        command->mStatus = NO_ERROR;
                        if (command->mWaitStatus) {
                        command->mCond.signal();
                        mWaitWorkCV.wait(mLock);
                        }
                        delete data;
                    }
                    break;
                case COMMAND_VOICECALL_PATH: 
                    {
                        CallData *data = (CallData *)command->mParam; 
                        vm->set_path(data->path);
                        command->mStatus = NO_ERROR;
                        if (command->mWaitStatus) {
                        command->mCond.signal();
                        mWaitWorkCV.wait(mLock);
                        }
                        delete data;
                    }
                    break;
                    
               
                default:
                    WARN("ServerCommandThread() unknown command %d", command->mCommand);
                    break;
                }
                delete command;
        }
        INFO("ServerCommandThread() going to sleep");
        mWaitWorkCV.wait(mLock );
        INFO("ServerCommandThread() waking up");
    }
    mLock.unlock();
    return false;
}


status_t VoiceCallServer::ServerCommandThread::VoiceCallCommand(bool start )
{
    ServerCommand *command = new ServerCommand();
    command->mCommand = COMMAND_VOICECALL;
    CallData *data = new CallData();
    data->start = start;
    command->mParam = data;
    return  insertCommand(command);
}

status_t VoiceCallServer::ServerCommandThread::VoiceCallVolumeCommand(float vol)
{
    ServerCommand *command = new ServerCommand();
    command->mCommand = COMMAND_VOICECALL_VOLUME;
    CallData *data = new CallData();
    data->vol = vol ;
    command->mParam = data;
    return  insertCommand(command);
}

status_t    VoiceCallServer::ServerCommandThread::VoiceCallPathCommand(int path)
{
    ServerCommand *command = new ServerCommand();
    command->mCommand = COMMAND_VOICECALL_PATH;
    CallData *data = new CallData();
    data->path = path ;
    command->mParam = data;
    return  insertCommand(command);
    
}


status_t VoiceCallServer::ServerCommandThread::insertCommand(ServerCommand *command, bool wait_finish)
{
    status_t status = NO_ERROR;
    command->mWaitStatus = wait_finish;
    Mutex::Autolock _l(mLock);
    insertCommand_l(command);
    mWaitWorkCV.signal();
    if (command->mWaitStatus) {
        command->mCond.wait(mLock);
        status =  command->mStatus;
        mWaitWorkCV.signal();
    }
    return status;
}



// insertCommand_l() must be called with mLock held
void VoiceCallServer::ServerCommandThread::insertCommand_l(ServerCommand *command)
{
    mCommands.add(command);
}

void VoiceCallServer::ServerCommandThread::exit()
{
    DBG("ServerCommandThread::exit");
    {
        AutoMutex _l(mLock);
        requestExit();
        mWaitWorkCV.signal();
    }
    requestExitAndWait();
}


}

