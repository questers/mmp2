
#define LOG_TAG "IVoiceCall"
#include <binder/Parcel.h>
#include <IVoiceCall.h>
#include <VoiceLog.h>

namespace android
{

enum
{
    TRANSACTION_registerListener = IBinder::FIRST_CALL_TRANSACTION,
    TRANSACTION_unregisterListener,        
    TRANSACTION_startCall,
    TRANSACTION_endCall,
    TRANSACTION_restartCall,
    TRANSACTION_setVolume,
    TRANSACTION_setPath,
};

class BpVoiceCall : public BpInterface<IVoiceCall>
{
public:
    BpVoiceCall(const sp<IBinder>& impl)
        : BpInterface<IVoiceCall>(impl)
    {
    }

    /*
    virtual void registerListener(const sp<IVoiceCallListener>& listener)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IVoiceCall::getInterfaceDescriptor());
        data.writeStrongBinder(listener->asBinder());
        if (remote()->transact(TRANSACTION_registerListener, data, &reply) != NO_ERROR) {
            DBG("registerListener could not contact remote\n");
            return;
        }
        int32_t err = reply.readExceptionCode();
        if (err < 0) {
            DBG("registerListener caught exception %d\n", err);
            return;
        }
    }

    virtual void unregisterListener(const sp<IVoiceCallListener>& listener)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IVoiceCall::getInterfaceDescriptor());
        data.writeStrongBinder(listener->asBinder());
        if (remote()->transact(TRANSACTION_unregisterListener, data, &reply) != NO_ERROR) {
            DBG("unregisterListener could not contact remote\n");
            return;
        }
        int32_t err = reply.readExceptionCode();
        if (err < 0) {
            DBG("unregisterListener caught exception %d\n", err);
            return;
        }
    }
    */
    virtual int startCall(void)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IVoiceCall::getInterfaceDescriptor());
        if (remote()->transact(TRANSACTION_startCall, data, &reply) != NO_ERROR)
        {
            DBG("startCall could not contact remote\n");
            return -1;
        }
        int32_t err = reply.readExceptionCode();
        if (err < 0)
        {
            DBG("startCall caught exception %d\n", err);
            return -2;
        }
        return reply.readInt32();

    }
    virtual int endCall(void)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IVoiceCall::getInterfaceDescriptor());
        if (remote()->transact(TRANSACTION_endCall, data, &reply) != NO_ERROR)
        {
            DBG("endCall could not contact remote\n");
            return -1;
        }
        int32_t err = reply.readExceptionCode();
        if (err < 0)
        {
            DBG("endCall caught exception %d\n", err);
            return -2;
        }
        return reply.readInt32();

    }
    virtual int restartCall(void)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IVoiceCall::getInterfaceDescriptor());
        if (remote()->transact(TRANSACTION_restartCall, data, &reply) != NO_ERROR)
        {
            DBG("restartCall could not contact remote\n");
            return -1;
        }
        int32_t err = reply.readExceptionCode();
        if (err < 0)
        {
            DBG("restartCall caught exception %d\n", err);
            return -2;
        }
        return reply.readInt32();

    }
    virtual int setVolume(float volume)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IVoiceCall::getInterfaceDescriptor());
        data.writeFloat(volume);
        if (remote()->transact(TRANSACTION_setVolume, data, &reply) != NO_ERROR)
        {
            DBG("setVolume could not contact remote\n");
            return -1;
        }
        int32_t err = reply.readExceptionCode();
        if (err < 0)
        {
            DBG("setVolume caught exception %d\n", err);
            return -2;
        }
        return reply.readInt32();

    }

    virtual int setVoicePath(int path)
    {
        Parcel data, reply;
        data.writeInterfaceToken(IVoiceCall::getInterfaceDescriptor());
        data.writeInt32(path);
        if (remote()->transact(TRANSACTION_setPath, data, &reply) != NO_ERROR)
        {
            DBG("setVoicePath could not contact remote\n");
            return -1;
        }
        int32_t err = reply.readExceptionCode();
        if (err < 0)
        {
            DBG("setVoicePath caught exception %d\n", err);
            return -2;
        }
        return reply.readInt32();

    }

    


};



IMPLEMENT_META_INTERFACE(VoiceCall, "IVoiceCall");

status_t BnVoiceCall::onTransact(
    uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags)
{
    switch(code) {
        case TRANSACTION_startCall:
            {
                CHECK_INTERFACE(IVoiceCall, data, reply);
                reply->writeInt32(startCall());                
                return NO_ERROR;
            }
            break;
        case TRANSACTION_endCall:
            {
                CHECK_INTERFACE(IVoiceCall, data, reply);
                reply->writeInt32(endCall());   
                return NO_ERROR;
            }
            break;
        case TRANSACTION_restartCall:
            {
                CHECK_INTERFACE(IVoiceCall, data, reply);
                reply->writeInt32(restartCall());      
                return NO_ERROR;               
            }
            break;
        case TRANSACTION_setVolume:
            {
                CHECK_INTERFACE(IVoiceCall, data, reply);
                reply->writeInt32(setVolume(data.readFloat()));            
                return NO_ERROR;
            }
            break;
        case TRANSACTION_setPath:
            {
                CHECK_INTERFACE(IVoiceCall, data, reply);
                reply->writeInt32(setVoicePath(data.readInt32()));            
                return NO_ERROR;
            }
            break;
            
        default:
            return BBinder::onTransact(code, data, reply, flags);
    }
}

};

