#ifndef VOICECALLSERVER_H
#define VOICECALLSERVER_H

#include <IVoiceCall.h>
#include <binder/BinderService.h>

namespace android {
class VoiceModem;
class VoiceCallServer : 
public BinderService<VoiceCallServer>,
public BnVoiceCall
{
    friend class BinderService<VoiceCallServer>;

public:	
    static char const* getServiceName() { return "media.voicecallserver"; }
public:

    virtual int startCall(void);
    virtual int endCall(void);
    virtual int restartCall(void);
    virtual int setVolume(float volume);	
	virtual int setVoicePath(int path);

    // IBinder::DeathRecipient
    virtual 	void		binderDied(const wp<IBinder>& who);

    virtual 	status_t	onTransact(uint32_t code,const Parcel& data,Parcel* reply,uint32_t flags);


private:
	VoiceCallServer();
	virtual      ~VoiceCallServer();

	class ServerCommandThread : public Thread
	{
		class ServerCommand;
		public:
			//commands for server
			enum
			{
				COMMAND_VOICECALL,
				COMMAND_VOICECALL_VOLUME,
				COMMAND_VOICECALL_PATH,
				COMMAND_MAX,
			};
			ServerCommandThread (String8 name,VoiceModem* _vm);
			virtual 			~ServerCommandThread();			
		
			
			status_t	VoiceCallCommand(bool start);	
			status_t	VoiceCallVolumeCommand(float vol);	
			status_t	VoiceCallPathCommand(int path);	
			
			void		exit();
		public:			
			VoiceModem* vm; 

		protected:	
			inline status_t insertCommand(ServerCommand *command, bool wait_finish=false);
			inline void insertCommand_l(ServerCommand *command);

		private:
			// Thread virtuals
			virtual 	void		onFirstRef();
			virtual 	bool		threadLoop();

			
			class ServerCommand
			{
				public:
					ServerCommand():mCommand(-1){}
					
					int mCommand;	//
					nsecs_t mTime;	// time stamp
					Condition mCond; // condition for status return
					status_t mStatus; // command status
					bool mWaitStatus; // true if caller is waiting for status
					void *mParam;	  // command parameter ()
					
			};

			class CallData
			{
				public:
					bool start;
					float vol;
					int path;
			};

	        Mutex   mLock;
	        Condition mWaitWorkCV;
	        Vector <ServerCommand *> mCommands; // list of pending commands
	        ServerCommand mLastCommand;          // last processed command (used by dump)
	        String8 mName;                      // string used by wake lock fo delayed commands
	        
		
	};
	
	private:		
		VoiceModem* vm; 
		sp <ServerCommandThread> mServerCommandThread;	// adm commands thread


};
};
#endif

