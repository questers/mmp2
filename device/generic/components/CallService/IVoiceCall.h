#ifndef _IVOICECALL_H_
#define _IVOICECALL_H_

#include <binder/IInterface.h>
#include <binder/Parcel.h>
#include <IVoiceCallListener.h>

namespace android {

class IVoiceCall:public IInterface
{
public:
	DECLARE_META_INTERFACE(VoiceCall);
	
	enum
	{
		kERROR_OK=0,
		kERROR_NOTREADY,
		kERROR_MODEM,	//modem 
		kERROR_GENERIC=-100
	};
    //virtual void registerListener(const sp<IVoiceCallListener>& listener) = 0;
   // virtual void  unregisterListener(const sp<IVoiceCallListener>& listener) = 0;

	enum
    {    	
    	kVoicePathHeadset=0,
		kVoicePathEarPiece,
		kVoicePathHandfree,
		kVoicePathDefault = kVoicePathHandfree,
		kVoicePathMax,
    };

	virtual int startCall(void)=0;
	virtual int endCall(void)=0;
	virtual int restartCall(void)=0;
	virtual int setVolume(float volume)=0;

	virtual int setVoicePath(int path)=0;
	

};

class BnVoiceCall : public BnInterface<IVoiceCall>{
public:
    virtual status_t onTransact(uint32_t code, const Parcel& data,
            Parcel* reply, uint32_t flags = 0);    
};


};
#endif

