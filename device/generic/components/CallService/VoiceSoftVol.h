#ifndef _VOICE_SOFTVOL_H
#define _VOICE_SOFTVOL_H

#include <utils/Singleton.h>
#include <cutils/compiler.h>
#include <alsa/asoundlib.h>

namespace android {


//
//only SND_PCM_FORMAT_S16_LE support
//
class VoiceSoftVolume
{
	public:
  
	   	VoiceSoftVolume();
	   	VoiceSoftVolume(unsigned int _channels,unsigned int _resolution,double _min_dB,double _max_dB);		
		~VoiceSoftVolume();


		
		snd_pcm_uframes_t write_areas(char* source,
						snd_pcm_uframes_t offset,
						snd_pcm_uframes_t size,
						char* target,
						snd_pcm_uframes_t target_offset,
						snd_pcm_uframes_t *target_sizep);

		int set_volume(unsigned int volume);
		int get_volume_max();
		int get_volume_min();
		int get_volume_0dB();
		

		int initCheck();

		snd_pcm_uframes_t write_areas(const snd_pcm_channel_area_t *areas,
						snd_pcm_uframes_t offset,
						snd_pcm_uframes_t size,
						const snd_pcm_channel_area_t *slave_areas,
						snd_pcm_uframes_t slave_offset,
						snd_pcm_uframes_t *slave_sizep);
		snd_pcm_uframes_t read_areas(const snd_pcm_channel_area_t *areas,
			   snd_pcm_uframes_t offset,
			   snd_pcm_uframes_t size,
			   const snd_pcm_channel_area_t *slave_areas,
			   snd_pcm_uframes_t slave_offset,
			   snd_pcm_uframes_t *slave_sizep);		

		void dump(void);

	public:
		snd_pcm_format_t sformat;
		unsigned int cchannels;
		unsigned int cur_vol[2];
		unsigned int max_val;	  /* max index */
		unsigned int zero_dB_val; /* index at 0 dB */
		double min_dB;
		double max_dB;
		unsigned int *dB_value;

	private:

		//intermediates
		unsigned int resolution;

		
};

//facility of often used voicecall volume control
class VoiceCallVolume:public Singleton<VoiceCallVolume>,public VoiceSoftVolume
{
	private:
		static const unsigned int _channels = 1;
		static const unsigned int _resolution = 256;
		static const double _min_dB=-50;
		static const double _max_dB=50;
	public:
		VoiceCallVolume();
};

#define VOICECALL_DATA(d,s,s_f,p_d_f) \
	VoiceCallVolume::getInstance().write_areas(s,0,s_f,d,0,p_d_f)
	

#define VOICECALL_VOL(v) \
	VoiceCallVolume::getInstance().set_volume(v)

#define VOICECALL_VOLMAX() \
	VoiceCallVolume::getInstance().get_volume_max()

#define VOICECALL_VOLMIN() \
	VoiceCallVolume::getInstance().get_volume_min()

#define VOICECALL_VOL0DB() \
	VoiceCallVolume::getInstance().get_volume_0dB()

}


#endif

