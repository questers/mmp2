#ifndef _VOICE_MODEM_H_
#define _VOICE_MODEM_H_
namespace android{

class VoiceModem{
	public:
		VoiceModem();
		virtual ~VoiceModem();

		virtual int start_call(void)=0;
		virtual int end_call(void)=0;
		virtual void set_volume(float vol)=0;
		virtual void set_path(int path)=0;
		
};

}
#endif 

