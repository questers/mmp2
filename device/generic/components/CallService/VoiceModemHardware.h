#ifndef _VOICE_MODEM_HARDWARE_H_
#define _VOICE_MODEM_HARDWARE_H_
#include <stdint.h>
#include <sys/types.h>
#include <semaphore.h>
#include <termios.h>
#include <utils/threads.h>
#include <alsa/asoundlib.h>
#include <media/AudioSystem.h>
#include <media/AudioTrack.h>
#include <media/AudioRecord.h>

#include "VoiceModem.h"

//forward declaration 
class VoiceCodec;

namespace android
{	
	
	class VoiceModemHardware: public VoiceModem
	{
		public:
			enum
			{			
				PCM_FMT_RAW = 1,
				PCM_FMT_ULAW = 2,
				PCM_FMT_ALAW = 3,
				PCM_FMT_MAX = PCM_FMT_ALAW,
				PCM_FMT_DEFAULT = PCM_FMT_RAW,
			};
			enum
			{
				sINIT,
				sSTARTING,
				sSTARTED,
				sSTOPPING,
				sSTOPPED
			};
			typedef struct modem_status
			{
				unsigned long attached:1;
				unsigned long simulated:1;
				unsigned long incall:1;
				unsigned long reserved:1;
				unsigned long trackstatus:4;
				unsigned long recordstatus:4;
				unsigned long hw_ready:1;
			}modem_status;
		
		public:
			VoiceModemHardware();
			virtual ~VoiceModemHardware();


			virtual int start_call(void);
			virtual int end_call(void);

			virtual void set_volume(float vol);

			virtual void set_path(int path);
			
		private:
			static void vc_track_callback(int event, void* user, void *info);
			static void vc_record_callback(int event,void* user,void * info);
			int attach(void);
			int detach(void);
			int init_hw_modem(int _baudrate);
			int deinit_hw_modem(void);
			void process_record_event(int event, void *info);
			void process_track_event(int event,void *info);

			int read_from_modem(char* buffer,int frames);
			int write_to_modem(char* buffer,int frames);

		protected:
			void reader_loop(void);
			void dispatch_loop(void);

			class ModemReaderThread : public Thread 
			{
			   private:
					ModemReaderThread();
					Mutex           mLock;
					Condition       mWaitWorkCV;
					Condition       mStopped;
					VoiceModemHardware* modem;
					volatile bool mActive;
					status_t  mStartStatus;
				public:
				   ModemReaderThread(VoiceModemHardware* vm):Thread(false),modem(vm) { }
				   virtual ~ModemReaderThread(){}
				   virtual void onFirstRef();
				   virtual bool threadLoop(); 
					virtual status_t readyToRun();

					int    start(void);
					void   stop(void);
					void   exit(void);
			};  
   
			class ModemDispatchThread : public Thread 
			{
				private:
					ModemDispatchThread();
						Mutex           mLock;
						Condition       mWaitWorkCV;
						Condition       mStopped;
						VoiceModemHardware* modem;
						volatile bool mActive;
						status_t  mStartStatus;
					public:
					ModemDispatchThread(VoiceModemHardware* vm):Thread(false),modem(vm) { }
					virtual ~ModemDispatchThread(){}
					virtual void onFirstRef();
					virtual bool threadLoop(); 
					virtual status_t readyToRun();

					int    start(void);
					void   stop(void);
					void   exit(void);
			};
   		protected:
			sp<ModemReaderThread>  mReaderThread;;              
            sp<ModemDispatchThread>  mDispatchThread;
			int latency_us;

		private:		
			int fd;
			struct termios oldtio,newtio;
			
			AudioTrack* mpVoiceCallTrack;
			AudioRecord* mpVoiceCallRecord;
			unsigned char read_buf[320];
			unsigned char write_buf[320];
			
			modem_status status;
			Mutex mLock;
			Mutex mTrackLock,mRecordLock;
			Condition mWaitTrackCond,mWaitRecordCond;
			

			VoiceCodec* codec;
			int voice_fmt;
			
			int rw_period_ms;
			//modem downstream<->track			
			int min_trackframes;
			int min_modemdownstreamframes;
			int max_cached_trackframes,cached_trackframes,consumed_trackframes;
			unsigned char* cached_trackbuffer;
			unsigned char* softvol_trackbuffer; //same size as cache track buffer ,for softvol plugin
			

			//modem upstream<->record
			int init_modemupstream_dropframes;
			int min_modemupstreamframes;
			int min_recordframes;
			int max_cached_recordframes,cached_recordframes;
			unsigned char* cached_recordbuffer;
			int framesize;
			FILE *downlinkDump,*uplinkDump;

			bool dlink_disabled;
			bool ulink_disabled;


			int voicePath;
			float volume;

			bool passive_mode;
	};

}

#endif
