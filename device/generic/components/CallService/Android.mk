LOCAL_PATH:= $(call my-dir)

#
# libvoicecall
#
include $(CLEAR_VARS)


LOCAL_SRC_FILES := \
	VoiceSoftVol.cpp \
	VoiceGenerator.cpp \
	VoiceLog.cpp \
	IVoiceCall.cpp \
	IVoiceCallListener.cpp \
	VoiceCodec.cpp \
	VoiceCodecPCM.cpp \
	VoiceCodecUlaw.cpp \
	VoiceModem.cpp \
	VoiceModemStub.cpp \
	VoiceModemHardware.cpp \
	VoiceCallServer.cpp


LOCAL_C_INCLUDES += \
        external/alsa/alsa-lib/include
#alsa lib required flags	
LOCAL_CFLAGS += -DPIC -D_POSIX_SOURCE

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
    libbinder \
    libmedia \
    libasound 

    

LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := libvoicecall

include $(BUILD_SHARED_LIBRARY)

#
# VoiceCallServer
#
include $(CLEAR_VARS)


LOCAL_SRC_FILES := \
	Main.cpp 


LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
    libbinder \
    libvoicecall
    

LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := VoiceCallServer

include $(BUILD_EXECUTABLE)


#
# VoiceCallTest
#
include $(CLEAR_VARS)


LOCAL_SRC_FILES := \
	vc.cpp 

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
	libbinder \
	libmedia \
	libvoicecall
	
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := tests
LOCAL_MODULE := vc

include $(BUILD_EXECUTABLE)


#
#vctest
#
include $(CLEAR_VARS)


LOCAL_SRC_FILES := \
	test.cpp 

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
	libbinder \
	libmedia \
	libvoicecall \
    libasound 


LOCAL_C_INCLUDES += \
        external/alsa/alsa-lib/include
#alsa lib required flags	
LOCAL_CFLAGS += -DPIC -D_POSIX_SOURCE
	
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := tests
LOCAL_MODULE := vctest

include $(BUILD_EXECUTABLE)


