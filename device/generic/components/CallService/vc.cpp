#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <binder/Parcel.h>
#include <binder/ProcessState.h>
#include <binder/IServiceManager.h>
#include <media/AudioSystem.h>
#include <media/AudioTrack.h>
#include <media/AudioRecord.h>
#include <media/mediarecorder.h>
#include <IVoiceCall.h>
using namespace android;

#define dprintf(...) fprintf(stderr,__VA_ARGS__)

static void usage(char *progname) {
    dprintf( "Usage: %s [cmd] [args]\n", progname);
    dprintf( "\tcmd avaiable\n");
    dprintf( "\tstart  start voice call \n");
    dprintf( "\tend    end voice call\n");
    dprintf( "\trestart restart voicecall\n");
    dprintf( "\tvol <value>  set voice call volume of speaker\n");	
	
    exit(1);
}


int main(int argc,char** argv)
{
    if(argc<2)
        usage(argv[0]);
    sp<IVoiceCall> mVoiceCall;
    sp<IServiceManager> sm = defaultServiceManager();
    if (sm == NULL) {
        dprintf("Couldn't get default ServiceManager\n");
        return -1;
    }
    
    mVoiceCall = interface_cast<IVoiceCall>(sm->getService(String16("media.voicecallserver")));
    if (mVoiceCall == NULL) {
        dprintf("Couldn't get connection to VoiceCall Server\n");
        return -2;
    }
    if(argc>1)
    {
        if(!strcmp(argv[1],"start"))
        {
            dprintf("start call ... result=%d\n",mVoiceCall->startCall());
        }
        else if(!strcmp(argv[1],"end"))
        {
            dprintf("end call ... result=%d\n",mVoiceCall->endCall());
        }
        else if(!strcmp(argv[1],"start"))
        {
            dprintf("restart call ... result=%d\n",mVoiceCall->restartCall());
        }
        else if(!strcmp(argv[1],"vol"))
        {
            float vol;
            if(argc<3)
            {
                usage(argv[0]);
            }
            vol = atof(argv[2]);
            dprintf("restart call ... result=%d\n",mVoiceCall->setVolume(vol));
        }
        
    }

    return 0;
}
