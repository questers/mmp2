#ifndef _PCM_H_
#define _PCM_H_
#include "VoiceCodec.h"


namespace android
{

	class VoiceCodecPCM: public VoiceCodec
	{
		public:
			VoiceCodecPCM();
			virtual ~VoiceCodecPCM();
			int initCheck(void);
			int framesize();
			int   setParameters(const String8& keyValuePairs);
			int decode(const unsigned char* src,const int samples,unsigned char* dst);
			int encode(const unsigned char *src,const int samples,unsigned char* dst);
			
	};
}

#endif

