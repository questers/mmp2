#ifndef _VOICE_MODEM_STUB_H
#define _VOICE_MODEM_STUB_H

#include <utils/threads.h>
#include "VoiceModem.h"

namespace android
{	

//forward declaration 
class VoiceCodec;
class AudioTrack;
class AudioRecord;

class VoiceModemStub:public VoiceModem
{
	private:		
		enum
		{
			sINIT,
			sSTARTING,
			sSTARTED,
			sSTOPPING,
			sSTOPPED
		};
		typedef struct modem_status
		{
			unsigned long incall:1;
			unsigned long trackstatus:4;
			unsigned long recordstatus:4;
		}modem_status;
		
    public:
		VoiceModemStub();
		virtual ~VoiceModemStub();
    	virtual int start_call(void);
			virtual int end_call(void);
			virtual void set_volume(float vol);
			virtual void set_path(int path);
			
	private:
		static void vc_track_callback(int event, void* user, void *info);
		static void vc_record_callback(int event,void* user,void * info);

	private:				
		AudioTrack* mpVoiceCallTrack;
		AudioRecord* mpVoiceCallRecord;
		unsigned char read_buf[320];
		unsigned char write_buf[320];

		modem_status status;
		Mutex mLock;
		Mutex mTrackLock,mRecordLock;
		Condition mWaitTrackCond,mWaitRecordCond;
		

		VoiceCodec* codec;
		
		int min_trackframes;
		int max_cached_trackframes,cached_trackframes;
		unsigned char* cached_trackbuffer;
		int min_recordframes;
		int max_cached_recordframes,cached_recordframes;
		unsigned char* cached_recordbuffer;
		
		int rw_period_ms;
		int framesize;
};

}
#endif

