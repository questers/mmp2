#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <binder/Parcel.h>
#include <binder/ProcessState.h>
#include <binder/IServiceManager.h>
#include <media/AudioSystem.h>
#include <media/AudioTrack.h>
#include <media/AudioRecord.h>
#include <media/mediarecorder.h>
#include <IVoiceCall.h>
#include <VoiceGenerator.h>
#include <VoiceSoftVol.h>

using namespace android;

#define dprintf(...) fprintf(stderr,__VA_ARGS__)

static void usage(char *progname) {
    dprintf( "Usage: %s [cmd] [args]\n", progname);
    dprintf( "\tcmd avaiable\n");
    dprintf( "\tt1  run audio record \n");	
    dprintf( "\tt2  run audio track \n");	
    dprintf( "\tt3  run audio track with sine wave\n");	
    dprintf( "\tt4  run audio track with sine wave and soft volume control\n");	
	
    exit(1);
}

void audio_record_callback(int event,void* user,void * info)
{
    static int min_modemframes=160;
    static int cached_frames=0;
	if(event!=AudioRecord::EVENT_MORE_DATA) return;
    AudioRecord::Buffer *buffer = static_cast<AudioRecord::Buffer *>(info); 
    unsigned int receivedFrames = buffer->frameCount;
    if(receivedFrames)
    {
        dprintf("got %d frames\n",receivedFrames);
        cached_frames+=receivedFrames;
        if(cached_frames>=min_modemframes)
        {
            cached_frames-=min_modemframes;
            dprintf("min modem frame count reached,cached[%d]\n",cached_frames);
        }
    }

    
}

static int testcase_1(void)
{    
    int min_recordframes;
    AudioRecord* mpVoiceCallRecord;
    AudioRecord::getMinFrameCount(&min_recordframes,8000,AudioSystem::PCM_16_BIT,1);
    min_recordframes = 160;
    dprintf("min_recordframes=%d\n",min_recordframes);
    
	mpVoiceCallRecord = new AudioRecord(AUDIO_SOURCE_DEFAULT,//AUDIO_SOURCE_VOICE_CALL,
			8000,
			AudioSystem::PCM_16_BIT,
			AudioSystem::CHANNEL_IN_MONO,
			min_recordframes*4,//ensure is times of vc packet, 8 times of cached frames is enough
			0,
			audio_record_callback);/*,
			NULL,
			min_recordframes//20ms voicecall data			
			);*/
	if(!mpVoiceCallRecord) goto start_failure;
	if (mpVoiceCallRecord->initCheck() != NO_ERROR) goto start_failure;

   // mpVoiceCallRecord->setPositionUpdatePeriod(min_recordframes);

    mpVoiceCallRecord->start();

    
    while(1) usleep(100000);
    return 0;
start_failure:
    if(mpVoiceCallRecord)
        delete mpVoiceCallRecord;   
    return -1;
    
}
static void audio_track_callback(int event,void* user,void * info)
{

	if(event!=AudioTrack::EVENT_MORE_DATA) return;
    AudioTrack::Buffer *buffer = static_cast<AudioTrack::Buffer *>(info);	
    unsigned int requestFrames = buffer->frameCount;

	dprintf("audio track event %d,frames[%d]\n",event,requestFrames);	
    buffer->size=0;

}

static int testcase_2(void)
{    
    int min_trackframes;
    AudioTrack* mpVoiceCallTrack;
    AudioTrack::getMinFrameCount(&min_trackframes,AudioSystem::VOICE_CALL,8000);
    dprintf("min_trackframes=%d\n",min_trackframes);
    
	mpVoiceCallTrack = new AudioTrack(AudioSystem::VOICE_CALL,
			  8000,
			  AudioSystem::PCM_16_BIT,
			  AudioSystem::CHANNEL_OUT_MONO,
			  0,//ensure is times of vc packet, 8 times of cached frames is enough
			  0,
			  audio_track_callback,
			  NULL,
			  0//20ms voicecall data
			  );
	if(!mpVoiceCallTrack) goto start_failure;
		
	if (mpVoiceCallTrack->initCheck() != NO_ERROR) goto start_failure;

	mpVoiceCallTrack->setVolume(1.0, 1.0);	

    mpVoiceCallTrack->start();

    
    while(1) usleep(100000);
    return 0;
start_failure:
    if(mpVoiceCallTrack)
        delete mpVoiceCallTrack;   
    return -1;
    
}


static void t3_audio_track_callback(int event,void* user,void * info)
{

	if(event!=AudioTrack::EVENT_MORE_DATA) return;
    AudioTrack::Buffer *buffer = static_cast<AudioTrack::Buffer *>(info);	
    unsigned int requestFrames = buffer->frameCount;

	dprintf("audio track event %d,frames[%d]\n",event,requestFrames);	

    buffer->size = SINE_READ((unsigned char*)buffer->raw,buffer->size);

        
}


static int testcase_3(void)
{
    int min_trackframes;
    AudioTrack* mpVoiceCallTrack;
    AudioTrack::getMinFrameCount(&min_trackframes,AudioSystem::VOICE_CALL,8000);
    dprintf("min_trackframes=%d\n",min_trackframes);
    
	mpVoiceCallTrack = new AudioTrack(AudioSystem::VOICE_CALL,
			  8000,
			  AudioSystem::PCM_16_BIT,
			  AudioSystem::CHANNEL_OUT_MONO,
			  min_trackframes*4,//ensure is times of vc packet, 8 times of cached frames is enough
			  0,
			  t3_audio_track_callback,
			  NULL,
			  min_trackframes
			  );
	if(!mpVoiceCallTrack) goto start_failure;
		
	if (mpVoiceCallTrack->initCheck() != NO_ERROR) goto start_failure;

	mpVoiceCallTrack->setVolume(1.0, 1.0);	

    mpVoiceCallTrack->start();
    while(1) usleep(100000);
    return 0;

start_failure:
    if(mpVoiceCallTrack)
        delete mpVoiceCallTrack;   
    return -1;
    
    
}

static int changevol=0;

static void t4_audio_track_callback(int event,void* user,void * info)
{
    char src[320];
	if(event!=AudioTrack::EVENT_MORE_DATA) return;
    AudioTrack::Buffer *buffer = static_cast<AudioTrack::Buffer *>(info);	
    unsigned int requestFrames = buffer->frameCount;
    size_t size = SINE_READ((unsigned char*)src,320);
        //SINE_READ((unsigned char*)buffer->raw,buffer->size);

    requestFrames = VOICECALL_DATA((char*)buffer->raw,src,(snd_pcm_uframes_t)requestFrames,(snd_pcm_uframes_t*)&requestFrames);
    buffer->size = requestFrames*2;//frame size of 2bytes for mono S16 voice


     
     changevol++;
     if(changevol%1000<500)
     {     
        dprintf("vol=1\n");
         VOICECALL_VOL(VOICECALL_VOL0DB());
     }
     else
     {
         dprintf("vol=max\n");
         VOICECALL_VOL(VOICECALL_VOLMAX());
        
     }
    
}

static int testcase_4(int vol)
{
    int min_trackframes;
    AudioTrack* mpVoiceCallTrack;
    AudioTrack::getMinFrameCount(&min_trackframes,AudioSystem::VOICE_CALL,8000);
    dprintf("min_trackframes=%d vol=%d\n",min_trackframes,vol);
    
    VOICECALL_VOL(vol);
    
	mpVoiceCallTrack = new AudioTrack(AudioSystem::VOICE_CALL,
			  8000,
			  AudioSystem::PCM_16_BIT,
			  AudioSystem::CHANNEL_OUT_MONO,
			  min_trackframes*4,//ensure is times of vc packet, 8 times of cached frames is enough
			  0,
			  t4_audio_track_callback,
			  NULL,
			  min_trackframes
			  );
	if(!mpVoiceCallTrack) goto start_failure;
		
	if (mpVoiceCallTrack->initCheck() != NO_ERROR) goto start_failure;

	mpVoiceCallTrack->setVolume(1.0, 1.0);	

    mpVoiceCallTrack->start();
    while(1) usleep(100000);
    return 0;

start_failure:
    if(mpVoiceCallTrack)
        delete mpVoiceCallTrack;   
    return -1;
    
    
}

int main(int argc,char** argv)
{
    if(argc<2)
        usage(argv[0]);
    if(argc>1)
    {
        if(!strcmp(argv[1],"t1"))
        {
            dprintf("run test case 1 ... \n");
            dprintf("\n\nRESULT=%s\n\n",testcase_1()?"FAIL":"PASS");
        }
        else if(!strcmp(argv[1],"t2"))
        {
            dprintf("run test case 2 ... \n");
            dprintf("\n\nRESULT=%s\n\n",testcase_2()?"FAIL":"PASS");
        }
        if(!strcmp(argv[1],"t3"))
        {
            dprintf("run test case 3 ... \n");
            dprintf("\n\nRESULT=%s\n\n",testcase_3()?"FAIL":"PASS");
        }
        if(!strcmp(argv[1],"t4"))
        {
            int vol=10;
            dprintf("run test case 4 ... \n");
            if(argc>2)
                vol = atoi(argv[2]);
            dprintf("\n\nRESULT=%s\n\n",testcase_4(vol)?"FAIL":"PASS");
        }
        
        
    }

    return 0;
}
