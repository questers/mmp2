#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/inotify.h>
#include <hardware_legacy/power.h>
#include <cutils/properties.h>
#include <libusb/libusb.h>
#include "ril-core.h"
#include "ril-handler.h"
#include "atchannel.h"

#define ANDROID_WAKE_LOCK_NAME "ril_hardware"


#define MAX_PORTS 10

struct port_offset
{
	int servc_port;
	int modem_port;
	int voice_port;
	int diags_port;
	int vidtm_port;
};

static struct port_offset port_offset_map[] =
{
	//0,1,2,x,x
	{0,1,2,-1,-1},

	//1,3,2,x,x
	{1,3,2,-1,-1},

	//0,3,1,x,x
	{0,3,1,-1,-1},

	//1,0,2,x,x
	{1,0,2,-1,-1},

	//2,0,4,x,x
	{2,0,4,-1,-1},

	//0,1,3,x,x
	{0,1,3,-1,-1},
	//new added must be start here
	
};

static char* port_names[] = 
{
	"/dev/ttyUSB0",
	"/dev/ttyUSB1",
	"/dev/ttyUSB2",
	"/dev/ttyUSB3",
	"/dev/ttyUSB4",
	"/dev/ttyUSB5",
	"/dev/ttyUSB6",
	"/dev/ttyUSB7",
	"/dev/ttyUSB8",
	"/dev/ttyUSB9",		
};

static ST_RIL_HARDWARE hw_mapping[] = 
{
	//model,		prefer net, vid,		pid,		service port,		modem port, 	voice port, 	voice format
	{
		.model_name		= "MF210",
		.model 			= kRIL_HW_MF210,
		.prefer_net 	= kPREFER_NETWORK_TYPE_WCDMA,
		.vid 			= 0x19d2,
		.pid			= 0x2003,
		.voice_format	= "ulaw",
		.voice_rw_period= "20",
		.private		= &port_offset_map[1],
	},
	{
		.model_name 	= "AD3812",
		.model			= kRIL_HW_AD3812,
		.prefer_net 	= kPREFER_NETWORK_TYPE_WCDMA,
		.vid			= 0x19d2,
		.pid			= 0xffeb,
		.voice_format	= "raw",
		.voice_rw_period= "20",
		.private		= &port_offset_map[2],
	},
	{
		.model_name 	= "MC2716",
		.model			= kRIL_HW_MC2716,
		.prefer_net 	= kPREFER_NETWORK_TYPE_CDMA_EVDV,
		.vid			= 0x19d2,
		.pid			= 0xffed,
		.voice_format	= "raw",
		.voice_rw_period= "20",
		.private		= &port_offset_map[3],
	},

	{
		.model_name 	= "M305",
		.model			= kRIL_HW_M305,
		.prefer_net 	= kPREFER_NETWORK_TYPE_TD_SCDMA,
		.vid			= 0x19d2,
		.pid			= 0x1303,
		.voice_format	= "raw",
		.voice_rw_period= "20",
		.private		= &port_offset_map[4],
	},

	{
		.model_name 	= "MC8630",
		.model			= kRIL_HW_MC8630,
		.prefer_net 	= kPREFER_NETWORK_TYPE_CDMA_EVDV,
		.vid			= 0x19d2,
		.pid			= 0xfffe,
		.voice_format	= "raw",
		.voice_rw_period= "20",
		.private		= &port_offset_map[3],
	},
	
	#if 0
	//MF210
	{kRIL_HW_MF210,kPREFER_NETWORK_TYPE_WCDMA,0x19d2,0x2003,
		"/dev/ttyUSB1","/dev/ttyUSB3","/dev/ttyUSB2","ulaw","20",},

	//AD3812
	{kRIL_HW_AD3812,kPREFER_NETWORK_TYPE_WCDMA,0x19d2,0xffeb,
	"/dev/ttyUSB0","/dev/ttyUSB3","/dev/ttyUSB1","raw","20"},

	//MC2716
	{kRIL_HW_MC2716,kPREFER_NETWORK_TYPE_CDMA_EVDV,0x19d2,0xffed,
	"/dev/ttyUSB1","/dev/ttyUSB0","/dev/ttyUSB2","raw","20"},

	//M305
	{kRIL_HW_M305,kPREFER_NETWORK_TYPE_TD_SCDMA,0x19d2,0x1303,
		"/dev/ttyUSB2","/dev/ttyUSB0","/dev/ttyUSB4","raw","20"},

	//MC8630
	{kRIL_HW_MC8630,kPREFER_NETWORK_TYPE_CDMA_EVDV,0x19d2,0xfffe,
		"/dev/ttyUSB1","/dev/ttyUSB0","/dev/ttyUSB2","raw","20"},
	#endif
	{
		.model_name		= "unknown",
		.model 			= 0,
	},
	
};

 
/*
 * return port numbers
*/
static int find_port_names(const char* pattern,char* names[])
{
	struct stat port_stat;
	char file[256];
	const char* default_pattern="/dev/ttyUSB";
	int base,i;
	int err;
	if(NULL==pattern)
		pattern = default_pattern;
	base=i=0;	
	do
	{
		sprintf(file,"%s%d",pattern,base);
		err=stat(file,&port_stat);
		if(!err)
		{
			names[i++]=port_names[base];
		}		
		base++;
		
	}while(base<MAX_PORTS);

	//reach here
	return i;
}

static int print_usbdev_config(libusb_device* dev){
	
	struct libusb_config_descriptor *config;
	int r;
	int i,j,numintf;

	r = libusb_get_active_config_descriptor(dev, &config);
	if (r < 0) {
		ERROR(
			"could not retrieve active config descriptor");
		return LIBUSB_ERROR_OTHER;
	}
	DBG("busnumber:%d",libusb_get_bus_number(dev));
	DBG("config->bNumInterfaces=%d",config->bNumInterfaces);
	numintf = config->bNumInterfaces;
	for(i=0;i<numintf;i++){
		for(j=0;j<config->interface[i].num_altsetting;j++){
			DBG("config interface number=%d",config->interface[i].altsetting[j].bInterfaceNumber);
		}
		
	}

	libusb_free_config_descriptor(config);

	return 0;
}
void rilhw_found(PST_RIL_HARDWARE *found)
{
    DBG("checking RIL hardware ...");
	PST_RIL_HARDWARE map = &hw_mapping[0];
	char* port_name_arry[10];

    //sanity checking    
	libusb_device **devs;
	int r;
	ssize_t cnt;

	*found = NULL;

	r = libusb_init(NULL);
	if (r < 0)
	{	
		WARN("libusb init failed[%d]",r);
		return ;
	}
		cnt = libusb_get_device_list(NULL, &devs);
		if (cnt < 0)
		{
			WARN("getusb device list failed");
		}
		else
		{
		
			libusb_device *dev;
			int i = 0,j;
			
			while ((dev = devs[i++]) != NULL) {
				struct libusb_device_descriptor desc;
				int r = libusb_get_device_descriptor(dev, &desc);
				if (r < 0) {
					ERROR("failed to get device descriptor");
					break;
				}

				for(j=0;;j++)
				{
					if(!map[j].vid) break;
					if((desc.idVendor == map[j].vid)&&
						(desc.idProduct== map[j].pid))
					{
						*found = &map[j];
						

						print_usbdev_config(dev);


						
						goto finished;
					}
				}
				DBG("usb device %04x:%04x found but not RIL HW\n",
					desc.idVendor, desc.idProduct);
			}
			
		}


finished:		
	libusb_free_device_list(devs, 1);
	
	libusb_exit(NULL);


	//find out the port names of found device
	if(NULL!=*found)
	{
		PST_RIL_HARDWARE hw = *found;
		struct port_offset* offset;
		int ports = find_port_names(NULL,port_name_arry);
		if(0==ports)
		{
			WARN("found ril hardware %s but no valid port found\n",hw->model_name);
			*found=NULL;
			return;
		}
		
		//assign port name
		offset = (struct port_offset*)hw->private;
		hw->service_port = port_name_arry[offset->servc_port];
		hw->modem_port   = port_name_arry[offset->modem_port];
		hw->voice_port	 = port_name_arry[offset->voice_port];
		if(!hw->service_port||!hw->modem_port){
			ERROR("modem port not available\n");
			*found=NULL;
			return;
		}
			
		/* BatteryService.java uses ril.hw.model to turn off 3G modem when battery capacity is low */
		property_set("ril.hw.model", hw->model_name);
		property_set("ril.vmodem.target",hw->voice_port);
		property_set("ril.vmodem.pcm",hw->voice_format); 
		property_set("ril.vmodem.period",hw->voice_rw_period);
		
		switch(hw->model)
		{
			case kRIL_HW_MC2716:
				property_set("ril.vmodem.dlink.volume","160");
				//sms_mode = AT_SMS_MODE_TEXT;
				break;
			case kRIL_HW_AD3812:
			//roperty_set("ril.vmodem.volume",180);
			//reak;
			case kRIL_HW_MF210:
			default:
				property_set("ril.vmodem.dlink.volume","200");
				break;					
		}
			
		DBG("found RILHW %s [%s],service[%s],modem[%s],voice[%s]\n",hw->model_name,
				(kPREFER_NETWORK_TYPE_WCDMA==hw->prefer_net)?"WCDMA":
				(kPREFER_NETWORK_TYPE_CDMA_EVDV==hw->prefer_net)?"CDMA-EVDV":
				(kPREFER_NETWORK_TYPE_TD_SCDMA==hw->prefer_net)?"TD-SCDMA":"unknown",
				hw->service_port?hw->service_port:"null",
				hw->modem_port?hw->modem_port:"null",
				hw->voice_port?hw->voice_port:"null");
				

		//enable selective suspend
		rilhw_autosuspend(NULL,1);
		
	}
	
		
}

static int readIntFromFile( const char* file, int* pResult )
{
	int fd = -1;
	fd = open( file, O_RDONLY );
	if( fd >= 0 ){
		char buf[20];
		int rlt = read( fd, buf, sizeof(buf) );
		if( rlt > 0 ){
			buf[rlt] = '\0';
			*pResult = atoi(buf);
			return 0;
		}
	}
	return -1;
}

int rilhw_power(PST_RIL_HARDWARE hardware,int on)
{
	int ret=-1;
	
	int fd = open( "/sys/devices/platform/usb_modem/usb_modem", O_WRONLY );
	if( fd >= 0 ){
		char buf[48];
		int len;
		len=sprintf(buf,"state %s 100\n",(on>0)?"on":"off");
		
		len = write( fd, buf, strlen(buf));
		if( len > 0 ){
			ret=0;
		}
	}
	return ret;
	
}
int rilhw_autosuspend(PST_RIL_HARDWARE hardware,int enable){					
	char property[PROPERTY_VALUE_MAX];
	int autosuspend_enable=0;
	int autosuspend_timeout=5;
	property_get("persist.ril.autosuspend", property, "1");	
	autosuspend_enable = atoi(property);
	property_get("persist.ril.autosuspendtimeout", property, "5");	
	autosuspend_timeout = atoi(property);
	if(autosuspend_timeout<0||autosuspend_timeout>120)
		autosuspend_timeout=5;
	if(autosuspend_enable)	{
		char* command;
		asprintf(&command, "modem_rt:%s %d",(enable>0)?"auto":"on",(enable>0)?autosuspend_timeout:-1);
		property_set("ctl.start",command);
		free(command);
	}else {
		WARN("persist.ril.autosuspend not enabled on rilhw autosuspend");
	}
	return enable;
}


static int rilhw_link_change(void)
{
	int fd = open( "/sys/devices/platform/usb_modem/link_state", O_RDWR );
	int ret=0;
	if( fd >= 0 )
	{
		char buf[20];
		int rlt = read( fd, buf, sizeof(buf) );
		if( rlt > 0 ){
			buf[rlt] = '\0';
			ret = atoi(buf);
		}
		if(ret)
		{
			write(fd,"0\n",strlen("0\n"));
		}
	}
	return ret;
}

int rilhw_notify_screen_state(int state)
{
	int fd = open( "/sys/devices/platform/usb_modem/screen_state", O_WRONLY );
	if( fd >= 0 ){
		char buf[48];
		int len;
		len=sprintf(buf,"%d\n",state);
		len = write( fd, buf, strlen(buf));
		if( len > 0 ){
			return 0;
		}
	}

	return -1;
}


static int rilhw_wakelock_count=0;

int rilhw_acquire_wakelock()
{
	if(!rilhw_wakelock_count)
    {
    	DBG("%s\n",__func__);
    	acquire_wake_lock(PARTIAL_WAKE_LOCK, ANDROID_WAKE_LOCK_NAME);
		rilhw_wakelock_count = 1;
	}
	return 0;
}
int rilhw_release_wakelock()
{
	if(rilhw_wakelock_count)
	{
    	DBG("%s\n",__func__);
		release_wake_lock(ANDROID_WAKE_LOCK_NAME);
		rilhw_wakelock_count=0;
	}
	return 0;
}

struct wd_entry
{
	int wd;
	int (*on_change)(unsigned int event);
	
};

#define MAX_WD_NUM 16
struct hardware_monitor_param
{
	int fd;//notify fd
	int wd_count;
	struct wd_entry entries[MAX_WD_NUM]; //max 16 wd
};

static int link_state_changed(unsigned int event)
{
	int link_state = rilhw_link_change();
	DBG("link state changed->%d\n",link_state);
	pdp_check();
	if(sState!=radio_state_ready)
	{
		WARN("radio state not ready,ignore link state change\n");
		return 0;
	}
	switch(link_state)
	{
		case 2:
			{
				DBG("\n\nradio link changed\n\n");
		        RIL_onUnsolicitedResponse (
		            RIL_UNSOL_RESPONSE_CALL_STATE_CHANGED,
		            NULL, 0);
				CheckSMSStorage();
			}
			break;
		case 1:
			{
				DBG("link state changed\n");
				if(!rilhw_wakelock_count)
				{
					RIL_onUnsolicitedResponse (
						RIL_UNSOL_RESPONSE_CALL_STATE_CHANGED,
						NULL, 0);
					CheckSMSStorage();
				}
			}
			break;
		
	}

	return 0;

}
static void *hardware_monitor(void* param)
{
#define EVENT_NUM 16
#define MAX_BUF_SIZE 1024		

	struct hardware_monitor_param* monitor_param = param;
	#if 0
	int i;
	char buffer[1024];
	char* offset = NULL;
	struct inotify_event * event;
	struct wd_entry* entry;
	int len, tmp_len;	

	INFO("%s started\n",__func__);
	while((len = read(monitor_param->fd, buffer, MAX_BUF_SIZE))>0) 
	{
		offset = buffer;
		event = (struct inotify_event *)buffer;
		while (((char *)event - buffer) < len) 
		{
			entry = NULL;
			for (i=0; i<MAX_WD_NUM; i++) {
				if (event->wd != monitor_param->entries[i].wd) continue;
				else
				{
					entry = &monitor_param->entries[i];
					break;
				}
			}
			if(entry&&entry->on_change)			
				entry->on_change(event->mask);
			tmp_len = sizeof(struct inotify_event) + event->len;
			event = (struct inotify_event *)(offset + tmp_len); 
			offset += tmp_len;
		}
	
	}
	
	#else
	fd_set mfds,fds;
	int nfds;
	int i,ret;
	struct timeval to={2,0};
	while(1)    
	{	    
		FD_ZERO(&mfds);
		nfds=0;
		for (i = 0; i < monitor_param->wd_count; i++)	
		{
			if (monitor_param->entries[i].wd < 0) continue;
			FD_SET(monitor_param->entries[i].wd, &mfds);	
			if (monitor_param->entries[i].wd >= nfds)		
				nfds = monitor_param->entries[i].wd+1;	
		}
		
		// make local copy of read fd_set    	
		memcpy(&fds, &mfds, sizeof(fd_set)); 
		/* call the select */    	
		if ((ret = select(nfds, &fds, NULL, NULL, &to)) < 0)    
		{            
			ERROR(" hardware_monitor select fail.\n");   
			goto bail;        
		}	
		for (i = 0; i < monitor_param->wd_count; i++)	
		{
			if (FD_ISSET(monitor_param->entries[i].wd,&fds))
			{
				if(monitor_param->entries[i].on_change)
					monitor_param->entries[i].on_change(0);
			}
		}
	}
	#endif

bail:
	return NULL;
}

int rilhw_init(void)
{
	static pthread_t s_tid_hw=-1;
	static struct hardware_monitor_param param;

	if(-1==s_tid_hw)
	{
		int ret,fd,wd;
		int i;
		pthread_attr_t attr;		
		pthread_attr_init (&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

		memset(&param,0,sizeof(struct hardware_monitor_param));
		//use select/poll API to monitor sysfs fs since inotify does not support sysfs
		#if 0
		fd = inotify_init();
		if(fd<0)
		{	
			ERROR("fail to init notify\n");
			return -1;
		}
		param.fd = fd;		
		//
		//Add wd files here ,don't over MAX_WD_NUM
		//
		wd = inotify_add_watch(fd,"/sys/devices/platform/usb_modem/link_state",IN_MODIFY);
		#else
		for(i=0;i<MAX_WD_NUM;i++)
			param.entries[i].wd = -1;
		wd = open("/sys/devices/platform/usb_modem/link_state",O_RDONLY);
		#endif
		if(wd>=0)
			param.entries[param.wd_count].wd = wd;
		param.entries[param.wd_count].on_change = link_state_changed;
		param.wd_count++;

		
		
		ret = pthread_create(&s_tid_hw, &attr, hardware_monitor, &param);
		if (ret < 0) {
			perror ("pthread_create");
			return -1;
		}
		
	}

    return 0;

}


