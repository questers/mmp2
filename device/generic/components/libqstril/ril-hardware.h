#ifndef _RIL_HARDWARE_H
#define _RIL_HARDWARE_H

enum
{
	kRIL_HW_UNKNOWN = 0,
	kRIL_HW_MF210,	
	kRIL_HW_MC2716,	
	kRIL_HW_AD3812,
	kRIL_HW_M305,
	kRIL_HW_MC8630,
	kRIL_HW_MAX,	
	kRIL_HW_MG3732 = kRIL_HW_AD3812
};
enum
{
	kPREFER_NETWORK_TYPE_WCDMA = 1,
	kPREFER_NETWORK_TYPE_CDMA_EVDV = 2,
	kPREFER_NETWORK_TYPE_TD_SCDMA = 3 ,
	kPREFER_NETWORK_TYPE_DEFAULT = kPREFER_NETWORK_TYPE_WCDMA
};
typedef struct
{
	unsigned int model;
	char* model_name;
	int		    prefer_net;
	unsigned long vid;
	unsigned long pid;
	char*   service_port;
	char*   modem_port;
	char*   voice_port;
	char*   voice_format;
	char*   voice_rw_period;

	//internal use
	void* private;
	
	
}ST_RIL_HARDWARE,*PST_RIL_HARDWARE;


int rilhw_init(void);

void rilhw_found(PST_RIL_HARDWARE *found);
int rilhw_power(PST_RIL_HARDWARE hardware,int on);
int rilhw_autosuspend(PST_RIL_HARDWARE hardware,int enable);
//int rilhw_link_change(void);
int rilhw_notify_screen_state(int state);

//wake lock mainly for in call use
int rilhw_acquire_wakelock();
int rilhw_release_wakelock();

#endif 


