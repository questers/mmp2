#ifndef _RIL_CONFIG_H_
#define _RIL_CONFIG_H_

#include <telephony/ril.h>

//-------------system---------

#define RIL_IMEISV_VERSION  2
#define RIL_DRIVER_VERSION "QRIL 1.1"
#define MAX_AT_RESPONSE (8 * 1024)
#define START_PPPD_TIMEOUT 	60 /*10*2=20s*/

//5s
#define PPPD_WDG_TIMEOUT		5

#define STOP_PPPD_TIMEOUT		30//s

#define SYS_NET_PATH   "/sys/class/net"
#define PPP_INTERFACE  "ppp0"

#define CDMA_MASQUERADING 1

#define MAX_DATA_CALL_COUNT 16

#endif //_ZTEMT_RIL_CONFIG_H

