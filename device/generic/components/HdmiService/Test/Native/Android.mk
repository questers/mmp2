LOCAL_PATH:= $(call my-dir)

#
# HdmiServer
#
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
	HdmiServiceClient.cpp \
	main.cpp

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
        libbinder \
        libhdmiservice


LOCAL_PRELINK_MODULE := false

LOCAL_MODULE := HdmiServiceTest
LOCAL_MODULE_TAGS := tests

include $(BUILD_EXECUTABLE)
