package com.marvell.hdmiservicetest;
import com.marvell.hdmimanager.HdmiManager;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class HdmiServiceTest extends Activity {
    /** Called when the activity is first created. */
	private static final String TAG = "HdmiServiceTest";
	private EventListener mListener = new EventListener();
	private HdmiManager mManager = new HdmiManager();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mManager.RegisterListener(mListener);
        int state = mManager.GetHdmiSinkState();
        Log.d(TAG, "hdmi sink state = " + state);
    }
}
