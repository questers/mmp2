package com.marvell.hdmiservicetest;
import android.util.Log;

import com.marvell.hdmimanager.HdmiListener;
public class EventListener implements HdmiListener {
	static private final String LOG_TAG = "EventListener";
	public void onEvent(int event_code, int reserved){
		Log.d(LOG_TAG, "onEvent code = " + event_code + "; reserved = " + reserved);
	}
}
