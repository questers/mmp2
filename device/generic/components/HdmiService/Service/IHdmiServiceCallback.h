/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

#ifndef ANDROID_IHDMISERVICECALLBACK_H
#define ANDROID_IHDMISERVICECALLBACK_H
#include <utils/Errors.h>  // for status_t
#include <sys/types.h>
#include <binder/IInterface.h>
#include <binder/Parcel.h>
namespace android{
// must key the value same as hdmi_stack event definition
enum {
    CBEVENT_SINK_CONNECTED = 0,
    CBEVENT_SINK_DISCONNECTED,
    CBEVENT_VIDEO_CFG_ERROR,
    CBEVENT_AUDIO_CFG_ERROR,
    CBEVENT_HDCP_ERROR,
    CBEVENT_DISPLAY_MODE_CHANGED
};

class IHdmiServiceCallback: public IInterface
{
public:
    DECLARE_META_INTERFACE(HdmiServiceCallback);
    /******************************************************
     *eventCode:
     *0 : HDMI sink disconnected 
     *1 : HDMI sink connected
     *2 : video cfg error
     *3 : audio cfg error
     *4 : hdcp error
     *5 : display mode changed
     *     reserved : new display mode
     ******************************************************/
    virtual void process_event(int eventCode, int reserved) = 0;
};

// ----------------------------------------------------------------------------

class BnHdmiServiceCallback: public BnInterface<IHdmiServiceCallback>
{
public:
    virtual status_t    onTransact( uint32_t code,
                                    const Parcel& data,
                                    Parcel* reply,
                                    uint32_t flags = 0);
};

}; // namespace android
#endif
