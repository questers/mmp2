/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */
#define LOG_TAG "HdmiSinkCaps"
#include <utils/Log.h>
#include "HdmiSinkCaps.h"
namespace android {
sp<HdmiSinkCaps> readFromParcel(const Parcel& data) {
    sp<HdmiSinkCaps> ret = new HdmiSinkCaps();
    data.read(&(ret->caps.validEdid), sizeof(ret->caps.validEdid));
    data.read(ret->caps.monitorName, sizeof(ret->caps.monitorName));
    data.read(ret->caps.mnfrName, sizeof(ret->caps.mnfrName));
    data.read(ret->caps.productCode, sizeof(ret->caps.productCode));
    data.read(&(ret->caps.mnfrWeek), sizeof(ret->caps.mnfrWeek));
    data.read(&(ret->caps.mnfrYear), sizeof(ret->caps.mnfrYear));
    ret->caps.serialNum = data.readInt32();

    data.read(&(ret->caps.minVRate), sizeof(ret->caps.minVRate));
    data.read(&(ret->caps.maxVRate), sizeof(ret->caps.maxVRate));
    data.read(&(ret->caps.minHRate), sizeof(ret->caps.minHRate));
    data.read(&(ret->caps.maxHRate), sizeof(ret->caps.maxHRate));
    ret->caps.maxPixelClock = data.readInt32();

    data.read(&(ret->caps.maxHSize), sizeof(ret->caps.maxHSize));
    data.read(&(ret->caps.maxVSize), sizeof(ret->caps.maxVSize));

    ret->caps.gamma =data.readFloat();

    ret->caps.resCnt = data.readInt32();

    for( int i = 0; i < ret->caps.resCnt; i++ ) {
        ret->caps.resInfo[i].hActive = data.readInt32();
        ret->caps.resInfo[i].vActive = data.readInt32();
        ret->caps.resInfo[i].refreshRate = data.readInt32();
        ret->caps.resInfo[i].interlaced = data.readInt32();
    }

    // always hdmi mode
    ret->caps.hdmiMode = 1;

    ret->caps.sprtedDispRes = data.readInt32();

    ret->caps.prefResInfo.hActive = data.readInt32();
    ret->caps.prefResInfo.vActive = data.readInt32();
    ret->caps.prefResInfo.refreshRate = data.readInt32();
    ret->caps.prefResInfo.interlaced = data.readInt32();

    data.read(&(ret->caps.YCbCr444Sprt), sizeof(ret->caps.YCbCr444Sprt));
    data.read(&(ret->caps.YCbCr422Sprt), sizeof(ret->caps.YCbCr422Sprt));
    data.read(&(ret->caps.DC30bitSprt), sizeof(ret->caps.DC30bitSprt));
    data.read(&(ret->caps.DC36bitSprt), sizeof(ret->caps.DC36bitSprt));
    data.read(&(ret->caps.YCbCr444AtDC), sizeof(ret->caps.YCbCr444AtDC));
    data.read(&(ret->caps.AISprt), sizeof(ret->caps.AISprt));

    for( int i =0; i < 4; i++){
        data.read(&(ret->caps.prInfo[i]), sizeof(ret->caps.prInfo[i]));
    }

    data.read(&(ret->caps.calInfo), sizeof(ret->caps.calInfo));
    data.read(&(ret->caps.spkrAlloc), sizeof(ret->caps.spkrAlloc));

    for( int i = 0; i < 15; i++) {
        ret->caps.audioInfo[i].audioFmt = data.readInt32();
        data.read(&(ret->caps.audioInfo[i].freqSprt), sizeof(ret->caps.audioInfo[i].freqSprt));
        ret->caps.audioInfo[i].maxBitRate = data.readInt32();
        data.read(&(ret->caps.audioInfo[i].wdLenSprt), sizeof(ret->caps.audioInfo[i].wdLenSprt));
        data.read(&(ret->caps.audioInfo[i].maxNumChnls), sizeof(ret->caps.audioInfo[i].maxNumChnls));
    }
    
}

status_t writeToParcel(Parcel* reply, const sp<HdmiSinkCaps>& o){

}
}; //namespace end
