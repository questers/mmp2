LOCAL_PATH:= $(call my-dir)

#
# libhdmiservice
#
include $(CLEAR_VARS)

LOCAL_C_INCLUDES += device/generic/components/libhdmi/include

LOCAL_SRC_FILES := \
	HdmiService.cpp \
	IHdmiService.cpp \
	IHdmiServiceCallback.cpp \
        IHdmiServerDeathNotifier.cpp 

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
        libbinder \
        libhdmi_mrvl \
        libwtpsp \
        libmedia

LOCAL_PRELINK_MODULE := false

LOCAL_MODULE := libhdmiservice
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)

#
# HdmiServer
#
include $(CLEAR_VARS)

LOCAL_C_INCLUDES += device/generic/components/libhdmi/include

LOCAL_SRC_FILES := \
        main.cpp 

LOCAL_SHARED_LIBRARIES := \
	libutils \
        libbinder \
        libhdmiservice \
        libwtpsp


LOCAL_PRELINK_MODULE := false

LOCAL_MODULE := HdmiServer
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)


