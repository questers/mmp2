/*
* (C) Copyright 2010 Marvell International Ltd.
* All Rights Reserved
*
* MARVELL CONFIDENTIAL
* Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
* The source code contained or described herein and all documents related to
* the source code ("Material") are owned by Marvell International Ltd or its
* suppliers or licensors. Title to the Material remains with Marvell International Ltd
* or its suppliers and licensors. The Material contains trade secrets and
* proprietary and confidential information of Marvell or its suppliers and
* licensors. The Material is protected by worldwide copyright and trade secret
* laws and treaty provisions. No part of the Material may be used, copied,
* reproduced, modified, published, uploaded, posted, transmitted, distributed,
* or disclosed in any way without Marvell's prior express written permission.
*
* No license under any patent, copyright, trade secret or other intellectual
* property right is granted to or conferred upon you by disclosure or delivery
* of the Materials, either expressly, by implication, inducement, estoppel or
* otherwise. Any license under such intellectual property rights must be
* express and approved by Marvell in writing.
*
*/

#define LOG_TAG	"HdmiService"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <math.h>

#include "HdmiService.h"
#include "HdmiAudioFormat.h"
#include "HdmiVideoFormat.h"
#include <binder/IServiceManager.h>
#include <binder/IPCThreadState.h>
#include <cutils/properties.h>
#include <media/AudioSystem.h>

#define PREFER_RES_PROP "persist.hdmi.prefer_resolution"
#define BOOTCOMPLETE_PROP "sys.boot_completed"

// must keep alignment with vendor/marvell/<product>/libaudiopath/AudioSetting.h
#define DEFAULT_OUTPUT_SAMPLE_RATE 48000
#define HEADSET_STATE_PATH "/sys/class/switch/h2w/state"

namespace android{
using namespace android;

Mutex HdmiStackService::mInstanceLock;
HdmiStackService* HdmiStackService::mInstance = NULL;

static int get_headset_state()
{
    int fd = 0;
    uint8_t buffer[1];

    fd = open(HEADSET_STATE_PATH, O_RDONLY);
    if( !fd )
        LOGE("can not open headset state file!");
    read(fd, buffer, sizeof(buffer));
    close(fd);

    if (buffer[0] & 0x03)
        return 1;
    else
        return 0;
}

int HdmiStackService:: mStateMachine[EVENT_MAX][DISPLAY_MODE_MAX] = {
    {DISPLAY_MODE_SINK_DISCONNECTED, DISPLAY_MODE_SINK_DISCONNECTED, DISPLAY_MODE_SINK_DISCONNECTED, DISPLAY_MODE_SINK_DISCONNECTED, DISPLAY_MODE_SINK_DISCONNECTED},
    {DISPLAY_MODE_MIRROR, DISPLAY_MODE_INVALID, DISPLAY_MODE_INVALID, DISPLAY_MODE_INVALID, DISPLAY_MODE_INVALID},
    {DISPLAY_MODE_INVALID, DISPLAY_MODE_NORMAL, DISPLAY_MODE_NORMAL, DISPLAY_MODE_NORMAL, DISPLAY_MODE_INVALID},
    {DISPLAY_MODE_INVALID, DISPLAY_MODE_MIRROR, DISPLAY_MODE_MIRROR, DISPLAY_MODE_MIRROR, DISPLAY_MODE_MIRROR,},
    {DISPLAY_MODE_INVALID, DISPLAY_MODE_HDMI_ONLY, DISPLAY_MODE_HDMI_ONLY, DISPLAY_MODE_HDMI_ONLY, DISPLAY_MODE_INVALID},
    {DISPLAY_MODE_INVALID, DISPLAY_MODE_VIDEO, DISPLAY_MODE_VIDEO, DISPLAY_MODE_VIDEO, DISPLAY_MODE_VIDEO},    
};

HdmiStackService::HdmiStackService(){
    mWorkThreadRunning = false;
    mCallbacks.clear();
    init();
    vpp_init();
}

HdmiStackService::~HdmiStackService(){    
    vpp_destroy();
}

bool HdmiStackService::HdmiWorkerThread::threadLoop(){
    char value[PROPERTY_VALUE_MAX];
    property_get( BOOTCOMPLETE_PROP, value, "0" );
    if( memcmp(value, "1", 1) != 0 ) {
        // boot not completed, sleep 50ms, try again
        usleep(50000);
        return true;
    }

    do {
        HdmiWorkBase *item = 0;
        // wait for work item
        Mutex::Autolock _l( mService->mWorkItemLock );
        while( mService->mWorkItems.size()<=0 ) {
            mService->mWorkItemCond.wait( mService->mWorkItemLock );
        }
        item = mService->mWorkItems[0];
        mService->mWorkItems.removeAt(0);
        item->doWork();
        delete item;
    } while(exitPending() == false);
    return false;
}

void HdmiStackService::ConnWork::doWork(){
    if( mState == 0 ) {
        AudioSystem::setDeviceConnectionState(AudioSystem::DEVICE_OUT_HDMI,
                  AudioSystem::DEVICE_STATE_UNAVAILABLE,"");
    } else {
        AudioSystem::setDeviceConnectionState(AudioSystem::DEVICE_OUT_HDMI,
                  AudioSystem::DEVICE_STATE_AVAILABLE,"");
    }
}

void HdmiStackService::EventCBWork::doWork(){
    if( mCB.get() ) {
        mCB->process_event(mEvent, mDetail);
    } else {
        mService->notify_client(mEvent, mDetail);  
    }
}

void HdmiStackService::init(){
    mVppHandle = 0;
    mSinkState = 0;
    mHdcpState = 0;
    mPrevHdcpState = 0;
    mSupportedDispRes = 0;
    mPreferedDispRes = 0;
    mCurrentDispRes = 0;
    mPrevDispRes = 0;
    mSupportedAudioFmt = 0;
    mCurrentAudioFmt = 0;
    mPrevAudioFmt = 0;
    mAudioMute = 0;
    mMode = DISPLAY_MODE_SINK_DISCONNECTED;
    mResWidth = 1280;
    mResHeight = 720;
    mNumChannels = 0;
    mSampleFrequence = 0;
    memset(&mSinkCaps ,0 , sizeof(mSinkCaps));

    struct fb_var_screeninfo var_base;
    int fd  = open("/dev/graphics/fb0", O_RDWR);
    if( fd > 0 ) {
        int ret = ioctl(fd, FBIOGET_VSCREENINFO, &var_base);
        if( ret >=  0 ) {
            mResWidth = var_base.xres;
            mResHeight = var_base.yres;    
        } else {
            LOGE("Get /dev/graphics/fb0 vscreen info fail");
        } 
        close(fd);
    } else {
        LOGE("Open /dev/graphics/fb0 fail");
        return;
    }

    // init work thread
    if( !mWorkThreadRunning ) {
        mWorkItems.clear();
        mWorkThread = new HdmiWorkerThread(this);
        mWorkThread->run("HdmiWorkThread", PRIORITY_DISPLAY, 0);
        mWorkThreadRunning = true;
    }
}

void HdmiStackService::binderDied(const wp<IBinder>& who) {
    LOGD("binderDied() 1 %p, tid %d, calling tid %d", 
        who.unsafe_get(), gettid(), IPCThreadState::self()->getCallingPid());
    AutoMutex _l(mInstanceLock);
    mCallbacks.removeItem(who.unsafe_get());
}

void HdmiStackService::instantiate() {
    defaultServiceManager()->addService(
            String16("HdmiService"), new HdmiStackService());
}

int HdmiStackService::event_cb(unsigned int code, void * info, void * user){
    HdmiStackService* service = (HdmiStackService*)user;
    return service->on_event(code, info);
}

void HdmiStackService::notify_client( int event, int info) {
    int n = mCallbacks.size();
    for( int i = 0; i < n; i++ ) {
        mCallbacks.valueAt(i)->process_event(event, info);
    }
}

int HdmiStackService::on_event(unsigned int code, void * info) {
    unsigned int evt = *((unsigned int*)info);
    AutoMutex _l(mInstanceLock);    
    // ok, we need to process hdmi event
    int ret = process_hdmi_event(evt);
    if( ret ) {
        do {
            Mutex::Autolock _l( mWorkItemLock );
            sp<IHdmiServiceCallback> cb;
            cb.clear();
            EventCBWork *item = new EventCBWork( evt, 0, cb,  this);
            mWorkItems.add(item);
            mWorkItemCond.signal();
        } while(0);        
    }
    return ret;
}

int HdmiStackService::set_display_mode(int mode) {
   int ret = 0;
   LOGD("set_display_mode = %d", mode);
   AutoMutex _l(mInstanceLock);
   if( mode >= DISPLAY_MODE_NORMAL && mode <= DISPLAY_MODE_VIDEO ) {
       ret = enter_mode(mStateMachine[mode + 1][mMode]);
   } else {
       ret = -1;
   }
   return ret;
}

int HdmiStackService::get_display_mode() {
   AutoMutex _l(mInstanceLock);
   LOGD("Current display mode = %d", mMode);
   return mMode;
}

int HdmiStackService::enter_mode(int mode) {
    int ret = 0;
    LOGD("mode change %d ----> %d", mMode, mode);
    int changed = (mode == mMode) ? 0 : 1;
    if( changed == 0 ) return ret;
    if( mode != DISPLAY_MODE_HDMI_ONLY ) {
        // we are not entering hdmi only mode, enable touch and keypad
        system("echo 1 > /proc/driver/tpk_r800");
        system("echo 1 > /proc/driver/pxa27x-keypad");
    }
    switch(mode) {
    case DISPLAY_MODE_INVALID:
        // do noghting
        ret = -1;
        break;
    case DISPLAY_MODE_SINK_DISCONNECTED:
        system("echo 0 > /proc/pxa168fb");
        if( get_headset_state() == 0 ) {
            LOGD("enable speaker codec switch");
            system("amixer cset iface='MIXER',name='SPKOUT Playback Switch' 1,1");
            system("amixer cset iface='MIXER',name='SPKOUT Mux' 1");
        } else {
            LOGD("enable hp codec switch");
            system("amixer cset iface='MIXER',name='HPOUT Playback Switch' 1,1");
        }
        break;
    case DISPLAY_MODE_NORMAL:
        // panel only, loud speaker only
        system("echo 0 > /proc/pxa168fb");
        set_audio_mute_l(1);
        break;
    case DISPLAY_MODE_MIRROR:
        // mirror, mute loud speaker
        system("echo 1 > /proc/pxa168fb");
        if( get_headset_state() == 0 ) {
            set_audio_mute_l(0);
            //disable codec switch
            LOGD("disable codec switch");
            system("amixer cset iface='MIXER',name='SPKOUT Playback Switch' 0,0");
        } else {
            set_audio_mute_l(1);
        }
        break;
    case DISPLAY_MODE_HDMI_ONLY:
        // hdmi only, mute loud speaker
        system("echo 2 > /proc/pxa168fb");
        // we enter hdmi only mode, disable touch and keypad
        system("echo 0 > /proc/driver/tpk_r800");
        system("echo 0 > /proc/driver/pxa27x-keypad");
        if( get_headset_state() == 0 ) {
            set_audio_mute_l(0);
        }
        break;
    case DISPLAY_MODE_VIDEO:
        // lcd mode switch to normal, enable all sound
        system("echo 0 > /proc/pxa168fb");
        set_audio_mute_l(0);
        break;
    default:
        ret = -1;
        break;
    }
    if( ret == 0 ) {
        mMode = mode;    
        // ok, notify display mode change event
        do {
            Mutex::Autolock _l( mWorkItemLock );
            sp<IHdmiServiceCallback> cb;
            cb.clear();
            EventCBWork *item = new EventCBWork( CBEVENT_DISPLAY_MODE_CHANGED, mMode, cb,  this);
            mWorkItems.add(item);
            mWorkItemCond.signal();
        } while(0);        
    }
    return 0;
}

int HdmiStackService::select_prefered_res_id(){
    int res = RES_INVALID;

    if ((mSinkCaps.prefResInfo.hActive == 720)
        &&(mSinkCaps.prefResInfo.vActive == 480)
        &&(mSinkCaps.prefResInfo.refreshRate == 60)) {

        if (mSinkCaps.prefResInfo.interlaced == 1) {
            if( mSupportedDispRes & (0x1 << RES_525I60 ) ) {
                res = RES_525I60;
            } else {
                res = RES_525I5994;
            }
        }
        else {
            if( mSupportedDispRes & (0x1 << RES_525P60 ) ) {
                res = RES_525P60;
            } else {
                res = RES_525P5994;
            }
        }

    } else if ((mSinkCaps.prefResInfo.hActive == 720)
               &&(mSinkCaps.prefResInfo.vActive == 576)
               &&(mSinkCaps.prefResInfo.refreshRate == 50)) {

        if (mSinkCaps.prefResInfo.interlaced == 1)
            res = RES_625I50;
        else
            res = RES_625P50;

    } else if ((mSinkCaps.prefResInfo.hActive == 1280)
               &&(mSinkCaps.prefResInfo.vActive == 720)) {

        if (mSinkCaps.prefResInfo.refreshRate == 30) {
            if( mSupportedDispRes & (0x1 << RES_720P30) ) {
                res = RES_720P30;
            } else {
                res = RES_720P2997;
            }
        }
        else if (mSinkCaps.prefResInfo.refreshRate == 25)
            res = RES_720P25;
        else if (mSinkCaps.prefResInfo.refreshRate == 60) {
            if( mSupportedDispRes & (0x1 << RES_720P60 ) ) {
                res = RES_720P60;
            } else {
                res = RES_720P5994;
            }
        }
        else if (mSinkCaps.prefResInfo.refreshRate == 50)
            res = RES_720P50;

    } else if ((mSinkCaps.prefResInfo.hActive == 1920)
               &&(mSinkCaps.prefResInfo.vActive == 1080)) {

        if (mSinkCaps.prefResInfo.refreshRate == 30) {
            if( mSupportedDispRes & (0x1 << RES_1080P30)) {
                res = RES_1080P30;
            } else {
                res = RES_1080P2997;
            }
        }
        else if (mSinkCaps.prefResInfo.refreshRate == 25)
            res = RES_1080P25;
        else if (mSinkCaps.prefResInfo.refreshRate == 24) {
            if( mSupportedDispRes & (0x1 << RES_1080P24) ) {
                res = RES_1080P24;
            } else {
                res = RES_1080P2398;
            }
        }
        else if (mSinkCaps.prefResInfo.refreshRate == 60) {
             if (mSinkCaps.prefResInfo.interlaced == 1){
                 if( mSupportedDispRes & (0x1 << RES_1080I60) ) {
                    res = RES_1080I60;
                 } else {
                    res = RES_1080I5994;
                 }
             }
             else {
                if( mSupportedDispRes & (0x1 << RES_1080P60) ) {
                    res = RES_1080P60;
                } else {
                    res = RES_1080P5994;
                }
             }
        }
        else if (mSinkCaps.prefResInfo.refreshRate == 50) {
             if (mSinkCaps.prefResInfo.interlaced == 1)
                 res = RES_1080I50;
             else
                 res = RES_1080P50;
        }
    }
    return res;
}

int HdmiStackService::sink_connected() {
    // sink connected
    MV_VPPOBJ_GetHDMISinkCaps(mVppHandle, &mSinkCaps);
    mSinkState = 1;
    if (mSinkCaps.validEdid)
    {

        mSupportedDispRes = mSinkCaps.sprtedDispRes;
        // now we only support 48k stereo
        mSupportedAudioFmt = HDMI_AUDIO_CHANNEL_STEREO | HDMI_AUDIO_SR_48K;

        char value[PROPERTY_VALUE_MAX];
        property_get( PREFER_RES_PROP, value, "none" );
        int w = 0;
        int h = 0;
        if( strcmp(value, "1080p") == 0 ) {
            w = 1920;
            h = 1080;
        } else if( strcmp(value, "720p") == 0 ) {
            w = 1280;
            h = 720;
        } else if( strcmp(value, "576p") == 0 ) {
            w = 720;
            h = 576;
        } else if( strcmp(value, "480p") == 0 ) {
            w = 720;
            h = 480;
        } else {
            w = mResWidth;
            h = mResHeight;
        }
        LOGD("choose_res_id %s : w = %d, h = %d", value,w, h);
        int res = choose_res_id(w, h);
        if(res == RES_INVALID) {
            LOGE("Cannot select prefered res id");
            return 0;
        }
        MV_VPPOBJ_PhyReset(mVppHandle);
        LOGD("ConfigOutputRes = %d", res);
        // configure video
        MV_VPPOBJ_ConfigOutputRes(mVppHandle, res);// set the resolution
        MV_VPPOBJ_SetHdmiVideoFmt(mVppHandle, OUTPUT_COLOR_FMT_RGB888, OUTPUT_BIT_DEPTH_8BIT, 1);
        mCurrentDispRes = res;
        mPreferedDispRes = res;

        // configure audio
        LOGD("ConfigAudio -----------------------");
        MV_VPPOBJ_ConfigAudio(mVppHandle, 2, DEFAULT_OUTPUT_SAMPLE_RATE, 128,-1,100);
        mCurrentAudioFmt = HDMI_AUDIO_CHANNEL_STEREO | HDMI_AUDIO_SR_48K;
        mPrevAudioFmt = mCurrentAudioFmt;
        mNumChannels = 2;
        mSampleFrequence = DEFAULT_OUTPUT_SAMPLE_RATE;
        LOGD("Config Audio Done--------------");
        do {
            Mutex::Autolock _l( mWorkItemLock );
            ConnWork *item = new ConnWork(1 , this);
            mWorkItems.add(item);
            mWorkItemCond.signal();
        } while(0);
        
        // state change
       enter_mode(mStateMachine[EVENT_SINK_CONNECTED][mMode]);

    } else {
        // no valid edid
        LOGE("no valid edid information!!!!!!!!!!!!!!!!");
    }
    return 1;
}

int HdmiStackService::sink_disconnected() {
    // disconnected 
    do {
       Mutex::Autolock _wl( mWorkItemLock );
       ConnWork *item = new ConnWork(0 , this);
       mWorkItems.add(item);
       mWorkItemCond.signal();
    } while(0);
    mSinkState = 0;
    enter_mode(mStateMachine[EVENT_SINK_DISCONNECTED][mMode]);
    return 1;
}

int HdmiStackService::process_hdmi_event(unsigned int code) {
    int ret = 0;
    LOGD("process hdmi event %d", code);
    switch(code) {
        case MV_VPP_EVENT_HDMI_SINK_CONNECTED:
            // hdmi sink connected
            ret = sink_connected();
            break;
        case MV_VPP_EVENT_HDMI_SINK_DISCONNECTED:
			// Ellie:don't handle it if state not changed
            if(mSinkState==0)
                break;
            // hdmi sink disconnected
            // disable hdmi to save power
            MV_VPPOBJ_DisableHdmi(mVppHandle);
            ret = sink_disconnected();
            // re-init state
            init();
            break;
        case MV_VPP_EVENT_HDMI_VIDEO_CFG_ERR:
            LOGE("MV_VPP_EVENT_HDMI_VIDEO_CFG_ERR: -----------");
            // TODO: need consider how to roll back previous video cfg
            break;
        case MV_VPP_EVENT_HDMI_AUDIO_CFG_ERR:
            // TODO: need consider how to roll back previous audio cfg
            LOGE("MV_VPP_EVENT_HDMI_AUDIO_CFG_ERR: ----------");
            break;
        case MV_VPP_EVENT_HDMI_HDCP_ERR:
            // load hdcp key again
            load_hdcp_keys();
            break;
        default:
            break;
    }
    return ret;
}

HdmiStackService* HdmiStackService::getInstance() {
    AutoMutex _l(mInstanceLock);
    if (mInstance == NULL) {
        mInstance = new HdmiStackService();
    }
    return mInstance;
}

int HdmiStackService::get_hdmi_sink_state() {
    AutoMutex _l(mInstanceLock);
    LOGD("get_hdmi_sink_state result = %d", mSinkState);
    return mSinkState;
}
int HdmiStackService::set_hdcp_state( int state ) {
    LOGD("set_hdcp_state %d", state);
    AutoMutex _l(mInstanceLock);
    int ret = MV_VPPOBJ_EnableHDCP(mVppHandle, state);
    if( ret == MV_VPP_OK ) {
        mHdcpState = state;
    }
    return ret;
}
int HdmiStackService::get_hdcp_state() {
    AutoMutex _l(mInstanceLock);    
    LOGD("get_hdcp_state %d", mHdcpState);
    return mHdcpState;
}
/*
int HdmiStackService::load_hdcp_keys(String8 keys){
    AutoMutex _l(mCallbackLock);
    LOGD("load_hdcp_keys %s", keys.string());
    // TODO: load hdcp keys
    return 0;
}
*/
int HdmiStackService::get_supported_display_resolution(){
    AutoMutex _l(mInstanceLock);    
    LOGD("get_supported_display_resolution 0x%x", mSupportedDispRes);
    return mSupportedDispRes;
}
int HdmiStackService::get_prefered_display_resolution() {
    AutoMutex _l(mInstanceLock);    
    LOGD("get_prefered_display_resolution 0x%x", mPreferedDispRes);
    return 0x1 << mPreferedDispRes;
}

int HdmiStackService::get_stack_res_id( int id ) {
    int resid = 0;
    if( id == 0 ) {
        return -1;
    }
    while( ((id >> resid) & 0x1) == 0 ) {
        resid++;
    }
    return resid;
}

// use emulated popcount optimization
// http://www.df.lth.se/~john_e/gems/gem002d.html
uint32_t HdmiStackService::popCount(uint32_t u)
{
    u = ((u&0x55555555) + ((u>>1)&0x55555555));
    u = ((u&0x33333333) + ((u>>2)&0x33333333));
    u = ((u&0x0f0f0f0f) + ((u>>4)&0x0f0f0f0f));
    u = ((u&0x00ff00ff) + ((u>>8)&0x00ff00ff));
    u = ( u&0x0000ffff) + (u>>16);
    return u;
}


int HdmiStackService::set_display_resolution(int res_id){
    if( popCount(res_id) != 1) {
        LOGD("set_display_resolution with invalid res_id 0x%x", res_id);
        return -1;        
    }
    // there is no log2 math api in bionic, so get res ugly
    int id = get_stack_res_id(res_id);
    if( id < 0 ) {
        return -1;
    }
    AutoMutex _l(mInstanceLock);
    if( id == mCurrentDispRes ) return 0;
    LOGD("set_display_resolution res_id = %d, id = %d", res_id, id);
    reconfig_hdmi(id, mNumChannels, mSampleFrequence);
    return 0;
}
int HdmiStackService::get_current_display_resolution() {
    AutoMutex _l(mCallbackLock);
    LOGD("get_current_display_resolution : 0x%x", mCurrentDispRes);
    return 0x1 << mCurrentDispRes;
}
int HdmiStackService::set_display_resolution_near(int w, int h) {
    int id = choose_res_id(w, h);
    AutoMutex _l(mInstanceLock);
    LOGD("set_display_resolution_near w = %d, h = %d",w, h);
    if( id == mCurrentDispRes ) return 0;
    reconfig_hdmi(id, mNumChannels, mSampleFrequence);
    return 0;
}
int HdmiStackService::get_supported_audio_format(){
    AutoMutex _l(mInstanceLock);
    LOGD("get_supported_audio_format 0x%x", mSupportedAudioFmt);
    return mSupportedAudioFmt;
}

int HdmiStackService::is_video_res_supported( int resid) {
    int ret = 0 ;
    if( mSupportedDispRes & (0x1 << resid) ) {
        ret = 1;
    }
    return ret;
}
int HdmiStackService::is_audio_fmt_supported( int fmt ) {
    return 0;
}


int HdmiStackService::get_audio_param(int audio_fmt, int *sample_rate, int *channel_count, int *spkrcfg){
     int ret = 0;
     int sr = audio_fmt & HDMI_AUDIO_SR_MASK;
     int ct = audio_fmt & HDMI_AUDIO_CHANNEL_MASK;
     switch( sr ) {
        case HDMI_AUDIO_SR_32K:
            *sample_rate = 32000;
            break;
        case HDMI_AUDIO_SR_44_1K:
            *sample_rate = 44100;
            break;
        case HDMI_AUDIO_SR_48K:
            *sample_rate = 48000;
            break;
        case HDMI_AUDIO_SR_64K:
            *sample_rate = 64000;
            break;
        case HDMI_AUDIO_SR_88_2K:
            *sample_rate = 88200;
            break;
        case HDMI_AUDIO_SR_96K:
            *sample_rate = 96000;
            break;
        case HDMI_AUDIO_SR_176_4k:
            *sample_rate = 176400;
            break;
        case HDMI_AUDIO_SR_192K:
            *sample_rate = 192000;
            break;
        default:
            LOGE("invalid sample rate");
            return -1;
            break;            
     }
     switch(ct) {
        case HDMI_AUDIO_CHANNEL_MONO:
            *channel_count = 1;
            break;
        case HDMI_AUDIO_CHANNEL_STEREO:
            *channel_count = 2;
            break;
        case HDMI_AUDIO_CHANNEL_5POINT1:
            *channel_count = 6;
            break;
        case HDMI_AUDIO_CHANNEL_7POINT1:
            *channel_count = 8;
            break;
        default:
            LOGE("invalid channel count");
            return -1;
     }
     // set as -1 as unknown now
     *spkrcfg = -1;
     return 0;
}

int HdmiStackService::set_audio_format(int audio_format){
    AutoMutex _l(mInstanceLock);
    LOGD("set_audio_format 0x%x", audio_format);
    int sample_rate = 0;
    int channel_count = 0;
    int spkrcfg = 0;
    if( get_audio_param(audio_format, &sample_rate, &channel_count, &spkrcfg) == 0 ) {
        reconfig_hdmi(mCurrentDispRes, channel_count, sample_rate);
        return 0;
    } else {
        LOGE("get_audio_param failed");
        return -1;
    }
}

int HdmiStackService::set_audio_mute_l(int mute){
    if( mute == mAudioMute ) {
        return 0;
    }
    int ret = MV_VPPOBJ_SetHdmiAudioMute(mVppHandle, mute);
    if( ret == 0/*MV_VPP_OK*/ ) {
        mAudioMute = mute;
        ret = 0;
    } else {
        LOGD("MV_VPPOBJ_SetHdmiAudioMute failed , error code = %d", ret);
        ret = -1;
    }
    return ret;
}

int HdmiStackService::set_audio_mute(int mute){
    AutoMutex _l(mInstanceLock);
    LOGD("set_audio_mute %d", mute);
    return set_audio_mute_l(mute);
}

int HdmiStackService::get_audio_fmt( int sample_rate, int channel_count){
    int sr;
    int cc;
    switch( sample_rate ) {
    case 32000:
        sr = HDMI_AUDIO_SR_32K;
        break;
    case 44100:
        sr = HDMI_AUDIO_SR_44_1K;
        break;
    case 48000:
        sr = HDMI_AUDIO_SR_48K;
        break;
    case 64000:
        sr = HDMI_AUDIO_SR_64K;
        break;
    case 88200:
        sr = HDMI_AUDIO_SR_88_2K;
        break;
    case 96000:
        sr = HDMI_AUDIO_SR_96K;
        break;
    case 176400:
        sr = HDMI_AUDIO_SR_176_4k;
        break;
    case 192000:
        sr = HDMI_AUDIO_SR_192K;
        break;
    default:
        return -1;
    }
    switch( channel_count ) {
    case 1:
        cc = HDMI_AUDIO_CHANNEL_MONO;
        break;
    case 2:
        cc = HDMI_AUDIO_CHANNEL_STEREO;
        break;
    case 6:
        cc = HDMI_AUDIO_CHANNEL_5POINT1;
        break;
    case 8:
        cc = HDMI_AUDIO_CHANNEL_7POINT1;
        break;
    default:
        return -1;
    }
    return cc & sr;
}

int HdmiStackService::reconfig_hdmi(int video_id, int channel_count, int sample_rate) {
    MV_VPPOBJ_PhyReset(mVppHandle);
    MV_VPPOBJ_ConfigOutputRes(mVppHandle, video_id);
    MV_VPPOBJ_SetHdmiVideoFmt(mVppHandle, OUTPUT_COLOR_FMT_RGB888, OUTPUT_BIT_DEPTH_8BIT, 1);    
    MV_VPPOBJ_ConfigAudio(mVppHandle, channel_count, sample_rate, 128,-1,100);
    if( mCurrentDispRes != video_id ) {
        mPrevDispRes = mCurrentDispRes;
        mCurrentDispRes = video_id;
    }
    
    mNumChannels = channel_count;
    mSampleFrequence = sample_rate;

    int audio_id = get_audio_fmt(sample_rate, channel_count);
    if( audio_id != mCurrentAudioFmt ) {
        mPrevAudioFmt = mCurrentAudioFmt;
        mCurrentAudioFmt = audio_id;
    }
    return 0;
}

int HdmiStackService::load_hdcp_keys() {
    int version;
    int sign_valid;
    int hdcp_fd;
    int byte_read;
    int last_read;
    int ret = 0;
    unsigned char *buf = (unsigned char*)malloc(5116);
    MV_VPPOBJ_LoadHDCPKeys(mVppHandle);
    hdcp_fd = open("/system/etc/hdcp/srm.bin", O_RDONLY);
    if(hdcp_fd >= 0)
    {
        byte_read = 0;
        do
        {
            last_read = byte_read;
            byte_read += read(hdcp_fd, buf, 5116);
        } while(byte_read != last_read);
        if(byte_read != 0)
        {
            MV_VPPOBJ_ConfigureSRM (mVppHandle, buf, &sign_valid, &version);
            LOGD("srm version is %d\n", version);
        }
        close(hdcp_fd);
    } else {
        ret = -1;
    }
    free(buf);
    return ret;
}

int HdmiStackService::registerCallback(const sp<IHdmiServiceCallback>& callback) {
    wp<IBinder> who = callback->asBinder();

    LOGD("registerCallback() %p, tid %d, calling tid %d", 
        who.unsafe_get(), gettid(), IPCThreadState::self()->getCallingPid());

    AutoMutex _l(mInstanceLock);
    mCallbacks.add(who.unsafe_get(), callback);
    callback->asBinder()->linkToDeath(this);
    do {
            Mutex::Autolock _l( mWorkItemLock );
            sp<IHdmiServiceCallback> cb = callback;
            EventCBWork *item = new EventCBWork(CBEVENT_DISPLAY_MODE_CHANGED , mMode, cb,  this);
            mWorkItems.add(item);
            mWorkItemCond.signal();
    } while(0);        

    return 0;    
}
int HdmiStackService::unregisterCallback(const sp<IHdmiServiceCallback>& callback){
     wp<IBinder> who = callback->asBinder();

    LOGD("registerCallback() %p, tid %d, calling tid %d", 
        who.unsafe_get(), gettid(), IPCThreadState::self()->getCallingPid());

    AutoMutex _l(mInstanceLock);
    mCallbacks.removeItem(who.unsafe_get());
    callback->asBinder()->unlinkToDeath(this);
    return 0;
}

int HdmiStackService::vpp_init() {
    MV_VPP_Init("/dev/graphics/fb1");
    MV_VPPOBJ_Create(&mVppHandle);
    MV_VPPOBJ_Reset(mVppHandle);
    MV_VPPOBJ_Config(mVppHandle);
    load_hdcp_keys();
    int status = MV_VPPOBJ_GetHDMIConnectStatus(mVppHandle);
    LOGD("status = %d, mSinkState = %d", status, mSinkState);
    if( status == 1 && mSinkState == 0 ) { 
        sink_connected();
    } else {
        //sink not connected, turn off hdmi to save power
        MV_VPPOBJ_DisableHdmi(mVppHandle);
    }
    MV_VPPOBJ_RegisterHdmiCallback (mVppHandle, 0, HdmiStackService::event_cb, this);    
    return 0;
}

int HdmiStackService::vpp_destroy(){
    MV_VPPOBJ_Destroy(mVppHandle);
    return 0;
}

int HdmiStackService::choose_res_id(int w, int h){
    // no we choose prefered res
    int res = RES_INVALID;
    if( w == 720 && h == 480 ) {
        if( mSupportedDispRes & (0x1 << RES_525P60) ) {
            res = RES_525P60;
        }else if( mSupportedDispRes & (0x1 << RES_525P5994) ) {
            res = RES_525P5994;
        }        
    } else if( w == 720 && h == 576 ) {
        if( mSupportedDispRes & (0x1 << RES_625P50) ) {
            res = RES_625P50;
        }        
    } else if( w == 1280 && h == 720 ) {
        if( mSupportedDispRes & (0x1 << RES_720P60) ) {
            res = RES_720P60;
        } else if( mSupportedDispRes & (0x1 << RES_720P5994) ) {
            res = RES_720P5994;
        } else if( mSupportedDispRes & (0x1 << RES_720P50) ) {
            res = RES_720P50;
        } else if( mSupportedDispRes & (0x1 << RES_720P30) ) {
            res = RES_720P30;
        } else if( mSupportedDispRes & (0x1 << RES_720P2997) ) {
            res = RES_720P2997;
        } else if( mSupportedDispRes & (0x1 << RES_720P25) ) {
            res = RES_720P25;
        }
    } else if( w == 1920 && h == 1080 ) {
        if( mSupportedDispRes & (0x1 << RES_1080P60) ) {
            res = RES_1080P60;
        } else if( mSupportedDispRes & (0x1 << RES_1080P5994) ) {
            res = RES_1080P5994;
        } else if( mSupportedDispRes & (0x1 << RES_1080P50) ) {
            res = RES_1080P50;
        } else if( mSupportedDispRes & (0x1 << RES_1080P30) ) {
            res = RES_1080P30;
        } else if( mSupportedDispRes & (0x1 << RES_1080P2997) ) {
            res = RES_1080P2997;
        } else if( mSupportedDispRes & (0x1 << RES_1080P25) ) {
            res = RES_1080P25;
        } else if( mSupportedDispRes & (0x1 << RES_1080P24) ) {
            res = RES_1080P24;
        } else if( mSupportedDispRes & (0x1 << RES_1080P2398) ) {
            res = RES_1080P2398;
        }
    }
    if ( res == RES_INVALID ) {
        // no exactly match resolution, choose lagest first
        if( mSupportedDispRes & (0x1 << RES_1080P60) ) {
            res = RES_1080P60;
        } else if( mSupportedDispRes & (0x1 << RES_1080P5994) ) {
            res = RES_1080P5994;
        } else if( mSupportedDispRes & (0x1 << RES_1080P50) ) {
            res = RES_1080P50;
        } else if( mSupportedDispRes & (0x1 << RES_1080P30) ) {
            res = RES_1080P30;
        } else if( mSupportedDispRes & (0x1 << RES_1080P2997) ) {
            res = RES_1080P2997;
        } else if( mSupportedDispRes & (0x1 << RES_1080P25) ) {
            res = RES_1080P25;
        } else if( mSupportedDispRes & (0x1 << RES_1080P24) ) {
            res = RES_1080P24;
        } else if( mSupportedDispRes & (0x1 << RES_1080P2398) ) {
            res = RES_1080P2398;
        } else if( mSupportedDispRes & (0x1 << RES_720P60) ) {
            res = RES_720P60;
        } else if( mSupportedDispRes & (0x1 << RES_720P5994) ) {
            res = RES_720P5994;
        } else if( mSupportedDispRes & (0x1 << RES_720P50) ) {
            res = RES_720P50;
        } else if( mSupportedDispRes & (0x1 << RES_720P30) ) {
            res = RES_720P30;
        } else if( mSupportedDispRes & (0x1 << RES_720P2997) ) {
            res = RES_720P2997;
        } else if( mSupportedDispRes & (0x1 << RES_720P25) ) {
            res = RES_720P25;
        } else if( mSupportedDispRes & (0x1 << RES_625P50) ) {
            res = RES_625P50;
        } else if( mSupportedDispRes & (0x1 << RES_525P60) ) {
            res = RES_525P60;
        } else if( mSupportedDispRes & (0x1 << RES_525P5994) ) {
            res = RES_525P5994;
        }        
    } 
    LOGD("choose_res_id result = %d", res);
    return res;
}

int HdmiStackService::test_supported_res() {
    int back = mCurrentDispRes;
    for( int i = 0; i < MAX_NUM_RESS; i++ ) {
        if( mSupportedDispRes & (0x1 << i) ) {
            //ok support, test it
            LOGD("******Test res id = %d, please check TV*****", i);
            reconfig_hdmi(i, mNumChannels, mSampleFrequence);
            sleep(10);
            LOGD("************done********************");
        }
    }
    LOGD("**************test_video done**************");
    reconfig_hdmi(back, mNumChannels, mSampleFrequence);
    return 0;
}

int HdmiStackService::test_set_video_res(int res) {
    reconfig_hdmi(res, mNumChannels, mSampleFrequence);
    return 0;
}
}; // end of namespace

