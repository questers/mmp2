/*
* (C) Copyright 2010 Marvell International Ltd.
* All Rights Reserved
*
* MARVELL CONFIDENTIAL
* Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
* The source code contained or described herein and all documents related to
* the source code ("Material") are owned by Marvell International Ltd or its
* suppliers or licensors. Title to the Material remains with Marvell International Ltd
* or its suppliers and licensors. The Material contains trade secrets and
* proprietary and confidential information of Marvell or its suppliers and
* licensors. The Material is protected by worldwide copyright and trade secret
* laws and treaty provisions. No part of the Material may be used, copied,
* reproduced, modified, published, uploaded, posted, transmitted, distributed,
* or disclosed in any way without Marvell's prior express written permission.
*
* No license under any patent, copyright, trade secret or other intellectual
* property right is granted to or conferred upon you by disclosure or delivery
* of the Materials, either expressly, by implication, inducement, estoppel or
* otherwise. Any license under such intellectual property rights must be
* express and approved by Marvell in writing.
*
*/

#ifndef __HDMI_SERVICE_H
#define __HDMI_SERVICE_H

#include <utils/Vector.h>
#include <utils/threads.h>
#include <utils/KeyedVector.h>
#include <utils/String8.h>
#include "IHdmiService.h"
#include "IHdmiServiceCallback.h"
extern "C" {
    #include "hdmi_stack.h"
}

namespace android{
class HdmiStackService : public BnHdmiService, public IBinder::DeathRecipient {
public:
    int get_hdmi_sink_state();
    // 1 : enable hdcp
    // 0 : disable hdcp
    int set_hdcp_state( int state );
    // get hdcp state
    int get_hdcp_state();
    // load hdcp key
//    int load_hdcp_keys(String8 keys);
    
    // configure srm,  support in next version
    // virtual uint32_t config_srm(String8 srm, int *sign_valid, int *vern) = 0;
    
    // get supported display resolution
    int get_supported_display_resolution();
    // get HDMI receiver prefered resolution
    int get_prefered_display_resolution();
    // set display resolution
    int set_display_resolution(int res_id);
    // get current display resolution
    int get_current_display_resolution();
    // set display resolution near
    int set_display_resolution_near(int w, int h);
    // get supported audio format
    int get_supported_audio_format();
    // set audio format
    int set_audio_format(int audio_format);    
    // set audio mute
    int set_audio_mute(int mute);

    int registerCallback(const sp<IHdmiServiceCallback>& callback);
    int unregisterCallback(const sp<IHdmiServiceCallback>& callback);

    int set_display_mode( int mode );

    int get_display_mode();

    int test_supported_res();

    int test_set_video_res(int res);

    int on_event(unsigned int code, void *info);
	
    // internal stactic functions
    static void instantiate();
    
    static HdmiStackService* getInstance();
    
    virtual void binderDied(const wp<IBinder>& who);    
    
   
private:
    // base work item
    class HdmiWorkBase {
        public:
            HdmiWorkBase(HdmiStackService *service) : mService(service){
            }
            virtual ~HdmiWorkBase(){
            }
            virtual void doWork() = 0;
        protected:
            HdmiStackService *mService;
    };

    class ConnWork : public HdmiWorkBase {
        public:
            ConnWork(int state, HdmiStackService *service) : HdmiWorkBase(service), mState(state){
            }
            virtual ~ConnWork() {
            }
            virtual void doWork();
        private:
            int mState;
    };

    class EventCBWork : public HdmiWorkBase{
        public :
            EventCBWork(int event, int detail, sp<IHdmiServiceCallback> cb, HdmiStackService *service)
                : HdmiWorkBase(service) , mEvent(event), mDetail(detail), mCB(cb) {
            }
            virtual ~EventCBWork(){
            }
            virtual void doWork();
            void setCB(sp<IHdmiServiceCallback> cb) {
                mCB = cb;
            }
        private:
            int mEvent;
            int mDetail;
            // if mCB.get() != 0, then just notify mCB, else notify all
            sp<IHdmiServiceCallback> mCB;
    };

    // worker thread
    class HdmiWorkerThread : public Thread {
        public:
            HdmiWorkerThread( HdmiStackService *service ) : mBootCompleted(false){
                this->mService = service;
            }
            virtual ~HdmiWorkerThread(){
            }
        private:
            virtual bool threadLoop();
            HdmiStackService *mService;
            bool mBootCompleted;
    };

    HdmiStackService();
    ~HdmiStackService();

    static int event_cb(unsigned int code, void *info, void *user);

    // 0 : means no callback work item generated
    // 1 : means work item generated
    int process_hdmi_event(unsigned int code);

    int set_audio_mute_l(int mute);

    void init();
    int vpp_init();
    int vpp_destroy();

    int enter_mode(int mode);
    int sink_disconnected();
    int sink_connected();
    int select_prefered_res_id();
    int get_stack_res_id( int id );
    int get_audio_param(int audio_fmt, int *sample_rate, int *channel_count, int *spkrcfg);
    int get_audio_fmt( int sample_rate, int channel_count);

    int is_video_res_supported( int resid);
    int is_audio_fmt_supported( int fmt );

    int reconfig_hdmi(int video_id, int channel_count, int sample_rate);

    void notify_client( int event, int info);

    //int get_spt_af_from_edit(

    uint32_t popCount(uint32_t u);

    int load_hdcp_keys();
    
    int choose_res_id(int w, int h);

    Mutex mWorkItemLock;
    Condition mWorkItemCond;
    Vector<HdmiWorkBase*> mWorkItems;
    sp<Thread> mWorkThread;
    bool mWorkThreadRunning;

    // Protect mCallbacks.
    Mutex   mCallbackLock;
    KeyedVector<wp<IBinder>, sp<IHdmiServiceCallback> >  mCallbacks;    

    static Mutex mInstanceLock;
    static HdmiStackService *mInstance;

    VPP_HDMI_SINK_CAPS mSinkCaps; 

    int mVppHandle;

    int mSinkState;
    int mHdcpState;
    int mPrevHdcpState;
    int mSupportedDispRes;
    int mPreferedDispRes;
    int mCurrentDispRes;
    int mPrevDispRes;
    int mSupportedAudioFmt;
    int mCurrentAudioFmt;
    int mPrevAudioFmt;
    int mAudioMute;
    int mNumChannels;
    int mSampleFrequence;

    int mResWidth;
    int mResHeight;

    /***********************************************************************************************************
     *                                                  state machine
     *                                                  current state
     *                                             DISCONNECTED        NORMAL               MIRROR               HDMI_ONLY            VIDEO
     *                                           ---------------------------------------------------------------------------------
     *              DISCONNECTED     |  DISCONNECTED     DISCONNECTED    DISCONNECTED    DISCONNECTED     DISCONNECTED
     *              CONNECTED          |  MIRROR                  INVALID              INVALID              INVALID                 INVALID
     *  input     NORMAL                 | INVALID                   NORMAL               NORMAL               NORMAL                INVALID
     *              MIRROR                 |  INVALID                  MIRROR                MIRROR               MIRROR                INVALID
     *              HDMI_ONLY            | INVALID                   HDMI_ONLY          HDMI_ONLY          HDMI_ONLY           INVALID
     *              VIDEO                   |  INVALID                  VIDEO                  VIDEO                  VIDEO                  VIDEO
     *
     ************************************************************************************************************/
    static int mStateMachine[EVENT_MAX][DISPLAY_MODE_MAX];
   
    int mMode;
};
}; // end of namespace
#endif

