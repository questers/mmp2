/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */
#ifndef _HDMI_AUDIO_FORMAT_H
#define _HDMI_AUDIO_FORMAT_H
#define HDMI_AUDIO_CHANNEL_MONO            0x10000
#define HDMI_AUDIO_CHANNEL_STEREO         0x20000
#define HDMI_AUDIO_CHANNEL_5POINT1       0x40000
#define HDMI_AUDIO_CHANNEL_7POINT1       0x80000
#define HDMI_AUDIO_CHANNEL_MASK            0xffff0000
#define HDMI_AUDIO_SR_32K                          0x1
#define HDMI_AUDIO_SR_44_1K                      0x2
#define HDMI_AUDIO_SR_48K                          0X4
#define HDMI_AUDIO_SR_64K                          0x8
#define HDMI_AUDIO_SR_88_2K                      0x10
#define HDMI_AUDIO_SR_96K                          0x20
#define HDMI_AUDIO_SR_176_4k                    0x40
#define HDMI_AUDIO_SR_192K                        0X80
#define HDMI_AUDIO_SR_MASK                       0xffff
#endif
