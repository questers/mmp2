/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

 #ifndef _HDMI_VIDEO_FORMAT_H
 #define _HDMI_VIDEO_FORMAT_H
 // keep alignment with hdmi_stack.h
 #define VIDEO_RES_NTSC_M                   0x1
 #define VIDEO_RES_NTSC_J                    0x2
 #define VIDEO_RES_PAL_M                     0x4
 #define VIDEO_RES_PAL_BGH                 0x8
 #define  VIDEO_RES_525I60                   0x10
 #define VIDEO_RES_525I5994                0x20
 #define VIDEO_RES_625I50                    0x40
 #define VIDEO_RES_525P60                   0x80
 #define VIDEO_RES_525P5994               0x100
 #define VIDEO_RES_625P50                   0x200
 #define VIDEO_RES_720P30                   0x400
 #define VIDEO_RES_720P2997               0x800
 #define VIDEO_RES_720P25                   0x1000
 #define VIDEO_RES_720P60                   0x2000
 #define VIDEO_RES_720P5994               0x4000
 #define VIDEO_RES_720P50                   0x8000
 #define VIDEO_RES_1080I60                  0x10000
 #define VIDEO_RES_1080I5994              0x20000
 #define VIDEO_RES_1080I50                  0x40000
 #define VIDEO_RES_1080P30                  0x80000
 #define VIDEO_RES_1080P2997              0x100000
 #define VIDEO_RES_1080P25                  0x200000
 #define VIDEO_RES_1080P24                  0x400000
 #define VIDEO_RES_1080P2398              0x800000
 #define VIDEO_RES_1080P60                  0x1000000
 #define VIDEO_RES_1080P5994              0x2000000
 #define VIDEO_RES_1080P50                  0x4000000
 #define VIDEO_RES_VGA_480P60            0x8000000
 #define VIDEO_RES_VGA_480P5994        0x10000000
 // TODO: define 3D resolution if needed 
 #endif
