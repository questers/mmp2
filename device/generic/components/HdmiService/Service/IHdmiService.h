/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

#ifndef ANDROID_IHDMISERVICE_H
#define ANDROID_IHDMISERVICE_H
#include <utils/Errors.h>  // for status_t
#include <binder/IInterface.h>
#include <binder/Parcel.h>
#include <utils/String8.h>
#include "IHdmiServiceCallback.h"
namespace android{

enum {
    EVENT_SINK_DISCONNECTED = 0,
    EVENT_SINK_CONNECTED,
    EVENT_SET_MODE_NORMAL,
    EVENT_SET_MODE_MIRROR,
    EVENT_SET_MODE_HDMI_ONLY,
    EVENT_SET_MODE_VIDEO,
    EVENT_MAX 
};

enum {
    DISPLAY_MODE_INVALID = -1,
    DISPLAY_MODE_SINK_DISCONNECTED = 0,
    DISPLAY_MODE_NORMAL,
    DISPLAY_MODE_MIRROR,
    DISPLAY_MODE_HDMI_ONLY,
    DISPLAY_MODE_VIDEO,
    DISPLAY_MODE_MAX
};
// must keep same as event definition in hdmi_stack.h
enum {
    HDMI_SINK_CONNECTED = 0,
    HDMI_SINK_DISCONNECTED = 1,
    HDMI_VIDEO_CFG_ERR = 2,
    HDMI_AUDIO_CFG_ERR = 3,
    HDMI_HDCP_ERR = 4
};

class IHdmiService: public IInterface
{
public:
    DECLARE_META_INTERFACE(HdmiService);
    // 1 : hdmi sink connected
    // 0 : hdmi sink disconnected
    virtual int get_hdmi_sink_state() = 0;
    // 1 : enable hdcp
    // 0 : disable hdcp
    virtual int set_hdcp_state( int state ) = 0;
    // get hdcp state
    virtual int get_hdcp_state() = 0;
    // load hdcp key
    //virtual int load_hdcp_keys(String8 keys) = 0;
    
    // configure srm,  support in next version
    // virtual uint32_t config_srm(String16 srm, int *sign_valid, int *vern) = 0;
    
    // get supported display resolution
    virtual int get_supported_display_resolution() = 0;
    // get HDMI receiver prefered resolution
    virtual int get_prefered_display_resolution() = 0;
    // set display resolution
    virtual int set_display_resolution(int res_id) = 0;
    // get current display resolution
    virtual int get_current_display_resolution() = 0;
    // set display resolution near
    virtual int set_display_resolution_near(int w, int h) = 0;
    // get supported audio format
    virtual int get_supported_audio_format() = 0;
    // set audio format
    virtual int set_audio_format(int audio_format) = 0;
    // mute/unmute audio
    virtual int set_audio_mute(int mute) = 0;

    // set display mode
    virtual int set_display_mode( int mode) = 0;
    // get display mode
    virtual int get_display_mode() = 0;
    
    virtual int registerCallback(const sp<IHdmiServiceCallback>& callback) = 0;
    virtual int unregisterCallback(const sp<IHdmiServiceCallback>& callback) = 0;

    // use for test
    virtual int test_supported_res() = 0;
    virtual int test_set_video_res(int res) = 0;
    
};

// ----------------------------------------------------------------------------

class BnHdmiService: public BnInterface<IHdmiService>
{
public:
    virtual status_t    onTransact( uint32_t code,
                                    const Parcel& data,
                                    Parcel* reply,
                                    uint32_t flags = 0);
};

}; //namespace end

#endif
