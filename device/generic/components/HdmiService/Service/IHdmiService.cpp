/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

#include <stdint.h>
#include <sys/types.h>
#define LOG_TAG "IHdmiService"
#include <utils/Log.h>
#include <binder/Parcel.h>
#include <binder/IMemory.h>

#include <utils/Errors.h>  // for status_t

#include "IHdmiService.h"
#include "IHdmiServiceCallback.h"
namespace android {

enum {
    GET_SINK_STATE = IBinder::FIRST_CALL_TRANSACTION,
    SET_HDCP_STATE,
    GET_HDCP_STATE,
//    LOAD_HDCP_KEYS,
    GET_SPT_DISPLAY_RES,
    GET_PREF_DISPLAY_RES,
    SET_DISPLAY_RES,
    GET_CURR_DISPLAY_RES,
    SET_DISPLAY_RES_NEAR,
    GET_SPT_AUDIO_FMT,
    SET_AUDIO_FMT,
    SET_AUDIO_MUTE,
    SET_DISPLAY_MODE,
    GET_DISPLAY_MODE,
    REGISTER_CALLBACK,
    UNREGISTER_CALLBACK,
    TEST_SUPPORTED_VIDEOS = 30,
    TEST_SET_RES = 31,
};

class BpHdmiService: public BpInterface<IHdmiService>
{
    public:
        BpHdmiService(const sp<IBinder>& impl)
            : BpInterface<IHdmiService>(impl)
        {
        }

        virtual int get_hdmi_sink_state() {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            int ret = remote()->transact(GET_SINK_STATE, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int set_hdcp_state(int state) {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            data.writeInt32(state);
            remote()->transact(SET_HDCP_STATE, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int get_hdcp_state() {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            remote()->transact(GET_HDCP_STATE, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }
/*
        virtual int load_hdcp_keys(String8 keys) {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            data.writeString8(keys);
            remote()->transact(SET_HDCP_STATE, data, &reply);
            return reply.readInt32();
        }
*/
        virtual int get_supported_display_resolution() {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            remote()->transact(GET_SPT_DISPLAY_RES, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int get_prefered_display_resolution() {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            remote()->transact(GET_PREF_DISPLAY_RES, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int set_display_resolution(int res_id) {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            data.writeInt32(res_id);
            remote()->transact(SET_DISPLAY_RES, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int get_current_display_resolution() {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            remote()->transact(GET_CURR_DISPLAY_RES, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int set_display_resolution_near(int w, int h) {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            data.writeInt32(w);
            data.writeInt32(h);
            remote()->transact(SET_DISPLAY_RES_NEAR, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int get_supported_audio_format() {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            remote()->transact(GET_SPT_AUDIO_FMT, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int set_audio_format(int audio_format) {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            data.writeInt32(audio_format);
            remote()->transact(SET_AUDIO_FMT, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int set_audio_mute(int mute) {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            data.writeInt32(mute);
            remote()->transact(SET_AUDIO_MUTE, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int set_display_mode(int mode) {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            data.writeInt32(mode);
            remote()->transact(SET_DISPLAY_MODE, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int get_display_mode() {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            remote()->transact(GET_DISPLAY_MODE, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }
        
        virtual int registerCallback(const sp<IHdmiServiceCallback>& callback){
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            data.writeStrongBinder(callback->asBinder());
            remote()->transact(REGISTER_CALLBACK, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();     
        }

        virtual int unregisterCallback(const sp<IHdmiServiceCallback>& callback) {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            data.writeStrongBinder(callback->asBinder());
            remote()->transact(UNREGISTER_CALLBACK, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int test_supported_res() {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            remote()->transact(TEST_SUPPORTED_VIDEOS, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }

        virtual int test_set_video_res(int res) {
            Parcel data, reply;
            data.writeInterfaceToken(IHdmiService::getInterfaceDescriptor());
            data.writeInt32(res);
            remote()->transact(TEST_SET_RES, data, &reply);
            // keep align with java implementation
            reply.readInt32();
            return reply.readInt32();
        }
};

//Notice, here the name should be same as IAdditionService.java -- "com.Addition.IHdmiService"
IMPLEMENT_META_INTERFACE(HdmiService, "com.marvell.hdmimanager.IHdmiService");

// ----------------------------------------------------------------------

status_t BnHdmiService::onTransact(
    uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags)
{
    switch(code) {
        case GET_SINK_STATE: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int result = get_hdmi_sink_state();
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
        } break;
        case SET_HDCP_STATE: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int state = data.readInt32();
            int result = set_hdcp_state(state);
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
            
        } break;
        case GET_HDCP_STATE: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int result = get_hdcp_state();
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
            
        } break;
/*        
        case LOAD_HDCP_KEYS: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            String8 keys = data.readString8();
            int result = load_hdcp_keys(keys);
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
        } break;
*/        
        case GET_SPT_DISPLAY_RES: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int result = get_supported_display_resolution();
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
        } break;
        case GET_PREF_DISPLAY_RES: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int result = get_prefered_display_resolution();
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
            
        } break;
        case SET_DISPLAY_RES: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int displayres = data.readInt32();
            int result = set_display_resolution(displayres);
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
            
        } break;
        case GET_CURR_DISPLAY_RES: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int result = get_current_display_resolution();
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
            
        } break;
        case SET_DISPLAY_RES_NEAR: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int w = data.readInt32();
            int h = data.readInt32();
            int result = set_display_resolution_near(w,h);
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
            
        } break;
        case GET_SPT_AUDIO_FMT: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int result = get_supported_audio_format();
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
            
        } break;
        case SET_AUDIO_FMT: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int fmt = data.readInt32();
            int result = set_audio_format(fmt);
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
            
        } break;
        case SET_AUDIO_MUTE: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int fmt = data.readInt32();
            int result = set_audio_mute(fmt);
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
            
        } break;
        case SET_DISPLAY_MODE: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int mode = data.readInt32();
            int result = set_display_mode(mode);
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
            
        } break;        
        case GET_DISPLAY_MODE: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int result = get_display_mode();
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
            
        } break;        
        case REGISTER_CALLBACK: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            sp<IHdmiServiceCallback> callback = interface_cast<IHdmiServiceCallback>(data.readStrongBinder());
            int result = registerCallback(callback);
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;

        } break;
        case UNREGISTER_CALLBACK: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            sp<IHdmiServiceCallback> callback = interface_cast<IHdmiServiceCallback>(data.readStrongBinder());
            int result = unregisterCallback(callback);
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;

        } break;
        case TEST_SUPPORTED_VIDEOS: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int result = test_supported_res();
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;

        } break;
        case TEST_SET_RES: {
            CHECK_INTERFACE(IHdmiService, data, reply);
            int res = data.readInt32();
            int result = test_set_video_res(res);
            // write no exception
            reply->writeInt32(0);
            reply->writeInt32(result);
            return NO_ERROR;
            
        } break;        
        default:
            return BBinder::onTransact(code, data, reply, flags);
    }
}

}

