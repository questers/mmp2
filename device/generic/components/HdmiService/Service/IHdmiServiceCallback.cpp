/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

#define LOG_TAG "HdmiServiceCallback"
#include <utils/Log.h>
#include <binder/Parcel.h>
#include <IHdmiServiceCallback.h>

#include <utils/Errors.h>
namespace android {
enum{
    PROCESS_EVENT = IBinder::FIRST_CALL_TRANSACTION,
};

class BpHdmiServiceCallback : public BpInterface<IHdmiServiceCallback>
{
public:
    BpHdmiServiceCallback(const sp<IBinder>& impl)
        : BpInterface<IHdmiServiceCallback>(impl){
    }
    virtual void process_event(int eventCode, int reserved){
        Parcel data, reply;
        data.writeInterfaceToken(IHdmiServiceCallback::getInterfaceDescriptor());
        data.writeInt32(eventCode);
        data.writeInt32(reserved);
        remote()->transact(PROCESS_EVENT, data, &reply);
        return;
    }
};

IMPLEMENT_META_INTERFACE(HdmiServiceCallback, "com.marvell.hdmimanager.IHdmiServiceCallback");

// ----------------------------------------------------------------------
status_t BnHdmiServiceCallback::onTransact(
    uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags)
{
    switch(code) {
        case PROCESS_EVENT:{
            CHECK_INTERFACE(IHdmiServiceCallback, data, reply);
            int code = data.readInt32();
            int reserved = data.readInt32();
            process_event(code, reserved);
            // write no exception
            reply->writeInt32(0);
            return NO_ERROR;
        } break;            
        default:
            return BBinder::onTransact(code, data, reply, flags);
    }
}

};// namespace end

