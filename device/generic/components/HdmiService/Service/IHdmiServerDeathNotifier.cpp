/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "IHdmiServerDeathNotifier"
#include <utils/Log.h>

#include <binder/IServiceManager.h>
#include <binder/IPCThreadState.h>
#include "IHdmiServerDeathNotifier.h"

namespace android {

// client singleton for binder interface to services
Mutex IHdmiServerDeathNotifier::sServiceLock;
sp<IHdmiService> IHdmiServerDeathNotifier::sHdmiService;
sp<IHdmiServerDeathNotifier::DeathNotifier> IHdmiServerDeathNotifier::sDeathNotifier;
SortedVector< wp<IHdmiServerDeathNotifier> > IHdmiServerDeathNotifier::sObitRecipients;

// establish binder interface to HdmiService
/*static*/const sp<IHdmiService>&
IHdmiServerDeathNotifier::getHdmiService()
{
    LOGV("getHdmiService");
    Mutex::Autolock _l(sServiceLock);
    if (sHdmiService.get() == 0) {
        sp<IServiceManager> sm = defaultServiceManager();
        sp<IBinder> binder;
        do {
            binder = sm->getService(String16("HdmiService"));
            if (binder != 0) {
                break;
             }
             LOGW("hdmi service not published, waiting...");
             usleep(500000); // 0.5 s
        } while(true);

        if (sDeathNotifier == NULL) {
        sDeathNotifier = new DeathNotifier();
    }
    binder->linkToDeath(sDeathNotifier);
    sHdmiService = interface_cast<IHdmiService>(binder);
    }
    LOGE_IF(sHdmiService == 0, "no hdmi service!?");
    return sHdmiService;
}

/*static*/ void
IHdmiServerDeathNotifier::addObitRecipient(const wp<IHdmiServerDeathNotifier>& recipient)
{
    LOGV("addObitRecipient");
    Mutex::Autolock _l(sServiceLock);
    sObitRecipients.add(recipient);
}

/*static*/ void
IHdmiServerDeathNotifier::removeObitRecipient(const wp<IHdmiServerDeathNotifier>& recipient)
{
    LOGV("removeObitRecipient");
    Mutex::Autolock _l(sServiceLock);
    sObitRecipients.remove(recipient);
}

void
IHdmiServerDeathNotifier::DeathNotifier::binderDied(const wp<IBinder>& who) {
    LOGW("hdmi server died");

    // Need to do this with the lock held
    SortedVector< wp<IHdmiServerDeathNotifier> > list;
    {
        Mutex::Autolock _l(sServiceLock);
        sHdmiService.clear();
        list = sObitRecipients;
    }

    // Notify application when media server dies.
    // Don't hold the static lock during callback in case app
    // makes a call that needs the lock.
    size_t count = list.size();
    for (size_t iter = 0; iter < count; ++iter) {
        sp<IHdmiServerDeathNotifier> notifier = list[iter].promote();
        if (notifier != 0) {
            notifier->died();
        }
    }
}

IHdmiServerDeathNotifier::DeathNotifier::~DeathNotifier()
{
    LOGV("DeathNotifier::~DeathNotifier");
    Mutex::Autolock _l(sServiceLock);
    sObitRecipients.clear();
    if (sHdmiService != 0) {
        sHdmiService->asBinder()->unlinkToDeath(this);
    }
}

}; // namespace android
