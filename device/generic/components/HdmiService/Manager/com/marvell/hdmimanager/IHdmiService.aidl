/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

package com.marvell.hdmimanager;
import com.marvell.hdmimanager.IHdmiServiceCallback;

// Declare the interface.
interface IHdmiService {
    // 1 : enable
    // 0 : disable
    int get_hdmi_sink_state();
    // 1 : enable hdcp
    // 0 : disable hdcp
    int  set_hdcp_state( int  state );
    // get hdcp state
    int  get_hdcp_state();
    // load hdcp key
    //int  load_hdcp_keys(String keys);
    // configure srm, support in next version
    // virtual uint32_t config_srm(const char* srm, int *sign_valid, int *vern) = 0;
    // get supported display resolution
    int  get_supported_display_resolution();
    // get HDMI receiver prefered resolution
    int  get_prefered_display_resolution();
    // set int  resolution
    int  set_display_resolution(int res_id);
    // get current display resolution
    int  get_current_display_resolution();
    // set display resolution near
    int  set_display_resolution_near(int w, int h);
    // get supported audio format
    int  get_supported_audio_format();
    // set audio format
    int  set_audio_format(int audio_format);
    // set audio mute
    int set_audio_mute(int mute);
    // set display mode
    int set_display_mode(int mode);
    // get display mode
    int get_display_mode();

    int registerCallback(IHdmiServiceCallback callback);
    int unregisterCallback(IHdmiServiceCallback callback);

}

