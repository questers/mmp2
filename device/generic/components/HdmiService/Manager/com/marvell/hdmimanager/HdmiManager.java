/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

package com.marvell.hdmimanager;

import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.IBinder;
import android.os.ServiceManager;
import android.util.Log;
import android.os.Handler;
import android.os.Message;

import java.util.ArrayList;

public class HdmiManager {
    public static final int HDMI_AUDIO_CHANNEL_MONO      = 0x10000;
    public static final int HDMI_AUDIO_CHANNEL_STEREO    = 0x20000;
    public static final int HDMI_AUDIO_CHANNEL_5POINT1   = 0x40000;
    public static final int HDMI_AUDIO_CHANNEL_7POINT1   = 0x80000;
    public static final int HDMI_AUDIO_CHANNEL_MASK      = 0xffff0000;
    public static final int HDMI_AUDIO_SR_32K             = 0x1;
    public static final int HDMI_AUDIO_SR_44_1K            = 0x2;
    public static final int HDMI_AUDIO_SR_48K             = 0x4;
    public static final int HDMI_AUDIO_SR_64K          = 0x8;
    public static final int HDMI_AUDIO_SR_88_2K            = 0x10;
    public static final int HDMI_AUDIO_SR_96K            = 0x20;
    public static final int HDMI_AUDIO_SR_176_4K        = 0x40;
    public static final int HDMI_AUDIO_SR_192K               = 0x80;
    public static final int HDMI_AUDIO_SR_MASK           = 0xffff;
    public static final int VIDEO_RES_NTSC_M             = 0x1;
    public static final int VIDEO_RES_NTSC_J             = 0x2;
    public static final int VIDEO_RES_PAL_M              = 0x4;
    public static final int VIDEO_RES_PAL_BGH            = 0x8;
    public static final int VIDEO_RES_525I60             = 0x10;
    public static final int VIDEO_RES_525I5994           = 0x20;
    public static final int VIDEO_RES_625I50             = 0x40;
    public static final int VIDEO_RES_525P60             = 0x80;
    public static final int VIDEO_RES_525P5994           = 0x100;
    public static final int VIDEO_RES_625P50             = 0x200;
    public static final int VIDEO_RES_720P30             = 0x400;
    public static final int VIDEO_RES_720P2997           = 0x800;
    public static final int VIDEO_RES_720P25             = 0x1000;
    public static final int VIDEO_RES_720P60             = 0x2000;
    public static final int VIDEO_RES_720P5994           = 0x4000;
    public static final int VIDEO_RES_720P50             = 0x8000;
    public static final int VIDEO_RES_1080I60            = 0x10000;
    public static final int VIDEO_RES_1080I5994          = 0x20000;
    public static final int VIDEO_RES_1080I50            = 0x40000;
    public static final int VIDEO_RES_1080P30            = 0x80000;
    public static final int VIDEO_RES_1080P2997          = 0x100000;
    public static final int VIDEO_RES_1080P25            = 0x200000;
    public static final int VIDEO_RES_1080P24            = 0x400000;
    public static final int VIDEO_RES_1080P2398          = 0x800000;
    public static final int VIDEO_RES_1080P60            = 0x1000000;
    public static final int VIDEO_RES_1080P5994          = 0x2000000;
    public static final int VIDEO_RES_1080P50            = 0x4000000;
    public static final int VIDEO_RES_VGA_480P60         = 0x8000000;
    public static final int VIDEO_RES_VGA_480P5994       = 0x10000000;

    public static final int DISPLAY_MODE_SINK_DISCONNECTED = 0;
    public static final int DISPLAY_MODE_NORMA = 1;
    public static final int DISPLAY_MODE_MIRROR = 2;
    public static final int DISPLAY_MODE_HDMI_ONLY = 3;
    public static final int DISPLAY_MODE_VIDEO = 4;

    public static final int HDMI_SINK_CONNECTED = 0;
    public static final int HDMI_SINK_DISCONNECTED = 1;
    public static final int HDMI_VIDEO_CFG_ERR = 2;
    public static final int HDMI_AUDIO_CFG_ERR = 3;
    public static final int HDMI_HDCP_ERR = 4;

    private IHdmiService mHdmiService;
    private static final String TAG = "HdmiManager";  
    private ArrayList<HdmiListener> mListeners = new ArrayList<HdmiListener>();
    public HdmiManager(){
        mHdmiService = IHdmiService.Stub.asInterface(
                ServiceManager.getService("HdmiService"));
        if (mHdmiService == null){
            Log.d(TAG, "mHdmiService == NULL");
            return;
        }else{
            Log.d(TAG, "Got HdmiService instance.");
        }        
        try {
            Log.d(TAG, "mHdmiService.registerCallback(mCallback)");
            mHdmiService.registerCallback(mCallback);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when registerCallback.");
            return;
        }        
        
    }

    @Override
    protected void finalize() throws Throwable {
        mHdmiService = IHdmiService.Stub.asInterface(
                ServiceManager.getService("HdmiService"));
        if (mHdmiService == null){
            Log.d(TAG, "mHdmiService == NULL");
        }else{
            Log.d(TAG, "Got HdmiService instance.");
        }        
        try {
            Log.d(TAG, "mHdmiService.unregisterCallback(mCallback)");
            mHdmiService.unregisterCallback(mCallback);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when unregisterCallback.");
        }        

        super.finalize();
    }

    public int GetHdmiSinkState() {
        int ret =0;
        try {
            Log.d(TAG, "mHdmiService.get_hdmi_sink_state()");
            ret = mHdmiService.get_hdmi_sink_state();
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when get_hdmi_sink_state.");
            return -1;
        }
        return ret;
    }

    public int SetHdcpState(int state) {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.get_hdmi_sink_state("+state+")");
            ret = mHdmiService.set_hdcp_state(state);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when get_hdmi_sink_state.");
            return -1;
        }        
        return ret;
    }

    public int GetHdcpState() {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.get_hdcp_state()");
            ret = mHdmiService.get_hdcp_state();
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when get_hdcp_state.");
            return -1;
        }                
        return ret;
    }
/*
    public int LoadHdcpKeys(String keys) {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.load_hdcp_keys("+keys+")");
            ret = mHdmiService.load_hdcp_keys(keys);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when load_hdcp_keys.");
            return 0;
        }                
        
        return ret;
    }
*/    
    public int GetSupportedDisplayResolution() {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.get_supported_display_resolution()");
            ret = mHdmiService.get_supported_display_resolution();
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when get_supported_display_resolution.");
            return -1;
        }                        
        return ret;
    }

    public int GetPreferedDisplayResolution() {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.get_prefered_display_resolution()");
            ret = mHdmiService.get_prefered_display_resolution();
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when get_prefered_display_resolution.");
            return -1;
        }                        
        return ret;
    }

    public int SetDisplayResolution( int resId) {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.set_display_resolution("+resId+")");
            ret = mHdmiService.set_display_resolution(resId);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when set_display_resolution.");
            return -1;
        }                
        
        return ret;
    }    

    public int GetCurrentDisplayResolution() {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.get_current_display_resolution()");
            ret = mHdmiService.get_current_display_resolution();
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when get_current_display_resolution.");
            return -1;
        }                        
        return ret;
    }

    public int SetDisplayResolutionNear( int w, int h) {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.set_display_resolution_near("+w+"X"+h+")");
            ret = mHdmiService.set_display_resolution_near(w,h);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when set_display_resolution_near.");
            return -1;
        }                
        
        return ret;
    }

    public int GetSupportedAudioFormat() {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.get_supported_audio_format()");
            ret = mHdmiService.get_supported_audio_format();
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when get_supported_audio_format.");
            return -1;
        }                        
        return ret;
    }    

    public int SetAudioFormat(int param) {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.set_audio_format("+param+")");
            ret = mHdmiService.set_audio_format(param);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when set_audio_format.");
            return -1;
        }                        
        return ret;
    }

    public int SetAudioMute(int param) {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.set_audio_mute("+param+")");
            ret = mHdmiService.set_audio_mute(param);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when set_audio_mute.");
            return -1;
        }                        
        return ret;
    }

    public int SetDisplayMode( int mode ) {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.set_display_mode("+mode+")");
            ret = mHdmiService.set_display_mode(mode);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when set_display_mode.");
            return -1;
        }                        
        return ret;
    }

    public int GetDisplayMode() {
        int ret = 0;
        try {
            Log.d(TAG, "mHdmiService.get_display_mode()");
            ret = mHdmiService.get_display_mode();
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.on
            Log.d(TAG, "Got exception when get_display_mode.");
            return -1;
        }                        
        return ret;        
    }
    
    public int RegisterListener(HdmiListener listener) {
        synchronized(mListeners) {
            if( !mListeners.contains(listener) ) {
                mListeners.add(listener);
            }
        }
        return 0;
    }

    public int UnRegisterListener(HdmiListener listener) {
        synchronized(mListeners) {
            mListeners.remove(listener);
        }
        return 0;
    }


    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg){
            switch (msg.what) {
                case M_PROCESS_EVENT:
                    synchronized(mListeners) {
                        for( HdmiListener l : mListeners) {
                            l.onEvent(msg.arg1, msg.arg2);
                        }
                        break;
                    }
                default:
                    super.handleMessage(msg);
            }
        }  
    };  

    private static final int M_PROCESS_EVENT = 1;

    private IHdmiServiceCallback mCallback = new IHdmiServiceCallback.Stub() {
        /**
         * This is called by the remote service regularly to tell us about
         * new values.  Note that IPC calls are dispatched through a thread
         * pool running in each process, so the code executing here will
         * NOT be running in our main thread like most other things -- so,
         * to update the UI, we need to use a Handler to hop over there.
         */
        public void process_event(int eventCode, int reserved) {
            Message msg = mHandler.obtainMessage(M_PROCESS_EVENT);
            msg.arg1 = eventCode;
            msg.arg2 = reserved;
            mHandler.sendMessage(msg);
            return;
        }
    };
    

}
