LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
    atchannel.c \
    pstn.c \
    ril-core.c \
    ril-call.c\
    ril-network.c\
    ril-sim.c\
    ril-device.c


LOCAL_SHARED_LIBRARIES := \
    libcutils libutils libril

# for asprinf
LOCAL_CFLAGS := -D_GNU_SOURCE -DRIL_SHLIB \
	-Wno-format-security 

LOCAL_LDLIBS := -lpthread

LOCAL_C_INCLUDES := $(KERNEL_HEADERS)

LOCAL_PRELINK_MODULE := false

  #build shared library
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE:= libpstn_ril
include $(BUILD_SHARED_LIBRARY)


#
#pstn execute
#
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
    atchannel.c \
    pstn.c \
    ril-core.c \
    ril-call.c\
    ril-network.c\
    ril-sim.c\
    ril-device.c

#build executable
LOCAL_MODULE:= pstn
LOCAL_MODULE_TAGS := tests
LOCAL_LDLIBS := -lpthread
LOCAL_SHARED_LIBRARIES := \
    libcutils libutils libril
LOCAL_CFLAGS := -D_GNU_SOURCE -Wno-format-security 

include $(BUILD_EXECUTABLE)

#
#test
#
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	test_pstn.c
	
#build executable
LOCAL_MODULE:= test_pstn
LOCAL_MODULE_TAGS := tests
LOCAL_LDLIBS := -lpthread
LOCAL_SHARED_LIBRARIES := \
    libcutils libutils libril
LOCAL_CFLAGS := -D_GNU_SOURCE -Wno-format-security 

include $(BUILD_EXECUTABLE)


