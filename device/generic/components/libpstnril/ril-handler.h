#ifndef _RIL_HANDLER_H_
#define _RIL_HANDLER_H_

#include "atchannel.h"
#include "ril-core.h"
#include "pstn.h"

//
//call handling
//
void requestHangupWaitingOrBackground(void *data, size_t datalen,
                                      RIL_Token t);
void requestHangupForegroundResumeBackground(void *data, size_t datalen,
                                             RIL_Token t);
void requestSwitchWaitingOrHoldingAndActive(void *data, size_t datalen,
                                            RIL_Token t);
void requestConference(void *data, size_t datalen, RIL_Token t);
void requestSeparateConnection(void *data, size_t datalen, RIL_Token t);
void requestExplicitCallTransfer(void *data, size_t datalen, RIL_Token t);
void requestUDUB(void *data, size_t datalen, RIL_Token t);
void requestSetMute(void *data, size_t datalen, RIL_Token t);
void requestGetMute(void *data, size_t datalen, RIL_Token t);
void requestLastCallFailCause(void *data, size_t datalen, RIL_Token t);
void requestGetCurrentCalls(void *data, size_t datalen, RIL_Token t);
void requestDial(void *data, size_t datalen, RIL_Token t);
void requestAnswer(void *data, size_t datalen, RIL_Token t);
void requestHangup(void *data, size_t datalen, RIL_Token t);
void requestDTMF(void *data, size_t datalen, RIL_Token t);
void requestDTMFStart(void *data, size_t datalen, RIL_Token t);
void requestDTMFStop(void *data, size_t datalen, RIL_Token t);
void requestCDMADTMF(void *data, size_t datalen, RIL_Token t);

//
//SMS
//
void requestSendSMS(void *data, size_t datalen, RIL_Token t);
void requestSendSMSExpectMore(void *data, size_t datalen, RIL_Token t);
void requestSMSAcknowledge(void *data, size_t datalen, RIL_Token t);
void requestWriteSmsToSim(void *data, size_t datalen, RIL_Token t);
void requestDeleteSmsOnSim(void *data, size_t datalen, RIL_Token t);
void requestGetSMSCAddress(void *data, size_t datalen, RIL_Token t);
void requestSetSMSCAddress(void *data, size_t datalen, RIL_Token t);
void requestGSMGetBroadcastSMSConfig(void *data, size_t datalen, RIL_Token t);
void requestGSMSetBroadcastSMSConfig(void *data, size_t datalen, RIL_Token t);
void requestGSMSMSBroadcastActivation(void *data, size_t datalen, RIL_Token t);

void requestCDMASendSMS(void *data, size_t datalen, RIL_Token t);
void requestCDMASMSAcknowledge(void *data, size_t datalen, RIL_Token t);
void CheckSMSStorage(void);

//
//network
//
extern RIL_RadioState sState;
extern pthread_mutex_t s_state_mutex;
extern pthread_cond_t s_state_cond;

void setRadioState(RIL_RadioState newState);
int isRadioOn();

void requestSetNetworkSelectionAutomatic(void *data, size_t datalen,
                                         RIL_Token t);
void requestSetNetworkSelectionManual(void *data, size_t datalen,
                                      RIL_Token t);
void requestQueryAvailableNetworks(void *data, size_t datalen,
                                   RIL_Token t);
void requestSetPreferredNetworkType(void *data, size_t datalen,
                                    RIL_Token t);
void requestGetPreferredNetworkType(void *data, size_t datalen,
                                    RIL_Token t);
void requestQueryNetworkSelectionMode(void *data, size_t datalen,
                                      RIL_Token t);
void requestSignalStrength(void *data, size_t datalen, RIL_Token t);
void requestRegistrationState(void *data,size_t datalen, RIL_Token t);

void requestGPRSRegistrationState(void *data, size_t datalen, RIL_Token t);

void requestOperator(void *data, size_t datalen, RIL_Token t);
void requestRadioPower(void *data, size_t datalen, RIL_Token t);
void requestSetLocationUpdates(void *data, size_t datalen, RIL_Token t);
void requestCDMASubScription(void *data, size_t datalen, RIL_Token t);
void requestQueryFacilityLock(void *data, size_t datalen, RIL_Token t);
void requestSetFacilityLock(void *data, size_t datalen, RIL_Token t);
int rssi2dbm(int rssi,int evdo);


//
//Device
//

void requestGetIMSI(void *data, size_t datalen, RIL_Token t);
void requestGetIMEI(void *data, size_t datalen, RIL_Token t);
void requestGetIMEISV(void *data, size_t datalen, RIL_Token t);
void requestDeviceIdentity(void *data, size_t datalen, RIL_Token t);
void requestBasebandVersion(void *data, size_t datalen, RIL_Token t);

int is_ztemt_device();
int is_qualcomm_device();

//
//PDP
//
int  pdp_init(void);
void pdp_uninit(void);

void requestOrSendPDPContextList(RIL_Token *t);
void onPDPContextListChanged(void *param);
void requestPDPContextList(void *data, size_t datalen, RIL_Token t);
void requestSetupDefaultPDP(void *data, size_t datalen, RIL_Token t);
void requestDeactivateDefaultPDP(void *data, size_t datalen, RIL_Token t);
void requestLastPDPFailCause(void *data, size_t datalen, RIL_Token t);
void requestScreenState(void *data, size_t datalen, RIL_Token t);


void pdp_maintain_ping();

void pdp_deactivate(void);

//
//Service
//
void requestQueryClip(void *data, size_t datalen, RIL_Token t);
void requestCancelUSSD(void *data, size_t datalen, RIL_Token t);
void requestSendUSSD(void *data, size_t datalen, RIL_Token t);
void requestGetCLIR(void *data, size_t datalen, RIL_Token t);
void requestSetCLIR(void *data, size_t datalen, RIL_Token t);
void requestQueryCallForwardStatus(void *data, size_t datalen,
                                   RIL_Token t);
void requestSetCallForward(void *data, size_t datalen, RIL_Token t);
void requestQueryCallWaiting(void *data, size_t datalen, RIL_Token t);
void requestSetCallWaiting(void *data, size_t datalen, RIL_Token t);
void requestSetSuppSvcNotification(void *data, size_t datalen,
                                   RIL_Token t);

void onSuppServiceNotification(const char *s, int type);
void onUSSDReceived(const char *s);

//
//SIM
//
typedef enum {
    SIM_ABSENT = 0,
    SIM_NOT_READY = 1,
    SIM_READY = 2, /* SIM_READY means the radio state is RADIO_STATE_SIM_READY */
    SIM_PIN = 3,
    SIM_PUK = 4,
    SIM_NETWORK_PERSONALIZATION = 5
} SIM_Status; 


void onSimStateChanged(const char *s);
void onSIMReady();

void requestGetSimStatus(void *data, size_t datalen, RIL_Token t);
void requestSIM_IO(void *data, size_t datalen, RIL_Token t);
void requestEnterSimPin(void *data, size_t datalen, RIL_Token t);
void requestChangeSimPin(void *data, size_t datalen, RIL_Token t);
void requestSetFacilityLock(void *data, size_t datalen, RIL_Token t);
void requestQueryFacilityLock(void *data, size_t datalen, RIL_Token t);

void pollSIMState(void *param);
SIM_Status getSIMStatus(int falce);


//
//OEM hook
//
void requestOEMHookRaw(void *data, size_t datalen, RIL_Token t);
void requestOEMHookStrings(void *data, size_t datalen, RIL_Token t);

//
//STK
//
void requestReportSTKServiceIsRunning(void *data, size_t datalen, RIL_Token t);
void requestSTKGetProfile(void *data, size_t datalen, RIL_Token t);
void requestSTKSetProfile(void *data, size_t datalen, RIL_Token t);
void requestSTKSendEnvelopeCommand(void *data, size_t datalen, RIL_Token t);
void requestSTKSendTerminalResponse(void *data, size_t datalen, RIL_Token t);



#endif
