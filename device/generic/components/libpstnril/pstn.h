#ifndef _PSTN_H
#define _PSTN_H


int pstn_hardware_init(void);


#define STATE_ON_HOOK 	0
#define STATE_OFF_HOOK 	1

//0 == onhook,1 == offhook
int pstn_handle_state(void);

//0 == onhook,1 == offhook
int pstn_hook_state(void);

//0 == onhook 1 == offhook
int pstn_set_hook(int hook);


//
//functions
//

enum
{
	kFuncInvalid			=	0xff,
	kFuncCall				=	0x00,
	
	//incoming
	kFuncRing				=	0x01,
	kFuncRingEnd			=	0x02,
	kFuncExtOffHook 		= 	0x03,
	kFuncExtOnHook			=	0x04,
	kFuncSecretCall			=	0x05,
	kFuncCallEnd			=	0x06,
	kFuncExtOffHookHolding	=	0x20,

	//outcoming
	kFuncFlash				= 	0x3C,
	kFuncPause				=	0x3D,
	kFuncHoldOn				=	0x3E,
	kFuncHoldOff			=	0x3F,
	kFuncSetFlashTime100MS	=	0x20,
	kFuncSetFlashTime300MS	=	0x21,
	kFuncSetFlashTime600MS	=	0x22,
	kFuncIncSpeakerVolume	=	0x23,
	kFuncDecSpeakerVolume	=	0x24,
	kFuncTelephonyAvailable	=	0x25,
	kFuncDialingToneStart	=	0x26,
	kFuncDialingToneStop	=	0x27,
	kFuncSetPresetDialing	=	0x28,
	kFuncUnsetPresetDialing	=	0x29,
	


	//internal use
	kDigit0					=	0x30,
	kDigit1 				=	0x31,
	kDigit2 				=	0x32,
	kDigit3 				=	0x33,
	kDigit4 				=	0x34,
	kDigit5 				=	0x35,
	kDigit6 				=	0x36,
	kDigit7 				=	0x37,
	kDigit8 				=	0x38,
	kDigit9 				=	0x39,
	kDigitAsterisk			=	0x3A,
	kDigitPound				=	0x3B,
	kDigitEscape			=	0x0E,
	kDigitDelimiter			=	0x0F,
};

struct pstn_func
{
	int func;
	union
	{
		char buffer[128];//
		char number[64];//for call number		
	};
};


struct call_entity
{
	char number[64];
};

//
//return value is bytes consumed,<=0 means parse error
//
int pstn_parse_function(struct pstn_func* func,unsigned  char* buf,int len);

//return vlaue is bytes consumed ,<=0 means error
int pstn_setup_function(struct pstn_func* func,unsigned char* buf,int len);

//
//turnkey pstn buffer 
//return: 0 success,otherwise failed
//  
int pstn_buffer_turnkey(unsigned char* buf,int len);




#endif

