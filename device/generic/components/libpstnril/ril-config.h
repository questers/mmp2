#ifndef _RIL_CONFIG_H_
#define _RIL_CONFIG_H_

#include <telephony/ril.h>

//-------------system---------

#define RIL_IMEISV_VERSION  1
#define RIL_BASEBAND_VERSION "Questers PSTN BaseBand"
#define RIL_DRIVER_VERSION "PSTN PSTN RIL VER 0.1"
#define MAX_AT_RESPONSE (1024)


#endif //_ZTEMT_RIL_CONFIG_H

