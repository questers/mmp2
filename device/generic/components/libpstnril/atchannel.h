#ifndef ATCHANNEL_H
#define ATCHANNEL_H 1

#include "pstn.h"
#ifdef __cplusplus
extern "C" {
#endif


#define AT_ERROR_GENERIC -1
#define AT_ERROR_COMMAND_PENDING -2
#define AT_ERROR_CHANNEL_CLOSED -3
#define AT_ERROR_TIMEOUT -4
#define AT_ERROR_INVALID_THREAD -5 

typedef void (*ATUnsolHandler)(struct pstn_func* func);

int at_open(int fd, ATUnsolHandler h);
void at_close();

/* This callback is invoked on the command thread.
   You should reset or handshake here to avoid getting out of sync */
void at_set_on_timeout(void (*onTimeout)(void));
/* This callback is invoked on the reader thread (like ATUnsolHandler)
   when the input stream closes before you call at_close
   (not when you call at_close())
   You should still call at_close()
   It may also be invoked immediately from the current thread if the read
   channel is already closed */
void at_set_on_reader_closed(void (*onClose)(void));


int at_handshake();

int at_send_command ( unsigned char *command,int len);

#ifdef __cplusplus
}
#endif

#endif /*ATCHANNEL_H*/
