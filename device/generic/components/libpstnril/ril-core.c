#include <telephony/ril.h>
#include <telephony/ril_cdma_sms.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <alloca.h>
#include <getopt.h>
#include <sys/socket.h>
#include <termios.h>
#include <cutils/properties.h>

#include "atchannel.h"
#include "ril-handler.h"
#include "request_entry.h"



extern const char * requestToString(int request);


const struct RIL_Env *s_rilenv;


//local flags
unsigned int	s_flags = (RIL_FLAG_ERROR|RIL_FLAG_WARN
							|RIL_FLAG_DEBUG|RIL_FLAG_INFO);

//service port
static int 				s_port = -1;
static int          	s_device_socket = 0;
//static 
char * 			s_service_port = NULL;
static char* 			s_def_service_port = "/dev/ttyS3"; /*UART4*/

int   s_cdma_device = 0;

unsigned int   radio_state_not_ready;
unsigned int   radio_state_ready;
unsigned int   radio_state_locked_or_absent;
unsigned int   ril_apptype;
unsigned int   ril_persosubstate_network;

PST_RIL_HARDWARE rilhw;		

/* trigger change to this with s_state_cond */
int s_closed = 0;

static const struct timeval TIMEVAL_0 = {0,0};


static void sendCallStateChanged(void *param)
{
    RIL_onUnsolicitedResponse (
        RIL_UNSOL_RESPONSE_CALL_STATE_CHANGED,
        NULL, 0);
}


/*** Callback methods from the RIL library to us ***/

/**
 * Call from RIL to us to make a RIL_REQUEST
 *
 * Must be completed with a call to RIL_onRequestComplete()
 *
 * RIL_onRequestComplete() may be called from any thread, before or after
 * this function returns.
 *
 * Will always be called from the same thread, so returning here implies
 * that the radio is ready to process another command (whether or not
 * the previous command has completed).
 */
static void
onRequest (int request, void *data, size_t datalen, RIL_Token t)
{
	int err = RIL_E_REQUEST_NOT_SUPPORTED;

	DBG("onRequest: [%d]:%s",request,requestToString(request));

    /* Ignore all requests except RIL_REQUEST_GET_SIM_STATUS
     * when RADIO_STATE_UNAVAILABLE.
     */
    if (sState == RADIO_STATE_UNAVAILABLE
        &&!( request == RIL_REQUEST_GET_SIM_STATUS)
    ) {
	 	err = RIL_E_RADIO_NOT_AVAILABLE;
		goto ERROR_HANDLER;
    }

    /* Ignore all non-power requests when RADIO_STATE_OFF
     * (except RIL_REQUEST_GET_SIM_STATUS)
     */
    if (sState == RADIO_STATE_OFF
        && !(request == RIL_REQUEST_RADIO_POWER
            || request == RIL_REQUEST_GET_SIM_STATUS
            ||request ==  RIL_REQUEST_DEACTIVATE_DATA_CALL)
    ) {
		 err = RIL_E_RADIO_NOT_AVAILABLE;
		 goto ERROR_HANDLER;
    }

	PRIL_REQUEST pRequest=RIL_DISP_TABLE;
	while(pRequest)
	{
		if((pRequest->request == request)||
			!pRequest->request)
			break;
		pRequest++;
	}

	if(pRequest->disp)
	{
		pRequest->disp(data,datalen,t);
		err = RIL_E_SUCCESS;
	}
   
ERROR_HANDLER:
	if(err)
	{
		//WARN("request[%s] can't be supported currently",requestToString(request));
		RIL_onRequestComplete(t, err, NULL, 0);
	}
		
}

/**
 * Synchronous call from the RIL to us to return current radio state.
 * RADIO_STATE_UNAVAILABLE should be the initial state.
 */
static RIL_RadioState
onStateRequest()
{
    return sState;
}
/**
 * Call from RIL to us to find out whether a specific request code
 * is supported by this implementation.
 *
 * Return 1 for "supported" and 0 for "unsupported"
 */

static int
onSupports (int requestCode)
{
    //@@@todo
    return 1;
}

static void onCancel (RIL_Token t)
{
    //@@@todo

}

static const char * getVersion(void)
{
    return RIL_DRIVER_VERSION;
}


static int rilhw_init(void)
{
	static ST_RIL_HARDWARE pstn_hw;
	PST_RIL_HARDWARE p_rilhw=&pstn_hw;
	if(!rilhw)
	{	
		rilhw = p_rilhw;
		p_rilhw->handle_state = pstn_handle_state();
		p_rilhw->hook_state = pstn_hook_state();
		p_rilhw->pstn_call.number[0] = '\0';
		
		p_rilhw->call.index = -1;
		p_rilhw->call.toa = 0;	//type of address,not available
		p_rilhw->call.isMpty = 0;
		p_rilhw->call.als = 0;
		p_rilhw->call.isVoice = 1;
		p_rilhw->call.isVoicePrivacy = 0;
		p_rilhw->call.numberPresentation = 0;//allowed;
		p_rilhw->call.name = NULL;
		p_rilhw->call.uusInfo = NULL;

		//below items will be filled by GetCurrentCall
		//call.index
		//call.number 
		
		
	}
	radio_state_not_ready = RADIO_STATE_SIM_NOT_READY;
	radio_state_ready = RADIO_STATE_SIM_READY;
	radio_state_locked_or_absent = RADIO_STATE_SIM_LOCKED_OR_ABSENT;
	ril_apptype = RIL_APPTYPE_SIM;
	ril_persosubstate_network = RIL_PERSOSUBSTATE_SIM_NETWORK;
	
	return 0;
}


void requestOEMHookRaw(void *data, size_t datalen, RIL_Token t)
{
	char* bytes = data;	

	at_send_command(bytes,1);
    /* Echo back data */
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
}


/**
 * Initialize everything that can be configured while we're still in
 * AT+CFUN=0
 */
static void initializeCallback(void *param)
{
	setRadioState (RADIO_STATE_OFF);

	setRadioState (radio_state_not_ready);
}

static void waitForClose()
{
    pthread_mutex_lock(&s_state_mutex);

    while (s_closed == 0) {
        pthread_cond_wait(&s_state_cond, &s_state_mutex);
    }

    pthread_mutex_unlock(&s_state_mutex);
}

static const char* func_name(int code)
{
	if(kFuncCall==code) return "kFuncCall";
	if(kFuncRing==code) return "kFuncRing";
	if(kFuncRingEnd==code) return "kFuncRingEnd";
	if(kFuncExtOffHook==code) return "kFuncExtOffHook";
	if(kFuncExtOnHook==code) return "kFuncExtOnHook";
	if(kFuncSecretCall==code) return "kFuncSecretCall";
	if(kFuncCallEnd==code) return "kFuncCallEnd";
	if(kFuncExtOffHookHolding==code) return "kFuncExtOffHookHolding";
	
	if(kFuncFlash==code) return "kFuncFlash";
	if(kFuncPause==code) return "kFuncPause";
	if(kFuncHoldOn==code) return "kFuncHoldOn";
	if(kFuncHoldOff==code) return "kFuncHoldOff";
	if(kFuncSetFlashTime100MS==code) return "kFuncSetFlashTime100MS";
	if(kFuncSetFlashTime300MS==code) return "kFuncSetFlashTime300MS";
	if(kFuncSetFlashTime600MS==code) return "kFuncSetFlashTime600MS";
	if(kFuncIncSpeakerVolume==code) return "kFuncIncSpeakerVolume";
	if(kFuncDecSpeakerVolume==code) return "kFuncDecSpeakerVolume";
	if(kFuncTelephonyAvailable==code) return "kFuncTelephonyAvailable";
	if(kFuncDialingToneStart==code) return "kFuncDialingToneStart";
	if(kFuncDialingToneStop==code) return "kFuncDialingToneStop";
	if(kFuncSetPresetDialing==code) return "kFuncSetPresetDialing";
	if(kFuncUnsetPresetDialing==code) return "kFuncUnsetPresetDialing";

	//fall back
	return "unknown";
}

/**
 * Called by atchannel when an unsolicited line appears
 * This is called on atchannel's reader thread. AT commands may
 * not be issued here
 */
static void onUnsolicited (struct pstn_func* func)
{
    int err;

    /* Ignore unsolicited responses until we're initialized.
     * This is OK because the RIL library will poll for initial state
     */
    if (sState == RADIO_STATE_UNAVAILABLE) {
		DBG("onUnsolicited radio not available now\n");
        return;
    }
	switch(func->func)
	{
		case kFuncRing:
		{
			rilhw->call.index = 1;
			rilhw->call.state = RIL_CALL_INCOMING;
			rilhw->call.number = NULL;
			RIL_onUnsolicitedResponse (
			RIL_UNSOL_RESPONSE_CALL_STATE_CHANGED,
			NULL, 0);						
		}
		break;
		case kFuncCall:
		{
			rilhw->call.state = RIL_CALL_INCOMING;
			strcpy(rilhw->pstn_call.number,func->number);
			RIL_onUnsolicitedResponse (
			RIL_UNSOL_RESPONSE_CALL_STATE_CHANGED,
			NULL, 0);			
		}
		break;
		default:
		{
			WARN("unsolicited function %s not handled\n",func_name(func->func));
		}
		break;
	}
	
	
}

/* Called on command or reader thread */
static void onATReaderClosed()
{
    INFO("AT channel closed\n");
    at_close();
    s_closed = 1;

    setRadioState (RADIO_STATE_UNAVAILABLE);
}

/* Called on command thread */
static void onATTimeout()
{
    INFO("AT channel timeout; closing\n");
    at_close();

    s_closed = 1;

    /* FIXME cause a radio reset here */

    setRadioState (RADIO_STATE_UNAVAILABLE);
}

static void usage(char *s)
{
    fprintf(stderr, "pstn-ril requires: -d /dev/tty_device [-f flag]\n");
}

static int open_tty_port(char* tty_path)
{
	int fd;
	struct termios	ios;

	fd = open(tty_path, O_RDWR | O_NOCTTY);

	if(fd<0)
		return fd;	

	tcgetattr( fd, &ios );	
	
	ios.c_cflag = B1200 | CS8 | CLOCAL | CREAD;/*databit8 ,receive enable*/
	
	ios.c_iflag = IGNPAR|IGNBRK;
	ios.c_iflag &=~(ICRNL);
	//ios.c_iflag = IGNPAR;/*ignore parity,no flow control*/
	ios.c_oflag = 0;
	ios.c_lflag = 0;  /* disable ECHO, ICANON, etc... */
	tcsetattr (fd, TCSANOW, &ios);
	return fd;
	
}


static void *
mainLoop(void *param)
{
    int fd;
    int ret;

    DBG("entering mainLoop()");


	pthread_detach(pthread_self());		
	
    at_set_on_reader_closed(onATReaderClosed);
    at_set_on_timeout(onATTimeout);
	pstn_hardware_init();
    for (;;) {
find_rilhw:
        fd = -1;
        while  (fd < 0) 
		{
			if (s_service_port != NULL) 
			{
                fd = open_tty_port(s_service_port);
            }

            if (fd < 0) {
                ERROR ("opening AT interface. retrying after 10s... ");				
                sleep(10);
				goto find_rilhw;
                /* never returns */
            }  
        }

	    DBG ("open AT interface success! \n");

		rilhw_init();	
        s_closed = 0;
        ret = at_open(fd,onUnsolicited);

        if (ret < 0) {
            ERROR ("AT error %d on at_open\n", ret);
            return 0;
        }

        RIL_requestTimedCallback(initializeCallback, NULL, &TIMEVAL_0);

        // Give initializeCallback a chance to dispatched, since
        // we don't presently have a cancellation mechanism
        sleep(1);
        waitForClose();
        DBG("Re-opening after close");
    }
}


/*** Static Variables ***/
static const RIL_RadioFunctions s_callbacks = {
    RIL_VERSION,
    onRequest,
    onStateRequest,
    onSupports,
    onCancel,
    getVersion
};



#ifdef RIL_SHLIB
static pthread_t s_tid_mainloop;
const RIL_RadioFunctions *RIL_Init(const struct RIL_Env *env, int argc, char **argv)
{
    int ret;
    int fd = -1;
    int opt;

    s_rilenv = env;

    while ( -1 != (opt = getopt(argc, argv, "p:d:s:f:"))) {
        switch (opt) {
            case 'p':
                s_port = atoi(optarg);
                if (s_port == 0) {
                    usage(argv[0]);
                    return NULL;
                }
                DBG("Opening loopback port %d\n", s_port);
            break;

            case 'd':
                s_service_port = optarg;
                DBG("Opening tty device %s\n", s_service_port);
            break;

            case 's':
                s_service_port   = optarg;
                s_device_socket = 1;
                DBG("Opening socket %s\n", s_service_port);
            break;

			case 'f':
				s_flags = atoi(optarg);
				//always turn on error
				s_flags |= RIL_FLAG_ERROR;
				DBG("Debug flag change to x%08x\n",s_flags);
            break;
            
            default:
                usage(argv[0]);
                return NULL;
        }
    }

	if(!s_service_port)
	{
		s_service_port = s_def_service_port;
		WARN("use default AT port [%s]\n",s_service_port);
	}    

    
    if (s_port < 0 && s_service_port == NULL) {
        usage(argv[0]);
        return NULL;
    }
	

    ret = pthread_create(&s_tid_mainloop, NULL, mainLoop, NULL);

    return &s_callbacks;
}

#else
int main(int argc,char** argv)
{
    int ret;
    int fd = -1;
    int opt;

    while ( -1 != (opt = getopt(argc, argv, "p:d:s:f:"))) {
        switch (opt) {
            case 'p':
                s_port = atoi(optarg);
                if (s_port == 0) {
                    usage(argv[0]);
                    return -1;
                }
                DBG("Opening loopback port %d\n", s_port);
            break;

            case 'd':
                s_service_port = optarg;
                DBG("Opening tty device %s\n", s_service_port);
            break;

            case 's':
                s_service_port   = optarg;
                s_device_socket = 1;
                DBG("Opening socket %s\n", s_service_port);
            break;

			case 'f':
				s_flags = atoi(optarg);
				//always turn on error
				s_flags |= RIL_FLAG_ERROR;
				DBG("Debug flag change to x%08x\n",s_flags);
            break;
            
            default:
                usage(argv[0]);
                return -1;
        }
    }

	if(!s_service_port)
	{
		s_service_port = s_def_service_port;
		WARN("use default AT port [%s]\n",s_service_port);
	}    

    if (s_port < 0 && s_service_port == NULL) {
        usage(argv[0]);
        return -1;
    }
	
    //RIL_register(&s_callbacks);

	mainLoop(NULL);
	return 0;
}
#endif

