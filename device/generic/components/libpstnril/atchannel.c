#include <telephony/ril.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include <alloca.h>
#include "atchannel.h"
#include "pstn.h"
#include "ril-core.h"

#define NUM_ELEMS(x) (sizeof(x)/sizeof(x[0]))

static pthread_t s_tid_reader;
static int s_fd = -1;    /* fd of the AT channel */
static ATUnsolHandler s_unsolHandler;

/* for input buffering */

static unsigned char s_ATBuffer[MAX_AT_RESPONSE+1];
static unsigned char *s_ATBufferCur = s_ATBuffer;
static unsigned char *s_ATBufferEnd = s_ATBuffer;
/*
 * for current pending command
 * these are protected by s_commandmutex
 */

static pthread_mutex_t commandmutex = PTHREAD_MUTEX_INITIALIZER;


static void (*s_onTimeout)(void) = NULL;
static void (*s_onReaderClosed)(void) = NULL;
static int s_readerClosed;


static void processLine(unsigned char *line,int len)
{
	struct pstn_func func;
	int ret;

	ret = pstn_parse_function(&func,line,len);
	if(ret!=len)
	{
		if(ret<=0)
		{			
			WARN("pstn parsing error[%d]\n",ret);
			return;
		}
		else
		{
			WARN("pstn parsing %d bytes but %d gotten\n",ret,len);
		}
	}
	
    if (s_unsolHandler != NULL) {
        s_unsolHandler(&func);
    }
	
}

static inline void debug_data(const char *function, int size,
					 unsigned char *data)
{
#define isprint(c)	(c>='!'&&c<='~')
//((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'))
	char* ptr;
	char digits[2048] = {0};
	int i, j;	
	unsigned char *buf = (unsigned char*)data;
	DBG("%s - length = %d\n",
			   function, size);

	
	for (i=0; i<size; i+=16) 
	{
	  ptr = &digits[0];
	  ptr+=sprintf(ptr,"%06x: ",i);
	  for (j=0; j<16; j++) 
		if (i+j < size)
		 ptr+=sprintf(ptr,"%02x ",buf[i+j]);
		else
		 ptr+=sprintf(ptr,"%s","   ");

	  ptr+=sprintf(ptr,"%s","  ");
		
	  for (j=0; j<16; j++) 
		if (i+j < size)			
			ptr+=sprintf(ptr,"%c",isprint(buf[i+j]) ? buf[i+j] : '.');
	  *ptr='\0';
	  DBG("%s\n",digits);
	}
}

static unsigned char * findNextEOL(unsigned char *cur,unsigned char *end)
{

	// Find next newline
	while (cur < end)
	{
		if (*cur == kFuncRing ||
			*cur == kFuncRingEnd||
			*cur == kFuncExtOffHook||
			*cur == kFuncExtOnHook||
			*cur == kFuncSecretCall||
			*cur == kFuncCallEnd||
			*cur == kFuncExtOffHookHolding) 
		{
			cur++;
			return cur;
		}
		else if(*cur==kDigitEscape)
		{
			cur++;
			return cur;
		}
		cur++;
	} 

	return NULL;
}

static unsigned char *tryALine()
{
	unsigned char *p_eol = NULL;
	if(s_ATBufferEnd>s_ATBufferCur)
	{
		p_eol=findNextEOL(s_ATBufferCur,s_ATBufferEnd);
		if (p_eol == NULL) 
		{
			size_t len;
			/* a partial line. move it up and prepare to read more */				
			len = s_ATBufferEnd-s_ATBufferCur;

			memmove(s_ATBuffer, s_ATBufferCur, len);
			s_ATBufferEnd = s_ATBuffer + len;
			s_ATBufferCur = s_ATBuffer;
		}
	}
	return p_eol;
}


/**
 * Reads a line from the AT channel, returns NULL on timeout.
 * Assumes it has exclusive read access to the FD
 *
 * This line is valid only until the next call to readline
 *
 * This function exists because as of writing, android libc does not
 * have buffered stdio.
 */

static int readline(unsigned char **line,int* len)
{
	int count;
	unsigned char *p_eol = NULL;
	unsigned char *ret=NULL;

	p_eol=tryALine();

	while (p_eol == NULL) 
	{
		if (0 == MAX_AT_RESPONSE - (s_ATBufferEnd - s_ATBuffer)) 
		{
			ERROR("ERROR: Input line exceeded buffer\n");
			/* ditch buffer and start over again */
			s_ATBufferCur = s_ATBufferEnd = s_ATBuffer;
		}

		do {
			count = read(s_fd, s_ATBufferEnd,
			MAX_AT_RESPONSE - (s_ATBufferEnd - s_ATBuffer));
		} while (count < 0 && errno == EINTR);
		DBG("read bytes count = %d\n",count);
		if (count > 0) 
		{
			//turnkey buffer
			pstn_buffer_turnkey(s_ATBufferEnd,count);
			
			//debug_data("AT RAW",count,(unsigned char*)s_ATBufferEnd);
			
			s_ATBufferEnd += count;
			p_eol=tryALine();
		} 
		else
		{
			/* read error encountered or EOF reached */
			if(count == 0) 
			{
				DBG("atchannel: EOF reached");
			} 
			else 
			{
				DBG("atchannel: read error %s", strerror(errno));
			}
			return -1;
		}
	}

	//change legacy 
	*line = s_ATBufferCur;
	*len = p_eol-s_ATBufferCur;
	
	s_ATBufferCur = p_eol; 
	if(s_ATBufferCur==s_ATBufferEnd)
	{
		s_ATBufferCur = s_ATBufferEnd = s_ATBuffer;
	}

	debug_data("AT < ",*len,*line);

	return 0;
}


static void onReaderClosed()
{
    if (s_onReaderClosed != NULL && s_readerClosed == 0) {
        s_readerClosed = 1;
        s_onReaderClosed();
    }
}


static void *readerLoop(void *arg)
{
	int ret;
    for (;;) {
        unsigned char * line;
		int len;

        ret = readline(&line,&len);

        if (ret) {
            break;
        }

        processLine(line,len);

    }

    onReaderClosed();

    return NULL;
}

/**
 * Sends string s to the radio with a \r appended.
 * Returns AT_ERROR_* on error, 0 on success
 *
 * This function exists because as of writing, android libc does not
 * have buffered stdio.
 */
static int writeline ( unsigned char *s,int len)
{
    int cur = 0;
    ssize_t written;

    if (s_fd < 0 || s_readerClosed > 0) {
        return AT_ERROR_CHANNEL_CLOSED;
    }


	debug_data("AT>",len,(unsigned char*)s);

	//turnkey buffer
	pstn_buffer_turnkey(s,len);
    /* the main string */
    while (cur < len) {
        do {
            written = write (s_fd, s + cur, 1/*len - cur*/);
        } while (written < 0 && errno == EINTR);

        if (written < 0) {
            return AT_ERROR_GENERIC;
        }

        cur += written;
		//delay 1s for each command
		usleep(1000*1000);
    }


    return 0;
}

/**
 * Starts AT handler on stream "fd'
 * returns 0 on success, -1 on error
 */
int at_open(int fd, ATUnsolHandler h)
{
    int ret;
    pthread_t tid;
    pthread_attr_t attr;

    s_fd = fd;
    s_unsolHandler = h;
    s_readerClosed = 0;


    pthread_attr_init (&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    ret = pthread_create(&s_tid_reader, &attr, readerLoop, &attr);

    if (ret < 0) {
        perror ("pthread_create");
        return -1;
    }


    return 0;
}


void at_close()
{
    if (s_fd >= 0) {
        close(s_fd);
    }
    s_fd = -1;

    s_readerClosed = 1;
}


/**
 * Internal send_command implementation
 * Doesn't lock or call the timeout callback
 *
 * timeoutMsec == 0 means infinite timeout
 */

static int at_send_command_full_nolock ( unsigned char *command,int len,long long timeoutMsec)
{
    int err = 0;

    err = writeline (command,len);

    if (err < 0) {
        goto error;
    }

    if(s_readerClosed > 0) {
        err = AT_ERROR_CHANNEL_CLOSED;
        goto error;
    }

    err = 0;
error:

    return err;
}

/**
 * Internal send_command implementation
 *
 * timeoutMsec == 0 means infinite timeout
 */
static int at_send_command_full ( unsigned char *command,int len)
{
    int err;

    if (0 != pthread_equal(s_tid_reader, pthread_self())) {
        /* cannot be called from reader thread */
        return AT_ERROR_INVALID_THREAD;
    }
    pthread_mutex_lock(&commandmutex);

    err = at_send_command_full_nolock(command,len,0);

    pthread_mutex_unlock(&commandmutex);

    if (err == AT_ERROR_TIMEOUT && s_onTimeout != NULL) {
        s_onTimeout();
    }

    return err;
}


int at_send_command (unsigned char *command,int len)
{
    return at_send_command_full (command,len);
}


/** This callback is invoked on the command thread */
void at_set_on_timeout(void (*onTimeout)(void))
{
    s_onTimeout = onTimeout;
}

/**
 *  This callback is invoked on the reader thread (like ATUnsolHandler)
 *  when the input stream closes before you call at_close
 *  (not when you call at_close())
 *  You should still call at_close()
 */

void at_set_on_reader_closed(void (*onClose)(void))
{
    s_onReaderClosed = onClose;
}


/**
 * Periodically issue an AT command and wait for a response.
 * Used to ensure channel has start up and is active
 */
int at_handshake()
{
    int i;
    int err = 0;

    if (0 != pthread_equal(s_tid_reader, pthread_self())) {
        /* cannot be called from reader thread */
        return AT_ERROR_INVALID_THREAD;
    }

    return err;
}

