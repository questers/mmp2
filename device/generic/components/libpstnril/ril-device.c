#include <stdio.h>
#include <string.h>

#include "ril-handler.h"

static char* def_IMEI="352037030152447";
static char* def_IMSI="460030912121001";

void requestGetIMSI(void *data, size_t datalen, RIL_Token t)
{
	RIL_onRequestComplete(t, RIL_E_SUCCESS,
						  def_IMSI,
						  sizeof(char *));
	
}

/* Deprecated */
/**
 * RIL_REQUEST_GET_IMEI
 *
 * Get the device IMEI, including check digit.
*/

void requestGetIMEI(void *data, size_t datalen, RIL_Token t)
{
	RIL_onRequestComplete(t, RIL_E_SUCCESS,
						  def_IMEI,
						  sizeof(char *));
}

void requestGetIMEISV(void *data, size_t datalen, RIL_Token t)
{
	char* response;
    // IMEISV 
    asprintf(&response, "%d", RIL_IMEISV_VERSION);

	RIL_onRequestComplete(t, RIL_E_SUCCESS,
						  response,
						  sizeof(char *));
	free(response);
	
}

/**
 * RIL_REQUEST_BASEBAND_VERSION
 *
 * Return string value indicating baseband version, eg
 * response from AT+CGMR.
*/

void requestBasebandVersion(void *data, size_t datalen, RIL_Token t)
{
	RIL_onRequestComplete(t, RIL_E_SUCCESS,
						  RIL_BASEBAND_VERSION,
						  sizeof(char *));
}

