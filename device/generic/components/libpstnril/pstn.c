#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "pstn.h"
#include "atchannel.h"
#include "ril-core.h"

#define PSTN_MODEM_PATH "/sys/devices/platform/pstn_modem/pstn_mode"
#define PSTN_MODEM_HANDLE_STATE_PATH "/sys/devices/platform/pstn_modem/pstn_handle_state"
#define PSTN_MODEM_HOOK_STATE_PATH "/sys/devices/platform/pstn_modem/pstn_hook_state"
#define PSTN_MODEM_LINKSTATE_PATH "/sys/devices/platform/pstn_modem/pstn_link_state"
#define PSTN_MODEM_SCREENSTATE_PATH "/sys/devices/platform/pstn_modem/pstn_screen_state"


static int readIntFromFile( const char* file, int* pResult )
{
	int fd = -1;
	fd = open( file, O_RDONLY );
	if( fd >= 0 ){
		char buf[20];
		int rlt = read( fd, buf, sizeof(buf) );
		if( rlt > 0 ){
			buf[rlt] = '\0';
			*pResult = atoi(buf);
			return 0;
		}
	}
	return -1;
}

static int writeIntToFile(const char* path,int value)
{
	int fd = open( path, O_RDWR );
	int ret=-1;
	if( fd >= 0 ){
		char buf[48];
		int len;
		len=sprintf(buf,"%d\n",value);
		len = write( fd, buf, strlen(buf));
		if( len > 0 ){
			ret = 0;
		}
	}

	return ret;
}

struct pstn_monitor_param
{
	int dummy;
};

static const struct timeval TIMEVAL_0 = {0,0};
static int handle_new_state;
static void handleStateChangeCallback(void *param)
{
	unsigned char command=0;
	int state = (int)param;
	if(STATE_ON_HOOK==state)
	{
		//command =kFuncDialingToneStop;
		//at_send_command(&command,1);
	}
	else if(STATE_OFF_HOOK==state)
	{
		command = kFuncDialingToneStart;
		at_send_command(&command,1);		
	}

}
static void *pstn_hardware_monitor(void* param)
{
	struct pstn_monitor_param* monitor_param = param;
	int old_state,state;
	old_state = pstn_handle_state();
    for (;;) {
		state = pstn_handle_state();
		if(old_state!=state)
		{
			DBG("handle state changed %d->%d\n",old_state,state);	
			handle_new_state = state;
			RIL_requestTimedCallback(handleStateChangeCallback, (void*)handle_new_state, &TIMEVAL_0);
			old_state = state;
		}
		// 1 second monitor ,enough for us
		usleep(200*1000);
    }


	return NULL;
}
//setup a simple thread to monitor handle state
int pstn_hardware_init(void)
{	
	static pthread_t s_tid_pstn=-1;
	static struct pstn_monitor_param param;

	if(-1==s_tid_pstn)
	{
		int ret;
		pthread_attr_t attr;		
		pthread_attr_init (&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
		
		ret = pthread_create(&s_tid_pstn, &attr, pstn_hardware_monitor, &param);
		if (ret < 0) {
			perror ("pthread_create");
			return -1;
		}
		
	}

    return 0;

}


//0 == onhook,1 == offhook
int pstn_handle_state(void)
{
	int state;
	if(readIntFromFile(PSTN_MODEM_HANDLE_STATE_PATH,&state)<0)
	{
		state = 0;
	}
	else if(state<0)
	{
		state=0;
	}

	return state;

}

//0 == onhook,1 == offhook
int pstn_hook_state(void)
{
	int state;
	if(readIntFromFile(PSTN_MODEM_HOOK_STATE_PATH,&state)<0)
	{
		state = 0;
	}
	else if(state<0)
	{
		state=0;
	}

	return state;
	
}

//0 == onhook 1 == offhook
int pstn_set_hook(int hook)
{
	DBG("set hksi state->%d\n",hook);
	return writeIntToFile(PSTN_MODEM_HOOK_STATE_PATH,hook);

}


int pstn_parse_function(struct pstn_func* func,unsigned char* buf,int len)
{
	int func_expect=kFuncInvalid;
	int call_digits=0;
	unsigned char* ptr;;
	int parsed=0;
	int digitDelimiter=0;
	if(!buf||!func) return -1;
	func->func = kFuncInvalid;

	ptr = buf;

	while(parsed<len)
	{
		if(kFuncInvalid == func->func && kFuncCall!=func_expect)
		{
			switch(ptr[parsed])
			{
				case 	kFuncRing:
				case 	kFuncRingEnd:
				case 	kFuncExtOffHook:
				case 	kFuncExtOnHook:
				case 	kFuncSecretCall:
				case 	kFuncCallEnd:
				{
					func->func = ptr[parsed];		
				}
				break;
				case	kDigit0:
				case	kDigit1:
				case	kDigit2:
				case	kDigit3:
				case	kDigit4:
				case	kDigit5:
				case	kDigit6:
				case	kDigit7:
				case	kDigit8:
				case	kDigit9:				
				{
					func_expect = kFuncCall;
					func->number[call_digits++] = ptr[parsed];
				}
				break;
				default:
					WARN("%s unkown command 0x%x\n",__func__,ptr[parsed]);
					
			}

			parsed++;
		}

		if(kFuncInvalid != func->func )
			break;

		if(kFuncCall==func_expect)
		{
			if(kDigitEscape!=ptr[parsed]&&kDigitDelimiter!=ptr[parsed]&&0==digitDelimiter)
			{
				func->number[call_digits++] = ptr[parsed];
			}
			else
			{
				if(kDigitDelimiter==ptr[parsed])
				{
					digitDelimiter=1;
				}
				else if(kDigitEscape==ptr[parsed])
				{
					//digits escaped
					func->number[call_digits] = '\0';
					func->func = kFuncCall;
				}
			}
			parsed++;
		}

		
		
	}

	if(kFuncInvalid == func->func&&kFuncCall ==func_expect )
	{
		WARN("incoming function not completed");
	}


	return parsed;
	
}

int pstn_setup_function(struct pstn_func* func,unsigned char* buf,int len)
{
	int eat=0;
	int call_digits=0;
	
	if(!func||!buf)
		return -1;
	switch(func->func)
	{		
		case kFuncFlash	:
		case kFuncPause	:
		case kFuncHoldOn :
		case kFuncHoldOff :
		case kFuncSetFlashTime100MS	:
		case kFuncSetFlashTime300MS	:
		case kFuncSetFlashTime600MS	:
		case kFuncIncSpeakerVolume	:
		case kFuncDecSpeakerVolume	:
		case kFuncTelephonyAvailable :
		case kFuncDialingToneStart	:
		case kFuncDialingToneStop	:
		case kFuncSetPresetDialing	:
		case kFuncUnsetPresetDialing :
		{
			buf[eat++] = func->func;
		}
		break;
		case kFuncCall:
		{
			while(eat<len)
			{
				if('\0'==func->number[call_digits])
				{
					//number terminated
					break;
				}
				else if(func->number[call_digits]<=0x39&&func->number[call_digits]>=0x30)
					buf[eat++] = func->number[call_digits++];
				else if('*'==func->number[call_digits])
				{
					buf[eat++] = kDigitAsterisk;call_digits++;
				}
				else if('#'==func->number[call_digits])
				{
					buf[eat++] = kDigitPound;call_digits++;
				}
				
			}
		}
		break;
		default:
			{
				WARN("%s unkown func %d\n",__func__,func->func);
			}
			break;
	}

	return eat;
}


static inline unsigned char reverseChar(unsigned char c)
{
  c=(c&0x55)<<1 | (c&0xAA)>>1;
  c=(c&0x33)<<2 | (c&0xCC)>>2;
  c=(c&0x0F)<<4 | (c&0xF0)>>4;
  return c;
}
int pstn_buffer_turnkey(unsigned char* buf,int len)
{
	int step=0;
	while(step<len)
	{
		buf[step]=reverseChar(buf[step]);
		step++;
	}
	return 0;
}


