#ifndef _RIL_CORE_H_
#define _RIL_CORE_H_


#include "ril-debug.h"
#include "ril-config.h"

typedef struct
{
	int handle_state;
	int hook_state;

	struct call_entity pstn_call;

	RIL_Call call;	
	
}ST_RIL_HARDWARE,*PST_RIL_HARDWARE;

extern int s_closed;
#ifdef RIL_SHLIB
extern const struct RIL_Env *s_rilenv;
#define RIL_onRequestComplete(t, e, response, responselen) s_rilenv->OnRequestComplete(t,e, response, responselen)
#define RIL_onUnsolicitedResponse(a,b,c) s_rilenv->OnUnsolicitedResponse(a,b,c)
#define RIL_requestTimedCallback(a,b,c) s_rilenv->RequestTimedCallback(a,b,c)
#else
#define RIL_onRequestComplete(t, e, response, responselen) do{}while(0)
#define RIL_onUnsolicitedResponse(a,b,c) do{}while(0) 
#define RIL_requestTimedCallback(a,b,c) do{}while(0)
#endif

extern char* s_service_port;
extern char* s_modem_port;

extern unsigned int   radio_state_not_ready;
extern unsigned int   radio_state_ready;
extern unsigned int   radio_state_locked_or_absent;
extern unsigned int   ril_apptype;
extern unsigned int   ril_persosubstate_network;

extern PST_RIL_HARDWARE rilhw;

#endif
