#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <alloca.h>
#include <getopt.h>
#include <sys/socket.h>
#include <termios.h>

#include "ril-debug.h"
//local flags
unsigned int	s_flags = (RIL_FLAG_ERROR|RIL_FLAG_WARN
							|RIL_FLAG_DEBUG|RIL_FLAG_INFO);

//service port
static int 				s_port = -1;
static int          	s_device_socket = 0;
//static 
char * 			s_service_port = NULL;
static char* 			s_def_service_port = "/dev/ttyS3"; /*UART4*/


static void usage(char *s)
{
    fprintf(stderr, "pstn-ril requires: -d /dev/tty_device [-f flag]\n");
}

static int open_tty_port(char* tty_path)
{
	int fd;
	struct termios	ios;

	fd = open(tty_path, O_RDWR | O_NOCTTY);

	if(fd<0)
		return fd;	

	tcgetattr( fd, &ios );	
	
	ios.c_cflag = B1200 | CS8 | CLOCAL | CREAD;/*databit8 ,receive enable*/
	
	ios.c_iflag = IGNPAR|IGNBRK;
	ios.c_iflag &=~(ICRNL);
	//ios.c_iflag = IGNPAR;/*ignore parity,no flow control*/
	ios.c_oflag = 0;
	ios.c_lflag = 0;  /* disable ECHO, ICANON, etc... */
	tcsetattr (fd, TCSANOW, &ios);
	return fd;
	
}

static inline void debug_data(const char *function, int size,
					 const unsigned char *data)
{
#define isprint(c)	(c>='!'&&c<='~')
//((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'))
	char* ptr;
	char digits[2048] = {0};
	int i, j;	
	unsigned char *buf = (unsigned char*)data;
	DBG("%s - length = %d\n",
			   function, size);

	
	for (i=0; i<size; i+=16) 
	{
	  ptr = &digits[0];
	  ptr+=sprintf(ptr,"%06x: ",i);
	  for (j=0; j<16; j++) 
		if (i+j < size)
		 ptr+=sprintf(ptr,"%02x ",buf[i+j]);
		else
		 ptr+=sprintf(ptr,"%s","   ");

	  ptr+=sprintf(ptr,"%s","  ");
		
	  for (j=0; j<16; j++) 
		if (i+j < size)			
			ptr+=sprintf(ptr,"%c",isprint(buf[i+j]) ? buf[i+j] : '.');
	  *ptr='\0';
	  DBG("%s\n",digits);
	}
}



static void 
mainLoop(void *param)
{
    int fd;
    int ret;
	char buffer[128];

    DBG("entering mainLoop()");
	pthread_detach(pthread_self());		
    for (;;) {
find_rilhw:
        fd = -1;
        while  (fd < 0) 
		{
			if (s_service_port != NULL) 
			{
                fd = open_tty_port(s_service_port);
            }

            if (fd < 0) {
                ERROR ("opening AT interface. retrying after 10s... ");				
                sleep(10);
				goto find_rilhw;
                /* never returns */
            }  
        }

	    DBG ("open AT interface success! \n");

		do {
			ret = read(fd, buffer,128);
			if(ret<0)
			{
				WARN("read error %s\n",strerror(errno));
			}
			else
			{
				debug_data(__func__,ret,buffer);	
			}
		} while (1);

        // Give initializeCallback a chance to dispatched, since
        // we don't presently have a cancellation mechanism
        sleep(1);
        DBG("Re-opening after close");
    }
	
}




int main(int argc,char** argv)
{
    int ret;
    int fd = -1;
    int opt;

    while ( -1 != (opt = getopt(argc, argv, "p:d:s:f:"))) {
        switch (opt) {
            case 'p':
                s_port = atoi(optarg);
                if (s_port == 0) {
                    usage(argv[0]);
                    return -1;
                }
                DBG("Opening loopback port %d\n", s_port);
            break;

            case 'd':
                s_service_port = optarg;
                DBG("Opening tty device %s\n", s_service_port);
            break;

            case 's':
                s_service_port   = optarg;
                s_device_socket = 1;
                DBG("Opening socket %s\n", s_service_port);
            break;

			case 'f':
				s_flags = atoi(optarg);
				//always turn on error
				s_flags |= RIL_FLAG_ERROR;
				DBG("Debug flag change to x%08x\n",s_flags);
            break;
            
            default:
                usage(argv[0]);
                return -1;
        }
    }

	if(!s_service_port)
	{
		s_service_port = s_def_service_port;
		WARN("use default AT port [%s]\n",s_service_port);
	}    

    if (s_port < 0 && s_service_port == NULL) {
        usage(argv[0]);
        return -1;
    }
	
    //RIL_register(&s_callbacks);

	mainLoop(NULL);

	return 0;
}

