#include <stdio.h>
#include <telephony/ril.h>
#include <string.h>
#include <sys/time.h>

#include "ril-handler.h"

/* Last call fail cause, obtained by *ECAV. */
static int s_lastCallFailCause = CALL_FAIL_ERROR_UNSPECIFIED;



/**
 * RIL_REQUEST_LAST_CALL_FAIL_CAUSE
 *
 * Requests the failure cause code for the most recently terminated call.
*
 * See also: RIL_REQUEST_LAST_PDP_FAIL_CAUSE
 ok
 */
void requestLastCallFailCause(void *data, size_t datalen, RIL_Token t)
{
    RIL_onRequestComplete(t, RIL_E_SUCCESS, &s_lastCallFailCause, sizeof(int));
}

/**
 * RIL_REQUEST_GET_CURRENT_CALLS
 *
 * Requests current call list
 *
 * "data" is NULL
 *
 * "response" must be a "const RIL_Call **"
 *
 * Valid errors:
 *
 *  SUCCESS
 *  RADIO_NOT_AVAILABLE (radio resetting)
 *  GENERIC_FAILURE
 *      (request will be made again in a few hundred msec)
 */

void requestGetCurrentCalls(void *data, size_t datalen, RIL_Token t)
{
    int err;
    int countCalls;
    int countValidCalls;
    RIL_Call *p_calls;
    RIL_Call **pp_calls;
    int i;

	countValidCalls = 0;
	p_calls = &rilhw->call;
	pp_calls = &p_calls;

	//only 1 active call supported for PSTN network
	if(-1!=p_calls->index)
	{
		if(strlen(rilhw->pstn_call.number))
			strcpy(p_calls->number,rilhw->pstn_call.number);

		if(RIL_CALL_DIALING==p_calls->state)
		{
			p_calls->state = RIL_CALL_ACTIVE;
		}
		
		countValidCalls = 1;
	}


    RIL_onRequestComplete(t, RIL_E_SUCCESS, pp_calls,
            countValidCalls * sizeof (RIL_Call *));	
}


/** 
 * RIL_REQUEST_DIAL
 *
 * Initiate voice call.
 ok
*/
void requestDial(void *data, size_t datalen, RIL_Token t)
{
    RIL_Dial *p_dial;
    char *cmd;
    int ret;
	struct pstn_func pstn;
	unsigned char buffer[64];

    p_dial = (RIL_Dial *)data;
	memset(&pstn,0,sizeof(struct pstn_func));
	pstn.func = kFuncCall;
	DBG("dial numer [%s]\n",p_dial->address);
	strncpy(pstn.number,p_dial->address,(strlen(p_dial->address)>63?63:strlen(p_dial->address)));
	ret = pstn_setup_function(&pstn,buffer,64);
	if(ret>0)
	{
		//unsigned char cmd=kFuncDialingToneStop;
		//at_send_command(&cmd,1);

		cmd = kFuncDialingToneStop;
		at_send_command(&cmd,1);

		usleep(10*1000);
		at_send_command(buffer,ret);

		//pstn_set_hook(STATE_OFF_HOOK);


		//call state
		rilhw->call.index = 1;
		rilhw->call.state = RIL_CALL_DIALING;
		rilhw->call.number = NULL;
		
		RIL_onUnsolicitedResponse (
		RIL_UNSOL_RESPONSE_CALL_STATE_CHANGED,
		NULL, 0);						
	}

	WARN("TODO %s\n",__func__);
    /* success or failure is ignored by the upper layer here.
       it will call GET_CURRENT_CALLS and determine success that way */
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
}

/**
 * RIL_REQUEST_ANSWER
 *
 * Answer incoming call.
 *
 * Will not be called for WAITING calls.
 * RIL_REQUEST_SWITCH_WAITING_OR_HOLDING_AND_ACTIVE will be used in this case
 * instead.
 ok
*/
void requestAnswer(void *data, size_t datalen, RIL_Token t)
{
	WARN("TODO %s\n",__func__);

	pstn_set_hook(STATE_OFF_HOOK);
	rilhw->call.state = RIL_CALL_ACTIVE;

    /* Success or failure is ignored by the upper layer here,
       it will call GET_CURRENT_CALLS and determine success that way. */
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
}

/**
 * RIL_REQUEST_HANGUP
 *
 * Hang up a specific line (like AT+CHLD=1x).
 ok
*/
void requestHangup(void *data, size_t datalen, RIL_Token t)
{
	unsigned char command = kFuncUnsetPresetDialing;

	WARN("TODO %s\n",__func__);
	
	pstn_set_hook(STATE_ON_HOOK);
	rilhw->call.index = -1;	

	//notify android call status changed
	RIL_onUnsolicitedResponse (
	RIL_UNSOL_RESPONSE_CALL_STATE_CHANGED,
	NULL, 0);						

	at_send_command(&command,1);
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

}

/**
 * RIL_REQUEST_DTMF
 *
 * Send a DTMF tone
 *
 * If the implementation is currently playing a tone requested via
 * RIL_REQUEST_DTMF_START, that tone should be cancelled and the new tone
 * should be played instead.
 ok
*/
void requestDTMF(void *data, size_t datalen, RIL_Token t)
{
	WARN("TODO %s\n",__func__);
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
}

/**
 * RIL_REQUEST_DTMF_START
 *
 * Start playing a DTMF tone. Continue playing DTMF tone until 
 * RIL_REQUEST_DTMF_STOP is received .
 *
 * If a RIL_REQUEST_DTMF_START is received while a tone is currently playing,
 * it should cancel the previous tone and play the new one.
 *
 * See also: RIL_REQUEST_DTMF, RIL_REQUEST_DTMF_STOP.
 ok
 */
void requestDTMFStart(void *data, size_t datalen, RIL_Token t)
{
	WARN("TODO %s\n",__func__);
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
}

/**
 * RIL_REQUEST_DTMF_STOP
 *
 * Stop playing a currently playing DTMF tone.
 *
 * See also: RIL_REQUEST_DTMF, RIL_REQUEST_DTMF_START.
 ok
 */
void requestDTMFStop(void *data, size_t datalen, RIL_Token t)
{
	WARN("TODO %s\n",__func__);
	RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
}



