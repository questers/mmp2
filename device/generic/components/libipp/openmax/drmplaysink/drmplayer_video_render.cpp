#include <stdio.h>
#include <string.h>

#include <media/stagefright/VideoRenderer.h>
#include "OMX_IVCommon.h"
#include "DrmHardwareRendererOverlay.h"

using namespace android;


typedef struct
{
    DrmHardwareRendererOverlay *SF_VideoRender;

}DrmPlayerVideoRenderPrivate;

extern "C"  void *drmplayer_videorender_init(void *pRenderHandle){
    DrmPlayerVideoRenderPrivate *handle;
    if (pRenderHandle == NULL){
        return NULL;
    }
    handle = (DrmPlayerVideoRenderPrivate *)malloc(sizeof(DrmPlayerVideoRenderPrivate));
    if (handle == NULL){
        return NULL;
    }   
    handle->SF_VideoRender = ( DrmHardwareRendererOverlay*)pRenderHandle;
    return (void *)handle;

}

extern "C" void drmplayer_videorender(void *handle, const void *data, size_t size, void *platformPrivate){
    DrmPlayerVideoRenderPrivate *pDrmPlayerRenderHandle = ( DrmPlayerVideoRenderPrivate *)handle;
    if (pDrmPlayerRenderHandle){
        pDrmPlayerRenderHandle->SF_VideoRender->render(data,size,platformPrivate);
    }
}
extern "C" void drmplayer_videorender_deinit(void *DrmRenderHandle){
    if (DrmRenderHandle){
        free(DrmRenderHandle);
    }
}

extern "C" void drmplayer_videorender_getDisplayInfo(void *DrmRenderHandle,OMX_COLOR_FORMATTYPE *pColorFormat, size_t *pDisplayWidth, size_t *pDisplayHeight, size_t *pDecodedWidth, size_t *pDecodedHeight){
    DrmPlayerVideoRenderPrivate *pDrmPlayerRenderHandle = ( DrmPlayerVideoRenderPrivate *)DrmRenderHandle;
    OMX_COLOR_FORMATTYPE mColorFormat;
    size_t mDisplayWidth, mDisplayHeight;
    size_t mDecodedWidth, mDecodedHeight;
    
    if (pDrmPlayerRenderHandle){
        pDrmPlayerRenderHandle->SF_VideoRender->getDisplayInfo(mColorFormat, mDisplayWidth, mDisplayHeight, mDecodedWidth, mDecodedHeight);
        *pColorFormat = mColorFormat;
        *pDisplayWidth = mDisplayWidth;
        *pDisplayHeight = mDisplayHeight;
        *pDecodedWidth = mDecodedWidth;
        *pDecodedHeight = mDecodedHeight;        
    }
}
extern "C" void drmplayer_videorender_reconfig(void *DrmRenderHandle, OMX_COLOR_FORMATTYPE mColorFormat, size_t displayWidth, size_t displayHeight, size_t decodedWidth, size_t decodedHeight, bool bdestroyov){
    DrmPlayerVideoRenderPrivate *pDrmPlayerRenderHandle = ( DrmPlayerVideoRenderPrivate *)DrmRenderHandle;
    if (pDrmPlayerRenderHandle){
        pDrmPlayerRenderHandle->SF_VideoRender->Reconfig(mColorFormat, displayWidth, displayHeight, decodedWidth, decodedHeight, bdestroyov);
         
    }
}
