# audio codec library
PRODUCT_PACKAGES += \
	libcodecaacdec \
	libcodecaacenc \
	libcodecmp3dec \
	libcodecwmadec \
	libcodecamrnbdec \
	libcodecamrnbenc \
	libcodecamrwbdec \
	libcodecamrwbenc \
	libcodecg711 \
	libcodeczspdec \
	libzspmsg \
	libzsphal \

# video codec library
PRODUCT_PACKAGES += \
	libcodecvmetaenc \
	libcodecvmetadec \
	libvmetahal \
	libcodech263dec \
	libcodech263enc \
	libcodech264dec \
	libcodech264enc \
	libcodecmpeg4dec \
	libcodecmpeg4enc \
	libcodecmpeg2dec \
	libcodecwmvdec \

# image codec library
PRODUCT_PACKAGES += \
	libcodecjpegdec \
	libcodecjpegenc \

# misc library
PRODUCT_PACKAGES += \
	libmiscgen \
	libippcam \
	libippsp \
	libippvp \
	libicrctrlsvr \        

# openmax library
PRODUCT_PACKAGES += \
	libMrvlOmx \
	libdrmplaysink \
		




