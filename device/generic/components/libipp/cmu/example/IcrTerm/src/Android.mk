LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_C_INCLUDES += device/generic/components/libipp/cmu/include

LOCAL_SRC_FILES := \
	IcrTerm.c \
	utility.c \
	viewmodes.c

LOCAL_SHARED_LIBRARIES := \
	liblog \
        libcutils \
	libicrctrlsvr

LOCAL_MODULE := icrterm
include $(BUILD_EXECUTABLE)

