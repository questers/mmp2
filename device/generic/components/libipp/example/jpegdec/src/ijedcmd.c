/***************************************************************************************** 
Copyright (c) 2009, Marvell International Ltd. 
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Marvell nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MARVELL ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MARVELL BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************/


#include "codecJP.h"
#include "misc.h"
#include "ijedcmd.h"

/******************************************************************************
// Name:				JPEGDec_Init
// Description:			Init the JPEG decoder, memory callback function table and
//						input stream buffer.
//
// Input Arguments:
//				fp:		Pointer to input JPEG file
//	 pSrcBitStream:		Pointer to the src bit stream
//ppSrcCallbackTable:	Double pointer to the call back function table
//
// Output Arguments:	
//		pDstPicture:	Point to the IppPicture, which returns the JPEG image
//						properties, such as width, height, format, etc.
//	 ppDecoderState:	Double pointer to the updated decoder state structure
//
// Returns:
//        [Success]		General IPP return code
//        [Failure]		General IPP return code					
******************************************************************************/

IPPCODECEXFUN(IppCodecStatus, JPEGDec_Init)(void *pInStreamHandler, IppBitstream *pSrcBitStream, 
			IppPicture *pDstPicture, MiscGeneralCallbackTable  *pSrcCallbackTable, void **ppDecoderState)
{
	int nReadByte, done = 0;
	IppCodecStatus rtCode; 
	MiscFileReadCallback funcFileRead;

	/* Check input argument */
	if (! (pInStreamHandler && pSrcBitStream && pDstPicture && pSrcCallbackTable && ppDecoderState))  {
		return IPP_STATUS_BADARG_ERR;
	}

	funcFileRead = pSrcCallbackTable->fFileRead;
	/*The first file read do not need cpy to working buffer. This is designed to optimize the performance for some special 
	usage model. Eg: the decoded pic file size is very large(eg: 10M), the user buffer size is also large(2M)*/

	/* 1. Read the first JPEG stream block */
	nReadByte = funcFileRead((void*)&pSrcBitStream->pBsBuffer, 1, pSrcBitStream->bsByteLen, pInStreamHandler);
	if(!nReadByte) {
		return IPP_STATUS_ERR;
	}
	pSrcBitStream->pBsCurByte = pSrcBitStream->pBsBuffer;
	pSrcBitStream->bsByteLen = nReadByte;
	pSrcBitStream->bsCurBitOffset = 0;

	/* 2. InitAlloc the JPEG decoder state */
	while(!done) {
		rtCode = DecoderInitAlloc_JPEG (pSrcBitStream, pDstPicture, pSrcCallbackTable, ppDecoderState);
		
		switch(rtCode){
			case IPP_STATUS_NEED_INPUT:
				nReadByte = funcFileRead((void*)&pSrcBitStream->pBsBuffer, 1, pSrcBitStream->bsByteLen, pInStreamHandler);
				if(!nReadByte) {
					return IPP_STATUS_ERR;
				}		
				pSrcBitStream->pBsCurByte = pSrcBitStream->pBsBuffer;
				pSrcBitStream->bsByteLen = nReadByte;
				pSrcBitStream->bsCurBitOffset = 0;
			//	appiCopyStream_JPEG(pSrcBitStream, (IppJPEGDecoderState*)(*ppDecoderState));
				break;

			case IPP_STATUS_NOERR:
				done = 1;
				break;

			case IPP_STATUS_BADARG_ERR:
			case IPP_STATUS_NOMEM_ERR:
			case IPP_STATUS_BITSTREAM_ERR:
			case IPP_STATUS_NOTSUPPORTED_ERR:
			case IPP_STATUS_ERR:
			default:
				return IPP_STATUS_ERR;
		}
	}

	return IPP_STATUS_NOERR;
}

/******************************************************************************
// Name:				JPEGDec_ROI
// Description:			Main JPEG decoder function: ROI decoding both for 
//						standard mode and interactive mode.
//
// Input Arguments:
//		   srcFile:		Pointer to input JPEG file
//		   dstFile:		Pointer to output JPEG file
//	 pSrcBitStream:		Pointer to the src bit stream
//		pDecoderPar:	Pointer to the decoder configuration structure
//
// Output Arguments:	
//		pDstPicture:	Point to the IppPicture, which returns the JPEG image
//						properties, such as width, height, format, etc.
//	 ppDecoderState:	Double pointer to the updated decoder state structure
//
// Returns:
//        [Success]		General IPP return code
//        [Failure]		General IPP return code					
******************************************************************************/

IPPCODECEXFUN(IppCodecStatus, JPEG_ROIDec)(void *pInStreamHandler, IppBitstream *pSrcBitStream,
			IppPicture *pDstPicture, IppJPEGDecoderParam *pDecoderPar, void *pDecoderState)
{
	//IppJPEGDecoderState *pState = (IppJPEGDecoderState*)pDecoderState;
//	MiscFileReadCallback funcFileRead;
	int nReadByte, done = 0;
	IppCodecStatus rtCode; 

	/* BAC check */
	if(!(pDecoderState && pInStreamHandler && pSrcBitStream && pDstPicture && pDecoderPar)) {
		return IPP_STATUS_BADARG_ERR;
	}

//	funcFileRead = pState->ijxFileRead;

	while(!done) {
		
		rtCode = Decode_JPEG (pSrcBitStream, NULL, pDstPicture, pDecoderPar, pDecoderState);
		
		switch(rtCode) {
		case IPP_STATUS_OUTPUT_DATA:
			/* Not supported in platform release */
			break;

		case IPP_STATUS_NEED_INPUT:
			nReadByte = ReadStreamFromFile((void*)&pSrcBitStream->pBsBuffer, 1, pSrcBitStream->bsByteLen, pInStreamHandler);
			if(!nReadByte) {
				return IPP_STATUS_ERR;
			}
			pSrcBitStream->pBsCurByte = pSrcBitStream->pBsBuffer;
			pSrcBitStream->bsByteLen = nReadByte;
			pSrcBitStream->bsCurBitOffset = 0;
			//appiCopyStream_JPEG(pSrcBitStream, pState);
			break;

		case IPP_STATUS_JPEG_EOF:
			done = 1;
			break;

		case IPP_STATUS_BADARG_ERR:
		case IPP_STATUS_NOMEM_ERR:
		case IPP_STATUS_BITSTREAM_ERR:
		case IPP_STATUS_NOTSUPPORTED_ERR:
		case IPP_STATUS_ERR:
		default:
			return IPP_STATUS_ERR;
		}
	}

	return IPP_STATUS_NOERR;
}

/******************************************************************************
// Name:				JPEGDec_Rotate
// Description:			Main function for lossless JPEG rotation.
//
// Input Arguments:
//		   srcFile:		Pointer to input JPEG file
//		   dstFile:		Pointer to output JPEG file
//	 pSrcBitStream:		Pointer to the src bit stream
//	   pDecoderPar:		Pointer to the decoder configuration structure
//
// Output Arguments:	
//	 pDstBitStream:		Point to the IppBitstream, which stores the compressed 
//						output JPEG stream.
//	ppDecoderState:		Double pointer to the updated decoder state structure
//
// Returns:
//        [Success]		General IPP return code
//        [Failure]		General IPP return code					
******************************************************************************/

IPPCODECEXFUN(IppCodecStatus, JPEG_Rotate)(void *pInStreamHandler, void *pOutStreamHandler,
			IppBitstream *pSrcBitStream, IppBitstream *pDstBitStream, 
			IppJPEGDecoderParam *pDecoderPar, void *pDecoderState)
{
	//IppJPEGDecoderState *pState = (IppJPEGDecoderState*)pDecoderState;
//	MiscFileReadCallback funcFileRead;
//	MiscStreamFlushCallback funcStreamFlush;
	int nReadByte, nWriteByte, done = 0;
	IppCodecStatus rtCode; 

	/* BAC check */
	if(!(pDecoderState && pInStreamHandler && pOutStreamHandler && pSrcBitStream && pDstBitStream && pDecoderPar)) {
		return IPP_STATUS_BADARG_ERR;
	}

//	funcFileRead = pState->ijxFileRead;
//	funcStreamFlush = pState->ijxStreamFlush;

	while(!done) {

		rtCode = Decode_JPEG (pSrcBitStream, pDstBitStream, NULL, pDecoderPar, pDecoderState);
		
		switch(rtCode) {
		
		case IPP_STATUS_NEED_INPUT:
			nReadByte = ReadStreamFromFile((void*)&pSrcBitStream->pBsBuffer, 1, pSrcBitStream->bsByteLen, pInStreamHandler);
			if(!nReadByte) {
				return IPP_STATUS_ERR;
			}
			pSrcBitStream->pBsCurByte = pSrcBitStream->pBsBuffer;
			pSrcBitStream->bsByteLen = nReadByte;
			pSrcBitStream->bsCurBitOffset = 0;
			//appiCopyStream_JPEG(pSrcBitStream, pState);
			break;

		case IPP_STATUS_JPEG_EOF:
			done = 1;
			break;
		case IPP_STATUS_BADARG_ERR:
		case IPP_STATUS_NOMEM_ERR:
		case IPP_STATUS_BITSTREAM_ERR:
		case IPP_STATUS_NOTSUPPORTED_ERR:
		case IPP_STATUS_ERR:
		default:
			return IPP_STATUS_ERR;
		}
	}

	if(pOutStreamHandler != NULL){
		/* write the left data in the buffer */
		nWriteByte = (int)pDstBitStream->pBsCurByte-(int)pDstBitStream->pBsBuffer;
		if (nWriteByte != miscgStreamFlush(pDstBitStream->pBsBuffer,pOutStreamHandler, nWriteByte, 1) ){
			return IPP_STATUS_ERR;
		}
	}


	return IPP_STATUS_NOERR;
}

/* EOF */

