
#ifndef __eglfb_h_
#define __eglfb_h_

#ifdef __cplusplus
extern "C" {
#endif

	
EGLNativeDisplayType
fbGetDisplay(
	void
	);
	    
void
fbGetDisplayGeometry(
	EGLNativeDisplayType Display,
	int * Width,
	int * Height
	);
	    
void
fbDestroyDisplay(
	EGLNativeDisplayType Display
	);
	
EGLNativeWindowType
fbCreateWindow(
    EGLNativeDisplayType Display,
	int X,
	int Y,
	int Width,
	int Height
	);

void
fbGetWindowGeometry(
    EGLNativeWindowType Window,
	int * X,
	int * Y,
	int * Width,
	int * Height
	);

void
fbDestroyWindow(
	EGLNativeWindowType Window
	);

EGLNativePixmapType
fbCreatePixmap(
	EGLNativeDisplayType Display,
	int Width,
	int Height
	);

void
fbGetPixmapGeometry(
    EGLNativePixmapType Pixmap,
    int * Width,
    int * Height
    );

void
fbDestroyPixmap(
	EGLNativePixmapType Pixmap
	);

#ifdef __cplusplus
}
#endif

#endif
