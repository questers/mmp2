/****************************************************************************
*  
*    Copyright (C) 2002 - 2008 by Vivante Corp.
*  
*    This program is free software; you can redistribute it and/or modify
*    it under the terms of the GNU General Public Lisence as published by
*    the Free Software Foundation; either version 2 of the license, or
*    (at your option) any later version.
*  
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*    GNU General Public Lisence for more details.
*  
*    You should have received a copy of the GNU General Public License
*    along with this program; if not write to the Free Software
*    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*  
*****************************************************************************/




/*
 * Include file for the 2D stuff.
 */

#ifndef __gchal_2d_h_
#define __gchal_2d_h_

#ifdef __cplusplus
extern "C" {
#endif

#include "aqHalEnum.h"
#include "aqTypes.h"

/******************************************************************************\
****************************** Object Declarations *****************************
\******************************************************************************/

typedef struct _gcoBRUSH *				gcoBRUSH;
typedef struct _gcoBRUSH_CACHE *  		gcoBRUSH_CACHE;

/******************************************************************************\
******************************** gcoBRUSH Object *******************************
\******************************************************************************/

/* Create a new solid color gcoBRUSH object. */
gceSTATUS
gcoBRUSH_ConstructSingleColor(
	IN gcoHAL Hal,
	IN gctUINT32 ColorConvert,
	IN gctUINT32 Color,
	IN gctUINT64 Mask,
	gcoBRUSH * Brush
	);

/* Create a new monochrome gcoBRUSH object. */
gceSTATUS
gcoBRUSH_ConstructMonochrome(
	IN gcoHAL Hal,
	IN gctUINT32 OriginX,
	IN gctUINT32 OriginY,
	IN gctUINT32 ColorConvert,
	IN gctUINT32 FgColor,
	IN gctUINT32 BgColor,
	IN gctUINT64 Bits,
	IN gctUINT64 Mask,
	gcoBRUSH * Brush
	);

/* Create a color gcoBRUSH object. */
gceSTATUS
gcoBRUSH_ConstructColor(
	IN gcoHAL Hal,
	IN gctUINT32 OriginX,
	IN gctUINT32 OriginY,
	IN gctPOINTER Address,
	IN gceSURF_FORMAT Format,
	IN gctUINT64 Mask,
	gcoBRUSH * Brush
	);

/* Destroy an gcoBRUSH object. */
gceSTATUS
gcoBRUSH_Destroy(
	IN gcoBRUSH Brush
	);

/******************************************************************************\
******************************** gcoSURF Object *******************************
\******************************************************************************/

/* Set cipping rectangle. */
gceSTATUS
gcoSURF_SetClipping(
	IN gcoSURF Surface
	);

/* Clear one or more rectangular areas. */
gceSTATUS
gcoSURF_Clear2D(
	IN gcoSURF DestSurface,
	IN gctUINT32 RectCount,
	IN gcsRECT_PTR DestRect,
	IN gctUINT32 LoColor,
	IN gctUINT32 HiColor
	);

/* Draw one or more Bresenham lines. */
gceSTATUS
gcoSURF_Line(
	IN gcoSURF Surface,
	IN gctUINT32 LineCount,
	IN gcsRECT_PTR Position,
	IN gcoBRUSH Brush,
	IN gctUINT8 FgRop,
	IN gctUINT8 BgRop
	);

/* Generic rectangular blit. */
gceSTATUS
gcoSURF_Blit(
	IN OPTIONAL gcoSURF SrcSurface,
	IN gcoSURF DestSurface,
	IN gctUINT32 RectCount,
	IN OPTIONAL gcsRECT_PTR SrcRect,
	IN gcsRECT_PTR DestRect,
	IN OPTIONAL gcoBRUSH Brush,
	IN gctUINT8 FgRop,
	IN gctUINT8 BgRop,
	IN OPTIONAL gceSURF_TRANSPARENCY Transparency,
	IN OPTIONAL gctUINT32 TransparencyColor,
	IN OPTIONAL gctPOINTER Mask,
	IN OPTIONAL gceSURF_MONOPACK MaskPack
	);

/* Monochrome blit. */
gceSTATUS
gcoSURF_MonoBlit(
	IN gcoSURF DestSurface,
	IN gctPOINTER Source,
	IN gceSURF_MONOPACK SourcePack,
	IN gcsPOINT_PTR SourceSize,
	IN gcsPOINT_PTR SourceOrigin,
	IN gcsRECT_PTR DestRect,
	IN OPTIONAL gcoBRUSH Brush,
	IN gctUINT8 FgRop,
	IN gctUINT8 BgRop,
	IN gctBOOL ColorConvert,
	IN gctUINT8 MonoTransparency,
	IN gceSURF_TRANSPARENCY Transparency,
	IN gctUINT32 FgColor,
	IN gctUINT32 BgColor
	);

/* Filter blit. */
gceSTATUS
gcoSURF_FilterBlit(
	IN gcoSURF SrcSurface,
	IN gcoSURF DestSurface,
	IN gcsRECT_PTR SrcRect,
	IN gcsRECT_PTR DestRect,
	IN gcsRECT_PTR DestSubRect
	);

/* Enable alpha blending engine in the hardware and disengage the ROP engine. */
gceSTATUS
gcoSURF_EnableAlphaBlend(
	IN gcoSURF Surface,
	IN gctUINT8 SrcGlobalAlphaValue,
	IN gctUINT8 DstGlobalAlphaValue,
	IN gceSURF_PIXEL_ALPHA_MODE SrcAlphaMode,
	IN gceSURF_PIXEL_ALPHA_MODE DstAlphaMode,
	IN gceSURF_GLOBAL_ALPHA_MODE SrcGlobalAlphaMode,
	IN gceSURF_GLOBAL_ALPHA_MODE DstGlobalAlphaMode,
	IN gceSURF_BLEND_FACTOR_MODE SrcFactorMode,
	IN gceSURF_BLEND_FACTOR_MODE DstFactorMode,
	IN gceSURF_PIXEL_COLOR_MODE SrcColorMode,
	IN gceSURF_PIXEL_COLOR_MODE DstColorMode
	);

/* Disable alpha blending engine in the hardware and engage the ROP engine. */
gceSTATUS
gcoSURF_DisableAlphaBlend(
	IN gcoSURF Surface
	);

/* Copy a rectangular area with format conversion. */
gceSTATUS
gcoSURF_CopyPixels(
	IN gcoSURF Source,
	IN gcoSURF Target,
	IN gctINT SourceX,
	IN gctINT SourceY,
	IN gctINT TargetX,
	IN gctINT TargetY,
	IN gctINT Width,
	IN gctINT Height
	);

/* Read surface pixel. */
gceSTATUS
gcoSURF_ReadPixel(
	IN gcoSURF Surface,
	IN gctPOINTER Memory,
	IN gctINT X,
	IN gctINT Y,
	IN gceSURF_FORMAT Format,
	OUT gctPOINTER PixelValue
	);

/* Write surface pixel. */
gceSTATUS
gcoSURF_WritePixel(
	IN gcoSURF Surface,
	IN gctPOINTER Memory,
	IN gctINT X,
	IN gctINT Y,
	IN gceSURF_FORMAT Format,
	IN gctPOINTER PixelValue
	);

/******************************************************************************\
********************************** gco2D Object *********************************
\******************************************************************************/

/* Construct a new gco2D object. */
gceSTATUS
gco2D_Construct(
	IN gcoHAL Hal,
	OUT gco2D * Hardware
	);

/* Destroy an gco2D object. */
gceSTATUS
gco2D_Destroy(
	IN gco2D Hardware
	);

/* Sets the maximum number of brushes in the brush cache. */
gceSTATUS
gco2D_SetBrushLimit(
	IN gco2D Hardware,
	IN gctUINT MaxCount
	);

/* Flush the brush. */
gceSTATUS
gco2D_FlushBrush(
	IN gco2D Engine,
	IN gcoBRUSH Brush,
	IN gceSURF_FORMAT Format
	);

/* Program the specified solid color brush. */
gceSTATUS
gco2D_LoadSolidBrush(
	IN gco2D Engine,
	IN gceSURF_FORMAT Format,
	IN gctUINT32 ColorConvert,
	IN gctUINT32 Color,
	IN gctUINT64 Mask
	);

/* Configure monochrome source. */
gceSTATUS
gco2D_SetMonochromeSource(
	IN gco2D Engine,
	IN gctBOOL ColorConvert,
	IN gctUINT8 MonoTransparency,
	IN gceSURF_MONOPACK DataPack,
	IN gctBOOL CoordRelative,
	IN gceSURF_TRANSPARENCY Transparency,
	IN gctUINT32 FgColor,
	IN gctUINT32 BgColor
	);

/* Configure color source. */
gceSTATUS
gco2D_SetColorSource(
	IN gco2D Engine,
	IN gctUINT32 Address,
	IN gctUINT32 Stride,
	IN gceSURF_FORMAT Format,
	IN gceSURF_ROTATION Rotation,
	IN gctUINT32 SurfaceWidth,
	IN gctBOOL CoordRelative,
	IN gceSURF_TRANSPARENCY Transparency,
	IN gctUINT32 TransparencyColor
	);

/* Configure masked color source. */
gceSTATUS
gco2D_SetMaskedSource(
	IN gco2D Engine,
	IN gctUINT32 Address,
	IN gctUINT32 Stride,
	IN gceSURF_FORMAT Format,
	IN gctBOOL CoordRelative,
	IN gceSURF_MONOPACK MaskPack
	);

/* Setup the source rectangle. */
gceSTATUS
gco2D_SetSource(
	IN gco2D Engine,
	IN gcsRECT_PTR SrcRect
	);

/* Set clipping rectangle. */
gceSTATUS
gco2D_SetClipping(
	IN gco2D Engine,
	IN gcsRECT_PTR Rect
	);

/* Configure destination. */
gceSTATUS
gco2D_SetTarget(
	IN gco2D Engine,
	IN gctUINT32 Address,
	IN gctUINT32 Stride,
	IN gceSURF_ROTATION Rotation,
	IN gctUINT32 SurfaceWidth
	);

/* Calculate and program the stretch factors. */
gceSTATUS
gco2D_SetStretchFactors(
	IN gco2D Engine,
	IN gctUINT32 HorFactor,
	IN gctUINT32 VerFactor
	);

/* Create a new solid color gcoBRUSH object. */
gceSTATUS
gco2D_ConstructSingleColorBrush(
	IN gco2D Engine,
	IN gctUINT32 ColorConvert,
	IN gctUINT32 Color,
	IN gctUINT64 Mask,
	gcoBRUSH * Brush
	);

/* Create a new monochrome gcoBRUSH object. */
gceSTATUS
gco2D_ConstructMonochromeBrush(
	IN gco2D Engine,
	IN gctUINT32 OriginX,
	IN gctUINT32 OriginY,
	IN gctUINT32 ColorConvert,
	IN gctUINT32 FgColor,
	IN gctUINT32 BgColor,
	IN gctUINT64 Bits,
	IN gctUINT64 Mask,
	gcoBRUSH * Brush
	);

/* Create a color gcoBRUSH object. */
gceSTATUS
gco2D_ConstructColorBrush(
	IN gco2D Engine,
	IN gctUINT32 OriginX,
	IN gctUINT32 OriginY,
	IN gctPOINTER Address,
	IN gceSURF_FORMAT Format,
	IN gctUINT64 Mask,
	gcoBRUSH * Brush
	);

/* Clear one or more rectangular areas. */
gceSTATUS
gco2D_Clear(
	IN gco2D Engine,
	IN gctUINT32 RectCount,
	IN gcsRECT_PTR Rect,
	IN gceSURF_FORMAT DestFormat,
	IN gctUINT32 LoColor,
	IN gctUINT32 HiColor
	);

/* Draw one or more Bresenham lines. */
gceSTATUS
gco2D_Line(
	IN gco2D Engine,
	IN gctUINT32 LineCount,
	IN gcsRECT_PTR Position,
	IN gcoBRUSH Brush,
	IN gctUINT8 FgRop,
	IN gctUINT8 BgRop,
	IN gceSURF_FORMAT DestFormat
	);

/* Generic blit. */
gceSTATUS
gco2D_Blit(
	IN gco2D Engine,
	IN gctUINT32 RectCount,
	IN gcsRECT_PTR Rect,
	IN gctUINT8 FgRop,
	IN gctUINT8 BgRop,
	IN gceSURF_FORMAT DestFormat
	);

/* Stretch blit. */
gceSTATUS
gco2D_StretchBlit(
	IN gco2D Engine,
	IN gctUINT32 RectCount,
	IN gcsRECT_PTR Rect,
	IN gctUINT8 FgRop,
	IN gctUINT8 BgRop,
	IN gceSURF_FORMAT DestFormat
	);

/* Monochrome blit. */
gceSTATUS
gco2D_MonoBlit(
	IN gco2D Engine,
	IN gctPOINTER StreamBits,
	IN gcsPOINT_PTR StreamSize,
	IN gcsRECT_PTR StreamRect,
	IN gceSURF_MONOPACK SrcStreamPack,
	IN gceSURF_MONOPACK DestStreamPack,
	IN gcsRECT_PTR DestRect,
	IN gctUINT32 FgRop,
	IN gctUINT32 BgRop,
	IN gceSURF_FORMAT DestFormat
	);

/* Set kernel size. */
gceSTATUS
gco2D_SetKernelSize(
	IN gco2D Engine,
	IN gctUINT8 HorKernelSize,
	IN gctUINT8 VerKernelSize
	);

/* Set filter type. */
gceSTATUS
gco2D_SetFilterType(
	IN gco2D Engine,
	IN gceFILTER_TYPE FilterType
	);

/* Frees the temporary buffer allocated by filter blit operation. */
gceSTATUS
gco2D_FreeFilterBuffer(
	IN gco2D Engine
	);

/* Filter blit. */
gceSTATUS
gco2D_FilterBlit(
	IN gco2D Engine,
	IN gctUINT32 SrcAddress,
	IN gctUINT SrcStride,
	IN gctUINT32 SrcUAddress,
	IN gctUINT SrcUStride,
	IN gctUINT32 SrcVAddress,
	IN gctUINT SrcVStride,
	IN gceSURF_FORMAT SrcFormat,
	IN gceSURF_ROTATION SrcRotation,
	IN gctUINT32 SrcSurfaceWidth,
	IN gcsRECT_PTR SrcRect,
	IN gctUINT32 DestAddress,
	IN gctUINT DestStride,
	IN gceSURF_FORMAT DestFormat,
	IN gceSURF_ROTATION DestRotation,
	IN gctUINT32 DestSurfaceWidth,
	IN gcsRECT_PTR DestRect,
	IN gcsRECT_PTR DestSubRect
	);

/* Enable alpha blending engine in the hardware and disengage the ROP engine. */
gceSTATUS
gco2D_EnableAlphaBlend(
	IN gco2D Engine,
	IN gctUINT8 SrcGlobalAlphaValue,
	IN gctUINT8 DstGlobalAlphaValue,
	IN gceSURF_PIXEL_ALPHA_MODE SrcAlphaMode,
	IN gceSURF_PIXEL_ALPHA_MODE DstAlphaMode,
	IN gceSURF_GLOBAL_ALPHA_MODE SrcGlobalAlphaMode,
	IN gceSURF_GLOBAL_ALPHA_MODE DstGlobalAlphaMode,
	IN gceSURF_BLEND_FACTOR_MODE SrcFactorMode,
	IN gceSURF_BLEND_FACTOR_MODE DstFactorMode,
	IN gceSURF_PIXEL_COLOR_MODE SrcColorMode,
	IN gceSURF_PIXEL_COLOR_MODE DstColorMode
	);

/* Disable alpha blending engine in the hardware and engage the ROP engine. */
gceSTATUS
gco2D_DisableAlphaBlend(
	IN gco2D Engine
	);

/* Retrieve the maximum number of 32-bit data chunks for a single DE command. */
gctUINT32
gco2D_GetMaximumDataCount(
	void
	);

/* Returns the pixel alignment of the surface. */
gceSTATUS
gco2D_GetPixelAlignment(
	gceSURF_FORMAT Format,
	gcsPOINT_PTR Alignment
	);

/* Retrieve monochrome stream pack size. */
gceSTATUS
gco2D_GetPackSize(
	IN gceSURF_MONOPACK StreamPack,
	OUT gctUINT32 * PackWidth,
	OUT gctUINT32 * PackHeight
	);

/* Flush the 2D pipeline. */
gceSTATUS
gco2D_Flush(
	IN gco2D Engine
	);

/* Load 256-entry color table for INDEX8 source surfaces. */
gceSTATUS
gco2D_LoadPalette(
	IN gco2D Engine,
	IN gctUINT FirstIndex,
	IN gctUINT IndexCount,
	IN gctPOINTER ColorTable,
	IN gctBOOL ColorConvert
	);

/* Enable/disable 2D BitBlt mirrorring. */
gceSTATUS
gco2D_SetBitBlitMirror(
	IN gco2D Engine,
	IN gctBOOL HorizontalMirror,
	IN gctBOOL VerticalMirror
	);

#ifdef __cplusplus
}
#endif

#endif /* __gchal_2d_h_ */
