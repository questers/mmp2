#ifndef __gl2_mrvl_h_
#define __gl2_mrvl_h_

/* \todo check that this should be in core. */
#define GL_NONE                          0

/* Shader Binary */
#define GL_PLATFORM_BINARY                0x8D63

#define GL_FRAMEBUFFER_INCOMPLETE_FORMATS            0x8CDA

#endif /* __gl2_mrvl_h_ */
