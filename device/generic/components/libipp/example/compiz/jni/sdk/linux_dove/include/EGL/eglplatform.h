#ifndef __eglplatform_h_
#define __eglplatform_h_

/*
** Copyright (c) 2007-2009 The Khronos Group Inc.
**
** Permission is hereby granted, free of charge, to any person obtaining a
** copy of this software and/or associated documentation files (the
** "Materials"), to deal in the Materials without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Materials, and to
** permit persons to whom the Materials are furnished to do so, subject to
** the following conditions:
**
** The above copyright notice and this permission notice shall be included
** in all copies or substantial portions of the Materials.
**
** THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.
*/

/* Platform-specific types and definitions for egl.h
 * $Revision: 7244 $ on $Date: 2009-01-20 17:06:59 -0800 (Tue, 20 Jan 2009) $
 *
 * Adopters may modify khrplatform.h and this file to suit their platform.
 * You are encouraged to submit all modifications to the Khronos group so that
 * they can be included in future versions of this file.  Please submit changes
 * by sending them to the public Khronos Bugzilla (http://khronos.org/bugzilla)
 * by filing a bug against product "EGL" component "Registry".
 */

#include <KHR/khrplatform.h>


/* Macros used in EGL function prototype declarations.
 *
 * EGL functions should be prototyped as:
 *
 * EGLAPI return-type EGLAPIENTRY eglFunction(arguments);
 * typedef return-type (EXPAPIENTRYP PFNEGLFUNCTIONPROC) (arguments);
 *
 * KHRONOS_APICALL and KHRONOS_APIENTRY are defined in KHR/khrplatform.h
 */

#ifndef EGLAPI
#define EGLAPI KHRONOS_APICALL
#endif

#ifndef EGLAPIENTRY
#define EGLAPIENTRY KHRONOS_APIENTRY
#endif
#define EGLAPIENTRYP EGLAPIENTRY*


/* Define EGLint. This must be a signed integral type large enough to contain
 * all legal attribute names and values passed into and out of EGL, whether
 * their type is boolean, bitmask, enumerant (symbolic constant), integer,
 * handle, or other.  While in general a 32-bit integer will suffice, if
 * handles are 64 bit types, then EGLint should be defined as a signed 64-bit
 * integer type.
 */
typedef khronos_int32_t EGLint;


/*
 * Vivante specific definitions and declarations for EGL library.
 * From "eglVivante.h"
 */


/* Windows calling convention boilerplate */
#if defined(_WIN32) && !defined(APIENTRY) && !defined(__CYGWIN__) && !defined(__SCITECH_SNAP__)
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#endif

#if !defined(UNDER_CE)
#include <sys/types.h>
#endif

/*
    USE VDK
    
    This define enables the VDK linkage for EGL.
*/

#ifndef USE_VDK
# define USE_VDK	0
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if defined(WIN32) || defined(UNDER_CE)
#   define int32_t INT32
#endif

#if USE_VDK
#if defined(_WIN32) || defined(__VC32__) && !defined(__CYGWIN__) && !defined(__SCITECH_SNAP__)
#include <windows.h>
#endif
/* VDK platform independent. */
#include <vdkTypes.h>
typedef vdkDisplay	EGLNativeDisplayType;
typedef vdkWindow	EGLNativeWindowType;
typedef vdkPixmap	EGLNativePixmapType;

#elif defined(_WIN32) || defined(__VC32__) && !defined(__CYGWIN__) && !defined(__SCITECH_SNAP__)
/* Win32 and Windows CE platforms. */
#include <windows.h>
typedef HDC				EGLNativeDisplayType;
typedef HWND		    EGLNativeWindowType;
typedef HBITMAP		    EGLNativePixmapType;

#elif defined(LINUX) && defined(EGL_API_FB)

/* Linux platform for FBDEV. */
typedef struct _FBDisplay * EGLNativeDisplayType;
typedef struct _FBWindow *  EGLNativeWindowType;
typedef struct _FBPixmap *  EGLNativePixmapType;
	
EGLNativeDisplayType
fbGetDisplay(
	void
	);
	    
void
fbGetDisplayGeometry(
	EGLNativeDisplayType Display,
	int * Width,
	int * Height
	);
	    
void
fbDestroyDisplay(
	EGLNativeDisplayType Display
	);
	
EGLNativeWindowType
fbCreateWindow(
    EGLNativeDisplayType Display,
	int X,
	int Y,
	int Width,
	int Height
	);

void
fbGetWindowGeometry(
    EGLNativeWindowType Window,
	int * X,
	int * Y,
	int * Width,
	int * Height
	);

void
fbDestroyWindow(
	EGLNativeWindowType Window
	);

EGLNativePixmapType
fbCreatePixmap(
	EGLNativeDisplayType Display,
	int Width,
	int Height
	);

void
fbGetPixmapGeometry(
    EGLNativePixmapType Pixmap,
    int * Width,
    int * Height
    );

void
fbDestroyPixmap(
	EGLNativePixmapType Pixmap
	);

#else
/* X11 platform. */
#include <X11/Xlib.h>
#include <X11/Xutil.h>

typedef Display *	EGLNativeDisplayType;
typedef Window		EGLNativeWindowType;

#ifdef CUSTOM_PIXMAP
typedef void *		EGLNativePixmapType;
#else
typedef XImage *	EGLNativePixmapType;
#endif /* CUSTOM_PIXMAP */

#endif

#ifdef __EGL_EXPORTS
# if defined(_WIN32) && !defined(__SCITECH_SNAP__)
#  define EGLAPI	__declspec(dllexport)
# else
#  define EGLAPI
# endif
#endif

/* EGL 1.2 types, renamed for consistency in EGL 1.3 */
typedef EGLNativeDisplayType NativeDisplayType;
typedef EGLNativePixmapType NativePixmapType;
typedef EGLNativeWindowType NativeWindowType;


#ifdef __cplusplus
}
#endif


#endif /* __eglplatform_h */




