#ifndef __gl2ext_mrvl_h_
#define __gl2ext_mrvl_h_

#define GL_BGRA                                                 0x80E1

/* GL_EXT_texture_format_BGRA8888 */
#ifndef GL_EXT_texture_compression_s3tc
#define GL_COMPRESSED_RGB_S3TC_DXT1_EXT							0x83F0
#define GL_COMPRESSED_RGBA_S3TC_DXT1_EXT						0x83F1
#define GL_COMPRESSED_RGBA_S3TC_DXT3_EXT						0x83F2
#define GL_COMPRESSED_RGBA_S3TC_DXT5_EXT						0x83F3
#endif

#ifndef GL_EXT_texture_compression_s3tc
#define GL_EXT_texture_compression_s3tc 1
#endif

#endif/* __gl2ext_mrvl_h_ */

