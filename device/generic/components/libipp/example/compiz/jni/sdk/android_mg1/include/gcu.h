/******************************************************************************
*(C) Copyright 2009 Marvell International Ltd.
* All Rights Reserved 
******************************************************************************/

/*!
*******************************************************************************
*  \file gcu.h
*  \brief  
*      Decalration of  GCU (GC Uitlities) interface
******************************************************************************/

#ifndef __GCU_H__
#define __GCU_H__

typedef unsigned int            GCUbool;
typedef unsigned int            GCUenum;
typedef int                     GCUint;
typedef unsigned int            GCUuint;
typedef float                   GCUfloat;
typedef void                    GCUvoid;

typedef void*    				GCUContext;
typedef void*    				GCUSurface;
typedef void*                   GCUFence;

typedef void*                   GCUVirtualAddr;
typedef unsigned int            GCUPhysicalAddr;

#define GCU_FALSE               0
#define GCU_TRUE                1
#define GCU_MAX_UINT32          0xffffffff

#define GCU_VERSION_1_0         0x00010000;
#define GCU_VERSION_2_0         0x00020000;

#define GCU_INFINITE            0xffffffff

/*!
*********************************************************************
*   \brief
*       Data types for initialization and termination
*********************************************************************
*/
typedef struct _GCU_INIT_DATA{
    GCUenum     version;        /*  input the version you want, return the 
                                    actual version of current library.
                                 */                                 
    GCUbool     debug;          /*  enable debug output or not */
    GCUuint     reserved[6];     
}GCU_INIT_DATA;


/*!
*********************************************************************
*   \brief
*       Data types for context operation
*********************************************************************
*/
typedef struct _GCU_CONTEXT_DATA{
    GCUuint     reserved[8];     
}GCU_CONTEXT_DATA;

/*!
*********************************************************************
*   \brief
*       Data types for surface operation
*********************************************************************
*/
typedef enum _GCU_SURFACE_LOCATION{
    GCU_SURFACE_LOCATION_VIDEO          =   0, /* physical continous memory     */
    GCU_SURFACE_LOCATION_VIRTUAL        =   1, /* non-physical continous memory */
    
    GCU_SURFACE_LOCATION_FORCE_UINT     =   GCU_MAX_UINT32
}GCU_SURFACE_LOCATION; 

typedef enum _GCU_SURFACE_DIMENSION{
    GCU_SURFACE_2D                      =   0,
    
    GCU_SURFACE_DIMENSION_FORCE_UINT    =   GCU_MAX_UINT32
}GCU_SURFACE_DIMENSION;        

typedef enum _GCU_FORMAT{
    /* RGB series */
    GCU_FORMAT_ARGB8888     =   0,
    GCU_FORMAT_XRGB8888     =   1,
    
    GCU_FORMAT_ARGB1555     =   2,
    GCU_FORMAT_XRGB1555     =   3,
    GCU_FORMAT_RGB565       =   4,
    
    /* BGR series */
    

    /* Luminance series */
    GCU_FORMAT_L8           =   100,
    
    /* YUV series */
    GCU_FORMAT_UYVY         =   200,
    GCU_FORMAT_YUY2         =   201,
    GCU_FORMAT_YV12         =   202,
    GCU_FORMAT_NV12         =   203,
    GCU_FORMAT_I420         =   204,

    GCU_FORMAT_UNKNOW       =   500,
    
    GCU_FORMAT_FORCE_UINT   =   GCU_MAX_UINT32
}GCU_FORMAT;

typedef struct _GCU_SURFACE_FLAG{
    union 
    {
        struct 
        {
            GCUuint preAllocVirtual     : 1;    
            GCUuint preAllocPhysical    : 1;            
            GCUuint reserved            : 30;
        }bits;
        unsigned int value;
    };
}GCU_SURFACE_FLAG;

typedef struct _GCU_ALLOC_INFO{
    unsigned int    width;
    unsigned int    height;
    int             stride;
    void*           mapInfo;
    GCUVirtualAddr  virtualAddr;        /* none cacheable virtual address */
    GCUPhysicalAddr physicalAddr;
    GCUuint         reserved[3];    
}GCU_ALLOC_INFO;

typedef struct _GCU_SURFACE_DATA{
    GCU_SURFACE_LOCATION    location;  
    GCU_SURFACE_DIMENSION   dimention; 
    GCU_SURFACE_FLAG        flag;       
    
    GCU_FORMAT              format;     
    GCUuint                 width;      /* base layer width */
    GCUuint                 height;     /* base layer size */
    GCUuint                 arraySize;  /* surface array size. 
                                           For normal 2D surface, set it to 1. 
                                           For YUV planar surface, set it to 3.
                                           For mipmapped surface, set it to mipmap count.
                                         */    
    GCU_ALLOC_INFO*         pPreAllocInfos;   
    GCUuint                 reserved[4];        
}GCU_SURFACE_DATA;

typedef struct _GCU_SURFACE_LOCK_FLAG{
    union 
    {
        struct 
        {
            GCUuint preserve    : 1;    /*  preserve contents in locked buffer */ 
            GCUuint reserved    : 31;
        }bits;
        GCUuint value;
    };
}GCU_SURFACE_LOCK_FLAG;


typedef struct _GCU_SURFACE_UPDATE_FLAG{
    union 
    {
        struct 
        {
            GCUuint preAllocVirtual     : 1;    
            GCUuint preAllocPhysical    : 1;            
            GCUuint reserved            : 30;
        }bits;
        GCUuint value;
    };
}GCU_SURFACE_UPDATE_FLAG;


typedef struct _GCU_SURFACE_LOCK_DATA{
    GCUSurface      pSurface; 
    GCU_SURFACE_LOCK_FLAG   flag;
    GCUuint         arraySize;      /* specify the size of alloc info array */
    GCU_ALLOC_INFO* pAllocInfos;    /* pointer to alloc info array          */
    GCUuint         reserved[4];     
}GCU_SURFACE_LOCK_DATA; 
                    
typedef struct _GCU_SURFACE_UPDATE_DATA{
    GCU_SURFACE_UPDATE_FLAG flag;
    GCUSurface      pSurface; 
    GCUuint         arraySize;
    GCU_ALLOC_INFO* pAllocInfos;
    GCUuint         reserved[4];        
}GCU_SURFACE_UPDATE_DATA;                        


/*!
*********************************************************************
*   \brief
*       Data types for rendering API
*********************************************************************
*/                        
typedef enum _GCU_ROTATION{     /* rotation is clock-wise */
    GCU_ROTATION_0              =   0x0,
    GCU_ROTATION_FLIP_H         =   0x1,
    GCU_ROTATION_FLIP_V         =   0x2,
    GCU_ROTATION_90             =   0x4,
    GCU_ROTATION_180            =   0x3,
    GCU_ROTATION_270            =   0x7,    
    
    GCU_ROTATION_FORCE_UINT     =   GCU_MAX_UINT32
}GCU_ROTATION;

typedef enum _GCU_QUALITY_TYPE{
    GCU_QUALITY_NORMAL          =   0,
    GCU_QUALITY_HIGH            =   1, /* high quality scaling when gcuBlt */     
        
    GCU_QUALITY_TYPE_FORCE_UINT =   GCU_MAX_UINT32
}GCU_QUALITY_TYPE;

/* State types for gcuSet(pCtx, GCU_STATE_TYPE, value) */
typedef enum _GCU_STATE_TYPE{
    GCU_QUALITY                 =   1,  /* Followed by GCU_QUALITY_TYPE value   */
              
    GCU_STATE_TYPE_FORCE_UINT   =   GCU_MAX_UINT32
}GCU_STATE_TYPE;

typedef enum _GCU_BLEND_MODE{
    GCU_BLEND_SRC               =   0,  /* dst = src                */
    GCU_BLEND_SRC_OVER          =   1,  /* dst = src + (1-a)* dst   */ 
    
    GCU_BLEND_MODE_FORCE_UINT   =   GCU_MAX_UINT32
}GCU_BLEND_MODE;

typedef enum _GCU_FILTER_TYPE{
    GCU_H_USER_FILTER         	=   0,	/* 	user filter on horizontal direction 					*/
    GCU_V_USER_FILTER           =   1,  /* 	user filter on vertical direction 						*/     
    GCU_BLUR_FILTER				=   2,	/*	blur filter on both horizontal and vertical direction 	*/
	
    GCU_FILTER_TYPE_FORCE_UINT  =   GCU_MAX_UINT32
}GCU_FILTER_TYPE;

typedef struct _GCU_RECT{
    GCUint left;       /* left point is included in rect   */
    GCUint top;        /* top point is included in rect    */
    GCUint right;      /* right point is not included in rect, width = right - left    */
    GCUint bottom;     /* bottom point is not included in rect, height = bottom - top  */
}GCU_RECT;

typedef struct _GCU_BLT_DATA{
    GCUSurface      pSrcSurface;
    GCUSurface      pDstSurface;
    GCU_RECT*       pSrcRect;           /* NULL means full surface */ 
    GCU_RECT*       pDstRect;           /* NULL means full surface */
    GCU_ROTATION    rotation;
    
    GCUuint         reserved[7];
}GCU_BLT_DATA;

typedef struct _GCU_BLEND_DATA{
    GCUSurface      pSrcSurface;
    GCUSurface      pDstSurface;
    GCU_RECT*       pSrcRect;           /* NULL means full surface */ 
    GCU_RECT*       pDstRect;           /* NULL means full surface */
    GCU_ROTATION    rotation;
    
    GCU_BLEND_MODE  blendMode;
    GCUuint         srcGlobalAlpha;
    GCUuint         dstGlobalAlpha;        
    
    GCUuint         reserved[4];
}GCU_BLEND_DATA;

typedef struct _GCU_FILTER_BLT_DATA{
    GCUSurface      pSrcSurface;
    GCUSurface      pDstSurface;
    GCU_RECT*       pSrcRect;           /* NULL means full surface */ 
    GCU_RECT*       pDstRect;           /* NULL means full surface */
    GCU_ROTATION    rotation;

    GCU_FILTER_TYPE filterType;
    
    GCUuint         reserved[6];
}GCU_FILTER_BLT_DATA;

typedef struct _GCU_FILL_DATA{
	GCUSurface 	pSurface;
	GCU_RECT*	pRect;
	GCUbool		bSolidColor;		/* Fill surface by solid color or pattern */					
	GCUuint		color;		 		/* Solid color in 0xAARRGGBB format */
	GCUSurface  pPattern;
	GCUuint		reserved[3];
}GCU_FILL_DATA;



/*!
*********************************************************************
*   \brief
*       Data types for query API
*********************************************************************
*/
typedef enum  _GCU_QUERY_NAME{
    GCU_VERSION                 =   0,
    GCU_VENDOR                  =   1,
    GCU_RENDERER                =   2,
    GCU_ERROR                   =   3,  

    GCU_QUERY_NAME_FORCE_UINT   =   GCU_MAX_UINT32   
}GCU_QUERY_NAME;

typedef enum _GCU_ERROR_TYPE{
    GCU_NO_ERROR                = 0,
    GCU_NOT_INITIALIZED         = 1,
    GCU_INVALID_PARAMETER       = 2,
    GCU_INVALID_OPERATION       = 3,
    GCU_OUT_OF_MEMORY           = 4,

    GCU_ERROR_TYPE_FORCE_UINT   = GCU_MAX_UINT32
}GCU_ERROR_TYPE;



/*check if the compiler is of C++*/
#ifdef __cplusplus 
extern "C" {
#endif


/*!
*********************************************************************
*   \brief
*       GCU interface definition
*********************************************************************
*/

/* Initialization and termination API */
GCUbool gcuInitialize(GCU_INIT_DATA* pData);
GCUvoid gcuTerminate();

/* Context operation API */
GCUContext gcuCreateContext(GCU_CONTEXT_DATA* pData);
GCUvoid    gcuDestroyContext(GCUContext pContext);     
 
/* Surface creatation and destroy API */
GCUSurface gcuCreateSurface(GCUContext pContext, GCU_SURFACE_DATA* pData);
GCUvoid    gcuDestroySurface(GCUContext pContext, GCUSurface pSurface);

/* Query surface information API
   Only return basic info like width, height and format, but won't return alloc info.
*/
GCUbool gcuQuerySurfaceInfo(GCUContext pContext, GCUSurface pSurface, GCU_SURFACE_DATA* pData);

/* Surface updating API */                   
GCUbool gcuLockSurface(GCUContext pContext, GCU_SURFACE_LOCK_DATA* pData);
GCUvoid gcuUnlockSurface(GCUContext pContext, GCUSurface pSurface);     

/* Update pre-alloced surface address */                                                                                                                 
GCUbool gcuUpdateSurface(GCUContext pContext, GCU_SURFACE_UPDATE_DATA* pData);    


/* Rendering API */

/* Fill surface with solid color or pattern */ 
GCUvoid gcuFill(GCUContext pContext, GCU_FILL_DATA* pData);

/* General blit, used for copy, scaling, blending and format conversion */ 
GCUvoid gcuBlit(GCUContext pContext, GCU_BLT_DATA* pData);  

/* Blend blit, user should specify the blend mode in GCU_BLEND_DATA */
GCUvoid gcuBlend(GCUContext pContext, GCU_BLEND_DATA* pData);


/* FilterBlt by user specified filter kernel, only support one pass per API call now */                             
GCUvoid gcuSetFilter(GCUContext       pContext,
                     GCU_FILTER_TYPE    filterType, 
                     GCUint             filterSize, /* Only supports filter_size == 9 now */
                     GCUfloat*          pCoef);
GCUvoid gcuFilterBlit(GCUContext pContext, GCU_FILTER_BLT_DATA* pData);

/* Set context state for blend type, filter type, etc. */      
GCUbool gcuSet(GCUContext pContext, GCUenum state, GCUint value);  

/* Flush and wait completion API */
GCUvoid gcuFlush(GCUContext pContext);
GCUvoid gcuFinish(GCUContext pContext);

GCUFence    gcuCreateFence(GCUContext pContext);
GCUvoid     gcuDestroyFence(GCUContext pContext, GCUFence pFence);

GCUbool     gcuSendFence(GCUContext pContext, GCUFence pFence);
GCUbool     gcuWaitFence(GCUContext pContext, GCUFence pFence, GCUuint wait_time_ms);

/* Misc API */
GCUenum gcuGetError();
const char* gcuGetString(GCUenum name);

/*!
*********************************************************************
*   \brief
*       GCU helper functions
*********************************************************************
*/

/* Create and destroy buffer in video memory  */
GCUSurface _gcuCreateBuffer(GCUContext	 pContext,
								 unsigned int        width, 
                                 unsigned int        height, 
                                 GCU_FORMAT          format, 
                                 GCUVirtualAddr*     pVirtAddr, 
                                 GCUPhysicalAddr*    pPhysicalAddr);
                              
/* Create, update and destroy pre-allocated buffer */
GCUSurface _gcuCreatePreAllocBuffer(GCUContext	    pContext,
                                    unsigned int        width, 
                                    unsigned int        height, 
                                    GCU_FORMAT          format,  
                                    GCUbool             bPreAllocVirtual,      
                                    GCUVirtualAddr      virtualAddr, 
                                    GCUbool             bPreAllocPhysical,
                                    GCUPhysicalAddr     physicalAddr);

/* When updating pre-alloc surface, the pre-alloc flag should be same as the 
   values you create this surface. Else, this function will directly return.
*/ 
GCUbool    _gcuUpdatePreAllocBuffer(GCUContext	 pContext,
                                    GCUSurface          pSurface,
                                    GCUbool             bPreAllocVirtual,      
                                    GCUVirtualAddr      virtualAddr, 
                                    GCUbool             bPreAllocPhysical,
                                    GCUPhysicalAddr     physicalAddr);

void       _gcuDestroyBuffer(GCUContext	 pContext, GCUSurface pSurface);




/* Create surface from bmp file, surface format is ARGB8888 */   
GCUSurface _gcuLoadRGBSurfaceFromFile(GCUContext    pContext, 
                                               const char*   filename);

/* Create surface from raw yuv file, user needs to provide format, width and height */
GCUSurface _gcuLoadYUVSurfaceFromFile(GCUContext    pContext, 
                                              const char*   filename, 
                                              GCU_FORMAT    format, 
                                              GCUuint       width, 
                                              GCUuint       height);

/* Save surface content to bmp or yuv file, file type decided by surface format */
GCUbool    _gcuDumpSurface(GCUContext pContext, GCUSurface pSurface, const char* prefix);
    
/*check if the compiler is of C++*/
#ifdef __cplusplus 
}
#endif

#endif /* __GCU_H__ */




