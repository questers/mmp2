#ifndef __glext_mrvl_h_
#define __glext_mrvl_h_


#define GL_BGRA                                                 0x80E1



/* GL_EXT_texture_compression_dxt1 */
#ifndef GL_EXT_texture_compression_dxt1
#define COMPRESSED_RGB_S3TC_DXT1_EXT							0x83F0
#define COMPRESSED_RGBA_S3TC_DXT1_EXT							0x83F1
#endif

/*------------------------------------------------------------------------*
 * IMG extension tokens
 *------------------------------------------------------------------------*/

/* GL_IMG_read_format */
#ifndef GL_IMG_read_format
#define GL_BGRA                                                 0x80E1
#define GL_UNSIGNED_SHORT_4_4_4_4_REV                           0x8365
#define GL_UNSIGNED_SHORT_1_5_5_5_REV                           0x8366
#endif

/* GL_IMG_user_clip_plane */
#ifndef GL_IMG_user_clip_plane
#define GL_IMG_user_clip_plane 1
#ifdef GL_GLEXT_PROTOTYPES
GL_API void GL_APIENTRY glClipPlanefIMG (GLenum, const GLfloat *);
GL_API void GL_APIENTRY glClipPlanexIMG (GLenum, const GLfixed *);
#endif
typedef void (GL_APIENTRYP PFNGLCLIPPLANEFIMG) (GLenum p, const GLfloat *eqn);
typedef void (GL_APIENTRYP PFNGLCLIPPLANEXIMG) (GLenum p, const GLfixed *eqn);
#endif

/*------------------------------------------------------------------------*
 * Vivante's OES extension functions
 *------------------------------------------------------------------------*/

/* All known OpenGL ES 1.1 extensions listed in glext.h we don't support
  (yet). */
 /* #define GL_OES_blend_equation_separate		0 */
 /* #define GL_OES_blend_func_separate			0 */
 /* #define GL_OES_blend_subtract				0 */
 /* #define GL_OES_depth24						0 */
 /* #define GL_OES_depth32						0 */
 /* #define GL_OES_element_index_uint			0 */
 /* #define GL_OES_extended_matrix_palette		0 */
 /* #define GL_OES_fbo_render_mipmap			0 */
 /* #define GL_OES_framebuffer_object			0 */
 /* #define GL_OES_packed_depth_stencil			0 */
 /* #define GL_OES_rgb8_rgba8					0 */
 /* #define GL_OES_stencil1						0 */
 /* #define GL_OES_stencil4						0 */
 /* #define GL_OES_stencil8						0 */
 /* #define GL_OES_stencil_wrap					0 */
/*#define GL_OES_texture_cube_map				0   Open it just for enum defines, we don't support it still */
 /* #define GL_OES_texture_env_crossbar			0 */
 /* #define GL_OES_texture_mirrored_repeat		0 */
 /* #define GL_AMD_compressed_3DC_texture		0 */
 /* #define GL_AMD_compressed_ATC_texture		0 */
 /* #define GL_EXT_texture_filter_anisotropic	0 */

#if !defined(GL_GLEXT_PROTOTYPES)
# define GL_GLEXT_PROTOTYPES
#endif
#include <GLES/glext.h>

/*------------------------------------------------------------------------*
 * VIV extension tokens
 *------------------------------------------------------------------------*/

/* GL_VIV_direct_texture */
#ifndef GL_VIV_direct_texture
#define GL_VIV_YV12												0x8FC0
#define GL_VIV_NV12												0x8FC1
#define GL_VIV_YUY2												0x8FC2
#define GL_VIV_UYVY												0x8FC3
#endif


/* From OpenGL Texture coordinate generation */
#define GL_OBJECT_LINEAR			0x2401
#define GL_OBJECT_PLANE				0x2501
#define GL_EYE_LINEAR				0x2400
#define GL_EYE_PLANE				0x2502
#define GL_SPHERE_MAP				0x2402

#define GL_TEXTURE_GEN_S			0x0C60
#define GL_TEXTURE_GEN_T			0x0C61
#define GL_TEXTURE_GEN_MODE			0x2500

#define GL_S					0x2000
#define GL_T					0x2001
#define GL_R					0x2002
#define GL_Q					0x2003

#define GL_TEXTURE_GEN_R			0x0C62
#define GL_TEXTURE_GEN_Q			0x0C63

/*------------------------------------------------------------------------*
 * VIV extension functions
 *------------------------------------------------------------------------*/

/* GL_VIV_direct_texture */
#ifndef GL_VIV_direct_texture
#define GL_VIV_direct_texture 1
#ifdef GL_GLEXT_PROTOTYPES
GL_API void GL_APIENTRY glTexDirectVIV (GLenum Target, GLsizei Width, GLsizei Height, GLenum Format, GLvoid ** Pixels);
GL_API void GL_APIENTRY glTexDirectVIVMap (GLenum Target, GLsizei Width, GLsizei Height, GLenum Format, GLvoid * Logic, GLuint Physical); 
GL_API void GL_APIENTRY glTexDirectInvalidateVIV (GLenum Target);
GL_API GLvoid * GL_APIENTRY glCreateFence(void);
GL_API void GL_APIENTRY glDestroyFence(GLvoid* );
GL_API void GL_APIENTRY glSendFence(GLvoid* );
GL_API void GL_APIENTRY glWaitFence(GLvoid* );
#endif
typedef void (GL_APIENTRYP PFNGLTEXDIRECTVIVPROC) (GLenum Target, GLsizei Width, GLsizei Height, GLenum Format, GLvoid ** Pixels);
typedef void (GL_APIENTRYP PFNGLTEXDIRECTINVALIDATEVIVPROC) (GLenum Target);
#endif


/*------------------------------------------------------------------------*
 * MRVL extension tokens and functions
 *------------------------------------------------------------------------*/

/* GL_MRVL_blur */
#ifndef GL_MRVL_blur
#define GL_MRVL_blur    1
#define GL_BLUR_MRVL    0x8FD0
#endif


/*-------------------------------------------------------------------------
 * Definitions of legacy defines.
 *-----------------------------------------------------------------------*/
#ifndef GL_OES_VERSION_1_1
#	define GL_OES_VERSION_1_1
#endif
#ifndef GL_WRITE_ONLY
#	define GL_WRITE_ONLY				0x88B9
#endif
#ifndef GL_BUFFER_ACCESS
#	define GL_BUFFER_ACCESS				0x88BB
#endif
#ifndef GL_BUFFER_MAPPED
#	define GL_BUFFER_MAPPED				0x88BC
#endif
#ifndef GL_MAX_ELEMENTS_VERTICES
#	define GL_MAX_ELEMENTS_VERTICES		0x80E8
#endif
#ifndef GL_MAX_ELEMENTS_INDICES
#	define GL_MAX_ELEMENTS_INDICES		0x80E9
#endif

#endif
