ifneq ($(TARGET_SIMULATOR), true) # not 64 bit clean

###################################################################
# Build JNI Shared Library
# ##################################################################

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

PATH_USR_MAN     := example/manchac/src
PATH_USR_GPU     := example/gpu
PATH_USR_JNI     := example/jni_interfaces
PATH_USR_VMETA   := example/vmetadec/src
PATH_IPP_INC 	 := $(LOCAL_PATH)/../../../include

# CPU can be [mg1 | dove | mmp2]
CPU         := mg1
VMETA_VER   := 17

# Optional tag would mean it doesn't get installed by default
#LOCAL_MODULE_TAGS := optional 

LOCAL_SRC_FILES :=                          \
$(PATH_USR_MAN)/mmain.c                     \
$(PATH_USR_MAN)/mperf.c                     \
$(PATH_USR_MAN)/mpara.c                     \
$(PATH_USR_MAN)/wmmx2_linux/perf_counter.c  \
$(PATH_USR_GPU)/demo.cpp                    \
$(PATH_USR_GPU)/glu_glutlib.cpp             \
$(PATH_USR_GPU)/utils.cpp                   \
$(PATH_USR_JNI)/compiz_jni_lib.c            \
$(PATH_USR_JNI)/compiz_jni_activity.cpp     \
$(PATH_USR_VMETA)/queue.c

LOCAL_CFLAGS := -DPLATFORM_PJ5 -D_DISPLAY_LCDDRIVER_NEW -DCORE_FREQ=800 -DLOG_OFF -DANDROID=1 
LOCAL_CFLAGS += -D_IPP_LINUX -D_VMETA_VER=$(VMETA_VER) -D_VMETADEC -DHWCODEC -DUSE_APK=1
LOCAL_CFLAGS += -DPURE_VMETA=0 -D__LINUX__ -DWINDOW_WIDTH=480 -DWINDOW_HEIGHT=640
#LOCAL_CFLAGS += -DDBG_MODE -DTHREAD_DEBUG
#LOCAL_CFLAGS += -DINFINITE_LOOP

LOCAL_SHARED_LIBRARIES :=              \
    libvmetahal                        \
    libcodecvmetadec                   \
    libbmm                             \
    libvmeta                           \
    libGAL                             \
    libEGL                             \
    libGLESv1_CM 		       		   \
    libstdc++                          \
    libdl                              \
    libui                              \
    liblog                             \
    libandroid_runtime                 \
    libnativehelper                    \
    libutils                           \
    libmiscgen

LOCAL_LDLIBS :=                        \
    -lpthread                          \
    -lm                                \
    -lrt  

LOCAL_MODULE := libcompiz 

LOCAL_ARM_MODE := arm

LOCAL_PRELINK_MODULE := false

LOCAL_C_INCLUDES :=                           \
    $(LOCAL_PATH)/example/gpu                 \
    $(JNI_H_INCLUDE)                          \
    $(LOCAL_PATH)/$(PATH_USR_JNI)             \
    $(LOCAL_PATH)/$(PATH_USR_MAN)             \
    $(LOCAL_PATH)/$(PATH_USR_MAN)/wmmx2_linux \
    $(PATH_IPP_INC)  						  \
	$(LOCAL_PATH)/sdk/android_$(CPU)/include  \
    $(LOCAL_PATH)/sdk/X11_ARM/include         \
    $(LOCAL_PATH)/$(PATH_USR_VMETA)                         

include $(BUILD_SHARED_LIBRARY)

endif # TARGET_SIMULATOR
