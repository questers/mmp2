/***************************************************************************************** 
Copyright (c) 2009, Marvell International Ltd. 
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Marvell nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MARVELL ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MARVELL BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************/

/*
 * change word order if big-endian 
 */  

typedef unsigned int __juint;
typedef long long int __int64; 

extern int doUseNewCP15;

typedef union {
	__int64 val;
	struct {
#ifdef __ARMEL__ /* default, -mlittle-endian */
		__juint lo64;
		int hi64;
#else /* __ARMEB__, -mbig-endian */
		int hi64;
		__juint lo64;
#endif
	} s;
} marvell_counter;

/*********** register 11, performance counter control(extension) **********/
/* Custom counters (Counter0, Counter1, Counter2, Counter3) */
/* Programming Details: */
#define DISABLE_COUNTER		0X0

#ifdef FALCON_COUNTER /* Old counter */
	/* Counter0: */
	#define C0_CYCLE_COUNT			0X00000001
#else /* New counter */
	/* Counter0: */
	#define C0_CYCLE_COUNT			0X00000003
	#define C0_DCACHE_READ_HIT		0X00000005
	#define C0_DCACHE_READ_MISS		0X00000009
	#define C0_DCACHE_WRITE_HIT		0X00000011
	#define C0_DCACHE_WRITE_MISS		0X00000021
	#define C0_RETIRED_INSTRUCTION		0X00000041
	#define C0_MMU_BUS_REQUEST_READ_LATENCY	0X00000401
	#define C0_ICACHE_BUS_REQUEST_READ_LATENCY	0X00000801
	#define C0_WB_BUS_REQUEST_WRITE_LATENCY		0X00001001
	#define C0_HOLD_LDSTM			0X00002001
	#define C0_NO_DUAL_CFLAG		0X00004001
	#define C0_NO_DUAL_REGFILE		0X00008001
	#define C0_PREDICTED_BRANCH		0X02000001
	#define C0_L2_WRITE_HIT			0X20000001
	#define C0_L2_WRITE_MISS		0X40000001
	#define C0_L2_READ_COUNT		0x80000001


	/* Counter1: */
	#define C1_CYCLE_COUNT			0X00000003
	#define C1_ICACHE_READ_MISS		0X00000005
	#define C1_DCACHE_READ_MISS		0X00000009
	#define C1_DCACHE_WRITE_MISS		0X00000011
	#define C1_ITLB_MISS			0X00000021
	#define C1_SINGLE_ISSUE			0X00000041
	#define C1_BRANCH_RETIRED		0X00000101
	#define C1_ROB_FULL			0X00000201
	#define C1_MMU_READ_COUNT		0X00000401
	#define C1_ICACHE_READ_COUNT		0X00000801
	#define C1_WB_WRITE_COUNT		0X00001001
	#define C1_DUAL_ISSUE			0X00002001
	#define C1_NO_DUAL_RAW			0X00004001
	#define C1_HOLD_IS			0X00008001
	#define C1_L2_WRITE_HIT			0x20000001
	#define C1_L2_WRITE_MISS		0X40000001

	/* Counter2 */
	#define C2_CYCLE_COUNT			0X00000003
	#define C2_DCACHE_ACCESS		0X00000009
	#define C2_DTLB_MISS			0X00000011
	#define C2_BRANCH_PREDICT_MISS		0X00000101
	#define C2_WB_WRITE_COUNT		0X00000201
	#define C2_A1_STALL			0X00000401
	#define C2_DCACHE_READ_LATENCY		0X00000801
	#define C2_DCACHE_WRITE_LATENCY		0X00001001
	#define C2_NO_DUAL_REGFILE		0X00004001
	#define C2_BIU_SIMULTANEOUS_ACCESS	0X00010001
	#define C2_L2_READ_HIT			0x20000001
	#define C2_L2_READ_MISS			0x40000001

	/* Counter3 */
	#define C3_CYCLE_COUNT			0X00000003
	#define C3_DCACHE_READ_MISS		0X00000005
	#define C3_DCACHE_WRITE_MISS		0X00000009
	#define C3_TLB_MISS			0X00000011
	#define C3_BRANCHES_TAKEN		0X00000101
	#define C3_WB_FULL			0X00000201
	#define C3_DCACHE_READ_COUNT		0X00000801
	#define C3_DCACHE_WRITE_COUNT		0X00001001
	#define C3_NO_DUAL_HW			0X00004001
	#define C3_NO_DUAL_SIMULATANEOUS	0X00008001
	#define C3_BIU_SINGLE_ACCESS		0X00010001
	#define C3_L2_READ_HIT			0X20000001
	#define C3_L2_READ_MISS			0X40000001
#endif

#define MYINLINE inline static


MYINLINE void reset_all_counters() {

		__asm__ __volatile__ (
		"mov	r0, #0x0 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c12, 0 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c12, 1 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c12, 2 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c12, 3 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 0 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 1 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 2 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 3 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 4 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 5 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 6 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 7 @cnt" : : : "r0");
}

MYINLINE void reset_counter0() {

		__asm__ __volatile__ (
		"mov	r0, #0x0 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c12, 0 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 0 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 1 @cnt" : : : "r0");
}

MYINLINE void reset_counter1() {

		__asm__ __volatile__ (
		"mov	r0, #0x0 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c12, 1 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 2 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 3 @cnt" : : : "r0");
}

MYINLINE void reset_counter2() {

		__asm__ __volatile__ (
		"mov	r0, #0x0 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c12, 2 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 4 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 5 @cnt" : : : "r0");
}


MYINLINE void reset_counter3() {

		__asm__ __volatile__ (
		"mov	r0, #0x0 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c12, 3 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 6 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c13, 7 @cnt" : : : "r0");
}

MYINLINE void set_counter_types(__juint counter0, __juint counter1, __juint counter2, __juint counter3) {
	__asm__ __volatile__ (
		"mcr	p15, 0, %0, c15, c12, 0 @cnt\n\t"
		"mcr	p15, 0, %1, c15, c12, 1 @cnt\n\t"
		"mcr	p15, 0, %2, c15, c12, 2 @cnt\n\t"
		"mcr	p15, 0, %3, c15, c12, 3 @cnt" : : "r" (counter0), "r" (counter1), "r" (counter2), "r" (counter3));
}

MYINLINE void stop_all_counters() {
	__asm__ __volatile__ (
		"mov	r0, #0x0 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c12, 0 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c12, 1 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c12, 2 @cnt\n\t"
		"mcr	p15, 0, r0, c15, c12, 3 @cnt" : : : "r0");
}

#ifdef FALCON_COUNTER

/* set_counter */
MYINLINE void set_counter0(__juint counter0) {
	__asm__ __volatile__ (
		"mcr	p15, 0, %0, c11, c0, 0 @mrvl:set_counter0" : : "r" (counter0));
}
/* stop_counter */
MYINLINE void stop_counter0() {
	set_counter0(0);
}

/* get_counter */
MYINLINE __juint get_counter0() {
	__juint counter;
	__asm__ __volatile__ (
		"mrc	p15, 0, %0, c12, c0, 0 @mrvl:get_counter0" : "=r" (counter));
	return counter;
}

#else

#define EXPAND_COUNTER_FUNTCIONS(cnum, clo64, chi64)\
/* set_counter */\
MYINLINE void set_counter##cnum(__juint counter##cnum) {\
	__asm__ __volatile__ (\
		"mcr	p15, 0, %0, c15, c12, "#cnum" @cnt" : : "r" (counter##cnum));\
}\
/* stop_counter */\
MYINLINE void stop_counter##cnum() {\
	set_counter##cnum(0);\
}\
/* get_counter */\
MYINLINE __int64 get_counter##cnum() {\
	marvell_counter counter;\
	__asm__ __volatile__ (\
		"mrc	p15, 0, %0, c15, c13, "#clo64" @mrvl:get_counter"#cnum"\n\t"\
		"mrc	p15, 0, %1, c15, c13, "#chi64" @mrvl:get_counter"#cnum : "=r" (counter.s.lo64), "=r" (counter.s.hi64));\
	return counter.val;\
}

EXPAND_COUNTER_FUNTCIONS(0, 0, 1)
EXPAND_COUNTER_FUNTCIONS(1, 2, 3)
EXPAND_COUNTER_FUNTCIONS(2, 4, 5)
EXPAND_COUNTER_FUNTCIONS(3, 6, 7)

#endif

MYINLINE void get_all_counters(__int64 *c0, __int64 *c1, __int64 *c2, __int64 *c3) {
	if(c0)
		*c0 = get_counter0();
#ifndef FALCON_COUNTER
	if(c1)
		*c1 = get_counter1();
	if(c2)
		*c2 = get_counter2();
	if(c3)
		*c3 = get_counter3();
#endif
}


/* EOF */

