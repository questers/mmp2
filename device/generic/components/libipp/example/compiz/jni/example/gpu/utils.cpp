/******************************************************************************
*(C) Copyright 2007 Marvell International Ltd.
* All Rights Reserved 
******************************************************************************/

#include "utils.h"
/*
void getErr()
{
	GLenum errCode;
	const GLubyte *errString;
	if ((errCode = glGetError()) != GL_NO_ERROR)
	{
		errString = gluErrorString(errCode);
		printf((char *)errString);
	}
}*/
char tochar(int cipher)
{
	switch(cipher)
	{
		case 0: return '0'; break;
		case 1: return '1'; break;
		case 2: return '2'; break;
		case 3: return '3'; break;
		case 4: return '4'; break;
		case 5: return '5'; break;
		case 6: return '6'; break;
		case 7: return '7'; break;
		case 8: return '8'; break;
		case 9: return '9'; break;
		default: return '\0';
	}
}

char * tostr(int val)
{
	bool minzero=false;
	if (val<0)
	{
		val=-val;
		minzero=true;
	}

	int * ciphers;
	char * str;
	int num=(int)(log10((float)val)+1);
	if (minzero) num++;
	if (val==0) return "0";
	ciphers=new int [num+1];
	str = new char[num+1];
	int d=1;
	for (int i=1; i<=num; i++)
	{
		d=1;
		for (int c=1; c<=i; c++) d*=10;
		ciphers[i]=(val%d)/(d/10);
		if (minzero) str[num-1-i]=tochar(ciphers[i]);
		else str[num-i]=tochar(ciphers[i]);
	}
	if (minzero) {str [num-1]='-';str[num]='\0';} else str [num]='\0';
	return str;
}
int power(int base,int exp)
{
	int res=1;
	for (int i=0; i<exp; i++) res*=base;
	return res;
}
int toint(char* str)
{
	int res=0;
	for (int i= (int)strlen(str)-1; i>=0; i--)
	{
		char ch=str[strlen(str)-1-i];
		res+=power(10,i)*(ch-48);
	}
	return res;
}
char * buildgamestring(int* params)
{
	char * res;
	res=strcat(tostr(params[1]),"x");
	res=strcat(res,tostr(params[2]));
	res=strcat(res,":");
	res=strcat(res,tostr(params[3]));
	res=strcat(res,"@");
	res=strcat(res,tostr(params[4]));
	return res;
}
#ifdef __LINUX__
#include <sys/time.h>
unsigned long long GetTickCount()
{
	struct timeval tv;
	struct timezone tz;
	unsigned long long tickcount = 0;
	gettimeofday(&tv,&tz);
	tickcount = (tv.tv_sec*((unsigned long long) 1000000)+tv.tv_usec);
	return(tickcount);
}
#endif
