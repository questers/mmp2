/******************************************************************************
// (C) Copyright 2009 Marvell International Ltd.
// All Rights Reserved
******************************************************************************/
#ifndef _QUEUE_H
#define _QUEUE_H

#include "misc.h"
#include <pthread.h>
#include "vdec_wrapper.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* This value should be larger than PICTURE_BUF_NUM and STRM_BUF_NUM */
#define BUFQUEUE_MAX 					16
#define BUFLIST_MAX						(PICTURE_BUF_NUM*MAX_MULTISTRM_NUM)


typedef void BufType;
typedef struct _BufQueue
{
	pthread_mutex_t	hMutex;
    pthread_cond_t  pEventNotEmpty;
    pthread_cond_t  pEventNotFull;

	BufType			*buffers[BUFQUEUE_MAX];
	unsigned int	headIndex;
	unsigned int	count;
} BufQueue;

typedef struct _PicGrpQueue
{
    pthread_mutex_t hMutex; 
    pthread_cond_t  pEventNotEmpty;
    pthread_cond_t  pEventReachThres;
    pthread_cond_t  pEventConsumeIncr;

    BufQueue        fifo[MAX_MULTISTRM_NUM];

    unsigned int    count;
    long int        latest_frameID[MAX_MULTISTRM_NUM];
    long int        old_frameID;
    long int        consumeID;
    int             thresh;
    int             channel_count;
    int             IsChannelValid[MAX_MULTISTRM_NUM];
} PicGrpQueue;

typedef struct _SortedNode
{
	BufType			    *pBuffer;
	long int	        frameID;
	struct _SortedNode  *pNext;
} SortedNode;

typedef struct _SortedList
{
	pthread_mutex_t	hMutex;
    pthread_cond_t  pEventNotEmpty;
    pthread_cond_t  pEventNotFull;
    pthread_cond_t  pEventReachThres;

	SortedNode 	    *pHead;
    SortedNode      *pNotUsedNodes;
    SortedNode      *pSortNodesList;
	int	            count;
    int             thresh;
} SortedList;

int BufQueue_Init(BufQueue *pq);
void BufQueue_DeInit(BufQueue *pq);
int BufQueue_EnQueue(BufQueue *pq,  BufType *pBufferHeader);
int BufQueue_DeQueue(BufQueue *pq,  BufType **ppBufferHeader);
int BufQueue_TryDeQueue(BufQueue *pq, BufType **ppBufferHeader);
int BufQueue_TimedDeQueue(BufQueue *pq, BufType **ppBufferHeader, unsigned int ms);
void BufQueue_Flush(BufQueue *pq);
int IsBufQueue_Empty(BufQueue *pq);
int BufQueue_Count(BufQueue *pq);

int  PicGrpQueue_Init(PicGrpQueue *pGrpQueue, int channel_count, int thresh);
void PicGrpQueue_DeInit(PicGrpQueue *pGrpQueue);
int  PicGrpQueue_EnQueue(PicGrpQueue *pGrpQueue,  
                        void *pBufferHeader, 
                        int channel_id, 
                        long int frame_id);
int PicGrpQueue_DeQueue(PicGrpQueue *pGrpQueue, void *pPicGroup[MAX_MULTISTRM_NUM]);
int PicGrpQueue_TryDeQueue(PicGrpQueue *pGrpQueue, void *pPicGroup[MAX_MULTISTRM_NUM]);
int PicGrpQueue_TimedDeQueue(PicGrpQueue *pGrpQueue, 
                             void *pPicGroup[MAX_MULTISTRM_NUM], 
                             unsigned int ms);
void PicGrpQueue_DisableChannel(PicGrpQueue *pGrpQueue, int channel_id);
int  PicGrpQueue_IsChannelValid(PicGrpQueue *pGrpQueue, int channel_id);
int  PicGrpQueue_WaitThresh(PicGrpQueue *pGrpQueue, unsigned int ms);
int  PicGrpQueue_SetThresh(PicGrpQueue *pGrpQueue, int thresh);

int SortedList_Init(SortedList *pList, int thresh);
void SortedList_DeInit(SortedList *pList);
int SortedList_EnList(SortedList *pList,  BufType *pBufferHeader, long int frameID);
int SortedList_DeList(SortedList *pList,  BufType **ppBufferHeader);
int SortedList_TryDeList(SortedList *pList, BufType **ppBufferHeader);
int IsSortedList_Full(SortedList *pList);
int IsSortedList_Empty(SortedList *pList);
int SortedList_TimedWaitBuffer(SortedList *pList, unsigned int ms);
int SortedList_Count(SortedList *pList);
void SortedList_SetThresh(SortedList *pList, int thresh);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif

/* EOF */


