/******************************************************************************
*(C) Copyright 2007 Marvell International Ltd.
* All Rights Reserved 
******************************************************************************/

#include "glu_glutlib.h"

static GLfloat dodec[20][3];
static MRVL_GLUquadricObj *quadObj;

#define UINT_MAX     0xffffffff

static int computeLog(GLuint value)
{
	int i = 0;
	/* Error! */
	if (value == 0) return -1;

	for (;;) {
		if (value & 1) {
			/* Error ! */
			if (value != 1) return -1;
			return i;
		}
		value = value >> 1;
		i++;
	}
}

static int nearestPower(GLuint value)
{
	int i = 1;

	/* Error! */
	if (value == 0) return -1;

	for (;;) {
		if (value == 1) {
			return i;
		} else if (value == 3) {
			return i*4;
		}
		value = value >> 1;
		i *= 2;
	}
}

static void retrieveStoreModes(PixelStorageModes *psm)
{
	glGetIntegerv(GL_UNPACK_ALIGNMENT, &psm->unpack_alignment);
//	glGetIntegerv(GL_UNPACK_ROW_LENGTH, &psm->unpack_row_length);
//	glGetIntegerv(GL_UNPACK_SKIP_ROWS, &psm->unpack_skip_rows);
//	glGetIntegerv(GL_UNPACK_SKIP_PIXELS, &psm->unpack_skip_pixels);
//	glGetIntegerv(GL_UNPACK_LSB_FIRST, &psm->unpack_lsb_first);
//	glGetIntegerv(GL_UNPACK_SWAP_BYTES, &psm->unpack_swap_bytes);

	glGetIntegerv(GL_PACK_ALIGNMENT, &psm->pack_alignment);
//	glGetIntegerv(GL_PACK_ROW_LENGTH, &psm->pack_row_length);
//	glGetIntegerv(GL_PACK_SKIP_ROWS, &psm->pack_skip_rows);
//	glGetIntegerv(GL_PACK_SKIP_PIXELS, &psm->pack_skip_pixels);
//	glGetIntegerv(GL_PACK_LSB_FIRST, &psm->pack_lsb_first);
//	glGetIntegerv(GL_PACK_SWAP_BYTES, &psm->pack_swap_bytes);
}

static GLint elements_per_group(GLenum format, GLenum type)
{
	/*
	* Return the number of elements per group of a specified format
	*/
	/* Types are not packed pixels, so get elements per group */
	switch(format) {
	  case GL_RGB:
		  return 3;
	  case GL_LUMINANCE_ALPHA:
		  return 2;
	  case GL_RGBA:
		  return 4;
	  default:
		  return 1;
	}
}

static GLint bytes_per_element(GLenum type)
{
	/*
	* Return the number of bytes per element, based on the element type
	*/
	switch(type) {
	  case GL_UNSIGNED_SHORT:
		  return(sizeof(GLushort));
	  case GL_SHORT:
		  return(sizeof(GLshort));
	  case GL_UNSIGNED_BYTE:
		  return(sizeof(GLubyte));
	  case GL_BYTE:
		  return(sizeof(GLbyte));
	  case GL_FIXED:
		  return(sizeof(GLfixed));
	  case GL_FLOAT:
		  return(sizeof(GLfloat));
	  default:
		  return 4;
	}
}

static GLint image_size(GLint width, GLint height, GLenum format, GLenum type)
{
	int bytes_per_row;
	int components;

	components = elements_per_group(format,type);

	bytes_per_row = bytes_per_element(type) * width;

	return bytes_per_row * height * components;
}


//////////////////////////////////////////////////////////////////////////
static void halve1Dimage_ubyte(GLint components, GLuint width, GLuint height,
							   const GLubyte *dataIn, GLubyte *dataOut,
							   GLint element_size, GLint ysize,
							   GLint group_size)
{
	GLint halfWidth= width / 2;
	GLint halfHeight= height / 2;
	const char *src= (const char *) dataIn;
	GLubyte *dest= dataOut;
	int jj;


	if (height == 1) {		/* 1 row */
		halfHeight= 1;
		for (jj= 0; jj< halfWidth; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
				*dest= (*(const GLubyte*)src +
					*(const GLubyte*)(src+group_size)) / 2;

				src+= element_size;
				dest++;
			}
			src+= group_size;	/* skip to next 2 */
		}
		{
			int padBytes= ysize - (width*group_size);
			src+= padBytes;	/* for assertion only */
		}
	}
	else if (width == 1) {	/* 1 column */
		int padBytes= ysize - (width * group_size);
		halfWidth= 1;
		/* one vertical column with possible pad bytes per row */
		/* average two at a time */

		for (jj= 0; jj< halfHeight; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
				*dest= (*(const GLubyte*)src + *(const GLubyte*)(src+ysize)) / 2;

				src+= element_size;
				dest++;
			}
			src+= padBytes; /* add pad bytes, if any, to get to end to row */
			src+= ysize;
		}
	}
} /* halve1Dimage_ubyte() */

static void halveImage_ubyte(GLint components, GLuint width, GLuint height,
							 const GLubyte *datain, GLubyte *dataout,
							 GLint element_size, GLint ysize, GLint group_size)
{
	int i, j, k;
	int newwidth, newheight;
	int padBytes;
	GLubyte *s;
	const char *t;

	/* handle case where there is only 1 column/row */
	if (width == 1 || height == 1) {
		halve1Dimage_ubyte(components,width,height,datain,dataout,
			element_size,ysize,group_size);
		return;
	}

	newwidth = width / 2;
	newheight = height / 2;
	padBytes = ysize - (width*group_size);
	s = dataout;
	t = (const char *)datain;

	/* Piece o' cake! */
	for (i = 0; i < newheight; i++) {
		for (j = 0; j < newwidth; j++) {
			for (k = 0; k < components; k++) {
				s[0] = (*(const GLubyte*)t +
					*(const GLubyte*)(t+group_size) +
					*(const GLubyte*)(t+ysize) +
					*(const GLubyte*)(t+ysize+group_size) + 2) / 4;
				s++; t += element_size;
			}
			t += group_size;
		}
		t += padBytes;
		t += ysize;
	}
}

static void halve1Dimage_byte(GLint components, GLuint width, GLuint height,
							  const GLbyte *dataIn, GLbyte *dataOut,
							  GLint element_size,GLint ysize, GLint group_size)
{
	GLint halfWidth= width / 2;
	GLint halfHeight= height / 2;
	const char *src= (const char *) dataIn;
	GLbyte *dest= dataOut;
	int jj;

	if (height == 1) {		/* 1 row */
		halfHeight= 1;

		for (jj= 0; jj< halfWidth; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
				*dest= (*(const GLbyte*)src + *(const GLbyte*)(src+group_size)) / 2;

				src+= element_size;
				dest++;
			}
			src+= group_size;	/* skip to next 2 */
		}
		{
			int padBytes= ysize - (width*group_size);
			src+= padBytes;	/* for assertion only */
		}
	}
	else if (width == 1) {	/* 1 column */
		int padBytes= ysize - (width * group_size);
		halfWidth= 1;
		/* one vertical column with possible pad bytes per row */
		/* average two at a time */

		for (jj= 0; jj< halfHeight; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
				*dest= (*(const GLbyte*)src + *(const GLbyte*)(src+ysize)) / 2;

				src+= element_size;
				dest++;
			}
			src+= padBytes; /* add pad bytes, if any, to get to end to row */
			src+= ysize;
		}
	}
} /* halve1Dimage_byte() */

static void halveImage_byte(GLint components, GLuint width, GLuint height,
							const GLbyte *datain, GLbyte *dataout,
							GLint element_size,
							GLint ysize, GLint group_size)
{
	int i, j, k;
	int newwidth, newheight;
	int padBytes;
	GLbyte *s;
	const char *t;

	/* handle case where there is only 1 column/row */
	if (width == 1 || height == 1) {
		halve1Dimage_byte(components,width,height,datain,dataout,
			element_size,ysize,group_size);
		return;
	}

	newwidth = width / 2;
	newheight = height / 2;
	padBytes = ysize - (width*group_size);
	s = dataout;
	t = (const char *)datain;

	/* Piece o' cake! */
	for (i = 0; i < newheight; i++) {
		for (j = 0; j < newwidth; j++) {
			for (k = 0; k < components; k++) {
				s[0] = (*(const GLbyte*)t +
					*(const GLbyte*)(t+group_size) +
					*(const GLbyte*)(t+ysize) +
					*(const GLbyte*)(t+ysize+group_size) + 2) / 4;
				s++; t += element_size;
			}
			t += group_size;
		}
		t += padBytes;
		t += ysize;
	}
}

static void halve1Dimage_ushort(GLint components, GLuint width, GLuint height,
								const GLushort *dataIn, GLushort *dataOut,
								GLint element_size, GLint ysize,
								GLint group_size, GLint myswap_bytes)
{
	GLint halfWidth= width / 2;
	GLint halfHeight= height / 2;
	const char *src= (const char *) dataIn;
	GLushort *dest= dataOut;
	int jj;

	if (height == 1) {		/* 1 row */
		halfHeight= 1;

		for (jj= 0; jj< halfWidth; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
#define BOX2 2
				GLushort ushort[BOX2];
				if (myswap_bytes) {
					ushort[0]= __GLU_SWAP_2_BYTES(src);
					ushort[1]= __GLU_SWAP_2_BYTES(src+group_size);
				}
				else {
					ushort[0]= *(const GLushort*)src;
					ushort[1]= *(const GLushort*)(src+group_size);
				}

				*dest= (ushort[0] + ushort[1]) / 2;
				src+= element_size;
				dest++;
			}
			src+= group_size;	/* skip to next 2 */
		}
		{
			int padBytes= ysize - (width*group_size);
			src+= padBytes;	/* for assertion only */
		}
	}
	else if (width == 1) {	/* 1 column */
		int padBytes= ysize - (width * group_size);
		halfWidth= 1;
		/* one vertical column with possible pad bytes per row */
		/* average two at a time */

		for (jj= 0; jj< halfHeight; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
#define BOX2 2
				GLushort ushort[BOX2];
				if (myswap_bytes) {
					ushort[0]= __GLU_SWAP_2_BYTES(src);
					ushort[1]= __GLU_SWAP_2_BYTES(src+ysize);
				}
				else {
					ushort[0]= *(const GLushort*)src;
					ushort[1]= *(const GLushort*)(src+ysize);
				}
				*dest= (ushort[0] + ushort[1]) / 2;

				src+= element_size;
				dest++;
			}
			src+= padBytes; /* add pad bytes, if any, to get to end to row */
			src+= ysize;
		}
	}
} /* halve1Dimage_ushort() */

static void halveImage_ushort(GLint components, GLuint width, GLuint height,
							  const GLushort *datain, GLushort *dataout,
							  GLint element_size, GLint ysize, GLint group_size,
							  GLint myswap_bytes)
{
	int i, j, k;
	int newwidth, newheight;
	int padBytes;
	GLushort *s;
	const char *t;

	/* handle case where there is only 1 column/row */
	if (width == 1 || height == 1) {
		halve1Dimage_ushort(components,width,height,datain,dataout,
			element_size,ysize,group_size, myswap_bytes);
		return;
	}

	newwidth = width / 2;
	newheight = height / 2;
	padBytes = ysize - (width*group_size);
	s = dataout;
	t = (const char *)datain;

	/* Piece o' cake! */
	if (!myswap_bytes)
		for (i = 0; i < newheight; i++) {
			for (j = 0; j < newwidth; j++) {
				for (k = 0; k < components; k++) {
					s[0] = (*(const GLushort*)t +
						*(const GLushort*)(t+group_size) +
						*(const GLushort*)(t+ysize) +
						*(const GLushort*)(t+ysize+group_size) + 2) / 4;
					s++; t += element_size;
				}
				t += group_size;
			}
			t += padBytes;
			t += ysize;
		}
	else
		for (i = 0; i < newheight; i++) {
			for (j = 0; j < newwidth; j++) {
				for (k = 0; k < components; k++) {
					s[0] = (__GLU_SWAP_2_BYTES(t) +
						__GLU_SWAP_2_BYTES(t+group_size) +
						__GLU_SWAP_2_BYTES(t+ysize) +
						__GLU_SWAP_2_BYTES(t+ysize+group_size)+ 2)/4;
					s++; t += element_size;
				}
				t += group_size;
			}
			t += padBytes;
			t += ysize;
		}
}

static void halve1Dimage_short(GLint components, GLuint width, GLuint height,
							   const GLshort *dataIn, GLshort *dataOut,
							   GLint element_size, GLint ysize,
							   GLint group_size, GLint myswap_bytes)
{
	GLint halfWidth= width / 2;
	GLint halfHeight= height / 2;
	const char *src= (const char *) dataIn;
	GLshort *dest= dataOut;
	int jj;


	if (height == 1) {		/* 1 row */
		halfHeight= 1;

		for (jj= 0; jj< halfWidth; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
#define BOX2 2
				GLshort sshort[BOX2];
				if (myswap_bytes) {
					sshort[0]= __GLU_SWAP_2_BYTES(src);
					sshort[1]= __GLU_SWAP_2_BYTES(src+group_size);
				}
				else {
					sshort[0]= *(const GLshort*)src;
					sshort[1]= *(const GLshort*)(src+group_size);
				}

				*dest= (sshort[0] + sshort[1]) / 2;
				src+= element_size;
				dest++;
			}
			src+= group_size;	/* skip to next 2 */
		}
		{
			int padBytes= ysize - (width*group_size);
			src+= padBytes;	/* for assertion only */
		}
	}
	else if (width == 1) {	/* 1 column */
		int padBytes= ysize - (width * group_size);
		halfWidth= 1;
		/* one vertical column with possible pad bytes per row */
		/* average two at a time */

		for (jj= 0; jj< halfHeight; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
#define BOX2 2
				GLshort sshort[BOX2];
				if (myswap_bytes) {
					sshort[0]= __GLU_SWAP_2_BYTES(src);
					sshort[1]= __GLU_SWAP_2_BYTES(src+ysize);
				}
				else {
					sshort[0]= *(const GLshort*)src;
					sshort[1]= *(const GLshort*)(src+ysize);
				}
				*dest= (sshort[0] + sshort[1]) / 2;

				src+= element_size;
				dest++;
			}
			src+= padBytes; /* add pad bytes, if any, to get to end to row */
			src+= ysize;
		}
	}
} /* halve1Dimage_short() */
static void halveImage_short(GLint components, GLuint width, GLuint height,
							 const GLshort *datain, GLshort *dataout,
							 GLint element_size, GLint ysize, GLint group_size,
							 GLint myswap_bytes)
{
	int i, j, k;
	int newwidth, newheight;
	int padBytes;
	GLshort *s;
	const char *t;

	/* handle case where there is only 1 column/row */
	if (width == 1 || height == 1) {
		halve1Dimage_short(components,width,height,datain,dataout,
			element_size,ysize,group_size, myswap_bytes);
		return;
	}

	newwidth = width / 2;
	newheight = height / 2;
	padBytes = ysize - (width*group_size);
	s = dataout;
	t = (const char *)datain;

	/* Piece o' cake! */
	if (!myswap_bytes)
		for (i = 0; i < newheight; i++) {
			for (j = 0; j < newwidth; j++) {
				for (k = 0; k < components; k++) {
					s[0] = (*(const GLshort*)t +
						*(const GLshort*)(t+group_size) +
						*(const GLshort*)(t+ysize) +
						*(const GLshort*)(t+ysize+group_size) + 2) / 4;
					s++; t += element_size;
				}
				t += group_size;
			}
			t += padBytes;
			t += ysize;
		}
	else
		for (i = 0; i < newheight; i++) {
			for (j = 0; j < newwidth; j++) {
				for (k = 0; k < components; k++) {
					GLushort b;
					GLint buf;
					b = __GLU_SWAP_2_BYTES(t);
					buf = *(const GLshort*)&b;
					b = __GLU_SWAP_2_BYTES(t+group_size);
					buf += *(const GLshort*)&b;
					b = __GLU_SWAP_2_BYTES(t+ysize);
					buf += *(const GLshort*)&b;
					b = __GLU_SWAP_2_BYTES(t+ysize+group_size);
					buf += *(const GLshort*)&b;
					s[0] = (GLshort)((buf+2)/4);
					s++; t += element_size;
				}
				t += group_size;
			}
			t += padBytes;
			t += ysize;
		}
}

static void halve1Dimage_uint(GLint components, GLuint width, GLuint height,
							  const GLuint *dataIn, GLuint *dataOut,
							  GLint element_size, GLint ysize,
							  GLint group_size, GLint myswap_bytes)
{
	GLint halfWidth= width / 2;
	GLint halfHeight= height / 2;
	const char *src= (const char *) dataIn;
	GLuint *dest= dataOut;
	int jj;
	if (height == 1) {		/* 1 row */
		halfHeight= 1;

		for (jj= 0; jj< halfWidth; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
#define BOX2 2
				GLuint uint[BOX2];
				if (myswap_bytes) {
					uint[0]= __GLU_SWAP_4_BYTES(src);
					uint[1]= __GLU_SWAP_4_BYTES(src+group_size);
				}
				else {
					uint[0]= *(const GLuint*)src;
					uint[1]= *(const GLuint*)(src+group_size);
				}
				*dest= (GLuint)(((GLfloat)uint[0]+(GLfloat)uint[1])/2.0f);

				src+= element_size;
				dest++;
			}
			src+= group_size;	/* skip to next 2 */
		}
		{
			int padBytes= ysize - (width*group_size);
			src+= padBytes;	/* for assertion only */
		}
	}
	else if (width == 1) {	/* 1 column */
		int padBytes= ysize - (width * group_size);
		halfWidth= 1;
		/* one vertical column with possible pad bytes per row */
		/* average two at a time */

		for (jj= 0; jj< halfHeight; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
#define BOX2 2
				GLuint uint[BOX2];
				if (myswap_bytes) {
					uint[0]= __GLU_SWAP_4_BYTES(src);
					uint[1]= __GLU_SWAP_4_BYTES(src+ysize);
				}
				else {
					uint[0]= *(const GLuint*)src;
					uint[1]= *(const GLuint*)(src+ysize);
				}
				*dest= (GLuint)(((GLfloat)uint[0]+(GLfloat)uint[1])/2.0f);

				src+= element_size;
				dest++;
			}
			src+= padBytes; /* add pad bytes, if any, to get to end to row */
			src+= ysize;
		}
	}
} /* halve1Dimage_uint() */
static void halveImage_uint(GLint components, GLuint width, GLuint height,
							const GLuint *datain, GLuint *dataout,
							GLint element_size, GLint ysize, GLint group_size,
							GLint myswap_bytes)
{
	int i, j, k;
	int newwidth, newheight;
	int padBytes;
	GLuint *s;
	const char *t;

	/* handle case where there is only 1 column/row */
	if (width == 1 || height == 1) {
		halve1Dimage_uint(components,width,height,datain,dataout,
			element_size,ysize,group_size, myswap_bytes);
		return;
	}
	newwidth = width / 2;
	newheight = height / 2;
	padBytes = ysize - (width*group_size);
	s = dataout;
	t = (const char *)datain;

	/* Piece o' cake! */
	if (!myswap_bytes)
		for (i = 0; i < newheight; i++) {
			for (j = 0; j < newwidth; j++) {
				for (k = 0; k < components; k++) {
					/* need to cast to GLfloat to hold large unsigned ints */
					s[0] = (GLuint)(((GLfloat)*(const GLuint*)t +
						(GLfloat)*(const GLuint*)(t+group_size) +
						(GLfloat)*(const GLuint*)(t+ysize) +
						(GLfloat)*(const GLuint*)(t+ysize+group_size))/4 + 0.5f);
					s++; t += element_size;

				}
				t += group_size;
			}
			t += padBytes;
			t += ysize;
		}
	else
		for (i = 0; i < newheight; i++) {
			for (j = 0; j < newwidth; j++) {
				for (k = 0; k < components; k++) {
					/* need to cast to GLfloat to hold large unsigned ints */
					GLfloat buf;
					buf = (GLfloat)__GLU_SWAP_4_BYTES(t) +
						(GLfloat)__GLU_SWAP_4_BYTES(t+group_size) +
						(GLfloat)__GLU_SWAP_4_BYTES(t+ysize) +
						(GLfloat)__GLU_SWAP_4_BYTES(t+ysize+group_size);
					s[0] = (GLuint)(buf/4 + 0.5);

					s++; t += element_size;
				}
				t += group_size;
			}
			t += padBytes;
			t += ysize;
		}
}


static void halve1Dimage_fixed(GLint components, GLuint width, GLuint height,
							 const GLfixed *dataIn, GLfixed *dataOut,
							 GLint element_size, GLint ysize,
							 GLint group_size, GLint myswap_bytes)
{
	GLint halfWidth= width / 2;
	GLint halfHeight= height / 2;
	const char *src= (const char *) dataIn;
	GLint *dest= dataOut;
	int jj;

	if (height == 1) {		/* 1 row */
		halfHeight= 1;

		for (jj= 0; jj< halfWidth; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
#define BOX2 2
				GLuint uint[BOX2];
				if (myswap_bytes) {
					uint[0]= __GLU_SWAP_4_BYTES(src);
					uint[1]= __GLU_SWAP_4_BYTES(src+group_size);
				}
				else {
					uint[0]= *(const GLuint*)src;
					uint[1]= *(const GLuint*)(src+group_size);
				}
				*dest= (GLuint)(((GLfloat)uint[0]+(GLfloat)uint[1])/2.0f);

				src+= element_size;
				dest++;
			}
			src+= group_size;	/* skip to next 2 */
		}
		{
			int padBytes= ysize - (width*group_size);
			src+= padBytes;	/* for assertion only */
		}
	}
	else if (width == 1) {	/* 1 column */
		int padBytes= ysize - (width * group_size);
		halfWidth= 1;
		/* one vertical column with possible pad bytes per row */
		/* average two at a time */

		for (jj= 0; jj< halfHeight; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
#define BOX2 2
				GLuint uint[BOX2];
				if (myswap_bytes) {
					uint[0]= __GLU_SWAP_4_BYTES(src);
					uint[1]= __GLU_SWAP_4_BYTES(src+ysize);
				}
				else {
					uint[0]= *(const GLuint*)src;
					uint[1]= *(const GLuint*)(src+ysize);
				}
				*dest= (GLuint)(((GLfloat)uint[0]+(GLfloat)uint[1])/2.0);

				src+= element_size;
				dest++;
			}
			src+= padBytes; /* add pad bytes, if any, to get to end to row */
			src+= ysize;
		}
	}
} /* halve1Dimage_int() */
static void halveImage_fixed(GLint components, GLuint width, GLuint height,
						   const GLfixed *datain, GLfixed *dataout, GLint element_size,
						   GLint ysize, GLint group_size, GLint myswap_bytes)
{
	int i, j, k;
	int newwidth, newheight;
	int padBytes;
	GLint *s;
	const char *t;

	/* handle case where there is only 1 column/row */
	if (width == 1 || height == 1) {
		halve1Dimage_fixed(components,width,height,datain,dataout,
			element_size,ysize,group_size, myswap_bytes);
		return;
	}

	newwidth = width / 2;
	newheight = height / 2;
	padBytes = ysize - (width*group_size);
	s = dataout;
	t = (const char *)datain;

	/* Piece o' cake! */
	if (!myswap_bytes)
		for (i = 0; i < newheight; i++) {
			for (j = 0; j < newwidth; j++) {
				for (k = 0; k < components; k++) {
					s[0] = (GLuint)(((GLfloat)*(const GLint*)t +
						(GLfloat)*(const GLint*)(t+group_size) +
						(GLfloat)*(const GLint*)(t+ysize) +
						(GLfloat)*(const GLint*)(t+ysize+group_size))/4 + 0.5f);
					s++; t += element_size;
				}
				t += group_size;
			}
			t += padBytes;
			t += ysize;
		}
	else
		for (i = 0; i < newheight; i++) {
			for (j = 0; j < newwidth; j++) {
				for (k = 0; k < components; k++) {
					GLuint b;
					GLint buf;
					b = __GLU_SWAP_4_BYTES(t);
					buf = *(GLint*)&b;
					b = __GLU_SWAP_4_BYTES(t+group_size);
					buf += *(GLint*)&b;
					b = __GLU_SWAP_4_BYTES(t+ysize);
					buf += *(GLint*)&b;
					b = __GLU_SWAP_4_BYTES(t+ysize+group_size);
					buf += *(GLint*)&b;
					s[0] = (GLint)(buf/4 + 0.5f);

					s++; t += element_size;
				}
				t += group_size;
			}
			t += padBytes;
			t += ysize;
		}
}
static void halve1Dimage_float(GLint components, GLuint width, GLuint height,
							   const GLfloat *dataIn, GLfloat *dataOut,
							   GLint element_size, GLint ysize,
							   GLint group_size, GLint myswap_bytes)
{
	GLint halfWidth= width / 2;
	GLint halfHeight= height / 2;
	const char *src= (const char *) dataIn;
	GLfloat *dest= dataOut;
	int jj;

	if (height == 1) {		/* 1 row */
		halfHeight= 1;

		for (jj= 0; jj< halfWidth; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
#define BOX2 2
				GLfloat sfloat[BOX2];
				if (myswap_bytes) {
					sfloat[0]= (GLfloat)(__GLU_SWAP_4_BYTES(src));
					sfloat[1]= (GLfloat)(__GLU_SWAP_4_BYTES(src+group_size));
				}
				else {
					sfloat[0]= *(const GLfloat*)src;
					sfloat[1]= *(const GLfloat*)(src+group_size);
				}

				*dest= (sfloat[0] + sfloat[1]) / 2.0f;
				src+= element_size;
				dest++;
			}
			src+= group_size;	/* skip to next 2 */
		}
		{
			int padBytes= ysize - (width*group_size);
			src+= padBytes;	/* for assertion only */
		}
	}
	else if (width == 1) {	/* 1 column */
		int padBytes= ysize - (width * group_size);
		halfWidth= 1;
		/* one vertical column with possible pad bytes per row */
		/* average two at a time */

		for (jj= 0; jj< halfHeight; jj++) {
			int kk;
			for (kk= 0; kk< components; kk++) {
#define BOX2 2
				GLfloat sfloat[BOX2];
				if (myswap_bytes) {
					sfloat[0]= (GLfloat)(__GLU_SWAP_4_BYTES(src));
					sfloat[1]= (GLfloat)(__GLU_SWAP_4_BYTES(src+ysize));
				}
				else {
					sfloat[0]= *(const GLfloat*)src;
					sfloat[1]= *(const GLfloat*)(src+ysize);
				}
				*dest= (sfloat[0] + sfloat[1]) / 2.0f;

				src+= element_size;
				dest++;
			}
			src+= padBytes; /* add pad bytes, if any, to get to end to row */
			src+= ysize;		/* skip to odd row */
		}
	}
} /* halve1Dimage_float() */
static void halveImage_float(GLint components, GLuint width, GLuint height,
							 const GLfloat *datain, GLfloat *dataout,
							 GLint element_size, GLint ysize, GLint group_size,
							 GLint myswap_bytes)
{
	int i, j, k;
	int newwidth, newheight;
	int padBytes;
	GLfloat *s;
	const char *t;

	/* handle case where there is only 1 column/row */
	if (width == 1 || height == 1) {
		halve1Dimage_float(components,width,height,datain,dataout,
			element_size,ysize,group_size, myswap_bytes);
		return;
	}

	newwidth = width / 2;
	newheight = height / 2;
	padBytes = ysize - (width*group_size);
	s = dataout;
	t = (const char *)datain;

	/* Piece o' cake! */
	if (!myswap_bytes)
		for (i = 0; i < newheight; i++) {
			for (j = 0; j < newwidth; j++) {
				for (k = 0; k < components; k++) {
					s[0] = (*(const GLfloat*)t +
						*(const GLfloat*)(t+group_size) +
						*(const GLfloat*)(t+ysize) +
						*(const GLfloat*)(t+ysize+group_size)) / 4;
					s++; t += element_size;
				}
				t += group_size;
			}
			t += padBytes;
			t += ysize;
		}
	else
		for (i = 0; i < newheight; i++) {
			for (j = 0; j < newwidth; j++) {
				for (k = 0; k < components; k++) {
					union { GLuint b; GLfloat f; } swapbuf;
					swapbuf.b = __GLU_SWAP_4_BYTES(t);
					s[0] = swapbuf.f;
					swapbuf.b = __GLU_SWAP_4_BYTES(t+group_size);
					s[0] += swapbuf.f;
					swapbuf.b = __GLU_SWAP_4_BYTES(t+ysize);
					s[0] += swapbuf.f;
					swapbuf.b = __GLU_SWAP_4_BYTES(t+ysize+group_size);
					s[0] += swapbuf.f;
					s[0] /= 4;
					s++; t += element_size;
				}
				t += group_size;
			}
			t += padBytes;
			t += ysize;
		}
}

static void scale_internal_ubyte(GLint components, GLint widthin,
								 GLint heightin, const GLubyte *datain,
								 GLint widthout, GLint heightout,
								 GLubyte *dataout, GLint element_size,
								 GLint ysize, GLint group_size)
{
	float convx;
	float convy;
	float percent;
	/* Max components in a format is 4, so... */
	float totals[4];
	float area;
	int i,j,k,xindex;

	const char *temp, *temp0;
	const char *temp_index;
	int outindex;

	int lowx_int, highx_int, lowy_int, highy_int;
	float x_percent, y_percent;
	float lowx_float, highx_float, lowy_float, highy_float;
	float convy_float, convx_float;
	int convy_int, convx_int;
	int l, m;
	const char *left, *right;

	if (widthin == widthout*2 && heightin == heightout*2) {
		halveImage_ubyte(components, widthin, heightin,
			(const GLubyte *)datain, (GLubyte *)dataout,
			element_size, ysize, group_size);
		return;
	}
	convy = (float) heightin/heightout;
	convx = (float) widthin/widthout;
	convy_int = (int)(floor(convy));
	convy_float = convy - convy_int;
	convx_int = (int)(floor(convx));
	convx_float = convx - convx_int;

	area = convx * convy;

	lowy_int = 0;
	lowy_float = 0;
	highy_int = convy_int;
	highy_float = convy_float;

	for (i = 0; i < heightout; i++) {
		/* Clamp here to be sure we don't read beyond input buffer. */
		if (highy_int >= heightin)
			highy_int = heightin - 1;
		lowx_int = 0;
		lowx_float = 0;
		highx_int = convx_int;
		highx_float = convx_float;

		for (j = 0; j < widthout; j++) {

			/*
			** Ok, now apply box filter to box that goes from (lowx, lowy)
			** to (highx, highy) on input data into this pixel on output
			** data.
			*/
			totals[0] = totals[1] = totals[2] = totals[3] = 0.0;

			/* calculate the value for pixels in the 1st row */
			xindex = lowx_int*group_size;
			if((highy_int>lowy_int) && (highx_int>lowx_int)) {

				y_percent = 1-lowy_float;
				temp = (const char *)datain + xindex + lowy_int * ysize;
				percent = y_percent * (1-lowx_float);
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLubyte)(*(temp_index)) * percent;
				}
				left = temp;
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							totals[k] += (GLubyte)(*(temp_index)) * y_percent;
					}
				}
				temp += group_size;
				right = temp;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLubyte)(*(temp_index)) * percent;
				}

				/* calculate the value for pixels in the last row */
				y_percent = highy_float;
				percent = y_percent * (1-lowx_float);
				temp = (const char *)datain + xindex + highy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLubyte)(*(temp_index)) * percent;
				}
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							totals[k] += (GLubyte)(*(temp_index)) * y_percent;
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLubyte)(*(temp_index)) * percent;
				}


				/* calculate the value for pixels in the 1st and last column */
				for(m = lowy_int+1; m < highy_int; m++) {
					left += ysize;
					right += ysize;
					for (k = 0; k < components;
						k++, left += element_size, right += element_size) {
							totals[k] += (GLubyte)(*(left))*(1-lowx_float)
								+(GLubyte)(*(right))*highx_float;
					}
				}
			} else if (highy_int > lowy_int) {
				x_percent = highx_float - lowx_float;
				percent = (1-lowy_float)*x_percent;
				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLubyte)(*(temp_index)) * percent;
				}
				for(m = lowy_int+1; m < highy_int; m++) {
					temp += ysize;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							totals[k] += (GLubyte)(*(temp_index)) * x_percent;
					}
				}
				percent = x_percent * highy_float;
				temp += ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLubyte)(*(temp_index)) * percent;
				}
			} else if (highx_int > lowx_int) {
				y_percent = highy_float - lowy_float;
				percent = (1-lowx_float)*y_percent;
				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLubyte)(*(temp_index)) * percent;
				}
				for (l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							totals[k] += (GLubyte)(*(temp_index)) * y_percent;
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLubyte)(*(temp_index)) * percent;
				}
			} else {
				percent = (highy_float-lowy_float)*(highx_float-lowx_float);
				temp = (const char *)datain + xindex + lowy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLubyte)(*(temp_index)) * percent;
				}
			}



			/* this is for the pixels in the body */
			temp0 = (const char *)datain + xindex + group_size +
				(lowy_int+1)*ysize;
			for (m = lowy_int+1; m < highy_int; m++) {
				temp = temp0;
				for(l = lowx_int+1; l < highx_int; l++) {
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							totals[k] += (GLubyte)(*(temp_index));
					}
					temp += group_size;
				}
				temp0 += ysize;
			}

			outindex = (j + (i * widthout)) * components;
			for (k = 0; k < components; k++) {
				dataout[outindex + k] = (GLubyte)(totals[k]/area);
				/*printf("totals[%d] = %f\n", k, totals[k]);*/
			}
			lowx_int = highx_int;
			lowx_float = highx_float;
			highx_int += convx_int;
			highx_float += convx_float;
			if(highx_float > 1) {
				highx_float -= 1.0;
				highx_int++;
			}
		}
		lowy_int = highy_int;
		lowy_float = highy_float;
		highy_int += convy_int;
		highy_float += convy_float;
		if(highy_float > 1) {
			highy_float -= 1.0;
			highy_int++;
		}
	}
}

static void scale_internal_byte(GLint components, GLint widthin,
								GLint heightin, const GLbyte *datain,
								GLint widthout, GLint heightout,
								GLbyte *dataout, GLint element_size,
								GLint ysize, GLint group_size)
{
	float convx;
	float convy;
	float percent;
	/* Max components in a format is 4, so... */
	float totals[4];
	float area;
	int i,j,k,xindex;

	const char *temp, *temp0;
	const char *temp_index;
	int outindex;

	int lowx_int, highx_int, lowy_int, highy_int;
	float x_percent, y_percent;
	float lowx_float, highx_float, lowy_float, highy_float;
	float convy_float, convx_float;
	int convy_int, convx_int;
	int l, m;
	const char *left, *right;

	if (widthin == widthout*2 && heightin == heightout*2) {
		halveImage_byte(components, widthin, heightin,
			(const GLbyte *)datain, (GLbyte *)dataout,
			element_size, ysize, group_size);
		return;
	}
	convy = (float) heightin/heightout;
	convx = (float) widthin/widthout;
	convy_int = (int)(floor(convy));
	convy_float = convy - convy_int;
	convx_int = (int)(floor(convx));
	convx_float = convx - convx_int;

	area = convx * convy;

	lowy_int = 0;
	lowy_float = 0;
	highy_int = convy_int;
	highy_float = convy_float;

	for (i = 0; i < heightout; i++) {
		/* Clamp here to be sure we don't read beyond input buffer. */
		if (highy_int >= heightin)
			highy_int = heightin - 1;
		lowx_int = 0;
		lowx_float = 0;
		highx_int = convx_int;
		highx_float = convx_float;

		for (j = 0; j < widthout; j++) {

			/*
			** Ok, now apply box filter to box that goes from (lowx, lowy)
			** to (highx, highy) on input data into this pixel on output
			** data.
			*/
			totals[0] = totals[1] = totals[2] = totals[3] = 0.0;

			/* calculate the value for pixels in the 1st row */
			xindex = lowx_int*group_size;
			if((highy_int>lowy_int) && (highx_int>lowx_int)) {

				y_percent = 1-lowy_float;
				temp = (const char *)datain + xindex + lowy_int * ysize;
				percent = y_percent * (1-lowx_float);
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLbyte)(*(temp_index)) * percent;
				}
				left = temp;
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							totals[k] += (GLbyte)(*(temp_index)) * y_percent;
					}
				}
				temp += group_size;
				right = temp;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLbyte)(*(temp_index)) * percent;
				}

				/* calculate the value for pixels in the last row */	        
				y_percent = highy_float;
				percent = y_percent * (1-lowx_float);
				temp = (const char *)datain + xindex + highy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLbyte)(*(temp_index)) * percent;
				}
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							totals[k] += (GLbyte)(*(temp_index)) * y_percent;
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLbyte)(*(temp_index)) * percent;
				}


				/* calculate the value for pixels in the 1st and last column */
				for(m = lowy_int+1; m < highy_int; m++) {
					left += ysize;
					right += ysize;
					for (k = 0; k < components;
						k++, left += element_size, right += element_size) {
							totals[k] += (GLbyte)(*(left))*(1-lowx_float)
								+(GLbyte)(*(right))*highx_float;
					}
				}
			} else if (highy_int > lowy_int) {
				x_percent = highx_float - lowx_float;
				percent = (1-lowy_float)*x_percent;
				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLbyte)(*(temp_index)) * percent;
				}
				for(m = lowy_int+1; m < highy_int; m++) {
					temp += ysize;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							totals[k] += (GLbyte)(*(temp_index)) * x_percent;
					}
				}
				percent = x_percent * highy_float;
				temp += ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLbyte)(*(temp_index)) * percent;
				}
			} else if (highx_int > lowx_int) {
				y_percent = highy_float - lowy_float;
				percent = (1-lowx_float)*y_percent;
				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLbyte)(*(temp_index)) * percent;
				}
				for (l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							totals[k] += (GLbyte)(*(temp_index)) * y_percent;
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLbyte)(*(temp_index)) * percent;
				}
			} else {
				percent = (highy_float-lowy_float)*(highx_float-lowx_float);
				temp = (const char *)datain + xindex + lowy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						totals[k] += (GLbyte)(*(temp_index)) * percent;
				}
			}



			/* this is for the pixels in the body */
			temp0 = (const char *)datain + xindex + group_size +
				(lowy_int+1)*ysize;
			for (m = lowy_int+1; m < highy_int; m++) {
				temp = temp0;
				for(l = lowx_int+1; l < highx_int; l++) {
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							totals[k] += (GLbyte)(*(temp_index));
					}
					temp += group_size;
				}
				temp0 += ysize;
			}

			outindex = (j + (i * widthout)) * components;
			for (k = 0; k < components; k++) {
				dataout[outindex + k] = (GLbyte)(totals[k]/area);
				/*printf("totals[%d] = %f\n", k, totals[k]);*/
			}
			lowx_int = highx_int;
			lowx_float = highx_float;
			highx_int += convx_int;
			highx_float += convx_float;
			if(highx_float > 1) {
				highx_float -= 1.0;
				highx_int++;
			}
		}
		lowy_int = highy_int;
		lowy_float = highy_float;
		highy_int += convy_int;
		highy_float += convy_float;
		if(highy_float > 1) {
			highy_float -= 1.0;
			highy_int++;
		}
	}
}

static void scale_internal_ushort(GLint components, GLint widthin,
								  GLint heightin, const GLushort *datain,
								  GLint widthout, GLint heightout,
								  GLushort *dataout, GLint element_size,
								  GLint ysize, GLint group_size,
								  GLint myswap_bytes)
{
	float convx;
	float convy;
	float percent;
	/* Max components in a format is 4, so... */
	float totals[4];
	float area;
	int i,j,k,xindex;

	const char *temp, *temp0;
	const char *temp_index;
	int outindex;

	int lowx_int, highx_int, lowy_int, highy_int;
	float x_percent, y_percent;
	float lowx_float, highx_float, lowy_float, highy_float;
	float convy_float, convx_float;
	int convy_int, convx_int;
	int l, m;
	const char *left, *right;

	if (widthin == widthout*2 && heightin == heightout*2) {
		halveImage_ushort(components, widthin, heightin,
			(const GLushort *)datain, (GLushort *)dataout,
			element_size, ysize, group_size, myswap_bytes);
		return;
	}
	convy = (float) heightin/heightout;
	convx = (float) widthin/widthout;
	convy_int = (int)floor(convy);
	convy_float = convy - convy_int;
	convx_int = (int)floor(convx);
	convx_float = convx - convx_int;

	area = convx * convy;

	lowy_int = 0;
	lowy_float = 0;
	highy_int = convy_int;
	highy_float = convy_float;

	for (i = 0; i < heightout; i++) {
		/* Clamp here to be sure we don't read beyond input buffer. */
		if (highy_int >= heightin)
			highy_int = heightin - 1;
		lowx_int = 0;
		lowx_float = 0;
		highx_int = convx_int;
		highx_float = convx_float;

		for (j = 0; j < widthout; j++) {
			/*
			** Ok, now apply box filter to box that goes from (lowx, lowy)
			** to (highx, highy) on input data into this pixel on output
			** data.
			*/
			totals[0] = totals[1] = totals[2] = totals[3] = 0.0;

			/* calculate the value for pixels in the 1st row */
			xindex = lowx_int*group_size;
			if((highy_int>lowy_int) && (highx_int>lowx_int)) {

				y_percent = 1-lowy_float;
				temp = (const char *)datain + xindex + lowy_int * ysize;
				percent = y_percent * (1-lowx_float);
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_2_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLushort*)temp_index * percent;
						}
				}
				left = temp;
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								totals[k] +=
									__GLU_SWAP_2_BYTES(temp_index) * y_percent;
							} else {
								totals[k] += *(const GLushort*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				right = temp;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_2_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLushort*)temp_index * percent;
						}
				}

				/* calculate the value for pixels in the last row */	        
				y_percent = highy_float;
				percent = y_percent * (1-lowx_float);
				temp = (const char *)datain + xindex + highy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_2_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLushort*)temp_index * percent;
						}
				}
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								totals[k] +=
									__GLU_SWAP_2_BYTES(temp_index) * y_percent;
							} else {
								totals[k] += *(const GLushort*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_2_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLushort*)temp_index * percent;
						}
				}

				/* calculate the value for pixels in the 1st and last column */
				for(m = lowy_int+1; m < highy_int; m++) {
					left += ysize;
					right += ysize;
					for (k = 0; k < components;
						k++, left += element_size, right += element_size) {
							if (myswap_bytes) {
								totals[k] +=
									__GLU_SWAP_2_BYTES(left) * (1-lowx_float) +
									__GLU_SWAP_2_BYTES(right) * highx_float;
							} else {
								totals[k] += *(const GLushort*)left * (1-lowx_float)
									+ *(const GLushort*)right * highx_float;
							}
					}
				}
			} else if (highy_int > lowy_int) {
				x_percent = highx_float - lowx_float;
				percent = (1-lowy_float)*x_percent;
				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_2_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLushort*)temp_index * percent;
						}
				}
				for(m = lowy_int+1; m < highy_int; m++) {
					temp += ysize;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								totals[k] +=
									__GLU_SWAP_2_BYTES(temp_index) * x_percent;
							} else {
								totals[k] += *(const GLushort*)temp_index * x_percent;
							}
					}
				}
				percent = x_percent * highy_float;
				temp += ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_2_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLushort*)temp_index * percent;
						}
				}
			} else if (highx_int > lowx_int) {
				y_percent = highy_float - lowy_float;
				percent = (1-lowx_float)*y_percent;
				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_2_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLushort*)temp_index * percent;
						}
				}
				for (l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								totals[k] +=
									__GLU_SWAP_2_BYTES(temp_index) * y_percent;
							} else {
								totals[k] += *(const GLushort*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_2_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLushort*)temp_index * percent;
						}
				}
			} else {
				percent = (highy_float-lowy_float)*(highx_float-lowx_float);
				temp = (const char *)datain + xindex + lowy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_2_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLushort*)temp_index * percent;
						}
				}
			}

			/* this is for the pixels in the body */
			temp0 = (const char *)datain + xindex + group_size +
				(lowy_int+1)*ysize;
			for (m = lowy_int+1; m < highy_int; m++) {
				temp = temp0;
				for(l = lowx_int+1; l < highx_int; l++) {
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								totals[k] += __GLU_SWAP_2_BYTES(temp_index);
							} else {
								totals[k] += *(const GLushort*)temp_index;
							}
					}
					temp += group_size;
				}
				temp0 += ysize;
			}

			outindex = (j + (i * widthout)) * components;
			for (k = 0; k < components; k++) {
				dataout[outindex + k] = (GLushort)(totals[k]/area);
				/*printf("totals[%d] = %f\n", k, totals[k]);*/
			}
			lowx_int = highx_int;
			lowx_float = highx_float;
			highx_int += convx_int;
			highx_float += convx_float;
			if(highx_float > 1) {
				highx_float -= 1.0;
				highx_int++;
			}
		}
		lowy_int = highy_int;
		lowy_float = highy_float;
		highy_int += convy_int;
		highy_float += convy_float;
		if(highy_float > 1) {
			highy_float -= 1.0;
			highy_int++;
		}
	}
}

static void scale_internal_short(GLint components, GLint widthin,
								 GLint heightin, const GLshort *datain,
								 GLint widthout, GLint heightout,
								 GLshort *dataout, GLint element_size,
								 GLint ysize, GLint group_size,
								 GLint myswap_bytes)
{
	float convx;
	float convy;
	float percent;
	/* Max components in a format is 4, so... */
	float totals[4];
	float area;
	int i,j,k,xindex;

	const char *temp, *temp0;
	const char *temp_index;
	int outindex;

	int lowx_int, highx_int, lowy_int, highy_int;
	float x_percent, y_percent;
	float lowx_float, highx_float, lowy_float, highy_float;
	float convy_float, convx_float;
	int convy_int, convx_int;
	int l, m;
	const char *left, *right;

	GLushort swapbuf;	/* unsigned buffer */

	if (widthin == widthout*2 && heightin == heightout*2) {
		halveImage_short(components, widthin, heightin,
			(const GLshort *)datain, (GLshort *)dataout,
			element_size, ysize, group_size, myswap_bytes);
		return;
	}
	convy = (float) heightin/heightout;
	convx = (float) widthin/widthout;
	convy_int = (int)floor(convy);
	convy_float = convy - convy_int;
	convx_int = (int)floor(convx);
	convx_float = convx - convx_int;

	area = convx * convy;

	lowy_int = 0;
	lowy_float = 0;
	highy_int = convy_int;
	highy_float = convy_float;

	for (i = 0; i < heightout; i++) {
		/* Clamp here to be sure we don't read beyond input buffer. */
		if (highy_int >= heightin)
			highy_int = heightin - 1;
		lowx_int = 0;
		lowx_float = 0;
		highx_int = convx_int;
		highx_float = convx_float;

		for (j = 0; j < widthout; j++) {
			/*
			** Ok, now apply box filter to box that goes from (lowx, lowy)
			** to (highx, highy) on input data into this pixel on output
			** data.
			*/
			totals[0] = totals[1] = totals[2] = totals[3] = 0.0;

			/* calculate the value for pixels in the 1st row */
			xindex = lowx_int*group_size;
			if((highy_int>lowy_int) && (highx_int>lowx_int)) {

				y_percent = 1-lowy_float;
				temp = (const char *)datain + xindex + lowy_int * ysize;
				percent = y_percent * (1-lowx_float);
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_2_BYTES(temp_index);
							totals[k] += *(const GLshort*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLshort*)temp_index * percent;
						}
				}
				left = temp;
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf = __GLU_SWAP_2_BYTES(temp_index);
								totals[k] += *(const GLshort*)&swapbuf * y_percent;
							} else {
								totals[k] += *(const GLshort*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				right = temp;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_2_BYTES(temp_index);
							totals[k] += *(const GLshort*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLshort*)temp_index * percent;
						}
				}

				/* calculate the value for pixels in the last row */
				y_percent = highy_float;
				percent = y_percent * (1-lowx_float);
				temp = (const char *)datain + xindex + highy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_2_BYTES(temp_index);
							totals[k] += *(const GLshort*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLshort*)temp_index * percent;
						}
				}
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf = __GLU_SWAP_2_BYTES(temp_index);
								totals[k] += *(const GLshort*)&swapbuf * y_percent;
							} else {
								totals[k] += *(const GLshort*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_2_BYTES(temp_index);
							totals[k] += *(const GLshort*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLshort*)temp_index * percent;
						}
				}

				/* calculate the value for pixels in the 1st and last column */
				for(m = lowy_int+1; m < highy_int; m++) {
					left += ysize;
					right += ysize;
					for (k = 0; k < components;
						k++, left += element_size, right += element_size) {
							if (myswap_bytes) {
								swapbuf = __GLU_SWAP_2_BYTES(left);
								totals[k] += *(const GLshort*)&swapbuf * (1-lowx_float);
								swapbuf = __GLU_SWAP_2_BYTES(right);
								totals[k] += *(const GLshort*)&swapbuf * highx_float;
							} else {
								totals[k] += *(const GLshort*)left * (1-lowx_float)
									+ *(const GLshort*)right * highx_float;
							}
					}
				}
			} else if (highy_int > lowy_int) {
				x_percent = highx_float - lowx_float;
				percent = (1-lowy_float)*x_percent;
				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_2_BYTES(temp_index);
							totals[k] += *(const GLshort*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLshort*)temp_index * percent;
						}
				}
				for(m = lowy_int+1; m < highy_int; m++) {
					temp += ysize;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf = __GLU_SWAP_2_BYTES(temp_index);
								totals[k] += *(const GLshort*)&swapbuf * x_percent;
							} else {
								totals[k] += *(const GLshort*)temp_index * x_percent;
							}
					}
				}
				percent = x_percent * highy_float;
				temp += ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_2_BYTES(temp_index);
							totals[k] += *(const GLshort*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLshort*)temp_index * percent;
						}
				}
			} else if (highx_int > lowx_int) {
				y_percent = highy_float - lowy_float;
				percent = (1-lowx_float)*y_percent;

				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_2_BYTES(temp_index);
							totals[k] += *(const GLshort*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLshort*)temp_index * percent;
						}
				}
				for (l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf = __GLU_SWAP_2_BYTES(temp_index);
								totals[k] += *(const GLshort*)&swapbuf * y_percent;
							} else {
								totals[k] += *(const GLshort*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_2_BYTES(temp_index);
							totals[k] += *(const GLshort*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLshort*)temp_index * percent;
						}
				}
			} else {
				percent = (highy_float-lowy_float)*(highx_float-lowx_float);
				temp = (const char *)datain + xindex + lowy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_2_BYTES(temp_index);
							totals[k] += *(const GLshort*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLshort*)temp_index * percent;
						}
				}
			}

			/* this is for the pixels in the body */
			temp0 = (const char *)datain + xindex + group_size +
				(lowy_int+1)*ysize;
			for (m = lowy_int+1; m < highy_int; m++) {
				temp = temp0;
				for(l = lowx_int+1; l < highx_int; l++) {
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf = __GLU_SWAP_2_BYTES(temp_index);
								totals[k] += *(const GLshort*)&swapbuf;
							} else {
								totals[k] += *(const GLshort*)temp_index;
							}
					}
					temp += group_size;
				}
				temp0 += ysize;
			}

			outindex = (j + (i * widthout)) * components;
			for (k = 0; k < components; k++) {
				dataout[outindex + k] = (GLshort)(totals[k]/area);
				/*printf("totals[%d] = %f\n", k, totals[k]);*/
			}
			lowx_int = highx_int;
			lowx_float = highx_float;
			highx_int += convx_int;
			highx_float += convx_float;
			if(highx_float > 1) {
				highx_float -= 1.0;
				highx_int++;
			}
		}
		lowy_int = highy_int;
		lowy_float = highy_float;
		highy_int += convy_int;
		highy_float += convy_float;
		if(highy_float > 1) {
			highy_float -= 1.0;
			highy_int++;
		}
	}
}

static void scale_internal_uint(GLint components, GLint widthin,
								GLint heightin, const GLuint *datain,
								GLint widthout, GLint heightout,
								GLuint *dataout, GLint element_size,
								GLint ysize, GLint group_size,
								GLint myswap_bytes)
{
	float convx;
	float convy;
	float percent;
	/* Max components in a format is 4, so... */
	float totals[4];
	float area;
	int i,j,k,xindex;

	const char *temp, *temp0;
	const char *temp_index;
	int outindex;

	int lowx_int, highx_int, lowy_int, highy_int;
	float x_percent, y_percent;
	float lowx_float, highx_float, lowy_float, highy_float;
	float convy_float, convx_float;
	int convy_int, convx_int;
	int l, m;
	const char *left, *right;

	if (widthin == widthout*2 && heightin == heightout*2) {
		halveImage_uint(components, widthin, heightin,
			(const GLuint *)datain, (GLuint *)dataout,
			element_size, ysize, group_size, myswap_bytes);
		return;
	}
	convy = (float) heightin/heightout;
	convx = (float) widthin/widthout;
	convy_int = (int)(floor(convy));
	convy_float = convy - convy_int;
	convx_int = (int)(floor(convx));
	convx_float = convx - convx_int;

	area = convx * convy;

	lowy_int = 0;
	lowy_float = 0;
	highy_int = convy_int;
	highy_float = convy_float;

	for (i = 0; i < heightout; i++) {
		/* Clamp here to be sure we don't read beyond input buffer. */
		if (highy_int >= heightin)
			highy_int = heightin - 1;
		lowx_int = 0;
		lowx_float = 0;
		highx_int = convx_int;
		highx_float = convx_float;

		for (j = 0; j < widthout; j++) {
			/*
			** Ok, now apply box filter to box that goes from (lowx, lowy)
			** to (highx, highy) on input data into this pixel on output
			** data.
			*/
			totals[0] = totals[1] = totals[2] = totals[3] = 0.0;

			/* calculate the value for pixels in the 1st row */
			xindex = lowx_int*group_size;
			if((highy_int>lowy_int) && (highx_int>lowx_int)) {

				y_percent = 1-lowy_float;
				temp = (const char *)datain + xindex + lowy_int * ysize;
				percent = y_percent * (1-lowx_float);
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_4_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLuint*)temp_index * percent;
						}
				}
				left = temp;
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								totals[k] +=
									__GLU_SWAP_4_BYTES(temp_index) * y_percent;
							} else {
								totals[k] += *(const GLuint*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				right = temp;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_4_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLuint*)temp_index * percent;
						}
				}

				/* calculate the value for pixels in the last row */
				y_percent = highy_float;
				percent = y_percent * (1-lowx_float);
				temp = (const char *)datain + xindex + highy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_4_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLuint*)temp_index * percent;
						}
				}
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								totals[k] +=
									__GLU_SWAP_4_BYTES(temp_index) * y_percent;
							} else {
								totals[k] += *(const GLuint*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_4_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLuint*)temp_index * percent;
						}
				}

				/* calculate the value for pixels in the 1st and last column */
				for(m = lowy_int+1; m < highy_int; m++) {
					left += ysize;
					right += ysize;
					for (k = 0; k < components;
						k++, left += element_size, right += element_size) {
							if (myswap_bytes) {
								totals[k] +=
									__GLU_SWAP_4_BYTES(left) * (1-lowx_float)
									+ __GLU_SWAP_4_BYTES(right) * highx_float;
							} else {
								totals[k] += *(const GLuint*)left * (1-lowx_float)
									+ *(const GLuint*)right * highx_float;
							}
					}
				}
			} else if (highy_int > lowy_int) {
				x_percent = highx_float - lowx_float;
				percent = (1-lowy_float)*x_percent;
				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_4_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLuint*)temp_index * percent;
						}
				}
				for(m = lowy_int+1; m < highy_int; m++) {
					temp += ysize;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								totals[k] +=
									__GLU_SWAP_4_BYTES(temp_index) * x_percent;
							} else {
								totals[k] += *(const GLuint*)temp_index * x_percent;
							}
					}
				}
				percent = x_percent * highy_float;
				temp += ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_4_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLuint*)temp_index * percent;
						}
				}
			} else if (highx_int > lowx_int) {
				y_percent = highy_float - lowy_float;
				percent = (1-lowx_float)*y_percent;

				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_4_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLuint*)temp_index * percent;
						}
				}
				for (l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								totals[k] +=
									__GLU_SWAP_4_BYTES(temp_index) * y_percent;
							} else {
								totals[k] += *(const GLuint*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_4_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLuint*)temp_index * percent;
						}
				}
			} else {
				percent = (highy_float-lowy_float)*(highx_float-lowx_float);
				temp = (const char *)datain + xindex + lowy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							totals[k] += __GLU_SWAP_4_BYTES(temp_index) * percent;
						} else {
							totals[k] += *(const GLuint*)temp_index * percent;
						}
				}
			}

			/* this is for the pixels in the body */
			temp0 = (const char *)datain + xindex + group_size +
				(lowy_int+1)*ysize;
			for (m = lowy_int+1; m < highy_int; m++) {
				temp = temp0;
				for(l = lowx_int+1; l < highx_int; l++) {
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								totals[k] += __GLU_SWAP_4_BYTES(temp_index);
							} else {
								totals[k] += *(const GLuint*)temp_index;
							}
					}
					temp += group_size;
				}
				temp0 += ysize;
			}

			outindex = (j + (i * widthout)) * components;
			for (k = 0; k < components; k++) {
				/* clamp at UINT_MAX */
				float value= totals[k]/area;
				if (value >= (float) UINT_MAX) {	/* need '=' */
					dataout[outindex + k] = UINT_MAX;
				}
				else dataout[outindex + k] = (GLuint)value;
			}
			lowx_int = highx_int;
			lowx_float = highx_float;
			highx_int += convx_int;
			highx_float += convx_float;
			if(highx_float > 1) {
				highx_float -= 1.0;
				highx_int++;
			}
		}
		lowy_int = highy_int;
		lowy_float = highy_float;
		highy_int += convy_int;
		highy_float += convy_float;
		if(highy_float > 1) {
			highy_float -= 1.0;
			highy_int++;
		}
	}
}



static void scale_internal_fixed(GLint components, GLint widthin,
							   GLint heightin, const GLfixed *datain,
							   GLint widthout, GLint heightout,
							   GLfixed *dataout, GLint element_size,
							   GLint ysize, GLint group_size,
							   GLint myswap_bytes)
{
	float convx;
	float convy;
	float percent;
	/* Max components in a format is 4, so... */
	float totals[4];
	float area;
	int i,j,k,xindex;

	const char *temp, *temp0;
	const char *temp_index;
	int outindex;

	int lowx_int, highx_int, lowy_int, highy_int;
	float x_percent, y_percent;
	float lowx_float, highx_float, lowy_float, highy_float;
	float convy_float, convx_float;
	int convy_int, convx_int;
	int l, m;
	const char *left, *right;

	GLuint swapbuf;	/* unsigned buffer */

	if (widthin == widthout*2 && heightin == heightout*2) {
		halveImage_fixed(components, widthin, heightin,
			(const GLfixed *)datain, (GLfixed *)dataout,
			element_size, ysize, group_size, myswap_bytes);
		return;
	}
	convy = (float) heightin/heightout;
	convx = (float) widthin/widthout;
	convy_int = (int)(floor(convy));
	convy_float = convy - convy_int;
	convx_int = (int)(floor(convx));
	convx_float = convx - convx_int;

	area = convx * convy;

	lowy_int = 0;
	lowy_float = 0;
	highy_int = convy_int;
	highy_float = convy_float;

	for (i = 0; i < heightout; i++) {
		/* Clamp here to be sure we don't read beyond input buffer. */
		if (highy_int >= heightin)
			highy_int = heightin - 1;
		lowx_int = 0;
		lowx_float = 0;
		highx_int = convx_int;
		highx_float = convx_float;

		for (j = 0; j < widthout; j++) {
			/*
			** Ok, now apply box filter to box that goes from (lowx, lowy)
			** to (highx, highy) on input data into this pixel on output
			** data.
			*/
			totals[0] = totals[1] = totals[2] = totals[3] = 0.0;

			/* calculate the value for pixels in the 1st row */
			xindex = lowx_int*group_size;
			if((highy_int>lowy_int) && (highx_int>lowx_int)) {

				y_percent = 1-lowy_float;
				temp = (const char *)datain + xindex + lowy_int * ysize;
				percent = y_percent * (1-lowx_float);
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += *(const GLint*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLint*)temp_index * percent;
						}
				}
				left = temp;
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf = __GLU_SWAP_4_BYTES(temp_index);
								totals[k] += *(const GLint*)&swapbuf * y_percent;
							} else {
								totals[k] += *(const GLint*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				right = temp;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += *(const GLint*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLint*)temp_index * percent;
						}
				}

				/* calculate the value for pixels in the last row */
				y_percent = highy_float;
				percent = y_percent * (1-lowx_float);
				temp = (const char *)datain + xindex + highy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += *(const GLint*)&swapbuf  * percent;
						} else {
							totals[k] += *(const GLint*)temp_index * percent;
						}
				}
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf = __GLU_SWAP_4_BYTES(temp_index);
								totals[k] += *(const GLint*)&swapbuf * y_percent;
							} else {
								totals[k] += *(const GLint*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += *(const GLint*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLint*)temp_index * percent;
						}
				}

				/* calculate the value for pixels in the 1st and last column */
				for(m = lowy_int+1; m < highy_int; m++) {
					left += ysize;
					right += ysize;
					for (k = 0; k < components;
						k++, left += element_size, right += element_size) {
							if (myswap_bytes) {
								swapbuf = __GLU_SWAP_4_BYTES(left);
								totals[k] += *(const GLint*)&swapbuf * (1-lowx_float);
								swapbuf = __GLU_SWAP_4_BYTES(right);
								totals[k] += *(const GLint*)&swapbuf * highx_float;
							} else {
								totals[k] += *(const GLint*)left * (1-lowx_float)
									+ *(const GLint*)right * highx_float;
							}
					}
				}
			} else if (highy_int > lowy_int) {
				x_percent = highx_float - lowx_float;
				percent = (1-lowy_float)*x_percent;
				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += *(const GLint*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLint*)temp_index * percent;
						}
				}
				for(m = lowy_int+1; m < highy_int; m++) {
					temp += ysize;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf = __GLU_SWAP_4_BYTES(temp_index);
								totals[k] += *(const GLint*)&swapbuf * x_percent;
							} else {
								totals[k] += *(const GLint*)temp_index * x_percent;
							}
					}
				}
				percent = x_percent * highy_float;
				temp += ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += *(const GLint*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLint*)temp_index * percent;
						}
				}
			} else if (highx_int > lowx_int) {
				y_percent = highy_float - lowy_float;
				percent = (1-lowx_float)*y_percent;

				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += *(const GLint*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLint*)temp_index * percent;
						}
				}
				for (l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf = __GLU_SWAP_4_BYTES(temp_index);
								totals[k] += *(const GLint*)&swapbuf * y_percent;
							} else {
								totals[k] += *(const GLint*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += *(const GLint*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLint*)temp_index * percent;
						}
				}
			} else {
				percent = (highy_float-lowy_float)*(highx_float-lowx_float);
				temp = (const char *)datain + xindex + lowy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += *(const GLint*)&swapbuf * percent;
						} else {
							totals[k] += *(const GLint*)temp_index * percent;
						}
				}
			}

			/* this is for the pixels in the body */
			temp0 = (const char *)datain + xindex + group_size +
				(lowy_int+1)*ysize;
			for (m = lowy_int+1; m < highy_int; m++) {
				temp = temp0;
				for(l = lowx_int+1; l < highx_int; l++) {
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf = __GLU_SWAP_4_BYTES(temp_index);
								totals[k] += *(const GLint*)&swapbuf;
							} else {
								totals[k] += *(const GLint*)temp_index;
							}
					}
					temp += group_size;
				}
				temp0 += ysize;
			}

			outindex = (j + (i * widthout)) * components;
			for (k = 0; k < components; k++) {
				dataout[outindex + k] = (GLfixed)(totals[k]/area);
				/*printf("totals[%d] = %f\n", k, totals[k]);*/
			}
			lowx_int = highx_int;
			lowx_float = highx_float;
			highx_int += convx_int;
			highx_float += convx_float;
			if(highx_float > 1) {
				highx_float -= 1.0;
				highx_int++;
			}
		}
		lowy_int = highy_int;
		lowy_float = highy_float;
		highy_int += convy_int;
		highy_float += convy_float;
		if(highy_float > 1) {
			highy_float -= 1.0;
			highy_int++;
		}
	}
}



static void scale_internal_float(GLint components, GLint widthin,
								 GLint heightin, const GLfloat *datain,
								 GLint widthout, GLint heightout,
								 GLfloat *dataout, GLint element_size,
								 GLint ysize, GLint group_size,
								 GLint myswap_bytes)
{
	float convx;
	float convy;
	float percent;
	/* Max components in a format is 4, so... */
	float totals[4];
	float area;
	int i,j,k,xindex;

	const char *temp, *temp0;
	const char *temp_index;
	int outindex;

	int lowx_int, highx_int, lowy_int, highy_int;
	float x_percent, y_percent;
	float lowx_float, highx_float, lowy_float, highy_float;
	float convy_float, convx_float;
	int convy_int, convx_int;
	int l, m;
	const char *left, *right;

	union { GLuint b; GLfloat f; } swapbuf;

	if (widthin == widthout*2 && heightin == heightout*2) {
		halveImage_float(components, widthin, heightin,
			(const GLfloat *)datain, (GLfloat *)dataout,
			element_size, ysize, group_size, myswap_bytes);
		return;
	}
	convy = (float) heightin/heightout;
	convx = (float) widthin/widthout;
	convy_int = (int)(floor(convy));
	convy_float = convy - convy_int;
	convx_int = (int)(floor(convx));
	convx_float = convx - convx_int;

	area = convx * convy;

	lowy_int = 0;
	lowy_float = 0;
	highy_int = convy_int;
	highy_float = convy_float;

	for (i = 0; i < heightout; i++) {
		/* Clamp here to be sure we don't read beyond input buffer. */
		if (highy_int >= heightin)
			highy_int = heightin - 1;
		lowx_int = 0;
		lowx_float = 0;
		highx_int = convx_int;
		highx_float = convx_float;

		for (j = 0; j < widthout; j++) {
			/*
			** Ok, now apply box filter to box that goes from (lowx, lowy)
			** to (highx, highy) on input data into this pixel on output
			** data.
			*/
			totals[0] = totals[1] = totals[2] = totals[3] = 0.0;

			/* calculate the value for pixels in the 1st row */
			xindex = lowx_int*group_size;
			if((highy_int>lowy_int) && (highx_int>lowx_int)) {

				y_percent = 1-lowy_float;
				temp = (const char *)datain + xindex + lowy_int * ysize;
				percent = y_percent * (1-lowx_float);
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += swapbuf.f * percent;
						} else {
							totals[k] += *(const GLfloat*)temp_index * percent;
						}
				}
				left = temp;
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
								totals[k] += swapbuf.f * y_percent;
							} else {
								totals[k] += *(const GLfloat*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				right = temp;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += swapbuf.f * percent;
						} else {
							totals[k] += *(const GLfloat*)temp_index * percent;
						}
				}

				/* calculate the value for pixels in the last row */
				y_percent = highy_float;
				percent = y_percent * (1-lowx_float);
				temp = (const char *)datain + xindex + highy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += swapbuf.f * percent;
						} else {
							totals[k] += *(const GLfloat*)temp_index * percent;
						}
				}
				for(l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
								totals[k] += swapbuf.f * y_percent;
							} else {
								totals[k] += *(const GLfloat*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += swapbuf.f * percent;
						} else {
							totals[k] += *(const GLfloat*)temp_index * percent;
						}
				}

				/* calculate the value for pixels in the 1st and last column */
				for(m = lowy_int+1; m < highy_int; m++) {
					left += ysize;
					right += ysize;
					for (k = 0; k < components;
						k++, left += element_size, right += element_size) {
							if (myswap_bytes) {
								swapbuf.b = __GLU_SWAP_4_BYTES(left);
								totals[k] += swapbuf.f * (1-lowx_float);
								swapbuf.b = __GLU_SWAP_4_BYTES(right);
								totals[k] += swapbuf.f * highx_float;
							} else {
								totals[k] += *(const GLfloat*)left * (1-lowx_float)
									+ *(const GLfloat*)right * highx_float;
							}
					}
				}
			} else if (highy_int > lowy_int) {
				x_percent = highx_float - lowx_float;
				percent = (1-lowy_float)*x_percent;
				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += swapbuf.f * percent;
						} else {
							totals[k] += *(const GLfloat*)temp_index * percent;
						}
				}
				for(m = lowy_int+1; m < highy_int; m++) {
					temp += ysize;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
								totals[k] += swapbuf.f * x_percent;
							} else {
								totals[k] += *(const GLfloat*)temp_index * x_percent;
							}
					}
				}
				percent = x_percent * highy_float;
				temp += ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += swapbuf.f * percent;
						} else {
							totals[k] += *(const GLfloat*)temp_index * percent;
						}
				}
			} else if (highx_int > lowx_int) {
				y_percent = highy_float - lowy_float;
				percent = (1-lowx_float)*y_percent;

				temp = (const char *)datain + xindex + lowy_int*ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += swapbuf.f * percent;
						} else {
							totals[k] += *(const GLfloat*)temp_index * percent;
						}
				}
				for (l = lowx_int+1; l < highx_int; l++) {
					temp += group_size;
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
								totals[k] += swapbuf.f * y_percent;
							} else {
								totals[k] += *(const GLfloat*)temp_index * y_percent;
							}
					}
				}
				temp += group_size;
				percent = y_percent * highx_float;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += swapbuf.f * percent;
						} else {
							totals[k] += *(const GLfloat*)temp_index * percent;
						}
				}
			} else {
				percent = (highy_float-lowy_float)*(highx_float-lowx_float);
				temp = (const char *)datain + xindex + lowy_int * ysize;
				for (k = 0, temp_index = temp; k < components;
					k++, temp_index += element_size) {
						if (myswap_bytes) {
							swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
							totals[k] += swapbuf.f * percent;
						} else {
							totals[k] += *(const GLfloat*)temp_index * percent;
						}
				}
			}

			/* this is for the pixels in the body */
			temp0 = (const char *)datain + xindex + group_size +
				(lowy_int+1)*ysize;
			for (m = lowy_int+1; m < highy_int; m++) {
				temp = temp0;
				for(l = lowx_int+1; l < highx_int; l++) {
					for (k = 0, temp_index = temp; k < components;
						k++, temp_index += element_size) {
							if (myswap_bytes) {
								swapbuf.b = __GLU_SWAP_4_BYTES(temp_index);
								totals[k] += swapbuf.f;
							} else {
								totals[k] += *(const GLfloat*)temp_index;
							}
					}
					temp += group_size;
				}
				temp0 += ysize;
			}

			outindex = (j + (i * widthout)) * components;
			for (k = 0; k < components; k++) {
				dataout[outindex + k] = totals[k]/area;
				/*printf("totals[%d] = %f\n", k, totals[k]);*/
			}
			lowx_int = highx_int;
			lowx_float = highx_float;
			highx_int += convx_int;
			highx_float += convx_float;
			if(highx_float > 1) {
				highx_float -= 1.0;
				highx_int++;
			}
		}
		lowy_int = highy_int;
		lowy_float = highy_float;
		highy_int += convy_int;
		highy_float += convy_float;
		if(highy_float > 1) {
			highy_float -= 1.0;
			highy_int++;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
static void initDodecahedron(void)
{
	GLfloat alpha, beta;

	alpha = (GLfloat)(sqrt(2.0 / (3.0 + sqrt(5.0))));
	beta = 1.0f + (GLfloat)(sqrt(6.0 / (3.0 + sqrt(5.0)) -
		2.0 + 2.0 * sqrt(2.0 / (3.0 + sqrt(5.0)))));
	/* *INDENT-OFF* */
	dodec[0][0] = -alpha; dodec[0][1] = 0; dodec[0][2] = beta;
	dodec[1][0] = alpha; dodec[1][1] = 0; dodec[1][2] = beta;
	dodec[2][0] = -1; dodec[2][1] = -1; dodec[2][2] = -1;
	dodec[3][0] = -1; dodec[3][1] = -1; dodec[3][2] = 1;
	dodec[4][0] = -1; dodec[4][1] = 1; dodec[4][2] = -1;
	dodec[5][0] = -1; dodec[5][1] = 1; dodec[5][2] = 1;
	dodec[6][0] = 1; dodec[6][1] = -1; dodec[6][2] = -1;
	dodec[7][0] = 1; dodec[7][1] = -1; dodec[7][2] = 1;
	dodec[8][0] = 1; dodec[8][1] = 1; dodec[8][2] = -1;
	dodec[9][0] = 1; dodec[9][1] = 1; dodec[9][2] = 1;
	dodec[10][0] = beta; dodec[10][1] = alpha; dodec[10][2] = 0;
	dodec[11][0] = beta; dodec[11][1] = -alpha; dodec[11][2] = 0;
	dodec[12][0] = -beta; dodec[12][1] = alpha; dodec[12][2] = 0;
	dodec[13][0] = -beta; dodec[13][1] = -alpha; dodec[13][2] = 0;
	dodec[14][0] = -alpha; dodec[14][1] = 0; dodec[14][2] = -beta;
	dodec[15][0] = alpha; dodec[15][1] = 0; dodec[15][2] = -beta;
	dodec[16][0] = 0; dodec[16][1] = beta; dodec[16][2] = alpha;
	dodec[17][0] = 0; dodec[17][1] = beta; dodec[17][2] = -alpha;
	dodec[18][0] = 0; dodec[18][1] = -beta; dodec[18][2] = alpha;
	dodec[19][0] = 0; dodec[19][1] = -beta; dodec[19][2] = -alpha;
	/* *INDENT-ON* */

}

static void pentagon(int a, int b, int c, int d, int e, GLenum shadeType)
{
	GLfloat n0[3], d1[3], d2[3];

	DIFF3(dodec[a], dodec[b], d1);
	DIFF3(dodec[b], dodec[c], d2);
	crossprod(d1, d2, n0);
	normalize(n0);

	GLfloat vertexBuffer[5*3];
	GLfloat normBuffer[5*3];
	int i = 0;
	int j = 0;
	vertexBuffer[i++] = dodec[a][0];
	vertexBuffer[i++] = dodec[a][1];
	vertexBuffer[i++] = dodec[a][2];
	vertexBuffer[i++] = dodec[b][0];
	vertexBuffer[i++] = dodec[b][1];
	vertexBuffer[i++] = dodec[b][2];
	vertexBuffer[i++] = dodec[c][0];
	vertexBuffer[i++] = dodec[c][1];
	vertexBuffer[i++] = dodec[c][2];
	vertexBuffer[i++] = dodec[d][0];
	vertexBuffer[i++] = dodec[d][1];
	vertexBuffer[i++] = dodec[d][2];
	vertexBuffer[i++] = dodec[e][0];
	vertexBuffer[i++] = dodec[e][1];
	vertexBuffer[i++] = dodec[e][2];

	normBuffer[j++] = n0[0];
	normBuffer[j++] = n0[1];
	normBuffer[j++] = n0[2];
	normBuffer[j++] = n0[0];
	normBuffer[j++] = n0[1];
	normBuffer[j++] = n0[2];
	normBuffer[j++] = n0[0];
	normBuffer[j++] = n0[1];
	normBuffer[j++] = n0[2];
	normBuffer[j++] = n0[0];
	normBuffer[j++] = n0[1];
	normBuffer[j++] = n0[2];
	normBuffer[j++] = n0[0];
	normBuffer[j++] = n0[1];
	normBuffer[j++] = n0[2];

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,GL_FLOAT,0,vertexBuffer);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_FLOAT,0,normBuffer);

	glDrawArrays(shadeType,0,5);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

}
static void dodecahedron(GLenum type)
{
	static int inited = 0;

	if (inited == 0) {
		inited = 1;
		initDodecahedron();
	}
	pentagon(0, 1, 9, 16, 5, type);
	pentagon(1, 0, 3, 18, 7, type);
	pentagon(1, 7, 11, 10, 9, type);
	pentagon(11, 7, 18, 19, 6, type);
	pentagon(8, 17, 16, 9, 10, type);
	pentagon(2, 14, 15, 6, 19, type);
	pentagon(2, 13, 12, 4, 14, type);
	pentagon(2, 19, 18, 3, 13, type);
	pentagon(3, 0, 5, 12, 13, type);
	pentagon(6, 15, 8, 10, 11, type);
	pentagon(4, 17, 8, 15, 14, type);
	pentagon(4, 12, 5, 16, 17, type);
}
static void doughnut(GLfloat r, GLfloat R, GLint nsides, GLint rings)
{
	int i, j;
	GLfloat theta, phi, theta1;
	GLfloat cosTheta, sinTheta;
	GLfloat cosTheta1, sinTheta1;
	GLfloat ringDelta, sideDelta;
	GLfloat *vertexBuffer;
	GLfloat *normBuffer;

	vertexBuffer = new GLfloat[rings*(nsides+1)*2*3];
	normBuffer = new GLfloat[rings*(nsides+1)*2*3];

	int vert_i = 0;
	int norm_i = 0;

	ringDelta = (GLfloat)(2.0f * M_PI) / rings;
	sideDelta = (GLfloat)(2.0f * M_PI) / nsides;

	theta = 0.0;
	cosTheta = 1.0;
	sinTheta = 0.0;
	for (i = rings - 1; i >= 0; i--) 
	{
		theta1 = theta + ringDelta;
		cosTheta1 = cos(theta1);
		sinTheta1 = sin(theta1);
		phi = 0.0;
		for (j = nsides;j>=0; j--)
		{
			GLfloat cosPhi, sinPhi, dist;
			phi += sideDelta;
			cosPhi = cos(phi);
			sinPhi = sin(phi);
			dist = R + r * cosPhi;
			vertexBuffer[vert_i++] = cosTheta1 * dist;
			vertexBuffer[vert_i++] = -sinTheta1 * dist;
			vertexBuffer[vert_i++] = r * sinPhi;
			vertexBuffer[vert_i++] = cosTheta * dist;
			vertexBuffer[vert_i++] = -sinTheta * dist;
			vertexBuffer[vert_i++] = r * sinPhi;

			normBuffer[norm_i++] = cosTheta1 * cosPhi;
			normBuffer[norm_i++] = -sinTheta1 * cosPhi;
			normBuffer[norm_i++] = sinPhi;
			normBuffer[norm_i++] = cosTheta * cosPhi;
			normBuffer[norm_i++] = -sinTheta * cosPhi;
			normBuffer[norm_i++] = sinPhi;
		}
		theta = theta1;
		cosTheta = cosTheta1;
		sinTheta = sinTheta1;
	}
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,GL_FLOAT,0,vertexBuffer);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_FLOAT,0,normBuffer);

	glDrawArrays(GL_TRIANGLE_STRIP,0,rings*(nsides+1)*2);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	
	delete []vertexBuffer;
	delete []normBuffer;
}

static void drawBox(GLfloat size, GLenum type)
{
	static GLfloat n[6][3] =
	{
		{-1.0, 0.0, 0.0},
		{0.0, 1.0, 0.0},
		{1.0, 0.0, 0.0},
		{0.0, -1.0, 0.0},
		{0.0, 0.0, 1.0},
		{0.0, 0.0, -1.0}
	};
	static GLint faces[6][4] =
	{
		{0, 1, 2, 3},
		{3, 2, 6, 7},
		{7, 6, 5, 4},
		{4, 5, 1, 0},
		{5, 6, 2, 1},
		{7, 4, 0, 3}
	};
	GLfloat v[8][3];
	GLint i;

	v[0][0] = v[1][0] = v[2][0] = v[3][0] = -size / 2;
	v[4][0] = v[5][0] = v[6][0] = v[7][0] = size / 2;
	v[0][1] = v[1][1] = v[4][1] = v[5][1] = -size / 2;
	v[2][1] = v[3][1] = v[6][1] = v[7][1] = size / 2;
	v[0][2] = v[3][2] = v[4][2] = v[7][2] = -size / 2;
	v[1][2] = v[2][2] = v[5][2] = v[6][2] = size / 2;

	GLfloat vertexBuffer[6*4*3];
	GLfloat normBuffer[6*4*3];
	int vert_i = 0;
	int norm_i = 0;
	for (i = 5; i >= 0; i--)
	{
		vertexBuffer[vert_i++] = v[faces[i][0]][0];
		vertexBuffer[vert_i++] = v[faces[i][0]][1];
		vertexBuffer[vert_i++] = v[faces[i][0]][2];
		vertexBuffer[vert_i++] = v[faces[i][1]][0];
		vertexBuffer[vert_i++] = v[faces[i][1]][1];
		vertexBuffer[vert_i++] = v[faces[i][1]][2];
		vertexBuffer[vert_i++] = v[faces[i][2]][0];
		vertexBuffer[vert_i++] = v[faces[i][2]][1];
		vertexBuffer[vert_i++] = v[faces[i][2]][2];
		vertexBuffer[vert_i++] = v[faces[i][3]][0];
		vertexBuffer[vert_i++] = v[faces[i][3]][1];
		vertexBuffer[vert_i++] = v[faces[i][3]][2];

		normBuffer[norm_i++] = n[i][0];
		normBuffer[norm_i++] = n[i][1];
		normBuffer[norm_i++] = n[i][2];
		normBuffer[norm_i++] = n[i][0];
		normBuffer[norm_i++] = n[i][1];
		normBuffer[norm_i++] = n[i][2];
		normBuffer[norm_i++] = n[i][0];
		normBuffer[norm_i++] = n[i][1];
		normBuffer[norm_i++] = n[i][2];
		normBuffer[norm_i++] = n[i][0];
		normBuffer[norm_i++] = n[i][1];
		normBuffer[norm_i++] = n[i][2];
	}
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,GL_FLOAT,0,vertexBuffer);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_FLOAT,0,normBuffer);

	glDrawArrays(type,0,4);
	glDrawArrays(type,4,4);
	glDrawArrays(type,8,4);
	glDrawArrays(type,12,4);
	glDrawArrays(type,16,4);
	glDrawArrays(type,20,4);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);	
}

static void crossprod(GLfloat v1[3], GLfloat v2[3], GLfloat prod[3])
{
	GLfloat p[3];         /* in case prod == v1 or v2 */

	p[0] = v1[1] * v2[2] - v2[1] * v1[2];
	p[1] = v1[2] * v2[0] - v2[2] * v1[0];
	p[2] = v1[0] * v2[1] - v2[0] * v1[1];
	prod[0] = p[0];
	prod[1] = p[1];
	prod[2] = p[2];
}
static void normalize(float v[3])
{
	float r;

	r = sqrt( v[0]*v[0] + v[1]*v[1] + v[2]*v[2] );
	if (r == 0.0) return;

	v[0] /= r;
	v[1] /= r;
	v[2] /= r;
}

static void cross(float v1[3], float v2[3], float result[3])
{
	result[0] = v1[1]*v2[2] - v1[2]*v2[1];
	result[1] = v1[2]*v2[0] - v1[0]*v2[2];
	result[2] = v1[0]*v2[1] - v1[1]*v2[0];
}

static void initQuadObj(void)
{
	quadObj = MRVL_gluNewQuadric();
	if (!quadObj)
		printf("out of memory.");
}

#define QUAD_OBJ_INIT() { if(!quadObj) initQuadObj(); }






//////////////////////////////////////////////////////////////////////////
/*---------------------------------------GLU Llb----------------------------------------------*/
static void MRVL_gluMakeIdentityf(GLfloat m[16])
{
	m[0+4*0] = 1; m[0+4*1] = 0; m[0+4*2] = 0; m[0+4*3] = 0;
	m[1+4*0] = 0; m[1+4*1] = 1; m[1+4*2] = 0; m[1+4*3] = 0;
	m[2+4*0] = 0; m[2+4*1] = 0; m[2+4*2] = 1; m[2+4*3] = 0;
	m[3+4*0] = 0; m[3+4*1] = 0; m[3+4*2] = 0; m[3+4*3] = 1;
}
void MRVL_gluPerspective(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar)
{
	GLfloat m[4][4];
	GLfloat sine, cotangent, deltaZ;
	GLfloat radians = (GLfloat)(fovy / 2 * PI / 180);

	deltaZ = zFar - zNear;
	sine = sin(radians);
	if ((deltaZ == 0) || (sine == 0) || (aspect == 0)) {
		return;
	}
	cotangent = COS(radians) / sine;

	MRVL_gluMakeIdentityf(&m[0][0]);
	m[0][0] = cotangent / aspect;
	m[1][1] = cotangent;
	m[2][2] = -(zFar + zNear) / deltaZ;
	m[2][3] = -1;
	m[3][2] = -2 * zNear * zFar / deltaZ;
	m[3][3] = 0;
	glMultMatrixf(&m[0][0]);
}

void MRVL_gluLookAt(GLfloat eyex, GLfloat eyey, GLfloat eyez, GLfloat centerx,
					GLfloat centery, GLfloat centerz, GLfloat upx, GLfloat upy,
					GLfloat upz)
{
	float forward[3], side[3], up[3];
	GLfloat m[4][4];

	forward[0] = centerx - eyex;
	forward[1] = centery - eyey;
	forward[2] = centerz - eyez;

	up[0] = upx;
	up[1] = upy;
	up[2] = upz;

	normalize(forward);

	/* Side = forward x up */
	cross(forward, up, side);
	normalize(side);

	/* Recompute up as: up = side x forward */
	cross(side, forward, up);

	MRVL_gluMakeIdentityf(&m[0][0]);
	m[0][0] = side[0];
	m[1][0] = side[1];
	m[2][0] = side[2];

	m[0][1] = up[0];
	m[1][1] = up[1];
	m[2][1] = up[2];

	m[0][2] = -forward[0];
	m[1][2] = -forward[1];
	m[2][2] = -forward[2];

	glMultMatrixf(&m[0][0]);
	glTranslatef(-eyex, -eyey, -eyez);
}
MRVL_GLUquadricObj* MRVL_gluNewQuadric(void)
{
	MRVL_GLUquadricObj *newstate;

	newstate = new (MRVL_GLUquadricObj);
	if (newstate == NULL) {
		/* Can't report an error at this point... */
		return NULL;
	}
	newstate->normals = GLU_SMOOTH;
	newstate->textureCoords = GL_FALSE;
	newstate->orientation = GLU_OUTSIDE;
	newstate->drawStyle = GLU_FILL;
	return newstate;
}

void MRVL_gluQuadricNormals(MRVL_GLUquadricObj *qobj, GLenum normals)
{
	qobj->normals = normals;
}

void MRVL_gluQuadricTexture(MRVL_GLUquadricObj *qobj, GLboolean textureCoords)
{
	qobj->textureCoords = textureCoords;
}
void MRVL_gluQuadricDrawStyle(MRVL_GLUquadricObj *qobj, GLenum drawStyle)
{
	switch(drawStyle) {
	  case GLU_POINT:
	  case GLU_LINE:
	  case GLU_FILL:
	  case GLU_SILHOUETTE:
		  break;
	  default:
		  return;
	}
	qobj->drawStyle = drawStyle;
}

void MRVL_gluCylinder(MRVL_GLUquadricObj *qobj, GLfloat baseRadius, GLfloat topRadius,
				  GLfloat height, GLint slices, GLint stacks)
{
	GLint i,j;
	GLfloat sinCache[CACHE_SIZE];
	GLfloat cosCache[CACHE_SIZE];
	GLfloat sinCache2[CACHE_SIZE];
	GLfloat cosCache2[CACHE_SIZE];
	GLfloat sinCache3[CACHE_SIZE];
	GLfloat cosCache3[CACHE_SIZE];
	GLfloat angle;
	GLfloat zLow, zHigh;
	GLfloat sintemp, costemp;
	GLfloat length;
	GLfloat deltaRadius;
	GLfloat zNormal;
	GLfloat xyNormalRatio;
	GLfloat radiusLow, radiusHigh;
	int needCache2, needCache3;

	if (slices >= CACHE_SIZE) slices = CACHE_SIZE-1;

	if (slices < 2 || stacks < 1 || baseRadius < 0.0 || topRadius < 0.0 ||
		height < 0.0) {
			return;
	}

	/* Compute length (needed for normal calculations) */
	deltaRadius = baseRadius - topRadius;
	length = SQRT(deltaRadius*deltaRadius + height*height);
	if (length == 0.0) {
		return;
	}

	/* Cache is the vertex locations cache */
	/* Cache2 is the various normals at the vertices themselves */
	/* Cache3 is the various normals for the faces */
	needCache2 = needCache3 = 0;
	if (qobj->normals == GLU_SMOOTH) {
		needCache2 = 1;
	}

	if (qobj->normals == GLU_FLAT) {
		if (qobj->drawStyle != GLU_POINT) {
			needCache3 = 1;
		}
		if (qobj->drawStyle == GLU_LINE) {
			needCache2 = 1;
		}
	}

	zNormal = deltaRadius / length;
	xyNormalRatio = height / length;

	for (i = 0; i < slices; i++) {
		angle = (GLfloat)(2 * PI * i) / slices;
		if (needCache2) {
			if (qobj->orientation == GLU_OUTSIDE) {
				sinCache2[i] = xyNormalRatio * SIN(angle);
				cosCache2[i] = xyNormalRatio * COS(angle);
			} else {
				sinCache2[i] = -xyNormalRatio * SIN(angle);
				cosCache2[i] = -xyNormalRatio * COS(angle);
			}
		}
		sinCache[i] = SIN(angle);
		cosCache[i] = COS(angle);
	}

	if (needCache3) {
		for (i = 0; i < slices; i++) {
			angle = (GLfloat)(2 * PI * (i-0.5f)) / slices;
			if (qobj->orientation == GLU_OUTSIDE) {
				sinCache3[i] = xyNormalRatio * SIN(angle);
				cosCache3[i] = xyNormalRatio * COS(angle);
			} else {
				sinCache3[i] = -xyNormalRatio * SIN(angle);
				cosCache3[i] = -xyNormalRatio * COS(angle);
			}
		}
	}

	sinCache[slices] = sinCache[0];
	cosCache[slices] = cosCache[0];
	if (needCache2) {
		sinCache2[slices] = sinCache2[0];
		cosCache2[slices] = cosCache2[0];
	}
	if (needCache3) {
		sinCache3[slices] = sinCache3[0];
		cosCache3[slices] = cosCache3[0];
	}
	
	GLfloat *vertexBuffer;
	GLfloat *txlBuffer;
	GLfloat *normBuffer;
	int vert_i = 0;
	int txl_i = 0;
	int norm_i = 0;

	switch (qobj->drawStyle) {
	  case GLU_FILL:
		  vertexBuffer = new GLfloat[stacks * (slices+1) * 2 * 3];
		  txlBuffer = new GLfloat[stacks * (slices+1) * 2 * 2];
		  normBuffer = new GLfloat[stacks * (slices+1) * 2 * 3];
		  glEnableClientState(GL_VERTEX_ARRAY);
		  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		  glEnableClientState(GL_NORMAL_ARRAY);
		  for (j = 0; j < stacks; j++) 
		  {
			  zLow = j * height / stacks;
			  zHigh = (j + 1) * height / stacks;
			  radiusLow = baseRadius - deltaRadius * ((float) j / stacks);
			  radiusHigh = baseRadius - deltaRadius * ((float) (j + 1) / stacks);

			  for (i = 0; i <= slices; i++)
			  {
				  switch(qobj->normals) 
				  {
					  case GLU_FLAT:
						  normBuffer[norm_i++] = sinCache3[i];
						  normBuffer[norm_i++] = cosCache3[i];
						  normBuffer[norm_i++] = zNormal;
						  normBuffer[norm_i++] = sinCache3[i];
						  normBuffer[norm_i++] = cosCache3[i];
						  normBuffer[norm_i++] = zNormal;
						  break;
					  case GLU_SMOOTH:
						  normBuffer[norm_i++] = sinCache2[i];
						  normBuffer[norm_i++] = cosCache2[i];
						  normBuffer[norm_i++] = zNormal;
						  normBuffer[norm_i++] = sinCache2[i];
						  normBuffer[norm_i++] = cosCache2[i];
						  normBuffer[norm_i++] = zNormal;
						  break;
					  case GLU_NONE:
					  default:
						  break;
				  }
				  if(qobj->orientation == GLU_OUTSIDE)
				  {
					  if (qobj->textureCoords)
					  {
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = (float) j / stacks;
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = (float) (j+1) / stacks;
					  }
					  vertexBuffer[vert_i++] = radiusLow * sinCache[i];
					  vertexBuffer[vert_i++] = radiusLow * cosCache[i];
					  vertexBuffer[vert_i++] = zLow;
					  vertexBuffer[vert_i++] = radiusHigh * sinCache[i];
					  vertexBuffer[vert_i++] = radiusHigh * cosCache[i];
					  vertexBuffer[vert_i++] = zHigh;	  
				  }
				  else
				  {
					  if (qobj->textureCoords)
					  {
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = (float) (j+1) / stacks;
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = (float) j / stacks;
					  }
					  vertexBuffer[vert_i++] = radiusHigh * sinCache[i];
					  vertexBuffer[vert_i++] = radiusHigh * cosCache[i];
					  vertexBuffer[vert_i++] = zHigh;
					  vertexBuffer[vert_i++] = radiusLow * sinCache[i];
					  vertexBuffer[vert_i++] = radiusLow * cosCache[i];
					  vertexBuffer[vert_i++] = zLow;				
				  }
			  }
		  }
		  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
		  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
		  glNormalPointer(GL_FLOAT,0,normBuffer);

		  glDrawArrays(GL_TRIANGLE_STRIP,0,stacks * (slices+1) * 2);
			
		  glDisableClientState(GL_VERTEX_ARRAY);
		  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		  glDisableClientState(GL_NORMAL_ARRAY);
		  delete [] vertexBuffer;
		  delete [] txlBuffer;
		  delete [] normBuffer;
		  break;
	  case GLU_POINT:
		  vertexBuffer = new GLfloat[slices * (stacks+1)*3];
		  txlBuffer = new GLfloat[slices * (stacks+1)*2];
		  normBuffer = new GLfloat[slices * (stacks+1)*3];
		  glEnableClientState(GL_VERTEX_ARRAY);
		  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		  glEnableClientState(GL_NORMAL_ARRAY);
		  for (i = 0; i < slices; i++)
		  {
			  switch(qobj->normals) 
			  {
				  case GLU_FLAT:
				  case GLU_SMOOTH:
					  normBuffer[norm_i++] = sinCache2[i];
					  normBuffer[norm_i++] = cosCache2[i];
					  normBuffer[norm_i++] = zNormal;
					  break;
				  case GLU_NONE:
				  default:
					  break;
			  }
			  sintemp = sinCache[i];
			  costemp = cosCache[i];
			  for (j = 0; j <= stacks; j++)
			  {
				  zLow = j * height / stacks;
				  radiusLow = baseRadius - deltaRadius * ((float) j / stacks);
				  if (qobj->textureCoords)
				  {
					  txlBuffer[txl_i++] = 1 - (float) i / slices;
					  txlBuffer[txl_i++] = (float) j / stacks;
				  }
				  vertexBuffer[vert_i++] = radiusLow * sintemp;
				  vertexBuffer[vert_i++] = radiusLow * costemp;
				  vertexBuffer[vert_i++] = zLow;
			  }
		  }
		  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
		  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
		  glNormalPointer(GL_FLOAT,0,normBuffer);

		  glDrawArrays(GL_POINTS,0,slices * (stacks+1));

		  glDisableClientState(GL_VERTEX_ARRAY);
		  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		  glDisableClientState(GL_NORMAL_ARRAY);
		  delete [] vertexBuffer;
		  delete [] txlBuffer;
		  delete [] normBuffer;
		  break;
	  case GLU_LINE:
		  vertexBuffer = new GLfloat[stacks * (slices+1) * 3];
		  txlBuffer = new GLfloat[stacks * (slices+1) * 2];
		  normBuffer = new GLfloat[stacks * (slices+1) * 3];
		  glEnableClientState(GL_VERTEX_ARRAY);
		  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		  glEnableClientState(GL_NORMAL_ARRAY);
		  for (j = 0; j < stacks; j++) 
		  {
			  zLow = j * height / stacks;
			  radiusLow = baseRadius - deltaRadius * ((float) j / stacks);

			  for (i = 0; i <= slices; i++)
			  {
				  switch(qobj->normals) 
				  {
					  case GLU_FLAT:
						  normBuffer[norm_i++] = sinCache3[i];
						  normBuffer[norm_i++] = cosCache3[i];
						  normBuffer[norm_i++] = zNormal;
						  break;
					  case GLU_SMOOTH:
						  normBuffer[norm_i++] = sinCache2[i];
						  normBuffer[norm_i++] = cosCache2[i];
						  normBuffer[norm_i++] = zNormal;
						  break;
					  case GLU_NONE:
					  default:
						  break;
				  }

				  if (qobj->textureCoords)
				  {
					  txlBuffer[txl_i++] = 1 - (float) i / slices;
					  txlBuffer[txl_i++] = (float) j / stacks;
				  }
				  vertexBuffer[vert_i++] = radiusLow * sinCache[i];
				  vertexBuffer[vert_i++] = radiusLow * cosCache[i];
				  vertexBuffer[vert_i++] = zLow;
			  }
		  }
		  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
		  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
		  glNormalPointer(GL_FLOAT,0,normBuffer);

		  glDrawArrays(GL_LINE_STRIP,0,stacks * (slices+1));

		  glDisableClientState(GL_VERTEX_ARRAY);
		  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		  glDisableClientState(GL_NORMAL_ARRAY);
		  delete [] vertexBuffer;
		  delete [] txlBuffer;
		  delete [] normBuffer;
		  break;
		  /* Intentionally fall through here... */
	  case GLU_SILHOUETTE:
		  vertexBuffer = new GLfloat[(stacks+1)*(slices+1)*3];
		  txlBuffer = new GLfloat[(stacks+1)*(slices+1)*2];
		  normBuffer = new GLfloat[(stacks+1)*(slices+1)*3];
		  glEnableClientState(GL_VERTEX_ARRAY);
		  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		  glEnableClientState(GL_NORMAL_ARRAY);

		  for (j = 0; j <= stacks; j++)
		  {
			  zLow = j * height / stacks;
			  radiusLow = baseRadius - deltaRadius * ((float) j / stacks);
			  for (i = 0; i <= slices; i++)
			  {
				  switch(qobj->normals) 
				  {
					  case GLU_FLAT:
						  normBuffer[norm_i++] = sinCache3[i];
						  normBuffer[norm_i++] = cosCache3[i];
						  normBuffer[norm_i++] = zNormal;
						  break;
					  case GLU_SMOOTH:
						  normBuffer[norm_i++] = sinCache2[i];
						  normBuffer[norm_i++] = cosCache2[i];
						  normBuffer[norm_i++] = zNormal;
						  break;
					  case GLU_NONE:
					  default:
						  break;
				  }
				  if (qobj->textureCoords)
				  {
					  txlBuffer[txl_i++] = 1 - (float) i / slices;
					  txlBuffer[txl_i++] = (float) j / stacks;
				  }
				  vertexBuffer[vert_i++] = radiusLow * sinCache[i];
				  vertexBuffer[vert_i++] = radiusLow * cosCache[i];
				  vertexBuffer[vert_i++] = zLow;
			  }
		  }
		  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
		  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
		  glNormalPointer(GL_FLOAT,0,normBuffer);

		  glDrawArrays(GL_LINE_STRIP,0,(stacks+1) * (slices+1));

		  delete [] vertexBuffer;
		  delete [] normBuffer;
		  delete [] txlBuffer;

		  vert_i = 0;
		  txl_i = 0;
		  norm_i = 0;

		  vertexBuffer = new GLfloat[(stacks+1)*slices*3];
		  txlBuffer = new GLfloat[(stacks+1)*slices*2];
		  normBuffer = new GLfloat[(stacks+1)*slices*3];

		  for (i = 0; i < slices; i++) 
		  {
			  switch(qobj->normals) {
				  case GLU_FLAT:
				  case GLU_SMOOTH:
					  normBuffer[norm_i++] = sinCache2[i];
					  normBuffer[norm_i++] = cosCache2[i];
					  normBuffer[norm_i++] = 0.0f;
					  break;
				  case GLU_NONE:
				  default:
					  break;
			  }
			  sintemp = sinCache[i];
			  costemp = cosCache[i];
			  for (j = 0; j <= stacks; j++) 
			  {
				  zLow = j * height / stacks;
				  radiusLow = baseRadius - deltaRadius * ((float) j / stacks);

				  if (qobj->textureCoords)
				  {
					  txlBuffer[txl_i++] = 1 - (float) i / slices;
					  txlBuffer[txl_i++] = (float) j / stacks;
				  }
				  vertexBuffer[vert_i++] = radiusLow * sintemp;
				  vertexBuffer[vert_i++] = radiusLow * costemp;
				  vertexBuffer[vert_i++] = zLow;
			  }
		  }
		  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
		  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
		  glNormalPointer(GL_FLOAT,0,normBuffer);

		  glDrawArrays(GL_LINE_STRIP,0,(stacks+1) * slices);

		  glDisableClientState(GL_NORMAL_ARRAY);
		  glDisableClientState(GL_VERTEX_ARRAY);
		  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		  delete [] vertexBuffer;
		  delete [] normBuffer;
		  delete [] txlBuffer;
		  break;
	  default:
		  break;
	}
}
void MRVL_gluCylinderPart(MRVL_GLUquadricObj *qobj, GLfloat baseRadius, GLfloat topRadius,
				  GLfloat height, GLint slices, GLint stacks, GLfloat partial)
{
	GLint i,j;
	GLfloat sinCache[CACHE_SIZE];
	GLfloat cosCache[CACHE_SIZE];
	GLfloat sinCache2[CACHE_SIZE];
	GLfloat cosCache2[CACHE_SIZE];
	GLfloat sinCache3[CACHE_SIZE];
	GLfloat cosCache3[CACHE_SIZE];
	GLfloat angle;
	GLfloat zLow, zHigh;
	GLfloat sintemp, costemp;
	GLfloat length;
	GLfloat deltaRadius;
	GLfloat zNormal;
	GLfloat xyNormalRatio;
	GLfloat radiusLow, radiusHigh;
	int needCache2, needCache3;

	if (slices >= CACHE_SIZE) slices = CACHE_SIZE-1;

	if (slices < 2 || stacks < 1 || baseRadius < 0.0 || topRadius < 0.0 ||
		height < 0.0) {
			return;
	}

	/* Compute length (needed for normal calculations) */
	deltaRadius = baseRadius - topRadius;
	length = SQRT(deltaRadius*deltaRadius + height*height);
	if (length == 0.0) {
		return;
	}

	/* Cache is the vertex locations cache */
	/* Cache2 is the various normals at the vertices themselves */
	/* Cache3 is the various normals for the faces */
	needCache2 = needCache3 = 0;
	if (qobj->normals == GLU_SMOOTH) {
		needCache2 = 1;
	}

	if (qobj->normals == GLU_FLAT) {
		if (qobj->drawStyle != GLU_POINT) {
			needCache3 = 1;
		}
		if (qobj->drawStyle == GLU_LINE) {
			needCache2 = 1;
		}
	}

	zNormal = deltaRadius / length;
	xyNormalRatio = height / length;

	for (i = 0; i < slices; i++) {
		angle = (GLfloat)(2 * PI * i) / slices;

        //
        angle *= partial;
        //
		if (needCache2) {
			if (qobj->orientation == GLU_OUTSIDE) {
				sinCache2[i] = xyNormalRatio * SIN(angle);
				cosCache2[i] = xyNormalRatio * COS(angle);
			} else {
				sinCache2[i] = -xyNormalRatio * SIN(angle);
				cosCache2[i] = -xyNormalRatio * COS(angle);
			}
		}
		sinCache[i] = SIN(angle);
		cosCache[i] = COS(angle);
	}

	if (needCache3) {
		for (i = 0; i < slices; i++) {
			angle = (GLfloat)(2 * PI * (i-0.5f)) / slices;
			if (qobj->orientation == GLU_OUTSIDE) {
				sinCache3[i] = xyNormalRatio * SIN(angle);
				cosCache3[i] = xyNormalRatio * COS(angle);
			} else {
				sinCache3[i] = -xyNormalRatio * SIN(angle);
				cosCache3[i] = -xyNormalRatio * COS(angle);
			}
		}
	}

	sinCache[slices] = sinCache[0];
	cosCache[slices] = cosCache[0];
	if (needCache2) {
		sinCache2[slices] = sinCache2[0];
		cosCache2[slices] = cosCache2[0];
	}
	if (needCache3) {
		sinCache3[slices] = sinCache3[0];
		cosCache3[slices] = cosCache3[0];
	}
	
	GLfloat *vertexBuffer;
	GLfloat *txlBuffer;
	GLfloat *normBuffer;
	int vert_i = 0;
	int txl_i = 0;
	int norm_i = 0;

	switch (qobj->drawStyle) {
	  case GLU_FILL:
		  vertexBuffer = new GLfloat[stacks * (slices+1) * 2 * 3];
		  txlBuffer = new GLfloat[stacks * (slices+1) * 2 * 2];
		  normBuffer = new GLfloat[stacks * (slices+1) * 2 * 3];
		  glEnableClientState(GL_VERTEX_ARRAY);
		  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		  glEnableClientState(GL_NORMAL_ARRAY);
		  for (j = 0; j < stacks; j++) 
		  {
			  zLow = j * height / stacks;
			  zHigh = (j + 1) * height / stacks;
			  radiusLow = baseRadius - deltaRadius * ((float) j / stacks);
			  radiusHigh = baseRadius - deltaRadius * ((float) (j + 1) / stacks);

			  for (i = 0; i <= slices; i++)
			  {
				  switch(qobj->normals) 
				  {
					  case GLU_FLAT:
						  normBuffer[norm_i++] = sinCache3[i];
						  normBuffer[norm_i++] = cosCache3[i];
						  normBuffer[norm_i++] = zNormal;
						  normBuffer[norm_i++] = sinCache3[i];
						  normBuffer[norm_i++] = cosCache3[i];
						  normBuffer[norm_i++] = zNormal;
						  break;
					  case GLU_SMOOTH:
						  normBuffer[norm_i++] = sinCache2[i];
						  normBuffer[norm_i++] = cosCache2[i];
						  normBuffer[norm_i++] = zNormal;
						  normBuffer[norm_i++] = sinCache2[i];
						  normBuffer[norm_i++] = cosCache2[i];
						  normBuffer[norm_i++] = zNormal;
						  break;
					  case GLU_NONE:
					  default:
						  break;
				  }
				  if(qobj->orientation == GLU_OUTSIDE)
				  {
					  if (qobj->textureCoords)
					  {
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = (float) j / stacks;
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = (float) (j+1) / stacks;
					  }
					  vertexBuffer[vert_i++] = radiusLow * sinCache[i];
					  vertexBuffer[vert_i++] = radiusLow * cosCache[i];
					  vertexBuffer[vert_i++] = zLow;
					  vertexBuffer[vert_i++] = radiusHigh * sinCache[i];
					  vertexBuffer[vert_i++] = radiusHigh * cosCache[i];
					  vertexBuffer[vert_i++] = zHigh;	  
				  }
				  else
				  {
					  if (qobj->textureCoords)
					  {
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = (float) (j+1) / stacks;
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = (float) j / stacks;
					  }
					  vertexBuffer[vert_i++] = radiusHigh * sinCache[i];
					  vertexBuffer[vert_i++] = radiusHigh * cosCache[i];
					  vertexBuffer[vert_i++] = zHigh;
					  vertexBuffer[vert_i++] = radiusLow * sinCache[i];
					  vertexBuffer[vert_i++] = radiusLow * cosCache[i];
					  vertexBuffer[vert_i++] = zLow;				
				  }
			  }
		  }
		  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
		  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
		  glNormalPointer(GL_FLOAT,0,normBuffer);
           
		  glDrawArrays(GL_TRIANGLE_STRIP,0,stacks * (slices+1) * 2);
			
		  glDisableClientState(GL_VERTEX_ARRAY);
		  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		  glDisableClientState(GL_NORMAL_ARRAY);
		  delete [] vertexBuffer;
		  delete [] txlBuffer;
		  delete [] normBuffer;
		  break;
	  case GLU_POINT:
		  vertexBuffer = new GLfloat[slices * (stacks+1)*3];
		  txlBuffer = new GLfloat[slices * (stacks+1)*2];
		  normBuffer = new GLfloat[slices * (stacks+1)*3];
		  glEnableClientState(GL_VERTEX_ARRAY);
		  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		  glEnableClientState(GL_NORMAL_ARRAY);
		  for (i = 0; i < slices; i++)
		  {
			  switch(qobj->normals) 
			  {
				  case GLU_FLAT:
				  case GLU_SMOOTH:
					  normBuffer[norm_i++] = sinCache2[i];
					  normBuffer[norm_i++] = cosCache2[i];
					  normBuffer[norm_i++] = zNormal;
					  break;
				  case GLU_NONE:
				  default:
					  break;
			  }
			  sintemp = sinCache[i];
			  costemp = cosCache[i];
			  for (j = 0; j <= stacks; j++)
			  {
				  zLow = j * height / stacks;
				  radiusLow = baseRadius - deltaRadius * ((float) j / stacks);
				  if (qobj->textureCoords)
				  {
					  txlBuffer[txl_i++] = 1 - (float) i / slices;
					  txlBuffer[txl_i++] = (float) j / stacks;
				  }
				  vertexBuffer[vert_i++] = radiusLow * sintemp;
				  vertexBuffer[vert_i++] = radiusLow * costemp;
				  vertexBuffer[vert_i++] = zLow;
			  }
		  }
		  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
		  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
		  glNormalPointer(GL_FLOAT,0,normBuffer);

		  glDrawArrays(GL_POINTS,0,slices * (stacks+1));

		  glDisableClientState(GL_VERTEX_ARRAY);
		  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		  glDisableClientState(GL_NORMAL_ARRAY);
		  delete [] vertexBuffer;
		  delete [] txlBuffer;
		  delete [] normBuffer;
		  break;
	  case GLU_LINE:
		  vertexBuffer = new GLfloat[stacks * (slices+1) * 3];
		  txlBuffer = new GLfloat[stacks * (slices+1) * 2];
		  normBuffer = new GLfloat[stacks * (slices+1) * 3];
		  glEnableClientState(GL_VERTEX_ARRAY);
		  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		  glEnableClientState(GL_NORMAL_ARRAY);
		  for (j = 0; j < stacks; j++) 
		  {
			  zLow = j * height / stacks;
			  radiusLow = baseRadius - deltaRadius * ((float) j / stacks);

			  for (i = 0; i <= slices; i++)
			  {
				  switch(qobj->normals) 
				  {
					  case GLU_FLAT:
						  normBuffer[norm_i++] = sinCache3[i];
						  normBuffer[norm_i++] = cosCache3[i];
						  normBuffer[norm_i++] = zNormal;
						  break;
					  case GLU_SMOOTH:
						  normBuffer[norm_i++] = sinCache2[i];
						  normBuffer[norm_i++] = cosCache2[i];
						  normBuffer[norm_i++] = zNormal;
						  break;
					  case GLU_NONE:
					  default:
						  break;
				  }

				  if (qobj->textureCoords)
				  {
					  txlBuffer[txl_i++] = 1 - (float) i / slices;
					  txlBuffer[txl_i++] = (float) j / stacks;
				  }
				  vertexBuffer[vert_i++] = radiusLow * sinCache[i];
				  vertexBuffer[vert_i++] = radiusLow * cosCache[i];
				  vertexBuffer[vert_i++] = zLow;
			  }
		  }
		  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
		  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
		  glNormalPointer(GL_FLOAT,0,normBuffer);

		  glDrawArrays(GL_LINE_STRIP,0,stacks * (slices+1));

		  glDisableClientState(GL_VERTEX_ARRAY);
		  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		  glDisableClientState(GL_NORMAL_ARRAY);
		  delete [] vertexBuffer;
		  delete [] txlBuffer;
		  delete [] normBuffer;
		  break;
		  /* Intentionally fall through here... */
	  case GLU_SILHOUETTE:
		  vertexBuffer = new GLfloat[(stacks+1)*(slices+1)*3];
		  txlBuffer = new GLfloat[(stacks+1)*(slices+1)*2];
		  normBuffer = new GLfloat[(stacks+1)*(slices+1)*3];
		  glEnableClientState(GL_VERTEX_ARRAY);
		  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		  glEnableClientState(GL_NORMAL_ARRAY);

		  for (j = 0; j <= stacks; j++)
		  {
			  zLow = j * height / stacks;
			  radiusLow = baseRadius - deltaRadius * ((float) j / stacks);
			  for (i = 0; i <= slices; i++)
			  {
				  switch(qobj->normals) 
				  {
					  case GLU_FLAT:
						  normBuffer[norm_i++] = sinCache3[i];
						  normBuffer[norm_i++] = cosCache3[i];
						  normBuffer[norm_i++] = zNormal;
						  break;
					  case GLU_SMOOTH:
						  normBuffer[norm_i++] = sinCache2[i];
						  normBuffer[norm_i++] = cosCache2[i];
						  normBuffer[norm_i++] = zNormal;
						  break;
					  case GLU_NONE:
					  default:
						  break;
				  }
				  if (qobj->textureCoords)
				  {
					  txlBuffer[txl_i++] = 1 - (float) i / slices;
					  txlBuffer[txl_i++] = (float) j / stacks;
				  }
				  vertexBuffer[vert_i++] = radiusLow * sinCache[i];
				  vertexBuffer[vert_i++] = radiusLow * cosCache[i];
				  vertexBuffer[vert_i++] = zLow;
			  }
		  }
		  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
		  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
		  glNormalPointer(GL_FLOAT,0,normBuffer);

		  glDrawArrays(GL_LINE_STRIP,0,(stacks+1) * (slices+1));

		  delete [] vertexBuffer;
		  delete [] normBuffer;
		  delete [] txlBuffer;

		  vert_i = 0;
		  txl_i = 0;
		  norm_i = 0;

		  vertexBuffer = new GLfloat[(stacks+1)*slices*3];
		  txlBuffer = new GLfloat[(stacks+1)*slices*2];
		  normBuffer = new GLfloat[(stacks+1)*slices*3];

		  for (i = 0; i < slices; i++) 
		  {
			  switch(qobj->normals) {
				  case GLU_FLAT:
				  case GLU_SMOOTH:
					  normBuffer[norm_i++] = sinCache2[i];
					  normBuffer[norm_i++] = cosCache2[i];
					  normBuffer[norm_i++] = 0.0f;
					  break;
				  case GLU_NONE:
				  default:
					  break;
			  }
			  sintemp = sinCache[i];
			  costemp = cosCache[i];
			  for (j = 0; j <= stacks; j++) 
			  {
				  zLow = j * height / stacks;
				  radiusLow = baseRadius - deltaRadius * ((float) j / stacks);

				  if (qobj->textureCoords)
				  {
					  txlBuffer[txl_i++] = 1 - (float) i / slices;
					  txlBuffer[txl_i++] = (float) j / stacks;
				  }
				  vertexBuffer[vert_i++] = radiusLow * sintemp;
				  vertexBuffer[vert_i++] = radiusLow * costemp;
				  vertexBuffer[vert_i++] = zLow;
			  }
		  }
		  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
		  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
		  glNormalPointer(GL_FLOAT,0,normBuffer);

		  glDrawArrays(GL_LINE_STRIP,0,(stacks+1) * slices);

		  glDisableClientState(GL_NORMAL_ARRAY);
		  glDisableClientState(GL_VERTEX_ARRAY);
		  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		  delete [] vertexBuffer;
		  delete [] normBuffer;
		  delete [] txlBuffer;
		  break;
	  default:
		  break;
	}
}
void MRVL_gluSphere(MRVL_GLUquadricObj *qobj, GLfloat radius, GLint slices, GLint stacks)
{
	GLint i,j;
	GLfloat sinCache1a[CACHE_SIZE];
	GLfloat cosCache1a[CACHE_SIZE];
	GLfloat sinCache2a[CACHE_SIZE];
	GLfloat cosCache2a[CACHE_SIZE];
	GLfloat sinCache3a[CACHE_SIZE];
	GLfloat cosCache3a[CACHE_SIZE];
	GLfloat sinCache1b[CACHE_SIZE];
	GLfloat cosCache1b[CACHE_SIZE];
	GLfloat sinCache2b[CACHE_SIZE];
	GLfloat cosCache2b[CACHE_SIZE];
	GLfloat sinCache3b[CACHE_SIZE];
	GLfloat cosCache3b[CACHE_SIZE];
	GLfloat angle;
	GLfloat zLow, zHigh;
	GLfloat sintemp1 = 0.0, sintemp2 = 0.0, sintemp3 = 0.0, sintemp4 = 0.0;
	GLfloat costemp1 = 0.0, costemp2 = 0.0, costemp3 = 0.0, costemp4 = 0.0;
	GLboolean needCache2, needCache3;
	GLint start, finish;

	if (slices >= CACHE_SIZE) slices = CACHE_SIZE-1;
	if (stacks >= CACHE_SIZE) stacks = CACHE_SIZE-1;
	if (slices < 2 || stacks < 1 || radius < 0.0) {
		return;
	}

	/* Cache is the vertex locations cache */
	/* Cache2 is the various normals at the vertices themselves */
	/* Cache3 is the various normals for the faces */
	needCache2 = needCache3 = GL_FALSE;

	if (qobj->normals == GLU_SMOOTH) {
		needCache2 = GL_TRUE;
	}

	if (qobj->normals == GLU_FLAT) {
		if (qobj->drawStyle != GLU_POINT) {
			needCache3 = GL_TRUE;
		}
		if (qobj->drawStyle == GLU_LINE) {
			needCache2 = GL_TRUE;
		}
	}

	for (i = 0; i < slices; i++) {
		angle = (GLfloat)(2 * PI * i) / slices;
		sinCache1a[i] = SIN(angle);
		cosCache1a[i] = COS(angle);
		if (needCache2) {
			sinCache2a[i] = sinCache1a[i];
			cosCache2a[i] = cosCache1a[i];
		}
	}

	for (j = 0; j <= stacks; j++) {
		angle = (GLfloat)(PI * j) / stacks;
		if (needCache2) {
			if (qobj->orientation == GLU_OUTSIDE) {
				sinCache2b[j] = SIN(angle);
				cosCache2b[j] = COS(angle);
			} else {
				sinCache2b[j] = -SIN(angle);
				cosCache2b[j] = -COS(angle);
			}
		}
		sinCache1b[j] = radius * SIN(angle);
		cosCache1b[j] = radius * COS(angle);
	}
	/* Make sure it comes to a point */
	sinCache1b[0] = 0;
	sinCache1b[stacks] = 0;

	if (needCache3) {
		for (i = 0; i < slices; i++) {
			angle = (GLfloat)(2 * PI * (i-0.5f)) / slices;
			sinCache3a[i] = SIN(angle);
			cosCache3a[i] = COS(angle);
		}
		for (j = 0; j <= stacks; j++) {
			angle = (GLfloat)(PI * (j - 0.5f)) / stacks;
			if (qobj->orientation == GLU_OUTSIDE) {
				sinCache3b[j] = SIN(angle);
				cosCache3b[j] = COS(angle);
			} else {
				sinCache3b[j] = -SIN(angle);
				cosCache3b[j] = -COS(angle);
			}
		}
	}

	sinCache1a[slices] = sinCache1a[0];
	cosCache1a[slices] = cosCache1a[0];
	if (needCache2) {
		sinCache2a[slices] = sinCache2a[0];
		cosCache2a[slices] = cosCache2a[0];
	}
	if (needCache3) {
		sinCache3a[slices] = sinCache3a[0];
		cosCache3a[slices] = cosCache3a[0];
	}

	switch (qobj->drawStyle) {
	  case GLU_FILL:
		  if (!(qobj->textureCoords)) 
		  {
			  start = 1;
			  finish = stacks - 1;

			  /* Low end first (j == 0 iteration) */
			  sintemp2 = sinCache1b[1];
			  zHigh = cosCache1b[1];
			  switch(qobj->normals) 
			  {
				  case GLU_FLAT:
					  sintemp3 = sinCache3b[1];
					  costemp3 = cosCache3b[1];
					  break;
				  case GLU_SMOOTH:
					  sintemp3 = sinCache2b[1];
					  costemp3 = cosCache2b[1];
					  glNormal3f(sinCache2a[0] * sinCache2b[0],cosCache2a[0] * sinCache2b[0],cosCache2b[0]);
					  break;
				  default:
					  break;
			  }
			  GLfloat *vertexBuffer;
			  GLfloat *normBuffer;
			  vertexBuffer = new GLfloat[(slices+1+slices+2)*3];
			  normBuffer = new GLfloat[(slices+1+slices+2)*3];
			  int vert_i = 0;
			  int norm_i = 0;
			  glEnableClientState(GL_VERTEX_ARRAY);
			  glEnableClientState(GL_NORMAL_ARRAY);

			  if (qobj->orientation == GLU_OUTSIDE)
			  {
				  for (i=slices;i>=0;i--)
				  {
					  switch(qobj->normals) {
						  case GLU_SMOOTH:
							  normBuffer[norm_i++] = sinCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = cosCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = costemp3;
							  break;
						  case GLU_FLAT:
							  if (i != slices) 
							  {
								  normBuffer[norm_i++] = sinCache3a[i+1] * sintemp3;
								  normBuffer[norm_i++] = cosCache3a[i+1] * sintemp3;
								  normBuffer[norm_i++] = costemp3;
							  }
							  break;
						  case GLU_NONE:
						  default:
							  break;
					  }
					  vertexBuffer[vert_i++] = sintemp2 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp2 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zHigh;
				  }
			  }
			  else
			  {
				  for (i = 0; i <= slices; i++) {
					  switch(qobj->normals) 
					  {
						  case GLU_SMOOTH:
							  normBuffer[norm_i++] = sinCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = cosCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = costemp3;
							  break;
						  case GLU_FLAT:
							  normBuffer[norm_i++] = sinCache3a[i] * sintemp3;
							  normBuffer[norm_i++] = cosCache3a[i] * sintemp3;
							  normBuffer[norm_i++] = costemp3;
							  break;
						  case GLU_NONE:
						  default:
							  break;
					  }
					  vertexBuffer[vert_i++] = sintemp2 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp2 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zHigh;
				  }
			  }
			  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
			  glNormalPointer(GL_FLOAT,0,normBuffer);

			  glDrawArrays(GL_TRIANGLE_FAN,0,slices+1);


				
			  /* High end next (j == stacks-1 iteration) */
			  sintemp2 = sinCache1b[stacks-1];
			  zHigh = cosCache1b[stacks-1];
			  switch(qobj->normals) {
				  case GLU_FLAT:
					  sintemp3 = sinCache3b[stacks];
					  costemp3 = cosCache3b[stacks];
					  break;
				  case GLU_SMOOTH:
					  sintemp3 = sinCache2b[stacks-1];
					  costemp3 = cosCache2b[stacks-1];
					  glNormal3f(sinCache2a[stacks] * sinCache2b[stacks],cosCache2a[stacks] * sinCache2b[stacks],cosCache2b[stacks]);
					  break;
				  default:
					  break;
			  }

			  vertexBuffer[vert_i++] = 0.0f;
			  vertexBuffer[vert_i++] = 0.0f;
			  vertexBuffer[vert_i++] = -radius;
			  if(qobj->orientation == GLU_OUTSIDE)
			  {
				  for (i = 0; i <= slices; i++) {
					  switch(qobj->normals) {
						  case GLU_SMOOTH:
							  normBuffer[norm_i++] = sinCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = cosCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = costemp3;
							  break;
						  case GLU_FLAT:
							  normBuffer[norm_i++] = sinCache3a[i] * sintemp3;
							  normBuffer[norm_i++] = cosCache3a[i] * sintemp3;
							  normBuffer[norm_i++] = costemp3;
							  break;
						  case GLU_NONE:
						  default:
							  break;
					  }
					  vertexBuffer[vert_i++] = sintemp2 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp2 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zHigh;
				  }
			  }
			  else
			  {
				  for (i = slices; i >= 0; i--) 
				  {
					  switch(qobj->normals) {
						  case GLU_SMOOTH:
							  normBuffer[norm_i++] = sinCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = cosCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = costemp3;
							  break;
						  case GLU_FLAT:
							  if (i != slices) {
								  normBuffer[norm_i++] = sinCache3a[i+1] * sintemp3;
								  normBuffer[norm_i++] = cosCache3a[i+1] * sintemp3;
								  normBuffer[norm_i++] = costemp3;
							  }
							  break;
						  case GLU_NONE:
						  default:
							  break;
					  }
					  vertexBuffer[vert_i++] = sintemp2 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp2 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zHigh;
				  }
			  }

			  glDrawArrays(GL_TRIANGLE_FAN,slices+1,slices+2);

			  glDisableClientState(GL_VERTEX_ARRAY);
			  glDisableClientState(GL_NORMAL_ARRAY);
			  delete[] vertexBuffer;
			  delete[] normBuffer;
	      } 
		  else 
		  {
			  start = 0;
			  finish = stacks;
		  }
		  {
		  GLfloat *vertexBuffer;
		  GLfloat *normBuffer;
		  GLfloat *txlBuffer;
		  vertexBuffer = new GLfloat[(finish-start)*(slices+1)*2*3];
		  normBuffer = new GLfloat[(finish-start)*(slices+1)*2*3];
		  txlBuffer = new GLfloat[(finish-start)*(slices+1)*2*2];

		  int vert_i = 0;
		  int norm_i = 0;
		  int txl_i = 0;

		  for (j = start; j < finish; j++) 
		  {
			  zLow = cosCache1b[j];
			  zHigh = cosCache1b[j+1];
			  sintemp1 = sinCache1b[j];
			  sintemp2 = sinCache1b[j+1];
			  switch(qobj->normals) {
				  case GLU_FLAT:
					  sintemp4 = sinCache3b[j+1];
					  costemp4 = cosCache3b[j+1];
					  break;
				  case GLU_SMOOTH:
					  if (qobj->orientation == GLU_OUTSIDE) {
						  sintemp3 = sinCache2b[j+1];
						  costemp3 = cosCache2b[j+1];
						  sintemp4 = sinCache2b[j];
						  costemp4 = cosCache2b[j];
					  } else {
						  sintemp3 = sinCache2b[j];
						  costemp3 = cosCache2b[j];
						  sintemp4 = sinCache2b[j+1];
						  costemp4 = cosCache2b[j+1];
					  }
					  break;
				  default:
					  break;
			  }
			  for (i = 0; i <= slices; i++)
			  {
				  switch(qobj->normals) 
				  {
					  case GLU_SMOOTH:
						  normBuffer[norm_i++] = sinCache2a[i] * sintemp3;
						  normBuffer[norm_i++] = cosCache2a[i] * sintemp3;
						  normBuffer[norm_i++] = costemp3;
						  break;
					  case GLU_FLAT:
					  case GLU_NONE:
					  default:
						  break;
				  }
				  if (qobj->orientation == GLU_OUTSIDE) 
				  {
					  if (qobj->textureCoords) 
					  {
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = 1 - (float) (j+1) / stacks;
					  }
					  vertexBuffer[vert_i++] = sintemp2 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp2 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zHigh;
				  } 
				  else 
				  {
					  if (qobj->textureCoords) 
					  {
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = 1 - (float) j / stacks;
					  }
					  vertexBuffer[vert_i++] = sintemp1 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp1 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zLow;
				  }
				  switch(qobj->normals) {
					  case GLU_SMOOTH:
						  normBuffer[norm_i++] = sinCache2a[i] * sintemp4;
						  normBuffer[norm_i++] = cosCache2a[i] * sintemp4;
						  normBuffer[norm_i++] = costemp4;
						  break;
					  case GLU_FLAT:
						  normBuffer[norm_i++] = sinCache3a[i] * sintemp4;
						  normBuffer[norm_i++] = cosCache3a[i] * sintemp4;
						  normBuffer[norm_i++] = costemp4;
						  break;
					  case GLU_NONE:
					  default:
						  break;
				  }
				  if (qobj->orientation == GLU_OUTSIDE) 
				  {
					  if (qobj->textureCoords) {
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = 1 - (float) j / stacks;
					  }
					  vertexBuffer[vert_i++] = sintemp1 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp1 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zLow;
				  } 
				  else 
				  {
					  if (qobj->textureCoords) {
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = 1 - (float) (j+1) / stacks;
					  }
					  vertexBuffer[vert_i++] = sintemp2 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp2 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zHigh;
				  }
			  }
		  }
		  glEnableClientState(GL_VERTEX_ARRAY);
		  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
		  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
		  glEnableClientState(GL_NORMAL_ARRAY);
		  glNormalPointer(GL_FLOAT,0,normBuffer);
		  glDrawArrays(GL_TRIANGLE_STRIP,0,(finish-start)*(slices+1)*2);

		  glDisableClientState(GL_VERTEX_ARRAY);
		  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		  glDisableClientState(GL_NORMAL_ARRAY);

		  delete[] vertexBuffer;
		  delete[] txlBuffer;
		  delete[] normBuffer;
	  }
		  break;
	  case GLU_POINT:
		  {
			  GLfloat *vertexBuffer;
			  GLfloat *txlBuffer;
			  GLfloat *normBuffer;
			  int vert_i = 0;
			  int norm_i = 0;
			  int txl_i = 0;
			  vertexBuffer = new GLfloat[(stacks+1)*slices*3];
			  txlBuffer = new GLfloat[(stacks+1)*slices*2];
			  normBuffer = new GLfloat[(stacks+1)*slices*3];

			  for (j = 0; j <= stacks; j++) 
			  {
				  sintemp1 = sinCache1b[j];
				  costemp1 = cosCache1b[j];
				  switch(qobj->normals) {
					  case GLU_FLAT:
					  case GLU_SMOOTH:
						  sintemp2 = sinCache2b[j];
						  costemp2 = cosCache2b[j];
						  break;
					  default:
						  break;
				  }
				  for (i = 0; i < slices; i++) 
				  {
					  switch(qobj->normals) {
							  case GLU_FLAT:
							  case GLU_SMOOTH:
								  normBuffer[norm_i++] = sinCache2a[i] * sintemp2;
								  normBuffer[norm_i++] = cosCache2a[i] * sintemp2;
								  normBuffer[norm_i++] = costemp2;
								  break;
							  case GLU_NONE:
							  default:
								  break;
					  }
					  zLow = j * radius / stacks;
					  if (qobj->textureCoords) 
					  {
							  txlBuffer[txl_i++] = 1 - (float) i / slices;
							  txlBuffer[txl_i++] = 1 - (float) j / stacks;
					  }
						  vertexBuffer[vert_i++] = sintemp1 * sinCache1a[i];
						  vertexBuffer[vert_i++] = sintemp1 * cosCache1a[i];
						  vertexBuffer[vert_i++] = costemp1;

				  }
		      }
			  glEnableClientState(GL_VERTEX_ARRAY);
			  glEnableClientState(GL_NORMAL_ARRAY);
			  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
			  glNormalPointer(GL_FLOAT,0,normBuffer);
			  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
			  
			  glDrawArrays(GL_POINTS,0,(stacks+1)*slices);

			  glDisableClientState(GL_VERTEX_ARRAY);
			  glDisableClientState(GL_NORMAL_ARRAY);
			  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			  delete[] vertexBuffer;
			  delete[] txlBuffer;
			  delete[] normBuffer;
	      }
		  break;
	  case GLU_LINE:
	  case GLU_SILHOUETTE:
	  default:
		  break;
	}
}


//
// add two parameter:
// 
void MRVL_gluSpherePart(MRVL_GLUquadricObj *qobj, GLfloat radius, GLint slices, GLint stacks, GLfloat slicesPartial, GLfloat stacksPartial)
{

    GLint clipPart = (1.0 - stacksPartial) * stacks /2;
    if (stacksPartial > 1.0)
    {
        stacksPartial = 1.0;
    }

    if (stacksPartial < 0.0)
    {
        stacksPartial = 0.0;
    }

    if (slicesPartial > 1.0)
    {
        slicesPartial = 1.0;
    }

    if (slicesPartial < 0.0)
    {
        slicesPartial = 0.0;
    }

	GLint i,j;
	GLfloat sinCache1a[CACHE_SIZE];
	GLfloat cosCache1a[CACHE_SIZE];
	GLfloat sinCache2a[CACHE_SIZE];
	GLfloat cosCache2a[CACHE_SIZE];
	GLfloat sinCache3a[CACHE_SIZE];
	GLfloat cosCache3a[CACHE_SIZE];
	GLfloat sinCache1b[CACHE_SIZE];
	GLfloat cosCache1b[CACHE_SIZE];
	GLfloat sinCache2b[CACHE_SIZE];
	GLfloat cosCache2b[CACHE_SIZE];
	GLfloat sinCache3b[CACHE_SIZE];
	GLfloat cosCache3b[CACHE_SIZE];
	GLfloat angle;
	GLfloat zLow, zHigh;
	GLfloat sintemp1 = 0.0, sintemp2 = 0.0, sintemp3 = 0.0, sintemp4 = 0.0;
	GLfloat costemp1 = 0.0, costemp2 = 0.0, costemp3 = 0.0, costemp4 = 0.0;
	GLboolean needCache2, needCache3;
	GLint start, finish;

	if (slices >= CACHE_SIZE) slices = CACHE_SIZE-1;
	if (stacks >= CACHE_SIZE) stacks = CACHE_SIZE-1;
	if (slices < 2 || stacks < 1 || radius < 0.0) {
		return;
	}

	/* Cache is the vertex locations cache */
	/* Cache2 is the various normals at the vertices themselves */
	/* Cache3 is the various normals for the faces */
	needCache2 = needCache3 = GL_FALSE;

	if (qobj->normals == GLU_SMOOTH) {
		needCache2 = GL_TRUE;
	}

	if (qobj->normals == GLU_FLAT) {
		if (qobj->drawStyle != GLU_POINT) {
			needCache3 = GL_TRUE;
		}
		if (qobj->drawStyle == GLU_LINE) {
			needCache2 = GL_TRUE;
		}
	}

	for (i = 0; i < slices; i++) {
		angle = (GLfloat)(2 * PI * i) / slices;
        //
        angle *= slicesPartial;

		sinCache1a[i] = SIN(angle);
		cosCache1a[i] = COS(angle);
		if (needCache2) {
			sinCache2a[i] = sinCache1a[i];
			cosCache2a[i] = cosCache1a[i];
		}
	}

	for (j = 0; j <= stacks; j++) {
		angle = (GLfloat)(PI * j) / stacks;
        //
        //angle *= slicesPartial;

		if (needCache2) {
			if (qobj->orientation == GLU_OUTSIDE) {
				sinCache2b[j] = SIN(angle);
				cosCache2b[j] = COS(angle);
			} else {
				sinCache2b[j] = -SIN(angle);
				cosCache2b[j] = -COS(angle);
			}
		}
		sinCache1b[j] = radius * SIN(angle);
		cosCache1b[j] = radius * COS(angle);
	}
	/* Make sure it comes to a point */
	sinCache1b[0] = 0;
	sinCache1b[stacks] = 0;

	if (needCache3) {
		for (i = 0; i < slices; i++) {
			angle = (GLfloat)(2 * PI * (i-0.5f)) / slices;
			sinCache3a[i] = SIN(angle);
			cosCache3a[i] = COS(angle);
		}
		for (j = 0; j <= stacks; j++) {
			angle = (GLfloat)(PI * (j - 0.5f)) / stacks;
			if (qobj->orientation == GLU_OUTSIDE) {
				sinCache3b[j] = SIN(angle);
				cosCache3b[j] = COS(angle);
			} else {
				sinCache3b[j] = -SIN(angle);
				cosCache3b[j] = -COS(angle);
			}
		}
	}

	sinCache1a[slices] = sinCache1a[0];
	cosCache1a[slices] = cosCache1a[0];
	if (needCache2) {
		sinCache2a[slices] = sinCache2a[0];
		cosCache2a[slices] = cosCache2a[0];
	}
	if (needCache3) {
		sinCache3a[slices] = sinCache3a[0];
		cosCache3a[slices] = cosCache3a[0];
	}

	switch (qobj->drawStyle) {
	  case GLU_FILL:
		  if (!(qobj->textureCoords)) 
		  {
			  start = 1;
			  finish = stacks - 1;

			  /* Low end first (j == 0 iteration) */
			  sintemp2 = sinCache1b[1];
			  zHigh = cosCache1b[1];
			  switch(qobj->normals) 
			  {
				  case GLU_FLAT:
					  sintemp3 = sinCache3b[1];
					  costemp3 = cosCache3b[1];
					  break;
				  case GLU_SMOOTH:
					  sintemp3 = sinCache2b[1];
					  costemp3 = cosCache2b[1];
					  glNormal3f(sinCache2a[0] * sinCache2b[0],cosCache2a[0] * sinCache2b[0],cosCache2b[0]);
					  break;
				  default:
					  break;
			  }
			  GLfloat *vertexBuffer;
			  GLfloat *normBuffer;
			  vertexBuffer = new GLfloat[(slices+1+slices+2)*3];
			  normBuffer = new GLfloat[(slices+1+slices+2)*3];
			  int vert_i = 0;
			  int norm_i = 0;
			  glEnableClientState(GL_VERTEX_ARRAY);
			  glEnableClientState(GL_NORMAL_ARRAY);

			  if (qobj->orientation == GLU_OUTSIDE)
			  {
				  for (i=slices;i>=0;i--)
				  {
					  switch(qobj->normals) {
						  case GLU_SMOOTH:
							  normBuffer[norm_i++] = sinCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = cosCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = costemp3;
							  break;
						  case GLU_FLAT:
							  if (i != slices) 
							  {
								  normBuffer[norm_i++] = sinCache3a[i+1] * sintemp3;
								  normBuffer[norm_i++] = cosCache3a[i+1] * sintemp3;
								  normBuffer[norm_i++] = costemp3;
							  }
							  break;
						  case GLU_NONE:
						  default:
							  break;
					  }
					  vertexBuffer[vert_i++] = sintemp2 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp2 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zHigh;
				  }
			  }
			  else
			  {
				  for (i = 0; i <= slices; i++) {
					  switch(qobj->normals) 
					  {
						  case GLU_SMOOTH:
							  normBuffer[norm_i++] = sinCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = cosCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = costemp3;
							  break;
						  case GLU_FLAT:
							  normBuffer[norm_i++] = sinCache3a[i] * sintemp3;
							  normBuffer[norm_i++] = cosCache3a[i] * sintemp3;
							  normBuffer[norm_i++] = costemp3;
							  break;
						  case GLU_NONE:
						  default:
							  break;
					  }
					  vertexBuffer[vert_i++] = sintemp2 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp2 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zHigh;
				  }
			  }
			  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
			  glNormalPointer(GL_FLOAT,0,normBuffer);

			  glDrawArrays(GL_TRIANGLE_FAN,0,slices+1);


				
			  /* High end next (j == stacks-1 iteration) */
			  sintemp2 = sinCache1b[stacks-1];
			  zHigh = cosCache1b[stacks-1];
			  switch(qobj->normals) {
				  case GLU_FLAT:
					  sintemp3 = sinCache3b[stacks];
					  costemp3 = cosCache3b[stacks];
					  break;
				  case GLU_SMOOTH:
					  sintemp3 = sinCache2b[stacks-1];
					  costemp3 = cosCache2b[stacks-1];
					  glNormal3f(sinCache2a[stacks] * sinCache2b[stacks],cosCache2a[stacks] * sinCache2b[stacks],cosCache2b[stacks]);
					  break;
				  default:
					  break;
			  }

			  vertexBuffer[vert_i++] = 0.0f;
			  vertexBuffer[vert_i++] = 0.0f;
			  vertexBuffer[vert_i++] = -radius;
			  if(qobj->orientation == GLU_OUTSIDE)
			  {
				  for (i = 0; i <= slices; i++) {
					  switch(qobj->normals) {
						  case GLU_SMOOTH:
							  normBuffer[norm_i++] = sinCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = cosCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = costemp3;
							  break;
						  case GLU_FLAT:
							  normBuffer[norm_i++] = sinCache3a[i] * sintemp3;
							  normBuffer[norm_i++] = cosCache3a[i] * sintemp3;
							  normBuffer[norm_i++] = costemp3;
							  break;
						  case GLU_NONE:
						  default:
							  break;
					  }
					  vertexBuffer[vert_i++] = sintemp2 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp2 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zHigh;
				  }
			  }
			  else
			  {
				  for (i = slices; i >= 0; i--) 
				  {
					  switch(qobj->normals) {
						  case GLU_SMOOTH:
							  normBuffer[norm_i++] = sinCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = cosCache2a[i] * sintemp3;
							  normBuffer[norm_i++] = costemp3;
							  break;
						  case GLU_FLAT:
							  if (i != slices) {
								  normBuffer[norm_i++] = sinCache3a[i+1] * sintemp3;
								  normBuffer[norm_i++] = cosCache3a[i+1] * sintemp3;
								  normBuffer[norm_i++] = costemp3;
							  }
							  break;
						  case GLU_NONE:
						  default:
							  break;
					  }
					  vertexBuffer[vert_i++] = sintemp2 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp2 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zHigh;
				  }
			  }

			  glDrawArrays(GL_TRIANGLE_FAN,slices+1,slices+2);

			  glDisableClientState(GL_VERTEX_ARRAY);
			  glDisableClientState(GL_NORMAL_ARRAY);
			  delete[] vertexBuffer;
			  delete[] normBuffer;
	      } 
		  else 
		  {
			  start = 0 + clipPart;
			  finish = stacks - clipPart;
		  }
		  {
		  GLfloat *vertexBuffer;
		  GLfloat *normBuffer;
		  GLfloat *txlBuffer;
		  vertexBuffer = new GLfloat[(finish-start)*(slices+1)*2*3];
		  normBuffer = new GLfloat[(finish-start)*(slices+1)*2*3];
		  txlBuffer = new GLfloat[(finish-start)*(slices+1)*2*2];

		  int vert_i = 0;
		  int norm_i = 0;
		  int txl_i = 0;

		  for (j = start; j < finish; j++) 
		  {
			  zLow = cosCache1b[j];
			  zHigh = cosCache1b[j+1];
			  sintemp1 = sinCache1b[j];
			  sintemp2 = sinCache1b[j+1];
			  switch(qobj->normals) {
				  case GLU_FLAT:
					  sintemp4 = sinCache3b[j+1];
					  costemp4 = cosCache3b[j+1];
					  break;
				  case GLU_SMOOTH:
					  if (qobj->orientation == GLU_OUTSIDE) {
						  sintemp3 = sinCache2b[j+1];
						  costemp3 = cosCache2b[j+1];
						  sintemp4 = sinCache2b[j];
						  costemp4 = cosCache2b[j];
					  } else {
						  sintemp3 = sinCache2b[j];
						  costemp3 = cosCache2b[j];
						  sintemp4 = sinCache2b[j+1];
						  costemp4 = cosCache2b[j+1];
					  }
					  break;
				  default:
					  break;
			  }
			  for (i = 0; i <= slices; i++)
			  {
				  switch(qobj->normals) 
				  {
					  case GLU_SMOOTH:
						  normBuffer[norm_i++] = sinCache2a[i] * sintemp3;
						  normBuffer[norm_i++] = cosCache2a[i] * sintemp3;
						  normBuffer[norm_i++] = costemp3;
						  break;
					  case GLU_FLAT:
					  case GLU_NONE:
					  default:
						  break;
				  }
				  if (qobj->orientation == GLU_OUTSIDE) 
				  {
					  if (qobj->textureCoords) 
					  {
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = 1 - (float) (j+1 - clipPart) / (stacks - 2* clipPart);
					  }
					  vertexBuffer[vert_i++] = sintemp2 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp2 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zHigh;
				  } 
				  else 
				  {
					  if (qobj->textureCoords) 
					  {
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = 1 - (float) (j - clipPart) / (stacks - 2* clipPart);
					  }
					  vertexBuffer[vert_i++] = sintemp1 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp1 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zLow;
				  }
				  switch(qobj->normals) {
					  case GLU_SMOOTH:
						  normBuffer[norm_i++] = sinCache2a[i] * sintemp4;
						  normBuffer[norm_i++] = cosCache2a[i] * sintemp4;
						  normBuffer[norm_i++] = costemp4;
						  break;
					  case GLU_FLAT:
						  normBuffer[norm_i++] = sinCache3a[i] * sintemp4;
						  normBuffer[norm_i++] = cosCache3a[i] * sintemp4;
						  normBuffer[norm_i++] = costemp4;
						  break;
					  case GLU_NONE:
					  default:
						  break;
				  }
				  if (qobj->orientation == GLU_OUTSIDE) 
				  {
					  if (qobj->textureCoords) {
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = 1 - (float) (j - clipPart) / (stacks - 2* clipPart);
					  }
					  vertexBuffer[vert_i++] = sintemp1 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp1 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zLow;
				  } 
				  else 
				  {
					  if (qobj->textureCoords) {
						  txlBuffer[txl_i++] = 1 - (float) i / slices;
						  txlBuffer[txl_i++] = 1 - (float) (j+1 - clipPart) / (stacks - 2* clipPart);
					  }
					  vertexBuffer[vert_i++] = sintemp2 * sinCache1a[i];
					  vertexBuffer[vert_i++] = sintemp2 * cosCache1a[i];
					  vertexBuffer[vert_i++] = zHigh;
				  }
			  }
		  }
		  glEnableClientState(GL_VERTEX_ARRAY);
		  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
		  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
		  glEnableClientState(GL_NORMAL_ARRAY);
		  glNormalPointer(GL_FLOAT,0,normBuffer);
		  glDrawArrays(GL_TRIANGLE_STRIP,0,(finish-start)*(slices+1)*2);

		  glDisableClientState(GL_VERTEX_ARRAY);
		  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		  glDisableClientState(GL_NORMAL_ARRAY);

		  delete[] vertexBuffer;
		  delete[] txlBuffer;
		  delete[] normBuffer;
	  }
		  break;
	  case GLU_POINT:
		  {
			  GLfloat *vertexBuffer;
			  GLfloat *txlBuffer;
			  GLfloat *normBuffer;
			  int vert_i = 0;
			  int norm_i = 0;
			  int txl_i = 0;
			  vertexBuffer = new GLfloat[(stacks+1)*slices*3];
			  txlBuffer = new GLfloat[(stacks+1)*slices*2];
			  normBuffer = new GLfloat[(stacks+1)*slices*3];

			  for (j = 0; j <= stacks; j++) 
			  {
				  sintemp1 = sinCache1b[j];
				  costemp1 = cosCache1b[j];
				  switch(qobj->normals) {
					  case GLU_FLAT:
					  case GLU_SMOOTH:
						  sintemp2 = sinCache2b[j];
						  costemp2 = cosCache2b[j];
						  break;
					  default:
						  break;
				  }
				  for (i = 0; i < slices; i++) 
				  {
					  switch(qobj->normals) {
							  case GLU_FLAT:
							  case GLU_SMOOTH:
								  normBuffer[norm_i++] = sinCache2a[i] * sintemp2;
								  normBuffer[norm_i++] = cosCache2a[i] * sintemp2;
								  normBuffer[norm_i++] = costemp2;
								  break;
							  case GLU_NONE:
							  default:
								  break;
					  }
					  zLow = j * radius / stacks;
					  if (qobj->textureCoords) 
					  {
							  txlBuffer[txl_i++] = 1 - (float) i / slices;
							  txlBuffer[txl_i++] = 1 - (float) j / stacks;
					  }
						  vertexBuffer[vert_i++] = sintemp1 * sinCache1a[i];
						  vertexBuffer[vert_i++] = sintemp1 * cosCache1a[i];
						  vertexBuffer[vert_i++] = costemp1;

				  }
		      }
			  glEnableClientState(GL_VERTEX_ARRAY);
			  glEnableClientState(GL_NORMAL_ARRAY);
			  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			  glVertexPointer(3,GL_FLOAT,0,vertexBuffer);
			  glNormalPointer(GL_FLOAT,0,normBuffer);
			  glTexCoordPointer(2,GL_FLOAT,0,txlBuffer);
			  
			  glDrawArrays(GL_POINTS,0,(stacks+1)*slices);

			  glDisableClientState(GL_VERTEX_ARRAY);
			  glDisableClientState(GL_NORMAL_ARRAY);
			  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			  delete[] vertexBuffer;
			  delete[] txlBuffer;
			  delete[] normBuffer;
	      }
		  break;
	  case GLU_LINE:
	  case GLU_SILHOUETTE:
	  default:
		  break;
	}
}

static int MRVL_gluBuild2DMipmapLevelsCore(GLenum target, GLint internalFormat,
									  GLsizei width, GLsizei height,
									  GLsizei widthPowerOf2,
									  GLsizei heightPowerOf2,
									  GLenum format, GLenum type,
									  GLint userLevel,
									  GLint baseLevel,GLint maxLevel,
									  const void *data)
{
	GLint newwidth, newheight;
	GLint level, levels;
	const void *usersImage; /* passed from user. Don't touch! */
	void *srcImage, *dstImage; /* scratch area to build mipmapped images */
	__GLU_INIT_SWAP_IMAGE;
	GLint memreq;
	GLint cmpts;

	GLint myswap_bytes, groups_per_line, element_size, group_size;
	GLint rowsize, padding;
	PixelStorageModes psm;

	srcImage = dstImage = NULL;

	newwidth= widthPowerOf2;
	newheight= heightPowerOf2;
	levels = computeLog(newwidth);
	level = computeLog(newheight);
	if (level > levels) levels=level;

	levels+= userLevel;

	retrieveStoreModes(&psm);
	cmpts = elements_per_group(format,type);
	groups_per_line = width;            

	element_size = bytes_per_element(type);
	group_size = element_size * cmpts;
	if (element_size == 1) myswap_bytes = 0;

	rowsize = groups_per_line * group_size;
	padding = (rowsize % psm.unpack_alignment);
	if (padding) {
		rowsize += psm.unpack_alignment - padding;
	}
	usersImage = (const GLubyte *) data;         

	level = userLevel;

	/* already power-of-two square */
	if (width == newwidth && height == newheight) 
	{
		/* Use usersImage for level userLevel */
		if (baseLevel <= level && level <= maxLevel) {
			glTexImage2D(target, level, internalFormat, width, height, 0, format, type, usersImage);
		}
		if(levels == 0) { /* we're done. clean up and return */
			glPixelStorei(GL_UNPACK_ALIGNMENT, psm.unpack_alignment);
			return 0;
		}
		{
			int nextWidth= newwidth/2;
			int nextHeight= newheight/2;

			/* clamp to 1 */
			if (nextWidth < 1) nextWidth= 1;
			if (nextHeight < 1) nextHeight= 1;
			memreq = image_size(nextWidth, nextHeight, format, type);
		}

		switch(type) {
	case GL_UNSIGNED_BYTE:
		dstImage = (GLubyte *)malloc(memreq);
		break;
	case GL_BYTE:
		dstImage = (GLbyte *)malloc(memreq);
		break;
	case GL_UNSIGNED_SHORT:
		dstImage = (GLushort *)malloc(memreq);
		break;
	case GL_SHORT:
		dstImage = (GLshort *)malloc(memreq);
		break;
	case GL_FIXED:
		dstImage = (GLfixed *)malloc(memreq);
		break;
	case GL_FLOAT:
		dstImage = (GLfloat *)malloc(memreq);
		break;
	default:
		return GLU_INVALID_ENUM;
		}
		if (dstImage == NULL) {
			glPixelStorei(GL_UNPACK_ALIGNMENT, psm.unpack_alignment);
			return GLU_OUT_OF_MEMORY;
		}
		else
			switch(type) {
	  case GL_UNSIGNED_BYTE:
		  halveImage_ubyte(cmpts, width, height,
			  (const GLubyte *)usersImage, (GLubyte *)dstImage,
			  element_size, rowsize, group_size);
		  break;
	  case GL_BYTE:
		  halveImage_byte(cmpts, width, height,
			  (const GLbyte *)usersImage, (GLbyte *)dstImage,
			  element_size, rowsize, group_size);
		  break;
	  case GL_UNSIGNED_SHORT:
		  halveImage_ushort(cmpts, width, height,
			  (const GLushort *)usersImage, (GLushort *)dstImage,
			  element_size, rowsize, group_size, myswap_bytes);
		  break;
	  case GL_SHORT:
		  halveImage_short(cmpts, width, height,
			  (const GLshort *)usersImage, (GLshort *)dstImage,
			  element_size, rowsize, group_size, myswap_bytes);
		  break;
	  case GL_FIXED:
		  halveImage_fixed(cmpts, width, height,
			  (const GLfixed *)usersImage, (GLfixed *)dstImage,
			  element_size, rowsize, group_size, myswap_bytes);
		  break;
	  case GL_FLOAT:
		  halveImage_float(cmpts, width, height,
			  (const GLfloat *)usersImage, (GLfloat *)dstImage,
			  element_size, rowsize, group_size, myswap_bytes);
		  break;
	  default:
		  break;
		}
		newwidth = width/2;
		newheight = height/2;
		/* clamp to 1 */
		if (newwidth < 1) newwidth= 1;
		if (newheight < 1) newheight= 1;

		myswap_bytes = 0;
		rowsize = newwidth * group_size;
		memreq = image_size(newwidth, newheight, format, type);
		/* Swap srcImage and dstImage */
		__GLU_SWAP_IMAGE(srcImage,dstImage);
		switch(type) {
	case GL_UNSIGNED_BYTE:
		dstImage = (GLubyte *)malloc(memreq);
		break;
	case GL_BYTE:
		dstImage = (GLbyte *)malloc(memreq);
		break;
	case GL_UNSIGNED_SHORT:
		dstImage = (GLushort *)malloc(memreq);
		break;
	case GL_SHORT:
		dstImage = (GLshort *)malloc(memreq);
		break;
	case GL_FIXED:
		dstImage = (GLfixed *)malloc(memreq);
		break;
	case GL_FLOAT:
		dstImage = (GLfloat *)malloc(memreq);
		break;
	default:
		return GLU_INVALID_ENUM;
		}
		if (dstImage == NULL) {
			glPixelStorei(GL_UNPACK_ALIGNMENT, psm.unpack_alignment);
			return GLU_OUT_OF_MEMORY;
		}
		/* level userLevel+1 is in srcImage; level userLevel already saved */
		level = userLevel+1;
	} 
	else 
	{ /* user's image is *not* nice power-of-2 sized square */
		memreq = image_size(newwidth, newheight, format, type);
		switch(type) {
		case GL_UNSIGNED_BYTE:
			dstImage = (GLubyte *)malloc(memreq);
			break;
		case GL_BYTE:
			dstImage = (GLbyte *)malloc(memreq);
			break;
		case GL_UNSIGNED_SHORT:
			dstImage = (GLushort *)malloc(memreq);
			break;
		case GL_SHORT:
			dstImage = (GLshort *)malloc(memreq);
			break;
		case GL_FIXED:
			dstImage = (GLfixed *)malloc(memreq);
			break;
		case GL_FLOAT:
			dstImage = (GLfloat *)malloc(memreq);
			break;
		default:
			return GLU_INVALID_ENUM;
		}

		if (dstImage == NULL) {
			glPixelStorei(GL_UNPACK_ALIGNMENT, psm.unpack_alignment);
			return GLU_OUT_OF_MEMORY;
		}

		switch(type) {
	case GL_UNSIGNED_BYTE:
		scale_internal_ubyte(cmpts, width, height,
			(const GLubyte *)usersImage, newwidth, newheight,
			(GLubyte *)dstImage, element_size,
			rowsize, group_size);
		break;
	case GL_BYTE:
		scale_internal_byte(cmpts, width, height,
			(const GLbyte *)usersImage, newwidth, newheight,
			(GLbyte *)dstImage, element_size,
			rowsize, group_size);
		break;
	case GL_UNSIGNED_SHORT:
		scale_internal_ushort(cmpts, width, height,
			(const GLushort *)usersImage, newwidth, newheight,
			(GLushort *)dstImage, element_size,
			rowsize, group_size, myswap_bytes);
		break;
	case GL_SHORT:
		scale_internal_short(cmpts, width, height,
			(const GLshort *)usersImage, newwidth, newheight,
			(GLshort *)dstImage, element_size,
			rowsize, group_size, myswap_bytes);
		break;
	case GL_FIXED:
		scale_internal_fixed(cmpts, width, height,
			(const GLfixed *)usersImage, newwidth, newheight,
			(GLfixed *)dstImage, element_size,
			rowsize, group_size, myswap_bytes);
		break;
	case GL_FLOAT:
		scale_internal_float(cmpts, width, height,
			(const GLfloat *)usersImage, newwidth, newheight,
			(GLfloat *)dstImage, element_size,
			rowsize, group_size, myswap_bytes);
		break;
	default:
		break;
		}
		myswap_bytes = 0;
		rowsize = newwidth * group_size;
		/* Swap dstImage and srcImage */
		__GLU_SWAP_IMAGE(srcImage,dstImage);

		if(levels != 0) { /* use as little memory as possible */
			{
				int nextWidth= newwidth/2;
				int nextHeight= newheight/2;
				if (nextWidth < 1) nextWidth= 1;
				if (nextHeight < 1) nextHeight= 1; 

				memreq = image_size(nextWidth, nextHeight, format, type);
			}

			switch(type) {
		case GL_UNSIGNED_BYTE:
			dstImage = (GLubyte *)malloc(memreq);
			break;
		case GL_BYTE:
			dstImage = (GLbyte *)malloc(memreq);
			break;
		case GL_UNSIGNED_SHORT:
			dstImage = (GLushort *)malloc(memreq);
			break;
		case GL_SHORT:
			dstImage = (GLshort *)malloc(memreq);
			break;
		case GL_FIXED:
			dstImage = (GLfixed *)malloc(memreq);
			break;
		case GL_FLOAT:
			dstImage = (GLfloat *)malloc(memreq);
			break;
		default:
			return GLU_INVALID_ENUM;
			}
			if (dstImage == NULL) {
				glPixelStorei(GL_UNPACK_ALIGNMENT, psm.unpack_alignment);
				return GLU_OUT_OF_MEMORY;
			}
		}
		/* level userLevel is in srcImage; nothing saved yet */
		level = userLevel;
	}

//	glPixelStorei(GL_UNPACK_SWAP_BYTES, GL_FALSE);
	if (baseLevel <= level && level <= maxLevel) {
		glTexImage2D(target, level, internalFormat, newwidth, newheight, 0,
			format, type, (void *)srcImage);
	}

	level++; /* update current level for the loop */
	for (; level <= levels; level++) {
		switch(type) {
		case GL_UNSIGNED_BYTE:
			halveImage_ubyte(cmpts, newwidth, newheight,
				(GLubyte *)srcImage, (GLubyte *)dstImage, element_size,
				rowsize, group_size);
			break;
		case GL_BYTE:
			halveImage_byte(cmpts, newwidth, newheight,
				(GLbyte *)srcImage, (GLbyte *)dstImage, element_size,
				rowsize, group_size);
			break;
		case GL_UNSIGNED_SHORT:
			halveImage_ushort(cmpts, newwidth, newheight,
				(GLushort *)srcImage, (GLushort *)dstImage, element_size,
				rowsize, group_size, myswap_bytes);
			break;
		case GL_SHORT:
			halveImage_short(cmpts, newwidth, newheight,
				(GLshort *)srcImage, (GLshort *)dstImage, element_size,
				rowsize, group_size, myswap_bytes);
			break;
		case GL_FIXED:
			halveImage_fixed(cmpts, newwidth, newheight,
				(GLfixed *)srcImage, (GLfixed *)dstImage, element_size,
				rowsize, group_size, myswap_bytes);
			break;
		case GL_FLOAT:
			halveImage_float(cmpts, newwidth, newheight,
				(GLfloat *)srcImage, (GLfloat *)dstImage, element_size,
				rowsize, group_size, myswap_bytes);
			break;
		default:
			break;
		}

		__GLU_SWAP_IMAGE(srcImage,dstImage);

		if (newwidth > 1) { newwidth /= 2; rowsize /= 2;}
		if (newheight > 1) newheight /= 2;
		{
			/* compute amount to pad per row, if any */
			int rowPad= rowsize % psm.unpack_alignment;

			/* should row be padded? */
			if (rowPad == 0) {	/* nope, row should not be padded */
				/* call tex image with srcImage untouched since it's not padded */
				if (baseLevel <= level && level <= maxLevel) {
					glTexImage2D(target, level, internalFormat, newwidth, newheight, 0,
						format, type, (void *) srcImage);
				}
			}
			else {			/* yes, row should be padded */
				/* compute length of new row in bytes, including padding */
				int newRowLength= rowsize + psm.unpack_alignment - rowPad;
				int ii; unsigned char *dstTrav, *srcTrav; /* indices for copying */

				/* allocate new image for mipmap of size newRowLength x newheight */
				void *newMipmapImage= malloc((size_t) (newRowLength*newheight));
				if (newMipmapImage == NULL) {
					/* out of memory so return */
					glPixelStorei(GL_UNPACK_ALIGNMENT, psm.unpack_alignment);
					return GLU_OUT_OF_MEMORY;
				}

				/* copy image from srcImage into newMipmapImage by rows */
				for (ii= 0,
					dstTrav= (unsigned char *) newMipmapImage,
					srcTrav= (unsigned char *) srcImage;
				ii< newheight;
				ii++,
					dstTrav+= newRowLength, /* make sure the correct distance... */
					srcTrav+= rowsize) {    /* ...is skipped */
						memcpy(dstTrav,srcTrav,rowsize);
						/* note that the pad bytes are not visited and will contain
						* garbage, which is ok.
						*/
				}

				/* ...and use this new image for mipmapping instead */
				if (baseLevel <= level && level <= maxLevel) {
					glTexImage2D(target, level, internalFormat, newwidth, newheight, 0,
						format, type, newMipmapImage);
				}
				free(newMipmapImage); /* don't forget to free it! */
			} /* else */
		}
	} /* for level */
	glPixelStorei(GL_UNPACK_ALIGNMENT, psm.unpack_alignment);

	free(srcImage); /*if you get to here, a srcImage has always been malloc'ed*/
	if (dstImage) { /* if it's non-rectangular and only 1 level */
		free(dstImage);
	}
	return 0;
} 

GLint MRVL_gluBuild2DMipmaps(GLenum target, GLint internalFormat,GLsizei width, GLsizei height,GLenum format, GLenum type,const void *data)
{
//	GLint widthPowerOf2, heightPowerOf2;
	int level, levels;
	GLint newWidth;
	GLint newHeight;
	GLint maxsize;

	if (width < 1 || height < 1) {
		return GLU_INVALID_VALUE;
	}
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxsize);
	/* clamp user's texture sizes to maximum sizes, if necessary */
	newWidth = nearestPower(width);
	if (newWidth > maxsize) newWidth = maxsize;
	newHeight = nearestPower(height);
	if (newHeight > maxsize) newHeight = maxsize;

	levels = computeLog(newWidth);
	level = computeLog(newHeight);
	if (level > levels) levels=level;

	return MRVL_gluBuild2DMipmapLevelsCore(target,internalFormat,
		width, height,
		newWidth,newHeight,
		format,type,
		0,0,levels,data);
}

/*-----------------------------------------  GLUT Lib -----------------------------------------------*/
void MRVL_glutSolidTorus(GLfloat innerRadius, GLfloat outerRadius,GLint nsides, GLint rings)
{
	doughnut(innerRadius, outerRadius, nsides, rings);
}

void MRVL_glutSolidCone(GLfloat base, GLfloat height, GLint slices, GLint stacks)
{
	QUAD_OBJ_INIT();
	MRVL_gluQuadricDrawStyle(quadObj, GLU_FILL);
	MRVL_gluQuadricNormals(quadObj, GLU_SMOOTH);
	/* If we ever changed/used the texture or orientation state
	of quadObj, we'd need to change it to the defaults here
	with gluQuadricTexture and/or gluQuadricOrientation. */
	MRVL_gluCylinder(quadObj, base, 0.0, height, slices, stacks);
}

void MRVL_glutSolidCube(GLfloat size)
{
	drawBox(size, GL_TRIANGLE_FAN);
}

void MRVL_glutSolidSphere(GLfloat radius, GLint slices, GLint stacks)
{
	QUAD_OBJ_INIT();
	MRVL_gluQuadricDrawStyle(quadObj, GLU_FILL);
	MRVL_gluQuadricNormals(quadObj, GLU_SMOOTH);
	/* If we ever changed/used the texture or orientation state
	of quadObj, we'd need to change it to the defaults here
	with gluQuadricTexture and/or gluQuadricOrientation. */
	MRVL_gluSphere(quadObj, radius, slices, stacks);
}

void MRVL_glutSolidDodecahedron(void)
{
	dodecahedron(GL_TRIANGLE_FAN);
}
