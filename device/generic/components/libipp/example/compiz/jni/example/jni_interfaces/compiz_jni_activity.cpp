/******************************************************************************
// (C) Copyright 2009 Marvell International Ltd.
// All Rights Reserved
******************************************************************************/

#include "misc.h"
#include "compiz_jni_activity.h"
#include "compiz_jni_lib.h"

#include <nativehelper/jni.h>
#include "JNIHelp.h"
#include "jni.h"
#include "utils/Errors.h"
#include "android_runtime/AndroidRuntime.h"
#include "vdec_wrapper.h"
#include "demo.h"

//#define JNI_ACT_DBG
#ifdef JNI_ACT_DBG
#define JNI_ACT_PRINTF(...) IPP_Printf(__VA_ARGS__)
#else
#define JNI_ACT_PRINTF(...) 
#endif

using namespace android;

/* Local Globals */
jclass      mClass;
jobject     mObject;
jmethodID   mKillProg;
jmethodID   mNormalExit;

void NormalExitCompiz(const char *str)
{
    JNIEnv *env = AndroidRuntime::getJNIEnv();
    jstring info = env->NewStringUTF(str);

    env->CallStaticVoidMethod(mClass, mNormalExit, mObject, info);
    env->ReleaseStringUTFChars(info, env->GetStringUTFChars(info, false));
}


void KillCompiz(const char *str)
{
    JNIEnv *env = AndroidRuntime::getJNIEnv();
    jstring info = env->NewStringUTF(str);

    env->CallStaticVoidMethod(mClass, mKillProg, mObject, info);
    env->ReleaseStringUTFChars(info, env->GetStringUTFChars(info, false));
}

static void com_android_compiz_COMPIZActivity_native_init(JNIEnv *env, 
                                                          jobject thiz)
{
    jclass clazz;
    JNI_ACT_PRINTF("Entering: native_init\n");

    clazz = env->FindClass("com/android/compiz/COMPIZActivity");
    if (NULL == clazz) {
        jniThrowException(env, "java/lang/RuntimeException", "Can't find com/android/compiz/COMPIZActivity");
        IPP_Printf("Exit: native_init with find class failed!\n");
        return ;
    }

    mKillProg = env->GetStaticMethodID(clazz, "KillProgCallback",
        "(Ljava/lang/Object;Ljava/lang/String;)V");
    if (NULL == mKillProg) {
        jniThrowException(env, "java/lang/RuntimeException", "Can't find COMPIZActivity.KillProgCallback");
        IPP_Printf("Exit: native_init with find method failed!\n");
        return ;
    }

    mNormalExit = env->GetStaticMethodID(clazz, "NormalExitCallback",
        "(Ljava/lang/Object;Ljava/lang/String;)V");
    if (NULL == mNormalExit) {
        jniThrowException(env, "java/lang/RuntimeException", "Can't find COMPIZActivity.NormalExitCallBack");
        IPP_Printf("Exit: native_init with find method failed!\n");
        return ;
    }

    JNI_ACT_PRINTF("Exit: native_init\n");

    return ;
}

static void com_android_compiz_COMPIZActivity_native_setup(JNIEnv *env, 
                                                           jobject thiz, 
                                                           jobject weak_this)
{
    jclass clazz;

    JNI_ACT_PRINTF("Entering native_setup\n");

    clazz = env->GetObjectClass(thiz);
    if (NULL == clazz) {
        IPP_Printf("Can't find com/android/compiz/COMPIZActivity\n");
        jniThrowException(env, "java/lang/Exception", NULL);
        return ;
    }
    mClass = (jclass)env->NewGlobalRef(clazz);
    mObject= env->NewGlobalRef(weak_this);

    JNI_ACT_PRINTF("Exit native_setup\n");
}

static void com_android_compiz_COMPIZActivity_native_deinit(JNIEnv *env, jobject thiz)
{
    JNI_ACT_PRINTF("Entering: native_deinit\n");

    env->DeleteGlobalRef(mObject);
    env->DeleteGlobalRef(mClass);

    JNI_ACT_PRINTF("Exit: native_deinit\n");
}

static JNINativeMethod lMethods[] =  {
    {"pause",    "()V",      (void*)Java_com_android_compiz_COMPIZLib_pause},
    {"resume",   "()V",      (void*)Java_com_android_compiz_COMPIZLib_resume},
    {"init",     "()V",      (void*)Java_com_android_compiz_COMPIZLib_init},
    {"deinit",   "()V",      (void*)Java_com_android_compiz_COMPIZLib_deinit},
    {"step",     "()V",      (void*)Java_com_android_compiz_COMPIZLib_step},
    {"change",   "(II)V",    (void*)Java_com_android_compiz_COMPIZLib_change},
    {"keyPressed","(I)V",    (void*)Java_com_android_compiz_COMPIZLib_keyPressed},
};

static int register_com_android_compiz_lib(JNIEnv *env)
{
    int rtCode; 

    JNI_ACT_PRINTF("Entering: register_com_android_compiz_lib\n");
    rtCode = AndroidRuntime::registerNativeMethods(env, "com/android/compiz/COMPIZLib",
        lMethods, NELEM(lMethods));
    JNI_ACT_PRINTF("Exit: register_com_android_compiz_lib\n");

    return rtCode;
}

static JNINativeMethod gMethods[] = {
    {"native_init",     "()V",                      (void*)com_android_compiz_COMPIZActivity_native_init},    
    {"native_setup",    "(Ljava/lang/Object;)V",    (void*)com_android_compiz_COMPIZActivity_native_setup},
    {"native_deinit",   "()V",                      (void*)com_android_compiz_COMPIZActivity_native_deinit},
};

static int register_com_android_compiz_activity(JNIEnv *env)
{
    int rtCode; 

    JNI_ACT_PRINTF("Entering: register_com_android_compiz_activity\n");
    rtCode = AndroidRuntime::registerNativeMethods(env, "com/android/compiz/COMPIZActivity",
        gMethods, NELEM(gMethods));
    JNI_ACT_PRINTF("Exit: register_com_android_compiz_activity\n");

    return rtCode;
}

jint JNI_OnLoad(JavaVM *vm, void* reserved)
{
    JNIEnv *env = NULL;
    jint result = -1;

    JNI_ACT_PRINTF("Entering: JNI_OnLoad\n");

    if (vm->GetEnv((void**)&env, JNI_VERSION_1_4) != JNI_OK) {
        IPP_Printf("Error: GetEnv Failed!\n");
        return result;
    }

    if (register_com_android_compiz_activity(env) < 0) {
        IPP_Printf("Error: Compiz native activity registeration failed!\n");
        return result;
    }

    if (register_com_android_compiz_lib(env) < 0) {
        IPP_Printf("Error: Compiz native lib registeration failed!\n");
        return result;
    }

    result = JNI_VERSION_1_4;

    JNI_ACT_PRINTF("Exit: JNI_OnLoad\n");

    return result;
}

/* EOF */
