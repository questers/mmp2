/***************************************************************************************** 
Copyright (c) 2009, Marvell International Ltd. 
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Marvell nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MARVELL ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MARVELL BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************/


#include <string.h>
#include <stdlib.h>
#include "manchac.h"

#define MAN_PAR_TOKENSIZE       2048
#define MANCHAC_HEADER          "!!!Manchac!!!"

#define MAN_PAR_OK         0
#define MAN_PAR_PARAMS     1
#define MAN_PAR_NOOBJ      2
#define MAN_PAR_MEMORY     3
#define MAN_PAR_EOF        4
#define MAN_PAR_FORMAT     5

#define ISSPACE(ch)     ((unsigned int)((ch) - 9) < 5u  ||  (ch) == ' ')

#ifdef _IPP_WCE
extern char*                    gCurrDir;               /* current directory */
#endif

static int parSkipSpace(IPP_FILE *m_fp)
{
        int ch;
        do {
                if(IPP_Feof(m_fp)) {
                        return MAN_PAR_EOF;
                }
                ch = IPP_Fgetc(m_fp);
        } while(ISSPACE(ch));

        IPP_Fungetc(ch, m_fp);
        return MAN_PAR_OK;
}
                
static int parSkipComment(IPP_FILE *m_fp)
{
        int ch;
        do {
                if(IPP_Feof(m_fp)) {
                        return MAN_PAR_EOF;
                }
                ch = IPP_Fgetc(m_fp);

                if(ch!='/' && ch!='*')  {
                        IPP_Fungetc(ch, m_fp);
                        return MAN_PAR_NOOBJ;
                }

                if(ch=='/') {
                        do {
                                if(IPP_Feof(m_fp)) {
                                        return MAN_PAR_EOF;
                                }
                                ch = IPP_Fgetc(m_fp);
                        } while(ch!='\n');
                }

                else if(ch=='*')
                {
                        int iState = 0;
                        do {
                                if(IPP_Feof(m_fp)) {
                                        return MAN_PAR_EOF;
                                }
                                ch = IPP_Fgetc(m_fp);
                                if(iState==0 && ch=='*') {
                                        iState = 1;
                                }
                                else if(iState==1)
                                {
                                        if(ch=='/') {
                                                iState = 2;
                                        } else if(ch!='*') {
                                                iState = 0;
                                        }
                                }
                        } while(iState!=2);
                }
                if(parSkipSpace(m_fp)==MAN_PAR_EOF) {
                        return MAN_PAR_EOF;
                }
                if(IPP_Feof(m_fp)) {
                        return MAN_PAR_EOF;
                }

                ch = IPP_Fgetc(m_fp);

        } while(ch=='/');

        IPP_Fungetc(ch, m_fp);
        return MAN_PAR_OK;
}

static int parGetToken(IPP_FILE *m_fp, char *pchBuf)
{
        int ch;
        int i=1,j=0;
        parSkipSpace(m_fp);
        if(IPP_Feof(m_fp)) {
                return MAN_PAR_EOF;
        }
        ch = IPP_Fgetc(m_fp);

        if(ch=='/')  {
                parSkipComment(m_fp);
                if(IPP_Feof(m_fp)) {
                        return MAN_PAR_EOF;
                }
                ch = IPP_Fgetc(m_fp);
        }

        if(ch=='=') {
                pchBuf[0] = (char)ch;
                pchBuf[1] = '\0';
                return MAN_PAR_OK;
        }

        pchBuf[0] = (char)ch;
        if(ch=='\"')    {
                ch = IPP_Fgetc(m_fp);
                while (ch != '\"'){
                        pchBuf[j] = (char)ch;
                        ch = IPP_Fgetc(m_fp);
                        j++;
                }
                pchBuf[j] = '\0';
                return MAN_PAR_OK;
        } else {
                for(i=1;i<MAN_PAR_TOKENSIZE - 1;i++)    {
                        if(IPP_Feof(m_fp)) {
                                pchBuf[i] = '\0';
                                return MAN_PAR_OK;
                        }
                        ch = IPP_Fgetc(m_fp);
                        if(ISSPACE(ch)||ch == '=') {
                                pchBuf[i] = '\0';
                                IPP_Fungetc(ch, m_fp);
                                return MAN_PAR_OK;
                        }
                        if(ch=='/' && parSkipComment(m_fp)!=MAN_PAR_NOOBJ) {
                                pchBuf[i] = '\0';
                                return MAN_PAR_OK;
                        }
                        pchBuf[i] = (char)ch;
                }
        }
        pchBuf[i] = '\0';
        return MAN_PAR_OK;
}

/* always return valid mode */
static int findPerfCounterMode(int cntID, const char *desp)
{
        int i;

        for (i=0;i<modeDesp_size;i++) {
                if(!IPP_Strcmp(desp, modeDesp[i].desp)) {
#if (defined PLATFORM_TTC)
                        if(cntID == modeDesp[i].counterID)
#endif
                                /* match */
                                return modeDesp[i].mode;
#if (defined PLATFORM_TTC)
                        if(modeDesp[i].counterID > cntID)       break;
#endif
                }
        }
        /* not found, set to default */
        MANPrintf(stdout, "Invalid performance counter setting, set to default %s\n", modeDesp[0].desp);
        return modeDesp[0].mode;
}


IppCodecStatus MANGetPar(MANMultiConfig **mCfg, IPP_FILE *pfPara)
{
        char    pchBuf[MAN_PAR_TOKENSIZE], pchName[MAN_PAR_TOKENSIZE];
        char    chBuf[80];
        char    *pch;
        int     streamBlock = 0;
        int     er = MAN_PAR_OK;        
        MANMultiConfig *pCfg = NULL;

        pch = IPP_Fgets(chBuf, IPP_Strlen(MANCHAC_HEADER)+1, pfPara);
        if( IPP_Strcmp(MANCHAC_HEADER, chBuf) ){
                IPP_Fprintf(stdout, "Not a valid parameter file!\n");   
                er = MAN_PAR_FORMAT;
                return IPP_STATUS_INPUT_ERR;
        }

        while (!(parGetToken(pfPara, pchBuf)==MAN_PAR_EOF)){
                if(IPP_Strcmp(pchBuf, "BEGIN") == 0) {
                        /* start of a stream case */
                        if(!pCfg) {
                                IPP_MemMalloc((void**)&pCfg, sizeof(MANMultiConfig), 4);
                                IPP_Memset(pCfg, 0, sizeof(MANMultiConfig));
                                *mCfg = pCfg;
                        } else {
                                IPP_MemMalloc((void**)(&(pCfg->next)), sizeof(MANMultiConfig), 4);
                                IPP_Memset(pCfg->next, 0, sizeof(MANMultiConfig));
                                pCfg = pCfg->next;
                        }
                        streamBlock = 1;
                        continue;
                
                } else if(IPP_Strcmp(pchBuf, "END") == 0) {
                        /* end of a stream case */
                        streamBlock = 0;
                }

                if(!streamBlock) {
                        continue;
                }

                IPP_Strcpy(pchName, pchBuf);

                if(MAN_PAR_EOF == parGetToken(pfPara, pchBuf)) {
                        er = MAN_PAR_FORMAT;
                }
                if(pchBuf[0] != '=') {
                        er = MAN_PAR_FORMAT;
                }
                if(MAN_PAR_EOF == parGetToken(pfPara, pchBuf)) {
                        er = MAN_PAR_FORMAT;
                }
                /* parse real contents */
                /* command line passed to codec */
                if (!IPP_Strcmp(pchName, "Input.Cmdline")) {
                        IPP_Strcpy(pCfg->cmdLine, pchBuf);
                }

                /* display control */
                else if (!IPP_Strcmp(pchName, "Output.DisplayEnable")) {
                        pCfg->UnionOutputConfig.displayConfig.bDisplayEnable = IPP_Atoi(pchBuf);
                } 
                else if (!IPP_Strcmp(pchName, "Output.FrameRate")) {
                        pCfg->UnionOutputConfig.displayConfig.nFrameRate = IPP_Atoi(pchBuf);
                } 
                /* audio render control */
                else if (!IPP_Strcmp(pchName, "Output.WaveOutEnable")) {
                        pCfg->UnionOutputConfig.waveoutConfig.bWaveoutEnable = IPP_Atoi(pchBuf);
                }
                /* PMU */
                else if(!IPP_Strcmp(pchName, "PerfCounter.Enable")) {
                        pCfg->cntConfig.bCntEnable = IPP_Atoi(pchBuf);
                } else if (!IPP_Strcmp(pchName, "PerfCounter.event0")) {
                        pCfg->cntConfig.nCntMode[0] = findPerfCounterMode(0, pchBuf);
                } else if (!IPP_Strcmp(pchName, "PerfCounter.event1")) {
                        pCfg->cntConfig.nCntMode[1] = findPerfCounterMode(1, pchBuf);
                }
# if( (defined PLATFORM_TTC) || (defined PLATFORM_PJ5))
                else if (!IPP_Strcmp(pchName, "PerfCounter.event2")) {
                        pCfg->cntConfig.nCntMode[2] = findPerfCounterMode(2, pchBuf);
                } else if (!IPP_Strcmp(pchName, "PerfCounter.event3")) {
                        pCfg->cntConfig.nCntMode[3] = findPerfCounterMode(3, pchBuf);
                }
#endif /* PLATFORM_TTC || PLATFORM_PJ5) */
                else {
                        er = MAN_PAR_FORMAT;
                }
        }

        if(er!=MAN_PAR_OK) {
                IPP_Fprintf(stdout, "error %d at some line of parameter file\n", er);
                return IPP_STATUS_INPUT_ERR;
        }

        return IPP_STATUS_NOERR;
}

void MANCleanupPar(MANMultiConfig *mCfg, IPP_FILE *pfPara)
{
        MANMultiConfig *pCfg;

        if(pfPara) {
                IPP_Fclose(pfPara);
        }

        while(mCfg) {
                pCfg = mCfg;
                mCfg = mCfg->next;
                IPP_MemFree((void**)(&pCfg));
        }
}


/* EOF */


