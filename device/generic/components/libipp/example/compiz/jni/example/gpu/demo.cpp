/******************************************************************************
*(C) Copyright 2007 Marvell International Ltd.
* All Rights Reserved 
******************************************************************************/
#ifndef __LINUX__
#include <windows.h>
#else
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#endif
#include "demo.h"
#if ANDROID && USE_APK
#include "compiz_jni_activity.h"
#endif
#include "misc.h"

#define ENABLE_GPU_RENDER 1

//#define DEMO_DBG
#ifdef DEMO_DBG
#define DEMO_PRINTF(...) IPP_Printf(__VA_ARGS__)
#else
#define DEMO_PRINTF(...)
#endif

#ifdef THREAD_DEBUG
#define PRINTF_THREAD(...)          IPP_Printf(__VA_ARGS__);
#define StartCounter(counter_index) IPP_StartPerfCounter(counter_index)
#define StopCounter(counter_index)  IPP_StopPerfCounter(counter_index)
#define GetPerfCounter(...)         IPP_GetPerfCounter(__VA_ARGS__)
#define ResetPerfCounter(...)       IPP_ResetPerfCounter(__VA_ARGS__)
#define FreePerfCounter(...)        IPP_FreePerfCounter(__VA_ARGS__)
#else
#define PRINTF_THREAD(...)
#define StartCounter(counter_index) 
#define StopCounter(counter_index)  
#define GetPerfCounter(...)
#define ResetPerfCounter(...)
#define FreePerfCounter(...)
#endif

#if USE_VDK
#include <vdk.h>
static vdkEGL egl;
#endif

#include <EGL/egl.h>
#include <GLES/gl.h>
#include "glu_glutlib.h"
#include "utils.h"
#include <dlfcn.h>
#include "manchac.h"

typedef GLvoid* (*GLCreateFenceFunc)();
typedef GLvoid  (*GLDestroyFenceFunc)(GLvoid *);
typedef GLvoid  (*GLSendFenceFunc)(GLvoid *);
typedef GLvoid  (*GLWaitFenceFunc)(GLvoid *);
typedef GLvoid  (*GLTexMapFunc)(GLenum type, GLsizei width, GLsizei height, GLenum format, GLvoid *pVAddr, GLuint pPAddr);
typedef GLvoid  (*GLTexDirectVIVFunc)(GLenum type, GLsizei width, GLsizei height, GLenum format, GLvoid **pVAddr);
typedef GLvoid  (*GLTexDirectInvalidateVIVFunc)(GLenum type);

#if ANDROID
#include <ui/FramebufferNativeWindow.h>
#include <ui/EGLUtils.h>
#endif

GLCreateFenceFunc   pGLCreateFence = NULL;
GLDestroyFenceFunc  pGLDestroyFence= NULL;
GLSendFenceFunc     pGLSendFence   = NULL;  
GLWaitFenceFunc     pGLWaitFence   = NULL;
GLTexMapFunc        pGLTexMap      = NULL;
GLTexDirectVIVFunc  pGLTexDirectVIV= NULL;
GLTexDirectInvalidateVIVFunc    pGLTexDirectInvalidate = NULL;

/* Configuration macros starts */
/* This two macros are for standalone test, textures are from bmp or yuv files */
//#define BMP_TEST			
//#define YUV_TEST
//#define DRAW_CYLINDER
#define DRAW_BACKGROUND
#define DRAW_FIREWORKS
#define MSAA
#define GPU_FPS				0
#define TEXTURE_FPS			0
//#define FIXED_STREAM_PER_UPDATE		
#define INITIAL_STREAM_PER_UPDATE		4
#define FPS_ADJUSTMENT_THRESH_LOWER		0.9
#define FPS_ADJUSTMENT_THRESH_UPPER		1.1
//#define FRAME_DROP
#define JITTER_THRESH		30000

/* Configuration macros ends */

#if !(defined BMP_TEST) && !(defined YUV_TEST)
#include "codecVC.h"
#include "queue.h"
extern MANPerfCT               gPerfCT;        /* Performance counter struct */
#endif	/* !BMP_TEST && !YUV_TEST */


#define MAX_TEXTURE         4                   /* doesn't count in background texture and particle texture */
#define TOTAL_TEXTURE       (MAX_TEXTURE+3)
#define TEXTURE_BACKGROUND  (MAX_TEXTURE)
#define TEXTURE_FIRE        (TEXTURE_BACKGROUND+1)
#define TEXTURE_MARVELL     (TEXTURE_BACKGROUND+2)
#define INVALID_TEXID       0xffffffff

#define TEXTURE_Y_ROTATE    1
#define REFLECT_EFFECT      1

#ifndef __LINUX__
#define MAX_LOADSTRING		100
#define SAMPLE_WNDCLS_NAME	TEXT("OGL-ES Compiz Sample")
#define SAMPLE_WINDOW_NAME  TEXT("OGL-ES Compiz Sample")
#endif

#define ALPHA_VALUE         0.255f

#define CUBE                0 // Draw the cube
#define WAVE_TEXTURE        1 // Draw the wave texture 
#define MOVING_TEXTURE      2 // Draw the moving texture
#define BLENDING_TEXTURE    3 // Draw the Blending texture
//#define FIREWORKS	        4 // Draw the fireworks
#define ROT_CUBE            4 // Draw the cube
#define SPHERE              5 // Draw the Sphere
#define CYLINDER            6 // Draw the Cylinder
#ifdef DRAW_CYLINDER
#define TYPES_NUM           7// the num of draw types
#else
#define TYPES_NUM           6// the num of draw types
#endif
unsigned int drawType   =   CUBE;//WAVE_TEXTURE;//CUBE;//BLENDING_TEXTURE;

#define DRAW_MODE0          0
#define DRAW_MODE1          1
#define DRAW_MODE2          2
#define DRAW_MODE3          3
#define DRAW_MODE4          4

#define BLEND_MODE_NUM      4
#define MOVING_MODE_NUM     5

unsigned int drawMode   =   DRAW_MODE0;
bool bDrawFireworks     =   true;
int bStop = 0;

extern int screenWidth;
extern int screenHeight;
extern int bSupportX;

/* Defines All GPU Render work variables */
int enqueue_pic_perf_index, dequeue_pic_perf_index;
int nPerfIndex;
IppVmetaPicture *pPicGrp[MAX_MULTISTRM_NUM] = {NULL};
IppVmetaPicture *pPrevPicGrp[MAX_MULTISTRM_NUM] = {NULL};
int ChannelUpdated[MAX_MULTISTRM_NUM], PicIndex[MAX_MULTISTRM_NUM];
int gframes = 0, vframes = 0, totalframes = 0;
float gfps = 0, vfps = 0;
unsigned long long gpu_start = 0;
#if GPU_FPS
unsigned long long nGPUExpectedTick = 0, nGPUArrivedTick = 0;
unsigned long nGPURate = 1000000/GPU_FPS;
#endif

#ifdef DUMP_TIMESTAMP
/* each frame dumps points at: 0. buffer dequeue 1. before update texture 2. right after update texture 3. AppUpdate finish 4. AppRender finish */
unsigned long long *timestamp[MAX_MULTISTRM_NUM];
#endif

// 3D performance tuning
// For the wave texture 
#define WAVE_SLICES         16  // Note: the WAVE_SLICES value should be less than 44
// For the fireworks
#define MAX_PARTICLES       35  // Max number of particles 
// For the Sphere
#define SPHERE_SLICES       48
#define SPHERE_STACKS       48
// For the Cylinder
#define CYLINDER_SLICES     48
#define CYLINDER_STACKS     8

#if (defined __LINUX__)

/* structures for bmp header */
#pragma pack(2)
typedef struct tagBITMAPFILEHEADER {
        unsigned short    	bfType;
        unsigned long   	bfSize;
        unsigned short    	bfReserved1;
        unsigned short    	bfReserved2;
        unsigned long   	bfOffBits;
} BITMAPFILEHEADER;
#pragma pack()

typedef struct tagBITMAPINFOHEADER{
        unsigned long      	biSize;
        long       			biWidth;
        long       			biHeight;
        unsigned short      biPlanes;
        unsigned short      biBitCount;
        unsigned long      	biCompression;
        unsigned long      	biSizeImage;
        long       			biXPelsPerMeter;
        long       			biYPelsPerMeter;
        unsigned long      	biClrUsed;
        unsigned long      	biClrImportant;
} BITMAPINFOHEADER;

#endif	/* __LINUX__ */



// Global Variables:
#if ANDROID
char sBackgroundFileName[]  = "/sdcard/compiz/bmp/armada_.bmp";
char sParticleFileName[] = "/sdcard/compiz/bmp/particle.bmp";
char sMarvellFileName[] = "/sdcard/compiz/bmp/marvell.bmp";
#else
char sBackgroundFileName[]  = "bmp/armada_.bmp";
char sParticleFileName[] = "bmp/particle.bmp";
char sMarvellFileName[] = "bmp/marvell.bmp";
#endif
BITMAPINFOHEADER info[TOTAL_TEXTURE];
unsigned char *pTexPixels[TOTAL_TEXTURE] = {NULL};
unsigned char *yuvTexPlanes[MAX_TEXTURE]; 

#if (defined BMP_TEST)
char *filenames[] = {"bmp/ubuntu2.bmp","bmp/ubuntu.bmp","bmp/2.bmp","bmp/Tim.bmp", "bmp/armada_.bmp", "bmp/particle.bmp"};
#elif (defined YUV_TEST)
char *filenames[] = {"yuv/ubuntu2.yuv","yuv/ubuntu.yuv","yuv/2.yuv","yuv/Tim.yuv", "yuv/ubuntu2.yuv", "yuv/ubuntu2.yuv"};
#else

/* External defined globals */
extern BufQueue PicResPool[MAX_MULTISTRM_NUM];		    /* buffer queue available to be pushed into vmeta as output buffer */
extern PicGrpQueue VmetaPicGrp;                         /* Queue to store all decoded pic */
extern int nActiveStreamCnt;                            /* Exit condition: !nActiveStreamCnt */
extern SortedList  SortList;
extern int nTotalStreamCnt;
extern void *pEventSuspend;

GLvoid *PicFence[MAX_MULTISTRM_NUM][PICTURE_BUF_NUM];

#endif

#ifdef __LINUX__
NativeWindowType    nativeWin = NULL;
#else
HINSTANCE       g_hInst;                // The current instance
#endif

EGLDisplay      eglDisplay;
EGLConfig       eglConfig;
EGLContext      eglContext;
EGLSurface      eglWindowSurface;

GLuint			texture[TOTAL_TEXTURE];	/* is this must be invalid texture id */


// GL content
float s_cubeVertices[] =
{
    /* FRONT */
    -10,   -10,    10,
    10,    -10,    10,
    -10,   10,     10,
    10,    10,     10,

    /* BACK */
    -10,   -10,    -10,
    -10,   10,     -10,
    10,    -10,    -10,
    10,    10,     -10,

    /* LEFT */
    -10,   -10,    10,
    -10,   10,     10,
    -10,   -10,    -10,
    -10,   10,     -10,

    /* RIGHT */
    10,    -10,    -10,
    10,    10,     -10,
    10,    -10,    10,
    10,    10,     10,

    /* TOP */
    -10,   10,     10,
    10,    10,     10,
    -10,   10,     -10,
    10,    10,     -10,

    /* BOTTOM */
    -10,   -10,    10,
    -10,   -10,    -10,
    10,    -10,    10,
    10,    -10,    -10,
};

// GL content
GLfloat rectVertices[] =
{
    /* FRONT */
    -16,   -9,    0,
    16,    -9,    0,
    -16,   9,     0,
    16,    9,     0,

    /* BACK */
    -12,   -15,    0,
    12,    -15,    0,
    -12,   15,     0,
    12,    15,     0,

};

GLfloat reflectCoords[] = {
    // FRONT
    0.0f, 1.0f,
    1.0f, 1.0f,
    0.0f, 0.0f,
    1.0f, 0.0f,

    // BACK
	 1.0f, 1.0f,
	 1.0f, 0.0f,
	 0.0f, 1.0f,
	 0.0f, 0.0f,
	// LEFT
	 1.0f, 1.0f,
	 1.0f, 0.0f,
	 0.0f, 1.0f,
	 0.0f, 0.0f,
	// RIGHT
	 1.0f, 1.0f,
	 1.0f, 0.0f,
	 0.0f, 1.0f,
	 0.0f, 0.0f,

};

GLfloat texCoords[] = {
	// FRONT
	 0.0f, 0.0f,
	 1.0f, 0.0f,
	 0.0f, 1.0f,
	 1.0f, 1.0f,
	// BACK
	 1.0f, 0.0f,
	 1.0f, 1.0f,
	 0.0f, 0.0f,
	 0.0f, 1.0f,
	// LEFT
	 1.0f, 0.0f,
	 1.0f, 1.0f,
	 0.0f, 0.0f,
	 0.0f, 1.0f,
	// RIGHT
	 1.0f, 0.0f,
	 1.0f, 1.0f,
	 0.0f, 0.0f,
	 0.0f, 1.0f,
	// TOP
	 0.0f, 0.0f,
	 1.0f, 0.0f,
	 0.0f, 1.0f,
	 1.0f, 1.0f,
	// BOTTOM
	 1.0f, 0.0f,
	 1.0f, 1.0f,
	 0.0f, 0.0f,
	 0.0f, 1.0f
};

#if TEXTURE_Y_ROTATE
#define TEX_COORDS reflectCoords
#define REFLECT_TEX_COORDS texCoords
#else
#define TEX_COORDS texCoords
#define REFLECT_TEX_COORDS reflectCoords
#endif
// Animation parameter
float  cubeRotate;


// For Waving Texture effect
GLfloat			xrot;					/* X Rotation */
GLfloat			yrot;					/* Y Rotation */
GLfloat			zrot;					/* Z Rotation */

GLfloat			points[45][45][3];			/* The Points On The Grid Of Our "Wave" */
GLfloat			vertices[45*45*4][3];		/* Vertices array */
GLfloat			texcoords[45*45*4][2];		/* Texture coordinates array */
GLfloat			hold;						/* Temporarily Holds A Floating Point Value */
int				wiggle_count	= 0;		/* Counter Used To Control How Fast Flag Waves */
GLfloat         MovingRot       = 0;
bool			keys[256];					// Array Used For The Keyboard Routine
MRVL_GLUquadricObj   *quadratic;	                // Storage For Our Quadratic Objects ( NEW )
///////////////////
// For Fireworks effect


bool rainbow = true;  /* Toggle rainbow effect                         */

float slowdown=1.0f;   /* Slow Down Particles                                */
float xspeed=-180;        /* Base X Speed (To Allow Keyboard Direction Of Tail) */
float yspeed=-180;        /* Base Y Speed (To Allow Keyboard Direction Of Tail) */
float zoom=-30.0f;     /* Used To Zoom Out                                   */

GLuint loop;           /* Misc Loop Variable                                 */
GLuint col=0;          /* Current Color Selection                            */
/* Create our particle structure */
typedef struct
{
   bool active; /* Active (Yes/No) */
   float life;   /* Particle Life   */
   float fade;   /* Fade Speed      */

   float r;      /* Red Value       */
   float g;      /* Green Value     */
   float b;      /* Blue Value      */

   float x;      /* X Position      */
   float y;      /* Y Position      */
   float z;      /* Z Position      */

   float xi;     /* X Direction     */
   float yi;     /* Y Direction     */
   float zi;     /* Z Direction     */

   float xg;     /* X Gravity       */
   float yg;     /* Y Gravity       */
   float zg;     /* Z Gravity       */
} particle;

/* Rainbow of colors */
static GLfloat colors[12][3]=
{
   {1.0f,  0.5f,  0.5f},
   {1.0f,  0.75f, 0.5f},
   {1.0f,  1.0f,  0.5f},
   {0.75f, 1.0f,  0.5f},
   {0.5f,  1.0f,  0.5f},
   {0.5f,  1.0f,  0.75f},
   {0.5f,  1.0f,  1.0f},
   {0.5f,  0.75f, 1.0f},
   {0.5f,  0.5f,  1.0f},
   {0.75f, 0.5f,  1.0f},
   {1.0f,  0.5f,  1.0f},
   {1.0f,  0.5f,  0.75f}
};

/* Our beloved array of particles */
particle particles[MAX_PARTICLES];

#define NUM_FIRE_TYPE   6
#define FIREWORKS_RANGE 18.0
GLfloat xInits[] = { -FIREWORKS_RANGE, FIREWORKS_RANGE, 0.0, -FIREWORKS_RANGE, -FIREWORKS_RANGE, FIREWORKS_RANGE};
GLfloat yInits[] = { -FIREWORKS_RANGE, -FIREWORKS_RANGE, -FIREWORKS_RANGE, 0.0, -FIREWORKS_RANGE, FIREWORKS_RANGE};
GLfloat xi[] = { 0.3f,  -0.3f, 0.0f, 0.42f, 0.3f, -0.3f,};
GLfloat yi[] = { 0.3f,  0.3f, 0.42f, 0.0f, -0.3f, -0.3f,};
float xspeeds[] ={ -180, 180, 0.0,  -180, -180, 180, };        /* Base X Speed (To Allow Keyboard Direction Of Tail) */
float yspeeds[] ={-180, -180, -180, 0, 180, 180,};              /* Base Y Speed (To Allow Keyboard Direction Of Tail) */

GLfloat fireX = -FIREWORKS_RANGE, fireY = -FIREWORKS_RANGE;
unsigned int fireworksType = 0;

/* function to reset one particle to initial state */
/* NOTE: I added this function to replace doing the same thing in several
 * places and to also make it easy to move the pressing of numpad keys
 * 2, 4, 6, and 8 into handleKeyPress function.
 */
void ResetParticle(int num, int color, float xDir, float yDir, float zDir)
{
    /* Make the particels active */
    particles[num].active= true;
    /* Give the particles life */
    particles[num].life=1.0f;
    /* Random Fade Speed */
    particles[num].fade=(float)(rand()%300)/1000.0f+0.003f;
    /* Select Red Rainbow Color */
    particles[num].r=colors[color][0];
    /* Select Green Rainbow Color */
    particles[num].g=colors[color][1];
    /* Select Blue Rainbow Color */
    particles[num].b=colors[color][2];
    /* Set the position on the X axis */
    particles[num].x=0.0f;
    /* Set the position on the Y axis */
    particles[num].y=0.0f;
    /* Set the position on the Z axis */
    particles[num].z=0.0f;
    /* Random Speed On X Axis */
    particles[num].xi=xDir;
    /* Random Speed On Y Axi */
    particles[num].yi=yDir;
    /* Random Speed On Z Axis */
    particles[num].zi=zDir;
    /* Set Horizontal Pull To Zero */
    particles[num].xg=0.0f;
    /* Set Vertical Pull Downward */
    particles[num].yg=0.1f;//-0.5f;
    /* Set Pull On Z Axis To Zero */
    particles[num].zg=0.0f;

    return;
}

void initParticles()
{
    /* Reset all the particles */
    for (loop=0; loop<MAX_PARTICLES; loop++)
    {
        int color=(loop+1)/(MAX_PARTICLES/12);
        float xi, yi, zi;

        xi= -0;//(float)((rand()%50)-26.0f)*10.0f;
        yi= -0;
        zi=(float)((rand()%50)-25.0f)*10.0f;

        ResetParticle(loop, color, xi, yi, zi);
    }
}

static int LoadGLLibrary(const char *lib, void** lib_handle)
{
    const char *err = NULL;
    void *handle = NULL;

    DEMO_PRINTF("Entering:%s\n", __FUNCTION__);

    if (lib_handle == NULL || NULL == lib) {
        return -1;
    }
	
	while(NULL != (err = dlerror())) {
        /* Do Nothing */
    }

    handle = dlopen(lib, RTLD_NOW);
    if (NULL == handle) {
        err = dlerror();
        if (NULL != err) {
            IPP_Printf("%s: %s, FILE: %s, LINE:%d\n", err, lib, __FILE__, __LINE__);
        }
        return -1;
    }

    *lib_handle = handle;

    DEMO_PRINTF("Exit:%s\n", __FUNCTION__);

    return 0;
}

static int UnloadGLLibrary(void *lib_handle)
{
    const char *err = NULL;

    DEMO_PRINTF("Entering:%s\n", __FUNCTION__);

    if (NULL == lib_handle) {
        return -1;
    }
    
    dlclose(lib_handle);
    err = dlerror();
    if (err) {
        IPP_Printf("Error:%s\n", err);
        return -1;
    }

    DEMO_PRINTF("Exit:%s\n", __FUNCTION__);

    return 0;
}

static int GetGLProc(void *lib_handle, const char *proc_name, void **proc_ptr)
{
    const char *err = NULL;
    void *proc = NULL;

    if (NULL == lib_handle || NULL == proc_name || NULL == proc_ptr) {
        return -1;
    }

    proc = dlsym(lib_handle, proc_name);

    err = dlerror();
    if (err) {
        IPP_Printf("%s:%s, File: %s Line:%d\n", err, proc_name, __FILE__, __LINE__);
        *proc_ptr = NULL;
        return -1;
    }
    *proc_ptr = proc;

    return 0;
}





#ifdef YUV_TEST
//*****************************************************************************
// Name:            loadYUV
// Description:     read yuv texture from file
//*****************************************************************************
unsigned char* loadYUV(char *filename,  unsigned int w, unsigned int h)
{
	FILE *file;
	unsigned char *bmpImage = NULL;

	file = fopen(filename, "rb");
	if (!file)
	{
		return NULL;
	}
	bmpImage = (unsigned char*)malloc(sizeof(unsigned char)*w*h*2);	
	if(!bmpImage) {
		return NULL;
	}
	fread(bmpImage, 1, w*h*2, file);

	fclose(file);
	return bmpImage;
}
#endif	/* YUV_TEST */

unsigned char *loadBMP(char *filename, BITMAPINFOHEADER *bmpInfo)
{
    int i;
	
	FILE *file;
	unsigned char *bmpImage = NULL;
	unsigned char tmpRGB;
	BITMAPFILEHEADER bmpFile;

	file = fopen(filename,"rb");

	if (!file)
	{
		IPP_Printf("Cannot open file %s\n", filename);
		return NULL;
	}

	fread(&bmpFile,sizeof(BITMAPFILEHEADER),1,file);

	if (bmpFile.bfType != 0x4D42)
	{
		IPP_Printf("Incorrect texture type\n");
		fclose(file);
		return NULL;
	}

	fread(bmpInfo,sizeof(BITMAPINFOHEADER),1,file);

	fseek(file,bmpFile.bfOffBits,SEEK_SET);
	bmpImage = (unsigned char*)malloc(sizeof(unsigned char)*bmpInfo->biHeight*bmpInfo->biWidth*4);

	if (!bmpImage)
	{
		fclose(file);
		return NULL;
	}

	if(fread(bmpImage,1,bmpInfo->biHeight*bmpInfo->biWidth*3,file) != bmpInfo->biHeight*bmpInfo->biWidth*3) {
		IPP_Printf("Error reading pTexPixels\n");
		free(bmpImage);
		bmpImage = NULL;
		fclose(file);
		return NULL;
	}

	for (i = 0; i < bmpInfo->biHeight*bmpInfo->biWidth*3; i+=3)
	{
		tmpRGB = bmpImage[i];
		bmpImage[i] = bmpImage[i+2];
		bmpImage[i+2] = tmpRGB;
	}

	for (i=(bmpInfo->biHeight*bmpInfo->biWidth)-1; i>=0; i--)
	{
		bmpImage[4*i]	= bmpImage[(3*i)];
		bmpImage[4*i+1] = bmpImage[(3*i)+1];
		bmpImage[4*i+2] = bmpImage[(3*i)+2];
		bmpImage[4*i+3] = 255;
	}

	fclose(file);

	return bmpImage;
}

/* don't care width/height if isRGB=1 */
bool LoadTexture(int id, char* filename, int width, int height, int isRGB)
{
	glGenTextures(1, &texture[id]);
	glBindTexture(GL_TEXTURE_2D, texture[id]);

    if(isRGB) {
        pTexPixels[id] = loadBMP(filename, &info[id]);
        if (!pTexPixels[id]) {
            IPP_Printf("loadBMP error\n");
            return false;
        }
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, info[id].biWidth, info[id].biHeight,
            0, GL_RGBA, GL_UNSIGNED_BYTE,
            pTexPixels[id]);
        //IPP_Printf("Texture created from %s %dx%d, id=%d texid=%d\n", filename, info[id].biWidth, info[id].biHeight, id, texture[id]);
    } 
    else 
    {
#ifdef YUV_TEST
        pTexPixels[id] = loadYUV(filename, width, height);
#endif  /* YUV_TEST */
        pGLTexDirectVIV(GL_TEXTURE_2D, width, height, GL_VIV_UYVY, (GLvoid**) &yuvTexPlanes[id]);
        //IPP_Printf("Texture created from %s %dx%d, id=%d texid=%d\n", filename, width, height, id, texture[id]);
    }
	glTexParameterf(GL_TEXTURE_2D, 
		GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D,
		GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D,
		GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D,
		GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	return true;
}

void UnloadTexture()
{
    int i;
	glDeleteTextures(MAX_TEXTURE, texture);
	glDeleteTextures(1, &texture[TEXTURE_BACKGROUND]);
	glDeleteTextures(1, &texture[TEXTURE_FIRE]);
    glDeleteTextures(1, &texture[TEXTURE_MARVELL]);
#if (defined BMP_TEST || defined YUV_TEST)
	{
		for(i=0;i<MAX_TEXTURE;i++)
        {
			free(pTexPixels[i]);
			pTexPixels[i] = NULL;
		}
	}

#endif	/* BMP_TEST || YUV_TEST */
    for(i=MAX_TEXTURE;i<TOTAL_TEXTURE;i++)
    {
        free(pTexPixels[i]);
        pTexPixels[i] = NULL;
    }
}

#if (MAX_TEXTURE==6)
static int validTextures[TYPES_NUM][MAX_TEXTURE] = {
	{0, 1, 2, 3, 4, 5}, 																			/* CUBE */
	{3, INVALID_TEXID, INVALID_TEXID, INVALID_TEXID, INVALID_TEXID, INVALID_TEXID},					/* WAVE_TEXTURE */
	{0, 1, 2, 3, INVALID_TEXID, INVALID_TEXID}, 													/* MOVING_TEXTURE */
	{1, 2, 3, INVALID_TEXID, INVALID_TEXID, INVALID_TEXID},											/* BLENDING_TEXTURE */
	{0, 1, 2, 3, 4, 5}, 																			/* ROT_CUBE */
	{0, 1, 2, 3, INVALID_TEXID, INVALID_TEXID},														/* SPHERE */
#ifdef DRAW_CYLINDER
	{2, 3, INVALID_TEXID, INVALID_TEXID, INVALID_TEXID, INVALID_TEXID},								/* CYLINDER */	
#endif
};
#endif

#if (MAX_TEXTURE==4)
static int validTextures[TYPES_NUM][MAX_TEXTURE] = {
	{0, 1, 2, 3}, 										/* CUBE */
	{3, INVALID_TEXID, INVALID_TEXID, INVALID_TEXID},	/* WAVE_TEXTURE */
	{0, 1, 2, 3}, 										/* MOVING_TEXTURE */
	{1, 2, 3, INVALID_TEXID},							/* BLENDING_TEXTURE */
	{0, 1, 2, 3}, 										/* ROT_CUBE */
	{0, 1, 2, 3},										/* SPHERE */
#ifdef DRAW_CYLINDER
	{2, 3, INVALID_TEXID, INVALID_TEXID},				/* CYLINDER */
#endif
};
#endif

static int validTextureNum[TYPES_NUM] = {
	4, 1, 4, 3, 4, 4,
#ifdef DRAW_CYLINDER
    2 
#endif
};

static bool CheckValidTexture(int id, int drawType) {
	int i;
	int texid;
	for(i=0;i<MAX_TEXTURE;i++) {
		texid = validTextures[drawType][i];
		if(texid == (int)INVALID_TEXID) {
			break;
		}
		if(texid == id) {
			return true;
		}
	}

	PRINTF_THREAD("[GPU Thread] Skip updating texture %d in draw type %d\n", id, drawType);
	
	return false;
}

int UpdateTexture(void *pPic, int id)
{
#if ENABLE_GPU_RENDER
    IppVmetaPicture *pInfo = (IppVmetaPicture*)pPic;

#if TEXTURE_FPS
	static unsigned long long nVideoExpectedTick[MAX_MULTISTRM_NUM] = {0}, nVideoArrivedTick[MAX_MULTISTRM_NUM] = {0};
	static long nVideoRate = 1000000 / TEXTURE_FPS;
#endif

	if(!CheckValidTexture(id, drawType)) {
		return 0;
	}

	if(texture[id] == INVALID_TEXID) {
        glGenTextures(1, &texture[id]);
        glBindTexture(GL_TEXTURE_2D, texture[id]);
        glTexParameterf(GL_TEXTURE_2D, 
	        GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D,
	        GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D,
	        GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D,
	        GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    } else {
        glBindTexture(GL_TEXTURE_2D, texture[id]);
    }

#if TEXTURE_FPS
    if (!nVideoExpectedTick[id]) {
    	nVideoExpectedTick[id] = nVideoArrivedTick[id] = GetTickCount();
    } else {
        nVideoArrivedTick[id] = GetTickCount();	
    }

    if(	nVideoArrivedTick[id] > nVideoExpectedTick[id] + JITTER_THRESH) {
    	nVideoExpectedTick[id] =  nVideoArrivedTick[id] + nVideoRate;
    	return 0;		/* drop late frames */
    }

    if(nVideoArrivedTick[id] < nVideoExpectedTick[id]) {
    	usleep(nVideoExpectedTick[id]- nVideoArrivedTick[id]);
    	nVideoExpectedTick[id] += nVideoRate;
	} else {
		nVideoExpectedTick[id] =  nVideoArrivedTick[id] + nVideoRate;
	}
#endif

    gPerfCT.ulFrameRenderNumber[id]++;
    pGLTexMap(GL_TEXTURE_2D, pInfo->pic.picWidth, 
        pInfo->pic.picHeight, GL_VIV_UYVY, pInfo->pBuf, pInfo->nPhyAddr);

    pGLTexDirectInvalidate(GL_TEXTURE_2D);
#endif
	return 1;
}

static int GetFuncPointer()
{

    void *handle;
#if ANDROID
    const char *lib_name = "libGLESv1_CM_MRVL.so";
#else
    const char *lib_name = "libGLES_CM.so";
#endif

    DEMO_PRINTF("Entering:%s\n", __FUNCTION__);

    if (LoadGLLibrary(lib_name, &handle) < 0) {
        IPP_Printf("Load %s failed!\n", lib_name);
        return -1;
    }

    if ( NULL == pGLCreateFence) {
        if (GetGLProc(handle, (const char*)"glCreateFence", (void**)&pGLCreateFence) < 0) {
            IPP_Printf("Get glCreateFence failed!\n");
            return -1;
        }
    }
    if ( NULL == pGLDestroyFence) {
        if (GetGLProc(handle, (const char*)"glDestroyFence", (void**)&pGLDestroyFence) < 0) {
            IPP_Printf("Get glDestroyFence failed!\n");
            return -1;
        }
    }
    if ( NULL == pGLSendFence) {
        if (GetGLProc(handle, (const char*)"glSendFence", (void**)&pGLSendFence) < 0) {
            IPP_Printf("Get glSendFence failed!\n");
            return -1;
        }
    }
    if ( NULL == pGLWaitFence) {
        if (GetGLProc(handle, (const char*)"glWaitFence", (void**)&pGLWaitFence) < 0) {
            IPP_Printf("Get glWaitFence failed\n");
            return -1;
        }
    }
    if (NULL == pGLTexMap) {
        if (GetGLProc(handle, (const char*)"glTexDirectVIVMap", (void**)&pGLTexMap) < 0) {
            IPP_Printf("Get glTexDirectVIVMap failed\n");
            return -1;
        }
    }
    if (NULL == pGLTexDirectVIV) {
        if (GetGLProc(handle, (const char*)"glTexDirectVIV", (void**)&pGLTexDirectVIV) < 0) {
            IPP_Printf("Get glTexDirectVIV failed\n");
            return -1;
        }
    }
    if (NULL == pGLTexDirectInvalidate) {
        if (GetGLProc(handle, (const char*)"glTexDirectInvalidateVIV", (void**)&pGLTexDirectInvalidate) < 0) {
            IPP_Printf("Get glTexDirectInvalidate failed\n");
            return -1;
        }
    }

    UnloadGLLibrary(handle);

    DEMO_PRINTF("Exit:%s\n", __FUNCTION__);

    return 0;
}

void updateFireworks()
{
    fireX += xi[fireworksType];
    fireY += yi[fireworksType];

    if (fireX > FIREWORKS_RANGE || fireX < -FIREWORKS_RANGE || fireY > FIREWORKS_RANGE || fireY < -FIREWORKS_RANGE)
    {
        fireworksType ++;
        fireworksType %= NUM_FIRE_TYPE;

        fireX = xInits[fireworksType];
        fireY = yInits[fireworksType];

        xspeed = xspeeds[fireworksType];
        yspeed = yspeeds[fireworksType];

        /* Reset all the particles */
        for (loop=0; loop<MAX_PARTICLES; loop++)
        {
            col ++;
            col %= 12;
            float xi, yi, zi;

            xi= -0;//(float)((rand()%50)-26.0f)*10.0f;
            yi= -0;
            zi=(float)((rand()%50)-25.0f)*10.0f;

            ResetParticle(loop, col, xi, yi, zi);
        }
    }

}

//*****************************************************************************
// Name:            AppUpdate
// Description:     Application update 
//****************************************************************************
#define CUBE_ROT    0.7f
int AppUpdate()
{
#if ENABLE_GPU_RENDER
    if (drawType == CUBE || drawType == ROT_CUBE)
    {
        // animate cube
        cubeRotate += CUBE_ROT;
        if (cubeRotate >= 360.0)
        {
            cubeRotate -= 360;
        }
    }
    else if(drawType == BLENDING_TEXTURE )
    {
void UpdateBlendTexture( );	    	
        UpdateBlendTexture();
    }
    else if (drawType == SPHERE)
    {
        // animate cube
        cubeRotate += 1.00f;
        if (cubeRotate >= 360.0)
        {
            cubeRotate -= 360;
        }
    }
    else if (drawType == CYLINDER)
    {
        // animate cube
        cubeRotate += 1.00f;
        if (cubeRotate >= 360.0)
        {
            cubeRotate -= 360;
        }
    }
    else if (drawType == MOVING_TEXTURE)
    {
void UpdateMovingTexture();	    	
	    UpdateMovingTexture();
	}
#ifdef DRAW_FIREWORKS
    if(bDrawFireworks)
    {
        updateFireworks();
    }
#endif 
#endif
	return 1;
}

//*****************************************************************************
// Name:            DrawVaveFlag
// Description:     draw the Waving Texture
//*****************************************************************************
void DrawWaveFlag( void )
{
    int x, y;                     /* Loop Variables */
    GLfloat f_x, f_y, f_xb, f_yb; /* Used To Break The Flag Into Tiny Quads */

    /* Clear The Screen And Depth Buffer */
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* Reset The Current Matrix */
    glLoadIdentity();

    /* Translate 17 Units Into The Screen */
    glTranslatef(0.0f, 0.0f, -17.0f);
#if TEXTURE_Y_ROTATE
    glScalef(1.0, -1.0, 1.0);
#endif
    glRotatef(xrot, 1.0f, 0.0f, 0.0f); /* Rotate On The X Axis */
    glRotatef(yrot, 0.0f, 1.0f, 0.0f); /* Rotate On The Y Axis */
    glRotatef(zrot, 0.0f, 0.0f, 1.0f); /* Rotate On The Z Axis */

    glBindTexture(GL_TEXTURE_2D, texture[3 % MAX_TEXTURE]); /* Select Our Texture */

    /* Set pointers to vertices and texcoords */
    glVertexPointer(3, GL_FLOAT, 0, vertices);
    glTexCoordPointer(2, GL_FLOAT, 0, texcoords);

    /* Enable vertices and texcoords arrays */
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    /* Loop Through The X Plane 0-WAVE_SLICES ((WAVE_SLICES + 1) Points) */
    for(x=0; x<WAVE_SLICES; x++)
    {
		if( (x%2) == 0)
		{
            /* Loop Through The Y Plane 0-WAVE_SLICES ((WAVE_SLICES + 1) Points) */
            for(y=0; y<WAVE_SLICES; y++)
			{
                /* Create A Floating Point X Value */
                f_x=(GLfloat)x/WAVE_SLICES;
                /* Create A Floating Point Y Value */
                f_y=(GLfloat)y/WAVE_SLICES;
                /* Create A Floating Point Y Value+0.0227f */
                f_xb=(GLfloat)(x+1)/WAVE_SLICES;
                /* Create A Floating Point Y Value+0.0227f */
                f_yb=(GLfloat)(y+1)/WAVE_SLICES;

                /* First Texture Coordinate (Bottom Left) */
                texcoords[(x*WAVE_SLICES*4) + (y*4)][0]=f_x;
                texcoords[(x*WAVE_SLICES*4) + (y*4)][1]=f_y;
                vertices[(x*WAVE_SLICES*4) + (y*4)][0]=points[x][y][0];
                vertices[(x*WAVE_SLICES*4) + (y*4)][1]=points[x][y][1];
                vertices[(x*WAVE_SLICES*4) + (y*4)][2]=points[x][y][2];

                /* Second Texture Coordinate (Top Left) */
                texcoords[(x*WAVE_SLICES*4) + (y*4)+1][0]=f_x;
                texcoords[(x*WAVE_SLICES*4) + (y*4)+1][1]=f_yb;
                vertices[(x*WAVE_SLICES*4) + (y*4)+1][0]=points[x][y+1][0];
                vertices[(x*WAVE_SLICES*4) + (y*4)+1][1]=points[x][y+1][1];
                vertices[(x*WAVE_SLICES*4) + (y*4)+1][2]=points[x][y+1][2];

                /* Fourth Texture Coordinate (Bottom Right) */
                texcoords[(x*WAVE_SLICES*4) + (y*4)+2][0]=f_xb;
                texcoords[(x*WAVE_SLICES*4) + (y*4)+2][1]=f_y;
                vertices[(x*WAVE_SLICES*4) + (y*4)+2][0]=points[x+1][y][0];
                vertices[(x*WAVE_SLICES*4) + (y*4)+2][1]=points[x+1][y][1];
                vertices[(x*WAVE_SLICES*4) + (y*4)+2][2]=points[x+1][y][2];

                /* Third Texture Coordinate (Top Right) */
                texcoords[(x*WAVE_SLICES*4) + (y*4)+3][0]=f_xb;
                texcoords[(x*WAVE_SLICES*4) + (y*4)+3][1]=f_yb;
                vertices[(x*WAVE_SLICES*4) + (y*4)+3][0]=points[x+1][y+1][0];
                vertices[(x*WAVE_SLICES*4) + (y*4)+3][1]=points[x+1][y+1][1];
                vertices[(x*WAVE_SLICES*4) + (y*4)+3][2]=points[x+1][y+1][2];
                ///* Draw one textured quad using two stripped triangles */
                //glDrawArrays(GL_TRIANGLE_STRIP, (x*WAVE_SLICES*4) + (y*4), 4);
			}
		}
		else
		{
            /* Loop Through The Y Plane 0-WAVE_SLICES ((WAVE_SLICES + 1) Points) */
            for(y=WAVE_SLICES-1; y>=0; y--)
            {
                /* Create A Floating Point X Value */
                f_x=(GLfloat)x/WAVE_SLICES;
                /* Create A Floating Point Y Value */
                f_y=(GLfloat)y/WAVE_SLICES;
                /* Create A Floating Point Y Value+0.0227f */
                f_xb=(GLfloat)(x+1)/WAVE_SLICES;
                /* Create A Floating Point Y Value+0.0227f */
                f_yb=(GLfloat)(y+1)/WAVE_SLICES;

                /* First Texture Coordinate (Bottom Left) */
                texcoords[(x*WAVE_SLICES*4) + (y*4)][0]=f_x;
                texcoords[(x*WAVE_SLICES*4) + (y*4)][1]=f_y;
                vertices[(x*WAVE_SLICES*4) + (y*4)][0]=points[x][y][0];
                vertices[(x*WAVE_SLICES*4) + (y*4)][1]=points[x][y][1];
                vertices[(x*WAVE_SLICES*4) + (y*4)][2]=points[x][y][2];

                /* Second Texture Coordinate (Top Left) */
                texcoords[(x*WAVE_SLICES*4) + (y*4)+1][0]=f_x;
                texcoords[(x*WAVE_SLICES*4) + (y*4)+1][1]=f_yb;
                vertices[(x*WAVE_SLICES*4) + (y*4)+1][0]=points[x][y+1][0];
                vertices[(x*WAVE_SLICES*4) + (y*4)+1][1]=points[x][y+1][1];
                vertices[(x*WAVE_SLICES*4) + (y*4)+1][2]=points[x][y+1][2];

                /* Fourth Texture Coordinate (Bottom Right) */
                texcoords[(x*WAVE_SLICES*4) + (y*4)+2][0]=f_xb;
                texcoords[(x*WAVE_SLICES*4) + (y*4)+2][1]=f_y;
                vertices[(x*WAVE_SLICES*4) + (y*4)+2][0]=points[x+1][y][0];
                vertices[(x*WAVE_SLICES*4) + (y*4)+2][1]=points[x+1][y][1];
                vertices[(x*WAVE_SLICES*4) + (y*4)+2][2]=points[x+1][y][2];

                /* Third Texture Coordinate (Top Right) */
                texcoords[(x*WAVE_SLICES*4) + (y*4)+3][0]=f_xb;
                texcoords[(x*WAVE_SLICES*4) + (y*4)+3][1]=f_yb;
                vertices[(x*WAVE_SLICES*4) + (y*4)+3][0]=points[x+1][y+1][0];
                vertices[(x*WAVE_SLICES*4) + (y*4)+3][1]=points[x+1][y+1][1];
                vertices[(x*WAVE_SLICES*4) + (y*4)+3][2]=points[x+1][y+1][2];
                ///* Draw one textured quad using two stripped triangles */
                //glDrawArrays(GL_TRIANGLE_STRIP, (x*WAVE_SLICES*4) + (y*4), 4);
			}
		}
    }
	/* Draw one textured quad using two stripped triangles */
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4*WAVE_SLICES*WAVE_SLICES);
    /* Disable texcoords and vertices arrays */
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);

    glFinish();

    /* Used To Slow Down The Wave (Every 2nd Frame Only) */
    if (wiggle_count==2)
    {
        /* Loop Through The Y Plane */
        for (y=0; y<WAVE_SLICES+1; y++)
        {
            /* Store Current Value One Left Side Of Wave */
            hold=points[0][y][2];
            /* Loop Through The X Plane */
            for(x=0; x<WAVE_SLICES; x++)
            {
                /* Current Wave Value Equals Value To The Right */
                points[x][y][2]=points[x+1][y][2];
            }

            /* Last Value Becomes The Far Left Stored Value */
            points[WAVE_SLICES][y][2]=hold;
        }
        wiggle_count=0; /* Set Counter Back To Zero */
    }
    wiggle_count++; /* Increase The Counter */

    xrot+=0.3f; /* Increase The X Rotation Variable */
    yrot+=0.2f; /* Increase The Y Rotation Variable */
    zrot+=0.4f; /* Increase The Z Rotation Variable */

    xrot+=0.3f; /* X Axis Rotation */
    yrot+=0.2f; /* Y Axis Rotation */
    zrot+=0.4f; /* Z Axis Rotation */

}

//*****************************************************************************
// Name:            UpdateMovingTexture
// Description:     Update the Moving Texture
//                    
//*****************************************************************************
#define BLEND_FRAMES    12
void UpdateMovingTexture()
{
    // For Moving Texture
    MovingRot += 90.0f/BLEND_FRAMES;
    switch (drawMode)
    {
    case DRAW_MODE0:
        // For Moving Texture
        MovingRot -= (90.0f/BLEND_FRAMES -1.0f);
        if(MovingRot >= 360.0f)
        {
            MovingRot -= 360.0f;
        }
        break;

    case DRAW_MODE1:

        if(MovingRot >= 90.0f)
        {
            MovingRot = 90.0f;
        }
        break;
    case DRAW_MODE2:

        if(MovingRot >= 180.0f)
        {
            MovingRot = 180.0f;
        }
        break;
    case DRAW_MODE3:

        if(MovingRot >= 270.0f)
        {
            MovingRot = 270.0f;
        }
        break;
    case DRAW_MODE4:

        if(MovingRot >= 360.0f)
        {
            MovingRot = 360.0f;
        }
        break;
    default:
        break;

    }

}
//*****************************************************************************
// Name:            DrawMovingTexture
// Description:     Draw the Moving Texture
//                    
//*****************************************************************************
#define MOVING_TEXTURE_Z  5
void DrawMovingTexture(  bool bDrawBlend = false)
{
	int i;
	GLfloat x = 0.0f, y = 0.0f, z= 0.0f, scale = 1.0f, radians = 0.0f;

	glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, rectVertices);

	// For texure coord
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    if(bDrawBlend)
    {
        glColor4f(1.0, 1.0, 1.0, ALPHA_VALUE);
    }

	for (i=0; i<4; i++)
	{
		// 
        //MovingRot = 0;
        radians = (GLfloat)( -1 * MovingRot * PI/180.0f);
		radians = radians + (i * PI) / 2;

        x = cos(radians) * 75;
        y = 5-sin(radians) * 45;
#if REFLECT_EFFECT
        y += 30.0;
#endif
        z = sin(radians) * 65 - 200 + MOVING_TEXTURE_Z;
        scale = 2.0f + sin(radians);

#if REFLECT_EFFECT
        glDisable(GL_BLEND);
#endif
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(x, y, z);   
		glScalef(scale, scale, 1.0);

        glBindTexture(GL_TEXTURE_2D, texture[i % MAX_TEXTURE]);
        glTexCoordPointer(2, GL_FLOAT, 0, TEX_COORDS);

        if(!bDrawBlend)
        {
            // Draw the opaque part
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}
        else
        {

#if REFLECT_EFFECT
            // Draw The blend one
            glEnable(GL_BLEND);

            glTranslatef(0, -18, 0);   
            glTexCoordPointer(2, GL_FLOAT, 0, REFLECT_TEX_COORDS);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
#endif
        }

	}


    if(bDrawBlend)
    {
        glColor4f(1.0, 1.0, 1.0, 1.0);
    }
	return;
}





//*****************************************************************************
// Name:            DrawBackground
// Description:     Draw the Background texture
//                    
//*****************************************************************************
void DrawBackground( void )
{
#ifdef DRAW_BACKGROUND	
    glEnable(GL_BLEND);
    glColor4f(1.0, 1.0, 1.0, 1.5*ALPHA_VALUE);

	GLfloat x =0.0f, y = 0.0f, z= 0.0f, scale = 1.0f;

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, s_cubeVertices);

	// For texure coord
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 0, texCoords);

    x = -150.0f;
    y = 110.0f;
	z = -300;
    scale = 19.0f * 0.5;
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(x, y, z);   
    glScalef(scale*0.512f, scale*0.064f, 1.0f);

	glBindTexture(GL_TEXTURE_2D, texture[TEXTURE_BACKGROUND]);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    x *= -1.0f;
    y *= -1.0f;
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(x, y, z);
    scale *= 0.8f;
    glScalef(scale*0.512f, scale*0.064f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, texture[TEXTURE_MARVELL]);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glColor4f(1.0, 1.0, 1.0, 1.0);
	glDisable(GL_BLEND);

#endif
	return;
}


//*****************************************************************************
// Name:            DrawBlendTexture
// Description:     Draw the Blend Texture
//                    
//*****************************************************************************

//#define BLEND_FRAMES    15		//redefinition
#define BLEND_ROT       45.0f
#define BLEND_X         15.0f
#define BLEND_Z         20.0f

GLfloat x[]     = { -30.0f,  0.0f,  30.0f, };
GLfloat y[]     = { 5.0f,   5.0f,   5.0f ,  };
GLfloat z[]     = { -60.0f, -40.0f,-60.0f, };
GLfloat rots[]  = { BLEND_ROT,  0.0, -BLEND_ROT, };

GLfloat blendXs[3];
GLfloat blendYs[3];
GLfloat blendZs[3];
GLfloat blendRots[3];

GLfloat blendX = 0.0f, blendZ = 0.0f, blendRot = 0.0f;

void UpdateBlendTexture( )
{
    if (drawMode == DRAW_MODE0 || drawMode == DRAW_MODE2)
    {
        blendRot -= (BLEND_ROT/BLEND_FRAMES);
        blendX += (BLEND_X/BLEND_FRAMES);
        blendZ += (BLEND_Z/BLEND_FRAMES);
        if(blendRot < -BLEND_ROT || blendX > BLEND_X || blendZ > BLEND_Z )
        {
            blendRot = -BLEND_ROT;
            blendX = BLEND_X;
            blendZ = BLEND_Z;
        }
        if(drawMode == DRAW_MODE0)
        {
            for(int i=0; i<3; i++)
            {
                if(i == 0)
                {
                    blendZs[i] = z[i] + blendZ;
                }
                else
                {
                    blendZs[i] = z[i] - blendZ;
                }
                blendXs[i] = x[i] + blendX;
                blendRots[i] = rots[i] + blendRot;
            }
        }
        else if(drawMode == DRAW_MODE2)
        {
            for(int i=0; i<3; i++)
            {
                if(i == 2)
                {
                    blendZs[i] = z[i] + blendZ;
                }
                else
                {
                    blendZs[i] = z[i] - blendZ;
                }
                blendXs[i] = x[i] - blendX;
                blendRots[i] = rots[i] - blendRot;
            }
        }
    }
    else if (drawMode == DRAW_MODE1 || drawMode == DRAW_MODE3)
    {
        blendRot += (BLEND_ROT/BLEND_FRAMES);
        blendX -= (BLEND_X/BLEND_FRAMES);
        blendZ -= (BLEND_Z/BLEND_FRAMES);
        if(blendRot > 0.0 || blendX < 0.0 || blendZ < 0.0)
        {
            blendRot = 0.0;
            blendX = 0.0;
            blendZ = 0.0;
        }
        if (drawMode == DRAW_MODE1)
        {
            for(int i=0; i<3; i++)
            {
                if(i == 0)
                {
                    blendZs[i] = z[i] + blendZ;
                }
                else
                {
                    blendZs[i] = z[i] - blendZ;
                }
                blendXs[i] = x[i] + blendX;
                blendRots[i] = rots[i] + blendRot;
            }
        }
        else if(drawMode == DRAW_MODE3)
        {
            for(int i=0; i<3; i++)
            {
                if(i == 2)
                {
                    blendZs[i] = z[i] + blendZ;
                }
                else
                {
                    blendZs[i] = z[i] - blendZ;
                }
                blendXs[i] = x[i] - blendX;
                blendRots[i] = rots[i] - blendRot;
            }
        }
    }

}

void DrawBlendTexture( bool bDrawBlend = false)
{
    glDisable(GL_CULL_FACE);
    if(bDrawBlend)
    {
        glColor4f(1.0, 1.0, 1.0, ALPHA_VALUE);
    }

    // For texure coord
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    for (int i=0; i<3; i++)
    {
        // The first quad
        glDisable(GL_BLEND);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(blendXs[i], blendYs[i], blendZs[i]);

        glRotatef(blendRots[i], 0.0, 1.0, 0.0);

        glEnableClientState(GL_VERTEX_ARRAY);
        
        glVertexPointer(3, GL_FLOAT, 0, rectVertices);
        
        glTexCoordPointer(2, GL_FLOAT, 0, TEX_COORDS);
        glBindTexture(GL_TEXTURE_2D, texture[(i+1) % MAX_TEXTURE]);

        if(!bDrawBlend)
        {
            // Draw the opaque part
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        }
        else
        {
            // Draw the blend one
            glEnable(GL_BLEND);

            glTranslatef(0, -18, 0); 
 
            glTexCoordPointer(2, GL_FLOAT, 0, REFLECT_TEX_COORDS);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        }

    }


    if(bDrawBlend)
    {
        glColor4f(1.0, 1.0, 1.0, 1.0);
    }
    return;
}



//*****************************************************************************
// Name:            DrawFireworks
// Description:     Draw the fireworks
//                    
//*****************************************************************************
void DrawFireworks( void )
{
    glDisable(GL_DEPTH_TEST);							// Disable Depth Testing
    /* Enable Blending */
    glEnable(GL_BLEND);
    /* Type Of Blending To Perform */
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);

    /* Clear The Screen And The Depth Buffer */
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    // Reset The ModelView Matrix
    glTranslatef(fireX, fireY, 0);

    /* Set pointers to vertices and texcoords */
    glVertexPointer(3, GL_FLOAT, 0, vertices);
    glTexCoordPointer(2, GL_FLOAT, 0, texcoords);

    /* Enable vertices and texcoords arrays */
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glBindTexture(GL_TEXTURE_2D, texture[TEXTURE_FIRE]);
    //glLoadIdentity();
    float curColors[4];
    glGetFloatv(GL_CURRENT_COLOR, curColors);
    /* Modify each of the particles */
    for (loop=0; loop<MAX_PARTICLES; loop++)
    {
        if (particles[loop].active)
        {
            /* Grab Our Particle X Position */
            float x=particles[loop].x;

            /* Grab Our Particle Y Position */
            float y=particles[loop].y;

            /* Particle Z Position + Zoom */
            float z=particles[loop].z+zoom;

            /* Draw The Particle Using Our RGB Values,
            * Fade The Particle Based On It's Life
            */
            glColor4f(particles[loop].r, particles[loop].g, particles[loop].b, particles[loop].life * ALPHA_VALUE);

            /* Top Right */
            texcoords[loop*6 +0][0]=1.0f; texcoords[loop*6 +0][1]=1.0f;
            vertices[loop*6 +0][0]=x+0.5f; vertices[loop*6 +0][1]=y+0.5f; vertices[loop*6 +0][2]=z;

            /* Top Left */
            texcoords[loop*6 +1][0]=0.0f; texcoords[loop*6 +1][1]=1.0f;
            vertices[loop*6 +1][0]=x-0.5f; vertices[loop*6 +1][1]=y+0.5f; vertices[loop*6 +1][2]=z;

            /* Bottom Right */
            texcoords[loop*6 +2][0]=1.0f; texcoords[loop*6 +2][1]=0.0f;
            vertices[loop*6 +2][0]=x+0.5f; vertices[loop*6 +2][1]=y-0.5f; vertices[loop*6 +2][2]=z;

            /* Bottom Right */
            texcoords[loop*6 +3][0]=1.0f; texcoords[loop*6 +3][1]=0.0f;
            vertices[loop*6 +3][0]=x+0.5f; vertices[loop*6 +3][1]=y-0.5f; vertices[loop*6 +3][2]=z;

            /* Top Left */
            texcoords[loop*6 +4][0]=0.0f; texcoords[loop*6 +4][1]=1.0f;
            vertices[loop*6 +4][0]=x-0.5f; vertices[loop*6 +4][1]=y+0.5f; vertices[loop*6 +4][2]=z;
            /* Bottom Left */
            texcoords[loop*6 +5][0]=0.0f; texcoords[loop*6 +5][1]=0.0f;
            vertices[loop*6 +5][0]=x-0.5f; vertices[loop*6 +5][1]=y-0.5f; vertices[loop*6 +5][2]=z;

            /* Move On The X Axis By X Speed */
            particles[loop].x+=particles[loop].xi/(slowdown*1000);
            /* Move On The Y Axis By Y Speed */
            particles[loop].y+=particles[loop].yi/(slowdown*1000);
            /* Move On The Z Axis By Z Speed */
            particles[loop].z+=particles[loop].zi/(slowdown*1000);

            /* Take Pull On X Axis Into Account */
            particles[loop].xi+=particles[loop].xg;
            /* Take Pull On Y Axis Into Account */
            particles[loop].yi+=particles[loop].yg;
            /* Take Pull On Z Axis Into Account */
            particles[loop].zi+=particles[loop].zg;

            /* Reduce Particles Life By 'Fade' */
            particles[loop].life-=particles[loop].fade;

            /* If the particle dies, revive it */
            if (particles[loop].life<0.5f)
            {
                float xi, yi, zi;

                xi=xspeed+(float)((rand()%60)-32.0f);
                yi=yspeed+(float)((rand()%60)-30.0f);
                zi=(float)((rand()%60)-30.0f);
                ResetParticle(loop, col, xi, yi, zi);
            }
        }
    }

    col ++;
    col = col % 12;

    /* Build Quad From A Triangle Strip */
    glDrawArrays(GL_TRIANGLES, 0, 6*MAX_PARTICLES);

    /* Disable texcoords and vertices arrays */
    //glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    //glDisableClientState(GL_VERTEX_ARRAY);


    glColor4f(curColors[0],curColors[1],curColors[2],curColors[3]);
    return;
}


//*****************************************************************************
// Name:            DrawRotCube
// Description:     Draw the rotate cube 
//   
//*****************************************************************************
#define INCLINATION 30
#define UP_SIZE     2
#define ROT_CUBE_Z 	-55.0f
void DrawRotCube( bool bDrawBlend = false)
{

    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, s_cubeVertices);

    // For texure coord
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    if(bDrawBlend)
    {
        glColor4f(1.0, 1.0, 1.0, ALPHA_VALUE);
    }
    for (int i=0; i<6; i++)
    {
        if( !bDrawBlend)
        {
#if REFLECT_EFFECT
            glDisable(GL_BLEND);
#endif
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
            glTranslatef(0.0f, 5.0f + UP_SIZE, ROT_CUBE_Z);
            
            glScalef(1.6f, 0.9f, 1.0f);
            
            glRotatef(INCLINATION, 1.0f,  0.0f, 0.0f);  
            //glRotatef(45, 0.0f,  1.0f, 0.0f);
            //cubeRotate = 0;
            glRotatef(cubeRotate, 0.0f,  0.1f, 0.0f);


            glTexCoordPointer(2, GL_FLOAT, 0, TEX_COORDS );
            if(i < 4)
            {
                if(i < 2)
                {
                    glBindTexture(GL_TEXTURE_2D, texture[0]);
                }
                else
                {
                    glBindTexture(GL_TEXTURE_2D, texture[1]);
                }
            }
            else
            {
                glBindTexture(GL_TEXTURE_2D, texture[3]);
            }
            glDrawArrays(GL_TRIANGLE_STRIP, i*4, 4);
        }
        else
        {
#if REFLECT_EFFECT
            if (i < 4)
            {
                glEnable(GL_BLEND);
                /////////////////////////////////
                //glScalef(-1.0f, -1.0f, 1.0f);
                //glTranslatef(0.0f, 20.0f, 0.0f);
                glMatrixMode(GL_MODELVIEW);
                glLoadIdentity();
                glTranslatef(0.0f, -25.0f + UP_SIZE, ROT_CUBE_Z);
                // For 16:9 screen
                glScalef(1.6f, 0.9f, 1.0f);

                glRotatef(-INCLINATION, 1.0f,  0.0f, 0.0f);  
                //glRotatef(45, 0.0f,  1.0f, 0.0f);
                glRotatef(cubeRotate, 0.0f,  0.1f, 0.0f);

                //////////////////////////////////   
                glTexCoordPointer(2, GL_FLOAT, 0, REFLECT_TEX_COORDS );
                if(i < 2)
                {
                    glBindTexture(GL_TEXTURE_2D, texture[0]);
                }
                else
                {
                    glBindTexture(GL_TEXTURE_2D, texture[1]);
                }
                glDrawArrays(GL_TRIANGLE_STRIP, i*4, 4);
            }
#endif
        }
    }

    if(bDrawBlend)
    {
        glColor4f(1.0, 1.0, 1.0, 1.0);
    }
}




//*****************************************************************************
// Name:            DrawCube
// Description:     Draw the cube 
//   
//*****************************************************************************
void DrawCube( void )
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -50.f);
    // For 16:9 screen
    glScalef(1.6f, 0.9f, 1.0f);
        glRotatef(cubeRotate, 1.0f,  0.0f, 0.0f);
        glRotatef(cubeRotate, 0.0f,  1.0f, 0.0f);  

        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, s_cubeVertices);

        // For texure coord
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glTexCoordPointer(2, GL_FLOAT, 0, TEX_COORDS);

        for (int i=0; i<6; i++)
        {
            //
            glBindTexture(GL_TEXTURE_2D, texture[i%4]);
            glDrawArrays(GL_TRIANGLE_STRIP, i*4, 4);
        }
}


//*****************************************************************************
// Name:            DrawSphereCylinder
// Description:     Draw the Sphere and Cylinder
//   
//*****************************************************************************
void DrawSphereCylinder( bool bDrawBlend = false)
{
    glPushMatrix();
	glLoadIdentity();									// Reset The View
	glTranslatef(0.0f,0.0f,-17);

    //cubeRotate = 0;
    if(bDrawBlend)
    {
        glColor4f(1.0, 1.0, 1.0, ALPHA_VALUE);
    }

    if (drawType == CYLINDER)
    {

#if TEXTURE_Y_ROTATE
        glRotatef(-98 + 180,1.0f,0.0f,0.0f);
        glRotatef(-cubeRotate,0.0f,0.0f,1.0f);
#else
        glRotatef(-98, 1.0f, 0.0f, 0.0f);
        glRotatef(cubeRotate,0.0f,0.0f,1.0f);
#endif

        glDisable(GL_BLEND);
        glTranslatef(0.0f,0.0f,-2.5f);
        
        if( !bDrawBlend)
        {
            glBindTexture(GL_TEXTURE_2D, texture[3 % MAX_TEXTURE]);
            // Draw the opaque part
            quadratic->orientation = GLU_OUTSIDE;
            MRVL_gluCylinder(quadratic, 5.0f, 5.0f, 5.0f, CYLINDER_SLICES, CYLINDER_STACKS);
        }

        glTranslatef(0.0f,0.0f, 0.25f);
        if( !bDrawBlend)
        {
            glBindTexture(GL_TEXTURE_2D, texture[2 % MAX_TEXTURE]);
            // Draw the opaque part
            MRVL_gluCylinderPart(quadratic, 5.01f, 5.01f, 4.5f, CYLINDER_SLICES/4, CYLINDER_STACKS, 1.0/4);
        }
        else
        {
#if TEXTURE_Y_ROTATE
            glTranslatef(0.0f, 0.0f, 10.0f);
#endif
            // Draw the Mirror/blend part
            glEnable(GL_BLEND);
            glScalef(1.0, 1.0, -1.0);
            glTranslatef(0.0f,0.0f, 0.25f);

            glBindTexture(GL_TEXTURE_2D, texture[3 % MAX_TEXTURE]);
            quadratic->orientation = GLU_INSIDE;
            MRVL_gluCylinder(quadratic, 5.0f, 5.0f, 5.0f, CYLINDER_SLICES, CYLINDER_STACKS);
            glTranslatef(0.0f,0.0f, 0.25f);
            glBindTexture(GL_TEXTURE_2D, texture[2 % MAX_TEXTURE]);
            MRVL_gluCylinderPart(quadratic, 5.01f, 5.01f, 4.5f, CYLINDER_SLICES, CYLINDER_STACKS, 1.0/4);
        }
    }
    else
    {
#if TEXTURE_Y_ROTATE
        glRotatef(90,1.0f,0.0f,0.0f);
        glRotatef(-cubeRotate,0.0f,0.0f,1.0f);
#else
        glRotatef(-90,1.0f,0.0f,0.0f);
        glRotatef(cubeRotate,0.0f,0.0f,1.0f);
#endif
        glBindTexture(GL_TEXTURE_2D, texture[3 % MAX_TEXTURE]);
        //glDisable(GL_CULL_FACE);
        quadratic->orientation = GLU_OUTSIDE;
                
#if TEXTURE_Y_ROTATE
        glTranslatef(0.0f,0.0f, -1.0f);
#else
        glTranslatef(0.0f,0.0f, 1.0f);
#endif

            if ( !bDrawBlend)
            {
                // Draw the opaque
                quadratic->orientation = GLU_INSIDE;
		  glScalef(-1.0, 1.0, 1.0);
            }
            else
            {
                // Draw the Mirror
                quadratic->orientation = GLU_OUTSIDE;
                glEnable(GL_BLEND);
                glScalef(-1.0, 1.0, -1.0);
#if TEXTURE_Y_ROTATE
                glTranslatef(0.0f,0.0f, -11.0f);
#else
                glTranslatef(0.0f,0.0f, 11.0f);
#endif
            }
            glBindTexture(GL_TEXTURE_2D, texture[3 % MAX_TEXTURE]);
            // Draw A Sphere With A Radius Of 5 And 64 Longitude And 64 Latitude 
        MRVL_gluSphere(quadratic,5.0f,SPHERE_SLICES, SPHERE_STACKS);    

            glBindTexture(GL_TEXTURE_2D, texture[1 % MAX_TEXTURE]);
        MRVL_gluSpherePart(quadratic,5.01f, SPHERE_SLICES/3, SPHERE_STACKS,  1.0f/3, 0.35f);

#if TEXTURE_Y_ROTATE
        glRotatef(-120,0.0f,0.0f,1.0f);
#else
        glRotatef(120,0.0f,0.0f,1.0f);
#endif
        glBindTexture(GL_TEXTURE_2D, texture[0 % MAX_TEXTURE]);
        MRVL_gluSpherePart(quadratic,5.01f, SPHERE_SLICES/3, SPHERE_STACKS,  1.0f/3, 0.35f);

#if TEXTURE_Y_ROTATE
        glRotatef(-120,0.0f,0.0f,1.0f);
#else
        glRotatef(120,0.0f,0.0f,1.0f);
#endif
            glBindTexture(GL_TEXTURE_2D, texture[2 % MAX_TEXTURE]);
        MRVL_gluSpherePart(quadratic,6.01f, SPHERE_SLICES/3.0f, SPHERE_STACKS ,1.0f/3, 0.22f);    



    }
    if(bDrawBlend)
    {
        glColor4f(1.0, 1.0, 1.0, 1.0);
    }
    glPopMatrix();

}

//*****************************************************************************
// Name:            AppRender
// Description:     Application rendering. Three triangles will be rendered abreast and their color 
//                  change according to different blending function setting.   
//*****************************************************************************
void AppRender(int *UpdatedFlag, int *Index)
{
#if ENABLE_GPU_RENDER
    int i;

    // For the blend texture
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	// Set The Blending Function For Translucency
    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);							// Disable Depth Testing
    glEnable(GL_CULL_FACE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
    if( drawType == CYLINDER || drawType == SPHERE )
    {
        DrawSphereCylinder(false);
        DrawBackground();
        DrawSphereCylinder(true);

    }
    else if (drawType == WAVE_TEXTURE)
    {
        DrawBackground();
        glDisable(GL_CULL_FACE);
        DrawWaveFlag();
    }
    else if (drawType == MOVING_TEXTURE)
    {
        DrawMovingTexture(false);
        DrawBackground();
        DrawMovingTexture(true);
    }
    else if (drawType == BLENDING_TEXTURE)
    {
        DrawBlendTexture(false);
        DrawBackground();
        DrawBlendTexture(true);
    }
    else if (drawType == ROT_CUBE)
    {
        DrawRotCube(false);
        DrawBackground();
        DrawRotCube(true);
    }
    else if (drawType == CUBE)
    {
        DrawCube();
        DrawBackground();

    }
#ifdef DRAW_FIREWORKS
    if(bDrawFireworks)
    {
        DrawFireworks();
    }
#endif

    for( i = 0; i < MAX_MULTISTRM_NUM; i++ ) {
        if(UpdatedFlag[i]) {
            PRINTF_THREAD("[GPU Thread] Channel:%d, PicIndex:%d Fenced!\n", i, Index[i]);
            pGLSendFence(PicFence[i][Index[i]]);
        }
    }

#if USE_VDK     
	vdkSwapEGL(&egl);
#else
   /* Draw it to the screen */
    eglSwapBuffers (eglDisplay, eglWindowSurface);
#endif
#endif
}


#ifndef __LINUX__
//*****************************************************************************
// Name:            WndProc
// Description:     Processes messages for the main window 
//*****************************************************************************
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message) 
    {
		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
            //drawMode ++;
            //drawMode %= BLEND_MODE_NUM;
            drawType += 1;
            drawType %= TYPES_NUM;
			keys[wParam] = TRUE;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}

		case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = FALSE;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}

        case WM_LBUTTONDOWN:
        case WM_CLOSE:
            DestroyWindow (hWnd);
            break;

        case WM_DESTROY:
            PostQuitMessage (0);
            break;

        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
    }

    return 0;
}

//*****************************************************************************
// Name:            MyRegisterClass
// Description:     Registers the window class 
//*****************************************************************************
ATOM MyRegisterClass(HINSTANCE hInstance, LPTSTR szWindowClass)
{
    WNDCLASS    wc;

    wc.style            = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc      = (WNDPROC) WndProc;
    wc.cbClsExtra       = 0;
    wc.cbWndExtra       = 0;
    wc.hInstance        = hInstance;
    wc.hIcon            = NULL;
    wc.hCursor          = 0;
    wc.hbrBackground    = (HBRUSH) GetStockObject(WHITE_BRUSH);
    wc.lpszMenuName     = 0;
    wc.lpszClassName    = szWindowClass;

    return RegisterClass(&wc);
}



//*****************************************************************************
// Name:            InitInstance
// Description:     Save application instance handle and creates main window 
//*****************************************************************************
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
    HWND    hWnd = NULL;

#if defined(WINCE) || defined(UNDER_CE)
    DWORD  style = WS_VISIBLE;
#else
    DWORD  style = WS_VISIBLE | WS_CAPTION | WS_SYSMENU;
#endif

    RECT    rect;

    // Store instance handle in our global variable
    g_hInst = hInstance;        

    //If it is already running, then focus on the window
    hWnd = FindWindow( SAMPLE_WNDCLS_NAME, SAMPLE_WINDOW_NAME);  
    if (hWnd) 
    {
        // set focus to foremost child window
        // The "| 0x01" is used to bring any owned windows to the foreground and
        // activate them.
        SetForegroundWindow((HWND)((ULONG) hWnd | 0x00000001));
        return 0;
    }

    // Register window class
    MyRegisterClass(hInstance, SAMPLE_WNDCLS_NAME );

    // Specify needed client size 
    rect.left   = 0;
    rect.top    = 0;
    rect.right  = WINDOW_WIDTH;
    rect.bottom = WINDOW_HEIGHT;

#if defined(WINCE) || defined(UNDER_CE)

    hWnd = CreateWindow( SAMPLE_WNDCLS_NAME, SAMPLE_WINDOW_NAME, style,
        rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, 
        NULL, NULL, hInstance, NULL );

#else

    // Get window rect according to client rect    
    AdjustWindowRect(&rect, style, FALSE);

    // Create main application window
    hWnd = CreateWindow(SAMPLE_WNDCLS_NAME, SAMPLE_WINDOW_NAME, style,
        CW_USEDEFAULT, CW_USEDEFAULT, rect.right - rect.left, rect.bottom - rect.top, 
        NULL, NULL, hInstance, NULL );

#endif

    if (!hWnd)
    {   
        return FALSE;
    }

    // Show main window
    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    EGLint      majorVersion;
    EGLint      minorVersion;
    EGLint      attrib_list[] = {   EGL_RED_SIZE,           5,
                                    EGL_GREEN_SIZE,         6,
                                    EGL_BLUE_SIZE,          5,
                                    EGL_ALPHA_SIZE,         0,
                                    EGL_DEPTH_SIZE,         16,
                                    EGL_RENDERABLE_TYPE,    EGL_OPENGL_ES_BIT,
                                    EGL_SURFACE_TYPE,       EGL_WINDOW_BIT,
                                    EGL_NONE,
                                };

    EGLConfig   configArray[32];
    EGLint      maxConfigs  = 32;        
    EGLint      numConfigs  = 0;    
    EGLConfig   config      = 0;
    EGLint      i           = 0;
    
	// Get EGL display
    eglDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if(eglDisplay == EGL_NO_DISPLAY || eglGetError() != EGL_SUCCESS)
    {
        return FALSE;
    }

	// Initialize EGL
    eglInitialize(eglDisplay, &majorVersion, &minorVersion);
    if(eglGetError() != EGL_SUCCESS)
    {
        return FALSE;
    }

	// Choose all possible configs
    if(!eglChooseConfig(eglDisplay, attrib_list, configArray, maxConfigs, &numConfigs))
    {
        return FALSE;
    }

    // Pick out the config most close to our requirement 
    config = configArray[0];
    for(i = 0; i < numConfigs; i++)
    {
        EGLint  red_size      = 0;
        EGLint  green_size    = 0;
        EGLint  blue_size     = 0;
        EGLint  alpha_size    = 0;       
        EGLint  depth_size    = 0;       

        eglGetConfigAttrib(eglDisplay, configArray[i], EGL_RED_SIZE,        &red_size);
        eglGetConfigAttrib(eglDisplay, configArray[i], EGL_GREEN_SIZE,      &green_size);
        eglGetConfigAttrib(eglDisplay, configArray[i], EGL_BLUE_SIZE,       &blue_size);
        eglGetConfigAttrib(eglDisplay, configArray[i], EGL_ALPHA_SIZE,      &alpha_size);
        eglGetConfigAttrib(eglDisplay, configArray[i], EGL_DEPTH_SIZE,      &depth_size);

        if( (red_size   == 5)  && 
            (green_size == 6)  && 
            (blue_size  == 5)  &&
            (alpha_size == 0)  &&
            (depth_size == 16) )
        {
            config = configArray[i];
            break;
        }
    }  


    if(!eglBindAPI(EGL_OPENGL_ES_API))
    {
        return FALSE;
    }

	// Create EGL rendering context
    eglContext = eglCreateContext(eglDisplay, config, NULL, NULL);
    if(eglContext == EGL_NO_CONTEXT || eglGetError() != EGL_SUCCESS)
    {
        return FALSE;
    }

	// Create EGL window surface
    eglWindowSurface = eglCreateWindowSurface(eglDisplay, config, hWnd, NULL);
    if(eglWindowSurface == EGL_NO_SURFACE || eglGetError() != EGL_SUCCESS)
    {
        return FALSE;
    }
    
	// Attach the EGL rendering context to EGL surfaces
    eglMakeCurrent(eglDisplay, eglWindowSurface, eglWindowSurface, eglContext);
    if(eglGetError() != EGL_SUCCESS)
    {
        return FALSE;
    }

    return TRUE;
}


//*****************************************************************************
// Name:            WinMain
// Description:     Application entry 
//*****************************************************************************
//int WINAPI WinMain (
//    HINSTANCE   hInstance, 
//    HINSTANCE   hPrevInstance, 
//    LPTSTR      lpCmdLine, 
//    int         nCmdShow)
//{
int WINAPI WinMain (HINSTANCE hInstance,
                    HINSTANCE hPrevInstance,
                    PSTR szCmdLine,
                    int  nCmdShow)
{
    MSG msg;

    // Perform application initialization:
    if( !InitInstance (hInstance, nCmdShow) ) 
    {
        return FALSE;
    }

    // Application initialization
    if( !AppInit() )
    {
        return FALSE;
    }

    while( TRUE )
    {
        if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
        {
            if( WM_QUIT == msg.message )
                break;
            TranslateMessage( &msg );
            DispatchMessage ( &msg );
        }
        else
        {
            if (keys[' '])
            {
                drawType--;
                drawType %= TYPES_NUM;
                if(drawType == BLENDING_TEXTURE)
                {
                    drawMode ++;
                    drawMode %= BLEND_MODE_NUM;
                }
                else if (drawType == MOVING_TEXTURE)
                {
                    if (drawMode == DRAW_MODE0)
                    {
                        drawMode = (unsigned int )((MovingRot /90) + 1);
                    }
                    else
                    {
                        if( (drawMode + 1)  == MOVING_MODE_NUM )
                        {
                            drawMode += 2;
                            if(MovingRot >= 360.0f)
                            {
                                MovingRot -= 360.0f;
                            }
                        }
                        else
                        {
                            drawMode ++;
                        }
                        drawMode %= MOVING_MODE_NUM;
                    }
                }
                keys[' '] = false;
            }
            if (keys[VK_DOWN])
            {
                drawType--;
                drawType %= TYPES_NUM;
                if (drawType == MOVING_TEXTURE)
                {
                    drawMode = DRAW_MODE0;
                }
                keys[VK_DOWN] = false;
            }
            //if (keys[VK_LEFT])
            //{
            //    drawType -= 1;
            //             drawType %= NUM_TYPES;
            //}
            //if (keys[VK_RIGHT])
            //{
            //    drawType += 1;
            //             drawType %= NUM_TYPES;
            //}
            // Application update
            AppUpdate();

            // Application render
            AppRender();
        }
    }

    // Application release
    AppDeInit();

    return msg.wParam;
}

#else	/* __LINUX__ */

#if ((defined BMP_TEST) || (defined YUV_TEST))
#if USE_VDK

/*******************************************************************************
** Initialization code.
*/
int main(int argc, char * argv[])
{
    int run, i;
    // Application initialization
    if( !AppInit() )
    {
        return NULL;
    }
    if(!LoadTexture(TEXTURE_BACKGROUND, sBackgroundFileName, 0, 0, 1)) {
        IPP_Printf("load background error\n");
        return NULL;
    }
    if(!LoadTexture(TEXTURE_FIRE, sParticleFileName, 0, 0, 1)) {
        IPP_Printf("load particle error\n");
        return NULL;		
    }
    /* Main loop. */
    for (run = 1; run;)
    {
        vdkEvent event;

        /* Get an event. */
        if (vdkGetEvent(egl.window, &event))
        {
            /* Test for keyboard event. */
            if ((event.type == VDK_KEYBOARD)
                &&  event.data.keyboard.pressed
                )
            {
                /* press any key to switch */
                drawType += 1;
                drawType %= TYPES_NUM;
                /* Dispatch on scancode. */
                switch (event.data.keyboard.scancode)
                {
                case VDK_ESCAPE:
                    /* Escape - quit. */
                    run = 0;
                    break;

                case VDK_RIGHT:
                    break;

                case VDK_LEFT:
                    break;

                case VDK_COMMA:
                case VDK_PAD_HYPHEN:
                    break;

                case VDK_PERIOD:
                case VDK_PAD_PLUS:
                    break;

                case VDK_SPACE:
                    drawType--;
                    drawType %= TYPES_NUM;
                    if(drawType == BLENDING_TEXTURE)
                    {
                        drawMode ++;
                        drawMode %= BLEND_MODE_NUM;
                    }
                    else if (drawType == MOVING_TEXTURE)
                    {
                        if (drawMode == DRAW_MODE0)
                        {
                            drawMode = (unsigned int )((MovingRot /90) + 1);
                        }
                        else
                        {
                            if( (drawMode + 1)  == MOVING_MODE_NUM )
                            {
                                drawMode += 2;
                                if(MovingRot >= 360.0f)
                                {
                                    MovingRot -= 360.0f;
                                }
                            }
                            else
                            {
                                drawMode ++;
                            }
                            drawMode %= MOVING_MODE_NUM;
                        }
                    }
                    break;
                case VDK_DOWN:

                    drawType--;
                    drawType %= TYPES_NUM;
                    if (drawType == MOVING_TEXTURE)
                    {
                        drawMode = DRAW_MODE0;
                    }
                    break;

                default:
                    break;
                }



            }

            /* Application closing. */
            else if (event.type == VDK_CLOSE)
            {
                /* Quit. */
                run = 0;
            }

        }

        else
        {
            // Application update
#ifdef BMP_TEST        
			UpdateTexture(&info[i%MAX_TEXTURE], i%MAX_TEXTURE);
            AppUpdate();
#else
			UpdateTexture(NULL, i%MAX_TEXTURE);
            AppUpdate();
#endif        

            // Application render
            AppRender();
        }
    }

    /* Close the VDK & EGL. */
    AppDeInit();

    /* Success. */
    return 0;
}

#else
void* WorkThread(void *pcnt)
{
	int i;
	int cnt = *(int*)pcnt;
    unsigned long long start, end;
    // Application initialization
    if( !AppInit() )
    {
        return NULL;
    }
	if(!LoadTexture(TEXTURE_BACKGROUND, sBackgroundFileName, 0, 0, 1)) {
		IPP_Printf("load background error\n");
		return NULL;
	}
	if(!LoadTexture(TEXTURE_FIRE, sParticleFileName, 0, 0, 1)) {
		IPP_Printf("load particle error\n");
		return NULL;		
	}
    start = GetTickCount();
   
    for(i=0; i < cnt; i++ )
    {
        // Application update
#ifdef BMP_TEST        
		UpdateTexture (&info[i%MAX_TEXTURE], i%MAX_TEXTURE);
        AppUpdate();
#else
		UpdateTexture(NULL, i%MAX_TEXTURE);
		AppUpdate();
#endif        

        // Application render
        AppRender();
    }    
    end = GetTickCount();

    // Application release
    AppDeInit();
    IPP_Printf("FPS: %.2f\n", (float)cnt*1000000./(float)(end - start));	

}

//*****************************************************************************
// Name:            main
// Description:     Application entry 
//*****************************************************************************
int main()
{ 	
	pthread_t thrd;
	int cnt = 500, i;
	for(drawType=CUBE;drawType<TYPES_NUM; drawType++) {
//				if(drawType == 1)
//					drawType++;
		
		IPP_Printf("*********** Draw type = %d\n", drawType);
		pthread_create(&thrd, NULL, WorkThread, (void*)&cnt);
		pthread_join(thrd, NULL);	
	}
    return 0;
}


#endif
#else	/* !(BMP_TEST || YUV_TEST) */
#define MAX(x,y)		(((x)>(y))?(x):(y))
#define MIN(x,y)		(((x)<(y))?(x):(y))

#ifndef KEYCODE_BACK
#define KEYCODE_BACK    0x00000004
#define KEYCODE_ENTER   0x00000042
#define KEYCODE_1       0x00000008
#define KEYCODE_2       0x00000009
#define KEYCODE_SPACE   0x00000012
#define KEYCODE_CALL    0x00000005
#define KEYCODE_DEL     0x00000043
#endif

int KeyPressHandler(int totalframes, int keycode) 
{
#if ENABLE_GPU_RENDER
#if ANDROID && USE_APK
    switch(keycode) {
        case KEYCODE_BACK:
        case KEYCODE_DEL:
            IPP_Printf("Del/Back is pressed!\n");
            bStop = 1;
            KillCompiz((const char*)"Destroy");
            break;
        case KEYCODE_CALL:
        case KEYCODE_ENTER:
            IPP_Printf("CALL/Enter is pressed!\n");
            drawType += 1;
            drawType %= TYPES_NUM;
            break;
        case KEYCODE_1:
            IPP_Printf("KEY_1 is pressed!\n");
            if (drawType == MOVING_TEXTURE) {
                drawMode = DRAW_MODE0;       
            }
            break;
        case KEYCODE_2:
            IPP_Printf("KEY_2 is pressed!\n");
            bDrawFireworks = !bDrawFireworks;
            break;
        case KEYCODE_SPACE:
            IPP_Printf("SPACE is pressed!\n");
            if(drawType == BLENDING_TEXTURE) {
                drawMode ++;
                drawMode %= BLEND_MODE_NUM;
            } else if (drawType == MOVING_TEXTURE) {
                if (drawMode == DRAW_MODE0) {
                    drawMode = (unsigned int )((MovingRot /90) + 1);
                } else {
                    if( (drawMode + 1)  == MOVING_MODE_NUM ) {
                        drawMode += 2;
                        if(MovingRot >= 360.0f) {
                            MovingRot -= 360.0f;
                        }
                    } else {
                        drawMode ++;
                    }
                    drawMode %= MOVING_MODE_NUM;
                }
            }
            break;
        default:
            break;
    }
#else
    if (bSupportX) {
#if USE_VDK
        /* support keyboard */
        vdkEvent event;

        /* Get an event. */
        if (vdkGetEvent(egl.window, &event)) {
            /* Test for keyboard event. */
            if ((event.type == VDK_KEYBOARD) &&  event.data.keyboard.pressed ) {
                /* Dispatch on scancode. */
                switch (event.data.keyboard.scancode) {
                case VDK_ESCAPE:
                case VDK_DELETE:
                    /* Escape - quit. */
	            IPP_Printf("Del/Esc pressed!\n");
                    bStop = 1;
                    break;

                case VDK_ENTER:
                    /* press ENTER key to switch */
		    IPP_Printf("Enter pressed!\n");
                    drawType += 1;
                    drawType %= TYPES_NUM;
                    break;

                case VDK_1:
                    /* press 1 key to specal swtich draw mode only for MOVING TEXTURE */
                    if (drawType == MOVING_TEXTURE) {
                        drawMode = DRAW_MODE0;
                    }
                    break;

                case VDK_2:
                    /* press 2 key to enable or disable fireworks effect */
                    bDrawFireworks = !bDrawFireworks;
                    break;

                case VDK_SPACE:
                    /* press SPACE key to swtich draw modes for MOVING TEXTURE and BLENDING TEXTURE */
                    if(drawType == BLENDING_TEXTURE) {
                        drawMode ++;
                        drawMode %= BLEND_MODE_NUM;
                    } else if (drawType == MOVING_TEXTURE) {
                        if (drawMode == DRAW_MODE0) {
                            drawMode = (unsigned int )((MovingRot /90) + 1);
                        } else {
                            if( (drawMode + 1)  == MOVING_MODE_NUM ) {
                                drawMode += 2;
                                if(MovingRot >= 360.0f) {
                                    MovingRot -= 360.0f;
                                }
                            } else {
                                drawMode ++;
                            }
                            drawMode %= MOVING_MODE_NUM;
                        }
                    }
                    break;

                case VDK_COMMA:
                case VDK_PAD_HYPHEN:
                    break;

                case VDK_PERIOD:
                case VDK_PAD_PLUS:
                    break;
                case VDK_DOWN:
                    break;

                default:
                    break;
                }
            } else if (event.type == VDK_CLOSE) {
                /* Quit. */
                bStop = 1;
            }
        }
#endif
    } else {
        if(totalframes % 300 == 0) {
            drawType ++;
            drawType %= TYPES_NUM;
        }

        if(totalframes %75 == 0) {
            if(drawType == BLENDING_TEXTURE) {
                drawMode ++;
                drawMode %= BLEND_MODE_NUM;
            } else if (drawType == MOVING_TEXTURE) {
                if (drawMode == DRAW_MODE0) {
                    drawMode = (unsigned int )((MovingRot /90) + 1);
                } else {
                    if( (drawMode + 1)  == MOVING_MODE_NUM ) {
                        drawMode += 2;
                        if(MovingRot >= 360.0f) {
                            MovingRot -= 360.0f;
                        }
                    } else {
                        drawMode ++;
                    }
                    drawMode %= MOVING_MODE_NUM;
                }
            }
        }
    }
#endif
#endif
    return 0;
}

int GPUInitGLTexture()
{
    int x;

    glEnable(GL_TEXTURE_2D);

    /* Depth buffer setup */
    glClearDepthf(1.0f);

    /* Enables Depth Testing */
    glEnable(GL_DEPTH_TEST);

    /* The Type Of Depth Test To Do */
    glDepthFunc(GL_LEQUAL);

    glClearColor(0.0f, 0.0f, 0.0f, ALPHA_VALUE);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glShadeModel(GL_SMOOTH);
    /* Really Nice Point Smoothing */
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);

    // For the blend texture
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);					// Set The Blending Function For Translucency

    /* init texture IDs to invalid texture id */
    for(x=0; x<MAX_TEXTURE; x++) {
        texture[x] = INVALID_TEXID;
    }

#ifdef DRAW_BACKGROUND
    if(!LoadTexture(TEXTURE_BACKGROUND, sBackgroundFileName, 0, 0, 1)) {
        IPP_Printf("load background error\n");
        return -1;
    }
    if(!LoadTexture(TEXTURE_MARVELL, sMarvellFileName, 0, 0, 1)) {
        IPP_Printf("load marvell log error\n");
        return -1;
    }
#endif
    if(!LoadTexture(TEXTURE_FIRE, sParticleFileName, 0, 0, 1)) {
        IPP_Printf("load particle error\n");
        return -1;		
    }

    return 0;
}

int GPUDeInitGLTexture()
{
    UnloadTexture();

    return 0;
}

int CreateGPURes()
{
    int x, y;

    DEMO_PRINTF("Entering:%s\n", __FUNCTION__);

    if (GetFuncPointer() < 0) {
        IPP_Printf("Get GPU Function pointer failed\n");
        return -1;
    }

#if !ANDROID
    WindowReshape(0, 0, screenWidth, screenHeight);
#endif

    GPUInitGLTexture();

    // Init animation parameter
    cubeRotate   = 0.0f;

    /////////////////////////////////////////////
    /* Loop Through The X Plane */
    for (x=0; x<(WAVE_SLICES + 1); x++)
    {
        /* Loop Through The Y Plane */
        for (y=0; y<(WAVE_SLICES + 1); y++)
        {
            /* Apply The Wave To Our Mesh */
            points[x][y][0]=(GLfloat)((x/((WAVE_SLICES + 1)/9.0f))-4.5f);
            points[x][y][1]=(GLfloat)((y/((WAVE_SLICES + 1)/9.0f))-4.5f);
            points[x][y][2]=(GLfloat)(sin((((x/((WAVE_SLICES + 1)/9.0f))*40.0f)/360.0f)*3.141592654*2.0f));
        }
    }
    ////////////////////////////////////
    // init particles
    initParticles();

    ////////////////////////////////////
    // for quadric
    quadratic=MRVL_gluNewQuadric();		            // Create A Pointer To The Quadric Object (Return 0 If No Memory) (NEW)
    //MRVL_gluQuadricNormals(quadratic, GLU_SMOOTH);// Create Smooth Normals (NEW)
    MRVL_gluQuadricTexture(quadratic, GL_TRUE);		// Create Texture Coords (NEW)

    for( x = 0; x < MAX_MULTISTRM_NUM; x++ ) {
        for( y = 0; y < PICTURE_BUF_NUM; y++ ) {
            PicFence[x][y] = pGLCreateFence();
        }
    }

    GetPerfCounter(&dequeue_pic_perf_index, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount);
    ResetPerfCounter(dequeue_pic_perf_index);

    GetPerfCounter(&enqueue_pic_perf_index, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount);
    ResetPerfCounter(enqueue_pic_perf_index);

    IPP_GetPerfCounter(&nPerfIndex, IPP_TimeGetTickCount, IPP_TimeGetTickCount);
    IPP_ResetPerfCounter(nPerfIndex);

    if (-1 == nPerfIndex) {
        IPP_Printf("Get counter failed\n");
        return -1;
    }

    for( x = 0; x < MAX_MULTISTRM_NUM; x++ ) {
        pPicGrp[x]      = NULL;
        pPrevPicGrp[x]  = NULL;
    }

    gpu_start = IPP_TimeGetTickCount();

    DEMO_PRINTF("Exit:%s\n", __FUNCTION__);

    return 0;
}


int DestroyGPURes()
{
#if ENABLE_GPU_RENDER
    int i;
    int x, y;

	UnloadTexture();

    /* to triggler reload texture next time */
	for (i = 0; i < MAX_TEXTURE; i++)
	{
		texture[i] = INVALID_TEXID;
	}

    for( x = 0; x < MAX_MULTISTRM_NUM; x++ ) {
        for( y = 0; y < PICTURE_BUF_NUM; y++ ) {
            pGLDestroyFence(PicFence[x][y]);
        }
    }

    g_Comp_Tot_DeviceTime[IPP_VIDEO_INDEX] = IPP_GetPerfData(nPerfIndex);

    PRINTF_THREAD("[GPU Thread] GPU render:%ld\n", g_Comp_Tot_DeviceTime[IPP_VIDEO_INDEX]);

    IPP_FreePerfCounter(nPerfIndex);
    FreePerfCounter(enqueue_pic_perf_index);
    FreePerfCounter(dequeue_pic_perf_index);
#endif

    return 0;
}

void WindowReshape(int x, int y, int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
#if ANDROID
    MRVL_gluPerspective(55.0f, width*1.0f/height, 0.1f, 500.f);
#else
    MRVL_gluPerspective(45.0f, WINDOW_WIDTH*1.0f/WINDOW_HEIGHT, 0.1f, 500.f);
#endif

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glViewport(x, y, width, height);
}


int CreateWindowSystem( void )
{
#if ENABLE_GPU_RENDER
#ifdef __LINUX__
    const EGLint attrib_list[] =
    {
        EGL_RED_SIZE,       8,
        EGL_GREEN_SIZE,     8,
        EGL_BLUE_SIZE,      8,
        EGL_ALPHA_SIZE,     EGL_DONT_CARE,
        EGL_DEPTH_SIZE,     16,
        EGL_STENCIL_SIZE,    EGL_DONT_CARE,
#ifdef MSAA
        EGL_SURFACE_TYPE,    EGL_WINDOW_BIT,  EGL_SAMPLES,  2,
#else
        EGL_SURFACE_TYPE,    EGL_WINDOW_BIT,
#endif
        EGL_NONE,
    };

#if USE_VDK
    if (screenWidth <= 0 || screenHeight <= 0)
    {
        /* Default is full screen mode */

        /* Initialize the VDK. */
        if (egl.vdk == NULL)
        {
            egl.vdk = vdkInitialize();
            if (egl.vdk == NULL)
            {
                /* Error. */
                IPP_Printf("vdkInitialize errors \n");
                return 0;
            }

        }

        if (egl.display == NULL)
        {
            /* Get the VDK display. */
            egl.display = vdkGetDisplay(egl.vdk);
            if (egl.display  == NULL)
            {
                /* Error. */
                IPP_Printf("vdkGetDisplay errors \n");
                return 0;
            }

        }
        /* Get the screen Width and Height */
        vdkGetDisplayInfo(egl.display, &screenWidth, &screenHeight, NULL, NULL, NULL);

        /* Setup the USE_VDK and EGL. */
        if (!vdkSetupEGL(-1, -1, screenWidth, screenHeight, attrib_list, NULL, NULL, &egl))
        {
            /* Error. */
            return 0;
        }
    }

    else
    {
        /* Setup the USE_VDK and EGL. */
        if (!vdkSetupEGL(-1, -1, screenWidth, screenHeight, attrib_list, NULL, NULL, &egl))
        {
            /* Error. */
            return 0;
        }
    }
    IPP_Printf("Resolution = %dx%d\n", screenWidth, screenHeight);
    /* Set window title. */
    vdkSetWindowTitle(egl.window, "OES Vmeta Compiz Demo Window");

    /* Show the window. */
    vdkShowWindow(egl.window);


#else

    EGLint      numConfigs;
    EGLint      majorVersion;
    EGLint      minorVersion;
    EGLConfig   config;
    int         width, height;

#if ANDROID 
#if !USE_APK
    nativeWin = android_createDisplaySurface();
    // Get EGL display
    eglDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    
    screenWidth = 480;
    screenHeight= 640;
#endif
#else
    EGLNativeDisplayType display = NULL;
    display = fbGetDisplay();
    fbGetDisplayGeometry(display, &width, &height);

    if ((screenWidth <= 0 || screenHeight <= 0) || 
        (width < screenWidth || height < screenHeight)) {
        //  full screen mode
        screenWidth = width;
        screenHeight = height;
        // Init native window
        nativeWin = fbCreateWindow(display, 0, 0, screenWidth, screenHeight);
    } else {
        // Init native window
        nativeWin = fbCreateWindow(display, (width-screenWidth)/2, (height -screenHeight)/2, screenWidth, screenHeight);

    }
    // Get EGL display
    eglDisplay = eglGetDisplay(display);
#endif

    if(!nativeWin)
    {
        return 0;
    } 

    if(eglDisplay == EGL_NO_DISPLAY || eglGetError() != EGL_SUCCESS)
    {
        return 0;
    }

    // Initialize EGL
    eglInitialize(eglDisplay, &majorVersion, &minorVersion);
    if(eglGetError() != EGL_SUCCESS)
    {
        return 0;
    }

    // Choose EGL config
    if(!eglChooseConfig(eglDisplay, attrib_list, &config, 1, &numConfigs))
    {
        return 0;
    }

    // Create EGL rendering context
    eglContext = eglCreateContext(eglDisplay, config, NULL, NULL);
    if(eglContext == EGL_NO_CONTEXT || eglGetError() != EGL_SUCCESS)
    {
        return 0;
    }

    // Create EGL window surface
    eglWindowSurface = eglCreateWindowSurface(eglDisplay, config, nativeWin, NULL);
    if(eglWindowSurface == EGL_NO_SURFACE || eglGetError() != EGL_SUCCESS)
    {
        return 0;
    }

    // Attach the EGL rendering context to EGL surfaces
    eglMakeCurrent(eglDisplay, eglWindowSurface, eglWindowSurface, eglContext);
    if(eglGetError() != EGL_SUCCESS)
    {
        return 0;
    }       
#endif
    // Check vendor, renderer and version
#if (defined BMP_TEST || defined YUV_TEST)
    IPP_Printf("Vendor   : %s\n", glGetString(GL_VENDOR));
    IPP_Printf("Renderer : %s\n", glGetString(GL_RENDERER));
    IPP_Printf("Version  : %s\n", glGetString(GL_VERSION));
#endif    	
#endif	/* __LINUX__ */

#endif
    return 1;
}



int DestroyWindowSystem( void )
{
#if ENABLE_GPU_RENDER
#if USE_VDK
    vdkFinishEGL(&egl);
#else
    // Destroy all EGL resources 
    eglMakeCurrent(eglDisplay, NULL, NULL, NULL);
    eglDestroyContext(eglDisplay, eglContext);
    eglDestroySurface(eglDisplay, eglWindowSurface);
    eglTerminate(eglDisplay);
#endif
#endif

    return 0;
}


int DoGPURender()
{
    IppVmetaPicture *pPic = NULL;
	int bReached, id, i;
    int updated = 0;

    /* Make sure all GPU holded resource can be safely released */
    if (bStop && nActiveStreamCnt) {
        /* Step 1: Recycle previous displayed pic */
        for( i = 0; i < nTotalStreamCnt; i++ ) {
            if (pPrevPicGrp[i]) {
                int index = (int)pPrevPicGrp[i]->pUsrData0;

#if ENABLE_GPU_RENDER
                pGLWaitFence(PicFence[i][index]);
#endif
                while(BufQueue_EnQueue(&PicResPool[i], pPrevPicGrp[i]) < 0) {
                    /* Do nothing */
                    PRINTF_THREAD("[GPU Thread] Enqueue PrevPic:p Failed!\n", pPrevPicGrp[id]);
                }
                pPrevPicGrp[i] = NULL;
            }
        }

        /* Step 2: Clear SortedList */
        while(1) {
            SortedList_TryDeList(&SortList, (BufType**)&pPic);
            if (NULL == pPic) {
                break;
            }

            id = (int)pPic->pUsrData2;
            while(BufQueue_EnQueue(&PicResPool[id], (BufType*)pPic) < 0) {
                /* Do nothing */
                PRINTF_THREAD("[GPU Thread] Enqueue Pic:%p Failed!\n", pPicGrp[id]);
            }
        }

        return 0;
    }

    /* All decoder threads are destroyed, we can exit GPU thread safety */
    if (0 == nActiveStreamCnt) {
        return -1;
    }

    memset(ChannelUpdated, 0, MAX_MULTISTRM_NUM*sizeof(int));

    IPP_EventWait(pEventSuspend, INFINITE_WAIT, NULL);
    IPP_EventSet(pEventSuspend);

    PRINTF_THREAD("[GPU Thread] Before TimedWaitBuffer SortList:%p, thresh:%d, count:%d\n", 
        &SortList, SortList.thresh, SortList.count);
    StartCounter(dequeue_pic_perf_index);
    bReached = SortedList_TimedWaitBuffer(&SortList, 1000);
    StopCounter(dequeue_pic_perf_index);
    PRINTF_THREAD("[GPU Thread] After TimedWaitBuffer SortList:%p, thresh:%d, count:%d, Reached:%d\n", 
        &SortList, SortList.thresh, SortList.count, bReached);

    if (!bReached) {
        return 1;
    }

    IPP_StartPerfCounter(nPerfIndex);
    for( i = 0; i < nActiveStreamCnt; i++ ) {
        /* Here we use TryDelist, because the TimedWait may have some async with decoder */
        PRINTF_THREAD("[GPU Thread] Before TryGet SortList:%p\n", &SortList);
        SortedList_TryDeList(&SortList, (BufType**)&pPic);
        PRINTF_THREAD("[GPU Thread] After TryGet SortList:%p\n", &SortList);
        if (!pPic) {
            continue;
        }
        id = (int)pPic->pUsrData2;

        PRINTF_THREAD("[GPU Thread] Channel %d: Have recieved %d frames!\n", id, (int)pPic->pUsrData3);
		
	    /* Update Texture */
        updated += UpdateTexture(pPic, id);

        if (pPicGrp[id]) {
            int index = (int)pPicGrp[id]->pUsrData0;
            /* Overlap the previous frame with the same id */
            PRINTF_THREAD("[GPU Thread] Before Enqueue PicResPool[%d] pic_id=%d\n", id, index);
            StartCounter(enqueue_pic_perf_index);
            while(BufQueue_EnQueue(&PicResPool[id], (BufType*)pPicGrp[id]) < 0) {
                /* Do nothing */
                PRINTF_THREAD("[GPU Thread] Enqueue Pic:%p Failed!\n", pPicGrp[id]);
            }
            StopCounter(enqueue_pic_perf_index);
            PRINTF_THREAD("[GPU Thread] After Enqueue PicResPool[%d] pic_id=%d\n", id, index);
            pPicGrp[id] = NULL;
        }
        pPicGrp[id] = pPic;
        PicIndex[id] = (int)pPic->pUsrData0;
        ChannelUpdated[id] = 1;
    }
	
	vframes += updated;
    if (30 == gframes) {
        unsigned long long now = IPP_TimeGetTickCount();
        gfps = gframes*1000000.f/(now - gpu_start);
        vfps = vframes*1000000.f/((now - gpu_start)*validTextureNum[drawType]);

        IPP_Printf("Video FPS:%.2f, 3D FPS:%.2f\n", vfps, gfps);
        
        gframes = 0;
        vframes = 0;
        gpu_start = IPP_TimeGetTickCount();
    }

    /* The keycode here is invalid */
    KeyPressHandler(totalframes++, 0);

#if GPU_FPS
    if(!nGPUExpectedTick)  {
        nGPUArrivedTick = nGPUExpectedTick = GetTickCount();
    } else  {
        nGPUArrivedTick = GetTickCount();
    }

    /* kenter: The logic here is stange, if Arrived earlier, not update? */
    // It should be Arrive later, we skip it.
    if(nGPUArrivedTick >= nGPUExpectedTick ) {
        AppUpdate();

        while(nGPUExpectedTick < nGPUArrivedTick) {
	   		nGPUExpectedTick += nGPURate;
	   	}
    }
#else
    AppUpdate();
#endif
    gPerfCT.ulDeviceFrameNumber2++;

    if (updated) {
        AppRender(ChannelUpdated, PicIndex);
        for( i = 0; i < nTotalStreamCnt; i++ ) {
            if (ChannelUpdated[i]) {
                PRINTF_THREAD("[GPU Thread] Channel:%d updated!\n", i);
                if (pPrevPicGrp[i]) {
                    int index = (int)pPrevPicGrp[i]->pUsrData0;

                    PRINTF_THREAD("[GPU Thread] Channel:%d, PicIdx:%d need recycle!\n", i, index);
#if ENABLE_GPU_RENDER
                    pGLWaitFence(PicFence[i][index]);
#endif
                    PRINTF_THREAD("[GPU Thread] Before Enqueue PicResPool[%d] pic_id=%d\n", i, index);
                    StartCounter(enqueue_pic_perf_index);
                    while(BufQueue_EnQueue(&PicResPool[i], pPrevPicGrp[i]) < 0) {
                        /* Do nothing */
                        PRINTF_THREAD("[GPU Thread] Enqueue PrevPic:p Failed!\n", pPrevPicGrp[id]);
                    }
                    StopCounter(enqueue_pic_perf_index);
                    PRINTF_THREAD("[GPU Thread] After Enqueue PicResPool[%d] pic_id=%d\n", i, index);   
                    pPrevPicGrp[i] = NULL;
                }
                pPrevPicGrp[i] = pPicGrp[i];
                pPicGrp[i] = NULL;
            }
        }

        gframes++;
        gPerfCT.ulDeviceFrameNumber++;
    }

    IPP_StopPerfCounter(nPerfIndex);

    return 2;
}


void GPUWorkThread(int dummy)
{
    IPP_Printf("[GPU Thread] Created.\n");

    if (1 != CreateWindowSystem()) {
        IPP_Printf("Failed to create window system!\n");
        return ;
    }

    if (CreateGPURes() < 0) {
        IPP_Printf("Failed to create GPU Res!\n");
        return ;
    }

    while(1) {
        if (DoGPURender() < 0) {
            break;
        }
    }

    DestroyGPURes();

    DestroyWindowSystem();
    PRINTF_THREAD("[GPU Thread] Exited: %s!\n", __FUNCTION__);
}

#endif	/* (BMP_TEST || YUV_TEST) */
#endif	/* __LINUX__ */

/* EOF */

