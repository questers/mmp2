#ifndef __VDEC_WRAPPER_H__
#define __VDEC_WRAPPER_H__

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_MULTISTRM_NUM 		        6
#define STREAM_BUF_SIZE                 (1024 * 1024) /*must equal to or greater than 64k*/
#define STREAM_BUF_NUM                  4
#define PICTURE_BUF_NUM                 3

int compiz_init(const char *argv);
int compiz_deinit();

extern int g_Comp_Frame_Num[2][MAX_MULTISTRM_NUM];
extern long long g_Comp_Total_Time[2][MAX_MULTISTRM_NUM];
extern long long g_Comp_Tot_CPUTime[2][MAX_MULTISTRM_NUM];
extern long long g_Comp_Tot_DeviceTime[2];

#ifdef __cplusplus
}
#endif

#endif