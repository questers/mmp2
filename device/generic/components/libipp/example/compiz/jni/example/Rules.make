
# T: tool chain selection; mavell gcc / arm gcc
#    valuse: -gcc
# CPU_TYPE: CFLAGS selection; -mcpu=iwmmxt -mtune=iwmmxt / -mcpu=xscale -mtune=xscale
#    values: xsc_linux
# PGO_FLAGS: pgo switch; 
#    values: -lgcov
#    if PGO, toolchain should be the same with codec lib build
# USE_STATICLIB: use static or dynamic lib switch; y-use static lib / n-use dynamic lib
#    values: y, n


USE_STATICLIB=n
USE_DEBUG=0
#==============================================================================
# GNU pathes                          (server admin update)
#==============================================================================
ifeq ($(T), -gcc)
PATH_GNU_BIN=/usr/local/arm-marvell-linux-gnueabi-4.4.4/bin
TOOLCHAIN_PREFIX=arm-marvell-linux-gnueabi
#cpu: wmmx2_linux, wmmx_linux, xsc_linux
ifeq ($(CPU_TYPE), xsc_linux)
CFLAGS= -mcpu=xscale -mtune=xscale
else
CFLAGS= -mcpu=iwmmxt -mtune=iwmmxt
endif
else
PATH_GNU_BIN=/usr/local/arm-marvell-linux-gnueabi-4.4.4-vfp/bin
TOOLCHAIN_PREFIX=arm-marvell-linux-gnueabi
#CFLAGS= -mcpu=marvell-fv6t2 -march=marvell-fv6t2
endif

#==============================================================================
# GNU           binaries             (server admin update)
#==============================================================================
CC=$(PATH_GNU_BIN)/$(TOOLCHAIN_PREFIX)-gcc
CXX=$(PATH_GNU_BIN)/$(TOOLCHAIN_PREFIX)-gcc
AR=$(PATH_GNU_BIN)/$(TOOLCHAIN_PREFIX)-ar
AS=$(PATH_GNU_BIN)/$(TOOLCHAIN_PREFIX)-as
LN=$(PATH_GNU_BIN)/$(TOOLCHAIN_PREFIX)-gcc

#==============================================================================
# GNU build options:                (build engineer update)
#==============================================================================
ifeq ($(USE_DEBUG), 0)
CFLAGS+= -O3 -Wall -mabi=aapcs-linux -fPIC -D_IPP_LINUX
else
CFLAGS+= -g -Wall -mabi=aapcs-linux -fPIC -D_IPP_LINUX
endif
CXXFLAGS= $(CFLAGS)
ASFLAGS= -k
ARFLAGS=
LNFLAGS= -lpthread -ldl -lm -lrt $(PGO_FLAGS) -Wl,-R ./

#==============================================================================
# User root path												(user update)
#==============================================================================
PATH_USR_ROOT=../../../..
PATH_USR_LIB=$(PATH_USR_ROOT)/lib
PATH_USR_BUILD=$(PATH_USR_ROOT)/example/$(CODEC_NAME)/build
PATH_USR_SRC=$(PATH_USR_ROOT)/example/$(CODEC_NAME)/src
PATH_USR_BIN=$(PATH_USR_ROOT)/example/$(CODEC_NAME)/bin

#==============================================================================
# External include option 											(user update)
#==============================================================================
OPT_INC_EXT=\
-I $(PATH_USR_ROOT)/include\
-I $(PATH_USR_SRC)\

#==============================================================================
# User libraries          										(user update)
#==============================================================================
ifeq ($(USE_STATICLIB), y)
USR_LIB_SUFFIX=a
else
USR_LIB_SUFFIX=so
endif
USR_LIBS=\
$(PATH_USR_LIB)/libcodec$(CODEC_NAME).$(USR_LIB_SUFFIX)\
$(PATH_USR_LIB)/libmiscgen.$(USR_LIB_SUFFIX)
