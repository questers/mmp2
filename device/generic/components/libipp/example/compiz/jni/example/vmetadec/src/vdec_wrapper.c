/****************************************************************************** // 
(C) Copyright 2009 Marvell International Ltd.
// All Rights Reserved
******************************************************************************/
#include <stdio.h>
#include "codecVC.h"
#include "misc.h"
#include "vdec_os_api.h"
#include "queue.h"
#include <signal.h>
#include "demo.h"
#include "vdec_wrapper.h"
#if ANDROID && USE_APK
#include "compiz_jni_activity.h"
#include "manchac.h"
#endif

#ifdef THREAD_DEBUG
#define PRINTF_THREAD(...)  IPP_Printf(__VA_ARGS__);
#define StartCounter(counter_index) IPP_StartPerfCounter(counter_index)
#define StopCounter(counter_index)  IPP_StopPerfCounter(counter_index)
#define GetPerfCounter(...)         IPP_GetPerfCounter(__VA_ARGS__)
#define ResetPerfCounter(...)       IPP_ResetPerfCounter(__VA_ARGS__)
#define FreePerfCounter(...)        IPP_FreePerfCounter(__VA_ARGS__)
#else
#define PRINTF_THREAD(...)  
#define StartCounter(counter_index)
#define StopCounter(counter_index) 
#define GetPerfCounter(...)
#define ResetPerfCounter(...)
#define FreePerfCounter(...)
#endif

//#define DBG_MODE
#ifdef DBG_MODE
#define DBG_PRINTF(...)  IPP_Printf(__VA_ARGS__);
#else
#define DBG_PRINTF(...)  
#endif

#if 0
int nReorderListHWaterMark[] = 
{
	0,		/* not used */
	1,		/* 1 stream no need to reorder */
	2*(PICTURE_BUF_NUM-1),
	3*(PICTURE_BUF_NUM-1),
	4*(PICTURE_BUF_NUM-1)
};
#endif

//#define INFINITE_LOOP

//temporary
#define ALIGN(x,a)		__ALIGN_MASK(x,(typeof(x))(a)-1)
#define __ALIGN_MASK(x,mask)	(((x)+(mask))&~(mask))                

extern int bStop;

static int g_bOptChksum = 0;

/* Globals */
BufQueue PicResPool[MAX_MULTISTRM_NUM];	        /* buffer queue available to be pushed into vmeta as output buffer */
BufQueue StrmResPool[MAX_MULTISTRM_NUM];        /* Stream resource Pool, Stores the stream buffer with no valid data */
BufQueue StrmBufFIFO[MAX_MULTISTRM_NUM];        /* Stream buffer fifo, Stores the stream buffer with valid stream data */
SortedList  SortList;
int nActiveStreamCnt = 0;				        /* Exit condition: !nActiveStreamCnt */
IppVmetaPicture     PictureGroup[MAX_MULTISTRM_NUM][PICTURE_BUF_NUM];
int bUsrInited[MAX_MULTISTRM_NUM] = {0, 0, 0, 0};
void *pEventStrmResPoolNotEmpty = NULL;
void *pEventSuspend = NULL;
void *ActiveStrmCnt_Mutex;

int nTotalStreamCnt  = 0;
IPP_FILE *fin[MAX_MULTISTRM_NUM];
int thrd[MAX_MULTISTRM_NUM], hThread, FileThread;
IppVmetaBitstream   BitStreamGroup[MAX_MULTISTRM_NUM][STREAM_BUF_NUM];

static void sig_handler(int sig)
{
    IPP_Printf("-----signal:%d-----\n", sig);

    bStop = 1;

    return ;
}

#if PURE_VMETA
#define START_TIMER
#define END_TIMER
#define MAXFRAME  10000
int vmetadec(IPP_FILE *fin, IPP_FILE *fout, IppVmetaDecParSet *pDecParSet, int ID)
{
    void *pDecoderState;
    MiscGeneralCallbackTable CBTable;
    IppCodecStatus rtCode;
    IppVmetaBitstream BitStreamGroup[STREAM_BUF_NUM];
    int i;
    IppVmetaBitstream *pBitStream;
    IppVmetaPicture PictureGroup[PICTURE_BUF_NUM];
    IppVmetaDecInfo VideoDecInfo;
    IppVmetaPicture *pPicture;
    IppVmetaDecInfo *pDecInfo;
    int nReadNum;
    int nCurStrmBufIdx;
    int nCurPicBufIdx;
    int bFreeStrmBufFlag[STREAM_BUF_NUM];
    int bFreePicBufFlag[PICTURE_BUF_NUM]; /* declaring as global variable is for manchac requirement*/

    int bStop;
    int nTotalFrames;   
    Ipp32u nTotalTime;
    int nFreeStrmBufNum, nFreePicBufNum;
    int codec_perf_index;


    int rtFlag = IPP_OK;
    int bIntWaitEventDisable;

    int ret;

    IPP_FILE *flen = NULL;
    int nFrameLen;

    Ipp32u start, end;

    IPP_Printf("ID = %d vmeta_ver: %d\n", ID, _VMETA_VER);
    IPP_Printf("ID = %d no-reording: %d\n", ID, pDecParSet->no_reordering);
    IPP_Printf("ID = %d stream format: %d\n", ID, pDecParSet->strm_fmt);
    IPP_Printf("ID = %d input buffer number: %d\n", ID, STREAM_BUF_NUM);
    IPP_Printf("ID = %d input buffer size: %d\n", ID, STREAM_BUF_SIZE);
    IPP_Printf("ID = %d output buffer number: %d\n", ID, PICTURE_BUF_NUM);

    ret = vdec_os_driver_init();
    if (0 > ret) {
        IPP_Printf("error: driver init fail! ret = %d\n", ret);
        return IPP_FAIL;
    }

    CBTable.fMemMalloc  = IPP_MemMalloc;
    CBTable.fMemFree    = IPP_MemFree;

    for (i = 0; i < STREAM_BUF_NUM; i++) {
        BitStreamGroup[i].pBuf                      = (Ipp8u*)vdec_os_api_dma_alloc(STREAM_BUF_SIZE, 128, &(BitStreamGroup[i].nPhyAddr));
        if (NULL == BitStreamGroup[i].pBuf) {
            IPP_Printf("error: no memory!\n");
            return IPP_FAIL;
        }
        BitStreamGroup[i].nBufSize                  = STREAM_BUF_SIZE;
        BitStreamGroup[i].nDataLen                  = 0;
        BitStreamGroup[i].nOffset                   = 0;
        BitStreamGroup[i].nBufProp                 |= VMETA_BUF_PROP_CACHEABLE;
        BitStreamGroup[i].pUsrData0                 = i;
        BitStreamGroup[i].pUsrData1                 = NULL;
        BitStreamGroup[i].pUsrData2                 = NULL;
        BitStreamGroup[i].pUsrData3                 = NULL;
        BitStreamGroup[i].nFlag                     = IPP_VMETA_STRM_BUF_PART_OF_FRAME;
        bFreeStrmBufFlag[i]                         = 1;
    }

    for (i = 0; i < PICTURE_BUF_NUM; i++) {
        PictureGroup[i].pBuf                = NULL;
        PictureGroup[i].nPhyAddr            = 0;
        PictureGroup[i].nBufSize            = 0;
        PictureGroup[i].nDataLen            = 0;
        PictureGroup[i].nOffset             = 0;
        PictureGroup[i].nBufProp           |= VMETA_BUF_PROP_CACHEABLE;
        PictureGroup[i].pUsrData0           = i;
        PictureGroup[i].pUsrData1           = NULL;
        PictureGroup[i].pUsrData2           = NULL;
        PictureGroup[i].pUsrData3           = NULL;
        PictureGroup[i].nFlag               = 0;
        IPP_Memset((void*)&(PictureGroup[i].pic), 0, sizeof(IppPicture));
        bFreePicBufFlag[i]                  = 1;
    }


    DBG_PRINTF("ID = %d before DecoderInitAlloc_Vmeta!\n", ID);
    rtCode = DecoderInitAlloc_Vmeta(pDecParSet, &CBTable, &pDecoderState);
    DBG_PRINTF("ID = %d end DecoderInitAlloc_Vmeta!\n", ID);
    if (IPP_STATUS_NOERR != rtCode) {
        IPP_Printf("ID = %d error: decoder init fail, error code %d!\n", ID, rtCode);
        return IPP_FAIL;
    }
    bIntWaitEventDisable = 1;
    bUsrInited[ID] = 1;
    //rtCode = DecodeSendCmd_Video(IPPVC_SET_INTWAITEVENTDISABLE, &bIntWaitEventDisable, NULL, pDecoderState);

    bStop               = 0;
    pBitStream          = NULL;
    pPicture            = NULL;
    pDecInfo            = &VideoDecInfo;
    nCurStrmBufIdx      = 0;
    nCurPicBufIdx       = 0;
    nTotalFrames        = 0;
    nTotalTime          = 0;
    nFreeStrmBufNum     = STREAM_BUF_NUM;

    IPP_GetPerfCounter(&codec_perf_index, DEFAULT_TIMINGFUNC_START, DEFAULT_TIMINGFUNC_STOP);
    IPP_ResetPerfCounter(codec_perf_index);


#if 1
    while(!bStop) {
        //IPP_Printf("ID = %d DecodeFrame_Vmeta before\n", ID);
            
        //gettimeofday(&g_tv, &g_tz);
        //start = g_tv.tv_sec * 1000000 + g_tv.tv_usec;
        START_TIMER;
        IPP_StartPerfCounter(codec_perf_index);
        rtCode = DecodeFrame_Vmeta(pDecInfo, pDecoderState);
        IPP_StopPerfCounter(codec_perf_index);
        END_TIMER;
        //gettimeofday(&g_tv, &g_tz);
        //end = g_tv.tv_sec * 1000000 + g_tv.tv_usec;
        //nTotalTime += (end - start);

        //IPP_Printf("ID = %d DecodeFrame_Vmeta end. rtCode = %d\n", ID, rtCode);
        if (IPP_STATUS_NEED_INPUT == rtCode) {
            DBG_PRINTF("ID = %d nFreeStrmBufNum = %d\n",ID, nFreeStrmBufNum);
            nCurStrmBufIdx = 0;
            while ((0 == bFreeStrmBufFlag[nCurStrmBufIdx]) && (STREAM_BUF_NUM > nCurStrmBufIdx)) {
                nCurStrmBufIdx++;
            }
            if (STREAM_BUF_NUM <= nCurStrmBufIdx) {
                rtFlag = IPP_FAIL;
                IPP_Printf("error: strm buf is not enough!\n");
                break;
            }

            pBitStream = &(BitStreamGroup[nCurStrmBufIdx]);
            bFreeStrmBufFlag[nCurStrmBufIdx] = 0;
            nFreeStrmBufNum--;
            IPP_Memset(pBitStream->pBuf, 0, STREAM_BUF_SIZE);
            //IPP_Printf("read stream fin = %x pBuf = %x\n", fin, pBitStream->pBuf);
            nReadNum = IPP_Fread(pBitStream->pBuf, 1, STREAM_BUF_SIZE, fin);
            pBitStream->nDataLen = STREAM_BUF_SIZE;
            if (STREAM_BUF_SIZE != nReadNum) {
                /*end of file*/
                pBitStream->nFlag       = IPP_VMETA_STRM_BUF_END_OF_UNIT;
                pBitStream->nDataLen    = nReadNum;                    
            }
            //IPP_Printf("before DecoderPushBuffer_Vmeta\n");
            //gettimeofday(&g_tv, &g_tz);
            //start = g_tv.tv_sec * 1000000 + g_tv.tv_usec;
            START_TIMER;
            rtCode = DecoderPushBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM, (void*)pBitStream, pDecoderState);
            END_TIMER;
            //gettimeofday(&g_tv, &g_tz);
            //end = g_tv.tv_sec * 1000000 + g_tv.tv_usec;
            //nTotalTime += (end - start);

            if (IPP_STATUS_NOERR != rtCode) {
                IPP_Printf("push stream buffer error!\n");
                break;
            }
            DBG_PRINTF("ID = %d push strm buf finish. strm buf id: %d datalen=%d\n", ID, (int)pBitStream->pUsrData0, pBitStream->nDataLen);
            if (STREAM_BUF_SIZE != pBitStream->nDataLen) {
                //IPP_Printf("send end of stream command!, pre datalen = %d\n", pBitStream->nDataLen);
                rtCode = DecodeSendCmd_Vmeta(IPPVC_END_OF_STREAM, NULL, NULL, pDecoderState);
                DBG_PRINTF("ID = %d send end of stream command!, rtCode = %d\n", ID, rtCode);
            }
        } else if (IPP_STATUS_NEED_OUTPUT_BUF == rtCode) {
            //IPP_Printf("need output buffer\n");
            nCurPicBufIdx = 0;
            while ((0 == bFreePicBufFlag[nCurPicBufIdx]) && (PICTURE_BUF_NUM > nCurPicBufIdx)) {
                nCurPicBufIdx++;
            }
            if (PICTURE_BUF_NUM <= nCurPicBufIdx) {
                rtFlag = IPP_FAIL;
                IPP_Printf("error: dis pic buf is not enough!\n");
                break;
            }
            pPicture = &(PictureGroup[nCurPicBufIdx]);
            if (NULL == pPicture->pBuf) {
                pPicture->pBuf = (Ipp8u*)vdec_os_api_dma_alloc(pDecInfo->seq_info.dis_buf_size, 1024, &(pPicture->nPhyAddr));
                if (NULL == pPicture->pBuf) {
                    IPP_Printf("error: no memory for display!\n");
                    
                    break;
                }
                pPicture->nBufSize = pDecInfo->seq_info.dis_buf_size;
            } else {
                if (pPicture->nBufSize < pDecInfo->seq_info.dis_buf_size) {
                    DBG_PRINTF("reallocate display buffer! id = %d presize = %d cursize = %d\n", (int)pPicture->pUsrData0, 
                        pPicture->nBufSize, pDecInfo->seq_info.dis_buf_size);
                    vdec_os_api_dma_free(pPicture->pBuf);
                    pPicture->pBuf = NULL;
                    pPicture->pBuf = (Ipp8u*)vdec_os_api_dma_alloc(pDecInfo->seq_info.dis_buf_size, 1024, &(pPicture->nPhyAddr));
                    if (NULL == pPicture->pBuf) {
                        IPP_Printf("error: no memory for display!\n");
                        break;
                    }
                    pPicture->nBufSize = pDecInfo->seq_info.dis_buf_size;
                }
            }
            //gettimeofday(&g_tv, &g_tz);
            //start = g_tv.tv_sec * 1000000 + g_tv.tv_usec;
            START_TIMER;
            DecoderPushBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC, (void*)pPicture, pDecoderState);
            END_TIMER;
            //gettimeofday(&g_tv, &g_tz);
            //end = g_tv.tv_sec * 1000000 + g_tv.tv_usec;
            //nTotalTime += (end - start);

            DBG_PRINTF("ID = %d push dis buf finish. dis pic buf id: %d\n", ID, (int)pPicture->pUsrData0);
            bFreePicBufFlag[nCurPicBufIdx] = 0;
        } else if (IPP_STATUS_RETURN_INPUT_BUF == rtCode) {
            while (1) {
                //gettimeofday(&g_tv, &g_tz);
                //start = g_tv.tv_sec * 1000000 + g_tv.tv_usec;
                START_TIMER;
                DecoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM, (void**)&pBitStream, pDecoderState);
                END_TIMER;
                //gettimeofday(&g_tv, &g_tz);
                //end = g_tv.tv_sec * 1000000 + g_tv.tv_usec;
                //nTotalTime += (end - start);
                if (NULL == pBitStream) {
                    break;
                }
                bFreeStrmBufFlag[(int)pBitStream->pUsrData0] = 1;
                nFreeStrmBufNum++;
                DBG_PRINTF("ID = %d pop strm buf finish. strm buf id: %d\n", ID, (int)pBitStream->pUsrData0);
            }    
        } else if (IPP_STATUS_FRAME_COMPLETE == rtCode) {
            while (1) {
                //gettimeofday(&g_tv, &g_tz);
                //start = g_tv.tv_sec * 1000000 + g_tv.tv_usec;
                START_TIMER;
                DecoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC, (void**)&pPicture, pDecoderState);
                END_TIMER;
                //gettimeofday(&g_tv, &g_tz);
                //end = g_tv.tv_sec * 1000000 + g_tv.tv_usec;
                //nTotalTime += (end - start);
                if (NULL == pPicture) {
                    break;
                }
                bFreePicBufFlag[(int)pPicture->pUsrData0] = 1;

                if (0 < pPicture->nDataLen) {
                    //OutputPicture_Video(pPicture, fout, pDecInfo, ID);
                    nTotalFrames++;
                    DBG_PRINTF("ID = %d IPP_STATUS_FRAME_COMPLETE %d id=%d pic_tye = %d POC = %d %d, coded pic = [%dx%d] roi=[%d %d %d %d]\n", 
                        ID,
                        nTotalFrames, (int)pPicture->pUsrData0, 
                        pPicture->PicDataInfo.pic_type,
                        pPicture->PicDataInfo.poc[0], pPicture->PicDataInfo.poc[1], 
                        pPicture->pic.picWidth, pPicture->pic.picHeight,
                        pPicture->pic.picROI.x, pPicture->pic.picROI.y,
                        pPicture->pic.picROI.width, pPicture->pic.picROI.height);
                    //gettimeofday(&g_tv, &g_tz);
                    //start = g_tv.tv_sec * 1000000 + g_tv.tv_usec;
                    START_TIMER;
                    DecoderPushBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC, (void*)pPicture, pDecoderState);
                    END_TIMER;
                    //gettimeofday(&g_tv, &g_tz);
                    //end = g_tv.tv_sec * 1000000 + g_tv.tv_usec;
                    //nTotalTime += (end - start);
                    bFreePicBufFlag[(int)pPicture->pUsrData0] = 0;
                } else {
                    DBG_PRINTF("ID = %d pop empty picture buffer! id = %d\n", ID, (int)pPicture->pUsrData0);
                }
            }
            if (MAXFRAME <= nTotalFrames) {
                break;
            }
        } else if (IPP_STATUS_NEW_VIDEO_SEQ == rtCode) {
            while (1) {
                DecoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC, (void**)&pPicture, pDecoderState);
                if (NULL == pPicture) {
                    break;
                }
                bFreePicBufFlag[(int)pPicture->pUsrData0] = 1;
                DBG_PRINTF("pop display buffer id = %d when encounter new sequence!\n", (int)pPicture->pUsrData0);
            }

            DBG_PRINTF("ID = %d IPP_STATUS_NEW_VIDEO_SEQ\n", ID);
        } else if (IPP_STATUS_END_OF_STREAM == rtCode){
            DBG_PRINTF("ID = %d IPP_STATUS_END_OF_STREAM\n", ID);
            break;
        } else {
            rtFlag = IPP_FAIL;
            IPP_Printf("ID = %d error: deadly error! %d\n", ID, rtCode);
            bStop = 1;
        }
    }

#endif

    DBG_PRINTF("ID = %d before DecoderFree_Vmeta.\n", ID);
    DecoderFree_Vmeta(&pDecoderState);
    DBG_PRINTF("ID = %d after DecoderFree_Vmeta.\n", ID);

    for (i = 0; i < STREAM_BUF_NUM; i++) {
        if (BitStreamGroup[i].pBuf) {
            vdec_os_api_dma_free(BitStreamGroup[i].pBuf);
        }
    }

    for (i = 0; i < PICTURE_BUF_NUM; i++) {
        if (PictureGroup[i].pBuf) {
            vdec_os_api_dma_free(PictureGroup[i].pBuf);
        }
    }        
    
    DBG_PRINTF("ID = %d before driver clean\n", ID);
    vdec_os_driver_clean();
    DBG_PRINTF("ID = %d after driver clean\n", ID);

    IPP_Printf("[Decode Thread %d] Exited, vpro fps:%.2f\n", ID, 
        (1000000.f*nTotalFrames)/(float)IPP_GetPerfData(codec_perf_index));
    IPP_DeinitPerfCounter(codec_perf_index);

    return rtFlag;
}

#else

static int GetFileLen(FILE *fp)
{
    int len; 

    fseek(fp, 0, SEEK_END);
    len = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    return len;
}

void file_reader(FILE **fin_array, int stream_cnt) {
    int nReadNum, failed_count = 0;
    IppVmetaBitstream *pBitStream = NULL;
    FILE *fin = NULL;
    int i;
    int stream_valid_flag[MAX_MULTISTRM_NUM], valid_stream_cnt = stream_cnt;
    int enqueue_stream_perf_index, dequeue_stream_perf_index;
    int worker_perf_index;
    int len[MAX_MULTISTRM_NUM], bytes_read_ptr[MAX_MULTISTRM_NUM];

    IPP_Printf("[FILE READER] Thread Created, Func:%s!\n", __FUNCTION__);

    GetPerfCounter(&dequeue_stream_perf_index, DEFAULT_TIMINGFUNC_START, DEFAULT_TIMINGFUNC_STOP);
    ResetPerfCounter(dequeue_stream_perf_index);

    GetPerfCounter(&enqueue_stream_perf_index, DEFAULT_TIMINGFUNC_START, DEFAULT_TIMINGFUNC_STOP);
    ResetPerfCounter(enqueue_stream_perf_index);

    GetPerfCounter(&worker_perf_index, (IPP_COUNTER_FUNC)IPP_TimeGetThreadTime, (IPP_COUNTER_FUNC)IPP_TimeGetThreadTime);
    ResetPerfCounter(worker_perf_index);

    for( i = 0; i < stream_cnt; i++ ) {
        stream_valid_flag[i] = 1;
        len[i] = GetFileLen(fin_array[i]);
        bytes_read_ptr[i] = 0;
    }

    while(valid_stream_cnt && !bStop) {
        failed_count = 0;

        for( i = 0; i < stream_cnt; i++ ) {
            if (stream_valid_flag[i] && !bStop) {
                fin = fin_array[i];
                PRINTF_THREAD("[FILE READER %d] Before TryDequeue StreamRes\n", i);
                StartCounter(dequeue_stream_perf_index);
                while(BufQueue_TryDeQueue(&StrmResPool[i], (BufType**)&pBitStream) < 0) {
                    /* Do Nothing */
                    PRINTF_THREAD("[FILE READER %d] TryDequeue StrmRes Failed, Retry...\n", i);
                }
                StopCounter(dequeue_stream_perf_index);
                PRINTF_THREAD("[FILE READER %d] After TryDequeue StreamRes\n", i);
                if (NULL == pBitStream) {
                    failed_count++;
                    continue;
                }
                pBitStream->nDataLen    = 0;
                pBitStream->nOffset     = 0;
                pBitStream->nFlag       = 0; /*default value means neither end of frame nor end of unit*/

                DBG_PRINTF("[FILE READER] ID=%d read stream fin = %p pBuf = %p\n", i, fin, pBitStream->pBuf);
                nReadNum = 0;
                StartCounter(worker_perf_index);
                while(nReadNum < STREAM_BUF_SIZE && !IPP_Feof(fin)) {
                    nReadNum += IPP_Fread(pBitStream->pBuf+nReadNum, sizeof(Ipp8u), STREAM_BUF_SIZE-nReadNum, fin);
                }
                StopCounter(worker_perf_index);
                DBG_PRINTF("[FILE READER] ID=%d read %d bytes from %p\n", i, nReadNum, fin);
                pBitStream->nDataLen = nReadNum;
                if (STREAM_BUF_SIZE != nReadNum) {
                    /*end of file*/
#ifdef INFINITE_LOOP
		            IPP_Fseek(fin, 0L, IPP_SEEK_SET);
		            int nReadNum2 = IPP_Fread(pBitStream->pBuf+nReadNum, sizeof(Ipp8u), STREAM_BUF_SIZE-nReadNum, fin);
                    pBitStream->nDataLen    += nReadNum2;
            		
		            IPP_Printf("===============End of one run, sent buffer size %d, should send %d ============\n", 
                        pBitStream->nDataLen, STREAM_BUF_SIZE);
#else
                    pBitStream->nFlag       = IPP_VMETA_STRM_BUF_END_OF_UNIT;
                    stream_valid_flag[i]    = 0;
                    valid_stream_cnt--;
#endif
                }

                PRINTF_THREAD("[FILE READER %d] Before Enqueue StreamRes into FIFO\n", i);
                StartCounter(enqueue_stream_perf_index);
                while(BufQueue_EnQueue(&StrmBufFIFO[i], pBitStream) < 0) {
                    /* Do Nothing */
                    PRINTF_THREAD("[FILE READER %d] Enqueue StreamBuf:%p Failed, Retry...\n", i, pBitStream);
                }
                StopCounter(enqueue_stream_perf_index);
                PRINTF_THREAD("[FILE READER %d] After  Enqueue StreamRes into FIFO\n", i);
            }
        }

        if (failed_count == valid_stream_cnt) {
            PRINTF_THREAD("[FILE READER %d] pEventStrmResPoolNotEmpty Reset!\n", i);
            IPP_EventReset(pEventStrmResPoolNotEmpty);
            PRINTF_THREAD("[FILE READER %d] Wait for pEventStrmResPoolNotEmpty signal!\n", i);
            IPP_EventWait(pEventStrmResPoolNotEmpty, INFINITE_WAIT, NULL);
        }
    }

#if THREAD_DBG
    PRINTF_THREAD("[FILE READER] Enqueue stream:%ld, Dequeue stream:%ld\n", 
        IPP_GetPerfData(enqueue_stream_perf_index),
        IPP_GetPerfData(dequeue_stream_perf_index));
#endif
    PRINTF_THREAD("[FILE READER] read stream:%ld\n",
        IPP_GetPerfData(worker_perf_index));

    FreePerfCounter(enqueue_stream_perf_index);
    FreePerfCounter(dequeue_stream_perf_index);
    FreePerfCounter(worker_perf_index);

    PRINTF_THREAD("[FILE READER] Exit:%s\n", __FUNCTION__);
}

static void __VmetaExit(int ID)
{
    IppVmetaBitstream *pBitStream = NULL;
    int i;

    IPP_MutexLock(ActiveStrmCnt_Mutex, INFINITE_WAIT, NULL);
    nActiveStreamCnt--;
    IPP_MutexUnlock(ActiveStrmCnt_Mutex);
    SortedList_SetThresh(&SortList, nActiveStreamCnt);

    /* Push all stream buffer into Pool to restore the initial state */
    for (i = 0; i < STREAM_BUF_NUM; i++ ) {
        while(BufQueue_EnQueue(&StrmResPool[ID], &BitStreamGroup[ID][i]) < 0) {
            /* Do Nothing */
            PRINTF_THREAD("[Decode Thread %d] EnQueue Strm:%p Failed, Retry...\n", ID, pBitStream);
        }
        PRINTF_THREAD("[Decode Thread %d] Set pEventStrmResPoolNotEmpty signal!\n", ID);
    }
    IPP_EventSet(pEventStrmResPoolNotEmpty);

    if (nActiveStreamCnt == 0) {
#if ANDROID && USE_APK
        if (1 != bStop) {
            NormalExitCompiz((const char*)"NormalExit");
            bStop = 1;
        }
#else
        bStop = 1;
#endif
        IPP_Printf("[Decode Thread %d] Terminate Program!\n", ID);
    }
}
int vmetadec(IPP_FILE *fout, IppVmetaDecParSet *pDecParSet, int ID)
{
    void *pDecoderState;
    MiscGeneralCallbackTable CBTable;
    IppCodecStatus rtCode;
    IppVmetaBitstream *pBitStream;
    IppVmetaDecInfo VideoDecInfo;
    /* pNextPicture is set to valid picture pointer at frame complete time, and pushed into vMeta at need output buffer time */
    /* at need output buffer time vMeta is already locked and doesn't have chance to switch to other streams, we don't hope to wait there */
    IppVmetaPicture *pPicture, *pNextPicture;	
    IppVmetaDecInfo *pDecInfo;
    int nCurStrmBufIdx;
    int nCurPicBufIdx;

    long int nTotalFrames;
    Ipp32u nTotalTime;

	int codec_perf_index;
    int dequeue_pic_perf_index, enqueue_pic_perf_index;
    int dequeue_stream_perf_index, enqueue_stream_perf_index;
    int rtFlag = IPP_OK;
    int bSafePt = 0;

    int ret;


	PRINTF_THREAD("[Decode Thread %d] Created.\n", ID);
    PRINTF_THREAD("[Decode Thread %d] vmeta_ver: %d\n", ID, _VMETA_VER);
    PRINTF_THREAD("[Decode Thread %d] no-reording: %d\n", ID, pDecParSet->no_reordering);
    PRINTF_THREAD("[Decode Thread %d] stream format: %d\n", ID, pDecParSet->strm_fmt);
    PRINTF_THREAD("[Decode Thread %d] input buffer number: %d\n", ID, STREAM_BUF_NUM);
    PRINTF_THREAD("[Decode Thread %d] input buffer size: %d\n", ID, STREAM_BUF_SIZE);
    PRINTF_THREAD("[Decode Thread %d] output buffer number: %d\n", ID, PICTURE_BUF_NUM);

    ret = vdec_os_driver_init();
    if (0 > ret) {
        IPP_Printf("error: driver init fail! ret = %d\n", ret);
        __VmetaExit(ID);
        return IPP_FAIL;
    }

    CBTable.fMemMalloc  = IPP_MemMalloc;
    CBTable.fMemFree    = IPP_MemFree;

    rtCode = DecoderInitAlloc_Vmeta(pDecParSet, &CBTable, &pDecoderState);
    bUsrInited[ID] = 1;

    if (IPP_STATUS_NOERR != rtCode) {
        IPP_Printf("ID = %d error: decoder init fail, error code %d!\n", ID, rtCode);
        __VmetaExit(ID);
        return IPP_FAIL;
    }

    pBitStream          = NULL;
    pPicture            = NULL;
    pDecInfo            = &VideoDecInfo;
    nCurStrmBufIdx      = 0;
    nCurPicBufIdx       = 0;
    nTotalFrames        = 0;
    nTotalTime          = 0;

    PRINTF_THREAD("[Decode Thread %d] Before Dequeue PicRes\n", ID);
    while(BufQueue_DeQueue(&PicResPool[ID], (BufType**)&pNextPicture) < 0) {
        /* Do nothing */;
        PRINTF_THREAD("[Decode Thread %d] DeQueue PicRes Failed, Retry...\n", ID);
    }
    PRINTF_THREAD("[Decode Thread %d] After Dequeue Pic buffer:%d success!\n", ID, (int)pNextPicture->pUsrData0);

    PRINTF_THREAD("[Decode Thread %d] Dequeue PicResPool 0x%p, index:%d\n", 
        ID, pNextPicture, (int)pNextPicture->pUsrData0);

    GetPerfCounter(&dequeue_stream_perf_index, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount);
    ResetPerfCounter(dequeue_stream_perf_index);

    GetPerfCounter(&enqueue_stream_perf_index, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount);
    ResetPerfCounter(enqueue_stream_perf_index);

    GetPerfCounter(&dequeue_pic_perf_index, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount);
    ResetPerfCounter(dequeue_pic_perf_index);

    GetPerfCounter(&enqueue_pic_perf_index, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount);
    ResetPerfCounter(enqueue_pic_perf_index);

    IPP_GetPerfCounter(&codec_perf_index, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount, (IPP_COUNTER_FUNC)IPP_TimeGetTickCount);
    IPP_ResetPerfCounter(codec_perf_index);

    if (-1 == codec_perf_index) {
        IPP_Printf("Failed to create counter for vmetadec!\n");
        return IPP_FAIL;
    }

    while(!(bStop && bSafePt)) {
        IPP_StartPerfCounter(codec_perf_index);
        rtCode = DecodeFrame_Vmeta(pDecInfo, pDecoderState);
        IPP_StopPerfCounter(codec_perf_index);

        bSafePt = 0;

        if (IPP_STATUS_NEED_INPUT == rtCode) {
            DBG_PRINTF("[Decode Thread %d] Event:IPP_STATUS_NEED_INPUT\n", ID);

            /* For multi-instance, stream buffer may not explicitly output */
            if (IsBufQueue_Empty(&StrmBufFIFO[ID])) {
                while (1) {
                    IPP_StartPerfCounter(codec_perf_index);
                    pBitStream = NULL;
                    DecoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM, (void**)&pBitStream, pDecoderState);
                    IPP_StopPerfCounter(codec_perf_index);
                    if (NULL == pBitStream) {
                        break;
                    }

                    PRINTF_THREAD("[Decode Thread %d] Before Enqueue StrmPool\n", ID);
                    StartCounter(enqueue_stream_perf_index);
                    while(BufQueue_EnQueue(&StrmResPool[ID], pBitStream) < 0) {
                        PRINTF_THREAD("[Decode Thread %d] EnQueue Strm:%p Failed, Retry...\n", ID, pBitStream);
                    }
                    StopCounter(enqueue_stream_perf_index);
                    PRINTF_THREAD("[Decode Thread %d] After Enqueue StrmPool\n", ID);

                    PRINTF_THREAD("[Decode Thread %d] Triggered StrmPool NotEmpty!\n", ID);
                    IPP_EventSet(pEventStrmResPoolNotEmpty);

                    DBG_PRINTF("ID = %d pop strm buf finish. strm buf id: %d\n", ID, (int)pBitStream->pUsrData0);
                }
            }

            PRINTF_THREAD("[Decode Thread %d] Before Dequeue StrmFIFO\n", ID);
            StartCounter(dequeue_stream_perf_index);
            while(BufQueue_DeQueue(&StrmBufFIFO[ID], (BufType**)&pBitStream) < 0) {
                /* Do Nothing */
                PRINTF_THREAD("[Decode Thread %d] DeQueue StreamBuf Failed, Retry...\n", ID);
            }
            StopCounter(dequeue_stream_perf_index);
            PRINTF_THREAD("[Decode Thread %d] After Dequeue StrmFIFO\n", ID);
            
            DBG_PRINTF("ID = %d before DecoderPushBuffer_Vmeta\n", ID);
            IPP_StartPerfCounter(codec_perf_index);
            rtCode = DecoderPushBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM, (void*)pBitStream, pDecoderState);
            IPP_StopPerfCounter(codec_perf_index);
            DBG_PRINTF("ID = %d push stream buf finish. stream buf id: %d\n", ID, (int)pBitStream->pUsrData0);
            if (IPP_STATUS_NOERR != rtCode) {
                IPP_Printf("ID = %d, push stream buffer error, retcode:%d!\n", ID, rtCode);
                break;
            }

            if (pBitStream->nFlag&IPP_VMETA_STRM_BUF_END_OF_UNIT) {
                DBG_PRINTF("ID = %d send end of stream command!, pre datalen = %d\n", ID, pBitStream->nDataLen);
                IPP_StartPerfCounter(codec_perf_index);
                rtCode = DecodeSendCmd_Vmeta(IPPVC_END_OF_STREAM, NULL, NULL, pDecoderState);
                IPP_StopPerfCounter(codec_perf_index);
                DBG_PRINTF("ID = %d send end of stream command!, rtCode = %d\n", ID, rtCode);
            }
        } else if (IPP_STATUS_NEED_OUTPUT_BUF == rtCode) {
            DBG_PRINTF("[Decode Thread %d] Event:IPP_STATUS_NEED_OUTPUT_BUF\n", ID);
        	/* at this time, vmeta is locked, so move this to frame complete */

            if ( NULL == pNextPicture ) {
                /* For multi-instance, picture buffer may not return explicity */
                while (1) {
                    IPP_StartPerfCounter(codec_perf_index);
                    DecoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC, (void**)&pPicture, pDecoderState);
                    IPP_StopPerfCounter(codec_perf_index);
                    if (NULL == pPicture) {
                        break;
                    }
                    if (0 < pPicture->nDataLen) {
                	    pPicture->pUsrData2 = (void*)ID;
					    pPicture->pUsrData3 = (void*)nTotalFrames;
                        PRINTF_THREAD("[Decode Thread %d] Before Enqueue SortedList frameID:%ld, pic:%d\n", 
                            ID, nTotalFrames, (int)pPicture->pUsrData0);
                        StartCounter(enqueue_pic_perf_index);
                        while(SortedList_EnList(&SortList, (BufType*)pPicture, nTotalFrames) < 0) {
                            PRINTF_THREAD("[Decode Thread %d] Enqueue SortedList Fail, Retry...\n", ID);
                        }
                        StopCounter(enqueue_pic_perf_index);
                        PRINTF_THREAD("[Decode Thread %d] After Enqueue SortedList frameID:%ld, pic:%d Success!\n", 
                            ID, nTotalFrames, (int)pPicture->pUsrData0);

                        DBG_PRINTF("ID = %d IPP_STATUS_FRAME_COMPLETE %d id=%d pic_type = %d POC = %d %d, coded pic = [%dx%d] roi=[%d %d %d %d]\n", 
                            ID, nTotalFrames, (int)pPicture->pUsrData0, 
                            pPicture->PicDataInfo.pic_type,
                            pPicture->PicDataInfo.poc[0], pPicture->PicDataInfo.poc[1], 
                            pPicture->pic.picWidth, pPicture->pic.picHeight,
                            pPicture->pic.picROI.x, pPicture->pic.picROI.y,
                            pPicture->pic.picROI.width, pPicture->pic.picROI.height);

                        nTotalFrames++;

                        DBG_PRINTF("ID:%d, dequeue pic buffer!\n", ID);
                        if ( NULL == pNextPicture ) {
                            PRINTF_THREAD("[Decode Thread %d] Before Dequeue\n", ID);
                            StartCounter(dequeue_pic_perf_index);
                            DBG_PRINTF("ID:%d, before dequeue pic, count:%d\n", ID, PicResPool[ID].count);
                            while(BufQueue_DeQueue(&PicResPool[ID], (BufType**)&pNextPicture) < 0) {
                                PRINTF_THREAD("[Decode Thread %d] Dequeue Pic Buffer Failed, Retry...\n", ID);
                            }
                            DBG_PRINTF("ID:%d, after dequeue pic, count:%d\n", ID, PicResPool[ID].count);
                            StopCounter(dequeue_pic_perf_index);
                            PRINTF_THREAD("[Decode Thread %d] After Dequeue Pic buffer:%d success!\n", ID, (int)pNextPicture->pUsrData0);
                        }
                    } else {
                        if (NULL == pNextPicture) {
                            pNextPicture = pPicture;
                        } else {
                            DBG_PRINTF("[Warning]: ID = %d pop empty picture buffer! id = %d\n", ID, (int)pPicture->pUsrData0);
                            StartCounter(enqueue_pic_perf_index);
                            while(BufQueue_EnQueue(&PicResPool[ID], pPicture) < 0) {
                                /* Do Nothing */
                                PRINTF_THREAD("[Decode Thread %d] EnQueue PicRes:%p Failed, Retry...\n", ID, pPicture);
                            }
                            StopCounter(enqueue_pic_perf_index);
                            PRINTF_THREAD("[Decode Thread %d] Warning Pop empty picture buffer:%d!\n", ID, (int)pPicture->pUsrData0);
                        }
                    }
                }
            } 

            pPicture = pNextPicture;
            pNextPicture = NULL;

            if (NULL == pPicture->pBuf) {
                pPicture->pBuf = (Ipp8u*)vdec_os_api_dma_alloc(pDecInfo->seq_info.dis_buf_size, 
                    VMETA_DIS_BUF_ALIGN, &(pPicture->nPhyAddr));
                
                DBG_PRINTF("[Decode Thread %d] Allocate %d KB picture 0x%p\n", ID, pDecInfo->seq_info.dis_buf_size, pPicture);   
                if (NULL == pPicture->pBuf) {
                    IPP_Printf("ID = %d, error: no memory for display!\n", ID);
                    break;
                }
                pPicture->nBufSize = pDecInfo->seq_info.dis_buf_size;
            } else {
                if (pPicture->nBufSize < pDecInfo->seq_info.dis_buf_size) {
                    DBG_PRINTF("Reallocate display buffer! id = %d presize = %d newsize = %d\n", (int)pPicture->pUsrData0, 
                        pPicture->nBufSize, pDecInfo->seq_info.dis_buf_size);
                 
                    vdec_os_api_dma_free(pPicture->pBuf);
                    pPicture->pBuf = NULL;
                    pPicture->pBuf = (Ipp8u*)vdec_os_api_dma_alloc(pDecInfo->seq_info.dis_buf_size, 
                        VMETA_DIS_BUF_ALIGN, &(pPicture->nPhyAddr));                    
                    if (NULL == pPicture->pBuf) {
                        IPP_Printf("ID = %d, error: no memory for display!\n", ID);
                        break;
                    }
                    pPicture->nBufSize = pDecInfo->seq_info.dis_buf_size;
                }
            }
            
            IPP_StartPerfCounter(codec_perf_index);
            DecoderPushBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC, (void*)pPicture, pDecoderState);
            IPP_StopPerfCounter(codec_perf_index);
            DBG_PRINTF("ID = %d push dis buf finish. dis pic buf id: %d\n", ID, (int)pPicture->pUsrData0);
        } else if (IPP_STATUS_RETURN_INPUT_BUF == rtCode) {
            DBG_PRINTF("[Decode Thread %d] Event:IPP_STATUS_RETURN_INPUT_BUF\n", ID);
            while (1) {
                IPP_StartPerfCounter(codec_perf_index);
                DecoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM, (void**)&pBitStream, pDecoderState);
               IPP_StopPerfCounter(codec_perf_index);
                if (NULL == pBitStream) {
                    break;
                }

                PRINTF_THREAD("[Decode Thread %d] Before Enqueue StrmPool\n", ID);
                StartCounter(enqueue_stream_perf_index);
                while(BufQueue_EnQueue(&StrmResPool[ID], pBitStream) < 0) {
                    /* Do Nothing */
                    PRINTF_THREAD("[Decode Thread %d] EnQueue Strm:%p Failed, Retry...\n", ID, pBitStream);
                }
                StopCounter(enqueue_stream_perf_index);
                PRINTF_THREAD("[Decode Thread %d] After Enqueue StrmPool\n", ID);

                PRINTF_THREAD("[Decode Thread %d] Triggered StrmPool NotEmpty!\n", ID);
                IPP_EventSet(pEventStrmResPoolNotEmpty);

                DBG_PRINTF("ID = %d pop strm buf finish. strm buf id: %d\n", ID, (int)pBitStream->pUsrData0);
            }
        } else if (IPP_STATUS_FRAME_COMPLETE == rtCode) {
            DBG_PRINTF("[Decode Thread %d] Event:IPP_STATUS_FRAME_COMPLETE\n", ID);
            while (1) {
                IPP_StartPerfCounter(codec_perf_index);
                pBitStream = NULL;
                DecoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM, (void**)&pBitStream, pDecoderState);
                IPP_StopPerfCounter(codec_perf_index);
                if (NULL == pBitStream) {
                    break;
                }

                PRINTF_THREAD("[Decode Thread %d] Before Enqueue StrmPool\n", ID);
                StartCounter(enqueue_stream_perf_index);
                while(BufQueue_EnQueue(&StrmResPool[ID], pBitStream) < 0) {
                    PRINTF_THREAD("[Decode Thread %d] EnQueue Strm:%p Failed, Retry...\n", ID, pBitStream);
                }
                StopCounter(enqueue_stream_perf_index);
                PRINTF_THREAD("[Decode Thread %d] After Enqueue StrmPool\n", ID);

                PRINTF_THREAD("[Decode Thread %d] Triggered StrmPool NotEmpty!\n", ID);
                IPP_EventSet(pEventStrmResPoolNotEmpty);

                DBG_PRINTF("ID = %d pop strm buf finish. strm buf id: %d\n", ID, (int)pBitStream->pUsrData0);
            }

            while (1) {
                IPP_StartPerfCounter(codec_perf_index);
                DecoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC, (void**)&pPicture, pDecoderState);
                IPP_StopPerfCounter(codec_perf_index);
                if (NULL == pPicture) {
                    break;
                }
                if (0 < pPicture->nDataLen) {
                	pPicture->pUsrData2 = (void*)ID;
					pPicture->pUsrData3 = (void*)nTotalFrames;
                    PRINTF_THREAD("[Decode Thread %d] Before Enqueue SortedList frameID:%ld, pic:%d\n", 
                        ID, nTotalFrames, (int)pPicture->pUsrData0);
                    StartCounter(enqueue_pic_perf_index);
                    while(SortedList_EnList(&SortList, (BufType*)pPicture, nTotalFrames) < 0) {
                        PRINTF_THREAD("[Decode Thread %d] Enqueue SortedList Fail, Retry...\n", ID);
                    }
                    StopCounter(enqueue_pic_perf_index);
                    PRINTF_THREAD("[Decode Thread %d] After Enqueue SortedList frameID:%ld, pic:%d Success!\n", 
                        ID, nTotalFrames, (int)pPicture->pUsrData0);

                    DBG_PRINTF("ID = %d IPP_STATUS_FRAME_COMPLETE %d id=%d pic_type = %d POC = %d %d, coded pic = [%dx%d] roi=[%d %d %d %d]\n", 
                        ID, nTotalFrames, (int)pPicture->pUsrData0, 
                        pPicture->PicDataInfo.pic_type,
                        pPicture->PicDataInfo.poc[0], pPicture->PicDataInfo.poc[1], 
                        pPicture->pic.picWidth, pPicture->pic.picHeight,
                        pPicture->pic.picROI.x, pPicture->pic.picROI.y,
                        pPicture->pic.picROI.width, pPicture->pic.picROI.height);

                    nTotalFrames++;

                    DBG_PRINTF("ID:%d, dequeue pic buffer!\n", ID);
                    if ( NULL == pNextPicture ) {
                        PRINTF_THREAD("[Decode Thread %d] Before Dequeue\n", ID);
                        StartCounter(dequeue_pic_perf_index);
                        DBG_PRINTF("ID:%d, before timed dequeue pic, count:%d\n", ID, PicResPool[ID].count);
                        BufQueue_TimedDeQueue(&PicResPool[ID], (BufType**)&pNextPicture, 2000);
                        DBG_PRINTF("ID:%d, after timed dequeue pic, count:%d\n", ID, PicResPool[ID].count);
                        StopCounter(dequeue_pic_perf_index);
                        if ( NULL != pNextPicture ) {
                            PRINTF_THREAD("[Decode Thread %d] After Dequeue Pic buffer:%d success!\n", ID, (int)pNextPicture->pUsrData0);
                        } else {
                            PRINTF_THREAD("[Decode Thread %d] After Dequeue FAIL!\n", ID);
                        }
                    }
                } else {
                    if (NULL == pNextPicture) {
                        pNextPicture = pPicture;
                    } else {
                        DBG_PRINTF("[Warning]: ID = %d pop empty picture buffer! id = %d\n", ID, (int)pPicture->pUsrData0);
                        StartCounter(enqueue_pic_perf_index);
                        while(BufQueue_EnQueue(&PicResPool[ID], pPicture) < 0) {
                            /* Do Nothing */
                            PRINTF_THREAD("[Decode Thread %d] EnQueue PicRes:%p Failed, Retry...\n", ID, pPicture);
                        }
                        StopCounter(enqueue_pic_perf_index);
                        PRINTF_THREAD("[Decode Thread %d] Warning Pop empty picture buffer:%d!\n", ID, (int)pPicture->pUsrData0);
                    }
                }
            }
            bSafePt = 1;
        } else if (IPP_STATUS_NEW_VIDEO_SEQ == rtCode) {
            DBG_PRINTF("[Decode Thread %d] Event:IPP_STATUS_NEW_VIDEO_SEQ\n", ID);
            while (1) {
                IPP_StartPerfCounter(codec_perf_index);
                DecoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC, (void**)&pPicture, pDecoderState);
                IPP_StopPerfCounter(codec_perf_index);
                if (NULL == pPicture) {
                    break;
                }
                PRINTF_THREAD("[Decode Thread %d] Before Dequeue\n", ID);
                StartCounter(enqueue_pic_perf_index);
                while(BufQueue_EnQueue(&PicResPool[ID], pPicture) < 0) {
                    /* Do Nothing */;
                    PRINTF_THREAD("[Decode Thread %d] EnQueue PicRes:%p Failed, Retry...\n", ID, pPicture);
                }
                StopCounter(enqueue_pic_perf_index);
                PRINTF_THREAD("[Decode Thread %d] After Dequeue Pic buffer:%d success!\n", ID, (int)pPicture->pUsrData0);
			
                DBG_PRINTF("pop display buffer id = %d when encounter new sequence!\n", (int)pPicture->pUsrData0);
            }

            if (NULL == pNextPicture) {
                PRINTF_THREAD("[Decode Thread %d] Before timed Dequeue\n", ID);
                StartCounter(dequeue_pic_perf_index);
                BufQueue_TimedDeQueue(&PicResPool[ID], (BufType**)&pNextPicture, 2000);
                StopCounter(dequeue_pic_perf_index);
                if (NULL != pNextPicture) {
                    PRINTF_THREAD("[Decode Thread %d] After timed Dequeue Pic buffer:%d success!\n", ID, (int)pNextPicture->pUsrData0);
                } else {
                    PRINTF_THREAD("[Decode Thread %d] After timed Dequeue FAIL!\n", ID);
                }
            }

            DBG_PRINTF("ID = %d IPP_STATUS_NEW_VIDEO_SEQ\n", ID);
        } else if (IPP_STATUS_END_OF_STREAM == rtCode) {
            DBG_PRINTF("ID = %d, IPP_STATUS_END_OF_STREAM\n", ID);
            while (1) {
                IPP_StartPerfCounter(codec_perf_index);
                DecoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM, (void**)&pBitStream, pDecoderState);
                IPP_StopPerfCounter(codec_perf_index);
                if (NULL == pBitStream) {
                    break;
                }

                PRINTF_THREAD("[Decode Thread %d] Before Enqueue StrmPool\n", ID);
                StartCounter(enqueue_stream_perf_index);
                while(BufQueue_EnQueue(&StrmResPool[ID], pBitStream) < 0) {
                    /* Do Nothing */
                    PRINTF_THREAD("[Decode Thread %d] EnQueue Strm:%p Failed, Retry...\n", ID, pBitStream);
                }
                StopCounter(enqueue_stream_perf_index);
                PRINTF_THREAD("[Decode Thread %d] After Enqueue StrmPool\n", ID);

                DBG_PRINTF("ID = %d pop strm buf finish. strm buf id: %d\n", ID, (int)pBitStream->pUsrData0);
            }
            break;
        } else {
            rtFlag = IPP_FAIL;
            IPP_Printf("ID = %d error: deadly error! %d\n", ID, rtCode);
            break;
        }
    }

    __VmetaExit(ID);

    DBG_PRINTF("ID = %d before DecoderFree_Vmeta.\n", ID);
    DecoderFree_Vmeta(&pDecoderState);
    DBG_PRINTF("ID = %d after DecoderFree_Vmeta.\n", ID);

    g_Comp_Total_Time[IPP_VIDEO_INDEX][ID]     = IPP_GetPerfData(codec_perf_index);
    g_Comp_Frame_Num[IPP_VIDEO_INDEX][ID]    = nTotalFrames;	

    DBG_PRINTF("ID = %d before driver clean\n", ID);
    vdec_os_driver_clean();
    DBG_PRINTF("ID = %d after driver clean\n", ID);

    IPP_Printf("[Decode Thread %d] Exited, vpro fps:%.2f, %ld frames decoded!\n", ID, 
        (1000000.f*nTotalFrames)/(float)g_Comp_Total_Time[IPP_VIDEO_INDEX][ID], nTotalFrames);

#if THREAD_DBG
    PRINTF_THREAD("[Decode Thread %d] Enqueue stream:%ld, Dequeue stream:%ld, Enqueue Pic:%ld, Dequeue Pic:%ld\n", ID, 
        IPP_GetPerfData(enqueue_stream_perf_index), IPP_GetPerfData(dequeue_stream_perf_index),
        IPP_GetPerfData(enqueue_pic_perf_index),    IPP_GetPerfData(dequeue_pic_perf_index));
#endif
    FreePerfCounter(enqueue_stream_perf_index);
    FreePerfCounter(dequeue_stream_perf_index);
    FreePerfCounter(enqueue_pic_perf_index);
    FreePerfCounter(dequeue_pic_perf_index);
    IPP_FreePerfCounter(codec_perf_index);
    fflush(stdout);

    return rtFlag;
}
#endif

typedef struct _FileReaderPara {
    FILE **fp_array;
    int  stream_cnt;
    void (*pFunc)(FILE **fin_array, int stream_cnt);
} FileReaderPara;

void FileReaderWorkThread(void *para)
{
    FileReaderPara *p = (FileReaderPara*)para;
    p->pFunc(p->fp_array, p->stream_cnt);
}

#if PURE_VMETA
struct ThreadPara {
    int ID;
    IPP_FILE *fin;
    IPP_FILE *fout;
    IppVmetaDecParSet *pDecParSet;
    int (*pFunc)(IPP_FILE *fin, IPP_FILE *fout, IppVmetaDecParSet *pDecParSet, int ID);
};

void WorkThread(void *para)
{
    struct ThreadPara *p = (struct ThreadPara*)para;
   	p->pFunc(p->fin, p->fout, p->pDecParSet, p->ID);
}
#else
struct ThreadPara {
    int ID;
    IPP_FILE *fout;
    IppVmetaDecParSet *pDecParSet;
    int (*pFunc)(IPP_FILE *fout, IppVmetaDecParSet *pDecParSet, int ID);
};

void WorkThread(void *para)
{
    struct ThreadPara *p = (struct ThreadPara*)para;
   	p->pFunc(p->fout, p->pDecParSet, p->ID);
}
#endif

int ParseVmetaDecCmd(char *pCmdLine, 
                     IPP_FILE **pSrcFile, 
                    void *pParSet, 
                    int  *pTotalCnt)
{
#define MAX_PAR_NAME_LEN    1024
#define MAX_PAR_VALUE_LEN   2048
#define STRNCPY(dst, src, len) \
{\
    IPP_Strncpy((dst), (src), (len));\
    (dst)[(len)] = '\0';\
}
    char *pCur, *pEnd;
    char par_name[MAX_PAR_NAME_LEN];
    char par_value[MAX_PAR_VALUE_LEN];
    int  par_name_len;
    int  par_value_len;
    char *p1, *p2, *p3;
    IppVmetaDecParSet *pDecParSet = (IppVmetaDecParSet*)pParSet;
    int nTotal = 0, nStart = 0, nEnd = 0;
    int strm_fmt;
    char pSrcFileName[MAX_PAR_NAME_LEN] = {0};
    FILE *fp = NULL;

    pCur = pCmdLine;
    pEnd = pCmdLine + IPP_Strlen(pCmdLine);

    while((p1 = IPP_Strstr(pCur, "-"))){
        p2 = IPP_Strstr(p1, ":");
        if (NULL == p2) {
            return IPP_FAIL;
        }
        p3 = IPP_Strstr(p2, " "); /*one space*/
        if (NULL == p3) {
            p3 = pEnd;
        }

        par_name_len    = p2 - p1 - 1;
        par_value_len   = p3 - p2 - 1;

        if ((0 >= par_name_len)  || (MAX_PAR_NAME_LEN <= par_name_len) ||
            (0 >= par_value_len) || (MAX_PAR_VALUE_LEN <= par_value_len)) {
            return IPP_FAIL;
        }

        STRNCPY(par_name, p1 + 1, par_name_len);
        if ((0 == IPP_Strcmp(par_name, "i")) || (0 == IPP_Strcmp(par_name, "I"))) {
            /*input file*/
            STRNCPY(pSrcFileName, p2 + 1, par_value_len);
            nStart = 1;
        } else if ((0 == IPP_Strcmp(par_name, "o")) || (0 == IPP_Strcmp(par_name, "O"))) {
            /*output file*/
            IPP_Printf("Dst file is not supported, ignore\n");
            //STRNCPY(pDstFileName, p2 + 1, par_value_len);
        } else if ((0 == IPP_Strcmp(par_name, "l")) || (0 == IPP_Strcmp(par_name, "L"))) {
            /*log file*/
            //STRNCPY(pLogFileName, p2 + 1, par_value_len);
            IPP_Printf("Log file is not supported, ignore\n");
        } else if ((0 == IPP_Strcmp(par_name, "p")) || (0 == IPP_Strcmp(par_name, "P"))) {
            /*par file*/
            /*parse par file to fill pParSet*/
        } else if ((0 == IPP_Strcmp(par_name, "c")) || (0 == IPP_Strcmp(par_name, "C"))) {
            STRNCPY(par_value, p2 + 1, par_value_len);
            g_bOptChksum = IPP_Atoi(par_value);
        } else if (0 == IPP_Strcmp(par_name, "fmt")) {
            STRNCPY(par_value, p2 + 1, par_value_len);
            strm_fmt = IPP_Atoi(par_value);
            nStart = 0;
            nEnd = 1;
        } else {
            /*parse other parameters for encoder*/
        }
        if(nEnd == 1) {
        	/* we get a complete set of parameter for one pair */
            fp = IPP_Fopen(pSrcFileName, "rb");
            if(!fp) {
            	IPP_Printf("Error open input stream %s\n", pSrcFileName);
            	return IPP_FAIL;
            }
            *(pSrcFile+nTotal) 			  = fp;
        	pDecParSet->no_reordering     = 1;		//0; possible wrong display order, but okay for demo
        	pDecParSet->opt_fmt           = IPP_YCbCr422I;
        	pDecParSet->strm_fmt          = strm_fmt;
        	pDecParSet->bMultiIns        = 1;
			
        	if (0 == nTotal) {
            	pDecParSet->bFirstUser    = 1;
        	} else {
            	pDecParSet->bFirstUser    = 0;
        	}
        	nTotal++;
            if (nTotal >= MAX_MULTISTRM_NUM) {
                break;
            }

        	pDecParSet++;
        	nStart = 0;
        	nEnd   = 0;
        }

        pCur = p3;
    }
    
    if(nStart && !nEnd) {
    	IPP_Printf("Cmd error, incomplete stream info.\n");
    	*pTotalCnt = 0;
    	return IPP_FAIL;
    }
    
    *pTotalCnt = nTotal;
    
    return IPP_OK;
}


#if PURE_VMETA
int CodecTest(int argc, char **argv)
{
    int i, j, err;
    IPP_FILE *fin[MAX_MULTISTRM_NUM];
    IppVmetaDecParSet decpar[MAX_MULTISTRM_NUM];
    int thrd[MAX_MULTISTRM_NUM], hThread;
    struct ThreadPara para[MAX_MULTISTRM_NUM];
    int rtCode = 0;
    int nTotalStreamCnt  = 0;

    DBG_PRINTF("Entering:%s\n", __FUNCTION__);

	err = ParseVmetaDecCmd(argv[1], fin, decpar, &nTotalStreamCnt);
	if(err != IPP_OK) {
		IPP_Printf("Parse command line error.\n");
		return IPP_FAIL;
	}
    nActiveStreamCnt = nTotalStreamCnt;

    for (i = 0; i< nTotalStreamCnt; i++)  {
        para[i].fin           = fin[i];
        para[i].fout          = NULL;
        para[i].pDecParSet    = &(decpar[i]);
        para[i].pFunc         = &vmetadec;
        para[i].ID            = i;

        IPP_Printf("Create Decode Thread %d:\n", i);
        IPP_Printf("bMultiIns:  %d\n", para[i].pDecParSet->bMultiIns);
        IPP_Printf("bFirstUser: %d\n", para[i].pDecParSet->bFirstUser);
        rtCode = IPP_ThreadCreate(&thrd[i], 0, WorkThread, &para[i]);
		if(rtCode) {
			IPP_Printf("IPP_ThreadCreate failed return %d.\n", rtCode);
			return -3;
		}

        //kenter: why bFirstUser need to sleep 20ms?
        while(!bUsrInited[i]) {
            usleep(20);
        }
    }

    for (i = 0; i < nTotalStreamCnt; i++) {
    	IPP_ThreadDestroy(&thrd[i], 1);
        IPP_Printf("Destroy Decode Thread: %d\n", i);
    }

	for(i = 0; i<nTotalStreamCnt; i++) {
		IPP_Fclose(para[i].fin);
	}

    DBG_PRINTF("Exit:%s\n", __FUNCTION__);
    fflush(stdout);
    return 1;
}


#else

int compiz_init(const char *argv)
{
    int i, j, err;
    IppVmetaDecParSet decpar[MAX_MULTISTRM_NUM];
    struct ThreadPara para[MAX_MULTISTRM_NUM];
    FileReaderPara  file_param;
    int rtCode = 0;
    int dummy = 1;
    char *cmd = NULL;
    char compiz_cmd[1024];

    if (argv != NULL) {
        cmd = argv;
    } else {
#if ANDROID && USE_APK
        FILE *inCfgPar = fopen("/sdcard/compiz/vmeta_decoder.cfg", "r");
        MANMultiConfig *temp_config = NULL;
        if (NULL == inCfgPar) {
            IPP_Printf("Failed to open config file!\n");
            return IPP_STATUS_ERR;
        }

        MANGetPar(&temp_config, inCfgPar);
        strcpy(compiz_cmd, temp_config->cmdLine);
        cmd = compiz_cmd;
        MANCleanupPar(temp_config, inCfgPar);
#else
        cmd = "-i:/sdcard/compiz/streams/h264dec/astro.264 -fmt:5 -i:/sdcard/compiz/streams/h264dec/gijoe.264 -fmt:5 -i:/sdcard/compiz/streams/h264dec/iceage3.264 -fmt:5 -i:/sdcard/compiz/streams/h264dec/madagascar.264 -fmt:5";
#endif
    }

    IPP_InitPerfCounter();

    /* Init and Create File Thread */
	err = ParseVmetaDecCmd(cmd, fin, decpar, &nTotalStreamCnt);
    if(err != IPP_OK) {
		IPP_Printf("Parse command line error.\n");
		return IPP_FAIL;
	}
    nActiveStreamCnt = nTotalStreamCnt;

    IPP_MutexCreate(&ActiveStrmCnt_Mutex);
    IPP_EventCreate(&pEventStrmResPoolNotEmpty);
    IPP_EventCreate(&pEventSuspend);

    IPP_EventSet(pEventSuspend);

    for ( i = 0; i < nTotalStreamCnt; i++ ) {
        BufQueue_Init(&StrmResPool[i]);
        BufQueue_Init(&StrmBufFIFO[i]);

        /* Init stream buffer for each streams */
        for (j = 0; j < STREAM_BUF_NUM; j++) {
            BitStreamGroup[i][j].pBuf = (Ipp8u*)vdec_os_api_dma_alloc(STREAM_BUF_SIZE, 
                                        VMETA_STRM_BUF_ALIGN, &(BitStreamGroup[i][j].nPhyAddr));
            if (NULL == BitStreamGroup[i][j].pBuf) {
                IPP_Printf("error: no memory for stream buffer!\n");
                return IPP_FAIL;
            }
            BitStreamGroup[i][j].nBufSize                  = STREAM_BUF_SIZE;
            BitStreamGroup[i][j].nDataLen                  = 0;
            BitStreamGroup[i][j].nOffset                   = 0;
            BitStreamGroup[i][j].nBufProp                  = 0;
            BitStreamGroup[i][j].pUsrData0                 = (void*)j;
            BitStreamGroup[i][j].pUsrData1                 = NULL;
            BitStreamGroup[i][j].pUsrData2                 = NULL;
            BitStreamGroup[i][j].pUsrData3                 = NULL;
            BitStreamGroup[i][j].nFlag                     = IPP_VMETA_STRM_BUF_PART_OF_FRAME;
            while(BufQueue_EnQueue(&StrmResPool[i], &BitStreamGroup[i][j]) < 0) {
                /* Do Nothing */;
            }
            PRINTF_THREAD("[Decode Thread %d] Enqueue StrmResPool[%d] 0x%p\n", i, i, &BitStreamGroup[i][j]);
        }
    }
    IPP_EventSet(pEventStrmResPoolNotEmpty);

    file_param.stream_cnt = nTotalStreamCnt;
    file_param.fp_array   = (FILE**)fin;
    file_param.pFunc      = file_reader;
    rtCode = IPP_ThreadCreate(&FileThread, 0, FileReaderWorkThread, &file_param);
	if(rtCode) {
		IPP_Printf("Create FileReader failed return %d.\n", rtCode);
		return -3;
	}

    /* Init and Create Decoder Threads */
    SortedList_Init(&SortList, nActiveStreamCnt);    

    for (i = 0; i< nTotalStreamCnt; i++)  {
        para[i].fout          = NULL;
        para[i].pDecParSet    = &(decpar[i]);
        para[i].pFunc         = &vmetadec;
        para[i].ID            = i;
		
	    BufQueue_Init(&PicResPool[i]);
	    
		/* move out from each thread, use main thread to create and destroy the buffers,
		   to make sure GPU thread can access them correctly before exiting */
		for (j = 0; j < PICTURE_BUF_NUM; j++) {
			PictureGroup[i][j].pBuf                = NULL;
	        PictureGroup[i][j].nPhyAddr            = 0;
	        PictureGroup[i][j].nBufSize            = 0;
	        PictureGroup[i][j].nDataLen            = 0;
	        PictureGroup[i][j].nOffset             = 0;
	        PictureGroup[i][j].nBufProp            = 0;
	        PictureGroup[i][j].pUsrData0           = (void*)j;
	        PictureGroup[i][j].pUsrData1           = (void*)0;
	        PictureGroup[i][j].pUsrData2           = (void*)i;
	        PictureGroup[i][j].pUsrData3           = NULL;
	        PictureGroup[i][j].nFlag               = 0;
	        //new property
	        PictureGroup[i][j].nBufProp            = VMETA_BUF_PROP_CACHEABLE;
	        IPP_Memset((void*)&(PictureGroup[i][j].pic), 0, sizeof(IppPicture));
            while(BufQueue_EnQueue(&PicResPool[i], &PictureGroup[i][j]) < 0) {
                /* Do Nothing */;
            }
			PRINTF_THREAD("[Decode Thread %d] Enqueue PicResPool[%d] 0x%p\n", i, i, &PictureGroup[i][j]);
		}

        IPP_Printf("Create Decode Thread %d:\n", i);
        IPP_Printf("bMultiIns:  %d\n", para[i].pDecParSet->bMultiIns);
        IPP_Printf("bFirstUser: %d\n", para[i].pDecParSet->bFirstUser);
        rtCode = IPP_ThreadCreate(&thrd[i], 0, WorkThread, &para[i]);
		if(rtCode) {
			IPP_Printf("IPP_ThreadCreate decoder thread failed return %d.\n", rtCode);
			return -3;
		}

        //kenter: why bFirstUser need to sleep 20ms?
        while(!bUsrInited[i]) {
            usleep(20);
        }
    }

#if !ANDROID
    /* Init and Create GPU Thread */ 
	rtCode = IPP_ThreadCreate(&hThread, 0, GPUWorkThread, dummy);
	if(rtCode) {
		IPP_Printf("IPP_ThreadCreate GPU thread failed return %d.\n", rtCode);
		return -3;
	}
#endif

    return 0;
}

int compiz_deinit()
{
    int i, j;

    /* DeInit File Reader Thread */
    IPP_ThreadDestroy(&FileThread, 1);
    IPP_Printf("[File Reader] Destroy.\n");

    /* DeInit Decoder Threads */
    for (i = 0; i < nTotalStreamCnt; i++) {
    	IPP_ThreadDestroy(&thrd[i], 1);
        IPP_Printf("Destroy Decode Thread: %d\n", i);
    }

#if !ANDROID
    /* DeInit GPU Thread */
    IPP_ThreadDestroy(&hThread, 1);     // destroy GPU thread
    IPP_Printf("[GPU Thread] Destroy.\n");
#endif

    /* Release All resources */
	for (i = 0; i< nTotalStreamCnt; i++)  {
    	for (j = 0; j < PICTURE_BUF_NUM; j++) {
        	if (PictureGroup[i][j].pBuf) {
            	vdec_os_api_dma_free(PictureGroup[i][j].pBuf);
        	}
    	}
    }
    DBG_PRINTF("CodecTest: Release StrmCnt*PicNum=%d x %d Picture Buffers!\n", nTotalStreamCnt, PICTURE_BUF_NUM);

    for (i = 0; i < nTotalStreamCnt; i++) {
        for (j = 0; j < STREAM_BUF_NUM; j++ ) {
            if (BitStreamGroup[i][j].pBuf) {
                vdec_os_api_dma_free(BitStreamGroup[i][j].pBuf);
            }
        }
    }
    DBG_PRINTF("CodecTest: Release StrmCnt*StrmResNum=%d x %d Stream Buffers!\n", nTotalStreamCnt, STREAM_BUF_NUM);

	for(i = 0; i < nTotalStreamCnt; i++) {
		BufQueue_DeInit(&PicResPool[i]);
        BufQueue_DeInit(&StrmResPool[i]);
        BufQueue_DeInit(&StrmBufFIFO[i]);
		IPP_Fclose(fin[i]);
	}

    SortedList_DeInit(&SortList);
    DBG_PRINTF("CodecTest: Release %d Pic/Strm Pool, FIFO and %d PicGrp!\n", nTotalStreamCnt, nTotalStreamCnt);
    IPP_MutexDestroy(ActiveStrmCnt_Mutex);
    IPP_EventDestroy(pEventStrmResPoolNotEmpty);
    IPP_EventDestroy(pEventSuspend);

    return 0;
}

int CodecTest(int argc, char **argv)
{
    DBG_PRINTF("Entering:%s\n", __FUNCTION__);

    compiz_init(argv[argc-1]);

#if ANDROID && !USE_APK
    CreateWindowSystem();
    if (CreateGPURes() < 0) {
        IPP_Printf("Failed to create GPU Res\n");
    }
    while(1) {
        if (DoGPURender() < 0) {
            break;
        }
    }
    DestroyGPURes();
    DestroyWindowSystem();
#endif

    compiz_deinit();

    DBG_PRINTF("Exit:%s\n", __FUNCTION__);
    fflush(stdout);

    return 1;
}

#endif

/* EOF */


