/***************************************************************************************** 
Copyright (c) 2009, Marvell International Ltd. 
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Marvell nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MARVELL ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MARVELL BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************/

#include "misc.h"
#include "manchac.h"

#ifdef _IPP_WCE

#ifdef DEFAULT_TIMINGFUNC_START
  #undef DEFAULT_TIMINGFUNC_START
  #define DEFAULT_TIMINGFUNC_START		IPP_TimeGetTickCount 
 #endif
 #ifdef DEFAULT_TIMINGFUNC_STOP
  #undef DEFAULT_TIMINGFUNC_STOP
  #define DEFAULT_TIMINGFUNC_STOP		IPP_TimeGetTickCount
 #endif

#elif (defined _IPP_LINUX)
 #ifdef TIMING_PMU
  #ifdef DEFAULT_TIMINGFUNC_START
   #undef DEFAULT_TIMINGFUNC_START
   #define DEFAULT_TIMINGFUNC_START       	start_pmu
  #endif  /* DEFAULT_TIMINGFUNC_START */
  #ifdef DEFAULT_TIMINGFUNC_STOP
   #undef DEFAULT_TIMINGFUNC_STOP
   #define DEFAULT_TIMINGFUNC_STOP        	stop_pmu
  #endif  /* DEFAULT_TIMINGFUNC_STOP */
 #else /* !TIMING_PMU */
 #ifdef DEFAULT_TIMINGFUNC_START
   #undef DEFAULT_TIMINGFUNC_START
   #define DEFAULT_TIMINGFUNC_START              IPP_TimeGetTickCount
  #endif  /* DEFAULT_TIMINGFUNC_START */
  #ifdef DEFAULT_TIMINGFUNC_STOP
   #undef DEFAULT_TIMINGFUNC_STOP
   #define DEFAULT_TIMINGFUNC_STOP               IPP_TimeGetTickCount
  #endif  /* DEFAULT_TIMINGFUNC_STOP */
 #endif	/* TIMING_PMU */
#endif


#if (defined _VMETADEC)
 #define MAN_CODEC_NAME     "vMeta Based Video Decoder"
 #define MAN_LOG_FILE       "vmeta_decoder.log"
 #define MAN_CODEC_INDEX    IPP_VIDEO_INDEX
 #include "../../vmetadec/src/vdec_wrapper.c"
 #define MAN_CONFIG_FILE    "vmeta_decoder.cfg"

#else
        #error one codec name must be defined
#endif

#include "manchac.c"

/* EOF */

