/******************************************************************************/
* Manchac Multimedia Performance Benchmark Toolsuite 5.2.2
/******************************************************************************/

0. Version and Feature enhancement from last version
1. Feature 
	1.1 Coverred codecs
	1.2 Performance Matrix
	1.3 Performance Log and Report
	1.4 Stream set
2. Directories
3. How to build 
4. How to run
5. How to generate performance report
6. Test configurations
	6.1 Common parameters
	6.2 Display parameters
	6.3 Audio waveout parameters	
	6.4 Performance counter parameters
7. Limitations


-------------------------------------------------------------------------------

0. Version

Manchac 5.2.1 is fully integrated with IPP sample code. It should be built with 
IPP sample code. 

Changes from last version are:
1) WMV added to Linux branch.
2) config files and report template are modified to accomodate latest stream
   set
3) -DTIMING_PMU is added back. Now for MH and PJ5, PMU is not defaultly enabled
   for timing as the 32-bit timer tends to overflow in JPEG decoder and encoder
   cases.  User can enable it manual by adding -DTIMING_PMU.  for TTC, as cycle
   timer is 64-bit, the PMU timing is defaultly enabled.
   
-------------------------------------------------------------------------------

1. Feature 

1.1 Coverred codecs

H.264 BP decoder
H.264 BP encoder
MPEG4 SP/ASP/BVOP decoder
MPEG4 SP encoder
H.263 Profile-0 and Profile-3 encoder
WMV9 SP/MP decoder
MPEG2 Decoder
JPEG BMP/YUV encoder
JPEG decoder (ROI, Rotation integrated)
AAC-LC/AAC+/AAC+v2 decoder
MP3 decoder
WMA decoder
vMeta-based Video decoder

1.2 Performance Matrix

For each case of codec benchmarking, it repeats 4 times.  Following performance 
matrix are reported:

Total Frames            : Frame count the test codec processed
Total Codec Tick        : Timing of codec core routines in unit of microsecond 
			  (minimal overhead of IO)
Total Device Tick       : Timing spent for device IO (display, waveout) in unit of 
			  microsecond
Total Application Tick  : Timing of complete codec processing in unit of microsecond 
			  (including FILE IO, device IO)
Codec Tick/Frame        : Total Codec Tick / Total Frames
Application Tick/Frame  : Total Application Tick / Total Frames
Codec FPS               : Total Frames / Total Codec Tick * 1000000
Applicaton FPS          : Total Frames / Total Application Tick * 1000000
Codec CPU%              : Total Codec Tick / Total Application Tick %
Application CPU%        : Total Tick spend in this process / Total Application Tick %
Performance Count Event 0 : event setting dependant, measurement period is same 
			    as Total Codec Tick
Performance Count Event 1 : event setting dependant, measurement period is same 
			    as Total Codec Tick
Performance Count Event 2 : event setting dependant, measurement period is same 
			    as Total Codec Tick
Performance Count Event 3 : event setting dependant, measurement period is same 
		 	    as Total Codec Tick
Performance Count Event 4 : event setting dependant, measurement period is same 
			    as Total Codec Tick

Performance Count Event may be 0-3 or 0-4 which is platform performance counter 
implementation dependent.  Performance Count Events are only shown when macro 
ENABLE_PMU is enabled. (see 3. How to build).

1.3 Performance Log and Report

Manchac outputs performance report to both stdout and log file.  Log file name is 
named as codec_name.log. For example, h264_decoder.log, h264_encoder.log, etc.

1.4 Stream set

There is an pre-defined stream set which cover most common usage scenarios and 
codec features. All the streams are summarized in stream/StreamList.xls.

-------------------------------------------------------------------------------

2. Directories

<Manchac root>/
src/				root of source codes, cross-platform sources are placed here
    wmmx2_linux			Linux platform sources
    wmmx2_wince			WM platform sources
build/				root of build makefile, projects
    wmmx2_linux			makefile for Linux
    wmmx2_wince			build projects for WM
    log				Log files for build
    obj				Temp obj file placeholder
bin/				root of generated binaries
    pre-built/			pre-built binaries
	wmmx2_linux/		pre-built binaries for Linux	
	    mh			for Monahans/Tavor Platform
	    ttc			for TavorL platform
	    pj5			for PJ5 platform
config/				config files 
    pmu				example config files of event settings when PMU is enabled				
script				log file post-processing script, in perl.
report				Performance report template which can be completed
				by importing post-processed log files.
stream				StreamList.xls listing predefined stream features. 
				Streams will be distributed in seperate package

-------------------------------------------------------------------------------
				
3. How to build

3.1 Linux
cd <Manchac root>/build/wmmx2_linux

edit makefile and modify macros as desired:
PATH_GNU_BIN=
PLATFORM=
FREQ=
INSTALL=

and type make.  Binaries are generated under <Manchac root>/bin

type make <codec_name> will build each codec individually. For example,
make h264_decoder

For Linux version above 2.6.28, please enable macro _DISPLAY_LCDDRIVER_NEW
for incorporating latest LCD driver APIs. 

3.2 WM
Make sure VS2005 is installed on host, go to <Manchac root>/build/wmmx2_wince, 
double click Manchac.sln, and the solution with all codec project files is opened.
Select Rebuilt for each codec project and binaries are generated under 
<Manchac root>/bin.

Notes PMU is not supported for WM OS. 
WMA and WMV benchmarking are not supported.

-------------------------------------------------------------------------------

4. How to run

Copy all the binaries to target platform, copy all the config files to same 
directory of binaries, copy and unpack streams package (provided seperately) under 
same directory too.  Then run each of program without any argument.  After program 
finishes, a <codec_name>.log is generated in same directory.

On Linux OS, for those which has TIMING_PMU enabled, please prepare the PMU 
kernel module accordingly (contact BSP team or Cathy Bao for the modules) and 
insert the module before launching test.

On WM PPC OS, batch script tool (MortScript-4.2-PPC.cab) is provided and the 
script (.mscr) is specific for that tool only. 

All batch scripts can be found in script directory.

-------------------------------------------------------------------------------

5. How to generate performance report

For each case of benchmarking, it repeats 4 times. First time is acted as dry
run for cache warm-up.  Performance data from next three runs are averaged as
final data.  The perfomance data post-processing script is for two purposes:
1) Average the 2-4 runs data.
2) Convert from human-readable data to excel importable data format

Copy log files to <Manchac root>/script. If all 11 log files are ready, run 
convert.sh.  If partial of logs are ready, use
./getmanchacdata_v3.pl <num of logs> <log_0.log> <log_1.log> .. <log_n.log>

Output will be manchac.csv.  Import this to <Manchac root>/report/ 
ManchacReport_Fullset_template.xls. This will give a basic performance report 
ready for further customization.

-------------------------------------------------------------------------------

6. Test configurations

config file is in the format of:
!!!Manchac!!!

BEGIN
PARAMETER	= VALUE
PARAMETER	= VALUE
...
END

BEGIN
PARAMETER	= VALUE
PARAMETER	= VALUE
...
END

Each BEGIN-END block stands for one test case. Following sections explain 
meanings of most available parameters.

6.1 Common parameters

Input.Cmdline		: Input Cmdline which is parsed to each codec CodecTest.
			  Cmdline format is codec spcified and fully owned by 
			  each IPP codec.

6.2 Display parameters
Output.DisplayEnable	: 0 -- disable playback display on LCD
			  1 -- enable playback display on LCD	  
Output.FrameRate	: 1-30 -- playback frame rate
			  0    -- full speed
			  
6.3 Audio waveout parameters
Output.WaveOutEnable 	: 0 -- wave out disabled
			  1 -- wave out enabled	

6.8 Performance counter parameters

PerfCounter.Enable	: 0 -- disable
			  1 -- enable
PerfCounter.event0-3	: event mode

Due to legacy reason, MH/TV is only configured by two events (PerfCounter.event0
and PerfCounter.event1), each of event mode configuring two events actually.
For example, if PerfCounter.event0 = "IC_EFFICIENCY", it means counter1 is 
set to "Instruction Retired" and counter2 is set to "Instruction Cache Miss".

For each of platform supported, an h.264 decoder example config file is provided
that has performance counter enabled and has most typical performance counter 
events configured.  The example config files are:

<Manchac root>/config/pmu/
	h264_decoder_mhppmu_example.cfg	for MH/TV family
	h264_decoder_ttcpmu_example.cfg	for TTC
	h264_decoder_pj5pmu_example.cfg	for PJ5

However, the supported events are not limited to those listed in config files. 
For a full set of events, please refer to related core specifications.

-------------------------------------------------------------------------------
			  
7. Limitations	

1. Display is enabled only in CLIP_MODE. No post-processing is enabled 
   (resize, rotate, etc). Videos are clipped by screen size.
2. PMU is not supported on WM. 
3. WMA and WMV are only benchmarked through Windows Media Player on WM, Manchac 
   doesn't support them on WM yet.
4. Multi-media framework benchmarking is an open area.

/* EOF */

