/***************************************************************************************** 
Copyright (c) 2009, Marvell International Ltd. 
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Marvell nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MARVELL ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MARVELL BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************/

#include "manchac.h"

/* multiple stream */
void MANPerformaneStatistics(MANPerfCT *pct)
{
        int i;
        int frames = 0;
        for (i=0;i<MAX_MULTISTRM_NUM;i++) {
                if(pct->ulFrameNumber[i] == 0) 
                        continue;
                pct->codecTickPerFrame[i]  = (float)pct->ulCodecTick[i] / pct->ulFrameNumber[i];
                pct->appTickPerFrame[i]	   = (float)pct->ulAppTick / pct->ulFrameNumber[i];
                pct->codec_fps[i]          = (float)pct->ulFrameNumber[i] * 1000000. / pct->ulCodecTick[i];
                pct->app_fps[i]  		   = (float)pct->ulFrameRenderNumber[i] * 1000000. / pct->ulAppTick;
#ifdef HWCODEC
                pct->codec_cpu[i]          = (float)pct->ulCodecCPUTick[i] / pct->ulAppTick;
#else   
                pct->codec_cpu[i]          = (float)pct->ulCodecTick[i] / pct->ulAppTick;
#endif  
                frames += pct->ulFrameNumber[i];
        }
        pct->app_cpu            = 0;
        pct->device_fps = (float)pct->ulDeviceFrameNumber * 1000000. / pct->ulDeviceTick;
}

void MANPerformanceLog(MANPerfCT *pct, IPP_FILE *dst)
{
        int i;
        for(i=0;i<MAX_MULTISTRM_NUM;i++) {  
                if(pct->ulFrameNumber[i] == 0) 
                        continue;        		
                MANPrintf(dst, "*** Stream No.%d Summary ***\n", i);
                MANPrintf(dst, "Total Decoded Frames    = %lu\n", pct->ulFrameNumber[i]);
                MANPrintf(dst, "Total Rendered Frames   = %lu\n", pct->ulFrameRenderNumber[i]);
                MANPrintf(dst, "Frame drop rate         = %.2f%%\n", (float)100.-(float)100.*pct->ulFrameRenderNumber[i]/pct->ulFrameNumber[i]);
                //MANPrintf(dst, "Total Codec Tick        = %Ld\tus\n", pct->ulCodecTick[i]);
                //MANPrintf(dst, "Codec Tick/Frame        = %.02f\tus\n", pct->codecTickPerFrame[i]);
                //MANPrintf(dst, "Application Tick/Frame  = %.02f\tus\n", pct->appTickPerFrame[i]);
                MANPrintf(dst, "Decode FPS              = %.02f\t\n", pct->codec_fps[i]);
                //MANPrintf(dst, "Codec CPU%%             = %.02f%%\n", pct->codec_cpu[i]*100);
                MANPrintf(dst, "Render FPS              = %.02f\t\n", pct->app_fps[i]);
        }
        MANPrintf(dst, "*** GPU Summary ***\n");
        MANPrintf(dst, "Total Scene Rendered    = %lu\n", pct->ulDeviceFrameNumber);
        MANPrintf(dst, "Total Scene Updated     = %lu\n", pct->ulDeviceFrameNumber2);
        MANPrintf(dst, "Total Playback Duration = %Ld\tus\n", pct->ulAppTick);
        MANPrintf(dst, "Total GPU Tick       	= %Ld\tus\n", pct->ulDeviceTick);
        MANPrintf(dst, "GPU FPS              	= %.02f\n", pct->device_fps);
        MANPrintf(dst, "Overall FPS             = %.2f\n", (float)pct->ulDeviceFrameNumber * 1000000.f / pct->ulAppTick);
        MANPrintf(dst, "\n");
}
/* EOF */

