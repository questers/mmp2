#ifndef __DEMO_H__
#define __DEMO_H__

#ifdef __cplusplus
extern "C" {
#endif

int CreateGPURes();
int DestroyGPURes();

int CreateWindowSystem();
int DestroyWindowSystem();

int  DoGPURender();
void WindowReshape(int x, int y, int width, int height);

/* This dummy arg is used to avoid create_thread fail in IPP_ThreadCreate */
void GPUWorkThread(int dummy);

int GPUInitGLTexture();
int GPUDeInitGLTexture();

int KeyPressHandler(int total_frames, int keycode);

#ifdef __cplusplus
}
#endif

#endif