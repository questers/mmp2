#ifndef __COMPIZ_JNI_LIB_H__
#define __COMPIZ_JNI_LIB_H__

#ifdef __cplusplus
extern "C" {
#endif
#include <nativehelper/jni.h>

/* COMPIZLib interfaces */
void Java_com_android_compiz_COMPIZLib_pause(JNIEnv *env, jobject obj);
void Java_com_android_compiz_COMPIZLib_resume(JNIEnv *env, jobject obj);

void Java_com_android_compiz_COMPIZLib_init(JNIEnv *env, jobject obj);
void Java_com_android_compiz_COMPIZLib_deinit(JNIEnv *env, jobject obj);

void Java_com_android_compiz_COMPIZLib_step(JNIEnv *env, jobject obj);

void Java_com_android_compiz_COMPIZLib_change(JNIEnv *env, jobject obj, int width, int height);

void Java_com_android_compiz_COMPIZLib_keyPressed(JNIEnv *env, jobject obj, int keyCode);

#ifdef __cplusplus
}
#endif

#endif