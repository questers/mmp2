/***************************************************************************************** 
Copyright (c) 2009, Marvell International Ltd. 
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Marvell nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MARVELL ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MARVELL BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************/

#ifndef _MANCHAC_H_
#define _MANCHAC_H_

#include <stdio.h>
#include "misc.h"
#include "vdec_wrapper.h"
#ifdef __cplusplus
extern "C" {
#endif

/***********************************/
/* General */
/***********************************/
#define MANCHAC_VER     "v5.2.2"
#define IPP_VER         "v5.2.2"

/* manchac run modes */
typedef enum{
        MAN_SERIAL_MODE,
        MAN_PARALLEL_MODE,
        MAN_LOOP_MODE,
        MAN_SINGLE_MODE
}MANRunMode;

/* add specific one because IPP_Printf is turned off in manchac build */
#define MANPrintf       fprintf

/***********************************/
/* configurations */
/***********************************/
/* display mode */
typedef enum {
        MAN_DISPLAY_CLIP,
        MAN_DISPLAY_RESIZE,     /* NOT SUPPORTED */
        MAN_DISPLAY_ROTATE      /* NOT SUPPORTED */
}ManDisplayMode;

/* Performance counter configuration */
typedef struct _MANPerfCntConfig {
        int     bCntEnable;
        int     nCntMode[4];
} MANPerfCntConfig;

/* Display configuration */
typedef struct _MANDisplayConfig {
        int             bDisplayEnable;
        int             nFrameRate;
        ManDisplayMode  nMode;
        int             nDeinterlaceMode;
} MANDisplayConfig;

/* Waveout configuration */
typedef struct _MANWaveoutConfig {
        int     bWaveoutEnable;
} MANWaveoutConfig;

/* multiple configuration */
typedef struct _MANMultiConfig {
        char                     cmdLine[256];
        union _OutputConfig {
                MANDisplayConfig displayConfig;
                MANWaveoutConfig waveoutConfig;
        } UnionOutputConfig;
        MANPerfCntConfig         cntConfig;
        struct _MANMultiConfig   *next;
} MANMultiConfig;

extern MANMultiConfig  *gCurrConfig;
#define MANGetDisplayEnable()   gCurrConfig->UnionOutputConfig.displayConfig.bDisplayEnable
#define MANGetDisplayMode()     gCurrConfig->UnionOutputConfig.displayConfig.nMode
#define MANGetFrameRate()       gCurrConfig->UnionOutputConfig.displayConfig.nFrameRate 
#define MANGetDeinterlaceMode() gCurrConfig->UnionOutputConfig.displayConfig.nDeinterlaceMode
#define MANSetDeinterlaceMode(mode)	gCurrConfig->UnionOutputConfig.displayConfig.nDeinterlaceMode = mode;
#define MANGetWaveoutEnable()   gCurrConfig->UnionOutputConfig.waveoutConfig.bWaveoutEnable


IppCodecStatus MANGetPar(MANMultiConfig **mCfg, IPP_FILE *pfPara);
void MANCleanupPar(MANMultiConfig *mCfg, IPP_FILE *pfPara);


/***********************************/
/* performance */
/***********************************/
typedef struct _MANPerfCT{
		unsigned long			ulDeviceFrameNumber;
		unsigned long			ulDeviceFrameNumber2;
        unsigned long           ulFrameNumber[MAX_MULTISTRM_NUM];
        unsigned long			ulFrameRenderNumber[MAX_MULTISTRM_NUM];
        unsigned long long      ulCodecTick[MAX_MULTISTRM_NUM];         /* raw codec time (us) */
        unsigned long long      ulCodecCPUTick[MAX_MULTISTRM_NUM];      /* raw codec CPU time (us), extended for HW codecs */
        unsigned long long      ulDeviceTick;        					/* device time (us)    */
        unsigned long long      ulAppTick;           					/* application time (us)   */
        unsigned long long      ulThreadTick;        					/* thread time (us) (user mode plus system mode)  */
        float                   codecTickPerFrame[MAX_MULTISTRM_NUM];   /* tick per frame */
        float                   appTickPerFrame[MAX_MULTISTRM_NUM];
        float 					ppTickPerFrame;
        float                   codec_fps[MAX_MULTISTRM_NUM];           /* fps */
        float					device_fps;
        float                   app_fps[MAX_MULTISTRM_NUM];
        float                   codec_cpu[MAX_MULTISTRM_NUM];           /* cpu% */
        float                   app_cpu;
        float                   system_cpu;
        float                   user_cpu;
        MANPerfCntConfig        *pCntConfig;
        unsigned long long      cnt[5];
}MANPerfCT;

/* performance counter APIs */
void open_pmu();
long long start_pmu();
long long stop_pmu();
void close_pmu();

typedef struct _PMUModeDesp {
        int counterID;
        int mode;
        char desp[40];
} PMUModeDesp;

extern const PMUModeDesp modeDesp[];
extern const int modeDesp_size;

#define MANPerformanceInit(pct)    IPP_Memset(pct, 0, sizeof(MANPerfCT)) 
#define MANPerformanceDeinit(pct)  \
        close_pmu();

void MANPerformaneStatistics(MANPerfCT *pct);
void MANPerformanceLog(MANPerfCT *pct, IPP_FILE *dst);

/***********************************/
/* Log */
/***********************************/
#define MAN_LOG_HEADER(dst, codecName)\
        MANPrintf((dst), "===============================================\n");\
        MANPrintf((dst), "Manchac %s - %s Performance Report\n", MANCHAC_VER, (codecName));\
        MANPrintf((dst), "===============================================\n\n");

#define MAN_LOG_CASENAME(dst, caseName) \
        MANPrintf((dst), "\nCase Name: %s\n\n", (caseName));

/***********************************/
/* Codec entry */
/***********************************/
int CodecTest(int argc, char** argv);

#ifdef __cplusplus
}
#endif

#endif /* _MANCHAC_H_ */

/* EOF */


