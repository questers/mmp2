#ifndef _GLU_GLUT_LIB_H
#define _GLU_GLUT_LIB_H

#include <stdlib.h>
#include <stdio.h>
#include <EGL/egl.h>
#include <EGL/eglplatform.h>
#include <GLES/gl.h>
#include <GLES/glext.h>

#include <string.h>

#include <math.h>

#ifndef __LINUX__
#include <windows.h>
#endif


#define PI					3.14159265358979323846
#define CACHE_SIZE			240
#define COS					cos
#define SIN					sin
#define SQRT				sqrt
#ifndef M_PI
#define M_PI                PI
#endif



//GLU MACRO
//////////////////////////////////////////////////////////////////////////
/* Errors: (return value 0 = no error) */
#define GLU_INVALID_ENUM        100900
#define GLU_INVALID_VALUE       100901
#define GLU_OUT_OF_MEMORY       100902
#define GLU_INCOMPATIBLE_GL_VERSION     100903

/* StringName */
#define GLU_VERSION             100800
#define GLU_EXTENSIONS          100801

/* Boolean */
#define GLU_TRUE                GL_TRUE
#define GLU_FALSE               GL_FALSE


/****           Quadric constants               ****/

/* QuadricNormal */
#define GLU_SMOOTH              100000
#define GLU_FLAT                100001
#define GLU_NONE                100002

/* QuadricDrawStyle */
#define GLU_POINT               100010
#define GLU_LINE                100011
#define GLU_FILL                100012
#define GLU_SILHOUETTE          100013

/* QuadricOrientation */
#define GLU_OUTSIDE             100020
#define GLU_INSIDE              100021
//////////////////////////////////////////////////////////////////////////
#define DIFF3(_a,_b,_c) { \
	(_c)[0] = (_a)[0] - (_b)[0]; \
	(_c)[1] = (_a)[1] - (_b)[1]; \
	(_c)[2] = (_a)[2] - (_b)[2]; \
}
#define __GLU_INIT_SWAP_IMAGE void *tmpImage
#define __GLU_SWAP_IMAGE(a,b) tmpImage = a; a = b; b = tmpImage;
#define __GLU_SWAP_2_BYTES(s)\
	(GLushort)(((GLushort)((const GLubyte*)(s))[1])<<8 | ((const GLubyte*)(s))[0])

#define __GLU_SWAP_4_BYTES(s)\
	(GLuint)(((GLuint)((const GLubyte*)(s))[3])<<24 | \
	((GLuint)((const GLubyte*)(s))[2])<<16 | \
	((GLuint)((const GLubyte*)(s))[1])<<8  | ((const GLubyte*)(s))[0])


typedef struct MRVL_GLUquadic{
	GLint	normals;
	GLboolean	textureCoords;
	GLint	orientation;
	GLint	drawStyle;
} MRVL_GLUquadricObj;

typedef struct PSM{
	GLint pack_alignment;
/*	GLint pack_row_length;
	GLint pack_skip_rows;
	GLint pack_skip_pixels;
	GLint pack_lsb_first;
	GLint pack_swap_bytes;
	GLint pack_skip_images;
	GLint pack_image_height;*/

	GLint unpack_alignment;
/*	GLint unpack_row_length;
	GLint unpack_skip_rows;
	GLint unpack_skip_pixels;
	GLint unpack_lsb_first;
	GLint unpack_swap_bytes;
	GLint unpack_skip_images;
	GLint unpack_image_height;*/
} PixelStorageModes;

static void doughnut(GLfloat r, GLfloat R, GLint nsides, GLint rings);
static void drawBox(GLfloat size, GLenum type);
static void initQuadObj(void);
static void normalize(float v[3]);
static void cross(float v1[3], float v2[3], float result[3]);
static void crossprod(GLfloat v1[3], GLfloat v2[3], GLfloat prod[3]);

static void initDodecahedron(void);
static void pentagon(int a, int b, int c, int d, int e, GLenum shadeType);
static void dodecahedron(GLenum type);
static int computeLog(GLuint value);
static int nearestPower(GLuint value);
static void retrieveStoreModes(PixelStorageModes *psm);
static GLint elements_per_group(GLenum format, GLenum type);
static GLint bytes_per_element(GLenum type);
static GLint image_size(GLint width, GLint height, GLenum format, GLenum type);
static void halve1Dimage_ubyte(GLint components, GLuint width, GLuint height,
							   const GLubyte *dataIn, GLubyte *dataOut,
							   GLint element_size, GLint ysize,
							   GLint group_size);
static void halveImage_ubyte(GLint components, GLuint width, GLuint height,
							 const GLubyte *datain, GLubyte *dataout,
							 GLint element_size, GLint ysize, GLint group_size);
static void halve1Dimage_byte(GLint components, GLuint width, GLuint height,
							  const GLbyte *dataIn, GLbyte *dataOut,
							  GLint element_size,GLint ysize, GLint group_size);
static void halveImage_byte(GLint components, GLuint width, GLuint height,
							const GLbyte *datain, GLbyte *dataout,
							GLint element_size,
							GLint ysize, GLint group_size);
static void halve1Dimage_ushort(GLint components, GLuint width, GLuint height,
								const GLushort *dataIn, GLushort *dataOut,
								GLint element_size, GLint ysize,
								GLint group_size, GLint myswap_bytes);
static void halveImage_ushort(GLint components, GLuint width, GLuint height,
							  const GLushort *datain, GLushort *dataout,
							  GLint element_size, GLint ysize, GLint group_size,
							  GLint myswap_bytes);
static void halve1Dimage_short(GLint components, GLuint width, GLuint height,
							   const GLshort *dataIn, GLshort *dataOut,
							   GLint element_size, GLint ysize,
							   GLint group_size, GLint myswap_bytes);
static void halveImage_short(GLint components, GLuint width, GLuint height,
							 const GLshort *datain, GLshort *dataout,
							 GLint element_size, GLint ysize, GLint group_size,
							 GLint myswap_bytes);
static void halve1Dimage_uint(GLint components, GLuint width, GLuint height,
							  const GLuint *dataIn, GLuint *dataOut,
							  GLint element_size, GLint ysize,
							  GLint group_size, GLint myswap_bytes);
static void halveImage_uint(GLint components, GLuint width, GLuint height,
							const GLuint *datain, GLuint *dataout,
							GLint element_size, GLint ysize, GLint group_size,
							GLint myswap_bytes);
static void halve1Dimage_fixed(GLint components, GLuint width, GLuint height,
							 const GLfixed *dataIn, GLfixed *dataOut,
							 GLint element_size, GLint ysize,
							 GLint group_size, GLint myswap_bytes);
static void halveImage_fixed(GLint components, GLuint width, GLuint height,
						   const GLfixed *datain, GLfixed *dataout, GLint element_size,
						   GLint ysize, GLint group_size, GLint myswap_bytes);
static void halve1Dimage_float(GLint components, GLuint width, GLuint height,
							   const GLfloat *dataIn, GLfloat *dataOut,
							   GLint element_size, GLint ysize,
							   GLint group_size, GLint myswap_bytes);
static void halveImage_float(GLint components, GLuint width, GLuint height,
							 const GLfloat *datain, GLfloat *dataout,
							 GLint element_size, GLint ysize, GLint group_size,
							 GLint myswap_bytes);
static void scale_internal_ubyte(GLint components, GLint widthin,
								 GLint heightin, const GLubyte *datain,
								 GLint widthout, GLint heightout,
								 GLubyte *dataout, GLint element_size,
								 GLint ysize, GLint group_size);
static void scale_internal_byte(GLint components, GLint widthin,
								GLint heightin, const GLbyte *datain,
								GLint widthout, GLint heightout,
								GLbyte *dataout, GLint element_size,
								GLint ysize, GLint group_size);
static void scale_internal_ushort(GLint components, GLint widthin,
								  GLint heightin, const GLushort *datain,
								  GLint widthout, GLint heightout,
								  GLushort *dataout, GLint element_size,
								  GLint ysize, GLint group_size,
								  GLint myswap_bytes);
static void scale_internal_short(GLint components, GLint widthin,
								 GLint heightin, const GLshort *datain,
								 GLint widthout, GLint heightout,
								 GLshort *dataout, GLint element_size,
								 GLint ysize, GLint group_size,
								 GLint myswap_bytes);
static void scale_internal_uint(GLint components, GLint widthin,
								GLint heightin, const GLuint *datain,
								GLint widthout, GLint heightout,
								GLuint *dataout, GLint element_size,
								GLint ysize, GLint group_size,
								GLint myswap_bytes);
static void scale_internal_int(GLint components, GLint widthin,
							   GLint heightin, const GLint *datain,
							   GLint widthout, GLint heightout,
							   GLint *dataout, GLint element_size,
							   GLint ysize, GLint group_size,
							   GLint myswap_bytes);
static void scale_internal_float(GLint components, GLint widthin,
								 GLint heightin, const GLfloat *datain,
								 GLint widthout, GLint heightout,
								 GLfloat *dataout, GLint element_size,
								 GLint ysize, GLint group_size,
								 GLint myswap_bytes);

//GLU API
//////////////////////////////////////////////////////////////////////////
static void MRVL_gluMakeIdentityd(GLfloat m[16]);
void MRVL_gluPerspective(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar);
void MRVL_gluLookAt(GLfloat eyex, GLfloat eyey, GLfloat eyez, GLfloat centerx,
					GLfloat centery, GLfloat centerz, GLfloat upx, GLfloat upy,
					GLfloat upz);

MRVL_GLUquadricObj* MRVL_gluNewQuadric(void);
void MRVL_gluQuadricNormals(MRVL_GLUquadricObj *qobj, GLenum normals);
void MRVL_gluQuadricTexture(MRVL_GLUquadricObj *qobj, GLboolean textureCoords);
void MRVL_gluQuadricDrawStyle(MRVL_GLUquadricObj *qobj, GLenum drawStyle);
void MRVL_gluCylinder(MRVL_GLUquadricObj *qobj, GLfloat baseRadius, GLfloat topRadius, GLfloat height, GLint slices, GLint stacks);
void MRVL_gluCylinderPart(MRVL_GLUquadricObj *qobj, GLfloat baseRadius, GLfloat topRadius,
				  GLfloat height, GLint slices, GLint stacks, GLfloat partial);
void MRVL_gluSphere(MRVL_GLUquadricObj *qobj, GLfloat radius, GLint slices, GLint stacks);
void MRVL_gluSpherePart(MRVL_GLUquadricObj *qobj, GLfloat radius, GLint slices, GLint stacks, GLfloat slicesPartial, GLfloat stacksPartial);
static int MRVL_gluBuild2DMipmapLevelsCore(GLenum target, GLint internalFormat,
										   GLsizei width, GLsizei height,
										   GLsizei widthPowerOf2,
										   GLsizei heightPowerOf2,
										   GLenum format, GLenum type,
										   GLint userLevel,
										   GLint baseLevel,GLint maxLevel,
										   const void *data);
GLint MRVL_gluBuild2DMipmaps(GLenum target, GLint internalFormat,GLsizei width, GLsizei height,GLenum format, GLenum type,const void *data);

//GLUT API
//////////////////////////////////////////////////////////////////////////
void MRVL_glutSolidTorus(GLfloat innerRadius, GLfloat outerRadius,GLint nsides, GLint rings);
void MRVL_glutSolidCone(GLfloat base, GLfloat height, GLint slices, GLint stacks);
void MRVL_glutSolidCube(GLfloat size);
void MRVL_glutSolidSphere(GLfloat radius, GLint slices, GLint stacks);
void MRVL_glutSolidDodecahedron(void);

#endif

/* EOF */