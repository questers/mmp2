/******************************************************************************
// (C) Copyright 2009 Marvell International Ltd.
// All Rights Reserved
******************************************************************************/
#include "queue.h"
#include <stdio.h>
#include "misc.h"

//#define QUEUE_DEBUG

void PRINTF_QUEUE(const char *format, ...)
{
#ifdef QUEUE_DEBUG
#if ANDROID
    char buf[1024];
	va_list args;

	buf[1024-1] = '\0';

    va_start(args, format);
    vsprintf(buf,format, args);
    va_end(args);
    LOGD("%s", buf);
#else
    fprintf(stdout, format, __VA_ARGS__);
    fflush(stdout);
#endif
#endif
}

//////////////////////////////////////////////////////////////////////
// Buffer Queue
//////////////////////////////////////////////////////////////////////

void __cleanup_func(void *arg)
{
    pthread_mutex_t *pmutex = NULL;

    pmutex = (pthread_mutex_t*)arg;
    if (NULL == pmutex) {
        fprintf(stderr, "__cleanup_func error: NULL input!\n");
        fflush(stderr);
    }

    pthread_mutex_unlock(pmutex);
}

int BufQueue_Init(BufQueue *pq)
{
	int rt = 0;

    rt  = pthread_mutex_init(&pq->hMutex, NULL);
    rt |= pthread_cond_init(&pq->pEventNotEmpty, NULL);
    rt |= pthread_cond_init(&pq->pEventNotFull, NULL);
	pq->headIndex = 0;
	pq->count = 0;

    PRINTF_QUEUE("%s success!\n", __FUNCTION__);
	return rt;
}

void BufQueue_DeInit(BufQueue *pq)
{
    pthread_mutex_destroy(&pq->hMutex);
    pthread_cond_destroy(&pq->pEventNotEmpty);
    pthread_cond_destroy(&pq->pEventNotFull);

    PRINTF_QUEUE("%s success!\n", __FUNCTION__);
}

int BufQueue_EnQueue(BufQueue *pq,  BufType *pBufferHeader)
{
    unsigned int next;
		
    PRINTF_QUEUE("Entering %s: Queue=%p\n", __FUNCTION__, pq);

	pthread_mutex_lock(&pq->hMutex);
    while(BUFQUEUE_MAX < pq->count) {
        int status;
        PRINTF_QUEUE("Queue:%p is waiting for NotFull signal, count=%d!\n", pq, pq->count);
        pthread_cleanup_push(__cleanup_func, (void*)&pq->hMutex);
        status = pthread_cond_wait(&pq->pEventNotFull, &pq->hMutex);
        pthread_cleanup_pop(0);
        PRINTF_QUEUE("Queue:%p the NotFull signal is return with:%d, count=%d!\n", pq, status, pq->count);
        if ( status != 0 ) {
            pthread_mutex_unlock(&pq->hMutex);
            PRINTF_QUEUE("Exit:%s Queue:%p Cond_Wait NotFull Failed!\n", __FUNCTION__, pq);
            return -1;
        }
    }

	next = (pq->headIndex + pq->count) % BUFQUEUE_MAX;
	pq->buffers[next] = pBufferHeader;
	pq->count++;
	pthread_mutex_unlock(&pq->hMutex);

    pthread_cond_signal(&pq->pEventNotEmpty);
    PRINTF_QUEUE("Queue:%p Sginal NotEmpty, count:%d, head:%d!\n", pq, pq->count, pq->headIndex);

    PRINTF_QUEUE("Exit %s: Queue=%p\n", __FUNCTION__, pq);
	return 0;
}

int BufQueue_TimedDeQueue(BufQueue *pq, BufType **ppBufferHeader, unsigned int ms)
{
    int bDequeued = 0;

    *ppBufferHeader = NULL;
    pthread_mutex_lock(&pq->hMutex);
    PRINTF_QUEUE("Entering:%s, Queue:%p\n", __FUNCTION__, pq);

    if (pq->count <= 0) {
        int status;
        struct timespec ts;

        clock_gettime(CLOCK_REALTIME, &ts);
        ts.tv_sec   += ms/1000;
        ts.tv_nsec  += (ms%1000)*1000000;

        PRINTF_QUEUE("Queue:%p is timed waiting for NotEmpty signal!\n",  pq);
        status = pthread_cond_timedwait(&pq->pEventNotEmpty, &pq->hMutex, &ts);
        PRINTF_QUEUE("Queue:%p timed wait for NotEmpty returned with:%d, count:%d!\n", pq, status, pq->count);
    }

    if(pq->count > 0) {
        *ppBufferHeader = pq->buffers[pq->headIndex];
        pq->headIndex++;
        if (pq->headIndex == BUFQUEUE_MAX) {
            pq->headIndex = 0;
        }
        pq->count--;

        bDequeued = 1;
    }

    PRINTF_QUEUE("Exit:%s, Queue:%p\n", __FUNCTION__, pq);
    pthread_mutex_unlock(&pq->hMutex);

    if (bDequeued) {
        pthread_cond_signal(&pq->pEventNotFull);
        PRINTF_QUEUE("Queue:%p Sginal NotFull, count:%d!\n", pq, pq->count);
    }

    return 0;
}

int BufQueue_TryDeQueue(BufQueue *pq, BufType **ppBufferHeader)
{
    int bDequeued = 0;

    PRINTF_QUEUE("Entering:%s, Queue:%p\n", __FUNCTION__, pq);

    *ppBufferHeader = NULL;
    pthread_mutex_lock(&pq->hMutex);

    if (pq->count > 0) {
        *ppBufferHeader = pq->buffers[pq->headIndex];
        pq->headIndex++;
        if (pq->headIndex == BUFQUEUE_MAX) {
            pq->headIndex = 0;
        }
	    pq->count--;

        bDequeued = 1;
    }

    pthread_mutex_unlock(&pq->hMutex);

    if (bDequeued) {
        pthread_cond_signal(&pq->pEventNotFull);
        PRINTF_QUEUE("Queue:%p Sginal NotFull, count:%d!\n", pq, pq->count);
    }

    PRINTF_QUEUE("Exit:%s, Queue:%p\n", __FUNCTION__, pq);
    return 0;
}

int BufQueue_DeQueue(BufQueue *pq,  BufType **ppBufferHeader)
{
		
    PRINTF_QUEUE("Entering: %s, Queue:%p\n", __FUNCTION__, pq);

    *ppBufferHeader = NULL;
	pthread_mutex_lock(&pq->hMutex);
    while(0 >= pq->count) {
        int status;

        PRINTF_QUEUE("Queue:%p is waiting for NotEmpty signal, count=%d!\n", pq, pq->count);
        pthread_cleanup_push(__cleanup_func, (void*)&pq->hMutex);
        status = pthread_cond_wait(&pq->pEventNotEmpty, &pq->hMutex);
        pthread_cleanup_pop(0);
        PRINTF_QUEUE("Queue:%p NotEmpty signal is returned with:%d, count=%d\n", pq, status, pq->count);

        if (status != 0) {
            pthread_mutex_unlock(&pq->hMutex);
            PRINTF_QUEUE("Exit:%s, Queue:%p Cond_Wait NotEmpty Failed!\n", __FUNCTION__, pq);
            return -1;
        }
    }

    *ppBufferHeader = pq->buffers[pq->headIndex];
    pq->headIndex++;
    if (pq->headIndex == BUFQUEUE_MAX) {
        pq->headIndex = 0;
    }
	pq->count--;

    pthread_mutex_unlock(&pq->hMutex);

    pthread_cond_signal(&pq->pEventNotFull);
    PRINTF_QUEUE("Queue:%p Sginal NotFull, count:%d, head:%d!\n", pq, pq->count, pq->headIndex);

    PRINTF_QUEUE("Exit:%s, Queue:%p\n", __FUNCTION__, pq);
	return 0;
}

void BufQueue_Flush(BufQueue *pq)
{
	pthread_mutex_lock(&pq->hMutex);

	pq->headIndex = 0;
	pq->count = 0;

	pthread_mutex_unlock(&pq->hMutex);
}


int IsBufQueue_Empty(BufQueue *pq)
{
	return (0 == pq->count);
}

int BufQueue_Count(BufQueue *pq)
{
	return pq->count;
}

///////////////////////////////////////////////////////////////////
// Picture Group Queue
///////////////////////////////////////////////////////////////////
int PicGrpQueue_Init(PicGrpQueue *pGrpQueue, int channel_count, int thresh)
{
    int rt;
    int i;

    PRINTF_QUEUE("Entering:%s\n", __FUNCTION__);

    rt  = pthread_mutex_init(&pGrpQueue->hMutex,            NULL);
    rt |= pthread_cond_init(&pGrpQueue->pEventNotEmpty,     NULL);
    rt |= pthread_cond_init(&pGrpQueue->pEventReachThres,   NULL);
    rt |= pthread_cond_init(&pGrpQueue->pEventConsumeIncr, NULL);

    for ( i = 0; i < channel_count; i++ ) {
        rt |= BufQueue_Init(&pGrpQueue->fifo[i]);
    }

    pGrpQueue->count            = 0;
    pGrpQueue->old_frameID      = -1;
    pGrpQueue->consumeID        = -1;
    pGrpQueue->channel_count    = channel_count;
    pGrpQueue->thresh           = thresh;
    for ( i = 0; i < channel_count; i++ ) {
        pGrpQueue->IsChannelValid[i] = 1;
        pGrpQueue->latest_frameID[i] = -1;
    }

    PRINTF_QUEUE("Exit:%s\n", __FUNCTION__);

    return rt;
}

void PicGrpQueue_DeInit(PicGrpQueue *pGrpQueue) 
{
    int i;
    int channel_count = pGrpQueue->channel_count;

    PRINTF_QUEUE("Entering:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);

    pthread_mutex_destroy(&pGrpQueue->hMutex);
    for ( i = 0; i < channel_count; i++ ) {
        BufQueue_DeInit(&pGrpQueue->fifo[i]);
    }
    pthread_cond_destroy(&pGrpQueue->pEventNotEmpty);
    pthread_cond_destroy(&pGrpQueue->pEventReachThres);
    pthread_cond_destroy(&pGrpQueue->pEventConsumeIncr);

    PRINTF_QUEUE("Exit:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);
}

int PicGrpQueue_SetThresh(PicGrpQueue *pGrpQueue, int thresh)
{
    PRINTF_QUEUE("Entering:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);

    pthread_mutex_lock(&pGrpQueue->hMutex);
    pGrpQueue->thresh = thresh;
    pthread_mutex_unlock(&pGrpQueue->hMutex);

    PRINTF_QUEUE("Exit:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);

    return 1;
}

int PicGrpQueue_WaitThresh(PicGrpQueue *pGrpQueue, unsigned int ms) 
{
    int bReached;

    PRINTF_QUEUE("Entering:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);
    pthread_mutex_lock(&pGrpQueue->hMutex);

    if (pGrpQueue->count < pGrpQueue->thresh) {
        int status;
        struct timespec ts;

        clock_gettime(CLOCK_REALTIME, &ts);
        ts.tv_sec   += ms/1000;
        ts.tv_nsec  += (ms%1000)*1000000;

        PRINTF_QUEUE("PicGrp:%p is timed waiting for ReachThres signal, thresh:%d, count:%d!\n", 
            pGrpQueue, pGrpQueue->thresh, pGrpQueue->count);
        status = pthread_cond_timedwait(&pGrpQueue->pEventReachThres, &pGrpQueue->hMutex, &ts);
        PRINTF_QUEUE("PicGrp:%p timed wait for ReachThres returned with:%d, thresh:%d, count:%d\n", 
            pGrpQueue, status, pGrpQueue->thresh, pGrpQueue->count);
    }

    bReached = (pGrpQueue->count >= pGrpQueue->thresh);
    
    pthread_mutex_unlock(&pGrpQueue->hMutex);
    PRINTF_QUEUE("Exit:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);

    return bReached;
}

int PicGrpQueue_EnQueue(PicGrpQueue *pGrpQueue,  
                        void *pBufferHeader, 
                        int channel_id, 
                        long int frame_id)
{
    int bNotEmpty = 0, bReachThresh = 0;
    long int min_frameID = 0x7FFFFFFF, i;
    int channel_count = pGrpQueue->channel_count;

    PRINTF_QUEUE("Entering:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);

    /* Step 1: Enqueue the element into specified channel */
    while (BufQueue_EnQueue(&pGrpQueue->fifo[channel_id], pBufferHeader) < 0) {
        /* Do Nothing */
        PRINTF_QUEUE("PicGrp:%p, Enqueue Channel:%d Failed, Retry...\n", pGrpQueue, channel_id);
    }

    PRINTF_QUEUE("PicGrp:%p, Channel:%d enqueue %d frames!\n", pGrpQueue, channel_id, frame_id);

    /* Step 2: Calculate the minimum frame ID */
    pthread_mutex_lock(&pGrpQueue->hMutex);
    pGrpQueue->latest_frameID[channel_id] = frame_id;

#if 1
    while(frame_id - pGrpQueue->consumeID > 1) {
        int status;
        PRINTF_QUEUE("PicGrp:%p, Wait for pEventConsumeIncr!\n", pGrpQueue);
        pthread_cleanup_push(__cleanup_func, (void*)&pGrpQueue->hMutex);
        status = pthread_cond_wait(&pGrpQueue->pEventConsumeIncr, &pGrpQueue->hMutex);
        pthread_cleanup_pop(0);
        PRINTF_QUEUE("PicGrp:%p, pEventConsumeIncr returned with:%d\n", pGrpQueue, status);

        if ( status != 0 ) {
            PRINTF_QUEUE("PicGrp:%p, Wait for pEventConsumeIncr Failed!\n", pGrpQueue);
            pthread_mutex_unlock(&pGrpQueue->hMutex);
            return -1;
        }
    }
#endif

    for ( i = 0; i < channel_count; i++ ) {
        if (pGrpQueue->IsChannelValid[i] &&
            min_frameID > pGrpQueue->latest_frameID[i]) {
            min_frameID = pGrpQueue->latest_frameID[i];
        }
    }

    /* Step 3: Update the information about Pic Group */
    if (min_frameID > pGrpQueue->old_frameID) {
        pGrpQueue->old_frameID = min_frameID;
        pGrpQueue->count++;
        bNotEmpty = 1;

        if (pGrpQueue->count >= pGrpQueue->thresh) {
            bReachThresh = 1;
        }
    }

    pthread_mutex_unlock(&pGrpQueue->hMutex);

    if (bNotEmpty) {
        PRINTF_QUEUE("PicGrp:%p, NotEmpty signaled!\n", pGrpQueue);
        pthread_cond_signal(&pGrpQueue->pEventNotEmpty);
    }

    if (bReachThresh) {
        PRINTF_QUEUE("PicGrp:%p, ReachThresh signaled!\n", pGrpQueue);
        pthread_cond_signal(&pGrpQueue->pEventReachThres);
    }

    PRINTF_QUEUE("Exit:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);

    return 1;
}

int PicGrpQueue_DeQueue(PicGrpQueue *pGrpQueue, void *pPicGroup[MAX_MULTISTRM_NUM])
{
    int i, PicCnt = 0;
    int channel_count = pGrpQueue->channel_count;

    PRINTF_QUEUE("Entering:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);

    pthread_mutex_lock(&pGrpQueue->hMutex);
    while( pGrpQueue->count <= 0) {
        int status;
        PRINTF_QUEUE("PicGrp:%p, Wait for NotEmpty signal!\n", pGrpQueue);
        pthread_cleanup_push(__cleanup_func, (void*)&pGrpQueue->hMutex);
        status = pthread_cond_wait(&pGrpQueue->pEventNotEmpty, &pGrpQueue->hMutex);
        pthread_cleanup_pop(0);
        PRINTF_QUEUE("PicGrp:%p, NotEmpty signal triggered with :%d\n", pGrpQueue, status);

        if (0 != status) {
            PRINTF_QUEUE("PicGrp:%p wait NotEmpty Failed!\n", pGrpQueue);
            pthread_mutex_unlock(&pGrpQueue->hMutex);
            return -1;
        }
    }
    pGrpQueue->count--;
    /* No need to signal NotFull here, because each channel have it's own signal*/

    pGrpQueue->consumeID++;

    pthread_mutex_unlock(&pGrpQueue->hMutex);

    PRINTF_QUEUE("PicGrp:%p, ConsumerIncr broadcasted!\n", pGrpQueue);
    pthread_cond_broadcast(&pGrpQueue->pEventConsumeIncr);

    for( i = 0; i < channel_count; i++ ) {
        if (pGrpQueue->IsChannelValid[i]) {
            while(BufQueue_DeQueue(&pGrpQueue->fifo[i], &pPicGroup[i]) < 0) {
                /* Do Nothing */
                PRINTF_QUEUE("PicGrp:%p, DeQueue Channel:%d Failed, Retry...\n", pGrpQueue, i);
            }
            PicCnt++;
        }
    }
    
    PRINTF_QUEUE("Exit:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);

    return PicCnt;
}

int PicGrpQueue_TryDeQueue(PicGrpQueue *pGrpQueue, void *pPicGroup[MAX_MULTISTRM_NUM])
{
    int channel_count = pGrpQueue->channel_count;
    int PicCnt = 0, i, bDeQueued = 0;

    PRINTF_QUEUE("Entering:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);
    pthread_mutex_lock(&pGrpQueue->hMutex);

    if (pGrpQueue->count > 0) {
        for( i = 0; i < channel_count; i++ ) {
            if (pGrpQueue->IsChannelValid[i]) {
                while(BufQueue_DeQueue(&pGrpQueue->fifo[i], &pPicGroup[i]) < 0) {
                    /* Do Nothing */
                    PRINTF_QUEUE("PicGrp:%p, DeQueue Channel:%d Failed, Retry...\n", pGrpQueue, i);
                }
                PicCnt++;
            }
        }
        pGrpQueue->count--;

        pGrpQueue->consumeID++;
        bDeQueued = 1;
    }

    PRINTF_QUEUE("Exit:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);
    pthread_mutex_unlock(&pGrpQueue->hMutex);

    if (bDeQueued) {
        PRINTF_QUEUE("PicGrp:%p, ConsumeIncr broadcasted!\n", pGrpQueue);
        pthread_cond_broadcast(&pGrpQueue->pEventConsumeIncr);
    }

    return PicCnt;
}

int PicGrpQueue_TimedDeQueue(PicGrpQueue *pGrpQueue, 
                             void *pPicGroup[MAX_MULTISTRM_NUM], 
                             unsigned int ms)
{
    int channel_count = pGrpQueue->channel_count;
    int PicCnt = 0, i;
    int bHaveItem = 0;

    PRINTF_QUEUE("Entering:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);

    pthread_mutex_lock(&pGrpQueue->hMutex);
    if ( pGrpQueue->count <= 0) {
        int status;
        struct timespec ts;

        clock_gettime(CLOCK_REALTIME, &ts);
        ts.tv_sec   += ms/1000;
        ts.tv_nsec  += (ms%1000)*1000000;

        PRINTF_QUEUE("PicGrp:%p, Wait for NotEmpty signal!\n", pGrpQueue);
        pthread_cleanup_push(__cleanup_func, (void*)&pGrpQueue->hMutex);
        status = pthread_cond_timedwait(&pGrpQueue->pEventNotEmpty, &pGrpQueue->hMutex, &ts);
        pthread_cleanup_pop(0);
        PRINTF_QUEUE("PicGrp:%p, NotEmpty signal triggered with :%d\n", pGrpQueue, status);
    }
    if (pGrpQueue->count > 0) {
        pGrpQueue->count--;
        bHaveItem = 1;
        pGrpQueue->consumeID++;
    }
    pthread_mutex_unlock(&pGrpQueue->hMutex);

    if (bHaveItem) {
        for( i = 0; i < channel_count; i++ ) {
            if (pGrpQueue->IsChannelValid[i]) {
                while(BufQueue_DeQueue(&pGrpQueue->fifo[i], &pPicGroup[i]) < 0) {
                    /* Do Nothing */
                    PRINTF_QUEUE("PicGrp:%p, DeQueue Channel:%d Failed, Retry...\n", pGrpQueue, i);
                }
                PicCnt++;
            }
        }

        PRINTF_QUEUE("PicGrp:%p, ConsumedIncr broadcasted!\n", pGrpQueue);
        pthread_cond_broadcast(&pGrpQueue->pEventConsumeIncr);
    }

    PRINTF_QUEUE("Exit:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);

    return PicCnt;
}

void PicGrpQueue_DisableChannel(PicGrpQueue *pGrpQueue, int channel_id) 
{
    PRINTF_QUEUE("Entering:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);
    pthread_mutex_lock(&pGrpQueue->hMutex);

    pGrpQueue->IsChannelValid[channel_id] = 0;
    pGrpQueue->latest_frameID[channel_id] = 0x7FFFFFFF;

    pthread_mutex_unlock(&pGrpQueue->hMutex);
    PRINTF_QUEUE("Exit:%s, PicGrp:%p\n", __FUNCTION__, pGrpQueue);
}

int PicGrpQueue_IsChannelValid(PicGrpQueue *pGrpQueue, int channel_id)
{
    return pGrpQueue->IsChannelValid[channel_id];
}

///////////////////////////////////////////////////////////////////
// Sorted linked list
///////////////////////////////////////////////////////////////////
#include "codecVC.h"
static void SortedList_DumpBrief(SortedList *pList, int isEnList)
{
	if(!pList->pHead)
		return;
	pthread_mutex_lock(&pList->hMutex);
	printf("=========%s Dump (%d Nodes)=========\n", (isEnList?"EnList":"DeList"), pList->count);
	pthread_mutex_unlock(&pList->hMutex);
	
}

static void SortedList_Dump(SortedList *pList, int isEnList)
{
	SortedNode *pNode;
	IppVmetaPicture *pPic;
	int i;
	if(!pList->pHead)
		return;
	pthread_mutex_lock(&pList->hMutex);
	pNode = pList->pHead;
	i=0;
    printf("=========%s Dump (%d Nodes)=========\n", isEnList?"EnList":"DeList", pList->count);

	while(pNode) {
		pPic = (IppVmetaPicture*)pNode->pBuffer;
		printf("List Node %d: buffer=0x%x, timestamp=%ld, id=%d\n", 
            i, (unsigned int)pNode->pBuffer, pNode->frameID, (int)pPic->pUsrData2);
		pNode=pNode->pNext;
		i++;
	}
	pthread_mutex_unlock(&pList->hMutex);
}

int SortedList_Init(SortedList *pList, int thresh)
{
	int rt = 0;
    int i;

    IPP_MemMalloc((void**)&pList->pSortNodesList, BUFLIST_MAX*sizeof(SortedNode), 4);
    if (NULL == pList->pSortNodesList) {
        return -1;
    }
    pList->pNotUsedNodes    = pList->pSortNodesList;
    for( i = 0; i < BUFLIST_MAX-1; i++ ) {
        pList->pSortNodesList[i].pNext = &pList->pSortNodesList[i+1];
    }
    pList->pSortNodesList[BUFLIST_MAX-1].pNext = NULL;
    pList->pHead = NULL;
    
    rt  = pthread_mutex_init(&pList->hMutex,            NULL);
    rt |= pthread_cond_init(&pList->pEventNotEmpty,     NULL);
    rt |= pthread_cond_init(&pList->pEventNotFull,      NULL);
    rt |= pthread_cond_init(&pList->pEventReachThres,   NULL);

	pList->count    = 0;
    pList->thresh   = thresh;

    PRINTF_QUEUE("SortQueue is create with code:%d!\n", rt);

	return rt;
}

void SortedList_DeInit(SortedList *pList)
{
    PRINTF_QUEUE("Entering:%s, SortList:%p\n", __FUNCTION__, pList);

    IPP_MemFree((void**)&pList->pSortNodesList); 
	pList->pHead            = NULL;
    pList->pNotUsedNodes    = NULL;

    pthread_mutex_destroy(&pList->hMutex);
    pthread_cond_destroy(&pList->pEventNotEmpty);
    pthread_cond_destroy(&pList->pEventNotFull);
    pthread_cond_destroy(&pList->pEventReachThres);

    PRINTF_QUEUE("Exit:%s, SortList:%p\n", __FUNCTION__, pList);
}

int SortedList_EnList(SortedList *pList,  BufType *pBufferHeader, long int frameID)
{
    int bReachThresh = 0;
	SortedNode *pNode, *pCurr, *pNext;

    PRINTF_QUEUE("Entering:%s, SortList:%p\n", __FUNCTION__, pList);

    pthread_mutex_lock(&pList->hMutex);

    while (BUFLIST_MAX < pList->count) {
        int status;

        PRINTF_QUEUE("SortList:%p is wait for NotFull signal, count:%d!\n", pList, pList->count);
        pthread_cleanup_push(__cleanup_func, (void*)&pList->hMutex);
        status = pthread_cond_wait(&pList->pEventNotFull, &pList->hMutex);
        pthread_cleanup_pop(0);
        PRINTF_QUEUE("SortList:%p wait NotFull is returned with:%d, count:%d!\n", pList, status, pList->count);
        if (0 != status) {
            pthread_mutex_unlock(&pList->hMutex);
            PRINTF_QUEUE("Exit:%s, SortList:%p, Cond_Wait Failed!\n", __FUNCTION__, pList);
            return -1;
        }
    }

    pNode = pList->pNotUsedNodes;
    pList->pNotUsedNodes = pList->pNotUsedNodes->pNext;

    pNode->frameID = frameID;
    pNode->pBuffer = pBufferHeader;
    pNode->pNext   = NULL;

	if(0 == pList->count) {
		pList->pHead = pNode;
	} else {
		pCurr = pList->pHead;
		pNext = pCurr->pNext;
		if(frameID < pList->pHead->frameID) {
			/* new head */
			pNode->pNext = pList->pHead;
			pList->pHead = pNode;
		} else {
			while(pNext && frameID > pNext->frameID) {
				pCurr = pNext;
				pNext = pNext->pNext;
			}
			pCurr->pNext = pNode;
			pNode->pNext = pNext;
        }
    }
	pList->count ++;

    PRINTF_QUEUE("SortList:%p: There are %d pic buffer in list!\n", pList, pList->count);

    if (pList->count >= pList->thresh) {
        bReachThresh = 1;
    }
    pthread_mutex_unlock(&pList->hMutex);

    pthread_cond_signal(&pList->pEventNotEmpty);
    PRINTF_QUEUE("SortList:%p Signal NotEmpty, count:%d\n", pList, pList->count);

    if (bReachThresh) {
        pthread_cond_signal(&pList->pEventReachThres);
        PRINTF_QUEUE("SortList:%p Sginal ReachThres\n", pList);
    }

    PRINTF_QUEUE("Exit:%s, SortList:%p\n", __FUNCTION__, pList);

    return 0;
}

int SortedList_TryDeList(SortedList *pList, BufType **ppBufferHeader)
{
    int bDeList = 0;
    SortedNode *pNode = NULL;

    PRINTF_QUEUE("Entering:%s, SortList:%p\n", __FUNCTION__, pList);
    
    *ppBufferHeader = NULL;
    pthread_mutex_lock(&pList->hMutex);

    if ( 0 < pList->count ) {
        pNode = pList->pHead;
        *ppBufferHeader = pNode->pBuffer;
        pList->pHead = pNode->pNext;
        pList->count --;

        pNode->pNext = pList->pNotUsedNodes->pNext;
        pList->pNotUsedNodes->pNext = pNode;

        bDeList = 1;
    } 

    pthread_mutex_unlock(&pList->hMutex);

    if (bDeList) {
        pthread_cond_signal(&pList->pEventNotFull);
        PRINTF_QUEUE("SortList:%p Signal NotFull, count:%d\n", pList, pList->count);
    }

    PRINTF_QUEUE("Exit:%s, SortList:%p\n", __FUNCTION__, pList);

    return 0;
}

int SortedList_DeList(SortedList *pList,  BufType **ppBufferHeader)
{
    SortedNode *pNode = NULL;

    PRINTF_QUEUE("Entering:%s, SortList:%p\n", __FUNCTION__, pList);

    *ppBufferHeader = NULL;
    pthread_mutex_lock(&pList->hMutex);

    while( 0 >= pList->count ) {
        int status;

        PRINTF_QUEUE("SortList:%p is waiting for NotEmpty signal, count:%d!\n", pList, pList->count);
        pthread_cleanup_push(__cleanup_func, (void*)&pList->hMutex);
        status = pthread_cond_wait(&pList->pEventNotEmpty, &pList->hMutex);
        pthread_cleanup_pop(0);
        PRINTF_QUEUE("SortList:%p wait for NotEmpty is returned with:%d, count:%d\n", pList, status, pList->count);

        if (0 != status) {
            pthread_mutex_unlock(&pList->hMutex);
            PRINTF_QUEUE("Exit:%s, SortList:%p, Cond_Wait Failed\n", __FUNCTION__, pList);
            return -1;
        }
    }

    pNode = pList->pHead;
    *ppBufferHeader = pNode->pBuffer;
    pList->pHead = pNode->pNext;
    pList->count --;
    pNode->pNext = pList->pNotUsedNodes->pNext;
    pList->pNotUsedNodes->pNext = pNode;

    pthread_mutex_unlock(&pList->hMutex);

    pthread_cond_signal(&pList->pEventNotFull);
    PRINTF_QUEUE("SortList:%p Sginal NotFull, count:%d\n", pList, pList->count);

    PRINTF_QUEUE("Exit:%s, SortList:%p\n", __FUNCTION__, pList);

    return 0;
}

void SortedList_SetThresh(SortedList *pList, int thresh)
{
    pthread_mutex_lock(&pList->hMutex);

    pList->thresh = thresh;

    pthread_mutex_unlock(&pList->hMutex);
}

int IsSortedList_Full(SortedList *pList)
{
	return (BUFLIST_MAX == pList->count);
}

int IsSortedList_Empty(SortedList *pList)
{
	return (0 == pList->count);
}

int SortedList_TimedWaitBuffer(SortedList *pList, unsigned int ms)
{
    int bReached;

    PRINTF_QUEUE("Entering:%s, SortList:%p\n", __FUNCTION__, pList);
    pthread_mutex_lock(&pList->hMutex);

    if (pList->count < pList->thresh) {
        int status;
        struct timespec ts;

        clock_gettime(CLOCK_REALTIME, &ts);
        ts.tv_sec   += ms/1000;
        ts.tv_nsec  += (ms%1000)*1000000;

        PRINTF_QUEUE("SortList:%p is timed waiting for ReachThres signal, thresh:%d, count:%d!\n", 
            pList, pList->thresh, pList->count);
        status = pthread_cond_timedwait(&pList->pEventReachThres, &pList->hMutex, &ts);
        PRINTF_QUEUE("SortList:%p timed wait for ReachThres returned with:%d, thresh:%d, count:%d\n", 
            pList, status, pList->thresh, pList->count);
    }

    bReached = (pList->count >= pList->thresh);
    
    pthread_mutex_unlock(&pList->hMutex);
    PRINTF_QUEUE("Exit:%s, SortList:%p\n", __FUNCTION__, pList);

    return bReached;
}

int SortedList_Count(SortedList *pList)
{
	return pList->count;
}

/* EOF */
