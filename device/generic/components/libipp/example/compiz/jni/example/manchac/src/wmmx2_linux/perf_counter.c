/***************************************************************************************** 
Copyright (c) 2009, Marvell International Ltd. 
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Marvell nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MARVELL ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MARVELL BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "manchac.h"

extern MANPerfCT       	gPerfCT;
static int bEnabled 	= 0;

#ifdef  PLATFORM_MH
#include "gcc_counter_manzano_v5.c"

void open_pmu()
{
#ifdef TIMING_PMU	
	if(ixs_open_pmu()) {
		printf("Canot open PMU driver.\n");
		exit(1);
	}
	bEnabled = 1;
	if(gPerfCT.pCntConfig->bCntEnable) {
		ixs_set_pmu(gPerfCT.pCntConfig->nCntMode[0],
			gPerfCT.pCntConfig->nCntMode[1]);
	} else {
		/* use default */
		ixs_set_pmu(IC_EFFICIENCY, DC_EFFICIENCY);
	}
#endif
}

long long start_pmu()
{
#ifdef TIMING_PMU
	if(bEnabled) {
		ixs_begin_pmu(0);
	}
#endif
	return 0;
}

long long stop_pmu()
{
#ifdef TIMING_PMU
	unsigned long lcnt[5];
	if(!bEnabled) {
		return 0;
	}

	ixs_stop_pmu(lcnt);
	gPerfCT.cnt[0] += (unsigned long long)lcnt[0];
	gPerfCT.cnt[1] += (unsigned long long)lcnt[1];
	gPerfCT.cnt[2] += (unsigned long long)lcnt[2];
	gPerfCT.cnt[3] += (unsigned long long)lcnt[3];
	gPerfCT.cnt[4] += (unsigned long long)lcnt[4];
	return (long long)lcnt[0]/CORE_FREQ;
#else
	return 0L;
#endif
}

void close_pmu()
{
#ifdef TIMING_PMU	
	if(bEnabled) {
		ixs_close_pmu();
		bEnabled = 0;
	}
#endif
}

#elif (defined PLATFORM_TTC)
#include "gcc_counter_xsc3_v5.c"
#define CYCLE_COUNTER	3
__int64 lcnt[4];
void open_pmu()
{
#ifdef TIMING_PMU
	reset_all_counters();
	if(gPerfCT.pCntConfig->bCntEnable) {
		set_counter_types(gPerfCT.pCntConfig->nCntMode[0],
			gPerfCT.pCntConfig->nCntMode[1],
			gPerfCT.pCntConfig->nCntMode[2],
			gPerfCT.pCntConfig->nCntMode[3]);
	} else {
		/* use default */
		set_counter_types(DISABLE_COUNTER, DISABLE_COUNTER, DISABLE_COUNTER, C3_CYCLE_COUNT); 
	}
	bEnabled = 1;
#endif
}

long long start_pmu()
{
#ifdef TIMING_PMU
	if(bEnabled) {
		lcnt[0] = get_counter0();
		lcnt[1] = get_counter1();
		lcnt[2] = get_counter2();
		lcnt[3] = get_counter3();
		return (long long)lcnt[CYCLE_COUNTER]/CORE_FREQ;
	} else {
		return 0L;
	}
#else
	return 0L;
#endif
}

long long stop_pmu()
{
#ifdef TIMING_PMU
	__int64 llcnt[4];
	if(bEnabled) {
		llcnt[0] = get_counter0();
		llcnt[1] = get_counter1();
		llcnt[2] = get_counter2();
		llcnt[3] = get_counter3();

		gPerfCT.cnt[0] += llcnt[0] - lcnt[0];
		gPerfCT.cnt[1] += llcnt[1] - lcnt[1];
		gPerfCT.cnt[2] += llcnt[2] - lcnt[2];
		gPerfCT.cnt[3] += llcnt[3] - lcnt[3];

		return (long long)llcnt[CYCLE_COUNTER]/CORE_FREQ;
	} else {
		return 0L;
	}
#else
	return 0L;
#endif
}

void close_pmu()
{
#ifdef TIMING_PMU
        bEnabled = 0;
#endif
}

#elif (defined PLATFORM_PJ5)
#include "gcc_counter_xsc3_v7.c" 
#define CYCLE_COUNTER   0
void open_pmu()
{
#ifdef TIMING_PMU	
	pcd_open();
	if(gPerfCT.pCntConfig->bCntEnable) {
		set_counter_types(gPerfCT.pCntConfig->nCntMode[0],
			gPerfCT.pCntConfig->nCntMode[1],
			gPerfCT.pCntConfig->nCntMode[2],
			gPerfCT.pCntConfig->nCntMode[3]);
	} else {
		/* use default */
		set_counter_types(CYCLE_COUNT, SOFTWARE_INCR, SOFTWARE_INCR, SOFTWARE_INCR); 
	}
	bEnabled = 1;
#endif
}

long long start_pmu()
{
#ifdef TIMING_PMU	
	if(bEnabled) {
		reset_all_counters();
		start_all_counters();
	}
#endif
	return 0L;
}

long long stop_pmu()
{
#ifdef TIMING_PMU	
        unsigned int lcnt[4];

        if(bEnabled) {
		stop_all_counters();
		get_all_counters(&lcnt[0], &lcnt[1], &lcnt[2], &lcnt[3]);

		gPerfCT.cnt[0] += lcnt[0];
		gPerfCT.cnt[1] += lcnt[1];
		gPerfCT.cnt[2] += lcnt[2];
		gPerfCT.cnt[3] += lcnt[3];
		return (long long)lcnt[0]/CORE_FREQ;
	} else {
		return 0L;
	}
#else
	return 0L;
#endif
}

void close_pmu()
{
#ifdef TIMING_PMU	
	pcd_close();
	bEnabled = 0;
#endif
}


#else
        #error "Please specify a platform name (PLATFORM_TTC | PLATFORM_MH | PLATFORM_PJ5)"
#endif


/* EOF */

