/***************************************************************************************** 
Copyright (c) 2009, Marvell International Ltd. 
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Marvell nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MARVELL ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MARVELL BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************//*
 * Flareon PJ4/XScale 3
 * ARM v6/v7 Processor Core
 * with MMU and L1/L2 C
 * Datasheet
 * Doc. No. 
 */

/*
 * change word order if big-endian
 */

typedef unsigned int __juint;

/*********** performance counter control(event selection) **********/
/* Custom counters (Counter0, Counter1, Counter2, Counter3) */
/* Programming Details: */
#define SOFTWARE_INCR			0x00	/* software increment */
#define IFU_IFETCH_REFILL		0x01	/* instruction fetch that cause a refill at the lowest level of instruction or unified cache */
#define IF_TLB_REFILL			0x02	/* instruction fetch that cause a TLB refill at the lowest level of TLB */
#define DATA_RW_CACHE_REFILL		0x03	/* data read or write operation that causes a refill of at the lowest level of data or unified cache */
#define DATA_RW_CACHE_ACCESS		0x04	/* data read or write operation that causes a cache access at the lowest level of data or unified cache */
#define DATA_RW_TLB_REFILL		0x05	/* data read or write operation that causes a TLB refill at the lowest level of TLB */
#define DATA_READ_INST_EXEC		0x06	/* data read architecturally executed */
#define DATA_WRIT_INST_EXEC		0x07	/* data write architecturally executed */
#define INSN_EXECUTED			0x08	/* instruction architecturally executed */
#define EXCEPTION_TAKEN			0x09	/* exception taken */
#define EXCEPTION_RETURN		0x0a	/* exception return architecturally executed */
#define INSN_WR_CONTEXTIDR		0x0b	/* instruction that writes to the Context ID Register architecturally executed */
#define SW_CHANGE_PC			0x0c	/* software change of PC, except by an exception, architecturally executed */
#define BR_EXECUTED			0x0d	/* immediate branch architecturally executed, taken or not taken */
#define PROCEDURE_RETURN		0x0e	/* procedure return architecturally executed */
#define UNALIGNED_ACCESS		0x0f	/* unaligned access architecturally executed */
#define BR_INST_MISS_PRED		0x10	/* branch mispredicted or not predicted */
#define CYCLE_COUNT			0x11	/* cycle count */
#define BR_PRED_TAKEN			0x12	/* branches or other change in the program flow that could have been predicted by the branch prediction resources of the processor */
#define DCACHE_READ_HIT 		0x40	/* counts the number of Data Cache read hits */
#define DCACHE_READ_MISS		0x41	/* connts the number of Data Cache read misses */
#define DCACHE_WRITE_HIT 		0x42	/* counts the number of Data Cache write hits */
#define DCACHE_WRITE_MISS		0x43	/* counts the number of Data Cache write misses */
#define MMU_BUS_REQUEST			0x44	/* counts the number of cycles of request to the MMU Bus */
#define ICACHE_BUS_REQUEST 		0x45	/* counts the number of cycles the Instruction Cache requests the bus until the data return */
#define WB_WRITE_LATENCY 		0x46	/* counts the number of cycles the Write Buffer requests the bus */
#define HOLD_LDM_STM 			0x47	/* counts the number of cycles the pipeline is held because of a load/store multiple instruction */
#define NO_DUAL_CFLAG 			0x48	/* counts the number of cycles the processor cannot dual issue because of a Carry flag dependency */
#define NO_DUAL_REGISTER_PLUS 		0x49	/* counts the number of cycles the processor cannot dual issue because the register file does not have enough read ports and at least one other reason */
#define LDST_ROB0_ON_HOLD 		0x4a	/* counts the number of cycles a load or store instruction waits to retire from ROB0 */
#define LDST_ROB1_ON_HOLD 		0x4b	/* counts the number of cycles a load or store instruction waits to retire from ROB0=1 */
#define DATA_WRITE_ACCESS_COUNT 	0x4c 	/* counts the number of any Data write access */
#define DATA_READ_ACCESS_COUNT 		0x4d 	/* counts the number of any Data read access */
#define A2_STALL 			0x4e 	/* counts the number of cycles ALU A2 is stalled */
#define L2C_WRITE_HIT 			0x4f 	/* counts the number of write accesses to addresses already in the L2C */
#define L2C_WRITE_MISS 			0x50	/* counts the number of write accesses to addresses not in the L2C */
#define L2C_READ_COUNT 			0x51	/* counts the number of L2C cache-to-bus external read request */
#define ICACHE_READ_MISS 		0x60 	/* counts the number of Instruction Cache read misses */
#define ITLB_MISS 			0x61 	/* counts the number of instruction TLB miss */
#define SINGLE_ISSUE 			0x62 	/* counts the number of cycles the processor single issues */
#define BR_RETIRED 			0x63 	/* counts the number of times one branch retires */
#define ROB_FULL 			0x64 	/* counts the number of cycles the Re-order Buffer (ROB) is full */
#define MMU_READ_BEAT 			0x65 	/* counts the number of times the bus returns RDY to the MMU */
#define WB_WRITE_BEAT 			0x66 	/* counts the number times the bus returns ready to the Write Buffer */
#define DUAL_ISSUE 			0x67 	/* counts the number of cycles the processor dual issues */
#define NO_DUAL_RAW 			0x68 	/* counts the number of cycles the processor cannot dual issue because of a Read after Write hazard */
#define HOLD_IS 			0x69 	/* counts the number of cycles the issue is held */
#define L2C_LATENCY 			0x6a 	/* counts the latency for the most recent L2C read from the external bus Counts cycles */
#define DCACHE_ACCESS 			0x70 	/* counts the number of times the Data cache is accessed */
#define DTLB_MISS 			0x71 	/* counts the number of data TLB misses */
#define BR_PRED_MISS 			0x72 	/* counts the number of mispredicted branches */
#define A1_STALL 			0x74 	/* counts the number of cycles ALU A1 is stalled */
#define DCACHE_READ_LATENCY 		0x75 	/* counts the number of cycles the Data cache requests the bus for a read */
#define DCACHE_WRITE_LATENCY 		0x76 	/* counts the number of cycles the Data cache requests the bus for a write */
#define NO_DUAL_REGISTER_FILE 		0x77 	/* counts the number of cycles the processor cannot dual issue because the register file doesn't have enough read ports */
#define BIU_SIMULTANEOUS_ACCESS 	0x78 	/* BIU Simultaneous Access */
#define L2C_READ_HIT 			0x79 	/* counts the number of L2C cache-to-bus external read requests */
#define L2C_READ_MISS 			0x7a 	/* counts the number of L2C read accesses that resulted in an external read request */
#define L2C_WB_FULL 			0x7b 	/* Not Applicable (L2C no longer has a Write Buffer(WB)) */
#define TLB_MISS 			0x80 	/* counts the number of instruction and data TLB misses */
#define BR_TAKEN 			0x81 	/* counts the number of taken branches */
#define WB_FULL 			0x82 	/* counts the number of cycles WB is full */
#define DCACHE_READ_BEAT 		0x83 	/* counts the number of times the bus returns Data to the Data cache during read request */
#define DCACHE_WRITE_BEAT 		0x84 	/* counts the number of times the bus returns ready to the Data cache during write request */
#define NO_DUAL_HW 			0x85 	/* counts the number of cycles the processor cannot dual issue because of hardware conflict */
#define NO_DUAL_MULTIPLE 		0x86 	/* counts the number of cycles the processor cannot dual issue because of multiple reasons */
#define BIU_ANY_ACCESS 			0x87 	/* counts the number of cycles the BIU is accessed by any unit */
#define MAIN_TLB_REFILL_BY_ICACHE 	0x88 	/* counts the number of instruction fetch operations that causes a Main TLB walk */
#define MAIN_TLB_REFILL_BY_DCACHE 	0x89 	/* counts the number of Data read or write operations that causes a Main TLB walk */
#define ICACHE_READ_BEAT 		0x8a 	/* counts the number of times the bus returns RDY to the instruction cache */
#define WMMX2_STORE_FIFO_FULL 		0xc0 	/* counts the number of cycles when the WMMX2 store FIFO is full */
#define WMMX2_FINISH_FIFO_FULL 		0xc1 	/* counts the number of cycles when the WMMX2 finish FIFO is full */
#define WMMX2_INST_FIFO_FULL 		0xc2 	/* counts the number of cycles when the WMMX2 instruction FIFO is full */
#define WMMX2_INST_RETIRED 		0xc3 	/* counts the number of retired WMMX2 instructions */
#define WMMX2_BUSY 			0xc4 	/* counts the number of cycles when the WMMX2 is busy */
#define WMMX2_HOLD_MI 			0xc5 	/* counts the number of cycles when WMMX2 holds the issue stage */
#define WMMX2_HOLD_MW 			0xc6 	/* counts the number of cycles when WMMX2 holds the write back stage */


int pcd_open();
int pcd_close(); 

void set_counter_types(__juint counter0, __juint counter1, __juint counter2, __juint counter3);
void stop_all_counters();
void start_all_counters();
void get_all_counters(__juint *c0, __juint *c1, __juint *c2, __juint *c3);
void reset_all_counters();
void show_counters();

void set_counter0(__juint counter0);
void stop_counter0();
__juint get_counter0();
void reset_counter0();
void start_counter0();

void set_counter1(__juint counter1);
void stop_counter1();
__juint get_counter1();
void reset_counter1();
void start_counter1();

void set_counter2(__juint counter2);
void stop_counter2();
__juint get_counter2();
void reset_counter2();
void start_counter2();

void set_counter3(__juint counter3);
void stop_counter3();
__juint get_counter3();
void reset_counter3();
void start_counter3();
