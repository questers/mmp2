#ifndef __COMPIZ_JNI_ACTIVITY_H__
#define __COMPIZ_JNI_ACTIVITY_H__

#ifdef __cplusplus
extern "C" {
#endif

/* COMPIZActivity Interfaces */
void KillCompiz(const char *str);
void NormalExitCompiz(const char *str);

#ifdef __cplusplus
}
#endif

#endif