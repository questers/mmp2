//void getErr();

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <math.h>

char tochar(int);
char * tostr(int);
int power(int,int);
int toint(char*);
char * buildgamestring(int*);

#ifdef __LINUX__
unsigned long long GetTickCount();
#endif
