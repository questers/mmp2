/******************************************************************************
// (C) Copyright 2009 Marvell International Ltd.
// All Rights Reserved
******************************************************************************/

#include "demo.h"
#include "vdec_wrapper.h"
#include "misc.h"
#include <stdio.h>
#ifndef LOG_TAG
#define LOG_TAG "COMPIZ"
#endif
#include "compiz_jni_lib.h"
#include <nativehelper/jni.h>

extern int bStop;

//#define JNI_LIB_DBG
#ifdef JNI_LIB_DBG
#define JNI_LIB_PRINTF(...) IPP_Printf(__VA_ARGS__)
#else
#define JNI_LIB_PRINTF(...) 
#endif

/* EXTERN from manchac */
extern int screenWidth;
extern int screenHeight;

/* EXTERN from vdec_wrapper */
extern void *pEventSuspend;

/* EXTERN from demo */

 void Java_com_android_compiz_COMPIZLib_keyPressed(JNIEnv *env, jobject obj, int keyCode)
{
    JNI_LIB_PRINTF("Entering: COMPIZLib lib KeyPressed\n");
    /* The total_frame here is invalid */
    KeyPressHandler(0, keyCode);
    JNI_LIB_PRINTF("Exit: COMPIZLib lib KeyPressed\n");
}
 void Java_com_android_compiz_COMPIZLib_pause(JNIEnv *env, jobject obj)
{
    JNI_LIB_PRINTF("Entering: COMPIZLib lib Pause\n");
    IPP_EventReset(pEventSuspend);
    GPUDeInitGLTexture();
    JNI_LIB_PRINTF("Exit: COMPIZLib lib Pause\n");
}

void Java_com_android_compiz_COMPIZLib_resume(JNIEnv *env, jobject obj)
{
    JNI_LIB_PRINTF("Entering: COMPIZLib lib Resume\n");
    IPP_EventSet(pEventSuspend);
    GPUInitGLTexture();
    JNI_LIB_PRINTF("Exit: COMPIZLib lib Resume\n");
}

void Java_com_android_compiz_COMPIZLib_init(JNIEnv *env, jobject obj)
{
    JNI_LIB_PRINTF("Entering: COMPIZLib lib init\n");

    if (0 != compiz_init(NULL)) {
        IPP_Printf("Failed to init\n");
        bStop = 1;
        KillCompiz((const char*)"Destroy");

        return ;
    }

    if (CreateGPURes() < 0) {
        IPP_Printf("Failed to create GPU Res\n");
        bStop = 1;
        KillCompiz((const char*)"Destroy");

        return ;
    }

    JNI_LIB_PRINTF("Exit: COMPIZLib lib init\n");
}

void Java_com_android_compiz_COMPIZLib_deinit(JNIEnv *env, jobject obj)
{
    JNI_LIB_PRINTF("Entering: COMPIZLib lib deinit\n");

    compiz_deinit();
    IPP_Printf("COMPIZ Res released!\n");

    DestroyGPURes();
    IPP_Printf("GPU Res released!\n");

    JNI_LIB_PRINTF("Exit: COMPIZLib lib deinit\n");
}

void Java_com_android_compiz_COMPIZLib_change(JNIEnv *env, jobject obj, int width, int height)
{
    JNI_LIB_PRINTF("Entering: COMPIZLib lib Change\n");
    screenWidth = width;
	screenHeight= height;

    WindowReshape(0, 0, width, height);
    JNI_LIB_PRINTF("Exit: COMPIZLib lib Change\n");
}

void Java_com_android_compiz_COMPIZLib_step(JNIEnv *env, jobject obj)
{
    JNI_LIB_PRINTF("Entering: COMPIZLib lib step\n");
    DoGPURender();
    JNI_LIB_PRINTF("Exit: COMPIZLib lib step\n");
}

/* EOF */
