/***************************************************************************************** 
Copyright (c) 2009, Marvell International Ltd. 
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Marvell nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MARVELL ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MARVELL BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************/

#include <stdio.h>
#include <unistd.h>

#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "gcc_counter_xsc3_v7.h"

/*
 * Ioctl definitions
 */
/* Use 'P' as magic number for Performance Counter */ 
#define PCD_IOC_MAGIC  'P'

/* Please use a different 8-bit number in your code */
#define PCD_IOCS_TYPE 	_IOW(PCD_IOC_MAGIC, 0, int)		// Set Type 
#define PCD_IOCS_CNTR	_IOW(PCD_IOC_MAGIC, 1, int)		// Set Counter
#define PCD_IOCG_CNTR	_IOR(PCD_IOC_MAGIC, 2, int)		// Get Counter
#define PCD_IOCS_CNTR_ENABLE _IOW(PCD_IOC_MAGIC, 3, int)	// Enable Counter
#define PCD_IOCS_CNTR_DISABLE _IOW(PCD_IOC_MAGIC, 4, int)	// Disable Counter
#define PCD_IOCG_SHOW_CNTR  _IOR(PCD_IOC_MAGIC, 5, int)		// Show Counter


static int devfd=0; 

void reset_all_counters() {
	reset_counter0(0);
	reset_counter1(0);
	reset_counter2(0);
	reset_counter3(0);
}

void set_counter_types(__juint counter0, __juint counter1, __juint counter2, __juint counter3) {
	set_counter0(counter0);
	set_counter1(counter1);
	set_counter2(counter2);
	set_counter3(counter3);
}

void start_all_counters() {
	start_counter0();
	start_counter1();
	start_counter2();
	start_counter3();
}

void stop_all_counters() {
	stop_counter0();
	stop_counter1();
	stop_counter2();
	stop_counter3();
}

/* OPEN PERFORMANCE COUNTER DRIVER DEVICE*/ 
int __attribute__((constructor)) pcd_open() {
	if (!devfd) {
		devfd = open("/dev/pcd", O_RDWR);
		if (devfd == -1) {
			printf("Can't open /dev/pcd\n");
			return -1;
		}
	}
	return 0;
}
int __attribute__((destructor)) pcd_close() {
	if (devfd) 
		close(devfd); 
	devfd = 0; 
	return 0;
}


#define EXPAND_COUNTER_FUNTCIONS(cnum)\
/* set_counter */\
void set_counter##cnum(__juint counter##cnum) {\
	__juint iocparam[2];\
	iocparam[0]=cnum;\
	iocparam[1]=counter##cnum;\
	ioctl(devfd, PCD_IOCS_TYPE, &iocparam);\
}\
/* stop_counter */\
void stop_counter##cnum() {\
	int counter=cnum;\
	ioctl(devfd, PCD_IOCS_CNTR_DISABLE, &counter);\
}\
/* get_counter */\
__juint get_counter##cnum() {\
	__juint counter = cnum;\
	ioctl(devfd, PCD_IOCG_CNTR, &counter);\
	return counter;\
}\
/* reset_counter */\
void reset_counter##cnum(__juint num) {\
	__juint iocparam[2];\
	iocparam[0]=cnum;\
	iocparam[1]=num;\
	ioctl(devfd, PCD_IOCS_CNTR, &iocparam);\
}\
/* start_counter */\
void start_counter##cnum() {\
	int counter=cnum;\
	ioctl(devfd, PCD_IOCS_CNTR_ENABLE, &counter);\
}

EXPAND_COUNTER_FUNTCIONS(0)
EXPAND_COUNTER_FUNTCIONS(1)
EXPAND_COUNTER_FUNTCIONS(2)
EXPAND_COUNTER_FUNTCIONS(3)

void show_counters() {
    ioctl(devfd, PCD_IOCG_SHOW_CNTR, 0);
}

void get_all_counters(__juint *c0, __juint *c1, __juint *c2, __juint *c3) {
	if(c0)
		*c0 = get_counter0();
	if(c1)
		*c1 = get_counter1();
	if(c2)
		*c2 = get_counter2();
	if(c3)
		*c3 = get_counter3();
}

const PMUModeDesp modeDesp[] = {
	{0, SOFTWARE_INCR, 		"SOFTWARE_INCR"},		                 
	{0, IFU_IFETCH_REFILL,		"IFU_IFETCH_REFILL"},	                 
	{0, IF_TLB_REFILL,		"IF_TLB_REFILL"},		                 
	{0, DATA_RW_CACHE_REFILL,	"DATA_RW_CACHE_REFILL"},	                 
	{0, DATA_RW_CACHE_ACCESS,	"DATA_RW_CACHE_ACCESS"},	                 
	{0, DATA_RW_TLB_REFILL,		"DATA_RW_TLB_REFILL"},	                 
	{0, DATA_READ_INST_EXEC,        "DATA_READ_INST_EXEC"},	                 
	{0, DATA_WRIT_INST_EXEC,        "DATA_WRIT_INST_EXEC"},	                 
	{0, INSN_EXECUTED,		"INSN_EXECUTED"},		                 
	{0, EXCEPTION_TAKEN,		"EXCEPTION_TAKEN"},		         
	{0, EXCEPTION_RETURN,	        "EXCEPTION_RETURN"},	                 
	{0, INSN_WR_CONTEXTIDR,	        "INSN_WR_CONTEXTIDR"},	                 
	{0, SW_CHANGE_PC,		"SW_CHANGE_PC"},		                 
	{0, BR_EXECUTED,	        "BR_EXECUTED"},		                 
	{0, PROCEDURE_RETURN,	        "PROCEDURE_RETURN"},	                 
	{0, UNALIGNED_ACCESS,	        "UNALIGNED_ACCESS"},	                 
	{0, BR_INST_MISS_PRED,	        "BR_INST_MISS_PRED"},	                 
	{0, CYCLE_COUNT,	        "CYCLE_COUNT"},		                 
	{0, BR_PRED_TAKEN,		"BR_PRED_TAKEN"},		                 
	{0, DCACHE_READ_HIT, 	        "DCACHE_READ_HIT"}, 	                 
	{0, DCACHE_READ_MISS,	        "DCACHE_READ_MISS"},	                 
	{0, DCACHE_WRITE_HIT, 	        "DCACHE_WRITE_HIT"}, 	                 
	{0, DCACHE_WRITE_MISS,	        "DCACHE_WRITE_MISS"},	                 
	{0, MMU_BUS_REQUEST,		"MMU_BUS_REQUEST"},		         
	{0, ICACHE_BUS_REQUEST,	        "ICACHE_BUS_REQUEST"}, 	                 
	{0, WB_WRITE_LATENCY, 	        "WB_WRITE_LATENCY"}, 	                 
	{0, HOLD_LDM_STM, 		"HOLD_LDM_STM"}, 		                 
	{0, NO_DUAL_CFLAG, 		"NO_DUAL_CFLAG"}, 		                 
	{0, NO_DUAL_REGISTER_PLUS,      "NO_DUAL_REGISTER_PLUS"},
	{0, LDST_ROB0_ON_HOLD, 	        "LDST_ROB0_ON_HOLD"}, 	                 
	{0, LDST_ROB1_ON_HOLD, 	        "LDST_ROB1_ON_HOLD"}, 	                 
	{0, DATA_WRITE_ACCESS_COUNT,    "DATA_WRITE_ACCESS_COUNT"},                 
	{0, DATA_READ_ACCESS_COUNT, 	"DATA_READ_ACCESS_COUNT"}, 	         
	{0, A2_STALL, 		        "A2_STALL"}, 		                 
	{0, L2C_WRITE_HIT, 		"L2C_WRITE_HIT"}, 		                 
	{0, L2C_WRITE_MISS, 		"L2C_WRITE_MISS"}, 		         
	{0, L2C_READ_COUNT, 		"L2C_READ_COUNT"}, 		         
	{0, ICACHE_READ_MISS, 	        "ICACHE_READ_MISS"}, 	                 
	{0, ITLB_MISS, 		        "ITLB_MISS"}, 		                 
	{0, SINGLE_ISSUE, 		"SINGLE_ISSUE"}, 		                 
	{0, BR_RETIRED,		        "BR_RETIRED"}, 		                 
	{0, ROB_FULL, 		        "ROB_FULL"}, 		                 
	{0, MMU_READ_BEAT, 		"MMU_READ_BEAT"}, 		                 
	{0, WB_WRITE_BEAT, 		"WB_WRITE_BEAT"}, 		                 
	{0, DUAL_ISSUE,		        "DUAL_ISSUE"}, 		                 
	{0, NO_DUAL_RAW, 		"NO_DUAL_RAW"}, 		                 
	{0, HOLD_IS, 		        "HOLD_IS"}, 		                 
	{0, L2C_LATENCY, 		"L2C_LATENCY"}, 		                 
	{0, DCACHE_ACCESS, 		"DCACHE_ACCESS"}, 		                 
	{0, DTLB_MISS, 		        "DTLB_MISS"}, 		                 
	{0, BR_PRED_MISS, 		"BR_PRED_MISS"}, 		                 
	{0, A1_STALL, 		        "A1_STALL"}, 		                 
	{0, DCACHE_READ_LATENCY, 	"DCACHE_READ_LATENCY"}, 	                 
	{0, DCACHE_WRITE_LATENCY, 	"DCACHE_WRITE_LATENCY"}, 	                 
	{0, NO_DUAL_REGISTER_FILE, 	"NO_DUAL_REGISTER_FILE"}, 	                 
	{0, BIU_SIMULTANEOUS_ACCESS,    "BIU_SIMULTANEOUS_ACCESS"},                 
	{0, L2C_READ_HIT, 		"L2C_READ_HIT"}, 		                 
	{0, L2C_READ_MISS, 		"L2C_READ_MISS"}, 		                 
	{0, L2C_WB_FULL, 		"L2C_WB_FULL"}, 		                 
	{0, TLB_MISS, 		        "TLB_MISS"}, 		                 
	{0, BR_TAKEN, 		        "BR_TAKEN"}, 		                 
	{0, WB_FULL, 		        "WB_FULL"}, 		                 
	{0, DCACHE_READ_BEAT, 	        "DCACHE_READ_BEAT"}, 	                 
	{0, DCACHE_WRITE_BEAT, 	        "DCACHE_WRITE_BEAT"}, 	                 
	{0, NO_DUAL_HW,		        "NO_DUAL_HW"}, 		                 
	{0, NO_DUAL_MULTIPLE, 	        "NO_DUAL_MULTIPLE"}, 	                 
	{0, BIU_ANY_ACCESS, 		"BIU_ANY_ACCESS"}, 		         
	{0, MAIN_TLB_REFILL_BY_ICACHE,  "MAIN_TLB_REFILL_BY_ICACHE"},               
	{0, MAIN_TLB_REFILL_BY_DCACHE,  "MAIN_TLB_REFILL_BY_DCACHE"},               
	{0, ICACHE_READ_BEAT, 	        "ICACHE_READ_BEAT"}, 	                 
	{0, WMMX2_STORE_FIFO_FULL, 	"WMMX2_STORE_FIFO_FULL"}, 	                 
	{0, WMMX2_FINISH_FIFO_FULL, 	"WMMX2_FINISH_FIFO_FULL"}, 	         
	{0, WMMX2_INST_FIFO_FULL, 	"WMMX2_INST_FIFO_FULL"}, 	                 
	{0, WMMX2_INST_RETIRED,	        "WMMX2_INST_RETIRED"}, 	                 
	{0, WMMX2_BUSY,		        "WMMX2_BUSY"}, 		                 
	{0, WMMX2_HOLD_MI, 		"WMMX2_HOLD_MI"}, 		                 
	{0, WMMX2_HOLD_MW, 		"WMMX2_HOLD_MW"}		                 
};

const int modeDesp_size = sizeof(modeDesp)/sizeof(PMUModeDesp);


