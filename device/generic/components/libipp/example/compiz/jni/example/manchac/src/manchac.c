/***************************************************************************************** 
Copyright (c) 2009, Marvell International Ltd. 
All Rights Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Marvell nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY MARVELL ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MARVELL BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************************/

#include "misc.h"
#include "manchac.h"
#include <string.h>
#include <stdio.h>

int screenWidth = -1;
int screenHeight = -1;
int bSupportX = 1;

/* Globals */
MANPerfCT               gPerfCT;        /* Performance counter struct */
MANMultiConfig          *gCurrConfig;   /* Ponter to current case config struct */
char*                   gCurrDir;       /* current directory, to accomodate WM */

int         g_Comp_Frame_Num[2][MAX_MULTISTRM_NUM];
long long   g_Comp_Total_Time[2][MAX_MULTISTRM_NUM];
long long   g_Comp_Tot_CPUTime[2][MAX_MULTISTRM_NUM];
long long   g_Comp_Tot_DeviceTime[2];

/* Macros */
#ifndef MAX_PATH
#define MAX_PATH        260
#endif

#define MAX_ARG_LEN 1024
char *IPP_Strcat(char *dst,const char *src);

IPP_FILE                    *inCfgFile = NULL, *outLogFile = NULL;

#ifdef DBG_MODE
#define PRINTF_TRACE(...)   fprintf(stdout, __VA_ARGS__);\
                            fflush(stdout);
#else
#define PRINTF_TRACE(...)  
#endif

void MANRunBenchMark(char *pCmdLine)
{
        char argv0[] = "";
        char argv1[MAX_ARG_LEN];
        char *argv[2];
        int  nThreadPerfIndex = -1;

        PRINTF_TRACE("Entering:%s\n", __FUNCTION__);

        IPP_Strcpy(argv1, pCmdLine);
        argv[0] = argv0;
        argv[1] = argv1;

        IPP_GetPerfCounter(&nThreadPerfIndex, (IPP_COUNTER_FUNC)IPP_TimeGetThreadTime, (IPP_COUNTER_FUNC)IPP_TimeGetThreadTime);
        if(nThreadPerfIndex == -1) {
                MANPrintf(stdout, "Benchmark failed: Allocate timer error\n");
                MANPrintf(outLogFile, "Benchmark failed: Allocate timer error\n");
                return;
        }
        IPP_ResetPerfCounter(nThreadPerfIndex);
        IPP_StartPerfCounter(nThreadPerfIndex);

        if(CodecTest(2, argv) == IPP_FAIL) {
                MANPrintf(stdout, "Benchmark failed: Codec error\n");
       			MANPrintf(outLogFile, "Benchmark failed: Codec error\n");
        }
        
        IPP_StopPerfCounter(nThreadPerfIndex);
        gPerfCT.ulThreadTick = IPP_GetPerfData(nThreadPerfIndex);
        IPP_FreePerfCounter(nThreadPerfIndex);

        PRINTF_TRACE("Exit:%s\n", __FUNCTION__);
}

static int MANBenchMarkProc(int mode, int loops)
{
        MANMultiConfig      *multiConfig, *tempConfig;
        int                 tid, iCount;
        char                cfgFileName[MAX_PATH], logFileName[MAX_PATH];
        long long           start_time = 0;

#ifdef  _IPP_WCE
extern void GetCurrentDir(char *dstDir);
        /* Init and save current directory */
        IPP_MemMalloc((void**)&gCurrDir, MAX_PATH, 8);
        GetCurrentDir(gCurrDir);
#endif

        PRINTF_TRACE("Entering:%s\n", __FUNCTION__);

        /* Read configure file*/
        if( (inCfgFile = IPP_Fopen(MAN_CONFIG_FILE,"r" )) == NULL ){
                MANPrintf(stdout, "Can't open config file %s.\r\n", cfgFileName);
                return IPP_FAIL;
        }
        if (IPP_STATUS_NOERR != MANGetPar(&multiConfig, inCfgFile)) {
                MANPrintf(stdout, "Configuration parse error.\r\n");
                return IPP_FAIL;
        }
        
        /* Open log file */
        outLogFile = IPP_Fopen(MAN_LOG_FILE,"w" );
        if( outLogFile == NULL ){
                MANPrintf(stdout, "Can't open log file %s\r\n", logFileName);
                return IPP_FAIL;
        }

        IPP_InitPerfCounter();

        MAN_LOG_HEADER(outLogFile, MAN_CODEC_NAME)

        tempConfig = multiConfig;
        while(tempConfig != NULL)
        {
            gCurrConfig = tempConfig;
            gPerfCT.pCntConfig = &(tempConfig->cntConfig);
            
            MAN_LOG_CASENAME((IPP_FILE*)stdout, tempConfig->cmdLine);
            MAN_LOG_CASENAME(outLogFile, tempConfig->cmdLine);

            for(iCount = 0; iCount < loops; iCount++) {
                /* Init performance logs */
                MANPerformanceInit(&gPerfCT);
                
                /* Start counter */
                start_time = IPP_TimeGetTickCount();
                
                if(IPP_ThreadCreate(&tid, 0, (void *)MANRunBenchMark, tempConfig->cmdLine)) {
                    MANPrintf(stdout, "Thread create error!\n");
                }
                if(IPP_ThreadDestroy(&tid,1)) {
                    MANPrintf(stdout, "Thread destroy error!\n");
                }
                
                /* stop counter */
                gPerfCT.ulAppTick = IPP_TimeGetTickCount() - start_time;
             
                /* calculate perf statistics */
                {
					int i;
					for(i=0;i<MAX_MULTISTRM_NUM;i++) {
            			gPerfCT.ulFrameNumber[i] = g_Comp_Frame_Num[MAN_CODEC_INDEX][i]; 
            			gPerfCT.ulCodecTick[i]   = g_Comp_Total_Time[MAN_CODEC_INDEX][i]; 
#ifdef HWCODEC
            			gPerfCT.ulCodecCPUTick[i]= g_Comp_Tot_CPUTime[MAN_CODEC_INDEX][i];
#endif
        			}
        			gPerfCT.ulDeviceTick  = g_Comp_Tot_DeviceTime[MAN_CODEC_INDEX];
            	}

                MANPerformaneStatistics(&gPerfCT);

                /* perf log */
                MANPerformanceLog(&gPerfCT, (IPP_FILE*)stdout);
                MANPerformanceLog(&gPerfCT, outLogFile);

                /* deinit */
                MANPerformanceDeinit(&gPerfCT);
            }

            tempConfig = tempConfig->next;
        }

        /* finish and clean up */
        IPP_Fclose(outLogFile);
        MANCleanupPar(multiConfig, inCfgFile);

        IPP_DeinitPerfCounter();
#ifdef _IPP_WCE
        IPP_MemFree((void**)&gCurrDir);
#endif
        PRINTF_TRACE("Exit:%s\n", __FUNCTION__);

        return IPP_OK;
}

static void MANDisplayConsoleStartupBanner(const char *codecName)
{
        MANPrintf(stdout, "\r\n\r\n");
        MANPrintf(stdout, "********************************************************************\r\n");
        MANPrintf(stdout, "        Marvell Manchac Benchmark\r\n");
        MANPrintf(stdout, "        for the Marvel Xscale Microprocessor.\r\n");
        MANPrintf(stdout, "        Benchmark Manchac %s for Linux based on IPP %s\r\n", MANCHAC_VER, IPP_VER);
        MANPrintf(stdout, "        %s\r\n\r\n", codecName);
        MANPrintf(stdout, "        Copyright (c) 2000-2006 Intel Corporation.\r\n");
        MANPrintf(stdout, "        2007-2008 Marvell International Ltd. \r\n");
        MANPrintf(stdout, "        All Rights Reserved.\r\n");
        MANPrintf(stdout, "********************************************************************\r\n\r\n");
}

#ifdef _IPP_WCE
#include <windows.h>

int _tmain(int argc, _TCHAR* argv[])
{
        int loops = 4;
        int mode  = MAN_SINGLE_MODE;

        MANDisplayConsoleStartupBanner(MAN_CODEC_NAME);
        MANBenchMarkProc(mode, loops);

        return 0;
}

#elif (defined _IPP_LINUX)
#if ANDROID && USE_APK
int main_func(int argc, char *argv[])
#else
int main(int argc, char* argv[])
#endif
{
        int loops  = 1;
        int mode = MAN_SINGLE_MODE;

#if 1
        int i;
        for(i=1; i<argc; ++i)
        {
            if(strcmp("-help", argv[i])==0)
            {
                printf("Supported options:\n");
                printf(" -w    : specify window width defalut value is %d, means full screen \n",screenWidth );
                printf(" -h    : specify window height default value is %d, means full screen \n",screenHeight );
                printf(" -x    : specify whether support x windows default value is %d, means support \n",bSupportX );
                printf(" -help : show options\n");   
                return 0;         
            }
            else if(strcmp("-w", argv[i])==0)
            {
                screenWidth = atoi(argv[++i]);
                printf("screenWidth = %d\n", screenWidth);
            }
            else if(strcmp("-h", argv[i])==0)
            {
                screenHeight = atoi(argv[++i]);
                printf("screenHeight = %d\n", screenHeight);
            }
            else if(strcmp("-x", argv[i])==0)
            {
                bSupportX = atoi(argv[++i]);
                printf("bSupportX = %d\n", bSupportX);
            }
            else
            {
                printf("%s - Unknown option: %s\n", argv[0], argv[i]);
                printf("Supported options:\n");
                printf(" -w    : specify window width defalut value is %d, means full screen \n",screenWidth );
                printf(" -h    : specify window height default value is %d, means full screen \n",screenHeight );
                printf(" -x    : specify whether support x windows default value is %d, means support \n",bSupportX );
                printf(" -help : show options\n");   
                return -1;
            }
        }

#else
        if(argc > 1){
                if( !IPP_Strcmp(argv[1], "SERIAL") || !IPP_Strcmp(argv[1], "serial")){
                        mode = MAN_SERIAL_MODE;
                }
                else if(!IPP_Strcmp(argv[1], "PARALLEL") || !IPP_Strcmp(argv[1], "parallel")){
                        mode = MAN_PARALLEL_MODE;
                }
                else if(!IPP_Strcmp(argv[1], "LOOP") || !IPP_Strcmp(argv[1], "loop")){
                        loops = IPP_Atoi(argv[2]);
                } 
        }

        if(MAN_SINGLE_MODE != mode) {
                MANPrintf(stdout, "Unsupported Parallel Mode.\n");
                return -1;
        }
#endif

        MANDisplayConsoleStartupBanner(MAN_CODEC_NAME);
        MANBenchMarkProc(mode, loops);
        
        return 0;
}
#else
#error _IPP_WCE,_IPP_LINUX,or _IPP_NUCLEUS must be defined
#endif



/* EOF */

