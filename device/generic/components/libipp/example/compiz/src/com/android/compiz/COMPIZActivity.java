/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.compiz;

import android.app.Activity;
import android.os.Bundle;
import java.lang.ref.WeakReference;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.os.Looper;

public class COMPIZActivity extends Activity {

    COMPIZView mView;
    private final static String TAG = "COMPIZActivity";
    private final static int STATE_CHANGED = 0; 
    private EventHandler mEventHandler;
    private static String mStateString;

    static {
	System.loadLibrary("compiz");
	native_init();
    }

    @Override
    protected void onCreate(Bundle icicle) {
        mOnInfoListener = null;
        Looper looper;

	super.onCreate(icicle);
        mView = new COMPIZView(getApplication());
	mView.setFocusableInTouchMode(true);
	setContentView(mView);

        if ((looper = Looper.myLooper()) != null) {
            mEventHandler = new EventHandler(this, looper);
        } else if ((looper = Looper.getMainLooper()) != null) {
            mEventHandler = new EventHandler(this, looper);
        } else {
            mEventHandler = null;
        }
	native_setup(new WeakReference<COMPIZActivity>(this));
    }

    @Override
    protected void onDestroy() {
	native_deinit();
        mView.onDestroy();	
        super.onDestroy();
	android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    protected void onPause() {
        super.onPause();
        mView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mView.onResume();
    }

    public interface OnInfoListener
    {
       void onInfo(String str);
    }

    private OnInfoListener mOnInfoListener;
    public void setOnInfoListener(OnInfoListener l) {
        mOnInfoListener = l;
    }

    private static void NormalExitCallback(Object COMPIZActivity_ref, String str)
    {
        COMPIZActivity actv = (COMPIZActivity)((WeakReference)COMPIZActivity_ref).get();
        if (null == actv) {
            Log.i(TAG, "[warning]: WeakReference is null!\n");
            return;
        }
	
	actv.native_deinit();
	android.os.Process.killProcess(android.os.Process.myPid());
    }
    
    private static void KillProgCallback(Object COMPIZActivity_ref, String str)
    {
        COMPIZActivity actv = (COMPIZActivity)((WeakReference)COMPIZActivity_ref).get();
        if (null == actv) {
	     Log.i(TAG, "[warning]: WeakReference is null!\n");
             return;
        }

	actv.onDestroy();
    }

    private static void postEventFromNative(Object COMPIZActivity_ref, String str)
    {
        COMPIZActivity actv = (COMPIZActivity)((WeakReference)COMPIZActivity_ref).get();
        if (null == actv) {
	     Log.i(TAG, "[warning]: WeakReference is null!\n");
             return;
        }

        if (actv.mEventHandler != null) {
            mStateString = str;
            Message m = actv.mEventHandler.obtainMessage(STATE_CHANGED);
            actv.mEventHandler.sendMessage(m);
        } 
    }

    private class EventHandler extends Handler
    {
        private COMPIZActivity mAct;
        
        public EventHandler(COMPIZActivity actv, Looper looper) {
            super(looper);
            mAct = actv;
       }

       @Override
       public void handleMessage(Message msg) {
           boolean error_was_handled = false;
           switch(msg.what) {
           case STATE_CHANGED:
               Log.i(TAG, "mOnInfoListener: " + mStateString);
               if (mOnInfoListener != null) {
                   mOnInfoListener.onInfo(mStateString);
               } 
               return;
           default:
               Log.e(TAG, "Unknown messange type " + msg.what);
               return;
           }
       }
    }

    private static native final void native_init();

    private native final void native_setup(Object COMPIZActivity_this) throws IllegalStateException;

    private native final void native_deinit();
}
