1. Build demo:
    1.1 Android: 
        >> cd <compiz_path>/jni
        >> mm
	    >> cd <compiz_path>
	    >> mm
    1.2 Generic Linux:
        >> cd <compiz_path>/example/manchac/build/wmmx2_linux
        >> make

    For different version, user need to change the CPU type in makefile accordingly
    
    External Dependency:
    DOVE:	libxxf86vm-dev
    MG1:	N/A
    MMP2:	N/A
    
    For all the platform, user also place ipp include files in ../../include/ or modify jni/Android.mk

2. Install:
    2.1 Android:
    Get the apk and libcompiz.so, then:
    >>adb install COMPIZ.apk
    >>adb push libcompiz.so /system/lib
    if push .so failed, user may need to run command:
    >>adb shell mount -o remount,rw -t ext3 /dev/block/mmcblk0p2 /system

    2.2 Generic Linux:
    cp manchac_vmeta_decoder $(WORKING_PATH)

3. Run:
    3.1 Android:
    press the COMPIZ icon

    For andorid, user must guarantee the config file: vmeta_decoder.cfg in /sdcard/compiz/
    user can config stream count and stream name by modify vmeta_decoder.cfg

    also, user must guarantee the:
    /sdcard/compiz/bmp/armada_.bmp
    /sdcard/compiz/bmp/particle.bmp
    /sdcard/compiz/bmp/marvell.bmp
    are valid bmp files.

    The upper bmp files and config file can get from $(compiz_path)/jni/resource/

    3.2 Generic Linux:
    You must make sure there is a config file: "vmeta_decoder.cfg" in the same directory with the executable. The bmps are also needed:
    $(WORKING_PATH)/bmp/armada_.bmp
    $(WORKING_PATH)/bmp/particle.bmp
    $(WORKING_PATH)/bmp/marvell.bmp

    The parameter of executable is:
    -w    : specify window width  defalut value is -1, means full screen.
    -h    : specify window height default value is -1, means full screen.
    -x    : specify whether support x windows default value is 1, means support key board response.
    

4. Key Control:
    There are 6 3d scenes (draw types) in all as below , 
    1) CUBE
    2) WAVE TEXTURE
    3) MOVING TEXTURE
    4) BLENDING TEXTURE
    5) ROTATE CUBE
    6) SPHERE
 
     "ENTER/CALL" key:   switch 3d scene (draw types)
     "SPACE"      key:   swtich draw modes for MOVING TEXTURE and BLENDING TEXTURE
     "1"          key:   specal swtich draw mode only for MOVING TEXTURE
     "2"          key:   enalbe or disable fireworks effect
     "ESC/DEL"    key:   quit demo
