####################################################################
# Compiz Demo
# This makefile builds both an activity and shared library
####################################################################
ifneq ($(TARGET_SIMULATOR), true) # not 64 bit clean

# Build activity

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

#LOCAL_MODULE_TAGS := optional 

LOCAL_SRC_FILES := $(call all-subdir-java-files)

LOCAL_PACKAGE_NAME  := COMPIZ 

include $(BUILD_PACKAGE)

endif # TARGET_SIMULATOR
