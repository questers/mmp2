/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_SENSORS_H
#define ANDROID_SENSORS_H

#include <stdint.h>
#include <errno.h>
#include <sys/cdefs.h>
#include <sys/types.h>

#include <linux/input.h>

#include <hardware/hardware.h>
#include <hardware/sensors.h>

#ifndef LOG_TAG
#define LOG_TAG "Sensors"
#endif

class SensorBase;

#if defined(__cplusplus)
extern "C" {
#endif


/*****************************************************************************/

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

#define ID_A  (0)
#define ID_M  (1)
#define ID_O  (2)
#define ID_L  (3)
#define ID_P  (4)
#define ID_GY (5)
#define ID_GR (6)
#define ID_LA (7)
#define ID_RV (8)

/*****************************************************************************/

/*
 * The SENSORS Module
 */

/* the GP2A is a binary proximity sensor that triggers around 5 cm on
 * this hardware */
#define PROXIMITY_THRESHOLD_GP2A  5.0f

/*****************************************************************************/

#define EVENT_TYPE_ACCEL_X          REL_Y
#define EVENT_TYPE_ACCEL_Y          REL_X
#define EVENT_TYPE_ACCEL_Z          REL_Z

#define EVENT_TYPE_YAW              REL_RX
#define EVENT_TYPE_PITCH            REL_RY
#define EVENT_TYPE_ROLL             REL_RZ
#define EVENT_TYPE_ORIENT_STATUS    REL_WHEEL

#define EVENT_TYPE_MAGV_X           REL_DIAL
#define EVENT_TYPE_MAGV_Y           REL_HWHEEL
#define EVENT_TYPE_MAGV_Z           REL_MISC

#define EVENT_TYPE_PROXIMITY        ABS_DISTANCE
#define EVENT_TYPE_LIGHT            ABS_VOLUME

#define EVENT_TYPE_GYRO_X           ABS_HAT1X
#define EVENT_TYPE_GYRO_Y           ABS_HAT1Y
#define EVENT_TYPE_GYRO_Z           ABS_GAS


#define LSB                         (64.0f)
#define NUMOFACCDATA                (8.0f)

// conversion of acceleration data to SI units (m/s^2)
#define RANGE_A                     (2*GRAVITY_EARTH)
#define CONVERT_A                   (GRAVITY_EARTH / LSB / NUMOFACCDATA / 2) /* add /2. -guang */
#define CONVERT_A_X                 (CONVERT_A)
#define CONVERT_A_Y                 (CONVERT_A)
#define CONVERT_A_Z                 (CONVERT_A)

// conversion of magnetic data to uT units
#define CONVERT_M                   (1.0f/16.0f)
#define CONVERT_M_X                 (-CONVERT_M)
#define CONVERT_M_Y                 (-CONVERT_M)
#define CONVERT_M_Z                 (-CONVERT_M)

/* conversion of orientation data to degree units */
#define CONVERT_O                   (1.0f/64.0f)
#define CONVERT_O_A                 (CONVERT_O)
#define CONVERT_O_P                 (CONVERT_O)
#define CONVERT_O_R                 (-CONVERT_O)

// conversion of gyro data to SI units (radian/sec)
#define RANGE_GYRO                  (2000.0f*(float)M_PI/180.0f)
#define CONVERT_GYRO                ((70.0f / 1000.0f) * ((float)M_PI / 180.0f))
#define CONVERT_GYRO_X              (CONVERT_GYRO)
#define CONVERT_GYRO_Y              (-CONVERT_GYRO)
#define CONVERT_GYRO_Z              (CONVERT_GYRO)

#define SENSOR_STATE_MASK           (0x7FFF)

#define MAGIO                   0xA1
#define MAG_IOCTL_WRITE         _IOW(MAGIO, 0x01, char*)
#define MAG_IOCTL_READ          _IOWR(MAGIO, 0x02, char*)
#define MAG_IOCTL_RESET         _IO(MAGIO, 0x03)
#define MAG_IOCTL_SET_MODE      _IOW(MAGIO, 0x04, short)
#define MAG_IOCTL_GETDATA       _IOR(MAGIO, 0x05, char[5])
#define MAG_IOCTL_SET_DELAY     _IOW(MAGIO, 0x18, int64_t)
#define MAG_IOCTL_GET_DELAY     _IOR(MAGIO, 0x30, int64_t)

struct acc_data {
	__s16 x;
	__s16 y;
	__s16 z;
};
#define ACC_IOCTL_BASE 'a'
#define ACC_IOCTL_SET_DELAY       _IOW(ACC_IOCTL_BASE, 0, int64_t)
#define ACC_IOCTL_GET_DELAY       _IOR(ACC_IOCTL_BASE, 1, int64_t)
#define ACC_IOCTL_READ_ACCEL_XYZ  _IOR(ACC_IOCTL_BASE, 8, struct acc_data)

/*****************************************************************************/
int addFd(void* context,SensorBase* sensor,int fd);
int remFd(void* context,int fd);


#if defined(__cplusplus)
}
#endif
#endif  // ANDROID_SENSORS_H
