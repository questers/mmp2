/******************** (C) COPYRIGHT 2009 STMicroelectronics ********************
* File Name          : MEMSAlgLib_Fusion.h
* Department         : APM G.C. FAE TEAM
* Author             : Travis Tu
* Date First Issued  : 2010.11.11
********************************************************************************
* History:
* 2010.12.11  V0.1
********************************************************************************
* THE PRESENT SOFTWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR REFERENCE OR
* EDUCATION. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
* DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
* FROM THE CONTENT OF SUCH SOFTWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
* CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

#ifndef __MEMSALGLIB_FUSION_H
#define __MEMSALGLIB_FUSION_H
typedef struct
{
  long ax;
  long ay;
  long az;
  long mx;
  long my;
  long mz;
  long gx;
  long gy;
  long gz;
  long Azimuth;
  long pitch;
  long roll;
  long time; //system time in ms
}NineAxisTypeDef;

typedef struct
{
  float a;
  float b;
  float c;
  float d;
}QuaternionTypeDef;

typedef struct
{
  float x;
  float y;
  float z;
}GravityTypeDef;

typedef struct
{
  float x;
  float y;
  float z;
}LinearAccTypeDef;

typedef struct
{
	QuaternionTypeDef q;
	GravityTypeDef	  g;
	LinearAccTypeDef  l;
}FusionTypeDef;


//void MEMSAlgLib_Fusion_Init(long oneGravityValue, long oneGaussValue, float oneDSPValue);

//gyro in ST's LSB short to long
//acc in mGravity
//mag in mGauss
FusionTypeDef MEMSAlgLib_Fusion_Update(NineAxisTypeDef v);


//to get the record data
int  MEMSAlgLib_Fusion_GetStoreValuesLength(void);
void MEMSAlgLib_Fusion_GetStoreValues(unsigned char* buf);
void MEMSAlgLib_Fusion_SetStoreValues(unsigned char* buf);


//property API
//algorithm customization API
void MEMSAlgLib_Fusion_SetMode(int mode);
void MEMSAlgLib_FuSION_LPF(int active);
void MEMSAlgLib_FuSION_AntiInterfere(int active);
long MEMSAlgLib_Get(int id);

//property API
void MEMSAlgLib_Config(int id, float value);
#endif
