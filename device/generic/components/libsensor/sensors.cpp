/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <hardware/sensors.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include <math.h>
#include <poll.h>
#include <pthread.h>
#include <stdlib.h>

#include <linux/input.h>

#include <utils/Atomic.h>
#include <utils/Log.h>
#include <utils/Timers.h>
#include <utils/KeyedVector.h>
#include <utils/threads.h>
#include <cutils/properties.h>

#include "sensors.h"
#include "SensorBase.h"
#include "LightSensor.h"
#include "ProximitySensor.h"
#include "iNemoSensor.h"

using namespace android;
/*****************************************************************************/

#define DELAY_OUT_TIME 0x7FFFFFFF

#define LIGHT_SENSOR_POLLTIME    2000000000


#define SENSORS_ACCELERATION     (1<<ID_A)
#define SENSORS_MAGNETIC_FIELD   (1<<ID_M)
#define SENSORS_ORIENTATION      (1<<ID_O)
#define SENSORS_LIGHT            (1<<ID_L)
#define SENSORS_PROXIMITY        (1<<ID_P)
#define SENSORS_GYROSCOPE        (1<<ID_GY)

#define SENSORS_ACCELERATION_HANDLE     0
#define SENSORS_MAGNETIC_FIELD_HANDLE   1
#define SENSORS_ORIENTATION_HANDLE      2
#define SENSORS_LIGHT_HANDLE            3
#define SENSORS_PROXIMITY_HANDLE        4
#define SENSORS_GYROSCOPE_HANDLE        5
#define SENSORS_GRAVITY_HANDLE              6
#define SENSORS_LINEAR_ACCELERATION_HANDLE  7
#define SENSORS_ROTATION_VECTOR_HANDLE      8


#define MAX_FDS 16

/*****************************************************************************/

/* The SENSORS Module */
static const struct sensor_t sSensorList[] = {
    /*[SENSORS_ACCELERATION_HANDLE] = */{
        "accelerameter",
        "STMicroelectronics",
        1, 
        SENSORS_ACCELERATION_HANDLE,
        SENSOR_TYPE_ACCELEROMETER, 
        RANGE_A, 
        CONVERT_A, 
        0.23f, 
        20000, 
        { } 
    },

    /*[SENSORS_MAGNETIC_FIELD_HANDLE] = */{ 
        "magnetometer",
        "STMicroelectronics",
        1, 
        SENSORS_MAGNETIC_FIELD_HANDLE,
        SENSOR_TYPE_MAGNETIC_FIELD, 
        2000.0f, 
        CONVERT_M, 
        6.8f, 
        16667, 
        { } 
    },
    
    /*[SENSORS_ORIENTATION_HANDLE] = */{
        "iNemo Orientation sensor",
        "STMicroelectronics",
        1, 
        SENSORS_ORIENTATION_HANDLE,
        SENSOR_TYPE_ORIENTATION, 
        360.0f, 
        CONVERT_O, 
        7.8f, 
        16667, 
        { } 
    },
        
    /*[SENSORS_LIGHT_HANDLE] = */{ 
        "lightsensor",
        "STMicroelectronics",
        1, 
        SENSORS_LIGHT_HANDLE,
        SENSOR_TYPE_LIGHT, 
        4096*0.5, 
        0.5f, 
        3.0f, 
        0, 
        { } 
    },

    /*[SENSORS_PROXIMITY_HANDLE] = */{ 
        "proximity",
        "STMicroelectronics",
        1, 
        SENSORS_PROXIMITY_HANDLE,
        SENSOR_TYPE_PROXIMITY, 
        3000.0f, 
        1.0f, 
        0.75f, 
        0, 
        { } 
    },

    /*[SENSORS_GYROSCOPE_HANDLE] = */{ 
        "gyroscope",
        "STMicroelectronics",
        1, 
        SENSORS_GYROSCOPE_HANDLE,
        SENSOR_TYPE_GYROSCOPE, 
        RANGE_GYRO, 
        CONVERT_GYRO, 
        6.1f, 
        1190, 
        { } 
    },
        
    /*[SENSORS_GRAVITY_HANDLE] = */{ 
        "iNemo Gravity sensor",
        "STMicroelectronics",
        1, 
        SENSORS_GRAVITY_HANDLE,
        SENSOR_TYPE_GRAVITY, 
        1, 
        1, 
        1, 
        0, 
        { } 
    },

    /*[SENSORS_LINEAR_ACCELERATION_HANDLE] =*/
    { 
        "iNemo Linear Acceleration sensor",
        "STMicroelectronics",
        1, 
        SENSORS_LINEAR_ACCELERATION_HANDLE,
        SENSOR_TYPE_LINEAR_ACCELERATION, 
        1, 
        1, 
        1, 
        0, 
        { } 
    },

    /*[SENSORS_ROTATION_VECTOR_HANDLE] = */{ 
        "iNemo Rotation Vector sensor",
        "STMicroelectronics",
        1, 
        SENSORS_ROTATION_VECTOR_HANDLE,
        SENSOR_TYPE_ROTATION_VECTOR, 
        1, 
        1, 
        1, 
        0, 
        { } 
     },
};

static struct sensor_t sSensorListDetected[SENSORS_ROTATION_VECTOR_HANDLE+1];


static int open_sensors(const struct hw_module_t* module, const char* id,
                        struct hw_device_t** device);


static int sensors__get_sensors_list
(
    struct sensors_module_t* module,
    struct sensor_t const** list
)
{
    int count = 0;
    char line[128];
    int acc_ok = 0;
    int mag_ok = 0;
    int gyro_ok = 0;
    const char* path = "/sys/bus/platform/devices/skm/sensorslist";
    const char* acc_name = "accelerameter";
    const char* mag_name = "magnetometer";
    const char* gyro_name = "gyroscope";
    const char* light_name = "lightsensor";

    FILE* fp = fopen(path, "r");
    if (!fp)
    {
        LOGE("Can't open %s", path);
        goto error;
    }

    while (fgets(line, sizeof(line), fp))
    {
        if (!strncmp(line, acc_name, strlen(acc_name)))
        {
            sSensorListDetected[count++] = sSensorList[SENSORS_ACCELERATION_HANDLE];
            acc_ok = 1;
            LOGI("Detected acc sensor");
        }
        
        if (!strncmp(line, mag_name, strlen(mag_name)))
        {
            sSensorListDetected[count++] = sSensorList[SENSORS_MAGNETIC_FIELD_HANDLE];
            mag_ok = 1;
            LOGI("Detected mag sensor");
        }

        if (!strncmp(line, gyro_name, strlen(gyro_name)))
        {
            sSensorListDetected[count++] = sSensorList[SENSORS_GYROSCOPE_HANDLE];
            gyro_ok = 1;
            LOGI("Detected gyro sensor");
        }
        
        if (!strncmp(line, light_name, strlen(light_name)))
        {
            sSensorListDetected[count++] = sSensorList[SENSORS_LIGHT_HANDLE];
            LOGI("Detected light sensor");
        }
    }

    fclose(fp);

    if (acc_ok && mag_ok && gyro_ok)
    {    
        LOGD("to turn off nemo sensors, type: setprop persist.sensors.nemo 0 (default 1)");
        int nemo_enabled;
        char property[PROPERTY_VALUE_MAX];
        property_get("persist.sensors.nemo",property,"1");
        nemo_enabled=atoi(property);
        if(!nemo_enabled)
        {
            LOGD("All(acc/mag/gyro) present, but nemo disabled add others");
        }
        else
       {
            LOGD("All(acc/mag/gyro) present, nemo enabled");
            sSensorListDetected[count++] = sSensorList[SENSORS_ORIENTATION_HANDLE];
            sSensorListDetected[count++] = sSensorList[SENSORS_GRAVITY_HANDLE];
            sSensorListDetected[count++] = sSensorList[SENSORS_LINEAR_ACCELERATION_HANDLE];
            sSensorListDetected[count++] = sSensorList[SENSORS_ROTATION_VECTOR_HANDLE];
       }
    }
    
    LOGE("%d sensors detected", count);
    *list = sSensorListDetected;
    return count;

error:
    *list = NULL;
    return 0;
}

static struct hw_module_methods_t sensors_module_methods = {
        open: open_sensors
};

struct sensors_module_t HAL_MODULE_INFO_SYM = {
        common: {
                tag: HARDWARE_MODULE_TAG,
                version_major: 1,
                version_minor: 0,
                id: SENSORS_HARDWARE_MODULE_ID,
                name: "ST Sensor module",
                author: "STMicroelectronics",
                methods: &sensors_module_methods,
        },
        get_sensors_list: sensors__get_sensors_list,
};

struct sensors_poll_context_t {
    struct sensors_poll_device_t device; // must be first

        sensors_poll_context_t();
        ~sensors_poll_context_t();
    int activate(int handle, int enabled);
    int setDelay(int handle, int64_t ns);
    int pollEvents(sensors_event_t* data, int count);
    SensorBase* fd2Sensor(int fd);

private:
    enum {
        light           = 0,
        proximity      = 1,
        iNemo           = 2, 
        numSensorDrivers,
    };


    SensorBase* mSensors[numSensorDrivers];
    
    

    int handleToDriver(int handle) const {
        switch (handle) {
            case ID_A:
            case ID_M:
            case ID_O:
            case ID_GY:
            case ID_GR:
            case ID_LA:
            case ID_RV:
                return iNemo;
            case ID_P:
                return proximity;
            case ID_L:
                return light;
        }
        return -EINVAL;
    }
    public:
        KeyedVector <int, SensorBase*> mManagedSensors;    
        struct pollfd ufds[MAX_FDS];
        struct pollfd new_ufds[MAX_FDS];        
        int nfds;
        int new_nfds;
    
        int mWritePipeFd;
        int mReadPipeFd;
        static const char WAKE_MESSAGE = 'W';
};

/*****************************************************************************/
const char* sensor_id(int32_t handle)
{
    const char* ids[]=
    {
       "accelerameter",
       "magnetometer",
       "orientation",
       "light",
       "proximity",
       "gyroscope",
       "gravity",
       "linear accelerate",
       "rotation vector",
       "unknown"
    };
    if(handle<(ID_RV+1))
        return ids[handle];
    return ids[ID_RV+1];
}

int addFd(void* context,SensorBase* sensor,int fd)
{
    sensors_poll_context_t *ctx = (sensors_poll_context_t *)context;
    ssize_t index;

    index=ctx->mManagedSensors.indexOfKey(fd);
    if(index>=0)
    {
        LOGW("sensor fd %d already polled\n",fd);
        return 0;
    }

    if(fd<0) 
    {
        LOGW("invalid sensor fd %d\n",fd);
        return -1;
    }
    if(ctx->new_nfds<MAX_FDS)    
    {
        //add to vector key,value
        ctx->mManagedSensors.add(fd,sensor);
        
        ctx->new_ufds[ctx->new_nfds].fd = fd;
        ctx->new_ufds[ctx->new_nfds].events = POLLIN;
        ctx->new_ufds[ctx->new_nfds].revents = 0;
        ctx->new_nfds++;
        
    }
    
    
    return 0;
    
}
int remFd(void* context,int fd)
{
    sensors_poll_context_t *ctx = (sensors_poll_context_t *)context;
    ssize_t index;


    index=ctx->mManagedSensors.indexOfKey(fd);
    if(index<0)
    {
        LOGW("sensor fd %d not managed but trying to remove it",fd);
        return -1;
    }

    int i;
    for(i = 0; i < ctx->new_nfds; i++) {
        if(ctx->new_ufds[i].fd== fd) {
            //remove fd from managed sensors
            //key
            ctx->mManagedSensors.removeItem(fd);
            
            int count = ctx->new_nfds - i - 1;
            memmove(ctx->new_ufds + i, ctx->new_ufds + i + 1, sizeof(ctx->new_ufds[0]) * count);
            ctx->new_nfds--;
            return 0;
        }
    }


    
    return -1;
}

sensors_poll_context_t::sensors_poll_context_t()
{
    char property[PROPERTY_VALUE_MAX];
    nfds=new_nfds=0;
    memset(ufds,0,sizeof(struct pollfd)*MAX_FDS);
    memset(new_ufds,0,sizeof(struct pollfd)*MAX_FDS);    
    #if 0
    mSensors[light] = new LightSensor(this);
    mPollFds[light].fd = mSensors[light]->getFd();
    mPollFds[light].events = POLLIN;
    mPollFds[light].revents = 0;

    mSensors[proximity] = new ProximitySensor(this);
    mPollFds[proximity].fd = mSensors[proximity]->getFd();
    mPollFds[proximity].events = POLLIN;
    mPollFds[proximity].revents = 0;

    mSensors[iNemo] = new iNemoSensor(this);
    mPollFds[iNemo].fd = mSensors[iNemo]->getFd();
    mPollFds[iNemo].events = POLLIN;
    mPollFds[iNemo].revents = 0;
    
    int wakeFds[2];
    int result = pipe(wakeFds);
    LOGE_IF(result<0, "error creating wake pipe (%s)", strerror(errno));
    fcntl(wakeFds[0], F_SETFL, O_NONBLOCK);
    fcntl(wakeFds[1], F_SETFL, O_NONBLOCK);
    mWritePipeFd = wakeFds[1];

    mPollFds[wake].fd = wakeFds[0];
    mPollFds[wake].events = POLLIN;
    mPollFds[wake].revents = 0;
    mRead = wakeFds[0];
    #else
    //new design leave fd action to sensor driver itself
    //and we only add wakeup fd which is created by myself.
    int wakeFds[2];
    int result = pipe(wakeFds);
    LOGE_IF(result<0, "error creating wake pipe (%s)", strerror(errno));
    fcntl(wakeFds[0], F_SETFL, O_NONBLOCK);
    fcntl(wakeFds[1], F_SETFL, O_NONBLOCK);
    mReadPipeFd = wakeFds[0];
    mWritePipeFd = wakeFds[1];

    ufds[0].fd = new_ufds[0].fd = mReadPipeFd;
    ufds[0].events= new_ufds[0].events = POLLIN;    
    nfds=new_nfds=1;
    mSensors[light] = new LightSensor(this);
    mSensors[proximity] = new ProximitySensor(this);
    mSensors[iNemo] = new iNemoSensor(this);
    
    #endif

    LOGD("to see more message, type: setprop persist.sensors.level <level> (the higher level, the more message)");
    property_get("persist.sensors.level",property,"0");
    SensorBase::debug_level=atoi(property);
}

sensors_poll_context_t::~sensors_poll_context_t() {
    int sensors=mManagedSensors.size();    
    while(mManagedSensors.size())
    {
        SensorBase* sensor = mManagedSensors.valueAt(0);
        mManagedSensors.removeItemsAt(0);
        if(sensor)
            delete sensor;        
    }
    if(mReadPipeFd>=0) close(mReadPipeFd);
    if(mWritePipeFd>=0) close(mWritePipeFd);
}

int sensors_poll_context_t::activate(int handle, int enabled) {    

    LOGD("%s sensor %s",enabled?"enable":"disable",sensor_id(handle));
    
    int index = handleToDriver(handle);
    if (index < 0) return index;
    int err =  mSensors[index]->enable(handle, enabled);
    
    if (!err) {
        const char wakeMessage(WAKE_MESSAGE);
        int result = write(mWritePipeFd, &wakeMessage, 1);
        LOGE_IF(result<0, "error sending wake message (%s)", strerror(errno));
    }
    return err;
}

int sensors_poll_context_t::setDelay(int handle, int64_t ns) {

    int index = handleToDriver(handle);
    if (index < 0) return index;
    LOGD("set sensor %s delay ->%ldms",sensor_id(handle),(long)ns2ms(ns));
    
    return mSensors[index]->setDelay(handle, ns);
}

int sensors_poll_context_t::pollEvents(sensors_event_t* data, int count)
{
   
    int nbEvents = 0;
    int n = 0;
    
    do {
        // see if we have some leftover from the last poll()
        for (int i=0 ; count && i<(int)mManagedSensors.size(); i++) {            
            SensorBase* sensor = mManagedSensors.valueAt(i);
            if(sensor&&sensor->hasPendingEvents())
            {                
                int nb = sensor->readEvents(sensor->getFd(),data, count);
                
                if (nb < count) {
                    // no more data for this sensor
                    ufds[i].revents = 0;
                }
                count -= nb;
                nbEvents += nb;
                data += nb;                        
            }
        }
        for (int i=0 ; count && i<nfds ; i++) {
            if(ufds[i].revents & POLLIN)
            {
                if(ufds[i].fd == mReadPipeFd)
                {
                    char msg;
                    int result = read(ufds[i].fd, &msg, 1);
                    LOGE_IF(result<0, "error reading from wake pipe (%s)", strerror(errno));
                    LOGE_IF(msg != WAKE_MESSAGE, "unknown message on wake queue (0x%02x)", int(msg));
                    ufds[i].revents = 0;       
                }
                else
                {
                    SensorBase* sensor = fd2Sensor(ufds[i].fd);
                    if(sensor)
                    {
                        int nb = sensor->readEvents(ufds[i].fd,data, count);
                        
                        if (nb < count) {
                            // no more data for this sensor
                            ufds[i].revents = 0;
                        }
                        count -= nb;
                        nbEvents += nb;
                        data += nb;                        
                    }
                    
                }
            }
        }

        if (count) {
            // we still have some room, so try to see if we can get
            // some events immediately or just wait if we don't have
            // anything to return
            if(nfds!=new_nfds)
            {
                memcpy(ufds,new_ufds,new_nfds*sizeof(struct pollfd));
                nfds = new_nfds;
            }
            
            
            n = poll(ufds, nfds, nbEvents ? 0 : -1);
            //LOGD("sensors_poll_context_t::pollEvents(): poll(): count=%d, nbEvents=%d, return: %d", count, nbEvents,n);

            if (n<0) {
                LOGE("poll() failed (%s)", strerror(errno));
                return -errno;
            }

        }
        // if we have events and space, go read them
    } while (n && count);


    //LOGD("sensors_poll_context_t::pollEvents(): return : %d", nbEvents);
    return nbEvents;
}

SensorBase* sensors_poll_context_t::fd2Sensor(int fd)
{
    int index=mManagedSensors.indexOfKey(fd);
    if(index>=0)
        return mManagedSensors.valueAt(index);
    return NULL;    
}


/*****************************************************************************/

static int poll__close(struct hw_device_t *dev)
{
    sensors_poll_context_t *ctx = (sensors_poll_context_t *)dev;
    if (ctx) {
        delete ctx;
    }
    return 0;
}

static int poll__activate(struct sensors_poll_device_t *dev,
        int handle, int enabled) {
    sensors_poll_context_t *ctx = (sensors_poll_context_t *)dev;
    return ctx->activate(handle, enabled);
}

static int poll__setDelay(struct sensors_poll_device_t *dev,
        int handle, int64_t ns) {
    sensors_poll_context_t *ctx = (sensors_poll_context_t *)dev;
    return ctx->setDelay(handle, ns);
}

static int poll__poll(struct sensors_poll_device_t *dev,
        sensors_event_t* data, int count) {
    sensors_poll_context_t *ctx = (sensors_poll_context_t *)dev;
    return ctx->pollEvents(data, count);
}

/*****************************************************************************/

/** Open a new instance of a sensor device using name */
static int open_sensors(const struct hw_module_t* module, const char* id,
                        struct hw_device_t** device)
{
        int status = -EINVAL;
        sensors_poll_context_t *dev = new sensors_poll_context_t();

        memset(&dev->device, 0, sizeof(sensors_poll_device_t));

        dev->device.common.tag = HARDWARE_DEVICE_TAG;
        dev->device.common.version  = 0;
        dev->device.common.module   = const_cast<hw_module_t*>(module);
        dev->device.common.close    = poll__close;
        dev->device.activate        = poll__activate;
        dev->device.setDelay        = poll__setDelay;
        dev->device.poll            = poll__poll;

        *device = &dev->device.common;
        status = 0;

        return status;
}

