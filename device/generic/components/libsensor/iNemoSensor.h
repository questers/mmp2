/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_INEMO_SENSOR_H
#define ANDROID_INEMO_SENSOR_H

#include <stdint.h>
#include <errno.h>
#include <sys/cdefs.h>
#include <sys/types.h>

#include "sensors.h"
#include "SensorBase.h"
#include "InputEventReader.h"
#include "MEMSAlgLib_eCompass.h"

/*****************************************************************************/

struct input_event;
class iNemoSensor : public SensorBase {
private:
    int mGyroEnabled;
    int mAccEnabled;
    int mMagEnabled;
    int mOrEnabled;
    int mGrEnabled;
    int mLAEnabled;
    int mRVEnabled;
    uint32_t mPendingMask;

    bool mHasPendingEvent;
    char input_sysfs_path[PATH_MAX];
    int  input_sysfs_path_len;

    InputEventCircularReader mInputReader;

    enum {
	Gy	  = 0,
	Acc  = 1,
	Mag  = 2,
	Or   = 3,
	Gr   = 4,
    LA   = 5,
	RV	 = 6,
    numSensors
    };

    sensors_event_t mPendingEvents[numSensors];

    int  mAccDataFd;                   //handle of accelerometer input device
    int  mMagDataFd;                   //handle of magnetometer input device
    int  mGyrDataFd;				   //handle of gyroscope input device

    short accData[3];
    short magData[3];
    short gyroData[3];
	ECOM_ValueTypeDef calib_val;

	bool mNemoEnabled;

    //the implements of the following 7 functions, depending on the drivers, is only a reference,
    //you can rewrite them according your driver implement
    int getAccData(short * data);  //get accelerometer data from input device
    int getMagData(short * data);  //get magnetometer data from input device
    int getGyroData(short * data);  //get gyro data from input device
    void filtGyroData(short * data);
    int accEnable(int en);             //enable/disable accelerometer
    int magEnable(int en);             //enable/disable magnetometer
    int gyroEnable(int en);            //enable/disable gyroscope

    int read_set_fusion_data();       //read fusion data from file and set it lib
    int get_write_fusion_data();      //get fusion data form lib and write into file
    int read_mag_calibration_data(int *mx, int *my, int *mz);  //read calibrated data from file
    int write_mag_calibration_data(int mx, int my, int mz);    //write calibrated data into file


	void process_events(int fd);

public:
             iNemoSensor(void* context);
    virtual ~iNemoSensor();
    virtual int readEvents(int fd,sensors_event_t* data, int count);
    virtual bool hasPendingEvents() const;
    virtual int setDelay(int32_t handle, int64_t ns);
    virtual int enable(int32_t handle, int enabled);

};

/*****************************************************************************/

#endif  // ANDROID_INEMO_SENSOR_H
