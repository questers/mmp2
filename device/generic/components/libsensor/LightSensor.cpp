/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <math.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/select.h>
#include <cutils/log.h>

#include "LightSensor.h"

/*****************************************************************************/

/* The Crespo ADC sends 4 somewhat bogus events after enabling the sensor.
   This becomes a problem if the phone is turned off in bright light
   and turned back on in the dark.
   To avoid this we ignore the first 4 events received after enabling the sensor.
 */
#define FIRST_GOOD_EVENT    1

LightSensor::LightSensor(void* context)
    : SensorBase(context,NULL, NULL),
      mEnabled(0),
      mEventsSinceEnable(0),
      mInputReader(1),
      mHasPendingEvent(false)
{
    mPendingEvent.version = sizeof(sensors_event_t);
    mPendingEvent.sensor = ID_L;
    mPendingEvent.type = SENSOR_TYPE_LIGHT;
    memset(mPendingEvent.data, 0, sizeof(mPendingEvent.data));

    if (data_fd) {
        strcpy(input_sysfs_path, "/sys/bus/platform/devices/skm/");
        input_sysfs_path_len = strlen(input_sysfs_path);
        enable(0, 1);
    }
}

LightSensor::~LightSensor() {
    if (mEnabled) {
        enable(0, 0);
    }
}

int LightSensor::setDelay(int32_t handle, int64_t ns)
{
    int fd;
    strcpy(&input_sysfs_path[input_sysfs_path_len], "delayms");
    fd = open(input_sysfs_path, O_RDWR);
    if (fd >= 0) {
        char buf[80];
        sprintf(buf, "16 %lld", ns/1000000);
        write(fd, buf, strlen(buf)+1);
        close(fd);
        return 0;
    }
    return -1;
}

int LightSensor::enable(int32_t handle, int en)
{
    if (en != mEnabled ) {
        mEnabled = en;
        if(!en&&(data_fd>=0))
        {
            remFd(context,data_fd);
            close(data_fd);
            data_fd = -1;
        }
        else if(data_fd<0)
        {
            data_fd = openInput("lightsensor");
            if(data_fd>=0)
                addFd(context,this,data_fd);            
            else
            {
                LOGW("fail to open sensor lightsensor");
            }
        }

        char buf[4];
        sprintf(buf, "%d", en);
        strcpy(&input_sysfs_path[input_sysfs_path_len], "lightsensor");
        int fd = open(input_sysfs_path, O_RDWR);
        write(fd, buf, 1);
        close(fd);

    }
    return 0;

}

bool LightSensor::hasPendingEvents() const {
    return mHasPendingEvent;
}

int LightSensor::readEvents(int fd,sensors_event_t* data, int count)
{
    if(SensorBase::debug_level>0)
    {
        LOGD("%s", __func__);
    }

    if (count < 1)
        return -EINVAL;

    if (mHasPendingEvent) {
        mHasPendingEvent = false;
        mPendingEvent.timestamp = getTimestamp();
        *data = mPendingEvent;
        return mEnabled ? 1 : 0;
    }

    ssize_t n = mInputReader.fill(data_fd);
    if (n < 0)
        return n;

    int numEventReceived = 0;
    input_event const* event;

    while (count && mInputReader.readEvent(&event)) {
        int type = event->type;
        if (type == EV_ABS) {
            if (event->code == EVENT_TYPE_LIGHT) {
                mPendingEvent.light = indexToValue(event->value);
                if (mEventsSinceEnable < FIRST_GOOD_EVENT)
                    mEventsSinceEnable++;
            }
        } else if (type == EV_SYN) {
            mPendingEvent.timestamp = timevalToNano(event->time);
            if (mEnabled && (fabs(mPendingEvent.light-mPreviousLight)>5)&&
                    mEventsSinceEnable >= FIRST_GOOD_EVENT) {
                *data++ = mPendingEvent;
                count--;
                numEventReceived++;
                mPreviousLight = mPendingEvent.light;
            }
        } else {
            LOGE("LightSensor: unknown event (type=%d, code=%d)",
                    type, event->code);
        }
        mInputReader.next();
    }

    return numEventReceived;
}

float LightSensor::indexToValue(size_t index) const
{
    #define LIGHT_SCALE 0.125
    return (float)(index*LIGHT_SCALE);//lux
}
