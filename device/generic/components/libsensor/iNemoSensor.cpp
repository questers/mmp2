/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/select.h>
#include<sys/types.h> 
#include<sys/stat.h> 
#include <cutils/log.h>
#include <cutils/properties.h>
#include "iNemoSensor.h"

#include "MEMSAlgLib_Fusion.h"
#include "MEMSAlgLib_eCompass.h"

#define FETCH_FULL_EVENT_BEFORE_RETURN 0
#define IGNORE_EVENT_TIME 350000000
/*****************************************************************************/

iNemoSensor::iNemoSensor(void* context):SensorBase(context,NULL, NULL),
mGyroEnabled(0),
mAccEnabled(0),
mMagEnabled(0),
mOrEnabled(0),
mGrEnabled(0),
mLAEnabled(0),
mRVEnabled(0),
mPendingMask(0),
mHasPendingEvent(false),
mInputReader(4),
mAccDataFd(-1),
mMagDataFd(-1),
mGyrDataFd(-1)
{
	LOGV("%s", __FUNCTION__);

    //pendingEvent initialize
    memset(mPendingEvents, 0, sizeof(mPendingEvents));

    mPendingEvents[Gy].version = sizeof(sensors_event_t);
    mPendingEvents[Gy].sensor = ID_GY;
    mPendingEvents[Gy].type = SENSOR_TYPE_GYROSCOPE;
    mPendingEvents[Gy].acceleration.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[Acc].version = sizeof(sensors_event_t);
    mPendingEvents[Acc].sensor = ID_A;
    mPendingEvents[Acc].type = SENSOR_TYPE_ACCELEROMETER;
    mPendingEvents[Acc].acceleration.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[Mag].version = sizeof(sensors_event_t);
    mPendingEvents[Mag].sensor = ID_M;
    mPendingEvents[Mag].type = SENSOR_TYPE_MAGNETIC_FIELD;
    mPendingEvents[Mag].acceleration.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[Or].version = sizeof(sensors_event_t);
    mPendingEvents[Or].sensor = ID_O;
    mPendingEvents[Or].type = SENSOR_TYPE_ORIENTATION;
    mPendingEvents[Or].acceleration.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[Gr].version = sizeof(sensors_event_t);
    mPendingEvents[Gr].sensor = ID_GR;
    mPendingEvents[Gr].type = SENSOR_TYPE_GRAVITY;
    mPendingEvents[Gr].acceleration.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[LA].version = sizeof(sensors_event_t);
    mPendingEvents[LA].sensor = ID_LA;
    mPendingEvents[LA].type = SENSOR_TYPE_LINEAR_ACCELERATION;
    mPendingEvents[LA].acceleration.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[RV].version = sizeof(sensors_event_t);
    mPendingEvents[RV].sensor = ID_RV;
    mPendingEvents[RV].type = SENSOR_TYPE_ROTATION_VECTOR;
    mPendingEvents[RV].acceleration.status = SENSOR_STATUS_ACCURACY_HIGH;

    strcpy(input_sysfs_path, "/sys/bus/platform/devices/skm/");
    input_sysfs_path_len = strlen(input_sysfs_path);

    
    char property[PROPERTY_VALUE_MAX];
    property_get("persist.sensors.nemo",property,"1");
    mNemoEnabled=(atoi(property)>0)?true:false;

    if(mNemoEnabled)
    {
        //compass calibration
        MEMSAlgLib_eCompass_Init(1000,1000);
        MEMSAlgLib_eCompass_enable_evo();

        //use the last calibrated data, if it exists
        int mx = 0, my = 0, mz = 0;
        read_mag_calibration_data(&mx, &my, &mz);
        MEMSAlgLib_eCompass_SetCalibration(mx, my, mz);

        //use the last fusion data, if it exists
        if(0!=read_set_fusion_data())
        {
            LOGD("read_set_fusion_data() failed!");
        }
    }
}

iNemoSensor::~iNemoSensor() {
	LOGV("%s", __FUNCTION__);

    //record teh fusion data in file
    if(0!=get_write_fusion_data())
    {
        LOGD("get_write_fusion_data() failed!");
    }

    accEnable(0);
    magEnable(0);
    gyroEnable(0);
    
    if(mAccDataFd>=0)
        close(mAccDataFd);
    if(mMagDataFd>=0)
        close(mMagDataFd);
    if(mGyrDataFd>=0)
        close(mGyrDataFd);

}



//read mag calibration data in /data/system/sensorscalibr.dat
int iNemoSensor::read_mag_calibration_data(int *mx, int *my, int *mz)
{
	LOGV("%s", __FUNCTION__);

    FILE * fd = fopen("/data/system/sensorscalibr.dat", "r");

    if (fd != NULL) {
        if(fscanf(fd, "%d\n%d\n%d\n",mx, my, mz)==3)
        LOGD("%s: valid calibration data, %d %d %d", __func__, *mx, *my, *mz);
        fclose(fd);
        return 0;
    }

    return -1;
}
//write mag calibration data in /data/system/sensorscalibr.dat
int iNemoSensor::write_mag_calibration_data(int mx, int my, int mz)
{
	LOGV("%s", __FUNCTION__);

    FILE * fd = fopen("/data/system/sensorscalibr.dat", "w+");

    if(fd != NULL) {
        fprintf(fd, "%d\n%d\n%d\n", mx, my, mz);
        fclose(fd);
        LOGD("%s: valid calibration data, %d %d %d", __func__, mx, my, mz);
    }

    return 0;
}
//read from "/data/system/sensorsfusion.dat" and set to fusion lib
int iNemoSensor::read_set_fusion_data()
{
    int rtn=-1;
    int fd = open("/data/system/sensorsfusion.dat", O_RDWR,S_IRUSR|S_IWUSR);
    if (fd >=  0) {
        int len = MEMSAlgLib_Fusion_GetStoreValuesLength();
        unsigned char* buffer = new unsigned char[len+4];
        int l = -1;
        if(buffer)
        {
            if((len+4)==read(fd, buffer, len+4)){
                l = *((int*)buffer);
                if(l==len){
                    MEMSAlgLib_Fusion_SetStoreValues(buffer+4);
                    rtn = 0;
                }
            }
            delete [] buffer;
        }
        close(fd);
    }

    return -1;
}
//get data from fusion lib and write to "/data/system/sensorsfusion.dat"
int iNemoSensor::get_write_fusion_data()
{
     int rtn=-1;
    int fd = open("/data/system/sensorsfusion.dat", O_RDWR|O_CREAT,S_IRUSR|S_IWUSR);

    if(fd >= 0) {
        int len = MEMSAlgLib_Fusion_GetStoreValuesLength();
        unsigned char * buffer = new unsigned char[len+4];

        if(buffer)
        {
            *((int*)buffer) = len;
            MEMSAlgLib_Fusion_GetStoreValues(buffer+4);
            int l = write(fd, buffer, len+4);

            if((len+4)==l){
                rtn = 0;
            }
            delete [] buffer;
        }
        close(fd);
    }

    return rtn;
}
//enable/disable the sensors

int iNemoSensor::enable(int32_t handle, int en)
{

    int gyroEnabled=mGyroEnabled;
    int accEnabled=mAccEnabled;
    int magEnabled=mMagEnabled;
    if(handle==ID_GY)
        gyroEnabled = en;
    if(handle==ID_A)
        accEnabled = en;
    if(handle==ID_M)
        magEnabled = en;
    if(handle==ID_O)
        mOrEnabled = en;
    if(handle==ID_GR)
        mGrEnabled = en;
    if(handle==ID_LA)
        mLAEnabled = en;
    if(handle==ID_RV)
        mRVEnabled = en;

    int gyroflag=1;
    if(!gyroEnabled && !mGrEnabled && !mLAEnabled && !mRVEnabled)
        gyroflag=0;

    int accflag=1;
    if(!accEnabled && !mOrEnabled && !mGrEnabled && !mLAEnabled && !mRVEnabled)
        accflag=0;

    int magflag=1;
    if(!magEnabled && !mOrEnabled && !mGrEnabled && !mLAEnabled && !mRVEnabled)
        magflag=0;

    if(!mGrEnabled && !mLAEnabled && !mRVEnabled){
        if(0!=get_write_fusion_data())
        LOGD("get_write_fusion_data() failed!");
    }

    LOGD("%s mGyroEnabled[%d],mAccEnabled[%d],mMagEnabled[%d]", __func__,mGyroEnabled,mAccEnabled,mMagEnabled);
    LOGD("%s mGyrDataFd[%d],mAccDataFd[%d],mMagDataFd[%d]", __func__,mGyrDataFd,mAccDataFd,mMagDataFd);

    LOGD("%s gyroEnabled[%d],accEnabled[%d],magEnabled[%d]", __func__,gyroEnabled,accEnabled,magEnabled);
    LOGD("%s gyroflag[%d],accflag[%d],magflag[%d]", __func__,gyroflag,accflag,magflag);


    if( 0==gyroEnable(gyroflag) &&
        0==accEnable(accflag) &&
        0==magEnable(magflag) ){
        LOGD("iNemo enabled");
    }

    return 0;
}
int iNemoSensor::accEnable(int en)
{
    if(en!=mAccEnabled){
        mAccEnabled = en;
        if(!en&&(mAccDataFd>=0))
        {
            remFd(context,mAccDataFd);            
            close(mAccDataFd);
            mAccDataFd = -1;
        }
        else if(mAccDataFd<0)
        {
            mAccDataFd = openInput("accelerameter");
            if(mAccDataFd>=0)
                addFd(context,this,mAccDataFd);  
            else
            {
                LOGW("fail to open sensor accelerameter");
            }
        }

        char buf[4];
        sprintf(buf, "%d", en);
        strcpy(&input_sysfs_path[input_sysfs_path_len], "accelerameter");
        int fd = open(input_sysfs_path, O_RDWR);
        write(fd, buf, 1);
        close(fd);
        
    }
    return 0;
}
int iNemoSensor::magEnable(int en)
{
    if (en != mMagEnabled ) {
        mMagEnabled = en;
        if(!en&&(mMagDataFd>=0))
        {
            remFd(context,mMagDataFd);            
            close(mMagDataFd);
            mMagDataFd = -1;
        }
        else if(mMagDataFd<0)
        {
            mMagDataFd = openInput("magnetometer");
            if(mMagDataFd>=0)
                addFd(context,this,mMagDataFd);            
            else
            {
                LOGW("fail to open sensor magnetometer");
            }
        }

        char buf[4];
        sprintf(buf, "%d", en);
        strcpy(&input_sysfs_path[input_sysfs_path_len], "magnetometer");
        int fd = open(input_sysfs_path, O_RDWR);
        write(fd, buf, 1);
        close(fd);

    }
    return 0;
}
int iNemoSensor::gyroEnable(int en)
{
    if (en != mGyroEnabled) {
        mGyroEnabled = en;
        if(!en&&(mGyrDataFd>=0))
        {
            remFd(context,mGyrDataFd);            
            close(mGyrDataFd);
            mGyrDataFd = -1;
        }
        else if(mGyrDataFd<0)
        {
            mGyrDataFd = openInput("gyroscope");
            if(mGyrDataFd>=0)
                addFd(context,this,mGyrDataFd);    
            else
            {
                LOGW("fail to open sensor gyroscope");
            }
            
        }

        char buf[4];
        sprintf(buf, "%d", en);
        strcpy(&input_sysfs_path[input_sysfs_path_len], "gyroscope");
        int fd = open(input_sysfs_path, O_RDWR);
        write(fd, buf, 1);
        close(fd);

    }
    return 0;
}
bool iNemoSensor::hasPendingEvents() const
{
    return mHasPendingEvent;
}
//set sensors poll delay
int iNemoSensor::setDelay(int32_t handle, int64_t delay_ns)
{
	LOGV("%s", __FUNCTION__);

    //LOGD("iNemoSensor::setDelay(handle=%d, ns=%d)", handle, delay_ns);
    int delay_ms = delay_ns / 1000000; //ms
    char buf[16];
    int fd, ret_val;
    int id; /* id used in kernel */

    if(handle==ID_A||handle==ID_O||handle==ID_RV||handle==ID_LA||handle==ID_GR){
        id = 0;  /* acc */
    }
    if(handle==ID_M||handle==ID_O||handle==ID_RV||handle==ID_LA||handle==ID_GR){
        id = 1;  /* mag */
    }
    if(handle==ID_GY||handle==ID_RV||handle==ID_LA||handle==ID_GR){
        id = 3;  /* gyro */
    }
    else {
        id = -1;
    }

    sprintf(buf,"%d %d",id, delay_ms); //
    strcpy(&input_sysfs_path[input_sysfs_path_len], "delayms");
    fd = open(input_sysfs_path, O_RDWR);
    ret_val = write(fd,buf,strlen(buf));
    close(fd);
    
    return 0;
}
//poll sensors data
int iNemoSensor::readEvents(int fd,sensors_event_t* data, int count)
{
    if(SensorBase::debug_level>3)
    {
        LOGD("%s", __func__);
    }

    if (count < 1)
        return -EINVAL;

    int numEventReceived = 0;

    #if 0
    input_event const* event;
    static short accData[3];
    static short magData[3];
    static short gyroData[3];

    if (!mHasPendingEvent) {
        if (mGyroEnabled)
        {
            int result= getGyroData(gyroData);
            if(!result)
            {
                filtGyroData(gyroData);
                mPendingEvents[Gy].gyro.x = gyroData[0] * CONVERT_GYRO_X;
                mPendingEvents[Gy].gyro.y = gyroData[1] * CONVERT_GYRO_Y;
                mPendingEvents[Gy].gyro.z = gyroData[2] * CONVERT_GYRO_Z;
                mPendingMask |= 1<<Gy;            
            }
        }
        
        //get accelerometer data
        if(mAccEnabled){
            int result=getAccData(accData);
            if(!result)
            {
                mPendingEvents[Acc].acceleration.x = accData[0]*CONVERT_A_X;
                mPendingEvents[Acc].acceleration.y = accData[1]*CONVERT_A_Y;
                mPendingEvents[Acc].acceleration.z = accData[2]*CONVERT_A_Z;
                mPendingMask |= 1<<Acc;            
            }
        }

        //get magnetometer data, and calibrate it
        signed short MAGOFFX1,MAGOFFY1,MAGOFFZ1, MAGOFFX2,MAGOFFY2,MAGOFFZ2;
        static ECOM_ValueTypeDef calib_val;
        if (mMagEnabled || mOrEnabled || mGrEnabled || mLAEnabled || mRVEnabled) {
            int result=getMagData(magData);
            //get calibrated data of last time
            MEMSAlgLib_eCompass_GetCalibration(&MAGOFFX1,&MAGOFFY1,&MAGOFFZ1);

            calib_val.ax = accData[0];
            calib_val.ay = accData[1];
            calib_val.az = accData[2];
            calib_val.mx = magData[0];
            calib_val.my = magData[1];
            calib_val.mz = magData[2];
            calib_val.time = getTimestamp()/1000000;

            //LOGD("MagOffX %d  MagOffY %d  MagOffZ %d  cali.mx %d  cali.my %d  cali.mz %d  cali.t %d",
            //      MAGOFFX1, MAGOFFY1, MAGOFFZ1, calib_val.mx, calib_val.my, calib_val.mz, calib_val.time);

            //update the calibrated data
            MEMSAlgLib_eCompass_Update(&calib_val);
            if (MEMSAlgLib_eCompass_IsCalibrated()) {//if the data has been calibrated sucessfully
                magData[0] -=  MAGOFFX1;
                magData[1] -=  MAGOFFY1;
                magData[2] -=  MAGOFFZ1;

                if(mMagEnabled){
                    mPendingEvents[Mag].magnetic.x = magData[0]/10;    /* mG to uT */
                    mPendingEvents[Mag].magnetic.y = magData[1]/10;
                    mPendingEvents[Mag].magnetic.z = magData[2]/10;
                    mPendingMask |= 1<<Mag;
#if 0                    
                    LOGI("MAG: (%f, %f, %f)", 
                        mPendingEvents[Mag].magnetic.x, 
                        mPendingEvents[Mag].magnetic.y, 
                        mPendingEvents[Mag].magnetic.z);
#endif
                }
                if(mOrEnabled){
                    mPendingEvents[Or].magnetic.x = MEMSAlgLib_eCompass_Get_Azimuth();
                    mPendingEvents[Or].magnetic.y = MEMSAlgLib_eCompass_Get_Pitch();
                    mPendingEvents[Or].magnetic.z = MEMSAlgLib_eCompass_Get_Roll();
                    mPendingMask |= 1<<Or;

#if 0
                    LOGI("ORI: (%f, %f, %f)", 
                        mPendingEvents[Or].magnetic.x, 
                        mPendingEvents[Or].magnetic.y, 
                        mPendingEvents[Or].magnetic.z);
#endif
                }
                //get calibrated data again, and record into file if changed
                MEMSAlgLib_eCompass_GetCalibration(&MAGOFFX2,&MAGOFFY2,&MAGOFFZ2);
                if( (MAGOFFX1!=MAGOFFX2) || (MAGOFFY1!=MAGOFFY2) || (MAGOFFZ1!=MAGOFFZ2) )
                {
                    LOGI("Write calibration data...");
                    write_mag_calibration_data(MAGOFFX2, MAGOFFY2, MAGOFFZ2);
                }
                else
                {
                    //LOGI("No calibration data written.");
                }
            }
            else {
                LOGI("Sensor not calibrated");
            }
        }
        
        //fusion of 9-axis data
        if(mGrEnabled || mLAEnabled || mRVEnabled)
        {
            NineAxisTypeDef nineInput;
            memset(&nineInput, 0, sizeof(nineInput));

            nineInput.ax =  (long)(accData[0]);
            nineInput.ay =  (long)(accData[1]);
            nineInput.az =  (long)(accData[2]);

            nineInput.mx =  (long)(magData[0]);
            nineInput.my =  (long)(magData[1]);
            nineInput.mz =  (long)(magData[2]);

            nineInput.gx =  (long)(mPendingEvents[Gy].gyro.x/CONVERT_GYRO_X);
            nineInput.gy =  (long)(mPendingEvents[Gy].gyro.y/CONVERT_GYRO_Y);
            nineInput.gz =  (long)(mPendingEvents[Gy].gyro.z/CONVERT_GYRO_Z);

            FusionTypeDef fusionData;

            nineInput.time = getTimestamp()/1000000;
            //update the fusion data
            fusionData = MEMSAlgLib_Fusion_Update(nineInput);

#if 1         //this is important log for 9-axis fusion
            {
                long vs,vr,va,vg,vt;
                vs = MEMSAlgLib_Get(2);
                vr = MEMSAlgLib_Get(10);
                va = MEMSAlgLib_Get(11);
                vg = MEMSAlgLib_Get(12);
                vt = MEMSAlgLib_Get(13);
#if 0                
                //LOGD("off_x %ld off_y %ld off_z %ld", MAGOFFX1,MAGOFFY1,MAGOFFZ1);
                LOGD("9x: t %ld ax %ld ay %ld az %ld mx %ld my %ld mz %ld gx %ld gy %ld gz %ld vs %f vr %f va %f vg %f vt %f",nineInput.time,
                      nineInput.ax, nineInput.ay, nineInput.az, nineInput.mx, nineInput.my, nineInput.mz, nineInput.gx, nineInput.gy, nineInput.gz,
                      *(float*)(&vs),*(float*)(&vr),*(float*)(&va),*(float*)(&vg),*(float*)(&vt));
                LOGD("9x: t %ld ax %ld ay %ld az %ld mx %ld my %ld mz %ld gx %ld gy %ld gz %ld",nineInput.time,
                      nineInput.ax, nineInput.ay, nineInput.az, nineInput.mx, nineInput.my, nineInput.mz, nineInput.gx, nineInput.gy, nineInput.gz);
                LOGD("%5ld, %5ld, %5ld, %5ld, %5ld, %5ld, %5ld, %5ld, %5ld, %5ld", nineInput.time,
                    nineInput.ax, nineInput.ay, nineInput.az, 
                    nineInput.mx, nineInput.my, nineInput.mz, 
                    nineInput.gx, nineInput.gy, nineInput.gz);
#endif
            }
#endif
            if(mRVEnabled) {
                mPendingEvents[RV].data[0] = fusionData.q.b;
                mPendingEvents[RV].data[1] = fusionData.q.c;
                mPendingEvents[RV].data[2] = fusionData.q.d;
                mPendingEvents[RV].data[3] = fusionData.q.a;
                mPendingMask |= 1<<RV;
            }
            if(mLAEnabled) {
                mPendingEvents[LA].data[0] = fusionData.l.x;
                mPendingEvents[LA].data[1] = fusionData.l.y;
                mPendingEvents[LA].data[2] = fusionData.l.z;
                mPendingMask |= 1<<LA;
            }
            if(mGrEnabled) {
                mPendingEvents[Gr].data[0] = fusionData.g.x;
                mPendingEvents[Gr].data[1] = fusionData.g.y;
                mPendingEvents[Gr].data[2] = fusionData.g.z;
                mPendingMask |= 1<<Gr;
            }
        }
    }
    #endif
    if (!mHasPendingEvent)
    {
        process_events(fd);
    }
    int64_t time = getTimestamp();//timevalToNano(event->time);
    for (int j=0 ; count && mPendingMask && j<numSensors ; j++) {
        if (mPendingMask & (1<<j)) {
            mPendingMask &= ~(1<<j);
            mPendingEvents[j].timestamp = time;
    
            *data++ = mPendingEvents[j];
            count--;
            numEventReceived++;
        }
    }
    
    mHasPendingEvent = mPendingMask?true:false;
    
    return numEventReceived;
}
//get accelerometer data from input device
//you can override this function according you driver
int iNemoSensor::getAccData(short * accData)
{
	LOGV("%s", __FUNCTION__);

    struct input_event acc_event;
    int count = 0;
    int ret = -1;

    while(1) {
        count++;
        if(count==30) break;

        ret = read(mAccDataFd, &acc_event, sizeof(struct input_event));
        if (ret != sizeof(struct input_event)){
            usleep(1);
            continue;
        }

        if(acc_event.type == EV_ABS) {
            if(acc_event.code == ABS_X){
                accData[0] = acc_event.value;
            }
            if(acc_event.code == ABS_Y){
                accData[1] = acc_event.value;
            }
            if(acc_event.code == ABS_Z){
                accData[2] = acc_event.value;
            }
        }
        if(acc_event.type == EV_SYN) {
            return 0;
        }
    }

    return -1;
}
//get mamnetometer data from input device
//you can override this function according you driver
int iNemoSensor::getMagData(short * magData)
{
	LOGV("%s", __FUNCTION__);

    struct input_event mag_event;
    int count = 0;
    int ret = -1;

    while(1) {
        count++;
        if(count==30) break;

        ret = read(mMagDataFd, &mag_event, sizeof(struct input_event));
        if (ret != sizeof(struct input_event)){
            usleep(1);
            continue;
        }

        if(mag_event.type == EV_ABS) {
            if(mag_event.code == ABS_X){
                magData[0] = mag_event.value;
            }
            if(mag_event.code == ABS_Y){
                magData[1] = mag_event.value;
            }
            if(mag_event.code == ABS_Z){
                magData[2] = mag_event.value;
            }
        }
        if(mag_event.type == EV_SYN) {
            return 0;
        }
    }

    return -1;
}

int iNemoSensor::getGyroData(short * gyroData)
{
	LOGV("%s", __FUNCTION__);

    struct input_event gyro_event;
    int count = 0;
    int ret = -1;

    while(1) {
        count++;
        if(count==30) break;

        ret = read(mGyrDataFd, &gyro_event, sizeof(struct input_event));
        if (ret != sizeof(struct input_event)){
            usleep(1);
            continue;
        }

        if(gyro_event.type == EV_ABS) {
            if(gyro_event.code == ABS_X){
                gyroData[0] = gyro_event.value;
            }
            if(gyro_event.code == ABS_Y){
                gyroData[1] = gyro_event.value;
            }
            if(gyro_event.code == ABS_Z){
                gyroData[2] = gyro_event.value;
            }
        }
        if(gyro_event.type == EV_SYN) {
            return 0;
        }
    }

    return -1;
}

/* a simple variance filter */
void iNemoSensor::filtGyroData(short * gyroData)
{
    static int NA = -65535;
    static int lx = NA; /* last x/y/z*/
    static int ly = NA;
    static int lz = NA;
    static int lv = NA; /* last valid */
    static int lvx = NA; /* last valid x/y/z */
    static int lvy = NA;
    static int lvz = NA;
    static int TH = 200; /* distance threshold */

    int x = gyroData[0];
    int y = gyroData[1];
    int z = gyroData[2];
    int v;

    if (x == NA &&
        y == NA &&
        z == NA)
    {
        //printf("%5d,%5d,%5d\n", x, y, z);
        v = 1;
    }
    else
    {
        int d = sqrt((x-lvx)*(x-lvx)+(y-lvy)*(y-lvy)+(z-lvz)*(z-lvz));
        v = d < TH;

        if (v)
        {
            //printf("%5d%5d%5d\n", x, y, z);
            lvx = x;
            lvy = y;
            lvz = z;
        }
        else
        {
            if (!lv)
            {
                //printf("%5d%5d%5d\n", lx, ly, lz);
                //printf("%5d%5d%5d\n", x, y, z);
                lvx = x;
                lvy = y;
                lvz = z;
            }
            else
            {
                /* fill with last */
                //LOGI("gyro: discard (%5d,%5d,%5d), dist: %d", gyroData[0], gyroData[1], gyroData[2], d);
                gyroData[0] = lx;
                gyroData[1] = ly;
                gyroData[2] = lz;
            }
        }
    }

    lx = x;
    ly = y;
    lz = z;
    lv = v;
}


void iNemoSensor::process_events(int fd)
{
    if(SensorBase::debug_level>1)
    {
        LOGD("%s fd=%d\n",__FUNCTION__,fd);
    }

    if(fd==mAccDataFd)
    {
        if(SensorBase::debug_level>1) LOGD("%s fd=%d(acc)\n",__FUNCTION__,fd);
        
        int result=getAccData(accData);
        if(!result&&mAccEnabled)
        {
            mPendingEvents[Acc].acceleration.x = accData[0]*CONVERT_A_X;
            mPendingEvents[Acc].acceleration.y = accData[1]*CONVERT_A_Y;
            mPendingEvents[Acc].acceleration.z = accData[2]*CONVERT_A_Z;
            mPendingMask |= 1<<Acc;            
        }
    }
    else if(fd==mGyrDataFd)
    {        
        if(SensorBase::debug_level>1) LOGD("%s fd=%d(gyr)\n",__FUNCTION__,fd);

        int result= getGyroData(gyroData);
        if(!result&&mGyroEnabled)
        {
            filtGyroData(gyroData);
            mPendingEvents[Gy].gyro.x = gyroData[0] * CONVERT_GYRO_X;
            mPendingEvents[Gy].gyro.y = gyroData[1] * CONVERT_GYRO_Y;
            mPendingEvents[Gy].gyro.z = gyroData[2] * CONVERT_GYRO_Z;
            mPendingMask |= 1<<Gy;            
        }
    }
    else if(fd==mMagDataFd)
    {
        if(SensorBase::debug_level>1) LOGD("%s fd=%d(mag)\n",__FUNCTION__,fd);
        
        if(mNemoEnabled)
        {
            if (mMagEnabled || mOrEnabled || mGrEnabled || mLAEnabled || mRVEnabled) 
            {
                //get magnetometer data, and calibrate it
                signed short MAGOFFX1,MAGOFFY1,MAGOFFZ1, MAGOFFX2,MAGOFFY2,MAGOFFZ2;
                int result=getMagData(magData);
                //get calibrated data of last time
                MEMSAlgLib_eCompass_GetCalibration(&MAGOFFX1,&MAGOFFY1,&MAGOFFZ1);

                calib_val.ax = accData[0];
                calib_val.ay = accData[1];
                calib_val.az = accData[2];
                calib_val.mx = magData[0];
                calib_val.my = magData[1];
                calib_val.mz = magData[2];
                calib_val.time = getTimestamp()/1000000;

                //LOGD("MagOffX %d  MagOffY %d  MagOffZ %d  cali.mx %d  cali.my %d  cali.mz %d  cali.t %d",
                //      MAGOFFX1, MAGOFFY1, MAGOFFZ1, calib_val.mx, calib_val.my, calib_val.mz, calib_val.time);

                //update the calibrated data
                MEMSAlgLib_eCompass_Update(&calib_val);
                if (MEMSAlgLib_eCompass_IsCalibrated()) {//if the data has been calibrated sucessfully
                    magData[0] -=  MAGOFFX1;
                    magData[1] -=  MAGOFFY1;
                    magData[2] -=  MAGOFFZ1;

                    if(mMagEnabled){
                        mPendingEvents[Mag].magnetic.x = magData[0]/10;    /* mG to uT */
                        mPendingEvents[Mag].magnetic.y = magData[1]/10;
                        mPendingEvents[Mag].magnetic.z = magData[2]/10;
                        mPendingMask |= 1<<Mag;
                    }
                    if(mOrEnabled){
                        mPendingEvents[Or].magnetic.x = MEMSAlgLib_eCompass_Get_Azimuth();
                        mPendingEvents[Or].magnetic.y = MEMSAlgLib_eCompass_Get_Pitch();
                        mPendingEvents[Or].magnetic.z = MEMSAlgLib_eCompass_Get_Roll();
                        mPendingMask |= 1<<Or;

                    }
                    //get calibrated data again, and record into file if changed
                    MEMSAlgLib_eCompass_GetCalibration(&MAGOFFX2,&MAGOFFY2,&MAGOFFZ2);
                    if( (MAGOFFX1!=MAGOFFX2) || (MAGOFFY1!=MAGOFFY2) || (MAGOFFZ1!=MAGOFFZ2) )
                    {
                        LOGI("Write calibration data...");
                        write_mag_calibration_data(MAGOFFX2, MAGOFFY2, MAGOFFZ2);
                    }
                }
            }

            //fusion of 9-axis data
            if(mGrEnabled || mLAEnabled || mRVEnabled)
            {
                NineAxisTypeDef nineInput;
                memset(&nineInput, 0, sizeof(nineInput));

                nineInput.time = getTimestamp()/1000000;
                
                nineInput.ax =  (long)(accData[0]);
                nineInput.ay =  (long)(accData[1]);
                nineInput.az =  (long)(accData[2]);

                nineInput.mx =  (long)(magData[0]);
                nineInput.my =  (long)(magData[1]);
                nineInput.mz =  (long)(magData[2]);

                nineInput.gx =  (long)(gyroData[0]);
                nineInput.gy =  (long)(gyroData[1]);
                nineInput.gz =  (long)(gyroData[2]);

                FusionTypeDef fusionData;

                //update the fusion data
                fusionData = MEMSAlgLib_Fusion_Update(nineInput);

                #if 1         //this is important log for 9-axis fusion
                {
                    long vs,vr,va,vg,vt;
                    vs = MEMSAlgLib_Get(2);
                    vr = MEMSAlgLib_Get(10);
                    va = MEMSAlgLib_Get(11);
                    vg = MEMSAlgLib_Get(12);
                    vt = MEMSAlgLib_Get(13);
                }
                #endif
                if(mRVEnabled) {
                    mPendingEvents[RV].data[0] = fusionData.q.b;
                    mPendingEvents[RV].data[1] = fusionData.q.c;
                    mPendingEvents[RV].data[2] = fusionData.q.d;
                    mPendingEvents[RV].data[3] = fusionData.q.a;
                    mPendingMask |= 1<<RV;
                }
                if(mLAEnabled) {
                    mPendingEvents[LA].data[0] = fusionData.l.x;
                    mPendingEvents[LA].data[1] = fusionData.l.y;
                    mPendingEvents[LA].data[2] = fusionData.l.z;
                    mPendingMask |= 1<<LA;
                }
                if(mGrEnabled) {
                    mPendingEvents[Gr].data[0] = fusionData.g.x;
                    mPendingEvents[Gr].data[1] = fusionData.g.y;
                    mPendingEvents[Gr].data[2] = fusionData.g.z;
                    mPendingMask |= 1<<Gr;
                }

                if (debug_level > 0)
                {
                    LOGD("%5ld, %5ld, %5ld, %5ld, %5ld, %5ld, %5ld, %5ld, %5ld, %5ld", nineInput.time,
                        nineInput.ax, nineInput.ay, nineInput.az, 
                        nineInput.mx, nineInput.my, nineInput.mz, 
                        nineInput.gx, nineInput.gy, nineInput.gz);
                }
            }         
        }
        else
        {
            //report mag data directly            
            int result=getMagData(magData);            
            if(!result&&mMagEnabled)
            {
                mPendingEvents[Mag].magnetic.x = magData[0]/10;    /* mG to uT */
                mPendingEvents[Mag].magnetic.y = magData[1]/10;
                mPendingEvents[Mag].magnetic.z = magData[2]/10;
                mPendingMask |= 1<<Mag;                
            }
        }


        
    }
    
}

