/*
Copyright (c) 2010, Marvell International Ltd.
All Rights Reserved.

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_VMETAENC_H264_H__
#define __GST_VMETAENC_H264_H__

#include "gstvmetaenc_config.h"
#include <gst/gst.h>
#include "gstvmetaenc_share.h"


G_BEGIN_DECLS

#define GST_TYPE_VMETAH264ENC	(gst_vmetah264enc_get_type())
#define GST_VMETAH264ENC(obj)	(G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_VMETAH264ENC,Gstvmetah264enc))
#define GST_VMETAH264ENC_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_VMETAH264ENC,Gstvmetah264encClass))
#define GST_IS_VMETAH264ENC(obj)		(G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_VMETAH264ENC))
#define GST_IS_VMETAH264ENC_CLASS(obj)	(G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_VMETAH264ENC))


GType gst_vmetah264enc_get_type(void);

typedef struct _Gstvmetah264enc Gstvmetah264enc;
typedef struct _Gstvmetah264encClass Gstvmetah264encClass;

struct _Gstvmetah264enc
{
	GstElement	element;
	GstPad		*sinkpad;
	GstPad		*srcpad;

	IppGSTVMETAEncShare		baseobj;
};

struct _Gstvmetah264encClass
{
	GstElementClass parent_class;
};

G_END_DECLS

#endif
