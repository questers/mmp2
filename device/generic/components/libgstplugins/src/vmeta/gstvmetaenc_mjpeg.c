/*
Copyright (c) 2010, Marvell International Ltd.
All Rights Reserved.

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstvmetaenc_mjpeg.h"

GST_DEBUG_CATEGORY_STATIC (vmetaenc_mjpeg_debug);
#define GST_CAT_DEFAULT vmetaenc_mjpeg_debug

GST_BOILERPLATE (Gstvmetamjpegenc, gst_vmetamjpegenc, GstElement, GST_TYPE_ELEMENT);

void gst_vmetamjpegenc_echofun(void* mjpeg, GstDebugLevel level, const char* format, ...)
{
	if(G_LIKELY(level >= GST_LEVEL_ERROR && level <= GST_LEVEL_LOG)) {
		char msg[256];
		va_list args;
		msg[sizeof(msg)-1] = '\0';
		va_start(args, format);
		vsnprintf(msg, sizeof(msg)-1, format, args);
		va_end(args);

		switch(level) {
		case GST_LEVEL_ERROR:
			GST_ERROR_OBJECT(mjpeg, msg);
			break;
		case GST_LEVEL_WARNING:
			GST_WARNING_OBJECT(mjpeg, msg);
			break;
		case GST_LEVEL_INFO:
			GST_INFO_OBJECT(mjpeg, msg);
			break;
		case GST_LEVEL_DEBUG:
			GST_DEBUG_OBJECT(mjpeg, msg);
			break;
		case GST_LEVEL_LOG:
			GST_LOG_OBJECT(mjpeg, msg);
			break;
		default:
			break;
		}
	}
	return;
}

static gboolean
gst_vmetamjpegenc_sinkpad_setcaps(GstPad *pad, GstCaps *caps)
{
	Gstvmetamjpegenc* mjpeg = GST_VMETAMJPEGENC(GST_PAD_PARENT(pad));
	return _vmetaencshare_sinkpad_setcaps(pad, caps, &mjpeg->baseobj);
}

static GstFlowReturn
gst_vmetamjpegenc_chain(GstPad *pad, GstBuffer *buf)
{
	Gstvmetamjpegenc* mjpeg = GST_VMETAMJPEGENC(GST_PAD_PARENT(pad));
	int bTerminate = 0, loopRet = 0;
	GstFlowReturn flowRt;
	flowRt = _vmetaencshare_chain(buf, &mjpeg->baseobj, &bTerminate, &loopRet);
	if(bTerminate) {
		GST_ELEMENT_ERROR(mjpeg, STREAM, ENCODE, (NULL), ("IPP GST vMeta encoder plug-in fatal error in _chain, error %d", loopRet));
	}
	return flowRt;
}

static GstStateChangeReturn
gst_vmetamjpegenc_change_state(GstElement *element, GstStateChange transition)
{
	Gstvmetamjpegenc* mjpeg = GST_VMETAMJPEGENC(element);
	return _vmetaencshare_change_state(element, transition, &mjpeg->baseobj, parent_class);
}

static void
gst_vmetamjpegenc_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
	Gstvmetamjpegenc* mjpeg = GST_VMETAMJPEGENC(object);
	_vmetaencshare_set_property(object, prop_id, value, pspec, &mjpeg->baseobj);
	return;
}

static void
gst_vmetamjpegenc_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	Gstvmetamjpegenc* mjpeg = GST_VMETAMJPEGENC(object);
	_vmetaencshare_get_property(object, prop_id, value, pspec, &mjpeg->baseobj);
	return;
}


static void
gst_vmetamjpegenc_finalize(GObject * object)
{
	Gstvmetamjpegenc* mjpeg = (Gstvmetamjpegenc*)object;
	_vmetaencshare_onfinalize(&mjpeg->baseobj);

	G_OBJECT_CLASS(parent_class)->finalize(object);

#if 0
	assist_myecho("Gstvmetamjpegenc instance(%p) is finalized!!!\n", object);
#else
	printf("Gstvmetamjpegenc instance(%p) is finalized!!!\n", object);
#endif
	return;
}


static void
gst_vmetamjpegenc_base_init(gpointer g_klass)
{
	_vmetaencshare_base_init(g_klass, IPP_VIDEO_STRM_FMT_MJPG);
	return;
}

static void
gst_vmetamjpegenc_class_init(GstvmetamjpegencClass* klass)
{
	GObjectClass *gobject_class  = (GObjectClass*) klass;
	GstElementClass *gstelement_class = (GstElementClass*) klass;

	gobject_class->set_property = gst_vmetamjpegenc_set_property;
	gobject_class->get_property = gst_vmetamjpegenc_get_property;

	_vmetaencshare_class_install_property(gobject_class);

	gobject_class->finalize = gst_vmetamjpegenc_finalize;
	gstelement_class->change_state = GST_DEBUG_FUNCPTR(gst_vmetamjpegenc_change_state);

	GST_DEBUG_CATEGORY_INIT(vmetaenc_mjpeg_debug, "vmetaenc_mjpeg", 0, "vMeta Encode mjpeg Element");

	return;
}


static GstFlowReturn
gst_vmetamjpegenc_sinkpad_allocbuf(GstPad *pad, guint64 offset, guint size, GstCaps *caps, GstBuffer **buf)
{
	assist_myechomore("call gst_vmetaenc_sinkpad_allocbuf(), pad(0x%x), offset(%lld), size(%d)\n",(unsigned int)pad, offset, size);
	Gstvmetamjpegenc* mjpeg = GST_VMETAMJPEGENC(GST_PAD_PARENT(pad));
	return _vmetaencshare_sinkpad_allocbuf(pad, offset, size, caps, buf, &mjpeg->baseobj);
}


static gboolean
gst_vmetamjpegenc_sinkpad_event(GstPad* pad, GstEvent* event)
{
	Gstvmetamjpegenc* mjpeg = GST_VMETAMJPEGENC(GST_PAD_PARENT(pad));
	return _vmetaencshare_sinkpad_event(pad, event, &mjpeg->baseobj);
}

static void
gst_vmetamjpegenc_init(Gstvmetamjpegenc* mjpeg, GstvmetamjpegencClass* venc_klass)
{
	GstElementClass *klass = GST_ELEMENT_CLASS (venc_klass);

	mjpeg->sinkpad = gst_pad_new_from_template(gst_element_class_get_pad_template (klass, "sink"), "sink");

	gst_pad_set_setcaps_function(mjpeg->sinkpad, GST_DEBUG_FUNCPTR(gst_vmetamjpegenc_sinkpad_setcaps));
	gst_pad_set_chain_function(mjpeg->sinkpad, GST_DEBUG_FUNCPTR(gst_vmetamjpegenc_chain));
	gst_pad_set_event_function(mjpeg->sinkpad, GST_DEBUG_FUNCPTR(gst_vmetamjpegenc_sinkpad_event));
	gst_pad_set_bufferalloc_function(mjpeg->sinkpad, GST_DEBUG_FUNCPTR(gst_vmetamjpegenc_sinkpad_allocbuf));

	gst_element_add_pad (GST_ELEMENT(mjpeg), mjpeg->sinkpad);

	mjpeg->srcpad = gst_pad_new_from_template(gst_element_class_get_pad_template (klass, "src"), "src");

	gst_element_add_pad(GST_ELEMENT(mjpeg), mjpeg->srcpad);

	_vmetaencshare_init_members(&mjpeg->baseobj, IPP_VIDEO_STRM_FMT_MJPG, gst_vmetamjpegenc_echofun, mjpeg->sinkpad, mjpeg->srcpad, mjpeg);

#if 0
	assist_myecho("Gstvmetamjpegenc instance(%p) is inited!!!\n", mjpeg);
#else
	printf("Gstvmetamjpegenc instance(%p) is inited!!!\n", mjpeg);
#endif
	return;
}

