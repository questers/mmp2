/*
Copyright (c) 2010, Marvell International Ltd.
All Rights Reserved.

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_VMETAENC_SHARE_H__
#define __GST_VMETAENC_SHARE_H__

#include "gstvmetaenc_config.h"
#include <gst/gst.h>

#include "codecVC.h"

G_BEGIN_DECLS

//#define IPPGSTVMETAENC_ECHO
#ifdef IPPGSTVMETAENC_ECHO
#define assist_myecho(...)	printf(__VA_ARGS__)		//C99 style
#else
#define assist_myecho(...)
#endif

//#define IPPGSTVMETAENC_ECHOMORE
#ifdef IPPGSTVMETAENC_ECHOMORE
#define assist_myechomore(...)	printf(__VA_ARGS__)
#else
#define assist_myechomore(...)
#endif

#define IPPPSEUDO_GST_ERROR_OBJECT(shareobj, args...)		shareobj->GSTobj_echo_fun(shareobj->gstinst, GST_LEVEL_ERROR, ##args)	//Because it use callback function and there is sprintf in callback, there is some additional cost to use this echo system. Therefore, it's better not to use it in each frame operation like _chain function.
#define IPPPSEUDO_GST_WARNING_OBJECT(shareobj, args...)		shareobj->GSTobj_echo_fun(shareobj->gstinst, GST_LEVEL_WARNING, ##args)
#define IPPPSEUDO_GST_INFO_OBJECT(shareobj, args...)		shareobj->GSTobj_echo_fun(shareobj->gstinst, GST_LEVEL_INFO, ##args)
#define IPPPSEUDO_GST_DEBUG_OBJECT(shareobj, args...)		shareobj->GSTobj_echo_fun(shareobj->gstinst, GST_LEVEL_DEBUG, ##args)
#define IPPPSEUDO_GST_LOG_OBJECT(shareobj, args...)			shareobj->GSTobj_echo_fun(shareobj->gstinst, GST_LEVEL_LOG, ##args)

typedef void*											RES_HOLD_BY_EXTUSER_WATCHMAN;		//The reason using an individual object instead of the instance of GstVmetaEnc, is gst_buffer_ref/unref is more light calling than gst_object_ref/unref. And sometimes if application developper forget to unref the GstVmetaEnc, RES_HOLD_BY_EXTUSER_WATCHMAN still could be finalized.
#define RES_HOLD_BY_EXTUSER_WATCHMAN_CREATE()			((RES_HOLD_BY_EXTUSER_WATCHMAN)gst_buffer_new())
#define RES_HOLD_BY_EXTUSER_WATCHMAN_REF(watchman)		gst_buffer_ref(watchman)
#define RES_HOLD_BY_EXTUSER_WATCHMAN_UNREF(watchman)	gst_buffer_unref(watchman)

typedef void (*pfun_PSEUDO_GST_XXX_OBJECT)(void*, GstDebugLevel, const char*, ...);

typedef struct{
	void*	gstinst;
	pfun_PSEUDO_GST_XXX_OBJECT GSTobj_echo_fun;
	GstPad*	sinkpad;
	GstPad*	srcpad;

	RES_HOLD_BY_EXTUSER_WATCHMAN	ExtHoldingResWatcher;

	int							driver_init_ret;

	void*						vMetaEnc_Handle;
	IppVmetaEncParSet			vMetaEnc_Par;
	IppVmetaEncInfo				vMetaEnc_Info;

	GSList*						vMetaStmBufRepo;
	GSList*						vMetaFrameBufRepo;


	GQueue						vMetaFrameCandidateQueue;
	GQueue						vMetaStmTSCandidateQueue;

	GstClockTime				AotoIncTimestamp;

	IppVmetaPicture*			FrameBufferForFileSrc;


	int							iEncW;
	int							iEncH;
	int							iEncFrameRate;

	int							i1SrcFrameDataLen;
	int							iWantedStmBufSz;

	guint64						iFixedFRateDuration;

	int							iWholeFrameInputModeFlag;


	//property
	int							iTargtBitRate;
	gint64						ForceStartTimestamp;
	int							bDisableRateControl;
	int							iQP;
	int							bOnlyIFrame;
	int							SupportMultiinstance;
	int							HungryStrategy;

	//for log
	guint64						iEncProducedByteCnt;
	int							iEncProducedCompressedFrameCnt;

	gint64						codec_time;

	gpointer					pProbeData;

}IppGSTVMETAEncShare;

gboolean _vmetaencshare_sinkpad_setcaps(GstPad* pad, GstCaps* caps, IppGSTVMETAEncShare* share);
GstFlowReturn _vmetaencshare_chain(GstBuffer *buf, IppGSTVMETAEncShare* share, int* pbTerminate, int* pLoopRet);
GstStateChangeReturn _vmetaencshare_change_state(GstElement *element, GstStateChange transition, IppGSTVMETAEncShare* share, GstElementClass* father_class);
void _vmetaencshare_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec, IppGSTVMETAEncShare* share);
void _vmetaencshare_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec, IppGSTVMETAEncShare* share);
void _vmetaencshare_onfinalize(IppGSTVMETAEncShare* share);
void _vmetaencshare_base_init(gpointer g_klass, IppVideoStreamFormat eOutputStrmFmt);
void _vmetaencshare_class_install_property(GObjectClass *gobject_class);
void _vmetaencshare_init_members(IppGSTVMETAEncShare* share, IppVideoStreamFormat eOutputStrmFmt, pfun_PSEUDO_GST_XXX_OBJECT echofun, GstPad* sinkpad, GstPad* srcpad, void* inst);
GstFlowReturn _vmetaencshare_sinkpad_allocbuf(GstPad *pad, guint64 offset, guint size, GstCaps *caps, GstBuffer **buf, IppGSTVMETAEncShare* share);
gboolean _vmetaencshare_sinkpad_event(GstPad *pad, GstEvent *event, IppGSTVMETAEncShare* share);

G_END_DECLS

#endif
