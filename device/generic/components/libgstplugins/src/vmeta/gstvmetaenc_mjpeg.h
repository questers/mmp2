/*
Copyright (c) 2010, Marvell International Ltd.
All Rights Reserved.

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_VMETAENC_MJPEG_H__
#define __GST_VMETAENC_MJPEG_H__

#include "gstvmetaenc_config.h"
#include <gst/gst.h>
#include "gstvmetaenc_share.h"


G_BEGIN_DECLS

#define GST_TYPE_VMETAMJPEGENC	(gst_vmetamjpegenc_get_type())
#define GST_VMETAMJPEGENC(obj)	(G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_VMETAMJPEGENC,Gstvmetamjpegenc))
#define GST_VMETAMJPEGENC_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_VMETAMJPEGENC,GstvmetamjpegencClass))
#define GST_IS_VMETAMJPEGENC(obj)		(G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_VMETAMJPEGENC))
#define GST_IS_VMETAMJPEGENC_CLASS(obj)	(G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_VMETAMJPEGENC))


GType gst_vmetamjpegenc_get_type(void);

typedef struct _Gstvmetamjpegenc Gstvmetamjpegenc;
typedef struct _GstvmetamjpegencClass GstvmetamjpegencClass;

struct _Gstvmetamjpegenc
{
	GstElement	element;
	GstPad		*sinkpad;
	GstPad		*srcpad;

	IppGSTVMETAEncShare		baseobj;
};

struct _GstvmetamjpegencClass
{
	GstElementClass parent_class;
};

G_END_DECLS

#endif
