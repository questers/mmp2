/*
Copyright (c) 2010, Marvell International Ltd.
All Rights Reserved.

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_VMETAENC_MPEG4_H__
#define __GST_VMETAENC_MPEG4_H__

#include "gstvmetaenc_config.h"
#include <gst/gst.h>
#include "gstvmetaenc_share.h"


G_BEGIN_DECLS

#define GST_TYPE_VMETAMPEG4ENC	(gst_vmetampeg4enc_get_type())
#define GST_VMETAMPEG4ENC(obj)	(G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_VMETAMPEG4ENC,Gstvmetampeg4enc))
#define GST_VMETAMPEG4ENC_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_VMETAMPEG4ENC,Gstvmetampeg4encClass))
#define GST_IS_VMETAMPEG4ENC(obj)		(G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_VMETAMPEG4ENC))
#define GST_IS_VMETAMPEG4ENC_CLASS(obj)	(G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_VMETAMPEG4ENC))


GType gst_vmetampeg4enc_get_type(void);

typedef struct _Gstvmetampeg4enc Gstvmetampeg4enc;
typedef struct _Gstvmetampeg4encClass Gstvmetampeg4encClass;

struct _Gstvmetampeg4enc
{
	GstElement	element;
	GstPad		*sinkpad;
	GstPad		*srcpad;

	IppGSTVMETAEncShare		baseobj;
};

struct _Gstvmetampeg4encClass
{
	GstElementClass parent_class;
};

G_END_DECLS

#endif
