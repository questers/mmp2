/*
Copyright (c) 2010, Marvell International Ltd.
All Rights Reserved.

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_VMETAENC_CONFIG_H__
#define __GST_VMETAENC_CONFIG_H__

#define GSTVMETAENC_PLATFORM_NOMJPEG			0
#define GSTVMETAENC_PLATFORM_HASMJPEG			1
#define GSTVMETAENC_PLATFORM					GSTVMETAENC_PLATFORM_HASMJPEG

#define ENABLE_IPPGSTPLUGIN_VMETAENC_H264_STANDALONE
#define ENABLE_IPPGSTPLUGIN_VMETAENC_H263_STANDALONE
#define ENABLE_IPPGSTPLUGIN_VMETAENC_MPEG4_STANDALONE

#if GSTVMETAENC_PLATFORM == GSTVMETAENC_PLATFORM_HASMJPEG
#define ENABLE_IPPGSTPLUGIN_VMETAENC_MJPEG_STANDALONE
#endif

#endif
