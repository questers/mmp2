/*
Copyright (c) 2010, Marvell International Ltd.
All Rights Reserved.

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstvmetaenc_config.h"
#include <gst/gst.h>

//#include "gstvmetaenc.h"

#ifdef ENABLE_IPPGSTPLUGIN_VMETAENC_H264_STANDALONE
#include "gstvmetaenc_h264.h"
#endif

#ifdef ENABLE_IPPGSTPLUGIN_VMETAENC_H263_STANDALONE
#include "gstvmetaenc_h263.h"
#endif

#ifdef ENABLE_IPPGSTPLUGIN_VMETAENC_MPEG4_STANDALONE
#include "gstvmetaenc_mpeg4.h"
#endif

#ifdef ENABLE_IPPGSTPLUGIN_VMETAENC_MJPEG_STANDALONE
#include "gstvmetaenc_mjpeg.h"
#endif


static gboolean
plugin_init(GstPlugin *plugin)
{
//	if(!gst_element_register(plugin, "vmetaenc", GST_RANK_MARGINAL, GST_TYPE_VMETAENC)) {
//		return FALSE;
//	}

#ifdef ENABLE_IPPGSTPLUGIN_VMETAENC_H264_STANDALONE
	if(!gst_element_register(plugin, "vmetaenc_h264", GST_RANK_MARGINAL+3, GST_TYPE_VMETAH264ENC)) {
		return FALSE;
	}
#endif

#ifdef ENABLE_IPPGSTPLUGIN_VMETAENC_H263_STANDALONE
	if(!gst_element_register(plugin, "vmetaenc_h263", GST_RANK_MARGINAL+3, GST_TYPE_VMETAH263ENC)) {
		return FALSE;
	}
#endif

#ifdef ENABLE_IPPGSTPLUGIN_VMETAENC_MPEG4_STANDALONE
	if(!gst_element_register(plugin, "vmetaenc_mpeg4", GST_RANK_MARGINAL+3, GST_TYPE_VMETAMPEG4ENC)) {
		return FALSE;
	}
#endif

#ifdef ENABLE_IPPGSTPLUGIN_VMETAENC_MJPEG_STANDALONE
	if(!gst_element_register(plugin, "vmetaenc_mjpeg", GST_RANK_MARGINAL+3, GST_TYPE_VMETAMJPEGENC)) {
		return FALSE;
	}
#endif

	return TRUE;
}

GST_PLUGIN_DEFINE(GST_VERSION_MAJOR,
					GST_VERSION_MINOR,
					"mvl_vmetaenc",
					"vMeta encoder based on IPP vMeta, build date "__DATE__,
					plugin_init,
					VERSION,
					"LGPL",
					"",
					"");

