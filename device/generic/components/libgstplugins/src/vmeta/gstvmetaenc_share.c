/*
Copyright (c) 2010, Marvell International Ltd.
All Rights Reserved.

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include "vdec_os_api.h"
#include "misc.h"
#include "ippGST_sideinfo.h"
#include "gstvmetaenc_share.h"

enum {
	ARG_0,
	/* fill below with user defined arguments */
	ARG_FORCE_STARTTIME,
	ARG_TARGET_BITRATE,
	ARG_DISABLE_RATECTRL,
	ARG_QP,
	ARG_ONLY_I,
	ARG_SUPPORTMULTIINSTANCE,
	ARG_HUNGRYSTRATEGY,
#ifdef DEBUG_PERFORMANCE
	ARG_TOTALFRAME,
	ARG_CODECTIME,
#endif
};

//SUYV isn't an standard fourcc, used by Samsung, it equal to UYVY
static GstStaticPadTemplate sink_factory = \
	GST_STATIC_PAD_TEMPLATE ("sink", GST_PAD_SINK, GST_PAD_ALWAYS, \
	GST_STATIC_CAPS ("video/x-raw-yuv," \
					 "format = (fourcc) { UYVY, SUYV }, " \
					 "framerate = (fraction) [ 1/1, 60/1 ], " \
					 "width = (int) [ 16, 2048 ], " \
					 "height = (int) [ 16, 2048 ]" \
					 ));


static GstStaticPadTemplate src_factory_h264 = \
	GST_STATIC_PAD_TEMPLATE ("src", GST_PAD_SRC, GST_PAD_ALWAYS, \
	GST_STATIC_CAPS ("video/x-h264, width = (int) [ 16, 2048 ], height = (int) [ 16, 2048 ], framerate = (fraction) [ 1/1, 60/1 ]"
	));
static GstStaticPadTemplate src_factory_h263 = \
	GST_STATIC_PAD_TEMPLATE ("src", GST_PAD_SRC, GST_PAD_ALWAYS, \
	GST_STATIC_CAPS ("video/x-h263, variant = (string)itu, width = (int) [ 16, 2048 ], height = (int) [ 16, 2048 ], framerate = (fraction) [ 1/1, 60/1 ]"
	));
static GstStaticPadTemplate src_factory_mpeg4 = \
	GST_STATIC_PAD_TEMPLATE ("src", GST_PAD_SRC, GST_PAD_ALWAYS, \
	GST_STATIC_CAPS ("video/mpeg, mpegversion = (int)4, systemstream = (boolean) FALSE, width = (int) [ 16, 2048 ], height = (int) [ 16, 2048 ], framerate = (fraction) [ 1/1, 60/1 ]"
	));
static GstStaticPadTemplate src_factory_mjpeg = \
	GST_STATIC_PAD_TEMPLATE ("src", GST_PAD_SRC, GST_PAD_ALWAYS, \
	GST_STATIC_CAPS ("image/jpeg, width = (int) [ 16, 2048 ], height = (int) [ 16, 2048 ], framerate = (fraction) [ 1/1, 60/1 ]"
	));

#define IPPGST_DEFAULT_TARGETBITRATE	300000
#define IPPGST_DEFAULT_STEAMBUFSZMIN	(128*1024)
#define IPPGST_ENCODER_DEFAULTQP		30

#define IPPGST_VMETAENC_TS_MAX_CAPACITY		60


typedef struct{
	struct timeval EncAPI_clk0;
	struct timeval EncAPI_clk1;
	int bufdistinguish_cnt0;
	int bufdistinguish_cnt1;
	int bufdistinguish_cnt2;
	int bufdistinguish_cnt3;
}VmetaProbeData;

#define SET_IPPVMETAFRAME_BUSYFLAG(pframe, flag)	(((IppVmetaPicture*)(pframe))->pUsrData0 = (void*)flag)
#define IS_IPPVMETA_FRAME_BUSY(pframe)				(((IppVmetaPicture*)(pframe))->pUsrData0 != (void*)0)
#define IPPVMETAFRAME_ATTCHEDGSTBUF(pframe)			(((IppVmetaPicture*)(pframe))->pUsrData1)
#define IS_IPPVMETAFRAME_NOTFROMREPO(pframe)		(((IppVmetaPicture*)(pframe))->pUsrData2 != (void*)0)

#define IS_IPPVMETA_STM_BUSY(pframe)				(((IppVmetaBitstream*)(pframe))->pUsrData0 != (void*)0)
#define SET_IPPVMETASTM_BUSYFLAG(pstm, flag)		(((IppVmetaBitstream*)(pstm))->pUsrData0 = (void*)flag)

//a subclass from GstBuffer to hook GstBuffer finalize
typedef struct {
	GstBuffer						gstbuf;
	IppVmetaPicture*				pVmetaFrame;
	RES_HOLD_BY_EXTUSER_WATCHMAN	watcher;
}GstVmetaEncShareFrameBuffer;

#define GST_TYPE_VMETAENCSHARE_BUFFER		(gst_vmetaencshare_buffer_get_type())
#define GST_IS_VMETAENCSHARE_BUFFER(obj)	(G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_VMETAENCSHARE_BUFFER))
#define GST_VMETAENCSHARE_BUFFER(obj)		(G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_VMETAENCSHARE_BUFFER, GstVmetaEncShareFrameBuffer))
#define GST_VMETAENCSHARE_BUFFER_CAST(obj)	((GstVmetaEncShareFrameBuffer *)(obj))

static GstMiniObjectClass* vmetaencshare_buffer_parent_class = NULL;

static void
gst_vmetaencshare_buffer_finalize(GstVmetaEncShareFrameBuffer * buf)
{
	SET_IPPVMETAFRAME_BUSYFLAG(buf->pVmetaFrame, 0);
	RES_HOLD_BY_EXTUSER_WATCHMAN_UNREF(buf->watcher);
	vmetaencshare_buffer_parent_class->finalize(GST_MINI_OBJECT_CAST(buf));
	return;
}

static void
gst_vmetaencshare_buffer_class_init(gpointer g_class, gpointer class_data)
{
	GstMiniObjectClass *mini_object_class = GST_MINI_OBJECT_CLASS(g_class);
	vmetaencshare_buffer_parent_class = g_type_class_peek_parent(g_class);
	mini_object_class->finalize = (GstMiniObjectFinalizeFunction)gst_vmetaencshare_buffer_finalize;
	return;
}

static void
gst_vmetaencshare_buffer_init(GTypeInstance * instance, gpointer g_class)
{
	return;
}

GType
gst_vmetaencshare_buffer_get_type(void)
{
	static GType buffer_type;

	if (G_UNLIKELY(buffer_type == 0)) {
		static const GTypeInfo buffer_info = {
			sizeof(GstBufferClass),
			NULL,
			NULL,
			gst_vmetaencshare_buffer_class_init,
			NULL,
			NULL,
			sizeof(GstVmetaEncShareFrameBuffer),
			0,
			gst_vmetaencshare_buffer_init,
			NULL
		};
		buffer_type = g_type_register_static(GST_TYPE_BUFFER, "GstVmetaEncShareFrameBuffer", &buffer_info, 0);
	}
	return buffer_type;
}

static __inline GstVmetaEncShareFrameBuffer *
gst_vMetaEncShareBuffer_new()
{
	return (GstVmetaEncShareFrameBuffer*)gst_mini_object_new(GST_TYPE_VMETAENCSHARE_BUFFER);
}
//end of a subclass from GstBuffer to hook GstBuffer finalize



#define IPPGST_VMETAENC_IS_2ALIGN(x)		(((unsigned int)(x)&1) == 0)
#define IPPGST_VMETAENC_IS_16ALIGN(x)		(((unsigned int)(x)&15) == 0)

static int
IPP_videoCheckPar_vMetaEnc(int w, int h, int FR, int bR)
{
	if(! IPPGST_VMETAENC_IS_16ALIGN(w)) {
		g_warning("src image width %d isn't 16 aligned!", w);
		return -1;
	}
	if(! IPPGST_VMETAENC_IS_2ALIGN(h)) {
		g_warning("src image height %d isn't 2 aligned!", h);
		return -2;
	}
	if(FR <= 0) {
		g_warning("src framerate %d exceeds range, and don't support variable framerate!", FR);
		return -3;
	}
	if(bR <= 0) {
		g_warning("target bitrate %d exceeds range!", bR);
		return -4;
	}
	return 0;
}


static void
IPP_SetEncPar_vMetaEnc(IppGSTVMETAEncShare* share)
{
	assist_myecho("Set vmeta encoder parameter, w %d, h %d, FR %d, bR %d\n", share->iEncW, share->iEncH, share->iEncFrameRate, share->iTargtBitRate);
	share->vMetaEnc_Par.nWidth = share->iEncW;
	share->vMetaEnc_Par.nHeight = share->iEncH;
	share->vMetaEnc_Par.nFrameRateNum = share->iEncFrameRate;
	if(share->bOnlyIFrame == 0) {
		share->vMetaEnc_Par.nPBetweenI = share->iEncFrameRate - 1 > 0 ? share->iEncFrameRate - 1 : 1;
	}else{
		share->vMetaEnc_Par.nPBetweenI = 0;
	}
	share->vMetaEnc_Par.bRCEnable = share->bDisableRateControl == 0 ? 1 : 0;
	share->vMetaEnc_Par.nQP = share->iQP;
	share->vMetaEnc_Par.nRCBitRate = share->iTargtBitRate;
	assist_myecho("vmeta encoder parameter is set to: eInputYUVFmt %d, eOutputStrmFmt %d, nWidth %d, nHeight %d, nFrameRateNum %d, nPBetweenI %d, bRCEnable %d, nQP %d, nRCBitRate %d\n", \
		share->vMetaEnc_Par.eInputYUVFmt, share->vMetaEnc_Par.eOutputStrmFmt, share->vMetaEnc_Par.nWidth, share->vMetaEnc_Par.nHeight, share->vMetaEnc_Par.nFrameRateNum, share->vMetaEnc_Par.nPBetweenI, share->vMetaEnc_Par.bRCEnable, share->vMetaEnc_Par.nQP, share->vMetaEnc_Par.nRCBitRate);
	return;
}

static __inline GstCaps *
IPP_NewCaps(IppVideoStreamFormat eOutputStrmFmt, IppGSTVMETAEncShare* share)
{
	GstCaps* pTmp = NULL;
	if(eOutputStrmFmt == IPP_VIDEO_STRM_FMT_H264) {
		pTmp = gst_caps_new_simple ("video/x-h264",
		"width", G_TYPE_INT, share->iEncW,
		"height", G_TYPE_INT, share->iEncH,
		"framerate", GST_TYPE_FRACTION,  share->iEncFrameRate, 1,
		NULL);
	}else if(eOutputStrmFmt == IPP_VIDEO_STRM_FMT_H263) {
		pTmp = gst_caps_new_simple ("video/x-h263",
			"width", G_TYPE_INT, share->iEncW,
			"height", G_TYPE_INT, share->iEncH,
			"variant", G_TYPE_STRING, "itu",
			"framerate", GST_TYPE_FRACTION,  share->iEncFrameRate, 1,
			NULL);
	}else if(eOutputStrmFmt == IPP_VIDEO_STRM_FMT_MPG4) {
		pTmp = gst_caps_new_simple ("video/mpeg",
			"width", G_TYPE_INT, share->iEncW,
			"height", G_TYPE_INT, share->iEncH,
			"mpegversion", G_TYPE_INT, 4,
			"systemstream", G_TYPE_BOOLEAN, FALSE,
			"framerate", GST_TYPE_FRACTION,  share->iEncFrameRate, 1,
			NULL);
	}else if(eOutputStrmFmt == IPP_VIDEO_STRM_FMT_MJPG) {
		pTmp = gst_caps_new_simple ("image/jpeg",
			"width", G_TYPE_INT, share->iEncW,
			"height", G_TYPE_INT, share->iEncH,
			"framerate", GST_TYPE_FRACTION,  share->iEncFrameRate, 1,
			NULL);
	}
	return pTmp;
}

gboolean _vmetaencshare_sinkpad_setcaps(GstPad* pad, GstCaps* caps, IppGSTVMETAEncShare* share)
{
#ifdef IPPGSTVMETAENC_ECHOMORE
	gchar* capstring = gst_caps_to_string(caps);
	assist_myechomore("caps is %s\n", capstring==NULL? "none" : capstring);
	if(capstring) {
		g_free(capstring);
	}
#endif

	guint stru_num = gst_caps_get_size (caps);
	if(stru_num == 0) {
		g_warning("No content in vmetaenc sinkpad setcaps!");
		return FALSE;
	}

	GstStructure *str = gst_caps_get_structure (caps, 0);

	if (FALSE == gst_structure_get_int(str, "width", &(share->iEncW))) {
		g_warning("Couldn't get src image width in setcaps()!");
		IPPPSEUDO_GST_ERROR_OBJECT(share, "Couldn't get src image width in setcaps()!");
		return FALSE;
	}

	if (FALSE == gst_structure_get_int(str, "height", &(share->iEncH))) {
		g_warning("Couldn't get src image height in setcaps()!");
		IPPPSEUDO_GST_ERROR_OBJECT(share, "Couldn't get src image height in setcaps()!");
		return FALSE;
	}

	int iFRateNum, iFRateDen;
	if (FALSE == gst_structure_get_fraction(str, "framerate", &iFRateNum, &iFRateDen)) {
		g_warning("Couldn't get framerate in setcaps()!");
		IPPPSEUDO_GST_ERROR_OBJECT(share, "Couldn't get framerate in setcaps()!");
		return FALSE;
	}

	if(iFRateDen == 0) {
		g_warning("frame rate info from caps isn't correct, (%d)/(%d)!", iFRateNum, iFRateDen);
		IPPPSEUDO_GST_ERROR_OBJECT(share, "frame rate info from caps isn't correct, (%d)/(%d)!", iFRateNum, iFRateDen);
		return FALSE;
	}
	share->iEncFrameRate = iFRateNum/iFRateDen;
	if(iFRateDen * share->iEncFrameRate != iFRateNum) {	//current vmeta encoder only support interger fps
		g_warning("Couldn't support fractional framerate (%d)/(%d) fps!", iFRateNum, iFRateDen);
		IPPPSEUDO_GST_ERROR_OBJECT(share, "Couldn't support fractional framerate (%d)/(%d) fps!", iFRateNum, iFRateDen);
		return FALSE;
	}
	if(share->iEncFrameRate) {
		share->iFixedFRateDuration = gst_util_uint64_scale_int(GST_SECOND, 1, share->iEncFrameRate);
		assist_myecho("share->iFixedFRateDuration is set %lld\n", share->iFixedFRateDuration);
	}

	if( 0 != IPP_videoCheckPar_vMetaEnc(share->iEncW, share->iEncH, share->iEncFrameRate, share->iTargtBitRate) ) {
		IPPPSEUDO_GST_ERROR_OBJECT(share, "The parameter for encoder is wrong!!!");
		return FALSE;
	}

	GstCaps *TmpCap;
	TmpCap = IPP_NewCaps(share->vMetaEnc_Par.eOutputStrmFmt, share);
	if(!gst_pad_set_caps(share->srcpad, TmpCap)) {
		gst_caps_unref(TmpCap);
		g_warning("Set caps on srcpad fail!");
		IPPPSEUDO_GST_ERROR_OBJECT(share, "Set caps on srcpad fail!");
		return FALSE;
	}
	gst_caps_unref(TmpCap);

	share->i1SrcFrameDataLen = (share->iEncW*share->iEncH)<<1;
	share->vMetaEnc_Info.dis_buf_size = GST_ROUND_UP_16(share->iEncW) * GST_ROUND_UP_16(share->iEncH) << 1;	//share->vMetaEnc_Info.dis_buf_size will be filled when EncodeFrame_Vmeta() return IPP_STATUS_NEED_INPUT, but before that, it probably has been used. Therefore, we calculate it as early as possible, and EncodeFrame_Vmeta() will refresh it.
	if(share->iWantedStmBufSz < ((share->iEncW*share->iEncH)>>2)) {
		share->iWantedStmBufSz = (share->iEncW*share->iEncH)>>2;
	}
	if(share->iWantedStmBufSz*share->iEncFrameRate*8 < (share->iTargtBitRate*3>>1) ) {
		share->iWantedStmBufSz = (3*(share->iTargtBitRate>>3)/share->iEncFrameRate)>>1;
	}

	IPP_SetEncPar_vMetaEnc(share);
	return TRUE;
}

static __inline void
IPP_AdjustStmSz(IppGSTVMETAEncShare* share, int iCurCompressedDataLen)
{
	if(iCurCompressedDataLen > share->iWantedStmBufSz*3>>2) {
		share->iWantedStmBufSz = share->iWantedStmBufSz*5>>2;
	}
	return;
}

static __inline IppVmetaPicture*
IPP_PopFrameFromQueue(IppGSTVMETAEncShare* share)
{
	return (IppVmetaPicture*)g_queue_pop_tail(&share->vMetaFrameCandidateQueue);
}

static __inline void
IPP_PushFrameToQueue(IppGSTVMETAEncShare* share, IppVmetaPicture* pFrame)
{
	g_queue_push_head(&share->vMetaFrameCandidateQueue, pFrame);
	return;
}

static GstClockTime
IPP_PopTSFromQueue(IppGSTVMETAEncShare* share)
{
	GstClockTime* pTs = (GstClockTime*)g_queue_pop_tail(&share->vMetaStmTSCandidateQueue);
	GstClockTime ts;
	if(pTs == NULL) {
		g_warning("No timestamp left in TS queue could be pop!");
		return GST_CLOCK_TIME_NONE;
	}else{
		ts = *pTs;
		g_slice_free(GstClockTime, pTs);
		return ts;
	}
}

static void
IPP_PushTSToQueue(IppGSTVMETAEncShare* share, GstClockTime ts)
{
	GstClockTime* pTs = g_slice_new(GstClockTime);
	*pTs = ts;
	g_queue_push_head(&share->vMetaStmTSCandidateQueue, pTs);

	//remove redundant TS to avoid queue overflow
	if(g_queue_get_length(&share->vMetaStmTSCandidateQueue) > IPPGST_VMETAENC_TS_MAX_CAPACITY) {
		GstClockTime* pRedundantTs = (GstClockTime*)g_queue_pop_tail(&share->vMetaStmTSCandidateQueue);
		g_slice_free(GstClockTime, pRedundantTs);
	}
	return;
}

static void
IPP_IdleFrameBuf(IppVmetaPicture* pFrame)
{
	if(pFrame != NULL) {
		GstBuffer* buf = (GstBuffer*)IPPVMETAFRAME_ATTCHEDGSTBUF(pFrame);
		if(buf != NULL) {
			if(IS_IPPVMETAFRAME_NOTFROMREPO(pFrame)) {
				//this IppVmetaPicture is not from repo
				g_slice_free(IppVmetaPicture, pFrame);
			}
			gst_buffer_unref(buf);
		}else{
			SET_IPPVMETAFRAME_BUSYFLAG(pFrame, 0);
		}
	}
	return;
}

static __inline void
IPP_IdleStmBuf(IppVmetaBitstream* pStm)
{
	if(pStm != NULL) {
		SET_IPPVMETASTM_BUSYFLAG(pStm, 0);
	}
	return;
}

static void
IPP_ClearCandidateQueus(IppGSTVMETAEncShare* share)
{
	assist_myecho("Before clear frame queue, there is %d frames in queue\n", g_queue_get_length(&share->vMetaFrameCandidateQueue));
	IppVmetaPicture* pFrame;
	while(NULL != (pFrame=(IppVmetaPicture*)g_queue_pop_tail(&share->vMetaFrameCandidateQueue))) {
		IPP_IdleFrameBuf(pFrame);
	}

	assist_myecho("Before clear ts queue, there is %d ts in queue\n", g_queue_get_length(&share->vMetaStmTSCandidateQueue));
	GstClockTime* pTs;
	while(NULL != (pTs=(GstClockTime*)g_queue_pop_tail(&share->vMetaStmTSCandidateQueue))) {
		g_slice_free(GstClockTime, pTs);
	}
	return;
}

#define IPP_STMREPO_CNT(share)				(g_slist_length((share)->vMetaStmBufRepo))
#define IPP_FRAMEREPO_CNT(share)			(g_slist_length((share)->vMetaFrameBufRepo))
#define IPP_STMREPO_TOTALSZ(share)			(IPP_GetRepoBufSz((share)->vMetaStmBufRepo, 0))
#define IPP_FRAMEREPO_TOTALSZ(share)		(IPP_GetRepoBufSz((share)->vMetaFrameBufRepo, 1))

static int
IPP_GetRepoBufSz(GSList* list, int type)
{
	int sz = 0;
	while(list) {
		if(type == 0) {
			sz += ((IppVmetaBitstream*)(list->data))->nBufSize;
		}else{
			sz += ((IppVmetaPicture*)(list->data))->nBufSize;
		}
		list = list->next;
	}
	return sz;
}

static IppVmetaPicture*
IPP_NewFrameBufForRepo(int sz)
{
	Ipp32u nPhyAddr;
	unsigned char* pBuf = vdec_os_api_dma_alloc(sz, VMETA_DIS_BUF_ALIGN, &nPhyAddr);
	if(pBuf != NULL) {
		IppVmetaPicture* pFrame = g_slice_new0(IppVmetaPicture);
		if(pFrame != NULL) {
			pFrame->pBuf = pBuf;
			pFrame->nPhyAddr = nPhyAddr;
			pFrame->nBufSize = sz;
			return pFrame;
		}else{
			vdec_os_api_dma_free(pBuf);
		}
	}
	return NULL;
}

static void
IPP_DestroyIdelFramesInRepos(IppGSTVMETAEncShare* share)
{
	GSList* OldList = share->vMetaFrameBufRepo;
	GSList* NewBusyList = NULL;
	GSList* pNode;
	while(OldList) {
		pNode = OldList;
		//OldList = g_slist_remove_link(OldList, OldList);
		OldList = OldList->next;
		pNode->next = NULL;
		if(IS_IPPVMETA_FRAME_BUSY(pNode->data)) {
			//NewBusyList = g_slist_concat(pNode, NewBusyList);
			pNode->next = NewBusyList;
			NewBusyList = pNode;
		}else{
			vdec_os_api_dma_free(((IppVmetaPicture*)(pNode->data))->pBuf);
			g_slice_free(IppVmetaPicture, pNode->data);
			g_slist_free1(pNode);
		}
	}
	share->vMetaFrameBufRepo = NewBusyList;
	return;
}

#define IPPGST_TOOMUCH_IDLEFSMALLRAME_INREPO		3
#define IPPGST_TOOMUCH_IDLESMALLSTM_INREPO			5

static IppVmetaPicture*
IPP_RentFrameBufferFromRepo(IppGSTVMETAEncShare* share, int wantedsz)
{
	GSList* pNode = share->vMetaFrameBufRepo;
	int cnt = 0, idlecnt = 0;
	IppVmetaPicture* pFrame;
	while(pNode) {
		cnt++;
		pFrame = (IppVmetaPicture*)(pNode->data);
		if(!IS_IPPVMETA_FRAME_BUSY(pFrame)) {
			idlecnt++;
			if((int)(pFrame->nBufSize) >= wantedsz) {
				pFrame->pUsrData1 = (void*)0;
				pFrame->pUsrData2 = (void*)0;
				pFrame->nDataLen = 0;
				SET_IPPVMETAFRAME_BUSYFLAG(pFrame, 1);
				return pFrame;
			}
		}
		pNode = pNode->next;
	}

	if(idlecnt >= IPPGST_TOOMUCH_IDLEFSMALLRAME_INREPO) {
		IPP_DestroyIdelFramesInRepos(share);
	}

	pFrame = IPP_NewFrameBufForRepo(wantedsz);
	if(pFrame) {
		SET_IPPVMETAFRAME_BUSYFLAG(pFrame, 1);
		share->vMetaFrameBufRepo = g_slist_prepend(share->vMetaFrameBufRepo, pFrame);
	}
	return pFrame;
}

static IppVmetaBitstream*
IPP_NewStmBufForRepo(int sz)
{
	Ipp32u nPhyAddr;
	unsigned char* pBuf = vdec_os_api_dma_alloc(sz, VMETA_STRM_BUF_ALIGN, &nPhyAddr);
	if(pBuf != NULL) {
		IppVmetaBitstream* pStm = g_slice_new0(IppVmetaBitstream);
		if(pStm != NULL) {
			pStm->pBuf = pBuf;
			pStm->nPhyAddr = nPhyAddr;
			pStm->nBufSize = sz;
			return pStm;
		}else{
			vdec_os_api_dma_free(pBuf);
		}
	}
	return NULL;
}

static void
IPP_DestroyIdelStmsInRepos(IppGSTVMETAEncShare* share)
{
	GSList* OldList = share->vMetaStmBufRepo;
	GSList* NewBusyList = NULL;
	GSList* pNode;
	while(OldList) {
		pNode = OldList;
		//OldList = g_slist_remove_link(OldList, OldList);
		OldList = OldList->next;
		pNode->next = NULL;
		if(IS_IPPVMETA_STM_BUSY(pNode->data)) {
			//NewBusyList = g_slist_concat(pNode, NewBusyList);
			pNode->next = NewBusyList;
			NewBusyList = pNode;
		}else{
			vdec_os_api_dma_free(((IppVmetaBitstream*)(pNode->data))->pBuf);
			g_slice_free(IppVmetaBitstream, pNode->data);
			g_slist_free1(pNode);
		}
	}
	share->vMetaStmBufRepo = NewBusyList;
	return;
}


static IppVmetaBitstream*
IPP_RentStmBufferFromRepo(IppGSTVMETAEncShare* share, int wantedsz)
{
	GSList* pNode = share->vMetaStmBufRepo;
	int cnt = 0, idlecnt = 0;
	IppVmetaBitstream* pStm;
	while(pNode) {
		cnt++;
		pStm = (IppVmetaBitstream*)(pNode->data);
		if(!IS_IPPVMETA_STM_BUSY(pStm)) {
			idlecnt++;
			if((int)(pStm->nBufSize) >= wantedsz) {
				pStm->nDataLen = 0;
				SET_IPPVMETASTM_BUSYFLAG(pStm, 1);
				return pStm;
			}
		}
		pNode = pNode->next;
	}

	if(idlecnt >= IPPGST_TOOMUCH_IDLESMALLSTM_INREPO) {
		IPP_DestroyIdelStmsInRepos(share);
	}

	pStm = IPP_NewStmBufForRepo(wantedsz);
	if(pStm) {
		SET_IPPVMETASTM_BUSYFLAG(pStm, 1);
		share->vMetaStmBufRepo = g_slist_prepend(share->vMetaStmBufRepo, pStm);
	}
	return pStm;
}


#define IPPGSTBUFTYPE_DIRECTUSABLE_ENCPLUGIN_ALLOCATED		0
#define IPPGSTBUFTYPE_DIRECTUSABLE_SRCPLUGIN_ALLOCATED		1
#define IPPGSTBUFTYPE_DIRECTUSABLE_SRCPLUGIN_ALLOCATEDEX	2
#define IPPGSTBUFTYPE_NOTDIRECTUSABLE						-1
static int
IPP_DistinguishGstBufFromUpstream(IppGSTVMETAEncShare* share, GstBuffer* buf, int* pnPhyAddr)
{
	if(GST_IS_VMETAENCSHARE_BUFFER(buf))	{
		return IPPGSTBUFTYPE_DIRECTUSABLE_ENCPLUGIN_ALLOCATED;
	}else{
		unsigned int PhyAddr;
		IPPGSTDecDownBufSideInfo* downbufsideinfo = IPPGST_BUFFER_CUSTOMDATA(buf);
		if(downbufsideinfo != NULL) {
			PhyAddr = (unsigned int)downbufsideinfo->phyAddr;
			if(PhyAddr != 0 && (PhyAddr&3) == 0) {
				*pnPhyAddr = (int)PhyAddr;
				return IPPGSTBUFTYPE_DIRECTUSABLE_SRCPLUGIN_ALLOCATEDEX;
			}
		}
		PhyAddr = (unsigned int)vdec_os_api_get_pa((unsigned int)(GST_BUFFER_DATA(buf)));
		if(PhyAddr != 0 && (PhyAddr&3) == 0) {
			*pnPhyAddr = (int)PhyAddr;
			return IPPGSTBUFTYPE_DIRECTUSABLE_SRCPLUGIN_ALLOCATED;
		}
	}
	return IPPGSTBUFTYPE_NOTDIRECTUSABLE;
}

static __inline int
IPP_IsWholeFrameInputMode(IppGSTVMETAEncShare* share, int iFragdataLen)
{
	if(G_LIKELY(share->iWholeFrameInputModeFlag != -1)) {
		return share->iWholeFrameInputModeFlag;
	}else{
		if(iFragdataLen == share->i1SrcFrameDataLen) {
			share->iWholeFrameInputModeFlag = 1;
			assist_myecho("It's whole frame input mode, input fragdata length is %d!!!\n", iFragdataLen);
			return 1;
		}else{
			share->iWholeFrameInputModeFlag = 0;
			assist_myecho("It's not whole frame input mode, input fragdata length is %d!!!\n", iFragdataLen);
			return 0;
		}
	}
}


#define LOOPVMETA_ERR_FATAL						0x80000000
#define LOOPVMETA_ERR_RENTSTMBUF				(1<<0)
#define LOOPVMETA_ERR_RENTFRAMEBUF				(1<<1)
#define LOOPVMETA_ERR_NEWPUSHDOWNBUF			(1<<2)
#define LOOPVMETA_ERR_PUSHSTMDOWN				(1<<3)
#define LOOPVMETA_SUCRETURN_AT_NEEDINPUT		(1<<8)
#define LOOPVMETA_SUCRETURN_AT_EOS				(1<<9)
#define LOOPVMETA_SUCRETURN_AT_PICENCODED		(1<<10)
#define LOOPVMETA_ERR_VMETAAPI_CMDEOS			(1<<16)
#define LOOPVMETA_ERR_VMETAAPI_PUSHFRAMEBUF		(1<<17)
#define LOOPVMETA_ERR_VMETAAPI_PUSHSTMBUF		(1<<18)
#define LOOPVMETA_ERR_VMETAAPI_POPFRAMEBUF		(1<<19)
#define LOOPVMETA_ERR_VMETAAPI_POPSTMBUF		(1<<20)
#define LOOPVMETA_ERR_VMETAAPI_RETUNWANTEDEOS	(1<<21)
#define LOOPVMETA_ERR_VMETAAPI_RETUNWANTED		(1<<22)

static int
IPP_LoopVmeta(IppGSTVMETAEncShare* share, int not_push_stream_down, int bStopCodec, int bExitAtEndofpic)
{
	int ret = 0;
	IppCodecStatus codecRet;
	IppVmetaPicture* pFrame = NULL;
	IppVmetaBitstream* pStm = NULL;
	void ** ppTmp;
	for(;;) {
#ifdef DEBUG_PERFORMANCE
		gettimeofday(&((VmetaProbeData*)(share->pProbeData))->EncAPI_clk0, NULL);
#endif
		codecRet = EncodeFrame_Vmeta(&share->vMetaEnc_Info, share->vMetaEnc_Handle);
#ifdef DEBUG_PERFORMANCE
		gettimeofday(&((VmetaProbeData*)(share->pProbeData))->EncAPI_clk1, NULL);
		share->codec_time += (((VmetaProbeData*)(share->pProbeData))->EncAPI_clk1.tv_sec - ((VmetaProbeData*)(share->pProbeData))->EncAPI_clk0.tv_sec)*1000000 + (((VmetaProbeData*)(share->pProbeData))->EncAPI_clk1.tv_usec - ((VmetaProbeData*)(share->pProbeData))->EncAPI_clk0.tv_usec);
#endif
		//IPPPSEUDO_GST_LOG_OBJECT(share, "EncodeFrame_Vmeta() return %d", codecRet);	//IPPPSEUDO_GST_LOG_OBJECT cost cpu time, comment it.
		if(codecRet == IPP_STATUS_NEED_INPUT) {
			//IPPPSEUDO_GST_LOG_OBJECT(share, "venc:  IPP_STATUS_NEED_INPUT");
			if(bStopCodec == 1) {
				codecRet = EncodeSendCmd_Vmeta(IPPVC_END_OF_STREAM, NULL, NULL, share->vMetaEnc_Handle);
				if(G_UNLIKELY(codecRet != IPP_STATUS_NOERR)) {
					g_warning("EncodeSendCmd_Vmeta(IPPVC_END_OF_STREAM,...) fail, return %d!", codecRet);
					IPPPSEUDO_GST_ERROR_OBJECT(share, "EncodeSendCmd_Vmeta(IPPVC_END_OF_STREAM,...) fail, return %d!", codecRet);
					ret |= (LOOPVMETA_ERR_FATAL | LOOPVMETA_ERR_VMETAAPI_CMDEOS);
					return ret;
				}else{
					continue;
				}
			}

			pFrame = IPP_PopFrameFromQueue(share);
			if(pFrame == NULL) {
				return (ret | LOOPVMETA_SUCRETURN_AT_NEEDINPUT);		//exit loop
			}else{
				//IPPPSEUDO_GST_LOG_OBJECT(share, "Get a frame from candidate queue");
				if(G_UNLIKELY(pFrame->nBufSize < share->vMetaEnc_Info.dis_buf_size)) {
					//the buf size isn't meet vmeta request
					//IPPPSEUDO_GST_LOG_OBJECT(share, "Frame buffer size(%d) doesn't meet the request(%d) of vmeta, do additional copy", pFrame->nBufSize, share->vMetaEnc_Info.dis_buf_size);
					IppVmetaPicture* oldpFrame = pFrame;
					int wantedsz = share->i1SrcFrameDataLen > (int)(share->vMetaEnc_Info.dis_buf_size) ? share->i1SrcFrameDataLen : (int)(share->vMetaEnc_Info.dis_buf_size);
					pFrame = IPP_RentFrameBufferFromRepo(share, wantedsz);
					if(pFrame) {
						memcpy(pFrame->pBuf, oldpFrame->pBuf, share->i1SrcFrameDataLen);
						pFrame->nDataLen = share->i1SrcFrameDataLen;
						IPP_IdleFrameBuf(oldpFrame);
					}else{
						g_warning("IPP_RentFrameBufferFromRepo() fail when extending frame buffer size, total frame buffer cnt %d size %d in repo, new frame buffer wanted size %d", IPP_FRAMEREPO_CNT(share), IPP_FRAMEREPO_TOTALSZ(share), wantedsz);
						IPPPSEUDO_GST_ERROR_OBJECT(share, "IPP_RentFrameBufferFromRepo() fail when extending frame buffer size, total frame buffer cnt %d size %d in repo, new frame buffer wanted size %d", IPP_FRAMEREPO_CNT(share), IPP_FRAMEREPO_TOTALSZ(share), wantedsz);
						IPP_IdleFrameBuf(oldpFrame);
						ret |= (LOOPVMETA_ERR_FATAL | LOOPVMETA_ERR_RENTFRAMEBUF);
						return ret;
					}
				}
				codecRet = EncoderPushBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC, (void*)pFrame, share->vMetaEnc_Handle);
				//IPPPSEUDO_GST_LOG_OBJECT(share, "push a buffer from candidate queue");
				if(G_UNLIKELY(codecRet != IPP_STATUS_NOERR)) {
					g_warning("EncoderPushBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC,...) fail, return %d", codecRet);
					IPPPSEUDO_GST_ERROR_OBJECT(share, "EncoderPushBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC,...) fail, return %d", codecRet);
					ret |= (LOOPVMETA_ERR_FATAL | LOOPVMETA_ERR_VMETAAPI_PUSHFRAMEBUF);
					IPP_IdleFrameBuf(pFrame);
					return ret;
				}
			}
		}else if(codecRet == IPP_STATUS_NEED_OUTPUT_BUF) {
			//IPPPSEUDO_GST_LOG_OBJECT(share, "venc:  IPP_STATUS_NEED_OUTPUT_BUF");
			pStm = IPP_RentStmBufferFromRepo(share, share->iWantedStmBufSz);
			if(G_UNLIKELY(pStm == NULL)) {
				g_warning("IPP_RentStmBufferFromRepo() fail, has allocated %d stream buffers, total size %d, new stream buffer wanted size %d", IPP_STMREPO_CNT(share), IPP_STMREPO_TOTALSZ(share), share->iWantedStmBufSz);
				IPPPSEUDO_GST_ERROR_OBJECT(share, "IPP_RentStmBufferFromRepo() fail, has allocated %d stream buffers, total size %d, new stream buffer wanted size %d", IPP_STMREPO_CNT(share), IPP_STMREPO_TOTALSZ(share), share->iWantedStmBufSz);
				ret |= (LOOPVMETA_ERR_FATAL | LOOPVMETA_ERR_RENTSTMBUF);
				return ret;
			}else{
				codecRet = EncoderPushBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM, (void*)pStm, share->vMetaEnc_Handle);
				if(G_UNLIKELY(codecRet != IPP_STATUS_NOERR)) {
					g_warning("EncoderPushBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM,...) fail, return %d", codecRet);
					IPPPSEUDO_GST_ERROR_OBJECT(share, "EncoderPushBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM,...) fail, return %d", codecRet);
					ret |= (LOOPVMETA_ERR_FATAL | LOOPVMETA_ERR_VMETAAPI_PUSHSTMBUF);
					IPP_IdleStmBuf(pStm);
					return ret;
				}
			}
		}else if(codecRet == IPP_STATUS_END_OF_PICTURE) {
			//IPPPSEUDO_GST_LOG_OBJECT(share, "venc:  IPP_STATUS_END_OF_PICTURE");
			for(;;) {
				ppTmp = (void**)(&pFrame);
				codecRet = EncoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC, ppTmp, share->vMetaEnc_Handle);
				if(G_UNLIKELY(codecRet != IPP_STATUS_NOERR)) {
					g_warning("EncoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC,...) fail, return %d", codecRet);
					IPPPSEUDO_GST_ERROR_OBJECT(share, "EncoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC,...) fail, return %d", codecRet);
					ret |= (LOOPVMETA_ERR_FATAL | LOOPVMETA_ERR_VMETAAPI_POPFRAMEBUF);
					return ret;
				}
				if(pFrame == NULL) {
					break;
				}
				//IPPPSEUDO_GST_LOG_OBJECT(share, "venc:  IPP_STATUS_END_OF_PICTURE:  pop a pic buffer");
				IPP_IdleFrameBuf(pFrame);
			}

			GstClockTime ts;
			for(;;) {
				ppTmp = (void**)(&pStm);
				codecRet = EncoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM, ppTmp, share->vMetaEnc_Handle);
				if(G_UNLIKELY(codecRet != IPP_STATUS_NOERR)) {
					g_warning("EncoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM,...) fail, return %d", codecRet);
					IPPPSEUDO_GST_ERROR_OBJECT(share, "EncoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM,...) fail, return %d", codecRet);
					ret |= (LOOPVMETA_ERR_FATAL | LOOPVMETA_ERR_VMETAAPI_POPSTMBUF);
					return ret;
				}
				if(pStm == NULL) {
					break;
				}
				if(G_LIKELY(pStm->nDataLen > 0)) {
					share->iEncProducedByteCnt += pStm->nDataLen;
					share->iEncProducedCompressedFrameCnt++;
				}else{
					g_warning("vMeta encoder produced an abnormal compressed frame whose length is %d!", pStm->nDataLen);
					break;
				}
				//IPPPSEUDO_GST_LOG_OBJECT(share, "share:  IPP_STATUS_END_OF_PICTURE:  pop a stream buffer");
				ts = IPP_PopTSFromQueue(share);

				IPP_AdjustStmSz(share, pStm->nDataLen);

				if(not_push_stream_down == 0) {
					GstBuffer* down_buf = gst_buffer_new_and_alloc(pStm->nDataLen);
					if(G_LIKELY(down_buf != NULL)) {
						memcpy(GST_BUFFER_DATA(down_buf), pStm->pBuf, pStm->nDataLen);
						IPP_IdleStmBuf(pStm);
					}else{
						IPP_IdleStmBuf(pStm);
						assist_myecho("Allocate push down buffer fail, give up this compressed frame, continue encoding");
						g_warning("Allocate push down buffer fail, give up this compressed frame, continue encoding");
						ret |= LOOPVMETA_ERR_NEWPUSHDOWNBUF;
						continue;
					}

					//push the data to downstream
					GST_BUFFER_TIMESTAMP(down_buf) = ts;
					GST_BUFFER_DURATION(down_buf) = share->iFixedFRateDuration;
					gst_buffer_set_caps(down_buf, GST_PAD_CAPS(share->srcpad));

					//IPPPSEUDO_GST_LOG_OBJECT(share, "Pushdown buffer sz %d, TS %lld, DU %lld", GST_BUFFER_SIZE(down_buf), GST_BUFFER_TIMESTAMP(down_buf), GST_BUFFER_DURATION(down_buf));
					GstFlowReturn pushret = gst_pad_push(share->srcpad, down_buf);
					if(pushret != GST_FLOW_OK) {
						//IPPPSEUDO_GST_LOG_OBJECT(share, "gst_pad_push fail, return %d", pushret);
						if(pushret != GST_FLOW_WRONG_STATE) {	//During pipeline state change, gst_pad_push() offen return GST_FLOW_WRONG_STATE, it's normal. Therefore, we don't print this warning.
							IPPPSEUDO_GST_WARNING_OBJECT(share, "gst_pad_push fail, return %d\n", pushret);
						}
						ret |= LOOPVMETA_ERR_PUSHSTMDOWN;
					}
				}else{
					IPP_IdleStmBuf(pStm);
				}
			}
			//IPPPSEUDO_GST_LOG_OBJECT(share, "share:  IPP_STATUS_END_OF_PIC:  done");
			if(bExitAtEndofpic) {
				//IPPPSEUDO_GST_LOG_OBJECT(share, "share:  IPP_STATUS_END_OF_PIC:  exit");
				return (ret | LOOPVMETA_SUCRETURN_AT_PICENCODED);
			}
		}else if(codecRet == IPP_STATUS_END_OF_STREAM) {
			IPPPSEUDO_GST_DEBUG_OBJECT(share, "share:  IPP_STATUS_END_OF_STREAM");
			if(bStopCodec == 0) {
				g_warning("EncodeFrame_Vmeta return IPP_STATUS_END_OF_STREAM, but not expected!");
				IPPPSEUDO_GST_ERROR_OBJECT(share, "EncodeFrame_Vmeta return IPP_STATUS_END_OF_STREAM, but not expected!");
				ret |= (LOOPVMETA_ERR_FATAL | LOOPVMETA_ERR_VMETAAPI_RETUNWANTEDEOS);
				return ret;
			}else{
				for(;;) {
					ppTmp = (void**)(&pFrame);
					codecRet = EncoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC, ppTmp, share->vMetaEnc_Handle);
					if(G_UNLIKELY(codecRet != IPP_STATUS_NOERR)) {
						g_warning("EncoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC,...) fail, return %d", codecRet);
						IPPPSEUDO_GST_ERROR_OBJECT(share, "EncoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_PIC,...) fail, return %d", codecRet);
						ret |= LOOPVMETA_ERR_VMETAAPI_POPFRAMEBUF;
						continue;
					}
					if(pFrame == NULL) {
						break;
					}
					IPP_IdleFrameBuf(pFrame);
				}

				for(;;) {
					ppTmp = (void**)(&pStm);
					codecRet = EncoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM, ppTmp, share->vMetaEnc_Handle);
					if(G_UNLIKELY(codecRet != IPP_STATUS_NOERR)) {
						g_warning("EncoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM,...) fail, return %d", codecRet);
						IPPPSEUDO_GST_ERROR_OBJECT(share, "EncoderPopBuffer_Vmeta(IPP_VMETA_BUF_TYPE_STRM,...) fail, return %d", codecRet);
						ret |= LOOPVMETA_ERR_VMETAAPI_POPSTMBUF;
						continue;
					}
					if(pStm == NULL) {
						break;
					}
					IPP_IdleStmBuf(pStm);
				}
				return (ret | LOOPVMETA_SUCRETURN_AT_EOS);		//exit loop
			}
		}else if(codecRet == IPP_STATUS_RETURN_INPUT_BUF) {
			assist_myecho("EncodeFrame_Vmeta() return IPP_STATUS_RETURN_INPUT_BUF, don't handle this according to vmeta encoder sample code!!!\n");
		}else if(codecRet == IPP_STATUS_OUTPUT_DATA) {
			//IPPPSEUDO_GST_LOG_OBJECT(share, "EncodeFrame_Vmeta() return IPP_STATUS_OUTPUT_DATA, don't handle this according to vmeta encoder sample code!!!");
		}else{
			g_warning("EncodeFrame_Vmeta() return unwanted code %d, encoding fail!", codecRet);
			IPPPSEUDO_GST_ERROR_OBJECT(share, "EncodeFrame_Vmeta() return unwanted code %d, encoding fail!", codecRet);
			ret |= (LOOPVMETA_ERR_FATAL | LOOPVMETA_ERR_VMETAAPI_RETUNWANTED);
			return ret;
		}
	}

	return ret;
}

static int
IPP_PrepareFrameTSFromFileSrc(IppGSTVMETAEncShare* share, GstBuffer* buf)
{
	int iSrcLen = GST_BUFFER_SIZE(buf);
	unsigned char* p = GST_BUFFER_DATA(buf);
	int copylen;
	int framebufsz = share->i1SrcFrameDataLen > (int)(share->vMetaEnc_Info.dis_buf_size) ? share->i1SrcFrameDataLen : (int)(share->vMetaEnc_Info.dis_buf_size);
	while(iSrcLen > 0) {
		if(share->FrameBufferForFileSrc == NULL) {
			share->FrameBufferForFileSrc = IPP_RentFrameBufferFromRepo(share, framebufsz);
			if(share->FrameBufferForFileSrc == NULL) {
				g_warning("IPP_RentFrameBufferFromRepo() fail when copy data from filesrc, total frame cnt %d sz %d in repo, new frame wanted sz %d", IPP_FRAMEREPO_CNT(share), IPP_FRAMEREPO_TOTALSZ(share), framebufsz);
				return -1;
			}
		}
		copylen = share->i1SrcFrameDataLen - share->FrameBufferForFileSrc->nDataLen;
		if(iSrcLen < copylen) {
			copylen = iSrcLen;
		}
		memcpy(share->FrameBufferForFileSrc->pBuf + share->FrameBufferForFileSrc->nDataLen, p, copylen);
		p += copylen;
		share->FrameBufferForFileSrc->nDataLen += copylen;
		iSrcLen -= copylen;
		if((int)(share->FrameBufferForFileSrc->nDataLen) == share->i1SrcFrameDataLen) {
			assist_myechomore("gather a frame from streaming input, frame data length is %d\n", share->FrameBufferForFileSrc->nDataLen);
			IPP_PushFrameToQueue(share, share->FrameBufferForFileSrc);
			share->FrameBufferForFileSrc = NULL;
			IPP_PushTSToQueue(share, share->AotoIncTimestamp);
			share->AotoIncTimestamp += share->iFixedFRateDuration;
		}
	}
	return 0;
}


GstFlowReturn _vmetaencshare_chain(GstBuffer* buf, IppGSTVMETAEncShare* share, int* pbTerminate, int* pLoopRet)
{
	int loopret = 0;
	IppVmetaPicture* pFrame = NULL;
	IppCodecStatus codecRet;

	assist_myechomore("Input buffer sz %d, TS %lld, DU %lld\n", GST_BUFFER_SIZE(buf), GST_BUFFER_TIMESTAMP(buf), GST_BUFFER_DURATION(buf));

	*pbTerminate = 0;

	if(share->driver_init_ret < 0) {
		gst_buffer_unref(buf);
		g_warning("vMeta driver hasn't been inited!");
		IPPPSEUDO_GST_ERROR_OBJECT(share, "vMeta driver hasn't been inited!");
		goto VMETAENC_CHAIN_FATALERR;
	}

	if(share->vMetaEnc_Par.nWidth == 0) {
		gst_buffer_unref(buf);
		g_warning("encoder parameter is still unset in chain()!");
		IPPPSEUDO_GST_ERROR_OBJECT(share, "encoder parameter is still unset in chain()!");
		goto VMETAENC_CHAIN_FATALERR;
	}

	if(share->vMetaEnc_Handle == NULL) {
		MiscGeneralCallbackTable CBTable;
		CBTable.fMemMalloc  = IPP_MemMalloc;
		CBTable.fMemFree    = IPP_MemFree;
		share->vMetaEnc_Par.bMultiIns = share->SupportMultiinstance == 0 ? 0 : 1;
		share->vMetaEnc_Par.bFirstUser = share->SupportMultiinstance != 2 ? 0 : 1;
		assist_myecho("Calling EncoderInitAlloc_Vmeta()...\n");
		codecRet = EncoderInitAlloc_Vmeta(&share->vMetaEnc_Par, &CBTable, &share->vMetaEnc_Handle);
		assist_myecho("EncoderInitAlloc_Vmeta() return %d\n", codecRet);
		if(G_UNLIKELY(codecRet != IPP_STATUS_NOERR)) {
			gst_buffer_unref(buf);
			g_warning("EncoderInitAlloc_Vmeta() fail, ret %d", codecRet);
			IPPPSEUDO_GST_ERROR_OBJECT(share, "EncoderInitAlloc_Vmeta() fail, ret %d", codecRet);
			share->vMetaEnc_Handle = NULL;
			goto VMETAENC_CHAIN_FATALERR;
		}

		if(share->ForceStartTimestamp != -1) {
			share->AotoIncTimestamp = share->ForceStartTimestamp;
		}else if(! IPP_IsWholeFrameInputMode(share, GST_BUFFER_SIZE(buf))) {
			share->AotoIncTimestamp = 0;
		}
	}

	if(! IPP_IsWholeFrameInputMode(share, GST_BUFFER_SIZE(buf))) {
		//stream input
		int ret = IPP_PrepareFrameTSFromFileSrc(share, buf);
		gst_buffer_unref(buf);
		if(ret < 0) {
			g_warning("IPP_CopyDataFromFileSrc() fail, ret %d!", ret);
			IPPPSEUDO_GST_ERROR_OBJECT(share, "IPP_CopyDataFromFileSrc() fail, ret %d!", ret);
			goto VMETAENC_CHAIN_FATALERR;
		}
	}else{
		//frame input
		if(share->AotoIncTimestamp != GST_CLOCK_TIME_NONE) {
			IPP_PushTSToQueue(share, share->AotoIncTimestamp);
			share->AotoIncTimestamp += share->iFixedFRateDuration;
		}else{
			IPP_PushTSToQueue(share, GST_BUFFER_TIMESTAMP(buf));
		}
		int nPhyAddr;
		int gstbuftype = IPP_DistinguishGstBufFromUpstream(share, buf, &nPhyAddr);
		if( gstbuftype == IPPGSTBUFTYPE_DIRECTUSABLE_ENCPLUGIN_ALLOCATED ) {
#ifdef IPPGSTVMETAENC_ECHO
			((VmetaProbeData*)(share->pProbeData))->bufdistinguish_cnt0++;
#endif
			pFrame = GST_VMETAENCSHARE_BUFFER_CAST(buf)->pVmetaFrame;
			IPPVMETAFRAME_ATTCHEDGSTBUF(pFrame) = (void*)buf;
			pFrame->nDataLen = share->i1SrcFrameDataLen;
			IPP_PushFrameToQueue(share, pFrame);
		}else if(gstbuftype == IPPGSTBUFTYPE_DIRECTUSABLE_SRCPLUGIN_ALLOCATED || gstbuftype == IPPGSTBUFTYPE_DIRECTUSABLE_SRCPLUGIN_ALLOCATEDEX) {
			int bufsz = GST_BUFFER_SIZE(buf);
#ifdef IPPGSTVMETAENC_ECHO
			if(gstbuftype == IPPGSTBUFTYPE_DIRECTUSABLE_SRCPLUGIN_ALLOCATED) {
				((VmetaProbeData*)(share->pProbeData))->bufdistinguish_cnt1++;
			}else{
				((VmetaProbeData*)(share->pProbeData))->bufdistinguish_cnt2++;
			}
#endif
			pFrame = g_slice_new0(IppVmetaPicture);
			pFrame->pBuf = GST_BUFFER_DATA(buf);
			pFrame->nPhyAddr = nPhyAddr;
			pFrame->nBufSize = bufsz > (int)share->vMetaEnc_Info.dis_buf_size ? bufsz : (int)share->vMetaEnc_Info.dis_buf_size;	//It's a trick. If the memory is allocated by marvell plug-in like marvell camera-hal or marvell vMeta dec, the real buffer size is larger than GST_BUFFER_SIZE(). For example, for 1920x1080 UYVY frame, camera-hal allocated memory size is 1920*1088*2 bytes, but it will let GST_BUFFER_SIZE() = 1920*1080*2.
			pFrame->nDataLen = share->i1SrcFrameDataLen;
			IPPVMETAFRAME_ATTCHEDGSTBUF(pFrame) = (void*)buf;
			pFrame->pUsrData2 = (void*)1;	//indicate this IppVmetaPicture isn't from repo
			IPP_PushFrameToQueue(share, pFrame);
		}else{
#ifdef IPPGSTVMETAENC_ECHO
			((VmetaProbeData*)(share->pProbeData))->bufdistinguish_cnt3++;
#endif
			int wantedsz = share->i1SrcFrameDataLen > (int)(share->vMetaEnc_Info.dis_buf_size) ? share->i1SrcFrameDataLen : (int)(share->vMetaEnc_Info.dis_buf_size);
			pFrame = IPP_RentFrameBufferFromRepo(share, wantedsz);
			if(pFrame) {
				memcpy(pFrame->pBuf, GST_BUFFER_DATA(buf), share->i1SrcFrameDataLen);
				pFrame->nDataLen = share->i1SrcFrameDataLen;
				IPP_PushFrameToQueue(share, pFrame);
			}else{
				g_warning("IPP_RentFrameBufferFromRepo() fail when copy data from upstream frame, total frame cnt %d sz %d in repo, new frame wanted sz %d", IPP_FRAMEREPO_CNT(share), IPP_FRAMEREPO_TOTALSZ(share), wantedsz);
			}
			gst_buffer_unref(buf);
		}
	}

	if(!g_queue_is_empty(&share->vMetaFrameCandidateQueue)) {
		//only when there is frame candidate, call vMeta API
		loopret = IPP_LoopVmeta(share, 0, 0, (share->HungryStrategy==0));
		if(loopret & LOOPVMETA_ERR_FATAL) {
			g_warning("Loop vMeta encoder fatal error occur!");
			goto VMETAENC_CHAIN_FATALERR;
		}
	}
	*pLoopRet = loopret;
	return GST_FLOW_OK;

VMETAENC_CHAIN_FATALERR:
	*pbTerminate = 1;
	*pLoopRet = loopret;
	return GST_FLOW_ERROR;
}

static void
IPP_ExtHoldingResWatcher_on_Finalize(gpointer data)
{
	assist_myecho("IPP_ExtHoldingResWatcher_on_Finalize() is calling!\n");
	IppGSTVMETAEncShare* share = (IppGSTVMETAEncShare*)data;
	IPP_DestroyIdelFramesInRepos(share);
	if(share->vMetaFrameBufRepo != NULL) {
		g_warning("Still frame buffers (%d) in repo are not idle!", IPP_FRAMEREPO_CNT(share));
		IPPPSEUDO_GST_ERROR_OBJECT(share, "Still frame buffers (%d) in repo are not idle!", IPP_FRAMEREPO_CNT(share));
	}
	assist_myecho("call vdec_os_driver_clean()...\n");
	vdec_os_driver_clean();
	gst_object_unref(share->gstinst);	//since the os_driver is cleaned, vmetaenc_xxx could be finalized.
	assist_myecho("IPP_ExtHoldingResWatcher_on_Finalize() is finished!\n");
	return;
}


static int
vmetaencshare_null2ready(IppGSTVMETAEncShare* share)
{
	assist_myecho("calling vdec_os_driver_init()...\n");
	share->driver_init_ret = vdec_os_driver_init();
	assist_myecho("vdec_os_driver_init() return %d\n", share->driver_init_ret);
	if(share->driver_init_ret < 0) {
		g_warning("vdec_os_driver_init() fail, return %d\n", share->driver_init_ret);
		IPPPSEUDO_GST_ERROR_OBJECT(share, "vdec_os_driver_init() fail, return %d\n", share->driver_init_ret);
		return -1;
	}
	share->ExtHoldingResWatcher = RES_HOLD_BY_EXTUSER_WATCHMAN_CREATE();
	gst_object_ref(share->gstinst);
	GST_BUFFER_MALLOCDATA(share->ExtHoldingResWatcher) = (guint8*)share;
	assist_myecho("ref vmetaenc_xxx object for ExtHoldingResWatcher!!!\n");
	GST_BUFFER_FREE_FUNC(share->ExtHoldingResWatcher) = (GFreeFunc)IPP_ExtHoldingResWatcher_on_Finalize;

	return 0;
}


static int
vmetaencshare_ready2null(IppGSTVMETAEncShare* share)
{
	//terminate encoder
	if(share->vMetaEnc_Handle) {
		assist_myecho("terminate encoder handle: loop vemta to EOS when ready2null\n");
		int loopret = IPP_LoopVmeta(share, 1, 1, 0);
		assist_myecho("terminate encoder handle: loop vemta to EOS when ready2null, it return %d\n", loopret);
		if(loopret & LOOPVMETA_ERR_FATAL) {
			g_warning("IPP_LoopVmeta(venc, 1, 1, 0) fail, ret %d", loopret);
		}
		assist_myecho("terminate encoder handle: call EncoderFree_Vmeta()\n");
		IppCodecStatus codecRet = EncoderFree_Vmeta(&(share->vMetaEnc_Handle));
		share->vMetaEnc_Handle = NULL;
		if(codecRet != IPP_STATUS_NOERR) {
			g_warning("EncoderFree_Vmeta() fail, return %d", codecRet);
			IPPPSEUDO_GST_ERROR_OBJECT(share, "EncoderFree_Vmeta() fail, return %d", codecRet);
			return -1;
		}
	}

	//clear idle resource
	if(share->FrameBufferForFileSrc) {
		IPP_IdleFrameBuf(share->FrameBufferForFileSrc);
		share->FrameBufferForFileSrc = NULL;
	}
	IPP_ClearCandidateQueus(share);
	IPP_DestroyIdelFramesInRepos(share);
	IPP_DestroyIdelStmsInRepos(share);

	if(share->vMetaStmBufRepo != NULL) {
		g_warning("Still stream buffers (%d) in repo are not idle!", IPP_STMREPO_CNT(share));
		IPPPSEUDO_GST_ERROR_OBJECT(share, "Still stream buffers (%d) in repo are not idle!", IPP_STMREPO_CNT(share));
	}

	if(share->ExtHoldingResWatcher) {
		assist_myecho("unref ExtHoldingResWatcher of vmetaenc during ready2null\n");
		RES_HOLD_BY_EXTUSER_WATCHMAN_UNREF(share->ExtHoldingResWatcher);
		share->ExtHoldingResWatcher = NULL;
	}

#ifdef DEBUG_PERFORMANCE
	//print information
	if(share->pProbeData) {
		assist_myecho("vmetaenc_xxx codec closed, totally outputted %lld bytes and %d frames, input frame type counters are %d,%d,%d,%d.\n", share->iEncProducedByteCnt, share->iEncProducedCompressedFrameCnt, ((VmetaProbeData*)(share->pProbeData))->bufdistinguish_cnt0, ((VmetaProbeData*)(share->pProbeData))->bufdistinguish_cnt1, ((VmetaProbeData*)(share->pProbeData))->bufdistinguish_cnt2, ((VmetaProbeData*)(share->pProbeData))->bufdistinguish_cnt3);
	}

	printf("vmetaenc_xxx pure codec outputted %d compressed frames and costed %lld microseconds!!!\n", share->iEncProducedCompressedFrameCnt, share->codec_time);
#endif

	return 0;
}

GstStateChangeReturn _vmetaencshare_change_state(GstElement* element, GstStateChange transition, IppGSTVMETAEncShare* share, GstElementClass* father_class)
{
	GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;

	assist_myecho("GST vmetaenc state change trans from %d to %d\n", transition>>3, transition&7);

	switch (transition)
	{
	case GST_STATE_CHANGE_NULL_TO_READY:
		{
			int retcode = vmetaencshare_null2ready(share);
			if(retcode != 0) {
				g_warning("vmetaenc_null2ready() fail, ret %d", retcode);
				IPPPSEUDO_GST_ERROR_OBJECT(share, "vmetaenc_null2ready() fail, ret %d", retcode);
				return GST_STATE_CHANGE_FAILURE;
			}
		}
		break;
	default:
		break;
	}

	ret = father_class->change_state(element, transition);
	if (ret == GST_STATE_CHANGE_FAILURE){
		return ret;
	}

	switch (transition)
	{
	case GST_STATE_CHANGE_READY_TO_NULL:
		{
			int retcode = vmetaencshare_ready2null(share);
			if(retcode != 0) {
				g_warning("vmetaenc_ready2null() fail, ret %d", retcode);
				IPPPSEUDO_GST_ERROR_OBJECT(share, "vmetaenc_ready2null() fail, ret %d", retcode);
				return GST_STATE_CHANGE_FAILURE;
			}
		}
		break;

	default:
		break;
	}

	return ret;
}

void _vmetaencshare_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec, IppGSTVMETAEncShare* share)
{
	switch (prop_id)
	{
	case ARG_FORCE_STARTTIME:
		{
			gint64 st = g_value_get_int64(value);
			if(st<(gint64)(-1)) {
				g_warning("force-starttime %lld exceed range!", st);
			}else{
				share->ForceStartTimestamp = st;
			}
		}
		break;
	case ARG_TARGET_BITRATE:
		{
			int bR = g_value_get_int(value);
			if(bR <= 0) {
				g_warning("bitrate %d exceed range!", bR);
			}else{
				share->iTargtBitRate = bR;
			}
		}
		break;
	case ARG_DISABLE_RATECTRL:
		{
			int bDisable = g_value_get_int(value);
			if(bDisable != 0 && bDisable != 1) {
				g_warning("disable-ratecontrol %d exceed range!", bDisable);
			}else{
				share->bDisableRateControl = bDisable;
			}
		}
		break;
	case ARG_QP:
		{
			int qp = g_value_get_int(value);
			if(qp < 1) {
				g_warning("qp %d exceed range!", qp);
			}else{
				share->iQP = qp;
			}
		}
		break;
	case ARG_ONLY_I:
		{
			int onlyI = g_value_get_int(value);
			if(onlyI != 0 && onlyI != 1) {
				g_warning("only-Iframe %d exceed range!", onlyI);
			}else{
				share->bOnlyIFrame = onlyI;
			}
		}
		break;
	case ARG_SUPPORTMULTIINSTANCE:
		{
			int mi = g_value_get_int(value);
			if(mi != 0 && mi != 1 && mi != 2) {
				g_warning("support-multiinstance %d exceeds range!", mi);
			}else{
				share->SupportMultiinstance = mi;
			}
		}
		break;
	case ARG_HUNGRYSTRATEGY:
		{
			int hs = g_value_get_int(value);
			if(hs != 0 && hs != 1) {
				g_warning("hungry-strategy %d exceeds range!", hs);
			}else{
				share->HungryStrategy = hs;
			}
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}

	return;
}

void _vmetaencshare_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec, IppGSTVMETAEncShare* share)
{
	switch (prop_id)
	{
	case ARG_FORCE_STARTTIME:
		g_value_set_int64(value, share->ForceStartTimestamp);
		break;
	case ARG_TARGET_BITRATE:
		g_value_set_int(value, share->iTargtBitRate);
		break;
	case ARG_DISABLE_RATECTRL:
		g_value_set_int(value, share->bDisableRateControl);
		break;
	case ARG_QP:
		g_value_set_int(value, share->iQP);
		break;
	case ARG_ONLY_I:
		g_value_set_int(value, share->bOnlyIFrame);
		break;
	case ARG_SUPPORTMULTIINSTANCE:
		g_value_set_int(value, share->SupportMultiinstance);
		break;
	case ARG_HUNGRYSTRATEGY:
		g_value_set_int(value, share->HungryStrategy);
		break;

#ifdef DEBUG_PERFORMANCE
	case ARG_TOTALFRAME:
		g_value_set_int(value, share->iEncProducedCompressedFrameCnt);
		break;
	case ARG_CODECTIME:
		g_value_set_int64(value, share->codec_time);
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
	return;
}


void _vmetaencshare_onfinalize(IppGSTVMETAEncShare* share)
{
	if(share->pProbeData) {
		g_free(share->pProbeData);
		share->pProbeData = NULL;
	}
	return;
}


void _vmetaencshare_base_init(gpointer g_klass, IppVideoStreamFormat eOutputStrmFmt)
{
	static GstElementDetails vmetaenc_details = {
		"vMeta Video Encoder",
		"Codec/Encoder/Video",
		"vMeta Video Encoder based-on IPP vMeta codec",
		""
	};
	GstElementClass *element_class = GST_ELEMENT_CLASS (g_klass);

	if(eOutputStrmFmt == IPP_VIDEO_STRM_FMT_H264) {
		vmetaenc_details.longname = "vMeta Video Encoder h264";
		gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&src_factory_h264));
	}else if(eOutputStrmFmt == IPP_VIDEO_STRM_FMT_H263) {
		vmetaenc_details.longname = "vMeta Video Encoder h263";
		gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&src_factory_h263));
	}else if(eOutputStrmFmt == IPP_VIDEO_STRM_FMT_MPG4) {
		vmetaenc_details.longname = "vMeta Video Encoder mpeg4";
		gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&src_factory_mpeg4));
	}else if(eOutputStrmFmt == IPP_VIDEO_STRM_FMT_MJPG) {
		vmetaenc_details.longname = "vMeta Video Encoder mjpeg";
		gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&src_factory_mjpeg));
	}
	gst_element_class_add_pad_template(element_class, gst_static_pad_template_get(&sink_factory));

	gst_element_class_set_details(element_class, &vmetaenc_details);

	return;
}

void _vmetaencshare_class_install_property(GObjectClass *gobject_class)
{
	g_object_class_install_property (gobject_class, ARG_FORCE_STARTTIME, \
		g_param_spec_int64 ("force-starttime", "The first frame's timestamp", \
		"Set the first compressed frame's timestamp(unit is nanoseconds). If this value is -1, adopt input frame's timestamp. Otherwise, ignore all timestamp attached on input frame buffer and produce following timestamps according to frame rate.", \
		-1/* range_MIN */, G_MAXINT64/* range_MAX */, -1/* default_INIT */, G_PARAM_READWRITE));

	g_object_class_install_property (gobject_class, ARG_TARGET_BITRATE, \
		g_param_spec_int ("bitrate", "The bit rate of encoded stream", \
		"Only valid when disable-ratecontrol==0. The target bit rate of the encoded stream(bit/second), it's not the real encoded stream's bitrate, only wanted bitrate.", \
		1/* range_MIN */, G_MAXINT/* range_MAX */, IPPGST_DEFAULT_TARGETBITRATE/* default_INIT */, G_PARAM_READWRITE));

	g_object_class_install_property (gobject_class, ARG_DISABLE_RATECTRL, \
		g_param_spec_int ("disable-ratecontrol", "disable rate control", \
		"Disable encoder rate control mechanism.", \
		0/* range_MIN */, 1/* range_MAX */, 0/* default_INIT */, G_PARAM_READWRITE));

	g_object_class_install_property (gobject_class, ARG_QP, \
		g_param_spec_int ("qp", "qp of encoder", \
		"Only valid when disable-ratecontrol==1. QP of encoder.", \
		1/* range_MIN */, G_MAXINT/* range_MAX */, IPPGST_ENCODER_DEFAULTQP/* default_INIT */, G_PARAM_READWRITE));

	g_object_class_install_property (gobject_class, ARG_ONLY_I, \
		g_param_spec_int ("only-Iframe", "only produce I frame", \
		"Let encoder produce only I frame.", \
		0/* range_MIN */, 1/* range_MAX */, 0/* default_INIT */, G_PARAM_READWRITE));

	g_object_class_install_property (gobject_class, ARG_SUPPORTMULTIINSTANCE, \
		g_param_spec_int ("support-multiinstance", "support multiple instance", \
		"To support multiple instance usage. Don't access it unless you know what you're doing very clear. It could be 0<not support>, 1<support> or 2<support and reinit>.", \
		0/* range_MIN */, 2/* range_MAX */, 0/* default_INIT */, G_PARAM_READWRITE));

	g_object_class_install_property (gobject_class, ARG_HUNGRYSTRATEGY, \
		g_param_spec_int ("hungry-strategy", "vMeta hungry strategy", \
		"Let vMeta always hungry or not. Don't access it unless you know what you're doing very clear. It could be 0<not always let vMeta hungry>, 1<always let vMeta hungry>.", \
		0/* range_MIN */, 1/* range_MAX */, 0/* default_INIT */, G_PARAM_READWRITE));

#ifdef DEBUG_PERFORMANCE
	g_object_class_install_property (gobject_class, ARG_TOTALFRAME,
		g_param_spec_int ("totalframes", "Totalframe of compressed stream",
		"Number of total frames encoder produced. Under some case, number of downstream plug-in received frames is probably less than this value.", 0, G_MAXINT, 0, G_PARAM_READABLE));
	g_object_class_install_property (gobject_class, ARG_CODECTIME,
		g_param_spec_int64 ("codectime", "encoder spent time",
		"Total pure encoder spend system time(microsecond).", 0, G_MAXINT64, 0, G_PARAM_READABLE));
#endif
	return;
}


void _vmetaencshare_init_members(IppGSTVMETAEncShare* share, IppVideoStreamFormat eOutputStrmFmt, pfun_PSEUDO_GST_XXX_OBJECT echofun, GstPad* sinkpad, GstPad* srcpad, void* inst)
{
	share->pProbeData = g_malloc0(sizeof(VmetaProbeData));

	share->ExtHoldingResWatcher = NULL;

	share->driver_init_ret = -1;

	share->vMetaEnc_Handle = NULL;

	memset(&share->vMetaEnc_Par, 0, sizeof(share->vMetaEnc_Par));
	share->vMetaEnc_Par.eInputYUVFmt = IPP_YCbCr422I;		//always should be IPP_YCbCr422I
	memset(&share->vMetaEnc_Info, 0, sizeof(share->vMetaEnc_Info));

	share->vMetaStmBufRepo = NULL;
	share->vMetaFrameBufRepo = NULL;

	g_queue_init(&share->vMetaFrameCandidateQueue);
	g_queue_init(&share->vMetaStmTSCandidateQueue);

	share->AotoIncTimestamp = GST_CLOCK_TIME_NONE;

	share->FrameBufferForFileSrc = NULL;

	share->iEncW = 0;
	share->iEncH = 0;
	share->iEncFrameRate = 0;
	share->i1SrcFrameDataLen = 0;
	share->iWantedStmBufSz = IPPGST_DEFAULT_STEAMBUFSZMIN;

	share->iFixedFRateDuration = GST_CLOCK_TIME_NONE;
	share->iWholeFrameInputModeFlag = -1;	//-1 means undecided

	share->ForceStartTimestamp = -1;

	share->iTargtBitRate = IPPGST_DEFAULT_TARGETBITRATE;
	share->bDisableRateControl = 0;
	share->iQP = IPPGST_ENCODER_DEFAULTQP;
	share->bOnlyIFrame = 0;

	share->iEncProducedByteCnt = 0;
	share->iEncProducedCompressedFrameCnt = 0;

	share->SupportMultiinstance = 1;

	share->HungryStrategy = 0;

	share->codec_time = 0;

	share->gstinst = inst;
	share->GSTobj_echo_fun = echofun;
	share->sinkpad = sinkpad;
	share->srcpad = srcpad;

	share->vMetaEnc_Par.eOutputStrmFmt = eOutputStrmFmt;

	return;
}

GstFlowReturn _vmetaencshare_sinkpad_allocbuf(GstPad *pad, guint64 offset, guint size, GstCaps *caps, GstBuffer **buf, IppGSTVMETAEncShare* share)
{
	assist_myechomore("call gst_vmetaenc_sinkpad_allocbuf(), pad(%p), offset(%lld), size(%d)\n", pad, offset, size);
	GstStructure* stru;
	if(share->driver_init_ret < 0) {
		g_warning("vMeta driver isn't inited %d when upstream require buffer, GST will allocate common buffer", share->driver_init_ret);
		*buf = NULL;
		return GST_FLOW_OK;
	}
	int iBufSz = ((int)size) > share->i1SrcFrameDataLen ? ((int)size) : share->i1SrcFrameDataLen;
	if(share->vMetaEnc_Info.dis_buf_size == 0) {
		if((stru = gst_caps_get_structure(caps, 0)) != NULL) {
			int w,h;
			if(TRUE == gst_structure_get_int(stru, "width", &w) && TRUE == gst_structure_get_int(stru, "height", &h)) {
				int vMeta_probably_need_sz = GST_ROUND_UP_16(w) * GST_ROUND_UP_16(h) << 1;
				if(iBufSz < vMeta_probably_need_sz) {
					iBufSz = vMeta_probably_need_sz;
				}
			}
		}
	}else{
		if(iBufSz < (int)(share->vMetaEnc_Info.dis_buf_size)) {
			iBufSz = share->vMetaEnc_Info.dis_buf_size;
		}
	}
	IppVmetaPicture* pFrame = IPP_RentFrameBufferFromRepo(share, iBufSz);
	if(pFrame == NULL) {
		g_warning("Couldn't allocate frame buffer(sz %d) when upstream require buffer, GST will allocate common buffer", iBufSz);
		*buf = NULL;
		return GST_FLOW_OK;
	}
	GstVmetaEncShareFrameBuffer * vMetaEncBuf = gst_vMetaEncShareBuffer_new();
	vMetaEncBuf->pVmetaFrame = pFrame;
	vMetaEncBuf->watcher = RES_HOLD_BY_EXTUSER_WATCHMAN_REF(share->ExtHoldingResWatcher);
	//IPPVMETAFRAME_ATTCHEDGSTBUF(pFrame) = (void*)vMetaEncBuf;
	GST_BUFFER_DATA(vMetaEncBuf) = pFrame->pBuf;
	GST_BUFFER_SIZE(vMetaEncBuf) = size;
	GST_BUFFER_OFFSET(vMetaEncBuf) = offset;
	*buf = (GstBuffer*)vMetaEncBuf;
	gst_buffer_set_caps(*buf, caps);
	return GST_FLOW_OK;
}


gboolean _vmetaencshare_sinkpad_event(GstPad *pad, GstEvent *event, IppGSTVMETAEncShare* share)
{
	gboolean eventRet;
	assist_myechomore("receive event %d on vmetaenc sinkpad(%p).\n", GST_EVENT_TYPE(event), pad);

	switch(GST_EVENT_TYPE (event))
	{
	case GST_EVENT_EOS:
		assist_myecho("GST_EVENT_EOS is received!\n");
		if(share->vMetaEnc_Handle) {
			assist_myecho("call IPP_LoopVmeta(venc, 0, 1, 0) when EOS!\n");
			int loopret = IPP_LoopVmeta(share, 0, 1, 0);
			if(loopret & LOOPVMETA_ERR_FATAL) {
				g_warning("call IPP_LoopVmeta(venc, 0, 1, 0) when EOS fail, return %d", loopret);
				IPPPSEUDO_GST_ERROR_OBJECT(share, "call IPP_LoopVmeta(venc, 0, 1, 0) when EOS fail, return %d", loopret);
			}
			assist_myecho("terminate encoder handle when receiving EOS: call EncoderFree_Vmeta()\n");
			IppCodecStatus codecRet = EncoderFree_Vmeta(&(share->vMetaEnc_Handle));
			if(codecRet != IPP_STATUS_NOERR) {
				g_warning("EncoderFree_Vmeta() fail, return %d", codecRet);
				IPPPSEUDO_GST_ERROR_OBJECT(share, "EncoderFree_Vmeta() fail, return %d", codecRet);
			}
			share->vMetaEnc_Handle = NULL;
		}

		eventRet = gst_pad_push_event(share->srcpad, event);
		break;
	default:
		eventRet = gst_pad_event_default(pad, event);
		break;
	}
	return eventRet;
}
