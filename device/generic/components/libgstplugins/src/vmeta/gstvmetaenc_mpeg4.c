/*
Copyright (c) 2010, Marvell International Ltd.
All Rights Reserved.

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstvmetaenc_mpeg4.h"

GST_DEBUG_CATEGORY_STATIC (vmetaenc_mpeg4_debug);
#define GST_CAT_DEFAULT vmetaenc_mpeg4_debug

GST_BOILERPLATE (Gstvmetampeg4enc, gst_vmetampeg4enc, GstElement, GST_TYPE_ELEMENT);

void gst_vmetampeg4enc_echofun(void* mpeg4, GstDebugLevel level, const char* format, ...)
{
	if(G_LIKELY(level >= GST_LEVEL_ERROR && level <= GST_LEVEL_LOG)) {
		char msg[256];
		va_list args;
		msg[sizeof(msg)-1] = '\0';
		va_start(args, format);
		vsnprintf(msg, sizeof(msg)-1, format, args);
		va_end(args);

		switch(level) {
		case GST_LEVEL_ERROR:
			GST_ERROR_OBJECT(mpeg4, msg);
			break;
		case GST_LEVEL_WARNING:
			GST_WARNING_OBJECT(mpeg4, msg);
			break;
		case GST_LEVEL_INFO:
			GST_INFO_OBJECT(mpeg4, msg);
			break;
		case GST_LEVEL_DEBUG:
			GST_DEBUG_OBJECT(mpeg4, msg);
			break;
		case GST_LEVEL_LOG:
			GST_LOG_OBJECT(mpeg4, msg);
			break;
		default:
			break;
		}
	}
	return;
}

static gboolean
gst_vmetampeg4enc_sinkpad_setcaps(GstPad *pad, GstCaps *caps)
{
	Gstvmetampeg4enc* mpeg4 = GST_VMETAMPEG4ENC(GST_PAD_PARENT(pad));
	return _vmetaencshare_sinkpad_setcaps(pad, caps, &mpeg4->baseobj);
}

static GstFlowReturn
gst_vmetampeg4enc_chain(GstPad *pad, GstBuffer *buf)
{
	Gstvmetampeg4enc* mpeg4 = GST_VMETAMPEG4ENC(GST_PAD_PARENT(pad));
	int bTerminate = 0, loopRet = 0;
	GstFlowReturn flowRt;
	flowRt = _vmetaencshare_chain(buf, &mpeg4->baseobj, &bTerminate, &loopRet);
	if(bTerminate) {
		GST_ELEMENT_ERROR(mpeg4, STREAM, ENCODE, (NULL), ("IPP GST vMeta encoder plug-in fatal error in _chain, error %d", loopRet));
	}
	return flowRt;
}

static GstStateChangeReturn
gst_vmetampeg4enc_change_state(GstElement *element, GstStateChange transition)
{
	Gstvmetampeg4enc* mpeg4 = GST_VMETAMPEG4ENC(element);
	return _vmetaencshare_change_state(element, transition, &mpeg4->baseobj, parent_class);
}

static void
gst_vmetampeg4enc_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
	Gstvmetampeg4enc* mpeg4 = GST_VMETAMPEG4ENC(object);
	_vmetaencshare_set_property(object, prop_id, value, pspec, &mpeg4->baseobj);
	return;
}

static void
gst_vmetampeg4enc_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	Gstvmetampeg4enc* mpeg4 = GST_VMETAMPEG4ENC(object);
	_vmetaencshare_get_property(object, prop_id, value, pspec, &mpeg4->baseobj);
	return;
}


static void
gst_vmetampeg4enc_finalize(GObject * object)
{
	Gstvmetampeg4enc* mpeg4 = (Gstvmetampeg4enc*)object;
	_vmetaencshare_onfinalize(&mpeg4->baseobj);

	G_OBJECT_CLASS(parent_class)->finalize(object);

#if 0
	assist_myecho("Gstvmetampeg4enc instance(%p) is finalized!!!\n", object);
#else
	printf("Gstvmetampeg4enc instance(%p) is finalized!!!\n", object);
#endif
	return;
}


static void
gst_vmetampeg4enc_base_init(gpointer g_klass)
{
	_vmetaencshare_base_init(g_klass, IPP_VIDEO_STRM_FMT_MPG4);
	return;
}

static void
gst_vmetampeg4enc_class_init(Gstvmetampeg4encClass* klass)
{
	GObjectClass *gobject_class  = (GObjectClass*) klass;
	GstElementClass *gstelement_class = (GstElementClass*) klass;

	gobject_class->set_property = gst_vmetampeg4enc_set_property;
	gobject_class->get_property = gst_vmetampeg4enc_get_property;

	_vmetaencshare_class_install_property(gobject_class);

	gobject_class->finalize = gst_vmetampeg4enc_finalize;
	gstelement_class->change_state = GST_DEBUG_FUNCPTR(gst_vmetampeg4enc_change_state);

	GST_DEBUG_CATEGORY_INIT(vmetaenc_mpeg4_debug, "vmetaenc_mpeg4", 0, "vMeta Encode mpeg4 Element");

	return;
}


static GstFlowReturn
gst_vmetampeg4enc_sinkpad_allocbuf(GstPad *pad, guint64 offset, guint size, GstCaps *caps, GstBuffer **buf)
{
	assist_myechomore("call gst_vmetaenc_sinkpad_allocbuf(), pad(0x%x), offset(%lld), size(%d)\n",(unsigned int)pad, offset, size);
	Gstvmetampeg4enc* mpeg4 = GST_VMETAMPEG4ENC(GST_PAD_PARENT(pad));
	return _vmetaencshare_sinkpad_allocbuf(pad, offset, size, caps, buf, &mpeg4->baseobj);
}


static gboolean
gst_vmetampeg4enc_sinkpad_event(GstPad* pad, GstEvent* event)
{
	Gstvmetampeg4enc* mpeg4 = GST_VMETAMPEG4ENC(GST_PAD_PARENT(pad));
	return _vmetaencshare_sinkpad_event(pad, event, &mpeg4->baseobj);
}

static void
gst_vmetampeg4enc_init(Gstvmetampeg4enc* mpeg4, Gstvmetampeg4encClass* venc_klass)
{
	GstElementClass *klass = GST_ELEMENT_CLASS (venc_klass);

	mpeg4->sinkpad = gst_pad_new_from_template(gst_element_class_get_pad_template (klass, "sink"), "sink");

	gst_pad_set_setcaps_function(mpeg4->sinkpad, GST_DEBUG_FUNCPTR(gst_vmetampeg4enc_sinkpad_setcaps));
	gst_pad_set_chain_function(mpeg4->sinkpad, GST_DEBUG_FUNCPTR(gst_vmetampeg4enc_chain));
	gst_pad_set_event_function(mpeg4->sinkpad, GST_DEBUG_FUNCPTR(gst_vmetampeg4enc_sinkpad_event));
	gst_pad_set_bufferalloc_function(mpeg4->sinkpad, GST_DEBUG_FUNCPTR(gst_vmetampeg4enc_sinkpad_allocbuf));

	gst_element_add_pad (GST_ELEMENT(mpeg4), mpeg4->sinkpad);

	mpeg4->srcpad = gst_pad_new_from_template(gst_element_class_get_pad_template (klass, "src"), "src");

	gst_element_add_pad(GST_ELEMENT(mpeg4), mpeg4->srcpad);

	_vmetaencshare_init_members(&mpeg4->baseobj, IPP_VIDEO_STRM_FMT_MPG4, gst_vmetampeg4enc_echofun, mpeg4->sinkpad, mpeg4->srcpad, mpeg4);

#if 0
	assist_myecho("Gstvmetampeg4enc instance(%p) is inited!!!\n", mpeg4);
#else
	printf("Gstvmetampeg4enc instance(%p) is inited!!!\n", mpeg4);
#endif
	return;
}

