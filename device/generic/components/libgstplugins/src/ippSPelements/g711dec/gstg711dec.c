/*
 * GStreamer
 * Copyright (C) 2005 Thomas Vander Stichele <thomas@apestaart.org>
 * Copyright (C) 2005 Ronald S. Bultje <rbultje@ronald.bitfreak.net>
 * Copyright (C) 2010 root <<user@hostname.org>>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:element-g711dec
 *
 * FIXME:Describe g711dec here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! g711dec ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gst/gst.h>
#ifdef DEBUG_PERFORMANCE
#include  <sys/time.h>
#include <time.h>
#endif
#include <string.h> 
//#include <strings.h>	//to include strcasecmp()
#include "gstg711dec.h"

GST_DEBUG_CATEGORY_STATIC (gst_g711dec_debug);
#define GST_CAT_DEFAULT gst_g711dec_debug

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  ARG_0,
  ARG_STARTTIME,
#ifdef DEBUG_PERFORMANCE
  ARG_TOTALFRAME,
  ARG_CODECTIME,
  #endif
};

#define G711_BLOCK 256
#if 0
#define assist_myecho(...)	printf(__VA_ARGS__)
#else
#define assist_myecho(...)
#endif

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-alaw,"
    					"channels = (int) [1,2],"
    					"rate = (int) [4000,192000];"
    					"audio/x-mulaw,"
    					"channels = (int) [1,2],"
    					"rate = (int) [4000,192000]")
    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-raw-int,"
	"endianness = (int) "G_STRINGIFY (G_BYTE_ORDER)", "
	"signed = (boolean) TRUE, "
	"width = (int) 16, "
	"depth = (int) 16, "
	"rate = (int) [4000,192000], "
	"channels = (int) [1,2]")
    );

GST_BOILERPLATE (Gstg711dec, gst_g711dec, GstElement,
    GST_TYPE_ELEMENT);

static void gst_g711dec_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_g711dec_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static gboolean gst_g711dec_sinkpad_set_caps (GstPad * pad, GstCaps * caps);
static GstFlowReturn gst_g711dec_chain (GstPad * pad, GstBuffer * buf);

static inline void
g711dec_init_members(Gstg711dec *g711)
{
		//g711->ts = GST_CLOCK_TIME_NONE;
		g711->startts = GST_CLOCK_TIME_NONE;
		//g711->channels = 1;
		//g711->rate = 8000;
#ifdef DEBUG_PERFORMANCE
		g711->totalframes = 0;
		g711->codec_time = 0;
#endif
}

static gboolean
gst_g711dec_null2ready(Gstg711dec *g711)
{
	return TRUE;
}

static gboolean
gst_g711dec_ready2null(Gstg711dec *g711)
{


#ifdef DEBUG_PERFORMANCE
	printf("codec system time %lld musec, frame number %d\n", g711->codec_time, g711->totalframes);
#endif
	return TRUE;
}

static GstStateChangeReturn 
gst_g711dec_change_state(GstElement *element, GstStateChange transition)
{
	GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
	Gstg711dec *g711 = GST_G711DEC (element);
	
        GST_LOG_OBJECT(g711,"GST g711dec state change trans from %d to %d\n", transition>>3, transition&7);

	switch (transition)
	{
	case GST_STATE_CHANGE_NULL_TO_READY:
		GST_LOG_OBJECT(g711,"GST g711dec state change: NULL to Ready\n");
		if(!gst_g711dec_null2ready(g711)){
			goto G711GST_STATECHANGE_ERR;
		}
		break;
	case GST_STATE_CHANGE_READY_TO_PAUSED:
		
		break;
	default:
		break;
	}
	ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
	if (ret == GST_STATE_CHANGE_FAILURE) {
		g_warning("GST g711dec parent class state change fail!");
		return ret;
	}
	
	switch (transition)
	{
	case GST_STATE_CHANGE_PAUSED_TO_READY:
		
		break;
	case GST_STATE_CHANGE_READY_TO_NULL:
		GST_LOG_OBJECT(g711,"GST g711dec state change: Ready to NULL\n");
		if(!gst_g711dec_ready2null(g711)){
			goto G711GST_STATECHANGE_ERR;
		}
		break;
	default:
		break;
	}

	return ret;

	 /* ERRORS */
G711GST_STATECHANGE_ERR:
		 {
			 g_warning("g711dec state change failed");
			 /* subclass must post a meaningfull error message */
			 GST_ERROR_OBJECT (g711, "state change failed");
			 return GST_STATE_CHANGE_FAILURE;
		 }

}


/* GObject vmethod implementations */

static void
gst_g711dec_base_init (gpointer gclass)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (gclass);

  gst_element_class_set_details_simple(element_class,
    "G.711 audio decoder",
    "Codec/Decoder/Speech",
    "G.711 Alaw&Mulaw audio decoder based on IPP codecs",
    "");

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_factory));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_factory));
}

/* initialize the g711dec's class */
static void
gst_g711dec_class_init (Gstg711decClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  gobject_class->set_property = gst_g711dec_set_property;
  gobject_class->get_property = gst_g711dec_get_property;

  
  g_object_class_install_property (gobject_class, ARG_STARTTIME,
	  g_param_spec_uint64 ("starttime", "start time (nanosecond)",
	  "start timestamp for the stream(nanosecond)", 0, G_MAXUINT64, 0, G_PARAM_READWRITE));
#ifdef DEBUG_PERFORMANCE
	  g_object_class_install_property (gobject_class, ARG_TOTALFRAME,
		  g_param_spec_int ("totalframes", "Number of frame",
		  "Number of total decoded frames for all tracks", 0, G_MAXINT, 0, G_PARAM_READABLE));
	  g_object_class_install_property (gobject_class, ARG_CODECTIME,
		  g_param_spec_int64 ("codectime", "codec time (microsecond)",
		  "Total pure decoding spend system time for all tracks (microsecond)", 0, G_MAXINT64, 0, G_PARAM_READABLE));
#endif
  
  gstelement_class->change_state = GST_DEBUG_FUNCPTR (gst_g711dec_change_state);

  GST_DEBUG_CATEGORY_INIT (gst_g711dec_debug, "g711dec", 0, "G.711 audio decoder");

}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
static void
gst_g711dec_init (Gstg711dec * g711,
    Gstg711decClass * gclass)
{
  g711->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
  gst_pad_set_setcaps_function (g711->sinkpad,
                                GST_DEBUG_FUNCPTR(gst_g711dec_sinkpad_set_caps));
  gst_pad_set_chain_function (g711->sinkpad,
                              GST_DEBUG_FUNCPTR(gst_g711dec_chain));

  g711->srcpad = gst_pad_new_from_static_template (&src_factory, "src");

  gst_element_add_pad (GST_ELEMENT (g711), g711->sinkpad);
  gst_element_add_pad (GST_ELEMENT (g711), g711->srcpad);

  g711dec_init_members(g711);

}

static void
gst_g711dec_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  Gstg711dec *g711 = GST_G711DEC (object);

  switch (prop_id) {
  	case ARG_STARTTIME:
 	 {
  		g711->startts=g_value_get_uint64(value);
  		//g711->ts = g711->startts;
  		break;
 	 }
    	default:
      		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      		break;
  }
}

static void
gst_g711dec_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  Gstg711dec *g711 = GST_G711DEC (object);

  switch (prop_id) {
	case ARG_STARTTIME:
	{
		g_value_set_uint64(value,g711->startts);
		break;
	}
#ifdef DEBUG_PERFORMANCE
	case ARG_TOTALFRAME:
			g_value_set_int(value, g711->totalframes);
			break;
	case ARG_CODECTIME:
			g_value_set_int64(value, g711->codec_time);
			break;
#endif
      	default:
      		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      		break;
  }
}

/* GstElement vmethod implementations */

/* this function handles the link with other elements */
static gboolean
gst_g711dec_sinkpad_set_caps (GstPad * pad, GstCaps * caps)
{
  Gstg711dec *g711;
  //GstPad *otherpad;
  GstStructure *str;
  GstCaps *Tmp;
  //gchar *name;
  const gchar *name;//, *strings;
  int i = 0;
  //const GValue *value;
  int stru_num = gst_caps_get_size (caps);
    
  g711 = GST_G711DEC (gst_pad_get_parent (pad));
  
  if(stru_num != 1) {
	  gchar* sstr;
	  g_warning("Multiple MIME stream type in sinkpad caps, only select the first item.");
	  for(i=0; i<stru_num; i++) {
		  str = gst_caps_get_structure(caps, i);
		  sstr = gst_structure_to_string(str);
		  g_warning("struture %d is %s.", i, sstr);
		  g_free(sstr);
	  }
  }
  str = gst_caps_get_structure (caps, 0);
  name = gst_structure_get_name (str);
  //strings = gst_structure_to_string (str);
  if(strcmp(name, "audio/x-alaw") == 0) {
  	g711->LawUsed = IPP_ALAW;
  }
  else if(strcmp(name, "audio/x-mulaw") == 0){
  	g711->LawUsed = IPP_ULAW;
  }
  else{
	  g_warning("Unsupported stream MIME type %s.", name);
	  return FALSE;
  }
  /* get channel count */
  if(G_UNLIKELY(FALSE == gst_structure_get_int(str, "channels", &g711->channels))) {
	  g_warning("no channels in caps.");
	  return FALSE;
	  //g711->channels = 1;	  //if this information isn't in caps, we assume it is 0
  }
  if(G_UNLIKELY(FALSE == gst_structure_get_int(str, "rate", &g711->rate))) {
	  g_warning("no rate in caps.");
	  return FALSE;
	  //g711->rate = 8000;	  //if this information isn't in caps, we assume it is 0
  }  
  //g711->duration = gst_util_uint64_scale_int (GST_SECOND, G711_BLOCK, g711->rate * g711->channels);
  
  Tmp = gst_caps_new_simple ("audio/x-raw-int", 
	  "endianness", G_TYPE_INT, G_BYTE_ORDER, 
	  "signed", G_TYPE_BOOLEAN, TRUE, 
	  "width", G_TYPE_INT, 16, 
	  "depth", G_TYPE_INT, 16, 
	  "rate", G_TYPE_INT, g711->rate,
	  "channels", G_TYPE_INT, g711->channels,
	  NULL);
  if(TRUE != gst_pad_set_caps(g711->srcpad, Tmp)) {
	  gchar* cap_string = gst_caps_to_string(Tmp);
	  g_warning("set g711dec src caps fail, caps is %s",cap_string);
	  g_free(cap_string);
  }
  gst_caps_unref(Tmp);

  return TRUE;
}

/*static GstFlowReturn
G711_push_data(Gstg711dec *g711, Ipp16s* pcmdata)
{
        GstFlowReturn ret = GST_FLOW_OK;
	GstBuffer *down_buf = NULL;
	
	ret = gst_pad_alloc_buffer_and_set_caps(g711->srcpad, 0, G711_BLOCK*sizeof(Ipp16s), GST_PAD_CAPS(g711->srcpad), &down_buf);
	if(ret != GST_FLOW_OK) {
		return ret;								
	}

	GST_BUFFER_TIMESTAMP(down_buf) = g711->ts;
	g711->ts += g711->duration;
	
	ret = gst_pad_push(g711->srcpad, down_buf);
	return ret;
}*/

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_g711dec_chain (GstPad * pad, GstBuffer * buf)
{
	  Gstg711dec *g711;
	  //guint8 uFirstByte = 0;
	  int ret = 0;
	  int left = 0;
	  int pcmsize;
	 // int offset = 0;
	  guint8 *data = NULL;
	  Ipp16s  *pPcmCode;
	  GstBuffer *down_buf = NULL;
	  GstClockTime ts;
	  GstClockTime dur;
#ifdef DEBUG_PERFORMANCE
	  struct timeval clk0, clk1;
#endif
	  //Ipp8u   g711Input[G711_BLOCK];  //a A/U law code

	  g711 = GST_G711DEC (GST_OBJECT_PARENT (pad));
  
	  //if (GST_BUFFER_TIMESTAMP_IS_VALID (buf)){
		//g711->ts = GST_BUFFER_TIMESTAMP (buf);
	 // }
  	  ts = GST_BUFFER_TIMESTAMP (buf);
	  dur = GST_BUFFER_DURATION(buf);
	  
	  left = GST_BUFFER_SIZE (buf);
	  data = GST_BUFFER_DATA (buf); 
  
	  if(left > 0){
	  	  pcmsize = left*2;
		  ret = gst_pad_alloc_buffer_and_set_caps(g711->srcpad, 0, pcmsize, GST_PAD_CAPS(g711->srcpad), &down_buf);
 	  	  if(ret != GST_FLOW_OK) {
	  	  	goto err;
 	  	  }
   		pPcmCode = (Ipp16s *)GST_BUFFER_DATA(down_buf);
#ifdef DEBUG_PERFORMANCE
		gettimeofday(&clk0, NULL);
#endif

		ret = Decode_G711(data,left,g711->LawUsed,pPcmCode);

#ifdef DEBUG_PERFORMANCE
		gettimeofday(&clk1, NULL);
		g711->codec_time += (clk1.tv_sec - clk0.tv_sec)*1000000 + (clk1.tv_usec - clk0.tv_usec);
#endif
   		if(IPP_STATUS_NOERR != ret){

			GST_ERROR_OBJECT(g711, "G.711 decoder error");
			ret = GST_FLOW_ERROR;
			goto err;
   		}
		  
		  GST_BUFFER_TIMESTAMP(down_buf) = ts;
		  if(dur == GST_CLOCK_TIME_NONE){
			dur = gst_util_uint64_scale_int (GST_SECOND, left, g711->rate * g711->channels);;
		  }
		  GST_BUFFER_DURATION(down_buf) = dur;
		  
		  ret = gst_pad_push(g711->srcpad, down_buf);
		  
		  if(GST_FLOW_OK != ret){	
			  goto err;
		  }   
		  
		  g711->totalframes ++;
	  }
	  
	  gst_buffer_unref(buf);
	  
	  return GST_FLOW_OK;
	  
  err:
	  gst_buffer_unref(buf);
	  if(ret < GST_FLOW_UNEXPECTED) {
		GST_ELEMENT_ERROR(g711, STREAM, DECODE, (NULL), ("%s() return un-accepted error %d", __FUNCTION__, ret));
	  }
	  
	  return ret;

}


/* entry point to initialize the plug-in
 * initialize the plug-in itself
 * register the element factories and other features
 */
static gboolean
plugin_init (GstPlugin * plugin)
{
  /* debug category for fltering log messages
   *
   * exchange the string 'Template g711dec' with your description
   */
  return gst_element_register (plugin, "g711dec", GST_RANK_PRIMARY+2,
      GST_TYPE_G711DEC);
}

/* gstreamer looks for this structure to register g711decs
 *
 * exchange the string 'Template g711dec' with your g711dec description
 */
GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    "mvl_g711dec",
    "G.711 ALaw&Mulaw audio decoder based on IPP codec"__DATE__,
    plugin_init,
    VERSION,
    "LGPL",
    "",
    ""
)
