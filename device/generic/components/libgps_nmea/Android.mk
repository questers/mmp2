LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

ifeq ($(strip $(BOARD_GPS_SUPPLIER)),GPS_NMEA)

LOCAL_CFLAGS += -DHAVE_GPS_HARDWARE -DGNS_DEVICES=\"/dev/ttyS3\"

LOCAL_SRC_FILES := \
	gpshal.c
	
LOCAL_MODULE := gps.marvell
LOCAL_MODULE_TAGS := optional

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	hardware\libhardware_legacy


LOCAL_SHARED_LIBRARIES := \
	libc \
	libutils \
	libcutils \
	liblog

LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw

include $(BUILD_SHARED_LIBRARY)

endif
