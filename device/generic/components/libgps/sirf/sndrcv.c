/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * sndrcv.c                                                                *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * DESCRIPTION:                                                            *
 *                                                                         *
 * Functions in this module send packets to and receive packets from the   *
 * target.                                                                 *
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#include "SiRFflashEngineEP.h"
#include "global.h"
#include "chksum.h"
#include "commio.h"
#include "pckunpck.h"
#include "sndrcv.h"

//SiRFflashEngineEP_UINT8 PacketBuff[1660 + 14];
SiRFflashEngineEP_UINT8 *PacketBuff;


/*
 * Function : SendPacket
 * Purpose  : Form a packet by adding leading 2-byte length and trailing
 *            one byte checksum, and receive positive response (ACK) or
 *            negative response (NAK)
 */

SiRFflashEngineEP_INT32
SendPacket (SiRFflashEngineEP_UINT8 * p,
			SiRFflashEngineEP_UINT32 BytesToWrite)
{
  SiRFflashEngineEP_UINT32 BytesWritten;
  SiRFflashEngineEP_UINT32 BytesRead;
  SiRFflashEngineEP_UINT8 Response;
  SiRFflashEngineEP_UINT8 *src = p + BytesToWrite;
  SiRFflashEngineEP_UINT8 *dst = p + 2 + BytesToWrite;
  SiRFflashEngineEP_UINT32 n = BytesToWrite;

  while (n--)
    *--dst = *--src;

  *p = (SiRFflashEngineEP_UINT8) (BytesToWrite >> 8);
  *(p + 1) = (SiRFflashEngineEP_UINT8) BytesToWrite;

  *(p + BytesToWrite + 2) = CalculateChecksum (p, BytesToWrite + 2);
  BytesToWrite += 3;

  /* Send packet out */

  if (CommWrite (p, BytesToWrite, &BytesWritten) != SUCCESS)
    goto send_fail;
  if (BytesToWrite != BytesWritten)
    goto send_fail;

  /* Receive response */

  if (CommRead (&Response, 1, &BytesRead) != SUCCESS)
    goto ack_fail;
  if (BytesRead != 1)
    goto ack_fail;
  if (Response == ACK)
    goto send_success;
  else
    goto ack_fail;

ack_fail:
  return FAILURE;
send_fail:
  return FAILURE;
send_success:
  return SUCCESS;
}

/* 
 * Function: ReceivePacket
 * Purpose : Receive a packet and if checksum is ok send ACK, otherwise NAK. 
 */

SiRFflashEngineEP_INT32
ReceivePacket (SiRFflashEngineEP_UINT8 * p, SiRFflashEngineEP_UINT32 * n)
{
  SiRFflashEngineEP_UINT32 BytesRead;
  SiRFflashEngineEP_UINT32 BytesWritten;
  SiRFflashEngineEP_UINT32 ByteLen;
  SiRFflashEngineEP_UINT8 Response;
  SiRFflashEngineEP_UINT8 LenChksum;
  SiRFflashEngineEP_UINT8 *q = p;

  /* Get packet length */

  if (CommRead (p, 2, &BytesRead) != SUCCESS)
    goto receive_length_fail;
  if (BytesRead != 2)
    goto receive_length_fail;

  ByteLen = UnpackWord (&q) + 1;

  LenChksum = CalculateChecksum (p, 2);

  /* Get the rest of packet */

  if (CommRead (p, ByteLen, &BytesRead) != SUCCESS)
    goto receive_data_fail;
  if (BytesRead != ByteLen)
    goto receive_data_fail;

  /* Check if sum of all elements in packet is zero */

  if (((LenChksum + CalculateChecksum (p, ByteLen)) & 0xff) != 0)
    Response = NAK;
  else
    Response = ACK;

  /* Send response */

  if (CommWrite (&Response, 1, &BytesWritten) != SUCCESS)
    goto ack_fail;
  if (BytesWritten != 1)
    goto ack_fail;

  *n = ByteLen - 1;

  if (Response != ACK)
    goto ack_fail;
  else
    goto receive_success;

ack_fail:
  return FAILURE;
receive_length_fail:
  return FAILURE;
receive_data_fail:
  return FAILURE;
receive_success:
  return SUCCESS;
}
