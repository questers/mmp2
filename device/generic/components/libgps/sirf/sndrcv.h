/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * sndrcv.h                                                                *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#ifndef __SNDRCV_H
#define __SNDRCV_H

#define ACK		6
#define NAK		21

#define MAX_PACKET_SIZE                1024

//extern SiRFflashEngineEP_UINT8 PacketBuff[1660 + 14];
extern SiRFflashEngineEP_UINT8* PacketBuff;


extern SiRFflashEngineEP_INT32 SendPacket (SiRFflashEngineEP_UINT8 * p, SiRFflashEngineEP_UINT32 len);
extern SiRFflashEngineEP_INT32 ReceivePacket (SiRFflashEngineEP_UINT8 * p, SiRFflashEngineEP_UINT32 * len);

#endif
