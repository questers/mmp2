/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * chksum.c                                                                *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * DESCRIPTION:                                                            *
 *                                                                         *
 * Function in this module performs checksum calculation.                  *
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#include "SiRFflashEngineEP.h"
#include "global.h"
#include "chksum.h"

/*
 * Function : CalculateChecksum
 * Purpose  : Calculate checksum for given array of bytes.
 */

SiRFflashEngineEP_UINT8
CalculateChecksum (SiRFflashEngineEP_UINT8 * p, SiRFflashEngineEP_INT32 n)
{
  SiRFflashEngineEP_INT32 c = 0;

  while (n--)
    c += *p++;
  return (SiRFflashEngineEP_UINT8)(-c);
}
