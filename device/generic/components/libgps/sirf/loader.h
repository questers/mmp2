/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * loader.h                                                                *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#ifndef __LOADER_H
#define __LOADER_H

extern SiRFflashEngineEP_INT32 LoadStarter (const SiRFflashEngineEP_UINT8 *BufferPtr,
											SiRFflashEngineEP_UINT32 BufferSize);
extern SiRFflashEngineEP_INT32 LoadBurner (const SiRFflashEngineEP_UINT8 *BufferPtr,
										   SiRFflashEngineEP_UINT32 BufferSize);

#endif
