#ifndef _IMGACC_H_
#define _IMGACC_H_

extern SiRFflashEngineEP_INT32 BinaryImageOpen(SiRFflashEngineEP_CHAR *FileName,
											   SiRFflashEngineEP_UINT32 *BinaryImageDataSizePtr);
extern SiRFflashEngineEP_INT32 BinaryImageReadNextBlock(SiRFflashEngineEP_UINT8 *DataPtr,
														SiRFflashEngineEP_UINT32 Size);
extern SiRFflashEngineEP_INT32 BinaryImageClose(void);

#endif
