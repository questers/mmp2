/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *     (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved        *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * commio.h                                                                *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#ifndef __COMMIO_H
#define __COMMIO_H

extern SiRFflashEngineEP_INT32 CommOpen (SiRFflashEngineEP_CHAR *portname,
					                     SiRFflashEngineEP_UINT32 baudrate);
extern SiRFflashEngineEP_INT32 CommChangeBaudRate(SiRFflashEngineEP_UINT32 baudrate);
extern SiRFflashEngineEP_INT32 CommRead (SiRFflashEngineEP_UINT8 *p,
										 SiRFflashEngineEP_UINT32 requested,
										 SiRFflashEngineEP_UINT32 *actual);
extern SiRFflashEngineEP_INT32 CommWrite (SiRFflashEngineEP_UINT8 *p,
										  SiRFflashEngineEP_UINT32 requested,
										  SiRFflashEngineEP_UINT32 *actual);
extern void                    CommClose (void);
extern void                    CommSetBridgePresent (SiRFflashEngineEP_BOOL YesNo);
extern SiRFflashEngineEP_BOOL  CommGetBridgePresent(void);

#endif

