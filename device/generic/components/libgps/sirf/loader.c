/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * loader.c                                                                *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * DESCRIPTION:                                                            *
 *                                                                         *
 * Functions in this module load starter and burner programs to the target *
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "SiRFflashEngineEP.h"
#include "global.h"
#include "commio.h"
#include "cmdcodes.h"
#include "sndrcv.h"
#include "chksum.h"
#include "loader.h"

/*
 * Function: LoadStarter
 * Purpose:  Load starter program to the target
 */

SiRFflashEngineEP_INT32
LoadStarter (const SiRFflashEngineEP_UINT8 *BufferPtr,
			 SiRFflashEngineEP_UINT32 BufferSize)
{

  SiRFflashEngineEP_UINT8 *p = &PacketBuff[0];
  SiRFflashEngineEP_UINT32 ResponseSize;

  *p = LOAD_STARTER;

  /* Send information about code size */

  *(p + 1) = 'S';
  *(p + 2) = 0;
  *(p + 3) = (SiRFflashEngineEP_UINT8) (BufferSize >> 24);
  *(p + 4) = (SiRFflashEngineEP_UINT8) (BufferSize >> 16);
  *(p + 5) = (SiRFflashEngineEP_UINT8) (BufferSize >> 8);
  *(p + 6) = (SiRFflashEngineEP_UINT8) BufferSize;

  memcpy (p + 7, BufferPtr, BufferSize);

  *(p + BufferSize + 7) = 0;
  *(p + BufferSize + 8) = 0;
  *(p + BufferSize + 9) = 0;
  *(p + BufferSize + 10) = 0;
  if (SendPacket (p, BufferSize + 11) == FAILURE)
    return FAILURE;

  if (ReceivePacket (&PacketBuff[0], &ResponseSize) == FAILURE)
    return FAILURE;
  if ((ResponseSize != 2) || (PacketBuff[0] != CONFIRM_LOAD_STARTER))
    return FAILURE;
  if (PacketBuff[1] != CalculateChecksum ((SiRFflashEngineEP_UINT8 *)BufferPtr, BufferSize))
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: LoadBurner
 * Purpose:  Load burner program to the target
 */

SiRFflashEngineEP_INT32
LoadBurner (const SiRFflashEngineEP_UINT8 *BufferPtr,
			SiRFflashEngineEP_UINT32 BufferSize)
{
  SiRFflashEngineEP_UINT32 BytesTransfered = 0;
  SiRFflashEngineEP_UINT32 BytesToTransfer;
  SiRFflashEngineEP_UINT32 ResponseSize;

  /* Transfer program code */

  while (BytesTransfered < BufferSize)
  {

      BytesToTransfer = (BufferSize - BytesTransfered) < MAX_PACKET_SIZE ?
						(BufferSize - BytesTransfered) : MAX_PACKET_SIZE;

      PacketBuff[0] = LOAD_RAM;
      PacketBuff[1] = (SiRFflashEngineEP_UINT8) (BytesTransfered >> 24);
      PacketBuff[2] = (SiRFflashEngineEP_UINT8) (BytesTransfered >> 16);
      PacketBuff[3] = (SiRFflashEngineEP_UINT8) (BytesTransfered >> 8);
      PacketBuff[4] = (SiRFflashEngineEP_UINT8) BytesTransfered;
      memcpy (&PacketBuff[5], BufferPtr + BytesTransfered, BytesToTransfer);
      if (SendPacket (&PacketBuff[0], BytesToTransfer + 5) == FAILURE)
		return FAILURE;
	  
      if (ReceivePacket (&PacketBuff[0], &ResponseSize) == FAILURE)
		return FAILURE;
      if (PacketBuff[0] != CONFIRM_LOAD_RAM)
		return FAILURE;
      BytesTransfered += BytesToTransfer;
  }

  /* Request start of the program */

  PacketBuff[0] = START_PROGRAM;
  PacketBuff[1] = 0;
  PacketBuff[2] = 0;
  PacketBuff[3] = 0;
  PacketBuff[4] = 0;
  
  if (SendPacket (&PacketBuff[0], 5) == FAILURE)
    return FAILURE;
  if (ReceivePacket (&PacketBuff[0], &ResponseSize) == FAILURE)
    return FAILURE;
  if (PacketBuff[0] != CONFIRM_START_PROGRAM)
    return FAILURE;

  return SUCCESS;
}
