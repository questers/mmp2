/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *     (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved        *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * commio.c                                                                *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * DESCRIPTION:                                                            *
 *                                                                         *
 * Functions in this module perform initialization, reading, writing, and  *
 * closing of the serial port that is used for communication between       *
 * Linux PC host and GSP2/GSP3 based target.                               *
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, February 24, 2005, Voya Protic (vprotic@sirf.com)          *
 *                                                                         *
 ***************************************************************************/

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <stdio.h>
#include "SiRFflashEngineEP.h"
#include "global.h"
#include "commio.h"

static int fd, c;
static struct termios oldtio, newtio;

static SiRFflashEngineEP_BOOL CommBridgeIsPresent = 0;
static SiRFflashEngineEP_UINT32 LastWriteSize = 1;
static char PortName[16];

/*
 * Function: CommOpen
 * Purpose:  Open communication channel
 */

SiRFflashEngineEP_INT32
CommOpen (SiRFflashEngineEP_CHAR *portname, SiRFflashEngineEP_UINT32 baudrate) 
{
  int BaudRate;

  if (baudrate == 38400)
    BaudRate = B38400;
  else if (baudrate == 57600)
    BaudRate = B57600;
  else if (baudrate == 115200)
    BaudRate = B115200;
  else if (baudrate == 230400)
  	BaudRate = B230400;
  else if (baudrate == 460800)
	BaudRate = B460800;
  else if (baudrate == 921600)
  	BaudRate = B921600;  
  else
    return FAILURE;


  if(PortName!=portname)
  	strcpy(PortName, portname);

  fd = open (portname, O_RDWR | O_NOCTTY);

  if (fd < 0)
    return FAILURE;

  tcgetattr (fd, &oldtio);	/* save current port settings */
  bzero (&newtio, sizeof (newtio));
  newtio.c_cflag = BaudRate | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR;
  newtio.c_oflag = 0;
  
    /* set input mode (non-canonical, no echo,...) */

  newtio.c_lflag = 0;
  newtio.c_cc[VTIME] = 20;	/* wait 20 deciseconds to receive a character */
  newtio.c_cc[VMIN] = 0;	/* no minimum number of characters */
  tcflush (fd, TCIOFLUSH);
  tcsetattr (fd, TCSANOW, &newtio);
  return SUCCESS;
}

/*
 * Function: CommChangeBaudRate
 * Purpose:  Change baud rate on the communication channel
 *           by closing and opening the port at new baud rate
 */

SiRFflashEngineEP_INT32
CommChangeBaudRate(SiRFflashEngineEP_UINT32 baudrate)
{
  CommClose();
  return CommOpen(PortName, baudrate);
}

/*
 * Function: CommSetBridgePresent
 * Purpose:  Set if communication bridge is present or not.
 */

void
CommSetBridgePresent (SiRFflashEngineEP_BOOL YesNo)
{
	CommBridgeIsPresent = YesNo;
}

/*
 * Function: CommGetBridgePresent
 * Purpose:  Get value of CommBridgePresent variable
 */

SiRFflashEngineEP_BOOL 
CommGetBridgePresent (void)
{
	return CommBridgeIsPresent;
}
/*
 * Function: CommRead
 * Purpose: Read specified number of characters from communication channel
 */ 

SiRFflashEngineEP_INT32
CommRead (SiRFflashEngineEP_UINT8 *p, SiRFflashEngineEP_UINT32 requested, SiRFflashEngineEP_UINT32 *actual) 
{
  *actual = read (fd, p, requested);
  return SUCCESS;
}


/*
 * Function: CommWrite
 * Purpose: Write specified number of characters to communication channel
 */ 

SiRFflashEngineEP_INT32
CommWrite (SiRFflashEngineEP_UINT8 *p, SiRFflashEngineEP_UINT32 requested, SiRFflashEngineEP_UINT32 *actual) 
{
  LastWriteSize = requested;
  write (fd, p, requested);
  *actual = requested;
  return SUCCESS;
}


/*
 * Function: CommClose
 * Purpose: Close communication channel
 */ 

void
CommClose (void) 
{
  tcflush (fd, TCIOFLUSH);
  tcsetattr (fd, TCSANOW, &oldtio);
  close (fd);
}
