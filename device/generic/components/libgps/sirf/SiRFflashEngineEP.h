/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * SiRFflashEngineEP.h                                                     *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#ifndef __SIRFFLASHENGINEEP_H
#define __SIRFFLASHENGINEEP_H

/* SiRFflashEngineEP TYPES */

typedef char SiRFflashEngineEP_CHAR;
typedef unsigned long SiRFflashEngineEP_BOOL;
typedef signed char SiRFflashEngineEP_INT8;
typedef unsigned char SiRFflashEngineEP_UINT8;
typedef signed short SiRFflashEngineEP_INT16;
typedef unsigned short SiRFflashEngineEP_UINT16;
typedef signed long SiRFflashEngineEP_INT32;
typedef unsigned long SiRFflashEngineEP_UINT32;

typedef struct
{
   SiRFflashEngineEP_CHAR   *DeviceName;
   SiRFflashEngineEP_CHAR   *ManufacturerName;
} SiRFflashEngineEP_FLASH_NAME;

typedef struct
{
   SiRFflashEngineEP_UINT16   DeviceCode;
   SiRFflashEngineEP_UINT16   ManufacturerCode;
} SiRFflashEngineEP_FLASH_ID;

typedef   struct
{
   SiRFflashEngineEP_UINT32   NoSectors;
   SiRFflashEngineEP_UINT32   SectorSize;
} SiRFflashEngineEP_SECTOR_GROUP;

typedef struct
{
   SiRFflashEngineEP_FLASH_NAME      Fname;
   SiRFflashEngineEP_FLASH_ID        Fid;
   SiRFflashEngineEP_UINT32          Capacity;
   SiRFflashEngineEP_SECTOR_GROUP    *SectorGroupPtr;
   SiRFflashEngineEP_UINT8           NoSectorGroups;
   SiRFflashEngineEP_CHAR            Class;
   SiRFflashEngineEP_UINT16          ExtraData;
} SiRFflashEngineEP_FLASH_DESC;

/* CODES FOR FLASH CHIP SPECIFIC FEATURES */

#define ERASE_SUSPEND_MASK				1
#define ERASE_SUSPEND_SUPPORTED			1
#define ERASE_SUSPEND_NOT_SUPPORTED		0

#define SOFT_LOCK_UNLOCK_MASK			2
#define SOFT_LOCK_UNLOCK_SUPPORTED		2
#define SOFT_LOCK_UNLOCK_NOT_SUPPORTED	0

#define ERROR_BITS_MASK					4
#define ERROR_BITS_SUPPORTED			0
#define ERROR_BITS_NOT_SUPPORTED		4

#define SHARP_FLAVOR_MASK				8
#define SHARP_FLAVOR_ENABLED			8
#define SHARP_FLAVOR_DISABLED			0

/* SiRFflashEngineEP ERROR CODES */

typedef enum
{
   SiRFflashEngineEP_OK = 0,
   SiRFflashEngineEP_FLASH_ALREADY_OPENED = 1,
   SiRFflashEngineEP_BAD_CHIP_SELECT = 2,
   SiRFflashEngineEP_ERASE_SECTOR_SET_ERROR = 3,
   SiRFflashEngineEP_FLASH_WRITE_ERROR = 4,
   SiRFflashEngineEP_FLASH_NOT_FOUND = 5,
   SiRFflashEngineEP_FLASH_NOT_OPENED = 6,
   SiRFflashEngineEP_UNKNOWN_FLASH_TYPE = 7,
   SiRFflashEngineEP_INVALID_OFFSET = 8,
   SiRFflashEngineEP_INVALID_ERASE_SIZE = 9,
   SiRFflashEngineEP_BUS_WIDTH_NOT_16_OR_32 = 10,
   SiRFflashEngineEP_CAN_NOT_REPROGRAM = 11,
   SiRFflashEngineEP_AREA_NOT_BLANK = 12,
   SiRFflashEngineEP_INVALID_SECTOR_SET_NUMBER = 13,
   SiRFflashEngineEP_UNSUPPORTED_GSP2_VERSION = 16,
   SiRFflashEngineEP_INVALID_FLASH_DESCRIPTOR_POINTER = 17,
   SiRFflashEngineEP_INVALID_FLASH_DESCRIPTOR = 18,
   SiRFflashEngineEP_SPECIFIED_FLASH_NOT_FOUND = 19,
   SiRFflashEngineEP_UNSUPPORTED_GSP3_CONFIGURATION = 20,
   SiRFflashEngineEP_INVALID_PARAM_ERROR = 21,
   SiRFflashEngineEP_SESSION_ALREADY_STARTED = 100,
   SiRFflashEngineEP_SESSION_NOT_STARTED = 101,
   SiRFflashEngineEP_COMM_LINE_SETUP_ERROR = 102,
   SiRFflashEngineEP_INITIAL_LOAD_ERROR = 103,
   SiRFflashEngineEP_COMM_ERROR = 104,
   SiRFflashEngineEP_INVALID_AUTO_RF_INPUT_CLOCK = 105
} SiRFflashEngineEP_RESULT;

/* SiRFflashEngineEP parameter types */

typedef enum
{
  SiRFflashEngineEP_GSP3_RF_CHIP_INPUT_FREQUENCY = 0
} SiRFflashEngineEP_PARAM;


/* SiRFflashEngineEP INTERFACE FUNCTIONS */

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPStartSession(SiRFflashEngineEP_CHAR *Line,
                                                              SiRFflashEngineEP_UINT32 BaudRate);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPEndSession(void);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPOpen(SiRFflashEngineEP_UINT32     ChipSelect,
                                                      SiRFflashEngineEP_UINT32     ChipOffset,
                                                      SiRFflashEngineEP_UINT32     *pNoChips,
                                                      SiRFflashEngineEP_FLASH_DESC *pFD);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPCreate(SiRFflashEngineEP_UINT32     ChipSelect,
                                                        SiRFflashEngineEP_UINT32     ChipOffset,
                                                        SiRFflashEngineEP_UINT32     *pNoChips,
                                                        SiRFflashEngineEP_FLASH_DESC *pFD);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPWrite(SiRFflashEngineEP_UINT32 Offset,
                                                       SiRFflashEngineEP_UINT8 *pData,
                                                       SiRFflashEngineEP_UINT32 Count);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPRead(SiRFflashEngineEP_UINT32 Offset,
                                                      SiRFflashEngineEP_UINT8 *pData,
                                                      SiRFflashEngineEP_UINT32 Count);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPEraseSectorSet(SiRFflashEngineEP_UINT32 Offset,
                                                                SiRFflashEngineEP_UINT32 Size);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPClose(void);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPGetCapacity(SiRFflashEngineEP_UINT32 *pCapacity);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPGetNoSectorSets(SiRFflashEngineEP_UINT32 *pNoSectorSets);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPGetSectorSetInfo(SiRFflashEngineEP_UINT32 SectorSetNo,
                                                                  SiRFflashEngineEP_UINT32 *pOffset,
                                                                  SiRFflashEngineEP_UINT32 *pSize);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPCheckReprogram(SiRFflashEngineEP_UINT32 Offset,
                                                                SiRFflashEngineEP_UINT8 *pData,
                                                                SiRFflashEngineEP_UINT32 Count);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPCheckBlank(SiRFflashEngineEP_UINT32 Offset,
                                                            SiRFflashEngineEP_UINT32 Count);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPGetVersion(SiRFflashEngineEP_CHAR *pVersion);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPCalculateFlashCRC16(SiRFflashEngineEP_UINT32 Offset,
                                                                     SiRFflashEngineEP_UINT32 Count,
                                                                     SiRFflashEngineEP_UINT16 InitCRC16,
                                                                     SiRFflashEngineEP_UINT16 *pResultCRC16);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPCalculateMemoryCRC16(SiRFflashEngineEP_UINT8 *pData,
                                                                      SiRFflashEngineEP_UINT32 Count,
                                                                      SiRFflashEngineEP_UINT16 InitCRC16,
                                                                      SiRFflashEngineEP_UINT16 *pResultCRC16);

extern SiRFflashEngineEP_RESULT SiRFflashEngineEPSetParam(SiRFflashEngineEP_PARAM ParamType,
					                                      SiRFflashEngineEP_UINT32 ParamValue);

#endif

