/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * command.c                                                               *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * DESCRIPTION:                                                            *
 *                                                                         *
 * Functions in this module pack commands and send them to the target      *
 * and unpack responses after they are received from the target.           *
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#include <string.h>
#include "SiRFflashEngineEP.h"
#include "global.h"
#include "sndrcv.h"
#include "commio.h"
#include "cmdcodes.h"
#include "pckunpck.h"
#include "delay.h"
#include "command.h"

#define GPS_CLOCK	            49107000L	

SiRFflashEngineEP_UINT32 ResponseSize;
SiRFflashEngineEP_UINT8 *Ptr;

/*
 * Function: SetBaudRate
 * Purpose:  Request from the target to change its baudrate.
 *           If bridge is present, request change of baud rate
 *           on the bridge also.
 */

SiRFflashEngineEP_INT32
SetBaudRate (SiRFflashEngineEP_INT32 baudrate,
			 SiRFflashEngineEP_UINT32 TargetBasebandChip)
{
  SiRFflashEngineEP_UINT32 TargetBaudrateDivider;

  Ptr = &PacketBuff[0];

  if (TargetBasebandChip == GSP2ELP_BASEBAND_CHIP)
	TargetBaudrateDivider = ((((GPS_CLOCK / baudrate) + 8) >> 4) << 4);
  else if (TargetBasebandChip == GSP2LPX_BASEBAND_CHIP)
	TargetBaudrateDivider = ((((GPS_CLOCK / baudrate) + 2) >> 2) << 2);
  else if (TargetBasebandChip == GSP3LT_BASEBAND_CHIP)
	TargetBaudrateDivider = ((((GPS_CLOCK / baudrate) + 2) >> 2) << 2);
  else if (TargetBasebandChip == GSP3BT_BASEBAND_CHIP)
	TargetBaudrateDivider = ((((GPS_CLOCK / baudrate) + 2) >> 2) << 2);
  else
	TargetBaudrateDivider = ((((GPS_CLOCK / baudrate) + 2) >> 2) << 2);

  PackByte (SET_BAUD_RATE, &Ptr);
  PackDword ((SiRFflashEngineEP_UINT32)baudrate, &Ptr);
  PackWord ((SiRFflashEngineEP_UINT16)TargetBaudrateDivider, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != CONFIRM_SET_BAUD_RATE)
    return FAILURE;

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;

  if (CommGetBridgePresent())
  {
     Ptr = &PacketBuff[0];

	 PackByte (BRIDGE_SET_BAUD_RATE, &Ptr);
     PackDword ((SiRFflashEngineEP_UINT32)baudrate, &Ptr);

	 if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
        return FAILURE;

     Ptr = &PacketBuff[0];

     if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
        return FAILURE;
     if (UnpackByte (&Ptr) != CONFIRM_BRIDGE_SET_BAUD_RATE)
        return FAILURE;

     if ((Ptr - &PacketBuff[0]) != ResponseSize)
        return FAILURE;

	 DelayMs(1000);
  }

  return CommChangeBaudRate(baudrate);
}

/*
 * Function: TargetMayExit
 * Purpose:  Inform the target that it may reenter internal boot program
 */

SiRFflashEngineEP_INT32
TargetMayExit(void)
{
  Ptr = &PacketBuff[0];
  PackByte (TARGET_MAY_EXIT, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_TARGET_MAY_EXIT)
    return FAILURE;

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: BridgeMayExit
 * Purpose:  Inform the bridge that it may exit
 */

SiRFflashEngineEP_INT32
BridgeMayExit(void)
{
  Ptr = &PacketBuff[0];
  PackByte (BRIDGE_MAY_EXIT, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_BRIDGE_MAY_EXIT)
    return FAILURE;

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: CreateSiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMCreate and
 *           receive response.
 */

SiRFflashEngineEP_INT32
CreateSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
               SiRFflashEngineEP_UINT32 ChipSelect,
               SiRFflashEngineEP_UINT32 ChipOffset,
               SiRFflashEngineEP_UINT32 * pNoChips,
               SiRFflashEngineEP_BOOL * pIsEraseSuspendSupported,
               SiRFflashEngineEP_FLASH_DESC * pFD)
{
  SiRFflashEngineEP_INT32 i;
  SiRFflashEngineEP_SECTOR_GROUP * sgp;

  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_CREATE, &Ptr);
  PackDword (ChipSelect, &Ptr);
  PackDword (ChipOffset, &Ptr);
  PackDword (0L /* SIRFFAM_INTS_NONE */, &Ptr);
  PackDword (0L /* SiRFFAM_INTS_NONE */, &Ptr);
  PackString (pFD->Fname.DeviceName, &Ptr);
  PackString (pFD->Fname.ManufacturerName, &Ptr);
  PackWord (pFD->Fid.DeviceCode, &Ptr);
  PackWord (pFD->Fid.ManufacturerCode, &Ptr);
  PackDword (pFD->Capacity, &Ptr);
  PackByte (pFD->NoSectorGroups, &Ptr);
  
  for (i = 0, sgp = pFD->SectorGroupPtr; i < pFD->NoSectorGroups; i++, sgp++)
  {
    PackDword (sgp->NoSectors, &Ptr);
    PackDword (sgp->SectorSize, &Ptr);
  }

  PackByte (pFD->Class, &Ptr);
  PackWord (pFD->ExtraData, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_CREATE)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);
  *pNoChips = UnpackDword (&Ptr);
  *pIsEraseSuspendSupported = UnpackDword (&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: OpenSiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMOpen and
 *           receive response.
 */

SiRFflashEngineEP_INT32
OpenSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
             SiRFflashEngineEP_UINT32 ChipSelect,
             SiRFflashEngineEP_UINT32 ChipOffset,
             SiRFflashEngineEP_UINT32 * pNoChips,
             SiRFflashEngineEP_BOOL * pIsEraseSuspendSupported,
             SiRFflashEngineEP_FLASH_DESC * pFD)
{
  SiRFflashEngineEP_INT32 i;
  SiRFflashEngineEP_SECTOR_GROUP * sgp;

  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_OPEN, &Ptr);
  PackDword (ChipSelect, &Ptr);
  PackDword (ChipOffset, &Ptr);
  PackDword (0L /* SiRFFAM_INTS_NONE */, &Ptr);
  PackDword (0L /* SiRFFAM_INTS_NONE */, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_OPEN)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);
  *pNoChips = UnpackDword (&Ptr);
  *pIsEraseSuspendSupported = UnpackDword (&Ptr);
  strcpy (pFD->Fname.DeviceName, Ptr);
  Ptr += strlen (pFD->Fname.DeviceName) + 1;
  strcpy (pFD->Fname.ManufacturerName, Ptr);
  Ptr += strlen (pFD->Fname.ManufacturerName) + 1;
  pFD->Fid.DeviceCode = UnpackWord (&Ptr);
  pFD->Fid.ManufacturerCode = UnpackWord (&Ptr);
  pFD->Capacity = UnpackDword (&Ptr);
  pFD->NoSectorGroups = UnpackByte (&Ptr);

  for (i = 0, sgp = pFD->SectorGroupPtr; i < pFD->NoSectorGroups; i++, sgp++)
  {
    sgp->NoSectors = UnpackDword (&Ptr);
    sgp->SectorSize = UnpackDword (&Ptr);
  }

  pFD->Class = UnpackByte (&Ptr);
  pFD->ExtraData = UnpackWord (&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: WriteSiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMWrite and
 *           receive response.
 */

SiRFflashEngineEP_INT32
WriteSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
              SiRFflashEngineEP_UINT32 Offset,
              SiRFflashEngineEP_UINT8 * pData,
			  SiRFflashEngineEP_UINT32 Count)
{
  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_WRITE, &Ptr);
  PackDword (Offset, &Ptr);
  PackDword (Count, &Ptr);
  memcpy (Ptr, pData, Count);
  Ptr += Count;

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_WRITE)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: ReadSiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMRead and
 *           receive response.
 */

SiRFflashEngineEP_INT32
ReadSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
             SiRFflashEngineEP_UINT32 Offset,
             SiRFflashEngineEP_UINT8 * pData,
			 SiRFflashEngineEP_UINT32 Count)
{
  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_READ, &Ptr);
  PackDword (Offset, &Ptr);
  PackDword (Count, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_READ)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);
  memcpy (pData, Ptr, Count);
  Ptr += Count;

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;

}

/*
 * Function: EraseSectorSetSiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMEraseSectorSet and
 *           receive response.
 */

SiRFflashEngineEP_INT32
EraseSectorSetSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                       SiRFflashEngineEP_UINT32 Offset,
					   SiRFflashEngineEP_UINT32 Size)
{
  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_ERASE_SECTOR_SET, &Ptr);
  PackDword (Offset, &Ptr);
  PackDword (Size, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_ERASE_SECTOR_SET)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;

}

/*
 * Function: CloseSiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMClose and
 *           receive response.
 */

SiRFflashEngineEP_INT32
CloseSiRFFAM (SiRFflashEngineEP_RESULT * pResult)
{
  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_CLOSE, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_CLOSE)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: GetCapacitySiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMGetCapacity and
 *           receive response.
 */

SiRFflashEngineEP_INT32
GetCapacitySiRFFAM (SiRFflashEngineEP_RESULT * pResult,
					SiRFflashEngineEP_UINT32 * pCapacity)
{
  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_GET_CAPACITY, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_GET_CAPACITY)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);
  *pCapacity = UnpackDword (&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: GetNoSectorSetsSiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMGetNoSectorSets and
 *           receive response.
 */

SiRFflashEngineEP_INT32
GetNoSectorSetsSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                        SiRFflashEngineEP_UINT32 * pNoSectorSets)
{
  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_GET_NO_SECTOR_SETS, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_GET_NO_SECTOR_SETS)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);
  *pNoSectorSets = UnpackDword (&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: GetSectorSetInfoSiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMGetSectorSetInfo and
 *           receive response.
 */

SiRFflashEngineEP_INT32
GetSectorSetInfoSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                         SiRFflashEngineEP_UINT32 SectorSetNo,
                         SiRFflashEngineEP_UINT32 * pOffset,
						 SiRFflashEngineEP_UINT32 * pSize)
{
  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_GET_SECTOR_SET_INFO, &Ptr);
  PackDword (SectorSetNo, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_GET_SECTOR_SET_INFO)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);
  *pOffset = UnpackDword (&Ptr);
  *pSize = UnpackDword (&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: CheckReprogramSiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMCheckReprogram and
 *           receive response.
 */

SiRFflashEngineEP_INT32
CheckReprogramSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                       SiRFflashEngineEP_UINT32 Offset,
                       SiRFflashEngineEP_UINT8 * pData,
					   SiRFflashEngineEP_UINT32 Count)
{
  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_CHECK_REPROGRAM, &Ptr);
  PackDword (Offset, &Ptr);
  PackDword (Count, &Ptr);
  memcpy (Ptr, pData, Count);
  Ptr += Count;

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_CHECK_REPROGRAM)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: CheckBlankSiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMCheckBlank and
 *           receive response.
 */

SiRFflashEngineEP_INT32
CheckBlankSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                   SiRFflashEngineEP_UINT32 Offset,
				   SiRFflashEngineEP_UINT32 Count)
{
  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_CHECK_BLANK, &Ptr);
  PackDword (Offset, &Ptr);
  PackDword (Count, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_CHECK_BLANK)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: SetWriteAtomicUnitSizeSiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMSetWriteAtomicUnitSize and
 *           receive response.
 */

SiRFflashEngineEP_INT32
SetWriteAtomicUnitSizeSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
							   SiRFflashEngineEP_UINT32 Size)
{
  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_SET_WRITE_ATOMIC_UNIT_SIZE, &Ptr);
  PackDword (Size, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_SET_WRITE_ATOMIC_UNIT_SIZE)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: GetVersionSiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMGetVersion and
 *           receive response.
 */

SiRFflashEngineEP_INT32
GetVersionSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
				   SiRFflashEngineEP_CHAR * pVersion)
{
  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_GET_VERSION, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_GET_VERSION)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);
  strcpy (pVersion, Ptr);
  Ptr += strlen (pVersion) + 1;

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: CalculateCRC16SiRFFAM
 * Purpose:  Request from the target to execute SiRFFAMCalculateCRC16 and
 *           receive response.
 */

SiRFflashEngineEP_INT32
CalculateCRC16 (SiRFflashEngineEP_RESULT * pResult,
                SiRFflashEngineEP_UINT32 Offset,
                SiRFflashEngineEP_UINT32 Count,
                SiRFflashEngineEP_UINT16 InitCRC16,
				SiRFflashEngineEP_UINT16 * pResultCRC16)
{
  Ptr = &PacketBuff[0];

  PackByte (SIRFFAM_CALCULATE_CRC16, &Ptr);
  PackDword (Offset, &Ptr);
  PackDword (Count, &Ptr);
  PackWord (InitCRC16, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_SIRFFAM_CALCULATE_CRC16)
    return FAILURE;

  *pResult = UnpackDword (&Ptr);
  *pResultCRC16 = UnpackWord (&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: GetChipVersion
 * Purpose:  Obtain version of the baseband chip
 */
 
SiRFflashEngineEP_INT32 
GetChipVersion(SiRFflashEngineEP_UINT8 *pChipVersion)
{
  Ptr = &PacketBuff[0];

  PackByte (GET_CHIP_VERSION, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_GET_CHIP_VERSION)
    return FAILURE;

  *pChipVersion = UnpackByte(&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: GetRFChipVersion
 * Purpose:  Obtain version of the RF chip
 */
 
SiRFflashEngineEP_INT32
GetRFChipVersion(SiRFflashEngineEP_UINT8 *pRFChipVersion)
{
  Ptr = &PacketBuff[0];

  PackByte (GET_RF_CHIP_VERSION, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_GET_RF_CHIP_VERSION)
    return FAILURE;

  *pRFChipVersion = UnpackByte(&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: GetChipConfig
 * Purpose:  Obtain value of GSP3 CHIP_CONFIG register
 */
 
SiRFflashEngineEP_INT32
GetChipConfig(SiRFflashEngineEP_UINT16 *pChipConfig)
{
  Ptr = &PacketBuff[0];

  PackByte (GET_CHIP_CONFIG, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_GET_CHIP_CONFIG)
    return FAILURE;

  *pChipConfig = UnpackWord(&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: GetRomConfig
 * Purpose:  Obtain value of GSP3 ROM_CONFIG register
 */
 
SiRFflashEngineEP_INT32
GetRomConfig(SiRFflashEngineEP_UINT16 *pRomConfig)
{
  Ptr = &PacketBuff[0];

  PackByte (GET_ROM_CONFIG, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != RESPONSE_GET_ROM_CONFIG)
    return FAILURE;

  *pRomConfig = UnpackWord(&Ptr);

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: SetupGSP2Chip
 * Purpose:  Set bus clock divider of GSP2 chip.
 */
 
SiRFflashEngineEP_INT32
SetupGSP2Chip(SiRFflashEngineEP_UINT8 BusClockDivider)
{
  Ptr = &PacketBuff[0];

  PackByte (SETUP_GSP2_CHIP, &Ptr);
  PackByte (BusClockDivider, &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != CONFIRM_SETUP_GSP2_CHIP)
    return FAILURE;

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}

/*
 * Function: SetupGSP2LPXChip
 * Purpose:  Set output clock of RF chip, PLL, bus clock source
 *           and divider, as well as UART clock source and divider
 *           on GSP2LPX chip.
 * Note:     At this moment, this function should be called with input
 *           argnument TargetRFChip equal to GRF3I_RF_CHIP only.
 */
 
SiRFflashEngineEP_INT32
SetupGSP2LPXChip(SiRFflashEngineEP_UINT32 TargetRFChip, SiRFflashEngineEP_UINT32 RFChipInputFrequency)
{
  SiRFflashEngineEP_UINT8 RFSPISequence[8];
  SiRFflashEngineEP_UINT16 TargetDivider;
  SiRFflashEngineEP_INT32 i;
  Ptr = &PacketBuff[0];
  PackByte (SETUP_GSP2LPX_CHIP, &Ptr);
  if (TargetRFChip == GRF3I_RF_CHIP)
  {
	SiRFflashEngineEP_UINT8 RDIV;
	SiRFflashEngineEP_UINT32 NDIV;
	switch(RFChipInputFrequency)
	{
		case 13000000L: RDIV = 9;  NDIV = 1088; break;
		case 16369000L: RDIV = 1;  NDIV = 96;   break;
		case 24553500L: RDIV = 1;  NDIV = 64;   break;
		case 26000000L: RDIV = 9;  NDIV = 544;  break;
		default: return FAILURE;
	}
	RFSPISequence[0] = 0x03;
	RFSPISequence[1] = 0xCF;
	RFSPISequence[2] = 0x10;
	RFSPISequence[3] = 0x38;
	RFSPISequence[4] = (SiRFflashEngineEP_UINT8)((NDIV >> 9) & 3);
	RFSPISequence[5] = (SiRFflashEngineEP_UINT8)(NDIV >> 1);
	RFSPISequence[6] = (SiRFflashEngineEP_UINT8)(3 | (RDIV << 2) | ((NDIV & 1) << 7));
	RFSPISequence[7] = 0xa0;
	TargetDivider = ((((GPS_CLOCK / 38400) + 8) >> 4) << 4);
	PackWord (TargetDivider, &Ptr);			// divider for 38400 baud
	PackByte (8, &Ptr);			            // SPI sequence length
  }
  for (i = 0; i < 8; i++)		            // SPI sequence data
	PackByte(RFSPISequence[i], &Ptr);
  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;
  Ptr = &PacketBuff[0];
  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != CONFIRM_SETUP_GSP2LPX_CHIP)
    return FAILURE;
  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}
/*
 * Function: SetupGSP3Chip
 * Purpose:  Set output clock of RF chip, PLL, bus clock source
 *           and divider, as well as UART clock source and divider
 *           on GSP3 chip.
 * Note:     At this moment, this function should be called with input
 *           argnument TargetRFChip equal to GRF3W_RF_CHIP only.
 */
 
SiRFflashEngineEP_INT32
SetupGSP3Chip(SiRFflashEngineEP_UINT32 TargetRFChip, SiRFflashEngineEP_UINT32 RFChipInputFrequency)
{
  SiRFflashEngineEP_UINT8 RFSPISequence[8];
  SiRFflashEngineEP_UINT16 TargetDivider;
  SiRFflashEngineEP_INT32 i;

  Ptr = &PacketBuff[0];
  PackByte (SETUP_GSP3_CHIP, &Ptr);

  if (TargetRFChip == GRF3W_RF_CHIP)
  {
	SiRFflashEngineEP_UINT8 ND;
	SiRFflashEngineEP_UINT32 Numerator;

	  // Calculate RF SPI sequence

	switch(RFChipInputFrequency)
	{
		case 13000000L: ND = 117; Numerator = 14743301; break;
		case 16369000L: ND = 93;  Numerator = 0;        break;
		case 16800000L: ND = 90;  Numerator = 9011762;  break;
		case 19200000L: ND = 78;  Numerator = 14176748; break;
		case 24553500L: ND = 61;  Numerator = 0;        break;
		case 26000000L: ND = 57;  Numerator = 7371651;  break;
		default: return FAILURE;
	}

	RFSPISequence[0] = (SiRFflashEngineEP_UINT8)((Numerator >> 19) & 0x1F); /* 5 Bits 19..23 of Numerator */ /* Bits 55..48 */
	RFSPISequence[1] = (SiRFflashEngineEP_UINT8)((Numerator >> 11) & 0xFF); /* 8 Bits 11..18 of Numerator */ /* Bits 47..40 */
	RFSPISequence[2] = (SiRFflashEngineEP_UINT8)((Numerator >>  3) & 0xFF); /* 8 Bits 3..10 of Numerator */  /* Bits 39..32 */
	RFSPISequence[3] = (SiRFflashEngineEP_UINT8)((ND >> 3) | ((Numerator & 0x7) << 5));/* Upper 5 bits of ND and Low 3 bits of Numerator */  /* Bits 31..24 */
	RFSPISequence[4] = 0x03 | (ND & 0x7) << 5 ; /* Low 3 bits of ND, into bits 23,22, 21 */  /* Bits 23..16 */
	RFSPISequence[5] = 0x27; /* 1110 0101 */  /* Bits 15..8  */
	RFSPISequence[6] = 0xdc | 0x1; /* Bits  7..0, enable all, Mode = Normal Operating Mode */
	RFSPISequence[7] = 0; /* Not used in GRF3w/GRF2m */

	TargetDivider = ((((GPS_CLOCK / 38400) + 8) >> 4) << 4);

	PackWord (TargetDivider, &Ptr);			// divider for 38400 baud
	PackByte (7, &Ptr);			            // SPI sequence length
  }
  else
  {
	SiRFflashEngineEP_UINT8 RDIV;
	SiRFflashEngineEP_UINT32 NDIV;

	switch(RFChipInputFrequency)
	{
		case 13000000L: RDIV = 9;  NDIV = 1088; break;
		case 16369000L: RDIV = 1;  NDIV = 96;   break;
		case 16800000L: RDIV = 13; NDIV = 1216; break;
		case 19200000L: RDIV = 13; NDIV = 1064; break;
		case 24553500L: RDIV = 1;  NDIV = 64;   break;
		case 26000000L: RDIV = 9;  NDIV = 544;  break;
		case 33600000L: RDIV = 13; NDIV = 608;  break;
		default: return FAILURE;
	}

	RFSPISequence[0] = 0x03;
	RFSPISequence[1] = 0xCF;
	RFSPISequence[2] = 0x10;
	RFSPISequence[3] = 0x38;
	RFSPISequence[4] = (SiRFflashEngineEP_UINT8)((NDIV >> 9) & 3);
	RFSPISequence[5] = (SiRFflashEngineEP_UINT8)(NDIV >> 1);
	RFSPISequence[6] = (SiRFflashEngineEP_UINT8)(3 | (RDIV << 2) | ((NDIV & 1) << 7));
	RFSPISequence[7] = 0xa0;

	TargetDivider = ((((GPS_CLOCK / 38400) + 8) >> 4) << 4);

	PackWord (TargetDivider, &Ptr);			// divider for 38400 baud
	PackByte (8, &Ptr);			            // SPI sequence length
  }

  for (i = 0; i < 8; i++)		            // SPI sequence data
	PackByte(RFSPISequence[i], &Ptr);

  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;

  Ptr = &PacketBuff[0];

  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;

  if (UnpackByte (&Ptr) != CONFIRM_SETUP_GSP3_CHIP)
    return FAILURE;

  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}
SiRFflashEngineEP_INT32
SetupGSP3LTChip(SiRFflashEngineEP_UINT32 TargetRFChip, SiRFflashEngineEP_UINT32 RFChipInputFrequency)
{
  SiRFflashEngineEP_UINT8 RFSPISequence[8];
  SiRFflashEngineEP_UINT16 TargetDivider;
  SiRFflashEngineEP_INT32 i;
  Ptr = &PacketBuff[0];
  PackByte (SETUP_GSP3_CHIP, &Ptr);
  if (TargetRFChip == GRF3I_RF_CHIP)
  {
	SiRFflashEngineEP_UINT8 RDIV;
	SiRFflashEngineEP_UINT32 NDIV;
	switch(RFChipInputFrequency)
	{
		case 13000000L: RDIV = 9;  NDIV = 1088; break;
		case 16369000L: RDIV = 1;  NDIV = 96;   break;
		case 16800000L: RDIV = 13; NDIV = 1216; break;
		case 19200000L: RDIV = 13; NDIV = 1064; break;
		case 24553500L: RDIV = 1;  NDIV = 64;   break;
		case 26000000L: RDIV = 9;  NDIV = 544;  break;
		case 33600000L: RDIV = 13; NDIV = 608;  break;
		default: return FAILURE;
	}
	RFSPISequence[0] = 0x03;
	RFSPISequence[1] = 0xCF;
	RFSPISequence[2] = 0x10;
	RFSPISequence[3] = 0x38;
	RFSPISequence[4] = (SiRFflashEngineEP_UINT8)((NDIV >> 9) & 3);
	RFSPISequence[5] = (SiRFflashEngineEP_UINT8)(NDIV >> 1);
	RFSPISequence[6] = (SiRFflashEngineEP_UINT8)(3 | (RDIV << 2) | ((NDIV & 1) << 7));
	RFSPISequence[7] = 0xa0;
	TargetDivider = ((((GPS_CLOCK / 38400) + 8) >> 4) << 4);
	PackWord (TargetDivider, &Ptr);			// divider for 38400 baud
	PackByte (8, &Ptr);			            // SPI sequence length
  }
  for (i = 0; i < 8; i++)		            // SPI sequence data
	PackByte(RFSPISequence[i], &Ptr);
  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;
  Ptr = &PacketBuff[0];
  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != CONFIRM_SETUP_GSP3_CHIP)
    return FAILURE;
  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}
/*
 * Function: SetupGSP3BTChip
 * Purpose:  Set output clock of RF chip, PLL, bus clock source
 *           and divider, as well as UART clock source and divider
 *           on GSP3BT chip.
 * Note:     At this moment, this function should be called with input
 *           argnument TargetRFChip equal to GRF3BT_RF_CHIP only.
 */
 
SiRFflashEngineEP_INT32
SetupGSP3BTChip(SiRFflashEngineEP_UINT32 TargetRFChip, SiRFflashEngineEP_UINT32 RFChipInputFrequency)
{
  SiRFflashEngineEP_UINT8 RFSPISequence[8];
  SiRFflashEngineEP_UINT16 TargetDivider;
  SiRFflashEngineEP_INT32 i;
  Ptr = &PacketBuff[0];
  PackByte (SETUP_GSP3_CHIP, &Ptr);
  if (TargetRFChip == GRF3BT_RF_CHIP)
  {
	SiRFflashEngineEP_UINT8 RDIV;
	SiRFflashEngineEP_UINT32 NDIV;
	switch(RFChipInputFrequency)
	{
		case 13000000L: RDIV = 9;  NDIV = 1088; break;
		case 16369000L: RDIV = 1;  NDIV = 96;   break;
		case 16800000L: RDIV = 13; NDIV = 1216; break;
		case 19200000L: RDIV = 13; NDIV = 1064; break;
		case 19800000L: RDIV = 11; NDIV = 873;  break;
		case 24553500L: RDIV = 1;  NDIV = 64;   break;
		case 26000000L: RDIV = 9;  NDIV = 544;  break;
		case 33600000L: RDIV = 13; NDIV = 608;  break;
		case 38400000L: RDIV = 13; NDIV = 532;  break;
		case 39600000L: RDIV = 22; NDIV = 873;  break;
		default: return FAILURE;
	}
	RFSPISequence[0] = 0x02;
	RFSPISequence[1] = 0xEF;
	RFSPISequence[2] = 0xF0;
	RFSPISequence[3] = 0x30;
	RFSPISequence[4] = (SiRFflashEngineEP_UINT8)(0xF8 | ((NDIV >> 9) & 3));
	RFSPISequence[5] = (SiRFflashEngineEP_UINT8)(NDIV >> 1);
	RFSPISequence[6] = (SiRFflashEngineEP_UINT8)(2 | (RDIV << 2) | ((NDIV & 1) << 7));
	RFSPISequence[7] = 0x40;
	TargetDivider = ((((GPS_CLOCK / 38400) + 8) >> 4) << 4);
	PackWord (TargetDivider, &Ptr);			// divider for 38400 baud
	PackByte (8, &Ptr);			            // SPI sequence length
  }
  for (i = 0; i < 8; i++)		            // SPI sequence data
	PackByte(RFSPISequence[i], &Ptr);
  if (SendPacket (&PacketBuff[0], Ptr - &PacketBuff[0]) == FAILURE)
    return FAILURE;
  Ptr = &PacketBuff[0];
  if (ReceivePacket (Ptr, &ResponseSize) == FAILURE)
    return FAILURE;
  if (UnpackByte (&Ptr) != CONFIRM_SETUP_GSP3_CHIP)
    return FAILURE;
  if ((Ptr - &PacketBuff[0]) != ResponseSize)
    return FAILURE;
  else
    return SUCCESS;
}
