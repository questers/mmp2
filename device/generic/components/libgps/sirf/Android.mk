LOCAL_PATH:= $(call my-dir)


########################
# sirf loader library
#
include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := debug eng
LOCAL_MODULE:= libsirfloader
LOCAL_SHARED_LIBRARIES := libcutils libc
LOCAL_CFLAGS := -DBINARY_IMAGE_IN_FILE
LOCAL_SRC_FILES:= \
	burner.c \
	chksum.c \
	command.c \
	pckunpck.c \
	sndrcv.c \
	SiRFflashEngineEP.c \
	imgacc.c\
	starter.c \
	loader.c \
	commio.c \
	delay.c \
	errsup.c \
	timsup.c \
	sirfloader.c

LOCAL_COPY_HEADERS := \
	sirfloader.h

LOCAL_PRELINK_MODULE := false
	

include $(BUILD_STATIC_LIBRARY)
	
#include $(BUILD_SHARED_LIBRARY)


