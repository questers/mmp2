/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * global.h                                                                *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#ifndef __GLOBAL_H
#define __GLOBAL_H

#define  LOG_TAG  "gpsloader"

#include <cutils/log.h>

#define FAILURE		0
#define SUCCESS		1


#undef GET_SHORT_FILENAME

#define  GET_SHORT_FILENAME(longfilename)   \
    ( (strrchr(longfilename, '/')==NULL) ? \
        longfilename: ((char*)(strrchr(longfilename, '/')+1))) 
        
#define SIRF_ERROR(fmt, args...)      \
	LOGE("%s:%d,"fmt,			 \
	 GET_SHORT_FILENAME(__FILE__), __LINE__,  ##args)		
	 
#define SIRF_WARNING(fmt, args...) \
	LOGW("%s:%d,"fmt,			 \
		 GET_SHORT_FILENAME(__FILE__), __LINE__,  ##args)		
		 
#define SIRF_INFO(fmt, args...)       \
		LOGI("%s:%d,"fmt,			 \
	 GET_SHORT_FILENAME(__FILE__), __LINE__,  ##args)		

#define SIRF_DEBUG(fmt, args...)      \
   LOGD("%s:%d, "fmt,			\
	GET_SHORT_FILENAME(__FILE__), __LINE__,  ##args)	   
	
#define SIRF_LOG(fmt, args...)        \
	LOGV("%s:%d, "fmt,			 \
	 GET_SHORT_FILENAME(__FILE__), __LINE__,  ##args)		


#endif
