/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * pckunpck.h                                                              *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#ifndef __PCKUNPCK_H
#define __PCKUNPCK_H

extern void                     PackByte (SiRFflashEngineEP_UINT8 Val,
										  SiRFflashEngineEP_UINT8 ** DstPtr);
extern void                     PackWord (SiRFflashEngineEP_UINT16 Val,
										  SiRFflashEngineEP_UINT8 ** DstPtr);
extern void                     PackDword (SiRFflashEngineEP_UINT32 Val,
										   SiRFflashEngineEP_UINT8 ** DstPtr);
extern void                     PackString (SiRFflashEngineEP_UINT8 * StrPtr,
											SiRFflashEngineEP_UINT8 ** DstPtr);
extern SiRFflashEngineEP_UINT8  UnpackByte (SiRFflashEngineEP_UINT8 ** SrcPtr);
extern SiRFflashEngineEP_UINT16 UnpackWord (SiRFflashEngineEP_UINT8 ** SrcPtr);
extern SiRFflashEngineEP_UINT32 UnpackDword (SiRFflashEngineEP_UINT8 ** SrcPtr);

#endif
