/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * SiRFflashEngineEP.c                                                     *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * DESCRIPTION:                                                            *
 *                                                                         *
 * Functions in this module execute top level SiRFflashEngineEP functions  *
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#include <string.h>
#include "SiRFflashEngineEP.h"
#include "global.h"
#include "command.h"
#include "starter.h"
#include "burner.h"
#include "commio.h"
#include "loader.h"
#include "cmdcodes.h"

static char *VersionStr = "2.11";

static SiRFflashEngineEP_RESULT Result;
static SiRFflashEngineEP_INT32 SessionStarted = 0;

static SiRFflashEngineEP_UINT8 TargetBasebandChip;
static SiRFflashEngineEP_UINT8 TargetRFChip;
static SiRFflashEngineEP_UINT32 RFChipInputFrequency = 0;

static const SiRFflashEngineEP_UINT16 vCRC16[256] = {
  0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
  0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
  0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
  0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
  0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
  0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
  0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
  0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
  0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
  0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
  0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
  0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
  0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
  0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
  0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
  0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
  0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
  0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
  0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
  0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
  0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
  0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
  0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
  0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
  0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
  0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
  0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
  0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
  0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
  0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
  0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
  0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
};

#define MAX_DATA_BLOCK_SIZE		1024

/*
 * Function: SiRFflashEngineEPStartSession
 * Purpose:  Establish serial connection with the target and download
 *           flashing code.
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPStartSession(SiRFflashEngineEP_CHAR *Line, SiRFflashEngineEP_UINT32 BaudRate)
{
  if (SessionStarted)
    return SiRFflashEngineEP_SESSION_ALREADY_STARTED;

  if (CommOpen (Line, 38400L) == FAILURE)
    return SiRFflashEngineEP_COMM_LINE_SETUP_ERROR;

  SIRF_DEBUG("load starter... size[%u]",(unsigned int)starter_size);
  if (LoadStarter (&starter[0], starter_size) == FAILURE)
    return SiRFflashEngineEP_INITIAL_LOAD_ERROR;

  if (GetChipVersion(&TargetBasebandChip) == FAILURE)
	  return SiRFflashEngineEP_COMM_ERROR;

  SIRF_DEBUG("chip version == %d",TargetBasebandChip);
  if (TargetBasebandChip == GSP2ELP_BASEBAND_CHIP)
  {
      if (SetupGSP2Chip(0) == FAILURE)
	  	  return SiRFflashEngineEP_COMM_ERROR;
  }
  else if (TargetBasebandChip == GSP2LPX_BASEBAND_CHIP)
  {
	  if (GetRFChipVersion(&TargetRFChip) == FAILURE)
		  return SiRFflashEngineEP_COMM_ERROR;
	  if (RFChipInputFrequency == 0)
	  {
          SiRFflashEngineEP_UINT16 ChipConfig;
          if (GetChipConfig(&ChipConfig) == FAILURE)
              return SiRFflashEngineEP_COMM_ERROR;
          if (TargetRFChip == GRF3I_RF_CHIP)
          {
              switch ((ChipConfig >> 12) & 3)
              {
                  case 0: RFChipInputFrequency = 13000000L; break;
                  case 1: RFChipInputFrequency = 24553500L; break;
                  case 2: RFChipInputFrequency = 26000000L; break;
                  case 3: RFChipInputFrequency = 16369000L; break;
                  default: return SiRFflashEngineEP_INVALID_AUTO_RF_INPUT_CLOCK;
              }
          }
  else
          {
			  return SiRFflashEngineEP_INVALID_AUTO_RF_INPUT_CLOCK;
          }
	  }
	  if (SetupGSP2LPXChip(TargetRFChip, RFChipInputFrequency) == FAILURE)
		  return SiRFflashEngineEP_COMM_ERROR;
  }
  else if (TargetBasebandChip == GSP3_BASEBAND_CHIP)
  {
	  if (GetRFChipVersion(&TargetRFChip) == FAILURE)
		  return SiRFflashEngineEP_COMM_ERROR;

	  if (RFChipInputFrequency == 0)
	  {
          SiRFflashEngineEP_UINT16 ChipConfig;
          SiRFflashEngineEP_UINT16 RomConfig;

          if (GetChipConfig(&ChipConfig) == FAILURE)
              return SiRFflashEngineEP_COMM_ERROR;

          if (GetRomConfig(&RomConfig) == FAILURE)
              return SiRFflashEngineEP_COMM_ERROR;

          if (TargetRFChip == GRF3W_RF_CHIP)
          {
              switch ((ChipConfig >> 12) & 3)
              {
                  case 0: RFChipInputFrequency = 16369000L; break;
                  case 1: RFChipInputFrequency = 24553500L; break;
                  case 2: RFChipInputFrequency = 26000000L; break;
                  case 3: switch (RomConfig & 0x1f)
                          {
                              case 0:  RFChipInputFrequency = 13000000L; break;
                              case 10: RFChipInputFrequency = 16369000L; break;
                              case 11: RFChipInputFrequency = 16800000L; break;
                              case 18: RFChipInputFrequency = 19200000L; break;
                              case 29: RFChipInputFrequency = 24553500L; break;
                              case 31: RFChipInputFrequency = 26000000L; break;
                              default: return SiRFflashEngineEP_INVALID_AUTO_RF_INPUT_CLOCK;
                          }
              }
          }
          else
          {
              switch ((ChipConfig >> 12) & 3)
              {
                  case 0: RFChipInputFrequency = 16369000L; break;
                  case 1: RFChipInputFrequency = 24553500L; break;
                  case 2: RFChipInputFrequency = 26000000L; break;
                  case 3: switch (RomConfig & 0x1f)
                          {
                              case 0:  RFChipInputFrequency = 13000000L; break;
                              case 7:  RFChipInputFrequency = 16369000L; break;
                              case 8:  RFChipInputFrequency = 16800000L; break;
                              case 13: RFChipInputFrequency = 19200000L; break;
                              case 21: RFChipInputFrequency = 24553500L; break;
                              case 23: RFChipInputFrequency = 26000000L; break;
                              case 30: RFChipInputFrequency = 33600000L; break;
                          }
              }
          }
	  }

	  if (SetupGSP3Chip(TargetRFChip, RFChipInputFrequency) == FAILURE)
		  return SiRFflashEngineEP_COMM_ERROR;
  }
  else if (TargetBasebandChip == GSP3LT_BASEBAND_CHIP)
  {
	  if (GetRFChipVersion(&TargetRFChip) == FAILURE)
		  return SiRFflashEngineEP_COMM_ERROR;
  
	  if (RFChipInputFrequency == 0)
	  {
          SiRFflashEngineEP_UINT16 ChipConfig;
          if (GetChipConfig(&ChipConfig) == FAILURE)
              return SiRFflashEngineEP_COMM_ERROR;
          if (TargetRFChip == GRF3I_RF_CHIP)
          {
              switch ((ChipConfig >> 6) & 7)
              {
                  case 1: RFChipInputFrequency = 33600000; break;
                  case 2: RFChipInputFrequency = 19200000; break;
                  case 3: RFChipInputFrequency = 16800000; break;
                  case 4: RFChipInputFrequency = 13000000; break;
                  case 5: RFChipInputFrequency = 24553500; break;
                  case 6: RFChipInputFrequency = 26000000; break;
                  case 7: RFChipInputFrequency = 16369000; break;
                  default: return SiRFflashEngineEP_INVALID_AUTO_RF_INPUT_CLOCK;
             }
          }
          else
          {
			  return SiRFflashEngineEP_INVALID_AUTO_RF_INPUT_CLOCK;
          }
	  }
	  if (SetupGSP3LTChip(TargetRFChip, RFChipInputFrequency) == FAILURE)
		  return SiRFflashEngineEP_COMM_ERROR;
  }
  else if (TargetBasebandChip == GSP3BT_BASEBAND_CHIP)
  {
	  if (GetRFChipVersion(&TargetRFChip) == FAILURE)
		  return SiRFflashEngineEP_COMM_ERROR;
	  if (RFChipInputFrequency == 0)
	  {
          SiRFflashEngineEP_UINT16 ChipConfig;
          if (GetChipConfig(&ChipConfig) == FAILURE)
              return SiRFflashEngineEP_COMM_ERROR;
          if (TargetRFChip == GRF3BT_RF_CHIP)
          {
              switch (((ChipConfig >> 7) & 7) /*| ((ChipConfig & 2) << 2)*/)
              {
                  case 0: RFChipInputFrequency = 38400000; break;
                  case 1: RFChipInputFrequency = 33600000; break;
                  case 2: RFChipInputFrequency = 19200000; break;
                  case 3: RFChipInputFrequency = 16800000; break;
                  case 4: RFChipInputFrequency = 13000000; break;
                  case 5: RFChipInputFrequency = 24553500; break;
                  case 6: RFChipInputFrequency = 26000000; break;
                  case 7: RFChipInputFrequency = 16369000; break;
                  case 8: RFChipInputFrequency = 19800000; break;
                  case 9: RFChipInputFrequency = 39600000; break;
                  default: return SiRFflashEngineEP_INVALID_AUTO_RF_INPUT_CLOCK;
              }
          }
          else
          {
			  return SiRFflashEngineEP_INVALID_AUTO_RF_INPUT_CLOCK;
          }
	  }
	  if (SetupGSP3BTChip(TargetRFChip, RFChipInputFrequency) == FAILURE)
		  return SiRFflashEngineEP_COMM_ERROR;
  }

  SIRF_DEBUG("set baudrate [%u]",(unsigned int)BaudRate);
  if (SetBaudRate (BaudRate, TargetBasebandChip) == FAILURE)
    return SiRFflashEngineEP_COMM_LINE_SETUP_ERROR;
  SIRF_DEBUG("load burner ... size[%u]",(unsigned int)burner_size);
  if (LoadBurner (&burner[0], burner_size) == FAILURE)
    return SiRFflashEngineEP_INITIAL_LOAD_ERROR;

  SessionStarted = 1;

  return SiRFflashEngineEP_OK;
}

/*
 * Function: SiRFflashEngineEPEndSession
 * Purpose:  Terminate serial connection with the target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPEndSession(void)
{
	if (!SessionStarted)
		return SiRFflashEngineEP_SESSION_NOT_STARTED;

	TargetMayExit();
	if (CommGetBridgePresent())
		BridgeMayExit();
	CommClose();

	SessionStarted = 0;
	return SiRFflashEngineEP_OK;
}


/*
 * Function: SiRFflashEngineEPOpen
 * Purpose:  Execute SiRFFAMOpen on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPOpen (SiRFflashEngineEP_UINT32 ChipSelect,
                       SiRFflashEngineEP_UINT32 ChipOffset,
                       SiRFflashEngineEP_UINT32 * pNoChips,
                       SiRFflashEngineEP_FLASH_DESC * pFD)
{
  SiRFflashEngineEP_BOOL IsEraseSuspendSupported;

  if (OpenSiRFFAM (&Result,
                   ChipSelect,
                   ChipOffset,
                   pNoChips,
                   &IsEraseSuspendSupported,
                   pFD) == SUCCESS)
  {
    if (!Result)
	{
      if (SetWriteAtomicUnitSizeSiRFFAM (&Result, *pNoChips * 32) == SUCCESS)
	    return Result;
	  else
	    return SiRFflashEngineEP_COMM_ERROR;
    }
    else
      return Result; 
  }
  else
    return SiRFflashEngineEP_COMM_ERROR;
}

/*
 * Function: SiRFflashEngineEPCreate
 * Purpose:  Execute SiRFFAMCreate on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPCreate (SiRFflashEngineEP_UINT32 ChipSelect,
                         SiRFflashEngineEP_UINT32 ChipOffset,
                         SiRFflashEngineEP_UINT32 * pNoChips,
                         SiRFflashEngineEP_FLASH_DESC * pFD)
{
  SiRFflashEngineEP_BOOL IsEraseSuspendSupported;

  if (CreateSiRFFAM (&Result,
                     ChipSelect,
                     ChipOffset,
                     pNoChips,
                     &IsEraseSuspendSupported,
                     pFD) == SUCCESS)
  {
    if (!Result)
	{
      if (SetWriteAtomicUnitSizeSiRFFAM (&Result, *pNoChips * 32) == SUCCESS)
	    return Result;
	  else
	    return SiRFflashEngineEP_COMM_ERROR;
    }
    else
      return Result; 
  }
  else
    return SiRFflashEngineEP_COMM_ERROR;
}

/*
 * Function: SiRFflashEngineEPWrite
 * Purpose:  Execute SiRFFAMWrite on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPWrite (SiRFflashEngineEP_UINT32 Offset,
                        SiRFflashEngineEP_UINT8 * pData,
						SiRFflashEngineEP_UINT32 Count)
{
  SiRFflashEngineEP_UINT32 Remaining = Count;

  while (Remaining)
  {
    if (Remaining > MAX_DATA_BLOCK_SIZE)
      Count = MAX_DATA_BLOCK_SIZE;
	else
	  Count = Remaining;

    if (WriteSiRFFAM (&Result, Offset, pData, Count) == FAILURE)
      return SiRFflashEngineEP_COMM_ERROR;
	else if (Result != SiRFflashEngineEP_OK)
	  return Result;
	else
	{
      Remaining -= Count;
	  pData += Count;
	  Offset += Count;
	}
  }
  return Result;
}

/*
 * Function: SiRFflashEngineEPRead
 * Purpose:  Execute SiRFFAMRead on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPRead (SiRFflashEngineEP_UINT32 Offset,
                       SiRFflashEngineEP_UINT8 * pData,
					   SiRFflashEngineEP_UINT32 Count)
{
  SiRFflashEngineEP_UINT32 Remaining = Count;

  while (Remaining)
  {
    if (Remaining > MAX_DATA_BLOCK_SIZE)
      Count = MAX_DATA_BLOCK_SIZE;
	else
	  Count = Remaining;

    if (ReadSiRFFAM (&Result, Offset, pData, Count) == FAILURE)
      return SiRFflashEngineEP_COMM_ERROR;
	else if (Result != SiRFflashEngineEP_OK)
	  return Result;
	else
	{
	  Remaining -= Count;
	  pData += Count;
	  Offset += Count;
	}
  }
  return Result;
}

/*
 * Function: SiRFflashEngineEPEraseSectorSet
 * Purpose:  Execute SiRFFAMEraseSectorSet on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPEraseSectorSet (SiRFflashEngineEP_UINT32 Offset,
								 SiRFflashEngineEP_UINT32 Size)
{
  if (EraseSectorSetSiRFFAM (&Result, Offset, Size) == SUCCESS)
    return Result;
  else
    return SiRFflashEngineEP_COMM_ERROR;
}

/*
 * Function: SiRFflashEngineEPClose
 * Purpose:  Execute SiRFFAMClose on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPClose (void)
{
  if (CloseSiRFFAM (&Result) == SUCCESS)
    return Result;
  else
    return SiRFflashEngineEP_COMM_ERROR;
}

/*
 * Function: SiRFflashEngineEPGetCapacity
 * Purpose:  Execute SiRFFAMGetCapacity on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPGetCapacity (SiRFflashEngineEP_UINT32 * pCapacity)
{
  if (GetCapacitySiRFFAM (&Result, pCapacity) == SUCCESS)
    return Result;
  else
    return SiRFflashEngineEP_COMM_ERROR;
}

/*
 * Function: SiRFflashEngineEPGetNoSectorSets
 * Purpose:  Execute SiRFFAMGetNoSectorSets on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPGetNoSectorSets (SiRFflashEngineEP_UINT32 * pNoSectorSets)
{
  if (GetNoSectorSetsSiRFFAM (&Result, pNoSectorSets) == SUCCESS)
    return Result;
  else
    return SiRFflashEngineEP_COMM_ERROR;
}

/*
 * Function: SiRFflashEngineEPGetSectorSetInfo
 * Purpose:  Execute SiRFFAMGetSectorSetInfo on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPGetSectorSetInfo (SiRFflashEngineEP_UINT32 SectorSetNo,
                                   SiRFflashEngineEP_UINT32 * pOffset,
								   SiRFflashEngineEP_UINT32 * pSize)
{
  if (GetSectorSetInfoSiRFFAM (&Result,
                               SectorSetNo, pOffset, pSize) == SUCCESS)
    return Result;
  else
    return SiRFflashEngineEP_COMM_ERROR;
}

/*
 * Function: SiRFflashEngineEPCheckReprogram
 * Purpose:  Execute SiRFFAMCheckReprogram on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPCheckReprogram (SiRFflashEngineEP_UINT32 Offset,
                                 SiRFflashEngineEP_UINT8 * pData,
								 SiRFflashEngineEP_UINT32 Count)
{
  SiRFflashEngineEP_UINT32 Remaining = Count;

  while (Remaining)
  {
    if (Remaining > MAX_DATA_BLOCK_SIZE)
      Count = MAX_DATA_BLOCK_SIZE;
	else
	  Count = Remaining;

    if (CheckReprogramSiRFFAM (&Result, Offset, pData, Count) == FAILURE)
      return SiRFflashEngineEP_COMM_ERROR;
	else if (Result != SiRFflashEngineEP_OK)
	  return Result;
	else
	{
	  Remaining -= Count;
	  pData += Count;
	  Offset += Count;
	}
  }
  return Result;

}

/*
 * Function: SiRFflashEngineEPCheckBlank
 * Purpose:  Execute SiRFFAMCheckBlank on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPCheckBlank (SiRFflashEngineEP_UINT32 Offset,
							 SiRFflashEngineEP_UINT32 Count)
{
  if (CheckBlankSiRFFAM (&Result, Offset, Count) == SUCCESS)
    return Result;
  else
    return SiRFflashEngineEP_COMM_ERROR;
}

/*
 * Function: SiRFflashEngineEPGetVersion
 * Purpose:  Execute SiRFFAMGetVersion on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPGetVersion (SiRFflashEngineEP_CHAR * pVersion)
{
  strcpy(pVersion, VersionStr);
  return SiRFflashEngineEP_OK;
}

/*
 * Function: SiRFflashEngineEPCalculateFlashCRC16
 * Purpose:  Execute SiRFFAMCalculateFlashCRC16 on remote target
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPCalculateFlashCRC16 (SiRFflashEngineEP_UINT32 Offset,
                                      SiRFflashEngineEP_UINT32 Count,
                                      SiRFflashEngineEP_UINT16 InitCRC16,
									  SiRFflashEngineEP_UINT16 * ResultCRC16)
{
  if (CalculateCRC16 (&Result,
                      Offset, Count, InitCRC16, ResultCRC16) == SUCCESS)
    return Result;
  else
    return SiRFflashEngineEP_COMM_ERROR;
}

/*
 * Function: SiRFflashEngineEPCalculateMemoryCRC16
 * Purpose:  Calculate CRC16 for local array.
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPCalculateMemoryCRC16 (SiRFflashEngineEP_UINT8 * pData,
                                       SiRFflashEngineEP_UINT32 Count,
                                       SiRFflashEngineEP_UINT16 InitCRC16,
									   SiRFflashEngineEP_UINT16 * ResultCRC16)
{
  SiRFflashEngineEP_UINT16 Result = InitCRC16;

  while (Count--)
    Result = (Result >> 8) ^ vCRC16[(Result ^ *pData++) & 0xff];

  *ResultCRC16 = Result;

  return SiRFflashEngineEP_OK;
}

/*
 * Function: SiRFflashEngineEPSetParam
 * Purpose:  Set SiRFflashEngineEP parameter.
 */

SiRFflashEngineEP_RESULT
SiRFflashEngineEPSetParam(SiRFflashEngineEP_PARAM ParamType,
					      SiRFflashEngineEP_UINT32 ParamValue)
{
	if (ParamType == SiRFflashEngineEP_GSP3_RF_CHIP_INPUT_FREQUENCY)
	{
		RFChipInputFrequency = ParamValue;
		return SiRFflashEngineEP_OK;
	}
	else return SiRFflashEngineEP_INVALID_PARAM_ERROR;
}
