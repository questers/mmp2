/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                            Demo Program                                 *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * imgacc.c                                                                *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * DESCRIPTION:                                                            *
 *                                                                         *
 * Functions in this module provide access to binary image data in two     *
 * typical cases: when binary image to program into flash is in the file   *
 * or in the memory.                                                       *
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, March 26, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#if !defined(BINARY_IMAGE_IN_MEMORY) && !defined(BINARY_IMAGE_IN_FILE)
#error "Cases where binary image is in the file or memory are supported only."
#endif

#include "SiRFflashEngineEP.h"

#if defined(BINARY_IMAGE_IN_MEMORY)
  #include <string.h>
  #include "imgdata.h"
#elif defined(BINARY_IMAGE_IN_FILE)
  #include <stdio.h>
  static FILE *fp;
#endif

static SiRFflashEngineEP_UINT32 BinaryImagePosition;

/*
 * Function: BinaryImageOpen
 * Purpose:  Start access to binary image source and return
 *           the size of binary image.
 * Inputs:   FileName - when image in file, name of the file
 *                      when image in memory, ignored
 *           BinaryImageDataSizePtr - pointer to unsigned int
 *                                    where image size should
 *                                    be returned
 * Outputs:  *BinaryImageDataPtr - image size
 *           return value - 0 if success, 1 if failure
 */


SiRFflashEngineEP_INT32
BinaryImageOpen(SiRFflashEngineEP_CHAR *FileName,
				SiRFflashEngineEP_UINT32 *BinaryImageDataSizePtr)
{
#if defined(BINARY_IMAGE_IN_MEMORY)
	*BinaryImageDataSizePtr = ImageData_size;
#elif defined(BINARY_IMAGE_IN_FILE) 
	fp = fopen(FileName, "rb");
	if (fp == NULL)
		return 1;
	fseek(fp, 0, SEEK_END);
	*BinaryImageDataSizePtr = ftell(fp);
	fseek(fp, 0, SEEK_SET);
#endif
	BinaryImagePosition = 0;
	return 0;
}

/*
 * Function: BinaryImageReadNextBlock
 * Purpose:  Read next block of image data into user
 *           specified buffer.
 * Inputs:   DataPtr - pointer to user data buffer
 *           Size    - block size
 * Output:   0 if success, 1 if failure
 */

SiRFflashEngineEP_INT32
BinaryImageReadNextBlock(SiRFflashEngineEP_UINT8 *DataPtr,
						 SiRFflashEngineEP_UINT32 Size)
{
	int Result;

#if defined(BINARY_IMAGE_IN_MEMORY)
	memcpy(DataPtr, &ImageData[BinaryImagePosition], Size);
	Result = 0;
#elif defined(BINARY_IMAGE_IN_FILE)
	Result = (fread(DataPtr, 1, Size, fp) == Size) ? 0 : 1; 
#endif

	BinaryImagePosition += Size;
	return Result;
}

/*
 * Function: BinaryImageClose
 * Purpose:  Close binary image container.
 * Input:    None
 * Output:   Always returns 0.
 */

SiRFflashEngineEP_INT32
BinaryImageClose(void)
{
#if defined(BINARY_IMAGE_IN_MEMORY)
#elif defined(BINARY_IMAGE_IN_FILE)
	fclose(fp);
#endif

	return 0;
}
