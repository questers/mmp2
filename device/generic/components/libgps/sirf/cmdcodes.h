/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * cmdcodes.h                                                              *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, February 24, 2005, Voya Protic (vprotic@sirf.com)          *
 *                                                                         *
 ***************************************************************************/

#ifndef __CMDCODES_H
#define __CMDCODES_H

typedef enum {SET_BAUD_RATE = 0,
  CONFIRM_SET_BAUD_RATE = 1,
  LOAD_RAM = 2,
  CONFIRM_LOAD_RAM = 3,
  START_PROGRAM = 4,
  CONFIRM_START_PROGRAM = 5,
  SIRFFAM_OPEN = 6,
  RESPONSE_SIRFFAM_OPEN = 7,
  SIRFFAM_WRITE = 8,
  RESPONSE_SIRFFAM_WRITE = 9,
  SIRFFAM_READ = 10,
  RESPONSE_SIRFFAM_READ = 11,
  SIRFFAM_ERASE_SECTOR_SET = 12,
  RESPONSE_SIRFFAM_ERASE_SECTOR_SET = 13,
  SIRFFAM_CLOSE = 14,
  RESPONSE_SIRFFAM_CLOSE = 15,
  SIRFFAM_GET_CAPACITY = 16,
  RESPONSE_SIRFFAM_GET_CAPACITY = 17,
  SIRFFAM_GET_NO_SECTOR_SETS = 18,
  RESPONSE_SIRFFAM_GET_NO_SECTOR_SETS = 19,
  SIRFFAM_GET_SECTOR_SET_INFO = 20,
  RESPONSE_SIRFFAM_GET_SECTOR_SET_INFO = 21,
  SIRFFAM_CHECK_REPROGRAM = 22,
  RESPONSE_SIRFFAM_CHECK_REPROGRAM = 23,
  SIRFFAM_CHECK_BLANK = 24,
  RESPONSE_SIRFFAM_CHECK_BLANK = 25,
  SIRFFAM_SET_WRITE_ATOMIC_UNIT_SIZE = 26,
  RESPONSE_SIRFFAM_SET_WRITE_ATOMIC_UNIT_SIZE = 27,
  SIRFFAM_GET_VERSION = 28,
  RESPONSE_SIRFFAM_GET_VERSION = 29,
  SIRFFAM_CALCULATE_CRC16 = 30,
  RESPONSE_SIRFFAM_CALCULATE_CRC16 = 31,
  SIRFFAM_CREATE = 32,
  RESPONSE_SIRFFAM_CREATE = 33,
  TARGET_MAY_EXIT = 34,
  RESPONSE_TARGET_MAY_EXIT = 35,
  LOAD_STARTER = 36,
  CONFIRM_LOAD_STARTER = 37,
  SETUP_GSP2_CHIP = 38,
  CONFIRM_SETUP_GSP2_CHIP = 39,
  SETUP_GSP2LPX_CHIP = 40,
  CONFIRM_SETUP_GSP2LPX_CHIP = 41,
  SETUP_GSP3_CHIP = 42,
  CONFIRM_SETUP_GSP3_CHIP = 43,
  GET_CHIP_VERSION = 44,
  RESPONSE_GET_CHIP_VERSION = 45,
  GET_RF_CHIP_VERSION = 46,
  RESPONSE_GET_RF_CHIP_VERSION = 47,
  GET_CHIP_CONFIG = 48,
  RESPONSE_GET_CHIP_CONFIG = 49,
  GET_ROM_CONFIG = 50,
  RESPONSE_GET_ROM_CONFIG = 51
} COMMAND_CODE;

typedef enum {BRIDGE_SET_BAUD_RATE = 128,
  CONFIRM_BRIDGE_SET_BAUD_RATE = 129,
  BRIDGE_MAY_EXIT = 130,
  RESPONSE_BRIDGE_MAY_EXIT = 131
} BRIDGE_COMMAND_CODE;
  
#define GSP3_BASEBAND_CHIP			0x30
#define GSP2ELP_BASEBAND_CHIP      	0x23
#define GSP2LPX_BASEBAND_CHIP		0x26
#define GSP3LT_BASEBAND_CHIP		0x35
#define GSP3BT_BASEBAND_CHIP		0x36
#define UNSUPPORTED_BASEBAND_CHIP	0

#define GRF3W_RF_CHIP				0
#define GRF3I_RF_CHIP               1                
#define GRF3BT_RF_CHIP				2

#endif
