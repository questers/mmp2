#ifndef _SIRF_LOADER_H_
#define SIRF_LOADER_H_

#ifdef __cplusplus
extern "C" {
#endif

int sirf_loader (const char* bin_file,const char* port,int baudrate);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
