/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP_ - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * command.h                                                               *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#ifndef __COMMAND_H
#define __COMMAND_H

extern SiRFflashEngineEP_INT32 SetBaudRate (SiRFflashEngineEP_INT32 baudrate,
						                    SiRFflashEngineEP_UINT32 TargetBasebandChip);

extern SiRFflashEngineEP_INT32 TargetMayExit(void);

extern SiRFflashEngineEP_INT32 BridgeMayExit(void);

extern SiRFflashEngineEP_INT32 OpenSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                                            SiRFflashEngineEP_UINT32 ChipSelect,
                                            SiRFflashEngineEP_UINT32 ChipOffset,
                                            SiRFflashEngineEP_UINT32 * pNoChips,
                                            SiRFflashEngineEP_BOOL * pIsEraseSuspendSupported,
                                            SiRFflashEngineEP_FLASH_DESC * pFD);

extern SiRFflashEngineEP_INT32 CreateSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                                              SiRFflashEngineEP_UINT32 ChipSelect,
                                              SiRFflashEngineEP_UINT32 ChipOffset,
                                              SiRFflashEngineEP_UINT32 * pNoChips,
                                              SiRFflashEngineEP_BOOL * pIsEraseSuspendSupported,
                                              SiRFflashEngineEP_FLASH_DESC * pFD);

extern SiRFflashEngineEP_INT32 WriteSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                                             SiRFflashEngineEP_UINT32 Offset,
                                             SiRFflashEngineEP_UINT8 * pData,
											 SiRFflashEngineEP_UINT32 Count);

extern SiRFflashEngineEP_INT32 ReadSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                                            SiRFflashEngineEP_UINT32 Offset,
                                            SiRFflashEngineEP_UINT8 * pData,
											SiRFflashEngineEP_UINT32 Count);

extern SiRFflashEngineEP_INT32 EraseSectorSetSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                                                      SiRFflashEngineEP_UINT32 Offset,
													  SiRFflashEngineEP_UINT32 Size);

extern SiRFflashEngineEP_INT32 CloseSiRFFAM (SiRFflashEngineEP_RESULT * pResult);

extern SiRFflashEngineEP_INT32 GetCapacitySiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                                                   SiRFflashEngineEP_UINT32 * pCapacity);

extern SiRFflashEngineEP_INT32 GetNoSectorSetsSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                                                       SiRFflashEngineEP_UINT32 * pNoSectorSets);

extern SiRFflashEngineEP_INT32 GetSectorSetInfoSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                                                        SiRFflashEngineEP_UINT32 SectorSetNo,
                                                        SiRFflashEngineEP_UINT32 * pOffset,
                                                        SiRFflashEngineEP_UINT32 * pSize);

extern SiRFflashEngineEP_INT32 CheckReprogramSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                                                      SiRFflashEngineEP_UINT32 Offset,
                                                      SiRFflashEngineEP_UINT8 * pData,
                                                      SiRFflashEngineEP_UINT32 Count);

extern SiRFflashEngineEP_INT32 CheckBlankSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                                                  SiRFflashEngineEP_UINT32 Offset,
												  SiRFflashEngineEP_UINT32 Count);

extern SiRFflashEngineEP_INT32 SetWriteAtomicUnitSizeSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                                                              SiRFflashEngineEP_UINT32 Size);

extern SiRFflashEngineEP_INT32 GetVersionSiRFFAM (SiRFflashEngineEP_RESULT * pResult,
                                                  SiRFflashEngineEP_CHAR * pVersion);


extern SiRFflashEngineEP_INT32 CalculateCRC16 (SiRFflashEngineEP_RESULT * pResult,
                                               SiRFflashEngineEP_UINT32 Offset,
                                               SiRFflashEngineEP_UINT32 Count,
                                               SiRFflashEngineEP_UINT16 InitCRC16,
                                               SiRFflashEngineEP_UINT16 * pResultCRC16);

extern SiRFflashEngineEP_INT32 GetChipVersion(SiRFflashEngineEP_UINT8 *pChipVersion);

extern SiRFflashEngineEP_INT32 GetRFChipVersion(SiRFflashEngineEP_UINT8 *pRFChipVersion);

extern SiRFflashEngineEP_INT32 GetChipConfig(SiRFflashEngineEP_UINT16 *pChipConfig);

extern SiRFflashEngineEP_INT32 GetRomConfig(SiRFflashEngineEP_UINT16 *pRomConfig);

extern SiRFflashEngineEP_INT32 SetupGSP2Chip(SiRFflashEngineEP_UINT8 BusClockDivider);

extern SiRFflashEngineEP_INT32 SetupGSP2LPXChip(SiRFflashEngineEP_UINT32 TargetRFChip,
											 SiRFflashEngineEP_UINT32 RFChipInputFrequency);
extern SiRFflashEngineEP_INT32 SetupGSP3Chip(SiRFflashEngineEP_UINT32 TargetRFChip,
											 SiRFflashEngineEP_UINT32 RFChipInputFrequency);

extern SiRFflashEngineEP_INT32 SetupGSP3LTChip(SiRFflashEngineEP_UINT32 TargetRFChip,
											   SiRFflashEngineEP_UINT32 RFChipInputFrequency);
extern SiRFflashEngineEP_INT32 SetupGSP3BTChip(SiRFflashEngineEP_UINT32 TargetRFChip,
											   SiRFflashEngineEP_UINT32 RFChipInputFrequency);
#endif
