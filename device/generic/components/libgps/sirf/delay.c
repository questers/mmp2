/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * delay.c                                                                 *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * DESCRIPTION:                                                            *
 *                                                                         *
 * Function in this module creates delay of given length.                  *
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/


#include <time.h>
#include "global.h"
#include "SiRFflashEngineEP.h"
#include "delay.h"

/*
 * Function: DelayMs
 * Purpose: Wait for specified number of milliseconds
 */

void DelayMs(SiRFflashEngineEP_INT32 milliseconds)
{
	struct timespec req, rem;

	req.tv_sec =  milliseconds / 1000;
	req.tv_nsec = (milliseconds % 1000) * 1000000;
	
	while (!((req.tv_sec == 0) && (req.tv_nsec == 0)))
	{
		if (nanosleep(&req, &rem) == 0)
			return;
		req.tv_sec = rem.tv_sec;
		req.tv_nsec = rem.tv_nsec;
	}
}
