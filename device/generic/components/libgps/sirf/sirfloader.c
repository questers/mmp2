/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                            Demo Program                                 *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * main.c                                                                  *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * DESCRIPTION:                                                            *
 *                                                                         *
 * Demo program for SiRFflashEngineEP. Shows how to use various            *
 * SiRFflashEngineEP API functions to program given image to flash.        *
 *                                                                         *
 * USAGE:                                                                  *
 *                                                                         *
 * When invoked from the command line, prgflash invocation line should     *
 * look like this:                                                         *
 *                                                                         *
 * prgflash <image_filename> <COMn> <baudrate> [via_bridge]                *
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#if !defined(BINARY_IMAGE_IN_MEMORY) && !defined(BINARY_IMAGE_IN_FILE)
#error "Cases where binary image is in the file or memory are supported only."
#endif

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "SiRFflashEngineEP.h"
#include "imgacc.h"
#include "errsup.h"
#include "commio.h"
#include "timsup.h"
#include "global.h"


#define min(x,y)    (((x) < (y)) ? (x) : (y))
#undef BLOCK_SIZE
#ifndef BLOCK_SIZE
#define BLOCK_SIZE	4096
#endif

struct st_sirf_loader
{	
	SiRFflashEngineEP_UINT32 	  NoChips;
	SiRFflashEngineEP_FLASH_DESC   FD;
	SiRFflashEngineEP_CHAR		  DeviceName[64];
	SiRFflashEngineEP_CHAR		  ManufacturerName[64];
	SiRFflashEngineEP_CHAR		  Version[64];
	SiRFflashEngineEP_UINT32 	  Offset;
	SiRFflashEngineEP_UINT32 	  Size;
	SiRFflashEngineEP_SECTOR_GROUP SectorGroups[6];
	
	double						  StartTime, EndTime;
	SiRFflashEngineEP_UINT8		  Buffer[BLOCK_SIZE];
	SiRFflashEngineEP_UINT16 	  HostCRC, TargetCRC;
	SiRFflashEngineEP_UINT32 	  BaudRate;
	SiRFflashEngineEP_CHAR		  ComPortName[10];
	SiRFflashEngineEP_CHAR		  *FileNamePtr;
	SiRFflashEngineEP_UINT32 	  BinaryImageDataSize;
	SiRFflashEngineEP_UINT32 	  Remaining;
	SiRFflashEngineEP_UINT8		  packetBuffer[1660 + 14];
};

#define ERR_CHECK(x) \
	if(SiRFflashEngineEP_OK!=x) goto RET;
	
//int sirf_loader (int argc, char *argv[])
int sirf_loader (const char* bin_file,const char* port,int baudrate)
{
	extern SiRFflashEngineEP_UINT8 *PacketBuff;

	SiRFflashEngineEP_RESULT 	  Result;
	SiRFflashEngineEP_BOOL ViaBridge = 0;
	struct st_sirf_loader* loader;
	loader = calloc(1,sizeof(struct st_sirf_loader));
	if(!loader)
	{
		ReportErrorAndExit("memory low");
		return -1;		
	}

	PacketBuff = loader->packetBuffer;

#if defined(BINARY_IMAGE_IN_FILE)
	loader->FileNamePtr = (SiRFflashEngineEP_CHAR*)bin_file;
	strcpy(loader->ComPortName, port);
	loader->BaudRate = baudrate;

#elif defined(BINARY_IMAGE_IN_MEMORY)

	loader->FileNamePtr = NULL;
	strcpy(loader->ComPortName, "COM1");
	loader->BaudRate = 115200;

#endif


	CommSetBridgePresent(ViaBridge);

	SIRF_DEBUG("on port[%s],firmware[%s]",loader->ComPortName,loader->FileNamePtr);
	if (BinaryImageOpen(loader->FileNamePtr, &loader->BinaryImageDataSize) != 0)
	{
		ReportErrorAndExit("Failed to open image source");
		return -1;
	}

	loader->StartTime = TimeInSecs ();

	Result = SiRFflashEngineEPStartSession(loader->ComPortName, loader->BaudRate);

	ERR_CHECK(Result);

	 // Get version of the software
	Result = SiRFflashEngineEPGetVersion(&loader->Version[0]);

	
	ERR_CHECK(Result);
	

	SIRF_INFO("SiRFflashEngineEP version: %s\n", loader->Version);

	// Open flash device
	loader->FD.Fname.DeviceName = &loader->DeviceName[0];
	loader->FD.Fname.ManufacturerName = &loader->ManufacturerName[0];
	loader->FD.SectorGroupPtr = &loader->SectorGroups[0];

	Result = SiRFflashEngineEPOpen(0,        /* ChipSelect */
								   0,        /* ChipOffset */
								   &loader->NoChips, /* NoChips */
								   &loader->FD);     /* FlashDescriptor */

	ERR_CHECK(Result);

	SIRF_INFO("%s %s %s %s found on the target\n",
		   (loader->NoChips == 1) ? "One" : "Two",
		   loader->FD.Fname.ManufacturerName, loader->FD.Fname.DeviceName,
		   (loader->NoChips == 1) ? "chip" : "chips");

	// Check if flash area that should be programmed is blank
	Result = SiRFflashEngineEPCheckBlank(0, loader->BinaryImageDataSize);

	if ((Result != SiRFflashEngineEP_OK) && (Result != SiRFflashEngineEP_AREA_NOT_BLANK))
		goto RET;
	
	// If not, erase minimal set of sectors
	if (Result == SiRFflashEngineEP_AREA_NOT_BLANK) {
		SiRFflashEngineEP_UINT32 SectorSetNo = 0;

		SIRF_DEBUG("Erasing...\n");

		Result = SiRFflashEngineEPGetSectorSetInfo(SectorSetNo++, &loader->Offset, &loader->Size);

		ERR_CHECK(Result);

		while (loader->Offset < loader->BinaryImageDataSize) {
			Result = SiRFflashEngineEPEraseSectorSet(loader->Offset, loader->Size);

			ERR_CHECK(Result);

			if (loader->Offset + loader->Size < loader->FD.Capacity) {
				Result = SiRFflashEngineEPGetSectorSetInfo(SectorSetNo++, &loader->Offset, &loader->Size);

				ERR_CHECK(Result);

				//SIRF_DEBUG("."); //fflush(stdout);
			}
			else
				break;
		}
		SIRF_DEBUG("\nErasing done.\n");
	}

	// Program flash memory

	SIRF_DEBUG("Programming...\n");

	loader->Remaining = loader->BinaryImageDataSize;
	loader->Offset = 0;
	loader->HostCRC = 0xffff;

	while (loader->Remaining) {
		SiRFflashEngineEP_UINT32 ThisSize;

		ThisSize = min(loader->Remaining, BLOCK_SIZE);

		Result = BinaryImageReadNextBlock(&loader->Buffer[0], ThisSize);
		
		ERR_CHECK(Result);

		SIRF_INFO("programming @x%08x,remaining[%08u]",(unsigned int)loader->Offset,(unsigned int)loader->Remaining);
		SiRFflashEngineEPCalculateMemoryCRC16((SiRFflashEngineEP_UINT8 *)&loader->Buffer[0], ThisSize, loader->HostCRC, &loader->HostCRC);

		Result = SiRFflashEngineEPWrite(loader->Offset, &loader->Buffer[0], ThisSize);

		ERR_CHECK(Result);

		loader->Remaining -= ThisSize;
		loader->Offset += ThisSize;
	}

	SIRF_DEBUG("\nProgramming done.\n");

	Result = SiRFflashEngineEPCalculateFlashCRC16(0, loader->BinaryImageDataSize, 0xffff, &loader->TargetCRC);

	ERR_CHECK(Result);

	SIRF_DEBUG("CRC comparison %s\n", (loader->TargetCRC == loader->HostCRC) ? "passed" : "failed");

	loader->EndTime = TimeInSecs ();

	SIRF_DEBUG("Execution time: %5.1f seconds\n", loader->EndTime - loader->StartTime);

	// Close flash device
	Result = SiRFflashEngineEPClose ();

	// Terminate flashing session
	Result = SiRFflashEngineEPEndSession ();

	// Close binary image
	BinaryImageClose();

	free(loader);
	return 0;
RET:
	/* clean up when fail. -guang */ 
	SiRFflashEngineEPEndSession();
	BinaryImageClose();

	free(loader);
	ReportErrorAndExit(DecodeError(Result));
	return Result;
}


