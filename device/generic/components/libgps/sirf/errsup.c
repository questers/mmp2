/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                            Demo Program                                 *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * errsup.c                                                                *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * DESCRIPTION:                                                            *
 *                                                                         *
 * Functions in this module decode error codes returned by the EP engine   *
 * and report fatal errors before exiting.                                 *
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "SiRFflashEngineEP.h"
#include "errsup.h"
#include "global.h"

/*
 * Function: DecodeError
 * Purpose:  Take error code and return pointer to error string 
 */

SiRFflashEngineEP_CHAR
*DecodeError (SiRFflashEngineEP_RESULT Result)
{
	switch (Result)
	{
	case SiRFflashEngineEP_OK:
		return "OK";
	case SiRFflashEngineEP_FLASH_ALREADY_OPENED:
		return "Flash already opened";
	case SiRFflashEngineEP_BAD_CHIP_SELECT:
		return "Bad chip select";
	case SiRFflashEngineEP_ERASE_SECTOR_SET_ERROR:
		return "Erase sector error";
	case SiRFflashEngineEP_FLASH_WRITE_ERROR:
		return "Flash write error";
	case SiRFflashEngineEP_FLASH_NOT_FOUND:
		return "Flash not found";
	case SiRFflashEngineEP_FLASH_NOT_OPENED:
		return "Flash not opened";
	case SiRFflashEngineEP_UNKNOWN_FLASH_TYPE:
		return "Unknown flash type";
	case SiRFflashEngineEP_INVALID_OFFSET:
		return "Invalid offset";
	case SiRFflashEngineEP_INVALID_ERASE_SIZE:
		return "Invalid erase size";
	case SiRFflashEngineEP_BUS_WIDTH_NOT_16_OR_32:
		return "Bus width is not 16 nor 32";
	case SiRFflashEngineEP_CAN_NOT_REPROGRAM:
		return "Can not reprogram flash";
	case SiRFflashEngineEP_AREA_NOT_BLANK:
		return "Area is not blank";
	case SiRFflashEngineEP_INVALID_SECTOR_SET_NUMBER:
		return "Sector number is invalid";
	case SiRFflashEngineEP_UNSUPPORTED_GSP2_VERSION:
		return "This flavor of GSP2 chip is not supported";
	case SiRFflashEngineEP_INVALID_FLASH_DESCRIPTOR_POINTER:
		return "Flash descriptor pointer is invalid";
	case SiRFflashEngineEP_INVALID_FLASH_DESCRIPTOR:
		return "Flash descriptor is invalid";
	case SiRFflashEngineEP_SPECIFIED_FLASH_NOT_FOUND:
		return "Specified flash not found";
	case SiRFflashEngineEP_UNSUPPORTED_GSP3_CONFIGURATION:
		return "Unsupported GSP3 configuration found";
	case SiRFflashEngineEP_INVALID_PARAM_ERROR:
		return "Invalid parameter specified";
	case SiRFflashEngineEP_SESSION_ALREADY_STARTED:
		return "Session already started";
	case SiRFflashEngineEP_SESSION_NOT_STARTED:
		return "Session not started";
	case SiRFflashEngineEP_COMM_LINE_SETUP_ERROR:
		return "Comm line setup error";
	case SiRFflashEngineEP_INITIAL_LOAD_ERROR:
		return "Initial load error";
	case SiRFflashEngineEP_COMM_ERROR:
		return "Communication error";
	case SiRFflashEngineEP_INVALID_AUTO_RF_INPUT_CLOCK:
		return "GRF3w clock frequency not supported in auto mode"; 
	default:
		return "Unknown error";
	}
}

/*
 * Function: Report error
 * Purpose:  Output error message and exit
 */

void 
ReportErrorAndExit (SiRFflashEngineEP_CHAR *Str)
{
	SIRF_ERROR("Error: %s\n", Str);
}

