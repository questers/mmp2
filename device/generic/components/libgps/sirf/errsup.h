#ifndef _ERRSUP_H_
#define _ERRSUP_H_

extern SiRFflashEngineEP_CHAR *DecodeError(SiRFflashEngineEP_RESULT Result);
extern void ReportErrorAndExit(SiRFflashEngineEP_CHAR *Str);

#endif
