/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                            Demo Program                                 *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * timsup.c                                                                *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * DESCRIPTION:                                                            *
 *                                                                         *
 * Function in this module returns time in seconds.                        *
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, March 26, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#include <time.h>
#include <sys/times.h>
#include <unistd.h>

struct tms buf;

double TimeInSecs(void)
{
	return times(&buf) / sysconf(_SC_CLK_TCK);
}
