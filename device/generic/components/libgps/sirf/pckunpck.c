/***************************************************************************
 *                                                                         *
 *  SiRF Technology, Inc. SiRFflashEngineEP - SiRFflash Engine Easy Port   *
 *                                                                         *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source       *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or         *
 *  utilization of this source in whole or in part is forbidden            *
 *  without the written consent of SiRF Technology, Inc.                   *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *      (c) SiRF Technology, Inc. 2001 - 2005 -- All Rights Reserved       *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * pckunpck.c                                                              *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 * DESCRIPTION:                                                            *
 *                                                                         *
 * Functions in this module perform packing/unpacking to/from big endian.  *
 *                                                                         *
 * HISTORY:                                                                *
 *                                                                         *
 * Version 2.0, April 24, 2005, Voya Protic (vprotic@sirf.com)             *
 *                                                                         *
 ***************************************************************************/

#include "SiRFflashEngineEP.h"
#include "global.h"
#include "pckunpck.h"

/*
 * Function: PackByte
 * Purpose:  Pack a byte to given location in big endian
 *           and increment pointer.
 */

void
PackByte (SiRFflashEngineEP_UINT8 Val, SiRFflashEngineEP_UINT8 ** DstPtr)
{
  SiRFflashEngineEP_UINT8 *Dst = *DstPtr;

  *Dst++ = Val;
  *DstPtr = Dst;
}

/*
 * Function: PackWord
 * Purpose:  Pack a word to given location in big endian
 *           and increment pointer.
 */

void
PackWord (SiRFflashEngineEP_UINT16 Val, SiRFflashEngineEP_UINT8 ** DstPtr)
{
  SiRFflashEngineEP_UINT8 *Dst = *DstPtr;

  *Dst++ = (SiRFflashEngineEP_UINT8) (Val >> 8);
  *Dst++ = (SiRFflashEngineEP_UINT8) Val;
  *DstPtr = Dst;
}

/*
 * Function: PackDword
 * Purpose:  Pack a dword to given location in big endian
 *           and increment pointer.
 */

void
PackDword (SiRFflashEngineEP_UINT32 Val, SiRFflashEngineEP_UINT8 ** DstPtr)
{
  SiRFflashEngineEP_UINT8 *Dst = *DstPtr;

  *Dst++ = (SiRFflashEngineEP_UINT8) (Val >> 24);
  *Dst++ = (SiRFflashEngineEP_UINT8) (Val >> 16);
  *Dst++ = (SiRFflashEngineEP_UINT8) (Val >> 8);
  *Dst++ = (SiRFflashEngineEP_UINT8) Val;
  *DstPtr = Dst;
}

/*
 * Function: PackString
 * Purpose:  Pack a string to given location
 *           and increment pointer.
 */

void
PackString (SiRFflashEngineEP_UINT8 * StrPtr, SiRFflashEngineEP_UINT8 ** DstPtr)
{
  SiRFflashEngineEP_UINT8 *Dst = *DstPtr;

  while (*StrPtr)
    *Dst++ = *StrPtr++;
  *Dst++ = 0;
  *DstPtr = Dst;
}

/*
 * Function: UnpackByte
 * Purpose:  Unpack a byte from given location in big endian
 *           and increment pointer.
 */

SiRFflashEngineEP_UINT8
UnpackByte (SiRFflashEngineEP_UINT8 ** SrcPtr)
{
  SiRFflashEngineEP_UINT8 Result;
  SiRFflashEngineEP_UINT8 *Src = *SrcPtr;

  Result = *Src++;
  *SrcPtr = Src;
  return Result;
}

/*
 * Function: UnpackWord
 * Purpose:  Unpack a word from given location in big endian
 *           and increment pointer.
 */

SiRFflashEngineEP_UINT16
UnpackWord (SiRFflashEngineEP_UINT8 ** SrcPtr)
{
  SiRFflashEngineEP_UINT16 Result;
  SiRFflashEngineEP_UINT8 *Src = *SrcPtr;

  Result = *Src++;
  Result = (Result << 8) + *Src++;
  *SrcPtr = Src;
  return Result;

}

/*
 * Function: UnpackDword
 * Purpose:  Unpack a dword from given location in big endian
 *           and increment pointer.
 */

SiRFflashEngineEP_UINT32
UnpackDword (SiRFflashEngineEP_UINT8 ** SrcPtr)
{
  SiRFflashEngineEP_UINT32 Result;
  SiRFflashEngineEP_UINT8 *Src = *SrcPtr;

  Result = *Src++;
  Result = (Result << 8) + *Src++;
  Result = (Result << 8) + *Src++;
  Result = (Result << 8) + *Src++;
  *SrcPtr = Src;
  return Result;
}
