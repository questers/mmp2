#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <fcntl.h>
#include <termios.h>
#include <math.h>
#include <time.h>
#include <semaphore.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define  LOG_TAG  "gps_door"

#include <cutils/log.h>
#include <cutils/sockets.h>
#include "cmd_disp.h"
#include "gps_door.h"



static int door_sock=-1;
static pthread_t door_thread;

static void* gps_backdoor(void* arg)
{
	int client_sock=-1;

	//detach from parent thread
	pthread_detach(pthread_self());

	LOGI("start gps backdoor task");
	//should we always online
	while(1)
	{		
        fd_set read_fds;
        struct timeval to;
        int max = 0;
        int rc = 0;

        to.tv_sec = (60 * 60);
        to.tv_usec = 0;

        FD_ZERO(&read_fds);
        FD_SET(door_sock, &read_fds);
        if (door_sock > max)
            max = door_sock;

		
        if (client_sock != -1) {
            FD_SET(client_sock, &read_fds);
            if (client_sock > max)
                max = client_sock;
        }

		if ((rc = select(max + 1, &read_fds, NULL, NULL, &to)) < 0) 
		{
            LOGE("select() failed (%s)", strerror(errno));
            sleep(1);
            continue;
        }
        if (!rc) 
		{
            continue;
        }

		//handle server acceptance		
        if (FD_ISSET(door_sock, &read_fds)) {
            struct sockaddr addr;
            socklen_t alen;

            alen = sizeof(addr);
            if (client_sock != -1) {
                LOGE("Dropping duplicate gps connection");
                int tmp = accept(door_sock, &addr, &alen);
                close(tmp);
                continue;
            }

            if ((client_sock = accept(door_sock, &addr, &alen)) < 0) {
                LOGE("Unable to accept gps connection (%s)",
                     strerror(errno));
            }
        }

		//handle client request
        if (FD_ISSET(client_sock, &read_fds)) 
		{
	        if ((rc = process_gps_command(client_sock)) < 0) 
			{
                if (rc == -ECONNRESET) {
                    LOGE("gps socket disconnected");
                    close(client_sock);
                    client_sock = -1;
                } else {
                    LOGE("Error processing gps command (%s)",
                         strerror(errno));
                }
            }
        }
		
	}
	
	return NULL;
}

int init_gps_door(void)
{
	if(door_sock<0)
	{	
		LOGI("create GPS door task");
		door_sock = socket_local_server(GPS_DOOR_NAME, 
			ANDROID_SOCKET_NAMESPACE_ABSTRACT, SOCK_STREAM);
 
 		pthread_create( &door_thread, NULL, gps_backdoor, NULL ) ;
	}
	return 0;
}

