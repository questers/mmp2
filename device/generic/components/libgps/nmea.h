#ifndef _NMEA_H_
#define _NMEA_H_

#include <hardware/gps.h>
#ifdef __cplusplus
extern "C" {
#endif

typedef struct nmea_object * nmea_object_t;

int nmea_init(nmea_object_t* nmea,GpsCallbacks* callbacks,void* private);
void nmea_process(nmea_object_t nmea,char *nmea_sentence, int nmea_sentence_length);int nmea_dispose(nmea_object_t nmea);

int nmea_set_positionmode(nmea_object_t nmea,GpsPositionMode mode, int fix_frequency);

#ifdef __cplusplus
} /* extern "C" */
#endif


#endif
