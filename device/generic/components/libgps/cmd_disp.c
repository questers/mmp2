#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <cutils/properties.h>

#define  LOG_TAG  "gps_cmd"
#include <cutils/log.h>

#include "cmd_disp.h"
#include "gps_hal.h"

struct cmd_disp {
    char *cmd;
    int (* disp) (char *);
};

static int do_connect(char *cmd)
{
	gps_hal_start(NULL);
	return 0;
}

static int do_disconnect(char *cmd)
{
	gps_hal_stop(NULL);
	return 0;
}

static int do_dump_nmea(char *cmd)
{
    int result = 0;
    char* cmdline = cmd + strlen( CMD_DUMP_NMEA);
    char* args[1];//param
   
	
    int i = 0;
    for(;i<1;i++ ) {
		while(*cmdline==' ') cmdline++;
        args[i] = cmdline;
        do{
            cmdline++;
        } while( (*cmdline != ';') && ( *cmdline != '\0') );
        if( *cmdline != '\0' )
            *cmdline = '\0';        
          cmdline++;
    }

	if(!strncmp(args[0],"on",strlen("on")))
		gps_hal_cmd(GPS_HAL_CMD_DUMP_RAW,1);
	else if(!strncmp(args[0],"off",strlen("off")))
		gps_hal_cmd(GPS_HAL_CMD_DUMP_RAW,0);		
	else
	{
		LOGE("usage %s [on|off]",CMD_DUMP_NMEA);
		result = -1;
	}
	
	return result;
}

static int do_update(char *cmd)
{
	//just pass to gps hal layer
	gps_hal_update();
	
	return 0;
}

static int do_setdelay(char *cmd)
{
    int delay;
    char* cmdline = cmd + strlen( CMD_DELAY);
    char* args[1];//param
   
	
    int i = 0;
    for(;i<1;i++ ) {
		while(*cmdline==' ') cmdline++;
        args[i] = cmdline;
        do{
            cmdline++;
        } while( (*cmdline != ';') && ( *cmdline != '\0') );
        if( *cmdline != '\0' )
            *cmdline = '\0';        
          cmdline++;
    }

    delay = (int)strtoul(args[0],NULL,0);
    gps_hal_cmd(GPS_HAL_CMD_DELAY,delay);	
	
    return 0;
	
}

static int do_firmware_check(char *cmd)
{
	LOGI("do gps firmware check");
	if(!gps_hal_firmware_loaded())
	{
		LOGI("firmware not loaded,run firmware loader");
		gps_hal_fwck();
	}
	return 0;
}

static int do_preupdate(char *cmd)
{
	LOGI("do gps pre update");
	gps_hal_cmd(GPS_HAL_CMD_UPDATE_PRE,0);
	return 0;
}

static int do_postupdate(char *cmd)
{
	LOGI("do gps post update");
	gps_hal_cmd(GPS_HAL_CMD_UPDATE_POST,0);
	return 0;
}

static struct cmd_disp dispatch_table[] = {
    { CMD_CONNECT,        do_connect },
    { CMD_DISCONNECT,     do_disconnect },
    { CMD_DUMP_NMEA,     do_dump_nmea },
	{ CMD_FW_UPDATE,	 do_update },    
    { CMD_DELAY,		do_setdelay},
	{ CMD_FIRMWARE_CHECKER,	do_firmware_check},
    { CMD_UPDATE_PRE,	do_preupdate},
    { CMD_UPDATE_POST,	do_postupdate},    
    { NULL, NULL }
};

static void dispatch_cmd(char *cmd)
{
    struct cmd_disp *c;
    for (c = dispatch_table; c->cmd != NULL; c++) {
        if (!strncmp(c->cmd, cmd, strlen(c->cmd))) {
            c->disp(cmd);
            return;
        }
    }
    LOGE("No gps cmd handlers defined for '%s'", cmd);
}


int process_gps_command(int socket)
{
    int rc;
    char buffer[256];

    if ((rc = read(socket, buffer, sizeof(buffer) -1)) < 0) {
        LOGE("Unable to read gps command (%s)", strerror(errno));
        return -errno;
    } else if (!rc)
        return -ECONNRESET;

    int start = 0;
    int i;

    buffer[rc] = 0;

    for (i = 0; i < rc; i++) {
        if (buffer[i] == 0) {
            dispatch_cmd(buffer + start);
            start = i + 1;
        }
    }
    return 0;
}


