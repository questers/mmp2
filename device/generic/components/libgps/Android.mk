
LOCAL_PATH:= $(call my-dir)

ifeq ($(strip $(BOARD_GPS_SUPPLIER)), SIRF)

GPS_TOP := $(LOCAL_PATH)


########################
# GPS firmware binary
#
include $(CLEAR_VARS)
file := $(TARGET_OUT)/etc/firmware/sirf.bin
$(file) : $(LOCAL_PATH)/GSW3.2.5P3.0_NMEA9600_100727.bin | $(ACP)
	$(transform-prebuilt-to-target)
ALL_PREBUILT += $(file)


########################
# GPS firmware upgrade utility
#
include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE:= gpsloader
LOCAL_SHARED_LIBRARIES := libcutils libc 
LOCAL_CFLAGS := 
LOCAL_SRC_FILES:= \
	loader.c

LOCAL_STATIC_LIBRARIES:=libsirfloader	
	
include $(BUILD_EXECUTABLE)


# HAL module implemenation, not prelinked and stored in
# hw/<SENSORS_HARDWARE_MODULE_ID>.<ro.product.board>.so
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
	gps.c \
	nmea.c \
	gps_door.c \
	cmd_disp.c \
	gps_hal.c


LOCAL_CFLAGS += \
	-fno-short-enums 

ifneq ($(BOARD_GPS_BAD_AGPS),)
LOCAL_CFLAGS += \    
	-DNO_AGPS
endif

#ifneq ($(BOARD_GPS_NEEDS_XTRA),)

LOCAL_CFLAGS += \
	-DNEEDS_INITIAL_XTRA
#endif

LOCAL_PRELINK_MODULE := false

LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw


LOCAL_SHARED_LIBRARIES := \
	liblog \
	libcutils

LOCAL_C_INCLUDES:= \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/gps_hal
	
LOCAL_MODULE :=  gps.marvell

LOCAL_MODULE_TAGS := optional

LOCAL_STATIC_LIBRARIES:=libsirfloader	

include $(BUILD_SHARED_LIBRARY)

include $(GPS_TOP)/sirf/Android.mk


endif


