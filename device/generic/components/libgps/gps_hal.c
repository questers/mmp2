/*
	initial version ,created by Toming,
	I think there a uart open/close mechanism should be existed for supporting GPS firmware upgrade, since
	it is a isolated firmware binary.
*/
#define  LOG_TAG  "gpshal"

#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <fcntl.h>
#include <termios.h>
#include <math.h>
#include <time.h>
#include <semaphore.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <utils/threads.h>
#include <cutils/properties.h>
#include <cutils/log.h>
#include <cutils/sockets.h>

#include "nmea.h"
#include "gps_hal.h"
#include "gps_door.h"

#include <sirfloader.h>



#ifndef GPS_FIRMWARE_PATH
#define GPS_FIRMWARE_PATH "/system/etc/firmware/sirf.bin"
#endif

#define GPS_ATTR_POWER  "/sys/class/gpio/gpio11/value"
#define GPS_ATTR_UPDATE "/sys/class/gpio/gpio138/value"
#define GPS_ATTR_RESET  "/sys/class/gpio/gpio135/value"
#define GPS_ATTR_ENABLE "/sys/class/gpio/gpio122/value"

#define GPS_PORT_RATE 9600

#define GPS_UPDATE_RATE 921600UL
//115200UL
/*921600 460800 307200 230400 115200 57600  38400 */

#define PROP_GPS_FIRMWARE "persist.sys.gpsfw"
#define PROP_GPS_RIRMWARE_CHECKER "ro.gps.fwchecker"
#define PROP_GPS_PORT "ro.gps.port"

#define MSG_BUF_SIZE	256

typedef struct gps_hal
{
	GpsCallbacks callbacks;
	
    nmea_object_t			nmea;
	
	//low level handle
	int fd;
	
    int latency;

	char buffer[MSG_BUF_SIZE];

	struct termios oldtio,tio;


	#define FLAG_THREAD_STARTED		0x01
	#define FLAG_DUMP_RAW_PACKET 	0x02
	#define FLAG_FIRMWARE_CHECKER	0x04
	#define FLAG_REPORT_SV		0x08
	unsigned long flags;

	int firmware_checker_counter;

	int resumeOpen;

	//gps callback thread critical section
	pthread_t thread_hal;
	pthread_mutex_t		mLock;
	pthread_cond_t mWaitWorkCV;
	pthread_cond_t mStopped;
	volatile int mActive;
	int 		mExitPending;
}GPS_HAL_T;

static GPS_HAL_T gps;





static int open_gps_port(GPS_HAL_T* hal,int baudrate)
{
	int BaudRate=B9600;
	char uart_dev_name[32];	
	char value[PROPERTY_VALUE_MAX];
	//get gps port
	property_get(PROP_GPS_PORT, value, "/dev/ttyS1");
	sprintf(uart_dev_name, "%s", value);
	hal->fd = open(uart_dev_name, O_RDWR | O_NOCTTY | O_NONBLOCK);

	if(hal->fd<0)
		return hal->fd;	

	// Get the current options for the port...
	tcgetattr(hal->fd, &hal->oldtio);
	
	bzero (&hal->tio, sizeof (struct termios));
	// Set the baud rates to 9600...
	switch(baudrate)
	{
		default:
		case 9600:BaudRate = B9600; break;
		case 38400:BaudRate = B38400; break;
		case 115200:BaudRate = B115200; break;
	}
	
	hal->tio.c_cflag = BaudRate | CS8 | CLOCAL | CREAD;//databit8 ,receive enable
	hal->tio.c_iflag = IGNPAR;//ignore parity,no flow control
	hal->tio.c_oflag = 0;
	hal->tio.c_lflag = ICANON;
	hal->tio.c_cc[VTIME] = 1;   /* return after 0.5s */
	hal->tio.c_cc[VMIN] = 0;	  /*no blocking */
	tcflush (hal->fd, TCIOFLUSH);
	tcsetattr (hal->fd, TCSANOW, &hal->tio);
	return 0;
}

static void close_gps_port(GPS_HAL_T* hal)
{
	if(hal->fd > 0)
	{
		tcflush (hal->fd, TCIOFLUSH);
		tcsetattr (hal->fd, TCSANOW, &hal->oldtio);
		close(hal->fd);	
		hal->fd = -1;
	}
}

static void reportStatus(GPS_HAL_T* hal,int status)
{
	gps_status_callback report = hal->callbacks.status_cb;
	if(report)
	{
		GpsStatus stat;
		stat.size = sizeof(GpsStatus);
		stat.status = status;
		report(&stat);
	}
	
}

static void nmea_data_process(GPS_HAL_T* hal,char* m,int l)
{
	nmea_process(hal->nmea,m,l);
}


int  gps_halthread_start(GPS_HAL_T* hal)
{
	pthread_mutex_lock(&hal->mLock);

	hal->mActive = 1;

	// signal thread to start
	pthread_cond_signal(&hal->mWaitWorkCV);
	pthread_cond_wait(&hal->mWaitWorkCV,&hal->mLock);
	pthread_mutex_unlock(&hal->mLock);

	return 0;

}
void gps_halthread_stop(GPS_HAL_T* hal)
{
	pthread_mutex_lock(&hal->mLock);

	if (hal->mActive) {
		hal->mActive = 0;		
		pthread_cond_wait(&hal->mStopped,&hal->mLock);
	}
	pthread_mutex_unlock(&hal->mLock);
	
}

void gps_halthread_exit(GPS_HAL_T* hal)
{
	pthread_mutex_lock(&hal->mLock);
	hal->mExitPending = 1;
	pthread_cond_signal(&hal->mWaitWorkCV);
	pthread_mutex_unlock(&hal->mLock);
	
}



static void gps_halthread_loop(void* data)
{
	GPS_HAL_T* hal=(GPS_HAL_T*)data;

	hal->mActive = 0;
	hal->fd = -1;
	while(!hal->mExitPending) 
	{
		if(!hal->mActive) 
		{
			pthread_mutex_lock(&hal->mLock);
			if (!hal->mActive && !hal->mExitPending) 
			{
				LOGD("GPSReadThread: loop stopping");	

				
				close_gps_port(hal);
				
				if(hal->flags&FLAG_REPORT_SV)
					reportStatus(hal,GPS_STATUS_ENGINE_OFF);
				pthread_cond_signal(&hal->mStopped);
				pthread_cond_wait(&hal->mWaitWorkCV,&hal->mLock);
				LOGD("GPSReadThread: loop starting");
				//put some init action here
				if(open_gps_port(hal,GPS_PORT_RATE))
				{
					LOGE("open gps port failed: %s\n", strerror(errno));
				}
				
				if(hal->flags&FLAG_REPORT_SV)
					reportStatus(hal,GPS_STATUS_ENGINE_ON);				
				pthread_cond_signal(&hal->mWaitWorkCV);
			}
			pthread_mutex_unlock(&hal->mLock);
		}
		else 
		{
			int num_of_bytes;
			num_of_bytes = read(hal->fd, hal->buffer, MSG_BUF_SIZE); 	 
			if(num_of_bytes > 0 )
			{
				hal->buffer[num_of_bytes] = '\0';
				if(hal->flags&FLAG_DUMP_RAW_PACKET)
				{
					LOGI("GPS:%s",hal->buffer);
				}
				if(hal->buffer[0] == '$')
				{
					if(hal->flags&FLAG_REPORT_SV)
						nmea_data_process(hal,hal->buffer, num_of_bytes);
					if(hal->flags&FLAG_FIRMWARE_CHECKER)
						hal->firmware_checker_counter++;						
				}
			}
			//release cpu some time
			usleep(hal->latency*1000);
		 }
	}

}


//this function should be called after gps door open
static void firmware_checker(int force_check)
{
    char value[PROPERTY_VALUE_MAX];
	int check=1;

	//check persist property 
    property_get(PROP_GPS_FIRMWARE, value, "");
	LOGD("getprop [%s]=[%s]",PROP_GPS_FIRMWARE,value);
	if(!strcmp(value,"loaded"))
	{
		check = 0;
	}
	if(force_check)
		check = 1;	


	if(check)
	{
		//send message to gps door		
		system("gpsloader fwck");  
	}

	
}



gps_hal_handle gps_hal_init(GpsCallbacks *callbacks)
{
	GPS_HAL_T * hal = &gps;

	gps_create_thread thread_create;

	memset(hal, 0, sizeof(GPS_HAL_T));
	hal->flags = FLAG_DUMP_RAW_PACKET;
	hal->latency = 5; /* 5ms to sleep between reading */

	//section protection
	pthread_mutex_init(&hal->mLock, NULL);
	pthread_cond_init(&hal->mStopped, NULL);	
	pthread_cond_init(&hal->mWaitWorkCV, NULL);	

	hal->callbacks = *callbacks; 	
	
	nmea_init(&hal->nmea,&hal->callbacks,hal);	

	//use callback thread creating approach
	thread_create = callbacks->create_thread_cb;
	if(thread_create)
	{
		hal->thread_hal = thread_create("gpsreader",gps_halthread_loop,hal);
	}

    char value[PROPERTY_VALUE_MAX];
	//check firmware checker property 
    property_get(PROP_GPS_RIRMWARE_CHECKER, value, "");
	if(!strcmp(value,"true"))
	{
		firmware_checker(0);
	}

	init_gps_door();
	
	return hal;

}

void gps_hal_deinit(gps_hal_handle handle)
{
	GPS_HAL_T * hal = (GPS_HAL_T *)handle;

	if(hal->thread_hal)
	{
		gps_halthread_exit(hal);
		
	}
	
	nmea_dispose(hal->nmea);

	pthread_cond_destroy(&hal->mStopped);
	pthread_cond_destroy(&hal->mWaitWorkCV);
	pthread_mutex_destroy(&hal->mLock);
	
}


static int _hal_start(GPS_HAL_T * hal,int reportSV)
{
	if(hal->flags&FLAG_THREAD_STARTED) 
	{
		LOGW("gps hal already started");
		return 0;
	}	

	//gps start sequence ,power on->enable->reset		
	gps_hal_cmd(GPS_HAL_CMD_SET_POWER,1);
	gps_hal_cmd(GPS_HAL_CMD_RESET,0/*unused param*/);
	gps_hal_cmd(GPS_HAL_CMD_ENABLE,1);	
	
	if(reportSV)
		hal->flags |= FLAG_REPORT_SV;

	/* start gps read thread */
	gps_halthread_start(hal);

	hal->flags |= FLAG_THREAD_STARTED;


	return 1;
	
}

void gps_hal_start(gps_hal_handle handle)
{
	GPS_HAL_T * hal = (GPS_HAL_T *)handle;

	//gps door comp
	if(!hal) hal=&gps;


	if(!_hal_start(hal,1))
	{
		//set pending flag only
		hal->flags |= FLAG_REPORT_SV;
	}
}

void gps_hal_stop(gps_hal_handle handle)
{
	GPS_HAL_T * hal = (GPS_HAL_T *)handle;
	
	//gps door comp
	if(!hal) hal=&gps;

	if(0==(hal->flags&FLAG_THREAD_STARTED))
	{
		LOGW("gps hal already stopped");
		return;
	}

	gps_halthread_stop(hal);

	//gps hardware stop sequence
	//power off ,disabled
	gps_hal_cmd(GPS_HAL_CMD_SET_POWER,0);	
	gps_hal_cmd(GPS_HAL_CMD_ENABLE,0);		

	hal->flags&=~FLAG_THREAD_STARTED;

}

static int write_attr_file(const char* path,int val)
{
	int fd,nwr;
	char value[20]; 			
	fd = open(path, O_RDWR);
	if(fd<0)
	{
		LOGE("failed to write attr file %s\n",path);
		return -1;
	}
	
	nwr = sprintf(value, "%d\n", val);
	write(fd, value, nwr);
	close(fd);		

	return 0;

}

int gps_hal_cmd(int cmd,int param)
{
	switch(cmd)
	{
		case GPS_HAL_CMD_DUMP_RAW:
			if(param)
			{
				gps.flags|=FLAG_DUMP_RAW_PACKET;
				LOGI("set gps dump on");
			}
			else
			{
				gps.flags&=~FLAG_DUMP_RAW_PACKET;
				LOGI("set gps dump off");				
			}
			break;
		case GPS_HAL_CMD_SET_POWER:
			{				
				LOGI("set gps power %s",(param>0)?"on":"off");				
				if(param>0)
				{
					write_attr_file(GPS_ATTR_ENABLE,0);
				}
				write_attr_file(GPS_ATTR_POWER,(param>0)?0:1/*negative valid*/);
				//wait 2s for stablize power 
				if(param)
				{
					//
					sleep(2);
				}
			}
			break;
		case GPS_HAL_CMD_UPG_ENABLE:
			{	
				LOGI("set gps update %s",(param>0)?"on":"off");				
				write_attr_file(GPS_ATTR_UPDATE,(param>0)?1:0);			
			}			
			break;
		case GPS_HAL_CMD_RESET:
			{				
				LOGI("reset gps hardware");
				write_attr_file(GPS_ATTR_RESET,0);			
				//wait chip to finish reset
				usleep(250*1000);
				write_attr_file(GPS_ATTR_RESET,1);			
			}			
			break;			
		case GPS_HAL_CMD_ENABLE:
			{		
				LOGI("set gps enable %s",(param>0)?"on":"off");				
				if(param>0)
				{
					write_attr_file(GPS_ATTR_ENABLE,0);
					write_attr_file(GPS_ATTR_ENABLE,1);					
				}
				else
				{
					write_attr_file(GPS_ATTR_ENABLE,1);
					write_attr_file(GPS_ATTR_ENABLE,0);					
				}
			}			
			break;	
		case GPS_HAL_CMD_DELAY:
			{	
				int delay = param;
				
				if((delay<500)&&(delay>0))
				{
					LOGD("set new latency to %dms",delay);
					gps.latency = delay;
				}
			}			
			break;			
		case GPS_HAL_CMD_UPDATE_PRE:
			{	
				LOGD("pre-update");				
				gps.resumeOpen = (gps.flags&FLAG_THREAD_STARTED)?1:0;				
				//firstly stop gps hal ,it also power off the gps hw
				gps_hal_stop(NULL);
				
				//put gps in internal booting mode
				gps_hal_cmd(GPS_HAL_CMD_UPG_ENABLE,1);
				gps_hal_cmd(GPS_HAL_CMD_SET_POWER,1);
				gps_hal_cmd(GPS_HAL_CMD_RESET,1);
				gps_hal_cmd(GPS_HAL_CMD_ENABLE,1);
			}			
			break;		
		case GPS_HAL_CMD_UPDATE_POST:
			{	
				LOGD("post update");	
				//put gps in external booting mode
				gps_hal_cmd(GPS_HAL_CMD_UPG_ENABLE,0);
				gps_hal_cmd(GPS_HAL_CMD_SET_POWER,0);
				//start gps hal service again
				if(gps.resumeOpen)
					_hal_start(&gps,0);
			}			
			break;		
		
		
		
	}
	return 0;
}


int gps_hal_update(void)
{
#define RETRY_TIMES 10
	int res=-1;
	int start_hal=0;
	int retry=0;
	char value[PROPERTY_VALUE_MAX];

	if(gps.flags&FLAG_THREAD_STARTED)
		start_hal = 1;

	LOGD("start to run gps firmware update");	
	
	//power on gps hw
	while(++retry<RETRY_TIMES){
		
		//set gps internal boot mode
		gps_hal_cmd(GPS_HAL_CMD_UPDATE_PRE,0);
	
		property_get(PROP_GPS_PORT, value, "/dev/ttyS1");
		res = sirf_loader(GPS_FIRMWARE_PATH,value,GPS_UPDATE_RATE);
#if 0
#if (0==GPS_UART_PORT)
	res = sirf_loader(GPS_FIRMWARE_PATH,"/dev/ttyS0",GPS_UPDATE_RATE);
#elif (1==GPS_UART_PORT)
	res = sirf_loader(GPS_FIRMWARE_PATH,"/dev/ttyS1",GPS_UPDATE_RATE);
#elif (2==GPS_UART_PORT)
	res = sirf_loader(GPS_FIRMWARE_PATH,"/dev/ttyS2",GPS_UPDATE_RATE);
#endif
#endif
		
		//set gps external boot mode
		gps_hal_cmd(GPS_HAL_CMD_UPDATE_POST,0);

		if(res)
		{
			LOGE("Sirf loader return error %d,retry[%d]",res,retry);
		}
		else
			break;
	}

	//start gps hal service again if necessary
	if(start_hal)
		_hal_start(&gps,0);

	return res;
	
}

int gps_hal_firmware_loaded(void)
{	
    char value[PROPERTY_VALUE_MAX];

	//check persist property 
    property_get(PROP_GPS_FIRMWARE, value, "");
	LOGD("getprop [%s]=[%s]",PROP_GPS_FIRMWARE,value);
	if(!strcmp(value,"loaded"))
	{
		return 1;
	}

	return 0;
}

void gps_hal_fwck(void)
{
	GPS_HAL_T* hal=&gps;
	int hal_started=0;
	int firmware_loaded=0;
	int counter=50;//5s is enough?
	//reset checker counter
	hal->firmware_checker_counter = 0;

	if(hal->flags&FLAG_THREAD_STARTED)
		hal_started = 1;
	//start gps hal,note this is not thread safe operation
	_hal_start(hal,0);

	//set firmware checker enable
	hal->flags |= FLAG_FIRMWARE_CHECKER;

	sched_yield();
	while(counter--)
	{
		if(hal->firmware_checker_counter)
		{
			LOGD("firmware already loaded");
			firmware_loaded = 1;
			break;
		}
		else
		{
			usleep(100000);
		}
	}

	//stop gps hal if necessary	
	hal->flags &= ~FLAG_FIRMWARE_CHECKER;
	if(!hal_started)
		gps_hal_stop(NULL);

	if(!firmware_loaded)
	{
		//try to load firmware?
		LOGD("gps firmware not loaded ,try to update");
		if(!gps_hal_update())
			firmware_loaded = 1;
	}

	//check the persist property of gps
	if(firmware_loaded)
	{
		char value[PROPERTY_VALUE_MAX];
		//check persist property 
		property_get(PROP_GPS_FIRMWARE, value, "");
		if(strcmp(value,"loaded"))
		{
			LOGD("set %s=loaded",PROP_GPS_FIRMWARE);
			property_set(PROP_GPS_FIRMWARE,"loaded");
		}
		
	}
	
}


int gps_hal_set_positionmode(gps_hal_handle handle,GpsPositionMode mode, int fix_frequency)
{
	GPS_HAL_T* hal = (GPS_HAL_T*)handle;
	if(!hal) return -1;
	return nmea_set_positionmode(hal->nmea,mode,fix_frequency);
}



