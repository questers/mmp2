#ifndef _GPS_HAL_H_
#define _GPS_HAL_H_


#include <hardware/gps.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void* gps_hal_handle;

gps_hal_handle gps_hal_init(GpsCallbacks *callbacks);
void gps_hal_deinit(gps_hal_handle handle);
void gps_hal_start(gps_hal_handle handle);
void gps_hal_stop(gps_hal_handle handle);



/*
 * Fine control for GPS
*/
int gps_hal_set_positionmode(gps_hal_handle handle,GpsPositionMode mode, int fix_frequency);








/*
  * internal use
*/
//firmware update ,this function would block until firmware update progress finished
int gps_hal_update(void);
//firmware checker,it will run firmware updater automatically if it is raw chip
int gps_hal_firmware_loaded(void);
void gps_hal_fwck(void);
	
#define GPS_HAL_CMD_DUMP_RAW 	1 /*0:dump off,1:on*/
#define GPS_HAL_CMD_SET_POWER 	2 /*0:off,1:on*/
#define GPS_HAL_CMD_UPG_ENABLE	3 /*0:disabled,1:enabled*/
#define GPS_HAL_CMD_RESET 		4 /*initial a reset,param:not used*/
#define GPS_HAL_CMD_ENABLE		5 /*0:disable,1:enable*/
#define GPS_HAL_CMD_DELAY		6 /*delay ms*/
#define GPS_HAL_CMD_UPDATE_PRE	7
#define GPS_HAL_CMD_UPDATE_POST	8
int gps_hal_cmd(int cmd,int param);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _GPS_HAL_H_ */
