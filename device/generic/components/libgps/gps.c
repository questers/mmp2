#include <stdlib.h>
#include <hardware/gps.h>
//#define LOG_NDEBUG 0
#define LOG_TAG "gpshal"
#include <utils/Log.h>
#include <gps_hal.h>

#define DBG_LEVEL_WARN 0x01
#define DBG_LEVEL_INFO 0x02
#define DBG_LEVEL_ERROR 0x04
#define DBG_LEVEL_DEBUG 0x08


#define WARN(...) \
	LOGI_IF(debug_level&DBG_LEVEL_WARN,__VA_ARGS__); 

#define INFO(...) \
	LOGI_IF(debug_level&DBG_LEVEL_INFO,__VA_ARGS__); 

#define ERROR(...) \
	LOGI_IF(debug_level&DBG_LEVEL_ERROR,__VA_ARGS__); 

#define DBG(...) \
	LOGI_IF(debug_level&DBG_LEVEL_DEBUG,__VA_ARGS__); 

#define FUNC_ENTER() \
	LOGI_IF(debug_level&DBG_LEVEL_DEBUG,"enter %s",__FUNCTION__)
	
#define FUNC_LEAVE() \
	LOGI_IF(debug_level&DBG_LEVEL_DEBUG,"leave %s",__FUNCTION__)

#define DEF_DBG_LEVEL (DBG_LEVEL_ERROR| \
						DBG_LEVEL_WARN)


enum {
  STATE_QUIT  = 0,
  STATE_INIT  = 1,
  STATE_START = 2,
  STATE_STOP  = 3
};



typedef struct {
    int                     init;
	gps_hal_handle			hal;
	GpsInterface 			gpsIntf;
	GpsXtraInterface		gpsXtraIntf;
	AGpsInterface			gpsAidIntf;
	GpsNiInterface			gpsNiIntf;
} GpsObject_t;



static unsigned long debug_level=DEF_DBG_LEVEL;


static GpsObject_t gpsObject;

static int  gps_init(GpsCallbacks* callbacks)
{
	GpsObject_t* s=&gpsObject;

	FUNC_ENTER();
	if (!s->init)
	{	
		s->init 	  = STATE_INIT;
		
		#ifdef NO_AGPS
		callbacks->set_capabilities_cb(0);
		#else
		callbacks->set_capabilities_cb(GPS_CAPABILITY_MSB|GPS_CAPABILITY_MSA);
		#endif
		s->hal = gps_hal_init(callbacks);
	
	}
	FUNC_LEAVE();

	return 0;

}

static int gps_start(void)
{
	GpsObject_t* s=&gpsObject;

    if (!s->init) {
        ERROR("%s: gps not init", __FUNCTION__);
        return -1;
    }

    s->init = STATE_START;

    gps_hal_start(s->hal);   
    
    return 0;
}

static int gps_stop(void)
{
	GpsObject_t* s=&gpsObject;

    if (!s->init) {
        ERROR("%s: called with uninitialized state !!", __FUNCTION__);
        return -1;
    }

    s->init = STATE_STOP;

    gps_hal_stop(s->hal);
    
    return 0;
}

static void gps_cleanup(void)
{
	GpsObject_t* s=&gpsObject;

	FUNC_ENTER();

	if (s->init)
	{		
		s->init = STATE_QUIT;		
		gps_hal_deinit(s->hal);
	}
    
	FUNC_LEAVE();
}

static int gps_inject_time(GpsUtcTime time, int64_t timeReference, int uncertainty)
{
    return 0;
}

/** Injects current location from another location provider
 *  (typically cell ID).
 *  latitude and longitude are measured in degrees
 *  expected accuracy is measured in meters
 */
static int gps_inject_location(double latitude, double longitude, float accuracy)
{
    return 0;
}

/**
 * Specifies that the next call to start will not use the
 * information defined in the flags. GPS_DELETE_ALL is passed for
 * a cold start.
 */
static void gps_delete_aiding_data(GpsAidingData flags)
{
}    

/**
 * min_interval represents the time between fixes in milliseconds.
 * preferred_accuracy represents the requested fix accuracy in meters.
 * preferred_time represents the requested time to first fix in milliseconds.
 */
static int gps_set_position_mode(GpsPositionMode mode, GpsPositionRecurrence recurrence,
            uint32_t min_interval, uint32_t preferred_accuracy, uint32_t preferred_time)
{
	GpsObject_t* s=&gpsObject;
	FUNC_ENTER();
	#if 0
    // only standalone supported for now.
    if (mode != GPS_POSITION_MODE_STANDALONE)
    {
        WARN("%s: only standalone supported for now !!", __FUNCTION__);
        return -1;
    }

    if (!s->init || fix_frequency < 0) 
    {
        WARN("%s: called with uninitialized state !!", __FUNCTION__);
        return -1;
    }
	#endif
	//gps_hal_set_positionmode(s->hal,mode,fix_frequency); 

	FUNC_LEAVE();
    return 0;
}

/** Get a pointer to extension information. Not used now */
static const void* gps_get_extension(const char* name)
{
	void* ext_intf=NULL;
	FUNC_ENTER();
	if(!strcmp(name,GPS_XTRA_INTERFACE))
	{
		WARN("trying to get gps extension %s interface,no support",name);
	}
	if(!strcmp(name,GPS_DEBUG_INTERFACE))
	{
		WARN("trying to get gps extension %s interface,no support",name);
	}
	if(!strcmp(name,AGPS_INTERFACE))
	{
		WARN("trying to get gps extension %s interface,no support",name);
	}
	if(!strcmp(name,AGPS_RIL_INTERFACE))
	{
		WARN("trying to get gps extension %s interface,no support",name);
	}
	if(!strcmp(name,GPS_NI_INTERFACE))
	{
		WARN("trying to get gps extension %s interface,no support",name);
	}
	
	FUNC_LEAVE();
    return ext_intf;
}



/* HAL Methods */
static const GpsInterface* __get_gps_interface(struct gps_device_t* dev)
{
	GpsInterface* gps=&gpsObject.gpsIntf;
	FUNC_ENTER();
    gps->size 		= sizeof(GpsInterface);
    gps->init 		= gps_init;
    gps->start		= gps_start;
    gps->stop 		= gps_stop;
    gps->cleanup 	= gps_cleanup;
    gps->inject_time = gps_inject_time; 
    gps->inject_location = gps_inject_location; 
    gps->delete_aiding_data = gps_delete_aiding_data; 
    gps->set_position_mode = gps_set_position_mode; 
    gps->get_extension = gps_get_extension;

	FUNC_LEAVE();
    return gps;
}

static int open_gps(const struct hw_module_t* module, char const* name,
        struct hw_device_t** device)
{
    struct gps_device_t *dev = malloc(sizeof(struct gps_device_t));
    memset(dev, 0, sizeof(*dev));

    dev->common.tag = HARDWARE_DEVICE_TAG;
    dev->common.version = 0;
    dev->common.module = (struct hw_module_t*)module;
    dev->get_gps_interface = __get_gps_interface;

    *device = (struct hw_device_t*)dev;
    return 0;
}

static struct hw_module_methods_t gps_module_methods = {
    .open = open_gps
};

const struct hw_module_t HAL_MODULE_INFO_SYM = {
    .tag = HARDWARE_MODULE_TAG,
    .version_major = 1,
    .version_minor = 0,
    .id = GPS_HARDWARE_MODULE_ID,
    .name = "GPS HAL Wrapper Module",
    .author = "GBT Team",
    .methods = &gps_module_methods,
};
