#ifndef _GPS_DOOR_H_
#define _GPS_DOOR_H_

#ifdef __cplusplus
extern "C" {
#endif

#define GPS_DOOR_NAME "GPSDOOR"
int init_gps_door(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
