#ifndef _CMD_DISP_H_
#define _CMD_DISP_H_
// these commands should contain a volume mount point after the colon
#define CMD_CONNECT          "connect"
#define CMD_DISCONNECT       "disconnect"
#define CMD_DUMP_NMEA        "dump"
#define CMD_FW_UPDATE		 "update"
#define CMD_DELAY		 	 "delay"
#define CMD_FIRMWARE_CHECKER "fwck"
#define CMD_UPDATE_PRE		 "updpre"
#define CMD_UPDATE_POST		 "updpost"

int process_gps_command(int socket);
#endif

