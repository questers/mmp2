#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>
#define  LOG_TAG  "gps_loader"

#include <cutils/log.h>
#include <cutils/sockets.h>
#include <sirfloader.h>
#include "gps_door.h"


static void usage(char *progname) {
    fprintf(stderr, "Usage: %s <cmd> [arg]\n", progname);
    fprintf(stderr, "\tcmd avaiable\n");
    fprintf(stderr, "\tconnect        attach gps hal to android \n");
    fprintf(stderr, "\tdisconnect     detach gps hal to android\n");
    fprintf(stderr, "\tdump <on|off>  dump nmea raw message to android log\n");
    fprintf(stderr, "\tupdate         run gps firmware update\n");	
    fprintf(stderr, "\tdelay <ms>     set gps data fetch delay\n");	
    fprintf(stderr, "\tlocalupdate [port] [baudrate]   update in local process,default [/dev/ttyS0,115200]\n");	
	
    exit(1);
}

static int run_update(const char* port,int baudrate)
{
	#define GPS_FIRMWARE_PATH "/system/etc/firmware/sirf.bin"
	#define DEF_UPDATE_BAUDRATE 115200UL
	int res;
	if(!baudrate)
		baudrate = DEF_UPDATE_BAUDRATE;
	
	fprintf(stderr,"run sirf loader in port[%s],baudrate[%d]\n",!port?"/dev/ttyS0":port,baudrate);
	res = sirf_loader(GPS_FIRMWARE_PATH,!port?"/dev/ttyS0":port,baudrate);
	if(res)
	{
		fprintf(stderr,"Sirf loader return error %d\n",res);
		return res;
	}

	return 0;
}
static int do_cmd(int sock, int argc, char **argv) 
{
    char final_cmd[255] = { '\0' };
    int i;

    for (i = 1; i < argc; i++) {
        char *cmp;

        if (!index(argv[i], ' '))
            asprintf(&cmp, "%s%s", argv[i], (i == (argc -1)) ? "" : " ");
        else
            asprintf(&cmp, "\"%s\"%s", argv[i], (i == (argc -1)) ? "" : " ");

        strcat(final_cmd, cmp);
        free(cmp);
    }

    if (write(sock, final_cmd, strlen(final_cmd) + 1) < 0) {
        perror("write");
        return errno;
    }

    close(sock);
	
	return 0;
}

static int send_sock_cmd(char* cmd)
{
	int sock;
	if ((sock = socket_local_client(GPS_DOOR_NAME,
									 ANDROID_SOCKET_NAMESPACE_ABSTRACT,
									 SOCK_STREAM)) < 0) {
		fprintf(stderr, "Error connecting (%s)\n", strerror(errno));
		return -1;		
	}
	if (write(sock, cmd, strlen(cmd) + 1) < 0) {
		fprintf(stderr, "Error write (%s)\n", strerror(errno));
		return -1;
	}
	
	close(sock);

	return 0;
}

int main(int argc, char **argv)
{	
    if (argc < 2)
        usage(argv[0]);
	
	//special case 
	if(!strcmp(argv[1],"localupdate"))
	{
		char* port=NULL;
		int ret=-1;
		int baudrate=0;	
		int retrytimes=0;
		//
		fprintf(stderr, "start to run gps firmware update\n");

		if(argc>=3)
			port = argv[2];
		if(argc>=4)
			baudrate = (int)strtoul(argv[3],NULL,0);

		while(++retrytimes<10)
		{
			//pre update
			send_sock_cmd("updpre");			
			usleep(1*1000*1000);
			
			ret = run_update(port,baudrate);
			
			send_sock_cmd("updpost");			
			usleep(1*1000*1000);
			
			if(!ret)
				break;
			else
			{				
				fprintf(stderr, "retry %d\n",retrytimes);
			}
		}
		
		fprintf(stderr, "update %s\n",(ret==0)?"ok":"failed");

		
	}
	else
	{
		int sock;
		if ((sock = socket_local_client(GPS_DOOR_NAME,
										 ANDROID_SOCKET_NAMESPACE_ABSTRACT,
										 SOCK_STREAM)) < 0) {
			fprintf(stderr, "Error connecting (%s)\n", strerror(errno));
			exit(4);		
		}
		exit(do_cmd(sock,argc,argv));		
	}
	
	return 0;
}

