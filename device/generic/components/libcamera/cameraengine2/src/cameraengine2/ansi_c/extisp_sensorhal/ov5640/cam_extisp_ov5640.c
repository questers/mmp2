/*******************************************************************************
//(C) Copyright [2010 - 2011] Marvell International Ltd.
//All Rights Reserved
*******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#include "cam_log.h"
#include "cam_utility.h"


#include "cam_extisp_sensorwrapper.h"
#include "cam_extisp_v4l2base.h"
#include "cam_extisp_ov5640.h"

#include "cutils/properties.h"

#define REG(a,b) {a, 0, 15, 0, b}

#define u8 unsigned char
#define ov5640_read(state, reg, val)  _get_sensor_reg(state->stV4L2.iSensorFD, reg, 0, 15, 0, val);
#define ov5640_write(state, reg, val) _set_sensor_reg(state->stV4L2.iSensorFD, reg, 0, 15, 0, val);

#define OV5640_AF_MAX_INTERVAL 1000000
static int FocusStartTime = 0;


/* Sensor Capability Table */

// NOTE: if you want to enable static resolution table to bypass sensor dynamically detect to save camera-off to viwerfinder-on latency, 
//       you can fill the following four tables according to your sensor's capability. And open macro 
//       BUILD_OPTION_STARTEGY_DISABLE_DYNAMIC_SENSOR_DETECT in cam_extisp_buildopt.h

// OV5640 supported video/preview resolution
CAM_Size _OV5640VideoResTable[CAM_MAX_SUPPORT_IMAGE_SIZE_CNT] =
{
#if 0
	{176,  144},	// QCIF
	{320,  240},	// QVGA
	{352,  288},	// CIF
	{640,  480},	// VGA
	{720,  480},
//	{720,  576},	// D1
#endif
	{0,0}
};

// OV5640 supported still capture resolution 
CAM_Size _OV5640StillResTable[CAM_MAX_SUPPORT_IMAGE_SIZE_CNT] = 
{
#if 0
	{2592, 1944},	// 5M
#endif
	{0,0}
};

// OV5640 supported video/preview capture image format
CAM_ImageFormat _OV5640VideoFormatTable[CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT] = 
{
	CAM_IMGFMT_YCC420P,
};

// OV5640 supported still image format
CAM_ImageFormat _OV5640StillFormatTable[CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT] =
{
	CAM_IMGFMT_JPEG,
};

// OV5640 supported sensor rotation/flip/mirror type
CAM_FlipRotate _OV5640RotationTable[] = 
{
	CAM_FLIPROTATE_NORMAL,
//	CAM_FLIPROTATE_HFLIP,
//	CAM_FLIPROTATE_VFLIP,
//	CAM_FLIPROTATE_180,
};

// shot mode
CAM_ShotMode _OV5640ShotModeTable[] = 
{
	CAM_SHOTMODE_AUTO,
	//CAM_SHOTMODE_MANUAL,
};

// exposure metering mode
_CAM_RegEntry _OV5640ExpMeter_Mean[] = 
{
	{ OV5640_REG_EM_WIN0WEI, 0x01},
	{ OV5640_REG_EM_WIN1WEI, 0x10}, 
	{ OV5640_REG_EM_WIN2WEI, 0x01}, 
	{ OV5640_REG_EM_WIN3WEI, 0x10}, 
	{ OV5640_REG_EM_WIN10WEI,0x01},
	{ OV5640_REG_EM_WIN11WEI,0x10},
	{ OV5640_REG_EM_WIN12WEI,0x01},
	{ OV5640_REG_EM_WIN13WEI,0x10},
	{ OV5640_REG_EM_WIN20WEI,0x01},
	{ OV5640_REG_EM_WIN21WEI,0x10},
	{ OV5640_REG_EM_WIN22WEI,0x01},
	{ OV5640_REG_EM_WIN23WEI,0x10},
	{ OV5640_REG_EM_WIN30WEI,0x01},
	{ OV5640_REG_EM_WIN31WEI,0x10},
	{ OV5640_REG_EM_WIN32WEI,0x01},
	{ OV5640_REG_EM_WIN33WEI,0x10},

};

_CAM_RegEntry _OV5640ExpMeter_Spot[] = 
{
	{ OV5640_REG_EM_WIN0WEI, 0x00},
	{ OV5640_REG_EM_WIN1WEI, 0x00}, 
	{ OV5640_REG_EM_WIN2WEI, 0x00}, 
	{ OV5640_REG_EM_WIN3WEI, 0x00}, 
	{ OV5640_REG_EM_WIN10WEI,0x00},
	{ OV5640_REG_EM_WIN11WEI,0x10},
	{ OV5640_REG_EM_WIN12WEI,0x01},
	{ OV5640_REG_EM_WIN13WEI,0x00},
	{ OV5640_REG_EM_WIN20WEI,0x00},
	{ OV5640_REG_EM_WIN21WEI,0x10},
	{ OV5640_REG_EM_WIN22WEI,0x01},
	{ OV5640_REG_EM_WIN23WEI,0x00},
	{ OV5640_REG_EM_WIN30WEI,0x00},
	{ OV5640_REG_EM_WIN31WEI,0x00},
	{ OV5640_REG_EM_WIN32WEI,0x00},
	{ OV5640_REG_EM_WIN33WEI,0x00},

};

_CAM_RegEntry _OV5640ExpMeter_CW[] = 
{
	{ OV5640_REG_EM_WIN0WEI, 0x02},
	{ OV5640_REG_EM_WIN1WEI, 0x60}, 
	{ OV5640_REG_EM_WIN2WEI, 0x06}, 
	{ OV5640_REG_EM_WIN3WEI, 0x20}, 
	{ OV5640_REG_EM_WIN10WEI,0x06},
	{ OV5640_REG_EM_WIN11WEI,0xe0},
	{ OV5640_REG_EM_WIN12WEI,0x0e},
	{ OV5640_REG_EM_WIN13WEI,0x60},
	{ OV5640_REG_EM_WIN20WEI,0x0a},
	{ OV5640_REG_EM_WIN21WEI,0xe0},
	{ OV5640_REG_EM_WIN22WEI,0x0e},
	{ OV5640_REG_EM_WIN23WEI,0xa0},
	{ OV5640_REG_EM_WIN30WEI,0x06},
	{ OV5640_REG_EM_WIN31WEI,0xa0},
	{ OV5640_REG_EM_WIN32WEI,0x0a},
	{ OV5640_REG_EM_WIN33WEI,0x60},

};

_CAM_ParameterEntry _OV5640ExpMeterMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_EXPOSUREMETERMODE_MEAN, _OV5640ExpMeter_Mean),
	PARAM_ENTRY_USE_REGTABLE(CAM_EXPOSUREMETERMODE_SPOT, _OV5640ExpMeter_Spot),
	PARAM_ENTRY_USE_REGTABLE(CAM_EXPOSUREMETERMODE_CENTRALWEIGHTED, _OV5640ExpMeter_CW),
};

// ISO setting
_CAM_RegEntry _OV5640IsoMode_Auto[] = 
{
	{OV5640_REG_SET_ISOLOW, 0xfc},
};

_CAM_RegEntry _OV5640IsoMode_100[] =
{
	{OV5640_REG_SET_ISOLOW, 0x6c},
};

_CAM_RegEntry _OV5640IsoMode_200[] = 
{
	{OV5640_REG_SET_ISOHIGH, 0x00},
	{OV5640_REG_SET_ISOLOW, 0x8c},
};

_CAM_RegEntry _OV5640IsoMode_400[] = 
{
	{OV5640_REG_SET_ISOHIGH, 0x00},
	{OV5640_REG_SET_ISOLOW, 0xcc},
};

_CAM_RegEntry _OV5640IsoMode_800[] =
{
	{OV5640_REG_SET_ISOHIGH, 0x00},
	{OV5640_REG_SET_ISOLOW, 0xfc},
};

_CAM_RegEntry _OV5640IsoMode_1600[] =
{
	{OV5640_REG_SET_ISOHIGH, 0x01},
	{OV5640_REG_SET_ISOLOW, 0xfc},
};

_CAM_ParameterEntry _OV5640IsoMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_ISO_AUTO, _OV5640IsoMode_Auto),
	PARAM_ENTRY_USE_REGTABLE(CAM_ISO_100, _OV5640IsoMode_100),
	PARAM_ENTRY_USE_REGTABLE(CAM_ISO_200, _OV5640IsoMode_200),
	PARAM_ENTRY_USE_REGTABLE(CAM_ISO_400, _OV5640IsoMode_400),
	PARAM_ENTRY_USE_REGTABLE(CAM_ISO_800, _OV5640IsoMode_800),
	PARAM_ENTRY_USE_REGTABLE(CAM_ISO_1600,_OV5640IsoMode_1600),
};

// band filter setting
_CAM_RegEntry _OV5640BdFltMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _OV5640BdFltMode_Off[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _OV5640BdFltMode_50Hz[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _OV5640BdFltMode_60Hz[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _OV5640BdFltMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_BANDFILTER_AUTO, _OV5640BdFltMode_Auto),
	PARAM_ENTRY_USE_REGTABLE(CAM_BANDFILTER_OFF,  _OV5640BdFltMode_Off),
	PARAM_ENTRY_USE_REGTABLE(CAM_BANDFILTER_50HZ, _OV5640BdFltMode_50Hz),
	PARAM_ENTRY_USE_REGTABLE(CAM_BANDFILTER_60HZ, _OV5640BdFltMode_60Hz),
};

// Flash Mode
_CAM_RegEntry _OV5640FlashMode_Off[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _OV5640FlashMode_On[] =
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _OV5640FlashMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _OV5640FlashMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_FLASH_OFF,	_OV5640FlashMode_Off),
};

// white balance mode
_CAM_RegEntry _OV5640WBMode_Auto[] = 
{
	{OV5640_REG_WB_CTL,0x00},
};

_CAM_RegEntry _OV5640WBMode_Incandescent[] = 
{
	{OV5640_REG_WB_CTL,0x01},
	{OV5640_REG_WB_RHIGH,0x04},
	{OV5640_REG_WB_RLOW,0x88},
	{OV5640_REG_WB_GHIGH,0x04},
	{OV5640_REG_WB_GLOW,0x00},
	{OV5640_REG_WB_BHIGH,0x08},
	{OV5640_REG_WB_BLOW,0xb6},
};

_CAM_RegEntry _OV5640WBMode_CoolFluorescent[] = 
{
	{OV5640_REG_WB_CTL,0x01},
	{OV5640_REG_WB_RHIGH,0x06},
	{OV5640_REG_WB_RLOW,0x2a},
	{OV5640_REG_WB_GHIGH,0x04},
	{OV5640_REG_WB_GLOW,0x00},
	{OV5640_REG_WB_BHIGH,0x07},
	{OV5640_REG_WB_BLOW,0x24},
};

_CAM_RegEntry _OV5640WBMode_Daylight[] = 
{
	{OV5640_REG_WB_CTL,0x01},
	{OV5640_REG_WB_RHIGH,0x07},
	{OV5640_REG_WB_RLOW,0x02},
	{OV5640_REG_WB_GHIGH,0x04},
	{OV5640_REG_WB_GLOW,0x00},
	{OV5640_REG_WB_BHIGH,0x05},
	{OV5640_REG_WB_BLOW,0x15},
};

_CAM_RegEntry _OV5640WBMode_Cloudy[] = 
{
	{OV5640_REG_WB_CTL,0x01},
	{OV5640_REG_WB_RHIGH,0x07},
	{OV5640_REG_WB_RLOW,0x88},
	{OV5640_REG_WB_GHIGH,0x04},
	{OV5640_REG_WB_GLOW,0x00},
	{OV5640_REG_WB_BHIGH,0x05},
	{OV5640_REG_WB_BLOW,0x00},
};

_CAM_ParameterEntry _OV5640WBMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_AUTO, _OV5640WBMode_Auto),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_INCANDESCENT, _OV5640WBMode_Incandescent),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_COOLWHITE_FLUORESCENT, _OV5640WBMode_CoolFluorescent),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_DAYLIGHT,	_OV5640WBMode_Daylight),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_CLOUDY, _OV5640WBMode_Cloudy),
};


// focus mode
_CAM_RegEntry _OV5640FocusMode_Infinity[] =
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _OV5640FocusMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_FOCUS_INFINITY,  _OV5640FocusMode_Infinity),
};

// color effect mode
_CAM_RegEntry _OV5640ColorEffectMode_Off[] = 
{
	REG(0x3212, 0x03),
	REG(0x5580, 0x06),
	REG(0x5583, 0x40),
	REG(0x5584, 0x10),
	REG(0x5003, 0x08),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};

_CAM_RegEntry _OV5640ColorEffectMode_Sepia[] = 
{
	REG(0x3212, 0x03),
	REG(0x5580, 0x1e),
	REG(0x5583, 0x40),
	REG(0x5584, 0xa0),
	REG(0x5003, 0x08),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};

_CAM_RegEntry _OV5640ColorEffectMode_Grayscale[] = 
{
	REG(0x3212, 0x03),
	REG(0x5580, 0x1e),
	REG(0x5583, 0x80),
	REG(0x5584, 0x80),
	REG(0x5003, 0x08),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};


_CAM_RegEntry _OV5640ColorEffectMode_Negative[] =
{
	REG(0x3212, 0x03),
	REG(0x5580, 0x40),
	REG(0x5003, 0x08),
	REG(0x5583, 0x40),
	REG(0x5584, 0x10),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};

_CAM_RegEntry _OV5640ColorEffectMode_Solarize[] =
{
	REG(0x3212, 0x03),
	REG(0x5580, 0x06),
	REG(0x5583, 0x40),
	REG(0x5584, 0x10),
	REG(0x5003, 0x09),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};

_CAM_ParameterEntry _OV5640ColorEffectMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_OFF,   _OV5640ColorEffectMode_Off),
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_SEPIA, _OV5640ColorEffectMode_Sepia),
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_GRAYSCALE, _OV5640ColorEffectMode_Grayscale),
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_NEGATIVE, _OV5640ColorEffectMode_Negative),
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_SOLARIZE, _OV5640ColorEffectMode_Solarize),
};

// JPEG quality factor table
_CAM_RegEntry _OV5640JpegQuality_Low[] =
{
	{OV5640_REG_JPEG_QSCTL, 0x03},
};

_CAM_RegEntry _OV5640JpegQuality_Medium[] = 
{
	{OV5640_REG_JPEG_QSCTL, 0x04},
};

_CAM_RegEntry _OV5640JpegQuality_High[] = 
{
	{OV5640_REG_JPEG_QSCTL, 0x08},
};

_CAM_ParameterEntry _OV5640JpegQuality[] = 
{
	PARAM_ENTRY_USE_REGTABLE(0, _OV5640JpegQuality_Low),
	PARAM_ENTRY_USE_REGTABLE(1, _OV5640JpegQuality_Medium),
	PARAM_ENTRY_USE_REGTABLE(2, _OV5640JpegQuality_High),
};

_CAM_RegEntry _OV5640Brightness_P4[] = 
{
	REG(0x3212, 0x03),
	REG(0x5587, 0x40),
	REG(0x5588, 0x01),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};


_CAM_RegEntry _OV5640Brightness_P3[] = 
{
	REG(0x3212, 0x03),
	REG(0x5587, 0x30),
	REG(0x5588, 0x01),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};


_CAM_RegEntry _OV5640Brightness_P2[] = 
{
	REG(0x3212, 0x03),
	REG(0x5587, 0x20),
	REG(0x5588, 0x01),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};


_CAM_RegEntry _OV5640Brightness_P1[] = 
{
	REG(0x3212, 0x03),
	REG(0x5587, 0x10),
	REG(0x5588, 0x01),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};


_CAM_RegEntry _OV5640Brightness_00[] = 
{
	REG(0x3212, 0x03),
	REG(0x5587, 0x00),
	REG(0x5588, 0x01),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};


_CAM_RegEntry _OV5640Brightness_N1[] = 
{
	REG(0x3212, 0x03),
	REG(0x5587, 0x10),
	REG(0x5588, 0x09),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};

_CAM_RegEntry _OV5640Brightness_N2[] = 
{
	REG(0x3212, 0x03),
	REG(0x5587, 0x20),
	REG(0x5588, 0x09),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};

_CAM_RegEntry _OV5640Brightness_N3[] = 
{
	REG(0x3212, 0x03),
	REG(0x5587, 0x30),
	REG(0x5588, 0x09),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};

_CAM_RegEntry _OV5640Brightness_N4[] = 
{
	REG(0x3212, 0x03),
	REG(0x5587, 0x40),
	REG(0x5588, 0x09),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};


_CAM_ParameterEntry _OV5640Brightness[] = 
{
	PARAM_ENTRY_USE_REGTABLE( 4, _OV5640Brightness_P4),
	PARAM_ENTRY_USE_REGTABLE( 3, _OV5640Brightness_P3),
	PARAM_ENTRY_USE_REGTABLE( 2, _OV5640Brightness_P2),
	PARAM_ENTRY_USE_REGTABLE( 1, _OV5640Brightness_P1),
	PARAM_ENTRY_USE_REGTABLE( 0, _OV5640Brightness_00),
	PARAM_ENTRY_USE_REGTABLE(-1, _OV5640Brightness_N1),
	PARAM_ENTRY_USE_REGTABLE(-2, _OV5640Brightness_N2),
	PARAM_ENTRY_USE_REGTABLE(-3, _OV5640Brightness_N3),
	PARAM_ENTRY_USE_REGTABLE(-4, _OV5640Brightness_N4),
};

_CAM_ParameterEntry _OV5640FlashBrightness[] = 
{
	PARAM_ENTRY_USE_VALUE( 2, 80),
	PARAM_ENTRY_USE_VALUE( 1, 120),
	PARAM_ENTRY_USE_VALUE( 0, 160),
	PARAM_ENTRY_USE_VALUE(-1, 200),
	PARAM_ENTRY_USE_VALUE(-2, 240),
};

_CAM_ParameterEntry _OV5640TorchBrightness[] = 
{
	PARAM_ENTRY_USE_VALUE( 2, 150),
	PARAM_ENTRY_USE_VALUE( 1, 175),
	PARAM_ENTRY_USE_VALUE( 0, 200),
	PARAM_ENTRY_USE_VALUE(-1, 220),
	PARAM_ENTRY_USE_VALUE(-2, 240),
};


_CAM_RegEntry _OV5640Contrast_P3[] = 
{
	REG(0x3212, 0x03),
	REG(0x5586, 0x2c),
	REG(0x5585, 0x1c),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};

_CAM_RegEntry _OV5640Contrast_P2[] = 
{
	REG(0x3212, 0x03),
	REG(0x5586, 0x28),
	REG(0x5585, 0x18),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};

_CAM_RegEntry _OV5640Contrast_P1[] = 
{
	REG(0x3212, 0x03),
	REG(0x5586, 0x24),
	REG(0x5585, 0x10),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};

_CAM_RegEntry _OV5640Contrast_00[] = 
{
	REG(0x3212, 0x03),
	REG(0x5586, 0x20),
	REG(0x5585, 0x00),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};


_CAM_RegEntry _OV5640Contrast_N1[] = 
{
	REG(0x3212, 0x03),
	REG(0x5586, 0x1c),
	REG(0x5585, 0x1c),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};

_CAM_RegEntry _OV5640Contrast_N2[] = 
{
	REG(0x3212, 0x03),
	REG(0x5586, 0x18),
	REG(0x5585, 0x18),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};

_CAM_RegEntry _OV5640Contrast_N3[] = 
{
	REG(0x3212, 0x03),
	REG(0x5586, 0x14),
	REG(0x5585, 0x14),
	REG(0x3212, 0x13),
	REG(0x3212, 0xa3),
};


_CAM_ParameterEntry _OV5640Contrast[] = 
{
	PARAM_ENTRY_USE_REGTABLE( 3, _OV5640Contrast_P3),
	PARAM_ENTRY_USE_REGTABLE( 2, _OV5640Contrast_P2),
	PARAM_ENTRY_USE_REGTABLE( 1, _OV5640Contrast_P1),
	PARAM_ENTRY_USE_REGTABLE( 0, _OV5640Contrast_00),
	PARAM_ENTRY_USE_REGTABLE(-1, _OV5640Contrast_N1),
	PARAM_ENTRY_USE_REGTABLE(-2, _OV5640Contrast_N2),
	PARAM_ENTRY_USE_REGTABLE(-3, _OV5640Contrast_N3),
};

_CAM_RegEntry _OV5640EvComp_P3[] = 
{
	REG(0x3a0f, 0x60),
	REG(0x3a10, 0x58),
	REG(0x3a11, 0xa0),
	REG(0x3a1b, 0x60),
	REG(0x3a1e, 0x58),
	REG(0x3a1f, 0x20),
};


_CAM_RegEntry _OV5640EvComp_P2[] = 
{
	REG(0x3a0f, 0x50),
	REG(0x3a10, 0x48),
	REG(0x3a11, 0x90),
	REG(0x3a1b, 0x50),
	REG(0x3a1e, 0x48),
	REG(0x3a1f, 0x20),
};


_CAM_RegEntry _OV5640EvComp_P1[] = 
{
	REG(0x3a0f, 0x40),
	REG(0x3a10, 0x38),
	REG(0x3a11, 0x71),
	REG(0x3a1b, 0x40),
	REG(0x3a1e, 0x38),
	REG(0x3a1f, 0x10),
};


_CAM_RegEntry _OV5640EvComp_00[] = 
{
	REG(0x3a0f, 0x38),
	REG(0x3a10, 0x30),
	REG(0x3a11, 0x61),
	REG(0x3a1b, 0x38),
	REG(0x3a1e, 0x30),
	REG(0x3a1f, 0x10),
};


_CAM_RegEntry _OV5640EvComp_N1[] = 
{
	REG(0x3a0f, 0x30),
	REG(0x3a10, 0x28),
	REG(0x3a11, 0x61),
	REG(0x3a1b, 0x30),
	REG(0x3a1e, 0x28),
	REG(0x3a1f, 0x10),
};


_CAM_RegEntry _OV5640EvComp_N2[] = 
{
	REG(0x3a0f, 0x20),
	REG(0x3a10, 0x18),
	REG(0x3a11, 0x41),
	REG(0x3a1b, 0x20),
	REG(0x3a1e, 0x18),
	REG(0x3a1f, 0x10),
};


_CAM_RegEntry _OV5640EvComp_N3[] = 
{
	REG(0x3a0f, 0x10),
	REG(0x3a10, 0x08),
	REG(0x3a1b, 0x10),
	REG(0x3a1e, 0x08),
	REG(0x3a11, 0x20),
	REG(0x3a1f, 0x10),
};


_CAM_ParameterEntry _OV5640EvComp[] = 
{
	PARAM_ENTRY_USE_REGTABLE( 3, _OV5640EvComp_P3),
	PARAM_ENTRY_USE_REGTABLE( 2, _OV5640EvComp_P2),
	PARAM_ENTRY_USE_REGTABLE( 1, _OV5640EvComp_P1),
	PARAM_ENTRY_USE_REGTABLE( 0, _OV5640EvComp_00),
	PARAM_ENTRY_USE_REGTABLE(-1, _OV5640EvComp_N1),
	PARAM_ENTRY_USE_REGTABLE(-2, _OV5640EvComp_N2),
	PARAM_ENTRY_USE_REGTABLE(-3, _OV5640EvComp_N3),
};


static CAM_Error _OV5640SaveAeAwb(const _CAM_SmartSensorConfig*, void*);
static CAM_Error _OV5640RestoreAeAwb(const _CAM_SmartSensorConfig*, void*);
static CAM_Error _OV5640StartFlash(void*);
static CAM_Error _OV5640StopFlash(void*);
static CAM_Error _OV5640FillFrameShotInfo( OV5640State*, CAM_ImageBuffer* );
static CAM_Error _OV5640ApplyShotParam(void* pUserData);

// shot mode capability
static void _OV5640AutoModeCap( CAM_ShotModeCapability* );
// static void _OV5640ManualModeCap( CAM_ShotModeCapability*);
// static void _OV5640NightModeCap( CAM_ShotModeCapability*);
/* shot mode cap function table */
OV5640_ShotModeCap _OV5640ShotModeCap[CAM_SHOTMODE_NUM] = { _OV5640AutoModeCap, // CAM_SHOTMODE_AUTO = 0,
                                                           NULL              ,  // CAM_SHOTMODE_MANUAL,
                                                           NULL              ,  // CAM_SHOTMODE_PORTRAIT,
                                                           NULL              ,  // CAM_SHOTMODE_LANDSCAPE,
                                                           NULL              ,  // CAM_SHOTMODE_NIGHTPORTRAIT,
                                                           NULL              ,  // CAM_SHOTMODE_NIGHTSCENE,
                                                           NULL              ,  // CAM_SHOTMODE_CHILD,
                                                           NULL              ,  // CAM_SHOTMODE_INDOOR,
                                                           NULL              ,  // CAM_SHOTMODE_PLANTS,
                                                           NULL              ,  // CAM_SHOTMODE_SNOW,
                                                           NULL              ,  // CAM_SHOTMODE_BEACH,
                                                           NULL              ,  // CAM_SHOTMODE_FIREWORKS,
                                                           NULL              ,  // CAM_SHOTMODE_SUBMARINE, 
                                                          };  


/* OV5640 behavior interface */

extern _SmartSensorFunc func_ov5640; 
CAM_Error OV5640_SelfDetect( _SmartSensorAttri *pSensorInfo )
{
	CAM_Error error = CAM_ERROR_NONE;

	// NOTE:  If you open macro BUILD_OPTION_STARTEGY_DISABLE_DYNAMIC_SENSOR_DETECT in cam_extisp_buildopt.h 
	//        to bypass sensor dynamically detect to save camera-off to viwerfinder-on latency, you should initilize
	//        _OV5640VideoResTable/_OV5640StillResTable/_OV5640VideoFormatTable/_OV5640StillFormatTable manually.

#if !defined( BUILD_OPTION_STARTEGY_DISABLE_DYNAMIC_SENSOR_DETECT )
	error = V4L2_SelfDetect( pSensorInfo, "ov5640", &func_ov5640,
	                         _OV5640VideoResTable, _OV5640StillResTable,
	                         _OV5640VideoFormatTable, _OV5640StillFormatTable );
#else
	{
		_V4L2SensorEntry *pSensorEntry = (_V4L2SensorEntry*)( pSensorInfo->cReserved );
		strcpy( pSensorInfo->sSensorName, "ov5640" );
		pSensorInfo->pFunc = &func_ov5640;

		// FIXME: the following is just an example in Marvell platform, you should change it according to your driver implementation
		strcpy( pSensorEntry->sDeviceName, "/dev/video0" );
		pSensorEntry->iV4L2SensorID = 1;
	}
#endif

	return error;
} 

CAM_Error OV5640_GetCapability( _CAM_SmartSensorCapability *pCapability )
{
	CAM_Int32s i = 0;


	// preview/video supporting 
	// format
	pCapability->iSupportedVideoFormatCnt = 0;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT; i++ ) 
	{
		if ( _OV5640VideoFormatTable[i] == 0 )
		{
			break;
		}
		pCapability->eSupportedVideoFormat[pCapability->iSupportedVideoFormatCnt] = _OV5640VideoFormatTable[i];
		pCapability->iSupportedVideoFormatCnt++;
	}

	pCapability->bArbitraryVideoSize    = CAM_FALSE;
	pCapability->iSupportedVideoSizeCnt = 0;
	pCapability->stMinVideoSize.iWidth   = _OV5640VideoResTable[0].iWidth;
	pCapability->stMinVideoSize.iHeight  = _OV5640VideoResTable[0].iHeight;
	pCapability->stMaxVideoSize.iWidth   = _OV5640VideoResTable[0].iWidth;
	pCapability->stMaxVideoSize.iHeight  = _OV5640VideoResTable[0].iHeight;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_SIZE_CNT; i++ )
	{
		if ( _OV5640VideoResTable[i].iWidth == 0 || _OV5640VideoResTable[i].iHeight == 0)
		{
			break;
		}
		pCapability->stSupportedVideoSize[pCapability->iSupportedVideoSizeCnt] = _OV5640VideoResTable[i];
		pCapability->iSupportedVideoSizeCnt++;

		if ( ( pCapability->stMinVideoSize.iWidth > _OV5640VideoResTable[i].iWidth ) || 
		     ( ( pCapability->stMinVideoSize.iWidth == _OV5640VideoResTable[i].iWidth ) && ( pCapability->stMinVideoSize.iHeight > _OV5640VideoResTable[i].iHeight ) ) ) 
		{
			pCapability->stMinVideoSize.iWidth = _OV5640VideoResTable[i].iWidth;
			pCapability->stMinVideoSize.iHeight = _OV5640VideoResTable[i].iHeight;
		}
		if ( ( pCapability->stMaxVideoSize.iWidth < _OV5640VideoResTable[i].iWidth) ||
		     ( ( pCapability->stMaxVideoSize.iWidth == _OV5640VideoResTable[i].iWidth ) && ( pCapability->stMaxVideoSize.iHeight < _OV5640VideoResTable[i].iHeight ) ) )
		{
			pCapability->stMaxVideoSize.iWidth = _OV5640VideoResTable[i].iWidth;
			pCapability->stMaxVideoSize.iHeight = _OV5640VideoResTable[i].iHeight;
		}
	}

	// still capture supporting
	// format
	pCapability->iSupportedStillFormatCnt = 0;
	pCapability->stSupportedJPEGParams.bSupportJpeg = CAM_FALSE;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT; i++ ) 
	{
		if ( _OV5640StillFormatTable[i] == CAM_IMGFMT_JPEG )
		{
			// JPEG encoder functionalities
			pCapability->stSupportedJPEGParams.bSupportJpeg = CAM_TRUE;
			pCapability->stSupportedJPEGParams.bSupportExif = CAM_FALSE;
			pCapability->stSupportedJPEGParams.bSupportThumb = CAM_FALSE;
			pCapability->stSupportedJPEGParams.iMinQualityFactor = 80;
			pCapability->stSupportedJPEGParams.iMaxQualityFactor = 80;
		}
		if ( _OV5640StillFormatTable[i] == 0 )
		{
			break;
		}
		pCapability->eSupportedStillFormat[pCapability->iSupportedStillFormatCnt] = _OV5640StillFormatTable[i];
		pCapability->iSupportedStillFormatCnt++;
	}
	// resolution
	pCapability->bArbitraryStillSize = CAM_FALSE;
	pCapability->iSupportedStillSizeCnt = 0;
	pCapability->stMinStillSize.iWidth  = _OV5640StillResTable[0].iWidth;
	pCapability->stMinStillSize.iHeight = _OV5640StillResTable[0].iHeight;
	pCapability->stMaxStillSize.iWidth  = _OV5640StillResTable[0].iWidth;
	pCapability->stMaxStillSize.iHeight = _OV5640StillResTable[0].iHeight;
	for (i = 0; i < CAM_MAX_SUPPORT_IMAGE_SIZE_CNT; i++)
	{
		if ( _OV5640StillResTable[i].iWidth == 0 || _OV5640StillResTable[i] .iHeight == 0 )
		{
			break;
		}

		pCapability->stSupportedStillSize[pCapability->iSupportedStillSizeCnt] = _OV5640StillResTable[i];
		pCapability->iSupportedStillSizeCnt++;

		if ( ( pCapability->stMinStillSize.iWidth > _OV5640StillResTable[i].iWidth ) ||
		     ( ( pCapability->stMinStillSize.iWidth == _OV5640StillResTable[i].iWidth ) && ( pCapability->stMinStillSize.iHeight > _OV5640StillResTable[i].iHeight ) ) )
		{
			pCapability->stMinStillSize.iWidth  = _OV5640StillResTable[i].iWidth;
			pCapability->stMinStillSize.iHeight = _OV5640StillResTable[i].iHeight;
		}
		if ( ( pCapability->stMaxStillSize.iWidth < _OV5640StillResTable[i].iWidth ) || 
		     ( ( pCapability->stMaxStillSize.iWidth == _OV5640StillResTable[i].iWidth ) && ( pCapability->stMaxStillSize.iHeight < _OV5640StillResTable[i].iHeight ) ) )
		{
			pCapability->stMaxStillSize.iWidth = _OV5640StillResTable[i].iWidth;
			pCapability->stMaxStillSize.iHeight = _OV5640StillResTable[i].iHeight;
		}
	}

	// rotate
	pCapability->iSupportedRotateCnt = _ARRAY_SIZE( _OV5640RotationTable );
	for ( i = 0; i < pCapability->iSupportedRotateCnt; i++ )
	{
		pCapability->eSupportedRotate[i] = _OV5640RotationTable[i];
	}

	pCapability->iSupportedShotModeCnt = _ARRAY_SIZE( _OV5640ShotModeTable );
	for ( i = 0; i < pCapability->iSupportedShotModeCnt; i++ )
	{
		pCapability->eSupportedShotMode[i] = _OV5640ShotModeTable[i];
	}

	return CAM_ERROR_NONE;

}

CAM_Error OV5640_GetShotModeCapability( CAM_ShotMode eShotMode, CAM_ShotModeCapability *pShotModeCap )
{
	CAM_Int32u i;
	// BAC check
	for ( i = 0; i < _ARRAY_SIZE( _OV5640ShotModeTable ); i++ )
	{
		if ( _OV5640ShotModeTable[i] == eShotMode )
		{
			break;
		}
	}

	if ( i >= _ARRAY_SIZE(_OV5640ShotModeTable) || pShotModeCap ==NULL ) 
	{
		return CAM_ERROR_BADARGUMENT;
	}

    (void)(_OV5640ShotModeCap[eShotMode])( pShotModeCap );

	return CAM_ERROR_NONE;
}

static const char* OV5640_FLASH_LIGH_SWITCH_PATH_HW3 = "/sys/class/gpio/gpio47/value";
static const char* OV5640_FLASH_LIGH_SWITCH_PATH_HW4 = "/sys/class/gpio/gpio8/value";
static const char* OV5640_FLASH_LIGH_SWITCH_PATH;
static const char* OV5640_FLASH_BRIGHTNESS_PATH = "/sys/class/leds/flashlight/brightness";

#define FLASH_OFF_BRIGHTNESS 255


static CAM_Error _ov5640_set_flash_brightness(CAM_Int32u brightness )
{
	CAM_Error result = CAM_ERROR_NONE;
	char buf[11];
	CAM_Int32s len;

	len=sprintf(buf,"%d",brightness);

	int fd = open(OV5640_FLASH_BRIGHTNESS_PATH, O_RDWR);
	if (fd >= 0) 
	{
		if(write(fd, buf, len)!=len)
		{
			result = CAM_ERROR_DRIVEROPFAILED;
		}
		close(fd);
	} 
	else 
	{
		TRACE( CAM_ERROR, "_ov5640_set_flash_brightness:open %s ret %d\n", OV5640_FLASH_BRIGHTNESS_PATH,fd);
		result = CAM_ERROR_NOTIMPLEMENTED;
	}
	return result;
}

static CAM_Error _ov5640_set_flashlight(_V4L2SensorState *pV4l2State, CAM_Bool on, int isTorch )
{
	CAM_Error result = CAM_ERROR_NONE;
	int fd = -1;	

	if(on)
	{
		unsigned int i,brightness,arraysize;
		_CAM_ParameterEntry *table;

		if(isTorch)
		{
			table=_OV5640TorchBrightness;
			arraysize=_ARRAY_SIZE(_OV5640TorchBrightness);
		}
		else
		{
			table=_OV5640FlashBrightness;
			arraysize=_ARRAY_SIZE(_OV5640FlashBrightness);
		}

		brightness=table[0].stSetParam.value;
		for ( i = 0; i < arraysize; i++ ) 
		{
			if ( pV4l2State->stShotParam.iFlashBrightness <= table[i].iParameter )
				brightness=table[i].stSetParam.value;
			if ( pV4l2State->stShotParam.iFlashBrightness == table[i].iParameter ) 
			{
				break;
			}
		}
		if ( i >= arraysize ) 
		{
			TRACE( CAM_WARN, "unsupported flash brightness %d isTorch %d\n",pV4l2State->stShotParam.iFlashBrightness,isTorch);
		}
		CELOG( "flash brightness=%d isTorch %d\n", brightness,isTorch);
		result = _ov5640_set_flash_brightness(brightness);
		if ( result != CAM_ERROR_NONE) 
		{
			return result;
		}
	}
	else
	{
		result = _ov5640_set_flash_brightness(FLASH_OFF_BRIGHTNESS);
		if ( result != CAM_ERROR_NONE) 
		{
			return result;
		}
	}

	fd = open(OV5640_FLASH_LIGH_SWITCH_PATH, O_RDWR);
	
	if (fd >= 0) 
	{
		if(write(fd, on?"1":"0", 1)!=1)
		{
			result = CAM_ERROR_DRIVEROPFAILED;
		}
		close(fd);
	} 
	else 
	{
		TRACE( CAM_ERROR, "_ov5640_set_flashlight:open %s ret %d\n", OV5640_FLASH_LIGH_SWITCH_PATH,fd);
		result = CAM_ERROR_NOTIMPLEMENTED;
	}
	return result;
}


CAM_Error OV5640_Init( void **ppDevice, void *pSensorEntry ) 
{
	CAM_Error  error      = CAM_ERROR_NONE;
	CAM_Int32s iSkipFrame = 0;
	_V4L2SensorAttribute  _OV5640Attri;
	_V4L2SensorEntry *pSensor = (_V4L2SensorEntry*)(pSensorEntry);
	OV5640State *pState = (OV5640State*)malloc( sizeof( OV5640State ) );
	char hw_ver[PROPERTY_VALUE_MAX];

	memset( &_OV5640Attri, 0, sizeof(_V4L2SensorAttribute) );

	*ppDevice = pState;
	if ( *ppDevice == NULL )
	{
		return CAM_ERROR_OUTOFMEMORY;
	}

	_OV5640Attri.stV4L2SensorEntry.iV4L2SensorID = pSensor->iV4L2SensorID;
	strcpy( _OV5640Attri.stV4L2SensorEntry.sDeviceName, pSensor->sDeviceName );

	_OV5640Attri.fnStartFlash = NULL;
	_OV5640Attri.fnStopFlash =	NULL;

#if defined( BUILD_OPTION_DEBUG_DISABLE_SAVE_RESTORE_3A )
	iSkipFrame = 2;
	_OV5640Attri.fnSaveAeAwb = NULL;
	_OV5640Attri.fnRestoreAeAwb = NULL;
#else
	iSkipFrame = 2;
	_OV5640Attri.fnSaveAeAwb = _OV5640SaveAeAwb;
	_OV5640Attri.fnRestoreAeAwb = _OV5640RestoreAeAwb;
#endif

	_OV5640Attri.fnApplyShotParam = _OV5640ApplyShotParam;

	/* these callbacks make ov5640 has no output, so just disable it */
	iSkipFrame = 0;
	_OV5640Attri.fnSaveAeAwb = NULL;
	_OV5640Attri.fnRestoreAeAwb = NULL;
	
	_OV5640Attri.pSaveRestoreUserData = (void*)pState;
	error = V4L2_Init( &(pState->stV4L2), &(_OV5640Attri), iSkipFrame );

	/* here we can get default shot params */
	pState->stV4L2.stShotParam.eShotMode        = CAM_SHOTMODE_AUTO;
	pState->stV4L2.stShotParam.eExpMode         = CAM_EXPOSUREMODE_AUTO;
	pState->stV4L2.stShotParam.eExpMeterMode    = CAM_EXPOSUREMETERMODE_AUTO;
	pState->stV4L2.stShotParam.iEvCompQ16       = 0;
	pState->stV4L2.stShotParam.eIsoMode         = CAM_ISO_AUTO;
	pState->stV4L2.stShotParam.iShutterSpeedQ16 = -1;
	pState->stV4L2.stShotParam.iFNumQ16         = 1;
	pState->stV4L2.stShotParam.eBandFilterMode  = CAM_BANDFILTER_50HZ;
	pState->stV4L2.stShotParam.eWBMode          = CAM_WHITEBALANCEMODE_AUTO;
	pState->stV4L2.stShotParam.eFocusMode       = CAM_FOCUS_AUTO_ONESHOT;
	pState->stV4L2.stShotParam.iDigZoomQ16      = 0;
	pState->stV4L2.stShotParam.eColorEffect     = CAM_COLOREFFECT_OFF;
	pState->stV4L2.stShotParam.iSaturation      = 64;
	pState->stV4L2.stShotParam.iBrightness      = 0;
	pState->stV4L2.stShotParam.iContrast        = 0;
	pState->stV4L2.stShotParam.iSharpness       = 0;
	pState->stV4L2.stShotParam.iFlashBrightness = 0;
	pState->stV4L2.stShotParam.eFlashMode       = CAM_FLASH_OFF;
	pState->stV4L2.stShotParam.bVideoStabilizer = CAM_FALSE;
	pState->stV4L2.stShotParam.bVideoNoiseReducer = CAM_FALSE;
	pState->stV4L2.stShotParam.bZeroShutterLag    = CAM_FALSE;
	pState->stV4L2.stShotParam.bHighDynamicRange  = CAM_FALSE;

	pState->stV4L2.stShotParam.iBurstCnt          = 1;
	/* get default JPEG params */
	pState->stV4L2.stJpegParam.iSampling      = 1; // 0 - 420, 1 - 422, 2 - 444
	pState->stV4L2.stJpegParam.iQualityFactor = 67;
	pState->stV4L2.stJpegParam.bEnableExif    = CAM_FALSE;
	pState->stV4L2.stJpegParam.bEnableThumb   = CAM_FALSE;
	pState->stV4L2.stJpegParam.iThumbWidth    = 0;
	pState->stV4L2.stJpegParam.iThumbHeight   = 0;

	property_get("ro.hardware.rev", hw_ver, "4");
	if (atoi(hw_ver) < 4)
	{
		OV5640_FLASH_LIGH_SWITCH_PATH = OV5640_FLASH_LIGH_SWITCH_PATH_HW3;
	}
	else
	{
		OV5640_FLASH_LIGH_SWITCH_PATH = OV5640_FLASH_LIGH_SWITCH_PATH_HW4;
	}

#if defined( BUILD_OPTION_STRATEGY_ENABLE_AUTOFOCUS )
	//error = V4L2_AFDownload( &pState->stV4L2 );
#endif

	return error;
}

CAM_Error OV5640_Deinit( void *pDevice )
{
	OV5640State *pState = (OV5640State*)pDevice;

	(void)V4L2_Deinit( &pState->stV4L2 );

	free( pState );
	_ov5640_set_flashlight( &pState->stV4L2, CAM_FALSE, 0 );

	return CAM_ERROR_NONE;
}

CAM_Error _OV5640SetJpegParam( void *pDevice, CAM_JpegParam *pJpegParam );
CAM_Error OV5640_Config( void *pDevice, _CAM_SmartSensorConfig *pSensorConfig )
{
	OV5640State *pState = (OV5640State*)pDevice;
	CAM_Error   error   = CAM_ERROR_NONE;

	error = V4L2_Config( &pState->stV4L2, pSensorConfig );
	if ( error != CAM_ERROR_NONE )
	{
		return error;
	}

	if ( pSensorConfig->eState != CAM_CAPTURESTATE_IDLE )
	{
		/*if ( pSensorConfig->eFormat == CAM_IMGFMT_JPEG )
		{
			error = _OV5640SetJpegParam( pDevice, &(pSensorConfig->stJpegParam) );
			if ( error != CAM_ERROR_NONE )
			{
				TRACE( CAM_ERROR, "jpeg error!!!!!!!!!!!!!!\n");
				return error;
			}
		}*/
	}

	pState->stV4L2.stConfig = *pSensorConfig;

	/* moved from OV5640_Init to here. -guang */
#if defined( BUILD_OPTION_STRATEGY_ENABLE_AUTOFOCUS )
	if (0)
	{
		int ret;
		int w, h;
		char preview_size[PROPERTY_VALUE_MAX];
		property_get("persist.camera.preview_size", preview_size, "640x480");
		ret = sscanf(preview_size, "%dx%d", &w, &h);
		if (ret == 2 && w == pSensorConfig->iWidth)
			V4L2_AFDownload(&pState->stV4L2);
	}
#endif

	return CAM_ERROR_NONE;
}

CAM_Error OV5640_GetBufReq( void *pDevice, _CAM_SmartSensorConfig *pSensorConfig, CAM_ImageBufferReq *pBufReq )
{
	OV5640State *pState = (OV5640State*)pDevice;
	CAM_Error   error   = CAM_ERROR_NONE;

	error = V4L2_GetBufReq( &pState->stV4L2, pSensorConfig, pBufReq );

	return error;
}

CAM_Error OV5640_Enqueue( void *pDevice, CAM_ImageBuffer *pImgBuf )
{
	OV5640State *pState = (OV5640State*)pDevice;
	CAM_Error   error   = CAM_ERROR_NONE;

	error = V4L2_Enqueue( &pState->stV4L2, pImgBuf );

	return error;
}

CAM_Error OV5640_Dequeue( void *pDevice, CAM_ImageBuffer **ppImgBuf )
{
	OV5640State *pState = (OV5640State*)pDevice;
	CAM_Error   error   = CAM_ERROR_NONE;

	error = V4L2_Dequeue( &pState->stV4L2, ppImgBuf );

	if ( error == CAM_ERROR_NONE && (*ppImgBuf)->bEnableShotInfo )
	{
		error = _OV5640FillFrameShotInfo( pState, *ppImgBuf );
	}

	return error;
}

CAM_Error OV5640_Flush( void *pDevice )
{
	OV5640State *pState = (OV5640State*)pDevice;
	CAM_Error   error   = CAM_ERROR_NONE;

	error = V4L2_Flush( &pState->stV4L2 );

	return error;
}

static CAM_Int32s OV5640SetParamsReg( CAM_Int32s param,_CAM_ParameterEntry *optionArray,CAM_Int32s optionSize, CAM_Int32s sensorFD )
{
	CAM_Int32s i = 0;
	CAM_Int32s ret = 0;

	for ( i = 0; i < optionSize; i++ ) 
	{
		if ( param == optionArray[i].iParameter ) 
		{
			break;
		}
	}
	
	if ( i >= optionSize ) 
	{
		return -2;
	}

	if ( optionArray[i].stSetParam.stRegTable.pEntries[0].reg == 0x0000 )
	{
		// NOTE: in this case, no sensor registers need to be set
		ret = 0;
	}
	else
	{
		ret = _set_reg_array( sensorFD, optionArray[i].stSetParam.stRegTable.pEntries, optionArray[i].stSetParam.stRegTable.iLength );
	}

	return ret;
}

CAM_Error OV5640_SetShotParam( void *pDevice, _CAM_ShotParam *pShotParam )
{
	OV5640State *pState = (OV5640State*)pDevice;
	int         ret     = 0;
	CAM_Error   err;

	// ISO setting
	if ( pShotParam->eIsoMode != pState->stV4L2.stShotParam.eIsoMode ) 
	{

		unsigned int i = 0;
		for ( i = 0; i < _ARRAY_SIZE(_OV5640IsoMode); i++ ) 
		{
			if ( pShotParam->eIsoMode == _OV5640IsoMode[i].iParameter ) 
			{
				break;
			}
		}

		if ( i >= _ARRAY_SIZE(_OV5640IsoMode) ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		ret = _set_reg_array( pState->stV4L2.iSensorFD, _OV5640IsoMode[i].stSetParam.stRegTable.pEntries,_OV5640IsoMode[i].stSetParam.stRegTable.iLength );

		pState->stV4L2.stShotParam.eIsoMode = pShotParam->eIsoMode;

	}

	// Color Effect Setting
	if ( pShotParam->eColorEffect != pState->stV4L2.stShotParam.eColorEffect ) 
	{

		unsigned int i = 0;
		for ( i = 0; i < _ARRAY_SIZE(_OV5640ColorEffectMode); i++ ) 
		{
			if ( pShotParam->eColorEffect == _OV5640ColorEffectMode[i].iParameter ) 
			{
				break;
			}
		}

		if ( i >= _ARRAY_SIZE(_OV5640ColorEffectMode) ) 
		{

			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		ret = _set_reg_array( pState->stV4L2.iSensorFD, _OV5640ColorEffectMode[i].stSetParam.stRegTable.pEntries, _OV5640ColorEffectMode[i].stSetParam.stRegTable.iLength);

        pState->stV4L2.stShotParam.eColorEffect = pShotParam->eColorEffect;
	}

	// White Balance Setting
	if ( pShotParam->eWBMode != pState->stV4L2.stShotParam.eWBMode ) 
	{

		unsigned int i = 0;
		for ( i = 0; i < _ARRAY_SIZE(_OV5640WBMode); i++ ) 
		{
			if ( pShotParam->eWBMode == _OV5640WBMode[i].iParameter )
			{
				break;
			}
		}

		if ( i >= _ARRAY_SIZE(_OV5640WBMode) ) 
		{

			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		ret = _set_reg_array( pState->stV4L2.iSensorFD, _OV5640WBMode[i].stSetParam.stRegTable.pEntries, _OV5640WBMode[i].stSetParam.stRegTable.iLength);

		pState->stV4L2.stShotParam.eWBMode = pShotParam->eWBMode;
	}


	// Exposure Metering mode
	if ( pShotParam->eExpMeterMode != pState->stV4L2.stShotParam.eExpMeterMode ) 
	{

		unsigned int i = 0;
		for ( i = 0; i < _ARRAY_SIZE(_OV5640ExpMeterMode); i++ ) 
		{
			if ( pShotParam->eExpMeterMode == _OV5640ExpMeterMode[i].iParameter ) 
			{
				break;
			}
		}

		if ( i >= _ARRAY_SIZE(_OV5640ExpMeterMode) ) 
		{

		return CAM_ERROR_NOTSUPPORTEDARG;
		}

		ret = _set_reg_array( pState->stV4L2.iSensorFD, _OV5640ExpMeterMode[i].stSetParam.stRegTable.pEntries, _OV5640ExpMeterMode[i].stSetParam.stRegTable.iLength);

		pState->stV4L2.stShotParam.eExpMeterMode = pShotParam->eExpMeterMode;
	}

	// Shutter Speed setting
	if ( pShotParam->iShutterSpeedQ16 != pState->stV4L2.stShotParam.iShutterSpeedQ16 )
	{
		;
	}
	
	// Brightness
	if ( pShotParam->iBrightness!= pState->stV4L2.stShotParam.iBrightness ) 
	{
		unsigned int i = 0;
		for ( i = 0; i < _ARRAY_SIZE(_OV5640Brightness); i++ ) 
		{
			if ( pShotParam->iBrightness == _OV5640Brightness[i].iParameter ) 
			{
				break;
			}
		}

		if ( i >= _ARRAY_SIZE(_OV5640Brightness) ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		ret = _set_reg_array( pState->stV4L2.iSensorFD, _OV5640Brightness[i].stSetParam.stRegTable.pEntries, _OV5640Brightness[i].stSetParam.stRegTable.iLength);
		pState->stV4L2.stShotParam.iBrightness = pShotParam->iBrightness;
	}

	// Flash Brightness
	if ( pShotParam->iFlashBrightness!= pState->stV4L2.stShotParam.iFlashBrightness ) 
	{
		unsigned int i = 0;
		for ( i = 0; i < _ARRAY_SIZE(_OV5640FlashBrightness); i++ ) 
		{
			if ( pShotParam->iFlashBrightness == _OV5640FlashBrightness[i].iParameter ) 
			{
				break;
			}
		}

		if ( i >= _ARRAY_SIZE(_OV5640FlashBrightness) ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.iFlashBrightness = pShotParam->iFlashBrightness;
		if ( pShotParam->eFlashMode == CAM_FLASH_TORCH && pState->stV4L2.stShotParam.eFlashMode== CAM_FLASH_TORCH)
			_ov5640_set_flashlight(&pState->stV4L2, CAM_TRUE, 1);
	}


	// flash mode setting
	if ( pShotParam->eFlashMode != pState->stV4L2.stShotParam.eFlashMode ) 
	{
		if ( pShotParam->eFlashMode == CAM_FLASH_TORCH )
		{
			err = _ov5640_set_flashlight(&pState->stV4L2, CAM_TRUE, 1);
			if ( err != CAM_ERROR_NONE) 
			{
				return err;
			}
			pState->stV4L2.bIsFlashOn = CAM_TRUE;
		}
		else if ( pState->stV4L2.stShotParam.eFlashMode == CAM_FLASH_TORCH )
		{
			err = _ov5640_set_flashlight(&pState->stV4L2, CAM_FALSE, 0);
			if ( err != CAM_ERROR_NONE) 
			{
				return err;
			}
			pState->stV4L2.bIsFlashOn = CAM_FALSE;
		}

		pState->stV4L2.stShotParam.eFlashMode = pShotParam->eFlashMode;
	}

	// Contrast
	if ( pShotParam->iContrast!= pState->stV4L2.stShotParam.iContrast ) 
	{
		unsigned int i = 0;
		for ( i = 0; i < _ARRAY_SIZE(_OV5640Contrast); i++ ) 
		{
			if ( pShotParam->iContrast == _OV5640Contrast[i].iParameter ) 
			{
				break;
			}
		}

		if ( i >= _ARRAY_SIZE(_OV5640Contrast) ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		ret = _set_reg_array( pState->stV4L2.iSensorFD, _OV5640Contrast[i].stSetParam.stRegTable.pEntries, _OV5640Contrast[i].stSetParam.stRegTable.iLength);
		pState->stV4L2.stShotParam.iContrast = pShotParam->iContrast;
	}

	// EV
	if ( pShotParam->iEvCompQ16!= pState->stV4L2.stShotParam.iEvCompQ16 ) 
	{
		unsigned int i = 0;
		for ( i = 0; i < _ARRAY_SIZE(_OV5640EvComp); i++ ) 
		{
			if ( pShotParam->iEvCompQ16 == _OV5640EvComp[i].iParameter ) 
			{
				break;
			}
		}

		if ( i >= _ARRAY_SIZE(_OV5640EvComp) ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		ret = _set_reg_array( pState->stV4L2.iSensorFD, _OV5640EvComp[i].stSetParam.stRegTable.pEntries, _OV5640EvComp[i].stSetParam.stRegTable.iLength);
		pState->stV4L2.stShotParam.iEvCompQ16 = pShotParam->iEvCompQ16;
	}

	return CAM_ERROR_NONE;
}

CAM_Error OV5640_GetShotParam( void *pDevice, _CAM_ShotParam *pShotParam )
{
    OV5640State *pState = (OV5640State*)pDevice;

	*pShotParam = pState->stV4L2.stShotParam;
	return CAM_ERROR_NONE;
}

static CAM_Error OV5640_StartFlash( void *pDevice )
{
	CAM_Error       err;
	_V4L2SensorState *pV4l2State = pDevice;

	if ( pV4l2State->stShotParam.eFlashMode == CAM_FLASH_AUTO )
	{
		// TODO:add your auto strobe control algorithm here
		return CAM_ERROR_NOTSUPPORTEDARG;
	}
	
	if ( pV4l2State->stShotParam.eFlashMode == CAM_FLASH_ON ) 
	{
		if ( !pV4l2State->bIsFlashOn )
		{
			 /* enable strobe */
			err = _ov5640_set_flashlight(pV4l2State, CAM_TRUE, 0);
			if ( err != CAM_ERROR_NONE ) 
			{
				return err;
			}
			pV4l2State->bIsFlashOn = CAM_TRUE;
		}
	}
	return CAM_ERROR_NONE;
}

static CAM_Error OV5640_StopFlash( void *pDevice )
{
	CAM_Error       err;
	_V4L2SensorState *pV4l2State  = pDevice;

	if ( pV4l2State->stShotParam.eFlashMode == CAM_FLASH_AUTO )
	{
		// TODO: add your auto strobe control algorithm here
		return CAM_ERROR_NOTSUPPORTEDARG;
	}

	if ( pV4l2State->stShotParam.eFlashMode == CAM_FLASH_ON ) 
	{
		if ( pV4l2State->bIsFlashOn )
		{
			err = _ov5640_set_flashlight(pV4l2State, CAM_FALSE, 0);
			if ( err != CAM_ERROR_NONE ) 
			{
				return err;
			}
			pV4l2State->bIsFlashOn = CAM_FALSE;
		}
	}
	return CAM_ERROR_NONE;
}

static CAM_Error OV5640_GetFlashOn( void *pDevice, CAM_Bool *pFlashOn)
{
	_V4L2SensorState *pV4l2State  = pDevice;

	*pFlashOn = pV4l2State->bIsFlashOn;
	return CAM_ERROR_NONE;
}

CAM_Error OV5640_StartFocus( void *pDevice, CAM_AFMode eAFMode, void *pFocusParams )
{
	OV5640State *pState = (OV5640State*)pDevice;
	int count = 5;
	unsigned short temp;

	TRACE( CAM_WARN, "Start autofocus...\n");
	ov5640_read(pState, 0x3029, &temp);
	TRACE(CAM_WARN, "Current af status: %x\n", temp);
	ov5640_write(pState, 0x3022, 0x03);
	while (count--)
	{
		ov5640_read(pState, 0x3029, &temp);
		if (temp == 0x00 || temp == 0x10) break;
		usleep(100*1000); /* 100ms */
	}

	if (count <= 0)
	{
		TRACE( CAM_ERROR, "Start autofocus failed\n");
		return CAM_ERROR_TIMEOUT;
	}
	else
	{
		TRACE( CAM_ERROR, "Start autofocus done\n");
		FocusStartTime = IPP_TimeGetTickCount();
		return CAM_ERROR_NONE;
	}
}

CAM_Error OV5640_CancelFocus( void *pDevice )
{
	OV5640State *pState = (OV5640State*)pDevice;
	int count = 5;
	unsigned short temp;

	TRACE( CAM_WARN, "Stop autofocus...\n");
	ov5640_write(pState, 0x3022, 0x08);
	while (count--)
	{
		ov5640_read(pState, 0x3029, &temp);
		if (temp == 0x70) break;
		usleep(100*1000); /* 100ms */
	}

	if (count <= 0)
	{
		TRACE( CAM_ERROR, "Stop autofocus failed\n");
		return CAM_ERROR_TIMEOUT;
	}
	else
	{
		return CAM_ERROR_NONE;
	}
}

CAM_Error OV5640_CheckFocusState( void *pDevice, CAM_Bool *pFocusAutoStopped, _CAM_FocusState *pFocusState )
{
	CAM_Error error = CAM_ERROR_NONE;
	OV5640State *pState = (OV5640State*)pDevice;

	//TRACE( CAM_WARN, "Check autofocus state...");
	if (pState->stV4L2.stShotParam.eFocusMode == CAM_FOCUS_AUTO_ONESHOT)
	{
		unsigned short v;

		if (IPP_TimeGetTickCount() - FocusStartTime >= OV5640_AF_MAX_INTERVAL)
		{
			TRACE( CAM_ERROR, "Error: autofocus failed\n");
			*pFocusState       = CAM_AF_FAIL;
			*pFocusAutoStopped = CAM_TRUE;

			return error;
		}
		
		ov5640_read(pState, 0x3029, &v);
		if (v == 0x7f)
		{
			/* firmware downloaded and not run */
			TRACE( CAM_ERROR, "Error: firmware downloaded and not run\n");
			error = CAM_ERROR_NOTSUPPORTEDARG;
		}
		else if (v == 0x7e)
		{
			/* firmware initializing */
			TRACE( CAM_ERROR, "Error: firmware initializing\n");
			error = CAM_ERROR_NOTSUPPORTEDARG;
		}
		else if (v == 0x70)
		{
			/* idle */
			TRACE( CAM_ERROR, "Error: autofocus not started\n");
			error = CAM_ERROR_NOTSUPPORTEDARG;
		}
		else if (v == 0x00)
		{
			/* focusing */
			*pFocusState       = CAM_KEEP_STATUS;
			*pFocusAutoStopped = CAM_FALSE;
		}
		else if (v == 0x10)
		{
			/* focused */
			*pFocusState       = CAM_IN_FOCUS;
			*pFocusAutoStopped = CAM_TRUE;

			unsigned short temp;
			ov5640_read(pState, 0x3022, &temp);
			ov5640_write(pState, 0x3022, 0x06);
			TRACE( CAM_WARN, "Pause lens[%02x]\n", temp);
		}
		if(v!=0x00)
			TRACE( CAM_WARN, "0x%x\n", v);
	}
	else
	{
		TRACE( CAM_ERROR, "Error: cancel AF should only be called while focus mode set to AUTO  ( %s, %d )\n", __FILE__, __LINE__ );
		error = CAM_ERROR_NOTSUPPORTEDARG;
	}

	//TRACE( CAM_WARN, "%d\n", *pFocusState);

	return error;
}

CAM_Error OV5640_GetDigitalZoomCapability( CAM_Int32s iWidth, CAM_Int32s iHeight, CAM_Int32s *pSensorDigZoomQ16 )
{
	// TODO: add your get zoom capability code here,an refrence is ov5642.c
	*pSensorDigZoomQ16 = ( 1 << 16 );

	return CAM_ERROR_NONE;
}


_SmartSensorFunc func_ov5640 = 
{
	OV5640_GetCapability,
	OV5640_GetShotModeCapability,

	OV5640_SelfDetect,
	OV5640_Init,
	OV5640_Deinit,
	OV5640_Config,
	OV5640_GetBufReq,
	OV5640_Enqueue,
	OV5640_Dequeue,
	OV5640_Flush,
	OV5640_SetShotParam,
	OV5640_GetShotParam,

	OV5640_StartFocus,
	OV5640_CancelFocus,
	OV5640_CheckFocusState,
	
	OV5640_StartFlash,
	OV5640_StopFlash,
	OV5640_GetFlashOn,

	OV5640_GetDigitalZoomCapability,
};

CAM_Error _OV5640SetJpegParam( void *pDevice, CAM_JpegParam *pJpegParam )
{

	OV5640State *pState = (OV5640State*)pDevice;

	if ( pJpegParam->bEnableExif )
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}

	if ( pJpegParam->bEnableThumb )
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}

	if ( pJpegParam->iQualityFactor != 80 )
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}
	else
	{
		pState->stV4L2.stJpegParam.iQualityFactor = 80;
	}

	return CAM_ERROR_NONE;
}

/*-------------------------------------------------------------------------------------------------------------------------------------
 * OV5640 shotmode capability
 * TODO: if you enable new shot mode, pls add a correspoding modcap function here, and add it to OV5640_shotModeCap _OV5640ShotModeCap array
 *------------------------------------------------------------------------------------------------------------------------------------*/
static void _OV5640AutoModeCap( CAM_ShotModeCapability *pShotModeCap )
{
	unsigned int i;
	
	// exposure mode
	pShotModeCap->iSupportedExpModeCnt  = 1;
	pShotModeCap->eSupportedExpMode[0]  = CAM_EXPOSUREMODE_AUTO;

	// exposure metering mode 
	pShotModeCap->iSupportedExpMeterCnt = 1;
	pShotModeCap->eSupportedExpMeter[0] = CAM_EXPOSUREMETERMODE_AUTO;

	// EV compensation 
	pShotModeCap->iEVCompStepQ16 =1;
	pShotModeCap->iMinEVCompQ16 = -3;
	pShotModeCap->iMaxEVCompQ16 = 3;

	// ISO mode 
	pShotModeCap->iSupportedIsoModeCnt = 1;
	pShotModeCap->eSupportedIsoMode[0] = CAM_ISO_AUTO;

	// shutter speed
	pShotModeCap->iMinShutSpdQ16 = -1;
	pShotModeCap->iMaxShutSpdQ16 = -1;

	// F-number
	pShotModeCap->iMinFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);
	pShotModeCap->iMaxFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);

	// Band filter
	pShotModeCap->iSupportedBdFltModeCnt = 1; 
	pShotModeCap->eSupportedBdFltMode[0] = CAM_BANDFILTER_50HZ;

	// Flash mode
	pShotModeCap->iSupportedFlashModeCnt = 3;
	pShotModeCap->eSupportedFlashMode[0] = CAM_FLASH_OFF;
	pShotModeCap->eSupportedFlashMode[1] = CAM_FLASH_ON;
	pShotModeCap->eSupportedFlashMode[2] = CAM_FLASH_TORCH;

	// white balance mode
	pShotModeCap->iSupportedWBModeCnt = 1;
	pShotModeCap->eSupportedWBMode[0] = CAM_WHITEBALANCEMODE_AUTO;

	// focus mode
	pShotModeCap->iSupportedFocusModeCnt = 1;
	pShotModeCap->eSupportedFocusMode[0] = CAM_FOCUS_AUTO_ONESHOT;

	// AF mode 
	pShotModeCap->iSupportedAFModeCnt = 1;	/* disable af, CAM_FOCUS_AUTO_ONESHOT/CAM_FOCUS_INFINITY */
	pShotModeCap->eSupportedAFMode[0] = CAM_AFMODE_CENTER;

	// optical zoom mode
	pShotModeCap->iMinOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);

	// digital zoom mode
	pShotModeCap->iMinDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->bSupportSmoothDigZoom = CAM_FALSE;

	// fps
	pShotModeCap->iMinFpsQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxFpsQ16 = (CAM_Int32s)(40.0 * 65536 + 0.5);

	// color effect
	pShotModeCap->iSupportedColorEffectCnt = _ARRAY_SIZE(_OV5640ColorEffectMode);
	for (i=0; i<_ARRAY_SIZE(_OV5640ColorEffectMode); i++)
		pShotModeCap->eSupportedColorEffect[i] = _OV5640ColorEffectMode[i].iParameter; 

	// brightness
	pShotModeCap->iMinBrightness = -4;
	pShotModeCap->iMaxBrightness = 4;

	// contrast
	pShotModeCap->iMinContrast = -3;
	pShotModeCap->iMaxContrast = 3;

	// contrast
	pShotModeCap->iMinFlashBrightness= -2;
	pShotModeCap->iMaxFlashBrightness = 2;

	// saturation
	pShotModeCap->iMinSaturation = 0;
	pShotModeCap->iMaxSaturation = 0;

	// sharpness
	pShotModeCap->iMinSharpness = 0;
	pShotModeCap->iMaxSharpness = 0;

	// advanced features
	pShotModeCap->bSupportVideoStabilizer   = CAM_FALSE;
	pShotModeCap->bSupportVideoNoiseReducer	= CAM_FALSE;
	pShotModeCap->bSupportZeroShutterLag    = CAM_FALSE;
	pShotModeCap->bSupportBurstCapture      = CAM_FALSE;
	pShotModeCap->bSupportHighDynamicRange	= CAM_FALSE;
	pShotModeCap->iMaxBurstCnt              = 1;

	return;
}


/*
* OV5640 utility functions
*/

#define Capture_Framerate 375
#define Preview_FrameRate 1500

/******************************************************************************************************
//
// Name:         _ov5640_save_ae_awb
// Description:  save preview's AEC/AGC,AWB,and VTS(Vertical Total Size) values 
// Arguments:    pCurrent3A[OUTPUT]: pointer to the structure for save AE/AG/VTS values
// Return:       camera engine error code,refer to CameraEngine.h for more detail
// Notes:        
//     1) for more detail about OV5640 register, pls refer to its software application note
// Version Log:  version      Date          Author    Description
//                v0.1      07/14/2010    Matrix Yao   Create
//                v0.2      07/30/2010    Matrix Yao   rename function, add save AWB content
*******************************************************************************************************/

static CAM_Error _ov5640_save_ae_awb( CAM_Int32s iSensorFD, _OV5640_3AState *pCurrent3A )
{
	CAM_Int16u  reg      = 0; 

	CAM_Int16u exposure  = 0;
	CAM_Int16u gain      = 0;
	CAM_Int16u awbRgain  = 0;
	CAM_Int16u awbGgain  = 0;
	CAM_Int16u awbBgain  = 0;
	CAM_Int32u maxLines  = 0;

	// BAC (Bad Argument Check)
	_CHECK_BAD_POINTER( pCurrent3A );


	// check if AEC/AGC convergence
	// !!!FIXME
	if ( 1 )
	{
		;
	}


	// stop AEC/AGC
	reg = 0x0007;
	_set_sensor_reg( iSensorFD, OV5640_REG_AECAGC, reg );

	/* gain */
	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_GAINLOW, &reg );
	gain = ( reg & 0x000f ) + 16;
	if ( reg & 0x0010 )
	{
		gain <<= 1;
	}
	if ( reg & 0x0020 )
	{
		gain <<= 1;
	}
	if ( reg & 0x0040 )
	{
		gain <<= 1;
	}
	if ( reg & 0x0080 )
	{
		gain <<= 1;
	}
	pCurrent3A->gainQ4 = gain;

	/* exposure */
	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_EXPOSUREHIGH, &reg );
	exposure = ( reg & 0x000f );
	exposure <<= 12;

	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_EXPOSUREMID, &reg );
	reg = ( reg & 0x00ff );
	exposure += ( reg << 4 );

	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_EXPOSURELOW, &reg );
	reg = ( reg & 0x00f0 );
	exposure += ( reg >> 4 );

	pCurrent3A->exposure = exposure;

	/* VTS */
	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_VTSHIGH, &reg );
	maxLines = ( reg  & 0x00ff );
	maxLines <<= 8;

	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_VTSLOW, &reg );
	maxLines += ( reg & 0x00ff );

	pCurrent3A->maxLines = maxLines;

	/* AWB */
	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_CURRENT_AWBRHIGH, &reg );
	awbRgain = ( reg & 0x000f );
	awbRgain <<= 8;
	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_CURRENT_AWBRLOW, &reg );
	awbRgain |= reg;
	pCurrent3A->awbRgain = awbRgain;

	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_CURRENT_AWBGHIGH, &reg );
	awbGgain = ( reg & 0x000f );
	awbGgain <<= 8;
	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_CURRENT_AWBGLOW, &reg );
	awbGgain |= reg;
	pCurrent3A->awbGgain = awbGgain;

	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_CURRENT_AWBBHIGH, &reg );
	awbBgain = ( reg & 0x000f );
	awbBgain <<= 8;
	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_CURRENT_AWBBLOW, &reg );
	awbBgain |= reg;
	pCurrent3A->awbBgain = awbBgain;

	return  CAM_ERROR_NONE;

}

/******************************************************************************************************
//
// Name:         _ov5640_calc_new_3A
// Description:  calculate capture exposure from preview exposure
// Arguments:    p3Astat[IN/OUT] : preview's AEC paras(exposure\gain\maxlines)
// Return:       camera engine error code,refer to CameraEngine.h for more detail
// Notes:        
//     1) for more detail about OV5640 register, pls refer to its software application note
// Version Log:  version      Date          Author    Description
//                v0.1      07/14/1010    Matrix Yao   Create
//                v0.2      07/30/2010    Matrix Yao   rename function,etc
*******************************************************************************************************/

static CAM_Error _ov5640_calc_new_3A( CAM_Int32s iSensorFD, _OV5640_3AState *p3Astat )
{
	CAM_Int16u  reg = 0;

	// line_10ms is Banding Filter Value
	// here 10ms means the time unit is 10 mili-seconds
	CAM_Int32u lines_10ms        = 0;
	CAM_Int32u newMaxLines       = 0;

	CAM_Int64u newGainQ4         = 0;
	CAM_Int64u newExposure       = 0;
	CAM_Int64u newExposureGain   = 0;


	// stop AEC/AGC
	reg = 0x0007;
	_set_sensor_reg( iSensorFD, OV5640_REG_AECAGC, reg );

	// get capture VTS
	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_VTSHIGH, &reg );
	newMaxLines = ( reg & 0x00ff );
	newMaxLines <<= 8;

	reg = 0;
	_get_sensor_reg( iSensorFD, OV5640_REG_VTSLOW, &reg );
	newMaxLines += ( reg & 0x00ff );

	//get capture's banding filter value, 50Hz flicker as example
	// !!!FIXME
	lines_10ms = Capture_Framerate * newMaxLines / 10000;


	if ( p3Astat->maxLines == 0 )
	{
		p3Astat->maxLines = 1;
	}

	if ( lines_10ms == 0 ) 
	{
		lines_10ms = 1;
	}

	/*
	if (p3Astat->maxLines == 0 || lines_10ms == 0)
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}
	*/

	// calculate capture exposure, in uint of t_ROW_INTERVAL 
	newExposure = (p3Astat->exposure) * (Capture_Framerate) * newMaxLines / p3Astat->maxLines / Preview_FrameRate;

	// calculate exposure * gain for capture mode
	newExposureGain = newExposure * p3Astat->gainQ4;

	// get exposure for capture. 
	if ( newExposureGain < ((CAM_Int64u)newMaxLines << 4) ) 
	{
		newExposure = ( newExposureGain >> 4 );
		if ( newExposure > lines_10ms ) 
		{
			newExposure /= lines_10ms;
			newExposure *= lines_10ms;
		}
	}
	else 
	{
		newExposure = newMaxLines;
	}	

	if ( newExposure == 0 ) 
	{
		newExposure = 1;
	}

	// get gain for capture
	newGainQ4 = (newExposureGain * 2 / newExposure + 1) / 2;

	
	// update 3A status
	p3Astat->maxLines = newMaxLines;
	p3Astat->exposure = (CAM_Int16u)newExposure;
	p3Astat->gainQ4 = (CAM_Int16u)newGainQ4;

	// AWB no need to update
	return  CAM_ERROR_NONE;

}


/******************************************************************************************************
//
// Name:         _ov5640_set_new_exposure
// Description:  set capture exposure(exposure\gain\maxlines) manually
// Arguments:    pNew3A[INPUT] : capture's AEC paras(exposure\gain\maxlines)
// Return:       camera engine error code,refer to CameraEngine.h for more detail
// Notes:        
//     1) for more detail about OV5640 register, pls refer to its software application note;
//     2) pls update current AEC state once this function return CAM_ERROR_NONE;
// Version Log:  version      Date          Author    Description
//                v0.1      07/14/1010    Matrix Yao   Create
//                v0.2      07/30/2010    Matrix Yao   rename function,etc 
*******************************************************************************************************/

static CAM_Error _ov5640_set_new_exposure( CAM_Int32s iSensorFD,_OV5640_3AState *pNew3A )
{
	CAM_Int8u  exposureLow  = 0;
	CAM_Int8u  exposureMid  = 0;
	CAM_Int8u  exposureHigh = 0;

	CAM_Int8u  gain         = 0;

	CAM_Int16u gainQ4       = 0;  
	CAM_Int16u reg          = 0;

	CAM_Int32s  ret         = 0;            
	// calculate exposure value for OV5640 register
	exposureLow  = (CAM_Int8u)(((pNew3A->exposure) & 0x000f) << 4);
	exposureMid  = (CAM_Int8u)(((pNew3A->exposure) & 0x0ff0) >> 4);
	exposureHigh = (CAM_Int8u)(((pNew3A->exposure) & 0xf000) >> 12);

	// calculate gain value for OV5640 register
	gainQ4 = pNew3A->gainQ4;
	if ( gainQ4 > 31 )
	{
		gain   |= 0x10;
		gainQ4 >>= 1; 
	}

	if ( gainQ4 > 31 )
	{
		gain   |= 0x20;
		gainQ4 >>= 1; 
	}

	if ( gainQ4 > 31 )
	{
		gain   |= 0x40;
		gainQ4 >>= 1; 
	}

	if ( gainQ4 > 31 )
	{
		gain   |= 0x80;
		gainQ4 >>= 1; 
	}

	if ( gainQ4 > 16 )
	{
		gain |= ( ( gainQ4 -16 ) & 0x0f );
	}  

	// I still don't know the real meaning of the following statement 
	if ( gain == 0x10 )
	{
		gain = 0x11;
	}  

	// write gain and exposure to OV5640 registers

	/* gain */
	reg = (CAM_Int16u)gain;
	ret = _set_sensor_reg( iSensorFD, OV5640_REG_GAINLOW, reg );
	if ( ret )
	{
		return CAM_ERROR_DRIVEROPFAILED;
	}

	/* exposure */
	reg = (CAM_Int16u)exposureLow;
	ret = _set_sensor_reg( iSensorFD, OV5640_REG_EXPOSURELOW, reg );

	reg = (CAM_Int16u)exposureMid;
	ret = _set_sensor_reg( iSensorFD, OV5640_REG_EXPOSUREMID, reg );

	reg = (CAM_Int16u)exposureHigh;
	ret = _set_sensor_reg( iSensorFD, OV5640_REG_EXPOSUREHIGH, reg );

	/* AWB */
	reg = 0x0083;
	ret = _set_sensor_reg( iSensorFD, OV5640_REG_AWBC_22, reg );

	reg = ( pNew3A->awbRgain & 0x0f00 );
	reg = (reg >> 8);
	ret = _set_sensor_reg( iSensorFD, OV5640_REG_SET_AWBRHIGH, reg );
	reg = (pNew3A->awbRgain & 0x00ff);
	ret = _set_sensor_reg( iSensorFD, OV5640_REG_SET_AWBRLOW, reg );

	reg = (pNew3A->awbGgain & 0x0f00);
	reg = (reg >> 8);
	ret = _set_sensor_reg( iSensorFD, OV5640_REG_SET_AWBGHIGH, reg );
	reg = (pNew3A->awbGgain & 0x00ff);
	ret = _set_sensor_reg( iSensorFD, OV5640_REG_SET_AWBGLOW, reg );

	reg = (pNew3A->awbBgain & 0x0f00);
	reg = (reg >> 8);
	ret = _set_sensor_reg( iSensorFD, OV5640_REG_SET_AWBBHIGH, reg );
	reg = (pNew3A->awbBgain & 0x00ff);
	ret = _set_sensor_reg( iSensorFD, OV5640_REG_SET_AWBBLOW, reg );


	return  CAM_ERROR_NONE;
}

static CAM_Error _OV5640SaveAeAwb( const _CAM_SmartSensorConfig *pOldCfg, void *pUserData )
{
	CAM_Error   error   = CAM_ERROR_NONE;
	OV5640State *pState = (OV5640State*)pUserData;

	if ( pOldCfg->eState != CAM_CAPTURESTATE_PREVIEW )
	{
		return CAM_ERROR_NONE;
	}

	error = _ov5640_save_ae_awb( pState->stV4L2.iSensorFD, &pState->st3Astat );

	return error;
}

static CAM_Error _OV5640RestoreAeAwb( const _CAM_SmartSensorConfig *pNewCfg, void *pUserData )
{
	CAM_Error   error   = CAM_ERROR_NONE;
	OV5640State *pState = (OV5640State*)pUserData;

	if ( pNewCfg->eState != CAM_CAPTURESTATE_STILL )
	{
		return CAM_ERROR_NONE;
	}

	error = _ov5640_calc_new_3A( pState->stV4L2.iSensorFD, &pState->st3Astat );
	if ( error != CAM_ERROR_NONE )
	{
		return error;
	}

	error = _ov5640_set_new_exposure( pState->stV4L2.iSensorFD, &pState->st3Astat );
	return error;
}


static CAM_Error _OV5640ApplyShotParam(void* pUserData)
{
	OV5640State *pState = (OV5640State*) pUserData;

	OV5640SetParamsReg(pState->stV4L2.stShotParam.eWBMode, _OV5640WBMode, _ARRAY_SIZE(_OV5640WBMode), pState->stV4L2.iSensorFD);

	OV5640SetParamsReg(pState->stV4L2.stShotParam.eColorEffect, _OV5640ColorEffectMode, _ARRAY_SIZE(_OV5640ColorEffectMode), pState->stV4L2.iSensorFD);

	OV5640SetParamsReg(pState->stV4L2.stShotParam.eBandFilterMode, _OV5640BdFltMode, _ARRAY_SIZE(_OV5640BdFltMode), pState->stV4L2.iSensorFD);

	OV5640SetParamsReg(pState->stV4L2.stShotParam.iEvCompQ16, _OV5640EvComp, _ARRAY_SIZE(_OV5640EvComp), pState->stV4L2.iSensorFD);

	OV5640SetParamsReg(pState->stV4L2.stShotParam.iBrightness, _OV5640Brightness, _ARRAY_SIZE(_OV5640Brightness), pState->stV4L2.iSensorFD);

	OV5640SetParamsReg(pState->stV4L2.stShotParam.iContrast, _OV5640Contrast, _ARRAY_SIZE(_OV5640Contrast), pState->stV4L2.iSensorFD);

	return CAM_ERROR_NONE;
}

// get shot info
static CAM_Error _OV5640FillFrameShotInfo( OV5640State *pState, CAM_ImageBuffer *pImgBuf )
{
	// TODO: add real value of these parameters here

	CAM_Error    error      = CAM_ERROR_NONE;
	CAM_ShotInfo *pShotInfo = &(pImgBuf->stShotInfo);

	pShotInfo->iExposureTimeQ16    = (1 << 16) / 30;
	pShotInfo->iFNumQ16            = (int)( 2.8 * 65536 + 0.5 );
	pShotInfo->eExposureMode       = CAM_EXPOSUREMODE_AUTO;
	pShotInfo->eExpMeterMode       = CAM_EXPOSUREMETERMODE_MEAN;
	pShotInfo->iISOSpeed           = 100;
	pShotInfo->iSubjectDistQ16     = 0;
	pShotInfo->iFlashStatus        = 0x0010;
	pShotInfo->iFocalLenQ16        = (int)( 3.79 * 65536 + 0.5 );
	pShotInfo->iFocalLen35mm       = 0;       

	return error;
}
