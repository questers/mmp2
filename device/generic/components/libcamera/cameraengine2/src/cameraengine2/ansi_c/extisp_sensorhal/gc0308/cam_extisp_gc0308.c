/*******************************************************************************
//(C) Copyright [2010 - 2011] Marvell International Ltd.
//All Rights Reserved
*******************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "cam_log.h"
#include "cam_utility.h"


#include "cam_extisp_sensorwrapper.h"
#include "cam_extisp_v4l2base.h"
#include "cam_extisp_gc0308.h"

#define REG(a,b) {a, 0, 7, 0, b}

// NOTE: if you want to enable static resolution table to bypass sensor dynamically detect to save camera-off to viwerfinder-on latency, 
//       you can fill the following four tables according to your sensor's capability. And open macro 
//       BUILD_OPTION_STARTEGY_DISABLE_DYNAMIC_SENSOR_DETECT in cam_extisp_buildopt.h

/* Basic Sensor video/preview resolution table */
CAM_Size _GC0308VideoResTable[CAM_MAX_SUPPORT_IMAGE_SIZE_CNT] = 
{
	{0, 0},
};

/* Basic Sensor still resolution table */
CAM_Size _GC0308StillResTable[CAM_MAX_SUPPORT_IMAGE_SIZE_CNT] = 
{ 
	{0, 0},
};

/* Basic Sensor video/preview format table */
CAM_ImageFormat _GC0308VideoFormatTable[CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT] = 
{ 
	0, 
};

/* Basic Sensor still format table */
CAM_ImageFormat _GC0308StillFormatTable[CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT] = 
{
	0,
};


CAM_FlipRotate _GC0308RotationTable[] = 
{
	CAM_FLIPROTATE_NORMAL,
};


CAM_ShotMode _GC0308ShotModeTable[] = 
{
	CAM_SHOTMODE_AUTO,
	//CAM_SHOTMODE_MANUAL,
};

_CAM_RegEntry _GC0308ExpMeter_Mean[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _GC0308ExpMeter[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_EXPOSUREMETERMODE_MEAN, _GC0308ExpMeter_Mean),
};

_CAM_RegEntry _GC0308IsoMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _GC0308IsoMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_ISO_AUTO, _GC0308IsoMode_Auto),
};

_CAM_RegEntry _GC0308BdFltMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _GC0308BdFltMode_Off[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _GC0308BdFltMode_50Hz[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _GC0308BdFltMode_60Hz[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _GC0308BdFltMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_BANDFILTER_50HZ, _GC0308BdFltMode_50Hz),
};

_CAM_RegEntry _GC0308FlashMode_Off[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _GC0308FlashMode_On[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _GC0308FlashMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _GC0308FlashMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_FLASH_OFF,	_GC0308FlashMode_Off),
};

_CAM_RegEntry GC0308WBMode_Auto[] =
{
	REG(0x5a,0x56),
	REG(0x5b,0x40),
	REG(0x5c,0x4a),
	REG(0x22,0x57),
};

_CAM_RegEntry GC0308WBMode_CLOUDY[] =
{
	REG(0x22,0x55),
	REG(0x5a,0x8c),
	REG(0x5b,0x50),
	REG(0x5c,0x40),
};

_CAM_RegEntry GC0308WBMode_DAYLIGHT[] =
{
	REG(0x22,0x55),
	REG(0x5a,0x74),
	REG(0x5b,0x52),
	REG(0x5c,0x40),
};

_CAM_RegEntry GC0308WBMode_INCANDESCENT[] =
{
	REG(0x22,0x55),
	REG(0x5a,0x48),
	REG(0x5b,0x40),
	REG(0x5c,0x5c),
};

_CAM_RegEntry GC0308WBMode_FLUORESCENT1[] =
{
	REG(0x22,0x55),
	REG(0x5a,0x40),
	REG(0x5b,0x42),
	REG(0x5c,0x50),
};

_CAM_ParameterEntry GC0308WBMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_AUTO, GC0308WBMode_Auto),
	/*
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_CLOUDY, GC0308WBMode_CLOUDY),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_DAYLIGHT, GC0308WBMode_DAYLIGHT),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_INCANDESCENT, GC0308WBMode_INCANDESCENT),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_FLUORESCENT1, GC0308WBMode_FLUORESCENT1),*/
};

_CAM_RegEntry _GC0308FocusMode_Infinity[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _GC0308FocusMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_FOCUS_INFINITY,  _GC0308FocusMode_Infinity),
};

_CAM_RegEntry GC0308ColorEffectMode_OFF[] = 
{
	REG(0x23, 0x00),
	REG(0x2d, 0x0a), 
	REG(0x20, 0x7f), 
	REG(0xd2, 0x90), 
	REG(0x73, 0x00), 
	REG(0x77, 0x38),
	REG(0xb3, 0x40), 
	REG(0xb4, 0x80), 
	REG(0xba, 0x00), 
	REG(0xbb, 0x00), 
};

_CAM_RegEntry GC0308ColorEffectMode_SEPIA[] = 
{
	REG(0x23, 0x02),
	REG(0x2d, 0x0a), 
	REG(0x20, 0xff), 
	REG(0xd2, 0x90), 
	REG(0x73, 0x00), 
	REG(0xb3, 0x40), 
	REG(0xb4, 0x80), 
	REG(0xba, 0xd0), 
	REG(0xbb, 0x28), 
};

_CAM_RegEntry GC0308ColorEffectMode_GRAYSCALE[] = 
{
	REG(0x23, 0x02),
	REG(0x2d, 0x0a), 
	REG(0x20, 0xff), 
	REG(0xd2, 0x90), 
	REG(0x73, 0x00), 
	REG(0xb3, 0x40), 
	REG(0xb4, 0x80), 
	REG(0xba, 0x00), 
	REG(0xbb, 0x00), 
};

_CAM_ParameterEntry GC0308ColorEffectMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_OFF, GC0308ColorEffectMode_OFF),
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_SEPIA, GC0308ColorEffectMode_SEPIA),
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_GRAYSCALE, GC0308ColorEffectMode_GRAYSCALE),
};


static CAM_Error _GC0308SaveAeAwb( const _CAM_SmartSensorConfig*, void* );
static CAM_Error _GC0308RestoreAeAwb( const _CAM_SmartSensorConfig*, void* );
static CAM_Error _GC0308StartFlash( void* );
static CAM_Error _GC0308StopFlash( void* );
static CAM_Error _GC0308ApplyShotParam(void*);
static CAM_Error _GC0308FillFrameShotInfo( GC0308State*, CAM_ImageBuffer* );

// shot mode capability
static void _GC0308AutoModeCap( CAM_ShotModeCapability* );
static void _GC0308ManualModeCap( CAM_ShotModeCapability*);
// static void _GC0308NightModeCap( CAM_ShotModeCapability*);
/* shot mode cap function table */
GC0308_ShotModeCap _GC0308ShotModeCap[CAM_SHOTMODE_NUM] = { 
	_GC0308AutoModeCap     , // CAM_SHOTMODE_AUTO = 0,
	_GC0308ManualModeCap   , // CAM_SHOTMODE_MANUAL,
	NULL                   , // CAM_SHOTMODE_PORTRAIT,
	NULL                   , // CAM_SHOTMODE_LANDSCAPE,
	NULL                   , // CAM_SHOTMODE_NIGHTPORTRAIT,
	NULL                   , // CAM_SHOTMODE_NIGHTSCENE,
	NULL                   , // CAM_SHOTMODE_CHILD,
	NULL                   , // CAM_SHOTMODE_INDOOR,
	NULL                   , // CAM_SHOTMODE_PLANTS,
	NULL                   , // CAM_SHOTMODE_SNOW,
	NULL                   , // CAM_SHOTMODE_BEACH,
	NULL                   , // CAM_SHOTMODE_FIREWORKS,
	NULL                   , // CAM_SHOTMODE_SUBMARINE, 
};  

extern _SmartSensorFunc func_gc0308; 
CAM_Error GC0308_SelfDetect( _SmartSensorAttri *pSensorInfo )
{
	CAM_Error error = CAM_ERROR_NONE;
	
	// NOTE:  If you open macro BUILD_OPTION_STARTEGY_DISABLE_DYNAMIC_SENSOR_DETECT in cam_extisp_buildopt.h 
	//        to bypass sensor dynamically detect to save camera-off to viwerfinder-on latency, you should initilize
	//        _GC0308VideoResTable/_GC0308StillResTable/_GC0308VideoFormatTable/_GC0308StillFormatTable manually.

#if !defined( BUILD_OPTION_STARTEGY_DISABLE_DYNAMIC_SENSOR_DETECT )	
	error = V4L2_SelfDetect( pSensorInfo, "gc0308", &func_gc0308,
	                         _GC0308VideoResTable, _GC0308StillResTable,
	                         _GC0308VideoFormatTable, _GC0308StillFormatTable );

#else
	{
		_V4L2SensorEntry *pSensorEntry = (_V4L2SensorEntry*)( pSensorInfo->cReserved );
		strcpy( pSensorInfo->sSensorName, "GC0308-unknown" );
		pSensorInfo->pFunc = &func_gc0308;

		// FIXME: the following is just an example in Marvell platform, you should change it according to your driver implementation
		strcpy( pSensorEntry->sDeviceName, "/dev/video0" );
		pSensorEntry->iV4L2SensorID = 0;
	}
#endif

	return error;
} 


CAM_Error GC0308_GetCapability( _CAM_SmartSensorCapability *pCapability )
{
	CAM_Int32s i = 0;


	// preview/video supporting 
	// format
	pCapability->iSupportedVideoFormatCnt = 0;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT; i++ ) 
	{
		if ( _GC0308VideoFormatTable[i] == 0 )
		{
			break;
		}
		pCapability->eSupportedVideoFormat[pCapability->iSupportedVideoFormatCnt] = _GC0308VideoFormatTable[i];
		pCapability->iSupportedVideoFormatCnt++;
	}

	pCapability->bArbitraryVideoSize     = CAM_FALSE;
	pCapability->iSupportedVideoSizeCnt  = 0;
	pCapability->stMinVideoSize.iWidth   = _GC0308VideoResTable[0].iWidth;
	pCapability->stMinVideoSize.iHeight  = _GC0308VideoResTable[0].iHeight;
	pCapability->stMaxVideoSize.iWidth   = _GC0308VideoResTable[0].iWidth;
	pCapability->stMaxVideoSize.iHeight  = _GC0308VideoResTable[0].iHeight;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_SIZE_CNT; i++ )
	{
		if ( _GC0308VideoResTable[i].iWidth == 0 || _GC0308VideoResTable[i].iHeight == 0)
		{
			break;
		}
		pCapability->stSupportedVideoSize[pCapability->iSupportedVideoSizeCnt] = _GC0308VideoResTable[i];
		pCapability->iSupportedVideoSizeCnt++;

		if ( ( pCapability->stMinVideoSize.iWidth > _GC0308VideoResTable[i].iWidth ) || 
		     ( ( pCapability->stMinVideoSize.iWidth == _GC0308VideoResTable[i].iWidth ) && ( pCapability->stMinVideoSize.iHeight > _GC0308VideoResTable[i].iHeight ) ) ) 
		{
			pCapability->stMinVideoSize.iWidth = _GC0308VideoResTable[i].iWidth;
			pCapability->stMinVideoSize.iHeight = _GC0308VideoResTable[i].iHeight;
		}
		if ( ( pCapability->stMaxVideoSize.iWidth < _GC0308VideoResTable[i].iWidth) ||
		     ( ( pCapability->stMaxVideoSize.iWidth == _GC0308VideoResTable[i].iWidth ) && ( pCapability->stMaxVideoSize.iHeight < _GC0308VideoResTable[i].iHeight ) ) )
		{
			pCapability->stMaxVideoSize.iWidth = _GC0308VideoResTable[i].iWidth;
			pCapability->stMaxVideoSize.iHeight = _GC0308VideoResTable[i].iHeight;
		}
	}

	// still capture supporting
	// format
	pCapability->iSupportedStillFormatCnt           = 0;
	pCapability->stSupportedJPEGParams.bSupportJpeg = CAM_FALSE;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT; i++ ) 
	{
		if ( _GC0308StillFormatTable[i] == CAM_IMGFMT_JPEG )
		{
			// JPEG encoder functionalities
			pCapability->stSupportedJPEGParams.bSupportJpeg = CAM_TRUE;
			pCapability->stSupportedJPEGParams.bSupportExif = CAM_FALSE;
			pCapability->stSupportedJPEGParams.bSupportThumb = CAM_FALSE;
			pCapability->stSupportedJPEGParams.iMinQualityFactor = 80;
			pCapability->stSupportedJPEGParams.iMaxQualityFactor = 80;
		}
		if ( _GC0308StillFormatTable[i] == 0 )
		{
			break;
		}
		pCapability->eSupportedStillFormat[pCapability->iSupportedStillFormatCnt] = _GC0308StillFormatTable[i];
		pCapability->iSupportedStillFormatCnt++;
	}
	// resolution
	pCapability->bArbitraryStillSize    = CAM_FALSE;
	pCapability->iSupportedStillSizeCnt = 0;
	pCapability->stMinStillSize.iWidth  = _GC0308StillResTable[0].iWidth;
	pCapability->stMinStillSize.iHeight = _GC0308StillResTable[0].iHeight;
	pCapability->stMaxStillSize.iWidth  = _GC0308StillResTable[0].iWidth;
	pCapability->stMaxStillSize.iHeight = _GC0308StillResTable[0].iHeight;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_SIZE_CNT; i++ )
	{
		if ( _GC0308StillResTable[i].iWidth == 0 || _GC0308StillResTable[i].iHeight == 0 )
		{
			break;
		}

		pCapability->stSupportedStillSize[pCapability->iSupportedStillSizeCnt] = _GC0308StillResTable[i];
		pCapability->iSupportedStillSizeCnt++;

		if ( ( pCapability->stMinStillSize.iWidth > _GC0308StillResTable[i].iWidth ) ||
		     ( ( pCapability->stMinStillSize.iWidth == _GC0308StillResTable[i].iWidth ) && ( pCapability->stMinStillSize.iHeight > _GC0308StillResTable[i].iHeight ) ) )
		{
			pCapability->stMinStillSize.iWidth  = _GC0308StillResTable[i].iWidth;
			pCapability->stMinStillSize.iHeight = _GC0308StillResTable[i].iHeight;
		}
		if ( ( pCapability->stMaxStillSize.iWidth < _GC0308StillResTable[i].iWidth ) || 
		     ( ( pCapability->stMaxStillSize.iWidth == _GC0308StillResTable[i].iWidth ) && ( pCapability->stMaxStillSize.iHeight < _GC0308StillResTable[i].iHeight ) ) )
		{
			pCapability->stMaxStillSize.iWidth = _GC0308StillResTable[i].iWidth;
			pCapability->stMaxStillSize.iHeight = _GC0308StillResTable[i].iHeight;
		}
	}

	// rotate
	pCapability->iSupportedRotateCnt = _ARRAY_SIZE( _GC0308RotationTable );
	for ( i = 0; i < pCapability->iSupportedRotateCnt; i++ )
	{
		pCapability->eSupportedRotate[i] = _GC0308RotationTable[i];
	}

	pCapability->iSupportedShotModeCnt = _ARRAY_SIZE(_GC0308ShotModeTable);
	for ( i = 0; i < pCapability->iSupportedShotModeCnt; i++ )
	{
		pCapability->eSupportedShotMode[i] = _GC0308ShotModeTable[i];
	}

	return CAM_ERROR_NONE;
}

CAM_Error GC0308_GetShotModeCapability( CAM_ShotMode eShotMode, CAM_ShotModeCapability *pShotModeCap )
{
	CAM_Int32u i;
	// BAC check
	for ( i = 0; i < _ARRAY_SIZE( _GC0308ShotModeTable ); i++ )
	{
		if ( _GC0308ShotModeTable[i] == eShotMode )
		{
			break;
		}
	}

	if ( i >= _ARRAY_SIZE( _GC0308ShotModeTable ) || pShotModeCap ==NULL ) 
	{
		return CAM_ERROR_BADARGUMENT;
	}

	(void)(_GC0308ShotModeCap[eShotMode])( pShotModeCap );

	return CAM_ERROR_NONE;
}

CAM_Error GC0308_Init( void **ppDevice, void *pSensorEntry )
{
	CAM_Error             error      = CAM_ERROR_NONE;
	CAM_Int32s            iSkipFrame = 0;
	_V4L2SensorAttribute  _GC0308Attri;
	_V4L2SensorEntry      *pSensor = (_V4L2SensorEntry*)(pSensorEntry);
	GC0308State      *pState  = (GC0308State*)malloc( sizeof(GC0308State) );

	memset( &_GC0308Attri, 0, sizeof(_V4L2SensorAttribute) );

	*ppDevice = pState;
	if ( *ppDevice == NULL )
	{
		return CAM_ERROR_OUTOFMEMORY;
	}

	_GC0308Attri.stV4L2SensorEntry.iV4L2SensorID = pSensor->iV4L2SensorID;
	strcpy( _GC0308Attri.stV4L2SensorEntry.sDeviceName, pSensor->sDeviceName );

	/**************************************************************************************
     * defaultly, we will skip 30 frames in FAST VALIDATION PASS to avoid potential black
	 * frame if 3A is not convergence while resolution switch. If this is not need to your 
	 * sensor, just modify iSkipFrame, if you sensor need do save/restore 3A, pls refer to
	 * your sensor vendor, and implement these functions/
	***************************************************************************************/
#if defined( BUILD_OPTION_DEBUG_DISABLE_SAVE_RESTORE_3A )
	iSkipFrame                             = 30;
	_GC0308Attri.fnSaveAeAwb          = NULL;
	_GC0308Attri.fnRestoreAeAwb       = NULL;
	_GC0308Attri.pSaveRestoreUserData = NULL;
	_GC0308Attri.fnStartFlash         = NULL;
 	_GC0308Attri.fnStopFlash          = NULL;
#else
	iSkipFrame                             = 2;
	_GC0308Attri.fnSaveAeAwb          = _GC0308SaveAeAwb;
	_GC0308Attri.fnRestoreAeAwb       = _GC0308RestoreAeAwb;
	_GC0308Attri.pSaveRestoreUserData = (void*)pState;
 	_GC0308Attri.fnStartFlash         =  _GC0308StartFlash;
 	_GC0308Attri.fnStopFlash          =  _GC0308StopFlash;
#endif
	_GC0308Attri.fnApplyShotParam = _GC0308ApplyShotParam;

	error = V4L2_Init( &(pState->stV4L2), &(_GC0308Attri), iSkipFrame );

	/* here we can get default shot params */
	pState->stV4L2.stShotParam.eShotMode            = CAM_SHOTMODE_AUTO;
	pState->stV4L2.stShotParam.eExpMode             = CAM_EXPOSUREMODE_AUTO;
	pState->stV4L2.stShotParam.eExpMeterMode        = CAM_EXPOSUREMETERMODE_AUTO;
	pState->stV4L2.stShotParam.iEvCompQ16           = 0;
	pState->stV4L2.stShotParam.eIsoMode             = CAM_ISO_AUTO;
	pState->stV4L2.stShotParam.iShutterSpeedQ16     = -1;
	pState->stV4L2.stShotParam.iFNumQ16             = 1;
	pState->stV4L2.stShotParam.eBandFilterMode      = CAM_BANDFILTER_50HZ;
	pState->stV4L2.stShotParam.eWBMode              = CAM_WHITEBALANCEMODE_AUTO;
	pState->stV4L2.stShotParam.eFocusMode           = CAM_FOCUS_INFINITY;
	pState->stV4L2.stShotParam.iDigZoomQ16          = 0;
	pState->stV4L2.stShotParam.eColorEffect         = CAM_COLOREFFECT_OFF;
	pState->stV4L2.stShotParam.iSaturation          = 64;
	pState->stV4L2.stShotParam.iBrightness          = 0;
	pState->stV4L2.stShotParam.iContrast            = 0;
	pState->stV4L2.stShotParam.iSharpness           = 0;
	pState->stV4L2.stShotParam.eFlashMode           = CAM_FLASH_OFF;
	pState->stV4L2.stShotParam.bVideoStabilizer     = CAM_FALSE;
	pState->stV4L2.stShotParam.bVideoNoiseReducer   = CAM_FALSE;
	pState->stV4L2.stShotParam.bZeroShutterLag      = CAM_FALSE;
	pState->stV4L2.stShotParam.bHighDynamicRange	= CAM_FALSE;

	pState->stV4L2.stShotParam.iBurstCnt            = 1;
	/* get default JPEG params */
	pState->stV4L2.stJpegParam.iSampling      = -1; // 0 - 420, 1 - 422, 2 - 444
	pState->stV4L2.stJpegParam.iQualityFactor = 80;
	pState->stV4L2.stJpegParam.bEnableExif    = CAM_FALSE;
	pState->stV4L2.stJpegParam.bEnableThumb   = CAM_FALSE;
	pState->stV4L2.stJpegParam.iThumbWidth    = 0;
	pState->stV4L2.stJpegParam.iThumbHeight   = 0;


	return error;
}

CAM_Error GC0308_Deinit( void *pDevice )
{
	CAM_Error error = CAM_ERROR_NONE;
	GC0308State *pState = (GC0308State*)pDevice;

	error = V4L2_Deinit( &pState->stV4L2 );

	free( pDevice );

	return error;
}

CAM_Error _GC0308SetJpegParam( void *pDevice, CAM_JpegParam *pJpegParam );
CAM_Error GC0308_Config( void *pDevice, _CAM_SmartSensorConfig *pSensorConfig )
{
	CAM_Error        error   = CAM_ERROR_NONE;
	GC0308State *pState = (GC0308State*)pDevice;

	error = V4L2_Config( &pState->stV4L2, pSensorConfig );
	if ( error != CAM_ERROR_NONE )
	{
		return error;
	}

	if ( pSensorConfig->eState != CAM_CAPTURESTATE_IDLE )
	{
		if ( pSensorConfig->eFormat == CAM_IMGFMT_JPEG )
		{
			error = _GC0308SetJpegParam( pDevice, &(pSensorConfig->stJpegParam) );
			if ( error != CAM_ERROR_NONE )
			{
				return error;
			}
		}
	}

	pState->stV4L2.stConfig = *pSensorConfig;
	return CAM_ERROR_NONE;
}

CAM_Error GC0308_GetBufReq( void *pDevice, _CAM_SmartSensorConfig *pSensorConfig, CAM_ImageBufferReq *pBufReq )
{
	CAM_Error        error   = CAM_ERROR_NONE;
	GC0308State *pState = (GC0308State*)pDevice;

	error = V4L2_GetBufReq( &pState->stV4L2, pSensorConfig, pBufReq );

	return error;
}

CAM_Error GC0308_Enqueue( void *pDevice, CAM_ImageBuffer *pImgBuf )
{
	CAM_Error        error   = CAM_ERROR_NONE;
	GC0308State *pState = (GC0308State*)pDevice;

	error = V4L2_Enqueue( &pState->stV4L2, pImgBuf );

	return error;
}

CAM_Error GC0308_Dequeue(void *pDevice, CAM_ImageBuffer **ppImgBuf)
{
	CAM_Error        error   = CAM_ERROR_NONE;
	GC0308State *pState = (GC0308State*)pDevice;

	error = V4L2_Dequeue( &pState->stV4L2, ppImgBuf );

	if ( error == CAM_ERROR_NONE && (*ppImgBuf)->bEnableShotInfo )
	{
		error = _GC0308FillFrameShotInfo( pState, *ppImgBuf );
	}

	return error;
}

CAM_Error GC0308_Flush(void *pDevice)
{
	CAM_Error        error   = CAM_ERROR_NONE;
	GC0308State *pState = (GC0308State*)pDevice;

	error = V4L2_Flush( &pState->stV4L2 );

	return error;
}

static CAM_Int32s GC0308SetParamsReg( CAM_Int32s param,_CAM_ParameterEntry *optionArray,CAM_Int32s optionSize, CAM_Int32s sensorFD )
{
	CAM_Int32s i = 0;
	CAM_Int32s ret = 0;

	for ( i = 0; i < optionSize; i++ ) 
	{
		if ( param == optionArray[i].iParameter ) 
		{
			break;
		}
	}
	
	if ( i >= optionSize ) 
	{
		return -2;
	}

	if ( optionArray[i].stSetParam.stRegTable.pEntries[0].reg == 0x0000 )
	{
		// NOTE: in this case, no sensor registers need to be set
		ret = 0;
	}
	else
	{
		ret = _set_reg_array( sensorFD, optionArray[i].stSetParam.stRegTable.pEntries, optionArray[i].stSetParam.stRegTable.iLength );
	}

	return ret;
}

CAM_Error GC0308_SetShotParam( void *pDevice, _CAM_ShotParam *pShotParam )
{
	CAM_Error error = CAM_ERROR_NONE;
	CAM_Int32s ret = 0;
	GC0308State *pState = (GC0308State*)pDevice;
	_CAM_ShotParam sSetShotParam = *pShotParam;

	if (sSetShotParam.eShotMode != pState->stV4L2.stShotParam.eShotMode)
		pState->stV4L2.stShotParam.eShotMode = sSetShotParam.eShotMode;

	// Color Effect Setting
	LOGD("Set color effect to %d, old: %d", sSetShotParam.eColorEffect, pState->stV4L2.stShotParam.eColorEffect);
	if ( sSetShotParam.eColorEffect != pState->stV4L2.stShotParam.eColorEffect ) 
	{
		ret = GC0308SetParamsReg( sSetShotParam.eColorEffect, GC0308ColorEffectMode, _ARRAY_SIZE(GC0308ColorEffectMode), pState->stV4L2.iSensorFD );
		
		if ( ret == -2 ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.eColorEffect = sSetShotParam.eColorEffect;
	}

	// White Balance Setting
	LOGD("Set WB mode to %d, old: %d", sSetShotParam.eWBMode, pState->stV4L2.stShotParam.eWBMode);
	if ( sSetShotParam.eWBMode != pState->stV4L2.stShotParam.eWBMode ) 
	{
		ret = GC0308SetParamsReg( sSetShotParam.eWBMode, GC0308WBMode, _ARRAY_SIZE(GC0308WBMode), pState->stV4L2.iSensorFD );
		
		if ( ret == -2 ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.eWBMode = sSetShotParam.eWBMode;
	}
	
	return CAM_ERROR_NONE;
}

CAM_Error GC0308_GetShotParam( void *pDevice, _CAM_ShotParam *pShotParam )
{
	GC0308State *pState = (GC0308State*)pDevice;
    
	*pShotParam = pState->stV4L2.stShotParam;

	return CAM_ERROR_NONE;
}

CAM_Error GC0308_StartFocus( void *pDevice, CAM_AFMode eAFMode, void *pFocusParams )
{
	// TODO: add your start focus code here,an refrence is ov5642.c

	return CAM_ERROR_NONE;
}

CAM_Error GC0308_CancelFocus( void *pDevice )
{
	// TODO: add yourcancel focus code here,an refrence is ov5642.c
	return CAM_ERROR_NONE;
}

CAM_Error GC0308_CheckFocusState( void *pDevice, CAM_Bool *pFocusAutoStopped, _CAM_FocusState *pFocusState )
{

	// TODO: add your check focus status code here,an refrence is ov5642.c
	*pFocusAutoStopped = CAM_TRUE;
	*pFocusState       = CAM_IN_FOCUS;

	return CAM_ERROR_NONE;
}


CAM_Error GC0308_GetDigitalZoomCapability( CAM_Int32s iWidth, CAM_Int32s iHeight, CAM_Int32s *pSensorDigZoomQ16 )
{
	// TODO: add your get zoom capability code here,an refrence is ov5642.c
	*pSensorDigZoomQ16 = ( 1 << 16 );

	return CAM_ERROR_NONE;
}

_SmartSensorFunc func_gc0308 = 
{
	GC0308_GetCapability,
	GC0308_GetShotModeCapability,

	GC0308_SelfDetect,
	GC0308_Init,
	GC0308_Deinit,
	GC0308_Config,
	GC0308_GetBufReq,
	GC0308_Enqueue,
	GC0308_Dequeue,
	GC0308_Flush,
	GC0308_SetShotParam,
	GC0308_GetShotParam,

	GC0308_StartFocus,
	GC0308_CancelFocus,
	GC0308_CheckFocusState,
	GC0308_GetDigitalZoomCapability,

};

CAM_Error _GC0308SetJpegParam( void *pDevice, CAM_JpegParam *pJpegParam )
{
	GC0308State *pState = (GC0308State*)pDevice;

	if ( pJpegParam->bEnableExif )
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}

	if ( pJpegParam->bEnableThumb )
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}

	if ( pJpegParam->iQualityFactor != 80 )
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}
	else
	{
		pState->stV4L2.stJpegParam.iQualityFactor = 80;
	}

	return CAM_ERROR_NONE;
}

/*-------------------------------------------------------------------------------------------------------------------------------------
 * GC0308 shotmode capability
 * TODO: if you enable new shot mode, pls add a correspoding modcap function here, and add it to GC0308_shotModeCap _GC0308ShotModeCap array
 *------------------------------------------------------------------------------------------------------------------------------------*/
static void _GC0308AutoModeCap( CAM_ShotModeCapability *pShotModeCap )
{
	int i;

	// exposure mode
	pShotModeCap->iSupportedExpModeCnt  = 1;
	pShotModeCap->eSupportedExpMode[0]  = CAM_EXPOSUREMODE_AUTO;
	
	// exposure metering mode 
	pShotModeCap->iSupportedExpMeterCnt = 1;
	pShotModeCap->eSupportedExpMeter[0] = CAM_EXPOSUREMETERMODE_AUTO;

	// EV compensation 
	pShotModeCap->iEVCompStepQ16 = 0;
	pShotModeCap->iMinEVCompQ16 = 0;
	pShotModeCap->iMaxEVCompQ16 = 0;

	// ISO mode 
	pShotModeCap->iSupportedIsoModeCnt = 1;
	pShotModeCap->eSupportedIsoMode[0] = CAM_ISO_AUTO;

	// shutter speed
	pShotModeCap->iMinShutSpdQ16 = -1;
	pShotModeCap->iMaxShutSpdQ16 = -1;

	// F-number
	pShotModeCap->iMinFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);
	pShotModeCap->iMaxFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);

	// Band filter
	pShotModeCap->iSupportedBdFltModeCnt = 1; 
	pShotModeCap->eSupportedBdFltMode[0] = CAM_BANDFILTER_50HZ;

	// Flash mode
	pShotModeCap->iSupportedFlashModeCnt = 1;
	pShotModeCap->eSupportedFlashMode[0] = CAM_FLASH_OFF;

	// white balance mode
	pShotModeCap->iSupportedWBModeCnt = _ARRAY_SIZE(GC0308WBMode);
	for (i=0; i<pShotModeCap->iSupportedWBModeCnt; i++)
		pShotModeCap->eSupportedWBMode[i] = GC0308WBMode[i].iParameter;


	// focus mode
	pShotModeCap->iSupportedFocusModeCnt = 1;
	pShotModeCap->eSupportedFocusMode[0] = CAM_FOCUS_INFINITY;

	// AF mode 
	pShotModeCap->iSupportedAFModeCnt = 0;

	// optical zoom mode
	pShotModeCap->iMinOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);

	// digital zoom mode
	pShotModeCap->iMinDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->bSupportSmoothDigZoom = CAM_FALSE;

	// frame rate
	pShotModeCap->iMinFpsQ16     = 1 << 16;
	pShotModeCap->iMaxFpsQ16     = 40 << 16;

	// color effect
	pShotModeCap->iSupportedColorEffectCnt = _ARRAY_SIZE(GC0308ColorEffectMode);
	for (i=0; i<pShotModeCap->iSupportedColorEffectCnt; i++)
		pShotModeCap->eSupportedColorEffect[i] = GC0308ColorEffectMode[i].iParameter; 

	// brightness
	pShotModeCap->iMinBrightness = 0;
	pShotModeCap->iMaxBrightness = 0;

	// contrast
	pShotModeCap->iMinContrast = 0;
	pShotModeCap->iMaxContrast = 0;

	// saturation
	pShotModeCap->iMinSaturation = 0;
	pShotModeCap->iMaxSaturation = 0;

	// sharpness
	pShotModeCap->iMinSharpness = 0;
	pShotModeCap->iMaxSharpness = 0;

	// advanced features
	pShotModeCap->bSupportVideoStabilizer   = CAM_FALSE;
	pShotModeCap->bSupportVideoNoiseReducer	= CAM_FALSE;
	pShotModeCap->bSupportZeroShutterLag    = CAM_FALSE;
	pShotModeCap->bSupportBurstCapture      = CAM_FALSE;
	pShotModeCap->bSupportHighDynamicRange  = CAM_FALSE;
	pShotModeCap->iMaxBurstCnt              = 1;

	return;
}


/*-------------------------------------------------------------------------------------------------------------------------------------
 * GC0308 shotmode capability
 * TODO: if you enable new shot mode, pls add a correspoding modcap function here, and add it to GC0308_shotModeCap _GC0308ShotModeCap array
 *------------------------------------------------------------------------------------------------------------------------------------*/
static void _GC0308ManualModeCap( CAM_ShotModeCapability *pShotModeCap )
{
	int i;

	// exposure mode
	pShotModeCap->iSupportedExpModeCnt  = 1;
	pShotModeCap->eSupportedExpMode[0]  = CAM_EXPOSUREMODE_AUTO;
	
	// exposure metering mode 
	pShotModeCap->iSupportedExpMeterCnt = 1;
	pShotModeCap->eSupportedExpMeter[0] = CAM_EXPOSUREMETERMODE_AUTO;

	// EV compensation 
	pShotModeCap->iEVCompStepQ16 = 0;
	pShotModeCap->iMinEVCompQ16 = 0;
	pShotModeCap->iMaxEVCompQ16 = 0;

	// ISO mode 
	pShotModeCap->iSupportedIsoModeCnt = 1;
	pShotModeCap->eSupportedIsoMode[0] = CAM_ISO_AUTO;

	// shutter speed
	pShotModeCap->iMinShutSpdQ16 = -1;
	pShotModeCap->iMaxShutSpdQ16 = -1;

	// F-number
	pShotModeCap->iMinFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);
	pShotModeCap->iMaxFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);

	// Band filter
	pShotModeCap->iSupportedBdFltModeCnt = 1; 
	pShotModeCap->eSupportedBdFltMode[0] = CAM_BANDFILTER_50HZ;

	// Flash mode
	pShotModeCap->iSupportedFlashModeCnt = 1;
	pShotModeCap->eSupportedFlashMode[0] = CAM_FLASH_OFF;

	// white balance mode
	pShotModeCap->iSupportedWBModeCnt = _ARRAY_SIZE(GC0308WBMode);
	for (i=0; i<pShotModeCap->iSupportedWBModeCnt; i++)
		pShotModeCap->eSupportedWBMode[i] = GC0308WBMode[i].iParameter;

	// focus mode
	pShotModeCap->iSupportedFocusModeCnt = 1;
	pShotModeCap->eSupportedFocusMode[0] = CAM_FOCUS_INFINITY;

	// AF mode 
	pShotModeCap->iSupportedAFModeCnt = 0;

	// optical zoom mode
	pShotModeCap->iMinOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);

	// digital zoom mode
	pShotModeCap->iMinDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->bSupportSmoothDigZoom = CAM_FALSE;

	// frame rate
	pShotModeCap->iMinFpsQ16     = 1 << 16;
	pShotModeCap->iMaxFpsQ16     = 40 << 16;

	// color effect
	pShotModeCap->iSupportedColorEffectCnt = _ARRAY_SIZE(GC0308ColorEffectMode);
	for (i=0; i<pShotModeCap->iSupportedColorEffectCnt; i++)
		pShotModeCap->eSupportedColorEffect[i] = GC0308ColorEffectMode[i].iParameter; 

	// brightness
	pShotModeCap->iMinBrightness = 0;
	pShotModeCap->iMaxBrightness = 0;

	// contrast
	pShotModeCap->iMinContrast = 0;
	pShotModeCap->iMaxContrast = 0;

	// saturation
	pShotModeCap->iMinSaturation = 0;
	pShotModeCap->iMaxSaturation = 0;

	// sharpness
	pShotModeCap->iMinSharpness = 0;
	pShotModeCap->iMaxSharpness = 0;

	// advanced features
	pShotModeCap->bSupportVideoStabilizer   = CAM_FALSE;
	pShotModeCap->bSupportVideoNoiseReducer	= CAM_FALSE;
	pShotModeCap->bSupportZeroShutterLag    = CAM_FALSE;
	pShotModeCap->bSupportBurstCapture      = CAM_FALSE;
	pShotModeCap->bSupportHighDynamicRange  = CAM_FALSE;
	pShotModeCap->iMaxBurstCnt              = 1;

	return;
}

static CAM_Error _GC0308SaveAeAwb( const _CAM_SmartSensorConfig *pOldConfig, void *pUserData )
{
	// TODO: add your sensor specific save 3A function here
	return CAM_ERROR_NONE;
}

static CAM_Error _GC0308RestoreAeAwb( const _CAM_SmartSensorConfig *pNewConfig, void *pUserData )
{
	// TODO: add your sensor specific restore 3A function here
	return CAM_ERROR_NONE;  
}

static CAM_Error _GC0308StartFlash( void *pSensorState )
{
	// TODO: add your sensor specific start flash function here
	return CAM_ERROR_NONE;
}

static CAM_Error _GC0308StopFlash( void *pSensorState )
{
	// TODO: add your sensor specific stop flash function here
	return CAM_ERROR_NONE;
}

static CAM_Error _GC0308ApplyShotParam(void* pUserData)
{
	GC0308State *pState = (GC0308State*) pUserData;

	LOGD("Apply shot params, WB mode: %d", pState->stV4L2.stShotParam.eWBMode);
	GC0308SetParamsReg(pState->stV4L2.stShotParam.eWBMode, GC0308WBMode, _ARRAY_SIZE(GC0308WBMode), pState->stV4L2.iSensorFD);

	LOGD("Apply shot params, color effect: %d", pState->stV4L2.stShotParam.eColorEffect);
	GC0308SetParamsReg(pState->stV4L2.stShotParam.eColorEffect, GC0308ColorEffectMode, _ARRAY_SIZE(GC0308ColorEffectMode), pState->stV4L2.iSensorFD);

	return CAM_ERROR_NONE;
}

// get shot info
static CAM_Error _GC0308FillFrameShotInfo( GC0308State *pState, CAM_ImageBuffer *pImgBuf )
{
	// TODO: add real value of these parameters here

	CAM_Error    error      = CAM_ERROR_NONE;
	CAM_ShotInfo *pShotInfo = &(pImgBuf->stShotInfo);

	pShotInfo->iExposureTimeQ16    = (1 << 16) / 30;
	pShotInfo->iFNumQ16            = (int)( 2.8 * 65536 + 0.5 );
	pShotInfo->eExposureMode       = CAM_EXPOSUREMODE_AUTO;
	pShotInfo->eExpMeterMode       = CAM_EXPOSUREMETERMODE_MEAN;
	pShotInfo->iISOSpeed           = 100;
	pShotInfo->iSubjectDistQ16     = 0;
	pShotInfo->iFlashStatus        = 0x0010;
	pShotInfo->iFocalLenQ16        = (int)( 3.79 * 65536 + 0.5 );
	pShotInfo->iFocalLen35mm       = 0;       

	return error;
}
