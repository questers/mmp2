LOCAL_PATH:= $(call my-dir)


#
## libcameraengine
#
#

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := debug eng

LOCAL_CFLAGS+=$(CAMERA_ENGINE_CFLAGS)
LOCAL_SHARED_LIBRARIES+=$(LOCAL_SHARED_LIBRARIES)

#LOCAL_WHOLE_STATIC_LIBRARIES :=
LOCAL_SHARED_LIBRARIES += libphycontmem
# libGAL libgcu

LOCAL_SRC_FILES := \
     extisp_cameraengine/cam_extisp_eng.c \
     extisp_cameraengine/cam_extisp_ppu.c \
     general/cam_gen.c \
     general/cam_utility.c \
     general/cam_trace.c \

LOCAL_CFLAGS += \
    -I device/generic/components/libcamera/cameraengine2/include \
    -I device/generic/components/libcamera/cameraengine2/src/cameraengine2/ansi_c/extisp_cameraengine \
    -I device/generic/components/libcamera/cameraengine2/src/cameraengine2/ansi_c/_include \
    -D LINUX \
    -D ANDROID \
    -D CAM_LOG_ERROR
    
LOCAL_SHARED_LIBRARIES +=libmiscgen \
                         libutils
    
#    -mabi=aapcs-linux
#
#    # if you have any extra directories, put them here.

LOCAL_C_INCLUDES := device/generic/components/libphycontmem/phycontmem \
                    device/generic/components/graphics \
                    device/generic/components/libipp/include
                    

LOCAL_PRELINK_MODULE := false

LOCAL_MODULE := libcameraengine

include $(BUILD_STATIC_LIBRARY)
#include $(BUILD_SHARED_LIBRARY)

