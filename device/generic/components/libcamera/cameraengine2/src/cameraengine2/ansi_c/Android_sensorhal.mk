#LOCAL_PATH:= $(call my-dir)


#
## libsensorhal_extisp
#
#

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := debug eng

LOCAL_CFLAGS+=$(CAMERA_ENGINE_CFLAGS)
LOCAL_SHARED_LIBRARIES+=$(LOCAL_SHARED_LIBRARIES)

#LOCAL_WHOLE_STATIC_LIBRARIES :=
#LOCAL_SHARED_LIBRARIES += libphycontmem libGAL libgcu

LOCAL_SRC_FILES := \
     extisp_sensorhal/cam_extisp_v4l2base.c \
     extisp_sensorhal/cam_extisp_sensorhal.c \
     extisp_sensorhal/hm2055/cam_extisp_hm2055.c \
     extisp_sensorhal/hm2057/cam_extisp_hm2057.c \
     extisp_sensorhal/gc2015/cam_extisp_gc2015.c \
     extisp_sensorhal/gc0308/cam_extisp_gc0308.c \
     extisp_sensorhal/ov3640/cam_extisp_ov3640.c \
     extisp_sensorhal/ov5642/cam_extisp_ov5642.c \
     extisp_sensorhal/ov5640/cam_extisp_ov5640.c \
     extisp_sensorhal/gt2005/cam_extisp_gt2005.c \
     extisp_sensorhal/basicsensor/cam_extisp_basicsensor.c \
     extisp_sensorhal/fakesensor/cam_extisp_fakesensor.c \


LOCAL_CFLAGS += \
    -I device/generic/components/libcamera/cameraengine2/include \
    -I device/generic/components/libipp/include \
    -I device/generic/components/libcamera/cameraengine2/src/cameraengine2/ansi_c/_include \
    -I device/generic/components/libcamera/cameraengine2/src/cameraengine2/ansi_c/extisp_sensorhal \
    -D LINUX\
    -D ANDROID\
    -D CAM_LOG_ERROR
LOCAL_SHARED_LIBRARIES += libutils
    
#    -mabi=aapcs-linux
#
#    # if you have any extra directories, put them here.

LOCAL_C_INCLUDES := device/generic/components/libphycontmem/phycontmem

LOCAL_PRELINK_MODULE := false

LOCAL_MODULE := libsensorhal_extisp

include $(BUILD_STATIC_LIBRARY)
#include $(BUILD_SHARED_LIBRARY)
