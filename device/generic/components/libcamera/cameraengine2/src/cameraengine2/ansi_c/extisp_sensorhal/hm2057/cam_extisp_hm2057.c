/*******************************************************************************
//(C) Copyright [2010 - 2011] Marvell International Ltd.
//All Rights Reserved
*******************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "cam_log.h"
#include "cam_utility.h"


#include "cam_extisp_sensorwrapper.h"
#include "cam_extisp_v4l2base.h"
#include "cam_extisp_hm2057.h"

#define REG(a,b) {a, 0, 15, 0, b}

// NOTE: if you want to enable static resolution table to bypass sensor dynamically detect to save camera-off to viwerfinder-on latency, 
//       you can fill the following four tables according to your sensor's capability. And open macro 
//       BUILD_OPTION_STARTEGY_DISABLE_DYNAMIC_SENSOR_DETECT in cam_extisp_buildopt.h

/* Basic Sensor video/preview resolution table */
CAM_Size _HM2057VideoResTable[CAM_MAX_SUPPORT_IMAGE_SIZE_CNT] = 
{
	{0, 0},
};

/* Basic Sensor still resolution table */
CAM_Size _HM2057StillResTable[CAM_MAX_SUPPORT_IMAGE_SIZE_CNT] = 
{ 
	{0, 0},
};

/* Basic Sensor video/preview format table */
CAM_ImageFormat _HM2057VideoFormatTable[CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT] = 
{ 
	0, 
};

/* Basic Sensor still format table */
CAM_ImageFormat _HM2057StillFormatTable[CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT] = 
{
	0,
};


CAM_FlipRotate _HM2057RotationTable[] = 
{
	CAM_FLIPROTATE_NORMAL,
};


CAM_ShotMode _HM2057ShotModeTable[] = 
{
	CAM_SHOTMODE_AUTO,
	//CAM_SHOTMODE_MANUAL,
};

_CAM_RegEntry _HM2057ExpMeter_Mean[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _HM2057ExpMeter[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_EXPOSUREMETERMODE_MEAN, _HM2057ExpMeter_Mean),
};

_CAM_RegEntry _HM2057IsoMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _HM2057IsoMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_ISO_AUTO, _HM2057IsoMode_Auto),
};

_CAM_RegEntry _HM2057BdFltMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _HM2057BdFltMode_Off[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _HM2057BdFltMode_50Hz[] = 
{
};

_CAM_RegEntry _HM2057BdFltMode_60Hz[] = 
{
};

_CAM_ParameterEntry _HM2057BdFltMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_BANDFILTER_50HZ, _HM2057BdFltMode_50Hz),
	PARAM_ENTRY_USE_REGTABLE(CAM_BANDFILTER_60HZ, _HM2057BdFltMode_60Hz),
};

_CAM_RegEntry _HM2057FlashMode_Off[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _HM2057FlashMode_On[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _HM2057FlashMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _HM2057FlashMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_FLASH_OFF,	_HM2057FlashMode_Off),
};

_CAM_RegEntry HM2057WBMode_Auto[] =
{
	REG(0x0380, 0xff),
};

_CAM_RegEntry HM2057WBMode_CLOUDY[] =
{
	REG(0x0380, 0xFD),  	//Disable AWB
	REG(0x032D, 0x70),
	REG(0x032E, 0x01), 	//Red
	REG(0x032F, 0x00),
	REG(0x0330, 0x01),		//Green
	REG(0x0331, 0x08),
	REG(0x0332, 0x01),		//Blue
	REG(0x0101, 0xFF),                        
};

_CAM_RegEntry HM2057WBMode_DAYLIGHT[] =
{
	REG(0x0380, 0xFD),  	//Disable AWB
	REG(0x032D, 0x60),
	REG(0x032E, 0x01), 	//Red
	REG(0x032F, 0x00),
	REG(0x0330, 0x01),		//Green
	REG(0x0331, 0x20),
	REG(0x0332, 0x01),		//Blue
	REG(0x0101, 0xFF),  
};

_CAM_RegEntry HM2057WBMode_INCANDESCENT[] =
{
	REG(0x0380, 0xFD),  	//Disable AWB
	REG(0x032D, 0x50),
	REG(0x032E, 0x01), 	//Red
	REG(0x032F, 0x00),
	REG(0x0330, 0x01),		//Green
	REG(0x0331, 0x30),
	REG(0x0332, 0x01),		//Blue
	REG(0x0101, 0xFF),  
};

_CAM_RegEntry HM2057WBMode_FLUORESCENT1[] =
{
	REG(0x0380, 0xFD),  	//Disable AWB
	REG(0x032D, 0x80),
	REG(0x032E, 0x01), 	//Red
	REG(0x032F, 0x00),
	REG(0x0330, 0x01),		//Green
	REG(0x0331, 0x00),
	REG(0x0332, 0x01),		//Blue
	REG(0x0101, 0xFF), 
};

_CAM_ParameterEntry HM2057WBMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_AUTO, HM2057WBMode_Auto),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_CLOUDY, HM2057WBMode_CLOUDY),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_DAYLIGHT, HM2057WBMode_DAYLIGHT),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_INCANDESCENT, HM2057WBMode_INCANDESCENT),
};

_CAM_RegEntry _HM2057FocusMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _HM2057FocusMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_FOCUS_AUTO_ONESHOT,  _HM2057FocusMode_Auto),
};

_CAM_RegEntry HM2057ColorEffectMode_OFF[] = 
{
	REG(0x0488, 0x10), //[0]:Image scense, [1]:Image Xor
	REG(0x0486, 0x00), //Hue, sin                       
	REG(0x0487, 0xFF), //Hue, cos                       
	REG(0x0101, 0xFF), //AWB CMU                                   
};

_CAM_RegEntry HM2057ColorEffectMode_SEPIA[] = 
{
	REG(0x0488, 0x11), //[0]:Image scense, [1]:Image Xor
	REG(0x0486, 0x40), //Hue, sin                       
	REG(0x0487, 0x90), //Hue, cos                       
	REG(0x0101, 0xFF), //AWB CMU
};

_CAM_RegEntry HM2057ColorEffectMode_GRAYSCALE[] = 
{
	REG(0x0488, 0x11), //[0]:Image scense, [1]:Image Xor
	REG(0x0486, 0x80), //Hue, sin                       
	REG(0x0487, 0x80), //Hue, cos                       
	REG(0x0101, 0xFF), //AWB CMU
};

_CAM_ParameterEntry HM2057ColorEffectMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_OFF, HM2057ColorEffectMode_OFF),
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_SEPIA, HM2057ColorEffectMode_SEPIA),
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_GRAYSCALE, HM2057ColorEffectMode_GRAYSCALE),
};

// EV compensation
_CAM_RegEntry HM2057EvComp_N12[] = 
{
	REG(0x04C0,0xC0),
	REG(0x038E,0x30),
	REG(0x0381,0x38),
	REG(0x0382,0x28),
	REG(0x0100,0xFF),                         
};

_CAM_RegEntry HM2057EvComp_N09[] = 
{
	REG(0x04C0,0xB0),
	REG(0x038E,0x30),
	REG(0x0381,0x38),
	REG(0x0382,0x28),
	REG(0x0100,0xFF),             
};

_CAM_RegEntry HM2057EvComp_N06[] = 
{
 	REG(0x04C0,0xA0),
	REG(0x038E,0x38),
	REG(0x0381,0x40),
	REG(0x0382,0x30),                        
	REG(0x0100,0xFF),             
};

_CAM_RegEntry HM2057EvComp_N03[] = 
{
 	REG(0x04C0,0x90),
	REG(0x038E,0x40),
	REG(0x0381,0x48),
	REG(0x0382,0x38),
	REG(0x0100,0xFF),             
};

_CAM_RegEntry HM2057EvComp_000[] = 
{
 	REG(0x04C0,0x80),
	REG(0x038E,0x44),
	REG(0x0381,0x4C),
	REG(0x0382,0x3C),
	REG(0x0100,0xFF),
};

_CAM_RegEntry HM2057EvComp_P03[] = 
{
 	REG(0x04C0,0x10),	
	REG(0x038E,0x50),
	REG(0x0381,0x58),
	REG(0x0382,0x48),                      
	REG(0x0100,0xFF),           
};

_CAM_RegEntry HM2057EvComp_P06[] = 
{
 	REG(0x04C0,0x20),	
	REG(0x038E,0x58),
	REG(0x0381,0x60),
	REG(0x0382,0x50),                       
	REG(0x0100,0xFF),             
};

_CAM_RegEntry HM2057EvComp_P09[] = 
{
 	REG(0x04C0,0x30),	
	REG(0x038E,0x60),
	REG(0x0381,0x68),
	REG(0x0382,0x58),                       
	REG(0x0100,0xFF),             
};

_CAM_RegEntry HM2057EvComp_P12[] = 
{
    REG(0x04C0,0x40),
	REG(0x038E,0x60),
	REG(0x0381,0x68),
	REG(0x0382,0x58),                       
	REG(0x0100,0xFF),             
};

_CAM_ParameterEntry HM2057EvCompMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(-12,	HM2057EvComp_N12),
	PARAM_ENTRY_USE_REGTABLE(-9,	HM2057EvComp_N09),
	PARAM_ENTRY_USE_REGTABLE(-6,	HM2057EvComp_N06),
	PARAM_ENTRY_USE_REGTABLE(-3,	HM2057EvComp_N03),
	PARAM_ENTRY_USE_REGTABLE(0,		HM2057EvComp_000),
	PARAM_ENTRY_USE_REGTABLE(3,		HM2057EvComp_P03),
	PARAM_ENTRY_USE_REGTABLE(6,		HM2057EvComp_P06),
	PARAM_ENTRY_USE_REGTABLE(9,		HM2057EvComp_P09),
	PARAM_ENTRY_USE_REGTABLE(12,	HM2057EvComp_P12),
};

_CAM_RegEntry HM2057Brightness_P2[] = 
{
    REG(0x04C0,0x40),
};

_CAM_RegEntry HM2057Brightness_P1[] = 
{
    REG(0x04C0,0x60),
};

_CAM_RegEntry HM2057Brightness_00[] = 
{
    REG(0x04C0,0x80),
};

_CAM_RegEntry HM2057Brightness_N1[] = 
{
    REG(0x04C0,0xa0),
};

_CAM_RegEntry HM2057Brightness_N2[] = 
{
    REG(0x04C0,0xc0),
};

_CAM_ParameterEntry HM2057Brightness[] = 
{
	PARAM_ENTRY_USE_REGTABLE(2, HM2057Brightness_P2),
	PARAM_ENTRY_USE_REGTABLE(1, HM2057Brightness_P1),
	PARAM_ENTRY_USE_REGTABLE(0, HM2057Brightness_00),
	PARAM_ENTRY_USE_REGTABLE(-1, HM2057Brightness_N1),
	PARAM_ENTRY_USE_REGTABLE(-2, HM2057Brightness_N2),
};

_CAM_RegEntry HM2057Contrast_P2[] = 
{
    REG(0x04b0,0xa0),
};

_CAM_RegEntry HM2057Contrast_P1[] = 
{
    REG(0x04b0,0x80),
};

_CAM_RegEntry HM2057Contrast_00[] = 
{
    REG(0x04b0,0x60),
};

_CAM_RegEntry HM2057Contrast_N1[] = 
{
    REG(0x04b0,0x40),
};

_CAM_RegEntry HM2057Contrast_N2[] = 
{
    REG(0x04b0,0x20),
};

_CAM_ParameterEntry HM2057Contrast[] = 
{
	PARAM_ENTRY_USE_REGTABLE(2, HM2057Contrast_P2),
	PARAM_ENTRY_USE_REGTABLE(1, HM2057Contrast_P1),
	PARAM_ENTRY_USE_REGTABLE(0, HM2057Contrast_00),
	PARAM_ENTRY_USE_REGTABLE(-1, HM2057Contrast_N1),
	PARAM_ENTRY_USE_REGTABLE(-2, HM2057Contrast_N2),
};

static CAM_Error _HM2057SaveAeAwb( const _CAM_SmartSensorConfig*, void* );
static CAM_Error _HM2057RestoreAeAwb( const _CAM_SmartSensorConfig*, void* );
static CAM_Error _HM2057StartFlash( void* );
static CAM_Error _HM2057StopFlash( void* );
static CAM_Error _HM2057ApplyShotParam(void*);
static CAM_Error _HM2057FillFrameShotInfo( HM2057State*, CAM_ImageBuffer* );

// shot mode capability
static void _HM2057AutoModeCap( CAM_ShotModeCapability* );
static void _HM2057ManualModeCap( CAM_ShotModeCapability*);
// static void _HM2057NightModeCap( CAM_ShotModeCapability*);
/* shot mode cap function table */
HM2057_ShotModeCap _HM2057ShotModeCap[CAM_SHOTMODE_NUM] = { 
	_HM2057AutoModeCap     , // CAM_SHOTMODE_AUTO = 0,
	_HM2057ManualModeCap   , // CAM_SHOTMODE_MANUAL,
	NULL                   , // CAM_SHOTMODE_PORTRAIT,
	NULL                   , // CAM_SHOTMODE_LANDSCAPE,
	NULL                   , // CAM_SHOTMODE_NIGHTPORTRAIT,
	NULL                   , // CAM_SHOTMODE_NIGHTSCENE,
	NULL                   , // CAM_SHOTMODE_CHILD,
	NULL                   , // CAM_SHOTMODE_INDOOR,
	NULL                   , // CAM_SHOTMODE_PLANTS,
	NULL                   , // CAM_SHOTMODE_SNOW,
	NULL                   , // CAM_SHOTMODE_BEACH,
	NULL                   , // CAM_SHOTMODE_FIREWORKS,
	NULL                   , // CAM_SHOTMODE_SUBMARINE, 
};  

extern _SmartSensorFunc func_hm2057; 
CAM_Error HM2057_SelfDetect( _SmartSensorAttri *pSensorInfo )
{
	CAM_Error error = CAM_ERROR_NONE;
	
	// NOTE:  If you open macro BUILD_OPTION_STARTEGY_DISABLE_DYNAMIC_SENSOR_DETECT in cam_extisp_buildopt.h 
	//        to bypass sensor dynamically detect to save camera-off to viwerfinder-on latency, you should initilize
	//        _HM2057VideoResTable/_HM2057StillResTable/_HM2057VideoFormatTable/_HM2057StillFormatTable manually.

#if !defined( BUILD_OPTION_STARTEGY_DISABLE_DYNAMIC_SENSOR_DETECT )	
	error = V4L2_SelfDetect( pSensorInfo, "hm2057", &func_hm2057,
	                         _HM2057VideoResTable, _HM2057StillResTable,
	                         _HM2057VideoFormatTable, _HM2057StillFormatTable );

#else
	{
		_V4L2SensorEntry *pSensorEntry = (_V4L2SensorEntry*)( pSensorInfo->cReserved );
		strcpy( pSensorInfo->sSensorName, "HM2057-unknown" );
		pSensorInfo->pFunc = &func_hm2057;

		// FIXME: the following is just an example in Marvell platform, you should change it according to your driver implementation
		strcpy( pSensorEntry->sDeviceName, "/dev/video0" );
		pSensorEntry->iV4L2SensorID = 0;
	}
#endif

	return error;
} 


CAM_Error HM2057_GetCapability( _CAM_SmartSensorCapability *pCapability )
{
	CAM_Int32s i = 0;


	// preview/video supporting 
	// format
	pCapability->iSupportedVideoFormatCnt = 0;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT; i++ ) 
	{
		if ( _HM2057VideoFormatTable[i] == 0 )
		{
			break;
		}
		pCapability->eSupportedVideoFormat[pCapability->iSupportedVideoFormatCnt] = _HM2057VideoFormatTable[i];
		pCapability->iSupportedVideoFormatCnt++;
	}

	pCapability->bArbitraryVideoSize     = CAM_FALSE;
	pCapability->iSupportedVideoSizeCnt  = 0;
	pCapability->stMinVideoSize.iWidth   = _HM2057VideoResTable[0].iWidth;
	pCapability->stMinVideoSize.iHeight  = _HM2057VideoResTable[0].iHeight;
	pCapability->stMaxVideoSize.iWidth   = _HM2057VideoResTable[0].iWidth;
	pCapability->stMaxVideoSize.iHeight  = _HM2057VideoResTable[0].iHeight;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_SIZE_CNT; i++ )
	{
		if ( _HM2057VideoResTable[i].iWidth == 0 || _HM2057VideoResTable[i].iHeight == 0)
		{
			break;
		}
		pCapability->stSupportedVideoSize[pCapability->iSupportedVideoSizeCnt] = _HM2057VideoResTable[i];
		pCapability->iSupportedVideoSizeCnt++;

		if ( ( pCapability->stMinVideoSize.iWidth > _HM2057VideoResTable[i].iWidth ) || 
		     ( ( pCapability->stMinVideoSize.iWidth == _HM2057VideoResTable[i].iWidth ) && ( pCapability->stMinVideoSize.iHeight > _HM2057VideoResTable[i].iHeight ) ) ) 
		{
			pCapability->stMinVideoSize.iWidth = _HM2057VideoResTable[i].iWidth;
			pCapability->stMinVideoSize.iHeight = _HM2057VideoResTable[i].iHeight;
		}
		if ( ( pCapability->stMaxVideoSize.iWidth < _HM2057VideoResTable[i].iWidth) ||
		     ( ( pCapability->stMaxVideoSize.iWidth == _HM2057VideoResTable[i].iWidth ) && ( pCapability->stMaxVideoSize.iHeight < _HM2057VideoResTable[i].iHeight ) ) )
		{
			pCapability->stMaxVideoSize.iWidth = _HM2057VideoResTable[i].iWidth;
			pCapability->stMaxVideoSize.iHeight = _HM2057VideoResTable[i].iHeight;
		}
	}

	// still capture supporting
	// format
	pCapability->iSupportedStillFormatCnt           = 0;
	pCapability->stSupportedJPEGParams.bSupportJpeg = CAM_FALSE;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT; i++ ) 
	{
		if ( _HM2057StillFormatTable[i] == CAM_IMGFMT_JPEG )
		{
			// JPEG encoder functionalities
			pCapability->stSupportedJPEGParams.bSupportJpeg = CAM_TRUE;
			pCapability->stSupportedJPEGParams.bSupportExif = CAM_FALSE;
			pCapability->stSupportedJPEGParams.bSupportThumb = CAM_FALSE;
			pCapability->stSupportedJPEGParams.iMinQualityFactor = 80;
			pCapability->stSupportedJPEGParams.iMaxQualityFactor = 80;
		}
		if ( _HM2057StillFormatTable[i] == 0 )
		{
			break;
		}
		pCapability->eSupportedStillFormat[pCapability->iSupportedStillFormatCnt] = _HM2057StillFormatTable[i];
		pCapability->iSupportedStillFormatCnt++;
	}
	// resolution
	pCapability->bArbitraryStillSize    = CAM_FALSE;
	pCapability->iSupportedStillSizeCnt = 0;
	pCapability->stMinStillSize.iWidth  = _HM2057StillResTable[0].iWidth;
	pCapability->stMinStillSize.iHeight = _HM2057StillResTable[0].iHeight;
	pCapability->stMaxStillSize.iWidth  = _HM2057StillResTable[0].iWidth;
	pCapability->stMaxStillSize.iHeight = _HM2057StillResTable[0].iHeight;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_SIZE_CNT; i++ )
	{
		if ( _HM2057StillResTable[i].iWidth == 0 || _HM2057StillResTable[i].iHeight == 0 )
		{
			break;
		}

		pCapability->stSupportedStillSize[pCapability->iSupportedStillSizeCnt] = _HM2057StillResTable[i];
		pCapability->iSupportedStillSizeCnt++;

		if ( ( pCapability->stMinStillSize.iWidth > _HM2057StillResTable[i].iWidth ) ||
		     ( ( pCapability->stMinStillSize.iWidth == _HM2057StillResTable[i].iWidth ) && ( pCapability->stMinStillSize.iHeight > _HM2057StillResTable[i].iHeight ) ) )
		{
			pCapability->stMinStillSize.iWidth  = _HM2057StillResTable[i].iWidth;
			pCapability->stMinStillSize.iHeight = _HM2057StillResTable[i].iHeight;
		}
		if ( ( pCapability->stMaxStillSize.iWidth < _HM2057StillResTable[i].iWidth ) || 
		     ( ( pCapability->stMaxStillSize.iWidth == _HM2057StillResTable[i].iWidth ) && ( pCapability->stMaxStillSize.iHeight < _HM2057StillResTable[i].iHeight ) ) )
		{
			pCapability->stMaxStillSize.iWidth = _HM2057StillResTable[i].iWidth;
			pCapability->stMaxStillSize.iHeight = _HM2057StillResTable[i].iHeight;
		}
	}

	// rotate
	pCapability->iSupportedRotateCnt = _ARRAY_SIZE( _HM2057RotationTable );
	for ( i = 0; i < pCapability->iSupportedRotateCnt; i++ )
	{
		pCapability->eSupportedRotate[i] = _HM2057RotationTable[i];
	}

	pCapability->iSupportedShotModeCnt = _ARRAY_SIZE(_HM2057ShotModeTable);
	for ( i = 0; i < pCapability->iSupportedShotModeCnt; i++ )
	{
		pCapability->eSupportedShotMode[i] = _HM2057ShotModeTable[i];
	}

	return CAM_ERROR_NONE;
}

CAM_Error HM2057_GetShotModeCapability( CAM_ShotMode eShotMode, CAM_ShotModeCapability *pShotModeCap )
{
	CAM_Int32u i;
	// BAC check
	for ( i = 0; i < _ARRAY_SIZE( _HM2057ShotModeTable ); i++ )
	{
		if ( _HM2057ShotModeTable[i] == eShotMode )
		{
			break;
		}
	}

	if ( i >= _ARRAY_SIZE( _HM2057ShotModeTable ) || pShotModeCap ==NULL ) 
	{
		return CAM_ERROR_BADARGUMENT;
	}

	(void)(_HM2057ShotModeCap[eShotMode])( pShotModeCap );

	return CAM_ERROR_NONE;
}

CAM_Error HM2057_Init( void **ppDevice, void *pSensorEntry )
{
	CAM_Error             error      = CAM_ERROR_NONE;
	CAM_Int32s            iSkipFrame = 0;
	_V4L2SensorAttribute  _HM2057Attri;
	_V4L2SensorEntry      *pSensor = (_V4L2SensorEntry*)(pSensorEntry);
	HM2057State      *pState  = (HM2057State*)malloc( sizeof(HM2057State) );

	memset( &_HM2057Attri, 0, sizeof(_V4L2SensorAttribute) );

	*ppDevice = pState;
	if ( *ppDevice == NULL )
	{
		return CAM_ERROR_OUTOFMEMORY;
	}

	_HM2057Attri.stV4L2SensorEntry.iV4L2SensorID = pSensor->iV4L2SensorID;
	strcpy( _HM2057Attri.stV4L2SensorEntry.sDeviceName, pSensor->sDeviceName );

	/**************************************************************************************
     * defaultly, we will skip 30 frames in FAST VALIDATION PASS to avoid potential black
	 * frame if 3A is not convergence while resolution switch. If this is not need to your 
	 * sensor, just modify iSkipFrame, if you sensor need do save/restore 3A, pls refer to
	 * your sensor vendor, and implement these functions/
	***************************************************************************************/
#if defined( BUILD_OPTION_DEBUG_DISABLE_SAVE_RESTORE_3A )
	iSkipFrame                             = 2;
	_HM2057Attri.fnSaveAeAwb          = NULL;
	_HM2057Attri.fnRestoreAeAwb       = NULL;
	_HM2057Attri.fnStartFlash         = NULL;
 	_HM2057Attri.fnStopFlash          = NULL;
#else
	iSkipFrame                             = 2;
	_HM2057Attri.fnSaveAeAwb          = _HM2057SaveAeAwb;
	_HM2057Attri.fnRestoreAeAwb       = _HM2057RestoreAeAwb;
 	_HM2057Attri.fnStartFlash         =  _HM2057StartFlash;
 	_HM2057Attri.fnStopFlash          =  _HM2057StopFlash;
#endif

	_HM2057Attri.pSaveRestoreUserData = (void*)pState;
	_HM2057Attri.fnApplyShotParam = _HM2057ApplyShotParam;

	error = V4L2_Init( &(pState->stV4L2), &(_HM2057Attri), iSkipFrame );

	/* here we can get default shot params */
	pState->stV4L2.stShotParam.eShotMode            = CAM_SHOTMODE_AUTO;
	pState->stV4L2.stShotParam.eExpMode             = CAM_EXPOSUREMODE_AUTO;
	pState->stV4L2.stShotParam.eExpMeterMode        = CAM_EXPOSUREMETERMODE_AUTO;
	pState->stV4L2.stShotParam.iEvCompQ16           = 0;
	pState->stV4L2.stShotParam.eIsoMode             = CAM_ISO_AUTO;
	pState->stV4L2.stShotParam.iShutterSpeedQ16     = -1;
	pState->stV4L2.stShotParam.iFNumQ16             = 1;
	pState->stV4L2.stShotParam.eBandFilterMode      = CAM_BANDFILTER_50HZ;
	pState->stV4L2.stShotParam.eWBMode              = CAM_WHITEBALANCEMODE_AUTO;
	pState->stV4L2.stShotParam.eFocusMode           = CAM_FOCUS_AUTO_ONESHOT;
	pState->stV4L2.stShotParam.iDigZoomQ16          = 0;
	pState->stV4L2.stShotParam.eColorEffect         = CAM_COLOREFFECT_OFF;
	pState->stV4L2.stShotParam.iSaturation          = 64;
	pState->stV4L2.stShotParam.iBrightness          = 0;
	pState->stV4L2.stShotParam.iContrast            = 0;
	pState->stV4L2.stShotParam.iSharpness           = 0;
	pState->stV4L2.stShotParam.eFlashMode           = CAM_FLASH_OFF;
	pState->stV4L2.stShotParam.bVideoStabilizer     = CAM_FALSE;
	pState->stV4L2.stShotParam.bVideoNoiseReducer   = CAM_FALSE;
	pState->stV4L2.stShotParam.bZeroShutterLag      = CAM_FALSE;
	pState->stV4L2.stShotParam.bHighDynamicRange	= CAM_FALSE;

	pState->stV4L2.stShotParam.iBurstCnt            = 1;
	/* get default JPEG params */
	pState->stV4L2.stJpegParam.iSampling      = -1; // 0 - 420, 1 - 422, 2 - 444
	pState->stV4L2.stJpegParam.iQualityFactor = 80;
	pState->stV4L2.stJpegParam.bEnableExif    = CAM_FALSE;
	pState->stV4L2.stJpegParam.bEnableThumb   = CAM_FALSE;
	pState->stV4L2.stJpegParam.iThumbWidth    = 0;
	pState->stV4L2.stJpegParam.iThumbHeight   = 0;


	return error;
}

CAM_Error HM2057_Deinit( void *pDevice )
{
	CAM_Error error = CAM_ERROR_NONE;
	HM2057State *pState = (HM2057State*)pDevice;

	error = V4L2_Deinit( &pState->stV4L2 );

	free( pDevice );

	return error;
}

CAM_Error _HM2057SetJpegParam( void *pDevice, CAM_JpegParam *pJpegParam );
CAM_Error HM2057_Config( void *pDevice, _CAM_SmartSensorConfig *pSensorConfig )
{
	CAM_Error        error   = CAM_ERROR_NONE;
	HM2057State *pState = (HM2057State*)pDevice;

	error = V4L2_Config( &pState->stV4L2, pSensorConfig );
	if ( error != CAM_ERROR_NONE )
	{
		return error;
	}

	if ( pSensorConfig->eState != CAM_CAPTURESTATE_IDLE )
	{
		if ( pSensorConfig->eFormat == CAM_IMGFMT_JPEG )
		{
			error = _HM2057SetJpegParam( pDevice, &(pSensorConfig->stJpegParam) );
			if ( error != CAM_ERROR_NONE )
			{
				return error;
			}
		}
	}

	pState->stV4L2.stConfig = *pSensorConfig;
	return CAM_ERROR_NONE;
}

CAM_Error HM2057_GetBufReq( void *pDevice, _CAM_SmartSensorConfig *pSensorConfig, CAM_ImageBufferReq *pBufReq )
{
	CAM_Error        error   = CAM_ERROR_NONE;
	HM2057State *pState = (HM2057State*)pDevice;

	error = V4L2_GetBufReq( &pState->stV4L2, pSensorConfig, pBufReq );

	return error;
}

CAM_Error HM2057_Enqueue( void *pDevice, CAM_ImageBuffer *pImgBuf )
{
	CAM_Error        error   = CAM_ERROR_NONE;
	HM2057State *pState = (HM2057State*)pDevice;

	error = V4L2_Enqueue( &pState->stV4L2, pImgBuf );

	return error;
}

CAM_Error HM2057_Dequeue(void *pDevice, CAM_ImageBuffer **ppImgBuf)
{
	CAM_Error        error   = CAM_ERROR_NONE;
	HM2057State *pState = (HM2057State*)pDevice;

	error = V4L2_Dequeue( &pState->stV4L2, ppImgBuf );

	if ( error == CAM_ERROR_NONE && (*ppImgBuf)->bEnableShotInfo )
	{
		error = _HM2057FillFrameShotInfo( pState, *ppImgBuf );
	}

	return error;
}

CAM_Error HM2057_Flush(void *pDevice)
{
	CAM_Error        error   = CAM_ERROR_NONE;
	HM2057State *pState = (HM2057State*)pDevice;

	error = V4L2_Flush( &pState->stV4L2 );

	return error;
}

static CAM_Int32s HM2057SetParamsReg( CAM_Int32s param,_CAM_ParameterEntry *optionArray,CAM_Int32s optionSize, CAM_Int32s sensorFD )
{
	CAM_Int32s i = 0;
	CAM_Int32s ret = 0;

	for ( i = 0; i < optionSize; i++ ) 
	{
		if ( param == optionArray[i].iParameter ) 
		{
			break;
		}
	}
	
	if ( i >= optionSize ) 
	{
		return -2;
	}

	if ( optionArray[i].stSetParam.stRegTable.pEntries[0].reg == 0x0000 )
	{
		// NOTE: in this case, no sensor registers need to be set
		ret = 0;
	}
	else
	{
		ret = _set_reg_array( sensorFD, optionArray[i].stSetParam.stRegTable.pEntries, optionArray[i].stSetParam.stRegTable.iLength );
	}

	return ret;
}

CAM_Error HM2057_SetShotParam( void *pDevice, _CAM_ShotParam *pShotParam )
{
	CAM_Error error = CAM_ERROR_NONE;
	CAM_Int32s ret = 0;
	HM2057State *pState = (HM2057State*)pDevice;
	_CAM_ShotParam sSetShotParam = *pShotParam;

	if (sSetShotParam.eShotMode != pState->stV4L2.stShotParam.eShotMode)
		pState->stV4L2.stShotParam.eShotMode = sSetShotParam.eShotMode;

	// Color Effect Setting
	if ( sSetShotParam.eColorEffect != pState->stV4L2.stShotParam.eColorEffect ) 
	{
		ret = HM2057SetParamsReg( sSetShotParam.eColorEffect, HM2057ColorEffectMode, _ARRAY_SIZE(HM2057ColorEffectMode), pState->stV4L2.iSensorFD );
		
		if ( ret == -2 ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.eColorEffect = sSetShotParam.eColorEffect;
	}

	// White Balance Setting
	if ( sSetShotParam.eWBMode != pState->stV4L2.stShotParam.eWBMode ) 
	{
		ret = HM2057SetParamsReg( sSetShotParam.eWBMode, HM2057WBMode, _ARRAY_SIZE(HM2057WBMode), pState->stV4L2.iSensorFD );
		
		if ( ret == -2 ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.eWBMode = sSetShotParam.eWBMode;
	}
	
	// Banding filter
	if ( sSetShotParam.eBandFilterMode != pState->stV4L2.stShotParam.eBandFilterMode ) 
	{
		ret = HM2057SetParamsReg( sSetShotParam.eBandFilterMode, _HM2057BdFltMode, _ARRAY_SIZE(_HM2057BdFltMode), pState->stV4L2.iSensorFD );
		
		if ( ret == -2 ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.eBandFilterMode = sSetShotParam.eBandFilterMode;
	}

	// Exposure value
	if ( sSetShotParam.iEvCompQ16 != pState->stV4L2.stShotParam.iEvCompQ16 ) 
	{
        int i;
        /*
		int index = 0;
		LOGD("Set ev to %d, old: %d", sSetShotParam.iEvCompQ16, pState->stV4L2.stShotParam.iEvCompQ16);
		for (i=0; i<_ARRAY_SIZE(HM2057EvCompMode)-1; i++)
		{
			if (sSetShotParam.iEvCompQ16 >= HM2057EvCompMode[i].iParameter && sSetShotParam.iEvCompQ16 <= HM2057EvCompMode[i+1].iParameter)
				index = i;
		}*/
		ret = HM2057SetParamsReg( sSetShotParam.iEvCompQ16, HM2057EvCompMode, _ARRAY_SIZE(HM2057EvCompMode), pState->stV4L2.iSensorFD );
		
		if ( ret == -2 ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.iEvCompQ16 = sSetShotParam.iEvCompQ16;
	}

	// Brightness
	if ( sSetShotParam.iBrightness != pState->stV4L2.stShotParam.iBrightness ) 
	{
		ret = HM2057SetParamsReg( sSetShotParam.iBrightness, HM2057Brightness, _ARRAY_SIZE(HM2057Brightness), pState->stV4L2.iSensorFD );
		
		if ( ret == -2 ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.iBrightness = sSetShotParam.iBrightness;
	}

	// Contrast
	if ( sSetShotParam.iContrast != pState->stV4L2.stShotParam.iContrast ) 
	{
		ret = HM2057SetParamsReg( sSetShotParam.iContrast, HM2057Contrast, _ARRAY_SIZE(HM2057Contrast), pState->stV4L2.iSensorFD );
		
		if ( ret == -2 ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.iContrast = sSetShotParam.iContrast;
	}

	return CAM_ERROR_NONE;
}

CAM_Error HM2057_GetShotParam( void *pDevice, _CAM_ShotParam *pShotParam )
{
	HM2057State *pState = (HM2057State*)pDevice;
    
	*pShotParam = pState->stV4L2.stShotParam;

	return CAM_ERROR_NONE;
}

CAM_Error HM2057_StartFocus( void *pDevice, CAM_AFMode eAFMode, void *pFocusParams )
{
	// TODO: add your start focus code here,an refrence is ov5642.c

	return CAM_ERROR_NONE;
}

CAM_Error HM2057_CancelFocus( void *pDevice )
{
	// TODO: add yourcancel focus code here,an refrence is ov5642.c
	return CAM_ERROR_NONE;
}

CAM_Error HM2057_CheckFocusState( void *pDevice, CAM_Bool *pFocusAutoStopped, _CAM_FocusState *pFocusState )
{

	// TODO: add your check focus status code here,an refrence is ov5642.c
	*pFocusAutoStopped = CAM_TRUE;
	*pFocusState       = CAM_IN_FOCUS;

	return CAM_ERROR_NONE;
}


CAM_Error HM2057_GetDigitalZoomCapability( CAM_Int32s iWidth, CAM_Int32s iHeight, CAM_Int32s *pSensorDigZoomQ16 )
{
	// TODO: add your get zoom capability code here,an refrence is ov5642.c
	*pSensorDigZoomQ16 = ( 1 << 16 );

	return CAM_ERROR_NONE;
}

_SmartSensorFunc func_hm2057 = 
{
	HM2057_GetCapability,
	HM2057_GetShotModeCapability,

	HM2057_SelfDetect,
	HM2057_Init,
	HM2057_Deinit,
	HM2057_Config,
	HM2057_GetBufReq,
	HM2057_Enqueue,
	HM2057_Dequeue,
	HM2057_Flush,
	HM2057_SetShotParam,
	HM2057_GetShotParam,

	HM2057_StartFocus,
	HM2057_CancelFocus,
	HM2057_CheckFocusState,
	HM2057_GetDigitalZoomCapability,

};

CAM_Error _HM2057SetJpegParam( void *pDevice, CAM_JpegParam *pJpegParam )
{
	HM2057State *pState = (HM2057State*)pDevice;

	if ( pJpegParam->bEnableExif )
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}

	if ( pJpegParam->bEnableThumb )
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}

	if ( pJpegParam->iQualityFactor != 80 )
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}
	else
	{
		pState->stV4L2.stJpegParam.iQualityFactor = 80;
	}

	return CAM_ERROR_NONE;
}

/*-------------------------------------------------------------------------------------------------------------------------------------
 * HM2057 shotmode capability
 * TODO: if you enable new shot mode, pls add a correspoding modcap function here, and add it to HM2057_shotModeCap _HM2057ShotModeCap array
 *------------------------------------------------------------------------------------------------------------------------------------*/
static void _HM2057AutoModeCap( CAM_ShotModeCapability *pShotModeCap )
{
	int i;

	// exposure mode
	pShotModeCap->iSupportedExpModeCnt  = 1;
	pShotModeCap->eSupportedExpMode[0]  = CAM_EXPOSUREMODE_AUTO;
	
	// exposure metering mode 
	pShotModeCap->iSupportedExpMeterCnt = 1;
	pShotModeCap->eSupportedExpMeter[0] = CAM_EXPOSUREMETERMODE_AUTO;

	// EV compensation 
	pShotModeCap->iEVCompStepQ16 = 3;
	pShotModeCap->iMinEVCompQ16 = -12;
	pShotModeCap->iMaxEVCompQ16 = 12;

	// ISO mode 
	pShotModeCap->iSupportedIsoModeCnt = 1;
	pShotModeCap->eSupportedIsoMode[0] = CAM_ISO_AUTO;

	// shutter speed
	pShotModeCap->iMinShutSpdQ16 = -1;
	pShotModeCap->iMaxShutSpdQ16 = -1;

	// F-number
	pShotModeCap->iMinFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);
	pShotModeCap->iMaxFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);

	// Band filter
	pShotModeCap->iSupportedBdFltModeCnt = _ARRAY_SIZE(_HM2057BdFltMode); 
    for (i=0; i<pShotModeCap->iSupportedBdFltModeCnt; i++)
		pShotModeCap->eSupportedBdFltMode[i] = _HM2057BdFltMode[i].iParameter;

	// Flash mode
	pShotModeCap->iSupportedFlashModeCnt = 1;
	pShotModeCap->eSupportedFlashMode[0] = CAM_FLASH_OFF;

	// white balance mode
	pShotModeCap->iSupportedWBModeCnt = _ARRAY_SIZE(HM2057WBMode);
	for (i=0; i<pShotModeCap->iSupportedWBModeCnt; i++)
		pShotModeCap->eSupportedWBMode[i] = HM2057WBMode[i].iParameter;


	// focus mode
	pShotModeCap->iSupportedFocusModeCnt = 1;
	pShotModeCap->eSupportedFocusMode[0] = CAM_FOCUS_AUTO_ONESHOT;

	// AF mode 
	pShotModeCap->iSupportedAFModeCnt = 0;

	// optical zoom mode
	pShotModeCap->iMinOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);

	// digital zoom mode
	pShotModeCap->iMinDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->bSupportSmoothDigZoom = CAM_FALSE;

	// frame rate
	pShotModeCap->iMinFpsQ16     = 1 << 16;
	pShotModeCap->iMaxFpsQ16     = 40 << 16;

	// color effect
	pShotModeCap->iSupportedColorEffectCnt = _ARRAY_SIZE(HM2057ColorEffectMode);
	for (i=0; i<pShotModeCap->iSupportedColorEffectCnt; i++)
		pShotModeCap->eSupportedColorEffect[i] = HM2057ColorEffectMode[i].iParameter; 

	// brightness
	pShotModeCap->iMinBrightness = -2;
	pShotModeCap->iMaxBrightness = 2;

	// contrast
	pShotModeCap->iMinContrast = -2;
	pShotModeCap->iMaxContrast = 2;

	// saturation
	pShotModeCap->iMinSaturation = 0;
	pShotModeCap->iMaxSaturation = 0;

	// sharpness
	pShotModeCap->iMinSharpness = 0;
	pShotModeCap->iMaxSharpness = 0;

	// advanced features
	pShotModeCap->bSupportVideoStabilizer   = CAM_FALSE;
	pShotModeCap->bSupportVideoNoiseReducer	= CAM_FALSE;
	pShotModeCap->bSupportZeroShutterLag    = CAM_FALSE;
	pShotModeCap->bSupportBurstCapture      = CAM_FALSE;
	pShotModeCap->bSupportHighDynamicRange  = CAM_FALSE;
	pShotModeCap->iMaxBurstCnt              = 1;

	return;
}


/*-------------------------------------------------------------------------------------------------------------------------------------
 * HM2057 shotmode capability
 * TODO: if you enable new shot mode, pls add a correspoding modcap function here, and add it to HM2057_shotModeCap _HM2057ShotModeCap array
 *------------------------------------------------------------------------------------------------------------------------------------*/
static void _HM2057ManualModeCap( CAM_ShotModeCapability *pShotModeCap )
{
	int i;

	// exposure mode
	pShotModeCap->iSupportedExpModeCnt  = 1;
	pShotModeCap->eSupportedExpMode[0]  = CAM_EXPOSUREMODE_AUTO;
	
	// exposure metering mode 
	pShotModeCap->iSupportedExpMeterCnt = 1;
	pShotModeCap->eSupportedExpMeter[0] = CAM_EXPOSUREMETERMODE_AUTO;

	// EV compensation 
	pShotModeCap->iEVCompStepQ16 = 3;
	pShotModeCap->iMinEVCompQ16 = -12;
	pShotModeCap->iMaxEVCompQ16 = 12;

	// ISO mode 
	pShotModeCap->iSupportedIsoModeCnt = 1;
	pShotModeCap->eSupportedIsoMode[0] = CAM_ISO_AUTO;

	// shutter speed
	pShotModeCap->iMinShutSpdQ16 = -1;
	pShotModeCap->iMaxShutSpdQ16 = -1;

	// F-number
	pShotModeCap->iMinFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);
	pShotModeCap->iMaxFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);

	// Band filter
	pShotModeCap->iSupportedBdFltModeCnt = 1; 
	pShotModeCap->eSupportedBdFltMode[0] = CAM_BANDFILTER_50HZ;

	// Flash mode
	pShotModeCap->iSupportedFlashModeCnt = 1;
	pShotModeCap->eSupportedFlashMode[0] = CAM_FLASH_OFF;

	// white balance mode
	pShotModeCap->iSupportedWBModeCnt = _ARRAY_SIZE(HM2057WBMode);
	for (i=0; i<pShotModeCap->iSupportedWBModeCnt; i++)
		pShotModeCap->eSupportedWBMode[i] = HM2057WBMode[i].iParameter;

	// focus mode
	pShotModeCap->iSupportedFocusModeCnt = 1;
	pShotModeCap->eSupportedFocusMode[0] = CAM_FOCUS_AUTO_ONESHOT;

	// AF mode 
	pShotModeCap->iSupportedAFModeCnt = 0;

	// optical zoom mode
	pShotModeCap->iMinOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);

	// digital zoom mode
	pShotModeCap->iMinDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->bSupportSmoothDigZoom = CAM_FALSE;

	// frame rate
	pShotModeCap->iMinFpsQ16     = 1 << 16;
	pShotModeCap->iMaxFpsQ16     = 40 << 16;

	// color effect
	pShotModeCap->iSupportedColorEffectCnt = _ARRAY_SIZE(HM2057ColorEffectMode);
	for (i=0; i<pShotModeCap->iSupportedColorEffectCnt; i++)
		pShotModeCap->eSupportedColorEffect[i] = HM2057ColorEffectMode[i].iParameter; 

	// brightness
	pShotModeCap->iMinBrightness = -2;
	pShotModeCap->iMaxBrightness = 2;

	// contrast
	pShotModeCap->iMinContrast = -2;
	pShotModeCap->iMaxContrast = 2;

	// saturation
	pShotModeCap->iMinSaturation = 0;
	pShotModeCap->iMaxSaturation = 0;

	// sharpness
	pShotModeCap->iMinSharpness = 0;
	pShotModeCap->iMaxSharpness = 0;

	// advanced features
	pShotModeCap->bSupportVideoStabilizer   = CAM_FALSE;
	pShotModeCap->bSupportVideoNoiseReducer	= CAM_FALSE;
	pShotModeCap->bSupportZeroShutterLag    = CAM_FALSE;
	pShotModeCap->bSupportBurstCapture      = CAM_FALSE;
	pShotModeCap->bSupportHighDynamicRange  = CAM_FALSE;
	pShotModeCap->iMaxBurstCnt              = 1;

	return;
}

static CAM_Error _HM2057SaveAeAwb( const _CAM_SmartSensorConfig *pOldConfig, void *pUserData )
{
	// TODO: add your sensor specific save 3A function here
	return CAM_ERROR_NONE;
}

static CAM_Error _HM2057RestoreAeAwb( const _CAM_SmartSensorConfig *pNewConfig, void *pUserData )
{
	// TODO: add your sensor specific restore 3A function here
	return CAM_ERROR_NONE;  
}

static CAM_Error _HM2057StartFlash( void *pSensorState )
{
	// TODO: add your sensor specific start flash function here
	return CAM_ERROR_NONE;
}

static CAM_Error _HM2057StopFlash( void *pSensorState )
{
	// TODO: add your sensor specific stop flash function here
	return CAM_ERROR_NONE;
}

static CAM_Error _HM2057ApplyShotParam(void* pUserData)
{
	HM2057State *pState = (HM2057State*) pUserData;

	HM2057SetParamsReg(pState->stV4L2.stShotParam.eWBMode, HM2057WBMode, _ARRAY_SIZE(HM2057WBMode), pState->stV4L2.iSensorFD);

	HM2057SetParamsReg(pState->stV4L2.stShotParam.eColorEffect, HM2057ColorEffectMode, _ARRAY_SIZE(HM2057ColorEffectMode), pState->stV4L2.iSensorFD);

	HM2057SetParamsReg(pState->stV4L2.stShotParam.eBandFilterMode, _HM2057BdFltMode, _ARRAY_SIZE(_HM2057BdFltMode), pState->stV4L2.iSensorFD);

	HM2057SetParamsReg(pState->stV4L2.stShotParam.iEvCompQ16, HM2057EvCompMode, _ARRAY_SIZE(HM2057EvCompMode), pState->stV4L2.iSensorFD);

	HM2057SetParamsReg(pState->stV4L2.stShotParam.iBrightness, HM2057Brightness, _ARRAY_SIZE(HM2057EvCompMode), pState->stV4L2.iSensorFD);

	HM2057SetParamsReg(pState->stV4L2.stShotParam.iContrast, HM2057Contrast, _ARRAY_SIZE(HM2057EvCompMode), pState->stV4L2.iSensorFD);

	return CAM_ERROR_NONE;
}

// get shot info
static CAM_Error _HM2057FillFrameShotInfo( HM2057State *pState, CAM_ImageBuffer *pImgBuf )
{
	// TODO: add real value of these parameters here

	CAM_Error    error      = CAM_ERROR_NONE;
	CAM_ShotInfo *pShotInfo = &(pImgBuf->stShotInfo);

	pShotInfo->iExposureTimeQ16    = (1 << 16) / 30;
	pShotInfo->iFNumQ16            = (int)( 2.8 * 65536 + 0.5 );
	pShotInfo->eExposureMode       = CAM_EXPOSUREMODE_AUTO;
	pShotInfo->eExpMeterMode       = CAM_EXPOSUREMETERMODE_MEAN;
	pShotInfo->iISOSpeed           = 100;
	pShotInfo->iSubjectDistQ16     = 0;
	pShotInfo->iFlashStatus        = 0x0010;
	pShotInfo->iFocalLenQ16        = (int)( 3.79 * 65536 + 0.5 );
	pShotInfo->iFocalLen35mm       = 0;       

	return error;
}
