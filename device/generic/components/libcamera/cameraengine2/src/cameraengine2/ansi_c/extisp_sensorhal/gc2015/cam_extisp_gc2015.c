/*******************************************************************************
//(C) Copyright [2010 - 2011] Marvell International Ltd.
//All Rights Reserved
*******************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "cam_log.h"
#include "cam_utility.h"


#include "cam_extisp_sensorwrapper.h"
#include "cam_extisp_v4l2base.h"
#include "cam_extisp_gc2015.h"

#define REG(a,b) {a, 0, 7, 0, b}

// NOTE: if you want to enable static resolution table to bypass sensor dynamically detect to save camera-off to viwerfinder-on latency, 
//       you can fill the following four tables according to your sensor's capability. And open macro 
//       BUILD_OPTION_STARTEGY_DISABLE_DYNAMIC_SENSOR_DETECT in cam_extisp_buildopt.h

/* Basic Sensor video/preview resolution table */
CAM_Size _GC2015VideoResTable[CAM_MAX_SUPPORT_IMAGE_SIZE_CNT] = 
{
	{0, 0},
};

/* Basic Sensor still resolution table */
CAM_Size _GC2015StillResTable[CAM_MAX_SUPPORT_IMAGE_SIZE_CNT] = 
{ 
	{0, 0},
};

/* Basic Sensor video/preview format table */
CAM_ImageFormat _GC2015VideoFormatTable[CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT] = 
{ 
	0, 
};

/* Basic Sensor still format table */
CAM_ImageFormat _GC2015StillFormatTable[CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT] = 
{
	0,
};


CAM_FlipRotate _GC2015RotationTable[] = 
{
	CAM_FLIPROTATE_NORMAL,
};


CAM_ShotMode _GC2015ShotModeTable[] = 
{
	CAM_SHOTMODE_AUTO,
	//CAM_SHOTMODE_MANUAL,
};

_CAM_RegEntry _GC2015ExpMeter_Mean[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _GC2015ExpMeter[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_EXPOSUREMETERMODE_MEAN, _GC2015ExpMeter_Mean),
};

_CAM_RegEntry _GC2015IsoMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _GC2015IsoMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_ISO_AUTO, _GC2015IsoMode_Auto),
};

_CAM_RegEntry _GC2015BdFltMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _GC2015BdFltMode_Off[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _GC2015BdFltMode_50Hz[] = 
{
	/*
	REG(0xfe, 0x00),
    REG(0x05, 0x01),
    REG(0x06, 0xc1),
    REG(0x07, 0x00),
    REG(0x08, 0x40),
    REG(0xfe, 0x01),
    REG(0x29, 0x00),
    REG(0x2a, 0x80),
    REG(0x2b, 0x05),
    REG(0x2c, 0x00),
    REG(0x2d, 0x06),
    REG(0x2e, 0x00),
    REG(0x2f, 0x08),
    REG(0x30, 0x00),
    REG(0x31, 0x09),
    REG(0x32, 0x00),
    REG(0xfe, 0x00), */
};

_CAM_RegEntry _GC2015BdFltMode_60Hz[] = 
{
    /*
	REG(0xfe, 0x00),
    REG(0x05, 0x01),
    REG(0x06, 0xc1),
    REG(0x07, 0x00),
    REG(0x08, 0x40),
    REG(0xfe, 0x01),
    REG(0x29, 0x00),
    REG(0x2a, 0x80),
    REG(0x2b, 0x05),
    REG(0x2c, 0x00),
    REG(0x2d, 0x06),
    REG(0x2e, 0x00),
    REG(0x2f, 0x08),
    REG(0x30, 0x00),
    REG(0x31, 0x09),
    REG(0x32, 0x00),
    REG(0xfe, 0x00), */
};

_CAM_ParameterEntry _GC2015BdFltMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_BANDFILTER_50HZ, _GC2015BdFltMode_50Hz),
	PARAM_ENTRY_USE_REGTABLE(CAM_BANDFILTER_60HZ, _GC2015BdFltMode_60Hz),
};

_CAM_RegEntry _GC2015FlashMode_Off[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _GC2015FlashMode_On[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_RegEntry _GC2015FlashMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _GC2015FlashMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_FLASH_OFF,	_GC2015FlashMode_Off),
};

_CAM_RegEntry GC2015WBMode_Auto[] =
{
	REG(0x42, 0x76),
};

_CAM_RegEntry GC2015WBMode_CLOUDY[] =
{
	REG(0x42, 0x74),
	REG(0x7a, 0x8c),
	REG(0x7b, 0x50),
	REG(0x7c, 0x40),
};

_CAM_RegEntry GC2015WBMode_DAYLIGHT[] =
{
	REG(0x42, 0x74),
	REG(0x7a, 0x74),
	REG(0x7b, 0x52),
	REG(0x7c, 0x40),
};

_CAM_RegEntry GC2015WBMode_INCANDESCENT[] =
{
	REG(0x42, 0x74),
	REG(0x7a, 0x48),
	REG(0x7b, 0x40),
	REG(0x7c, 0x5c),
};

_CAM_RegEntry GC2015WBMode_FLUORESCENT1[] =
{
	REG(0x42, 0x74),
	REG(0x7a, 0x40),
	REG(0x7b, 0x42),
	REG(0x7c, 0x50),
};

_CAM_ParameterEntry GC2015WBMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_AUTO, GC2015WBMode_Auto),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_CLOUDY, GC2015WBMode_CLOUDY),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_DAYLIGHT, GC2015WBMode_DAYLIGHT),
	PARAM_ENTRY_USE_REGTABLE(CAM_WHITEBALANCEMODE_INCANDESCENT, GC2015WBMode_INCANDESCENT),
};

_CAM_RegEntry _GC2015FocusMode_Auto[] = 
{
	{0, 0, 0, 0, 0},
};

_CAM_ParameterEntry _GC2015FocusMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_FOCUS_AUTO_ONESHOT,  _GC2015FocusMode_Auto),
};

_CAM_RegEntry GC2015ColorEffectMode_OFF[] = 
{
	REG(0x43, 0x00),
};

_CAM_RegEntry GC2015ColorEffectMode_SEPIA[] = 
{
	REG(0x43, 0x02),
	REG(0xda, 0xd0),
	REG(0xdb, 0x28),
};

_CAM_RegEntry GC2015ColorEffectMode_GRAYSCALE[] = 
{
	REG(0x43, 0x02),
	REG(0xda, 0x00),
	REG(0xdb, 0x00),
};

_CAM_ParameterEntry GC2015ColorEffectMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_OFF, GC2015ColorEffectMode_OFF),
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_SEPIA, GC2015ColorEffectMode_SEPIA),
	PARAM_ENTRY_USE_REGTABLE(CAM_COLOREFFECT_GRAYSCALE, GC2015ColorEffectMode_GRAYSCALE),
};

// EV compensation
_CAM_RegEntry GC2015EvComp_N12[] = 
{
    REG(0xfe, 0x01),
    REG(0x13, 0x40),
    REG(0xfe, 0x00),
    REG(0xd5, 0xc0),
};

_CAM_RegEntry GC2015EvComp_N09[] = 
{
    REG(0xfe, 0x01),
    REG(0x13, 0x48),
    REG(0xfe, 0x00),
    REG(0xd5, 0xd0),
};

_CAM_RegEntry GC2015EvComp_N06[] = 
{
    REG(0xfe, 0x01),
    REG(0x13, 0x50),
    REG(0xfe, 0x00),
    REG(0xd5, 0xe0),
};

_CAM_RegEntry GC2015EvComp_N03[] = 
{
    REG(0xfe, 0x01),
    REG(0x13, 0x58),
    REG(0xfe, 0x00),
    REG(0xd5, 0xf0),
};

_CAM_RegEntry GC2015EvComp_000[] = 
{
    REG(0xfe, 0x01),
    REG(0x13, 0x60),
    REG(0xfe, 0x00),
    REG(0xd5, 0x00),
};

_CAM_RegEntry GC2015EvComp_P03[] = 
{
    REG(0xfe, 0x01),
    REG(0x13, 0x68),
    REG(0xfe, 0x00),
    REG(0xd5, 0x10),
};

_CAM_RegEntry GC2015EvComp_P06[] = 
{
    REG(0xfe, 0x01),
    REG(0x13, 0x70),
    REG(0xfe, 0x00),
    REG(0xd5, 0x20),
};

_CAM_RegEntry GC2015EvComp_P09[] = 
{
    REG(0xfe, 0x01),
    REG(0x13, 0x78),
    REG(0xfe, 0x00),
    REG(0xd5, 0x30),
};

_CAM_RegEntry GC2015EvComp_P12[] = 
{
    REG(0xfe, 0x01),
    REG(0x13, 0x80),
    REG(0xfe, 0x00),
    REG(0xd5, 0x40),
};

_CAM_ParameterEntry GC2015EvCompMode[] = 
{
	PARAM_ENTRY_USE_REGTABLE(-78644,	GC2015EvComp_N12),
	PARAM_ENTRY_USE_REGTABLE(-58983,	GC2015EvComp_N09),
	PARAM_ENTRY_USE_REGTABLE(-39322,	GC2015EvComp_N06),
	PARAM_ENTRY_USE_REGTABLE(-19661,	GC2015EvComp_N03),
	PARAM_ENTRY_USE_REGTABLE(0,		GC2015EvComp_000),
	PARAM_ENTRY_USE_REGTABLE(19661,		GC2015EvComp_P03),
	PARAM_ENTRY_USE_REGTABLE(39322,		GC2015EvComp_P06),
	PARAM_ENTRY_USE_REGTABLE(58983,		GC2015EvComp_P09),
	PARAM_ENTRY_USE_REGTABLE(78644,		GC2015EvComp_P12),
};

static CAM_Error _GC2015SaveAeAwb( const _CAM_SmartSensorConfig*, void* );
static CAM_Error _GC2015RestoreAeAwb( const _CAM_SmartSensorConfig*, void* );
static CAM_Error _GC2015StartFlash( void* );
static CAM_Error _GC2015StopFlash( void* );
static CAM_Error _GC2015ApplyShotParam(void*);
static CAM_Error _GC2015FillFrameShotInfo( GC2015State*, CAM_ImageBuffer* );

// shot mode capability
static void _GC2015AutoModeCap( CAM_ShotModeCapability* );
static void _GC2015ManualModeCap( CAM_ShotModeCapability*);
// static void _GC2015NightModeCap( CAM_ShotModeCapability*);
/* shot mode cap function table */
GC2015_ShotModeCap _GC2015ShotModeCap[CAM_SHOTMODE_NUM] = { 
	_GC2015AutoModeCap     , // CAM_SHOTMODE_AUTO = 0,
	_GC2015ManualModeCap   , // CAM_SHOTMODE_MANUAL,
	NULL                   , // CAM_SHOTMODE_PORTRAIT,
	NULL                   , // CAM_SHOTMODE_LANDSCAPE,
	NULL                   , // CAM_SHOTMODE_NIGHTPORTRAIT,
	NULL                   , // CAM_SHOTMODE_NIGHTSCENE,
	NULL                   , // CAM_SHOTMODE_CHILD,
	NULL                   , // CAM_SHOTMODE_INDOOR,
	NULL                   , // CAM_SHOTMODE_PLANTS,
	NULL                   , // CAM_SHOTMODE_SNOW,
	NULL                   , // CAM_SHOTMODE_BEACH,
	NULL                   , // CAM_SHOTMODE_FIREWORKS,
	NULL                   , // CAM_SHOTMODE_SUBMARINE, 
};  

extern _SmartSensorFunc func_gc2015; 
CAM_Error GC2015_SelfDetect( _SmartSensorAttri *pSensorInfo )
{
	CAM_Error error = CAM_ERROR_NONE;
	
	// NOTE:  If you open macro BUILD_OPTION_STARTEGY_DISABLE_DYNAMIC_SENSOR_DETECT in cam_extisp_buildopt.h 
	//        to bypass sensor dynamically detect to save camera-off to viwerfinder-on latency, you should initilize
	//        _GC2015VideoResTable/_GC2015StillResTable/_GC2015VideoFormatTable/_GC2015StillFormatTable manually.

#if !defined( BUILD_OPTION_STARTEGY_DISABLE_DYNAMIC_SENSOR_DETECT )	
	error = V4L2_SelfDetect( pSensorInfo, "gc2015", &func_gc2015,
	                         _GC2015VideoResTable, _GC2015StillResTable,
	                         _GC2015VideoFormatTable, _GC2015StillFormatTable );

#else
	{
		_V4L2SensorEntry *pSensorEntry = (_V4L2SensorEntry*)( pSensorInfo->cReserved );
		strcpy( pSensorInfo->sSensorName, "GC2015-unknown" );
		pSensorInfo->pFunc = &func_gc2015;

		// FIXME: the following is just an example in Marvell platform, you should change it according to your driver implementation
		strcpy( pSensorEntry->sDeviceName, "/dev/video0" );
		pSensorEntry->iV4L2SensorID = 0;
	}
#endif

	return error;
} 


CAM_Error GC2015_GetCapability( _CAM_SmartSensorCapability *pCapability )
{
	CAM_Int32s i = 0;


	// preview/video supporting 
	// format
	pCapability->iSupportedVideoFormatCnt = 0;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT; i++ ) 
	{
		if ( _GC2015VideoFormatTable[i] == 0 )
		{
			break;
		}
		pCapability->eSupportedVideoFormat[pCapability->iSupportedVideoFormatCnt] = _GC2015VideoFormatTable[i];
		pCapability->iSupportedVideoFormatCnt++;
	}

	pCapability->bArbitraryVideoSize     = CAM_FALSE;
	pCapability->iSupportedVideoSizeCnt  = 0;
	pCapability->stMinVideoSize.iWidth   = _GC2015VideoResTable[0].iWidth;
	pCapability->stMinVideoSize.iHeight  = _GC2015VideoResTable[0].iHeight;
	pCapability->stMaxVideoSize.iWidth   = _GC2015VideoResTable[0].iWidth;
	pCapability->stMaxVideoSize.iHeight  = _GC2015VideoResTable[0].iHeight;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_SIZE_CNT; i++ )
	{
		if ( _GC2015VideoResTable[i].iWidth == 0 || _GC2015VideoResTable[i].iHeight == 0)
		{
			break;
		}
		pCapability->stSupportedVideoSize[pCapability->iSupportedVideoSizeCnt] = _GC2015VideoResTable[i];
		pCapability->iSupportedVideoSizeCnt++;

		if ( ( pCapability->stMinVideoSize.iWidth > _GC2015VideoResTable[i].iWidth ) || 
		     ( ( pCapability->stMinVideoSize.iWidth == _GC2015VideoResTable[i].iWidth ) && ( pCapability->stMinVideoSize.iHeight > _GC2015VideoResTable[i].iHeight ) ) ) 
		{
			pCapability->stMinVideoSize.iWidth = _GC2015VideoResTable[i].iWidth;
			pCapability->stMinVideoSize.iHeight = _GC2015VideoResTable[i].iHeight;
		}
		if ( ( pCapability->stMaxVideoSize.iWidth < _GC2015VideoResTable[i].iWidth) ||
		     ( ( pCapability->stMaxVideoSize.iWidth == _GC2015VideoResTable[i].iWidth ) && ( pCapability->stMaxVideoSize.iHeight < _GC2015VideoResTable[i].iHeight ) ) )
		{
			pCapability->stMaxVideoSize.iWidth = _GC2015VideoResTable[i].iWidth;
			pCapability->stMaxVideoSize.iHeight = _GC2015VideoResTable[i].iHeight;
		}
	}

	// still capture supporting
	// format
	pCapability->iSupportedStillFormatCnt           = 0;
	pCapability->stSupportedJPEGParams.bSupportJpeg = CAM_FALSE;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_FORMAT_CNT; i++ ) 
	{
		if ( _GC2015StillFormatTable[i] == CAM_IMGFMT_JPEG )
		{
			// JPEG encoder functionalities
			pCapability->stSupportedJPEGParams.bSupportJpeg = CAM_TRUE;
			pCapability->stSupportedJPEGParams.bSupportExif = CAM_FALSE;
			pCapability->stSupportedJPEGParams.bSupportThumb = CAM_FALSE;
			pCapability->stSupportedJPEGParams.iMinQualityFactor = 80;
			pCapability->stSupportedJPEGParams.iMaxQualityFactor = 80;
		}
		if ( _GC2015StillFormatTable[i] == 0 )
		{
			break;
		}
		pCapability->eSupportedStillFormat[pCapability->iSupportedStillFormatCnt] = _GC2015StillFormatTable[i];
		pCapability->iSupportedStillFormatCnt++;
	}
	// resolution
	pCapability->bArbitraryStillSize    = CAM_FALSE;
	pCapability->iSupportedStillSizeCnt = 0;
	pCapability->stMinStillSize.iWidth  = _GC2015StillResTable[0].iWidth;
	pCapability->stMinStillSize.iHeight = _GC2015StillResTable[0].iHeight;
	pCapability->stMaxStillSize.iWidth  = _GC2015StillResTable[0].iWidth;
	pCapability->stMaxStillSize.iHeight = _GC2015StillResTable[0].iHeight;
	for ( i = 0; i < CAM_MAX_SUPPORT_IMAGE_SIZE_CNT; i++ )
	{
		if ( _GC2015StillResTable[i].iWidth == 0 || _GC2015StillResTable[i].iHeight == 0 )
		{
			break;
		}

		pCapability->stSupportedStillSize[pCapability->iSupportedStillSizeCnt] = _GC2015StillResTable[i];
		pCapability->iSupportedStillSizeCnt++;

		if ( ( pCapability->stMinStillSize.iWidth > _GC2015StillResTable[i].iWidth ) ||
		     ( ( pCapability->stMinStillSize.iWidth == _GC2015StillResTable[i].iWidth ) && ( pCapability->stMinStillSize.iHeight > _GC2015StillResTable[i].iHeight ) ) )
		{
			pCapability->stMinStillSize.iWidth  = _GC2015StillResTable[i].iWidth;
			pCapability->stMinStillSize.iHeight = _GC2015StillResTable[i].iHeight;
		}
		if ( ( pCapability->stMaxStillSize.iWidth < _GC2015StillResTable[i].iWidth ) || 
		     ( ( pCapability->stMaxStillSize.iWidth == _GC2015StillResTable[i].iWidth ) && ( pCapability->stMaxStillSize.iHeight < _GC2015StillResTable[i].iHeight ) ) )
		{
			pCapability->stMaxStillSize.iWidth = _GC2015StillResTable[i].iWidth;
			pCapability->stMaxStillSize.iHeight = _GC2015StillResTable[i].iHeight;
		}
	}

	// rotate
	pCapability->iSupportedRotateCnt = _ARRAY_SIZE( _GC2015RotationTable );
	for ( i = 0; i < pCapability->iSupportedRotateCnt; i++ )
	{
		pCapability->eSupportedRotate[i] = _GC2015RotationTable[i];
	}

	pCapability->iSupportedShotModeCnt = _ARRAY_SIZE(_GC2015ShotModeTable);
	for ( i = 0; i < pCapability->iSupportedShotModeCnt; i++ )
	{
		pCapability->eSupportedShotMode[i] = _GC2015ShotModeTable[i];
	}

	return CAM_ERROR_NONE;
}

CAM_Error GC2015_GetShotModeCapability( CAM_ShotMode eShotMode, CAM_ShotModeCapability *pShotModeCap )
{
	CAM_Int32u i;
	// BAC check
	for ( i = 0; i < _ARRAY_SIZE( _GC2015ShotModeTable ); i++ )
	{
		if ( _GC2015ShotModeTable[i] == eShotMode )
		{
			break;
		}
	}

	if ( i >= _ARRAY_SIZE( _GC2015ShotModeTable ) || pShotModeCap ==NULL ) 
	{
		return CAM_ERROR_BADARGUMENT;
	}

	(void)(_GC2015ShotModeCap[eShotMode])( pShotModeCap );

	return CAM_ERROR_NONE;
}

CAM_Error GC2015_Init( void **ppDevice, void *pSensorEntry )
{
	CAM_Error             error      = CAM_ERROR_NONE;
	CAM_Int32s            iSkipFrame = 0;
	_V4L2SensorAttribute  _GC2015Attri;
	_V4L2SensorEntry      *pSensor = (_V4L2SensorEntry*)(pSensorEntry);
	GC2015State      *pState  = (GC2015State*)malloc( sizeof(GC2015State) );

	memset( &_GC2015Attri, 0, sizeof(_V4L2SensorAttribute) );

	*ppDevice = pState;
	if ( *ppDevice == NULL )
	{
		return CAM_ERROR_OUTOFMEMORY;
	}

	_GC2015Attri.stV4L2SensorEntry.iV4L2SensorID = pSensor->iV4L2SensorID;
	strcpy( _GC2015Attri.stV4L2SensorEntry.sDeviceName, pSensor->sDeviceName );

	/**************************************************************************************
     * defaultly, we will skip 30 frames in FAST VALIDATION PASS to avoid potential black
	 * frame if 3A is not convergence while resolution switch. If this is not need to your 
	 * sensor, just modify iSkipFrame, if you sensor need do save/restore 3A, pls refer to
	 * your sensor vendor, and implement these functions/
	***************************************************************************************/
#if defined( BUILD_OPTION_DEBUG_DISABLE_SAVE_RESTORE_3A )
	iSkipFrame                             = 2;
	_GC2015Attri.fnSaveAeAwb          = NULL;
	_GC2015Attri.fnRestoreAeAwb       = NULL;
	_GC2015Attri.fnStartFlash         = NULL;
 	_GC2015Attri.fnStopFlash          = NULL;
#else
	iSkipFrame                             = 2;
	_GC2015Attri.fnSaveAeAwb          = _GC2015SaveAeAwb;
	_GC2015Attri.fnRestoreAeAwb       = _GC2015RestoreAeAwb;
 	_GC2015Attri.fnStartFlash         =  _GC2015StartFlash;
 	_GC2015Attri.fnStopFlash          =  _GC2015StopFlash;
#endif

	_GC2015Attri.pSaveRestoreUserData = (void*)pState;
	_GC2015Attri.fnApplyShotParam = _GC2015ApplyShotParam;

	error = V4L2_Init( &(pState->stV4L2), &(_GC2015Attri), iSkipFrame );

	/* here we can get default shot params */
	pState->stV4L2.stShotParam.eShotMode            = CAM_SHOTMODE_AUTO;
	pState->stV4L2.stShotParam.eExpMode             = CAM_EXPOSUREMODE_AUTO;
	pState->stV4L2.stShotParam.eExpMeterMode        = CAM_EXPOSUREMETERMODE_AUTO;
	pState->stV4L2.stShotParam.iEvCompQ16           = 0;
	pState->stV4L2.stShotParam.eIsoMode             = CAM_ISO_AUTO;
	pState->stV4L2.stShotParam.iShutterSpeedQ16     = -1;
	pState->stV4L2.stShotParam.iFNumQ16             = 1;
	pState->stV4L2.stShotParam.eBandFilterMode      = CAM_BANDFILTER_50HZ;
	pState->stV4L2.stShotParam.eWBMode              = CAM_WHITEBALANCEMODE_AUTO;
	pState->stV4L2.stShotParam.eFocusMode           = CAM_FOCUS_INFINITY;
	pState->stV4L2.stShotParam.iDigZoomQ16          = 0;
	pState->stV4L2.stShotParam.eColorEffect         = CAM_COLOREFFECT_OFF;
	pState->stV4L2.stShotParam.iSaturation          = 64;
	pState->stV4L2.stShotParam.iBrightness          = 0;
	pState->stV4L2.stShotParam.iContrast            = 0;
	pState->stV4L2.stShotParam.iSharpness           = 0;
	pState->stV4L2.stShotParam.eFlashMode           = CAM_FLASH_OFF;
	pState->stV4L2.stShotParam.bVideoStabilizer     = CAM_FALSE;
	pState->stV4L2.stShotParam.bVideoNoiseReducer   = CAM_FALSE;
	pState->stV4L2.stShotParam.bZeroShutterLag      = CAM_FALSE;
	pState->stV4L2.stShotParam.bHighDynamicRange	= CAM_FALSE;

	pState->stV4L2.stShotParam.iBurstCnt            = 1;
	/* get default JPEG params */
	pState->stV4L2.stJpegParam.iSampling      = -1; // 0 - 420, 1 - 422, 2 - 444
	pState->stV4L2.stJpegParam.iQualityFactor = 80;
	pState->stV4L2.stJpegParam.bEnableExif    = CAM_FALSE;
	pState->stV4L2.stJpegParam.bEnableThumb   = CAM_FALSE;
	pState->stV4L2.stJpegParam.iThumbWidth    = 0;
	pState->stV4L2.stJpegParam.iThumbHeight   = 0;


	return error;
}

CAM_Error GC2015_Deinit( void *pDevice )
{
	CAM_Error error = CAM_ERROR_NONE;
	GC2015State *pState = (GC2015State*)pDevice;

	error = V4L2_Deinit( &pState->stV4L2 );

	free( pDevice );

	return error;
}

CAM_Error _GC2015SetJpegParam( void *pDevice, CAM_JpegParam *pJpegParam );
CAM_Error GC2015_Config( void *pDevice, _CAM_SmartSensorConfig *pSensorConfig )
{
	CAM_Error        error   = CAM_ERROR_NONE;
	GC2015State *pState = (GC2015State*)pDevice;

	error = V4L2_Config( &pState->stV4L2, pSensorConfig );
	if ( error != CAM_ERROR_NONE )
	{
		return error;
	}

	if ( pSensorConfig->eState != CAM_CAPTURESTATE_IDLE )
	{
		if ( pSensorConfig->eFormat == CAM_IMGFMT_JPEG )
		{
			error = _GC2015SetJpegParam( pDevice, &(pSensorConfig->stJpegParam) );
			if ( error != CAM_ERROR_NONE )
			{
				return error;
			}
		}
	}

	pState->stV4L2.stConfig = *pSensorConfig;
	return CAM_ERROR_NONE;
}

CAM_Error GC2015_GetBufReq( void *pDevice, _CAM_SmartSensorConfig *pSensorConfig, CAM_ImageBufferReq *pBufReq )
{
	CAM_Error        error   = CAM_ERROR_NONE;
	GC2015State *pState = (GC2015State*)pDevice;

	error = V4L2_GetBufReq( &pState->stV4L2, pSensorConfig, pBufReq );

	return error;
}

CAM_Error GC2015_Enqueue( void *pDevice, CAM_ImageBuffer *pImgBuf )
{
	CAM_Error        error   = CAM_ERROR_NONE;
	GC2015State *pState = (GC2015State*)pDevice;

	error = V4L2_Enqueue( &pState->stV4L2, pImgBuf );

	return error;
}

CAM_Error GC2015_Dequeue(void *pDevice, CAM_ImageBuffer **ppImgBuf)
{
	CAM_Error        error   = CAM_ERROR_NONE;
	GC2015State *pState = (GC2015State*)pDevice;

	error = V4L2_Dequeue( &pState->stV4L2, ppImgBuf );

	if ( error == CAM_ERROR_NONE && (*ppImgBuf)->bEnableShotInfo )
	{
		error = _GC2015FillFrameShotInfo( pState, *ppImgBuf );
	}

	return error;
}

CAM_Error GC2015_Flush(void *pDevice)
{
	CAM_Error        error   = CAM_ERROR_NONE;
	GC2015State *pState = (GC2015State*)pDevice;

	error = V4L2_Flush( &pState->stV4L2 );

	return error;
}

static CAM_Int32s GC2015SetParamsReg( CAM_Int32s param,_CAM_ParameterEntry *optionArray,CAM_Int32s optionSize, CAM_Int32s sensorFD )
{
	CAM_Int32s i = 0;
	CAM_Int32s ret = 0;

	for ( i = 0; i < optionSize; i++ ) 
	{
		if ( param == optionArray[i].iParameter ) 
		{
			break;
		}
	}
	
	if ( i >= optionSize ) 
	{
		return -2;
	}

	if ( optionArray[i].stSetParam.stRegTable.pEntries[0].reg == 0x0000 )
	{
		// NOTE: in this case, no sensor registers need to be set
		ret = 0;
	}
	else
	{
		ret = _set_reg_array( sensorFD, optionArray[i].stSetParam.stRegTable.pEntries, optionArray[i].stSetParam.stRegTable.iLength );
	}

	return ret;
}

CAM_Error GC2015_SetShotParam( void *pDevice, _CAM_ShotParam *pShotParam )
{
	CAM_Error error = CAM_ERROR_NONE;
	CAM_Int32s ret = 0;
	GC2015State *pState = (GC2015State*)pDevice;
	_CAM_ShotParam sSetShotParam = *pShotParam;

	if (sSetShotParam.eShotMode != pState->stV4L2.stShotParam.eShotMode)
		pState->stV4L2.stShotParam.eShotMode = sSetShotParam.eShotMode;

	// Color Effect Setting
	if ( sSetShotParam.eColorEffect != pState->stV4L2.stShotParam.eColorEffect ) 
	{
		ret = GC2015SetParamsReg( sSetShotParam.eColorEffect, GC2015ColorEffectMode, _ARRAY_SIZE(GC2015ColorEffectMode), pState->stV4L2.iSensorFD );
		
		if ( ret == -2 ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.eColorEffect = sSetShotParam.eColorEffect;
	}

	// White Balance Setting
	if ( sSetShotParam.eWBMode != pState->stV4L2.stShotParam.eWBMode ) 
	{
		ret = GC2015SetParamsReg( sSetShotParam.eWBMode, GC2015WBMode, _ARRAY_SIZE(GC2015WBMode), pState->stV4L2.iSensorFD );
		
		if ( ret == -2 ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.eWBMode = sSetShotParam.eWBMode;
	}
	
	// Banding filter
	if ( sSetShotParam.eBandFilterMode != pState->stV4L2.stShotParam.eBandFilterMode ) 
	{
		ret = GC2015SetParamsReg( sSetShotParam.eBandFilterMode, _GC2015BdFltMode, _ARRAY_SIZE(_GC2015BdFltMode), pState->stV4L2.iSensorFD );
		
		if ( ret == -2 ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.eBandFilterMode = sSetShotParam.eBandFilterMode;
	}

	// Exposure value
	if ( sSetShotParam.iEvCompQ16 != pState->stV4L2.stShotParam.iEvCompQ16 ) 
	{
        int i;
        /*
		int index = 0;
		LOGD("Set ev to %d, old: %d", sSetShotParam.iEvCompQ16, pState->stV4L2.stShotParam.iEvCompQ16);
		for (i=0; i<_ARRAY_SIZE(GC2015EvCompMode)-1; i++)
		{
			if (sSetShotParam.iEvCompQ16 >= GC2015EvCompMode[i].iParameter && sSetShotParam.iEvCompQ16 <= GC2015EvCompMode[i+1].iParameter)
				index = i;
		}*/
		ret = GC2015SetParamsReg( sSetShotParam.iEvCompQ16, GC2015EvCompMode, _ARRAY_SIZE(GC2015EvCompMode), pState->stV4L2.iSensorFD );
		
		if ( ret == -2 ) 
		{
			return CAM_ERROR_NOTSUPPORTEDARG;
		}

		pState->stV4L2.stShotParam.iEvCompQ16 = sSetShotParam.iEvCompQ16;
	}

	return CAM_ERROR_NONE;
}

CAM_Error GC2015_GetShotParam( void *pDevice, _CAM_ShotParam *pShotParam )
{
	GC2015State *pState = (GC2015State*)pDevice;
    
	*pShotParam = pState->stV4L2.stShotParam;

	return CAM_ERROR_NONE;
}

CAM_Error GC2015_StartFocus( void *pDevice, CAM_AFMode eAFMode, void *pFocusParams )
{
	// TODO: add your start focus code here,an refrence is ov5642.c

	return CAM_ERROR_NONE;
}

CAM_Error GC2015_CancelFocus( void *pDevice )
{
	// TODO: add yourcancel focus code here,an refrence is ov5642.c
	return CAM_ERROR_NONE;
}

CAM_Error GC2015_CheckFocusState( void *pDevice, CAM_Bool *pFocusAutoStopped, _CAM_FocusState *pFocusState )
{

	// TODO: add your check focus status code here,an refrence is ov5642.c
	*pFocusAutoStopped = CAM_TRUE;
	*pFocusState       = CAM_IN_FOCUS;

	return CAM_ERROR_NONE;
}


CAM_Error GC2015_GetDigitalZoomCapability( CAM_Int32s iWidth, CAM_Int32s iHeight, CAM_Int32s *pSensorDigZoomQ16 )
{
	// TODO: add your get zoom capability code here,an refrence is ov5642.c
	*pSensorDigZoomQ16 = ( 1 << 16 );

	return CAM_ERROR_NONE;
}

_SmartSensorFunc func_gc2015 = 
{
	GC2015_GetCapability,
	GC2015_GetShotModeCapability,

	GC2015_SelfDetect,
	GC2015_Init,
	GC2015_Deinit,
	GC2015_Config,
	GC2015_GetBufReq,
	GC2015_Enqueue,
	GC2015_Dequeue,
	GC2015_Flush,
	GC2015_SetShotParam,
	GC2015_GetShotParam,

	GC2015_StartFocus,
	GC2015_CancelFocus,
	GC2015_CheckFocusState,
	GC2015_GetDigitalZoomCapability,

};

CAM_Error _GC2015SetJpegParam( void *pDevice, CAM_JpegParam *pJpegParam )
{
	GC2015State *pState = (GC2015State*)pDevice;

	if ( pJpegParam->bEnableExif )
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}

	if ( pJpegParam->bEnableThumb )
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}

	if ( pJpegParam->iQualityFactor != 80 )
	{
		return CAM_ERROR_NOTSUPPORTEDARG;
	}
	else
	{
		pState->stV4L2.stJpegParam.iQualityFactor = 80;
	}

	return CAM_ERROR_NONE;
}

/*-------------------------------------------------------------------------------------------------------------------------------------
 * GC2015 shotmode capability
 * TODO: if you enable new shot mode, pls add a correspoding modcap function here, and add it to GC2015_shotModeCap _GC2015ShotModeCap array
 *------------------------------------------------------------------------------------------------------------------------------------*/
static void _GC2015AutoModeCap( CAM_ShotModeCapability *pShotModeCap )
{
	int i;

	// exposure mode
	pShotModeCap->iSupportedExpModeCnt  = 1;
	pShotModeCap->eSupportedExpMode[0]  = CAM_EXPOSUREMODE_AUTO;
	
	// exposure metering mode 
	pShotModeCap->iSupportedExpMeterCnt = 1;
	pShotModeCap->eSupportedExpMeter[0] = CAM_EXPOSUREMETERMODE_AUTO;

	/* disable exposure. -guang */
#if 0
	// EV compensation 
	pShotModeCap->iEVCompStepQ16 = 19661;
	pShotModeCap->iMinEVCompQ16 = -78644;
	pShotModeCap->iMaxEVCompQ16 = 78644;
#else
	// EV compensation 
	pShotModeCap->iEVCompStepQ16 = 0;
	pShotModeCap->iMinEVCompQ16 = 0;
	pShotModeCap->iMaxEVCompQ16 = 0;
#endif

	// ISO mode 
	pShotModeCap->iSupportedIsoModeCnt = 1;
	pShotModeCap->eSupportedIsoMode[0] = CAM_ISO_AUTO;

	// shutter speed
	pShotModeCap->iMinShutSpdQ16 = -1;
	pShotModeCap->iMaxShutSpdQ16 = -1;

	// F-number
	pShotModeCap->iMinFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);
	pShotModeCap->iMaxFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);

	// Band filter
	pShotModeCap->iSupportedBdFltModeCnt = _ARRAY_SIZE(_GC2015BdFltMode); 
    for (i=0; i<pShotModeCap->iSupportedBdFltModeCnt; i++)
		pShotModeCap->eSupportedBdFltMode[i] = _GC2015BdFltMode[i].iParameter;

	// Flash mode
	pShotModeCap->iSupportedFlashModeCnt = 1;
	pShotModeCap->eSupportedFlashMode[0] = CAM_FLASH_OFF;

	// white balance mode
	pShotModeCap->iSupportedWBModeCnt = _ARRAY_SIZE(GC2015WBMode);
	for (i=0; i<pShotModeCap->iSupportedWBModeCnt; i++)
		pShotModeCap->eSupportedWBMode[i] = GC2015WBMode[i].iParameter;


	// focus mode
	pShotModeCap->iSupportedFocusModeCnt = 1;
	pShotModeCap->eSupportedFocusMode[0] = CAM_FOCUS_AUTO_ONESHOT;

	// AF mode 
	pShotModeCap->iSupportedAFModeCnt = 0;

	// optical zoom mode
	pShotModeCap->iMinOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);

	// digital zoom mode
	pShotModeCap->iMinDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->bSupportSmoothDigZoom = CAM_FALSE;

	// frame rate
	pShotModeCap->iMinFpsQ16     = 1 << 16;
	pShotModeCap->iMaxFpsQ16     = 40 << 16;

	// color effect
	pShotModeCap->iSupportedColorEffectCnt = _ARRAY_SIZE(GC2015ColorEffectMode);
	for (i=0; i<pShotModeCap->iSupportedColorEffectCnt; i++)
		pShotModeCap->eSupportedColorEffect[i] = GC2015ColorEffectMode[i].iParameter; 

	// brightness
	pShotModeCap->iMinBrightness = 0;
	pShotModeCap->iMaxBrightness = 0;

	// contrast
	pShotModeCap->iMinContrast = 0;
	pShotModeCap->iMaxContrast = 0;

	// saturation
	pShotModeCap->iMinSaturation = 0;
	pShotModeCap->iMaxSaturation = 0;

	// sharpness
	pShotModeCap->iMinSharpness = 0;
	pShotModeCap->iMaxSharpness = 0;

	// advanced features
	pShotModeCap->bSupportVideoStabilizer   = CAM_FALSE;
	pShotModeCap->bSupportVideoNoiseReducer	= CAM_FALSE;
	pShotModeCap->bSupportZeroShutterLag    = CAM_FALSE;
	pShotModeCap->bSupportBurstCapture      = CAM_FALSE;
	pShotModeCap->bSupportHighDynamicRange  = CAM_FALSE;
	pShotModeCap->iMaxBurstCnt              = 1;

	return;
}


/*-------------------------------------------------------------------------------------------------------------------------------------
 * GC2015 shotmode capability
 * TODO: if you enable new shot mode, pls add a correspoding modcap function here, and add it to GC2015_shotModeCap _GC2015ShotModeCap array
 *------------------------------------------------------------------------------------------------------------------------------------*/
static void _GC2015ManualModeCap( CAM_ShotModeCapability *pShotModeCap )
{
	int i;

	// exposure mode
	pShotModeCap->iSupportedExpModeCnt  = 1;
	pShotModeCap->eSupportedExpMode[0]  = CAM_EXPOSUREMODE_AUTO;
	
	// exposure metering mode 
	pShotModeCap->iSupportedExpMeterCnt = 1;
	pShotModeCap->eSupportedExpMeter[0] = CAM_EXPOSUREMETERMODE_AUTO;

	// EV compensation 
	pShotModeCap->iEVCompStepQ16 = 0;
	pShotModeCap->iMinEVCompQ16 = 0;
	pShotModeCap->iMaxEVCompQ16 = 0;

	// ISO mode 
	pShotModeCap->iSupportedIsoModeCnt = 1;
	pShotModeCap->eSupportedIsoMode[0] = CAM_ISO_AUTO;

	// shutter speed
	pShotModeCap->iMinShutSpdQ16 = -1;
	pShotModeCap->iMaxShutSpdQ16 = -1;

	// F-number
	pShotModeCap->iMinFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);
	pShotModeCap->iMaxFNumQ16 = (CAM_Int32s)(2.8 * 65536 + 0.5);

	// Band filter
	pShotModeCap->iSupportedBdFltModeCnt = 1; 
	pShotModeCap->eSupportedBdFltMode[0] = CAM_BANDFILTER_50HZ;

	// Flash mode
	pShotModeCap->iSupportedFlashModeCnt = 1;
	pShotModeCap->eSupportedFlashMode[0] = CAM_FLASH_OFF;

	// white balance mode
	pShotModeCap->iSupportedWBModeCnt = _ARRAY_SIZE(GC2015WBMode);
	for (i=0; i<pShotModeCap->iSupportedWBModeCnt; i++)
		pShotModeCap->eSupportedWBMode[i] = GC2015WBMode[i].iParameter;

	// focus mode
	pShotModeCap->iSupportedFocusModeCnt = 1;
	pShotModeCap->eSupportedFocusMode[0] = CAM_FOCUS_AUTO_ONESHOT;

	// AF mode 
	pShotModeCap->iSupportedAFModeCnt = 0;

	// optical zoom mode
	pShotModeCap->iMinOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxOptZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);

	// digital zoom mode
	pShotModeCap->iMinDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->iMaxDigZoomQ16 = (CAM_Int32s)(1.0 * 65536 + 0.5);
	pShotModeCap->bSupportSmoothDigZoom = CAM_FALSE;

	// frame rate
	pShotModeCap->iMinFpsQ16     = 1 << 16;
	pShotModeCap->iMaxFpsQ16     = 40 << 16;

	// color effect
	pShotModeCap->iSupportedColorEffectCnt = _ARRAY_SIZE(GC2015ColorEffectMode);
	for (i=0; i<pShotModeCap->iSupportedColorEffectCnt; i++)
		pShotModeCap->eSupportedColorEffect[i] = GC2015ColorEffectMode[i].iParameter; 

	// brightness
	pShotModeCap->iMinBrightness = 0;
	pShotModeCap->iMaxBrightness = 0;

	// contrast
	pShotModeCap->iMinContrast = 0;
	pShotModeCap->iMaxContrast = 0;

	// saturation
	pShotModeCap->iMinSaturation = 0;
	pShotModeCap->iMaxSaturation = 0;

	// sharpness
	pShotModeCap->iMinSharpness = 0;
	pShotModeCap->iMaxSharpness = 0;

	// advanced features
	pShotModeCap->bSupportVideoStabilizer   = CAM_FALSE;
	pShotModeCap->bSupportVideoNoiseReducer	= CAM_FALSE;
	pShotModeCap->bSupportZeroShutterLag    = CAM_FALSE;
	pShotModeCap->bSupportBurstCapture      = CAM_FALSE;
	pShotModeCap->bSupportHighDynamicRange  = CAM_FALSE;
	pShotModeCap->iMaxBurstCnt              = 1;

	return;
}

static CAM_Error _GC2015SaveAeAwb( const _CAM_SmartSensorConfig *pOldConfig, void *pUserData )
{
	// TODO: add your sensor specific save 3A function here
	return CAM_ERROR_NONE;
}

static CAM_Error _GC2015RestoreAeAwb( const _CAM_SmartSensorConfig *pNewConfig, void *pUserData )
{
	// TODO: add your sensor specific restore 3A function here
	return CAM_ERROR_NONE;  
}

static CAM_Error _GC2015StartFlash( void *pSensorState )
{
	// TODO: add your sensor specific start flash function here
	return CAM_ERROR_NONE;
}

static CAM_Error _GC2015StopFlash( void *pSensorState )
{
	// TODO: add your sensor specific stop flash function here
	return CAM_ERROR_NONE;
}

static CAM_Error _GC2015ApplyShotParam(void* pUserData)
{
	GC2015State *pState = (GC2015State*) pUserData;

	GC2015SetParamsReg(pState->stV4L2.stShotParam.eWBMode, GC2015WBMode, _ARRAY_SIZE(GC2015WBMode), pState->stV4L2.iSensorFD);

	GC2015SetParamsReg(pState->stV4L2.stShotParam.eColorEffect, GC2015ColorEffectMode, _ARRAY_SIZE(GC2015ColorEffectMode), pState->stV4L2.iSensorFD);

	GC2015SetParamsReg(pState->stV4L2.stShotParam.eBandFilterMode, _GC2015BdFltMode, _ARRAY_SIZE(_GC2015BdFltMode), pState->stV4L2.iSensorFD);

	GC2015SetParamsReg(pState->stV4L2.stShotParam.iEvCompQ16, GC2015EvCompMode, _ARRAY_SIZE(GC2015EvCompMode), pState->stV4L2.iSensorFD);

	return CAM_ERROR_NONE;
}

// get shot info
static CAM_Error _GC2015FillFrameShotInfo( GC2015State *pState, CAM_ImageBuffer *pImgBuf )
{
	// TODO: add real value of these parameters here

	CAM_Error    error      = CAM_ERROR_NONE;
	CAM_ShotInfo *pShotInfo = &(pImgBuf->stShotInfo);

	pShotInfo->iExposureTimeQ16    = (1 << 16) / 30;
	pShotInfo->iFNumQ16            = (int)( 2.8 * 65536 + 0.5 );
	pShotInfo->eExposureMode       = CAM_EXPOSUREMODE_AUTO;
	pShotInfo->eExpMeterMode       = CAM_EXPOSUREMETERMODE_MEAN;
	pShotInfo->iISOSpeed           = 100;
	pShotInfo->iSubjectDistQ16     = 0;
	pShotInfo->iFlashStatus        = 0x0010;
	pShotInfo->iFocalLenQ16        = (int)( 3.79 * 65536 + 0.5 );
	pShotInfo->iFocalLen35mm       = 0;       

	return error;
}
