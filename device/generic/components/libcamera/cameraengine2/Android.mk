LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

CAMERA_ENGINE_TOP := $(LOCAL_PATH)
CAMERA_ENGINE_CFLAGS:=
CAMERA_ENGINE_SHARE_LIBRARIES:=



ifeq ($(TARGET_PRODUCT), saarbmg1)
CAMERA_ENGINE_CFLAGS    += -D MG1SAARB
CAMERA_ENGINE_SHARE_LIBRARIES+=libcodecjpegenc
else
ifeq ($(TARGET_PRODUCT), evbnevo)
CAMERA_ENGINE_CFLAGS    += -D MG1SAARB
CAMERA_ENGINE_SHARE_LIBRARIES+=libcodecjpegenc
else
ifeq ($(TARGET_PRODUCT), saarcnevo)
CAMERA_ENGINE_CFLAGS    += -D MG1SAARB
CAMERA_ENGINE_SHARE_LIBRARIES+=libcodecjpegenc
else
ifeq ($(TARGET_PRODUCT), jasper)
CAMERA_ENGINE_CFLAGS    += -D BONNELL
CAMERA_ENGINE_SHARE_LIBRARIES+=libcodecjpegenc
else
ifeq ($(TARGET_PRODUCT), brownstone)
CAMERA_ENGINE_CFLAGS    += -D BROWNSTONE
CAMERA_ENGINE_SHARE_LIBRARIES+=libcodecjpegenc
else
ifeq ($(TARGET_PRODUCT), abilene)
CAMERA_ENGINE_CFLAGS    += -D ABILENE
CAMERA_ENGINE_SHARE_LIBRARIES+=libcodecjpegenc
else
ifeq ($(TARGET_PRODUCT), dkb)
CAMERA_ENGINE_CFLAGS    += -D TDDKB
CAMERA_ENGINE_SHARE_LIBRARIES+=libcodecjpegenc
else
ifeq ($(TARGET_PRODUCT), evbmg1)
CAMERA_ENGINE_CFLAGS	+= -D MG1EVB
CAMERA_ENGINE_SHARE_LIBRARIES+=libcodecjpegenc
else
ifeq ($(TARGET_PRODUCT), avlite)
CAMERA_ENGINE_CFLAGS    += -D AVLITE
CAMERA_ENGINE_SHARE_LIBRARIES+=libcodecjpegenc
else
ifeq ($(TARGET_PRODUCT), p200)
CAMERA_ENGINE_CFLAGS    += -D P200
CAMERA_ENGINE_SHARE_LIBRARIES+=libcodecjpegenc
else
echo "using default brownstone"
#fallback to brownstone
CAMERA_ENGINE_CFLAGS    += -D BROWNSTONE
CAMERA_ENGINE_SHARE_LIBRARIES+=libcodecjpegenc
endif

endif

endif

endif

endif

endif

endif

endif

endif

endif






################################
## can not descriminate if it's ttc or td, hence we always build as td
################################



# camera-engine lib

include $(CAMERA_ENGINE_TOP)/src/cameraengine2/ansi_c/Android_cameraengine.mk
include $(CAMERA_ENGINE_TOP)/src/cameraengine2/ansi_c/Android_sensorhal.mk

# camera sample application in /system/bin/MrvlCameraDemo
#include device/generic/components/cameraengine2/example/cameraengine2/Android_example.mk

# tool library of camera usage model, including: 
# 1. libippexif to generate exif image from JPEG image
include $(CAMERA_ENGINE_TOP)/tool/lib/Android.mk

# tool library sample application, including:
# 1. MrvlExifGenerator in /system/bin
#include device/generic/components/cameraengine2/tool/example/Android.mk
