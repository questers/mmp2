/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */
/* cameraengine state machine
 **************************************************************************

		    standby
		    |
		    idle
		    |
	video - preview - still

 **************************************************************************
 */

#include <linux/ioctl.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <utils/Timers.h>
#include <codecDef.h>
#include <codecJP.h>
#include <misc.h>
#include <cutils/properties.h>
#include "ippIP.h"
#include "CameraSetting.h"

#include "CameraPreviewBaselaySW.h"
#include "CameraPreviewOverlay.h"
#include "CameraCtrl.h"
#include "Engine.h"
#include "FakeCam.h"
#include "cam_log_mrvl.h"
#include "CameraHardware.h"

#define LOG_TAG "CameraHardware"

#define LIKELY(exp)   __builtin_expect(!!(exp), 1)
#define UNLIKELY(exp) __builtin_expect(!!(exp), 0)

#define PMEM_DEVICE "/dev/pmem_adsp"

/*
 * property
 */
#define PROP_FAKE	"persist.service.camera.fake"	//0,1
#define PROP_CNT	"persist.service.camera.cnt"	//0,>0,<0
#define PROP_HW		"service.camera.hw"	//overlay,base
#define PROP_AF		"service.camera.af"	//0,>0

/*
 * variables for performance optimization
 * show preview/camcorder fps
 */
#define OPTIMIZE

#ifdef DEBUG_TIME
nsecs_t told,delta;
int lineold;
#define TIMETAG do{\
    TRACE_D("************************"); \
    delta=systemTime()-told; \
    TRACE_D("*line:%d - %d,\t dt=%f ms",lineold, __LINE__, double(delta/1000000)); \
    lineold=int(__LINE__); \
    told=systemTime(); \
    TRACED("************************");\
    }while(0)
#else
#define TIMETAG do{}while(0)
#endif

namespace android {

#ifdef OPTIMIZE
static long video_count=0;
static double preview_start=0;
static double video_start=0;
#endif

static int mEnableFake;

CameraHardware::CameraHardware(int cameraID):
                    mRawHeap(NULL),
		    mMsgEnabled(0),
                    mPreviewFrameCount(0),
                    mRecordingEnabled(false),
                    mPreviewDevice(NULL),
                    mOverlay(NULL),
		    mUseOverlay(false),
		    kPreviewBufCnt(6),
		    kStillBufCnt(2),
		    kVideoBufCnt(6),
		    mCamPtr(NULL),
		    mPreviewImageBuf(NULL),
		    mStillImageBuf(NULL),
		    mState(CAM_STATE_IDLE)
{
    FUNC_TAG;

    /*
     * initialize the default parameters for the first time.
     */
    /*
     * get property for overlay/base selection
     * setprop service.camera.hw x
     * x=base or overlay
     */
    char value[PROPERTY_VALUE_MAX];
    property_get(PROP_HW, value, "base");
    if (0 == strcmp(value, "overlay"))
	mUseOverlay = true;
    else
	mUseOverlay = false;
    TRACE_D("%s:"PROP_HW"=%s",__FUNCTION__,value);
    mPagesize = (int)getpagesize();

    if(mEnableFake != 0){
	mCamPtr = new FakeCam(cameraID);
    }
    else{
	mCamPtr = new Engine(cameraID);
    }
    /*
     * get preview/still/video buf cnt from camerasetting,
     * all buf cnt should be more than 2
     * use the default one if not specified.
     */
    int previewbufcnt,stillbufcnt,videobufcnt;
    mCamPtr->getBufCnt(&previewbufcnt,&stillbufcnt,&videobufcnt);
    if(previewbufcnt >= 2)
	kPreviewBufCnt = previewbufcnt;
    if(stillbufcnt >= 2)
	kStillBufCnt = stillbufcnt;
    if(videobufcnt >= 2)
	kVideoBufCnt = videobufcnt;
}

/*
 * stop CE
 * flush all CE buffers
 * free CE file handler
 */
CameraHardware::~CameraHardware()
{
    FUNC_TAG_E;

    //destroy preview devices
    if (mPreviewDevice != NULL){
        mPreviewDevice->flush();
	mPreviewDevice->setOverlay(0);//force clear overlay ref
        mPreviewDevice.clear();
    }
    //destroy camera device
    if (mCamPtr != NULL){
	mCamPtr->stopPreview();
	mCamPtr->unregisterStillBuffers();
	mCamPtr->unregisterPreviewBuffers();
	mCamPtr.clear();
    }
    //destroy still buffers.
    if (mStillImageBuf != NULL)
	mStillImageBuf.clear();
    //destroy preview buffers.
    if (mPreviewImageBuf != NULL)
	mPreviewImageBuf.clear();

    FUNC_TAG_X;
    singleton.clear();
}

sp<IMemoryHeap> CameraHardware::getPreviewHeap() const
{
    FUNC_TAG;
    if (mUseOverlay){
	return mPreviewImageBuf->getBufHeap();
    }
    else{
#ifdef DEBUG_BASE_RGB565
	return mPreviewImageBuf_base->getBufHeap();
#endif
	/*
	 * for base, yuv420p and uyvy format, we post imagebuffer directly to
	 * remove one memcpy.
	 *
	 * 1>yuv420p/uyvy: use preview buffer directly, callback without conversion.
	 * 2>yuv420sp: memcpy to special baseheap buffer, surface do not support this
	 * format temporarily, convert to yuv420p before callback.
	 * 3>rgb565: memcpy to special baseheap buffer, yuv->rgb color conversion before callback.
	 */
	CameraParameters params = mCamPtr->getParameters();
	const char* v = params.getPreviewFormat();

	if(0 == strcmp(v, CameraParameters::PIXEL_FORMAT_YUV420P) ||
		0 == strcmp(v, CameraParameters::PIXEL_FORMAT_YUV422I_UYVY)){
	    return mPreviewImageBuf->getBufHeap();
	}
	//for other format, we have to do color conversion & memcpy,
	//so one another buffer is introduced.
	return mPreviewImageBuf_base->getBufHeap();
    }
}

sp<IMemoryHeap> CameraHardware::getRawHeap() const
{
    FUNC_TAG;
    return mRawHeap;
}

void CameraHardware::setCallbacks(notify_callback notify_cb,
                                      data_callback data_cb,
                                      data_callback_timestamp data_cb_timestamp,
                                      void* user)
{
    FUNC_TAG;
    Mutex::Autolock lock(mLock);
    mNotifyCb = notify_cb;
    mDataCb = data_cb;
    mDataCbTimestamp = data_cb_timestamp;
    mCallbackCookie = user;

    //TODO:sensor instance may only sendout notification msg, no datacallback should be
    //called, so here only set notify_callback;
    //set NULL if no eventnotification needed by sensor instance
    //mCamPtr->setCallbacks();
    mCamPtr->setCallbacks(notify_cb,user);
}

//debug only
#define CHECK_MSG(msg)	do{ \
    if(mMsgEnabled == CAMERA_MSG_ALL_MSGS) \
    {TRACE_V("\tEnabled-msg:CAMERA_MSG_ALL_MSGS");} \
    else if(mMsgEnabled & msg) \
    {TRACE_D("\tEnabled-msg:" #msg );} \
    }while(0)
#if 0
#define CHECK_MSG_ALL() do{ \
    CHECK_MSG(CAMERA_MSG_ERROR); \
    CHECK_MSG(CAMERA_MSG_SHUTTER); \
    CHECK_MSG(CAMERA_MSG_FOCUS); \
    CHECK_MSG(CAMERA_MSG_ZOOM); \
    CHECK_MSG(CAMERA_MSG_PREVIEW_FRAME); \
    CHECK_MSG(CAMERA_MSG_VIDEO_FRAME); \
    CHECK_MSG(CAMERA_MSG_POSTVIEW_FRAME); \
    CHECK_MSG(CAMERA_MSG_RAW_IMAGE); \
    CHECK_MSG(CAMERA_MSG_COMPRESSED_IMAGE); \
	}while(0)
#else
#define CHECK_MSG_ALL() do{}while(0)
#endif
void CameraHardware::enableMsgType(int32_t msgType)
{
    TRACE_V("-%s:0x%x",__FUNCTION__,msgType);
    Mutex::Autolock lock(mLock);
    mMsgEnabled |= msgType;
    //TODO:if notification callback is not enabled in sensor instance,
    //then msgtype is not necessary
    mCamPtr->enableMsgType(msgType);
    CHECK_MSG_ALL();
}

void CameraHardware::disableMsgType(int32_t msgType)
{
    TRACE_V("-%s:0x%x",__FUNCTION__,msgType);
    Mutex::Autolock lock(mLock);
    mMsgEnabled &= ~msgType;
    mCamPtr->disableMsgType(msgType);
    CHECK_MSG_ALL();
}

bool CameraHardware::msgTypeEnabled(int32_t msgType)
{
    TRACE_V("-%s:0x%x",__FUNCTION__,msgType);
    Mutex::Autolock lock(mLock);
    return (mMsgEnabled & msgType);
}
#undef CHECK_MSG

int CameraHardware::previewThread()
{
    TRACE_V("-%s e", __FUNCTION__);
    /*
     * show preview/camcorder buf status, for debug purpose only
     */
    mBuffersLock.lock();
    //mPreviewImageBuf->showStatus();

    /*
     * loop all preview buf status
     * 1.increase ref count
     * 2.if free, enqueue to driver
     */
    //loop all buf and enqueue free buf to engine
    for (int i = 0; i < kPreviewBufCnt; i++) {
	if(mPreviewImageBuf->isFree(i)){
	    status_t stat=mCamPtr->enqPreviewBuffer(i);
	    if(NO_ERROR == stat)
		mPreviewImageBuf->toDriver(i);
	    else
		TRACE_E("enqPreviewBuffer fail!");
	}
    }
    /*
     * sum over all buf in driver.
     * if 0, flush overlay buf, wait and loop back the previewthread.
     * or else, dequeue from CE
     */
    int preview,still,video,driver;
    int driver_bufcnt=0;
    for (int i = 0; i < kPreviewBufCnt; i++) {
	mPreviewImageBuf->getRef(i,&preview,&still,&video,&driver);
	driver_bufcnt += driver;
    }
    mBuffersLock.unlock();//unlock before return

    if(driver_bufcnt <= 1){//we must leave at least 1 buf in v4l2 for user-pointer
	TRACE_D("%s:Driver waiting for buffer...",__FUNCTION__);
	mPreviewImageBuf->showStatus();
        /*
	 * TODO:
	 * flush preview buf, push overlay to call 'free' callback
	 * as to encoder buf, we can do nothing but waiting for the callback
	 */
	/*
	 * NOTES: mBuffersLock should be locked before call waitrelative
	 */
	mBuffersLock.lock();
	mBuffersCond.waitRelative(mBuffersLock, 20000000);//20ms timeout
	mBuffersLock.unlock();//unlock before return
	return NO_ERROR;
    }

    /*
     * at least one buf could be dequeue from engine
     */
    int index = -1;
    status_t error = NO_ERROR;

    error = mCamPtr->deqPreviewBuffer(&index);

    if (LIKELY(CAM_ERROR_NONE == error)) {
	mBuffersLock.lock();
	mPreviewImageBuf->fromDriver(index);
	mBuffersLock.unlock();

	mPreviewFrameCount++;
        sp<IMemory> buffer = mPreviewImageBuf->getIMemory(index);

	/*
	 * preview callback to surfaceflinger or overlay
	 */
	if (LIKELY(mOverlay.get() != NULL || mUseOverlay == false)){
	    if (LIKELY(mUseOverlay || mMsgEnabled & CAMERA_MSG_PREVIEW_FRAME)) {
		/*
		 * If we don't use overlay, we always need the preview frame for display.
		 * If we do use overlay, we only need the preview frame if the user
		 * wants the data.
		 */
		status_t ret=NO_ERROR;
		//increase the ref cnt beforehand
		mBuffersLock.lock();
		mPreviewImageBuf->toPreview(index);
		mBuffersLock.unlock();
		//for overlay & baselay display
		ret=mPreviewDevice->display(mPreviewImageBuf, index);

		//decrease the ref cnt if fail
		if(NO_ERROR != ret){
		    mBuffersLock.lock();
		    mPreviewImageBuf->fromPreview(index);
		    mBuffersLock.unlock();
		}
	    }
	    else {
		TRACE_E("CAMERA_MSG_PREVIEW_FRAME is disabled in preview mode");
	    }
	}
	//for user required overlay data callback only
	if(mUseOverlay && mMsgEnabled & CAMERA_MSG_PREVIEW_FRAME){
		ssize_t offset = 0;
		size_t size = 0;
		buffer->getMemory(&offset, &size);

	    mDataCb(CAMERA_MSG_PREVIEW_FRAME, buffer,
		    mCallbackCookie);
	}

	/*
	 * recorder callback to opencore
	 */
	if (mRecordingEnabled){
	    if (mMsgEnabled & CAMERA_MSG_VIDEO_FRAME) {
		#ifdef OPTIMIZE
		video_count++;
		#endif
		nsecs_t timestamp = systemTime();
		mBuffersLock.lock();
		mPreviewImageBuf->toVideo(index);
		mBuffersLock.unlock();
		mDataCbTimestamp(timestamp, CAMERA_MSG_VIDEO_FRAME,
			buffer, mCallbackCookie);
	    }
	    else {
		TRACE_E("CAMERA_MSG_VIDEO_FRAME is disabled in recording mode");
	    }
	}
    }
    else{
        TRACE_E("Fail to dequeue buf from CE... sth. must be wrong...");
    }
    usleep(1);

    TRACE_V("-%s x", __FUNCTION__);
    return NO_ERROR;
}

/*
 * stop CE to idle,
 * alloc buf & enqueue all CE buf
 * create preview thread
 */
status_t CameraHardware::startPreview()
{
    FUNC_TAG_E;

    mLock.lock();

    if (mPreviewThread != 0) {
	TRACE_E("previous mPreviewThread not clear");
	mLock.unlock();
	return INVALID_OPERATION;
    }
    mPreviewFrameCount = 0;

    CameraParameters params = mCamPtr->getParameters();
    int w,h;
    params.getPreviewSize(&w, &h);
    const char* format = params.getPreviewFormat();

    int previous_w,previous_h;
    const char* previous_format;
    mBuffersLock.lock();
    if(mPreviewImageBuf != NULL){
	previous_format = mPreviewImageBuf->getImageFormat();
	mPreviewImageBuf->getImageSize(&previous_w,&previous_h);
    }

    if(CAM_STATE_STILL == mState &&
	    previous_w == w &&
	    previous_h == h &&
	    !strcmp(format, previous_format)){//still->preview, and preview param not changed.
	mPreviewImageBuf->showStatus();

	//loop all buf and enqueue free buf to engine
	for (int i = 0; i < kPreviewBufCnt; i++) {
	    //increase preview/recorder buf ref count
	    if(mPreviewImageBuf->isFree(i)){
		status_t stat = mCamPtr->enqPreviewBuffer(i);
		if(NO_ERROR == stat)
		    mPreviewImageBuf->toDriver(i);
		else
		    TRACE_E("enqPreviewBuffer fail!");
	    }
	}
    }
    else{//idle->preview, or preview param changed.
	mPreviewImageBuf.clear();
	mPreviewImageBuf = new ImageBuf();
	status_t ret = mPreviewImageBuf->AllocImage(
		format,
		Size(w,h),
		kPreviewBufCnt,
		PMEM_DEVICE
		);

	mCamPtr->stopPreview();
	status_t stat=mCamPtr->registerPreviewBuffer(mPreviewImageBuf);
	if(NO_ERROR != stat)
	    TRACE_E("registerPreviewBuffer fail!");
	for(int i=0;i<kPreviewBufCnt;i++){
	    mPreviewImageBuf->toDriver(i);
	}
    }
    mBuffersLock.unlock();

    /*
     * TODO:we use dedicated image buffer for baselayer preview,
     * this solution is not much elegant.
     */
    //
    if (!mUseOverlay){
	TRACE_D("%s:prepare baselayer buffer",__FUNCTION__);
	mPreviewImageBuf_base = new ImageBuf();
	status_t ret = mPreviewImageBuf_base->AllocImage(
		CameraParameters::PIXEL_FORMAT_RGB565,
		Size(w,h),
		1
		);//use ashmem for baselayer preview
    }

    if (NO_ERROR != mCamPtr->startPreview()){
	mLock.unlock();
	TRACE_E("Fail to startPreview.");
	return -1;
    }

    //FIXME:check the lock location, it should be put below?
    mLock.unlock(); // release mlock before calling outside

    //reset preview device & recycle buf
    if(mPreviewDevice != NULL){
	// flush & recycle all buffers in display
	mPreviewDevice->flush();
    }
    else{
	if (mUseOverlay)
	    mPreviewDevice = new CameraPreviewOverlay(this);
	else
	    mPreviewDevice = new CameraPreviewBaselaySW(this);
    }
    mPreviewDevice->setOverlay(mOverlay);
    mPreviewDevice->setPreviewCallback(mDataCb, mCallbackCookie, mPreviewImageBuf);

#ifdef OPTIMIZE
    preview_start = systemTime();
#endif

    mState = CAM_STATE_PREVIEW;
    mPreviewImageBuf->showStatus();
    mPreviewThread = new PreviewThread(this);
    FUNC_TAG_X;
    return NO_ERROR;
}

bool CameraHardware::useOverlay()
{
    FUNC_TAG;
    if(mUseOverlay){
        TRACE_D("use overlay for preview");
        return true;
    }
    TRACE_D("DONT use overlay for preview");
    return false;
}

status_t CameraHardware::setOverlay(const sp<Overlay> &overlay)
{
    FUNC_TAG;
    TRACE_D("%s: overlay=%p",__FUNCTION__,(overlay==NULL)? 0 : overlay.get());
    mOverlay = overlay;
    if (mPreviewDevice != NULL){
	mPreviewDevice->setOverlay(overlay);
    }
    return NO_ERROR;
}

/*
 * this function maybe called multi-times by cameraservice.
 * this function maybe called without calling startpreview beforehand.
 */
void CameraHardware::stopPreview()
{
    FUNC_TAG_E;

    // Ellie: we need to cancel the auto focus thread before leaving preview state,
    //         otherwise it may keep running even after camerahardware is destroyed
    if(mState==CAM_STATE_PREVIEW )
    {
        mLock.lock();
        mState=CAM_STATE_PREVIEW_TO_IDLE;
        cancelAutoFocus();
        mLock.unlock();
    }
	
    __stopPreviewThread();

    mCamPtr->stopPreview();
    mCamPtr->unregisterPreviewBuffers();
    mBuffersLock.lock();
    if(mPreviewImageBuf != NULL){
	mPreviewImageBuf->showStatus();
	for (int i = 0; i < kPreviewBufCnt; i++) {
	    mPreviewImageBuf->setFree(i);
	}
	//mPreviewImageBuf.clear();//FIXME
    }
    mBuffersLock.unlock();

    mState = CAM_STATE_IDLE;

    FUNC_TAG_X;
}

//donot stop sensor preview, only cancel the preview thread
void CameraHardware::__stopPreviewThread()
{
    FUNC_TAG_E;

    #ifdef OPTIMIZE
    double preview_dur = (systemTime() - preview_start)/1000000000;
    if (preview_dur > 0 && mPreviewFrameCount > 10 && preview_start > 0){
	TRACE_D("preview_dur=%f,preview_count=%d",preview_dur,mPreviewFrameCount);
	long preview_fps = (long)(mPreviewFrameCount/preview_dur);
	preview_start = 0;
	TRACE_D("preview fps=%ld", preview_fps);
    }
    #endif
    mPreviewFrameCount = 0;

    sp<PreviewThread> previewThread;
    {//scope for the lock
        Mutex::Autolock lock(mLock);
        previewThread = mPreviewThread;
    }

    //don't hold the lock while waiting for the thread to quit
    if (previewThread != 0) {
        TRACE_D("%s(pid=%d): request preview thread to exit",
		__FUNCTION__, gettid());
        previewThread->requestExitAndWait();
    }

    {
	Mutex::Autolock lock(mLock);
	mPreviewThread.clear();
    }
    FUNC_TAG_X;
}

bool CameraHardware::previewEnabled()
{
    return (mPreviewThread != 0);
}

status_t CameraHardware::startRecording()
{
    FUNC_TAG;
    mBuffersLock.lock();
    mPreviewImageBuf->showStatus();
    mBuffersLock.unlock();
    Mutex::Autolock lock(mLock);
    mRecordingEnabled = true;

    #ifdef OPTIMIZE
    video_start = systemTime();
    video_count = 0;
    #endif
    /*
     * notify preview device that recording is on
     */
    mPreviewDevice->startRecording();
    return NO_ERROR;
}

void CameraHardware::stopRecording()
{
    FUNC_TAG;
    Mutex::Autolock lock(mLock);
    mRecordingEnabled = false;

    #ifdef OPTIMIZE
    double video_dur = (systemTime() - video_start)/1000000000;
    if( video_dur > 0 && video_count > 0 ){
	long video_fps = (long)(video_count/video_dur);
	video_count = 0;
	video_start = 0;
	TRACE_D("video fps = %ld", video_fps);
    }
    #endif

    /*
     * notify preview device that recording is off
     */
    mPreviewDevice->stopRecording();
    mBuffersLock.lock();
    mPreviewImageBuf->showStatus();

    //TODO:recycle all recording buffers when stoprecording
    int preview,still,video,driver;
    int driver_bufcnt=0;
    for (int i = 0; i < kPreviewBufCnt; i++) {
	mPreviewImageBuf->getRef(i,&preview,&still,&video,&driver);
	if(0 < video){
	    mPreviewImageBuf->fromVideo(i);
	}
    }
    mBuffersLock.unlock();
}

void CameraHardware::releasePreviewBuffer(int index)
{
    if (index>=0 && index<kPreviewBufCnt) {
	Mutex::Autolock buflock(mBuffersLock);
        TRACE_V("%s: buffer #%d returned", __FUNCTION__, index);
	mPreviewImageBuf->fromPreview(index);
	mBuffersCond.signal();
    }
    else {
        TRACE_E("invalid index %d", index);
    }
}

void CameraHardware::releaseRecordingFrame(const sp<IMemory>& mem)
{
    int index;

    index = mPreviewImageBuf->getBufIndex(mem);

    if (index >= 0 && index < kPreviewBufCnt) {
	Mutex::Autolock buflock(mBuffersLock);
        TRACE_V("%s: buffer #%d returned", __FUNCTION__, index);
	mPreviewImageBuf->fromVideo(index);
	mBuffersCond.signal();
    }
    else {
        TRACE_E("invalid index %d", index);
    }
}

int CameraHardware::beginAutoFocusThread(void *cookie)
{
    FUNC_TAG;
    CameraHardware *c = (CameraHardware *)cookie;
    return c->autoFocusThread();
}

status_t CameraHardware::autoFocusThread()
{
    int cesetting                           = -1;
    int iEnableAutoFocus                    = 0;
    char value[PROPERTY_VALUE_MAX]          = {0};

    property_get(PROP_AF, value, "0");
    TRACE_D("%s:"PROP_AF"=%s",__FUNCTION__, value);
    iEnableAutoFocus = atoi(value);
    if(iEnableAutoFocus == 0){
	mNotifyCb(CAMERA_MSG_FOCUS, true, 0, mCallbackCookie);
	return NO_ERROR;
    }

    // Ellie: don't start auto focus thread after preview is stopped
    Mutex::Autolock lock(mLock);

    if(mState!=CAM_STATE_PREVIEW)
    {
        TRACE_E("auto focus forbidden in non-preview state");
        return NO_ERROR;
    }

    status_t stat = mCamPtr->autoFocus();
    if(NO_ERROR != stat){
	TRACE_E("focus failed.");
	mNotifyCb(CAMERA_MSG_FOCUS, false, 0, mCallbackCookie);
    }
    return NO_ERROR;
}

status_t CameraHardware::autoFocus()
{
    FUNC_TAG;
    Mutex::Autolock lock(mLock);

    if ((mNotifyCb == NULL) || !(mMsgEnabled & CAMERA_MSG_FOCUS)){
        TRACE_E("mNotifyCb not set or CAMERA_MSG_FOCUS not enabled for autofocus");
        return INVALID_OPERATION;
    }

    //NOTES:donot trigger fake focus callback in MAIN thread, or else it will call deadlock.
    //but it should be safe if real focus is trigger.
    //return beginAutoFocusThread(this);
    if (createThreadEtc(beginAutoFocusThread, this,
		"focusthread", PRIORITY_URGENT_DISPLAY) == false){
        TRACE_E("fail to create autofocus thread");
        return UNKNOWN_ERROR;
    }
    return NO_ERROR;
}

status_t CameraHardware::cancelAutoFocus()
{
    FUNC_TAG;
    int iEnableAutoFocus                    = 0;
    char value[PROPERTY_VALUE_MAX]          = {0};

    property_get(PROP_AF, value, "0");
    TRACE_D("%s:"PROP_AF"=%s",__FUNCTION__, value);
    iEnableAutoFocus = atoi(value);
    if(iEnableAutoFocus == 0){
	return NO_ERROR;
    }

    return mCamPtr->cancelAutoFocus();
}

int CameraHardware::beginPictureThread(void *cookie)
{
    FUNC_TAG;
    CameraHardware *c = (CameraHardware *)cookie;
    return c->pictureThread();
}

/*
 * when enter picturethread,
 * CE should be at STILL state,
 * all free preview buf should have been enqueue to preview port for snapshot
 * when exit,
 * CE should be at preview state,
 * preview buf status are recorder by bufstate[]
 */
int CameraHardware::pictureThread()
{
    FUNC_TAG_E;

    #ifdef OPTIMIZE
	double pic_deq,pic_exif,pic_cb;
    double pic_start = systemTime();
    #endif

    status_t ret_code=NO_ERROR;
    sp<IMemory> buffer = NULL;
    int bufidx=-1;
    int buflen=-1;
    status_t stat;
    stat = mCamPtr->deqStillBuffer(&bufidx);
    mCamPtr->stopFlash();
	
#ifdef OPTIMIZE
    pic_deq = systemTime();
    TRACE_D("still dequeue duration=%f ms", (pic_deq - pic_start)/1000000);
#endif

    mBuffersLock.lock();
    mStillImageBuf->setFree(bufidx);
    mBuffersLock.unlock();
    //dequeue still fail, return directly.
    if(NO_ERROR != stat || bufidx == -1){
	TRACE_E("deqStillBuffer failed");
	TRACE_D("%s:NULL jpeg will be delivered.",__FUNCTION__);
	buffer = NULL;
	ret_code = UNKNOWN_ERROR;
    }
    else{//still buf generated, try exif based on it
	buffer = mCamPtr->getExifImage(bufidx);
	
#ifdef OPTIMIZE
    pic_exif = systemTime();
    TRACE_D("still exif duration=%f ms", (pic_exif - pic_deq)/1000000);
#endif

	//exif fail, return still image
	if( buffer == NULL ){
	    TRACE_E("getExifImage failed");
	    TRACE_D("%s:jpeg will be delivered.",__FUNCTION__);
	    mBuffersLock.lock();
	    buffer = mStillImageBuf->getIMemory(bufidx);
	    mBuffersLock.unlock();
	    ret_code = UNKNOWN_ERROR;
	}
	else{
	    TRACE_D("%s:exif will be delivered.",__FUNCTION__);
	    ret_code = NO_ERROR;
	}
    }
    if (mMsgEnabled & CAMERA_MSG_COMPRESSED_IMAGE) {
	TRACE_D("%s:CAMERA_MSG_COMPRESSED_IMAGE",__FUNCTION__);
	mDataCb(CAMERA_MSG_COMPRESSED_IMAGE,
		buffer,
		mCallbackCookie);
    }

#ifdef OPTIMIZE
    pic_cb = systemTime();
    TRACE_D("still callback duration=%f ms", (pic_cb - pic_exif)/1000000);
#endif

    /*
    //postview callback is not handled by camera apk layer
    if (mMsgEnabled & CAMERA_MSG_POSTVIEW_FRAME){
	TRACE_D("%s:CAMERA_MSG_POSTVIEW_FRAME",__FUNCTION__);
	mDataCb(CAMERA_MSG_POSTVIEW_FRAME,
		NULL,
		mCallbackCookie);
    }
    //raw data callback is not handled by cameraservice when use overlay
    if (mMsgEnabled & CAMERA_MSG_RAW_IMAGE){
	TRACE_D("%s:CAMERA_MSG_RAW_IMAGE",__FUNCTION__);
	mDataCb(CAMERA_MSG_RAW_IMAGE,
		NULL,
		mCallbackCookie);
    }
    */

    #ifdef OPTIMIZE
    double pic_dur = (systemTime() - pic_start)/1000000;
    if (pic_dur > 0){
	TRACE_D("still catpure duration=%f ms", pic_dur);
    }
    #endif

    //free still buf here, to avoid out-of-memory issue
    mCamPtr->unregisterStillBuffers();
    mBuffersLock.lock();
    for (int i = 0; i < kPreviewBufCnt; i++) {
	mStillImageBuf->setFree(i);
    }
    mBuffersLock.unlock();
    //mStillImageBuf.clear();

    FUNC_TAG_X;
    return ret_code;
}

status_t CameraHardware::takePicture()
{
    FUNC_TAG;
    bool flash_was_on = mCamPtr->isFlashOn();
    mCamPtr->startFlash();
    if((flash_was_on == false) && (mCamPtr->isFlashOn() == true))
	    usleep(500*1000);
	
    /*
     * DONOT stop ce preview, switch to still caputre mode directly,
     * BUT we do need to stop the camera-hal preview thread.
     * all preview bufs are flushed out.
     * Check the preview thread status on next StartPreview() calling
     */
    __stopPreviewThread();//XXX

    //for thumbnail
    mBuffersLock.lock();
    for (int i = 0; i < kPreviewBufCnt; i++) {
	//increase preview/recorder buf ref count
	if(mPreviewImageBuf->isFree(i)){
	    status_t stat=mCamPtr->enqPreviewBuffer(i);
	    if(NO_ERROR == stat)
		mPreviewImageBuf->toDriver(i);
	    else
		TRACE_E("enqPreviewBuffer fail!");
	}
    }
    mBuffersLock.unlock();

    CameraParameters params = mCamPtr->getParameters();
    int w,h;
    params.getPictureSize(&w, &h);
    const char* format = params.getPictureFormat();

    mStillImageBuf = new ImageBuf();
    status_t ret = mStillImageBuf->AllocImage(
	    format,
	    Size(w,h),
	    kStillBufCnt,
	    PMEM_DEVICE
	    );

    //register still buffer
    status_t stat=mCamPtr->registerStillBuffer(mStillImageBuf);
    if(NO_ERROR != stat)
	TRACE_E("registerStillBuffer fail!");
    mBuffersLock.lock();
    for(int i=0;i<kStillBufCnt;i++){
	mStillImageBuf->toDriver(i);
    }
    mBuffersLock.unlock();

    //enqueue preview buffer for snapshot
    mBuffersLock.lock();
    for (int i = 0; i < kPreviewBufCnt; i++) {
	if(mPreviewImageBuf->isFree(i)){
	    status_t stat = mCamPtr->enqPreviewBuffer(i);
	    if(NO_ERROR == stat)
		mPreviewImageBuf->toDriver(i);
	    else
		TRACE_E("enqPreviewBuffer fail!");
	}
    }
    mBuffersLock.unlock();
    /*
     * switch sensor from preview to capture mode,
     * prepare for capture thread
     */
    mState = CAM_STATE_STILL;
    if (createThread(beginPictureThread, this) == false)
    {
        mCamPtr->stopFlash();
        return -1;
    }

    return NO_ERROR;
}

status_t CameraHardware::cancelPicture()
{
    FUNC_TAG;
    return NO_ERROR;
}

status_t CameraHardware::dump(int fd, const Vector<String16>& args) const
{
    FUNC_TAG;
    const size_t SIZE = 256;
    char buffer[SIZE];
    String8 result;
    AutoMutex lock(&mLock);
    write(fd, result.string(), result.size());
    return NO_ERROR;
}

//NOTES: setparameters may not be called during some camera usage.
status_t CameraHardware::setParameters(const CameraParameters& params)
{
    FUNC_TAG;
    Mutex::Autolock lock(mLock);
    TRACE_D("%s:(%s)",__FUNCTION__,params.flatten().string());
    return mCamPtr->setParameters(params);
}

CameraParameters CameraHardware::getParameters() const
{
    FUNC_TAG;
    Mutex::Autolock lock(mLock);
    CameraParameters params = mCamPtr->getParameters();
    TRACE_D("%s:(%s)",__FUNCTION__,params.flatten().string());
    return params;
}

void CameraHardware::release()
{
    FUNC_TAG;
}

/*
 * -----------------------------------------------------------
 */
wp<CameraHardwareInterface> CameraHardware::singleton;

sp<CameraHardwareInterface> CameraHardware::createInstance()
{
    FUNC_TAG;
    if (singleton != 0) {
        sp<CameraHardwareInterface> hardware = singleton.promote();
        if (hardware != 0) {
            return hardware;
        }
    }
    sp<CameraHardwareInterface> hardware(new CameraHardware());
    singleton = hardware;
    return hardware;
}

extern "C" sp<CameraHardwareInterface> openCameraHardware()
{
    FUNC_TAG;
    return CameraHardware::createInstance();
}

/*
 * -----------------------------------------------------------
 */
wp<CameraHardwareInterface> CameraHardware::multiInstance[MAX_CAMERAS];

extern sp<CameraHardware> getInstance(wp<CameraHardwareInterface> camera){
    if (camera != 0) {
	sp<CameraHardwareInterface> hardware = camera.promote();
	if (hardware != 0) {
	    //'dynamic_cast' not permitted with -fno-rtti,
	    //so here we use static_cast instead.
	    return static_cast<CameraHardware*>(hardware.get());
	}
    }
    TRACE_E("CameraHardware instace already destroyed,promote failed!");
    return NULL;
}

extern "C" int HAL_getNumberOfCameras()
{
    FUNC_TAG;
    /*
     * if set to nonzero, then enable fake camera, for debug purpose only.
     */
    //DEBUG property: persist.service.camera.fake,
    //if set to 0, do not use fake camera
    //else, use fake camera
    //default: 0;
    char value[PROPERTY_VALUE_MAX];
    property_get(PROP_FAKE, value, "0");
    mEnableFake = atoi(value);
    TRACE_D("%s:"PROP_FAKE"=%d",__FUNCTION__, mEnableFake);

    if(mEnableFake != 0){
	TRACE_D("%s:Enable fake camera!",__FUNCTION__);
	int fakecnt = FakeCam::getNumberOfCameras();
	return fakecnt;
    }

    //DEBUG property: persist.service.camera.cnt,
    //if set to "0", do not probe sensor, return 0;
    //if set to positive, probe sensor, return specified value;
    //if set to negative, probe sensor, return probed sensor count;
    //default: -1;
    property_get(PROP_CNT, value, "-1");
    TRACE_D("%s:"PROP_CNT"=%s",__FUNCTION__,value);
    const int dbg_sensorcnt=atoi(value);

    if (dbg_sensorcnt == 0){
	TRACE_D("%s:use fake cnt=%d",__FUNCTION__,dbg_sensorcnt);
	return dbg_sensorcnt;
    }

    // get the camera engine handle
    int sensorcnt = Engine::getNumberOfCameras();

    if (dbg_sensorcnt > 0){
	TRACE_D("%s:actually probed sensor cnt=%d; but use fake sensor cnt=%d",
		__FUNCTION__,sensorcnt,dbg_sensorcnt);
	return dbg_sensorcnt;
    }

    TRACE_D("%s:use actually probed sensor cnt=%d",__FUNCTION__,sensorcnt);
    return sensorcnt;
}

extern "C" void HAL_getCameraInfo(int cameraId, struct CameraInfo* cameraInfo)
{
    FUNC_TAG;
    if(mEnableFake != 0){
	FakeCam::getCameraInfo(cameraId, cameraInfo);
    }
    else{
	Engine::getCameraInfo(cameraId, cameraInfo);
    }
}

sp<CameraHardwareInterface> CameraHardware::createInstance(int cameraId)
{
    FUNC_TAG;
    TRACE_D("%s:cameraId=%d",__FUNCTION__,cameraId);
    if( MAX_CAMERAS<=cameraId )
    {
	TRACE_E("only suppport %d cameras, required %d",
		MAX_CAMERAS,cameraId);
	return NULL;
    }
    if (multiInstance[cameraId] != 0) {
        sp<CameraHardwareInterface> hardware = multiInstance[cameraId].promote();
        if (hardware != 0) {
            return hardware;
        }
    }
    sp<CameraHardwareInterface> hardware(new CameraHardware(cameraId));
    multiInstance[cameraId] = hardware;
    return hardware;
}

extern "C" sp<CameraHardwareInterface> HAL_openCameraHardware(int cameraId)
{
    TRACE_D("%s:cameraId=%d",__FUNCTION__,cameraId);
    return CameraHardware::createInstance(cameraId);
}

}; // namespace android

