#ifndef ANDROID_HARDWARE_CAMERA_LOG_MRVL_H
#define ANDROID_HARDWARE_CAMERA_LOG_MRVL_H

#include "cam_log_mrvl.h"

/*
 * local debug info
 */
#define FUNC_TAG    do{LOGD("-%s", __FUNCTION__);}while(0)
#define FUNC_TAG_E  do{LOGD("-%s e", __FUNCTION__);}while(0)
#define FUNC_TAG_X  do{LOGD("-%s x", __FUNCTION__);}while(0)

#define TRACE_E(...)\
do \
{\
    LOGE("ERROR: %s:%s:%d",__FUNCTION__,__FILE__,__LINE__); \
    LOGE(__VA_ARGS__); \
} while(0);

#define TRACE_D(...)\
do \
{\
    LOGD(__VA_ARGS__); \
} while(0);

#define TRACE_V(...)\
do \
{\
    LOGV(__VA_ARGS__); \
} while(0);


//debug macro

/*
 * used to check rgb565 baselayer preview, which is supposed to be supported by surface by default.
 * notes: we only support yuv420p->rgb565 conversion, so yuv40p output should be used when enabling this macro,
 * and cameraservice should be changed accordingly.
 */
//#define DEBUG_BASE_RGB565

#endif
