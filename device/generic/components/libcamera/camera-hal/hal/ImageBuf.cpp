/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

#include <utils/Log.h>
#include <linux/ioctl.h>
#include "cam_log_mrvl.h"

#include "ImageBuf.h"
#define LOG_TAG "ImageBuf"

namespace android {

    ImageBuf::~ImageBuf()
    {
	//heap info (for all mem)
	for(int i=0;i<MAX_BUFCNT;i++){
	    mBuffers[i].clear();
	}
	mBufHeap.clear();
    }
    ImageBuf::ImageBuf(){
	mAlignSize = (int)getpagesize();
	//at least 32byte align. mAlignSize = 32;
	mBufCnt = 0;
	mBufLength = 0;
	mBufLength_align = 0;
	String8 mImageFormat=String8("");
    }
    /*
    ImageBuf::ImageBuf(const char* imageformat, Size size, int bufcnt){
	mAlignSize = (int)getpagesize();
	status_t t=AllocImage(imageformat,size,bufcnt);
	if(NO_ERROR != t){
	    TRACE_E("AllocImage failed!");
	    exit(-1);
	}
    }
    */
    status_t ImageBuf::AllocImage(const char* imageformat, Size size,
	    int bufcnt, const char* BufNode)
    {
	//XXX:clean all previously allocated buffers.
	mBufHeap.clear();
	for(int i=0;i<MAX_BUFCNT;i++){
	    mBuffers[i].clear();
	    /*
	     * reset all buffer status
	     */
	    setFree(i);
	}

	//
	mImageFormat = String8(imageformat);
	mSize = size;
	mBufCnt = bufcnt;
	//check input variables
	if(0>=mSize.width || 0>=mSize.height){
	    TRACE_E("Invalid size: %dx%d", mSize.width, mSize.height);
	    return BAD_VALUE;
	}
	if(mBufCnt>MAX_BUFCNT){
	    TRACE_E("Invalid bufcnt: %d, max: %d", mBufCnt, MAX_BUFCNT);
	    return BAD_VALUE;
	}
	//check image format & init image basic info
	if(mImageFormat == String8(CameraParameters::PIXEL_FORMAT_YUV420P)){
	    mBufLength = mSize.width * mSize.height * 3 / 2;
	    iStep[0] = mSize.width;
	    iStep[1] = mSize.width / 2;
	    iStep[2] = mSize.width / 2;
	    iAllocLen[0] = mSize.width * mSize.height;
	    iAllocLen[1] = iAllocLen[0]/4;
	    iAllocLen[2] = iAllocLen[0]/4;
	    iOffset[0] = 0;
	    iOffset[1] = 0;
	    iOffset[2] = 0;
	}
	else if(mImageFormat == String8(CameraParameters::PIXEL_FORMAT_YUV420SP)){
	    mBufLength = mSize.width * mSize.height * 3 / 2;
	    iStep[0] = mSize.width;
	    iStep[1] = mSize.width;
	    iStep[2] = 0;
	    iAllocLen[0] = mSize.width * mSize.height;
	    iAllocLen[1] = iAllocLen[0]/2;
	    iAllocLen[2] = 0;
	    iOffset[0] = 0;
	    iOffset[1] = 0;
	    iOffset[2] = 0;
	}
	else if(mImageFormat == String8(CameraParameters::PIXEL_FORMAT_YUV422I_UYVY)){
	    mBufLength = mSize.width * mSize.height * 2;
	    iStep[0] = mSize.width * 2;
	    iStep[1] = 0;
	    iStep[2] = 0;
	    iAllocLen[0] = mSize.width * mSize.height * 2;
	    iAllocLen[1] = 0;
	    iAllocLen[2] = 0;
	    iOffset[0] = 0;
	    iOffset[1] = 0;
	    iOffset[2] = 0;
	}
	else if(mImageFormat == String8(CameraParameters::PIXEL_FORMAT_YUV422P)){
	    mBufLength = mSize.width * mSize.height * 2;
	    iStep[0] = mSize.width;
	    iStep[1] = mSize.width/2;
	    iStep[2] = mSize.width/2;
	    iAllocLen[0] = mSize.width * mSize.height;
	    iAllocLen[1] = iAllocLen[0]/2;
	    iAllocLen[2] = iAllocLen[0]/2;
	    iOffset[0] = 0;
	    iOffset[1] = 0;
	    iOffset[2] = 0;
	}
	else if(mImageFormat == String8(CameraParameters::PIXEL_FORMAT_RGB565)){
	    mBufLength = mSize.width * mSize.height * 2;
	    iStep[0] = mSize.width * 2;
	    iStep[1] = 0;
	    iStep[2] = 0;
	    iAllocLen[0] = mSize.width * mSize.height * 2;
	    iAllocLen[1] = 0;
	    iAllocLen[2] = 0;
	    iOffset[0] = 0;
	    iOffset[1] = 0;
	    iOffset[2] = 0;
	}
	else if(mImageFormat == String8(CameraParameters::PIXEL_FORMAT_JPEG)){
	    mBufLength = mSize.width * mSize.height / 2;
	    iStep[0] = 0;
	    iStep[1] = 0;
	    iStep[2] = 0;
	    iAllocLen[0] = mBufLength;
	    iAllocLen[1] = 0;
	    iAllocLen[2] = 0;
	    iOffset[0] = 0;
	    iOffset[1] = 0;
	    iOffset[2] = 0;
	}
	else{
	    TRACE_E("Invalid imageformat: %s", mImageFormat.string());
	    return BAD_VALUE;
	}

	//buffer alignment, at least 32byte align
	mBufLength_align = ((mBufLength + mAlignSize-1) & ~(mAlignSize-1));

	TRACE_D("%s: ImageFormat=%s, size=%dx%d, bufcnt=%d, buflength=%d, buflength_align=%d, step=%d,%d,%d, alloclen=%d,%d,%d, offset=%d,%d,%d",
		__FUNCTION__,mImageFormat.string(),mSize.width, mSize.height,mBufCnt,
		mBufLength, mBufLength_align,
		iStep[0],iStep[1],iStep[2],iAllocLen[0],iAllocLen[1],iAllocLen[2],
		iOffset[0],iOffset[1],iOffset[2]);

	/*
	 * manually de-refer the buffer before next allocation,
	 * to avoid out-of-memory issue
	 */
	mBufHeap.clear();
	for (int i = 0; i < MAX_BUFCNT; i++) {
	    mBuffers[i].clear();
	}

	/*
	 * Make a new pmem heap that can be shared across processes.
	 * Two more buffers are reserved for color space conversion and
	 * resize/rotate.
	 */
	struct pmem_region region;
	if (0 != strcmp(BufNode, ""))
	{
	    //v4l2 user-pointer requires pagealignment for DMA buf offset(and size)
	    TRACE_D("%s:alloc imagebuf from %s",__FUNCTION__,BufNode);
	    sp<MemoryHeapBase> base = new MemoryHeapBase(BufNode,
		    mBufLength_align * (mBufCnt));
	    if (base->heapID() < 0) {
		TRACE_E("Failed to allocate imagebuf from %s", BufNode);
	    }

	    //TODO:consider non-pmem buffer
	    mBufHeap = new MemoryHeapPmem(base, 0);
	    int fd_pmem = mBufHeap->getHeapID();
	    if (ioctl(fd_pmem, PMEM_GET_PHYS, &region)) {
		TRACE_E("Failed to get imagebuf physical address");
	    }

	    /*
	     * Make an IMemory for each streaming buffer so that we can reuse
	     * them in callbacks.
	     */
	    for (int i = 0; i < mBufCnt; i++) {
		ssize_t offset = 0;
		size_t size = 0;
		mBuffers[i] = (static_cast<MemoryHeapPmem *>(mBufHeap.get()))->
		    mapMemory(i * mBufLength_align, mBufLength);
		mBuffers[i]->getMemory(&offset, &size);

		//paddr
		mBufPhyAddr[i][0] = (unsigned long)(region.offset + i * mBufLength_align);
		mBufPhyAddr[i][1] = (unsigned long)(mBufPhyAddr[i][0]) + iAllocLen[0];
		mBufPhyAddr[i][2] = (unsigned long)(mBufPhyAddr[i][1]) + iAllocLen[1];
		//vaddr
		mBufVirAddr[i][0] = (unsigned long*)((long long)mBufHeap->base() + offset);
		mBufVirAddr[i][1] = (unsigned long*)((long long)(mBufVirAddr[i][0]) +
			iAllocLen[0]);
		mBufVirAddr[i][2] = (unsigned long*)((long long)(mBufVirAddr[i][1]) +
			iAllocLen[1]);

		TRACE_V("pmem:bufidx=%d, vaddr=0x %x,%x,%x, len= %d,%d,%d",i,
			mBufVirAddr[i][0],mBufVirAddr[i][1],mBufVirAddr[i][2],
			iAllocLen[0], iAllocLen[1], iAllocLen[2]);

		memset((void*)mBufVirAddr[i][0], 0, iAllocLen[0]);
		memset((void*)mBufVirAddr[i][1], 0, iAllocLen[1]);
		memset((void*)mBufVirAddr[i][2], 0, iAllocLen[2]);
	    }
	}
	else{
	    sp<MemoryHeapBase> base = new MemoryHeapBase( mBufLength_align * mBufCnt);
	    mBufHeap = base;
	    for (int i = 0; i < mBufCnt; i++) {
		ssize_t offset = 0;
		size_t size = 0;
		mBuffers[i] = new MemoryBase(base, i*mBufLength_align , mBufLength);
		//paddr
		mBufPhyAddr[i][0] = 0;//no phyaddr available for ashmem
		mBufPhyAddr[i][1] = 0;
		mBufPhyAddr[i][2] = 0;
		//vaddr
		mBuffers[i]->getMemory(&offset, &size);
		mBufVirAddr[i][0] = (unsigned long*)((int*)mBufHeap->base() + offset);
		mBufVirAddr[i][1] = (unsigned long*)((long long)(mBufVirAddr[i][0]) +
			iAllocLen[0]);
		mBufVirAddr[i][2] = (unsigned long*)((long long)(mBufVirAddr[i][1]) +
			iAllocLen[1]);

		TRACE_V("ashmem:bufidx=%d, vaddr=0x %x,%x,%x, len= %d,%d,%d",i,
			mBufVirAddr[i][0],mBufVirAddr[i][1],mBufVirAddr[i][2],
			iAllocLen[0], iAllocLen[1], iAllocLen[2]);

		memset(mBufVirAddr[i][0], 0, iAllocLen[0]);
		memset(mBufVirAddr[i][1], 0, iAllocLen[1]);
		memset(mBufVirAddr[i][2], 0, iAllocLen[2]);
	    }
	}

	return NO_ERROR;
    }

    sp<IMemory>	ImageBuf::getIMemory(int idx)
    {
	if(idx < 0 || idx > mBufCnt){
	    TRACE_E("invalid buf index, required %d, available [0-%d]",
		    idx,mBufCnt);
	    return NULL;
	}
	return mBuffers[idx];
    }
    int ImageBuf::getBufIndex(const sp<IMemory>& mem)
    {
	ssize_t offset = 0;
	size_t size = 0;
	int idx;
#if 0
	//loop internal buffer
	const void* inptr = mem->pointer();
	for(int i=0;i<mBufCnt;i++){
	    const void* localptr = mBuffers[i]->pointer();
	    if( localptr == inptr){
		return i;
	    }
	}
	TRACE_E("invalid buf index, required %d, available [0-%d]",
		idx,mBufCnt);
	return -1;
#else
	//use imemory api
	mem->getMemory(&offset, &size);
	idx = offset / mBufLength_align;
	if(idx < 0 || idx > mBufCnt){
	    TRACE_E("invalid buf index, required %d, available [0-%d]",
		    idx,mBufCnt);
	    return -1;
	}
	return idx;
#endif
    }

    sp<IMemoryHeap> ImageBuf::getBufHeap()
    {
	return mBufHeap;
    }

    status_t ImageBuf::getPhyAddr(int index, unsigned long* paddr0,
	    unsigned long* paddr1, unsigned long* paddr2){
	*paddr0 = mBufPhyAddr[index][0];
	*paddr1 = mBufPhyAddr[index][1];
	*paddr2 = mBufPhyAddr[index][2];
	return NO_ERROR;
    }

    status_t ImageBuf::getVirAddr(int index, unsigned long** vaddr0,
	    unsigned long** vaddr1, unsigned long** vaddr2){
	*vaddr0 = mBufVirAddr[index][0];
	*vaddr1 = mBufVirAddr[index][1];
	*vaddr2 = mBufVirAddr[index][2];
	return NO_ERROR;
    }

    status_t ImageBuf::getAllocLen(int index, int* len0,
	    int* len1, int* len2){
	*len0 = iAllocLen[0];
	*len1 = iAllocLen[1];
	*len2 = iAllocLen[2];
	return NO_ERROR;
    }

    const char* ImageBuf::getImageFormat(){
	return mImageFormat.string();
    }

    status_t ImageBuf::getImageSize(int* w, int* h){
	*w = mSize.width;
	*h = mSize.height;
	return NO_ERROR;
    }

    status_t ImageBuf::getStep(int index, int* step0, int* step1, int* step2){
	*step0 = iStep[0];
	*step1 = iStep[1];
	*step2 = iStep[2];
	return NO_ERROR;
    }

}; //namespace android
