/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

#ifndef ANDROID_HARDWARE_CAMERA_IMAGEBUF_H
#define ANDROID_HARDWARE_CAMERA_IMAGEBUF_H

//for cameraparameters
#if PLATFORM_SDK_VERSION >= 8
#include <camera/CameraHardwareInterface.h>
#else
#include <ui/CameraHardwareInterface.h>
#endif
#include <binder/MemoryBase.h>
#include <binder/MemoryHeapBase.h>
#include <utils/threads.h>
#include <binder/MemoryHeapPmem.h>
#include <linux/android_pmem.h>
#include <cam_log_mrvl.h>

#define MAX_BUFCNT (20)

namespace android {

    //TODO:add time out strategy.
    class BufStatus{
	public:
	    virtual ~BufStatus(){}
	    BufStatus(){
		for(int i=0;i<MAX_BUFCNT;i++){
		    previewref[i]=0;
		    stillref[i]=0;
		    videoref[i]=0;
		    driverref[i]=0;
		}
	    }
	    void setFree(int i){
		previewref[i]=0;
		stillref[i]=0;
		videoref[i]=0;
		driverref[i]=0;
	    }
	    //buf setatus
	    void toPreview(int i){
		if(++previewref[i] > MAX_BUFCNT){
		    previewref[i] = 0;
		    TRACE_E("%s:bufidx=%d, reach max ref, reset!",__FUNCTION__,i);
		}
	    }
	    void toStill(int i){
		if(++stillref[i] > MAX_BUFCNT){
		    stillref[i] = 0;
		    TRACE_E("%s:bufidx=%d, reach max ref, reset!",__FUNCTION__,i);
		}
	    }
	    void toDriver(int i){
		if(++driverref[i] > MAX_BUFCNT){
		    driverref[i] = 0;
		    TRACE_E("%s:bufidx=%d, reach max ref, reset!",__FUNCTION__,i);
		}
	    }
	    void toVideo(int i){
		if(++videoref[i] > MAX_BUFCNT){
		    videoref[i] = 0;
		    TRACE_E("%s:bufidx=%d, reach max ref, reset!",__FUNCTION__,i);
		}
	    }
	    void fromPreview(int i){
		previewref[i] = 0;
		/*
		if(--previewref[i] < 0){
		    previewref[i] = 0;
		    TRACE_E("%s:bufidx=%d,less than 0, sth. wrong!",__FUNCTION__,i);
		}
		*/
	    }
	    void fromStill(int i){
		stillref[i] = 0;
		/*
		if(--stillref[i] < 0){
		    stillref[i] = 0;
		    TRACE_E("%s:bufidx=%d,less than 0, sth. wrong!",__FUNCTION__,i);
		}
		*/
	    }
	    void fromDriver(int i){
		driverref[i] = 0;
		/*
		if(--driverref[i] < 0){
		    driverref[i] = 0;
		    TRACE_E("%s:bufidx=%d,less than 0, sth. wrong!",__FUNCTION__,i);
		}
		*/
	    }
	    void fromVideo(int i){
		videoref[i] = 0;
		/*
		if(--videoref[i] < 0){
		    videoref[i] = 0;
		    TRACE_E("%s:bufidx=%d,less than 0, sth. wrong!",__FUNCTION__,i);
		}
		*/
	    }
	    bool isFree(int i){
		if(previewref[i] == 0 && videoref[i] == 0 && stillref[i] == 0 &&
			driverref[i] == 0){
		    return true;
		}
		else{
		    return false;
		}
	    }
	    void getRef(int i, int* preview, int* still, int* video, int* driver){
		*preview = previewref[i];
		*still = stillref[i];
		*video = videoref[i];
		*driver = driverref[i];
	    }
	    void getStatus(int i, int* previewcnt,
		    int* stillcnt, int* videocnt, int* drivercnt){
		*previewcnt = previewref[i];
		*stillcnt = stillref[i];
		*videocnt = videoref[i];
		*drivercnt = driverref[i];
	    }

	    void showStatus(int i){
		if (isFree(i)){
		    TRACE_D("idx=%d, free",i);
		}
		else{
		    TRACE_D("bufidx=%d, driver=%d, previewref=%d, stillref=%d, videoref=%d",
			    i, driverref[i], previewref[i], stillref[i],videoref[i]);
		}
	    }

	private:
	    //buffer reference cnt
	    int previewref[MAX_BUFCNT];
	    int stillref[MAX_BUFCNT];
	    int videoref[MAX_BUFCNT];
	    int driverref[MAX_BUFCNT];
    };//class BufStatus

    class ImageBuf: virtual public RefBase{
	public:
	    virtual ~ImageBuf();
	    ImageBuf();
	    //ImageBuf(const char* imageformat, Size size, int bufcnt);
	    status_t AllocImage(const char* imageformat, Size size, int bufcnt, const char* BufNode="");
	    int	    getBufIndex(const sp<IMemory>& mem);
	    int	    getBufCnt(){return mBufCnt;}
	    sp<IMemoryHeap> getBufHeap();

	    sp<IMemory>	getIMemory(int idx);
	    status_t getPhyAddr(int index, unsigned long* paddr0,
		    unsigned long* paddr1, unsigned long* paddr2);

	    status_t getVirAddr(int index, unsigned long** vaddr0,
		    unsigned long** vaddr1, unsigned long** vaddr2);

	    status_t getAllocLen(int index, int* len0,
		    int* len1, int* len2);

	    const char* getImageFormat();

	    status_t getImageSize(int* w, int* h);

	    status_t getStep(int index, int* step0, int* step1, int* step2);

	    void setFree(int i){ bufstatus.setFree(i);}
	    void toPreview(int i){bufstatus.toPreview(i);}
	    void toStill(int i){bufstatus.toStill(i);}
	    void toDriver(int i){bufstatus.toDriver(i);}
	    void toVideo(int i){bufstatus.toVideo(i);}
	    void fromPreview(int i){bufstatus.fromPreview(i);}
	    void fromStill(int i){bufstatus.fromStill(i);}
	    void fromDriver(int i){bufstatus.fromDriver(i);}
	    void fromVideo(int i){bufstatus.fromVideo(i);}
	    bool isFree(int i){return bufstatus.isFree(i);}
	    void getRef(int i, int* preview, int* still, int* video, int* driver){
		bufstatus.getRef(i, preview, still, video, driver);
	    }
	    void getStatus(int i, int* previewcnt,
		    int* stillcnt, int* videocnt, int* drivercnt){
		bufstatus.getStatus(i, previewcnt,
			stillcnt, videocnt, drivercnt);
	    }
	    void showStatus(int i){
		bufstatus.showStatus(i);
	    }
	    void showStatus(){
		TRACE_D("*****buf status*****");
		for(int i=0; i<mBufCnt; i++){
		    bufstatus.showStatus(i);
		}
	    }


	private:
	    int mBufCnt;
	    size_t mAlignSize;
	    int mBufLength;
	    int mBufLength_align;

	    //heap info (for all mem)
	    sp<MemoryHeapBase>  mBufHeap;

	    sp<IMemory>     mBuffers[MAX_BUFCNT];

	    unsigned long   mBufPhyAddr[MAX_BUFCNT][3];
	    unsigned long*  mBufVirAddr[MAX_BUFCNT][3];

	    //buf image info
	    String8 mImageFormat;
	    Size    mSize;
	    /* Notes:
	     * as we assume android will not use discrete yuv buffers for camera case,
	     * we may ignore below entries for simplicity.
	     */
	    int	    iStep[3];//reserved for overlay display
	    int	    iAllocLen[3];
	    int	    iOffset[3];

	    BufStatus bufstatus;
    };//class ImageBuf

}; // namespace android

#endif
