LOCAL_PATH:= $(call my-dir)

#
# libcamera
#

include $(CLEAR_VARS)

ifneq ($(strip $(USE_CAMERA_STUB)),true)
# put your source files here
LOCAL_SRC_FILES:=               \
	hal/CameraHardware.cpp \
	hal/ImageBuf.cpp \
	camctrl/Engine.cpp \
	camctrl/FakeCam.cpp \
	camctrl/exif_helper.cpp \
	setting/CameraSetting.cpp \
	preview/CameraPreviewOverlay.cpp \
	preview/CameraPreviewBaselaySW.cpp

# put the libraries you depend on here.
LOCAL_SHARED_LIBRARIES:= \
    libbinder            \
    libui                \
    libutils             \
    libcutils            \
    liblog               \
    libmedia		\
    libcamera_client	\
    libmiscgen           \
    libcodecjpegdec      \
    libcodecjpegenc	 \
    libphycontmem

# put your module name here
LOCAL_MODULE_TAGS := debug eng
LOCAL_MODULE:= libcamera

LOCAL_CFLAGS += -I device/generic/components/libipp/include \
	-I device/generic/components/libcamera/camera-hal/hal \
	-I device/generic/components/libcamera/camera-hal/camctrl \
	-I device/generic/components/libcamera/camera-hal/preview \
	-I device/generic/components/libcamera/camera-hal/setting \
	-I device/generic/components/libcamera/cameraengine2/include	\
	-I device/generic/components/libcamera/cameraengine2/src/cameraengine2/ansi_c/_include \
	-I device/generic/components/libbmm/lib \
	-I device/generic/components/liboverlay \
	-I device/generic/components/libcamera/cameraengine2/tool/include

LOCAL_CFLAGS += -DPLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)

# for cameraengine log
LOCAL_CFLAGS += -D ANDROID_CAMERAENGINE \
		-D CAM_LOG_VERBOSE

#LOCAL_PRELINK_MODULE:=false

#-----------------------------------------
#platform dependant macro

ifeq ($(TARGET_PRODUCT), evbnevo)
    LOCAL_CFLAGS    += -D SAARBMG1
else 
ifeq ($(TARGET_PRODUCT), saarbmg1)
    LOCAL_CFLAGS    += -D SAARBMG1
else
ifeq ($(TARGET_PRODUCT), jasper)
    LOCAL_CFLAGS    += -D JASPER
else
ifeq ($(TARGET_PRODUCT), abilene)
    LOCAL_CFLAGS    += -D ABILENE
else
ifeq ($(TARGET_PRODUCT), dkb)
    LOCAL_CFLAGS    += -D DKB
else
ifeq ($(TARGET_PRODUCT), evbmg1)
    LOCAL_CFLAGS    += -D EVBMG1
else
ifeq ($(TARGET_PRODUCT), saarcnevo)
    LOCAL_CFLAGS    += -D SAARBMG1
else
ifeq ($(TARGET_PRODUCT), p200)
    LOCAL_CFLAGS    += -DP200
else
	LOCAL_CFLAGS    += -DBROWNSTONE
endif

endif

endif

endif

endif

endif

endif

endif







#-----------------------------------------

LOCAL_STATIC_LIBRARIES += libcameraengine \
			  libcodecjpegdec \
			  libsensorhal_extisp \
			  libippcam \
			  libippexif

include $(BUILD_SHARED_LIBRARY)

endif
