#include "ippExif.h"

#include "codecJP.h"
#include "ippIP.h"
#include "misc.h"

#include "CameraEngine.h"//for CAM_ImageBuffer
#include "CameraSetting.h"//for CameraSetting

#include <cmath>
#include <stdio.h>
#include <stdlib.h>

#include <binder/MemoryBase.h>
#include <binder/MemoryHeapBase.h>
#include <binder/MemoryHeapPmem.h>
#include <linux/android_pmem.h>
#include <cutils/properties.h>

#include "exif_helper.h"

#define ASSERT_ERROR( err )\
 do {\
  switch ( (err) )\
    {\
   case ippStsDataTypeErr:\
      LOGD( "Bad input src image, do not having FFD8 in the head ( %s, %d )\n", __FILE__, __LINE__ );\
      break;\
   case ippStsNotSupportedModeErr:\
      LOGD( "Exif info len is over 64K, currently do not supported writing out of App1 ( %s, %d )\n", __FILE__, __LINE__ );\
      break;\
   case ippStsBadArgErr:\
      LOGD( "Bad input argment( %s, %d )\n", __FILE__, __LINE__ );\
      break;\
   case ippStsOutOfRangeErr:\
      LOGD( "Input info is out of rang defined in Exif spec 2.2( %s, %d )\n", __FILE__, __LINE__ );\
      break;\
   case ippStsNoMemErr:\
      LOGD( "No memory while reserved JIFI info( %s, %d )\n", __FILE__, __LINE__ );\
      break;\
   case ippStsNoErr:\
      break;\
   default:\
      LOGD( "Unknown Error - %d, %s - %d\n", err, __FILE__, __LINE__ );\
      break;\
    }\
  if ( err != ippStsNoErr )\
    {LOGD("some thing wrong must happen for exif, please pay attention\n");}\
  } while (0)

// Logging support -- this is for debugging only
static volatile int32_t gExifLogLevel = 1;
#define EXIF_LOG1(...) LOGD_IF(gExifLogLevel >= 1, __VA_ARGS__);
#define EXIF_LOG2(...) LOGD_IF(gExifLogLevel >= 2, __VA_ARGS__);


namespace android {

static IppStatus _ipp_jpeg_thumbnail( CAM_ImageBuffer *pSrcImgBuf, CAM_ImageBuffer *pDstImgBuf, Ipp32s iQualityFactor )
{
    MiscGeneralCallbackTable *pCallBackTable = NULL;

    IppJPEGEncoderParam       stEncoderPar;
    IppBitstream              stDstBitStream;
    IppPicture                stSrcPicture;
    void                      *pEncoderState = NULL;

    IppJPEGDecoderParam      stDecoderPar;
    IppPicture               stDstPicture;
    IppBitstream             stSrcBitStream;
    void                     *pDecoderState = NULL;

    IppCodecStatus           eRetCode;

    Ipp8u                    *pHeap = NULL, *pDstBuf = NULL;
    CAM_ImageBuffer         stTempBuffer;

    memset( &stDecoderPar, 0, sizeof(IppJPEGDecoderParam) );
    memset( &stDstPicture, 0, sizeof(IppPicture) );
    memset( &stTempBuffer, 0, sizeof(CAM_ImageBuffer) );

    if ( pSrcImgBuf->pBuffer[0] == NULL )
    {
	EXIFLOG( "Error: memory pointer is NULL !\n");
	return ippStsNoMemErr;
    }

    // Init callback table
    if ( miscInitGeneralCallbackTable( &pCallBackTable ) != 0 )
    {
	EXIFLOG( "Error: init JPEG decoder failed!\n");
	return ippStsErr;
    }
    // dummy fFileRead callback function
    pCallBackTable->fFileRead = NULL;
    stSrcBitStream.pBsCurByte = stSrcBitStream.pBsBuffer = pSrcImgBuf->pBuffer[0];
    stSrcBitStream.bsByteLen = pSrcImgBuf->iFilledLen[0];
    stSrcBitStream.bsCurBitOffset = 0;
#if 0

    EXIFLOG( "stSrcBitStream.pBsCurByte: %p\n stSrcBitStream.pBsBuffer: %p\n\
	    stSrcBitStream.bsByteLen: %d\n, stSrcBitStream.bsCurBitOffset :%d\n",
	    stSrcBitStream.pBsCurByte, stSrcBitStream.pBsBuffer, stSrcBitStream.bsByteLen,
	    stSrcBitStream.bsCurBitOffset );

#endif
    eRetCode = DecoderInitAlloc_JPEG( &stSrcBitStream, &stDstPicture, pCallBackTable, &pDecoderState );
    if ( IPP_STATUS_NOERR != eRetCode )
    {
	miscFreeGeneralCallbackTable( &pCallBackTable );
	EXIFLOG( "Error: init JPEG decoder failed!\n");
	return ippStsErr;
    }

    stDstPicture.picWidth  =  pDstImgBuf ->iWidth;
    stDstPicture.picHeight =  pDstImgBuf ->iHeight;
    stDecoderPar.UnionParamJPEG.roiDecParam.nDstWidth = pDstImgBuf ->iWidth;
    stDecoderPar.UnionParamJPEG.roiDecParam.nDstHeight = pDstImgBuf ->iHeight;
    EXIFLOG("Info: snapshot: %d x %d \n", stDstPicture.picWidth, stDstPicture.picHeight);

    if( ippExif_IMGFMT_JPEG == pDstImgBuf->eFormat)
    {
	pHeap = (Ipp8u*)malloc( pDstImgBuf ->iWidth * pDstImgBuf ->iHeight * 2 + 128 );

	if ( pHeap == 0 )
	{
	    EXIFLOG( "Error: no enough memory !\n");
	    return ippStsNoMemErr;
	}

	stTempBuffer.eFormat  = (CAM_ImageFormat)ippExif_IMGFMT_YCC422P;
	stTempBuffer.iWidth   = pDstImgBuf ->iWidth;
	stTempBuffer.iHeight  = pDstImgBuf ->iHeight;
	stTempBuffer.iStep[0] = pDstImgBuf ->iWidth;
	stTempBuffer.iStep[1] = pDstImgBuf ->iWidth >> 1;
	stTempBuffer.iStep[2] = pDstImgBuf ->iWidth >> 1;

	stTempBuffer.pBuffer[0] = (Ipp8u *)_ALIGN_TO( pHeap, 8 );
	stTempBuffer.pBuffer[1] = stTempBuffer.pBuffer[0] + stTempBuffer.iStep[0] * stTempBuffer.iHeight;
	stTempBuffer.pBuffer[2] = stTempBuffer.pBuffer[1] + stTempBuffer.iStep[1] * stTempBuffer.iHeight;

	stTempBuffer.iFilledLen[0] = stTempBuffer.iStep[0] * stTempBuffer.iHeight;
	stTempBuffer.iFilledLen[1] = stTempBuffer.iStep[1] * stTempBuffer.iHeight;
	stTempBuffer.iFilledLen[2] = stTempBuffer.iStep[2] * stTempBuffer.iHeight;
    }

    // map format between Camera Engine and ipp lib

    switch ( pDstImgBuf->eFormat )
    {
	case ippExif_IMGFMT_YCC420P :

	    stDecoderPar.UnionParamJPEG.roiDecParam.nDesiredColor = JPEG_YUV411;
	    stDstPicture.picPlaneNum     = 3;
	    stDstPicture.picPlaneStep[0] = pDstImgBuf->iStep[0];
	    stDstPicture.picPlaneStep[1] = pDstImgBuf->iStep[1];
	    stDstPicture.picPlaneStep[2] = pDstImgBuf->iStep[2];
	    stDstPicture.ppPicPlane[0]   = pDstImgBuf->pBuffer[0];
	    stDstPicture.ppPicPlane[1]   = pDstImgBuf->pBuffer[1];
	    stDstPicture.ppPicPlane[2]   = pDstImgBuf->pBuffer[2];
	    break;

	case ippExif_IMGFMT_YCC422P:

	    stDecoderPar.UnionParamJPEG.roiDecParam.nDesiredColor = JPEG_YUV422;
	    stDstPicture.picPlaneNum     = 3;
	    stDstPicture.picPlaneStep[0] = pDstImgBuf->iStep[0];
	    stDstPicture.picPlaneStep[1] = pDstImgBuf->iStep[1];
	    stDstPicture.picPlaneStep[2] = pDstImgBuf->iStep[2];
	    stDstPicture.ppPicPlane[0]   = pDstImgBuf->pBuffer[0];
	    stDstPicture.ppPicPlane[1]   = pDstImgBuf->pBuffer[1];
	    stDstPicture.ppPicPlane[2]   = pDstImgBuf->pBuffer[2];
	    break;

	case ippExif_IMGFMT_RGB888 :

	    stDecoderPar.UnionParamJPEG.roiDecParam.nDesiredColor = JPEG_RGB888;
	    stDstPicture.picPlaneNum     = 1;
	    stDstPicture.picPlaneStep[0] = pDstImgBuf->iStep[0];
	    stDstPicture.ppPicPlane[0]   = pDstImgBuf->pBuffer[0];
	    break;

	case ippExif_IMGFMT_JPEG :

	    //first generate yuv 422 temp format
	    stDecoderPar.UnionParamJPEG.roiDecParam.nDesiredColor = JPEG_YUV422;
	    stDstPicture.picPlaneNum     = 3;
	    stDstPicture.picPlaneStep[0] = stTempBuffer.iStep[0];
	    stDstPicture.picPlaneStep[1] = stTempBuffer.iStep[1];
	    stDstPicture.picPlaneStep[2] = stTempBuffer.iStep[2];
	    stDstPicture.ppPicPlane[0]   = stTempBuffer.pBuffer[0];
	    stDstPicture.ppPicPlane[1]   = stTempBuffer.pBuffer[1];
	    stDstPicture.ppPicPlane[2]   = stTempBuffer.pBuffer[2];
	    break;

	default:
	    EXIFLOG("Error: the given format[%d] is not supported by JPEG ROI decoder!\n", pDstImgBuf->eFormat );
	    return ippStsNotSupportedModeErr;
	    break;
    }

    stDecoderPar.nModeFlag = IPP_JPEG_ROIDEC_STD;

#if 0

    EXIFLOG( "Info:\
	    stDstPicture.picWidth=%d,\n\
	    stDstPicture.picHeight=%d,\n\
	    stDstPicture.picPlaneStep[0]=%d,\n\
	    stDstPicture.picPlaneStep[1]=%d,\n\
	    stDstPicture.picPlaneStep[2]=%d,\n\
	    stDstPicture.ppPicPlane[0]=%p,\n\
	    stDstPicture.ppPicPlane[1]=%p,\n\
	    stDstPicture.ppPicPlane[2]=%p,\n\
	    stDecoderPar.nModeFlag=%d,\n\
	    stDecoderPar.UnionParamJPEG.roiDecParam.nDesiredColor=%d,\n\
	    stDecoderPar.UnionParamJPEG.roiDecParam.srcROI.width=%d,\n\
	    stDecoderPar.UnionParamJPEG.roiDecParam.srcROI.height=%d,\n\
	    stDecoderPar.UnionParamJPEG.roiDecParam.srcROI.x=%d,\n\
	    stDecoderPar.UnionParamJPEG.roiDecParam.srcROI.y=%d,\n\
	    stDecoderPar.UnionParamJPEG.roiDecParam.nDstWidth=%d,\n\
	    stDecoderPar.UnionParamJPEG.roiDecParam.nDstHeight=%d\n",
	    stDstPicture.picWidth,
	    stDstPicture.picHeight,
	    stDstPicture.picPlaneStep[0],
	    stDstPicture.picPlaneStep[1],
	    stDstPicture.picPlaneStep[2],
	    stDstPicture.ppPicPlane[0],
	    stDstPicture.ppPicPlane[1],
	    stDstPicture.ppPicPlane[2],
	    stDecoderPar.nModeFlag,
	    stDecoderPar.UnionParamJPEG.roiDecParam.nDesiredColor,
	    stDecoderPar.UnionParamJPEG.roiDecParam.srcROI.width,
	    stDecoderPar.UnionParamJPEG.roiDecParam.srcROI.height,
	    stDecoderPar.UnionParamJPEG.roiDecParam.srcROI.x,
	    stDecoderPar.UnionParamJPEG.roiDecParam.srcROI.y,
	    stDecoderPar.UnionParamJPEG.roiDecParam.nDstWidth,
	    stDecoderPar.UnionParamJPEG.roiDecParam.nDstHeight );

#endif

    // call the core JPEG decoder function

    eRetCode = Decode_JPEG(&stSrcBitStream, NULL, &stDstPicture, &stDecoderPar, pDecoderState);

    if ( IPP_STATUS_JPEG_EOF != eRetCode )
    {
	EXIFLOG( "Error: JPEG_ROIDec Failed, error code[%d]( %s, %d )!\n", eRetCode, __FUNCTION__, __LINE__ );
	DecoderFree_JPEG( &pDecoderState );
	miscFreeGeneralCallbackTable( &pCallBackTable );
	return ippStsErr;
    }

    DecoderFree_JPEG( &pDecoderState );

    miscFreeGeneralCallbackTable( &pCallBackTable );

    //convert form yuv 422 to JPEG
    if( ippExif_IMGFMT_JPEG == pDstImgBuf->eFormat)
    {

	stSrcPicture.picWidth            = stTempBuffer.iWidth;
	stSrcPicture.picHeight           = stTempBuffer.iHeight;
	stSrcPicture.picFormat           = JPEG_YUV422;
	stSrcPicture.picChannelNum       = 3;
	stSrcPicture.picPlaneNum         = 3;
	stSrcPicture.picPlaneStep[0]     = stTempBuffer.iStep[0];
	stSrcPicture.picPlaneStep[1]     = stTempBuffer.iStep[1];
	stSrcPicture.picPlaneStep[2]     = stTempBuffer.iStep[2];
	stSrcPicture.ppPicPlane[0]       = stTempBuffer.pBuffer[0];
	stSrcPicture.ppPicPlane[1]       = stTempBuffer.pBuffer[1];
	stSrcPicture.ppPicPlane[2]       = stTempBuffer.pBuffer[2];
	stSrcPicture.picROI.x            = 0;
	stSrcPicture.picROI.y            = 0;
	stSrcPicture.picROI.width        = stTempBuffer.iWidth;
	stSrcPicture.picROI.height       = stTempBuffer.iHeight;

	stDstBitStream.pBsBuffer        = (Ipp8u*)pDstImgBuf->pBuffer[0];
	stDstBitStream.bsByteLen        = pDstImgBuf->iAllocLen[0];
	stDstBitStream.pBsCurByte       = (Ipp8u*)pDstImgBuf->pBuffer[0];
	stDstBitStream.bsCurBitOffset   = 0;

	stEncoderPar.nQuality          = iQualityFactor;
	stEncoderPar.nRestartInterval  = 0;
	stEncoderPar.nJPEGMode         = JPEG_BASELINE;
	stEncoderPar.nSubsampling      = JPEG_422 ;
	stEncoderPar.nBufType          = JPEG_INTEGRATEBUF;
	stEncoderPar.pSrcPicHandler    = (void*)NULL;
	stEncoderPar.pStreamHandler    = (void*)NULL;

	if ( miscInitGeneralCallbackTable(&pCallBackTable) != 0 )
	{
	    return ippStsNoMemErr;
	}

	eRetCode = EncoderInitAlloc_JPEG( &stEncoderPar, &stSrcPicture, &stDstBitStream, pCallBackTable, &pEncoderState );

	if ( IPP_STATUS_NOERR != eRetCode )
	{
	    return ippStsErr;
	}

	eRetCode = Encode_JPEG( &stSrcPicture, &stDstBitStream, pEncoderState );

	if ( IPP_STATUS_NOERR != eRetCode )
	{
	    EncoderFree_JPEG( &pEncoderState );
	    miscFreeGeneralCallbackTable( &pCallBackTable );
	    return ippStsErr;
	}

	pDstImgBuf->iFilledLen[0] = (Ipp32s)stDstBitStream.pBsCurByte - (Ipp32s)stDstBitStream.pBsBuffer;
	pDstImgBuf->iFilledLen[1] = 0;
	pDstImgBuf->iFilledLen[2] = 0;

	EXIFLOG("THE THUMBNAIL JPEG LENGTH IS %d\n",pDstImgBuf->iFilledLen[0] );
	EncoderFree_JPEG( &pEncoderState );
	miscFreeGeneralCallbackTable( &pCallBackTable );

    }
    if(NULL != pHeap){
	free(pHeap);
	pHeap = NULL;
    }

    switch ( pDstImgBuf->eFormat )
    {
	case ippExif_IMGFMT_YCC420P:

	    pDstImgBuf->iFilledLen[0] = pDstImgBuf->iStep[0] * pDstImgBuf->iHeight;
	    pDstImgBuf->iFilledLen[1] = pDstImgBuf->iStep[1] * pDstImgBuf->iHeight >> 1;
	    pDstImgBuf->iFilledLen[2] = pDstImgBuf->iStep[2] * pDstImgBuf->iHeight >> 1;
	    break;

	case ippExif_IMGFMT_YCC422P:

	    pDstImgBuf->iFilledLen[0] = pDstImgBuf->iStep[0] * pDstImgBuf->iHeight;
	    pDstImgBuf->iFilledLen[1] = pDstImgBuf->iStep[1] * pDstImgBuf->iHeight;
	    pDstImgBuf->iFilledLen[2] = pDstImgBuf->iStep[2] * pDstImgBuf->iHeight;
	    break;

	case ippExif_IMGFMT_RGB888 :

	    pDstImgBuf->iFilledLen[0] = pDstImgBuf->iStep[0] * pDstImgBuf->iHeight ;
	    pDstImgBuf->iFilledLen[1] = 0;
	    pDstImgBuf->iFilledLen[2] = 0;
	    break;

	default:
	    break;
    }

    return ippStsNoErr;
}

bool updateExifInfo(IppExifInfo *pExifInfo,const CameraSetting& setting, CAM_ImageBuffer* pImgBuf_still)
{
    IppExif32uRational  urXResolutionDefault = {72, 1};
    IppExif32uRational  urYResolutionDefault = {72, 1};
    IppExif32uRational  urFocalLength        = {400,4000,}; //
    int                 iExifW               = 0;
    int                 iExifH               = 0;
    CAM_ShotModeCapability cShotModeCapability = setting.getSupportedSceneModeCap();

    time_t rawtime;
    struct tm *timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    setting.getPictureSize(&iExifW, &iExifH);

   // pExifInfo->eCPUEndian = ippExifLittleEndian;
    strncpy((char *)pExifInfo->stTiffInfo.ucImageDescription,"tiff infomation", MAX_IMAGE_NAME);
   
	char value[PROPERTY_VALUE_MAX];
	property_get("ro.product.brand", value, "unknown");   
    strncpy((char *)pExifInfo->stTiffInfo.ucMake,value, MAX_MAKE_NAME);
	property_get("ro.product.model", value, "unknown");
    strncpy((char *)pExifInfo->stTiffInfo.ucModel, value, MAX_MODEL_NAME);
	
    pExifInfo->stTiffInfo.urXResolution     = urXResolutionDefault;
    pExifInfo->stTiffInfo.urYResolution     = urYResolutionDefault;
    pExifInfo->stTiffInfo.eResolutionUnit   = ippExifResolutionUnitInch;
    pExifInfo->stTiffInfo.eYCbCrPosition    = ippExifYCbCrPosCentral;
    strncpy((char *)pExifInfo->stTiffInfo.ucSoftware, "N/A", MAX_SOFTWARE_LEN);
    strftime((char *)pExifInfo->stTiffInfo.ucDataTime, TIME_DEFAULT_LEN, "%Y:%m:%d %H:%M:%S", timeinfo);

    pExifInfo->stDCFInteropbInfo.eInteropbIndex = ippExifInteropbR98;
    pExifInfo->stDCFInteropbInfo.eInteropbVersion = ippDCFInteropbVersion1;

    strftime((char *)pExifInfo->stExifPrivateTagInfo.ucDateTimeOriginal, TIME_DEFAULT_LEN, "%Y:%m:%d %H:%M:%S", timeinfo);
    strftime((char *)pExifInfo->stExifPrivateTagInfo.ucDateTimeDigitized, TIME_DEFAULT_LEN, "%Y:%m:%d %H:%M:%S", timeinfo);
    pExifInfo->stExifPrivateTagInfo.eComponentConfiguration = ippExifComponentConfigYCbCr;
    pExifInfo->stExifPrivateTagInfo.eColorSpace             = ippExifColorSpaceUnCalibrated; //default
    pExifInfo->stExifPrivateTagInfo.urFocalLength           = urFocalLength;
    pExifInfo->stExifPrivateTagInfo.uiPixelXDimention       = iExifW;
    pExifInfo->stExifPrivateTagInfo.uiPixelYDimention       = iExifH;
    pExifInfo->stExifPrivateTagInfo.eExposureMode           = ippExifExposureModeManual;
    //strcpy(pExifInfo->stExifPrivateTagInfo.cExifVersion,"0220");//default
    //strcpy(pExifInfo->stExifPrivateTagInfo.cFlashPixelVersion,"0100");//default

    pExifInfo->stExifPrivateTagInfo.eSceneType              = ippExifSceneTypeDirectly;
    pExifInfo->stExifPrivateTagInfo.eFileSource             = ippExifFileSourceDirectly;
    pExifInfo->stExifPrivateTagInfo.eLightSource            = ippExifLightSourceUnKnown;
    pExifInfo->stExifPrivateTagInfo.usFocalLenIn35mmFilm    = 0;  // FIXME: pImgBuf_still->stShotInfo.iFocalLen35mm;
    pExifInfo->stExifPrivateTagInfo.eSensingMethod          = ippExifSMOneSensor;

    int iZoom = setting.getInt(CameraParameters::KEY_ZOOM);
    pExifInfo->stExifPrivateTagInfo.urDigitalZoomRatio.numerator    = cShotModeCapability.iMinDigZoomQ16 + iZoom * cShotModeCapability.iDigZoomStepQ16;
    pExifInfo->stExifPrivateTagInfo.urDigitalZoomRatio.denominal    = 65536;
    EXIF_LOG1("EXIF information: DigitalZoomRatio: %.2f", (cShotModeCapability.iMinDigZoomQ16 + iZoom * cShotModeCapability.iDigZoomStepQ16) / 65536.0);

    char  pMakerNote[] = "none";
    pExifInfo->stExifPrivateTagInfo.pMakerNote               =(Ipp8u *) pMakerNote;
    pExifInfo->stExifPrivateTagInfo.uiMakerNoteLen           = strlen(pMakerNote );

    // spec2.2, annex C, APEX is a convenient unit for expressing exposure (Ev).
    // ApertureValue (Av) = 2 log2 (F number)
    // ShutterSpeedValue (Tv) = - log2 (exposure time)
    // BrightnessValue (Bv) = log2 ( B/NK ) Note that: B:cd/cm2, N,K: constant
    // Film sensitivity (Sv) = log2 ( ASA / 3.125 )
    // Ev = Av + Tv = Bv + Sv

    if (pImgBuf_still->bEnableShotInfo)
    {
        pExifInfo->stExifPrivateTagInfo.urExposureTime.numerator        = pImgBuf_still->stShotInfo.iExposureTimeQ16;
        pExifInfo->stExifPrivateTagInfo.urExposureTime.denominal        = 65536;

        //IppExifPrivateTagInfo.urFNumber
        pExifInfo->stExifPrivateTagInfo.urFNumber.numerator             = pImgBuf_still->stShotInfo.iFNumQ16;
        pExifInfo->stExifPrivateTagInfo.urFNumber.denominal             = 65536;

        uint32_t Av, Tv, Bv, Sv, Ev;

        double Fnumber              = (double)pExifInfo->stExifPrivateTagInfo.urFNumber.numerator / pExifInfo->stExifPrivateTagInfo.urFNumber.denominal;
        double ExposureTime         = (double)pExifInfo->stExifPrivateTagInfo.urExposureTime.numerator / pExifInfo->stExifPrivateTagInfo.urExposureTime.denominal; 
        uint16_t IsoSpeedRating     = 100;

        Ipp16u pISOSpeed[]                    = {100}; //FIXME: pImgBuf_still->stShotInfo.iISOSpeed;
        pExifInfo->stExifPrivateTagInfo.pISOSpeedRating        = pISOSpeed;
        pExifInfo->stExifPrivateTagInfo.uiISOSpeedRatingCnt    = sizeof(pISOSpeed)/sizeof(Ipp16u);

        #define _LOG2(x) (log((double)(x)) / log(2.0))
        Av = (int)(2 * _LOG2((double)(Fnumber)) + 0.5);
        Tv = ExposureTime >= 1 ? (int)(-_LOG2(ExposureTime) - 0.5) : (int)(-_LOG2(ExposureTime) + 0.5);
        Sv = ((int)(_LOG2((IsoSpeedRating) / 3.125) + 0.5));  // Film sensitivity (Sv) = log2 ( ASA / 3.125 )
        Bv = Av + Tv - Sv;
        Ev = Av + Tv;

        // ApertureValue (Av) = 2 log2 (F number)
        pExifInfo->stExifPrivateTagInfo.urApertureValue.numerator       = Av * 65536;
        pExifInfo->stExifPrivateTagInfo.urApertureValue.denominal       = 65536;

        // ShutterSpeedValue (Tv) = - log2 (exposure time)
        pExifInfo->stExifPrivateTagInfo.srShutterSpeed.inumerator       = Tv * 65536;
        pExifInfo->stExifPrivateTagInfo.srShutterSpeed.idenominal       = 65536;
        //ShutterSpeedValue

        // BrightnessValue (Bv) = log2 ( B/NK )
        pExifInfo->stExifPrivateTagInfo.srBrightnessValue.inumerator  = Bv * 65536;
        pExifInfo->stExifPrivateTagInfo.srBrightnessValue.idenominal  = 65536;

        //IppExifPrivateTagInfo.srExposureBiaValue  // SCENE_MODE_BEACH_SNOW ---> 65536/65536
        // Ev = Av + Tv = Bv + Sv
        pExifInfo->stExifPrivateTagInfo.srExposureBiaValue.inumerator   = 0; // TBD
        pExifInfo->stExifPrivateTagInfo.srExposureBiaValue.idenominal   = 65536;

        pExifInfo->stExifPrivateTagInfo.urMaxApertureValue.numerator    = (int)(2 * _LOG2((double)(cShotModeCapability.iMinFNumQ16 >> 16)) + 0.5) << 16; // FAKE: 194698;
        pExifInfo->stExifPrivateTagInfo.urMaxApertureValue.denominal    = 65536;
        EXIF_LOG1("EXIF information: MaxApertureValue: %.2f", pExifInfo->stExifPrivateTagInfo.urMaxApertureValue.numerator / 65536.0);

        pExifInfo->stExifPrivateTagInfo.eExposureProgram                = ippExifExposureProgramNotDefined;
        pExifInfo->stExifPrivateTagInfo.eMeteringMode                   = ippExifMeteringModeCenterWeightedAverage; //
        pExifInfo->stExifPrivateTagInfo.eFlash                          = ippExifFlashNotFired; // not used
        pExifInfo->stExifPrivateTagInfo.eContrast                       = ippExifContrastNormal;
        pExifInfo->stExifPrivateTagInfo.eSaturation                     = ippExifSaturationNormal;
        pExifInfo->stExifPrivateTagInfo.eSharpness                      = ippExifSharpnessNormal;
        pExifInfo->stExifPrivateTagInfo.eSceneCaptureType               = ippExifSceneCaptureTypeStandard;
    }

    float latitude      = setting.getFloat(CameraParameters::KEY_GPS_LATITUDE);
    float longitude     = setting.getFloat(CameraParameters::KEY_GPS_LONGITUDE);
    int altitude        = setting.getInt(CameraParameters::KEY_GPS_ALTITUDE);
    int timestamp       = setting.getInt(CameraParameters::KEY_GPS_TIMESTAMP);

    const char *gpsvalue  = setting.get(CameraParameters::KEY_GPS_LATITUDE);
    const char *gpstmp    = gpsvalue;

    if (0 != gpsvalue)
    {
	/*
	   D = TRUNC(d)
	   M = TRUNC((d - D) * 60)
	   s = (d - D - M/60) * 3600 = (d - D) * 3600 - M * 60
	   */
	pExifInfo->stGPSInfo.eGPSLatituteRef            = (latitude > 0 ) ? ippExifGPSLatituteRefNorth : ippExifGPSLatituteRefSouth;
	pExifInfo->stGPSInfo.eGPSLongtituteRef          = (longitude > 0) ? ippExifGPSLongtituteRefEast : ippExifGPSLongtituteRefWest;

	double fLatitude    = setting.getFloat(CameraParameters::KEY_GPS_LATITUDE);
	long lLatitude      = (long)(fLatitude * 10000 / 1);
	double fLongitude   = setting.getFloat(CameraParameters::KEY_GPS_LONGITUDE);
	long lLongitude     = (long)(fLongitude * 10000 / 1);
	double latitude     = fabs(lLatitude / 10000.0);
	double longitude    = fabs(lLongitude / 10000.0);


	pExifInfo->stGPSInfo.urGPSLatitute[0].numerator     = (uint32_t)latitude;
	pExifInfo->stGPSInfo.urGPSLatitute[0].denominal     = 1;
	pExifInfo->stGPSInfo.urGPSLatitute[1].numerator     = (uint32_t)((latitude - pExifInfo->stGPSInfo.urGPSLatitute[0].numerator) * 60);
	pExifInfo->stGPSInfo.urGPSLatitute[1].denominal     = 1;
	pExifInfo->stGPSInfo.urGPSLatitute[2].numerator     = (uint32_t)((((latitude - pExifInfo->stGPSInfo.urGPSLatitute[0].numerator) * 60)
		    - pExifInfo->stGPSInfo.urGPSLatitute[1].numerator) * 60);
	pExifInfo->stGPSInfo.urGPSLatitute[2].denominal     = 1;

	pExifInfo->stGPSInfo.urGPSLongtitute[0].numerator   = (uint32_t)longitude;
	pExifInfo->stGPSInfo.urGPSLongtitute[0].denominal   = 1;
	pExifInfo->stGPSInfo.urGPSLongtitute[1].numerator   = (uint32_t)((longitude - pExifInfo->stGPSInfo.urGPSLongtitute[0].numerator) * 60);
	pExifInfo->stGPSInfo.urGPSLongtitute[1].denominal   = 1;
	pExifInfo->stGPSInfo.urGPSLongtitute[2].numerator   = (uint32_t)((((longitude - pExifInfo->stGPSInfo.urGPSLongtitute[0].numerator) * 60)
		    - pExifInfo->stGPSInfo.urGPSLongtitute[1].numerator) * 60);
	pExifInfo->stGPSInfo.urGPSLongtitute[2].denominal   = 1;


	//time stamp
	struct tm tmTimestamp;
	const char *sTimestamp = setting.get(CameraParameters::KEY_GPS_TIMESTAMP);
	long lTimestamp = atol(sTimestamp);
	gmtime_r(&lTimestamp, &tmTimestamp);
	pExifInfo->stGPSInfo.urGPSTimeStamp[0].numerator = tmTimestamp.tm_hour;
	pExifInfo->stGPSInfo.urGPSTimeStamp[0].denominal = 1;
	pExifInfo->stGPSInfo.urGPSTimeStamp[1].numerator = tmTimestamp.tm_min;
	pExifInfo->stGPSInfo.urGPSTimeStamp[1].denominal = 1;
	pExifInfo->stGPSInfo.urGPSTimeStamp[2].numerator = tmTimestamp.tm_sec;
	pExifInfo->stGPSInfo.urGPSTimeStamp[2].denominal = 1;
	snprintf((char*)pExifInfo->stGPSInfo.urGPSTimeStamp, sizeof(pExifInfo->stGPSInfo.urGPSTimeStamp),
		"%04d:%02d:%02d", tmTimestamp.tm_year, tmTimestamp.tm_mon, tmTimestamp.tm_mday);

	//process method
	const char *tmpProcessMethod = setting.get(CameraParameters::KEY_GPS_PROCESSING_METHOD);
        pExifInfo->stGPSInfo.pGPSProcessMethod =(Ipp8u*) tmpProcessMethod;
        pExifInfo->stGPSInfo.uiGPSProcessMethodLen = strlen(tmpProcessMethod);
	//strncpy((char *)pExifInfo->stGPSInfo.cGPSProcessMethod, tmpProcessMethod, sizeof(pExifInfo->stGPSInfo.cGPSProcessMethod));   // MAX_GPS_PROCESS_METHOD

	//date stamp
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	strftime((char *)pExifInfo->stGPSInfo.ucGPSDateStamp, 11, "%Y:%m:%d %H:%M:%S", timeinfo);
    }

    //map exif rotation
    const char* v = setting.get(CameraParameters::KEY_ROTATION);
    LOGD("%s:KEY_ROTATION: %s", __FUNCTION__, v);
    if (0 != v){
	int rotation = setting.map_andr2ce(CameraSetting::map_exifrotation, v);
	if (-1 != rotation){
	    pExifInfo->stTiffInfo.eOrientation = (IppExifOrientation)rotation;
	    LOGD("%s:Exif eOrientation: %d", __FUNCTION__, rotation);
	}
    }

    //map exif whitebalance
    v = (char *)setting.get(CameraParameters::KEY_WHITE_BALANCE);
    LOGD("%s:KEY_WHITE_BALANCE: %s", __FUNCTION__, v);
    if (0 != v){
	int whitebalance = setting.map_andr2ce(CameraSetting::map_exifwhitebalance, v);
	if (-1 != whitebalance){
	    pExifInfo->stExifPrivateTagInfo.eWhiteBalance = (IppExifWhiteBalance)whitebalance;
	    LOGD("%s:Exif eWhiteBalance: %d", __FUNCTION__, whitebalance);
	}
    }

    if (0 == strcmp(setting.getSensorName(), "ov5642"))
    {
	// override the following values depends on diffenrent sensor.
#if 0
	IppExif32uRational  urXResolutionDefault = {72, 1};
	IppExif32uRational  urYResolutionDefault = {72, 1};
	IppExif32uRational  urFocalLength={400,4000,}; //
	pExifInfo->stTiffInfo.urXResolution = urXResolutionDefault;
	pExifInfo->stTiffInfo.urYResolution = urYResolutionDefault;
	pExifInfo->stExifPrivateTagInfo.urFocalLength = urFocalLength;
#endif
    }

    return true;
}

sp<MemoryBase> ExifGenerator(CAM_ImageBuffer* pImgBuf_still, const CameraSetting& setting)
{
    sp<MemoryHeapBase> pic_heap=NULL;
    sp<MemoryBase> pic_mem=NULL;
    ssize_t offset = 0;
    size_t size = 0;
    size_t pic_buflen = 0;
    int heap_size = 0;

    IppStatus  error           = ippStsNoErr;
    pic_buflen = pImgBuf_still->iFilledLen[0] +
	pImgBuf_still->iFilledLen[1] +
	pImgBuf_still->iFilledLen[2];

    //step 1:initialize default exif info
    IppExifInfo cExifInfo = IPPExif_Default_ExifInfo;

    //step 2:update specific exif info
    updateExifInfo(&cExifInfo, setting, pImgBuf_still);

    // not effected by stTiffInfo.eOrientation
    cExifInfo.stThumbnailInfo.eOrientation = ippExifOrientationNormal;

    //==================================================================================================
    //step 3:prepare to generate thumbnail
    CAM_ImageBuffer srcImgBuf;//source image buffer which contains raw jpeg pic, without exif info
    CAM_ImageBuffer ThumbnailBuf;//contain info for thubmnail buffer
    memset(&srcImgBuf, 0, sizeof(CAM_ImageBuffer));
    memset(&ThumbnailBuf, 0, sizeof(CAM_ImageBuffer));

    //provide src image info
    srcImgBuf.pBuffer[0] = (Ipp8u*)pImgBuf_still->pBuffer[0];
    srcImgBuf.iFilledLen[0] = pic_buflen;

    //provide dest image request, including format/width/height/qualityfactor
    ThumbnailBuf.eFormat = (CAM_ImageFormat)ippExif_IMGFMT_JPEG;//here we assume jpeg thumbnail.
    ThumbnailBuf.iWidth = setting.getParameters().getInt(
	    CameraParameters::KEY_JPEG_THUMBNAIL_WIDTH);
    ThumbnailBuf.iHeight = setting.getParameters().getInt(
	    CameraParameters::KEY_JPEG_THUMBNAIL_HEIGHT);
    Ipp32s iQualityFactor = setting.getParameters().getInt(
	    CameraParameters::KEY_JPEG_THUMBNAIL_QUALITY);

    //step 4:generate thumbnail
    Ipp8u *pHeap = NULL;//ptr to thumbnail image

    if(ThumbnailBuf.iWidth > 0 && ThumbnailBuf.iHeight > 0 &&
	    iQualityFactor > 0 && iQualityFactor <= 100){
	LOGD("%s:generate thumbnail:width=%d,height=%d,qualityfactor=%d",
		__FUNCTION__,ThumbnailBuf.iWidth, ThumbnailBuf.iHeight, iQualityFactor);
	//
	IppStatus error = ippStsNoErr;

	switch( ThumbnailBuf.eFormat )
	{
	    case ippExif_IMGFMT_YCC420P:

		pHeap = (Ipp8u *)malloc( ThumbnailBuf.iWidth * ThumbnailBuf.iHeight * 3 / 2 + 128 );
		if ( pHeap == NULL ){
		    return NULL;
		}

		ThumbnailBuf.iStep[0] = ThumbnailBuf.iWidth;
		ThumbnailBuf.iStep[1] = ThumbnailBuf.iWidth >> 1;
		ThumbnailBuf.iStep[2] = ThumbnailBuf.iWidth >> 1;

		ThumbnailBuf.pBuffer[0] = (Ipp8u *)_ALIGN_TO( pHeap, 8 );
		ThumbnailBuf.pBuffer[1] = ThumbnailBuf.pBuffer[0] +
		    ThumbnailBuf.iStep[0] * ThumbnailBuf.iHeight;
		ThumbnailBuf.pBuffer[2] = ThumbnailBuf.pBuffer[1] +
		    ThumbnailBuf.iStep[1] * ( ThumbnailBuf.iHeight >> 1 );

		error = _ipp_jpeg_thumbnail(&srcImgBuf, &ThumbnailBuf, iQualityFactor );

		break;

	    case ippExif_IMGFMT_YCC422P:
		pHeap = (Ipp8u *)malloc( ThumbnailBuf.iWidth * ThumbnailBuf.iHeight *  2 + 128 );
		if ( pHeap == NULL ){
		    return NULL;
		}

		ThumbnailBuf.iStep[0] = ThumbnailBuf.iWidth;
		ThumbnailBuf.iStep[1] = ThumbnailBuf.iWidth >> 1;
		ThumbnailBuf.iStep[2] = ThumbnailBuf.iWidth >> 1;

		ThumbnailBuf.pBuffer[0] = (Ipp8u *)_ALIGN_TO( pHeap, 8 );
		ThumbnailBuf.pBuffer[1] = ThumbnailBuf.pBuffer[0] +
		    ThumbnailBuf.iStep[0] * ThumbnailBuf.iHeight;
		ThumbnailBuf.pBuffer[2] = ThumbnailBuf.pBuffer[1] +
		    ThumbnailBuf.iStep[1] * ThumbnailBuf.iHeight;

		error = _ipp_jpeg_thumbnail(&srcImgBuf, &ThumbnailBuf, iQualityFactor );

		break;

	    case ippExif_IMGFMT_JPEG:

		pHeap = (Ipp8u *)malloc( ThumbnailBuf.iWidth * ThumbnailBuf.iHeight * 3  + 128 );
		if ( pHeap == NULL ){
		    return NULL;
		}

		ThumbnailBuf.pBuffer[0] = (Ipp8u *)_ALIGN_TO( pHeap, 8 );
		ThumbnailBuf.iAllocLen[0] = ThumbnailBuf.iWidth * ThumbnailBuf.iHeight * 3 ;
		iQualityFactor  = 60;

		error = _ipp_jpeg_thumbnail(&srcImgBuf, &ThumbnailBuf, iQualityFactor );

		//TODO: delete JPEG thumbnail APP tag.
		/*the exif thumnail need pure bit stream ,do not contain any APP tag,just from FFD8 to FFD9, but ipp_jpeg_thumbnail
		  function generate full jpeg image, so if want to be precise, need to delete jpeg thumbnail APP tag*/

		break;

	    case ippExif_IMGFMT_RGB888 :

		pHeap = (Ipp8u *)malloc( ThumbnailBuf.iWidth * ThumbnailBuf.iHeight * 3 + 128 );
		if ( pHeap == NULL ){
		    return NULL;
		}

		ThumbnailBuf.iStep[0] = ThumbnailBuf.iWidth * 3;
		ThumbnailBuf.pBuffer[0] = (Ipp8u *)_ALIGN_TO( pHeap, 8 );

		error = _ipp_jpeg_thumbnail(&srcImgBuf, &ThumbnailBuf, iQualityFactor );
		break;

	    default:
		error = ippStsNotSupportedModeErr;
		break;
	}

	cExifInfo.stThumbnailInfo.pData = ThumbnailBuf.pBuffer[0];
	cExifInfo.stThumbnailInfo.iDataLen = ThumbnailBuf.iFilledLen[0] +
	    ThumbnailBuf.iFilledLen[1] + ThumbnailBuf.iFilledLen[2];

	switch( ThumbnailBuf.eFormat )
	{
	    case ippExif_IMGFMT_YCC420P:

		cExifInfo.stThumbnailInfo.eCompression = ippExifCompressionUnCompressed;
		cExifInfo.stThumbnailInfo.ePhotometricInterpretation = ippExifPhotometricInterpretationYCC;
		cExifInfo.stThumbnailInfo.eResolutionUnit = ippExifResolutionUnitInch;
		cExifInfo.stThumbnailInfo.stRawThumbAttribute.uiHeight = ThumbnailBuf.iHeight;
		cExifInfo.stThumbnailInfo.stRawThumbAttribute.uiWidth = ThumbnailBuf.iWidth;
		cExifInfo.stThumbnailInfo.stRawThumbAttribute.ePlanarConfig = ippExifPlanarConfigPlanar;
		cExifInfo.stThumbnailInfo.stRawThumbAttribute.eYCCSubSampling = ippExifYCCSubSampling420;
		break;

	    case  ippExif_IMGFMT_YCC422P:

		cExifInfo.stThumbnailInfo.eCompression = ippExifCompressionUnCompressed;
		cExifInfo.stThumbnailInfo.ePhotometricInterpretation = ippExifPhotometricInterpretationYCC;
		cExifInfo.stThumbnailInfo.eResolutionUnit = ippExifResolutionUnitInch ;
		cExifInfo.stThumbnailInfo.stRawThumbAttribute.uiHeight = ThumbnailBuf.iHeight;
		cExifInfo.stThumbnailInfo.stRawThumbAttribute.uiWidth = ThumbnailBuf.iWidth;
		cExifInfo.stThumbnailInfo.stRawThumbAttribute.ePlanarConfig = ippExifPlanarConfigPlanar;
		cExifInfo.stThumbnailInfo.stRawThumbAttribute.eYCCSubSampling = ippExifYCCSubSampling422;
		break;

	    case ippExif_IMGFMT_JPEG:

		cExifInfo.stThumbnailInfo.eCompression = ippExifCompressionThumbnailJPEG;
		break;

	    case ippExif_IMGFMT_RGB888 :

		cExifInfo.stThumbnailInfo.eCompression = ippExifCompressionUnCompressed;
		cExifInfo.stThumbnailInfo.ePhotometricInterpretation = ippExifPhotometricInterpretationRGB;
		cExifInfo.stThumbnailInfo.eResolutionUnit = ippExifResolutionUnitInch ;
		cExifInfo.stThumbnailInfo.stRawThumbAttribute.uiHeight = ThumbnailBuf.iHeight;
		cExifInfo.stThumbnailInfo.stRawThumbAttribute.uiWidth = ThumbnailBuf.iWidth;
		cExifInfo.stThumbnailInfo.stRawThumbAttribute.ePlanarConfig = ippExifPlanarConfigChunky;
		break;

	    default:
		break;
	}//switch( ThumbnailBuf.eFormat )
    }//if
    else{
	//no thumbnail request, so we may take the no thubmnail branch later.
	LOGE("%s:Error in thumbnail parameters:width=%d,height=%d,qualityfactor=%d",
		__FUNCTION__,ThumbnailBuf.iWidth, ThumbnailBuf.iHeight, iQualityFactor);
    }

    //step 5:update buffer requirement with exifinfo and thumbnail
    IppExifBufReq cBufReq;
    error = ippExifGetBufReq(&cExifInfo, &cBufReq);//ipp API
    ASSERT_ERROR( error );

    //prepare dest buffer for Camera Image Callback
    pic_heap = new MemoryHeapBase((size_t)cBufReq.iMinBufLen + pic_buflen);
    heap_size = (int)pic_heap->getSize();
    pic_mem = new MemoryBase(pic_heap, 0, heap_size);

    pic_mem->getMemory(&offset, &size);
    uint8_t* ptr_pic = (uint8_t*)pic_heap->base()+offset;
    memset(ptr_pic,0,size);

    //exif dest buffer info
    IppExifImgBuf cDesImg;
    cDesImg.pBuffer=ptr_pic;
    cDesImg.iFilledLen=cBufReq.iMinBufLen + pic_buflen;

    //exif src buffer info
    IppExifImgBuf cSrcImg;
    cSrcImg.pBuffer = pImgBuf_still->pBuffer[0];
    cSrcImg.iFilledLen = pic_buflen;

    //step 6:generate dest exif pic
    error = ippExifWrite(&cSrcImg, &cDesImg, &cExifInfo);//ipp API
    ASSERT_ERROR( error );

    if(NULL != pHeap){
	free(pHeap);
	pHeap = NULL;
    }

    return pic_mem;
}

};

