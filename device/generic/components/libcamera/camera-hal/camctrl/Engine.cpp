/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

#include <binder/MemoryBase.h>
#include <binder/MemoryHeapBase.h>
#include <binder/MemoryHeapPmem.h>
#include <linux/android_pmem.h>
#include <utils/Log.h>

#include "cam_log.h"
#include "cam_log_mrvl.h"
#include "CameraSetting.h"
#include "ippIP.h"
#include "CameraCtrl.h"
#include "Engine.h"
#include "ippExif.h"
#include "exif_helper.h"

#define LOG_TAG "CameraMrvl"

//#define DUMP_PREVIEW_FRAME
//#define DUMP_STILL
//#define DUMP_SNAPSHOT
#define EXIF_GENERATOR

#define PROP_TUNING	"service.camera.tuning"	//0,1
#define PROP_W_PREVIEW	"service.camera.width_preview"	//0,>0
#define PROP_H_PREVIEW	"service.camera.height_preview"	//0,>0
#define PROP_W_STILL	"service.camera.width_still"	//0,>0
#define PROP_H_STILL	"service.camera.height_still"	//0,>0

namespace android {

    int Engine::getNumberOfCameras(){
	CAM_Error error = CAM_ERROR_NONE;
	CAM_DeviceHandle handle;
	char sSensorName[4][32];

	// get the camera engine handle
	error = CAM_GetHandle(&entry_extisp, &handle);
	//ASSERT_CAM_ERROR(error);
	if (CAM_ERROR_NONE != error)
	    return 0;

	// list all supported sensors
	int iSensorCount;
	error = CAM_SendCommand(handle,
		CAM_CMD_ENUM_SENSORS,
		(CAM_Param)&iSensorCount,
		(CAM_Param)sSensorName);
	//ASSERT_CAM_ERROR(error);
	if (CAM_ERROR_NONE != error)
	    return 0;

	// list all supported sensors
	TRACE_D("All supported sensors:");
	for (int i=0 ; i<iSensorCount ; i++){
	    //init sensor table with existing sensorid, name, facing & orientation on platform
	    CameraSetting::initTableSensorID(sSensorName[i], i);
	    TRACE_D("\t%d - %s", i, sSensorName[i]);
	}

	error = CAM_FreeHandle(&handle);
	//ASSERT_CAM_ERROR(error);
	if (CAM_ERROR_NONE != error)
	    return 0;

	return CameraSetting::getNumOfCameras();
    }

    void Engine::getCameraInfo(int cameraId, struct CameraInfo* cameraInfo)
    {
	CameraSetting camerasetting(cameraId);
	const CameraInfo* infoptr = camerasetting.getCameraInfo();
	if( NULL == infoptr ){
	    TRACE_E("Error occur when get CameraInfo");
	}
	memcpy(cameraInfo, infoptr, sizeof(CameraInfo));
    }

    Engine::Engine(int cameraID):
	mSetting(cameraID),//if cameraID not specified, using 0 by default
	iExpectedPicNum(1)
	//mState(CAM_STATE_IDLE)
    {
	char value[PROPERTY_VALUE_MAX];
	ceInit(&mCEHandle,&mCamera_caps);
	//initialized default shot capability
	mCamera_shotcaps = mCamera_caps.stSupportedShotParams;
	/*
	 * comment this command to use the default setting in camerasetting
	 * comment it for debug only
	 */
	mSetting.setBasicCaps(mCamera_caps);
	/*
	 * choose camera tuning
	 * setprop service.camera.tuning x
	 * if x is set to 0, no camera tuning will be used
	 */
	property_get(PROP_TUNING, value, "1");
	mTuning = atoi(value);
	TRACE_D("%s:"PROP_TUNING"=%d",__FUNCTION__, mTuning);
	if( mTuning != 0 ){
	    /*
	     * update cameraparameter SUPPORTED KEY
	     * all supported tuning parameters from engine are stored in stSupportedShotParams
	     */
	    mSetting.setSupportedSceneModeCap(mCamera_shotcaps);
	    /*
	     * query current used tuning parameters from engine,
	     * update cameraparameter KEY
	     */
	    ceGetCurrentSceneModeCap(mCEHandle,mSetting);
	}

	/*
	 * override preview size/format setting, for debug only
	 * setprop service.camera.width_preview x
	 * setprop service.camera.height_preview y
	 * if x or y is set to 0, we'll use the parameters from class CameraSetting.
	 */
	int width=0,height=0;
	property_get(PROP_W_PREVIEW, value, "0");
	width = atoi(value);
	TRACE_D("%s:"PROP_W_PREVIEW"=%d",__FUNCTION__, width);
	property_get(PROP_H_PREVIEW, value, "0");
	height = atoi(value);
	TRACE_D("%s:"PROP_H_PREVIEW"=%d",__FUNCTION__, height);

	if( width > 0 && height > 0 ){
	    mSetting.setPreviewSize(width, height);
	    const char* v=mSetting.get(CameraParameters::KEY_PREVIEW_SIZE);
	    mSetting.set(CameraParameters::KEY_SUPPORTED_PREVIEW_SIZES, v);
	}

	/*
	 * override preview size/format setting, for debug only
	 * setprop service.camera.width_still x
	 * setprop service.camera.height_still y
	 * if x or y is set to 0, we'll use the parameters from class CameraSetting.
	 */
	width=0,height=0;
	property_get(PROP_W_STILL, value, "0");
	width = atoi(value);
	TRACE_D("%s:"PROP_W_STILL"=%d",__FUNCTION__, width);
	property_get(PROP_H_STILL, value, "0");
	height = atoi(value);
	TRACE_D("%s:"PROP_H_STILL"=%d",__FUNCTION__, height);

	if( width > 0 && height > 0 ){
	    mSetting.setPictureSize(width, height);
	    const char* v=mSetting.get(CameraParameters::KEY_PICTURE_SIZE);
	    mSetting.set(CameraParameters::KEY_SUPPORTED_PICTURE_SIZES, v);
	}
    }

    Engine::~Engine(){
	CAM_Error error = CAM_ERROR_NONE;
	error = CAM_FreeHandle(&mCEHandle);
	ASSERT_CAM_ERROR(error);
    }

    void Engine::enableMsgType(int32_t msgType)
    {
	mMsgEnabled |= msgType;
    }
    void Engine::disableMsgType(int32_t msgType)
    {
	mMsgEnabled &= ~msgType;
    }

    //keep event handler function concise, or else it may affect preview performance.
    void Engine::NotifyEvents(CAM_EventId eEventId,int param,void *pUserData)
    {
	Engine* ptr=static_cast<Engine*>(pUserData);
	TRACE_V("%s:eEventId=%d,param=%d",__FUNCTION__,eEventId,param);
	if(NULL != ptr->mNotifyCb){
	    switch ( eEventId )
	    {
		case CAM_EVENT_FRAME_DROPPED:
		    break;
		case CAM_EVENT_FRAME_READY:
		    TRACE_V("%s:CAM_EVENT_FRAME_READY",__FUNCTION__);
		    break;
		case CAM_EVENT_STILL_SHUTTERCLOSED:
		    TRACE_D("%s:CAM_EVENT_STILL_SHUTTERCLOSED",__FUNCTION__);
		    if (ptr->mMsgEnabled & CAMERA_MSG_SHUTTER)
			ptr->mNotifyCb(CAMERA_MSG_SHUTTER, 0, 0, ptr->mCallbackCookie);
		    break;
		case CAM_EVENT_IN_FOCUS:
		    break;
		case CAM_EVENT_OUTOF_FOCUS:
		    break;
		case CAM_EVENT_FOCUS_AUTO_STOP:
		    TRACE_D("%s:CAM_EVENT_FOCUS_AUTO_STOP:%d",__FUNCTION__,param);
		    if (ptr->mMsgEnabled & CAMERA_MSG_FOCUS){
			ptr->mNotifyCb(CAMERA_MSG_FOCUS,
				((param>0)?true:false), 0, ptr->mCallbackCookie);
		    }
		    break;
		case CAM_EVENT_STILL_ALLPROCESSED:
		    break;
		case CAM_EVENT_FATALERROR:
		    break;
		default:
		    break;
	    }
	}
    }

    void Engine::setCallbacks(notify_callback notify_cb,void* user){
	mNotifyCb = notify_cb;
	mCallbackCookie = user;
    }

    //check previous state:
    //still: then it wanna switch back to preview, we only flush still port buffer,
    //enqueue preview buffers to preview port.
    //idle: then it's first time preview, we need to alloc preview buffers, switch ce
    //state from idle to preview.
    status_t Engine::startPreview(){
	return ceStartPreview(mCEHandle);
    }

    status_t Engine::stopPreview(){
	return ceStopPreview(mCEHandle);
    }

    status_t Engine::setParameters(CameraParameters param){
	status_t ret=NO_ERROR;
	//store the parameters set from service
	ret |= mSetting.setParameters(param);

	//set tuning parameters as specified
	if( mTuning != 0 ){
	    /*
	     * update engine param according to parameters,
	     * including flash/whitebalance/coloreffect/focus/antibanding etc.
	     */
	    ret |= ceUpdateSceneModeSetting(mCEHandle,mSetting);

	    /*
	     * read back setting from engine and update CameraSetting,
	     * flash/whitebalance/focus mode may changed with different scene mode.
	     * refer to Camera.java
	     */
	    ret |= ceGetCurrentSceneModeCap(mCEHandle,mSetting);

	    /*
	     * FIXME:cameraengine may NOT support focus mode while scene mode is enabled,
	     * but camera app may assume autofocus be supported if scene mode key is set,
	     * which may cause camera app quit when doautofocus/takepicture.
	     * refer to Camera.java
	     */
	    if ( 0 != mSetting.get(CameraParameters::KEY_SCENE_MODE) &&
		    0 == mSetting.get(CameraParameters::KEY_FOCUS_MODE) ){
		mSetting.set(CameraParameters::KEY_FOCUS_MODE,
			CameraParameters::FOCUS_MODE_AUTO);
	    }
	}
	return ret;
    }

    CameraParameters Engine::getParameters(){
	CameraParameters params = mSetting.getParameters();
	return params;
    }

    status_t Engine::registerPreviewBuffer(sp<ImageBuf> imagebuf){
	//TODO:
	getPreviewBufReq();

	//
	int bufcnt=imagebuf->getBufCnt();
	for (int i=0;i<bufcnt;i++){
	    CAM_ImageBuffer* pImgBuf = mPreviewImgBuf+i;
	    status_t stat = ImageBuftoCAM_ImageBuffer(imagebuf, i, pImgBuf);
	    if(NO_ERROR != stat){
		TRACE_E("fail in ImageBuftoCAM_ImageBuffer");
		return UNKNOWN_ERROR;
	    }

	    pImgBuf->iFlag =
		BUF_FLAG_PHYSICALCONTIGUOUS |
		BUF_FLAG_L1NONCACHEABLE |
		BUF_FLAG_L2NONCACHEABLE |
		BUF_FLAG_UNBUFFERABLE |
		BUF_FLAG_YUVBACKTOBACK |
		BUF_FLAG_FORBIDPADDING;

	    CAM_Error error = CAM_ERROR_NONE;
	    error = CAM_SendCommand(mCEHandle,
		    CAM_CMD_PORT_ENQUEUE_BUF,
		    (CAM_Param)CAM_PORT_PREVIEW,
		    (CAM_Param)pImgBuf);
	    if(CAM_ERROR_NONE != error)
		return UNKNOWN_ERROR;
	}

	return NO_ERROR;
    }

    status_t Engine::unregisterPreviewBuffers(){
	//get all buffers back
	CAM_Error error = CAM_ERROR_NONE;
	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_PORT_FLUSH_BUF,
		(CAM_Param)CAM_PORT_PREVIEW,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);
	//if(CAM_ERROR_NONE != error)
	//    return UNKNOWN_ERROR;
	/*
	 * TODO:do mem clear if any memory heap/buffer sp is hold
	 * in this class previously, to forcely free all buffers and
	 * prepare for next mem alloc.
	 */
	return NO_ERROR;
    }

    //XXX:check buf req/cnt here
    //TODO: we need only the buf index as input after register buffer.
    //status_t Engine::enqPreviewBuffer(sp<ImageBuf> imagebuf, int index){
    status_t Engine::enqPreviewBuffer(int index){
	CAM_ImageBuffer* pPreviewImgBuf=&mPreviewImgBuf[index];
	CAM_Error error = CAM_ERROR_NONE;
	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_PORT_ENQUEUE_BUF,
		(CAM_Param)CAM_PORT_PREVIEW,
		(CAM_Param)pPreviewImgBuf);
	ASSERT_CAM_ERROR(error);
	return NO_ERROR;
    }

    status_t Engine::deqPreviewBuffer(int* index){
	CAM_Error error = CAM_ERROR_NONE;
	error = ceGetPreviewFrame(mCEHandle,index);
	if(CAM_ERROR_NONE != error)
	    return UNKNOWN_ERROR;
	else
	    return NO_ERROR;
    }

    //--------------
    status_t Engine::registerStillBuffer(sp<ImageBuf> imagebuf){
	//TODO:
	getStillBufReq();

	//
	int bufcnt=imagebuf->getBufCnt();
	for (int i=0;i<bufcnt;i++){
	    CAM_ImageBuffer* pImgBuf = mStillImgBuf+i;
	    status_t stat = ImageBuftoCAM_ImageBuffer(imagebuf, i, pImgBuf);
	    if(NO_ERROR != stat){
		TRACE_E("fail in ImageBuftoCAM_ImageBuffer");
		return UNKNOWN_ERROR;
	    }

	    pImgBuf->iFlag =
		BUF_FLAG_PHYSICALCONTIGUOUS |
		BUF_FLAG_L1NONCACHEABLE |
		BUF_FLAG_L2NONCACHEABLE |
		BUF_FLAG_UNBUFFERABLE |
		BUF_FLAG_YUVBACKTOBACK |
		BUF_FLAG_FORBIDPADDING;

	    CAM_Error error = CAM_ERROR_NONE;
	    error = CAM_SendCommand(mCEHandle,
		    CAM_CMD_PORT_ENQUEUE_BUF,
		    (CAM_Param)CAM_PORT_STILL,
		    (CAM_Param)pImgBuf);
	    if(CAM_ERROR_NONE != error)
		return UNKNOWN_ERROR;
	}
	return NO_ERROR;
    }

    status_t Engine::unregisterStillBuffers(){
	CAM_Error error = CAM_ERROR_NONE;
	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_PORT_FLUSH_BUF,
		(CAM_Param)CAM_PORT_STILL,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);
	//if(CAM_ERROR_NONE != error)
	//    return UNKNOWN_ERROR;
	/*
	 * TODO:do mem clear if any memory heap/buffer sp is hold
	 * in this class previously, to forcely free all buffers and
	 * prepare for next mem alloc.
	 */
	return NO_ERROR;
    }

    //XXX:check buf req/cnt here
    //TODO: we need only the buf index as input after register buffer.
    //status_t Engine::enqStillBuffer(sp<ImageBuf> imagebuf, int index){
    status_t Engine::enqStillBuffer(int index){
	CAM_ImageBuffer* pStillImgBuf=&mStillImgBuf[index];
	CAM_Error error = CAM_ERROR_NONE;
	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_PORT_ENQUEUE_BUF,
		(CAM_Param)CAM_PORT_STILL,
		(CAM_Param)pStillImgBuf);
	ASSERT_CAM_ERROR(error);
	return NO_ERROR;
    }

    status_t Engine::deqStillBuffer(int* index){
	CAM_Error error = CAM_ERROR_NONE;
	status_t stat;
	stat = ceStartStill(mCEHandle);
	if(NO_ERROR != stat){
	    TRACE_E("ceStartStill fail");
	    return stat;
	}

	error = ceGetStillFrame(mCEHandle,index);
	if(CAM_ERROR_NONE != error){
	    TRACE_E("ceGetStillFrame fail");
	    return UNKNOWN_ERROR;
	}
	else
	    return NO_ERROR;
    }

    //FIXME:: we have to use the hacking method to get image detail info,
    //the method "getUserData()" break the encapsulation of class CameraCtrl.
    //If we could implement the buf detail info outside, then we could fix this
    //workaround.
    void* Engine::getUserData(void* ptr){
	int index = *static_cast<int*>(ptr);
	return static_cast<void*>(&mStillImgBuf[index]);
    }

    sp<IMemory> Engine::getExifImage(int index)
    {
	sp<MemoryHeapBase> pic_heap=NULL;
	sp<MemoryBase> pic_mem=NULL;
	ssize_t offset = 0;
	size_t size = 0;
	size_t pic_buflen = 0;
	int heap_size = 0;

	CAM_ImageBuffer* pImgBuf_still = &mStillImgBuf[index];
#ifdef EXIF_GENERATOR
	pic_mem = ExifGenerator(pImgBuf_still, mSetting);
#else//EXIF_GENERATOR
	//raw jpeg pic buffer length
	pic_buflen = pImgBuf_still->iFilledLen[0] +
	    pImgBuf_still->iFilledLen[1] +
	    pImgBuf_still->iFilledLen[2];

	pic_heap = new MemoryHeapBase(pic_buflen);
	heap_size = (int)pic_heap->getSize();
	pic_mem = new MemoryBase(pic_heap, 0, heap_size);

	pic_mem->getMemory(&offset, &size);
	uint8_t* ptr_pic = (uint8_t*)pic_heap->base()+offset;
	memset(ptr_pic,0,size);

	memcpy(ptr_pic, pImgBuf_still->pBuffer[0], pImgBuf_still->iFilledLen[0]);
	if(pImgBuf_still->iFilledLen[1])
	    memcpy(ptr_pic + pImgBuf_still->iFilledLen[0],
		    pImgBuf_still->pBuffer[1],
		    pImgBuf_still->iFilledLen[1]);
	if(pImgBuf_still->iFilledLen[2])
	    memcpy(ptr_pic + pImgBuf_still->iFilledLen[0] + pImgBuf_still->iFilledLen[1],
		    pImgBuf_still->pBuffer[2],
		    pImgBuf_still->iFilledLen[2]);
#endif//EXIF_GENERATOR
	return pic_mem;
    }

    status_t Engine::autoFocus()
    {
	//focus mode
	int cesetting                           = -1;
	CAM_ShotModeCapability& camera_shotcaps  = mCamera_shotcaps;

	const char* v=mSetting.get(CameraParameters::KEY_FOCUS_MODE);
	if( 0 == v){
	    return UNKNOWN_ERROR;
	}

	if(0 != strcmp(v,CameraParameters::FOCUS_MODE_AUTO) &&
		0 != strcmp(v,CameraParameters::FOCUS_MODE_MACRO)){
	    TRACE_E("app should not call autoFocus in mode %s",v);
	    return UNKNOWN_ERROR;
	}

	CAM_Error error = CAM_ERROR_NONE;
	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_START_FOCUS,
		(CAM_Param)CAM_AFMODE_CENTER,
		UNUSED_PARAM);
	//ASSERT_CAM_ERROR(error);
	if (CAM_ERROR_NONE != error){
	    TRACE_E("start focus failed!");
	    return UNKNOWN_ERROR;
	}

	TRACE_D("%s:start focus ok", __FUNCTION__);
	return NO_ERROR;
    }

    status_t Engine::cancelAutoFocus()
    {
	const char* v=mSetting.get(CameraParameters::KEY_FOCUS_MODE);
	if( 0 == v){
	    return NO_ERROR;
	}

	if(0 != strcmp(v,CameraParameters::FOCUS_MODE_AUTO) &&
		0 != strcmp(v,CameraParameters::FOCUS_MODE_MACRO)){
	    TRACE_E("app should not call calcelAutoFocus in mode %s",v);
	}

	CAM_Error error = CAM_ERROR_NONE;
	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_CANCEL_FOCUS,
		UNUSED_PARAM,
		UNUSED_PARAM);
	//ASSERT_CAM_ERROR(error);
	if (CAM_ERROR_NONE != error){
	    TRACE_E("cancel focus failed!");
	    return UNKNOWN_ERROR;
	}

	TRACE_D("%s:cancel focus ok", __FUNCTION__);
	return NO_ERROR;
    }

    status_t Engine::startFlash()
    {
		CAM_Error error = CAM_ERROR_NONE;
		error = CAM_SendCommand(mCEHandle,
			CAM_CMD_START_FLASH,
			UNUSED_PARAM,
			UNUSED_PARAM);
		//ASSERT_CAM_ERROR(error);
		if (CAM_ERROR_NONE != error){
		    TRACE_E("start flash failed!");
		    return UNKNOWN_ERROR;
		}

		TRACE_D("%s:start flash ok", __FUNCTION__);

		return NO_ERROR;
    }

    status_t Engine::stopFlash()
    {
		CAM_Error error = CAM_ERROR_NONE;
		error = CAM_SendCommand(mCEHandle,
			CAM_CMD_STOP_FLASH,
			UNUSED_PARAM,
			UNUSED_PARAM);
		//ASSERT_CAM_ERROR(error);
		if (CAM_ERROR_NONE != error){
		    TRACE_E("stop flash failed!");
		    return UNKNOWN_ERROR;
		}

		TRACE_D("%s:stop flash ok", __FUNCTION__);
		return NO_ERROR;
    }

    bool Engine::isFlashOn()
    {
		CAM_Error error = CAM_ERROR_NONE;
		CAM_Bool isOn;
		error = CAM_SendCommand(mCEHandle,
			CAM_CMD_GET_FLASH_ON,
			(CAM_Param)&isOn,
			UNUSED_PARAM);
		//ASSERT_CAM_ERROR(error);
		if (CAM_ERROR_NONE != error){
		    TRACE_E("get flash on failed!");
			return CAM_FALSE;
		}

		return isOn;
    }

    status_t Engine::getBufCnt(int* previewbufcnt,int* stillbufcnt,int* videobufcnt)const
    {
	return mSetting.getBufCnt(previewbufcnt, stillbufcnt, videobufcnt);
    }

    CAM_ImageBufferReq Engine::getPreviewBufReq()
    {
	CAM_Error error = CAM_ERROR_NONE;

	/*
	 * check current CE status, which should be IDLE
	 */
	CAM_CaptureState state;
	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_GET_STATE,
		(CAM_Param)&state,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);
	if(CAM_CAPTURESTATE_IDLE != state){
	    TRACE_E("wrong CE state");
	    exit -1;
	}

	/* Set preview parameters */
	CAM_Size szPreviewSize;
	mSetting.getPreviewSize(&szPreviewSize.iWidth,
		&szPreviewSize.iHeight);
	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_PORT_SET_SIZE,
		(CAM_Param)CAM_PORT_PREVIEW,
		(CAM_Param)&szPreviewSize);
	ASSERT_CAM_ERROR(error);

	//android framework doc requires that preview/picture format
	//is in supported parameters.
	const char* v = mSetting.getPreviewFormat();
	int ePreviewFormat = mSetting.map_andr2ce(CameraSetting::map_imgfmt, v);
	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_PORT_SET_FORMAT,
		(CAM_Param)CAM_PORT_PREVIEW,
		(CAM_Param)ePreviewFormat);
	ASSERT_CAM_ERROR(error);

	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_PORT_GET_BUFREQ,
		(CAM_Param)CAM_PORT_PREVIEW,
		(CAM_Param)&mPreviewBufReq);
	ASSERT_CAM_ERROR(error);
	return mPreviewBufReq;
    }

    CAM_ImageBufferReq Engine::getStillBufReq(){
	CAM_Error error = CAM_ERROR_NONE;

	/*
	 * Set picture parameters
	 */
	CAM_Size szPictureSize;
	mSetting.getPictureSize(&szPictureSize.iWidth,
		&szPictureSize.iHeight);

	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_PORT_SET_SIZE,
		(CAM_Param)CAM_PORT_STILL,
		(CAM_Param)&szPictureSize);
	ASSERT_CAM_ERROR(error);


	int jpeg_quality = mSetting.getInt(CameraParameters::KEY_JPEG_QUALITY);
	if(jpeg_quality > mCamera_caps.stSupportedJPEGParams.iMaxQualityFactor ||
		jpeg_quality < mCamera_caps.stSupportedJPEGParams.iMinQualityFactor){
	    TRACE_E("invalid quality factor=%d. supported range [%d,%d]",
		    jpeg_quality,mCamera_caps.stSupportedJPEGParams.iMaxQualityFactor,
		    mCamera_caps.stSupportedJPEGParams.iMinQualityFactor);
	    jpeg_quality = mCamera_caps.stSupportedJPEGParams.iMaxQualityFactor;
	}

	//default jpeg param
	CAM_JpegParam jpegparam = {
	    0,	// 420
	    jpeg_quality, // quality factor
	    CAM_FALSE, // enable exif
	    CAM_FALSE, // embed thumbnail
	    0, 0,
	};

	const char* v = mSetting.getPictureFormat();
	int ePictureFormat = mSetting.map_andr2ce(CameraSetting::map_imgfmt, v);
	if (CAM_IMGFMT_JPEG == ePictureFormat){
	    error = CAM_SendCommand(mCEHandle,
		    CAM_CMD_SET_JPEGPARAM,
		    (CAM_Param)&jpegparam,
		    UNUSED_PARAM);
	    ASSERT_CAM_ERROR(error);
	}
	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_PORT_SET_FORMAT,
		(CAM_Param)CAM_PORT_STILL,
		(CAM_Param)ePictureFormat);
	ASSERT_CAM_ERROR(error);

	error = CAM_SendCommand(mCEHandle,
		CAM_CMD_PORT_GET_BUFREQ,
		(CAM_Param)CAM_PORT_STILL,
		(CAM_Param)&mStillBufReq);
	ASSERT_CAM_ERROR(error);

	return mStillBufReq;
    }
    //----------------------------------------------------------------------------------------
    CAM_Error Engine::ceInit(CAM_DeviceHandle *pHandle,CAM_CameraCapability *pcamera_caps)
    {
	CAM_Error error = CAM_ERROR_NONE;
	int ret;
	CAM_DeviceHandle handle;
	CAM_PortCapability *pCap;

	// get the camera engine handle
	error = CAM_GetHandle(&entry_extisp, &handle);
	*pHandle = handle;
	ASSERT_CAM_ERROR(error);

	// select the proper sensor id
	int sensorid = mSetting.getSensorID();
	const char* sensorname = mSetting.getSensorName();
	error = CAM_SendCommand(handle,
		CAM_CMD_SET_SENSOR_ID,
		(CAM_Param)sensorid,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);
	TRACE_D("Current sensor index:");
	TRACE_D("\t%d - %s", sensorid, sensorname);

	error = CAM_SendCommand(handle,
		CAM_CMD_QUERY_CAMERA_CAP,
		(CAM_Param)sensorid,
		(CAM_Param)pcamera_caps);
	ASSERT_CAM_ERROR(error);
#if 1
	error = CAM_SendCommand(handle,
		CAM_CMD_SET_EVENTHANDLER,
		(CAM_Param)NotifyEvents,
		(CAM_Param)this);
	ASSERT_CAM_ERROR(error);
#endif

	error = CAM_SendCommand(handle,
		CAM_CMD_SET_SENSOR_ORIENTATION,
		(CAM_Param)CAM_FLIPROTATE_NORMAL,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	return error;
    }

    status_t Engine::ceUpdateSceneModeSetting(const CAM_DeviceHandle &handle,
	    CameraSetting& setting)
    {
	const char* v;
	int scenemode;
	CAM_FocusMode FocusMode;

	status_t ret = NO_ERROR;
	CAM_Error error = CAM_ERROR_NONE;
	CAM_ShotModeCapability camera_shotcaps;

	/*
	 * update scene mode as required if engine support
	 */
	//current scene mode
	error = CAM_SendCommand(handle,
		CAM_CMD_GET_SHOTMODE,
		(CAM_Param)&scenemode,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	//target scene mode
	v = setting.get(CameraParameters::KEY_SCENE_MODE);
	if( 0 != v){
	    int scenemode_target = setting.map_andr2ce(CameraSetting::map_scenemode, v);

	    //update scene mode if the target scenemode is different from current
	    if (scenemode_target != scenemode &&
		    scenemode_target != -1){
		//update ce scene mode
		error = CAM_SendCommand(handle,
			CAM_CMD_SET_SHOTMODE,
			(CAM_Param)scenemode_target,
			UNUSED_PARAM);
		ASSERT_CAM_ERROR(error);
		scenemode = scenemode_target;
	    }
	}
	TRACE_D("%s:scene mode=%s", __FUNCTION__, v);
	//query tuning parameters supported in current scene mode
	if( 0 != v){
	    error = CAM_SendCommand(handle,
		    CAM_CMD_QUERY_SHOTMODE_CAP,
		    (CAM_Param)scenemode,
		    (CAM_Param)&camera_shotcaps);
	    ASSERT_CAM_ERROR(error);
	    //update shot capability under new shot mode.
	    mCamera_shotcaps = camera_shotcaps;
	}
	else{
	    TRACE_E("No valid scene mode, return.");
	    ret = BAD_VALUE;
	    return ret;
	}

	/*
	 * update focus mode as required if engine support
	 */
	//current focus mode
	error = CAM_SendCommand(handle,
		CAM_CMD_GET_FOCUSMODE,
		(CAM_Param)&FocusMode,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	//target focus mode
	v = setting.get(CameraParameters::KEY_FOCUS_MODE);
	if( 0 != v){
	    int focusmode_target = setting.map_andr2ce(CameraSetting::map_focus, v);

	    //update focus mode if the target focus mode is different from current
	    if (focusmode_target != FocusMode){
		//update ce focus mode
		error = CAM_SendCommand(handle,
			CAM_CMD_SET_FOCUSMODE,
			(CAM_Param)focusmode_target,
			UNUSED_PARAM);
		ASSERT_CAM_ERROR(error);
	    }
	}
	TRACE_D("%s:focus mode=%s", __FUNCTION__, v);

	/*
	 * update other parameters according to current used scene mode
	 */
	int cesetting;
	int ce_idx;
	//white balance setting
	v = setting.get(CameraParameters::KEY_WHITE_BALANCE);
	if( 0 != v){
	    cesetting = setting.map_andr2ce(CameraSetting::map_whitebalance, v);
	    if (-1 != cesetting){
		for (int i=0; i < camera_shotcaps.iSupportedWBModeCnt; i++){
		    ce_idx = camera_shotcaps.eSupportedWBMode[i];
		    if(ce_idx == cesetting){
			error = CAM_SendCommand(handle,
				CAM_CMD_SET_WHITEBALANCEMODE,
				(CAM_Param)cesetting,
				UNUSED_PARAM);
			ASSERT_CAM_ERROR(error);
			TRACE_D("%s:white balance=%s", __FUNCTION__, v);
		    }
		}
	    }
	}

	//effect setting
	v = setting.get(CameraParameters::KEY_EFFECT);
	if( 0 != v){
	    cesetting = setting.map_andr2ce(CameraSetting::map_coloreffect, v);
	    if (-1 != cesetting){
		for (int i=0; i < camera_shotcaps.iSupportedColorEffectCnt; i++){
		    ce_idx = camera_shotcaps.eSupportedColorEffect[i];
		    if(ce_idx == cesetting){
			error = CAM_SendCommand(handle,
				CAM_CMD_SET_COLOREFFECT,
				(CAM_Param)cesetting,
				UNUSED_PARAM);
			ASSERT_CAM_ERROR(error);
			TRACE_D("%s:color effect=%s", __FUNCTION__, v);
		    }
		}
	    }
	}

	//bandfilter setting
	v = setting.get(CameraParameters::KEY_ANTIBANDING);
	if( 0 != v){
	    cesetting = setting.map_andr2ce(CameraSetting::map_bandfilter, v);
	    if (-1 != cesetting){
		for (int i=0; i < camera_shotcaps.iSupportedBdFltModeCnt; i++){
		    ce_idx = camera_shotcaps.eSupportedBdFltMode[i];
		    if(ce_idx == cesetting){
			error = CAM_SendCommand(handle,
				CAM_CMD_SET_BANDFILTER,
				(CAM_Param)cesetting,
				UNUSED_PARAM);
			ASSERT_CAM_ERROR(error);
			TRACE_D("%s:antibanding=%s", __FUNCTION__, v);
		    }
		}
	    }
	}

	//flash mode
	v = setting.get(CameraParameters::KEY_FLASH_MODE);
	if( 0 != v){
	    cesetting = setting.map_andr2ce(CameraSetting::map_flash, v);
	    if (-1 != cesetting){
		for (int i=0; i < camera_shotcaps.iSupportedFlashModeCnt; i++){
		    ce_idx = camera_shotcaps.eSupportedFlashMode[i];
		    if(ce_idx == cesetting){
			error = CAM_SendCommand(handle,
				CAM_CMD_SET_FLASHMODE,
				(CAM_Param)cesetting,
				UNUSED_PARAM);
			ASSERT_CAM_ERROR(error);
			TRACE_D("%s:flash mode=%s", __FUNCTION__, v);
		    }
		}
	    }
	}

	//EVC
	if ( 0 != camera_shotcaps.iEVCompStepQ16){
	    v = setting.get(CameraParameters::KEY_EXPOSURE_COMPENSATION);
	    if( 0 != v){
		int exposureQ16 = atoi(v) * camera_shotcaps.iEVCompStepQ16;
		// verify the range of the settings
		if ((exposureQ16 <= camera_shotcaps.iMaxEVCompQ16) &&
			(exposureQ16 >= camera_shotcaps.iMinEVCompQ16) ){
		    error = CAM_SendCommand(handle,
			    CAM_CMD_SET_EVCOMPENSATION,
			    (CAM_Param)exposureQ16,
			    UNUSED_PARAM);
		    ASSERT_CAM_ERROR(error);
		    TRACE_D("%s:exposure compensation=%s", __FUNCTION__, v);
		}
	    }
	}

	//digital zoom
	const int minzoom = camera_shotcaps.iMinDigZoomQ16;
	const int maxzoom = camera_shotcaps.iMaxDigZoomQ16;
	const int cezoomstep = camera_shotcaps.iDigZoomStepQ16;
	if((0x1<<16) != maxzoom){
	    v = setting.get(CameraParameters::KEY_ZOOM);
	    int cedigzoom = minzoom + atoi(v) * cezoomstep;
	    if (cedigzoom < minzoom){
		TRACE_E("Invalid zoom value:%d. set to min:%d",cedigzoom,minzoom);
		cedigzoom = minzoom;
		ret = BAD_VALUE;
	    }
	    else if (cedigzoom > maxzoom){
		TRACE_E("Invalid zoom value:%d. set to max:%d",cedigzoom,maxzoom);
		cedigzoom = maxzoom;
		ret = BAD_VALUE;
	    }
	    error = CAM_SendCommand(handle,
		    CAM_CMD_SET_DIGZOOM,
		    (CAM_Param)cedigzoom,
		    UNUSED_PARAM );
	    ASSERT_CAM_ERROR(error);
	    TRACE_D("%s:digital zoom=%s", __FUNCTION__, v);
	}

	// check fps range
	{
	    int min_fps, max_fps;
	    setting.getPreviewFpsRange(&min_fps, &max_fps);
	    if((min_fps<0) || (max_fps<0) || (min_fps > max_fps)){
		TRACE_E("invalid fps range: min_fps=%d, max_fps=%d",
			min_fps,max_fps);
		ret = BAD_VALUE;
	    }
	}

	/* brightness */
	v = setting.get("brightness");
	if( 0 != v){
		int brightness = atoi(v);
		if ((brightness <= camera_shotcaps.iMaxBrightness) &&
			(brightness >= camera_shotcaps.iMinBrightness) ){
			error = CAM_SendCommand(handle,
				CAM_CMD_SET_BRIGHTNESS,
				(CAM_Param)brightness,
				UNUSED_PARAM);
			ASSERT_CAM_ERROR(error);
			TRACE_D("%s:brightness=%s", __FUNCTION__, v);
		}
	}

	/* contrast */
	v = setting.get("contrast");
	if( 0 != v){
		int contrast = atoi(v);
		if ((contrast <= camera_shotcaps.iMaxContrast) &&
			(contrast >= camera_shotcaps.iMinContrast) ){
			error = CAM_SendCommand(handle,
				CAM_CMD_SET_CONTRAST,
				(CAM_Param)contrast,
				UNUSED_PARAM);
			ASSERT_CAM_ERROR(error);
			TRACE_D("%s:contrast=%s", __FUNCTION__, v);
		}
	}

	/* flash brightness */
	v = setting.get("flash_brightness");
	if( 0 != v){
		int flash_brightness = atoi(v);
		if ((flash_brightness <= camera_shotcaps.iMaxFlashBrightness) &&
			(flash_brightness >= camera_shotcaps.iMinFlashBrightness) ){
			error = CAM_SendCommand(handle,
				CAM_CMD_SET_FLASH_BRIGHTNESS,
				(CAM_Param)flash_brightness,
				UNUSED_PARAM);
			ASSERT_CAM_ERROR(error);
			TRACE_D("%s:flash_brightness=%s", __FUNCTION__, v);
		}
	}

	return ret;
    }

    status_t Engine::ceStartPreview(const CAM_DeviceHandle &handle)
    {
	CAM_Error error = CAM_ERROR_NONE;
	CAM_CaptureState state;

	error = CAM_SendCommand(handle,
		CAM_CMD_GET_STATE,
		(CAM_Param)&state,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	if (state == CAM_CAPTURESTATE_PREVIEW){
	    return NO_ERROR;
	}
	else if (state == CAM_CAPTURESTATE_STILL ||
		state == CAM_CAPTURESTATE_VIDEO ||
		state == CAM_CAPTURESTATE_IDLE
		){
	    error = CAM_SendCommand(handle,
		    CAM_CMD_SET_STATE,
		    (CAM_Param)CAM_CAPTURESTATE_PREVIEW,
		    UNUSED_PARAM);
	    //ASSERT_CAM_ERROR(error);
	    if(CAM_ERROR_NONE != error){
		return UNKNOWN_ERROR;
	    }
	    else{
		return NO_ERROR;
	    }
	}
	else{
	    TRACE_E("Unknown error happened,wrong state");
	    return UNKNOWN_ERROR;
	}
    }

    /*
     * ONLY switch to capture state for ce,
     * assume that ce was already in preview state
     */
    status_t Engine::ceStartStill(const CAM_DeviceHandle &handle)
    {

	CAM_Error error = CAM_ERROR_NONE;
#if 0
	// Enqueue all still image buffers
	for (int i=0; i<kStillBufCnt; i++){
	    error = CAM_SendCommand(handle,
		    CAM_CMD_PORT_ENQUEUE_BUF,
		    (CAM_Param)CAM_PORT_STILL,
		    (CAM_Param)&mStillImgBuf[i]);
	    ASSERT_CAM_ERROR(error);
	}
#endif
	error = CAM_SendCommand(handle,
		CAM_CMD_GET_STILL_BURST,
		(CAM_Param)&iExpectedPicNum,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	// switch to still capture state
	error = CAM_SendCommand(handle,
		CAM_CMD_SET_STATE,
		(CAM_Param)CAM_CAPTURESTATE_STILL,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	return NO_ERROR;
    }



    /*
     * 1.set ce state from preview to idle
     * 2.unuse buf on preview port
     */
    status_t Engine::ceStopPreview(const CAM_DeviceHandle &handle)
    {

	CAM_Error error = CAM_ERROR_NONE;
	CAM_CaptureState state;

	error = CAM_SendCommand(handle,
		CAM_CMD_GET_STATE,
		(CAM_Param)&state,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	if (state == CAM_CAPTURESTATE_STILL){
	    error = CAM_SendCommand(handle,
		    CAM_CMD_SET_STATE,
		    (CAM_Param)CAM_CAPTURESTATE_PREVIEW,
		    UNUSED_PARAM);
	    ASSERT_CAM_ERROR(error);

	    error = CAM_SendCommand(handle,
		    CAM_CMD_SET_STATE,
		    (CAM_Param)CAM_CAPTURESTATE_IDLE,
		    UNUSED_PARAM);
	    ASSERT_CAM_ERROR(error);
	}
	else if (state == CAM_CAPTURESTATE_VIDEO){
	    error = CAM_SendCommand(handle,
		    CAM_CMD_SET_STATE,
		    (CAM_Param)CAM_CAPTURESTATE_PREVIEW,
		    UNUSED_PARAM);
	    ASSERT_CAM_ERROR(error);

	    error = CAM_SendCommand(handle,
		    CAM_CMD_SET_STATE,
		    (CAM_Param)CAM_CAPTURESTATE_IDLE,
		    UNUSED_PARAM);
	    ASSERT_CAM_ERROR(error);
	}
	else if (state == CAM_CAPTURESTATE_PREVIEW){
	    error = CAM_SendCommand(handle,
		    CAM_CMD_SET_STATE,
		    (CAM_Param)CAM_CAPTURESTATE_IDLE,
		    UNUSED_PARAM);
	    ASSERT_CAM_ERROR(error);
	}

	error = CAM_SendCommand(handle,
		CAM_CMD_PORT_FLUSH_BUF,
		(CAM_Param)CAM_PORT_ANY,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);
	return NO_ERROR;
    }


#ifdef DUMP_PREVIEW_FRAME
    int dump_frame_buffer(int counter,CAM_ImageBuffer *pImgBuf)
    {
	char fname[100];
	sprintf(fname,"/data/video%d.yuv",counter);
	TRACE_D("dump-preview-buf to:%s",fname);
	//FILE *fp_video = fopen(fname, "wb");
	FILE *fp_video = fopen(fname, "ab");
	if (fp_video) {
	    fwrite(pImgBuf->pBuffer[0], 1, pImgBuf->iFilledLen[0], fp_video);
	    fwrite(pImgBuf->pBuffer[1], 1, pImgBuf->iFilledLen[1], fp_video);
	    fwrite(pImgBuf->pBuffer[2], 1, pImgBuf->iFilledLen[2], fp_video);
	}
	fclose(fp_video);
	return 0;
    }
#endif

    CAM_Error Engine::ceGetPreviewFrame(const CAM_DeviceHandle &handle,int *index)
    {
	TRACE_V("-%s", __FUNCTION__);

	CAM_Error error = CAM_ERROR_NONE;
	CAM_ImageBuffer *pImgBuf;

	int i;
	IppStatus ippret;

	int port = CAM_PORT_PREVIEW;
	error = CAM_SendCommand(handle,
		CAM_CMD_PORT_DEQUEUE_BUF,
		(CAM_Param)&port,
		(CAM_Param)&pImgBuf);

	//filter the v4l2 polling failure error
	if (CAM_ERROR_FATALERROR == error) {
	    TRACE_D("%s dequeue return with error %d, restore cameraengine", __FUNCTION__, error);
	    error = CAM_SendCommand(handle,
		    CAM_CMD_SET_STATE,
		    (CAM_Param)CAM_CAPTURESTATE_IDLE,
		    UNUSED_PARAM);
	    ASSERT_CAM_ERROR(error);

	    //only recover engine to preview status if preview thread is not request to exit
	    //if (CAM_STATE_PREVIEW == mState){
	    error = CAM_SendCommand(handle,
		    CAM_CMD_SET_STATE,
		    (CAM_Param)CAM_CAPTURESTATE_PREVIEW,
		    UNUSED_PARAM);
	    ASSERT_CAM_ERROR(error);
	    //}
	    return CAM_ERROR_FATALERROR;
	}
	//check any other errors
	if (CAM_ERROR_NONE != error) {
	    TRACE_D("%s x with error %d", __FUNCTION__, error);
	    return error;
	}

	*index = pImgBuf->iUserIndex;

#ifdef DUMP_PREVIEW_FRAME
	{
	    //DEBUG:dmup preview frame buffer
	    static int counter=0;
	    counter ++;
	    if (counter >= 5000){
		counter = 5000;
		//else if(counter >2000 && counter < 2020)
	    }
	    else if(counter%50 == 0){
		//dump_frame_buffer(counter,pImgBuf);
		dump_frame_buffer(12345,pImgBuf);
	    }
	}
#endif
	return error;
    }

    CAM_Error Engine::ceGetStillFrame(const CAM_DeviceHandle &handle,int *index)
    {
	int port;
	CAM_Error error = CAM_ERROR_NONE;
	CAM_ImageBuffer *pImgBuf;
	do{
	    port = CAM_PORT_STILL;
	    error = CAM_SendCommand(handle,
		    CAM_CMD_PORT_DEQUEUE_BUF,
		    (CAM_Param)&port,
		    (CAM_Param)&pImgBuf);
		
	    if (CAM_ERROR_NONE == error){
		break;
	    }
	    else if (CAM_ERROR_FATALERROR == error) {
		TRACE_D("%s dequeue return with error %d, restore cameraengine", __FUNCTION__, error);
		error = CAM_SendCommand(handle,
			CAM_CMD_SET_STATE,
			(CAM_Param)CAM_CAPTURESTATE_PREVIEW,
			UNUSED_PARAM);
		ASSERT_CAM_ERROR(error);

		error = CAM_SendCommand(handle,
			CAM_CMD_SET_STATE,
			(CAM_Param)CAM_CAPTURESTATE_IDLE,
			UNUSED_PARAM);
		ASSERT_CAM_ERROR(error);

		error = CAM_SendCommand(handle,
			CAM_CMD_SET_STATE,
			(CAM_Param)CAM_CAPTURESTATE_PREVIEW,
			UNUSED_PARAM);
		ASSERT_CAM_ERROR(error);

		error = CAM_SendCommand(handle,
			CAM_CMD_SET_STATE,
			(CAM_Param)CAM_CAPTURESTATE_STILL,
			UNUSED_PARAM);
		ASSERT_CAM_ERROR(error);
	    }
	    else{
		ASSERT_CAM_ERROR(error);
	    }
	}while(1);
#if 1
	//TODO: we have to switch back to preview directly after still capture,
	//as engine donot permit setting whitebalance etc. parameters in still state.
	error = CAM_SendCommand(handle,
		CAM_CMD_SET_STATE,
		(CAM_Param)CAM_CAPTURESTATE_PREVIEW,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);
#endif

#ifdef DUMP_STILL
	{//NOTES:dump yuv/jpeg still here, iFilledLen[1,2] should be filled correctly
	    TRACE_D("DUMP still pic to /data/still.pic");
	    FILE *fp_still = fopen("/data/still.pic", "wb");
	    if (fp_still) {
		fwrite(pImgBuf_still->pBuffer[0], 1, pImgBuf_still->iFilledLen[0], fp_still);
		fwrite(pImgBuf_still->pBuffer[1], 1, pImgBuf_still->iFilledLen[1], fp_still);
		fwrite(pImgBuf_still->pBuffer[2], 1, pImgBuf_still->iFilledLen[2], fp_still);
	    }
	    fclose(fp_still);
	}
#endif
	*index = pImgBuf->iUserIndex;
	return error;
    }

    CAM_Error Engine::ceGetCurrentSceneModeCap(const CAM_DeviceHandle &handle,
	    CameraSetting& setting)
    {
	CAM_Error error = CAM_ERROR_NONE;
	String8 v = String8("");

	// scene mode
	int scenemode;
	error = CAM_SendCommand(handle,
		CAM_CMD_GET_SHOTMODE,
		(CAM_Param)&scenemode,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	v = String8("");
	v = setting.map_ce2andr(CameraSetting::map_scenemode,scenemode);
	if( v != String8("") ){
	    setting.set(CameraParameters::KEY_SCENE_MODE,v.string());
	    TRACE_D("%s:CE used scene mode:%s",__FUNCTION__,v.string());
	}

	// white balance
	CAM_WhiteBalanceMode WhiteBalance;
	error = CAM_SendCommand(handle,
		CAM_CMD_GET_WHITEBALANCEMODE,
		(CAM_Param)&WhiteBalance,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	v = String8("");
	v = setting.map_ce2andr(CameraSetting::map_whitebalance,WhiteBalance);
	if( v != String8("") ){
	    setting.set(CameraParameters::KEY_WHITE_BALANCE,v.string());
	    TRACE_D("%s:CE used whitebalance:%s",__FUNCTION__,v.string());
	}

	// color effect
	CAM_ColorEffect ColorEffect;
	error = CAM_SendCommand(handle,
		CAM_CMD_GET_COLOREFFECT,
		(CAM_Param)&ColorEffect,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	v = String8("");
	v = setting.map_ce2andr(CameraSetting::map_coloreffect,ColorEffect);
	if( v != String8("") ){
	    setting.set(CameraParameters::KEY_EFFECT,v.string());
	    TRACE_D("%s:CE used color effect:%s",__FUNCTION__,v.string());
	}

	// bandfilter
	CAM_BandFilterMode BandFilter;
	error = CAM_SendCommand(handle,
		CAM_CMD_GET_BANDFILTER,
		(CAM_Param)&BandFilter,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	v = String8("");
	v = setting.map_ce2andr(CameraSetting::map_bandfilter,BandFilter);
	if( v != String8("") ){
	    setting.set(CameraParameters::KEY_ANTIBANDING,v.string());
	    TRACE_D("%s:CE used antibanding:%s",__FUNCTION__,v.string());
	}

	// flash mode
	CAM_FlashMode FlashMode;
	error = CAM_SendCommand(handle,
		CAM_CMD_GET_FLASHMODE,
		(CAM_Param)&FlashMode,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	v = String8("");
	v = setting.map_ce2andr(CameraSetting::map_flash,FlashMode);
	if( v != String8("") ){
	    setting.set(CameraParameters::KEY_FLASH_MODE,v.string());
	    TRACE_D("%s:CE used flash mode:%s",__FUNCTION__,v.string());
	}

	// focus mode
	CAM_FocusMode FocusMode;
	error = CAM_SendCommand(handle,
		CAM_CMD_GET_FOCUSMODE,
		(CAM_Param)&FocusMode,
		UNUSED_PARAM);
	ASSERT_CAM_ERROR(error);

	v = String8("");
	v = setting.map_ce2andr(CameraSetting::map_focus,FocusMode);
	if( v != String8("") ){
	    setting.set(CameraParameters::KEY_FOCUS_MODE,v.string());
	    TRACE_D("%s:CE used focus mode:%s",__FUNCTION__,v.string());
	}

	// EVC
	const int evcstep=mCamera_shotcaps.iEVCompStepQ16;
	if ( 0 != evcstep){
	    int ev = 0;
	    error = CAM_SendCommand(handle,
		    CAM_CMD_GET_EVCOMPENSATION,
		    (CAM_Param)&ev,
		    UNUSED_PARAM);
	    ASSERT_CAM_ERROR(error);

	    v = String8("");
	    char tmp[32] = {'\0'};
	    int exposure = ev / evcstep;
	    sprintf(tmp, "%d", exposure);
	    v = String8(tmp);
	    setting.set(CameraParameters::KEY_EXPOSURE_COMPENSATION, v.string());
	    TRACE_D("%s:CE used exposure compensation:%s",__FUNCTION__,v.string());
	}

	//digital zoom
	const int minzoom = mCamera_shotcaps.iMinDigZoomQ16;
	const int maxzoom = mCamera_shotcaps.iMaxDigZoomQ16;
	TRACE_V("%s:%d: minzoom=%d,maxzoom=%d",__FUNCTION__,__LINE__,minzoom,maxzoom);
	const int cezoomstep = mCamera_shotcaps.iDigZoomStepQ16;
	if((0x1<<16) != maxzoom){
	    int cedigzoom;
	    char tmp[32] = {'\0'};
	    error = CAM_SendCommand(handle,
		    CAM_CMD_GET_DIGZOOM,
		    (CAM_Param)&cedigzoom,
		    UNUSED_PARAM );
	    ASSERT_CAM_ERROR(error);

	    int andrzoomval = (cedigzoom-minzoom)/(cezoomstep);//1.0x for andr min zoom
	    sprintf(tmp, "%d", andrzoomval);
	    mSetting.set(CameraParameters::KEY_ZOOM, tmp);
	    TRACE_D("%s:CE used digital zoom:%s",__FUNCTION__,tmp);
	}

	int brightness;
	error = CAM_SendCommand(handle,
		CAM_CMD_GET_BRIGHTNESS,
		(CAM_Param)&brightness,
		UNUSED_PARAM );
	if (error == CAM_ERROR_NONE)
	{
		char brightness_str[32];
		sprintf(brightness_str, "%d", brightness);
		setting.set("brightness", v.string());
	}

	int contrast;
	error = CAM_SendCommand(handle,
		CAM_CMD_GET_CONTRAST,
		(CAM_Param)&contrast,
		UNUSED_PARAM );
	if (error == CAM_ERROR_NONE)
	{
		char contrast_str[32];
		sprintf(contrast_str, "%d", contrast);
		setting.set("contrast", v.string());
	}

	return error;
    }

    status_t Engine::ImageBuftoCAM_ImageBuffer(const sp<ImageBuf>& imagebuf,
	    int i, CAM_ImageBuffer* cam_img)
    {
	cam_img->eFormat =
	    (CAM_ImageFormat)mSetting.map_andr2ce(
		    CameraSetting::map_imgfmt,imagebuf->getImageFormat());
	cam_img->iOffset[0] = 0;
	cam_img->iOffset[1] = 0;
	cam_img->iOffset[2] = 0;

	int step[3];
	imagebuf->getStep(i, step+0,step+1,step+2);
	cam_img->iStep[0] = step[0];
	cam_img->iStep[1] = step[1];
	cam_img->iStep[2] = step[2];

	int len[3];
	imagebuf->getAllocLen(i, len+0,len+1,len+2);
	cam_img->iAllocLen[0] = len[0];
	cam_img->iAllocLen[1] = len[1];
	cam_img->iAllocLen[2] = len[2];
	cam_img->iFilledLen[0] = 0;
	cam_img->iFilledLen[1] = 0;
	cam_img->iFilledLen[2] = 0;

	int w,h;
	imagebuf->getImageSize(&w,&h);
	cam_img->iWidth = w;
	cam_img->iHeight = h;
	cam_img->iUserIndex = i;
	cam_img->pUserData = NULL;
	cam_img->iPrivateIndex = 0;
	cam_img->pPrivateData = NULL;
	cam_img->iTick = 0;
	cam_img->bEnableShotInfo = CAM_TRUE;

	unsigned long* vaddr[3];
	imagebuf->getVirAddr(i,vaddr+0,vaddr+1,vaddr+2);
	cam_img->pBuffer[0] = (CAM_Int8u*)vaddr[0];
	cam_img->pBuffer[1] = (CAM_Int8u*)vaddr[1];
	cam_img->pBuffer[2] = (CAM_Int8u*)vaddr[2];

	unsigned long paddr[3];
	imagebuf->getPhyAddr(i,paddr+0,paddr+1,paddr+2);
	cam_img->iPhyAddr[0] = (unsigned long)paddr[0];
	cam_img->iPhyAddr[1] = (unsigned long)paddr[1];
	cam_img->iPhyAddr[2] = (unsigned long)paddr[2];

	return NO_ERROR;
    }

}; // namespace android
