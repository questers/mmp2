/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

#ifndef ANDROID_HARDWARE_ENGINE_H
#define ANDROID_HARDWARE_ENGINE_H

namespace android {

    class Engine:public CameraCtrl{
	public:
	    //Engine(const CameraSetting& setting);
	    Engine(int cameraID=0);
	    virtual ~Engine();
	    int	    mTuning;

	    status_t startPreview();
	    status_t stopPreview();

	    status_t setParameters(CameraParameters param);
	    CameraParameters getParameters();

	    //preview
	    status_t registerPreviewBuffer(sp<ImageBuf> imagebuf);
	    //status_t enqPreviewBuffer(sp<ImageBuf> imagebuf, int index);//XXX:check buf req/cnt here
	    status_t enqPreviewBuffer(int index);
	    status_t deqPreviewBuffer(int* index);
	    status_t unregisterPreviewBuffers();

	    //still
	    status_t registerStillBuffer(sp<ImageBuf> imagebuf);
	    //status_t enqStillBuffer(sp<ImageBuf> imagebuf, int index);
	    status_t enqStillBuffer(int index);
	    status_t deqStillBuffer(int* index);
	    status_t unregisterStillBuffers();

	    void* getUserData(void* ptr = NULL);

	    sp<IMemory> getExifImage(int index);
#if 1
	    CAM_ImageBufferReq getPreviewBufReq();
	    CAM_ImageBufferReq getStillBufReq();
#endif

	    status_t autoFocus();
	    status_t cancelAutoFocus();
	    status_t startFlash();
	    status_t stopFlash();
		
	    bool isFlashOn();
	    status_t getBufCnt(int* previewbufcnt,int* stillbufcnt,int* videobufcnt) const;

	    static int getNumberOfCameras();
	    static void getCameraInfo(int cameraId, struct CameraInfo* cameraInfo);

	    virtual void setCallbacks(notify_callback notify_cb=NULL,
		    void* user=NULL);
	    virtual void enableMsgType(int32_t msgType);
	    virtual void disableMsgType(int32_t msgType);

	    static void NotifyEvents(CAM_EventId eEventId,int param,void *pUserData);
	private:
	    CameraSetting mSetting;

	    notify_callback     mNotifyCb;
	    void                *mCallbackCookie;
	    int32_t             mMsgEnabled;

	    CAM_DeviceHandle	    mCEHandle;

	    CAM_CameraCapability    mCamera_caps;
	    CAM_ShotModeCapability  mCamera_shotcaps;

	    static const int kMaxBufCnt = 20;//max preview/still/video buffer count

	    //engine defined structure
	    CAM_ImageBufferReq	    mPreviewBufReq;
	    CAM_ImageBufferReq	    mStillBufReq;
	    CAM_ImageBufferReq      mVideoBufReq;

	    CAM_ImageBuffer mPreviewImgBuf[kMaxBufCnt];
	    CAM_ImageBuffer mStillImgBuf[kMaxBufCnt];

	    status_t ImageBuftoCAM_ImageBuffer(const sp<ImageBuf>& imagebuf, int i,
		    CAM_ImageBuffer* cam_img);

#if 1
	    int iExpectedPicNum;
#endif
	    CAM_Error ceInit(CAM_DeviceHandle *pHandle,CAM_CameraCapability *pcamera_caps);
	    status_t ceStartPreview(const CAM_DeviceHandle &handle);
	    status_t ceUpdateSceneModeSetting(const CAM_DeviceHandle &handle,
		    CameraSetting& setting);
	    status_t ceStopPreview(const CAM_DeviceHandle &handle);
	    status_t ceStartStill(const CAM_DeviceHandle &handle);
	    CAM_Error ceGetPreviewFrame(const CAM_DeviceHandle &handle,int *index);
	    CAM_Error ceGetStillFrame(const CAM_DeviceHandle &handle,int *index);
	    CAM_Error ceGetCurrentSceneModeCap(const CAM_DeviceHandle &handle,
		    CameraSetting& setting);
    };

}; // namespace android
#endif
