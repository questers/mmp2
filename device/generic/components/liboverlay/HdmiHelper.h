/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

#ifndef __HDMI_HELPER_H
#define __HDMI_HELPER_H
#include <utils/Errors.h>  // for status_t
#include <utils/threads.h>

#include "IHdmiService.h"
namespace android{
class HdmiHelper{
public:
    HdmiHelper(sp<IHdmiService> service);
    HdmiHelper(sp<IHdmiService> service, int res);
    ~HdmiHelper();
    void CheckAndUpdate( bool full_screen, bool need_720p);
    // if conn == 1, res is sink supported res
    void SinkStateChanged( int conn, int res );
private:
    void getDefault(int res);
    sp<IHdmiService> mService;
    bool mSupportFullHD;
    bool mIs720P;
    int m1080pRes;
    int m720pRes;
    int mOrigRes;
    int mCurrRes;
    Mutex mLock; // lock for guc agent thread and hdmi callback thread
    
};
};
#endif
