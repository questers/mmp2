# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


LOCAL_PATH:= $(call my-dir)
# HAL module implemenation, not prelinked and stored in
# hw/<COPYPIX_HARDWARE_MODULE_ID>.<ro.board.platform>.so
include $(CLEAR_VARS)

ifdef OVERLAY_DEVICE_PATH
LOCAL_CFLAGS += -DOVERLAY_DEVICE_PATH=\"$(OVERLAY_DEVICE_PATH)\"
endif

ifeq ($(strip $(ROTATE_ENGINE)),)
ROTATE_ENGINE := ire
endif

LOCAL_SRC_FILES := \
	overlay.marvell.cpp \
    PhysMemPool.cpp \
    GCUAgent.cpp \
    dumpers.cpp 

LOCAL_C_INCLUDES := \
        device/generic/components/libipp/include \
        device/generic/components/libbmm/lib \
	device/generic/components/libhgl \
	device/generic/components/libhgl/include 
	
ifeq ($(BOARD_HAVE_MARVELL_GCU), true)
LOCAL_C_INCLUDES += \
    $(MARVELL_GCU_INCLUDE_PATH)
endif

ifeq ($(strip $(BOARD_HAVE_HDMI_SERVICE)),true)
LOCAL_C_INCLUDES += \
        device/generic/components/HdmiService/Service
LOCAL_CFLAGS += -DHAVE_HDMI_SERVICE     

LOCAL_SRC_FILES += \
	HdmiServiceClient.cpp \
    HdmiHelper.cpp
endif

LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw

LOCAL_SHARED_LIBRARIES := \
        liblog \
        libcutils \
        libutils \
        libbinder \
        libgcu \
        libhdmiservice
LOCAL_MODULE_TAGS := optional

LOCAL_STATIC_LIBRARIES += libippcam

LOCAL_MODULE := overlay.marvell

LOCAL_CFLAGS += -DPLATFORM_SDK_VERSION=$(PLATFORM_SDK_VERSION)

include $(BUILD_SHARED_LIBRARY)
