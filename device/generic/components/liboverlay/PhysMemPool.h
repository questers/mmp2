/*******************************************************************************
//(C) Copyright [2009] Marvell International Ltd.
//All Rights Reserved
 *******************************************************************************/
#ifndef __PHYSMEM_POOL_H__
#define __PHYSMEM_POOL_H__

#include <sys/ioctl.h>
#include <linux/android_pmem.h>

#include <utils/RefBase.h>
#include <binder/MemoryBase.h>
#include <binder/MemoryHeapBase.h>
#include <binder/MemoryHeapPmem.h>
#include "common.h"

#define MAX_PMEM_COUNT	(MAX_FRAME_BUFFERS*2)

namespace android {

/**
 * PMEM memory pool
 */
class PhysMem;
class PhysMemPool : public RefBase {
public:
    PhysMemPool();
    ~PhysMemPool();
    int init();
    int deinit();
    PhysMem * alloc( unsigned int size );
    void release( PhysMem * mem );

private:
    Mutex mLock;
    int realAllocCount;
	const char *pmem_dev;
	sp<MemoryHeapPmem> arrPmemHeap[MAX_PMEM_COUNT];
	bool arrPmemUseFlag[MAX_PMEM_COUNT];
	PhysMem * arrPhysMem[MAX_PMEM_COUNT];

};

/**
 * Each allocated memory block from PhysMemPool be wrapped by this class.
 */
class PhysMem {
public:
    PhysMem( PhysMemPool *pool );
    ~PhysMem();
    void release(); // release and return back to pool

    void *paddr;
    void *vaddr;
    unsigned int size;

private:
    PhysMemPool * pool;
};

}//namespace android

#endif //__PHYSMEM_POOL_H__

