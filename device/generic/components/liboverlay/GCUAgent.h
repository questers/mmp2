/*******************************************************************************
//(C) Copyright [2009] Marvell International Ltd.
//All Rights Reserved
 *******************************************************************************/

#ifndef __GCU_AGENT_H__
#define __GCU_AGENT_H__

#define DO_ORIENTATION_FLIP  1

//#include <utils/vector.h>
#include <utils/RefBase.h>
#include <utils/Vector.h>
#include <utils/threads.h>
#include <binder/IServiceManager.h>
#include <binder/IPCThreadState.h>
#include <hardware/hardware.h>
#include <hardware/overlay.h>

#include "gcu.h"
#include "common.h"
#include "PhysMemPool.h"
#include "overlay.marvell.h"
#ifdef HAVE_HDMI_SERVICE       
#include "IHdmiService.h"
#include "HdmiHelper.h"
#endif
namespace android {

struct MemoryFreeData {
    // memory of gcu
    PhysMem *physMem;

    // codec call back;
    void *user;
    void (*free)(void *user);
};

#ifdef HAVE_HDMI_SERVICE       
class HdmiServiceClient;
#endif
// actions may be imposed on video frame
typedef enum {
    GCU_DO_NONE        = 0,
    GCU_DO_ROTATE      = 1,
    GCU_DO_DEINTERLACE = 2,
    GCU_DO_COLORCONVERT= 4
} GCUAction;

typedef enum {
    GCU_FLAG_NONE = 0,
    GCU_FLAG_FULLSCREEN = 1,
    GCU_FLAG_ROTATE_IN_TV = 2
} GCUFlag;

class GCURequest {
public:
    VideoBuffer vb;
    struct overlay_buffer_header_t *bh;
    //action's arguments
    int rot_degree;
    int new_fmt;

    //ctor
    GCURequest() {
        bh = NULL;
        actions = GCU_DO_NONE;

        rot_degree = 0;
        new_fmt = -1;
        mFlags = 0;
    }
    //dtor
    ~GCURequest() {}
    //assignment operator
    GCURequest & operator= (const GCURequest & other) {
        if (this != &other) {
            vb = other.vb;
            bh = other.bh;
            rot_degree = other.rot_degree;
            new_fmt = other.new_fmt;
            actions = other.actions;
            mFlags = other.mFlags;
        }
        return *this;
    }

    // add Action
    void addAct( GCUAction act ) {
        actions |= act;
    }
    // test if including act
    bool testAct( GCUAction act ) {
        return (actions & act) != 0;
    }
    // remove the specified act
    void removeAct( GCUAction act ) {
        actions &= (~act);
    }

    void addFlag( GCUFlag flag) {
        mFlags |= flag;
    }

    bool testFlag( GCUFlag flag ) {
        return (mFlags & flag) != 0;
    }
    void removeFlag( GCUFlag flag) {
        mFlags &= (~flag);
    }
    void dump();
private:
    unsigned int actions;
    int mFlags;
};

class PhysMem;
class GCUResult {
public:
    VideoBuffer vb;
    PhysMem *physMem;

    GCUResult() {
        physMem = NULL;
    }
    ~GCUResult(){}
};

/**
 * GCU agent class
 */
class PhysMemPool;
class GCUAgent : public RefBase {
public:
    GCUAgent( int fdDev, PhysMemPool * memPool );
    ~GCUAgent();
    int init();
	bool isInitialized();
    int deinit();
    bool addRequest( GCURequest *gcuReq );
	
	#ifdef HAVE_HDMI_SERVICE       
    void reacquireHdmiService();
    void hdmiConnChanged( int state );
	#endif

private:
    Mutex & _mLock() {
        return mLock;
    }
    // class : worker thread
    class GCUWorkerThread : public Thread {
        public:
            GCUWorkerThread( GCUAgent *gcuAgent ) {
                this->gcuAgent = gcuAgent;
            }
            virtual ~GCUWorkerThread(){
            }
        private:
            virtual bool threadLoop();
            GCUAgent *gcuAgent;
    };

private:
    int fd; // overlay-hw-dev file descriptor
	PhysMemPool *memPool;
    Mutex mLock; // lock for "this" instance
    Condition mCondIncoming;
    Vector<GCURequest*> requests;
    sp<Thread> workerThread;
    bool exitFlag;

    /* gcu related stuff */
    bool initialized;
    GCU_INIT_DATA gcuInitData;
    GCU_CONTEXT_DATA gcuCtxData;
    GCUContext gcuCtx;

	#ifdef HAVE_HDMI_SERVICE       
    
    /******************
      * hdmi status          *
      * 0 : disconnected   *
      * 1 : connected       *
      ******************/
    int mHdmiConnState;

    sp<IHdmiService> mService;
    sp<HdmiServiceClient> mClient;
    Mutex mHdmiEventLock;

    HdmiHelper *mHelper;
	#endif

    bool _color420SP_to_420P( GCURequest *gcuReq );
#if DO_ORIENTATION_FLIP
    bool _doOrientationFlip( GCURequest *gcuReq, int ovlyRotFlag );
#endif
    bool process( GCURequest *gcuReq, GCUResult *gcuRes );
    bool _createSrcSurface( GCURequest *gcuReq, GCUSurface *pSrcSurface, GCU_RECT *srcRect );
    bool _createDstSurface( GCURequest *gcuReq, GCUSurface *pDstSurface, GCU_RECT *dstRect,
                            GCUResult *gcuResult);

	GCU_FORMAT _getGCUFmt( int ovlyFmt );
    int _getOverlayFmt( GCU_FORMAT gcuFmt );
	GCU_ROTATION _getGCURotationDegree( int numeric_degree );
};

}//namespace android

#endif //__GCU_AGENT_H__

