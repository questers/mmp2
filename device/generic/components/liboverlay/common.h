/*******************************************************************************
//(C) Copyright [2009] Marvell International Ltd.
//All Rights Reserved
*******************************************************************************/

#ifndef _OVERLAYHAL_COMMON_H
#define _OVERLAYHAL_COMMON_H

// hdmi : video output to hdmi
// normal : video output to normal
#define PROP_VIDEO_PATH "service.video.path"

// overlay transform property, currently use property to do IPC
#define PROP_OVERLAY_ROTATE "service.overlay.rotate"

#define BASELAY_DEVICE "/dev/graphics/fb1"

#ifndef OVERLAY_DEVICE_PATH
#define OVERLAY_DEVICE_PATH "/dev/graphics/fb1"
#endif

#define ALIGN16(m)  (((m + 15)/16) * 16)
#define ALIGN4(m)  (((m + 3)/4) * 4)

/* pmem dev node */
#define DEV_PMEM_ADSP "/dev/pmem_adsp"

#define MAX_FRAME_BUFFERS    7

class VideoBuffer {
public:
    void         *paddr; // physical address
    void         *vaddr; // virtual address
    int          ovlyFmt;
    int          len;
    int          x_off;
    int          y_off;
    int          width; // valid data width
    int          height;// valid data width
    int          stride_x;
    int          stride_y;
    int          y_step;
    int          u_step;
    int          v_step;

    VideoBuffer() {
        paddr = vaddr = 0;
        ovlyFmt = -1;
        len = 0;
        x_off = y_off = 0;
        width = height = 0;
        stride_x = stride_y = 0;
        y_step = u_step = v_step = 0;
    }

    VideoBuffer & operator= (const VideoBuffer & other) {
        if (this != &other) {
            paddr = other.paddr;
            vaddr = other.vaddr;
            ovlyFmt = other.ovlyFmt;
            len = other.len;
            x_off = other.x_off;
            y_off = other.y_off;
            width = other.width;
            height = other.height;
            stride_x = other.stride_x;
            stride_y = other.stride_y;
            y_step = other.y_step;
            u_step = other.u_step;
            v_step = other.v_step;
        }
        return *this;
    }
};

extern bool doFlip( int fd, VideoBuffer *vb, VideoBuffer *hb );
extern void _physMemFree( void *user );
extern void _physMemFree_TV( void *user );

#endif  // _OVERLAYHAL_COMMON_H
