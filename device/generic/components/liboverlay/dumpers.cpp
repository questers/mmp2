/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//#define LOG_NDEBUG 0
#define LOG_TAG "OverlayHal.Dumper"

#include <cutils/log.h>
#include <hardware/overlay.h>
#include <cutils/properties.h>
#include <stdlib.h>
#include "dumpers.h"

using namespace android;

Dumper *UYVYDumper::singleton = NULL;
Dumper *RGB565Dumper::singleton = NULL;
Dumper *YUV420pDumper::singleton = NULL;


void UYVYDumper::dumpBuf( void *buf, unsigned int size, unsigned int w, unsigned int h ) {
    char fn[128];
    sprintf( fn, "/data/overlay_dump%d.uyvy", ++index );
    FILE *fp = fopen( fn, "ab" );
    unsigned int len = w * h * 2;
    if( fp ) {
        LOGD("%s#%d: fwrite len=%d, buf=%p", __FUNCTION__, __LINE__, len, buf);
        LOGD("%s#%d:        buf[0]=%x, buf[1]=%x, buf[2]=%x",
                __FUNCTION__, __LINE__, ((char*)buf)[0],((char*)buf)[1],((char*)buf)[2]);
        fwrite( (char*)buf, sizeof(char), len, fp );
        fclose( fp );
    }
}

void RGB565Dumper::dumpBuf( void *buf, unsigned int size, unsigned int w, unsigned int h ) {
    char fn[128];
    sprintf( fn, "/data/overlay_dump%d.rgb565", ++index );
    FILE *fp = fopen( fn, "ab" );
    unsigned int len = w * h * 2;
    if( fp ) {
        LOGD("%s#%d: fwrite len=%d, buf=%p", __FUNCTION__, __LINE__, len, buf);
        LOGD("%s#%d:        buf[0]=%x, buf[1]=%x, buf[2]=%x",
                __FUNCTION__, __LINE__, ((char*)buf)[0],((char*)buf)[1],((char*)buf)[2]);
        fwrite( (char*)buf, sizeof(char), len, fp );
        fclose( fp );
    }
}

void YUV420pDumper::dumpBuf( void *buf, unsigned int size, unsigned int w, unsigned int h ) {
    char fn[128];
    sprintf( fn, "/data/overlay_dump%d.yuv420", ++index );
    FILE *fp = fopen( fn, "ab" );
    unsigned int len = w * h * 3/2;
    if( fp ) {
        LOGD("%s#%d: fwrite len=%d, buf=%p", __FUNCTION__, __LINE__, len, buf);
        LOGD("%s#%d:        buf[0]=%x, buf[1]=%x, buf[2]=%x",
                __FUNCTION__, __LINE__, ((char*)buf)[0],((char*)buf)[1],((char*)buf)[2]);
        fwrite( (char*)buf, sizeof(char), len, fp );
        fclose( fp );
    }
}

/**
 * dump entry function
 */
void dumpBuf( int ovlyFmt, void *buf, unsigned int size, unsigned int w, unsigned int h ) {
    char prop_value[ PROPERTY_VALUE_MAX ];
    property_get( "overlay.dump.frame", prop_value, "0" );
    if( strcmp( prop_value, "0" ) == 0 ) {
        return;//no need to dump
    }

    switch( ovlyFmt ) {
        case OVERLAY_FORMAT_YCbYCr_422_I:
            UYVYDumper::getInstance()->dumpBuf( buf, size, w, h );
            break;
        case OVERLAY_FORMAT_RGB_565:
            RGB565Dumper::getInstance()->dumpBuf( buf, size, w, h );
            break;
        case OVERLAY_FORMAT_YCbCr_420_P:
            YUV420pDumper::getInstance()->dumpBuf( buf, size, w, h );
            break;
        default:
            LOGE("%s#%d: cann't dump unknown ovlyFmt=%d", __FUNCTION__, __LINE__, ovlyFmt);
            break;
    }
}

