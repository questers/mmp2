/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//#define LOG_NDEBUG 0
#define LOG_TAG "OverlayHal.PhysMemPool"

#include <cutils/log.h>
#include "PhysMemPool.h"

using namespace android;

PhysMem::PhysMem( PhysMemPool *pool ) {
    this->pool = pool;
    paddr = NULL;
    vaddr = NULL;
    size = 0;
}

PhysMem::~PhysMem( ) {
    pool = NULL;
}

void PhysMem::release() {
    if( pool != NULL )
        pool->release( this );
}

PhysMemPool::PhysMemPool() {
	pmem_dev = (const char*)"/dev/pmem_adsp";
    realAllocCount = 0;
}

PhysMemPool::~PhysMemPool() {
}

int PhysMemPool::init( ) {
    Mutex::Autolock _l( mLock );
	for( int i=0; i<MAX_PMEM_COUNT; i++ ) {
		arrPmemHeap[i].clear();
		arrPmemUseFlag[i] = false;
        arrPhysMem[i] = NULL;
	}
	return 0;
}

int PhysMemPool::deinit() {
    Mutex::Autolock _l( mLock );
	for( int i=0; i<MAX_PMEM_COUNT; i++ ) {
        if( arrPhysMem[i] ) {
            delete arrPhysMem[i];
            arrPhysMem[i] = NULL;
        }
        if( arrPmemHeap[i].get() != NULL ) {
            arrPmemHeap[i].clear();
        }
		arrPmemUseFlag[i] = false;
	}
    LOGD("%s#%d. total alloc pmem count=%d", __FUNCTION__, __LINE__, realAllocCount );
	return 0;
}

PhysMem * PhysMemPool::alloc( unsigned int len ) {
    Mutex::Autolock _l( mLock );

    int blk_size = ALIGN16( 1920 ) * ALIGN4( 1080 ) * 2;
	PhysMem *ret = NULL;
	//search PhysMem that is previous allocated but not in use now
	//and also marks the first empty element's index.
	int idx = 0;
	int idxFirstEmpty = -1;
	for( idx=0; idx<MAX_PMEM_COUNT; idx++ ) {
        if( arrPmemHeap[idx].get()!=NULL ) {
            if( arrPmemUseFlag[idx]==false ) {
                ret = arrPhysMem[idx];
                arrPmemUseFlag[idx] = true;
                break;
            }
            else {
                continue;
            }
        }
        else {
            if( idxFirstEmpty == -1 )
				idxFirstEmpty = idx;
        }
	}
	if( ret != NULL )
		return ret; // return previous allocated element which is not in-use now
	//allocating
	if( idxFirstEmpty == -1 ) {
		LOGE("%s() exception on check idxFirstEmpty==-1", __FUNCTION__ );
		return NULL;
	}

	sp<MemoryHeapBase> heapBase = new MemoryHeapBase(pmem_dev, blk_size);
	if (heapBase->heapID() < 0) {
		LOGE("%s() failed to create MemoryHeapBase(%s,%d)", __FUNCTION__,
			pmem_dev, blk_size);
		return NULL;
	}
	sp<MemoryHeapPmem> heapPmem = new MemoryHeapPmem(heapBase, 0);
	heapPmem->slap();
	heapBase.clear();
	struct pmem_region region;
	if( ioctl(heapPmem->getHeapID(), PMEM_GET_PHYS, &region)  < 0 ) {
		LOGE("%s() failed on PMEM_GET_PHYS", __FUNCTION__ );
		return NULL;
	}
	unsigned long pmem_phybase = region.offset;
	if( pmem_phybase & 0x3F ) {
		LOGW( "%s() pmem_phybase:%p is not 64-bytes aligned!", __FUNCTION__,(void*)pmem_phybase );
	}
	unsigned char* pmem_virbase = static_cast<unsigned char *>(heapPmem->base());
	PhysMem* newPhysMem = new PhysMem( this );
	newPhysMem->paddr = (void*)pmem_phybase;
	newPhysMem->vaddr = (void*)pmem_virbase;
	newPhysMem->size = blk_size;

	arrPmemHeap[idxFirstEmpty] = heapPmem;
	arrPmemUseFlag[idxFirstEmpty] = true;
	arrPhysMem[idxFirstEmpty] = newPhysMem;

    realAllocCount++;

    return arrPhysMem[idxFirstEmpty];
}

void PhysMemPool::release( PhysMem * mem ) {
    Mutex::Autolock _l( mLock );

	if( mem == NULL )
		return;
	for( int i=0; i<MAX_PMEM_COUNT; i++ ) {
		PhysMem * cur = arrPhysMem[i];
		if( cur == mem ) {
			arrPmemUseFlag[i] = false;
			break;
		}
	}
}


