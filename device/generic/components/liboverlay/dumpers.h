/*******************************************************************************
//(C) Copyright [2009] Marvell International Ltd.
//All Rights Reserved
 *******************************************************************************/

#ifndef __DUMPERS_H__
#define __DUMPERS_H__

#include <utils/RefBase.h>
#include <utils/Vector.h>
#include <utils/threads.h>
#include <hardware/hardware.h>
#include <hardware/overlay.h>

#include "gcu.h"
#include "common.h"
#include "PhysMemPool.h"
#include "overlay.marvell.h"
#ifdef HAVE_HDMI_SERVICE       
#include <binder/IServiceManager.h>
#include <binder/IPCThreadState.h>
#include "IHdmiService.h"
#endif
extern void dumpBuf( int ovlyFmt,
        void *buf, unsigned int size,
        unsigned int w, unsigned int h );

namespace android {
    /**
     * base abstract dump class
     */
    class Dumper {
        protected:
            int index;
            Dumper(){
                index = 0;
            }
            virtual ~Dumper(){
            }
        public:
            virtual void dumpBuf( void *buf, unsigned int size, unsigned int w, unsigned int h ) = 0;
    };


    /**
     * UYVY dumper class
     */
    class UYVYDumper : public Dumper {
        private:
            static Dumper *singleton;
        public:
            static Dumper * getInstance() {
                if(singleton == NULL){
                    singleton = new UYVYDumper();
                }
                return singleton;
            }
            virtual void dumpBuf( void *buf, unsigned int size, unsigned int w, unsigned int h );
    };

    /**
     * RGB565 dumper class
     */
    class RGB565Dumper : public Dumper {
        private:
            static Dumper *singleton;
        public:
            static Dumper * getInstance() {
                if(singleton == NULL){
                    singleton = new RGB565Dumper();
                }
                return singleton;
            }
            virtual void dumpBuf( void *buf, unsigned int size, unsigned int w, unsigned int h );
    };

    /**
     * YUV420p dumper class
     */
    class YUV420pDumper : public Dumper {
        private:
            static Dumper * singleton;
        public:
            static Dumper * getInstance() {
                if(singleton == NULL){
                    singleton = new YUV420pDumper();
                }
                return singleton;
            }
            virtual void dumpBuf( void *buf, unsigned int size, unsigned int w, unsigned int h );
    };

}//namespace android

#endif //__DUMPERS_H__

