/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//#define LOG_NDEBUG 0
#define LOG_TAG "OverlayHal.GCUAgent"

#include "common.h"
#include <hardware/overlay.h>
#include <cutils/log.h>
#include "GCUAgent.h"
#include "ippIP.h"
#ifdef HAVE_HDMI_SERVICE       
#include "HdmiServiceClient.h"
#endif
#define DUMP_SRC    0
#define DUMP_DST    0

using namespace android;

GCUAgent::GCUAgent( int fdDev, PhysMemPool * memPool ) {
    this->fd = fdDev;
	this->memPool = memPool;
    this->workerThread = NULL;

	initialized = false;
    memset(&gcuInitData, 0 ,sizeof(gcuInitData));
    memset(&gcuCtxData, 0 ,sizeof(gcuCtxData));
    gcuCtx = NULL;
    exitFlag = false;
	
	#ifdef HAVE_HDMI_SERVICE       
    mHelper = 0;
	#endif
}

GCUAgent::~GCUAgent() {
}

int GCUAgent::init() {
    Mutex::Autolock _l( mLock );

    if( initialized )
        return -1;

    gcuInitialize( &gcuInitData );

    gcuCtx = gcuCreateContext( &gcuCtxData );
    if( gcuCtx == NULL ) {
        LOGE("%s() failed gcuCreateContext()", __FUNCTION__ );
        return -1;
    }
	
	#ifdef HAVE_HDMI_SERVICE       
    /* init hdmi staff */
    if( mService.get() == 0 ) {
        sp<IServiceManager> sm = defaultServiceManager();
        sp<IBinder> binder = sm->getService(String16("HdmiService"));
        if( binder == 0 ) {
            LOGE("Hdmi service not published, exit...");
            return -1;
        }
        mService = interface_cast<IHdmiService>(binder);
        if(mService.get() == 0 ) {
            LOGE("get hdmi service failed----------------");
            return -1;
        }
    }
    if( mClient == 0 ) {
        mClient = new HdmiServiceClient( this );
    }
    mHdmiConnState = mService->get_hdmi_sink_state();
    if( mHdmiConnState ) {
        // get if hdmi sink support FULL HD
        int supportres = mService->get_supported_display_resolution();
        mHelper = new HdmiHelper(mService, supportres);
    } else {
        mHelper = new HdmiHelper(mService );
    }
    if(mService->registerCallback(interface_cast<IHdmiServiceCallback>(mClient)) < 0 ) {
        LOGE("register HdmiService callback failed");
        return -1;
    }
    #endif
    exitFlag = false;
    requests.clear();
    workerThread = new GCUWorkerThread( this );
    workerThread->run("GCUWorkerThread", PRIORITY_DISPLAY, 0);

    initialized = true;

    return 0;
}

bool GCUAgent::isInitialized() {
    Mutex::Autolock _l( mLock );
	return initialized;
}


int GCUAgent::deinit() {
    LOGD("GCUAgent::deinit() entered.");
    if( initialized == false )
        return -1;
    // stop worker thread: send an empty GCURequest to indicate EXIT
    {
        Mutex::Autolock _l( mLock );
        GCURequest *emptyReq = new GCURequest;
        requests.insertAt( emptyReq, 0 );//insert to first element to exit ASAP
        mCondIncoming.signal();
    }
	workerThread->requestExitAndWait();
	workerThread.clear();
    // free all pending requests
    {
        Mutex::Autolock _l( mLock );
        int nbRequests = requests.size();
        for(int i=nbRequests-1; i>=0; i-- ) {
            GCURequest *req = requests[i];
            if( req->bh == NULL ) {
                delete req;
                continue;
            }
            if( req->bh->free != NULL ) {
                req->bh->free( req->bh->user );
                req->bh->free = NULL;
                req->bh->user = NULL;
            }
            requests.removeAt( i );
            delete req;
        }
    }

    if( gcuCtx ) {
        gcuDestroyContext( gcuCtx );
        gcuCtx = NULL;
    }
    gcuTerminate();
	
	#ifdef HAVE_HDMI_SERVICE       
		
    if( mService.get() && mClient != 0 ) {
        mService->unregisterCallback(mClient);
    }
    if( mHelper != 0 ) {
        delete mHelper;
    }
	#endif
    initialized = false;

    LOGD("GCUAgent::deinit() completed.");

    return 0;
}

/**
 * add new request
 */
bool GCUAgent::addRequest( GCURequest *gcuReq ) {
    Mutex::Autolock _l( mLock );
    if( initialized == false ) {
        return false;
    }

    GCURequest *newReq = new GCURequest();
    *newReq = *gcuReq;
    requests.add( newReq );
    mCondIncoming.signal();
    return true;
}

#ifdef HAVE_HDMI_SERVICE       

void GCUAgent::reacquireHdmiService() {
    sp<IServiceManager> sm = defaultServiceManager();
    for( int i = 10; i>= 0; i-- ) {
        sp<IBinder> binder = sm->getService(String16("HdmiService"));
        if( binder == 0 ) {
            LOGE("HdmiService doesn't show up, retry remaining %d, wait 200ms...", i);
            usleep(2000000);
            continue;
        }
        mService = interface_cast<IHdmiService>(binder);
        if(mService.get() == 0 ) {
            LOGE("HdmiService doesn't show up, retry remaining %d, wait 200ms...", i);
            usleep(2000000);
            continue;
        }

        mService->registerCallback(interface_cast<IHdmiServiceCallback>(mClient));
        break;
    }
    if( mService.get() == 0 ) {
        LOGE("%s can not get HdmiService", __FUNCTION__);
        /* failed to get HdmiService, set hdmi state as disconnected */
        mHdmiConnState = 0;
    } else {
        mHdmiConnState = mService->get_hdmi_sink_state();
    }
    if( mHdmiConnState && mHelper != 0) {
        int res = mService->get_supported_display_resolution();
        mHelper->SinkStateChanged(1, res);
    }
}

void GCUAgent::hdmiConnChanged(int state){
    Mutex::Autolock _l(mHdmiEventLock);
    mHdmiConnState = state;
    int res = 0;
    if( mHdmiConnState ) {
        res = mService->get_supported_display_resolution();
    }
    mHelper->SinkStateChanged(state, res);
}
#endif
bool GCUAgent::GCUWorkerThread::threadLoop() {
    LOGD("GCUWorkerThread::threadLoop() entered.");
    do{
        GCURequest *req = NULL;
        //1. wait for incoming frames
        {
            Mutex::Autolock _l( gcuAgent->_mLock() );
            while( gcuAgent->requests.size()<=0 ) {
                gcuAgent->mCondIncoming.wait( gcuAgent->_mLock() );
            }
            req = gcuAgent->requests[0];
            gcuAgent->requests.removeAt(0);
            if( req->bh == NULL ) {
                //found empty request, we should exit
                delete req;
                return false;
            }
        }
		#ifdef HAVE_HDMI_SERVICE
        // check and update hdmi configuration
        bool need_720p = (req->vb.width <= 1280) && (req->vb.height <= 720);
        gcuAgent->mHelper->CheckAndUpdate(req->testFlag(GCU_FLAG_FULLSCREEN), need_720p);
		#endif
        //2. process this request
        GCUResult gcuRes;
        if( gcuAgent->process( req, &gcuRes ) == true ) {
            // no phy memory avail, just return
            if( gcuRes.physMem == NULL ) {
                LOGE("gcuAgent->process with no phy mem error");
                if( req->bh->free != NULL ) {
                    req->bh->free( req->bh->user );
                    req->bh->free = NULL;
                    req->bh->user = NULL;
                }
                delete req;
                return false;
            }
			#ifdef HAVE_HDMI_SERVICE
            //something done, the to-show data is in gcuRes
            if( gcuAgent->mHdmiConnState == 0 ) {
                //here can free src buffer to reduce wait time of Stagefirght
                if( req->bh->free != NULL ) {
                    req->bh->free( req->bh->user );
                    req->bh->free = NULL;
                    req->bh->user = NULL;
                }
                req->bh->paddr = gcuRes.physMem->paddr;
                req->bh->vaddr = gcuRes.physMem->vaddr;
                req->bh->len = gcuRes.physMem->size;
                req->bh->user = gcuRes.physMem;
                req->bh->free = _physMemFree;
            }else {
                MemoryFreeData *freedata = new MemoryFreeData;
                freedata->physMem = gcuRes.physMem;
                freedata->user = req->bh->user;
                freedata->free = req->bh->free;
                req->bh->free = _physMemFree_TV;
                req->bh->user = freedata;
            }
			#else			
			//here can free src buffer to reduce wait time of Stagefirght
			if( req->bh->free != NULL ) {
				req->bh->free( req->bh->user );
				req->bh->free = NULL;
				req->bh->user = NULL;
			}
			req->bh->paddr = gcuRes.physMem->paddr;
			req->bh->vaddr = gcuRes.physMem->vaddr;
			req->bh->len = gcuRes.physMem->size;
			req->bh->user = gcuRes.physMem;
			req->bh->free = _physMemFree;
			#endif
            if( !req->testFlag(GCU_FLAG_ROTATE_IN_TV) ) {
                req->bh->paddr = gcuRes.physMem->paddr;
                req->bh->vaddr = gcuRes.physMem->vaddr;
            }
            if( req->testFlag(GCU_FLAG_ROTATE_IN_TV) ) {
                if( doFlip( gcuAgent->fd, &req->vb, &gcuRes.vb ) == false ) {
                    delete req;
                    LOGE("%s:%d. failed doFlip()", __FUNCTION__, __LINE__);
                    return false;
                }

            } else {
            	#ifdef HAVE_HDMI_SERVICE
                if( req->testAct(GCU_DO_ROTATE) && (gcuAgent->mHdmiConnState == 1)) {
                    // video rotateed
                    if( req->rot_degree & OVERLAY_TRANSFORM_ROT_90 ) {
                        if( doFlip( gcuAgent->fd, &gcuRes.vb, &req->vb ) == false ) {
                            delete req;
                            LOGE("%s:%d. failed doFlip()", __FUNCTION__, __LINE__);
                            return false;
                        }
                    }else {
                        if( doFlip( gcuAgent->fd, &gcuRes.vb, 0 ) == false ) {
                            delete req;
                            LOGE("%s:%d. failed doFlip()", __FUNCTION__, __LINE__);
                            return false;
                        }
                    }
                } else 
                #endif
                {
                    // something done, the to-show data is in gcuRes
                    if( doFlip( gcuAgent->fd, &gcuRes.vb, 0 ) == false ) {
                        delete req;
                        LOGE("%s:%d. failed doFlip()", __FUNCTION__, __LINE__);
                        return false;
                    }
                }
            }
        }
        else {
            // nothing done, the to-show data is still in gcuReq
            if( doFlip( gcuAgent->fd, &req->vb, 0 ) == false ) {
                delete req;
                LOGE("%s:%d. failed doFlip()", __FUNCTION__, __LINE__);
                return false;
            }
        }
        delete req;

        LOGV("%s#%d. successfully processed & flipped one video request.",
                __FUNCTION__, __LINE__ );
    }while( exitPending()==false );
    LOGD("%s#%d. GCUAgent worker thread exiting.",
                __FUNCTION__, __LINE__ );

    return false;
}

static double _time_ms( struct timespec *t1, struct timespec *t2 ) {
    double ret = 0;
    if( t2->tv_nsec < t1->tv_nsec ) {
        t2->tv_sec--;
        ret = ( t2->tv_nsec + 1000000000 ) - t1->tv_nsec;
    }else {
        ret = t2->tv_nsec - t1->tv_nsec;
    }

    ret += ( t2->tv_sec - t1->tv_sec ) * 1000000000;

    return ret/1000000;
}

/**
 * color conversion: 420SP --> 420P
 */
bool GCUAgent::_color420SP_to_420P( GCURequest *gcuReq ) {
    // alloc phys mem
    unsigned int memSize = gcuReq->vb.len;
    PhysMem *pPhysMem = memPool->alloc( memSize );
    if( pPhysMem == NULL ) {
        LOGE("%s#%d: failed alloc PhysMem for dst surface", __FUNCTION__, __LINE__ );
        return false;
    }

    int ippRet = 0;
    Ipp8u *pSrc[2], *pDst[3];
    int srcStep[2], dstStep[3];
    int ySize = gcuReq->vb.height * gcuReq->vb.y_step;
    IppiSize roiSize;

    pSrc[0] = (Ipp8u*)(gcuReq->vb.vaddr);
    pSrc[1] = pSrc[0] + ySize;
    pDst[0] = (Ipp8u*)(pPhysMem->vaddr);
    pDst[2] = pDst[0] + ySize;
    pDst[1] = pDst[2] + (ySize>>2);
    srcStep[0] = gcuReq->vb.y_step; // from bh.step0
    srcStep[1] = gcuReq->vb.u_step; // from bh.step1
    dstStep[0] = gcuReq->vb.y_step;
    dstStep[1] = dstStep[2] = srcStep[1]>>1;
    roiSize.height = gcuReq->vb.height;
    roiSize.width = gcuReq->vb.width;

    ippRet = ippiYCbCr420SPToYCbCr420P_8u_P2P3R( (const Ipp8u**)pSrc, srcStep,
                                                    pDst, dstStep, roiSize );
    if( ippRet != ippStsNoErr ) {
        LOGE("%s#%d. Failed ippiYCbCr420SPToYCbCr420P_8u_P2P3R()",
                __FUNCTION__, __LINE__ );
        pPhysMem->release();
        return false;
    }
    if( gcuReq->bh->free != NULL ) {
        gcuReq->bh->free( gcuReq->bh->user );
        gcuReq->bh->free = NULL;
        gcuReq->bh->user = NULL;
    }
    gcuReq->bh->paddr = pPhysMem->paddr;
    gcuReq->bh->vaddr = pPhysMem->vaddr;
    gcuReq->bh->len = memSize;//pPhysMem->size;
    gcuReq->bh->user = pPhysMem;
    gcuReq->bh->free = _physMemFree;
    gcuReq->vb.ovlyFmt = OVERLAY_FORMAT_YCbCr_420_P;
    gcuReq->vb.len = memSize;
    gcuReq->vb.paddr = pPhysMem->paddr;
    gcuReq->vb.vaddr = pPhysMem->vaddr;

    return true;
}

/**
 * do orientation flip
 *
 */
#if DO_ORIENTATION_FLIP
bool GCUAgent::_doOrientationFlip( GCURequest *gcuReq, int ovlyRotFlag ) {

    bool ret = true;
    unsigned int xStride = gcuReq->vb.stride_x;
    unsigned int yStride = gcuReq->vb.stride_y;
    unsigned int gcuW = ALIGN16(xStride);
    unsigned int gcuH = ALIGN4(yStride);
    //create gcu src surface
    GCUSurface srcSurf = _gcuCreatePreAllocBuffer( gcuCtx,
                                                   gcuW, gcuH,
                                                   _getGCUFmt( gcuReq->vb.ovlyFmt ),
                                                   GCU_TRUE, gcuReq->vb.vaddr,
                                                   GCU_TRUE, (GCUPhysicalAddr)gcuReq->vb.paddr );
    if( srcSurf == NULL ) {
        LOGE("%s()#%d - failed create src surface", __FUNCTION__, __LINE__);
        return false;
    }
    GCU_RECT srcRect;
    srcRect.left  = gcuReq->vb.x_off;
    srcRect.right = gcuReq->vb.x_off + gcuReq->vb.width;
    srcRect.top   = gcuReq->vb.y_off;
    srcRect.bottom= gcuReq->vb.y_off + gcuReq->vb.height;
    //create gcu dst surface
    unsigned int memSize = gcuW * gcuH * 2; // RGB565
    PhysMem * pPhysMem = memPool->alloc( memSize );
    if( !pPhysMem ) {
        LOGE("%s()#%d - failed alloc phys mem for dst surface", __FUNCTION__, __LINE__);
        return false;
    }
    GCUSurface dstSurf = _gcuCreatePreAllocBuffer( gcuCtx,
                                                   gcuW, gcuH,
                                                   GCU_FORMAT_RGB565,
                                                   GCU_TRUE, pPhysMem->vaddr,
                                                   GCU_TRUE, (GCUPhysicalAddr)pPhysMem->paddr );
    if( dstSurf == NULL ) {
        LOGE("%s()#%d - failed create dst surface", __FUNCTION__, __LINE__);
        return false;
    }
    GCU_RECT dstRect;
    dstRect.left  = 0;
    dstRect.right = gcuW;
    dstRect.top   = 0;
    dstRect.bottom= gcuH;
    // call gcu blt
    GCU_BLT_DATA bltData;
    memset( &bltData, 0, sizeof(bltData) );
    bltData.pSrcSurface = srcSurf;
    bltData.pDstSurface = dstSurf;
    bltData.pSrcRect = &srcRect;
    bltData.pDstRect = &dstRect;
    bltData.rotation = _getGCURotationDegree( ovlyRotFlag );
    gcuBlit( gcuCtx, &bltData );
    gcuFinish( gcuCtx );
    //free surfaces resource
    gcuDestroySurface( gcuCtx, srcSurf );
    srcSurf = NULL;
    gcuDestroySurface( gcuCtx, dstSurf );
    dstSurf = NULL;

    //use output buffer as src buffer for next plugin
    if( gcuReq->bh->free != NULL ) {
        gcuReq->bh->free( gcuReq->bh->user );
        gcuReq->bh->free = NULL;
        gcuReq->bh->user = NULL;
    }
    gcuReq->bh->paddr = pPhysMem->paddr;
    gcuReq->bh->vaddr = pPhysMem->vaddr;
    gcuReq->bh->len = memSize;//actually = pPhysMem->size
    gcuReq->bh->user = pPhysMem;
    gcuReq->bh->free = _physMemFree;
    gcuReq->vb.ovlyFmt = _getOverlayFmt( GCU_FORMAT_RGB565 );
    gcuReq->vb.len = memSize;
    gcuReq->vb.paddr = pPhysMem->paddr;
    gcuReq->vb.vaddr = pPhysMem->vaddr;

    return ret;
}
#endif

/**
 * do processing
 * if do processing within this function, it should return true, otherwise false
 */
bool GCUAgent::process( GCURequest *gcuReq, GCUResult *gcuRes ) {

    bool ret = true;
    // 420sp --> 420p
    if( gcuReq->testAct(GCU_DO_COLORCONVERT) && gcuReq->vb.ovlyFmt == OVERLAY_FORMAT_YCbCr_420_SP ) {
        LOGV("%s#%d: doing 420sp --> %d", __FUNCTION__, __LINE__, gcuReq->new_fmt );
        if( gcuReq->new_fmt != OVERLAY_FORMAT_YCbCr_420_P ) {
            LOGE("%s#%d: Error for 420SP conversion, .new_fmt MUST be 420P",
                    __FUNCTION__, __LINE__ );
            return false;
        }
        if( !_color420SP_to_420P( gcuReq ) ) {
            LOGE("%s#%d: Failed _color420SP_to_420P()", __FUNCTION__, __LINE__ );
            return false;
        }
        gcuReq->removeAct(GCU_DO_COLORCONVERT);
    }

    // handle OVERLAY_TRANSFORM_FLIP_H|OVERLAY_TRANSFORM_ROT_90, OVERLAY_TRANSFORM_FLIP_V|OVERLAY_TRANSFORM_ROT_90

#if DO_ORIENTATION_FLIP
    if( gcuReq->rot_degree == (OVERLAY_TRANSFORM_FLIP_H|OVERLAY_TRANSFORM_ROT_90) ||
        gcuReq->rot_degree == (OVERLAY_TRANSFORM_FLIP_V|OVERLAY_TRANSFORM_ROT_90) )
    {

        if(gcuReq->rot_degree & OVERLAY_TRANSFORM_FLIP_H)
        {
            if(!_doOrientationFlip( gcuReq, OVERLAY_TRANSFORM_FLIP_H ) )
            {
               LOGE("%s#%d: Failed _doOrientationFlip", __FUNCTION__, __LINE__ );
               return false;
            } else {
               gcuReq->rot_degree &= ~OVERLAY_TRANSFORM_FLIP_H;
            }
        }

        if(gcuReq->rot_degree & OVERLAY_TRANSFORM_FLIP_V)
        {
            if(!_doOrientationFlip( gcuReq, OVERLAY_TRANSFORM_FLIP_H ) )
            {
               LOGE("%s#%d: Failed _doOrientationFlip", __FUNCTION__, __LINE__ );
               return false;
            } else {
               gcuReq->rot_degree &= ~OVERLAY_TRANSFORM_FLIP_V;
            }
        }
    }
#endif

	if( gcuReq->testAct(GCU_DO_ROTATE) || gcuReq->testAct(GCU_DO_DEINTERLACE) ) {
        // src stuff
        GCUSurface srcSurface = NULL;
        GCU_RECT srcRect;
        if( _createSrcSurface( gcuReq, &srcSurface, &srcRect ) == false ) {
            LOGE("%s#%d: failed create src GCUSurface", __FUNCTION__, __LINE__);
            return false;
        }
        // dump src buffer
#if DUMP_SRC
        {
            static int indexSrcDump = 0;
            indexSrcDump++;
            char fn[128];
            memset(fn, 0, 128);
            sprintf(fn, "/data/src_%d", indexSrcDump);
            _gcuDumpSurface(gcuCtx,
                            srcSurface,
                            fn);
        }
#endif
        // dst stuff
        GCUSurface dstSurface = NULL;
        GCU_RECT dstRect;
        PhysMem *pDstPhysMem = NULL;
        if( _createDstSurface( gcuReq, &dstSurface, &dstRect, gcuRes ) == false ) {
            LOGE("%s#%d: failed create dst GCUSurface", __FUNCTION__, __LINE__);
            return false;
        }
        // call GCU to do process
        GCU_BLT_DATA bltData;
        memset( &bltData, 0, sizeof(bltData) );
        bltData.pSrcSurface = srcSurface;
        bltData.pDstSurface = dstSurface;
        bltData.pSrcRect = &srcRect;
        bltData.pDstRect = &dstRect;
        bltData.rotation = _getGCURotationDegree( gcuReq->rot_degree );
        gcuBlit( gcuCtx, &bltData );
        gcuFinish( gcuCtx );
        // dump dst buffer
#if DUMP_DST
        {
            static int indexDstDump = 0;
            indexDstDump++;
            char fn[128];
            memset(fn, 0, 128);
            sprintf(fn, "/data/dst_%d", indexDstDump);
            _gcuDumpSurface(gcuCtx,
                            dstSurface,
                            fn);
        }
#endif
        //free surfaces resource
        gcuDestroySurface( gcuCtx, srcSurface );
        srcSurface = NULL;
        gcuDestroySurface( gcuCtx, dstSurface );
        dstSurface = NULL;

        // At this point, the result already is updated in _createDstSurface()
        // gcu action is required for hdmi mirror, do not remove it.
        //gcuReq->removeAct( GCU_DO_ROTATE );
        //gcuReq->removeAct( GCU_DO_DEINTERLACE );
        //dumpCnt++;
    }
    else {
        ret = false;
    }

    return ret;
}

/**
 * create source surface for GCURequest
 */
bool GCUAgent::_createSrcSurface( GCURequest *gcuReq, GCUSurface *pSrcSurface, GCU_RECT *pSrcRect ) {
    unsigned int xStride = gcuReq->vb.stride_x;
	unsigned int yStride = gcuReq->vb.stride_y;
    unsigned int gcuW = ALIGN16(xStride);
    unsigned int gcuH = ALIGN4(yStride);
    if( gcuReq->testAct(GCU_DO_DEINTERLACE) ) {
        gcuW = ALIGN16(xStride*2);
		gcuH = ALIGN4(yStride/2);
    }

    *pSrcSurface = _gcuCreatePreAllocBuffer( gcuCtx,
                                            gcuW, gcuH,
                                            _getGCUFmt( gcuReq->vb.ovlyFmt ),
                                            GCU_TRUE, gcuReq->vb.vaddr,
                                            GCU_TRUE, (GCUPhysicalAddr)gcuReq->vb.paddr );
    if( *pSrcSurface == NULL ) {
        LOGE("%s#%d: failed create src by _gcuCreatePreAllocBuffer()", __FUNCTION__, __LINE__);
        return false;
    }

    pSrcRect->left   = gcuReq->vb.x_off;
	pSrcRect->right  = gcuReq->vb.x_off + gcuReq->vb.width;
	pSrcRect->top    = gcuReq->vb.y_off;
    if( gcuReq->testAct(GCU_DO_DEINTERLACE) ) {
        pSrcRect->bottom = gcuReq->vb.y_off + (gcuReq->vb.height / 2);
    }
    else {
        pSrcRect->bottom = gcuReq->vb.y_off + gcuReq->vb.height;
    }


    return true;
}

/**
 * create destination surface for GCURequest
 */
bool GCUAgent::_createDstSurface( GCURequest *gcuReq, GCUSurface *pDstSurface, GCU_RECT *pDstRect,
                                  GCUResult *gcuResult ) {
    unsigned int xStride = gcuReq->vb.stride_x;
	unsigned int yStride = gcuReq->vb.stride_y;
    unsigned int gcuW = ALIGN16(xStride);
    unsigned int gcuH = ALIGN4(yStride);
	if( gcuReq->testAct( GCU_DO_ROTATE ) && (gcuReq->rot_degree & OVERLAY_TRANSFORM_ROT_90) ) {
             gcuW = ALIGN16( yStride );
	     gcuH = ALIGN4( xStride );
	}
    // alloc phys mem
    unsigned int memSize = gcuW * gcuH * 2; // RGB565
    PhysMem * pPhysMem = memPool->alloc( memSize );
    if( pPhysMem == NULL ) {
        LOGE("%s#%d: failed alloc PhysMem for dst survace", __FUNCTION__, __LINE__);
        return false;
    }
    // create surface
    *pDstSurface = _gcuCreatePreAllocBuffer( gcuCtx,
                            gcuW, gcuH,
                            GCU_FORMAT_RGB565,
                            GCU_TRUE, pPhysMem->vaddr,
                            GCU_TRUE, (GCUPhysicalAddr)pPhysMem->paddr );
    if( *pDstSurface == NULL ) {
        LOGE("%s#%d: failed create dst by _gcuCreatePreAllocBuffer()", __FUNCTION__, __LINE__);
        return false;
    }

    pDstRect->left   = 0;
	pDstRect->right  = gcuW;
	pDstRect->top    = 0;
	pDstRect->bottom = gcuH;

    // update GCUResult's fields
    gcuResult->physMem = pPhysMem;
    gcuResult->vb.paddr = pPhysMem->paddr;
    gcuResult->vb.vaddr = pPhysMem->vaddr;
    gcuResult->vb.len = memSize;
    gcuResult->vb.ovlyFmt = _getOverlayFmt( GCU_FORMAT_RGB565 );
    gcuResult->vb.x_off = 0;
    gcuResult->vb.y_off = 0;
    gcuResult->vb.width = gcuW;
    gcuResult->vb.height = gcuH;
    gcuResult->vb.stride_x = gcuW;
    gcuResult->vb.stride_y = gcuH;

    return true;
}

GCU_FORMAT GCUAgent::_getGCUFmt( int ovlyFmt ) {
	GCU_FORMAT ret = GCU_FORMAT_UNKNOW;
	switch(ovlyFmt) {
		case OVERLAY_FORMAT_RGBA_8888:
			break;
		case OVERLAY_FORMAT_RGB_565:
			ret = GCU_FORMAT_RGB565;
			break;
		case OVERLAY_FORMAT_BGRA_8888:
			break;
#if PLATFORM_SDK_VERSION < 9
		case OVERLAY_FORMAT_YCbCr_422_SP:
			break;
		case OVERLAY_FORMAT_YCbCr_420_SP:
			ret = GCU_FORMAT_YV12;
			break;
#endif
		case OVERLAY_FORMAT_YCbYCr_422_I:
			ret = GCU_FORMAT_UYVY;
			break;
#if PLATFORM_SDK_VERSION < 9
		case OVERLAY_FORMAT_YCbYCr_420_I:
			break;
#endif
		case OVERLAY_FORMAT_YCbCr_420_P:
			ret = GCU_FORMAT_YV12;
			break;
		default:
			break;
	}
	return ret;
}

int GCUAgent::_getOverlayFmt( GCU_FORMAT gcuFmt ) {
    if( gcuFmt == GCU_FORMAT_RGB565 ) {
        return OVERLAY_FORMAT_RGB_565;
    }
    return -1;
}

GCU_ROTATION GCUAgent::_getGCURotationDegree( int ovlyRot ) {
	unsigned int gcuRot = (unsigned int)GCU_ROTATION_0;
        if( ovlyRot & OVERLAY_TRANSFORM_FLIP_H ) {
            gcuRot |= (unsigned int)GCU_ROTATION_FLIP_H;
        }
        if( ovlyRot & OVERLAY_TRANSFORM_FLIP_V ) {
            gcuRot |= (unsigned int)GCU_ROTATION_FLIP_V;
        }
        if( ovlyRot & OVERLAY_TRANSFORM_ROT_90 ) {
            gcuRot |= (unsigned int)GCU_ROTATION_90;
        }
	LOGV("%s() - gcuRot = 0x%08x", __FUNCTION__, gcuRot);

	return (GCU_ROTATION)gcuRot;
}

void GCURequest::dump(){    
    LOGD("GCURequst:: action = %d, rot_degree = %d", actions, rot_degree);
}


