#include "HdmiHelper.h"
#include "HdmiVideoFormat.h"
#define LOG_TAG "HdmiHelper"
#include <utils/Log.h>
namespace android{
HdmiHelper::HdmiHelper(sp < IHdmiService > service, int res)
    :mSupportFullHD(true), mIs720P(false), m1080pRes(0), m720pRes(0), mOrigRes(0), mCurrRes(0){
    mService = service;
    mOrigRes = mService->get_current_display_resolution();
    mCurrRes = mOrigRes;
    getDefault(res);
}

HdmiHelper::HdmiHelper(sp < IHdmiService > service) 
    : mSupportFullHD(false), mIs720P(false), m1080pRes(0), m720pRes(0), mOrigRes(0){
    mService = service;
}

HdmiHelper:: ~HdmiHelper() {
    // resotre orig
    if( mCurrRes != mOrigRes ) {
        mService->set_display_resolution(mOrigRes);
    }
}
void HdmiHelper::getDefault(int res) {
    if( res & 
        (VIDEO_RES_1080P30 | VIDEO_RES_1080P2997 | VIDEO_RES_1080P25 |
        VIDEO_RES_1080P24 | VIDEO_RES_1080P2398 | VIDEO_RES_1080P60  |
        VIDEO_RES_1080P5994 | VIDEO_RES_1080P50 )  == 0 ) {
        mSupportFullHD = false;
    }
    if( mSupportFullHD ) {
        // get default 1080p resolution
        if( res & VIDEO_RES_1080P60 ) {
            m1080pRes = VIDEO_RES_1080P60;
        } else if( res & VIDEO_RES_1080P5994 ) {
            m1080pRes = VIDEO_RES_1080P5994;
        } else if( res & VIDEO_RES_1080P50 ) {
            m1080pRes = VIDEO_RES_1080P50;
        } else if( res & VIDEO_RES_1080P30 ) {
            m1080pRes = VIDEO_RES_1080P30;
        } else if( res & VIDEO_RES_1080P2997) {
            m1080pRes = VIDEO_RES_1080P2997;
        } else if( res & VIDEO_RES_1080P25 ) {
            m1080pRes = VIDEO_RES_1080P25;
        } else if( res & VIDEO_RES_1080P24 ) {
            m1080pRes = VIDEO_RES_1080P24;
        } else if( res & VIDEO_RES_1080P2398 ) {
            m1080pRes = VIDEO_RES_1080P2398;
        } else {
            LOGE("we should never go here, no 1080p support");
        }

        // get default 720p resolution
        if( res & VIDEO_RES_720P60 ) {
            m720pRes = VIDEO_RES_720P60;
        } else if( res & VIDEO_RES_720P5994 ) {
            m720pRes = VIDEO_RES_720P5994;
        } else if( res & VIDEO_RES_720P50 ) {
            m720pRes = VIDEO_RES_720P50;
        } else if( res & VIDEO_RES_720P30 ) {
            m720pRes = VIDEO_RES_720P30;
        } else if( res & VIDEO_RES_720P2997 ) {
            m720pRes = VIDEO_RES_720P2997;
        } else if( res & VIDEO_RES_720P25 ) {
            m720pRes = VIDEO_RES_720P25;
        } else {
            LOGE("we should never go here, no 720p support");
        }
    }
}
void HdmiHelper::CheckAndUpdate(bool full_screen, bool need_720p) {
    Mutex::Autolock _l( mLock );
    if( !mSupportFullHD ) {
        // do nothing
        return;
    } else {
        if( full_screen ) {
            if( need_720p && !mIs720P ) {
                // need set to 720p
                if( mCurrRes != m720pRes ) {
                    mService->set_display_resolution(m720pRes);
                    mCurrRes = m720pRes;
                }
                mIs720P = true;
            }
        } else if ( mIs720P ) {
            // reset to 1080p
            if( mCurrRes != m1080pRes ) {
                mService->set_display_resolution(m1080pRes);
                mCurrRes = m1080pRes;
            }
            mIs720P = false;
	} 
    }
}

void HdmiHelper::SinkStateChanged(int conn, int res) {
    Mutex::Autolock _l( mLock );    
    if( conn ) {
        mOrigRes = mService->get_current_display_resolution();
        mCurrRes = mOrigRes;
        getDefault(res);
    } else {
        mSupportFullHD = true;
        mIs720P = false;
        m1080pRes = 0;
        m720pRes = 0;
        mOrigRes = 0;
        mCurrRes = mOrigRes;
    }
}
};
