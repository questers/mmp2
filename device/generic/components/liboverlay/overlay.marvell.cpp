/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//#define LOG_NDEBUG 0

#include <hardware/hardware.h>
#include <hardware/overlay.h>

#include <fcntl.h>
#include <errno.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/poll.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <linux/types.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/android_pmem.h>

#define LOG_TAG "OverlayHal"

#include <cutils/log.h>
#include <cutils/atomic.h>
#include <cutils/properties.h>

#include "overlay.marvell.h"
#include "pxa168fb.h"

#if PLATFORM_SDK_VERSION <= 4
#include <utils/MemoryBase.h>
#include <utils/MemoryHeapBase.h>
#include <utils/MemoryHeapPmem.h>
#else
#include <binder/MemoryBase.h>
#include <binder/MemoryHeapBase.h>
#include <binder/MemoryHeapPmem.h>
#endif

#include "gcu.h"
#include "common.h"
#include "PhysMemPool.h"
#include "GCUAgent.h"
#include "dumpers.h"

using namespace android;


/**
 * macros for read/write user_data fields in _sOvlySurface
 */
#define _SHVAR_ROTATE( ovlySurf )  ( ((int*)( (char*)(ovlySurf.user_data)+0 ))[0] )
#define _SHVAR_DEINTERLACE( ovlySurf )  ( ((int*)( (char*)(ovlySurf.user_data)+0 ))[1] )
#define _SHVAR_SwitchOn( ovlySurf )  ( ((int*)( (char*)(ovlySurf.user_data)+0 ))[2] )

static int _readOvlySurface( int fd, struct _sOvlySurface *pOvlySurf )
{
    if(ioctl(fd, FB_IOCTL_GET_SURFACE, pOvlySurf )) {
        LOGE("read _sOvlySurface from overlay-drv failed.");
        return -1;
    }
    return 0;
}
static int _writeOvlySurface( int fd, const struct _sOvlySurface *pOvlySurf )
{
    if(ioctl(fd, FB_IOCTL_SET_SURFACE, pOvlySurf )) {
        LOGE("write _sOvlySurface to overlay-drv failed.");
        return -1;
    }
    return 0;
}

/*****************************************************************************/

struct overlay_control_context_t {
    struct overlay_control_device_t device;
    /* our private state goes below here */
};

struct overlay_data_context_t {
    struct overlay_data_device_t device;
    /* our private state goes below here */
    int data_handle_index;
};

static int overlay_device_open(const struct hw_module_t* module, const char* name,
        struct hw_device_t** device);


static struct hw_module_methods_t overlay_module_methods = {
    open: overlay_device_open
};

struct overlay_module_t HMI = {
    common: {
        tag: HARDWARE_MODULE_TAG,
        version_major: 1,
        version_minor: 0,
        id: OVERLAY_HARDWARE_MODULE_ID,
        name: "Marvell Sample Overlay module",
        author: "Marvell",
        methods: &overlay_module_methods,
    }
};

/*****************************************************************************/

/*
 * This is the overlay_t object, it is returned to the user and represents
 * an overlay.
 * This handles will be passed across processes and possibly given to other
 * HAL modules (for instance video decode modules).
 */

class overlay_object : public overlay_t {

public:
    struct handle_t : public native_handle {
        /* add the data fields we need here, for instance: */
        int ov_fd;
        // source data format
        int format;
        // driver format
        int drv_format;
        // orignal frame width
        int step;
        // orignal frame height
        int height;
        // hdmi output ?
        int hdmi;
        // baselay width
        int screen_width;
        // baselay height
        int screen_height;
    };

    int pos_x;
    int pos_y;
    int dst_w;
    int dst_h;
    int rotate;
    // save staged setting from control channel
    struct _sOvlySurface ovly_surf_cfg;
    int invalid_position;// the input of setPosition() is valid

    static overlay_handle_t getHandleRef(struct overlay_t* overlay) {
        /* returns a reference to the handle, caller doesn't take ownership */
        return &(static_cast<overlay_object *>(overlay)->mHandle);
    }

    handle_t mHandle;

    overlay_object(int w, int h, int f, int drv_f, int fd, int tv, int bw, int bh) {
        this->overlay_t::getHandleRef = getHandleRef;
        mHandle.version = sizeof(native_handle);
        mHandle.numFds = 1;
        int tmpExtraInts = ( ( sizeof(handle_t)-sizeof(native_handle) ) + (sizeof(int)-1) ) / sizeof(int);
        mHandle.numInts = tmpExtraInts - mHandle.numFds; // extra ints exclusive numFds  we have in  our handle
        mHandle.ov_fd = fd;
        mHandle.format = f;
        mHandle.drv_format = drv_f;
        mHandle.step = w;
        mHandle.height = h;
        mHandle.hdmi = tv;
        mHandle.screen_width = bw;
        mHandle.screen_height = bh;
        pos_x = 0;
        pos_y = 0;
        dst_w = 0;
        dst_h = 0;
        rotate = 0;
        memset( &ovly_surf_cfg, 0, sizeof( _sOvlySurface ) );
        invalid_position = 0;
    }
};

// 0 : for panel overlay ; 1 : for hdmi overlay
static overlay_object *gOverlayObj[2] = {NULL , NULL };
static int gBaselayWidth[2] ={ 0, 0 };
static int gBaselayHeight[2] ={ 0, 0 };
static Mutex gLock; // lock for create/destroy overlay

// ****************************************************************************
// Control module
// ****************************************************************************

static int overlay_get(struct overlay_control_device_t *dev, int name) {
    int result = -1;
    switch (name) {
        case OVERLAY_MINIFICATION_LIMIT:
            result = 0; // 0 = no limit
            break;
        case OVERLAY_MAGNIFICATION_LIMIT:
            result = 0; // 0 = no limit
            break;
        case OVERLAY_SCALING_FRAC_BITS:
            result = 0; // 0 = infinite
            break;
        case OVERLAY_ROTATION_STEP_DEG:
            result = 90; // 90 rotation steps (for instance)
            break;
        case OVERLAY_HORIZONTAL_ALIGNMENT:
            result = 1; // 1-pixel alignment
            break;
        case OVERLAY_VERTICAL_ALIGNMENT:
            result = 1; // 1-pixel alignment
            break;
        case OVERLAY_WIDTH_ALIGNMENT:
            result = 1; // 1-pixel alignment
            break;
        case OVERLAY_HEIGHT_ALIGNMENT:
            result = 1; // 1-pixel alignment
            break;
    }
    return result;
}


/**
 * calc color-key's mode from fb_var_screeninfo
 */
unsigned int _getColorKeyMode( struct fb_var_screeninfo *var ) {
    unsigned int ret = FB_DISABLE_COLORKEY_MODE;
    /*
     * Check for 565/1555.
     */
    if (var->bits_per_pixel == 16 && var->red.length <= 5 &&
            var->green.length <= 6 && var->blue.length <= 5) {
        if (var->transp.length == 0) {
            if (var->red.offset >= var->blue.offset)
                ret = FB_ENABLE_RGB_COLORKEY_MODE;//PIX_FMT_RGB565;
            else
                ret = FB_ENABLE_RGB_COLORKEY_MODE;//PIX_FMT_BGR565;
        }
    }
    // FIXME: for other cases, currently use FB_DISABLE_COLORKEY_MODE;
    if( ret == FB_DISABLE_COLORKEY_MODE )
        LOGD("%s() FB_DISABLE_COLORKEY_MODE", __FUNCTION__ );
    else
        LOGD("%s() FB_ENABLE_RGB_COLORKEY_MODE", __FUNCTION__ );

    return ret;
}

int overlay_get_index(overlay_t* overlay) {
    int index = -1;
    if( gOverlayObj[0] == overlay ) {
        index = 0;
    } else if( gOverlayObj[1] == overlay ) {
        index = 1;
    }
    return index;
}
static overlay_t* overlay_createOverlay(struct overlay_control_device_t *dev,
         uint32_t w, uint32_t h, int32_t format)
{


    char value[PROPERTY_VALUE_MAX];
    int index = 0;
    Mutex::Autolock _l( gLock );
    property_get( PROP_VIDEO_PATH, value, "normal" );
    if( strcmp(value, "hdmi") == 0 ) {
        index = 1;
    }

    // restore it
    property_set(PROP_VIDEO_PATH, "normal");

    LOGD("overlay_createOverlay on %s----------------%p",index?"hdmi tv":"lcd panel", dev);
    LOGD("overlay_createOverlay width:%d, height:%d", w, h);

    // currently we only allow two overlay : one for hdmi, one for panel
    if( gOverlayObj[index] != NULL ) {
         LOGE("Overlay %s is not null", index?"hdmi tv":"lcd panel");
         return NULL;
    }

    int f = 0;
    /* check the input params, reject if not supported or invalid */
    switch (format) {
        case OVERLAY_FORMAT_RGBA_8888:
            f = FB_VMODE_RGBA888;
            break;
        case OVERLAY_FORMAT_RGB_565:
            f = FB_VMODE_RGB565;
            break;
        case OVERLAY_FORMAT_BGRA_8888:
            f = FB_VMODE_BGRA888;
            break;
#if PLATFORM_SDK_VERSION < 9
        case OVERLAY_FORMAT_YCbCr_422_SP:
            f = FB_VMODE_YUV422PLANAR;
            break;
        case OVERLAY_FORMAT_YCbCr_420_SP:
            f = FB_VMODE_YUV420PLANAR;
            break;
#endif
#if PLATFORM_SDK_VERSION >= 5
        case OVERLAY_FORMAT_YCbYCr_422_I:
            f = FB_VMODE_YUV422PACKED;
            break;
#if PLATFORM_SDK_VERSION < 9
        case OVERLAY_FORMAT_YCbYCr_420_I:
            return NULL;
#endif
#endif
        case OVERLAY_FORMAT_YCbCr_420_SP:
        case OVERLAY_FORMAT_YCbCr_420_P:
            f = FB_VMODE_YUV420PLANAR;
            break;

	case OVERLAY_FORMAT_DEFAULT:
 	    f = FB_VMODE_YUV422PACKED;
	    format = OVERLAY_FORMAT_YCbYCr_422_I;
 	    break;

        default:
            return NULL;
    }

    int fd;
    struct fb_var_screeninfo var_base;
    const char* base_device = NULL;
    const char* ov_device = NULL;
    if( index == 1 ) {
        base_device = "/dev/graphics/fb1";
        ov_device = "/dev/graphics/fb3";
    } else {
        base_device = "/dev/graphics/fb0";
        ov_device = OVERLAY_DEVICE_PATH;
    }

    fd  = open(base_device, O_RDWR);
    int ret = ioctl(fd, FBIOGET_VSCREENINFO, &var_base);
    close(fd);
    if( ret < 0 ) {
        LOGE("Get %s vscreen info fail", index ? "hdmi" : "panel");
        return NULL;
    }

    gBaselayWidth[index] = var_base.xres;
    gBaselayHeight[index] = var_base.yres;

    LOGD("Baselayer[%s] resolution = %d*%d", index ? "hdmi" : "panel", gBaselayWidth[index], gBaselayHeight[index]);
    fd = open(ov_device, O_RDWR);
    if( fd < 0 ) {
        LOGE("Open overlay[%s] device fail", index ? "hdmi" : "panel");
        return NULL;
    }

    //LOGD("[#%d] before SET-VIDEO-MODE, fmt=0x%x", __LINE__, f );
    if(ioctl(fd, FB_IOCTL_SET_VIDEO_MODE, &f)) {
        LOGE("Set video mode fail %x\n", f);
        close(fd);
        return NULL;
    }

    struct _sColorKeyNAlpha colorkey;
    colorkey.mode = _getColorKeyMode( &var_base );
    colorkey.alphapath = FB_GRA_PATH_ALPHA;
    colorkey.config = 0x20;
    colorkey.Y_ColorAlpha = 0x0;
    colorkey.U_ColorAlpha = 0x0;
    colorkey.V_ColorAlpha = 0x0;
    if( ioctl(fd, FB_IOCTL_SET_COLORKEYnALPHA, &colorkey) ) {
        LOGE("Set color key failed");
        close(fd);
        return NULL;
    }

    struct _sViewPortInfo ViewPortInfo;
    struct _sViewPortOffset ViewPortOffset;

    memset((void*)&ViewPortInfo, 0, sizeof(struct _sViewPortInfo));
    memset((void*)&ViewPortOffset, 0, sizeof(struct _sViewPortOffset));
    // work around for vmeta decode, now it can not provide ROI information,
    // some 1920 * 1080 video will be reported as 1920 * 1088
    // remove this workaround when ipp fixed the bug.
    if( w > 1920 ) {
        w = 1920;
    }
    if( h > 1080 ) {
        h = 1080;
    }

    // initialize ViewPortInfo
    ViewPortInfo.srcWidth = w;
    ViewPortInfo.srcHeight = h;
    ViewPortInfo.zoomXSize = gBaselayWidth[index];
    ViewPortInfo.zoomYSize = gBaselayHeight[index];

    if(ioctl(fd, FB_IOCTL_SET_VIEWPORT_INFO, &ViewPortInfo)) {
        LOGE("Set view port info fail\n");
        close(fd);
        return NULL;
    }

    // initialize ViewPortOffset
    ViewPortOffset.xOffset = 0;
    ViewPortOffset.yOffset = 0;

    if(ioctl(fd, FB_IOCTL_SET_VID_OFFSET, &ViewPortOffset)) {
        LOGE("Set view port offset fail\n");
        close(fd);
        return NULL;
    }

    // initialize struct _sOvlySurface in drv
    struct _sOvlySurface tmpOvlySurf;
    memset( &tmpOvlySurf, 0, sizeof( struct _sOvlySurface ) );
    memcpy( &tmpOvlySurf.viewPortInfo, &ViewPortInfo, sizeof( ViewPortInfo ) );
    memcpy( &tmpOvlySurf.viewPortOffset,&ViewPortOffset, sizeof( ViewPortOffset ) );
    tmpOvlySurf.videoMode = f;
    if( _writeOvlySurface( fd, &tmpOvlySurf ) ) {
        close(fd);
        return NULL;
    }

    /* Create overlay object. Talk to the h/w here and adjust to what it can
     * do. the overlay_t returned can  be a C++ object, subclassing overlay_t
     * if needed.
     *
     * we probably want to keep a list of the overlay_t created so they can
     * all be cleaned up in overlay_close().
     */
    gOverlayObj[index] = new overlay_object(w, h, format, f, fd, index,gBaselayWidth[index], gBaselayHeight[index] );

    LOGD("createOverlay on %s;  succ with dev = %p ", index?"hdmi":"panel", dev);

    return gOverlayObj[index];

}

static void overlay_destroyOverlay(struct overlay_control_device_t *dev,
         overlay_t* overlay)
{
    Mutex::Autolock _l( gLock );
    LOGD("Overlay_destroyOverlay with overlay = %p", overlay);
    /* free resources associated with this overlay_t */
    int index = overlay_get_index(overlay);
    if( index < 0 ){
        LOGE("Overlay_destroyOverlay failed , cannot find overlay = %p", overlay);
        LOGE("Available overlay = %p , %p", gOverlayObj[0], gOverlayObj[1]);
    }
    if( gOverlayObj[index] != NULL ) {
        if( gOverlayObj[index]->mHandle.ov_fd > 0 ) {
            // reset sOvlySurface in drv
            struct _sOvlySurface tmpOvlySurf;
            memset( &tmpOvlySurf, 0, sizeof( struct _sOvlySurface ) );
            _writeOvlySurface( gOverlayObj[index]->mHandle.ov_fd, &tmpOvlySurf );
            // disalbe overlay
            int on = 0;
            if(ioctl(gOverlayObj[index]->mHandle.ov_fd, FB_IOCTL_SWITCH_VID_OVLY, &on)) {
                LOGE("Switch off video overlay failed\n" );
            }
            close( gOverlayObj[index]->mHandle.ov_fd );
        }
        delete overlay;
        gOverlayObj[index] = NULL;
    }
}

static int overlay_setPosition(struct overlay_control_device_t *dev,
         overlay_t* overlay,
         int x, int y, uint32_t w, uint32_t h) {
    LOGD("Overlay_setPosition(%d, %d, %d, %d)", x, y, w, h);

    int index = overlay_get_index(overlay);
    if( index < 0 ) {
        LOGE("overlay_setPosition cannot find overlay = %p", overlay);
        LOGE("Available overlay = %p , %p", gOverlayObj[0], gOverlayObj[1]);
        return -EINVAL;
    }

    if( x < 0 || x >= gBaselayWidth[index]
       || y < 0 || y >= gBaselayHeight[index]
       || w < 0 || x + w > gBaselayWidth[index]
       || h < 0 || y + h > gBaselayHeight[index] ) {
       LOGE("overlay_setPosition request position and area are invalid : offset ( %d, %d); resolution(%d*%d)", x, y, w, h);
       gOverlayObj[index]->invalid_position = 1;
       return -EINVAL;
    }

    if( index == 1 ) {
        // hdmi output, re-calculate the x,y,w,h
        if( gOverlayObj[index]->mHandle.step * gBaselayHeight[index] > gOverlayObj[index]->mHandle.height * gBaselayWidth[index] ) {
            w = gBaselayWidth[index];
            h = gBaselayWidth[index] * gOverlayObj[index]->mHandle.height / gOverlayObj[index]->mHandle.step;
        } else {
            h = gBaselayHeight[index];
            w = gBaselayHeight[index] * gOverlayObj[index]->mHandle.step / gOverlayObj[index]->mHandle.height;
        }
        if( w % 2 != 0 ) {
            w = w - 1;
        }
        if( h % 2 != 0 ) {
            h = h - 1;
        }
        if( w == gBaselayWidth[index] ) {
            x = 0;
            y = (gBaselayHeight[index] - h) / 2;
        } else {
            y = 0;
            x = (gBaselayWidth[index] - w) / 2;
        }
        if( x % 2 != 0 ) {
            x = x - 1;
        }
        if( y % 2 != 0 ) {
            y = y - 1;
        }
    }

    /**
     * we just stage the setting into gOverlayObj[index].ovly_surf_cfg
     * and make them effective in commit(...)
     */
    struct _sViewPortInfo *pStagedVPI = &( gOverlayObj[index]->ovly_surf_cfg.viewPortInfo );
    struct _sViewPortOffset *pStagedVPO = &( gOverlayObj[index]->ovly_surf_cfg.viewPortOffset );

    memset( pStagedVPI, 0, sizeof( *pStagedVPI ) );
    memset( pStagedVPO, 0, sizeof( *pStagedVPO ) );

    pStagedVPI->zoomXSize = w;
    pStagedVPI->zoomYSize = h;
    pStagedVPI->srcWidth = gOverlayObj[index]->mHandle.step;
    pStagedVPI->srcHeight= gOverlayObj[index]->mHandle.height;

    pStagedVPO->xOffset = x;
    pStagedVPO->yOffset = y;

    gOverlayObj[index]->invalid_position = 0;

    /**
     * below saving will not affect getPosition any more,
     * we read the saved data in drv instead to serve getPosition(..).
     */
    gOverlayObj[index]->dst_w = w;
    gOverlayObj[index]->dst_h = h;

    gOverlayObj[index]->pos_x = x;
    gOverlayObj[index]->pos_y = y;

    return 0;
}

static int overlay_getPosition(struct overlay_control_device_t *dev,
         overlay_t* overlay,
         int* x, int* y, uint32_t* w, uint32_t* h) {
    LOGV("overlay_getPosition");
    int index = overlay_get_index(overlay);
    if( index < 0 ){
        LOGE("overlay_getPosition cannot find overlay = %p", overlay);
        LOGE("available overlay = %p , %p", gOverlayObj[0], gOverlayObj[1]);
        return -EINVAL;
    }

    int fd_ovly = gOverlayObj[index]->mHandle.ov_fd;
    struct _sOvlySurface drvOvlySurf;
    if( _readOvlySurface( fd_ovly, &drvOvlySurf ) ) {
        return -EINVAL;
    }
    *x = drvOvlySurf.viewPortOffset.xOffset;
    *y = drvOvlySurf.viewPortOffset.yOffset;
    *w = drvOvlySurf.viewPortInfo.zoomXSize;
    *h = drvOvlySurf.viewPortInfo.zoomYSize;

    return 0;
}

int overlay_gcu_rotate(int index, int value) {
    LOGV("overlay_gcu_rotate() entered. index = %d, and value =%d", index, value);
    int ret = -EINVAL;
    int degree = 0;
    const char* rotate_str[] = {
        "0",
        "90",
        "180",
        "270"
    };
    struct _sViewPortInfo info;
    int format;
    memset((void*)&info, 0, sizeof(struct _sViewPortInfo));
    if(ioctl(gOverlayObj[index]->mHandle.ov_fd, FB_IOCTL_GET_VIEWPORT_INFO, &info)) {
        LOGE("overlay_rotate : FB_IOCTL_GET_VIEWPORT_INFO failed");
        return ret;
    }

    int origin_w = gOverlayObj[index]->mHandle.step;
    int align16_w = ALIGN16( origin_w );
    int align4_w = ALIGN4( origin_w );
    int origin_h = gOverlayObj[index]->mHandle.height;
    int align16_h = ALIGN16( origin_h );
    int align4_h = ALIGN4( origin_h );

    if( value == 0 ) {
        info.srcWidth = origin_w;
        info.srcHeight = origin_h;
        info.yPitch = 0;
        format = gOverlayObj[index]->mHandle.drv_format;
    }else {
        if( value & OVERLAY_TRANSFORM_ROT_90 ) {
            info.srcWidth = origin_h;
            info.srcHeight = origin_w;
            info.yPitch = align16_h;
        }else {
            info.srcWidth = origin_w;
            info.srcHeight = origin_h;
            info.yPitch = align16_w;
	    }
        format = FB_VMODE_RGB565;
        degree = value;
    }
    // update pitch in terms of bytes
    if( format == FB_VMODE_RGB565 ) {
        info.yPitch *= 2;
    }

    struct _sOvlySurface *pOvlySurf = &(gOverlayObj[index]->ovly_surf_cfg);
    pOvlySurf->viewPortInfo.srcWidth = info.srcWidth;
    pOvlySurf->viewPortInfo.srcHeight = info.srcHeight;
    pOvlySurf->viewPortInfo.yPitch = info.yPitch;
    pOvlySurf->viewPortInfo.uPitch = info.uPitch;
    pOvlySurf->viewPortInfo.vPitch = info.vPitch;
    _SHVAR_ROTATE( (*pOvlySurf) ) = degree;
    pOvlySurf->videoMode = format;

    return 0;
}

int overlay_switch(int index, int value) {
    struct _sOvlySurface *pOvlySurf = &(gOverlayObj[index]->ovly_surf_cfg);
    switch(value) {
    case OVERLAY_ENABLE:
        LOGD("%s() case OVERLAY_SWITCH_ON", __FUNCTION__);
        _SHVAR_SwitchOn( (*pOvlySurf) ) = 1;
        break;
    case OVERLAY_DISABLE:
        LOGD("%s() case OVERLAY_SWITCH_Off", __FUNCTION__);
        _SHVAR_SwitchOn( (*pOvlySurf) ) = 0;
        break;
    default:
        LOGD("%s() case default", __FUNCTION__);
        return -1;
    }
    return 0;
}

static int overlay_setParameter(struct overlay_control_device_t *dev,
         overlay_t* overlay, int param, int value) {

    int result = 0;
    //LOGD("overlay_setParameter() entered.");
    int index = overlay_get_index(overlay);
    if( index < 0 ){
        LOGE("overlay_setParameter cannot find overlay = %p", overlay);
        LOGE("Available overlay = %p , %p", gOverlayObj[0], gOverlayObj[1]);
        return -EINVAL;
    }

    /* set this overlay's parameter (talk to the h/w) */
    switch (param) {
        case OVERLAY_ROTATION_DEG:
            LOGV("overlay_setParameter() OVERLAY_ROTATION_DEG");
            /* if only 90 rotations are supported, the call fails
             * for other values */
            result = -EINVAL;
            break;
        case OVERLAY_DITHER:
            LOGV("overlay_setParameter() OVERLAY_DITHER");
            result = -EINVAL;
            break;
        case OVERLAY_TRANSFORM:
            LOGD("overlay_setParameter() OVERLAY_TRANSFORM");
            // see OVERLAY_TRANSFORM_*
            // if output to HDMI, do not rotate
            if( index == 0 ) {
                result = overlay_gcu_rotate(index, value);
            }
            break;
        case OVERLAY_SWITCH:
            LOGD("overlay_setParameter() OVERLAY_SWITCH value = %d", value);
            if( overlay_switch(index, value) < 0 ) {
                result = -EINVAL;
            }
            break;
        default:
            LOGD("overlay_setParameter() default");
            result = -EINVAL;
            break;
    }
    //LOGD("overlay_setParameter param=%d, value = %d, result = %d", param, value, result);
    return result;
}

#if PLATFORM_SDK_VERSION >= 5
static  int overlay_stage(struct overlay_control_device_t *dev, overlay_t* overlay)
{
    // do nothing now, there is no comments about it in android overlay api definition
    LOGD("overlay_stage() entered.");
    return 0;
}
static int  overlay_commit(struct overlay_control_device_t *dev, overlay_t* overlay)
{
    LOGD("overlay_commit() entered.");
    int index = overlay_get_index(overlay);
    if( index < 0 ){
        LOGE("overlay_commit() cannot find overlay = %p", overlay);
        return -EINVAL;
    }

    if( gOverlayObj[index]->invalid_position ) {
        LOGD("%s() not do commit() because .invalid_position!=0", __FUNCTION__ );
        return -EINVAL;
    }

    int fd_ovly = gOverlayObj[index]->mHandle.ov_fd;

    struct _sOvlySurface *newOvlySurf = &( gOverlayObj[index]->ovly_surf_cfg) ;

    int rot_degree = _SHVAR_ROTATE( (*newOvlySurf) );
    LOGV("%s() write-to-drv: .rotation_degree=%d, .srcWidth=%d, .srcHeight=%d, .zoomXSize=%d, .zoomYSize=%d",
            __FUNCTION__, rot_degree,
            newOvlySurf->viewPortInfo.srcWidth, newOvlySurf->viewPortInfo.srcHeight,
            newOvlySurf->viewPortInfo.zoomXSize, newOvlySurf->viewPortInfo.zoomYSize);
    if( _writeOvlySurface( fd_ovly, newOvlySurf ) ) {
        return -EINVAL;
    }

    return 0;
}
#endif

static int overlay_control_close(struct hw_device_t *dev)
{
    struct overlay_control_context_t* ctx = (struct overlay_control_context_t*)dev;

    LOGD("overlay_control_close");

    overlay_destroyOverlay( (struct overlay_control_device_t *)dev, gOverlayObj[0]);
    overlay_destroyOverlay( (struct overlay_control_device_t *)dev, gOverlayObj[1]);

    if (ctx) {
        /* free all resources associated with this device here
         * in particular the overlay_handle_t, outstanding overlay_t, etc...
         */
        free(ctx);
    }
    return 0;
}

// ****************************************************************************
// Data module
// ****************************************************************************

struct overlay_data_handle {
    struct overlay_data_device_t *dev;
    overlay_object::handle_t *handle; // overlay-drv-device handle

    sp<PhysMemPool> memPool;
    sp<GCUAgent> gcuAgent;

    struct _sOvlySurface overlay_surface; // video overlay surface
    unsigned char* free_buff_addr[MAX_QUEUE_NUM][3]; /* To store buffer which could be released. */
    overlay_buffer_header_t buff_header[MAX_FRAME_BUFFERS];
    int avail_header_num;   // avail buff_header count
    int avail_offet;      // next avail buff_buffer

    int dma_on;
    int first_frame;
    // mapping with overlay_handle_t->flag
    int flag;
    // if need de-interlace
    int deinterlace;
};


static struct overlay_data_handle gDataHandle[2];;
static int count = 0;

// data functions forward declarations
static void _resetBufferHeader( struct overlay_buffer_header_t *bh );

void _physMemFree( void *user ) {
    PhysMem * physMem = (PhysMem*)user;
    if( physMem )
        physMem->release();
}

/* user will be deleted, do not touch it again */
void _physMemFree_TV( void *user ) {
    struct MemoryFreeData *data = (struct MemoryFreeData*)user;
    if( data->physMem != 0) {
        data->physMem->release();
    }
    if( data->free != 0 ) {
        data->free(data->user);
    }
    delete data;
}

float get_pixel_size(int format) {
    float size = 0;
//    LOGD("get_pixel_size : format = %d", format);
    switch(format) {
        case OVERLAY_FORMAT_RGBA_8888:
            break;
        case OVERLAY_FORMAT_RGB_565:
            size = 2;
            break;
        case OVERLAY_FORMAT_BGRA_8888:
            break;
#if PLATFORM_SDK_VERSION < 9
        case OVERLAY_FORMAT_YCbCr_422_SP:
            break;
        case OVERLAY_FORMAT_YCbCr_420_SP:
            size = 1.5;
            break;
#endif
        case OVERLAY_FORMAT_YCbYCr_422_I:
            size = 2;
            break;
#if PLATFORM_SDK_VERSION < 9
        case OVERLAY_FORMAT_YCbYCr_420_I:
            break;
#endif
        case OVERLAY_FORMAT_YCbCr_420_P:
            size = 1.5;
            break;
        default:
            break;
    }
    return size;
}

int get_rotate_degree( int fd ) {
    int ret = 0;
    struct _sOvlySurface drvOvlySurf;
    if( _readOvlySurface( fd, &drvOvlySurf ) ) {
        ret = 0;
    }else {
        ret = _SHVAR_ROTATE( drvOvlySurf );
    }
    LOGV("get_rotate_degree() ret=%d", ret );
    return ret;
}

int overlay_data_init( struct overlay_data_handle *data_handle ) {
    int i;
    data_handle->avail_header_num = MAX_FRAME_BUFFERS;
    data_handle->avail_offet = 0;
    if(ioctl(data_handle->handle->ov_fd, FB_IOCTL_GET_VIEWPORT_INFO, &(data_handle->overlay_surface.viewPortInfo))) {
        LOGE("Error: get view port info fail\n");
        return -1;
    }
    if(ioctl(data_handle->handle->ov_fd, FB_IOCTL_GET_VID_OFFSET, &(data_handle->overlay_surface.viewPortOffset))) {
        LOGE("Error: get view port offset fail\n");
        return -1;
    }

    data_handle->overlay_surface.videoMode = data_handle->handle->drv_format;
    data_handle->deinterlace = 0;
    data_handle->first_frame = 1;
    for( int i = 0; i < MAX_FRAME_BUFFERS; i++ ) {
        _resetBufferHeader( &data_handle->buff_header[i] );
    }
    // init PhysMemPool&GCUAgent
    data_handle->memPool = new PhysMemPool;
    data_handle->memPool->init();
    data_handle->gcuAgent = new GCUAgent( data_handle->handle->ov_fd,
                                          data_handle->memPool.get() );
    data_handle->gcuAgent->init();

    return 0;
}

GCU_FORMAT get_gcu_format(int format) {
    GCU_FORMAT ret = GCU_FORMAT_UNKNOW;
    switch(format) {
        case OVERLAY_FORMAT_RGBA_8888:
            break;
        case OVERLAY_FORMAT_RGB_565:
            ret = GCU_FORMAT_RGB565;
            break;
        case OVERLAY_FORMAT_BGRA_8888:
            break;
#if PLATFORM_SDK_VERSION < 9
        case OVERLAY_FORMAT_YCbCr_422_SP:
            break;
        case OVERLAY_FORMAT_YCbCr_420_SP:
            ret = GCU_FORMAT_YV12;
            break;
#endif
        case OVERLAY_FORMAT_YCbYCr_422_I:
            ret = GCU_FORMAT_UYVY;
            break;
#if PLATFORM_SDK_VERSION < 9
        case OVERLAY_FORMAT_YCbYCr_420_I:
            break;
#endif
        case OVERLAY_FORMAT_YCbCr_420_P:
            ret = GCU_FORMAT_YV12;
            break;
        default:
            break;
    }
    return ret;
}

int overlay_initialize(struct overlay_data_device_t *dev,
        overlay_handle_t handle)
{
    /*
     * overlay_handle_t should contain all the information to "inflate" this
     * overlay. Typically it'll have a file descriptor, informations about
     * how many buffers are there, etc...
     * It is also the place to mmap all buffers associated with this overlay
     * (see getBufferAddress).
     *
     * NOTE: this function doesn't take ownership of overlay_handle_t
     *
     */
    int index = 0;
    if( gDataHandle[0].dev == NULL ) {
        index = 0;
    } else if( gDataHandle[1].dev == NULL ){
        index = 1;
    } else {
        LOGE("overlay_initialize More than 2 overlay created, no supporting...................");
        return -EINVAL;
    }
    LOGD("overlay_initialize ----------------- dev = %p , index = %d", dev, index);

    // initialize data ctx
    memset((void*)&( gDataHandle[index]), 0 , sizeof(gDataHandle[index]));
    gDataHandle[index].dev = dev;
    gDataHandle[index].handle = (overlay_object::handle_t*)handle;
    if( overlay_data_init(&(gDataHandle[index])) < 0 ) {
        LOGE("overlay_data_init failed");
        return -EINVAL;
    }
    // turn off it dma during init
    int on=0;
    if(ioctl(gDataHandle[index].handle->ov_fd, FB_IOCTL_SWITCH_VID_OVLY, &on)) {
            LOGE("Error: switch off video overlay fail\n" );
            return -EINVAL;
    }

    count = 0;//retry count

    struct overlay_data_context_t *context = (struct overlay_data_context_t*)dev;
    context->data_handle_index = index;
    LOGD("overlay_initialize success");
    return 0;
}

void release_buffer_header(int index) {
    if( gDataHandle[index].avail_header_num >= MAX_FRAME_BUFFERS ) {
        LOGE("release_buffer_header with avail_header_num already equal to MAX_FRAME_BUFFERS[%d], something wrong", MAX_FRAME_BUFFERS);
        return;
    }
    gDataHandle[index].avail_header_num++;
}

overlay_buffer_t get_buffer_header(int index) {
    overlay_buffer_t buffer = NULL;
    int offset = gDataHandle[index].avail_offet;
    if( gDataHandle[index].avail_header_num == 0 ) {
        return NULL;
    }
    gDataHandle[index].avail_header_num--;
    buffer = (overlay_buffer_t)(&gDataHandle[index].buff_header[offset]);
    _resetBufferHeader( (struct overlay_buffer_header_t*)buffer );
    offset++;
    offset = offset % MAX_FRAME_BUFFERS;
    gDataHandle[index].avail_offet = offset;
    return buffer;
}

// client want to memcpy virt mem data
void *overlay_getBufferAddress(struct overlay_data_device_t *dev,
        overlay_buffer_t buffer)
{
    int index = 0;
    if( gDataHandle[0].dev == dev ) {
        index = 0;
    } else if( gDataHandle[1].dev == dev ) {
        index = 1;
    } else {
        LOGE("overlay_getBufferAddress with invalid dev = %p", dev);
        return NULL;
    }

    struct overlay_buffer_header_t *buf = (struct overlay_buffer_header_t*)buffer;
    if( buf->paddr==NULL || buf->vaddr==NULL ) {
        // alloc phys mem
        PhysMem *physMem = gDataHandle[index].memPool->alloc( 1920*1080*2 );
        if( physMem == NULL ) {
            LOGE("overlay_getBufferAddress() failed alloc pmem");
            return NULL;
        }
        buf->paddr = physMem->paddr;
        buf->vaddr = physMem->vaddr;
        buf->user = (void*)physMem;
        buf->free = _physMemFree;
    }

    return buf->vaddr;
}

// reset buffer header fileds to default values
static void _resetBufferHeader( struct overlay_buffer_header_t *bh ) {
    bh->paddr = NULL;
    bh->vaddr = NULL;
    bh->len = 0;
    bh->flag = 1; //virtual mem
    bh->user = NULL;
    bh->free = NULL;
    bh->reserved = NULL;
    bh->x_off = 0;
    bh->y_off = 0;
    bh->stride_x = 0;
    bh->stride_y = 0;
    bh->deinterlace = 0;
    bh->step0 = 0;
    bh->step1 = 0;
    bh->step2 = 0;
    bh->width = 0;
    bh->height = 0;
    bh->fmt = -1;
}

static int freeCodecBuffer(int index)
{
    /* Prepare for released the decoding buffer. */
    memset( gDataHandle[index].free_buff_addr, 0, 3*MAX_QUEUE_NUM*sizeof(unsigned char*) );     /* Clear buffer. */

    /* 1. Get buffer list which could be released. */
    if ( ioctl(gDataHandle[index].handle->ov_fd, FB_IOCTL_GET_FREELIST, &(gDataHandle[index].free_buff_addr)) )
    {
        LOGE("Can't FB_IOCTL_GET_FREELIST\n");
        return -1;
    }

    int deinterlace = gDataHandle[index].deinterlace;
    /* 2. Release free buffer list */
    for( int i=0 ; i < MAX_QUEUE_NUM; i++ )
    {
        if( 0 == gDataHandle[index].free_buff_addr[i][0] )
            continue;
        LOGV("freeCodecBuffer found one collected from drv.");
        for(int j=0;j<MAX_FRAME_BUFFERS;j++)
        {
        //LOGV("gDataHandle[index].buff_header[j].paddr = 0x%x", (long)gDataHandle[index].buff_header[j].paddr);
            if( gDataHandle[index].buff_header[j].paddr == gDataHandle[index].free_buff_addr[i][0] ) {
                if( gDataHandle[index].buff_header[j].free != NULL ) {
                    gDataHandle[index].buff_header[j].free(gDataHandle[index].buff_header[j].user);
                }
                _resetBufferHeader( &gDataHandle[index].buff_header[j] );
                release_buffer_header(index);
                break;
            }
        }
    }
    return 0;
}


/* To support both fast overlay and normal overlay sink. I made the following design:
If upper layer does not know fast overlay, it just calls normal like this:
    overlay_dequeueBuffer(buf);
    ptr = overlay_getBufferAddress(buf);
    memcpy(ptr, src, len);
    overlay_enqueueBuffer(buf);
If upper layer knows fast overlay, it should call like this:
    overlay_dequeueBuffer(buf);
    struct overlay_buffer_context_t* buffer = (struct overlay_buffer_context_t*)buf;
    buffer->ptr = ...;
    buffer->flag = 2;
    overlay_enqueueBuffer(buf);
These two calling sequences are delivering implict messages to our overlay sink.

If upper layer what to flush overlay to collect all buffer pushed to lcd driver, do as following:
    overlay_dequeueBuffer(buf);
    struct overlay_buffer_context_t* buffer = (struct overlay_buffer_context_t*)buf;
    buffer->flag = 3;
    overlay_enqueueBuffer(buf);
*/
int overlay_dequeueBuffer(struct overlay_data_device_t *dev,
			  overlay_buffer_t* buf)
{
    /* blocks until a buffer is available and return an opaque structure
     * representing this buffer.
     */
    struct overlay_data_context_t *context = (struct overlay_data_context_t*)dev;
    int block = 1;
    int retrycount = 10;
    int index = 0;
    if( gDataHandle[0].dev == dev ) {
        index = 0;
    } else if( gDataHandle[1].dev == dev ) {
        index = 1;
    } else {
        LOGE("overlay_dequeueBuffer with invalid dev = %p", dev);
        return -EINVAL;
    }
    do {
        freeCodecBuffer(index);
        if( gDataHandle[index].avail_header_num == 0 ) {
            LOGD("overlay_dequeueBuffer : no avail buffer header, remaining retry count = %d", retrycount);
            if( retrycount-- < 0 ) break;
            // sleep 10ms before get free list again
            usleep(10 * 1000);
            continue;
        }
        // Ok, there is an avail buffer header
        overlay_buffer_t tmp = get_buffer_header(index);
        if( tmp == NULL ) {
            LOGE("get_free_buffer_header return NULL, we should not be here, something must be wrong.");
        } else {
            _resetBufferHeader( (struct overlay_buffer_header_t *)tmp );
            *buf = tmp;
            block = 0;
        }
    } while( block );
    if( block ) {
        *buf = NULL;
    }
    return 0;
}

/**
 * get overlay driver's format from overlay-hal format
 */
static int _getDrvFmt( int ovlyFmt ) {
    int drvFmt = FB_VMODE_YUV422PACKED;
    switch( ovlyFmt ) {
        case OVERLAY_FORMAT_RGBA_8888:
            drvFmt = FB_VMODE_RGBA888;
            break;
        case OVERLAY_FORMAT_RGB_565:
            drvFmt = FB_VMODE_RGB565;
            break;
        case OVERLAY_FORMAT_BGRA_8888:
            drvFmt = FB_VMODE_BGRA888;
            break;
        case OVERLAY_FORMAT_YCbYCr_422_I:
            drvFmt = FB_VMODE_YUV422PACKED;
            break;
        case OVERLAY_FORMAT_YCbCr_420_SP:
        case OVERLAY_FORMAT_YCbCr_420_P:
            drvFmt = FB_VMODE_YUV420PLANAR;
            break;
        default:
            break;
    }

    return drvFmt;
}

bool setBufferAddr(VideoBuffer* vb, struct _sViewPortInfo *info, struct _sVideoBufferAddr *addr){
    // data
    unsigned int XxY=0;
    unsigned char *yData = NULL;
    unsigned char *uData = NULL;
    unsigned char *vData = NULL;
    unsigned int yPitch = 0;
    unsigned int uPitch = 0;
    unsigned int vPitch = 0;
    switch( vb->ovlyFmt ) {
        case OVERLAY_FORMAT_YCbCr_420_SP:
            LOGE("%s:%d. not support flipping 420_SP to drv!", __FUNCTION__, __LINE__);
            return false;
            break;
        case OVERLAY_FORMAT_YCbCr_420_P:
            addr->inputData = 0;
            addr->length = vb->len;

            XxY = vb->stride_x * vb->stride_y;
            yData = (unsigned char*)vb->paddr;
            uData = yData + XxY;
            vData = uData + XxY/4;
            yPitch = vb->stride_x;
            uPitch = yPitch/2;
            vPitch = yPitch/2;
            addr->startAddr[0] = yData;
            addr->startAddr[1] = uData;
            addr->startAddr[2] = vData;
            // TODO: we should move tv path overlay param calculation to 
            // overlay hal
            if( info != NULL ) {
                info->yPitch = yPitch;
                info->uPitch = uPitch;
                info->vPitch = vPitch;
            }
            break;
        case OVERLAY_FORMAT_YCbYCr_422_I:
            addr->inputData = 0;
            addr->startAddr[0] = (unsigned char*)vb->paddr;
            addr->length = vb->len;
            if( info != NULL ) {
                info->yPitch = vb->stride_x * 2;
            }
            break;
        case OVERLAY_FORMAT_RGB_565:
            addr->inputData = 0;
            addr->startAddr[0] = (unsigned char*)vb->paddr;
            addr->length = vb->len;
            if( info != NULL ) {
                info->yPitch = vb->stride_x * 2;
            }
            break;
        default:
            addr->inputData = 0;
            addr->startAddr[0] = (unsigned char*)vb->paddr;
            addr->length = vb->len;
            if( info != NULL ) {
                info->yPitch = vb->stride_x * 2;
            }
            break;
    }
    return true;
}

/**
 * do flipping, vb : video buffer for panel, hb : video buffer for hdmi
 */
bool doFlip( int fd, VideoBuffer *vb, VideoBuffer *hb ) {
    LOGV("%s#%d: flipping. .paddr=%p, .vaddr=%p, .len=%d, .fmt=%d, .w=%d, .h=%d",
          __FUNCTION__, __LINE__,
          vb->paddr, vb->vaddr, vb->len, vb->ovlyFmt,
          vb->width, vb->height );
    // get realtime View setting from ctrl device
    struct _sOvlySurface drvOvlySurfBak;
    if( _readOvlySurface( fd, &drvOvlySurfBak ) ) {
        LOGE("%s() failed on _readOvlySurface().", __FUNCTION__);
        return false;
    }

    struct _sOvlySurface curOvlySurface;
    memset( &curOvlySurface, 0 , sizeof(curOvlySurface) );
    // use realtime(effective) View setting, such as, viewPortInfo, viewPortOffset
    memcpy( &curOvlySurface.viewPortInfo,
            &drvOvlySurfBak.viewPortInfo, sizeof( drvOvlySurfBak.viewPortInfo ) );
    memcpy( &curOvlySurface.viewPortOffset,
            &drvOvlySurfBak.viewPortOffset, sizeof( drvOvlySurfBak.viewPortOffset ) );
    // use new width/height of valid resolution
    curOvlySurface.viewPortInfo.srcWidth = vb->width;
    curOvlySurface.viewPortInfo.srcHeight = vb->height;
    // video mode
    curOvlySurface.videoMode = _getDrvFmt( vb->ovlyFmt );
    if( hb != NULL ) {
        curOvlySurface.dualInfo.videoMode = _getDrvFmt(hb->ovlyFmt);
        curOvlySurface.dualInfo.rotate = 1;
        curOvlySurface.dualInfo.srcWidth = hb->width;
        curOvlySurface.dualInfo.srcHeight = hb->height;
    } else {
        curOvlySurface.dualInfo.rotate = 0;
    }

    bool success = setBufferAddr(vb, (struct _sViewPortInfo *)&(curOvlySurface.viewPortInfo), 
        (struct _sVideoBufferAddr *) &(curOvlySurface.videoBufferAddr));
    if( !success ) {
        return false;
    }
    // dump buffer(it will check if needs dump internally)
    dumpBuf( vb->ovlyFmt, vb->vaddr, vb->len, vb->width, vb->height );

    if( hb != NULL ) {        
        success = setBufferAddr(hb, NULL, 
                (struct _sVideoBufferAddr *) &(curOvlySurface.dualInfo.videoBufferAddr));
        if( !success ) {
            return false;
        }
    }
    LOGV("FLIP: .videoMode=%d, .srcWidth=%d, .srcHeight=%d, .xOffset=%d, .yOffset=%d, .zoomXSize=%d, .zoomYSize=%d",
                curOvlySurface.videoMode,
                curOvlySurface.viewPortInfo.srcWidth,
                curOvlySurface.viewPortInfo.srcHeight,
                curOvlySurface.viewPortOffset.xOffset,
                curOvlySurface.viewPortOffset.yOffset,
                curOvlySurface.viewPortInfo.zoomXSize,
                curOvlySurface.viewPortInfo.zoomYSize);


    if( ioctl( fd, FB_IOCTL_FLIP_VID_BUFFER, &curOvlySurface) ) {
        LOGE("Can't FB_IOCTL_FLIP_VID_BUFFER\n");
        return false;
    }

    return true;
}

bool is_fullscreen_display(struct overlay_data_handle *data_handle, struct _sOvlySurface *surface) {
    if( data_handle->handle->screen_width == surface->viewPortInfo.zoomXSize ||
        data_handle->handle->screen_height == surface->viewPortInfo.zoomYSize ) {
        return true;
    } else {
        return false;
    }
}

int overlay_queueBuffer(struct overlay_data_device_t *dev,
        overlay_buffer_t buffer)
{
    /* Mark this buffer for posting and recycle or free overlay_buffer_t. */
    struct overlay_buffer_header_t *buf = (struct overlay_buffer_header_t*)buffer;

    int index = 0;
    if( gDataHandle[0].dev == dev ) {
        index = 0;
    } else if (gDataHandle[1].dev == dev ){
        index = 1;
    } else {
        LOGE("cannot find correct index---------------");
	    return -EINVAL;
    }
    // wait for VSYNC
    if (buf->flag == 5) {
        if ( ioctl(gDataHandle[index].handle->ov_fd, FB_IOCTL_WAIT_VSYNC, NULL) ) {
            LOGE("!!!! FB_IOCTL_WAIT_VSYNC wrong");
        }
        return 0;
    }
    // client wants to poll/trigger any completion callback
    if( buf->flag == 4 ) {
        freeCodecBuffer(index);
        return 0;
    }
    // client request to flush overlay, return all user pointer buffer to client
    if( buf->flag == 3 ) {
        if( gDataHandle[index].dma_on == 1 ) {
            int on = 0;
            if(ioctl(gDataHandle[index].handle->ov_fd, FB_IOCTL_SWITCH_VID_OVLY, &on)) {
                LOGE("Error: switch off video overlay fail\n" );
                return -EINVAL;
            }
            gDataHandle[index].dma_on = 0;
        }
        // free buffer
        freeCodecBuffer(index);
        return 0;
    }

    LOGV("buf->deinterlace=%d\n", buf->deinterlace );
    if( gDataHandle[index].first_frame && buf->deinterlace ) {
        gDataHandle[index].deinterlace = 1;
    }

    int fd = gDataHandle[index].handle->ov_fd;

    // read current params saved in drv( most likely be updated by ctrl-dev )
    struct _sOvlySurface drvOvlySurfBak;
    if( _readOvlySurface( fd, &drvOvlySurfBak ) ) {
        LOGE("%s() failed on _readOvlySurface().", __FUNCTION__);
        return 0;
    }

    int rotate_degree = get_rotate_degree(gDataHandle[index].handle->ov_fd);
    LOGV("overlay_queueBuffer, rotate_degree = %d, flag = %d", rotate_degree, buf->flag);
    gDataHandle[index].flag = buf->flag;

    char value[PROPERTY_VALUE_MAX];
    property_get( "service.fb_mirror", value, "0");
    bool screen_rotated = !strcmp(value, "1");
    bool tv_need_rotate = false;
    if( screen_rotated && (rotate_degree == 0) ) {
        tv_need_rotate = true;
    }
    
    /**
     * do buffer processing, i.e. rotateion/deinterlace/color-conversion...
     */
    int originFmt   = gDataHandle[index].handle->format;
    int originWidth = gDataHandle[index].handle->step;
    int originHeight= gDataHandle[index].handle->height;
    GCURequest gcuReq;
    gcuReq.bh = buf;
    gcuReq.vb.paddr = buf->paddr;
    gcuReq.vb.vaddr = buf->vaddr;
    // color format may change in each new buffer
    gcuReq.vb.ovlyFmt = buf->fmt==-1 ? originFmt : buf->fmt;
    gcuReq.vb.len = buf->len;
    gcuReq.vb.x_off = buf->x_off;
    gcuReq.vb.y_off = buf->y_off;
    gcuReq.vb.width = buf->width==0 ? originWidth : buf->width;
    gcuReq.vb.height = buf->height==0 ? originHeight : buf->height;
    gcuReq.vb.stride_x = buf->stride_x;
    gcuReq.vb.stride_y = buf->stride_y;
    gcuReq.vb.y_step = buf->step0;
    gcuReq.vb.u_step = buf->step1;
    gcuReq.vb.v_step = buf->step2;
    if( tv_need_rotate == true ) {
        gcuReq.addAct(GCU_DO_ROTATE);
        gcuReq.rot_degree = OVERLAY_TRANSFORM_ROT_90;
        gcuReq.addFlag(GCU_FLAG_ROTATE_IN_TV);
    }
    if( rotate_degree != 0 ) {
        // need rotate
        gcuReq.addAct( GCU_DO_ROTATE );
        gcuReq.rot_degree = rotate_degree;
    }
    if( buf->deinterlace != 0 ) {
        // need deinterlace
        gcuReq.addAct( GCU_DO_DEINTERLACE );
    }
    if( gcuReq.vb.ovlyFmt == OVERLAY_FORMAT_YCbCr_420_SP ) {
        // need 420SP --> 420P
        gcuReq.addAct( GCU_DO_COLORCONVERT );
        gcuReq.new_fmt = OVERLAY_FORMAT_YCbCr_420_P;
    }
    if( is_fullscreen_display(&(gDataHandle[index]), &drvOvlySurfBak) && (rotate_degree == 0)) {
        gcuReq.addFlag(GCU_FLAG_FULLSCREEN);
    }
    LOGV("before calling addRequest()");
    gDataHandle[index].gcuAgent->addRequest( &gcuReq );
    LOGV("after called addRequest()");

    // sync device on/off with ctrl channel
    int ctl_on = _SHVAR_SwitchOn( drvOvlySurfBak );
    if( gDataHandle[index].dma_on != ctl_on ) {
        if(ioctl(gDataHandle[index].handle->ov_fd, FB_IOCTL_SWITCH_VID_OVLY, &ctl_on)) {
            LOGE("Error: switch video overlay fail\n" );
        } else {
            gDataHandle[index].dma_on = ctl_on;
        }
    }
    return 0;
}

#if PLATFORM_SDK_VERSION >= 5
    /* can be called to change the width and height of the overlay. */
int overlay_resizeInput(struct overlay_data_device_t *dev,
            uint32_t w, uint32_t h) {
    return -EINVAL;
}

int overlay_setCrop(struct overlay_data_device_t *dev,
            uint32_t x, uint32_t y, uint32_t w, uint32_t h){
    return -EINVAL;
}

int overlay_getCrop(struct overlay_data_device_t *dev,
       uint32_t* x, uint32_t* y, uint32_t* w, uint32_t* h){
    return -EINVAL;
}

int overlay_setParameter_from_data(struct overlay_data_device_t *dev,
            int param, int value){
    return -EINVAL;
}
#endif

static int overlay_data_close(struct hw_device_t *dev)
{
    LOGD("overlay_data_close ----------------- dev = %p",dev);
    LOGE("gDataHandle[0].dev = %p ; gDataHandle[1].dev = %p", gDataHandle[0].dev, gDataHandle[1].dev);

    struct overlay_data_context_t* ctx = (struct overlay_data_context_t*)dev;
    int index = 0;
    if( gDataHandle[0].dev == (struct overlay_data_device_t*)dev ) {
         index = 0;
    } else if ( gDataHandle[1].dev == (struct overlay_data_device_t*)dev ) {
         index = 1;
    } else {
        LOGE("Cannot get the index when overlay_data_close----------------------- dev = %p", dev);
	 return -EINVAL;
    }
    if (ctx) {
#if 1
		int on = 0;
		if(ioctl(gDataHandle[index].handle->ov_fd, FB_IOCTL_SWITCH_VID_OVLY, &on)) {
			LOGE("Error: switch off video overlay fail\n" );
		}
		gDataHandle[index].dma_on = 0;
		/* Sometimes there are still remained buffers in overlay driver, since we have close the overlay,
		   so we must free these remained buffer here. */
		for(int j=0;j<MAX_FRAME_BUFFERS;j++)
		{
			if( gDataHandle[index].buff_header[j].free != NULL ) {
				gDataHandle[index].buff_header[j].free(gDataHandle[index].buff_header[j].user);
			}
			else {
				continue;
			}
			_resetBufferHeader( &gDataHandle[index].buff_header[j] );

			count--;
		}

#endif
        /* free all resources associated with this device here
         * in particular all pending overlay_buffer_t if needed.
         *
         * NOTE: overlay_handle_t passed in initialize() is NOT freed and
         * its file descriptors are not closed (this is the responsibility
         * of the caller).
         */
        if( gDataHandle[index].gcuAgent.get() != NULL ) {
            gDataHandle[index].gcuAgent->deinit();
            gDataHandle[index].gcuAgent.clear();
            gDataHandle[index].gcuAgent = NULL;
        }
        if( gDataHandle[index].memPool.get() != NULL ) {
            gDataHandle[index].memPool->deinit();
            gDataHandle[index].memPool.clear();
            gDataHandle[index].memPool = NULL;
        }
#if 0
        int on = 0;
        if(ioctl(gDataHandle[index].handle->ov_fd, FB_IOCTL_SWITCH_VID_OVLY, &on)) {
            LOGE("Error: switch off video overlay fail\n" );
        }
        gDataHandle[index].dma_on = 0;
        /* Sometimes there are still remained buffers in overlay driver, since we have close the overlay,
           so we must free these remained buffer here. */
        for(int j=0;j<MAX_FRAME_BUFFERS;j++)
        {
            if( gDataHandle[index].buff_header[j].free != NULL ) {
                gDataHandle[index].buff_header[j].free(gDataHandle[index].buff_header[j].user);
            }
            else {
                continue;
            }
            _resetBufferHeader( &gDataHandle[index].buff_header[j] );

            count--;
        }
#endif
        //close( gDataHandle[index].handle->ov_fd);
        gDataHandle[index].dev = NULL;
        free(ctx);
    }
    LOGD("*****************Final count is %d*********************", count);
    return 0;
}

/*****************************************************************************/

static int overlay_device_open(const struct hw_module_t* module, const char* name,
        struct hw_device_t** device)
{
    int status = -EINVAL;
    LOGD("overlay_device_open %s\n", name);
    if (!strcmp(name, OVERLAY_HARDWARE_CONTROL)) {
        struct overlay_control_context_t *dev;
        dev = (overlay_control_context_t*)malloc(sizeof(*dev));

        /* initialize our state here */
        memset(dev, 0, sizeof(*dev));

        /* initialize the procs */
        dev->device.common.tag = HARDWARE_DEVICE_TAG;
        dev->device.common.version = 0;
        dev->device.common.module = const_cast<hw_module_t*>(module);
        dev->device.common.close = overlay_control_close;

        dev->device.get = overlay_get;
        dev->device.createOverlay = overlay_createOverlay;
        dev->device.destroyOverlay = overlay_destroyOverlay;
        dev->device.setPosition = overlay_setPosition;
        dev->device.getPosition = overlay_getPosition;
        dev->device.setParameter = overlay_setParameter;
#if PLATFORM_SDK_VERSION >= 5
        dev->device.stage = overlay_stage;
        dev->device.commit = overlay_commit;
#endif
        *device = &dev->device.common;
        status = 0;
    } else if (!strcmp(name, OVERLAY_HARDWARE_DATA)) {
        struct overlay_data_context_t *dev;
        dev = (overlay_data_context_t*)malloc(sizeof(*dev));

        /* initialize our state here */
        memset(dev, 0, sizeof(*dev));

        /* initialize the procs */
        dev->device.common.tag = HARDWARE_DEVICE_TAG;
        dev->device.common.version = 0;
        dev->device.common.module = const_cast<hw_module_t*>(module);
        dev->device.common.close = overlay_data_close;

        dev->device.initialize = overlay_initialize;
        dev->device.dequeueBuffer = overlay_dequeueBuffer;
        dev->device.queueBuffer = overlay_queueBuffer;
        dev->device.getBufferAddress = overlay_getBufferAddress;
#if PLATFORM_SDK_VERSION >= 5
        dev->device.resizeInput = overlay_resizeInput;
        dev->device.setCrop = overlay_setCrop;
        dev->device.getCrop = overlay_getCrop;
        dev->device.setParameter = overlay_setParameter_from_data;
#endif
        *device = &dev->device.common;
        status = 0;
    }
    return status;
}

