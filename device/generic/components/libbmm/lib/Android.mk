LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

ifeq ($(BOARD_USE_PMEM_TO_SIMULATE_BMM), true)
LOCAL_SRC_FILES := \
	bmm_lib.pmem.c
else
LOCAL_SRC_FILES :=  \
	bmm_lib.c
endif

LOCAL_MODULE := libbmm
LOCAL_MODULE_TAGS := debug eng
LOCAL_SHARED_LIBRARIES := libutils

LOCAL_PRELINK_MODULE := false

include $(BUILD_SHARED_LIBRARY)

COMPILE_BMM_TEST := true

ifeq ($(COMPILE_BMM_TEST), true)

include $(CLEAR_VARS)

LOCAL_SRC_FILES :=  \
        bmm_test.c

LOCAL_MODULE := bmmtest
LOCAL_MODULE_TAGS := debug eng
LOCAL_SHARED_LIBRARIES += libbmm

include $(BUILD_EXECUTABLE)

endif
