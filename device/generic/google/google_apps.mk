
GOOGLE_TOP:=device/generic/google
#
# 3rd party packages 
# 

PRODUCT_COPY_FILES += \
	$(GOOGLE_TOP)/framework/com.google.android.maps.jar:system/framework/com.google.android.maps.jar \
	$(GOOGLE_TOP)/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml \
	$(GOOGLE_TOP)/etc/permissions/features.xml:system/etc/permissions/features.xml \
	$(GOOGLE_TOP)/lib/libspeech.so:system/lib/libspeech.so \
	$(GOOGLE_TOP)/lib/libvoicesearch.so:system/lib/libvoicesearch.so 

#google apps commons
PRODUCT_COPY_FILES += \
	$(GOOGLE_TOP)/app/GooglePartnerSetup.apk:system/app/GooglePartnerSetup.apk \
	$(GOOGLE_TOP)/app/GoogleServicesFramework.apk:system/app/GoogleServicesFramework.apk \
	$(GOOGLE_TOP)/app/GoogleBackupTransport.apk:system/app/GoogleBackupTransport.apk \
	$(GOOGLE_TOP)/app/GoogleCalendarSyncAdapter.apk:system/app/GoogleCalendarSyncAdapter.apk \
	$(GOOGLE_TOP)/app/GoogleContactsSyncAdapter.apk:system/app/GoogleContactsSyncAdapter.apk \
	$(GOOGLE_TOP)/app/GoogleFeedback.apk:system/app/GoogleFeedback.apk \
	$(GOOGLE_TOP)/app/MarketUpdater.apk:system/app/MarketUpdater.apk \
	$(GOOGLE_TOP)/app/NetworkLocation.apk:system/app/NetworkLocation.apk \
	$(GOOGLE_TOP)/app/MediaUploader.apk:system/app/MediaUploader.apk 


#Google search box is duplicated to android intenal quick search box application	
#	$(GOOGLE_TOP)/app/GoogleQuickSearchBox.apk:system/app/GoogleQuickSearchBox.apk \

#	$(GOOGLE_TOP)/app/SetupWizard.apk:system/app/SetupWizard.apk \

#???
PRODUCT_COPY_FILES += \
	$(GOOGLE_TOP)/app/kickback.apk:system/app/kickback.apk \
	$(GOOGLE_TOP)/app/OneTimeInitializer.apk:system/app/OneTimeInitializer.apk 
#	$(GOOGLE_TOP)/app/CarHomeGoogle.apk:system/app/CarHomeGoogle.apk \
#	$(GOOGLE_TOP)/app/CarHomeLauncher.apk:system/app/CarHomeLauncher.apk \

#   not used in system tutorial
#	$(GOOGLE_TOP)/app/LatinImeTutorial.apk:system/app/LatinImeTutorial.apk \

#weather widget	
PRODUCT_COPY_FILES += \
	$(GOOGLE_TOP)/app/GenieWidget.apk:system/app/GenieWidget.apk \

#Social network
#PRODUCT_COPY_FILES += \
#	$(GOOGLE_TOP)/app/Facebook.apk:system/app/Facebook.apk \
#	$(GOOGLE_TOP)/app/Twitter.apk:system/app/Twitter.apk 


#Google Voice
PRODUCT_COPY_FILES += \
	$(GOOGLE_TOP)/app/VoiceSearch.apk:system/app/VoiceSearch.apk

#	$(GOOGLE_TOP)/app/googlevoice.apk:system/app/googlevoice.apk \

#Google Market
PRODUCT_COPY_FILES += \
	$(GOOGLE_TOP)/app/Vending.apk:system/app/Vending.apk 

#Google Mail
PRODUCT_COPY_FILES += \
	$(GOOGLE_TOP)/app/Gmail.apk:system/app/Gmail.apk 

#Google Maps
PRODUCT_COPY_FILES += \
	$(GOOGLE_TOP)/app/Maps.apk:system/app/Maps.apk 

#Google Talk	
PRODUCT_COPY_FILES += \
	$(GOOGLE_TOP)/app/Talk.apk:system/app/Talk.apk \
	$(GOOGLE_TOP)/app/talkback.apk:system/app/talkback.apk 

#Google Youtube
PRODUCT_COPY_FILES += \
	$(GOOGLE_TOP)/app/YouTube.apk:system/app/YouTube.apk 

#Google Street	
PRODUCT_COPY_FILES += \
	$(GOOGLE_TOP)/app/Street.apk:system/app/Street.apk 

PRODUCT_PROPERTY_OVERRIDES += \
	ro.url.legal=http://www.google.com/intl/%s/mobile/android/basic/phone-legal.html \
	ro.url.legal.android_privacy=http://www.google.com/intl/%s/mobile/android/basic/privacy.html \
	ro.com.google.clientidbase=android-google \
	ro.setupwizard.mode=DISABLED \
	ro.com.google.networklocation=1
	
