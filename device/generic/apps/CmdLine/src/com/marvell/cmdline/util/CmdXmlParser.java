/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */


package com.marvell.cmdline.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.text.TextUtils;



public class CmdXmlParser
{

	private static final String TAG = "CmdXmlParser";

	private XmlPullParserFactory mXmlFactory = null;
	private XmlPullParser mXmlParser = null;

	private static final boolean mDebug = false;



	public CmdXmlParser()
	{
		try
		{
			mXmlFactory = XmlPullParserFactory.newInstance();
			mXmlFactory.setNamespaceAware(true);
			mXmlParser = mXmlFactory.newPullParser();
		}
		catch (XmlPullParserException e)
		{
			e.printStackTrace();
		}
	}



	public CmdFile parserXml(String path) throws XmlPullParserException, IOException
	{
		CmdFile cmdfile = null;
		String command = null;
		String state = null;
		String root = null;
		
		boolean mTagRoot = false;
		boolean mTagList = false;
		boolean mTagCommand = false;
		boolean mTagBackground = false;
		boolean mTagPrivilege = false;
		
		LogUtil.d(TAG, "Parse file: " + path);

		if (null == mXmlFactory || null == mXmlParser)
		{
			return cmdfile;
		}

		mXmlParser.setInput(new FileInputStream(path), "UTF-8");
		String TagName = null;

		int eventType = mXmlParser.getEventType();
		while (true)
		{
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				if (mDebug)
					LogUtil.d(TAG, "START_DOCUMENT");

				cmdfile = new CmdFile();
				break;

			case XmlPullParser.START_TAG:
				TagName = mXmlParser.getName();

				if (mDebug)
					LogUtil.d(TAG, "TagName: " + TagName);

				if (TagName.equals(CmdConst.TAG_ROOT))
				{
					mTagRoot = true;
					break;
				}

				if (TagName.equals(CmdConst.TAG_LIST))
				{
					mTagList = true;
					cmdfile.mCmdList = new ArrayList<String>();
					cmdfile.mCmdList.clear();
					break;
				}

				if (TagName.equals(CmdConst.TAG_COMMAND))
				{
					mTagCommand = true;
					command = mXmlParser.getAttributeValue(null, CmdConst.ATTR_LINE);

					if (mDebug)
						LogUtil.d(TAG, "cmd: " + command);
					break;
				}

				if (TagName.equals(CmdConst.TAG_BACKGROUND))
				{
					mTagBackground = true;
					state = mXmlParser.getAttributeValue(null, CmdConst.ATTR_STATE);

					if (mDebug)
						LogUtil.d(TAG, "Background: " + state);
					break;
				}

				if (TagName.equals(CmdConst.TAG_PRIVILEGE))
				{
					mTagPrivilege = true;
					root = mXmlParser.getAttributeValue(null, CmdConst.ATTR_ROOT);

					if (mDebug)
						LogUtil.d(TAG, "Privilege: " + root);
					break;
				}

				break;

			case XmlPullParser.COMMENT:
				break;

			case XmlPullParser.TEXT:
				String TagContent = mXmlParser.getText().trim();

				if (mDebug)
					LogUtil.d(TAG, "TagContent: " + TagContent);
				

				if (TextUtils.isEmpty(TagContent))
				{
					break;
				}

				if (TagName.equals(CmdConst.TAG_COMMAND))
				{
					break;
				}

				if (TagName.equals(CmdConst.TAG_BACKGROUND))
				{
					break;
				}

				if (TagName.equals(CmdConst.TAG_PRIVILEGE))
				{
					break;
				}

				break;

			case XmlPullParser.END_TAG:
				TagName = mXmlParser.getName();

				if (mDebug)
					LogUtil.d(TAG, "TagName: " + TagName);			

				if (TagName.equals(CmdConst.TAG_ROOT))
				{
					break;
				}

				if (TagName.equals(CmdConst.TAG_LIST))
				{
					break;
				}

				if (TagName.equals(CmdConst.TAG_COMMAND) && !TextUtils.isEmpty(command) && 
						null != cmdfile.mCmdList)
				{
					cmdfile.mCmdList.add(command);						
					break;
				}

				if (TagName.equals(CmdConst.TAG_BACKGROUND) && !TextUtils.isEmpty(state))
				{
					if (state.equals(CmdConst.VALUE_YES))
						cmdfile.mBackground = true;
					else if (state.equals(CmdConst.VALUE_NO))
						cmdfile.mBackground = false;
					else if (mDebug)
						LogUtil.e(TAG, "Invalid background state value " + state);

					break;
				}

				if (TagName.equals(CmdConst.TAG_PRIVILEGE) && !TextUtils.isEmpty(root))
				{
					if (root.equals(CmdConst.VALUE_YES))
						cmdfile.mRoot = true;
					else if (root.equals(CmdConst.VALUE_NO))
						cmdfile.mRoot = false;
					else if (mDebug)
						LogUtil.e(TAG, "Invalid privilege root value " + state);

					break;
				}

				break;

			case XmlPullParser.END_DOCUMENT:
				if (mDebug)
					LogUtil.d(TAG, "END_DOCUMENT");
				
				
				if (! mTagRoot)
				{
					LogUtil.e(TAG, "Failed to find tag: " + CmdConst.TAG_ROOT);
					return null;
				}					

				if (! mTagList)
				{
					LogUtil.e(TAG, "Failed to find tag: " + CmdConst.TAG_LIST);
					return null;
				}					

				if (! mTagCommand)
				{
					LogUtil.e(TAG, "Failed to find tag: " + CmdConst.TAG_COMMAND);
					return null;
				}					

				if (! mTagBackground)
				{
					LogUtil.e(TAG, "Failed to find tag: " + CmdConst.TAG_BACKGROUND);
					return null;
				}

				if (! mTagPrivilege)
				{
					LogUtil.e(TAG, "Failed to find tag: " + CmdConst.TAG_PRIVILEGE);
					return null;
				}

				return cmdfile;
			}

			eventType = mXmlParser.next();
		}

	}
}
