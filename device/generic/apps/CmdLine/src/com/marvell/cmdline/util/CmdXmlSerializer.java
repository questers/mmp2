/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */


package com.marvell.cmdline.util;

import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;

import org.xmlpull.v1.XmlSerializer;

import android.util.Xml;



public class CmdXmlSerializer
{
	private XmlSerializer mXmlSerializer = null;
	private static final String TAG = "CmdXmlSerializer";



	public CmdXmlSerializer()
	{
		mXmlSerializer = Xml.newSerializer();
	}



	public void flush(CmdFile item)
	{
		String path = CmdConst.CMD_DIR + "/" + item.mName + CmdConst.SUFFIX_XML;
		try
		{
			File newxmlfile = new File(path);
			newxmlfile.createNewFile();
			FileOutputStream fileos = new FileOutputStream(newxmlfile);

			mXmlSerializer.setOutput(fileos, "UTF-8");
			mXmlSerializer.startDocument(null, null);

			mXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			mXmlSerializer.startTag(null, CmdConst.TAG_ROOT);

			mXmlSerializer.startTag(null, CmdConst.TAG_LIST);

			for (int i = 0; i < item.mCmdList.size(); i++)
			{
				String cmd = item.mCmdList.get(i);
				mXmlSerializer.startTag(null, CmdConst.TAG_COMMAND);
				mXmlSerializer.attribute(null, CmdConst.ATTR_LINE, cmd);
				mXmlSerializer.endTag(null, CmdConst.TAG_COMMAND);
			}

			mXmlSerializer.endTag(null, CmdConst.TAG_LIST);

			mXmlSerializer.startTag(null, CmdConst.TAG_BACKGROUND);
			if (item.mBackground)
				mXmlSerializer.attribute(null, CmdConst.ATTR_STATE, CmdConst.VALUE_YES);
			else
				mXmlSerializer.attribute(null, CmdConst.ATTR_STATE, CmdConst.VALUE_NO);

			mXmlSerializer.endTag(null, CmdConst.TAG_BACKGROUND);

			mXmlSerializer.startTag(null, CmdConst.TAG_PRIVILEGE);
			if (item.mRoot)
				mXmlSerializer.attribute(null, CmdConst.ATTR_ROOT, CmdConst.VALUE_YES);
			else
				mXmlSerializer.attribute(null, CmdConst.ATTR_ROOT, CmdConst.VALUE_NO);

			mXmlSerializer.endTag(null, CmdConst.TAG_PRIVILEGE);

			mXmlSerializer.endTag(null, CmdConst.TAG_ROOT);
			mXmlSerializer.endDocument();

			mXmlSerializer.flush();

			fileos.close();
		}
		catch (IOException e)
		{
			LogUtil.e(TAG, "Fail to save data to " + path);
			e.printStackTrace();
		}
	}
}
