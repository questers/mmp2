/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

package com.marvell.cmdline.util;

public class CmdConst
{
	public final static String CMD_DIR = "/data/data/com.marvell.cmdline/files";
	public final static String CMD_DIR_PRESET = "/system/etc/cmds";

	public static final String TAG_ROOT = "CmdFile";
	
	public static final String TAG_LIST = "List";
	public static final String TAG_COMMAND = "Command";
	public static final String ATTR_LINE	= "Line";
	
	public static final String TAG_BACKGROUND	= "Background";
	public static final String ATTR_STATE 		= "State";

	public static final String TAG_PRIVILEGE	= "Privilege";
	public static final String ATTR_ROOT 		= "Root";

	public static final String VALUE_YES 		= "yes";
	public static final String VALUE_NO 		= "no";

	public static final String SUFFIX_XML	=  ".xml";
	public static final String SUFFIX_LOG	=  ".log";
}
