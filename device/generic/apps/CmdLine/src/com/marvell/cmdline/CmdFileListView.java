/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

package com.marvell.cmdline;

import java.io.File;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;

import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;

import com.marvell.cmdline.service.CmdLineService;
import com.marvell.cmdline.service.Terminal;
import com.marvell.cmdline.util.CmdConst;
import com.marvell.cmdline.util.CmdFile;
import com.marvell.cmdline.util.CmdFileHolder;
import com.marvell.cmdline.util.LogUtil;
import com.marvell.cmdline.view.ViewResult;



public class CmdFileListView extends Activity implements View.OnCreateContextMenuListener, OnItemClickListener
{
	private static final String TAG = "CmdFileListView";

	private static final int MENU_ADD = 1;
	private static final int MENU_EDIT = 2;
	private static final int MENU_DELETE = 3;
	private static final int MENU_RUN = 4;

	public static boolean mActivityRun = false;

	private CmdFileHolder mCmdFileHolder = null;
	private ListView mListView = null;
	private CmdFileAdapter mCmdFileAdapter = null;



	public static Intent getIntent(Context context)
	{
		Intent intent = new Intent(context, CmdFileListView.class);
		intent.setAction(Intent.ACTION_VIEW);

		return intent;
	}



	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		if (!checkDirectory(CmdConst.CMD_DIR))
		{
			finish();
			return;
		}

		CmdLineService.cancelNotification(this);
		mCmdFileHolder = CmdFileHolder.getSingleInstance();
		mCmdFileAdapter = new CmdFileAdapter(this, R.layout.cmd_file_list_item, mCmdFileHolder.getFileList());

		mListView = (ListView) findViewById(R.id.file_list);
		mListView.setAdapter(mCmdFileAdapter);
		mListView.setOnItemClickListener(this);
		mListView.setOnCreateContextMenuListener(this);
		mListView.setEmptyView(findViewById(R.id.empty));

		IntentFilter filter = new IntentFilter();
		filter.addAction(CmdLineService.ACTION_CMDLINE_STOP);
		registerReceiver(mIntentReceiver, filter);
		mActivityRun = true;
	}



	public void onDestroy()
	{
		super.onDestroy();

		if (mActivityRun)
			unregisterReceiver(mIntentReceiver);

		mActivityRun = false;
	}

	private BroadcastReceiver mIntentReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			String action = intent.getAction();
			LogUtil.d(TAG, "Receive Intent action: " + action);

			if (action.equals(CmdLineService.ACTION_CMDLINE_STOP))
			{
				LogUtil.d(TAG, "enter ACTION_CMDLINE_STOP!");
				mCmdFileAdapter.notifyDataSetChanged();
				return;
			}
		}
	};



	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(0, MENU_ADD, 0, R.string.menu_add);
		menu.add(0, MENU_EDIT, 0, R.string.menu_edit);
		menu.add(0, MENU_DELETE, 0, R.string.menu_delete);
		return true;
	}



	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		/*
		 * menu.findItem(CLEAR_PHONE_CODE).setVisible(false);
		 * 
		 * LogUtil.d(TAG, "sel: " + mSmsCodeListView.getSelectedItemId());
		 */

		return super.onPrepareOptionsMenu(menu);
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case MENU_ADD:
			CmdFileEditView.startActivity(CmdFileListView.this, MENU_ADD, -1);
			break;

		case MENU_EDIT:
			EditCmdFile(mListView.getSelectedItemPosition());
			break;

		case MENU_DELETE:
			DeleteCmdFile(mListView.getSelectedItemPosition());
			break;
		}

		return super.onOptionsItemSelected(item);
	}



	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{
		List<CmdFile> CmdFileList = mCmdFileHolder.getFileList();

		AdapterContextMenuInfo info;
		try
		{
			info = (AdapterContextMenuInfo) menuInfo;
		}
		catch (ClassCastException e)
		{
			LogUtil.e(TAG, "bad menuInfo", e);
			return;
		}

		CmdFile item = CmdFileList.get(info.position);
		menu.setHeaderTitle(item.mName);
		menu.add(0, MENU_RUN, 0, R.string.menu_run);
		menu.add(0, MENU_EDIT, 0, R.string.menu_edit);
		menu.add(0, MENU_DELETE, 0, R.string.menu_delete);

	}



	public boolean onContextItemSelected(MenuItem item)
	{
		AdapterContextMenuInfo info;
		try
		{
			info = (AdapterContextMenuInfo) item.getMenuInfo();
		}
		catch (ClassCastException e)
		{
			LogUtil.e(TAG, "bad menuInfo", e);
			return false;
		}

		switch (item.getItemId())
		{
		case MENU_RUN:
			List<CmdFile> CmdFileList = mCmdFileHolder.getFileList();
			CmdFile cmdfile = CmdFileList.get(info.position);
			CmdFileRunView.startActivity(CmdFileListView.this, MENU_RUN, cmdfile);
			break;

		case MENU_EDIT:
			EditCmdFile(info.position);
			break;

		case MENU_DELETE:
			DeleteCmdFile(info.position);
			break;
		}

		return super.onContextItemSelected(item);
	}



	private void EditCmdFile(int position)
	{
		List<CmdFile> CmdFileList = mCmdFileHolder.getFileList();

		if (position < 0 || CmdFileList.size() <= position)
			return;

		if (CmdFileList.get(position).mRun)
		{
			popupDialog(getResources().getString(R.string.cmd_file_running_edit));
			return;
		}

		CmdFileEditView.startActivity(CmdFileListView.this, MENU_EDIT, position);
	}



	private void DeleteCmdFile(int position)
	{
		List<CmdFile> CmdFileList = mCmdFileHolder.getFileList();

		if (position < 0 || CmdFileList.size() <= position)
			return;

		if (CmdFileList.get(position).mRun)
		{
			popupDialog(getResources().getString(R.string.cmd_file_running_delete));
			return;
		}

		mCmdFileHolder.delete(position);
		mCmdFileAdapter.notifyDataSetChanged();
	}



	public void onItemClick(AdapterView<?> parent, View v, int position, long id)
	{
		LogUtil.d(TAG, "click item : " + position + " id: " + id);
		List<CmdFile> CmdFileList = mCmdFileHolder.getFileList();

		if (position < 0 || CmdFileList.size() <= position)
			return;

		CmdFile item = CmdFileList.get(position);

		if (item.mHasLog)
		{
			mCmdFileAdapter.restore(v);
			ViewResult.start(CmdFileListView.this, item);
		}
		else
		{
			CmdFileRunView.startActivity(CmdFileListView.this, MENU_RUN, item);
		}
	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode)
		{
		case MENU_ADD:
		case MENU_EDIT:
		case MENU_RUN:

			if (resultCode == RESULT_CANCELED)
				break;

			mCmdFileAdapter.notifyDataSetChanged();
			break;
		}
	}



	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		switch (keyCode)
		{
		case KeyEvent.KEYCODE_BACK:
			CmdLineService.startNotification(this);
			finish();
			return true;

		default:
			break;

		}

		return super.onKeyDown(keyCode, event);
	}



	private void popupDialog(String tip)
	{
		Toast.makeText(this, tip, Toast.LENGTH_SHORT).show();
	}



	private boolean checkDirectory(String directory)
	{
		File dir = new File(directory);

		if (dir.isFile())
		{
			Log.e(TAG, directory + " is a file, it must be a directory!");
			popupDialog(directory + "is a file, it must be a directory!");
			return false;
		}

		if (dir.isDirectory())
		{
			Log.d(TAG, directory + " is a directory");
			return true;
		}

		if (!dir.exists())
		{
			Terminal.createDirectory(directory);
			return true;
		}

		return false;
	}
}
