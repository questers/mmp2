/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

package com.marvell.cmdline;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;

import com.marvell.cmdline.util.CmdFile;
import com.marvell.cmdline.util.CmdFileHolder;
import com.marvell.cmdline.util.LogUtil;



public class CmdFileEditView extends Activity
{
	private static final String TAG = "CmdFileEditView";
	private static final String FILE_INDEX = "file_index";
	private static final int MENU_SAVE = 1;

	private CmdFileHolder mCmdFileHolder = null;
	private CmdFile mCmdFile = null;
	private int mFileIndex = 0;
	private EditText mCmdFileName = null;
	private EditText mCmdFileContent = null;
	private CheckBox mBgCheckBox = null;
	private CheckBox mRootCheckBox = null;



	public static void startActivity(Activity parent, int reqCode, int index)
	{
		Intent intent = new Intent(parent, CmdFileEditView.class);
		intent.putExtra(FILE_INDEX, index);
		parent.startActivityForResult(intent, reqCode);
	}



	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cmd_file_edit);

		mCmdFileName = (EditText) findViewById(R.id.cmd_file_name);
		mCmdFileContent = (EditText) findViewById(R.id.cmd_file_content);
		mBgCheckBox = (CheckBox) findViewById(R.id.cmd_run_background);
		mRootCheckBox = (CheckBox) findViewById(R.id.cmd_run_root);

		mCmdFileHolder = CmdFileHolder.getSingleInstance();
		mFileIndex = getIntent().getExtras().getInt(FILE_INDEX);

		if (-1 < mFileIndex && mFileIndex < mCmdFileHolder.getFileList().size())
		{
			mCmdFile = mCmdFileHolder.getFileList().get(mFileIndex).clone();

			mCmdFileName.setText(mCmdFile.mName);

			String command = "";
			for (int i = 0; i < mCmdFile.mCmdList.size(); i++)
			{
				command += mCmdFile.mCmdList.get(i);

				if (i < mCmdFile.mCmdList.size() - 1)
					command += "\n";
			}

			mCmdFileContent.setText(command);
			mBgCheckBox.setChecked(mCmdFile.mBackground);
			mRootCheckBox.setChecked(mCmdFile.mRoot);
			setTitle(R.string.edit_cmd_file);
		}
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(0, MENU_SAVE, 0, R.string.menu_save);
		return true;
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case MENU_SAVE:
			saveCmdFile();
			break;
		}

		return super.onOptionsItemSelected(item);
	}



	private void saveCmdFile()
	{
		String filename = mCmdFileName.getText().toString();
		String filecontent = mCmdFileContent.getText().toString();

		if (TextUtils.isEmpty(filename) || TextUtils.isEmpty(filecontent))
		{
			LogUtil.d(TAG, "Command line name and content cann't be empty!");
			return;
		}

		if (null == mCmdFile)
		{
			mCmdFile = new CmdFile();
			mCmdFile.mName = filename;
			mCmdFile.mCmdList = splitLine(filecontent);
			mCmdFile.mBackground = mBgCheckBox.isChecked();
			mCmdFile.mRoot = mRootCheckBox.isChecked();
			mCmdFileHolder.add(mCmdFile, true);
		}
		else
		{
			mCmdFile.mName = filename;
			mCmdFile.mCmdList = splitLine(filecontent);
			mCmdFile.mBackground = mBgCheckBox.isChecked();
			mCmdFile.mRoot = mRootCheckBox.isChecked();
			mCmdFileHolder.edit(mFileIndex, mCmdFile);
		}

		setResult(RESULT_OK);
		finish();
	}



	private List<String> splitLine(String command)
	{
		List<String> CmdList = new ArrayList<String>();
		String line[] = command.split("\n", 50);

		for (int i = 0; i < line.length; i++)
			CmdList.add(line[i]);

		return CmdList;
	}
}
