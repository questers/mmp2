/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

package com.marvell.cmdline.service;

import java.util.ArrayList;
import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;

import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.marvell.cmdline.CmdFileListView;

import com.marvell.cmdline.R;
import com.marvell.cmdline.util.CmdFile;
import com.marvell.cmdline.util.CmdFileHolder;
import com.marvell.cmdline.util.LogUtil;
import com.marvell.cmdline.view.ListResult;
import com.marvell.cmdline.view.ViewResult;



public class CmdLineService extends Service
{
	private static final String TAG = "CmdLineService";

	public static final String ACTION_CMDLINE_STOP = "com.marvell.cmdline.stop";

	private final static int CMDLINE_NOTIFICATION = 1;

	private static List<Terminal> mTermArray = new ArrayList<Terminal>();
	private static Terminal mTerm = null;



	public static Terminal getTerminalByCmdFile(CmdFile cmdfile)
	{
		synchronized (mTermArray)
		{
			for (int i = 0; i < mTermArray.size(); i++)
			{
				if (cmdfile == mTermArray.get(i).mCmdFile)
					return mTermArray.get(i);
			}

			return null;
		}
	}



	public static void start(Context context, Terminal term)
	{
		synchronized (mTermArray)
		{
			mTerm = term;

			if (!mTermArray.contains(mTerm))
				mTermArray.add(mTerm);

			Intent intent = new Intent(context, CmdLineService.class);
			context.startService(intent);
			LogUtil.i(TAG, "add " + mTerm.mCmdFile.mName + " to service");
		}
	}



	public static void stop(Context context, Terminal term)
	{
		synchronized (mTermArray)
		{
			if (mTermArray.contains(mTerm))
				mTermArray.remove(term);

			if (0 == mTermArray.size())
			{
				Intent intent = new Intent(context, CmdLineService.class);
				context.stopService(intent);
			}
		}

		LogUtil.i(TAG, "remove " + mTerm.mCmdFile.mName + " from service");
	}



	public void onCreate()
	{
		super.onCreate();
		LogUtil.d(TAG, "enter onCreate");
	}



	public void onStart(Intent intent, int startId)
	{
		mTerm.setHandler(mHandler, Terminal.STATE_BACKGROUND);
	}



	public void onDestroy()
	{
		super.onDestroy();
		LogUtil.d(TAG, "enter onDestroy");
	}



	public IBinder onBind(Intent intent)
	{
		return null;
	}

	private final Handler mHandler = new Handler()
	{
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
			case Terminal.MSG_STATE_STOP:
				LogUtil.e(TAG, "Receive MSG_STATE_STOP");
				broadcast(msg);
				break;

			default:
				break;

			}
		}



		private void broadcast(Message msg)
		{
			Terminal term = (Terminal) msg.obj;
			CmdLineService.stop(CmdLineService.this, term);

			List<CmdFile> LogList = CmdFileHolder.getSingleInstance().getLogList();

			synchronized (LogList)
			{
				if (!LogList.contains(term.mCmdFile))
					LogList.add(term.mCmdFile);
			}

			if (CmdFileListView.mActivityRun || ListResult.mActivityRun)
			{
				Intent intent = new Intent(ACTION_CMDLINE_STOP);
				CmdLineService.this.sendBroadcast(intent);
			}
			else
			{
				startNotification(CmdLineService.this);
			}
		}
	};



	public static void startNotification(Context context)
	{
		List<CmdFile> LogList = CmdFileHolder.getSingleInstance().getLogList();

		synchronized (LogList)
		{
			Intent intent = null;

			if (0 == LogList.size())
				return;

			if (1 < LogList.size())
			{
				intent = ListResult.getIntent(context);
			}
			else
			{
				intent = ViewResult.getIntent(context, LogList.get(0));
			}

			PendingIntent pi = PendingIntent.getActivity(context, 0, intent, 0);

			Notification notif = new Notification(R.drawable.icon, context.getString(com.marvell.cmdline.R.string.app_name), System.currentTimeMillis());
			CmdFile cmdfile = LogList.get(0);
			notif.setLatestEventInfo(context, cmdfile.mName, cmdfile.mCmdList.get(0), pi);

			LogUtil.d(TAG, "Notify top bar to display command line result.");
			NotificationManager mNotificationMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationMgr.notify(CMDLINE_NOTIFICATION, notif);
		}
	}



	public static void cancelNotification(Context context)
	{
		LogUtil.d(TAG, "Notify top bar to remove command line result.");
		NotificationManager mNotificationMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationMgr.cancel(CMDLINE_NOTIFICATION);
	}
}
