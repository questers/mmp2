/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */


package com.marvell.cmdline.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.marvell.cmdline.R;
import com.marvell.cmdline.service.CmdLineService;
import com.marvell.cmdline.util.CmdFile;



public class ViewResult extends Activity
{
	// private static final String TAG = "ViewResult";

	private EditText mLogEdit = null;
	private Button mRunBtn = null;

	private static CmdFile mCmdFile = null;



	public static void start(Activity parent, CmdFile cmdfile)
	{
		mCmdFile = cmdfile;
		Intent intent = new Intent(parent, ViewResult.class);
		parent.startActivity(intent);
	}



	public static Intent getIntent(Context context, CmdFile cmdfile)
	{
		mCmdFile = cmdfile;
		return new Intent(context, ViewResult.class);
	}



	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cmd_file_run);

		mRunBtn = (Button) findViewById(R.id.btn_run_stop);
		mRunBtn.setVisibility(View.GONE);
		mLogEdit = (EditText) findViewById(R.id.cmd_output);

		mCmdFile.mHasLog = false;
		CmdLineService.cancelNotification(this);

		String output = mCmdFile.getOutput();
		if (!TextUtils.isEmpty(output))
			mLogEdit.setText(output);
	}
}
