/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */


package com.marvell.cmdline.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.text.TextUtils;
import android.util.Log;



public class CmdFileHolder
{
	private final static String TAG = "CmdFileHolder";
	private static CmdFileHolder mSingleInstance = null;

	private List<CmdFile> mCmdFileList = null;
	private CmdXmlParser mXmlParser = null;
	private CmdXmlSerializer mXmlSerializer = null;

	private List<CmdFile> mLogList = null;



	public static CmdFileHolder getSingleInstance()
	{
		if (null == mSingleInstance)
			mSingleInstance = new CmdFileHolder();

		return mSingleInstance;
	}



	private CmdFileHolder()
	{
		mXmlParser = new CmdXmlParser();
		mXmlSerializer = new CmdXmlSerializer();

		mCmdFileList = new ArrayList<CmdFile>();
		mCmdFileList.clear();

		mLogList = new ArrayList<CmdFile>();
		
		handleCmdDir(CmdConst.CMD_DIR_PRESET);
		handleCmdDir(CmdConst.CMD_DIR);
	}
	
	public void handleCmdDir(String cmddir)
	{
		File dir = new File(cmddir);

		if (!dir.isDirectory())
		{
			Log.e(TAG, cmddir + " isn't a direcotry!");
			return;
		}

		File[] files = dir.listFiles();

		if (null == files)
		{
			Log.e(TAG, "Fail to read files from " + cmddir);
			return;
		}

		for (int i = 0; i < files.length; i++)
		{
			File file = files[i];
			if (!file.isFile())
			{
				continue;
			}

			String suffix = getSuffix(file.getName());
			if (TextUtils.isEmpty(suffix) || !suffix.equals(CmdConst.SUFFIX_XML))
			{
				continue;
			}

			String prefix = filterSuffix(file.getName());
			if (TextUtils.isEmpty(prefix))
			{
				continue;
			}

			try
			{
				CmdFile item = mXmlParser.parserXml(file.getPath());
				if (null != item)
				{
					item.mName = prefix;
					item.mRun = false;
					item.mHasLog = false;
					add(item, false);
				}
			}
			catch (XmlPullParserException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}



	public int add(CmdFile item, boolean flush)
	{
		if (null == item || TextUtils.isEmpty(item.mName))
		{
			LogUtil.d(TAG, "Command name/content mustn't be empty!");
			return -1;
		}

		int pos = BinarySearch(item.mName);
		mCmdFileList.add(pos, item);
		
		if (flush)
			mXmlSerializer.flush(item);
		return 0;
	}



	public int edit(int index, CmdFile item)
	{
		delete(index);
		return add(item, true);
	}



	public void delete(int index)
	{
		if (index < 0 || mCmdFileList.size() <= index)
		{
			LogUtil.d(TAG, "Index " + index + "is invalid, it must between 0 and " + (mCmdFileList.size() - 1));
			return;
		}

		CmdFile item = mCmdFileList.get(index);
		mCmdFileList.remove(index);

		File cmdfile = new File(CmdConst.CMD_DIR + "/" + item.mName + CmdConst.SUFFIX_XML);
		if (cmdfile.exists() && cmdfile.isFile())
			cmdfile.delete();

		File logfile = new File(CmdConst.CMD_DIR + "/" + item.mName + CmdConst.SUFFIX_LOG);
		if (logfile.exists() && logfile.isFile())
			logfile.delete();
	}



	public List<CmdFile> getFileList()
	{
		return mCmdFileList;
	}



	public List<CmdFile> getLogList()
	{
		synchronized (mLogList)
		{
			for (int i = 0; i < mLogList.size(); i++)
			{
				CmdFile item = mLogList.get(i);
				if (!item.mHasLog)
				{
					mLogList.remove(i);
					i--;
				}
			}
		}

		return mLogList;
	}



	private String filterSuffix(String fileName)
	{
		int nPos = fileName.lastIndexOf(".");

		if (nPos < 0)
			return null;

		String prefix = fileName.substring(0, nPos);

		return prefix;
	}



	private String getSuffix(String fileName)
	{
		int nPos = fileName.lastIndexOf(".");

		if (nPos < 0)
			return null;

		String suffix = fileName.substring(nPos);

		return suffix;
	}



	private int BinarySearch(String name)
	{
		if (0 == mCmdFileList.size())
			return 0;

		int left = 0;
		int right = mCmdFileList.size() - 1;

		CmdFile item = mCmdFileList.get(0);

		if (name.compareTo(item.mName) < 0)
		{
			return 0;
		}

		item = mCmdFileList.get(right);

		if (name.compareTo(item.mName) > 0)
		{
			return mCmdFileList.size();
		}

		while (left <= right)
		{
			int middle = (left + right) / 2;
			item = mCmdFileList.get(middle);

			if (name.compareTo(item.mName) > 0)
				left = middle + 1;
			else
				right = middle - 1;

		}

		return left;
	}
}
