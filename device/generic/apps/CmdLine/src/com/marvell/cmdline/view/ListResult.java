/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */


package com.marvell.cmdline.view;

import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import android.view.KeyEvent;

import android.view.View;

import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.marvell.cmdline.R;
import com.marvell.cmdline.CmdFileAdapter;
import com.marvell.cmdline.service.CmdLineService;
import com.marvell.cmdline.util.CmdFile;
import com.marvell.cmdline.util.CmdFileHolder;
import com.marvell.cmdline.util.LogUtil;



public class ListResult extends Activity implements OnItemClickListener
{
	private static final String TAG = "ListResult";
	public static boolean mActivityRun = false;

	private ListView mListView = null;
	private CmdFileAdapter mCmdFileAdapter = null;

	private List<CmdFile> mLogList = null;



	public static Intent getIntent(Context context)
	{
		return new Intent(context, ListResult.class);
	}



	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		mActivityRun = true;

		CmdLineService.cancelNotification(this);

		mLogList = CmdFileHolder.getSingleInstance().getLogList();
		mCmdFileAdapter = new CmdFileAdapter(this, R.layout.cmd_file_list_item, mLogList);
		mListView = (ListView) findViewById(R.id.file_list);
		mListView.setAdapter(mCmdFileAdapter);
		mListView.setOnItemClickListener(this);

		IntentFilter filter = new IntentFilter();
		filter.addAction(CmdLineService.ACTION_CMDLINE_STOP);
		registerReceiver(mIntentReceiver, filter);
	}



	public void onDestroy()
	{
		super.onDestroy();
		unregisterReceiver(mIntentReceiver);
		mActivityRun = false;
	}

	private BroadcastReceiver mIntentReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			String action = intent.getAction();
			LogUtil.d(TAG, "Receive Intent action: " + action);

			if (action.equals(CmdLineService.ACTION_CMDLINE_STOP))
			{
				LogUtil.d(TAG, "enter ACTION_CMDLINE_STOP!");
				mCmdFileAdapter.notifyDataSetChanged();
				return;
			}
		}
	};



	public void onItemClick(AdapterView<?> parent, View v, int position, long id)
	{
		LogUtil.d(TAG, "click item : " + position + " id: " + id);
		synchronized (mLogList)
		{
			if (position < 0 || mLogList.size() <= position)
				return;

			mCmdFileAdapter.restore(v);
			ViewResult.start(ListResult.this, mLogList.get(position));
		}
	}



	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		switch (keyCode)
		{
		case KeyEvent.KEYCODE_BACK:
			CmdLineService.startNotification(this);
			finish();
			return true;

		default:
			break;

		}

		return super.onKeyDown(keyCode, event);
	}
}
