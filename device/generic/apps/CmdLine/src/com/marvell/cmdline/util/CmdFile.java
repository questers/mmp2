/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */


package com.marvell.cmdline.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public class CmdFile
{
	public String mName;
	public List<String> mCmdList;
	public boolean mBackground;
	public boolean mRun;
	public boolean mHasLog;
	public boolean mRoot;



	public CmdFile clone()
	{
		CmdFile obj = new CmdFile();
		obj.mName = mName;

		obj.mCmdList = new ArrayList<String>();

		for (int i = 0; i < mCmdList.size(); i++)
			obj.mCmdList.add(mCmdList.get(i));

		obj.mBackground = mBackground;
		obj.mRun = mRun;
		obj.mRoot = mRoot;
		return obj;
	}



	public String getCommand()
	{
		String command = "";
		for (int i = 0; i < mCmdList.size(); i++)
		{
			command += mCmdList.get(i);

			if (0 < command.length() && i < mCmdList.size() - 1)
				command += ";";
		}

		command += "\r";

		return command;
	}



	public String getOutput()
	{
		File logfile = new File(CmdConst.CMD_DIR + "/" + mName + CmdConst.SUFFIX_LOG);
		if (!logfile.isFile())
			return null;

		try
		{
			FileInputStream fis = new FileInputStream(logfile);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			int cnt = -1;
			byte[] buf = new byte[256];

			while ((cnt = fis.read(buf)) != -1)
			{
				baos.write(buf, 0, cnt);
			}

			return new String(baos.toByteArray());
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return null;
	}

}
