/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

package com.marvell.cmdline;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.marvell.cmdline.service.CmdLineService;
import com.marvell.cmdline.service.Terminal;
import com.marvell.cmdline.util.CmdFile;
import com.marvell.cmdline.util.LogUtil;



public class CmdFileRunView extends Activity implements OnClickListener
{
	private static final String TAG = "CmdFileRunView";

	private EditText mLogEdit = null;
	private Button mRunBtn = null;

	private Terminal mTerminal = null;
	private static CmdFile mCmdFile = null;



	public static void startActivity(Activity parent, int reqCode, CmdFile cmdfile)
	{
		mCmdFile = cmdfile;
		Intent intent = new Intent(parent, CmdFileRunView.class);
		parent.startActivityForResult(intent, reqCode);
	}



	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cmd_file_run);

		mRunBtn = (Button) findViewById(R.id.btn_run_stop);
		mRunBtn.setOnClickListener(this);
		mLogEdit = (EditText) findViewById(R.id.cmd_output);

		setTitle(mCmdFile.mName);

		if (mCmdFile.mBackground)
		{
			LogUtil.d(TAG, "Check whether command " + mCmdFile.mName + " is running!");
			mTerminal = CmdLineService.getTerminalByCmdFile(mCmdFile);
		}

		if (null == mTerminal)
		{
			LogUtil.d(TAG, "command " + mCmdFile.mName + " doesn't run!");
			mTerminal = new Terminal(mHandler, mCmdFile);
		}
		else
		{
			LogUtil.d(TAG, "command " + mCmdFile.mName + " is running!");
			mTerminal.setHandler(mHandler, Terminal.STATE_FOREGROUND);
			CmdLineService.stop(this, mTerminal);

			mRunBtn.setText(R.string.cmd_stop);
			String output = mTerminal.getOutput();
			if (!TextUtils.isEmpty(output))
				mLogEdit.setText(output);
		}
	}

	private final Handler mHandler = new Handler()
	{
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
			case Terminal.MSG_UPDATE_DATA:
				String log = mTerminal.update();
				if (null != log)
					mLogEdit.append(log);

				break;

			case Terminal.MSG_STATE_STOP:
				LogUtil.d(TAG, "Enable control button, receive MSG_STATE_STOP");
				mRunBtn.setEnabled(true);
				mRunBtn.setText(R.string.cmd_run);
				break;

			default:
				break;

			}
		}
	};



	/*
	 * Reset will cost some time to accomplish, so we need to disable user operation.
	 */
	public void onClick(View v)
	{
		if (!mCmdFile.mRun)
		{
			mLogEdit.setText("");
			mRunBtn.setText(R.string.cmd_stop);
			mTerminal.exec();
		}
		else
		{
			LogUtil.d(TAG, "Disable control button!");
			mRunBtn.setEnabled(false);
			mRunBtn.setText(R.string.cmd_run);
			mTerminal.reset();
		}
	}



	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		switch (keyCode)
		{
		case KeyEvent.KEYCODE_BACK:
			onKeyBack();
			return true;

		default:
			break;

		}

		return super.onKeyDown(keyCode, event);
	}



	private void onKeyBack()
	{
		if (mCmdFile.mRun)
		{
			if (mCmdFile.mBackground)
			{
				CmdLineService.start(this, mTerminal);
			}
			else
			{
				mTerminal.kill();
			}
		}
		else
		{
			mTerminal.exit();
		}

		setResult(RESULT_OK);

		finish();
	}
}
