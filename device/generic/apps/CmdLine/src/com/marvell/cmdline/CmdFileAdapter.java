/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

package com.marvell.cmdline;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.marvell.cmdline.util.CmdFile;
import com.marvell.cmdline.util.LogUtil;



public class CmdFileAdapter extends ArrayAdapter<CmdFile>
{
	// private static final String TAG = "CmdFileAdapter";
	class CmdLineHolder
	{
		ImageView mIconFgBg;
		TextView mName;
		ImageView mIconRunStop;
	}

	private int mListItemLayoutId = -1;
	private LayoutInflater mInflater = null;
	private Typeface mTypeFace = Typeface.create(Typeface.SERIF, Typeface.BOLD);



	public CmdFileAdapter(Context context, int textViewResourceId, List<CmdFile> objects)
	{
		super(context, textViewResourceId, objects);
		mListItemLayoutId = textViewResourceId;
		mInflater = LayoutInflater.from(context);
	}



	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = null;
		CmdLineHolder holder = null;

		if (null == convertView)
		{
			view = mInflater.inflate(mListItemLayoutId, parent, false);
			holder = new CmdLineHolder();
			holder.mName = (TextView) view.findViewById(R.id.cmd_name);
			holder.mIconFgBg = (ImageView) view.findViewById(R.id.icon_fg_bg);
			holder.mIconRunStop = (ImageView) view.findViewById(R.id.icon_run_stop);
			view.setTag(holder);
		}
		else
		{
			view = convertView;
			holder = (CmdLineHolder) view.getTag();
		}

		CmdFile item = (CmdFile) getItem(position);

		if (item.mBackground)
			holder.mIconFgBg.setImageResource(R.drawable.run_background);
		else
			holder.mIconFgBg.setImageResource(R.drawable.run_foreground);

		if (item.mRun)
			holder.mIconRunStop.setImageResource(R.drawable.state_run);
		else
			holder.mIconRunStop.setImageResource(R.drawable.state_stop);

		// Set the text lines
		if (!TextUtils.isEmpty(item.mName))
		{
			holder.mName.setText(item.mName);
			// LogUtil.d(TAG, "item name: " + item.mName + " hasLog: " +
			// item.mHasLog);
			if (item.mHasLog)
			{
				holder.mName.setTypeface(mTypeFace);
				holder.mName.setTextSize(18);
			}
			else
			{
				holder.mName.setTextSize(16);
				holder.mName.setTypeface(null);
			}
		}

		return view;

	}



	public void restore(View v)
	{
		CmdLineHolder holder = (CmdLineHolder) v.getTag();
		holder.mName.setTextSize(16);
		holder.mName.setTypeface(null);
	}
}
