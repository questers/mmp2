/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 */

package com.marvell.cmdline.service;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.marvell.cmdline.util.CmdConst;
import com.marvell.cmdline.util.CmdFile;
import com.marvell.cmdline.util.LogUtil;



public class Terminal implements Runnable
{
	private static final String TAG = "Terminal";
	private static final String DEFAULT_SHELL = "/system/bin/sh";
	private static final String DEFAULT_PATH = "export PATH=/data/local/bin:$PATH\r";

	private static final int BUFF_SIZE = (4 * 1024);
	public static final int MSG_UPDATE_DATA = 1;
	public static final int MSG_STATE_STOP = 2;

	private Handler mHandler = null;
	public CmdFile mCmdFile = null;

	private int mProcessState = STATE_INVALID;
	public static final int STATE_INVALID = -1;
	public static final int STATE_IDLE = 0;
	public static final int STATE_FOREGROUND = 1;
	public static final int STATE_BACKGROUND = 2;
	public static final int STATE_RESET = 3;

	private int mProcessId = -1;
	private FileDescriptor mTermFd = null;
	private FileOutputStream mTermOut = null;
	private FileInputStream mTermIn = null;

	private ByteQueue mByteQueue = null;
	private byte[] mReceiveBuffer = null;
	private byte[] mReadBuffer = null;

	private FileOutputStream mLogFileOutputStream = null;
	private Thread mPollingThread = null;



	public Terminal(Handler handler, CmdFile cmdfile)
	{
		mHandler = handler;
		mCmdFile = cmdfile;

		init();
	}



	public void setHandler(Handler handler, int state)
	{
		mHandler = handler;
		setState(state);
	}



	public void exec()
	{
		File logfile = new File(CmdConst.CMD_DIR + "/" + mCmdFile.mName + CmdConst.SUFFIX_LOG);
		if (logfile.exists())
			logfile.delete();

		String command = mCmdFile.getCommand();
		LogUtil.d(TAG, "set " + mCmdFile.mName + " to true");
		LogUtil.d(TAG, "Create read thread and save output to file: " + logfile.getAbsolutePath());

		try
		{
			mCmdFile.mRun = true;
			setState(STATE_FOREGROUND);
			logfile.createNewFile();
			mLogFileOutputStream = new FileOutputStream(logfile);

			mTermOut.write(command.getBytes());
			mTermOut.flush();

			// Set up a thread to read input from the pseudo-teletype
			mPollingThread = new Thread(this);
			mPollingThread.setName("Terminal output reader");
			mPollingThread.start();
		}
		catch (IOException e)
		{
			reset();
			LogUtil.e(TAG, "Fail to exec: " + command);
			LogUtil.e(TAG, "msg: " + e.getMessage());
			e.printStackTrace();
		}
	}



	private void execBlock(String cmd) throws IOException
	{
		LogUtil.d(TAG, "exec block command: " + cmd);
		mTermOut.write(cmd.getBytes());
		mTermOut.flush();

		int readlen = mTermIn.read(mReadBuffer);

		if (0 < readlen)
		{
			String output = new String(mReadBuffer, 0, readlen);
			LogUtil.e(TAG, "output: " + output);
		}
	}



	/*
	 * Set mRun of CmdFile to false to avoid failing to exit thread!
	 */
	public void kill()
	{
		LogUtil.d(TAG, "enter kill process!");

		mCmdFile.mRun = false;
		setState(STATE_IDLE);
		Exec.destroySubprocess(mProcessId);
	}



	public void reset()
	{
		LogUtil.d(TAG, "enter reset");
		
		try
		{
			LogUtil.d(TAG, "sleep 1000 milliseconds to avoid too many click!");
			Thread.sleep(100, 0);
		}
		catch (InterruptedException e)
		{
			LogUtil.e(TAG, "Failede to sleep 50 in thread!");
			e.printStackTrace();
		}		

		mCmdFile.mRun = false;
		setState(STATE_RESET);
		Exec.destroySubprocess(mProcessId);
	}



	public void exit()
	{
		LogUtil.d(TAG, "enter exit!");
		try
		{
			if (null != mCmdFile && mCmdFile.mRoot)
			{
				LogUtil.d(TAG, "Exit root");
				execBlock("exit\r");
			}

			execBlock("exit\r");
			mTermIn.close();
			mTermOut.close();
		}
		catch (IOException e)
		{
			LogUtil.e(TAG, "Fail to exit terminal: " + e.getMessage());
			e.printStackTrace();
		}
	}



	public String getOutput()
	{
		if (null == mLogFileOutputStream)
			return null;

		synchronized (mLogFileOutputStream)
		{
			return mCmdFile.getOutput();
		}
	}



	public void run()
	{
		while (true)
		{
			try
			{
				if (!readData())
					return;
			}
			catch (IOException e)
			{
				setState(STATE_IDLE);
				quit();
				LogUtil.e(TAG, "IOException error: " + e.getMessage());
				e.printStackTrace();
				return;
			}
			catch (InterruptedException e)
			{
				setState(STATE_IDLE);
				quit();
				LogUtil.e(TAG, "InterruptedException error: " + e.getMessage());
				e.printStackTrace();
				return;
			}
		}
	}



	public String update()
	{
		LogUtil.d(TAG, "enter update");
		int bytesAvailable = mByteQueue.getBytesAvailable();
		int bytesToRead = Math.min(bytesAvailable, mReceiveBuffer.length);

		try
		{
			int bytesRead = mByteQueue.read(mReceiveBuffer, 0, bytesToRead);

			if (0 < bytesRead)
				return formatLine(mReceiveBuffer, bytesRead);
		}
		catch (InterruptedException e)
		{
			LogUtil.e(TAG, "read error: " + e.getMessage());
			e.printStackTrace();
		}

		return null;
	}



	private boolean readData() throws IOException, InterruptedException
	{
		int readlen = mTermIn.read(mReadBuffer);

		if (readlen <= 0)
		{
			LogUtil.e(TAG, "Fail to read data from input stream, read thread will exit!");
			quit();
			return false;
		}

		mByteQueue.write(mReadBuffer, 0, readlen);

		String output = formatLine(mReadBuffer, readlen);
		
		if (TextUtils.isEmpty(output))
		{
			LogUtil.d(TAG, "Output is empty, so we ignore it!");
			return true;
		}

		if (null == mLogFileOutputStream)
		{
			LogUtil.e(TAG, "mLogFileOutputStream is null, so we exit read thread!");
			return false;
		}

		synchronized (mLogFileOutputStream)
		{
			mLogFileOutputStream.write(output.getBytes());
			mLogFileOutputStream.flush();
		}

		if (null != mHandler)
		{
			mHandler.sendMessage(mHandler.obtainMessage(MSG_UPDATE_DATA));
		}

		// LogUtil.d(TAG, "last char: " + (byte)output.charAt(output.length() -
		// 1));

		if (output.charAt(output.length() - 1) == '#' || output.charAt(output.length() - 1) == '$')
		{
			LogUtil.e(TAG, "Command have been finished!");
			quit();
			return false;
		}

		return true;
	}



	private void quit()
	{
		try
		{
			if (null != mLogFileOutputStream)
			{
				mLogFileOutputStream.close();
				mLogFileOutputStream = null;
			}
		}
		catch (IOException e)
		{
			LogUtil.e(TAG, "Fail to exit terminal: " + e.getMessage());
			e.printStackTrace();
		}

		mPollingThread = null;
		mCmdFile.mRun = false;
		LogUtil.d(TAG, "set " + mCmdFile.mName + " to false");
		
		synchronized (this)
		{
			// Execute exit in shell process, then it will destroy itself.
			switch (mProcessState)
			{
			case STATE_IDLE:
				LogUtil.e(TAG, "enter quit process STATE_IDLE!");
				mCmdFile.mHasLog = false;
				exit();
				break;

			case STATE_BACKGROUND:
				LogUtil.e(TAG, "enter quit process STATE_BACKGROUND!");
				mCmdFile.mHasLog = true;
				exit();
				break;

			case STATE_FOREGROUND:
				LogUtil.e(TAG, "enter quit process STATE_FOREGROUND!");
				mCmdFile.mHasLog = false;
				break;
				
			case STATE_RESET:
				LogUtil.e(TAG, "enter quit process STATE_RESET!");
				mCmdFile.mHasLog = false;
				exit();
				init();
				break;

			default:
				break;
			}
		}

		if (null != mHandler)
		{
			mHandler.sendMessage(mHandler.obtainMessage(MSG_STATE_STOP, this));
		}
	}



	private void init()
	{
		LogUtil.d(TAG, "init Terminal");
		int[] processId = new int[1];
		mTermFd = Exec.createSubprocess(DEFAULT_SHELL, "-", null, processId);
		mProcessId = processId[0];
		setState(STATE_IDLE);

		mTermIn = new FileInputStream(mTermFd);
		mTermOut = new FileOutputStream(mTermFd);

		mByteQueue = new ByteQueue(BUFF_SIZE);
		mReceiveBuffer = new byte[BUFF_SIZE];
		mReadBuffer = new byte[BUFF_SIZE];

		try
		{
			execBlock(DEFAULT_PATH);
			if (null != mCmdFile && mCmdFile.mRoot)
			{
				LogUtil.d(TAG, "Run " + mCmdFile.mName + " as root!");
				execBlock("msu\r");
			}

			waitFor();
		}
		catch (IOException e)
		{
			LogUtil.e(TAG, "Fail to create Terminal: " + e.getMessage());
			e.printStackTrace();
		}
	}



	private void waitFor()
	{
		Runnable watchForDeath = new Runnable()
		{
			public void run()
			{
				LogUtil.d(TAG, "waiting for: " + mProcessId);
				int result = Exec.waitFor(mProcessId);
				LogUtil.d(TAG, "Subprocess exited: " + result);
			}
		};

		Thread watcher = new Thread(watchForDeath);
		watcher.start();
	}



	private void setState(int state)
	{
		synchronized (this)
		{
			mProcessState = state;
			switch (mProcessState)
			{
			case STATE_IDLE:
				LogUtil.e(TAG, "setState STATE_IDLE!");
				break;

			case STATE_FOREGROUND:
				LogUtil.e(TAG, "setState STATE_FOREGROUND!");
				break;

			case STATE_BACKGROUND:
				LogUtil.e(TAG, "setState STATE_BACKGROUND!");
				break;
				
			case STATE_RESET:
				LogUtil.e(TAG, "setState STATE_RESET!");
				break;

			default:
				LogUtil.e(TAG, "setState invalid state: " + mProcessState);
				break;
			}
		}
	}



	private String formatLine(byte[] buffer, int length)
	{
		return new String(buffer, 0, length).replaceAll("\r", "").trim();
	}



	public static void createDirectory(String direcotry)
	{
		Terminal term = new Terminal(null, null);
		try
		{
			Log.d(TAG, CmdConst.CMD_DIR + " will be created exist!");
			term.execBlock("msu\r");
			term.execBlock("mkdir " + direcotry + "\r");
			term.execBlock("exit\r");
			term.exit();
			LogUtil.d(TAG, "If " + CmdConst.CMD_DIR + " doesn't exist, please execute chmod 4755 /system/xbin/msu");
		}
		catch (IOException e)
		{
			LogUtil.e(TAG, "Fail to create directory: " + direcotry);
			e.printStackTrace();
		}
	}
}
