#if !defined QST_LOG_H
#define QST_LOG_H

#define LOG_TAG "usbcam"
#include <android/log.h>

#define ENABLE_LOGGING

#if defined(ENABLE_LOGGING)
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#else
#define  LOGD(...)
#endif

#define BYTE unsigned char
#define WORD unsigned short
#define DWORD unsigned int
#define UINT unsigned int
#define BOOL int

#endif // QST_LOG_H
