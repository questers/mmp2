#include <stdio.h>
#include <string.h>
#include <jni.h>

#include "log.h"
#include "libusb/libusb.h"
#include "zkfpi.h"

#define U8 unsigned char

#define ASSERT(b) \
do \
{ \
    if (!(b)) \
    { \
        LOGD("error on %s:%d", __FUNCTION__, __LINE__); \
        return 0; \
    } \
} while (0)

#define C5515_VID 0x1b55
#define C5515_PID 0x0830
#define CAM_BUF_SZ 480*320
#define CMD_INIT 0xE0
#define CMD_GET_IMAGE 0xE5
#define TIME_OUT 2500
struct JNI_CONTROL_BLOCK
{
    JNIEnv* env;
    jobject obj;
    jmethodID notifyDataReady;
} jcb;


static struct libusb_device_handle *devh = NULL;
static struct libusb_transfer *img_transfer = NULL;
static unsigned char buf[CAM_BUF_SZ];
static char ep_bulk = 0x81;

static const char* myClassName="com/quester/cam/CamView";

static void cb_img(struct libusb_transfer *transfer)
{
}

int init_usb(struct JNI_CONTROL_BLOCK* jcb)
{
    int ret;

    ret = libusb_init(NULL);
    ASSERT(ret == 0);

    devh = libusb_open_device_with_vid_pid(NULL, C5515_VID, C5515_PID);
    ASSERT(devh != NULL);

    ret = libusb_set_configuration(devh, 1);
    ASSERT(ret == 0);

    ret = libusb_claim_interface(devh, 0);
    ASSERT(ret == 0);

    libusb_control_transfer(devh, 
        LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_OUT, 
        CMD_INIT, 0, 0, NULL, 0, TIME_OUT);

    libusb_control_transfer(devh, 
        LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_OUT, 
        CMD_GET_IMAGE, 0, 0, NULL, 0, TIME_OUT);

    img_transfer = libusb_alloc_transfer(0);
    ASSERT(img_transfer != NULL);

    libusb_fill_bulk_transfer(img_transfer, devh, ep_bulk, buf,
        sizeof(buf), cb_img, jcb, 0);

    ret = libusb_submit_transfer(img_transfer);
    ASSERT(ret == 0);

    return 0;
}

int init_usb_sync(struct JNI_CONTROL_BLOCK* jcb)
{
    int ret;

    ret = libusb_init(NULL);
    ASSERT(ret == 0);

    devh = libusb_open_device_with_vid_pid(NULL, C5515_VID, C5515_PID);
    ASSERT(devh != NULL);

    ret = libusb_set_configuration(devh, 1);
    ASSERT(ret == 0);

    ret = libusb_claim_interface(devh, 0);
    ASSERT(ret == 0);

    libusb_control_transfer(devh, 
        LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_OUT, 
        CMD_INIT, 0, 0, NULL, 0, TIME_OUT);

    libusb_control_transfer(devh, 
        LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_OUT, 
        CMD_GET_IMAGE, 0, 0, NULL, 0, TIME_OUT);

    int len = 0;
    ret = libusb_bulk_transfer(devh, ep_bulk, buf, CAM_BUF_SZ, &len, 0);
    ASSERT(ret == 0);

    return 0;
}

void free_usb()
{
    libusb_free_transfer(img_transfer);
    libusb_release_interface(devh, 0);
    libusb_close(devh);
    libusb_exit(NULL);
}

#ifndef SKELETON 
int main()
{
#if 1
    int i;
    int ret;

    ZKFPI_Init();
    ret = ZKFPI_Select(0);
    ASSERT(ret==0);

    ret = ZKFPI_Open();
    ASSERT(ret==0);

    while (1)
    {
        ret = ZKFPI_GetImage(buf, CAM_BUF_SZ);
        ASSERT(ret==0);

        char tmp[64] = {"---\n"};
        for (i=0; i<16; i++)
            sprintf(&tmp[strlen(tmp)], "%02x ", buf[i]);
        LOGD("%s", tmp);
    }

    ret = ZKFPI_Close();
    ASSERT(ret==0);
#else
    init_usb_sync(NULL);
    free_usb();
#endif

    return 0;
}
#endif
jint
jni_zkfpi_init(JNIEnv *env, jobject thiz) {
    LOGD("%s", __FUNCTION__);

    return ZKFPI_Init();
}

jint
jni_zkfpi_select(JNIEnv *env, jobject thiz, jint i) {
    LOGD("%s", __FUNCTION__);

    return ZKFPI_Select(i);
}

jint
jni_zkfpi_open(JNIEnv *env, jobject thiz) {
    LOGD("%s", __FUNCTION__);

    return ZKFPI_Open();
}

jint
jni_zkfpi_close(JNIEnv *env, jobject thiz) {
    LOGD("%s", __FUNCTION__);

    return ZKFPI_Close();
}

//static int _refs=0;
jint
jni_zkfpi_get_image(JNIEnv *env, jobject thiz, jbyteArray array) {
    //LOGD("%s ref=%d", __FUNCTION__, _refs++);

    int ret=-1;
    
    jbyte* jbuf = env->GetByteArrayElements(array, NULL);
	if(NULL!=jbuf){
		jsize length = env->GetArrayLength(array);
		ret = ZKFPI_GetImage((unsigned char*)jbuf, length);
	    env->ReleaseByteArrayElements(array, jbuf, 0/*JNI_COMMIT*/);
	}

    return ret;
}

static JNINativeMethod methods[] = {
  {"zkfpi_init", "()I", (void*)jni_zkfpi_init },
  {"zkfpi_select", "(I)I", (void*)jni_zkfpi_select },
  {"zkfpi_open", "()I", (void*)jni_zkfpi_open },
  {"zkfpi_close", "()I", (void*)jni_zkfpi_close },
  {"zkfpi_get_image", "([B)I", (void*)jni_zkfpi_get_image },
};

/*
 * Register several native methods for one class.
 */
static int registerNativeMethods(JNIEnv* env, const char* className,
    JNINativeMethod* gMethods, int numMethods)
{
    jclass clazz;

    clazz = env->FindClass(className);
    if (clazz == NULL) {
        LOGD("Native registration unable to find class '%s'", className);
        return JNI_FALSE;
    }
    if (env->RegisterNatives(clazz, gMethods, numMethods) < 0) {
        LOGD("RegisterNatives failed for '%s'", className);
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

/*
 * Register native methods for all classes we know about.
 *
 * returns JNI_TRUE on success.
 */
static int registerNatives(JNIEnv* env)
{
  if (!registerNativeMethods(env, myClassName,
                 methods, sizeof(methods) / sizeof(methods[0]))) {
    return JNI_FALSE;
  }

  return JNI_TRUE;
}


// ----------------------------------------------------------------------------

/*
 * This is called by the VM when the shared library is first loaded.
 */
 
typedef union {
    JNIEnv* env;
    void* venv;
} UnionJNIEnvToVoid;

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    jint result = -1;
    JNIEnv* env = NULL;
    
    LOGD("JNI_OnLoad");

    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_4) != JNI_OK) {
        LOGD("ERROR: GetEnv failed");
        goto bail;
    }
    env = uenv.env;

    if (registerNatives(env) != JNI_TRUE) {
        LOGD("ERROR: registerNatives failed");
        goto bail;
    }

    result = JNI_VERSION_1_4;
    
bail:
    return result;
}
