#ifndef __ZKFPI_H__
#define __ZKFPI_H__

#include "log.h"
#include <errno.h>

#define TRACE LOGD
#define ETRANSFER_TIMEDOUT	116		//传输超时

#define GPIO_GLED			0		//绿色指示灯
#define GPIO_RLED			1		//红色指示灯
#define GPIO_BEEP			2		//蜂鸣器
#define GPIO_BLED			3		//指纹头背光
#define GPIO_KEY1			4		//按键1
#define GPIO_KEY2			5		//按键2
#define GPIO_F1				16		//左拇指
#define GPIO_F2				17		//左食指
#define GPIO_F3				18		//左中指
#define GPIO_F4				19		//左无名指
#define GPIO_F5				20		//左小指
#define GPIO_F6				21		//右拇指
#define GPIO_F7				22		//右食指
#define GPIO_F8				23		//右中指
#define GPIO_F9				24		//右无名指
#define GPIO_F10			25		//右小指
#define GPIO_M1				26		//左手
#define GPIO_M2				27		//右手
#define GPIO_M3				28		//双手拇指
#define GPIO_M4				29		//单指滚动

#define GPIO_ON				1		//打开/有效/亮
#define GPIO_OFF			0		//关闭/无效/灭

#ifdef __cplusplus
extern "C"
{
#endif
	int ZKFPI_Init();
	int ZKFPI_Select(int index);

	int ZKFPI_Open ();
	int ZKFPI_Close();

	int ZKFPI_GetModel (BYTE * buffer, BYTE size);
	int ZKFPI_GetVidPid(WORD * vid,    WORD * pid);
	int ZKFPI_GetType  (BYTE * type);

	int ZKFPI_GetGPIO(BYTE index, BYTE * value);
	int ZKFPI_SetGPIO(BYTE index, BYTE * value);

	int ZKFPI_ReadCamera (BYTE offset, BYTE * value);
	int ZKFPI_WriteCamera(BYTE offset, BYTE * value);

	int ZKFPI_ReadEeprom (BYTE offset, BYTE * value);
	int ZKFPI_WriteEeprom(BYTE offset, BYTE * value);

	int ZKFPI_GetImage(BYTE * buffer, UINT size);
	int ZKFPI_DetImage(BYTE * buffer, UINT size);
	int ZKFPI_ClrImage();

	int ZKFPI_GetI2C(BYTE address, BYTE offset, BYTE * value);
	int ZKFPI_SetI2C(BYTE address, BYTE offset, BYTE * value);

	int ZKFPI_GetThreshold(BYTE * low, BYTE * high);
	int ZKFPI_SetThreshold(BYTE * low, BYTE * high);

	int ZKFPI_ReadProgram (WORD offset, BYTE size, BYTE * data);
	int ZKFPI_WriteProgram(WORD offset, BYTE size, BYTE * data);
	int ZKFPI_LockProgram (BOOL lock);
#ifdef __cplusplus
}
#endif


#endif