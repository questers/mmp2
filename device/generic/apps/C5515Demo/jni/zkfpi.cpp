#include <stdio.h>
#include "usb.h"
#include "zkfpi.h"
#include "log.h"

#undef  TRACE
#define TRACE(...)
//LOGD

#define USB_VID			0x1B55
#define USB_PID			0x0830

#define CMD_INIT		0xE0	//初始化
#define CMD_SET_GPIO	0xE1	//GPIO写
#define CMD_GET_GPIO	0xE2	//GPIO读
#define CMD_SET_CMOS	0xE3	//CMOS写
#define CMD_GET_CMOS	0xE4	//CMOS读
#define CMD_GET_IMAGE	0xE5	//取图（连续方式）
#define CMD_SET_NVM		0xE6	//EEPROM写
#define CMD_GET_NVM		0xE7	//EEPROM读
#define CMD_SET_IIC		0xE8	//IIC写
#define CMD_GET_IIC		0xE9	//IIC读
#define CMD_DET_IMAGE	0xEA	//取图（探测方式）
#define CMD_CLR_IMAGE	0xEB	//清空图像
#define CMD_SET_THRD	0xEC	//设置灵敏度
#define CMD_GET_THRD	0xED	//读取灵敏度

#define CMD_GET_TYPE	0xFF	// to be changed

#define TIME_OUT		0	//指令传输超时时间
#define MAX_COUNT		16		//最大设备数

struct usb_device *devices[MAX_COUNT];
struct usb_dev_handle *device = NULL;

unsigned char DevCnt = 0;
unsigned char CurDev = 0;
unsigned char endpoint = 0;

int ZKFPI_Init()
{
	struct usb_bus *bus;
	struct usb_device *dev;
	
	usb_init();

	if(usb_find_busses() < 0)
		return 0;

	if(usb_find_devices() < 0)
		return 0;

	DevCnt = 0;
	
	for (bus = usb_get_busses(); bus; bus = bus -> next)
	{
		for (dev = bus -> devices; dev; dev = dev -> next)
		{
			if(DevCnt >= MAX_COUNT)
				break;

			if (dev -> descriptor.idVendor == USB_VID && (
				dev -> descriptor.idProduct == USB_PID ||
				dev -> descriptor.idProduct == 0x0820  ||
				dev -> descriptor.idProduct == 0x0830  ))
			{
				devices[DevCnt++] = dev;
			}
		}
	}

	TRACE("Debug: ZKFPI_Init OK ! Found %d devices !\n", DevCnt);
	return DevCnt;
}

int ZKFPI_Select(int index)
{
	if(index < 0 || index >= DevCnt)
		return -ENODEV;
	
	CurDev = (unsigned char)index;

	TRACE("Debug: ZKFPI_Select OK ! Select device %d !\n", CurDev);
	return 0;
}

int ZKFPI_Open ()
{
	int ret = -ENODEV;
	
	if(devices[CurDev] == NULL)
		return ret;

	if((device = usb_open(devices[CurDev])) != NULL)
	{
		if((ret = usb_set_configuration(device, 1)) < 0)
		{
			usb_close(device);
			return ret;
		}
		if((ret = usb_claim_interface(device, 0)) < 0)
		{
			usb_close(device);
			return ret;
		}
		if(devices[CurDev] -> config -> interface -> altsetting -> endpoint -> bmAttributes == USB_ENDPOINT_TYPE_BULK)
		//if(devices[CurDev] -> config -> interface -> altsetting -> endpoint -> bmAttributes == USB_ENDPOINT_TYPE_ISOCHRONOUS)
		{
			endpoint = devices[CurDev] -> config -> interface -> altsetting -> endpoint -> bEndpointAddress;
			TRACE("Found bulk endpoint: %02x\n", endpoint);
		}
		else
		{
			usb_release_interface(device, 0);
			usb_close(device);

			return -ENODEV;
		}
		if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT, CMD_INIT, 0, 0, NULL, 0, TIME_OUT)) < 0)
		{
			usb_release_interface(device, 0);
			usb_close(device);

			return ret;
		}

		TRACE("Debug: ZKFPI_Open OK !\n");
		return 0;
	}
	
	device = NULL;

	TRACE("Debug: ZKFPI_Open Fail !\n");
	return ret;
}

int ZKFPI_Close()
{
	if(device != NULL)
	{
		usb_release_interface(device, 0);
		usb_close(device);

		device = NULL;

		TRACE("Debug: ZKFPI_Close OK !\n");
	}

	return 0;
}

int ZKFPI_GetModel(BYTE * buffer, BYTE size)
{
	int ret = 0;

	if((ret = usb_get_string_simple(device, devices[CurDev] -> descriptor.iProduct, (char *)buffer, size)) < 0)
	{
		TRACE("Debug: ZKFPI_GetString Fail !\n");
	}
	else
	{
		TRACE("Debug: ZKFPI_GetString OK !\n");
	}

	return ret;
}

int ZKFPI_GetVidPid(WORD * vid, WORD * pid)
{
	*vid = devices[CurDev] -> descriptor.idVendor;
	*pid = devices[CurDev] -> descriptor.idProduct;

	return 0;
}

int ZKFPI_GetType(BYTE * type)
{
	int ret = 0;

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, CMD_GET_TYPE, 0, 0, (char *)type, 1, TIME_OUT)) < 0)
	{
		TRACE("Debug: ZKFPI_GetType Fail !\n");
	}
	else
	{
		TRACE("Debug: ZKFPI_GetType OK !\n");
	}

	if(ret == 1)
		ret = 0;

	return ret;
}

int ZKFPI_GetGPIO(BYTE index, BYTE * value)
{
	int ret = 0;

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, CMD_GET_GPIO, 0, index, (char *)value, 1, TIME_OUT)) < 0)
	{
		TRACE("Debug: ZKFPI_GetGPIO Fail !\n");
	}
	else
	{
		TRACE("Debug: ZKFPI_GetGPIO OK !\n");
	}

	if(ret == 1)
		ret = 0;

	return ret;
}

int ZKFPI_SetGPIO(BYTE index, BYTE * value)
{
	int ret = 0;

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT, CMD_SET_GPIO, *value, index, NULL, 0, TIME_OUT)) < 0)
	{
		TRACE("Debug: ZKFPI_SetGPIO Fail !\n");
	}
	else
	{
		TRACE("Debug: ZKFPI_SetGPIO OK !\n");
	}

	return ret;
}

int ZKFPI_ReadCamera (BYTE offset, BYTE * value)
{
	int ret = 0;

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, CMD_GET_CMOS, 0, offset, (char *)value, 2, TIME_OUT)) < 0)
	{
		TRACE("Debug: ZKFPI_ReadCamera Fail !\n");
	}
	else
	{
		TRACE("Debug: ZKFPI_ReadCamera OK !\n");
	}

	if(ret == 2)
		ret = 0;

	return ret;
}

int ZKFPI_WriteCamera(BYTE offset, BYTE * value)
{
	int ret = 0;

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT, CMD_SET_CMOS, *((int*)value), offset, NULL, 0, TIME_OUT)) < 0)
	{
		TRACE("Debug: ZKFPI_WriteCamera Fail !\n");
	}
	else
	{
		TRACE("Debug: ZKFPI_WriteCamera OK !\n");
	}

	return ret;
}

int ZKFPI_ReadEeprom (BYTE offset, BYTE * value)
{
	int ret  = 0;

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, CMD_GET_NVM, 0, offset, (char *)value, 1, TIME_OUT)) < 0)
	{
		TRACE("Debug: ZKFPI_ReadEeprom Fail !\n");
	}
	else
	{
		TRACE("Debug: ZKFPI_ReadEeprom OK !\n");
	}

	if(ret == 1)
		ret = 0;

	return ret;
}

int ZKFPI_WriteEeprom(BYTE offset, BYTE * value)
{
	int ret = 0;

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT, CMD_SET_NVM, *value, offset, NULL, 0, TIME_OUT)) < 0)
	{
		TRACE("Debug: ZKFPI_WriteEeprom Fail !\n");
	}
	else
	{
		TRACE("Debug: ZKFPI_WriteEeprom OK !\n");
	}

	return ret;
}

int ZKFPI_GetImage(BYTE * buffer, UINT size)
{
	int ret = 0;

#if 0
	void *context = NULL;

	usb_resetep(device, endpoint);
	//usb_clear_halt(device, endpoint);

	//if((ret = usb_isochronous_setup_async(device, &context, endpoint, 1024)) < 0)
	if((ret = usb_bulk_setup_async(device, &context, endpoint)) < 0)
	{
		goto Error;
	}
	
	if((ret = usb_submit_async(context, (char *)buffer, size)) < 0)
	//if((ret = usb_submit_async(context, (char *)buffer, 1024)) < 0)
	{
		usb_free_async(&context);
		goto Error;
	}

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT, CMD_GET_IMAGE, 0, 0, NULL, 0, TIME_OUT)) < 0)
	{
		usb_free_async(&context);
		goto Error;
	}

	if((ret = usb_reap_async(context, TIME_OUT)) < 0)
	{
		usb_free_async(&context);

		//if(ret = -ETRANSFER_TIMEDOUT)
		//	usb_resetep(device, endpoint);
			//usb_clear_halt(device, endpoint);

		goto Error;
	}

	usb_free_async(&context);
#else

	usb_resetep(device, endpoint);
	
	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT, CMD_GET_IMAGE, 0, 0, NULL, 0, TIME_OUT)) < 0)
	{
		goto Error;
	}

	if((ret = usb_bulk_read(device, endpoint, (char*)buffer, size, TIME_OUT)) <= 0)
	{
		if(ret = -ETRANSFER_TIMEDOUT)
			usb_resetep(device, endpoint);

		goto Error;
	}
#endif

	//TRACE("Debug: ZKFPI_GetImage OK ! Size = %d, Ret = %d\n", size, ret);

	if(ret == size)
		ret = 0;
	return ret;

Error:
	
	TRACE("Debug: ZKFPI_GetImage Fail ! Size = %d, Ret = %d\n", size, ret);
	return ret;
}

int ZKFPI_GetI2C(BYTE address, BYTE offset, BYTE * value)
{
	int ret = 0;

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, CMD_GET_IIC,  0, address | (offset << 8), (char *)value, 1, TIME_OUT)) < 0)
	{
		TRACE("Debug: ZKFPI_GetI2C Fail !\n");
		return ret;
	}
	else
	{
		TRACE("Debug: ZKFPI_GetI2C OK !\n");
	}

	if(ret == 1)
		ret = 0;

	return ret;
}

int ZKFPI_SetI2C(BYTE address, BYTE offset, BYTE * value)
{
	int ret = 0;

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT, CMD_SET_IIC,  *value, address | (offset << 8), NULL, 0, TIME_OUT)) < 0)
	{
		TRACE("Debug: ZKFPI_SetI2C Fail !\n");
		return ret;
	}
	else
	{
		TRACE("Debug: ZKFPI_SetI2C OK !\n");
	}

	return ret;
}

int ZKFPI_DetImage(BYTE * buffer, UINT size)
{
	int ret = 0;

#if 0
	void *context = NULL;

	if((ret = usb_bulk_setup_async(device, &context, endpoint)) < 0)
	{
		goto Error;
	}

	if((ret = usb_submit_async(context, (char *)buffer, size)) < 0)
	{
		usb_free_async(&context);
		goto Error;
	}

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, CMD_DET_IMAGE, 0, 0, NULL, 0, TIME_OUT)) < 0)
	{
		usb_free_async(&context);
		goto Error;
	}

	if((ret = usb_reap_async(context, TIME_OUT)) < 0)
	{
		usb_free_async(&context);
		goto Error;
	}

	usb_free_async(&context);
#else
	int status = 0;

	usb_resetep(device, endpoint);

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, CMD_DET_IMAGE, 0, 0, (char *)&status, 1, TIME_OUT)) < 0)
	{
		goto Error;
	}

	if(status == 1)
	{
		ret = usb_bulk_read(device, endpoint, (char *)buffer, size, TIME_OUT);
	}
	else
	{
		ret = -EFAULT;
		goto Error;
	}
#endif

	if(ret == size)
		ret = 0;

	TRACE("Debug: ZKFPI_DetImage OK !\n");
	return ret;

Error:
	TRACE("Debug: ZKFPI_DetImage Fail !\n");
	return ret;
}

int ZKFPI_ClrImage()
{
	int ret = 0;

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT, CMD_CLR_IMAGE, 0, 0, NULL, 0, TIME_OUT)) < 0)
	{
		TRACE("Debug: ZKFPI_ClrImage Fail !\n");
	}
	else
	{
		TRACE("Debug: ZKFPI_ClrImage OK !\n");
	}

	return ret;
}

int ZKFPI_GetThreshold(BYTE * low, BYTE * high)
{
	int ret = 0;
	int value = 0;

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, CMD_GET_THRD,  0, 0, (char *)&value, 2, TIME_OUT)) < 0)
	{
		TRACE("Debug: ZKFPI_GetThreshold Fail !\n");
		return ret;
	}
	else
	{
		TRACE("Debug: ZKFPI_GetThreshold OK !\n");
	}

	*high = value / 256;
	*low  = value % 256;

	if(ret == 2)
		ret = 0;

	return ret;
}

int ZKFPI_SetThreshold(BYTE * low, BYTE * high)
{
	int ret = 0;
	int value;
	
	value = (*high) * 256 + (*low) % 256;

	if((ret = usb_control_msg(device, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT, CMD_SET_THRD,  value, 0, NULL, 0, TIME_OUT)) < 0)
	{
		TRACE("Debug: ZKFPI_SetThreshold Fail !\n");
		return ret;
	}
	else
	{
		TRACE("Debug: ZKFPI_SetThreshold OK !\n");
	}

	return ret;
}

//void test()
//{
//	BYTE tmp, i;
//	BYTE buf[307200];
//
//	if(ZKFPI_Init() < 0) return;
//
//	ZKFPI_Select(0);
//
//	if(ZKFPI_Open() < 0) return;
//
//	//Sleep(100);
//
//	//ZKFPI_GetModel(buf, 64);
//	//TRACE("\tDevice model: %s\n", buf);
//
//	i   = 0x1d;
//	tmp = 0;
//	ZKFPI_SetGPIO(i, &tmp);
//	ZKFPI_GetGPIO(i, &tmp);
//	TRACE("\tSet 0, Get %d\n", tmp);
//	
//	tmp = 1;
//	ZKFPI_SetGPIO(i, &tmp);
//	ZKFPI_GetGPIO(i, &tmp);
//	TRACE("\tSet 1, Get %d\n", tmp);
//
//	//for(i = 0; i < 254; i++)
//	//{
//	//	tmp = i;
//	//	ZKFPI_WriteEeprom(i, &tmp);
//	//	//ZKFPI_ReadEeprom(i, &tmp);
//	//	TRACE("\tWrite 0x%02X, Read 0x%02X\n", i, tmp);
//	//}
//	Sleep(5);
//	//for(i = 0; i < 254; i++)
//	//{
//	//	tmp = 0;
//	//	//Sleep(5);
//	//	//ZKFPI_WriteEeprom(i, &tmp);
//	//	ZKFPI_ReadEeprom(i, &tmp);
//	//	TRACE("\tWrite 0x%02X, Read 0x%02X\n", i, tmp);
//	//}
//
//	//tmp = 0x24;
//	//Sleep(50);	//最少延迟1帧时间 50ms
//	//ZKFPI_WriteCamera(0x97, &tmp);
//	//Sleep(50);	//最少延迟1帧时间 50ms
//	//ZKFPI_ReadCamera(0x97, &tmp);
//	//TRACE("\tWrite 0x24, Read 0x%02X\n", tmp);
//
//	//tmp = 0xe0;
//	//Sleep(50);	//最少延迟1帧时间 50ms
//	//ZKFPI_WriteCamera(0x97, &tmp);
//	//Sleep(50);	//最少延迟1帧时间 50ms
//	//ZKFPI_ReadCamera(0x97, &tmp);
//	//TRACE("\tWrite 0xE0, Read 0x%02X\n", tmp);
//
//	//tmp = 0x63;
//	//ZKFPI_SetI2C(0xA2, 0x05, &tmp);
//	//ZKFPI_GetI2C(0xA2, 0x05, &tmp);
//	//TRACE("\tSet 0xA2, 0x05, 0x63, Get 0xA2, 0x05, 0x%02X\n", tmp);
//
//	//tmp = 0x27;
//	//ZKFPI_SetI2C(0xA2, 0x05, &tmp);
//	//ZKFPI_GetI2C(0xA2, 0x05, &tmp);
//	//TRACE("\tSet 0xA2, 0x05, 0x27, Get 0xA2, 0x05, 0x%02X\n", tmp);
//
//	////Sleep(50);
//	////ZKFPI_GetImage(buf, 307200);
//
//	//for(i = 0; i < 64; i++)
//	//buf[i] = i;
//	//ZKFPI_WriteProgram(0, 64, buf);
//	//ZKFPI_LockProgram(0);
//	//for(i = 0; i < 64; i++)
//	//buf[i] = 0;
//	//ZKFPI_ReadProgram(0, 64, buf);
//	//TRACE("%X%X%X%X%X%X%X%X%X%X%X%X%X%X%X%X\n", buf);
//	//ZKFPI_LockProgram(1);
////
//	ZKFPI_Close();
//}
