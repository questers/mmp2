package com.quester.cam;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import android.util.Log;

public class Util {

	public static final String TAG = "usbcam";
	public static final String FILE_PATH = "/sys/class/gpio/gpio104/value";
	
	public static byte[] stream2Bytes(InputStream iStream) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buf = new byte[256];
		int len = 0;
		try {
			while ((len = iStream.read(buf, 0, buf.length)) != -1) {
				baos.write(buf, 0, len);
			}
		} catch (IOException e) {
			Log.w(TAG, "Stream to bytes failed!");
		}
		return baos.toByteArray();
	}

	public static void openDevicePower() {
		try {
			BufferedWriter bw104 = new BufferedWriter(new FileWriter(new File(FILE_PATH)));
			bw104.write("1");
			bw104.flush();
			bw104.close();
		} catch (IOException e) {
			Log.w(TAG, "Open fingerprint exception!");
		}
	}

	public static void closeDevicePower() {
		try {
			BufferedWriter bw104 = new BufferedWriter(new FileWriter(new File(FILE_PATH)));
			bw104.write("0");
			bw104.flush();
			bw104.close();
		} catch (IOException e) {
			Log.w(TAG, "Close fingerprint exception!");
		}
	}
	
}
