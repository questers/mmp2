package com.quester.cam;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Cam extends Activity  {
	
	public static ImageView mImageView;	
	public static TextView mTextView;
	
	private CamView mCamView;
	private Context mContext;
	private WakeLock mWakeLock;
	private boolean firstInit;
	private Thread devInitThrd;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mContext = this;
		firstInit = true;
        mTextView = (TextView)findViewById(R.id.my_mesg);
        mImageView = (ImageView)findViewById(R.id.my_fingerprint);

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "FingerprintObserver");
		mWakeLock.setReferenceCounted(false);
    }

	private void goToWork() {
		runOnUiThread(new Runnable() {
			public void run() {
				mCamView = new CamView(mContext);
				if (!mWakeLock.isHeld()) mWakeLock.acquire();
			}
		});
	}

	protected void onResume() {
		super.onResume();
		Util.openDevicePower();
		if (firstInit) {
			firstInit = false;
		} else {
			mTextView.setText("Restart..");
			mTextView.setVisibility(View.VISIBLE);
			mImageView.setImageResource(R.drawable.bmp_model);
			mImageView.setVisibility(View.GONE);
		}
		devInitThrd = new Thread(new Runnable() {
			public void run() {
				try {
					//This delay used to find device and init
					Thread.sleep(3000);
					goToWork();
				} catch (InterruptedException e) {
				}
			}
		});
		devInitThrd.start();
	}
    
    protected void onPause() {
    	super.onPause();
		if (!devInitThrd.isInterrupted()) devInitThrd.interrupt();
    	if (mCamView != null) {
			mCamView.closeDevice();
			mCamView = null;
		} else {
			Util.closeDevicePower();
		}
		if (mWakeLock != null) {
			if (mWakeLock.isHeld()) mWakeLock.release();
		}
    }

	protected void onDestroy() {
		super.onDestroy();
		if (mWakeLock != null) mWakeLock = null;
	}
    
}
