package com.quester.cam;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import java.io.IOException;
import java.io.InputStream;

public class CamView {
	
	static {
		System.loadLibrary("usbcam");
    }
	
	private native int zkfpi_init();
    private native int zkfpi_select(int i);
    private native int zkfpi_open();
    private native int zkfpi_close();
    private native int zkfpi_get_image(byte[] buf);
	
    private final int WIDTH = 320;
	private final int HEIGHT = 480;
	
	/** fingerprint data, if you want to save the picture and you can storage by this */
	private byte[] rawBuf;
	private boolean mCaptureTrdDone;
	private Handler mUiHandler;
	private Thread mCaptureThread;
	private AsyncHandler mAsyncHandler;
	private boolean mFirstCapture;

	public void closeDevice() {
		mAsyncHandler.close();
		new Thread(new Runnable() {
			public void run() {
				int delay = 0;
				while (!mCaptureTrdDone) {
					try {
						Thread.sleep(100);
						if (++delay==10 && !mCaptureThread.isInterrupted()) {
							mCaptureThread.interrupt();
							zkfpi_close();
							mCaptureTrdDone = true;
						}
					} catch (InterruptedException e) {
						//
					}
				}
				Util.closeDevicePower();
			}
		}).start();
	}
	
	public CamView(Context context) {
		try {
			InputStream iStream = context.getResources().openRawResource(
					R.drawable.bmp_model);
			rawBuf = Util.stream2Bytes(iStream);
			iStream.close();
		} catch (IOException e) {
			Log.w(Util.TAG, "Opening bmp model failed.");
		}
		mUiHandler = new Handler();
		initCapture();
		mFirstCapture = true;
	}

	private void initCapture() {
		mAsyncHandler = new AsyncHandler();
        mCaptureThread = new Thread(mAsyncHandler);
		mCaptureThread.start();
    }

    private void postConnectError() {
		mCaptureTrdDone = true;
        mUiHandler.post(new Runnable() {
            public void run() {
				Cam.mTextView.setVisibility(View.VISIBLE);
				Cam.mImageView.setVisibility(View.GONE);
				Cam.mTextView.setText("Connect device failed!");
            }
        });
    }

	private void postStandby() {
		mCaptureTrdDone = false;
		Cam.mTextView.setVisibility(View.GONE);
		Cam.mImageView.setVisibility(View.VISIBLE);
	}

	private synchronized void updateCanvas(final byte[] imgBuf) {
		mUiHandler.post(new Runnable() {
			public void run() {
				mAsyncHandler.release();
				System.arraycopy(imgBuf, 0, rawBuf, rawBuf.length - imgBuf.length, imgBuf.length);
				Cam.mImageView.setImageBitmap(BitmapFactory.decodeByteArray(rawBuf, 0, rawBuf.length));
				
				if (mFirstCapture) {
					mFirstCapture = false;
					postStandby();
				}
			}
		});
    }
	
	private class AsyncHandler implements Runnable {
		private volatile boolean capture = false;
		
		public AsyncHandler() {
			capture = true;
		}
		
		public synchronized void release() {
			this.notify();
		}
		
		public synchronized void close() {
			capture = false;
			release();
		}
		
		private synchronized void takePicture() {
			int ret = zkfpi_init();
            if (ret <= 0) {
				try {
					/* This delay used to reconnect device */
					Thread.sleep(1500);
					ret = zkfpi_init();
					if (ret <= 0) {
						Thread.sleep(1500);
						ret = zkfpi_init();
						if (ret <= 0) {
							Log.w(Util.TAG, "No usb fingerprint scaner found. Abort.");
							postConnectError();
							return;
						}
					}
				} catch (InterruptedException e) {
					//
				}
            }

            zkfpi_select(0);
            ret = zkfpi_open();
            if (ret != 0) {
                Log.w(Util.TAG, "Opening usb fingerprint scanner failed. Abort.");
                postConnectError();
                return;
            }

			byte[] mImgBuf = new byte[WIDTH*HEIGHT];
            while (capture) {
                if (zkfpi_get_image(mImgBuf) == 0) {
                    updateCanvas(mImgBuf);
                    try {
                    	this.wait();
                    } catch (InterruptedException e) {
                    	//
                    }
				} else {
					Log.w(Util.TAG, "Obtaining image failed!");
				}
            }
            
            zkfpi_close();
			mCaptureTrdDone = true;
		}
		
		public void run() {
			takePicture();
        }
		
	}

}
