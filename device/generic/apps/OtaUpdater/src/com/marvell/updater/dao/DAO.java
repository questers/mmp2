package com.marvell.updater.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.marvell.updater.DBHelper.DBHelper;
import com.marvell.updater.entity.DownloadInfo;

public class DAO {

	private DBHelper dbHelper;
	
	public DAO(Context context){
		
		dbHelper = new DBHelper(context);
	}
	
	public boolean isHasInfos(String urlStr){
		
		SQLiteDatabase database = dbHelper.getReadableDatabase();
		String sql = "select count(thread_id) from download_info where url=?";
		Cursor cursor = database.rawQuery(sql, new String[] {urlStr});
		int count = 0;
		if(cursor.moveToNext()){
			count = cursor.getInt(0);
		}
		/*cursor.moveToFirst();
		int count = cursor.getInt(0);*/
		cursor.close();
		System.out.println("count == " + count);
		return count > 0;
	}
	
	public void saveInfos(List<DownloadInfo> infos){
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		for(DownloadInfo info : infos){
			String sql = "insert into download_info(thread_id,start_pos," +
					"end_pos,compelete_size,url) values(?,?,?,?,?)";
			Object[] bindArgs = {info.getThreadId(),info.getStartPos(),info.getEndPos(),
					info.getCompeleteSize(),info.getUrl()};
			database.execSQL(sql, bindArgs);
		}
	}
	
	public List<DownloadInfo> getInfos(String urlStr){
		
		List<DownloadInfo> list = new ArrayList<DownloadInfo>();
		SQLiteDatabase database = dbHelper.getReadableDatabase();
		String sql = "select thread_id,start_pos,end_pos,compelete_size,url from download_info where url=?";
		Cursor cursor = database.rawQuery(sql, new String[] {urlStr});
		while(cursor.moveToNext()){
			DownloadInfo info = new DownloadInfo(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2),
					cursor.getInt(3), cursor.getString(4));
			list.add(info);
		}
		cursor.close();
		return list;
	}
	
	public void updateInfos(int threadId, int compeleteSize, String urlStr){
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		String sql = "update download_info set compelete_size=? where thread_id=? and url=?";
		Object[] bindArgs = {compeleteSize, threadId, urlStr};
		database.execSQL(sql, bindArgs);
	}
	
	public void closeDB(){
		
		dbHelper.close();
	}
	
	public void deleteById(int threadId){
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		database.delete("download_info", "thread_id=?", new String[] {threadId+""});
		database.close();
	}
	
	public void deleteByUrl(String url){
		
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		database.delete("download_info", "url=?", new String[] {url});
		database.close();
	}
}
