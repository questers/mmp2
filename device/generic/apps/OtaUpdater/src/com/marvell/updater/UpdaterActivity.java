package com.marvell.updater;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.marvell.updater.entity.BaseOtaPackage;
import com.marvell.updater.entity.BasePackageInfo;
import com.marvell.updater.model.DataModel;
import com.marvell.updater.net.ScanThread;
import com.marvell.updater.utils.Constants;

public class UpdaterActivity extends PreferenceActivity implements
	ScanThread.ScanListener {

    public static final String TAG = "UpdateActivity";

    public static final int FIND_UPDATE = 1;

    public static final int UNFIND_UPDATE = 2;

    public static final int NETWORK_ERROR = 3;
    
    public static final int SYSTEM_ERROR = 4;

    private static final String KEY_CURRENT_VERSION = "current_version";
    
    private static final String KEY_SCAN_UPDATE = "scan_update";
    
    private static final String KEY_NETWORK_SETTING = "network_setting";
    
    private ProgressDialog mProgress;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

	super.onCreate(savedInstanceState);
	addPreferencesFromResource(R.xml.system_update);
	PreferenceScreen screen = getPreferenceScreen();
	Preference currentVersion = screen.findPreference(KEY_CURRENT_VERSION);
	currentVersion.setSummary(Build.VERSION.RELEASE);
    }
    
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
            Preference preference) {
	
	if (KEY_SCAN_UPDATE.equals(preference.getKey())) {
	    SharedPreferences preferences = getSharedPreferences(
		    Constants.NETWORK_SETTING, Context.MODE_PRIVATE);
		String defaultUrl = SystemProperties.get("ro.system.update_url", Constants.URL);
	    String url = preferences.getString(Constants.KEY_SERVER, defaultUrl);
	    ScanThread scanThread = new ScanThread(url
		    + Constants.GET_SYSTEM_VERSION);
	    scanThread.setScanListenerListener(UpdaterActivity.this);
	    scanThread.start();
	    mProgress = new ProgressDialog(UpdaterActivity.this);
	    mProgress.setMessage(getString(R.string.scanning));
	    mProgress.show();
	}
	
	return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

	MenuInflater inflater = getMenuInflater();
	inflater.inflate(R.menu.option_menu, menu);
	return true;
	
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

	switch (item.getItemId()) {
	case R.id.network:
	    updateNetwork();
	}
	return super.onOptionsItemSelected(item);

    }

    public void onUpdateCheck(BaseOtaPackage otaPackage) {

	DataModel.getInstance().mOtaPackage = otaPackage;
	
	if (Constants.CODE_SUCCESS.equals(otaPackage.mCode)) {
	    mHandler.sendEmptyMessage(FIND_UPDATE);
	} else if (Constants.CODE_NO_UPDATE.equals(otaPackage.mCode)) {
	    mHandler.sendEmptyMessage(UNFIND_UPDATE);
	} else {
	    mHandler.sendEmptyMessage(SYSTEM_ERROR);
	}

    }

    public void onNetworkException() {
	mHandler.sendEmptyMessage(NETWORK_ERROR);
    }
    
    private void updateNetwork() {
	    AlertDialog.Builder networkDialog = new AlertDialog.Builder(
		    UpdaterActivity.this);
	    LinearLayout layout = (LinearLayout) LayoutInflater.from(this)
		    .inflate(R.layout.network_setting, null, false);
	    networkDialog.setTitle(R.string.network_title);
	    networkDialog.setView(layout);
	    final SharedPreferences preferences = getSharedPreferences(
		    Constants.NETWORK_SETTING, Context.MODE_PRIVATE);
	    String defaultUrl = SystemProperties.get("ro.system.update_url", Constants.URL);
	    String url = preferences.getString(Constants.KEY_SERVER, defaultUrl);
	    final EditText editText = (EditText) layout
		    .findViewById(R.id.server);
	    editText.setText(url);
	    networkDialog.setPositiveButton(R.string.ok,
		    new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dia, int which) {
			    String url = editText.getText().toString();
			    SharedPreferences.Editor edit = preferences.edit();
			    edit.putString(Constants.KEY_SERVER, url);
			    edit.commit();
			}
		    });

	    networkDialog.setNegativeButton(R.string.cancel, null);
	    networkDialog.show();
    }

    private Handler mHandler = new Handler() {

	@Override
	public void handleMessage(Message msg) {

	    if (mProgress != null) {
		mProgress.dismiss();
	    }
	    
	    switch (msg.what) {

	    case FIND_UPDATE:
		
		Intent intent = new Intent(UpdaterActivity.this, ModelListActivity.class);
		startActivity(intent);
		
		break;

	    case UNFIND_UPDATE:
		
		Toast.makeText(UpdaterActivity.this, R.string.unfind_update,
			Toast.LENGTH_SHORT).show();
		break;
		
	    case SYSTEM_ERROR:
		
		Toast.makeText(UpdaterActivity.this, R.string.unfind_update,
			Toast.LENGTH_SHORT).show();
		break;

	    case NETWORK_ERROR:
		
		AlertDialog.Builder dialog = new AlertDialog.Builder(
			UpdaterActivity.this);
		dialog.setMessage(R.string.network_error);
		dialog.setNeutralButton(R.string.ok, null);
		dialog.show();
		break;
		
	    }
	}

    };

}