package com.marvell.updater;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemProperties;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.marvell.updater.utils.Constants;

public class RebootConfirmActivity extends Activity {

    String path;
    private static final String TAG = "ConfirmActivity";

    @Override
        public void onCreate(Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);

            setContentView(R.layout.update_confirm);

            Intent intent = getIntent();
            path = intent.getStringExtra(Constants.KEY_OTA_PATH);

            if (Constants.LOG_ON) Log.d(TAG, "ota package path = " + path);


            findViewById(R.id.confirm_yes).setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    /* do some conversion on path, so recovery understands it */
                    int index = path.lastIndexOf("/");
                    String prefix = path.substring(0, index);
                    String postfix = path.substring(index);
                    String recoveryPath = prefix;

                    if (SystemProperties.getBoolean("ro.have.internalstorage", true))
                    {
                        //if (prefix.equals("/mnt/sdcard"))
                        //    recoveryPath = "/mnt/internalstorage";
                        if (prefix.equals("/mnt/sdcard"))
                            recoveryPath = "/mnt/sdcard";
                        path = recoveryPath+postfix;
                    }
                    Log.d(TAG, "real path: "+path);

                    Intent intent = new Intent("com.marvell.intent.OTA_UPDATE");
                    intent.putExtra(Constants.KEY_OTA_PATH, path);
                    sendBroadcast(intent);
                    if (Constants.LOG_ON) Log.d(TAG, "broadcast has been sent out, system will reboot");
                    finish();
                }
            });

            findViewById(R.id.confirm_later).setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {

                    AlarmManager am = (AlarmManager) RebootConfirmActivity.this.getSystemService(Context.ALARM_SERVICE);
                    Intent intent = new Intent("com.marvell.intent.UPDATE_CONFIRM");
                    intent.putExtra(Constants.KEY_OTA_PATH, path);
                    PendingIntent contentIntent = PendingIntent.getActivity(RebootConfirmActivity.this, 0,
                        intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    am.cancel(contentIntent);
                    am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 
                        + Constants.CONFIRM_DELAY, contentIntent);
                    finish();
                }

            });

        }

}
