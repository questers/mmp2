package com.marvell.updater.DBHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	
	public DBHelper(Context context){
		
		super(context, "otadownload.db", null, DATABASE_VERSION);
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE download_info(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"thread_id INTEGER,start_pos INTEGER,end_pos INTEGER,compelete_size INTEGER,url CHAR)");
	}
 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS download_info");
		onCreate(db);
	}
	
}
