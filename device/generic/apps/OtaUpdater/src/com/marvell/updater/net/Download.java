package com.marvell.updater.net;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.marvell.updater.dao.DAO;
import com.marvell.updater.entity.DownloadInfo;
import com.marvell.updater.utils.Constants;

import android.os.Environment;
import android.util.Log;
import android.content.Context;

public class Download {

    public static final String TAG = "DownloadThread";

    private int threadCount = Constants.THREAD_COUNT;
    private String mUrl; 
    private String mDate; 
    private int fileSize;
    private String localPath;
    private String finalPath;
    private DAO dao;
    private File mFile;
    private int downloadSize;
    private List<DownloadInfo> infos;
    private int mLastPercent = -1;
    private long mStartTime;
    private int downloaded = 0;

    private DownloadListener mListener;

    private boolean mCancel = false;

    public Download(String url, String date, Context context) {     
	    
    	try {
	    	mUrl = encode(url, "UTF-8");
	    } catch (Exception e1) {
	        e1.printStackTrace();
	        mUrl = url;
	    }
	    dao = new DAO(context);
	    
	    mDate = date;
    }

    private void updatePercent(int readLength) {

        downloadSize += readLength;
        downloaded += readLength;
        int percentNow = (int)((double)downloadSize * 100 / fileSize);
        if (percentNow != mLastPercent) {
            mLastPercent = percentNow;
            if (mListener != null) {
                mListener.onDownloadPercent(percentNow);
            }
        }

        if(downloadSize == fileSize){
            Log.d(TAG, "Download complete. Rename to "+finalPath);
            File f = new File(finalPath);
            mFile.renameTo(f);
            
        	dao.deleteByUrl(mUrl);
            if (mListener != null) {
                mListener.onDownloadFinish(finalPath);
            }

            long elapsed = System.currentTimeMillis()-mStartTime;
            Log.d(TAG, String.format("Elapsed time: %dms, speed: %dkB/s", 
                        elapsed,
                        (long)downloaded*1000/(elapsed*1024)));
        }
    }

    public void cancel() {
    
        if (Constants.LOG_ON) Log.d(TAG, "cancel thread");
            
            mCancel = true;
        }

    public static HttpURLConnection openUrl(String urlStr){
        
        URL urlURL = null;
        HttpURLConnection httpConn = null;
        try{
            urlURL = new URL(urlStr);
            httpConn = (HttpURLConnection) urlURL.openConnection();
            httpConn.setDoInput(true);
            
        }catch(NullPointerException npe){
            npe.printStackTrace();
        }catch(MalformedURLException mue){
            mue.printStackTrace();
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
        return httpConn;
    }
    
    public static int connect(HttpURLConnection httpConn){
        
        int code = -1;
        if(httpConn!=null){
            try{
                httpConn.connect();
                code = httpConn.getResponseCode();
            }catch(IOException ioe){
                ioe.printStackTrace();
            }
        }
        return code;
    }

    public boolean tryCreateFile(String path, int size)
    {
        boolean ok = true;
        String storageDir = path;
        localPath = storageDir + File.separator + mUrl.replaceAll("[^a-zA-Z_0-9]", "_");
        finalPath = storageDir + File.separator + mDate + ".zip";

        try {
            long start = System.currentTimeMillis();
            mFile = new File(localPath);
            if(!mFile.exists()){
                mFile.createNewFile();
            }

            RandomAccessFile accessFile = new RandomAccessFile(mFile, "rws");
            accessFile.setLength(size);
            accessFile.close();

            long end = System.currentTimeMillis();
            Log.d(TAG, String.format("%dms taken to create %s", end-start, localPath));
        } catch (IOException e) {
            Log.e(TAG, "exception: "+e);
            ok = false;
        }

        return ok;
    }
    
    public boolean init(){
        boolean ok = true;

        mStartTime = System.currentTimeMillis();
        downloaded = 0;
        if (mListener != null) {
            mListener.onDownloadStarted();
        }

    	try {
			HttpURLConnection httpConn = openUrl(mUrl);
    		int respondCode = connect(httpConn);
            if(respondCode == HttpURLConnection.HTTP_OK){
                fileSize = httpConn.getContentLength();
                Log.d(TAG, "file size: "+fileSize);
                updatePercent(0);

                String storageDir = Environment.getExternalStorageDirectory().toString();
                boolean mount = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
                if (mount)
                {
                    boolean ret = tryCreateFile(Environment.getExternalStorageDirectory().toString(), fileSize);
                    if (!ret)
                    {
                        Log.d(TAG, "Failed create temp file in sdcard, try external sdcard...");
                        storageDir = Constants.TFLASH;
                        ret = tryCreateFile(Constants.TFLASH, fileSize);
                        if (ret)
                        {
                            Log.d(TAG, "OK");
                            ok = true;
                        }
                        else
                        {
                            Log.d(TAG, "Failed create temp file in external sdcard, abort");
                            ok = false;

                            if (mListener != null) mListener.onNotifyIOError(storageDir);
                        }
                    }
                }
                else
                {
                    Log.d(TAG, "SD card (/mnt/sdcard) not mounted, abort");
                    ok = false;
                    if (mListener != null) mListener.onNotifyIOError(storageDir);
                }

                httpConn.disconnect();
            }else{
    			Log.e(TAG, "Cann't connect to Server");
                if (mListener != null) {
                    mListener.onNetworkException();
                }
    		}
		}catch(Exception e) {
			e.printStackTrace();
			Log.d(TAG, "Unknown error, abort");
			ok = false;
		}

		if (!ok) return ok;
    	
    	if(dao.isHasInfos(mUrl)){
    		infos = dao.getInfos(mUrl);
    		int completeSize = 0;
    		for(DownloadInfo info : infos){
    			completeSize += info.getCompeleteSize();
    		}
    		downloadSize = completeSize;
    		
    	}else{
    		infos = new ArrayList<DownloadInfo>();
    		int block = fileSize / threadCount;
    		for(int i=0;i<threadCount-1;i++){
    			DownloadInfo info = new DownloadInfo(i, i*block, (i+1)*block-1, 0, mUrl);
    			infos.add(info);	
    		}
    		DownloadInfo info = new DownloadInfo(threadCount-1, (threadCount-1)*block, fileSize-1, 0, mUrl);
    		infos.add(info);
    		dao.saveInfos(infos);
    		downloadSize = 0;
    	}

    	updatePercent(0);

    	return ok;
    }
    
    public void download(){
        if (!init()) {
            if (mListener != null)
                mListener.onDownloadAbort();
            return;
        }

        if(infos!=null){
            for(DownloadInfo info : infos){
                new DownloadThread(info.getThreadId(), info.getStartPos(), info.getEndPos(),
                        info.getCompeleteSize(), info.getUrl()).start();
                Log.d(TAG, "Start thread "+info.getThreadId());
            }
        }
    }
    
    public class DownloadThread extends Thread{
    	
    	private int threadId;
    	private int startPos;
    	private int endPos;
    	private int completeSize;
    	private String url;
    	
    	public DownloadThread(int threadId, int startPos, int endPos, int completeSize, String url){
    		this.threadId = threadId;
    		this.startPos = startPos;
    		this.endPos = endPos;
    		this.completeSize = completeSize;
    		this.url = url;
    	}
    	
    	 public void run() {

	        HttpURLConnection httpConn = null;
	        RandomAccessFile randomAccessFile = null;
	        InputStream is = null;
	        int dataBlockLength = Constants.DOWNLOAD_BLOCK_SIZE;
	        byte[] data = new byte[10*1024];
	        byte[] accumulated = new byte[dataBlockLength];
	        int readLength = -1;
	        boolean done = false;
	        while (!done && !mCancel) {
    	        try {
    	        	randomAccessFile = new RandomAccessFile(mFile, "rws");
    	        	randomAccessFile.seek(startPos + completeSize);
    	        	httpConn = openUrl(url);
    	        	httpConn.setRequestProperty("Range", "bytes="+(startPos+completeSize)+"-"+endPos);
    	        	int responseCode = connect(httpConn);
    	        	if(responseCode == HttpURLConnection.HTTP_OK ||
    	        		responseCode == HttpURLConnection.HTTP_PARTIAL){
    	        		is = httpConn.getInputStream();

    	        		int len = 0;
    	        		while (((readLength=is.read(data))!= -1 || len > 0) && !mCancel){
    	        		    if (readLength > 0)
                            {
                                System.arraycopy(data, 0, accumulated, len, readLength);
                                len += readLength;
                            }
                            else
                            {
    	        		        Log.d(TAG, String.format("Thread %d reach the end", threadId));
                            }

                            if (len > dataBlockLength/2 || readLength < 0)
                            {
    	        		        Log.d(TAG, String.format("Thread %d write %d bytes to storage", threadId, len));
                                randomAccessFile.write(accumulated, 0, len);
                                completeSize += len;
                                dao.updateInfos(threadId, completeSize, url);
                                updatePercent(len);
                                len = 0;
                            }

    	        			if(mCancel) {
    	        				mFile.delete();
        	        			dao.deleteById(threadId);
    	        				interrupt();
        	        		}
    	        		}

    	        		done = true;
    	        	}else {
	                    Log.e(TAG, "Cann't connect to Server");
	                    if (mListener != null) {
	                        mListener.onNetworkException();
	                    }
	                }
    	        } catch (Exception e) {
	                Log.d(TAG, "exception: "+e);
	                try {
	                    sleep(60*1000);
	                } catch (Exception error) {
	                    /* nothing done */
	                }
	                e.printStackTrace();
	                mListener.onNetworkException();
	            } finally {
	                try {
	                    if (is != null) {
	                        is.close();
	                    }
	                    if (randomAccessFile != null){
	                    	randomAccessFile.close();
	                    	randomAccessFile = null;
	                    }
	                    if (httpConn != null) {
	                        httpConn.disconnect();
	                    }
	                    dao.closeDB();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
    	 } 
   }
    
    private static String zhPattern = "[\u4E00-\u9FA5]";

    public String encode(String str, String charset)
        throws UnsupportedEncodingException {
        
        Pattern p = Pattern.compile(zhPattern);
        Matcher m = p.matcher(str);
        StringBuffer b = new StringBuffer();
        while (m.find()) {
            m.appendReplacement(b, URLEncoder.encode(m.group(0), charset));
        }
        m.appendTail(b);
        return b.toString();
    }

    public void setOnDownloadListener(DownloadListener listener) {
    
        this.mListener = listener;
    }

    public interface DownloadListener {
    
        public void onDownloadStarted();
        
        public void onDownloadPercent(int downloadByte);
    
        public void onDownloadFinish(String localPath);
    
        public void onNetworkException();

        public void onNotifyIOError(String msg);

        public void onDownloadAbort();
    }

}
