package com.marvell.updater;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemProperties;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.content.SharedPreferences; 
import android.widget.TextView;
import android.widget.Scroller;
import android.text.method.ScrollingMovementMethod;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Notification;
import android.app.NotificationManager;

import com.marvell.updater.utils.Constants;

public class DownloadConfirmActivity extends Activity {
    
    private static final String TAG = "ConfirmActivity";
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    
    setContentView(R.layout.download_confirm);

    ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(Constants.NOTIFICATION_ID);
    
    final SharedPreferences preferences = getSharedPreferences(
        Constants.NETWORK_SETTING, Context.MODE_PRIVATE);
    String defaultUrl = SystemProperties.get("ro.system.update_url", Constants.URL);
    final String url = preferences.getString(Constants.KEY_SERVER, defaultUrl);
    Intent intent = getIntent();
    final String date = intent.getStringExtra(Constants.TAG_DATE);
    final String model = intent.getStringExtra(Constants.TAG_MODEL);
    final String branch = intent.getStringExtra(Constants.TAG_BRANCH);
    final String path = intent.getStringExtra(Constants.TAG_URL);

    TextView v = ((TextView)findViewById(R.id.message));
    v.setScroller(new Scroller(this)); 
    v.setMaxLines(15); 
    v.setVerticalScrollBarEnabled(true); 
    v.setMovementMethod(new ScrollingMovementMethod());
    v.setText(getReleaseInfo(url, path));

    String info = String.format("%s: %s\n%s: %s\n%s:",
        getString(R.string.product), model,
        getString(R.string.date), date,
        getString(R.string.release_info));
    v = ((TextView)findViewById(R.id.info));
    v.setText(info);
    
    if (Constants.LOG_ON) Log.d(TAG, "ota package path = " + path);
    
    findViewById(R.id.confirm_yes).setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent(DownloadConfirmActivity.this,
                DownloadService.class);
            intent.putExtra(Constants.KEY_OTA_PATH, url + path);
            intent.putExtra(Constants.TAG_MODEL, model);
            intent.putExtra(Constants.TAG_BRANCH, branch);
            intent.putExtra(Constants.TAG_DATE, date);

            startService(intent);

            finish();
        }
    });
    
    findViewById(R.id.confirm_later).setOnClickListener(new View.OnClickListener() {
        
        public void onClick(View v) {
        finish();
        }
        
    });
    
    }

    String getReleaseInfo(String url, String path) {
        String mUrl = null;
        String gUrl = null;
        HttpURLConnection httpConn = null;
        BufferedReader br = null;
        InputStream is = null;
        String unavail = getString(R.string.release_info_unavailable);
        String releaseInfo = "";

        try {
            mUrl = encode(url + path, "UTF-8");
        } catch (Exception e1) {
            e1.printStackTrace();
            mUrl = url + path;
        }
        String gPath = mUrl.substring(0, mUrl.lastIndexOf("/")+1);
        gUrl = gPath + "release.txt";
        try{
            httpConn = (HttpURLConnection) new URL(gUrl).openConnection();
            httpConn.connect();
            if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                is = httpConn.getInputStream();
                br = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line = br.readLine()) != null)
                {
                    releaseInfo += line;
                    releaseInfo += "\n";
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(br!=null){
                    br.close();
                }
                if(is!=null){
                    is.close();
                }
                if(httpConn!=null){
                    httpConn.disconnect();
                }
            }catch(Exception e){
                e.printStackTrace();
            }		
        }   

        return releaseInfo.length()!=0?releaseInfo:unavail;
    }

    public String encode(String str, String charset)
	    throws UnsupportedEncodingException {
    		String zhPattern = "[\u4E00-\u9FA5]";
		Pattern p = Pattern.compile(zhPattern);
		Matcher m = p.matcher(str);
		StringBuffer b = new StringBuffer();
		while (m.find()) {
		    m.appendReplacement(b, URLEncoder.encode(m.group(0), charset));
		}
		m.appendTail(b);
		return b.toString();
    }

}
