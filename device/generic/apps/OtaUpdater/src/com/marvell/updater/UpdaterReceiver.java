package com.marvell.updater;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.os.Build;
import android.os.SystemProperties;
import android.util.Log;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;

import com.marvell.updater.entity.BaseOtaPackage;
import com.marvell.updater.entity.BasePackageInfo;
import com.marvell.updater.entity.DevelopOtaPackage;
import com.marvell.updater.entity.DevelopPackageInfo;
import com.marvell.updater.model.DataModel;
import com.marvell.updater.net.ScanThread;
import com.marvell.updater.utils.Constants;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;

public class UpdaterReceiver 
    extends BroadcastReceiver 
    implements ScanThread.ScanListener {

    public static final String TAG = "UpdateActivity";

    private Context mContext;
    static final String action_boot = "android.intent.action.BOOT_COMPLETED";
    static final String scan = "com.marvell.updater.SCAN"; /* for test only */

    static boolean mThreadRunning = false;

    public void onReceive(Context context, final Intent intent) {
        mContext = context;

        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION))
        {
            if (intent.hasExtra(ConnectivityManager.EXTRA_NETWORK_INFO))
            {
                Log.i(TAG, "Received "+intent);
                NetworkInfo info = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (info.isConnected())
                {
                    Log.i(TAG, "Network connected, mThreadRunning="+mThreadRunning);

                    boolean enabled = SystemProperties.getBoolean("ro.system.update_enable", Constants.UPDATE_ENABLE);
                    if (enabled)
                    {
                    if (!mThreadRunning)
                    {
                        mThreadRunning = true;

                        Log.d(TAG, "start period check thread...");
                        startPeriodicCheckThread();
                    }
                    }
                    else
                    {
                        Log.d(TAG, "Auto update check disabled. To turn on it, set ro.system.update_enable");
                    }
                }
            }
        }
    }

    public void startPeriodicCheckThread()
    {
        Thread thr = new Thread("PeriodicCheckThread") 
        {
            public void run() 
            {
                while (true)
                {
                    int interval = SystemProperties.getInt("ro.system.update_poll_interval", Constants.UPDATE_POLL_INTERVAL); /* 1 day */

                    Log.d(TAG, "Scan thread running...");
                    final SharedPreferences preferences = mContext.getSharedPreferences(
                        Constants.NETWORK_SETTING, Context.MODE_PRIVATE);
                    String defaultUrl = SystemProperties.get("ro.system.update_url", Constants.URL);
                    String url = preferences.getString(Constants.KEY_SERVER, defaultUrl);
                
                    ScanThread scan = new ScanThread(url+Constants.GET_SYSTEM_VERSION);
                    scan.setScanListenerListener(UpdaterReceiver.this);
                    scan.start();

                    try {
                        String s = String.format("Sleep %ds before next checking. You can set this time through ro.system.update_poll_interval", interval);
                        Log.d(TAG, "Sleep ");
                        Thread.sleep(interval*1000);
                    } catch (Exception e) {
                        Log.e(TAG, ""+e);
                    }
                }
            }
        };
        thr.start();
    }

    public void onUpdateCheck(BaseOtaPackage otaPackage) {
        String model = Build.PRODUCT;
        String release = SystemProperties.get("ro.build.version.release");
        DevelopOtaPackage p = (DevelopOtaPackage) otaPackage;
        HashMap<String, ArrayList<DevelopPackageInfo>> mData;

        /* butterfly-userdebug 2.3.5 GINGERBREAD builder.20120103.094213 test-keys */
        String displayId = SystemProperties.get("ro.build.display.id");
        String patt = "builder.";
        int start = displayId.indexOf(patt);
        start += patt.length();
        int current = Integer.parseInt(displayId.substring(start, start+8));
        int min = Integer.MAX_VALUE;

        String bestUrl = null;
        DevelopPackageInfo bestInfo = null;
        Iterator<String> iter1 = p.mPackages.keySet().iterator();
        String key = null;
        while (iter1.hasNext())
        {
            key = iter1.next();
            Log.d(TAG, "\t"+key);
            mData = p.mPackages.get(key);
            Iterator<String> iter2 = mData.keySet().iterator();
            while (iter2.hasNext())
            {
                key = iter2.next();
                Log.d(TAG, "\t\t"+key);
    
                ArrayList<DevelopPackageInfo> a = mData.get(key);
                Iterator<DevelopPackageInfo> iter3 = a.iterator();
                while (iter3.hasNext())
                {
                    DevelopPackageInfo info = iter3.next();
                    Log.d(TAG, "\t\t\t"+info.mDate);
                    Log.d(TAG, "\t\t\t"+info.mModel);
                    Log.d(TAG, "\t\t\t"+info.mBranch);
                    Log.d(TAG, "\t\t\t"+info.mPath);
    
                    int diff = Integer.parseInt(info.mDate.replace("-", "")) - current;
                    if (diff > 0 && diff < min)
                    {
                        min = diff;
                        bestUrl = info.mPath;
                        bestInfo = info;
                    }
                }
            }
        }

        if (bestUrl == null)
        {
            Log.d(TAG, "Nothing done.");
            return;
        }

        Notification notification = new Notification(R.drawable.icon, 
            mContext.getString(R.string.update_available), 
            System.currentTimeMillis());
        Context context = mContext;
        Intent intent = new Intent(context, DownloadConfirmActivity.class);
        intent.putExtra(Constants.TAG_DATE, bestInfo.mDate);
        intent.putExtra(Constants.TAG_MODEL, bestInfo.mModel);
        intent.putExtra(Constants.TAG_BRANCH, bestInfo.mBranch);
        intent.putExtra(Constants.TAG_URL, bestInfo.mPath);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);
        notification.setLatestEventInfo(context, 
            mContext.getString(R.string.update_available), 
            bestInfo.mDate, 
            contentIntent);
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(Constants.NOTIFICATION_ID, notification);
    }

    public void onNetworkException() {
        Log.d(TAG, "Network failure");
    }
}
