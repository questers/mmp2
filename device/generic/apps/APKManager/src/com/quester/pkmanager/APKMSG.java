package com.quester.pkmanager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageParser;
import android.content.pm.PublicKeyXmlParser;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class APKMSG extends Activity implements OnClickListener {

	private static final String TAG = "InstallManager";

	private PackageParser parser;
	private Certificate[] certs;
	private PublicKey publicKey;
	private PublicKeyXmlParser pkxp;
	private List<String> pkmsgs;
	private List<String> pkgNames;
	
	private Spinner fileName;
	private EditText fileMSG;
	private Button refresh;
	private Button getMSG;
	private Button clearMSG;
	private Button allowMSG;
	
	private ArrayAdapter<String> adapter;
	private static int count;
	private static boolean getSomething;
	
	private String APK_PATH = "mnt/sdcard";
	private final String PUBLIC_KEY_PATH = "data/system/publicKey.xml";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setup();
    }
    
    private void setup() {
    	fileName = (Spinner)findViewById(R.id.fileName);
    	fileMSG = (EditText)findViewById(R.id.fileMSG);
		//fileMSG.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);
    	refresh = (Button)findViewById(R.id.refresh);
    	refresh.setOnClickListener(this);
		getMSG = (Button)findViewById(R.id.getMSG);
		getMSG.setOnClickListener(this);
		clearMSG = (Button)findViewById(R.id.clearMSG);
		clearMSG.setOnClickListener(this);
		allowMSG = (Button)findViewById(R.id.allowMSG);
		allowMSG.setOnClickListener(this);
		
		refreshAdapter();

		fileName.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				arg0.setVisibility(View.VISIBLE);
				count = arg2;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
			
		});
		
		pkxp = new PublicKeyXmlParser(PUBLIC_KEY_PATH);
    }

	private void refreshAdapter() {
    	List<String> tmp = traverse(APK_PATH);
		if (tmp.size() != 0) {
			pkgNames = tmp;
			tmp = null;
			getSomething = true;
		} else {
			pkgNames = new ArrayList<String>();
			pkgNames.add(getString(R.string.noFound));
			getSomething = false;
		}
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, pkgNames);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		fileName.setAdapter(adapter);
		
    }
    
    private List<String> traverse(String path) {
		List<String> filesName = new ArrayList<String>();
		try {
			File file = new File(path);
			if (file.exists()) {
				for (String name : file.list()) {
					String tmpPath = path + File.separator + name;
					System.out.println(tmpPath);
					if (new File(tmpPath).isDirectory()) {
						for (String sName : traverse(tmpPath)) {
							filesName.add(sName);
						}
					} else {
						if (tmpPath.endsWith(".apk") || tmpPath.endsWith(".APK") 
								|| tmpPath.endsWith(".x509.pem")
								|| tmpPath.endsWith(".crt") || tmpPath.endsWith(".CRT"))
						filesName.add(tmpPath);
					}
				}
			}
			filesName.add(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filesName;
	}

	@Override
	public void onClick(View v) {
		if (v == refresh) {
			refreshAdapter();
		}
		else if (v == getMSG) {
			if (getSomething) {
				String fullPath = pkgNames.get(count);
				if (fullPath.endsWith(".apk") || fullPath.endsWith(".APK")) {
					parser = new PackageParser(fullPath);
					try {
						JarFile jarFile = new JarFile(fullPath);
						JarEntry jarEntry = jarFile.getJarEntry("AndroidManifest.xml");
						byte[] readBuffer = new byte[8192];
						certs = parser.loadCerts(jarFile, jarEntry, readBuffer);
						if (certs == null) {
							fileMSG.setText(R.string.noCert);
							jarFile.close();
						} else {
							publicKey = certs[0].getPublicKey();
							fileMSG.setText(publicKey.toString());
						}
					} catch(Exception e) {
						Log.e(TAG, e.getMessage());
					}
				}
				else if (fullPath.endsWith(".x509.pem") || fullPath.endsWith(".crt") 
						|| fullPath.endsWith(".CRT")) {
					FileInputStream fis = null;
					BufferedInputStream bis = null;
					Certificate cert = null;
					try {
						fis = new FileInputStream(fullPath);
						bis = new BufferedInputStream(fis);
						CertificateFactory cf = CertificateFactory.getInstance("X.509");
						while (bis.available() > 0) {
							cert = cf.generateCertificate(bis);
						}
						if (cert == null) {
							fileMSG.setText(R.string.noCert);
						} else {
							publicKey = cert.getPublicKey();
							fileMSG.setText(publicKey.toString());
						}
					} catch (Exception e) {
						Log.e(TAG, e.getMessage());
					} finally {
						if (bis != null) {
							try {
								bis.close();
							} catch (Exception e) {
								Log.e(TAG, e.getMessage());
							}
						}
					}
				}
			}
		}
		else if (v == clearMSG) {
			fileMSG.setText("");
			publicKey = null;
		}
		else if (v == allowMSG) {
			if (getSomething) {
				if (publicKey == null) {
					fileMSG.setText(R.string.noCert);
				} else {
					pkmsgs = new ArrayList<String>();
					pkmsgs.add(publicKey.toString());
					if (pkxp.insertPK2Xml(pkmsgs)) {
						fileMSG.setText(R.string.success);
					} else {
						fileMSG.setText(R.string.fault);
					}
				}
			}
		}
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.menu, menu);
    	return true;
    }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	int itemId = item.getItemId();
    	switch (itemId) {
    	case R.id.help:
    		openAboutDialog();
    		break;
    	case R.id.exit:
    		APKMSG.this.finish();
    		break;
    	}
    	return true;
    }
	private void openAboutDialog() {
    	LayoutInflater inflater = LayoutInflater.from(this);
    	View view = inflater.inflate(R.layout.helpview, null);
    	new AlertDialog.Builder(APKMSG.this).setTitle(getString(R.string.help)).setView(view)
    			.setNegativeButton(getString(R.string.close), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				}).show();
    }
}