/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.com.quester.OpenTheUsbMassStorage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.net.Uri;
import java.io.File;

/**
 *
 * @author Administrator
 */
public class OpenTheUsbMassStorageActivity extends Activity
{

    /** Called when the activity is first created. */
    @Override
    public void onCreate( Bundle icicle )
    {
        super.onCreate( icicle );
        // ToDo add your GUI initialization code here
        Intent intent = new Intent("com.estrongs.action.PICK_DIRECTORY");
        intent.setData(Uri.fromFile(new File("/mnt/ums_h")));
        startActivity(intent);
        finish();
    }
}
