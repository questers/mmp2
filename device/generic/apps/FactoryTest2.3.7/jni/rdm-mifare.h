#ifndef __RDM_MIFARE_H_
#define __RDM_MIFARE_H_

#ifdef __cplusplus
extern "C" 
{
#endif

#define DelayUS(a) usleep(a)
#define DelayMS(a) usleep((a)*1000)

#ifndef U8 
#define U8 unsigned char 
#endif 


#ifndef U32 
#define U32 unsigned int
#endif
#ifndef U16 
#define U16 unsigned short
#endif

#define IDLE			0x00
#define ALL			0x01
#define REQA_IDLE		0x26
#define REQA_ALL		0x52

#define STX			0
#define STATION			1	
#define DATALENGTH		2		
#define COMMAND			3		
#define STATUS			3
#define DATA			4


#define AUTH_KEYB		0x02
#define AUTH_KEYA		0x00

/*ISO14443 Type A Command */
#define REQA			0x03
#define ANTICOLLA		0x04
#define SELECTA			0x05
#define HALTA			0x06

/*Aplication Command */
#define MF_READ			0x20		
#define MF_WRITE		0x21		
#define MF_INIT_VAL		0x22
#define MF_DEC			0x23
#define MF_INC			0x24
#define MF_GET_SNR		0x25
/*System Command  */
#define SET_ADDRESS		0x80
#define SET_BAUDRATE		0x81
#define SET_SERNUM		0x82
#define GET_SERNUM		0x83
#define WRITE_USERINFO		0x84
#define READ_USERINFO		0x85
#define GET_VERNUM		0x86
#define CONTROL_LED		0x88
#define CONTROL_BUZZER		0x89

/* Status and Error code */
#define COMM_OK			0x00
#define COMM_ERR		0xFF
#define PARAM_OK		0x80
#define	SET_FAILED		0x81
#define	TIME_OUT		0x82
#define NO_CARD			0x83
#define RECEIVE_ERROR		0x84
#define AUTH_FAILED		0x85
#define UNKNOWN_ERROR		0x87
#define CARD_OP_FAILED		0x89
#define PASS_FAILED		0x8C
#define BAD_COMMAND		0x8F

int (* mifare_raw_read)();
int (* mifare_raw_write)(int value);
int (* mifare_raw_poll)();
int (* mifare_raw_flush_output)();
void (* mifare_raw_close)();

/*
 * 功能：寻卡
 * 参数：
 *   U8 mode - 寻卡模式。REQA_IDLE：Idle模式（一次只对一张卡操作）；REQA_ALL：All模式（一次可对多张卡操作）
 * 返回值：
 *   0x00 - 成功找到卡
 *   0x82 - 通信超时
 *   0x83 - 卡不存在
 *   0x84 - 接受卡数据出错
 *   0x87 - 未知错误
 *   0x89 - 参数错误
 *   0x8f - 输入的指令不存在
 */
int Mifare_REQA(U8 mode);
/*
 * 功能：设置模组通信的波特率
 * 参数：
 *   U32 rate - 波特率。
 *     B9600 - 9600
 *     B19200 - 19200
 *     B38400 - 38400
 *     B57600 - 57600
 *     B115200 - 115200
 * 返回值：
 *   0x00 - 成功找到卡
 *   0x89 - 参数错误
 *   0x8f - 输入的指令不存在
 */
int SetBaudRate(U32 );
int Mifare_AnticollA(U32 *);
int Mifare_SelectA(U32);
int Mifare_GetSerNum(U8 *);
int Mifare_SetSerNum(U8 *);
int Mifare_REQA(U8);
int Mifare_HaltA();
int Mifare_SelectA(U32);
/*
 * 功能：读取Mifare卡
 * 参数：
 *   U8 addr - 读取的块地址，取值范围0x00-0x3f
 *   U8 blocks - 读取的块数，取值范围0x01-0x04
 *   U8 *key - 长度为6的缓冲区，传入卡的密钥
 *   U8 auth - 认证模式。0：对KEYA进行验证；1：对KEYB进行验证
 *   U8 mode - 请求模式。0：IDLE模式；1：ALL模式
 *   U8 *buf - 长度至少为blks*16的缓冲区，用来存放读出的数据
 *   U32 * uid - 返回4字节的卡序列号
 */
int Mifare_Read(U8 addr, U8 blocks, U8 *key, U8 auth,U8 mode, U8 *buf, U32 * uid);
/*
 * 功能：写Mifare卡
 * 参数：
 *   U8 addr - 块地址，取值范围0x00-0x3f
 *   U8 blocks - 块数，取值范围0x01-0x04
 *   U8 *key - 长度为6的缓冲区，传入卡的密钥
 *   U8 auth - 认证模式。0：对KEYA进行验证；1：对KEYB进行验证
 *   U8 mode - 请求模式。0：IDLE模式；1：ALL模式
 *   U8 * buf - 长度至少为blks*16的缓冲区，用来存放写入的数据
 *   U32 * uid - 返回4字节的卡序列号
 *   int protect - 0: 允许写最后一个block；1：不允许写最后一个block
 */
int Mifare_Write(U8 addr, U8 blocks, U8 *key, U8 auth, U8 mode, U8 * buf, U32 * uid, int protect);
/*
 * 功能：获取身份证卡号
 * 参数：
 *   U8 mode - REQA_IDLE：Idle模式；REQA_ALL：All模式
 *   U8 halt - 0x00: 不需要执行halt命令；0x01: 需要执行halt命令
 *   U8 *serialnumber - 用来存放返回序列号的缓冲区
 * 返回值：
 *   0x00 - 成功
 *   0x82 - 通信超时
 *   0x83 - 卡不存在
 *   0x84 - 接受卡数据出错
 *   0x87 - 未知错误
 *   0x89 - 参数错误
 *   0x8f - 输入的指令不存在
 */
int Mifare_Get_SNR(U8 mode, U8 halt, U8 *serialnumber);
void Mifare_Perror(int errno);
const char* Mifare_Strerror( int errno );

/*
 * 功能：对SIM卡进行复位
 * 参数：无
 * 返回值：
 *   0x00 - 复位成功
 *   0x83 - 卡不存在
 *   0x84 - 接受卡数据出错
 *   0x85 - 参数格式错误
 */
int SIM_Reset();
/*
 * 功能：进入SIM卡地址薄
 * 参数：
 *   U8* nr - 返回地址薄条数
 *   U8* max_size - 返回地址薄最大字节数
 * 返回值：
 *   0x00 - 复位成功
 *   0x83 - 卡不存在
 *   0x84 - 接受卡数据出错
 *   0x85 - 参数格式错误
 */
int SIM_EnterDirectory(U8* nr, U8* max_size);
/*
 * 功能：读取地址薄
 * 参数：
 *   U8 index - 要读的地址薄的索引，从1到地址薄条数
 *   U8 max_size - 地址薄容量。必须为sim_enter_directory返回的最大字节数
 *   U8* buf - 缓冲区，大小为max_size，用来存放读出地址薄数据
 * 返回值：
 *   0x00 - 复位成功
 *   0x83 - 卡不存在
 *   0x84 - 接受卡数据出错
 *   0x85 - 参数格式错误
 */
int SIM_ReadDirectory(U8 index, U8 max_size, U8* buf);
/*
 * 功能：进入SIM卡短信目录
 * 参数：
 *   U8* nr - 返回短信条数
 *   U8* max_size - 返回短信最大字节数
 * 返回值：
 *   0x00 - 复位成功
 *   0x83 - 卡不存在
 *   0x84 - 接受卡数据出错
 *   0x85 - 参数格式错误
 */
int SIM_EnterSMS(U8* nr, U8* max_size);
/*
 * 功能：读取短信
 * 参数：
 *   U8 index - 要读的短信的索引，从1到短信条数
 *   U8 max_size - 短信容量。必须为sim_enter_sms返回的最大字节数
 *   U8* buf - 缓冲区，大小为max_size，用来存放读出短信数据
 * 返回值：
 *   0x00 - 复位成功
 *   0x83 - 卡不存在
 *   0x84 - 接受卡数据出错
 *   0x85 - 参数格式错误
 */
int SIM_ReadSMS(U8 index, U8 max_size, U8* buf);

#ifdef __cplusplus
}
#endif
#endif
