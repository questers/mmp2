#if !defined(MIFARE_SERIAL_H)

int serial_open(int mt);
int serial_read();
int serial_write(int value);
int serial_poll();
int serial_flush();
void serial_close();

struct serial_op_t
{
	int (* open)(int mt);
	int (* read)();
	int (* write)(int value);
	int (* poll)();
	int (* flush_input)();
	int (* flush_output)();
	void (* free)();
} st232 =
{
	.open = serial_open,
	.read = serial_read,
	.write = serial_write,
	.poll = serial_poll,
	.flush_input = serial_flush,
	.flush_output = serial_flush,
	.free = serial_close,
};

#endif