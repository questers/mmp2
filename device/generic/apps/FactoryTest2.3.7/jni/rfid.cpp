#include <stdio.h>
#include <stdlib.h>
#include <jni.h>

#include "log.h"
#include "zlg500b.h"

static char className[] = "com/quester/factory/Utils";

jboolean
jni_mifare_init(JNIEnv *env, jobject thiz, jint mt) {
    LOGD("%s", __FUNCTION__);

    int ret;
    ret = MFInit(mt);
    if (!ret) 
    {
        LOGD("MFInit failed");
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

void
jni_mifare_free(JNIEnv *env, jobject thiz) {
    LOGD("%s", __FUNCTION__);
    MFFree();
}

static JNINativeMethod methods[] = {
  {"mifare_init", "(I)Z", (void*) jni_mifare_init },
  {"mifare_free", "()V", (void*) jni_mifare_free },
};

/*
 * Register several native methods for one class.
 */
static int registerNativeMethods(JNIEnv* env, const char* className,
    JNINativeMethod* gMethods, int numMethods)
{
    jclass clazz;

    clazz = env->FindClass(className);
    if (clazz == NULL) {
        LOGD("Native registration unable to find class '%s'", className);
        return JNI_FALSE;
    }
    if (env->RegisterNatives(clazz, gMethods, numMethods) < 0) {
        LOGD("RegisterNatives failed for '%s'", className);
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

/*
 * Register native methods for all classes we know about.
 *
 * returns JNI_TRUE on success.
 */
static int registerNatives(JNIEnv* env)
{
  if (!registerNativeMethods(env, className,
                 methods, sizeof(methods) / sizeof(methods[0]))) {
    return JNI_FALSE;
  }

  return JNI_TRUE;
}


// ----------------------------------------------------------------------------

/*
 * This is called by the VM when the shared library is first loaded.
 */
 
typedef union {
    JNIEnv* env;
    void* venv;
} UnionJNIEnvToVoid;

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    jint result = -1;
    JNIEnv* env = NULL;
    
    LOGD("JNI_OnLoad");

    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_4) != JNI_OK) {
        LOGD("ERROR: GetEnv failed");
        goto bail;
    }
    env = uenv.env;

    if (registerNatives(env) != JNI_TRUE) {
        LOGD("ERROR: registerNatives failed");
        goto bail;
    }

    result = JNI_VERSION_1_4;
    
bail:
    return result;
}
