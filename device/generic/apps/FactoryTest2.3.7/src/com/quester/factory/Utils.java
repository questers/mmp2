package com.quester.factory;

public class Utils {

	static {
		System.loadLibrary("rfids");
	}
	
	private native boolean mifare_init(int value);
    private native void mifare_free();
    
	public boolean init(int value) {
		return mifare_init(value);
	}

	public void free() {
		mifare_free();
	}
}