package com.quester.factory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class FactoryActivity extends Activity {
	
	private Button mStartBtn;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mStartBtn = (Button)findViewById(R.id.start_button);
        mStartBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(FactoryActivity.this, FactoryProcess.class));
				finish();
			}
		});
    }
}