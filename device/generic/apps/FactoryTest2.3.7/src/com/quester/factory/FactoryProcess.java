package com.quester.factory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Camera;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.IPowerManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.storage.IMountService;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

public class FactoryProcess extends Activity {

	static {
		System.loadLibrary("usbcamtest");
	}
	
	private static final String TAG = "factory";
	private static final int TO_TOUCH = 1;
	private static final int TO_BACKLIGHT = 2;
	private static final int TO_MEDIA = 3;
	private static final int TO_MIC = 4;
	private static final int TO_TING = 5;
	private static final int TO_FLASH = 6;
	private static final int TO_AUTO = 7;
	
	private static final int MINIMUM_BACKLIGHT = android.os.Power.BRIGHTNESS_DIM;
	private static final int MAXIMUM_BACKLIGHT = android.os.Power.BRIGHTNESS_ON;
	
	private LinearLayout firstLayout, secondLayout, thirdLayout;
	private TextView firstText, secondText, thirdText;
	
	private static StringBuilder testInfo;
	private static boolean mark, flag, finish;
	private static int touchDown, touchUp, touchMove;
	private Context mContext;
	private BluetoothAdapter mBluetoothAdapter;
	private Thread autoThread;
	
	private native int zkfpi_init();
    private native int zkfpi_select(int i);
    private native int zkfpi_open();
    private native int zkfpi_close();
    private native int zkfpi_get_image(byte[] buf);
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
        setContentView(R.layout.factory_test);
        initialize();
		testInfo = new StringBuilder();
		mContext = this;
        
        if (checkExternalStorage()) {
			testInfo.append(getString(R.string.external_storage_s) + "\n");
		} else {
			testInfo.append(getString(R.string.external_storage_f) + "\n");
		}
        
        checkScreenBadPoint();
    }
	
	private void initialize() {
		firstLayout = (LinearLayout)findViewById(R.id.first_partition);
		secondLayout = (LinearLayout)findViewById(R.id.second_partition);
		thirdLayout = (LinearLayout)findViewById(R.id.third_partition);
		firstText = (TextView)findViewById(R.id.first_text);
		secondText = (TextView)findViewById(R.id.second_text);
		thirdText = (TextView)findViewById(R.id.third_text);
		firstText.setTextSize(21);
		secondText.setTextSize(21);
		thirdText.setTextSize(21);
	}
	
	private boolean checkExternalStorage() {
		boolean flag = false;
		IMountService mMountService = null;
		IBinder service = ServiceManager.getService("mount");
		if (service != null) {
			mMountService = IMountService.Stub.asInterface(service);
		} else {
			Log.i(TAG, "Can't get mount service.");
		}
    	
    	if (mMountService == null) {
    		Log.i(TAG, "Mount service is null.");
    		return flag;
    	}
    	
    	String status_external = null;
    	//String status_usbdisk = null;
    	try {
    		status_external = mMountService.getVolumeState("/mnt/sdcard/tflash");
    		//status_usbdisk = mMountService.getVolumeState("/mnt/usbdisk");
    		if (status_external.equals(Environment.MEDIA_MOUNTED)) {
    				//|| status_usbdisk.equals(Environment.MEDIA_MOUNTED)) {
    			flag = true;
    		} else {
    			flag = false;
    		}
    	} catch (RemoteException ex) {
    		Log.w(TAG, ex.getMessage());
    	}
    	return flag;
	}
	
	private void checkScreenBadPoint() {
		firstLayout.setBackgroundColor(Color.RED);
		secondLayout.setBackgroundColor(Color.GREEN);
		thirdLayout.setBackgroundColor(Color.BLUE);
		secondText.setText(R.string.bad_point_info);
		new Thread(new Runnable() {
			public void run() {
				int i = 0;
				while (i < 7) {
					sleep(1000);
					updateOnUiThread(i++);
				}
			}
		}).start();
	}
	
	private void updateScreenColor(int num) {
		switch (num) {
			case 0:
				firstLayout.setBackgroundColor(Color.GREEN);
				secondLayout.setBackgroundColor(Color.BLUE);
				thirdLayout.setBackgroundColor(Color.RED);
				break;
			case 1:
				firstLayout.setBackgroundColor(Color.BLUE);
				secondLayout.setBackgroundColor(Color.RED);
				thirdLayout.setBackgroundColor(Color.GREEN);
				break;
			case 2:
				firstLayout.setBackgroundColor(Color.RED);
				secondLayout.setBackgroundColor(Color.RED);
				thirdLayout.setBackgroundColor(Color.RED);
				break;
			case 3:
				firstLayout.setBackgroundColor(Color.GREEN);
				secondLayout.setBackgroundColor(Color.GREEN);
				thirdLayout.setBackgroundColor(Color.GREEN);
				break;
			case 4:
				firstLayout.setBackgroundColor(Color.BLUE);
				secondLayout.setBackgroundColor(Color.BLUE);
				thirdLayout.setBackgroundColor(Color.BLUE);
				break;
			case 5:
				firstLayout.setBackgroundColor(Color.BLACK);
				secondLayout.setBackgroundColor(Color.BLACK);
				thirdLayout.setBackgroundColor(Color.BLACK);
				break;
			default: 
				secondText.setText("");
				creatDialog(getString(R.string.screen_bad_point), TO_TOUCH);
				break;
		}
	}
	
	private void checkScreenTouch() {
		if (mark) {
			testInfo.append(getString(R.string.bad_point_f) + "\n");
		} else {
			testInfo.append(getString(R.string.bad_point_s) + "\n");
		}
		firstText.setText(R.string.screen_touch_notice);
		thirdText.setText(R.string.leave);
		//thirdText.setBackgroundColor(Color.GRAY);
		thirdText.setTextColor(Color.WHITE);
		thirdText.setClickable(true);
		thirdText.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				creatDialog(getString(R.string.screen_touch), TO_BACKLIGHT);
			}
		});
		flag = true;
	}
	
	private void checkScreenBacklight() {
		flag = false;
		firstText.setText("");
		secondText.setText(R.string.screen_backligt_info);
		thirdText.setClickable(false);
		thirdText.setBackgroundColor(Color.BLACK);
		thirdText.setText("");
		if (mark) {
			testInfo.append(getString(R.string.touch_s) + "\n");
		} else {
			testInfo.append(getString(R.string.touch_f) + "\n");
		}
		firstLayout.setBackgroundColor(Color.WHITE);
		secondLayout.setBackgroundColor(Color.WHITE);
		thirdLayout.setBackgroundColor(Color.WHITE);
		
		new Thread(new Runnable() {
			public void run() {
				try {
					int mBrightness = Settings.System.getInt(mContext.getContentResolver(), 
							Settings.System.SCREEN_BRIGHTNESS);
					IPowerManager power = IPowerManager.Stub.asInterface(
							ServiceManager.getService("power"));
					if (power != null) {
						int brightness = mBrightness;
						while (brightness >= MINIMUM_BACKLIGHT) {
							power.setBacklightBrightness(brightness);
							brightness -= 10;
							Thread.sleep(200);
						}
						brightness += 20;
						while (brightness <= MAXIMUM_BACKLIGHT) {
							power.setBacklightBrightness(brightness);
							brightness += 10;
							Thread.sleep(200);
						}
						brightness -= 20;
						while (brightness >= mBrightness) {
							power.setBacklightBrightness(brightness);
							brightness -= 10;
							Thread.sleep(200);
						}
					}
				} catch (RemoteException e) {
					Log.w(TAG, e.getMessage());
				} catch (SettingNotFoundException e) {
					Log.w(TAG, e.getMessage());
				} catch (InterruptedException e) {
					Log.w(TAG, e.getMessage());
				}
				updateOnUiThread(12);
			}
		}).start();
	}
	
	private void playMedia() {
		if (mark) {
			testInfo.append(getString(R.string.backlight_s) + "\n");
		} else {
			testInfo.append(getString(R.string.backlight_f) + "\n");
		}
		firstLayout.setBackgroundColor(Color.BLACK);
		secondLayout.setBackgroundColor(Color.BLACK);
		thirdLayout.setBackgroundColor(Color.BLACK);
		
		setContentView(R.layout.media);
		final VideoView mVideo = (VideoView)findViewById(R.id.media_video);
		mVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				mVideo.stopPlayback();
				creatDialog(getString(R.string.about_media), TO_MIC);
			}
		});
		
		File file = new File("/sdcard/Beyonce.flv");
		try {
			if (!file.exists()) {
				byte[] buffer = new byte[512];
				InputStream inStream = getAssets().open("Beyonce.flv");
				FileOutputStream outStream = new FileOutputStream(file);
				while (inStream.read(buffer) != -1) {
					outStream.write(buffer, 0, buffer.length);
				}
				outStream.flush();
				outStream.close();
				inStream.close();
			}
		} catch (IOException e) {
			Log.w(TAG, e.getMessage());
		}
		
		mVideo.setVideoPath(file.getPath());
		mVideo.start();
	}
	
	private void checkMicrophone() {
		if (mark) {
			testInfo.append(getString(R.string.media_s) + "\n");
		} else {
			testInfo.append(getString(R.string.media_f) + "\n");
		}
		setContentView(R.layout.factory_test);
		initialize();
		firstText.setText(R.string.mic_info);
		new Thread(new Runnable() {
			public void run() {
				int i = 7;
				while (i < 11) {
					updateOnUiThread(i++);
					sleep(1000);
				}
			}
		}).start();
	}
	
	private void recordVoice(int num) {
		switch (num) {
			case 7:
				secondText.setText(R.string.l_three);
				break;
			case 8:
				secondText.setText(R.string.l_two);
				break;
			case 9:
				secondText.setText(R.string.l_one);
				break;
			default:
				firstText.setText("");
				secondText.setText(R.string.l_start);
				new Thread(new Runnable() {
					public void run() {
						MediaRecorder mRecorder = new MediaRecorder();
						mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
						mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
						mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
						mRecorder.setOutputFile("/sdcard/mic_record.3gpp");
						try {
							mRecorder.prepare();
							mRecorder.start();
						} catch (IOException e) {
							Log.w(TAG, e.getMessage());
						}
						
						sleep(5000);
						mRecorder.stop();
						mRecorder.release();
						mRecorder = null;
						updateOnUiThread(13);
					}
				}).start();
				break;
		}
	}

	private void playRecord() {
		secondText.setText(R.string.l_play);
		final MediaPlayer mPlayer = new MediaPlayer();
		mPlayer.setOnCompletionListener(new OnCompletionListener() {
			public void onCompletion(MediaPlayer arg0) {
				mPlayer.release();
				creatDialog(getString(R.string.about_mic), TO_FLASH);
			}
		});
		try {
			mPlayer.setDataSource("/sdcard/mic_record.3gpp");
			mPlayer.prepare();
			mPlayer.start();
		} catch (IOException e) {
			Log.w(TAG, e.getMessage());
		}
	}
	
	private void checkTingTone() {
		if (mark) {
			testInfo.append(getString(R.string.mic_s) + "\n");
		} else {
			testInfo.append(getString(R.string.mic_f) + "\n");
		}
		secondText.setText(R.string.ting_info);

		File file = new File("/sdcard/Caribbean.mp3");
		try {
			if (!file.exists()) {
				byte[] buffer = new byte[512];
				InputStream inStream = getAssets().open("Caribbean.mp3");
				FileOutputStream outStream = new FileOutputStream(file);
				while (inStream.read(buffer) != -1) {
					outStream.write(buffer, 0, buffer.length);
				}
				outStream.flush();
				outStream.close();
				inStream.close();
			}
		} catch (IOException e) {
			Log.w(TAG, e.getMessage());
		}
		
		final AudioManager audioManager = 
				(AudioManager)getSystemService(Context.AUDIO_SERVICE);
		audioManager.setSpeakerphoneOn(false);  
		setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);  
		audioManager.setMode(AudioManager.MODE_IN_CALL);
		
		final MediaPlayer mPlayer = new MediaPlayer();
		mPlayer.setOnCompletionListener(new OnCompletionListener() {
			public void onCompletion(MediaPlayer arg0) {
				mPlayer.release();
				audioManager.setMode(AudioManager.MODE_NORMAL);
				creatDialog(getString(R.string.about_ting), TO_FLASH);
			}
		});
		
		try {
			mPlayer.reset();
			mPlayer.setDataSource("/sdcard/Caribbean.mp3");
			mPlayer.prepare();
			mPlayer.start();
		} catch (IOException e) {
			Log.w(TAG, e.getMessage());
		}
	}
	
	private void checkFlash() {
		/*if (mark) {
			testInfo.append(getString(R.string.ting_s) + "\n");
		} else {
			testInfo.append(getString(R.string.ting_f) + "\n");
		}*/
		if (mark) {
			testInfo.append(getString(R.string.mic_s) + "\n");
		} else {
			testInfo.append(getString(R.string.mic_f) + "\n");
		}
		secondText.setText("");
		Intent intent = new Intent("quester.intent.action.FLASHLIGHT");
        sendBroadcast(intent);
        sleep(1000);
        creatDialog(getString(R.string.about_flash), TO_AUTO);
	}
	
	private void autoChecking() {
		Intent intent = new Intent("quester.intent.action.FLASHLIGHT");
        sendBroadcast(intent);
		if (mark) {
			testInfo.append(getString(R.string.flash_s) + "\n");
		} else {
			testInfo.append(getString(R.string.flash_f) + "\n");
		}

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		setContentView(R.layout.auto);
		autoThread = new Thread(new Runnable() {
			public void run() {
				checking();
			}
		});
		autoThread.start();
		
		finish = false;
		new Thread(new Runnable() {
			public void run() {
				int i = 0;
				while (!finish) {
					sleep(1000*30);
					i++;
					if (i == 10 && !finish) {
						finish = true;
						if (!autoThread.isInterrupted()) {
							autoThread.interrupt();
							testInfo.append(getString(R.string.gps_f) + "\n");
							updateOnUiThread(11);
						}
					}
				}
			}
		}).start();
	}
	
	private void checking() {
		int num = Camera.getNumberOfCameras();
		if (num > 0) {
			Camera camDevice = Camera.open();
			if (camDevice != null) {
				camDevice.release();
				testInfo.append(getString(R.string.camera_s) + "\n");
			} else {
				testInfo.append(getString(R.string.camera_f) + "\n");
			}
		} else {
			testInfo.append(getString(R.string.camera_f) + "\n");
		}
		
		String baseband = SystemProperties.get("gsm.version.baseband", "null");
		if (baseband.equals("null")) {
			testInfo.append(getString(R.string.m_3g_f) + "\n");
		} else {
			testInfo.append(getString(R.string.m_3g_s) + "\n");
		}
		
		WifiManager mWifiManager = 
				(WifiManager)getSystemService(WIFI_SERVICE);
		if (mWifiManager.getWifiState() == WifiManager.WIFI_STATE_UNKNOWN) {
			testInfo.append(getString(R.string.wifi_f) + "\n");
		} else {
			testInfo.append(getString(R.string.wifi_s) + "\n");
		}
		
		if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
			mBluetoothAdapter.enable();
			if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_TURNING_ON 
					|| mBluetoothAdapter.getState() == BluetoothAdapter.STATE_ON) {
				testInfo.append(getString(R.string.bluetooth_s) + "\n");
				mBluetoothAdapter.disable();
			} else {
				testInfo.append(getString(R.string.bluetooth_f) + "\n");
			}
		} else {
			testInfo.append(getString(R.string.bluetooth_s) + "\n");
		}
		
		try {
			BufferedWriter bw104 = new BufferedWriter(
					new FileWriter(new File("/sys/class/gpio/gpio104/value")));
			bw104.write("1");
			bw104.flush();
			bw104.close();
		} catch (IOException e) {
			Log.w(TAG, e.getMessage());
		}
		sleep(5000);
		int ret = zkfpi_init();
		if (ret > 0) {
			zkfpi_select(0);
	        ret = zkfpi_open();
	        if (ret == 0) {
	        	zkfpi_close();
	        	testInfo.append(getString(R.string.fp_s) + "\n");
	        } else {
	        	testInfo.append(getString(R.string.fp_f) + "\n");
	        }
		} else {
			testInfo.append(getString(R.string.fp_f) + "\n");
		}
		
		try {
			BufferedWriter bw104 = new BufferedWriter(
					new FileWriter(new File("/sys/class/gpio/gpio104/value")));
			bw104.write("0");
			bw104.flush();
			bw104.close();
		} catch (IOException e) {
			Log.w(TAG, e.getMessage());
		}
		
		Utils utils = new Utils();
		if (utils.init(0)) {
			utils.free();
			testInfo.append(getString(R.string.mifare_s) + "\n");
		} else {
			testInfo.append(getString(R.string.mifare_f) + "\n");
		}
		
		if (utils.init(1)) {
			utils.free();
			testInfo.append(getString(R.string.rfid_s) + "\n");
		} else {
			testInfo.append(getString(R.string.rfid_f) + "\n");
		}
		
		LocationManager mLocationManager = 
				(LocationManager)getSystemService(Context.LOCATION_SERVICE);
		if (mLocationManager != null) {
			if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				Settings.Secure.setLocationProviderEnabled(getContentResolver(),
	                    LocationManager.GPS_PROVIDER, true);
			}
			Criteria criteria = new Criteria();
	        criteria.setAccuracy(Criteria.ACCURACY_LOW);
	        criteria.setAltitudeRequired(false);
	        criteria.setBearingRequired(false);
	        criteria.setCostAllowed(true);
	        criteria.setPowerRequirement(Criteria.POWER_HIGH);
	        
	        String provider = mLocationManager.getBestProvider(criteria, true);
	        Location location = mLocationManager.getLastKnownLocation(provider);
	        if (location != null) {
	            double longitude= location.getLongitude();
	            double latitude = location.getLatitude();
	            Log.i(TAG, "location -> " + longitude + "," + latitude);
	            testInfo.append(getString(R.string.gps_s) + "\n");
	        } else {
	        	testInfo.append(getString(R.string.gps_f) + "\n");
	        }
		} else {
			testInfo.append(getString(R.string.gps_f) + "\n");
		}
		finish =true;
		
		updateOnUiThread(11);
	}
	
	private void showResult() {
		setContentView(R.layout.result);
		TextView result = (TextView)findViewById(R.id.final_result);
		result.setText(testInfo.toString());
	}
	
	private void sleep(int millisecond) {
		try {
			Thread.sleep(millisecond);
		} catch (InterruptedException e) {
			Log.w(TAG, e.getMessage());
		}
	}
	
	private void updateOnUiThread(final int num) {
		runOnUiThread(new Runnable() {
			public void run() {
				if (num < 7) {
					updateScreenColor(num);
				} else if (num < 11) {
					recordVoice(num);
				} else if (num == 11){
					showResult();
				} else if (num == 12) {
					creatDialog(getString(R.string.screen_backlight), TO_MEDIA);
				} else if (num == 13) {
					playRecord();
				}
			}
		});
	}
	
	private void creatDialog(final String message, final int num) {
		Dialog dialog = new AlertDialog.Builder(this)
			.setTitle(getString(R.string.app_name))
			.setIcon(R.drawable.dialog_question)
			.setMessage(message)
			.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					mark = true;
					dialog.dismiss();
					switch (num) {
						case TO_TOUCH:
							checkScreenTouch();
							break;
						case TO_BACKLIGHT:
							checkScreenBacklight();
							break;
						case TO_MEDIA:
							playMedia();
							break;
						case TO_MIC:
							checkMicrophone();
							break;
						case TO_TING:
							checkTingTone();
							break;
						case TO_FLASH:
							checkFlash();
							break;
						case TO_AUTO:
							autoChecking();
							break;
					}
				}
			})
			.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					mark = false;
					dialog.dismiss();
					switch (num) {
						case TO_TOUCH:
							checkScreenTouch();
							break;
						case TO_BACKLIGHT:
							checkScreenBacklight();
							break;
						case TO_MEDIA:
							playMedia();
							break;
						case TO_MIC:
							checkMicrophone();
							break;
						case TO_TING:
							checkTingTone();
							break;
						case TO_FLASH:
							checkFlash();
							break;
						case TO_AUTO:
							autoChecking();
							break;
					}
				}
			}).create();
		dialog.show();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (flag) {
			if (event.getAction()==MotionEvent.ACTION_DOWN) {
				touchDown++;
			} else if (event.getAction()==MotionEvent.ACTION_UP) {
				touchUp++;
			} else if (event.getAction()==MotionEvent.ACTION_MOVE) {
				touchMove++;
			}
			secondText.setText(getString(R.string.touch_down) + touchDown + " " 
					+ getString(R.string.touch_up) + touchUp + " " 
					+ getString(R.string.touch_move) + touchMove);
		}
		return true;
	}

}
