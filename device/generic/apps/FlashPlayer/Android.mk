
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := com.adobe.flashplayer
LOCAL_SRC_FILES := adobe_flash_player_11.0.1.153.apk
#flash_player_oem.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/custom/prebuilt
LOCAL_CERTIFICATE := platform

include $(BUILD_PREBUILT)

