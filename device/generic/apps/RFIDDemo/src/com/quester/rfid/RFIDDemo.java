package com.quester.rfid;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.util.Log;

import java.math.BigInteger;


public class RFIDDemo extends Activity {
    
	static {
        System.loadLibrary("rfid");
    }

	private native boolean mifare_init();
    private native void mifare_free();

    private native int mifare_reqa();
    private native int mifare_get_snr(byte[] array);
    private native int mifare_read_card(byte addr, byte blks, byte[] key, byte auth, byte mode, byte[] array);
    private native int mifare_write_card(byte addr, byte blks, byte[] key, byte auth, byte mode, byte[] array);

    private native int sim_reset();
    private native int sim_enter_directory(Integer num, Integer max_size);
    private native int sim_read_directory(int index, int max_size, byte[] buf);
    private native int sim_enter_sms(Integer num, Integer max_size);
    private native int sim_read_sms(int index, int max_size, byte[] buf);

    private LinearLayout mLayout;
    private TextView showMesg;
    private static boolean mInitOK;
    private static boolean mScanner;
    private static boolean isFinish;
    private static HandlerThread mAsyncThrd;
    private static byte[] mSerial;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mLayout = (LinearLayout) findViewById(R.id.panel);
    	showMesg = (TextView) findViewById(R.id.mifare_get_snr_label);
		showMesg.setText(R.string.app_init);
		isFinish = true;
		mScanner = false;

		mAsyncThrd = new HandlerThread("scanner");
		mAsyncThrd.start();
		Handler mAsyncHandler = new Handler(mAsyncThrd.getLooper());
		mAsyncHandler.post(new Runnable() {
			public void run() {
				mInitOK = mifare_init();
				if (!mInitOK) {
					showErrorMesg();
				} else {
					mSerial = new byte[4];
					isFinish = false;
					while (!mScanner) {
						updateCanvas(mifare_get_snr(mSerial));
						try {
							Thread.sleep(1000);
						} catch (Exception e) {
							Log.w("rfid", e.getMessage());
						}
					}
					isFinish = true;
				}
			}
		});
    }

    protected void onDestroy() {
        super.onDestroy();
        mScanner = true;
        while (!isFinish) {
        	try {
        		Thread.sleep(100);
        	} catch (InterruptedException e) {
        		Log.w("rfid", e.getMessage());
        	}
        }
        mAsyncThrd.quit();
        if (mInitOK) mifare_free();
    }

	private void showErrorMesg() {
		runOnUiThread(new Runnable() {
			public void run() {
				showMesg.setText(R.string.init_failed);
			}
		});
	}

    private void updateCanvas(final int status) {
        runOnUiThread(new Runnable() {
            public void run() {
                if (status == 0) {
                	mLayout.setBackgroundColor(0xff0000ff);
                	showMesg.setText(dump(mSerial));
                }
                else if (status == 0x83) {
                	mLayout.setBackgroundColor(0xff000000);
                	showMesg.setText(R.string.app_standby);
                }
                else {
                	mLayout.setBackgroundColor(0xff000000);
                	showMesg.setText("Error status: " + String.format("%02x", status));
                }
            }
        });
    }

    private String dump(byte[] array) {
		if (array.length != 4) return getString(R.string.parse_failed);
		StringBuffer sb = new StringBuffer();
		try {
			byte firstByte[] = {0, array[0], array[1], array[2], array[3]};
			byte secondByte[] = {0, array[1]};
			byte lastByte[] = {0, array[2], array[3]};
			String tempStr = new BigInteger(firstByte).toString();
			for (int i = tempStr.length(); i < 10; i++) {
				sb.append("0");
			}
			sb.append(tempStr);
			sb.append(" ");
			tempStr = new BigInteger(secondByte).toString();			
			for (int i = tempStr.length(); i < 3; i++) {
				sb.append("0");
			}
			sb.append(tempStr);
			sb.append(",");
			tempStr = new BigInteger(lastByte).toString();
			for (int i = tempStr.length(); i < 5; i++) {
				sb.append("0");
			}
			sb.append(tempStr);
		} catch (NumberFormatException e) {
			//
		}
        return sb.toString();
    }

}
