#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <poll.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "zlg500b.h"

#include "log.h"
#ifndef LOG_TAG
#define LOG_TAG "rfid"
#endif
#include <cutils/properties.h>

#define POWER_ON_FILE "/sys/class/gpio/gpio138/value"
#define RFID_PORT "/dev/ttyS1"
#define BAUND_RATE B9600

static int serial_fd = -1;
static struct termios oldios,newios;

void dump(u8 v)
{
    #if 0 //def MIFARE_DEBUG
    static char buf[128] = {0};

    if (v == 0xaa)
        buf[0] = 0;

    int len = strlen(buf);
    if (len < 128)
        snprintf(&buf[len], 128-len, "%02x ", v);

    if (v == 0xbb) 
        LOGD("%s", buf);
    #endif
}

int serial_open()
{
	char value[PROPERTY_VALUE_MAX];    
    int fd;
    int ret;

    /* power on rfid/mf */
    fd = open(POWER_ON_FILE, O_RDWR);
    if (fd < 0)
    {
        LOGD("cant open %s: %d(%s)", POWER_ON_FILE, errno, strerror(errno));
        
    }else {

        char on = '1';
        ret = write(fd, &on, 1);
        if (ret != 1)
        {
            LOGD("power on failed: %d(%s)!", errno, strerror(errno));
            return FALSE;
        }

        close(fd);
        usleep(100*1000);
    }
    LOGD("power on ok.");

    property_get("persist.rfid.port", value, RFID_PORT);
    fd = open(value, O_RDWR | O_NOCTTY);
    if (fd < 0)
    {
        LOGD("open failed: %d(%s)", errno, strerror(errno));
        return FALSE;
    }

    tcgetattr( fd, &oldios );

    memcpy((void*) &newios,&oldios, sizeof(oldios));
    newios.c_cflag = BAUND_RATE | CS8 | CLOCAL | CREAD;
    newios.c_iflag = IGNPAR|IGNBRK;
    newios.c_oflag = 0;
    newios.c_lflag = 0;
    newios.c_cc[VTIME] = 1;
    newios.c_cc[VMIN] = 0;
    ret = tcsetattr(fd, TCSANOW, &newios);
    if (ret < 0)
    {
        LOGD("error setting terminal attr");
        return FALSE;
    }

    serial_fd = fd;
    return TRUE;
}

int serial_read()
{
    u8 v;
    int ret = read(serial_fd, &v, 1);
    if (ret != 1)
        return -1;
    else
    {
        dump(v);
        return v;
    }
}

int serial_write(int value)
{
    dump(value);

    u8 v = (u8) value;
    int ret = write(serial_fd, &v, 1);
    if (ret != 1)
        return -1;
    else
        return TRUE;
}

int serial_poll()
{
    struct pollfd pfd = {
        .fd = serial_fd,
        .events = POLLIN,
        .revents = 0,
    };
    int ret;

    ret = poll(&pfd, 1, 1000); /* 1s timeout */
    if (ret == 1 && (pfd.revents&POLLIN)) 
        return TRUE;
    else
        return FALSE;
}

int serial_flush()
{
    return 0;
}

void serial_close()
{
    int fd;
    int ret;

    tcsetattr(serial_fd, TCSANOW, &oldios);
    close(serial_fd);
    serial_fd = -1;

    /* power off rfid/mf */
    fd = open(POWER_ON_FILE, O_RDWR);
    if (fd < 0)
    {
        LOGD("cant open %s: %d(%s)", POWER_ON_FILE, errno, strerror(errno));

    }else {

        char off = '0';
        ret = write(fd, &off, 1);
        if (ret != 1)
        {
            LOGD("power off failed: %d(%s)!", errno, strerror(errno));
            return;
        }

        close(fd);
        LOGD("power off ok.");
    }
}
