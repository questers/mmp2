#include <stdio.h>
#include <stdlib.h>
#include <jni.h>

#include "log.h"
#include "zlg500b.h"

static char className[] = "com/quester/rfid/RFIDDemo";

char* buf2str(U8* buf, int len)
{
    int i;
    static char tmp[128] = {0};

    tmp[0] = 0;
    for (i=0; i<len; i++)
        snprintf(&tmp[strlen(tmp)], 128-strlen(tmp), "%02x ", buf[i]);
    
    return tmp;
}

int main(int argc, char* argv[])
{
    int ret;
    U8 num, max_size;
    U8 index;
    U8* buf;

    ret = MFInit();
    if (!ret) 
    {
        LOGD("MFInit failed");
        return -1;
    }
    LOGD("MFInit OK");

    u8 sn[4];
    ret = MFCheckCard(sn);
    if (!ret) 
    {
        LOGD("MFCheckCard failed");
        return -1;
    }
    LOGD("MFCheckCard OK");

    ret = Mifare_REQA(0x26);
    LOGD("Mifare_REQA: %02x", ret);

    ret = SIM_Reset();
    LOGD("SIM_Reset: %02x", ret);

    num, max_size;
    ret = SIM_EnterDirectory(&num, &max_size);
    LOGD("SIM_EnterDirectory: %02x, directory number: %02x, max size: %02x", ret, num, max_size);

    index = 1;
    buf = (U8*) malloc(max_size);
    ret = SIM_ReadDirectory(index, max_size, buf);
    LOGD("SIM_ReadDirectory: %02x", ret);
    if (!ret) LOGD("directory %d: %s", index, buf2str(buf, max_size));
    free(buf);

    ret = SIM_EnterSMS(&num, &max_size);
    LOGD("SIM_EnterSMS: %02x, directory number: %02x, max size: %02x", ret, num, max_size);

    index = 1;
    buf = (U8*) malloc(max_size);
    ret = SIM_ReadSMS(index, max_size, buf);
    LOGD("SIM_ReadSMS: %02x", ret);
    if (!ret) LOGD("sms %d: %s", index, buf2str(buf, max_size));
    free(buf);

    MFFree();

    return 0;
}


jboolean
jni_mifare_init(JNIEnv *env, jobject thiz) {
    LOGD("%s", __FUNCTION__);

    int ret;
    ret = MFInit();
    if (!ret) 
    {
        LOGD("MFInit failed");
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

void
jni_mifare_free(JNIEnv *env, jobject thiz) {
    LOGD("%s", __FUNCTION__);
    MFFree();
}

jint
jni_mifare_reqa(JNIEnv *env, jobject thiz) {
    LOGD("%s", __FUNCTION__);

    int ret;

    ret = Mifare_REQA(0x26);
    LOGD("Mifare_REQA: %02x", ret);
    /*
    u8 sn[4];
    ret = MFCheckCard(sn);
    if (!ret) 
    {
        LOGD("MFCheckCard failed");
        return JNI_FALSE;
    }*/

    return ret;
}

jint
jni_mifare_get_snr(JNIEnv *env, jobject thiz, jbyteArray array) {
    LOGD("%s", __FUNCTION__);

    int ret;

    u8 sn[4];
    ret = MFCheckCard(sn);
    if (!ret) 
    {
        int length = env->GetArrayLength(array);
        if (length < 4) return -1;

        unsigned char* buf = (U8*) env->GetByteArrayElements(array, NULL);
        memcpy(buf, sn, 4);
        env->ReleaseByteArrayElements(array, (jbyte*)buf, JNI_COMMIT);
    }

    return ret;
}

jint
jni_mifare_read_card(JNIEnv *env, jobject thiz, jbyte addr, jbyte blks, jbyteArray key, jbyte auth, jbyte mode, jbyteArray array) {
    int ret;

    int key_len = env->GetArrayLength(key);
    U8* key_ptr = (U8*) env->GetByteArrayElements(key, NULL);
    int array_len = env->GetArrayLength(array);
    U8* array_ptr = (U8*) env->GetByteArrayElements(array, NULL);
    unsigned int uid;

    if (key_len != 6 ||
        key_ptr == NULL ||
        array_len != (blks<<4) ||
        array == NULL)
    {
        LOGD("%s, invalid args", __FUNCTION__);
        return -1;
    }

    ret = Mifare_Read(addr, blks, key_ptr, auth, mode, array_ptr, &uid);

    env->ReleaseByteArrayElements(key, (jbyte*)key_ptr, JNI_COMMIT);
    env->ReleaseByteArrayElements(array, (jbyte*)array_ptr, JNI_COMMIT);

    return ret;
}

jint
jni_mifare_write_card(JNIEnv *env, jobject thiz, jbyte addr, jbyte blks, jbyteArray key, jbyte auth, jbyte mode, jbyteArray array) {
    int ret;

    int key_len = env->GetArrayLength(key);
    U8* key_ptr = (U8*) env->GetByteArrayElements(key, NULL);
    int array_len = env->GetArrayLength(array);
    U8* array_ptr = (U8*) env->GetByteArrayElements(array, NULL);
    unsigned int uid;

    if (key_len != 6 ||
        key_ptr == NULL ||
        array_len != (blks<<4) ||
        array == NULL)
    {
        LOGD("%s, invalid args", __FUNCTION__);
        return -1;
    }

    ret = Mifare_Write(addr, blks, key_ptr, auth, mode, array_ptr, &uid, 1);

    env->ReleaseByteArrayElements(key, (jbyte*)key_ptr, JNI_COMMIT);
    env->ReleaseByteArrayElements(array, (jbyte*)array_ptr, JNI_COMMIT);

    return ret;
}

jint
jni_sim_reset(JNIEnv *env, jobject thiz) {
    int ret;

    ret = SIM_Reset();
    LOGD("SIM_Reset: %02x", ret);

    return ret;
}

jint
jni_sim_enter_directory(JNIEnv *env, jobject thiz, jobject num, jobject max_size) {
    int ret;
    jclass c;
    jfieldID id;
    c = env->FindClass("java/lang/Integer");
    if (c==NULL)
    {
        LOGD("FindClass failed");
        return -1;
    }

    id = env->GetFieldID(c, "value", "I");
    if (id==NULL)
    {
        LOGD("GetFiledID failed");
        return -1;
    }

    U8 n, s;
    ret = SIM_EnterDirectory(&n, &s);
    LOGD("SIM_EnterDirectory: %02x, directory number: %02x, max size: %02x", ret, n, s);

    env->SetIntField(num, id, n);
    env->SetIntField(max_size, id, s);
    return 0;
}

jint
jni_sim_read_directory(JNIEnv *env, jobject thiz, jint index, jint max_size, jbyteArray array)
{
    int ret;

    int length = env->GetArrayLength(array);
    unsigned char* buf = (U8*) env->GetByteArrayElements(array, NULL);
    ret = SIM_ReadDirectory(index, max_size, buf);
    LOGD("SIM_ReadDirectory: %02x", ret);
    if (!ret) LOGD("directory %d: %s", index, buf2str(buf, max_size));
    env->ReleaseByteArrayElements(array, (jbyte*)buf, JNI_COMMIT);

    return ret;
}

jint
jni_sim_enter_sms(JNIEnv *env, jobject thiz, jobject num, jobject max_size) {
    jclass c;
    jfieldID id;
    c = env->FindClass("java/lang/Integer");
    if (c==NULL)
    {
        LOGD("FindClass failed");
        return -1;
    }

    id = env->GetFieldID(c, "value", "I");
    if (id==NULL)
    {
        LOGD("GetFiledID failed");
        return -1;
    }

    U8 n, s;
    int ret = SIM_EnterSMS(&n, &s);
    LOGD("SIM_EnterSMS: %02x, SMS number: %02x, max size: %02x", ret, n, s);

    env->SetIntField(num, id, n);
    env->SetIntField(max_size, id, s);
    return 0;
}

jint
jni_sim_read_sms(JNIEnv *env, jobject thiz, jint index, jint max_size, jbyteArray array)
{
    int ret;

    int length = env->GetArrayLength(array);
    unsigned char* buf = (U8*) env->GetByteArrayElements(array, NULL);
    ret = SIM_ReadSMS(index, max_size, buf);
    LOGD("SIM_ReadSMS: %02x", ret);
    if (!ret) LOGD("sms %d: %s", index, buf2str(buf, max_size));
    env->ReleaseByteArrayElements(array, (jbyte*)buf, JNI_COMMIT);

    return ret;
}

static JNINativeMethod methods[] = {
  {"mifare_init", "()Z", (void*) jni_mifare_init },
  {"mifare_free", "()V", (void*) jni_mifare_free },

  {"mifare_reqa", "()I", (void*) jni_mifare_reqa },
  {"mifare_get_snr", "([B)I", (void*) jni_mifare_get_snr },
  {"mifare_read_card", "(BB[BBB[B)I", (void*) jni_mifare_read_card },
  {"mifare_write_card", "(BB[BBB[B)I", (void*) jni_mifare_write_card },

  {"sim_reset", "()I", (void*) jni_sim_reset},
  {"sim_enter_directory", "(Ljava/lang/Integer;Ljava/lang/Integer;)I", (void*) jni_sim_enter_directory},
  {"sim_read_directory", "(II[B)I", (void*) jni_sim_read_directory},
  {"sim_enter_sms", "(Ljava/lang/Integer;Ljava/lang/Integer;)I", (void*) jni_sim_enter_sms},
  {"sim_read_sms", "(II[B)I", (void*) jni_sim_read_sms},
};

/*
 * Register several native methods for one class.
 */
static int registerNativeMethods(JNIEnv* env, const char* className,
    JNINativeMethod* gMethods, int numMethods)
{
    jclass clazz;

    clazz = env->FindClass(className);
    if (clazz == NULL) {
        LOGD("Native registration unable to find class '%s'", className);
        return JNI_FALSE;
    }
    if (env->RegisterNatives(clazz, gMethods, numMethods) < 0) {
        LOGD("RegisterNatives failed for '%s'", className);
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

/*
 * Register native methods for all classes we know about.
 *
 * returns JNI_TRUE on success.
 */
static int registerNatives(JNIEnv* env)
{
  if (!registerNativeMethods(env, className,
                 methods, sizeof(methods) / sizeof(methods[0]))) {
    return JNI_FALSE;
  }

  return JNI_TRUE;
}


// ----------------------------------------------------------------------------

/*
 * This is called by the VM when the shared library is first loaded.
 */
 
typedef union {
    JNIEnv* env;
    void* venv;
} UnionJNIEnvToVoid;

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    jint result = -1;
    JNIEnv* env = NULL;
    
    LOGD("JNI_OnLoad");

    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_4) != JNI_OK) {
        LOGD("ERROR: GetEnv failed");
        goto bail;
    }
    env = uenv.env;

    if (registerNatives(env) != JNI_TRUE) {
        LOGD("ERROR: registerNatives failed");
        goto bail;
    }

    result = JNI_VERSION_1_4;
    
bail:
    return result;
}
