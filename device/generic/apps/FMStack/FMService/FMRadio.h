/*
* (C) Copyright 2010 Marvell International Ltd.
* All Rights Reserved
*
* MARVELL CONFIDENTIAL
* Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
* The source code contained or described herein and all documents related to
* the source code ("Material") are owned by Marvell International Ltd or its
* suppliers or licensors. Title to the Material remains with Marvell International Ltd
* or its suppliers and licensors. The Material contains trade secrets and
* proprietary and confidential information of Marvell or its suppliers and
* licensors. The Material is protected by worldwide copyright and trade secret
* laws and treaty provisions. No part of the Material may be used, copied,
* reproduced, modified, published, uploaded, posted, transmitted, distributed,
* or disclosed in any way without Marvell's prior express written permission.
*
* No license under any patent, copyright, trade secret or other intellectual
* property right is granted to or conferred upon you by disclosure or delivery
* of the Materials, either expressly, by implication, inducement, estoppel or
* otherwise. Any license under such intellectual property rights must be
* express and approved by Marvell in writing.
*
*/


#ifndef FMRADIO_H
#define FMRADIO_H

#include <utils/Log.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <utils/threads.h>
#include <linux/videodev2.h>
#include <binder/IPCThreadState.h>
#include <media/AudioTrack.h>
#include <media/stagefright/AudioPlayer.h>
#include <media/stagefright/MediaDebug.h>
#include <media/stagefright/MediaDefs.h>
#include <media/stagefright/MediaSource.h>
#include <media/mediarecorder.h>
#include <media/AudioRecord.h>
#include <media/AudioSystem.h>
#include <media/stagefright/MetaData.h>
#include <linux/videodev2.h>
#include <linux/videodev.h>
#include <cutils/properties.h>
#include <utils/KeyedVector.h>


#include <media/AudioSystem.h>
#include <signal.h>
#include <poll.h>
#include "IFMRadioService.h"
#include "IFMRadioCallback.h"

namespace android{
#define ANDROID_SET_AID_AND_CAP
typedef struct
{
    char *ptr;
    mutable Mutex buffer_lock;
    bool buffer_filled;
}ringbuf;

class FMRadio : public BnFMRadioService, public IBinder::DeathRecipient {
public:
    enum mrv8787_cmd {    
        MRVL8787_CID_BASE = (V4L2_CID_PRIVATE_BASE),
        MRVL8787_SET_FM_BAND = 0x13,
        MRVL8787_SET_CHANNEL = 0x03,
        MRVL8787_GET_CHANNEL = 0x04,
        MRVL8787_GET_CURRENT_RSSI = 0x34,
        MRVL8787_SET_SEARCH_MODE = 0x09,
        MRVL8787_SET_SEARCH_DIRECTION = 0x07,
        MRVL8787_AUDIO_VOLUME = 0x65,
        MRVL8787_AUDIO_MUTE = 0x15,
        MRVL8787_STOP_SEARCH = 0x57
    };

    enum audio_buf_parameter {
        SAMPLE_RATE = 44100,
        CHANNEL_COUNT = 2,
        CHUNK_SIZE = (320*6*2),
        BUFFER_NUM = 32
    };
    // exported APIs
    bool isFMEnabled();
    int enable();
    int disable();
    int suspend();
    int resume();
    int scan_all();
    int stop_scan();
    int set_channel(unsigned int freq);
    int get_channel();
    int get_rssi();
    float get_volume();
    int set_volume(int volume);
    bool set_mute(bool flag);
    int set_band(int band);
    int scan_next();
    int scan_prev();
    int setSpeakerOn(bool on);
    int registerCallback(const sp<IFMRadioCallback>& callback);    
    int unregisterCallback(const sp<IFMRadioCallback>& callback);

    // internal stactic functions
    static void signal_action(int signum, siginfo_t *info, void *p);
    static void instantiate();
    static FMRadio* getInstance();
    
    // Protect mCallbacks.
    mutable Mutex   mCallbackLock;      
    KeyedVector<wp<IBinder>, sp<IFMRadioCallback> >  mCallbacks;    
    virtual     void        binderDied(const wp<IBinder>& who);    
    
   
private:
    FMRadio();
    ~FMRadio();

    bool isEnabled;
    bool isScanning;
    bool isSpeaker;
    static int mfd;
    unsigned int mMinFrequency;
    unsigned int mMaxFrequency;\
    // Protect all internal state.
    mutable Mutex mLock;
    // Only let one thread to stop scan
    mutable Mutex mLock_stopScan; 
    
    static Mutex mInstanceLock;
    static FMRadio* mInstance;
    static const char* radioDev;
 
    class EventThread;
    EventThread*  mEventThread;
#ifndef FM_NOT_USES_RECORD
    class FMRecordThread;
    FMRecordThread *mFMRecordThread;
    class FMPlayerThread;
    FMPlayerThread *mFMPlayerThread;
    static ringbuf g_ringbuffer[];     	
#endif	
    
    int radio_set_ctrl(int ctrl_type, int value);
    int radio_get_ctrl(int ctrl_type);
#ifdef ANDROID_SET_AID_AND_CAP    
    void android_set_aid_and_cap();
    void print_process_info();

#endif
};
};//end android
#endif // FMRADIO_H

