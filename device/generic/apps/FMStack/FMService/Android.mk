LOCAL_PATH:= $(call my-dir)

#
# libfmradio
#
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
	FMRadio.cpp \
	FMEventParser.cpp \
	IFMRadioService.cpp \
	IFMRadioCallback.cpp \
	Main_FMRadio.cpp

LOCAL_C_INCLUDES := \
        external/bluetooth/bluez/include \
        device/generic/components/libMarvellWireless

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils \
	libmedia \
        libbinder \
	libMarvellWireless

ifeq ($(strip $(FM_NOT_USES_RECORD)),true)
  LOCAL_CFLAGS += -DFM_NOT_USES_RECORD
endif

LOCAL_PRELINK_MODULE := false

LOCAL_MODULE := FMRadioServer
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)
