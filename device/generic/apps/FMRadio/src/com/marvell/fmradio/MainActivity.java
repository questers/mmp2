/*
 * (C) Copyright 2010 Marvell International Ltd.
 * All Rights Reserved
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2008 ~ 2010 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 */

package com.marvell.fmradio;
import android.view.Window;
import android.graphics.drawable.Drawable;
import android.content.Context;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ProgressBar;
import android.view.WindowManager;
import com.marvell.fmradio.list.BrowserListActivity;
import com.marvell.fmradio.list.EditStationView;
import com.marvell.fmradio.util.ChannelHolder;
import com.marvell.fmradio.util.LogUtil;
import com.marvell.fmradio.util.Station;
import java.lang.Runnable;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.os.PowerManager;
import android.media.AudioSystem;
import java.lang.reflect.Field;
import android.content.Intent;
import android.content.IntentFilter;

public class MainActivity extends Activity implements OnClickListener, OnTouchListener
{
	private final static String TAG = "FMRadioMainUI";
	public final static String WORK_INDEX = "work_index";
    public static int screenWidth = 0;
	private static final int REQ_BROWSER = 0;
	private static final int REQ_DELETE = 1;
	public static int enable;
	private static final int DIALOG_MSG_POWER_ON = 0;
	private static final int DIALOG_MSG_POWER_OFF = 1;	
	private static final int DIALOG_MSG_CLEAR_SCAN = 2;
	private static final int DIALOG_MSG_SCAN_WAIT = 3;	
	private static final int DIALOG_MSG_HEADSET = 4;
	private static final int DIALOG_SCAN_CLEAR = 5;
	private static final int DIALOG_MSG_OPEN_FAILED = 6;
	private static final int DIALOG_MSG_AIRPLANE_MODE = 7;
	private final static int[] mButtonID = 
	{
		R.id.new_btn_tune_left, R.id.new_btn_tune_right,
		R.id.new_btn_list, R.id.new_btn_tune_prev, R.id.new_btn_tune_next,
		R.id.new_btn_add
	};	
        private int scrolWidth = 0;	
        private boolean mIsHeadsetPlugged = false;
	private boolean mPowerOn = false;
	private boolean scrolOn = false;
	private boolean mMute = false;
	private boolean mSpeaker = false;
	private boolean mMono = false;
        public static ImageView rssi;
	private Button mScrol = null;
	private Button power = null;
	private ChannelHolder mChannelHolder = null;
    // Broadcast receiver for device connections intent broadcasts
    private final BroadcastReceiver mReceiver = new AudioServiceBroadcastReceiver();
	private AudioManager am = null;
	private TextView mChannelName = null;
	private TextView mScanning = null; 
	public static FrequencyView mFreqView = null;
	private FMCallback mCallback = null;
        private GestureDetector mGestureDetector = null;
        private AlertDialog ma = null;
	private AlertDialog maOn = null;
        private AlertDialog maScan = null;
	private PowerManager mPowerManager;
        private PowerManager.WakeLock mWakeLock;
	private ProgressBar pb,pbScan;
	public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final Context mContext = this.getApplicationContext();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(android.provider.Settings.System.getInt(getContentResolver(), android.provider.Settings.System.AIRPLANE_MODE_ON,0)!=0)
        {
            mHandler.sendMessage(Message.obtain(mHandler, MSG_AIRPLANE_MODE));
        } else {
        setContentView(R.layout.main_ui);
        mChannelHolder = ChannelHolder.getInstance();
        for (int i = 0; i < mButtonID.length; i++)
        {
           Button btn = (Button)findViewById(mButtonID[i]);
	        btn.setOnClickListener(this);
        }
	am = (AudioManager)getSystemService(AUDIO_SERVICE);
        mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "FMRadio");
	initRssi(); 
	mScrol = (Button)findViewById(R.id.new_btn_scrol);
	mScrol.setOnTouchListener(this);
	mGestureDetector = new GestureDetector(this, new ScrolGestureListener());
        power = (Button)findViewById(R.id.btn_power);
        power.setOnClickListener(this);
        DisplayMetrics dm = new DisplayMetrics();   
        getWindowManager().getDefaultDisplay().getMetrics(dm);   
        screenWidth = dm.widthPixels;   
        mFreqView = (FrequencyView) findViewById(R.id.frequency_view);
        mFreqView.setHandler(mHandler, MSG_UPDATE_RULER);
	mScanning = (TextView) findViewById(R.id.scan);
        String name = mChannelHolder.getWorkFreqName();
        setWorkFreq(name, mChannelHolder.mWorkFreq); 
        mCallback = FMCallback.getInstance();
        mCallback.setHandler(mHandler);
	FMService.setHandler(mHandler);
	enableProgressBar();
	startServiceByAction(FMService.WAITFOR_ACTION);
    // Register for device connection intent broadcasts.
    IntentFilter intentFilter =
        new IntentFilter(Intent.ACTION_HEADSET_PLUG);

    mContext.registerReceiver(mReceiver, intentFilter);
        }
	
    }
     private void enableProgressBar(){

	showDialog(DIALOG_MSG_POWER_ON);
	Field field;
	        try {

		field = maOn.getClass().getSuperclass().getDeclaredField("mShowing" );		
		field.setAccessible( true );
						            
		field.set(maOn, false );
		}
		catch (Exception e)
		{
		}
	pb = (ProgressBar)maOn.findViewById(R.id.power_percent); 
        pb.setProgress(0);
	Thread t = new Thread(){

	public void run(){
									 
	int i = 0;
	while(i<=100){
		if(pb!=null){

		
		pb.incrementProgressBy(1);
		i++;
		}
		else
			break;
	try{
	Thread.sleep(60);       } 
	catch (InterruptedException e) {

		// TODO Auto-generated catch block
	e.printStackTrace();                        
	}
       }
     }
     };
	t.start();
          }
    private void inspectAirplane(){
	Runnable eventHandler = new Runnable()
		{
		public void run()
		 {
			 while(true)
			 {
				 if(android.provider.Settings.System.getInt(getContentResolver(), android.provider.Settings.System.AIRPLANE_MODE_ON,0)!=0)
				 {
				mHandler.sendMessage(Message.obtain(mHandler, MSG_FINISH));
				 }
            	try{
            	    Thread.sleep(10);       } 
            	catch (InterruptedException e) {

            		// TODO Auto-generated catch block
            	    e.printStackTrace();                        
            	}
		}
		}
		};
	Thread watcher = new Thread(eventHandler);
	watcher.start();    
    }
    private void initRssi(){
        rssi = (ImageView)findViewById(R.id.rssi);
    }
    private void showNotification() {
	NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	Intent intent = new Intent(getApplicationContext(),MainActivity.class);
intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		        | Intent.FLAG_ACTIVITY_NEW_TASK);
	PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(),0,intent, 0); 
	Notification mNotification = new Notification(
		 R.drawable.fm_app_icon,  "FMRadio is on!", 
			            System.currentTimeMillis());
	mNotification.setLatestEventInfo(
		 getApplicationContext(),"FMRadio","FMRadio is running",contentIntent);
	mNotificationManager.notify(1, mNotification);
    }
    private void startServiceByAction(String action){

	Intent mIntent = new Intent(this,FMService.class);
	mIntent.setAction(action);
	if(action==FMService.MUTE_ACTION)
	mIntent.putExtra("mute", mMute);
	else if(action==FMService.SPEAKER_ACTION)
	mIntent.putExtra("speaker", mSpeaker);
	this.startService(mIntent);
    }

    private void startServiceByActionAndValue(String action, boolean enable){

	Intent mIntent = new Intent(this,FMService.class);
	mIntent.setAction(action);
	if(action==FMService.MUTE_ACTION)
	mIntent.putExtra("mute", enable);
	else if(action==FMService.SPEAKER_ACTION)
	mIntent.putExtra("speaker", enable);
	this.startService(mIntent);
    }
    private void operateScrol(MotionEvent e1, MotionEvent e2){

	mScrol.setPressed(!scrolOn);
	scrolOn=!scrolOn;
	scrolWidth = mScrol.getWidth();
	if(e2.getX()>e1.getX()){

	   float f;
							          
		if(e2.getX()<=(float)scrolWidth)
		   f = e2.getX()-e1.getX();
		else
		   f = scrolWidth-e1.getX();
		if(screenWidth==800)
		mChannelHolder.mWorkFreq += ((int)f/12)*100;
		else
		mChannelHolder.mWorkFreq += ((int)f/6)*100;
		if(mChannelHolder.  mWorkFreq<=108500)
		{
		startServiceByAction(FMService.SETCHANNEL_ACTION);   
		}
		else{
			mChannelHolder.mWorkFreq = 108500;
		startServiceByAction(FMService.SETCHANNEL_ACTION);
		}
	}
	 else if(e2.getX()<e1.getX()){

		float f;
		if(e2.getX()>=0)
		   f = e1.getX()-e2.getX();
		else
		   f = e1.getX();
		if(screenWidth==800)
			mChannelHolder.mWorkFreq -= ((int)f/12)*100;
		else
			mChannelHolder.mWorkFreq -= ((int)f/6)*100;
		if(mChannelHolder.mWorkFreq>=87500)
		{
			startServiceByAction(FMService.SETCHANNEL_ACTION);
		}
		else{
			mChannelHolder.mWorkFreq = 87500;
			startServiceByAction(FMService.SETCHANNEL_ACTION);
		} 
	 }			 
    }

    class ScrolGestureListener extends GestureDetector.SimpleOnGestureListener {
                    @Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,  float velocityY)
		{
                              operateScrol(e1,e2);
			      return true;
			}
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
			operateScrol(e1,e2);
			return true;
			}
    }
    public void onClick(View v)
    {
		switch (v.getId())
		{
		case R.id.btn_power:
			if (! mPowerOn)
			{
                                if (!mWakeLock.isHeld()){
                                    mWakeLock.acquire();
                                }
				WindowManager.LayoutParams lp = getWindow().getAttributes();  
								                                 
				lp.screenBrightness = 0.1f;
				getWindow().setAttributes(lp);
						                                
				startServiceByAction(FMService.ENABLE_ACTION);
				lp.screenBrightness = 1.0f;   
				getWindow().setAttributes(lp);
				enableProgressBar();
			}
			else
			{
				mPowerOn = false;
				Drawable off = getResources().getDrawable(R.drawable.power);
				power.setBackgroundDrawable(off);
				showDialog(DIALOG_MSG_POWER_OFF);
                                startServiceByAction(FMService.DISABLE_ACTION);
				NotificationManager mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.cancel(1); 
				FrequencyViewListener.on = false;
		                mScrol.setClickable(false);
				rssi.setBackgroundResource(R.drawable.xinhao_00);

                                if (mWakeLock.isHeld()){
                                    mWakeLock.release();
                                }
			}
			break;
            
		case R.id.new_btn_list:				
                        if (!mPowerOn) {
                             break;
                        }
			BrowserListActivity.startActivity(MainActivity.this, REQ_BROWSER);
			break;

		case R.id.new_btn_tune_prev:
                        if (!mPowerOn) {
                             break;
                        }
			mScanning.setVisibility(View.VISIBLE);
			startServiceByAction(FMService.SCANPREV_ACTION);
			break;
			
		case R.id.new_btn_tune_next:
                        if (!mPowerOn) {
                             break;
                        }
			mScanning.setVisibility(View.VISIBLE);
			startServiceByAction(FMService.SCANNEXT_ACTION);
			break;
			
		case R.id.new_btn_add:	
                        if (!mPowerOn) {
                             break;
                        }
			EditStationView.startActivity(MainActivity.this, REQ_BROWSER, -1, String.valueOf(mChannelHolder.mWorkFreq));
			break;
			
		case R.id.new_btn_tune_left:
                        if (!mPowerOn) {
                             break;
                        }
			if (87500 < mChannelHolder.mWorkFreq)
			{
			    mScrol.setPressed(!scrolOn);
			    scrolOn=!scrolOn;
				mChannelHolder.mWorkFreq -= 100;
			startServiceByAction(FMService.SETCHANNEL_ACTION); 
			}
			break;
			
		case R.id.new_btn_tune_right:
                        if (!mPowerOn) {
                             break;
                        }
			if (mChannelHolder.mWorkFreq < 108500)
			{
			    mScrol.setPressed(!scrolOn);
			    scrolOn=!scrolOn;
				mChannelHolder.mWorkFreq += 100;
			startServiceByAction(FMService.SETCHANNEL_ACTION); 
			}
			break;				

		default:
			break;
		}
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
    	super.onCreateOptionsMenu(menu);
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.main_ui, menu);
    	return true;
    }
    
    
    public boolean onPrepareOptionsMenu(Menu menu) 
    {
    	MenuItem item = menu.findItem(R.id.menu_set_mute);
    	
    	if (mMute)
    	{
    		item.setIcon(R.drawable.icon_menu_unmute);
    		item.setTitle(R.string.unmute);
    	}
    	else
    	{
    		item.setIcon(R.drawable.icon_menu_mute);
    		item.setTitle(R.string.mute);
    	}    	
    	
    	item = menu.findItem(R.id.menu_set_mono);
    	
    	if (mMono)
    	{
    		item.setIcon(R.drawable.icon_menu_stereo);
    		item.setTitle(R.string.stereo);
    	}
    	else
    	{
    		item.setIcon(R.drawable.icon_menu_mono);
    		item.setTitle(R.string.mono);
    	}    	
    	
    	item = menu.findItem(R.id.menu_set_speaker);
    	
    	if (mSpeaker)
    	{
    		item.setIcon(R.drawable.icon_menu_headset);
    		item.setTitle(R.string.headset);
    	}
    	else
    	{
    		item.setIcon(R.drawable.icon_menu_speaker);
    		item.setTitle(R.string.speaker);
    	}    	
    	
    	return super.onPrepareOptionsMenu(menu);
    }

	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case R.id.menu_set_mute:
			mMute = ! mMute;
            // Only set/unset mute when headset is on,
            // otherwize FM is muted.
            if(am.isWiredHeadsetOn() == true)
			    startServiceByAction(FMService.MUTE_ACTION);
			return true;
			
		case R.id.menu_set_mono:
			mMono = ! mMono;
			return true;
			
		case R.id.menu_set_speaker:
			mSpeaker = ! mSpeaker;
			startServiceByAction(FMService.SPEAKER_ACTION);
			return true;
			
		case R.id.menu_scan_save:
			showDialog(DIALOG_MSG_CLEAR_SCAN);
			return true;

		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		switch (keyCode)
		{
		case KeyEvent.KEYCODE_BACK:
			if(!mPowerOn){
				Intent i = new Intent(this, FMService.class);
				this.stopService(i);
				finish();
			}
			else
			moveTaskToBack(true);
			return true;

		default:
			break;
		}

		return super.onKeyDown(keyCode, event);
	}	
	
	private final static int MSG_UPDATE_RULER = 0;

	private final static int MSG_SCAN = 2;
	private final static int MSG_TUNE = 1;
	private final static int MSG_ENABLE = 3;
	private final static int MSG_SCAN_FAILED = 4;
	private final static int MSG_SCANALL = 5;
	private final static int MSG_SETCHANNEL = 9;
	private final static int MSG_FINISH = 8;
	private final static int MSG_DISABLE = 10;
	private final static int MSG_DISMISS = 6;
	private final static int MSG_INITIATE = 7;
	private final static int MSG_AIRPLANE = 11;
	private final static int MSG_AIRPLANE_MODE = 12;
	private final static int MSG_DISMISS_SCAN = 15;
	private static final int EVENT_WIRED_HEADSET_PLUG = 1;

	private final Handler mHandler = new Handler()
	{
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
			case MSG_UPDATE_RULER:
				updateRuler(msg);
				break;
			case MSG_TUNE:
				String name = mChannelHolder.getWorkFreqName();		
				setWorkFreq(name, mChannelHolder.mWorkFreq);
				break;
			case MSG_SCAN:
				startServiceByAction(FMService.GETCHANNEL_ACTION);
				mScanning.setVisibility(View.INVISIBLE);
				break;
			case MSG_SCAN_FAILED:
				mScanning.setVisibility(View.INVISIBLE);
				break;
			case MSG_INITIATE:
				inspectAirplane();
				startServiceByAction(FMService.INITIATE_ACTION);
				break;
			case MSG_ENABLE:
				Field field1;
				try {
			        field1 = maOn.getClass().getSuperclass().getDeclaredField("mShowing");		                
				field1.setAccessible( true );
		
			        field1.set(maOn, true);
		                 }
				catch (Exception e)
				{

				}
				maOn.dismiss();
				if(pb!=null){

				pb.setProgress(0);   
				pb = null;
				}
				if(enable == -1){
					showDialog(DIALOG_MSG_OPEN_FAILED);
					mPowerOn = false;
					FrequencyViewListener.on = false;
				}
				 else{
				mPowerOn =true;
				Drawable on = getResources().getDrawable(R.drawable.power_on);
				power.setBackgroundDrawable(on);
				showNotification();
				FrequencyViewListener.on = true;
				mScrol.setClickable(true);
                // Mute FM if found headset was unpluged.
                if(am.isWiredHeadsetOn() == false){
                    showDialog(DIALOG_MSG_HEADSET);
                    startServiceByActionAndValue(FMService.MUTE_ACTION, true);
                }
				startServiceByAction(FMService.SETCHANNEL_ACTION); 
				 }
				break;
			case MSG_SCANALL:
				 if(maOn!=null){
					Field field2;
					try {

					field2 = maOn.getClass().getSuperclass().                    getDeclaredField("mShowing");					
					field2.setAccessible( true );
					field2.set(maOn, true);
					}
					catch (Exception e)
					{
					}
					maOn.dismiss();
					if(pb!=null){
					pb.setProgress(0);   
					pb = null;
					}
				}
				showDialog(DIALOG_SCAN_CLEAR);
				pbScan = (ProgressBar)maScan.findViewById(R.id.power_percent);   
				pbScan.setProgress(0);
				Thread t = new Thread(){
				public void run(){
					int i = 0;
					while(i<=100){
					 if(!maScan.isShowing()){

					pbScan.setProgress(0);
					startServiceByAction(FMService.STOPSCAN);
					break;
					}
					pbScan.incrementProgressBy(1);
					i++;
				try 
				{								
				sleep(100);
				}
				catch (InterruptedException e) {

				// TODO Auto-generated catch block
				e.printStackTrace();
				}
				}
				}
				};
				 t.start();
				for (int pos = mChannelHolder.getCount() - 1 ; 0 <= pos ; pos--) 
		                {
					mChannelHolder.delete(pos);
				}
				setWorkFreq(null, mChannelHolder.mWorkFreq);
				startServiceByAction(FMService.SCANALL);
				break;
			case MSG_SETCHANNEL:
				startServiceByAction(FMService.SETCHANNEL_ACTION);
				break;
			case MSG_FINISH:
				mPowerOn = false;
				NotificationManager mNotificationManager =		    (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.cancel(1); 
				Intent mIntent = new Intent(MainActivity.this,FMService.class);  
				stopService(mIntent);
				finish();
				System.exit(0);	
				break;
			case MSG_DISABLE:
				startServiceByAction(FMService.DISABLE_ACTION);
				break;
			case MSG_AIRPLANE_MODE:
				showDialog(DIALOG_MSG_AIRPLANE_MODE);
				break;
			case MSG_DISMISS_SCAN:
				if(maScan!=null){

				if(maScan.isShowing()){
				maScan.dismiss();
				pbScan.setProgress(0);
				}
				}
				break;
			case MSG_DISMISS:
				if(ma!=null){
				if(ma.isShowing())
				ma.dismiss();
				break;
				}
				if(maOn!=null){
				Field field;
				try {

					 
					field = maOn.getClass().getSuperclass().getDeclaredField("mShowing" );
					field.setAccessible( true );
					 
					field.set(maOn, true);
				}
				catch (Exception e)
				{

				}
				maOn.dismiss();
				if(pb!=null){
				pb.setProgress(0);   
				pb = null;
				}
				}
				 if(enable == -1){
				showDialog(DIALOG_MSG_OPEN_FAILED);
				mPowerOn = false;
				FrequencyViewListener.on = false;
				Drawable off = getResources().getDrawable(R.drawable.power);
				power.setBackgroundDrawable(off);
				 }
				 else{
					if(am.isWiredHeadsetOn() == true){
						startServiceByAction(FMService.MUTE_ACTION);
						startServiceByAction(FMService.SPEAKER_ACTION);
					}
					else{
						showDialog(DIALOG_MSG_HEADSET);
						startServiceByActionAndValue(FMService.MUTE_ACTION, true);
					}
					mPowerOn = true;
					Drawable on = getResources().getDrawable(R.drawable.power_on);
					power.setBackgroundDrawable(on);
					showNotification();
					FrequencyViewListener.on = true;
					startServiceByAction(FMService.SETCHANNEL_ACTION); 
									                                                                if (!mWakeLock.isHeld()){
					mWakeLock.acquire();
																	}
				}
				break;
			default:
				break;

			}
		}
		
		private void updateRuler(Message msg)
		{
			mChannelHolder.mWorkFreq = msg.arg1;
			String name = mChannelHolder.getWorkFreqName();
			setWorkFreq(name, mChannelHolder.mWorkFreq);
		}
	};
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
	    
		switch (requestCode)
		{
		case REQ_BROWSER:
		    
			if (resultCode == RESULT_CANCELED)
				break;
			if(data!=null){
			int index = data.getExtras().getInt(WORK_INDEX);
			
			Station item = mChannelHolder.getItem(index);	
			
			mChannelHolder.mWorkFreq = Integer.valueOf(item.mFreq);
			startServiceByAction(FMService.SETCHANNEL_ACTION); 
			setWorkFreq(item.mName, mChannelHolder.mWorkFreq);}
			else{
                            startServiceByAction(FMService.SETCHANNEL_ACTION); 
			    setWorkFreq(null, mChannelHolder.mWorkFreq);
			}
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id)
	{
		switch (id)
		{
		case DIALOG_MSG_POWER_ON:
			return createPowerOnDialog();
			
		case DIALOG_MSG_POWER_OFF:
			return createPowerOffDialog();
		
		case DIALOG_MSG_CLEAR_SCAN:
			return createClearScanDialog();
			
		case DIALOG_MSG_SCAN_WAIT:
			return createScanWaitDialog();
			
		case DIALOG_MSG_HEADSET:
			return createHeadsetDialog();
		
		case DIALOG_SCAN_CLEAR:
			return createScanClearDialog();
	
		case DIALOG_MSG_OPEN_FAILED:
			return createOpenFailedDialog();

		case DIALOG_MSG_AIRPLANE_MODE:
			return createAirplaneDialog();
		default:
			return null;
		}
	}

	private AlertDialog createAirplaneDialog()
		    {

			LayoutInflater factory = LayoutInflater.from(this);
			AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(this);
			 return mAlertDialog.setView(factory.inflate(R.layout.dialog_airplane, null)).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int whichButton) {
			mHandler.sendMessage(Message.obtain(mHandler, MSG_FINISH));
					 }}).create();
			 
		    }
	private AlertDialog createOpenFailedDialog()
		{

		LayoutInflater factory = LayoutInflater.from(this);
		AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(this);
		mAlertDialog.setView(factory.inflate(R.layout.dialog_open_failed, null));
					            
		return mAlertDialog.create();
					                    
		}
	private AlertDialog createScanClearDialog()
		{
	         
		LayoutInflater factory = LayoutInflater.from(this);
		AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(this);
		mAlertDialog.setView(factory.inflate(R.layout.dialog_scan_clear, null));
				
		return maScan = mAlertDialog.create();
				    
		}

	private AlertDialog createHeadsetDialog()
	{
		LayoutInflater factory = LayoutInflater.from(this);
		AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(this);
		mAlertDialog.setView(factory.inflate(R.layout.headset_on, null));
						            
		return mAlertDialog.create();
	}
	
	private AlertDialog createPowerOnDialog()
	{		
		if(maOn!=null)
					return maOn;
				else
							{

							
	        LayoutInflater factory = LayoutInflater.from(this);
		AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(this);
		mAlertDialog.setView(factory.inflate(R.layout.dialog_power_on, null));
		
		return maOn = mAlertDialog.create();
							}
		
	}
	
	private AlertDialog createPowerOffDialog()
	{		
		LayoutInflater factory = LayoutInflater.from(this);
		AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(this);
		mAlertDialog.setView(factory.inflate(R.layout.dialog_power_off, null));
		
		return ma = mAlertDialog.create();
	}
	
	private AlertDialog createClearScanDialog()
	{		
		LayoutInflater factory = LayoutInflater.from(this);
		AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(this);
		mAlertDialog.setTitle(R.string.app_title);
		
		return mAlertDialog.setView(factory.inflate(R.layout.dialog_clear_scan, null)).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if(mPowerOn){
		    showDialog(DIALOG_SCAN_CLEAR);
		for (int pos = mChannelHolder.getCount() - 1 ; 0 <= pos ; pos--) 
                {
                 mChannelHolder.delete(pos);
                 }
                setWorkFreq(null, mChannelHolder.mWorkFreq);
		 mScanning.setVisibility(View.VISIBLE);
		startServiceByAction(FMService.SCANALL);
		 pbScan = (ProgressBar)maScan.findViewById(R.id.power_percent);   
		pbScan.setProgress(0);
		 Thread t = new Thread(){

		public void run(){
                int i = 0;
                while(i<=100){
			if(!maScan.isShowing()){
			pbScan.setProgress(0);
			mHandler.sendMessage(Message.obtain(mHandler, MSG_SCAN_FAILED));
			startServiceByAction(FMService.STOPSCAN);
			break;
			}
			pbScan.incrementProgressBy(1);
			i++;
                try {

		sleep(100);
		} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
         	  }
		}
		};
		t.start();
            }
	    }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create();
		
	}	
    
	
	private AlertDialog createScanWaitDialog()
	{		
		LayoutInflater factory = LayoutInflater.from(this);
		AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(this);
		mAlertDialog.setView(factory.inflate(R.layout.dialog_scanning, null));
		
		return mAlertDialog.create();
	}
    
    private void setWorkFreq(String name, int freq)
    {if(name!=null)
        mFreqView.setName(name);
    else
        mFreqView.setName("");
    
     mFreqView.setFrequency(freq);
    }

    public boolean onTouch(View v, MotionEvent event) {
            return mGestureDetector.onTouchEvent(event);
    }

    /**
     * Receiver for headset intent broadcasts the FM app cares about.
     */
    private class AudioServiceBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", 0);
                int microphone = intent.getIntExtra("microphone", 0);
                if (state == 0 && mPowerOn){
                    // headset unplug
                    showDialog(DIALOG_MSG_HEADSET);
                    // Mute FM
                    if (mMute == false){
                        startServiceByActionAndValue(FMService.MUTE_ACTION, true);
                    }
                }
                else if (state == 1 && mPowerOn){
                    startServiceByActionAndValue(FMService.MUTE_ACTION, mMute);
                }
            }
        }
    }

}
