package com.quester.cam;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.ImageView;

public class Cam extends Activity 
{
	private final String ACTION_NAME = "ACTION_UPDATE_IMAGEVIEW";
	private ImageView mImageView;
	private CamView mCamView;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        registerBroadcastReceiver();
		mCamView = new CamView(this);
        mImageView = (ImageView)findViewById(R.id.myView);
          
    }
    @Override
    protected void onDestroy() 
    {
    	super.onDestroy();
    	unregisterReceiver(mBroadcastReceiver);
    	mCamView.mDone = true;
    }
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(ACTION_NAME)) {
				mImageView.setImageBitmap(mCamView.mBitmap);				
			}
		}
	};
	public void registerBroadcastReceiver() {
		IntentFilter myFilter = new IntentFilter();
		myFilter.addAction(ACTION_NAME);
		registerReceiver(mBroadcastReceiver, myFilter);
	}
}
