package com.quester.cam;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.util.Log;
import android.os.Handler;
import android.os.HandlerThread;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CamView extends View
{
	static final int WIDTH = 640;
	static final int HEIGHT = 480;

	int mCount = 0;
    byte[] mImgBuf = new byte[WIDTH*HEIGHT];
    Handler mUiHandler;
    Handler mAsyncHandler;
    public boolean mDone = false;

    native int zkfpi_init();
    native int zkfpi_select(int i);
    native int zkfpi_open();
    native int zkfpi_close();
    native int zkfpi_get_image(byte[] buf);

    static {
        System.loadLibrary("usbcam");
    }

	Bitmap mBitmap;
	Context mContext;
	
	public CamView(Context context)
	{
		super(context);
		mContext = context;

		initCapture();
	}

	public void initCapture()
    {
        mUiHandler = new Handler();

        HandlerThread thr = new HandlerThread("capture");
        thr.start();
        mAsyncHandler = new Handler(thr.getLooper());

        mAsyncHandler.post(new Runnable() {
            public void run() {
                int ret;
                ret = zkfpi_init();
                if (ret <= 0)
                {
                    Log.d("usbcam", "No usb fingerprint scaner found. Abort.");
                    postHardwareError();
                    return;
                }

                zkfpi_select(0); /* low sensor */
                ret = zkfpi_open();
                if (ret != 0)
                {
                    Log.d("usbcam", "Opening usb fingerprint scanner failed. Abort.");
                    postHardwareError();
                    return;
                }

                while (!mDone)
                {
                    if (zkfpi_get_image(mImgBuf) == 0)
                        postUpdateCanvasMessage();
                }

                zkfpi_close();
            }
        });
    }

    public void postUpdateCanvasMessage()
    {
        mUiHandler.post(new Runnable() {
            public void run() {
                updateCanvas();
            }
        });
    }

    public void postHardwareError()
    {
        mUiHandler.post(new Runnable() {
            public void run() {
                Toast.makeText(mContext, "Hardware error!", Toast.LENGTH_LONG).show();
            }
        });
    }

	public void updateCanvas()
    {
		try {
			InputStream ip = getResources().openRawResource(R.drawable.test_640x480_24bit);
			byte[] buffer = stream2Bytes(ip);
			System.arraycopy(mImgBuf, 0, buffer, (buffer.length - mImgBuf.length), mImgBuf.length);
			Bitmap tmpBitmap = bytes2Bitmap(buffer);
			ip.close();
			
			mBitmap = Bitmap.createBitmap(tmpBitmap.getWidth(), tmpBitmap.getHeight(), Bitmap.Config.RGB_565);
			Canvas canvas = new Canvas(mBitmap);
			Paint paint = new Paint();
			canvas.drawBitmap(tmpBitmap, 0, 0, paint);
			
			/*
			File file = new File(String.format("/data/pic%03d.bmp", mCount++));
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(buffer, 0, buffer.length);
			fos.close(); */
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Intent mIntent = new Intent("ACTION_UPDATE_IMAGEVIEW");
		getContext().sendBroadcast(mIntent);
    }
	
	/**
	 * 通过数据流得到位图数组
	 * @param is
	 * @return
	 */
	public byte[] stream2Bytes(InputStream is) {
		ByteArrayOutputStream os = new ByteArrayOutputStream(1024);
		byte[] buffer = new byte[1024];
		int len;
		try {
			while ((len = is.read(buffer)) >= 0) {
				os.write(buffer, 0, len);
			}
		} catch (IOException e) {
			
		}
		return os.toByteArray();
	}
	
	/**
	 * 通过位图数组得到位图
	 * @param data
	 * @return
	 */
	public Bitmap bytes2Bitmap(byte[] data) {
		if (data.length != 0) {
			return BitmapFactory.decodeByteArray(data, 0, data.length);
		} else {
			return null;
		}
	}
	
	/**
	 * 将位图转换为位图数组
	 * @param bitmap
	 * @return
	 */
	public byte[] bitmap2Bytes(Bitmap bitmap) {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
		return os.toByteArray();
	}
	
	
	public void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
	}
}
