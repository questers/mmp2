#if !defined(CAM_H)
#define CAM_H

#include "log.h"

int cam_open();
int cam_close();
int cam_select(int index);
int cam_init();
int cam_get_image(u8* out_buffer, int out_buffer_size);

#endif
