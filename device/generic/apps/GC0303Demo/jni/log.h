#if !defined QST_LOG_H
#define QST_LOG_H

#define LOG_TAG "usbcam"
#include <android/log.h>

#define ENABLE_LOGGING

#if defined(ENABLE_LOGGING)
//#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGD(...)  do {printf(__VA_ARGS__);printf("\n");} while (0)
#else
#define  LOGD(...)
#endif

#define DBG(fmt, args...) LOGD("%s:%d, " fmt, __FUNCTION__, __LINE__, ##args);

#define BYTE unsigned char
#define WORD unsigned short
#define DWORD unsigned int
#define UINT unsigned int
#define BOOL int

#define u8 unsigned char

#endif // QST_LOG_H
