#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <jni.h>
//#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>

#include "log.h"
#include "cam.h"

#define U8 unsigned char

#define ASSERT(b) \
do \
{ \
    if (!(b)) \
    { \
        LOGD("error on %s:%d", __FUNCTION__, __LINE__); \
        return 0; \
    } \
} while (0)

#define C5515_VID 0x1b55
#define C5515_PID 0x0830
#define CAM_BUF_SZ 640*480*2
#define CMD_INIT 0xE0
#define CMD_GET_IMAGE 0xE5
#define TIME_OUT 2500
struct JNI_CONTROL_BLOCK
{
    JNIEnv* env;
    jobject obj;
    jmethodID notifyDataReady;
} jcb;


static struct libusb_device_handle *devh = NULL;
static struct libusb_transfer *img_transfer = NULL;
static unsigned char buf[CAM_BUF_SZ];
static char ep_bulk = 0x81;

static const char* myClassName="com/quester/cam/CamView";

int main()
{
    int i;
    int ret;

    ret = cam_open();
    ASSERT(ret==0);

    ret = cam_select(0);
    ASSERT(ret==0);

    ret = cam_init();
    ASSERT(ret==0);

    int count = 0;
    while (1)
    {
        ret = cam_get_image(buf, CAM_BUF_SZ);
        ASSERT(ret==0);

        char tmp[64] = {"---\n"};
        for (i=0; i<16; i++)
            sprintf(&tmp[strlen(tmp)], "%02x ", buf[i]);
        LOGD("%s", tmp);

        char filename[32];
        sprintf(filename, "/data/cam/%05d.yuv", count++);
        int fd = open(filename, O_RDWR | O_CREAT);
        if (fd >= 0)
        {
            write(fd, buf, CAM_BUF_SZ);
            close(fd);
        }
        else
        {
            LOGD("open() failed: %d(%s)", errno, strerror(errno));
        }
    }

    ret = cam_close();
    ASSERT(ret==0);

    return 0;
}


/* return sensor count */
jint
jni_zkfpi_init(JNIEnv *env, jobject thiz) {
    int ret;

    ret = cam_open();
    if (ret == 0) return 1;
    else return -1;
}

jint
jni_zkfpi_select(JNIEnv *env, jobject thiz, jint i) {
    return cam_select(i);
}

jint
jni_zkfpi_open(JNIEnv *env, jobject thiz) {
    /* nothing done */
    return 0;
}

jint
jni_zkfpi_close(JNIEnv *env, jobject thiz) {
    LOGD("%s", __FUNCTION__);

    return cam_close();
}

jint
jni_zkfpi_get_image(JNIEnv *env, jobject thiz, jbyteArray array) {
    LOGD("%s", __FUNCTION__);

    int ret;
    int length = env->GetArrayLength(array);
    U8* ptr = (U8*) env->GetByteArrayElements(array, NULL);
    ret = cam_get_image(ptr, length);
    env->ReleaseByteArrayElements(array, (jbyte*)ptr, JNI_COMMIT);

    return ret;
}

static JNINativeMethod methods[] = {
  {"zkfpi_init", "()I", (void*)jni_zkfpi_init },
  {"zkfpi_select", "(I)I", (void*)jni_zkfpi_select },
  {"zkfpi_open", "()I", (void*)jni_zkfpi_open },
  {"zkfpi_close", "()I", (void*)jni_zkfpi_close },
  {"zkfpi_get_image", "([B)I", (void*)jni_zkfpi_get_image },
};

/*
 * Register several native methods for one class.
 */
static int registerNativeMethods(JNIEnv* env, const char* className,
    JNINativeMethod* gMethods, int numMethods)
{
    jclass clazz;

    clazz = env->FindClass(className);
    if (clazz == NULL) {
        LOGD("Native registration unable to find class '%s'", className);
        return JNI_FALSE;
    }
    if (env->RegisterNatives(clazz, gMethods, numMethods) < 0) {
        LOGD("RegisterNatives failed for '%s'", className);
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

/*
 * Register native methods for all classes we know about.
 *
 * returns JNI_TRUE on success.
 */
static int registerNatives(JNIEnv* env)
{
  if (!registerNativeMethods(env, myClassName,
                 methods, sizeof(methods) / sizeof(methods[0]))) {
    return JNI_FALSE;
  }

  return JNI_TRUE;
}


// ----------------------------------------------------------------------------

/*
 * This is called by the VM when the shared library is first loaded.
 */
 
typedef union {
    JNIEnv* env;
    void* venv;
} UnionJNIEnvToVoid;

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    jint result = -1;
    JNIEnv* env = NULL;
    
    LOGD("JNI_OnLoad");

    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_4) != JNI_OK) {
        LOGD("ERROR: GetEnv failed");
        goto bail;
    }
    env = uenv.env;

    if (registerNatives(env) != JNI_TRUE) {
        LOGD("ERROR: registerNatives failed");
        goto bail;
    }

    result = JNI_VERSION_1_4;
    
bail:
    return result;
}
