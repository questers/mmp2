package com.quester.mifare;

import java.util.HashMap;
import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.SimpleAdapter;

public class SMSListView extends ListActivity {

	private List<HashMap<String, String>> mList;
	private SimpleAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mList = (List<HashMap<String,String>>)getIntent().getSerializableExtra("smsList");
		setup();
		setListAdapter(adapter);
	}
	
	private void setup() {
		adapter = new SimpleAdapter(this, mList, 
				R.layout.messages, new String[]{"addr","sms"}, 
				new int[]{R.id.sms_addr,R.id.sms_body});
	}
}
