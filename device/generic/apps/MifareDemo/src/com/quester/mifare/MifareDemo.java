package com.quester.mifare;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.telephony.SmsMessage;

import com.android.internal.telephony.AdnRecord;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class MifareDemo extends Activity implements OnClickListener {
	
    static {
        System.loadLibrary("mifare");
    }

    /*
     * 功能：Mifare/RFID初始化。
     * 参数：无
     * 返回值：
     *   true - 初始化成功；
     *   false - 初始化失败
     */
    private native boolean mifare_init();
    /*
     * 功能：关闭Mifare/RFID连接
     * 参数：无
     * 返回值：无
     */
    private native void mifare_free();

    /*
     * 功能：寻卡
     * 参数：无
     * 返回值：
     *   0x00 - 成功找到卡
     *   0x82 - 通信超时
     *   0x83 - 卡不存在
     *   0x84 - 接受卡数据出错
     *   0x87 - 未知错误
     *   0x89 - 参数错误
     *   0x8f - 输入的指令不存在
     */
    private native int mifare_reqa();
    /*
     * 功能：获取身份证卡号
     * 参数：
     *   byte[] array - 长度至少为4字节的缓冲区，用来存放返回的卡号
     * 返回值：
     *   0x00 - 成功
     *   0x82 - 通信超时
     *   0x83 - 卡不存在
     *   0x84 - 接受卡数据出错
     *   0x87 - 未知错误
     *   0x89 - 参数错误
     *   0x8f - 输入的指令不存在
     */
    private native int mifare_get_snr(byte[] array);
    /*
     * 功能：读取Mifare卡
     * 参数：
     *   byte addr - 读取的块地址，取值范围0x00-0x3f
     *   byte blks - 读取的块数，取值范围0x01-0x04
     *   byte[] key - 长度为6的缓冲区，传入卡的密钥
     *   byte auth - 认证模式。0：对KEYA进行验证；1：对KEYB进行验证
     *   byte mode - 请求模式。0：IDLE模式；1：ALL模式
     *   byte[] array - 长度至少为blks*16的缓冲区，用来存放读出的数据
     */
    private native int mifare_read_card(byte addr, byte blks, byte[] key, byte auth, byte mode, byte[] array);
    /*
     * 功能：写Mifare卡
     * 参数：
     *   byte addr - 块地址，取值范围0x00-0x3f
     *   byte blks - 块数，取值范围0x01-0x04
     *   byte[] key - 长度为6的缓冲区，传入卡的密钥
     *   byte auth - 认证模式。0：对KEYA进行验证；1：对KEYB进行验证
     *   byte mode - 请求模式。0：IDLE模式；1：ALL模式
     *   byte[] array - 长度至少为blks*16的缓冲区，用来存放写入的数据
     */
    private native int mifare_write_card(byte addr, byte blks, byte[] key, byte auth, byte mode, byte[] array);

    /*
     * 功能：对SIM卡进行复位
     * 参数：无
     * 返回值：
     *   0x00 - 复位成功
     *   0x83 - 卡不存在
     *   0x84 - 接受卡数据出错
     *   0x85 - 参数格式错误
     */
    private native int sim_reset();
    /*
     * 功能：进入SIM卡地址薄
     * 参数：
     *   Integer num - 返回地址薄条数
     *   Integer max_size - 返回地址薄最大字节数
     * 返回值：
     *   0x00 - 复位成功
     *   0x83 - 卡不存在
     *   0x84 - 接受卡数据出错
     *   0x85 - 参数格式错误
     */
    private native int sim_enter_directory(Integer num, Integer max_size);
    /*
     * 功能：读取地址薄
     * 参数：
     *   int index - 要读的地址薄的索引，从1到地址薄条数
     *   int max_size - 地址薄容量。必须为sim_enter_directory返回的最大字节数
     *   byte[] buf - 缓冲区，大小为max_size，用来存放读出地址薄数据
     * 返回值：
     *   0x00 - 复位成功
     *   0x83 - 卡不存在
     *   0x84 - 接受卡数据出错
     *   0x85 - 参数格式错误
     */
    private native int sim_read_directory(int index, int max_size, byte[] buf);
    /*
     * 功能：进入SIM卡短信目录
     * 参数：
     *   Integer num - 返回短信条数
     *   Integer max_size - 返回短信最大字节数
     * 返回值：
     *   0x00 - 复位成功
     *   0x83 - 卡不存在
     *   0x84 - 接受卡数据出错
     *   0x85 - 参数格式错误
     */
    private native int sim_enter_sms(Integer num, Integer max_size);
    /*
     * 功能：读取短信
     * 参数：
     *   int index - 要读的短信的索引，从1到短信条数
     *   int max_size - 短信容量。必须为sim_enter_sms返回的最大字节数
     *   byte[] buf - 缓冲区，大小为max_size，用来存放读出短信数据
     * 返回值：
     *   0x00 - 复位成功
     *   0x83 - 卡不存在
     *   0x84 - 接受卡数据出错
     *   0x85 - 参数格式错误
     */
    private native int sim_read_sms(int index, int max_size, byte[] buf);

    private static final String TAG = "mifare";
	private boolean mInitOK;
	private Context mContext;
    
    private Button mReqa;
    private Button mGetSnr;
    private Button mReadCard;
    private Button mWriteCard;
    private Button simEnterDir;
    private Button simReadDir;
    private Button simEnterSms;
    private Button simReadSms;
    
    private TextView mReqaLabel;
    private TextView mGetSnrLabel;
    private TextView mReadCardLabel;
    private TextView mWriteCardLabel;
    private TextView simEnterDirLabel;
    private TextView simReadDirLabel;
    private TextView simEnterSmsLabel;
    private TextView simReadSmsLabel;

	private ProgressBar mReqaBar;
	private ProgressBar mGetSnrBar;
	private ProgressBar mReadCardBar;
	private ProgressBar mWriteCardBar;
	private ProgressBar simEnterDirBar;
    private ProgressBar simReadDirBar;
	private ProgressBar simEnterSmsBar;
    private ProgressBar simReadSmsBar;
    
    private List<HashMap<String, String>> mList;
	private String resultPass;
	private String resultFail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		mContext = this;
        initialize();
    }
    
    private void initialize() {
    	mReqa = (Button) findViewById(R.id.mifare_reqa);
    	mReqa.setOnClickListener(this);
    	mGetSnr = (Button) findViewById(R.id.mifare_get_snr);
    	mGetSnr.setOnClickListener(this);
    	mReadCard = (Button) findViewById(R.id.mifare_read_card);
    	mReadCard.setOnClickListener(this);
    	mWriteCard = (Button) findViewById(R.id.mifare_write_card);
    	mWriteCard.setOnClickListener(this);
    	simEnterDir = (Button) findViewById(R.id.sim_enter_directory);
    	simEnterDir.setOnClickListener(this);
    	simReadDir = (Button) findViewById(R.id.sim_read_directory);
    	simReadDir.setOnClickListener(this);
    	simEnterSms = (Button) findViewById(R.id.sim_enter_sms);
    	simEnterSms.setOnClickListener(this);
    	simReadSms = (Button) findViewById(R.id.sim_read_sms);
    	simReadSms.setOnClickListener(this);
    	
    	mReqaLabel = (TextView) findViewById(R.id.mifare_reqa_label);
    	mGetSnrLabel = (TextView) findViewById(R.id.mifare_get_snr_label);
    	mReadCardLabel = (TextView) findViewById(R.id.mifare_read_card_label);
    	mWriteCardLabel = (TextView) findViewById(R.id.mifare_write_card_label);
    	simEnterDirLabel = (TextView) findViewById(R.id.sim_enter_directory_label);
    	simReadDirLabel = (TextView) findViewById(R.id.sim_read_directory_label);
    	simEnterSmsLabel = (TextView) findViewById(R.id.sim_enter_sms_label);
    	simReadSmsLabel = (TextView) findViewById(R.id.sim_read_sms_label);

		mReqaBar = (ProgressBar) findViewById(R.id.mifare_reqa_pb);
    	mGetSnrBar = (ProgressBar) findViewById(R.id.mifare_get_snr_pb);
		mReadCardBar = (ProgressBar) findViewById(R.id.mifare_read_card_pb);
    	mWriteCardBar = (ProgressBar) findViewById(R.id.mifare_wirte_card_pb);
		simEnterDirBar = (ProgressBar) findViewById(R.id.sim_enter_dir_pb);
    	simReadDirBar = (ProgressBar) findViewById(R.id.sim_read_dir_pb);
    	simEnterSmsBar = (ProgressBar) findViewById(R.id.sim_enter_sms_pb);
    	simReadSmsBar = (ProgressBar) findViewById(R.id.sim_read_sms_pb);

		resultPass = getString(R.string.result_pass);
		resultFail = getString(R.string.result_fail);
    }

	private void getData() {
		mReqa.setVisibility(View.GONE);
		mReqaBar.setVisibility(View.VISIBLE);
		mGetSnr.setVisibility(View.GONE);
		mGetSnrBar.setVisibility(View.VISIBLE);
		mReadCard.setVisibility(View.GONE);
		mReadCardBar.setVisibility(View.VISIBLE);
		mWriteCard.setVisibility(View.GONE);
		mWriteCardBar.setVisibility(View.VISIBLE);
		simEnterDir.setVisibility(View.GONE);
		simEnterDirBar.setVisibility(View.VISIBLE);
		simReadDir.setVisibility(View.GONE);
		simReadDirBar.setVisibility(View.VISIBLE);
		simEnterSms.setVisibility(View.GONE);
		simEnterSmsBar.setVisibility(View.VISIBLE);
		simReadSms.setVisibility(View.GONE);
		simReadSmsBar.setVisibility(View.VISIBLE);
	}

	private void dataCallback() {
		runOnUiThread(new Runnable() {
			public void run() {
				mReqa.setVisibility(View.VISIBLE);
				mReqaBar.setVisibility(View.GONE);
				mGetSnr.setVisibility(View.VISIBLE);
				mGetSnrBar.setVisibility(View.GONE);
				mReadCard.setVisibility(View.VISIBLE);
				mReadCardBar.setVisibility(View.GONE);
				mWriteCard.setVisibility(View.VISIBLE);
				mWriteCardBar.setVisibility(View.GONE);
				simEnterDir.setVisibility(View.VISIBLE);
				simEnterDirBar.setVisibility(View.GONE);
				simReadDir.setVisibility(View.VISIBLE);
				simReadDirBar.setVisibility(View.GONE);
				simEnterSms.setVisibility(View.VISIBLE);
				simEnterSmsBar.setVisibility(View.GONE);
				simReadSms.setVisibility(View.VISIBLE);
				simReadSmsBar.setVisibility(View.GONE);
			}
		});
	}
    
    private void getMifareState() {
    	if (!mInitOK) mInitOK = mifare_init();
    	if (!mInitOK) Toast.makeText(this, 
    			getString(R.string.init_failed), 
    			Toast.LENGTH_LONG).show();
    }

	@Override
    protected void onResume() {
    	super.onResume();
    	getMifareState();
    }

    @Override
    protected void onPause() {
    	super.onPause();
    	if (mInitOK) {
			mInitOK = false;
			mifare_free();
    	}
    }

    private String dump(byte[] array) {
    	StringBuffer sb = new StringBuffer();
        for (int i=0; i<array.length; i++)
            sb.append(String.format("%02x", array[i]));
        return sb.toString();
    }

	@Override
    public void onClick(View v) {
    	if (!mInitOK) {
			Toast.makeText(this, getString(R.string.init_failed),
				Toast.LENGTH_LONG).show();
			return;
		}
    	switch (v.getId()) {
			case R.id.mifare_reqa:
				getData();
				mifareReqa();
				break;
			case R.id.mifare_get_snr:
				getData();
				mifareGetSnr();
				break;
			case R.id.mifare_read_card:
				getData();
				mifareReadCard();
				break;
			case R.id.mifare_write_card:
				getData();
				mifareWriteCard();
				break;
			case R.id.sim_enter_directory:
				getData();
				simEnterDirectory();
				break;
			case R.id.sim_read_directory:
				getData();
				simReadDirLabel.setText(R.string.reading);
				simReadDirectory();
				break;
			case R.id.sim_enter_sms:
				getData();
				simEnterSms();
				break;
			case R.id.sim_read_sms:
				getData();
				simReadSmsLabel.setText(R.string.reading);
				simReadSms();
				break;
			default: break;
		}
	}

	private void mifareReqa() {
		new Thread(new Runnable() {
			public void run() {
				showMessage(mReqaLabel, String.format("%s", 
					mifare_reqa() == 0 ? resultPass : resultFail));
				dataCallback();
			}
		}).start();
	}

	private void mifareGetSnr() {
		new Thread(new Runnable() {
			public void run() {
				byte[] array = new byte[4];
				showMessage(mGetSnrLabel, String.format("%s", 
					mifare_get_snr(array) == 0 ? dump(array) : resultFail));
				dataCallback();
			}
		}).start();
	}

	private void mifareReadCard() {
		new Thread(new Runnable() {
			public void run() {
				byte addr = 0x01;
	            byte blks = 0x01;
	            byte[] key = new byte[6];
	            for (int i=0; i<6; i++) key[i] = (byte) 0xff;
	            byte auth = 0x00;
	            byte mode = 0x00;
	            byte[] array = new byte[blks<<4];
				showMessage(mReadCardLabel, String.format("%s", 
					mifare_read_card(addr, blks, key, auth, mode, array)== 0 
					? dump(array) : resultFail));
				dataCallback();
			}
		}).start();
	}

	private void mifareWriteCard() {
		new Thread(new Runnable() {
			public void run() {
				byte addr = 0x01;
	            byte blks = 0x01;
	            byte[] key = new byte[6];
	            for (int i=0; i<6; i++) key[i] = (byte) 0xff;
	            byte auth = 0x00;
	            byte mode = 0x00;
	            byte[] array = new byte[blks<<4];
	            Random r = new Random();
	            r.nextBytes(array);
				showMessage(mWriteCardLabel, String.format("%s", 
					mifare_write_card(addr, blks, key, auth, mode, array) == 0 
					? dump(array) : resultFail));
				dataCallback();
			}
		}).start();
	}

	private void simEnterDirectory() {
		new Thread(new Runnable() {
			public void run() {
				Integer num = new Integer(0);
	            Integer max_size = new Integer(0); 
	            int ret = sim_enter_directory(num, max_size);
				String info = null;
	            if (ret == 0) {
					info = String.format(getString(R.string.total_space) + " %d"
						+ getString(R.string.enter_sim_contacts) + " %d", 
						num.intValue(), max_size.intValue());
	            } else {
	                info = String.format("%s", resultFail);
	            }
				showMessage(simEnterDirLabel, info);
				dataCallback();
			}
		}).start();
	}

	/*
	 * Android 2.3 Api do not supported for AdnRecord to parse the email.
	 * ( the AdnRecord parses email will be setting email=null. )
	 * so here, we're just to reserve the function.
	 */
	private void simReadDirectory() {
		new Thread(new Runnable() {
			public void run() {
				Integer num = new Integer(0);
		        Integer max_size = new Integer(0);
		        int ret = sim_enter_directory(num, max_size);
		        if (ret == 0) {
		        	boolean flag = true;
		        	mList = new ArrayList<HashMap<String,String>>();
		        	int index = 1;
		        	byte[] buf = new byte[max_size.intValue()];
		        	
		        	while (flag) {
		        		ret = sim_read_directory(index, max_size.intValue(), buf);
		        		if (ret == 0) {
		            		AdnRecord record = new AdnRecord(buf);
		            		HashMap<String, String> map = new HashMap<String, String>();
		            		String name = "";
		                    String number = "";
		                    //String[] emails;
		                    if (!record.isEmpty()) {
		                    	name = record.getAlphaTag() == null ? " " : record.getAlphaTag();
								number = record.getNumber() == null ? " " : record.getNumber();
								//emails = record.getEmails();
								map.put("name", name);
								map.put("phone", number);
								/*StringBuilder email = new StringBuilder();
								if (emails != null) {
									for (int i = 0; i < emails.length; i++) {
										email.append(emails[i] == null ? " " : emails[i]);
										if ((i+1) < emails.length) email.append(", ");
									}
									map.put("email", email.toString());
								} else {
									map.put("email", " ");
								}*/
								mList.add(map);
		                    }

							if (index == num.intValue())
								flag = false;
							else
								index++;
		        		} else {
		        			flag = false;
		        		}
		        	}
		        	if (mList.size() == 0) 
		        		showMessage(simReadDirLabel, String.format("%s", getString(R.string.sim_contacts_empty)));
		        } else {
		        	showMessage(simReadDirLabel, String.format("%s", getString(R.string.sim_contacts_fail)));
		        }
				
				dataCallback();
		        
		        if (mList.size() != 0) {
		        	showMessage(simReadDirLabel, "Found " + mList.size() + " records");
		        	Intent intent = new Intent();
		        	intent.setClass(mContext, ContactListView.class);
		        	intent.putExtra("bookList", (Serializable)mList);
		        	startActivity(intent);
		        }
			}
		}).start();
	}

	private void simEnterSms() {
		new Thread(new Runnable() {
			public void run() {
				Integer num = new Integer(0);
	            Integer max_size = new Integer(0);
	            int ret = sim_enter_sms(num, max_size);
				String info = null;
	            if (ret == 0) {
	                info = String.format(getString(R.string.total_space) + " %d"
						+ getString(R.string.enter_sms) + " %d", 
	                		num.intValue(), max_size.intValue());
	            } else {
	                info = String.format("%s", resultFail);
	            }
				showMessage(simEnterSmsLabel, info);
				dataCallback();
			}
		}).start();
	}

	private void simReadSms() {
		new Thread(new Runnable() {
			public void run() {
				Integer num = new Integer(0);
		        Integer max_size = new Integer(0);
		        int ret = sim_enter_sms(num, max_size);
		        if (ret == 0) {
		        	boolean flag = true;
		        	mList = new ArrayList<HashMap<String,String>>();
		            int index = 1;
		            byte[] buf = new byte[max_size.intValue()];
		            
		            while (flag) {
		            	ret = sim_read_sms(index, max_size.intValue(), buf);
		            	if (ret == 0) {
							System.arraycopy(buf, 1, buf, 0, buf.length-1);
							try {
								String addr = SmsMessage.createFromPdu(buf).getDisplayOriginatingAddress();
								String body = SmsMessage.createFromPdu(buf).getDisplayMessageBody();
								if (addr != null || body != null) {
									HashMap<String, String> map = new HashMap<String, String>();
									map.put("addr", addr == null ? " " : addr);
									map.put("sms", body == null ? " " : body);
									mList.add(map);
								}
							} catch (NullPointerException e) {
								Log.w(TAG, "Data format error");
							}

							if (index == num.intValue())
								flag = false;
							else
								index++;
		            	} else {
		            		flag = false;
		            	}
		            }
		            if (mList.size() == 0) 
		        		showMessage(simReadSmsLabel, String.format("%s", getString(R.string.sms_empty)));
		        } else {
		        	showMessage(simReadSmsLabel, String.format("%s", getString(R.string.sms_fail)));
		        }
		        
		        dataCallback();
		        
		        if (mList.size() != 0) {
		        	showMessage(simReadSmsLabel, "Found " + mList.size() + " records");
		        	Intent intent = new Intent();
		        	intent.setClass(mContext, SMSListView.class);
		        	intent.putExtra("smsList", (Serializable)mList);
		        	startActivity(intent);
		        } 
			}
		}).start();
	}
	
	private void showMessage(final TextView tv, final String info) {
		runOnUiThread(new Runnable() {
			public void run() {
				if (tv != null) tv.setText(info);
			}
		});
	}
	
}
