package com.quester.mifare;

import java.util.HashMap;
import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.SimpleAdapter;

public class ContactListView extends ListActivity {

	private List<HashMap<String, String>> mList;
	private SimpleAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mList = (List<HashMap<String,String>>)getIntent().getSerializableExtra("bookList");
		setup();
		setListAdapter(adapter);
	}
	
	private void setup() {
		adapter = new SimpleAdapter(this, mList, 
				R.layout.contacts, new String[]{"name","phone"/*,"email"*/}, 
				new int[]{R.id.contact_name, R.id.contact_phone/*, R.id.contact_email*/});
	}
}
