/* 
 * Copyright (c) 2011, Quester Techonology, Shenzhen, China. All rights reserved.
 */

package com.quester.clocksync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.app.*;
import android.os.*;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import com.quester.clocksync.ClockSyncService;

public class ClockSyncReceiver extends BroadcastReceiver
{
    private static String TAG = "ClockSync";

    public void onReceive(Context context, Intent intent)
    {
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION))
        {
            if (intent.hasExtra(ConnectivityManager.EXTRA_NETWORK_INFO))
            {
                Log.i(TAG, "Received "+intent);
                NetworkInfo info = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (info.isConnected())
                {
                    Log.i(TAG, "Network connected, start ClockSyncService");
                    Intent intnt = new Intent(context, ClockSyncService.class);
                    context.startService(intnt);
                }
                else
                {
                    Log.i(TAG, "Network disconnected, stop ClockSyncService");
                    Intent intnt = new Intent(context, ClockSyncService.class);
                    context.stopService(intnt);
                }
            }
        }
    }
}


