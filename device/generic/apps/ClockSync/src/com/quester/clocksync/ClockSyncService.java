/* 
 * Copyright (c) 2011, Quester Techonology, Shenzhen, China. All rights reserved.
 */

package com.quester.clocksync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.app.*;
import android.os.*;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;

import android.net.SntpClient;
import android.os.SystemClock;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class ClockSyncService extends Service 
{
    Thread mThread = null;
    private static String TAG = "ClockSync";
    private static int NTP_TIMEOUT = 10*1000; /* 10s */
    private static int NTP_INTERVAL_MIN = 5*1000; /* 5s */
    private static int NTP_INTERVAL_MAX = 10*60*1000; /* 10m */
    private static String PROPERTY_NTP = "persist.sys.ntp_server";
    private static String CONFIG_FILE_NTP = "/etc/ntp.conf";
    private static String DEFAULT_NTP = "pool.ntp.org;asia.pool.ntp.org";
    private boolean mFound = false;

    public ClockSyncService()
    {
        Log.i(TAG, "ClockSyncService");
    }

    public void onCreate()
    {
        super.onCreate();
        Log.i(TAG, "onCreate");
    }

    public void onDestroy()
    {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
    }

    public IBinder onBind(Intent intent)
    {
        Log.i(TAG, "onBind");
        return null;
    }

    public int onStartCommand(Intent intent, int i, int j)
    {
        Log.i(TAG, "onStartCommand");
        if (mThread == null)
        {
            mThread = new Thread()
            {
                public void run()
                {
                    FileInputStream stream = null;
                    String ntpServers = null;
                    String[] ntpServer;

                    ntpServers = SystemProperties.get(PROPERTY_NTP, null);
                    if (ntpServers == null || ntpServers.equals(""))
                    {
                        try {
                            Properties properties = new Properties();
                            File file = new File(CONFIG_FILE_NTP);
                            stream = new FileInputStream(file);
                            properties.load(stream);
                            ntpServers = properties.getProperty("NTP_SERVER", null);
                        } catch (IOException e) {
                            Log.e(TAG, "Could not open configuration file " + CONFIG_FILE_NTP);
                        } finally {
                            if (stream != null) {
                                try {
                                    stream.close();
                                } catch (Exception e) {}
                            }
                        }
                    }

                    if (ntpServers == null)
                        ntpServers = new String(DEFAULT_NTP);

                    int interval = NTP_INTERVAL_MIN;
                    ntpServer = ntpServers.split(";");
                    for (int i=0; !mFound && i<ntpServer.length;)
                    {
                        Log.i(TAG, "Send NTP request to "+ntpServer[i]);
                        SntpClient client = new SntpClient();
                        if (client.requestTime(ntpServer[i], NTP_TIMEOUT))
                        {
                            boolean auto_time = true;
                            try
                            {
                                auto_time = Settings.System.getInt(getContentResolver(), Settings.System.AUTO_TIME) > 0;
                            }
                            catch (SettingNotFoundException snfe)
                            {
                                Log.i(TAG, "auto_time setting not found.");
                            }
                            if (!auto_time)
                            {
                                Log.i(TAG, "Automatic time configuration not enabled, skip setting system time.");
                                break;
                            }

                            Log.i(TAG, "Setting system time to "+client.getNtpTime());
                            SystemClock.setCurrentTimeMillis(client.getNtpTime());
                            mFound = true;
                        }
                        else
                        {
                            Log.i(TAG, "Error fetching NTP time");
                            
                            i++;
                            if (i >= ntpServer.length)
                            {
                                i = 0;

                                try
                                {
                                    Log.i(TAG, "Sleep "+interval+"ms and try...");
                                    sleep(interval, 0);
                                }
                                catch (InterruptedException e)
                                {
                                    Log.i(TAG, "Exception happended: "+e);
                                    mFound = true; /* force quit */
                                }

                                interval *= 2;
                                if (interval > NTP_INTERVAL_MAX)
                                    interval = NTP_INTERVAL_MAX;
                            }
                        }
                    } /* for */
                } /* run() */
            }; /* new Thread() */

            mThread.start();
        }
        return 1;    
    }
}    

