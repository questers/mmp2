#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <jni.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include "log.h"

static char className[] = "com/quester/mac/MacDemo";

int set_mac(u8* addr, int len);
int get_mac(u8* addr, int len);

int main()
{
    int ret = 0;
    u8 addr[6] = {0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};

    ret = set_mac(addr, 6);
    if (ret < 0)
    {
        LOGD("set_mac() error");
        return 0;
    }

    LOGD("set_mac() done");

    ret = get_mac(addr, 6);
    if (ret < 0)
    {
        LOGD("get_mac() error");
        return 0;
    }

    LOGD("get_mac() done");

    char buf[32] = {0};
    snprintf(buf, 32, "%02x:%02x:%02x:%02x:%02x:%02x",
        addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);
    LOGD("%s", buf);

    return 0;
}

int set_mac(u8* addr, int len)
{
    int s;
    int ret;
    struct ifreq ifr;
    
    if (len < 6)
    {
        LOGD("set_mac(), invalid length");
        return -1;
    }

    s = socket(PF_INET, SOCK_DGRAM, 0);
    if (s < 0)
    {
        LOGD("socket() error: %s", strerror(errno));
        return -1;
    }

    strcpy(ifr.ifr_ifrn.ifrn_name, "usb0");
    ifr.ifr_ifru.ifru_hwaddr.sa_family = 1;
    memcpy(ifr.ifr_ifru.ifru_hwaddr.sa_data, addr, 6);
    ret = ioctl(s, SIOCSIFHWADDR, &ifr);
    if (ret != 0)
    {
        LOGD("ioctl(SIOCSIFHWADDR) error: %d(%s)", errno, strerror(errno));
        return -1;
    }

    return 0;
}


int get_mac(u8* addr, int len)
{
    int s;
    int ret;
    struct ifreq ifr;

    if (len < 6)
    {
        LOGD("get_mac(), invalid length");
        return -1;
    }

    s = socket(PF_INET, SOCK_DGRAM, 0);
    if (s < 0)
    {
        LOGD("socket() error: %s", strerror(errno));
        return -1;
    }

    strcpy(ifr.ifr_ifrn.ifrn_name, "usb0");
    ret = ioctl(s, SIOCGIFHWADDR, &ifr);
    if (ret != 0)
    {
        LOGD("ioctl(SIOCSIFHWADDR) error: %s", strerror(errno));
        return -1;
    }

    memcpy(addr, ifr.ifr_ifru.ifru_hwaddr.sa_data, 6);

    return 0;
}

jint
jni_get_mac(JNIEnv *env, jobject thiz, jbyteArray array) {
    int ret = 0;
    int length = env->GetArrayLength(array);
    if (length < 6) return -1;

    unsigned char* buf = (u8*) env->GetByteArrayElements(array, NULL);
    ret = get_mac(buf, 6);
    env->ReleaseByteArrayElements(array, (jbyte*)buf, JNI_COMMIT);

    return ret;
}

jint
jni_set_mac(JNIEnv *env, jobject thiz, jbyteArray array) {
    int ret = 0;
    int length = env->GetArrayLength(array);
    if (length < 6) return -1;

    unsigned char* buf = (u8*) env->GetByteArrayElements(array, NULL);
    ret = set_mac(buf, 6);
    env->ReleaseByteArrayElements(array, (jbyte*)buf, JNI_ABORT);

    return ret;
}

static JNINativeMethod methods[] = {
  {"get_mac", "([B)I", (void*) jni_get_mac },
  {"set_mac", "([B)I", (void*) jni_set_mac },
};

/*
 * Register several native methods for one class.
 */
static int registerNativeMethods(JNIEnv* env, const char* className,
    JNINativeMethod* gMethods, int numMethods)
{
    jclass clazz;

    clazz = env->FindClass(className);
    if (clazz == NULL) {
        LOGD("Native registration unable to find class '%s'", className);
        return JNI_FALSE;
    }
    if (env->RegisterNatives(clazz, gMethods, numMethods) < 0) {
        LOGD("RegisterNatives failed for '%s'", className);
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

/*
 * Register native methods for all classes we know about.
 *
 * returns JNI_TRUE on success.
 */
static int registerNatives(JNIEnv* env)
{
  if (!registerNativeMethods(env, className,
                 methods, sizeof(methods) / sizeof(methods[0]))) {
    return JNI_FALSE;
  }

  return JNI_TRUE;
}


// ----------------------------------------------------------------------------

/*
 * This is called by the VM when the shared library is first loaded.
 */
 
typedef union {
    JNIEnv* env;
    void* venv;
} UnionJNIEnvToVoid;

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    jint result = -1;
    JNIEnv* env = NULL;
    
    LOGD("JNI_OnLoad");

    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_4) != JNI_OK) {
        LOGD("ERROR: GetEnv failed");
        goto bail;
    }
    env = uenv.env;

    if (registerNatives(env) != JNI_TRUE) {
        LOGD("ERROR: registerNatives failed");
        goto bail;
    }

    result = JNI_VERSION_1_4;
    
bail:
    return result;
}
