#if !defined QST_LOG_H
#define QST_LOG_H

#define LOG_TAG "mac"
#include <android/log.h>
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
//#define  LOGD(...)  do{printf(__VA_ARGS__);printf("\n");}while(0)

#define u8 unsigned char

#endif // QST_LOG_H
