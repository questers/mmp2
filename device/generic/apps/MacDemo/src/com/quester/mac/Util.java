package com.quester.mac;

public class Util {

	public static final String TAG = "MacDemo";
	public static final String MAC_ADDR = "/data/data/com.quester.mac/files/mac_addr";
    public static final String MAC_ADDR_DIR = "/data/data/com.quester.mac/files/";
    
	public static byte[] string2Bytes(String str) {
    	if (str.length() != 12) {
    		return null;
    	}
    	byte[] mac = new byte[6];
    	for (int i = 0; i < 6; i++) {
    		try {
    			mac[i] = (byte) Integer.parseInt(str.substring(i*2, i*2+2), 16);
    		} catch (NumberFormatException e) {
    			return null;
    		}
    	}
    	return mac;
    }

}
