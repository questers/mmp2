package com.quester.mac;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MacService extends Service {
    
    private File file;
	private MacDemo macDemo;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		macDemo = new MacDemo();
		File fileDir = new File(Util.MAC_ADDR_DIR);
		if (!fileDir.exists()) {
			try {
				fileDir.mkdirs();
			} catch (SecurityException e) {
				Log.e(Util.TAG, "Create dir fail!");
			}
		}
		file = new File(Util.MAC_ADDR);
		if (!file.exists()) {
    		try {
    			file.createNewFile();
    			String command = "chmod 666 " + file.getAbsolutePath();
    			Runtime runtime = Runtime.getRuntime();
    			runtime.exec(command);
    			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
    			String macAddr = "aabbccddeeff";
    			bw.write(macAddr);
    			bw.flush();
    			bw.close();
    		} catch(IOException e) {
    			Log.e(Util.TAG, "Can't write mac address!");
    		}
    	};
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		if (file.exists()) {
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String macAddr = br.readLine();
				br.close();
				macDemo.setMacAddress(macAddr);
			} catch (IOException e) {
				Log.e(Util.TAG, "Can't read mac address!");
			}
		}
		onDestroy();
	}

}
