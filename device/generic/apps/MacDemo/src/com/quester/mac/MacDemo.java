package com.quester.mac;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MacDemo extends Activity implements OnClickListener {
	
    static {
        System.loadLibrary("mac");
    }
    
    private native int get_mac(byte[] addr);
    private native int set_mac(byte[] addr);
    
    private Button setMac;
    private Button exitBt;
    private TextView getMacLabel;
    private EditText setMacLabel;
    
    private String macShow;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        initialize();
        getMacAddress();
    }
    
    private void initialize() {
    	setMac = (Button) findViewById(R.id.set_mac);
    	setMac.setOnClickListener(this);
    	setMacLabel = (EditText) findViewById(R.id.set_mac_label);
    	exitBt = (Button) findViewById(R.id.exit);
    	exitBt.setOnClickListener(this);
    	getMacLabel = (TextView) findViewById(R.id.get_mac_label);
    	macShow = getString(R.string.mac_address) + " ";
    }

    private String dump(byte[] array) {
        String s = "";
        for (int i=0; i<array.length; i++)
            s += String.format("%02x", array[i]);

        return s;
    }
    
    private void getMacAddress() {
    	byte[] mac = new byte[6];
    	getMacLabel.setText(macShow + String.format("%s", 
    			get_mac(mac) == 0 ? dump(mac) : "Obtain fail"));
    }
    
    private void setMacAddress() {
    	String info = setMacLabel.getText().toString();
    	byte[] mac = Util.string2Bytes(info);
    	if (mac == null) {
    		Toast.makeText(this, "Mac address error!", Toast.LENGTH_LONG).show();
    		return;
    	}
    	int ret = set_mac(mac);
    	if (ret != 0) {
    		Toast.makeText(this, "Set mac address fail!", Toast.LENGTH_LONG).show();
    		return;
    	}
    	getMacAddress();
    	File file = new File(Util.MAC_ADDR);
    	if (file.exists()) {
    		try {
    			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
    			bw.write(info);
    			bw.flush();
    			bw.close();
    		} catch (IOException e) {
    			Log.e(Util.TAG, "Can't write mac address!");
    		}
    	}
    	Toast.makeText(this, "Set mac address success!", Toast.LENGTH_LONG).show();
    }

	public void setMacAddress(String addr) {
    	byte[] mac = Util.string2Bytes(addr);
    	if (mac == null) {
    		Log.e(Util.TAG, "Mac address error!");
    		return;
    	}
    	int ret = set_mac(mac);
    	if (ret != 0) {
    		Log.e(Util.TAG, "Set mac address fail!");
    		return;
    	}
    	Log.d(Util.TAG, "Set mac address success!");
    }

    @Override
    public void onClick(View v) {
        if (v == exitBt) {
			System.exit(0);
        }
        else if (v == setMac) {
        	setMacAddress();
        }
    }

}
