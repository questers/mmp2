package com.quester.flashlight;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.FileObserver;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Message;
import android.os.SystemProperties;
import android.util.Log;

public class LightService extends Service {
	
	private static final String TAG = "flashlight";
	private static final int REFRESH = 0x1;
	
	private static final String LOW = "0";
	private static final String HIGH = "1";
	private static final String DEFAULT_VERSION = "1";
	private static final String OBSERVER_INTENT = "quester.intent.action.FLASHLIGHT";
	private static final String LIGHT_VALUE = "/sys/class/leds/flashlight/brightness";
	private static final String MAX_LIGHT_VALUE = "/sys/class/leds/flashlight/max_brightness";
	// version 1
	private static final String GPIO48 = "/sys/class/gpio/gpio48/value";
	private static final String GPIO49 = "/sys/class/gpio/gpio49/value";
	private static final String GPIO54 = "/sys/class/gpio/gpio54/value";
	// version 2
	private static final String GPIO47 = "/sys/class/gpio/gpio47/value";
	// version 4
	private static final String GPIO8 = "/sys/class/gpio/gpio8/value";

	// version 1
	private File gpio48;
	private File gpio49;
	private File gpio54;
	// version 2
	private File gpio47;
	// version 4
	private File gpio8;

	private File brightness;
	private int minBrightness;
	private int midBrightness;
	private int maxBrightness;

	private static int lightState;
	private static int currentVersion;
	
	private HardwareKeyReceiver mReceiver;
	private BrightnessObserver mObserver;

	private WakeLock mWakeLock;
	
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if(msg.what == REFRESH) {
				if (checkState()) {
					chooseVersion();
				}
			}
		}
	};
	
	private boolean checkState() {
		boolean flag = false;
		try {
			FileInputStream fis = null;
			if (currentVersion == 1) {
				fis = new FileInputStream(gpio54);
			} else if (currentVersion < 4) {
				fis = new FileInputStream(gpio47);
			} else if (currentVersion >= 4) {
				fis = new FileInputStream(gpio8);
			}
			// the byte read or -1 if the end of stream has been reached
			// '0' -> ASCII -> 48
			if (fis.read() == 48) {
				flag = true;
			}
			fis.close();
		} catch (IOException e) {
			Log.w(TAG, e.getMessage());
		}
		return flag;
	}
	
	public static int getState() {
		return lightState;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		initialize();
	}

	private void initialize() {
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "FlashlightObserver");
		mWakeLock.setReferenceCounted(false);
		currentVersion = Integer.parseInt(
			SystemProperties.get("ro.hardware.rev", DEFAULT_VERSION));
		if (currentVersion == 1) {
			gpio48 = new File(GPIO48);
			gpio49 = new File(GPIO49);
			gpio54 = new File(GPIO54);
			if (!gpio48.exists() && !gpio49.exists() 
					&& !gpio54.exists()) {
				Log.e(TAG, "Flashlight v1 init failed");
				onDestroy();
				return;
			}
		} else if (currentVersion < 4) {
			gpio47 = new File(GPIO47);
			brightness = new File(LIGHT_VALUE);
			if (!gpio47.exists() && !brightness.exists()) {
				Log.e(TAG, "Flashlight v2 init failed");
				onDestroy();
				return;
			}
			try {
				BufferedReader br = new BufferedReader(new FileReader(MAX_LIGHT_VALUE));
				int values = Integer.parseInt(br.readLine());
				br.close();
				if (values > 0) {
					if (values <= 255) {
						minBrightness = 235;
						midBrightness = 200;
						maxBrightness = 160;
					} else {
						minBrightness = values / 3 * 2;
						midBrightness = values / 3;
						maxBrightness = 1;
					}
				} else {
					Log.e(TAG, "Flashlight v2 init failed");
					onDestroy();
					return;
				}
			} catch (IOException e) {
				Log.e(TAG, "Flashlight v2 init failed");
				onDestroy();
				return;
			}
		} else if (currentVersion >= 4) {
			gpio8 = new File(GPIO8);
			brightness = new File(LIGHT_VALUE);
			if (!gpio8.exists() && !brightness.exists()) {
				Log.e(TAG, "Flashlight v4 init failed");
				onDestroy();
				return;
			}
			try {
				BufferedReader br = new BufferedReader(new FileReader(MAX_LIGHT_VALUE));
				int values = Integer.parseInt(br.readLine());
				br.close();
				if (values > 0) {
					if (values <= 255) {
						minBrightness = 235;
						midBrightness = 200;
						maxBrightness = 160;
					} else {
						minBrightness = values / 3 * 2;
						midBrightness = values / 3;
						maxBrightness = 1;
					}
				} else {
					Log.e(TAG, "Flashlight v4 init failed");
					onDestroy();
					return;
				}
			} catch (IOException e) {
				Log.e(TAG, "Flashlight v4 init failed");
				onDestroy();
				return;
			}
		}
		
		mReceiver = new HardwareKeyReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(OBSERVER_INTENT);
		registerReceiver(mReceiver, filter);

		if (currentVersion == 1) {
			mObserver = new BrightnessObserver(GPIO54, FileObserver.CLOSE_WRITE);
		} else if (currentVersion < 4) {
			mObserver = new BrightnessObserver(GPIO47, FileObserver.CLOSE_WRITE);
		} else if (currentVersion >= 4 ) {
			mObserver = new BrightnessObserver(GPIO8, FileObserver.CLOSE_WRITE);
		}
		mObserver.startWatching();
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		lightState = intent.getIntExtra("status", 0);		
		chooseVersion();
	}
	
	private void chooseVersion() {		
		mObserver.stopWatching();
		if (currentVersion == 1) {
			ledCtrlVer1();
		} else if (currentVersion < 4) {
			ledCtrlVer2();
		} else if (currentVersion >= 4) {
			ledCtrlVer4();
		}
		mObserver.startWatching();
		
		if (lightState == 0) {
			if (mWakeLock.isHeld()) mWakeLock.release();
		} else {
			if (!mWakeLock.isHeld()) mWakeLock.acquire();
		}
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mReceiver != null) {
			unregisterReceiver(mReceiver);
		}
		if (mObserver != null) {
			mObserver.stopWatching();
		}
	}

	
	private void ledCtrlVer1() {
		try {
			BufferedWriter bw48 = new BufferedWriter(new FileWriter(gpio48));
			BufferedWriter bw49 = new BufferedWriter(new FileWriter(gpio49));
			BufferedWriter bw54 = new BufferedWriter(new FileWriter(gpio54));
			/*
			 * close: 54(0)
			 * min brightness: 54(1), 48(0), 49(1)
			 * mid brightness: 54(1), 48(1), 49(0)
			 * max brightness: 54(1), 48(0), 49(0)
			 */
			switch (lightState) {
				case 0:
					bw54.write(LOW);
					bw54.flush();
					break;
				case 1:
					bw48.write(LOW);
					bw48.flush();
					bw49.write(HIGH);
					bw49.flush();
					bw54.write(HIGH);
					bw54.flush();
					break;
				case 2:
					bw48.write(HIGH);
					bw48.flush();
					bw49.write(LOW);
					bw49.flush();
					bw54.write(HIGH);
					bw54.flush();
					break;
				case 3:
					bw48.write(LOW);
					bw48.flush();
					bw49.write(LOW);
					bw49.flush();
					bw54.write(HIGH);
					bw54.flush();
					break;
				default: break;
			}
			bw49.close();
			bw48.close();
			bw54.close();
		} catch (IOException e) {
			Log.w(TAG, e.getMessage());
		}
	}
	
	private void ledCtrlVer2() {
		try {
			BufferedWriter bw47 = new BufferedWriter(new FileWriter(gpio47));
			BufferedWriter bwBts = new BufferedWriter(new FileWriter(brightness));
			/*
			 * close: 47(0)
			 * min: 47(1), brightness(min)
			 * mid: 47(1), brightness(mid)
			 * max: 47(1), brightness(max)
			 */
			switch (lightState) {
				case 0:
					bw47.write(LOW);
					bw47.flush();
					break;
				case 1:
					bwBts.write(String.valueOf(minBrightness));
					bwBts.flush();
					bw47.write(HIGH);
					bw47.flush();
					break;
				case 2:
					bwBts.write(String.valueOf(midBrightness));
					bwBts.flush();
					bw47.write(HIGH);
					bw47.flush();
					break;
				case 3:
					bwBts.write(String.valueOf(maxBrightness));
					bwBts.flush();
					bw47.write(HIGH);
					bw47.flush();
					break;
				default: break;
			}
			bwBts.close();
			bw47.close();
		} catch (IOException e) {
			Log.w(TAG, e.getMessage());
		}
	}

	private void ledCtrlVer4() {
		try {
			BufferedWriter bw8 = new BufferedWriter(new FileWriter(gpio8));
			BufferedWriter bwBts = new BufferedWriter(new FileWriter(brightness));
			/*
			 * close: 8(0)
			 * min: 8(1), brightness(min)
			 * mid: 8(1), brightness(mid)
			 * max: 8(1), brightness(max)
			 */
			switch (lightState) {
				case 0:
					bw8.write(LOW);
					bw8.flush();
					break;
				case 1:
					bwBts.write(String.valueOf(minBrightness));
					bwBts.flush();
					bw8.write(HIGH);
					bw8.flush();
					break;
				case 2:
					bwBts.write(String.valueOf(midBrightness));
					bwBts.flush();
					bw8.write(HIGH);
					bw8.flush();
					break;
				case 3:
					bwBts.write(String.valueOf(maxBrightness));
					bwBts.flush();
					bw8.write(HIGH);
					bw8.flush();
					break;
				default: break;
			}
			bwBts.close();
			bw8.close();
		} catch (IOException e) {
			Log.w(TAG, e.getMessage());
		}
	}
	
	class HardwareKeyReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(OBSERVER_INTENT)) {
				if (lightState == 0) {
					lightState = 2;
					if (MainActivity.getView() != null) {
						MainActivity.getView().setBackgroundResource(R.drawable.light2);
					}
				} else {
					lightState = 0;
					if (MainActivity.getView() != null) {
						MainActivity.getView().setBackgroundResource(R.drawable.light0);
					}
				}
			}
			chooseVersion();
		}
		
	}
	
	class BrightnessObserver extends FileObserver {

		public BrightnessObserver(String path) {
			super(path);
		}
		
		public BrightnessObserver(String path, int event) {
			super(path, event);
		}

		@Override
		public void onEvent(int event, String path) {
			if (event == FileObserver.CLOSE_WRITE) {
				if (!Thread.currentThread().isInterrupted() && lightState != 0) {
					Message message = new Message();
					message.what = REFRESH;
					//mHandler.sendMessage(message);
	    			mHandler.sendMessageDelayed(message, 200);
	    		}
			}
		}
		
	}

}
