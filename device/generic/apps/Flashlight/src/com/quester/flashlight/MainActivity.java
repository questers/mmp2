package com.quester.flashlight;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity {
	
	private static ImageView mImageView;
	private Intent mIntent;
	private float screenWidth;
	private float screenHeight;
	private int lightState;

	public static ImageView getView () {
		return mImageView;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        initialize();
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    }
    
    private void initialize() {
    	mImageView = (ImageView)findViewById(R.id.imageView);
		lightState = LightService.getState();
    	switch (lightState) {
			case 0:
				mImageView.setBackgroundResource(R.drawable.light0);
				break;
			case 1:
				mImageView.setBackgroundResource(R.drawable.light1);
				break;
			case 2:
				mImageView.setBackgroundResource(R.drawable.light2);
				break;
			case 3:
				mImageView.setBackgroundResource(R.drawable.light3);
				break;
			default:
				MainActivity.this.finish();
				break;
		}
        
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        screenWidth = dm.widthPixels;
        screenHeight = dm.heightPixels;
        
        mIntent = new Intent();
		mIntent.setClass(MainActivity.this, LightService.class);
        goService();
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	float x = event.getX();
    	float y = event.getY();    	   	   	
    	switch (event.getAction()) {
	    	case MotionEvent.ACTION_DOWN:
	    		if ((x/screenWidth) > 0.38 && (x/screenWidth) < 0.62 && (y/screenHeight) > 0.52
	        			&& (y/screenHeight) < 0.68) {
	        		lightState = LightService.getState(); 
	    			if (lightState == 0) {
	        			mImageView.setBackgroundResource(R.drawable.light1);
	        			lightState = 1;
	        		}
	        		else if (lightState == 1) {
	        			mImageView.setBackgroundResource(R.drawable.light2);
	        			lightState = 2;
	        		}
	        		else if (lightState == 2) {
	        			mImageView.setBackgroundResource(R.drawable.light3);
	        			lightState = 3;
	        		}
	        		else {
	        			mImageView.setBackgroundResource(R.drawable.light0);
	        			lightState = 0;
	        		}
	        		goService();
	        	}
	    		break;
	    	case MotionEvent.ACTION_UP:
	    		break;
	    	case MotionEvent.ACTION_MOVE:
	    		break;
	    	case MotionEvent.ACTION_CANCEL:
	    		break;
    	}
    	
    	return true;
    }
    
    private void goService() {
		mIntent.putExtra("status", lightState);
		MainActivity.this.startService(mIntent);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.menu, menu);
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	int itemId = item.getItemId();
    	switch (itemId) {
    	case R.id.about:
    		openAboutDialog();
    		break;
    	case R.id.exit:
    		MainActivity.this.finish();
    		break;
    	}
    	return true;
    }
    
    private void openAboutDialog() {
    	LayoutInflater inflater = LayoutInflater.from(this);
    	View view = inflater.inflate(R.layout.aboutview, null);
    	new AlertDialog.Builder(MainActivity.this).setTitle(getString(R.string.about)).setView(view)
    			.setNegativeButton(getString(R.string.close), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				}).show();
    }
    
}