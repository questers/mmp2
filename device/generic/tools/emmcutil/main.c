#include <errno.h>
#include <sys/mount.h>
#include <cutils/properties.h>
#include <cutils/log.h>


int mount_internal_storage(char* dev, char* mount_point) {
    int flags = MS_NODEV | MS_NOEXEC | MS_NOSUID | MS_DIRSYNC;
    return mount(dev, mount_point, "vfat", flags, "utf8,shortname=mixed");
}

int main(int argc, char* argv[]){
    char volume_name[PROPERTY_VALUE_MAX];
    char cmd[256];
    if( argc != 3 ) {
        LOGE("Invalid parameter");
	LOGE("usage : emmcexpose mmcdev mountpoint");
        return 0;
    }
    int rc = mount_internal_storage(argv[1], argv[2]);
    if( rc != 0 ) {
        //mount failed, mkdosfs first, try mount again
        property_get("ro.volumename", volume_name, "unknown");
        if( strcmp(volume_name, "unknown") == 0) {
            property_get("ro.product.name", volume_name, "unknown");
        }
        //Ellie changed to use newfs_msdos
        //sprintf(cmd, "mkdosfs -F 32 -n %s %s", volume_name, argv[1]);
        sprintf(cmd, "newfs_msdos -F 32 -O %s %s", volume_name, argv[1]);
        system(cmd);
        rc = mount_internal_storage(argv[1], argv[2]);
    }
    if( rc == 0 ) {
        // unmoun it
        umount(argv[2]);
	LOGD("Internal storage check ok");
    } else {
        LOGE("Internal storage initialize failed");
    }
    return rc;
}
