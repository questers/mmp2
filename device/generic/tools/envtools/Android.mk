LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

TOOLS := \
	fw_printenv \
	fw_setenv
	
LOCAL_SRC_FILES := \
	crc32.c fw_env.c  fw_env_main.c

    
LOCAL_CFLAGS := -DUSE_HOSTCC -I$(TOP)/boot/uboot/include -DRAW_DEVICE 
#-DDEBUG

LOCAL_MODULE := envtool
LOCAL_MODULE_TAGS := optional
include $(BUILD_EXECUTABLE)

# Make #!/system/bin/envtool launchers for each tool.
#
SYMLINKS := $(addprefix $(TARGET_OUT)/bin/,$(TOOLS))
$(SYMLINKS): TOOLBOX_BINARY := $(LOCAL_MODULE)
$(SYMLINKS): $(LOCAL_INSTALLED_MODULE) $(LOCAL_PATH)/Android.mk
	@echo "Symlink: $@ -> $(TOOLBOX_BINARY)"
	@mkdir -p $(dir $@)
	@rm -rf $@
	$(hide) ln -sf $(TOOLBOX_BINARY) $@

ALL_DEFAULT_INSTALLED_MODULES += $(SYMLINKS)


