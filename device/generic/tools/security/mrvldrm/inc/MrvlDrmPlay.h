
#ifndef __MRVL_DRM_PLAY_H__
#define __MRVL_DRM_PLAY_H__

#include "OMX_Core.h"

#ifdef __cplusplus
extern "C" {
#endif

OMX_ERRORTYPE MrvlDrmPlayInit(void);
OMX_ERRORTYPE MrvlDrmPlayCleanup(void);
OMX_ERRORTYPE MrvlDrmPlayGenChallenge(OMX_PTR pvParam);
OMX_ERRORTYPE MrvlDrmPlayGetResponse(OMX_PTR pvParam);
OMX_ERRORTYPE MrvlDrmPlayProc(
         OMX_BUFFERHEADERTYPE *pstInBufHead,
         OMX_BUFFERHEADERTYPE *pstOutBufHead);
OMX_ERRORTYPE MrvlDrmPlayDelLicense(OMX_PTR pvParam);

#ifdef __cplusplus
}
#endif

#endif /* __MRVL_DRM_PLAY_H__ */

