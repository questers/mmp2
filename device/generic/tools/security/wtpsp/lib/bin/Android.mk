LOCAL_PATH:=$(call my-dir)

MY_SECURITY_STATIC_LIBRARIES := \
        libwtpsp.a

include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := $(MY_SECURITY_STATIC_LIBRARIES)
LOCAL_MODULE_TAGS := optional
include $(BUILD_MULTI_PREBUILT)

define translate-a-to-so
$(foreach t,$(1), \
  $(eval include $(CLEAR_VARS)) \
  $(eval LOCAL_WHOLE_STATIC_LIBRARIES := $$(basename $(t))) \
  $(eval LOCAL_MODULE := $$(basename $(t))) \
  $(eval LOCAL_MODULE_TAGS := optional) \
  $(eval LOCAL_PRELINK_MODULE := false) \
  $(eval include $(BUILD_SHARED_LIBRARY)) \
 )
endef

$(call translate-a-to-so, $(MY_SECURITY_STATIC_LIBRARIES))
