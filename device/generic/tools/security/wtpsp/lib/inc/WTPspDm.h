/*
 * (C) Copyright 2009 Marvell International Ltd.  
 * All Rights Reserved 
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret 
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * @file        WTPsp.h
 * @author      Leo Yan
 * @date        03/17/2009
 * @brief       The header file declares the library interfaces.
 *
 */

#ifndef __WTPSP_DM_H__
#define __WTPSP_DM_H__

#include "WTPsp.h"

#ifdef  __cplusplus
extern "C" {
#endif

#define WTPSP_TSR_RKEK_PROV_MASK    (0x00000001)
#define WTPSP_TSR_FIPS_ONLY_MASK    (0x00002000)
#define WTPSP_TSR_NONFIPS_ONLY_MASK (0x00004000)
#define WTPSP_TSR_INIT_MASK         (0x00010000)

#define WTPSP_TSR_RKEK_PROVED       (0x00000001)
#define WTPSP_TSR_FIPS_ONLY         (0x00002000)
#define WTPSP_TSR_NONFIPS_ONLY      (0x00004000)
#define WTPSP_TSR_IS_INITED         (0x00010000)

typedef enum
{
    WtpspPlainTextTransitKey,
    WtpspAES128EncrytedTransitKey,
    WtpspAES256EncrytedTransitKey,
}WtpspTransitKeyType;

typedef enum
{
    WtpspPlayReadyNoSwap,
    WtpspPlayReadySwap,
}WtpspPlayReadyByteSwapFlag;

/* key management interfaces */
WTPSPAPI(WtpspStatus, WtpspDmKeyRingInit,
         (const WtpspPasswdInfo *pstKeyRingPasswd,
          Wtpsp32u uiSymKeyNum,
          Wtpsp32u uiAsymKeyNum,
          WtpspKeyRing *pstKeyRing));

WTPSPAPI(WtpspStatus, WtpspDmKeyRingCreateSymUsrKey,
         (const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          WtpspKeyScheme enKeyScheme,
          Wtpsp32u *puiKeyId,
          WtpspKeyRing *pstKeyRing));
WTPSPAPI(WtpspStatus, WtpspDmKeyRingCreateRsaUsrKey,
         (const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          WtpspKeyScheme enKeyScheme,
          Wtpsp32u uiLimit,
          Wtpsp32u uiTrails,
          Wtpsp32u *puiKeyId,                             /* private key id */
          Wtpsp8u *pucRsaKeyN,
          Wtpsp8u *pucRsaKeyE,
          WtpspKeyRing *pstKeyRing));
WTPSPAPI(WtpspStatus, WtpspDmKeyRingCreateEccpUsrKey,
         (const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          WtpspKeyScheme enKeyScheme,
          Wtpsp32u *puiKeyId,                             /* private key id */
          Wtpsp8u *pucPublicKeyX,
          Wtpsp8u *pucPublicKeyY,
          WtpspKeyRing *pstKeyRing));

WTPSPAPI(WtpspStatus, WtpspDmKeyRingCreateEndorsementKey,
         (const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          Wtpsp32u *puiKeyId,
          WtpspKeyRing *pstKeyRing));

WTPSPAPI(WtpspStatus, WtpspDmKeyRingEnrollHmacKey,
         (const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          const Wtpsp8u *pucKey,
          WtpspKeyScheme enKeyScheme,
          Wtpsp32u *puiKeyId,
          WtpspKeyRing *pstKeyRing));
WTPSPAPI(WtpspStatus, WtpspDmKeyRingEnrollAesKey,
         (const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          const Wtpsp8u *pucKey0,
          const Wtpsp8u *pucKey1,                 /* for XTS mode */
          WtpspKeyScheme enKeyScheme,
          Wtpsp32u *puiKeyId,
          WtpspKeyRing *pstKeyRing));
WTPSPAPI(WtpspStatus, WtpspDmKeyRingEnrollRsaKey,
         (const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyD,
          WtpspKeyScheme enKeyScheme,
          Wtpsp32u *puiKeyId,
          WtpspKeyRing *pstKeyRing));
WTPSPAPI(WtpspStatus, WtpspDmKeyRingEnrollEccpKey,
         (const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          const Wtpsp8u *pucPrivateKey,
          WtpspKeyScheme enKeyScheme,
          Wtpsp32u *puiKeyId,
          WtpspKeyRing *pstKeyRing));
WTPSPAPI(WtpspStatus, WtpspDmKeyRingEnrollEndorsementKey,
         (const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          const Wtpsp8u *pucKey,
          Wtpsp32u *puiKeyId,
          WtpspKeyRing *pstKeyRing));

WTPSPAPI(WtpspStatus, WtpspDmKeyRingDeleteUsrKey,
         (const WtpspUsrKeyInfo *pstUsrKeyInfo,
          WtpspKeyRing *pstKeyRing));

/* provision functional interface, only can be used in DM */
typedef enum {
    WtpspOtpRkekProvClient,
    WtpspOtpRkekProvGen,
    WtpspOtpRkekProvReseed,
} WtpspOtpRkekProvType;

typedef enum {
    WtpspEc521DkProvClient,
    WtpspEc521DkProvGen,
    WtpspEc521DkProvReseed,
} WtpspEc521DkProvType;

WTPSPAPI(WtpspStatus, WtpspRkekProvision,
         (WtpspOtpRkekProvType enType,
          const Wtpsp8u *pucKey,
          const Wtpsp8u *pucSeed));
WTPSPAPI(WtpspStatus, WtpspEc521DkProvision,
         (WtpspEc521DkProvType enType,
          const Wtpsp8u *pucKey,
          const Wtpsp8u *pucSeed));
WTPSPAPI(WtpspStatus, WtpspOEMPlatformBind,
         (WtpspKeyScheme enKeyScheme,
          const Wtpsp8u *pucKeyArg0,
          const Wtpsp8u *pucKeyArg1));
WTPSPAPI(WtpspStatus, WtpspOEMJtagKeyBind,
         (WtpspKeyScheme enKeyScheme,
          const Wtpsp8u *pucKeyArg0,
          const Wtpsp8u *pucKeyArg1));
WTPSPAPI(WtpspStatus, WtpspSetJtagPermanentDisable,
         (Wtpsp32u uiArg0));
WTPSPAPI(WtpspStatus, WtpspSetTemporaryFaDisable,
         (Wtpsp32u uiArg0));
WTPSPAPI(WtpspStatus, WtpspOTPWritePlatformConfig,
         (const Wtpsp8u *pucCfg));
WTPSPAPI(WtpspStatus, WtpspOEMUsbIdProvision,
         (Wtpsp8u *pucId));
WTPSPAPI(WtpspStatus, WtpspLifecycleAdvance,
         (Wtpsp32u uiReq));
WTPSPAPI(WtpspStatus, WtpspSoftwareVersionAdvance,
         (Wtpsp32u uiReq));
WTPSPAPI(WtpspStatus, WtpspTrustStatusRegisterRead,
         (Wtpsp32u *puiVal));
WTPSPAPI(WtpspStatus, WtpspSetFipsPerm, (void));
WTPSPAPI(WtpspStatus, WtpspSetNonFipsPerm, (void));
WTPSPAPI(WtpspStatus, WtpspOemTransitKeyProvision,
         (WtpspKeyScheme enKeyScheme,
          WtpspTransitKeyType enType,
          const Wtpsp8u *pucKey));
WTPSPAPI(WtpspStatus, WtpspPlayReadyKeyBind,
         (const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          const Wtpsp8u *pucEncryptedPlayReadyKey,
          const Wtpsp8u *pucAesIv,
          WtpspKeyScheme enOemTransitKeyScheme,
          WtpspKeyScheme enPlayReadyKeyScheme,
          WtpspPlayReadyByteSwapFlag enByteSwap,
          Wtpsp32u *puiKeyId,
          WtpspKeyRing *pstKeyRing));

#ifdef  __cplusplus
}
#endif

#endif /* __WTPSP_DM_H__ */

