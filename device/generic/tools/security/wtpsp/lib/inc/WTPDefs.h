/*
 * (C) Copyright 2009 Marvell International Ltd.  
 * All Rights Reserved 
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret 
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * @file        WTPDefs.h
 * @author      Leo Yan
 * @date        03/17/2009
 * @brief       The header file defines the basic macros and data types.
 *
 */

#ifndef __WTPDEFS_H__
#define __WTPDEFS_H__

#ifdef __cplusplus
extern "C" {
#endif

#if defined( _WIN32 ) || defined ( _WIN64 )
#define __STDCALL   __stdcall
#define __CDECL     __cdecl
#define __INT64     __int64
#define __UINT64    unsigned __int64
#else
#define __STDCALL
#define __CDECL
#define __INT64     long long
#define __UINT64    unsigned long long
#endif


#if defined(WTPSP_WCE_DLL) && defined(_MSC_VER)

#if defined (WTPSP_WCE_DLL_EXPORT)
#define WTPSP_DLL_API __declspec(dllexport)
#else
#define WTPSP_DLL_API __declspec(dllimport)
#endif  /* WTPSP_WCE_DLL_EXPORT */
    
#else   /* WTPSP_WCE_DLL && _MSC_VER */

#define WTPSP_DLL_API
    
#endif  /* defined(WTPSP_WCE_DLL) && defined(_MSC_VER) */


#define WTPSPAPI(type, name, arg) extern WTPSP_DLL_API type __STDCALL name arg; 

#define WTPSP_MAX_8U     (0xFF)
#define WTPSP_MAX_16U    (0xFFFF)
#define WTPSP_MAX_32U    (0xFFFFFFFF)
#define WTPSP_MIN_8U     (0)
#define WTPSP_MIN_16U    (0)
#define WTPSP_MIN_32U    (0)
#define WTPSP_MIN_8S     (-128)
#define WTPSP_MAX_8S     (127)
#define WTPSP_MIN_16S    (-32768)
#define WTPSP_MAX_16S    (32767)
#define WTPSP_MIN_32S    (-2147483647 - 1)
#define WTPSP_MAX_32S    (2147483647)

#if defined( _WIN32 ) || defined ( _WIN64 )

#define WTPSP_MAX_64S  ( 9223372036854775807i64 )
#define WTPSP_MIN_64S  (-9223372036854775807i64 - 1 )

#else

#define WTPSP_MAX_64S  ( 9223372036854775807LL )
#define WTPSP_MIN_64S  (-9223372036854775807LL - 1 )

#endif

#define WTPSP_COUNT_OF( obj )  (sizeof(obj)/sizeof(obj[0]))

#define WTPSP_MAX( a, b ) ( ((a) > (b)) ? (a) : (b) )
#define WTPSP_MIN( a, b ) ( ((a) < (b)) ? (a) : (b) )

typedef unsigned char   Wtpsp8u;
typedef unsigned short  Wtpsp16u;
typedef unsigned int    Wtpsp32u;

typedef signed char     Wtpsp8s;
typedef signed short    Wtpsp16s;
typedef signed int      Wtpsp32s;
typedef float           Wtpsp32f;
typedef __INT64         Wtpsp64s;
typedef __UINT64        Wtpsp64u;
typedef double          Wtpsp64f;

typedef void            WtpspVoid;

typedef struct {
    Wtpsp8s re;
    Wtpsp8s im;
} Wtpsp8sc;

typedef struct {
    Wtpsp16s re;
    Wtpsp16s im;
} Wtpsp16sc;

typedef struct {
    Wtpsp32s re;
    Wtpsp32s im;
} Wtpsp32sc;

typedef struct {
    Wtpsp32f re;
    Wtpsp32f im;
} Wtpsp32fc;

typedef struct {
    Wtpsp64s re;
    Wtpsp64s im;
} Wtpsp64sc;

typedef struct {
    Wtpsp64f re;
    Wtpsp64f im;
} Wtpsp64fc;

typedef enum {
    WtpspCmpLess,
    WtpspCmpLessEq,
    WtpspCmpEq,
    WtpspCmpGreaterEq,
    WtpspCmpGreater
} WtpspCmpOp;

typedef enum {
    WtpspFalse = 0,
    WtpspTrue = 1
} WtpspBool;

/*
 * The following enumerator defines a status of WTPSP operations
 * negative value means error
 */
typedef enum {

    WtpspStsWtmSystemFailureErr     = 0xffff0000,   /* WTM system call failure */
    WtpspStsWtmFaultErr,                            /* WTM failure */
    WtpspStsWtmDriverNotExistErr,                   /* WTM Driver Don't exist */
    WtpspStsInvalidKeysInfo,                        /* Invalid keys' info */
    WtpspStsKeyRingFull,                            /* Key ring is full */
    WtpspStsInvalidKeyId,                           /* Invalid key id */
    WtpspStsNoUsrKey,                               /* No such an user key */
    WtpspStsInvalidKeyRing,                         /* Invalid key ring */
    WtpspStsGenKeyFailure,                          /* Generate key failure */
    WtpspStsOutOfMemErr,                            /* Out of memory */
    WtpspStsPaddingErr,                             /* Detected padding error */
    WtpspStsUnderRunErr,                            /* Data under run error */
    WtpspStsLengthErr,                              /* Wrong value of the length */
    WtpspStsErrInvalidScheme,                       /* Invalid crypto scheme */
    WtpspStsErrStateMachine,                        /* Wrong state machine */
    WtpspStsErrSequence,                            /* Wrong state sequence */
    WtpspStsNullPtrErr,                             /* Null pointer error */
    WtpspStsBadArgErr,                              /* Function arg/param is bad */
    WtpspStsNoSupport,                              /* Function not supported */
    WtpspStsPermissionErr,
    WtpspStsInvalidBinding,
    WtpspStsInvalidRequest,
    WtpspStsOutOfRangeErr,
    WtpspStsErr,                                    /* Unknown/unspecified error */

    /* no errors */
    WtpspStsNoErr                   = 0,            /* No error, it's OK */

} WtpspStatus;

#ifdef __cplusplus                       
}   /* extern "C" { */
#endif                                   

#endif /* __WTPDEFS_H__ */

