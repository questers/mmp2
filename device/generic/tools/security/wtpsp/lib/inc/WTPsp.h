/*
 * (C) Copyright 2009 Marvell International Ltd.  
 * All Rights Reserved 
 *
 * MARVELL CONFIDENTIAL
 * Copyright 2004 ~ 2009 Marvell International Ltd All Rights Reserved.
 * The source code contained or described herein and all documents related to
 * the source code ("Material") are owned by Marvell International Ltd or its
 * suppliers or licensors. Title to the Material remains with Marvell International Ltd
 * or its suppliers and licensors. The Material contains trade secrets and
 * proprietary and confidential information of Marvell or its suppliers and
 * licensors. The Material is protected by worldwide copyright and trade secret 
 * laws and treaty provisions. No part of the Material may be used, copied,
 * reproduced, modified, published, uploaded, posted, transmitted, distributed,
 * or disclosed in any way without Marvell's prior express written permission.
 *
 * No license under any patent, copyright, trade secret or other intellectual
 * property right is granted to or conferred upon you by disclosure or delivery
 * of the Materials, either expressly, by implication, inducement, estoppel or
 * otherwise. Any license under such intellectual property rights must be
 * express and approved by Marvell in writing.
 *
 * @file        WTPsp.h
 * @author      Leo Yan
 * @date        03/17/2009
 * @brief       The header file declares the library interfaces.
 *
 */

#ifndef __WTPSP_H__
#define __WTPSP_H__

#include "WTPDefs.h"

#ifdef  __cplusplus
extern "C" {
#endif

/* data structure */
typedef struct _WtpspHash WtpspSha1;
typedef struct _WtpspHash WtpspSha224;
typedef struct _WtpspHash WtpspSha256;
typedef struct _WtpspHash WtpspSha384;
typedef struct _WtpspHash WtpspSha512;
typedef struct _WtpspHash WtpspMd5;

typedef struct _WtpspHmac WtpspHmacSha1;
typedef struct _WtpspHmac WtpspHmacSha224;
typedef struct _WtpspHmac WtpspHmacSha256;
typedef struct _WtpspHmac WtpspHmacSha384;
typedef struct _WtpspHmac WtpspHmacSha512;
typedef struct _WtpspHmac WtpspHmacMd5;

typedef struct _WtpspSym WtpspAes;

typedef struct _WtpspPkcs WtpspPkcsSha1;
typedef struct _WtpspPkcs WtpspPkcsSha224;
typedef struct _WtpspPkcs WtpspPkcsSha256;

typedef struct _WtpspEccpDsa WtpspEccpDsaSha1;
typedef struct _WtpspEccpDsa WtpspEccpDsaSha224;
typedef struct _WtpspEccpDsa WtpspEccpDsaSha256;
typedef struct _WtpspEccpDsa WtpspEccpDsaSha384;
typedef struct _WtpspEccpDsa WtpspEccpDsaSha512;

typedef struct _WtpspKeyRingRoot WtpspKeyRing;


/* aes key length */
typedef enum {
    WtpspAesKey128 = 128,
    WtpspAesKey192 = 192,
    WtpspAesKey256 = 256,
} WtpspAesKeyLength;

/* symmetric scheme padding mode */
typedef enum {
    WtpspSymPaddingNone  = 0,
    WtpspSymPaddingPkcs7 = 1,
    WtpspSymPaddingZeros = 2,
} WtpspSymPaddingType;

/* rsa key length */
typedef enum {
    WtpspRsaKey1024 = 1024,
    WtpspRsaKey2048 = 2048,
} WtpspRsaKeyLength;

/* eccp key length */
typedef enum {
    WtpspEccpKey224 = 224,
    WtpspEccpKey256 = 256,
    WtpspEccpKey384 = 384,
    WtpspEccpKey521 = 544,
} WtpspEccpKeyLength;

/* eccp mode */
typedef enum {
    WtpspEccpRandom = 0,
    WtpspEccpFix    = 1,
} WtpspEccpMode;

#define WTPSP_EC_P256_CIPHER_TEXT_LEN   (128)
#define WTPSP_ECCP_MAX_COEF_SIZE        (32)

typedef struct {
    Wtpsp32u uiBits;
    Wtpsp32u uiCofactor;
    Wtpsp8u pucEcc[WTPSP_ECCP_MAX_COEF_SIZE];
    Wtpsp8u pucN[WTPSP_ECCP_MAX_COEF_SIZE];
    Wtpsp8u pucA[WTPSP_ECCP_MAX_COEF_SIZE];
    Wtpsp8u pucB[WTPSP_ECCP_MAX_COEF_SIZE];
    Wtpsp8u pucGx[WTPSP_ECCP_MAX_COEF_SIZE];
    Wtpsp8u pucGy[WTPSP_ECCP_MAX_COEF_SIZE];
} WtpspEccpDomain;

typedef enum
{
    WtpspKeyHmacSha1 = 0,
    WtpspKeyHmacSha224 = 1,
    WtpspKeyHmacSha256 = 2,
    WtpspKeyHmacSha384 = 3,
    WtpspKeyHmacSha512 = 4,
    WtpspKeyHmacMd5 = 5,
    WtpspKeyAesECB128 = 6,
    WtpspKeyAesECB192 = 7,
    WtpspKeyAesECB256 = 8,
    WtpspKeyAesCBC128 = 9,
    WtpspKeyAesCBC192 = 10,
    WtpspKeyAesCBC256 = 11,
    WtpspKeyAesCTR128 = 12,
    WtpspKeyAesCTR192 = 13,
    WtpspKeyAesCTR256 = 14,
    WtpspKeyAesXTS256 = 15,
    WtpspKeyAesXTS512 = 16,
    WtpspKeyPkcsSha1Rsa1024 = 22,
    WtpspKeyPkcsSha224Rsa1024 = 23,
    WtpspKeyPkcsSha256Rsa1024 = 24,
    WtpspKeyPkcsSha1Rsa2048 = 25,
    WtpspKeyPkcsSha224Rsa2048 = 26,
    WtpspKeyPkcsSha256Rsa2048 = 27,
    WtpspKeyEccp224DH = 28,
    WtpspKeyEccp256DH = 29,
    WtpspKeyEccp384DH = 30,
    WtpspKeyEccp521DH = 31,
    WtpspKeySha1Eccp224 = 32,
    WtpspKeySha224Eccp224 = 33,
    WtpspKeySha256Eccp224 = 34,
    WtpspKeySha384Eccp224 = 35,
    WtpspKeySha512Eccp224 = 36,
    WtpspKeySha1Eccp256 = 37,
    WtpspKeySha224Eccp256 = 38,
    WtpspKeySha256Eccp256 = 39,
    WtpspKeySha384Eccp256 = 40,
    WtpspKeySha512Eccp256 = 41,
    WtpspKeySha1Eccp384 = 42,
    WtpspKeySha224Eccp384 = 43,
    WtpspKeySha256Eccp384 = 44,
    WtpspKeySha384Eccp384 = 45,
    WtpspKeySha512Eccp384 = 46,
    WtpspKeySha1Eccp521 = 47,
    WtpspKeySha224Eccp521 = 48,
    WtpspKeySha256Eccp521 = 49,
    WtpspKeySha384Eccp521 = 50,
    WtpspKeySha512Eccp521 = 51,
    WtpspKeyEnd = 54,
} WtpspKeyScheme;

typedef struct {
    Wtpsp8u *pucPasswd;
    Wtpsp32u uiPasswdSize;
} WtpspPasswdInfo;

typedef struct {
    WtpspPasswdInfo stKeyRingPasswd;
    WtpspPasswdInfo stUsrKeyPasswd;
    Wtpsp32u uiKeyId;
} WtpspUsrKeyInfo;

/*
 * Wtpsp global init and exit API
 */
WTPSPAPI(WtpspStatus, WtpspInit, (void));
WTPSPAPI(WtpspStatus, WtpspExit, (void));
 
/* 
 * Wtpsp Key Ring Function Object Module API
 */

/* key management interfaces */
WTPSPAPI(WtpspStatus, WtpspKeyRingBufferSize,
         (Wtpsp32u uiSymKeyNum,
          Wtpsp32u uiAsyKeyNum,
          Wtpsp32u *puiSize));

WTPSPAPI(WtpspStatus, WtpspKeyRingInit,
         (const WtpspUsrKeyInfo *pstEndorsementKeyInfo,
          const WtpspKeyRing *pstEndorsementKeyRing,
          const WtpspPasswdInfo *pstKeyRingPasswd,
          Wtpsp32u uiSymKeyNum,
          Wtpsp32u uiAsymKeyNum,
          WtpspKeyRing *pstKeyRing));

WTPSPAPI(WtpspStatus, WtpspKeyRingCreateSymUsrKey,
         (const WtpspUsrKeyInfo *pstEndorsementKeyInfo,
          const WtpspKeyRing *pstEndorsementKeyRing,
          const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          WtpspKeyScheme enKeyScheme,
          Wtpsp32u *puiKeyId,
          WtpspKeyRing *pstKeyRing));
WTPSPAPI(WtpspStatus, WtpspKeyRingCreateRsaUsrKey,
         (const WtpspUsrKeyInfo *pstEndorsementKeyInfo,
          const WtpspKeyRing *pstEndorsementKeyRing,
          const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          WtpspKeyScheme enKeyScheme,
          Wtpsp32u uiLimit,
          Wtpsp32u uiTrials,
          Wtpsp32u *puiKeyId,                             /* private key id */
          Wtpsp8u *pucRsaKeyN,
          Wtpsp8u *pucRsaKeyE,
          WtpspKeyRing *pstKeyRing));
WTPSPAPI(WtpspStatus, WtpspKeyRingCreateEccpUsrKey,
         (const WtpspUsrKeyInfo *pstEndorsementKeyInfo,
          const WtpspKeyRing *pstEndorsementKeyRing,
          const WtpspPasswdInfo *pstKeyRingPasswd,
          const WtpspPasswdInfo *pstUsrKeyPasswd,
          WtpspKeyScheme enKeyScheme,
          Wtpsp32u *puiKeyId,                             /* private key id */
          Wtpsp8u *pucPublicKeyX,
          Wtpsp8u *pucPublicKeyY,
          WtpspKeyRing *pstKeyRing));
WTPSPAPI(WtpspStatus, WtpspKeyRingCreateEccpDHSharedKey,
         (const WtpspUsrKeyInfo *pstEccpPrivateKeyInfo,   /* own private key info */
          const WtpspKeyRing *pstEccpPrivateKeyRing,
          Wtpsp8u *pucPeerPublicKeyX,                     /* peer public key info */
          Wtpsp8u *pucPeerPublicKeyY,
          WtpspEccpKeyLength enPeerPublicKeyLen,
          const WtpspPasswdInfo *pstKeyRingPasswd,        /* for output key info */
          WtpspKeyScheme enKeyScheme,
          Wtpsp32u *puiKeyId,
          WtpspKeyRing *pstKeyRing));

WTPSPAPI(WtpspStatus, WtpspKeyRingDeleteUsrKey,
         (const WtpspUsrKeyInfo *pstUsrKeyInfo,
          WtpspKeyRing *pstKeyRing));

WTPSPAPI(WtpspStatus, WtpspKeyRingGetTotalKeyNum,
         (Wtpsp32u *puiSymKeyNum,
          Wtpsp32u *puiAsymKeyNum,
          WtpspKeyRing *pstKeyRing));
WTPSPAPI(WtpspStatus, WtpspKeyRingGetUsedKeyNum,
         (Wtpsp32u *puiSymKeyNum,
          Wtpsp32u *puiAsymKeyNum,
          WtpspKeyRing *pstKeyRing));
WTPSPAPI(WtpspStatus, WtpspKeyRingGetFreeKeyNum,
         (Wtpsp32u *puiSymKeyNum,
          Wtpsp32u *puiAsymKeyNum,
          WtpspKeyRing *pstKeyRing));

WTPSPAPI(WtpspStatus, WtpspKeyRingImageBufferSize,
         (Wtpsp32u uiSymKeyNum,
          Wtpsp32u uiAsymKeyNum,
          Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspKeyRingImageGetTotalKeyNum,
         (Wtpsp32u *puiSymKeyNum,
          Wtpsp32u *puiAsymKeyNum,
          Wtpsp8u *pucKeyRingImage));
WTPSPAPI(WtpspStatus, WtpspKeyRingImageSave,
         (const WtpspKeyRing *pstKeyRing,
          Wtpsp8u *pucKeyRingImage));
WTPSPAPI(WtpspStatus, WtpspKeyRingImageRestore,
         (const Wtpsp8u *pucKeyRingImage,
          WtpspKeyRing *pstKeyRing));

WTPSPAPI(WtpspStatus, WtpspUtilGenerateEccpKey,
         (WtpspKeyScheme enKeyScheme,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          Wtpsp8u *pucPrivateKey,
          Wtpsp8u *pucPublicKeyX,
          Wtpsp8u *pucPublicKeyY));
WTPSPAPI(WtpspStatus, WtpspUtilDeriveEccpPubKey,
         (WtpspKeyScheme enKeyScheme,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          Wtpsp8u *pucPrivateKey,
          Wtpsp8u *pucPublicKeyX,
          Wtpsp8u *pucPublicKeyY));
WTPSPAPI(WtpspStatus, WtpspUtilGenerateEccpDHSharedKey,
         (WtpspKeyScheme enKeyScheme,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          Wtpsp8u *pucPeerPublicKeyX,
          Wtpsp8u *pucPeerPublicKeyY,
          Wtpsp8u *pucPrivateKey,
          Wtpsp8u *pucSharedKey));
WTPSPAPI(WtpspStatus, WtpspPlayReadyEccp256DecryptCipherKey,
        (const Wtpsp8u *pucCipherKey, Wtpsp32u uiCipherKeyLen,
        const WtpspUsrKeyInfo *pstEccp256PrvKeyInfo,
        WtpspKeyRing *pstEccp256PrvKeyRing,
        const WtpspPasswdInfo *pstLsbKeyRingPasswd,
        WtpspKeyScheme enLsb128KeyScheme,
        Wtpsp32u *puiLsb128KeyId,
        WtpspKeyRing *pstLsb128KeyRing,
        const WtpspPasswdInfo *pstMsbKeyRingPasswd,
        WtpspKeyScheme enMsb128KeyScheme,
        Wtpsp32u *puiMsb128KeyId,
        WtpspKeyRing *pstMsb128KeyRing));

/*
 * Wtpsp One-Way Hash Function Object Module API
 */

/* hash functional interface */
WTPSPAPI(WtpspStatus, WtpspSha1BufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspSha1Init, (WtpspSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha1Update,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha1Final,(Wtpsp8u *pMD, WtpspSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha1MessageDigest,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pMD));

WTPSPAPI(WtpspStatus, WtpspSha224BufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspSha224Init, (WtpspSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha224Update,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha224Final, (Wtpsp8u *pMD, WtpspSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha224MessageDigest,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pMD));

WTPSPAPI(WtpspStatus, WtpspSha256BufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspSha256Init, (WtpspSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha256Update,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha256Final, (Wtpsp8u *pMD, WtpspSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha256MessageDigest,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pMD));

WTPSPAPI(WtpspStatus, WtpspSha384BufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspSha384Init, (WtpspSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha384Update,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha384Final, (Wtpsp8u *pMD, WtpspSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha384MessageDigest,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pMD));

WTPSPAPI(WtpspStatus, WtpspSha512BufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspSha512Init, (WtpspSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha512Update,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha512Final, (Wtpsp8u *pMD, WtpspSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspSha512MessageDigest,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pMD));

WTPSPAPI(WtpspStatus, WtpspMd5BufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspMd5Init, (WtpspMd5 *pstState));
WTPSPAPI(WtpspStatus, WtpspMd5Update,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspMd5 *pstState));
WTPSPAPI(WtpspStatus, WtpspMd5Final,(Wtpsp8u *pMD, WtpspMd5 *pstState));
WTPSPAPI(WtpspStatus, WtpspMd5MessageDigest,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pMD));

/* 
 * Wtpsp HMAC Function Object Module API
 */

/* hmac functional interface */
WTPSPAPI(WtpspStatus, WtpspHmacSha1BufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspHmacSha1InitNonFips,
         (const Wtpsp8u *pucKey,
          Wtpsp32u uiKeyLen,
          WtpspHmacSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha1InitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspHmacSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha1Update,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspHmacSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha1Final,
         (Wtpsp8u *pMAC,
          Wtpsp32u uiMACLen,
          WtpspHmacSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha1MessageDigestNonFips,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucKey,
          Wtpsp32u uiKeyLen,
          Wtpsp8u *pMAC,
          Wtpsp32u uiMACLen));

WTPSPAPI(WtpspStatus, WtpspHmacSha224BufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspHmacSha224InitNonFips,
         (const Wtpsp8u *pucKey,
          Wtpsp32u uiKeyLen,
          WtpspHmacSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha224InitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspHmacSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha224Update,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspHmacSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha224Final,
         (Wtpsp8u *pMAC,
          Wtpsp32u uiMACLen,
          WtpspHmacSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha224MessageDigestNonFips,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucKey,
          Wtpsp32u uiKeyLen,
          Wtpsp8u *pMAC,
          Wtpsp32u uiMACLen));

WTPSPAPI(WtpspStatus, WtpspHmacSha256BufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspHmacSha256InitNonFips,
         (const Wtpsp8u *pucKey,
          Wtpsp32u uiKeyLen,
          WtpspHmacSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha256InitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspHmacSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha256Update,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspHmacSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha256Final,
         (Wtpsp8u *pMAC,
          Wtpsp32u uiMACLen,
          WtpspHmacSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha256MessageDigestNonFips,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucKey,
          Wtpsp32u uiKeyLen,
          Wtpsp8u *pMAC,
          Wtpsp32u uiMACLen));

WTPSPAPI(WtpspStatus, WtpspHmacSha384BufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspHmacSha384InitNonFips,
         (const Wtpsp8u *pucKey,
          Wtpsp32u uiKeyLen,
          WtpspHmacSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha384InitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspHmacSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha384Update,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspHmacSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha384Final,
         (Wtpsp8u *pMAC,
          Wtpsp32u uiMACLen,
          WtpspHmacSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha384MessageDigestNonFips,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucKey,
          Wtpsp32u uiKeyLen,
          Wtpsp8u *pMAC,
          Wtpsp32u uiMACLen));

WTPSPAPI(WtpspStatus, WtpspHmacSha512BufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspHmacSha512InitNonFips,
         (const Wtpsp8u *pucKey,
          Wtpsp32u uiKeyLen,
          WtpspHmacSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha512InitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspHmacSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha512Update,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspHmacSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha512Final,
         (Wtpsp8u *pMAC,
          Wtpsp32u uiMACLen,
          WtpspHmacSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacSha512MessageDigestNonFips,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucKey,
          Wtpsp32u uiKeyLen,
          Wtpsp8u *pMAC,
          Wtpsp32u uiMACLen));

WTPSPAPI(WtpspStatus, WtpspHmacMd5BufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspHmacMd5InitNonFips,
         (const Wtpsp8u *pucKey,
          Wtpsp32u uiKeyLen,
          WtpspHmacMd5 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacMd5InitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspHmacMd5 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacMd5Update,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspHmacMd5 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacMd5Final,
         (Wtpsp8u *pMAC,
          Wtpsp32u uiMACLen,
          WtpspHmacMd5 *pstState));
WTPSPAPI(WtpspStatus, WtpspHmacMd5MessageDigestNonFips,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucKey,
          Wtpsp32u uiKeyLen,
          Wtpsp8u *pMAC,
          Wtpsp32u uiMACLen));

/* 
 * Wtpsp Symmetric Cipher Schemes Function Object Module API
 */

/* symmetric functional interface */

/* aes interfaces */
WTPSPAPI(WtpspStatus, WtpspAesBufferSize,
         (Wtpsp32u *puiSize));

/* ecb encrypt */
WTPSPAPI(WtpspStatus, WtpspAesEncryptECBInitNonFips, 
         (const Wtpsp8u *pucKey,
          WtpspAesKeyLength enKeyLen,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesEncryptECBInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesEncryptECBProcess,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiSrcMsgLen,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesEncryptECBFinish,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiSrcMsgLen,
          WtpspSymPaddingType enAesPaddingType,
          WtpspAes *pstState));

/* ecb decrypt */
WTPSPAPI(WtpspStatus, WtpspAesDecryptECBInitNonFips,
         (const Wtpsp8u *pucKey,
          WtpspAesKeyLength enKeyLen,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesDecryptECBInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesDecryptECBProcess,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiDstMsgLen,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesDecryptECBFinish,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiDstMsgLen,
          WtpspSymPaddingType enAesPaddingType,
          WtpspAes *pstState));

/* cbc encrypt */
WTPSPAPI(WtpspStatus, WtpspAesEncryptCBCInitNonFips,
         (const Wtpsp8u *pucKey,
          WtpspAesKeyLength enKeyLen,
          const Wtpsp8u *pucIv,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesEncryptCBCInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          const Wtpsp8u *pucIv,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesEncryptCBCProcess,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiSrcMsgLen,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesEncryptCBCFinish,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiSrcMsgLen,
          WtpspSymPaddingType enAesPaddingType,
          WtpspAes *pstState));

/* cbc decrypt */
WTPSPAPI(WtpspStatus, WtpspAesDecryptCBCInitNonFips,
         (const Wtpsp8u *pucKey,
          WtpspAesKeyLength enKeyLen,
          const Wtpsp8u *pucIv,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesDecryptCBCInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          const Wtpsp8u *pucIv,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesDecryptCBCProcess,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiDstMsgLen,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesDecryptCBCFinish,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiDstMsgLen,
          WtpspSymPaddingType enAesPaddingType,
          WtpspAes *pstState));

/* ctr encrypt */
WTPSPAPI(WtpspStatus, WtpspAesEncryptCTRInitNonFips,
         (const Wtpsp8u *pucKey,
          WtpspAesKeyLength enKeyLen,
          const Wtpsp8u *pucIv,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesEncryptCTRInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          const Wtpsp8u *pucIv,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesEncryptCTRProcess,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiSrcMsgLen,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesEncryptCTRFinish,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiSrcMsgLen,
          WtpspSymPaddingType enAesPaddingType,
          WtpspAes *pstState));

/* ctr decrypt */
WTPSPAPI(WtpspStatus, WtpspAesDecryptCTRInitNonFips,
         (const Wtpsp8u *pucKey,
          WtpspAesKeyLength enKeyLen,
          const Wtpsp8u *pucIv,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesDecryptCTRInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          const Wtpsp8u *pucIv,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesDecryptCTRProcess,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiDstMsgLen,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesDecryptCTRFinish,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiDstMsgLen,
          WtpspSymPaddingType enAesPaddingType,
          WtpspAes *pstState));

/* xts encrypt */
WTPSPAPI(WtpspStatus, WtpspAesEncryptXTSInitNonFips,
         (const Wtpsp8u *pucKey0,
          const Wtpsp8u *pucKey1,
          WtpspAesKeyLength enKeyLen,
          const Wtpsp8u *pucIv,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesEncryptXTSInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          const Wtpsp8u *pucIv,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesEncryptXTSProcess,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiSrcMsgLen,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesEncryptXTSFinish,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiSrcMsgLen,
          WtpspSymPaddingType enAesPaddingType,
          WtpspAes *pstState));

/* xts decrypt */
WTPSPAPI(WtpspStatus, WtpspAesDecryptXTSInitNonFips,
         (const Wtpsp8u *pucKey0,
          const Wtpsp8u *pucKey1,
          WtpspAesKeyLength enKeyLen,
          const Wtpsp8u *pucIv,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesDecryptXTSInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          const Wtpsp8u *pucIv,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesDecryptXTSProcess,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiDstMsgLen,
          WtpspAes *pstState));
WTPSPAPI(WtpspStatus, WtpspAesDecryptXTSFinish,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp8u *pucDstMsg,
          Wtpsp32u uiDstMsgLen,
          WtpspSymPaddingType enAesPaddingType,
          WtpspAes *pstState));


/* 
 * Wtpsp PKCS Function Object Module API
 */

/* pkcs functional interface */
WTPSPAPI(WtpspStatus, WtpspPkcsSha1VerifyBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspPkcsSha1VerifyInit,
         (const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyE,
          WtpspRsaKeyLength enKeyLen,
          WtpspPkcsSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha1VerifyUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspPkcsSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha1VerifyFinal,
         (const Wtpsp8u *pucSignature,
          WtpspBool *pbResult,
          WtpspPkcsSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha1Verify,
         (const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyE,
          WtpspRsaKeyLength enKeyLen,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucSignature,
          WtpspBool *pbResult));

WTPSPAPI(WtpspStatus, WtpspPkcsSha1SignBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspPkcsSha1SignInitNonFips,
         (const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyD,
          WtpspRsaKeyLength enKeyLen,
          WtpspPkcsSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha1SignInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspPkcsSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha1SignUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspPkcsSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha1SignFinal,
         (Wtpsp8u *pucSignature,
          WtpspPkcsSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha1SignNonFips,
         (const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyD,
          WtpspRsaKeyLength enKeyLen,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pucSignature));

WTPSPAPI(WtpspStatus, WtpspPkcsSha224VerifyBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspPkcsSha224VerifyInit,
         (const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyE,
          WtpspRsaKeyLength enKeyLen,
          WtpspPkcsSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha224VerifyUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspPkcsSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha224VerifyFinal,
         (const Wtpsp8u *pucSignature,
          WtpspBool *pbResult,
          WtpspPkcsSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha224Verify,
         (const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyE,
          WtpspRsaKeyLength enKeyLen,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucSignature,
          WtpspBool *pbResult));

WTPSPAPI(WtpspStatus, WtpspPkcsSha224SignBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspPkcsSha224SignInitNonFips,
         (const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyD,
          WtpspRsaKeyLength enKeyLen,
          WtpspPkcsSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha224SignInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspPkcsSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha224SignUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspPkcsSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha224SignFinal,
         (Wtpsp8u *pucSignature,
          WtpspPkcsSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha224SignNonFips,
         (const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyD,
          WtpspRsaKeyLength enKeyLen,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pucSignature));

WTPSPAPI(WtpspStatus, WtpspPkcsSha256VerifyBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspPkcsSha256VerifyInit,
         (const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyE,
          WtpspRsaKeyLength enKeyLen,
          WtpspPkcsSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha256VerifyUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspPkcsSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha256VerifyFinal,
         (const Wtpsp8u *pucSignature,
          WtpspBool *pbResult,
          WtpspPkcsSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha256Verify,
         (const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyE,
          WtpspRsaKeyLength enKeyLen,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucSignature,
          WtpspBool *pbResult));

WTPSPAPI(WtpspStatus, WtpspPkcsSha256SignBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspPkcsSha256SignInitNonFips,
         (const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyD,
          WtpspRsaKeyLength enKeyLen,
          WtpspPkcsSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha256SignInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspPkcsSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha256SignUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspPkcsSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha256SignFinal,
         (Wtpsp8u *pucSignature,
          WtpspPkcsSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspPkcsSha256SignNonFips,
         (const Wtpsp8u *pucRsaKeyN,
          const Wtpsp8u *pucRsaKeyD,
          WtpspRsaKeyLength enKeyLen,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pucSignature));

/* 
 * Wtpsp ECC Function Object Module API
 */

/* eccp functional interface */
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha1VerifyBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha1VerifyInit,
         (const Wtpsp8u *pucPublicKeyX,
          const Wtpsp8u *pucPublicKeyY,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          WtpspEccpDsaSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha1VerifyUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspEccpDsaSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha1VerifyFinal,
         (const Wtpsp8u *pucSignatureR,
          const Wtpsp8u *pucSignatureS,
          WtpspBool *pbResult,
          WtpspEccpDsaSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha1Verify,
         (const Wtpsp8u *pucPublicKeyX,
          const Wtpsp8u *pucPublicKeyY,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucSignatureR,
          const Wtpsp8u *pucSignatureS,
          WtpspBool *pbResult));

WTPSPAPI(WtpspStatus, WtpspEccpDsaSha1SignBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha1SignInitNonFips,
         (const Wtpsp8u *pucPrivateKey,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          WtpspEccpDsaSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha1SignInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspEccpDsaSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha1SignUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspEccpDsaSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha1SignFinal,
         (Wtpsp8u *pucSignatureR,
          Wtpsp8u *pucSignatureS,
          WtpspEccpDsaSha1 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha1SignNonFips,
         (const Wtpsp8u *pucPrivateKey,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pucSignatureR,
          Wtpsp8u *pucSignatureS));

WTPSPAPI(WtpspStatus, WtpspEccpDsaSha224VerifyBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha224VerifyInit,
         (const Wtpsp8u *pucPublicKeyX,
          const Wtpsp8u *pucPublicKeyY,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          WtpspEccpDsaSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha224VerifyUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspEccpDsaSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha224VerifyFinal,
         (const Wtpsp8u *pucSignatureR,
          const Wtpsp8u *pucSignatureS,
          WtpspBool *pbResult,
          WtpspEccpDsaSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha224Verify,
         (const Wtpsp8u *pucPublicKeyX,
          const Wtpsp8u *pucPublicKeyY,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucSignatureR,
          const Wtpsp8u *pucSignatureS,
          WtpspBool *pbResult));

WTPSPAPI(WtpspStatus, WtpspEccpDsaSha224SignBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha224SignInitNonFips,
         (const Wtpsp8u *pucPrivateKey,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          WtpspEccpDsaSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha224SignInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspEccpDsaSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha224SignUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspEccpDsaSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha224SignFinal,
         (Wtpsp8u *pucSignatureR,
          Wtpsp8u *pucSignatureS,
          WtpspEccpDsaSha224 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha224SignNonFips,
         (const Wtpsp8u *pucPrivateKey,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pucSignatureR,
          Wtpsp8u *pucSignatureS));

WTPSPAPI(WtpspStatus, WtpspEccpDsaSha256VerifyBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha256VerifyInit,
         (const Wtpsp8u *pucPublicKeyX,
          const Wtpsp8u *pucPublicKeyY,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          WtpspEccpDsaSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha256VerifyUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspEccpDsaSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha256VerifyFinal,
         (const Wtpsp8u *pucSignatureR,
          const Wtpsp8u *pucSignatureS,
          WtpspBool *pbResult,
          WtpspEccpDsaSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha256Verify,
         (const Wtpsp8u *pucPublicKeyX,
          const Wtpsp8u *pucPublicKeyY,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucSignatureR,
          const Wtpsp8u *pucSignatureS,
          WtpspBool *pbResult));

WTPSPAPI(WtpspStatus, WtpspEccpDsaSha256SignBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha256SignInitNonFips,
         (const Wtpsp8u *pucPrivateKey,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          WtpspEccpDsaSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha256SignInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspEccpDsaSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha256SignUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspEccpDsaSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha256SignFinal,
         (Wtpsp8u *pucSignatureR,
          Wtpsp8u *pucSignatureS,
          WtpspEccpDsaSha256 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha256SignNonFips,
         (const Wtpsp8u *pucPrivateKey,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pucSignatureR,
          Wtpsp8u *pucSignatureS));

WTPSPAPI(WtpspStatus, WtpspEccpDsaSha384VerifyBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha384VerifyInit,
         (const Wtpsp8u *pucPublicKeyX,
          const Wtpsp8u *pucPublicKeyY,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          WtpspEccpDsaSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha384VerifyUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspEccpDsaSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha384VerifyFinal,
         (const Wtpsp8u *pucSignatureR,
          const Wtpsp8u *pucSignatureS,
          WtpspBool *pbResult,
          WtpspEccpDsaSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha384Verify,
         (const Wtpsp8u *pucPublicKeyX,
          const Wtpsp8u *pucPublicKeyY,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucSignatureR,
          const Wtpsp8u *pucSignatureS,
          WtpspBool *pbResult));

WTPSPAPI(WtpspStatus, WtpspEccpDsaSha384SignBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha384SignInitNonFips,
         (const Wtpsp8u *pucPrivateKey,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          WtpspEccpDsaSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha384SignInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspEccpDsaSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha384SignUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspEccpDsaSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha384SignFinal,
         (Wtpsp8u *pucSignatureR,
          Wtpsp8u *pucSignatureS,
          WtpspEccpDsaSha384 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha384SignNonFips,
         (const Wtpsp8u *pucPrivateKey,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pucSignatureR,
          Wtpsp8u *pucSignatureS));

WTPSPAPI(WtpspStatus, WtpspEccpDsaSha512VerifyBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha512VerifyInit,
         (const Wtpsp8u *pucPublicKeyX,
          const Wtpsp8u *pucPublicKeyY,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          WtpspEccpDsaSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha512VerifyUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspEccpDsaSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha512VerifyFinal,
         (const Wtpsp8u *pucSignatureR,
          const Wtpsp8u *pucSignatureS,
          WtpspBool *pbResult,
          WtpspEccpDsaSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha512Verify,
         (const Wtpsp8u *pucPublicKeyX,
          const Wtpsp8u *pucPublicKeyY,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          const Wtpsp8u *pucSignatureR,
          const Wtpsp8u *pucSignatureS,
          WtpspBool *pbResult));

WTPSPAPI(WtpspStatus, WtpspEccpDsaSha512SignBufferSize, (Wtpsp32u *puiSize));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha512SignInitNonFips,
         (const Wtpsp8u *pucPrivateKey,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          WtpspEccpDsaSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha512SignInitFips,
         (const WtpspUsrKeyInfo *pstKeyInfo,
          WtpspKeyRing *pstKeyRing,
          WtpspEccpDsaSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha512SignUpdate,
         (const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          WtpspEccpDsaSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha512SignFinal,
         (Wtpsp8u *pucSignatureR,
          Wtpsp8u *pucSignatureS,
          WtpspEccpDsaSha512 *pstState));
WTPSPAPI(WtpspStatus, WtpspEccpDsaSha512SignNonFips,
         (const Wtpsp8u *pucPrivateKey,
          WtpspEccpKeyLength enKeyLen,
          WtpspEccpDomain *pstDomain,
          WtpspEccpMode enMode,
          const Wtpsp8u *pucSrcMsg,
          Wtpsp32u uiMsgLen,
          Wtpsp8u *pucSignatureR,
          Wtpsp8u *pucSignatureS));

/* hdcp functional interface */

#define WTPSP_HDCP_KEY_SIZE     7
#define WTPSP_HDCP_KEY_NUM      40
#define WTPSP_HDCP_KSV_SIZE     5
#define WTPSP_HDCP_WOF_SIZE     340

typedef char WtpspHdcpKeySet[WTPSP_HDCP_KEY_NUM][WTPSP_HDCP_KEY_SIZE];
typedef char WtpspHdcpKsv[WTPSP_HDCP_KSV_SIZE];
typedef char WtpspHdcpWof[WTPSP_HDCP_WOF_SIZE];

WTPSPAPI(WtpspStatus, WtpspHdcpWrapKey,
         (WtpspHdcpKeySet pucKeySet,
          WtpspHdcpKsv pucKsv,
          WtpspHdcpWof pucWof));
WTPSPAPI(WtpspStatus, WtpspHdcpLoadKey,
         (WtpspHdcpWof pucWof));

/* drbg functional interface */

/* seed length */
typedef enum {
    WtpspDRBGSeed128 = 128,
    WtpspDRBGSeed192 = 192,
    WtpspDRBGSeed256 = 256,
} WtpspDRBGSeedLength;

WTPSPAPI(WtpspStatus, WtpspDRBGReseed,
         (const Wtpsp8u *pucSeed,
          WtpspDRBGSeedLength enSeedLen));
WTPSPAPI(WtpspStatus, WtpspDRBGGen,
         (const Wtpsp8u *pucRandSeq,
          Wtpsp32u uiLen));

/* jtag functional interface */
#define WTPSP_JTAG_NONCE_SIZE   32

typedef char WtpspJtagNonce[WTPSP_JTAG_NONCE_SIZE];

WTPSPAPI(WtpspStatus, WtpspJtagStart,
         (WtpspJtagNonce pucNonce));
WTPSPAPI(WtpspStatus, WtpspJtagAuthorization,
         (WtpspKeyScheme enKeyScheme,
          Wtpsp32u uiAccessCtrol,
          Wtpsp8u *pucPublicKey0,
          Wtpsp8u *pucPublicKey1,
          Wtpsp8u *pucSignature,
          WtpspBool *pbResult));

/* provision functional interface can be used in DM and DD */

#define WTPSP_RKEK_SIZE             (256 >> 3)
#define WTPSP_RKEK_SEED_SIZE        (256 >> 3)
#define WTPSP_EC521_KEY_SIZE        (512 >> 3)
#define WTPSP_EC521_SEED_SIZE       (256 >> 3)
#define WTPSP_OEM_UNIQUE_ID_SIZE    (64 >> 3)
#define WTPSP_OEM_USB_ID_SIZE       (64 >> 3)
#define WTPSP_PLATFROM_CONFIG_SIZE  (128 >> 3)

typedef enum {
    WtpspLifeCycleCM,
    WtpspLifeCycleDM,
    WtpspLifeCycleDD,
    WtpspLifeCycleFA,
} WtpspLifeCycleType;

WTPSPAPI(WtpspStatus, WtpspOEMPlatformVerify,
         (WtpspKeyScheme enKeyScheme,
          const Wtpsp8u *pucKeyArg0,
          const Wtpsp8u *pucKeyArg1));
WTPSPAPI(WtpspStatus, WtpspOEMJtagKeyVerify,
         (WtpspKeyScheme enKeyScheme,
          const Wtpsp8u *pucKeyArg0,
          const Wtpsp8u *pucKeyArg1));
WTPSPAPI(WtpspStatus, WtpspOEMUniqueIdRead,
         (Wtpsp8u *pucId));
WTPSPAPI(WtpspStatus, WtpspOTPReadPlatformConfig,
         (Wtpsp8u *pucCfg));
WTPSPAPI(WtpspStatus, WtpspOEMUsbIdRead,
         (Wtpsp8u *pucId));
WTPSPAPI(WtpspStatus, WtpspLifecycleRead,
         (Wtpsp32u uiReq,
          WtpspLifeCycleType *pucLifecycle));
WTPSPAPI(WtpspStatus, WtpspSoftwareVersionRead,
         (Wtpsp32u uiReq,
          Wtpsp32u *pucSwVer));
WTPSPAPI(WtpspStatus, WtpspKernelVersionRead,
         (Wtpsp32u uiReq,
          Wtpsp32u *pucKernelVer));
WTPSPAPI(WtpspStatus, WtpspTrustStatusRegisterRead,
         (Wtpsp32u *puiVal));

#ifdef  __cplusplus
}
#endif

#endif /* __WTPSP_H__ */

