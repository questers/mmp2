
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

Wtpsp32u puiEccpPubKeyX[8] = {
    0x57e4e4a8, 0x973b45a9, 0x0f262f3f, 0x8a3c8fe9,
    0x5f24b6fd, 0x2b5edc13, 0x61d32da6, 0xa947215a,
};

Wtpsp32u puiEccpPubKeyY[8] = {
    0xd7ce0e11, 0x70ee1669, 0x60b0d7c7, 0x6b8d4bdd,
    0x2b965c0b, 0xff8b6a2e, 0x6343fbd8, 0xc3081fa0,
};

Wtpsp32u puiEccpPrvKey[8] = {
    0x359c7c58, 0xe5f28e51, 0xc1ae34bf, 0x2cb5bb3d,
    0x6b457615, 0x41ca74a7, 0x9cf52324, 0x7d519a41,
};

Wtpsp32u puiSrcMsg[8] = {
    0x01234567, 0x12345678, 0x23456789, 0x3456789A,
    0x456789AB, 0x56789ABC, 0x6789ABCD, 0x789ABCDE,
};

Wtpsp32u puiSignR[8], puiSignS[8];

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;
    WtpspEccpDsaSha1 *pstState;
    WtpspStatus uiStatus;

    WtpspInit();

    WtpspEccpDsaSha1VerifyBufferSize(&uiSize);

    pstState = malloc(uiSize);

    uiStatus = WtpspEccpDsaSha1SignInitNonFips(
            (Wtpsp8u *)puiEccpPrvKey, WtpspEccpKey256,
            NULL, WtpspEccpRandom, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("eccp sign init failed.\n");
        return -1;
    }

    uiStatus = WtpspEccpDsaSha1SignUpdate((Wtpsp8u *)puiSrcMsg, 32, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("eccp sign update failed.\n");
        return -1;
    }

    uiStatus = WtpspEccpDsaSha1SignFinal(
            (Wtpsp8u *)puiSignR, (Wtpsp8u *)puiSignS, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("eccp sign final failed.\n");
        return -1;
    }

    free(pstState);

    return 0;
}

