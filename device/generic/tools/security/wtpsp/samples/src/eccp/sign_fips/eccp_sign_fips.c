#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

#include "kr.h"

Wtpsp32u puiSrcMsg[8] = {
    0x01234567, 0x12345678, 0x23456789, 0x3456789A,
    0x456789AB, 0x56789ABC, 0x6789ABCD, 0x789ABCDE,
};

Wtpsp32u puiSignR[8], puiSignS[8];

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;
    WtpspEccpDsaSha1 *pstState;
    WtpspStatus uiStatus;

    WtpspUsrKeyInfo stKeyInfo;
    Wtpsp8u *pucKeyRingImg;
    WtpspKeyRing *pstKeyRing;

    FILE *fp;

    WtpspInit();

    WtpspKeyRingBufferSize(SYM_KEY_NUM, ASYM_KEY_NUM, &uiSize);
    pucKeyRingImg = malloc(uiSize);
    pstKeyRing = malloc(uiSize);

    fp = fopen("./keyring.img", "rb");
    if (!fp) {
        printf("failed to open keyring.img\n");
        return -1;
    }
    fread(pucKeyRingImg, sizeof(unsigned char), uiSize, fp);
    fclose(fp);

    WtpspKeyRingImageRestore(pucKeyRingImg, pstKeyRing);
    free(pucKeyRingImg);

    /* allocate the state structure */
    WtpspEccpDsaSha1SignBufferSize(&uiSize);
    pstState = malloc(uiSize);

    /* 
     * The eccp-dh key has been pre-loaded into the key ring already,
     * and the key id is returned back when wrapping the key;
     * At this point just use it.
     */
    stKeyInfo.stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stKeyInfo.stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");
    stKeyInfo.stUsrKeyPasswd.pucPasswd     = (Wtpsp8u *)"uk_passwd";
    stKeyInfo.stUsrKeyPasswd.uiPasswdSize  = strlen("uk_passwd");
    stKeyInfo.uiKeyId = ECCP256_SHA1_KEY_ID; /* the key id from key management APIs */

    uiStatus = WtpspEccpDsaSha1SignInitFips(&stKeyInfo, pstKeyRing, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("eccp sign init failed.\n");
        return -1;
    }

    uiStatus = WtpspEccpDsaSha1SignUpdate((Wtpsp8u *)puiSrcMsg, 32, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("eccp sign update failed.\n");
        return -1;
    }

    uiStatus = WtpspEccpDsaSha1SignFinal(
            (Wtpsp8u *)puiSignR, (Wtpsp8u *)puiSignS, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("eccp sign final failed.\n");
        return -1;
    }

    /* free resource */
    free(pstState);
    free(pstKeyRing);

    WtpspExit();

    return 0;
}

