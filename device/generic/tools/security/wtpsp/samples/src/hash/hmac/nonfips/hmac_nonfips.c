#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

Wtpsp8u pucData[48] = {
    0x0F, 0x14, 0x05, 0x65,
    0x1B, 0x28, 0x61, 0xC9,
    0xC5, 0xE7, 0x2C, 0x8E,
    0x46, 0x36, 0x08, 0xDC,
    0xFF, 0xA0, 0xD0, 0x3B,
    0x75, 0x06, 0x8C, 0x7E,
    0x24, 0x5E, 0x0D, 0x1C,
    0x06, 0xB7, 0x47, 0xDE,
    0xB3, 0x12, 0x4D, 0xC8,
    0x43, 0xBB, 0x8B, 0xA6,
    0x1F, 0x03, 0x5A, 0x7D,
    0x09, 0x38, 0x25, 0x1F,
};

Wtpsp8u pucKey[32] = {
    0x6C, 0xD4, 0xBF, 0x9A,
    0xBA, 0x9D, 0x88, 0x9C,
    0x9A, 0xF2, 0x8E, 0x80,
    0x4E, 0xCB, 0xDA, 0x6D,
    0x49, 0x76, 0x2B, 0xFB,
    0xF1, 0x3A, 0xAE, 0xA3,
    0xD6, 0x6B, 0xF2, 0x1F,
    0x2B, 0x8C, 0xFB, 0xF6,
};

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;
    WtpspStatus uiStatus;
    WtpspHmacSha1 *pstState;
    Wtpsp8u pucMAC[20];

    WtpspInit();

    /* allocate the state structure */
    WtpspHmacSha1BufferSize(&uiSize);
    pstState = malloc(uiSize);

    /* hmac initialization */
    uiStatus = WtpspHmacSha1InitNonFips(pucKey, 32, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("hmac init failed.\n");
        return -1;
    }

    /* hmac update, input the message */
    uiStatus = WtpspHmacSha1Update(pucData, 48, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("hmac update failed.\n");
        return -1;
    }

    /* get the digest value */
    uiStatus = WtpspHmacSha1Final(pucMAC, 20, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("hmac final failed.\n");
        return -1;
    }

    /* free resource */
    free(pstState);

    /* you also can get the message digest in one step */
    uiStatus = WtpspHmacSha1MessageDigestNonFips(pucData, 48, pucKey, 32, pucMAC, 20);
    if (uiStatus != WtpspStsNoErr) {
        printf("hmac digest failed.\n");
        return -1;
    }


    WtpspExit();

    return 0;
}

