#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

#include "kr.h"

Wtpsp8u pucData[48] = {
    0x0F, 0x14, 0x05, 0x65,
    0x1B, 0x28, 0x61, 0xC9,
    0xC5, 0xE7, 0x2C, 0x8E,
    0x46, 0x36, 0x08, 0xDC,
    0xFF, 0xA0, 0xD0, 0x3B,
    0x75, 0x06, 0x8C, 0x7E,
    0x24, 0x5E, 0x0D, 0x1C,
    0x06, 0xB7, 0x47, 0xDE,
    0xB3, 0x12, 0x4D, 0xC8,
    0x43, 0xBB, 0x8B, 0xA6,
    0x1F, 0x03, 0x5A, 0x7D,
    0x09, 0x38, 0x25, 0x1F,
};

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;
    WtpspStatus uiStatus;
    WtpspHmacSha1 *pstState;
    Wtpsp8u pucMAC[20];

    WtpspUsrKeyInfo stKeyInfo;
    Wtpsp8u *pucKeyRingImg;
    WtpspKeyRing *pstKeyRing;

    FILE *fp;

    WtpspInit();

    WtpspKeyRingBufferSize(SYM_KEY_NUM, ASYM_KEY_NUM, &uiSize);
    pucKeyRingImg = malloc(uiSize);
    pstKeyRing = malloc(uiSize);

    fp = fopen("./keyring.img", "rb");
    if (!fp) {
        printf("failed to open keyring.img\n");
        return -1;
    }
    fread(pucKeyRingImg, sizeof(unsigned char), uiSize, fp);
    fclose(fp);

    WtpspKeyRingImageRestore(pucKeyRingImg, pstKeyRing);
    free(pucKeyRingImg);

    /* allocate the state structure */
    WtpspHmacSha1BufferSize(&uiSize);
    pstState = malloc(uiSize);

    stKeyInfo.stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stKeyInfo.stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");
    stKeyInfo.stUsrKeyPasswd.pucPasswd     = (Wtpsp8u *)"uk_passwd";
    stKeyInfo.stUsrKeyPasswd.uiPasswdSize  = strlen("uk_passwd");
    stKeyInfo.uiKeyId = HMAC_SHA1_KEY_ID; /* the key id from key management APIs */

    /* hmac initialization */
    uiStatus = WtpspHmacSha1InitFips(&stKeyInfo, pstKeyRing, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("hmac init failed.\n");
        return -1;
    }

    /* hmac update, input the message */
    uiStatus = WtpspHmacSha1Update(pucData, 48, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("hmac update failed.\n");
        return -1;
    }

    /* get the digest value */
    uiStatus = WtpspHmacSha1Final(pucMAC, 20, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("hmac final failed.\n");
        return -1;
    }

    /* free resource */
    free(pstState);
    free(pstKeyRing);

    WtpspExit();

    return 0;
}

