
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

#include "kr.h"

#ifndef WIN

Wtpsp32u puiPeerPubKeyX[8] = {
    0xc4671af1, 0x0cea8a70, 0xdf351eb0, 0xf6e08ad1,
    0x6a075de7, 0x512657c0, 0x8b7ce93f, 0x8d824f32,
};

Wtpsp32u puiPeerPubKeyY[8] = {
    0xca75d739, 0x858a2268, 0x68b6007e, 0x0f23234c,
    0xddb6f6c4, 0xd91096c3, 0xec162248, 0x4a3650f6,
};

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;
    WtpspStatus uiStatus;

    Wtpsp8u *pucPreKeyRingImg;
    WtpspKeyRing *pstPreKeyRing;

    WtpspUsrKeyInfo stEcDhKeyInfo;
    WtpspUsrKeyInfo stEndorsementKeyInfo;

    WtpspPasswdInfo stKeyRingPasswd;

    Wtpsp32u uiKeyId;
    WtpspKeyRing *pstKeyRing;
    Wtpsp8u *pucKeyRingImg;

    FILE *fp;

    WtpspInit();

    WtpspKeyRingBufferSize(SYM_KEY_NUM, ASYM_KEY_NUM, &uiSize);
    pucPreKeyRingImg = malloc(uiSize);
    pstPreKeyRing = malloc(uiSize);

    fp = fopen("./keyring.img", "rb");
    if (!fp) {
        printf("failed to open keyring.img\n");
        return -1;
    }
    fread(pucPreKeyRingImg, sizeof(unsigned char), uiSize, fp);
    fclose(fp);

    WtpspKeyRingImageRestore(pucPreKeyRingImg, pstPreKeyRing);
    free(pucPreKeyRingImg);

    stEndorsementKeyInfo.stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stEndorsementKeyInfo.stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");
    stEndorsementKeyInfo.stUsrKeyPasswd.pucPasswd     = (Wtpsp8u *)"uk_passwd";
    stEndorsementKeyInfo.stUsrKeyPasswd.uiPasswdSize  = strlen("uk_passwd");
    stEndorsementKeyInfo.uiKeyId                      = AES_ECB256_KEY_ID;

    /* 
     * The eccp key has been pre-loaded into the key ring already,
     * and the key id is returned back when wrapping the key;
     * At this point just use it.
     */
    stEcDhKeyInfo.stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stEcDhKeyInfo.stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");
    stEcDhKeyInfo.stUsrKeyPasswd.pucPasswd     = (Wtpsp8u *)"uk_passwd";
    stEcDhKeyInfo.stUsrKeyPasswd.uiPasswdSize  = strlen("uk_passwd");
    stEcDhKeyInfo.uiKeyId = ECCP256_SHA1_KEY_ID; /* the key id from key management APIs */

    stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");

    WtpspKeyRingBufferSize(1, 0, &uiSize);
    pstKeyRing = (WtpspKeyRing *)malloc(uiSize);
    pucKeyRingImg = malloc(uiSize);

    stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");

    WtpspKeyRingInit(&stEndorsementKeyInfo, pstPreKeyRing,
            &stKeyRingPasswd, 1, 0, pstKeyRing);

    uiStatus = WtpspKeyRingCreateEccpDHSharedKey(
            &stEcDhKeyInfo, pstPreKeyRing,
            (Wtpsp8u *)puiPeerPubKeyX, (Wtpsp8u *)puiPeerPubKeyY,
            WtpspEccpKey256, &stKeyRingPasswd,
            WtpspKeyAesECB256, &uiKeyId, pstKeyRing);
    if (uiStatus != WtpspStsNoErr) {
        printf("failed to create ec-dh shared key.\n");
        printf("the sample needs to be run in DD stage, please check it.\n");
        return -1;
    }

    WtpspKeyRingImageSave(pstKeyRing, pucKeyRingImg);

    fp = fopen("ec_dh_keyring.img", "wb");
    fwrite(pucKeyRingImg, uiSize, 1, fp);
    fclose(fp);

    /* free resource */
    free(pstKeyRing);
    free(pstPreKeyRing);

    WtpspExit();

    return 0;
}

