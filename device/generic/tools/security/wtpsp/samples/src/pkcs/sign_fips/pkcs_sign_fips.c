
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

#include "kr.h"

const Wtpsp32u pucMsg[32] = {
    0xE0E26C0B, 0x0EEE1131, 0x927B00FA, 0x7DE08E26,
    0x7794565A, 0x9B947669, 0x8805FBD2, 0xB41B998A,
    0x0439A0FB, 0x9A5A2B4C, 0xEFAF9AE3, 0x21894328,
    0x8E7A658C, 0xA1B75DCB, 0xECC11D6D, 0x7745D993,
    0x584003F0, 0x84667A16, 0xE5C5FFF3, 0xADA6E89B,
    0xE9B41848, 0x59602E9E, 0x7F83FD34, 0xF6453D51,
    0x053F7FF3, 0x74DC6716, 0x9E051532, 0xC8FDE406,
    0xB1895794, 0x6A55506D, 0x6893832E, 0xD8E52B4B,
};

Wtpsp32u pucSign[32];

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;
    WtpspPkcsSha1 *pstState;
    WtpspStatus uiStatus;

    WtpspUsrKeyInfo stKeyInfo;
    Wtpsp8u *pucKeyRingImg;
    WtpspKeyRing *pstKeyRing;

    FILE *fp;

    WtpspInit();

    WtpspKeyRingBufferSize(SYM_KEY_NUM, ASYM_KEY_NUM, &uiSize);
    pucKeyRingImg = malloc(uiSize);
    pstKeyRing = malloc(uiSize);

    fp = fopen("./keyring.img", "rb");
    if (!fp) {
        printf("failed to open keyring.img\n");
        return -1;
    }
    fread(pucKeyRingImg, sizeof(unsigned char), uiSize, fp);
    fclose(fp);

    WtpspKeyRingImageRestore(pucKeyRingImg, pstKeyRing);
    free(pucKeyRingImg);

    /* allocate the state structure */
    WtpspPkcsSha1SignBufferSize(&uiSize);
    pstState = malloc(uiSize);

    /* 
     * The des key has been pre-loaded into the key ring already,
     * and the key id is returned back when wrapping the key;
     * At this point just use it.
     */
    stKeyInfo.stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stKeyInfo.stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");
    stKeyInfo.stUsrKeyPasswd.pucPasswd     = (Wtpsp8u *)"uk_passwd";
    stKeyInfo.stUsrKeyPasswd.uiPasswdSize  = strlen("uk_passwd");
    stKeyInfo.uiKeyId = PKCS_SHA1_RSA1024_KEY_ID; /* the key id from key management APIs */

    uiStatus = WtpspPkcsSha1SignInitFips(&stKeyInfo, pstKeyRing, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("pkcs sign init failed.\n");
        return -1;
    }

    /* pkcs update, input the message */
    uiStatus = WtpspPkcsSha1SignUpdate((Wtpsp8u *)pucMsg, 128, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("pkcs sign update failed.\n");
        return -1;
    }

    /* pkcs final, get the signature */
    uiStatus = WtpspPkcsSha1SignFinal((Wtpsp8u *)pucSign, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("pkcs sign final failed.\n");
        return -1;
    }

    /* free resource */
    free(pstState);
    free(pstKeyRing);

    WtpspExit();

    return 0;
}


