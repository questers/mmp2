#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

const Wtpsp32u pucRsaKeyN[32] = {
    0xDCB50F7B, 0x3BCF466D, 0xBC9CDC65, 0xF2E9E44A,
    0x667770C4, 0x23450E21, 0x25B0FCEA, 0xEBF9E1B6,
    0x077B82C1, 0x68ED5F05, 0xBBE364B9, 0xBC263C4E,
    0xA9F88D2F, 0x5F65561E, 0xD7C40DA1, 0xA071340E,
    0x9F26969D, 0xAAEB1A37, 0x0392E0E2, 0x65E95046,
    0xBFAF3805, 0x5F7D6F2C, 0x7900ED0A, 0x2EF58A5A,
    0xB8BAA3CF, 0xFAF3A012, 0xDC19B661, 0xD2CCD0E6,
    0x76D523CB, 0xA3F4F30D, 0x050F736F, 0xE1335BD8,
};

const Wtpsp32u pucRsaKeyD[32] = {
    0x3CEC6569, 0xF45D3C75, 0x50CB83C4, 0x85BE9A64,
    0x9B1102C1, 0xF5400815, 0x97792A93, 0x530F3AEB,
    0xEB391BD7, 0x63F11C91, 0xDDC0B3BB, 0x92C105E5,
    0x95BFD2AA, 0x79562E59, 0x89197D7A, 0xDEDBE594,
    0x0057DABE, 0x9D152FAE, 0xCE49B764, 0xE2D45532,
    0x5BF215F4, 0x63103712, 0xC996F7D9, 0x1997EAB3,
    0xA967E32E, 0x741FC817, 0x652F6FB9, 0x6AAF2E5D,
    0x49C3EAEA, 0x139259BE, 0x75EA30DE, 0xD353399D,
};

const Wtpsp32u pucMsg[32] = {
    0xE0E26C0B, 0x0EEE1131, 0x927B00FA, 0x7DE08E26,
    0x7794565A, 0x9B947669, 0x8805FBD2, 0xB41B998A,
    0x0439A0FB, 0x9A5A2B4C, 0xEFAF9AE3, 0x21894328,
    0x8E7A658C, 0xA1B75DCB, 0xECC11D6D, 0x7745D993,
    0x584003F0, 0x84667A16, 0xE5C5FFF3, 0xADA6E89B,
    0xE9B41848, 0x59602E9E, 0x7F83FD34, 0xF6453D51,
    0x053F7FF3, 0x74DC6716, 0x9E051532, 0xC8FDE406,
    0xB1895794, 0x6A55506D, 0x6893832E, 0xD8E52B4B,
};

Wtpsp32u pucSign[32];

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;
    WtpspPkcsSha1 *pstState;
    WtpspStatus uiStatus;

    WtpspInit();

    /* allocate the state structure */
    WtpspPkcsSha1SignBufferSize(&uiSize);
    pstState = malloc(uiSize);

    uiStatus = WtpspPkcsSha1SignInitNonFips((Wtpsp8u *)pucRsaKeyN, (Wtpsp8u *)pucRsaKeyD,
                                            WtpspRsaKey1024, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("pkcs sign init failed.\n");
        return -1;
    }

    /* pkcs update, input the message */
    uiStatus = WtpspPkcsSha1SignUpdate((Wtpsp8u *)pucMsg, 128, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("pkcs sign update failed.\n");
        return -1;
    }

    /* pkcs final, get the signature */
    uiStatus = WtpspPkcsSha1SignFinal((Wtpsp8u *)pucSign, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("pkcs sign final failed.\n");
        return -1;
    }

    /* free resource */
    free(pstState);

    WtpspExit();

    return 0;
}

