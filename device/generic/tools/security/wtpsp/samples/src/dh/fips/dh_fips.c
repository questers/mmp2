#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

#include "kr.h"

Wtpsp32u puiPrime[32] = {
    0x46c37303, 0x2c8a58a8, 0xe4a03884, 0x4dce4c3d,
    0xa7867d1e, 0x025c1710, 0x73ef52b2, 0x11ebfb97,
    0x2b679c32, 0x0d4f02bf, 0x6bd100f9, 0x02c49da9,
    0x0632408f, 0xb2d4368d, 0x0db8de59, 0x25cd3eb6,
    0x36a57200, 0x8c68f49a, 0xdb50991b, 0x2debc1c9,
    0xfa7a3f2e, 0x30fcfeb5, 0xe1369246, 0x19095951,
    0xa1da306f, 0x086eacac, 0xb751d219, 0x69f73e02,
    0xd8532623, 0x227fb1e5, 0x6d1320d8, 0xb35a95c6,
};

Wtpsp32u puiGenerator[32] = {
    0x1ad7b5db, 0xda3111fe, 0x5750f89f, 0x424229ea,
    0xb0c00843, 0x1d626376, 0x89fda5c7, 0xb97669a2,
    0xe0614a2b, 0x75f9cf6e, 0x7835666f, 0xdb95cfda,
    0x79b9a393, 0xe7120de6, 0x3cc32297, 0xb84f5492,
    0xdc8abc7b, 0x947c56de, 0xccb9423f, 0xd5346fca,
    0xec41bce3, 0x94b86256, 0x5a19ad67, 0x49f11982,
    0x7b5f4ccb, 0xb56c684e, 0x11fdcc0f, 0x1b03c9ba,
    0xd65f9433, 0x6a5d20c6, 0x9e3e8637, 0x4af57872,
};

Wtpsp32u puiPeerPubKey[32] = {
    0x9c83db04, 0x4b7feb54, 0xa9f26f40, 0xe61084c2,
    0x80896c4a, 0x09212391, 0xe2c35539, 0x8fadea10,
    0xd89733b1, 0xbb650afb, 0x97575fb5, 0x11f3f22b,
    0xb65009af, 0xba39877f, 0x7ad7fc8c, 0xdeafb861,
    0xc4369d9e, 0x10a5618c, 0xdd0d290a, 0x9330d88b,
    0xb27ffe62, 0x970f0568, 0x748b0f61, 0x0101fdaa,
    0x97daf4f4, 0xef1bc293, 0x03de54a5, 0x861da02d,
    0xe729a56f, 0xd315c39c, 0x7d726a51, 0x45215014,
};

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;
    WtpspStatus uiStatus;

    Wtpsp8u *pucPreKeyRingImg;
    WtpspKeyRing *pstPreKeyRing;

    WtpspUsrKeyInfo stDhKeyInfo;
    WtpspUsrKeyInfo stEndorsementKeyInfo;

    WtpspPasswdInfo stKeyRingPasswd;

    Wtpsp32u uiKeyId;
    WtpspKeyRing *pstKeyRing;
    Wtpsp8u *pucKeyRingImg;

    FILE *fp;

    WtpspInit();

    WtpspKeyRingBufferSize(SYM_KEY_NUM, ASYM_KEY_NUM, &uiSize);
    pucPreKeyRingImg = malloc(uiSize);
    pstPreKeyRing = malloc(uiSize);

    fp = fopen("./keyring.img", "rb");
    if (!fp) {
        printf("failed to open keyring.img\n");
        return -1;
    }
    fread(pucPreKeyRingImg, sizeof(unsigned char), uiSize, fp);
    fclose(fp);

    WtpspKeyRingImageRestore(pucPreKeyRingImg, pstPreKeyRing);
    free(pucPreKeyRingImg);

    stEndorsementKeyInfo.stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stEndorsementKeyInfo.stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");
    stEndorsementKeyInfo.stUsrKeyPasswd.pucPasswd     = (Wtpsp8u *)"uk_passwd";
    stEndorsementKeyInfo.stUsrKeyPasswd.uiPasswdSize  = strlen("uk_passwd");
    stEndorsementKeyInfo.uiKeyId                      = AES_ECB256_KEY_ID;

    /* 
     * The eccp key has been pre-loaded into the key ring already,
     * and the key id is returned back when wrapping the key;
     * At this point just use it.
     */
    stDhKeyInfo.stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stDhKeyInfo.stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");
    stDhKeyInfo.stUsrKeyPasswd.pucPasswd     = (Wtpsp8u *)"uk_passwd";
    stDhKeyInfo.stUsrKeyPasswd.uiPasswdSize  = strlen("uk_passwd");
    stDhKeyInfo.uiKeyId = RSA_DH1024_KEY_ID; /* the key id from key management APIs */

    stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");

    WtpspKeyRingBufferSize(1, 0, &uiSize);
    pstKeyRing = (WtpspKeyRing *)malloc(uiSize);
    pucKeyRingImg = malloc(uiSize);

    stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");

    WtpspKeyRingInit(&stEndorsementKeyInfo, pstPreKeyRing,
            &stKeyRingPasswd, 1, 0, pstKeyRing);

    uiStatus = WtpspKeyRingCreateDHSharedKey(
            &stDhKeyInfo, pstPreKeyRing,
            (Wtpsp8u *)puiPeerPubKey, WtpspRsaDHKey1024, (Wtpsp8u *)puiPrime,
            &stKeyRingPasswd, WtpspKeyAesECB256, &uiKeyId, pstKeyRing);
    if (uiStatus != WtpspStsNoErr) {
        printf("failed to create dh shared key.\n");
        return -1;
    }

    WtpspKeyRingImageSave(pstKeyRing, pucKeyRingImg);

    fp = fopen("dh_keyring.img", "wb");
    fwrite(pucKeyRingImg, uiSize, 1, fp);
    fclose(fp);

    /* free resource */
    free(pstKeyRing);
    free(pstPreKeyRing);

    WtpspExit();

    return 0;
}

