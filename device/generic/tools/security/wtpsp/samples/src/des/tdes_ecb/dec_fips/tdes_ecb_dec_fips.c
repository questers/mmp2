
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

#include "kr.h"

#define TDES_BLOCK_SIZE      8

Wtpsp8u pucCipherText1[32] = {
    0x29, 0x23, 0xBE, 0x84,
    0xE1, 0x6C, 0xD6, 0xAE, 
    0x52, 0x90, 0x49, 0xF1,
    0xF1, 0xBB, 0xE9, 0xEB, 
    0xB3, 0xA6, 0xDB, 0x3C,
    0x87, 0x0C, 0x3E, 0x99, 
    0x24, 0x5E, 0x0D, 0x1C,
    0x06, 0xB7, 0x47, 0xDE,
};

Wtpsp8u pucCipherText2[32] = {
    0x29, 0x23, 0xBE, 0x84,
    0xE1, 0x6C, 0xD6, 0xAE, 
    0x52, 0x90, 0x49, 0xF1,
    0xF1, 0xBB, 0xE9, 0xEB, 
    0xB3, 0xA6, 0xDB, 0x3C,
    0x87, 0x0C, 0x3E, 0x99, 
    0x24, 0x5E, 0x0D, 0x1C,
    0x06, 0xB7, 0x47, 0xDE,
};

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;
    WtpspTDes *pstState;
    WtpspStatus uiStatus;

    Wtpsp8u *pucPlainText;

    WtpspUsrKeyInfo stKeyInfo;
    Wtpsp8u *pucKeyRingImg;
    WtpspKeyRing *pstKeyRing;

    FILE *fp;

    WtpspInit();

    WtpspKeyRingBufferSize(SYM_KEY_NUM, ASYM_KEY_NUM, &uiSize);
    pucKeyRingImg = malloc(uiSize);
    pstKeyRing = malloc(uiSize);

    fp = fopen("./keyring.img", "rb");
    if (!fp) {
        printf("failed to open keyring.img\n");
        return -1;
    }
    fread(pucKeyRingImg, sizeof(unsigned char), uiSize, fp);
    fclose(fp);

    WtpspKeyRingImageRestore(pucKeyRingImg, pstKeyRing);
    free(pucKeyRingImg);

    /* allocate the state structure */
    WtpspTDesBufferSize(&uiSize);
    pstState = malloc(uiSize);

    /* 
     * The tdes key has been pre-loaded into the key ring already,
     * and the key id is returned back when wrapping the key;
     * At this point just use it.
     */
    stKeyInfo.stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stKeyInfo.stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");
    stKeyInfo.stUsrKeyPasswd.pucPasswd     = (Wtpsp8u *)"uk_passwd";
    stKeyInfo.stUsrKeyPasswd.uiPasswdSize  = strlen("uk_passwd");
    stKeyInfo.uiKeyId = TDES_ECB_KEY_ID; /* the key id from key management APIs */

    /* tdes initialization */
    uiStatus = WtpspTDesDecryptECBInitFips(&stKeyInfo, pstKeyRing, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("tdes init failed.\n");
        return -1;
    }

    /* allocate the tdestination cipher text buffer */
    pucPlainText = malloc(32);

    /* tdes process, input the message */
    uiStatus = WtpspTDesDecryptECBProcess(pucCipherText1, pucPlainText, 32, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("tdes process failed.\n");
        return -1;
    }

    /* free cipher text buffer */
    free(pucPlainText);

    /* 
     * The final length must be without the padding value,
     * otherwise will return back the padding error.
     */
    pucPlainText = malloc(16);

    /* finish the cryption */
    uiStatus = WtpspTDesDecryptECBFinish(pucCipherText2, pucPlainText, 16,
                                        WtpspSymPaddingNone, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("tdes final failed.\n");
        return -1;
    }

    free(pucPlainText);

    /* free resource */
    free(pstState);
    free(pstKeyRing);

    WtpspExit();

    return 0;
}

