#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

#define DES_BLOCK_SIZE      8

Wtpsp8u pucPlainText1[32] = {
    0x29, 0x23, 0xBE, 0x84,
    0xE1, 0x6C, 0xD6, 0xAE,
    0x52, 0x90, 0x49, 0xF1,
    0xF1, 0xBB, 0xE9, 0xEB,
    0xB3, 0xA6, 0xDB, 0x3C,
    0x87, 0x0C, 0x3E, 0x99,
    0x24, 0x5E, 0x0D, 0x1C,
    0x06, 0xB7, 0x47, 0xDE,
};

Wtpsp8u pucPlainText2[19] = {
    0x29, 0x23, 0xBE, 0x84,
    0xE1, 0x6C, 0xD6, 0xAE,
    0x52, 0x90, 0x49, 0xF1,
    0xF1, 0xBB, 0xE9, 0xEB,
    0xB3, 0xA6, 0xDB,
};

Wtpsp8u pucKey[8] = {
    0x6E, 0x3A, 0x21, 0x00,
    0x9D, 0x21, 0x43, 0xDE,
};

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;
    WtpspDes *pstState;
    WtpspStatus uiStatus;

    Wtpsp8u *pucCipherText;
    Wtpsp32u uiCipherSize;

    WtpspInit();

    /* allocate the state structure */
    WtpspDesBufferSize(&uiSize);
    pstState = malloc(uiSize);

    /* des initialization */
    uiStatus = WtpspDesEncryptECBInitNonFips(pucKey, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("des init failed.\n");
        return -1;
    }

    /* allocate the destination cipher text buffer */
    pucCipherText = malloc(32);

    /* des process, input the message */
    uiStatus = WtpspDesEncryptECBProcess(pucPlainText1, pucCipherText, 32, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("des process failed.\n");
        return -1;
    }

    /* free cipher text buffer */
    free(pucCipherText);

    /* 
     * calculate the cipher buffer size, if need padding,
     * the last operation must be block aligned.
     */
    uiCipherSize = ((19 / DES_BLOCK_SIZE) + 1) * DES_BLOCK_SIZE;
    pucCipherText = malloc(uiCipherSize);

    /* finish the cryption */
    uiStatus = WtpspDesEncryptECBFinish(pucPlainText2, pucCipherText, 19,
                                        WtpspSymPaddingPkcs7, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("des final failed.\n");
        return -1;
    }

    free(pucCipherText);

    /* free resource */
    free(pstState);

    WtpspExit();

    return 0;
}

