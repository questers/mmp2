

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

#define TDES_BLOCK_SIZE      8

Wtpsp8u pucCipherText1[32] = {
    0x29, 0x23, 0xBE, 0x84,
    0xE1, 0x6C, 0xD6, 0xAE, 
    0x52, 0x90, 0x49, 0xF1,
    0xF1, 0xBB, 0xE9, 0xEB, 
    0xB3, 0xA6, 0xDB, 0x3C,
    0x87, 0x0C, 0x3E, 0x99, 
    0x24, 0x5E, 0x0D, 0x1C,
    0x06, 0xB7, 0x47, 0xDE,
};

Wtpsp8u pucCipherText2[16] = {
    0x29, 0x23, 0xBE, 0x84,
    0xE1, 0x6C, 0xD6, 0xAE, 
    0x52, 0x90, 0x49, 0xF1,
    0xF1, 0xBB, 0xE9, 0xEB, 
};

Wtpsp8u pucKey0[8] = {
    0x6E, 0x3A, 0x21, 0x00,
    0x9D, 0x21, 0x43, 0xDE,
};

Wtpsp8u pucKey1[8] = {
    0x6E, 0x3A, 0x21, 0x00,
    0x9D, 0x21, 0x43, 0xDE,
};

Wtpsp8u pucKey2[8] = {
    0x6E, 0x3A, 0x21, 0x00,
    0x9D, 0x21, 0x43, 0xDE,
};

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;
    WtpspTDes *pstState;
    WtpspStatus uiStatus;

    Wtpsp8u *pucPlainText;

    WtpspInit();

    /* allocate the state structure */
    WtpspTDesBufferSize(&uiSize);
    pstState = malloc(uiSize);

    /* hmac initialization */
    uiStatus = WtpspTDesDecryptECBInitNonFips(pucKey0, pucKey1, pucKey2, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("tdes init failed.\n");
        return -1;
    }

    /* allocate the tdestination cipher text buffer */
    pucPlainText = malloc(32);

    /* tdes process, input the message */
    uiStatus = WtpspTDesDecryptECBProcess(pucCipherText1, pucPlainText, 32, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("tdes process failed.\n");
        return -1;
    }

    /* free cipher text buffer */
    free(pucPlainText);

    /* 
     * The final length must be without the padding value.
     */
    pucPlainText = malloc(16);

    /* finish the cryption */
    uiStatus = WtpspTDesDecryptECBFinish(pucCipherText2, pucPlainText, 16,
                                        WtpspSymPaddingNone, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("tdes final failed.\n");
        return -1;
    }

    free(pucPlainText);

    /* free resource */
    free(pstState);

    WtpspExit();

    return 0;
}


