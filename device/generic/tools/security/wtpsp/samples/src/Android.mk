LOCAL_PATH := $(call my-dir)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_cbc/dec_fips/aes_cbc_dec_fips.c

LOCAL_MODULE := aes_cbc_dec_fips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_cbc/dec_nonfips/aes_cbc_dec_nonfips.c

LOCAL_MODULE := aes_cbc_dec_nonfips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_cbc/enc_fips/aes_cbc_enc_fips.c

LOCAL_MODULE := aes_cbc_enc_fips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_cbc/enc_nonfips/aes_cbc_enc_nonfips.c

LOCAL_MODULE := aes_cbc_enc_nonfips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_ctr/dec_fips/aes_ctr_dec_fips.c

LOCAL_MODULE := aes_ctr_dec_fips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_ctr/dec_nonfips/aes_ctr_dec_nonfips.c

LOCAL_MODULE := aes_ctr_dec_nonfips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_ctr/enc_fips/aes_ctr_enc_fips.c

LOCAL_MODULE := aes_ctr_enc_fips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_ctr/enc_nonfips/aes_ctr_enc_nonfips.c

LOCAL_MODULE := aes_ctr_enc_nonfips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_ecb/dec_fips/aes_ecb_dec_fips.c

LOCAL_MODULE := aes_ecb_dec_fips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_ecb/dec_nonfips/aes_ecb_dec_nonfips.c

LOCAL_MODULE := aes_ecb_dec_nonfips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_ecb/enc_fips/aes_ecb_enc_fips.c

LOCAL_MODULE := aes_ecb_enc_fips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_ecb/enc_nonfips/aes_ecb_enc_nonfips.c

LOCAL_MODULE := aes_ecb_enc_nonfips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_xts/dec_fips/aes_xts_dec_fips.c

LOCAL_MODULE := aes_xts_dec_fips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_xts/dec_nonfips/aes_xts_dec_nonfips.c

LOCAL_MODULE := aes_xts_dec_nonfips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_xts/enc_fips/aes_xts_enc_fips.c

LOCAL_MODULE := aes_xts_enc_fips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES := libwtpsp

LOCAL_SRC_FILES := ./aes/aes_xts/enc_nonfips/aes_xts_enc_nonfips.c

LOCAL_MODULE := aes_xts_enc_nonfips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES :=\
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES :=libwtpsp

LOCAL_SRC_FILES := ./hash/hmac/fips/hmac_fips.c

LOCAL_MODULE := hmac_fips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES :=\
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES :=libwtpsp

LOCAL_SRC_FILES := ./hash/hmac/nonfips/hmac_nonfips.c

LOCAL_MODULE := hmac_nonfips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES :=\
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES :=libwtpsp

LOCAL_SRC_FILES := ./hash/sha/sha1.c

LOCAL_MODULE := sha1
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES :=\
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES :=libwtpsp

LOCAL_SRC_FILES := km/dm/dm_key_ring.c

LOCAL_MODULE := dm_key_ring
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES :=\
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES :=libwtpsp

LOCAL_SRC_FILES := pkcs/sign_fips/pkcs_sign_fips.c

LOCAL_MODULE := pkcs_sign_fips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES :=\
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES :=libwtpsp

LOCAL_SRC_FILES := pkcs/sign_nonfips/pkcs_sign_nonfips.c

LOCAL_MODULE := pkcs_sign_nonfips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES :=\
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES :=libwtpsp

LOCAL_SRC_FILES := pkcs/verify/pkcs_verify.c

LOCAL_MODULE := pkcs_verify
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES :=\
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES :=libwtpsp

LOCAL_SRC_FILES := prov/fips/prov_fips.c

LOCAL_MODULE := prov_fips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES :=\
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES :=libwtpsp

LOCAL_SRC_FILES := prov/nonfips/prov_nonfips.c

LOCAL_MODULE := prov_nonfips
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES :=\
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES :=libwtpsp

LOCAL_SRC_FILES := prov/nonfips_fips/prov_nonfips_fips_dd.c

LOCAL_MODULE := prov_nonfips_fips_dd
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES :=\
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES :=libwtpsp

LOCAL_SRC_FILES := prov/nonfips_fips/prov_nonfips_fips_dm.c

LOCAL_MODULE := prov_nonfips_fips_dm
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

###############################################################################

include $(CLEAR_VARS)

LOCAL_C_INCLUDES :=\
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/../../lib/inc

LOCAL_SHARED_LIBRARIES :=libwtpsp

LOCAL_SRC_FILES := prov/nonfips_fips/prov_nonfips_fips_trans_to_dd.c

LOCAL_MODULE := prov_nonfips_fips_trans_to_dd
LOCAL_MODULE_TAGS := optional

include $(BUILD_EXECUTABLE)

