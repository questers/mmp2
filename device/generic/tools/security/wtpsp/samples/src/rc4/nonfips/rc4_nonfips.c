
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

Wtpsp8u pucPlainText1[32] = {
    0x29, 0x23, 0xBE, 0x84,
    0xE1, 0x6C, 0xD6, 0xAE,
    0x52, 0x90, 0x49, 0xF1,
    0xF1, 0xBB, 0xE9, 0xEB,
    0xB3, 0xA6, 0xDB, 0x3C,
    0x87, 0x0C, 0x3E, 0x99,
    0x24, 0x5E, 0x0D, 0x1C,
    0x06, 0xB7, 0x47, 0xDE,
};

Wtpsp8u pucCipherText1[32];

Wtpsp8u pucPlainText2[32] = {
    0x29, 0x23, 0xBE, 0x84,
};

Wtpsp8u pucCipherText2[4];

Wtpsp8u pucKey[32] = {
    0x6E, 0x3A, 0x21, 0x00,
    0x9D, 0x21, 0x43, 0xDE,
    0x21, 0x29, 0x88, 0x54,
    0x90, 0x33, 0x8A, 0x24,
    0x6E, 0x3A, 0x21, 0x00,
    0x9D, 0x21, 0x43, 0xDE,
    0x21, 0x29, 0x88, 0x54,
    0x90, 0x33, 0x8A, 0x24,
};

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    WtpspRc4 *pstState;
    WtpspStatus uiStatus;
    Wtpsp32u uiSize;

    WtpspInit();

    /* allocate the state structure */
    WtpspRc4BufferSize(&uiSize);
    pstState = malloc(uiSize);

    uiStatus = WtpspRc4InitNonFips(pucKey, 32, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("rc4 init failed.\n");
        return -1;
    }

    /* rc4 process, input the message */
    uiStatus = WtpspRc4Process(pucPlainText1, pucCipherText1, 32, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("rc4 process failed.\n");
        return -1;
    }

    /*
     * The final length must be without the padding value.
     */

    uiStatus = WtpspRc4Finish(pucPlainText2, pucCipherText2, 4, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("rc4 final failed.\n");
        return -1;
    }

    /* free resource */
    free(pstState);

    WtpspExit();

    return 0;
}

