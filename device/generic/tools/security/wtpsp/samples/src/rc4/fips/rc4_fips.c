
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

#include "kr.h"

#define AES_BLOCK_SIZE      16

Wtpsp8u pucPlainText1[32] = {
    0x29, 0x23, 0xBE, 0x84,
    0xE1, 0x6C, 0xD6, 0xAE,
    0x52, 0x90, 0x49, 0xF1,
    0xF1, 0xBB, 0xE9, 0xEB,
    0xB3, 0xA6, 0xDB, 0x3C,
    0x87, 0x0C, 0x3E, 0x99,
    0x24, 0x5E, 0x0D, 0x1C,
    0x06, 0xB7, 0x47, 0xDE,
};

Wtpsp8u pucCipherText1[32];

Wtpsp8u pucPlainText2[32] = {
    0x29, 0x23, 0xBE, 0x84,
};

Wtpsp8u pucCipherText2[4];

Wtpsp8u pucKey[32] = {
    0x6E, 0x3A, 0x21, 0x00,
    0x9D, 0x21, 0x43, 0xDE,
    0x21, 0x29, 0x88, 0x54,
    0x90, 0x33, 0x8A, 0x24,
    0x6E, 0x3A, 0x21, 0x00,
    0x9D, 0x21, 0x43, 0xDE,
    0x21, 0x29, 0x88, 0x54,
    0x90, 0x33, 0x8A, 0x24,
};

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;
    WtpspAes *pstState;
    WtpspStatus uiStatus;

    WtpspUsrKeyInfo stKeyInfo;
    Wtpsp8u *pucKeyRingImg;
    WtpspKeyRing *pstKeyRing;

    FILE *fp;

    WtpspInit();

    WtpspKeyRingBufferSize(SYM_KEY_NUM, ASYM_KEY_NUM, &uiSize);
    pucKeyRingImg = malloc(uiSize);
    pstKeyRing = malloc(uiSize);

    fp = fopen("./keyring.img", "rb");
    if (!fp) {
        printf("failed to open keyring.img\n");
        return -1;
    }
    fread(pucKeyRingImg, sizeof(unsigned char), uiSize, fp);
    fclose(fp);

    WtpspKeyRingImageRestore(pucKeyRingImg, pstKeyRing);
    free(pucKeyRingImg);

    /* allocate the state structure */
    WtpspRc4BufferSize(&uiSize);
    pstState = malloc(uiSize);

    /* 
     * The rc4 key has been pre-loaded into the key ring already,
     * and the key id is returned back when wrapping the key;
     * At this point just use it.
     */
    stKeyInfo.stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stKeyInfo.stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");
    stKeyInfo.stUsrKeyPasswd.pucPasswd     = (Wtpsp8u *)"uk_passwd";
    stKeyInfo.stUsrKeyPasswd.uiPasswdSize  = strlen("uk_passwd");
    stKeyInfo.uiKeyId = RC4_KEY_ID; /* the key id from key management APIs */

    /* rc4 initialization */
    uiStatus = WtpspRc4InitFips(&stKeyInfo, pstKeyRing, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("rc4 init failed.\n");
        return -1;
    }

    /* rc4 process, input the message */
    uiStatus = WtpspRc4Process(pucPlainText1, pucCipherText1, 32, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("rc4 process failed.\n");
        return -1;
    }

    /*
     * The final length must be without the padding value.
     */

    uiStatus = WtpspRc4Finish(pucPlainText2, pucCipherText2, 4, pstState);
    if (uiStatus != WtpspStsNoErr) {
        printf("rc4 final failed.\n");
        return -1;
    }

    /* free resource */
    free(pstState);
    free(pstKeyRing);

    WtpspExit();

    return 0;
}
 
