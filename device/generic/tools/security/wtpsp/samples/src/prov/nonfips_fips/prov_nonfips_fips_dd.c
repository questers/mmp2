
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>
#include <WTPspDm.h>

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiTsr;
    WtpspStatus enStatus;
    Wtpsp32u auiCfg0[32], auiCfg1[32];

    WtpspInit();

    enStatus = WtpspTrustStatusRegisterRead(&uiTsr);
    if (WtpspStsNoErr != enStatus) {
        return -1; 
    }   

    if (WTPSP_TSR_RKEK_PROVED != (uiTsr & WTPSP_TSR_RKEK_PROV_MASK)) {
        auiCfg0[0] = 0x00002000;
        auiCfg0[1] = 0x00000000;
        auiCfg0[2] = 0x00000000;
        auiCfg0[3] = 0x00000000;

        enStatus = WtpspOTPWritePlatformConfig((Wtpsp8u *)auiCfg0);
        if (WtpspStsNoErr != enStatus) {
            return -1;
        }

        auiCfg0[0] = 0x00000000;
        auiCfg0[1] = 0xD96D97F0;
        auiCfg0[2] = 0x00000000;
        auiCfg0[3] = 0x00000000;

        enStatus = WtpspOTPWritePlatformConfig((Wtpsp8u *)auiCfg0);
        if (WtpspStsNoErr != enStatus) {
            return -1;
        }

        auiCfg0[0] = 0x00000000;
        auiCfg0[1] = 0x00000000;
        auiCfg0[2] = 0x20000016;
        auiCfg0[3] = 0x00000000;

        enStatus = WtpspOTPWritePlatformConfig((Wtpsp8u *)auiCfg0);
        if (WtpspStsNoErr != enStatus) {
            return -1;
        }

        auiCfg0[0] = 0x12345678;
        auiCfg0[1] = 0x87654321;

        enStatus = WtpspOEMUsbIdProvision((Wtpsp8u *)auiCfg0);
        if (WtpspStsNoErr != enStatus) {
            return -1;
        }

        enStatus = WtpspRkekProvision(WtpspOtpRkekProvGen, NULL, NULL);
        if (WtpspStsNoErr != enStatus) {
            return -1;
        }

        enStatus = WtpspEc521DkProvision(WtpspEc521DkProvGen, NULL, NULL);
        if (WtpspStsNoErr != enStatus) {
            return -1;
        }

        auiCfg0[ 0] = 0xE34F43BD;
        auiCfg0[ 1] = 0x75049E3F;
        auiCfg0[ 2] = 0xA3527B1A;
        auiCfg0[ 3] = 0x9846D1AC;
        auiCfg0[ 4] = 0xD15DC003;
        auiCfg0[ 5] = 0x0B3DF2F4;
        auiCfg0[ 6] = 0xB9AD7A35;
        auiCfg0[ 7] = 0x5DF0EC2D;
        auiCfg0[ 8] = 0xE48AF431;
        auiCfg0[ 9] = 0xAEE9F9E5;
        auiCfg0[10] = 0x4925FD4F;
        auiCfg0[11] = 0x5AAFEF7C;
        auiCfg0[12] = 0x4E676F39;
        auiCfg0[13] = 0x3A3A69BE;
        auiCfg0[14] = 0x9EC21E4D;
        auiCfg0[15] = 0xD5C1E639;
        auiCfg0[16] = 0x984AB34B;
        auiCfg0[17] = 0xE9802CB8;
        auiCfg0[18] = 0x0FA840F5;
        auiCfg0[19] = 0x28A83FE7;
        auiCfg0[20] = 0x64D6569A;
        auiCfg0[21] = 0x80172DE0;
        auiCfg0[22] = 0x450D7FA9;
        auiCfg0[23] = 0x52E6A947;
        auiCfg0[24] = 0xEEE8B271;
        auiCfg0[25] = 0x7954D939;
        auiCfg0[26] = 0x003BC642;
        auiCfg0[27] = 0x0D0AD7E5;
        auiCfg0[28] = 0x664CA269;
        auiCfg0[29] = 0xF9786672;
        auiCfg0[30] = 0xDFE54FE8;
        auiCfg0[31] = 0xACD1CC46;

        auiCfg1[ 0] = 0x00000003;
        auiCfg1[ 1] = 0x00000000;
        auiCfg1[ 2] = 0x00000000;
        auiCfg1[ 3] = 0x00000000;
        auiCfg1[ 4] = 0x00000000;
        auiCfg1[ 5] = 0x00000000;
        auiCfg1[ 6] = 0x00000000;
        auiCfg1[ 7] = 0x00000000;
        auiCfg1[ 8] = 0x00000000;
        auiCfg1[ 9] = 0x00000000;
        auiCfg1[10] = 0x00000000;
        auiCfg1[11] = 0x00000000;
        auiCfg1[12] = 0x00000000;
        auiCfg1[13] = 0x00000000;
        auiCfg1[14] = 0x00000000;
        auiCfg1[15] = 0x00000000;
        auiCfg1[16] = 0x00000000;
        auiCfg1[17] = 0x00000000;
        auiCfg1[18] = 0x00000000;
        auiCfg1[19] = 0x00000000;
        auiCfg1[20] = 0x00000000;
        auiCfg1[21] = 0x00000000;
        auiCfg1[22] = 0x00000000;
        auiCfg1[23] = 0x00000000;
        auiCfg1[24] = 0x00000000;
        auiCfg1[25] = 0x00000000;
        auiCfg1[26] = 0x00000000;
        auiCfg1[27] = 0x00000000;
        auiCfg1[28] = 0x00000000;
        auiCfg1[29] = 0x00000000;
        auiCfg1[30] = 0x00000000;
        auiCfg1[31] = 0x00000000;

        enStatus = WtpspOEMPlatformBind(
                WtpspKeyPkcsSha256Rsa1024,
                (Wtpsp8u *)auiCfg0, (Wtpsp8u *)auiCfg1);
        if (WtpspStsNoErr != enStatus) {
            return -1;
        }

        auiCfg0[ 0] = 0x33209593;
        auiCfg0[ 1] = 0x7F53D3FE;
        auiCfg0[ 2] = 0xA2740D38;
        auiCfg0[ 3] = 0x142334C5;
        auiCfg0[ 4] = 0xAD2BEC61;
        auiCfg0[ 5] = 0x7C4A3D5C;
        auiCfg0[ 6] = 0x1C77A0EC;
        auiCfg0[ 7] = 0x97E9E140;
        auiCfg0[ 8] = 0x3B14415B;
        auiCfg0[ 9] = 0xF1BC40D2;
        auiCfg0[10] = 0x92CC7659;
        auiCfg0[11] = 0x726F8F66;
        auiCfg0[12] = 0x0822E3F3;
        auiCfg0[13] = 0xAD227FF2;
        auiCfg0[14] = 0xDDF6382A;
        auiCfg0[15] = 0x584038BB;
        auiCfg0[16] = 0xA33169BE;
        auiCfg0[17] = 0x353F0F40;
        auiCfg0[18] = 0xE3C24035;
        auiCfg0[19] = 0x5D90C917;
        auiCfg0[20] = 0x4FA7C0EC;
        auiCfg0[21] = 0xB96EC9AF;
        auiCfg0[22] = 0xF4B79BC3;
        auiCfg0[23] = 0xE0DDE15C;
        auiCfg0[24] = 0xB4CA43E8;
        auiCfg0[25] = 0x5918C487;
        auiCfg0[26] = 0xA818242E;
        auiCfg0[27] = 0x93AE1FF7;
        auiCfg0[28] = 0xCD8F7519;
        auiCfg0[29] = 0x9DCDCE11;
        auiCfg0[30] = 0x43FCB57F;
        auiCfg0[31] = 0xAA1C2E1F;

        auiCfg1[ 0] = 0x00000017;
        auiCfg1[ 1] = 0x00000000;
        auiCfg1[ 2] = 0x00000000;
        auiCfg1[ 3] = 0x00000000;
        auiCfg1[ 4] = 0x00000000;
        auiCfg1[ 5] = 0x00000000;
        auiCfg1[ 6] = 0x00000000;
        auiCfg1[ 7] = 0x00000000;
        auiCfg1[ 8] = 0x00000000;
        auiCfg1[ 9] = 0x00000000;
        auiCfg1[10] = 0x00000000;
        auiCfg1[11] = 0x00000000;
        auiCfg1[12] = 0x00000000;
        auiCfg1[13] = 0x00000000;
        auiCfg1[14] = 0x00000000;
        auiCfg1[15] = 0x00000000;
        auiCfg1[16] = 0x00000000;
        auiCfg1[17] = 0x00000000;
        auiCfg1[18] = 0x00000000;
        auiCfg1[19] = 0x00000000;
        auiCfg1[21] = 0x00000000;
        auiCfg1[22] = 0x00000000;
        auiCfg1[23] = 0x00000000;
        auiCfg1[24] = 0x00000000;
        auiCfg1[25] = 0x00000000;
        auiCfg1[26] = 0x00000000;
        auiCfg1[27] = 0x00000000;
        auiCfg1[28] = 0x00000000;
        auiCfg1[29] = 0x00000000;
        auiCfg1[30] = 0x00000000;
        auiCfg1[31] = 0x00000000;

        enStatus = WtpspOEMJtagKeyBind(
                WtpspKeyPkcsSha256Rsa1024,
                (Wtpsp8u *)auiCfg0, (Wtpsp8u *)auiCfg1);
        if (WtpspStsNoErr != enStatus) {
            return -1;
        }
    }

    enStatus = WtpspLifecycleAdvance(0);
    if (WtpspStsNoErr != enStatus) {
        return -1;
    }

    WtpspExit();

    return 0;
}
