
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>
#include <WTPspDm.h>

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    WtpspStatus enStatus;

    WtpspInit();

    enStatus = WtpspSetNonFipsPerm();
    if (WtpspStsNoErr != enStatus) {
        return -1;
    }

    enStatus = WtpspLifecycleAdvance(0);
    if (WtpspStsNoErr != enStatus) {
        return -1;
    }

    WtpspExit();

    return 0;
}
