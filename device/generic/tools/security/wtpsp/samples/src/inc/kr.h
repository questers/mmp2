
#ifndef _KR_H_
#define _KR_H_

#define SYM_KEY_NUM                 (10)
#define ASYM_KEY_NUM                (3)

#define AES_CBC256_KEY_ID           (0x80000000)
#define AES_CTR256_KEY_ID           (0x80000001)
#define AES_ECB256_KEY_ID           (0x80000002)
#define AES_XTS256_KEY_ID           (0x80000003)
#if 0
#define DES_CBC_KEY_ID              (0x80000004)
#define DES_ECB_KEY_ID              (0x80000005)
#define TDES_CBC_KEY_ID             (0x80000006)
#define TDES_ECB_KEY_ID             (0x80000007)
#endif
#define HMAC_SHA1_KEY_ID            (0x80000004)
#if 0
#define RC4_KEY_ID                  (0x80000009)
#endif
#define PKCS_SHA1_RSA1024_KEY_ID    (0x80000005)
#define ECCP256_SHA1_KEY_ID         (0x80000006)
#if 0
#define RSA_DH1024_KEY_ID           (0x8000000C)
#endif

#endif /* _KR_H_ */

