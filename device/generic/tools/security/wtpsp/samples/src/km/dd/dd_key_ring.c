#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPsp.h>

#include "kr.h"

#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    WtpspStatus uiStatus;
    Wtpsp32u uiSize;

    Wtpsp8u *pucEndorsementKeyRingImg;
    WtpspKeyRing *pstEndorsementKeyRing;
    Wtpsp8u *pucKeyRingImg;
    WtpspKeyRing *pstKeyRing;
    Wtpsp32u uiKeyId;

    WtpspUsrKeyInfo stEndorsementKeyInfo;
    WtpspPasswdInfo stKeyRingPasswd;
    WtpspPasswdInfo stUsrKeyPasswd;

    FILE *fp;

    WtpspInit();

    WtpspKeyRingBufferSize(SYM_KEY_NUM, ASYM_KEY_NUM, &uiSize);
    pucEndorsementKeyRingImg = malloc(uiSize);
    pstEndorsementKeyRing = (WtpspKeyRing *)malloc(uiSize);

    fp = fopen("./keyring.img", "rb");
    if (!fp) {
        printf("failed to open keyring.img\n");
        return -1; 
    }   
    fread(pucEndorsementKeyRingImg, sizeof(unsigned char), uiSize, fp);
    fclose(fp);

    WtpspKeyRingImageRestore(pucEndorsementKeyRingImg, pstEndorsementKeyRing);
    free(pucEndorsementKeyRingImg);

    stEndorsementKeyInfo.stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"kr_passwd";
    stEndorsementKeyInfo.stKeyRingPasswd.uiPasswdSize = strlen("kr_passwd");
    stEndorsementKeyInfo.stUsrKeyPasswd.pucPasswd     = (Wtpsp8u *)"uk_passwd";
    stEndorsementKeyInfo.stUsrKeyPasswd.uiPasswdSize  = strlen("uk_passwd");
    stEndorsementKeyInfo.uiKeyId                      = AES_ECB256_KEY_ID;

    stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)"dd_kr_passwd";
    stKeyRingPasswd.uiPasswdSize = strlen("dd_kr_passwd");

    /* dd key ring: sym_key - 1 asym_key - 1 */
    WtpspKeyRingBufferSize(1, 1, &uiSize);
    pstKeyRing = malloc(uiSize);
    pucKeyRingImg = malloc(uiSize);

    /* init the key ring */
    uiStatus = WtpspKeyRingInit(&stEndorsementKeyInfo, pstEndorsementKeyRing,
                     &stKeyRingPasswd, 1, 1, pstKeyRing);
    if (uiStatus != WtpspStsNoErr) {
        printf("failed to init dd key ring. 0x%08x\n", uiStatus);
        return -1;
    }

    stUsrKeyPasswd.pucPasswd    = (Wtpsp8u *)"dd_uk_passwd";
    stUsrKeyPasswd.uiPasswdSize = strlen("dd_uk_passwd");

    /* create a des key */
    uiStatus = WtpspKeyRingCreateSymUsrKey(&stEndorsementKeyInfo,
                                           pstEndorsementKeyRing,
                                           &stKeyRingPasswd,
                                           &stUsrKeyPasswd,
                                           WtpspKeyDesECB,
                                           &uiKeyId,
                                           pstKeyRing);
    if (uiStatus != WtpspStsNoErr) {
        printf("key ring create sym key failed.\n");
        return -1;
    }

    WtpspKeyRingImageSave(pstKeyRing, pucKeyRingImg);

    fp = fopen("dd_keyring.img", "wb");
    fwrite(pucKeyRingImg, uiSize, 1, fp);
    fclose(fp);

    free(pstEndorsementKeyRing);
    free(pucKeyRingImg);
    free(pstKeyRing);

    WtpspExit();

    return 0;
}

