#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN
#include <windows.h>
#endif

#include <WTPspDm.h>

#include "kr.h"

#define KEY_RING_PASSWD     "kr_passwd"
#define USR_KEY_PASSWD      "uk_passwd"

#define KEY_IMG_NAME        "keyring.img"

Wtpsp32u pucAesCbc256Key[8] = {
    0x00000000, 0x11111111, 0x22222222, 0x33333333,
    0x44444444, 0x55555555, 0x66666666, 0x77777777,
};

Wtpsp32u pucAesCtr256Key[8] = {
    0x00000000, 0x11111111, 0x22222222, 0x33333333,
    0x44444444, 0x55555555, 0x66666666, 0x77777777,
};

Wtpsp32u pucAesEcb256Key[8] = {
    0x00000000, 0x11111111, 0x22222222, 0x33333333,
    0x44444444, 0x55555555, 0x66666666, 0x77777777,
};

Wtpsp32u pucAesXts256Key[8] = {
    0x00000000, 0x11111111, 0x22222222, 0x33333333,
    0x44444444, 0x55555555, 0x66666666, 0x77777777,
};

Wtpsp32u pucDesCbcKey[2] = {
    0x00000000, 0x11111111,
};

Wtpsp32u pucDesEcbKey[2] = {
    0x00000000, 0x1111111,
};

Wtpsp32u pucTDesCbcKey[2] = {
    0x00000000, 0x11111111,
};

Wtpsp32u pucTDesEcbKey[2] = {
    0x00000000, 0x1111111,
};

Wtpsp32u pucHmacSha1Key[4] = {
    0x00000000, 0x11111111, 0x22222222, 0x33333333,
};

Wtpsp32u pucRc4Key[8] = {
    0x00000000, 0x11111111, 0x22222222, 0x33333333,
    0x44444444, 0x55555555, 0x66666666, 0x77777777,
};

Wtpsp32u puiRsaKeyN[32] = {
    0x92b354c3, 0x6ad51948, 0x998c5cc5, 0xe475d1f8,
    0x41c19c4b, 0x06b8d039, 0x1ebb8a14, 0xe5a68256,
    0xb3e748d7, 0x792486eb, 0x409aedbd, 0x9780ba64,
    0xab971a69, 0xefc86574, 0x7909511b, 0xde7797a9,
    0x10d4f952, 0x42fdfd24, 0xb7c9e1e7, 0x92858a48,
    0x70ca4502, 0x236cc8c2, 0x441232fa, 0x84511e29,
    0xea02315a, 0x1697e28d, 0xb0cfca2c, 0xad06cd39,
    0x88c88f5a, 0x1abe53d4, 0x6ac02519, 0xdfd8f6de,
};

Wtpsp32u puiRsaKeyD[32] = {
    0x0dcafbab, 0x69214040, 0xf70489fc, 0x01fc34db,
    0xab4b92d2, 0x872ae114, 0x944f65c8, 0xc7de24b2,
    0x8b847fd9, 0xa44fc079, 0x9d35b745, 0xcb8e9cc8,
    0xb3e35cbf, 0x7ebbd206, 0x0d2f525c, 0xaa4ee5ae,
    0xb5e350e0, 0x81fea8c2, 0xcfdbebef, 0xb703b185,
    0xf5dc2e01, 0xc24885d6, 0x82b6cca6, 0x5836141b,
    0x9c01763c, 0x0f0fec5e, 0x208a86c8, 0x1e0488d1,
    0xb085b4e7, 0xbc7ee28d, 0xf1d56e10, 0x953b4f3e,
};

Wtpsp32u puiEccpPubKeyX[8] = {
    0x57e4e4a8, 0x973b45a9, 0x0f262f3f, 0x8a3c8fe9,
    0x5f24b6fd, 0x2b5edc13, 0x61d32da6, 0xa947215a,
};

Wtpsp32u puiEccpPubKeyY[8] = {
    0xd7ce0e11, 0x70ee1669, 0x60b0d7c7, 0x6b8d4bdd,
    0x2b965c0b, 0xff8b6a2e, 0x6343fbd8, 0xc3081fa0,
};

Wtpsp32u puiEccpPrvKey[8] = {
    0x359c7c58, 0xe5f28e51, 0xc1ae34bf, 0x2cb5bb3d,
    0x6b457615, 0x41ca74a7, 0x9cf52324, 0x7d519a41,
};

Wtpsp32u puiDhPubKey[32] = {
    0x8e6f9634, 0xdd0dab71, 0x2b74348e, 0xfd5ca27c,
    0xfe94198e, 0xbada83dd, 0xeecf04b2, 0xd5874b09,
    0x617d322e, 0xf230b327, 0xee2b7cfb, 0x5e906128,
    0x8c82664e, 0x68797f1c, 0xb8828598, 0x7614e11c,
    0x09a2b945, 0xdd30a5e0, 0x3e86ead8, 0x1a3a5d2b,
    0x7b21cfa1, 0x6cc0a689, 0xe37a5f78, 0x932f58f7,
    0xab7d8b3b, 0xfa12e765, 0xa7521402, 0x4fe552fd,
    0xc8d18767, 0x981f3bc5, 0xd35d4799, 0x59024edc,
};

Wtpsp32u puiDhPrvKey[32] = {
    0xa53eee4e, 0xba5e777a, 0x7e493c8f, 0x56e0c384,
    0x51a7d770, 0x9bc4fc32, 0x52112d0f, 0xba51cc45,
    0x2b8b2c90, 0x1c49c1ad, 0x35a7ff65, 0x4c707ffe,
    0xb6274049, 0x5d0140ec, 0x8c1e1fe0, 0x07b17419,
    0x1b171719, 0xe1d610d8, 0xbfc0aa6b, 0x4f8880d0,
    0x19a2b6ea, 0x7c67a525, 0xc54c226b, 0x9954ab34,
    0x463b189a, 0xb4348178, 0xc02af02b, 0x41059f7a,
    0x2e10979f, 0xd06c8324, 0x9b8f1893, 0x75e9e1d4,
};

/*
 * In this example, we will enroll one aes_cbc_256 key
 * in the dm phase.
 */
#ifndef WIN

int main(void)

#else

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)

#endif
{
    Wtpsp32u uiSize;

    WtpspKeyRing *pstKeyRing;
    Wtpsp32u uiKeyId;

    Wtpsp32u uiKeyRingImageSize;
    Wtpsp8u *pucKeyRingImage;

    WtpspPasswdInfo stKeyRingPasswd;
    WtpspPasswdInfo stUsrKeyPasswd;

    FILE *fp;

    WtpspInit();

    /*
     * Allocate key ring context for one symmetric key 
     * and one asymmetric key.
     */
    WtpspKeyRingBufferSize(SYM_KEY_NUM, ASYM_KEY_NUM, &uiSize);
    pstKeyRing = (WtpspKeyRing *)malloc(uiSize);

    stKeyRingPasswd.pucPasswd    = (Wtpsp8u *)KEY_RING_PASSWD;
    stKeyRingPasswd.uiPasswdSize = strlen(KEY_RING_PASSWD);

    /* 
     * init key ring
     */
    WtpspDmKeyRingInit(&stKeyRingPasswd, SYM_KEY_NUM, ASYM_KEY_NUM, pstKeyRing);

    stUsrKeyPasswd.pucPasswd    = (Wtpsp8u *)USR_KEY_PASSWD;
    stUsrKeyPasswd.uiPasswdSize = strlen(USR_KEY_PASSWD);

    printf("Scheme                  -   Key Id\n");

    /*
     * enroll aes_cbc_256 key
     */
    WtpspDmKeyRingEnrollAesKey(&stKeyRingPasswd,
                               &stUsrKeyPasswd,
                               (Wtpsp8u *)pucAesCbc256Key,
                               NULL,
                               WtpspKeyAesCBC256,
                               &uiKeyId,
                               pstKeyRing);
    printf("WtpspKeyAesCBC256       -   0x%08x\n", uiKeyId);

    /*
     * enroll aes_ctr_256 key
     */
    WtpspDmKeyRingEnrollAesKey(&stKeyRingPasswd,
                               &stUsrKeyPasswd,
                               (Wtpsp8u *)pucAesCtr256Key,
                               NULL,
                               WtpspKeyAesCTR256,
                               &uiKeyId,
                               pstKeyRing);
    printf("WtpspKeyAesCTR256       -   0x%08x\n", uiKeyId);

    /*
     * enroll aes_ecb_256 key
     */
    WtpspDmKeyRingEnrollAesKey(&stKeyRingPasswd,
                               &stUsrKeyPasswd,
                               (Wtpsp8u *)pucAesEcb256Key,
                               NULL,
                               WtpspKeyAesECB256,
                               &uiKeyId,
                               pstKeyRing);
    printf("WtpspKeyAesECB256       -   0x%08x\n", uiKeyId);

    /*
     * enroll aes_xts_256 key
     */
    WtpspDmKeyRingEnrollAesKey(&stKeyRingPasswd,
                               &stUsrKeyPasswd,
                               (Wtpsp8u *)pucAesXts256Key,
                               (Wtpsp8u *)pucAesXts256Key,
                               WtpspKeyAesXTS256,
                               &uiKeyId,
                               pstKeyRing);
    printf("WtpspKeyAesXTS256       -   0x%08x\n", uiKeyId);
#if 0
    /*
     * enroll des_cbc key
     */
    WtpspDmKeyRingEnrollDesKey(&stKeyRingPasswd,
                               &stUsrKeyPasswd,
                               (Wtpsp8u *)pucDesCbcKey,
                               WtpspKeyDesCBC,
                               &uiKeyId,
                               pstKeyRing);
    printf("WtpspKeyDesCbc          -   0x%08x\n", uiKeyId);

    /*
     * enroll des_ecb key
     */
    WtpspDmKeyRingEnrollDesKey(&stKeyRingPasswd,
                               &stUsrKeyPasswd,
                               (Wtpsp8u *)pucDesEcbKey,
                               WtpspKeyDesECB,
                               &uiKeyId,
                               pstKeyRing);
    printf("WtpspKeyDesEcb          -   0x%08x\n", uiKeyId);

    /*
     * enroll tdes_cbc key
     */
    WtpspDmKeyRingEnrollTDesKey(&stKeyRingPasswd,
                                &stUsrKeyPasswd,
                                (Wtpsp8u *)pucTDesCbcKey,
                                (Wtpsp8u *)pucTDesCbcKey,
                                (Wtpsp8u *)pucTDesCbcKey,
                                WtpspKeyTDesCBC,
                                &uiKeyId,
                                pstKeyRing);
    printf("WtpspKeyTDesCbc         -   0x%08x\n", uiKeyId);

    /*
     * enroll tdes_ecb key
     */
    WtpspDmKeyRingEnrollTDesKey(&stKeyRingPasswd,
                                &stUsrKeyPasswd,
                                (Wtpsp8u *)pucTDesEcbKey,
                                (Wtpsp8u *)pucTDesEcbKey,
                                (Wtpsp8u *)pucTDesEcbKey,
                                WtpspKeyTDesECB,
                                &uiKeyId,
                                pstKeyRing);
    printf("WtpspKeyTDesEcb         -   0x%08x\n", uiKeyId);
#endif
    /*
     * enroll hmac_sha1 key
     */
    WtpspDmKeyRingEnrollHmacKey(&stKeyRingPasswd,
                                &stUsrKeyPasswd,
                                (Wtpsp8u *)pucHmacSha1Key,
                                WtpspKeyHmacSha1,
                                &uiKeyId,
                                pstKeyRing);
    printf("WtpspKeyHmacSha1        -   0x%08x\n", uiKeyId);
#if 0
    /*
     * enroll rc4 key
     */
    WtpspDmKeyRingEnrollRc4Key(&stKeyRingPasswd,
                               &stUsrKeyPasswd,
                               (Wtpsp8u *)pucRc4Key,
                               &uiKeyId,
                               pstKeyRing);
    printf("WtpspKeyRc4             -   0x%08x\n", uiKeyId);
#endif
    /*
     * enroll rsa key
     */
    WtpspDmKeyRingEnrollRsaKey(&stKeyRingPasswd,
                               &stUsrKeyPasswd,
                               (Wtpsp8u *)puiRsaKeyN,
                               (Wtpsp8u *)puiRsaKeyD,
                               WtpspKeyPkcsSha1Rsa1024,
                               &uiKeyId,
                               pstKeyRing);
    printf("WtpspKeyPkcsSha1Rsa1024 -   0x%08x\n", uiKeyId);

    /*
     * enroll eccp key
     */
    WtpspDmKeyRingEnrollEccpKey(&stKeyRingPasswd,
                                &stUsrKeyPasswd,
                                (Wtpsp8u *)puiEccpPrvKey,
                                WtpspKeySha1Eccp256,
                                &uiKeyId,
                                pstKeyRing);
    printf("WtpspKeySha1Eccp256     -   0x%08x\n", uiKeyId);
#if 0 /* currently not support */
    /*
     * enroll dh key
     */
    WtpspDmKeyRingEnrollDhKey(&stKeyRingPasswd,
                              &stUsrKeyPasswd,
                              (Wtpsp8u *)puiDhPrvKey,
                              WtpspKeyRsaDH1024,
                              &uiKeyId,
                              pstKeyRing);
    printf("WtpspKeyRsaDH1024       -   0x%08x\n", uiKeyId);
#endif
    /*
     * save key ring into a image
     */
    WtpspKeyRingImageBufferSize(
            SYM_KEY_NUM, ASYM_KEY_NUM, &uiKeyRingImageSize);
    pucKeyRingImage = malloc(uiKeyRingImageSize);

    WtpspKeyRingImageSave(pstKeyRing, pucKeyRingImage);

    /*
     * save key ring image to filesystem
     */
    fp = fopen(KEY_IMG_NAME, "wb");
    fwrite(pucKeyRingImage, uiKeyRingImageSize, 1, fp);

    /* 
     * free resource
     */
    fclose(fp);
    free(pucKeyRingImage);
    free(pstKeyRing);

    WtpspExit();

    return 0;
}

