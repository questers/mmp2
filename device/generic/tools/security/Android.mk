TOP_LOCAL_PATH:= $(call my-dir)
WTPSP_PATH:=$(TOP_LOCAL_PATH)/wtpsp/lib/bin/
WTPSP_SAMPLE_PATH:=$(TOP_LOCAL_PATH)/wtpsp/samples/src
AA_PATH:=$(TOP_LOCAL_PATH)/aa
MRVLDRM_PATH:=$(TOP_LOCAL_PATH)/mrvldrm

LOCAL_PATH:= $(call my-dir)
include $(WTPSP_PATH)/Android.mk
include $(WTPSP_SAMPLE_PATH)/Android.mk
include $(AA_PATH)/Android.mk
include $(MRVLDRM_PATH)/Android.mk
