LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
	usbmon.c 

LOCAL_SHARED_LIBRARIES := \
   libcutils \
   libutils
   
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := usbmon

include $(BUILD_EXECUTABLE)

