/*
V0:
目前需要3个GPIO，两个做输入（PWR_ON；PWR_OFF），一个做输出（PWR_EN）。逻辑如下：

PWR_ON：平时为高电平，
情况一：当PWR_ON电平低超过1.5S，并且PWR_EN为低电平时，PWR_EN变成高电平，并持续高电平。
情况二：当PWR_ON电平低超过4S，并且PWR_EN为高电平时，PWR_EN变成低电平，并持续低电平。

PWR_OFF：当PWR_EN为高电平时，PWR_OFF从高电平变到低电平时，
时间小于100ms时，PWE_EN变成低电平，后变高
时间大于100ms时，PWE_EN变成低电平，并持续低电平。

PWR_EN：默认是低电平。
V1:
  remove power_off short low detection which cause vcore reset.
V2:
  Optimize code executing performance.

*/
/*
1.5S  = 1500000us = 23*256 * 256
4S = 4000000us = 61*256 * 256
时间并非准确，仅作参考

*/

#include<pic.h>

__CONFIG (MCLRDIS & WDTDIS & UNPROTECT);

/*
 *	FUNC		PIN		IO
 * -------------------------------------
 *  GP0_LOWPWR  	GP0		I
 *  GP1_VCORE_EN	GP1		O
 *  GP2_PWRBTN	GP2		I
 *  GP3_PWROFF	GP3		I
*/
#define GP3_PWROFF		GP3
#define GP2_PWRBTN		GP2
#define GP1_VCORE_EN		GP1
#define GP0_LOWPWR		GP0

volatile unsigned char count;
volatile unsigned char last;
volatile unsigned char on; /* 0: power off; 1: power on */
//volatile unsigned char rst;/* 0: power on and normal state; 1: power on and in reset state */
volatile unsigned char en; /* enable/disable */

void main(void)
{
	OPTION = 0xD7;		//Timer0 source from internal osc, PSA for Timer0 and 1:256
	//CMCON0 = 0xF7;		//Comparator is off
	//see IO config table
	TRISGPIO = 0xD;			
	GPIO = 0x00;		//output set low
	//OSCCAL = 0;

	on = 0;

	while(1)
	{
		count = 0;
		last = 0;

		while (!GP2_PWRBTN && !on)
		{
			if (last > TMR0) count++;
			last = TMR0;

			if (count == 15) /* 1s */
			{
				GP1_VCORE_EN = 1;
				on = 1;
				count = 0;
				break;
			}
		}

		while (!GP2_PWRBTN && on)
		{
			if (last > TMR0) count++;
			last = TMR0;

			if (count == 45) /* 3s */
			{
				GP1_VCORE_EN = 0;
				on = 0;
				count = 0;
				break;
			}
		}

		while (!GP3_PWROFF && on)
		{
			if (last > TMR0) count++;
			last = TMR0;

			if (count == 15)
			{
				GP1_VCORE_EN = 0;
				on = 0;
				break;
			}
		}

		if (GP0_LOWPWR)
		{
			en = 1; /* enable GP0_LOWPWR triggerred reset */
		}

		if (!GP0_LOWPWR && on && en)
		{
			en = 0; /* disable */
			GP1_VCORE_EN = 0;
			/* delay about 400ms */
			while (1)
			{
				if (last > TMR0) count++;
				if (count >= 3) break;
				last = TMR0;
			}
			GP1_VCORE_EN = 1;
		}
	}
}
	
