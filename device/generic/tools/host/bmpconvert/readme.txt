bmpconvert.c is for converting from *.bmp to *.c 
-------------------------------------------------
step1:   convert your picture to *.bmp, save as 256 colors bitmap
step2:   ./bmptoc
         please input the .bmp file name:
         example.bmp
         please select output bmp file format
         1.RGB888
         2:RGB565
         2
         file open success.
         Color Plate Number: 256
         Converting ok!
step3:   example_rgb888.bmp is a 24bit(rgb888) bitmap converted from example.bmp if format choice is 1,use it as you wish
         example_rgb565.bmp is a 16bit(rgb565) bitmap converted from example.bmp if format choice is 2,use it as you wish
         example.c is the file you need, copy it to /drivers/video/logo/
         
         
rawconvert.c is for converting from *.raw to *.bmp
---------------------------------------------------
step1:   capture your framebuffer data to *.raw 
step2:   ./rawtobmp
         please input the .raw file name(should be in RGB565 format):
         example.raw
         please input the image width:
         800
         please input the image height:
         600
         Converting ok!
step3:   example_rgb888.bmp is the 24bit(rgb888) bitmap file created


bmptoraw.c is for converting from *.bmp to *.raw
------------------------------------------------------------------------
step1:   save your bitmap file as RGB888 format
step2:   ./bmp2raw
         please input the .bmp file name:
         example.bmp
         file open success.
         Converting ok!
step3:   example.raw is the file created

Enjoy!^^
Ellie