//ReadBitMap   
//   
#include <string.h>    
#include <math.h>      
#include <stdio.h>      
#include <stdlib.h>      
#include <malloc.h>   
  
  
#define   WIDTHBYTES(bits) (((bits)+31)/32*4)   
  
typedef unsigned char BYTE;   
typedef unsigned short WORD;   
typedef unsigned long DWORD;   
typedef long LONG;   
  
  
//位图文件头信息结构定义   
//其中不包含文件类型信息（由于结构体的内存结构决定，要是加了的话将不能正确读取文件信息）   
  
typedef struct tagBITMAPFILEHEADER {   
  
DWORD bfSize;           //文件大小   
WORD   bfReserved1;     //保留字，不考虑   
WORD   bfReserved2;     //保留字，同上   
DWORD bfOffBits;        //实际位图数据的偏移字节数，即前三个部分长度之和   
} BITMAPFILEHEADER;    
  
  
//信息头BITMAPINFOHEADER，也是一个结构，其定义如下：   
  
typedef struct tagBITMAPINFOHEADER{   
//public:   
DWORD   biSize;             //指定此结构体的长度，为40   
LONG    biWidth;            //位图宽   
LONG    biHeight;           //位图高   
WORD    biPlanes;           //平面数，为1   
WORD    biBitCount;         //采用颜色位数，可以是1，2，4，8，16，24，新的可以是32   
DWORD   biCompression;      //压缩方式，可以是0，1，2，其中0表示不压缩   
DWORD   biSizeImage;        //实际位图数据占用的字节数   
LONG    biXPelsPerMeter;    //X方向分辨率   
LONG    biYPelsPerMeter;    //Y方向分辨率   
DWORD   biClrUsed;          //使用的颜色数，如果为0，则表示默认值(2^颜色位数)   
DWORD   biClrImportant;     //重要颜色数，如果为0，则表示所有颜色都是重要的   
} BITMAPINFOHEADER;    
  
  
//调色板Palette，当然，这里是对那些需要调色板的位图文件而言的。24位和32位是不需要调色板的。   
//（似乎是调色板结构体个数等于使用的颜色数。）   
  
typedef struct tagRGBQUAD {    
//public:   
BYTE     rgbBlue; //该颜色的蓝色分量   
BYTE     rgbGreen; //该颜色的绿色分量   
BYTE     rgbRed; //该颜色的红色分量   
BYTE     rgbReserved; //保留值   
} RGBQUAD;   
  
  
#if 0  
void showBmpHead(BITMAPFILEHEADER* pBmpHead)   
{   
printf("位图文件头:\n");   
printf("文件大小:%d\n",pBmpHead->bfSize);   
printf("保留字:%d\n",pBmpHead->bfReserved1);   
printf("保留字:%d\n",pBmpHead->bfReserved2);   
printf("实际位图数据的偏移字节数:%d\n",pBmpHead->bfOffBits);   
  
}   
  
  
void showBmpInforHead(tagBITMAPINFOHEADER* pBmpInforHead)   
{   
printf("位图信息头:\n");   
printf("结构体的长度:%d\n",pBmpInforHead->biSize);   
printf("位图宽:%d\n",pBmpInforHead->biWidth);   
printf("位图高:%d\n",pBmpInforHead->biHeight);   
printf("biPlanes平面数:%d\n",pBmpInforHead->biPlanes);   
printf("biBitCount采用颜色位数:%d\n",pBmpInforHead->biBitCount);   
printf("压缩方式:%d\n",pBmpInforHead->biCompression);   
printf("biSizeImage实际位图数据占用的字节数:%d\n",pBmpInforHead->biSizeImage);   
printf("X方向分辨率:%d\n",pBmpInforHead->biXPelsPerMeter);   
printf("Y方向分辨率:%d\n",pBmpInforHead->biYPelsPerMeter);   
printf("使用的颜色数:%d\n",pBmpInforHead->biClrUsed);   
printf("重要颜色数:%d\n",pBmpInforHead->biClrImportant);   
}   
  
void showRgbQuan(tagRGBQUAD* pRGB)   
{    
printf("(%-3d,%-3d,%-3d)   ",pRGB->rgbRed,pRGB->rgbGreen,pRGB->rgbBlue);   
  
}   
#endif  
  
char bmp24h[54]=
{
    0x42, 0x4d, 
	0, 0, 0, 0,  // file size
	0, 0, 0, 0,
    0x36, 0, 0, 0, 
    0x28, 0, 0, 0,
    0, 0, 0, 0,  // width
    0, 0, 0, 0,  // height
    0x1, 0, 0x18, 0,  // 24 bit(RGB888)
    0, 0, 0, 0, 
    0, 0, 0, 0,  // data size
    0, 0, 0, 0,  // h dpi
    0, 0, 0, 0,  // v dpi
    0, 0, 0, 0,
    0, 0, 0, 0
};

char zero[4]={0};
int main()   
{   
  
FILE* pfile,*outfile;  
char *p;
int i,j,width,height;
  
char strFile[50]; 
printf("please input the .raw file name(should be in RGB565 format):\n");   
scanf("%s",strFile);   

printf("please input the image width:\n");   
scanf("%d",&width);   

printf("please input the image height:\n");   
scanf("%d",&height);   

pfile = fopen(strFile,"rb");//打开文件   

p=strFile+strlen(strFile)-4;
*p=0;

if(pfile==NULL)   
{   
   printf("file open fail!\n");   
   return -1;   
}   
  
//分配内存空间把源图存入内存      
int l_width   = WIDTHBYTES(width* 16);//计算位图的实际宽度并确保它为32的倍数   
BYTE    *pColorData=(BYTE *)malloc(height*l_width);      
memset(pColorData,0,height*l_width);      
long nData = height*l_width;   
BYTE *pb;

  
//把位图数据信息读到数组里      
fread(pColorData,1,nData,pfile);      

//将位图数据转化为RGB数据   

int l_width_dst   = WIDTHBYTES(width* 24);
long file_size = (long)l_width_dst * (long)height + 54;
bmp24h[2] = (unsigned char)(file_size &0x000000ff);
bmp24h[3] = (file_size >> 8) & 0x000000ff;
bmp24h[4] = (file_size >> 16) & 0x000000ff;
bmp24h[5] = (file_size >> 24) & 0x000000ff;

bmp24h[18] = width & 0x000000ff;
bmp24h[19] = (width >> 8) &0x000000ff;
bmp24h[20] = (width >> 16) &0x000000ff;
bmp24h[21] = (width >> 24) &0x000000ff;

bmp24h[22] = height &0x000000ff;
bmp24h[23] = (height >> 8) &0x000000ff;
bmp24h[24] = (height >> 16) &0x000000ff;
bmp24h[25] = (height >> 24) &0x000000ff;

file_size-=54;
bmp24h[34] = (unsigned char)(file_size &0x000000ff);
bmp24h[35] = (file_size >> 8) & 0x000000ff;
bmp24h[36] = (file_size >> 16) & 0x000000ff;
bmp24h[37] = (file_size >> 24) & 0x000000ff;

strcat(strFile,"_rgb888.bmp");
unlink(strFile);
outfile=fopen(strFile,"w");
fwrite(bmp24h, sizeof(unsigned char), 54, outfile);

struct tagRGBQUAD* dataOfBmp;   
dataOfBmp = (struct tagRGBQUAD *)malloc(width*height*sizeof(struct tagRGBQUAD));//用于保存各像素对应的RGB数据   
memset(dataOfBmp,0,width*height*sizeof(struct tagRGBQUAD));   
  
int k;   
int index = 0;   
  
for(i=height-1;i>=0;i--)   
 for(j=0;j<width;j++)   
 {   
  k = i*l_width + j*2;   
  WORD shortTemp;   
  shortTemp = pColorData[k+1];   
  shortTemp = shortTemp<<8;   
  shortTemp+=pColorData[k];
   
  dataOfBmp[index].rgbRed = (shortTemp & 0xf800) >> 8;   
  dataOfBmp[index].rgbGreen = (shortTemp & 0x7e0) >> 3;   
  dataOfBmp[index].rgbBlue = (shortTemp & 0x1f) << 3;   
  
  fwrite(&dataOfBmp[index], sizeof(unsigned char), 3, outfile);
  index++;  
 }   
 fwrite(zero, sizeof(unsigned char), WIDTHBYTES(width* 24)-3*width, outfile);
    
fclose(pfile);   
fclose(outfile);
free(dataOfBmp);   
free(pColorData);   
printf("Converting ok!\n");   
return 0;  
} 

