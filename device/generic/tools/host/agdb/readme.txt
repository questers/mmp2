agdb.py is a tool for android crash symbol analysis.

Usage: 
        debug native application:    agdb.py [options] process_name
        debug dalvik application:    agdb.py [options] --dalvik process_name
        convert address to file name and line number:    agdb.py [options] -r 
-e symbol_name address

        convert call stack to file names and line numbers:    (adb logcat OR 
cat tombstone_file) | agdb.py [options] -r


wrapper on android gdb

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  --android-src-root=ANDROID_SRC_ROOT
                        root of android source tree, can be set via
                        ANDROID_SRC_ROOT environment variable
  -d DEBUGGER_WRAPPER, --debugger-wrapper=DEBUGGER_WRAPPER
                        wrappers on gdb, e.g., cgdb, ddd
  --dalvik              debugee is dalvik(java) application [default: False]
  -k, --kill            kill process on target [default: False]
  --debugger-version=DEBUGGER_VERSION
                        specify a debugger version, e.g., 4.4.0, 4.2, 4. Will
                        use 4.4.0 if not specified
  --product-name=PRODUCT_NAME
                        product name, [default: generic]
  --program-args=PROGRAM_ARGS
                        arguments passed to debugee, [default: ]
  -l FILE_LOCATION, --location=FILE_LOCATION
                        the directory to search for process file on target,
                        [default: /system/bin/]
  -p GDB_PORT, --port=GDB_PORT
                        port used for gdbserver, [default: 7890]
  -s SERIAL_NUMBER      direct commands to device with given serial number.
  -r, --resolve         [addr2line] resolve address to file names and line
                        numbers [default: False]
  -e SYMBOL_FILE_NAME, --symbol-file=SYMBOL_FILE_NAME
                        [addr2line] specify the name of the executable for
                        which addresses should be translated. [default: a.out]
  -f, --functions       [addr2line] show function names [default: True]
  -C, --demangle        [addr2line] demangle function names [default: False]
  -S, --basenames       [addr2line] demangle function names [default: False]


For example:
cat tombstone_00 |./agdb.py --android-src-root=/opt/workspace/mmp2_devel/ --
product-name=phoenix  -r

tombstone_00 is a file pulled from /data/tombstones/tombstone_00 
