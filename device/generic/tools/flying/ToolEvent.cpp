/*
 * ToolEvent.cpp
 *
 *  Created on: May 16, 2011
 *      Author: lianbing
 */
#include <utils/Log.h>
#include <unistd.h>

#include "FlyingEvent.h"
using namespace flying;

int main(int argc, const char* const argv[]) {
	FlyingEvent event;
	event.start();
	sleep(300);
	LOGW("Bye flying!");
}
