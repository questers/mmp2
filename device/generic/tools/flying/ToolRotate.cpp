/*
 * ToolRotate.cpp
 *
 *  Created on: Jun 17, 2011
 *      Author: lianbing
 */

#include <utils/Log.h>
#include <stdio.h>

#include <surfaceflinger/SurfaceComposerClient.h>
using namespace android;

int main(int argc, const char* const argv[]) {
	LOGW("Hello ToolRotate!");
	int orientation = 0;
	int id = 0;
	if (argc == 3) {
		sscanf(argv[1], "%d", &id);
		sscanf(argv[2], "%d", &orientation);
		if (id < 0 || id > 3 || orientation < 0 || orientation > 2) {
			LOGW("invalid argument, set the default value!");
			id = 0;
			orientation = 1;
		}
	} else {
		orientation = 0;
	}
	SurfaceComposerClient::setOrientation(id, orientation, 0);
	LOGW("Bye ToolRotate!");

}
