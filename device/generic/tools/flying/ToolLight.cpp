/*
 * ToolLight.cpp
 *
 *  Created on: May 14, 2011
 *      Author: lianbing
 */

#include "FlyingLight.h"

#include <utils/Log.h>
#include <unistd.h>
#include <stdio.h>

using namespace flying;
int main(int argc, const char* const argv[]) {
	printf("hi:%d",125);
	LOGW("Hello ToolLight!");
	int brightness = 255;
	if (argc == 2) {
		sscanf(argv[1], "%d", &brightness);
	} else {
		LOGE("illegal arg number!!!");
		return -1;
	}
	LOGW("you input:%d",brightness);
	FlyingLight light;
	light.openDevice();
	light.setLight(brightness);
	light.closeDevice();
	LOGW("Bye ToolLight!");

}
