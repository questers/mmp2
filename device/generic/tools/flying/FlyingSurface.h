/*
 * FlyingSurface.h
 *
 *  Created on: Apr 15, 2011
 *      Author: lianbing
 */

#ifndef FLYINGSURFACE_H_
#define FLYINGSURFACE_H_

#include <unistd.h>
#include <assert.h>

#include <surfaceflinger/SurfaceComposerClient.h>
#include <surfaceflinger/ISurfaceComposer.h>
#include <ui/Region.h>
#include <ui/Rect.h>
#include <ui/DisplayInfo.h>
#include <EGL/egl.h>
#include <core/SkCanvas.h>
#include <core/SkBitmap.h>
#include <core/SkRegion.h>
#include <utils/Log.h>
using namespace android;

namespace flying {
class FlyingSurface {
private:
	static int sessionId;
	static sp<SurfaceComposerClient> session;
public:
	static void createSession();
	static void destroySession();
	static void openTransaction() {
		SurfaceComposerClient::openGlobalTransaction();
	}
	static void closeTransaction() {
		SurfaceComposerClient::closeGlobalTransaction();
	}
	static bool setOrientation(int display, int orientation, int flags) {
		status_t err = SurfaceComposerClient::setOrientation(display,
				orientation, flags);
		return err == 0;
	}
	static bool freezeDisplay(int display) {
		int err = SurfaceComposerClient::freezeDisplay(display, 0);
		return err == 0;
	}
	static bool unfreezeDisplay(int display) {
		int err = SurfaceComposerClient::unfreezeDisplay(display, 0);
		return err == 0;
	}
private:
	DisplayInfo display;
	sp<SurfaceControl> surfaceControl;
	sp<Surface> surface;
	SkCanvas canvas;
	int saveCount;

	FlyingSurface(const FlyingSurface& other);
	FlyingSurface& operator=(const FlyingSurface& other);

	SkBitmap::Config convertPixelFormat(PixelFormat format);
public:
	FlyingSurface();
	~FlyingSurface();

	SkCanvas* lockCanvas(const Rect& dirtyRect);
	void unlockCanvasAndPost();

	DisplayInfo getDisplayInfo() {
		return display;
	}

	void setLayer(int zorder) {
		surfaceControl->setLayer(zorder);
	}
	void setPosition(int x, int y) {
		surfaceControl->setPosition(x, y);
	}
	void setSize(int w, int h) {
		surfaceControl->setSize(w, h);
	}
	void hide() {
		surfaceControl->hide();
	}
	void show() {
		surfaceControl->show();
	}
	void freeze() {
		surfaceControl->freeze();
	}
	void unfreeze() {
		surfaceControl->unfreeze();
	}
	void setFlags(int flags, int mask) {
		surfaceControl->setFlags(flags, mask);
	}
	void setTransparentRegion(SkRegion region);
	void setAlpha(float alpha) {
		surfaceControl->setAlpha(alpha);
	}
	void setMatrix(float dsdx, float dtdx, float dsdy, float dtdy) {
		surfaceControl->setMatrix(dsdx, dtdx, dsdy, dtdy);
	}
	void setFreezeTint(int tint) {
		surfaceControl->setFreezeTint(tint);
	}
	void readFromParcel(const Parcel& parcel) {
		surface = Surface::readFromParcel(parcel);
	}
	void writeToParcel(Parcel& parcel) {
		SurfaceControl::writeSurfaceToParcel(surfaceControl, &parcel);
	}
};

}

#endif /* FLYINGSURFACE_H_ */
