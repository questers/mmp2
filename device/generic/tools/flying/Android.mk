LOCAL_PATH:= $(call my-dir)

# build ToolSensor
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	ToolSensor.cpp \

LOCAL_SHARED_LIBRARIES := \
	libhardware \

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE:= ToolSensor

include $(BUILD_EXECUTABLE)

# build ToolSurfaceFlinger
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	ToolSurfaceFlinger.cpp \
	FlyingSemaphoreObject.cpp \
	FlyingSurface.cpp \
	FlyingSynchronizedObject.cpp \
	FlyingThread.cpp \
	FlyingUI.cpp \
	FlyingConfig.cpp \
	
LOCAL_SHARED_LIBRARIES := \
	libutils \
	libskia \
	libsurfaceflinger_client \
	libui \

LOCAL_C_INCLUDES += \
	external/skia/include/core \
	$(LOCAL_PATH)/../../include \

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE:= ToolSurfaceFlinger

include $(BUILD_EXECUTABLE)

# build ToolEvent
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	ToolEvent.cpp \
	FlyingThread.cpp \
	FlyingEvent.cpp \
	
LOCAL_SHARED_LIBRARIES := \
	libutils \
	libui \

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../../include \

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE:= ToolEvent

include $(BUILD_EXECUTABLE)

# build ToolLight
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	ToolLight.cpp \
	FlyingLight.cpp \

LOCAL_SHARED_LIBRARIES := \
	libutils \
	libhardware \

LOCAL_C_INCLUDES += \
	hardware/libhardware/include \
	$(LOCAL_PATH)/../../include \

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE:= ToolLight

include $(BUILD_EXECUTABLE)

# build ToolRoot
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	ToolRoot.cpp \

LOCAL_SHARED_LIBRARIES := \
	libutils \

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE:= ToolRoot

include $(BUILD_EXECUTABLE)

# build ToolIO
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	ToolIO.cpp \

LOCAL_SHARED_LIBRARIES := \
	libutils \

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE:= ToolIO

include $(BUILD_EXECUTABLE)

# build ToolMtrace
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	ToolLibrary.cpp \

LOCAL_SHARED_LIBRARIES := \
	libutils \
	libdl \

# LOCAL_LDLIBS += -ldl

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/include \

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE := ToolLibrary

include $(BUILD_EXECUTABLE)

# build ToolRotate
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	ToolRotate.cpp \
	
LOCAL_SHARED_LIBRARIES := \
	libutils \
	libsurfaceflinger_client \

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../../include \

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE:= ToolRotate

include $(BUILD_EXECUTABLE)

# build shared library
include $(LOCAL_PATH)/lib/Android.mk