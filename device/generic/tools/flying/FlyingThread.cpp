/*
 * Thread.cpp
 *
 *  Created on: Dec 29, 2010
 *      Author: lianbing
 */

#include <FlyingThread.h>

namespace flying {

FlyingThread::~FlyingThread() {
}
static void* threadRun(void* target) {
	FlyingThread* thread = (FlyingThread*) target;
	thread->run();
	return 0;
}
void FlyingThread::start() {
	pthread_create(&id, 0, threadRun, (void*) this);
}

}
