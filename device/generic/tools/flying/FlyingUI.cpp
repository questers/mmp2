/*
 * FlyingThread.cpp
 *
 *  Created on: Apr 18, 2011
 *      Author: lianbing
 */

#include "FlyingUI.h"

namespace flying {

FlyingUI::~FlyingUI() {

}
void FlyingUI::onCreate() {
	FlyingSurface::createSession();
	surface = new FlyingSurface;
}
void FlyingUI::run() {
	onCreate();
	DisplayInfo display = surface->getDisplayInfo();
	LOGW("display:%d/%d",display.w,display.h);
	Rect rect(display.w, display.h);
	SkRect clipRect;
	clipRect.set(0, 0, display.w, display.h);
	SkPaint whitePaint;
	whitePaint.setColor(0xffffffff);
	SkPaint blackPaint;
	blackPaint.setColor(0xff000000);
	while (!canceled) {
		SkCanvas* canvas = surface->lockCanvas(rect);
		canvas->clipRect(clipRect);
		canvas->drawColor(0xff000000);
		canvas->drawColor(0x8800ff00);
		//=========================
		for (size_t i = 0; i < display.h; i++) {
			if (i & 0x1) {
				canvas->drawLine(0, i, display.w, i, whitePaint);
			} else {
				canvas->drawLine(0, i, display.w, i, blackPaint);
			}
		}
		//=========================
		//		canvas->drawPaint(paint);
		surface->unlockCanvasAndPost();
	}
	onDestroy();
}
void FlyingUI::onDestroy() {
	delete surface;
	surface = NULL;
}

}
