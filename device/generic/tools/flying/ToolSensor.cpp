/*
 * ToolSensor.cpp
 *
 *  Created on: Jun 9, 2011
 *      Author: lianbing
 */

#include <stdio.h>
#include <string.h>

#include <hardware/sensors.h>

static struct sensors_poll_device_t* sensorDevice;
static struct sensors_module_t* sensorModule;

sensor_t const* list;
ssize_t count;

static void open() {
	int err = hw_get_module(SENSORS_HARDWARE_MODULE_ID,
			(hw_module_t const**) &sensorModule);
	if (err) {
		printf("couldn't load %s module (%s)\n", SENSORS_HARDWARE_MODULE_ID,
				strerror(-err));
	}

	//	err = sensors_open(&sensorModule->common, &sensorDevice);
	err = sensorModule->common.methods->open(&(sensorModule->common),
			SENSORS_HARDWARE_POLL, (struct hw_device_t**) &sensorDevice);
	if (err) {
		printf("couldn't open device for module %s (%s)\n",
				SENSORS_HARDWARE_MODULE_ID, strerror(-err));
	}

	count = sensorModule->get_sensors_list(sensorModule, &list);
	printf("sensor count:%ld\n", count);
	printf("=========================\n");
	for (int i = 0; i < count; i++) {
		printf("handle:%d\n", (*(list + i)).handle);
		printf("maxRange:%f\n", (*(list + i)).maxRange);
		printf("minDelay:%d\n", (*(list + i)).minDelay);
		printf("name:%s\n", (*(list + i)).name);
		printf("power:%f\n", (*(list + i)).power);
		printf("resolution:%f\n", (*(list + i)).resolution);
		printf("type:%d\n", (*(list + i)).type);
		printf("vendor:%s\n", (*(list + i)).vendor);
		printf("version:%d\n", (*(list + i)).version);
		printf("=========================\n");
	}
}
static void active(int handle, bool enable) {
	int err = sensorDevice->activate(sensorDevice, handle, enable ? 1 : 0);
	if (err) {
		printf("couldn't active %d (%s)\n", handle, strerror(-err));
	}
}
static void setDelay(int handle, int64_t ns) {
	int err = sensorDevice->setDelay(sensorDevice, handle, ns);
	if (err) {
		printf("couldn't setDelay %d (%s)\n", handle, strerror(-err));
	}
}
static void readData(ssize_t count) {
	sensors_event_t* buffer = new sensors_event_t[count];
	ssize_t size = sensorDevice->poll(sensorDevice, buffer, count);
	printf("=========================\n");
	for (int i = 0; i < size; i++) {
		printf("version:%d\n", (*(buffer + i)).version);
		printf("sensor:%d\n", (*(buffer + i)).sensor);
		printf("type:%d\n", (*(buffer + i)).type);
		printf("timestamp:%lld\n", (*(buffer + i)).timestamp);
		printf("=========================\n");
	}
	delete[] buffer;
}
static void close() {
	int err = sensorDevice->common.close((hw_device_t*) sensorDevice);
	//err always be 0
}

int main(int argc, const char* const argv[]) {
	open();
	active(0, 1);
	active(1, 1);
	active(2, 1);
	setDelay(0, 10000000);
	readData(3);
	active(0, 0);
	active(1, 0);
	active(2, 0);
	close();
}
