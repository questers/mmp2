/*
 * ToolRoot.cpp
 *
 *  Created on: May 25, 2011
 *      Author: lianbing
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <pwd.h>
#include <fcntl.h>

#include <utils/Log.h>

//system(char * command)?
int main(int argc, char **argv) {
	char szppid[256];
	sprintf(szppid, "/proc/%d", getppid());
	struct stat stats;
	stat(szppid, &stats);
	passwd* passwordData = getpwuid(stats.st_uid);
	char* name = passwordData->pw_name;
	char* password = passwordData->pw_passwd;
	LOGW("name/password(%s/%s)",name,password);
	if (setgid(0) || setuid(0)) {
		LOGW("su: permission denied(%s)",strerror(errno));
		return -1;
	}
	LOGW("set OK");

	char *exec_args[argc + 1];
	exec_args[0] = "/system/bin/sh";
	exec_args[argc] = NULL;
	for (int i = 1; i < argc; i++) {
		exec_args[i] = argv[i];
	}
	execv("/system/bin/logwrapper", exec_args);
	LOGW("su: sh error(%s)", strerror(errno));
	return -errno;
}
