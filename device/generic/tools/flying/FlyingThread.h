/*
 * Thread.h
 *
 *  Created on: Dec 29, 2010
 *      Author: lianbing
 */

#ifndef FLYINGTHREAD_H_
#define FLYINGTHREAD_H_

#include <pthread.h>

namespace flying {

class FlyingThread {
public:
	FlyingThread() :
		canceled(false), id(0) {
	}
	virtual ~FlyingThread();
	pthread_t getThreadID() {
		return id;
	}
	void start();
	virtual void run()=0;
	void cancel() {
		canceled = true;
	}
protected:
	bool canceled;
private:
	pthread_t id;
	//	FlyingThread(const FlyingThread& thread);
	//	FlyingThread& operator=(const FlyingThread& thread);
};

}

#endif /* THREAD_H_ */
