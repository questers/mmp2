/*
 * FlyingEvent.cpp
 *
 *  Created on: May 13, 2011
 *      Author: lianbing
 */

#include "FlyingEvent.h"

#include <utils/Log.h>
using namespace android;

namespace flying {

FlyingEvent::FlyingEvent() :
	FlyingThread(), mEventHub(new EventHub), waiting(false) {

}
FlyingEvent::~FlyingEvent() {

}
void FlyingEvent::run() {
	while (1) {
		RawEvent event;
		mEventHub->getEvent(&event);
		process(event);
	}
}

void FlyingEvent::process(const RawEvent& rawEvent) {
	String8 name = mEventHub->getDeviceName(rawEvent.deviceId);
	switch (rawEvent.type) {
	case EventHubInterface::DEVICE_ADDED:
		LOGW("add:%s",name.string());
		break;

	case EventHubInterface::DEVICE_REMOVED:
		LOGW("add:%s",name.string());
		break;

	case EventHubInterface::FINISHED_DEVICE_SCAN:
		LOGW("finished scan:%s",name.string());
		break;

	default:
		consume(rawEvent);
		break;
	}
}
void FlyingEvent::consume(const RawEvent& rawEvent) {
	switch (rawEvent.type) {
	case EV_ABS:
		if (!waiting) {
			waiting = true;
			tempEvent.type = TouchEvent::Moving;
		}
		switch (rawEvent.scanCode) {
		case ABS_X:
			tempEvent.x = rawEvent.value;
			break;
		case ABS_Y:
			tempEvent.y = rawEvent.value;
			break;
		case ABS_PRESSURE:
			tempEvent.type = (rawEvent.value == 1 ? TouchEvent::Down
					: TouchEvent::Up);
			break;
		}
		break;
	case EV_SYN:
		switch (rawEvent.scanCode) {
		case SYN_REPORT:
			if (waiting) {
				printTouchEventType();
				eventBuffer.push_back(tempEvent);
				waiting = false;
			}
			break;
		}
		break;
	}
}
void FlyingEvent::printTouchEventType() {
	switch (tempEvent.type) {
	case TouchEvent::Down:
		LOGW("down:%d,%d",tempEvent.x,tempEvent.y);
		break;
	case TouchEvent::Up:
		LOGW("up:%d,%d",tempEvent.x,tempEvent.y);
		break;
	case TouchEvent::Moving:
		LOGW("moving:%d,%d",tempEvent.x,tempEvent.y);
		break;
	}
}

}
