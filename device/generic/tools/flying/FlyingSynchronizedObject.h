/*
 * SynchronizedObject.h
 *
 *  Created on: Dec 30, 2010
 *      Author: lianbing
 */

#ifndef FLYINGSYNCHRONIZEDOBJECT_H_
#define FLYINGSYNCHRONIZEDOBJECT_H_

#include <pthread.h>

namespace flying {

class FlyingSynchronizedObject {
private:
	pthread_mutex_t mMutex;
	pthread_cond_t mCondition;
	FlyingSynchronizedObject(const FlyingSynchronizedObject& syn);
	FlyingSynchronizedObject& operator=(const FlyingSynchronizedObject& syn);
public:
	void* obj;
public:
	FlyingSynchronizedObject();
	FlyingSynchronizedObject(pthread_mutex_t mutex, pthread_cond_t condition);
	~FlyingSynchronizedObject() {
		destroyLock();
		destroyCondition();
	}

	int lock();
	int unlock();
	int destroyLock();

	int wait();
	int timedWait(struct timespec* time);
	int signal();
	int broadcast();
	int destroyCondition();
};

}

#endif /* SYNCHRONIZEDOBJECT_H_ */
