/*
 * SynchronizedObject.cpp
 *
 *  Created on: Dec 30, 2010
 *      Author: lianbing
 */

#include <FlyingSynchronizedObject.h>

namespace flying {

FlyingSynchronizedObject::FlyingSynchronizedObject() {
	int status = pthread_mutex_init(&mMutex, NULL);
	status = pthread_cond_init(&mCondition, NULL);
}
FlyingSynchronizedObject::FlyingSynchronizedObject(pthread_mutex_t mutex,
		pthread_cond_t condition) :
	mMutex(mutex), mCondition(condition) {

}
//==================================
int FlyingSynchronizedObject::lock() {
	return pthread_mutex_lock(&mMutex);
}
int FlyingSynchronizedObject::unlock() {
	return pthread_mutex_unlock(&mMutex);
}
int FlyingSynchronizedObject::destroyLock() {
	return pthread_mutex_destroy(&mMutex);
}
//==================================
int FlyingSynchronizedObject::wait() {
	return pthread_cond_wait(&mCondition, &mMutex);
}
int FlyingSynchronizedObject::timedWait(struct timespec* time) {
	return pthread_cond_timedwait(&mCondition, &mMutex, time);
}
int FlyingSynchronizedObject::signal() {
	return pthread_cond_signal(&mCondition);
}
int FlyingSynchronizedObject::broadcast() {
	return pthread_cond_broadcast(&mCondition);
}
int FlyingSynchronizedObject::destroyCondition() {
	return pthread_cond_destroy(&mCondition);
}

}
