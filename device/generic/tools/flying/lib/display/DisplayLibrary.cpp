/*
 * DisplayLibrary.cpp
 *
 *  Created on: Jun 8, 2011
 *      Author: lianbing
 */

#include <utils/Log.h>

#define LOG_TAG "libflying_display"

#ifdef __cplusplus
extern "C" {
#endif

void displayHello() {
	LOGW("Hello Display");
}

#ifdef __cplusplus
}
#endif
