# build mtrace
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	DisplayLibrary.cpp \

LOCAL_SHARED_LIBRARIES := \
	libutils \

LOCAL_MODULE_PATH	:= $(TARGET_OUT_SHARED_LIBRARIES)

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE:= libflying_display

LOCAL_PRELINK_MODULE := false

include $(BUILD_SHARED_LIBRARY)