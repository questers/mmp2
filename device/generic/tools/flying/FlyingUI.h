/*
 * FlyingThread.h
 *
 *  Created on: Apr 18, 2011
 *      Author: lianbing
 */

#ifndef FLYINGUI_H_
#define FLYINGUI_H_

#include "FlyingSurface.h"
#include "FlyingThread.h"
using namespace flying;

namespace flying {

class FlyingUI: public FlyingThread {
public:
	FlyingUI() :
		FlyingThread(), surface(0) {
	}
	virtual ~FlyingUI();
	virtual void run();
private:
	FlyingSurface* surface;
	//	FlyingUI(const FlyingUI& thread);
	//	FlyingUI& operator=(const FlyingUI& thread);
	void onCreate();
	void onDestroy();
};

}

#endif /* FLYINGTHREAD_H_ */
