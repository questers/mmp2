/*
 * SemaphoreObject.h
 *
 *  Created on: Jan 5, 2011
 *      Author: lianbing
 */

#ifndef FLYINGSEMAPHOREOBJECT_H_
#define FLYINGSEMAPHOREOBJECT_H_

#include <semaphore.h>

namespace flying {

class FlyingSemaphoreObject {
private:
	sem_t sem;
	FlyingSemaphoreObject(const FlyingSemaphoreObject& sem);
	FlyingSemaphoreObject& operator=(const FlyingSemaphoreObject& sem);
public:
	FlyingSemaphoreObject(unsigned int value = 0) {
		sem_init(&sem, 0, value);//Linux目前只支持0
	}
	~FlyingSemaphoreObject() {
		destroy();
	}
	int getValue();
	void down() {
		int state = sem_wait(&sem);
	}
	void up() {
		int state = sem_post(&sem);
	}
	void destroy() {
		int state = sem_destroy(&sem);
	}
};

}
#endif /* SEMAPHOREOBJECT_H_ */
