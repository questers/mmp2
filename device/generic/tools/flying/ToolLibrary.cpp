/*
 * ToolMtrace.cpp
 *
 *  Created on: Jun 7, 2011
 *      Author: lianbing
 */

#include <utils/Log.h>
#include <dlfcn.h>

#define LOG_TAG "ToolDisplay"

int main(int argc, char* argv[]) {
	LOGW("Hello ToolLibrary!");
	void* handle = NULL;
	handle = dlopen("libflying_display.so", RTLD_NOW);
	if (!handle) {
		LOGW("dlopen failed: %s\n", dlerror());
		return -1;
	}
	void(*displayHello)() = NULL;
	displayHello = (void(*)()) dlsym(handle, "displayHello");
	if (!displayHello) {
		LOGW("dlsym failed: %s\n", dlerror());
		return -1;
	}
	displayHello();
	dlclose(handle);
}
