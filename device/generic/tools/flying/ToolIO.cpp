/*
 * ToolIO.cpp
 *
 *  Created on: May 25, 2011
 *      Author: lianbing
 */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <utils/Log.h>

int main(int argc, char **argv) {
	//	printf("(%d)printf(before redirect)\n", getpid());
	fprintf(stdout, "(%d)stdout(before redirect)\n", getpid());
	fprintf(stderr, "(%d)stderr(before redirect)\n", getpid());

	if (argc == 2) {
		const char* path = NULL;
		char* name = argv[1];
		if (!strcmp(name, "console")) {
			path = "/dev/console";
		} else if (!strcmp(name, "null")) {
			path = "/dev/null";
		} else {
			LOGW("illegal argument!!!");
			return -1;
		}
		int fd = open(path, O_RDWR);
		if (fd == -1) {
			LOGW("open device error(%s)", strerror(errno));
			return -1;
		}
		dup2(fd, 1);
		dup2(fd, 2);
		close(fd);
	}

	//	printf("(%d)printf(after redirect)\n", getpid());
	fprintf(stdout, "(%d)stdout(after redirect)\n", getpid());
	fprintf(stderr, "(%d)stderr(after redirect)\n", getpid());
}

