/*
 * SemaphoreObject.cpp
 *
 *  Created on: Jan 5, 2011
 *      Author: lianbing
 */

#include <FlyingSemaphoreObject.h>

namespace flying {

int FlyingSemaphoreObject::getValue() {
	int store = 0;
	int state = sem_getvalue(&sem, &store);
	return store;
}

}
