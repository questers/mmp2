/*
 * FlyingSurface.cpp
 *
 *  Created on: Apr 15, 2011
 *      Author: lianbing
 */
#include "FlyingSurface.h"
#include "FlyingConfig.h"

namespace flying {

int FlyingSurface::sessionId(0);
sp<SurfaceComposerClient> FlyingSurface::session(0);
void FlyingSurface::createSession() {
	if (session == 0) {
		LOGW("create session");
		session = new SurfaceComposerClient;
		//session->incStrong(&sessionId);
	}
}
void FlyingSurface::destroySession() {
	//session->decStrong(&sessionId);
	session->dispose();
}
FlyingSurface::FlyingSurface() {
	surfaceControl = session->createSurface(getpid(), 0, 800, 600,
			PIXEL_FORMAT_RGBA_8888);
	assert(Surface::isValid(surfaceControl));
	status_t status = session->getDisplayInfo(0, &display);
	assert(!status);
	//composer parameters必须在一对open/close transaction之间当close调用时，
	//参数被提交给服务端参数值的改变都存放在/frameworks/base/include/private/surfaceflinger/LayerState.h:layer_state_t中，
	//当调用closeTransaction时，结构体才会被传送到服务端。这样可以明显简化C/S接口、加快C/S通信速度。
	session->openTransaction();
	surfaceControl->setLayer(layerNumber);
	surfaceControl->setSize(display.w, display.h);
	surfaceControl->setPosition(0, 0);
	session->closeTransaction();

	surface = surfaceControl->getSurface();
	assert(Surface::isValid(surface));
}
FlyingSurface::~FlyingSurface() {
	surface.clear();
	surfaceControl.clear();
}
SkCanvas* FlyingSurface::lockCanvas(const Rect& dirtyRect) {
	// get dirty region
	Region dirtyRegion;
	if (!dirtyRect.isEmpty()) {
		dirtyRegion.set(dirtyRect);
	}
	Surface::SurfaceInfo info;
	status_t err = surface->lock(&info, &dirtyRegion);
	assert(err == 0);
	SkBitmap bitmap;
	ssize_t bpr = info.s * bytesPerPixel(info.format);
	bitmap.setConfig(convertPixelFormat(info.format), info.w, info.h, bpr);
	if (info.format == PIXEL_FORMAT_RGBX_8888) {
		bitmap.setIsOpaque(true);
	}
	if (info.w > 0 && info.h > 0) {
		bitmap.setPixels(info.bits);
	} else {
		// be safe with an empty bitmap.
		bitmap.setPixels(NULL);
	}
	canvas.setBitmapDevice(bitmap);

	saveCount = canvas.save();
	return &canvas;
}
void FlyingSurface::unlockCanvasAndPost() {
	// detach the canvas from the surface
	canvas.restoreToCount(saveCount);
	canvas.setBitmapDevice(SkBitmap());
	// unlock surface
	status_t err = surface->unlockAndPost();
	assert(err == 0);
}
void FlyingSurface::setTransparentRegion(SkRegion region) {
	const SkIRect& b(region.getBounds());
	Region reg(Rect(b.fLeft, b.fTop, b.fRight, b.fBottom));
	if (region.isComplex()) {
		SkRegion::Iterator it(region);
		while (!it.done()) {
			const SkIRect& r(it.rect());
			reg.addRectUnchecked(r.fLeft, r.fTop, r.fRight, r.fBottom);
			it.next();
		}
	}
	status_t err = surfaceControl->setTransparentRegionHint(reg);
}
//==========================================
SkBitmap::Config FlyingSurface::convertPixelFormat(PixelFormat format) {
	/* note: if PIXEL_FORMAT_RGBX_8888 means that all alpha bytes are 0xFF, then
	 we can map to SkBitmap::kARGB_8888_Config, and optionally call
	 bitmap.setIsOpaque(true) on the resulting SkBitmap (as an accelerator)
	 */
	switch (format) {
	case PIXEL_FORMAT_RGBX_8888:
		return SkBitmap::kARGB_8888_Config;
	case PIXEL_FORMAT_RGBA_8888:
		return SkBitmap::kARGB_8888_Config;
	case PIXEL_FORMAT_RGBA_4444:
		return SkBitmap::kARGB_4444_Config;
	case PIXEL_FORMAT_RGB_565:
		return SkBitmap::kRGB_565_Config;
	case PIXEL_FORMAT_A_8:
		return SkBitmap::kA8_Config;
	default:
		return SkBitmap::kNo_Config;
	}
}

}
