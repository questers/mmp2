/*
 * FlyingEvent.h
 *
 *  Created on: May 13, 2011
 *      Author: lianbing
 */

#ifndef FLYINGEVENT_H_
#define FLYINGEVENT_H_

#include "FlyingThread.h"

#include <utils/RefBase.h>
#include <utils/KeyedVector.h>
#include <utils/List.h>
#include <ui/EventHub.h>
#include <ui/InputReader.h>
using namespace android;

namespace flying {

class FlyingEvent: public FlyingThread {
public:
	FlyingEvent();
	virtual ~FlyingEvent();
	virtual void run();
	struct TouchEvent {
		enum TouchEventType {
			Down, Up, Moving
		};
		int32_t x;
		int32_t y;
		enum TouchEventType type;
	};
private:
	sp<EventHub> mEventHub;
	TouchEvent tempEvent;
	List<TouchEvent> eventBuffer;
	bool waiting;
	void process(const RawEvent& rawEvent);
	void consume(const RawEvent& rawEvent);
	void printTouchEventType();
};

}

#endif /* FLYINGEVENT_H_ */
