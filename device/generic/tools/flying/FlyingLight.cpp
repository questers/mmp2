/*
 * FlyingLight.cpp
 *
 *  Created on: May 14, 2011
 *      Author: lianbing
 */

#include "FlyingLight.h"

#include <assert.h>

namespace flying {

FlyingLight::FlyingLight() :
	light(NULL) {
	openDevice();
}
FlyingLight::~FlyingLight() {

}

void FlyingLight::setLight(int brightness) {
	//亮度转化成RGB值，R、G、B的值始终相同。
	int color = brightness & 0x000000ff;
	color = 0xff000000 | (color << 16) | (color << 8) | color;
	setLight(color, 0, 0, 0, 0);
}

void FlyingLight::openDevice() {

	hw_module_t* module;
	int err = hw_get_module(LIGHTS_HARDWARE_MODULE_ID,
			(hw_module_t const**) &module);
	assert(!err);
	hw_device_t* device;
	err = module->methods->open(module, LIGHT_ID_BACKLIGHT, &device);
	assert(!err);
	light = (light_device_t*) device;
}
void FlyingLight::setLight(int colorARGB, int flashMode, int onMS, int offMS,
		int brightnessMode) {
	light_state_t state;
	memset(&state, 0, sizeof(light_state_t));
	state.color = colorARGB;
	state.flashMode = flashMode;
	state.flashOnMS = onMS;
	state.flashOffMS = offMS;
	state.brightnessMode = brightnessMode;

	light->set_light(light, &state);
}
void FlyingLight::closeDevice() {
	light->common.close((hw_device_t*) light);
}

}
