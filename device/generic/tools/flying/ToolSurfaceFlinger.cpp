/*
 * flying.cpp
 *
 *  Created on: Apr 12, 2011
 *      Author: lianbing
 */

#include <utils/Log.h>
#include <unistd.h>
#include <stdio.h>

#include "FlyingUI.h"
#include "FlyingConfig.h"
using namespace flying;
int main(int argc, const char* const argv[]) {
	if (argc > 1) {
		sscanf(argv[1], "%d", &layerNumber);
	} else {
		layerNumber = 100000;
	}
	LOGW("Hello flying!");
	FlyingUI thread;
	thread.start();
	sleep(10);
	LOGW("Bye flying!");

}
