/*
 * FlyingLight.h
 *
 *  Created on: May 14, 2011
 *      Author: lianbing
 */

#ifndef FLYINGLIGHT_H_
#define FLYINGLIGHT_H_

#include <utils/RefBase.h>
#include <utils/Log.h>
#include <hardware/lights.h>
using namespace android;

namespace flying {

class FlyingLight: public RefBase {
public:
	FlyingLight();
	virtual ~FlyingLight();
	void openDevice();
	void setLight(int brightness);
	void closeDevice();
private:

	light_device_t* light;
	light_device_t* get_device(hw_module_t* module, char const* name);
	void setLight(int colorARGB, int flashMode, int onMS, int offMS,
			int brightnessMode);
};

}

#endif /* FLYINGLIGHT_H_ */
